#!/bin/bash

## -------------------
## -- Setup DIM DNS --
## -------------------
# DAQ-SIDE - aldaqecs01
export DIM_DNS_NODE=192.168.2.1
# DAQ-SIDE - aldaqecs02
#export DIM_DNS_NODE=192.168.2.2

# HLT-SIDE - portal-ecs0
export DIM_DNS_HOST=192.168.2.3
export DIM_DNS_PORT=2505

## ------------------------------------
## -- Set detector specific settings --
## ------------------------------------

user=`whoami`
if [ "${user}" = "hlt-operator" ] ; then
    export DETECTOR_CONTROL=ALICE
    let "runmanagerport = $ALIHLT_PORT_BASE_RUNCONTROL + $ALIHLT_PORTRANGE_DETECTOR * $ALIHLT_DETECTOR_IDENTIFIER_ALICE"
#    cd ${HOME}/control
#elif [ "${user}" = "tpc-operator" ] ; then
#    export DETECTOR_CONTROL=TPC
#    export ALIHLT_HCDBDIR=$HOME/HCDB
#    let "runmanagerport = $ALIHLT_PORT_BASE_RUNCONTROL + $ALIHLT_PORTRANGE_DETECTOR * $ALIHLT_DETECTOR_IDENTIFIER_TPC"
#    cd ${HOME}/control
#elif [ "${user}" = "trd-operator" ] ; then
#    export DETECTOR_CONTROL=TRD
#    export ALIHLT_HCDBDIR=$HOME/HCDB
#    let "runmanagerport = $ALIHLT_PORT_BASE_RUNCONTROL + $ALIHLT_PORTRANGE_DETECTOR * $ALIHLT_DETECTOR_IDENTIFIER_TRD"
#    cd ${HOME}/control
#elif [ "${user}" = "phos-operator" ] ; then
#    export DETECTOR_CONTROL=PHOS
#    export ALIHLT_HCDBDIR=$HOME/HCDB
#    let "runmanagerport = $ALIHLT_PORT_BASE_RUNCONTROL + $ALIHLT_PORTRANGE_DETECTOR * $ALIHLT_DETECTOR_IDENTIFIER_PHOS"
#    cd ${HOME}/control
#elif [ "${user}" = "dimuon-operator" ] ; then
#    export DETECTOR_CONTROL=DIMUON
#    export ALIHLT_HCDBDIR=$HOME/HCDB
#    let "runmanagerport = $ALIHLT_PORT_BASE_RUNCONTROL + $ALIHLT_PORTRANGE_DETECTOR * $ALIHLT_DETECTOR_IDENTIFIER_DIMUON"
#    cd ${HOME}/control
#elif [ "${user}" = "its-operator" ] ; then
#    export DETECTOR_CONTROL=ITS
#    export ALIHLT_HCDBDIR=$HOME/HCDB
#    let "runmanagerport = $ALIHLT_PORT_BASE_RUNCONTROL + $ALIHLT_PORTRANGE_DETECTOR * $ALIHLT_DETECTOR_IDENTIFIER_ITS"
#    cd ${HOME}/control
#elif [ "${user}" = "emcal-operator" ] ; then
#    export DETECTOR_CONTROL=EMCAL
#    export ALIHLT_HCDBDIR=$HOME/HCDB
#    let "runmanagerport = $ALIHLT_PORT_BASE_RUNCONTROL + $ALIHLT_PORTRANGE_DETECTOR * $ALIHLT_DETECTOR_IDENTIFIER_EMCAL"
#    cd ${HOME}/control
#elif [ "${user}" = "dev-operator" ] ; then
#    export DETECTOR_CONTROL=DEV
#    export ALIHLT_HCDBDIR=$HOME/HCDB
#    let "runmanagerport = $ALIHLT_PORT_BASE_RUNCONTROL + $ALIHLT_PORTRANGE_DETECTOR * $ALIHLT_DETECTOR_IDENTIFIER_DEV"
#    cd ${HOME}/control
#elif [ "${user}" = "test-operator" ] ; then
#    export DETECTOR_CONTROL=TEST
#    export ALIHLT_HCDBDIR=$HOME/HCDB
#    let "runmanagerport = $ALIHLT_PORT_BASE_RUNCONTROL + $ALIHLT_PORTRANGE_DETECTOR * $ALIHLT_DETECTOR_IDENTIFIER_TEST"
#    cd ${HOME}/control
fi

## ---------------------------------------
## -- RunManger / TaskManager BasePorts --
## ---------------------------------------

export ALIHLT_PORT_BASE_RUNMGR=$runmanagerport

let "port = $ALIHLT_PORT_BASE_RUNMGR + $ALIHLT_PORTRANGE_RUNMGR"
export ALIHLT_PORT_BASE_TASKMGR=$port

## -------------------------
## -- Set Interface Ports --
## -------------------------
let "port = $ALIHLT_PORT_BASE_TASKMGR - $ALIHLT_PORTOFFSET_PEND_1"
export ALIHLT_PORT_PEND_1=$port
let "port = $ALIHLT_PORT_BASE_TASKMGR - $ALIHLT_PORTOFFSET_PEND_2"
export ALIHLT_PORT_PEND_2=$port
let "port = $ALIHLT_PORT_BASE_TASKMGR - $ALIHLT_PORTOFFSET_PEND_3"
export ALIHLT_PORT_PEND_3=$port
let "port = $ALIHLT_PORT_BASE_TASKMGR - $ALIHLT_PORTOFFSET_PEND_SL"
export ALIHLT_PORT_PEND_SL=$port
let "port = $ALIHLT_PORT_BASE_TASKMGR - $ALIHLT_PORTOFFSET_PEND_MGR"
export ALIHLT_PORT_PEND_MGR=$port

let "port = $ALIHLT_PORT_BASE_TASKMGR - $ALIHLT_PORTOFFSET_ECSLIB"
export ALIHLT_PORT_ECSLIB=$port

## --------------------------
## -- Set HLT konsole path --
## --------------------------

#if [ ! -d ${HOME}/.kde/share/apps/konsole/profiles ] ; then
#    mkdir -p ${HOME}/.kde/share/apps/konsole/profiles
#fi

#if [ ! -h ${HOME}/.kde/share/apps/konsole/profiles/HLTKonsole ] ; then
#    pushd  ${HOME}/.kde/share/apps/konsole/profiles > /dev/null
#    ln -s ${HLTCONTROL_SETUPDIR}/HLTKonsole HLTKonsole
#    popd > /dev/null
#else
#    KONSOLE_LINK=`readlink ${HOME}/.kde/share/apps/konsole/profiles/HLTKonsole`

#    if [ "${KONSOLE_LINK}" != "${HLTCONTROL_SETUPDIR}/HLTKonsole" ] ; then
#	pushd  ${HOME}/.kde/share/apps/konsole/profiles > /dev/null
#	rm HLTKonsole
#	ln -s ${HLTCONTROL_SETUPDIR}/HLTKonsole HLTKonsole
#	popd > /dev/null
#    fi
#fi

## -----------------
## -- SET ALIASES --
## -----------------

# -- ls aliases
# ---------------
alias l="ls -l"
alias lr="ls -ltrh"
alias ll="ls -lh"
alias la="ls -lah"

# -- typo aliases
# -----------------
#alias xs="cd"
#alias dc="cd"
#alias cd..="cd .."

# -- xemacs aliases
# -------------------
alias xemacs="xemacs -bg white"
alias xe="xemacs"

# --firefox aliases
# -------------------
#alias mozilla="mozilla --no-xshm" 
#alias firefox="firefox --no-xshm"

# -- ssh alias
# --------------
alias ssh="ssh -o StrictHostKeyChecking=no"

# -- node aliases
# -----------------
alias ecs0="ssh -X portal-ecs0"
alias ecs1="ssh -X portal-ecs1"
alias dcs0="ssh -X portal-dcs0"
alias dcs1="ssh -X portal-dcs1"
alias dev0="ssh -X dev0"
alias dev1="ssh -X dev1"
alias dev2="ssh -X dev2"
alias dev3="ssh -X dev3"

#alias eve="ssh -X hlt@aldaqacrs1.cern.ch eve"
alias s1="ssh -X hlt@aldaqacrs1.cern.ch"

# -- module aliases
# -------------------
alias ma="module avail"
alias mu="module unload"
alias ml="module load"
alias mls="module list"
alias mrm="module unload HLTdevel && module unload HLT"
alias mh="module avail 2>&1 | grep HLT/"

# -- afs alias
# --------------
#alias k="kinit"
#alias kl="klist -T"
#alias kr="kinit -R"

# -- run control alias
# ----------------------
#alias hltkonsole='konsole -profile=HLTKonsole'

#alias InfoBrowser="ssh -X infologger /opt/HLT/tools/bin/infoBrowser.sh"
#alias infoBrowser="ssh -X infologger /opt/HLT/tools/bin/infoBrowser.sh"

# -- svn alias
# --------------
#alias coHLT="svn co https://alisoft.cern.ch/AliRoot/trunk/HLT"
