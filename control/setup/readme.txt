==================================
 Overview of files in this folder
==================================
Last Change : 2008-08-30
Authors     : Jochen Thaeder
==================================

* HLTKonsole  
----------------------------------
 - Set konsole layout for operators
 - is linked at /afsuser/<operator-name>/.kde/share/apps/konsole/profiles/HLTKonsole

.loadModules.sh   
----------------------------------
 - Load general moduls for operators

readme.txt  
----------------------------------
 -  This file

* .setbash.sh 
----------------------------------
 - Set common aliases, and environment variables for operators
 - Set also environment variables special for one operator