#!/bin/bash
#
# * Get Network usgae on localhost. Gives 
#
# File   : getLocalNetUsage.sh
#
# Author : Timo Breitner  tbreitne _at_ kip.uni-heidelberg.de ( Initial )
# Date   : 05.12.2010
#

INTERVAL=1

if [[ -n "$1" ]]
then INTERVAL="$1"
fi


if [[ -z "$(which sar)" ]]
then
  echo "sar not found, please install sysstat" >&2
  exit 1
fi


echo -e "${HOSTNAME}:\t $(sar -n DEV $INTERVAL 1 | grep -v Average | grep -v Linux | grep eth1)"

