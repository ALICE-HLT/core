#!/bin/bash
# 
# * Control RunManager
#
# File   : activate-config.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 06.12.2007
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

# -- Check Comand Line Arguments --
# ---------------------------------
if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0 [--help]"
else
    echo "Error: This command has to be started as operator."
    exit 1
fi

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    * ) echo "Error : Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    fi

    echo "Error : Argument not known: $cmd"
    echo ${USAGE}
    exit 1

done

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------

${HLTCONTROL_TOPDIR}/statsGUI/statsGUI ${MASTERNODE} ${ALIHLT_PORT_BASE_TASKMGR}