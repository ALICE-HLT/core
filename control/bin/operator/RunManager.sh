#!/bin/bash
# 
# * Control RunManager
#
# File   : RunManager.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.05.2008
#
###################################################################
# Include Functions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

# -- Defaults --
# --------------
MODE=start
PASSIVE=""

# -- Check Comand Line Arguments --
# ---------------------------------
if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0 (start|stop  - start is default) [-force-kill] [-passive] [--help]"
else
    USAGE="Usage : $0 -detector=<Detector-ID> (start|stop  - start is default) [-force-kill] [--help]"
fi

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get force kill all
	    -force-kill* ) FORCEKILL=$arg ;;    

	    # Passive part in active/passive failover setting
	    -passive* ) PASSIVE=-failoverpassive ;;    

	    * ) echo "Error : Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    else
	case "$cmd" in

	    # get start
	    start ) MODE=start ;;    

	    # get stop
	    stop )  MODE=stop ;;

	    * ) echo "Error : Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    fi
done

# -- Check usage --
# -----------------
if [ ! ${DETECTOR} ] ; then
    echo -e "Error: " ${USAGE}
    exit 1
fi

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------

## -- if detector == ALICE start with pendolino
#if [ "${DETECTOR}" == "ALICE" ] ; then
#    RUNMANAGER_BIN=`dirname ${RUNMANAGER_BIN}`/`basename ${RUNMANAGER_BIN} .py`-with-pendolino.py
#fi

# -- START / STOP --
# ------------------
if [ "$MODE" = "start" ] ; then

    PROCESS=`ps ux | grep RunManager.xml | grep -v grep`
    RUN_PROCESS=$?

    SECONDMASTER=""

  #  if [[ "${MASTERNODE}" == "${ECSNODE_0}" ]] ; then
  #      SECONDMASTER="-secondmaster ${ECSNODE_1}"
  #  elif [[ "${MASTERNODE}" == "${ECSNODE_1}" ]] ; then
  #      SECONDMASTER="-secondmaster ${ECSNODE_0}"
  #  elif [[ "${MASTERNODE}" == "fepdev12" ]] ; then
  #      SECONDMASTER="-secondmaster fepdev11" # Just for testing on development cluster
  #  elif [[ "${MASTERNODE}" == "fepdev11" ]] ; then
  #      SECONDMASTER="-secondmaster fepdev12" # Just for testing on development cluster
  #  else
  #      SECONDMASTER=""
  #  fi


    if [ "${RUN_PROCESS}" = "0" ] ; then
        echo "RunManager is already running"  
    else
        # -- Dump used modules 
	. /etc/profile.modules
	module list 2> ${DETECTORRUNDIR}/ModulesConfig.txt

	# Start RunManager
        ${RUNMANAGER_BIN} -nopendolino -basedir ${HLTCONTROL_TOPDIR} -runcontroldir ${HLTCONTROL_TOPDIR} -rundir ${HLTCONTROL_DIR}/run/${DETECTOR} -partition ${DETECTOR} ${PASSIVE} ${SECONDMASTER}
    fi
else

    if [ ${FORCEKILL} ] ; then
        # -- Get KillBinary --
        # --------------------
	KILLBINARY=${DETECTORRUNDIR}/config/config-created/${DETECTOR}-${MASTERNODE}-${MASTERNODE}-Master-Kill.sh
	KILLBINARY_1=${DETECTORRUNDIR}/config/config-created/${DETECTOR}-portal-ecs0-portal-ecs0-Master-Kill.sh
	KILLBINARY_2=${DETECTORRUNDIR}/config/config-created/${DETECTOR}-portal-ecs1-portal-ecs1-Master-Kill.sh
    
	if [ \( ! -x ${KILLBINARY} \) -o \( ! -f ${KILLBINARY} \) ] ; then
	    if [ \( ! -x ${KILLBINARY_1} \) -o \( ! -f ${KILLBINARY_1} \) ] ; then
		if [ \( ! -x ${KILLBINARY_2} \) -o \( ! -f ${KILLBINARY_2} \) ] ; then
		    echo "Error : Cannot execute master kill script ${KILLBINARY}"
		    exit 3
		else  
		    KILLBINARY=${KILLBINARY_2}
		fi
	    else
		KILLBINARY=${KILLBINARY_1}
	    fi
	fi
     
     	${KILLBINARY}
    fi

    killall TaskManager 2>> /dev/null

    rm -rf ${DETECTORRUNDIR}/TaskManager-RunManager.xml-*.log ${DETECTORRUNDIR}/ECS-RunManager-*.txt ${DETECTORRUNDIR}/ModulesConfig.txt
fi