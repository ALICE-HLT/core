#!/bin/bash
# 
# * COMMIT configuration Files for HLT
#
# File   : verify-config.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.05.2008
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

# -- DEFAULT --
# -------------
RUNCONFIG=0

# -- Check Comand Line Arguments --
# ---------------------------------
if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0 -config=<Config-ID> -type=<Config-Type:default=production> [-guiusage] [-runconfig] [-cleanup] [--help]\nWhere <Config-ID> is the directory name of the configuration, not the full path name. For example: if you have the path ${HLTCONTROL_DIR}/detector/ALICE/config/ALICE-Dummy the config ID is ALICE-Dummy."
else
    USAGE="Usage : $0 -config=<Config-ID> -detector=<Detector-ID> -type=<Config-Type:default=production> [-guiusage] [-runconfig] [-cleanup] [--help]\nWhere <Config-ID> is the directory name of the configuration, not the full path name. For example: if you have the path ${HLTCONTROL_DIR}/detector/ALICE/config/ALICE-Dummy the config ID is ALICE-Dummy."
fi

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo -e ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get configuration
	    -config* )  CONFIG=$arg ;;

	    # get config type
	    -type*)     CONFIGTYPE=$arg ;;

	    # get cleanup for SHM regions
	    -cleanup )  CLEANUP=$cmd ;;

	    # get flag if used by GUI
	    -guiusage )  GUIUSAGE=1 ;;

	    # get flag if used for checking the running also
	    -runconfig )  RUNCONFIG=1 ;;
	    * ) echo "Error Argument not known: $cmd"; echo -e ${USAGE};  exit 1;;
	esac		

    else
	echo "Error Argument not known: $cmd"
	echo -e ${USAGE} 
	exit 1
    fi
done

# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} || ! ${CONFIG} ]] ; then
    echo -e "Error: " ${USAGE}
    exit 1
fi

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
CHECK_CONFIG
# --------------------------------

# -- Check Configuration status ${CONFIG} in svn --
# -------------------------------------------------
UNCOMMITTED=0
if [ ! -d ${CONFIGDIR}/.svn ] ; then
    #echo Configuration directory not added to svn
    UNCOMMITTED=1
else
    # store xml file
    FOUND=`svn status ${CONFIGDIR} | grep "?.*${CONFIGFILE}$"`
    if [ -n "${FOUND}" ] ; then
        # Configuration file not added to svn
	UNCOMMITTED=2
    else
	SVNSTATUS=`svn status ${CONFIGFILE}`
	if [ -n "${SVNSTATUS}" ] ; then
                # Changes to Configuration file
	    UNCOMMITTED=3
	fi
    fi
fi

if [ "${GUIUSAGE}" != "1" ] ; then
    case "${UNCOMMITTED}" in 
	1 ) echo "New Configuration : ${CONFIG} not committed to svn yet." ;;
	2 ) echo "New Configuration : ${CONFIGFILE} not committed to svn yet." ;;
	3 ) echo "Uncommitted changes in Configuration file : ${CONFIGFILE}" ;;
    esac
fi

# -- No changes --
# ----------------
if [ "${UNCOMMITTED}" == "0" ] ; then
    echo "No changes in Configuration ${CONFIGFILE}"
    echo "-- Verified --"
    exit 0
fi

# -- Create Configuration ${CONFIG} --
# ------------------------------------
${SYSTEM_BINDIR}/verify-config-create.sh -detector=${DETECTOR} -config=${CONFIG} -type=${CONFIGTYPE}

if [ $? -gt 0 ] ; then
    exit 3
fi 

# -- Release Config --
# --------------------
if [ "${RUNCONFIG}" == "1" ] ; then
   
    echo Releaseing ${CONFIG}
    ${RELEASE_BIN} -detector=${DETECTOR} -quiet
fi

# -- Testing Configuration ${CONFIG} --
# -------------------------------------
if [ "${RUNCONFIG}" == "1" ] ; then

    echo Running Test ${CONFIG}
    ${SYSTEM_BINDIR}/verify-config-test.sh -detector=${DETECTOR} -config=${CONFIG} -type=${CONFIGTYPE} ${CLEANUP}
    
    if [ $? -gt 0 ] ; then
	exit 4
    fi
fi

# -- Commit changes --
# --------------------

if [ "${GUIUSAGE}" != "1" ] ; then
    echo "Do you want to commit the changes now? Enter \"(N)o\" if you don't want to commit. Default is yes."
    
    read TMP
    if [[ "$TMP" == "n" || "$TMP" == "no" ||"$TMP" == "No" ||"$TMP" == "N" ]] ; then
	echo "Please run ${DETECTORDIR}/bin/verify-conifg.sh to verify configuration changes and add them into svn."
	exit 5
    fi
    
    ${SYSTEM_BINDIR}/commit-config-changes.sh -detector=${DETECTOR} -config=${CONFIG} -type=${CONFIGTYPE}

    if [ $? -gt 0 ] ; then
	exit 6
    fi 
fi