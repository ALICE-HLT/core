#!/bin/bash
# 
# * Activate configuration
#
# File   : activate-config.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.10.2009
#
# * Update to auto configuration 
#
###################################################################
# Include Defintions and Defaults
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

RUNTYPE="STANDALONE"

# -- Check Comand Line Arguments --
# ---------------------------------
if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0 -config=<Config Name> -type=<Config-Type:default=production> -beamtype=<ALICE beam type (pp|AA) default pp> [--help]"
else
    USAGE="Usage : $0 -detector=<Detector-ID> -config=<Config Name> -type=<Config-Type:default=production> -beamtype=<ALICE beam type (pp|AA)default pp> [--help]"
fi

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get config
	    -config* ) SRCCONFIG=$arg ;;    

	    # get config type
	    -type*)     CONFIGTYPE=$arg ;;

	    # get beamtype, pp or AA
	    -beamtype* )  BEAMTYPE=$arg ;;

	    * ) echo "Error : Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    else
	echo "Error : Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
    fi
done

# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} || ! ${SRCCONFIG} || ! ${BEAMTYPE} ]] ; then
    echo "Error : " ${USAGE}
    exit 1
fi

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------


# -- Check Source Configuration --
# --------------------------------
TGTCONFIG="${DETECTOR}-${BEAMTYPE}-${RUNTYPE}"

LIST=`svn list ${REPOS}/${REPOSDIR}|grep -- "${DETECTOR}/"`
if [ -z "${LIST}" ] ; then
  echo "Detector directory detector/${DETECTOR}/ does not exist in SVN"
  exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/config/${CONFIGTYPE}|grep -- "${SRCCONFIG}/"`
if [ -z "${LIST}" ] ; then
  echo "Configuration directory detector/${DETECTOR}/config/${CONFIGTYPE}/${SRCCONFIG} does not exist in SVN"
  exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/config/${CONFIGTYPE}/${SRCCONFIG}/|grep -- "${SRCCONFIG}.xml"`
if [ -z "${LIST}" ] ; then
  echo "Configuration file ${DETECTOR}/config/${CONFIGTYPE}/${SRCCONFIG}/${SRCCONFIG}.xml does not exist in SVN"
  exit 1
fi

# -- Check if TGTCONFIG exist --
# ------------------------------
LIST_CHECK=`svn list ${REPOS}/${CONFIGREPOSDIR}/partitions/${DETECTOR}/devel/${TGTCONFIG}.xml 2>&1 | grep "non-existent in that revision"`
if [  -z "${LIST_CHECK}" ] ; then
    svn -q rm ${REPOS}/${CONFIGREPOSDIR}/partitions/${DETECTOR}/devel/${TGTCONFIG}.xml -m "activate config ${TGTCONFIG} with ${SRCCONFIG}"
fi    

# -- copy inside svn --
# ---------------------
svn -q cp ${REPOS}/${REPOSDIR}/${DETECTOR}/config/${CONFIGTYPE}/${SRCCONFIG}/${SRCCONFIG}.xml ${REPOS}/${CONFIGREPOSDIR}/partitions/${DETECTOR}/devel/${TGTCONFIG}.xml -m "activate config ${TGTCONFIG} with ${SRCCONFIG}"

# -- update hlt-configuration --
# ------------------------------
pushd ${HLTCONFIGPARTITIONDEVELDIR} > /dev/null
svn -q up
popd > /dev/null

# -- Write Config Name to File for Status display --
# --------------------------------------------------
if [ ! -d ${DETECTORDIR}/run/config ] ; then
    mkdir -p ${DETECTORDIR}/run/config
fi

pushd ${DETECTORDIR}/run/config > /dev/null
echo ${CONFIGTYPE}/${SRCCONFIG} > .active.${TGTCONFIG}
popd > /dev/null

exit 0