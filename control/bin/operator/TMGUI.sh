#!/bin/bash
# 
# * RUN TMGUI for HLT
#
# File   : TMGUI.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 24.06.2007
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

# -- DEFAULTS --
# --------------
INDDLLIST="A=3-0,B=3-1,C=3-2,D=3-3"
OUTDDLLIST="a=28-0,b=28-1"

# -- Check Comand Line Arguments --
# ---------------------------------
if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0 [-master-node=<Master-Node>] [-sendrunparameters -inddllist=<A=3-0,B=3-1,C=3-2,D=3-3> -outddllist=<a:28-0,b:28-1>] [--help]"
else
    echo "Error: This command has to be started as operator."
    exit 1
fi

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get configuration
	    -config* )  CONFIG=$arg ;;

	    # get master node
	    -master-node* ) MASTERNODE=$arg ;;    

             # get sendrunparameters
	    -sendrunparameters ) SENDRUNPARAMETERS=$cmd ;;

	    # get inddllist
	    -inddllist* ) INDDLLIST=$arg ;;

	    # get outddllist
	    -outddllist* ) OUTDDLLIST=$arg ;;
	    
	    * ) echo "Error Argument not known: $cmd"; echo ${USAGE};  exit 1;;
	esac		

    else
	echo "Error Argument not known: $cmd"
	echo ${USAGE} 
	exit 1

    fi
done

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS #  "NO_MASTER_CHECK" 
# --------------------------------

# -- Get TMGUI PORT --
# --------------------
let "TOPPORT = $ALIHLT_PORT_BASE_RUNMGR + $ALIHLT_PORTRANGE_DETECTOR"

for ((offset=1 ; offset<99 ; offset=offset+1)) ; do
    let "PORT = $TOPPORT -$offset"
    echo $PORT
    RESVAL=`netstat -aptunv 2>&1 | grep $PORT`
    if [ ! "${RESVAL}" ] ; then
	TMPORT=$PORT
	break
    fi
done

if [ ! $TMPORT ] ; then
    echo "No free Port availible !!"
    exit 6
fi

# -- Get RUNARGS --
# -----------------
if [ ${SENDRUNPARAMETERS} ] ; then
    RUNARGS=${SENDRUNPARAMETERS}

    if [ ${INDDLLIST} ] ; then
	RUNARGS="${RUNARGS} -inddllist ${INDDLLIST}"
    fi

    if [ ${OUTDDLLIST} ] ; then
	RUNARGS="${RUNARGS} -outddllist ${OUTDDLLIST}"
    fi

    RUNARGS="${RUNARGS} -runnumberfile ${HLTCONTROL_TOPDIR}/templates/.runnumber"
fi

echo "Running TMGUI on host: `hostname -s`  port: $TMPORT ."
TMGUI.py -address tcpmsg://:${TMPORT} -control tcpmsg://${MASTERNODE}:${ALIHLT_PORT_BASE_RUNMGR} -unconnected ${RUNARGS} -runmanagercontrol