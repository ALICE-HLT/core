#!/bin/bash
#
# * Get CPU usgae on localhost. Gives 
#   "Total Idle time" "highest CPU usage" "procname" "PID"
#
# File   : getLocalCpuUsage.sh
#
# Author : Timo Breitner  tbreitne _at_ kip.uni-heidelberg.de ( Initial )
# Date   : 05.12.2010
#

INTERVAL=1

if [[ -n "$1" ]]
then INTERVAL="$1"
fi


if [[ -z "$(which pidstat)" ]]
then
  echo "pidstat not found, please install sysstat" >&2
  exit 1
fi

if [[ -z "$(which mpstat)" ]]
then
  echo "mpstat not found, please install sysstat" >&2
  exit 1
fi


echo -e "${HOSTNAME}:\t $(mpstat $INTERVAL 1 | grep Average | awk '{print $10}')  $(pidstat $INTERVAL 1| grep Average | grep -v PID  | awk '{print $5 "  " $7 "  " $2 }' | sort -n -r | head -n1)"
