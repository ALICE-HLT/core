#!/bin/bash
# 
# * Checkout configuration file for HLT from svn repository
#
# File   : checkout-config.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 08.07.2009
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

# -- DEFAULTS --
# --------------
CREATE=no
DETECTORID=""
SNIPPETID=""

# -- Check Comand Line Arguments --
# ---------------------------------
if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0 -beamtype=<ALICE beam type (pp|AA)> -runtype=<detector run-type> -type=<type of file (stability level); usually used production, valid-test or devel (not necessary for -filetype=allowedtypes)> -filetype=<file type (partitionbase|partitionbeam|partitionrun|partitionbeamrun|triggerlist|trigger|component|detectorbase|detectorbeam|detectorrun|detectorbeamrun)> (-id=<snippet identifier>) (-snippetdetectorid=<detector identifier>) (-create=<Create file if it does not exist (yes|no)>) [--help]"
else
    USAGE="Usage : $0 -detector=<Detector-ID> -beamtype=<ALICE beam type (pp|AA)> -runtype=<detector run-type> -type=<type of file (stability level); usually used production, valid-test or devel (not necessary for -filetype=allowedtypes)> -filetype=<file type (partitionbase|partitionbeam|partitionrun|partitionbeamrun|triggerlist|trigger|component|detectorbase|detectorbeam|detectorrun|detectorbeamrun)> (-id=<snippet identifier>) (-snippetdetectorid=<detector identifier>) (-create=<Create file if it does not exist (yes|no)>) [--help]"
fi

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get beamtype, pp or AA
	    -beamtype* )  BEAMTYPE=$arg ;;

	    # get runtype
	    -runtype* )  RUNTYPE=$arg ;;

	    # get filetype
	    -filetype* )  FILETYPE=$arg ;;

	    # get snippet-id
	    -id* )  SNIPPETID=$arg ;;

	    # get detector identifier
	    -snippetdetectorid* )  DETECTORID=$arg ;;

	    # get type/level
	    -type* )  TYPE=$arg ;;

	    # Get whether file is to be created
	    -create* )  CREATE=$arg ;;

	    * ) echo "Error : Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    else
	echo "Error : Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
    fi
done

# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} || ! ${BEAMTYPE} || ! ${RUNTYPE} ]] ; then
    echo "Error : " ${USAGE} ; echo
    exit 1
fi

if [ ! -d ${OUTPUTDIR} ] ; then 
    mkdir ${OUTPUTDIR}
fi

if [ ! -d ${OUTPUTDIR} ] ; then 
    echo "Error : Unable to create directory ${OUTPUTDIR}" ; echo
    exit 2
fi

## BUGFIX -- needed in ECS proxy -- remove later on
if [ "${BEAMTYPE}" = "PP" ] ; then
    BEAMTYPE=pp
fi

if [ "${FILETYPE}" != "allowedtypes" -a "${FILETYPE}" != "partitionbase" -a "${FILETYPE}" != "partitionbeam" -a "${FILETYPE}" != "partitionrun" -a "${FILETYPE}" != "partitionbeamrun" -a "${FILETYPE}" != "trigger" -a "${FILETYPE}" != "triggerlist" -a "${FILETYPE}" != "component" -a "${FILETYPE}" != "detectorbase" -a "${FILETYPE}" != "detectorbeam" -a "${FILETYPE}" != "detectorrun" -a "${FILETYPE}" != "detectorbeamrun" ] ; then
    echo "Error : " ${USAGE} "(Unknown specifier for -filetype): ${FILETYPE}" ; echo
    exit 1
fi

if [ "${FILETYPE}" != "allowedtypes" -a -z "${TYPE}" ] ; then
    echo "-type has to be specified (typically production, valid-test, or devel)" ; echo
    exit 1
fi

if [ "${FILETYPE}" != "allowedtypes" -a "${TYPE}" != "production" -a "${TYPE}" != "valid-test" -a "${TYPE}" != "devel" ] ; then
    echo "Warning: -type is not one of the normally used production, valid-test, or devel" ; echo
fi

if [ "${FILETYPE}" = "detectorbase" -a -z "${DETECTORID}" ] ; then
  echo "-snippetdetectorid needs to be passed for type detectorbase" ; echo
  exit 1
fi

if [ "${FILETYPE}" = "detectorbeamrun" -a -z "${DETECTORID}" ] ; then
  echo "-snippetdetectorid needs to be passed for type detectorbeamrun" ; echo
  exit 1
fi

if [ "${FILETYPE}" = "component" -a -z "${DETECTORID}" ] ; then
  echo "-snippetdetectorid needs to be passed for type component" ; echo
  exit 1
fi

if [ "${FILETYPE}" = "component" -a -z "${SNIPPETID}" ] ; then
  echo "-id needs to be passed for type component" ; echo
  exit 1
fi

if [ "${FILETYPE}" = "triggerlist" -a -z "${SNIPPETID}" ] ; then
  echo "-id needs to be passed for type triggerlist" ; echo
  exit 1
fi

if [ "${FILETYPE}" = "trigger" -a -z "${SNIPPETID}" ] ; then
  echo "-id needs to be passed for type trigger" ; echo
  exit 1
fi

if [ "${CREATE}" != "yes" -a "${CREATE}" != "no" ] ; then
  echo "-create needs 'yes' or 'no' as a parameter" ; echo
  exit 1
fi




# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------

INPUTFILENAME="${CHECKEDOUTREPOS}"
RELATIVEDIR=""

case "${FILETYPE}" in
    "allowedtypes")
      RELATIVEDIR="partitions/${DETECTOR}/allowed_types"
      ;;
    "partitionbase") 
      RELATIVEDIR="partitions/${DETECTOR}/${TYPE}/${DETECTOR}.xml"
      ;;
    "partitionbeam") 
      RELATIVEDIR="partitions/${DETECTOR}/${TYPE}/${BEAMTYPE}.xml"
      ;;
    "partitionrun") 
      RELATIVEDIR="partitions/${DETECTOR}/${TYPE}/${RUNTYPE}.xml"
      ;;
    "partitionbeamrun") 
      RELATIVEDIR="partitions/${DETECTOR}/${TYPE}/${BEAMTYPE}-${RUNTYPE}.xml"
      ;;
    "trigger")
      RELATIVEDIR="components/trigger/${TYPE}/${SNIPPETID}.xml"
      ;;
    "triggerlist")
      MENUID=`echo ${SNIPPETID}|tr abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ`
      RELATIVEDIR="triggermenus/${TYPE}/${MENUID}"
      ;;
    "component")
      RELATIVEDIR="components"
      if [ -n "${DETECTORID}" ] ; then
	RELATIVEDIR="${RELATIVEDIR}/${DETECTORID}"
      else
	RELATIVEDIR="${RELATIVEDIR}/ALICE"
      fi
      RELATIVEDIR="${RELATIVEDIR}/${TYPE}/${SNIPPETID}.xml"
      ;;
    "detectorbase")
      RELATIVEDIR="detectors/${DETECTORID}/${TYPE}/${DETECTORID}.xml"
      ;;
    "detectorbeam")
      RELATIVEDIR="detectors/${DETECTORID}/${TYPE}/${BEAMTYPE}.xml"
      ;;
    "detectorrun")
      RELATIVEDIR="detectors/${DETECTORID}/${TYPE}/${RUNTYPE}.xml"
      ;;
    "detectorbeamrun")
      RELATIVEDIR="detectors/${DETECTORID}/${TYPE}/${BEAMTYPE}-${RUNTYPE}.xml"
      ;;
esac


INPUTFILENAME="${CHECKEDOUTREPOS}/${RELATIVEDIR}"


if [ ! -f "${INPUTFILENAME}" ] ; then
    echo "File ${INPUTFILENAME} does not exist" ; echo 
    exit 1
fi


DIFF=`svn diff "${INPUTFILENAME}"`
if [ -z "${DIFF}" ] ; then
    # No change
    exit 0
fi


echo "Do you REALLY want to revert the changes now? Enter \"(N)o\" if you don't want to commit. Default is yes."
    
read TMP
if [[ "$TMP" == "n" || "$TMP" == "no" ||"$TMP" == "No" ||"$TMP" == "N" ||"$TMP" == "NO" ||"$TMP" == "nO" ]] ; then
    echo "Please run ${DETECTORDIR}/bin/commit-config-snippet.sh to commit configuration changes into svn or ${DETECTORDIR}/bin/revert-config-snippet.sh to revert configuration changes."
    exit 5
fi

svn revert "${INPUTFILENAME}"

