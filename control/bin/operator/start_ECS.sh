#!/bin/bash
# 
# * RUN ECS proxy for HLT
#
# File   : start_ECS.sh
#
# Author : Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 07.12.2007
#
###################################################################
# Include Functions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

STANDALONE=0

# -- Check Comand Line Arguments --
# ---------------------------------
if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0 -partition=<PARTITION-NAME | default is ${DETECTOR}> [-standalone] [-debug] [--help]"
else
    USAGE="Usage : $0 -detector=<Detector-ID> -partition=<PARTITION-NAME | default is ${DETECTOR}> [-standalone] [-debug] [--help]"
fi

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

            # get detector
	    -debug* ) DEBUG=--debug ;;

	    # get partition name
	    -partition* ) PARTITION=$arg ;;

	    # get standalone flag
	    -standalone ) STANDALONE=1 ;;

	    * ) echo "Error Argument not known: $cmd"; echo ${USAGE};  exit 1;;
	esac		

    else

	echo "Error Argument not known: $cmd"; 
	echo ${USAGE} ; 	
	exit 1
	
    fi
done

# -- Check usage --
# -----------------
if [ ! ${DETECTOR} ] ; then
    echo "Error: " ${USAGE}
    exit 1
fi

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------

if [ "${DETECTOR}" = "DIMUON" ] ; then
    DETECTOR=MUON_TRK
fi

if [ ! ${PARTITION} ] ; then
    PARTITION=${DETECTOR}
fi

# -- Stop old ECS processes --
# ----------------------------
KILL_ECS_PROCESSES () { 
    
    PID_DNS=`ps ux | grep ./dim/linux/dns | grep -v grep | awk -F' ' '{ print $2 }' `
    
    if [ -n "${PID_DNS}" ] ; then
	kill -9 ${PID_DNS} 
    fi

    PID_LOGIC=`ps ux | grep ./start-logic-engine.sh | grep -v grep | awk -F' ' '{ print $2 }'`
    if [ -n "${PID_LOGIC}" ] ; then
	kill -a -s 15 ${PID_LOGIC} 
    fi
}

KILL_ECS_PROCESSES

# -- Start ECS --
# ---------------

pushd ${HLTECS_TOPDIR} > /dev/null

if [ "${STANDALONE}" == "0" ] ; then 

    # -- Start only ECS PROXY --
    # --------------------------

    ./start-hlt-proxy.sh --domain ${PARTITION}_HLT --dimDnsNode ${DIM_DNS_NODE} --propertyFile ${DETECTORDIR}/setup/ecsproperties.txt --logFile /tmp/ecsproxy_${DETECTOR}.log ${DEBUG} 
    
elif [ "${STANDALONE}" == "1" ] ; then 

    # -- Start standalone ECS --
    # --------------------------
    
    # catch interupt signals and jump to KILL_ECS_PROCESSES
    # catch SIGINT, SIGQUIT and SIGTERM
    trap 'KILL_ECS_PROCESSES' 2 3 15  

    # -- Setup DNS HOST
    . ./setupECS-Standalone.sh

    # -- Start DIM
    ./dim/linux/dns &

    # -- Start logic engine 
    ./start-logic-engine.sh ${PARTITION}_HLT &

    # -- Start ECS proxy
    ./start-hlt-proxy.sh --domain ${PARTITION}_HLT --dimDnsNode ${DIM_DNS_NODE} --propertyFile ${DETECTORDIR}/setup/ecsproperties.txt --logFile /tmp/ecsproxy_standalone_${DETECTOR}.log ${DEBUG} 

fi

popd > /dev/null
    

