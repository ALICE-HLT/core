#!/bin/bash
# 
# * Checkout node and ddl list files for HLT from svn repository
#
# File   : edit_setup_file.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.08.2007
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

# -- DEFAULTS --
# --------------

# -- Check Comand Line Arguments --
# ---------------------------------
if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0 [--help]"
    SPECIALCMDUSAGE="Usage : $0 [--help]"
else
    USAGE="Usage : $0 -detector=<Detector-ID> [--help]"
    SPECIALCMDUSAGE="Usage : $0 -detector=<Detector-ID> [--help]"
fi


for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    * ) echo "Error : Argument not known: $cmd"; if [[ "${SPECIALCMD}" = "0" ]] ; then echo ${USAGE}; else echo ${SPECIALCMDUSAGE} ; fi ; exit 1;;
	esac		

    else
	echo "Error : Argument not known: $cmd"
	if [[ "${SPECIALCMD}" = "0" ]] ; then echo ${USAGE}; else echo ${SPECIALCMDUSAGE} ; fi
	exit 1
    fi
done



# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} ]] ; then
    if [[ "${SPECIALCMD}" = "0" ]] ; then echo "Error : " ${USAGE}; else echo "Error : " ${SPECIALCMDUSAGE} ; fi
    exit 1
fi

# -- Check if file in svn --
# --------------------------
    LIST=`svn list ${REPOS}/${REPOSDIR}/|grep -- "${DETECTOR}/"`
    if [ -z "${LIST}" ] ; then
	echo "Detector directory ${REPOSDIR}/${DETECTOR}/ does not exist in SVN"
	exit 1
    fi

    LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/|grep -- "setup/"`
    if [ -z "${LIST}" ] ; then
	echo "Setup directory ${REPOSDIR}/${DETECTOR}/setup does not exist in SVN"
	exit 1
    fi

    LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/setup|grep -- "nodelist.xml"`
    if [ -z "${LIST}" ] ; then
	echo "Configuration file ${REPOSDIR}/${DETECTOR}/setup/nodelist.xml does not exist in SVN"
	exit 1
    fi


# -- Create Output Dir --
# -----------------------
if ! OUTPUTDIR=`mktemp -dt -p . \`basename $0 .sh\`.tmpdir.\`date +%Y%m%d.%H%M%S\`.XXXXXX` ; then
  echo "Error : Cannot make temporary directory"
  exit 1
fi

# -- Checkout
# -------------------------------

svn export ${REPOS}/${REPOSDIR}/${DETECTOR}/setup/nodelist.xml ${OUTPUTDIR}/nodelist.xml 2>&1 |grep -Eiv "^(A  |Export complete)" | grep -v "Export complete."


# -- Rename checked out nodelist
mv ${OUTPUTDIR}/nodelist.xml ${OUTPUTDIR}/nodelist-svn.xml
FILES="${OUTPUTDIR}/nodelist-svn.xml"

# -- Copy current node list if existing
if [ -e ~/control/run/setup/nodelist.xml ] ; then cp ~/control/run/setup/nodelist.xml ${OUTPUTDIR}/nodelist-config.xml ; FILES="${FILES} ${OUTPUTDIR}/nodelist-config.xml"  ; fi


NODES=`${READ_NODELIST_BIN} ${FILES}| sort|sort -u`

#for n in ${NODES} ; do 
#  echo -n $n:
#  if ping -c 1 -w 1 $n >/dev/null 2>/dev/null ; then
#    ssh -X $n echo kill -9 -1
#    echo Done
#  else
#    echo Not accessible
#  fi
#done

#echo ========================= ps x =========================
#${REMOTE_STARTER} -command "ps x" -nodes "`echo ${NODES}`"

#echo ; echo
#echo ========================= netstat -apntuv =========================
#${REMOTE_STARTER} -command "netstat -apntuv" -nodes "`echo ${NODES}`"

#echo ; echo
#echo ========================= ipcs =========================
#${REMOTE_STARTER} -command "ipcs" -nodes "`echo ${NODES}`"

#echo ; echo
#echo ========================= ps ax =========================
#${REMOTE_STARTER} -command "ps ax" -nodes "`echo ${NODES}`"

${REMOTE_STARTER_BIN} -command "echo ===================== \; hostname \; echo \; ps x \; echo \; echo \; netstat -apntuv \; echo \; echo \; ipcs \; echo \; echo \; ps ax \; echo \; echo \; echo" -nodes "`echo ${NODES}`"



rm -rf ${OUTPUTDIR}