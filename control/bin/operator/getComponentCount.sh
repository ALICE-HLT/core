#!/bin/bash
# 
#
# Author : Timo Breitner  tbreitne _at_ kip.uni-heidelberg.de
# Date   : 12.05.2010
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################



# -- Check Comand Line Arguments --
# ---------------------------------
if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0"
else
    echo "Error: This command has to be started as operator."
    exit 1
fi


# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------

if [[ ! -f "$SCC1_CONFIGFILE" ]]
then
	echo "no configuration found, exiting."
	exit 0
fi

grep "Proc ID" $SCC1_CONFIGFILE | sed -e 's/.*ID=\"//' | sed -e 's/\".*//' | sed -e 's/[0-9]//g' |  sort | uniq -c

