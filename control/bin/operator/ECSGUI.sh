#!/bin/bash
# 
# * RUN ECS GUI for HLT
#
# File   : ECSGUI.sh
#
# Author : Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 07.12.2007
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

STANDALONE=0

# -- Check Comand Line Arguments --
# ---------------------------------
if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0 -partition=<PARTITION-NAME | default is ${DETECTOR}> [-standalone] [--help]"
else
    USAGE="Usage : $0 -detector=<Detector-ID> -partition=<PARTITION-NAME | default is ${DETECTOR}> [-standalone] [--help]"
fi

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

           # get partition name
	    -partition* ) PARTITION=$arg ;;
	    
	    # get standalone flag : Start
	    -standalone ) STANDALONE=1 ;;

	    * ) echo "Error Argument not known: $cmd"; echo ${USAGE};  exit 1;;
	esac		

    else
	echo "Error Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
    fi
done

# -- Check usage --
# -----------------
if [ ! ${DETECTOR} ] ; then
    echo "Error: " ${USAGE}
    exit 1
fi

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------

if [ "${DETECTOR}" = "DIMUON" ] ; then
    DETECTOR=MUON_TRK
fi

if [ ! ${PARTITION} ] ; then
    PARTITION=${DETECTOR}
fi

# -- Start ECS GUI --
# ---------------------
pushd ${HLTECS_TOPDIR} > /dev/null

if [ "${STANDALONE}" == "1" ] ; then 
    . ./setupECS-Standalone.sh
fi

./start-test-gui.sh ${PARTITION}_HLT

popd > /dev/null