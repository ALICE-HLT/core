#!/bin/bash
# 
# * Get CPU usage of all nodes invovled in a run
#
# File   : getCpuUsage.sh
#
# Author : Timo Breitner  tbreitne _at_ kip.uni-heidelberg.de
# Date   : 12.05.2010
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################



# -- Check Comand Line Arguments --
# ---------------------------------
if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0 [measurement interval]"
else
    echo "Error: This command has to be started as operator."
    exit 1
fi

INTERVALL=1

if [[ -n "$1" ]]
then
	INTERVALL="$1"
fi

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------

if [[ ! -f "$SCC1_CONFIGFILE" ]]
then
	echo "no run ongoing, exiting."
	exit 0
fi

HOSTS="$(echo  $( cat $SCC1_CONFIGFILE | grep 'Node ID="' | sed -e 's/<Node ID="//' | sed -e 's/".*//'))"

if [[ -z "$HOSTS" ]]
then
	echo "no hosts configured, exiting."
	exit 0
fi

REMOTESTARTER="$ALIHLT_DC_DIR/src/SimpleChainConfig2/scripts/remote_starter.py"

if [[ ! -x "$REMOTESTARTER" ]]
then
	echo "$REMOTESTARTER not found, exiting"
	exit 1
fi

GETCPUUSAGE="$HLTCONTROL_TOPDIR/bin/operator/getLocalCpuUsage.sh"
if [[ ! -x "$GETCPUUSAGE" ]]
then
	echo "cannot execute $GETCPUUSAGE, exiting"
	exit 1
fi

TMPFILE="/tmp/cpuusage_${USER}_$(date +%s)"
touch $TMPFILE || exit 1

GETOUT() {
  exit 0
}

trap GETOUT 2 3 15

while true; do
	$REMOTESTARTER -maxthreads 100 -quiet -command "$GETCPUUSAGE $INTERVAL" -nodes "$HOSTS" | sort -n -k3 -r | head -n 50 > $TMPFILE
	clear
	echo "hostname         Idle   maxCpu procName    PID"
	cat $TMPFILE
done


