#!/bin/bash
# 
# * set Verbosity for HLT chain
#
# File   : set-Verbosity.sh
#
# Author : Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.05.2008
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

# -- Check Comand Line Arguments --
# ---------------------------------
if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0 -verbosity=<Hex-Verbosity> [--help]"
else
    USAGE="Usage : $0 -detector=<Detector-ID> -verbosity=<Hex-Verbosity> [--help]"
fi

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get verbosity
	    -verbosity* ) VERBOSITY=$arg ;;    

	    * ) echo "Error Argument not known: $cmd"; echo ${USAGE};  exit 1;;
	esac		

    else

	echo "Error Argument not known: $cmd"; 
	echo ${USAGE} ; 	
	exit 1
	
    fi
done

# -- Check usage --
# -----------------
if [ ! ${DETECTOR} ] ; then
    echo "Error: " ${USAGE}
    exit 1
fi

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------

# -- Set Verbosity --
# -------------------
pushd ${HLTECS_TOPDIR} > /dev/null

echo "SET VERBOSITY to ${VERBOSITY}"
echo "not implemented ... yet"
popd > /dev/null


