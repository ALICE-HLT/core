#!/bin/bash
# 
# * Control TAXI
#
# File   : start_TAXI.sh
#
# Author : Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 24.10.2009
#
###################################################################
# Include Functions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

# -- Check Comand Line Arguments --
# ---------------------------------
if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0 [--help]"
else
    USAGE="Usage : $0 -detector=<Detector-ID> [--help]"
fi

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    * ) echo "Error : Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		
    fi
done

# -- Check usage --
# -----------------
if [ ! ${DETECTOR} ] ; then
    echo -e "Error: " ${USAGE}
    exit 1
fi

# -- Check if hlt-operator --
# ---------------------------
if [ "${DETECTOR}" != "ALICE" ] ; then
    echo "Error : Operator is not hlt-operator"; echo ${USAGE}
    exit 2
fi

# -- Check if node is vobox0 or vobox1 --
# ---------------------------------------
HOSTNAME=`hostname -s`

if [[ "${HOSTNAME}" != "${TAXINODE_0}" && "${HOSTNAME}" != "${TAXINODE_1}" ]] ; then
    echo "Error : This script must be started on host ${TAXINODE_0} or ${TAXINODE_1}" 
    exit 3
fi

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS NO_MASTER_CHECK
# --------------------------------

PROCESS=`ps ux | grep TaxiDriver.sh | grep -v grep`
RUN_PROCESS=$?

if [ "${RUN_PROCESS}" = "0" ] ; then
    echo "TAXI is already running"  
else
    # Start TAXI
    ${TAXI_DRIVER_BIN} roehrich /alice/data/2009/OCDB /opt/T-HCDB /opt/T-HCDB/lists/lists-taxi ${HOME}/.passphrase.txt alien 30
fi
