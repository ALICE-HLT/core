#!/bin/bash
# 
# * Run Componentstatus Scan 
#
# File   : StatusDump.sh
#
# Author : Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de and Timm Steinbeck timm.steinbeck _at_ kip.uni-heidelberg.de
# Date   : 25.05.2008
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

# -- Check Comand Line Arguments --
# ---------------------------------
if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0 [-master-node=<Master-Node>] [--help]"
else
    USAGE="Usage : $0 -detector=<Detector-ID> [-master-node=<Master-Node>] [--help]"
fi

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get master node
	    -master-node* ) MASTERNODE=$arg ;;    

	    * ) echo "Error Argument not known: $cmd"; echo ${USAGE};  exit 1;;
	esac		

    else
	echo "Error Argument not known: $cmd"
	echo ${USAGE} 
	exit 1

    fi
done

# -- Check usage --
# -----------------
if [ ! ${DETECTOR} ] ; then
    echo "Error: " ${USAGE}
    exit 1
fi

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS # "NO_MASTER_CHECK" 
# --------------------------------

# -- Get Free PORT --
# --------------------
let "TOPPORT = $ALIHLT_PORT_BASE_RUNMGR + $ALIHLT_PORTRANGE_DETECTOR"

for ((offset=1 ; offset<99 ; offset=offset+1)) ; do
    let "PORT = $TOPPORT -$offset"
    RESVAL=`netstat -aptunv 2>&1 | grep $PORT`
    if [ ! "${RESVAL}" ] ; then
	TMPORT=$PORT
	break
    fi
done

if [ ! $TMPORT ] ; then
    echo "No free Port availible !!"
    exit 6
fi

echo "Running StatusScan on host: `hostname -s`  port: ${TMPORT} ."
ComponentStatusScan.py -address tcpmsg://:${TMPORT} -control tcpmsg://${MASTERNODE}:${ALIHLT_PORT_BASE_TASKMGR} -dump
 

