#!/bin/bash
# 
# * Common functions for the Run Environment
#
# File   : functions.sh
#
# Author : Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.05.2008
#
###################################################################

###################################################################
CHECK_MODULES () {

    # -- CHECK if module HLT --
    # -------------------------
    if [ ! -f /etc/profile.modules ] ; then
	echo "Error :  No Modules installed"    
	exit 4
    fi
    
    MODULE_HLT=`. /etc/profile.modules; module -t list 2>&1 | grep HLT/ `
    
    if [ ! ${MODULE_HLT} ] ; then
	echo "Error : No module HLT has been loaded !"
	exit 4
    fi
}

###################################################################
CHECK_PRODUCTION () {
    # -- Get Production Switch --
    # ---------------------------
    if [ "${HLTBUILDTYPE}" = "prod" ] ; then
	PRODUCTION="-production 1"
    fi
}

###################################################################
CHECK_MASTERNODE () {
    # -- Get MasterNode --
    # --------------------
    MASTERNODE=`hostname -s`
    
    if [ "${DETECTOR}" == "DEV" ] ; then
	if [[ "${MASTERNODE}" != "${DEVNODE_0}" && "${MASTERNODE}" != "${DEVNODE_1}" ]] ; then 
	    echo "Error : This script must be started on host ${DEVNODE_0} ( = ${DEVNODE_1} )"
	    exit 5  
	fi
    elif [ "${DETECTOR}" == "TEST" ] ; then
	if [[ "${MASTERNODE}" != "${ECSNODE_0}" && "${MASTERNODE}" != "${ECSNODE_1}" ]] ; then
	    echo "Error : This script must be started on host ${ECSNODE_0} or ${ECSNODE_1}" 
	    exit 5  
	fi
    else
	if [[ "${MASTERNODE}" != "${ECSNODE_0}" && "${MASTERNODE}" != "${ECSNODE_1}" ]] ; then
	    echo "Error : This script must be started on host ${ECSNODE_0} or ${ECSNODE_1}" 
	    exit 5  
	fi
    fi
}

###################################################################
CHECK_CONFIGTYPE () {

    TESTTYPE=0
    for config in ${SUPPORTED_CONFIGTYPES} ; do 

	if [ "$CONFIGTYPE" == "${config}" ] ; then
	    TESTTYPE=1
	    break;
	fi
    done

    if [ "${TESTTYPE}" == "0" ] ; then
	echo "Error : CONFIGTYPE ${CONFIGTYPE} unknown."
	exit 2
    fi
}

###################################################################
CHECK_DETECTOR () {

    HLTCONFIGDIR=${HLTCONTROL_DIR}/hlt-configuration
    HLTCONFIGPARTITIONDIR=${HLTCONFIGDIR}/partitions/${DETECTOR}
    HLTCONFIGPARTITIONDEVELDIR=${HLTCONFIGPARTITIONDIR}/devel

    DETECTORDIR=${HLTCONTROL_DIR}/detector/${DETECTOR}
    #deprcated
    #DETECTORCONFIGDIR=${DETECTORDIR}/config/${CONFIGTYPE}

    DETECTORRUNDIR=${HLTCONTROL_DIR}/run/${DETECTOR} 
    DETECTORRUNCONFIGDIR=${DETECTORRUNDIR}/config
    DETECTORRUNCONFIGSRCDIR=${DETECTORRUNCONFIGDIR}/config-src
    DETECTORRUNCONFIGCREATEDDIR=${DETECTORRUNCONFIGDIR}/config-created

    VERIFY_DETECTORCONFIGDIR=${DETECTORDIR}/run/.verify
    VERIFY_DETECTORRUNDIR=${VERIFY_DETECTORCONFIGDIR}

    if [ ! -d ${HLTCONFIGDIR} ] ; then
	echo "Error : AutoConfiguration directory  ${HLTCONFIGDIR} does not exist."
	exit 1
    fi

    if [ ! -d ${HLTCONFIGPARTITIONDIR} ] ; then
	echo "Error : AutoConfiguration partition directory for detector ${DETECTOR} does not exist."
	exit 1
    fi

    if [ ! -d ${HLTCONFIGPARTITIONDEVELDIR} ] ; then
	echo "Error : Devel autoConfiguration partition directory for detector ${DETECTOR} does not exist."
	exit 1
    fi
    
    if [ ! -d ${DETECTORDIR} ] ; then
	echo "Error : Detector ${DETECTORDIR} does not exist."
	exit 1
    fi
    #deprecated
#    if [ ! -d ${DETECTORCONFIGDIR} ] ; then
#	echo "Error : Config directory for Detector ${DETECTOR} does not exist."
#	exit 1
#    fi

    if [ ! -d ${DETECTORRUNDIR} ] ; then
	echo "Error: Run directory ${DETECTORRUNDIR} does not exist."
	exit 1
    fi

    if [ ! -d ${DETECTORRUNCONFIGDIR} ] ; then
	echo "Error: Run config directory ${DETECTORRUNCONFIGDIR} does not exist."
	exit 1
    fi

    if [ ! -d ${VERFIY_DETECTORCONFIGDIR} ] ; then
	echo "Error : Verify directory for Detector ${DETECTOR} does not exist."
	exit 1
    fi
}

###################################################################
CHECK_RUNMANAGER () {

    RUNMANAGERCONFIGDIR=${HLTCONTROL_TOPDIR}/configs/${DETECTOR}
    RUNMANAGERCONFIGFILE=${RUNMANAGERCONFIGDIR}/settings

    RUNMANAGERFILE=${DETECTORRUNDIR}/RunManager.xml

    # -- Check if RunManager configuration exists --
    # ----------------------------------------------
    if [[ ! -d ${RUNMANAGERCONFIGDIR} || ! -f  ${RUNMANAGERCONFIGFILE} ]] ; then
	echo RunManager Configuration for Detector ${DETECTOR} does not exist.
	exit 3
    fi

}

###################################################################
###################################################################
###################################################################
SET_COMMON_VARS () {

    CHECK_CONFIGTYPE
    CHECK_DETECTOR
    CHECK_RUNMANAGER

    CONFIGDIR=${DETECTORCONFIGDIR}/${CONFIG}
    CONFIGFILE=${CONFIGDIR}/${CONFIG}.xml
    RUNCONFIGFILE=${DETECTORRUNCONFIGCREATEDDIR}/${CONFIG}.xml

    SCC1_CONFIGFILE=${DETECTORRUNCONFIGCREATEDDIR}/${DETECTOR}-SCC1-Generate.xml

    CHECK_MODULES
    CHECK_PRODUCTION

    if [ "$1" != "NO_MASTER_CHECK" ] ; then 
	CHECK_MASTERNODE
    fi

}

###################################################################
CHECK_CONFIG () {

    if [ ! -d ${CONFIGDIR} ] ; then
	echo "Error : Configuration directory ${CONFIGDIR} does not exist."
	exit 2
    fi
    
    if [ ! -f ${CONFIGFILE} ] ; then
	echo "Error : Configuration xml-file ${CONFIGFILE} does not exist."
	exit 2
    fi

    if [ ! -f ${SCC1_CONFIGFILE} ] ; then
	foo=foo
	## taken out because it creates trouble the first time, when run folder is empty
	# echo "Error : Configuration xml-file ${SCC1_CONFIGFILE} does not exist."
	# exit 2
    else
	CONFIGID=`grep \<SimpleChainConfig1 ${SCC1_CONFIGFILE} | awk -F\" '{print $2} '`

	if [ "$CONFIGID" != "$DETECTOR" ] ; then
	    echo "Error : ConfigID : ${CONFIGID} does not fit to ${DETECTOR}."
	    exit 3
	fi
    fi
    
}

###################################################################
