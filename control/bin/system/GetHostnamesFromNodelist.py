#!/usr/bin/env python

#!/usr/bin/env python

import libxml2, sys

if len(sys.argv)!=2:
    print "Usage: "+sys.argv[0]+" <XML nodelist filename>"
    sys.exit(1)

xmlFile = libxml2.parseFile(sys.argv[1])
xPathContext = xmlFile.xpathNewContext()
if len(xPathContext.xpathEval("/SimpleChainConfig2NodeList"))!=1:
    raise NodeListException( "Exactly one top level SimpleChainConfig2NodeList node allowed and required in XML nodelist file" )
nodes = xPathContext.xpathEval("/SimpleChainConfig2NodeList/Node")
for node in nodes:
    id=node.prop("ID")
    if id==None:
        raise NodeListException( "ID attribute required for Node XML node" )
    if len(node.xpathEval("./Hostname"))>1:
        raise NodeListException( "At most one Hostname node required for Node "+id )
    if len(node.xpathEval("./Hostname"))==1:
        hostname = node.xpathEval("./Hostname")[0].content
    else:
        hostname = id
    print hostname

