#!/bin/bash
# 
# * COMMIT configuration Files for HLT
#
# File   : commit-config-changes.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.05.2008
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

# -- Check Comand Line Arguments --
# ---------------------------------
if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0 -config=<Config-ID> -type=<Config-Type:default=production> [-guiusage -message=<svn ci message>] [--help]"
else
    USAGE="Usage : $0 -detector=<Detector-ID> -config=<Config-ID> -type=<Config-Type:default=production> [-guiusage -message=<svn ci message>] [--help]"
fi

for cmd in "$@" ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get configuration
	    -config* )  CONFIG=$arg ;;

	    # get flag if used by GUI
	    -guiusage )  GUIUSAGE=1 ;;

	    # get commit message
	    -message* ) COMMIT_MESSAGE=$arg ;;

	    # get config type
	    -type*)     CONFIGTYPE=$arg ;;

	    * ) echo "Error Argument not known: $cmd"; echo ${USAGE};  exit 1;;
	esac		

    else
	echo "Error Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
    fi

done

# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} || ! ${CONFIG} ]] ; then
    echo "Error: " ${USAGE}
    exit 1
fi

# -- Set/Check common Variables --
# --------------------------------
CHECK_CONFIGTYPE
CHECK_DETECTOR

CONFIGDIR=${DETECTORCONFIGDIR}/${CONFIG}
CONFIGFILE=${CONFIGDIR}/${CONFIG}.xml
# --------------------------------


# -- Check if config exits --
# ---------------------------
if [ ! -d ${CONFIGDIR} ] ; then
    echo "Error : Configuration directory ${CONFIGDIR} does not exist."
    exit 2
fi

if [ ! -f ${CONFIGFILE} ] ; then
    echo "Error : Configuration xml-file ${CONFIGFILE} does not exist."
    exit 2
fi


if [ "$GUIUSAGE" != 1 ] ; then
    echo "-===========================================================-"
    echo "-= DETECTOR : $d"
    echo "-===========================================================-"
fi

# -- Add CONFIG --
# ----------------
if [ ! -d ${CONFIGDIR}/.svn ] ; then
    echo "Found new configuration ${CONFIG} (${CONFIGFILE}). Adding now..."
    svn add -N ${CONFIGDIR} ${CONFIGFILE}
else
    FOUND=`svn status ${CONFIGDIR} | grep "?.*${c}.xml$"`
    if [ -n "${FOUND}" ] ; then
	echo "Found new configuration ${CONFIGFILE}. Adding now..."
	svn add ${CONFIGFILE}
    fi
fi

# -- Commit CONFIG --
# -------------------
pushd ${CONFIGDIR} >/dev/null

DIFF=`svn diff`

if [ -n "${DIFF}" ] ; then
    
    if [ "$GUIUSAGE" == 1 ] ; then
	svn ci -m "$COMMIT_MESSAGE"
    else
	( svn status ${CONFIGFILE}; svn diff ${CONFIGFILE}) | less -i | grep -v ? | grep -v !
	
	echo "Please press return to continue and enter commit message"
	read TMP
	
	svn ci
    fi
fi

popd >/dev/null


exit 0