#!/bin/bash
# 
# * Test configuration Files for HLT
#
# File   : verify-config-test.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.05.2008
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

# -- Default --
# -------------
CLEANUP=0

# -- Check Comand Line Arguments --
# ---------------------------------
USAGE="Usage : $0 -detector=<Detector-ID> -config=<Config-ID> -type=<Config-Type:default=production> [-cleanup] [--help]"

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get configuration
	    -config* )  CONFIG=$arg ;;

	    # get config type
	    -type* )    CONFIGTYPE=$arg ;;

	    # get cleanup for SHM regions
	    -cleanup )  CLEANUP=1 ;;

	    * ) echo "Error Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    else
	echo "Error Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
    fi
done

# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} || ! ${CONFIG} ]] ; then
    echo "Error: " ${USAGE}
    exit 1
fi

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------

# -- Get Nodes --
# ---------------
NODES=`grep hostname ${VERIFY_DETECTORCONFIGDIR}/${DETECTOR}-SCC1-Generate.xml | grep -v master | awk -F\" '{print $4} ' `

# -- Prepare Nodes --
# -------------------
if [ "${CLEANUP}" = "1" ] ; then 
    for node in ${NODES} ; do
	if [ "${node}" == "${MASTERNODE}" ] ; then 
	    continue
	fi
	
	echo Preparing ${node}
	ssh ${node} /opt/HLT/tools/bin/CloseSysVregions.sh  
	
	if [[ "$node" = "portal-dcs0" || "$node" = "portal-dcs1" || "$node" = "portal-vobox0" || "$node" = "portal-vobox1" ]] ; then 
	    continue
	fi

	ssh ${node} /usr/local/bin/psi_cleanup -all

    done
fi

TESTBINARY=${VERIFY_DETECTORCONFIGDIR}/${DETECTOR}-${MASTERNODE}-${MASTERNODE}-Master-Check.sh

if [ \( ! -x ${TESTBINARY} \) -o \( ! -f ${TESTBINARY} \) ] ; then
    echo "Error : Cannot execute test start script ${TESTBINARY}"
    exit 4
fi

pushd ${VERIFY_DETECTORCONFIGDIR} >/dev/null

${TESTBINARY}

popd >/dev/null

if [ $? -gt 0 ] ; then
    echo "Error : There have been errors during check of configuration ${CONIFG}!"
    exit 5
fi 
