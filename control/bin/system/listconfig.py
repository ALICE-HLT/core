#!/usr/bin/python
#
# Improved list config script, invoked by blabla
#
# Author: Gerd Troeger
# Created: 2008-07-22
#
# Modified: Gerd Troeger, 2008-08-06
#   changed to accomodate the new configuration directory hierarchy, with the
#   separate subdirectories for production, devel (development) and valid-test
#   (validation-testing) configurations
#   the type is output as <configtype>xxx</configtype> for each config entry
#

import os, sys

# print header
print '<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?>'
print '<configlist>'

# fetch detector subdirectories
## TODO ... change to ${HLTCONTROL_DIT}/detector

DET_DIR = '/opt/HLT/control/detector'
try:	dlist = os.listdir(DET_DIR)
except:	dlist = []

for detector in dlist:
	# fetch all configuration type subdirectories
	TYPE_DIR = os.path.join(DET_DIR, detector, 'config')
	try:	tlist = os.listdir(TYPE_DIR)
	except:	continue

	for configtype in tlist:
		# fetch all configuration (directory) names
	        CONF_DIR = os.path.join(DET_DIR, detector, 'config', configtype)
		try:	clist = os.listdir(CONF_DIR)
		except:	continue

		for name in clist:
			# try opening+reading the XML configuration with that name
			try:	infile = open(os.path.join(CONF_DIR, name, name+'.xml'), 'r')
			except:	continue
			indata = infile.read().splitlines()
			infile.close()

			# print the configuration record
			print '<config>'
			print '<configname>' + name + '</configname>'
			print '<detector>' + detector + '</detector>'
			print '<configtype>' + configtype + '</configtype>'
			for line in indata:
				if line.find('<author>') >= 0: print line
				if line.find('<description>') >= 0:
					if line.find('</description>') < 0: print line + '... </description>'
					else: print line
				if line.find('<date>') >= 0: print line
			print '</config>'

# print footer
print '</configlist>'
