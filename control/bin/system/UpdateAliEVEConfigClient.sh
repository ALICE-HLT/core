#!/bin/bash
# 
# * RUN ECS GUI for HLT
#
# File   : UpdateAliEVEConfigClient.sh
#
# Author : Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 21.02.2008
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

# -- Check Comand Line Arguments --
# ---------------------------------
if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0 [--help]"
else
    USAGE="Usage : $0 -detector=<Detector-ID> [--help]"
fi

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    * ) echo "Error Argument not known: $cmd"; echo ${USAGE};  exit 1;;
	esac		

    else
	echo "Error Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
    fi
done

# -- Check usage --
# -----------------
if [ ! ${DETECTOR} ] ; then
    echo "Error: " ${USAGE}
    exit 1
fi
# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS NO_MASTER_CHECK
# --------------------------------

# -- Update AliEVE Config --
# --------------------------
EVECONFIG=${DETECTORRUNDIR}/config/${DETECTOR}-SCC1-Generate.xml

if [ -f ${EVECONFIG} ] ; then
    scp  ${EVECONFIG} hlt@aldaqacrs1.cern.ch:AliEVE-Config.xml
    scp  ${EVECONFIG} dev0.internal:/tmp/AliEVE-Config.xml
else
    echo "AliEVE config : ${EVECONFIG} does not exist for detector ${DETECTOR}." 
    echo "-> The reason could be the HLT is not in a state higher than \"CONFIGURED\"!" 
fi
