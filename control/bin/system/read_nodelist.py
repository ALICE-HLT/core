#!/usr/bin/env python

#!/usr/bin/env python

import libxml2, sys

if len(sys.argv)<2:
    print "Usage: "+sys.argv[0]+" <xml-nodelist-filename>+"
    sys.exit(1)


for filename in sys.argv[1:]:
    xmlFile = libxml2.parseFile(filename)
    xPathContext = xmlFile.xpathNewContext()
    if len(xPathContext.xpathEval("/SimpleChainConfig2NodeList"))!=1:
        print filename+": Wrong type of file, no XML nodelist..."
        continue

    nodes = xPathContext.xpathEval("/SimpleChainConfig2NodeList/Node")
    for node in nodes:
        id=node.prop("ID")
        if id==None:
            continue
        print id


