#!/bin/bash
# 
# * Script which should be executed after end of TM
# ** Clears SHM regions
# ** kills user on this node
# ** does nothing on portal-ecsX nodes
#
# File   : post_exec_cmd.sh
#
# Author : Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 01.03.2008
#
###################################################################

HOSTNAME=`hostname`

if [[ "$HOSTNAME" != "portal-ecs0" && "$HOSTNAME" != "portal-ecs1" ]] ; then

    # -- Clear SHM regions --
    # -----------------------
    if [[ -x /usr/local/bin/psi_cleanup && -e /dev/psi ]] ; then 
	/usr/local/bin/psi_cleanup -all
    fi
    
    if [ -x /opt/HLT/tools/bin/CloseSysVregions.sh ] ; then
	/opt/HLT/tools/bin/CloseSysVregions.sh   
    fi
    

    # -- Kills user --
    # ----------------
    
#    kill -9 -1 > /dev/null 2>/dev/null

fi