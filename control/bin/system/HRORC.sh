#!/bin/bash
# 
# * Load and Start event Replay in HRORCs according to nodeList
#   According to LinkName of script  
#
#   Scripts have to be run in "configured state"
#
# File   :  HRORC.sh ( HRORCLoad.sh || HRORCStart.sh )
#
# Author : Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.05.2008
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

# -- Get mode --
# --------------
for f in ${SUPPORTED_HRORC_FILES} ; do
    if [[ `basename $0 .sh` = "HRORC${f}" ]] ; then
        MODE=${f}
    fi
done

# -- Check Comand Line Arguments --
# ---------------------------------
if [ "$MODE" == "Load" ] ; then
    if [ ${DETECTOR_CONTROL} ] ; then
	DETECTOR=${DETECTOR_CONTROL}
	USAGE="Usage : $0 (-directory=<DataDirectory>|-d=<DataDirectory>) [--help]"
    else
	USAGE="Usage : $0 -detector=<Detector-ID> (-directory=<DataDirectory>|-d=<DataDirectory>) [--help]"
    fi
elif [ "$MODE" == "Burst" ] ; then
    if [ ${DETECTOR_CONTROL} ] ; then
	DETECTOR=${DETECTOR_CONTROL}
	USAGE="Usage : $0 (-numberofevents=<numberOfEvents>|-j <numberOfEvents>) (-numberofbursts=<numberOfBursts>|-k <numberOfBursts>) (-burstwait=<WaitTimeBetweenBursts>|-w <WaitTimeBetweenBursts>) [--help]"
    else
	USAGE="Usage : $0 -detector=<Detector-ID> (-numberofevents=<numberOfEvents>|-j <numberOfEvents>) (-numberofbursts=<numberOfBursts>|-k <numberOfBursts>) (-burstwait=<WaitTimeBetweenBursts>|-w <WaitTimeBetweenBursts>) [--help]"
    fi
elif [ "$MODE" == "Start" ] ; then
    if [ ${DETECTOR_CONTROL} ] ; then
	DETECTOR=${DETECTOR_CONTROL}
	USAGE="Usage : $0 (-startmode=<one|single|loop|singleBurst|continousBurst>|-s=<one|single|loop|singleBurst|continousBurst>) (-w <waittime in 25 ns clock cycles>) [--help]"
    else
	USAGE="Usage : $0 -detector=<Detector-ID> (-startmode=<one|single|loop|singleBurst|continousBurst>|-s=<one|single|loop|singleBurst|continousBurst>) (-w <waittime in us>) [--help]"
    fi
else
    if [ ${DETECTOR_CONTROL} ] ; then
	DETECTOR=${DETECTOR_CONTROL}
	USAGE="Usage : $0 [--help]"
    else
	USAGE="Usage : $0 -detector=<Detector-ID> [--help]"
    fi
fi  

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;

	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

            # directory
	    -d* | -directory* ) DATADIR=$arg ;;

            # startmode
	    -s* | -startmode* ) STARTMODES="one single loop singleBurst continousBurst"

	    for f in ${STARTMODES} ; do
		if [[ "${f}" = "${arg}" ]] ; then
		    if [ "$arg" = "singleBurst" ] ; then 
			STARTMODE=ps
		    elif [ "$arg" = "continousBurst" ] ; then 
			STARTMODE=pc
		    else
			STARTMODE=$arg
		    fi
		fi
	    done
	    
	    if [ ! ${STARTMODE} ] ; then
		echo "Error start mode not known: $arg"; echo ${USAGE}; exit 1
	    fi
	    ;;

	    # number of events in burst
	    -j* | -numberofevents* ) BURST_EVENTS=$arg ;;

	    # number of bursts
	    -k* | -numberofbursts* ) BURST_NUMBER=$arg ;;

	    # time between 2 bursts
	    -w* | -burstwait*) BURST_WAIT=$arg ;;


	    * ) echo "Error Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    else
	echo "Error Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
    fi
done

# -- Check usage --
# -----------------
if [ "$MODE" == "Load" ] ; then
    if [[ ! ${DETECTOR} || ! ${DATADIR} ]] ; then
	echo "Error: " ${USAGE}
	exit 1
    fi
elif [ "$MODE" == "Burst" ] ; then
    if [[ ! ${DETECTOR} || ! ${BURST_EVENTS} || ! ${BURST_NUMBER} || ! ${BURST_WAIT} ]] ; then
	echo "Error: " ${USAGE}
	exit 1
    fi
elif [ "$MODE" == "Start" ] ; then
    if [[ ! ${DETECTOR} || ! ${STARTMODE} ]] ; then
	echo "Error: " ${USAGE}
	exit 1
    fi
else
    if [ ! ${DETECTOR} ] ; then
	echo "Error: " ${USAGE}
	exit 1
    fi
fi  

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------

# -- Get Files --
# ---------------
DDLLISTFILE=${DETECTORRUNDIR}/setup/ddllist.xml
SSC1CONFIGFILE=${SCC1_CONFIGFILE}

if [ ! -f ${DDLLISTFILE} ] ; then
    echo "Error: " ${USAGE}
    echo DDLList file ${DDLLISTFILE} does not exist
    exit 1
fi

if [ ! -f ${SSC1CONFIGFILE} ] ; then
    echo "Error: " ${USAGE}
    echo SSC1 config file ${SSC1CONFIGFILE} does not exist
    exit 1
fi

# -- Set Command --
# -----------------
if [ "$MODE" == "Start" ] ; then
    WAITTIME=$(( ${BURST_WAIT:=0} * 133 ))
    HRORCCMD="${HRORCHANDLER_BIN} --mode ${MODE} --ddllist ${DDLLISTFILE} --startmode ${STARTMODE} --waittime ${WAITTIME}"
elif [ "$MODE" == "Burst" ] ; then
    HRORCCMD="${HRORCHANDLER_BIN} --mode ${MODE} --ddllist ${DDLLISTFILE} --numberofevents=${BURST_EVENTS} --numberofbursts=${BURST_NUMBER} --burstwait=${BURST_WAIT}"
elif [ "$MODE" == "Load" ] ; then
    HRORCCMD="${HRORCHANDLER_BIN} --mode ${MODE} --ddllist ${DDLLISTFILE} --directory ${DATADIR}"
else
    HRORCCMD="${HRORCHANDLER_BIN} --mode ${MODE} --ddllist ${DDLLISTFILE}"
fi

# -- Get Nodes --
# ---------------
NODES=`grep hostname ${SSC1CONFIGFILE} | grep fep | grep -v hltout | awk -F\" '{print $4} ' | xargs `

# -- Start scripts on all nodes --
# --------------------------------

echo -e "\n\nCOMMAND: ${REMOTE_STARTER_BIN} -command '${HRORCCMD}' -nodes '${NODES}'\n\n"
eval "${REMOTE_STARTER_BIN} -command '${HRORCCMD}' -nodes '${NODES}'" 2>&1 | grep -v "closed by remote host"

echo " All nodes ${MODE}ed."

