#!/usr/bin/env python

# rsh options
# list of hostnames
# directories to be sync'ed

"""
Sample scheme for synchronization to 12 nodes

from node 00 to nodes 01-12


00 : 00->01 ; 01:02-06 , 00:07-12 ; L0

00 : 00->07 ; 07:08-09 , 00:10-12 ; L0
01 : 01->02 ; 02:03-04 , 01:05-06 ; L1

00 : 00->10 ; 10:11 , 00:12 ; L0
01 : 01->05 ;  05: , 01:06 ; L1
02 : 02->03 ; 03: , 02:04 ; L2
07 : 07->08 ; 08: 07:09 ; L1 

00 : 00->12 ; 12: , 00: ; L0
01 : 01->06 ; 06: , 01: ; L1
02 : 02->04 ; 04: , 02: ; L2
03 : 
05 : 
07 : 07->09 ; 09: , 07: ; L1
08 : 
10 : 10->11 ; 11: , 10: ; L1

01,02,03,04,05,06,07,08,09,10,11,12


N12
S1
N6
S2
N3
S3
N1
S4
N0
"""

import optparse
import threading
import sys, os, os.path, time, datetime

class remote_sync_thread(threading.Thread):
    def __init__( self, node, thisCommand, rsync_opts, paths, secondary_nodes ):
        threading.Thread.__init__(self)
        self.fNode = node
        self.fThisCommand = thisCommand
        self.fRsyncOpts = rsync_opts
        self.fPaths = paths
        self.fSecondaryNodes = secondary_nodes
        self.fOutputLines = []
        self.fPingWaitOpt = "-w" # -w for linux, -W for Mac OS X

    def GetNode(self):
        return self.fNode

    def GetOutputLines(self):
        return self.fOutputLines[:]

    def run(self):
        command = "if ping -c 1 "+self.fPingWaitOpt+" 1 "+self.fNode+" >/dev/null 2>/dev/null ; then ssh -o ConnectTimeout=3 "+self.fNode+" "+self.fThisCommand+" --rsync-options \\\""+self.fRsyncOpts+"\\\" "+(" ".join( [ "--node "+node for node in self.fSecondaryNodes ]))+" "+(" ".join( [ "--path "+path for path in self.fPaths ]))+" \\; echo $? 2>&1; else echo Not reachable ; echo 1 ; fi"
        #command = "echo '"+command+"'"
        # print "Remote command:",command
        inp = os.popen( command, "r" )
        
        line=inp.readline()
        lastline=None
        while line !="":
            #print "remote line:",line
            if lastline!=None:
                self.fOutputLines.append( lastline )
            lastline = line
            line=inp.readline()
        try:
            self.fReturnValue = int(lastline)
        except ValueError:
            self.fReturnValue = 65536
            self.fOutputLines.append( lastline )
        #print "Remote output:",self.fOutputLines
        inp.close()


parser = optparse.OptionParser()


parser.add_option( "-n", "--node", action='append', dest='nodes', default=[], help="specify node as synchronization target" )
parser.add_option( "-p", "--path", action='append', dest='paths', default=[], help="specify a path that is to be synchronized" )
parser.add_option( "--rsync-options", action='store', dest='rsync_opt', default="-aH", help="specify options to be used for rsync instead of the default '-aH'" )
parser.add_option( "-v", "--verbose", action='store_true', dest='verbose', default=False, help="be more verbose" )


(options,args) = parser.parse_args()
#print options.nodes
#print options.paths
#print options.rsync_opt
if len(options.nodes)<=0:
    parser.error( "No node for synchronization specified. (-n/--node)" )

if len(options.paths)<=0:
    parser.error( "No path specified to be synchronized. (-p/--path)" )


thisCommand = os.path.abspath( sys.argv[0] )
if options.verbose:
    thisCommand += " --verbose"
inp = os.popen( "hostname" )
thisHostname=inp.readline()
inp.close()
thisHostname = thisHostname[:-1] if thisHostname[-1]=="\n" else thisHostname

paths = []
for path in options.paths:
    if not os.path.isabs(path):
        if options.verbose:
            print "Converting relative path",path,"to absolute path",os.path.abspath(path)
        paths.append( os.path.abspath(path) )
    else:
        paths.append( path )


# rsync_command = self.fShell+" "+self.fNode+" rsync "+self.fRsync

rem_threads = []

nodes = options.nodes
# print "nodes0:",nodes
while len(nodes)>0:
    # print "nodes1:",nodes
    node = nodes[0]
    for path in paths:
        rsync_command = "rsync "+options.rsync_opt+" \""+path+"/\""+" \""+node+":"+path+"/\" 2>&1"
        # rsync_command="echo '"+rsync_command+"'"
        inp = os.popen( rsync_command, "r" )
        
        line=inp.readline()
        lines = []
        while line !="":
            lines.append( line )
            line=inp.readline()
        inp.close()
        for line in lines:
            line = line[:-1] if (len(line)>0 and line[-1]=="\n") else line
            print datetime.datetime.now().isoformat()+" : "+thisHostname+" -> "+node+" rsync: "+line

    rem_nodes = nodes[1:(len(nodes)-1)/2+1]
    nodes = nodes[(len(nodes)-1)/2+1:]
    if len(rem_nodes)>0:
        rem_threads.append( remote_sync_thread(node, thisCommand, options.rsync_opt, paths, rem_nodes ) )
        rem_threads[-1].start()
    # print "nodes2:",nodes

while len(rem_threads)>0:
    if not rem_threads[0].isAlive():
        rem_threads[0].join()
        node = rem_threads[0].GetNode()
        outputLines = rem_threads[0].GetOutputLines()
        for line in outputLines:
            # print "rem_line:",line
            line = line[:-1] if (len(line)>0 and line[-1]=="\n") else line
            print node+" :_:_:_: "+line
        rem_threads = rem_threads[1:]
    else:
        time.sleep( 0.5 )
        


