#!/bin/bash
# 
# * Create configuration Files for HLT
#
# File   : verify-config-create.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.05.2008
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

# -- Check Comand Line Arguments --
# ---------------------------------
USAGE="Usage : $0 -detector=<Detector-ID> -config=<Config-ID> -type=<Config-Type:default=production> [--help]"

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get configuration
	    -config* )  CONFIG=$arg ;;

	    # get config type
	    -type*)     CONFIGTYPE=$arg ;;

	    * ) echo "Error : Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    else
	echo "Error : Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
    fi
done

# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} || ! ${CONFIG} ]] ; then
    echo "Error : " ${USAGE}
    exit 1
fi

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------

# -- Check Config ID --
# ---------------------
CONFIGID=`grep \<SimpleChainConfig2 ${CONFIGFILE} | awk -F\" '{print $2} '`

if [ "$CONFIGID" != "$DETECTOR" ] ; then
    echo "Error : ConfigID : ${CONFIGID} does not fit to ${DETECTOR}."
    exit 6
fi

# -- Check if files in svn --
# ---------------------------
LIST=`svn list ${REPOS}/${REPOSDIR}/|grep -- "setup/"`
if [ -z "${LIST}" ] ; then
    echo "Setup directory $REPOSDIR/$setup/ does not exist in SVN"
    exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/|grep -- "${DETECTOR}/"`
if [ -z "${LIST}" ] ; then
    echo "Detector directory ${REPOSDIR}/${DETECTOR}/ does not exist in SVN"
    exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/|grep -- "setup/"`
if [ -z "${LIST}" ] ; then
    echo "Detector setup directory ${REPOSDIR}/${DETECTOR}/setup does not exist in SVN"
    exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/setup|grep -- "${DDLLIST}"`
if [ -z "${LIST}" ] ; then
    echo "Configuration file ${REPOSDIR}/setup/${DDLLIST} does not exist in SVN"
    exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/setup|grep -- "${NODELIST}"`
if [ -z "${LIST}" ] ; then
    echo "Configuration file ${REPOSDIR}/${DETECTOR}/setup/${NODELIST} does not exist in SVN"
    exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/setup|grep -- "${SITECONFIG}"`
if [ -z "${LIST}" ] ; then
    echo "Configuration file ${REPOSDIR}/${DETECTOR}/setup/${SITECONFIG} does not exist in SVN"
    exit 1
fi

# -- Create Config --
# -------------------
echo Createing ${CONFIG}

pushd ${VERIFY_DETECTORCONFIGDIR} >/dev/null

CONT=`find ${VERIFY_DETECTORCONFIGDIR} -type f`

if [ -n "${CONT}" ] ; then
    rm  $CONT
fi

if [ ! -d ./setup ] ; then 
    mkdir ./setup
else
    rm -rf ./setup/*
fi

pushd ${VERIFY_DETECTORCONFIGDIR}/setup >/dev/null
svn export ${REPOS}/${REPOSDIR}/setup/${DDLLIST} 2>&1 |grep -Eiv "^(A  |Export complete)" | grep -v "Export complete."
svn export ${REPOS}/${REPOSDIR}/${DETECTOR}/setup/${NODELIST}  2>&1 |grep -Eiv "^(A  |Export complete)" | grep -v "Export complete."
svn export ${REPOS}/${REPOSDIR}/${DETECTOR}/setup/${SITECONFIG} 2>&1 |grep -Eiv "^(A  |Export complete)" | grep -v "Export complete."
svn export ${REPOS}/${REPOSDIR}/setup/${GLOBALSITECONFIG} 2>&1 |grep -Eiv "^(A  |Export complete)" | grep -v "Export complete."
popd >/dev/null

RETVAL=`MakeTaskManagerConfig2.py -masternode ${MASTERNODE} -siteconfig ${VERIFY_DETECTORRUNDIR}/setup/${GLOBALSITECONFIG} -siteconfig ${VERIFY_DETECTORRUNDIR}/setup/${SITECONFIG} -nodelist ${VERIFY_DETECTORRUNDIR}/setup/nodelist.xml -ddllist ${VERIFY_DETECTORRUNDIR}/setup/ddllist.xml -rundir /tmp/HLT-rundir/${DETECTOR}/${CONFIG}-\`date +%Y%m%d.%H%M%S\` -config ${CONFIGFILE} -basesocket ${VERIFY_BASESOCKET} -readoutlistversion 3 -output TM -output SCC1 -output GraphViz -quiet 1 ${PRODUCTION} 2>&1`

popd >/dev/null

if [ "${RETVAL}" ] ; then
    echo "Error : Creation of Configuration ${CONFIG} failed!"
    echo " "
    echo $RETVAL
    exit 6
fi 

exit 0