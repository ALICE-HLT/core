#!/bin/bash
# 
# * Definitions for the Run Environment
#
# File   : definitions.sh
#
# Author : Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.05.2008
#          11.10.2009 Added support for auto configuration
#
##################################################################
# Include Functions
. ${HLTCONTROL_TOPDIR}/bin/system/functions.sh
###################################################################

# -- BINDIR --
# ------------
SYSTEM_BINDIR=${HLTCONTROL_TOPDIR}/bin/system
OPERATOR_BINDIR=${HLTCONTROL_TOPDIR}/bin/operator
RUNMANAGER_BINDIR=${HLTCONTROL_TOPDIR}/bin/runmanager

# -- Results directory
# --------------
RESULT_BASEDIR=/mnt/fhgfs/data/results



# -- Defaults --
# --------------
BEAMTYPE=pp
RUNTYPE=PHYSICS

# -- Use ESMP GUI
GUIUSAGE=0

# -- Use rsync based remote sync (binary tree based) instead of AFS (or other network FS) for TaskManager control files during CONFIGURE
# -- valid values 1 (yes/sync) or 0 (no/network FS)
CONFIGURE_USE_TREE_SYNC=1

# -- Synchronize Pendolino XML files (using rsync) instead of AFS (or other network FS) during ENGAGE
# -- valid values 1 (yes/sync) or 0 (no/network FS)
CONFIGURE_USE_SYNC_PENDOLINO=1

# -- Execute specified initialization scripts during INITIALIZE (e.g. local software installation or local operator home directories)
# -- valid values 1 (yes/sync) or 0 (no/network FS)
CONFIGURE_USE_SYNC_INITIALIZE=0

# -- REPOS
REPOS=file:///afs/.alihlt.cern.ch/repository/svn
REPOSDIR=control/trunk/detector
CONFIGREPOSDIR=control/trunk/hlt-configuration
CHECKEDOUTREPOS=${HLTCONTROL_DIR}/hlt-configuration

# -- Files to edit
NODELIST=nodelist.xml
DDLLIST=ddllist.xml
SITECONFIG=siteconfig.xml
GLOBALSITECONFIG=globalsiteconfig.xml

# -- Config type
CONFIGTYPE=production

# -- LISTS  --
# ------------
SUPPORTED_CONFIGTYPES="production devel valid-test"
SUPPORTED_HRORC_FILES="Load Start Stop Clear Info Burst"
SUPPORTED_EDIT_FILES="globalsiteconfig siteconfig ddllist nodelist"

# -- NODES --
# -----------
ECSNODE_0=portal-ecs0
ECSNODE_1=portal-ecs1

TAXINODE_0=portal-vobox0
TAXINODE_1=portal-vobox1

DEVNODE_0=msdev0
DEVNODE_1=msdev0

#######################################################################################
VERIFY_BASESOCKET=15000
#######################################################################################

# -- BINARIES  --
# ---------------

# -- Release control volume
RELEASE_BIN=/opt/HLT/tools/bin/release-control.sh
 
if [ ! -f ${RELEASE_BIN} ] ; then
    echo ${RELEASE_BIN} does not exist.
    exit 1
fi

if [ ! -x ${RELEASE_BIN} ] ; then
    echo ${RELEASE_BIN} is not executable
    exit 1
fi


# - tree remote sync program
TREE_SYNC_BIN=${SYSTEM_BINDIR}/tree_sync.py

if [ ! -f ${TREE_SYNC_BIN} ] ; then
    echo ${TREE_SYNC_BIN} does not exist.
    exit 1
fi

if [ ! -x ${TREE_SYNC_BIN} ] ; then
    echo ${TREE_SYNC_BIN} is not executable
    exit 1
fi


# -- RunManager
RUNMANAGER_BIN=${RUNMANAGER_BINDIR}/HLT-with-pendolino.py
 
if [ ! -f ${RUNMANAGER_BIN} ] ; then
    echo ${RUNMANAGER_BIN} does not exist.
    exit 1
fi

if [ ! -x ${RUNMANAGER_BIN} ] ; then
    echo ${RUNMANAGER_BIN} is not executable
    exit 1
fi

# -- HRORC Handler
HRORCHANDLER_BIN=/opt/HLT/tools/bin/hrorc_handler.py

if [ ! -f ${HRORCHANDLER_BIN} ] ; then
    echo ${HRORCHANDLER_BIN} does not exist.
    exit 1
fi

if [ ! -x ${HRORCHANDLER_BIN} ] ; then
    echo ${HRORCHANDLER_BIN} is not executable
    exit 1
fi

# -- Remote Starter
REMOTE_STARTER_BIN=${SYSTEM_BINDIR}/remote-starter.py

if [ ! -f ${REMOTE_STARTER_BIN} ] ; then
    echo ${REMOTE_STARTER_BIN} does not exist.
    exit 1
fi

if [ ! -x ${REMOTE_STARTER_BIN} ] ; then
    echo ${REMOTE_STARTER_BIN} is not executable
    exit 1
fi

# -- Config snippet checkout program
SNIPPET_RETRIEVER_BIN=${RUNMANAGER_BINDIR}/retrieve-config-snippets.sh

if [ ! -f ${SNIPPET_RETRIEVER_BIN} ] ; then
    echo ${SNIPPET_RETRIEVER_BIN} does not exist.
    exit 1
fi

if [ ! -x ${SNIPPET_RETRIEVER_BIN} ] ; then
    echo ${SNIPPET_RETRIEVER_BIN} is not executable
    exit 1
fi

# -- TAXI DRIVER
TAXI_DRIVER_BIN=${HLTTAXI_TOPDIR}/TaxiDriver.sh 

if [ ! -f ${TAXI_DRIVER_BIN} ] ; then
    echo ${TAXI_DRIVER_BIN} does not exist.
    exit 1
fi

if [ ! -x ${TAXI_DRIVER_BIN} ] ; then
    echo ${TAXI_DRIVER_BIN} is not executable
    exit 1
fi

# -- Read node list
READ_NODELIST_BIN=${SYSTEM_BINDIR}/read_nodelist.py

if [ ! -f ${READ_NODELIST_BIN} ] ; then
    echo ${READ_NODELIST_BIN} does not exist.
    exit 1
fi

if [ ! -x ${READ_NODELIST_BIN} ] ; then
    echo ${READ_NODELIST_BIN} is not executable
    exit 1
fi

# -- Post exec
POST_EXEC_BIN=${SYSTEM_BINDIR}/post-exec-cmd.sh

if [ ! -f ${POST_EXEC_BIN} ] ; then
    echo ${POST_EXEC_BIN} does not exist.
    exit 1
fi

if [ ! -x ${POST_EXEC_BIN} ] ; then
    echo ${POST_EXEC_BIN} is not executable
    exit 1
fi
