#!/usr/bin/env python

import os
import sys
import posix
import re

def split_filename( filename ):
    """Split up a past filename.

    Splits up a passed filename according to either the template
    'Test0x00000000-0x00000000.log` or 'Test0x00000000.log`, where the prefix 'Test' is
    arbitrary, '0x00000000' is an eight digit hex number,
    and '.log' is fixed. Returns a
    tuple consisting of the prefix and the number."""
    match = re.compile( "(.*)(0x[0-9A-F]{8}-0x[0-9A-F]{8})(\.log)" ).search( filename );
    if ( match != None ):
        prefix = match.group( 1 )
        number = match.group( 2 )
        return ( prefix, number )
    match = re.compile( "(.*)(0x[0-9A-F]{8})(\.log)" ).search( filename );
    if ( match != None ):
        prefix = match.group( 1 )
        number = match.group( 2 )
        return ( prefix, number )
    
    return None

count=3
if len(sys.argv)>1:
    try:
        count=int(sys.argv[1])
    except:
        print "Usage: "+sys.argv[0]+" (<logfile-count-to-keep>)"
        print "Count argument is not an integer"
        sys.exit( 1 )

found_list = []

# Get all files
files = posix.listdir( "." )
files.sort( key=lambda x: os.stat(x).st_mtime )

# For each file found check, wether it is a numbered log file.
for file in files:
    #print "File:",file
    spl = split_filename( file )
    if spl != None:
        # If it is a numbered logfile, check if there is already a matching list.
        #print "  Split:",spl[0],spl[1]
        found = 0
        for l in found_list:
            if l[0][0]==spl[0]:
                # If a matching list is found, add the new file to it.
                #print "Found Match:",l[0][0],l[0][1]
                found = 1
                l.append( spl )

        # If not matching list is found, create a new one.
        if not found:
            newlist = [ spl ]
            found_list.append( newlist )
        
#print found_list
#print

for l in found_list:
    #l.sort()
    for t in l[:-count]:
        f = t[0]+t[1]+".log"
        print "Delete ",f
        posix.unlink( f )

