#!/bin/bash
# 
# * Control Archive this run
#
# File   : archiveTHISrun.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 06.12.2007
#
###################################################################

T0=`which $0`
T1=`dirname $T0`
T2=`readlink $T0`
T3=`dirname $T2`
pushd ${T1} >/dev/null
cd $T3/..
BASEDIR=`pwd`
popd >/dev/null

# -- Check Comand Line Arguments --
# ---------------------------------
if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0 [--help]"
else
    USAGE="Usage : $0 -detector=<Detector-ID> [--help]"
fi

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    * ) echo "Error : Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		
    else
	echo "Error : Argument not known: $cmd" ; USAGE
	exit 1
    fi
done

# -- Check usage --
# -----------------
if [ ! ${DETECTOR} ] ; then
    echo "Error: " ${USAGE}
    exit 1
fi

# -- Check if Detector exists --
# ------------------------------
DETECTORDIR=${BASEDIR}/detector/${DETECTOR}

if [ ! -d ${DETECTORDIR} ] ; then
    echo Detector ${DETECTOR} does not exist.
    exit 2
fi

# -- Check if RunManager configuration exists --
# ----------------------------------------------
RUNMANAGERCONFIGDIR=${BASEDIR}/configs/${DETECTOR}
RUNMANAGERCONFIGFILE=${RUNMANAGERCONFIGDIR}/settings

if [[ ! -d ${RUNMANAGERCONFIGDIR} || ! -f  ${RUNMANAGERCONFIGFILE} ]] ; then
    echo RunManager Configuration for Detector ${DETECTOR} does not exist.
    exit 3
fi


# -- Get BASE PORT --
# -------------------
#grep ARCHIVETHISRUN ${RUNMANAGERCONFIGFILE} | 

sed  -i 's/ARCHIVETHISRUN=[-_a-zA-Z0-9]*/ARCHIVETHISRUN=1/' ${RUNMANAGERCONFIGFILE}

