#!/bin/bash

TMPDIR=`dirname $0`
pushd ${TMPDIR} >/dev/null
cd ..
BASEDIR=`pwd`
popd >/dev/null


RESULTDIR=${BASEDIR}/results

find ${RESULTDIR} -name "*.tar" -exec bzip2 "{}" \;

