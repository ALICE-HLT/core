#!/bin/bash
# 
# * Archive configuration - result -Files for HLT
#
# File   : archive_config-ECS.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 27.08.2007
#
###################################################################

TMPDIR=`dirname $0`
pushd ${TMPDIR} >/dev/null
cd ..
BASEDIR=`pwd`
popd >/dev/null




# -- Clear SHM regions --
# -----------------------
if [ -x /usr/local/bin/psi_cleanup ] ; then 
    /usr/local/bin/psi_cleanup -all
fi

/opt/HLT/tools/bin/CloseSysVregions.sh   
    
    
kill -9 -1 > /dev/null 2>/dev/null

