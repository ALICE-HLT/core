#!/bin/bash

. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

if ! OUTPUTDIR=`mktemp -dt -p /tmp \`basename $0 .sh\`.tmpdir.\`date +%Y%m%d.%H%M%S\`.XXXXXX` ; then
  log "Error : Cannot make temporary directory for nodelist"
  exit 1
fi

DETECTOR=${DETECTOR_CONTROL}
OUTPUTFILE=${OUTPUTDIR}/output

DEBUGFILE=/tmp/.`basename $0 .sh`.debug
# echo >${DEBUGFILE}


echo "${SNIPPET_RETRIEVER_BIN} -filetype=partitionnodelist -outputdirectory=${OUTPUTDIR} -detector=${DETECTOR}" >> ${OUTPUTDIR}/.debug
${SNIPPET_RETRIEVER_BIN} -filetype=partitionnodelist -outputdirectory=${OUTPUTDIR} -detector=${DETECTOR} > ${OUTPUTFILE} 2>&1

# cat "${OUTPUTFILE}" >> ${DEBUGFILE}
log "partition nodelist file retrieved"

LINECNT=`cat ${OUTPUTFILE}|wc -l`
ERROR=`cat ${OUTPUTFILE}|grep -E "^Error"`
if [ "${LINECNT}" -gt 1 -o -n "${ERROR}" ] ; then
    log "Error retrieving partition nodelist: "`cat ${OUTPUTFILE}`
    echo "Error retrieving partition nodelist: "
    cat ${OUTPUTFILE}
    exit 3
fi
NODELIST=`cat ${OUTPUTFILE}`
# echo Nodelist: ${NODELIST} >>${DEBUGFILE}

NODES=`${HLTCONTROL_TOPDIR}/bin/system/GetHostnamesFromNodelist.py ${NODELIST}`
# echo Nodes: ${NODES} >>${DEBUGFILE}

NODELISTPARAM=""
for n in ${NODES} ; do
  NODELISTPARAM="${NODELISTPARAM} -m $n"
done
# echo Nodelistparam: ${NODELISTPARAM} >>${DEBUGFILE}

OPHOME=`echo ~`

rm -rf ${OUTPUTDIR}

#dsh -g prodcluster rsync -avH ecs1:$OPHOME/ '~'
# echo dsh ${NODELISTPARAM} rsync -avH ecs1:$OPHOME/ '~' >>${DEBUGFILE}
dsh ${NODELISTPARAM} rsync -avH ecs1:$OPHOME/ '~'
