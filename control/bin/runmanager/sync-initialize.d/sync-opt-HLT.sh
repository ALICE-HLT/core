#!/bin/bash

. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

if ! OUTPUTDIR=`mktemp -dt -p /tmp \`basename $0 .sh\`.tmpdir.\`date +%Y%m%d.%H%M%S\`.XXXXXX` ; then
  log "Error : Cannot make temporary directory for nodelist"
  exit 1
fi

DETECTOR=${DETECTOR_CONTROL}
OUTPUTFILE=${OUTPUTDIR}/output


echo "${SNIPPET_RETRIEVER_BIN} -filetype=partitionnodelist -outputdirectory=${OUTPUTDIR} -detector=${DETECTOR}" >> ${OUTPUTDIR}/.debug
${SNIPPET_RETRIEVER_BIN} -filetype=partitionnodelist -outputdirectory=${OUTPUTDIR} -detector=${DETECTOR} > ${OUTPUTFILE} 2>&1

cat "${OUTPUTFILE}" >> ${OUTPUTDIR}/.debug
log "partition nodelist file retrieved"

LINECNT=`cat ${OUTPUTFILE}|wc -l`
ERROR=`cat ${OUTPUTFILE}|grep -E "^Error"`
if [ "${LINECNT}" -gt 1 -o -n "${ERROR}" ] ; then
    log "Error retrieving partition nodelist: "`cat ${OUTPUTFILE}`
    echo "Error retrieving partition nodelist: "
    cat ${OUTPUTFILE}
    exit 3
fi
NODELIST=`cat ${OUTPUTFILE}`

NODES=`${HLTCONTROL_TOPDIR}/bin/system/GeHostnamesFromNodelist.py ${NODELIST}`

NODESPARAM=""
for n in ${NODES} ; do
  NODESPARAM="${NODESPARAM} -n $n"
done


${HLTCONTROL_TOPDIR}/bin/system/tree_sync.py ${NODESPARAM} -p /opt/HLT/ --rsync-options="-aqHc --delete"

