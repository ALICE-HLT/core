#!/bin/bash
# 
# * Checkout configuration file for HLT from svn repository
#
# File   : checkout-config.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.08.2007
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

# -- DEFAULTS --
# --------------

# -- Check Comand Line Arguments --
# ---------------------------------

if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0 -targetnode <targetnode> -targetdirectory <targetdirectory> [--help]"
else
    USAGE="Usage : $0 -detector=<Detector-ID> -targetnode <targetnode> -targetdirectory <targetdirectory> [--help]"
fi

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector=* ) DETECTOR=$arg ;;    

	    # get targetnode
	    -targetnode=* ) TARGETNODE=$arg ;;    

	    # get targetdirectory
	    -targetdirectory=* ) TARGETDIRECTORY=$arg ;;    

	    * ) echo "Error : Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    else
	echo "Error : Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
fi
done

# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} || ! ${TARGETNODE} || ! ${TARGETDIRECTORY} ]] ; then
    echo "Error : " ${USAGE}
    exit 1
fi


# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------


### ==============================================================================================
### ==============================================================================================
### ==============================================================================================
### ==============================================================================================

if [ "${CONFIGURE_USE_SYNC_PENDOLINO}" = 1 ] ; then
    rsync -aqH ${TARGETDIRECTORY}/ ${TARGETNODE}:${TARGETDIRECTORY}/
fi

