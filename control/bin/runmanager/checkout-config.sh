#!/bin/bash
# 
# * Checkout configuration file for HLT from svn repository
#
# File   : checkout-config.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.08.2007
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

# -- DEFAULTS --
# --------------
OUTPUTDIR="."

# -- Check Comand Line Arguments --
# ---------------------------------
USAGE="Usage : $0 -detector=<Detector-ID> -beamtype=<ALICE beam type (pp|AA)> -runtype=<detector run-type> (-outputdir=<Output-Dir>) [--help]"

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get beamtype, pp or AA
	    -beamtype* )  BEAMTYPE=$arg ;;

	    # get runtype
	    -runtype* )  RUNTYPE=$arg ;;

	    # get output directory (checkout target)
	    -outputdir* )  OUTPUTDIR=$arg ;;

	    * ) echo "Error : Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    else
	echo "Error : Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
    fi
done

# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} || ! ${BEAMTYPE} || ! {RUNTYPE} ]] ; then
    echo "Error : " ${USAGE}
    exit 1
fi

if [ ! -d ${OUTPUTDIR}/config ] ; then 
    mkdir ${OUTPUTDIR}/config
fi

if [ ! -d ${OUTPUTDIR}/config ] ; then 
    echo "Error : Unable to create directory ${OUTPUTDIR}/config"
    exit 2
fi

## BUGFIX -- needed in ECS proxy -- remove later on
if [ "${BEAMTYPE}" = "PP" ] ; then
    BEAMTYPE=pp
fi

CONFIG="${BEAMTYPE}-${RUNTYPE}"

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------

LIST=`svn list ${REPOS}/${REPOSDIR}|grep -- "${DETECTOR}/"`
if [ -z "${LIST}" ] ; then
  echo "Detector directory ${REPOSDIR}/${DETECTOR}/ does not exist in SVN"
  exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/config/|grep -- "${CONFIG}/"`

if [ -z "${LIST}" ] ; then
  echo "Configuration directory ${REPOSDIR}/${DETECTOR}/config/${CONFIG} does not exist in SVN"
  exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/config/${CONFIG}/|grep -- "${CONFIG}.xml"`

if [ -z "${LIST}" ] ; then
  echo "Configuration file ${REPOSDIR}/${DETECTOR}/config/${CONFIG}/${CONFIG}.xml does not exist in SVN"
  exit 1
fi

rm -rf ${OUTPUTDIR}/config/* ${OUTPUTDIR}/config/.svn

svn checkout ${REPOS}/${REPOSDIR}/${DETECTOR}/config/${CONFIG}/ ${OUTPUTDIR}/config/ 2>&1 |grep -Ev "^(A  |U  |Checked out revision|Restored )"

# Write Config Name to File for Status display
pushd ${OUTPUTDIR}/config > /dev/null

ACTIVECONFIG=`cat .active.${CONFIG}`

echo ${CONFIG} > .running
echo ${ACTIVECONFIG} >> .running

popd > /dev/null