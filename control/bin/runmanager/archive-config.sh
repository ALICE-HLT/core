#!/bin/bash
# 
# * Archive configuration - result -Files for HLT
#
# File   : archive-config.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.05.2008
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

# -- DEFAULTS --
# --------------
# enable/disable Archive of results/logs
ENABLEARCHIVE=1
ARCHIVETHISRUN=0
VERBOSE=0

# -- Check Comand Line Arguments --
# ---------------------------------
USAGE="Usage : $0 -detector=<Detector-ID> -beamtype=<ALICE beam type (pp|AA)> -runtype=<detector run-type> -detectordir=<Detector-directory> -master=<MasterNodeID>  [-configdir=<configuration-base-directory>] [-disable-archive] (-verbose) [--help]"

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector directory
	    -detectordir* ) DETECTORDIR=$arg ;;    

	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get beamtype, pp or AA
	    -beamtype* )  BEAMTYPE=$arg ;;

	    # get runtype
	    -runtype* )  RUNTYPE=$arg ;;

	    # get configuration base directory
	    -configdir* )  CONFIGDIR=$arg ;;

	    # get master node
	    -master* )  MASTERNODE=$arg ;;

            # disable archiving 
	    -disable-archive ) ENABLEARCHIVE=0 ;;

            # verbosity
	    -verbose ) VERBOSE=1 ;;

	    * ) echo "Error Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    else
	echo "Error Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
    fi
done

# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} || ! ${BEAMTYPE} || ! ${RUNTYPE} || ! ${DETECTORDIR} || ! ${MASTERNODE} ]] ; then
    echo "Error: " ${USAGE}
    exit 1
fi

CONFIG="${BEAMTYPE}-${RUNTYPE}"

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------

. ${RUNMANAGERCONFIGFILE}

# -- Get ARCHIVETHISRUN --
# ------------------------
ARCHIVETHISRUN=`grep ARCHIVETHISRUN ${RUNMANAGERCONFIGFILE} | sed 's|[-_a-zA-Z0-9]*=||'`

# -- Get Nodes --
# ---------------
#NODES=`grep hostname ${SCC1_CONFIGFILE} | grep -v master | awk -F\" '{print $4} ' `

# RESULTDIR=${HLTCONTROL_DIR}/results/${DETECTOR}/${CONFIG}-`date +%Y%m%d.%H%M%S`
RESULTDIR=${RESULT_BASEDIR}/${DETECTOR}/${CONFIG}-`date +%Y%m%d.%H%M%S`

if [ "${VERBOSE}" == "1" ] ; then
    echo  "--- Waiting before archival ---"
fi

# -- Archive masternode --
# ------------------------
# Config files are always archived 
if [ "${VERBOSE}" == "1" ] ; then
    echo  "--- Archiving node ${MASTERNODE} ---"
fi

if [ ! -d ${RESULTDIR}/master-${MASTERNODE} ] ; then
    mkdir -p ${RESULTDIR}/master-${MASTERNODE}
fi
    
if [ ! -d  /tmp/HLT-rundir-archived/${DETECTOR} ] ; then
    mkdir -p /tmp/HLT-rundir-archived/${DETECTOR}
fi

# Archive base config
tar cf ${RESULTDIR}/master-${MASTERNODE}/base-config.tar ${RUNMANAGERCONFIGDIR} 2>&1 | grep -v "tar: Removing leading ... from member names"

# Build list of files to archive
ARCHIVELIST=""

if [ -f ${DETECTORRUNDIR}/ModulesConfig.txt ] ; then
    ARCHIVELIST="${ARCHIVELIST} ${DETECTORRUNDIR}/ModulesConfig.txt"
fi

if [ -f ${DETECTORRUNDIR}/ECS-RunManager-start-parameters.txt ] ; then
    ARCHIVELIST="${ARCHIVELIST} ${DETECTORRUNDIR}/ECS-RunManager-start-parameters.txt"
fi

if [ -f ${DETECTORRUNDIR}/ECS-RunManager-connect-parameters.txt ] ; then
    ARCHIVELIST="${ARCHIVELIST} ${DETECTORRUNDIR}/ECS-RunManager-connect-parameters.txt"
fi

if [ -d ${DETECTORRUNCONFIGSRCDIR} ] ; then
    ARCHIVELIST="${ARCHIVELIST} ${DETECTORRUNCONFIGSRCDIR}"
fi

if [ -f ${DETECTORRUNCONFIGCREATEDDIR}/${DETECTOR}-SCC1-Generate.xml ] ; then
    ARCHIVELIST="${ARCHIVELIST} ${DETECTORRUNCONFIGCREATEDDIR}/${DETECTOR}-SCC1-Generate.xml"
fi

if [ -f ${DETECTORRUNCONFIGCREATEDDIR}/${DETECTOR}-Processes-Graph.dot ] ; then
    ARCHIVELIST="${ARCHIVELIST} ${DETECTORRUNCONFIGCREATEDDIR}/${DETECTOR}-Processes-Graph.dot"
fi

if [ -f ${DETECTORRUNCONFIGCREATEDDIR}/${DETECTOR}-ProcessList.out ] ; then
    ARCHIVELIST="${ARCHIVELIST} ${DETECTORRUNCONFIGCREATEDDIR}/${DETECTOR}-ProcessList.out"
fi

# Archive minimum run config
tar cf ${RESULTDIR}/master-${MASTERNODE}/run-config.tar ${ARCHIVELIST} 2>&1 | grep -v "tar: Removing leading ... from member names" | grep -v  "tar: Removing leading"

# -- MASTER NODE - run dir --
# ---------------------------
rm -f ${DETECTORRUNDIR}/TaskManager-${DETECTOR}-${MASTERNODE}-${MASTERNODE}-Master.xml-*.log 

pushd ${DETECTORDIR} >/dev/null
if [ "${VERBOSE}" == "1" ] ; then
    echo Cleaning RunManager logs.
fi

${SYSTEM_BINDIR}/clean-logs.py 1 > /dev/null 2>/dev/null

popd > /dev/null
