#!/bin/bash
# 
# * Checkout configuration file for HLT from svn repository
#
# File   : checkout-config.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.08.2007
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

# -- DEFAULTS --
# --------------
OUTPUTDIR="."
CTPTRIGGERCLASSES=""
SECONDMASTER=""

# -- Check Comand Line Arguments --
# ---------------------------------

if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0 -beamtype=<ALICE beam type (pp|AA)> -runtype=<detector run-type> -master=<master-node> -basesocket=<base-socket> -inddllist=<input-ddl-list> -outddllist=<output-ddl-list> -detectorlist=<ECS-detector-list> -hlttriggers=<ECS HLT trigger menu> -readoutlistversion=<readout-list-data-format-version-number> [-ctptriggerclasses=<ctp-trigger-classes>] [-secondmaster=<second-master-node>] [-outputdir=<Output-Dir>] [--help]"
else
    USAGE="Usage : $0 -detector=<Detector-ID> -beamtype=<ALICE beam type (pp|AA)> -runtype=<detector run-type> -master=<master-node> -basesocket=<base-socket> -inddllist=<input-ddl-list> -outddllist=<output-ddl-list> -detectorlist=<ECS-detector-list> -hlttriggers=<ECS HLT trigger menu> -readoutlistversion=<readout-list-data-format-version-number> [-ctptriggerclasses=<ctp-trigger-classes>] [-secondmaster=<second-master-node>] [-outputdir=<Output-Dir>] [--help]"
fi

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector=* ) DETECTOR=$arg ;;    

	    # get beamtype, pp or AA
	    -beamtype=* )  BEAMTYPE=$arg ;;

	    # get runtype
	    -runtype=* )  RUNTYPE=$arg ;;

	    # get output directory (checkout target)
	    -outputdir=* )  OUTPUTDIR=$arg ;;

	    # get masternode
	    -master=* )  MASTERNODE=$arg ;;

	    # get second masternode
	    -secondmaster=* )  SECONDMASTER="-masternode $arg" ;;

	    # get base socket
	    -basesocket=* )  BASESOCKET=$arg ;;

	    # get input ddl list
	    -inddllist=* )  INPUTDDLLIST=$arg ;;

	    # get output ddl list
	    -outddllist=* )  OUTPUTDDLLIST=$arg ;;

	    # get ctp trigger classes
	    -ctptriggerclasses=* )  CTPTRIGGERCLASSES="-ctptriggerclasses \"$arg\"" ;;

	    # get ECS detector list
	    -detectorlist=* )  ECSDETECTORLIST=$arg ;;

	    # get ECS HLT trigger menu list
	    -hlttriggers=* ) ECSHLTTRIGGERS=`echo $arg| tr abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ` ;;

	    # get HLT readout list data format version number
	    -readoutlistversion=*) READOUTLISTVERSION=$arg ;;

	    * ) echo "Error : Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    else
	echo "Error : Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
fi
done

# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} || ! ${BEAMTYPE} || ! ${RUNTYPE} || ! ${READOUTLISTVERSION} ]] ; then
    echo "Error : " ${USAGE}
    exit 1
fi

if [ ! -d ${OUTPUTDIR}/config ] ; then 
    mkdir -p ${OUTPUTDIR}/config
fi

if [ ! -d ${OUTPUTDIR}/config ] ; then 
    echo "Error : Unable to create directory ${OUTPUTDIR}/config"
    exit 2
fi
if [[ ! ${MASTERNODE} ]] ; then
    echo "Error : Missing -master parameter"
    echo "Error : " ${USAGE}
    exit 1
fi
if [[ ! ${BASESOCKET} ]] ; then
    echo "Error : Missing -basesocket parameter"
    echo "Error : " ${USAGE}
    exit 1
fi
if [[ ! ${INPUTDDLLIST} ]] ; then
    echo "Error : Missing -inddllist parameter"
    echo "Error : " ${USAGE}
    exit 1
fi
if [[ ! ${BEAMTYPE} ]] ; then
    echo "Error : Missing -beamtype parameter"
    echo "Error : " ${USAGE}
    exit 1
fi
if [[ ! ${RUNTYPE} ]] ; then
    echo "Error : Missing -runtype parameter"
    echo "Error : " ${USAGE}
    exit 1
fi
if [[ ! ${ECSDETECTORLIST} ]] ; then
    echo "Error : Missing -detectorlist parameter"
    echo "Error : " ${USAGE}
    exit 1
fi

if [[ ! ${ECSHLTTRIGGERS} ]] ; then
    echo "Error : Missing -hlttriggers parameter"
    echo "Error : " ${USAGE}
    exit 1
fi

## BUGFIX -- needed in ECS proxy -- remove later on
if [ "${BEAMTYPE}" = "PP" ] ; then
    BEAMTYPE=pp
fi

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------


### ==============================================================================================
### ==============================================================================================
### ==============================================================================================
### ==============================================================================================

# -- Clean up of output directory --
# ----------------------------------

if [ -d ${OUTPUTDIR}/config/.svn ] ; then
    rm -rf ${OUTPUTDIR}/config/.svn
fi

rm -rf ${OUTPUTDIR}/config/* 

mkdir ${OUTPUTDIR}/config/config-src
mkdir ${OUTPUTDIR}/config/config-created

# ---------------------------------------------------------------------------------
pushd ${OUTPUTDIR}/config > /dev/null

rm -rf ${OUTPUTDIR}/config/.debug* 
echo -n >.debug
echo -n > snippet-List


OUTPUTFILE=`mktemp -t -p . \`basename $0 .sh\`.tmpfile.\`date +%Y%m%d.%H%M%S\`.XXXXXX`

pushd ${OUTPUTDIR}/config/config-src >/dev/null

log "Updating checked out out svn config directory"
SVNUPDATECMD="svn update --accept postpone \"${CHECKEDOUTREPOS}\""
echo ${SVNUPDATECMD} >> ${OUTPUTDIR}/config/.debug
RETVAL=`${SVNUPDATECMD}`
echo "  OUTPUT: ${RETVAL}" > ${OUTPUTFILE}
if [ -n "${RETVAL}" ] ; then
    log "svn update output: ${RETVAL}"
fi

# -- Get ignore-file --
log "Retrieving ignore config file"
echo "${SNIPPET_RETRIEVER_BIN} -filetype=ignorefile -outputdirectory=. -detector=${DETECTOR} -runtype=${RUNTYPE} -beamtype=${BEAMTYPE}" >> ${OUTPUTDIR}/config/.debug
${SNIPPET_RETRIEVER_BIN} -filetype=ignorefile -outputdirectory=. -detector=${DETECTOR} -runtype=${RUNTYPE} -beamtype=${BEAMTYPE} > ${OUTPUTFILE} 2>&1
cat "${OUTPUTFILE}" >> ${OUTPUTDIR}/config/.debug
log "Ignore config file retrieved"

LINECNT=`cat ${OUTPUTFILE}|wc -l`
ERROR=`cat ${OUTPUTFILE}|grep -E "^Error"`
if [ "${LINECNT}" -gt 1 -o -n "${ERROR}" ] ; then
    echo "Error retrieving ignore config file: "
    cat ${OUTPUTFILE}
    exit 3
fi

IGNOREFILE=`cat ${OUTPUTFILE}`
echo IGNOREFILE \'"${IGNOREFILE}"\' >> ${OUTPUTDIR}/config/.debug
IGNOREBEAM=""
if [ -n "${IGNOREFILE}" ] ; then 
    echo grep "${RUNTYPE}:beamtype" ${IGNOREFILE} >> ${OUTPUTDIR}/config/.debug
    IGNOREBEAM=`grep "${RUNTYPE}:beamtype" ${IGNOREFILE}`
fi

echo ignorebeam \'"${IGNOREBEAM}"\' >> ${OUTPUTDIR}/config/.debug


SITECONFIGS=""
DDLLISTS=""
# -- Get setup files -- 
# -----------------------------
#     globalsiteconfig
#     globalsiteconfigbeam
#     globalsiteconfigrun
#     globalsiteconfigbeamrun
#     partitionsiteconfig
#     partitionsiteconfigbeam
#     partitionsiteconfigrun
#     partitionsiteconfigbeamrun
#     partitionnodelist
#     partitionddllist
#     globalddllist
for SCOPE in global partition ; do
    for CONFTYPE in "" beam run beamrun ; do 
	echo "${SNIPPET_RETRIEVER_BIN} -filetype=${SCOPE}siteconfig${CONFTYPE} -outputdirectory=. -detector=${DETECTOR} -runtype=${RUNTYPE} -beamtype=${BEAMTYPE}" >> ${OUTPUTDIR}/config/.debug
	${SNIPPET_RETRIEVER_BIN} -filetype=${SCOPE}siteconfig${CONFTYPE} -outputdirectory=. -detector=${DETECTOR} -runtype=${RUNTYPE} -beamtype=${BEAMTYPE} > ${OUTPUTFILE} 2>&1
	cat "${OUTPUTFILE}" >> ${OUTPUTDIR}/config/.debug
	log "${SCOPE} ${CONFTYPE} siteconfig file retrieved"

	LINECNT=`cat ${OUTPUTFILE}|wc -l`
	ERROR=`cat ${OUTPUTFILE}|grep -E "^Error"`
	if [ "${LINECNT}" -gt 1 -o -n "${ERROR}" ] ; then
	    log "Error retrieving ${SCOPE} ${CONFTYPE} siteconfig: "`cat ${OUTPUTFILE}`
	    echo "Error retrieving ${SCOPE} ${CONFTYPE} siteconfig: "
	    cat ${OUTPUTFILE}
	    exit 3
	fi

	SITECONF=`cat ${OUTPUTFILE}`
	if [ \( "${CONFTYPE}" = beam -o "${CONFTYPE}" = run \) -a -z "${SITECONF}" ] ; then
	    log "Retrieving default ${SCOPE} ${CONFTYPE} siteconfig files"
	    echo "${SNIPPET_RETRIEVER_BIN} -filetype=${SCOPE}siteconfig${CONFTYPE} -outputdirectory=. -detector=${DETECTOR} -runtype=rundefault -beamtype=beamdefault" >> ${OUTPUTDIR}/config/.debug
	    ${SNIPPET_RETRIEVER_BIN} -filetype=${SCOPE}siteconfig${CONFTYPE} -outputdirectory=. -detector=${DETECTOR} -runtype=rundefault -beamtype=beamdefault > ${OUTPUTFILE} 2>&1
	    cat "${OUTPUTFILE}" >> ${OUTPUTDIR}/config/.debug
	    log "default ${SCOPE} ${CONFTYPE} siteconfig file retrieved"

	    LINECNT=`cat ${OUTPUTFILE}|wc -l`
	    ERROR=`cat ${OUTPUTFILE}|grep -E "^Error"`
	    if [ "${LINECNT}" -gt 1 -o -n "${ERROR}" ] ; then
		log "Error retrieving default ${SCOPE} ${CONFTYPE} siteconfig: "`cat ${OUTPUTFILE}`
		echo "Error retrieving default ${SCOPE} ${CONFTYPE} siteconfig: "
		cat ${OUTPUTFILE}
		exit 3
	    fi
	    
	    SITECONF=`cat ${OUTPUTFILE}`
	
	fi

	SITECONFIGS="${SITECONFIGS} ${SITECONF}"
    done
    echo "${SNIPPET_RETRIEVER_BIN} -filetype=${SCOPE}ddllist -outputdirectory=. -detector=${DETECTOR} -runtype=${RUNTYPE} -beamtype=${BEAMTYPE}" >> ${OUTPUTDIR}/config/.debug
    ${SNIPPET_RETRIEVER_BIN} -filetype=${SCOPE}ddllist -outputdirectory=. -detector=${DETECTOR} -runtype=${RUNTYPE} -beamtype=${BEAMTYPE} > ${OUTPUTFILE} 2>&1
    cat "${OUTPUTFILE}" >> ${OUTPUTDIR}/config/.debug
    log "${SCOPE} ddllist file retrieved"
    
    LINECNT=`cat ${OUTPUTFILE}|wc -l`
    ERROR=`cat ${OUTPUTFILE}|grep -E "^Error"`
    if [ "${LINECNT}" -gt 1 -o -n "${ERROR}" ] ; then
	log "Error retrieving ${SCOPE} ddllist: "`cat ${OUTPUTFILE}`
	echo "Error retrieving ${SCOPE} ddllist: "
	cat ${OUTPUTFILE}
	exit 3
    fi
    
    DDLLISTFILE=`cat ${OUTPUTFILE}`
    if [ "${SCOPE}" = partition -a -n "${DDLLISTFILE}" ] ; then
	log "ATTENTION : Using partition specific DDL list file!! This can be useful for standalone testing but dangerous in real runs with ALICE ECS/DAQ"
    fi
    DDLLISTS="${DDLLISTS} ${DDLLISTFILE}"
done

SITECONFIGPARAM=""
for SCNF in ${SITECONFIGS} ; do
    SITECONFIGPARAM="${SITECONFIGPARAM} -siteconfig ${SCNF}"
done

for DDLLIST in ${DDLLISTS} ; do
    DDLLISTPARAM="-ddllist ${DDLLIST}"
    # Overwrite done on purpose, use global list only if no partition specific list defined
done

echo "${SNIPPET_RETRIEVER_BIN} -filetype=partitionnodelist -outputdirectory=. -detector=${DETECTOR} -runtype=${RUNTYPE} -beamtype=${BEAMTYPE}" >> ${OUTPUTDIR}/config/.debug
${SNIPPET_RETRIEVER_BIN} -filetype=partitionnodelist -outputdirectory=. -detector=${DETECTOR} -runtype=${RUNTYPE} -beamtype=${BEAMTYPE} > ${OUTPUTFILE} 2>&1
cat "${OUTPUTFILE}" >> ${OUTPUTDIR}/config/.debug
log "partition nodelist file retrieved"

LINECNT=`cat ${OUTPUTFILE}|wc -l`
ERROR=`cat ${OUTPUTFILE}|grep -E "^Error"`
if [ "${LINECNT}" -gt 1 -o -n "${ERROR}" ] ; then
    log "Error retrieving partition nodelist: "`cat ${OUTPUTFILE}`
    echo "Error retrieving partition nodelist: "
    cat ${OUTPUTFILE}
    exit 3
fi
NODELIST=`cat ${OUTPUTFILE}`
NODELISTPARAM="-nodelist ${NODELIST}"


# -- Get Partition base file --
# -----------------------------
log "Retrieving partition base config file"
echo "${SNIPPET_RETRIEVER_BIN} -filetype=partition -outputdirectory=. -detector=${DETECTOR} -runtype=${RUNTYPE} -beamtype=${BEAMTYPE}" >> ${OUTPUTDIR}/config/.debug
${SNIPPET_RETRIEVER_BIN} -filetype=partitionbase -outputdirectory=. -detector=${DETECTOR} -runtype=${RUNTYPE} -beamtype=${BEAMTYPE} > ${OUTPUTFILE} 2>&1
cat "${OUTPUTFILE}" >> ${OUTPUTDIR}/config/.debug
log "Partition base config files retrieved"


LINECNT=`cat ${OUTPUTFILE}|wc -l`
ERROR=`cat ${OUTPUTFILE}|grep -E "^Error"`
if [ "${LINECNT}" -gt 1 -o -n "${ERROR}" ] ; then
    log "Error retrieving partition config snippet: "
    echo "Error retrieving partition config snippet: "
    cat ${OUTPUTFILE}
    exit 3
fi

BASECONFIGS=`cat ${OUTPUTFILE}`


# -- Get Partition beamtype and runtype files --
# ----------------------------------------------
for PARTTYPE in partitionbeam partitionrun partitionbeamrun ; do

    if [ \( "${PARTTYPE}" = partitionbeam -o "${PARTTYPE}" = partitionbeamrun \) -a -n "${IGNOREBEAM}" ] ; then
      log "Ignoring ${PARTTYPE} file due to ignore specification: ${IGNOREBEAM}"
      continue
    fi

    log "Retrieving ${PARTTYPE} config files"
    ${SNIPPET_RETRIEVER_BIN} -filetype=${PARTTYPE} -outputdirectory=. -detector=${DETECTOR} -runtype=${RUNTYPE} -beamtype=${BEAMTYPE} >${OUTPUTFILE} 2>&1
    log "${PARTTYPE} config files retrieved"

    cat "${OUTPUTFILE}" >>${OUTPUTDIR}/config/.debug

    LINECNT=`cat ${OUTPUTFILE}|wc -l`
    ERROR=`cat ${OUTPUTFILE}|grep -E "^Error"`

    if [ "${LINECNT}" -gt 1 -o -n "${ERROR}" ] ; then
        echo "Error retrieving partition config snippet: "
        cat ${OUTPUTFILE}
        exit 3
    fi

    CONFIG=`cat ${OUTPUTFILE}`
    if [ \( "${PARTTYPE}" = partitionbeam -o "${PARTTYPE}" = partitionrun \) -a -z "${CONFIG}" ] ; then
	log "Retrieving ${PARTTYPE} default config files"
	${SNIPPET_RETRIEVER_BIN} -filetype=${PARTTYPE} -outputdirectory=. -detector=${DETECTOR} -runtype=rundefault -beamtype=beamdefault >${OUTPUTFILE} 2>&1
	log "${PARTTYPE} config files retrieved"

	cat "${OUTPUTFILE}" >>${OUTPUTDIR}/config/.debug
	
	LINECNT=`cat ${OUTPUTFILE}|wc -l`
	ERROR=`cat ${OUTPUTFILE}|grep -E "^Error"`
	
	if [ "${LINECNT}" -gt 1 -o -n "${ERROR}" ] ; then
            echo "Error retrieving partition config snippet: "
            cat ${OUTPUTFILE}
            exit 3
	fi
	
	CONFIG=`cat ${OUTPUTFILE}`
	
    fi
    BASECONFIGS="${BASECONFIGS} ${CONFIG}"
done

CONFIGS=""
for CNF in ${BASECONFIGS} ; do
  CONFIGS="${CONFIGS} -config ${CNF}"
done


# -- Get Snippets from triggermenu, detectors and components --
# -------------------------------------------------------------

log "Retrieving remaining config files"
echo "GetSnippetFileNames.py ${SITECONFIGPARAM} ${NODELISTPARAM} ${DDLLISTPARAM} "${CONFIGS}" -ecsdetectors ${ECSDETECTORLIST} -readoutlistversion ${READOUTLISTVERSION} -ecsruntype ${RUNTYPE} -ecshlttrigger ${ECSHLTTRIGGERS} -quiet 0 -fileretriever "${SNIPPET_RETRIEVER_BIN} -outputdirectory=. -detector=${DETECTOR} -runtype=${RUNTYPE} -beamtype=${BEAMTYPE}" -quiet 1 -debug 0 -ignorefile "${IGNOREFILE}"" >>${OUTPUTDIR}/config/.debug

GetSnippetFileNames.py ${SITECONFIGPARAM} ${NODELISTPARAM} ${DDLLISTPARAM} ${CONFIGS} -ecsdetectors ${ECSDETECTORLIST} -readoutlistversion ${READOUTLISTVERSION} -ecsruntype ${RUNTYPE} -ecshlttrigger ${ECSHLTTRIGGERS} -quiet 0 -fileretriever "${SNIPPET_RETRIEVER_BIN} -outputdirectory=. -detector=${DETECTOR} -runtype=${RUNTYPE} -beamtype=${BEAMTYPE}" -quiet 1 -debug 0 -ignorefile "${IGNOREFILE}" >${OUTPUTFILE} 2>&1
log "Remaining config files retrieved"

echo  >>${OUTPUTDIR}/config/.debug
( echo -n OUTPUTFILE: ; cat "${OUTPUTFILE}" ) >>${OUTPUTDIR}/config/.debug


LINECNT=`cat ${OUTPUTFILE}|wc -l`
echo LINECNT: "${LINECNT}" >>${OUTPUTDIR}/config/.debug

if [ "${LINECNT}" -gt 1 ] ; then
    echo "Error retrieving list of config snippets: "
    cat ${OUTPUTFILE}
    exit 3
fi

FILELIST=`cat ${OUTPUTFILE}`

rm ${OUTPUTFILE}
popd >/dev/null



#ACTIVECONFIG=`cat .active.${CONFIG}`

echo ${CONFIG} > .running
#echo ${ACTIVECONFIG} >> .running
echo "${BASECONFIG} ${FILELIST}" >>.running_config_snippet_list


CONFIGS=""
for CNF in ${FILELIST} ; do
  CONFIGS="${CONFIGS} -config ../config-src/${CNF}"
done

SITECONFIGPARAM=""
for SCNF in ${SITECONFIGS} ; do
    SITECONFIGPARAM="${SITECONFIGPARAM} -siteconfig ../config-src/${SCNF}"
done

for DDLLIST in ${DDLLISTS} ; do
    DDLLISTPARAM="-ddllist ../config-src/${DDLLIST}"
    # Overwrite done on purpose, use global list only if no partition specific list defined
done

NODELISTPARAM="-nodelist ../config-src/${NODELIST}"

# -- Create Configuration --
# --------------------------

#### chmod a+x `which MakeTaskManagerConfig2.py` 2>/dev/null >/dev/null

pushd ${OUTPUTDIR}/config/config-created >/dev/null
log "Creating configuration"
echo "MakeTaskManagerConfig2.py -masternode ${MASTERNODE} ${SECONDMASTER} ${SITECONFIGPARAM} ${NODELISTPARAM} ${DDLLISTPARAM} -rundir /tmp/HLT-rundir/${DETECTOR}/${CONFIG}-\`date +%Y%m%d.%H%M%S\` ${CONFIGS} -basesocket ${BASESOCKET} -ecsinputlist ${INPUTDDLLIST} -ecsoutputlist "${OUTPUTDDLLIST}" -readoutlistversion ${READOUTLISTVERSION} ${CTPTRIGGERCLASSES} -output TM -output SCC1 -quiet 1" >>${OUTPUTDIR}/config/.debug

RETVAL=`MakeTaskManagerConfig2.py -masternode ${MASTERNODE} ${SECONDMASTER} ${SITECONFIGPARAM} ${NODELISTPARAM} ${DDLLISTPARAM} -rundir /tmp/HLT-rundir/${DETECTOR}/${CONFIG}-\`date +%Y%m%d.%H%M%S\` ${CONFIGS} -basesocket ${BASESOCKET} -ecsinputlist ${INPUTDDLLIST} -ecsoutputlist "${OUTPUTDDLLIST}" -readoutlistversion ${READOUTLISTVERSION} ${CTPTRIGGERCLASSES} -output TM -output SCC1 -quiet 1 2>&1` 
log "Configuration created"

if false ; then
    DOT=`which dot`
    if [ -r "${DETECTOR}-ProcessGraph.dot" -a -x "${DOT}" ] ; then
	for ft in png svg ; do
	    log "Creation configuration graph ${ft} image"
	    dot -T ${ft} -o "${DETECTOR}-ProcessGraph.${ft}" "${DETECTOR}-ProcessGraph.dot"
	    log "Configuration graph ${ft} image created"
	done
    elif [ -r "${DETECTOR}-ProcessGraph.dot" ] ; then
	log "Configuration graph not found (too old software release). Could not create configuration graph images"
    elif [ -x "${DOT}" ] ; then
	log "Could not find dot executable (GraphViz package) in path. Could not create configuration graph images"
    fi
fi

popd >/dev/null

popd >/dev/null

if [ "${RETVAL}" ] ; then
    echo "Error : Creation of Configuration ${CONFIG} failed!"
    echo $RETVAL
    exit 5
fi 


# -- Synchronize/push config dir to other nodes --
# --------------------
if [ "${CONFIGURE_USE_TREE_SYNC}" = 1 ] ; then
    log "Pushing directory ${OUTPUTDIR} to remote nodes"
    HOSTNAME=`hostname`
    echo Step 1 >>${OUTPUTDIR}/config/.debug
    grep "<Node ID=" ${OUTPUTDIR}/config/config-created/${DETECTOR}-SCC1-Generate.xml >>${OUTPUTDIR}/config/.debug
    echo Step 2 >>${OUTPUTDIR}/config/.debug
    grep "<Node ID=" ${OUTPUTDIR}/config/config-created/${DETECTOR}-SCC1-Generate.xml | awk -F\" '{print $4}' >>${OUTPUTDIR}/config/.debug
    echo Step 3 >>${OUTPUTDIR}/config/.debug
    grep "<Node ID=" ${OUTPUTDIR}/config/config-created/${DETECTOR}-SCC1-Generate.xml | awk -F\" '{print $4}' | grep -Ev "^${HOSTNAME}\$" >>${OUTPUTDIR}/config/.debug
    echo "grep \"<Node ID=\" ${OUTPUTDIR}/config/config-created/${DETECTOR}-SCC1-Generate.xml | awk -F\\\" '{print \$4}' | grep -Ev \"^${HOSTNAME}\$\"" >>${OUTPUTDIR}/config/.debug
    NODES=`grep "<Node ID=" ${OUTPUTDIR}/config/config-created/${DETECTOR}-SCC1-Generate.xml | awk -F\" '{print $4}' | grep -Ev "^${HOSTNAME}\$" | grep -v ecs`
    echo Push nodes: ${NODES} >>${OUTPUTDIR}/config/.debug
    NODEPARAMS=""
    for N in ${NODES} ; do
	NODEPARAMS="${NODEPARAMS} --node ${N}"
    done
    echo ${TREE_SYNC_BIN} --rsync-options=\"-aHc --delete\" ${NODEPARAMS} --path \"${OUTPUTDIR}\" >>${OUTPUTDIR}/config/.debug
    #${TREE_SYNC_BIN} --rsync-options="-aHc --delete" ${NODEPARAMS} --path "${OUTPUTDIR}" >>${OUTPUTDIR}/config/.debug 2>&1


    DSHPARAMS=""
    for N in ${NODES} ; do
	test -n "$(ls ${OUTPUTDIR}/config/config-created/*${N}* 2>/dev/null)" && \
        	DSHPARAMS="${DSHPARAMS} -m ${N}"
    done

    dsh ${DSHPARAMS} "mkdir -p ${OUTPUTDIR}/config/config-created; rm -rf ${OUTPUTDIR}/config/config-created/*" \; scp ${HOSTNAME}:${OUTPUTDIR}/config/config-created/'*`hostname`*' ${OUTPUTDIR}/config/config-created/ '&&' scp ${HOSTNAME}:${OUTPUTDIR}/config/config-created/${DETECTOR}-SCC1-Generate.xml ${OUTPUTDIR}/config/config-created/
    if [ "$?" -ne 0 ]; then
        log "Error pushing individual config files to remote hosts..."
        #${TREE_SYNC_BIN} --rsync-options="-aH --delete" ${NODEPARAMS} --path "${OUTPUTDIR}" >>${OUTPUTDIR}/config/.debug 2>&1
    fi
    log "Directory ${OUTPUTDIR} pushed to remote nodes"
else
    log "Releasing AFS detector volume ${DETECTOR}"
    ${RELEASE_BIN} -detector=${DETECTOR} -quiet > /dev/null
    log "AFS detector volume ${DETECTOR} released"
fi


exit 0



