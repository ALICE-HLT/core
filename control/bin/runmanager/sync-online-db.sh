#!/bin/bash
# 
# * Checkout configuration file for HLT from svn repository
#
# File   : checkout-config.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.08.2007
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

if [ $# -lt 1 ] ; then 
    echo "Usage: $0 <sync-item>"
    echo Missing argument
    exit 1
fi

SYNC_ITEM=$1

if [ "${SYNC_ITEM}" != HCDB -a "${SYNC_ITEM}" != T-HCDB ] ; then
    echo "Usage: $0 <sync-item>"
    echo Unknown sync item ${SYNC_ITEM}
    exit 1
fi

# Rely on GlusterFS for distribution, no more action required.
