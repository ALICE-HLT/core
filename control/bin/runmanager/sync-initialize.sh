#!/bin/bash
# 
# * Checkout configuration file for HLT from svn repository
#
# File   : checkout-config.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.08.2007
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

DETECTOR=${DETECTOR_CONTROL}

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------

DEBUGFILE=/tmp/.`basename $0 .sh`.debug
# echo >${DEBUGFILE}

if [ "${CONFIGURE_USE_SYNC_INITIALIZE}" = 1 ] ; then
    SCRIPTS_DIR=`dirname $0`/sync-initialize.d
    SCRIPTS=`ls -1 ${SCRIPTS_DIR} | grep -E "\.sh$"|sort`

# echo Scripts: ${SCRIPTS} >>${DEBUGFILE}
# echo "  "`ls -1 ${SCRIPTS_DIR}` >>${DEBUGFILE}
# echo "  "`ls -1 ${SCRIPTS_DIR} | grep -E "\.sh$"` >>${DEBUGFILE}
# echo "  "`ls -1 ${SCRIPTS_DIR} | grep -E "\.sh$"|sort` >>${DEBUGFILE}

    for scr in ${SCRIPTS} ; do
        # echo ${SCRIPTS_DIR}/${scr} >>${DEBUGFILE}
	if [ -r ${SCRIPTS_DIR}/${scr} -a -x ${SCRIPTS_DIR}/${scr} -a -f ${SCRIPTS_DIR}/${scr} ] ; then
	    # echo Running ${SCRIPTS_DIR}/${scr} >>${DEBUGFILE}
	    ${SCRIPTS_DIR}/${scr}
	fi
    done
fi
