#!/bin/bash
# 
# * Checkout node and ddl list files for HLT from svn repository
#
# File   : checkout_setupfiles-ECS.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.08.2007
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

exit 0

# -- DEFAULTS --
# --------------
OUTPUTDIR="."

# -- Check Comand Line Arguments --
# ---------------------------------
USAGE="Usage : $0 -detector=<Detector-ID> (-outputdir=<Output-Dir>) [--help]"

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get output directory (checkout target)
	    -outputdir* )  OUTPUTDIR=$arg ;;

	    * ) echo "Error : Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    else
	echo "Error : Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
    fi
done

# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} ]] ; then
    echo "Error : " ${USAGE}
    exit 1
fi

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------

# -- Check if files in svn --
# ---------------------------
LIST=`svn list ${REPOS}/${REPOSDIR}/|grep -- "setup/"`
if [ -z "${LIST}" ] ; then
    echo "Setup directory $REPOSDIR/$setup/ does not exist in SVN"
    exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/|grep -- "${DETECTOR}/"`
if [ -z "${LIST}" ] ; then
    echo "Detector directory ${REPOSDIR}/${DETECTOR}/ does not exist in SVN"
    exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/|grep -- "setup/"`
if [ -z "${LIST}" ] ; then
    echo "Detector setup directory ${REPOSDIR}/${DETECTOR}/setup does not exist in SVN"
    exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/setup|grep -- "${DDLLIST}"`
if [ -z "${LIST}" ] ; then
    echo "Configuration file ${REPOSDIR}/setup/${DDLLIST} does not exist in SVN"
    exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/setup|grep -- "${GLOBALSITECONFIG}"`
if [ -z "${LIST}" ] ; then
    echo "Configuration file ${REPOSDIR}/setup/${GLOBALSITECONFIG} does not exist in SVN"
    exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/setup|grep -- "${NODELIST}"`
if [ -z "${LIST}" ] ; then
    echo "Configuration file ${REPOSDIR}/${DETECTOR}/setup/${NODELIST} does not exist in SVN"
    exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/setup|grep -- "${SITECONFIG}"`
if [ -z "${LIST}" ] ; then
    echo "Configuration file ${REPOSDIR}/${DETECTOR}/setup/${SITECONFIG} does not exist in SVN"
    exit 1
fi

pushd ${OUTPUTDIR} > /dev/null
if [[ "${DETECTOR}" == "DEV" || "${DETECTOR}" == "TEST" ]] ; then
    true
    #echo "DO soome"
else
    svn export ${REPOS}/${REPOSDIR}/setup/${DDLLIST} 2>&1 |grep -Eiv "^(A  |Export complete)" | grep -v "Export complete."
    svn export ${REPOS}/${REPOSDIR}/setup/${GLOBALSITECONFIG} 2>&1 |grep -Eiv "^(A  |Export complete)" | grep -v "Export complete."
fi
svn export ${REPOS}/${REPOSDIR}/${DETECTOR}/setup/${NODELIST}  2>&1 |grep -Eiv "^(A  |Export complete)" | grep -v "Export complete."
svn export ${REPOS}/${REPOSDIR}/${DETECTOR}/setup/${SITECONFIG} 2>&1 |grep -Eiv "^(A  |Export complete)" | grep -v "Export complete."
popd > /dev/null
