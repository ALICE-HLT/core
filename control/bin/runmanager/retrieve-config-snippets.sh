#!/bin/bash
# 
# * Checkout configuration file for HLT from svn repository
#
# File   : checkout-config.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 08.07.2009
#
###################################################################
# Include Defintions
. ${HLTCONTROL_TOPDIR}/bin/system/definitions.sh
###################################################################

# -- DEFAULTS --
# --------------
OUTPUTDIR="."
DETECTORID=""
SNIPPETID=""

# -- Check Comand Line Arguments --
# ---------------------------------
USAGE="Usage : $0 -detector=<Detector-ID> -beamtype=<ALICE beam type (pp|AA)> -runtype=<detector run-type> -filetype=<file type (partitionbase|partitionbeam|partitionrun|partitionbeamrun|triggerlist|trigger|component|detectorbase|detectorbeam|detectorrun|detectorbeamrun|ignorefile|globalsiteconfig|globalsiteconfigbeam|globalsiteconfigrun|globalsiteconfigbeamrun|partitionsiteconfig|partitionsiteconfigbeam|partitionsiteconfigrun|partitionsiteconfigbeamrun|partitionnodelist|partitionddllist|globalddllist)> [-id=<snippet identifier>] [-snippetdetectorid=<detector identifier>] [-outputdir=<Output-Dir>] [--help]"

######################## USAGE ####################################
# -detector=<Detector-ID> 
# -beamtype=<ALICE beam type (pp|AA)> 
# -runtype=<detector run-type> 
# -filetype=<file type (
#     partitionbase
#     partitionbeam
#     partitionrun
#     partitionbeamrun
#     triggerlist
#     trigger
#     component
#     detectorbase
#     detectorbeam
#     detectorrun
#     detectorbeamrun
#     ignorefile
#     globalsiteconfig
#     globalsiteconfigbeam
#     globalsiteconfigrun
#     globalsiteconfigbeamrun
#     partitionsiteconfig
#     partitionsiteconfigbeam
#     partitionsiteconfigrun
#     partitionsiteconfigbeamrun
#     partitionnodelist
#     partitionddllist
#     globalddllist
# )> 
# [-id=<snippet identifier>]
# [-snippetdetectorid=<detector identifier>]
# [-outputdir=<Output-Dir>]
# [--help]"
######################## USAGE ####################################

####################### DEBUG ######################
echo >>"${OUTPUTDIR}/.debug.retrieve-config.snippets.sh"
echo >>"${OUTPUTDIR}/.debug.retrieve-config.snippets.sh"
echo $0 $* >>"${OUTPUTDIR}/.debug.retrieve-config.snippets.sh"
####################### DEBUG ######################

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo "Error: ${USAGE}" ; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get beamtype, pp or AA
	    -beamtype* )  BEAMTYPE=$arg ;;

	    # get runtype
	    -runtype* )  RUNTYPE=$arg ;;

	    # get filetype
	    -filetype* )  FILETYPE=$arg ;;

	    # get snippet-id
	    -id* )  SNIPPETID=$arg ;;

	    # get detector identifier
	    -snippetdetectorid* )  DETECTORID=$arg ;;

	    # get output directory (checkout target)
	    -outputdir* )  OUTPUTDIR=$arg ;;

	    * ) echo "Error: Argument not known: $cmd"; echo "Error: ${USAGE}" ; exit 1;;
	esac		

    else
	echo "Error: Argument not known: $cmd"
	echo "Error: ${USAGE}"
	exit 1
    fi
done

# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} || ! ${BEAMTYPE} || ! ${RUNTYPE} || ! ${FILETYPE} ]] ; then
    echo "Error: " ${USAGE}
    exit 1
fi

if [ ! -d ${OUTPUTDIR} ] ; then 
    mkdir ${OUTPUTDIR}
fi

if [ ! -d ${OUTPUTDIR} ] ; then 
    echo "Error: Unable to create directory ${OUTPUTDIR}"
    exit 2
fi

if [ "${FILETYPE}" != "partitionbase" -a "${FILETYPE}" != "partitionbeam" -a "${FILETYPE}" != "partitionrun" -a "${FILETYPE}" != "partitionbeamrun" -a "${FILETYPE}" != "trigger" -a "${FILETYPE}" != "triggerlist" -a "${FILETYPE}" != "component" -a "${FILETYPE}" != "detectorbase" -a "${FILETYPE}" != "detectorbeam" -a "${FILETYPE}" != "detectorrun" -a "${FILETYPE}" != "detectorbeamrun" -a "${FILETYPE}" != "ignorefile" -a "${FILETYPE}" != "globalsiteconfig" -a "${FILETYPE}" != "globalsiteconfigbeam" -a "${FILETYPE}" != "globalsiteconfigrun" -a "${FILETYPE}" != "globalsiteconfigbeamrun" -a "${FILETYPE}" != "partitionsiteconfig" -a "${FILETYPE}" != "partitionsiteconfigbeam" -a "${FILETYPE}" != "partitionsiteconfigrun" -a "${FILETYPE}" != "partitionsiteconfigbeamrun" -a "${FILETYPE}" != "partitionnodelist" -a "${FILETYPE}" != "partitionddllist" -a "${FILETYPE}" != "globalddllist" ] ; then
    echo "Error: " ${USAGE} "(Unknown specifier for -filetype): ${FILETYPE}" ;
    exit 1
fi

if [ "${FILETYPE}" = "detectorbase" -a -z "${DETECTORID}" ] ; then
  echo "Error: -snippetdetectorid needs to be passed for type detectorbase"
  exit 1
fi

if [ "${FILETYPE}" = "detectorbeam" -a -z "${DETECTORID}" ] ; then
  echo "Error: -snippetdetectorid needs to be passed for type detectorbeam"
  exit 1
fi

if [ "${FILETYPE}" = "detectorrun" -a -z "${DETECTORID}" ] ; then
  echo "Error: -snippetdetectorid needs to be passed for type detectorrun"
  exit 1
fi

if [ "${FILETYPE}" = "detectorbeamrun" -a -z "${DETECTORID}" ] ; then
  echo "Error: -snippetdetectorid needs to be passed for type detectorbeamrun"
  exit 1
fi

if [ "${FILETYPE}" = "component" -a -z "${DETECTORID}" ] ; then
  echo "Error: -snippetdetectorid needs to be passed for type component"
  exit 1
fi

if [ "${FILETYPE}" = "component" -a -z "${SNIPPETID}" ] ; then
  echo "Error: -id needs to be passed for type component"
  exit 1
fi

if [ "${FILETYPE}" = "triggerlist" -a -z "${SNIPPETID}" ] ; then
  echo "Error: -id needs to be passed for type triggerlist"
  exit 1
fi

if [ "${FILETYPE}" = "trigger" -a -z "${SNIPPETID}" ] ; then
  echo "Error: -id needs to be passed for type trigger"
  exit 1
fi

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------

INPUTFILENAME=""
OUTPUTFILENAME=""

# -- Check for allowed types --
# -----------------------------
ALLOWED_TYPES_FILE="${CHECKEDOUTREPOS}/partitions/${DETECTOR}/allowed_types"

if [ ! -f "${ALLOWED_TYPES_FILE}" ] ; then
  echo "Error: Input file ${ALLOWED_TYPES_FILE} does not exist"
  exit 1
fi

ALLOWED_TYPES=`cat ${ALLOWED_TYPES_FILE}`

# -- Get filenames for input/output --
# ------------------------------------

# -- Check for partition snippets, which can have different allowed types --
# --------------------------------------------------------------------------
case "${FILETYPE}" in
    "partitionbase") 
      INPUTFILENAME1="${CHECKEDOUTREPOS}/partitions/${DETECTOR}"
      INPUTFILENAME2="${DETECTOR}.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/Base-${DETECTOR}-${DETECTOR}.xml"
      ;;
    "partitionbeam") 
      INPUTFILENAME1="${CHECKEDOUTREPOS}/partitions/${DETECTOR}"
      INPUTFILENAME2="${DETECTOR}-${BEAMTYPE}.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/Base-${DETECTOR}-${BEAMTYPE}.xml"
      ;;
    "partitionrun") 
      INPUTFILENAME1="${CHECKEDOUTREPOS}/partitions/${DETECTOR}"
      INPUTFILENAME2="${DETECTOR}-${RUNTYPE}.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/Base-${DETECTOR}-${RUNTYPE}.xml"
      ;;
    "partitionbeamrun") 
      INPUTFILENAME1="${CHECKEDOUTREPOS}/partitions/${DETECTOR}"
      INPUTFILENAME2="${DETECTOR}-${BEAMTYPE}-${RUNTYPE}.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/Base-${DETECTOR}-${BEAMTYPE}-${RUNTYPE}.xml"
      ;;
    "trigger")
      INPUTFILENAME1="${CHECKEDOUTREPOS}/components/trigger"
      INPUTFILENAME2="${SNIPPETID}.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/Trigger-${SNIPPETID}.xml"
      ALLOWED_TYPES="${ALLOWED_TYPES} ."
      ;;
    "triggerlist")
      MENUID=`echo ${SNIPPETID}|tr abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ`
      INPUTFILENAME1="${CHECKEDOUTREPOS}/triggermenus"
      INPUTFILENAME2="${MENUID}"
      OUTPUTFILENAME="${OUTPUTDIR}/TriggerMenu-${MENUID}"
      ALLOWED_TYPES="${ALLOWED_TYPES} ."
      ;;

    "component")
      if [ -n "${DETECTORID}" ] ; then
	  INPUTFILENAME1="${CHECKEDOUTREPOS}/components/${DETECTORID}"
	  INPUTFILENAME2="${SNIPPETID}.xml"
	  OUTPUTFILENAME="${OUTPUTDIR}/Component-${DETECTORID}-${SNIPPETID}.xml"
      else
	  INPUTFILENAME1="${CHECKEDOUTREPOS}/components/ALICE"
	  INPUTFILENAME2="${SNIPPETID}.xml"
	  OUTPUTFILENAME="${OUTPUTDIR}/Component-ALICE-${SNIPPETID}.xml"
      fi
      ALLOWED_TYPES="${ALLOWED_TYPES} ."
      ;;
    "detectorbase")
      INPUTFILENAME1="${CHECKEDOUTREPOS}/detectors/${DETECTORID}"
      INPUTFILENAME2="${DETECTORID}.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/Detector-${DETECTORID}.xml"
      ALLOWED_TYPES="${ALLOWED_TYPES} ."
      ;;
    "detectorbeam")
      INPUTFILENAME1="${CHECKEDOUTREPOS}/detectors/${DETECTORID}"
      INPUTFILENAME2="${DETECTORID}-${BEAMTYPE}.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/Detector-${DETECTORID}-${BEAMTYPE}.xml"
      ALLOWED_TYPES="${ALLOWED_TYPES} ."
      ;;
    "detectorrun")
      INPUTFILENAME1="${CHECKEDOUTREPOS}/detectors/${DETECTORID}"
      INPUTFILENAME2="${DETECTORID}-${RUNTYPE}.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/Detector-${DETECTORID}-${RUNTYPE}.xml"
      ALLOWED_TYPES="${ALLOWED_TYPES} ."
      ;;
    "detectorbeamrun")
      INPUTFILENAME1="${CHECKEDOUTREPOS}/detectors/${DETECTORID}"
      INPUTFILENAME2="${DETECTORID}-${BEAMTYPE}-${RUNTYPE}.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/Detector-${DETECTORID}-${BEAMTYPE}-${RUNTYPE}.xml"
      ALLOWED_TYPES="${ALLOWED_TYPES} ."
      ;;   
    "ignorefile") 
      INPUTFILENAME1="${CHECKEDOUTREPOS}/partitions/${DETECTOR}"
      INPUTFILENAME2="ignorefile"
      OUTPUTFILENAME="${OUTPUTDIR}/ignorefile"
      ALLOWED_TYPES="${ALLOWED_TYPES} ." # ignorefile is also searched in partition top directory
      ;;
    "globalsiteconfig")
      INPUTFILENAME1="${CHECKEDOUTREPOS}/setup/"
      INPUTFILENAME2="globalsiteconfig.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/globalsiteconfig.xml"
      ALLOWED_TYPES="${ALLOWED_TYPES} ." # file is also searched in partition top directory
      ;;
    "globalsiteconfigbeam")
      INPUTFILENAME1="${CHECKEDOUTREPOS}/setup/"
      INPUTFILENAME2="globalsiteconfig-${BEAMTYPE}.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/globalsiteconfig-${BEAMTYPE}.xml"
      ALLOWED_TYPES="${ALLOWED_TYPES} ." # file is also searched in partition top directory
      ;;
    "globalsiteconfigrun")
      INPUTFILENAME1="${CHECKEDOUTREPOS}/setup/"
      INPUTFILENAME2="globalsiteconfig-${RUNTYPE}.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/globalsiteconfig-${RUNTYPE}.xml"
      ALLOWED_TYPES="${ALLOWED_TYPES} ." # file is also searched in partition top directory
      ;;
    "globalsiteconfigbeamrun")
      INPUTFILENAME1="${CHECKEDOUTREPOS}/setup/"
      INPUTFILENAME2="globalsiteconfig-${BEAMTYPE}-${RUNTYPE}.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/globalsiteconfig-${BEAMTYPE}-${RUNTYPE}.xml"
      ALLOWED_TYPES="${ALLOWED_TYPES} ." # file is also searched in partition top directory
      ;;
    "partitionsiteconfig")
      INPUTFILENAME1="${CHECKEDOUTREPOS}/partitions/${DETECTOR}"
      INPUTFILENAME2="partitionsiteconfig.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/partitionsiteconfig.xml"
      ALLOWED_TYPES="${ALLOWED_TYPES} ." # file is also searched in partition top directory
      ;;
    "partitionsiteconfigbeam")
      INPUTFILENAME1="${CHECKEDOUTREPOS}/partitions/${DETECTOR}"
      INPUTFILENAME2="partitionsiteconfig-${BEAMTYPE}.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/partitionsiteconfig-${BEAMTYPE}.xml"
      ALLOWED_TYPES="${ALLOWED_TYPES} ." # file is also searched in partition top directory
      ;;
    "partitionsiteconfigrun")
      INPUTFILENAME1="${CHECKEDOUTREPOS}/partitions/${DETECTOR}"
      INPUTFILENAME2="partitionsiteconfig-${RUNTYPE}.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/partitionsiteconfig-${RUNTYPE}.xml"
      ALLOWED_TYPES="${ALLOWED_TYPES} ." # file is also searched in partition top directory
      ;;
    "partitionsiteconfigbeamrun")
      INPUTFILENAME1="${CHECKEDOUTREPOS}/partitions/${DETECTOR}"
      INPUTFILENAME2="partitionsiteconfig-${BEAMTYPE}-${RUNTYPE}.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/partitionsiteconfig-${BEAMTYPE}-${RUNTYPE}.xml"
      ALLOWED_TYPES="${ALLOWED_TYPES} ." # file is also searched in partition top directory
      ;;
    "partitionnodelist")
      INPUTFILENAME1="${CHECKEDOUTREPOS}/partitions/${DETECTOR}"
      INPUTFILENAME2="partitionnodelist.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/partitionnodelist.xml"
      ALLOWED_TYPES="${ALLOWED_TYPES} ." # file is also searched in partition top directory
      ;;
    "partitionddllist")
      INPUTFILENAME1="${CHECKEDOUTREPOS}/partitions/${DETECTOR}"
      INPUTFILENAME2="partitionddllist.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/partitionddllist.xml"
      ALLOWED_TYPES="${ALLOWED_TYPES} ." # file is also searched in partition top directory
      ;;
    "globalddllist")
      INPUTFILENAME1="${CHECKEDOUTREPOS}/setup/"
      INPUTFILENAME2="globalddllist.xml"
      OUTPUTFILENAME="${OUTPUTDIR}/globalddllist.xml"
      ALLOWED_TYPES="${ALLOWED_TYPES} ." # file is also searched in partition top directory
      ;;
esac

# -- Check if partitionbase exists, otherwise exit 0
INPUTFILENAME=""
SEARCHED_FILES=""

for TYPE in ${ALLOWED_TYPES} ; do
    SEARCHED_FILES="${SEARCHED_FILES} ${INPUTFILENAME1}/${TYPE}/${INPUTFILENAME2}"
    if [ -f "${INPUTFILENAME1}/${TYPE}/${INPUTFILENAME2}" ] ; then
        INPUTFILENAME="${INPUTFILENAME1}/${TYPE}/${INPUTFILENAME2}"
        break
    fi
done

if [ ! -f "${INPUTFILENAME}" ] ; then
    MATCH=`expr "${FILETYPE}" : ".*\(beam\|run\|partitionddllist\)"`
    if [ -z "${MATCH}" ] ; then
    #if [ "${FILETYPE}" != "partitionbeam" -a "${FILETYPE}" != "partitionrun" -a "${FILETYPE}" != "partitionbeamrun" -a "${FILETYPE}" != "detectorbeam" -a "${FILETYPE}" != "detectorrun" -a "${FILETYPE}" != "detectorbeamrun" -a "${FILETYPE}" != "partitionddllist" ] ; then
        echo "Error: ${FILETYPE} input file ${INPUTFILENAME2} could not be found in either searched location out of ${SEARCHED_FILES}"
        exit 1
    else
        exit 0
    fi
fi

####################### DEBUG ######################
# echo >"${OUTPUTDIR}/.debug.retrieve-config.snippets.sh"
echo INPUT: "${INPUTFILENAME}" >>"${OUTPUTDIR}/.debug.retrieve-config.snippets.sh"
echo OUTPUT: "${OUTPUTFILENAME}" >>"${OUTPUTDIR}/.debug.retrieve-config.snippets.sh"
echo FILETYPE: "${FILETYPE}" >>"${OUTPUTDIR}/.debug.retrieve-config.snippets.sh"
####################### DEBUG ######################

# -- Check if all files are commited
DIFF=`svn diff "${INPUTFILENAME}"`
if [ -n "${DIFF}" ] ; then
    IGNOREFILE="${CHECKEDOUTREPOS}/partitions/${DETECTOR}/allow-local-modifications"
    if [ -e "${IGNOREFILE}" ] ; then 
	log -i "WARNING: File ${INPUTFILENAME} has local modifications. Allowing modifications because file ${IGNOREFILE} exists"
    else
        echo "Error: File ${INPUTFILENAME} has local modifications. Please commit or revert and try again."
	exit 3
    fi
fi

echo ${INPUTFILENAME} ${OUTPUTFILENAME} >${OUTPUTDIR}/snippet-List
cp ${INPUTFILENAME} ${OUTPUTFILENAME}
echo ${OUTPUTFILENAME}
