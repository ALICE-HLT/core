# buildforkernels macro hint: when you build a new version or a new release
# that contains bugfixes or other improvements then you must disable the
# "buildforkernels newest" macro for just that build; immediately after
# queuing that build enable the macro again for subsequent builds; that way
# a new akmod package will only get build when a new one is actually needed
%global buildforkernels current

# Tweak to have debuginfo - part 1/2
%if 0%{?fedora} > 7
%define __debug_install_post %{_builddir}/%{?buildsubdir}/find-debuginfo.sh %{_builddir}/%{?buildsubdir}\
%{nil}
%endif

Name:        catalyst-kmod
Version:     14.301.1010
Release:     1%{?dist}
# Taken over by kmodtool
Summary:     AMD display driver kernel module
Group:       System Environment/Kernel
License:     Redistributable, no modification permitted
URL:         http://ati.amd.com/support/drivers/linux/linux-radeon.html
#This file is created during prepare stage of associated xorg-x11-drv-catalyst package
#Please take it from there
Source0:     catalyst-kmod-data-%{version}.tar.bz2
Source11:    catalyst-kmodtool-excludekernel-filterfile
Patch0:      compat_alloc-Makefile.patch
Patch1:      3-17_kernel.patch
Patch2:      caps_nox.patch

BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# needed for plague to make sure it builds for i686
ExclusiveArch:  i686 x86_64

# get the needed BuildRequires (in parts depending on what we build for)
BuildRequires:  %{_bindir}/kmodtool
%{!?kernels:BuildRequires: buildsys-build-rpmfusion-kerneldevpkgs-%{?buildforkernels:%{buildforkernels}}%{!?buildforkernels:current}-%{_target_cpu} }
# kmodtool does its magic here
%{expand:%(kmodtool --target %{_target_cpu} --repo rpmfusion --kmodname %{name} --filterfile %{SOURCE11} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null) }

%description
The catalyst %{version} display driver kernel module.


%prep
# error out if there was something wrong with kmodtool
%{?kmodtool_check}
# print kmodtool output for debugging purposes:
kmodtool  --target %{_target_cpu} --repo rpmfusion --kmodname %{name} --filterfile %{SOURCE11} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null
%setup -q -c -T -a 0

# Tweak to have debuginfo - part 2/2
%if 0%{?fedora} > 7
cp -p %{_prefix}/lib/rpm/find-debuginfo.sh .
sed -i -e 's|strict=true|strict=false|' find-debuginfo.sh
%endif

mkdir fglrxpkg
%ifarch %{ix86}
cp -r fglrx/common/* fglrx/arch/x86/* fglrxpkg/
%endif

%ifarch x86_64
cp -r fglrx/common/* fglrx/arch/x86_64/* fglrxpkg/
%endif

# proper permissions
find fglrxpkg/lib/modules/fglrx/build_mod/ -type f -print0 | xargs -0 chmod 0644

# debuginfo fix
#sed -i -e 's|strip -g|/bin/true|' fglrxpkg/lib/modules/fglrx/build_mod/make.sh

pushd fglrxpkg
%patch0 -p0
%patch1 -p0
%patch2 -p0
popd

for kernel_version  in %{?kernel_versions} ; do
    cp -a fglrxpkg/  _kmod_build_${kernel_version%%___*}
done


%build
for kernel_version in %{?kernel_versions}; do
    pushd _kmod_build_${kernel_version%%___*}/lib/modules/fglrx/build_mod/2.6.x
    make V=1 CC="gcc" PAGE_ATTR_FIX=0 \
      KVER="${kernel_version%%___*}" \
      KDIR="/usr/src/kernels/${kernel_version%%___*}"
    popd
done


%install
rm -rf $RPM_BUILD_ROOT
for kernel_version in %{?kernel_versions}; do
    install -D -m 0755 _kmod_build_${kernel_version%%___*}/lib/modules/fglrx/build_mod/2.6.x/fglrx.ko $RPM_BUILD_ROOT%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/fglrx.ko
done
%{?akmod_install}


%clean
rm -rf $RPM_BUILD_ROOT


%changelog
* Thu Jan 30 2014 Leigh Scott <leigh123linux@googlemail.com> - 13.9-3
- patch for 3.13.0 kernel

* Thu Jan 30 2014 Nicolas Chauvet <kwizart@gmail.com> - 13.9-2.14
- Rebuilt for kernel

* Tue Jan 28 2014 Nicolas Chauvet <kwizart@gmail.com> - 13.9-2.13
- Rebuilt for kernel

* Fri Jan 17 2014 Nicolas Chauvet <kwizart@gmail.com> - 13.9-2.12
- Rebuilt for kernel

* Sun Jan 12 2014 Nicolas Chauvet <kwizart@gmail.com> - 13.9-2.11
- Rebuilt for kernel

* Wed Dec 25 2013 Nicolas Chauvet <kwizart@gmail.com> - 13.9-2.10
- Rebuilt for kernel

* Fri Dec 20 2013 Nicolas Chauvet <kwizart@gmail.com> - 13.9-2.9
- Rebuilt for kernel

* Tue Dec 03 2013 Nicolas Chauvet <kwizart@gmail.com> - 13.9-2.8
- Rebuilt for kernel

* Thu Nov 21 2013 Nicolas Chauvet <kwizart@gmail.com> - 13.9-2.7
- Rebuilt for kernel

* Thu Nov 14 2013 Nicolas Chauvet <kwizart@gmail.com> - 13.9-2.6
- Rebuilt for kernel

