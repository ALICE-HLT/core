Begin
for c in (select trigger_name from user_triggers where trigger_name like 'DEV4_%') loop
execute immediate ('drop trigger '||c.trigger_name);
end loop;
End;
/

delete from DEV4_EGROUP;
delete from DEV4_PRIVILEGE2MEMBER;
delete from DEV4_ROLE;
delete from DEV4_EXPANDED_PRIVILEGE;
delete from DEV4_GENERIC_PRIVILEGE;
delete from DEV4_BOARD;
delete from DEV4_CLUSTER2INSTITUTE;
delete from DEV4_CLUSTER;
delete from DEV4_PROJECT;
delete from DEV4_THESIS;
delete from DEV4_WORKINGGROUP;
delete from DEV4_EMPLOYMENT;
delete from DEV4_EMPLOY_CATEGORY;
delete from DEV4_ALICEASSIGNED_TL;
delete from DEV4_INSTITUTE;
delete from DEV4_FA;
delete from DEV4_ADDRESS;
delete from DEV4_SERVICETASK2MEMBER;
delete from DEV4_MEMBER;
delete from DEV4_SERVICETASK;
delete from DEV4_ENTITY;

commit;

insert into DEV4_ENTITY select * from alice_ENTITY@acdbdevlink;
insert into dev4_member select * from alice_member@acdbdevlink;
insert into DEV4_ADDRESS select * from alice_ADDRESS@acdbdevlink;
insert into dev4_fa select * from alice_fa@acdbdevlink;
insert into dev4_institute select * from alice_institute@acdbdevlink;
insert into dev4_employ_category select * from alice_employ_category@acdbdevlink;
insert into dev4_employment select * from alice_employment@acdbdevlink;
insert into dev4_cluster select * from alice_cluster@acdbdevlink;
insert into dev4_cluster2institute select * from alice_cluster2institute@acdbdevlink;
insert into dev4_project select * from alice_project@acdbdevlink;
insert into dev4_workinggroup select * from alice_workinggroup@acdbdevlink;
insert into dev4_board select * from alice_board@acdbdevlink;
insert into dev4_thesis select * from alice_thesis@acdbdevlink;
insert into DEV4_EXPANDED_PRIVILEGE select * from alice_EXPANDED_PRIVILEGE@acdbdevlink;
insert into DEV4_GENERIC_PRIVILEGE select * from alice_GENERIC_PRIVILEGE@acdbdevlink;
insert into DEV4_ALICEASSIGNED_TL select * from alice_ALICEASSIGNED_TL@acdbdevlink;
insert into DEV4_SERVICETASK select * from alice_SERVICETASK@acdbdevlink;
insert into DEV4_SERVICETASK2MEMBER select * from alice_SERVICETASK2MEMBER@acdbdevlink;
insert into DEV4_ROLE select * from alice_ROLE@acdbdevlink;

set constraints DEV4_PRIV2MEMB_PRIV_FK, DEV4_EGROUP_PRIVILEGE_FK deferred;
exec DBMS_MVIEW.REFRESH('DEV4_V_ASSIGNABLE_PRIVILEGE');
set constraints DEV4_PRIV2MEMB_PRIV_FK, DEV4_EGROUP_PRIVILEGE_FK immediate;

insert into dev4_privilege2member select * from alice_privilege2member@acdbdevlink;
insert into dev4_egroup select * from alice_egroup@acdbdevlink;

commit;
drop sequence DEV4_ID_SEQ;

create sequence DEV4_ID_SEQ
start with 2000000
increment by 1
nomaxvalue;


CREATE OR REPLACE TRIGGER DEV4_TRG_PROJECT_INSERT
BEFORE INSERT
   ON DEV4_PROJECT
   FOR EACH ROW
BEGIN
    insert into dev4_entity(id,type) values(:new.id, 'PROJECT');
END;
/

CREATE OR REPLACE TRIGGER DEV4_TRG_BOARD_INSERT
BEFORE INSERT
   ON DEV4_BOARD
   FOR EACH ROW
BEGIN
    insert into dev4_entity(id,type) values(:new.id, 'BOARD');
END;
/

CREATE OR REPLACE TRIGGER DEV4_TRG_WORKINGGROUP_INSERT
BEFORE INSERT
   ON DEV4_WORKINGGROUP
   FOR EACH ROW
BEGIN
    insert into dev4_entity(id,type) values(:new.id, 'WG');
END;
/


CREATE OR REPLACE TRIGGER DEV4_TRG_CLUSTER_INSERT
BEFORE INSERT
   ON DEV4_CLUSTER
   FOR EACH ROW
BEGIN
    insert into dev4_entity(id,type) values(:new.id, :new.type);
END;
/


CREATE OR REPLACE TRIGGER DEV4_TRG_PROJECT_DELETE
BEFORE DELETE
   ON DEV4_PROJECT
   FOR EACH ROW
BEGIN
    delete from dev4_entity where id = :old.id and type = 'PROJECT';
END;
/

CREATE OR REPLACE TRIGGER DEV4_TRG_BOARD_DELETE
BEFORE DELETE
   ON DEV4_BOARD
   FOR EACH ROW
BEGIN
    delete from dev4_entity where id = :old.id and type = 'BOARD';
END;
/

CREATE OR REPLACE TRIGGER DEV4_TRG_WORKINGGROUP_DELETE
BEFORE DELETE
   ON DEV4_WORKINGGROUP
   FOR EACH ROW
BEGIN
    delete from dev4_entity where id = :old.id and type = 'WG';
END;
/

CREATE OR REPLACE TRIGGER DEV4_TRG_CLUSTER_DELETE
BEFORE DELETE
   ON DEV4_CLUSTER
   FOR EACH ROW
BEGIN
    delete from dev4_entity where id = :old.id and type = :old.type;
END;
/


CREATE OR REPLACE TRIGGER DEV4_TRG_EXPDPRIV_UPDATE
BEFORE UPDATE OF ID
   ON DEV4_EXPANDED_PRIVILEGE
BEGIN
   RAISE_APPLICATION_ERROR (-20002, 'Update of Column EXPANDED_PRIVILEGE.ID is not allowed. Transaction aborted.');
END;
/

CREATE OR REPLACE TRIGGER DEV4_TRG_ROLE_UPDATE
BEFORE UPDATE OF ID
   ON DEV4_ROLE
BEGIN
   RAISE_APPLICATION_ERROR (-20002, 'Update of Column ROLE.ID is not allowed. Transaction aborted.');
END;
/

CREATE OR REPLACE TRIGGER DEV4_TRG_EXPDPRIV_DELETE
BEFORE DELETE
   ON DEV4_EXPANDED_PRIVILEGE
   FOR EACH ROW
DECLARE
   temp number;
BEGIN
   select 1 into temp from dual where not exists (select 1 from DEV4_PRIVILEGE2MEMBER where :old.id = privilege
   union select 1 from DEV4_EGROUP where :old.id = privilege union select 1 from DEV4_ROLE where :old.id = privilege);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN RAISE_APPLICATION_ERROR (-20001, 'The privilege is still referenced by records in other tables. Delete transaction aborted.');
END;
/

CREATE OR REPLACE TRIGGER DEV4_TRG_ROLE_DELETE
BEFORE DELETE
   ON DEV4_ROLE
   FOR EACH ROW
DECLARE
   temp number;
BEGIN
   select 1 into temp from dual where not exists (select 1 from DEV4_PRIVILEGE2MEMBER where :old.id = privilege
   union select 1 from DEV4_EGROUP where :old.id = privilege);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN RAISE_APPLICATION_ERROR (-20001, 'The privilege is still referenced by records in other tables. Delete transaction aborted.');
END;
/

commit;
quit


