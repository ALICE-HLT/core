#!/usr/bin/env perl
use strict;
use warnings FATAL => 'all';

my $par_file = 'migrate.par';
my $table = $ARGV[0];

# List of tables to copy
#my @tables = qw(
#    $table
#    );

# Disable oracle constraints and triggers (if needed)
#`sqlplus 'acdb/alice.1234@(DESCRIPTION=(ADDRESS= (PROTOCOL=TCP) (HOST=pdbr1-s.cern.ch) (PORT=10121) )(ADDRESS= (PROTOCOL=TCP) (HOST=pdbr2-s.cern.ch) (PORT=10121) )(LOAD_BALANCE=on)(ENABLE=BROKEN)(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=pdbr.cern.ch)))' \@disable_constraints.sql`;
print "Step1\n";

# Copy the tables
#foreach my $table (@tables) {
#    `perl ./pg_2_sqlloader.pl $table`;
    sqlload($table, $par_file);
#}

print "Step2\n";
# Enable oracle triggers and constraints (if needed)
#`sqlplus 'acdb/alice.1234@(DESCRIPTION=(ADDRESS= (PROTOCOL=TCP) (HOST=pdbr1-s.cern.ch) (PORT=10121) )(ADDRESS= (PROTOCOL=TCP) (HOST=pdbr2-s.cern.ch) (PORT=10121) )(LOAD_BALANCE=on)(ENABLE=BROKEN)(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=pdbr.cern.ch)))' \@enable_constraints.sql`;

sub sqlload {
    my ($table, $par_file) = @_;
    my $ctl_file = "${table}.ctl";
    my $log_file = "${table}.log";
    warn("  Loading $ctl_file...");
    system(
        "sqlldr rows=500 readsize=4000000 bindsize=4000000 control=$ctl_file log=$log_file parfile=$par_file"
        );
}
print "Step3\n";
__END__
