--empty ACDB1 tables

delete from CITY;
delete from COUNTRY;
--delete from EGROUP;
--delete from EMEMBER;
delete from EMPLOYMENT;
delete from FA;
delete from INSTITUTE;
delete from MEMBER;
delete from MNOGROUP;
delete from MNOGROUP2INSTITUTE;
delete from PWG;
--delete from ROLE;
--delete from ROLEUSER;
delete from THESIS;

--empty ACDB2 tables

Begin
for c in (select trigger_name from user_triggers) loop
execute immediate ('drop trigger '||c.trigger_name);
end loop;
End;
/

--drop trigger DEV4_TRG_WORKINGGROUP_DELETE;
--drop trigger DEV4_TRG_BOARD_DELETE;
--drop trigger DEV4_TRG_PROJECT_DELETE;
--drop trigger DEV4_TRG_CLUSTER_DELETE;
--drop trigger DEV4_TRG_WORKINGGROUP_INSERT;
--drop trigger DEV4_TRG_BOARD_INSERT;
--drop trigger DEV4_TRG_PROJECT_INSERT;
--drop trigger DEV4_TRG_CLUSTER_INSERT;
--drop trigger DEV4_TRG_EXPDPRIV_DELETE;
--drop trigger DEV4_TRG_ROLE_DELETE;
--drop trigger DEV4_TRG_EXPDPRIV_UPDATE;
--drop trigger DEV4_TRG_ROLE_UPDATE;

delete from DEV4_EGROUP;
delete from DEV4_PRIVILEGE2MEMBER;
delete from DEV4_ROLE;
delete from DEV4_EXPANDED_PRIVILEGE;
delete from DEV4_GENERIC_PRIVILEGE;
delete from DEV4_BOARD;
delete from DEV4_CLUSTER2INSTITUTE;
delete from DEV4_CLUSTER;
delete from DEV4_PROJECT;
delete from DEV4_THESIS;
delete from DEV4_WORKINGGROUP;
delete from DEV4_EMPLOYMENT;
delete from DEV4_EMPLOY_CATEGORY;
delete from DEV4_ALICEASSIGNED_TL;
delete from DEV4_INSTITUTE;
delete from DEV4_FA;
delete from DEV4_ADDRESS;
delete from DEV4_SERVICETASK2MEMBER;
delete from DEV4_MEMBER;
delete from DEV4_SERVICETASK;
delete from DEV4_ENTITY;

exec DBMS_MVIEW.REFRESH('DEV4_V_ASSIGNABLE_PRIVILEGE');

commit;
drop sequence DEV4_ID_SEQ;

create sequence DEV4_ID_SEQ
start with 2000000
increment by 1
nomaxvalue;

quit
