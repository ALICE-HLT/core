#!/bin/bash

# prepare environment

test=dump_`date '+%Y-%m-%d'`
rm -rf ./$test 
mkdir $test 1>/dev/null 2>&1
export PGDATABASE=acdb4 1>/dev/null 2>&1
export PGPASSWORD=ahphoh5Oweetaip0 1>/dev/null 2>&1
cp migrate.par.prod ./$test/migrate.par 1>/dev/null 2>&1
cp pg2oracle.pl ./$test 1>/dev/null 2>&1
cp pg_2_sqlloader.pl ./$test 1>/dev/null 2>&1
cp prodDataInsertion.sql ./$test 1>/dev/null 2>&1
cp prodEmptyTables.sql ./$test 1>/dev/null 2>&1
#cp prodTableCreation.sql ./$test 1>/dev/null 2>&1
cd $test 1>/dev/null 2>&1


echo "Environment prepared"

#if [ "$1" = "withTables" ]; then
#sqlplus 'alice_glance/alice.1234@(DESCRIPTION=(ADDRESS= (PROTOCOL=TCP) (HOST=pdb2-v.cern.ch) (PORT=10121) )(ADDRESS= (PROTOCOL=TCP) (HOST=pdb1-v.cern.ch) (PORT=10121) )(LOAD_BALANCE=off)(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=pdb_cerndb1.cern.ch)))' @prodTableCreation.sql > ./cleanProdTables.log
#echo "ACDB2 Tables in PROD Schema created"
#fi

# extract postgres data
./pg_2_sqlloader.pl country 1>>./pgExtraction.log 2>&1
./pg_2_sqlloader.pl city 1>>./pgExtraction.log 2>&1
./pg_2_sqlloader.pl member 1>>./pgExtraction.log 2>&1
./pg_2_sqlloader.pl fa 1>>./pgExtraction.log 2>&1
./pg_2_sqlloader.pl institute 1>>./pgExtraction.log 2>&1
./pg_2_sqlloader.pl mnogroup 1>>./pgExtraction.log 2>&1
./pg_2_sqlloader.pl mnogroup2institute 1>>./pgExtraction.log 2>&1
./pg_2_sqlloader.pl thesis 1>>./pgExtraction.log 2>&1
./pg_2_sqlloader.pl employment 1>>./pgExtraction.log 2>&1
./pg_2_sqlloader.pl pwg 1>>./pgExtraction.log 2>&1


echo "ACDB1 data from Postgres extracted"

# migrate from acdb1 tables to acdb2 tables
sqlplus 'alice_glance/alice.1234@(DESCRIPTION=(ADDRESS= (PROTOCOL=TCP) (HOST=pdb2-v.cern.ch) (PORT=10121) )(ADDRESS= (PROTOCOL=TCP) (HOST=pdb1-v.cern.ch) (PORT=10121) )(LOAD_BALANCE=off)(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=pdb_cerndb1.cern.ch)))' @prodEmptyTables.sql >> ./cleanProdTables.log

echo "ACDB1 Tables and ACDB2 Tables in PROD Schema emptied"

# import to oracle
./pg2oracle.pl country 1>./pgImport2Oracle.log 2>&1
./pg2oracle.pl city 1>>./pgImport2Oracle.log 2>&1
./pg2oracle.pl member 1>>./pgImport2Oracle.log 2>&1
./pg2oracle.pl fa 1>>./pgImport2Oracle.log 2>&1
./pg2oracle.pl institute 1>>./pgImport2Oracle.log 2>&1
./pg2oracle.pl mnogroup 1>>./pgImport2Oracle.log 2>&1
./pg2oracle.pl mnogroup2institute 1>./pgImport2Oracle.log 2>&1
./pg2oracle.pl thesis 1>>./pgImport2Oracle.log 2>&1
./pg2oracle.pl employment 1>>./pgImport2Oracle.log 2>&1
./pg2oracle.pl pwg 1>>./pgImport2Oracle.log 2>&1

echo "ACDB1 Tables in PROD Schema filled"

# migrate from acdb1 tables to acdb2 tables
sqlplus 'alice_glance/alice.1234@(DESCRIPTION=(ADDRESS= (PROTOCOL=TCP) (HOST=pdb2-v.cern.ch) (PORT=10121) )(ADDRESS= (PROTOCOL=TCP) (HOST=pdb1-v.cern.ch) (PORT=10121) )(LOAD_BALANCE=off)(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=pdb_cerndb1.cern.ch)))' @prodDataInsertion.sql > ./Postgres2Prod.log 

echo "ACDB2 Tables in PROD Schema filled"

echo "Done."

echo "Please check ./"$test"/cleanProdTables.log and"
echo "             ./"$test"/Postgres2Prod.log for potential errors."


