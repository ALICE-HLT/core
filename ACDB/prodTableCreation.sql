CREATE TABLE T_MEMBER
    (
     CCID NUMBER(7) NOT NULL ,
     LTX_NAME VARCHAR2(100) ,
     INITIALS VARCHAR2(50) ,
     PHOTO VARCHAR2(150) ,
     FOOTNOTE VARCHAR2(4000) ,
     COMMENTS VARCHAR2(4000),
     INSPIRE VARCHAR2(25),
     DECEASED CHAR(1) DEFAULT 'N',
     LASTDEGREE_DATE DATE,
     CONSTRAINT T_MEMBER_PK PRIMARY KEY ( CCID )
);

ALTER TABLE T_MEMBER
ADD CONSTRAINT T_MEMBER_DECEASED_CHECK
 CHECK (DECEASED IN ('Y', 'N'));

CREATE TABLE T_FA
(
     ID NUMBER NOT NULL ,
     NAME VARCHAR2(200) NOT NULL,
     LTX_NAME VARCHAR2(250),
     SHORT_NAME VARCHAR2(100),
     ADDRESS VARCHAR2(2000),
     MOU CHAR(1) DEFAULT 'N',
     MOUREF VARCHAR2(100),
     CONTACT_PERSON VARCHAR2(100),
     CONTACT_EMAIL VARCHAR2(100),
     COMMENTS VARCHAR2(4000),
     CONSTRAINT T_FA_PK PRIMARY KEY (ID)
);

ALTER TABLE T_FA
ADD CONSTRAINT T_FA_MOU_CHECK
 CHECK (MOU IN ('Y', 'N'));

ALTER TABLE T_FA
ADD CONSTRAINT T_FA_MOUREF_CHECK
 CHECK (NOT(MOU = 'Y' and MOUREF is NULL));


CREATE TABLE T_ADDRESS
(
    ID NUMBER NOT NULL,
    CITY VARCHAR2(100),
    COUNTRY VARCHAR2(100),
    FULL_ADDRESS VARCHAR2(4000),
    WEBPAGE VARCHAR2(200),
    CONSTRAINT T_ADDRESS_PK PRIMARY KEY (ID)
);

CREATE TABLE T_INSTITUTE
(
     ID NUMBER NOT NULL,
     CODE NUMBER,
     FA NUMBER,
     LTX_NAME VARCHAR2(250),
     SHORT_NAME VARCHAR2(50),
     STATUS VARCHAR2(30) NOT NULL,
     ADDRESS NUMBER,
     COMMENTS VARCHAR2(4000),
     NAME VARCHAR2(250) NOT NULL,
     INSPIRE_NAME VARCHAR2(150),
     INSPIRE_DOMAIN VARCHAR2(150),
     CONSTRAINT T_INSTITUTE_PK PRIMARY KEY (ID)
);

ALTER TABLE T_INSTITUTE
ADD CONSTRAINT T_INSTITUTE_FA_FK FOREIGN KEY (FA)
REFERENCES T_FA (ID);

ALTER TABLE T_INSTITUTE
ADD CONSTRAINT T_INSTITUTE_ADDRESS_FK FOREIGN KEY (ADDRESS)
REFERENCES T_ADDRESS (ID);

ALTER TABLE T_INSTITUTE
ADD CONSTRAINT T_INSTITUTE_STATUS_CHECK
 CHECK ( STATUS IN ('ASSOCIATE', 'CANDIDATE', 'FULL', 'SUSPENDED', 'NON', 'CLUSTERED'));

CREATE TABLE T_EMPLOY_CATEGORY
(
     ID NUMBER(2) NOT NULL,
     NAME VARCHAR2(50) NOT NULL,
     CONSTRAINT T_EMPLOY_CATEGORY_PK PRIMARY KEY (ID)
);


CREATE TABLE T_EMPLOYMENT
(
     ID NUMBER NOT NULL,
     MEMBER NUMBER(7) NOT NULL,
     INSTITUTE NUMBER NOT NULL,
     STARTDATE DATE not null,
     ENDDATE DATE,
     CATEGORY NUMBER(2) not null,
     MNO CHAR(1) NOT NULL,
     RANK NUMBER(1) default 1,
     CONSTRAINT T_EMPLOYMENT_PK PRIMARY KEY (ID),
     CONSTRAINT T_EMPLOYMENT_UQ UNIQUE (MEMBER, INSTITUTE, STARTDATE)
);

ALTER TABLE T_EMPLOYMENT
ADD CONSTRAINT T_EMPLOYMENT_MEMBER_FK FOREIGN KEY (MEMBER)
REFERENCES T_MEMBER (CCID);

ALTER TABLE T_EMPLOYMENT
ADD CONSTRAINT T_EMPLOYMENT_INSTITUTE_FK FOREIGN KEY (INSTITUTE)
REFERENCES T_INSTITUTE (ID);

ALTER TABLE T_EMPLOYMENT
ADD CONSTRAINT T_EMPLOYMENT_CATEGORY_FK FOREIGN KEY (CATEGORY)
REFERENCES T_EMPLOY_CATEGORY (ID);

ALTER TABLE T_EMPLOYMENT
ADD CONSTRAINT T_EMPLOYMENT_MNO_CHECK
 CHECK (MNO IN ('Y', 'N'));

CREATE TABLE T_ASSIGNED_TL
(
     ID NUMBER NOT NULL,
     MEMBER NUMBER(7) NOT NULL,
     INSTITUTE NUMBER NOT NULL,
     CONSTRAINT T_ASSIGNED_TL_PK PRIMARY KEY (ID),
     CONSTRAINT T_ASSIGNED_TL_UQ UNIQUE (MEMBER, INSTITUTE)
);

ALTER TABLE T_ASSIGNED_TL
ADD CONSTRAINT T_ASSIGNED_TL_MEMBER_FK FOREIGN KEY (MEMBER)
REFERENCES T_MEMBER (CCID);

ALTER TABLE T_ASSIGNED_TL
ADD CONSTRAINT T_ASSIGNED_TL_INSTITUTE_FK FOREIGN KEY (INSTITUTE)
REFERENCES T_INSTITUTE (ID);


create table T_ENTITY
(
     ID NUMBER NOT NULL,
     TYPE VARCHAR2(15) NOT NULL,
     CONSTRAINT T_ENTITY_PK PRIMARY KEY (ID, TYPE)
);

ALTER TABLE T_ENTITY
ADD CONSTRAINT T_ENTITY_TYPE_CHECK
 CHECK (TYPE IN ('BOARD', 'WG', 'PROJECT', 'MNOGROUP', 'FUNDING', 'VOTING', 'INITIAL'));


CREATE TABLE T_CLUSTER
(
     ID NUMBER NOT NULL ,
     TYPE VARCHAR2(15) DEFAULT 'MNOGROUP',
     NAME VARCHAR2(100),
     LTX_NAME VARCHAR2(250),
     SHORT_NAME VARCHAR2(50),
     COMMENTS VARCHAR2(4000),
     STATUS VARCHAR2(30),
     CONSTRAINT T_CLUSTER_PK PRIMARY KEY (ID)
);

ALTER TABLE T_CLUSTER
ADD CONSTRAINT T_CLUSTER_TYPE_CHECK
 CHECK (TYPE IN ('FUNDING', 'VOTING', 'INITIAL', 'MNOGROUP'));

ALTER TABLE T_CLUSTER
ADD CONSTRAINT T_CLUSTER_STATUS_CHECK
 CHECK ((TYPE = 'INITIAL' and STATUS in ('ASSOCIATE', 'CANDIDATE', 'FULL', 'SUSPENDED', 'NON')) or (STATUS is NULL));

ALTER TABLE T_CLUSTER
ADD CONSTRAINT T_CLUSTER_ID_TYPE_FK FOREIGN KEY (ID, TYPE)
 REFERENCES T_ENTITY (ID, TYPE);

CREATE TABLE T_CLUSTER2INSTITUTE
(
     ID NUMBER NOT NULL,
     CLUSTERID NUMBER NOT NULL,
     INSTITUTE NUMBER NOT NULL,
     STARTDATE DATE not null,
     ENDDATE DATE,
     COMMENTS VARCHAR2(4000),
     CONSTRAINT T_CLUST2INST_PK PRIMARY KEY (ID),	
     CONSTRAINT T_CLUST2INST_UQ UNIQUE (CLUSTERID, INSTITUTE, STARTDATE)
);

ALTER TABLE T_CLUSTER2INSTITUTE
ADD CONSTRAINT T_CLUST2INST_CLUSTERID_FK FOREIGN KEY (CLUSTERID)
REFERENCES T_CLUSTER (ID);

ALTER TABLE T_CLUSTER2INSTITUTE
ADD CONSTRAINT T_CLUST2INST_INSTITUTE_FK FOREIGN KEY (INSTITUTE)
REFERENCES T_INSTITUTE (ID);

create table T_GENERIC_PRIVILEGE
(
     ID NUMBER NOT NULL,
     ENTITY_TYPE VARCHAR2(15) NOT NULL,
     ABSTRACT_NAME VARCHAR2(50) NOT NULL,
     CONSTRAINT T_GENERICPRIVILEGE_PK PRIMARY KEY (ID),
     CONSTRAINT T_GENERICPRIVILEGE_UQ UNIQUE(ENTITY_TYPE, ABSTRACT_NAME)
);

alter table T_GENERIC_PRIVILEGE
ADD CONSTRAINT T_GENPRIV_TYPE_CHECK
CHECK (ENTITY_TYPE IN ('BOARD', 'WG', 'PROJECT', 'MNOGROUP', 'FUNDING', 'VOTING', 'INITIAL'));

create table T_EXPANDED_PRIVILEGE
(
     ID RAW(16) DEFAULT SYS_GUID() NOT NULL,
     ENTITY_ID NUMBER,
     ENTITY_TYPE VARCHAR2(15),
     ABSTRACT_NAME VARCHAR2(50) NOT NULL,
     ALTERNATIVE_NAME VARCHAR(100) NOT NULL,
     CONSTRAINT T_EXPPRIVILEGE_PK PRIMARY KEY (ID),
     CONSTRAINT T_EXPPRIVILEGE_UQ UNIQUE(ENTITY_ID, ENTITY_TYPE, ABSTRACT_NAME)
);

ALTER TABLE T_EXPANDED_PRIVILEGE
ADD CONSTRAINT T_EXPPRIV_IDTYPE_FK FOREIGN KEY (ENTITY_ID, ENTITY_TYPE)
 REFERENCES T_ENTITY (ID, TYPE);

ALTER TABLE T_EXPANDED_PRIVILEGE
ADD CONSTRAINT T_EXPPRIV_IDTYPE_CHECK 
CHECK ((ENTITY_ID is not null and ENTITY_TYPE is not null) OR (ENTITY_ID is null and ENTITY_TYPE is null));


-- create triggers: after update of expanded_privilege, T_role exec DBMS_MVIEW.REFRESH('T_V_ASSIGNABLE_PRIVILEGE');
-- if we want roles to have a name, then an extra table T_role is needed, consequently the table below needs to be rename to role2privilege (which 
-- better describes its semantics)

create table T_ROLE
(
     ID RAW(16) DEFAULT SYS_GUID() NOT NULL,
     PRIVILEGE RAW(16) NOT NULL,
     CONSTRAINT T_ROLE_ID_PK PRIMARY KEY (ID, PRIVILEGE)
);

ALTER TABLE T_ROLE
ADD CONSTRAINT T_ROLE_PRIV_FK FOREIGN KEY (PRIVILEGE)
 REFERENCES T_EXPANDED_PRIVILEGE (ID);



--create or replace function varchar2hash(p_string in varchar2) return number is
--     v_hash_value_md5    raw (100);
--     v_varchar_key_md5   varchar2 (32);
--begin
--    v_hash_value_md5 := dbms_crypto.hash(src => utl_raw.cast_to_raw(p_string) ,typ => dbms_crypto.hash_md5);
--    select to_char(rawtohex (v_hash_value_md5))
--      into v_varchar_key_md5
--      from dual;
--   return to_number(v_varchar_key_md5,'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX');
--end;
--/



create or replace view V_ASSIGNABLE_PRIVILEGE as
select a.id, a.entity_id, a.entity_type, a.abstract_name, a.alternative_name from T_expanded_privilege a where not exists (select 1 from T_ROLE b where a.id = b.privilege)
union
select b.id, null, null, null, LISTAGG(a.alternative_name, ' and ') within group (order by a.alternative_name) as alternative_name from T_expanded_privilege a, T_role b where b.privilege = a.id group by b.id
minus select null, null, null, null, null from dual;


--alter table T_v_ASSIGNABLE_privilege 
--  add CONSTRAINT T_ASSIGNABLE_PRIV_ID_UQ UNIQUE(ID); 


create table T_PRIVILEGE2MEMBER
(
     ID NUMBER NOT NULL,
     MEMBER NUMBER(7) NOT NULL,
     PRIVILEGE RAW(16) NOT NULL,
     STARTDATE DATE NOT NULL,
     ENDDATE DATE,
     CONSTRAINT T_PRIVILEGE2MEMBER_PK PRIMARY KEY (ID),
     CONSTRAINT T_PRIVILEGE2MEMBER_UQ UNIQUE (MEMBER, PRIVILEGE, STARTDATE)
);

ALTER TABLE T_PRIVILEGE2MEMBER
ADD CONSTRAINT T_PRIV2MEMB_MEMBER_FK FOREIGN KEY (MEMBER)
 REFERENCES T_MEMBER (CCID);
 
-- ALTER TABLE T_PRIVILEGE2MEMBER
-- ADD CONSTRAINT T_PRIV2MEMB_PRIV_FK FOREIGN KEY (PRIVILEGE)
-- REFERENCES T_V_ASSIGNABLE_PRIVILEGE (ID) DEFERRABLE;
--

create table T_EGROUP
(
     ID NUMBER NOT NULL,
     NAME VARCHAR2(50),
     MEMBER NUMBER(7),
     PRIVILEGE RAW(16),
     VIEWNAME VARCHAR(50),
     CONSTRAINT T_EGROUP_PK PRIMARY KEY (ID)
);

ALTER TABLE T_EGROUP
ADD CONSTRAINT T_EGROUP_MEMBER_FK FOREIGN KEY (MEMBER)
 REFERENCES T_MEMBER (CCID);

--ALTER TABLE T_EGROUP
--ADD CONSTRAINT T_EGROUP_PRIVILEGE_FK FOREIGN KEY (PRIVILEGE)
-- REFERENCES T_V_ASSIGNABLE_PRIVILEGE (ID) DEFERRABLE;
--

ALTER TABLE T_EGROUP
ADD CONSTRAINT T_EGROUP_NOTNULL_CHECK
 CHECK (NOT(MEMBER is NULL and PRIVILEGE is NULL and VIEWNAME is null));


create table T_WORKINGGROUP
(
     ID NUMBER NOT NULL,
     TYPE VARCHAR(15) DEFAULT 'WG',
     NAME VARCHAR2(150) NOT NULL,
     DESCRIPTION VARCHAR2(4000),
     SUBTYPE VARCHAR2(20),
     ABBREVIATION VARCHAR2(10),
     COMMENTS VARCHAR2(4000),
     CONSTRAINT T_WORKINGGROUP_PK PRIMARY KEY (ID)
);

ALTER TABLE T_WORKINGGROUP
ADD CONSTRAINT T_WG_SUBTYPE_CHECK
 CHECK (SUBTYPE IN ('PHYSICS', 'COMPUTING', 'NONE'));

ALTER TABLE T_WORKINGGROUP
ADD CONSTRAINT T_WG_TYPE_CHECK
 CHECK (TYPE IN ('WG'));

ALTER TABLE T_WORKINGGROUP
ADD CONSTRAINT T_WG_ID_TYPE_FK FOREIGN KEY (ID, TYPE)
 REFERENCES T_ENTITY (ID, TYPE);


create table T_THESIS
(
     ID NUMBER NOT NULL,
     MEMBER NUMBER(7) NOT NULL,
     TITLE VARCHAR2(1024) NOT NULL,
     WG NUMBER(2) NOT NULL,
     SUPERVISOR NUMBER(7),
     EXTERNAL_SV VARCHAR2(100),
     STARTDATE DATE not null,
     ENDDATE DATE,
     DISCARDED CHAR(1) DEFAULT 'N',
     COMMENTS VARCHAR2(4000),
     CONSTRAINT T_THESIS_PK PRIMARY KEY (ID),
     CONSTRAINT T_THESIS_UQ UNIQUE (MEMBER, STARTDATE)
);

ALTER TABLE T_THESIS
ADD CONSTRAINT T_THESIS_DISCARDED_CHECK
 CHECK ((DISCARDED = 'Y' and ENDDATE is not NULL) or DISCARDED = 'N');

ALTER TABLE T_THESIS
ADD CONSTRAINT T_THESIS_MEMBER_FK FOREIGN KEY (MEMBER)
REFERENCES T_MEMBER (CCID);

ALTER TABLE T_THESIS
ADD CONSTRAINT T_THESIS_SUPERVISOR_FK FOREIGN KEY (SUPERVISOR)
REFERENCES T_MEMBER (CCID);

ALTER TABLE T_THESIS
ADD CONSTRAINT T_THESIS_WG_FK FOREIGN KEY (WG)
REFERENCES T_WORKINGGROUP (ID);



create table T_PROJECT
(
     ID NUMBER NOT NULL,
     TYPE VARCHAR(15) DEFAULT 'PROJECT',
     NAME VARCHAR2(150) NOT NULL,
     DESCRIPTION VARCHAR2(4000),
     ABBREVIATION VARCHAR2(10),
     COMMENTS VARCHAR2(4000),
     CONSTRAINT T_PROJECT_PK PRIMARY KEY (ID)
);

ALTER TABLE T_PROJECT
ADD CONSTRAINT T_PROJECT_TYPE_CHECK
 CHECK (TYPE IN ('PROJECT'));

ALTER TABLE T_PROJECT
ADD CONSTRAINT T_PROJECT_ID_TYPE_FK FOREIGN KEY (ID, TYPE)
REFERENCES T_ENTITY (ID, TYPE);

create table T_BOARD
(
     ID NUMBER NOT NULL,
     TYPE VARCHAR(15) DEFAULT 'BOARD',
     NAME VARCHAR2(150) NOT NULL,
     DESCRIPTION VARCHAR2(4000),
     ABBREVIATION VARCHAR2(10),
     COMMENTS VARCHAR2(4000),
     CONSTRAINT T_BOARD_PK PRIMARY KEY (ID)
);

ALTER TABLE T_BOARD
ADD CONSTRAINT T_BOARD_TYPE_CHECK
 CHECK (TYPE IN ('BOARD'));

ALTER TABLE T_BOARD
ADD CONSTRAINT T_BOARD_ID_TYPE_FK FOREIGN KEY (ID, TYPE)
REFERENCES T_ENTITY (ID, TYPE);


create table T_SERVICETASK
(
     ID NUMBER NOT NULL,
     ENTITYID NUMBER,
     ENTITYTYPE VARCHAR2(15) NOT NULL,
     CODE VARCHAR2(50),
     DEFINITION  VARCHAR2(2000),
     ACTION VARCHAR2(10),
     FTE NUMBER(3,2),
     CONSTRAINT T_SERVICETASK_PK PRIMARY KEY (ID)
);

ALTER TABLE T_SERVICETASK
ADD CONSTRAINT T_ENTITY_ID_TYPE_FK FOREIGN KEY (ENTITYID, ENTITYTYPE)
REFERENCES T_ENTITY (ID, TYPE);

ALTER TABLE T_SERVICETASK
ADD CONSTRAINT T_SERVICETASK_ACTION_CHECK
 CHECK (ACTION in ('UPGRADE', 'MO'));


-- Basic formula: per SV:     sum(all assigned workshare(SV) * (enddate - startdate)/(6 monate)) =< FTE(SV),
--                per person: sum(all assigned workshare(SV) * (enddate - startdate)/(6 monate)) = 1

create table T_SERVICETASK2MEMBER
(
     ID NUMBER NOT NULL,
     SERVICETASK NUMBER NOT NULL,
     MEMBER NUMBER(7) NOT NULL,
     STARTDATE DATE NOT NULL,
     ENDDATE DATE NOT NULL,
     WORKSHARE NUMBER,
     CONSTRAINT T_MEMBER2SERVICETASK_PK PRIMARY KEY (ID), 
     CONSTRAINT T_MEMBER2SERVICETASK_UQ UNIQUE (SERVICETASK, MEMBER, STARTDATE)
);

ALTER TABLE T_SERVICETASK2MEMBER
ADD CONSTRAINT T_SV2MEMBER_SV_FK FOREIGN KEY (SERVICETASK)
REFERENCES T_SERVICETASK (ID);

ALTER TABLE T_SERVICETASK2MEMBER
ADD CONSTRAINT T_SV2MEMBER_MEMBER_FK FOREIGN KEY (MEMBER)
REFERENCES T_MEMBER (CCID);

-- for illustrational purposes

create or replace view V_MEMBER as
select b.*, a.ltx_name, a.initials, a.photo, a.footnote
from  T_MEMBER a, foundation_pub.alice_persons b
where a.ccid = b.person_id
with read only;

create or replace view V_EMPLOYMENT as
select a.ccid, b.last_name, b.first_name, c.name as institute, d.startdate, d.enddate, e.name as category, d.mno
from T_MEMBER a, foundation_pub.alice_persons b, T_INSTITUTE c, T_EMPLOYMENT d, T_employ_category e
where a.ccid = b.person_id and a.ccid = d.member and d.institute = c.id and e.id = d.category
with read only;

create or replace view V_INSTITUTE as
select a.name as institute, a.status, c.name as Funding_Agency, b.city, b.country
from T_institute a, T_address b, T_fa c
where a.fa = c.id and a.address = b.id
with read only;

create or replace view V_TEAMLEADER as
select a.name as institute, b.city, e.first_name ||' '|| e.last_name as Teamleader
from T_institute a, T_address b, FOUNDATION_PUB.expanded_roles d, foundation_pub.alice_persons e
where a.id not in (select institute from T_assigned_tl) and b.id = a.address and a.code = d.entity2_id (+)
and d.role_type (+) = 'TL' and d.entity1_id (+) = 'ALICE' and d.person_id = e.person_id (+)
union
select a.name as institute, b.city, c.first_name ||' '|| c.last_name as Teamleader
from T_institute a, T_address b, T_assigned_tl d, foundation_pub.alice_persons c
where a.id = d.institute and b.id = a.address and d.member = c.person_id
with read only;

create or replace view V_CLUSTER as
select a.name as institute_cluster, a.type, b.name as institute from T_cluster a, T_institute b, T_cluster2institute c
where a.id = c.clusterid and b.id = c.institute
order by a.type asc, a.name desc
with read only;

----------------

-- views for EGroups based on non-trivial properties (i.e. which cannot be derived from privilege and role assignment)
-- these views should not be queried manually but are used automatically when issueing: select distinct(member) from table(retrieveEgroupMembers(>>EGroupname<<));   
-- the function retrieveEgroupMembers(>>EGroupname<<) does not filter duplicates

create or replace view V_ALL_EG as
select a.ccid as member from T_member a where exists (select 'X' from T_employment b where a.ccid = b.member and (b.enddate >= sysdate or b.enddate is null));

create or replace view V_TEAMLEADER_EG as
select member from (
select e.person_id as member from T_institute a, FOUNDATION_PUB.expanded_roles d, foundation_pub.alice_persons e
where a.id not in (select institute from T_assigned_tl) and a.code = d.entity2_id and (d.role_type = 'TL' or d.role_type = 'DTL') and d.entity1_id = 'ALICE' and d.person_id = e.person_id
union
select d.member as member from T_assigned_tl d);


CREATE OR REPLACE TYPE T_membertable IS TABLE OF number(7)
/

CREATE OR REPLACE FUNCTION retrieveEgroupMembers(egroupname IN VARCHAR2)
RETURN T_membertable AS t_members T_membertable;
TYPE refcursor is ref cursor;
egview varchar2(50);
rc refcursor;
BEGIN
execute immediate 'select c.member from T_egroup a, v_assignable_privilege b, T_privilege2member c where a.name = '||CHR(39)||egroupname||CHR(39)||' and a.privilege = b.id and c.privilege = b.id and a.viewname is null and c.startdate <= sysdate and (c.enddate >= sysdate or c.enddate is null) union all select member from T_egroup where name = '||CHR(39)||egroupname||CHR(39)||' and member is not null' bulk collect into t_members;
open rc for 'select viewname from T_egroup where name = '||CHR(39)||egroupname||CHR(39)||' and viewname is not null';
loop
fetch rc into egview;
EXIT WHEN rc%NOTFOUND;
if egview is not null
then execute immediate 'select member from V_' || egview || '_EG' bulk collect into t_members;
end if;
end loop;
close rc;
return t_members;
END;
/


commit;
quit
