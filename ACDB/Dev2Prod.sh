#!/bin/bash

test=dump_`date '+%Y-%m-%d'`
mkdir $test 1>/dev/null 2>&1
cp migrateDataFromDEVToPROD.sql ./$test/
cd $test 1>/dev/null 2>&1

sqlplus 'alice_glance/alice.1234@(DESCRIPTION=(ADDRESS= (PROTOCOL=TCP) (HOST=pdb2-v.cern.ch) (PORT=10121) )(ADDRESS= (PROTOCOL=TCP) (HOST=pdb1-v.cern.ch) (PORT=10121) )(LOAD_BALANCE=off)(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=pdb_cerndb1.cern.ch)))' @migrateDataFromDEVToPROD.sql > ./Dev2Prod.log

echo "Done."
echo "Please check ./"$test"/Dev2Prod.log for potential errors."


