-- this script adds history functionality to existing acdb.v2 tables

--ALICE_ROLE;
--ALICE_CLUSTER2INSTITUTE;

--ALICE_ADDRESS

alter table alice_ADDRESS add AGENT NUMBER(7);
update alice_ADDRESS set agent = 666996;
alter table alice_ADDRESS modify AGENT NOT NULL;


CREATE TABLE ALICE_ADDRESS_HIST
(
    ID NUMBER NOT NULL,
    AGENT NUMBER(7) NOT NULL,
    SINCE DATE NOT NULL,
    UP_TO DATE,
    WHAT_CHANGED NUMBER(10),
    CITY VARCHAR2(100),
    COUNTRY VARCHAR2(100),
    FULL_ADDRESS VARCHAR2(4000),
    WEBPAGE VARCHAR2(200),
    CONSTRAINT ALICE_ADDRESS_HIST_PK PRIMARY KEY (ID, SINCE)
);

CREATE OR REPLACE TRIGGER ALICE_TRG_ADDRESS_HIST
AFTER INSERT OR UPDATE OR DELETE ON ALICE_ADDRESS
FOR EACH ROW
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.CITY  <> :OLD.CITY)
        OR (:NEW.CITY IS NULL AND :OLD.CITY IS NOT NULL)
        OR (:NEW.CITY IS NOT NULL AND :OLD.CITY IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF ((:NEW.COUNTRY <> :OLD.COUNTRY)
        OR (:NEW.COUNTRY IS NULL AND :OLD.COUNTRY IS NOT NULL)
        OR (:NEW.COUNTRY IS NOT NULL AND :OLD.COUNTRY IS NULL)) THEN
        change_composition:=change_composition+power(2,1);
    END IF;
    IF ((:NEW.FULL_ADDRESS  <> :OLD.FULL_ADDRESS)
        OR (:NEW.FULL_ADDRESS IS NULL AND :OLD.FULL_ADDRESS IS NOT NULL)
        OR (:NEW.FULL_ADDRESS IS NOT NULL AND :OLD.FULL_ADDRESS IS NULL)) THEN
        change_composition:=change_composition+power(2,2);
    END IF;
    IF ((:NEW.webpage <> :OLD.webpage)
        OR (:NEW.WEBPAGE IS NULL AND :OLD.WEBPAGE IS NOT NULL)
        OR (:NEW.WEBPAGE IS NOT NULL AND :OLD.WEBPAGE IS NULL)) THEN
        change_composition:=change_composition+power(2,3);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE alice_address_hist SET up_to = operation_date WHERE up_to IS NULL AND id = :OLD.id;
        IF :NEW.id IS NOT NULL THEN
            INSERT INTO alice_address_hist
            VALUES (
            :NEW.id, :NEW.agent,
            operation_date, null, change_composition,
            :NEW.city, :NEW.country, :NEW.full_address, :NEW.webpage
            );
        ELSE
            INSERT INTO alice_address_hist
            VALUES (
            :OLD.id, :OLD.agent,
            operation_date, operation_date, change_composition,
            :old.city, :old.country, :old.full_address, :old.webpage
            );
        END IF;
    END IF;
END;
/



--ALICE_ASSIGNED_TL;

alter table alice_ASSIGNED_TL add AGENT NUMBER(7);
update alice_ASSIGNED_TL set agent = 666996;
alter table alice_ASSIGNED_TL modify AGENT NOT NULL;

CREATE TABLE ALICE_ASSIGNED_TL_HIST
(
     ID NUMBER NOT NULL,
     AGENT NUMBER(7) NOT NULL,
     SINCE DATE NOT NULL,
     UP_TO DATE,
     WHAT_CHANGED NUMBER(10),
     MEMBER NUMBER(7) NOT NULL,
     INSTITUTE NUMBER NOT NULL,
     CONSTRAINT ALICE_ASSIGNED_TL_HIST_PK PRIMARY KEY (ID, SINCE)
);

CREATE OR REPLACE TRIGGER ALICE_TRG_ASSIGNED_TL_HIST
AFTER INSERT OR UPDATE OR DELETE ON ALICE_ASSIGNED_TL
FOR EACH ROW
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.MEMBER  <> :OLD.MEMBER)
        OR (:NEW.MEMBER IS NULL AND :OLD.MEMBER IS NOT NULL)
        OR (:NEW.MEMBER IS NOT NULL AND :OLD.MEMBER IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF ((:NEW.INSTITUTE <> :OLD.INSTITUTE)
        OR (:NEW.INSTITUTE IS NULL AND :OLD.INSTITUTE IS NOT NULL)
        OR (:NEW.INSTITUTE IS NOT NULL AND :OLD.INSTITUTE IS NULL)) THEN
        change_composition:=change_composition+power(2,1);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE alice_ASSIGNED_TL_hist SET up_to = operation_date WHERE up_to IS NULL AND id = :OLD.id;
        IF :NEW.id IS NOT NULL THEN
            INSERT INTO alice_ASSIGNED_TL_hist
            VALUES (
            :NEW.id, :NEW.agent,
            operation_date, null, change_composition,
            :NEW.member, :NEW.institute
            );
        ELSE
            INSERT INTO alice_ASSIGNED_TL_hist
            VALUES (
            :OLD.id, :OLD.agent,
            operation_date, operation_date, change_composition,
            :OLD.member, :OLD.institute
            );
        END IF;
    END IF;
END;
/




--ALICE_PRIVILEGE2MEMBER;

alter table alice_PRIVILEGE2MEMBER add AGENT NUMBER(7);
update alice_PRIVILEGE2MEMBER set agent = 666996;
alter table alice_PRIVILEGE2MEMBER modify AGENT NOT NULL;


create table ALICE_PRIVILEGE2MEMBER_HIST
(
     ID NUMBER NOT NULL,
     AGENT NUMBER(7) NOT NULL,
     SINCE DATE NOT NULL,
     UP_TO DATE,
     WHAT_CHANGED NUMBER(10),
     MEMBER NUMBER(7) NOT NULL,
     PRIVILEGE RAW(16) NOT NULL,
     STARTDATE DATE NOT NULL,
     ENDDATE DATE,
     CONSTRAINT ALICE_PRIV2MEMB_HIST_PK PRIMARY KEY (ID, SINCE)
);

CREATE OR REPLACE TRIGGER ALICE_TRG_PRIV2MEMB_HIST
AFTER INSERT OR UPDATE OR DELETE ON ALICE_PRIVILEGE2MEMBER
FOR EACH ROW
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.MEMBER  <> :OLD.MEMBER)
        OR (:NEW.MEMBER IS NULL AND :OLD.MEMBER IS NOT NULL)
        OR (:NEW.MEMBER IS NOT NULL AND :OLD.MEMBER IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF ((:NEW.PRIVILEGE <> :OLD.PRIVILEGE)
        OR (:NEW.PRIVILEGE IS NULL AND :OLD.PRIVILEGE IS NOT NULL)
        OR (:NEW.PRIVILEGE IS NOT NULL AND :OLD.PRIVILEGE IS NULL)) THEN
        change_composition:=change_composition+power(2,1);
    END IF;
    IF ((:NEW.STARTDATE <> :OLD.STARTDATE)
        OR (:NEW.STARTDATE IS NULL AND :OLD.STARTDATE IS NOT NULL)
        OR (:NEW.STARTDATE IS NOT NULL AND :OLD.STARTDATE IS NULL)) THEN
        change_composition:=change_composition+power(2,2);
    END IF;
    IF ((:NEW.ENDDATE  <> :OLD.ENDDATE)
        OR (:NEW.ENDDATE IS NULL AND :OLD.ENDDATE IS NOT NULL)
        OR (:NEW.ENDDATE IS NOT NULL AND :OLD.ENDDATE IS NULL)) THEN
        change_composition:=change_composition+power(2,3);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE alice_PRIVILEGE2MEMBER_hist SET up_to = operation_date WHERE up_to IS NULL AND id = :OLD.id;
        IF :NEW.id IS NOT NULL THEN
            INSERT INTO alice_PRIVILEGE2MEMBER_hist
            VALUES (
            :NEW.id, :NEW.agent,
            operation_date, null, change_composition,
            :NEW.member, :NEW.privilege, :NEW.startdate, :NEW.enddate
            );
        ELSE
            INSERT INTO alice_PRIVILEGE2MEMBER_hist
            VALUES (
            :OLD.id, :OLD.agent,
            operation_date, operation_date, change_composition,
            :OLD.member, :OLD.privilege, :old.startdate, :old.enddate
            );
        END IF;
    END IF;
END;
/





--ALICE_EXPANDED_PRIVILEGE

alter table alice_EXPANDED_PRIVILEGE add AGENT NUMBER(7);
update alice_EXPANDED_PRIVILEGE set agent = 666996;
alter table alice_EXPANDED_PRIVILEGE modify AGENT NOT NULL;


create table ALICE_EXPPRIVILEGE_HIST
(
     ID RAW(16) DEFAULT SYS_GUID() NOT NULL,
     AGENT NUMBER(7) NOT NULL,
     SINCE DATE NOT NULL,
     UP_TO DATE,
     WHAT_CHANGED NUMBER(10),
     ENTITY_ID NUMBER,
     ENTITY_TYPE VARCHAR2(15),
     ABSTRACT_NAME VARCHAR2(50) NOT NULL,
     ALTERNATIVE_NAME VARCHAR(100) NOT NULL,
     CONSTRAINT ALICE_EXPPRIVILEGE_HIST_PK PRIMARY KEY (ID, SINCE)
);

CREATE OR REPLACE TRIGGER ALICE_TRG_EXPPRIVILEGE_HIST
AFTER INSERT OR UPDATE OR DELETE ON ALICE_EXPANDED_PRIVILEGE
FOR EACH ROW
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.ENTITY_ID <> :OLD.ENTITY_ID)
        OR (:NEW.ENTITY_ID IS NULL AND :OLD.ENTITY_ID IS NOT NULL)
        OR (:NEW.ENTITY_ID IS NOT NULL AND :OLD.ENTITY_ID IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF ((:NEW.ENTITY_TYPE <> :OLD.ENTITY_TYPE)
        OR (:NEW.ENTITY_TYPE IS NULL AND :OLD.ENTITY_TYPE IS NOT NULL)
        OR (:NEW.ENTITY_TYPE IS NOT NULL AND :OLD.ENTITY_TYPE IS NULL)) THEN
        change_composition:=change_composition+power(2,1);
    END IF;
    IF ((:NEW.ABSTRACT_NAME <> :OLD.ABSTRACT_NAME)
        OR (:NEW.ABSTRACT_NAME IS NULL AND :OLD.ABSTRACT_NAME IS NOT NULL)
        OR (:NEW.ABSTRACT_NAME IS NOT NULL AND :OLD.ABSTRACT_NAME IS NULL)) THEN
        change_composition:=change_composition+power(2,2);
    END IF;
     IF ((:NEW.ALTERNATIVE_NAME <> :OLD.ALTERNATIVE_NAME)
        OR (:NEW.ALTERNATIVE_NAME IS NULL AND :OLD.ALTERNATIVE_NAME IS NOT NULL)
        OR (:NEW.ALTERNATIVE_NAME IS NOT NULL AND :OLD.ALTERNATIVE_NAME IS NULL)) THEN
        change_composition:=change_composition+power(2,3);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE alice_expprivilege_hist SET up_to = operation_date WHERE up_to IS NULL AND id = :OLD.id;
        IF :NEW.id IS NOT NULL THEN
            INSERT INTO alice_expprivilege_hist
            VALUES (
            :NEW.id, :NEW.agent,
            operation_date, null, change_composition,
            :NEW.entity_id, :NEW.entity_type, :NEW.ABSTRACT_NAME, :NEW.ALTERNATIVE_NAME
            );
        ELSE
            INSERT INTO alice_expprivilege_hist
            VALUES (
            :OLD.id, :OLD.agent,
            operation_date, operation_date, change_composition,
            :OLD.entity_id, :OLD.entity_type, :old.ABSTRACT_NAME, :old.ALTERNATIVE_NAME
            );
        END IF;
    END IF;
END;
/




--ALICE_THESIS

alter table alice_thesis add AGENT NUMBER(7);
update alice_thesis set agent = 666996;
alter table alice_thesis modify AGENT NOT NULL;

create table ALICE_THESIS_HIST
(
     ID NUMBER NOT NULL,
     AGENT NUMBER(7) NOT NULL,
     SINCE DATE NOT NULL,
     UP_TO DATE,
     WHAT_CHANGED NUMBER(10),
     MEMBER NUMBER(7) NOT NULL,
     TITLE VARCHAR2(1024) NOT NULL,
     WG NUMBER(2) NOT NULL,
     SUPERVISOR NUMBER(7),
     EXTERNAL_SV VARCHAR2(100),
     STARTDATE DATE not null,
     ENDDATE DATE,
     DISCARDED CHAR(1) DEFAULT 'N',
     CONSTRAINT ALICE_THESIS_HIST_PK PRIMARY KEY (ID, SINCE)
);

CREATE OR REPLACE TRIGGER ALICE_TRG_THESIS_HIST
AFTER INSERT OR UPDATE OR DELETE ON ALICE_THESIS
FOR EACH ROW
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.MEMBER <> :OLD.MEMBER)
        OR (:NEW.MEMBER IS NULL AND :OLD.MEMBER IS NOT NULL)
        OR (:NEW.MEMBER IS NOT NULL AND :OLD.MEMBER IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF ((:NEW.TITLE <> :OLD.TITLE)
        OR (:NEW.TITLE IS NULL AND :OLD.TITLE IS NOT NULL)
        OR (:NEW.TITLE IS NOT NULL AND :OLD.TITLE IS NULL)) THEN
        change_composition:=change_composition+power(2,1);
    END IF;
    IF ((:NEW.WG <> :OLD.WG)
        OR (:NEW.WG IS NULL AND :OLD.WG IS NOT NULL)
        OR (:NEW.WG IS NOT NULL AND :OLD.WG IS NULL)) THEN
        change_composition:=change_composition+power(2,2);
    END IF;
    IF ((:NEW.SUPERVISOR <> :OLD.SUPERVISOR)
        OR (:NEW.SUPERVISOR IS NULL AND :OLD.SUPERVISOR IS NOT NULL)
        OR (:NEW.SUPERVISOR IS NOT NULL AND :OLD.SUPERVISOR IS NULL)) THEN
        change_composition:=change_composition+power(2,3);
    END IF;
    IF ((:NEW.EXTERNAL_SV <> :OLD.EXTERNAL_SV)
        OR (:NEW.EXTERNAL_SV IS NULL AND :OLD.EXTERNAL_SV IS NOT NULL)
        OR (:NEW.EXTERNAL_SV IS NOT NULL AND :OLD.EXTERNAL_SV IS NULL)) THEN
        change_composition:=change_composition+power(2,4);
    END IF;
    IF ((:NEW.STARTDATE <> :OLD.STARTDATE)
        OR (:NEW.STARTDATE IS NULL AND :OLD.STARTDATE IS NOT NULL)
        OR (:NEW.STARTDATE IS NOT NULL AND :OLD.STARTDATE IS NULL)) THEN
        change_composition:=change_composition+power(2,5);
    END IF;
    IF ((:NEW.ENDDATE <> :OLD.ENDDATE)
        OR (:NEW.ENDDATE IS NULL AND :OLD.ENDDATE IS NOT NULL)
        OR (:NEW.ENDDATE IS NOT NULL AND :OLD.ENDDATE IS NULL)) THEN
        change_composition:=change_composition+power(2,6);
    END IF;
    IF ((:NEW.DISCARDED <> :OLD.DISCARDED)
        OR (:NEW.DISCARDED IS NULL AND :OLD.DISCARDED IS NOT NULL)
        OR (:NEW.DISCARDED IS NOT NULL AND :OLD.DISCARDED IS NULL)) THEN
        change_composition:=change_composition+power(2,7);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE alice_thesis_hist SET up_to = operation_date WHERE up_to IS NULL AND id = :OLD.id;
        IF :NEW.id IS NOT NULL THEN
            INSERT INTO alice_thesis_hist
            VALUES (
            :NEW.id, :NEW.agent,
            operation_date, null, change_composition,
            :NEW.member, :NEW.title, :NEW.wg, :NEW.supervisor, :NEW.external_sv, :NEW.startdate, :NEW.enddate, :NEW.discarded
            );
        ELSE
            INSERT INTO alice_thesis_hist
            VALUES (
            :OLD.id, :OLD.agent,
            operation_date, operation_date, change_composition,
            :OLD.member, :OLD.title, :old.wg, :old.supervisor, :old.external_sv, :old.startdate, :old.enddate, :old.discarded
            );
        END IF;
    END IF;
END;
/



--ALICE_EGROUP

alter table alice_egroup add AGENT NUMBER(7);
update alice_egroup set agent = 666996;
alter table alice_egroup modify AGENT NOT NULL;

create table ALICE_EGROUP_HIST
(
     ID NUMBER NOT NULL,
     AGENT NUMBER(7) NOT NULL,
     SINCE DATE NOT NULL,
     UP_TO DATE,
     WHAT_CHANGED NUMBER(10),
     NAME VARCHAR2(50),
     MEMBER NUMBER(7),
     PRIVILEGE RAW(16),
     VIEWNAME VARCHAR(50),
     CONSTRAINT ALICE_EGROUP_HIST_PK PRIMARY KEY (ID, SINCE)
);

CREATE OR REPLACE TRIGGER ALICE_TRG_EGROUP_HIST
AFTER INSERT OR UPDATE OR DELETE ON ALICE_EGROUP
FOR EACH ROW
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.NAME <> :OLD.NAME)
        OR (:NEW.NAME IS NULL AND :OLD.NAME IS NOT NULL)
        OR (:NEW.NAME IS NOT NULL AND :OLD.NAME IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF ((:NEW.MEMBER <> :OLD.MEMBER)
        OR (:NEW.MEMBER IS NULL AND :OLD.MEMBER IS NOT NULL)
        OR (:NEW.MEMBER IS NOT NULL AND :OLD.MEMBER IS NULL)) THEN
        change_composition:=change_composition+power(2,1);
    END IF;
    IF ((:NEW.PRIVILEGE <> :OLD.PRIVILEGE)
        OR (:NEW.PRIVILEGE IS NULL AND :OLD.PRIVILEGE IS NOT NULL)
        OR (:NEW.PRIVILEGE IS NOT NULL AND :OLD.PRIVILEGE IS NULL)) THEN
        change_composition:=change_composition+power(2,2);
    END IF;
    IF ((:NEW.VIEWNAME <> :OLD.VIEWNAME)
        OR (:NEW.VIEWNAME IS NULL AND :OLD.VIEWNAME IS NOT NULL)
        OR (:NEW.VIEWNAME IS NOT NULL AND :OLD.VIEWNAME IS NULL)) THEN
        change_composition:=change_composition+power(2,3);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE alice_egroup_hist SET up_to = operation_date WHERE up_to IS NULL AND id = :OLD.id;
        IF :NEW.id IS NOT NULL THEN
            INSERT INTO alice_egroup_hist
            VALUES (
            :NEW.id, :NEW.agent,
            operation_date, null, change_composition,
            :NEW.name, :NEW.member, :NEW.privilege, :NEW.viewname
            );
        ELSE
            INSERT INTO alice_egroup_hist
            VALUES (
            :OLD.id, :OLD.agent,
            operation_date, operation_date, change_composition,
            :OLD.name, :OLD.member, :OLD.privilege, :old.viewname
            );
        END IF;
    END IF;
END;
/



--ALICE_CLUSTER

alter table alice_cluster add AGENT NUMBER(7);
update alice_cluster set agent = 666996;
alter table alice_cluster modify AGENT NOT NULL;


CREATE TABLE ALICE_CLUSTER_HIST
(
     ID NUMBER NOT NULL ,
     AGENT NUMBER(7) NOT NULL,
     SINCE DATE NOT NULL,
     UP_TO DATE,
     WHAT_CHANGED NUMBER(10),
     TYPE VARCHAR2(15) DEFAULT 'MNOGROUP',
     NAME VARCHAR2(100),
     LTX_NAME VARCHAR2(250),
     SHORT_NAME VARCHAR2(50),
     STATUS VARCHAR2(30),
     CONSTRAINT ALICE_CLUSTER_HIST_PK PRIMARY KEY (ID, SINCE)
);


CREATE OR REPLACE TRIGGER ALICE_TRG_CLUSTER_HIST
AFTER INSERT OR UPDATE OR DELETE ON ALICE_CLUSTER
FOR EACH ROW
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.TYPE <> :OLD.TYPE)
        OR (:NEW.TYPE IS NULL AND :OLD.TYPE IS NOT NULL)
        OR (:NEW.TYPE IS NOT NULL AND :OLD.TYPE IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF ((:NEW.NAME <> :OLD.NAME)
        OR (:NEW.NAME IS NULL AND :OLD.NAME IS NOT NULL)
        OR (:NEW.NAME IS NOT NULL AND :OLD.NAME IS NULL)) THEN
        change_composition:=change_composition+power(2,1);
    END IF;
    IF ((:NEW.LTX_NAME <> :OLD.LTX_NAME)
        OR (:NEW.LTX_NAME IS NULL AND :OLD.LTX_NAME IS NOT NULL)
        OR (:NEW.LTX_NAME IS NOT NULL AND :OLD.LTX_NAME IS NULL)) THEN
        change_composition:=change_composition+power(2,2);
    END IF;
    IF ((:NEW.SHORT_NAME <> :OLD.SHORT_NAME)
        OR (:NEW.SHORT_NAME IS NULL AND :OLD.SHORT_NAME IS NOT NULL)
        OR (:NEW.SHORT_NAME IS NOT NULL AND :OLD.SHORT_NAME IS NULL)) THEN
        change_composition:=change_composition+power(2,3);
    END IF;
    IF ((:NEW.STATUS <> :OLD.STATUS)
        OR (:NEW.STATUS IS NULL AND :OLD.STATUS IS NOT NULL)
        OR (:NEW.STATUS IS NOT NULL AND :OLD.STATUS IS NULL)) THEN
        change_composition:=change_composition+power(2,4);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE alice_cluster_hist SET up_to = operation_date WHERE up_to IS NULL AND id = :OLD.id;
        IF :NEW.id IS NOT NULL THEN
            INSERT INTO alice_cluster_hist
            VALUES (
            :NEW.id, :NEW.agent,
            operation_date, null, change_composition,
            :NEW.type, :NEW.name, :NEW.ltx_name, :NEW.short_name, :NEW.status
            );
        ELSE
            INSERT INTO alice_cluster_hist
            VALUES (
            :OLD.id, :OLD.agent,
            operation_date, operation_date, change_composition,
            :old.type, :OLD.name, :OLD.ltx_name, :OLD.short_name, :old.status
            );
        END IF;
    END IF;
END;
/




--ALICE_WORKINGGROUP

alter table alice_WORKINGGROUP add AGENT NUMBER(7);
update alice_WORKINGGROUP set agent = 666996;
alter table alice_WORKINGGROUP modify AGENT NOT NULL;

create table ALICE_WORKINGGROUP_HIST
(
     ID NUMBER NOT NULL,
     AGENT NUMBER(7) NOT NULL,
     SINCE DATE NOT NULL,
     UP_TO DATE,
     WHAT_CHANGED NUMBER(10),
     NAME VARCHAR2(150) NOT NULL,
     SUBTYPE VARCHAR2(20),
     ABBREVIATION VARCHAR2(10),
     CONSTRAINT ALICE_WORKINGGROUP_HIST_PK PRIMARY KEY (ID, SINCE)
);

CREATE OR REPLACE TRIGGER ALICE_TRG_WORKINGGROUP_HIST
AFTER INSERT OR UPDATE OR DELETE ON ALICE_WORKINGGROUP
FOR EACH ROW
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.NAME <> :OLD.NAME)
        OR (:NEW.NAME IS NULL AND :OLD.NAME IS NOT NULL)
        OR (:NEW.NAME IS NOT NULL AND :OLD.NAME IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF ((:NEW.SUBTYPE <> :OLD.SUBTYPE)
        OR (:NEW.SUBTYPE IS NULL AND :OLD.SUBTYPE IS NOT NULL)
        OR (:NEW.SUBTYPE IS NOT NULL AND :OLD.SUBTYPE IS NULL)) THEN
        change_composition:=change_composition+power(2,1);
    END IF;
    IF ((:NEW.ABBREVIATION <> :OLD.ABBREVIATION)
        OR (:NEW.ABBREVIATION IS NULL AND :OLD.ABBREVIATION IS NOT NULL)
        OR (:NEW.ABBREVIATION IS NOT NULL AND :OLD.ABBREVIATION IS NULL)) THEN
        change_composition:=change_composition+power(2,2);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE alice_workinggroup_hist SET up_to = operation_date WHERE up_to IS NULL AND id = :OLD.id;
        IF :NEW.id IS NOT NULL THEN
            INSERT INTO alice_workinggroup_hist
            VALUES (
            :NEW.id, :NEW.agent,
            operation_date, null, change_composition,
            :NEW.name, :NEW.subtype, :NEW.abbreviation
            );
        ELSE
            INSERT INTO alice_workinggroup_hist
            VALUES (
            :OLD.id, :OLD.agent,
            operation_date, operation_date, change_composition,
            :OLD.name, :OLD.subtype, :OLD.abbreviation
            );
        END IF;
    END IF;
END;
/

--ALICE_PROJECT 

alter table alice_project add AGENT NUMBER(7);
update alice_project set agent = 666996;
alter table alice_project modify AGENT NOT NULL;

create table ALICE_PROJECT_HIST
(
     ID NUMBER NOT NULL,
     AGENT NUMBER(7) NOT NULL,
     SINCE DATE NOT NULL,
     UP_TO DATE,
     WHAT_CHANGED NUMBER(10),
     NAME VARCHAR2(150) NOT NULL,
     ABBREVIATION VARCHAR2(10),
     CONSTRAINT ALICE_PROJECT_HIST_PK PRIMARY KEY (ID,SINCE)
);


CREATE OR REPLACE TRIGGER ALICE_TRG_PROJECT_HIST
AFTER INSERT OR UPDATE OR DELETE ON ALICE_PROJECT
FOR EACH ROW
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.NAME <> :OLD.NAME)
        OR (:NEW.NAME IS NULL AND :OLD.NAME IS NOT NULL)
        OR (:NEW.NAME IS NOT NULL AND :OLD.NAME IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF ((:NEW.ABBREVIATION <> :OLD.ABBREVIATION)
        OR (:NEW.ABBREVIATION IS NULL AND :OLD.ABBREVIATION IS NOT NULL)
        OR (:NEW.ABBREVIATION IS NOT NULL AND :OLD.ABBREVIATION IS NULL)) THEN
        change_composition:=change_composition+power(2,1);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE alice_project_hist SET up_to = operation_date WHERE up_to IS NULL AND id = :OLD.id;
        IF :NEW.id IS NOT NULL THEN
            INSERT INTO alice_project_hist
            VALUES (
            :NEW.id, :NEW.agent,
            operation_date, null, change_composition,
            :NEW.name, :NEW.abbreviation
            );
        ELSE
            INSERT INTO alice_project_hist
            VALUES (
            :OLD.id, :OLD.agent,
            operation_date, operation_date, change_composition,
            :OLD.name, :OLD.abbreviation
            );
        END IF;
    END IF;
END;
/

--ALICE_BOARD

alter table alice_board add AGENT NUMBER(7);
update alice_board set agent = 666996;
alter table alice_board modify AGENT NOT NULL;

create table ALICE_BOARD_HIST
(
     ID NUMBER NOT NULL,
     
     NAME VARCHAR2(150) NOT NULL,
     AGENT NUMBER(7) NOT NULL,
     SINCE DATE NOT NULL,
     UP_TO DATE,
     WHAT_CHANGED NUMBER(10),
     ABBREVIATION VARCHAR2(10),
     CONSTRAINT ALICE_BOARD_HIST_PK PRIMARY KEY (ID, SINCE)
);

CREATE OR REPLACE TRIGGER ALICE_TRG_BOARD_HIST
AFTER INSERT OR UPDATE OR DELETE ON ALICE_BOARD
FOR EACH ROW
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.NAME <> :OLD.NAME)
        OR (:NEW.NAME IS NULL AND :OLD.NAME IS NOT NULL)
        OR (:NEW.NAME IS NOT NULL AND :OLD.NAME IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF ((:NEW.ABBREVIATION <> :OLD.ABBREVIATION)
        OR (:NEW.ABBREVIATION IS NULL AND :OLD.ABBREVIATION IS NOT NULL)
        OR (:NEW.ABBREVIATION IS NOT NULL AND :OLD.ABBREVIATION IS NULL)) THEN
        change_composition:=change_composition+power(2,1);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE alice_BOARD_hist SET up_to = operation_date WHERE up_to IS NULL AND id = :OLD.id;
        IF :NEW.id IS NOT NULL THEN
            INSERT INTO alice_BOARD_hist
            VALUES (
            :NEW.id, :NEW.agent,
            operation_date, null, change_composition,
            :NEW.name, :NEW.abbreviation
            );
        ELSE
            INSERT INTO alice_BOARD_hist
            VALUES (
            :OLD.id, :OLD.agent,
            operation_date, operation_date, change_composition,
            :OLD.name, :OLD.abbreviation
            );
        END IF;
    END IF;
END;
/



--ALICE_EMPLOYMENT;

alter table alice_employment add AGENT NUMBER(7);
update alice_employment set agent = 666996;
alter table alice_employment modify AGENT NOT NULL;


CREATE TABLE ALICE_EMPLOYMENT_HIST
(
     ID NUMBER NOT NULL,
     AGENT NUMBER(7) NOT NULL,
     SINCE DATE NOT NULL,
     UP_TO DATE,
     WHAT_CHANGED NUMBER(10),
     MEMBER NUMBER(7) NOT NULL,
     INSTITUTE NUMBER NOT NULL,
     STARTDATE DATE not null,
     ENDDATE DATE,
     CATEGORY NUMBER(2) not null,
     MNO CHAR(1) NOT NULL,
     RANK NUMBER(1) default 1,
     CONSTRAINT ALICE_EMPLOYMENT_HIST_PK PRIMARY KEY (ID, SINCE),
);

CREATE OR REPLACE TRIGGER ALICE_TRG_EMPLOYMENT_HIST
AFTER INSERT OR UPDATE OR DELETE ON ALICE_EMPLOYMENT
FOR EACH ROW
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.MEMBER  <> :OLD.MEMBER)
        OR (:NEW.MEMBER IS NULL AND :OLD.MEMBER IS NOT NULL)
        OR (:NEW.MEMBER IS NOT NULL AND :OLD.MEMBER IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF ((:NEW.INSTITUTE <> :OLD.INSTITUTE)
        OR (:NEW.INSTITUTE IS NULL AND :OLD.INSTITUTE IS NOT NULL)
        OR (:NEW.INSTITUTE IS NOT NULL AND :OLD.INSTITUTE IS NULL)) THEN
        change_composition:=change_composition+power(2,1);
    END IF;
    IF ((:NEW.STARTDATE <> :OLD.STARTDATE)
        OR (:NEW.STARTDATE IS NULL AND :OLD.STARTDATE IS NOT NULL)
        OR (:NEW.STARTDATE IS NOT NULL AND :OLD.STARTDATE IS NULL)) THEN
        change_composition:=change_composition+power(2,2);
    END IF;
    IF ((:NEW.ENDDATE  <> :OLD.ENDDATE)
        OR (:NEW.ENDDATE IS NULL AND :OLD.ENDDATE IS NOT NULL)
        OR (:NEW.ENDDATE IS NOT NULL AND :OLD.ENDDATE IS NULL)) THEN
        change_composition:=change_composition+power(2,3);
    END IF;
    IF ((:NEW.CATEGORY  <> :OLD.CATEGORY)
        OR (:NEW.CATEGORY IS NULL AND :OLD.CATEGORY IS NOT NULL)
        OR (:NEW.CATEGORY IS NOT NULL AND :OLD.CATEGORY IS NULL)) THEN
        change_composition:=change_composition+power(2,4);
    END IF;
    IF ((:NEW.MNO  <> :OLD.MNO)
        OR (:NEW.MNO IS NULL AND :OLD.MNO IS NOT NULL)
        OR (:NEW.MNO IS NOT NULL AND :OLD.MNO IS NULL)) THEN
        change_composition:=change_composition+power(2,5);
    END IF;
    IF ((:NEW.RANK  <> :OLD.RANK)
        OR (:NEW.RANK IS NULL AND :OLD.RANK IS NOT NULL)
        OR (:NEW.RANK IS NOT NULL AND :OLD.RANK IS NULL)) THEN
        change_composition:=change_composition+power(2,6);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE alice_employment_hist SET up_to = operation_date WHERE up_to IS NULL AND id = :OLD.id;
        IF :NEW.id IS NOT NULL THEN
            INSERT INTO alice_employment_hist
            VALUES (
            :NEW.id, :NEW.agent,
            operation_date, null, change_composition,
            :NEW.member, :NEW.institute, :NEW.startdate, :NEW.enddate, :NEW.category, :NEW.mno, :NEW.rank
            );
        ELSE
            INSERT INTO alice_employment_hist
            VALUES (
            :OLD.id, :OLD.agent,
            operation_date, operation_date, change_composition,
            :OLD.member, :OLD.institute, :OLD.startdate, :OLD.enddate, :OLD.category, :OLD.mno, :OLD.rank
            );
        END IF;
    END IF;
END;
/



--ALICE_INSTITUTE;

alter table alice_institute add AGENT NUMBER(7);
update alice_institute set agent = 666996;
alter table alice_institute modify AGENT NOT NULL;

create table alice_institute_hist
(
     ID NUMBER NOT NULL,
     AGENT NUMBER(7) NOT NULL,
     SINCE DATE NOT NULL,          
     UP_TO DATE,          
     WHAT_CHANGED NUMBER(10),   
     FA NUMBER,
     LTX_NAME VARCHAR2(250),
     SHORT_NAME VARCHAR2(50),
     STATUS VARCHAR2(30) NOT NULL,
     ADDRESS NUMBER,
     NAME VARCHAR2(250) NOT NULL,
     INSPIRE_NAME VARCHAR2(150),
     INSPIRE_DOMAIN VARCHAR2(150),
     CONSTRAINT ALICE_INST_HIST_PK PRIMARY KEY (ID, SINCE)
);

CREATE OR REPLACE TRIGGER ALICE_TRG_INSTITUTE_HIST
AFTER INSERT OR UPDATE OR DELETE ON ALICE_INSTITUTE
FOR EACH ROW
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.FA  <> :OLD.FA)
        OR (:NEW.FA IS NULL AND :OLD.FA IS NOT NULL)
        OR (:NEW.FA IS NOT NULL AND :OLD.FA IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF ((:NEW.ltx_name <> :OLD.ltx_name)
        OR (:NEW.ltx_name IS NULL AND :OLD.ltx_name IS NOT NULL)
        OR (:NEW.ltx_name IS NOT NULL AND :OLD.ltx_name IS NULL)) THEN
        change_composition:=change_composition+power(2,1);
    END IF;
    IF ((:NEW.short_name <> :OLD.short_name)
        OR (:NEW.short_name IS NULL AND :OLD.short_name IS NOT NULL)
        OR (:NEW.short_name IS NOT NULL AND :OLD.short_name IS NULL)) THEN
        change_composition:=change_composition+power(2,2);
    END IF;
    IF ((:NEW.status  <> :OLD.status)
        OR (:NEW.status IS NULL AND :OLD.status IS NOT NULL)
        OR (:NEW.status IS NOT NULL AND :OLD.status IS NULL)) THEN
        change_composition:=change_composition+power(2,3);
    END IF;
       IF ((:NEW.address  <> :OLD.address)
        OR (:NEW.address IS NULL AND :OLD.address IS NOT NULL)
        OR (:NEW.address IS NOT NULL AND :OLD.address IS NULL)) THEN
        change_composition:=change_composition+power(2,4);
    END IF;
       IF ((:NEW.name  <> :OLD.name)
        OR (:NEW.name IS NULL AND :OLD.name IS NOT NULL)
        OR (:NEW.name IS NOT NULL AND :OLD.name IS NULL)) THEN
        change_composition:=change_composition+power(2,5);
    END IF;
       IF ((:NEW.inspire_name  <> :OLD.inspire_name)
        OR (:NEW.inspire_name IS NULL AND :OLD.inspire_name IS NOT NULL)
        OR (:NEW.inspire_name IS NOT NULL AND :OLD.inspire_name IS NULL)) THEN
        change_composition:=change_composition+power(2,6);
    END IF;
       IF ((:NEW.inspire_domain  <> :OLD.inspire_domain)
        OR (:NEW.inspire_domain IS NULL AND :OLD.inspire_domain IS NOT NULL)
        OR (:NEW.inspire_domain IS NOT NULL AND :OLD.inspire_domain IS NULL)) THEN
        change_composition:=change_composition+power(2,7);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE alice_institute_hist SET up_to = operation_date WHERE up_to IS NULL AND id = :OLD.id;
        IF :NEW.id IS NOT NULL THEN
            INSERT INTO alice_institute_hist
            VALUES (
            :NEW.id, :NEW.agent,
            operation_date, null, change_composition,
            :NEW.FA, :NEW.ltx_name, :NEW.short_name, :NEW.status, :NEW.address, :NEW.name, :NEW.inspire_name, :NEW.inspire_domain
            );
        ELSE
            INSERT INTO alice_institute_hist
            VALUES (
            :OLD.id, :OLD.agent, 
            operation_date, operation_date, change_composition,
	    :OLD.FA, :OLD.ltx_name, :OLD.short_name, :OLD.status, :OLD.address, :OLD.name, :OLD.inspire_name, :OLD.inspire_domain
            );
        END IF;
    END IF;
END;
/

--ALICE_FA;

alter table alice_fa add AGENT NUMBER(7);
update alice_fa set agent = 666996;
alter table alice_fa modify AGENT NOT NULL;

CREATE TABLE ALICE_FA_HIST
(
     ID NUMBER NOT NULL ,
     AGENT NUMBER(7) NOT NULL,
     SINCE DATE NOT NULL,
     UP_TO DATE,
     WHAT_CHANGED NUMBER(10),
     NAME VARCHAR2(200) NOT NULL,
     LTX_NAME VARCHAR2(250),
     SHORT_NAME VARCHAR2(100),
     ADDRESS VARCHAR2(2000),
     MOU CHAR(1) DEFAULT 'N',
     CONTACT_PERSON VARCHAR2(100),
     CONTACT_EMAIL VARCHAR2(100),
     CONSTRAINT ALICE_FA_HIST_PK PRIMARY KEY (ID, SINCE)
);

CREATE OR REPLACE TRIGGER ALICE_TRG_FA_HIST
AFTER INSERT OR UPDATE OR DELETE ON ALICE_FA
FOR EACH ROW
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.name <> :OLD.name)
        OR (:NEW.name IS NULL AND :OLD.name IS NOT NULL)
        OR (:NEW.name IS NOT NULL AND :OLD.name IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF ((:NEW.ltx_name <> :OLD.ltx_name)
        OR (:NEW.ltx_name IS NULL AND :OLD.ltx_name IS NOT NULL)
        OR (:NEW.ltx_name IS NOT NULL AND :OLD.ltx_name IS NULL)) THEN
        change_composition:=change_composition+power(2,1);
    END IF;
    IF ((:NEW.short_name <> :OLD.short_name)
        OR (:NEW.short_name IS NULL AND :OLD.short_name IS NOT NULL)
        OR (:NEW.short_name IS NOT NULL AND :OLD.short_name IS NULL)) THEN
        change_composition:=change_composition+power(2,2);
    END IF;
    IF ((:NEW.ADDRESS  <> :OLD.ADDRESS)
        OR (:NEW.ADDRESS IS NULL AND :OLD.ADDRESS IS NOT NULL)
        OR (:NEW.ADDRESS IS NOT NULL AND :OLD.ADDRESS IS NULL)) THEN
        change_composition:=change_composition+power(2,3);
    END IF;
    IF ((:NEW.MOU  <> :OLD.MOU)
        OR (:NEW.MOU IS NULL AND :OLD.MOU IS NOT NULL)
        OR (:NEW.MOU IS NOT NULL AND :OLD.MOU IS NULL)) THEN
        change_composition:=change_composition+power(2,4);
    END IF;
    IF ((:NEW.CONTACT_PERSON  <> :OLD.CONTACT_PERSON)
        OR (:NEW.CONTACT_PERSON IS NULL AND :OLD.CONTACT_PERSON IS NOT NULL)
        OR (:NEW.CONTACT_PERSON IS NOT NULL AND :OLD.CONTACT_PERSON IS NULL)) THEN
        change_composition:=change_composition+power(2,5);
    END IF;
    IF ((:NEW.CONTACT_EMAIL  <> :OLD.CONTACT_EMAIL)
        OR (:NEW.CONTACT_EMAIL IS NULL AND :OLD.CONTACT_EMAIL IS NOT NULL)
        OR (:NEW.CONTACT_EMAIL IS NOT NULL AND :OLD.CONTACT_EMAIL IS NULL)) THEN
        change_composition:=change_composition+power(2,6);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE alice_fa_hist SET up_to = operation_date WHERE up_to IS NULL AND id = :OLD.id;
        IF :NEW.id IS NOT NULL THEN
            INSERT INTO alice_fa_hist
            VALUES (
            :NEW.id, :NEW.agent,
            operation_date, null, change_composition,
            :NEW.name, :NEW.ltx_name, :NEW.short_name, :NEW.address, :NEW.mou, :NEW.contact_person, :NEW.contact_email 
            );
        ELSE
            INSERT INTO alice_fa_hist
            VALUES (
            :OLD.ccid, :OLD.agent,
            operation_date, operation_date, change_composition,
            :OLD.name, :OLD.ltx_name, :OLD.short_name, :OLD.address, :OLD.mou, :OLD.contact_person, :OLD.contact_email
            );
        END IF;
    END IF;
END;
/


--ALICE_MEMBER;

alter table alice_member add AGENT NUMBER(7);
update alice_member set agent = 666996;
alter table alice_member modify AGENT NOT NULL;


CREATE TABLE ALICE_MEMBER_HIST(
     CCID NUMBER(7) NOT NULL ,
     AGENT NUMBER(7) NOT NULL,
     SINCE DATE NOT NULL,
     UP_TO DATE,
     WHAT_CHANGED NUMBER(10),
     LTX_NAME VARCHAR2(100) ,
     FOOTNOTE VARCHAR2(4000) ,
     INSPIRE VARCHAR2(25),
     DECEASED CHAR(1) DEFAULT 'N',
     LASTDEGREE_DATE DATE,
     CONSTRAINT ALICE_MEMBER_HIST_PK PRIMARY KEY (CCID, SINCE)
);


CREATE OR REPLACE TRIGGER ALICE_TRG_MEMBER_HIST
AFTER INSERT OR UPDATE OR DELETE ON ALICE_MEMBER
FOR EACH ROW
DECLARE
    change_composition  NUMBER;
    operation_date      DATE;
BEGIN
    change_composition:=0;
    IF ((:NEW.ltx_name <> :OLD.ltx_name)
        OR (:NEW.ltx_name IS NULL AND :OLD.ltx_name IS NOT NULL)
        OR (:NEW.ltx_name IS NOT NULL AND :OLD.ltx_name IS NULL)) THEN
        change_composition:=change_composition+power(2,0);
    END IF;
    IF ((:NEW.FOOTNOTE <> :OLD.FOOTNOTE)
        OR (:NEW.FOOTNOTE IS NULL AND :OLD.FOOTNOTE IS NOT NULL)
        OR (:NEW.FOOTNOTE IS NOT NULL AND :OLD.FOOTNOTE IS NULL)) THEN
        change_composition:=change_composition+power(2,1);
    END IF;
    IF ((:NEW.INPSIRE  <> :OLD.INPSIRE)
        OR (:NEW.INPSIRE IS NULL AND :OLD.INPSIRE IS NOT NULL)
        OR (:NEW.INPSIRE IS NOT NULL AND :OLD.INPSIRE IS NULL)) THEN
        change_composition:=change_composition+power(2,2);
    END IF;
    IF ((:NEW.DECEASED  <> :OLD.DECEASED)
        OR (:NEW.DECEASED IS NULL AND :OLD.DECEASED IS NOT NULL)
        OR (:NEW.DECEASED IS NOT NULL AND :OLD.DECEASED IS NULL)) THEN
        change_composition:=change_composition+power(2,3);
    END IF;
    IF ((:NEW.LASTDEGREE_DATE  <> :OLD.LASTDEGREE_DATE)
        OR (:NEW.LASTDEGREE_DATE IS NULL AND :OLD.LASTDEGREE_DATE IS NOT NULL)
        OR (:NEW.LASTDEGREE_DATE IS NOT NULL AND :OLD.LASTDEGREE_DATE IS NULL)) THEN
        change_composition:=change_composition+power(2,4);
    END IF;
    IF change_composition!=0 THEN
        operation_date:=SYSDATE;
        UPDATE alice_member_hist SET up_to = operation_date WHERE up_to IS NULL AND ccid = :OLD.ccid;
        IF :NEW.ccid IS NOT NULL THEN
            INSERT INTO alice_member_hist
            VALUES (
            :NEW.ccid, :NEW.agent,
            operation_date, null, change_composition,
            :NEW.ltx_name, :NEW.footnote, :NEW.inspire, :NEW.deceased, :NEW.lastdegree_date
            );
        ELSE
            INSERT INTO alice_member_hist
            VALUES (
            :OLD.ccid, :OLD.agent,
            operation_date, operation_date, change_composition,
            :OLD.ltx_name, :OLD.footnote, :OLD.inspire, :OLD.deceased, :OLD.lastdegree_date
            );
        END IF;
    END IF;
END;
/



--ALICE_SERVICETASK2MEMBER;
--ALICE_SERVICETASK;
--ALICE_ENTITY;
--ALICE_GENERIC_PRIVILEGE

