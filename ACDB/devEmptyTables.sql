--empty ACDB1 tables

delete from CITY;
delete from COUNTRY;
--delete from EGROUP;
delete from EMEMBER;
delete from EMPLOYMENT;
delete from FA;
delete from INSTITUTE;
delete from MEMBER;
delete from MNOGROUP;
delete from MNOGROUP2INSTITUTE;
delete from PWG;
delete from ROLE;
delete from ROLEUSER;
delete from THESIS;

--empty ACDB2 tables

--Begin
--for c in (select trigger_name from user_triggers) loop
--execute immediate ('drop trigger '||c.trigger_name);
--end loop;
--End;
--/

drop trigger ALICE_TRG_WORKINGGROUP_DELETE;
drop trigger ALICE_TRG_BOARD_DELETE;
drop trigger ALICE_TRG_PROJECT_DELETE;
drop trigger ALICE_TRG_CLUSTER_DELETE;
drop trigger ALICE_TRG_WORKINGGROUP_INSERT;
drop trigger ALICE_TRG_BOARD_INSERT;
drop trigger ALICE_TRG_PROJECT_INSERT;
drop trigger ALICE_TRG_CLUSTER_INSERT;
drop trigger ALICE_TRG_EXPDPRIV_DELETE;
drop trigger ALICE_TRG_ROLE_DELETE;
drop trigger ALICE_TRG_EXPDPRIV_UPDATE;
drop trigger ALICE_TRG_ROLE_UPDATE;
drop trigger ALICE_TRG_PRIV2MEMB_UPDTINSRT;
drop trigger ALICE_TRG_EGROUP_UPDTINSRT;


delete from ALICE_EGROUP;
delete from ALICE_PRIVILEGE2MEMBER;
delete from ALICE_ROLE;
delete from ALICE_EXPANDED_PRIVILEGE;
delete from ALICE_GENERIC_PRIVILEGE;
delete from ALICE_BOARD;
delete from ALICE_CLUSTER2INSTITUTE;
delete from ALICE_CLUSTER;
delete from ALICE_PROJECT;
delete from ALICE_THESIS;
delete from ALICE_WORKINGGROUP;
delete from ALICE_EMPLOYMENT;
delete from ALICE_EMPLOY_CATEGORY;
delete from ALICE_ASSIGNED_TL;
delete from ALICE_INSTITUTE;
delete from ALICE_FA;
delete from ALICE_ADDRESS;
delete from ALICE_SERVICETASK2MEMBER;
delete from ALICE_MEMBER;
delete from ALICE_SERVICETASK;
delete from ALICE_ENTITY;

--exec DBMS_MVIEW.REFRESH('ALICE_V_ASSIGNABLE_PRIVILEGE');

commit;
drop sequence ALICE_ID_SEQ;

create sequence ALICE_ID_SEQ
start with 2000000
increment by 1
nomaxvalue;

quit
