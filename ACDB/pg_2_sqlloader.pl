#!/usr/bin/env perl
########################################################################

=head1 NAME

pg_to_sqlloader.pl

=head1 SYNOPSIS

B<pg_to_sqlloader.pl> <list of tables to export>.

=head1 DESCRIPTION

Extracts data from a postgresql table and creates the necessary files 
for importing (via sqlloader) to oracle.

=head1 ASSERTIONS

 - The schema name matches.

 - The table names match; over-long table names got properly shortened.

 - The column names match; over-long column names got properly shortened.

 - The case of the schema/table/column names is the default for the 
    respective databases.

 - Boolean data types are translated to char(1) {Y, N}

=head1 AUTHOR

gsiems

=head1 HISTORY

2010.09.30 Created.

=cut

########################################################################
# SETUP:
use strict;
use warnings FATAL => 'all';
########################################################################
# MAIN:

use Data::Dumper;

my $pg_host = 'acdb.cern.ch';
my $pg_port = '5432';
my $pg_user = 'acdbro';
my $pg_db   = 'acdb4';

foreach my $arg (@ARGV) {
    my ($schema, $tablename) = parse_tablename($arg);
    warn "Extracting $tablename data...";
    export_pg($schema, $tablename);
    my @table_meta = get_pg_meta($schema, $tablename);
    warn "  Creating control file...";
    make_ora_ctl($schema, $tablename, \@table_meta);
}

sub parse_tablename {
    my $arg = shift;
    my ($schema, $tablename);
    if ($arg =~ m/\./) {
        ($schema, $tablename) = split /\./, $arg;
    } else {
        $tablename = $arg;
        $schema    = 'public';
    }
    return ($schema, $tablename);
}

sub pg_export_filename {
    my ($schema, $tablename) = @_;
    return "${tablename}.out";
}

sub export_pg {
    my ($schema, $tablename) = @_;

    my $export_file = pg_export_filename($schema, $tablename);
    my $cmd =
          "psql -h $pg_host -p $pg_port -U $pg_user "
        . "-d $pg_db -t -A "
        . "-c \"select * from $schema.$tablename;\" "
        . " > $export_file";
    print $cmd;
    `$cmd`;
}

sub get_pg_meta {
    my ($schema, $tablename) = @_;

    my $cmd = "psql -h $pg_host -p $pg_port -U $pg_user -d $pg_db"
        . " -t -A -c \"\\d $schema.$tablename;\" ";
    print "\n".$cmd;
    my @table_meta = grep { $_ =~ m/\|/ } `$cmd`;
    chomp @table_meta;
    return @table_meta;
}

sub make_ora_ctl {
    my ($schema, $tablename, $meta_ref) = @_;

    my $ctl_filename  = "${tablename}.ctl";
    my $export_file   = pg_export_filename($schema, $tablename);
    my $imp_tablename = shorten_name($tablename);

    my $ctl = << "EOT";
LOAD DATA
INFILE      '$export_file'
BADFILE     '$imp_tablename.bad'
DISCARDFILE '$imp_tablename.dsc'
TRUNCATE INTO TABLE $imp_tablename
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS 
(
EOT

    my @cols = ();
    foreach my $col_meta (@{$meta_ref}) {
        my ($colname, $datatype, $modifiers) = split /\|/, $col_meta;
        my $column_name = shorten_name($colname);

        my $data_type = 'CHAR';    # set the default

        # translate postgres datatypes and domains to something oracle can deal with:
        my $data_trans = '';
        if ($datatype =~ m/^(date|timestamp)/ and ($tablename eq 'log' or $tablename eq 'transection')) {
            $data_type = 'timestamp "YYYY-MM-DD HH24:MI:SS"';
        } elsif ($colname eq 'tdate') {
	  $data_type = 'char "TO_TIMESTAMP_TZ(:tdate, \'YYYY-MM-DD HH24:MI:SS.FF6TZH\')"';
	} elsif ($colname eq 'vdate' or $colname eq 'edate') {
	  $data_type = 'timestamp with time zone "YYYY-MM-DD HH24:MI:SSTZH"';
	} elsif ($datatype eq 'd_text_comment') {
            $data_type = 'CHAR(1024)';
        }
        elsif ($datatype =~ m/^(character varying|varchar)/) {
            my ($data_length) = $datatype =~ m/\((\d+)\)/;
            if ($data_length && $data_length >= 500) {
                $data_type = "CHAR($data_length)";
            }
        } elsif ($datatype =~ m/^(d_flag|boolean)/) {
            $data_type  = "CHAR ";
            $data_trans = " \"translate(upper(:$column_name), 'TF','YN')\" ";
        }

        my $nullif = ($modifiers =~ m/not null/) ? '' : "NULLIF ($column_name = blanks)";

        my $line = sprintf("  %-32s%-30s%-70s", $column_name, $data_type, $nullif . $data_trans);
        $line =~ s/ +$//;

        push @cols, $line;
    }

    $ctl .= join ",\n", @cols;
    $ctl .= "\n)\n";

    print $ctl;

    my $OUT;
    open($OUT, '>', $ctl_filename) || die "could not open $ctl_filename for output $!\n";
    print $OUT $ctl;
    close $OUT;
}

sub shorten_name {
    my $name = shift;
    return $name; # if (length $name <= 30);

    # For those names that exceed the oracle length limit...
    my @abbreviations = (
        'control,ctrl',     'process,proc',  'material,matl',
        'capacity,cap',     'group,grp',    
        );

    my $new_name = $name;
    foreach my $abbr (@abbreviations) {
        my ($f, $r) = split ',', $abbr;
        $new_name =~ s/$f/$r/g;

        last if (length($new_name) <= 30);
    }

    if (length $new_name > 30) {
        warn "$new_name (" . length $new_name . ") is still too long! Was $name\n";
    }

    return $new_name;
}

__END__
