#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

testPath="/tmp/cmaketest"

import os
def createDir(dirPath):
    if not os.path.isdir(dirPath):
        os.makedirs(dirPath)
        return True
    else: return False

def removeDir(dirPath):
    import shutil
    if os.path.isdir(dirPath):
        shutil.rmtree(dirPath)
        return True
    else: return False

import subprocess
import time, signal, sys
#def startServer():
    #subprocess.Popen

sys.path.append(".")
import infoLogger

createDir(testPath)

# Pre-condition

# Set stand-alone, log dir and host
os.environ["DATE_INFOLOGGER_LOGHOST"]="localhost"
os.environ["DATE_SITE"]="standalone"
os.environ["DATE_SITE_LOGS"]=testPath

# listening ports of the log server
os.environ["DATE_SOCKET_INFOLOG_RX"]="6003"
os.environ["DATE_SOCKET_INFOLOG_TX"]="6004"

# Do not connect to mysql
os.environ["DATE_INFOLOGGER_MYSQL"]="FALSE"

server = subprocess.Popen(["./infoLoggerServer"])

time.sleep(3)

reader = subprocess.Popen(["./infoLoggerReader"])

time.sleep(3)

infoLogger.infoOpen()

infoLogger.infoLog("ctest", "F", "Testing from ctest")
infoLogger.infoSetFacility("ctest")
infoLogger.extendedInfoLog("F", "Testing from ctest", "keyword", "filename.cpp", 0, "compile_date", "compile_time")

infoLogger.infoClose()

time.sleep(2)

#returncode = subprocess.check_call(["./log", "-i", "This is an error log message"])

os.kill(reader.pid, signal.SIGTERM)     # for python 2.6: server.terminate()
reader.wait()

os.kill(server.pid, signal.SIGTERM)     # for python 2.6: server.terminate()
server.wait()

logfile = open(testPath + "/dateLogs")
#print logfile.read()
logfile.close()

removeDir(testPath)

import sys
sys.exit(0)
