#!/bin/bash

# Make sure that infologgerReader is not already running 
# on the host where this test is run.

# Include commen env vars
. `dirname $0`/testenv.sh

sleep 5

export DATE_INFOLOGGER_SOCKET=${DATE_SITE_LOGS}/mysqld.sock

# Connect to database
export DATE_INFOLOGGER_MYSQL=TRUE

export DATE_INFOLOGGER_MYSQL_USER=infologger
export DATE_INFOLOGGER_MYSQL_PWD=CHANGEME
export DATE_INFOLOGGER_MYSQL_HOST=localhost
export DATE_INFOLOGGER_MYSQL_DB=infologger

echo "* Set up temporary directory"
mkdir -p ${DATE_SITE_LOGS}

echo "* Setting up MySQL"
mysql_install_db --datadir=${DATE_SITE_LOGS}

ASSERT "Could not setup mysql in temporary directory."

echo "* Starting MySQL daemon"
mysqld_safe --no-defaults \
            --pid-file=${DATE_SITE_LOGS}/mysqld.pid \
            --socket=${DATE_INFOLOGGER_SOCKET} \
            --datadir=${DATE_SITE_LOGS} \
            --log-error=${DATE_SITE_LOGS}/mysql.error &

ASSERT "Could not start mysql"

sleep 5

echo "* Inserting infologger database definitions"
mysql -u root --socket=${DATE_INFOLOGGER_SOCKET} < ${TESTDIR}/../infoLogger.sql

ASSERT "Could not setup infologger table definitions."

echo "* Starting infoLoggerServer"
./infoLoggerServer
sleep 1
SERVERPID=`pidof -o %PPID ./infoLoggerServer`
ASSERT_FALSE "$SERVERPID" "" "Could not start infoLoggerServer"

echo "* Starting infoLoggerReader"
./infoLoggerReader
sleep 1
READERPID=`pidof -o %PPID ./infoLoggerReader`
ASSERT_FALSE "$READERPID" "" "Could not start infoLoggerReader"

echo "* Send various types of messages"
./log -i "This is a information message"
./log -e "This is an error message"
./log -f "This is a fatal message"

echo "* Stop processes and clean-up"
kill ${READERPID}
kill ${SERVERPID}

ERRORS=`grep ERROR /tmp/cmaketest/infoLoggerServerLog`
ASSERT_TRUE "$ERRORS" "" "$ERRORS"

echo "* Stopping MySQL daemon"
mysqladmin -u root --socket=${DATE_INFOLOGGER_SOCKET} shutdown

ASSERT "Could not stop mysql server."

echo "* Cleaning up"
rm -fr ${DATE_SITE_LOGS}

exit 0

