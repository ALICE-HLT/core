#!/bin/bash

# TODO: proper TEAR_DOWN/SET_UP mechanism. Currently doesn't stop mysql server if test fails
# 
# 'wait pid' doesn't work as it isn't a child process of the shell

TESTDIR=`dirname $0`

# Set stand-alone, log dir and host
export DATE_INFOLOGGER_LOGHOST=localhost
export DATE_SITE=standalone
export DATE_SITE_LOGS=/tmp/cmaketest

# listening port of the log server
export DATE_SOCKET_INFOLOG_RX=6003
export DATE_SOCKET_INFOLOG_TX=6004

# Do not connect to a database
export DATE_INFOLOGGER_MYSQL=FALSE

# Checks exit code for last ran command
# Calls exit with exit code of last command if non-zero
function ASSERT {
    MSG=$@

    if [[ $? != 0 ]] ; then
        echo "ASSERT Failed: $MSG"
        rm -fr ${DATE_SITE_LOGS}
        exit $?
    fi
}

# exits with 1 if VAR1 and VAR2 are equal
function ASSERT_FALSE {
    VAR1=$1
    VAR2=$2
    ERROR_MSG=$3

    if [[ $VAR1 == $VAR2 ]] ; then
        echo -e "ASSERT_FALSE Failed: VAR1 == VAR2\n$ERROR_MSG."
        rm -fr ${DATE_SITE_LOGS}
        exit 1
    fi
}

# exits with 1 if VAR1 and VAR2 differ
function ASSERT_TRUE {
    VAR1=$1
    VAR2=$2
    ERROR_MSG=$3

    if [[ $VAR1 != $VAR2 ]] ; then
        echo -e "ASSERT_TRUE Failed: VAR1 != VAR2\n$ERROR_MSG."
        rm -fr ${DATE_SITE_LOGS}
        exit 1
    fi
}
