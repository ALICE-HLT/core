#!/bin/bash

# Include commen env vars
. `dirname $0`/testenv.sh

sleep 5

echo "* Start server and reader and log various types of messages."
mkdir -p ${DATE_SITE_LOGS}

echo "* Starting infoLoggerServer"
./infoLoggerServer
sleep 1
SERVERPID=`pidof -o %PPID ./infoLoggerServer`
ASSERT_FALSE "$SERVERPID" "" "Could not start infoLoggerServer"

echo "* Starting infoLoggerReader"
./infoLoggerReader
sleep 1
READERPID=`pidof -o %PPID ./infoLoggerReader`
ASSERT_FALSE "$READERPID" "" "Could not start infoLoggerReader"

echo "* Send various types of messages"
./log -i "This is a information message"
./log -e "This is an error message"
./log -f "This is a fatal message"

echo "* Stop processes and clean-up"
kill ${READERPID}
kill ${SERVERPID}

ERRORS=`grep ERROR /tmp/cmaketest/infoLoggerServerLog`
ASSERT_TRUE "$ERRORS" "" "$ERRORS"

rm -fr ${DATE_SITE_LOGS}

exit 0
