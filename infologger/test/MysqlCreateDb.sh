#!/bin/bash

# Include commen env vars
. `dirname $0`/testenv.sh

sleep 2

export DATE_INFOLOGGER_SOCKET=${DATE_SITE_LOGS}/mysqld.sock

# Set up temporary directory and log one message
mkdir -p ${DATE_SITE_LOGS}

echo "* Setting up MySQL"
mysql_install_db --datadir=${DATE_SITE_LOGS}

ASSERT "Could not setup mysql in temporary directory."

echo "* Starting MySQL daemon"
mysqld_safe --no-defaults \
            --pid-file=${DATE_SITE_LOGS}/mysqld.pid \
            --socket=${DATE_INFOLOGGER_SOCKET} \
            --datadir=${DATE_SITE_LOGS} \
            --log-error=${DATE_SITE_LOGS}/mysql.error &

ASSERT "Could not start mysql"

sleep 2

echo "* Inserting infologger database definitions"
mysql -u root --socket=${DATE_INFOLOGGER_SOCKET} < ${TESTDIR}/../infoLogger.sql

ASSERT "Could not setup infologger table definitions."

echo "* Stopping MySQL daemon"
mysqladmin -u root --socket=${DATE_INFOLOGGER_SOCKET} shutdown

ASSERT "Could not stop mysql server."

echo "* Cleaning up"
rm -fr ${DATE_SITE_LOGS}

exit 0
