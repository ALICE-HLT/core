#!/bin/bash

# Include commen env vars
. `dirname $0`/testenv.sh

mkdir -p ${DATE_SITE_LOGS}

echo "* Will start server in test mode and log one message to the database."

./infoLoggerServer -t 1
SERVERPID=`pidof -o %PPID ./infoLoggerServer`
ASSERT_FALSE "$SERVERPID" "" "Server was not started."

sleep 1

ERRORS=`grep ERROR /tmp/cmaketest/infoLoggerServerLog`
ASSERT_TRUE "$ERRORS" "" "$ERRORS"
kill ${SERVERPID}
rm -fr ${DATE_SITE_LOGS}

exit 0
