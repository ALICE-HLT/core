DROP DATABASE IF EXISTS infologger;

CREATE DATABASE infologger;

GRANT ALL ON infologger.* TO 'infologger'@'localhost' IDENTIFIED BY 'CHANGEME';

USE infologger;

CREATE TABLE IF NOT EXISTS messages(
    severity char(1),
    timestamp double(16,6),
    hostname varchar(20),
    rolename varchar(32),
    pid int,
    username varchar(20),
    system varchar(20),
    facility varchar(20),
    dest varchar(20),
    run int,
    keyword varchar(20),
    filename varchar(80),
    compile_date varchar(20),
    compile_time varchar(20),
    linenr int,
    message text,
    index ix_severity(severity),
    index ix_timestamp(timestamp),
    index ix_hostname(hostname),
    index ix_rolename(rolename),
    index ix_facility(facility),
    index ix_dest(dest),
    index ix_run(run),
    index ix_linenr(linenr),
    index ix_system(system)
);

