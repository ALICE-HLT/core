#! /bin/sh

# Setup DATE and infoLogger environment
if [ "${DATE_INFOLOGGER_DIR}" = "" ]; then
  DATE_INFOLOGGER_DIR=/date/infoLogger
fi
. ${DATE_INFOLOGGER_DIR}/infoLoggerConfig.sh --


# Launch infoBrowser
${DATE_INFOLOGGER_DIR}/infoBrowser.tcl -- $@ &
sleep 1
