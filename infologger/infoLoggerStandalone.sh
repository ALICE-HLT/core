#######################################
# This script defines variables to work with the DATE infoLogger
# without the full environment defined.
#######################################

# directory to temporarily store logs
export DATE_SITE_LOGS=/tmp/infoLogger

# name of the host running the log server
export DATE_INFOLOGGER_LOGHOST=localhost


#######################################
# static configuration
#######################################

# a string to identify the DATE site
# forces the standalone operation of infoLogger
export DATE_SITE=standalone

# installation path of infoLogger standalone software
export DATE_ROOT=/opt/infoLogger
export DATE_INFOLOGGER_DIR=${DATE_ROOT}

# listening port of the log server
export DATE_SOCKET_INFOLOG_RX=6001
export DATE_SOCKET_INFOLOG_TX=6002

#######################################
# optionnal items
#######################################

# export DATE_RUN_NUMBER=
# export DATE_INFOLOGGER_SYSTEM=

# export DATE_INFOLOGGER_STDOUT= [FALSE|TRUE|ONLY]
# if set to only, all logs go to stdout
# if set to true, logs go to stdout and to the DATE log system
# by default, logs go to DATE log system only
