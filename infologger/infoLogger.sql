DROP DATABASE IF EXISTS infologger;

CREATE DATABASE infologger;

GRANT ALL ON infologger.* TO 'infologger'@'localhost' IDENTIFIED BY 'CHANGEME';
GRANT SELECT ON infologger.* TO 'infoBrowser'@'localhost' IDENTIFIED BY 'infoBrowser';

USE infologger;

CREATE TABLE IF NOT EXISTS messages(
    severity char(1),
    timestamp double(16,6),
    hostname varchar(20),
    rolename varchar(32),
    pid int,
    username varchar(20),
    system varchar(20),
    facility varchar(50),
    dest varchar(20),
    run int,
    message text,
    index ix_severity(severity),
    index ix_timestamp(timestamp),
    index ix_hostname(hostname),
    index ix_rolename(rolename),
    index ix_facility(facility),
    index ix_dest(dest),
    index ix_run(run),
    index ix_system(system)
);


CREATE TABLE hltproxy (
        severity CHAR(1),
        timestamp DOUBLE,
        hostname VARCHAR(20),
        rolename VARCHAR(32),
        pid INT,
        username VARCHAR(20),
        system VARCHAR(20),
        facility VARCHAR(50),
        dest VARCHAR(20),
        run INT,
        message TEXT
);

CREATE INDEX ix_rolename ON hltproxy (rolename ASC);
CREATE INDEX ix_system ON hltproxy (system ASC);
CREATE INDEX ix_timestamp ON hltproxy (timestamp ASC);
CREATE INDEX ix_hostname ON hltproxy (hostname ASC);
CREATE INDEX ix_run ON hltproxy (run ASC);
CREATE INDEX ix_severity ON hltproxy (severity ASC);
CREATE INDEX ix_dest ON hltproxy (dest ASC);
CREATE INDEX ix_facility ON hltproxy (facility ASC);

delimiter //
CREATE TRIGGER insert_check BEFORE INSERT ON messages FOR EACH ROW
BEGIN
    IF (NEW.facility like 'HLT-Proxy%') THEN
        INSERT INTO hltproxy VALUES (
            NEW.severity,
            NEW.timestamp,
            NEW.hostname,
            NEW.rolename,
            NEW.pid,
            NEW.username,
            NEW.system,
            NEW.facility,
            NEW.dest,
            NEW.run,
            NEW.message
        );
    END IF;
END//

delimiter ;
