// infoLogger.i : SWIG interface for infoLogger library.
// 
// This exports infoLogger functions to scripting languages as loadable modules.
// To load the module from Tcl: load $env(DATE_INFOLOGGER_BIN)/libInfo_tcl.so infoLogger
//
// S.C.  8 Jul 2005  Interface created

%module infoLogger
%{
#include "infoLogger.h"
%}

/* open infoLoggerReader connection */
void infoOpen();

/* close infoLoggerReader connection */
void infoClose();

/* Log message to given facility/stream with given severity */
void _infoLogTo(const char *Facility, const char *logStream, const char Severity,const char *Message);

/* Log message to given facility with given severity */
void infoLog(const char *Facility, const char Severity,const char *Message);

/* Extended logging functionality or hlt */
void extendedInfoLog(const char Severity, const char *Message, const char *keyword, const char *filename, int linenr, const char *compile_date, const char *compile_time);

/* define default log facility (pointer must remain valid) - NULL resets to default */
void infoSetFacility(const char * const facility);

/* define default log stream (pointer must remain valid) - NULL resets to default */
/* THIS FUNCTION SHOULD NOT BE USED unless called from the runControl */
void infoSetStream( const char * const fileName );

/* set user Name (optionnal) */
void infoSetUserName(const char * username);
