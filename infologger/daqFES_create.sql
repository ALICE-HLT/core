# create table for DAQ File Exchange Server

# All timestamps in UNIX time format: number of seconds since 1-1-1970

create table if not exists daqFES_files (
  run int COMMENT 'Run number',
  detector char(3) COMMENT 'Detector code',
  fileId char(128) COMMENT 'File ID',
  filePath char(255) COMMENT 'Full path in File Exchange Server',
  size bigint unsigned  COMMENT 'File size in bytes',
  time_created double(16,6) COMMENT 'Time of file creation',
  time_processed double(16,6) COMMENT 'Time of file processing end (retrieval)',
  time_deleted double(16,6) COMMENT 'Time of file deletion',
  DAQsource char(32) COMMENT 'DAQ role which created the file',
  fileChecksum char(32) COMMENT 'MD5 checksum',
  index(run),
  index(detector),
  primary key (filePath(255))
) COMMENT 'Files in DAQ File Exchange Server', ENGINE = INNODB;
