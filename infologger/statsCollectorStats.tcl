##
## For each run make some statistical calcs on it's data in the logbook
##
## Creator: Simon Lord
##
## History
## 22/04/2005 Created

##
## return alist of all runNumbers available in logbook table
##
proc getRunNumbers {} {
    global db
    set query "select distinct run from logbook where run is not null order by run"
    set runs [mysqlsel $db $query -list]
    return $runs
}

##
## return log for specific run number
##
proc getRunData {runNumber} {
    global db
    set query "select log from logbook where run = $runNumber"
    set runData [mysqlsel $db $query -flatlist]
    return $runData
}


##
## insert stats into db
##
proc insertData {run totEvents totData avData avEvents avDuration ldc gdc streams} {
    global db
    set query "update logbook set totalEvents = $totEvents, totalData = $totData, averageDataRate = $avData, averageEventsPerSecond = $avEvents, averageDuration = $avDuration, numberOfLDCs = $ldc, numberOfGDCs = $gdc, numberOfStreams = $streams where run = $run"
    mysqlexec $db $query
}



##
## create stats for a specific run, if no run specified then create stats for all runs in logbook
## when creating stats for one run it is possible to pass tehe log where the stats are derived from
## if you don't this will be queried from the db.
##
proc createStats {run log} {

    if {$run} {
	set myRunNumbers [list $run]
    } else {
	set myRunNumbers [getRunNumbers]
    }


    
    foreach run $myRunNumbers {

	if {[string length $log]} {
	    set myData $log
	} else {
	    set myData [getRunData $run]
	}

	set newData [split $myData "\n"]
	set totalEvents 0
	set totalEventSize 0
	set dataRate 0
	set totalDataRate 0
	set averageDataRate 0
	set eventRate 0
	set totalEventRate 0
	set averageEventRate 0
	set duration 0
	set totalDuration 0
	set averageDuration 0
	set count 0

        set ldc 0
        set gdc 0
        set streams 0

	foreach line $newData {
	    
	    if [regexp -- {Recorded.*seconds} $line match] {
		
		# Get number of events and build a total
		set events [lindex [split [lindex $match 0] :] 1]
		set totalEvents [expr $totalEvents + $events]
		
		
		# Get total event size and build an overall total
		set eventSize [lindex [split [lindex $match 2] :] 1]
		set measure [lindex $match 3]
		if {$measure == "GB"} {
		    set totalEventSize [expr $totalEventSize + $eventSize]
		} else {
		    set totalEventSize [expr $totalEventSize + [expr $eventSize / 1024]]
		}
		
		
		# get mb/s and build an average
		set dataRate [lindex $match 5]
		set totalDataRate [expr $totalDataRate + $dataRate]
		
		
		# get events per second and build an average
		set eventRate [lindex $match 7]
		set totalEventRate [expr $totalEventRate + $eventRate]
		
		
		# get run duration and build an average
        set subindex [string first "runDuration" $match]
        if {$subindex!=-1} {
    		if {[scan [string range $match $subindex end] "runDuration:%d" duration]==1} {
    		    set totalDuration [expr $totalDuration + $duration]
            } else {
                set duration 0
            }
        }
		
		incr count 1
		
	    } elseif {[regexp -- {DAQ configuration:.*stream} $line match]} {
		## Get number of LDC and GDC used in run

		set ldc [lindex $match 2]
		set gdc [lindex $match 4]
		set streams [lindex $match 6]
	    }
	    
	    if {$totalDataRate} {
		set averageDataRate [expr $totalDataRate/$count]
	    }
	    
	    if {$totalEventRate} {
		set averageEventRate [expr $totalEventRate/$count]
	    }
	    
	    if {$totalDuration} {
		set averageDuration [expr $totalDuration/$count]
	    }
	    
	}
	
	puts "Stats for run $run"
	puts "    LDC(s) used: $ldc"
	puts "    GDC(s) used: $gdc"
	puts "    Number of recording streams: $streams"
	puts ""
	puts "    Total events: $totalEvents"
	puts "    Total data recorded: $totalEventSize GB"
	puts "    Average data rate $averageDataRate MB/s"
	puts "    Average events per second: $averageEventRate Ev/s"
	puts "    Average runDuration: $averageDuration"
	puts ""
	
	# The important insert line!!!
        # insert only if valuable info found
        if {$ldc>0} {
          insertData $run $totalEvents $totalEventSize $averageDataRate $averageEventRate $averageDuration $ldc $gdc $streams
        }
    }
}
