// compile:
// gcc fake_statsCollectorDaemon.c /opt/dim/linux/libdim.a -lpthread -I/opt/dim/dim -o fake_statsCollectorDaemon
/*

implements:

- a DIM command DISPATCH_EOR
- a DIM service /DATE/LOGBOOK/UPDATE

the DIM service is triggered when DIM command is received

*/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <limits.h>
#include <pwd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <string.h>
#include "dis.h"

#define TIMEOUT 0  /* Mins before timing out and running statsCollector.tcl - 0 means feature disabled */
#define DELAY   5   /* Delay before launching statsCollector - need to make sure everything is in db */

/* Globals */
int   shutdown_now = 0;     /* set to 1 in signal handler on TERM/QUIT request */
struct timeval  tv1;        /* updated every time statsCollector is run */


unsigned int logbook_update_service_id=0;     /* DIM service for logbook updates */
long         logbook_update_time=0;           /* time of last logbook update */

    
/* Simple sighandler for a nice shutdown */

static void sig_handle(int signo){
  switch(signo){
  case SIGQUIT:
  case SIGTERM:
    shutdown_now = 1; break;
  }
}



/* 
** This is ran each time a runControl process activates the
** dim command we setup later on. It executes the StatsCollector.tcl
** script.
*/

void runStatsCollector(long *tag, int *address, int *size){

  time_t newtime;
  newtime=time(NULL);
  if (newtime<=logbook_update_time) {
    newtime=logbook_update_time+1;
  }
  logbook_update_time=newtime;
  printf("statsCollector success - %d clients updated at %ld\n", dis_update_service(logbook_update_service_id), newtime );

}



int main(int argc, char **argv){
  int                status         = 0;

  char              *tmp_path       = NULL;

  char              *date_root      = NULL;
  char              *date_site      = NULL;
  char              *date_user      = NULL;

  struct timeval     tv2;
  struct passwd     *date_user_info = NULL;

  int   server_id = 0;


  /* Daemonize ourself :) */
/*
  if(daemon(0,0) == -1){
    printf("Error whilst calling daemon() at line %d of %s: %s\n",
	    __LINE__,__FILE__,strerror(errno));
    return 1;
  }
*/

  printf("statsCollectorDaemon started\n");

  /* Redirect SIGQUIT and SIQTERM for a clean shutdown */
  signal(SIGQUIT, sig_handle);
  signal(SIGTERM, sig_handle);
  
  /* 
  ** Create our command which the client will access to let us know to run
  ** the statsCollector.tcl script
  */
  server_id = dis_add_cmnd("DISPATCH_EOR", 0, runStatsCollector, 1);
  logbook_update_service_id = dis_add_service("/DATE/LOGBOOK/UPDATE", "L:1", (int *) &logbook_update_time, sizeof(logbook_update_time), NULL, 0);
  
  if(dis_start_serving("statsCollectorDaemon") != 1){
    printf("dis_start_serving() failed\n");
    return 1;
  }

  gettimeofday(&tv1,NULL);
  /* Do nothing until we get SIGQUIT or SIGTERM */
  while(shutdown_now == 0){
    if (TIMEOUT!=0) {
      /* 
      ** Run the statsCollector.tcl script if it hasn't 
      ** been triggered every TIMEOUT minutes 
      */
      gettimeofday(&tv2,NULL);
      if((tv2.tv_sec - tv1.tv_sec) > (TIMEOUT * 60)){
        printf("No statsCollection for the last %d minutes, starting now...\n",TIMEOUT);
        runStatsCollector(NULL,NULL,NULL);
        gettimeofday(&tv1,NULL);
      }
    }
    sleep(1);
  }


  /* Shutdown */
  dis_remove_service(server_id);
  dis_stop_serving();
  printf("statsCollectorDaemon stopped\n");
  
  return 0;
}
