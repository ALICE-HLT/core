#include <unistd.h>
#include <stdio.h>

#include "transport_client.h"
#include "simplelog.h"
#include "utility.c"


/*
gcc -Wall infoLoggerServerTester.c Linux/libInfo.a -o Linux/log  -L/usr/lib/mysql -lmysqlclient -lcrypt -lnsl -lm -lz -lc -lnss_files -lnss_dns -lresolv -lc -lnss_files -lnss_dns -lresolv -lc -ldl -lm -lpthread -lTransport -L./Linux -o logToServer
*/

void usage() {
  printf("usage: \n\
  -s server name \n\
  -p port number \n\
  -m message \n\
  -n number of times to send messages\n\
  -f filename to read messages from\n\
  ");
}


int main(int argc,char **argv){
  TR_client_configuration cfg;
  TR_client_handle h;
  int i;
  TR_file* f;
  TR_blob* b;
  int j,k;
  int option;

  int n_msg=1;
  char *msg="*1.2#I#1099570259#pcald10#testrole#12345#testuser#DAQ#testclient#defaultLog#12345#message";
  char *filename=NULL;
  FILE *fp=NULL;
  char fline[10000];
  
  cfg.server_name="localhost";
  cfg.server_port=6001;
  cfg.queue_length=1000000;
  cfg.msg_queue_path=NULL;
  cfg.client_name=NULL;
  cfg.proxy_state=TR_PROXY_CAN_NOT_BE_PROXY;
  
  
    /* parse command line parameters */
    while((option = getopt(argc, argv, "s:m:p:n:f:h")) != -1){
      switch(option){
      case 's': 
        cfg.server_name=optarg;
        break;
      case 'm': 
        msg=optarg;
        break;
      case 'n':
        sscanf(optarg,"%d",&n_msg);
        break;
      case 'p':
        sscanf(optarg,"%d",&cfg.server_port);
        break;
      case 'f':
        filename=optarg;
        break; 
      case 'h':
      case '?':    
        usage();
        return 0;

      default:
        usage();
        return -1;
      }
    }

  if (filename!=NULL) {
    fp=fopen(filename,"r");
    if (fp==NULL) {printf("Failed to open %s\n",filename); return -1;}
  }

for (k=0;k<1;k++) {
  h=TR_client_start(&cfg);

  for(j=0;j<n_msg;j++){
  
   if (fp!=NULL) {
     if (fgets(fline,sizeof(fline),fp)==NULL) {
       rewind(fp);
       if (fgets(fline,sizeof(fline),fp)==NULL) {
         msg=NULL;
         break;
       }
     }
     msg=fline;
   }
   f=TR_file_new();

   b=checked_malloc(sizeof(*b));
   b->value=checked_strdup(msg);

   b->size=strlen(msg);
   b->next=NULL;
   f->id.minId=j;
   f->id.majId=1;
   f->size=strlen(msg);
   f->first=b;

   // TR_file_dump(f);
   TR_client_queueAddFile(h,f);
   TR_file_dec_usage(f);
  }

  for(i=0;i!=1;){
    f=TR_client_getLastFileSent(h);
    if (f!=NULL) {
      // TR_file_dump(f);
      if (f->id.minId%10000==0) {slog(SLOG_INFO,"%d.%d ok\n",f->id.majId,f->id.minId);}
      if ((f->id.minId==n_msg-1)) {i=1;}
      TR_file_dec_usage(f);
    } else {
      usleep(1000);
    }
  }

  TR_client_stop(h);
} 
  
  if (fp!=NULL) {
    fclose(fp);
  }
  
  slog(SLOG_INFO,"%d items left",checked_memstat());

  return 0;
}
