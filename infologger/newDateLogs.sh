#!/bin/sh

# Clean/create log database
# This script must be run on the host where infoLoggerServer is running in case
# of the flat file database. The new log file is created with current user
# access.

# 07 Apr 2005  SC  Added Run Number log field (number 8).
# 22 Apr 2005  SL  Added new statistics field to logbook.
# 25 Apr 2005  SC  source config file
# 08 Nov 2005  SC  Added new table for logbook. Added indexes.
# 22 Feb 2007  SC  Creation of logbook/FES tables moved to separate SQL file


usage() {
  echo "Usage: $0 [-c|-d|-e]"
  echo "  -c : create table only, no backup"
  echo "  -d : delete messages only, no backup"
  echo "  -e : erase all message archives and create new message table"
  echo "By default, messages are saved in a table with backup time"
}

DELETE=0;
CREATE=0;
ERASE_ALL=0;
export OPTIND=1
while getopts 'hdce' OPTION; do
 case $OPTION in
  h|\?)
   usage
   exit 0 ;;
  d)
   DELETE=1 ;;
  c)
   CREATE=1 ;;
  e)
   DELETE=1
   CREATE=1
   ERASE_ALL=1 ;;
 esac
done

# Setup DATE and infoLogger environment
if [ "${DATE_INFOLOGGER_DIR}" = "" ]; then
  DATE_INFOLOGGER_DIR=/date/infoLogger
fi
. ${DATE_INFOLOGGER_DIR}/infoLoggerConfig.sh --

# current date-time
NOW="__"`date "+%Y_%m_%d__%H_%M_%S"`

# definition of message table
TABLEDESCR='severity char(1), timestamp double(16,6), hostname varchar(20), rolename varchar(32), pid int, username varchar(20), system varchar(20), facility varchar(20), dest varchar(20), run int, message text \
,index ix_severity(severity), index ix_timestamp(timestamp), index ix_hostname(hostname), index ix_rolename(rolename), index ix_facility(facility), index ix_dest(dest), index ix_run(run), index ix_system(system)'

  if [ "$MYSQL_PATH" == "" ]; then
    SQLCLIENT="mysql"
  else
    SQLCLIENT="$MYSQL_PATH/bin/mysql"
  fi

  if [ "$ERASE_ALL" == "1" ]; then
      # drop all messsages tables in db
      QUERY="show tables;"
      MSGTABLES=`$SQLCLIENT -u $DATE_INFOLOGGER_MYSQL_USER --password=$DATE_INFOLOGGER_MYSQL_PWD -h $DATE_INFOLOGGER_MYSQL_HOST -D $DATE_INFOLOGGER_MYSQL_DB -B -N -e "$QUERY"`
      T2=`echo $MSGTABLES|tr ' ' '\n'|grep messages`
      if [ "$T2" != "" ]; then
	      QUERY2="drop table `echo $T2|tr ' ' ','`"
	      $SQLCLIENT -u $DATE_INFOLOGGER_MYSQL_USER --password=$DATE_INFOLOGGER_MYSQL_PWD -h $DATE_INFOLOGGER_MYSQL_HOST -D $DATE_INFOLOGGER_MYSQL_DB -B -N -e "$QUERY2"
      fi      
  fi

  if [ "$CREATE" == "1" ]; then
      QUERY="create table if not exists messages($TABLEDESCR);"
  else
    if [ "$DELETE" == "1" ]; then
      QUERY="delete from messages;"
    else
      QUERY="drop table if exists new; create table new ($TABLEDESCR); rename table messages to messages$NOW, new to messages;"
    fi
  fi

  $SQLCLIENT -u $DATE_INFOLOGGER_MYSQL_USER --password=$DATE_INFOLOGGER_MYSQL_PWD -h $DATE_INFOLOGGER_MYSQL_HOST -D $DATE_INFOLOGGER_MYSQL_DB -B -N -e "$QUERY"
  

  # create tables for dAQ File Exchange Server filesystem
  QUERY="source  ${DATE_INFOLOGGER_DIR}/daqFES_create.sql"
  $SQLCLIENT -u $DATE_INFOLOGGER_MYSQL_USER --password=$DATE_INFOLOGGER_MYSQL_PWD -h $DATE_INFOLOGGER_MYSQL_HOST -D $DATE_INFOLOGGER_MYSQL_DB -B -N -e "$QUERY"

  

exit 0
