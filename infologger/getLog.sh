#!/bin/sh

# Get log messages from infoLogger database
#
# Log stream can be selected with -s option.
# DATE v4 message format can be selected with -o option.
# Default message output format is TAB delimited columns: timestamp,severity,hostname,pid,username,facility,dest,run,message
# NOTE: subsecond timestamp information is dropped

# 07 Apr 2005  SC  Added run number field (field number 8)
# 25 Apr 2005  SC  source config file

usage() {
  echo "Usage: $0 [-s logStream][-o]"
  echo "  -s : select given log stream"
  echo "  -o : output in DATE v4 infoLogger format"
}

STREAM=""
OLD=0

export OPTIND=1
while getopts 'hos:' OPTION; do
 case $OPTION in
  h|\?)
   usage
   exit 0 ;;
  s)
   STREAM="${OPTARG}" ;;
  o)
   OLD=1 ;;
 esac
done

# Setup DATE and infoLogger environment
if [ "${DATE_INFOLOGGER_DIR}" = "" ]; then
  DATE_INFOLOGGER_DIR=/date/infoLogger
fi
. ${DATE_INFOLOGGER_DIR}/infoLoggerConfig.sh --

  
  if [ "$MYSQL_PATH" == "" ]; then
    SQLCLIENT="mysql"
  else
    SQLCLIENT="$MYSQL_PATH/bin/mysql"
  fi

  CMD="$SQLCLIENT -u $DATE_INFOLOGGER_MYSQL_USER --password=$DATE_INFOLOGGER_MYSQL_PWD -h $DATE_INFOLOGGER_MYSQL_HOST -D $DATE_INFOLOGGER_MYSQL_DB -B -N -e"

  if [ "$OLD" == "1" ]; then
    SQLCMD='select concat(from_unixtime(timestamp,"%d%m%H%i%s"),"@",hostname,"@",":",pid,":","%",facility,"%","#",severity,"#","+",message) from messages'
    if [ "$STREAM" == "" ]; then
      $CMD "$SQLCMD;"
    else
      $CMD "$SQLCMD where dest=\"$STREAM\";"
    fi
  else
    SQLCMD='select from_unixtime(timestamp,"%d/%m/%Y %H:%i:%s"),severity,hostname,pid,username,facility,dest,run,message from messages'
    if [ "$STREAM" == "" ]; then
      $CMD "$SQLCMD;"
    else
      $CMD "$SQLCMD where dest=\"$STREAM\";"
    fi
  fi
