/*
** logfunc.c: Client side C-library for logging to infoLoggerReader
** 
** @author: Simon Lord
**
** $Id: libInfo.c,v 1.26 2007/11/09 17:10:01 chapelan Exp $
**
** Last changed by: $Author: chapelan $
**
** Revision History:
** 05/10/2004 Created
** 29/10/2004 Connect is now attempted 5 times over 5 seconds
** 01/11/2004 Now using SC's slog functions for error logging
** 02/11/2004 Bug fix in connectNow(), changed message format
**            added infoOpen()
** 08/11/2004 Added recovery code for when infoLoggerReader dies
** 23/11/2004 Added microsecond time precision for message timestamp
** 02/12/2004 Commenting and squigglies for everyone!!
** 16/12/2004 S.C.  Log only to file when LOG_TO_FILE defined at compile time.
** 10/01/2005 S.C.  Added printf-like formatted log function: infolog_f().
** 14/01/2005 Client now execs reader if it cannot connect to unix socket.
** 07/04/2005 Added runnumber to data flow when available
** 20/05/2005 Changed socket name from $DATE_SITE to 'infoLogger-$DATE_SITE'
** 17/11/2005 S.C.  Added infoLogTo_f : same as infoLog_f with stream.
              output logs to STDOUT only when DATE_INFOLOGGER_STDOUT=ONLY
              (nothing transmitted to infoLoggerReader). If TRUE, copy.
** 02/12/2005 S.C.  Added "system" to log fields, defined on startup by env. DATE_INFOLOGGER_SYSTEM
** 19/08/2006 S.C   Added automatic facility handling/definition
** 03/04/2007 S.C   Default log set to partition name if DATE_RC_NAME defined
** 04/05/2007 S.C   Added version + roleName to records sent to infologgerServer
** 03/08/2007 S.C   Added function infoLogS
** 08/11/2007 S.C   Added handling of 'debug' severity. Controlled by DATE_INFOLOGGER_DEBUG variable: if 1, messages kept, otherwise dropped
                    Added message flood prevention
                    Cleanup in doLog(). LOG_TO_FILE def removed (not needed any more)
**
*/

#define LOG_FACILITY "libInfo"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/times.h>
#include <sys/time.h>
#include <time.h>
#include <fcntl.h>
#include <assert.h>
#include <pwd.h>
#include <stdarg.h>
#include <sys/wait.h>
#include "simplelog.h"
#include "infoLogger.h"

#include <limits.h>

#define MAXRETRY 200
#define VERSION "$Revision: 1.26 $"

#ifndef FALSE
# define FALSE (0 == 1)
#endif
#ifndef TRUE
# define TRUE (0 == 0)
#endif

#define SP(l) l,sizeof(l)
#define AP(l) &l[strlen(l)],sizeof(l)-strlen(l)

extern struct tm *localtime_r(const time_t *, struct tm *);

/* Where the log files go in case DATE_SITE_LOGS is not defined */
#define DEFAULT_LOGS_ROOT "/tmp"

/* Begin of definition for format of the log records */
/* make sure definition of LOG_MAX_RECORD_SIZE is consistent with HDR_LOG_MESSAGE */

#define LOG_MAX_RECORD_HEADER_SIZE 256
#define LOG_MAX_RECORD_SIZE 4096
#define LOG_MAX_RECORD_TOTALSIZE LOG_MAX_RECORD_HEADER_SIZE+LOG_MAX_RECORD_SIZE

#define HDR_LOG_VERSION   "*1.2"
#define HDR_LOG_S         "#%1c"
#define HDR_LOG_TIMESTAMP "#%f"
#define HDR_LOG_HOSTID    "#%s"
#define HDR_LOG_ROLEID    "#%s"
#define HDR_LOG_PID       "#%d"
#define HDR_LOG_USER      "#%s"
#define HDR_LOG_SYSTEM    "#%s"
#define HDR_LOG_FACILITY  "#%s"
#define HDR_LOG_STREAM    "#%s"
#define HDR_LOG_RUNNUMBER "#%d"
#define HDR_LOG_KEYWORD   "#%s"
#define HDR_LOG_FILENAME  "#%s"
#define HDR_LOG_LINENR    "#%d"
#define HDR_LOG_COMPILE_DATE    "#%s"
#define HDR_LOG_COMPILE_TIME    "#%s"
#define HDR_LOG_MESSAGE   "#%s"
#define HDR_LOG_TRAILER   "\n"


#define LOG_RECORD_HDR  HDR_LOG_VERSION   \
                        HDR_LOG_S         \
                        HDR_LOG_TIMESTAMP \
                        HDR_LOG_HOSTID    \
                        HDR_LOG_ROLEID    \
                        HDR_LOG_PID       \
                        HDR_LOG_USER      \
                        HDR_LOG_SYSTEM    \
                        HDR_LOG_FACILITY  \
                        HDR_LOG_STREAM    \
                        HDR_LOG_RUNNUMBER \
                        HDR_LOG_KEYWORD   \
                        HDR_LOG_FILENAME  \
                        HDR_LOG_LINENR    \
                        HDR_LOG_COMPILE_DATE    \
                        HDR_LOG_COMPILE_TIME

#define LOG_CR '\f' /* Character used to replace CRs for multi-line msgs */

/* End of declaration for format of the log records */


#define STR_FACILITY_UNDEF   "unknown"  /* facility name when unknown */
#define STR_HOST_UNDEF       "unknown"  /* host name when unknown */
#define STR_USERNAME_UNDEF   "unknown"  /* user name when unknown */
#define STR_STREAM_UNDEF     "unknown"  /* stream when unknown */
#define STR_KEYWORD_UNDEF    "unknown"  /* keyword when unknown */
#define STR_FILENAME_UNDEF   "unknown"  /* filename when unknown */
#define STR_LINENR_UNDEF     0          /* line number when unknown */
#define STR_COMPILE_DATE_UNDEF   "unknown"  /* compile_date when unknown */
#define STR_COMPILE_TIME_UNDEF   "unknown"  /* compile_time when unknown */



static char  *hostId = NULL;            /* This pointer will also be used for 
					 * static initialisations' control */
static char   hostName[256];            /* Our host name */
static char   *roleName = NULL;         /* Our role name */
static pid_t  myPid;                    /* Our process ID */
static char   panicLog[PATH_MAX];       /* The panic log file */
static char   localLogFile[PATH_MAX];   /* Where our messages go if infoLoggerReader is dead */
static char   localFloodFile[PATH_MAX]; /* Where our messages go in case of message flood */
static char   logStream[32]=STR_STREAM_UNDEF;  /* the default log stream */
static char * siteLogs=DEFAULT_LOGS_ROOT;      /* path to directory to store log files if DATE_SITE_LOGS undefined */

static int                 logChannel=-1;  /* Channel used for I/O */
static int                 logToStdout=0;  /* Set to 1 if log messages should be copied to stdout - set when connecting, depending on DATE_INFOLOGGER_STDOUT */
static int                 logDebugMode=0; /* By default, messages with 'debug' severity are dropped - unless DATE_INFOLOGGER_DEBUG = 1 */

static unsigned int     floodStat_msgs_sec=0;
static unsigned int     floodStat_msgs_min=0;
static double           floodStat_time_lastsec=0;
static double           floodStat_time_lastmin=0;
static int              floodMode=0;           /* 0: no flood, 1: messages redirected to local file, 2: messages dropped */
static FILE             *floodFile_fp=NULL;    /* handle to flood file to store messages in excess */
static unsigned int     floodFile_msg=0;       /* messages recorded to flood file */
static unsigned int     floodFile_msg_drop=0;  /* messages dropped */
/* maximum messages per second, per minute, in flood file, and maximum number of messages in one minute to resume to normal operation */
#define FLOOD_MAXMSG_SEC    500U
#define FLOOD_MAXMSG_MIN    1000U
#define FLOOD_MAXMSG_FILE  10000U
#define FLOOD_MAXMSG_RESET  10U


static char * systemId = "";          /* name of the system originating log messages : DAQ, ECS, ... */

static int logLocal = 0;    /* If set to 1 then we log to a file */

static char *facilityName = STR_FACILITY_UNDEF;   /* Pointer to facility name           */
static char *username = STR_USERNAME_UNDEF;       /* Pointer to username                */
static char *undefKeyword = STR_KEYWORD_UNDEF;    /* Pointer to keyword                 */
static char *undefFilename = STR_FILENAME_UNDEF;  /* Pointer to filename                */
static int  undefLinenr = STR_LINENR_UNDEF;       /* Pointer to linenr                  */
static char *undefCompileDate = STR_COMPILE_DATE_UNDEF;  /* Pointer to compile_date     */
static char *undefCompileTime = STR_COMPILE_TIME_UNDEF;  /* Pointer to compile_time     */



static time_t start; /* used for calculating when to retry connecting to reader */

/* pointers to various env variables */
static char *dateSite;
static char *dateRoot;
static char *dateInfoLoggerDir;


/* Prototypes for functions */
static int     connectNow(int);
static int     writeMessage(int, char *);
static int     getHostName();
static int     setNonBlocking(int);
static char   *getUserName();
static void    infoPanic(const char * const);




/*
** Print version information into passed buffer pointer
*/
void getLogfuncVersion(char *buf, size_t size){
  snprintf(buf,size,VERSION);
}




/*
** Get users unix username and return it
*/
static char * getUserName(){
  struct passwd *passent;
  passent = getpwuid(getuid());
  if(passent == NULL){
    slog(SLOG_WARNING,"Could not get username: %s\n",strerror(errno));
    return NULL;
  }
  return passent->pw_name;
}




/*
** Get hostname from environment variable HOST and return.
** as we'll want the hostname in each message header.
*/
static int getHostName(){
  int status = 0;
  memset(hostName,0,sizeof hostName);
  status = gethostname(hostName,sizeof hostName);
  if(status == -1){
    memset(hostName,0,sizeof hostName);
    strncat(hostName,STR_HOST_UNDEF,sizeof hostName);
    infoPanic("Can't find hostname, using \"" STR_HOST_UNDEF "\" instead");
    return -1;
  }
  
  /* remove domain name from host name */
  /* cut the string at the first '.' */
  char * dotptr;
  dotptr=strchr(hostName,'.');
  if (dotptr!=NULL) *dotptr=0;
  
  return 0;
}


/*
** Get current DATE run number
** returns run number, or -1 if unknown
*/
static int getRunNumber(){
  int runnumber=-1;
  char *run;
  
  run=getenv("DATE_RUN_NUMBER");
  if (run==NULL) return -1;
  if (sscanf(run,"%d",&runnumber)!=1) return -1;
  return runnumber;   
}


/*
** Initialize all static variable other functions require
*/
static void initStaticVars(){

  if(hostId != NULL){
    return;
  }


  /* Ignore SIGPIPE so that we catch them on write(2) */
  signal(SIGPIPE,SIG_IGN);

  /*
  ** Register infoClose() function to be ran when client exits, allowing
  ** for a more graceful shutdown.
  */
  atexit(infoClose);
  umask(0);
  logChannel = -1;

  myPid = getpid();
  start = time(NULL);

  /* Find out our hostname */
  getHostName();
  hostId = hostName;
  
  /* Find out our role name */
  roleName=getenv("DATE_ROLE_NAME");
  if (roleName==NULL) roleName="";

  /* Find out our username */
  infoSetUserName(NULL);

  /* Find out system name */
  systemId = getenv("DATE_INFOLOGGER_SYSTEM");
  if (systemId==NULL) {
    systemId="";
  } 

  /* Set log file name */
  infoSetStream(getenv("DATE_RC_NAME"));

  /* Init local logs names */
  siteLogs = getenv("DATE_SITE_LOGS");
  if (siteLogs==NULL) siteLogs=DEFAULT_LOGS_ROOT;
  
  snprintf(SP(localLogFile),"%s/libInfo.msg@%s",siteLogs,hostId);
  snprintf(SP(panicLog),"%s/libInfo.err-%d@%s",siteLogs,(int)getpid(),hostId);

  slog_set_file(panicLog,SLOG_FILE_OPT_DONT_CREATE_NOW + SLOG_FILE_OPT_IF_UNSET);

  /* Get DATE_SITE for use an env variable to pass to execve of infoLoggerReader.sh */
  dateSite = getenv("DATE_SITE");
  if(dateSite == NULL||strlen(dateSite)<1){
    slog(SLOG_WARNING,"DATE_SITE not defined, may not be able to launch infoLoggerReader if required.");
  }

  /* Get DATE_ROOT for use an env variable to pass to execve of infoLoggerReader.sh */
  dateRoot = getenv("DATE_ROOT");
  if(dateRoot == NULL||strlen(dateRoot)<1){
    slog(SLOG_WARNING,"DATE_ROOT not defined, may not be able to launch infoLoggerReader if required.");
  }

  /* Get DATE_INFOLOGGER_DIR for use an env variable to pass to execve of infoLoggerReader.sh */
  dateInfoLoggerDir = getenv("DATE_INFOLOGGER_DIR");
  if(dateInfoLoggerDir == NULL||strlen(dateInfoLoggerDir)<1){
    slog(SLOG_WARNING,"DATE_INFOLOGGER_DIR not defined, may not be able to launch infoLoggerReader if required.");
  }

  /* Copy log messages to stdout? */
  char *e;
  e=getenv("DATE_INFOLOGGER_STDOUT");
  logToStdout=0;
  if (e!=NULL) {
    if (!strcmp("TRUE",e)) {logToStdout=1;}
    if (!strcmp("ONLY",e)) {logToStdout=2;}
  }
  
  /* Set debug mode? */
  e=getenv("DATE_INFOLOGGER_DEBUG");
  logDebugMode=0;
  if (e!=NULL) {
    if (!strcmp("1",e)) {logDebugMode=1;}
  }

}






/* 
   Create the log record and ship it to reader
*/
int createRecord(const char * facility,
		   const char * const stream,
		   const char         severity,
                   double time,
                    const char * const message,
                    int extargs,
                   ...) {
  
  int i;
  int status=0;
  char record[LOG_MAX_RECORD_TOTALSIZE];         /* buffer to store formatted message */
    
    char const *v_keyword = undefKeyword;
    char const *v_filename = undefFilename;
    int v_linenr = undefLinenr;
    char const *v_compileDate = undefCompileDate;
    char const *v_compileTime = undefCompileTime;
    
    if (extargs == 5) {
        va_list listPointer;
        va_start(listPointer, extargs);
        v_keyword = va_arg(listPointer, char const *);
        v_filename = va_arg(listPointer, char const *);
        v_linenr = va_arg(listPointer, int);
        v_compileDate = va_arg(listPointer, char const *);
        v_compileTime = va_arg(listPointer, char const *);
        va_end(listPointer);
    } else {
        if (extargs != 0) infoPanic("Invalid value for extargs");
    }
    
  /* If we are logging locally try and reconnect to reader */
  if(logLocal){
    /* Retry every 60 seconds or more */
    if(difftime(time,start)>=60.0){
      status = connectNow(1);
      start = time; /* Reset for next attempt */
    }
  } else {
    /* Connect to server if we're not already connected */
    if (logChannel == -1){
      logChannel = connectNow(0);
    }
  }
  
  /* If no log channel, drop the message */
  if (logChannel == -1) {
    return -1;
  }


  /* format the message */

  char const *v_facility=facility;
  char const *v_stream=stream;
  if (v_facility==NULL) {
    v_facility=facilityName;
  } else {
    if (facility[0]==0) v_facility=facilityName;
  }
  if (v_stream==NULL) {
    v_stream=logStream;
  } else {
    if (v_stream[0]==0) v_stream=logStream;
  }
  if (v_filename==NULL) {
    v_filename=undefFilename;
  } else {
    if (v_filename[0]==0) v_filename=undefFilename;
  }

  i=snprintf(SP(record),
	   LOG_RECORD_HDR,
	   severity,
	   time,
	   hostId,
           roleName,
	   (int)myPid,
	   username,
           systemId,
	   v_facility,
	   v_stream,
            getRunNumber(),
            v_keyword,
            v_filename,
            v_linenr,
            v_compileDate,
            v_compileTime
           );
  i+=snprintf(&record[i],sizeof(record)-i,HDR_LOG_MESSAGE,message);
  snprintf(&record[i],sizeof(record)-i,HDR_LOG_TRAILER);

  /* Sanity check for no CRs in the middle of the message: */
  for (i--; i>=0; i--){
      if (record[i] == '\n'){record[i] = LOG_CR;}
  }

  status=writeMessage(logChannel,record);

  return status;
} /* End of createRecord */





int createRecord_withargs(const char * facility,
		   const char * const stream,
		   const char         severity,
                   double time,
		   const char * const message,
                   ...){

  char buffer[LOG_MAX_RECORD_SIZE];         /* buffer to store formatted message */
  va_list     ap;                           /* list of additionnal params */

  va_start(ap, message);
  vsnprintf(buffer,LOG_MAX_RECORD_SIZE,message,ap);
  va_end(ap);

  return createRecord(facility,stream,severity,time,buffer,0);
}


/*
** Main log function for parsing log messages to
** infoLoggerReader process
*/
static void doLog(
		  const char * const facility,
		  const char * const stream,
		  const char         severity,
		  const char * const message,
                  int extargs,
                  ...){
  
  int         status = 0;         /* Holds return from system/function calls */

  /* init library */
  initStaticVars();

    char const *v_keyword = undefKeyword;
    char const *v_filename = undefFilename;
    int v_linenr = undefLinenr;
    char const *v_compileDate = undefCompileDate;
    char const *v_compileTime = undefCompileTime;
    
    if (extargs == 5) {
        va_list listPointer;
        va_start(listPointer, extargs);
        v_keyword = va_arg(listPointer, char const *);
        v_filename = va_arg(listPointer, char const *);
        v_linenr = va_arg(listPointer, int);
        v_compileDate = va_arg(listPointer, char const *);
        v_compileTime = va_arg(listPointer, char const *);
        va_end(listPointer);
    } else {
        if (extargs != 0) infoPanic("Invalid value for extargs");
    }

  /* check severity */
  switch (severity) {
    case LOG_BENCHMARK:
    case LOG_INFO:
    case LOG_WARNING:
    case LOG_ERROR:
    case LOG_FATAL:
    case LOG_IMPORTANT:
      break;
    case LOG_DDEBUG:
      /* skip debug messages unless debug mode set */
      if (!logDebugMode) return;
      break;
    default:
      return;
  }


  /* copy log messages to stdout? */
  if (logToStdout) {
    switch (severity) {
      case 'E':
        fprintf(stdout,"Error - ");
        break;
      case 'F':
        fprintf(stdout,"Fatal - ");
        break;
      default:      
        break;
    }
    fprintf(stdout,"%s\n",message);
    fflush(stdout);
    if (logToStdout==2) return;
  }


  /* Get microsecond timestamp */
  double  time = 0.0;  /* Holds microsecond accuracy time stamp */
  struct timeval tv;   /* Filled in by gettimeofday             */
  status = gettimeofday(&tv,NULL);
  if(status == -1){
    infoPanic("gettimeofday failed in doLog()");
  }
  time = (double)tv.tv_sec + (double)tv.tv_usec/1000000;


  /* update message statistics */
  if (time-floodStat_time_lastsec>1) {
    floodStat_time_lastsec=time;
    floodStat_msgs_sec=1;
  } else {
    floodStat_msgs_sec++;
  }
  if (time-floodStat_time_lastmin>60) {
    floodStat_time_lastmin=time;
    floodStat_msgs_min=1;
  } else {
    floodStat_msgs_min++;
  }
  
 
  /* message flood prevention */
  switch (floodMode) {
    case 0:
      if ((floodStat_msgs_sec>FLOOD_MAXMSG_SEC)||(floodStat_msgs_min>FLOOD_MAXMSG_MIN)) {
        floodFile_msg=0;
        floodFile_msg_drop=0;
        snprintf(SP(localFloodFile),"%s/libInfo.flood-%d@%s-%d",siteLogs,(int)getpid(),hostId,(int)time);
        floodFile_fp=fopen(localFloodFile,"w");
        if (floodFile_fp==NULL) {
          floodMode=2;
          createRecord_withargs(LOG_FACILITY,stream,LOG_INFO,time,"Message flood detected - further messages will be dropped (failed to create flood file %s)",localFloodFile);
          goto case2;
        } else {
          floodMode=1;
          createRecord_withargs(LOG_FACILITY,stream,LOG_INFO,time,"Message flood detected - further messages will be stored locally in %s",localFloodFile);
        }
      } else {
        break;
      }
    case 1:
      if (floodStat_msgs_min<FLOOD_MAXMSG_RESET) { 
        // reset flood mode
        break;
      }
      if (floodFile_msg>=FLOOD_MAXMSG_FILE) {
        if (floodFile_fp!=NULL) {
          fclose(floodFile_fp);
          floodFile_fp=NULL;
        }
        createRecord(LOG_FACILITY,stream,LOG_INFO,time,"Message flood - maximum entries in local file exceeded, further messages will be dropped", 5, v_keyword, v_filename, v_linenr, v_compileDate, v_compileTime);
        floodMode=2;
      } else {
        // log to flood file
        fprintf(floodFile_fp,"%f\t%c\t%s\t%s\t%s\n",time,severity,facility,stream,message);
        fflush(floodFile_fp);
        floodFile_msg++;
        return;
      }
    case 2:
    case2:
      if (floodStat_msgs_min<FLOOD_MAXMSG_RESET) { 
        // reset flood mode
        break;
      }
      // drop message
      floodFile_msg_drop++;
      return;
  }
  if (floodMode) {
    floodMode=0;
    if (floodFile_fp!=NULL) {
      fclose(floodFile_fp);
      floodFile_fp=NULL;
    }
    createRecord_withargs(LOG_FACILITY,stream,LOG_INFO,time,
      "Message flood - resuming normal operation: %u messages stored locally, %u messages dropped"
      ,floodFile_msg,floodFile_msg_drop);
  }
    
  /* create the log entry */
  if (createRecord(facility, stream, severity, time, message, 5, v_keyword, v_filename, v_linenr, v_compileDate, v_compileTime)) {
    infoPanic(message);
  }

  return;
}







/*
** Sets the passed fd to non-blocking io
*/
static int setNonBlocking(int s){

  long opts;

  opts = fcntl(s,F_GETFL);
  if (opts == -1){
    slog(SLOG_WARNING,"F_GETFL: %s: fcntl(2)\n",strerror(errno));
    return -1;
  }
  opts = (opts | O_NONBLOCK);
  if (fcntl(s,F_SETFL,opts) == -1){
    slog(SLOG_WARNING,
	 "Cannot set socket into non blocking mode: %s: fcntl(2)\n",
	 strerror(errno));
    return -1;
  }
  return 0;
}





/*
** Open connection to socket or file on disk if that fails
**
** if option not zero we will attempt to connect to reader
** without any sort of connect time out.
*/
static int connectNow(int option){

  int                 status =  0;  /* Holds return from system calls */
  int                 s      = -1;  /* Socket File descriptor         */
  int                 count;        /* Counter                        */
  int                 tmp_pid = 0;
  int                 forked = 0;

  char buffer[500];
  char scriptPath[256]; /* Path to infoLoggerReader.sh */
  char dateSiteEnv[256];
  char dateRootEnv[256];
  char socketName[256];

  char *myargv[2];
  char *myenv[3];

  static char        *path = NULL;  /* Pointer to the abstract path for connect */
  static struct sockaddr_un  addr1;        /* Holds details for connecting to infoLoggerReader */


  memset(scriptPath,0,sizeof scriptPath);
  memset(dateSiteEnv,0,sizeof dateSiteEnv);
  memset(dateRootEnv,0,sizeof dateRootEnv);
  /* Values required for starting infoLoggerReader if it's not running */
  snprintf(SP(scriptPath),"%s/infoLoggerReader.sh",dateInfoLoggerDir);
  snprintf(SP(dateSiteEnv),"DATE_SITE=%s",dateSite);
  snprintf(SP(dateRootEnv),"DATE_ROOT=%s",dateRoot);
  myargv[0] = scriptPath;
  myargv[1] = 0;
  myenv[0] = dateSiteEnv;
  myenv[1] = dateRootEnv;
  myenv[2] = 0;


  if(path == NULL||strlen(path)<1){
    memset(socketName,0,sizeof socketName);
    snprintf(SP(socketName),"infoLogger-%s",dateSite);
    
    path = socketName;
  }

  if(option){
    /* create a socket to work with */
    s = socket(PF_LOCAL,SOCK_STREAM,0);
    if(s == -1){
      slog(SLOG_ERROR,"%s: socket(2)\n",strerror(errno));
      return -1;
    }
    /* Connect */
    status = connect(s, (struct sockaddr *)&addr1, sizeof(addr1));
    if(!status){
      //      fflush(logChannel);
      close(logChannel);
      logChannel = -1;
      logLocal = 0;
      setNonBlocking(s);
      logChannel = s;
      slog(SLOG_INFO,"We have connected to infoLoggerReader after logging locally");
      /* Let reader know we have local messages stored */
      memset(buffer,0,sizeof buffer);
      snprintf(buffer,sizeof buffer,
	       "We have just connected and have messages stored locally, please retrieve from %s",localLogFile);
      doLog( LOG_FACILITY, NULL,'I', buffer,0);
    }
    return status;
  }

  if(!logLocal && (logChannel == -1)){
    /* Get the env var to tell us where to connect to */
    if(path == NULL){
      memset(socketName,0,sizeof socketName);
      snprintf(SP(socketName),"infoLogger-%s",dateSite);      
      path = socketName;
      if(path == NULL||strlen(path)<1){
	slog(SLOG_WARNING,"DATE_SITE not defined");

      }
    }

    /* Create a socket to work with */
    s = socket(PF_LOCAL,SOCK_STREAM,0);
    if(s == -1){
      slog(SLOG_ERROR,"%s: socket(2)\n",strerror(errno));
      status=-1;
    } 
    else {
      /* Init sockaddr */
      bzero(&addr1, sizeof(addr1));
      assert(sizeof path < sizeof addr1.sun_path);
      addr1.sun_family = PF_LOCAL;
      /* +1 so that we use abstract address, see man 7 unix for details!*/
      strncpy(addr1.sun_path+1, path, strlen(path)+1);
      
      /* Try and connect every second for 5 seconds */
      for(count = 0;count < 5;count++){    
        status = connect(s, (struct sockaddr *)&addr1, sizeof(addr1));
	if(status == -1 && !forked){
	  /* infoLoggerReader is not running, so we'll start it */
	  if((tmp_pid = fork()) == -1){
	  /* fork failed */
	  }
	  else if(tmp_pid == 0){
	    /* Child */
	    forked = 1;
	    execve(scriptPath,myargv,myenv);
	    slog(SLOG_ERROR,"infoLoggerReader exec failed: %s",strerror(errno));
	    exit(0); /* This might be too drastic! */
	  }
	  else{
	    wait(NULL); /* wait for script to exit before continuing */
	    forked = 1; /* so we don't fork again on the next iteration of the for loop */
	    sleep(2); /* Script ends before infoLoggerReader is ready to receive messages */
	  }
	}
	else if(status == -1){
	  sleep(1);
	}
        else {
	  /*slog(SLOG_INFO,"Connected to %s, fd: %d\n",addr1.sun_path+1,s);*/
	  
	  break;
        }
      }
    } 
  }
  
  /*
  ** If we haven't connected to infoLoggerReader then
  ** start logging locally.
  */
  if((status == -1) || logLocal){
    if(status == -1){
      slog(SLOG_WARNING, "%s: connect(2)\n",strerror(errno));
      slog(SLOG_WARNING,"Couldn't open socket to infoLogger Reader\n");
    }
    /* No infoLoggerReader running, log to file */
    logLocal = 1;
    
    s = open(localLogFile,
	     O_CREAT|O_WRONLY|O_APPEND,
	     S_IROTH|S_IWOTH|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);
    
    if(s == -1){
      slog(SLOG_ERROR,"%s: open(2): %s\n",strerror(errno),localLogFile);
    }

    /*Let people know where the messages are going. */
    slog(SLOG_INFO,"(pid %d) Outputing to: %s\n",getpid(),localLogFile);
  }

  setNonBlocking(s);
  return s;
}






/*
** Set default logfile
*/
void infoSetStream(const char * const stream ){
  initStaticVars();

  if (stream==NULL) {
    snprintf(SP(logStream),"%s",STR_STREAM_UNDEF);
  } else if (!strncmp(stream,"ALL",3)) {
    snprintf(SP(logStream),"%s",&stream[3]);
  } else {
    snprintf(SP(logStream),"%s",stream);
  }
}




/* 
** Log a message to the default log file. 
*/
void infoLog( const char * const facility,
              const char         severity,
              const char * const message ) {
  doLog( facility, NULL, severity, message, 0 );
}


/*
** Extended log message including filename
*/
void extendedInfoLog(   const char severity,
                        const char * const message,
                        const char * const keyword,
                        const char * const filename,
                        int linenr,
                        const char * const compile_date,
                        const char * const compile_time) {
    doLog(NULL, NULL, severity, message, 5, keyword, filename, linenr, compile_date, compile_time);
}


/*
** Log a message to a given log file. 
*/
void _infoLogTo( const char * const facility,
                 const char * const stream,
                 const char         severity,
                 const char * const message ) {
  doLog( facility, stream, severity, message, 0);
}











/*
** Write the passed message to the passed fd
*/
static int writeMessage(int fd, char *msg){

  int status; /* Holds return form system calls */
  int count;  /* Counter                        */

  for(count = 0; count < MAXRETRY; count++){
    errno = 0;
    status = write(fd, msg, strlen(msg));
    if(status > 0){
      return 0;
    }
    else if((status == -1) && (errno == EAGAIN)){
      usleep(300);
    }
    else if((status == -1) && (errno == EPIPE)){
      slog(SLOG_WARNING,
	   "(pid %d) %s:InfoLoggerReader died, logging to disk.\n",
	   getpid(),
	   strerror(errno));
      close(logChannel);
      logChannel = -1;
      logLocal = 1;
      logChannel = connectNow(0);
      status = writeMessage(logChannel,msg);
      return status;
    }
    else{
      slog(SLOG_WARNING,"%d:%s: write(2)\n",errno,strerror(errno));
      return -1;
    }
  }
  return -1;
}/* End of writeMessage */






/*
** Initialize and open communications with infoLoggerReader
*/
void infoOpen(){
  initStaticVars();
  logChannel = connectNow(0);
}





/*
** Close open fd if possible
*/
void infoClose(){

  int status = 0; /* Holds return from system calls */

  /* delete username */
  if ((username!=NULL) && (username!=STR_USERNAME_UNDEF)) {
    free(username);
  }
  username=STR_USERNAME_UNDEF;

  if(logChannel != (-1)){
    if(!logLocal){
      status = shutdown(logChannel, SHUT_WR);
    }
    if(status == -1){
      printf("%s : infoClose shutdown(2)",strerror(errno));
    }
    status = close(logChannel);
    if(status == -1){
      printf("%s : infoClose close(2)",strerror(errno));
    }
    logChannel = -1;
  }

  hostId=NULL;
}




/*
** Print the description message plus the relevant data into our
** "private" log file
*/
static void infoPanic( const char * const msg ) {
  slog(SLOG_WARNING,
       "PANIC: \"%s\" :: logChannel:%d panicLog=\"%s\"\n",
       msg,logChannel,panicLog);

} /* End of infoPanic */




/* Log a message to the default log stream. */
/* printf-like message formatting, variable number of arguments */
void infoLog_f( const char * const facility,
	      const char         severity,
	      const char * const message,
              ... ) {

  char buffer[LOG_MAX_RECORD_SIZE];         /* buffer to store formatted message */
  va_list     ap;                           /* list of additionnal params */

  va_start(ap, message);
  vsnprintf(buffer,LOG_MAX_RECORD_SIZE,message,ap);
  va_end(ap);

  doLog( facility, NULL, severity, buffer, 0 );
} /* End of infoLog_f */


/* Log a message to the specified log stream */
/* printf-like message formatting, variable number of arguments */
void infoLogTo_f( const char * const stream,
                  const char * const facility,
                  const char         severity,
                  const char * const message,
                  ... ) {

  char buffer[LOG_MAX_RECORD_SIZE];         /* buffer to store formatted message */
  va_list     ap;                           /* list of additionnal params */

  va_start(ap, message);
  vsnprintf(buffer,LOG_MAX_RECORD_SIZE,message,ap);
  va_end(ap);

  doLog( facility, stream, severity, buffer, 0 );
} /* End of infoLogTo_f */


/* set username - if NULL, take current user */
void infoSetUserName( const char * name) {
 
  /* first delete previous one */
  if ((username!=NULL) && (username!=STR_USERNAME_UNDEF)) {
    free(username);
  }

  /* assign new user name */  
  if (name==NULL) {
    name=getUserName();
  } else {
    if (strlen(name)==0) {
      name=getUserName();
    }
  }
  if (name==NULL) {
    username=STR_USERNAME_UNDEF;
  } else {
    username=strdup(name);
  } 
  if (username==NULL) {
    username=STR_USERNAME_UNDEF;
  }
} /* End of infoSetUserName */


/* define default facility - if NULL, use default */
void infoSetFacility(const char * const facility) {
  /* first delete previous one */
  if ((facilityName!=NULL) && (facilityName!=STR_FACILITY_UNDEF)) {
    free(facilityName);
  }

  if (facility==NULL) {
    facilityName=STR_FACILITY_UNDEF;
  } else {
    facilityName=strdup(facility);
    if (facilityName==NULL) {
      facilityName=STR_FACILITY_UNDEF;
    }
  }
}


/* send log message */
extern void infoLogS_f(	const char         severity,
			const char * const message,
                        ... ) {
  char buffer[LOG_MAX_RECORD_SIZE];         /* buffer to store formatted message */
  va_list     ap;                           /* list of additionnal params */

  va_start(ap, message);
  vsnprintf(buffer,LOG_MAX_RECORD_SIZE,message,ap);
  va_end(ap);

  doLog( NULL, NULL, severity, buffer, 0 );
} /* End of infoLogS_f */


/* print msg stats */
void infoPrintStat() {
  printf("sec: %lf => %u\n",floodStat_time_lastsec,floodStat_msgs_sec);
  printf("min: %lf => %u\n",floodStat_time_lastmin,floodStat_msgs_min);
}
