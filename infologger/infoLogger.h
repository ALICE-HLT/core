/*
**	infoLogger.h
**	============
**
** Definitions for the infoLogger facilities. Should be included in all
** modules using the DATE infoLogger package.
**
** Predefined symbols:
**
**	LOG_FACILITY	The description of the facility issuing log messages
**			(optional)
**
**	PACKAGE_NAME	Used as LOG_FACILITY if this is not defined (should
** 			be automatically defined by the DATE GNUmakefiles)
**
**	__FILE__	Used as LOG_FACILITY if nothing else is defined
** 			(should be automatically defined by the C compiler)
**
** Output symbols:
**
**	LOG_LOGBOOK		The default logBook file
**	LOG_INFO		Informational message tag
**	LOG_ERROR		Error message tag
**	LOG_FATAL		Fatal message tag
**      LOG_DDEBUG              Debug message tag
**	LOG_MAX_RECORD_SIZE	Max record size for log messages
**	LOG( )			Log message to the default log file
**
**	INFO( )			Log info message
**	ERROR( )		Log error message
**	FATAL( )		Log fatal message
**	DEBUG( )		Log debug message
**
**	LOG_ERROR_TH		Threshold for error log messages
**	LOG_NORMAL_TH		Threshold for normal log messages
**	LOG_DETAILED_TH		Threshold for detailed log messages
**	LOG_DEBUG_TH		Threshold for debug log messages
**
**	LOGBOOK( )		Log message to the logbook
**	DATE_LOGBOOK_MARKER	Add an End-of-record logbook marker
**
** The strategy behind different types of log messages is the following:
**
**   Severities:
**	Info	Messages concerning normal functioning
**	Error	Errors detected during normal functioning, recover possible
**	Fatal	Fatal condition, execution cannot continue
**      Debug   Debugging messages, dropped unless in debugging mode (DATE_INFOLOGGER_DEBUG variable set to 1)
**
**   Thresholds
**	---		No log messages, execution at full speed, total
**			blackup from all sources
**	Error		Error messages only
**	Normal		Normal functioning mode, execution 100% effective,
**			some messages logged
**	Detailed	Some extra information, execution effective but
**			could be slower then usual
**	Debug		Maximum detailed level, execution could be heavily
**			effected
**
**  03/01/2005  S.C.  Added infoLog_f(), same as infolog but printf like: variable arguments ok, string formatted. No need of intermediate buffer.
**  04/04/2007  S.C.  Removed all macros specifying the log stream.
**  08/11/2007  S.C.  Added debug severity
*/

#ifndef __infoLogger_h__
#define __infoLogger_h__

#ifndef LOG_FACILITY
# ifdef PACKAGE_NAME
#  define LOG_FACILITY PACKAGE_NAME
# else
#  ifdef __FILE__
#   define LOG_FACILITY __FILE__
#  else
#   define LOG_FACILITY "UNKNOWN"
#  endif
# endif
#endif

#define LOG_LOGBOOK    		"logBook"

#ifdef LOG_INFO
#undef LOG_INFO
#endif

#define LOG_BENCHMARK 'B'
#define LOG_INFO  'I'
#define LOG_WARNING 'W'
#define LOG_ERROR 'E'
#define LOG_FATAL 'F'
#define LOG_IMPORTANT 'M'
#define LOG_DDEBUG 'D'

#define LOG( severity, message ) \
                   infoLog(    LOG_FACILITY,       severity, message )

#define INFO( message )            LOG(           LOG_INFO,  message )
#define ERROR( message )           LOG(           LOG_ERROR, message )
#define FATAL( message )           LOG(           LOG_FATAL, message )
#define DEBUG( message )           LOG(           LOG_DDEBUG, message )

#define LOG_ERROR_TH	1	/* Errors only */
#define LOG_NORMAL_TH	10	/* Normal functioning mode */
#define LOG_DETAILED_TH	20	/* Some extra information */
#define LOG_DEBUG_TH	30	/* Debug, execution heavily effected */

#define LOGBOOK( message ) _infoLogTo(  LOG_FACILITY, LOG_LOGBOOK, LOG_INFO, message )
#define LOGBOOK_EOR        "+++++++++++++++++++++++++++++++++++++++"
#define LOGBOOK_MARKER     LOGBOOK( LOGBOOK_EOR )


extern void infoLog(    const char * const facility,
			const char         severity,
			const char * const message );

extern void extendedInfoLog(    const char severity,
                                const char * const message,
                                const char * const keyword,
                                const char * const filename,
                                int linenr,
                                const char * const compile_date,
                                const char * const compile_time);

extern void _infoLogTo(  const char * const facility,
			const char * const fileName,
			const char         severity,
			const char * const message );

/* define default log stream (pointer must remain valid) - NULL resets to default */
/* THIS FUNCTION SHOULD NOT BE USED unless called from the runControl */
extern void infoSetStream( const char * const fileName );

extern void infoClose();
extern void infoOpen(); /* optionnal */
/* facility can be NULL or a zero-length string, in which case the default facility is used */
extern void infoLog_f(  const char * const facility,
			const char         severity,
			const char * const message,
                        ... ); /* printf-like formatted log function */
extern void _infoLogTo_f(  const char * const stream,
                          const char * const facility,
			  const char         severity,
			  const char * const message,
                        ... ); /* printf-like formatted log function - with stream specification*/
                        
extern void infoSetUserName(  const char * username); /* set username - if NULL, take current user */

extern void infoSetFacility(const char * const facility); /* define default log facility (pointer must remain valid) - NULL resets to default */
extern void infoLogS_f(	const char         severity,
			const char * const message,
                        ... ); /* (S means short) : printf-like formatted log function, using facility defined by infoSetFacility */

#endif
