set(CTEST_SOURCE_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
set(CTEST_BINARY_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

set(CTEST_CMAKE_GENERATOR "Unix Makefiles")

set(MODEL analysis)

ctest_start(${MODEL} TRACK ${MODEL} )

# Add output from checkout to res?
execute_process(COMMAND git pull
                        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                        RESULT_VARIABLE GIT_PULL_RESULT
                        OUTPUT_VARIABLE GIT_PULL_OUTPUT
                        ERROR_VARIABLE GIT_PULL_ERROR)

MESSAGE("git pull: ${GIT_PULL_OUTPUT}")

ctest_configure(BUILD "${CTEST_BINARY_DIRECTORY}" RETURN_VALUE res)

ctest_build(BUILD "${CTEST_BINARY_DIRECTORY}" RETURN_VALUE res)

ctest_test(BUILD "${CTEST_BINARY_DIRECTORY}" RETURN_VALUE res)

ctest_submit(RETURN_VALUE res)

