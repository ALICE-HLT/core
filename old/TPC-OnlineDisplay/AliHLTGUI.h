/************************************************************************
**
** This file is property of and copyright by the Computer Science/Computer 
** Engineering Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2005
** This file has been written by Jochen Thaeder, 
** thaeder@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://www.kip.uni-heidelberg.de/ti/HLT/
** or the corresponding page of the Heidelberg Alice HLT group.
**
*************************************************************************/

#include "AliHLTGUIMainForm.h"
#include "AliHLTTPCTrackArray.h"
#include <vector>

using namespace std;

class AliHLTGUI : public AliHLTGUIMainForm {
    Q_OBJECT

public:
    AliHLTGUI();
    ~AliHLTGUI();

public slots:
    void connectDisplay();          // connect button
    void nextEvent();               // next event button
    void redisplayEvent();          // redisplay button
    void setSector();               // set sector to display
    void setPadRow();               // set padrow,slice to display
    void setPad();                  // set pad in (above) padrow to display
    void set3D();                   // What to display in 3D: cluster, tracks, geometry, padrow
    void setTrack();                // 
    void setCluster();              // Which cluster to display: all, used, unused
    void setInvert();               // Invert 3D Display
    void setSelectTrack();          // turn single track on/off
    void selectTrackSector();        // select slice for single track
    void selectTrack();             // Select single track    
    void addHost();                 // Add host to hostlist
    void removeHost();              // Remove host from hostlist
    void setKeepView();             // set keep angles when redisplaying     
    
protected:

    void displayEvent(bool nextSwitch, bool threeDSwitch = TRUE, bool histPadRowSwitch= TRUE);

    TQtWidget *padrowHistWidget;
    TQtWidget *pad1HistWidget;
    TQtWidget *pad2HistWidget;
    TQtWidget *pad3HistWidget;
    TQtWidget *residualsHistWidget;
    TQtWidget *chargeHistWidget;
    TQtWidget *geomWidget;

    bool fConnected;
    
    int fTracksPerSlice[36];   // TrackCount per slice

    void* fODHReader;          //really HOMERReader*
    void* fODHDisplay;          //really AliHLTTPCDisplay* 

    float fTheta;
    float fPhi;

    vector<QString> fHostnames;
    vector<QString> fPorts;
    
    AliHLTTPCTrackArray* fTrackArray;
};
