/************************************************************************
**
** This file is property of and copyright by the Computer Science/Computer 
** Engineering Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2005
** This file has been written by Jochen Thaeder, 
** thaeder@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://www.kip.uni-heidelberg.de/ti/HLT/
** or the corresponding page of the Heidelberg Alice HLT group.
**
*************************************************************************/

// Online Display for the Alice HLT/TPC - GUI
// One has to use this class together with AliHLTTPCDisplay, as these class provides the basic functions
// Displays:
// - 3D Geometry
//   - display padrows, cluster, tracks
//   - select single Tracks
//   - select used / unused cluster
// - Histograms
//   - display padrows
//   - display pads in padrows
// Using the HOMER libraries, the GUI can be connected to running TCP-Dump-Subscriber of a running analysis chain

#include "AliHLTGUI.h"

#include <TQtWidget.h>
#include <qlabel.h>
#include <qcheckbox.h>
#include <qspinbox.h>
#include <qbuttongroup.h>
#include <qgroupbox.h>
#include <qpushbutton.h>
#include <qlineedit.h>
#include <qlistview.h>
#include <qslider.h>

#include "TCanvas.h"
#include "TH1.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TAttFill.h"
#include "TH1F.h"
#include "TFormula.h"
#include "TF1.h" 
#include "TSystem.h"
#include "TString.h"

#include "AliHLTDataTypes.h"
#include "AliHLTTPCSpacePointData.h"
#include "AliHLTTPCClusterDataFormat.h"
#include "AliHLTTPCTrackletDataFormat.h"
#include "AliHLTTPCTrackArray.h"
#include "AliHLTTPCDefinitions.h"
#include "AliHLTTPCDigitReader.h"
#include "AliHLTTPCDisplay.h"
#include "AliHLTTPCTransform.h"
#include "AliHLT_C_Component_WrapperInterface.h"
#include "HOMERReader.h"

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <iostream>
#include <vector>

#include <stdio.h>         // include because of connecttofile()
#include <stdlib.h>        // include because of connecttofile()

using namespace std;


// ####################################################################################################
AliHLTGUI::AliHLTGUI() {

    padrowHistWidget = tQtWidgetPadRow; 
    residualsHistWidget = tQtWidgetResiduals;
    geomWidget = tQtWidget3D;
    pad1HistWidget  = tQtWidgetPad1; 
    pad2HistWidget  = tQtWidgetPad2; 
    pad3HistWidget  = tQtWidgetPad3; 
    chargeHistWidget = tQtWidgetCharge;

    fTheta = 90.;
    fPhi = 0.;

    fTrackArray = NULL;
    fConnected = kFALSE;
    
    TString geoPath;

    cout << "Creating display" << endl;

    Char_t *gfile="alice.geom"; // geometrie file
    Char_t * path_to_geom_file = "../GEO";

    if ( path_to_geom_file ){	
	geoPath = path_to_geom_file;
	geoPath += "/";
    }
    else {
	geoPath = "";
    }
    geoPath += gfile;

    AliHLTTPCDisplay* display = new AliHLTTPCDisplay( (Char_t*)geoPath.Data() );
   
    fODHDisplay = (void*)display;
   
    display->SetupHist();
}
// ####################################################################################################
AliHLTGUI::~AliHLTGUI() {     
    if ( fTrackArray )  delete fTrackArray;   
    fTrackArray = NULL;
}

// ####################################################################################################
void AliHLTGUI::connectDisplay() {      

    int cnt = fHostnames.size();
    int ret;
      
    unsigned short* ports = new unsigned short[cnt];
    const char** hostnames = new const char*[cnt];

    for ( Int_t i = 0; i < cnt; i++ ) {
	hostnames[i] = new char[50];
	hostnames[i] = fHostnames[i];
	ports[i] = (unsigned short) atoi(fPorts[i]);
    }

    if (fConnected) {
	cout << "Disonnecting..."    << endl;

	if ( !fODHReader ) return;
	HOMERReader* reader = (HOMERReader*)fODHReader;
	delete reader;
	fODHReader = NULL;

	connectTextLabel->setText("DISCONNECTED");
	connectTextLabel->setPaletteForegroundColor(red);
	redisplayGroupBox->setEnabled(FALSE);
	redisplayPushButton->setEnabled(FALSE);
	nextEventGroupBox->setEnabled(FALSE);
	
	threeDGroupBox->setEnabled(FALSE);
	sectorButtonGroup->setEnabled(FALSE);
	padrowGroupBox->setEnabled(FALSE);
	
	HostListTextLabel->setEnabled(TRUE);
	hostListView->setEnabled(TRUE);
	removePushButton->setEnabled(TRUE);
	addGroupBox->setEnabled(TRUE);
	connectPushButton->setText("Connect");
	fConnected = kFALSE;    
	return;
    }

 
    connectTextLabel->setPaletteForegroundColor(white);
    connectTextLabel->setText("CONNECTING...");
    cout << "Connecting..."    << endl;

    Int_t tries = 0;

    if (cnt == 0){
	printf( "Error establishing connection: No Hostnames specified\n" );
	connectTextLabel->setText("ERROR");  
	connectTextLabel->setPaletteForegroundColor(red);
	return;
    }

    while(1) {
	HOMERReader* reader = new HOMERReader( cnt, (const char**) hostnames, ports );

	connectTextLabel->setText("CONNECTING");
	connectTextLabel->setPaletteForegroundColor(white);

	ret=reader->GetConnectionStatus();

	if ( ret ) {	
	    int ndx = reader->GetErrorConnectionNdx();
	    if ( ndx < cnt) {
		connectTextLabel->setText("ERROR");
		connectTextLabel->setPaletteForegroundColor(red);
		printf( "Error establishing connection to TCP source %s:%hu: %s (%d)\n", hostnames[ndx], ports[ndx], strerror(ret), ret );
		return;
	    }
	    else {   
		connectTextLabel->setText("ERROR");
		connectTextLabel->setPaletteForegroundColor(red);
		printf( "Error establishing connection to unknown source with index %d: %s (%d)\n",ndx, strerror(ret), ret );
		return;
	    }	
	    delete reader;
	    reader = NULL;

	}

	cout << "TRY = " << tries << " || " ;
	tries++;

	int  ret1 = reader->ReadNextEvent();

	if ( ret1 ) {
	    int ndx = reader->GetErrorConnectionNdx();
	    printf( "Error reading event from source %d: %s (%d)\n", ndx, strerror(ret1), ret1 );
	    delete reader;
	    reader = NULL;
	    continue;
	}

	unsigned long blockCnt = reader->GetBlockCnt();
	printf( "Event 0x%016LX (%Lu) with %lu blocks\n", (ULong64_t)reader->GetEventID(), (ULong64_t)reader->GetEventID(), blockCnt );

	if (blockCnt == 0){
	    delete reader;
	    reader = NULL;
	    continue;
	}
	fODHReader = (void*) reader;
	break;
    }
       
    connectTextLabel->setText("CONNECTED");
    connectTextLabel->setPaletteForegroundColor(green);
    redisplayGroupBox->setEnabled(TRUE);
    redisplayPushButton->setEnabled(TRUE);
    nextEventGroupBox->setEnabled(TRUE);
    
    threeDGroupBox->setEnabled(TRUE);
    sectorButtonGroup->setEnabled(TRUE);
    padrowGroupBox->setEnabled(TRUE);
    
    HostListTextLabel->setEnabled(FALSE);
    hostListView->setEnabled(FALSE);
    removePushButton->setEnabled(FALSE);
    addGroupBox->setEnabled(FALSE);
    connectPushButton->setText("Disconnect");
    fConnected = kTRUE;    

    nextEvent();
}

// ####################################################################################################
void AliHLTGUI::nextEvent(){
    displayEvent(kTRUE);
}

// ####################################################################################################
void AliHLTGUI::redisplayEvent(){
    displayEvent(kFALSE);
}

// ####################################################################################################
void AliHLTGUI::displayEvent(Bool_t nextSwitch, Bool_t threeDSwitch, Bool_t PadRowSwitch){

    if ( !fODHReader || !fODHDisplay ) {
    	printf( "ERROR no READER or DISPLAY" );
	return;
    }

    HOMERReader* reader = (HOMERReader*)fODHReader;
    AliHLTTPCDisplay* display = (AliHLTTPCDisplay*)fODHDisplay;

    // -- input datatypes , reverse
    char* spptID="SRETSULC";
    char* trkID = "SGESKART";
    char* padrowID = "KPWR_LDD";
    Int_t ret;
    
    if (nextSwitch) {
	ret = reader->ReadNextEvent();

	if ( ret ) {
	    int ndx = reader->GetErrorConnectionNdx();
	    printf( "------------ TRY AGAIN --------------->Error reading event from source %d: %s (%d)\n", ndx, strerror(ret), ret );
	    return ; 
	}
   
	unsigned long blockCnt = reader->GetBlockCnt();
	printf( "Event 0x%016LX (%Lu) with %lu blocks\n", (ULong64_t)reader->GetEventID(), (ULong64_t)reader->GetEventID(), blockCnt );
	
	for ( unsigned long i = 0; i < blockCnt; i++ ) {
	    char tmp1[9], tmp2[5];
	    memset( tmp1, 0, 9 );
	    memset( tmp2, 0, 5 );
	    void *tmp11 = tmp1;
	    ULong64_t* tmp12 = (ULong64_t*)tmp11;
	    *tmp12 = reader->GetBlockDataType( i );
	    void *tmp21 = tmp2;
	    ULong_t* tmp22 = (ULong_t*)tmp21;
	    *tmp22 = reader->GetBlockDataOrigin( i );
	    printf( "Block %lu length: %lu - type: %s - origin: %s\n",i, reader->GetBlockDataLength( i ), tmp1, tmp2 );
	}
   
	for(Int_t ii=0;ii<36;ii++) fTracksPerSlice[ii] = 0;
    }
    
    connectTextLabel->setText("READ DATA");
    connectTextLabel->setPaletteForegroundColor(green);  

    //--------------------------------------------------------------------------------------------
    // READ CLUSTER DATA
    //-------------------------------------------------------------------------------------------- 
    if (nextSwitch) {
	unsigned long blk = reader->FindBlockNdx( spptID, " CPT",0xFFFFFFFF );

	while ( blk != ~(unsigned long)0 ) {	    
	    printf( "Found clusters block %lu\n", blk );
	    const AliHLTTPCClusterData* clusterData = (const AliHLTTPCClusterData*)reader->GetBlockData( blk );
	    if ( !clusterData ) {
		printf( "No track data for block %lu\n", blk );
		continue;
	    }
	
	    ULong_t spec = reader->GetBlockDataSpec( blk );
	    Int_t patch = AliHLTTPCDefinitions::GetMinPatchNr( spec );
	    Int_t slice = AliHLTTPCDefinitions::GetMinSliceNr( spec );
	    printf( "%lu Clusters found for slice %u - patch %u\n", clusterData->fSpacePointCnt, slice, patch );
	
	    void* tmp30 = (void*)clusterData;
	    Byte_t* tmp31 = (Byte_t*)tmp30;
	    unsigned long offset;
	    offset = sizeof(clusterData->fSpacePointCnt);
	    if ( offset <= reader->GetBlockTypeAlignment( blk, 1 ) ) offset = reader->GetBlockTypeAlignment( blk, 1 );
	    tmp31 += offset;
	    tmp30 = tmp31;
	    AliHLTTPCSpacePointData* tmp32 = (AliHLTTPCSpacePointData*)tmp30;

	    display->SetupCluster( slice, patch, clusterData->fSpacePointCnt, tmp32 );
	    
	    blk = reader->FindBlockNdx( spptID, " CPT", 0xFFFFFFFF, blk+1 );
	}
    }

    //--------------------------------------------------------------------------------------------
    // READ TRACKS
    //-------------------------------------------------------------------------------------------- 
    if (nextSwitch) {
	if ( fTrackArray ) delete fTrackArray; 
	fTrackArray = new AliHLTTPCTrackArray;

	unsigned long blk = reader->FindBlockNdx( trkID, " CPT", 0xFFFFFFFF );

	while ( blk != ~(unsigned long)0 ) {
	    printf( "Found tracks in block %lu\n", blk );
	    const AliHLTTPCTrackletData* trackData = (const AliHLTTPCTrackletData*)reader->GetBlockData( blk );
	    if ( !trackData ) {
		printf( "No track data for block %lu\n", blk );
		continue;
	    }
	    
	    ULong_t spec = reader->GetBlockDataSpec( blk );
	    Int_t patchmin = AliHLTTPCDefinitions::GetMinPatchNr( spec );
	    Int_t patchmax = AliHLTTPCDefinitions::GetMaxPatchNr( spec );
 	    Int_t slice = AliHLTTPCDefinitions::GetMinSliceNr( spec );  
	    printf( "%lu tracks found for slice %u - patch %u-%u\n", trackData->fTrackletCnt, slice, patchmin, patchmax );

	    fTracksPerSlice[slice] = trackData->fTrackletCnt;
	    
	    void* tmp40 = (void*)trackData;
	    Byte_t* tmp41 = (Byte_t*)tmp40;
	    unsigned long offset;
	    offset = sizeof(trackData->fTrackletCnt);
	    if ( offset <= reader->GetBlockTypeAlignment( blk, 1 ) ) offset = reader->GetBlockTypeAlignment( blk, 1 );
	    tmp41 += offset;
	    tmp40 = tmp41;
	    AliHLTTPCTrackSegmentData* tmp42 = (AliHLTTPCTrackSegmentData*)tmp40;

	    fTrackArray->FillTracks( trackData->fTrackletCnt, tmp42, slice );

 	    blk = reader->FindBlockNdx( trkID, " CPT", 0xFFFFFFFF, blk+1 );	
	}
	
	display->SetupTracks( fTrackArray );  
    } 

    //--------------------------------------------------------------------------------------------
    // READ CLUSTER DATA for PADROW HISTOGRAM
    //--------------------------------------------------------------------------------------------
    if (PadRowSwitch) {
	AliHLTUInt8_t padslice = (AliHLTUInt8_t) display->GetSlicePadRow();
	Int_t padrow = display->GetPadRow();
	Int_t patch =  AliHLTTPCTransform::GetPatch(padrow);
	Int_t maxpatch = patch;
	
	display->ResetHistPadRow();
	
	padSpinBox->setMaxValue(display->GetNPads());
	padSlider->setMaxValue(display->GetNPads());
	
	if (padrow == 30 || padrow == 90 || padrow == 139) maxpatch++;
	
	for (Int_t tpatch=patch;tpatch <= maxpatch;tpatch++) {
	    unsigned long blk;
	    AliHLTUInt32_t padrowSpec = AliHLTTPCDefinitions::EncodeDataSpecification( padslice, padslice,(AliHLTUInt8_t) tpatch,(AliHLTUInt8_t) tpatch );
	    
	    // READ RAW DATA BLOCK - READ CLUSTERS BLOCK
	    blk = reader->FindBlockNdx( padrowID, " CPT", padrowSpec );	

	    if ( ~(unsigned long)0 != blk ){
		printf( "Raw Data found for slice %u/patch %u\n", padslice, tpatch );
		unsigned long rawDataBlock = (long unsigned) reader->GetBlockData( blk );
		unsigned long rawDataLen = reader->GetBlockDataLength( blk );

		display->FillPadRow( tpatch, rawDataBlock, rawDataLen);
	    }
	}
    
	padrowHistWidget->cd();
	display->DrawHistPadRow();
	padrowHistWidget->GetCanvas()->Update();
	
	pad1HistWidget->cd();
	display->DrawHistPad1();
	pad1HistWidget->GetCanvas()->Update();
	
	pad2HistWidget->cd();
	display->DrawHistPad2();
	pad2HistWidget->GetCanvas()->Update();
	
	pad3HistWidget->cd();
	display->DrawHistPad3();
	pad3HistWidget->GetCanvas()->Update();
    }


    //--------------------------------------------------------------------------------------------
    // RESET HISTOGRAMS
    //--------------------------------------------------------------------------------------------
    if (clusterCheckBox->isEnabled() ) display->ResetHistCharge();
    if (tracksCheckBox->isEnabled() ) display->ResetHistResiduals();

    //--------------------------------------------------------------------------------------------
    // DRAW 3D
    //--------------------------------------------------------------------------------------------
    if (threeDSwitch || (PadRowSwitch && padrowCheckBox->isEnabled() ) ) {

	geomWidget->cd();
	geomWidget->GetCanvas()->Clear();
	geomWidget->GetCanvas()->SetFillColor(1);

	if (!keepViewCheckBox->isChecked() ){ 
	    geomWidget->GetCanvas()->SetTheta(fTheta);
	    geomWidget->GetCanvas()->SetPhi(fPhi);
	}

	geomWidget->GetCanvas()->SetFillColor(display->GetBackColor());
	
	display->Draw3D();
	
	geomWidget->GetCanvas()->Modified();
	geomWidget->GetCanvas()->Update();

	// Display Track Parameters
	if ( selectTrackCheckBox->isChecked() ){


	}

    }



    // Display trackparameter, if single track is selected
    if (selectTrackCheckBox->isChecked()){ 
	Char_t nHits[256];
	Char_t charge[256];
	Char_t kappa[256];
	Char_t radius[256];
//    Double_t xyzF[3];
//    Double_t xyzL[3];
	Char_t slice[256];
	Char_t phi0[256];
	Char_t psi[256];
	Char_t lambda[256];
	Char_t pt[256];
	Char_t id[256];
	Char_t bfield[256] ;

	sprintf(kappa,"%f",display->fTrackParam.kappa);
	sprintf(nHits,"%d",display->fTrackParam.nHits);
	sprintf(charge,"%d",display->fTrackParam.charge);
	sprintf(radius,"%f",display->fTrackParam.radius);
	sprintf(slice,"%d",display->fTrackParam.slice);
	sprintf(phi0,"%f",display->fTrackParam.phi0);
	sprintf(psi,"%f",display->fTrackParam.psi);
	sprintf(lambda,"%f",display->fTrackParam.lambda);
	sprintf(pt,"%f",display->fTrackParam.pt);
	sprintf(id,"%d",display->fTrackParam.id);
	sprintf(bfield,"%f",display->fTrackParam.bfield);

	trackParamSliceTextLabel->setText(slice);
	trackParamIDTextLabel->setText(id);
	trackParamKappaTextLabel->setText(kappa);
	trackParamPtTextLabel->setText(pt);
	trackParamNHitsTextLabel->setText(nHits);
	trackParamChargeTextLabel->setText(charge);
	trackParamRadiusTextLabel->setText(radius);
	trackParamPhi0TextLabel->setText(phi0);
	trackParamPsiTextLabel->setText(psi);
	trackParamLambdaTextLabel->setText(lambda);
	trackParamBfieldTextLabel->setText(bfield);
    }

    //--------------------------------------------------------------------------------------------
    // DRAW RESIDUALS
    //--------------------------------------------------------------------------------------------
    if (tracksCheckBox->isEnabled() ) {
	residualsHistWidget->cd();  
     	residualsHistWidget->GetCanvas()->Clear();
	display->DrawHistResiduals();
	residualsHistWidget->GetCanvas()->Update();
    }

    //--------------------------------------------------------------------------------------------
    // DRAW CHARGE
    //--------------------------------------------------------------------------------------------
    if (clusterCheckBox->isEnabled() ){
	chargeHistWidget->cd();       
	display->DrawHistCharge();
	chargeHistWidget->GetCanvas()->Update();
    }

}

// ####################################################################################################
//       SETTER - minor functions
// ####################################################################################################
void AliHLTGUI::setSector(){    
    // -- set sectors to display
    if ( !fODHDisplay ) {
    	printf( "ERROR no DISPLAY" );
	return;
    }

    AliHLTTPCDisplay* display = (AliHLTTPCDisplay*)fODHDisplay;
    
    Int_t selectSectorID = sectorButtonGroup->selectedId();

    // Set ALL Sectors
    if (selectSectorID == 0) {
	display->SetSlices();
	fTheta = 90.;
    }
    // Set ONE Sector
    else if (selectSectorID == 1) {
	display->SetSlices( atoi( oneSpinBox->text() ) );
	fTheta = 00.;
    }
    // Set RANGE of Sectors
    else if (selectSectorID == 2) {
	display->SetSlices( atoi( rangeMinSpinBox->text() ) ,atoi( rangeMaxSpinBox->text() ) );
	fTheta = 90.;
    }
    // Set PAIR of Sectors
    else if (selectSectorID == 3) { 
	display->SetSlicesPair(atoi( pairSpinBox->text() ) );	
	fTheta = 90.;
    }
    // Set PAIR RANGE of Sectors
    else if (selectSectorID == 4) {
	display->SetSlicesPair(atoi( pairRangeMinSpinBox->text() ) ,atoi( pairRangeMaxSpinBox->text() ) );
	fTheta = 90.;
    }
    // redisplay 3D
    displayEvent(kFALSE,kTRUE,kFALSE);
}

// ####################################################################################################
void AliHLTGUI::setCluster(){    
    // -- set used/unuse/all cluster
    if ( !fODHDisplay ) {
    	printf( "ERROR no DISPLAY" );
	return;
    }

    AliHLTTPCDisplay* display = (AliHLTTPCDisplay*)fODHDisplay;
    
    //  ALL Cluster = 0  | USED Cluster = 1 | UNUSED Cluster = 2
    display->SetSelectCluster( clusterPropButtonGroup->selectedId()  );
          
    // redisplay 3D
    displayEvent(kFALSE,kTRUE,kFALSE);
}

// ####################################################################################################
void AliHLTGUI::setPadRow(){    
    // -- set padrow
    if ( !fODHDisplay ) {
    	printf( "ERROR no DISPLAY" );
	return;
    }

    AliHLTTPCDisplay* display = (AliHLTTPCDisplay*)fODHDisplay;
    display->SetPadRow(atoi (padrowSpinBox->text() ) );
    display->SetSlicePadRow( atoi (padrowSectorSpinBox->text() ) );
    display->SetHistPadRowAxis();

    setPad();
    displayEvent(kFALSE,kTRUE,kFALSE);
}

// ####################################################################################################
void AliHLTGUI::setPad(){    
    // -- set pad, dor pad histograms
    if ( !fODHDisplay ) {
    	printf( "ERROR no DISPLAY" );
	return;
    }

    AliHLTTPCDisplay* display = (AliHLTTPCDisplay*)fODHDisplay;
 
    display->SetPad(atoi (padSpinBox->text() ) );
    displayEvent(kFALSE,kFALSE,kTRUE);
}

// ####################################################################################################
void AliHLTGUI::setTrack(){    
    // -- set track properties
    if ( !fODHDisplay ) {
    	printf( "ERROR no DISPLAY" );
	return;
    }

    AliHLTTPCDisplay* display = (AliHLTTPCDisplay*)fODHDisplay;
    display->SetMinHits(atoi (minHitsSpinBox->text() ) );
    display->SetPtThreshold( (Float_t) atof (ptThresholdLineEdit->text() ) );
}

// ####################################################################################################
void AliHLTGUI::set3D(){    
    // -- set 3D Display
    if ( !fODHDisplay ) {
	printf( "ERROR no DISPLAY" );	
	return;
    }
    AliHLTTPCDisplay* display = (AliHLTTPCDisplay*)fODHDisplay;
    
    if  (padrowCheckBox->isChecked()) setPadRow();
    
    display->SetSwitches(tracksCheckBox->isChecked(), clusterCheckBox->isChecked(), padrowCheckBox->isChecked(), geometryCheckBox->isChecked() );

    displayEvent(kFALSE,kTRUE,kFALSE);
}

// ####################################################################################################
void AliHLTGUI::setSelectTrack(){    
    // --  turn on /off of single track
    if ( !fODHDisplay ) {	
	printf( "ERROR no DISPLAY" );	
	return;
    }
     
    AliHLTTPCDisplay* display = (AliHLTTPCDisplay*)fODHDisplay;

    display->SetSelectTrackSwitch( selectTrackCheckBox->isChecked() );
  
    if (! selectTrackCheckBox->isChecked() ) {
	display->SetSelectTrack(0);
	displayEvent(kFALSE,kTRUE,kFALSE);
    }
    else selectTrackSector();
}

// ####################################################################################################
void AliHLTGUI::selectTrackSector(){      
    // -- select slice for single track
    if ( !fODHDisplay ) {	
	printf( "ERROR no DISPLAY" );	
	return;
    }

    AliHLTTPCDisplay* display = (AliHLTTPCDisplay*)fODHDisplay;

    Int_t slice = atoi( selectTrackSectorSpinBox->text());

    if (fTracksPerSlice[slice] == 0)  selectTrackSpinBox->setMaxValue( 0 );
    else selectTrackSpinBox->setMaxValue( fTracksPerSlice[slice]  - 1 );

    selectTrackSpinBox->setValue(0);

    display->SetSelectTrackSlice(slice); 
    
    selectTrack();
}

// ####################################################################################################
void AliHLTGUI::selectTrack(){    
    // -- select single track in slice
    if ( !fODHDisplay ) {	
	printf( "ERROR no DISPLAY" );	
	return;
    }

    AliHLTTPCDisplay* display = (AliHLTTPCDisplay*)fODHDisplay;

    display->SetSelectTrack( atoi( selectTrackSpinBox->text() ) );

    displayEvent(kFALSE,kTRUE,kFALSE);
}

// ####################################################################################################
void AliHLTGUI::setInvert(){    
    // -- set invert 3D display
    if ( !fODHDisplay ) {	
	printf( "ERROR no DISPLAY" );	
	return;
    }
    
    AliHLTTPCDisplay* display = (AliHLTTPCDisplay*)fODHDisplay;
    display->SetInvert();
    displayEvent(kFALSE,kTRUE,kFALSE);
}

// ####################################################################################################
void AliHLTGUI::setKeepView(){    
    // -- keep angle view 
   if ( !fODHDisplay ) {	
	printf( "ERROR no DISPLAY" );	
	return;
    }

   AliHLTTPCDisplay* display = (AliHLTTPCDisplay*)fODHDisplay;

   if (keepViewCheckBox->isChecked() ) display->SetKeepView(kTRUE);
   else display->SetKeepView(kFALSE);
}

// ####################################################################################################
void AliHLTGUI::addHost(){    
    // -- add host to hostlist
    // push in vector
    fHostnames.push_back( hostnameLineEdit->text() );
    fPorts.push_back( portSpinBox->text() );
    
    // write in List
    QListViewItem * item1 = new QListViewItem(hostListView,  hostnameLineEdit->text(), portSpinBox->text() );
    hostListView->insertItem(item1);
}

// ####################################################################################################
void AliHLTGUI::removeHost(){    
    // -- remove host from hostlist
    if (hostListView->selectedItem()) {
	for ( unsigned i = 0; i < fHostnames.size(); i++ ) {
	    if ( fHostnames[i] == hostListView->selectedItem()->text(0)  && fPorts[i] == hostListView->selectedItem()->text(1)){
		fHostnames.erase( fHostnames.begin()+i );
		fPorts.erase( fPorts.begin()+i );
	    }
	}	
	hostListView->removeItem(hostListView->selectedItem());
    }
}
