/*
   H-RORC Flash Utility, Version 2 

   Florian Painke (fp) <painke@kip.uni-heidelberg.de>a
   Torsten Alt (ta) <talt@cern.ch>
   based on flash_cmd.c by Joerg Peschek
   CRC32 code by Gary S. Brown

   history:
   2010-05-07 changed version info to Version 2 (ta)
   2007-02-20 initial implementation (fp)
   2007-07-25 increase max retries (fp)
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <asm/errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <termios.h>
#include <linux/pci.h>

#include <psi.h>
#include <psi_error.h>



/* version string */
#define VERSION "0.9.11"

/* error codes */
#ifndef SUCCESS
#define SUCCESS 0
#endif
#define EMISSING  1000 /* Missing option argument */
#define EMULTCMDS 1001 /* Multiple commands */
#define EMULTTGTS 1002 /* Multiple targets */
#define EINVALCRD 1003 /* Invalid card index */
#define EINVALTGT 1004 /* Invalid target partition index */
#define EINVALSER 1005 /* Invalid serial number */
#define ENOCMD    1006 /* No command */
#define ENOTGT    1007 /* No target partition */
#define ECRC      1008 /* CRC check failed */

/* H-RORC identification */
#define VENDOR_ID           0x10EE
#define DEVICE_ID           0xDEAD
#define BASE_ADDRESS_REGION 0
#define MAX_INDEX           32
#define MAX_DEVICE_PATH     256

/* registers */
#define CPLD_CONTROL_REG     0x40
#define FLASH_READ_DATA_REG  0x48
#define FLASH_WRITE_DATA_REG 0x4C
#define FLASH_ADDRESS_REG    0x50
#define FLASH_COMMAND_REG    0x54
#define FLASH_ACCESS_REG     0x58

/* commands */
#define CPLD_DISABLE_WD_CMD 0x9
#define CPLD_RESTART_CMD    0x5
#define CPLD_COMMAND_MASK   0xF
#define FLASH_WRITE_CMD     0x2
#define FLASH_READ_CMD      0x4

/* status bits */
#define CPLD_COMMAND_ACK    0x10
#define CPLD_COMMAND_NAK    0x20
#define CPLD_FACTORY_ACTIVE 0x40
#define CPLD_WD_ENABLED     0x80

/* flash sectors */
#define FLASH_SECTOR_MASK        0x00FFFF
#define FLASH_SECTOR_SIZE        0x010000
#define FLASH_SMALL_SECTOR_SIZE  0x002000
#define FLASH_BOTTOM_SECTOR_ADDR 0x000000
#define FLASH_TOP_SECTOR_ADDR    0x7F0000

/* flash partitions */
#define FLASH_PARTITIONS          4
#define FLASH_PARTITION_SECTORS   25
#define FLASH_PARTITION_SIZE      (FLASH_PARTITION_SECTORS * FLASH_SECTOR_SIZE)
#define ACTIVE_PARTITION_TAG_MASK 0xF0
#define ACTIVE_PARTITION_TAG      0xA0

/* partition info block */
#define PARTINFO_SECTOR_OFF    (24 * FLASH_SECTOR_SIZE)
#define PARTINFO_TAG           0xA55A
#define PARTINFO_TAG_OFF       0
#define PARTINFO_TAG_LEN       2
#define PARTINFO_TIMESTAMP_OFF 2
#define PARTINFO_TIMESTAMP_LEN 24
#define PARTINFO_CRC_OFF       26
#define PARTINFO_CRC_LEN       4
#define INVALID_CRC            0xFFFFFFFF
#define PARTINFO_MESSAGE_OFF   32
#define PARTINFO_MESSAGE_LEN   224

/* flash info block */
#define CARD_SERIAL_SECTOR      (126 * FLASH_SECTOR_SIZE)
#define CARD_SERIAL_OFF         0
#define CARD_SERIAL_LEN         8
#define INVALID_CARD_SERIAL     0xFFFFFFFF
#define ACTIVE_PARTITION_SECTOR (127 * FLASH_SECTOR_SIZE)
#define ACTIVE_PARTITION_OFF    0
#define ACTIVE_PARTITION_LEN    1

/* design files */
#define FILE_CREATE_MODE       0664
#define DESIGN_FILE_SIZE       1532464
#define DESIGN_FILE_BLOCK_SIZE 5041
#define DESIGN_FILE_BLOCKS     (DESIGN_FILE_SIZE / DESIGN_FILE_BLOCK_SIZE)

/* timing */
#define USLEEP_TIME               10
#define ERASE_CHIP_TIMEOUT        9000
#define ERASE_SECTOR_TIMEOUT      150000
#define VERIFY_WRITE_DATA_RETRIES 5
#define COMMAND_ACK_RETRIES       5



/* 
*** globals *******************************************************************
 */

enum verbosity {
  VERBOSITY_QUIET,
  VERBOSITY_LOW,
  VERBOSITY_HIGH
};

static enum verbosity g_verbosity = VERBOSITY_LOW;

#define QUIET (g_verbosity == VERBOSITY_QUIET)
#define VERBOSE (g_verbosity >= VERBOSITY_HIGH)

enum mode {
  TEXT_MODE,
  SYSTEM_MODE,
  GRAPHIC_MODE
};

static enum mode g_mode = GRAPHIC_MODE;

#define TEXT (g_mode == TEXT_MODE)
#define SYSTEM (g_mode == SYSTEM_MODE)
#define GRAPHIC (g_mode == GRAPHIC_MODE)

static FILE * g_errout = NULL;

int FW_Version;


/*
*** utility functions *********************************************************
*/



/* busy status display */
void busy( unsigned long current, unsigned long total )
{
  static const char flip[] = "|/-\\";
  static int last_percent = 100, i = 0;
  int percent = (current * 100 + 50)/total;

  if ( percent != last_percent ) {
    printf( "\r[%c] Please wait... (%d%% done)", flip[i], percent );
    fflush( stdout );

    i = (i + 1) % 4;
    last_percent = percent;
  }
}



/* clear busy status display */
void busy_clear( void )
{
  printf( "\r                             \r" );
  fflush( stdout );
}



/* set terminal to non-canonical mode */
void set_term_nc( struct termios * term_settings )
{
  struct termios settings;

  /* get current settings */
  tcgetattr( 0, term_settings );
  settings = (* term_settings);
     
  /* disable canonical mode, and set buffer size to 1 byte */
  settings.c_lflag &= (~ICANON);
  settings.c_cc[VTIME] = 0;
  settings.c_cc[VMIN] = 1;   
  tcsetattr( 0, TCSANOW, & settings );
}

/* reset terminal */     
void reset_term( struct termios * term_settings )
{
  tcsetattr( 0, TCSANOW, term_settings);
}



/* ask yes/no */
int ask_yn( const char * message )
{
  struct termios ts;
  int c;

  /* set terminal to non-canonical mode */
  set_term_nc( & ts );

  /* print message */
  if ( message != NULL )
    printf( "%s\n", message );
  printf( "Do you want to proceed? (y/n)  " );
  // fflush( stdout );

  /* accept only yYnN */
  do {
    printf( "\b \b" );
    c = getchar();
  } while ( c != 'y' && c != 'Y' && c != 'n' && c != 'N' );

  /* reset terminal mode */
  reset_term( & ts );
  printf( "\n" );

  if ( c != 'y' && c != 'Y' ) return EPERM;

  return SUCCESS;
}



/* safe calculate string length */
/* !!!!!!! conflicts with string.h !!!!!!!!!
size_t strnlen( size_t max, const char * buf )
{
  size_t len = 0;

  while ( max-- && buf[len] != '\0' ) ++len;

  return len;
}
*/


/* reverse bit order of a block in memory */
static void bit_reverse( void * buf, size_t size )
{
  static const u_int8_t _rev[] = {
    0x00, 0x80, 0x40, 0xC0, 0x20, 0xA0, 0x60, 0xE0, 
    0x10, 0x90, 0x50, 0xD0, 0x30, 0xB0, 0x70, 0xF0, 
    0x08, 0x88, 0x48, 0xC8, 0x28, 0xA8, 0x68, 0xE8, 
    0x18, 0x98, 0x58, 0xD8, 0x38, 0xB8, 0x78, 0xF8, 
    0x04, 0x84, 0x44, 0xC4, 0x24, 0xA4, 0x64, 0xE4, 
    0x14, 0x94, 0x54, 0xD4, 0x34, 0xB4, 0x74, 0xF4, 
    0x0C, 0x8C, 0x4C, 0xCC, 0x2C, 0xAC, 0x6C, 0xEC, 
    0x1C, 0x9C, 0x5C, 0xDC, 0x3C, 0xBC, 0x7C, 0xFC, 
    0x02, 0x82, 0x42, 0xC2, 0x22, 0xA2, 0x62, 0xE2, 
    0x12, 0x92, 0x52, 0xD2, 0x32, 0xB2, 0x72, 0xF2, 
    0x0A, 0x8A, 0x4A, 0xCA, 0x2A, 0xAA, 0x6A, 0xEA, 
    0x1A, 0x9A, 0x5A, 0xDA, 0x3A, 0xBA, 0x7A, 0xFA,
    0x06, 0x86, 0x46, 0xC6, 0x26, 0xA6, 0x66, 0xE6, 
    0x16, 0x96, 0x56, 0xD6, 0x36, 0xB6, 0x76, 0xF6, 
    0x0E, 0x8E, 0x4E, 0xCE, 0x2E, 0xAE, 0x6E, 0xEE, 
    0x1E, 0x9E, 0x5E, 0xDE, 0x3E, 0xBE, 0x7E, 0xFE,
    0x01, 0x81, 0x41, 0xC1, 0x21, 0xA1, 0x61, 0xE1, 
    0x11, 0x91, 0x51, 0xD1, 0x31, 0xB1, 0x71, 0xF1,
    0x09, 0x89, 0x49, 0xC9, 0x29, 0xA9, 0x69, 0xE9, 
    0x19, 0x99, 0x59, 0xD9, 0x39, 0xB9, 0x79, 0xF9, 
    0x05, 0x85, 0x45, 0xC5, 0x25, 0xA5, 0x65, 0xE5, 
    0x15, 0x95, 0x55, 0xD5, 0x35, 0xB5, 0x75, 0xF5,
    0x0D, 0x8D, 0x4D, 0xCD, 0x2D, 0xAD, 0x6D, 0xED, 
    0x1D, 0x9D, 0x5D, 0xDD, 0x3D, 0xBD, 0x7D, 0xFD,
    0x03, 0x83, 0x43, 0xC3, 0x23, 0xA3, 0x63, 0xE3, 
    0x13, 0x93, 0x53, 0xD3, 0x33, 0xB3, 0x73, 0xF3, 
    0x0B, 0x8B, 0x4B, 0xCB, 0x2B, 0xAB, 0x6B, 0xEB, 
    0x1B, 0x9B, 0x5B, 0xDB, 0x3B, 0xBB, 0x7B, 0xFB,
    0x07, 0x87, 0x47, 0xC7, 0x27, 0xA7, 0x67, 0xE7, 
    0x17, 0x97, 0x57, 0xD7, 0x37, 0xB7, 0x77, 0xF7, 
    0x0F, 0x8F, 0x4F, 0xCF, 0x2F, 0xAF, 0x6F, 0xEF, 
    0x1F, 0x9F, 0x5F, 0xDF, 0x3F, 0xBF, 0x7F, 0xFF
  };
  u_int8_t * _buf = buf;

  while ( size-- ) {
    (* _buf) = _rev[(* _buf)];
    _buf++;
  }
}



/* calculate crc32 */
u_int32_t crc32( u_int32_t initial, size_t size, u_int8_t * buffer )
{
  /* crc polynomial 0xedb88320 */
  static const u_int32_t _table[] = {
    0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f,
    0xe963a535, 0x9e6495a3, 0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988,
    0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91, 0x1db71064, 0x6ab020f2,
    0xf3b97148, 0x84be41de, 0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
    0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec, 0x14015c4f, 0x63066cd9,
    0xfa0f3d63, 0x8d080df5, 0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
    0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b, 0x35b5a8fa, 0x42b2986c,
    0xdbbbc9d6, 0xacbcf940, 0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
    0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423,
    0xcfba9599, 0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
    0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d, 0x76dc4190, 0x01db7106,
    0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
    0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x086d3d2d,
    0x91646c97, 0xe6635c01, 0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e,
    0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,
    0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
    0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2, 0x4adfa541, 0x3dd895d7,
    0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
    0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa,
    0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
    0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81,
    0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,
    0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683, 0xe3630b12, 0x94643b84,
    0x0d6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
    0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb,
    0x196c3671, 0x6e6b06e7, 0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc,
    0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5, 0xd6d6a3e8, 0xa1d1937e,
    0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
    0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55,
    0x316e8eef, 0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
    0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe, 0xb2bd0b28,
    0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
    0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 0x9c0906a9, 0xeb0e363f,
    0x72076785, 0x05005713, 0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38,
    0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21, 0x86d3d2d4, 0xf1d4e242,
    0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
    0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69,
    0x616bffd3, 0x166ccf45, 0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2,
    0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc,
    0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
    0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605, 0xcdd70693,
    0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
    0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
  };

  u_int32_t crc = (~initial);

  while ( size-- ) {
    crc = _table[(crc ^ (* buffer)) & 0xFF] ^ (crc >> 8);
    ++buffer;
  }

  return (~crc);
}



/*
*** cpld functions ************************************************************
 */



/* send cpld command */
int cpld_command( tRegion region, u_int8_t cmd )
{
  int result;
  u_int8_t status;

  int retries = COMMAND_ACK_RETRIES;

  /* send command */
  cmd &= CPLD_COMMAND_MASK;
  result = PSI_write( region, CPLD_CONTROL_REG, _byte_, 1, & cmd );
  if ( result != PSI_OK ) return result;

  /* wait for acknowledge */
  while ( retries-- ) {
    result = PSI_read( region, CPLD_CONTROL_REG, _byte_, 1, & status );
    if ( result != PSI_OK ) return result;

    if ( (status & CPLD_COMMAND_ACK) == CPLD_COMMAND_ACK ) return PSI_OK;
    if ( (status & CPLD_COMMAND_NAK) == CPLD_COMMAND_NAK ) 
      return PSI_SYS_ERRNO + EIO;
  }

  return PSI_SYS_ERRNO + ETIME;
}



/* get cpld status */
int cpld_status( tRegion region, u_int8_t * status )
{
  int result;

  /* read status */
  result = PSI_read( region, CPLD_CONTROL_REG, _byte_, 1, status );
  (* status) &= (~CPLD_COMMAND_MASK);

  return result;
}



/* read cpld info */
int read_cpldinfo( tRegion region )
{
  u_int8_t status;
  int result;

  if ( ! QUIET ) printf( "* Reading CPLD info.\n" );

  /* get cpld status */
  result = cpld_status( region, & status );
  if ( result != PSI_OK ) {
    fprintf( g_errout, "Failed to read CPLD status: %s.\n", 
	     PSI_strerror( result ) );
    return EIO;
  }

  if ( ! QUIET ) {
    /* print active design */
    printf( "- FPGA is loaded with " );
    if ( (status & CPLD_FACTORY_ACTIVE) == CPLD_FACTORY_ACTIVE )
      printf( "factory partition.\n" );
    else
      printf( "user partition.\n" );

    /* print watchdog status */
    printf( "- Watchdog is " );
    if ( (status & CPLD_WD_ENABLED) == CPLD_WD_ENABLED )
      printf( "enabled.\n" );
    else 
      printf( "disabled.\n" );
  }

  return SUCCESS;
}



/* disable watchdog */
int disable_watchdog( tRegion region )
{
  int result;

  if ( ! QUIET ) printf( "* Disabling watchdog.\n" );

  /* send command sequence */
  result = cpld_command( region, CPLD_DISABLE_WD_CMD );
  if ( result == PSI_OK )
    result = cpld_command( region, ~CPLD_DISABLE_WD_CMD );
  if ( result != PSI_OK ) {
    fprintf( g_errout, "Failed to send command to CPLD: %s.\n", 
	     PSI_strerror( result ) );
    return EIO;
  }

  return SUCCESS;
}



/* restart configuration */
int restart_config( tRegion region )
{
  int result;

  if ( ! QUIET ) printf( "* Restarting configuration.\n" );

  /* send command sequence */
  result = cpld_command( region, CPLD_RESTART_CMD );
  if ( result == PSI_OK )
    result = cpld_command( region, ~CPLD_RESTART_CMD );
  if ( result != PSI_OK ) {
    fprintf( g_errout, "Failed to send command to CPLD: %s.\n", 
	     PSI_strerror( result ) );
    return EIO;
  }

  return SUCCESS;
}



/*
*** low level flash functions *************************************************
*/


/* check the version of the hardware configuration controller */
int check_FW( tRegion region, int *FW_Version)
{
  int result;
  u_int32_t check_word_write = 0xDEAD;
  u_int32_t check_word_read = 0x0;

  *FW_Version = 2;

  /* write the check_word to the FLASH_ADDRESS_REG */
  result = PSI_write( region, FLASH_ADDRESS_REG, _dword_, 4, &check_word_write );
  if ( result != PSI_OK ) return result;

  /* read back the check word to verify the existenz of the register */
  result = PSI_read( region, FLASH_ADDRESS_REG, _dword_, 4, &check_word_read );
  if ( result != PSI_OK ) return result;

  if ( check_word_read == check_word_write )
    *FW_Version = 1;
  
  return result;
}

/* write single byte to flash */
int write_byte( tRegion region, u_int32_t address, u_int8_t data )
{
  static u_int32_t command = FLASH_WRITE_CMD;
  int result;
  u_int32_t write_data;
  
  if ( FW_Version == 2 )
    {
      write_data = 0x80000000 + (((u_int32_t)(data)) << 23) + address;
      result = PSI_write( region, FLASH_ACCESS_REG, _dword_, 4, & write_data);
    } else
    {
      /* set data */
      result = PSI_write( region, FLASH_WRITE_DATA_REG, _dword_, 4, & data );
      if ( result != PSI_OK ) return result;
      /* set address */
      result = PSI_write( region, FLASH_ADDRESS_REG, _dword_, 4, & address );
      if ( result != PSI_OK ) return result;
      /* set command */;
      result = PSI_write( region, FLASH_COMMAND_REG, _dword_, 4, & command );

    };
  return result;
}



/* read single byte from flash */
int read_byte( tRegion region, u_int32_t address, u_int8_t * data )
{
  static u_int32_t command = FLASH_READ_CMD;
  u_int32_t readback;
  int result;

  if ( FW_Version == 2 )
    {
      /* low level access for FW Version 2 */
      /* set address in the FLASH_ACCESS_REG */
      result = PSI_write( region, FLASH_ACCESS_REG, _dword_, 4, & address );
      if ( result != PSI_OK ) return result;
      /* read the data from the FLASH_ACCESS_REG */
      result = PSI_read( region, FLASH_ACCESS_REG, _dword_, 4, & readback );
      *data = (0x7F800000 & readback) >> 23;
      return result;
    } else
    {
      /* low level access for the FW Version 1 */
      /* set address */
      result = PSI_write( region, FLASH_ADDRESS_REG, _dword_, 4, & address );
      if ( result != PSI_OK ) return result;
      /* set command */
      result = PSI_write( region, FLASH_COMMAND_REG, _dword_, 4, & command );
      if ( result != PSI_OK ) return result;
      /* get data */
      result = PSI_read( region, FLASH_READ_DATA_REG, _byte_, 1, data );
      return result;
    };
}



/* flash sequence element */
struct t_sequel {
  u_int32_t address;
  u_int8_t data;
};

/* write sequence to flash */
int write_sequence( tRegion region, const struct t_sequel seq[] )
{
  int result;

  /* write sequence */
  while ( seq->address || seq->data ) {
    result = write_byte( region, seq->address, seq->data );
    if ( result != PSI_OK ) break;

    ++seq;
  }

  return result;
}



/*
*** intermediate level flash functions ****************************************
*/



/* reset sequence */
static const struct t_sequel _reset_seq[] = {
  { 0x000AAA, 0xF0 },
  { 0, 0 }
};

/* reset flash */
int reset_flash( tRegion region )
{
  /* write reset sequence */
  return write_sequence( region, _reset_seq );
}



/* erase chip sequence */
static const struct t_sequel _erase_chip_seq[] = {
  { 0x000AAA, 0xAA },
  { 0x000555, 0x55 },
  { 0x000AAA, 0x80 },
  { 0x000AAA, 0xAA },
  { 0x000555, 0x55 },
  { 0x000AAA, 0x10 },
  { 0, 0 }
};

/* erase flash chip */
int erase_chip( tRegion region )
{
  int result;
  int timeout = ERASE_CHIP_TIMEOUT;
  u_int8_t status;

  /* write erase chip sequence */
  result = write_sequence( region, _erase_chip_seq );
  if ( result != PSI_OK ) return result;

  /* wait for completion */
  while ( timeout ) {
    result = read_byte( region, 0, & status );
    if ( result != PSI_OK ) return result;

    if ( status == 0xFF ) return PSI_OK;
    usleep( USLEEP_TIME );
    --timeout;
  }

  return PSI_SYS_ERRNO + ETIME;
}



/* erase sector sequence */
static const struct t_sequel _erase_sector_seq[] = {
  { 0x000AAA, 0xAA },
  { 0x000555, 0x55 },
  { 0x000AAA, 0x80 },
  { 0x000AAA, 0xAA },
  { 0x000555, 0x55 },
  { 0, 0 }
};

/* erase sector on flash (internal function) */
int _erase_sector( tRegion region, u_int32_t address )
{
  int result;
  int timeout = ERASE_SECTOR_TIMEOUT;
  u_int8_t status;

  /* write erase sector sequence */
  result = write_sequence( region, _erase_sector_seq );
  if ( result != PSI_OK ) return result;

  /* erase sectors */
  result = write_byte( region, address, 0x30 );
  if ( result != PSI_OK ) return result;

  /* wait for completion */
  while ( timeout ) {
    result = read_byte( region, address, & status );
    if ( result != PSI_OK ) return result;

    if ( status == 0xFF ) return PSI_OK;
    // usleep( USLEEP_TIME );
    --timeout;
  }

  return PSI_SYS_ERRNO + ETIME;
}

/* formward declaration for tagging byte */
int write_data( tRegion region, u_int32_t address, 
		size_t size, u_int8_t * buffer );

/* erase sector on flash (interface function) */
int erase_sector( tRegion region, u_int32_t address )
{
  u_int8_t tag = 0;
  int result;

  /* check address */
  if ( address & FLASH_SECTOR_MASK ) return PSI_INVALID_ADDR;

  /* handle bottom and top sectors separately (autodetect B/T flash type) */
  if ( address == FLASH_BOTTOM_SECTOR_ADDR || 
       address == FLASH_TOP_SECTOR_ADDR ) {
    /* assume 64k sector, tag last byte */
    result = write_data( region, address + FLASH_SECTOR_SIZE - 1, 1, & tag );
    if ( result != PSI_OK ) return result;

    /* erase sector */
    result = _erase_sector( region, address );
    if ( result != PSI_OK ) return result;

    /* read back tag */
    result = read_byte( region, address + FLASH_SECTOR_SIZE - 1, & tag );
    if ( result != PSI_OK ) return result;

    /* check tag */
    if ( tag != 0xFF ) {
      /* erase remaining 8k sectors */
      address += FLASH_SMALL_SECTOR_SIZE;
      while ( address & FLASH_SECTOR_MASK ) {
	result = _erase_sector( region, address );
	if ( result != PSI_OK ) return result;

	address += FLASH_SMALL_SECTOR_SIZE;
      }
    }

    return PSI_OK;
  } 
  else return _erase_sector( region, address );
}



/* write data sequence */
static const struct t_sequel _write_data_seq[] = {
  { 0x000AAA, 0xAA },
  { 0x000555, 0x55 },
  { 0x000AAA, 0xA0 },
  { 0, 0 }
};

/* write data to flash */
int write_data( tRegion region, u_int32_t address, 
		size_t size, u_int8_t * buffer )
{
  int result;
  u_int8_t data;

  if ( size < 1 ) return PSI_OK;

  /* write data */
  while ( size-- ) {
    int retries = VERIFY_WRITE_DATA_RETRIES;

    /* write data sequence */
    result = write_sequence( region, _write_data_seq );
    if ( result != PSI_OK ) return result;

    /* write data */
    result = write_byte( region, address, (* buffer) );
    if ( result != PSI_OK ) return result;

    /* wait for completion */
    while ( retries ) {
      result = read_byte( region, address, & data );
      if ( result != PSI_OK ) return result;

      if ( data == (* buffer) ) break;
      if ( ! --retries ) return PSI_SYS_ERRNO + ETIME;
    }

    ++address;
    ++buffer;
  }

  return PSI_OK;
}



/* read data from flash */
int read_data( tRegion region, u_int32_t address,
	       size_t size, u_int8_t * buffer )
{
  int result;

  if ( size < 1 ) return PSI_OK;

  /* read data */
  while ( size-- ) {
    /* read data */
    result = read_byte( region, address, buffer );
    if ( result != PSI_OK ) return result;

    ++address;
    ++buffer;
  }

  return PSI_OK;
}



/*
*** high level flash functions ************************************************
*/



/* read flash info */
int read_flashinfo( tRegion region, u_int32_t * serial, int * active )
{
  static const int _map[] = { 
    -1, 0, 1, -1, 2, -1, -1, -1, 3, -1, -1, -1, -1, -1, -1, -1
  };

  int result;
  u_int32_t csn;
  u_int8_t key;
  int partition;

  if ( ! QUIET ) printf( "* Reading flash info.\n" );

  /* read card serial number */
  result = read_data( region, CARD_SERIAL_SECTOR + CARD_SERIAL_OFF, 
		      4, (u_int8_t *) & csn );
  if ( result != PSI_OK ) {
    fprintf( g_errout, "Failed to read data from flash: %s.\n", 
	     PSI_strerror( result ) );
    return EIO;
  }
  if ( ! QUIET ) {
    if ( csn == INVALID_CARD_SERIAL ) 
      printf( "- Card has no serial number.\n" );
    else 
      printf( "- Card serial number: %08x\n", csn );
  }

  /* read active partition key */
  result = read_data( region, ACTIVE_PARTITION_SECTOR + ACTIVE_PARTITION_OFF,
		      1, & key );
  if ( result != PSI_OK ) {
    fprintf( g_errout, "Failed to read data from flash: %s.\n",
	     PSI_strerror( result ) );
    return EIO;
  }

  /* get active partition */
  partition = _map[key & 0x0F];
  if ( (key & ACTIVE_PARTITION_TAG_MASK) != ACTIVE_PARTITION_TAG ||
       partition < 0 || partition >= FLASH_PARTITIONS ) {
    fprintf( g_errout, "Invalid partition key %x.\n", key );
    return EINVAL;
  }

  if ( ! QUIET ) printf( "- Active partition: %d\n", partition );

  /* return serial number and active partition */
  if ( serial != NULL ) (* serial) = csn;
  if ( active != NULL ) (* active) = partition;

  return SUCCESS;
}



/* write partition info block */
int write_partinfo( tRegion region, int partition, 
		    u_int32_t crc, const char * text )
{
  static const u_int16_t tag = PARTINFO_TAG;

  time_t now;
  struct tm * tm;
  char timestamp[PARTINFO_TIMESTAMP_LEN + 1];
  char message[PARTINFO_MESSAGE_LEN + 1];
  u_int8_t len;

  u_int32_t address;
  int result;

  if ( ! QUIET ) 
    printf( "* Writing info for partition %d.\n", partition );

  /* get timestamp */
  time( & now );
  tm = gmtime( & now );
  snprintf( timestamp, PARTINFO_TIMESTAMP_LEN + 1,
	    "%c%04d-%02d-%02d %02d:%02d:%02d GMT", PARTINFO_TIMESTAMP_LEN - 1,
	    1900 + tm->tm_year, tm->tm_mon, tm->tm_mday,
	    tm->tm_hour, tm->tm_min, tm->tm_sec );

  /* get message */
  if ( text != NULL ) {
    len = strnlen( text, PARTINFO_MESSAGE_LEN - 1 );
    if ( len > 0 )
      snprintf( message, PARTINFO_MESSAGE_LEN + 1, "%c%s", len, text );
  }
  else len = 0;

  /* get address */
  address = FLASH_PARTITION_SIZE * partition + PARTINFO_SECTOR_OFF;

  /* erase sector */
  result = erase_sector( region, address );
  if ( result != PSI_OK ) {
    fprintf( g_errout, "Failed to erase sector: %s.\n", 
             PSI_strerror( result ) );
    return EIO;
  }

  /* write info block */
  result = write_data( region, address + PARTINFO_TAG_OFF, 
		       2, (u_int8_t *) & tag );
  if ( result == PSI_OK )
    result = write_data( region, address + PARTINFO_TIMESTAMP_OFF, 
			 PARTINFO_TIMESTAMP_LEN, (u_int8_t *) timestamp );
  if ( result == PSI_OK && crc != INVALID_CRC )
    result = write_data( region, address + PARTINFO_CRC_OFF,
			 4, (u_int8_t *) & crc );
  if ( result == PSI_OK && len > 0 )
    result = write_data( region, address + PARTINFO_MESSAGE_OFF, 
			 len + 1, (u_int8_t *) message );
  if ( result != PSI_OK ) {
    fprintf( g_errout, "Failed to write data to flash: %s.\n", 
	     PSI_strerror( result ) );
    return EIO;
  }

  return SUCCESS;
}



/* read partition info */
int read_partinfo( tRegion region, int partition, u_int32_t * crc )
{
  u_int32_t address;
  int result;

  u_int16_t tag;
  char timestamp[PARTINFO_TIMESTAMP_LEN + 1];
  char message[PARTINFO_MESSAGE_LEN + 1];
  u_int8_t len;

  if ( ! QUIET ) 
    printf( "* Reading info for partition %d.\n", partition );

  /* get address */
  address = FLASH_PARTITION_SIZE * partition + PARTINFO_SECTOR_OFF;

  /* read info block */
  result = read_data( region, address + PARTINFO_TAG_OFF, 
		      2, (u_int8_t *) & tag );
  if ( result == PSI_OK )
    result = read_data( region, address + PARTINFO_TIMESTAMP_OFF,
			PARTINFO_TIMESTAMP_LEN, (u_int8_t *) timestamp );
  if ( result == PSI_OK && crc != NULL )
    result = read_data( region, address + PARTINFO_CRC_OFF,
			4, (u_int8_t *) crc );
  if ( result == PSI_OK )
    result = read_data( region, address + PARTINFO_MESSAGE_OFF,
			PARTINFO_MESSAGE_LEN, (u_int8_t *) message );
  if ( result != PSI_OK ) {
    fprintf( g_errout, "Failed to read data from flash: %s.\n", 
	     PSI_strerror( result ) );
    return EIO;
  }

  /* get tag */
  if ( tag != PARTINFO_TAG ) {
    fprintf( g_errout, "Invalid partition tag or partition empty.\n" );
    return EINVAL;
  }

  /* get timestamp */
  if ( timestamp[0] != PARTINFO_TIMESTAMP_LEN - 1 ) {
    fprintf( g_errout, "Invalid partition timestamp.\n" );
    return EINVAL;
  }

  if ( ! QUIET ) {
    /* print timestamp */
    timestamp[PARTINFO_TIMESTAMP_LEN] = '\0';
    printf( "- Timestamp: %s\n", timestamp + 1 );

    /* get message */
    len = message[0];
    if ( len > 0 && len < PARTINFO_MESSAGE_LEN ) {
      message[len + 1] = '\0';
      printf( "- Comment: %s\n", message + 1 );
    }
  }

  return SUCCESS;
}



/* activate partition */
int activate_partition( tRegion region, int partition )
{
  u_int8_t key;
  int result;

  if ( ! QUIET ) printf( "* Activating partition %d.\n", partition );

  /* erase sector */
  result = erase_sector( region, ACTIVE_PARTITION_SECTOR );
  if ( result != PSI_OK ) {
    fprintf( g_errout, "Failed to erase sector: %s.\n", 
             PSI_strerror( result ) );
    return EIO;
  }

  /* write active partition key */
  key = ACTIVE_PARTITION_TAG | (1 << partition);
  result = write_data( region, ACTIVE_PARTITION_SECTOR + ACTIVE_PARTITION_OFF,
		       1, & key );
  if ( result != PSI_OK ) {
    fprintf( g_errout, "Failed to write to flash: %s.\n", 
	     PSI_strerror( result ) );
    return EIO;
  }

  return SUCCESS;
}



/* dump partition to file */
int dump_partition( tRegion region, int partition, 
		    u_int32_t * crc, const char * path )
{
  int file = -1;
  u_int8_t * buffer = 0;

  int i, result;
  u_int32_t address;
  ssize_t bytes_written;

  if ( ! QUIET ) 
    printf( "* Dumping partition %d to file %s.\n", partition, path );

  /* open file for writing */
  file = open( path, O_RDWR | O_CREAT | O_TRUNC, FILE_CREATE_MODE );
  if ( file < 0 ) {
    fprintf( g_errout, "Failed to open file for writing: %s.\n", 
	     strerror( errno ) );
    return errno;
  }

  /* allocate buffer */
  buffer = malloc( DESIGN_FILE_BLOCK_SIZE );
  if ( buffer == NULL ) {
    fprintf( g_errout, "Failed to allocate transfer buffer\n" );
    close( file );
    return ENOMEM;
  }

  /* read partition */
  address = FLASH_PARTITION_SIZE * partition;
  if ( crc != NULL ) 
    (* crc) = 0;
  for ( i = 0; i < DESIGN_FILE_BLOCKS; ++i ) {
    if ( GRAPHIC ) busy( i, DESIGN_FILE_BLOCKS );

    /* read buffer */
    result = read_data( region, address, DESIGN_FILE_BLOCK_SIZE, buffer );
    if ( result != PSI_OK ) {
      if ( GRAPHIC ) busy_clear();
      fprintf( g_errout, "Failed to read data from flash: %s.\n",
	       PSI_strerror( result ) );
      result = EIO;
      break;
    }
    if ( crc != NULL ) 
      (* crc) = crc32( (* crc), DESIGN_FILE_BLOCK_SIZE, buffer );

    /* reverse buffer and write to file */
    bit_reverse( buffer, DESIGN_FILE_BLOCK_SIZE );
    bytes_written = write( file, buffer, DESIGN_FILE_BLOCK_SIZE );
    if ( bytes_written != DESIGN_FILE_BLOCK_SIZE ) {
      if ( GRAPHIC ) busy_clear();
      fprintf( g_errout, "Failed to write data to file: %s.\n",
	       strerror( errno ) );
      result = errno;
      break;
    }

    address += DESIGN_FILE_BLOCK_SIZE;
    result = SUCCESS;
  }
  if ( GRAPHIC ) busy_clear();

  free( buffer );
  close( file );

  return result;
}



/* load partition from file */
int load_partition( tRegion region, int partition, 
		    u_int32_t * crc, const char * path )
{
  int file = -1;
  u_int8_t * buffer = 0;

  int result;
  u_int32_t start, address, end;
  ssize_t bytes_read;

  if ( ! QUIET ) 
    printf( "* Loading partition %d from file %s.\n", partition, path );

  /* open file for reading */
  file = open( path, O_RDONLY );
  if ( file < 0 ) {
    fprintf( g_errout, "Failed to open file for reading: %s.\n", 
	     strerror( errno ) );
    return errno;
  }

  /* check file size */
  bytes_read = lseek( file, 0, SEEK_END );
  if ( bytes_read != DESIGN_FILE_SIZE ) {
    fprintf( g_errout, "Wrong file format.\n" );
    close( file );
    return EINVAL;
  }
  lseek( file, 0, SEEK_SET );

  /* allocate buffer */
  buffer = malloc( FLASH_SECTOR_SIZE );
  if ( buffer == NULL ) {
    fprintf( g_errout, "Failed to allocate transfer buffer.\n" );
    close( file );
    return ENOMEM;
  }

  /* write partition */
  start = FLASH_PARTITION_SIZE * partition;
  address = start;
  end = address + DESIGN_FILE_SIZE;
  if ( crc != NULL )
    (* crc) = 0;
  while ( address < end ) {
    if ( GRAPHIC ) busy( address - start, DESIGN_FILE_SIZE );

    /* read sector from file */
    bytes_read = read( file, buffer, FLASH_SECTOR_SIZE );
    if ( bytes_read != FLASH_SECTOR_SIZE && address + bytes_read != end ) {
      if ( GRAPHIC ) busy_clear();
      fprintf( g_errout, "Failed to read data from file: %s.\n", 
	       strerror( errno ) );
      result = errno;
      break;
    }
    bit_reverse( buffer, bytes_read );

    /* erase sector on flash */
    result = erase_sector( region, address );
    if ( result != PSI_OK ) {
      if ( GRAPHIC ) busy_clear();
      fprintf( g_errout, "Failed to erase sector: %s.\n", 
	       PSI_strerror( result ) );
      result = EIO;
      break;
    }

    /* write sector to flash */
    result = write_data( region, address, bytes_read, buffer );
    if ( result != PSI_OK ) {
      if ( GRAPHIC ) busy_clear();
      fprintf( g_errout, "Failed to write data to flash: %s.\n",
	       PSI_strerror( result ) );
      result = EIO;
      break;
    }
    if ( crc != NULL )
      (* crc) = crc32( (* crc), bytes_read, buffer );

    address += bytes_read;
    result = SUCCESS;
  }
  if ( GRAPHIC ) busy_clear();

  free( buffer );
  close( file );

  return result;
}



/* verify partition from file */
int verify_partition( tRegion region, int partition, 
		      u_int32_t * crc, const char * path )
{
  int file = -1;
  u_int8_t * buf_1 = 0, * buf_2 = 0;

  int i, result;
  u_int32_t address;
  ssize_t bytes_read;

  if ( ! QUIET ) 
    printf( "* Comparing partition %d to file %s.\n", partition, path );

  /* open file for reading */
  file = open( path, O_RDONLY );
  if ( file < 0 ) {
    fprintf( g_errout, "Failed to open file for reading: %s.\n", 
	     strerror( errno ) );
    return errno;
  }

  /* check file size */
  bytes_read = lseek( file, 0, SEEK_END );
  if ( bytes_read != DESIGN_FILE_SIZE ) {
    fprintf( g_errout, "Wrong file format.\n" );
    close( file );
    return EINVAL;
  }
  lseek( file, 0, SEEK_SET );

  /* allocate buffers */
  buf_1 = malloc( DESIGN_FILE_BLOCK_SIZE );
  buf_2 = malloc( DESIGN_FILE_BLOCK_SIZE );
  if ( buf_1 == NULL || buf_2 == NULL ) {
    fprintf( g_errout, "Failed to allocate transfer buffers.\n" );
    close( file );
    if ( buf_1 != NULL ) free( buf_1 );
    if ( buf_2 != NULL ) free( buf_2 );
    return ENOMEM;
  }

  /* verify partition */
  address = FLASH_PARTITION_SIZE * partition;
  if ( crc != NULL )
    (* crc) = 0;
  for ( i = 0; i < DESIGN_FILE_BLOCKS; ++i ) {
    if ( GRAPHIC ) busy( i, DESIGN_FILE_BLOCKS );

    /* read block from file */
    bytes_read = read( file, buf_1, DESIGN_FILE_BLOCK_SIZE );
    if ( bytes_read != DESIGN_FILE_BLOCK_SIZE ) {
      if ( GRAPHIC ) busy_clear();
      fprintf( g_errout, "Failed to data from read file: %s.\n", 
	       strerror( errno ) );
      result = errno;
      break;
    }
    bit_reverse( buf_1, DESIGN_FILE_BLOCK_SIZE );

    /* read block from flash */
    result = read_data( region, address, DESIGN_FILE_BLOCK_SIZE, buf_2 );
    if ( result != PSI_OK ) {
      if ( GRAPHIC ) busy_clear();
      fprintf( g_errout, "Failed to read data from flash: %s.\n",
	       PSI_strerror( result ) );
      result = EIO;
      break;
    }
    if ( crc != NULL )
      (* crc) = crc32( (* crc), DESIGN_FILE_BLOCK_SIZE, buf_2 );

    /* compare blocks */
    if ( memcmp( buf_1, buf_2, DESIGN_FILE_BLOCK_SIZE ) ) {
      if ( GRAPHIC ) busy_clear();
      fprintf( g_errout, "Verification failed.\n" );
      result = EIO;
      break;
    }

    address += DESIGN_FILE_BLOCK_SIZE;
    result = SUCCESS;
  }
  if ( GRAPHIC ) busy_clear();

  free( buf_1 );
  free( buf_2 );
  close( file );

  return result;
}



/* check partition integrity */
int check_integrity( tRegion region, int partition, u_int32_t crc )
{
  u_int8_t * buffer = 0;

  int i, result;
  u_int32_t address, crc_calc;

  if ( ! QUIET ) 
    printf( "* Checking integrity of partition %d.\n", partition );

  /* allocate buffer */
  buffer = malloc( DESIGN_FILE_BLOCK_SIZE );
  if ( buffer == NULL ) {
    fprintf( g_errout, "Failed to allocate transfer buffer.\n" );
    return ENOMEM;
  }

  /* verify partition */
  address = FLASH_PARTITION_SIZE * partition;
  crc_calc = 0;
  for ( i = 0; i < DESIGN_FILE_BLOCKS; ++i ) {
    if ( GRAPHIC ) busy( i, DESIGN_FILE_BLOCKS );

    /* read block from flash */
    result = read_data( region, address, DESIGN_FILE_BLOCK_SIZE, buffer );
    if ( result != PSI_OK ) {
      if ( GRAPHIC ) busy_clear();
      fprintf( g_errout, "Failed to read data from flash: %s.\n",
	       PSI_strerror( result ) );
      result = EIO;
      break;
    }
    crc_calc = crc32( crc_calc, DESIGN_FILE_BLOCK_SIZE, buffer );

    address += DESIGN_FILE_BLOCK_SIZE;
    result = SUCCESS;
  }
  if ( GRAPHIC ) busy_clear();

  /* check crc */
  if ( crc != crc_calc ) {
    fprintf( g_errout, "CRC check failed.\n" );
    result = ECRC;
  }

  free( buffer );

  return result;
}



/* erase partition */
int erase_partition( tRegion region, int partition )
{
  int result, i;
  u_int32_t address;

  if ( ! QUIET ) printf( "* Erasing partition %d.\n", partition );

  /* erase sectors */
  address = FLASH_PARTITION_SIZE * partition;
  for ( i = 0; i < FLASH_PARTITION_SECTORS; ++i ) {
    if ( GRAPHIC ) busy( i, FLASH_PARTITION_SECTORS );

    result = erase_sector( region, address );
    if ( result != PSI_OK ) {
      if ( GRAPHIC ) busy_clear();
      fprintf( g_errout, "Failed to erase sector: %s.\n", 
	       PSI_strerror( result ) );
      break;
    }

    address += FLASH_SECTOR_SIZE;
  }
  if ( GRAPHIC ) busy_clear();

  return result;
}



/* set card serial number */
int set_serial( tRegion region, u_int32_t serial )
{
  int result;
  u_int32_t data;

  if ( ! QUIET ) printf( "* Setting card serial number to %08x.\n", serial );

  /* read serial number */
  result = read_data( region, CARD_SERIAL_SECTOR + CARD_SERIAL_OFF,
		      4, (u_int8_t *) & data );
  if ( result != PSI_OK ) {
    fprintf( g_errout, "Failed to read data from flash: %s.\n",
	     PSI_strerror( result ) );
    return EIO;
  }
  if ( data != 0xFFFFFFFF && ! QUIET ) {
    fprintf( g_errout, "Card serial number already set.\n" );
    return EPERM;
  }

  /* erase sector */
  result = erase_sector( region, CARD_SERIAL_SECTOR );
  if ( result != PSI_OK ) {
    fprintf( g_errout, "Failed to erase sector: %s.\n", 
             PSI_strerror( result ) );
    return EIO;
  }

  /* write serial number */
  result = write_data( region, CARD_SERIAL_SECTOR + CARD_SERIAL_OFF,
		       4, (u_int8_t *) & serial );
  if ( result != PSI_OK ) {
    fprintf( g_errout, "Failed to write data to flash: %s.\n", 
	     PSI_strerror( result ) );
    return EIO;
  }

  return SUCCESS;
}



/*
*** user interface functions **************************************************
*/



/* commands */
enum t_command {
  NONE,
  LOAD,
  DUMP,
  VERIFY,
  INTEGRITY,
  ERASE,
  PRINT,
  ACTIVATE,
  SERIAL,
  WATCHDOG,
  RESTART
};



/* params */
struct t_params {
  enum t_command command;
  int partition;
  int index;
  const char * path;
  const char * message;
  u_int32_t serial;
  int integrity;
};

#define INVALID_PARTITION (-1)



/* print usage */
void usage( void )
{
  fprintf( g_errout, "Usage: rflash [options] command.\n" );
  fprintf( g_errout, "\nCommand can be one of:\n" );
  fprintf( g_errout, "\t-a\tactivate partition,\n" );
  fprintf( g_errout, "\t-d file\tdump partition to file\n" );
  fprintf( g_errout, "\t-e\terase partition\n" );
  fprintf( g_errout, "\t-g\tperform integrity check on partition\n" );
  fprintf( g_errout, "\t-l file\tload partition from file\n" );
  fprintf( g_errout, "\t-p\tprint partition info\n" );
  fprintf( g_errout, "\t-r\trestart configuration\n" );
  fprintf( g_errout, "\t-s n\tset card serial number\n" );
  fprintf( g_errout, "\t-v file\tverify partition, compare to file\n" );
  fprintf( g_errout, "\t-w\tdisable watchdog timer\n" );
  fprintf( g_errout, "\nOptions can be any of:\n" );
  fprintf( g_errout, "\t-b\tbrief mode (implies -t; use with caution!)\n" );
  fprintf( g_errout, "\t-c text\tappend text to partition\n" );
  fprintf( g_errout, "\t-f\tuse factory partition (use with caution!)\n" );
  fprintf( g_errout, "\t-G\tsuppress integrity check\n" );
  fprintf( g_errout, "\t-i n\tuse nth card\n" );
  fprintf( g_errout, "\t-o\tuse standard output for errors\n" );
  fprintf( g_errout, "\t-q\tquiet mode (implies -t; use with caution!)\n" );
  fprintf( g_errout, "\t-t\ttext mode (suppress terminal features)\n" );
  fprintf( g_errout, "\t-u n\tuse nth user partition (1..3)\n" );
  fprintf( g_errout, "\t-V\tverbose mode\n" );
}



/* get error message */
const char * get_error( int result )
{
  switch ( result ) {
  case EMISSING:
    return "Missing option argument";

  case EMULTCMDS:
    return "Multiple commands";

  case EMULTTGTS:
    return "Multiple target partitions";

  case EINVALCRD:
    return "Invalid card index";

  case EINVALTGT:
    return "Invalid target partition";

  case EINVALSER:
    return "Invalid serial number";

  case ENOCMD:
    return "No command given";

  case ENOTGT:
    return "No target partition given";

  case ECRC:
    return "CRC check failed";

  default:
    return strerror( result );
  }
}



/* parse command line arguments */
int parse_args( int argc, char * argv[], struct t_params * params )
{
  extern char * optarg;
  extern int optind, opterr, optopt;

  int opt;
  int result = SUCCESS;

  /* initialise params */
  params->command = NONE;
  params->partition = INVALID_PARTITION;
  params->index = 0;
  params->path = 0;
  params->message = 0;
  params->serial = 0xFFFFFFFF;
  params->integrity = 1;

  /* parse command line arguments */
  errno = 0;
  while ( (opt = getopt( argc, argv, ":abd:c:efgGhi:l:opqrs:tu:v:Vw" )) != -1 ) {
    switch ( opt ) {
    case 'a':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = ACTIVATE;
      break;

    case 'b':
      g_verbosity = VERBOSITY_QUIET;
      g_mode = SYSTEM_MODE;
      break;

    case 'c':
      params->message = optarg;
      break;

    case 'd':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = DUMP;
      params->path = optarg;
      break;

    case 'e':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = ERASE;
      break;

    case 'f':
      if ( params->partition != -1 ) result = EMULTCMDS;
      params->partition = 0;
      break;

    case 'g':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = INTEGRITY;
      break;

    case 'G':
      params->integrity = 0;
      break;

    case 'h':
      usage();
      exit( 0 );

    case 'i':
      params->index = strtol( optarg, 0, 0 );
      if ( params->index < 0 || params->index > MAX_INDEX )
	result = EINVALCRD;
      break;

    case 'l':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = LOAD;
      params->path = optarg;
      break;

    case 'o':
      g_errout = stdout;
      break;

    case 'p':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = PRINT;
      break;

    case 'q':
      g_verbosity = VERBOSITY_QUIET;
      g_mode = TEXT_MODE;
      break;

    case 'r':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = RESTART;
      break;

    case 's':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = SERIAL;
      params->serial = strtol( optarg, 0, 0 );
      if ( params->serial == 0xFFFFFFFF ) result = EINVALSER;
      break;
      
    case 't':
      g_mode = TEXT_MODE;
      break;

    case 'u':
      if ( params->partition != -1 ) result = EMULTTGTS;
      params->partition = strtol( optarg, 0, 0 );
      if ( params->partition < 1 || params->partition > 3 )
	result = EINVALTGT;
      break;

    case 'v':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = VERIFY;
      params->path = optarg;
      break;

    case 'V':
      g_verbosity = VERBOSITY_HIGH;
      break;

    case 'w':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = WATCHDOG;
      break;

    case ':':
      result = EMISSING;
      break;

    case '?':
      result = EINVAL;
      break;
    }

    /* check for error */
    if ( result != SUCCESS ) return result;
  }

  /* check if command was given */
  if ( params->command == NONE ) result = ENOCMD;

  /* check if target partition was given */
  else if ( params->partition == INVALID_PARTITION && 
	    params->command != PRINT && 
	    params->command != SERIAL &&
            params->command != WATCHDOG &&
	    params->command != RESTART ) 
    result = ENOTGT;


  return result;
}



/* main */
int main( int argc, char * argv[] )
{
  struct t_params params;

  char regionpath[MAX_DEVICE_PATH];
  int result;
  tRegion region;
  tRegionStatus info;

  int active;
  u_int32_t crc;

  /* initialise globals */
  g_errout = stderr;

  /* parse command line arguments */
  result = parse_args( argc, argv, & params );
  if ( result != SUCCESS ) {
    fprintf( g_errout, "Error while parsing command line options: %s.\n\n",
	     get_error( result ) );
    usage();
    return result;
  }

  /* print greeting */
  if ( ! QUIET || SYSTEM )
    printf( "H-RORC Flash Utility, Version %s\n", VERSION );

  /* open card */
  sprintf( regionpath, "/dev/psi/vendor/0x%04hX/device/0x%04hX/%i/base%d",
	   VENDOR_ID, DEVICE_ID, params.index, BASE_ADDRESS_REGION );
  result = PSI_openRegion( & region, regionpath );
  if ( result != PSI_OK ) {
    fprintf( g_errout, "Failed to open device: %s.\n", PSI_strerror( result ) );
    return EACCES;
  }

  /* print device info */
  result = PSI_getRegionStatus( region, & info );
  if ( result != PSI_OK ) {
    fprintf( g_errout, "Failed to get device info: %s.\n", 
	     PSI_strerror( result ) );
    PSI_closeRegion( region );
    return EIO;
  }
  if ( VERBOSE || SYSTEM )
    printf( "- Using device %d (%02x:%02x.%x)\n", params.index, 
	    info.bus, PCI_SLOT( info.devfn ), PCI_FUNC( info.devfn ) );


  /* check the version of the hardware configuration controller */
  result = check_FW( region, & FW_Version);
  if ( result != PSI_OK) {
    fprintf( g_errout, "Failed to determinated FW version %s.\n",
	     PSI_strerror( result ) );
    return EIO;
  };
  printf("Configuration Controller version %x detected \n", FW_Version);

  /* select mode of operation */
  switch ( params.command ) {
  case LOAD:
    if ( ! QUIET ) {
      /* check before overwriting design */
      result = read_partinfo( region, params.partition, NULL );
      if ( result == SUCCESS )
	result = ask_yn( "Trying to overwrite partition"
			 " with a valid design." );
      else 
	result = SUCCESS;
    }
    else 
      result = SUCCESS;

    /* write file to partition and store timestamp, crc, ... */
    if ( result == SUCCESS )
      result = load_partition( region, params.partition, & crc, params.path );
    if ( result == SUCCESS )
      result = write_partinfo( region, params.partition, crc, params.message );
    break;

  case DUMP:
    if ( ! QUIET )
      read_partinfo( region, params.partition, NULL );

    result = dump_partition( region, params.partition, NULL, params.path );
    break;

  case VERIFY:
    if ( ! QUIET )
      read_partinfo( region, params.partition, NULL );

    result = verify_partition( region, params.partition, NULL, params.path );
    break;

  case INTEGRITY:
    result = read_partinfo( region, params.partition, & crc );
    if ( result == SUCCESS )
      result = check_integrity( region, params.partition, crc );
    break;

  case ERASE:
    if ( ! QUIET ) {
      /* check before erasing design */
      result = read_partinfo( region, params.partition, NULL );
      if ( result == SUCCESS )
	result = ask_yn( "Trying to erase a partition with a valid design." );
      else 
	result = SUCCESS;
    }
    else
      result = SUCCESS;

    if ( result == SUCCESS )
      result = erase_partition( region, params.partition );
    break;

  case ACTIVATE:
    /* no need to activate already active partition */
    result = read_flashinfo( region, NULL, & active );
    if ( result == SUCCESS && params.partition == active ) break;

    if ( ! QUIET ) {
      /* check before activating empty or currupt partition */
      result = read_partinfo( region, params.partition, & crc );
      if ( result == SUCCESS && params.integrity )
	result = check_integrity( region, params.partition, crc );
      if ( result != SUCCESS )
	result = ask_yn( "Trying to activate a partition"
			 " with no valid design." );
    }
    else
      result = SUCCESS;

    if ( result == SUCCESS )
      result = activate_partition( region, params.partition );
    break;

  case PRINT:
    if ( params.partition == INVALID_PARTITION ) {
      result = read_flashinfo( region, NULL, NULL );
      if ( result == PSI_OK )
        result = read_cpldinfo( region );
    }
    else
      result = read_partinfo( region, params.partition, NULL );
    break;

  case WATCHDOG:
    result = disable_watchdog( region );
    break;

  case RESTART:
    if ( ! QUIET ) {
      /* check before restarting with invalid, empty or currupt partition */
      result = read_flashinfo( region, NULL, & active );
      if ( result == SUCCESS )
	result = read_partinfo( region, active, & crc );
      if ( result == SUCCESS && params.integrity )
	result = check_integrity( region, active, crc );
      if ( result != SUCCESS )
	result = ask_yn( "Trying to restart configuration"
			 " with no valid active partition" );
    }
    else
      result = SUCCESS;

    if ( result == SUCCESS )
      result = restart_config( region );
    break;

  case SERIAL:
    result = set_serial( region, params.serial );
    break;

  default:
    fprintf( g_errout, "Command not implemented.\n" );
    result = ENOSYS;
  }

  if ( result == SUCCESS && (! QUIET || SYSTEM) ) printf( "Done.\n" );
  PSI_closeRegion( region );

  return result;
}
