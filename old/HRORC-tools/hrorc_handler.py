#!/usr/bin/env python
# -*- coding: utf-8 -*-

#TODO: 
# -make -f optional, default to machine the script is running on
# -m should be called something different, like command or similar
# -use the same wording as evtreplay for modes
# -also show what kind of run/running/stopped in HRORCInfo.sh

#Modes:
#Start modes -> one, single, loop
    #one: sends only one event once         :   -r o
    #single: sends all loaded events once   :   -r s
    #loop: all events in a continous loop   :   -r c

# -r mode start run
# mode is one of o[neshot], s[ingle], c[ontinuous]

import sys, socket, os
from subprocess import Popen, PIPE
from optparse import OptionParser, OptionGroup
import libxml2

description="""
ps and pc in startmode stands for programmable burst and continous burst.
"""

optParser = OptionParser(description=description)

optParser.add_option("-m", "--mode", dest="mode", help="Mode of operation: Stop, Start, Clear, Info, Load, Burst")
optParser.add_option("-f", "--ddllist", dest="file", help="Path to ddllist file.")
optParser.add_option("-t", "--test", action="store_true", dest="testmode", default=False, help="Shows the command that will be run, instead of running it.")

startOptions = OptionGroup(optParser, "Start options.")
startOptions.add_option("-s", "--startmode", dest="startmode", help="Run mode: one, single, loop, ps and pc")
startOptions.add_option("-W", "--waittime", dest="waittime", help="Wait time between events in clock cycles (@133 Mhz).")
optParser.add_option_group(startOptions)

loadOptions = OptionGroup(optParser, "Load options.")
loadOptions.add_option("-d", "--directory", dest="directory", help="Directory containing ddl files.")
optParser.add_option_group(loadOptions)

burstOptions = OptionGroup(optParser, "Burst options.")
burstOptions.add_option("-j", "--numberofevents", dest="numberofevents", help="Number of events pr burst.")
burstOptions.add_option("-k", "--numberofbursts", dest="numberofbursts", help="Numbr of bursts.")
burstOptions.add_option("-w", "--burstwait", dest="burstwait", help="Waittime between bursts.")
optParser.add_option_group(burstOptions)

(options, args) = optParser.parse_args()

def getRorcId(bus, slot):
    if bus == 3 and (slot == 4 or slot == 6):
        return str(1)
    elif bus == 2 and (slot == 7 or slot == 5):
        return str(0)
    else:
        raise ValueError("No valid values for bus and slot.")

def getRorcInfo(xmlNode):
    ddlId = xmlNode.xpathEval("@ID")[0].content
    bus = int(xmlNode.xpathEval("Bus")[0].content)
    slot = int(xmlNode.xpathEval("Slot")[0].content)
    diuId = xmlNode.xpathEval("DIU")[0].content
    return ddlId, bus, slot, diuId

def main():
    if not (options.mode and options.file):
        print "A mandatory command line opption is missing. As a minimum -m and -f is needed. See -h for more info."
        sys.exit(1)
    else:
        hostname = socket.gethostname()
        
        try:
            ddllist = libxml2.parseFile(options.file)
            ddlNodes = ddllist.xpathEval("/SimpleChainConfig2DDLList/DDL")
        except libxml2.parserError, e:
            print "Could not parse xml file %s: %s." % (options.file, str(e))
            sys.exit(1)
        
        # Stop mode
        if options.mode == "Stop":
            
            processes = []
            
            for node in ddlNodes:
                nodeName = node.xpathEval("Node")[0].content
                if nodeName == hostname:
                    ddlId, bus, slot, diuId = getRorcInfo(node)
                    
                    try:
                        rorcId = getRorcId(bus, slot)
                    except ValueError, e:
                        print str(e)
                    
                    cmd = ["evtreplay"]
                    cmd.append("-i")
                    cmd.append(rorcId)
                    cmd.append("-n")
                    cmd.append(diuId)
                    cmd.append("-s")
                    
                    if options.testmode:
                        print cmd
                    else:
                        processes.append(Popen(cmd, stdout=PIPE))
            
            for proc in processes:
                proc.wait()
            
            print "%s: rorc replay stopped on all ddls" % hostname
        
        # Clear mode
        elif options.mode == "Clear":
            print "\n%s:" % hostname
            for node in ddlNodes:
                nodeName = node.xpathEval("Node")[0].content
                if nodeName == hostname:
                    ddlId, bus, slot, diuId = getRorcInfo(node)
                    
                    try:
                        rorcId = getRorcId(bus, slot)
                    except ValueError, e:
                        print str(e)
                    
                    cmd = ["evtreplay"]
                    cmd.append("-i")
                    cmd.append(rorcId)
                    cmd.append("-n")
                    cmd.append(diuId)
                    
                    import copy
                    
                    cmd1 = copy.copy(cmd)
                    cmd2 = copy.copy(cmd)
                    cmd3 = copy.copy(cmd)
                    
                    cmd1.append("-c")
                    cmd2.append("-a")
                    cmd3.append("-x")
                    
                    if options.testmode:
                        print cmd1
                        print cmd2
                        print cmd3
                    else:
                        p1 = Popen(cmd1, stdout=PIPE)
                        p2 = Popen(cmd2, stdout=PIPE)
                        p3 = Popen(cmd3, stdout=PIPE)
                        if p1.wait() == p2.wait() == p3.wait() == 0:
                            print "  ddl %s cleared" % ddlId
        
        elif options.mode == "Burst":
            
            if options.numberofevents and options.numberofbursts and options.burstwait:
                
                print "\n%s:" % hostname
                for node in ddlNodes:
                    nodeName = node.xpathEval("Node")[0].content
                    if nodeName == hostname:
                        ddlId, bus, slot, diuId = getRorcInfo(node)
                        
                        try:
                            rorcId = getRorcId(bus, slot)
                        except ValueError, e:
                            print str(e)
                        
                        cmd = ["evtreplay"]
                        cmd.append("-i")
                        cmd.append(rorcId)
                        cmd.append("-n")
                        cmd.append(diuId)
                        cmd.append("-j")
                        cmd.append(options.numberofevents)
                        cmd.append("-k")
                        cmd.append(options.numberofbursts)
                        cmd.append("-m")
                        cmd.append(options.burstwait)
                        cmd.append("-b")
                        
                        if options.testmode:
                            print "Test mode. Command is: ", cmd
                        else:
                            proc = Popen(cmd, stdout=PIPE)
                            if proc.wait() == 0:
                                print "  ddl %s burst initialized." % ddlId
            else:
                print "Burst needs all of -j, -k and -w to be set. See help (-h)."
                sys.exit(1)

        
        # Info mode
        elif options.mode == "Info":
            print "\n%s:" % hostname
            for node in ddlNodes:
                nodeName = node.xpathEval("Node")[0].content
                if nodeName == hostname:
                    ddlId, bus, slot, diuId = getRorcInfo(node)
                    
                    try:
                        rorcId = getRorcId(bus, slot)
                    except ValueError, e:
                        print str(e)
                    
                    cmd = ["evtreplay"]
                    cmd.append("-i")
                    cmd.append(rorcId)
                    cmd.append("-n")
                    cmd.append(diuId)
                    cmd.append("-t")
                    
                    if options.testmode:
                        print cmd
                    else:
                        proc = Popen(cmd, stdout=PIPE)
                        
                        events = 0
                        for line in proc.stdout.readlines():
                            if "size" in line: events += 1
                        print "  ddl %s: %s loaded events" % (ddlId, events)
        
        # Start mode
        elif options.mode == "Start":
            modeArg = ""
            if not options.startmode:
                print "'--mode Start' needs the --startmode option."
                sys.exit(1)
            else:
                if options.startmode == "one":
                    modeArg = "o"
                elif options.startmode == "single":
                    modeArg = "s"
                elif options.startmode == "loop":
                    modeArg = "c"
                elif options.startmode == "ps":
                    modeArg = "ps"
                elif options.startmode == "pc":
                    modeArg = "pc"
                else:
                    print "No known options passed to --startmode. Mode was: %s" % options.startmode
                    sys.exit(1)
            
            processes = []
            
            for node in ddlNodes:
                nodeName = node.xpathEval("Node")[0].content
                if nodeName == hostname:
                    ddlId, bus, slot, diuId = getRorcInfo(node)
                    
                    try:
                        rorcId = getRorcId(bus, slot)
                    except ValueError, e:
                        print str(e)
                    
                    cmd = ["evtreplay"]
                    cmd.append("-i")
                    cmd.append(rorcId)
                    cmd.append("-n")
                    cmd.append(diuId)
                    cmd.append("-r")
                    cmd.append(modeArg)
                    if options.waittime:
                      cmd.append("-w")
                      cmd.append(options.waittime)
                    
                    if options.testmode:
                        print cmd
                    else:
                        processes.append(Popen(cmd, stdout=PIPE))
            
            for proc in processes:
                proc.wait()
            
            print "%s: rorc replay started on all ddls in mode: %s" % (hostname, options.startmode)
        
        # Load mode
        elif options.mode == "Load":
            if not options.directory:
                print "Directory option not specified. See -h for more info."
                sys.exit(1)
            
            print "\n%s:" % hostname
            
            directory = options.directory
            
            for node in ddlNodes:
                nodeName = node.xpathEval("Node")[0].content
                if nodeName == hostname:
                    ddlId, bus, slot, diuId = getRorcInfo(node)
                    
                    try:
                        rorcId = getRorcId(bus, slot)
                    except ValueError, e:
                        print str(e)
                    
                    fileCount = 0
                    
                    for dataDir in sorted(os.listdir(directory)):
                        dataDir = os.path.join(directory, dataDir)
                        
                        if os.path.isdir(dataDir):
                            
                            for ddlFile in os.listdir(dataDir):
                                
                                if "_" in ddlFile and os.path.splitext(ddlFile)[0].split("_")[1] == ddlId:
                                    
                                    cmd = ["evtreplay"]
                                    cmd.append("-i")
                                    cmd.append(rorcId)
                                    cmd.append("-n")
                                    cmd.append(diuId)
                                    cmd.append("-l")
                                    cmd.append(os.path.join(dataDir, ddlFile))
                                    
                                    if options.testmode:
                                        print cmd
                                    else:
                                        proc = Popen(cmd, stdout=PIPE)
                                        if proc.wait() == 0:
                                            fileCount += 1
                                        else:
                                            print "Failed loading file: %s for ddl %s" % (ddlFile, ddlId)
                                            
                    print "  ddl %s: loaded %d file(s)" % (ddlId, fileCount)
        else:
            print "No valid mode specified: mode = ", options.mode
            sys.exit(1)

if __name__ == "__main__":
    main()
