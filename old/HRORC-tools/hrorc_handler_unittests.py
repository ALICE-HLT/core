#/usr/bin/env python

import unittest
import hrorc_handler

class TestHrorcHandler(unittest.TestCase):
    
    def testGetRorcId(self):
        bus = 3
        slot = 4
        self.assertEqual(0, hrorc_handler.getRorcId(bus, slot))
        slot = 6
        self.assertEqual(0, hrorc_handler.getRorcId(bus, slot))
        bus = 2
        slot = 5
        self.assertEqual(1, hrorc_handler.getRorcId(bus, slot))
        slot = 7
        self.assertEqual(1, hrorc_handler.getRorcId(bus, slot))
    
    def testGetRorcIdException(self):
        bus = 5
        slot = 7
        self.assertRaises(ValueError, hrorc_handler.getRorcId, bus, slot)
        bus = 1
        slot = 3
        self.assertRaises(ValueError, hrorc_handler.getRorcId, bus, slot)

if __name__ == '__main__':
    unittest.main()