/*
   FCF_Control  Utility, Version 1

   Torsten Alt (ta) <talt@kip.uni-heidelberg.de>

   history:
   2009-09-03 initial implementation (ta)
   2010-11-16 Version 0.1.1 added monitoring for the maximum fifo values and replaced old bit values for the fcf_control register
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <asm/errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <termios.h>
#include <linux/pci.h>

#include <psi.h>
#include <psi_error.h>



/* version string */
#define VERSION "0.1.1"

/* error codes */
#ifndef SUCCESS
#define SUCCESS 0
#endif
#define EMISSING    1000 /* Missing option argument */
#define EMULTCMDS   1001 /* Multiple commands */
#define EINVALCRD   1002 /* Invalid card index */
#define EINVALLNK   1003 /* Invalid link index */
#define EINVALGAIN  1004 /* Invalid gain value */
#define EINVALLIMIT 1005 /* Invalid sequence limit */
#define ENOCMD      1006 /* No command */

/* message verbosity */
#define QUIET	0
#define DEFAULT	1
#define VERBOSE	2


/* H-RORC identification */
#define VENDOR_ID           0x10EE
#define DEVICE_ID           0xDEAD
#define BASE_ADDRESS_REGION 0
#define MAX_INDEX           32
#define MAX_DEVICE_PATH     256

/* H-RORC internal register addresses */
/* FastClusterFinder  */
#define FCF_CONTROL_REG    0x100  // local address of the FastClusterFinder control register
#define FCF_STATUS_REG     0x104  // local address of the FastClusterFinder status register
#define CFG_RAM_ADDR_REG   0x108  // address register for the access to the FCF CFG ram
#define CFG_RAM_DIN_REG    0x10C  // data input register to access the FCF CFG ram
#define CFG_RAM_DOUT_REG   0x110  // data output register to access the FCF CFG ram
#define FCF_STS_RCUDEC_REG 0x118  // register address for the rcuDecoder and CF_Decoder fifo 
#define FCF_STS_CFD_REG    0x11C  // register address for the merger fifos A & B
#define FCF_STS_CFMA_REG   0x120  // register address for the divider fifo
#define FCF_STS_CFMB_REG   0x124  // register address for the divider fifo
#define FCF_STS_DIV_REG    0x128  // register address for the divider fifo

/* Bitmasks for the HWA mapping  */
#define HWA_PADMASK  0x000000FF    // mask for the pad value
#define HWA_ROWMASK  0x0000003F    // mask for the row value
#define HWA_GAINMASK 0x00001FFF    // mask for the gain correction value

/* FCF control commands*/
#define FCF_DISABLE  0x00000000  // disable the FCF
#define FCF_ENABLE   0x34000000  // enables the FCF with flow control
#define FCF_RESET    0x80000000  // resets  the FCF

/* Mask for the Sequence Limit value */
#define FCF_SEQ_LIMIT_MASK 0xFF

/* Masks  for the FCF status bits */
#define RCU_FIFO_MASK   0x01  // RCU decoder fifo bit mask
#define CFD_FIFO_MASK   0x02  // cluster finder decoder fifo bit mask
#define CFM_FIFO_A_MASK 0x08  // cluster finder merger fifo A bit mask
#define CFM_FIFO_B_MASK 0x10  // cluster finder merger fifo B bit mask
#define DIV_FIFO_MASK   0x20  // cluster finder divider fifo bit mask


typedef struct {
  u_int32_t control;       // control register
  u_int32_t status_fifo;   // status register
  u_int32_t status_rcuDec;
  u_int32_t status_CFD;
  u_int32_t status_CFMA;
  u_int32_t status_CFMB;
  u_int32_t status_DIV;
} regFCF;
    
/*
*** utility functions *********************************************************
*/


/* set terminal to non-canonical mode */
void set_term_nc( struct termios * term_settings )
{
  struct termios settings;

  /* get current settings */
  tcgetattr( 0, term_settings );
  settings = (* term_settings);
     
  /* disable canonical mode, and set buffer size to 1 byte */
  settings.c_lflag &= (~ICANON);
  settings.c_cc[VTIME] = 0;
  settings.c_cc[VMIN] = 1;   
  tcsetattr( 0, TCSANOW, & settings );
}

/* reset terminal */     
void reset_term( struct termios * term_settings )
{
  tcsetattr( 0, TCSANOW, term_settings);
}



/* upload the row, pad and gain information for a specific HWA 
   into the H-RORC */
int upload_HWA( tRegion region, u_int32_t addr, u_int32_t data )
{
  int result;
  
  /*write addr into the CFG_RAM address register in the H-RORC */
  result = PSI_write( region, CFG_RAM_ADDR_REG, 4, 4, &addr );
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to access CFG_RAM_ADDR_REG: %s.\n", PSI_strerror( result ) );

  /*write the mapping into the CFG_RAM data in  register in the H-RORC */
  result = PSI_write( region, CFG_RAM_DIN_REG, 4, 4, &data );
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to access CFG_RAM_DIN_REG: %s.\n", PSI_strerror( result ) );

  return result;
};


/* load event buffer from file, returns number of words written */
int upload_mapping( tRegion region, const char * path, int gain, int level )
{

  u_int32_t data;
  u_int32_t addr = 0x0;
  FILE *pFile;
  int result = 0;

  /* open the mapping file*/
  pFile =fopen( path, "ro");
  if( pFile == NULL )
    {
      fprintf( stderr, "Failed to open file : %s\n", path);
      return -1;
    };

  while( result != EOF )
    {
      result = fscanf( pFile, "%x ", &data);
      if ( result < 0 )
	break;
      // printf("0x%x\n", data);
      result = upload_HWA( region, addr, data );
      addr++;
    };

  fclose(pFile);

  return result;

}

int write_FCF_command( tRegion region, u_int32_t command )
{
  int result;

  /*write into the FCF_CTRL_REG of the H-RORC */
  result = PSI_write( region, FCF_CONTROL_REG, 4, 4, & command );
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to access FCF_CONTROL_REG: %s.\n", PSI_strerror( result ) );

  return result;
};

int read_FCF_status( tRegion region, regFCF * FCF_status )
{
  int result;
  u_int32_t regValue;
  
  // read the FCF status register 
  result = PSI_read( region, FCF_STATUS_REG, 4, 4, &regValue );
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to access FCF_STATUS_REG: %s.\n", PSI_strerror( result ) );
  FCF_status->status_fifo = regValue;
  
  // read the FCF control register 
  result = PSI_read( region, FCF_CONTROL_REG, 4, 4, &regValue );
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to access FCF_STATUS_REG: %s.\n", PSI_strerror( result ) );
  FCF_status->control = regValue;
  
  // read the FCF status_rcuDec register 
  result = PSI_read( region, FCF_STS_RCUDEC_REG, 4, 4, &regValue );
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to access FCF_STS_RCUDEC_REG: %s.\n", PSI_strerror( result ) );
  FCF_status->status_rcuDec = regValue;
  
  // read the FCF status_CFD register 
  result = PSI_read( region, FCF_STS_CFD_REG, 4, 4, &regValue );
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to access FCF_STS_CFD_REG: %s.\n", PSI_strerror( result ) );
  FCF_status->status_CFD = regValue;
  
  // read the FCF status_CFMA register 
  result = PSI_read( region, FCF_STS_CFMA_REG, 4, 4, &regValue );
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to access FCF_STS_CFMA_REG: %s.\n", PSI_strerror( result ) );
  FCF_status->status_CFMA = regValue;
  
    // read the FCF status_CFMB register 
  result = PSI_read( region, FCF_STS_CFMB_REG, 4, 4, &regValue );
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to access FCF_STS_CFMA_REG: %s.\n", PSI_strerror( result ) );
  FCF_status->status_CFMB = regValue;
  
    // read the FCF status_DIV register 
  result = PSI_read( region, FCF_STS_DIV_REG, 4, 4, &regValue );
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to access FCF_STS_CFMA_REG: %s.\n", PSI_strerror( result ) );
  FCF_status->status_DIV = regValue;
  
  return result;
};

void print_FCF_status( regFCF FCF_status )
{
  
  // *****************************************************************************
  //        extract and print the control register information
  // *****************************************************************************
  int CTRL_SeqLimit, CTRL_ClusterLimit, CTRL_DeconvDecoder, CTRL_DeconvMerger, CTRL_SinglePadSupp, CTRL_BypassMerger, CTRL_EnableFCF, CTRL_XOFF_Enabled;
  CTRL_SeqLimit = (FCF_status.control & 0xFF);
  CTRL_ClusterLimit = (FCF_status.control & 0xFFFF00) >> 8;
  CTRL_DeconvDecoder = (FCF_status.control & 0x1000000) >> 24;
  CTRL_DeconvMerger = (FCF_status.control & 0x2000000 ) >> 25;
  CTRL_SinglePadSupp = (FCF_status.control & 0x4000000 ) >> 26;
  CTRL_BypassMerger = (FCF_status.control & 0x8000000 ) >> 27;
  CTRL_EnableFCF    = (FCF_status.control & 0x10000000 ) >> 28;
  CTRL_XOFF_Enabled = (FCF_status.control & 0x20000000 ) >> 29;
  
  printf("FCF Control Reg : SeqLimit: %d | ClusterLimit: %d | DeconvDecoder: %d | DeconvMerger: %d | SinglePadSuppr: %d | BypassMerger: %d | FCFEnabled: %d XOFFEnabled: %d \n", 
	 CTRL_SeqLimit, CTRL_ClusterLimit, CTRL_DeconvDecoder, CTRL_DeconvMerger, CTRL_SinglePadSupp, CTRL_BypassMerger, CTRL_EnableFCF, CTRL_XOFF_Enabled); 
  // *****************************************************************************
  
  // *****************************************************************************
  // extract the bit from the status register and print them
  // *****************************************************************************
  int RCUFifo, CFDFifo, CFMFifoA, CFMFifoB, DIVFifo;
  RCUFifo = ( FCF_status.status_fifo & RCU_FIFO_MASK );
  CFDFifo = ( FCF_status.status_fifo & CFD_FIFO_MASK ) >> 1;
  CFMFifoA = ( FCF_status.status_fifo & CFM_FIFO_A_MASK ) >> 3;
  CFMFifoB = ( FCF_status.status_fifo & CFM_FIFO_B_MASK ) >> 4;
  DIVFifo = ( FCF_status.status_fifo & DIV_FIFO_MASK ) >> 5;

  printf("Full : RCU_FIFO: %3d CFD_FIFO: %3d CFM_FIFOA: %3d CFM_FIFOB: %3d DIV_FIFO: %3d\n", RCUFifo, CFDFifo, CFMFifoA, CFMFifoB, DIVFifo);
  // *****************************************************************************
  
  


  // *****************************************************************************
  //       extract and print the FIFO information
  // *****************************************************************************
  int RCU_FIFO_MAX  =   FCF_status.status_rcuDec & 0x1FF;
  int RCU_FIFO      = ( FCF_status.status_rcuDec & 0x1FF0000 ) >> 16;
  
  int CFD_FIFO      = ( FCF_status.status_CFD & 0x1FF0000 ) >> 16;
  int CFD_FIFO_MAX  =   FCF_status.status_CFD & 0x1FF;
  
  int CFM_FIFOA_MAX  =   FCF_status.status_CFMA & 0x1FF;
  int CFM_FIFOA      = ( FCF_status.status_CFMA & 0x1FF0000 ) >> 16;
  
  int CFM_FIFOB_MAX  =   FCF_status.status_CFMB & 0x1FF;
  int CFM_FIFOB      = ( FCF_status.status_CFMB & 0x1FF0000 ) >> 16;
  
  int DIV_FIFO_MAX  =   FCF_status.status_DIV & 0x1FF;
  int DIV_FIFO      = ( FCF_status.status_DIV & 0x1FF0000 ) >> 16;
  
  
  printf("Max  : RCU_FIFO: %3d CFD_FIFO: %3d CFM_FIFOA: %3d CFM_FIFOB: %3d DIV_FIFO: %3d \n", RCU_FIFO_MAX, CFD_FIFO_MAX, CFM_FIFOA_MAX, CFM_FIFOB_MAX, DIV_FIFO_MAX);
  printf("Act  : RCU_FIFO: %3d CFD_FIFO: %3d CFM_FIFOA: %3d CFM_FIFOB: %3d DIV_FIFO: %3d \n", RCU_FIFO, CFD_FIFO, CFM_FIFOA, CFM_FIFOB, DIV_FIFO);

};


/*
*** user interface functions **************************************************
*/

/* commands */
enum t_command {
  NONE,
  RESET_FCF,
  ENABLE_FCF,
  DISABLE_FCF,
  PRINT_STATUS,
  LOAD_MAPPING
};



/* params */
struct t_params {
  enum t_command command;
  int link;
  int index;
  int gain;
  int seqLimit;
  const char * path;
  int silent;
};



#define INVALID_PARTITION (-1)



/* print usage */
void usage( void )
{
  fprintf( stderr, "Usage: fcf_ctonrol [options] command.\n" );
  fprintf( stderr, "\nCommand can be one of:\n" );
  fprintf( stderr, "\t-l\tupload patch mapping,\n" );
  fprintf( stderr, "\t-r\treset FCF,\n" );
  fprintf( stderr, "\t-e\tenable FCF,\n" );
  fprintf( stderr, "\t-d\tdisable FCF, \n" );
  fprintf( stderr, "\t-p\tprint status of FCF,\n" );

  fprintf( stderr, "\nOptions can be any of:\n" );
  fprintf( stderr, "\t-i n\tuse nth card\n" );
  fprintf( stderr, "\t-n n\tuse nth link\n" );
  fprintf( stderr, "\t-g n\tset gain calibration\n" );
  fprintf( stderr, "\t-s n\tset sequence limit\n");
  fprintf( stderr, "\t-q\t quit mode\n");
}



/* get error message */
const char * get_error( int result )
{
  switch ( result ) {
  case EMISSING:
    return "Missing option argument";

  case EMULTCMDS:
    return "Multiple commands";


  case EINVALCRD:
    return "Invalid card index";

  case EINVALLNK:
    return "Invalid link index";


  case ENOCMD:
    return "No command given";


  default:
    return strerror( result );
  }
}



/* parse command line arguments */
int parse_args( int argc, char * argv[], struct t_params * params )
{
  extern char * optarg;
  extern int optind, opterr, optopt;

  int opt;
  int result = SUCCESS;

  /* initialise params */
  params->command = NONE;
  params->index = 0;
  params->link = 4;
  params->gain = 0x1000;
  params->path = 0;
  params->seqLimit = 0;
  params->silent = 0;

  /* parse command line arguments */
  errno = 0;
  while ( (opt = getopt( argc, argv, ":del:i:n:g:prs:" )) != -1 ) {
    switch ( opt ) {

      /* enable the FCF */
    case 'd':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = DISABLE_FCF;
      break;

      /* enable the FCF */
    case 'e':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = ENABLE_FCF;
      break;

      /* set card number */
    case 'i':
      params->index = strtol( optarg, 0, 0 );
      if ( params->index < 0 || params->index > MAX_INDEX )
	result = EINVALCRD;
      break;

      /* upload the patch mapping */
    case 'l':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = LOAD_MAPPING;
      params->path = optarg;
      break;

    case 'r':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = RESET_FCF;
      break;

    case 'p':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = PRINT_STATUS;
      break;


    case 'h':
      usage();
      exit( 0 );

    case 'g':
      params->gain = strtol( optarg, 0, 0 );
      if ( params->gain < 0 || params->gain > 0x1FFF )
	result = EINVALGAIN;
      break;

    case 'n':
      params->link = strtol( optarg, 0, 0 ) + 4;
      if ( params->link < 0 || params->index > 1 )
	result = EINVALLNK;
      break;

    case 's':
      params->seqLimit = strtol( optarg, 0, 0 );
      if ( params->seqLimit < 0 || params->seqLimit > 0xFF )
	result = EINVALLIMIT;
      break;

    case 'q':
      params->silent = 1;
      break;
      
    case ':':
      result = EMISSING;
      break;

    case '?':
      result = EINVAL;
      break;
    }

    /* check for error */
    if ( result != SUCCESS ) return result;
  }

  /* check if command was given */
  if ( params->command == NONE ) result = ENOCMD;

 
  return result;
}



/* main */
int main( int argc, char * argv[] )
{
  struct t_params params;

  char regionpath[MAX_DEVICE_PATH];
  int result;
  regFCF FCF_Status;
  u_int32_t FCF_Command;
  tRegion region;
  tRegionStatus info;



  /* parse command line arguments */
  result = parse_args( argc, argv, & params );
  if ( result != SUCCESS ) {
    fprintf( stderr, "Error while parsing command line options: %s.\n\n",
	     get_error( result ) );
    usage();
    return result;
  }

  /* print greeting */
  // system("clear");
  if (params.silent == 0){
    printf( "H-RORC Status Utility, Version %s\n\n", VERSION );
  };

  /* open card */
  sprintf( regionpath, "/dev/psi/vendor/0x%04hX/device/0x%04hX/%i/base%d",
	   VENDOR_ID, DEVICE_ID, params.index, params.link );
  result = PSI_openRegion( & region, regionpath );
  if ( result != PSI_OK ) {
    fprintf( stderr, "Failed to open device: %s.\n", PSI_strerror( result ) );
    return EACCES;
  }


  /* print device info */
  result = PSI_getRegionStatus( region, & info );
  if ( result != PSI_OK ) {
    fprintf( stderr, "Failed to get device info: %s.\n", 
	     PSI_strerror( result ) );
    PSI_closeRegion( region );
    return EIO;
  }
  if (params.silent == 0 ){
  printf( "- Using device %d link %d (%02x:%02x.%x)\n", params.index, params.link, 
	  info.bus, PCI_SLOT( info.devfn ), PCI_FUNC( info.devfn ) );
  };



  /* select mode of operation */
  switch ( params.command ) {

  case DISABLE_FCF :
    FCF_Command = FCF_DISABLE;
    write_FCF_command( region, FCF_Command );
    printf("FCF disabled\n");
    break;

  case ENABLE_FCF :
    FCF_Command = FCF_ENABLE | (params.seqLimit & FCF_SEQ_LIMIT_MASK);
    write_FCF_command( region, FCF_Command );
    printf("FCF enabled\n");
    break;

  case RESET_FCF :
    FCF_Command = FCF_RESET;
    write_FCF_command( region, FCF_Command );
    printf("FCF reseted\n");
    break;

  case PRINT_STATUS :
    printf("Status of FCF\n");
    read_FCF_status( region, &FCF_Status );
    print_FCF_status ( FCF_Status );
    break;
 
  case LOAD_MAPPING :
    printf("Loading mapping file %s ..\n", params.path);
    upload_mapping( region, params.path, params.gain, params.silent );
    break;

  default:
    fprintf( stderr, "Command not implemented.\n" );
    result = ENOSYS;
  }
  
  if (params.silent == 0) {
    if ( result == SUCCESS ) printf( "Done.\n" );
  };
  PSI_closeRegion( region );

  return result;
}
