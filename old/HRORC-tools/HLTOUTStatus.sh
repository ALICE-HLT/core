#!/bin/bash
#
# Shows status of HLTOUT links
#
# Intial Author : Jochen Thaeder
# Date : 2009-08-13
# Filename : HLTOUTStatus.sh
#
#####################################################


CHECK_LINK () {
  
    RORC_STATUS=`ssh fephltout$1 pci_device_rw -slot $2 $3 $4 $5 0xc4 4 1`

    RORCCMD=`echo ${RORC_STATUS} | awk -F': ' '{print $2}' | awk -F' 0x' '{ print $1 }'`
    
    if [ "${RORCCMD}" == "0x20000000" ] ; then
	echo "  -> Link closed"
    elif [ "${RORCCMD}" == "0xC0000000" ] ; then
	echo "  -> Link open"
    else
	echo "Status unkown : ${RORCCMD}"
    fi
}

echo " >> NODE : fephltout0"
echo " ===================="

echo -n "DDL  : 7680     [340-0|0-0]" ; CHECK_LINK 0 3 4 0 0
echo -n "DDL  : 7681     [340-1|0-1]" ; CHECK_LINK 0 3 4 0 1
echo -n "DDL  : 7682     [250-0|1-0]" ; CHECK_LINK 0 2 5 0 0
echo -n "DDL  : 7683     [250-1|1-1]" ; CHECK_LINK 0 2 5 0 1

echo " "
echo " >> NODE : fephltout1"
echo " ===================="

echo -n "DDL  : 7684     [340-0|0-0]" ; CHECK_LINK 1 3 4 0 0
echo -n "DDL  : 7685     [340-1|0-1]" ; CHECK_LINK 1 3 4 0 1
echo -n "DDL  : 7686     [250-0|1-0]" ; CHECK_LINK 1 2 5 0 0
echo -n "DDL  : 7687     [250-1|1-1]" ; CHECK_LINK 1 2 5 0 1

echo " "
echo " >> NODE : fephltout2"
echo " ===================="

echo -n "DDL  : 7688     [360-0|0-0]" ; CHECK_LINK 2 3 6 0 0
echo -n "DDL  : ____     [360-1|0-1]" ; CHECK_LINK 2 3 6 0 1
echo -n "DDL  : 7689     [270-0|1-0]" ; CHECK_LINK 2 2 7 0 0
echo -n "DDL  : ____     [270-1|1-1]" ; CHECK_LINK 2 2 7 0 1


