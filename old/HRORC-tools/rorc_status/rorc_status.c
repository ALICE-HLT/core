/*
   H-RORC Status Utility, Version 1

   Torsten Alt (ta) <talt@kip.uni-heidelberg.de>

   history:
   2008-05-05 initial implementation (ta)
   2010-02-18 VERSION 0.2.3 : added link status information to the DDL status (ta)
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <asm/errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <termios.h>
#include <linux/pci.h>

#include <psi.h>
#include <psi_error.h>



/* version string */
#define VERSION "0.2.3"

/* error codes */
#ifndef SUCCESS
#define SUCCESS 0
#endif
#define EMISSING  1000 /* Missing option argument */
#define EMULTCMDS 1001 /* Multiple commands */
#define EINVALCRD 1002 /* Invalid card index */
#define EINVALLNK 1003 /* Invalid link index */
#define ENOCMD    1004 /* No command */


/* H-RORC identification */
#define VENDOR_ID           0x10EE
#define DEVICE_ID           0xDEAD
#define BASE_ADDRESS_REGION 0
#define MAX_INDEX           32
#define MAX_DEVICE_PATH     256

/* H-RORC internal register addresses */
/* DMA engine */
#define DMA_BUFFER_START_ADDR_L  0x0   // DMA buffer start address 31..0
#define DMA_BUFFER_START_ADDR_H  0x4   // DMA buffer start address 63..31
#define DMA_BUFFER_END_ADDR_L    0x8   // DMA buffer end address 31..0
#define DMA_BUFFER_END_ADDR_H    0xC   // DMA buffer end address 63..32
#define DMA_REPORT_START_ADDR_L  0x10  // DMA report buffer start address 31..0
#define DMA_REPORT_START_ADDR_H  0x14  // DMA report buffer start address 63..32
#define DMA_REPORT_END_ADDR_L    0x18  // DMA report buffer end address 31..0
#define DMA_REPORT_END_ADDR_H    0x1C  // DMA report buffer end address 63..32
#define DMA_RDPTR_ADDR_L         0x20  // DMA read pointer address 31..0
#define DMA_RDPTR_ADDR_H         0x24  // DMA read pointer address 63..32
#define DMA_RDPTR_INIT_L         0x28  // DMA initial read pointer value 31..0
#define DMA_RDPTR_INIT_H         0x2C  // DMA initial read pointer value 63..32
#define DMA_FIFO                 0x30  // DMA FIFO status and control register
#define DMA_CTRL                 0x34  // DMA engine control register
#define DMA_EVENTCOUNTER         0x38  // DMA event counter
#define DMA_BLOCKSIZE            0x3C  // DMA PCI-X transmission burst length

/* DDL interface */
#define DDL_CTRL_REG             0xE8  // DDL interface control register
#define DDL_LINK_REG             0xD0  // DDL-DIU Link status and FlowControl
#define DDL_EVENTCOUNTER_REG     0xCC  // DIU event counter
#define DDL_DEADTIME_REG         0xC8  // DIU deadtime counter


/* DMA profiler */
#define DMA_PROFILER_0           0x140  /* idle */
#define DMA_PROFILER_1           0x144  /* transfer time */
#define DMA_PROFILER_2           0x148  /* report time */
#define DMA_PROFILER_3           0x14C  /* readpointer update time */
#define DMA_PROFILER_4           0x150  /* number of transfered blocks */
#define DMA_PROFILER_5           0x154  /* number of transfered events */
#define DMA_PROFILER_6           0x158  /* actual DMA address low */
#define DMA_PROFILER_7           0x15C  /* actual DMA address high */
#define DMA_PROFILER_8           0x160  /* actual readpointer low */
#define DMA_PROFILER_9           0x164  /* actual readpointer high */

#define VERSION_ID_ADDR          0x90  // version ID of the firmware

/* Bitmasks for the different H-RORC registers */
#define DMA_ENABLE_MASK         0x00000001  // mask for the DMA enable bit
#define DMA_BLOCKSIZE_MASK      0x00000FFF  // mask for the PCI-X burst length
#define DMA_FIFO_NUMWORDS_MASK  0x000003FF  // mask for the FIFO numwords
#define DMA_FIFO_FULL_MASK      0x80000000  // mask for the FIFO progfull bit
#define DMA_FIFO_PROG_MASK      0x40000000  // mask for the FIFO progempty bit
#define DMA_FIFO_EMPTY_MASK     0x20000000  // mask for the FIFO empty bit
#define DMA_FIFO_FULL_BIT       32
#define DMA_FIFO_PROG_BIT       31
#define DMA_FIFO_EMPTY_BIT      30
#define DDL_FLOW_CONTROL_MASK   0x00000001
#define DDL_CTRL_MASK           0x00000003 // mask for the DDL control bits 
#define DIU_SELECTED_MASK       0x00000002 // mask for the DIU selected bit
#define DIU_ENABLED_MASK        0x00000001 // mask for the DIU enabled bit
#define DDL_LINK_UP_MASK        0x80000000  // mask for the DDL link status bit
#define DDL_LINK_BUSY_MASK      0x00000001  // mask for the flow control bit


/*
*** utility functions *********************************************************
*/


/* set terminal to non-canonical mode */
void set_term_nc( struct termios * term_settings )
{
  struct termios settings;

  /* get current settings */
  tcgetattr( 0, term_settings );
  settings = (* term_settings);
     
  /* disable canonical mode, and set buffer size to 1 byte */
  settings.c_lflag &= (~ICANON);
  settings.c_cc[VTIME] = 0;
  settings.c_cc[VMIN] = 1;   
  tcsetattr( 0, TCSANOW, & settings );
}

/* reset terminal */     
void reset_term( struct termios * term_settings )
{
  tcsetattr( 0, TCSANOW, term_settings);
}



/* H-RORC DMA engine */
struct t_hrorc_dma {
  u_int32_t buffer_start_addr_l;
  u_int32_t buffer_start_addr_h;
  u_int32_t buffer_end_addr_l;
  u_int32_t buffer_end_addr_h;
  u_int32_t report_start_addr_l;
  u_int32_t report_start_addr_h;
  u_int32_t report_end_addr_l;
  u_int32_t report_end_addr_h;
  u_int32_t rdptr_addr_l;
  u_int32_t rdptr_addr_h;
  u_int32_t rdptr_init_l;
  u_int32_t rdptr_init_h;
  u_int32_t eventcount;
  u_int32_t control;
  u_int32_t blocksize;
  u_int32_t fifo;
};

/* H-RORC DDL interface */
struct t_hrorc_ddl {
  u_int32_t control;
  u_int32_t eventcount;
  u_int32_t flowcontrol;
  u_int32_t linkstatus;
  u_int32_t deadtime;
};

/* H-RORC DMA profiler */
struct t_hrorc_profiler {
  u_int32_t idle_time;
  u_int32_t transfer_time;
  u_int32_t report_time;
  u_int32_t hbuff_full_time;
  u_int32_t numBlocks;
  u_int32_t numEvents;
  u_int32_t act_dma_addr_l;
  u_int32_t act_dma_addr_h;
  u_int32_t act_rdptr_l;
  u_int32_t act_rdptr_h;
};

/* H-RORC Status */
struct t_hrorc_status {
  u_int32_t dma_control;
  u_int32_t fifo_numwords;
  u_int32_t ec_diu;
  u_int32_t ec_dma;
  u_int32_t flow_control;
};


u_int32_t hrorc_version;

/* read the PROFILER registers in the H-RORC  */
int read_PROFILER( tRegion region, struct t_hrorc_profiler *hrorc_profiler )
{
  int result = 0;

  result = PSI_read( region, DMA_PROFILER_0, _dword_, 4, &(hrorc_profiler->idle_time));
  result = PSI_read( region, DMA_PROFILER_1, _dword_, 4, &(hrorc_profiler->transfer_time));
  result = PSI_read( region, DMA_PROFILER_2, _dword_, 4, &(hrorc_profiler->report_time));
  result = PSI_read( region, DMA_PROFILER_3, _dword_, 4, &(hrorc_profiler->hbuff_full_time));
  result = PSI_read( region, DMA_PROFILER_4, _dword_, 4, &(hrorc_profiler->numBlocks));
  result = PSI_read( region, DMA_PROFILER_5, _dword_, 4, &(hrorc_profiler->numEvents));
  result = PSI_read( region, DMA_PROFILER_6, _dword_, 4, &(hrorc_profiler->act_dma_addr_l));
  result = PSI_read( region, DMA_PROFILER_7, _dword_, 4, &(hrorc_profiler->act_dma_addr_h));
  result = PSI_read( region, DMA_PROFILER_8, _dword_, 4, &(hrorc_profiler->act_rdptr_l));
  result = PSI_read( region, DMA_PROFILER_9, _dword_, 4, &(hrorc_profiler->act_rdptr_h));
  return result;
};

/* read (some) of the status registers in the H-RORC */
int read_STATUS( tRegion region, struct t_hrorc_status *hrorc_status )
{
  int result;

  /* read the H-RORC status register : FifoNumwords, EventCounter_DIU, EventCounter_DMA, FlowControl */
  result = PSI_read( region, DMA_CTRL, _dword_, 4, &(hrorc_status->dma_control));
  result = PSI_read( region, DMA_FIFO, _dword_, 4, &(hrorc_status->fifo_numwords));
  result = PSI_read( region, DMA_EVENTCOUNTER, _dword_, 4, &(hrorc_status->ec_dma));
  result = PSI_read( region, DDL_EVENTCOUNTER_REG, _dword_, 4, &(hrorc_status->ec_diu));
  result = PSI_read( region, DDL_LINK_REG, _dword_, 4, &(hrorc_status->flow_control));
  return result;
};

int print_STATUS( int quit, struct t_hrorc_status hrorc_status )
{

  if (quit == 0){
    printf("RORC Status: \n");
    printf("--------------\n");
    printf("DMA enabled....: %01d \n", (hrorc_status.dma_control & DMA_ENABLE_MASK));
    printf("Flow Control...: %01d \n",(hrorc_status.flow_control & DDL_FLOW_CONTROL_MASK));
    printf("Fifo Numwords..: 0x%08X ( %08d )\n",(hrorc_status.fifo_numwords & DMA_FIFO_NUMWORDS_MASK),(hrorc_status.fifo_numwords & DMA_FIFO_NUMWORDS_MASK));
    printf("DMA Eventcount.: 0x%08X ( %08u )\n",hrorc_status.ec_dma, hrorc_status.ec_dma);
    printf("DIU Eventcount.: 0x%08X ( %08u )\n",hrorc_status.ec_diu, hrorc_status.ec_diu);
  } else {
    printf("%01d\n", (hrorc_status.dma_control & DMA_ENABLE_MASK));
    printf("%d\n",(hrorc_status.flow_control & DDL_FLOW_CONTROL_MASK));
    printf("%d\n",(hrorc_status.fifo_numwords & DMA_FIFO_NUMWORDS_MASK)); 
    printf("%u\n",hrorc_status.ec_dma);
    printf("%u\n",hrorc_status.ec_diu);
  };

  return 0;
};


int read_DDL( tRegion region, struct t_hrorc_ddl *hrorc_ddl )
{
  int result;
  u_int32_t value;

  /* read the DDL control register */
  result = PSI_read( region, DDL_CTRL_REG, _dword_, 4, &value);
  hrorc_ddl->control = value & DDL_CTRL_MASK;

  /* read the DDL eventcounter register */
  result = PSI_read( region, DDL_EVENTCOUNTER_REG, _dword_, 4, &(hrorc_ddl->eventcount));

  /* read the link status/control register */
  result = PSI_read( region, DDL_LINK_REG, _dword_, 4, &value);
  hrorc_ddl->flowcontrol = (value & DDL_LINK_BUSY_MASK); 
  hrorc_ddl->linkstatus = ( value & DDL_LINK_UP_MASK ) >> 31;

 /* read the DDL deadtime register */
  result = PSI_read( region, DDL_DEADTIME_REG, _dword_, 4, &(hrorc_ddl->deadtime));

  return result;
};


int read_DMA( tRegion region, struct t_hrorc_dma *hrorc_dma )
{
  int result;

  /* read DMA Buffer register */
  result = PSI_read( region, DMA_BUFFER_START_ADDR_L, _dword_, 4, &(hrorc_dma->buffer_start_addr_l));
  result = PSI_read( region, DMA_BUFFER_START_ADDR_H, _dword_, 4, &(hrorc_dma->buffer_start_addr_h));
  result = PSI_read( region, DMA_BUFFER_END_ADDR_L, _dword_, 4, &(hrorc_dma->buffer_end_addr_l));
  result = PSI_read( region, DMA_BUFFER_END_ADDR_H, _dword_, 4, &(hrorc_dma->buffer_end_addr_h));
  /* read DMA report buffer register */
  result = PSI_read( region, DMA_REPORT_START_ADDR_L, _dword_, 4, &(hrorc_dma->report_start_addr_l));
  result = PSI_read( region, DMA_REPORT_START_ADDR_H, _dword_, 4, &(hrorc_dma->report_start_addr_h));
  result = PSI_read( region, DMA_REPORT_END_ADDR_L, _dword_, 4, &(hrorc_dma->report_end_addr_l));
  result = PSI_read( region, DMA_REPORT_END_ADDR_H, _dword_, 4, &(hrorc_dma->report_end_addr_h));
  /* read DMA read pointer register */
  result = PSI_read( region, DMA_RDPTR_ADDR_L, _dword_, 4, &(hrorc_dma->rdptr_addr_l));
  result = PSI_read( region, DMA_RDPTR_ADDR_H, _dword_, 4, &(hrorc_dma->rdptr_addr_h));
  result = PSI_read( region, DMA_RDPTR_INIT_L, _dword_, 4, &(hrorc_dma->rdptr_init_l));
  result = PSI_read( region, DMA_RDPTR_INIT_H, _dword_, 4, &(hrorc_dma->rdptr_init_h));
  /* read DMA eventcounter register */
  result = PSI_read( region, DMA_EVENTCOUNTER, _dword_, 4, &(hrorc_dma->eventcount));
  /* read DMA_CTRL register */
  result = PSI_read( region, DMA_CTRL, _dword_, 4, &(hrorc_dma->control));
  /* read DMA Blocksize register */
  result = PSI_read( region, DMA_BLOCKSIZE, _dword_, 4, &(hrorc_dma->blocksize));
  /* read DMA FIFO register */
  result = PSI_read( region, DMA_FIFO, _dword_, 4, &(hrorc_dma->fifo));

  return result;
}

int read_VERSION( tRegion region, u_int32_t *hrorc_version )
{
  int result;
  u_int32_t value;
  result = PSI_read( region, VERSION_ID_ADDR, _dword_, 4, &value );
  *hrorc_version = value;

  return result;

};

void print_VERSION( u_int32_t hrorc_version)
{
  printf("H-RORC version ID: 0x%08x\n", hrorc_version );
};

int read_FIFO( tRegion region, u_int32_t *hrorc_fifo )
{
  int result;
  u_int32_t value;
  result = PSI_read( region, DMA_FIFO, _dword_, 4, &value );
  *hrorc_fifo = value;

  return result;

};

void print_FIFO( u_int32_t hrorc_fifo)
{
  printf("Fifo : 0x%08x\n", hrorc_fifo );
};

void print_DMA( struct t_hrorc_dma hrorc_dma )
{
  printf("\nDMA engine:\n");
  printf("--------------------------------------------------------------------------------------------\n");

  printf("DMA enable\t: 0x%01x\t DMA blocksize: 0x%03x\n",(hrorc_dma.control & DMA_ENABLE_MASK), (hrorc_dma.blocksize & DMA_BLOCKSIZE_MASK));

  printf("DMA eventcounter: 0x%08x (%08d)\n", hrorc_dma.eventcount, hrorc_dma.eventcount);

  printf("DMA fifo\t: 0x%03x\t full: %01d\t progempty: %01d\t empty: %01d\n", (hrorc_dma.fifo & DMA_FIFO_NUMWORDS_MASK), ((hrorc_dma.fifo & DMA_FIFO_FULL_MASK) >> (DMA_FIFO_FULL_BIT-1)), ((hrorc_dma.fifo & DMA_FIFO_PROG_MASK) >> (DMA_FIFO_PROG_BIT-1)), ((hrorc_dma.fifo & DMA_FIFO_EMPTY_MASK) >> (DMA_FIFO_EMPTY_BIT-1)));

  printf("Buffer start address: 0x%08x.%08x\t Buffer end address: 0x%08x.%08x\n",hrorc_dma.buffer_start_addr_h, hrorc_dma.buffer_start_addr_l, hrorc_dma.buffer_end_addr_h, hrorc_dma.buffer_end_addr_l);

  printf("Report start address: 0x%08x.%08x\t Report end address: 0x%08x.%08x\n",hrorc_dma.report_start_addr_h, hrorc_dma.report_start_addr_l, hrorc_dma.report_end_addr_h, hrorc_dma.report_end_addr_l);

  printf("Read Pointer address: 0x%08x.%08x\t Read Pointer Init : 0x%08x.%08x\n",hrorc_dma.rdptr_addr_h, hrorc_dma.rdptr_addr_l, hrorc_dma.rdptr_init_h, hrorc_dma.rdptr_init_l);
 printf("--------------------------------------------------------------------------------------------\n");

};

void print_DDL( struct t_hrorc_ddl hrorc_ddl )
{
  printf("\nDDL interface: \n");
  printf("--------------------------------------------------------------------------------------------\n");
  printf("DIU selected: %01d\t DIU enabled: %01d\n", ((hrorc_ddl.control & DIU_SELECTED_MASK) > 1), (hrorc_ddl.control & DIU_ENABLED_MASK));
  printf("DIU eventcounter: 0x%08x (%08d)\n",hrorc_ddl.eventcount, hrorc_ddl.eventcount);
  printf("DIU deadtime    : 0x%08x (%08d)\n",hrorc_ddl.deadtime, hrorc_ddl.deadtime);
  printf("DIU linkstatus  : 0x%08x \n",hrorc_ddl.linkstatus);
  printf("--------------------------------------------------------------------------------------------\n");
};

void print_PROFILER( int quit, struct t_hrorc_profiler hrorc_profiler )
{
  if (quit == 0){
    printf("DMA profiler: \n");
    printf("--------------\n");
    printf(" Idle...........: 0x%08x \n",hrorc_profiler.idle_time);
    printf(" Transfer.......: 0x%08x \n",hrorc_profiler.transfer_time);
    printf(" Report.........: 0x%08x \n",hrorc_profiler.report_time);
    printf(" Update RdPtr...: 0x%08x \n",hrorc_profiler.hbuff_full_time);
    printf(" Blocks trans...: 0x%08x \n",hrorc_profiler.numBlocks);
    printf(" Events trans...: 0x%08x \n",hrorc_profiler.numEvents);
    printf(" Actual DMA addr: 0x%08x.%08x \n",hrorc_profiler.act_dma_addr_h,hrorc_profiler.act_dma_addr_l);
    printf(" Actual ReadPtr : 0x%08x.%08x \n",hrorc_profiler.act_rdptr_h,hrorc_profiler.act_rdptr_l);
  } else {
      printf(" 0x%08x ",hrorc_profiler.idle_time);
      printf(" 0x%08x ",hrorc_profiler.transfer_time);
      printf(" 0x%08x ",hrorc_profiler.report_time);
      printf(" 0x%08x ",hrorc_profiler.hbuff_full_time);
      printf(" 0x%08x ",hrorc_profiler.numBlocks);
      printf(" 0x%08x ",hrorc_profiler.numEvents);
      printf(" 0x%08x.%08x ",hrorc_profiler.act_dma_addr_h,hrorc_profiler.act_dma_addr_l);
      printf(" 0x%08x.%08x ",hrorc_profiler.act_rdptr_h,hrorc_profiler.act_rdptr_l);
  };

};

/*
*** user interface functions **************************************************
*/



/* commands */
enum t_command {
  NONE,
  SHOW_ALL,
  SHOW_DMA,
  SHOW_DDL,
  SHOW_VERSION,
  SHOW_EC,
  SHOW_FIFO,
  SHOW_REPLAY,
  SHOW_PROFILER,
  SHOW_STATUS
};



/* params */
struct t_params {
  enum t_command command;
  int link;
  int index;
  int silent;
  int continous;
};



#define INVALID_PARTITION (-1)



/* print usage */
void usage( void )
{
  fprintf( stderr, "Usage: rorc_status [options] command.\n" );
  fprintf( stderr, "\nCommand can be one of:\n" );
  fprintf( stderr, "\t-a\tshow all,\n" );
  fprintf( stderr, "\t-d\tshow DMA\n" );
  fprintf( stderr, "\t-l\tshow DDL\n" );
  fprintf( stderr, "\t-v\tshow VERSION ID\n" );
  fprintf( stderr, "\t-c\tshow EventCounter\n" );
  fprintf( stderr, "\t-f\tshow FIFOs\n" );
  fprintf( stderr, "\t-p\tshow DMA profiler\n" );
  fprintf( stderr, "\t-r\tshow Datareplay\n" );
  fprintf( stderr, "\nOptions can be any of:\n" );
  fprintf( stderr, "\t-i n\tuse nth card\n" );
  fprintf( stderr, "\t-n n\tuse nth link\n" );
  fprintf( stderr, "\t-q\t quit mode\n");
  fprintf( stderr, "\t-c\t continous mode\n" );
}



/* get error message */
const char * get_error( int result )
{
  switch ( result ) {
  case EMISSING:
    return "Missing option argument";

  case EMULTCMDS:
    return "Multiple commands";


  case EINVALCRD:
    return "Invalid card index";

  case EINVALLNK:
    return "Invalid link index";


  case ENOCMD:
    return "No command given";


  default:
    return strerror( result );
  }
}



/* parse command line arguments */
int parse_args( int argc, char * argv[], struct t_params * params )
{
  extern char * optarg;
  extern int optind, opterr, optopt;

  int opt;
  int result = SUCCESS;

  /* initialise params */
  params->command = NONE;
  params->index = 0;
  params->link = 0;
  params->silent = 0;
  params->continous = 0;

  /* parse command line arguments */
  errno = 0;
  while ( (opt = getopt( argc, argv, ":adlvcfpqsi:n:" )) != -1 ) {
    switch ( opt ) {
    case 'a':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = SHOW_ALL;
      break;
      

    case 'd':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = SHOW_DMA;
      break;

    case 'l':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = SHOW_DDL;
      break;


    case 'v':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = SHOW_VERSION;
      break;


    case 's':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = SHOW_STATUS;
      break;

    case 'f':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = SHOW_FIFO;
      break;

    case 'p':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = SHOW_PROFILER;
      break;

    case 'h':
      usage();
      exit( 0 );

    case 'i':
      params->index = strtol( optarg, 0, 0 );
      if ( params->index < 0 || params->index > MAX_INDEX )
	result = EINVALCRD;
      break;

    case 'n':
      params->link = strtol( optarg, 0, 0 );
      if ( params->link < 0 || params->index > 1 )
	result = EINVALLNK;
      break;

    case 'q':
      params->silent = 1;
      break;

    case 'c':
      params->continous = 1;
      break;
      
    case ':':
      result = EMISSING;
      break;

    case '?':
      result = EINVAL;
      break;
    }

    /* check for error */
    if ( result != SUCCESS ) return result;
  }

  /* check if command was given */
  if ( params->command == NONE ) result = ENOCMD;

 
  return result;
}



/* main */
int main( int argc, char * argv[] )
{
  struct t_params params;
  struct t_hrorc_dma hrorc_dma;
  struct t_hrorc_ddl hrorc_ddl;
  struct t_hrorc_profiler hrorc_profiler;
  struct t_hrorc_status hrorc_status;
  u_int32_t hrorc_version = 0;
  u_int32_t hrorc_fifo = 0;

  time_t act_time;

  char regionpath[MAX_DEVICE_PATH];
  int result;
  tRegion region;
  tRegionStatus info;


  /* parse command line arguments */
  result = parse_args( argc, argv, & params );
  if ( result != SUCCESS ) {
    fprintf( stderr, "Error while parsing command line options: %s.\n\n",
	     get_error( result ) );
    usage();
    return result;
  }

  /* print greeting */
  // system("clear");
  if (params.silent == 0){
    printf( "H-RORC Status Utility, Version %s\n\n", VERSION );
  };

  /* open card */
  sprintf( regionpath, "/dev/psi/vendor/0x%04hX/device/0x%04hX/%i/base%d",
	   VENDOR_ID, DEVICE_ID, params.index, params.link );
  result = PSI_openRegion( & region, regionpath );
  if ( result != PSI_OK ) {
    fprintf( stderr, "Failed to open device: %s.\n", PSI_strerror( result ) );
    return EACCES;
  }


  /* print device info */
  result = PSI_getRegionStatus( region, & info );
  if ( result != PSI_OK ) {
    fprintf( stderr, "Failed to get device info: %s.\n", 
	     PSI_strerror( result ) );
    PSI_closeRegion( region );
    return EIO;
  }
  if (params.silent == 0 ){
  printf( "- Using device %d link %d (%02x:%02x.%x)\n", params.index, params.link, 
	  info.bus, PCI_SLOT( info.devfn ), PCI_FUNC( info.devfn ) );
  };



  /* select mode of operation */
  switch ( params.command ) {
  case SHOW_ALL:
    /* read values from H-RORC register */
    result = read_DMA( region, &hrorc_dma );
    result = read_VERSION( region, &hrorc_version );
    result = read_DDL( region, &hrorc_ddl );
    /* print values from H-RORC register */
    print_VERSION ( hrorc_version);
    print_DMA( hrorc_dma );
    print_DDL( hrorc_ddl );
    break;

  case SHOW_DMA:
    result = read_DMA( region, &hrorc_dma );
    print_DMA( hrorc_dma);
    break;

  case SHOW_DDL:
    result = read_DDL( region, &hrorc_ddl );
    print_DDL( hrorc_ddl);
    break;
    

  case SHOW_STATUS:
    result = read_STATUS( region, &hrorc_status );
    print_STATUS(params.silent, hrorc_status); 
    break;


  case SHOW_VERSION:
    result = read_VERSION( region, &hrorc_version );
    print_VERSION( hrorc_version);
    break;

  case SHOW_FIFO:
    result = read_FIFO( region, &hrorc_fifo );
    print_FIFO( hrorc_fifo);
    break;

  case SHOW_PROFILER:
    do {
    result = read_PROFILER( region, &hrorc_profiler );
    if ( result != PSI_OK ) {
       /* reopen card if region has been closed */
      if ( result == PSI_INVALID_REGION ) {
        fprintf(stderr, "Region error. Reopening region \n");
        result = PSI_openRegion( & region, regionpath );
        if ( result != PSI_OK ) {
          fprintf( stderr, "Failed to open device: %s.\n", PSI_strerror( result ) );
          return EACCES;
	};
      } else {
	fprintf( stderr, "PSI error : %s \n", PSI_strerror( result));
      };
    };
      /* print value */
    if (params.silent == 1) {
      print_PROFILER(1, hrorc_profiler);
      time(&act_time); 
      /* chartime = asctime(localtime(&act_time)); */
      printf("%s",asctime(localtime(&act_time)));
    } else
      print_PROFILER(0, hrorc_profiler);
    fflush(stdout);
    sleep(1);
    } while (params.continous == 1);
    break;

  default:
    fprintf( stderr, "Command not implemented.\n" );
    result = ENOSYS;
  }
  
  if (params.silent == 0) {
    if ( result == SUCCESS ) printf( "Done.\n" );
  };
  PSI_closeRegion( region );

  return result;
}
