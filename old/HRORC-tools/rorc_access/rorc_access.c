/*
   rorc_access  Utility, Version 1

   Torsten Alt (ta) <talt@kip.uni-heidelberg.de>

   history:
   2009-12-08 initial implementation (ta)
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <asm/errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <termios.h>
#include <linux/pci.h>

#include <psi.h>
#include <psi_error.h>



/* version string */
#define VERSION "v0.1"

/* error codes */
#ifndef SUCCESS
#define SUCCESS 0
#endif
#define EMISSING    1000 /* Missing option argument */
#define EMULTCMDS   1001 /* Multiple commands */
#define EINVALCRD   1002 /* Invalid card index */
#define EINVALBAR   1003 /* Invalid link index */
#define EINVALADDR  1004 /* Invalid link index */
#define ENOCMD      1006 /* No command */

/* message verbosity */
#define QUIET	0
#define DEFAULT	1
#define VERBOSE	2


/* H-RORC identification */
#define VENDOR_ID           0x10EE
#define DEVICE_ID           0xDEAD
#define BASE_ADDRESS_REGION 0
#define MAX_INDEX           32
#define MAX_DEVICE_PATH     256


/*
*** utility functions *********************************************************
*/


/* set terminal to non-canonical mode */
void set_term_nc( struct termios * term_settings )
{
  struct termios settings;

  /* get current settings */
  tcgetattr( 0, term_settings );
  settings = (* term_settings);
     
  /* disable canonical mode, and set buffer size to 1 byte */
  settings.c_lflag &= (~ICANON);
  settings.c_cc[VTIME] = 0;
  settings.c_cc[VMIN] = 1;   
  tcsetattr( 0, TCSANOW, & settings );
}

/* reset terminal */     
void reset_term( struct termios * term_settings )
{
  tcsetattr( 0, TCSANOW, term_settings);
}




/*
*** user interface functions **************************************************
*/

/* commands */
enum t_command {
  NONE,
  READ_ACCESS,
  WRITE_ACCESS
};



/* params */
struct t_params {
  enum t_command command;
  int card;
  int bar;
  int address;
  int writeData;
};



#define INVALID_PARTITION (-1)



/* print usage */
void usage( void )
{
  fprintf( stderr, "Usage: fcf_ctonrol [options] command.\n" );
  fprintf( stderr, "\nCommand can be one of:\n" );
  fprintf( stderr, "\t-r\tread from address,\n" );
  fprintf( stderr, "\t-w\twrite to address,\n" );

  fprintf( stderr, "\nOptions can be any of:\n" );
  fprintf( stderr, "\t-i n\tuse ith card\n" );
  fprintf( stderr, "\t-n n\tuse nth bar\n" );
  fprintf( stderr, "\t-a n\tacces nth address\n" );
  fprintf( stderr, "\t-q\t quit mode\n");
}



/* get error message */
const char * get_error( int result )
{
  switch ( result ) {
  case EMISSING:
    return "Missing option argument";

  case EMULTCMDS:
    return "Multiple commands";


  case EINVALCRD:
    return "Invalid card index";

  case EINVALBAR:
    return "Invalid bar index";


  case ENOCMD:
    return "No command given";


  default:
    return strerror( result );
  }
}



/* parse command line arguments */
int parse_args( int argc, char * argv[], struct t_params * params )
{
  extern char * optarg;
  extern int optind, opterr, optopt;

  int opt;
  int result = SUCCESS;

  /* initialise params */
  params->command = NONE;
  params->card = 0;
  params->bar = 0;
  params->address = 0x0;
  params->writeData = 0x0;

  /* parse command line arguments */
  errno = 0;
  while ( (opt = getopt( argc, argv, ":i:n:a:w:rhv" )) != -1 ) {
    switch ( opt ) {

      /* set card number */
    case 'i':
      params->card = strtol( optarg, 0, 0 );
      if ( params->card < 0 || params->card > MAX_INDEX )
	result = EINVALCRD;
      break;
      
    case 'n':
      params->bar = strtol( optarg, 0, 0 );
      if ( params->bar < 0 || params->bar > 5 )
	result = EINVALBAR;
      break;


      /* upload the patch mapping */
    case 'a':
      params->address = strtol( optarg, 0, 0 );
      if ( params->address < 0 || params->address > 0xFFFFFF )
	result = EINVALADDR;
      break;

    case 'r':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = READ_ACCESS;
      break;

    case 'w':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = WRITE_ACCESS;
      params->writeData = strtol( optarg, 0, 0 );
      break;

    case 'h':
      usage();
      exit( 0 );

    case 'v':
     printf("Version %s\n", VERSION);
     exit( 0 );
    };

    /* check for error */
    if ( result != SUCCESS ) return result;
  }

  /* check if command was given */
  if ( params->command == NONE ) result = ENOCMD;

 
  return result;
}



/* main */
int main( int argc, char * argv[] )
{
  struct t_params params;

  char regionpath[MAX_DEVICE_PATH];
  int result;
  u_int32_t readData;
  tRegion region;
  tRegionStatus info;



  /* parse command line arguments */
  result = parse_args( argc, argv, & params );
  if ( result != SUCCESS ) {
    fprintf( stderr, "Error while parsing command line options: %s.\n\n",
	     get_error( result ) );
    usage();
    return result;
  }

  /* print greeting */
  // system("clear");
  
  //printf( "H-RORC access, Version %s\n\n", VERSION );


  /* open card */
  sprintf( regionpath, "/dev/psi/vendor/0x%04hX/device/0x%04hX/%i/base%d",
	   VENDOR_ID, DEVICE_ID, params.card, params.bar );
  result = PSI_openRegion( & region, regionpath );
  if ( result != PSI_OK ) {
    fprintf( stderr, "Failed to open device: %s.\n", PSI_strerror( result ) );
    return EACCES;
  }


  /* print device info */
  result = PSI_getRegionStatus( region, & info );
  if ( result != PSI_OK ) {
    fprintf( stderr, "Failed to get device info: %s.\n", 
	     PSI_strerror( result ) );
    PSI_closeRegion( region );
    return EIO;
  }
  if ( 0 ){
  printf( "- Using card %d BAR %d (%02x:%02x.%x)\n", params.card, params.bar, 
	  info.bus, PCI_SLOT( info.devfn ), PCI_FUNC( info.devfn ) );
  };



  /* select mode of operation */
  switch ( params.command ) {

  case READ_ACCESS :
    result = PSI_read( region, params.address, 4, 4, &readData );
    printf("0x%x\n", readData);
    break;

  case WRITE_ACCESS :
    // printf("Write access\n");
    result = PSI_write( region,  params.address, 4, 4, & params.writeData );
    break;

  default:
    fprintf( stderr, "Command not implemented.\n" );
    result = ENOSYS;
  }
  
  PSI_closeRegion( region );

  return result;
}
