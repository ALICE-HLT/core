#!/bin/bash

export HLTECS_TOPDIR="${HLTBASEDIR}/interfaces/ecs-proxy/${ECSVERSION}-${DATA_TRANSPORTVERSION}"
export HLT_PROXY_DIR="$HLTECS_TOPDIR"
export HLTECS_LIBDIR="${HLTECS_TOPDIR}/lib"
export TASK_MANAGER_LIBS="${ALIHLT_DC_DIR}/lib/Linux-x86_64-releasedebug"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${HLTECS_LIBDIR}"

check_ecs(){
    check_binary $HLTECS_TOPDIR/bin/hlt_SM_proxy && \
	check_binary $HLTECS_TOPDIR/bin/ecs_gui
}

checkout_ecs() {
    test -d ${HLTECS_TOPDIR} && echo "ecs-proxy-${ECSVERSION}-${DATA_TRANSPORTVERSION} already checked out." && \
	return 0
    mkdir -p ${HLTBASEDIR}/interfaces/ecs-proxy
    pushd ${HLTBASEDIR}/interfaces/ecs-proxy

    svn co https://svn.cern.ch/reps/hlt-alice/interfaces/ecs-proxy/tags/release-${ECSVERSION} $HLTECS_TOPDIR
    cd $HLTECS_TOPDIR
    mkdir ./bin
    mkdir ./lib
    ln -s ${TASK_MANAGER_LIBS}/SlaveControlInterfaceLib.so lib/libSlaveControlInterface.so
    ln -s ${TASK_MANAGER_LIBS}/TMLib.so lib/libTM.so

    popd
    test -d ${HLTECS_TOPDIR} 
}

compile_ecs() {
    check_ecs && echo "ecs-proxy-${ECSVERSION}-${DATA_TRANSPORTVERSION} already compiled." && \
	return 0
    pushd $HLTECS_TOPDIR
    make
    make gui
    popd
    check_ecs || return 1
}


module_ecs(){
    MODULE_FILE="${HLTBASEDIR}/modules/ECSPROXY/${ECSVERSION}-${DATA_TRANSPORTVERSION}"
    test -f ${MODULE_FILE} && \
	echo "ecs-proxy-${ECSVERSION}-${DATA_TRANSPORTVERSION} already has a module file." && \
	return 0
    mkdir -p "${HLTBASEDIR}/modules/ECSPROXY"
    cat <<EOF > ${MODULE_FILE}
#%Module1.0#####################################################################
##
## HLT - versions modulefile
##

proc ModulesHelp { } {
        global version
        puts stderr "This module is a module of the HLT for the ECS proxy."
}

set     version         1.2

module-whatis   "ECS proxy versions module for the HLT "

####################################################

set             nodename             [uname nodename]

####################################################

## -- ECS PROXY --
setenv          HLTECS_TOPDIR        \$::env(HLTECS_BASEDIR)/${ECSVERSION}-${DATA_TRANSPORTVERSION}
setenv          HLT_PROXY_DIR        \$::env(HLTECS_TOPDIR)

setenv          HLTECS_LIBDIR        \$::env(HLTECS_TOPDIR)/lib
setenv          TASK_MANAGER_LIBS    \$::env(ALIHLT_DC_DIR)/lib/Linux-x86_64-releasedebug

append-path     LD_LIBRARY_PATH      \$::env(HLTECS_LIBDIR)

setenv  HLTECS_MODULE        HLT

EOF

}

get_package_name_ecs() {
    echo "ecs-proxy-${ECSVERSION}-${DATA_TRANSPORTVERSION}"
}

deb_ecs() {
    DEST="$PKGDIR"
    PREFIX="$(get_package_name_ecs)"

    test -f $DEST/${PREFIX}.deb && \
    echo "Packages for $PREFIX already exist." && \
	return 0

    rm -rf $DEST/${PREFIX}
    mkdir -p $DEST/${PREFIX}

    echo "$HLTECS_TOPDIR" | path_cp "$DEST/${PREFIX}"

    pushd "$DEST/${PREFIX}/$HLTECS_TOPDIR"

    find . -name '*.o' -exec rm -f '{}' \;
    find . -depth -name '.svn' -exec rm -rf '{}' \;
    popd

    MODULE_FILE="${HLTBASEDIR}/modules/ECSPROXY/${ECSVERSION}-${DATA_TRANSPORTVERSION}"
    echo "$MODULE_FILE" | path_cp $DEST/$PREFIX

    pushd $DEST

    create_deb $PREFIX "$(get_package_deps ecs)" \
	"HLT ECS proxy" "HLT ECS proxy"

    popd

}
