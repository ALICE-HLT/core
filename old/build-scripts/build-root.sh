#!/bin/bash


export ROOTSYS="${HLTBASEDIR}/root/root_${ROOTVERSION}"
export PATH="${PATH}:${ROOTSYS}/bin"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ROOTSYS}/lib"
export ROOT_TARGET="linuxx8664gcc"



check_root(){
    check_binary ${ROOTSYS}/bin/root
}

checkout_root() {
    test -d $ROOTSYS && echo "root_${ROOTVERSION} already checked out." && \
	return 0
    mkdir -p ${HLTBASEDIR}/root
    svn co https://root.cern.ch/svn/root/tags/${ROOTVERSION} $ROOTSYS
    pushd $ROOTSYS

    test -f "$(dirname $0)/patches/root_${ROOTVERSION}.sh" && \
        source "$(dirname $0)/patches/root_${ROOTVERSION}.sh"
    patch_root || die "patching root failed."

    popd

}

compile_root() {
    check_root && echo "root_${ROOTVERSION} already compiled." && \
	return 0
    pushd $ROOTSYS
    sed -i -e 's/^OPTFLAGS.*/OPTFLAGS      = -O2 -g/' config/Makefile.linuxx8664gcc
    ./configure ${ROOT_TARGET} --enable-mathmore --enable-roofit --enable-minuit2 \
	--enable-table --with-alien-incdir=${ALIENDIR}/api/include \
	--with-alien-libdir=${ALIENDIR}/api/lib --with-f77=gfortran \
	--enable-globus --with-pythia6-uscore=SINGLE --with-xrootd=${ALIENDIR}/api \
	--enable-explicitlink --enable-qt --enable-qtgsi --enable-opengl \
	--with-monalisa-libdir=${ALIENDIR}/api/lib --with-monalisa-incdir=${ALIENDIR}/api/include || \
	die "root configure failed."
    make -j9 || make -j9 || make -j9 || make
    split_debug ${ROOTSYS}/bin ${ROOTSYS}/lib
    popd
    check_root || return 1
}


module_root(){
    MODULE_FILE="${HLTBASEDIR}/modules/ROOT/${ROOTVERSION}"
    test -f ${MODULE_FILE} && \
	echo "root_${ROOTVERSION} already has a module file." && \
	return 0
    mkdir -p "${HLTBASEDIR}/modules/ROOT"
    cat <<EOF > ${MODULE_FILE}
#%Module1.0#####################################################################
##
## HLT - ROOT modulefile
##

proc ModulesHelp { } {
        global version
        puts stderr "This module is a module of the HLT for ROOT."
}

set     version         1.2

module-whatis   "ROOT versions module for the HLT "

## -- VERSION --
setenv          ROOT_RELEASE    ${ROOTVERSION}

setenv          ROOTSYS         \$::env(ROOT_BASEDIR)/root_\$::env(ROOT_RELEASE)

append-path     PATH            \$::env(ROOTSYS)/bin
prepend-path    LD_LIBRARY_PATH \$::env(ROOTSYS)/lib

EOF
}


get_package_name_root() {
    echo "root-${ROOTVERSION}-minimal"
}

deb_root() {
    DEST="$PKGDIR"
    PREFIX="$(get_package_name_root)"
    PREFIX=${PREFIX%%-minimal}

    test -f $DEST/${PREFIX}-full.deb && \
    test -f $DEST/${PREFIX}-minimal.deb && \
    test -f $DEST/${PREFIX}-debug.deb && \
    test -f $DEST/${PREFIX}-src.deb && \
    echo "Packages for $PREFIX already exist." && \
	return 0
    rm -rf $DEST/${PREFIX}-{full,minimal,debug,src}

    mkdir -p $DEST/$PREFIX-full/$(dirname $ROOTSYS)
    cp -a $ROOTSYS $DEST/$PREFIX-full/$(dirname $ROOTSYS)

    pushd $DEST/$PREFIX-full
    #clean unneccesary stuff (svn dirs)
    find .  -depth -path '*/.svn' -exec rm -rf '{}' \; \
	-o -name '*.o' -exec rm -rf '{}' \; \
	-o -name '*.d' -exec rm -rf '{}' \;

    #debug symbols
    mkdir -p $DEST/$PREFIX-debug
    find . -depth -path "*/.debug" | path_mv $DEST/$PREFIX-debug

    #minimal
    mkdir -p $DEST/$PREFIX-minimal
    echo "./$ROOTSYS/lib" | path_mv $DEST/$PREFIX-minimal
    echo "./$ROOTSYS/bin" | path_mv $DEST/$PREFIX-minimal
    echo "./$ROOTSYS/include" | path_mv $DEST/$PREFIX-minimal
    echo "./$ROOTSYS/etc" | path_mv $DEST/$PREFIX-minimal
    echo "./$ROOTSYS/cint" | path_mv $DEST/$PREFIX-minimal
    
    MODULE_FILE="${HLTBASEDIR}/modules/ROOT/${ROOTVERSION}"
    mkdir -p $DEST/$PREFIX-minimal/$(dirname ${MODULE_FILE})
    cp -a "$MODULE_FILE" $DEST/$PREFIX-minimal/$(dirname ${MODULE_FILE})



    #sources and headers
    mkdir -p $DEST/$PREFIX-src
    find . -name '*.cxx' -o -name '*.F' \
	-o -name '*.c' -o -name '*.f' \
	-o -name '*.h' -o -name '*.hh' -o -name '*.hxx' | \
	path_mv $DEST/$PREFIX-src   

    pushd $DEST

    
    create_deb $PREFIX-minimal "libgfortran3, $(get_package_deps root)" \
	"Root minimal" "Root minimal installation, for HLT online use." "2"

    create_deb $PREFIX-debug "$PREFIX-minimal" \
	"Root debug symbols" "Root debug symbols." "2"

    create_deb $PREFIX-src "$PREFIX-minimal" \
	"Root sources" "Root source and header files" "2"

    create_deb $PREFIX-full "gfortran, libx11-6, libxpm4, libxft2, libxext6, libssl0.9.8, libglew1.5, libglu1-mesa, libfftw3-3, libmysqlclient16, libldap-2.4-2, python, libxml2, libqtgui4, libqtcore4, libqt4-qt3support, libqt4-opengl, $PREFIX-minimal, $PREFIX-debug, $PREFIX-src" \
	"Root full version" "Root full installation." "2"


    popd
    popd

}
