patch_aliroot() {

echo "Patching aliroot to patch level 6:"
 
# dEdx patches
svn merge -c 52047 https://alisoft.cern.ch/AliRoot/trunk || die

# "OriginalData" bit patch for GlobalTrigger
svn merge -c 52046 https://alisoft.cern.ch/AliRoot/trunk || die

#TRD workaround for corrupt raw data 
cat <<EOF | patch -p0 || die
Index: STEER/ESD/AliESDTrdTrack.cxx
===================================================================
--- STEER/ESD/AliESDTrdTrack.cxx        (revision 51975)
+++ STEER/ESD/AliESDTrdTrack.cxx        (working copy)
@@ -163,7 +163,7 @@
     Int_t layerMaskId = maskIdLut[this->GetLayerMask()];
     Int_t c1 = c1Lut[layerMaskId];
     Int_t c1Ext = c1 << 8;
-    Int_t ptRawStage4 = c1Ext / (this->GetA() >> 2);
+    Int_t ptRawStage4 = c1Ext / ((this->GetA() >> 2) != 0 ? (this->GetA() >> 2) : 1 );
     Int_t ptRawComb4 = ptRawStage4;
     Int_t ptExtComb4 = (ptRawComb4 > 0) ? ptRawComb4 + 33 : ptRawComb4 - 30;
EOF

#Improved TString handlingin HLT components:
cat <<EOF | patch -p0 || die
Index: HLT/global/AliHLTGlobalEsdConverterComponent.cxx
===================================================================
--- HLT/global/AliHLTGlobalEsdConverterComponent.cxx    (revision 51940)
+++ HLT/global/AliHLTGlobalEsdConverterComponent.cxx    (working copy)
@@ -255,7 +255,7 @@
	  TIter next(pTokens);
	  TObject* pObject=NULL;
	  while ((pObject=next())!=NULL) {
-	    id=((TObjString*)pObject)->GetString().Data();
+	    id=((TObjString*)pObject)->String().Data();
	    TObject* pObj=fESD->GetList()->FindObject(id);
	    if (pObj) {
	      HLTDebug("removing object %s", id);
Index: HLT/BASE/AliHLTHOMERSourceDesc.h
===================================================================
--- HLT/BASE/AliHLTHOMERSourceDesc.h	(revision 55720)
+++ HLT/BASE/AliHLTHOMERSourceDesc.h	(working copy)
@@ -97,7 +97,7 @@
   /** Get node name of this source 
    * @return           hostname
    */
-  TString GetHostname()       { return fHostname; }
+  TString& GetHostname()       { return fHostname; }
 
   /** Get node name of this source 
    * @return           port
@@ -107,12 +107,12 @@
   /** Get name of this source 
    * @return           name
    */
-  TString GetSourceName()     { return fSourceName; }
+  TString& GetSourceName()     { return fSourceName; }
 
   /** Get detector of this source 
    * @return           detector
    */
-  TString GetDetector()       { return fDetector; }
+  TString& GetDetector()       { return fDetector; }
 
   /** Get sub detector of this source 
    * @return           subdetector
@@ -127,7 +127,7 @@
   /** Get HLT data type of this source
    * @return           HLT data type
    */
-  TString GetDataType()       { return fDataType; }
+  TString& GetDataType()       { return fDataType; }
 
   /** Get HLT specification of this source
    * @return           HLT specification
EOF

#update of TPC classes already ported to release branch
svn merge -c 52052 https://alisoft.cern.ch/AliRoot/branches/v5-01-Release || die

#update of BASE classes requested to be ported
svn merge -c 52104 https://alisoft.cern.ch/AliRoot/trunk || die


#update of TPC classes requested to be ported
for r in 52114 52116 52117 52118 ; do svn merge -c $r https://alisoft.cern.ch/AliRoot/trunk  || die; done

#Fix for increased number of HLTout DDLs, already ported to release branch 
svn merge -c 52072 https://alisoft.cern.ch/AliRoot/branches/v5-01-Release || die

#new TRD raw format: 
svn merge -c 52375 https://alisoft.cern.ch/AliRoot/trunk || die



#additional EMCAL patch to support 2 Modules per clusterizer: 
svn merge -c 52643 https://alisoft.cern.ch/AliRoot/trunk || die
svn merge -c 52644 https://alisoft.cern.ch/AliRoot/trunk || die

#adds a event modulo options to components (forwarding empty blocks; less complex than the framework approach): 
svn merge -c 52674 https://alisoft.cern.ch/AliRoot/trunk || die

#Bug fix for component event modulo: 
svn merge -c 52714 https://alisoft.cern.ch/AliRoot/trunk || die

#patch from Maththias which gives a slight performance improvement: 
cat <<EOF | patch -p0 || die
Index: TPC/AliTPCTransform.cxx
===================================================================
--- TPC/AliTPCTransform.cxx	(revision 52156)
+++ TPC/AliTPCTransform.cxx	(working copy)
@@ -359,7 +359,16 @@
   Double_t sign = 1.;
   Double_t zwidth    = param->GetZWidth()*driftCorr;
   Float_t xyzPad[3];
-  AliTPCROC::Instance()->GetPositionGlobal(sector, TMath::Nint(x[0]) ,TMath::Nint(x[1]), xyzPad);
+  AliTPCROC::Instance()->GetPositionLocal(sector, TMath::Nint(x[0]) ,TMath::Nint(x[1]), xyzPad);
+  {
+    // FIXME: if RotatedGlobal2Global uses a template type it can be used
+    // directly, now xyzPad is float and can not be used as argument
+    Double_t cos,sin;
+    GetCosAndSin(sector,cos,sin);
+    Float_t tmp=xyzPad[0];
+    xyzPad[0]= cos*tmp-sin*xyzPad[1];
+    xyzPad[1]=+sin*tmp+cos*xyzPad[1];
+  }
   if (AliTPCRecoParam:: GetUseTimeCalibration()) zwidth*=vdcorrectionTime*(1+xyzPad[1]*vdcorrectionTimeGY);
   Double_t padWidth  = 0;
   Double_t padLength = 0;
EOF

#Fix for tracker segfault on exit: 
cat <<EOF | patch -p0 || die
Index: HLT/TPCLib/tracking-ca/AliHLTTPCCATrackerFramework.cxx
===================================================================
--- HLT/TPCLib/tracking-ca/AliHLTTPCCATrackerFramework.cxx	(revision 55720)
+++ HLT/TPCLib/tracking-ca/AliHLTTPCCATrackerFramework.cxx	(working copy)
@@ -283,6 +283,7 @@
 	{
 		if (fGPUTracker)
 		{
+			ExitGPU();
 #ifdef R__WIN32
 			FARPROC destroyFunc = GetProcAddress(hGPULib, "AliHLTTPCCAGPUTrackerNVCCDestroy");
 #else
EOF

#temporary workaround for tracker problem with extremely high number of clusters (>250000): 
cat<<EOF | patch -p0 || die
Index: HLT/TPCLib/tracking-ca/AliHLTTPCCATrackerComponent.cxx
===================================================================
--- HLT/TPCLib/tracking-ca/AliHLTTPCCATrackerComponent.cxx	(revision 55720)
+++ HLT/TPCLib/tracking-ca/AliHLTTPCCATrackerComponent.cxx	(working copy)
@@ -634,8 +634,15 @@
 	  Logging( kHLTLogDebug, "HLT::TPCCATracker::DoEvent", "Reading hits",
 			   "Total %d hits to read for slice %d", nClustersTotal, slice );
 
+	  if (nClustersTotal > 150000)
+	  {
+		HLTWarning( "Too many clusters in tracker input: Slice %d, Number of Clusters %d, some clusters are droped", slice, nClustersTotal );
+		nClustersTotal = 150000;
+	  }
 
+
 	  clusterData[islice].StartReading( slice, nClustersTotal );
+	  int nSliceClust = 0;
 
 	  for ( std::vector<unsigned long>::iterator pIter = patchIndices.begin(); pIter != patchIndices.end(); pIter++ ) {
 		ndx = *pIter;
@@ -653,7 +660,11 @@
 		      HLTError( "Wrong TPC cluster with row number %d received", c->fPadRow );
 		      continue;
 		    }
-		    clusterData[islice].ReadCluster( c->fID, c->fPadRow, c->fX, c->fY, c->fZ, c->fCharge );
+		    if (nSliceClust < nClustersTotal)
+		    {
+			clusterData[islice].ReadCluster( c->fID, c->fPadRow, c->fX, c->fY, c->fZ, c->fCharge );
+			nSliceClust++;
+		    }
 		  }	      
 		} else 	       
 		if ( iter->fDataType == AliHLTTPCCADefinitions::fgkCompressedInputDataType){
@@ -686,8 +697,9 @@
 		      
 		      UInt_t cluId = AliHLTTPCSpacePointData::GetID( jslice, jpatch, nPatchClust );
 		      //cout<<"clu "<<i<<": "<<x<<" "<<y<<" "<<z<<" "<<cluId<<endl;
-		      if ( CAMath::Abs( z ) <= fClusterZCut ){
+		      if ( CAMath::Abs( z ) <= fClusterZCut && nSliceClust < nClustersTotal){
 			clusterData[islice].ReadCluster( cluId, jrow, x, y, z, 0 );
+			nSliceClust++;
 		      }
 		      nPatchClust++;		  
 		    }
@@ -729,7 +741,7 @@
   for (int islice = 0;islice < slicecount;islice++)
   {
     if( outputControl.fEndOfSpace ){
-      HLTWarning( "Output buffer size exceed, tracks are not stored" );
+      HLTWarning( "Output buffer size exceed (buffer size %d, current size %d), tracks are not stored", maxBufferSize, mySize );
       ret = -ENOSPC;
       error = 1;
       break;     
@@ -747,7 +759,7 @@
       }
     else
       {
-	HLTWarning( "Output buffer size exceed (buffer size %d, current size %d), tracks are not stored", maxBufferSize, mySize );
+	HLTWarning( "Error during Tracking, no tracks stored" );
 	mySize = 0;
 	ret = -ENOSPC;
 	ntracks = 0;
EOF

}


