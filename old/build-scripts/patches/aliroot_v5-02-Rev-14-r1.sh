patch_aliroot() {

echo "Patching aliroot to patch level 1:"
 
# Transformation speedup
svn merge -c 57054 https://alisoft.cern.ch/AliRoot/trunk || die
svn merge -c 57200 https://alisoft.cern.ch/AliRoot/trunk || die


cat <<EOF | patch -p0 || die
Index: HLT/CMakelibHLTrec.pkg
===================================================================
--- HLT/CMakelibHLTrec.pkg      (revision 57272)
+++ HLT/CMakelibHLTrec.pkg      (working copy)
@@ -49,7 +49,7 @@

 set ( EINCLUDE  "HLT/rec HLT/BASE HLT/BASE/HOMER RAW STEER/STEER STEER/ESD STEER/STEERBase")

-set ( ELIBS  "HLTbase CDB ESD STEERBase STEER RAWDatarec RAWDatabase")
+set ( ELIBS  "HLTbase CDB ESD STEERBase STEER RAWDatarec RAWDatabase ANALYSIS")

 include ("CMakehlt.conf")

EOF
}
