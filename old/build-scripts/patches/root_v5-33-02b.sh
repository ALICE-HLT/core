patch_root() {

echo "Patching root to patch level 1:"


cat <<EOF | patch -p0 || die
Index: geom/geom/inc/LinkDef1.h
===================================================================
--- geom/geom/inc/LinkDef1.h	(revision 44150)
+++ geom/geom/inc/LinkDef1.h	(working copy)
@@ -86,4 +86,6 @@
 #pragma link C++ class TGeoNavigator+;
 #pragma link C++ class TGeoNavigatorArray;
 
+#pragma link C++ struct std::map<Long_t, TGeoNavigatorArray *>;
+#pragma link C++ struct std::pair<Long_t, TGeoNavigatorArray *>;
 #endif
EOF

}
