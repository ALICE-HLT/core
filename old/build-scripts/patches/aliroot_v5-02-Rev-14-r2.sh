patch_aliroot() {

echo "Patching aliroot to patch level 1:"
 
# Transformation speedup
for rev in 57054 57200 57437 57438; do
	svn merge -c $rev https://alisoft.cern.ch/AliRoot/trunk || die
done


cat <<EOF | patch -p0 || die
Index: HLT/CMakelibHLTrec.pkg
===================================================================
--- HLT/CMakelibHLTrec.pkg      (revision 57272)
+++ HLT/CMakelibHLTrec.pkg      (working copy)
@@ -49,7 +49,7 @@

 set ( EINCLUDE  "HLT/rec HLT/BASE HLT/BASE/HOMER RAW STEER/STEER STEER/ESD STEER/STEERBase")

-set ( ELIBS  "HLTbase CDB ESD STEERBase STEER RAWDatarec RAWDatabase")
+set ( ELIBS  "HLTbase CDB ESD STEERBase STEER RAWDatarec RAWDatabase ANALYSIS")

 include ("CMakehlt.conf")

EOF

cat <<EOF | patch -p0 || die
Index: HLT/trigger/AliHLTGlobalTriggerComponent.cxx
===================================================================
--- HLT/trigger/AliHLTGlobalTriggerComponent.cxx        (revision 57430)
+++ HLT/trigger/AliHLTGlobalTriggerComponent.cxx        (working copy)
@@ -1482,7 +1482,7 @@
     gSystem->SetIncludePath(includePath);
     gSystem->SetFlagsOpt("-O3 -DNDEBUG");
     gSystem->SetFlagsDebug("-g3 -DDEBUG -D__DEBUG");
-    
+    gSystem->SetLinkedLibs("-L$ROOTSYS/lib -lCore -lCint -lRint");
     int result = kTRUE;
     if (fDebugMode)
     {
EOF
}
