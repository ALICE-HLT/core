patch_aliroot() {

echo "Patching aliroot to patch level 1:"
 
# Transformation speedup
for rev in 57054 57200 57437 57438; do
	svn merge -c $rev https://alisoft.cern.ch/AliRoot/trunk || die
done


cat <<EOF | patch -p0 || die
Index: HLT/CMakelibHLTrec.pkg
===================================================================
--- HLT/CMakelibHLTrec.pkg      (revision 57272)
+++ HLT/CMakelibHLTrec.pkg      (working copy)
@@ -49,7 +49,7 @@

 set ( EINCLUDE  "HLT/rec HLT/BASE HLT/BASE/HOMER RAW STEER/STEER STEER/ESD STEER/STEERBase")

-set ( ELIBS  "HLTbase CDB ESD STEERBase STEER RAWDatarec RAWDatabase")
+set ( ELIBS  "HLTbase CDB ESD STEERBase STEER RAWDatarec RAWDatabase ANALYSIS")

 include ("CMakehlt.conf")

EOF

cat <<EOF | patch -p0 || die
Index: HLT/trigger/AliHLTGlobalTriggerComponent.cxx
===================================================================
--- HLT/trigger/AliHLTGlobalTriggerComponent.cxx        (revision 57430)
+++ HLT/trigger/AliHLTGlobalTriggerComponent.cxx        (working copy)
@@ -1482,7 +1482,7 @@
     gSystem->SetIncludePath(includePath);
     gSystem->SetFlagsOpt("-O3 -DNDEBUG");
     gSystem->SetFlagsDebug("-g3 -DDEBUG -D__DEBUG");
-    
+    gSystem->SetLinkedLibs("-L$ROOTSYS/lib -lCore -lCint -lRint");
     int result = kTRUE;
     if (fDebugMode)
     {
EOF


#Tracker speedup
svn merge -c 57538 https://alisoft.cern.ch/AliRoot/trunk || die

#Display fixes
cat <<EOF | patch -p0 || die
Index: EVE/EveHLT/AliEveHLTEventManager.cxx
===================================================================
--- EVE/EveHLT/AliEveHLTEventManager.cxx	(revision 57469)
+++ EVE/EveHLT/AliEveHLTEventManager.cxx	(working copy)
@@ -195,12 +195,23 @@
   gEve->DisableRedraw();
 
   AliHLTHOMERBlockDesc * block = NULL;
+  //First try to find TPC tracks
+  TIter nextIter(blockList);
+  while ((block = (AliHLTHOMERBlockDesc*)nextIter())) {
+    if( ! (block->GetDataType().CompareTo("HLTTRACK")) ) {
+      cout << "Found Tracks, setting up cluster filter." << endl;
+      ProcessBlock(block);
+    }
+  } 
+  //Then all the rest
   TIter next(blockList);
   while ((block = (AliHLTHOMERBlockDesc*)next())) {
     cout <<"Process Block"<<endl;
     ProcessBlock(block);
   } 
   
+  gEve->FullRedraw3D(0, 1);
+  gEve->EnableRedraw();
 
   return 0;
 
Index: EVE/hlt-macros/od.C
===================================================================
--- EVE/hlt-macros/od.C	(revision 57469)
+++ EVE/hlt-macros/od.C	(working copy)
@@ -225,7 +225,7 @@
   gGeomGentleTRD  = geom_gentle_trd();
 
   gGeoManager = fGeoManager;
-
+  /*
   gEMCALNode = gGeoManager->GetTopVolume()->FindNode("XEN1_1");
 
   TEveGeoTopNode* emcal_re = new TEveGeoTopNode(gGeoManager, gEMCALNode);
@@ -242,7 +242,7 @@
 
   gEve->AddGlobalElement(emcal_re);
   gEve->Redraw3D();
-
+  */
   if (gShowMUON) 
     gGeomGentleMUON = geom_gentle_muon(kFALSE);
   
Index: EVE/hlt-macros/geom_gentle_hlt.C
===================================================================
--- EVE/hlt-macros/geom_gentle_hlt.C	(revision 57469)
+++ EVE/hlt-macros/geom_gentle_hlt.C	(working copy)
@@ -33,7 +33,7 @@
   elHMPID->SetRnrState(kFALSE);
 
   TEveElement* elPHOS = gsre->FindChild("PHOS");
-  elPHOS->SetRnrState(kTRUE);
+  elPHOS->SetRnrState(kFALSE);
   elPHOS->FindChild("PHOS_4")->SetRnrState(kFALSE);
   elPHOS->FindChild("PHOS_5")->SetRnrState(kFALSE);
 
@@ -55,7 +55,7 @@
   f.Close();
 
   TEveElement* elPHOS = gsre->FindChild("PHOS");
-  elPHOS->SetRnrState(kTRUE);
+  elPHOS->SetRnrState(kFALSE);
   elPHOS->FindChild("PHOS_4")->SetRnrState(kFALSE);
   elPHOS->FindChild("PHOS_5")->SetRnrState(kFALSE);
 
@@ -95,7 +95,7 @@
       TEveGeoShape* lvl2 = (TEveGeoShape*) *j;
       
       if ( sm == 0 || sm == 1 || sm == 7 || sm == 8 || sm == 9 || sm == 10 || sm == 17 )
-	lvl2->SetRnrSelf(kTRUE);	
+	lvl2->SetRnrSelf(kFALSE);	
       else 
 	lvl2->SetRnrSelf(kFALSE);	
 
Index: HLT/EVE/AliHLTEveTPC.cxx
===================================================================
--- HLT/EVE/AliHLTEveTPC.cxx	(revision 57469)
+++ HLT/EVE/AliHLTEveTPC.cxx	(working copy)
@@ -29,6 +29,8 @@
 #include "TMath.h"
 #include "AliHLTTPCClusterDataFormat.h"
 #include "TH1F.h"
+#include "AliHLTTPCDefinitions.h"
+#include "AliHLTExternalTrackParam.h"
 
 ClassImp(AliHLTEveTPC)
 
@@ -36,6 +38,7 @@
   AliHLTEveBase("TPC Clusters"), 
   fEveClusters(NULL),
   fEveColClusters(NULL),
+  fEveColClustersBackground(NULL),
   fNColorBins(15), 
   fHistCharge(NULL), 
   fHistQMax(NULL), 
@@ -50,6 +53,9 @@
   if(fEveColClusters)
     delete fEveColClusters;
   fEveColClusters = NULL;
+  if(fEveColClustersBackground)
+    delete fEveColClustersBackground;
+  fEveColClustersBackground = NULL;
 
   if(fEveClusters)
     delete fEveClusters;
@@ -72,20 +78,27 @@
 
 void AliHLTEveTPC::ProcessBlock(AliHLTHOMERBlockDesc * block) {
   //See header file for documentation
+  if ( ! block->GetDataType().CompareTo("HLTTRACK") ) {
+    for(int i=0; i<216; ++i)
+      fTrackClusters[i].clear();
+    cout << "Processing TPC tracks" << endl;
+    ProcessTracks(block);
+  }
+  else if ( ! block->GetDataType().CompareTo("CLUSTERS") ) {
 
-  if ( ! block->GetDataType().CompareTo("CLUSTERS") ) {
-
     if(!fEveClusters){	  
       fEveClusters = CreatePointSet();
       //fEventManager->GetEveManager()->AddElement(fEveClusters);
     } 
     
     if(!fEveColClusters){
-      fEveColClusters = CreatePointSetArray();
+      fEveColClusters=CreatePointSetArray("TPC Clusters Colorized");
+      fEveColClustersBackground=CreatePointSetArray("TPC Clusters Colorized, Background");
       AddElement(fEveColClusters);
+      AddElement(fEveColClustersBackground);
     } 
-    
-    ProcessClusters(block, fEveClusters, fEveColClusters);
+    cout << "Processing TPC clusters" << endl;
+    ProcessClusters(block);
    
   }
   
@@ -117,13 +130,13 @@
 
 }
 
-TEvePointSetArray * AliHLTEveTPC::CreatePointSetArray(){
+TEvePointSetArray * AliHLTEveTPC::CreatePointSetArray(const char* name){
   //See header file for documentation
 
-  TEvePointSetArray * cc = new TEvePointSetArray("TPC Clusters Colorized");
+  TEvePointSetArray * cc = new TEvePointSetArray(name);
   cc->SetMainColor(kRed);
   cc->SetMarkerStyle(4); // antialiased circle
-  cc->SetMarkerSize(0.4);
+  cc->SetMarkerSize(0.6);
   cc->InitBins("Cluster Charge", fNColorBins, 0., fNColorBins*20.);
   
   const Int_t nCol = TColor::GetNumberOfColors();
@@ -164,18 +177,59 @@
       fEveColClusters->GetBin(ib)->Reset();
     }
   }
+  if(fEveColClustersBackground){
+    for (Int_t ib = 0; ib <= fNColorBins + 1; ++ib) {
+      fEveColClustersBackground->GetBin(ib)->Reset();
+    }
+  }
 
 }
 
-Int_t AliHLTEveTPC::ProcessClusters( AliHLTHOMERBlockDesc * block, TEvePointSet * cont, TEvePointSetArray * contCol ){
+Int_t AliHLTEveTPC::ProcessTracks( AliHLTHOMERBlockDesc * block ){
+  AliHLTTracksData* dataPtr = ( AliHLTTracksData* ) block->GetData();
+  int nTracks = dataPtr->fCount;
+  AliHLTExternalTrackParam* currTrack = dataPtr->fTracklets;
+  for(int i=0; i<nTracks; ++i){
+    for( UInt_t ic=0; ic < currTrack->fNPoints; ++ic){	    
+      UInt_t id = currTrack->fPointIDs[ic];
+      int iSlice = AliHLTTPCSpacePointData::GetSlice(id);
+      int iPatch = AliHLTTPCSpacePointData::GetPatch(id);
+      UInt_t iCluster = AliHLTTPCSpacePointData::GetNumber(id);
+      int slicepatch=iSlice*6 + iPatch;
+      if(slicepatch >= 216){
+	cerr << "Cluster out of bounds: slice " << iSlice << " patch " << iPatch << endl;
+      } else {
+	vector<bool>* currTrackClusters = &(fTrackClusters[slicepatch]);
+	//cout << iSlice << " " << iPatch << " " << iCluster << " " << currTrackClusters->size() << endl;
+	if(iCluster >= currTrackClusters->size()){
+	  //cout << "Resizing to size " << iCluster+1 << endl;
+	  currTrackClusters->resize(iCluster+1, false);
+	}
+	//cout << iSlice << " " << iPatch << " " << iCluster << " " << currTrackClusters->size() << endl;
+	currTrackClusters->at(iCluster)=true;
+      }
+    }
+    unsigned int step = sizeof( AliHLTExternalTrackParam ) + currTrack->fNPoints * sizeof( unsigned int );
+    currTrack = ( AliHLTExternalTrackParam* )( (( Byte_t * )currTrack) + step );  
+  }
+  
+  return 0;
+}
+
+Int_t AliHLTEveTPC::ProcessClusters( AliHLTHOMERBlockDesc * block ){ 
   //See header file for documentation
 
 
   // if(!fHistCharge) fHistCharge = new TH1F("ClusterCharge","ClusterCharge",100,0,500);
   // if(!fHistQMax) fHistQMax = new TH1F("QMax","QMax",50,0,250);
   // if(!fHistQMaxOverCharge) fHistQMaxOverCharge = new TH1F("QMaxOverCharge","QMaxOverCharge",50,0,1);
+  Int_t slicepatch = AliHLTTPCDefinitions::GetMinSliceNr(block->GetSpecification()) * 6 + AliHLTTPCDefinitions::GetMinPatchNr(block->GetSpecification());
+  if(slicepatch >= 216){
+    cerr << "Cluster out of bounds: slice * 6 + patch =  " << slicepatch << endl;
+    return 0;
+  }
+  vector<bool>* currTrackClusters = &(fTrackClusters[slicepatch]);
 
-
   Int_t   slice = block->GetSubDetector();
   Float_t phi   = ( slice + 0.5 ) * TMath::Pi() / 9.0;  
   Float_t cos   = TMath::Cos( phi );
@@ -187,11 +241,15 @@
   if ( cd->fSpacePointCnt != 0 ) {
     for (UInt_t iter = 0; iter < cd->fSpacePointCnt; ++iter, data += sizeof(AliHLTTPCSpacePointData)) {
       AliHLTTPCSpacePointData *sp = reinterpret_cast<AliHLTTPCSpacePointData*> (data);
-      cont->SetNextPoint(cos*sp->fX - sin*sp->fY, sin*sp->fX + cos*sp->fY, sp->fZ);
-      if (contCol)
-	contCol->Fill(cos*sp->fX - sin*sp->fY, sin*sp->fX + cos*sp->fY, sp->fZ, sp->fCharge);
+      if( iter < currTrackClusters->size() && currTrackClusters->at(iter)) {
+	fEveClusters->SetNextPoint(cos*sp->fX - sin*sp->fY, sin*sp->fX + cos*sp->fY, sp->fZ);
+	if (fEveColClusters)
+	  fEveColClusters->Fill(cos*sp->fX - sin*sp->fY, sin*sp->fX + cos*sp->fY, sp->fZ, sp->fCharge);
+      } else {
+	if (fEveColClustersBackground)
+	  fEveColClustersBackground->Fill(cos*sp->fX - sin*sp->fY, sin*sp->fX + cos*sp->fY, sp->fZ, sp->fCharge);
+      }
 
-
       // fHistCharge->Fill(sp->fCharge);
       // fHistQMax->Fill(sp->fQMax);
       // fHistQMaxOverCharge->Fill(((Float_t)sp->fQMax)/((Float_t)sp->fCharge));
@@ -199,9 +257,10 @@
   }
 
 
-  cont->ElementChanged();
+  fEveClusters->ElementChanged();
 
-  if(contCol) contCol->ElementChanged();
+  if(fEveColClusters) fEveColClusters->ElementChanged();
+  if(fEveColClustersBackground) fEveColClustersBackground->ElementChanged();
     
   return 0;  
 
Index: HLT/EVE/AliHLTEveHLT.cxx
===================================================================
--- HLT/EVE/AliHLTEveHLT.cxx	(revision 57469)
+++ HLT/EVE/AliHLTEveHLT.cxx	(working copy)
@@ -63,7 +63,8 @@
   fHistDCAr(NULL),
   fTrCount(0), 
   fVCount(0),
-  fNTrackBins(10)
+  fNTrackBins(10),
+  fBz(0)
 {
   // Constructor.
   CreateHistograms();
@@ -314,7 +315,8 @@
 ///_________________________________________________________________________________
 Color_t AliHLTEveHLT::GetColor(Float_t pt) {
   //See header file
-  Color_t baseColor = kOrange;
+  //Color_t baseColor = kOrange;
+  Color_t baseColor = kBlue;
   
   Float_t binlimit = 0.1;
   for(Int_t i = 0; i< 10; i++) {
@@ -453,7 +455,8 @@
 
 
   const double kCLight = 0.000299792458;
-  double bz = - kCLight*10.*( cont->GetPropagator()->GetMagField(0,0,0).fZ);
+  //double bz = - kCLight*10.*( cont->GetPropagator()->GetMagField(0,0,0).fZ);
+  double bz = - kCLight*10.* fBz;
 
   Bool_t innerTaken = kFALSE;
   if ( ! at->IsOn(AliESDtrack::kITSrefit) && fUseIpOnFailedITS)
@@ -614,10 +617,12 @@
   //See header file for documentation
 
   if (fTrueField) {
-    trkProp->SetMagFieldObj(new AliEveMagField);
-  
+    TEveMagField* field=new AliEveMagField;
+    trkProp->SetMagFieldObj(field);
+    fBz=field->GetFieldD(0,0,0)[2];
   } else {
     trkProp->SetMagField(magF);
+    fBz=magF;
   }
  
   if (fUseRkStepper) {
Index: HLT/EVE/AliHLTEveTPC.h
===================================================================
--- HLT/EVE/AliHLTEveTPC.h	(revision 57469)
+++ HLT/EVE/AliHLTEveTPC.h	(working copy)
@@ -11,6 +11,7 @@
 /// @brief  TPC Instance of Eve display processor
 
 #include "AliHLTEveBase.h"
+#include <vector>
 class TEvePointSetArray;
 class TEvePointSet;
 class TH1F;
@@ -43,16 +44,22 @@
   /** Create point set for clusters */
   TEvePointSet * CreatePointSet();
   /** Create point set array for colour coded clusters */
-  TEvePointSetArray * CreatePointSetArray();
+  TEvePointSetArray * CreatePointSetArray(const char* name);
 
   /** Process clusters block */
-  Int_t ProcessClusters( AliHLTHOMERBlockDesc * block, TEvePointSet * cont, TEvePointSetArray * contCol );
+  Int_t ProcessClusters( AliHLTHOMERBlockDesc * block);
 
+  /** Process tracks **/
+  Int_t ProcessTracks( AliHLTHOMERBlockDesc * block );
+
   /** Draw the TPC histograms */
   void DrawHistograms();
 
   TEvePointSet * fEveClusters;          //Clusters pointset
-  TEvePointSetArray * fEveColClusters;  //Color coded clusters pointset
+  
+  TEvePointSetArray * fEveColClusters;  //Color coded clusters pointset, tracks
+  TEvePointSetArray * fEveColClustersBackground;  //Color coded clusters pointset, background
+  std::vector<bool> fTrackClusters[216]; //cluster to track association
   const Int_t fNColorBins;             //Number of colorbins for the colored clusters
 
   TH1F * fHistCharge;                  //Histo
Index: HLT/EVE/AliHLTEveHLT.h
===================================================================
--- HLT/EVE/AliHLTEveHLT.h	(revision 57469)
+++ HLT/EVE/AliHLTEveHLT.h	(working copy)
@@ -127,7 +127,7 @@
   Int_t fVCount;
   
   Int_t fNTrackBins;
-  
+  Double_t fBz;
   ClassDef(AliHLTEveHLT, 0);
 };
 
EOF
}
