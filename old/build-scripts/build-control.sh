#!/bin/bash





HLTCONTROL_TOPDIR="/opt/HLT/control/releases/${CONTROLVERSION}"

check_control(){
    test -d $HLTCONTROL_TOPDIR
}

create_control_base_layout() {

    test -d /opt/HLT/control/run/ALICE/config || \
	mkdir -p /opt/HLT/control/run/ALICE/config

    test -d /opt/HLT/control/hlt-configuration || \
	svn co https://svn.cern.ch/reps/hlt-alice/control/trunk/hlt-configuration \
	/opt/HLT/control/hlt-configuration

    test -d /opt/HLT/control/detector || \
	svn co https://svn.cern.ch/reps/hlt-alice/control/trunk/detector \
	/opt/HLT/control/detector

    test -L /opt/HLT/control/detector/run || \
	ln -s ../../run/ALICE/ /opt/HLT/control/detector/ALICE/run
}

checkout_control() {
    test -d ${HLTCONTROL_TOPDIR} && echo "control-${CONTROLVERSION} already checked out." && \
	return 0

    create_control_base_layout

    mkdir -p ${HLTBASEDIR}/control/releases
    pushd ${HLTBASEDIR}/control/releases

    svn co https://svn.cern.ch/reps/hlt-alice/control/trunk $HLTCONTROL_TOPDIR
    cd $HLTCONTROL_TOPDIR
    rm detector hlt-configuration -rf
    ln -s ../../run/ run

    popd
    test -d ${HLTECS_TOPDIR} 
}

compile_control() {
    check_control
}


module_control(){
    MODULE_FILE="${HLTBASEDIR}/modules/CONTROL/$CONTROLVERSION"
    test -f ${MODULE_FILE} && \
	echo "control-${CONTROLVERSION} already has a module file." && \
	return 0
    mkdir -p "${HLTBASEDIR}/modules/CONTROL"
    cat <<EOF > ${MODULE_FILE}
#%Module1.0#####################################################################
##
## HLT - versions modulefile
##

proc ModulesHelp { } {
        global version
        puts stderr "This module is a module of the HLT for the CONTROL."
}

set     version         1.4

module-whatis   "CONTROL versions module for the HLT "

####################################################

## -- CONTROL --
setenv          HLTCONTROL_TOPDIR          \$::env(HLTCONTROL_BASEDIR)/$CONTROLVERSION
setenv          HLTCONTROL_SETUPDIR        \$::env(HLTCONTROL_TOPDIR)/setup
append-path     PATH                       \$::env(HLTCONTROL_TOPDIR)/bin/operator
EOF

}

get_package_name_control() {
    echo "$(echo control-${CONTROLVERSION} | tr '[A-Z]' '[a-z]' | tr '_' '-')"
}

make_control_base_deb() {
    test -f $DEST/control-base.deb && return 0

    rm -rf $DEST/control-base
    mkdir -p $DEST/control-base/opt/HLT/control
    for dir in detector hlt-configuration run; do
	cp -a /opt/HLT/control/$dir $DEST/control-base/opt/HLT/control/
    done

    pushd $DEST
    mkdir -p control-base/DEBIAN
    cat <<EOF >control-base/DEBIAN/postinst
#!/bin/bash
   chown -R 999:999 /opt/HLT/control/{detector,hlt-configuration,run}
EOF
    chmod +x control-base/DEBIAN/postinst
    create_deb control-base "" \
	"HLT control base layout" "HLT control base layout" 

    popd
}

deb_control() {
    DEST="$PKGDIR"
    PREFIX="$(get_package_name_control)"

    test -f $DEST/${PREFIX}.deb && \
    test -f $DEST/control-base.deb && \
    echo "Packages for $PREFIX already exist." && \
	return 0

    make_control_base_deb || die

    rm -rf $DEST/${PREFIX}
    mkdir -p $DEST/${PREFIX}/$(dirname $HLTCONTROL_TOPDIR)
    cp -a $HLTCONTROL_TOPDIR $DEST/${PREFIX}/$(dirname $HLTCONTROL_TOPDIR)


    MODULE_FILE="${HLTBASEDIR}/modules/CONTROL/$CONTROLVERSION"
    echo "$MODULE_FILE" | path_cp $DEST/$PREFIX

    pushd $DEST

    create_deb $PREFIX "control-base" \
	"HLT control" "HLT control" "1.1"

    popd

}
