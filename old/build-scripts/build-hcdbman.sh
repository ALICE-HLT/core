#!/bin/bash

export HLTHCDBMANAGER_TOPDIR="${HLTBASEDIR}/interfaces/HCDBManager/$HCDBMANVERSION-$ALIROOTVERSION"


check_hcdbman(){
    check_binary ${HLTHCDBMANAGER_TOPDIR}/libAliHLTHCDBConfigManagerLib.so
}

checkout_hcdbman() {
    test -d ${HLTHCDBMANAGER_TOPDIR} && echo "hcdbmanager-$HCDBMANVERSION-$ALIROOTVERSION already checked out." && \
	return 0

    mkdir -p ${HLTBASEDIR}/interfaces/HCDBManager

    svn co https://svn.cern.ch/reps/hlt-alice/interfaces/HCDBManager/tags/release-$HCDBMANVERSION \
	${HLTHCDBMANAGER_TOPDIR} || die
}

compile_hcdbman() {
    check_hcdbman && echo "hcdbmanager-$HCDBMANVERSION-$ALIROOTVERSION already compiled." && \
	return 0 
    pushd ${HLTHCDBMANAGER_TOPDIR}
    make
    popd
    check_hcdbman
}


module_hcdbman(){
    MODULE_FILE="${HLTBASEDIR}/modules/HCDBMANAGER/$HCDBMANVERSION-$ALIROOTVERSION"
    test -f ${MODULE_FILE} && \
	echo "hcdbmanager-$HCDBMANVERSION-$ALIROOTVERSION already has a module file." && \
	return 0
    mkdir -p "${HLTBASEDIR}/modules/HCDBMANAGER"
    cat <<EOF > ${MODULE_FILE}
#%Module1.0#####################################################################
##
## HLT - versions modulefile
##

proc ModulesHelp { } {
        global version
        puts stderr "This module is a module of the HLT for the HCDBMANAGER."
}

set     version         1.4

module-whatis   "HCDBManager versions module for the HLT "

####################################################

## -- HCDBManager --
setenv          HLTHCDBMANAGER_TOPDIR  \$::env(HLTHCDBMANAGER_BASEDIR)/$HCDBMANVERSION-$ALIROOTVERSION
append-path     PATH                   \$::env(HLTHCDBMANAGER_TOPDIR)
EOF

}

get_package_name_hcdbman() {
    echo "$(echo hcdbmanager-$HCDBMANVERSION-$ALIROOTVERSION | tr '[A-Z]' '[a-z]')"
}

deb_hcdbman() {
    DEST="$PKGDIR"
    PREFIX="$(get_package_name_hcdbman)"

    test -f $DEST/${PREFIX}.deb && \
    echo "Packages for $PREFIX already exist." && \
	return 0

    rm -rf $DEST/${PREFIX}
    mkdir -p $DEST/${PREFIX}

    echo "$HLTHCDBMANAGER_TOPDIR" | path_cp "$DEST/${PREFIX}"

    MODULE_FILE="${HLTBASEDIR}/modules/HCDBMANAGER/$HCDBMANVERSION-$ALIROOTVERSION"
    echo "$MODULE_FILE" | path_cp $DEST/$PREFIX

    pushd $DEST

    create_deb $PREFIX "" \
	"HCDBManager" "HCDBManager"

    popd

}
