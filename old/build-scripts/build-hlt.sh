#!/bin/bash


check_hlt(){
    true
}

checkout_hlt() {
    true
}

compile_hlt() {
    check_hlt
}

module_hlt(){
    MODULE_FILE="${HLTBASEDIR}/modules/HLT/${ALIROOTVERSION}-prod"
    test -f ${MODULE_FILE} && \
	echo "HLT-${ALIROOTVERSION}-prod already has a module file." && \
	return 0
    mkdir -p "${HLTBASEDIR}/modules/HLT"
    cat <<EOF > ${MODULE_FILE}
#%Module1.0#####################################################################
##
## HLT - versions modulefile
##

proc ModulesHelp { } {
        global version
	puts stderr "This module is THE module of the HLT."
}

set 	version 	1.3

module-whatis	"HLT versions module for the HLT "

####################################################
set 	hlt_build_type		prod
set	hlt_version		${ALIROOTVERSION}

set	base_version		base
set     definition_version      base
set     alien_version           $ALIENVERSION
set	aliroot_version		${ALIROOTVERSION}
set     geant_version           $GEANT3VERSION
set	root_version		$ROOTVERSION
#set	fastjet_version		2.4.4
set	data_transport_version	$DATA_TRANSPORTVERSION
set     ecsproxy_version        $ECSVERSION
set     dim_version             $DIMVERSION
set     smi_version             $SMIVERSION
set     taxi_version            $TAXIVERSION
set     pendolino_version       $PENDOLINOVERSION
set     hcdbmanager_version     $HCDBMANVERSION
set     control_version         $CONTROLVERSION
####################################################

## set type to "prod" or "debug"
## -------------------------------------
setenv  HLTBUILDTYPE	\$hlt_build_type
## -------------------------------------

set	osname		[uname sysname]
set 	osarchitecture	[uname machine]

if { \$osarchitecture == "x86_64" } {
 	setenv	ALICE_TARGET	linuxx8664gcc
        	
	if { \$hlt_build_type == "prod" } {
		setenv	HLT_TARGET	   \$osname-\$osarchitecture-releasedebug
                setenv	ALICE_TARGET_EXT   linuxx8664gcc
 	} else {
		setenv	HLT_TARGET	   \$osname-\$osarchitecture-\$::env(HLTBUILDTYPE)
                setenv	ALICE_TARGET_EXT   linuxx8664gccDEBUG
	}
} elseif { \$osarchitecture == "i686" } {
 	setenv	ALICE_TARGET	linux
        	
	if { \$hlt_build_type == "prod" } {
		setenv	HLT_TARGET	   \$osname-\$osarchitecture-releasedebug
                setenv	ALICE_TARGET_EXT   linux
 	} else {
		setenv	HLT_TARGET	   \$osname-\$osarchitecture-\$::env(HLTBUILDTYPE)
                setenv	ALICE_TARGET_EXT   linuxDEBUG
	}
} else {
	puts stderr "\tYour architecture is \$osarchitecture. Please use x86_64 or i686 architecture"
	break
}

if {[info exists env(MODULE_INFO_SUPRESS)]} {	
	set supress	1
} else {	
 	set supress	0
}

## Actions on load / unload
## ------------------------
if { [module-info mode load] } {
        if { \$supress == "0" } { 
   	    puts stderr "\tHLT version loaded: (GLOBAL)\t\$hlt_version-\$::env(HLTBUILDTYPE)"
	    puts stderr "\t\t      AliRoot version:\t\$aliroot_version"
            puts stderr "\t\t        Alien version:\t\$alien_version"
            puts stderr "\t\t         Root version:\t\$root_version"
            puts stderr "\t\t       Geant3 version:\t\$geant_version"
            #puts stderr "\t\t      FastJet version:\t\$fastjet_version"
            puts stderr "\t\tDataTransport version:\t\$data_transport_version"
            puts stderr "\t\t    ECS proxy version:\t\$ecsproxy_version"
            puts stderr "\t\t          DIM version:\t\$dim_version"
            puts stderr "\t\t          SMI version:\t\$smi_version"
            puts stderr "\t\t         Taxi version:\t\$taxi_version"
            puts stderr "\t\t    Pendolino version:\t\$pendolino_version"
            puts stderr "\t\t  HCDBManager version:\t\$hcdbmanager_version"
            puts stderr "\t\t      Control version:\t\$control_version"
        }

	module load BASE/\$base_version
	module load DEFINITION/\$definition_version
	module load ALIEN/\$alien_version
	#module load FASTJET/$fastjet_version
	module load ALIROOT/\$aliroot_version
	module load ROOT/\$root_version
	module load GEANT3/\$geant_version
	module load DATATRANSPORT/\$data_transport_version
        module load TAXI/\$taxi_version-\$hlt_version
        module load ECSPROXY/\$ecsproxy_version-\$data_transport_version
        module load DIM/\$dim_version
        module load SMI/\$smi_version
        module load PENDOLINO/\$pendolino_version
        module load HCDBMANAGER/\$hcdbmanager_version-\$hlt_version
        module load CONTROL/\$control_version

} elseif { [module-info mode remove] } {
        if { \$supress == "0" } { 
	    puts stderr "\tHLT version unloaded: (GLOBAL)\t\$hlt_version-\$::env(HLTBUILDTYPE)" 
        }

        if { [is-loaded ECSPROXY] } { module unload ECSPROXY }
        if { [is-loaded DIM] } { module unload DIM }
        if { [is-loaded SMI] } { module unload SMI }
        if { [is-loaded TAXI] } { module unload TAXI }
        if { [is-loaded PENDOLINO] } { module unload PENDOLINO }
        if { [is-loaded HCDBMANAGER] } { module unload HCDBMANAGER }
        if { [is-loaded CONTROL] } { module unload CONTROL }
	if { [is-loaded ALIROOT] } { module unload ALIROOT }
	if { [is-loaded ROOT] } { module unload ROOT }
	if { [is-loaded GEANT3] } { module unload GEANT3 }
	#if { [is-loaded FASTJET] } { module unload FASTJET }
	if { [is-loaded DATATRANSPORT] } { module unload DATATRANSPORT }
	if { [is-loaded ALIEN] } { module unload ALIEN }
	if { [is-loaded DEFINITION] } { module unload DEFINITION }
	if { [is-loaded BASE] } { module unload BASE }
}       
EOF

}

get_package_name_hlt() {
    echo "$(echo hlt-${ALIROOTVERSION}-prod | tr '[A-Z]' '[a-z]')-minimal"
}

deb_hlt() {
    DEST="$PKGDIR"
    PREFIX="$(get_package_name_hlt)"
    PREFIX=${PREFIX%%-minimal}

    test -f $DEST/${PREFIX}-minimal.deb && \
    test -f $DEST/${PREFIX}-full.deb && \
    echo "Packages for $PREFIX already exist." && \
	return 0

    rm -rf $DEST/${PREFIX}-minimal
    rm -rf $DEST/${PREFIX}-full
    mkdir -p $DEST/${PREFIX}-minimal
    mkdir -p $DEST/${PREFIX}-full

    MODULE_FILE="${HLTBASEDIR}/modules/HLT/${ALIROOTVERSION}-prod"

    echo "${MODULE_FILE}" | path_cp $DEST/$PREFIX-minimal
    pushd $DEST

    create_deb $PREFIX-minimal "$(get_package_deps hlt)" \
	"HLT minimal" "HLT minimal"

    create_deb $PREFIX-full " $PREFIX-minimal, $(get_package_deps hlt full)" \
	"HLT full" "HLT full"

    popd

}
