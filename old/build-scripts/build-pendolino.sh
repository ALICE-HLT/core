#!/bin/bash

export HLTPENDOLINO_TOPDIR="${HLTBASEDIR}/interfaces/pendolino/$PENDOLINOVERSION"


check_pendolino(){
    test -d $HLTPENDOLINO_TOPDIR
}

checkout_pendolino() {
    test -d ${HLTPENDOLINO_TOPDIR} && echo "pendolino-$PENDOLINOVERSION already checked out." && \
	return 0

    mkdir -p ${HLTBASEDIR}/interfaces/pendolino

    svn co https://svn.cern.ch/reps/hlt-alice/interfaces/pendolino/tags/release-$PENDOLINOVERSION \
	${HLTPENDOLINO_TOPDIR} || die

}

compile_pendolino() {
    check_pendolino
}


module_pendolino(){
    MODULE_FILE="${HLTBASEDIR}/modules/PENDOLINO/$PENDOLINOVERSION"
    test -f ${MODULE_FILE} && \
	echo "pendolino-$PENDOLINOVERSION already has a module file." && \
	return 0
    mkdir -p "${HLTBASEDIR}/modules/PENDOLINO"
    cat <<EOF > ${MODULE_FILE}
#%Module1.0#####################################################################
##
## HLT - versions modulefile
##

proc ModulesHelp { } {
        global version
        puts stderr "This module is a module of the HLT for the PENDOLINO."
}

set     version         1.4

module-whatis   "PENDOLINO versions module for the HLT "

####################################################

## -- PENDOLINO --
setenv          HLTPENDOLINO_TOPDIR    \$::env(HLTPENDOLINO_BASEDIR)/$PENDOLINOVERSION
append-path     PATH                   \$::env(HLTPENDOLINO_TOPDIR)/bin
EOF

}

get_package_name_pendolino() {
    echo "pendolino-$PENDOLINOVERSION"
}

deb_pendolino() {
    DEST="$PKGDIR"
    PREFIX="$(get_package_name_pendolino)"

    test -f $DEST/${PREFIX}.deb && \
    echo "Packages for $PREFIX already exist." && \
	return 0

    rm -rf $DEST/${PREFIX}
    mkdir -p $DEST/${PREFIX}

    echo "$HLTPENDOLINO_TOPDIR" | path_cp "$DEST/${PREFIX}"

    MODULE_FILE="${HLTBASEDIR}/modules/PENDOLINO/$PENDOLINOVERSION"
    echo "$MODULE_FILE" | path_cp $DEST/$PREFIX

    pushd $DEST

    create_deb $PREFIX "" \
	"Pendolino" "Pendolino"

    popd

}
