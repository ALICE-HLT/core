#!/bin/bash


export OS="Linux"
export DIMDIR="${HLTBASEDIR}/interfaces/dim/dim_${DIMVERSION}"
export ODIR="linux"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${DIMDIR}/${ODIR}"



check_dim(){
    check_binary ${DIMDIR}/${ODIR}/libdim.so
}

checkout_dim() {
    test -d ${DIMDIR} && echo "dim_${DIMVERSION} already checked out." && \
	return 0
    mkdir -p ${HLTBASEDIR}/interfaces/dim
    pushd ${HLTBASEDIR}/interfaces/dim
    wget http://dim.web.cern.ch/dim/dim_${DIMVERSION}.zip
    unzip dim_${DIMVERSION}.zip
    rm -f dim_${DIMVERSION}.zip
    popd
    test -d ${DIMDIR} 
}

compile_dim() {
    check_dim && echo "dim_${DIMVERSION} already compiled." && \
	return 0
    pushd $DIMDIR
    alias CC=gcc 
    make
    popd
    check_dim || return 1
}


module_dim(){
    MODULE_FILE="${HLTBASEDIR}/modules/DIM/${DIMVERSION}"
    test -f ${MODULE_FILE} && \
	echo "dim_${DIMVERSION} already has a module file." && \
	return 0
    mkdir -p "${HLTBASEDIR}/modules/DIM"
    cat <<EOF > ${MODULE_FILE}
#%Module1.0#####################################################################
##
## HLT - DIM modulefile
##

proc ModulesHelp { } {
        global version
        puts stderr "This module is a module of the HLT for DIM."
}

set     version         1.2

module-whatis   "DIM versions module for the HLT "

setenv          OS                   $OS
setenv          DIMDIR               $DIMDIR
setenv          ODIR                 $ODIR
append-path     LD_LIBRARY_PATH      \$::env(DIMDIR)/\$::env(ODIR)
EOF

}

get_package_name_dim() {
    echo "dim-${DIMVERSION}-minimal"
}

deb_dim() {
    DEST="$PKGDIR"
    PREFIX="$(get_package_name_dim)"
    PREFIX=${PREFIX%%-minimal}

    test -f $DEST/${PREFIX}-full.deb && \
    test -f $DEST/${PREFIX}-minimal.deb && \
    echo "Packages for $PREFIX already exist." && \
	return 0

    rm -rf $DEST/${PREFIX}-full
    rm -rf $DEST/${PREFIX}-minimal

    echo "$DIMDIR" | path_cp "$DEST/${PREFIX}-minimal"

    pushd "$DEST/${PREFIX}-minimal/$DIMDIR"
    #remove Windows stuff
    rm -rf ./bin ./Visual

    find . -name '*.o' -exec rm -f '{}' \;
    popd

    MODULE_FILE="${HLTBASEDIR}/modules/DIM/${DIMVERSION}"
    echo "$MODULE_FILE" | path_cp $DEST/$PREFIX-minimal

    #dummy package, pulls in some more deps
    mkdir $DEST/${PREFIX}-full

    pushd $DEST

    create_deb $PREFIX-minimal "" \
	"DIM minimal" "DIM minimal, for HLT online use."

    
    create_deb $PREFIX-full "lesstif2, ${PREFIX}-minimal" \
	"DIM full" "Full version of DIM."

    popd


}
