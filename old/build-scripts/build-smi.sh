#!/bin/bash


export SMIDIR="${HLTBASEDIR}/interfaces/smi/smixx_${SMIVERSION}"
export SMIRTLDIR="${SMIDIR}"
export PATH="${PATH}:${SMIDIR}/${ODIR}"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${SMIDIR}/${ODIR}"



check_smi(){
    check_binary ${SMIDIR}/${ODIR}/libsmi.so
}

checkout_smi() {
    test -d ${SMIDIR} && echo "smi_${SMIVERSION} already checked out." && \
	return 0
    mkdir -p ${HLTBASEDIR}/interfaces/smi
    pushd ${HLTBASEDIR}/interfaces/smi
    wget http://smi.web.cern.ch/smi/smixx_${SMIVERSION}.zip
    unzip smixx_${SMIVERSION}.zip

    rm -f smixx_${SMIVERSION}.zip
    popd
    test -d ${SMIDIR} 
}

compile_smi() {
    check_smi && echo "smi_${SMIVERSION} already compiled." && \
	return 0
    pushd $SMIDIR
    sed -i -e 's:gmake:make:g' makefile
    make
    echo "Ignoring error during build of \"tcl/Tk GUI\"..."
    popd
    check_smi || return 1
}


module_smi(){
    MODULE_FILE="${HLTBASEDIR}/modules/SMI/${SMIVERSION}"
    test -f ${MODULE_FILE} && \
	echo "smi_${SMIVERSION} already has a module file." && \
	return 0
    mkdir -p "${HLTBASEDIR}/modules/SMI"
    cat <<EOF > ${MODULE_FILE}
#%Module1.0#####################################################################
##
## HLT - DIM modulefile
##

proc ModulesHelp { } {
        global version
        puts stderr "This module is a module of the HLT for SMI."
}

set     version         1.2

module-whatis   "SMI versions module for the HLT "

setenv          SMIDIR               $SMIDIR
setenv          SMIRTLDIR            $::env(SMIDIR)

append-path     PATH                 $::env(SMIDIR)/$::env(ODIR)
append-path     LD_LIBRARY_PATH      $::env(SMIDIR)/$::env(ODIR)
EOF

}

get_package_name_smi() {
    echo "smi-${SMIVERSION}-minimal"
}

deb_smi() {
    DEST="$PKGDIR"
    PREFIX="$(get_package_name_smi)"
    PREFIX=${PREFIX%%-minimal}

    test -f $DEST/${PREFIX}-full.deb && \
    test -f $DEST/${PREFIX}-minimal.deb && \
    echo "Packages for $PREFIX already exist." && \
	return 0

    rm -rf $DEST/${PREFIX}-full
    rm -rf $DEST/${PREFIX}-minimal

    echo "$SMIDIR" | path_cp "$DEST/${PREFIX}-minimal"

    pushd "$DEST/${PREFIX}-minimal/$SMIDIR"
    #remove Windows stuff
    rm -rf ./bin ./Visual

    find . -name '*.o' -exec rm -f '{}' \;
    popd

    MODULE_FILE="${HLTBASEDIR}/modules/SMI/${SMIVERSION}"
    echo "$MODULE_FILE" | path_cp $DEST/$PREFIX-minimal

    #dummy package, pulls in some more deps
    mkdir $DEST/${PREFIX}-full

    pushd $DEST

    create_deb $PREFIX-minimal "$(get_package_deps smi)" \
	"SMI minimal" "SMI minimal, for HLT online use."

    
    create_deb $PREFIX-full "tk, ${PREFIX}-minimal" \
	"SMI full" "Full version of SMI."

    popd


}
