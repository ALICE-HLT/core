#!/bin/bash


export HLTTOOLSDIR="/opt/HLT/tools"

check_tools(){
    test -d ${HLTTOOLSDIR}
}



checkout_libInfoLoggerLogServer(){
    if [[ -d "${HLTTOOLSDIR}/git/InfoLoggerLogServer" ]]; then
	pushd ${HLTTOOLSDIR}/git/InfoLoggerLogServer
	git pull || die
	popd
    else
	mkdir -p ${HLTTOOLSDIR}/git
	pushd ${HLTTOOLSDIR}/git
	git clone git://git-repo/InfoLoggerLogServer.git || die
	popd
    fi
}
compile_libInfoLoggerLogServer(){
    pushd  ${HLTTOOLSDIR}/git/InfoLoggerLogServer/ || die
    if [[ -d "./build" ]]; then
	cd build
	make install
    else
	mkdir build
	cd build
	cmake .. -DCMAKE_INSTALL_PREFIX="${HLTTOOLSDIR}"
	make install
    fi
    popd
}

checkout_TM_tools() {
    if [[ -d "${HLTTOOLSDIR}/src/TM-tools" ]]; then
	svn up ${HLTTOOLSDIR}/src/TM-tools || die
    else
	mkdir -p ${HLTTOOLSDIR}/src
	svn co https://svn.cern.ch/reps/hlt-alice/tools/src/TM-tools ${HLTTOOLSDIR}/src/TM-tools || die
    fi
}

compile_TM_tools() {
    mkdir -p ${HLTTOOLSDIR}/bin/
    for sh in ${HLTTOOLSDIR}/src/TM-tools/*.sh; do
	ln -sf $sh ${HLTTOOLSDIR}/bin/
    done
}

checkout_HRORC_tools() {
    if [[ -d "${HLTTOOLSDIR}/src/HRORC-tools" ]]; then
	svn up ${HLTTOOLSDIR}/src/HRORC-tools || die
    else
	mkdir -p ${HLTTOOLSDIR}/src
	svn co https://svn.cern.ch/reps/hlt-alice/tools/src/HRORC-tools ${HLTTOOLSDIR}/src/HRORC-tools || die
    fi
}

compile_HRORC_tools() {
    mkdir -p ${HLTTOOLSDIR}/bin/
    for pkg in evt_replay fcf_control rflash rorc_access rorc_status; do
	pushd ${HLTTOOLSDIR}/src/HRORC-tools/$pkg
	make || die
	popd
    done
    find ${HLTTOOLSDIR}/src/HRORC-tools -type f -a -perm -u=x \
	-exec ln -sf '{}' ${HLTTOOLSDIR}/bin/ \;
}

#dummy
mk_release_CDB() {
    mkdir -p ${HLTTOOLSDIR}/bin
    cat <<EOF > ${HLTTOOLSDIR}/bin/release-HCDB.sh
#!/bin/bash
/bin/true
EOF
    chmod +x  ${HLTTOOLSDIR}/bin/release-HCDB.sh
    cp -a ${HLTTOOLSDIR}/bin/release-HCDB.sh ${HLTTOOLSDIR}/bin/release-T-HCDB.sh
    cp -a ${HLTTOOLSDIR}/bin/release-HCDB.sh ${HLTTOOLSDIR}/bin/release-control.sh
}

checkout_tools() {
    checkout_libInfoLoggerLogServer
    checkout_TM_tools
    checkout_HRORC_tools
}

compile_tools() {
    compile_libInfoLoggerLogServer
    compile_TM_tools
    compile_HRORC_tools
    mk_release_CDB
}


module_tools(){
    check_tools
}

get_package_name_tools() {
    echo "tools"
}

deb_tools() {
    true
    DEST="$PKGDIR"
    PREFIX="$(get_package_name_tools)"

    test -f $DEST/${PREFIX}.deb && \
    echo "Packages for $PREFIX already exist." && \
	return 0

    rm -rf $DEST/${PREFIX}
    mkdir -p $DEST/${PREFIX}

    echo "${HLTTOOLSDIR}" | path_cp "$DEST/${PREFIX}"

    #hack script
    cat <<EOF > "$DEST/${PREFIX}/${HLTTOOLSDIR}/bin/infoBrowser.sh"
#!/usr/bin/env bash


export DATE_SOCKET_INFOLOG_RX=6001
export DATE_SITE_LOGS=/tmp/infoLogger
export DATE_SITE=standalone
export DATE_SOCKET_INFOLOG_TX=6002
export DATE_INFOLOGGER_LOGHOST=infologger

export DATE_INFOLOGGER_MYSQL=TRUE
export DATE_INFOLOGGER_MYSQL_HOST=mysql
export DATE_INFOLOGGER_MYSQL_USER=infologger
export DATE_INFOLOGGER_MYSQL_PWD=gee8Chao
export DATE_INFOLOGGER_MYSQL_DB=infologger

/usr/local/bin/infoBrowser.tcl

EOF
    chmod +x "$DEST/${PREFIX}/${HLTTOOLSDIR}/bin/infoBrowser.sh"
    
    pushd $DEST

    create_deb $PREFIX "mysqltcl" \
	"tools" "tools" "1.4"

    popd

}
