#!/bin/bash

export ALICE="${HLTBASEDIR}"
export ALICE_ROOT="${HLTBASEDIR}/aliroot/AliRoot_${ALIROOTVERSION}"

export ALICE_TARGET="${ROOT_TARGET}"

export PATH="${PATH}:${ALICE_ROOT}/bin/tgt_${ALICE_TARGET}"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ALICE_ROOT}/lib/tgt_${ALICE_TARGET}"


patch_aliroot() {
    true
}

check_aliroot(){
    check_binary ${ALICE_ROOT}/bin/tgt_${ALICE_TARGET}/aliroot
}

checkout_aliroot() {
    test -d  ${ALICE_ROOT} && echo "aliroot_${ALIROOTVERSION} already checked out." && \
	return 0
    mkdir -p ${HLTBASEDIR}/aliroot
    svn co https://alisoft.cern.ch/AliRoot/tags/${ALIROOTSVNVERSION} ${ALICE_ROOT} || die "checkout of aliroot failed"
    pushd $ALICE_ROOT

    test -f "$(dirname $0)/patches/aliroot_${ALIROOTVERSION}.sh" && \
	source "$(dirname $0)/patches/aliroot_${ALIROOTVERSION}.sh"
    patch_aliroot || die "patching aliroot failed."

    popd
}

compile_aliroot() {

    check_aliroot && echo "aliroot_${ALIROOTVERSION} already compiled." && \
	return 0

    pushd $ALICE_ROOT

    mkdir -p build || die
    cd build/
    cmake -DCMAKE_BUILD_TYPE="Release" -DCMAKE_C_FLAGS_RELEASE="-g -O3 -DLOG_NO_DEBUG" \
	-DCMAKE_CXX_FLAGS_RELEASE="-g -O3 -DLOG_NO_DEBUG" -DSHUTTLE=ON -DALIEN_ROOT=$ALIENDIR ..
    make -j9 || make -j9 || make -j9 || make
    make -j9 install
    split_debug ${ALICE_ROOT}/bin ${ALICE_ROOT}/lib
    popd
    check_aliroot || return 1
}

module_aliroot(){
    MODULE_FILE="${HLTBASEDIR}/modules/ALIROOT/${ALIROOTVERSION}"
    test -f ${MODULE_FILE} && \
	echo "dim_${DIMVERSION} already has a module file." && \
	return 0
    mkdir -p "${HLTBASEDIR}/modules/ALIROOT"
    cat <<EOF > ${MODULE_FILE}
#%Module1.0#####################################################################
##
## HLT - ALIROOT modulefile
##

proc ModulesHelp { } {
        global version
        puts stderr "This module is a module of the HLT for ALIROOT."
}

set     version         1.2

module-whatis   "ALIROOT versions module for the HLT"

## -- VERSION --
setenv          ALIROOT_RELEASE ${ALIROOTVERSION}

## -- ALICE --
setenv          ALICE           \$::env(ALICE_BASEDIR)

## -- ALIROOT --
setenv          ALICE_ROOT      \$::env(ALICE_BASEDIR)/AliRoot_\$::env(ALIROOT_RELEASE)

prepend-path    PATH            \$::env(ALICE_ROOT)/bin/tgt_\$::env(ALICE_TARGET_EXT)
prepend-path    LD_LIBRARY_PATH \$::env(ALICE_ROOT)/lib/tgt_\$::env(ALICE_TARGET_EXT)
EOF
}

get_package_name_aliroot() {
    echo "$(echo aliroot-${ALIROOTVERSION} | tr '[A-Z]' '[a-z]')-minimal"
}

deb_aliroot() {

    DEST="$PKGDIR"
    PREFIX="$(get_package_name_aliroot)"
    PREFIX=${PREFIX%%-minimal}

    test -f $DEST/${PREFIX}-full.deb && \
    test -f $DEST/${PREFIX}-minimal.deb && \
    test -f $DEST/${PREFIX}-debug.deb && \
    test -f $DEST/${PREFIX}-src.deb && \
    echo "Packages for $PREFIX already exist." && \
	return 0
    rm -rf $DEST/${PREFIX}-{full,minimal,debug,src}

    mkdir -p $DEST/$PREFIX-full/$(dirname $ALICE_ROOT)
    cp -a $ALICE_ROOT $DEST/$PREFIX-full/$(dirname $ALICE_ROOT)

    pushd $DEST/$PREFIX-full
    #clean unneccesary stuff (svn dirs, build dir)
    find .  -depth -path '*/.svn' -exec rm -rf '{}' \;
    rm -rf ./$ALICE_ROOT/build

    #debug symbols
    mkdir -p $DEST/$PREFIX-debug
    find . -depth -path "*/.debug" | path_mv $DEST/$PREFIX-debug

    #minimal
    mkdir -p $DEST/$PREFIX-minimal
    echo "./$ALICE_ROOT/lib" | path_mv $DEST/$PREFIX-minimal
    echo "./$ALICE_ROOT/bin" | path_mv $DEST/$PREFIX-minimal
    echo "./$ALICE_ROOT/include" | path_mv $DEST/$PREFIX-minimal
    echo "./$ALICE_ROOT/data" | path_mv $DEST/$PREFIX-minimal
    find . -depth -path "*/mapping" | path_mv $DEST/$PREFIX-minimal
    find . -depth -path "*/maps" | path_mv $DEST/$PREFIX-minimal
    find "./$ALICE_ROOT/HLT" -depth -name  '*.h' -o -name '*.hh' -o -name '*.hxx' | \
	path_mv $DEST/$PREFIX-minimal
    MODULE_FILE="${HLTBASEDIR}/modules/ALIROOT/${ALIROOTVERSION}"
    echo "$MODULE_FILE" | path_cp $DEST/$PREFIX-minimal


    #sources and headers
    mkdir -p $DEST/$PREFIX-src
    find . -name '*.cxx' -o -name '*.F' \
	-o -name '*.c' -o -name '*.f' \
	-o -name '*.h' -o -name '*.hh' -o -name '*.hxx' | \
	path_mv $DEST/$PREFIX-src   

    popd
    pushd $DEST
        
    create_deb $PREFIX-minimal "libgfortran3, $(get_package_deps aliroot)" \
	"AliRoot minimal" "AliRoot minimal installation, for HLT online use."

    create_deb $PREFIX-debug "$PREFIX-minimal" \
	"AliRoot debug symbols" "AliRoot debug symbols."

    create_deb $PREFIX-src "$PREFIX-minimal" \
	"AliRoot sources" "AliRoot source and header files"

    create_deb $PREFIX-full "$PREFIX-minimal, $PREFIX-debug, $PREFIX-src, $(get_package_deps aliroot full)" \
	"AliRoot full version" "AliRoot full installation."



    popd


}