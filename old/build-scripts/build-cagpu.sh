#!/bin/bash

export CAGPUDIR="${HLTBASEDIR}/cagpu/${CAGPUVERSION}-${ALIROOTVERSION}"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${CAGPUDIR}"




check_cagpu(){
    check_binary $CAGPUDIR/libAliHLTTPCCAGPU.so
}

checkout_cagpu() {
    test -d ${CAGPUDIR} && echo "cagpu-${CAGPUVERSION}-${ALIROOTVERSION} already checked out." && \
	return 0
    mkdir -p ${HLTBASEDIR}/cagpu
    svn -r $CAGPUREVISION co $CAGPUSVNURL $CAGPUDIR || die
    pushd $CAGPUDIR
    sed -i -e "s:/usr/local/cuda/lib64:$CUDADIR/lib64 -L$NVDIR:g" makefile
    popd
}

compile_cagpu() {
    check_cagpu && echo "cagpu-${CAGPUVERSION}-${ALIROOTVERSION} already compiled." && \
	return 0
    pushd $CAGPUDIR
    make
    popd
    check_cagpu || return 1
}


module_cagpu(){
    MODULE_FILE="${HLTBASEDIR}/modules/CAGPU/${CAGPUVERSION}-${ALIROOTVERSION}"
    test -f ${MODULE_FILE} && \
	echo "cagpu-${CAGPUVERSION}-${ALIROOTVERSION} already has a module file." && \
	return 0
    mkdir -p "${HLTBASEDIR}/modules/CAGPU"
    cat <<EOF > ${MODULE_FILE}
#%Module1.0#####################################################################
##
## HLT - versions modulefile
##

proc ModulesHelp { } {
        global version
        puts stderr "This module is cagpu tracker module of the HLT."
}

set     version         1.2

module-whatis   "cagpu tracker module for the HLT "

####################################################

## -- HLT - CAGPU --
setenv  CAGPUDIR $CAGPUDIR
prepend-path    LD_LIBRARY_PATH \$::env(CAGPUDIR)
prepend-path    LD_LIBRARY_PATH /usr/local/cuda/lib64
EOF

}

get_package_name_cagpu() {
    echo cagpu-${CAGPUVERSION}-${ALIROOTVERSION} | tr '[A-Z]' '[a-z]'
}

deb_cagpu() {
    DEST="$PKGDIR"
    PREFIX="$(get_package_name_cagpu)"

    test -f $DEST/${PREFIX}.deb && \
    echo "Packages for $PREFIX already exist." && \
	return 0

    rm -rf $DEST/${PREFIX}

    echo "$CAGPUDIR/libAliHLTTPCCAGPU.so" | path_cp $DEST/$PREFIX
    
    MODULE_FILE="${HLTBASEDIR}/modules/CAGPU/${CAGPUVERSION}-${ALIROOTVERSION}"
    echo "$MODULE_FILE" | path_cp $DEST/$PREFIX


    pushd $DEST

    
    create_deb $PREFIX "$(get_package_deps cagpu)" \
	"cagpu" "CA GPU tracker cuda libs."

    popd


}
