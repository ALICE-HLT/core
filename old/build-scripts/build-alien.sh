#!/bin/bash




export ALIENDIR="${HLTBASEDIR}/alien/${ALIENVERSION}" 
export PATH="${PATH}:${ALIENDIR}/api/bin" 
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ALIENDIR}/api/lib"

check_alien(){
    check_binary ${ALIENDIR}/api/bin/gbbox
}

checkout_alien() {
    check_alien && echo "alien-${ALIENVERSION} already checked out." && \
	return 0
    
    mkdir -p "${HLTBASEDIR}/alien"
    pushd "${HLTBASEDIR}/alien"
    
	#clean
    rm -rf ${ALIENVERSION} alien.${ALIENVERSION}.*
    
    wget http://alien.cern.ch/alien-installer
    chmod +x alien-installer
    ./alien-installer -install-dir "${ALIENDIR}" -version ${ALIENVERSION}
	#remove automatically created scripts in ~/bin
    rm -f ~/bin/aliensh ~/bin/alien-token-init
    rm -f alien-installer
	#remove 3rd-party libs, use OS shipped ones
    mv -f "${ALIENDIR}/lib" "${ALIENDIR}/lib.orig"
    popd
    check_alien || return 1
}

compile_alien() {
    check_alien && \
	echo "alien-${ALIENVERSION} already compiled." && \
	return 0
    return 1
}

module_alien(){
    MODULE_FILE="${HLTBASEDIR}/modules/ALIEN/${ALIENVERSION}"
    test -f ${MODULE_FILE} && \
	echo "alien-${ALIENVERSION} already has a module file." && \
	return 0
    mkdir -p "${HLTBASEDIR}/modules/ALIEN"
    cat <<EOF > ${MODULE_FILE}
#%Module1.0#####################################################################
##
## HLT - ALIEN modulefile
##

proc ModulesHelp { } {
        global version
        puts stderr "This module is a module of the HLT for ALIEN."
}

set     version         1.3

module-whatis   "ALIEN versions module for the HLT "

####################################################

## -- VERSION
set             alien_version                   ${ALIENVERSION}

## -- ALIEN --
setenv          ALIEN_ROOT                      \$::env(ALIEN_BASEDIR)/\$alien_version
setenv          ALIEN_INSTALLER_PLATFORM        x86_64-unknown-linux-gnu
setenv          GSHELL_GCC                      /usr/bin/gcc

append-path     LD_LIBRARY_PATH                 \$::env(ALIEN_ROOT)/api/lib

append-path     PATH                            \$::env(ALIEN_ROOT)/api/bin

## -- Globus Location --
setenv          GLOBUS_LOCATION                 \$::env(ALIEN_ROOT)/globus
EOF

}

get_package_name_alien() {
    echo "alien-$ALIENVERSION"
}

deb_alien() {
    DEST="$PKGDIR"
    PREFIX="$(get_package_name_alien)"

    test -f $DEST/${PREFIX}.deb && echo "Package $PREFIX already exists." && \
	return 0

    rm -rf "$DEST/$PREFIX"

    REALALIEN="$(readlink -e $ALIENDIR)"
    if [[ -n "$REALALIEN" ]]; then
	mkdir -p $DEST/$PREFIX/$(dirname ${REALALIEN})
	cp -a ${REALALIEN} $DEST/$PREFIX/$(dirname ${REALALIEN})
    fi
    mkdir -p $DEST/$PREFIX/$(dirname ${ALIENDIR})
    cp -a ${ALIENDIR} $()  $DEST/$PREFIX/$(dirname ${ALIENDIR})

    MODULE_FILE="${HLTBASEDIR}/modules/ALIEN/${ALIENVERSION}"
    mkdir -p $DEST/$PREFIX/$(dirname ${MODULE_FILE})
    cp -a "$MODULE_FILE" $DEST/$PREFIX/$(dirname ${MODULE_FILE})

    pushd $DEST/$PREFIX/$ALIENDIR
    rm lib lib.orig -rf
    popd

    pushd  $DEST
    create_deb "$PREFIX" "libssl0.9.8, libxml2, zlib1g-dev" \
	"Alien HLT package" "Alien distribution, packaged for use in HLT."
    popd

}