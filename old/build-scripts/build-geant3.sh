#!/bin/bash



export GEANT3DIR="${HLTBASEDIR}/geant3/geant3_${GEANT3VERSION}_root_${ROOTVERSION}"
export PLATFORM="${ROOT_TARGET}"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${GEANT3DIR}/lib/tgt_${PLATFORM}"


check_geant3(){
    check_binary ${GEANT3DIR}/lib/tgt_${PLATFORM}/libgeant321.so
}

checkout_geant3() {
    test -d ${GEANT3DIR} && echo "geant3_${GEANT3VERSION}_root_${ROOTVERSION} already checked out." && \
	return 0
    mkdir -p ${HLTBASEDIR}/geant3
    svn co https://root.cern.ch/svn/geant3/tags/${GEANT3VERSION} $GEANT3DIR
}

compile_geant3() {
    check_geant3 && echo "geant3_${GEANT3VERSION}_root_${ROOTVERSION}  already compiled." && \
	return 0
    pushd $GEANT3DIR
    make -j9 || make -j9 || make -j9 || make
    popd
    check_geant3 || return 1
}


module_geant3(){
    MODULE_FILE="${HLTBASEDIR}/modules/GEANT3/${GEANT3VERSION}"
    test -f ${MODULE_FILE} && \
	echo "geant3_${GEANT3VERSION}_root_${ROOTVERSION} already has a module file." && \
	return 0
    mkdir -p "${HLTBASEDIR}/modules/GEANT3"
    cat <<EOF > ${MODULE_FILE}
#%Module1.0#####################################################################
##
## HLT - GEANT3 modulefile
##

proc ModulesHelp { } {
        global version
        puts stderr "This module is a module of the HLT for GEANT3."
}

set     version         1.2

module-whatis   "GEANT3 versions module for the HLT "

## -- VERSION --
setenv          GEANT3_RELEASE  ${GEANT3VERSION}

## -- GEANT3 --
setenv          PLATFORM        \$::env(ALICE_TARGET)
append-path     LD_LIBRARY_PATH \$::env(GEANT3_BASEDIR)/geant3_\$::env(GEANT3_RELEASE)_root_\$::env(ROOT_RELEASE)/lib/tgt_\$::env(ALICE_TARGET)
EOF

}

get_package_name_geant3() {
    echo "geant3-${GEANT3VERSION}-root-${ROOTVERSION}"
}

deb_geant3() {
    DEST="$PKGDIR"
    PREFIX="$(get_package_name_geant3)"

    test -f $DEST/${PREFIX}.deb && \
    echo "Packages for $PREFIX already exist." && \
	return 0

    rm -rf $DEST/${PREFIX}

    echo "$GEANT3DIR/lib" | path_cp $DEST/$PREFIX
    
    MODULE_FILE="${HLTBASEDIR}/modules/GEANT3/${GEANT3VERSION}"
    echo "$MODULE_FILE" | path_cp $DEST/$PREFIX


    pushd $DEST

    
    create_deb $PREFIX "libgfortran3, $(get_package_deps geant3)" \
	"Geant 3" "Geant 3, for HLT online use."

    popd


}