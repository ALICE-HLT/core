#!/bin/bash

source "$(dirname $0)/build-helpers.sh"

usage() {
    echo "$0 [checkout|compile] [packages ...]"
}

checkout_pkg() {
    for package in $@; do
	echo "Checking out \"$package\"..."
	source "$(dirname $0)/build-${package}.sh" || die "Error trying to source build script"
	checkout_${package} || die "Checkout of \"$package\" failed!"
	echo "Package \"$package\" properly checked out."
    done
}

compile_pkg() {
    for package in $@; do
	checkout_pkg ${package}
	echo "Compiling \"$package\"..."
	compile_${package} || die "Compilation of \"$package\" failed!"
	echo "Package \"$package\" compiled."
    done
}

module_pkg() {
    for package in $@; do
	compile_pkg ${package}
	echo "Creating module file for \"$package\"..."
	module_${package} || die "Creation of module file for \"$package\" failed!"
	echo "Module file for \"$package\" created."
    done
}

deb_pkg() {
    for package in $@; do
	source "$(dirname $0)/build-${package}.sh" 
	#module_pkg ${package}
	echo "Packaging \"$package\"..."
	deb_${package} || die "Packaging of \"$package\" failed!"
	echo "Package \"$package\" packaged."
    done
}

resolve_deps() {
    package="$1"
    shift
    DEPENDS=" $@ "
    for dep in $(eval echo \$${package}_deps); do
	echo "$DEPENDS" | grep -q " $dep " || \
	    DEPENDS=" $(resolve_deps $dep $DEPENDS) $dep "
    done
    echo $DEPENDS
}

COMMAND="$1"
shift
PACKAGES="$@"

source "$(dirname $0)/config/HLT-current"


echo "Resolving dependencies..."

DEPENDS=""
REMAINS=""
for package in $PACKAGES; do
    DEPENDS=" $(resolve_deps $package $DEPENDS) "
done

for package in $PACKAGES; do
    echo "$DEPENDS" | grep -q " $package " || \
	    REMAINS="$REMAINS $package "
done

echo "Dependencies:       $DEPENDS"
echo "Remaining packages: $REMAINS"


case $COMMAND in
    checkout)
	checkout_pkg $DEPENDS $REMAINS
	;;
    compile)
	compile_pkg $DEPENDS $REMAINS
	;;
    module)
	module_pkg $DEPENDS $REMAINS
	;;
    deb)
	deb_pkg $DEPENDS $REMAINS
	;;
    *)
	usage
	exit 1
	;;
esac


