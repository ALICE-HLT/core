#!/bin/bash


check_base(){
    true
}

checkout_base() {
    true
}

compile_base() {
    check_base
}

module_definition(){
    MODULE_FILE="${HLTBASEDIR}/modules/DEFINITION/$BASEVERSION"
    test -f ${MODULE_FILE} && \
	return 0
    mkdir -p "${HLTBASEDIR}/modules/DEFINITION"
    cat <<EOF > ${MODULE_FILE}
#%Module1.0#####################################################################
##
## HLT - DEFINITION modulefile
##

proc ModulesHelp { } {
        global version
        puts stderr "This module is a the DEFINITION module of the HLT."
}

set     version         1.0

module-whatis   "DEFINITION module for the HLT "

## ----------------
## -- BASE PORTS --
## ----------------
setenv          ALIHLT_PORT_BASE_RUNCONTROL             20000

setenv          ALIHLT_DETECTOR_IDENTIFIER_ALICE            0
setenv          ALIHLT_DETECTOR_IDENTIFIER_TPC              1
setenv          ALIHLT_DETECTOR_IDENTIFIER_TRD              2
setenv          ALIHLT_DETECTOR_IDENTIFIER_PHOS             3
setenv          ALIHLT_DETECTOR_IDENTIFIER_DIMUON           4
setenv          ALIHLT_DETECTOR_IDENTIFIER_ITS              5
setenv          ALIHLT_DETECTOR_IDENTIFIER_DEV              6
setenv          ALIHLT_DETECTOR_IDENTIFIER_TEST             7
setenv          ALIHLT_DETECTOR_IDENTIFIER_EMCAL            8

setenv          ALIHLT_PORTRANGE_RUNMGR                   100
setenv          ALIHLT_PORTRANGE_DETECTOR                2000

## ---------------------
## -- PENDOLINO PORTS --
## ---------------------
setenv          ALIHLT_PORTOFFSET_PEND_1                    1
setenv          ALIHLT_PORTOFFSET_PEND_2                    2
setenv          ALIHLT_PORTOFFSET_PEND_3                    3
setenv          ALIHLT_PORTOFFSET_PEND_SL                   4
setenv          ALIHLT_PORTOFFSET_PEND_MGR                  5

## ---------------------
## -- ECS PROXY PORTS --
## ---------------------
setenv          ALIHLT_PORTOFFSET_ECSLIB                   10
EOF
}

module_base(){
    module_definition
    MODULE_FILE="${HLTBASEDIR}/modules/BASE/$BASEVERSION"
    test -f ${MODULE_FILE} && \
	echo "base-$BASEVERSION already has a module file." && \
	return 0
    mkdir -p "${HLTBASEDIR}/modules/BASE"
    cat <<EOF > ${MODULE_FILE}
#%Module1.0#####################################################################
##
## HLT - BASE modulefile
##

proc ModulesHelp { } {
        global version
        puts stderr "This module is the BASE module of the HLT."
}

set     version         1.3

module-whatis   "BASE module for the HLT"

## -- BASE DIRECTORYS --
setenv          ALICE_BASEDIR              /opt/HLT/aliroot
setenv          ALIEN_BASEDIR              /opt/HLT/alien
setenv          ROOT_BASEDIR               /opt/HLT/root
setenv          GEANT3_BASEDIR             /opt/HLT/geant3
setenv          FASTJET_BASEDIR            /opt/HLT/fastjet
setenv          DC_BASEDIR                 /opt/HLT/data-transport
setenv          HLTECS_BASEDIR             /opt/HLT/interfaces/ecs-proxy
setenv          HLTTAXI_BASEDIR            /opt/HLT/interfaces/taxi
setenv          HLTPENDOLINO_BASEDIR       /opt/HLT/interfaces/pendolino
setenv          HLTHCDBMANAGER_BASEDIR     /opt/HLT/interfaces/HCDBManager
setenv          HLTCONTROL_DIR             /opt/HLT/control
setenv          HLTCONTROL_BASEDIR         /opt/HLT/control/releases

setenv          ALIHLT_HCDBDIR             /opt/HCDB
setenv          ALIHLT_T_HCDBDIR           /opt/T-HCDB

#setenv          HLTTESTSERVER_BASEDIR      /afs/.alihlt.cern.ch/testServer
#setenv          HLTTEST_BASEDIR            /opt/HLT-test

## -- SET TOOLS DIRECTORYS --
append-path     PATH                       /opt/HLT/tools/bin
append-path     LD_LIBRARY_PATH            /opt/HLT/tools/lib

## -- XERCES --
setenv          XERCESCDIR                 /usr/

## -- QT --
setenv          QTDIR                      /usr/share/qt3

prepend-path    PATH                       \$::env(QTDIR)/bin
prepend-path    LD_LIBRARY_PATH            \$::env(QTDIR)/lib
## -- Info Logger --
setenv          HLTINFOLOGGER_LIBDIR       /opt/HLT/tools/lib
setenv          DATE_SITE_LOGS             /tmp/infoLogger
setenv          DATE_INFOLOGGER_LOGHOST    infologger
setenv          DATE_SITE                  standalone
setenv          DATE_SOCKET_INFOLOG_RX     6001
setenv          DATE_SOCKET_INFOLOG_TX     6002
append-path     LD_LIBRARY_PATH            /usr/local/lib

## -- Repositories --
#setenv          HLTSVN_BASEDIR             file:///afs/.alihlt.cern.ch/repository/svn
#setenv          HLTSVN_BASEURL             svn+ssh://dev0/afs/.alihlt.cern.ch/repository/svn

#setenv          HLTGIT_BASEDIR             ssh://dev0/afs/.alihlt.cern.ch/repository/git
#setenv          HLTGIT_BASEURL             file:///afs/.alihlt.cern.ch/repository/git
EOF

}

get_package_name_base() {
    echo "base-$BASEVERSION"
}

deb_base() {
    DEST="$PKGDIR"
    PREFIX="$(get_package_name_base)"

    test -f $DEST/${PREFIX}.deb && \
    echo "Packages for $PREFIX already exist." && \
	return 0

    rm -rf $DEST/${PREFIX}
    mkdir -p $DEST/${PREFIX}

    echo "${HLTBASEDIR}/modules/BASE/$BASEVERSION" | path_cp $DEST/$PREFIX
    echo "${HLTBASEDIR}/modules/DEFINITION/$BASEVERSION" | path_cp $DEST/$PREFIX
    pushd $DEST

    create_deb $PREFIX "" \
	"Base" "Base"

    popd

}
