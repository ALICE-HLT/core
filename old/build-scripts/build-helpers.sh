#!/bin/bash

die() {
    echo "$@" >&2
    exit 1
}

check_binary(){
#heuristic to determine already installed version
    test -x $1 || return 1
    ldd $1 | grep -q "not found" && return 1 
    return 0
}

is_elf(){
    f=$(file "${1}")
    [[ ${f} == *"SB executable"* || ${f} == *"SB shared object"* ]]
}

split_debug(){
    for searchdir in $*  ; do 
	echo "Looking for ELFs in \"$searchdir\"..."
	for f in $(find "$searchdir" -type f); do
	    t=$(file "${f}")
	    if [[ ${t} == *"SB executable"* || ${t} == *"SB shared object"* ]]; then
		echo "Stripping $f"
		local d="$(dirname $f)/.debug/$(basename ${f}).dbg"
		mkdir -p "$(dirname $f)/.debug"
		objcopy --only-keep-debug  "$f" "$d"
		strip --strip-debug "$f"
		objcopy --add-gnu-debuglink="$d" "$f"
	    fi
	done
    done
}

path_mv() {
    DST="$1"
    mkdir -p $DST
    cat /dev/stdin | while read file; do
	mkdir -p $DST/$(dirname $file)
	mv $file $DST/$file
    done
}

path_cp() {
    DST="$1"
    mkdir -p $DST
    cat /dev/stdin | while read file; do
	mkdir -p $DST/$(dirname $file)
	cp -a $file $DST/$file
    done
}

create_deb() {
    #possible: 
    # -- gzip (very fast)
    # -- bzip2 (a bit slower, slightly better compression ratio)
    # -- lzma (4 times slower, compression significantly better than bzip2)
    local COMPRESSION="lzma"

    local PKGNAME="$1"
    local DEPEND="$2"
    local DESCR_SHORT="$3"
    local DESCR_LONG="$4"
    local PKGVERSION="$5"
    [[ -z "$PKGVERSION" ]] && PKGVERSION="1"

    local POSTINSTCMD="$6"

    mkdir -p ${PKGNAME}/DEBIAN
    cat <<EOF > ${PKGNAME}/DEBIAN/control
Package: $PKGNAME
Version: $PKGVERSION
Architecture: amd64
Maintainer: hlt-admin <alice-hlt-cluster-admin@cern.ch>
EOF
    test -n "$DEPEND" && echo "Depends: $DEPEND" >> ${PKGNAME}/DEBIAN/control
    cat <<EOF >> ${PKGNAME}/DEBIAN/control
Section: hlt
Priority: optional
Description: ALICE HLT: $DESCR_SHORT
 ALICE HLT package:
 $DESCR_LONG
EOF

    if [[ -n "$POSTINSTCMD" ]]; then
	cat <<EOF > ${PKGNAME}/DEBIAN/postinst
#!/bin/bash
$POSTINSTCMD
EOF
	chmod +x ${PKGNAME}/DEBIAN/postinst
    fi
    chown -R root:root ${PKGNAME} || die "not root????"
    dpkg-deb -Z$COMPRESSION --build ${PKGNAME}
}

get_package_deps() {
    local first=1
    for dep in $(eval echo \$${1}_deps); do
	local pkg_name="$(get_package_name_$dep)"
	[[ $first -eq 0 ]] && echo -n ", "
	if [[ "$2" == "full" ]]; then
	    echo -n "${pkg_name/%-minimal/-full}"
	else
	    echo -n "$pkg_name"
	fi
	first=0
    done
}