#!/bin/bash

export HLTTAXI_TOPDIR="${HLTBASEDIR}/interfaces/taxi/$TAXIVERSION-$ALIROOTVERSION"


check_taxi(){
    check_binary ${HLTTAXI_TOPDIR}/AliHLTTaxiLib.so
}

checkout_taxi() {
    test -d ${HLTTAXI_TOPDIR} && echo "taxi-$TAXIVERSION-$ALIROOTVERSION already checked out." && \
	return 0

    mkdir -p ${HLTBASEDIR}/interfaces/taxi

    svn co https://svn.cern.ch/reps/hlt-alice/interfaces/taxi/tags/release-$TAXIVERSION \
	${HLTTAXI_TOPDIR} || die
}

compile_taxi() {
    check_taxi && echo "taxi-$TAXIVERSION-$ALIROOTVERSION already compiled." && \
	return 0 
    pushd ${HLTTAXI_TOPDIR}
    make
    popd
    check_taxi
}


module_taxi(){
    MODULE_FILE="${HLTBASEDIR}/modules/TAXI/$TAXIVERSION-$ALIROOTVERSION"
    test -f ${MODULE_FILE} && \
	echo "taxi-$TAXIVERSION-$ALIROOTVERSION already has a module file." && \
	return 0
    mkdir -p "${HLTBASEDIR}/modules/TAXI"
    cat <<EOF > ${MODULE_FILE}
#%Module1.0#####################################################################
##
## HLT - versions modulefile
##

proc ModulesHelp { } {
        global version
        puts stderr "This module is a module of the HLT for the TAXI."
}

set     version         1.4

module-whatis   "TAXI versions module for the HLT "

####################################################

## -- TAXI --
setenv          HLTTAXI_TOPDIR         \$::env(HLTTAXI_BASEDIR)/$TAXIVERSION-$ALIROOTVERSION
EOF

}

get_package_name_taxi() {
    echo "$(echo taxi-$TAXIVERSION-$ALIROOTVERSION | tr '[A-Z]' '[a-z]')"
}

deb_taxi() {
    DEST="$PKGDIR"
    PREFIX="$(get_package_name_taxi)"

    test -f $DEST/${PREFIX}.deb && \
    echo "Packages for $PREFIX already exist." && \
	return 0

    rm -rf $DEST/${PREFIX}
    mkdir -p $DEST/${PREFIX}

    echo "$HLTTAXI_TOPDIR" | path_cp "$DEST/${PREFIX}"

    MODULE_FILE="${HLTBASEDIR}/modules/TAXI/$TAXIVERSION-$ALIROOTVERSION"
    echo "$MODULE_FILE" | path_cp $DEST/$PREFIX

    pushd $DEST

    create_deb $PREFIX "" \
	"Taxi" "Taxi"

    popd

}
