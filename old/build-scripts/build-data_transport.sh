#!/bin/bash

export ALIHLT_DC_DIR="${HLTBASEDIR}/data-transport/HLT-trunk-${DATA_TRANSPORTVERSION}"
export ALIHLT_DC_LIBDIR="${ALIHLT_DC_DIR}/lib/Linux-x86_64-releasedebug"
export ALIHLT_DC_BINDIR="${ALIHLT_DC_DIR}/bin/Linux-x86_64-releasedebug"

export PATH="${PATH}:${ALIHLT_DC_DIR}/bin/Linux-x86_64-releasedebug"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ALIHLT_DC_DIR}/lib/Linux-x86_64-releasedebug"


TARGETS="
           MLUC
           BCL 
           TaskManager 
           PSI2 
           Utility_Software/HLTReadoutList 
           Utility_Software/CommonDataHeader 
           Utility_Software/NOPEEvent 
           Utility_Software/AliRootInterfaceLibrary 
           Framework 
           SimpleChainConfig1 
           SimpleChainConfig2 
           Utility_Software/LogFileDelivery 
           Utility_Software/HOMER 
           Utility_Software/ERSGUI 
           Utility_Software/DDLHeaderCopy 
           Utility_Software/SimpleComponentWrapper 
           Utility_Software/TCPHOMERDataProvider 
           Utility_Software/WHALE 
           Utility_Software/DATEDataReader 
           Utility_Software/ComponentStatusScan 
           Utility_Software/TM-State-Check 
           Components/ACEXPublisher 
           Components/AliRootWrapperSubscriber 
           Components/ASCIIDataWriter 
           Components/BinaryDataWriter 
           Components/BlockComparer 
           Components/DATEPublisher 
           Components/DDLHeaderPublisher 
           Components/DDLConnectionTester 
           Components/DummyLoad 
           Components/DummyReadoutListProducer 
           Components/EventDoneDataProducer 
           Components/EventDoneRequestTestDummy 
           Components/EventProtocolSubscriber 
           Components/EventRateSubscriber 
           Components/EventStorageWriter 
           Components/FilePublisher 
           Components/FXSSubscriber 
           Components/HLTOutWriterSubscriber 
           Components/HLTOutFormatter 
           Components/Relay 
           Components/RandomDataPublisher 
           Components/RORCPGTestSubscriber 
           Components/RORCPublisher 
           Components/ShmDumpSubscriber 
           Components/SingleBlockUnwrapper 
           Components/StoreForwarder 
           Components/TCPDumpSubscriber 
           Utility_Software/RORCDebugger 
           Utility_Software/TM_Notifier
"

check_data_transport(){
    check_binary ${ALIHLT_DC_DIR}/lib/Linux-x86_64-releasedebug/TMLib.so && \
    check_binary ${ALIHLT_DC_DIR}/bin/Linux-x86_64-releasedebug/AliRootWrapperSubscriber && \
    check_binary ${ALIHLT_DC_DIR}/bin/Linux-x86_64-releasedebug/FXSSubscriber
}

checkout_data_transport() {
    let maxtries=3

    test -f ${ALIHLT_DC_DIR}/src/Makefile && echo "data-transport-${DATA_TRANSPORTVERSION} already checked out." && \
	return 0

    mkdir -p ${HLTBASEDIR}/data-transport

    pushd ${HLTBASEDIR}/data-transport
    if [[ -d Build ]]; then
	svn up Build
    else
	let tries=0
	while [[ ! -d  Build ]]; do
	    [[ $tries -ge $maxtries ]] && die "couldn't checkout Build"
	    let tries++
	    svn co https://svn.cern.ch/reps/hlt-alice/Build/trunk Build
	done
    fi
    SVNREVARG=""
    [[ -n "$DATA_TRANSPORTSVNREV" ]] && SVNREVARG="-r$DATA_TRANSPORTSVNREV"

    mkdir -p HLT-trunk-${DATA_TRANSPORTVERSION}/src
    cd HLT-trunk-${DATA_TRANSPORTVERSION}/src

    for target in $TARGETS; do
	echo "Checking out $target:"
	let tries=0
	while [[ ! -d "$target"  ]]; do
	    [[ $tries -ge $maxtries ]] && die "Couldn't checkout \"$target\""
	    let tries++
	    test -d $(dirname $target) || mkdir -p $(dirname $target)
	    echo svn $SVNREVARG co https://svn.cern.ch/reps/hlt-alice/$target/trunk $target
	    svn $SVNREVARG co https://svn.cern.ch/reps/hlt-alice/$target/trunk $target
	    [[ ! -d "$target"  ]] && echo "Didn't work, try again."
	done
    done
    for file in XMLConfigReader.py SimpleChainConfig1.py scripts templates; do
	test -L SimpleChainConfig2/$file || ln -s ../SimpleChainConfig1/$file SimpleChainConfig2/$file
    done
    ln -s Utility_Software Util
    cp ${HLTBASEDIR}/data-transport/Build/Makefile . || die
    cp ${HLTBASEDIR}/data-transport/Build/install.sh . || die
    make clean PRODUCTIONDEBUG=1 &>/dev/null
    make wipe PRODUCTIONDEBUG=1 &>/dev/null
    
    ret=$?
    popd
    return $ret
}

compile_data_transport() {
    check_data_transport && echo "data-transport-${DATA_TRANSPORTVERSION} already compiled." && \
	return 0

    pushd ${ALIHLT_DC_DIR}/src

    #clean dirs
    find . -depth -name .debug -exec rm -rf '{}' \;
    rm -rf ${ALIHLT_DC_DIR}/lib ${ALIHLT_DC_DIR}/bin


    cp $ALICE_ROOT/HLT/BASE/AliHLTDataTypes.h Util/AliRootInterfaceLibrary/ || die
    make PRODUCTIONDEBUG=1 || die    
    ./install.sh -productiondebug || die

    mkdir -p "${ALIHLT_DC_DIR}/lib/Linux-x86_64-releasedebug/.debug"
    mkdir -p "${ALIHLT_DC_DIR}/bin/Linux-x86_64-releasedebug/.debug"

    for dirname in bin lib; do
	for dir in $( find . -name $dirname -a -type d ); do
	    split_debug $dir
	    for dbg in $( find $dir -name '*.dbg' -a -type f ); do
		ln -s ../../../src/$dbg  ${ALIHLT_DC_DIR}/$dirname/Linux-x86_64-releasedebug/.debug/
	    done
	done
    done
    popd
    check_data_transport || return 1
}


module_data_transport(){
    MODULE_FILE="${HLTBASEDIR}/modules/DATATRANSPORT/${DATA_TRANSPORTVERSION}"
    test -f ${MODULE_FILE} && \
	echo "data-transport-${DATA_TRANSPORTVERSION} already has a module file." && \
	return 0
    mkdir -p "${HLTBASEDIR}/modules/DATATRANSPORT"
    cat <<EOF > ${MODULE_FILE}
#%Module1.0#####################################################################
##
## HLT - DATATRANSPORT modulefile
##

proc ModulesHelp { } {
        global version
        puts stderr "This module is a module of the HLT for the DATATRANSPORT."
}

set     version         1.2

module-whatis   "DATATRANSPORT versions module for the HLT "

####################################################

setenv          ALIHLT_DC_DIR      \$::env(DC_BASEDIR)/HLT-trunk-${DATA_TRANSPORTVERSION}
setenv          ALIHLT_DC_BINDIR   \$::env(ALIHLT_DC_DIR)/bin/Linux-x86_64-releasedebug
setenv          ALIHLT_DC_LIBDIR   \$::env(ALIHLT_DC_DIR)/lib/Linux-x86_64-releasedebug

prepend-path    PATH               \$::env(ALIHLT_DC_DIR)/bin/Linux-x86_64-releasedebug
prepend-path    LD_LIBRARY_PATH    \$::env(ALIHLT_DC_DIR)/lib/Linux-x86_64-releasedebug

EOF

}

get_package_name_data_transport() {
    echo "data-transport-${DATA_TRANSPORTVERSION}-minimal"
}

deb_data_transport() {
    DEST="$PKGDIR"
    PREFIX="$(get_package_name_data_transport)"
    PREFIX=${PREFIX%%-minimal}

    test -f $DEST/${PREFIX}-minimal.deb && \
    test -f $DEST/${PREFIX}-full.deb && \
    test -f $DEST/${PREFIX}-debug.deb && \
    test -f $DEST/${PREFIX}-src.deb && \
    test -f $DEST/psi-${KERNELVERSION}.deb && \
    echo "Packages for $PREFIX already exist." && \
	return 0

    rm -rf $DEST/${PREFIX}-{minimal,full,debug,src}
    rm -rf $DEST/psi-${KERNELVERSION}

    mkdir -p $DEST/${PREFIX}-minimal
    mkdir -p $DEST/${PREFIX}-full
    mkdir -p $DEST/${PREFIX}-debug
    mkdir -p $DEST/${PREFIX}-src
    mkdir -p $DEST/psi-${KERNELVERSION}

    echo "${ALIHLT_DC_DIR}" | path_cp $DEST/${PREFIX}-minimal

    MODULE_FILE="${HLTBASEDIR}/modules/DATATRANSPORT/${DATA_TRANSPORTVERSION}"
    echo "$MODULE_FILE" | path_cp $DEST/$PREFIX-minimal

    pushd $DEST/${PREFIX}-minimal
    find  . -depth -path '*/.svn' -exec rm -rf '{}' \; \
	-o -name '*.o'  -exec rm -rf '{}' \; \
	-o -name '*.d'  -exec rm -rf '{}' \;
    find . -depth -name .debug -a -type d | path_mv $DEST/${PREFIX}-debug

    find -name '*.cpp' -o -name '*.hpp' \
	-o -name '*.c' -o -name '*.h' \
	-o -name '*.cxx' -o -name '*.hxx' \
	-o -name '*.vstdl' | path_mv $DEST/${PREFIX}-src
    popd

    mkdir -p $DEST/psi-${KERNELVERSION}/lib/modules/${KERNELVERSION}/kernel/drivers/misc/psi/
    cp -a \
	${ALIHLT_DC_DIR}/src/PSI2/src/module/build/Linux-x86_64-releasedebug/${KERNELVERSION}/psi.ko \
	$DEST/psi-${KERNELVERSION}/lib/modules/${KERNELVERSION}/kernel/drivers/misc/psi/

    #udev rule
    mkdir -p $DEST/psi-${KERNELVERSION}/etc/udev/rules.d/
    cat <<EOF >$DEST/psi-${KERNELVERSION}/etc/udev/rules.d/90-hlt-psi.rules
KERNEL=="psi", OWNER="hlt-operator", GROUP="hlt-operators"
EOF
    chmod 0644 $DEST/psi-${KERNELVERSION}/etc/udev/rules.d/90-hlt-psi.rules

    pushd $DEST

    create_deb $PREFIX-minimal \
	"python-libxml2, libxerces-c28, libavahi-client3, libavahi-common3, $(get_package_deps data_transport)" \
	"Data Transport Framework" "Data Transport Framework" "1.4"

    create_deb $PREFIX-src \
	"$PREFIX-minimal" \
	"Data Transport Framework sources and header" "Data Transport Framework sources and headers" "1.4"

    create_deb $PREFIX-debug \
	"$PREFIX-minimal" \
	"Data Transport Framework debug symbols" "Data Transport Framework debug symbols" "1.4"

    create_deb $PREFIX-full \
	"python-qt3, $PREFIX-minimal, $PREFIX-debug, $PREFIX-src" \
	"Data Transport Framework full" "Data Transport Framework full version (more deps)" "1.4"

    create_deb psi-${KERNELVERSION} "" \
	"psi driver for kernel ${KERNELVERSION}" "psi driver for kernel ${KERNELVERSION}" \
	"1" "depmod -a ${KERNELVERSION}"

    popd
}
