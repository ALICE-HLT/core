#!/bin/bash

export NVDIR="${HLTBASEDIR}/nvidia-driver/NVIDIA-Linux-x86_64-$NVIDIADRIVERVERSION"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${NVDIR}"



check_nvidia_driver(){
    check_binary $NVDIR/libcuda.so.${NVIDIADRIVERVERSION} && test -f $NVDIR/kernel/nvidia.ko
}

checkout_nvidia_driver() {
    
    test -d ${NVDIR} && echo "nvidia-driver-${NVIDIADRIVERVERSION} already checked out." && \
	return 0
 
    mkdir -p ${HLTBASEDIR}/nvidia-driver
    cd ${HLTBASEDIR}/nvidia-driver
    test -f ./NVIDIA-Linux-x86_64-${NVIDIADRIVERVERSION}.run || \
	wget ftp://download.nvidia.com/XFree86/Linux-x86_64/295.40/NVIDIA-Linux-x86_64-${NVIDIADRIVERVERSION}.run

    chmod +x ./NVIDIA-Linux-x86_64-${NVIDIADRIVERVERSION}.run
    
    ./NVIDIA-Linux-x86_64-${NVIDIADRIVERVERSION}.run -x --keep

}

liblink() {
    LIB="$1"
    BASELIB="$(echo ${LIB} | awk -F. '{print $1}').so"
    MAJOR="$2"
    ln -s "$LIB" "${BASELIB}"
    [[ -n "$2" ]] && ln -s "${LIB}" "${BASELIB}.${MAJOR}"
}

compile_nvidia_driver() {
    check_nvidia_driver && echo "nvidia-driver-${NVIDIADRIVERVERSION} already compiled." && \
	return 0
    pushd $NVDIR/kernel
    make
    make module SYSSRC=/lib/modules/${KERNELVERSION}/build

    cd ..
    for lib in lib*.so.*; do
	liblink $lib 1
    done
    popd
    check_nvidia_driver || return 1
}


module_nvidia_driver(){
    true
}

get_package_name_nvidia_driver() {
    echo "nvidia-driver-${KERNELVERSION}"
}

deb_nvidia_driver() {
    DEST="$PKGDIR"
    PREFIX="$(get_package_name_nvidia_driver)"

    test -f $DEST/${PREFIX}.deb && \
    echo "Packages for $PREFIX already exist." && \
	return 0

    rm -rf $DEST/${PREFIX}

    mkdir -p $DEST/${PREFIX}
    pushd $DEST/${PREFIX}

    mkdir -p usr/lib
    for lib in libcuda.so libnvcuvid.so libnvidia-cfg.so libnvidia-compiler.so \
	libnvidia-glcore.so libnvidia-ml.so libnvidia-tls.so libvdpau.so libOpenCL.so libXvMCNVIDIA.so; do
	cp -a $NVDIR/${lib}* usr/lib/
    done

# omitting OpenGL/Xorg stuff for the moment
#    cp -a $NVDIR/libGL.so* usr/lib/

#    mkdir -p usr/lib/xorg/modules/extensions
#    cp -a $NVDIR/libglx.so* usr/lib/xorg/modules/extensions

#    mkdir -p usr/lib/xorg/modules/drivers
#    cp -a $NVDIR/nvidia_drv.so usr/lib/xorg/modules/drivers/

#    cp -a $NVDIR/libnvidia-wfb.so* usr/lib/xorg/modules/

#    mkdir -p usr/lib/tls
#    cp -a $NVDIR/tls/libnvidia-tls.so* usr/lib/tls/

#    mkdir -p usr/lib/vdpau
#    cp -a  $NVDIR/libvdpau_nvidia.so $NVDIR/libvdpau_trace.so usr/lib/vdpau

    mkdir -p lib/modules/2.6.32.16-alice-hlt-fep/kernel/drivers/video
    cp -a $NVDIR/kernel/nvidia.ko lib/modules/2.6.32.16-alice-hlt-fep/kernel/drivers/video

    mkdir -p usr/bin
    for bin in nvidia-bug-report.sh nvidia-debugdump nvidia-installer nvidia-settings \
	nvidia-smi nvidia-xconfig; do
	cp -a $NVDIR/$bin usr/bin
    done

    mkdir -p etc/OpenCL/vendors
    cp -a $NVDIR/nvidia.icd etc/OpenCL/vendors/

    mkdir -p usr/share/doc/$PREFIX
    for doc in LICENSE nvidia-settings.png README.txt NVIDIA_Changelog html; do
	cp -a $NVDIR/$doc usr/share/doc/$PREFIX/
    done

    mkdir -p usr/share/man/man1
    for man in nvidia-*.1.gz ; do
	cp -a $NVDIR/$man usr/share/man/man1
    done

    mkdir -p etc/modprobe.d
    echo "blacklist nouveau" > etc/modprobe.d/blacklist-nouveau.conf

    mkdir -p etc/udev/rules.d
    cat <<EOF >etc/udev/mkcudadevnodes.sh
#! /bin/bash

NoOfCards=\$(lspci | grep -i nvidia | grep "VGA\|3D" | wc -l)
let NoOfCards="NoOfCards - 1"
for i in \$(seq 0 \$NoOfCards); do
mknod -m 666 /dev/nvidia\$i c 195 \$i
chown root:video /dev/nvidia\$i
done
/bin/mknod -m 666 /dev/nvidiactl c 195 255
EOF
    chmod +x etc/udev/mkcudadevnodes.sh
    cat <<EOF >etc/udev/rules.d/86-nvidia.rules
SUBSYSTEM=="module", KERNEL=="nvidia", RUN+="/etc/udev/mkcudadevnodes.sh"
EOF

    

    popd
    pushd $DEST

    create_deb $PREFIX "" \
	"nvidia-driver" "Custom package of nvidia drivers (kernel ${KERNELVERSION}) and libs." \
	"${NVIDIADRIVERVERSION}" "depmod -a ${KERNELVERSION}"

    popd
}