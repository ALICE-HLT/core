#!/bin/bash

export CUDADIR="${HLTBASEDIR}/cuda/${CUDAVERSION}"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${CUDADIR}/lib64"
export PATH="${PATH}:${CUDADIR}/bin"


check_cuda(){
    check_binary ${CUDADIR}/lib64/libcudart.so && check_binary ${CUDADIR}/bin/nvcc
}

checkout_cuda() {
    
    test -d ${CUDADIR} && echo "nvidia-driver-${NVIDIADRIVERVERSION} already checked out." && \
	return 0
 
    mkdir -p "$CUDADIR"
    cd ${HLTBASEDIR}/cuda
    test -f ./cudatoolkit_${CUDAVERSION}_linux_64_ubuntu10.10.run || \
	wget http://developer.download.nvidia.com/compute/cuda/4_0/toolkit/cudatoolkit_${CUDAVERSION}_linux_64_ubuntu10.10.run

    chmod +x ./cudatoolkit_${CUDAVERSION}_linux_64_ubuntu10.10.run
    ./cudatoolkit_${CUDAVERSION}_linux_64_ubuntu10.10.run -- --prefix="$CUDADIR"

}

compile_cuda() {
    check_cuda && echo "cuda-${CUDAVERSION} already compiled." && \
	return 0

    return 1
}


module_cuda(){
    true
}

get_package_name_cuda() {
    echo "cuda"
}

deb_cuda() {
    DEST="$PKGDIR"
    PREFIX="$(get_package_name_cuda)"

    test -f $DEST/${PREFIX}.deb && \
    echo "Packages for $PREFIX already exist." && \
	return 0

    rm -rf $DEST/${PREFIX}

    mkdir -p $DEST/${PREFIX}
    pushd $DEST/${PREFIX}

    mkdir -p usr/local/cuda

    cp -a ${CUDADIR}/{bin,include,open64,lib64,src} usr/local/cuda/


    popd
    pushd $DEST

    create_deb $PREFIX "$(get_package_deps cuda)" \
	"cuda toolkit" "Nvidia Cuda toolkit" \
	"${CUDAVERSION}"

    popd
}