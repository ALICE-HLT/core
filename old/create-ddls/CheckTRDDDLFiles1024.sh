#!/bin/bash

BASEPATH=$1
CREATEFILES=""
FIRSTFILE=

#cd $BASEPATH

BASEPATH=`pwd`
echo $BASEPATH


for (( i=1024; i< 1042 ; i++ )) ; do
  if [ ! -f TRD_$i.ddl ] ; then
#    echo File TRD_$i.ddl not found
    CREATEFILES="$CREATEFILES TRD_$i.ddl"
  else
    if [ -z "$FIRSTFILE" ] ; then
      FIRSTFILE=TRD_$i.ddl
    fi
  fi
done

if [ -z "$FIRSTFILE" ] ; then
  echo No TRD ddl file found. Aborting
  exit -1
fi

#echo Creating missing files as empty files using template file $FIRSTFILE
for p in $CREATEFILES ; do
#  echo Creating file $p
  ${ALIHLT_DC_BINDIR}/DDLHeaderCopy $FIRSTFILE $p
done

