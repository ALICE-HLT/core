#!/bin/bash

BASEPATH=$1
CREATEFILES=""
FIRSTFILE=

cd $BASEPATH

for (( i=768; i<984 ; i++ )) ; do
  if [ ! -f TPC_$i.ddl ] ; then
#    echo File TPC_$i.ddl not found
    CREATEFILES="$CREATEFILES TPC_$i.ddl"
  else
    if [ -z "$FIRSTFILE" ] ; then
      FIRSTFILE=TPC_$i.ddl
    fi
  fi
done

if [ -z "$FIRSTFILE" ] ; then
  echo No TPC ddl file found. Aborting
  exit -1
fi

#echo Creating missing files as empty files using template file $FIRSTFILE
for p in $CREATEFILES ; do
#  echo Creating file $p
  ${ALIHLT_DC_BINDIR}/DDLHeaderCopy $FIRSTFILE $p
done

