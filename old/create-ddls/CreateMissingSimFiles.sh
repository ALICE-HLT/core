#!/bin/bash

echo  ====================== `hostname` ====================


TEST=0
BASEDIR=$1

if [ "$2" -eq "1" ] ; then
    TEST=1
fi

cd $BASEDIR

if [ ! -f $BASEDIR/raw0/TPC_768.ddl ] ; then
    DDLHeaderCopy $BASEDIR/raw1/TPC_768.ddl $BASEDIR/raw0/TPC_768.ddl
fi

dirs=`find $BASEDIR  -maxdepth 1 -type d -name "raw*"`

LASTDIR=XXX
IDX=0
IDXOK=0
for d in $dirs ; do
    let "IDX=IDX+1"
    count=`ls -1 $d |wc -l`

    if [ "$TEST" -eq "1" ] ; then
	if [ "$count" -eq "217" ] ; then
	    # ALL FILES THERE
	    let "IDXOK=IDXOK+1"
	    continue
	fi
    fi

    cd $d

    if [ "$count" -eq "217" ] ; then
	# ALL FILES THERE
	echo "$d  -> ok"
	let "IDXOK=IDXOK+1"
	continue
    elif [ "$count" -gt "217" ] ; then
	# ALL FILES THERE
	echo "ERROR to many file in $d"
	exit -1
    elif [ "$count" -le "1" ] ; then
	# NO FILES THERE
	if [ -f "$LASTDIR/TPC_768.ddl" ] ; then
	    DDLHeaderCopy $LASTDIR/TPC_768.ddl $d/TPC_768.ddl
	else
	    echo "No ddl-file in $d and $LASTDIR"
	fi
    fi
    echo "--> Creating Files for $d"
    /opt/HLT/tools/bin/CheckTPCDDLFiles768.sh $d

    cd ..

    LASTDIR=$d

done


echo $IDXOK events out of $IDX events are ok.

echo  =================== `hostname` done ==================