#!/usr/bin/env python

import os, sys, re, string

var_pattern = re.compile( "(.*)\$\(([a-zA-Z_][a-zA-Z0-9_]*)\)(.*)" )

def open_include_file( filename, includeDirs ):
    include_paths=""
    for d in includeDirs:
        try:
            include_file=open( d+"/"+filename, "r" )
            return include_file
        except:
            if include_paths != "":
                include_paths = include_paths+", "
            include_paths=include_paths+d
    print "Could not open include file '"+filename+"' in any directory from",includeDirs,"."
    print "Aborting..."
    sys.exit(1)

def add_include_file( outputFile, include_file, includePaths, vars ):
    inc_file = open_include_file( include_file, includePaths )
    line=inc_file.readline()
    while line != "":
        line=replace_vars( vars, line )
        outputFile.write( line )
        line=inc_file.readline()
    inc_file.close()


def replace_vars( vars, line ):
    match = var_pattern.search(line)
    while match != None:
        if not vars.has_key( match.group(2) ):
            print "Unknown variable '"+match.group(2)+"'."
            sys.exit(1)
        varval=vars[match.group(2)]
        line=match.group(1)+varval+match.group(3)+"\n"
        match = var_pattern.search(line)
    return line

if len(sys.argv)<=1:
    print "Usage: "+sys.argv[0]+" <config-file>"
    sys.exit( 1 )

outputDir = os.path.realpath( "." )
taskname = sys.argv[1] # TODO add date
baseDir = os.path.dirname( os.path.realpath( sys.argv[0] ) )
baseConfigDir = os.path.dirname( os.path.realpath( sys.argv[1] ) )
includePaths = [ outputDir+"/templates", baseConfigDir+"/templates", baseDir+"/templates" ]
    

option_pattern = re.compile( "^\s*([a-zA-Z0-9_]+)\s*=\s*\"(.*)\"\s*(#.*)?$" )
comment_pattern = re.compile( "^\s*#.*" )
empty_pattern = re.compile( "^\s*$" )
dcs_hostname_pattern = re.compile( "DCS_([0-9]+)_HOSTNAME" )
dcs_slave_list_pattern = re.compile( "DCS_([0-9]+)_LINKPAIR_([0-9]+)_LINK_([0-9]+)_SLAVES" )
dcs_tds_pattern = re.compile( "DCS_([0-9]+)_USE_TCPDUMPSUBSCRIBER" )
trap_cpu_pattern = re.compile( "TRAP_CPU([0-3])_MEM_(HIGH|LOW)" )

DCS_BOARD_CNT=""
DCS_BOARDS=[]
DCS_DEBUG=None

TRAP_CPU_MEM = [ [ 0, 0 ], [ 0, 0 ], [ 0, 0 ], [ 0, 0 ] ]
TRAP_CPU_MEM_STR=[ "", "", "", "" ]

PC_USE_TCPDUMP_SUBSCRIBER=None
PC_USE_HLTOUT_WRITER=None
PC_HOSTNAME=None
TRIGGER_OPT=""
GUI_SYNCHRONOUS_DEFAULT="0"

slavecount=0

vars={}

try:
    configfile=open( sys.argv[1], "r" )
except IOError:
    print "Unable to open file "+sys.argv[1]
    sys.exit( 1 )

# Parse configuration file
line=configfile.readline()
linenr=1
while line != "":
    match = option_pattern.search( line )
    if match != None:
        #print "Found option '"+match.group(1)+"' = '"+match.group(2)+"'"
        opt=match.group(1)
        val=match.group(2)
        if opt=="DCS_BOARD_CNT":
            if DCS_BOARD_CNT != "":
                print "Line %d: DCS_BOARD_CNT already set." % linenr
                sys.exit(1)
            try:
                DCS_BOARD_CNT=int(val,0)
            except:
                print "Line %d: DCS_BOARD_CNT not a number." % linenr
                sys.exit(1)
            if DCS_BOARD_CNT<=0:
                print "DCS_BOARD_CNT is zero."
                sys.exit(1)
            for i in range( DCS_BOARD_CNT ):
                DCS_BOARDS.append( { "hostname" : None, "slaves": [ [ None, None ], [ None, None ], [ None, None ], [ None, None ] ], "tds": None, "slave_opts" : "" } )
        elif dcs_hostname_pattern.search( opt ) != None:
            match = dcs_hostname_pattern.search( opt )
            dcs_nr = int(match.group(1),0)
            if dcs_nr >= DCS_BOARD_CNT:
                print "Line %d: Board number %d larger than DCS_BOARD_CNT of %d" % ( linenr, dcs_nr, DCS_BOARD_CNT )
                sys.exit(1)
            if DCS_BOARDS[dcs_nr]["hostname"]!=None:
                print "Line %d: DCS_BOARD %d hostname already set." ( linenr, dcs_nr )
                sys.exit(1)
            DCS_BOARDS[dcs_nr]["hostname"]=val
        elif dcs_slave_list_pattern.search(line)!=None:
            match=dcs_slave_list_pattern.search(line)
            dcs_nr = int(match.group(1),0)
            if dcs_nr >= DCS_BOARD_CNT:
                print "Line %d: Board number %d larger than DCS_BOARD_CNT of %d" % ( linenr, dcs_nr, DCS_BOARD_CNT )
                sys.exit(1)
            linkpair = int(match.group(2),0)
            if linkpair>3:
                print "Line %d: Link pair number %d larger than allowed maximum of 3" % ( linenr, linkpair )
                sys.exit(1)
            link = int(match.group(3),0)
            if link>1:
                print "Line %d: Link number %d larger than allowed maximum of 1" % ( linenr, link )
                sys.exit(1)
            if DCS_BOARDS[dcs_nr]["slaves"][linkpair][link]!=None:
                print "Line %d: DCS_BOARD %d linkpair %d link %d slave list already set." % ( linenr, dcs_nr, linkpair, link )
                sys.exit(1)
            DCS_BOARDS[dcs_nr]["slaves"][linkpair][link] = []
            n=0
            for s in string.split( val ):
                try:
                    DCS_BOARDS[dcs_nr]["slaves"][linkpair][link].append( int( s,0 ) )
                    slavecount=slavecount+1
                except ValueError:
                    print "Line %d: DCS_BOARD %d linkpair %d link %d slave ID %d invalid." % ( linenr, dcs_nr, linkpair, link, n )
                    sys.exit(1)
                n=n+1
        elif trap_cpu_pattern.search(line)!=None:
            match = trap_cpu_pattern.search(line)
            cpu_nr = int(match.group(1),0)
            if match.group(2) == "HIGH":
                border = 1
            else:
                border=0
            try:
                use = int(val,0)
            except ValueError:
                print "Line %d: TRAP CPU %d %s memory %s not a number." % ( linenr, cpu_nr, match.group(2), val )
                sys.exit(1)
            TRAP_CPU_MEM[cpu_nr][border] = use
        elif dcs_tds_pattern.search(line)!=None:
            match=dcs_tds_pattern.search(line)
            dcs_nr = int(match.group(1),0)
            if dcs_nr >= DCS_BOARD_CNT:
                print "Line %d: Board number %d larger than DCS_BOARD_CNT of %d" % ( linenr, dcs_nr, DCS_BOARD_CNT )
                sys.exit(1)
            if DCS_BOARDS[dcs_nr]["tds"] != None:
                print "Line %d: DCS_BOARD %d TCPDumpSubscriber already set." ( linenr, dcs_nr )
                sys.exit(1)
            try:
                use=int(val,0)
            except:
                print "Line %d: TCPDumpSubscriber selection must be 0 or 1." % linenr
                sys.exit(1)
            if use != 1 and use != 0:
                print "Line %d: TCPDumpSubscriber selection must be 0 or 1." % linenr
                sys.exit(1)
            DCS_BOARDS[dcs_nr]["tds"]=use
        elif opt == "DCS_DEBUG":
            if val!="1" and val!="0":
                print "Line %d: DCS_BOARD debug must be 1 or 0." ( linenr, dcs_nr )
                sys.exit(1)
            if DCS_DEBUG!=None:
                print "Line %d: DCS_BOARD debug already set." ( linenr, dcs_nr )
                sys.exit(1)
            DCS_DEBUG=int(val,0)
        elif opt=="PC_USE_TCPDUMP_SUBSCRIBER":
            try:
                use=int(val,0)
            except:
                print "Line %d: TCPDumpSubscriber selection must be 0 or 1." % linenr
                sys.exit(1)
            if use != 1 and use != 0:
                print "Line %d: TCPDumpSubscriber selection must be 0 or 1." % linenr
                sys.exit(1)
            if PC_USE_TCPDUMP_SUBSCRIBER!=None:
                print "Line %d: PC_USE_TCPDUMP_SUBSCRIBER already set." ( linenr )
                sys.exit(1)
            PC_USE_TCPDUMP_SUBSCRIBER=use
        elif opt=="PC_HOSTNAME":
            if PC_HOSTNAME!=None:
                print "Line %d: PC_HOSTNAME already set." ( linenr )
                sys.exit(1)
            PC_HOSTNAME = val
        elif opt=="PC_USE_HLTOUT_WRITER":
            try:
                use=int(val,0)
            except:
                print "Line %d: PC_USE_HLTOUT_WRITER selection must be 0 or 1." % linenr
                sys.exit(1)
            if use != 1 and use != 0:
                print "Line %d: PC_USE_HLTOUT_WRITER selection must be 0 or 1." % linenr
                sys.exit(1)
            if PC_USE_HLTOUT_WRITER!=None:
                print "Line %d: PC_USE_HLTOUT_WRITER already set." ( linenr )
                sys.exit(1)
            PC_USE_HLTOUT_WRITER=use
        elif opt=="USE_TRIGGER":
            use=0
            try:
                use=int(val,0)
            except:
                print "Line %d: USE_TRIGGER selection must be 0 or 1." % linenr
                sys.exit(1)
            if use == 0:
                TRIGGER_OPT="-notrigger"
        else:
            vars[opt]=val
    elif comment_pattern.search(line)!=None or empty_pattern.search(line)!=None:
        pass
    else:
        print "Line %d: Wrong format" % (linenr)
        sys.exit(1)
    line=configfile.readline()
    linenr=linenr+1

# Check options from configuration file
for i in range(DCS_BOARD_CNT):
    if DCS_BOARDS[i]["hostname"] == None:
        print "DCS_BOARD %d hostname must be set." % (i)
        sys.exit(1)
    found_slaves=0
    for lp in range(4):
        for l in range(2):
            if DCS_BOARDS[i]["slaves"][lp][l] != None and len(DCS_BOARDS[i]["slaves"][lp][l])>0:
                found_slaves=1
    if not found_slaves:
        print "No slaves specified for DCS_BOARD %d." % ( i)
        sys.exit(1)
    if DCS_BOARDS[i]["tds"] != 1 and PC_USE_HLTOUT_WRITER!=1 and PC_USE_TCPDUMP_SUBSCRIBER!=1:
        print "Data from DCS_BOARD %d is not used in any way." % ( i)
        sys.exit(1)
    for lp in range( 4 ):
        for l in range( 2 ):
            if DCS_BOARDS[i]["slaves"][lp][l] != None:
                for id in DCS_BOARDS[i]["slaves"][lp][l]:
                    DCS_BOARDS[i]["slave_opts"] = DCS_BOARDS[i]["slave_opts"]+(" -slave %u %u %u" % ( lp, l, id ))
if DCS_DEBUG and not vars.has_key( "DCS_DEBUG_MAX_EVENT_SIZE" ):
    print "DCS_DEBUG_MAX_EVENT_SIZE is not specified although DCS_DEBUG is activated."
    sys.exit(1)
    

MAX_SIZE=0
if DCS_DEBUG!=1:
    for i in range( 4 ):
        for b in range( 2 ):
            if TRAP_CPU_MEM[i][b] == 0:
                if b==0:
                    border="LOW"
                else:
                    border="HIGH"
                print "TRAP CPU %d %s memory is zero or not specified." % ( i, border )
                sys.exit(1)
        if TRAP_CPU_MEM[i][0] > TRAP_CPU_MEM[i][1]:
            print "TRAP CPU %d LOW memory is larger than HIGH memory." % ( i )
            sys.exit(1)
        TRAP_CPU_MEM_STR[i] = hex(TRAP_CPU_MEM[i][0])+" "+hex(TRAP_CPU_MEM[i][1])
        MAX_SIZE = MAX_SIZE + (TRAP_CPU_MEM[i][1]-TRAP_CPU_MEM[i][0])*2 # is words
    MAX_SIZE = MAX_SIZE * slavecount
else:
    if vars["DCS_DEBUG_MAX_EVENT_SIZE"][-1]=="G":
        val=vars["DCS_DEBUG_MAX_EVENT_SIZE"][:-1]
        mult=1024*1024*1024
    elif vars["DCS_DEBUG_MAX_EVENT_SIZE"][-1]=="M":
        val=vars["DCS_DEBUG_MAX_EVENT_SIZE"][:-1]
        mult=1024*1024
    elif vars["DCS_DEBUG_MAX_EVENT_SIZE"][-1]=="k":
        val=vars["DCS_DEBUG_MAX_EVENT_SIZE"][:-1]
        mult=1024
    else:
        val=vars["DCS_DEBUG_MAX_EVENT_SIZE"]
        mult=1
    try:
        MAX_SIZE=int(val,0)*mult
    except ValueError:
        print "DCS_DEBUG_MAX_EVENT_SIZE is not a number (suffixes k, M, and G are allowed)."
        sys.exit(1)

MAX_SIZE = MAX_SIZE + 2048

if not vars.has_key("TRAPPUBLISHER_SYSV_SHM_SIZE"):
    print "TRAPPUBLISHER_SYSV_SHM_SIZE is not specified."
    sys.exit(1)
if vars["TRAPPUBLISHER_SYSV_SHM_SIZE"][-1]=="G":
    val=vars["TRAPPUBLISHER_SYSV_SHM_SIZE"][:-1]
    mult=1024*1024*1024
elif vars["TRAPPUBLISHER_SYSV_SHM_SIZE"][-1]=="M":
    val=vars["TRAPPUBLISHER_SYSV_SHM_SIZE"][:-1]
    mult=1024*1024
elif vars["TRAPPUBLISHER_SYSV_SHM_SIZE"][-1]=="k":
    val=vars["TRAPPUBLISHER_SYSV_SHM_SIZE"][:-1]
    mult=1024
else:
    val=vars["TRAPPUBLISHER_SYSV_SHM_SIZE"]
    mult=1
try:
    #print "'"+val+"'"
    int(val,0)
except ValueError:
    print "TRAPPUBLISHER_SYSV_SHM_SIZE is not a number (suffixes k, M, and G are allowed)."
    sys.exit(1)

# preliminaries
vars["MAX_EVENT_SIZE"] = hex(MAX_SIZE)
block_cnt = ((int(val,0)*mult) / MAX_SIZE)+1
bit=1
cnt=0
while bit < block_cnt and bit>0:
    bit = bit << 1
    cnt = cnt + 1
vars["EVENTSEXP2"] = "%u" % cnt

if PC_HOSTNAME==None:
    print "PC_HOSTNAME is not specified."
    sys.exit(1)

vars["task_name"]=taskname
vars["pc"] = PC_HOSTNAME
vars["PC_HOSTNAME"] = PC_HOSTNAME
vars["TRIGGER_OPT"] = TRIGGER_OPT

TMoutputFilename_base = outputDir+"/"+taskname+"-"+PC_HOSTNAME+"-PC.xml"
DCSsh_outputFilename_base = outputDir+"/"+taskname

# create TaskManager XML configuration file
try:
    outputFile=open(TMoutputFilename_base, "w" )
except IOError:
    print "Cannot create output file '"+TMoutputFilename_base+"'."
    sys.exit(1)
    
vars["CONFIGFILE"]=TMoutputFilename_base
add_include_file( outputFile, "main1.xml", includePaths, vars )

merger_subscriptions=""
for i in range( DCS_BOARD_CNT ):
    outputFile.write( "SourceIDs.append( \"DCS%03d-TRAPPublisher\" )\n" % i )
    outputFile.write( "Proxies[\"DCS%03d-TRAPPublisher\"]=1\n" % i )
    if PC_USE_HLTOUT_WRITER==1 or PC_USE_TCPDUMP_SUBSCRIBER==1:
        outputFile.write( "ConnectCmdIDs.append( \"DCS%03d-Bridge-SBH\" )\n" % i )
        outputFile.write( "Proxies[\"DCS%03d-Bridge-SBH\"]=1\n" % i )
        outputFile.write( "ConnectNoCmdIDs.append( \"DCS%03d-Bridge-PBH\" )\n" % i )
        merger_subscriptions=merger_subscriptions + ( " -subscribe DCS%03d-Bridge-PBHPub Merger-%03dSub" % ( i, i ) )
    if DCS_BOARDS[i]["tds"]==1:
        outputFile.write( "ProcSinkIDs.append( \"DCS%03d-TDS\" )\n" % i )
        outputFile.write( "Proxies[\"DCS%03d-TDS\"]=1\n" % i )

outputFile.write( "\n\n" )
if PC_USE_HLTOUT_WRITER==1 or PC_USE_TCPDUMP_SUBSCRIBER==1:
    outputFile.write( "ProcSinkIDs.append( \"Merger\" )\n" )

if PC_USE_TCPDUMP_SUBSCRIBER==1:
    outputFile.write( "ProcSinkIDs.append( \"TDS\" )\n" )

if PC_USE_HLTOUT_WRITER==1:
    outputFile.write( "ProcSinkIDs.append( \"HLTOutWriterSubsciber\" )\n" )

#print merger_subscriptions

outputFile.write( "\n\n" )

add_include_file( outputFile, "main2.xml", includePaths, vars )

for i in range( DCS_BOARD_CNT ):
    outputFile.write( "\n\n\n" )
    vars["NR"] = "%03d" % i
    vars["DCSNODE"] = DCS_BOARDS[i]["hostname"]
    add_include_file( outputFile, "TRAPPublisher1.xml", includePaths, vars )
    if PC_USE_HLTOUT_WRITER==1 or PC_USE_TCPDUMP_SUBSCRIBER==1:
        outputFile.write( "children.append( \"DCS%03d-Bridge-SBH\" )\n" % i )
        outputFile.write( "child_subscribe_keyword[\"DCS%03d-Bridge-SBH\"] = \"Configured\"\n" % i )
    if DCS_BOARDS[i]["tds"]==1:
        outputFile.write( "children.append( \"DCS%03d-TDS\" )\n" % i )
        outputFile.write( "child_subscribe_keyword[\"DCS%03d-TDS\"] = \"Configured\"\n" % i )
    add_include_file( outputFile, "TRAPPublisher2.xml", includePaths, vars )
    if DCS_BOARDS[i]["tds"]==1:
        outputFile.write( "\n\n" )
        add_include_file( outputFile, "DCS-TDS.xml", includePaths, vars )
    if PC_USE_HLTOUT_WRITER==1 or PC_USE_TCPDUMP_SUBSCRIBER==1:
        outputFile.write( "\n\n" )
        add_include_file( outputFile, "DCS-SBH.xml", includePaths, vars )
        outputFile.write( "\n\n" )
        add_include_file( outputFile, "PC-PBH1.xml", includePaths, vars )
        for n in range( 0, i ):
            outputFile.write( "inlaws.append( \"DCS%03d-Bridge-PBH\" )\n" % n )
        for n in range( i+1, DCS_BOARD_CNT ):
            outputFile.write( "inlaws.append( \"DCS%03d-Bridge-PBH\" )\n" % n )
        add_include_file( outputFile, "PC-PBH2.xml", includePaths, vars )
        
    del vars["NR" ]
    del vars["DCSNODE"]
    

outputFile.write( "\n\n\n" )
if PC_USE_HLTOUT_WRITER==1 or PC_USE_TCPDUMP_SUBSCRIBER==1:
    vars["MERGER_SUBSCRIPTION_LIST"] = merger_subscriptions
    add_include_file( outputFile, "Merger1.xml", includePaths, vars )
    if PC_USE_TCPDUMP_SUBSCRIBER==1:
        outputFile.write( "children.append( \"TDS\" )\n" )
        outputFile.write( "child_subscribe_keyword[\"TDS\"] = \"Configured\"\n" )
    if PC_USE_HLTOUT_WRITER==1:
        outputFile.write( "children.append( \"HLTOutWriterSubscriber\" )\n" )
        outputFile.write( "child_subscribe_keyword[\"HLTOutWriterSubscriber\"] = \"Configured\"\n" )
    for i in range( DCS_BOARD_CNT ):
        outputFile.write( "parents.append( \"DCS%03d-Bridge-PBH\" )\n" % i )
    add_include_file( outputFile, "Merger2.xml", includePaths, vars )
    del vars["MERGER_SUBSCRIPTION_LIST"]


if PC_USE_TCPDUMP_SUBSCRIBER==1:
    outputFile.write( "\n\n\n" )
    add_include_file( outputFile, "PC-TDS.xml", includePaths, vars )

if PC_USE_HLTOUT_WRITER==1:
    outputFile.write( "\n\n\n" )
    add_include_file( outputFile, "HLTOutWriterSubscriber.xml", includePaths, vars )

outputFile.write( "\n\n\n" )
add_include_file( outputFile, "main3.xml", includePaths, vars )
for i in range( DCS_BOARD_CNT ):
    outputFile.write( "SourceIDs.append( \"DCS%03d-TRAPPublisher\" )\n" % i )
    outputFile.write( "Proxies[\"DCS%03d-TRAPPublisher\"]=1\n" % i )
    if PC_USE_HLTOUT_WRITER==1 or PC_USE_TCPDUMP_SUBSCRIBER==1:
        outputFile.write( "ConnectCmdIDs.append( \"DCS%03d-Bridge-SBH\" )\n" % i )
        outputFile.write( "Proxies[\"DCS%03d-Bridge-SBH\"]=1\n" % i )
        outputFile.write( "ConnectNoCmdIDs.append( \"DCS%03d-Bridge-PBH\" )\n" % i )
        merger_subscriptions=merger_subscriptions + ( " -subscribe DCS%03d-Bridge-PBHPub Merger-%03dSub" % ( i, i ) )
    if DCS_BOARDS[i]["tds"]==1:
        outputFile.write( "ProcSinkIDs.append( \"DCS%03d-TDS\" )\n" % i )
        outputFile.write( "Proxies[\"DCS%03d-TDS\"]=1\n" % i )

outputFile.write( "\n\n" )
if PC_USE_HLTOUT_WRITER==1 or PC_USE_TCPDUMP_SUBSCRIBER==1:
    outputFile.write( "ProcSinkIDs.append( \"Merger\" )\n" )

if PC_USE_TCPDUMP_SUBSCRIBER==1:
    outputFile.write( "ProcSinkIDs.append( \"TDS\" )\n" )

if PC_USE_HLTOUT_WRITER==1:
    outputFile.write( "ProcSinkIDs.append( \"HLTOutWriterSubsciber\" )\n" )
add_include_file( outputFile, "main4.xml", includePaths, vars )
    
outputFile.close()

# further preliminaries
if PC_USE_HLTOUT_WRITER==1 or PC_USE_TCPDUMP_SUBSCRIBER==1:
    vars["START_SBH"] = "1"
else:
    vars["START_SBH"] = "0"
vars["TRAP_CPU0_MEM"] = TRAP_CPU_MEM_STR[0]
vars["TRAP_CPU1_MEM"] = TRAP_CPU_MEM_STR[1]
vars["TRAP_CPU2_MEM"] = TRAP_CPU_MEM_STR[2]
vars["TRAP_CPU3_MEM"] = TRAP_CPU_MEM_STR[3]

# Create script start and kill files for DCS boards.
for i in range( DCS_BOARD_CNT ):
    if DCS_BOARDS[i]["tds"]==1:
        vars["START_TDS"] = "1"
    else:
        vars["START_TDS"] = "0"
    try:
        outputFile=open(DCSsh_outputFilename_base+"-"+DCS_BOARDS[i]["hostname"]+"-"+("DCS%03d" % i)+".sh", "w" )
    except IOError:
        print "Cannot create output file '"+DCSsh_outputFilename_base+"-"+DCS_BOARDS[i]["hostname"]+"-"+("DCS%03d" % i)+".sh"+"'."
        sys.exit(1)
    vars["NR"] = "%03d" % i
    vars["DCSNODE"] = DCS_BOARDS[i]["hostname"]
    vars["SLV_OPTS"] = DCS_BOARDS[i]["slave_opts"]
    if DCS_DEBUG==1:
        vars["USE_FILEPUBLISHER"] = "1"
    else:
        vars["USE_FILEPUBLISHER"] = "0"
    add_include_file( outputFile, "DCS.sh", includePaths, vars )
    outputFile.close()
    os.system( "chmod a+x "+DCSsh_outputFilename_base+"-"+DCS_BOARDS[i]["hostname"]+"-"+("DCS%03d" % i)+".sh" )

    try:
        outputFile=open(DCSsh_outputFilename_base+"-kill-"+DCS_BOARDS[i]["hostname"]+"-"+("DCS%03d" % i)+".sh", "w" )
    except IOError:
        print "Cannot create output file '"+DCSsh_outputFilename_base+"-kill-"+DCS_BOARDS[i]["hostname"]+"-"+("DCS%03d" % i)+".sh"+"'."
        sys.exit(1)
    add_include_file( outputFile, "dcs-kill.sh", includePaths, vars )
    outputFile.close()
    os.system( "chmod a+x "+DCSsh_outputFilename_base+"-kill-"+DCS_BOARDS[i]["hostname"]+"-"+("DCS%03d" % i)+".sh" )

    del vars["NR" ]
    del vars["DCSNODE"]
    del vars["SLV_OPTS"]
    del vars["START_TDS"]
    
del vars["START_SBH"]
del vars["TRAP_CPU0_MEM"]
del vars["TRAP_CPU1_MEM"]
del vars["TRAP_CPU2_MEM"]
del vars["TRAP_CPU3_MEM"]


# Create script start file for PC
try:
    outputFile=open(taskname+"-"+PC_HOSTNAME+"-PC.sh", "w" )
except IOError:
    print "Cannot create output file '"+taskname+"-"+PC_HOSTNAME+"-PC.sh"
    sys.exit(1)
add_include_file( outputFile, "pc.sh", includePaths, vars )

outputFile.close()
os.system( "chmod a+x "+taskname+"-"+PC_HOSTNAME+"-PC.sh" )

# Create script kill file for PC
try:
    outputFile=open(taskname+"-kill-"+PC_HOSTNAME+"-PC.sh", "w" )
except IOError:
    print "Cannot create output file '"+taskname+"-kill-"+PC_HOSTNAME+"-PC.sh"
    sys.exit(1)
add_include_file( outputFile, "pc-kill.sh", includePaths, vars )

outputFile.close()
os.system( "chmod a+x "+taskname+"-kill-"+PC_HOSTNAME+"-PC.sh" )
