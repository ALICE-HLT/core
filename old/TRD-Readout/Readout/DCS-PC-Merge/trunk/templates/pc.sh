#!/bin/bash

GUI_SYNCHRONOUS=$(GUI_SYNCHRONOUS_DEFAULT)
GUI_ONLY=0

for o in $* ; do 

#echo $o
if [ $o = "-synchronous-gui" ] ; then
    GUI_SYNCHRONOUS=1
elif [ $o = "-asynchronous-gui" ] ; then
    GUI_SYNCHRONOUS=0
elif [ $o = "-gui-only" ] ; then
    GUI_ONLY=1
else
    echo "Unknown option '"$o"'" >&2
    exit 1
fi

done

if [ ${GUI_ONLY} -ne 1 ] ; then
    $(PC_BIN_BASEDIR)/kip/TaskManager/bin/Linux-i686/TaskManager $(CONFIGFILE) -V 0x39 -nostdout &
else
    GUI_SYNCHRONOUS=0
fi

sleep 1

if [ ${GUI_SYNCHRONOUS} -eq 1 ] ; then
    $(PC_BIN_BASEDIR)/kip/TMGUI/TMGUI.py -address tcpmsg://$(pc):34000 -config $(CONFIGFILE) -unconnected || exit 1
    killall TaskManager TCPDumpSubscriber PublisherBridgeHeadNew EventMergerNew TRDDataHLTOutWriter
    sleep 2
    killall -9 TaskManager TCPDumpSubscriber PublisherBridgeHeadNew EventMergerNew TRDDataHLTOutWriter
else
    $(PC_BIN_BASEDIR)/kip/TMGUI/TMGUI.py -address tcpmsg://$(pc):34000 -config $(CONFIGFILE) -unconnected &
fi


