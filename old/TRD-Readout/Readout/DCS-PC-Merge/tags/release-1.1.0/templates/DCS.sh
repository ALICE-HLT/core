#!/bin/bash

START_TCPDUMPSUBSCRIBER=$(START_TDS)
START_SUBSCRIBERBRIDGEHEAD=$(START_SBH)
DEBUG_USE_FILEPUBLISHER=$(USE_FILEPUBLISHER)

DEBUG=0

check_state()
{
if [[ -z "$1" ]] ; then
    echo "check_state: first parameter (remote address) has zero length."
    return -1
fi
if [[ -z "$2" ]] ; then
    echo "check_state: second parameter (state to check) has zero length."
    return -1
fi
if [[ -z "$3" ]] ; then
    MAXCNT=30
else
    MAXCNT=$3
fi

CNT=0
STATE_OK=""
while [ -z "$STATE_OK" -a $CNT -lt ${MAXCNT} ] ; do 
    TRAP_STATE=`$(DCS_BIN_BASEDIR)/ComponentInterface -address tcpmsg://$(DCSNODE):21000 -status $1 2>/dev/null`
    if [ $DEBUG -eq 1 ] ; then echo "check_state: $1 ($2): $TRAP_STATE - $CNT" ; fi
    echo -n .
    STATE_OK=`echo $TRAP_STATE|grep $2`
    if [ -z "$STATE_OK" ] ; then
	sleep 1
    fi
    let CNT=CNT+1
done
if [ -z "$STATE_OK" ] ; then
    return 0
fi
return 1
}

echo -n Starting programs" "
if [ ${DEBUG_USE_FILEPUBLISHER} -ne 1 ] ; then 
    $(DCS_BIN_BASEDIR)/TRAPPublisher -control tcpmsg://$(DCSNODE):20000 -publisher TRAPPublisher -V 0x3F -minblocksize $(MAX_EVENT_SIZE) -shm sysv $(TRAPPUBLISHER_SYSV_SHM_ID) $(TRAPPUBLISHER_SYSV_SHM_SIZE) -cpumem 0 $(TRAP_CPU0_MEM) -cpumem 1 $(TRAP_CPU1_MEM) -cpumem 2 $(TRAP_CPU2_MEM) -cpumem 3 $(TRAP_CPU3_MEM) $(SLV_OPTS) -sleep -sleeptime 10000 -eventupdatetimeout 10 $(TRIGGER_OPT) -nostdout -name DCS$(NR)-TRAPPublisher &
else
    $(DCS_BIN_BASEDIR)/ControlledFilePublisher -control tcpmsg://$(DCSNODE):20000 -publisher TRAPPublisher -V 0x3F -minblocksize $(MAX_EVENT_SIZE) -shm sysv $(TRAPPUBLISHER_SYSV_SHM_ID) $(TRAPPUBLISHER_SYSV_SHM_SIZE) -file data-DCS$(NR).data -nostdout -name DCS$(NR)-TRAPPublisher &
fi
echo -n .
sleep 1

if [ ${START_TCPDUMPSUBSCRIBER} -eq 1 ] ; then
    $(DCS_BIN_BASEDIR)/TCPDumpSubscriber -control tcpmsg://$(DCSNODE):20001/ -V 0x3F -minblocksize 4k -shm sysv $(TCPDUMPSUBSCRIBER_SYSV_SHM_ID) 4k -port 42000 -subscribe TRAPPublisher TDS -nostdout -name DCS$(NR)-TDS &
    echo -n .
    sleep 1
fi

if [ ${START_SUBSCRIBERBRIDGEHEAD} -eq 1 ] ; then
    $(DCS_BIN_BASEDIR)/SubscriberBridgeHeadNew -control tcpmsg://$(DCSNODE):20002/ -V 0x3F -subscribe TRAPPublisher TRAPBridge -nostdout -msg tcpmsg://$(DCSNODE):1$(NR)0 tcpmsg://$(pc):1$(NR)0 -blobmsg tcpmsg://$(DCSNODE):1$(NR)1 tcpmsg://$(pc):1$(NR)1 -blob 4096 tcpblob://$(DCSNODE):1$(NR)2 -minblocksize $(MAX_EVENT_SIZE) -eventsexp2 $(EVENTSEXP2) -name TRAPBridge-DCS$(NR) & 
    echo -n .
    sleep 1
fi

check_state tcpmsg://$(DCSNODE):20000 "Started"
STATE=$?
if [[ $STATE -eq 0 ]] ; then
    echo "Could not start TRAPPulisher on DCS $(DCSNODE)"
    return 0
fi
echo -n .

if [ ${START_TCPDUMPSUBSCRIBER} -eq 1 ] ; then
    check_state tcpmsg://$(DCSNODE):20001 "Started"
    STATE=$?
    if [[ $STATE -eq 0 ]] ; then
	echo "Could not start TCPDumpSubscriber on DCS $(DCSNODE)"
	return 0
    fi
    echo -n .
fi

if [ ${START_SUBSCRIBERBRIDGEHEAD} -eq 1 ] ; then
    check_state tcpmsg://$(DCSNODE):20002 "Started"
    STATE=$?
    if [[ $STATE -eq 0 ]] ; then
	echo "Could not start SubscriberBridgeHead on DCS $(DCSNODE)"
	return 0
    fi
    echo -n .
fi

echo " "Done

