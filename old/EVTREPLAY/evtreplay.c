/*
   H-RORC Event Replay Utility, Version 1

   Florian Painke (fp) <painke@kip.uni-heidelberg.de>

   history:
   2008-01-17 initial implementation (fp)
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <asm/errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <time.h>
#include <termios.h>
#include <linux/pci.h>

#include <psi.h>
#include <psi_error.h>



/* version string */
#define VERSION "0.10"

/* message verbosity */
#define QUIET	0
#define DEFAULT	1
#define VERBOSE	2

/* directory seperator */
#define DIRSEP_CHAR '/'
#define DIRSEP_STRING "/"

/* error codes */
#ifndef SUCCESS
#define SUCCESS 0
#endif
#define EMISSING   1000 /* Missing option argument */
#define ENOCMD     1001 /* No command given */
#define EMULTCMDS  1002 /* Multiple commands */
#define EINVALCRD  1003 /* Invalid card index */
#define EINVALLNK  1004 /* Invalid link index */
#define EINVALOFF  1005 /* Invalid offset */
#define EINVALENT  1006 /* Invalid entry index */
#define EINVALWAIT 1007 /* Invalid wait cycles */
#define EINVALMODE 1008 /* Invalid mode */
#define EMULTLEVLS 1009 /* Multiple verbosity levels */

/* H-RORC identification */
#define VENDOR_ID		0x10EE
#define DEVICE_ID		0xDEAD
#define MAX_INDEX		31
#define MAX_DEVICE_PATH		256

/* links */
#define MAX_LINK		1
static int LINK_BAR[MAX_LINK+1] = { 2, 3 };

/* ram size */
#define MAX_OFFSET		0x00FFFFFFul

/* registers */
#define RAM_ADDR_REG		0x1000
#define RAM_CTRL_REG		0x1004
#define RAM_DATA_IN_REG		0x1008
#define RAM_DATA_OUT_REG	0x100C
#define REPLAY_CTRL_REG		0x1014
#define ACCESS_CTRL_REG		0x1018

/* ram control register bits */
#define RAM_CTRL_READ		0x01
#define RAM_CTRL_FRAME		0x02

/* frame types */
#define RAM_READ_FRAME		(RAM_CTRL_READ | RAM_CTRL_FRAME)
#define RAM_WRITE_FRAME		RAM_CTRL_FRAME

/* replay control register bits */
#define REPLAY_CTRL_ERROR	0x80000000ul
#define REPLAY_CTRL_ENABLE	0x40000000ul
#define REPLAY_CTRL_CONT	0x20000000ul
#define REPLAY_CTRL_ONESHOT	0x10000000ul

/* replay control register masks */
#define REPLAY_CTRL_STATUS	0xF0000000ul
#define REPLAY_CTRL_MODE	0x70000000ul
#define REPLAY_CTRL_WAIT	0x0FFFFFFFul

/* replay modes */
#define REPLAY_MODE_SINGLE	(REPLAY_CTRL_ENABLE)
#define REPLAY_MODE_CONT	(REPLAY_CTRL_ENABLE | REPLAY_CTRL_CONT)
#define REPLAY_MODE_ONESHOT	(REPLAY_CTRL_ENABLE | REPLAY_CTRL_ONESHOT)

/* access control register bits */
#define ACCESS_RAM_DRC		0x80000000ul
#define ACCESS_FLOW_CTRL	0x40000000ul

/* access modes */
#define ACCESS_PCI		0
#define ACCESS_DRC		(ACCESS_RAM_DRC | ACCESS_FLOW_CTRL)

/* wait cycles */
#define MAX_WAIT		0x0FFFFFFFul

/* event descriptor entires */
#define MAX_ENTRY		511

/* entry masks and flags */
#define ENTRY_FLAGS		0xFF000000ul
#define ENTRY_ADDR		0x00FFFFFFul
#define ENTRY_VALID		0x80000000ul



/*
*** utility functions *********************************************************
*/



/* busy status display */
void busy( unsigned long current, unsigned long total )
{
  static const char flip[] = "|/-\\";
  static int last_percent = 100, i = 0;
  int percent = (current * 100 + 50)/total;

  if ( percent != last_percent ) {
    printf( "\r[%c] Please wait... (%d%% done)", flip[i], percent );
    fflush( stdout );

    i = (i + 1) % 4;
    last_percent = percent;
  }
}



/* clear busy status display */
void busy_clear( void )
{
  printf( "\r                             \r" );
  fflush( stdout );
}



/* set terminal to non-canonical mode */
void set_term_nc( struct termios * term_settings )
{
  struct termios settings;

  /* get current settings */
  tcgetattr( 0, term_settings );
  settings = (* term_settings);
     
  /* disable canonical mode, and set buffer size to 1 byte */
  settings.c_lflag &= (~ICANON);
  settings.c_cc[VTIME] = 0;
  settings.c_cc[VMIN] = 1;   
  tcsetattr( 0, TCSANOW, & settings );
}

/* reset terminal */     
void reset_term( struct termios * term_settings )
{
  tcsetattr( 0, TCSANOW, term_settings);
}



/* ask yes/no */
int ask_yn( const char * message )
{
  struct termios ts;
  int c;

  /* set terminal to non-canonical mode */
  set_term_nc( & ts );

  /* print message */
  if ( message != NULL )
    printf( "%s\n", message );
  printf( "Do you want to proceed? (y/n)  " );
  // fflush( stdout );

  /* accept only yYnN */
  do {
    printf( "\b \b" );
    c = getchar();
  } while ( c != 'y' && c != 'Y' && c != 'n' && c != 'N' );

  /* reset terminal mode */
  reset_term( & ts );
  printf( "\n" );

  if ( c != 'y' && c != 'Y' ) return EPERM;

  return SUCCESS;
}



/* safe calculate string length */
size_t strnlen( const char * buf, size_t max )
{
  size_t len = 0;

  while ( max-- && buf[len] != '\0' ) ++len;

  return len;
}



/*
*** access control functions **************************************************
*/



/* set access control mode */
int set_access( tRegion region, u_int32_t mode )
{
  int result;

  result = PSI_write( region, ACCESS_CTRL_REG, 4, 4, & mode );
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to set access control mode: %s.\n",
             PSI_strerror( result ) );

  return result;
}

/* get access control mode */
int get_access( tRegion region, u_int32_t * mode )
{
  int result;
  u_int32_t status;

  result = PSI_read( region, ACCESS_CTRL_REG, 4, 4, & status );
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to get access control mode: %s.\n", 
             PSI_strerror( result ) );
  else {
    if ( mode ) (* mode) = status;
  }

  return result;
}



/*
*** low level event buffer functions ******************************************
*/



/* start frame */
int start_frame( tRegion region, u_int32_t type, u_int32_t addr )
{
  int result;

  result = PSI_write( region, RAM_ADDR_REG, 4, 4, & addr );
  if ( result == SUCCESS )
    result = PSI_write( region, RAM_CTRL_REG, 4, 4, & type ); 
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to start frame: %s.\n",
             PSI_strerror( result ) );

  return result;
}

/* write frame data */
int write_frame_data( tRegion region, u_int32_t * data )
{
  int result;

  result = PSI_write( region, RAM_DATA_IN_REG, 4, 4, data );
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to write frame data: %s.\n",
             PSI_strerror( result ) );

  return result;
}

/* end frame */
int end_frame( tRegion region )
{
  int result;
  u_int32_t ctrl = 0;
  
  result = PSI_write( region, RAM_CTRL_REG, 4, 4, & ctrl ); 
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to end frame: %s.\n",
             PSI_strerror( result ) );

  return result;
}

/* get event descriptor entry */
int get_entry( tRegion region, unsigned int entry,
               u_int32_t * offs, u_int32_t * size )
{
  int result;
  u_int32_t o, s;

  result = PSI_read( region, entry * 8, 4, 4, & o );
  if ( result == SUCCESS )
    result = PSI_read( region, entry*8 + 4, 4, 4, & s );
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to read event descriptor entry: %s.\n",
             PSI_strerror( result ) );
  else {
    if ( (o & ENTRY_VALID) && (s & ENTRY_VALID) ) {
      if ( offs ) (* offs) = o;
      if ( size ) (* size) = s;
    }
    else {
      if ( offs ) (* offs) = 0;
      if ( size ) (* size) = 0;
    }
  }

  return result;
}

/* set event descriptor entry */
int set_entry( tRegion region, unsigned int entry,
               u_int32_t offs, u_int32_t size )
{
  int result;

  offs |= ENTRY_VALID; size |= ENTRY_VALID;
  result = PSI_write( region, entry * 8, 4, 4, & offs );
  if ( result == SUCCESS )
    result = PSI_write( region, entry*8 + 4, 4, 4, & size );
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to set event descriptor entry: %s.\n",
             PSI_strerror( result ) );

  return result;
}

/* invalidate event descriptor entry */
int invalidate_entry( tRegion region, unsigned int entry )
{
  u_int32_t data = 0;
  int result;

  result = PSI_write( region, entry * 8, 4, 4, & data );
  if ( result == SUCCESS )
    result = PSI_write( region, entry*8 + 4, 4, 4, & data );
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to invalidate event descriptor entry: %s.\n",
	     PSI_strerror( result ) );

  return result;
}

/* get replay controller mode */
int get_mode( tRegion region, u_int32_t * mode, u_int32_t * wait )
{
  int result;
  u_int32_t status;

  result = PSI_read( region, REPLAY_CTRL_REG, 4, 4, & status );
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to read mode: %s.\n", PSI_strerror( result ) );
  else {
    if ( mode ) (* mode) = status & REPLAY_CTRL_STATUS;
    if ( wait ) (* wait) = status & REPLAY_CTRL_WAIT;
  }

  return result;
}

/* set replay controller mode */
int set_mode( tRegion region, u_int32_t mode, u_int32_t wait )
{
  int result;
  u_int32_t ctrl = (mode & REPLAY_CTRL_STATUS) | (wait & REPLAY_CTRL_WAIT);

  result = PSI_write( region, REPLAY_CTRL_REG, 4, 4, & ctrl );
  if ( result != SUCCESS )
    fprintf( stderr, "Failed to read mode: %s.\n", PSI_strerror( result ) );

  return result;
}



/*
*** high level event buffer functions *****************************************
*/



/* load event buffer from file, returns number of words written */
size_t upload_file( tRegion region, const char * path, int offs, int level )
{
  int file, result;
  size_t nbytes;
  off_t total, size, current;
  u_int32_t data;

  /* print info */
  if ( level > QUIET )
    printf( "- Uploading file %s\n", path );

  /* open file */
  file = open( path, O_RDONLY );
  if ( file < 0 ) {
    fprintf( stderr, "Failed to open file %s: %s.\n", 
    	     path, strerror( errno ) );
    return 0;
  }

  /* get file size */
  current = 0;
  total = size = lseek( file, 0, SEEK_END );
  lseek( file, 0, SEEK_SET );
  if ( ! size ) {
    close( file );
    fprintf( stderr, "Skipping file %s: no data.\n", path );
    return 0;
  }

  /* check file size */
  if ( (offs + size/4) > (MAX_OFFSET + 1) ) {
    close( file );
    fprintf( stderr, "Skipping file %s: too large.\n", path );
    return 0;
  }

  /* start write frame */
  result = start_frame( region, RAM_WRITE_FRAME, offs );

  /* read file */
  while ( size && result == SUCCESS ) {
    /* progress indicator */
    if ( level > QUIET && !(current % 1024) ) busy( current, total );

    /* read 32 bit word */
    data = 0;
    nbytes = read( file, & data, 4 );
    if ( nbytes < 1 ) break;

    /* write data to event buffer */
    result = write_frame_data( region, & data );

    current += nbytes;
    size -= nbytes;
  }

  /* clean up */
  if ( level > QUIET ) busy_clear();
  end_frame( region );
  close( file );

  /* check for errors */
  if ( result != SUCCESS ) return 0;
  if ( nbytes < 0 ) {
    fprintf( stderr, "Failed to read from file %s: %s.\n", 
    	     path, strerror( errno ) );
    return 0;
  }

  if ( level >= VERBOSE ) printf( "%lx words written.\n", (current / 4) );
  return (current / 4);
}

/* load event buffer from file or directory */
int load_buffer( tRegion region, const char * path, int entry, int offs,
		 int term, int level )
{
  int result, free_entry;
  off_t free_offs;
  u_int32_t access;
  size_t nwords;
  struct stat st;
  DIR * dir;
  struct dirent * file;
  char basename[FILENAME_MAX], filename[FILENAME_MAX];
  int pos, dirsep;

  /* stat file or directory */
  if ( stat( path, & st ) ) {
    fprintf( stderr, "Failed to stat %s: %s.\n", path, strerror( errno ) );
    return PSI_SYS_ERRNO;
  }

  /* get access control */
  result = get_access( region, & access );
  if ( result != PSI_OK ) return result;
  result = set_access( region, ACCESS_PCI );
  if ( result != PSI_OK ) return result;

  /* if not given explicitly, find next free entry and/or offset */
  if ( entry > MAX_ENTRY || offs > MAX_OFFSET ) {
    free_offs = 0;
    free_entry = 0;
    while ( free_entry < MAX_ENTRY ) {
      u_int32_t noffs, nsize;

      /* get nth entry */
      result = get_entry( region, free_entry, & noffs, & nsize );
      if ( result != SUCCESS ) break;

      /* is entry empty? */
      if ( noffs == 0 ) break;
      noffs &= ~ENTRY_FLAGS;
      nsize &= ~ENTRY_FLAGS;

      /* get next offset and entry */
      if ( free_offs < noffs + nsize ) free_offs = noffs + nsize;
      ++free_entry;
    }
    if ( entry > MAX_ENTRY ) entry = free_entry;
    if ( offs > MAX_OFFSET ) offs = free_offs;
  }
  if ( level >= VERBOSE )
    printf( "- Using entry %d, offset 0x%06x.\n", entry, offs );

  /* process regular file */
  if ( S_ISREG( st.st_mode ) ) {
    result = PSI_SYS_ERRNO;
    nwords = upload_file( region, path, offs, level );
    if ( nwords ) {
      result = set_entry( region, entry, offs, nwords );
      if ( term && entry < MAX_ENTRY )
        invalidate_entry( region, entry + 1 );
    }
  }
  else if ( S_ISDIR( st.st_mode ) ) {
    if ( level > QUIET )
      printf( "- Uploading files from directory %s.\n", path );

    /* open the directory for listing */
    dir = opendir( path );
    if ( ! dir ) {
      fprintf( stderr, "Failed to open directory %s: %s.\n", path,
               strerror( errno ) );
      return PSI_SYS_ERRNO;
    }

    /* contruct base path name */
    strncpy( basename, path, FILENAME_MAX );
    pos = strlen( basename );
    if ( basename[pos-1] == DIRSEP_CHAR ) dirsep = 1;
    else dirsep = 0;
    result = SUCCESS;

    /* browse files */
    while ( (file = readdir( dir )) ) {
      if ( file->d_type != DT_REG ) continue;

      /* construct filename */
      strncpy( filename, basename, FILENAME_MAX );
      if ( ! dirsep ) strncat( filename, DIRSEP_STRING, FILENAME_MAX );
      strncat( filename, file->d_name, FILENAME_MAX );

      /* upload file to event buffer and set event descriptor entry */
      nwords = upload_file( region, filename, offs, level );
      if ( ! nwords ) continue;
      result = set_entry( region, entry, offs, nwords );
      if ( term && entry < MAX_ENTRY )
        invalidate_entry( region, entry + 1 );
      if ( result != SUCCESS ) break;

      offs += nwords;
      entry++;
    }

    /* cleanup */
    closedir( dir );
  }
  else {
    fprintf( stderr, "Failed to process %s: "
    	     "not a regular file or directory.\n", path );
    errno = ENOENT;
    result = PSI_SYS_ERRNO;
  }

  /* release access control */
  set_access( region, access );

  return result;
}



/* print status and event descriptor entries */
int print_status( tRegion region, int level )
{
  int result, entry;
  u_int32_t access, mode, offs, size, wait;

  if ( level > QUIET ) printf( "- Event replay controller status: " );

  /* get access control */
  result = get_access( region, & access );
  if ( result != SUCCESS ) return result;

  /* print access mode */
  if ( access & ACCESS_RAM_DRC )
    printf( "DRC, " );
  else
    printf( "PCI, " );
  if ( access & ACCESS_FLOW_CTRL )
    printf( "Flow-ctrl, " );

  /* get mode */
  result = get_mode( region, & mode, & wait );
  if ( result != SUCCESS ) return result;

  /* print status */
  switch ( mode & REPLAY_CTRL_STATUS ) {
  case 0:
    printf( "Idle\n" );
    break;

  case REPLAY_MODE_ONESHOT:
    printf( "One-shot mode\n" );
    break;

  case REPLAY_MODE_SINGLE:
    printf( "Single run mode (wait %d cycles)\n", wait );
    break;

  case REPLAY_MODE_CONT:
    printf( "Continuous run mode (wait %d cycles)\n", wait );
    break;

  case REPLAY_CTRL_ERROR:
    printf( "Error flag set\n" );
    break;

  default:
    printf( "Undefined\n" );
  }

  /* print event descriptor entries only when not busy */
  if ( mode & REPLAY_CTRL_MODE ) return SUCCESS;
  if ( level > QUIET ) printf( "- Event descriptor entries:\n" );

  /* get access control */
  result = set_access( region, ACCESS_PCI );
  if ( result != SUCCESS ) return result;

  /* get entries */
  for ( entry = 0; entry <= MAX_ENTRY; ++entry ) {
    result = get_entry( region, entry, & offs, & size );
    if ( result != SUCCESS ) break;

    if ( (offs == 0) && (size == 0) ) break;
    printf( "%3d: 0x%06lx .. 0x%06lx (size 0x%06lx)\n", entry, 
            (offs & ~ENTRY_FLAGS), 
	    (offs & ~ENTRY_FLAGS) + (size & ~ENTRY_FLAGS) - 1, 
	    (size & ~ENTRY_FLAGS) ); 
  }

  /* release access control */
  set_access( region, access );

  return result;
}



/*
*** user interface functions **************************************************
*/



/* commands */
enum t_command {
  NONE,
  LOAD,
  PRINT,
  ONESHOT,
  SINGLE,
  CONTINUOUS,
  PAUSE,
  STOP,
  ACCESS,
  CLEAR,
  RESET
};



/* params */
struct t_params {
  enum t_command command;
  int index, link, bar;
  const char * path;
  int entry, offs, wait;
  int level, term;
};



/* print usage */
void usage( void )
{
  fprintf( stderr, "Usage: evtreplay [options] command.\n" );
  fprintf( stderr, "\nCommand can be one of:\n" );
  fprintf( stderr, "\t-a\tset access mode to PCI\n" );
  fprintf( stderr, "\t-c\tclear event descriptor list\n");
  fprintf( stderr, "\t-l file\tload event buffer from file\n" );
  fprintf( stderr, "\t-p\ttoggle run pause\n" );
  fprintf( stderr, "\t-r mode\tstart run\n" );
  fprintf( stderr, "\t\tmode is one of o[neshot], s[ingle], c[ontinuous]\n" );
  fprintf( stderr, "\t-s\tstop run\n" );
  fprintf( stderr, "\t-t\tprint status\n" );
  fprintf( stderr, "\nOptions can be any of:\n" );
  fprintf( stderr, "\t-d\tdon't terminate event descriptor list\n" );
  fprintf( stderr, "\t-e n\tuse nth event descriptor entry\n" );
  fprintf( stderr, "\t-i n\tuse nth card\n" );
  fprintf( stderr, "\t-n n\tuse nth link on card\n" );
  fprintf( stderr, "\t-o offs\tuse offset into event buffer\n" );
  fprintf( stderr, "\t-q\tquiet mode\n" );
  fprintf( stderr, "\t-v\tbe verbose\n" );
  fprintf( stderr, "\t-w n\twait n cycles between single events\n" );
}



/* get error message */
const char * get_error( int result )
{
  switch ( result ) {
  case EMISSING:
    return "Missing option argument";

  case ENOCMD:
    return "No command given";

  case EMULTCMDS:
    return "Multiple commands";

  case EINVALCRD:
    return "Invalid card index";

  case EINVALLNK:
    return "Invalid link index";

  case EINVALENT:
    return "Invalid event descriptor entry";

  case EINVALOFF:
    return "Invalid offset into event buffer";

  case EINVALWAIT:
    return "Invalid wait cycles";

  case EINVALMODE:
    return "Invalid run mode";

  case EMULTLEVLS:
    return "Multiple verbosity levels";

  default:
    return strerror( result );
  }
}



/* parse command line arguments */
int parse_args( int argc, char * argv[], struct t_params * params )
{
  extern char * optarg;
  extern int optind, opterr, optopt;

  int opt;
  int result = SUCCESS;

  /* initialise params */
  params->command = NONE;
  params->index = 0;
  params->link = 0;
  params->bar = LINK_BAR[0];
  params->path = 0;
  params->entry = MAX_ENTRY + 1;
  params->offs = MAX_OFFSET + 1;
  params->wait = MAX_WAIT + 1;
  params->level = DEFAULT;
  params->term = 1;

  /* parse command line arguments */
  errno = 0;
  while ( (opt = getopt( argc, argv, ":acde:hi:l:n:o:pqr:stvw:" )) != -1 ) {
    switch ( opt ) {
    case 'a':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = ACCESS;
      break;

    case 'c':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = CLEAR;
      break;

    case 'd':
      params->term = 0;
      break;

    case 'e':
      params->entry = strtol( optarg, 0, 0 );
      if ( params->entry < 0 || params->entry > MAX_ENTRY )
	result = EINVALENT;
      break;

    case 'h':
      usage();
      exit( 0 );

    case 'i':
      params->index = strtol( optarg, 0, 0 );
      if ( params->index < 0 || params->index > MAX_INDEX )
	result = EINVALCRD;
      break;

    case 'l':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = LOAD;
      params->path = optarg;
      break;

    case 'n':
      params->link = strtol( optarg, 0, 0 );
      if ( params->link < 0 || params->link > MAX_LINK )
	result = EINVALLNK;
      params->bar = LINK_BAR[params->link];
      break;

    case 'o':
      params->offs = strtol( optarg, 0, 0 );
      if ( params->offs < 0 || params->offs > MAX_OFFSET )
	result = EINVALOFF;
      break;
    
    case 'p':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = PAUSE;
      break;

    case 'q':
      if ( params->level >= DEFAULT ) result = EMULTLEVLS;
      params->level = QUIET;
      break;

    case 'r':
      if ( params->command != NONE ) result = EMULTCMDS;
      if ( !strncmp( optarg, "oneshot", strlen( optarg ) ) )
        params->command = ONESHOT;
      else if ( !strncmp( optarg, "single", strlen( optarg ) ) )
        params->command = SINGLE;
      else if ( !strncmp( optarg, "continuous", strlen( optarg ) ) )
        params->command = CONTINUOUS;
      else
        result = EINVALMODE;
      break;

    case 's':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = STOP;
      break;

    case 't':
      if ( params->command != NONE ) result = EMULTCMDS;
      params->command = PRINT;
      break;

    case 'v':
      if ( params->level != DEFAULT ) result = EMULTLEVLS;
      params->level = VERBOSE;
      break;

    case 'w':
      params->wait = strtol( optarg, 0, 0 );
      if ( params->wait < 0 || params->wait > MAX_WAIT )
        result = EINVALWAIT;
      break;

    case ':':
      result = EMISSING;
      break;

    case '?':
      result = EINVAL;
      break;
    }

    /* check for error */
    if ( result != SUCCESS ) return result;
  }

  /* check if command was given */
  if ( params->command == NONE ) result = ENOCMD;

  return result;
}



/* main */
int main( int argc, char * argv[] )
{
  struct t_params params;
  char regionpath[MAX_DEVICE_PATH];
  int result, i;
  tRegion region;
  tRegionStatus info;
  u_int32_t access, mode, wait;

  /* parse command line arguments */
  result = parse_args( argc, argv, & params );
  if ( result != SUCCESS ) {
    printf( "H-RORC Event Replay Utility, Version %s\n\n", VERSION );
    fprintf( stderr, "Error while parsing command line options: %s.\n\n",
	     get_error( result ) );
    usage();
    return result;
  }

  /* print greeting */
  if ( params.level > QUIET )
    printf( "H-RORC Event Replay Utility, Version %s\n\n", VERSION );

  /* open card */
  sprintf( regionpath, "/dev/psi/vendor/0x%04hX/device/0x%04hX/%i/base%d",
	   VENDOR_ID, DEVICE_ID, params.index, params.bar );
  result = PSI_openRegion( & region, regionpath );
  if ( result != PSI_OK ) {
    fprintf( stderr, "Failed to open device: %s.\n", PSI_strerror( result ) );
    return EACCES;
  }

  /* print device info */
  result = PSI_getRegionStatus( region, & info );
  if ( result != PSI_OK ) {
    fprintf( stderr, "Failed to get device info: %s.\n", 
	     PSI_strerror( result ) );
    PSI_closeRegion( region );
    return EIO;
  }
  if ( params.level >= VERBOSE )
    printf( "- Using device %d (0x%02x:0x%02x.0x%x)\n", params.index, 
	    info.bus, PCI_SLOT( info.devfn ), PCI_FUNC( info.devfn ) );

  /* get current mode */
  result = get_mode( region, & mode, & wait );
  if ( result != PSI_OK ) {
    PSI_closeRegion( region );
    return EIO;
  }
  if ( params.wait > MAX_WAIT )
    params.wait = wait;

  /* select mode of operation */
  switch ( params.command ) {
  case LOAD:
    /* check if replay contoller is busy */
    if ( params.level > QUIET && (mode & REPLAY_CTRL_MODE) )
      if ( ask_yn( "Event replay controller is busy." ) ) break;

    /* load event buffer from file or directory */
    result = 
      load_buffer( region, params.path, params.entry, params.offs, 
                   params.term, params.level );
    break;

  case ONESHOT:
    /* check if replay contoller is busy */
    if ( params.level > QUIET && (mode & REPLAY_CTRL_MODE) )
      if ( ask_yn( "Event replay controller is busy." ) ) break;

    /* start one-shot run */
    if ( params.level > QUIET ) printf( "- Starting oneshot run.\n" );
    result = set_access( region, ACCESS_DRC ); 
    if ( result == PSI_OK )
      result = set_mode( region, REPLAY_MODE_ONESHOT, wait );
    break;

  case SINGLE:
    /* check if replay contoller is busy */
    if ( params.level > QUIET && (mode & REPLAY_CTRL_MODE) )
      if ( ask_yn( "Event replay controller is busy." ) ) break;

    /* start single run */
    if ( params.level > QUIET ) printf( "- Starting single run.\n" );
    result = set_access( region, ACCESS_DRC );
    if ( result == PSI_OK ) 
      result = set_mode( region, REPLAY_MODE_SINGLE, params.wait );
    break;

  case CONTINUOUS:
    /* check if replay contoller is busy */
    if ( params.level > QUIET && (mode & REPLAY_CTRL_MODE) )
      if ( ask_yn( "Event replay controller is busy." ) ) break;

    /* start continuous run */
    if ( params.level > QUIET ) printf( "- Starting continuous run.\n" );
    result = set_access( region, ACCESS_DRC );
    if ( result == PSI_OK )
      result = set_mode( region, REPLAY_MODE_CONT, params.wait );
    break;

  case PAUSE:
    /* toggle run pause */
    if ( params.level > QUIET ) {
      if ( mode & REPLAY_CTRL_ENABLE ) printf( "- Pausing run.\n" );
      else printf( "- Resuming run.\n" );
    }
    result = set_mode( region, mode ^ REPLAY_CTRL_ENABLE, wait );
    break;

  case STOP:
    /* stop run */
    if ( params.level > QUIET ) printf( "- Stopping run.\n" );
    result = set_mode( region, 0, wait );
    break;

  case ACCESS:
    /* set access to PCI */
    if ( params.level > QUIET ) printf( "- Setting access to PCI.\n" );
    result = set_access( region, ACCESS_PCI );
    break;

  case CLEAR:
    /* check if replay contoller is busy */
    if ( params.level > QUIET && (mode & REPLAY_CTRL_MODE) )
      if ( ask_yn( "Event replay controller is busy." ) ) break;

    /* clear event descriptor list */
    if ( params.level > QUIET )
      printf( "- Clearing event descriptor list.\n" );
    result = get_access( region, & access );
    if ( result == PSI_OK ) result = set_access( region, ACCESS_PCI );
    if ( result != PSI_OK ) break;
    for ( i = 0; i <= MAX_ENTRY; ++i ) {
      result = invalidate_entry( region, i );
      if ( result != PSI_OK ) break;
    }
    set_access( region, access );
    break;

  case PRINT:
    /* print status and event descriptor entries */
    result = print_status( region, params.level );
    break;

  default:
    fprintf( stderr, "Command not implemented.\n" );
    result = ENOSYS;
  }

  if ( result == SUCCESS && params.level > QUIET ) printf( "Done.\n" );
  PSI_closeRegion( region );

  return result;
}
