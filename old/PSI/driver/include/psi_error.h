#ifndef PSIERROR_H
#define PSIERROR_H

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#define PSI_OK             0	/* OK */

#define PSI_NOT_IMPL       1
#define PSI_DEV_NOT_READY  2
#define PSI_INVALID_REGION 3	/* region does not exist */

/* for PSI_openRegion */
#define PSI_OPEN           100
#define PSI_INVALID_PATH   1 + PSI_OPEN	/* invalid pathname */
#define PSI_BUS_OOR        2 + PSI_OPEN	/* bus number out of range */
#define PSI_SLOT_OOR       3 + PSI_OPEN	/* slot number out of range */
#define PSI_FNKT_OOR       4 + PSI_OPEN	/* function number out of range */
#define PSI_BASE_OOR       5 + PSI_OPEN	/* base number out of range */
#define PSI_VEND_NF        6 + PSI_OPEN	/* vendor-id not found */
#define PSI_DEV_NF         7 + PSI_OPEN	/* device-id not found */
#define PSI_NMBR_NF        8 + PSI_OPEN	/* device number not found */
#define PSI_INVALID_ADDR   9 + PSI_OPEN	/* invalid physical adress */
#define PSI_MAX_REG        10+ PSI_OPEN	/* we have no regions left */
#define PSI_NOT_ALIGNED    11+ PSI_OPEN	/* the pyhsadr is not aligned */
#define PSI_DEVICE_NF      12+ PSI_OPEN	/* device not found */

/* for PSI_closeRegion */
#define PSI_CLOSE          200

/* for PSI_sizeRegion */
#define PSI_SIZE           300
#define PSI_SIZE_SET       1 + PSI_SIZE	/* Regionsize is allready set */
#define PSI_SIZE_2_BIG     2 + PSI_SIZE	/* Regionsize is to big */
#define PSI_N_SIZEABLE     3 + PSI_SIZE	/* Regionsize cannot be changed */

/* for PSI_mapRegion */
#define PSI_MAP            400
#define PSI_N_MAPPABLE     1 + PSI_MAP	/* region does not support mapping */
#define PSI_N_SIZED        2 + PSI_MAP	/* region has size==0 */

/* for PSI_mapWindow in addition*/
/* #define PSI_INV_LENGTH     1 + PSI_RW    Invalid length */
/* #define PSI_NOT_ALIGNED    11+ PSI_OPEN  the pyhsadr is not aligned*/
/* #define PSI_SIZE_2_BIG     2 + PSI_SIZE  Regionsize is to big */

/* for PSI_unmapRegion */
#define PSI_UNMAP          500

/* for PSI_read & write */
#define PSI_RW             600
#define PSI_INV_LENGTH     1 + PSI_RW	/* Invalid length */
#define PSI_OFFSET_OOR     2 + PSI_RW	/* Offset out of range */
/* PSI_NOT_ALIGNED also possible */


/* for any pcibios errno */
#define PSI_PCI_ERRNO      1500

/* for any system errno */
#define PSI_SYS_ERRNO      1500

#endif // PSIERROR_H
