/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "psi.h"
#include "psi_error.h"

unsigned char gtestArray[16384];

int
main (int argc, char **argv)
{
  tRegion region = 0;
  void *addr;
  unsigned char stestArray[16384];
  unsigned char *stestP;
  unsigned char *sbigP;
  unsigned long size;
  PSI_Status retval;
  void *phys;
  void *bus;

  stestP = (unsigned char *) malloc (16384);

  retval = PSI_openRegion (&region, "/dev/psi/bigphys/MemTest0");
  size = 16384;
  PSI_sizeRegion (region, &size);
  addr = NULL;
  PSI_mapRegion (region, &addr);
  sbigP = addr;

  *stestP = 0;
  PSI_getPhysAddress (stestP, &phys, &bus);
  printf ("Malloc: Virtual: 0x%016lX - Physical: 0x%016lX - Bus: 0x%016lX\n",
	  (unsigned long) stestP, (unsigned long) phys, (unsigned long) bus);

  *stestArray = 0;
  PSI_getPhysAddress (stestArray, &phys, &bus);
  printf ("Stack: Virtual: 0x%016lX - Physical: 0x%016lX - Bus: 0x%016lX\n",
	  (unsigned long) stestArray, (unsigned long) phys, (unsigned long) bus);

  *gtestArray = 0;
  PSI_getPhysAddress (gtestArray, &phys, &bus);
  printf ("Static heap: Virtual: 0x%016lX - Physical: 0x%016lX - Bus: 0x%016lX\n",
	  (unsigned long) gtestArray, (unsigned long) phys, (unsigned long) bus);

  PSI_getPhysAddress (sbigP, &phys, &bus);
  printf ("bigphys: Virtual: 0x%016lX - Physical: 0x%016lX - Bus: 0x%016lX\n",
	  (unsigned long) sbigP, (unsigned long) phys, (unsigned long) bus);

  PSI_unmapRegion (region, addr);
  PSI_closeRegion (region);
  if (stestP)
    free (stestP);

  return 0;
}
