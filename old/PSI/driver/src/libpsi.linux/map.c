/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#include "psilib.h"

#include <linux/pci.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <errno.h>
#include <asm/page.h>
#ifdef DEBUG
#include <stdio.h>
#include <string.h>
#endif

extern int inode;

PSI_Status
PSI_mapRegion (tRegion reg, void **virtaddr)
{
  tRegionStatus rs;
  long size;
  PSI_Status status = PSI_OK;
#ifdef DEBUG
  int error;
#endif
  tMMapData mapData;
  PSI_Status retval;
  int trys = 0;

  /* get the size */
  status = PSI_getRegionStatus (reg, &rs);
  size = rs.size;
  /* exit if region not sized and it is not a PCI base region (for those we can determine the size) */
  if (size == 0 && rs.typ != _base_)
    return PSI_N_SIZED;
  /* exit if region is not mappable */
  switch (rs.typ)
    {
    case _none_:
    case _baseIO_:
    case _config_:
      return PSI_N_MAPPABLE;
    case _base_:
    case _mem_:
    case _physmem_:
    case _bigphys_:
	break;
    }
  /* map region */
  mapData.region = reg;
  mapData.size = size;
  mapData.prot = PROT_READ | PROT_WRITE;
  mapData.flags = MAP_SHARED;
  mapData.fd = inode;
  mapData.offset = 0;
  mapData.addr = 0;
  do
    {
      errno = 0;
      retval = ioctl (inode, PSI_DOMMAP, &mapData);
    }
  while (errno == EINTR && trys++ < EINTR_RETRYS);
/* XXX ??? */
/*   if ( mapData.addr && *(long*)(mapData.addr) == -1) { */
/*     error = errno; */
/* #ifdef DEBUG */
/*     fprintf(stderr,"mmap(): %s\n",strerror(error)); */
/* #endif */
/*     return PSI_SYS_ERRNO + error; */
/*   } */
  *virtaddr = mapData.addr;
#ifdef DEBUG
  fprintf (stderr, "mapped %d to %p\n", (int) reg, *virtaddr);
#endif
  return PSI_OK;
}

PSI_Status
PSI_unmapRegion (tRegion reg, void *virtaddr)
{
  tRegionStatus rs;
  long size;
  PSI_Status status = PSI_OK;
  int error;

  /* get the size */
#ifdef DEBUG
  fprintf (stderr, "enter map.c\n");
#endif
  status = PSI_getRegionStatus (reg, &rs);
  size = rs.size;
  /* unmap region */
  if (-1 == munmap (virtaddr, size))
    {
      error = errno;
#ifdef DEBUG
      fprintf (stderr, "munmap(): %s\n", strerror (error));
#endif
      return PSI_SYS_ERRNO + error;
    }
#ifdef DEBUG
  fprintf (stderr, "unmapped %d\n", (int) reg);
#endif
  return PSI_OK;
}


PSI_Status
PSI_mapWindow (tRegion reg, unsigned long offset, unsigned long size,
	       void **virtaddr)
{
  tRegionStatus rs;
  PSI_Status status = PSI_OK;
  int error;
  tMMapData mapData;
  PSI_Status retval;

#ifdef DEBUG
  printf ("mapWindow size: %lu - offset: %lu\n", size, offset);
#endif
  if (size == 0)
    return PSI_INV_LENGTH;
  if (offset & ~PAGE_MASK)
    return PSI_NOT_ALIGNED;
  /* get the size */
  status = PSI_getRegionStatus (reg, &rs);
  if (offset + size > rs.size)
    return PSI_SIZE_2_BIG;
  /* exit if region not sized */
  if (rs.size == 0)
    return PSI_N_SIZED;
  /* exit if region is not mappable */
  switch (rs.typ)
    {
    case _none_:
    case _baseIO_:
    case _config_:
      return PSI_N_MAPPABLE;
    case _base_:
    case _mem_:
    case _physmem_:
    case _bigphys_:
	break;
    }
  /* map region */
  mapData.region = reg;
  mapData.size = size;
  mapData.prot = PROT_READ | PROT_WRITE;
  mapData.flags = MAP_SHARED;
  mapData.fd = inode;
  mapData.offset = offset;
  mapData.addr = 0;
  retval = ioctl (inode, PSI_DOMMAP, &mapData);
  if (*(long *) (mapData.addr) == -1)
    {
      error = errno;
#ifdef DEBUG
      fprintf (stderr, "mmap(): %s\n", strerror (error));
#endif
      return PSI_SYS_ERRNO + error;
    }
  *virtaddr = mapData.addr;
#ifdef DEBUG
  fprintf (stderr, "mapped %d to %p\n", (int) reg, *virtaddr);
#endif
  return PSI_OK;
}

PSI_Status
PSI_unmapWindow (tRegion reg, void *virtaddr, unsigned long size)
{
  int error;

  /* unmap region */
  if (-1 == munmap (virtaddr, size))
    {
      error = errno;
#ifdef DEBUG
      fprintf (stderr, "munmap(): %s\n", strerror (error));
#endif
      return PSI_SYS_ERRNO + error;
    }
#ifdef DEBUG
  fprintf (stderr, "unmapped %d\n", (int) reg);
#endif
  return PSI_OK;
}
