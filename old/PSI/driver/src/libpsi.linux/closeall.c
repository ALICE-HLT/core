/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "../../include/psi.h"
#include "../../include/psi_error.h"

int
main (int argc, char **argv)
{
  tRegion region = 0;

  if (PSI_OK == PSI_openRegion (&region, "/dev/psi/physmem/0"))
    for (region = 0; region < 100; region++)
      {
	while (PSI_OK == PSI_closeRegion (region));
      }
  return 0;
}
