/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#include "psilib.h"

#include <string.h>
#include <stdio.h>

#include <linux/pci.h>
#include <sys/ioctl.h>
#include <errno.h>

extern int inode;

PSI_Status
PSI_getRegionStatus( tRegion reg, pRegionStatus rs )
{
  tRegStat regs;
  PSI_Status status = PSI_OK;
  int trys = 0;

  /* get the status */
  regs.region = reg;
  do
    {
      errno = 0;
      status = ioctl (inode, PSI_REGION_STAT, &regs);
    }
  while (errno == EINTR && trys++ < EINTR_RETRYS);
  /* fill the struct */
  sprintf (rs->name, "%s", regs.rs.name);
  rs->users = regs.rs.users;
  rs->address = regs.rs.address;
  rs->bus = regs.rs.bus;
  rs->devfn = regs.rs.devfn;
  rs->users = regs.rs.users;
  rs->base = regs.rs.base;
  rs->typ = regs.rs.typ;
  rs->size = regs.rs.size;

  return status;
}
