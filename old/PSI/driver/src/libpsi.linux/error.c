/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#include "psilib.h"
#include <stdio.h>
#include <string.h>

char *
PSI_strerror (PSI_Status status)
{

  if (status > PSI_SYS_ERRNO)
    return strerror (status - PSI_SYS_ERRNO);

  if (status > PSI_PCI_ERRNO)
    return strerror (status - PSI_PCI_ERRNO);

  switch (status)
    {
    case PSI_OK:
      return "OK";
    case PSI_NOT_IMPL:
      return "Function not implemented";
    case PSI_DEV_NOT_READY:
      return "Device not ready";
    case PSI_INVALID_REGION:
      return "Region does not exist";

    case PSI_INVALID_PATH:
      return "Invalid pathname";
    case PSI_BUS_OOR:
      return "Bus number out of range";
    case PSI_SLOT_OOR:
      return "Slot number out of range";
    case PSI_FNKT_OOR:
      return "Function number out of range";
    case PSI_BASE_OOR:
      return "Base number out of range";
    case PSI_VEND_NF:
      return "Vendor-id not found";
    case PSI_DEV_NF:
      return "Device-id not found";
    case PSI_NMBR_NF:
      return "Device number not found";
    case PSI_INVALID_ADDR:
      return "Invalid physical adress";
    case PSI_MAX_REG:
      return "We have no regions left";
    case PSI_NOT_ALIGNED:
      return "The pyhsadr is not aligned";
    case PSI_DEVICE_NF:
      return "Device not found";

    case PSI_SIZE_SET:
      return "Regionsize is allready set";
    case PSI_SIZE_2_BIG:
      return "Regionsize is to big";
    case PSI_N_SIZEABLE:
      return "Regionsize cannot be changed";

    case PSI_N_MAPPABLE:
      return "Region does not support mapping";
    case PSI_N_SIZED:
      return "Region has size==0";

    case PSI_INV_LENGTH:
      return "Invalied length";
    case PSI_OFFSET_OOR:
      return "Offset out of range";

    default:
      return "Unknown error";
    }
  return "";
}
