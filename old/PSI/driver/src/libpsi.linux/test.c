/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "../../include/psi.h"
#include "../../include/psi_error.h"
#include <asm/page.h>

tRegion region = 0;
void *addr;

void
testrw (int OFFSET)
{
  PSI_Status retval;
  unsigned long i, j;

  i = 0;
  retval = PSI_read (region, OFFSET, _dword_, _dword_, &i);

  printf ("read before: 0x%08lX\n", i);

  //return;

  if (retval == PSI_OK && addr)
    {
      if (*(unsigned long *) (addr + OFFSET) == i)
	printf ("OK! 0x%08lX == 0x%08lX\n",
		*(unsigned long *) (addr + OFFSET), i);
      else
	printf ("Error! 0x%08lX != 0x%08lX\n",
		*(unsigned long *) (addr + OFFSET), i);
    }
  else
    fprintf (stderr, "Errorcode(read): %ld = %s\n",
	     retval, PSI_strerror (retval));
  j = i;
  i = OFFSET;
  /*printf( "writing to 0x%08lX\n", i );
     retval = PSI_write(region,OFFSET,_dword_,&i);
     if (retval == PSI_OK && addr ) {
     if (*(unsigned long*)(addr+OFFSET) == i)
     printf("OK! 0x%08lX == 0x%08lX\n",
     *(unsigned long*)(addr+OFFSET),i);
     else
     printf ("Error! 0x%08lX != 0x%08lX\n",
     *(unsigned long*)(addr+OFFSET),i);
     }
     else fprintf(stderr, "Errorcode(write): %ld = %s\n",
     retval, PSI_strerror(retval)); */
  retval = PSI_read (region, OFFSET, _dword_, _dword_, &i);

  printf ("read after: 0x%08lX\n", i);

}

int
main (int argc, char **argv)
{
  PSI_Status retval;
  unsigned long s, h;
  void *phys;
  void *bus;
  void *winAddr;

  if (argc < 3)
    {
      fprintf (stderr, "I need 2 parameters\n test <device> <size>\n");
      exit (0);
    }

  retval = PSI_openRegion (&region, argv[1]);
  fprintf (stderr, "region:%d\n", (int) region);

  if (retval != PSI_OK)
    fprintf (stderr, "Errorcode(open): %ld = %s\n",
	     retval, PSI_strerror (retval));

  if (retval == PSI_OK)
    {
      unsigned long size = atol (argv[2]);
      tRegionStatus rs;

      retval = PSI_getRegionStatus (region, &rs);
      if (retval == PSI_OK)
	fprintf (stderr, "Status: users=%d\n", rs.users);
      else
	fprintf (stderr, "Errorcode(stat): %ld = %s\n",
		 retval, PSI_strerror (retval));

      retval = PSI_sizeRegion (region, &size);
      printf (" => size = %ld\n", size);
      if (retval == PSI_OK)
	{
	  retval = PSI_getRegionStatus (region, &rs);
	  if (retval == PSI_OK)
	    fprintf (stderr, "Status: size=%lu\n", rs.size);
	  else
	    fprintf (stderr, "Errorcode(stat): %ld = %s\n",
		     retval, PSI_strerror (retval));
	}
      else
	fprintf (stderr, "Errorcode(size): %ld = %s\n",
		 retval, PSI_strerror (retval));
      addr = NULL;
      retval = PSI_mapRegion (region, &addr);
      if (retval == PSI_OK)
	{
	  unsigned long i;
/*       for(i = 0 ; i < size/4 ; i++) */
/* 	{ */
/* 	  if(!(i%5)) printf("\n0x%08lX | ",i*4); */
/* 	  printf("0x%08X ",((int*)addr)[i]); */
/* 	} */
	  printf ("\n");
	  testrw (0);
	  testrw (16);
	  printf ("Setting by pointer: ");
	  s = 0;
	  //*(((int*)addr)+s/4) = s;
	  h = *(((int *) addr) + s / 4);
	  printf (" @ 0x%08lX -> 0x%08lX (should be 0x%08lX)\n", s, h, s);
	  s = 16;
	  //*(((int*)addr)+s/4) = s;
	  h = *(((int *) addr) + s / 4);
	  printf (" @ 0x%08lX -> 0x%08lX (should be 0x%08lX)\n", s, h, s);
	  printf ("Enter number to continue...");
	  scanf ("%ld", &i);
	  printf ("\n");

	  retval = PSI_getRegionStatus (region, &rs);
	  retval = PSI_getPhysAddress (addr, &phys, &bus);
	  printf ("%s: 0x%08lX %s 0x%08lX  (bus==0x%08lX)\n",
		  (rs.address == (unsigned long) phys ? "Ok" : "Error"),
		  rs.address,
		  (rs.address == (unsigned long) phys ? "==" : "!="),
		  (unsigned long) phys, (unsigned long) bus);

	  if (rs.size >= 2 * PAGE_SIZE)
	    {
	      retval = PSI_mapWindow (region, PAGE_SIZE, PAGE_SIZE, &winAddr);
	      if (retval != PSI_OK)
		fprintf (stderr, "Errorcode(mapWindow): %ld = %s\n",
			 retval, PSI_strerror (retval));
	      else
		{
		  PSI_unmapWindow (region, winAddr, PAGE_SIZE);
		}
	    }
	  PSI_unmapRegion (region, addr);

	}
      else
	{
	  addr = NULL;
	  fprintf (stderr, "Errorcode(mmap): %ld = %s\n",
		   retval, PSI_strerror (retval));
	  testrw (0);
	  testrw (16);
	}


      PSI_closeRegion (region);
    }
  else
    fprintf (stderr, "Errorcode(open): %ld = %s\n",
	     retval, PSI_strerror (retval));

  return region;
}
