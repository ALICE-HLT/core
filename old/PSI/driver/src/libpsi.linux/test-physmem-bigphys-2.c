/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/


#include <stdio.h>
#include <psi.h>
#include <psi_error.h>
#include <stdlib.h>

int
main (int argc, char **argv)
{
  PSI_Status retval;
  unsigned long s, h;
  void *addr;
  void *phys, *bus;
  unsigned long *p;
  unsigned long a, c, i, ok;
  char *cerr;
  tRegion region = 0;
  char name[1024];


  if (argc < 3)
    {
      printf ("Usage: %s <physical address> <size in MB>\n", argv[0]);
      return -1;
    }

  a = strtoul (argv[1], &cerr, 0);
  if (*cerr)
    {
      printf ("Error converting numerical argument 1.\n");
      return -1;
    }

  s = strtoul (argv[2], &cerr, 0);
  if (*cerr)
    {
      printf ("Error converting numerical argument 2.\n");
      return -1;
    }
  s *= 1024 * 1024;


  sprintf (name, "/dev/psi/physmem/0x%08lX", a);
  retval = PSI_openRegion (&region, name);
  if (retval != PSI_OK)
    {
      printf ("Error opening region\n");
      return -1;
    }

  h = s;
  retval = PSI_sizeRegion (region, &h);
  if (retval != PSI_OK)
    {
      printf ("Error sizing region of %lu bytes\n", s);
      PSI_closeRegion (region);
      return -1;
    }
  printf ("Region sized to %lu.\n", h);

  retval = PSI_mapRegion (region, &addr);
  if (retval != PSI_OK)
    {
      printf ("Error mapping region.\n");
      PSI_closeRegion (region);
      return -1;
    }

  p = (unsigned long *) addr;
  retval = PSI_getPhysAddress (addr, &phys, &bus);
  if (retval == PSI_OK)
    {
      printf ("Phys: 0x%08lX - Bus: 0x%08lX\n", (unsigned long) phys,
	      (unsigned long) bus);
    }
  else
    {
      printf ("Error determining physical addresses...\n");
      phys = addr;
    }

  c = h / sizeof (void *);
  p = (unsigned long *) addr;
  a = (unsigned long) phys;
  ok = 1;
  for (i = 0; i < c; i++)
    {
      if (*p != a)
	{
	  printf
	    ("ERROR: Value expected at physical address 0x%08lX (virtual 0x%08lX) is 0x%08lX.\n",
	     a, (unsigned long) p, a);
	  printf
	    ("ERROR: Value found at physical address 0x%08lX (virtual 0x%08lX) is 0x%08lX.\n",
	     a, (unsigned long) p, *p);
	  ok = 0;
	}
      a += sizeof (unsigned long *);
      p++;
    }

  if (ok)
    {
      printf ("All values are as expected.\n");
    }

  PSI_closeRegion (region);
  return 0;
}
