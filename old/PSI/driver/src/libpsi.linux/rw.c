/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#include "psilib.h"

#include <sys/ioctl.h>
#include <errno.h>

extern int inode;

PSI_Status
PSI_read (tRegion region, unsigned long offset,
	  tStepSize stepSize, unsigned long length, void *buffer)
{
  tReadWrite arg;
  int trys = 0, retval;
  arg.region = region;
  arg.offset = offset;
  arg.length = length;
  arg.stepsize = stepSize;
  arg.buffer = buffer;
  do
    {
      errno = 0;
      retval = ioctl (inode, PSI_READ, &arg);
    }
  while (errno == EINTR && trys++ < EINTR_RETRYS);
  return retval;
}

PSI_Status
PSI_write (tRegion region, unsigned long offset,
	   tStepSize stepSize, unsigned long length, void *buffer)
{
  tReadWrite arg;
  int trys = 0, retval;
  arg.region = region;
  arg.offset = offset;
  arg.length = length;
  arg.stepsize = stepSize;
  arg.buffer = buffer;
  do
    {
      errno = 0;
      retval = ioctl (inode, PSI_WRITE, &arg);
    }
  while (errno == EINTR && trys++ < EINTR_RETRYS);
  return retval;
}
