/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#include "psilib.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <linux/pci.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

#define MEMPATH     "/dev/psi/mem/%s"
#define BIGPHYSPATH "/dev/psi/bigphys/%s"
#define PHYSMEMPATH "/dev/psi/physmem/%s"
#define BUSPATH     "/dev/psi/bus/%i/slot/%i/function/%i/%s"
#define VENDORPATH  "/dev/psi/vendor/%hi/device/%hi/%hi/%s"

int inode = -1;

PSI_Status
PSI_openRegion (tRegion * region, const char *dev)
{
  PSI_Status status;

  tMem mem;
  tPhysmem physmem;
  tVendor vendor;
  tBase base;
  tConf conf;
  unsigned int bus;
  unsigned int slot;
  unsigned int func;
  char tail[100];
  int trys = 0, retval;

  /* perhaps we still have to open the device inode */
  if (inode == -1)
    {
      do
	{
	  errno = 0;
	  inode = open (INODE, O_RDWR);
	}
      while (errno == EINTR && trys++ < EINTR_RETRYS);
      trys = 0;
    }
  if (inode == -1)
    return PSI_DEV_NOT_READY;

  if (!dev)
    return PSI_INVALID_PATH;

  /* is it /dev/psi/mem ? */
  if (sscanf (dev, MEMPATH, tail) == 1)
    {
      mem.name = tail;
      mem.region = region;
#ifdef DEBUG
      fprintf (stderr, "name:%s\n", mem.name);
#endif
      do
	{
	  errno = 0;
	  retval = ioctl (inode, PSI_OPEN_MEM, &mem);
	}
      while (errno == EINTR && trys++ < EINTR_RETRYS);
      return retval;
    }

  /* is it /dev/psi/bigphys ? */
  if (sscanf (dev, BIGPHYSPATH, tail) == 1)
    {
      mem.name = tail;
      mem.region = region;
#ifdef DEBUG
      fprintf (stderr, "name:%s\n", mem.name);
#endif
      do
	{
	  errno = 0;
	  retval = ioctl (inode, PSI_OPEN_BIGPHYS, &mem);
	}
      while (errno == EINTR && trys++ < EINTR_RETRYS);
      return retval;
    }

  /* is this /dev/psi/physmem ? */
  if (sscanf (dev, PHYSMEMPATH, tail) == 1)
    {
      physmem.address = strtoul (tail, NULL, 0);
      physmem.region = region;
#ifdef DEBUG
      fprintf (stderr, "address:0x%lX\n", physmem.address);
#endif
      do
	{
	  errno = 0;
	  retval = ioctl (inode, PSI_OPEN_PHYSMEM, &physmem);
	}
      while (errno == EINTR && trys++ < EINTR_RETRYS);
      return retval;
    }

  /* is this /dev/psi/bus... ? */
  if (sscanf (dev, BUSPATH, &bus, &slot, &func, tail) == 4)
    {
#ifdef DEBUG
      fprintf (stderr, "Found a bus request: %s\n", tail);
#endif
      /* is it a base ? */
      if (sscanf (tail, "base%hd", &base.base) == 1)
	{
	  base.devfn = PCI_DEVFN (slot, func);
	  base.bus = bus;
	  base.region = region;
#ifdef DEBUG
	  fprintf (stderr, "bus:%u\ndevfn:%u\nbase:%d\n",
		   bus, base.devfn, base.base);
#endif
	  do
	    {
	      errno = 0;
	      retval = ioctl (inode, PSI_OPEN_BASE, &base);
	    }
	  while (errno == EINTR && trys++ < EINTR_RETRYS);
	  return retval;
	}
      /* is it a conf ? */
      if (strncmp (tail, "conf", 4) == 0)
	{
	  conf.devfn = PCI_DEVFN (slot, func);
	  conf.bus = bus;
	  conf.region = region;
#ifdef DEBUG
	  fprintf (stderr, "bus:%u\ndevfn:%u\n", conf.bus, conf.devfn);
#endif
	  do
	    {
	      errno = 0;
	      retval = ioctl (inode, PSI_OPEN_CONFIG, &conf);
	    }
	  while (errno == EINTR && trys++ < EINTR_RETRYS);
	  return retval;
	}
      return PSI_INVALID_PATH;
    }
  /* is this /dev/psi/vendor... ? */
  if (sscanf (dev, VENDORPATH,
	      &vendor.vendorID, &vendor.deviceID, &vendor.number, tail) == 4)
    {
#ifdef DEBUG
      fprintf (stderr, "Found a bus request: %s\n", tail);
#endif
      /* Vendor . Bus */
      do
	{
	  errno = 0;
	  status = ioctl (inode, PSI_VENDOR_2_BUS, &vendor);
	}
      while (errno == EINTR && trys++ < EINTR_RETRYS);
      trys = 0;
      if (status != PSI_OK)
	return status;
      /* is it a base ? */
      if (sscanf (tail, "base%hd", &base.base) == 1)
	{
	  base.devfn = vendor.devfn;
	  base.bus = vendor.bus;
	  base.region = region;
#ifdef DEBUG
	  fprintf (stderr, "bus:%u\ndevfn:%u\nbase:%d\n",
		   base.bus, base.devfn, base.base);
#endif
	  do
	    {
	      errno = 0;
	      retval = ioctl (inode, PSI_OPEN_BASE, &base);
	    }
	  while (errno == EINTR && trys++ < EINTR_RETRYS);
	  return retval;
	}
      /* is it a conf ? */
      if (strncmp (tail, "conf", 4) == 0)
	{
	  conf.devfn = vendor.devfn;
	  conf.bus = vendor.bus;
	  conf.region = region;
#ifdef DEBUG
	  fprintf (stderr, "bus:%u\ndevfn:%u\n", conf.bus, conf.devfn);
#endif
	  do
	    {
	      errno = 0;
	      retval = ioctl (inode, PSI_OPEN_CONFIG, &conf);
	    }
	  while (errno == EINTR && trys++ < EINTR_RETRYS);
	  return retval;
	}
      return PSI_INVALID_PATH;
    }

  return PSI_INVALID_PATH;
}
