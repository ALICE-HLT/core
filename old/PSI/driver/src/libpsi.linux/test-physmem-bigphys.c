/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/


#include <stdio.h>
#include <psi.h>
#include <psi_error.h>
#include <stdlib.h>

int
main (int argc, char **argv)
{
  PSI_Status retval;
  unsigned long s, h;
  void *phys;
  void *bus;
  void *addr;
  unsigned long *p;
  unsigned long c, i, a;
  char *cerr;
  tRegion region = 0;


  if (argc < 2)
    {
      printf ("Usage: %s <memsize in MB>\n", argv[0]);
      return -1;
    }

  s = strtoul (argv[1], &cerr, 0);
  if (*cerr)
    {
      printf ("Error converting numerical argument.\n");
      return -1;
    }

  s *= 1024 * 1024;

  retval = PSI_openRegion (&region, "/dev/psi/bigphys/test-phys-big1");
  if (retval != PSI_OK)
    {
      printf ("Error opening region\n");
      return -1;
    }

  h = s;
  retval = PSI_sizeRegion (region, &h);
  if (retval != PSI_OK)
    {
      printf ("Error sizing region of %lu bytes\n", s);
      PSI_closeRegion (region);
      return -1;
    }
  printf ("Region sized to %lu.\n", h);

  retval = PSI_mapRegion (region, &addr);
  if (retval != PSI_OK)
    {
      printf ("Error mapping region.\n");
      PSI_closeRegion (region);
      return -1;
    }

  retval = PSI_getPhysAddress (addr, &phys, &bus);
  if (retval == PSI_OK)
    {
      printf ("Phys: 0x%08lX - Bus: 0x%08lX\n", (unsigned long) phys,
	      (unsigned long) bus);
    }
  else
    {
      printf ("Error determining physical addresses...\n");
      phys = addr;
    }

  c = h / sizeof (void *);
  p = (unsigned long *) addr;
  a = (unsigned long) phys;
  for (i = 0; i < c; i++)
    {
      *p++ = a;
      a += sizeof (unsigned long *);
    }
  PSI_closeRegion (region);

  return 0;
}
