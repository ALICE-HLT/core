#ifndef IOCTRL_H
#define IOCTRL_H

#include "../../include/psi.h"

#define PSI_OPEN_MEM     1
#define PSI_OPEN_BIGPHYS 2
#define PSI_OPEN_PHYSMEM 3
#define PSI_OPEN_BASE    4
#define PSI_OPEN_CONFIG  5
#define PSI_SIZE_REGION  6
#define PSI_VENDOR_2_BUS 7
#define PSI_CLOSE_REGION 8
#define PSI_REGION_STAT  9
#define PSI_RESET_USAGE  10
#define PSI_READ         11
#define PSI_WRITE        12
#define PSI_GETPHYSADDR  13
#define PSI_DOMMAP       14

typedef struct sSize
{
  // param
  tRegion region;
  // param & retval
  long size;
}
tSize;

typedef struct sRegStat
{
  // param
  tRegion region;
  // retval
  tRegionStatus rs;
}
tRegStat;

typedef struct sMem
{
  // retval
  tRegion *region;
  // param
  char *name;
}
tMem;

typedef struct sPhysmem
{
  // retval
  tRegion *region;
  // param
  unsigned long address;
}
tPhysmem;

typedef struct sBase
{
  // retval
  tRegion *region;
  // param
  char bus;
  unsigned int devfn;
  short base;
}
tBase;

typedef struct sConf
{
  // retval
  tRegion *region;
  // param
  char bus;
  unsigned int devfn;
}
tConf;

typedef struct sVendor
{
  // param
  unsigned short vendorID;
  unsigned short deviceID;
  unsigned short number;
  // retval
  char bus;
  unsigned int devfn;
}
tVendor;

typedef struct sReadWrite
{
  // param
  tRegion region;
  tStepSize stepsize;
  unsigned long offset;
  unsigned long length;
  // retval or param
  void *buffer;
}
tReadWrite;

typedef struct sVirtPhysAddr
{
  const void *virtAddr;
  void *physAddr;
  void *busAddr;
  pid_t pid;
}
tVirtPhysAddr, *pVirtPhysAddr;

typedef struct sMMapData
{
  tRegion region;
  size_t size;
  int prot;
  int flags;
  int fd;
  off_t offset;
  void *addr;
}
tMMapData, *pMMapData;

/*
 * The PCI interface treats multi-function devices as independent
 * devices.  The slot/function address of each device is encoded
 * in a single byte as follows:
 *
 *      7:3 = slot
 *      2:0 = function
 */
// #define PCI_DEVFN(slot,func)    ((((slot) & 0x1f) << 3) | ((func) & 0x07))
// #define PCI_SLOT(devfn)         (((devfn) >> 3) & 0x1f)
// #define PCI_FUNC(devfn)         ((devfn) & 0x07)

#endif
