#ifndef PSIMOD_H
#define PSIMOD_H

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#include "../../include/psi.h"
#include "../../include/psi_error.h"

/* the maximum Number of regions */
#define MAXREG 100

/* my name */
#define DRVNAME "PSI"
/* my on inode numbers */
#define PSI_MAJOR 100
#define PSI_MINOR 0

/* Size of the PCI Configspace */
#define PCI_CONFIG_SIZE 256

void resetReg (tRegionStatus * shm);

#endif
