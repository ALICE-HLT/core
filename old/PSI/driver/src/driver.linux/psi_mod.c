/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
++ 2001/08/08 Introduced spinlocks to protect global variables (AWI)
*************************************************************************/

// $Id$ //

/*
 * Include files
 */

#include <asm/mman.h>
#include <asm/io.h>
/*#include <linux/malloc.h>*/
#include <linux/slab.h>
#include <linux/vmalloc.h>
#include <asm/uaccess.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/delay.h>
#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/major.h>
#include <linux/mm.h>
#include <linux/file.h>
#ifdef WITHBIGPHYS
#include <linux/bigphysarea.h>
#endif /* WITHBIGPHYS */
/*#include <asm-i386/pgtable.h>*/
#include <linux/highmem.h>
#include <linux/spinlock.h>

#include "psimod.h"
#include "ioctrl.h"
static inline long do_mmap2 (unsigned long addr, unsigned long len,
			     unsigned long prot, unsigned long flags,
			     unsigned long fd, unsigned long pgoff);


/*#define DEBUG*/


/*
 * global top-level pointer to array of shared memories
 */


tRegionStatus RegList[MAXREG];

/* AWI Lock for RegList array */
rwlock_t RegList_rwlock = RW_LOCK_UNLOCKED;


/* what is our highest region ? */
int psi_reg_count;

/* AWI Lock for psi_reg_count */
spinlock_t psi_reg_count_lock = SPIN_LOCK_UNLOCKED;


/**
 **
 **      PSI API
 **
 **/

#define pci_find_dev pci_find_slot

#ifdef DEVELOPMENT
#if defined(__i386__)

/* Copied and adapted from arch/i386/kernel/pci-pc.c */

#define PCI_CONF1_ADDRESS(bus, dev, fn, reg) \
	(0x80000000 | (bus << 16) | (dev << 11) | (fn << 8) | (reg & ~3))

static int pci_conf1_read (int seg, int bus, int dev, int fn, int reg, int len, u32 *value) /* !CONFIG_MULTIQUAD */
{
	unsigned long flags;

	if (bus > 255 || dev > 31 || fn > 7 || reg > 255)
		return -EINVAL;

	outl(PCI_CONF1_ADDRESS(bus, dev, fn, reg), 0xCF8);

	switch (len) {
	case 1:
		*value = inb(0xCFC + (reg & 3));
		break;
	case 2:
		*value = inw(0xCFC + (reg & 2));
		break;
	case 4:
		*value = inl(0xCFC);
		break;
	}

	spin_unlock_irqrestore(&pci_config_lock, flags);

	return 0;
}

static int pci_conf1_write (int seg, int bus, int dev, int fn, int reg, int len, u32 value) /* !CONFIG_MULTIQUAD */
{
	unsigned long flags;

	if ((bus > 255 || dev > 31 || fn > 7 || reg > 255)) 
		return -EINVAL;

	spin_lock_irqsave(&pci_config_lock, flags);

	outl(PCI_CONF1_ADDRESS(bus, dev, fn, reg), 0xCF8);

	switch (len) {
	case 1:
		outb((u8)value, 0xCFC + (reg & 3));
		break;
	case 2:
		outw((u16)value, 0xCFC + (reg & 2));
		break;
	case 4:
		outl((u32)value, 0xCFC);
		break;
	}

	spin_unlock_irqrestore(&pci_config_lock, flags);

	return 0;
}

#undef PCI_CONF1_ADDRESS

static int pci_conf1_read_config_byte(struct pci_dev *dev, int where, u8 *value)
{
	int result; 
	u32 data;

	result = pci_conf1_read(0, dev->bus->number, PCI_SLOT(dev->devfn), 
		PCI_FUNC(dev->devfn), where, 1, &data);

	*value = (u8)data;

	return result;
}

static int pci_conf1_read_config_word(struct pci_dev *dev, int where, u16 *value)
{
	int result; 
	u32 data;

	result = pci_conf1_read(0, dev->bus->number, PCI_SLOT(dev->devfn), 
		PCI_FUNC(dev->devfn), where, 2, &data);

	*value = (u16)data;

	return result;
}

static int pci_conf1_read_config_dword(struct pci_dev *dev, int where, u32 *value)
{
	return pci_conf1_read(0, dev->bus->number, PCI_SLOT(dev->devfn), 
		PCI_FUNC(dev->devfn), where, 4, value);
}

static int pci_conf1_write_config_byte(struct pci_dev *dev, int where, u8 value)
{
	return pci_conf1_write(0, dev->bus->number, PCI_SLOT(dev->devfn), 
		PCI_FUNC(dev->devfn), where, 1, value);
}

static int pci_conf1_write_config_word(struct pci_dev *dev, int where, u16 value)
{
	return pci_conf1_write(0, dev->bus->number, PCI_SLOT(dev->devfn), 
		PCI_FUNC(dev->devfn), where, 2, value);
}

static int pci_conf1_write_config_dword(struct pci_dev *dev, int where, u32 value)
{
	return pci_conf1_write(0, dev->bus->number, PCI_SLOT(dev->devfn), 
		PCI_FUNC(dev->devfn), where, 4, value);
}

/* End copy from arch/i386/kernel/pci-pc.c */
#else /* __i386__ */
#error PCI configuration read routines currently not supported for other architectures than i386
#endif
#endif /* Development */


void
get_bar_size (char bus, unsigned devfn, short base, unsigned long *maxsize)
{
  struct pci_dev *dev;
  u32 curr, mask;
  u16 cmd;

  base = 16 + 4 * base;
  dev = pci_find_dev (bus, devfn);
  if (!dev)
    *maxsize = 0;

  pci_read_config_word (dev, 4, &cmd);
  /* Disable device while we mess with it. */
  pci_write_config_word (dev, 4, cmd & ~3);

  pci_read_config_dword (dev, base, &curr);
  pci_write_config_dword (dev, base, ~0);
  pci_read_config_dword (dev, base, &mask);
  pci_write_config_dword (dev, base, curr);

  /* Reenable the device. */
  pci_write_config_word (dev, 4, cmd);

  if (mask & 1)
    {
      /* IO BAR */
      mask &= ~0x3;
    }
  else
    {
      /* Memory BAR */
      mask &= ~0xF;
    }

  *maxsize = ~mask + 1;

#ifdef DEBUG
  printk ("%s: max. basesize %d\n", DRVNAME, ~mask + 1);
#endif /* DEBUG */
}



PSI_Status
psi_read (tReadWrite * arg)
{
  struct pci_dev *dev;
  tRegion region = arg->region;
  int r = 0, i;
  void *p;

#ifdef DEBUG
  printk
    ("psi_read: arg: 0x%p - arg->offset, 0x%lX - arg->length: 0x%lX - arg->stepsize: %d - arg->buffer: 0x%p\n",
     arg, arg->offset, arg->length, arg->stepsize, arg->buffer);
#endif


  read_lock (&RegList_rwlock);
  if ((RegList[region].address + arg->offset) % arg->stepsize ||
      arg->length % arg->stepsize)
    {
      read_unlock (&RegList_rwlock);
      return PSI_NOT_ALIGNED;
    }

  if (RegList[region].size == 0
      && (RegList[region].typ == _base_ || RegList[region].typ == _baseIO_))
    {
      unsigned long maxsize;

      get_bar_size (RegList[region].bus, RegList[region].devfn,
		    RegList[region].base, &maxsize);
      RegList[region].size = maxsize;
    }

  if (RegList[region].size == 0 && RegList[region].typ == _config_)
    {
      RegList[region].size = PCI_CONFIG_SIZE;
    }

  /* check the offset */
  if (((arg->offset + arg->length)) > RegList[region].size)
    {
      read_unlock (&RegList_rwlock);
      return PSI_OFFSET_OOR;
    }

  switch (RegList[region].typ)
    {
    case _none_:
      read_unlock (&RegList_rwlock);
      return PSI_INVALID_REGION;

    case _baseIO_:
      switch (arg->stepsize)
	{
	case _byte_:
	  for (i = 0; i < arg->length; i++)
	    {
	      *(unsigned char *) (arg->buffer + i) =
		inb (RegList[region].address + arg->offset + i);
	    }
	  break;
	case _word_:
	  for (i = 0; i < arg->length; i += 2)
	    {
	      *(unsigned short *) (arg->buffer + i) =
		inw (RegList[region].address + arg->offset + i);
	    }
	  break;
	case _dword_:
	  for (i = 0; i < arg->length; i += 4)
	    {
	      *(unsigned long *) (arg->buffer + i) =
		inl (RegList[region].address + arg->offset + i);
	    }
	  break;
	default:
	  printk ("%s: bad data step size length %d\n", DRVNAME,
		  arg->stepsize);
	  read_unlock (&RegList_rwlock);
	  return PSI_INV_LENGTH;
	}
      break;
    case _base_:
      p = ioremap (RegList[region].address, RegList[region].size);
      switch (arg->stepsize)
	{
	case _byte_:
	  for (i = 0; i < arg->length; i++)
	    {
	      *(unsigned char *) (arg->buffer + i) =
		readb (p + arg->offset + i);
	    }
	  break;
	case _word_:
	  for (i = 0; i < arg->length; i += 2)
	    {
	      *(unsigned short *) (arg->buffer + i) =
		readw (p + arg->offset + i);
	    }
	  break;
	case _dword_:
	  for (i = 0; i < arg->length; i += 4)
	    {
	      *(unsigned long *) (arg->buffer + i) = readl (p + arg->offset + i);
	    }
	  break;
	default:
	  printk ("%s: bad data step size length %d\n", DRVNAME,
		  arg->stepsize);
	  read_unlock (&RegList_rwlock);
	  return PSI_INV_LENGTH;
	}
      iounmap (p);
      break;
    case _mem_:
    case _bigphys_:
    case _physmem_:
      switch (arg->stepsize)
	{
	case _byte_:
	  for (i = 0; i < arg->length; i++)
	    {
	      *(unsigned char *) (arg->buffer + i) =
		readb (RegList[region].address + arg->offset + i);
	    }
	  break;
	case _word_:
	  for (i = 0; i < arg->length; i += 2)
	    {
	      *(unsigned short *) (arg->buffer + i) =
		readw (RegList[region].address + arg->offset + i);
	    }
	  break;
	case _dword_:
	  for (i = 0; i < arg->length; i += 4)
	    {
	      *(unsigned long *) (arg->buffer + i) =
		readl (RegList[region].address + arg->offset + i);
	    }
	  break;
	default:
	  printk ("%s: bad data step size length %d\n", DRVNAME,
		  arg->stepsize);
	  read_unlock (&RegList_rwlock);
	  return PSI_INV_LENGTH;
	}
      break;

      //    case _baseIO_:
      //      return PSI_NOT_IMPL;

    case _config_:
      dev = pci_find_dev (RegList[region].bus, RegList[region].devfn);
      /* find device */
      if (dev == NULL)
	{
	  read_unlock (&RegList_rwlock);
	  return PSI_DEV_NOT_READY;
	}
      else
	{
	  /* read the data */
	  switch (arg->stepsize)
	    {
	    case _byte_:
	      for (i = 0; i < arg->length && !r; i++)
		{
		  r =
		    pci_read_config_byte (dev, arg->offset + i,
					  (u8 *) (arg->buffer + i));
		}
	      break;
	    case _word_:
	      for (i = 0; i < arg->length && !r; i += 2)
		{
		  r =
		    pci_read_config_word (dev, arg->offset + i,
					  (u16 *) (arg->buffer + i));
		}
	      break;
	    case _dword_:
	      for (i = 0; i < arg->length && !r; i += 4)
		{
		  r =
		    pci_read_config_dword (dev, arg->offset + i,
					   (u32 *) (arg->buffer + i));
		}
	      break;
	    default:
	      printk ("%s: bad data step size length %d\n", DRVNAME,
		      arg->stepsize);
	      read_unlock (&RegList_rwlock);
	      return PSI_INV_LENGTH;
	    }
	  if (r)
	    {
	      read_unlock (&RegList_rwlock);
	      return PSI_PCI_ERRNO + r;
	    }
	}
      break;
    }
  read_unlock (&RegList_rwlock);
  return PSI_OK;
}

PSI_Status
psi_write (tReadWrite * arg)
{
  struct pci_dev *dev;
  tRegion region = arg->region;
  int r = 0, i;
  void *p;

#ifdef DEBUG
  printk
    ("psi_write: arg: 0x%p - arg->offset, 0x%lX - arg->length: 0x%lX - arg->stepsize: %d - arg->buffer: 0x%p\n",
     arg, arg->offset, arg->length, arg->stepsize, arg->buffer);
#endif

  read_lock (&RegList_rwlock);

  if ((RegList[region].address + arg->offset) % arg->stepsize ||
      arg->length % arg->stepsize)
    {
      read_unlock (&RegList_rwlock);
      return PSI_NOT_ALIGNED;
    }

  if (RegList[region].size == 0
      && (RegList[region].typ == _base_ || RegList[region].typ == _baseIO_))
    {
      unsigned long maxsize;

      get_bar_size (RegList[region].bus, RegList[region].devfn,
		    RegList[region].base, &maxsize);
      RegList[region].size = maxsize;
    }

  if (RegList[region].size == 0 && RegList[region].typ == _config_)
    {
      RegList[region].size = PCI_CONFIG_SIZE;
    }

  /* check the offset */
  if (((arg->offset + arg->length)) > RegList[region].size)
    {
      read_unlock (&RegList_rwlock);
      return PSI_OFFSET_OOR;
    }

  switch (RegList[region].typ)
    {
    case _none_:
      read_unlock (&RegList_rwlock);
      return PSI_INVALID_REGION;

    case _baseIO_:
      switch (arg->stepsize)
	{
	case _byte_:
	  for (i = 0; i < arg->length; i++)
	    {
	      outb (*(unsigned char *) (arg->buffer + i),
		    RegList[region].address + arg->offset + i);
	    }
	  break;
	case _word_:
	  for (i = 0; i < arg->length; i += 2)
	    {
	      outw (*(unsigned short *) (arg->buffer + i),
		    RegList[region].address + arg->offset + i);
	    }
	  break;
	case _dword_:
	  for (i = 0; i < arg->length; i += 4)
	    {
	      outl (*(unsigned long *) (arg->buffer + i),
		    RegList[region].address + arg->offset + i);
	    }
	  break;
	default:
	  printk ("%s: bad data step length %d\n", DRVNAME, arg->stepsize);
	  read_unlock (&RegList_rwlock);
	  return PSI_INV_LENGTH;
	}
      break;
    case _base_:
      p = ioremap (RegList[region].address, RegList[region].size);
      switch (arg->stepsize)
	{
	case _byte_:
	  for (i = 0; i < arg->length; i++)
	    {
	      writeb (*(unsigned char *) (arg->buffer + i),
		      p + arg->offset + i);
	    }
	  break;
	case _word_:
	  for (i = 0; i < arg->length; i += 2)
	    {
	      writew (*(unsigned short *) (arg->buffer + i),
		      p + arg->offset + i);
	    }
	  break;
	case _dword_:
	  for (i = 0; i < arg->length; i += 4)
	    {
	      writel (*(unsigned long *) (arg->buffer + i),
		      p + arg->offset + i);
	    }
	  break;
	default:
	  printk ("%s: bad data step length %d\n", DRVNAME, arg->stepsize);
	  read_unlock (&RegList_rwlock);
	  return PSI_INV_LENGTH;
	}
      iounmap (p);
      break;
    case _mem_:
    case _bigphys_:
    case _physmem_:
      switch (arg->stepsize)
	{
	case _byte_:
	  for (i = 0; i < arg->length; i++)
	    {
	      writeb (*(unsigned char *) (arg->buffer + i),
		      RegList[region].address + arg->offset + i);
	    }
	  break;
	case _word_:
	  for (i = 0; i < arg->length; i += 2)
	    {
	      writew (*(unsigned short *) (arg->buffer + i),
		      RegList[region].address + arg->offset + i);
	    }
	  break;
	case _dword_:
	  for (i = 0; i < arg->length; i += 4)
	    {
	      writel (*(unsigned long *) (arg->buffer + i),
		      RegList[region].address + arg->offset + i);
	    }
	  break;
	default:
	  printk ("%s: bad data step length %d\n", DRVNAME, arg->stepsize);
	  read_unlock (&RegList_rwlock);
	  return PSI_INV_LENGTH;
	}
      break;
    case _config_:
      dev = pci_find_dev (RegList[region].bus, RegList[region].devfn);
      /* find device */
      if (dev == NULL)
	{
	  read_unlock (&RegList_rwlock);
	  return PSI_DEV_NOT_READY;
	}
      else
	{
	  /* write the data */
	  switch (arg->stepsize)
	    {
	    case _byte_:
	      for (i = 0; i < arg->length && !r; i++)
		{
		  r =
		    pci_write_config_byte (dev, arg->offset + i,
					   *((u8 *) arg->buffer + i));
		}
	      break;
	    case _word_:
	      for (i = 0; i < arg->length && !r; i += 2)
		{
		  r =
		    pci_write_config_word (dev, arg->offset + i,
					   *((u16 *) arg->buffer + i));
		}
	      break;
	    case _dword_:
	      for (i = 0; i < arg->length && !r; i += 4)
		{
		  r =
		    pci_write_config_dword (dev, arg->offset + i,
					    *((u32 *) arg->buffer + i));
		}
	      break;
	    default:
	      printk ("%s: bad data step length %d\n", DRVNAME,
		      arg->stepsize);
	      read_unlock (&RegList_rwlock);
	      return PSI_INV_LENGTH;
	    }
	  if (r)
	    {
	      read_unlock (&RegList_rwlock);
	      return PSI_PCI_ERRNO + r;
	    }
	}
      break;
    }
  read_unlock (&RegList_rwlock);
  return PSI_OK;
}

PSI_Status
psi_open_mem (tMem * mem)
{
  int i = 0;

  spin_lock (&psi_reg_count_lock);
  write_lock (&RegList_rwlock);

  /* search for a region with the same physadrr */
  for (i = 0; (RegList[i].typ != _mem_ ||
	       strcmp (RegList[i].name, mem->name) != 0) &&
       i <= psi_reg_count; i++);

  /* find first free region */
  if (strcmp (RegList[i].name, mem->name) != 0)
    {
      for (i = 0; RegList[i].users > 0 && i < MAXREG; i++);
    }

  if (i == MAXREG)
    {
      write_unlock (&RegList_rwlock);
      spin_unlock (&psi_reg_count_lock);
      return PSI_MAX_REG;
    }

  /* fill the structure */
  if (RegList[i].users == 0)
    {
      sprintf (RegList[i].name, "%s", mem->name);
      RegList[i].typ = _mem_;
    }
  RegList[i].users++;

  /* increase the RegionCounter */
  if (i > psi_reg_count)
    psi_reg_count = i;

  /* set the Region return value */
  *(mem->region) = i;

  MOD_INC_USE_COUNT;
#ifdef DEBUG
  printk ("%s: %s is region %d. (%d times used)\n",
	  DRVNAME, mem->name, i, RegList[i].users);
#endif

  write_unlock (&RegList_rwlock);
  spin_unlock (&psi_reg_count_lock);
  return PSI_OK;
}

PSI_Status
psi_open_bigphys (tMem * mem)
{
  int i = 0;

  spin_lock (&psi_reg_count_lock);
  write_lock (&RegList_rwlock);

  /* search for a region with the same physadrr */
  for (i = 0; (RegList[i].typ != _bigphys_ ||
	       strcmp (RegList[i].name, mem->name) != 0) &&
       i <= psi_reg_count; i++);

  /* find first free region */
  if (strcmp (RegList[i].name, mem->name) != 0)
    {
      for (i = 0; RegList[i].users > 0 && i < MAXREG; i++);
    }

  if (i == MAXREG)
    {
      write_unlock (&RegList_rwlock);
      spin_unlock (&psi_reg_count_lock);
      return PSI_MAX_REG;
    }

  /* fill the structure */
  if (RegList[i].users == 0)
    {
      sprintf (RegList[i].name, "%s", mem->name);
      RegList[i].typ = _bigphys_;
    }
  RegList[i].users++;

  /* increase the RegionCounter */
  if (i > psi_reg_count)
    psi_reg_count = i;



  /* set the Region return value */
  *(mem->region) = i;

  MOD_INC_USE_COUNT;
#ifdef DEBUG
  printk ("%s: %s is region %d. (%d times used)\n",
	  DRVNAME, mem->name, i, RegList[i].users);
#endif

  write_unlock (&RegList_rwlock);
  spin_unlock (&psi_reg_count_lock);
  return PSI_OK;
}

PSI_Status
psi_open_physmem (tPhysmem * mem)
{
  int i = 0;

#ifdef DEBUG
  printk ("%s: address: %p\n", DRVNAME, (void *) mem->address);
#endif

  /* address must be aligned */
  if (mem->address & (PAGE_SIZE - 1))
    return PSI_NOT_ALIGNED;


  spin_lock (&psi_reg_count_lock);
  write_lock (&RegList_rwlock);

  /* search for a region with the same physadrr */
  for (i = 0; (RegList[i].typ != _physmem_ ||
	       RegList[i].address !=
	       (unsigned long) phys_to_virt (mem->address))
       && i <= psi_reg_count; i++);

  /* find first free region */
  if (RegList[i].address != (unsigned long) phys_to_virt (mem->address))
    {
      for (i = 0; RegList[i].users > 0 && i < MAXREG; i++);
    }

  if (i == MAXREG)
    {
      write_unlock (&RegList_rwlock);
      spin_unlock (&psi_reg_count_lock);
      return PSI_MAX_REG;
    }

  /* fill the structure */
  if (RegList[i].users == 0)
    {
    /* Test XXX */
      RegList[i].address = (unsigned long) mem->address;
      /*RegList[i].address = (unsigned long) phys_to_virt (mem->address);*/
      RegList[i].typ = _physmem_;
    }
  RegList[i].users++;

  /* increase the RegionCounter */
  if (i > psi_reg_count)
    psi_reg_count = i;



  /* set the Region return value */
  *(mem->region) = i;

  MOD_INC_USE_COUNT;
#ifdef DEBUG
  printk ("%s: %p (virt %p) is region %d. (%d times used)\n",
	  DRVNAME, (void *) mem->address, phys_to_virt (mem->address), i,
	  RegList[i].users);
#endif

  write_unlock (&RegList_rwlock);
  spin_unlock (&psi_reg_count_lock);
  return PSI_OK;
}

PSI_Status
psi_open_base (tBase * base)
{
  int i = 0;
  struct pci_dev *dev;


  spin_lock (&psi_reg_count_lock);
  write_lock (&RegList_rwlock);

  /* search for a region with the same parameters */
  for (i = 0; ((RegList[i].typ != _base_ && RegList[i].typ != _baseIO_) ||
	       RegList[i].bus != base->bus ||
	       RegList[i].devfn != base->devfn ||
	       RegList[i].base != base->base) && i <= psi_reg_count; i++);

  /* find first free region */
  if (RegList[i].bus != base->bus && RegList[i].devfn != base->devfn &&
      RegList[i].base != base->base)
    {
      for (i = 0; RegList[i].users > 0 && i < MAXREG; i++);
    }

  if (i == MAXREG)
    {
      write_unlock (&RegList_rwlock);
      spin_unlock (&psi_reg_count_lock);
      return PSI_MAX_REG;
    }

  /* search the device */
  if (!(dev = pci_find_dev (base->bus, base->devfn)))
    {
      write_unlock (&RegList_rwlock);
      spin_unlock (&psi_reg_count_lock);
      return PSI_DEVICE_NF;
    }

  /* fill the structure */
  if (RegList[i].users == 0)
    {
      RegList[i].bus = base->bus;
      RegList[i].devfn = base->devfn;
      RegList[i].base = base->base;
      /* I must mask the size */
      RegList[i].address =
	dev->resource[base->base].start & PCI_BASE_ADDRESS_MEM_MASK;
      if ((dev->resource[base->base].flags & PCI_BASE_ADDRESS_SPACE) ==
	  PCI_BASE_ADDRESS_SPACE_IO)
	RegList[i].typ = _baseIO_;
      else
	RegList[i].typ = _base_;


      get_bar_size (RegList[i].bus, RegList[i].devfn,
		    RegList[i].base, &(RegList[i].size));


    }
  RegList[i].users++;


  /* increase the RegionCounter */
  if (i > psi_reg_count)
    psi_reg_count = i;


  /* set the Region return value */
  *(base->region) = i;

  MOD_INC_USE_COUNT;

#ifdef DEBUG
  printk ("%s: bus[%d]devfn[%d]base[%d] is region %d. (%d times used)\n",
	  DRVNAME, base->bus, base->devfn, base->base, i, RegList[i].users);
  printk ("%s: address: %p\n", DRVNAME, (void *) RegList[i].address);
#endif

  write_unlock (&RegList_rwlock);
  spin_unlock (&psi_reg_count_lock);
  return PSI_OK;
}

PSI_Status
psi_open_conf (tConf * base)
{
  int i = 0;

  spin_lock (&psi_reg_count_lock);
  write_lock (&RegList_rwlock);

  /* search for a region with the same parameters */
  for (i = 0; (RegList[i].typ != _config_ ||
	       RegList[i].bus != base->bus ||
	       RegList[i].devfn != base->devfn) && i <= psi_reg_count; i++);

  /* find first free region */
  if (RegList[i].bus != base->bus && RegList[i].devfn != base->devfn)
    {
      for (i = 0; RegList[i].users > 0 && i < MAXREG; i++);
    }

  if (i == MAXREG)
    {
      write_unlock (&RegList_rwlock);
      spin_unlock (&psi_reg_count_lock);
      return PSI_MAX_REG;
    }

  /* fill the structure */
  if (RegList[i].users == 0)
    {
      RegList[i].bus = base->bus;
      RegList[i].devfn = base->devfn;
      RegList[i].base = -1;	/* Is a configspace */
      RegList[i].typ = _config_;
      RegList[i].size = 64;	/* Size is known. */
    }
  RegList[i].users++;

  /* increase the RegionCounter */
  if (i > psi_reg_count)
    psi_reg_count = i;


  /* set the Region return value */
  *(base->region) = i;

  MOD_INC_USE_COUNT;

#ifdef DEBUG
  printk ("%s: bus[%d]devfn[%d]conf is region %d. (%d times used)\n",
	  DRVNAME, base->bus, base->devfn, i, RegList[i].users);
#endif

  write_unlock (&RegList_rwlock);
  spin_unlock (&psi_reg_count_lock);
  return PSI_OK;
}

PSI_Status
psi_vendor_2_bus (tVendor * vend)
{
  struct list_head *n = pci_devices.next;
  short number = 0;
  PSI_Status status = PSI_VEND_NF;

  while (n != &pci_devices)
    {
      /* search all devices for the right vendor&deviceID */
      struct pci_dev *dev = pci_dev_g (n);
      if (dev->vendor == vend->vendorID)
	{
	  status = PSI_DEV_NF;
	  if (dev->device == vend->deviceID)
	    {
	      status = PSI_NMBR_NF;
	      /* Take the number card with this ID */
	      if (number == vend->number)
		{
		  /* fill the struct */
		  vend->bus = dev->bus->number;
		  vend->devfn = dev->devfn;
		  /* return */
		  status = PSI_OK;
		  return status;
		}

	      /* we have on more device */
	      number++;
	    }
	}
      n = n->next;
    }
  return status;
}

PSI_Status
psi_close_region (tRegion r)
{

  write_lock (&RegList_rwlock);
  if (RegList[(int) r].users == 0)
    {
      write_unlock (&RegList_rwlock);
      return PSI_INVALID_REGION;
    }

  RegList[(int) r].users--;

#ifdef DEBUG
  printk
    ("%s: Closing region %d - Users left: %d - Allocated address: 0x%08lX\n",
     DRVNAME, (int) r, RegList[(int) r].users, RegList[(int) r].address);
#endif /* DEBUG */

  /* who need this.. nobody! */
  if (RegList[(int) r].users == 0)
    {
      if (RegList[(int) r].typ == _mem_)
	{
	  if (RegList[(int) r].size > 0)
	    {
	      /* free the allocated Memory */
	      unsigned long o, i;
	      i = RegList[(int) r].size;
	      /* calc order */
	      for (o = 0; i != 1; i = i >> 1)
		o++;
	      o = (o - PAGE_SHIFT > 0) ? o - PAGE_SHIFT : 0;
	      /* free pages */
	      free_pages ((unsigned long)phys_to_virt(RegList[(int) r].address), o /*order */ );
	    }
	}
#ifdef WITHBIGPHYS
      if (RegList[(int) r].typ == _bigphys_)
	{
	  if (RegList[(int) r].size > 0)
	    {
	      /* free the allocated Memory */
#ifdef DEBUG
	      printk ("%s: Freeing region %d. (%d times used)\n",
		      DRVNAME, (int) r, RegList[r].users);
#endif
	      bigphysarea_free_pages ((caddr_t) phys_to_virt( RegList[(int) r].address ));
	    }
	}
#endif /* WITHBIGPHYS */

      resetReg (&RegList[(int) r]);
    }
  write_unlock (&RegList_rwlock);

  MOD_DEC_USE_COUNT;
  return PSI_OK;
}



PSI_Status
psi_size_region (tSize * size)
{
  long r = (long) (size->region);
  PSI_Status retval = PSI_OK;

  write_lock (&RegList_rwlock);
  /* Is the size allready set ? */
  if (RegList[r].size > 0)
    {
      /* Tell them, how big we are */
      size->size = RegList[r].size;
      write_unlock (&RegList_rwlock);
      return PSI_SIZE_SET;
    }
  switch (RegList[r].typ)
    {
    case _mem_:
      {
	unsigned long i, p, o;
	unsigned long va;
	/* calc pages */
	i = size->size;
	p = i / PAGE_SIZE;
	if (i % PAGE_SIZE)
	  p++;
	/* recalc size */
	i = p * PAGE_SIZE;
	size->size = i;
	/* calc order */
	for (o = 0; i != 1; i = i >> 1)
	  o++;
	o = (o - PAGE_SHIFT > 0) ? o - PAGE_SHIFT : 0;
	/* get new pages */
#ifdef DEBUG
	printk ("%s: trying to allocate:\n", DRVNAME);
	printk (" => Byte:%ld\n    Pages:%ld\n    Order:%ld\n",
		size->size, p, o);
#endif

	/* Test XXX */
	/*RegList[r].address = __get_free_pages (GFP_KERNEL, o );*/
	va = __get_free_pages (GFP_KERNEL, o /*order */ );
	RegList[r].address = virt_to_phys( (void*)va );

#ifdef DEBUG
	writel (RegList[r].address, r);
	printk ("%s: tested: %li (%li)\n", DRVNAME,
		*((long *) (RegList[r].address)), r);
#endif /* DEBUG */
	if (!va)
	  {
	    printk ("%s: trying to FAILED:\n", DRVNAME);
	    printk (" => Byte:%ld\n    Pages:%ld\n    Order:%ld\n",
		    size->size, p, o);
	    RegList[r].size = 0;
	    RegList[r].address = 0;
	  }
	else
	  {
	    RegList[r].size = size->size;
	    RegList[r].address = 0;
	  }
#ifdef DEBUG
	printk ("%s: Allocated address: 0x%08lX\n", DRVNAME,
		RegList[r].address);
#endif /* DEBUG */
	break;
      }
    case _bigphys_:
#ifdef WITHBIGPHYS
      {
	unsigned long i, p;
	unsigned long va;
	/* calc pages */
	i = size->size;
	p = i / PAGE_SIZE;
	if (i % PAGE_SIZE)
	  p++;
	/* recalc size */
	i = p * PAGE_SIZE;
	size->size = i;
#ifdef DEBUG
	printk ("%s: trying to allocate:\n", DRVNAME);
	printk (" => Byte:%ld\n    Pages:%ld\n", size->size, p);
#endif /* DEBUG */
	/* get new pages */
	/* XXX Test wether v2p helps */
	va = (unsigned long) bigphysarea_alloc_pages (p, 0, GFP_KERNEL);
	RegList[r].address = (unsigned long)virt_to_phys ( (void*)va );
	printk ("%s: Allocated address: 0x%08lX (phys: 0x%08lX)\n", DRVNAME,
		va,
		RegList[r].address );
	if (!va)
	  {
	    printk ("%s: FAILED to allocate:\n", DRVNAME);
	    printk (" => Byte:%ld\n    Pages:%ld\n", size->size, p);
	    RegList[r].size = 0;
	    RegList[r].address = 0;
	  }
	else
	  {
	    RegList[r].size = size->size;
	  }
#ifdef DEBUG
	printk ("%s: Allocated address: 0x%08lX\n", DRVNAME,
		RegList[r].address);
#endif /* DEBUG */
	break;
      }
#endif /* WITHBIGPHYS */
    case _baseIO_:
    case _config_:
      retval = PSI_N_SIZEABLE;
      break;
    case _physmem_:
      RegList[r].size = size->size;
      break;
    case _base_:
      {
	unsigned long maxsize;

	get_bar_size (RegList[r].bus, RegList[r].devfn, RegList[r].base,
		      &maxsize);

	if (size->size > maxsize)
	  {
	    size->size = maxsize;
	  }
	RegList[r].size = size->size;
	break;
      }
    case _none_:
      retval = PSI_INVALID_REGION;
      break;
    }
  write_unlock (&RegList_rwlock);
  return retval;
}

PSI_Status
psi_region_stat (tRegStat * regs)
{
  long r = regs->region;
  if (r >= MAXREG)
    return PSI_INVALID_REGION;
  read_lock (&RegList_rwlock);
  if (RegList[r].users == 0)
    {
      read_unlock (&RegList_rwlock);
      return PSI_INVALID_REGION;
    }
  sprintf (regs->rs.name, "%s", RegList[r].name);
  regs->rs.users = RegList[r].users;
  /* XXX Test */
/*   if (RegList[r].typ == _mem_ || RegList[r].typ == _bigphys_) */
/*     regs->rs.address = virt_to_phys ((void *) RegList[r].address); */
/*   else */
    regs->rs.address = RegList[r].address;
  regs->rs.bus = RegList[r].bus;
  regs->rs.devfn = RegList[r].devfn;
  regs->rs.users = RegList[r].users;
  regs->rs.base = RegList[r].base;
  regs->rs.typ = RegList[r].typ;
  regs->rs.size = RegList[r].size;
  read_unlock (&RegList_rwlock);
  return PSI_OK;
}

/**
 **      linux device stuff
 **/

/* Taken from /usr/include/linux/fs.h :
struct file {
        struct file             *f_next, **f_pprev;
        struct dentry           *f_dentry;
        struct file_operations  *f_op;
        mode_t                  f_mode;
        loff_t                  f_pos;
        unsigned int            f_count, f_flags;
        unsigned long           f_reada, f_ramax, f_raend, f_ralen, f_rawin;
        struct fown_struct      f_owner;
        unsigned int            f_uid, f_gid;
        int                     f_error;

        unsigned long           f_version;

        // needed for tty driver, and maybe others
        void                    *private_data;
};

*/
/* Taken from /usr/include/linux/mm.h :
struct vm_area_struct {
        struct mm_struct * vm_mm;       // VM area parameters
        unsigned long vm_start;
        unsigned long vm_end;

        // linked list of VM areas per task, sorted by address
        struct vm_area_struct *vm_next;

        pgprot_t vm_page_prot;
        unsigned short vm_flags;

        // AVL tree of VM areas per task, sorted by address
        short vm_avl_height;
        struct vm_area_struct * vm_avl_left;
        struct vm_area_struct * vm_avl_right;

	// For areas with inode, the list inode->i_mmap, for shm areas,
        // the list of attaches, otherwise unused.
        struct vm_area_struct *vm_next_share;
        struct vm_area_struct **vm_pprev_share;

        struct vm_operations_struct * vm_ops;
        unsigned long vm_offset;
        struct file * vm_file;
        unsigned long vm_pte;                   // shared mem
};

*/

// from drivers/char/mem.c
/*
 * This should probably be per-architecture in <asm/pgtable.h>
 */
static inline unsigned long
pgprot_noncached (unsigned long prot)
{
#if defined(__i386__)
  /* On PPro and successors, PCD alone doesn't always mean 
     uncached because of interactions with the MTRRs. PCD | PWT
     means definitely uncached. */
  if (boot_cpu_data.x86 > 3)
    prot |= _PAGE_PCD | _PAGE_PWT;
#elif defined(__powerpc__)
  prot |= _PAGE_NO_CACHE | _PAGE_GUARDED;
#elif defined(__mc68000__)
  if (CPU_IS_020_OR_030)
    prot |= _PAGE_NOCACHE030;
  /* Use no-cache mode, serialized */
  if (CPU_IS_040_OR_060)
    prot = (prot & _CACHEMASK040) | _PAGE_NOCACHE_S;
#elif defined(__mips__)
  prot = (prot & ~_CACHE_MASK) | _CACHE_UNCACHED;
#endif

  return prot;
}

/*
 * Architectures vary in how they handle caching for addresses 
 * outside of main memory.
 */
static inline int
noncached_address (unsigned long addr)
{
#if defined(__i386__)
  /* 
   * On the PPro and successors, the MTRRs are used to set
   * memory types for physical addresses outside main memory, 
   * so blindly setting PCD or PWT on those pages is wrong.
   * For Pentiums and earlier, the surround logic should disable 
   * caching for the high addresses through the KEN pin, but
   * we maintain the tradition of paranoia in this code.
   */
  return !(test_bit (X86_FEATURE_MTRR, &boot_cpu_data.x86_capability) ||
	   test_bit (X86_FEATURE_K6_MTRR, &boot_cpu_data.x86_capability) ||
	   test_bit (X86_FEATURE_CYRIX_ARR, &boot_cpu_data.x86_capability) ||
	   test_bit (X86_FEATURE_CENTAUR_MCR, &boot_cpu_data.x86_capability))
    && addr >= __pa (high_memory);

#else
  return addr >= __pa (high_memory);
#endif
}

static int
psi_mmap (struct file *file, struct vm_area_struct *vma)
{
  unsigned long offset = vma->vm_pgoff << PAGE_SHIFT;

#ifdef DEBUG
  printk ("%s: psi_mmap vma->vm_pgoff: %lu / 0x%lX\n", DRVNAME, vma->vm_pgoff,
	  vma->vm_pgoff);
  printk ("%s: psi_mmap vm_offset: %lu / 0x%lX\n", DRVNAME, offset, offset);
#endif

  if (offset & ~PAGE_MASK)
    {
      printk ("%s: psi_mmap offset %p is not aligned \n", DRVNAME,
	      (void *) offset);
      return -ENXIO;
    }

  /*
   * Accessing memory above the top the kernel knows about or
   * through a file pointer that was marked O_SYNC will be
   * done non-cached.
   */
  if (noncached_address (offset) || (file->f_flags & O_SYNC))
    {
#ifdef DEBUG
      printk ("%s: psi_mmap offset %p used noncached.\n", DRVNAME,
	      (void *) offset);
#endif
      pgprot_val (vma->vm_page_prot)
	= pgprot_noncached (pgprot_val (vma->vm_page_prot));

    }

  /*
   * Don't dump addresses that are not real memory to a core file.
   */
  if (offset >= __pa (high_memory) || (file->f_flags & O_SYNC))
    vma->vm_flags |= VM_IO;

/*   if (remap_page_range(vma->vm_start, virt_to_phys((void*)(unsigned long)offset) */
/* 		       , vma->vm_end-vma->vm_start, */
/* 		       vma->vm_page_prot)){ */
/*     printk("%s:mapping failed\n", DRVNAME); */
/*     return -EAGAIN; */
/*   } */
  /* XXX */
  if (remap_page_range (vma->vm_start, offset, vma->vm_end - vma->vm_start,
			vma->vm_page_prot))
    {
      printk ("%s:mapping failed\n", DRVNAME);
      return -EAGAIN;
    }

#ifdef DEBUG

  printk ("%s: psi_mmap mmapped %p (phys %p) to %p\n", DRVNAME,
	  (void *) offset,
	  (void *) virt_to_phys ((void *) (unsigned long) offset),
	  (void *) vma->vm_start);
#endif
  return PSI_OK;
}



/*
 * User Space Virt 2 Pyhs mapping
 */


struct task_struct *
pid2task (int pid)
{
  int idx;
  struct task_struct *p;

#ifdef DEBUG
  printk ("Finding task structure for pid %d\n", pid);
#endif

  idx = 0;
  for_each_task (p)
  {
    if (p && p->pid == pid)
      {
	/* process found */
	return p;
      }
    idx++;
  }
  /* process not found */
  printk ("in pid2task(): process not found\n");
  return 0;
}

unsigned long
user_virt2phys (struct task_struct *task, unsigned long virt_addr,
		unsigned long *bus_addr)
{
/*pgd_t *pgd; *//* page directory */
  pgd_t *pgd_entry;		/* entry in page directory for given address */
  pmd_t *pmd_entry;		/* entry in middle directory for buffer */
  pte_t *pte_entry;		/* entry in page table for buffer */
  unsigned long phys_addr;	/* physical address to be returned */
  /*int* ptr; */
/*   void *vk_addr; */
/*   struct page *page; */

#ifdef DEBUG
  printk ("user_virt2phys virt. Addr: 0x%08lX\n", (unsigned long) virt_addr);
#endif
  /* get page directory entry for user buffer address */
  pgd_entry = pgd_offset (task->mm, virt_addr);

  /* get page middle directory entry for buffer */
  /* note: on x86, this is the same as pgd_entry */
  pmd_entry = pmd_offset (pgd_entry, virt_addr);

  /* get page table entry for buffer */
  pte_entry = pte_offset (pmd_entry, virt_addr);

  printk( "psi: pte_entry: 0x%08lX\n", (unsigned long)pte_entry );

  /* check if page is present */
  if (!pte_present (*pte_entry))
    {
#ifdef DEBUG
      printk ("page is not present\n");
#endif
      return 0;
    }


  /* Version that once seemed to work but should not have... */
/*   page_addr = pte_val (*pte_entry) & PAGE_MASK; */
/* #ifdef DEBUG */
/*   printk ("user_virt2phys pte page addr: 0x%08lX\n", */
/* 	  (unsigned long) page_addr); */
/* #endif */
/*   phys_addr = virt_to_phys ((void *) page_addr); */
/*   *bus_addr = virt_to_bus ((void *) page_addr); */


  /* Version that seems to work and luckily should as well... */
#ifdef DEBUG
  printk( "psi: pte_entry: 0x%08lX\n", (unsigned long)pte_entry );
#endif
  phys_addr = pte_val( *pte_entry) & PAGE_MASK;
#ifdef DEBUG
  printk( "psi: pteval(*pte_entry) & PAGE_MASK: 0x%08lX\n", (unsigned long)phys_addr );
#endif
  *bus_addr = (unsigned long)phys_to_virt( phys_addr );
  *bus_addr = (unsigned long)virt_to_bus( (void*)*bus_addr);


  /* Version that once has worked but now doesn't... */
/*   page = (struct page *) pte_page (*pte_entry); */
/* #ifdef DEBUG */
/*   printk ("user_virt2phys page*: 0x%08lX\n", */
/* 	  (unsigned long) page); */
/* #endif */
  
/*   //LockPage( page ); */
/*   vk_addr = (void *) page_address (page); */
/* #ifdef DEBUG */
/*   printk ("user_virt2phys vk_addr: 0x%08lX\n", */
/* 	  (unsigned long) vk_addr); */
/* #endif */
/*   phys_addr = virt_to_phys (vk_addr); */
/* #ifdef DEBUG */
/*   printk ("user_virt2phys phys_addr: 0x%08lX\n", */
/* 	  (unsigned long) phys_addr); */
/* #endif */
/*   *bus_addr = virt_to_bus (vk_addr); */


#ifdef DEBUG
  printk ("user_virt2phys phys page addr 2: 0x%08lX\n",
	  (unsigned long) phys_addr);
#endif

  phys_addr |= (virt_addr & ~PAGE_MASK);
  *bus_addr |= (virt_addr & ~PAGE_MASK);

  /* XXX */
/* #ifdef __i386__ */
/* 	*bus_addr = phys_addr; */
/* #else */
/* #error physical to bus address translation not implemented for architectures other than intel&co, search for phys_to_bus or something. */
/* #endif */



  return phys_addr;
}

static int
psi_virt2phys (tVirtPhysAddr * mem)
{
  tVirtPhysAddr lMem;
  struct task_struct *task;
  copy_from_user (&lMem, mem, sizeof (tVirtPhysAddr));
#ifdef DEBUG
  printk ("psi_virt2phys pid: %d - virt. Addr: 0x%08lX\n", lMem.pid,
	  (unsigned long) lMem.virtAddr);
#endif
  task = pid2task (lMem.pid);
  if (!task)
    {
      printk ("Task structure for pid %d cannot be found\n", lMem.pid);
      return PSI_N_MAPPABLE;
    }
  lMem.physAddr =
    (void *) user_virt2phys (task, (unsigned long) lMem.virtAddr,
			     (unsigned long *) &(lMem.busAddr));
#ifdef DEBUG
  printk ("Physical address found: 0x%08lX: bus: 0x%08lX\n",
	  (unsigned long) lMem.physAddr, (unsigned long) lMem.busAddr);
#endif
  //lMem.busAddr = phys_to_virt( virt_to_bus( lMem.physAddr ) );

  copy_to_user (mem, &lMem, sizeof (tVirtPhysAddr));
  return PSI_OK;
}


/* static int psi_virt2phys( tVirtPhysAddr* mem ) */
/*     { */
/*     tVirtPhysAddr lMem; */
/*     struct task_struct* task; */
/*     copy_from_user( &lMem, mem, sizeof(tVirtPhysAddr) ); */
/*     printk( "psi_virt2phys pid: %d - virt. Addr: 0x%08lX\n", lMem.pid, */
/* 	    (unsigned long)lMem.virtAddr ); */


/*     copy_to_user( mem, &lMem, sizeof(tVirtPhysAddr) ); */
/*     } */


/* Copied from kernel sources */
#ifdef __i386__
static inline long
do_mmap2 (unsigned long addr, unsigned long len,
	  unsigned long prot, unsigned long flags,
	  unsigned long fd, unsigned long pgoff)
{
  int error = -EBADF;
  struct file *file = NULL;

  flags &= ~(MAP_EXECUTABLE | MAP_DENYWRITE);
  if (!(flags & MAP_ANONYMOUS))
    {
      file = fget (fd);
      if (!file)
	goto out;
    }

  down_write (&current->mm->mmap_sem);
  error = do_mmap_pgoff (file, addr, len, prot, flags, pgoff);
  up_write (&current->mm->mmap_sem);
  if (file)
    fput (file);
out:
  return error;
}
#else /* __i386__ */
#error system mmap functions not defined for architectures other than intel&co
#endif /* __i386__ */


static int
psi_dommap (tMMapData * uMapData)
{
  tMMapData kMapData;

  copy_from_user (&kMapData, uMapData, sizeof (tMMapData));
  read_lock (&RegList_rwlock);
  if (kMapData.region >= MAXREG || RegList[kMapData.region].typ == _none_ || RegList[kMapData.region].typ == _baseIO_ || RegList[kMapData.region].typ == _config_)	// XXXX 
    {
      printk ("%s, invalid region %d", DRVNAME, (int) kMapData.region);
      read_unlock (&RegList_rwlock);
      return PSI_INVALID_REGION;
    }

  if (RegList[kMapData.region].typ == _base_
      && RegList[kMapData.region].size == 0)
    {
      unsigned long maxsize;

      get_bar_size (RegList[kMapData.region].bus,
		    RegList[kMapData.region].devfn,
		    RegList[kMapData.region].base, &maxsize);
      kMapData.size = RegList[kMapData.region].size = maxsize;
#ifdef DEBUG
      printk
	("%s psi_dommap determined region size %d bytes for region %d.\n",
	 DRVNAME, kMapData.size, (int) kMapData.region);
#endif
    }

#ifdef DEBUG
  printk
    ("%s psi_dommap mapping %d bytes from region %d, address 0x%lX (region offset %lu)\n",
     DRVNAME, kMapData.size, (int) kMapData.region,
     RegList[kMapData.region].address + kMapData.offset, kMapData.offset);
#endif
  kMapData.addr =
    (void *) do_mmap2 (0, kMapData.size, kMapData.prot, kMapData.flags,
		       kMapData.fd,
		       (RegList[kMapData.region].address +
			kMapData.offset) >> PAGE_SHIFT);
  read_unlock (&RegList_rwlock);
  copy_to_user (uMapData, &kMapData, sizeof (tMMapData));
#ifdef DEBUG
  printk ("%s psi_dommap mapped address: %p\n", DRVNAME, kMapData.addr);
#endif
  if (kMapData.addr == (void *) -1)
    return PSI_SYS_ERRNO + ENXIO;
  return PSI_OK;
}


static int
psi_ioctl (struct inode *inode, struct file *file,
	   unsigned int cmd, unsigned long arg)
{
#ifdef DEBUG
  unsigned int minor = MINOR (inode->i_rdev);
#endif
  PSI_Status status = PSI_OK;

#ifdef DEBUG
  printk ("%s: ioctl %x for minor device %x.\n", DRVNAME, cmd, minor);
#endif

  switch (cmd)
    {
    case PSI_OPEN_MEM:
      status = psi_open_mem ((tMem *) arg);
      break;
    case PSI_OPEN_BIGPHYS:
#ifdef WITHBIGPHYS
      status = psi_open_bigphys ((tMem *) arg);
      break;
#else /* WITHBIGPHYS */
      status = PSI_NOT_IMPL;
      break;
#ifdef DEBUG
      printk ("%s: trying to use bigphys, but not in driver\n", DRVNAME);
#endif /* DEBUG */
#endif /* WITHBIGPHYS */
    case PSI_OPEN_PHYSMEM:
      status = psi_open_physmem ((tPhysmem *) arg);
      break;
    case PSI_OPEN_BASE:
      status = psi_open_base ((tBase *) arg);
      break;
    case PSI_OPEN_CONFIG:
      status = psi_open_conf ((tConf *) arg);
      break;
    case PSI_SIZE_REGION:
      status = psi_size_region ((tSize *) arg);
      break;
    case PSI_VENDOR_2_BUS:
      status = psi_vendor_2_bus ((tVendor *) arg);
      break;
    case PSI_CLOSE_REGION:
      status = psi_close_region (*((tRegion *) arg));
      break;
    case PSI_REGION_STAT:
      status = psi_region_stat ((tRegStat *) arg);
      break;
    case PSI_READ:
      status = psi_read ((tReadWrite *) arg);
      break;
    case PSI_WRITE:
      status = psi_write ((tReadWrite *) arg);
      break;
    case PSI_RESET_USAGE:
      while (MOD_IN_USE)
	MOD_DEC_USE_COUNT;	// Set Usage = 0
      MOD_INC_USE_COUNT;	// Set Usage = 1
      break;
    case PSI_GETPHYSADDR:
      status = psi_virt2phys ((tVirtPhysAddr *) arg);
      break;
    case PSI_DOMMAP:
      status = psi_dommap ((tMMapData *) arg);
      break;
    default:
      status = PSI_NOT_IMPL;
    }
  return status;
}

static int
psi_open (struct inode *inode, struct file *file)
{
  unsigned int minor = MINOR (inode->i_rdev);
  //  unsigned int access = file->f_flags;


#ifdef DEBUG
  printk ("%s: Trying to open minor device %d\n", DRVNAME, minor);
#endif
  /*
     if(((access & O_RDWR)!=0) ||
     ((access & O_WRONLY)!=0) ||
     ((access & O_RDONLY)!=0))
     {
     printk("%s: access method not permitted.\n",DRVNAME);
     return -EACCES;
     }
   */
  MOD_INC_USE_COUNT;
  printk ("%s: open for device %d succeeded\n", DRVNAME, minor);

  return 0;
}

static int
psi_release (struct inode *inode, struct file *file)
{
  unsigned int minor = MINOR (inode->i_rdev);

  printk ("%s: release minor device %x.\n", DRVNAME, minor);
  MOD_DEC_USE_COUNT;

  return 0;
}


/* Taken from /usr/include/linux/fs.h :
struct file_operations {
        loff_t (*llseek) (struct file *, loff_t, int);
        ssize_t (*read) (struct file *, char *, size_t, loff_t *);
        ssize_t (*write) (struct file *, const char *, size_t, loff_t *);
        int (*readdir) (struct file *, void *, filldir_t);
        unsigned int (*poll) (struct file *, struct poll_table_struct *);
        int (*ioctl) (struct inode *, struct file *, unsigned int, unsigned long
);
        int (*mmap) (struct file *, struct vm_area_struct *);
        int (*open) (struct inode *, struct file *);
        int (*flush) (struct file *);
        int (*release) (struct inode *, struct file *);
        int (*fsync) (struct file *, struct dentry *);
        int (*fasync) (int, struct file *, int);
        int (*check_media_change) (kdev_t dev);
        int (*revalidate) (kdev_t dev);
        int (*lock) (struct file *, int, struct file_lock *);
};

*/

static struct file_operations psi_fops = {
  ioctl:psi_ioctl,
  mmap:psi_mmap,
  open:psi_open,
  release:psi_release,
};



/*
 *
 *      kernel-module startup code
 *
 */

static void
release_psi (void)
{
  if (MOD_IN_USE)
    printk ("%s: WARNING driver still in use\n", DRVNAME);
  else
    printk ("%s: driver released\n", DRVNAME);

  unregister_chrdev (PSI_MAJOR, DRVNAME);
}


void
resetReg (pRegionStatus rs)
{
  rs->name[0] = '\0';
  rs->users = 0;
  rs->address = 0;
  rs->bus = 0;
  rs->devfn = 0;
  rs->base = 0;			/* bus = 0..5, conf = -1 */
  rs->typ = _none_;
  rs->size = 0;
}

/*
 *     init_module()
 */

#ifdef MODULE

int
init_module (void)
{
#else
int
init_pci_cards (void)
{
#endif

  struct list_head *n = pci_devices.next;
  int i;

#ifdef DEBUG
  printk ("%s: init_module ENTER\n", DRVNAME);
#endif /* DEBUG */
  printk ("%s: Version %s %s loaded\n", DRVNAME, __DATE__, __TIME__);

  spin_lock (&psi_reg_count_lock);
  write_lock (&RegList_rwlock);
  /* reset the shared memory array */
  for (i = 0; i < MAXREG; i++)
    {
      resetReg (&RegList[i]);
    }

  psi_reg_count = 0;

  write_unlock (&RegList_rwlock);
  spin_unlock (&psi_reg_count_lock);

  if (!n)
    {
      printk ("No device found\n");
      return -EIO;
    }
  else
    {
    /*#ifdef DEBUG*/

      while (n != &pci_devices)
	{
	  struct pci_dev *dev = pci_dev_g (n);
	  printk ("Found vendor/%x/device/%x in bus/%d/devfn/%d\n",
		  dev->vendor, dev->device, dev->bus->number, dev->devfn);
	  n = n->next;
	}
      /*#endif*/

      if (register_chrdev (PSI_MAJOR, DRVNAME, &psi_fops) < 0)
	{
	  printk ("%s: unable to get major %d\n", DRVNAME, PSI_MAJOR);
	  return -EIO;
	}
    }
#ifdef DEBUG
  printk ("%s: init_module DONE\n", DRVNAME);
#endif /* DEBUG */
  return 0;
}


/*
 *     cleanup_module()
 */

#ifdef MODULE

void
cleanup_module (void)
{
  release_psi ();
}

#endif
