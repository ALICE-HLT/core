#include <stdio.h>
#include <stdlib.h>
/*
  #include "pci_usr.h"
  #include "pci_usr_rw_config.h"
  #include "pci_csrs.h"
*/
#include <psi.h>
#include <psi_error.h>
#include "pcitypes.h"


int main( int argc, char** argv )
    {
    PSI_Status retval;
    tRegion region;
    unsigned long s;
    unsigned long p;
    void* addr;
    void* phys;
    void* bus;
    char* usage = "Usage: %s <bigpyhsname> <memory size in 4kB pages>\n";
    char* errP;
    char name[1024];

    if ( argc != 3 )
	{
	fprintf( stderr, usage, argv[0] );
	return -1;
	}

    p = strtoul( argv[2], &errP, 0 );
    if ( *errP )
	{
	fprintf( stderr, usage, argv[0] );
	fprintf( stderr, "Error converting page count '%s'.\n", argv[2] );
	return -1;
	}
    
    sprintf( name, "/dev/psi/bigphys/%s", argv[1] );
    retval = PSI_openRegion( &region, name );
    if ( retval != PSI_OK )
	{
	printf( "Error opening region %s\n", name  );
	return -1;
	}
    
    s = p*4096;

    retval = PSI_sizeRegion( region, &s );
    if ( retval != PSI_OK )
	{
	printf( "Error sizing region %s to %lu bytes\n", name, s );
	PSI_closeRegion( region );
	return -1;
	}

    retval = PSI_mapRegion( region, &addr );
    if ( retval != PSI_OK )
	{
	printf( "Error mapping region %s.\n", name );
	PSI_closeRegion( region );
	return -1;
	}
    
    retval = PSI_getPhysAddress( addr, &phys, &bus );
    if ( retval == PSI_OK )
	{
	printf( "Physical address: 0x%08lX - Bus address: 0x%08lX\n", (unsigned long)phys, (unsigned long)bus );
	}
    else
	{
	printf( "Error determining physical addresses for region %s\n", name );
	phys = addr;
	}

    PSI_closeRegion( region );
    return 0;
    }
