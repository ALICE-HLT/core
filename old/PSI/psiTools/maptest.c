#include <stdio.h>
#include <stdlib.h>
/*
  #include "pci_usr.h"
  #include "pci_usr_rw_config.h"
  #include "pci_csrs.h"
*/
#include <psi.h>
#include <psi_error.h>
#include "pcitypes.h"


int main( int argc, char** argv )
    {
    PSI_Status retval;
    tRegion region;
    unsigned long s;
    void* addr;
    void* phys;
    void* bus;
    char* usage = "Usage: %s <physical address>\n";
    char* errP;
    char name[1024];

    if ( argc != 2 )
	{
	fprintf( stderr, usage, argv[0] );
	return -1;
	}

    s = strtoul( argv[1], &errP, 0 );
    if ( *errP )
	{
	fprintf( stderr, usage, argv[0] );
	fprintf( stderr, "Error converting memory address '%s'.\n", argv[1] );
	return -1;
	}
    
    sprintf( name, "/dev/psi/physmem/0x%08lX", s );
    printf( "Opening %s\n", name );
    retval = PSI_openRegion( &region, name );
    if ( retval != PSI_OK )
	{
	printf( "Error opening region\n"  );
	return -1;
	}
    
    s = 4096;

    retval = PSI_sizeRegion( region, &s );
    if ( retval != PSI_OK )
	{
	printf( "Error sizing region to %lu bytes\n", s );
	PSI_closeRegion( region );
	return -1;
	}

    retval = PSI_mapRegion( region, &addr );
    if ( retval != PSI_OK )
	{
	printf( "Error mapping region.\n" );
	PSI_closeRegion( region );
	return -1;
	}

    printf( "Virtual address: 0x%08lX\n", (unsigned long)addr );
    
    retval = PSI_getPhysAddress( addr, &phys, &bus );
    if ( retval == PSI_OK )
	{
	printf( "Phys: 0x%08lX - Bus: 0x%08lX\n", (unsigned long)phys, (unsigned long)bus );
	}
    else
	{
	printf( "Error determining physical addresses...\n" );
	phys = addr;
	}

    PSI_closeRegion( region );
    return 0;
    }
