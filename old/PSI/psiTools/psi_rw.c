#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*
  #include "pci_usr.h"
  #include "pci_usr_rw_config.h"
  #include "pci_csrs.h"
*/
#include <psi.h>
#include <psi_error.h>
#include "pcitypes.h"



int main( int argc, char* argv[] )
    {
    u16 vendor, device, nr;
    u32 pbus, pdev, pfunc;
    u32 addr, offset, value, bar;
    unsigned long size;
    u32 valuemask = 0;
    u32 readvalue;
    int do_write = 0;
    int i;
    int error, verify = 0;
    int base = 0;
    int useid = 0, usephys = 0, usemem = 0, usebigphys = 0;
    int dump = 0;
    unsigned long s;
	
    char* errorP = NULL;
    char* convErrP;
    int errorArg = -1;
    int address_seen = 0;
    PSI_Status psiStatus;
    tRegion region;
    char deviceName[1024];
    int args_needed = 0;
    unsigned long n, valuecount;
    void* values, *readvalues;
    unsigned long diffcount = 0;
    char* bigphysName = NULL;


    int mode = 0; /* 0: generic, 1: configuration, 2: device bars, 3: physical memory */
	
    char* usage;
    char* usage_bar = "Usage: %s (-D) (-v) (-h) (-d) (-o) (-p) [ \n"
		      "                               -id <vendor-id> <device-id> <card-nr> <bar-nr> <offset> | \n"
		      "                               -slot <bus-nr> <device-nr> <function-nr> <bar-nr> <offset> \n"
		      "                             ] <datasize> <datacount> (<value1> <value2> ...)\n"
		      "       -D: Dump only read out data in hex and suppress any other output. Ignored when writing.\n"
		      "       -v: Verify written values by reading them back.\n"
		      "       -h: Interpret all following numerical values as hexadecimal.\n"
		      "       -d: Interpret all following numerical values as decimal.\n"
		      "       -o: Interpret all following numerical values as octal.\n"
		      "       -p: Interpret all following numerical values according to their prefix (default).\n"
 "       -id <vendor-id> <device-id> <card-nr>: Access card with given vendor and device id. Use the nth (n==card-nr) card in the system.\n"
		      "       -slot <bus-nr> <device-nr> <function-nr>: Access device with the given bus/device/function physical PCI device offset.\n"
		      "       <bar>: Specifies the number of the base address register to access.\n"
		      "       <offset>: The offset to read from/write to.\n"
		      "       <datasize>: The size of each data item (in bytes) in which data is read/written.  Only 1, 2, and 4 are allowed here.\n"
		      "       <datacount>: The number data items to read/write.\n"
		      "       <value?>: The values/data items to write.\n\n"
		      "By default all numerical values are interpreted according to their prefix, '0x' for hexadecimal, `0` for octal, anything else for decimal.\n"
		      "This can be overridden with the -h/-d/-o options.\n";

    char* usage_cnf = "Usage: %s (-D) (-v) (-h) (-d) (-o) (-p) [ \n"
		      "                               -id <vendor-id> <device-id> <card-nr> <offset> | \n"
		      "                               -slot <bus-nr> <device-nr> <function-nr> <offset> \n"
		      "                             ] <datasize> <datacount> (<value1> <value2> ...)\n"
		      "       -D: Dump only read out data in hex and suppress any other output. Ignored when writing.\n"
		      "       -v: Verify written values by reading them back.\n"
		      "       -h: Interpret all following numerical values as hexadecimal.\n"
		      "       -d: Interpret all following numerical values as decimal.\n"
		      "       -o: Interpret all following numerical values as octal.\n"
		      "       -p: Interpret all following numerical values according to their prefix (default).\n"
 "       -id <vendor-id> <device-id> <card-nr>: Access card with given vendor and device id. Use the nth (n==card-nr) card in the system.\n"
		      "       -slot <bus-nr> <device-nr> <function-nr>: Access device with the given bus/device/function physical PCI device offset.\n"
		      "       <offset>: The offset to read from/write to.\n"
		      "       <datasize>: The size of each data item (in bytes) in which data is read/written.  Only 1, 2, and 4 are allowed here.\n"
		      "       <datacount>: The number data items to read/write.\n"
		      "       <value?>: The values/data items to write.\n\n"
		      "By default all numerical values are interpreted according to their prefix, '0x' for hexadecimal, `0` for octal, anything else for decimal.\n"
		      "This can be overridden with the -h/-d/-o options.\n";
	
    char* usage_mem = "Usage: %s (-D) (-v) (-h) (-d) (-o) (-p) [ \n"
		      "                               -memory <address> \n"
		      "                             ] <datasize> <datacount> (<value1> <value2> ...)\n"
		      "       -D: Dump only read out data in hex and suppress any other output. Ignored when writing.\n"
		      "       -v: Verify written values by reading them back.\n"
		      "       -h: Interpret all following numerical values as hexadecimal.\n"
		      "       -d: Interpret all following numerical values as decimal.\n"
		      "       -o: Interpret all following numerical values as octal.\n"
		      "       -p: Interpret all following numerical values according to their prefix (default).\n"
		      "       <address>: The physical memory address to access\n"
		      "       <datasize>: The size of each data item (in bytes) in which data is read/written.  Only 1, 2, and 4 are allowed here.\n"
		      "       <datacount>: The number data items to read/write.\n"
		      "       <value?>: The values/data items to write.\n\n"
		      "By default all numerical values are interpreted according to their prefix, '0x' for hexadecimal, `0` for octal, anything else for decimal.\n"
		      "This can be overridden with the -h/-d/-o options.\n";

    char* usage_gen = "Usage: %s (-D) (-v) (-h) (-d) (-o) (-p) [ \n"
		      "                               -id <vendor-id> <device-id> <card-nr> [config | <bar-nr>] <offset> | \n"
		      "                               -slot <bus-nr> <device-nr> <function-nr> [config | <bar-nr>] <offset> | \n"
		      "                               -memory <address> | \n"
		      "                               -bigphys <bigphysname> \n"
		      "                             ] <datasize> <datacount> (<value1> <value2> ...)\n"
		      "       -D: Dump only read out data in hex and suppress any other output. Ignored when writing.\n"
		      "       -v: Verify written values by reading them back.\n"
		      "       -h: Interpret all following numerical values as hexadecimal.\n"
		      "       -d: Interpret all following numerical values as decimal.\n"
		      "       -o: Interpret all following numerical values as octal.\n"
		      "       -p: Interpret all following numerical values according to their prefix (default).\n"
 "       -id <vendor-id> <device-id> <card-nr>: Access card with given vendor and device id. Use the nth (n==card-nr) card in the system.\n"
		      "       -slot <bus-nr> <device-nr> <function-nr>: Access device with the given bus/device/function physical PCI device offset.\n"
		      "       config: Specifies to access out the device's configuration space.\n"
		      "       <bar>: Specifies the number of the base address register to access.\n"
		      "       <offset>: The offset to read from/write to.\n"
		      "       <address>: The physical memory address to access\n"
		      "       <datasize>: The size of each data item (in bytes) in which data is read/written.  Only 1, 2, and 4 are allowed here.\n"
		      "       <datacount>: The number data items to read/write.\n"
		      "       <value?>: The values/data items to write.\n\n"
		      "By default all numerical values are interpreted according to their prefix, '0x' for hexadecimal, `0` for octal, anything else for decimal.\n"
		      "This can be overridden with the -h/-d/-o options.\n";


    if ( !strncmp( argv[0]+strlen(argv[0])-strlen("pci_config_rw"), "pci_config_rw", strlen("pci_config_rw") ) )
	{
	mode = 1;
	usage = usage_cnf;
	args_needed = 4;
	}
    else if ( !strncmp( argv[0]+strlen(argv[0])-strlen("pci_device_rw"), "pci_device_rw", strlen("pci_device_rw") ) )
	{
	mode = 2;
	usage = usage_bar;
	args_needed = 5;
	}
    else if ( !strncmp( argv[0]+strlen(argv[0])-strlen("mem_rw"), "mem_rw", strlen("mem_rw") ) )
	{
	mode = 3;
	usage = usage_mem;
	args_needed = 1;
	}
    else if ( !strncmp( argv[0]+strlen(argv[0])-strlen("bigphys_rw"), "bigphys_rw", strlen("bigphys_rw") ) )
	{
	mode = 4;
	usage = usage_mem;
	args_needed = 1;
	}
    else
	{
	mode = 0;
	usage = usage_gen;
	args_needed = 5;
	}


    /* Parse the command line arguments. */
    i = 1;
    error = 0;
    while ( i < argc )
	{
	if ( !strcmp( argv[i], "-D" ) )
	    {
	    dump = 1;
	    }
	else if ( !strcmp( argv[i], "-v" ) )
	    {
	    verify = 1;
	    }
	else if ( !strcmp( argv[i], "-h" ) )
	    {
	    base = 16;
	    }
	else if ( !strcmp( argv[i], "-d" ) )
	    {
	    base = 10;
	    }
	else if ( !strcmp( argv[i], "-o" ) )
	    {
	    base = 8;
	    }
	else if ( !strcmp( argv[i], "-p" ) )
	    {
	    base = 0;
	    }
	else if ( (!strcmp( argv[i], "-id" ) || !strcmp( argv[i], "-slot" )) &&
		  ( mode==0 || mode==1 || mode==2) )
	    {
	    if ( i+args_needed >= argc )
		{
		errorP = "Missing arguments for parameter.";
		errorArg = i;
		}
	    if ( !strcmp( argv[i], "-id" ) )
		{
		useid = 1;
		vendor = strtoul( argv[i+1], &convErrP, base );
		if ( *convErrP )
		    {
		    errorP = "Error converting vendor ID.";
		    errorArg = i+1;
		    break;
		    }
		device = strtoul( argv[i+2], &convErrP, base );
		if ( *convErrP )
		    {
		    errorP = "Error converting device ID.";
		    errorArg = i+2;
		    break;
		    }
		nr = strtoul( argv[i+3], &convErrP, base );
		if ( *convErrP )
		    {
		    errorP = "Error converting card nr.";
		    errorArg = i+3;
		    break;
		    }
		i += 4;
		}
	    else 
		{
		usephys = 1;
		pbus = strtoul( argv[i+1], &convErrP, base );
		if ( *convErrP )
		    {
		    errorP = "Error converting bus nr.";
		    errorArg = i+1;
		    break;
		    }
		pdev = strtoul( argv[i+2], &convErrP, base );
		if ( *convErrP )
		    {
		    errorP = "Error converting device nr.";
		    errorArg = i+2;
		    break;
		    }
		pfunc = strtoul( argv[i+3], &convErrP, base );
		if ( *convErrP )
		    {
		    errorP = "Error converting function nr.";
		    errorArg = i+3;
		    break;
		    }
		i += 4;
		}
	    if ( mode==0 )
		{
		if ( !strcmp( argv[i], "config" ) )
		    {
		    i++;
		    mode = 1;
		    }
		else
		    {
		    mode = 2;
		    }
		}
	    if ( mode==2 )
		{
		bar = strtoul( argv[i], &convErrP, base );
		if ( *convErrP )
		    {
		    errorP = "Error converting bar.";
		    errorArg = i;
		    break;
		    }
		i++;
		}
	    if ( mode==1 || mode==2 )
		{
		offset = strtoul( argv[i], &convErrP, base );
		if ( *convErrP )
		    {
		    errorP = "Error converting offset.";
		    errorArg = i;
		    break;
		    }
		i++;
		}
	    i--;
	    }
	else if ( !strcmp( argv[i], "-memory" ) &&
		  ( mode==0 || mode==3) )
	    {
	    mode=3;
	    usemem = 1;
	    if ( i+1 >= argc )
		{
		errorP = "Missing arguments for -slots parameter.";
		break;
		}
	    addr = strtoul( argv[i+1], &convErrP, base );
	    offset = 0;
	    if ( *convErrP )
		{
		errorP = "Error converting address.";
		errorArg = i;
		break;
		}
	    i++;
	    }
	else if ( !strcmp( argv[i], "-bigphys" ) &&
		  ( mode==0 || mode==4) )
	    {
	    mode=3;
	    usebigphys = 1;
	    if ( i+1 >= argc )
		{
		errorP = "Missing arguments for -bigphys parameter.";
		break;
		}
	    bigphysName = argv[i+1];
	    offset = 0;
	    i++;
	    }
	else
	    {
	    if ( i+1>=argc )
		{
		errorP = "Missing datasize or datacount parameter.";
		errorArg = -1;
		break;
		}
	    address_seen = 1;
	    if ( !strcmp( argv[i], "1" ) )
		{
		size = 1;
		valuemask = 0xFF;
		}
	    else if ( !strcmp( argv[i], "2" ) )
		{
		size = 2;
		valuemask = 0xFFFF;
		}
	    else if ( !strcmp( argv[i], "4" ) )
		{
		size = 4;
		valuemask = 0xFFFFFFFF;
		}
	    else
		{
		errorP = "Only values of 1, 2, or 4 allowed for size.";
		errorArg = i;
		break;
		}
	    i++;
	    valuecount = strtoul( argv[i], &convErrP, base );
	    if ( *convErrP )
		{
		errorP = "Error converting value count.";
		errorArg = i+n;
		break;
		}
	    i++;

	    readvalues = malloc( size*valuecount );
	    if ( !readvalues )
		{
		fprintf( stderr, "OUT OF MEMORY (%lu bytes)\n", size*valuecount );
		return -1;
		}

	    if ( i>=argc )
		break;

	    if ( i+valuecount-1>=argc )
		{
		errorP = "Missing values.";
		errorArg = -1;
		break;
		}
	    do_write = 1;

	    values = malloc( size*valuecount );
	    if ( !values )
		{
		fprintf( stderr, "OUT OF MEMORY (%lu bytes)\n", size*valuecount );
		return -1;
		}

	    for ( n = 0; n < valuecount; n++ )
		{
		value = strtoul( argv[i+n], &convErrP, base );
		if ( *convErrP )
		    {
		    errorP = "Error converting value.";
		    errorArg = i+n;
		    break;
		    }
		value &= valuemask;
		switch ( size )
		    {
		    case 1: 
			*(((u8*)values)+n) = (u8)value;
			break;
		    case 2: 
			*(((u16*)values)+n) = (u16)value;
			break;
		    case 4: 
			*(((u32*)values)+n) = (u32)value;
			break;
		    }
		}
	    break;
	    }
	i++;
	}

    if ( !errorP )
	{
	if ( !address_seen )
	    {
	    errorP = "Must specify size parameter.\n";
	    }
	else if ( useid+usephys+usemem+usebigphys!=1 )
	    errorP = "Must specify one of either -id, -slot, -memory, or -bigphys parameter.";
	}

    if ( errorP )
	{
	fprintf( stderr, usage, argv[0] );
	fprintf( stderr, "Error: %s\n", errorP );
	if ( errorArg>=0 )
	    fprintf( stderr, "Offending argument: %s\n", argv[errorArg] );
	return -1;
	}

    if ( do_write )
	dump = 0;
	
    /* Provide the user with some information about what we are going to do... */
    if ( !dump )
	{
	if ( do_write )
	    printf( "Writing %lu %lu byte values to offset 0x%08lX / %lu.\n", valuecount, size, offset, offset );
	else
	    printf( "Reading %lu %lu byte values from offset 0x%08lX / %lu.\n", valuecount, size, offset, offset );
	}


    /* Build the device name of the board according to the passed parameters. */
    if ( usemem )
	{
	sprintf( deviceName, "/dev/psi/physmem/0x%08lX", addr );
	offset = 0;
	}
    else if ( usebigphys )
	{
	sprintf( deviceName, "/dev/psi/bigphys/%s", bigphysName );
	offset = 0;
	}
    else if ( useid )
	{
	switch ( mode )
	    {
	    case 1: sprintf( deviceName, "/dev/psi/vendor/0x%04hX/device/0x%04hX/0x%04hX/conf", vendor, device, nr ); break;
	    case 2: sprintf( deviceName, "/dev/psi/vendor/0x%04hX/device/0x%04hX/0x%04hX/base%u", vendor, device, nr, (unsigned int)bar ); break;
	    }
	}
    else
	{
	switch ( mode )
	    {
	    case 1: sprintf( deviceName, "/dev/psi/bus/0x%04lX/slot/0x%04lX/function/0x%04lX/conf", pbus, pdev, pfunc ); break;
	    case 2: sprintf( deviceName, "/dev/psi/bus/0x%04lX/slot/0x%04lX/function/0x%04lX/base%u", pbus, pdev, pfunc, (unsigned int)bar ); break;
	    }
	}

    psiStatus = PSI_openRegion( &region, deviceName );
    if ( psiStatus != PSI_OK )
	{
	fprintf( stderr, "PSI Error opening region '%s': %s (%d)\n", deviceName, PSI_strerror(psiStatus),
		 (int)psiStatus );
	return -1;
	}

    if ( usemem )
	{
	s = (unsigned long)size*valuecount;
	psiStatus = PSI_sizeRegion( region, &s );
	if ( psiStatus == PSI_SIZE_SET && s >= (unsigned long)size*valuecount )
	    psiStatus = PSI_OK;
	if ( psiStatus != PSI_OK )
	    {
	    fprintf( stderr, "PSI Error sizing region '%s' to %lu bytes: %s (%d) - Resulting size: %lu\n", deviceName, (unsigned long)size*valuecount, 
		     PSI_strerror(psiStatus), (int)psiStatus, s );
	    PSI_closeRegion( region );
	    return -1;
	    }
	//printf( "Sized region to %lu of %lu bytes.\n", s, (unsigned long)size );
	}
    if ( usebigphys )
	{
	tRegionStatus regStat;
	psiStatus = PSI_getRegionStatus( region, &regStat );
	if ( psiStatus != PSI_OK )
	    {
	    fprintf( stderr, "PSI Error getting region status '%s': %s (%d)\n", deviceName, PSI_strerror(psiStatus),
		     (int)psiStatus );
	    PSI_closeRegion( region );
	    return -1;
	    }
	if ( regStat.size < size*valuecount )
	    {
	    fprintf( stderr, "PSI bigphys region '%s' is smaller than required size. Actual size: %lu (0x%08lX) bytes - Required size: %lu (0x%08lX) bytes\n",
		     bigphysName, regStat.size, regStat.size, size*valuecount,size*valuecount );
	    PSI_closeRegion( region );
	    return -1;
	    }
	else
	    {
	    printf( "PSI bigphys region '%s' has size: %lu (0x%08lX) bytes\n",
		     bigphysName, regStat.size, regStat.size );
	    }
	}

    if ( do_write )
	{
	/* Do the write. */
	psiStatus = PSI_write( region, offset, size, size*valuecount, values );
	if ( psiStatus != PSI_OK )
	    {
	    fprintf( stderr, "PSI Error writing value %lu to region '%s': %s (%d)\n", n, deviceName, PSI_strerror(psiStatus),
		     (int)psiStatus );
	    PSI_closeRegion( region );
	    return -1;
	    }
	/* Say what we have done. */
	printf( "Wrote" );
	for ( n = 0; n < valuecount; n++ )
	    {
	    switch ( size )
		{
		case 1: 
		    value = *(((u8*)values)+n);
		    break;
		case 2:
		    value = *(((u16*)values)+n);
		    break;
		case 4:
		    value = *(((u32*)values)+n);
		    break;
		}
	    if ( ! ((n*size) % 32) )
		printf( "\n0x%08lX / %10lu:", offset+n*size, offset+n*size );
	    switch ( size )
		{
		case 1: printf( " 0x%02lX", value ); break;
		case 2: printf( " 0x%04lX", value ); break;
		case 4: printf( " 0x%08lX", value ); break;
		}
	    }
	printf( "\n" );
	for ( n = 0; n < valuecount; n++ )
	    {
	    switch ( size )
		{
		case 1: 
		    value = *(((u8*)values)+n);
		    break;
		case 2:
		    value = *(((u16*)values)+n);
		    break;
		case 4:
		    value = *(((u32*)values)+n);
		    break;
		}
	    if ( ! ((n*size) % 32) )
		printf( "\n0x%08lX / %10lu:", offset+n*size, offset+n*size );
	    switch ( size )
		{
		case 1: printf( " %03lu", value ); break;
		case 2: printf( " %05lu", value ); break;
		case 4: printf( " %010lu", value ); break;
		}
	    }
	printf( "\nFinished writing.\n" );
	}
    else 
	verify = 0;
	
    if ( !do_write || verify )
	{
	if ( !dump )
	    printf( "Read" );
	psiStatus = PSI_read( region, offset, size, size*valuecount, readvalues );
	if ( psiStatus != PSI_OK )
	    {
	    fprintf( stderr, "PSI Error reading from region '%s': %s (%d)\n", deviceName, PSI_strerror(psiStatus),
		     (int)psiStatus );
	    PSI_closeRegion( region );
	    return -1;
	    }
	/* Read the value, either as our only task or to verify a previously written value */
	for ( n = 0; n < valuecount; n++ )
	    {
	    switch ( size )
		{
		case 1: 
		    readvalue = *(((u8*)readvalues)+n);
		    break;
		case 2:
		    readvalue = *(((u16*)readvalues)+n);
		    break;
		case 4:
		    readvalue = *(((u32*)readvalues)+n);
		    break;
		}
	    readvalue &= valuemask;
	    if ( verify )
		{
		switch ( size )
		    {
		    case 1: 
			if ( *(((u8*)readvalues)+n) != *(((u8*)values)+n) )
			    diffcount++;
			break;
		    case 2:
			if ( *(((u16*)readvalues)+n) != *(((u16*)values)+n) )
			    diffcount++;
			break;
		    case 4:
			if ( *(((u32*)readvalues)+n) != *(((u32*)values)+n) )
			    diffcount++;
		    break;
		    }
		}
	    if ( !((n*size) % 32) && !dump)
		printf( "\n0x%08lX / %10lu:", offset+n*size, offset+n*size );
	    switch ( size )
		{
		case 1: printf( " 0x%02lX", readvalue ); break;
		case 2: printf( " 0x%04lX", readvalue ); break;
		case 4: printf( " 0x%08lX", readvalue ); break;
		}
	    }
	printf( "\n" );
	if ( !dump )
	    {
	    for ( n = 0; n < valuecount; n++ )
		{
		switch ( size )
		    {
		    case 1: 
			readvalue = *(((u8*)readvalues)+n);
			break;
		    case 2:
			readvalue = *(((u16*)readvalues)+n);
			break;
		    case 4:
			readvalue = *(((u32*)readvalues)+n);
			break;
		    }
		if ( ! ((n*size) % 32) )
		    printf( "\n0x%08lX / %10lu:", offset+n*size, offset+n*size );
		switch ( size )
		    {
		    case 1: printf( " %03lu", readvalue ); break;
		    case 2: printf( " %05lu", readvalue ); break;
		    case 4: printf( " %010lu", readvalue ); break;
		    }
		}
	    printf( "\nFinished reading.\n" );
	    }
	}

    if ( verify )
	{ 
	if ( diffcount>0 )
	    {
	    fprintf( stderr, "Found %lu differences:\n", diffcount );
	    fprintf( stderr, "Offset:                Written    " );
	    switch ( size )
		{
		case 2: printf( "    " ); break;
		case 4: printf( "             " );
		}
	    printf( "Read      " );
	    switch ( size )
		{
		case 2: printf( "    " ); break;
		case 4: printf( "             " );
		}
	    printf( "XOR       " );
	    switch ( size )
		{
		case 2: printf( "    " ); break;
		case 4: printf( "             " );
		}
	    printf( "\n" );
	    
	    for ( n = 0; n < valuecount; n++ )
		{
		errorP = NULL;
		switch ( size )
		    {
		    case 1: 
			if ( *(((u8*)readvalues)+n) != *(((u8*)values)+n) )
			    errorP = (void*)1;
			break;
		    case 2:
			if ( *(((u16*)readvalues)+n) != *(((u16*)values)+n) )
			    errorP = (void*)1;
			break;
		    case 4:
			if ( *(((u32*)readvalues)+n) != *(((u32*)values)+n) )
			    errorP = (void*)1;
			break;
		    }
		if ( errorP )
		    {
		    fprintf( stderr, "0x%08lX / %10lu: ", offset+n*size, offset+n*size );
		    switch ( size )
			{
			case 1:  fprintf( stderr, " 0x%02lX / %03lu", value, value ); break;
			case 2:  fprintf( stderr, " 0x%04lX / %05lu", value, value ); break;
			case 4: fprintf( stderr, " 0x%08lX / %010lu", value, value ); break;
			}
		    fprintf( stderr, " " );
		    switch ( size )
			{
			case 1:  fprintf( stderr, " 0x%02lX / %03lu", readvalue, readvalue ); break;
			case 2:  fprintf( stderr, " 0x%04lX / %05lu", readvalue, readvalue ); break;
			case 4: fprintf( stderr, " 0x%08lX / %010lu", readvalue, readvalue ); break;
			}
		    fprintf( stderr, " " );
		    switch ( size )
			{
			case 1:  fprintf( stderr, " 0x%02lX / %03lu", (value ^ readvalue), (value ^ readvalue) ); break;
			case 2:  fprintf( stderr, " 0x%04lX / %05lu", (value ^ readvalue), (value ^ readvalue) ); break;
			case 4: fprintf( stderr, " 0x%08lX / %010lu", (value ^ readvalue), (value ^ readvalue) ); break;
			}
		    
		    }
		fprintf( stderr, "\n" );
		}
	    }
	else
	    printf( "Found no differences.\n" );
	}
    
    
    PSI_closeRegion( region );

    return 0;
    }


