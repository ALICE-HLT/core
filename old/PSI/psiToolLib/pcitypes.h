#ifndef _PCITYPES_H_
#define _PCITYPES_H_

#include <limits.h>

#ifdef __cplusplus
extern "C" {
#endif



typedef unsigned char u8;

#if USHRT_MAX==65535
typedef unsigned short u16;
#else // USHRT_MAX==65535

#if UINT_MAX==65535
typedef unsigned int u16;
#else // UINT_MAX==65535

#if ULONG_MAX==65535l
typedef unsigned long u16;
#else // ULONG_MAX==65535l

#error Could not typedef u16

#endif // ULONG_MAX==65535l

#endif // UINT_MAX==65535


#endif // USHRT_MAX==65535

#if USHRT_MAX==4294967295
typedef unsigned short u32;
#else // USHRT_MAX==4294967295

#if UINT_MAX==4294967295
typedef unsigned int u32;
#else // UINT_MAX==4294967295

#if ULONG_MAX==4294967295l
typedef unsigned long u32;
#else // ULONG_MAX==4294967295l

#error Could not typedef u32

#endif // ULONG_MAX==4294967295l

#endif // UINT_MAX==4294967295

#endif // USHRT_MAX==4294967295



    typedef struct
        {
	    unsigned short fVendorID_req;
	    unsigned short fDeviceID_req;
	    unsigned short fCommandReg_req;
	    unsigned short fStatusReg_req;
	    unsigned char fRevID_req;
	    unsigned int fClassCode_req:24;
	    unsigned char fCacheLineSize;
	    unsigned char fLatencyTimer;
	    unsigned char fHeadertype_req;
	    unsigned char fBIST;
	    unsigned int fBaseAddr[6];
	    unsigned int fCISPtr;
	    unsigned short fSubVendorID;
	    unsigned short fSubSystemID;
	    unsigned int fROMBaseAddr;
	    unsigned int fReserved[2];
	    unsigned char fIRQLine;
	    unsigned char fIrqPin;
	    unsigned char fMinGnt;
	    unsigned char fMaxLat;
        }
    PCI_CSR;


    typedef struct
        {
	    unsigned short fVendorID_req;
	    unsigned short fDeviceID_req;
	    unsigned short fCommandReg_req;
	    unsigned short fStatusReg_req;
	    unsigned char fRevID_req;
	    unsigned int fClassCode_req:24;
	    unsigned char fCacheLineSize;
	    unsigned char fLatencyTimer;
	    unsigned char fHeadertype_req;
	    unsigned char fBIST;
	    unsigned int fBaseAddr[2];
	    unsigned char fPrimBusNo_req;
	    unsigned char fSecBusNo_req;
	    unsigned char fSubordBusNo_req;
	    unsigned char fSecLatTimer_req;
	    unsigned char fIOBase;
	    unsigned char fIOLimit;
	    unsigned short fSecStatus_req;
	    unsigned short fMemBase_req;
	    unsigned short fMemLimit_req;
	    unsigned short fPrefMemBase;
	    unsigned short fPrefMemLimit;
	    unsigned int fPrefBaseUp;
	    unsigned int fPrefLimitUp;
	    unsigned short fIOBaseUp;
	    unsigned short fIOLimitUp;
	    unsigned int fReserved;
	    unsigned int fROMBaseAddr;
	    unsigned char fIRQLine;
	    unsigned char fIrqPin;
	    unsigned short fBridgeControl_req;
        }
    PCI_BRIDGE_CSR;


#define PCI_MAX_PHYS_BUS_NR  0x10
#define PCI_MAX_PHYS_DEV_NR  0x20
#define PCI_MAX_PHYS_FUNC_NR 0x08
#define PCI_MAX_DEVICE_COUNT (PCI_MAX_PHYS_BUS_NR*PCI_MAX_PHYS_DEV_NR*PCI_MAX_PHYS_FUNC_NR)


    typedef struct
	{
	    unsigned long long fAddress; 
	    unsigned long long fSize;
	    unsigned char fIsMemory;
	    unsigned char      fIs64; /* 1 if 64 bit address  */
	    short      fBridgeSecBus; /* Set to secondary bus on a bridge if region is 
					     ** bridge prefetchable memory region, -1 otherwise */
	    short      fBridgePrimBus; /* Set to primary bus on a bridge if region is 
					      ** bridge prefetchable memory region, -1 otherwise */
	    unsigned char      fIsPrefetchable;
	}
    PCI_BAR;


#ifdef __cplusplus
}
#endif


#endif /* _PCITYPES_H_ */
