/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "psiToolLib.h"
#include <psi_error.h>

int PTL_testRegion( tRegion region, unsigned long offset, unsigned long size, int test, 
		    unsigned loop_count, PTL_CompareCallbackFunc compareCallback )
    {
    unsigned long bytes_tested = 0;
    unsigned long thisSizeBytes;
    unsigned long thisSize;
    unsigned long mask;
    unsigned long pattern, patternread, patternreread;
    unsigned ilc;
    unsigned long testSize;
    unsigned long readwrite;
    unsigned long i;
    PSI_Status psiStatus;

    while ( bytes_tested < size )
	{
	/* Determine the size of each transaction */
	/* First check for alignment: */
	switch ( (bytes_tested) & 0x3 )
	    {
	    case 0: thisSizeBytes = 4; break;
	    case 2: thisSizeBytes = 2; break;
	    case 1: /* Fallthrough */
	    case 3: thisSizeBytes = 1; break;
	    }
	/* Is this too much to do? */
	if ( thisSizeBytes > (size-bytes_tested) )
	    {
	    switch ( size-bytes_tested )
		{
		case 3: thisSizeBytes = 1; break;
		default: thisSizeBytes = size-bytes_tested; break;
		}
	    }
	/* Now set the PSI size and the pattern. */
	switch ( thisSizeBytes )
	    {
	    case 4: thisSize = _dword_; mask = 0xFFFFFFFF; break;
	    case 2: thisSize = _word_; mask = 0xFFFF; break;
	    case 1: thisSize = _byte_; mask = 0xFF; break;
	    }
	
	/* Determine the loop amount for each test */
	switch ( test )
	    {
	    case 0: /* Fallthrough */
	    case 1: testSize = thisSizeBytes*8; break;
	    default: testSize = 2; break;
	    }
	
	for ( ilc = 0; ilc < loop_count; ilc++ )
	    {
	    /* Do the loop first writing, then reading. */
	    for ( readwrite = 0; readwrite < 2; readwrite++ )
		{
		/* Do the loops for each test */
		for ( i = 0; i < testSize; i++ )
		    {
		    /* Set the pattern for each test. */
		    switch ( test )
			{
			case 0: /* Walking ones */
			    pattern = 1 << i; break;
			case 1: /* Walking zeroes */
			    pattern = ~(1 << i); break;
			case 2: /* Full bits */
			    if ( !i )
				pattern = 0xFFFFFFFF; 
			    else 
				pattern = 0x00000000;
			    break;
			case 3: /* Flipping bits */
			    if ( !i )
				pattern = 0xAAAAAAAA; 
			    else 
				pattern = 0x55555555;
			    break;
			}
		    /* Apply the mask to the pattern. */
		    pattern &= mask;
		    
		    if ( !readwrite )
			{
			/* Do the write. */
			psiStatus = PSI_write( region, bytes_tested, thisSize, thisSize, &pattern );
			if ( psiStatus != PSI_OK )
			    {
#ifdef DO_OUTPUT
			    fprintf( stderr, "PSI Error writing pattern %d (0x%08lX/%lu) for test %d (%s) to region '%s': %s (%d)\n", 
				     i, pattern, pattern, test, testnames[test], deviceName, PSI_strerror(psiStatus),
				     (int)psiStatus );
#endif
			    PSI_closeRegion( region );
			    return -1;
			    }
			}
		    else
			{
			
			/* Read the value back. */
			psiStatus = PSI_read( region, bytes_tested, thisSize, thisSize, &patternread );
			if ( psiStatus != PSI_OK )
			    {
#ifdef DO_OUTPUT
			    fprintf( stderr, "PSI Error reading pattern %d (0x%08lX/%lu) for test %d (%s) to region '%s': %s (%d)\n", 
				     i, pattern, pattern, test, testnames[test], deviceName, PSI_strerror(psiStatus),
				     (int)psiStatus );
#endif
			    PSI_closeRegion( region );
			    return -1;
			    }
			patternread &= mask;
			if ( patternread != pattern )
			    {
			    /* Read the value back a second time (Determine read or write error). */
			    psiStatus = PSI_read( region, bytes_tested, thisSize, thisSize, &patternreread );
			    if ( psiStatus != PSI_OK )
				{
#ifdef DO_OUTPUT
				fprintf( stderr, "PSI Error reading pattern %d (0x%08lX/%lu) for test %d (%s) to region '%s': %s (%d)\n", 
					 i, pattern, pattern, test, testnames[test], deviceName, PSI_strerror(psiStatus),
					 (int)psiStatus );
#endif
				PSI_closeRegion( region );
				return -1;
				}
			    
#ifdef DO_OUTPUT
			    fprintf( stderr, "ERROR: Read back pattern does not match written pattern.\n" );
			    fprintf( stderr, "Pattern written; 0x%08lX / %lu.\n", pattern, pattern );
			    fprintf( stderr, "Pattern read; 0x%08lX / %lu.\n", patternread, patternread );
			    fprintf( stderr, "Pattern reread; 0x%08lX / %lu.\n", patternread, patternread );
			    fprintf( stderr, "XOR written/read: 0x%08lX / %lu.\n", (pattern ^ patternread), (pattern ^ patternread ) );
			    fprintf( stderr, "XOR written/reread: 0x%08lX / %lu.\n", (pattern ^ patternreread), (pattern ^ patternreread ) );
			    fprintf( stderr, "XOR read/reread: 0x%08lX / %lu.\n", (patternread ^ patternreread), (patternread ^ patternreread ) );
#endif
			    if ( compareCallback )
				(*compareCallback)( &pattern, &patternread, &patternreread, region, bytes_tested, thisSizeBytes );
			    }
			}
		    }
		}
	    }
	
	bytes_tested += thisSizeBytes;
	}
    return 0;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
