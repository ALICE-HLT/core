/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "psiToolLib.h"
#include "pcitypes.h"
#include <psi.h>
#include <psi_error.h>
#include <errno.h>


/*
** Find the physical address corresponding to the specified logical one.
** In other words: Find the bus and slot of the nth card with the specified
** vendor and device id.
*/
PSI_Status PTL_find_pci_device( u16 vendor, u16 device, u32 n, u16* bus, u16* dev, u16* func )
    {
    u16 pbus, pdev, pfunc;
    u32 nr = 0;
    PCI_CSR csr;
    PSI_Status psiStatus;

    /* Loop over all the possible busses, devices and functions. */
    for ( pbus = 0; pbus < PCI_MAX_PHYS_BUS_NR; pbus++ )
	{
	for ( pdev = 0; pdev < PCI_MAX_PHYS_DEV_NR; pdev++ )
	    {
	    for ( pfunc = 0; pfunc < PCI_MAX_PHYS_FUNC_NR; pfunc++ )
		{
		psiStatus = PTL_read_phys_pci_csr( pbus, pdev, pfunc, &csr );
		if ( psiStatus != PSI_OK )
		    continue;
		if ( (csr.fVendorID_req==vendor || vendor==0xFFFF) && (csr.fDeviceID_req==device || device==0xFFFF) )
		    {
		    if ( nr==n )
			{
			*bus = pbus;
			*dev = pdev;
			*func = pfunc;
			return PSI_OK;
			}
		    nr++;
		    }
		}
	    }
	}
    return PSI_SYS_ERRNO+ENODEV;
    }







/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
