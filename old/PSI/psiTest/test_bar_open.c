/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <stdio.h>
#include <psi.h>
#include <psi_error.h>
#include <pcitypes.h>

#define VENDOR ((u16)0x11C8)
#define DEVICE ((u16)0xD667)
#define NR ((u16)0)
#define BARCNT 3

int main( int argc, char** argv )
    {
    PSI_Status psiStatus;
    tRegion region;
    tRegionStatus regionStatus;
    char name[1024];
    int i;
    
    sprintf( name, "/dev/psi/vendor/0x%04hX/device/0x%04hX/%hu/conf",
	     VENDOR, DEVICE, NR );
    printf( "Opening region '%s'.\n", name );
    psiStatus = PSI_openRegion( &region, name );
    if ( psiStatus != PSI_OK )
	{
	printf( "Error opening region '%s': %s (%d).\n",
		name, PSI_strerror(psiStatus), (int)psiStatus );
	return -1;
	}
    psiStatus = PSI_getRegionStatus( region, &regionStatus );
    if ( psiStatus != PSI_OK )
	{
	printf( "Error determining status for region '%s': %s (%d).\n",
		name, PSI_strerror(psiStatus), (int)psiStatus );
	return -1;
	}
    printf( "Region '%s' size: 0x%08lX / %lu.\n",
	    name, regionStatus.size, regionStatus.size );
    PSI_closeRegion( region );
    

    for ( i = 0; i < BARCNT; i++ )
	{
	sprintf( name, "/dev/psi/vendor/0x%04hX/device/0x%04hX/%hu/base%d",
		 VENDOR, DEVICE, NR, i );
	printf( "Opening region '%s'.\n", name );
	psiStatus = PSI_openRegion( &region, name );
	if ( psiStatus != PSI_OK )
	    {
	    printf( "Error opening region '%s': %s (%d).\n",
		    name, PSI_strerror(psiStatus), (int)psiStatus );
	    return -1;
	    }
	psiStatus = PSI_getRegionStatus( region, &regionStatus );
	if ( psiStatus != PSI_OK )
	    {
	    printf( "Error determining status for region '%s': %s (%d).\n",
		    name, PSI_strerror(psiStatus), (int)psiStatus );
	    return -1;
	    }
	printf( "Region '%s' size: 0x%08lX / %lu.\n",
		name, regionStatus.size, regionStatus.size );
	PSI_closeRegion( region );
	}
    return 0;
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
