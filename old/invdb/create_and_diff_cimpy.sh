#!/bin/bash

# This script uses the mof compiler from sblim with a sqlalchemy backend to 
# produce a python implementation of the CIM model.
# The compiler with the sqlalchemy backend can be downloaded 
# from here: http://git.flekke.org/?p=mofc.git;a=summary

MOF_BASE=./mof-experimental-2.29/

[[ ! -d output.old ]] && mkdir output.old
[[ ! -d output.new ]] && mkdir output.new

rm output.new/*

# sqlalchemy version
~/unison/utvikling/build/mofc/mofcsqlalchemy \
	-I $MOF_BASE/ -i $MOF_BASE/qualifiers.mof -d output.new \
	$MOF_BASE/System/CIM_ComputerSystem.mof \
	$MOF_BASE/Core/CIM_System.mof \
	$MOF_BASE/Core/CIM_EnabledLogicalElement.mof \
	$MOF_BASE/Core/CIM_LogicalElement.mof \
	$MOF_BASE/Core/CIM_ManagedSystemElement.mof \
	$MOF_BASE/Core/CIM_ManagedElement.mof \
	$MOF_BASE/Device/CIM_Processor.mof \
	$MOF_BASE/Core/CIM_LogicalDevice.mof \
	$MOF_BASE/Device/CIM_EthernetPort.mof \
	$MOF_BASE/Device/CIM_NetworkPort.mof \
	$MOF_BASE/Device/CIM_LogicalPort.mof \
	$MOF_BASE/Device/CIM_ProcessorCapabilities.mof \
	$MOF_BASE/Core/CIM_EnabledLogicalElementCapabilities.mof \
	$MOF_BASE/Core/CIM_Capabilities.mof \
	$MOF_BASE/Device/CIM_Memory.mof \
	$MOF_BASE/Core/CIM_StorageExtent.mof \
	$MOF_BASE/Device/CIM_DiskDrive.mof \
	$MOF_BASE/Device/CIM_MediaAccessDevice.mof \
	$MOF_BASE/Device/CIM_DiskPartition.mof \
	$MOF_BASE/Device/CIM_MediaPartition.mof \
	$MOF_BASE/System/CIM_OperatingSystem.mof \
	$MOF_BASE/Application/CIM_BIOSElement.mof \
	$MOF_BASE/Application/CIM_SoftwareElement.mof \
	$MOF_BASE/Core/CIM_SystemDevice.mof \
	$MOF_BASE/Core/CIM_SystemComponent.mof \
	$MOF_BASE/Core/CIM_Component.mof \
	$MOF_BASE/Core/CIM_Dependency.mof \
	$MOF_BASE/System/CIM_RunningOS.mof \
	$MOF_BASE/Application/CIM_SystemBIOS.mof \
	$MOF_BASE/System/CIM_LocalFileSystem.mof \
	$MOF_BASE/System/CIM_FileSystem.mof \
	$MOF_BASE/System/CIM_HostedFileSystem.mof \
	$MOF_BASE/Physical/CIM_Rack.mof \
	$MOF_BASE/Physical/CIM_PhysicalFrame.mof \
	$MOF_BASE/Physical/CIM_PhysicalPackage.mof \
	$MOF_BASE/Core/CIM_PhysicalElement.mof \
	$MOF_BASE/Physical/CIM_Chassis.mof \
	$MOF_BASE/Physical/CIM_ChassisInRack.mof \
	$MOF_BASE/Physical/CIM_Container.mof \
	$MOF_BASE/Core/CIM_Location.mof \
	$MOF_BASE/Core/CIM_SystemPackaging.mof \
	$MOF_BASE/Core/CIM_ElementLocation.mof \
	$MOF_BASE/Device/CIM_GenericDiskPartition.mof \
	$MOF_BASE/Physical/CIM_MemoryCapacity.mof \
	$MOF_BASE/Physical/CIM_PhysicalCapacity.mof \
	$MOF_BASE/Physical/CIM_PhysicalMemory.mof \
	$MOF_BASE/Physical/CIM_Chip.mof \
	$MOF_BASE/Physical/CIM_PhysicalComponent.mof \
	$MOF_BASE/Core/CIM_AbstractComponent.mof \
	$MOF_BASE/Device/CIM_DisplayController.mof \
	$MOF_BASE/Device/CIM_Controller.mof \
	$MOF_BASE/Device/CIM_IBPort.mof \
	$MOF_BASE/Core/CIM_RedundancySet.mof \
	$MOF_BASE/Core/CIM_SystemSpecificCollection.mof \
	$MOF_BASE/Core/CIM_Collection.mof

# These not needed (yet)?
#	mof-2.26/Device/CIM_AssociatedProcessorMemory.mof \
#	mof-2.26/Device/CIM_AssociatedMemory.mof \

echo -e "\nDIFFERENCE NEW AND OLD OUTPUT DIRECTORY"
echo -e "#######################################"
diff -r output.new output.old

#echo -e "\n\nDIFFERENCE NEW OUTPUT DIR AND CURRENT CIMPY"
#echo -e "###########################################"
#diff -r output.new ../cimpy

#echo -e "\n\nDIFFERENCE OLD OUTPUT DIR AND CURRENT CIMPY"
#echo -e "###########################################"
#diff -r output.old ../cimpy

