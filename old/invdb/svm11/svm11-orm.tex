%\documentclass[a4paper,10pt]{article}
\documentclass[english,conference]{IEEEtran}
\usepackage[utf8x]{inputenc}
\usepackage{url}%oyst
%\usepackage[T1]{fontenc}
%\usepackage{babel}
\usepackage{url}
\usepackage{graphicx}
\usepackage[unicode=true]{hyperref}
\usepackage{breakurl}

\author{\IEEEauthorblockN{Marian Hermann\IEEEauthorrefmark{1}, Øystein S. Haaland\IEEEauthorrefmark{2}, Camilo Lara\IEEEauthorrefmark{1}, Jochen Ulrich\IEEEauthorrefmark{1}, Dieter Röhrich\IEEEauthorrefmark{2} and Udo Kebschull\IEEEauthorrefmark{1}}
\\
\and
\IEEEauthorblockA{\IEEEauthorrefmark{1}Heidelberg University\\
Kirchhoff Institute for Physics\\
Heidelberg, Germany\\
Email: \{mhermann,lara,julrich,kebschull\}@kip.uni-heidelberg.de}
\and
\IEEEauthorblockA{\IEEEauthorrefmark{2}University of Bergen\\ Department of Physics and Technology\\
Bergen, Norway\\ Email: \{Oystein.Haaland,Dieter.Rohrich\}@ift.uib.no}
}

\title{Object-Relational Mapping for the Common Information Model}


\begin{document}

\maketitle
\begin{abstract}
This paper presents a way to use Object Relational Mapping for persistent 
storage of the Common Information Model to Relational Database Systems.
Transactional access to the stored data as well as enhanced data integrity are 
the main advantages.
Another important aspect is the capability to provide access to the stored 
model for multiple programming languages using automatically generated
classes. This is realized by making the generated implementations compatible 
on SQL-level, so that they can access the same database. Hibernate and 
sqlalchemy have been chosen as pilot candidates for implementing this 
interoperability.
\end{abstract}



\section{Introduction}



%Why CIM, Why RDBS, Why ORM
%Point out shortcomings of existing CIM related software (no transactionality)


The management of computer systems requires information about the
managed environment. Object oriented models allow working on different
levels of abstraction and are therefore an attractive choice for handling
the complexity of heterogeneous environments. The Common Information
Model (CIM)\cite{CIMInfra} offers a detailed object oriented model
of the hard- and software of computer systems (and it is extendible
through the CIM meta schema).

To be able to store inventory information persistently and at the
same time make it available for access in a distributed and concurrent environment,
requires the use of a reliable storage back-end.

Common CIM server implementations aim at integration into the Web-Based Enterprise Management (WBEM)
architecture \cite{wbem}
where instances are meant to be handled by providers. The integrity mechanisms of their instance
data storages are not as elaborate as in common database systems. Particularly distributed
and concurrent access without transaction support is an issue. \cite{HermannDipl}
%TODO mention/ref wbem/cim/xml transactions


The intuitive way of working with object oriented information in management
applications is using application programming language objects. Object
Relational Mapping (ORM) \cite{ORIM} brings together the attractiveness of reliable
storage in Relational Database Systems (RDBS) and object oriented programming.


\subsection{ORM}

ORM is a programming technique that can be applied to any object model
to implement persistent storage of objects in a RDBS. Software packages
that formalize this approach are called object relational mappers.

The advantage of ORM is the sum of the advantages of the two concepts
it combines: Object oriented programming encourages reuse and modularity through encapsulation and
inheritance. That makes it easier to work at different levels of abstraction.
RDBS are proven technology supporting data integrity through referential integrity
constraints and transactionality under concurrent access. The interface to RDBS is standardized in
the Structured Query Language (SQL)\cite{SQL}, while object centric databases
tend to have more vendor specific interfaces. \cite{HermannDipl}

In addition, the fact that ORM is a layer in-between a model and permanent
storage bring benefits of its own, as it means that the database back-end
is interchangeable. This makes it easier to scale up to more capable
database back-ends during the life-time of the application.

ORM is a well-established technology, but requires special considerations
when used with the CIM. This is described in detail in section \ref{sect:mapping}.


\subsection{CIM}

CIM is based on the CIM meta schema and has typical features of object
oriented models such as classes and inheritance. In CIM, relationships
between objects are objects themselves, enabling classification, reuse
and different levels of abstraction also on these objects.
Qualifiers are used to provide metadata for schema elements. Among
other things, qualifiers can constrain values of properties and determine
which properties make up the identity of an object.

Unlike objects in programming languages, the identity of a CIM instance
is determined by a set of flagged properties. The CIM meta schema
defines a set of data types used in the CIM. Like common in programming
languages there are abstract and concrete classes in the CIM. All these
specialities have been taken into consideration when doing ORM for
the CIM. 

The language used to describe schema elements is called Managed Object
Format (MOF) \cite{mof}.


\subsection{Goals}

The main goal of the presented approach is to achieve reliable persistent
storage for CIM data in relational databases by using object relational
mapping. This will allow transactional access on data and object
oriented exchange of information between applications. 

Secondly, it will be investigated if it is possible to arrive at a
common mapping for ORMs across programming languages, so that applications
using different programming languages will be able to exchange information
via a common database. Solutions have been developed for two pilot
languages; Java and Python, that can automatically generate programming
language class definitions for CIM classes, the corresponding database
schema and the mapping between them.

\section{The Mapping -- Basics}

The difference between data and information models is reflected in CIM. As an information model the
CIM addresses an abstract representation of a certain domain that is independent of any concrete
representation. For a concrete representation of CIM information a data model is needed that
corresponds to specific technology. Such representations are for instance the content of a
relational database or objects in the memory of an application program.

\subsection{Hierarchies and the Level for Mapping}
Meta models are models of models which means there can be multi-layer model hierarchies. The CIM meta schema models the elements of the CIM, particularly classes and instances. It is defined as an UML \cite{UML} user model.

When mapping different modeling hierarchies one has to decide
which levels are mapped to each other.
For mapping the CIM to a relational model or to a specific programming language, there are basically two options:
\begin{itemize}
 \item Mapping at the level of CIM meta schema. On this level, CIM meta objects (e. g. "Class", "Qualifier", "Instance") are mapped as database tables or programming language classes and classes are
 mapped as relational tuples or objects in the application. The main advantage is the flexibility, as no schema specific classes or tables are needed. Disadvantages affect constraints and intuitivity.
 \item Mapping at the level of CIM schema. A CIM class corresponds to a programming language class or a database table, which is intuitive. Constraints can be directly mapped. A disadvantage is the lack of flexibility: Corresponding to a schema a specific implementation and a specific database schema are needed.
\end{itemize}

The decision has been to map at the level of CIM schema.
The main disadvantage is encountered by generating implementations, database schemata and mappings
automatically for any schema defined in the CIM meta schema.
%This effort brings ...
The effort of automated generation  brings
the flexibility available when mapping at a meta-level
to the mapping at the level of CIM schema.

% When mapping different modeling hierarchies one can define the mapping at the level of the CIM meta
% schema which means that the CIM meta objects (like "Class", "Qualifier", etc.) are mapped as
% database tables and programming language classes or the mapping can be at the level of the CIM schema which
% means that CIM classes correspond to database tables and programming language classes. The decision
% of this work is to map at the level of the CIM schema because it is intuitive and also allows the
% mapping of constraints. The main disadvantage is that the programming language classes and the
% database schema are specific to the given CIM schema. This is encountered by generating
% implementations, database schemata and mappings automatically for the given schema.


\subsection{Different Mappings}
To realize a mapping it is necessary to define mappings between each of the used models: 
\begin{itemize}
\item CIM to relational model: The schema is mapped into a relational database schema, and instances
are mapped into relations of the database value. 
\item CIM to specific programming language: For a specific object oriented programming
language the schema is mapped into a set of class definitions. Instances are mapped into instances of these classes. 
\item The actual object-relational mapping: It maps sets of programming language classes and relational database
schemata as well as instances of such classes and relation values.
This object-relational mapping is directly determined by the two previous ones.
\end{itemize}
The challenge here is to identify information that determines these
mappings and group it accordingly.


\section{The Mapping -- Details}

\label{sect:mapping} ORM for the CIM requires to deal with challenges
commonly arising in ORM due to the so-called object relational impedance
mismatch \cite{ORIM} --
such as dealing with inheritance or object identity. This needs to
be considered with the CIM in mind. However it is also required to deal with
challenges specific for the CIM, which are the mapping of the data types
of the CIM meta schema to types of the RDBS and the programming languages,
as well as dealing with qualifiers. In particular qualifiers constrain
values or determine the key properties that make up object identity.

Corresponding to the description in the introduction
of \cite{CIMInfra}, an ORM for the CIM maps the content of CIM, which
is the schema that is defined in the MOF, to a relational database
schema enabling that database to store instances. Applications use language
specific objects to work with a model. Hence this approach offers
shared access to the information in two ways: 
\begin{itemize}
\item {}``Applications can exchange information when they can bind to the
application objects{}`` 
\item {}``ORM-level'' If the ORM layer is constructed in a compatible
way (so that the resulting SQL from the ORM's can work on the same
data set), it can be used across programming languages and frameworks
as kind of a common language agnostic data access API for the CIM. 
\end{itemize}

The CIM describes three aspects of objects: Their structure (inheritance
and properties of classes), their behavior (methods) and constraints
ensuring the integrity of object data (qualifiers).
For a mapping of the CIM these aspects are determined:

\subsection{Mapping of the Structure}

The mapping of the structural part of a model deals with these challenges: 
\begin{itemize}
\item Naming of elements generally can't be preserved, because different
technologies come with different constraints on allowed names, e. g.
reserved names and limited lengths.
\item Suitable correspondents of CIM data types have to be identified or
created in the database and the programming language.
\item The relational model of data lacks the concept of classes and inheritance. 
\end{itemize}

\subsubsection{Naming}

The structural part of a class in the CIM consists of a set of properties
indexed by their names and of an identifier of the superclass.
It can be written as $S=(C,P)$ where $C$ holds the information on
class and superclass and $P=\{p_{i}\}$ is the set of properties.
The properties $p_{i}=(n_{i},T_{i})$ have name $n_{i}$ and type
$T_{i}$. Analogous definitions can be used for classes of object-oriented
programming languages which leads to a straightforward mapping.
E. g. the structural part of a CIM property $p=(n,T)$ can be mapped
to a java property $p_{J}$ as follows: $p\mapsto p_{J}:p_{J}=(javaname(n),javatype(T))$.
Analogously CIM properties can be mapped into name domain pairs $N,D$
as components of a relational schema: $p\mapsto(N,D):(N,D)=(attname(n),attdom(T))$.
At this point the mapping is about defining deterministic functions
to receive a valid name and to identify suitable types for each model.

\subsubsection{Data Types}
Some of the data types from the CIM meta schema have direct counterparts in SQL or in programming
languages, others do not. For programming languages the types can be mapped by implementing a
suitable class, if no such type is available. For SQL the numeric types,
strings and chars have either direct counterparts or limitations of size and sign can be realized
using constraints. Issues arise for the CIM data type ``datetime'', for reference data types and for
arrays. Datetime is complex because it supports precision and intervals of time as well as points
in time. An easy way to map it is using a fixed length string with a programming language class
that provides an intuitive handling of date and time. References can be mapped using a string
object path or using surrogate keys which enable referential constraints on data integrity. Arrays
can be mapped into separate array relations which enables lazy loading on access to these arrays.

\subsubsection{Inheritance}

The complex part of structural mapping is to map class inheritance
into a relational model, because relations lack an equivalent concept. There are three common
approaches to this problem (that also can be mixed): Table per concrete class, table per class
hierarchy and table per subclass. More information about these approaches and their advantages and disadvantages can be
found in \cite{ORIM}.%\cite[pages 56-57]{HermannDipl}
The method used in this approach is table per subclass, which results in normalized database tables.



\subsection{Mapping of Constraints}

A special feature of the CIM meta schema are qualifiers, which provide
meta information such as constraints on other elements. Mapping such constraints is a challenge in
context of ORM for the CIM. Such constraints (e. g. constraining maximum values of integers or
lengths of strings) can be present in a database schema or be ensured
by setter methods in an object-oriented programming language. A specific
treatment of every constraining qualifier is needed when considering
them in an ORM. Another challenge is the identity of objects. The
CIM makes use of key qualifiers, similar to the concept of keys in
relational databases. But these keys are typically composed of different properties, a direct
mapping to relation keys would make references to tuples of such relations
more complex. Also, depending on the mapping of inheritance, problems can arise since identity is typically only defined by key qualifiers
for concrete classes in the CIM. Relations representing the contribution
of abstract classes need to relate their values to the corresponding
instances, too. Hence it seems appropriate to use surrogate keys for
CIM instances, but additionally add constraints for the key qualifiers
in the database that ensure the identity of instances.


\subsection{Mapping of Behavior}

The CIM meta schema allows to define method signatures for classes,
but the methods themselves are not defined formally. CIM schema classes
come with method descriptions in natural language instead. This prevents
automated tools from covering the behavioral part of a schema.




\section{Implementations}



Implementations provide a mapping as described above and enable working with a mapping using programming language
objects or queries. Figure~\ref{fig:toolchain_orm} provides an overview
of the developed tools. The tools are built around the standards MOF
and SQL. This enables interoperability and also eases possible substitutions
or changes.

\begin{figure}[h]
\begin{centering}
\includegraphics[scale=0.3]{toolchain_orm}
\par\end{centering}

\caption{The corresponding parts of the two implementations.}
\label{fig:toolchain_orm}
\end{figure}



\subsection{Chainreaction: A Java Based Approach}

Chainreaction is a set of tools written in Java covering both:
\begin{itemize}
\item Working with schemata: A schema management tool allows to perform schema related tasks. 
\item Working with instances: An instance related subsystem is generated automatically and specifically for a certain schema or schema repository. 
It provides object oriented access on instances through a Java interface.
\end{itemize}
The ORM functionality makes use of Hibernate\cite{Hibernate}, an ORM library
for Java.


\subsubsection{Schema Subsystem }

The schema management tool is a highly modular and configurable tool.
Its tasks can be configured freely using the Extensible Markup Language
(XML) \cite{XML}. The following functionality is covered by pluggable
modules: 
\begin{itemize}
\item Parsing MOF files and representing a schema internally
\item Generating an adjustable Java implementation
\item Generating adjustable mapping definitions for Hibernate
\item Generating SQL DDL for a database schema, corresponding to a specific
schema and mapping strategy 
\item Defining a way to access a database and writing a database schema
into a database 
\item Using schema repositories to store all relevant data: configuration data as well as all generated data
\item Creating an instance management tool specific for a certain repository 
\end{itemize}
Information specific to the configuration of an implementation, of
a mapping and of a database are separated and grouped for conceptual
clarity and maximum flexibility. Hence the same implementation can
be used with different mappings or databases, or the same database
or mapping can be used with different implementations. 


\subsubsection{Instance Subsystem}

Schema repositories hold the implementations, mappings and database access information for working on instance data comfortably.


\paragraph{Different ways exist to work with instances}
\begin{itemize}
\item Use the instance manager of the instance subsystem. It is created programmatically by the schema management tool and wraps and manages hibernate sessions. 
\item Use the generated code and mapping definition with a suitable ORM library
(Hibernate). Instead of using a generated instance management tool
the corresponding java archives can be included to the application
project. 
\item Other database access, e. g. using SQL directly or another ORM for
another programming language. 
\end{itemize}

\paragraph{Queries}

As an effect of using Hibernate as ORM tool, the powerful Hibernate
Query Language (HQL) \cite{HQL} is there to perform queries on the Database.
HQL is inspired by SQL but also makes use of object features, e. g. 
inheritance, polymorphism and associations. With generating  implementation and mapping automatically
this becomes available for querying CIM instances stored in a relational database.



\subsection{CIM Instance Creation for Python}

The work done to provide CIM instance creation in Python can be divided
in two:
\begin{itemize}
\item Development of a MOF compiler back-end that produces ORM-enabled class
definitions for Python from MOF files.
\item Development of Cimpy, a package that contains a selection of the CIM
model compiled to Python code and a ObjectFactory with helper methods
to ease the creation of CIM objects.
\end{itemize}

\subsubsection{MOF Compiler Back-End}



Python code generation has been implemented as a back-end for the SBLIM\cite{sblim}
MOF compiler. When invoked with this back-end, the MOF compiler produces
ORM-enabled Python class definitions from the supplied MOF files.
The declarative module\cite{declarative} of sqlalchemy\cite{sqlalchemy}
is used to provide the ORM functionality.

When defining mappings in a declarative way, one only have to provide
class definitions, decorated with hints for the declarative module
to deduce the rest of the needed mappings; table mapping and the mapping
between table and class.

The compiler back-end can also add support for remote object manipulation
with the help of the PerspectiveBroker\cite{pb}
system of twisted.
Using twisted and PerspectiveBroker makes it a lot easier to implement
asynchronous network applications.


\subsubsection{Cimpy}

The MOF compiler back-end has been used to compile a selection of MOF
files into Python code. This compilation is called Cimpy and is primarily
targeted at inventory applications.

Cimpy also includes an object factory with helper methods for making
object creation easier. For instance by providing default values for
instance variables.

The MOF compiler back-end is currently at a stage where most of the
code for Cimpy can be built automatically. However, generation of polymorphic
association classes is not yet implemented and code to support the
associations between objects has therefore been developed by hand.
This code is also included in Cimpy package.



\section{Conclusion}

The developed concepts and frameworks described in this paper allow
the storage of CIM models and instances in relational databases using
ORM. Classes defined according to the CIM meta schema can be mapped
to Java and Python classes to use them directly in applications. This
enables the usage of CIM in applications where transactional and object-oriented access to a CIM model is required.
The next step of development will be to increase interoperability of the implementations through unification of the mapping and the configuration of the mapping.
%Following steps in the development. unification
%The next task will be increased interoperability of the implementations through a harmonization mapping and configuration of the mapping.
%Due to the use of SQL as common way to communicate with the RDSB, a more harmonized mapping will increase interoperability of the implementations.

%\bibliographystyle{plain}
%\bibliography{references}
\bibliographystyle{IEEEtran}
\bibliography{svm11-orm}


\end{document}

