#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
List of all comments
\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Itemize
CIM query language and wql are closer to traditional sql, while ORM's enable
 a more object oriented type of access.
\end_layout

\begin_layout Itemize
There is no need to have a repository available where one can get the classes
 and instances.
 They can be created directly from the module.
\end_layout

\begin_layout Itemize
Inventory database is like a read-only management system?
\end_layout

\begin_layout Itemize
Using a RDBS as a backend also allows the use of SQL dircetly against the
 database.
\end_layout

\begin_layout Itemize
OpenWBEM: Providers run in a transactional environment, making it much easier
 to write a robust provider free from deadlocks and race conditions.
 -> ORM provides this for free on the CIM repository side?
\end_layout

\begin_layout Itemize
Queries can be done directly on a database.
 For inventory information, most parts of it won't change often.
 So the normally distributed
\end_layout

\begin_layout Itemize
adcim can optionally use xml-to-relational mapping to store to relational
 databases.
 Normally uses a ldapbackend
\end_layout

\begin_deeper
\begin_layout Itemize
http://adcim.des.udc.es/distributedstoragelayer.html
\end_layout

\end_deeper
\end_inset


\end_layout

\begin_layout Section
[LATER] Motivation for ORM+CIM.
\end_layout

\begin_layout Itemize
What is the motivation for applying ORM on CIM in general?
\end_layout

\begin_deeper
\begin_layout Itemize
Is covered to some extent, but could maybe be clarified.
 Does he mean CIMOM vs.
 use objects directly?
\end_layout

\end_deeper
\begin_layout Section
[LATER] Data integrity
\end_layout

\begin_layout Itemize
[P5,R4]: The paper presents preliminary results, and it is hard to me to
 know how you solve the data integrity problem.
 This is, how can you access the data generated in the Java program with
 the Python program.
 If the ORM is not exactly the same, you are going to have a good problem
 to solve.
\end_layout

\begin_deeper
\begin_layout Itemize
We configure the ORMs to behave the same.
 This is the way that data integrity is imposed.
\end_layout

\begin_layout Itemize
Could try to clarify the current status and describe any ideas and plans
 we have for solving the problem in a more prominent.
\end_layout

\end_deeper
\begin_layout Section
[ØYSTEIN] Clarify what the contribution is/comparison to other works
\end_layout

\begin_layout Standard
(For place to start see for instance 3.3).
\end_layout

\begin_layout Standard
The most often mentioned comment has to do with the contribution of the
 work presented in the paper.
 It is not clear what the actual contribution is:
\end_layout

\begin_layout Itemize
What about related work? Especially in the area of the well-known ORM and
 CIM, there is a lot of related work that could be very helpful to solve
 your challenges.
 You should evaluate related work in your area of interest and also point
 out the deficits in oder to show the relevance and necessity of your work.
 As of now the later remains not visible.
\end_layout

\begin_layout Itemize
[P9,R2]: Object relational mapping (ORM) is an old concept and the contribution
 of the authors in this paper is not clear.
 What's new in this paper compared to previous works?
\end_layout

\begin_layout Itemize
However, the implementation section should be described in more detail,
 and comparisons with other works are expected.
\end_layout

\begin_layout Standard
[Move to goals section and integrate it there]: Obviously there is a lot
 of related work available for CIM and ORM individually, but there doesn't
 seem to be a lot of work on the combination of CIM and ORM.
 
\end_layout

\begin_layout Standard
Try to point this out more clearly:
\end_layout

\begin_layout Itemize
It is the configuration of existing (common) object relational mappers (hibernat
e, sqlalchemy) to conform to a defined way of configuring such mappers so
 as to enable multiple languages access through their respective mappers
 via the generated CIM classes.
\end_layout

\begin_layout Itemize
These classes can be directly used (instantiated) in applications without
 the need for a CIM repository, although a CIM repository normally would
 be helpful in the management of the CIM classes and instances.
\end_layout

\begin_layout Itemize
This enables powerful query possibilities and more idiomatic (explicit)
 usage from the respective languages.
\end_layout

\begin_layout Itemize
As well as the possibility to make use of traditional relational database
 systems, with the advantages that follows.
\end_layout

\begin_layout Section
[ØYSTEIN] Conceptual part of information gathering is lacking?
\end_layout

\begin_layout Itemize
[ALMOST DONE]: The design principles (as for example in section IV.B) are
 not described and one sentence is way too few.
\end_layout

\begin_deeper
\begin_layout Itemize
The conceptual part of the information gathering is lacking.
 Could be extended and more details provided.
\end_layout

\end_deeper
\begin_layout Section
Paper 9
\end_layout

\begin_layout Subsection
Review 1
\end_layout

\begin_layout Itemize
[JOCHEN]: By the way, it would be good if some real mapping cases are illustrate
d with a practical system.
 [3]
\end_layout

\begin_deeper
\begin_layout Itemize
The paper lacks a illustration of a mapping, an example...
 Might be able to use image of mapping between CIM instances 
\end_layout

\end_deeper
\begin_layout Section
Paper 17
\end_layout

\begin_layout Standard
Major weaknesses:
\end_layout

\begin_layout Itemize
The contribution of your paper is not clear to me.
 It seems as if the paper would rather describe a project's implementation
 and not any research results.
 In case this paper should be accepted, please do focus more on the research
 questions and the systematical approach used to solve those.
\end_layout

\begin_deeper
\begin_layout Itemize
[LATER]: Look for places where improvements can be done easily in the text.
\end_layout

\begin_layout Itemize
[JOCHEN]: Look in particular for research questions and systematical approach.
\end_layout

\end_deeper
\begin_layout Itemize
[ALMOST DONE]: Table I comprise a very generic metric ("good", "very good"
 and "OK").
 The way of evaluating the access types and how this leads to your conclusion
 is not stated and thus the result can not be reproduced.
\end_layout

\begin_deeper
\begin_layout Itemize
Be more concrete.
 Maybe the second part is becomes obsolete.
\end_layout

\end_deeper
\begin_layout Section
Done
\end_layout

\begin_layout Itemize
[DONE]: As you mentioned, mapping of behavioural part of schema is not easy
 and no other solution proposed for that.
 [4]
\end_layout

\begin_deeper
\begin_layout Itemize
Behavioral part not really needed by us.
 Can we be more clear about the advantages and motivation of CIM+ORM?
\end_layout

\end_deeper
\begin_layout Itemize
Outside access to project/strategy moving project forward.
\end_layout

\begin_deeper
\begin_layout Itemize
[DONE]: Addressed in Conclusions & Outlook.
\end_layout

\begin_layout Itemize
[ØYSTEIN]: These work will be more interesting if the authors could make
 it open source.
\end_layout

\begin_layout Itemize
[ØYSTEIN]: A statement regarding whether the implementation is available
 to others would be helpful.
\end_layout

\end_deeper
\begin_layout Subsection
Paper 9
\end_layout

\begin_layout Itemize
[DONE][JOCHEN][P9/R2]: Implementation part seems to be just a configuration
 of other tools (e.g.
 Hibernate, Sqlalchemy) and the author's contribution is not clear.
\end_layout

\begin_deeper
\begin_layout Itemize
Yes, we do configuration, but we automate the process.
\end_layout

\end_deeper
\begin_layout Itemize
[DONE]: However, the implementation section should be described in more
 detail
\end_layout

\begin_layout Subsection
Paper 5
\end_layout

\begin_layout Itemize
[P5,R4: With respect to your generic XML-RPC interface, you should have
 a look to the dynamic invocation of web services.
\end_layout

\begin_deeper
\begin_layout Itemize
Addressed in the data access section:
\end_layout

\begin_deeper
\begin_layout Itemize
want to stick to the simpler xml-rpc and json solution for now.
 The addition of REST support in wsdl 2.0 makes it more interesting for the
 future.
\end_layout

\begin_layout Itemize
Also service discovery, if implementaed, has some overlap which makes the
 benefit from dynamic invocation of web service less clear.
\end_layout

\begin_layout Itemize
It seems more like web technology (not necessarily needed), rather than
 local area network technology (needed).
\end_layout

\end_deeper
\end_deeper
\begin_layout Itemize
[DONE]: or challenges while applying the CI model could be discussed.
\end_layout

\begin_layout Itemize
[DONE]: When preparing the camera ready document, a spell check should be
 performed (e.g., implementet should be implemented).
\end_layout

\begin_layout Itemize
[DONE]: The description comprises an overview of the software components
 used in a project, but no conceptual contribution.
\end_layout

\begin_layout Itemize
[DONE]: Instead of presenting two separate papers, I would ask to merge
 both submissions.
\end_layout

\begin_layout Itemize
[DONE]: In figure 1, if you have a B&W printed copy, it is not possible
 to distinguish between orange and blue.
 You can use dashed and dotted lines instead.
\end_layout

\begin_layout Itemize
[DONE]: Use "such as" instead of "like" when enumerating examples.
\end_layout

\begin_layout Subsubsection
Review 2
\end_layout

\begin_layout Itemize
[DONE]: This paper describes a data integration project, superficially.
 While the work described seems to use a CIMOM, this is mentioned as part
 of the tool integration, and it is not in focus of the paper.
\end_layout

\begin_deeper
\begin_layout Itemize
Try to clarify.
 Say for instance this is similar to CIMOM with the exception so an so..
\end_layout

\end_deeper
\begin_layout Subsection
Paper 17
\end_layout

\begin_layout Itemize
[DONE][JOCHEN]: You do have a requirements section, but the derivation of
 the requirements remains completely unclear.
 How did you derive your requirements?
\end_layout

\begin_layout Itemize
[DONE]: Why is this list not too scenario-specific, but rather generally
 applicable? Put a lot more energies in this section, please.
\end_layout

\begin_layout Itemize
[DONE]: In section III.C.3) you only present issues you are having, but no
 solution.
 Also, you do not point to a reference of any further work where the point
 you raised is addressed.
\end_layout

\begin_deeper
\begin_layout Itemize
Not implemented as it is not needed.
 But this should follow from the requirements.
\end_layout

\end_deeper
\begin_layout Itemize
[DONE]: Figure 1 and 2 are of bad quality.
 Please use a vector graphics format and no pixel graphics!
\end_layout

\begin_layout Itemize
[DONE]: Be careful with using colors in your figures as there is a great
 chance of only having black and white copies of your paper.
\end_layout

\begin_layout Itemize
[DONE]: Your figures are not explained.
 It is not up to me to interpret what you wanted to say with some seemingly
 complex graphics.
 Please do explain and point out the value of your figures in a detailed
 fashion.
\end_layout

\begin_deeper
\begin_layout Itemize
Coming improvements to the text hopefully makes it more in line with the
 figure and we should remember to refer to the figures where we can.
\end_layout

\end_deeper
\begin_layout Itemize
[DONE]: In section III.C.1).c) you chose an approach from [4], but you did
 not qualify your choice.
 Why did you chose exactly the method "table per subclass" and no other?
 What are the advantages/disadvantages?
\end_layout

\begin_deeper
\begin_layout Itemize
Look at Marians thesis if there is anything that could be used from there.
\end_layout

\end_deeper
\begin_layout Section
Ignore
\end_layout

\begin_layout Itemize
[IGNORE]: To further improve the paper, additional insight could be provided,
 e.g., some difficulties faced during the implementation
\end_layout

\begin_layout Itemize
Keeping up-to date, conceptual part is short on details...
\end_layout

\begin_layout Itemize
[IGNORE]: The problem addressed (accessing up-to-date information) is an
 interesting one; alas, the paper focuses on the implementation details
 of your tool and refers the problem to the discovery software chosen.
\end_layout

\begin_deeper
\begin_layout Itemize
CIM+ORM is the conceptual challenge.
 Keeping up-date is the implemetation challenge? Or also lacking in details
 for the conceptual part of the information gathering.
\end_layout

\begin_layout Itemize
Might not be clear that we wrote the discovery solutions?
\end_layout

\end_deeper
\begin_layout Subsection
Paper 17
\end_layout

\begin_layout Itemize
[IGNORE]: How do you guarantee that the list of requirements is complete?
\end_layout

\begin_layout Subsection
Paper 9
\end_layout

\begin_layout Itemize
[IGNORE]: Natural idea.
 Not novel.
\end_layout

\begin_deeper
\begin_layout Itemize
The novelty is rather from the CIM+ORM combination
\end_layout

\end_deeper
\begin_layout Enumerate
[IGNORE?]: This paper is off topic for this workshop as it does not fit
 in any of the topics of interest:
\end_layout

\begin_deeper
\begin_layout Enumerate
Virtualization
\end_layout

\begin_layout Enumerate
Key management issues
\end_layout

\begin_layout Enumerate
Web services and SOA
\end_layout

\begin_layout Enumerate
New management paradigms
\end_layout

\begin_layout Enumerate
Experience implementing and deploying management technology
\end_layout

\begin_deeper
\begin_layout Enumerate
We think we do at least a little bit of this.
\end_layout

\end_deeper
\end_deeper
\begin_layout Enumerate
When combined with paper 5 into paper 17 this might not be the case anymore...
\end_layout

\begin_layout Subsection
Paper 5
\end_layout

\begin_layout Itemize
[IGNORE][P5,R1]: The remaining space could also be used to describe the
 real- world-experiences made so far.
\end_layout

\begin_deeper
\begin_layout Itemize
See if there is anything of tests that have been done that can be used here.
\end_layout

\begin_layout Itemize
Not enough time, not really any results yet.
\end_layout

\end_deeper
\begin_layout Itemize
[IGNORE]: The paper also fails to contribute to the domains of virtualization
 or systems management.
 Please refer to the Call for Papers.
\end_layout

\begin_layout Itemize
[IGNORE/P5,R3]: There should give some assessment on the two models, and
 have their advantages and disadvantages compared.
\end_layout

\begin_deeper
\begin_layout Itemize
This is work in progress for now and will be done sometime in the future.
\end_layout

\end_deeper
\end_body
\end_document
