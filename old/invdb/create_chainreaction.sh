#!/bin/bash

# This is the command used to compile the MOFs using Chainreaction
# from within the Chainreaction directory assuming the instruction
# XML file is located at "processes/SysMES-CIM/1repository.xml"

ant run -Dfile="processes/SysMES-CIM/1repository.xml"