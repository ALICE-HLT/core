#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

counter = 0
subDir = "instance-mofs"

#valid values:
values = dict()
values["boolean"] = ["null", "true", "false"]
values["char16"] = ["null", "'a'", "'7'", "'ö'", "'\"'"]
values["datetime"] = ["null", "\"20090222043632.361000+060\"", "\"19980525133015.123456-300\"", "\"19980525133015.123456+123\""]
values["real32"] = ["null" , "1.618", ".0", "-.1", "+.5e3", "1.2345e-12", "-0.00E+00", "1.40129846432481707e-45", "-3.40282346638528860e+38"]
values["real64"] = ["null", "3.142", "-4.94065645841246544e-324", "-1.79769313486231570e+308", "+4.94065645841246544e-324", "+1.79769313486231570e+308"]
values["sint16"] = ["null", "-1", "0", "11", "10b", "-11B", "+020", "-021", "0XF", "-0x1a"]
values["sint32"] = ["null", "-1", "0", "11", "10b", "-11B", "+020", "-021", "0XF", "-0x1a"]
values["sint64"] = ["null", "-1", "0", "11", "10b", "-11B", "+020", "-021", "0XF", "-0x1a"]
values["sint8"] = ["null", "-1", "0", "11", "10b", "-11B", "+020", "-021", "0XF", "-0x1a"]
values["string"] = ["null",  "\"test\"", "\"a\"", "\"\"", "\"null\"", "\"test1\" \"test2\"", "\"test1\" \"test2\" \"test3\"", "\"öäß\"", "\"\\\"quoted\\\"\"", "\"" + "".ljust(512 +1,"w") + "\""]
values["uint16"] = ["null", "1", "0", "11", "10b", "+11B", "+020", "0XF", "+0x1a", "65535"]
values["uint32"] = ["null", "1", "0", "11", "10b", "+11B", "+020", "0XF", "+0x1a", "4294967296"]
values["uint64"] = ["null", "1", "0", "11", "10b", "+11B", "+020", "0XF", "+0x1a", "18446744073709551615"]
values["uint8"] = ["null", "1", "0", "11", "10b", "+11B", "+020", "0XF", "+0x1a", "255"]

#instance prefixes:
suffixes = {"TEST_Values" : "Vals", "TEST_Arrays" : "Arrays", "TEST_Element" : "Element", "TEST_Subclass" : "Subclass", "TEST_SubSubclass" : "SubSubclass"}

class instance:
	def __init__(self, className):
		self.className = className
		self.properties = dict()
		self.hasAlias = False
		self.aliasName = ""

	def toMof(self):
		alias = ""
		if (self.hasAlias):
			alias = " as $" + self.aliasName
		mof = ""
		mof = mof + "instance of " + self.className + alias + " {\n\n"
		for p in self.properties:
			mof = mof + "\t" + p + " = " + self.properties[p] + ";\n\n"
		mof = mof + "};\n\n"
		return mof
		
	def setAlias(self, n):
		self.hasAlias = True
		self.aliasName = "alias" + str(n)

def getFile(name):
	return open("." + os.sep + subDir + os.sep + name, 'w')

def writeSingleInstances(instances):
	for c in instances:
		suffix = suffixes[c.className]
		name = c.properties["ElementName"].replace("\"","") + suffix + ".mof"
		f = getFile(name)
		f.write(c.toMof())
		f.close
		
def referenceInitializer(ref, keys, useAlias):
	if (useAlias):
		return "$" + ref.aliasName
	else:
		objectPath = ref.className + "."
		for key in keys:
			objectPath = objectPath + key + "=" + ref.properties[key].replace("\\","\\\\").replace("\"","\\\"") + ","
		objectPath = objectPath.rstrip(",")
		return "\"" + objectPath +"\""

def valueInstances():
	global counter
	instances = []
	# value-tests
	props = [("boolean","BooleanVal"),("char16","Char16Val"),("datetime","DatetimeVal"),("real32","Real32Val"),("real64","Real64Val"),("sint16","SInt16Val"),("sint32","SInt32Val"),("sint64","SInt64Val"),("sint8","SInt8Val"),("string","StringVal"),("uint16","UInt16Val"),("uint32","UInt32Val"),("uint64","UInt64Val"),("uint8","UInt8Val")]
	counter = counter + 1
	mvi = instance("TEST_Values")
	mvi.properties["ElementName"] = "\"instance" + str(counter) + "MultiValue" + "\""
	instances.append(mvi)
	counter = counter + 1
	mai = instance("TEST_Arrays")
	mai.properties["ElementName"] = "\"instance" + str(counter) + "MultiArray" + "\""
	instances.append(mai)
	for p in props:
		# single value instances
		mvi.properties[p[1]] = values[p[0]][1]
		for v in values[p[0]]:
			counter = counter + 1
			c = instance("TEST_Values")
			c.properties[p[1]] = v
			c.properties["ElementName"] = "\"instance" + str(counter) + p[0] + "\""
			instances.append(c)
		#array value instances
		arrayValues = ["null", "{" + values[p[0]][2] + "}", "{" + values[p[0]][0] +  ", " + values[p[0]][1] + "}"]
		mai.properties[p[1]] = arrayValues[1]
		for v in arrayValues:
			counter = counter + 1
			c = instance("TEST_Arrays")
			c.properties[p[1] + "s"] = v
			c.properties["ElementName"] = "\"instance" + str(counter) + p[0] + "\""
			instances.append(c)
	
	writeSingleInstances(instances)

def inheritanceInstances():
	global counter
	instances = []	
	# inheritance tests:
	counter = counter + 1
	c = instance("TEST_Element")
	c.properties["ElementName"] = "\"instance" + str(counter) + "inheritance0" + "\""
	instances.append(c)
	counter = counter + 1
	c = instance("TEST_Subclass")
	c.properties["ElementName"] = "\"instance" + str(counter) + "inheritance1" + "\""
	c.properties["additionalProperty"] = "\"testvalue of additional property" + "\""
	instances.append(c)
	counter = counter + 1
	c = instance("TEST_SubSubclass")
	c.properties["ElementName"] = "\"instance" + str(counter) + "inheritance2" + "\""
	c.properties["additionalProperty"] = "\"testvalue of additional property" + "\""
	c.properties["thirdProperty"] = "\"testvalue of third property" + "\""
	instances.append(c)
	
	writeSingleInstances(instances)
	
def associationInstances():
	global counter
	aiCount = 0
	for useAlias in [True, False]:
		aliasInfix = ""
		if (useAlias):
			aliasInfix = "Alias"
		for createAssociationToAssociation in [True, False]:
			# simple binary association:
			counter = counter + 1
			d = instance("TEST_Element")
			d.properties["ElementName"] =  "\"instance" + str(counter) + aliasInfix + "AssociationDomain" + "\""
			if (useAlias):
				d.setAlias(counter)
			counter = counter + 1
			r = instance("TEST_Subclass")
			r.properties["ElementName"] =  "\"instance" + str(counter) + aliasInfix + "AssociationRange" + "\""
			if (useAlias):
				r.setAlias(counter)
			counter = counter + 1
			a = instance("TEST_NamedAssociation")
			a.properties["Name"] =  "\"association Name for instance" + str(counter) + aliasInfix + "AssociationAssociaton" + "\""
			a.properties["Domain"] = referenceInitializer(d, ["ElementName"], useAlias)
			a.properties["Range"] = referenceInitializer(r, ["ElementName"], useAlias)
			if (useAlias):
				a.setAlias(counter)
			
			aiCount = aiCount + 1
			name = "associatedInstances" + str(aiCount) + aliasInfix + "Binary"
			if (createAssociationToAssociation):
				name = name + "ANA"
			name = name + ".mof"
			f = getFile(name)
			f.write(d.toMof())
			f.write(r.toMof())					
			f.write(a.toMof())
			if (createAssociationToAssociation):
				counter = counter + 1
				ana = instance("TEST_AssociationNamedAssociation")		
				ana.properties["Identifier"] = "\"instance" + str(counter) + aliasInfix + "AssociationANA" + "\""
				ana.properties["AssociationInstance"] = referenceInitializer(a, ["Domain", "Range"], useAlias)
				if (useAlias):
					ana.setAlias(counter)
				f.write(ana.toMof())
			f.close			
			
		# ternary association:
		counter = counter + 1
		s = instance("TEST_Element")
		s.properties["ElementName"] =  "\"instance" + str(counter) + aliasInfix + "TernarySubject" + "\""
		if (useAlias):
			s.setAlias(counter)
		counter = counter + 1
		p = instance("TEST_Element")
		p.properties["ElementName"] =  "\"instance" + str(counter) + aliasInfix + "TernaryPredicate" + "\""
		if (useAlias):
			p.setAlias(counter)
		counter = counter + 1
		o = instance("TEST_Element")
		o.properties["ElementName"] =  "\"instance" + str(counter) + aliasInfix + "TernaryObject" + "\""
		if (useAlias):
			o.setAlias(counter)
		counter = counter + 1
		t = instance("TEST_TernaryAssociation")
		t.properties["Subject"] = referenceInitializer(s, ["ElementName"], useAlias)
		t.properties["Predicate"] = referenceInitializer(p, ["ElementName"], useAlias)
		t.properties["Object"] = referenceInitializer(o, ["ElementName"], useAlias)
		if (useAlias):
			t.setAlias(counter)
		aiCount = aiCount + 1
		name = "associatedInstances" + str(aiCount) + aliasInfix + "Ternary.mof"
		f = getFile(name)
		f.write(s.toMof())
		f.write(p.toMof())					
		f.write(o.toMof())
		f.write(t.toMof())
		f.close()
		
		# association to association (chain):
		for rvd in [True, False]:
			l = []
			previous = "null"
			for i in range(5):
				counter = counter + 1
				a = instance("TEST_AssociationAssociation")
				a.properties["Identifier"] = "\"instance" + str(counter) + aliasInfix + "AssociationChainElement" + str(i) + "\""
				a.properties["AssociationInstance"] = previous
				if (useAlias):
					a.setAlias(counter)
				previous = referenceInitializer(a, ["Identifier"], useAlias)
				l.append(a)
			aiCount = aiCount + 1
			name = "associatedInstances" + str(aiCount) + aliasInfix + "UnaryChain"
			if (rvd):
				l.reverse()
				name = name + "Reversed"
			name = name + ".mof"
			f = getFile(name)
			for i in l:
				f.write(i.toMof())
			f.close()


def main():
	if not os.path.isdir("." + os.sep + subDir + os.sep):
		os.mkdir("." + os.sep + subDir + os.sep)
	valueInstances()
	inheritanceInstances()
	associationInstances()

		
main()
