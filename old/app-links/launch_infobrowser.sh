#!/usr/bin/env bash

# Take out the protocol part of the url (telnet://hostname)
host=`echo $1 | sed "s;ibrowser://;;"`

# Start telnet in a xterm
ssh -X $host /opt/HLT/tools/bin/infoBrowser.sh
