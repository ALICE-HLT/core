#!/usr/bin/env bash

# Stop firefox and add path to telnet launcher:

# 1. Find pref,js
# find ~/.mozilla -name prefs.js -print

# 2. Insert line below into file
# "user_pref("network.protocol-handler.app.telnet", "/afsuser/oystein/tmp/launch_telnet.sh");"

# Take out the protocol part of the url (telnet://hostname)
host=`echo $1 | sed "s;telnet://;;"`

# xmessage "got $host from browser"     # for debugging

# Start telnet in a xterm
xterm -e telnet $host
