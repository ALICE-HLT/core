if [ $# -ne 1 ] ; then
  echo "Usage: $0 <TYPE>"
  exit 1
fi

BASEDIR=/home/timm/src/HLT/devel
TYPE=$1

export LANG=C
ID=`ls -1d HLT-${TYPE}-????????.??????| tail -n1|awk -F/ '{print $1}'`

echo Cleaning up ${ID}
rm ${ID}.contents.template.for.diff ${ID}.contents.diff
rm ${ID}-build-debug.out ${ID}-build-release.out ${ID}-build-releasedebug.out
rm -rf ${ID}  ${ID}-test
rm -rf ${ID}  ${ID}-cmake-test
