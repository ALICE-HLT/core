#!/bin/bash

if [ $# -lt 3 ] ; then
    echo Usage: $0 '<base install directory>' '<pythonpath>' '<output-directory>'
    exit 1
fi

cat >$3/setenv.sh <<EOF
#!/bin/bash
export PATH=${PATH}:$1/bin
export PYTHONPATH=${PYTHONPATH}:$2
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$1/lib
EOF

