#!/bin/bash

pushd .. >/dev/null
BASEDIR=`pwd`
popd >/dev/null

USAGE="Usage: $0 (-production|-productiondebug)"

VERSION=debug
if [ $# -gt 0 ] ; then
  if [ "$1" = "-production" ] ; then
    VERSION=release
  elif [ "$1" = "-productiondebug" ] ; then
    VERSION=releasedebug
  else
    echo ${USAGE} >/dev/stderr
    exit -1
  fi
  if [ $# -gt 1 ] ; then
    echo ${USAGE} >/dev/stderr
    exit -1
  fi
fi

TARGET=`uname -s`-`uname -m`-${VERSION}

mkdir -p ../lib/$TARGET
mkdir -p ../bin/$TARGET

pushd ../lib/$TARGET >/dev/null

for lib in `find ../../src \( -type f -or -type l \) -iregex ".*/lib/${TARGET}/.*\.a"` ; do
  echo $lib
  ln -s $lib
done

for lib in `find ../../src \( -type f -or -type l \) -iregex ".*/lib/${TARGET}/.*\.so"` ; do
  echo $lib
  ln -s $lib
done

for lib in `find ../../src \( -type f -or -type l \) -iregex ".*/lib/${TARGET}/.*\.so\..*"` ; do
  echo $lib
  ln -s $lib
done

popd >/dev/null


pushd ../bin/$TARGET >/dev/null

for bin in `find ../../src \( -type f -or -type l \) -iregex ".*/bin/${TARGET}/.*"` ; do
  echo $bin
  ln -s $bin
done

ln -s ../../lib/$TARGET/TMControlInterface.so
#ln -s ../../src/SimpleChainConfig1/MakeTaskManagerConfig.py
#ln -s ../../src/SimpleChainConfig1/SimpleChainConfig1.py
ln -s ../../src/TaskManager/GUI/TMGUI*.py .

for bin in `find ../../src/SimpleChainConfig? ../../src/Util/ComponentStatusScan ../../src/Util/SimpleComponentWrapper ../../src/Util/TM-State-Check \( -type f -or -type l \) -name "*.py"` ; do
  echo $bin
  ln -s $bin
  chmod a+x $bin
done

chmod a+x *

popd >/dev/null

for include in `find . -type d -name include` ; do
    mkdir -p ../include/`dirname ${include}`
    cp ${include}/* ../include/`dirname ${include}`
done

#pushd SimpleChainConfig2 >/dev/null
#ln -s ../SimpleChainConfig1/templates
#ln -s ../SimpleChainConfig1/scripts
#popd >/dev/null

pushd Util/ComponentStatusScan >/dev/null
mkdir -p module-links/${TARGET}
cd module-links/${TARGET}
ln -s ../../../../TaskManager/lib/${TARGET}/TMControlInterface.so
popd >/dev/null



echo "#!/bin/bash" >$BASEDIR/bin/setenv-${VERSION}.sh
echo "BASEDIR=$BASEDIR" >>$BASEDIR/bin/setenv-${VERSION}.sh
echo "export PATH=\$PATH:\$BASEDIR/bin/\`uname -s\`-\`uname -m\`-${VERSION}" >>$BASEDIR/bin/setenv-${VERSION}.sh
echo "export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:\$BASEDIR/lib/\`uname -s\`-\`uname -m\`-${VERSION}" >>$BASEDIR/bin/setenv-${VERSION}.sh

echo "#!/bin/tcsh" >$BASEDIR/bin/setenv-${VERSION}.csh
echo "set BASEDIR=$BASEDIR" >>$BASEDIR/bin/setenv-${VERSION}.csh
echo "if ( $?PATH == 0 ) setenv PATH \"\"" >>$BASEDIR/bin/setenv-${VERSION}.csh
echo "setenv PATH \${PATH}:\${BASEDIR}/bin/\`uname -s\`-\`uname -m\`-${VERSION}" >>$BASEDIR/bin/setenv-${VERSION}.csh
echo "if ( $?LD_LIBRARY_PATH == 0 ) setenv LD_LIBRARY_PATH \"\"" >>$BASEDIR/bin/setenv-${VERSION}.csh
echo "setenv LD_LIBRARY_PATH \${LD_LIBRARY_PATH}:\${BASEDIR}/lib/\`uname -s\`-\`uname -m\`-${VERSION}" >>$BASEDIR/bin/setenv-${VERSION}.csh