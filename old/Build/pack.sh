#!/bin/bash

BASEDIR=`dirname $0`
usage="\[ -dir \<base-source-directory\> \]"

skip=0
sourcetype=""
source=""
argn=1
while [ $argn -le $# ] ; do
    eval arg=\$$argn
    #echo argn: $argn - arg: $arg
    if [ $arg = "-basedir" ] ; then
        sourcetype="dir"
        let param=argn+1
        eval source=\$$param
        let argn=argn+2
##    elif [ $arg = "-cvs" ] ; then
##        sourcetype="cvs"
##        let param=argn+1
##        eval source=\$$param
##        let argn=argn+2
##    elif [ $arg = "-web" ] ; then
##        sourcetype="web"
##        let param=argn+1
##        eval source=\$$param
##        let argn=argn+2
    else
        echo "Unknown option '$arg'"
        exit -1
    fi
done

DATE=`date "+%Y%m%d-%H%M%S"`

DIR=HLT-$DATE

mkdir $DIR $DIR/src
cp $BASEDIR/build-readme $DIR/README
cp $BASEDIR/build.sh $DIR/src
cp $BASEDIR/Configure.py $DIR/src
cp $BASEDIR/Replace.py $DIR/src

DESTDIRS=""
case "$sourcetype" in
    dir)
        KIPDIR=kip
        COMPDIR=Components
        DIRS="$KIPDIR/MLUC $KIPDIR/BCL $KIPDIR/TaskManager $KIPDIR/TMGUI AliHLT pci/psi $COMPDIR/ShmDumpSubscriber $COMPDIR/TCPDumpSubscriber $COMPDIR/AliRootWrapperSubscriber Util/HOMER/reader SimpleChainConfig1 python/ERSGUI python/XMLRead OnlineDisplay/TPC-OnlineDisplayHOMER Util/ROOTInterface Util/SimpleComponentWrapper"
        for d in $DIRS ; do
            tmpd=`dirname \`dirname $d\``
            if [ -z "$tmpd" ] ; then
                destdir=$DIR/src/`basename $d`
            else
                destdir=$DIR/src/`basename \`dirname $d\``/`basename $d`
            fi
            echo Copying $source/$d to $destdir
            mkdir -p `dirname $destdir`
            cp -a $source/$d $destdir
            DESTDIRS="$DESTDIRS $destdir"
        done
    ;;
    *)
        echo Usage: $0 $usage
        echo "Must specify one of the -basedir options"
    ;;
esac




for d in $DESTDIRS ; do
    echo Removing "~" files in $d
    find $d -type f -name "*~" -exec rm -f "{}" \; #-print
    echo Removing binaries in $d
    find $d -type f -name "*.o" -exec rm -f "{}" \; #-print
    find $d -type f -name "*.so" -exec rm -f "{}" \; #-print
    find $d -type f -name "*.a" -exec rm -f "{}" \; #-print
    find $d -type d -depth -name "Linux-i686" -exec rm -rf "{}" \; #-print
    echo Removing version control system files in $d
    find $d -type d -depth -name ".svn" -exec rm -rf "{}" \; #-print
    find $d -type d -depth -name "CVS" -exec rm -rf "{}" \; #-print
    if [ -f $d/NoReleaseFiles ] ; then
        echo Removing NoReleaseFiles in $d
        for nrf in `cat $d/NoReleaseFiles` ; do 
            find $d -depth -name "$nrf" -exec rm -rf "{}" \; #-print
        done
    fi
done


