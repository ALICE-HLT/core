#!/usr/bin/env python

import string, re, os, sys, time, os.path

d=1
source = ""
dir = ""
ext_date_str = None
version = ""
print_usage = 0
svn_type_dir = ""
dist = 0
svn_rev_arg=""
while d < len(sys.argv):
    if sys.argv[d][0:2] == "-r":
        svn_rev_arg=sys.argv[d]
        d=d+1
        continue
    if sys.argv[d] == "-svn":
        if d+2>=len(sys.argv):
            sys.stderr.write( "Expected repository and version argument to -svn parameter.\n" )
            print_usage = 1
            break
        source = "svn"
        repos_basename = sys.argv[d+1]
        if sys.argv[d+2].lower() == "trunk" or sys.argv[d+2].lower() == "head":
            version = "trunk"
            dir_version="trunk"
        elif sys.argv[d+2].lower() == "current":
            version = "current"
            dir_version="current"
        elif sys.argv[d+2].lower() == "stable":
            version = "stable"
            dir_version="stable"
        else:
            version = sys.argv[d+2].lower()
            #dir_version=version.replace( "tags/", "" )
            dir_version=version.replace( "/", "_" )
            #sys.stderr.write( "Unknown version argument '"+sys.argv[d+1]+"' to -svn parameter.\n" )
            #print_usage = 1
            #break
        d=d+3
        continue
    if sys.argv[d] == "-dir":
        if d+1>=len(sys.argv):
            sys.stderr.write( "Expected directory argument to -dir parameter.\n" )
            print_usage = 1
            break
        source = "dir"
        dir = sys.argv[d+1]
        d=d+2
        continue
    if sys.argv[d] == "-date":
        if d+1>=len(sys.argv):
            sys.stderr.write( "Expected date string argument to -date parameter.\n" )
            print_usage = 1
            break
        ext_date_str = sys.argv[d+1]
        d=d+2
        continue
    if sys.argv[d] == "-dist":
        dist = 1
        d=d+1
        continue
    if sys.argv[d] == "-branch":
        svn_type_dir = "branches/"
        d=d+1
        continue
    if sys.argv[d] == "-tag":
        svn_type_dir = "tags/"
        d=d+1
        continue
    if sys.argv[d] == "-nosvnbase":
        svn_type_dir = "empty"
        d=d+1
        continue
    if sys.argv[d] == "-help" or sys.argv[d] == "--help" or sys.argv[d] == "-h" or sys.argv[d] == "-?":
        print_usage = 1
        break
    sys.stderr.write("Unknown argument: \"" + sys.argv[d] + "\".\n")
    print_usage = 1
    break

if not print_usage:
    if source=="":
        sys.stderr.write( "You have to provide a package source.\n" )
        print_usage = 1
    elif source=="svn" and version=="":    
        sys.stderr.write( "You have to provide a version tag to checkout from Subversion.\n" )
        print_usage = 1
        
if print_usage:
    print "Usage: "+sys.argv[0]+"( [-svn <repository-base-path> [ head | current | stable | <common-svn-version-specicier> ] | -dir <source-directory> ] ) ([-help|--help|-h|-?])"
    sys.exit(1)

if source == "svn":
    if svn_type_dir=="":
        if version == "trunk":
            pass
        elif  version == "head":
            version = "trunk"
        elif version == "current" or version == "stable":
            svn_type_dir = "branches/"
        else:
            svn_type_dir = "tags/"
    if svn_type_dir=="empty":
        svn_type_dir=""
    version=svn_type_dir+version   ### was svn_type_version+version
    


#repos_basename = "http://kip1.kip.uni-heidelberg.de/repos/TI"

build_basedir = os.path.dirname( sys.argv[0] )


packages = [
    [ "/", [ [ "MLUC", None, None, None ],
             [ "BCL" , None, None, None ],
             [ "TaskManager", None, None, None ],
             [ "PSI2", None, None, None ],
             [ "Utility_Software/HLTReadoutList", None, None, None ],
             [ "Utility_Software/CommonDataHeader", None, None, None ],
             [ "Utility_Software/NOPEEvent", None, None, None ],
             [ "Utility_Software/AliRootInterfaceLibrary", None, None, None ],
             [ "Framework" , None, None, None ],
             [ "SimpleChainConfig1", None, None, None ], 
             [ "SimpleChainConfig2", None, None, [ [ "SimpleChainConfig1/XMLConfigReader.py", None ], [ "SimpleChainConfig1/SimpleChainConfig1.py", None ], [ "SimpleChainConfig1/scripts", None ], [ "SimpleChainConfig1/templates", None ] ] ], 
             [ "Utility_Software/LogFileDelivery", None, None, None ],
             [ "Utility_Software/HOMER", None, None, None ],
             [ "Utility_Software/ERSGUI", None, None, None ],
             [ "Utility_Software/DDLHeaderCopy", None, None, None ],
             [ "Utility_Software/SimpleComponentWrapper", None, None, None ],
             [ "Utility_Software/TCPHOMERDataProvider", None, None, None ],
             [ "Utility_Software/WHALE", None, None, None ],
             [ "Utility_Software/DATEDataReader", None, None, None ],
             [ "Utility_Software/ComponentStatusScan", None, None, None ],
             [ "Utility_Software/TM-State-Check", None, None, None ],
             [ "Components/ACEXPublisher", None, None, None ],
             [ "Components/AliRootWrapperSubscriber", None, None, None ],
             [ "Components/ASCIIDataWriter", None, None, None ],
             [ "Components/BinaryDataWriter", None, None, None ],
             [ "Components/BlockComparer", None, None, None ],
             [ "Components/DATEPublisher", None, None, None ],
             [ "Components/DDLHeaderPublisher", None, None, None ],
             [ "Components/DDLConnectionTester", None, None, None ],
             [ "Components/DummyLoad", None, None, None ],
             [ "Components/DummyReadoutListProducer", None, None, None ],
             [ "Components/EventDoneDataProducer", None, None, None ],
             [ "Components/EventDoneRequestTestDummy", None, None, None ],
             [ "Components/EventProtocolSubscriber", None, None, None ],
             [ "Components/EventRateSubscriber", None, None, None ],
             [ "Components/EventStorageWriter", None, None, None ],
             [ "Components/FilePublisher", None, None, None ],
             [ "Components/FXSSubscriber", None, None, None ],
             [ "Components/HLTOutWriterSubscriber", None, None, None ],
             [ "Components/HLTOutFormatter", None, None, None ],
             [ "Components/Relay", None, None, None ],
             [ "Components/RandomDataPublisher", None, None, None ],
             [ "Components/RORCPGTestSubscriber", None, None, None ],
             [ "Components/RORCPublisher", None, None, None ],
             [ "Components/ShmDumpSubscriber", None, None, None ],
             [ "Components/SingleBlockUnwrapper", None, None, None ],
             [ "Components/StoreForwarder", None, None, None ],
             [ "Components/TCPDumpSubscriber", None, None, None ],
             [ "Utility_Software/RORCDebugger", None, None, None ],
             [ "Utility_Software/TM_Notifier", None, None, None ],
             ] ],
    ]

svnReleaseTranslator = { "tilib" : { "PSI2" : { "branches/current" : "trunk", "branches/stable" : "trunk" } } }


now = time.time()
now_s = time.gmtime( now )
date_str = "%04d%02d%02d.%02d%02d%02d" % ( now_s.tm_year, now_s.tm_mon, now_s.tm_mday,
                                           now_s.tm_hour, now_s.tm_min, now_s.tm_sec )
if ext_date_str != None:
    date_str = ext_date_str

print "date_str:",date_str
hlt_basedir = os.path.join( ".", "HLT-"+dir_version+"-"+date_str )
os.mkdir( hlt_basedir )
os.mkdir( os.path.join( hlt_basedir, "src" ) )

target_dirs = []

for repos in packages:
    for p in repos[1]:
        if source=="svn":
            if p[1] == None:
                input = p[0]
            else:
                input = p[1]
            if svnReleaseTranslator.has_key(repos[0]) and svnReleaseTranslator[repos[0]].has_key(input) and svnReleaseTranslator[repos[0]][input].has_key(version):
                #print svnReleaseTranslator[repos[0]]
                #print svnReleaseTranslator[repos[0]][input]
                #print svnReleaseTranslator[repos[0]][input][version]
                tmp_version = svnReleaseTranslator[repos[0]][input][version]
            else:
                tmp_version = version
            checkout_cmd = "svn "+svn_rev_arg+" checkout "+repos_basename+"/"+repos[0]+"/"+input+"/"+tmp_version+" "+os.path.join( hlt_basedir, "src", p[0] )
            print "checkout cmd:",checkout_cmd
            print "Checking out package "+input+" from "+repos[0]+" repository to directory "+p[0]
            os.system( checkout_cmd )
        elif source=="dir":
            if p[2]==None:
                input = p[0]
            else:
                input = p[2]
            copy_cmd = "cp -a "+os.path.join( dir, input )+" "+os.path.join( hlt_basedir, "src", p[0] )
            print "copy cmd:",copy_cmd
            print "Copying package "+p[0]+" from directory "+os.path.join( dir, input )
            os.system( "mkdir -p "+os.path.dirname( os.path.join( hlt_basedir, "src", p[0] ) ) )
            os.system( copy_cmd )
        if p[3]!=None:
            print p[3]
            for l in p[3]:
                if l[1]==None:
                    updir=""
                    targetdir=p[0]
                    for d in targetdir.split("/"):
                        if d=="." or d=="":
                            continue
                        updir=os.path.join( updir, ".." )
                    #print os.path.join( updir, l[0] ),"->",os.path.join( hlt_basedir, "src", targetdir, os.path.basename( l[0] ) )
                    try:
                        os.symlink( os.path.join( updir, l[0] ), os.path.join( hlt_basedir, "src", targetdir, os.path.basename( l[0] ) ) )
                    except OSError,e:
                        print "Error trying to link "+os.path.join( updir, l[0] )+" -> "+os.path.join( hlt_basedir, "src", targetdir, os.path.basename( l[0] ) )+": "+str(e)
                else:
                    updir=""
                    targetdir=os.path.join( p[0], os.path.dirname( l[1] ) )
                    for d in targetdir.split("/"):
                        if d=="." or d=="":
                            continue
                        updir=os.path.join( updir, ".." )
                    #print os.path.join( updir, l[0] ),"->",os.path.join( hlt_basedir, "src", targetdir, os.path.basename( l[1] ) )
                    try:
                        os.symlink( os.path.join( updir, l[0] ), os.path.join( hlt_basedir, "src", targetdir, os.path.basename( l[1] ) ) )
                    except OSError,e:
                        print "Error trying to link "+os.path.join( updir, l[0] )+" -> "+os.path.join( hlt_basedir, "src", targetdir, os.path.basename( l[1] ) )+": "+str(e)
                               
        target_dirs.append( p[0] )

makefile = open( os.path.join( hlt_basedir, "src", "Makefile" ), "w" )

makefile.write( "\nTARGETS :=" )
for t in target_dirs:
    makefile.write( " \\\n           "+t )

makefile.write( "\n" )

mk_template = open( os.path.join( build_basedir, "Makefile.template" ), "r" )
line = mk_template.readline()
while line != "":
    makefile.write( line )
    line = mk_template.readline()
makefile.close()

#cmakefile = open( os.path.join( hlt_basedir, "src", "CMakeLists.txt" ), "w" )
#cmk_template = open( os.path.join( build_basedir, "CMakeLists.txt.template" ), "r" )
#cmakefile.write( "CMAKE_MINIMUM_REQUIRED(VERSION 2.6)\n" )
#cmakefile.write( "PROJECT(HLT-"+dir_version+")\n" )
#cmakefile.write( "\n" )
#line = cmk_template.readline()
#while line != "":
#    cmakefile.write( line )
#    line = cmk_template.readline()
#cmakefile.write( "\n" )
#cmakefile.write( "SET(CPACK_PACKAGE_VERSION_MAJOR \""+date_str.split(".")[0]+"\")\n" )
#cmakefile.write( "SET(CPACK_PACKAGE_VERSION_MINOR \""+date_str.split(".")[1]+"\")\n" )
#cmakefile.write( "SET(CPACK_SOURCE_PACKAGE_FILE_NAME \"HLT-"+dir_version+"-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}\")\n" )
#cmakefile.write( "\n" )
#for t in target_dirs:
#    cmakefile.write( "ADD_SUBDIRECTORY("+t+")\n" )
#cmakefile.close()

os.system( "cp "+os.path.join( build_basedir, "install.sh" )+" "+os.path.join( hlt_basedir, "src", "install.sh" ) )
os.system( "cp "+os.path.join( build_basedir, "build-readme" )+" "+os.path.join( hlt_basedir, "README" ) )
os.system( "cp "+os.path.join( build_basedir, "make-setenv-bash.sh" )+" "+os.path.join( hlt_basedir, "src", "make-setenv-bash.sh" ) )
os.system( "cp "+os.path.join( build_basedir, "make-setenv-csh.sh" )+" "+os.path.join( hlt_basedir, "src", "make-setenv-csh.sh" ) )
os.system( "chmod a+x "+os.path.join( hlt_basedir, "src", "install.sh" ) )
os.system( "chmod a+x "+os.path.join( hlt_basedir, "src", "make-setenv-csh.sh" ) )
os.system( "chmod a+x "+os.path.join( hlt_basedir, "src", "make-setenv-bash.sh" ) )

print( "if [ -d "+os.path.join( hlt_basedir, "src" )+" -a -f "+os.path.join( hlt_basedir, "src", "Makefile" )+" ] ; then cd "+os.path.join( hlt_basedir, "src" )+" ; make clean ; make wipe ; fi" )
os.system( "if [ -d "+os.path.join( hlt_basedir, "src" )+" -a -f "+os.path.join( hlt_basedir, "src", "Makefile" )+" ] ; then cd "+os.path.join( hlt_basedir, "src" )+" ; make clean &>/dev/null ; make wipe &>/dev/null ; fi" )
os.system( "find "+os.path.join( hlt_basedir, "src" )+" -name \"Makefile*depend\" -exec rm \"{}\" \;" )
os.system( "find "+os.path.join( hlt_basedir, "src" )+" -type d \\( \\( -name bin -or -name lib -or -name objs \\) -and -not \\( -iregex '.*/BCL/bin' -or -iregex '.*/RunManager/bin' \\) \\) -exec rm -rf \"{}\" \;" )
if dist:
    os.system( "find "+os.path.join( hlt_basedir, "src" )+" -type d -name .svn -exec rm -rf \"{}\" \;" )

