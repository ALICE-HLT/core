#!/bin/bash

if [ $# -lt 3 ] ; then
    echo Usage: $0 '<base install directory>' '<pythonpath>' '<output-directory>'
    exit 1
fi

cat >$3/setenv.csh <<EOF
#!/bin/csh
if ( 0PATH == 0 ) setenv PATH ""
setenv PATH ${PATH}:$1/bin
if ( 0PYTHONPATH == 0 ) setenv PYTHONPATH ""
setenv PYTHONPATH ${PYTHONPATH}:$2
if ( 0LD_LIBRARY_PATH == 0 ) setenv LD_LIBRARY_PATH ""
setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:$1/lib
EOF
