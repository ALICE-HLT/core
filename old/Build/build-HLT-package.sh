#!/bin/bash

if [ $# -ne 1 ] ; then
  echo "Usage: $0 (trunk|"
  exit 1
fi

BASEDIR=`dirname $0`

export LANG=C

TYPE=$1
${BASEDIR}/pack.py -svn https://www.kip.uni-heidelberg.de/repos/TI ${TYPE} 2>&1 | tee pack-${TYPE}-`date +%Y%m%d.%H%M%S`.out
ID=`ls -1d HLT-${TYPE}-????????.??????| tail -n1|awk -F/ '{print $1}'`
pushd ${ID}
( ls -1 ; find src/ -type d -not -iregex ".*/\.svn.*" | sort) > ../${ID}.contents.template.for.diff
popd
diff ${ID}.contents.template.for.diff ${BASEDIR}/contents.template.for.diff 2>&1 | tee ${ID}.contents.diff
MSG=`cat ${ID}.contents.diff`
if [ -n "${MSG}" ] ; then
    echo Error retrieving contents from repository
    exit 1
fi

BUILDDIR=`pwd`
pushd ${BASEDIR}/..
# pushd ${BUILDDIR}/${ID}/src
svn-versions.py | grep -Ei "^(=========|\. --- |(branches/)?${TYPE}: |$)"|sed -r -e "s|(branches/)?${TYPE}: release-||g" -e "s|\. --- (Util/[^/]+)$|\1|g" -e "s|\. --- (Components/[^/]+)$|\1|g" -e "s|\. --- ([^/]+)$|\1|g" >${BUILDDIR}/${ID}/Versions
popd
tar cjf ${ID}.tar.bz2 ${ID}

# cp -a ${ID} ${ID}-test
# pushd ${ID}-test/src
# ( make PRODUCTION=1 clean ; make PRODUCTION=1 all ; ./install.sh -production ) 2>&1 | tee ../../${ID}-build-release.out
# ( make clean ; make all ; ./install.sh ) 2>&1 | tee ../../${ID}-build-debug.out
# ( make PRODUCTION=1 DEBUG=1 clean ; make PRODUCTION=1 DEBUG=1 all ; ./install.sh -productiondebug ) 2>&1 | tee ../../${ID}-build-releasedebug.out
# popd

cp -a ${ID} ${ID}-cmake-test
mkdir ${ID}-cmake-test/build
mkdir ${ID}-cmake-test/install
pushd ${ID}-cmake-test/build
( cmake -D CMAKE_INSTALL_PREFIX=${BUILDDIR}/${ID}-cmake-test/install ../src && make && make install ) 2>&1 | tee ../../${ID}-cmake-build-release.out
popd

