#!/usr/bin/env python

import os, re, sys

if len(sys.argv)!=3:
    sys.stderr.write( "Usage: "+sys.argv[0]+" <configuration-entry> <configuration-value>\n" )
    sys.exit( -1 )

entryMatch = re.compile( "^\s*"+sys.argv[1]+"\s*:=\s*(.*)$" )

line = sys.stdin.readline()
while line != "":
    match = entryMatch.search( line )
    if match != None:
        print "# Before Configure.py:\n",
        print "# "+line,
        print sys.argv[1]+" := "+sys.argv[2]+"\n",
    else:
        print line,
    line = sys.stdin.readline()

