#!/bin/bash

DIR=`pwd`
TOPDIR=`dirname $DIR`

#export XERCESCROOT=$DIR/xerces-c-src_2_6_0

#XERCESTARGET=$DIR/xerces-c-2.6.0
XERCESTARGET=$DIR/xerces-c

TARGET=`uname -s`-`uname -m`

MAKEDEPENDDIRS="kip/MLUC/src kip/MLUC/test/Monitor kip/MLUC/test/BufferedLogTest kip/MLUC/test/Histogram kip/MLUC/test/TimedTasks kip/MLUC/test/gettimeofdayTest kip/BCL/src kip/BCL/test/SimpleMsgTest kip/BCL/test/SimpleBlobTest kip/BCL/test/SimpleBlobBench kip/TaskManager AliHLT"

MAKEDIRS="pci/psi/src/driver.linux pci/psi/src/libpsi.linux kip/MLUC/src kip/MLUC/test/Monitor kip/MLUC/test/BufferedLogTest kip/MLUC/test/Histogram kip/MLUC/test/TimedTasks kip/MLUC/test/gettimeofdayTest kip/BCL/src kip/BCL/test/SimpleMsgTest kip/BCL/test/SimpleBlobTest kip/BCL/test/SimpleBlobBench kip/TaskManager AliHLT kip/TMGUI/TMControlInterface Components/ShmDumpSubscriber Components/TCPDumpSubscriber HOMER/reader/src HOMER/reader/doc HOMER/reader/apps/homer_read Components/AliRootWrapperSubscriber Util/SimpleComponentWrapper"

BINDIRS="AliHLT/bin/$TARGET kip/TaskManager/bin/$TARGET Components/ShmDumpSubscriber/bin/$TARGET Components/TCPDumpSubscriber/bin/$TARGET HOMER/reader/bin/$TARGET HOMER/reader/bin/$TARGET Components/AliRootWrapperSubscriber/bin/$TARGET Util/SimpleComponentWrapper/bin/$TARGET"
ADDBINS="pci/psi/src/libpsi.linux/closeall kip/MLUC/test/Monitor/Monitor kip/MLUC/test/BufferedLogTest/BufferedLogTest kip/MLUC/test/Histogram/Histogram kip/MLUC/test/TimedTasks/TimedTasks kip/MLUC/test/gettimeofdayTest/gettimeofdayTest kip/BCL/test/SimpleMsgTest/SimpleMsgTest kip/BCL/test/SimpleBlobTest/SimpleBlobTest kip/BCL/test/SimpleBlobBench/SimpleBlobBench kip/TMGUI/TMControlInterface/$TARGET/TMControlInterface.so kip/TMGUI/TMGUI1Form.py kip/TMGUI/TMGUI1.py kip/TMGUI/TMGUI2.py kip/TMGUI/TMGUI.py python/XMLRead/XMLRead.py SimpleChainConfig1/MakeTaskManagerConfig.py SimpleChainConfig1/SimpleChainConfig1.py python/ERSGUI/ERSGUIForm.py python/ERSGUI/ERSGUILargeForm.py python/ERSGUI/ERSGUI.py python/ERSGUI/ERSGUILarge.py"
LIBDIRS="AliHLT/lib/$TARGET kip/TaskManager/lib/$TARGET kip/MLUC/lib/$TARGET kip/BCL/lib/$TARGET pci/psi/src/libpsi.linux HOMER/reader/lib/$TARGET"
INCLUDEDIRS="kip/MLUC kip/BCL kip/TaskManager AliHLT/Base AliHLT/SC AliHLT/DDL AliHLT/Trigger AliHLT/PubSub AliHLT/WorkerComp AliHLT/DataFlowComp AliHLT/HLT-Out AliHLT/HLT-RORC AliHLT/TaskManLib HOMER/reader"

if [ -n "`g++ -v 2>&1 |grep "version 2"`" ] ; then 
    gcc=2
else
    gcc=3
fi

if [ -z "$ALIHLT_TOPDIR" ] ; then echo The ALIHLT_TOPDIR environment variable has to be set for the AliRootWrapperSubscriber. ; exit 1 ; fi

if [ -z "$XERCESCROOT" ] ; then
    #XERCESCDIR=""
    if [ -z "$XERCESCDIR" ] ; then
        for dir in /usr /usr/local /opt ; do
            if [ -d $dir/include/xercesc -a -e $dir/lib/libxerces-c.so ] ; then
                XERCESCDIR=$dir
                break
            fi
        done
    fi
    if [ -z "$XERCESCDIR" ] ; then
        LASTXERCESCDIR=`ls -1d xerces-c-src_*|sort|tail -n1`
        if [ -z "$LASTXERCESCDIR" ] ; then
            if [ -e xerces-c-current.tar.gz ] ; then
                echo Unpacking xerces-c-current.tar.gz
                tar xzf xerces-c-current.tar.gz
            elif [ -e xerces-c-current.zip ] ; then
                echo Unpacking xerces-c-current.zip
                unzip xerces-c-current.zip
            else
                echo Could not find Xerces C++ library development files
                echo "*" Install a system package with the Xerces C++ development files
                echo " " or
                echo "*" Specify the pathname of an existing directory with the include/xercesc directory and the lib/libxerces-c.so library in the XERCESCDIR environment variable
                echo " " or
                echo "*" Specify the full pathname to a Xerces C++ source directory in the XERCESCROOT environment variable
                echo " " or
                echo "*" Place a copy of the current Xerces C++ sources in a xerces-c-current.tar.gz file \(e.g. from http://www.apache.org/dist/xml/xerces-c/xerces-c-current.tar.gz\) into the current directory
                exit -1
            fi
        fi
        LASTXERCESCDIR=`ls -1d xerces-c-src_*|sort|tail -n1`
        if [ -n "$LASTXERCESCDIR" ] ; then
            XERCESCROOT=$LASTXERCESCDIR
        else
            echo Error trying to locate Xerces C++ library development files
            echo Please try one of the following:
            echo "*" Install a system package with the Xerces C++ development files
            echo " " or
            echo "*" Specify the pathname of an existing directory with the include/xercesc directory and the lib/libxerces-c.so library in the XERCESCDIR environment variable
            echo " " or
            echo "*" Specify the full pathname to a Xerces C++ source directory in the XERCESCROOT environment variable
            echo " " or
            echo "*" Place a copy of the current Xerces C++ sources in a xerces-c-current.tar.gz file \(e.g. from http://www.apache.org/dist/xml/xerces-c/xerces-c-current.tar.gz\) into the current directory
            exit -1
        fi
    else
        echo Using Xerces C++ development files found in $XERCESCDIR
    fi
fi
if [ -n "$XERCESCROOT" ] ; then
    pushd .
    cd $XERCESCROOT/src/xercesc
    echo Configuring Xerces C++ Library
    ./runConfigure -p linux -r pthread -P $XERCESTARGET
    echo Building Xerces C++ Library
    make
    echo Installing Xerces C++ Library
    make install
    XERCESCDIR=$XERCESTARGET
    LIBDIRS="$LIBDIRS $XERCESTARGET/lib"
    popd
fi
if [ -z "$XERCESCDIR" ] ; then
    echo Error trying to locate or install Xerces C++ library development files
    echo Please try one of the following:
    echo "*" Install a system package with the Xerces C++ development files
    echo " " or
    echo "*" Specify the pathname of an existing directory with the include/xercesc directory and the lib/libxerces-c.so library in the XERCESCDIR environment variable
    echo " " or
    echo "*" Specify the full pathname to a Xerces C++ source directory in the XERCESCROOT environment variable
    echo " " or
    echo "*" Place a copy of the current Xerces C++ sources in a xerces-c-current.tar.gz file \(e.g. from http://www.apache.org/dist/xml/xerces-c/xerces-c-current.tar.gz\) into the current directory
    exit -1
fi

root=0
if [ -z "$ROOTSYS" ] ; then
    if [ ! -d root ] ; then
        NEWEST_ROOT_FILE=`ls -1 root_v*.source.tar.gz|tail -n1`
        if [ -n "$NEWEST_ROOT_FILE" ] ; then
            echo Using the $NEWEST_ROOT_FILE ROOT tar file.
            tar xvzf $NEWEST_ROOT_FILE
        fi
    fi
    if [ -d root ] ; then
        echo Configuring root
        pushd .
        cd root
        export ROOTSYS=$DIR/root
        ./configure linux --enable-soversion --enable-thread --prefix=$TOPDIR/root
        echo Building root
        make
        echo Building additonal root cint dlls
        make cintdlls
        cd lib
        mkdir root
        cd root
        ln -s ../../cint
        cd ../..
        cd include
        ln -s . root
        echo Installing root
        cd $DIR
        tar cf root-tmp.tar root
        cd $TOPDIR
        tar xf src/root-tmp.tar
        rm src/root-tmp.tar
        root=1
        popd
        export ROOTSYS=$TOPDIR/root
    fi
fi
if [ -n "$ROOTSYS" ] ; then
    export ROOT=1
fi

if [ -z "$ROOTSYS" ] ; then
    echo The ROOTSYS environment variable has to be set for the AliRootWrapperSubscriber.
    echo "*" Set the ROOTSYS environment variable to point to an already existing ROOT installation.
    echo " " or
    echo "*" Install a ROOT package and set the ROOTSYS environment variable accordingly.
    echo " " or
    echo "*" Unpack a source package in the current directory so that a \'root\' directory is created holding the sources.
    echo " " or
    echo "*" Place a .tar file holding a source package into the current directory. The package must have a naming of \'root_vVVVVVV.source.tar.gz\' with the VVVVVV part corresponding to a version number.
    exit -1
fi


PYTHONINCLUDE=`ls -1d /usr/include/python* 2>/dev/null|sort | tail -n 1`
PYTHONLIB=`ls -1d /usr/lib/python* 2>/dev/null|sort | tail -n 1`
PYTHONLIBFILE=`basename $PYTHONLIB`
LIBDB=`find /lib /usr/lib -name "libdb-3.*.so" 2>/dev/null | sort|tail -n1`
echo Python include path: $PYTHONINCLUDE
echo Python library path: $PYTHONLIB
echo Python library file: $PYTHONLIBFILE
echo DB library: $LIBDB

echo Making TaskManager configuration
mv kip/TaskManager/conf/Makefile.conf kip/TaskManager/conf/Makefile.conf.orig
cat kip/TaskManager/conf/Makefile.conf.orig | ./Configure.py MLUCTOPDIR $DIR/kip/MLUC > kip/TaskManager/conf/Makefile.conf.tmp1
cat kip/TaskManager/conf/Makefile.conf.tmp1 | ./Configure.py BCLTOPDIR $DIR/kip/BCL > kip/TaskManager/conf/Makefile.conf.tmp2
cat kip/TaskManager/conf/Makefile.conf.tmp2 | ./Configure.py XERCESTOPDIR $XERCESCDIR > kip/TaskManager/conf/Makefile.conf.tmp3
cat kip/TaskManager/conf/Makefile.conf.tmp3 | ./Configure.py PYTHONINCLUDEDIR -I$PYTHONINCLUDE > kip/TaskManager/conf/Makefile.conf.tmp4
cat kip/TaskManager/conf/Makefile.conf.tmp4 | ./Configure.py PYTHONLIBPATH $PYTHONLIB/config > kip/TaskManager/conf/Makefile.conf.tmp5
cat kip/TaskManager/conf/Makefile.conf.tmp5 | ./Configure.py PYTHONLIBFILE "\$(shell ls \$(PYTHONLIBPATH)/lib${PYTHONLIBFILE}.a)" > kip/TaskManager/conf/Makefile.conf.tmp6
cat kip/TaskManager/conf/Makefile.conf.tmp6 | ./Configure.py PYTHONLIB "-l\$(PYTHONLIBLIB) -lpthread -ldl -lutil $LIBDB" > kip/TaskManager/conf/Makefile.conf
rm -f kip/TaskManager/conf/Makefile.conf.tmp1 kip/TaskManager/conf/Makefile.conf.tmp2 kip/TaskManager/conf/Makefile.conf.tmp3 kip/TaskManager/conf/Makefile.conf.tmp4 kip/TaskManager/conf/Makefile.conf.tmp5 kip/TaskManager/conf/Makefile.conf.tmp6

echo Making TMGUI Configuration
mv kip/TMGUI/TMControlInterface/Makefile kip/TMGUI/TMControlInterface/Makefile.orig
cat kip/TMGUI/TMControlInterface/Makefile.orig | ./Configure.py MLUCTOPDIR $DIR/kip/MLUC > kip/TMGUI/TMControlInterface/Makefile.tmp1
cat kip/TMGUI/TMControlInterface/Makefile.tmp1 | ./Configure.py BCLTOPDIR $DIR/kip/BCL > kip/TMGUI/TMControlInterface/Makefile.tmp2
cat kip/TMGUI/TMControlInterface/Makefile.tmp2 | ./Configure.py TMTOPDIR $DIR/kip/TaskManager > kip/TMGUI/TMControlInterface/Makefile.tmp3
cat kip/TMGUI/TMControlInterface/Makefile.tmp3 | ./Configure.py PYTHONINCLUDEDIR -I$PYTHONINCLUDE > kip/TMGUI/TMControlInterface/Makefile.tmp4
cat kip/TMGUI/TMControlInterface/Makefile.tmp4 | ./Configure.py PYTHONLIBPATH $PYTHONLIB/config > kip/TMGUI/TMControlInterface/Makefile.tmp5
cat kip/TMGUI/TMControlInterface/Makefile.tmp5 | ./Configure.py PYTHONLIBFILE "\$(shell ls \$(PYTHONLIBPATH)/lib${PYTHONLIBFILE}.a)" > kip/TMGUI/TMControlInterface/Makefile.tmp6
cat kip/TMGUI/TMControlInterface/Makefile.tmp6 | ./Configure.py PYTHONLIB "-l\$(PYTHONLIBLIB) -lpthread -ldl -lutil $LIBDB" > kip/TMGUI/TMControlInterface/Makefile
rm -f kip/TMGUI/TMControlInterface/Makefile.tmp1 kip/TMGUI/TMControlInterface/Makefile.tmp2 kip/TMGUI/TMControlInterface/Makefile.tmp3 kip/TMGUI/TMControlInterface/Makefile.tmp4 kip/TMGUI/TMControlInterface/Makefile.tmp5 kip/TMGUI/TMControlInterface/Makefile.tmp6

echo Making AliHLT Configuration
mv AliHLT/conf/Makefile.conf AliHLT/conf/Makefile.conf.orig
cat AliHLT/conf/Makefile.conf.orig | ./Configure.py KIPDIR $DIR/kip > AliHLT/conf/Makefile.conf.tmp1
cat AliHLT/conf/Makefile.conf.tmp1 | ./Configure.py PSIDIR $DIR/pci/psi > AliHLT/conf/Makefile.conf
rm -f AliHLT/conf/Makefile.conf.orig AliHLT/conf/Makefile.conf.tmp1

mv Components/AliRootWrapperSubscriber/Makefile Components/AliRootWrapperSubscriber/Makefile.orig
cat Components/AliRootWrapperSubscriber/Makefile.orig | ./Configure.py BASEDIR $DIR > Components/AliRootWrapperSubscriber/Makefile.tmp1
cat Components/AliRootWrapperSubscriber/Makefile.tmp1 | ./Configure.py ALIROOTINTERFACEDIR $ALIHLT_TOPDIR > Components/AliRootWrapperSubscriber/Makefile.tmp2
cat Components/AliRootWrapperSubscriber/Makefile.tmp2 | ./Configure.py ROOTDIR $ROOTSYS > Components/AliRootWrapperSubscriber/Makefile
rm -f Components/AliRootWrapperSubscriber/Makefile.orig Components/AliRootWrapperSubscriber/Makefile.tmp1 Components/AliRootWrapperSubscriber/Makefile.tmp2

mv Util/SimpleComponentWrapper/Makefile Util/SimpleComponentWrapper/Makefile.orig
cat Util/SimpleComponentWrapper/Makefile.orig | ./Configure.py BASEDIR $DIR > Util/SimpleComponentWrapper/Makefile.tmp1
cat Util/SimpleComponentWrapper/Makefile.tmp1 | ./Configure.py ALIROOTINTERFACEDIR $ALIHLT_TOPDIR > Util/SimpleComponentWrapper/Makefile.tmp2
cat Util/SimpleComponentWrapper/Makefile.tmp2 | ./Configure.py ROOTDIR $ROOTSYS > Util/SimpleComponentWrapper/Makefile
rm -f Util/SimpleComponentWrapper/Makefile.orig Util/SimpleComponentWrapper/Makefile.tmp1 Util/SimpleComponentWrapper/Makefile.tmp2

echo Making ShmDumpSubscriber Configuration
mv Components/ShmDumpSubscriber/Makefile Components/ShmDumpSubscriber/Makefile.orig
cat Components/ShmDumpSubscriber/Makefile.orig | ./Configure.py BASEDIR $DIR > Components/ShmDumpSubscriber/Makefile.tmp1
cat Components/ShmDumpSubscriber/Makefile.tmp1 | ./Configure.py ROOTINTERFACEDIR $DIR/Util/ROOTInterface > Components/ShmDumpSubscriber/Makefile
rm -f Components/ShmDumpSubscriber/Makefile.orig Components/ShmDumpSubscriber/Makefile.tmp1

echo Making TCPDumpSubscriber Configuration
mv Components/TCPDumpSubscriber/Makefile Components/TCPDumpSubscriber/Makefile.orig
cat Components/TCPDumpSubscriber/Makefile.orig | ./Configure.py BASEDIR $DIR > Components/TCPDumpSubscriber/Makefile.tmp1
cat Components/TCPDumpSubscriber/Makefile.tmp1 | ./Configure.py ROOTINTERFACEDIR $DIR/Util/ROOTInterface > Components/TCPDumpSubscriber/Makefile
rm -f Components/TCPDumpSubscriber/Makefile.orig Components/TCPDumpSubscriber/Makefile.tmp1


if [ gcc = 2 ] ; then
    echo Found gcc Version 2.
    echo -n Adapting configuration files...
    conffiles=`find . \( -name Makefile -or -name Makefile.conf \) -exec grep -Eil -- "-m(arch=pentium3|mmx|sse|fpmath=sse)" "{}" \;`
    for f in $conffiles ; do
	mv $f $f.tmp
	cat $f.tmp | sed -e "s/ -mmmx//" |sed -e "s/ -msse//" | sed -e "s/ -mfpmath=sse,387//" | sed -e "s/ -mfpmath=sse//" | sed -e "s/-march=pentium3/-march=pentiumpro/"; > $f
	rm $f.tmp
    done
    echo "  " Done
fi


echo Now making library dependencies
for i in $MAKEDEPENDDIRS ; do
    echo Making dependencies in $i
    pushd .
    cd $i
    make depend
    popd
done

echo Now building libraries
for i in $MAKEDIRS ; do
    echo Building in $i
    pushd .
    cd $i
    make
    popd
done

echo Installing Libraries
mkdir -p ../lib/$TARGET
for d in $LIBDIRS ; do
    echo "  " Directory $d
    cp $d/*.so* $d/*.a ../lib/$TARGET
done

echo Installing binaries
mkdir -p ../bin/$TARGET
for d in $BINDIRS ; do
    echo "  " Directory $d
    cp $d/* ../bin/$TARGET
done
for b in $ADDBINS ; do
    echo "  " file $b
    cp $b ../bin/$TARGET
done
mv ../bin/$TARGET/MakeTaskManagerConfig.py ../bin/$TARGET/MakeTaskManagerConfig.py.orig
#cat ../bin/$TARGET/MakeTaskManagerConfig.py.orig | ./Replace.py "includePaths = \[ outputDir\+\"/templates\", baseDir\+\"/templates\" \]" "includePaths = [ outputDir+\"/templates\", \"$TOPDIR/share/SimpleChainConfig1/templates\", baseDir+\"/templates\" ]" > ../bin/$TARGET/MakeTaskManagerConfig.py.tmp1
cat ../bin/$TARGET/MakeTaskManagerConfig.py.orig | ./Replace.py "includePaths = \[ outputDir\+\"/templates\", baseDir\+\"/templates\" \]" "includePaths = [ outputDir+\"/templates\", baseDir+\"/../../share/SimpleChainConfig1/templates\", baseDir+\"/templates\" ]" > ../bin/$TARGET/MakeTaskManagerConfig.py.tmp1
#cat ../bin/$TARGET/MakeTaskManagerConfig.py.tmp1 | ./Replace.py "outputter2 = SimpleChainConfig1\.TaskManagerCheckOutputter\( masterNode, useSSH, includePaths, taskManDir, frameworkDir, platform, runDir, outputDir, prestartExec, baseDir\+\"/scripts\" \)" "outputter2 = SimpleChainConfig1.TaskManagerCheckOutputter( masterNode, useSSH, includePaths, taskManDir, frameworkDir, platform, runDir, outputDir, prestartExec, \"$TOPDIR/share/SimpleChainConfig1/scripts\" )" > ../bin/$TARGET/MakeTaskManagerConfig.py
cat ../bin/$TARGET/MakeTaskManagerConfig.py.tmp1 | ./Replace.py "outputter2 = SimpleChainConfig1\.TaskManagerCheckOutputter\( masterNode, useSSH, includePaths, taskManDir, frameworkDir, platform, runDir, outputDir, prestartExec, baseDir\+\"/scripts\" \)" "outputter2 = SimpleChainConfig1.TaskManagerCheckOutputter( masterNode, useSSH, includePaths, taskManDir, frameworkDir, platform, runDir, outputDir, prestartExec, baseDir+\"/../../share/SimpleChainConfig1/scripts\" )" > ../bin/$TARGET/MakeTaskManagerConfig.py
rm ../bin/$TARGET/MakeTaskManagerConfig.py.orig ../bin/$TARGET/MakeTaskManagerConfig.py.tmp1
chmod a+x ../bin/$TARGET/*

echo Installing include files
mkdir -p ../include
for d in $INCLUDEDIRS ; do
    echo "  " Directory $d
    mkdir -p ../include/$d
    cp $d/include/* ../include/$d
done

echo Installing additional files
mkdir -p ../share/SimpleChainConfig1/templates
cp SimpleChainConfig1/templates/* ../share/SimpleChainConfig1/templates
mkdir -p ../share/SimpleChainConfig1/scripts
cp -a SimpleChainConfig1/scripts/* ../share/SimpleChainConfig1/scripts
mkdir -p ../share/doc/HOMER/reader
cp HOMER/reader/doc/HOMERReader.ps HOMER/reader/doc/HOMERReader.pdf ../share/doc/HOMER/reader


echo Installing TPC HOMER Online Display
mkdir -p $TOPDIR/share/TPC-OnlineDisplayHOMER
cp $DIR/OnlineDisplay/TPC-OnlineDisplayHOMER/* $TOPDIR/share/TPC-OnlineDisplayHOMER

echo Creating environment setup scripts
echo "#!/bin/bash" >../bin/setenv.sh
echo >>../bin/setenv.sh
echo export ALIHLT_DC_DIR=$TOPDIR >>../bin/setenv.sh
echo export PATH=\$PATH:\$ALIHLT_DC_DIR/bin/$TARGET >>../bin/setenv.sh
echo export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:\$ALIHLT_DC_DIR/lib/$TARGET >>../bin/setenv.sh
if [ $root = 1 ]; then
    echo >>../bin/setenv.sh
    echo export ROOTSYS=\$ALIHLT_DC_DIR/root >>../bin/setenv.sh
    echo export PATH=\$PATH:\$ROOTSYS >>../bin/setenv.sh
    echo export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:\$ROOTSYS >>../bin/setenv.sh
fi
echo >>../bin/setenv.sh
echo export ALIHLT_LIBDIR=\$ALIHLT_DC_DIR/lib/$TARGET/ROOT >>../bin/setenv.sh
echo export ALIHLT_DATADIR=\$ALIHLT_DC_DIR/share/TPC-OnlineDisplayHOMER >>../bin/setenv.sh
echo >>../bin/setenv.sh
chmod a+x ../bin/setenv.sh


echo "#!/bin/tcsh" >../bin/setenv.csh
echo >>../bin/setenv.csh
echo setenv ALIHLT_DC_DIR $TOPDIR >>../bin/setenv.csh
echo setenv PATH \${PATH}:\${ALIHLT_DC_DIR}/bin/${TARGET} >>../bin/setenv.csh
echo setenv LD_LIBRARY_PATH \${LD_LIBRARY_PATH}:\${ALIHLT_DC_DIR}/lib/${TARGET} >>../bin/setenv.csh
if [ $root =  1 ]; then
    echo >>../bin/setenv.csh
    echo setenv ROOTSYS \${ALIHLT_DC_DIR}/root >>../bin/setenv.csh
    echo setenv PATH \${PATH}:\${ROOTSYS} >>../bin/setenv.csh
    echo setenv LD_LIBRARY_PATH \${LD_LIBRARY_PATH}:\${ROOTSYS} >>../bin/setenv.csh
fi
echo >>../bin/setenv.csh
echo setenv ALIHLT_LIBDIR \${ALIHLT_DC_DIR}/lib/${TARGET/ROOT} >>../bin/setenv.csh
echo setenv ALIHLT_DATADIR \${ALIHLT_DC_DIR}/share/TPC-OnlineDisplayHOMER >>../bin/setenv.csh
echo >>../bin/setenv.csh
chmod a+x ../bin/setenv.csh
echo Done