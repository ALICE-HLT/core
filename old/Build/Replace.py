#!/usr/bin/env python

import os, re, sys

if len(sys.argv)!=3:
    sys.stderr.write( "Usage: "+sys.argv[0]+" <original-entry-regexp> <replace-entry>\n" )
    sys.exit( -1 )

entryMatch = re.compile( "^(\s*)"+sys.argv[1]+"(\s*)$" )

line = sys.stdin.readline()
while line != "":
    match = entryMatch.search( line )
    if match != None:
        print match.group( 1 )+sys.argv[2]+match.group( 2 )+"\n",
    else:
        print line,
    line = sys.stdin.readline()

