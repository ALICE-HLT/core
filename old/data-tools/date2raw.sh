#!/bin/bash
# 
# Usage : date2raw <BASEDIR>
#
# Give basedir, where the date files liegen as argument 
#


OUTFILE=DATE2RAW.log
date > ${OUTFILE}

BASEDIR=$1

if [ $# -ge 1 ] ; then
    BASEDIR=$1
fi

if [ ! -d ${BASEDIR} ] ; then
    echo Directory ${BASEDIR} cannot be found. >/dev/stderr
    exit 1
fi

DATE_FILES=`find ${BASEDIR} -name "castor*.raw" -type f`
LASTRUNID=""

for f in ${DATE_FILES} ; do
    CUTOUT=`basename ${f}|awk -F. '{print $2}'`
    
    if [ "$CUTOUT" == "000" ] ; then
	continue
    fi
	

    RUNID=`basename ${f}|awk -F- '{print $8}'| awk -F. '{print $1}'`
    
  

    if [ "${RUNID}" != "${LASTRUNID}" ] ; then

	cd ${BASEDIR}

	echo "RUN ID -- ${RUNID}"
	echo "RUN ID -- ${RUNID}" >> $OUTFILE

	echo "Processing file:" `basename ${f}`
	LASTRUNID=${RUNID}
	
	if [ ! -d ${BASEDIR}/${RUNID} ] ; then
	    mkdir ${BASEDIR}/${RUNID}
	fi

	cd ${BASEDIR}/${RUNID}
	
	extra=0
    else
	extra=1

	echo "Processing file:" `basename ${f}`

	TEST_FILES=`find  ${BASEDIR}/${RUNID} -name "*.dateevent" -type f`
	LASTEVENT=0
	for tt in ${TEST_FILES} ; do 
	    HEX=`basename ${tt}|awk -F- '{print $3}'| awk -Fx '{print $2}'`
	    DEC=$(echo "ibase=16; $HEX" | bc)

	    if [ $DEC -gt $LASTEVENT ] ; then
		LASTEVENT=$DEC
	    fi

	done

	if [ ! -d ${BASEDIR}/${RUNID}/extra ] ; then
	    mkdir ${BASEDIR}/${RUNID}/extra
	fi
	cd  ${BASEDIR}/${RUNID}/extra
	
	
    fi
    
    eqcheck=0
    EQ=`ReadDATEData -file ${f} -eventstart 0 -eventstop 1 -showequipments`

    for eqID in ${EQ} ; do
	if [ $eqcheck -ne 2 ] ; then
	    let eqcheck=$eqcheck+1
	    continue
	else
	    eqcheck=0

	    ReadDATEData -file ${f} -equipmentid ${eqID}

	    if [ $extra -eq 1 ] ; then

		xEV=`ls -1 ${BASEDIR}/${RUNID}/extra | wc -l`
		evLAST=$LASTEVENT
		for ((oldEV=0; oldEV < xEV ; oldEV++))  
		do
		    oldEVhex=$(echo "obase=16; $oldEV" | bc)
		    oldEVhex="0x$oldEVhex"

		    OLDSRC="${BASEDIR}/${RUNID}/extra/DATEEvent-Event-`printf "0x%016X" $oldEVhex`-Equipment-`printf "0x%016X" $eqID`.dateevent"


		    let evLAST=$evLAST+1

		    evID=$(echo "obase=16; $evLAST" | bc)
		    evID="0x$evID"
		    
		    NEWSRC="${BASEDIR}/${RUNID}/DATEEvent-Event-`printf "0x%016X" $evID`-Equipment-`printf "0x%016X" $eqID`.dateevent"


		  mv $OLDSRC $NEWSRC
		done 
		
	    fi

	fi

    done

    cd ${BASEDIR}
    if [ -d ${BASEDIR}/${RUNID}/extra ] ; then
	rm -rf ${BASEDIR}/${RUNID}/extra
    fi

done
echo date2raw - DONE