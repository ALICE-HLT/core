#!/bin/bash
#
# Call this script in a directory where rootfiles are stored
# 

export ALIMDC_RAWDB1=$PWD/mdc1
export ALIMDC_RAWDB2=$PWD/mdc2
export ALIMDC_RUNDB=$PWD/mdc1/meta
export ALIMDC_TAGDB=$PWD/mdc1/tags

for f in $*; do
  o=`basename $f | sed 's/\.raw/.root/;'`
  echo $f -\> $o
  aliroot -bq <<fnord
AliSimulation sim;
sim.ConvertDateToRoot("$f","$o");
fnord
done
