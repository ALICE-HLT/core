#!/bin/bash
# 
# * Create configuration Files for HLT
#
# File   : create_config-ECS.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.08.2007
#
###################################################################

TMPDIR=`dirname $0`
pushd ${TMPDIR} >/dev/null
cd ..
BASEDIR=`pwd`
popd >/dev/null

# -- DEFAULTS --
# --------------

# enable/disable Creation of config files 
ENABLECREATION=1

#default Beamtype = pp
BEAMTYPE=pp

#default Runtype = PHYSICS
RUNTYPE=PHYSICS

# -- Check Comand Line Arguments --
# ---------------------------------
USAGE="Usage : $0 -detector=<Detector-ID> -beamtype=<ALICE beam type (pp|AA)> -runtype=<detector run-type> -detectordir=<Detector-directory> -master=<master-node> -basesocket=<base-socket> -inddllist=<input-ddl-list> -outddllist=<output-ddl-list> -ctptriggerclasses=<ctp-trigger-classes> [--help]"

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector directory
	    -detectordir* ) DETECTORDIR=$arg ;;    

	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get beamtype, pp or AA
	    -beamtype* )  BEAMTYPE=$arg ;;

	    # get runtype
	    -runtype* )  RUNTYPE=$arg ;;

	    # get masternode
	    -master* )  MASTERNODE=$arg ;;

	    # get base socket
	    -basesocket* )  BASESOCKET=$arg ;;

	    # get input ddl list
	    -inddllist* )  INPUTDDLLIST=$arg ;;

	    # get output ddl list
	    -outddllist* )  OUTPUTDDLLIST=$arg ;;

	    # get ctp trigger classes
	    -ctptriggerclasses* )  CTPTRIGGERCLASSES=$arg ;;

	    * ) echo "Error : Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    else
	echo "Error : Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
    fi
done

## BUGFIX -- needed in ECS proxy -- remove later on
if [ "${BEAMTYPE}" = "PP" ] ; then
    BEAMTYPE=pp
fi



# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} ]] ; then
    echo "Error : Missing -detector parameter"
    echo "Error : " ${USAGE}
    exit 1
fi
if [[ ! ${DETECTORDIR} ]] ; then
    echo "Error : Missing -detectordir parameter"
    echo "Error : DETECTORDIR: " ${DETECTORDIR}
    echo "Error : " ${USAGE}
    exit 1
fi
if [[ ! ${MASTERNODE} ]] ; then
    echo "Error : Missing -master parameter"
    echo "Error : " ${USAGE}
    exit 1
fi
if [[ ! ${BEAMTYPE} ]] ; then
    echo "Error : Missing -beamtype parameter"
    echo "Error : " ${USAGE}
    exit 1
fi
if [[ ! ${RUNTYPE} ]] ; then
    echo "Error : Missing -runtype parameter"
    echo "Error : " ${USAGE}
    exit 1
fi
if [[ ! ${BASESOCKET} ]] ; then
    echo "Error : Missing -basesocket parameter"
    echo "Error : " ${USAGE}
    exit 1
fi
if [[ ! ${INPUTDDLLIST} ]] ; then
    echo "Error : Missing -inddllist parameter"
    echo "Error : " ${USAGE}
    exit 1
fi
if [[ ! ${OUTPUTDDLLIST} ]] ; then
    echo "Error : Missing -outddllist parameter"
    echo "Error : " ${USAGE}
    exit 1
fi
if [[ ! ${CTPTRIGGERCLASSES} ]] ; then
    echo "Error : Missing -ctptriggerclasses parameter"
    echo "Error : " ${USAGE}
    exit 1
fi

CONFIG="${BEAMTYPE}-${RUNTYPE}"


# -- Check if Detector exists --
# ------------------------------
#DETECTORDIR=${BASEDIR}/detector/${DETECTOR}
#DETECTORCONFIGDIR=${DETECTORDIR}/.verify

if [ ! -d ${DETECTORDIR} ] ; then
    echo "Error : Detector ${DETECTOR} does not exist."
    exit 2
fi

if [[ ! -d ${DETECTORDIR}/config ]] ; then 
    echo "Error : Directory ${DETECTORDIR}/config does not exist"
    exit 2
fi


#if [ ! -d ${DETECTORCONFIGDIR} ] ; then
#    echo "Error : Config directory for Detector ${DETECTOR} does not exist."
#    exit 2
#fi

# -- Check if Configuration dir exists       --
# -- If generator script exists generate XML --
# -- Check if Configuration File exists      --
# ---------------------------------------------
#CONFIGDIR=${DETECTORDIR}
CONFIGFILE=${DETECTORDIR}/config/${CONFIG}.xml
#GENERATORFILE="${CONFIGDIR}/generate_${DETECTOR}.sh"

if [ ! -f ${CONFIGFILE} ] ; then
    echo "Error : Configuration xml-file ${CONFIGFILE} does not exist."
    exit 2
fi

# -- Check Config ID --
# ---------------------
CONFIGID=`grep \<SimpleChainConfig2 ${CONFIGFILE} | awk -F\" '{print $2} '`

if [ "$CONFIGID" != "$DETECTOR" ] ; then
    echo "Error : ConfigID : ${CONFIGID} does not fit to ${DETECTOR}."
    exit 3
fi

# -- CHECK if module HLT --
# -------------------------
if [ ! -f /etc/profile.modules ] ; then
    echo "Error :  No Modules installed"    
    exit 4
fi

MODULE_HLT=`. /etc/profile.modules; module -t list 2>&1 | grep HLT/ `
    
if [ ! ${MODULE_HLT} ] ; then
    echo "Error : No module HLT has been loaded !"
    exit 4
fi

# -- Create Config --
# -------------------
#echo Createing  ${CONFIG}

pushd ${DETECTORDIR}/config/ >/dev/null

# -- Remove CHECKED File --
# -------------------------
if [ -f ${DETECTORDIR}/config/checked ] ; then
    rm ${DETECTORDIR}/config/checked
fi 

#CONT=`find ${DETECTORDIR} -type f`
#if [ -n "${CONT}" ] ; then
#    rm *
#fi

#RETVAL=`MakeTaskManagerConfig.py -masternode master -taskmandir ${ALIHLT_DC_DIR}  -frameworkdir ${ALIHLT_DC_DIR}  -rundir /tmp/HLT-rundir/${DETECTOR}/${CONFIG}-\`date +%Y%m%d.%H%M%S\` -config ${CONFIGFILE} -basesocket ${BASESOCKET} -usessh ${PRODUCTION} ${EVENTMERGERTIMEOUT} -alicehlt 2>&1` 

chmod a+x `which MakeTaskManagerConfig2.py` 2>/dev/null >/dev/null

RETVAL=`MakeTaskManagerConfig2.py -masternode ${MASTERNODE} -siteconfig ${DETECTORDIR}/setup/siteconfig.xml -rundir /tmp/HLT-rundir/${DETECTOR}/${CONFIG}-\`date +%Y%m%d.%H%M%S\` -config ${CONFIGFILE} -basesocket ${BASESOCKET} -ecsinputlist ${INPUTDDLLIST} -ecsoutputlist "${OUTPUTDDLLIST}" -ctptriggerclasses "${CTPTRIGGERCLASSES}" -nodelist ${DETECTORDIR}/setup/nodelist.xml -ddllist ${DETECTORDIR}/setup/ddllist.xml -output TM -output SCC1 -output GraphViz -quiet 1 2>&1` 

#date >>/tmp/timm/create_config-ECS-debug.out
#pwd >>/tmp/timm/create_config-ECS-debug.out
#echo MakeTaskManagerConfig2.py -masternode ${MASTERNODE} -siteconfig ${DETECTORDIR}/setup/siteconfig.xml -rundir /tmp/HLT-rundir/${DETECTOR}/${CONFIG}-\`date +%Y%m%d.%H%M%S\` -config ${CONFIGFILE} -basesocket ${BASESOCKET} -ecsinputlist ${INPUTDDLLIST} -ecsoutputlist ${OUTPUTDDLLIST} -nodelist ${DETECTORDIR}/setup/nodelist.xml -ddllist ${DETECTORDIR}/setup/ddllist.xml -output TM -output SCC1 -output GraphViz -quiet 1 >>/tmp/timm/create_config-ECS-debug.out

popd >/dev/null

if [ "${RETVAL}" ] ; then
    echo "Error : Creation of Configuration ${CONFIG} failed!"
    echo $RETVAL
    exit 5
fi 

# -- Release Config --
# --------------------
#echo Releaseing ${CONFIG}

#RELEASE=/opt/HLT/control/bin/.releaseControl.sh
RELEASE=${BASEDIR}/bin/.releaseControl.sh

if [[ -x ${RELEASE} ]] ; then
  ${RELEASE} > /dev/null
fi

exit 0
