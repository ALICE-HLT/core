#!/bin/bash
# 
# * Checkout node and ddl list files for HLT from svn repository
#
# File   : edit_setup_file.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.08.2007
#
###################################################################
# Include Defintions
. /opt/HLT/control/bin/system/definitions.sh
###################################################################

# -- DEFAULTS --
# --------------
SPECIALCMD=0
if [[ -z "${EDITOR}" ]] ; then
    EDITOR=xemacs
fi

# -- Check Comand Line Arguments --
# ---------------------------------
if [ ${DETECTOR_CONTROL} ] ; then
    DETECTOR=${DETECTOR_CONTROL}
    USAGE="Usage : $0  -file=<file (siteconfig.xml|nodelist.xml|ddllist.xml)> (-editor=<editor-command>) [--help]"
    SPECIALCMDUSAGE="Usage : $0 (-editor=<editor-command>) [--help]"
else
    USAGE="Usage : $0 -detector=<Detector-ID> -file=<file (siteconfig.xml|nodelist.xml|ddllist.xml)> (-editor=<editor-command>) [--help]"
    SPECIALCMDUSAGE="Usage : $0 -detector=<Detector-ID> (-editor=<editor-command>) [--help]"
fi

for f in ${SUPPORTED_EDIT_FILES} ; do
    if [[ `basename $0 .sh` = "edit_${f}" ]] ; then
        SPECIALCMD=1
        FILE=${f}.xml
    fi
done

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get file to edit
	    -file* )  if [[ "${SPECIALCMD}" = "0" ]] ; then FILE=$arg ; else echo "Error : Argument not known: $cmd"; echo ${SPECIALCMDUSAGE}; exit 1 ; fi ;;

	    # get editor
	    -editor* ) EDITOR=$arg ;;    

	    * ) echo "Error : Argument not known: $cmd"; if [[ "${SPECIALCMD}" = "0" ]] ; then echo ${USAGE}; else echo ${SPECIALCMDUSAGE} ; fi ; exit 1;;
	esac		

    else
	echo "Error : Argument not known: $cmd"
	if [[ "${SPECIALCMD}" = "0" ]] ; then echo ${USAGE}; else echo ${SPECIALCMDUSAGE} ; fi
	exit 1
    fi
done


FOUND=0
for f in ${SUPPORTED_EDIT_FILES} ; do
    if [[ ${FILE} = "${f}.xml" ]] ; then
        FOUND=1
        break
    elif [[ ${FILE} = "${f}" ]] ; then
        FOUND=1
        ${FILE}="${FILE}.xml"
        break
    fi
done

if [[ "${FOUND}" = "0" ]] ; then
    echo "Error : Allowed files: siteconfig.xml globalsiteconfig.xml nodelist.xml ddllist.xml"
    exit 1
fi

if [ "${FILE}" == "ddllist.xml" ] ; then
    DETECTOR=ALICE
fi

# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} || ! ${FILE} ]] ; then
    if [[ "${SPECIALCMD}" = "0" ]] ; then echo "Error : " ${USAGE}; else echo "Error : " ${SPECIALCMDUSAGE} ; fi
    exit 1
fi

# -- Check if file in svn --
# --------------------------
if [[ "${FILE}" == "ddllist.xml" || "${FILE}" == "globalsiteconfig.xml" ]] ; then

    LIST=`svn list ${REPOS}/${REPOSDIR}/|grep -- "setup/"`
    if [ -z "${LIST}" ] ; then
	echo "Setup directory $REPOSDIR/$setup/ does not exist in SVN"
	exit 1
    fi

    LIST=`svn list ${REPOS}/${REPOSDIR}/setup|grep -- "${FILE}"`
    if [ -z "${LIST}" ] ; then
	echo "Configuration file ${REPOSDIR}/setup/${FILE} does not exist in SVN"
	exit 1
    fi

else

    LIST=`svn list ${REPOS}/${REPOSDIR}/|grep -- "${DETECTOR}/"`
    if [ -z "${LIST}" ] ; then
	echo "Detector directory ${REPOSDIR}/${DETECTOR}/ does not exist in SVN"
	exit 1
    fi

    LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/|grep -- "setup/"`
    if [ -z "${LIST}" ] ; then
	echo "Setup directory ${REPOSDIR}/${DETECTOR}/setup does not exist in SVN"
	exit 1
    fi

    LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/setup|grep -- "${FILE}"`
    if [ -z "${LIST}" ] ; then
	echo "Configuration file ${REPOSDIR}/${DETECTOR}/setup/${FILE} does not exist in SVN"
	exit 1
    fi
fi


# -- Create Output Dir --
# -----------------------
if ! OUTPUTDIR=`mktemp -dt -p . \`basename $0 .sh\`.tmpdir.\`date +%Y%m%d.%H%M%S\`.XXXXXX` ; then
  echo "Error : Cannot make temporary directory"
  exit 1
fi

# -- Checkout / Edit / Checkin --
# -------------------------------
if [[ "${FILE}" == "ddllist.xml" || "${FILE}" == "globalsiteconfig.xml" ]] ; then
    svn checkout ${REPOS}/${REPOSDIR}/setup/ ${OUTPUTDIR}/ 2>&1 |grep -Ev "^(A  |U  |Checked out revision|Restored )"
else
    svn checkout ${REPOS}/${REPOSDIR}/${DETECTOR}/setup/ ${OUTPUTDIR}/ 2>&1 |grep -Ev "^(A  |U  |Checked out revision|Restored )"
fi

${EDITOR} ${OUTPUTDIR}/${FILE}

svn commit ${OUTPUTDIR}

rm -rf ${OUTPUTDIR}