#!/bin/bash
# 
# * Create configuration Files for HLT
#
# File   : create_config-ECS.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.08.2007
#
###################################################################
# Include Defintions
. /opt/HLT/control/bin/system/definitions.sh
###################################################################

# -- DEFAULT --
# --------------
CTPTRIGGERCLASSES=""
DETECTORLIST=""
HLTTRIGGERS=""

# enable/disable Creation of config files 
ENABLECREATION=1

# -- Check Comand Line Arguments --
# ---------------------------------
USAGE="Usage : $0 -detector=<Detector-ID> -beamtype=<ALICE beam type (pp|AA)> -runtype=<detector run-type> -detectordir=<Detector-directory> -master=<master-node> -basesocket=<base-socket> -inddllist=<input-ddl-list> -outddllist=<output-ddl-list> (-ctptriggerclasses=<ctp-trigger-classes>) (-detectorlist=<ECS-Detector-List>) (-hlttriggerlist <ECS-HLT-Triggers>) [--help]"

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector directory
	    -detectordir* ) DETECTORDIR=$arg ;;    

	    # get detector list provided by ECS
	    -detectorlist* ) DETECTORLIST=$arg ;;    

	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get beamtype, pp or AA
	    -beamtype* )  BEAMTYPE=$arg ;;

	    # get runtype
	    -runtype* )  RUNTYPE=$arg ;;

	    # get masternode
	    -master* )  MASTERNODE=$arg ;;

	    # get base socket
	    -basesocket* )  BASESOCKET=$arg ;;

	    # get input ddl list
	    -inddllist* )  INPUTDDLLIST=$arg ;;

	    # get output ddl list
	    -outddllist* )  OUTPUTDDLLIST=$arg ;;

	    # get ctp trigger classes
	    -ctptriggerclasses* )  CTPTRIGGERCLASSES="-ctptriggerclasses \"$arg\"" ;;

	    # get HLT trigger list
	    -hlttriggerlist* )  HLTTRIGGERS=$arg ;;

	    * ) echo "Error : Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    else
	echo "Error : Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
    fi
done

# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} ]] ; then
    echo "Error : Missing -detector parameter"
    echo "Error : " ${USAGE}
    exit 1
fi
if [[ ! ${DETECTORDIR} ]] ; then
    echo "Error : Missing -detectordir parameter"
    echo "Error : DETECTORDIR: " ${DETECTORDIR}
    echo "Error : " ${USAGE}
    exit 1
fi
if [[ ! ${MASTERNODE} ]] ; then
    echo "Error : Missing -master parameter"
    echo "Error : " ${USAGE}
    exit 1
fi
if [[ ! ${BEAMTYPE} ]] ; then
    echo "Error : Missing -beamtype parameter"
    echo "Error : " ${USAGE}
    exit 1
fi
if [[ ! ${RUNTYPE} ]] ; then
    echo "Error : Missing -runtype parameter"
    echo "Error : " ${USAGE}
    exit 1
fi
if [[ ! ${BASESOCKET} ]] ; then
    echo "Error : Missing -basesocket parameter"
    echo "Error : " ${USAGE}
    exit 1
fi
if [[ ! ${INPUTDDLLIST} ]] ; then
    echo "Error : Missing -inddllist parameter"
    echo "Error : " ${USAGE}
    exit 1
fi

## BUGFIX -- needed in ECS proxy -- remove later on
if [ "${BEAMTYPE}" = "PP" ] ; then
    BEAMTYPE=pp
fi

CONFIG="${BEAMTYPE}-${RUNTYPE}"

# -- Set/Check common Variables --
# --------------------------------
SET_COMMON_VARS
# --------------------------------

# -- Create Config --
# -------------------
pushd ${DETECTORRUNDIR}/config/ >/dev/null

chmod a+x `which MakeTaskManagerConfig2.py` 2>/dev/null >/dev/null

ADDCONFIGS=""
DETECTORS=`echo "${DETECTORLIST}"|awk -F,'{for (i=1;i<=NF;i++) print "$(i)" ; }'`
for det in "${DETECTORS}" ; do
  if [ -f ${DETECTORRUNCONFIGDIR}/${det}.xml ] ; then
    ADDCONFIGS="${ADDCONFIGS} -config ${DETECTORRUNCONFIGDIR}/${det}.xml"
  fi
done

TRIGGERS=`echo "${HLTTRIGGERS}"|awk -F,'{for (i=1;i<=NF;i++) print "$(i)" ; }'`
for trig in "${TRIGGERS}" ; do
  if [ -f ${DETECTORRUNCONFIGDIR}/${trig}.xml ] ; then
    ADDCONFIGS="${ADDCONFIGS} -config ${DETECTORRUNCONFIGDIR}/${trig}.xml"
  fi
done



RETVAL=`MakeTaskManagerConfig2.py -masternode ${MASTERNODE} -siteconfig ${DETECTORRUNDIR}/setup/${GLOBALSITECONFIG}  -siteconfig ${DETECTORRUNDIR}/setup/${SITECONFIG} -rundir /tmp/HLT-rundir/${DETECTOR}/${CONFIG}-\`date +%Y%m%d.%H%M%S\` -config ${RUNCONFIGFILE} ${ADDCONFIGS} -basesocket ${BASESOCKET} -ecsinputlist ${INPUTDDLLIST} -ecsoutputlist "${OUTPUTDDLLIST}" ${CTPTRIGGERCLASSES} -nodelist ${DETECTORRUNDIR}/setup/${NODELIST} -ddllist ${DETECTORRUNDIR}/setup/${DDLLIST} -output TM -output SCC1 -output GraphViz -quiet 1 2>&1` 

popd >/dev/null

if [ "${RETVAL}" ] ; then
    echo "Error : Creation of Configuration ${CONFIG} failed!"
    echo $RETVAL
    exit 5
fi 


# -- Release Config --
# --------------------

RELEASE=/opt/HLT/tools/bin/release-control.sh

if [ -x ${RELEASE} ] ; then
  ${RELEASE} -detector=${DETECTOR} -quiet > /dev/null
fi

exit 0
