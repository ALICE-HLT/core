#!/bin/bash
# 
# * Archive configuration - result -Files for HLT
#
# File   : archive_config-ECS.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 27.08.2007
#
###################################################################

TMPDIR=`dirname $0`
pushd ${TMPDIR} >/dev/null
cd ..
BASEDIR=`pwd`
popd >/dev/null


# -- DEFAULTS --
# --------------

# enable/disable Archive of results/logs
ENABLEARCHIVE=1
CONFIGDIR=${BASEDIR}/configs
VERBOSE=0


# -- Check Comand Line Arguments --
# ---------------------------------
USAGE="Usage : $0 -detector=<Detector-ID> -mode=<HLT-mode (A|B|C)> -beamtype=<ALICE beam type (pp|AA)> -detectordir=<Detector-directory> -master=<MasterNodeID> [-configdir=<configuration-base-directory>] [-disable-archive] (-verbose) [--help]"

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector directory
	    -detectordir* ) DETECTORDIR=$arg ;;    

	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get HLT mode, either A, B, or C
	    -mode* )  MODE=$arg ;;

	    # get beamtype, pp or AA
	    -beamtype* )  BEAMTYPE=$arg ;;

	    # get configuration base directory
	    -configdir* )  CONFIGDIR=$arg ;;

	    # get master node
	    -master* )  MASTERNODE=$arg ;;

            # disable archiving 
	    -disable-archive ) ENABLEARCHIVE=0 ;;

            # verbosity
	    -verbose ) VERBOSE=1 ;;

	    * ) echo "Error Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    else
	echo "Error Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
    fi
done

# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} || ! ${MODE} || ! ${BEAMTYPE} || ! ${DETECTORDIR} || ! ${MASTERNODE} ]] ; then
    echo "Error: " ${USAGE}
    exit 1
fi

CONFIG="${MODE}-${BEAMTYPE}"

if [[ ! -d ${CONFIGDIR} ]] ; then
    echo "Error: " ${USAGE}
    echo Configuration base directory ${CONFIGDIR} does not exist
    exit 1
fi

if [[ ! -d ${CONFIGDIR}/${DETECTOR} ]] ; then
    echo "Error: " ${USAGE}
    echo Detector configuration directory ${CONFIGDIR}/${DETECTOR} does not exist
    exit 1
fi

if [[ ! -f ${CONFIGDIR}/${DETECTOR}/settings ]] ; then
    echo "Error: " ${USAGE}
    echo Detector configuration file ${CONFIGDIR}/${DETECTOR}/settings does not exist
    exit 1
fi

. ${CONFIGDIR}/${DETECTOR}/settings

# -- Get Variables ( -> have been checked during creation ) --
# ------------------------------------------------------------
#DETECTORDIR=${BASEDIR}/detector/${DETECTOR}
#DETECTORCONFIGDIR=${DETECTORDIR}/.config
#CONFIGDIR=${DETECTORDIR}/config/${CONFIG}
CONFIGFILE=${DETECTORDIR}/config/${DETECTOR}-SCC1-Generate.xml

if [ ! -d ${DETECTORDIR} ] ; then
    echo "Error : Detector ${DETECTOR} does not exist."
    exit 2
fi

CONFIGID=`grep \<SimpleChainConfig1 ${CONFIGFILE} | awk -F\" '{print $2} '`

# -- Get Nodes --
# ---------------
NODES=`grep hostname ${CONFIGFILE} | grep -v master | awk -F\" '{print $4} ' `
#MASTERNODE=`grep master ${CONFIGFILE} | awk -F\" '{print $4}'`

RESULTDIR=${BASEDIR}/results/${DETECTOR}/${CONFIG}-`date +%Y%m%d.%H%M%S`

if [ "${VERBOSE}" == "1" ] ; then
    echo  "--- Waiting before archival ---"
fi
sleep 3


# -- Archive masternode --
# ------------------------
# Config files are always archived 
if [ "${VERBOSE}" == "1" ] ; then
    echo  "--- Archiving node ${MASTERNODE} ---"
fi

if [ ! -d ${RESULTDIR}/master-${MASTERNODE} ] ; then
    mkdir -p ${RESULTDIR}/master-${MASTERNODE}
fi
    
if [ ! -d  /tmp/HLT-rundir-archived/${DETECTOR} ] ; then
    mkdir -p /tmp/HLT-rundir-archived/${DETECTOR}
fi

# Archive base config
tar cjf ${RESULTDIR}/master-${MASTERNODE}/base-config.tar.bz2 ${CONFIGDIR}/${DETECTOR}/ 2>&1 | grep -v "tar: Removing leading ... from member names"

# Archive run config
tar cjf ${RESULTDIR}/master-${MASTERNODE}/run-config.tar.bz2 ${DETECTORDIR}/ 2>&1 | grep -Ev "(tar: Removing leading ... from member names|tar: ${DETECTORDIR}/TaskManager-RunManager.xml-0x[0-9,A-F]{8}\.log: file changed as we read it)"

pushd /tmp/HLT-rundir/${DETECTOR} > /dev/null

DIRS=`ls -d ${CONFIG}-* 2>/dev/null`

for dir in ${DIRS} ; do
    if [ "${ENABLEARCHIVE}" == "0" ] ; then
        if [ "${VERBOSE}" == "1" ] ; then
	    echo Cleaning ${dir} on node `hostname`
        fi
	rm -rf ${dir}
    else
        if [ "${VERBOSE}" == "1" ] ; then
	    echo Packing ${dir} on node `hostname`.
        fi
	tar cjf ${dir}.tar.bz2 ${dir}

        if [ "${VERBOSE}" == "1" ] ; then
	    echo Archiving ${dir} on node `hostname`.
        fi
	cp -a ${dir}.tar.bz2 ${RESULTDIR}/master-${MASTERNODE}
	rm ${dir}.tar.bz2
	mv ${dir} /tmp/HLT-rundir-archived/${DETECTOR}/
        cp ${RESULTDIR}/master-${MASTERNODE}/base-config.tar.bz2 /tmp/HLT-rundir-archived/${DETECTOR}/
        cp ${RESULTDIR}/master-${MASTERNODE}/run-config.tar.bz2 /tmp/HLT-rundir-archived/${DETECTOR}/
	
        if [ "${VERBOSE}" == "1" ] ; then
	    echo Protecting ${RESULTDIR}/master-${MASTERNODE}
        fi
	find ${RESULTDIR}/master-${MASTERNODE} -type f -exec chmod a-w "{}" \;

	chmod -R 777 ${RESULTDIR}/master-${MASTERNODE}
    fi
done

#chmod -R 777 /tmp/HLT-rundir-archived/${DETECTOR}

popd > /dev/null

echo ============= >>/tmp/timm/archive_config-ECS-debug.out
date >>/tmp/timm/archive_config-ECS-debug.out

# -- Archive slavenodes --
# ------------------------
for node in ${NODES} ; do
    if [ "${ENABLEARCHIVE}" == "1" ] ; then
        if [ "${VERBOSE}" == "1" ] ; then
	    echo  "--- Archiving node ${node} ---"
        fi
	if [ ! -d ${RESULTDIR}/${node} ] ; then
	    mkdir -p ${RESULTDIR}/${node}
	fi
	ssh ${node} mkdir -p /tmp/HLT-rundir-archived/${DETECTOR}
    fi

    if [ ! -d /tmp/HLT-rundir/${DETECTOR} ] ; then
	continue
    fi
    echo "${node}" "${MASTERNODE}" >>/tmp/timm/archive_config-ECS-debug.out
    if [ "${node}" = "${MASTERNODE}" ] ; then
        echo continue >>/tmp/timm/archive_config-ECS-debug.out
	continue
    fi

    DIRS=`ssh ${node} find /tmp/HLT-rundir/${DETECTOR}/ -name "${CONFIG}-*" -type d 2> /dev/null`

    for dir in ${DIRS} ; do

	if [ "${ENABLEARCHIVE}"  == "0" ] ; then
            if [ "${VERBOSE}" == "1" ] ; then
	        echo Cleaning ${dir} on node ${node}
            fi
	    ssh ${node} rm -rf ${dir}
	else
            if [ "${VERBOSE}" == "1" ] ; then
	        echo Packing ${dir} on node ${node}.
            fi
	    ssh ${node} tar cjf ${dir}.tar.bz2 ${dir} 2> /dev/null

            if [ "${VERBOSE}" == "1" ] ; then
	        echo Archiving ${dir} on node ${node}.	    
            fi
	    ssh ${node} scp ${dir}.tar.bz2 ms1:${RESULTDIR}/${node}
	    ssh ${node} rm -rf ${dir}.tar.bz2
	    ssh ${node} mv ${dir} /tmp/HLT-rundir-archived/${DETECTOR}/

            if [ "${VERBOSE}" == "1" ] ; then
	        echo Protecting ${RESULTDIR}/${node}
            fi
	    find ${RESULTDIR}/${node} -type f -exec chmod a-w "{}" \;	  

	    chmod -R 777 ${RESULTDIR}/${node}
	fi
  
    done

  
    ssh ${node} chmod -R 777 /tmp/HLT-rundir-archived/${DETECTOR}

    # -- CleanUp slavenodes --
    # ------------------------
    ssh ${node} /usr/local/bin/psi_cleanup -all
    ssh ${node} /opt/HLT/tools/bin/CloseSysVregions.sh   
    echo ssh ${node} kill -9 -1 >>/tmp/timm/archive_config-ECS-debug.out ; sleep 1
    ssh ${node} kill -9 -1
    
done

if [ "${VERBOSE}" == "1" ] ; then
    echo "#################################################"
    echo "  ${CONFIG} archived ..."
    echo "#################################################"
fi
