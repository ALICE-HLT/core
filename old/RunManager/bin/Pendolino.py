#!/usr/bin/env python

import os, sys, string, os.path, re, datetime



def ProcessString( stri, settings ):
    # , includePath, topLevel, incFileName = None
    varList = {}
    for k in settings.keys():
        varList[k] = settings[k][-1]
    temp = string.Template( stri )
    return temp.substitute( varList )

def AddItem( dict, key, item ):
    if not dict.has_key(key):
        dict[key] = []
    dict[key].append( item )

def ReadSettings( settings, intSettings, filename ):
    try:
        infile = open( filename, "r" )
    except IOError:
        raise Exception( "Unable to open input file "+inputfilename )
    line = infile.readline()
    linenr=1
    while line != "":
        if line[-1]=="\n":
            line = line[:-1]
        if line[0]!="#":
            entries = line.split("=")
            if len(entries)<=1:
                raise Exception( "Unknown format on line %d of settings input file %s" % (linenr, filename) )
            if entries[0] in intSettings:
                try:
                    if entries[1][0] == '$':
                        temp = string.Template(entries[1]).substitute(os.environ)
                        value = int(temp,0)
                    else:
                        value = int(entries[1],0)
                except ValueError:
                    raise Exception( "Settings entry %s on line %d of settings input file %s has to be a number" % (entries[0], linenr, filename) )
            else:
                #value = "=".join( entries[1:] )
                temp = string.Template( "=".join( entries[1:] ) )
                value = temp.substitute( os.environ )
            AddItem( settings, entries[0], value )
        line = infile.readline()
        linenr += 1
    infile.close()
    return settings

def CheckSettings( settings, requiredSettings,filename ):
    for key in requiredSettings:
        if not settings.has_key(key):
            raise Exception( "Required settings key '"+key+"' not found in settings file "+filename )
        
        

baseDirectory = os.path.dirname( os.path.dirname( os.path.dirname( os.path.realpath( os.path.expandvars(os.path.expanduser(sys.argv[0] ) ))) ) )

# print "baseDirectory:",baseDirectory

runcontrol=None

partitions=set()

runnumber = None
beamtype = None
runtype = None
detectorlist = None

try:
    if len(sys.argv)>1:
        args = sys.argv[1:]
        while len(args)>0:
            if args[0]=="-basedir":
                if len(args)<=1:
                    raise Exception( "Missing -basedir command line parameter." )
                baseDirectory = args[1]
                args= args[2:]
            elif args[0]=="-runcontroldir":
                if len(args)<=1:
                    raise Exception( "Missing -runcontroldir command line parameter." )
                runcontrol = args[1]
                args= args[2:]
            elif args[0]=="-runnumber":
                if len(args)<=1:
                    raise Exception( "Missing -runnumber command line parameter." )
                try:
                    runnumber = int(args[1])
                except ValueError:
                    raise Exception( "-runnumber parameter is not a decimal number." )
                args= args[2:]
            elif args[0]=="-beamtype":
                if len(args)<=1:
                    raise Exception( "Missing -beamtype command line parameter." )
                beamtype = args[1]
                args= args[2:]
            elif args[0]=="-runtype":
                if len(args)<=1:
                    raise Exception( "Missing -runtype command line parameter." )
                runtype = args[1]
                args= args[2:]
            elif args[0]=="-detectorlist":
                if len(args)<=1:
                    raise Exception( "Missing -detectorlist command line parameter." )
                detectorlist = args[1]
                args= args[2:]
            elif args[0]=="-partition":
                if len(args)<=1:
                    raise Exception( "Missing -partition command line parameter." )
                partitions.add( args[1] )
                args= args[2:]
            else:
                raise Exception( "Unknown command line argument "+args[0] )

    if runnumber==None:
        raise Exception( "Run number has to be specified (via -runnumber parameter)." )
    if beamtype==None:
        raise Exception( "Beam type has to be specified (via -beamtype parameter)." )
    if runtype==None:
        raise Exception( "Run type has to be specified (via -runtype parameter)." )
    if detectorlist==None:
        raise Exception( "Detector list has to be specified (via -detectorlist parameter)." )

except Exception, e:
    print( "usage: "+sys.argv[0]+" -runnumber <Run number> -beamtype <beamtype> -runtype <runtype> -detectorlist <detectorlist> (-basedir <work-directory>) (-runcontroldir <work-directory>)" )
    print e
    sys.exit(1)

if runcontrol!=None:
    runControlDir = runcontrol
else:
    runControlDir = os.path.join(baseDirectory)


configdir = os.path.join( baseDirectory, "configs" )
templatedir = os.path.join( baseDirectory, "templates" )


requiredSettings = [ "RunManagerBasePort", "MasterTaskManagerBasePort", "PendolinoManagerBasePort","TaskManagerDir", "PendolinoCommandPath", "PendolinoCount" ]
intSettings = [ "RunManagerBasePort", "MasterTaskManagerBasePort", "PendolinoManagerBasePort", "Production", "Verbosity", "PendolinoCount" ]
platform = os.uname()[0]+"-"+os.uname()[4]

dirs = os.listdir( configdir  )
if len( dirs)<=0:
    raise Exception( "No detector configurations found in directory "+configdir )

# print "Partitions:",partitions
# print "Dirs:",dirs
detectors = []

for d in dirs:
    if os.path.isdir( os.path.join( configdir, d ) ) and (len(partitions)==0 or (d in partitions)):
        detectors.append(d)

#print "Detectors:",detectors

for part in partitions:
    if not part in detectors:
        # print( "Warning: Partition "+part+" not found in directory "+configdir )
        pass


for det in detectors:
    if not os.path.isfile( os.path.join( configdir, det, "settings" ) ):
        # print( "Warning: settings file not found for detector "+det )
        pass
    
    settings = {
        "BaseDir" : [baseDirectory],
        "RunControlDir" : [runControlDir],
        "Production" : [1],
        "Verbosity" : [0x39],
        "Host" : [os.uname()[1]],
        "MasterTaskManagerHost" : [os.uname()[1]],
        "Platform" : [platform],
        "Archive" : [1],
        "PendolinoLogFile" : [ "/tmp/pendolino-"+det+"-starter.log" ]
        }
    
    settingsFilename = os.path.join( configdir, det, "settings" )
    
    settings = ReadSettings( settings, intSettings, settingsFilename )
    CheckSettings( settings, requiredSettings, settingsFilename )
    if not settings["PendolinoCount"][-1] in [ 1, 2, 3 ]:
        raise Exception( "Only values of 1, 2, or 3 allowed for PendolinoCount" )
    AddItem( settings, "RunNumber", runnumber )
    AddItem( settings, "RunType", runtype )
    AddItem( settings, "BeamType", beamtype )
    AddItem( settings, "DetectorList", detectorlist )

    AddItem( settings, "Detector", det )
    AddItem( settings, "ConfigDir", configdir )

    AddItem( settings, "RunManagerInterruptPort", settings["RunManagerBasePort"][-1]+1 )
    AddItem( settings, "PendolinoManagerInterruptPort", settings["PendolinoManagerBasePort"][-1]+1 )


    if settings["Production"][-1]:
        version = "-release"
    else:
        version = "-debug"
    
    AddItem( settings, "FullVersion", settings["Platform"][-1]+version )

    if settings.has_key("TaskManagerDir"):
        tmBin = os.path.join( settings["TaskManagerDir"][-1], "bin", settings["FullVersion"][-1], "TaskManager" )
    else:
        tmBin = "TaskManager"

    AddItem( settings, "TaskManagerBinary", tmBin )

    rundir = os.path.join( settings["BaseDir"][-1], "run", det )
    AddItem( settings, "RunDir", rundir )

    
    if not os.path.isdir(os.path.dirname(rundir)):
        os.mkdir(os.path.dirname(rundir))
    if not os.path.isdir(rundir):
        os.mkdir(rundir)
    if not os.path.isdir(os.path.join( rundir, "setup" ) ):
        os.mkdir( os.path.join( rundir, "setup" ) )
    try:
        inputfilename = os.path.join( templatedir, "PendolinoManager-template.xml" )
        infile = open( inputfilename, "r" )
    except IOError:
        raise Exception( "Unable to open input file "+inputfilename )
    templatefile = infile.read()
    infile.close()
    output = ProcessString( templatefile, settings )

    try:
        runConfigFile = os.path.join( rundir, "PendolinoManager.xml" )
        outFile = open( runConfigFile, "w" )
    except IOError:
        raise Exception( "Unable to open output file "+runConfigFile )
    outFile.write( output )
    outFile.close()



        
    

    
