#!/bin/bash
# 
# * Test configuration Files for HLT
#
# File   : test_config-ECS.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 27.08.2007
#
###################################################################

TMPDIR=`dirname $0`
pushd ${TMPDIR} >/dev/null
cd ..
BASEDIR=`pwd`
popd >/dev/null

# -- Check Comand Line Arguments --
# ---------------------------------
USAGE="Usage : $0 -detector=<Detector-ID> -mode=<HLT-mode (A|B|C)> -beamtype=<ALICE beam type (pp|AA)> -detectordir=<Detector-directory> [-force-test] [--help]"

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector directory
	    -detectordir* ) DETECTORDIR=$arg ;;    

	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get HLT mode, either A, B, or C
	    -mode* )  MODE=$arg ;;

	    # get beamtype, pp or AA
	    -beamtype* )  BEAMTYPE=$arg ;;

	    # force configuration test
	    -force-test ) FORCETEST=1 ;;

	    * ) echo "Error Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    else
	echo "Error Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
    fi
done

# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} || ! ${MODE} || ! ${BEAMTYPE} || ! ${DETECTORDIR} ]] ; then
    echo "Error: " ${USAGE}
    exit 1
fi

CONFIG="${MODE}-${BEAMTYPE}"


# -- Get Variables ( -> have been checked during creation ) --
# ------------------------------------------------------------
#DETECTORDIR=${BASEDIR}/detector/${DETECTOR}
DETECTORCONFIGDIR=${DETECTORDIR}/config
#CONFIGDIR=${DETECTORDIR}/config/${CONFIG}
CONFIGFILE=${DETECTORCONFIGDIR}/${CONFIG}.xml

if [ ! -d ${DETECTORDIR} ] ; then
    echo "Error : Detector ${DETECTOR} does not exist."
    exit 2
fi

if [[ ! -d ${DETECTORCONFIGDIR} ]] ; then 
    echo "Error : Directory ${DETECTORDIR}/config does not exist"
    exit 2
fi

CONFIGID=`grep \<SimpleChainConfig1 ${CONFIGFILE} | awk -F\" '{print $2} '`



# -- Check if Configuration has to be checked --
# ----------------------------------------------
if [ ! ${FORCETEST} ]; then
    if [ -f ${DETECTORCONFIGDIR}/checked  ] ; then
	echo "Configuration ${CONFIG} already checked."
	exit 0
    fi 
fi

# -- Get Nodes --
# ---------------
NODES=`grep hostname ${CONFIGFILE} | grep -v master | awk -F\" '{print $4} ' `
MASTERNODE=`grep master ${CONFIGFILE} | awk -F\" '{print $4}'`

# -- Prepare Nodes --
# -------------------
for node in ${NODES} ; do
    echo Preparing ${node}
    ssh ${node} /usr/local/bin/psi_cleanup -all
    ssh ${node} /opt/HLT/tools/bin/CloseSysVregions.sh  
#    ssh ${node} `if [ ! -d "/tmp/HLT-rundir/${DETECTOR}" ] ; then  mkdir -p /tmp/HLT-rundir/${DETECTOR};  chmod -R 777 /tmp/HLT-rundir/${DETECTOR}; fi`
done

# -- Test Configuration --
# ------------------------
if [[ `hostname -s` != "${MASTERNODE}" ]] ; then 
    echo "Error : This script must be started on host ${MASTERNODE}" 
    exit 3  
fi

TESTBINARY=${DETECTORCONFIGDIR}/${CONFIGID}-master-${MASTERNODE}-Master-Check.sh

if [ \( ! -x ${TESTBINARY} \) -o \( ! -f ${TESTBINARY} \) ] ; then
    echo "Error : Cannot execute test start script ${TESTBINARY}"
    exit 4
fi

pushd ${DETECTORCONFIGDIR} >/dev/null

${TESTBINARY}

popd >/dev/null


if [ $? -eq 0 ] ; then
    touch ${DETECTORCONFIGDIR}/checked
else
    echo "Error : There have been errors during check of configuration ${CONFIG}!"
    exit 5
fi 
