#!/bin/bash
# 
# * Checkout node and ddl list files for HLT from svn repository
#
# File   : checkout_setupfiles-ECS.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.08.2007
#
###################################################################

TMPDIR=`dirname $0`
pushd ${TMPDIR} >/dev/null
cd ..
BASEDIR=`pwd`
popd >/dev/null

# -- DEFAULTS --
# --------------
OUTPUTDIR="."

# -- Check Comand Line Arguments --
# ---------------------------------
USAGE="Usage : $0 -detector=<Detector-ID> (-outputdir=<Output-Dir>) [--help]"

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get output directory (checkout target)
	    -outputdir* )  OUTPUTDIR=$arg ;;

	    * ) echo "Error : Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    else
	echo "Error : Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
    fi
done

# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} ]] ; then
    echo "Error : " ${USAGE}
    exit 1
fi

REPOS=file://${BASEDIR}/svn-repos
REPOSDIR=Configs/trunk/detector/
NODELIST=nodelist.xml
DDLList=ddllist.xml
SITECONFIG=siteconfig.xml

LIST=`svn list ${REPOS}/${REPOSDIR}/|grep -- "${DETECTOR}/"`
if [ -z "${LIST}" ] ; then
  echo "Detector directory Configs/trunk/detector/${DETECTOR}/ does not exist in SVN"
  exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/|grep -- "setup/"`
if [ -z "${LIST}" ] ; then
  echo "Setup directory Configs/trunk/detector/${DETECTOR}/setup does not exist in SVN"
  exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/setup|grep -- "${NODELIST}"`

if [ -z "${LIST}" ] ; then
  echo "Configuration file Configs/trunk/detector/${DETECTOR}/setup/${NODELIST} does not exist in SVN"
  exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/setup|grep -- "${DDLLIST}"`

if [ -z "${LIST}" ] ; then
  echo "Configuration file Configs/trunk/detector/${DETECTOR}/setup/${DDLLIST} does not exist in SVN"
  exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/setup|grep -- "${SITECONFIG}"`

if [ -z "${LIST}" ] ; then
  echo "Configuration file Configs/trunk/detector/${DETECTOR}/setup/${SITECONFIG} does not exist in SVN"
  exit 1
fi

if [ "${OUTPUTDIR}" != "." ] ; then
  rm -rf ${OUTPUTDIR}/* ${OUTPUTDIR}/.svn
fi

#svn checkout ${REPOS}/${REPOSDIR}/${DETECTOR}/config/${CONFIG}/${CONFIG}.xml ${OUTPUTDIR}/${CONFIG}.xml
svn checkout ${REPOS}/${REPOSDIR}/${DETECTOR}/setup/ ${OUTPUTDIR}/ 2>&1 |grep -Ev "^(A  |U  |Checked out revision|Restored )"
