#!/bin/bash
# 
# * Checkout configuration file for HLT from svn repository
#
# File   : checkout_config.sh
#
# Author : Timm Steinbeck  timm.steinbeck _at_ kip.uni-heidelberg.de ( Initial )
#          Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 25.08.2007
#
###################################################################

TMPDIR=`dirname $0`
pushd ${TMPDIR} >/dev/null
cd ..
BASEDIR=`pwd`
popd >/dev/null

# -- DEFAULTS --
# --------------
OUTPUTDIR="."

# -- Check Comand Line Arguments --
# ---------------------------------
USAGE="Usage : $0 -detector=<Detector-ID> -mode=<HLT-mode (A|B|C)> -beamtype=<ALICE beam type (pp|AA)> (-outputdir=<Output-Dir>) [--help]"

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # print usage
	    --help | -h ) echo ${USAGE}; exit;;
	    
	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get HLT mode, either A, B, or C
	    -mode* )  MODE=$arg ;;

	    # get beamtype, pp or AA
	    -beamtype* )  BEAMTYPE=$arg ;;

	    # get output directory (checkout target)
	    -outputdir* )  OUTPUTDIR=$arg ;;

	    * ) echo "Error : Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    else
	echo "Error : Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
    fi
done

# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} || ! ${MODE} || ! ${BEAMTYPE} ]] ; then
    echo "Error : " ${USAGE}
    exit 1
fi

if [[ ! -d ${OUTPUTDIR}/config ]] ; then 
    mkdir ${OUTPUTDIR}/config
fi

if [[ ! -d ${OUTPUTDIR}/config ]] ; then 
    echo "Error : Unable to create directory ${OUTPUTDIR}/config"
    exit 2
fi

CONFIG="${MODE}-${BEAMTYPE}"

REPOS=file://${BASEDIR}/svn-repos
REPOSDIR=Configs/trunk/detector/

LIST=`svn list ${REPOS}/${REPOSDIR}|grep -- "${DETECTOR}/"`
if [ -z "${LIST}" ] ; then
  echo "Detector directory Configs/trunk/detector/${DETECTOR}/ does not exist in SVN"
  exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/config/|grep -- "${CONFIG}/"`

if [ -z "${LIST}" ] ; then
  echo "Configuration directory Configs/trunk/detector/${DETECTOR}/config/${CONFIG} does not exist in SVN"
  exit 1
fi

LIST=`svn list ${REPOS}/${REPOSDIR}/${DETECTOR}/config/${CONFIG}/|grep -- "${CONFIG}.xml"`

if [ -z "${LIST}" ] ; then
  echo "Configuration file Configs/trunk/detector/${DETECTOR}/config/${CONFIG}/${CONFIG}.xml does not exist in SVN"
  exit 1
fi

rm -rf ${OUTPUTDIR}/config/* ${OUTPUTDIR}/config/.svn


#svn checkout ${REPOS}/${REPOSDIR}/${DETECTOR}/config/${CONFIG}/${CONFIG}.xml ${OUTPUTDIR}/${CONFIG}.xml
svn checkout ${REPOS}/${REPOSDIR}/${DETECTOR}/config/${CONFIG}/ ${OUTPUTDIR}/config/ 2>&1 |grep -Ev "^(A  |U  |Checked out revision|Restored )"

