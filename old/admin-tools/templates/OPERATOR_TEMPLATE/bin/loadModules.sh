#!/bin/bash

## -- Load Modules --

## Uncomment next line to supress the info of loaded HLT modules
export MODULE_INFO_SUPRESS=1

if [ -f /etc/profile.modules ] ; then
    . /etc/profile.modules
     # load actual HLT modules
     module unload HLT
     module unload HLTdevel
     
     ## - load global HLT module
     ## Comment next line and uncomment the following line, if you'd like to
     ## use not the globally updated HLT module. BEAWARE: Then YOU have to 
     ## take care of updateing the according module files.

     #. /opt/HLT/control/releases/setup/.loadModules.sh
     module load HLT/HEAD_2009-06-17-prod

     ## - load private HLT-ANALYSIS module
     ## If you like to use a privat HLT analysis installation, uncomment 
     ## the next line.

     module use --append $HOME/modules-HLT

     ## then enter modules here:

     # module load HLTdevel/HEAD_2008-12-01-debug
fi

