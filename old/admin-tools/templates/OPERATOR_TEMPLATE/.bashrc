#
# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples


# Print node name only if not dumb shell ( important for SCP )
if test "$TERM" != "dumb" ;  then
    UBUNTU=`grep cdrom /etc/apt/sources.list | awk -F_ '{print $2}'`
    if [ -n "${UBUNTU}" ] ; then
	UBUNTU="- ${UBUNTU}"
    fi
    echo "        NODE: `uname -n` - `uname -m` ${UBUNTU}"
fi


if [ -f ~/bin/loadModules.sh ]; then
. ~/bin/loadModules.sh
fi

case "$0" in
          -sh|sh|*/sh)  modules_shell=sh ;;
       -ksh|ksh|*/ksh)  modules_shell=ksh ;;
       -zsh|zsh|*/zsh)  modules_shell=zsh ;;
    -bash|bash|*/bash)  modules_shell=bash ;;
esac
module() { eval `/usr/local/Modules/$MODULE_VERSION/bin/modulecmd $modules_shell $*`; }

if [ -f ${HLTCONTROL_SETUPDIR}/.setbash.sh ] ; then
    . ${HLTCONTROL_SETUPDIR}/.setbash.sh
fi

if [ -f ~/bin/setbash.sh ] ; then
. ~/bin/setbash.sh
fi

# If not running interactively, don't do anything:
[ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
export HISTCONTROL=ignoredups

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(lesspipe)"

# enable color support of ls and also add handy aliases
if [ "$TERM" != "dumb" ]; then
    eval "`dircolors -b`"
fi

alias ls='ls --color=auto'
alias ll='ls -l'

# Comment in the above and uncomment this below for a color prompt
PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u\[\033[00m\]@\[\033[01;35m\]\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD}\007"'
    ;;
*)
    ;;
esac
