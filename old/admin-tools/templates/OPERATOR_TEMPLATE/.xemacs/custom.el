(custom-set-variables
 '(font-lock-mode t nil (font-lock))
 '(line-number-mode t)
 '(overwrite-mode nil))
(custom-set-faces
 '(default ((t (:size "11pt" :family "Fixed"))) t))

(setq minibuffer-max-depth nil)
