#/bin/bash
#
# apply changes to operator account
# after svn update or checkout
#
# Intial Author : Jochen Thaeder
# Date : 2009-01-11
# Filename : applyOperatorTemplate.sh
#
# USAGE : applyOperatorTemplate.sh [--detector=<DETECTOR_NAME>]
#####################################################

DETECTOR_LIST="ALICE TPC TRD PHOS DIMUON ITS DEV TEST EMCAL"
TEMPLATE_DIR=`pwd`/OPERATOR_TEMPLATE

#####################################################

# TOOL : Get Operator
#####################################################
GETOPERATOR() {
# Sets the "OPERATOR" variable
    case "$1" in
	ALICE )  OPERATOR=hlt    ;;
        TPC )    OPERATOR=tpc    ;;
	TRD )    OPERATOR=trd    ;;
	PHOS )   OPERATOR=phos   ;;
	DIMUON ) OPERATOR=dimuon ;;
	ITS )    OPERATOR=its    ;;
	DEV )    OPERATOR=dev    ;;
	TEST )   OPERATOR=test   ;;
        EMCAL )  OPERATOR=emcal  ;;
    esac
}

#####################################################

for cmd in $* ; do
	
    if [[ "$cmd" == --* || "$cmd" == -h ]] ; then
	    
        # Get  option
	arg=`echo $cmd | sed 's|--[-_a-zA-Z0-9]*=||'`
	    
	case "$cmd" in
		
      	    # print usage
	    --help | -h ) echo "USAGE: $0 [--detector=<DETECTOR_NAME>]"; exit 0 ;;

	    # get base path
	    --detector* )   DETECTOR=$arg ;;
	    
	    * ) echo "Error: Argument $cmd not known." ; exit 1;;
	esac
    else
	echo "Error: Argument $cmd not known."
	exit 1
    fi
done

if [ ! -d ${TEMPLATE_DIR} ] ; then
    echo "Error: ${TEMPLATE_DIR} does not exist."
    exit 1
fi

if [ ${DETECTOR} ] ; then
    DETECTOR_LIST=${DETECTOR}
fi

for detector in ${DETECTOR_LIST} ; do 
    GETOPERATOR $detector

    # check folder
    #####################################################
    if [ ! -d /afsuser/${OPERATOR}-operator ] ; then
	echo "Error: ${OPERATOR}-operator does not exist."
	exit 1
    fi

    pushd /afsuser/${OPERATOR}-operator > /dev/null

    # clean home
    #####################################################
    
    if [ -f winstart.bat ] ; then
	rm winstart.bat
    fi

    if [ -f .bashrc_kip1 ] ; then
	rm .bashrc_kip1
    fi

    if [ -f .bashrc ] ; then
	rm .bashrc
    fi

    if [ -h .kde/share/apps/konsole/profiles/HLTKonsole ] ; then 
	rm .kde/share/apps/konsole/profiles/HLTKonsole
    fi

    # create basic directories
    #####################################################

    if [ ! -d src/HLT-analysis ] ; then
	mkdir -p src/HLT-analysis
    fi

    if [ ! -d bin ] ; then
	mkdir -p bin
    fi

    if [[ ! -d HCDB && "${OPERATOR}" != "hlt" ]] ; then
	mkdir -p HCDB
    fi

    if [ ! -d tmp ] ; then
	mkdir -p tmp
    fi
    
    if [ ! -d modules-HLT/HLTdevel ] ; then
	mkdir -p modules-HLT/HLTdevel
    fi    

    if [ ! -d .xemacs ] ; then
	mkdir -p .xemacs
    fi 

    if [ ! -d .kde/share/apps/konsole/profiles ] ; then
	mkdir -p .kde/share/apps/konsole/profiles
    fi

    # create links for run control
    #####################################################

    if [ ! -f "Please do not litter this home" ] ; then
	touch "Please do not litter this home"
    fi
  
    if [ ! -h ./control ] ; then
	ln -s /opt/HLT/control/detector/${detector} control
    fi
    
    # apply template
    #####################################################
    
    pushd ${TEMPLATE_DIR} > /dev/null
    FILES=`find . -type f`
    popd > /dev/null
    
    for f in ${FILES} ; do

	if [ ! -f $f ] ; then
	    cp ${TEMPLATE_DIR}/$f $f
	fi
    done

    # finalize rights
    #####################################################

    chown -R ${OPERATOR}-operator:hlt-operators *

    #####################################################
    
    popd > /dev/null

done