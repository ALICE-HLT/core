#!/bin/bash
# 
# * Compare /etc and installed packages
# 
# File   : compareNodes
# Author : Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 18.07.2008
#
###################################################################

USAGE="$0 --source=<SOURCE-NODE> --target=<SOURCE-NODE> [--missingFiles]"

EXCLUDELIST="./etc/lemon/agent/transport/udp.conf ./etc/lemon/agent/general.conf ./etc/lvm/.cache ./etc/adjtime ./etc/hostname ./etc/iftab ./etc/motd ./etc/popularity-contest.conf ./etc/krb5.keytab ./etc/ld.so.cache ./installed0.packages ./etc/shadow- ./etc/passwd- ./root/.ssh/known_hosts ./root/.bash_history ./root/.viminfo ./usr/local/Modules/src/modules-3.2.5/config.log ./usr/local/Modules/src/modules-3.2.6/config.log ./usr/local/Modules/src/modules-3.2.5/stamp-h ./usr/local/Modules/src/modules-3.2.6/stamp-h ./var/lib/aptitude/pkgstates ./var/lib/aptitude/pkgstates.old ./var/lib/dhcp3/dhclient.eth1.leases ./var/lib/dhcp3/dhclient.leases ./var/lib/dpkg/available ./var/lib/dpkg/available-old ./var/lib/dpkg/status ./var/lib/dpkg/status-old ./var/lib/logrotate/status ./var/lib/ntp/ntp.drift"




for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
        
        # Get  option
        arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
        
        case "$cmd" in

            # print usage
            --help | -h ) echo ${USAGE}; exit;;

            # get detector
            --source* ) SOURCE_NODE=$arg ;;    

            # directory
            --target* ) TARGET_NODE=$arg ;;

            # print missing file
             --missingFiles ) MISSING_FILE=1;;


            * ) echo "Error : Argument not known: $cmd"; echo ${USAGE}; exit 1;;
        esac            

    else
        echo "Error : Argument not known: $cmd"
        echo ${USAGE} 
        exit 1
    fi
done

if [[ ! ${SOURCE_NODE} ||  ! ${TARGET_NODE} ]] ; then
    echo "Error : Not all Arguments known."
    echo ${USAGE} 
    exit 1
fi

# -- CREATE COMPARE_DIRECTORY
COMPAREDIR=$HOME/compare_${SOURCE_NODE}${TARGET_NODE}

if [ ! -d ${COMPAREDIR} ] ; then
    mkdir -p ${COMPAREDIR}
fi

pushd $COMPAREDIR > /dev/null

if [ ! -d ${SOURCE_NODE}/var ] ; then
    mkdir -p ${SOURCE_NODE}
fi

if [ ! -d ${TARGET_NODE}/var ] ; then
    mkdir -p ${TARGET_NODE}
fi

if [ ! -d ${SOURCE_NODE}/usr ] ; then
    mkdir -p ${SOURCE_NODE}
fi

if [ ! -d ${TARGET_NODE}/usr ] ; then
    mkdir -p ${TARGET_NODE}
fi

rsync -avz root@${SOURCE_NODE}:/boot ${SOURCE_NODE} > /dev/null
rsync -avz root@${TARGET_NODE}:/boot ${TARGET_NODE} > /dev/null

rsync -avz root@${SOURCE_NODE}:/etc ${SOURCE_NODE} > /dev/null
rsync -avz root@${TARGET_NODE}:/etc ${TARGET_NODE} > /dev/null

rsync -avz root@${SOURCE_NODE}:/root ${SOURCE_NODE} > /dev/null
rsync -avz root@${TARGET_NODE}:/root ${TARGET_NODE} > /dev/null

rsync -avz root@${SOURCE_NODE}:/usr/local ${SOURCE_NODE}/usr > /dev/null
rsync -avz root@${TARGET_NODE}:/usr/local ${TARGET_NODE}/usr > /dev/null

rsync -avz root@${SOURCE_NODE}:/var/local ${SOURCE_NODE}/var > /dev/null
rsync -avz root@${TARGET_NODE}:/var/local ${TARGET_NODE}/var > /dev/null
rsync -avz root@${SOURCE_NODE}:/var/lib ${SOURCE_NODE}/var > /dev/null
rsync -avz root@${TARGET_NODE}:/var/lib ${TARGET_NODE}/var > /dev/null
rsync -avz root@${SOURCE_NODE}:/var/opt ${SOURCE_NODE}/var > /dev/null
rsync -avz root@${TARGET_NODE}:/var/opt ${TARGET_NODE}/var > /dev/null

ssh root@${SOURCE_NODE} dpkg --get-selections > ${SOURCE_NODE}/installed0.packages
ssh root@${TARGET_NODE} dpkg --get-selections > ${TARGET_NODE}/installed0.packages

cat ${SOURCE_NODE}/installed0.packages | grep -v deinstall > ${SOURCE_NODE}/installed.packages
cat ${TARGET_NODE}/installed0.packages | grep -v deinstall > ${TARGET_NODE}/installed.packages

# compare ROOT
ssh ${SOURCE_NODE} find / -maxdepth 1 | sort > ${SOURCE_NODE}/compareRoot
ssh ${TARGET_NODE} find / -maxdepth 1 | sort > ${TARGET_NODE}/compareRoot

# compare links is /opt and /opt/HLT
ssh ${SOURCE_NODE} 'for i in \"`find /opt -maxdepth 1`\" ; do readlink -e $i ; done' | sort > ${TARGET_NODE}/compareOPT
ssh ${TARGET_NODE} 'for i in \"`find /opt -maxdepth 1`\" ; do readlink -e $i ; done' | sort > ${SOURCE_NODE}/compareOPT

ssh ${SOURCE_NODE} 'for i in \"`find /opt/HLT -maxdepth 1`\" ; do readlink -e $i ; done' | sort > ${TARGET_NODE}/compareOPTHLT
ssh ${TARGET_NODE} 'for i in \"`find /opt/HLT -maxdepth 1`\" ; do readlink -e $i ; done' | sort > ${SOURCE_NODE}/compareOPTHLT

pushd $SOURCE_NODE > /dev/null
SRCS=`find . -type f`
popd > /dev/null

if [ -f ${COMPAREDIR}/compare.log ] ; then
    rm ${COMPAREDIR}/compare.log
fi

for src in ${SRCS} ; do
    
    flag=0
    
    for exc in ${EXCLUDELIST} ; do 
	
	if [ "$src" == "$exc" ] ; then
	    flag=1
	    break
	fi
    done

    if [ $flag -eq 1 ] ; then 
	continue
    fi
    
    vartmp=`echo $src | awk -F/ '{ print $2 }' `
    varflag=0
    
    if [ "$vartmp" == "var" ] ; then
	var=`echo $src | awk -F/ '{ print $3 }' `
	
	for ii in "backups cache db lock mail run spool tmp" ; do 
	    if [ "$ii" == "$var" ] ; then 
		varflag=1
		break
	    fi
	done
	
    fi
    
    if [ $varflag -eq 1 ] ; then 
	continue
    fi

    foo=`echo ${src} | grep defoma`
    if [ "$?" == "0" ] ; then 
	continue
    fi

    foo=`echo ${src} | grep "gconf/defaults"`
    if [ "$?" == "0" ] ; then 
	continue
    fi

    foo=`echo ${src} | grep "var/lib/dpkg/info"`
    if [ "$?" == "0" ] ; then 
	continue
    fi

    foo=`echo ${src} | grep "var/lib/initramfs-tools"`
    if [ "$?" == "0" ] ; then 
	continue
    fi



    res=`diff ${SOURCE_NODE}/${src}  ${TARGET_NODE}/${src}`
    nofile=$?

    if [[ -z "$res" && "$nofile" != "2" ]] ; then
	continue
    fi
    
    if [[ "$MISSING_FILE" != "1" && "$nofile" == "2" ]] ; then
	continue
    fi
    
    echo "=== ${src} ===" >> ${COMPAREDIR}/compare.log
    diff ${SOURCE_NODE}/${src}  ${TARGET_NODE}/${src} >> ${COMPAREDIR}/compare.log
    
    if [ "$nofile" == "2" ] ; then
	echo "!!! No file ${src} at ${TARGETNODE}" >> ${COMPAREDIR}/compare.log	
    fi
    
done


popd > /dev/null