#!/bin/bash
# 
# * Compare /etc and installed packages
# 
# File   : compareNodes
# Author : Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 18.07.2008
#
###################################################################

USAGE="$0 --ddl=<DDL-ID> --bus=<BUS-ID> --slot=<SLOT-ID> --function=<FUNCTION-ID> --diu=<DIU-ID>"

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
        
        # Get  option
        arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
        
        case "$cmd" in

            # print usage
            --help | -h ) echo ${USAGE}; exit;;

            # get DDL ID
            --ddl* ) DDLID=$arg ;;    

            # get DDL ID
            --bus* ) BUSID=$arg ;;    

            # get DDL ID
            --slot* ) SLOTID=$arg ;;    

            # get DDL ID
            --function* ) FUNCTIONID=$arg ;;    

            # get DDL ID
            --diu* ) DIUID=$arg ;;    

            * ) echo "Error : Argument not known: $cmd"; echo ${USAGE}; exit 1;;
        esac            

    else
        echo "Error : Argument not known: $cmd"
        echo ${USAGE} 
        exit 1
    fi
done

if [[ ! ${DDLID} || ! ${BUSID} || ! ${SLOTID} || ! ${FUNCTIONID} || ! ${DIUID} ]] ; then
    echo "Error : Not all Arguments known."
    echo ${USAGE} 
    exit 1
fi

EXITVALUE=0

COMPAREDIR=`mktemp -dt`

pushd $COMPAREDIR > /dev/null

svn export ${HLTSVN_BASEDIR}/control/trunk/detector/setup/ddllist.xml > /dev/null

if [ -f ddllist.xml ] ; then
    DDLLIST=ddllist.xml
else
    echo "Error : ddllist.xml does not exist."
fi

ENTRY=`grep "\"${DDLID}\"" ${DDLLIST}`

popd > /dev/null

rm -rf ${COMPAREDIR}

## -- CHECK entries
BUS=`echo $ENTRY | awk -FBus '{ print $2 }' | awk -F\> '{ print $2}' | awk -F\< '{ print $1}'`
SLOT=`echo $ENTRY | awk -FSlot '{ print $2 }' | awk -F\> '{ print $2}' | awk -F\< '{ print $1}'`
FUNCTION=`echo $ENTRY | awk -FFunction '{ print $2 }' | awk -F\> '{ print $2}' | awk -F\< '{ print $1}'`
DIU=`echo $ENTRY | awk -FDIU '{ print $2 }' | awk -F\> '{ print $2}' | awk -F\< '{ print $1}'`    

if [[ "${BUS}" != "${BUSID}" || "${SLOT}" != "${SLOTID}" || "${FUNCTION}" != "${FUNCTIONID}" || "${DIU}" != "${DIUID}" ]] ; then 
    EXITVALUE=1
fi

if [ ${EXITVALUE} ] ; then
    echo "=========================================="
    echo "Error: DB and DDL list differ for ${DDLID}"
    echo "       DB ENTRY $DDLID $BUSID $SLOTID $FUNCTIONID $DIUID"
    echo "       DDLLIST  $DDLID $BUS $SLOT $FUNCTION $DIU"
    echo "=========================================="
fi

exit ${EXITVALUE}