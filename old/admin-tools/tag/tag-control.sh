#!/bin/bash
#
# tags a new release for the run 
#
# Intial Author : Jochen Thaeder
# Date : 2009-01-10
# Filename : tag-control.sh
#
# USAGE : tag-control.sh [--version=<VERSION_NUMBER>]
#####################################################

SVNDIR=file:///afs/.alihlt.cern.ch/repository/svn
RELEASE_LIST="bin configs operator.bin statsGUI templates"

#####################################################
for cmd in $* ; do
	
    if [[ "$cmd" == --* || "$cmd" == -h ]] ; then
	    
        # Get  option
	arg=`echo $cmd | sed 's|--[-_a-zA-Z0-9]*=||'`
	    
	case "$cmd" in
		
      	    # print usage
	    --help | -h ) echo "USAGE: $0 [--version=<VERSION_NUMBER>]"; exit 0 ;;

	    # get base path
	    --version* )   VERSION=$arg ;;
	    
	    * ) echo "Error: Argument $cmd not known." ; exit 1;;
	esac
    else
	echo "Error: Argument $cmd not known."
	exit 1
    fi
done

# CHECK argument
#####################################################
if [ ! ${VERSION} ] ; then
    echo "Error:  No version specified."
    exit 1
fi

# CREATE tag dir
#####################################################
TAGDIR=${SVNDIR}/control/tags/release-v${VERSION}

svn ls $TAGDIR 2> /dev/null

if [ $? -ne 1 ] ; then
    echo "Error: tag already exists."
    exit 1
fi

svn mkdir ${TAGDIR} -m "Tag release-v${VERSION}"

# tag
#####################################################

for entry in ${RELEASE_LIST} ; do 
    svn cp ${SVNDIR}/control/trunk/${entry} ${TAGDIR} -q -m "Tag release-v${VERSION}"
done