#!/bin/bash

#####################################################
# Updates the HLT manuals
#
# Auhtor: Oystein S. Haaland
# Initially created: 2008-07-26
# Filename : update-manuals.sh
#
# USAGE : update-manuals.sh [--basedir=<DEST_DIR>]
#####################################################

SCRIPTDIR=`dirname $0`

. $SCRIPTDIR/update-functions.sh

BASEDIR=/afs/.alihlt.cern.ch/www/doc/manuals

if [[ $# > 0 ]] ; then
    if [[ $# == 2 && $1 == "--basedir" ]]
    then
        BASEDIR=$2
    elif [ "$1" == "-h" ] || [ "$1" == "--help" ]
    then
        echo "Usage:"
        echo "update-manuals.sh [--basdir /path/to/base]"
        echo "update-manuals.sh [--help | -h]"
        exit 0

    else
        echo "Wrong parameters. Try: update-manuals.sh --help"
        exit 0
    fi
fi

TMP_BUILD_INSTALL hlt-operator-manual $BASEDIR "pdf,html"
TMP_BUILD_INSTALL hlt-admin-manual $BASEDIR "pdf,html"
TMP_BUILD_INSTALL hlt-developer-manual $BASEDIR "pdf,html"

