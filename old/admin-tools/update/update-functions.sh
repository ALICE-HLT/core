#!/bin/bash

#####################################################
# Helper functions for the update tools.
#
# Auhtor: Oystein S. Haaland
# Initially created: 2008-07-26
# Filename : update-functions.sh
#
#####################################################

GITBASEURL=ssh://dev0/afs/.alihlt.cern.ch/repository/git
TMPBUILDDIR=/tmp/build

# Function that checks out or updates a git project.
# Arguments:
#   1. PROJECT - name of project
#   2. GITSRCDIR - the directory that will hold the projects git sources
function GIT_CHECKOUT {
    PROJECT=$1
    GITSRCDIR=$2
    
    pushd ${GITSRCDIR} > /dev/null
    if [ ! -d ${PROJECT} ] ; then
        echo "Checking out repository: ${GITBASEURL}/${PROJECT}"
        git clone ${GITBASEURL}/${PROJECT}
    else
        pushd ${GITSRCDIR}/${PROJECT} > /dev/null
        echo "Updating repository: ${GITBASEURL}/${PROJECT}"
        git pull
        popd > /dev/null
    fi
    popd > /dev/null
}

# Function that uses cmake to configure and install from a temporary build directory
#   1. PROJECT - project to be built
#   2. GITSRCDIR - source dir to find project
#   3. INSTALLDIR - path where project should be installed
#   4. TARGETS - The cmake targets that should be built
#       * TIP: To list more than one target, use: "target1,target2"
function CMAKE_INSTALL {
    PROJECT=$1
    GITSRCDIR=$2
    INSTALLDIR=$3
    TARGETS=$4
    
    mkdir ${GITSRCDIR}/build
    pushd ${GITSRCDIR}/build > /dev/null
    echo "Compiling ${PROJECT}"
    cmake ${GITSRCDIR}/${PROJECT}
    # Replace all "," with " "
    make ${TARGETS//,/ }
    echo make ${TARGETS//,/ }
    echo "Installing ${PROJECT}"
    
    make install DESTDIR=${INSTALLDIR}
    popd > /dev/null
    rm -fr ${GITSRCDIR}/build
}

# Function that uses cmake to configure and install from a temporary build directory
# Uses -DCMAKE_INSTALL_PREFIX insead of the default DESTDIR
#   1. PROJECT - project to be built
#   2. GITSRCDIR - source dir to find project
#   3. INSTALLDIR - path where project should be installed
#   4. TARGETS - The cmake targets that should be built
#       * TIP: To list more than one target, use: "target1,target2"
function CMAKE_PREFIX_INSTALL {
    PROJECT=$1
    GITSRCDIR=$2
    INSTALLDIR=$3
    TARGETS=$4
    
    mkdir ${GITSRCDIR}/build
    pushd ${GITSRCDIR}/build > /dev/null
    echo "Compiling ${PROJECT}"
    cmake -DCMAKE_INSTALL_PREFIX=${INSTALLDIR} ${GITSRCDIR}/${PROJECT}
    # Replace all "," with " "
    make ${TARGETS//,/ }
    echo make ${TARGETS//,/ }
    echo "Installing ${PROJECT}"
    
    make install
    popd > /dev/null
    rm -fr ${GITSRCDIR}/build
}

# Builds a projcet in the /tmp/build directory and then deletes it
# after the project has been installed
#   1. PROJECT - project to be built
#   2. INSTALLDIR - path where project should be installed
#   3. TARGETS - The cmake targets that should be built
#       * TIP: To list more than one target, use: "target1,target2"
function TMP_BUILD_INSTALL {
    PROJECT=$1
    INSTALLDIR=$2
    TARGETS=$3
    
    if [ ! -d ${TMPBUILDDIR} ]
    then
        mkdir ${TMPBUILDDIR}
    fi
    
    GIT_CHECKOUT ${PROJECT} ${TMPBUILDDIR}
    CMAKE_INSTALL $PROJECT ${TMPBUILDDIR} ${INSTALLDIR} ${TARGETS}
    
    rm -fr $TMPBUILDDIR/$PROJECT
}

