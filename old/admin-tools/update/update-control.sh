#!/bin/bash
#
# creates all directories of control
# after svn update or checkout
#
# Intial Author : Jochen Thaeder
# Date : 2008-06-06
# Filename : update-control.sh
#
# USAGE : update-control.sh --version=<RELEASE VERSION> [--basedir=<PATH TO CONTROL>]
#####################################################

BASEDIR=/opt/HLT/control
SVNDIR=file:///afs/.alihlt.cern.ch/repository/svn
DETECTOR_LIST="ALICE TPC TRD PHOS DIMUON ITS DEV TEST"

#####################################################

for cmd in $* ; do
	
    if [[ "$cmd" == --* || "$cmd" == -h ]] ; then
	    
        # Get  option
	arg=`echo $cmd | sed 's|--[-_a-zA-Z0-9]*=||'`
	    
	case "$cmd" in
		
      	    # print usage
	    --help | -h ) echo "USAGE: $0 --version=<RELEASE VERSION> [--basedir=<PATH TO CONTROL>]"; exit 0 ;;

	    # get base path
	    --basedir* )   BASEDIR=$arg ;;

	    --version* )   VERSION=$arg ;;
	    
	    * ) echo "Error: Argument $cmd not known." ; exit 1;;
	esac
    else
	echo "Error: Argument $cmd not known."
	exit 1
    fi
done

# CHECK AFS ACL's
#####################################################
if [ ! -d ${BASEDIR} ] ; then
    mkdir -p ${BASEDIR}
fi

fs sa ${BASEDIR} -clear -a system:administrators all  \
    hlt-admin write \
    hlt-operators read \
    system:authuser read \

# TOOL : Get Operator
#####################################################
GETOPERATOR() {
# Sets the "OPERATOR" variable
    case "$1" in
	ALICE )  OPERATOR=hlt    ;;
        TPC )    OPERATOR=tpc    ;;
	TRD )    OPERATOR=trd    ;;
	PHOS )   OPERATOR=phos   ;;
	DIMUON ) OPERATOR=dimuon ;;
	ITS )    OPERATOR=its    ;;
	DEV )    OPERATOR=dev    ;;
	TEST )   OPERATOR=test   ;;
    esac
}

# INSTALL release
#####################################################

if [ ${VERSION} ] ; then 

    RELEASE=release-v${VERSION}
    RELEASEDIR=${BASEDIR}/releases/${RELEASE}

    if [ ! -d ${BASEDIR}/releases ] ; then 
	mkdir -p ${BASEDIR}/releases
    fi
    
    if [ -d ${RELEASEDIR} ]  ; then
	echo "Error: Release has already been installed."
	exit 1
    fi
    
    FLAG=`svn list ${SVNDIR}/control/tags | grep -- "${RELEASE}"`
    if [ -z "${FLAG}" ] ; then
	echo "Release ${RELEASE} does not exist in SVN"
	exit 1
    fi

    svn -q co ${SVNDIR}/control/tags/${RELEASE} ${BASEDIR}/releases/${RELEASE}/
    
    #---------------------------------------------------
    # -- operator.bin
    
    pushd ${RELEASEDIR}/operator.bin > /dev/null
    
    fs sa .  -clear -a system:administrators all  \
	hlt-admin write \
	hlt-operators read \
	hlt-user read

    # checkout - operator.bin of restricted
    if [ -d .svn ] ; then
	rm -rf .svn
    fi
	
    svn -q co ${SVNDIR}/restricted/control/operator.bin/ .

    for d in `find -name ".*operator" -type d` ; do 
	fs sa $d  -a hlt-operators rlk hlt-user rlk hlt-admin rlk 
    done

    popd > /dev/null

    #---------------------------------------------------
    # -- statsGUI

    pushd ${RELEASEDIR}/statsGUI > /dev/null
#    qmake
#    make -j 8
    popd > /dev/null

    #---------------------------------------------------
    # -- run

    pushd ${RELEASEDIR} > /dev/null
    ln -s ../../run
    popd > /dev/null

    #---------------------------------------------------
    # -- operator setup

    pushd ${HLTCONTROL_BASEDIR} > /dev/null
    if [ -h setup ] ; then
	rm setup
    fi
    
    ln -s ${RELEASE}/operator.bin/setup 

    popd > /dev/null

fi

# update common directories
#####################################################

#---------------------------------------------------
# -- run

if [ ! -d ${BASEDIR}/run ] ; then
    fs mkm ${BASEDIR}/run cont.detector
fi

fs sa ${BASEDIR}/run -clear -a system:administrators all  \
    hlt-admin write \
    hlt-operators read \
    system:authuser read \

for d in ${DETECTOR_LIST} ; do 
    GETOPERATOR $d

    if [ ! -d ${BASEDIR}/run/$d ] ; then
	fs mkm ${BASEDIR}/run/$d cont.det.$d
    fi

    fs sa ${BASEDIR}/run/$d -a ${OPERATOR}-operator write

    if [ ! -d ${BASEDIR}/run/$d/.verify ] ; then
	mkdir -p ${BASEDIR}/run/$d/.verify
    fi
    
    if [ ! -d ${BASEDIR}/run/$d/config ] ; then
	mkdir -p ${BASEDIR}/run/$d/config
    fi
    
    if [ ! -d ${BASEDIR}/run/$d/setup ] ; then
	mkdir -p ${BASEDIR}/run/$d/setup
    fi

    chown -R  ${OPERATOR}-operator:hlt-operators ${BASEDIR}/run/$d
    chmod -R 755 ${BASEDIR}/run/$d
done

#---------------------------------------------------
# -- results

if [ ! -d ${BASEDIR}/results ] ; then
    fs mkm ${BASEDIR}/results cont.results
fi

for d in ${DETECTOR_LIST} ; do 
    GETOPERATOR $d

    if [ ! -d ${BASEDIR}/results/$d ] ; then
	mkdir -p ${BASEDIR}/results/$d
	
	chown -R ${OPERATOR}-operator:hlt-operators ${BASEDIR}/results/$d
	chmod -R 755 ${BASEDIR}/results/$d
	
	fs sa ${BASEDIR}/results/$d -a ${OPERATOR}-operator write
    fi

done

#---------------------------------------------------
# -- detector

if [ ! -d ${BASEDIR}/detector ] ; then
    mkdir -p ${BASEDIR}/detector
fi

for d in ${DETECTOR_LIST} ; do 
    GETOPERATOR $d

    ## - create FOLDER
    if [ ! -d ${BASEDIR}/detector/$d ] ; then
	mkdir -p ${BASEDIR}/detector/$d
    fi  

    fs sa ${BASEDIR}/detector/$d -a ${OPERATOR}-operator write

    chown -R ${OPERATOR}-operator:hlt-operators ${BASEDIR}/detector/$d
    chmod -R 755 ${BASEDIR}/detector/$d

    pushd ${BASEDIR}/detector/$d/ > /dev/null
    
    ## - create link to run
    if [ ! -h run ] ; then
	ln -s ../../run/$d run
    fi

    ## - checkout / update config
    if [ ! -d config ] ; then
	svn -q co ${SVNDIR}/control/trunk/detector/$d/config
    fi
    
    pushd ${BASEDIR}/detector/$d/config > /dev/null

    svn -q up
    find -name "pp-*" -type d -exec rm -rf "{}" +
    find -name "AA-*" -type d -exec rm -rf "{}" +

    popd > /dev/null

    ## - checkout / update setup
    if [ ! -d setup ] ; then
	svn -q co ${SVNDIR}/control/trunk/detector/$d/setup
    fi
	
    pushd ${BASEDIR}/detector/$d/setup > /dev/null

    svn -q up
    find -name "*.xml" -type f -exec rm -rf "{}" +

    popd > /dev/null

    popd > /dev/null

done

/opt/HLT/tools/bin/release-control.sh -all



