#!/bin/bash

#####################################################
# Updates the HLT manuals
#
# Auhtor: Oystein S. Haaland
# Initially created: 2008-07-26
# Filename : update-manuals.sh
#
# USAGE : update-manuals.sh [--basedir=<DEST_DIR>]
#####################################################

SCRIPTDIR=`dirname $0`

. $SCRIPTDIR/update-functions.sh

EPYDOCDIR=/afs/.alihlt.cern.ch/www/doc/epydoc

[ ! -f ${TMPBUILDDIR} ] && mkdir ${TMPBUILDDIR}

[ -f ${TMPBUILDDIR}/clusterapi ] && rm ${TMPBUILDDIR}/clusterapi

GIT_CHECKOUT clusterapi ${TMPBUILDDIR}
pushd ${TMPBUILDDIR}/clusterapi > /dev/null
epydoc --config epydoc -o ${EPYDOCDIR}/clusterapi
popd > /dev/null
rm -fr ${TMPBUILDDIR}/clusterapi
