#!/bin/bash
#
# creates bin, include, lib dirctories of "tools"
# after svn update or checkout
#
# Intial Author : Jochen Thaeder
# Date : 2008-04-19
# Filename : update-tools.sh
#
# USAGE : update-tools.sh [--basedir=<PATH TO TOOLS>]
#####################################################

SCRIPTDIR=`dirname $0`

. $SCRIPTDIR/update-functions.sh

BASEDIR=/opt/HLT/tools
SVNDIR=file:///afs/.alihlt.cern.ch/repository/svn   # {HLT_SVNBASEDIR}
SVNURL=svn+ssh://dev0/afs/.alihlt.cern.ch/repository/svn  # Alternative svn url

SVN_RESTRICTED=svn+ssh://dev0/afs/.alihlt.cern.ch/repository/restricted

for cmd in $* ; do
	
    if [[ "$cmd" == --* || "$cmd" == -h ]] ; then
	    
        # Get  option
	arg=`echo $cmd | sed 's|--[-_a-zA-Z0-9]*=||'`
	    
	case "$cmd" in
		
      	    # print usage
	    --help | -h ) echo "USAGE: $0 [--basedir=<PATH TO TOOLS>]"; exit 0 ;;
        
        --allow-new-svn | -a )  ALLOW_NEW_SVN=1 ;;

	    # get base path
	    --basedir* )    BASEDIR=$arg ;;
	    
	    * ) echo "Error: Argument $cmd not known.";;
	esac
    else
	echo "Error: Argument $cmd not known."
    fi
done

BINDIR=${BASEDIR}/bin
SRCDIR=${BASEDIR}/src
INCDIR=${BASEDIR}/include
LIBDIR=${BASEDIR}/lib
GITDIR=${BASEDIR}/git

PYLIBDIR=${BASEDIR}/lib/python

# CHECK AFS ACL's
#####################################################
if [ ! -d ${BASEDIR} ] ; then
    mkdir -p ${BASEDIR}
fi

fs sa ${BASEDIR} -clear -a system:administrators all  \
    hlt-admin write system:authuser rlk

# Create Directories
#####################################################
if [ ! -d ${BINDIR} ] ; then
    mkdir -p ${BINDIR}
fi

if [ ! -d ${LIBDIR} ] ; then
    mkdir -p ${LIBDIR}
fi

if [ ! -d ${INCDIR} ] ; then
    mkdir -p ${INCDIR}
fi

if [ ! -d ${GITDIR} ] ; then
    mkdir -p ${GITDIR}
fi

if [ ! -d ${PYLIBDIR} ] ; then
    mkdir -p ${PYLIBDIR}
fi

# TOOLS : SVN UPDATE or CHECKOUT
#####################################################
pushd ${BASEDIR} > /dev/null
if [ ! -d .svn ] ; then
    svn co ${SVNDIR}/tools .
else
    svn update
fi
popd > /dev/null

# RESTRICTED : SVN UPDATE or CHECKOUT
#####################################################

# afs-tools
if [ ! -d ${SRCDIR}/afs-tools ] ; then
    mkdir -p ${SRCDIR}/afs-tools

    fs sa ${SRCDIR}/afs-tools -clear -a system:administrators all \
	hlt-admin write hlt-operators rlk

    svn co ${SVNDIR}/restricted/tools/src/afs-tools/ ${SRCDIR}/afs-tools/
else
    pushd ${SRCDIR}/afs-tools > /dev/null
    svn update
    popd > /dev/null
fi

# operator-tools
if [ ! -d ${SRCDIR}/operator-tools ] ; then
    mkdir -p ${SRCDIR}/operator-tools

    fs sa ${SRCDIR}/operator-tools -clear -a system:administrators all \
	hlt-admin write system:authuser rlk

    svn co ${SVNDIR}/restricted/tools/src/operator-tools/ ${SRCDIR}/operator-tools/
else
    pushd ${SRCDIR}/operator-tools > /dev/null
    svn update
    popd > /dev/null
fi


pushd ${SRCDIR}/afs-tools > /dev/null
if [ ! -e .krb5afs.keytab ] ; then
    svn export ${SVN_RESTRICTED}/.krb5afs.keytab
fi
popd > /dev/null

######################################################
# clusterapi and InfoLoggerLogServer
######################################################
#GIT_CHECKOUT clusterapi $GITDIR
#CMAKE_PREFIX_INSTALL clusterapi $GITDIR $BASEDIR

GIT_CHECKOUT InfoLoggerLogServer $GITDIR
CMAKE_PREFIX_INSTALL InfoLoggerLogServer $GITDIR $BASEDIR

# function to create links
#####################################################
CREATE_LINK() {

    FILENAME=$1

    if [ "$3" ] ; then
	BASENAME=$3
    else
	BASENAME=`basename ${FILENAME}`
    fi
    
    case "$2" in 
	lib ) LINKDIR=${LIBDIR} ;;
	include ) LINKDIR=${INCDIR} ;;
	bin ) LINKDIR=${BINDIR} ;;
	* ) LINKDIR=${BINDIR} ;;
    esac


    pushd ${LINKDIR} > /dev/null

    if [[ ! -h ${BASENAME} || ! -f ${BASENAME} ]] ; then
	ln -s $FILENAME $BASENAME
    fi
    
    popd > /dev/null
}

# Check all src folders
#####################################################

# -- update packages
CREATE_LINK ${SRCDIR}/update_packages/update-packages.sh 

# -- data-tools
FILES=`find ${SRCDIR}/data-tools -name "*.sh"`
for f in $FILES ; do  CREATE_LINK $f; done

# -- TM-tools
FILES=`find ${SRCDIR}/TM-tools -name "*.sh"`
for f in $FILES ; do  CREATE_LINK $f; done

# -- create-ddls
FILES=`find ${SRCDIR}/create-ddls -name "*.sh"`
for f in $FILES ; do  CREATE_LINK $f; done

# -- afs-tools
FILES=`find ${SRCDIR}/afs-tools -name "release*.sh"`
for f in $FILES ; do  CREATE_LINK $f; done

# -- BeOperator
source ${SRCDIR}/operator-tools/definitions.sh
for f in $SUPPORTED_OPERATORS ; do CREATE_LINK ${SRCDIR}/operator-tools/.BeOperator bin Be$f-Operator; done

# -- event-replay
for file in $( echo evtreplay rorc_control rorc_status hrorc_handler.py )
do
    CREATE_LINK ${SRCDIR}/HRORC-tools/${file}
done 

