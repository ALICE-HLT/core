
#include <HOMERReader.h>
#include <vector>
#include <string>
#include <sys/ipc.h>
#include <errno.h>

int main( int argc, char** argv )
    {
    // Settings, defined by command line parameters
    std::vector<const char*> hostnames;
    std::vector<unsigned short> ports;
    std::vector<key_t> shmKeys;
    std::vector<int> shmSizes;
    unsigned eventCnt=1;
    bool showSources=false;
    std::vector<unsigned> blockList;
    bool eventID = false;
    bool dataSize = true;


    // Parse and check command line parameters
    int i=1;
    char* error=NULL;
    int errorArg=-1;
    int errorParam=-1;
    char* cpErr=NULL;
    const char* usage="(-shm <shmkey> <shmsize>) (-tcp <hostname> <portnr>) (-showsources) (-block <blocknr>) (-eventid) (-nodatasize) (-full-binary-out)";
    while ( i < argc )
	{
	if ( !strcmp( argv[i], "-shm" ) )
	    {
	    if ( i+2>=argc )
		{
		error = "Missing arguments for -shm";
		errorArg=i;
		break;
		}
	    shmKeys.push_back( (key_t)strtoul( argv[i+1], &cpErr, 0 ) );
	    if ( *cpErr != '\0' )
		{
		error = "Error converting shared memory key";
		errorArg=i;
		errorParam=i+1;
		break;
		}
	    int tmp;
	    unsigned mult=1;
	    tmp=(int)strtoul( argv[i+2], &cpErr, 0 );
	    if ( *cpErr == 'k' )
		mult=1024;
	    else if ( *cpErr == 'M' )
		mult=1024*1024;
	    else if ( *cpErr == 'G' )
		mult=1024*1024*1024;
	    else if ( *cpErr != '\0' )
		{
		error = "Error converting shared memory size";
		errorArg=i;
		errorParam=i+2;
		break;
		}
	    tmp *= mult;
	    shmSizes.push_back( tmp );
	    i += 3;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-tcp" ) )
	    {
	    if ( i+2>=argc )
		{
		error = "Missing arguments for -tcp";
		errorArg=i;
		break;
		}
	    hostnames.push_back( argv[i+1] );
	    ports.push_back( (unsigned short)strtoul( argv[i+2], &cpErr, 0 ) );
	    if ( *cpErr != '\0' )
		{
		error = "Error converting port number";
		errorArg=i;
		errorParam=i+2;
		break;
		}
	    i += 3;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-showsources" ) )
	    {
	    showSources=true;
	    i++;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-block" ) )
	    {
	    if ( i+1>=argc )
		{
		error = "Missing argument for -block";
		errorArg=i;
		break;
		}
	    blockList.push_back( (unsigned)strtoul( argv[i+1], &cpErr, 0 ) );
	    if ( *cpErr != '\0' )
		{
		error = "Error converting block number";
		errorArg=i;
		errorParam=i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-eventid" ) )
	    {
	    eventID = true;
	    i++;
	    continue;
	    }
	else
	    {
	    error = "Unknown argument";
	    errorArg=i;
	    break;
	    }
	}
    if ( !error )
	{
	if ( hostnames.size()<=0 && shmKeys.size()<=0 )
	    error = "At least on data source has to be specified, e.g. with -shm or -tcp";
	}

    if ( error )
	{
	fprintf( stderr, "Usage: %s %s\n", argv[0], usage );
	fprintf( stderr, "Error: %s.\n", error );
	if ( errorArg>=0 )
	    fprintf( stderr, "       Offending argument: %s\n", argv[errorArg] );
	if ( errorParam>=0 )
	    fprintf( stderr, "       Offending parameter: %s\n", argv[errorParam] );
	return -1;
	}

    // Show sources in internally used order
    if ( showSources )
	{
	for ( unsigned n=0; n < hostnames.size(); n++ )
	    printf( "tcp:%s:%hu\n", hostnames[n], ports[n] );
	for ( unsigned n=0; n < shmKeys.size(); n++ )
	    printf( "shm:%d:%d\n", (int)shmKeys[n], shmSizes[n] );
	return 0;
	}

    // Create reader object
    HOMERReader reader( (unsigned)hostnames.size(), &(*hostnames.begin()), &(*ports.begin()),
			(unsigned)shmKeys.size(), &(*shmKeys.begin()), &(*shmSizes.begin()) );

    // Check connection to reader sources
    int ret=reader.GetConnectionStatus();
    if ( ret )
	{
	unsigned ndx = reader.GetErrorConnectionNdx();
	if ( ndx < hostnames.size() )
	    {
	    fprintf( stderr, "Error establishing connection to TCP source %s:%hu: %s (%d)\n", 
		     hostnames[ndx], ports[ndx], 
		     strerror(ret), ret );
	    }
	else
	    {
	    fprintf( stderr, "Error establishing connection to Shm source %d:%d: %s (%d)\n", 
		     (int)shmKeys[ndx], shmSizes[ndx],
		     strerror(ret), ret );
	    }
	return ret;
	}

    // Start event loop
    unsigned n=0;
    while ( n<eventCnt )
	{
	// Read event
	ret = reader.ReadNextEvent();
	if ( ret )
	    {
	    // Error upon reading?
	    unsigned ndx = reader.GetErrorConnectionNdx();
	    if ( ndx < hostnames.size() )
		{
		fprintf( stderr, "Error reading data from TCP source %s:%hu: %s (%d)\n", 
			 hostnames[ndx], ports[ndx], 
			 strerror(ret), ret );
		}
	    else
		{
		fprintf( stderr, "Error reading data from Shm source %d:%d: %s (%d)\n", 
			 (int)shmKeys[ndx], shmSizes[ndx],
			 strerror(ret), ret );
		}
	    return ret;
	    }
	// Dump blocks to stdout
	unsigned long blockCnt = reader.GetBlockCnt();
	if ( blockList.size()<=0 )
	    {
	    // Either all blocks ...
	    for ( unsigned long ndx = 0; ndx < blockCnt; ndx++ )
		{
		ret=fwrite( reader.GetBlockData( ndx ), 1, reader.GetBlockDataLength( ndx ), stdout );
		if ( ret != (int)reader.GetBlockDataLength( ndx ) )
		    {
		    ret=errno;
		    fprintf( stderr, "Error writing block %lu to output: %s (%d)\n",
			     ndx, strerror(ret), ret );
		    return ret;
		    }
		}
	    }
	else
	    {
	    // ... or the selected list of blocks
	    std::vector<unsigned>::iterator iter, end;
	    iter = blockList.begin();
	    end = blockList.end();
	    while ( iter != end )
		{
		if ( *iter < blockCnt )
		    {
		    ret=fwrite( reader.GetBlockData( *iter ), 1, reader.GetBlockDataLength( *iter ), stdout );
		    if ( ret != (int)reader.GetBlockDataLength( *iter ) )
			{
			ret=errno;
			fprintf( stderr, "Error writing block %lu to output: %s (%d)\n",
				 *iter, strerror(ret), ret );
			return ret;
			}
		    }
		iter++;
		}
	    }
	n++;
	}
    
    // Done
    return 0;
    }
