// XEMacs -*-C++-*-
#ifndef _HOMER_H_
#define _HOMER_H_
/* HLT Online Monitoring Environment including ROOT */

#include <limits.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#ifdef USE_ROOT
#include "Rtypes.h"
#endif


// Determine the sizes of the different integer type
// uint32, uint64
#if !defined(USE_ROOT) && !defined(__CINT__)
// First uint32
#if USHRT_MAX==4294967295
typedef unsigned short uint32;
#else // USHRT_MAX==4294967295

#if UINT_MAX==4294967295
typedef unsigned int uint32;
#else // UINT_MAX==4294967295

#if ULONG_MAX==4294967295l
typedef unsigned long uint32;
#else // ULONG_MAX==4294967295l

#error Could not typedef uint32

#endif // ULONG_MAX==4294967295l

#endif // UINT_MAX==4294967295

#endif // USHRT_MAX==4294967295

// Then uint64
#if ULONG_MAX==18446744073709551615ULL
typedef unsigned long uint64;
#else // ULONG_MAX==18446744073709551615UL

#if defined __GNUC__
typedef unsigned long long uint64;
#else // defined __GNUC__

#error Could not typedef uint64

#endif // defined __GNUC__

#endif // ULONG_MAX==18446744073709551615UL

typedef unsigned char uint8;

#else // !USE_ROOT && !CINT


typedef ULong_t uint32;
typedef ULong64_t uint64;
typedef Byte_t uint8;

#ifdef __CINT__
typedef int key_t;
#endif

#endif // USE_ROOT

typedef uint64 AliEventID_t;


class MonitoringReader
    {
    public:
#ifdef USE_ROOT
	//ClassDef(MonitoringReader,1)
#endif

	MonitoringReader() {};
	virtual ~MonitoringReader() {};
	
	/* Read in the next available event */
	virtual int ReadNextEvent() = 0;
	/* Read in the next available event, wait max. timeout microsecs. */
	virtual int ReadNextEvent( unsigned long timeout ) = 0;
	
	/* Return the ID of the current event */
	virtual uint64 GetEventID() const = 0;
	
	/* Return the number of data blocks in the current event */
	virtual unsigned long GetBlockCnt() const = 0;
	
	/* Return the size (in bytes) of the current event's data
	   block with the given block index (starting at 0). */
	virtual unsigned long GetBlockDataLength( unsigned long ndx ) const = 0;
	/* Return a pointer to the start of the current event's data
	   block with the given block index (starting at 0). */
	virtual const void* GetBlockData( unsigned long ndx ) const = 0;
	/* Return IP address or hostname of node which sent the 
	   current event's data block with the given block index 
	   (starting at 0). */
	virtual const char* GetBlockSendNodeID( unsigned long ndx ) const = 0;
	/* Return byte order of the data stored in the 
	   current event's data block with the given block 
	   index (starting at 0). 
	   0 is unknown alignment, 
	   1 ist little endian, 
	   2 is big endian. */
	virtual uint8 GetBlockByteOrder( unsigned long ndx ) const = 0;
	/* Return the alignment (in bytes) of the given datatype 
	   in the data stored in the current event's data block
	   with the given block index (starting at 0). 
	   Possible values for the data type are
	   0: uint64
	   1: uint32
	   2: uin16
	   3: uint8
	   4: double
	   5: float
	*/
	virtual uint8 GetBlockTypeAlignment( unsigned long ndx, uint8 dataType ) const = 0;
    
    };



class HOMERReader: public MonitoringReader
    {
    public:
#ifdef USE_ROOT
	ClassDef(HOMERReader,1)
	HOMERReader();
#endif

	/* Constructors & destructors, HOMER specific */
	/* For reading from a TCP port */
	HOMERReader( const char* hostname, unsigned short port );
	/* For reading from multiple TCP ports */
	HOMERReader( unsigned int tcpCnt, const char** hostnames, unsigned short* ports );
	/* For reading from a System V shared memory segment */
	HOMERReader( key_t shmKey, int shmSize );
	/* For reading from multiple System V shared memory segments */
	HOMERReader( unsigned int shmCnt, key_t* shmKey, int* shmSize );
	/* For reading from multiple TCP ports and multiple System V shared memory segments */
	HOMERReader( unsigned int tcpCnt, const char** hostnames, unsigned short* ports, 
		     unsigned int shmCnt, key_t* shmKey, int* shmSize );
	virtual ~HOMERReader();

	/* Return the status of the connection as established by one of the constructors.
	   0 means connection is ok, non-zero specifies the type of error that occured. */
	int GetConnectionStatus() const
		{
		return fConnectionStatus;
		}

	/* Return the index of the connection for which an error given by the above
	   function occured. */
	unsigned int GetErrorConnectionNdx() const
		{
		return fErrorConnection;
		}

	/* Defined in MonitoringReader */
	/* Read in the next available event */
	virtual int  ReadNextEvent();
	/* Read in the next available event */
	virtual int ReadNextEvent( unsigned long timeout );

	/* Return the ID of the current event */
	virtual uint64 GetEventID() const
		{
		return fCurrentEventID;
		}

	/* Return the number of data blocks in the current event */
	virtual unsigned long GetBlockCnt() const
		{
		return fBlockCnt;
		}

	/* Return the size (in bytes) of the current event's data
	   block with the given block index (starting at 0). */
	virtual void* GetBlockData( unsigned long ndx ) const;
	/* Return a pointer to the start of the current event's data
	   block with the given block index (starting at 0). */
	virtual unsigned long GetBlockDataLength( unsigned long ndx ) const;
	/* Return IP address or hostname of node which sent the 
	   current event's data block with the given block index 
	   (starting at 0).
	   For HOMER this is the ID of the node on which the subscriber 
	   that provided this data runs/ran. */
	virtual const char* GetBlockSendNodeID( unsigned long ndx ) const;
	/* Return byte order of the data stored in the 
	   current event's data block with the given block 
	   index (starting at 0). 
	   0 is unknown alignment, 
	   1 ist little endian, 
	   2 is big endian. */
	virtual uint8 GetBlockByteOrder( unsigned long ndx ) const;
	/* Return the alignment (in bytes) of the given datatype 
	   in the data stored in the current event's data block
	   with the given block index (starting at 0). 
	   Possible values for the data type are
	   0: uint64
	   1: uint32
	   2: uin16
	   3: uint8
	   4: double
	   5: float
	*/
	virtual uint8 GetBlockTypeAlignment( unsigned long ndx, uint8 dataType ) const;

	/* HOMER specific */
	/* Return the type of the data in the current event's data
	   block with the given block index (starting at 0). */
	uint64 GetBlockDataType( unsigned long ndx ) const;
	/* Return the origin of the data in the current event's data
	   block with the given block index (starting at 0). */
	uint32 GetBlockDataOrigin( unsigned long ndx ) const;
	/* Return a specification of the data in the current event's data
	   block with the given block index (starting at 0). */
	uint32 GetBlockDataSpec( unsigned long ndx ) const;

	/* Find the next data block in the current event with the given
	   data type, origin, and specification. Returns the block's 
	   index. */
	unsigned long FindBlockNdx( uint64 type, uint32 origin, 
				    uint32 spec, unsigned long startNdx=0 ) const;

	/* Return the ID of the node that actually produced this data block.
	   This may be different from the node which sent the data to this
	   monitoring object as returned by GetBlockSendNodeID. */
	const char* GetBlockCreateNodeID( unsigned long ndx ) const;

    protected:

	enum DataSourceType { kUndef=0, kTCP, kShm };
	struct DataSource
	    {
		DataSource() { fType = kUndef; };
		DataSourceType fType;
		unsigned fNdx; // This source's index
		const char* fHostname; // Filled for both Shm and TCP
		unsigned short fTCPPort;
		key_t fShmKey;
		int fShmSize;
		int fTCPConnection; // File descriptor for the TCP connection
		int fShmID; // ID of the shared memory area
		void* fShmPtr; // Pointer to shared memory area
		void* fData; // Pointer to data read in for current event from this source
		unsigned long fDataSize; // Size of data (to be) read in for current event from this source
		unsigned long fDataRead; // Data actually read for current event
	    };

	void Init();
	
	bool AllocDataSources( unsigned int sourceCnt );
	int AddDataSource( const char* hostname, unsigned short port, DataSource& source );
	int AddDataSource( key_t shmKey, int shmSize, DataSource& source );
	void FreeDataSources();
	int FreeShmDataSource( DataSource& source );
	int FreeTCPDataSource( DataSource& source );
	int ReadNextEvent( bool useTimeout, unsigned long timeout );
	void ReleaseCurrentEvent();
	int TriggerTCPSource( DataSource& source, bool useTimeout, unsigned long timeout );
	int TriggerShmSource( DataSource& source, bool useTimeout, unsigned long timeout );
	int ReadDataFromTCPSources( unsigned sourceCnt, DataSource* sources, bool useTimeout, unsigned long timeout );
	int ReadDataFromShmSources( unsigned sourceCnt, DataSource* sources, bool useTimeout, unsigned long timeout );
	int ParseSourceData( DataSource& source );
	int ReAllocBlocks( unsigned long newCnt );
	AliEventID_t GetSourceEventID( DataSource& source );
	uint64 Swap( uint8 destFormat, uint8 sourceFormat, uint64 source )
		{
		if ( destFormat == sourceFormat )
		    return source;
		else
		    return ((source & 0xFFULL) << 56) | 
			((source & 0xFF00ULL) << 40) | 
			((source & 0xFF0000ULL) << 24) | 
			((source & 0xFF000000ULL) << 8) | 
			((source & 0xFF00000000ULL) >> 8) | 
			((source & 0xFF0000000000ULL) >> 24) | 
			((source & 0xFF000000000000ULL) >>  40) | 
			((source & 0xFF00000000000000ULL) >> 56);
		}
	uint32 Swap( uint8 destFormat, uint8 sourceFormat, uint32 source )
		{
		if ( destFormat == sourceFormat )
		    return source;
		else
		    return ((source & 0xFFUL) << 24) | 
			((source & 0xFF00UL) << 8) | 
			((source & 0xFF0000UL) >> 8) | 
			((source & 0xFF000000UL) >> 24);
		}


	struct DataBlock
	    {
		unsigned int fSource; // Index of originating data source
		void* fData;
		unsigned long fLength;
		uint64* fMetaData; // Pointer to meta data describing data itself.
		const char* fOriginatingNodeID;
	    };

	uint64 fCurrentEventID;
	unsigned long fBlockCnt;
	unsigned long fMaxBlockCnt;
	DataBlock* fBlocks;
	
	unsigned int fDataSourceCnt;
	unsigned int fTCPDataSourceCnt;
	unsigned int fShmDataSourceCnt;
	unsigned int fDataSourceMaxCnt;
	DataSource* fDataSources;

	
	int fConnectionStatus;
	unsigned fErrorConnection;

    };

#endif /* _HOMER_H_ */
