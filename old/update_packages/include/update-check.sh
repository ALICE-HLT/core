#!/bin/bash
#
# * Check Log folder or log file for error
# * return 1 in case of error
#
# File   : update-check.sh
# Author : Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 29.03.2008
#
###################################################################

USAGE () {

    echo "USAGE ... to come "
}


###################################################################
#                                                                 #
# -- LOG EXCEPTIONS                                               #
#                                                                 #
###################################################################

LOG_ERROR_EXCEPTIONS="grep -v errorStr | grep -v errorArg | grep -v AliRawDataErrorLog. "

#| grep -v AliITSRawStreamSPDErrorLog. | grep -v error.c | grep -v cdhStatusError | grep -iv error.o | grep -iv errors.o | grep -v argError | grep -v BCLErrorCallback | grep -v -E \"checkoutlog-.+\.log:A[[:space:]]+\" | grep -v TGraph2DErrors.h | grep -v MUON/AliMUONRawStreamTrigger.cxx | grep -iv Error.h | grep -iv Errors.h | grep -v ErrorUpdator.h | grep -v include/RooErrorHandler.h | grep -v include/RooErrorVar.h | grep -iv Error.d | grep -iv Errors.d | grep -v ErrorUpdator.d  | grep -v ErrorUpdator.o | grep -v RooErrorVar.o | grep -v RooErrorVar.d | grep -v \"checking for strerror\" | grep -v \"Compiling XrdSysError.cc\" "

LOG_WARNING_EXCEPTIONS="foo"

###################################################################
#                                                                 #
# -- DEFAULTS                                                     #
#                                                                 #
###################################################################

# Current date
IDATE=`date +%F`

# Base dir for log files
LOG_BASEDIR=/opt/HLT/installation/log

# Default log target
LOG_TARGET="*.log"

# Default log case
LOG_CASE=Error

# Default error value for errors in ErrorChecker
EXIT_VAL=2

###################################################################
#                                                                 #
# -- CHECK COMMAND LINE ARGUMENTS                                 #
#                                                                 #
###################################################################

for cmd in "$@" ; do

    # Get  option
    arg=`echo $cmd | sed 's|--[-_a-zA-Z0-9]*=||'`
	
    case "$cmd" in
	
	# print usage
	--help | -h ) echo "--Help--"; USAGE ; exit ${EXIT_VAL};;

	# set LOGFile
	--file* )	  LOG_FILE=$arg ;;    
	
	# set date
	--date* )         LOG_DATE=$arg ;;
	
	# set case to warning
	--warning )       LOG_CASE=Warning ;;

	# set basedir
	--basedir* )      LOG_BASEDIR=$arg/installation/log ;;
	

	* ) echo "ErrorChecker: Argument $cmd not known."; USAGE; exit ${EXIT_VAL};;

    esac
done

###################################################################
#                                                                 #
# -- PREPARE CHECK                                                #
#                                                                 #
###################################################################

# -- Check if LOG_DIR exists
if [ ${LOG_DATE} ] ; then
    LOG_DIR=${LOG_BASEDIR}/${LOG_DATE}
else
    LOG_DIR=${LOG_BASEDIR}/${IDATE}
fi

if [ ! -d ${LOG_DIR} ] ; then
    echo "ErrorChecker: Folder ${LOG_DIR} does not exist."
    exit ${EXIT_VAL}
fi

# -- Change to Log folder
pushd ${LOG_DIR} > /dev/null

# -- Single logfile
if [ ${LOG_FILE} ] ; then
    
    LOG_TARGET=${LOG_FILE}

    if [ ! -f ${LOG_TARGET} ] ; then
	echo "ErrorChecker: Log file ${LOG_FILE} does not exist in folder ${LOG_DIR}."
	exit ${EXIT_VAL}
    fi
fi

# -- set log exceptions
if [ "${LOG_CASE}" == "Error" ] ; then
    LOG_EXCEPTIONS=${LOG_ERROR_EXCEPTIONS}
else
    LOG_EXCEPTIONS=${LOG_WARNING_EXCEPTIONS}
fi

###################################################################
#                                                                 #
# -- CHECK LOG TARGET                                             #
#                                                                 #
###################################################################

#echo grep -i ${LOG_CASE} ${LOG_TARGET} | ${LOG_EXCEPTIONS}

grep -i ${LOG_CASE} ${LOG_TARGET} | grep -v errorStr | grep -v errorArg | grep -v AliRawDataErrorLog. | grep -v AliITSRawStreamSPDErrorLog. | grep -v error.c | grep -v cdhStatusError | grep -iv error.o | grep -iv errors.o | grep -v argError | grep -v BCLErrorCallback | grep -v -E "checkoutlog-.+\.log:A[[:space:]]+" | grep -v -E "A[[:space:]]+" | grep -v TGraph2DErrors.h | grep -v MUON/AliMUONRawStreamTrigger.cxx | grep -iv Error.h | grep -iv Errors.h | grep -v ErrorUpdator.h | grep -v include/RooErrorHandler.h | grep -v include/RooErrorVar.h | grep -iv Error.d | grep -iv Errors.d | grep -v ErrorUpdator.d  | grep -v ErrorUpdator.o | grep -v RooErrorVar.o | grep -v RooErrorVar.d | grep -v "checking for strerror" | grep -v "Compiling XrdSysError.cc" | grep -v TGraphErrors | grep -v AliTRDqaGuiBlackError | grep -v "pedantic-errors" | grep -v "apps/base/Class-ErrorHandler" | grep -v "apps/base/Error" | grep -v "::EventAnnounceError" | grep -v "::TError" | grep -v "::DefaultErrorCallback" | grep -v "::BridgeHeadErrorCallback" | grep -v "::ETRDtrackError" | grep -v "void HLTProxy::setERROR" | grep -v "virtual BCLErrorAction BCLStackTraceCallback" | grep -v "Checking for libglobus_openssl_error_gcc64" | grep -v "void alice::hlt::proxy::HLT::setERROR()" | grep -v "void AliITSgeom::LtoLErrorMatrix" | grep -v "AliTRDtrackV1::ETRDlayerError" | grep -v "DUSE_ROOT_ERROR" | grep -v "libglobus_openssl_error_gcc64dbg.a" | grep -v "ExpectedErrorPruneTool." | grep -v "fastjet::DnnError::DnnError" | grep -v "fastjet::DnnError::_message" | grep -v "warning: unused parameter" | grep -v "void AliMUONTrackerQADataMakerRec::FillErrors(AliMUONLogger"

LOG_RESULT=$?

if [ "${LOG_RESULT}" == "0" ] ; then



##### TMP FIX

   # echo grep -i ${LOG_CASE} "compilation error, trying again without the -j option" ${LOG_TARGET}
    if [ "$?" == "0" ] ; then

	is_done=`tail -n 1  ${LOG_TARGET}` 
	if [ "${is_done}" == "done" ] ; then 
	    LOG_RESULT=1
	else
	    LOG_RESULT=0
	fi
    fi
fi 


## Check for empty file
if [ ! -s ${LOG_FILE} ] ; then 
    LOG_RESULT=0
    echo " =============================================== "
    echo " ERROR : File  ${LOG_FILE} is empty"
fi

## Check for checkout abort
if [ "${LOG_RESULT}" == "1" ] ; then

    grep -i "Connection reset by peer" ${LOG_TARGET}
    CHECKOUT_RESULT=$?

    if [ "${CHECKOUT_RESULT}" == "0" ] ; then
	LOG_RESULT=0
   
	echo " =============================================== "
	echo " ERROR : Checkout not complete"
    fi
fi

###################################################################
#                                                                 #
# -- GET RESULTS LOG TARGET                                       #
#                                                                 #
###################################################################

# -- error / warning occured
if [ "${LOG_RESULT}" == "0" ] ; then
    echo " =============================================== "
    if [ "${LOG_CASE}" == "Error" ] ; then
	echo "            ERORRS HAVE BEEN FOUND !!"
    else
	echo "           WARNINGS HAVE BEEN FOUND !!"
    fi
    echo " =============================================== "

    EXIT_VAL=1

# -- nothing occured
else
    EXIT_VAL=0
fi

popd > /dev/null

exit ${EXIT_VAL}