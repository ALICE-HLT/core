#!/bin/bash

###################################################################
#                                                                 #
# -- CHECKOUT COMPILE                                             #
#                                                                 #
###################################################################

CHECKOUT_COMPILE () {

    if [[ ${runTYPE} == "check" || ${runTYPE} == "consistency" ]] ; then
	return
    fi

    CREATE_MODULES

    if [ ${runTYPE} == "checkout" ] ; then
	CHECKOUT
    elif [ ${runTYPE} == "compile" ] ; then
	COMPILE
    elif [ ${runTYPE} == "all" ] ; then
	
        # installation
        ###############################################################
	if [ $coINSTALLATION ] ; then CHECKOUT_INSTALLATION; fi
	
        # alien
        ###############################################################
	if [ ${coALIEN} ] ; then CHECKOUT_ALIEN; fi
	if [ ${enableALIEN} ] ; then COMPILE_ALIEN; fi

        # fastjet
        ###############################################################
	if [ ${coFASTJET} ] ; then CHECKOUT_FASTJET; fi
	if [ ${enableFASTJET} ] ; then COMPILE_FASTJET; fi

	# aliroot / root / geant3
        ###############################################################
	if [ ${modeTEST} ] ; then 
	    if [ ${coROOT} ] ; then TESTMODE_ROOT; fi
	    if [ ${coGEANT3} ] ; then TESTMODE_GEANT3; fi
	    if [ ${coALIROOT} ] ; then TESTMODE_ALIROOT; fi
	else
	    if [ ${coALIROOT} ] ; then  CHECKOUT_ALIROOT; fi
	    if [[ ${enableALIROOT} ||  ${enableROOT} || ${enableGEANT3} ]] ; then COMPILE_ALIROOT; fi
	fi

        # data-transport
        ###############################################################
	if [ ${coDATATRANSPORT} ] ; then CHECKOUT_DATATRANSPORT; fi
	if [ ${enableDATATRANSPORT} ] ; then COMPILE_DATATRANSPORT; fi

        # hlt
        ###############################################################
	if [ ${modeTEST} ] ; then 
	    if [ ${coHLT} ] ; then TESTMODE_HLTANALYSIS; fi
	else
	    if [ ${coHLT} ] ; then CHECKOUT_HLTANALYSIS; fi
	    if [ ${enableHLT} ] ; then COMPILE_HLTANALYSIS; fi
	fi

	# cluster usage
        ###############################################################
	if [ "${clusterUSAGE}" == "1" ] ; then

            # ecs-proxy
            ###########################################################
	 #   if [ ${coECSPROXY} ] ; then CHECKOUT_ECSPROXY ; fi
	  #  if [ ${enableECSPROXY} ] ; then COMPILE_ECSPROXY; fi

            # taxi
            ###########################################################
	    if [ ${modeTEST} ] ; then 
		if [ ${coTAXI} ] ; then TESTMODE_TAXI; fi
	    else
		if [ ${coTAXI} ] ; then CHECKOUT_TAXI; fi
		if [ ${enableTAXI} ] ; then COMPILE_TAXI; fi
	    fi

            # pendolino
            ###########################################################
	    if [ ${modeTEST} ] ; then 
		if [ ${coPENDOLINO} ] ; then TESTMODE_PENDOLINO; fi
	    else
		if [ ${coPENDOLINO} ] ; then CHECKOUT_PENDOLINO; fi
		if [ ${enablePENDOLINO} ] ; then COMPILE_PENDOLINO; fi
	    fi
            # HCDB MANAGER
            ###########################################################
	    if [ ${modeTEST} ] ; then 
		if [ ${coHCDBMANAGER} ] ; then TESTMODE_HCDBMANAGER; fi
	    else
		if [ ${coHCDBMANAGER} ] ; then CHECKOUT_HCDBMANAGER; fi
		if [ ${enableHCDBMANAGER} ] ; then COMPILE_HCDBMANAGER; fi
	    fi
	    
            # control
            ###########################################################
#	    if [ ${coCONTROL} ] ; then CHECKOUT_CONTROL; fi
#	    if [ ${enableCONTROL} ] ; then COMPILE_CONTROL; fi
	    
	fi
    fi
}

###################################################################
#                                                                 #
# -- CHECKOUT                                                     #
#                                                                 #
###################################################################

CHECKOUT () {

    # installation
    ###############################################################
    if [ $coINSTALLATION ] ; then CHECKOUT_INSTALLATION; fi
    
    # alien
    ###############################################################
    if [ ${coALIEN} ] ; then CHECKOUT_ALIEN; fi

    # fastjet
    ###############################################################
    if [ ${coFASTJET} ] ; then CHECKOUT_FASTJET; fi
    
    # aliroot / root / geant3
    ###############################################################
    if [ ${coALIROOT} ] ; then CHECKOUT_ALIROOT; fi
    
    # data-transport
    ###############################################################
    if [ ${coDATATRANSPORT} ] ; then CHECKOUT_DATATRANSPORT; fi
    
    # cluster usage
    ###############################################################
    if [ "${clusterUSAGE}" == "1" ] ; then
	
        # ecs-proxy
        ###########################################################
	if [ ${coECSPROXY} ] ; then CHECKOUT_ECSPROXY; fi
	
        # taxi
        ###########################################################
	if [ ${coTAXI} ] ; then CHECKOUT_TAXI; fi
	
        # pendolino
        ###########################################################
	if [ ${coPENDOLINO} ] ; then CHECKOUT_PENDOLINO; fi
	
        # HCDB MANAGER
        ###########################################################
	if [ ${coHCDBMANAGER} ] ; then CHECKOUT_HCDBMANAGER; fi

        # control
        ###############################################################
	if [ ${coCONTROL} ] ; then CHECKOUT_CONTROL; fi
    fi
    
    # hlt
    ###############################################################
    if [ ${coHLT} ] ; then CHECKOUT_HLTANALYSIS; fi

    # if something changed ... create new toplevel HLT modules
    ###############################################################
    if [[ ${coALIEN} || ${coALIROOT} || ${coDATATRANSPORT} || ${coHLT} || ${coFASTJET} || ${coECSPROXY} || ${coTAXI} || ${coHCDBMANAGER} || ${coPENDOLINO} || ${coCONTROL} ]] ; then

	if [ ! ${coHLT} ] ; then
	    MODULE_HLT_VERSION=HEAD_${IDATE}
	else
	    MODULE_HLT_VERSION=${versionHLT}
	fi
	
	M_BUILD_TYPE="debug"
	MODULE_HLT
	M_BUILD_TYPE="prod"
	MODULE_HLT
    fi
    
    if [ "${runTYPE}" == "all" ] ; then 
	cecho "###################################################################" $bgreen
	cecho "#                                                                 #" $bgreen
	cecho "#                   --- CHECKOUT DONE ---                         #" $bgreen
	cecho "#                                                                 #" $bgreen
	cecho "###################################################################" $bgreen
    fi
    
    echo " "
    echo "Compile packages with :"
    echo "==============================="
    cecho "${compileCOMMAND}" $blue
    echo " " 
    cecho "or use" $blue
    cecho "${compileFILE}" $blue
}

###################################################################
#                                                                 #
# -- COMPILE                                                      #
#                                                                 #
###################################################################

COMPILE () {

    # alien
    ###############################################################
    if [ ${enableALIEN} ] ; then COMPILE_ALIEN; fi

    # fastjet
    ###############################################################
    if [ ${enableFASTJET} ] ; then COMPILE_FASTJET; fi
	
    # aliroot / root / geant3
    ###############################################################
    if [[ ${enableALIROOT} ||  ${enableROOT} || ${enableGEANT3} ]] ; then COMPILE_ALIROOT; fi
	
    # data-transport
    ###############################################################
    if [ ${enableDATATRANSPORT} ] ; then COMPILE_DATATRANSPORT;	fi
	
    # hlt
    ###############################################################
    if [ ${enableHLT} ] ; then COMPILE_HLTANALYSIS; fi

    # cluster usage
    ###############################################################
    if [ "${clusterUSAGE}" == "1" ] ; then

        # ecs proxy
        ###############################################################
	if [ ${enableECSPROXY} ] ; then COMPILE_ECSPROXY; fi

        # taxi
        ###############################################################
	if [ ${enableTAXI} ] ; then COMPILE_TAXI; fi

        # pendolino
        ###############################################################
	if [ ${enablePENDOLINO} ] ; then COMPILE_PENDOLINO; fi

        # HCDB manager
        ###############################################################
	if [ ${enableHCDBMANAGER} ] ; then COMPILE_HCDBMANAGER; fi

        # control
        ###############################################################
	if [ ${enableCONTROL} ] ; then COMPILE_CONTROL; fi

    fi	 

}

###################################################################
#                                                                 #
# -- CREATE MODULES                                               #
#                                                                 #
###################################################################

CREATE_MODULES () {

    # - create BASE module
    ################################
    MODULE_BASE

    # - create DEFINITION module
    ################################
    MODULE_DEFINITION

    # - create ALIEN module
    ################################
    MODULE_ALIEN

    # - create FASTJET module
    ################################
    MODULE_FASTJET
    
    # - create ROOT module
    ################################
    MODULE_ROOT

    # - create ROOT module
    ################################
    MODULE_GEANT3

    # - create ALIROOT module
    ################################
    MODULE_ALIROOT

    # - create DATATRANSPORT module
    ################################
    MODULE_DATATRANSPORT

    # - create HLTANALYSIS module
    ################################
    MODULE_HLTANALYSIS

    # cluster usage
    ###############################################################
    if [ "${clusterUSAGE}" == "1" ] ; then

        # - create ECSPROXY module
        ################################
	MODULE_ECSPROXY

        # - create TAXI module
        ################################
	MODULE_TAXI

        # - create PENDOLINO module
        ################################
	MODULE_PENDOLINO

        # - create HCDBMANAGER module
        ################################
	MODULE_HCDBMANAGER

        # - create CONTROL module
        ################################
	MODULE_CONTROL
    fi
    
    # if something changed ... create new toplevel HLT modules
    ###############################################################
    if [[ ${coALIEN} || ${coALIROOT} || ${coROOT} || ${coGEANT3} || ${coDATATRANSPORT} || ${coHLT} || ${coFASTJET} || ${coECSPROXY} || ${coTAXI} || ${coHCDBMANAGER} || ${coPENDOLINO} || ${coCONTROL} ]] ; then
	
	if [ ! ${coHLT} ] ; then
	    MODULE_HLT_VERSION=HEAD_${IDATE}
	else
	    MODULE_HLT_VERSION=${versionHLT}
	fi
	
	M_BUILD_TYPE="debug"
	MODULE_HLT
	M_BUILD_TYPE="prod"
	MODULE_HLT
    fi
    
}