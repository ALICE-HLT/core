#!/bin/bash

###################################################################
#                                                                 #
# -- rmPathFromList                                               #
#                                                                 #
###################################################################
#                                                                 #
#   $1 LIST                                                       #
#   $2 ENTRY                                                      #
#   USEAGE : FOO=`rmPathFromList "$FOO" "xxx"`                    #
#                                                                 #
###################################################################

rmPathFromList () {

    newList=""
    for iter in $1 ; do 
	entry=`basename $iter`
	
	if [ "$entry" != "$2" ] ; then
	    newList="$newList $iter"
	fi
    done
    
    echo "$newList"
}

###################################################################
#                                                                 #
# -- rmPathFromListChecked                                        #
#                                                                 #
###################################################################
#                                                                 #
#   $1 LIST                                                       #
#   $2 ENTRY                                                      #
#   USEAGE : FOO=`rmPathFromListChecked "$FOO" "xxx"`             #
#                                                                 #
###################################################################

rmPathFromListChecked () {
    
    if [ $2 ] ; then 
	path=`basename $2`
	list=`rmPathFromList "$1" $path`
	if [ -h $2 ] ; then
	    link=`readlink $2`
	    path=`basename $link`
	    list=`rmPathFromList "$list" $path`
	fi

	echo $list
    else
	echo $1
    fi
}

###################################################################
#                                                                 #
# -- rmModuleFromList                                             #
#                                                                 #
###################################################################
#                                                                 #
#   $1 LIST                                                       #
#   $2 ENTRY                                                      #
#   USEAGE : FOO=`rmModuleFromList "$FOO" xxx`                    #
#                                                                 #
###################################################################

rmModuleFromList () {

    entry=`module -l list 2>&1 | grep "\<$2/" | awk -F/ '{print $2}'| awk -F' ' '{ print $1 }'`

    if [ "$entry" ] ; then 
	entry="${MODULEDIR}/$2/${entry}"
	echo ${1/$entry/}
    else
	echo $1
    fi
}

###################################################################
#                                                                 #
# -- printList                                                    #
#                                                                 #
###################################################################

printList() {

    if [ "$1" ] ; then
	echo "- $2 :"
	for i in $1 ; do echo "  - `basename $i`"; done
	echo " -> " $1 
	echo " "
    fi
}

###################################################################
#                                                                 #
# -- createCheckLists                                             #
#                                                                 #
###################################################################

createCheckLists() {
    
    # -- find all modules --
    # ----------------------

    LIST_BASE_MODULES=`find ${MODULEDIR}/BASE/ -type f | grep -v ".version"`
    LIST_DEFINITION_MODULES=`find ${MODULEDIR}/DEFINITION/ -type f | grep -v ".version"`    
    LIST_HLT_MODULES=`find ${MODULEDIR}/HLT/ -type f | grep -v ".version"`

    LIST_ALIEN_MODULES=`find ${MODULEDIR}/ALIEN/ -type f | grep -v ".version"`
    LIST_FASTJET_MODULES=`find ${MODULEDIR}/FASTJET/ -type f | grep -v ".version"`
    LIST_ALIROOT_MODULES=`find ${MODULEDIR}/ALIROOT/ -type f | grep -v ".version"`
    LIST_ROOT_MODULES=`find ${MODULEDIR}/ROOT/ -type f | grep -v ".version"`
    LIST_GEANT3_MODULES=`find ${MODULEDIR}/GEANT3/ -type f | grep -v ".version"`

    LIST_HLTANALYSIS_MODULES=`find ${MODULEDIR}/HLTANALYSIS/ -type f | grep -v ".version"`
    LIST_DATATRANSPORT_MODULES=`find ${MODULEDIR}/DATATRANSPORT/ -type f | grep -v ".version"`

    if [ -d ${MODULEDIR}/TAXI ] ; then
	LIST_TAXI_MODULES=`find ${MODULEDIR}/TAXI/ -type f | grep -v ".version"`
    fi

    if [ -d ${MODULEDIR}/ECSPROXY ] ; then
	LIST_ECSPROXY_MODULES=`find ${MODULEDIR}/ECSPROXY/ -type f | grep -v ".version"`
    fi

    if [ -d ${MODULEDIR}/PENDOLINO ] ; then
	LIST_PENDOLINO_MODULES=`find ${MODULEDIR}/PENDOLINO/ -type f | grep -v ".version"`
    fi

    if [ -d ${MODULEDIR}/HCDBMANAGER ] ; then
	LIST_HCDBMANAGER_MODULES=`find ${MODULEDIR}/HCDBMANAGER/ -type f | grep -v ".version"`
    fi

    if [ -d ${MODULEDIR}/CONTROL ] ; then
	LIST_CONTROL_MODULES=`find ${MODULEDIR}/CONTROL/ -type f | grep -v ".version"`
    fi

    # -- find all PATHS --
    # --------------------

    LIST_ALIEN_PATH=`find ${HLT_BASEDIR}/alien/ -mindepth 1 -maxdepth 1`
    LIST_ALIEN_PATH=`rmPathFromList "$LIST_ALIEN_PATH" "installer"`
    LIST_ALIEN_PATH=`rmPathFromList "$LIST_ALIEN_PATH" "alien-installer"`
    LIST_ALIEN_PATH=`rmPathFromList "$LIST_ALIEN_PATH" ".alien"`

    LIST_FASTJET_PATH=`find ${HLT_BASEDIR}/fastjet/ -mindepth 1 -maxdepth 1`
    LIST_ALIROOT_PATH=`find ${HLT_BASEDIR}/aliroot/ -mindepth 1 -maxdepth 1`

    LIST_GEANT3_PATH=`find ${HLT_BASEDIR}/geant3/ -mindepth 1 -maxdepth 1`
    LIST_ROOT_PATH=`find ${HLT_BASEDIR}/root/ -mindepth 1 -maxdepth 1`

    LIST_DATATRANSPORT_PATH=`find ${HLT_BASEDIR}/data-transport/ -mindepth 1 -maxdepth 1`
    LIST_HLTANALYSIS_PATH=`find ${HLT_BASEDIR}/analysis/ -mindepth 1 -maxdepth 1`

    if [ -d ${HLT_BASEDIR}/interfaces/taxi ] ; then
	LIST_TAXI_PATH=`find ${HLT_BASEDIR}/interfaces/taxi/ -mindepth 1 -maxdepth 1`
    fi
    if [ -d ${HLT_BASEDIR}/interfaces/ecs-proxy ] ; then
	LIST_ECSPROXY_PATH=`find ${HLT_BASEDIR}/interfaces/ecs-proxy/ -mindepth 1 -maxdepth 1`
    fi
    if [ -d ${HLT_BASEDIR}/interfaces/HCDBManager ] ; then
	LIST_HCDBMANAGER_PATH=`find ${HLT_BASEDIR}/interfaces/HCDBManager/ -mindepth 1 -maxdepth 1`
    fi
    if [ -d ${HLT_BASEDIR}/interfaces/pendolino ] ; then
	LIST_PENDOLINO_PATH=`find ${HLT_BASEDIR}/interfaces/pendolino/ -mindepth 1 -maxdepth 1`
    fi
    if [ -d ${HLT_BASEDIR}/control/releases ] ; then
	LIST_CONTROL_PATH=`find ${HLT_BASEDIR}/control/releases/ -mindepth 1 -maxdepth 1`
    fi
}

###################################################################
#                                                                 #
# -- checkLoadability                                             #
#                                                                 #
###################################################################

checkLoadability() {

    iResult=0

    LIST_LOAD_MODULES=$LIST_HLT_MODULES

    for module in ${LIST_LOAD_MODULES} ; do
	name=`basename $module`
	
	if [ "$name" == ".version" ] ; then
	    continue;
	fi

	Error=`module load HLT/$name 2>&1`
	
	if [ "$Error" ] ; then
	    echo "Error loading all modules belonging to module"
	    echo " > $name < "
	    message=`echo "$Error" | awk -F\' '{print $2}'`
	    for msg in $message ; do 
		echo "    - $msg"
	    done
	    echo "-------------------------------------"
	    Error=""
	else
	    module load HLT/$name
	    
	    # -- check module List
	    # ----------------------

	    LIST_HLT_MODULES=`rmModuleFromList "${LIST_HLT_MODULES}" HLT`

	    LIST_BASE_MODULES=`rmModuleFromList "${LIST_BASE_MODULES}" BASE`
	    LIST_DEFINITION_MODULES=`rmModuleFromList "$LIST_DEFINITION_MODULES" DEFINITION`

	    LIST_ALIEN_MODULES=`rmModuleFromList "$LIST_ALIEN_MODULES" ALIEN`
	    LIST_FASTJET_MODULES=`rmModuleFromList "$LIST_FASTJET_MODULES" FASTJET`
	    LIST_ALIROOT_MODULES=`rmModuleFromList "$LIST_ALIROOT_MODULES" ALIROOT`
	    LIST_ROOT_MODULES=`rmModuleFromList "$LIST_ROOT_MODULES" ROOT`
	    LIST_GEANT3_MODULES=`rmModuleFromList "$LIST_GEANT3_MODULES" GEANT3`

	    LIST_DATATRANSPORT_MODULES=`rmModuleFromList "$LIST_DATATRANSPORT_MODULES" DATATRANSPORT`
	    LIST_TAXI_MODULES=`rmModuleFromList "$LIST_TAXI_MODULES" TAXI`
	    LIST_ECSPROXY_MODULES=`rmModuleFromList "$LIST_ECSPROXY_MODULES" ECSPROXY`
	    LIST_PENDOLINO_MODULES=`rmModuleFromList "$LIST_PENDOLINO_MODULES" PENDOLINO`
	    LIST_HCDBMANAGER_MODULES=`rmModuleFromList "$LIST_HCDBMANAGER_MODULES" HCDBMANAGER`
	    LIST_CONTROL_MODULES=`rmModuleFromList "$LIST_CONTROL_MODULES" CONTROL`
	    LIST_HLTANALYSIS_MODULES=`rmModuleFromList "$LIST_HLTANALYSIS_MODULES" HLTANALYSIS`

	    # -- check path List
	    # --------------------

	    LIST_ALIEN_PATH=`rmPathFromListChecked "$LIST_ALIEN_PATH" $ALIEN_ROOT`
	    LIST_FASTJET_PATH=`rmPathFromListChecked "$LIST_FASTJET_PATH" ${FASTJET%/build}`
	    LIST_ALIROOT_PATH=`rmPathFromListChecked "$LIST_ALIROOT_PATH" $ALICE_ROOT`

	    LIST_ROOT_PATH=`rmPathFromListChecked "$LIST_ROOT_PATH" $ROOTSYS`
	    LIST_ROOT_PATH=`rmPathFromListChecked "$LIST_ROOT_PATH" $ALICE_ROOT`

	    LIST_GEANT3_PATH=`rmPathFromListChecked "$LIST_GEANT3_PATH" $ALICE_ROOT`
	    LIST_GEANT3_PATH=`rmPathFromList "$LIST_GEANT3_PATH" libs_\`basename $ROOTSYS\``

	    geant3Version=`grep geant_version $module | grep set | awk -F' ' '{print $3}'`
	    LIST_GEANT3_PATH=`rmPathFromList "$LIST_GEANT3_PATH" geant3_$geant3Version`

	    LIST_DATATRANSPORT_PATH=`rmPathFromListChecked "$LIST_DATATRANSPORT_PATH" $ALIHLT_DC_DIR
`
	    LIST_TAXI_PATH=`rmPathFromListChecked "$LIST_TAXI_PATH" $HLTTAXI_TOPDIR`
	    LIST_ECSPROXY_PATH=`rmPathFromListChecked "$LIST_ECSPROXY_PATH" $HLTECS_TOPDIR`
	    LIST_HCDBMANAGER_PATH=`rmPathFromListChecked "$LIST_HCDBMANAGER_PATH"  $HLTHCDBMANAGER_TOPDIR`
	    LIST_PENDOLINO_PATH=`rmPathFromListChecked "$LIST_PENDOLINO_PATH" $HLTPENDOLINO_TOPDIR`

	    LIST_CONTROL_PATH=`rmPathFromListChecked "$LIST_CONTROL_PATH" $HLTCONTROL_TOPDIR`
	    LIST_HLTANALYSIS_PATH=`rmPathFromListChecked "$LIST_HLTANALYSIS_PATH" $ALIHLT_TOPDIR`

	    module unload HLT
	fi
    done
    
    return
}

