#!/bin/bash

###################################################################
#                                                                 #
# -- COLORED ECHO                                                 #
#                                                                 #
###################################################################

cecho () {
    local default_msg="foo"
    
    message=${1:-$default_msg}
    color=${2:-$black}
   
    echo -ne "$color"
    echo -n "$message"
    echo -e "$black"
}

###################################################################
#                                                                 #
# -- ERROR HANDLER                                                #
#                                                                 #
###################################################################

ERROR_HANDLER () {
    
    for cmd in "$@" ; do
	
        # Get  option
	arg=`echo $cmd | sed 's|--[-_a-zA-Z0-9]*=||'`

	case "$cmd" in
		
	    --error*  ) ERROR_MSG=$arg ;;    

	    --log* ) LOG_TARGET=$arg ;;    

	    --usage ) printUSAGE=1 ;;

	    * ) cecho "Error: Error in the Error checker." $magenta;   exit 2;;
	esac
    done

    if [ ${LOG_TARGET} ] ; then

	if [ ${QUIET} -eq 0 ] ; then 
	    cecho " ********************************************* " $blue
	    cecho "  Checking file : ${LOG_TARGET} " $blue
	    cecho " ********************************************* " $blue
	fi

	${INCLUDE_DIR}/update-check.sh --file=${LOG_TARGET} --basedir=${HLT_BASEDIR} --date=${IDATE}
       
	if [ "$?" == "1" ] ; then
	    echo " "
	    cecho " ********************************************* " $red
	    cecho "                ERROR OCCURED                  " $red
	    cecho " ********************************************* " $red
	    cecho " in : ${LOG_TARGET}" $blue
	    cecho " ********************************************* " $red
	    echo " "
	    exit 2
	elif [ "$?" == "2" ] ; then
	    cecho "Error: Error in the Error checker." $magenta
	    exit 1
	fi
	
	unset LOG_TARGET
    fi

    if [ -n "${ERROR_MSG}" ] ; then
	cecho " ********************************************* " $red
	cecho "                ERROR OCCURED                  " $red
	cecho " ********************************************* " $red 
	echo -e $ERROR_MSG
	cecho " ********************************************* " $red
	
	if [ "${printUSAGE}" == "1" ] ; then
	    cecho "See '$0 --help' for usage." $blue
	fi
	
	exit 3

    fi

    if [ ${QUIET} -eq 0 ] ; then 
	cecho "  ... File is ok " $blue
	cecho " ********************************************* " $blue
    fi
}  
   
###################################################################
#                                                                 #
# -- LOG HANDLER                                                  #
#                                                                 #
###################################################################

LOG_HANDLER () { 
    
    for cmd in "$@" ; do
	
        # Get  option
	arg=`echo $cmd | sed 's|--[-_a-zA-Z0-9]*=||'`

	case "$cmd" in
		
	    --log* ) LOG_NAME=$arg ;;    

	    * ) cecho "Error: Error in LOG_HANDLER." $magenta;   exit 2;;
	esac
    done

    if [ ! ${LOG_NAME} ] ; then
	cecho "Error: Error in LOG_HANDLER." $magenta;   
	exit 2
    fi
    
    LOG_FILE_VERSION=0

    LOG_FILE=${LOG_DIR}/${LOG_NAME}_${LOG_FILE_VERSION}.log

    while [ -f ${LOG_FILE} ]
      do

      let LOG_FILE_VERSION=${LOG_FILE_VERSION}+1
      LOG_FILE=${LOG_DIR}/${LOG_NAME}_${LOG_FILE_VERSION}.log
    done
    
    LOG_TARGET=${LOG_DIR}/${LOG_NAME}_${LOG_FILE_VERSION}.log
}

###################################################################
#                                                                 #
# -- INFO                                                         #
#                                                                 #
###################################################################

INFO () {
    
    if [ ${QUIET} -eq 2 ] ; then 
	return
    fi

    if [ "$1" == "CHECKOUT" ] ; then
	echo "Checkout src:"
    elif [ "$1" == "COMPILE" ] ; then
	echo "Compile src:"
    elif [ "$1" == "TESTMODE" ] ; then
	echo "Checkout/Compile src:"
    fi

    echo "=========================================="
    echo $2
    echo "=========================================="
    echo ""
} 
