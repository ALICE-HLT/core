#!/bin/bash

###################################################################
#                                                                 #
# -- CHECK CONSISTENCY                                            #
#                                                                 #
###################################################################

CHECK_CONSISTENCY () {

    if [ ${runTYPE} != "consistency" ] ; then
	return
    fi

    # -- Cleanup / Preparation --
    # -------------------------

    pushd ${MODULEDIR} > /dev/null
    find -name "*~" -exec rm "{}" +
    popd > /dev/null

    # -- Create Lists of modules and paths --
    # ---------------------------------------

    createCheckLists

    # -- Check loadability of the modules --
    # --------------------------------------

    checkLoadability

    # -- Print remaining modules --
    # -----------------------------

    if [[ "$LIST_BASE_MODULES" || "$LIST_DEFINITION_MODULES" || \
	"$LIST_ALIEN_MODULES" || "$LIST_FASTJET_MODULES" || "$LIST_ALIROOT_MODULES" || \
	"$LIST_ROOT_MODULES" || "$LIST_GEANT3_MODULES" || "$LIST_DATATRANSPORT_MODULES" || \
	"$LIST_TAXI_MODULES" || "$LIST_ECSPROXY_MODULES" || "$LIST_PENDOLINO_MODULES" || "$LIST_HCDBMANAGER_MODULES" || \
	"$LIST_CONTROL_MODULES" || "$LIST_HLTANALYSIS_MODULS" || "$LIST_HLT_MODULES" ]] ; then

	echo " Not used modules "
	echo "=================="

	printList "$LIST_BASE_MODULES" "BASE"
	printList "$LIST_DEFINITION_MODULES" "DEFINITION"
	
	printList "$LIST_ALIEN_MODULES" "ALIEN"
	printList "$LIST_FASTJET_MODULES" "FASTJET"
	printList "$LIST_ALIROOT_MODULES" "ALIROOT"
	printList "$LIST_ROOT_MODULES" "ROOT"
	printList "$LIST_GEANT3_MODULES" "GEANT3"
	
	printList "$LIST_DATATRANSPORT_MODULES" "DATATRANSPORT"
	printList "$LIST_TAXI_MODULES" "TAXI"
	printList "$LIST_ECSPROXY_MODULES" "ECSPROXY"
	printList "$LIST_PENDOLINO_MODULES" "PENDOLINO"
	printList "$LIST_HCDBMANAGER_MODULES" "HCDBMANAGER"
	
	printList "$LIST_CONTROL_MODULES" "CONTROL"
	printList "$LIST_HLTANALYSIS_MODULES" "HLTANALYSIS"
	
	printList "$LIST_HLT_MODULES" "HLT"

	echo " All unused modules "
	echo "===================="

	echo $LIST_BASE_MODULES $LIST_DEFINITION_MODULES $LIST_ALIEN_MODULES $LIST_FASTJET_MODULES \
	    $LIST_ALIROOT_MODULES $LIST_ROOT_MODULES $LIST_GEANT3_MODULES $LIST_DATATRANSPORT_MODULES \
	    $LIST_TAXI_MODULES $LIST_ECSPROXY_MODULES $LIST_PENDOLINO_MODULES $LIST_HCDBMANAGER_MODULES \
	    $LIST_CONTROL_MODULES $LIST_HLTANALYSIS_MODULES $LIST_HLT_MODULES
    else
	echo " * All installed modules are used."
    fi

    echo " "

    # -- Print remaining paths --
    # ---------------------------

    if [[ "$LIST_ALIEN_PATH" || "$LIST_FASTJET_PATH" || "$LIST_ALIROOT_PATH" || \
	"$LIST_ROOT_PATH" || "$LIST_GEANT3_PATH" || "$LIST_DATATRANSPORT_PATH" || \
	"$LIST_TAXI_PATH" || "$LIST_ECSPROXY_PATH" || "$LIST_PENDOLINO_PATH" || "$LIST_HCDBMANAGER_PATH" || \
	"$LIST_CONTROL_PATH" || "$LIST_HLTANALYSIS_MODULS" ]] ; then

	echo " Not used paths "
	echo "================"
    
	printList "$LIST_ALIEN_PATH" "ALIEN"
	printList "$LIST_FASTJET_PATH" "FASTJET"

	printList "$LIST_ALIROOT_PATH" "ALIROOT"
	printList "$LIST_ROOT_PATH" "ROOT"
	printList "$LIST_GEANT3_PATH" "GEANT3"
	printList "$LIST_DATATRANSPORT_PATH" "DATATRANSPORT"

	printList "$LIST_TAXI_PATH" "TAXI"
	printList "$LIST_ECSPROXY_PATH" "ECSPROXY"
	printList "$LIST_HCDBMANAGER_PATH" "HCDBMANAGER"
	printList "$LIST_PENDOLINO_PATH" "PENDOLINO"
	printList "$LIST_CONTROL_PATH" "CONTROL"
	printList "$LIST_HLTANALYSIS_PATH" "HLTANALYSIS"

	echo " All unused paths "
	echo "=================="

	echo $LIST_ALIEN_PATH $LIST_FASTJET_PATH $LIST_ALIROOT_PATH $LIST_ROOT_PATH $LIST_GEANT3_PATH $LIST_DATATRANSPORT_PATH \
	    $LIST_TAXI_PATH $LIST_ECSPROXY_PATH $LIST_PENDOLINO_PATH $LIST_HCDBMANAGER_PATH $LIST_CONTROL_PATH \
	    $LIST_HLTANALYSIS_MODULS

    else
	echo " * All installed paths are used."
    fi
}