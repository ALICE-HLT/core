#!/bin/bash

###################################################################
#                                                                 #
# -- SET DEFAULT VALUES                                           #
#                                                                 #
###################################################################

# Set to 0 if script is not intended to be used at the HLT Cluster at CERN 
clusterUSAGE=1

# The BASE PATH of the HLT directory
HLT_BASEDIR=/opt/HLT

# The version of the BASE module
versionBASE=base

# The version of the DEFINITION module
versionDEFINITION=base

# The default for datatransport type
typeDATATRANSPORT=stable

# SleepTime before the next action
SLEEPTIME=4

# CTest binary to use
CTEST="/usr/local/bin/ctest"

# Date of the installation
IDATE=`date +%F`
YESTERDAY=`date --date "1 day ago" +%F`

# Set to value > 1 in case of error
ERRORNO=0
	
# Quiet output off 
QUIET=0

# Colors for bash
black='\E[30m'
red='\E[31m'
green='\E[32m'
bgreen='\E[01;32m'
blue='\E[34m'
magenta='\E[35m'

# ---------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------

#                                          USAGE

# ---------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------

###################################################################
#                                                                 #
# -- USAGE of the update suite                                    #
#                                                                 #
###################################################################

USAGE () {

cat <<EOF

Usage :
$0 <RUNTYPE> <OPTIONS> [ <PACKAGES> | --config=<CONFIG-FILE-NAME|current> ]

 RUNTYPE
#################################################################
 The run type is mandatory, on of the following has to be chosen:
  * check       -    Check which packages have to be installed
  * checkout    -    Checkout packages
  * compile     -    Compile packages
  * all         -    Checkout and compile packages
  * consistency -    Checks consistency of installed packages

 PACKAGES
#################################################################
 This script install all, or some of the following packages:
  
  * alien
  * fastjet
  * root
  * geant3
  * aliroot
  * hlt
  * datatransport
  * ecsproxy
  * taxi
  * hcdbmanager
  * pendolino
  * control

 META-PACKAGES
#################################################################
 For the enable option also meta packages can be chosen.

  * interfaces
    - Includes
    -> ecsproxy
    -> taxi
    -> hcdbmanager
    -> pendolino
    -> control

 PACKAGE VERSIONS
#################################################################
 The versions of all the packages have to be given, even if they
 are not intended to be installed. This is to ensure the
 dependencies of the packages.

 Existing package combination will NOT be overwritten.

  --<PACKAGE-NAME>=<PACKAGE-VERSION>

 COMPILE OPTIONS
#################################################################
 In order to compile the checked out targets, the enable flags have
 to be set.

 --enable=<PACKAGE-NAME>


 OPTIONS
#################################################################
  --installation=<ALIROOT-INSTALLER-VERSION>
         Version of aliroot installer

  --config=<CONFIG-FILE-NAME|current>
         A configuration file can be specified, which keeps all
         versions which should be installed. This can be either
         'current' for the one of today, or 'config_YYYY-MM-DD'

  --config-path=<PATH-TO-CONFIG-FILE>
         Specifies the path to the config file, if it is not in
	 the default directory : 
	 "/opt/HLT/tools/src/update_packages/installation-config"

  --datatransport-type=<DATATRANSPORT-TYPE>
         Can be either "stable"  or "current".
         If not given, default is set to "stable".

  --disableClusterUsage
         Use this option if you want to run this script on system,
         different from the HLT cluster. It disables hostname checks
         which are not needed somewhere else.

  --basepath=<HLT-BASE-PATH>
         Use if you want to install it into another directory than
         the default "/opt/HLT". Overwrites any basepath in the config file

  --testmode
         Enables the test mode

  --quiet
         Sets outputmode to quiet, only errors are displayed

  --help
         Shows this help.

  --version
         Shows the version of these scripts.

 EXAMPLE
#################################################################
example:
$0 all --config=config_${IDATE}

checkout - example: 
$0 checkout --aliroot=HEAD --root=v5-34-05 --geant3=v1-9 --hlt=HEAD --installation=0.9.1 --datatransport=20080208.093506 --datatransport-type=current --ecsproxy=v1.2.0 --taxi=v1.4 --HCDBManager=v1.2 --pendolino=v0.9.4 --alien=v2-15 --fastjet=2.4.1
 - or -
$0 checkout --config=config_${IDATE}

compile - example:
$0 compile --config=config_${IDATE} --enable=aliroot --enable=root --enable=geant3 --enable=hlt --enable=datatransport --enable=interfaces --enable=fastjet

EOF

}

# ---------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------

#                                          CHECKS

# ---------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------

###################################################################
#                                                                 #
# -- CHECK COMMAND LINE ARGUMENTS                                 #
#                                                                 #
###################################################################

CHECK_COMMANDLINE () {

    for cmd in $CMD_LINE ; do
	
	if [[ "$cmd" == --* || "$cmd" == -h ]] ; then
	    
           # Get  option
	    arg=`echo $cmd | sed 's|--[-_a-zA-Z0-9]*=||'`
	    
	    case "$cmd" in
		
      	        # print usage
		--help | -h ) USAGE ; exit 0 ;;

	        # print version
                --version | -v ) echo "Version : ${INSTALLER_VERSION}" ; exit 0 ;;

		# ------------------------------------------------------------

  	        # get HLT version
                --hlt* )	             versionHLT=$arg ;;    

	        # get ALIROOT version
                --alien* )                   versionALIEN=$arg ;;

	        # get ALIROOT version
                --aliroot* )                 versionALIROOT=$arg ;;
			    
	        # get ROOT version
	        --root* )                    versionROOT=$arg ;;

  	        # get GEANT3 version
	        --geant3* )                  versionGEANT3=$arg ;;

  	        # get FASTJET version
                --fastjet* )	             versionFASTJET=$arg ;;    

	        # get DATATRANSPORT type
  	        --datatransport-type* )      typeDATATRANSPORT=$arg ;;

	        # get DATATRANSPORT version
	        --datatransport* )           versionDATATRANSPORT=$arg ;;

	        # get ECSPROXY version
	        --ecsproxy* )                versionECSPROXY=$arg ;;

	        # get TAXI version
	        --taxi* )                    versionTAXI=$arg ;;

	        # get PENDOLINO version
	        --pendolino* )               versionPENDOLINO=$arg ;;

	        # get HCDBManager version
	        --HCDBManager* )             versionHCDBMANAGER=$arg ;;

	        # get CONTROL version
	        --control* )                 versionCONTROL=$arg ;;

		# ------------------------------------------------------------

		--enable* )  
               	             # compile ALIEN
 		             if [ "$arg" == "alien" ] ; then
				 enableALIEN=1
               	             # compile ALIROOT		             
 		             elif [ "$arg" == "aliroot" ] ; then
				 enableALIROOT=1
               	             # compile ROOT		     
			     elif [ "$arg" == "root" ] ; then
				 enableROOT=1
               	             # compile GEANT3
			     elif [ "$arg" == "geant3" ] ; then
				 enableGEANT3=1
               	             # compile HLT 
			     elif [ "$arg" == "hlt" ] ; then
				 enableHLT=1
               	             # compile FASTJET
			     elif [ "$arg" == "fastjet" ] ; then
				 enableFASTJET=1
               	             # compile DATATRANSPORT
			     elif [ "$arg" == "datatransport" ] ; then
				 enableDATATRANSPORT=1
            	             # compile ECSPROXY
			     elif [ "$arg" == "ecsproxy" ] ; then
				 enableECSPROXY=1			     
               	             # compile TAXI
			     elif [ "$arg" == "taxi" ] ; then
				 enableTAXI=1 
			     # compile PENDOLINO
			     elif [ "$arg" == "pendolino" ] ; then
				 enablePENDOLINO=1
			     # compile HCDBManager
			     elif [ "$arg" == "hcdbmanager" ] ; then
				 enableHCDBMANAGER=1
			     # compile CONTROL
			     elif [ "$arg" == "control" ] ; then
				 enableCONTROL=1
			     # compile all interfaces
			     elif [ "$arg" == "interfaces" ] ; then
				 enableHCDBMANAGER=1
				 enableECSPROXY=1			     
				 enableTAXI=1 
				 enablePENDOLINO=1
				 enableCONTROL=1
			     else
				 ERROR_HANDLER --error="Argument $cmd not known."
			     fi
			     ;;

		# ------------------------------------------------------------

	        # disable ClusterUsage
	        --disableClusterUsage ) clusterUSAGE=0 ;;

	        # get installation version
	        --installation* )       versionINSTALLATION=$arg ;;

		# set test mode 
		--testmode )            modeTEST=1; runTYPE=all; SLEEPTIME=0; QUIET=1;;

	        # get base path
	        --basepath* )           HLT_BASEDIR_MANUAL=$arg ;;

                # get configuration path
	        --config-path* )        configPATH=$arg ;;

	        # get configuration file
	        --config* )             configNAME=$arg ;;

		# set quiet output
		--quiet )               QUIET=1 ;;

		* ) ERROR_HANDLER --error="Argument $cmd not known." --usage ;;
	    esac
	else
	
  	    if [ "$cmd" == "check" ] ; then
	        runTYPE="check"
 	    elif [[ "$cmd" == "checkout" || "$cmd" == "co" ]] ; then
	        runTYPE="checkout"
	    elif [ "$cmd" == "compile" ] ; then
	        runTYPE="compile"
	    elif [ "$cmd" == "all" ] ; then
	        runTYPE="all"
	    elif [ "$cmd" == "consistency" ] ; then
	        runTYPE="consistency"
            else
	        runTYPE=""
	    fi
        fi
    done
}

###################################################################
#                                                                 #
# -- CHECK ARGUMENTS                                              #
#                                                                 #
###################################################################

CHECK_ARGUMENTS () {

    # check if running on cluster
    ###################################################################
    IPList=`ifconfig  | grep 'inet ' | grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}' | awk -F. '{ print $1"."$2 }'`
    
    isCLUSTER=0
    for ip in ${IPList} ; do 
	if [ "$ip" == "10.162" ] ; then isCLUSTER=1; fi
    done

    if [ ${isCLUSTER} -eq 0 ] ; then clusterUSAGE=0; fi

    # CHECK for ADMIN_USER
    ###################################################################
    if [ "${clusterUSAGE}" == "1" ] ; then
	AFS_ID=`tokens | grep "AFS ID" | awk -F ' ' '{ print $4}' | awk -F')' '{ print $1}'`
	FLAG=`pts membership $AFS_ID | grep "system:administrators" > /dev/null ; echo $?`
	if [ "${FLAG}" == "0" ] ; then
	    ADMIN_USER=1
	fi
    fi

    # check config file
    ###################################################################
    if [ ${configNAME} ] ; then

	if [ ${configPATH} ] ; then
	    if [ ! -d ${configPATH} ] ; then
		ERROR_HANDLER --error="Path ${configPATH} to ${configNAME} does not exist." --usage
	    fi
	    configFILE_PATH=${configPATH} 
	else
	    configFILE_PATH=${HLT_BASEDIR}/tools/src/update_packages/installation-config  
	fi

	if [ "${configNAME}" == "current" ] ; then
	    configNAME=config_${IDATE}
	fi

	configFILE=${configFILE_PATH}/${configNAME}

	if [ ! -f ${configFILE} ] ; then 
	    ERROR_HANDLER --error="Config file ${configFILE} does not exist." --usage
	else
	    . ${configFILE}
	fi

    fi
    
    if [ ${HLT_BASEDIR_MANUAL} ] ; then
	HLT_BASEDIR=${HLT_BASEDIR_MANUAL}
    fi
    
    # check usage
    ###################################################################
    if [ ! ${runTYPE} ] ; then
	ERROR_HANDLER --error="No Run Type specified." --usage
    fi

    if [ "${runTYPE}" != "consistency" ] ; then

        ## always necessary
	if [[ ! ${versionALIEN} || ! ${versionALIROOT} || ! ${versionROOT} || ! ${versionGEANT3} || ! ${versionHLT} || ! ${versionDATATRANSPORT} || ! ${versionFASTJET} ]] ; then
	    ERROR_HANDLER --error="" --usage
	    ERROR_HANDLER --error="Not all standard packages have been supplied\n
                                     ALIEN          : ${versionALIEN}\n
                                     ALIROOT        : ${versionALIROOT}\n
                                     ROOT           : ${versionROOT}\n
                                     GEANT3         : ${versionGEANT3}\n
                                     HLT            : ${versionHLT}\n
                                     FASTJET        : ${versionFASTJET}\n
                                     DATA-TRANSPORT : ${versionDATATRANSPORT}" --usage
	fi

        ## only on cluster
	if [ "${clusterUSAGE}" == "1" ] ; then

	    if [[ ! ${versionECSPROXY} || ! ${versionTAXI} || ! ${versionHCDBMANAGER} || ! ${versionPENDOLINO} || ! ${versionCONTROL} ]] ; then
		ERROR_HANDLER --error="Not all interface packages have been supplied\n
                                       ECSPROXY    : ${versionECSPROXY}\n      
                                       TAXI        : ${versionTAXI}\n
                                       HCDBMANAGER : ${versionHCDBMANAGER}\n
                                       PENDOLINO   : ${versionPENDOLINO}\n
                                       CONTROL     : ${versionCONTROL}" --usage
	    fi
	fi
    fi
    
    # check hostname
    ###################################################################
    if [ "${clusterUSAGE}" == "1" ] ; then
	hostname=`hostname -s`
	
	if [[ ${hostname} != "dev0" && ${hostname} != "dev1" && ${hostname} != "dev2" && ${hostname} != "dev3" && ${hostname} != "msdev0" ]] ; then
	    ERROR_HANDLER --error="Run script on dev0, dev1, dev2, dev3 or msdev0." --usage
	fi
    fi

    # check datatransport type
    ###################################################################    
    if [[ "${typeDATATRANSPORT}" != "current" && "${typeDATATRANSPORT}" != "stable" ]] ; then
	ERROR_HANDLER --error="DATATRANSPORT TYPE -- Set to \"current\" or \"stable\"." --usage
    fi
	
    # check quiet
    ###################################################################
    if [ "${QUIET}" == "1" ] ; then
	REDIRECT_A=" > "
	REDIRECT_B=" 2>&1 "
	QUIET_WGET=" -q "
	QUIET_SVN=" -q "
    else
	REDIRECT_A=" 2>&1 | tee "
	REDIRECT_B=""
	QUIET_WGET=""
	QUIET_SVN=""
    fi

    # check fortran compiler
    ###################################################################
    which g77 > /dev/null
    HAS_G77=$?

    which gfortran > /dev/null
    HAS_GFORTRAN=$?

    if [ ${HAS_G77} -eq 0 ] ; then
	OPT_F77="g77"
    elif [ ${HAS_GFORTRAN} -eq 0 ] ; then
	OPT_F77="gfortran"
    else
	ERROR_HANDLER --error="No fortran compiler found."
    fi
}

###################################################################
#                                                                 #
# -- CHECK BASEDIRS UPDATE                                        #
#                                                                 #
###################################################################

CHECK_BASEDIRS () {
    
    if [ ! -d "${HLT_BASEDIR}" ] ; then
	ERROR_HANDLER --error="HLT_BASEDIR ${HLT_BASEDIR} does not exist!"
    fi

    if [ ! -d "${HLT_BASEDIR}/modules" ] ; then 
	mkdir -p ${HLT_BASEDIR}/modules
    fi

    MODULEDIR=${HLT_BASEDIR}/modules

    if [ ! -d "${HLT_BASEDIR}/alien" ] ; then 
	mkdir -p ${HLT_BASEDIR}/alien
    fi

    if [ ! -d "${HLT_BASEDIR}/aliroot" ] ; then
	mkdir -p ${HLT_BASEDIR}/aliroot
    fi

    if [ ! -d "${HLT_BASEDIR}/root" ] ; then
	mkdir -p ${HLT_BASEDIR}/root
    fi

    if [ ! -d "${HLT_BASEDIR}/geant3" ] ; then
	mkdir -p ${HLT_BASEDIR}/geant3
    fi

    if [ ! -d "${HLT_BASEDIR}/analysis" ] ; then
	mkdir -p ${HLT_BASEDIR}/analysis
    fi

    if [ ! -d "${HLT_BASEDIR}/fastjet" ] ; then
	mkdir -p ${HLT_BASEDIR}/fastjet
    fi

    if [ ! -d "${HLT_BASEDIR}/data-transport" ] ; then
	mkdir -p ${HLT_BASEDIR}/data-transport
    fi
    
    if [ ! -d "${HLT_BASEDIR}/installation" ] ; then
	mkdir -p ${HLT_BASEDIR}/installation
    fi

    if [ ! -d "${HLT_BASEDIR}/installation/archive" ] ; then
	mkdir -p ${HLT_BASEDIR}/installation/archive
    fi
    
    ## only on cluster
    if [ "${clusterUSAGE}" == "1" ] ; then

	if [ ! -d "${HLT_BASEDIR}/interfaces/ecs-proxy" ] ; then
	    mkdir -p ${HLT_BASEDIR}/interfaces/ecs-proxy
	fi
	
	if [ ! -d "${HLT_BASEDIR}/interfaces/taxi" ] ; then
	    mkdir -p ${HLT_BASEDIR}/interfaces/taxi
	fi
	
	if [ ! -d "${HLT_BASEDIR}/interfaces/pendolino" ] ; then
	    mkdir -p ${HLT_BASEDIR}/interfaces/pendolino
	fi
	
	if [ ! -d "${HLT_BASEDIR}/interfaces/HCDBManager" ] ; then
	    mkdir -p ${HLT_BASEDIR}/interfaces/HCDBManager
	fi

	if [ ! -d "${HLT_BASEDIR}/control" ] ; then
	    mkdir -p ${HLT_BASEDIR}/control
	fi
    fi
    
}

###################################################################
#                                                                 #
# -- CHECK TARGETS INTERNAL                                       #
#                                                                 #
###################################################################

CHECK_TARGETS_INTERNAL () {

    # check targets -- alien
    ###############################################################
    if [ ! -d "${HLT_BASEDIR}/alien/${targetALIEN}" ] ; then	    
	coALIEN=1
    fi

    # check targets -- installer exists for aliroot / root / geant3
    ###############################################################
    if [[ ! -h "${HLT_BASEDIR}/installation/current" && ! ${versionINSTALLATION} ]] ; then
	ERROR_HANDLER --error="Please install also installer for aliroot/root/geant3, using the \"--installation\" option." --usage
    fi

    # check targets -- aliroot / root / geant3
    ###############################################################
    
    #if aliroot
    if [ -d "${HLT_BASEDIR}/aliroot/${targetALIROOT}" ] ; then
	# &&  ( ! root || ! geant3 )  -> error
	if [[ ! -d "${HLT_BASEDIR}/root/${targetROOT}" || ! -d "${HLT_BASEDIR}/geant3/${targetGEANT3}" ]] ; then
	    ERROR_HANDLER --error="Please use a newer AliROOT version." --usage
	fi
	# && root && geant3  -> do nothing
    #if ! aliroot
    else
        # && ! root && ! geant3  -- normal update case
	if [[ ! -d "${HLT_BASEDIR}/root/${targetROOT}" || ! -d "${HLT_BASEDIR}/geant3/${targetGEANT3}" ]] ; then	
	    coALIROOT=1
	    coROOT=1
	    coGEANT3=1
	# && root 
	elif [ -d "${HLT_BASEDIR}/root/${targetROOT}" ] ; then	

	    # get ascociated Geant3 to root version
	    DIRS=`find ${HLT_BASEDIR}/geant3/ -name "AliRoot_*" -type d` 
	    for d in $DIRS ; do 
		LDIR=`ls -l $d/lib | awk -F\> '{print $2}' | awk -F/ '{print $2}' `
		if [ "${LDIR}" == "libs_${targetROOT}" ] ; then
		    GVERSION=`ls -l $d/Geant3_version | awk -F\> '{print $2}' | awk -F/ '{print $2}' `
		fi
	    done 
	    
	    if [ "${GVERSION}" ] ; then
		if [ "${targetGEANT3}" != "$GVERSION" ] ; then
		    ERROR_HANDLER --error="Selected ${targetROOT} exists with ${GVERSION}, but ${targetGEANT3} was selected!\n
                                           please use according geant version or change root version!" --usage
		fi

		if [ ! -d "${HLT_BASEDIR}/geant3/${targetGEANT3}" ] ; then	
		    ERROR_HANDLER --error="THIS SHOULD NOT HAPPEN AT ALL\n
                                           WHEN YOU INSTALLED ${targetROOT} ${targetGEANT3} SHOULD HAVE BEEN CREATED!" --usage
		fi
		
		coALIROOT=1
	    else
		ERROR_HANDLER --error="No ascociated geant3 version to ${targetROOT} has been found." --usage
	    fi

	# && ! root && geant3
	elif [[ ! -d "${HLT_BASEDIR}/root/${targetROOT}" || -d "${HLT_BASEDIR}/geant3/${targetGEANT3}" ]] ; then	
	    coALIROOT=1
	    coROOT=1
	fi
    fi

    # check targets -- datatransport
    ###############################################################
    if [ ! -d "${HLT_BASEDIR}/data-transport/${targetDATATRANSPORT}" ] ; then	    
	coDATATRANSPORT=1
    fi

    # check targets -- hlt
    ###############################################################
    if [ ! -d "${HLT_BASEDIR}/analysis/${targetHLT}" ] ; then	    
	coHLT=1
    fi

    # check targets -- fastjet
    ###############################################################
    if [ ! -d "${HLT_BASEDIR}/fastjet/${targetFASTJET}" ] ; then	    
	coFASTJET=1
    fi

    # check targets -- installation
    ###############################################################
    if [ ! -d "${HLT_BASEDIR}/installation/${targetINSTALLATION}" ] ; then	    
	coINSTALLATION=1
    fi

    ## only on cluster
    if [ "${clusterUSAGE}" == "1" ] ; then

        # check targets -- ecs-proxy
        ###############################################################
	if [ ! -d "${HLT_BASEDIR}/interfaces/ecs-proxy/${targetECSPROXY}" ] ; then	    
	    coECSPROXY=1
	fi

        # check targets -- taxi
        ###############################################################
	if [ ! -d "${HLT_BASEDIR}/interfaces/taxi/${targetTAXI}" ] ; then	    
	    coTAXI=1
	fi
	
        # check targets -- pendolino
        ###############################################################
	if [ ! -d "${HLT_BASEDIR}/interfaces/pendolino/${targetPENDOLINO}" ] ; then	    
	    coPENDOLINO=1
	fi

        # check targets -- HCDBManager
        ###############################################################
	if [ ! -d "${HLT_BASEDIR}/interfaces/HCDBManager/${targetHCDBMANAGER}" ] ; then	    
	    coHCDBMANAGER=1
	fi

        # check targets -- control
        ###############################################################
	if [ ! -d "${HLT_BASEDIR}/control/releases/${targetCONTROL}" ] ; then	    

###################################################################
#	    FLAG=`svn list ${SVNDIR}/control/tags | grep -- "${RELEASE}"`
#	    if [ -z "${FLAG}" ] ; then
#		ERROR_HANDLER --error="Release ${RELEASE} does not exist in SVN"
#	    fi
################################################################################

	    coCONTROL=1
	fi
    fi

    # summary
    ###############################################################
    if [[ ${coINSTALLATION} || ${coALIEN} || ${coALIROOT} || ${ROOT} || ${GEANT3} || ${coFASTJET} || ${coDATATRANSPORT} || ${coHLT} || ${coECSPROXY} || ${coTAXI} || ${coPENDOLINO} || ${coHCDBMANAGER} || ${coINSTALLATION}  || ${coCONTROL} ]] ; then
	echo ""
	cecho "Packages scheduled for checkout :" $bgreen
	cecho "-> Install base path: ${HLT_BASEDIR} " $blue
	cecho "###################################################################" $bgreen
	if [ ${coINSTALLATION} ] ; then
	    echo " INSTALLATION : ${versionINSTALLATION}"
	    echo " INSTALLATION : ${targetINSTALLATION} (Target)" 
	fi

	if [ ${coALIEN} ] ; then
	    enableSTRING="${enableSTRING} --enable=alien"
	    echo "        ALIEN : ${versionALIEN}"
	    echo "        ALIEN : ${targetALIEN} (Target)" 
	    enableALIEN=1
	fi

	if [ ${coALIROOT} ] ; then
	    enableSTRING="${enableSTRING} --enable=aliroot"
	    echo "      ALIROOT : ${versionALIROOT}"
	    echo "      ALIROOT : ${targetALIROOT} (Target)" 
	    enableALIROOT=1
	fi
	
	if [ ${coROOT} ] ; then
	    enableSTRING="${enableSTRING} --enable=root"
	    echo "         ROOT : ${versionROOT}"
	    echo "         ROOT : ${targetROOT} (Target)"
	    enableROOT=1
	fi
	
	if [ ${coGEANT3} ] ; then
	    enableSTRING="${enableSTRING} --enable=geant3"
	    echo "       GEANT3 : ${versionGEANT3}"
	    echo "       GEANT3 : ${targetGEANT3} (Target)"
	    enableGEANT3=1
	fi
	
	if [ ${coDATATRANSPORT} ] ; then
	    enableSTRING="${enableSTRING} --enable=datatransport"
	    echo "DATATRANSPORT : ${versionDATATRANSPORT}"
	    echo "DATATRANSPORT : ${targetDATATRANSPORT} (Target)"
	    enableDATATRANSPORT=1	    
	fi
	
	if [ ${coHLT} ] ; then
	    enableSTRING="${enableSTRING} --enable=hlt"
	    echo "          HLT : ${versionHLT}"
	    echo "          HLT : ${targetHLT} (Target)"
	    enableHLT=1
	fi

	if [ ${coFASTJET} ] ; then
	    enableSTRING="${enableSTRING} --enable=fastjet"
	    echo "      FASTJET : ${versionFASTJET}"
	    echo "      FASTJET : ${targetFASTJET} (Target)"
	    enableFASTJET=1
	fi
	
	if [ ${coECSPROXY} ] ; then
	    enableSTRING="${enableSTRING} --enable=ecsproxy"
	    echo "     ECSPROXY : ${versionECSPROXY}"
	    echo "     ECSPROXY : ${targetECSPROXY} (Target)"
	    enableECSPROXY=1	
	fi

	if [ ${coTAXI} ] ; then
	    enableSTRING="${enableSTRING} --enable=taxi"
	    echo "         TAXI : ${versionTAXI}"
	    echo "         TAXI : ${targetTAXI} (Target)"
	    enableTAXI=1 
	fi

	if [ ${coPENDOLINO} ] ; then
	    enableSTRING="${enableSTRING} --enable=pendolino"
	    echo "    PENDOLINO : ${versionPENDOLINO}"
	    echo "    PENDOLINO : ${targetPENDOLINO} (Target)"
	    enablePENDOLINO=1
	fi

	if [ ${coHCDBMANAGER} ] ; then
	    enableSTRING="${enableSTRING} --enable=hcdbmanager"
	    echo "  HCDBManager : ${versionHCDBMANAGER}"
	    echo "  HCDBManager : ${targetHCDBMANAGER} (Target)"
	    enableHCDBMANAGER=1
	fi

	if [ ${coCONTROL} ] ; then
	    enableSTRING="${enableSTRING} --enable=control"
	    echo "      CONTROL : ${versionCONTROL}"
	    echo "      CONTROL : ${targetCONTROL} (Target)"
	    enableCONTROL=1
	fi

	if [ "${clusterUSAGE}" == "0" ] ; then
	    usageSTRING="--disableClusterUsage"
	fi

	## only on cluster
	if [ "${clusterUSAGE}" == "1" ] ; then
	    compileCOMMAND_INTERFACE="--ecsproxy=${versionECSPROXY} --taxi=${versionTAXI} --pendolino=${versionPENDOLINO} --HCDBManager=${versionHCDBMANAGER} --control=${versionCONTROL}"
	fi

	compileCOMMAND="`dirname $0`/update-packages.sh compile"

	if [ "${configNAME}" ] ; then
	    compileCOMMAND="${compileCOMMAND} --config=${configNAME}"
	else
	    compileCOMMAND="${compileCOMMAND} --alien=${versionALIEN} --aliroot=${versionALIROOT} --root=${versionROOT} --geant3=${versionGEANT3} --hlt=${versionHLT} --fastjet=${versionFASTJET} --datatransport=${versionDATATRANSPORT} --datatransport-type=${typeDATATRANSPORT} ${compileCOMMAND_INTERFACE}"
	fi

	compileCOMMAND="${compileCOMMAND} ${enableSTRING} ${usageSTRING} --basepath=${HLT_BASEDIR}"

	if [ "${runTYPE}" == "checkout" ] ; then	
	    echo " "
	    echo "Compile packages with :"
	    echo "==================================================================="
	    echo "${compileCOMMAND}"
	    echo " " 
	    echo "or use"
	    echo "${compileFILE}"
	    echo "${compileCOMMAND}" >> ${compileFILE}
	fi
    else
	echo ""
	echo "No packages scheduled for checkout."
	echo "==================================================================="
	exit
    fi

}

###################################################################
#                                                                 #
# -- CHECK TARGETS                                                #
#                                                                 #
###################################################################

CHECK_TARGETS () {

    if [[ ${runTYPE} == "check" || ${runTYPE} == "checkout" || ${runTYPE} == "all" ]] ; then
	CHECK_TARGETS_INTERNAL
    fi
}

# ---------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------

#                                          SETUP

# ---------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------

###################################################################
#                                                                 #
# -- SETUP MODULES                                                #
#                                                                 #
###################################################################

SETUP_MODULES () {

    if [ ! -f  "/etc/profile.modules" ] ; then
	ERROR_HANDLER --error="The \"module\" environment has not be installed!"
    fi  

    . /etc/profile.modules

    module purge 2> /dev/null
    
    MODULEPATH=/usr/local/Modules/versions:/usr/local/Modules/$MODULE_VERSION/modulefiles
    module use ${MODULEDIR}
}

###################################################################
#                                                                 #
# -- SETUP ENVIRONMENT                                            #
#                                                                 #
###################################################################
SETUP_ENVIRONMENT () {
        
    pushd $HOME > /dev/null
    
    echo  "#!/bin/bash
export HLT_BASEDIR=${HLT_BASEDIR}
export ALICE_BASEDIR=\${HLT_BASEDIR}/aliroot
export ROOT_BASEDIR=\${HLT_BASEDIR}/root
export GEANT3_BASEDIR=\${HLT_BASEDIR}/geant3
" > "setenv_tmp_${IDATE}"
    
    . ./`echo setenv_tmp_${IDATE}`
    
    rm -rf `echo setenv_tmp_${IDATE}`
    
    popd > /dev/null

    if [ "${clusterUSAGE}" == "1" ] ; then 
	MAKEOPT="-j16"
    else
	MAKEOPT="-j1"
    fi
}

###################################################################
#                                                                 #
# -- SETUP LOGGING                                                #
#                                                                 #
###################################################################

SETUP_LOGGING () {
    LOG_DIR=${HLT_BASEDIR}/installation/log/${IDATE}

    if [ ! -d "${LOG_DIR}" ] ; then
	mkdir -p ${LOG_DIR}
    fi
}

###################################################################
#                                                                 #
# -- SETUP COMPILE / REMOVE FILES                                 #
#                                                                 #
###################################################################

SETUP_FILES () {
    if [[ "${runTYPE}" == "checkout" || "${runTYPE}" == "all" || "${runTYPE}" == "compile" ]] ; then

	compileFILE="${HOME}/compileHLT-${IDATE}.sh"
	echo "#!/bin/bash" > ${compileFILE}
	chmod 777 ${compileFILE}
	enableSTRING=""
	
	removeFILE="${HOME}/removeHLT-${IDATE}.sh"
	echo "#!/bin/bash" > ${removeFILE}
	chmod 777 ${removeFILE}

	currentModulesFILE="${HOME}/createCurrentModulesHLT-${IDATE}.sh"
	echo "#!/bin/bash" > ${currentModulesFILE}
	chmod 777 ${currentModulesFILE}
    fi
}

###################################################################
#                                                                 #
# -- SETUP TARGETS                                                #
#                                                                 #
###################################################################

SETUP_TARGETS () {

    # alien
    ###############################################################
    targetALIEN=${versionALIEN}

    # installation 
    ###############################################################
    if [ ${versionINSTALLATION} ] ; then 
	if [ "${versionINSTALLATION}" = "HEAD" ] ; then
	    targetINSTALLATION=AliRoot-install
	else
	    targetINSTALLATION=AliRoot-install-${versionINSTALLATION}
	fi
    fi

    # aliroot
    ###############################################################
    if [ "${versionALIROOT}" = "HEAD" ] ; then
	versionALIROOT=HEAD_${IDATE}
	typeALIROOT=""
    else
	echo ${versionALIROOT} | grep Release > /dev/null
	branch=$?

	echo ${versionALIROOT} | grep Rev > /dev/null
	tag=$?

	if [ "${tag}" == "0" ] ; then
	    typeALIROOT="--tag="
	elif [ "${branch}" == "0" ] ; then
	    typeALIROOT="--branch="
	fi
    fi

    targetALIROOT=AliRoot_${versionALIROOT} 

    # root
    ###############################################################
    if [[ "${versionROOT}" = "HEAD" || ${versionROOT}" = "head" || ${versionROOT}" = "trunk" ]] ; then
	versionROOT=HEAD_${IDATE}
	versionCoROOT=head
    else
	versionCoROOT=${versionROOT}
    fi

    targetROOT=root_${versionROOT}    
    
    # geant3
    ###############################################################
    if [[ "${versionGEANT3}" = "HEAD" || ${versionGEANT3}" = "head" || ${versionGEANT3}" = "trunk" ]] ; then 
	versionGEANT3=HEAD_${IDATE}
	versionCoGEANT3=head
    else
	versionCoGEANT3=${versionGEANT3}
    fi

    targetGEANT3=geant3_${versionGEANT3}

    # hlt
    ###############################################################
    if [ "${versionHLT}" = "HEAD" ] ; then
	versionHLT=HEAD_${IDATE}
    fi
	
    targetHLT=HLT-${versionHLT}

    # fastjet
    ###############################################################
    targetFASTJET=fastjet-${versionFASTJET}
    
    # datatransport
    ###############################################################
    targetDATATRANSPORT=HLT-${typeDATATRANSPORT}-${versionDATATRANSPORT}

    ## only on cluster
    if [ "${clusterUSAGE}" == "1" ] ; then

        # ecsproxy
        ###############################################################
	targetECSPROXY=${versionECSPROXY}-${versionDATATRANSPORT}

        # taxi
        ###############################################################
	targetTAXI=${versionTAXI}-${versionALIROOT}

        # pendolino
        ###############################################################
	targetPENDOLINO=${versionPENDOLINO}-${versionALIROOT}

        # HCDBManager
        ###############################################################
	targetHCDBMANAGER=${versionHCDBMANAGER}-${versionALIROOT}

        # control
        ###############################################################
	if [ "${versionCONTROL}" = "HEAD" ] ; then
	    versionCONTROL=HEAD_${IDATE}
	    versionCoCONTROL=trunk
	else
	    versionCoCONTROL=${versionCONTROL}
	fi
	
	targetCONTROL=${versionCONTROL}
    fi
}

###################################################################
#                                                                 #
# -- SET BASE MODULES                                             #
#                                                                 #
###################################################################

SET_BASE_MODULES () {

    if [ -z ${HLTSVN_BASEURL} ] ; then
	module load BASE/base 2> /dev/null
	module load DEFINITIONS/base 2> /dev/null
    fi
}

###################################################################
#                                                                 #
# -- UNSET BASE MODULES                                           #
#                                                                 #
###################################################################

UNSET_BASE_MODULES () {

    if [ ! -z ${HLTSVN_BASEURL} ] ; then
	module remove BASE/base 2> /dev/null
	module remove DEFINITIONS/base 2> /dev/null
    fi  
}

# ---------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------

#                                           ADD to FILE

# ---------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------

###################################################################
#                                                                 #
# -- ADD TO REMOVE FILE                                           #
#                                                                 #
###################################################################

ADD_TO_REMOVE () {
    if [ "$1" != "cmd" ] ; then 
	echo "rm -rf $1" >> ${removeFILE}	
    else
	echo "$2" >> ${removeFILE}
    fi	
}

###################################################################
#                                                                 #
# -- ADD TO REMOVE FILE                                           #
#                                                                 #
###################################################################

ADD_TO_CURRENT_MODULE () {
    echo $1 >> ${currentModulesFILE}	
}

# ---------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------

#                                          MODULE HANDLING

# ---------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------

###################################################################
#                                                                 #
# -- INITIALIZE MODULE CREATION                                   #
#                                                                 #
###################################################################
INIT_MODULE_CREATION () {

    CURRENT_MODULE_PATH=$1
    CURRENT_MODULE_NAME=$2

    if [ ! -d "${CURRENT_MODULE_PATH}" ] ; then 
	mkdir -p ${CURRENT_MODULE_PATH}
    fi

    ADD_TO_REMOVE ${CURRENT_MODULE_PATH}/${CURRENT_MODULE_NAME}
    
    ## -- Add current link
    
    ADD_TO_REMOVE ${CURRENT_MODULE_PATH}/current

    if [[ -f "${CURRENT_MODULE_PATH}/current" || -h "${CURRENT_MODULE_PATH}/current" ]] ;  then
	OLD_CURRENT_MODULE_NAME=`ls -l ${CURRENT_MODULE_PATH}/current | awk -F'> ' '{print $2}'`
	
	ADD_TO_REMOVE cmd "pushd ${CURRENT_MODULE_PATH} > /dev/null"
	ADD_TO_REMOVE cmd "ln -s ${OLD_CURRENT_MODULE_NAME} current"
	ADD_TO_REMOVE cmd "popd > /dev/null"
	
	ADD_TO_CURRENT_MODULE "${CURRENT_MODULE_PATH}/current"
    fi
    
    ADD_TO_CURRENT_MODULE "pushd ${CURRENT_MODULE_PATH} > /dev/null"
    ADD_TO_CURRENT_MODULE "ln -s ${CURRENT_MODULE_NAME} current"
    ADD_TO_CURRENT_MODULE "popd > /dev/null"
}

# ---------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------
