###################################################################
#                                                                 #
# -- TESTMODE TAXI                                                #
#                                                                 #
###################################################################

TESTMODE_TAXI () {

    INFO TESTMODE "TAXI  : ${versionTAXI}"

    M_BUILD_TYPE="prod"

    SET_BASE_MODULES
    export OPTION_CHECKOUT="${HLTSVN_BASEURL}/interfaces/taxi/tags/release-${versionTAXI} -r {${YESTERDAY}}"
    export OPTION_TRACK="Experimental"
    export OPTION_BUILD=${MAKEOPT}
    export OPTION_MODULE="${MODULEDIR}/HLT/${versionHLT}-${M_BUILD_TYPE}"
    UNSET_BASE_MODULES
    
    module unload HLT  2> /dev/null
    module load HLT/${versionHLT}-${M_BUILD_TYPE}  2> /dev/null
    
    ${CTEST} -VV -S ${HLTTESTSERVER_BASEDIR}/config/CTestConfig/taxi/taxi.cmake
    
    module unload HLT  2> /dev/null

    unset OPTION_MODULE
    unset OPTION_CHECKOUT
    unset OPTION_TRACK
    unset OPTION_BUILD
    
    ADD_TO_REMOVE ${HLT_BASEDIR}/interfaces/taxi/${targetTAXI}
}

###################################################################
#                                                                 #
# -- CHECKOUT TAXI                                                #
#                                                                 #
###################################################################

CHECKOUT_TAXI () {
  
    INFO CHECKOUT "TAXI  : ${versionTAXI}"

    sleep ${SLEEPTIME}

    SET_BASE_MODULES

    if [ ${modeTEST} ] ; then 
	export OPTION_CHECKOUT="${HLTSVN_BASEURL}/interfaces/taxi/tags/release-${versionTAXI} -r {${YESTERDAY}}"
	export OPTION_TRACK="Experimental"
    else
	pushd ${HLT_BASEDIR}/interfaces/taxi > /dev/null

	LOG_HANDLER --log="checkoutlog-TAXI-${IDATE}"
	eval svn co ${HLTSVN_BASEURL}/interfaces/taxi/tags/release-${versionTAXI} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	ERROR_HANDLER

	mv release-${versionTAXI} ${targetTAXI}

	popd > /dev/null
    fi

    UNSET_BASE_MODULES
    
    ADD_TO_REMOVE ${HLT_BASEDIR}/interfaces/taxi/${targetTAXI}
}

###################################################################
#                                                                 #
# -- COMPILE TAXI                                                 #
#                                                                 #
###################################################################

COMPILE_TAXI () {

    INFO COMPILE "TAXI  : ${versionTAXI}"

    sleep ${SLEEPTIME}
    
    # prod version
    M_BUILD_TYPE="prod"
    
    module unload HLT  2> /dev/null
    module load HLT/${versionHLT}-${M_BUILD_TYPE}  2> /dev/null

    if [ ${modeTEST} ] ; then 
	export OPTION_BUILD=${MAKEOPT}
	
	${CTEST} -S ${HLTTESTSERVER_BASEDIR}/config/CTestConfig/taxi/taxi.cmake

	unset OPTION_CHECKOUT
	unset OPTION_TRACK
	unset OPTION_BUILD
    else
	pushd ${HLTTAXI_TOPDIR} > /dev/null
	
	LOG_HANDLER --log="compilelog-TAXI-${IDATE}"
	eval make ${MAKEOPT} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	ERROR_HANDLER
	
	popd > /dev/null	
    fi

    module unload HLT  2> /dev/null
}

###################################################################
#                                                                 #
# -- CREATE MODULE TAXI                                           #
#                                                                 #
###################################################################

MODULE_TAXI () {

    INIT_MODULE_CREATION ${MODULEDIR}/TAXI ${versionTAXI}-${versionALIROOT}

    cat <<EOF > ${MODULEDIR}/TAXI/${versionTAXI}-${versionALIROOT}
#%Module1.0#####################################################################
##
## HLT - versions modulefile
##

proc ModulesHelp { } {
        global version
	puts stderr "This module is a module of the HLT for the TAXI."
}

set 	version 	1.4

module-whatis	"TAXI versions module for the HLT "

####################################################

## -- TAXI --
setenv          HLTTAXI_TOPDIR         $::env(HLTTAXI_BASEDIR)/${versionTAXI}-${versionALIROOT}
EOF

}