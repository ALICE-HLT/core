###################################################################
#                                                                 #
# -- CHECKOUT DATA TRANSPORT FRAMEWORK                            #
#                                                                 #
###################################################################

CHECKOUT_DATATRANSPORT () {

    INFO CHECKOUT "Data-Transport  : ${versionDATATRANSPORT}"
    
    sleep ${SLEEPTIME}
    
    pushd ${HLT_BASEDIR}/installation/archive > /dev/null
    if [ ! -f "HLT-${typeDATATRANSPORT}-${versionDATATRANSPORT}.tar.bz2" ] ; then
	wget ${QUIET_WGET} http://www.kip.uni-heidelberg.de/ti/HLT/software/download/HLT-${typeDATATRANSPORT}-${versionDATATRANSPORT}.tar.bz2
	if [ $? -ne 0 ] ; then
	    ERROR_HANDLER --error="wget failed to get DataTransport version ${versionDATATRANSPORT}."
	fi
    fi
    popd > /dev/null
    
    pushd ${HLT_BASEDIR}/data-transport > /dev/null
    
    tar xjf $HLT_BASEDIR/installation/archive/HLT-${typeDATATRANSPORT}-${versionDATATRANSPORT}.tar.bz2
    
    if [ "HLT-${typeDATATRANSPORT}-${versionDATATRANSPORT}" != "${targetDATATRANSPORT}" ]; then
	mv HLT-${typeDATATRANSPORT}-${versionDATATRANSPORT} ${targetDATATRANSPORT}
    fi

    popd > /dev/null
    
    ADD_TO_REMOVE ${HLT_BASEDIR}/data-transport/${targetDATATRANSPORT}
}

###################################################################
#                                                                 #
# -- COMPILE DATA TRANSPORT FRAMEWORK                             #
#                                                                 #
###################################################################

COMPILE_DATATRANSPORT () {

    INFO COMPILE "Data-Transport  : ${versionDATATRANSPORT}"

    sleep ${SLEEPTIME}

    # debug version
    M_BUILD_TYPE="debug"
    
    COMPILE_DATATRANSPORT_INTERNAL

    # prod version
    M_BUILD_TYPE="prod"
    
    COMPILE_DATATRANSPORT_INTERNAL
}

###################################################################
#                                                                 #
# -- COMPILE DATA TRANSPORT FRAMEWORK  -- INTERNAL --             #
#                                                                 #
###################################################################

COMPILE_DATATRANSPORT_INTERNAL () {
    
    module unload HLT  #2> /dev/null
    module load HLT/${versionHLT}-${M_BUILD_TYPE} # 2> /dev/null
    module unload HLTANALYSIS #2> /dev/null

    pushd ${ALIHLT_DC_DIR}/src > /dev/null

    echo "#!/bin/bash" > ${HOME}/tmp.sh
    echo "export ALIHLT_LIBDIR=${ALICE_ROOT}/lib/tgt_${ALICE_TARGET_EXT}" > ${HOME}/tmp.sh
    echo "export ALIHLT_TOPDIR=${ALICE_ROOT}/HLT" >> ${HOME}/tmp.sh
    chmod 777  ${HOME}/tmp.sh
    . ${HOME}/tmp.sh
    rm ${HOME}/tmp.sh
    
    MOPT="${MAKEOPT}"
    
    if [ ${M_BUILD_TYPE} = "prod" ] ; then
	MOPT="${MOPT} PRODUCTION=1"
	IOPT="-production"
    fi

    LOG_HANDLER --log="compilelog-DATA_TRANSPORT-${IDATE}-${M_BUILD_TYPE}"
    eval make ${MOPT} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
    ERROR_HANDLER

    LOG_HANDLER --log="installlog-DATA_TRANSPORT-${IDATE}-${M_BUILD_TYPE}"
    eval ./install.sh ${IOPT} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
    ERROR_HANDLER
    
    ## FIX for SimpleChainConfig2

    pushd SimpleChainConfig2 > /dev/null
    chmod +x *.py
    popd > /dev/null

    ## FIX for SimpleChainConfig2
 
    popd > /dev/null
   
    ## Link to INFOLOGGER lib

    pushd ${ALIHLT_DC_DIR}/lib/${HLT_TARGET}  > /dev/null

    if [[ ! -h libInfoLoggerLogServer.so && "${clusterUSAGE}" == "1" ]] ; then
	ln -s /opt/HLT/tools/lib/libInfoLoggerLogServer.so libInfoLoggerLogServer.so
    fi
    
    popd > /dev/null
}

###################################################################
#                                                                 #
# -- MODULE DATA TRANSPORT FRAMEWORK                              #
#                                                                 #
###################################################################

MODULE_DATATRANSPORT () {

    INIT_MODULE_CREATION ${MODULEDIR}/DATATRANSPORT ${versionDATATRANSPORT}

    cat <<EOF > ${MODULEDIR}/DATATRANSPORT/${versionDATATRANSPORT}
#%Module1.0#####################################################################
##
## HLT - DATATRANSPORT modulefile
##

proc ModulesHelp { } {
        global version
	puts stderr "This module is a module of the HLT for the DATATRANSPORT."
}

set 	version 	1.2

module-whatis	"DATATRANSPORT versions module for the HLT "

####################################################

setenv		ALIHLT_DC_DIR	   $::env(DC_BASEDIR)/${targetDATATRANSPORT}
setenv		ALIHLT_DC_BINDIR   $::env(ALIHLT_DC_DIR)/bin/$::env(HLT_TARGET)
setenv		ALIHLT_DC_LIBDIR   $::env(ALIHLT_DC_DIR)/lib/$::env(HLT_TARGET)

prepend-path	PATH		   $::env(ALIHLT_DC_DIR)/bin/$::env(HLT_TARGET)
prepend-path	LD_LIBRARY_PATH	   $::env(ALIHLT_DC_DIR)/lib/$::env(HLT_TARGET)
EOF

}