
###################################################################
#                                                                 #
# -- TESTMODE ALIROOT                                             #
#                                                                 #
###################################################################

TESTMODE_ALIROOT () {

    INFO TESTMODE "ALIROOT  : ${versionALIROOT}"

    if [ "${versionCoALIROOT}" == "head" ] ; then
	export OPTION_CHECKOUT="https://alisoft.cern.ch/AliRoot/trunk -r {${YESTERDAY}}"
    else
	if [ "${typeALIROOT}" == "--tag=" ] ; then
	    export OPTION_CHECKOUT="https://alisoft.cern.ch/AliRoot/tags/${versionALIROOT} -r {${YESTERDAY}}"
	elif [ "${typeALIROOT}" == "--branch=" ] ; then
	    export OPTION_CHECKOUT="https://alisoft.cern.ch/AliRoot/branches/${versionALIROOT} -r {${YESTERDAY}}"	    
	fi
    fi

    export OPTION_TRACK="Experimental"
    export OPTION_BUILD=""
      
    # -- debug version
    M_BUILD_TYPE="debug"
    
    TESTMODE_ALIROOT_INTERNAL

    # -- prod version
    M_BUILD_TYPE="prod"
    
    TESTMODE_ALIROOT_INTERNAL
 
    unset OPTION_CHECKOUT
    unset OPTION_TRACK
    unset OPTION_BUILD
 
    ADD_TO_REMOVE ${HLT_BASEDIR}/root/${targetROOT}
}

###################################################################
#                                                                 #
# -- TESTMODE ALIROOT  -- INTERNAL --                             #
#                                                                 #
###################################################################

TESTMODE_ALIROOT_INTERNAL () {

    module unload HLT  2> /dev/null
    module load HLT/${versionHLT}-${M_BUILD_TYPE}  2> /dev/null

    export OPTION_MODULE="${MODULEDIR}/HLT/${versionHLT}-${M_BUILD_TYPE}"
    
    ${CTEST} -VV -S ${HLTTESTSERVER_BASEDIR}/config/CTestConfig/aliroot/aliroot.cmake
    
    unset OPTION_MODULE
    
    module unload HLT 2> /dev/null
}

###################################################################
#                                                                 #
# -- CHECKOUT ALIROOT / ROOT / GEANT3                             #
#                                                                 #
###################################################################

CHECKOUT_ALIROOT () {

    INFO CHECKOUT "aliroot : ${versionALIROOT}"

    if [ ${coROOT} ] ; then
	INFO CHECKOUT "root    : ${versionROOT}"
    fi

    if [ ${coGEANT3} ] ; then
	INFO CHECKOUT "geant3  : ${versionGEANT3}"
    fi
    
    sleep ${SLEEPTIME}

    pushd ${HLT_BASEDIR}/installation/current/bin > /dev/null

    LOG_HANDLER --log="checkoutlog-ALIROOT-${IDATE}"

    # co ROOT
    if [ ${coROOT} ] ; then
	    
	# co GEANT3
	if [ ${coGEANT3} ] ; then
            # *** co aliroot / root / geant3 ***
	    if [ ${versionALIROOT} == "HEAD_${IDATE}" ] ; then
		versionALIROOT=HEAD_${IDATE}
		eval ./coAliRoot.csh --https \
		    --basedir=${HLT_BASEDIR}/aliroot \
		    --root_basedir=${HLT_BASEDIR}/root \
		    --geant3_basedir=${HLT_BASEDIR}/geant3 \
		    --root_version=${versionCoROOT} \
		    --geant3_version=${versionCoGEANT3} \
		    --extension=${versionALIROOT} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	    else
		eval ./coAliRoot.csh --https \
		    ${typeALIROOT}${versionALIROOT} \
		    --basedir=${HLT_BASEDIR}/aliroot \
		    --root_basedir=${HLT_BASEDIR}/root \
		    --geant3_basedir=${HLT_BASEDIR}/geant3 \
		    --root_version=${versionCoROOT} \
		    --geant3_version=${versionCoGEANT3} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	    fi

	    ADD_TO_REMOVE ${GEANT3_BASEDIR}/${targetGEANT3}
	    ADD_TO_REMOVE ${GEANT3_BASEDIR}/${targetALIROOT}
	    ADD_TO_REMOVE ${GEANT3_BASEDIR}/libs_${targetROOT}

	# ! co GEANT3
	else
	    # *** co aliroot / root ***
	    if [ ${versionALIROOT} == "HEAD_${IDATE}" ] ; then
		versionALIROOT=HEAD_${IDATE}
		eval ./coAliRoot.csh --https \
		    --basedir=${HLT_BASEDIR}/aliroot \
		    --root_basedir=${HLT_BASEDIR}/root \
		    --root_version=${versionCoROOT} \
		    --extension=${versionALIROOT} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	    else
		eval ./coAliRoot.csh --https \
		    ${typeALIROOT}${versionALIROOT} \
		    --basedir=${HLT_BASEDIR}/aliroot \
		    --root_basedir=${HLT_BASEDIR}/root \		   
		    --root_version=${versionCoROOT} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	    fi

	    # *** create links geant3 ***
	    pushd $GEANT3_BASEDIR > /dev/null
	    mkdir -p libs_${targetROOT}/lib
	    mkdir -p ${targetALIROOT}
	    cd ${targetALIROOT}
	    ln -s ../${targetGEANT3} Geant3_version
	    ln -s ../libs_${targetROOT}/lib lib
	    ln -s ../${targetGEANT3}/TGeant3 TGeant3
	    popd > /dev/null

	    ADD_TO_REMOVE ${GEANT3_BASEDIR}/${targetALIROOT}
	    ADD_TO_REMOVE ${GEANT3_BASEDIR}/libs_${targetROOT}
	fi	
	
	ADD_TO_REMOVE ${ROOT_BASEDIR}/${targetROOT}
	ADD_TO_REMOVE ${ROOT_BASEDIR}/${targetALIROOT}
	
    # ! coROOT
    else
	# *** aliroot ***
	if [ ${versionALIROOT} == "HEAD_${IDATE}" ] ; then
	    versionALIROOT=HEAD_${IDATE}
	    eval ./coAliRoot.csh --https \
		--basedir=${HLT_BASEDIR}/aliroot \
		--extension=${versionALIROOT} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	else
	    eval ./coAliRoot.csh --https \
		${typeALIROOT}${versionALIROOT} \
		--basedir=${HLT_BASEDIR}/aliroot ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	fi
	
	# *** create links root / geant3 ***
	pushd $ROOT_BASEDIR > /dev/null
	ln -s ${targetROOT} ${targetALIROOT}
	popd > /dev/null
	
	pushd $GEANT3_BASEDIR > /dev/null
	mkdir -p libs_${targetROOT}/lib
	mkdir -p ${targetALIROOT}
	cd ${targetALIROOT}
	ln -s ../${targetGEANT3} Geant3_version
	ln -s ../libs_${targetROOT}/lib lib
	ln -s ../${targetGEANT3}/TGeant3 TGeant3
	popd > /dev/null

	ADD_TO_REMOVE ${ROOT_BASEDIR}/${targetALIROOT}
	ADD_TO_REMOVE ${GEANT3_BASEDIR}/${targetALIROOT}
	ADD_TO_REMOVE ${GEANT3_BASEDIR}/libs_${targetROOT}
	
    fi

    ERROR_HANDLER

    ADD_TO_REMOVE ${ALICE_BASEDIR}/${targetALIROOT}

    popd > /dev/null
}

###################################################################
#                                                                 #
# -- COMPILE ALIROOT / ROOT / GEANT3                              #
#                                                                 #
###################################################################

COMPILE_ALIROOT () {

    INFO COMPILE "aliroot : ${versionALIROOT}"

    if [ ${enableROOT} ] ; then
	INFO COMPILE "root    : ${versionROOT}"
    fi

    if [ ${enableGEANT3} ] ; then
	INFO COMPILE "geant3  : ${versionGEANT3}"
    fi
    
    sleep ${SLEEPTIME}

    # debug version
    M_BUILD_TYPE="prod"

    COMPILE_ALIROOT_INTERNAL

    # prod version
    M_BUILD_TYPE="debug"

    COMPILE_ALIROOT_INTERNAL
}

###################################################################
#                                                                 #
# -- COMPILE ALIROOT / ROOT / GEANT3  -- INTERNAL --              #
#                                                                 #
###################################################################

COMPILE_ALIROOT_INTERNAL () {

    pushd ${HLT_BASEDIR}/installation/current/bin > /dev/null

    module unload HLT 2> /dev/null
    module load HLT/${versionHLT}-${M_BUILD_TYPE} 2> /dev/null
    module unload HLTANALYSIS 2> /dev/null
    module unload ECSPROXY 2> /dev/null

    # Set O3 in aliroot makefile
    if [ -f $ALICE_ROOT/build/Makefile.${ALICE_TARGET} ] ; then 
	if [ ! -f $ALICE_ROOT/build/Makefile.${ALICE_TARGET}.org ] ; then 
	    mv  $ALICE_ROOT/build/Makefile.${ALICE_TARGET} \
		$ALICE_ROOT/build/Makefile.${ALICE_TARGET}.org  
	    sed -e "s/OPT           =.*/OPT           = -O3 -g -DLOG_NO_DEBUG/1" \
		$ALICE_ROOT/build/Makefile.${ALICE_TARGET}.org \
		> $ALICE_ROOT/build/Makefile.${ALICE_TARGET}
	fi
    fi

    LOG_HANDLER --log="compilelog-ALIROOT-${M_BUILD_TYPE}-${IDATE}"

    OPT="--version=${targetALIROOT} --make='${MAKEOPT}'"

    if [ ${M_BUILD_TYPE} = "debug" ] ; then
	OPT="$OPT --debug"
    fi
    
    if [ "${enableROOT}" == "1" ] ; then 
	OPT="$OPT --all"

	if [ ${ALIEN_ROOT} ] ; then
	    eval ./compileAliRootVersion.csh $OPT \
		--basedir=${HLT_BASEDIR}/aliroot \
		--root_basedir=${HLT_BASEDIR}/root \
		--geant3_basedir=${HLT_BASEDIR}/geant3 \
		--root_configure=\"--enable-table --enable-alien --with-alien-incdir=${ALIEN_ROOT}/api/include --with-alien-libdir=${ALIEN_ROOT}/api/lib --enable-ssl --with-ssl-incdir=${ALIEN_ROOT}/include --with-ssl-libdir=${ALIEN_ROOT}/lib --with-f77=${OPT_F77} --enable-globus --with-pythia6-uscore=SINGLE\" ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	else
	    eval ./compileAliRootVersion.csh $OPT \
		--basedir=${HLT_BASEDIR}/aliroot \
		--root_basedir=${HLT_BASEDIR}/root \
		--geant3_basedir=${HLT_BASEDIR}/geant3 \
		--root_configure=\"--with-pythia6-uscore=SINGLE --enable-table --with-f77=${OPT_F77}\" ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	fi
    else
	eval ./compileAliRootVersion.csh $OPT --basedir=${HLT_BASEDIR}/aliroot ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
    fi
    
    ERROR_HANDLER

    popd > /dev/null
}

###################################################################
#                                                                 #
# -- CREATE MODULE ALIROOT / GEANT3                               #
#                                                                 #
###################################################################

MODULE_ALIROOT () {
 
    INIT_MODULE_CREATION ${MODULEDIR}/ALIROOT ${versionALIROOT}

    cat <<EOF > ${MODULEDIR}/ALIROOT/${versionALIROOT}
#%Module1.0#####################################################################
##
## HLT - ALIROOT modulefile
##

proc ModulesHelp { } {
        global version
        puts stderr "This module is a module of the HLT for ALIROOT."
}

set 	version 	1.2

module-whatis	"ALIROOT versions module for the HLT"

## -- VERSION --
setenv		ALIROOT_RELEASE	${versionALIROOT}

## -- ALICE --
setenv		ALICE		$::env(ALICE_BASEDIR)

## -- ALIROOT --
setenv		ALICE_ROOT	$::env(ALICE_BASEDIR)/AliRoot_$::env(ALIROOT_RELEASE)

prepend-path	PATH		$::env(ALICE_ROOT)/bin/tgt_$::env(ALICE_TARGET_EXT)
prepend-path	LD_LIBRARY_PATH	$::env(ALICE_ROOT)/lib/tgt_$::env(ALICE_TARGET_EXT)

EOF

}
