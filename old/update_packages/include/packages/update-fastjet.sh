###################################################################
#                                                                 #
# -- TESTMODE FASTJET                                             #
#                                                                 #
###################################################################

TESTMODE_FASTJET () {

    INFO TESTMODE "FASTJET  : ${versionFASTJET}"


}

###################################################################
#                                                                 #
# -- CHECKOUT FASTJET                                             #
#                                                                 #
###################################################################

CHECKOUT_FASTJET () {

    return

    INFO CHECKOUT "FASTJET  : ${versionFASTJET}" 

    sleep ${SLEEPTIME}
    
    pushd ${HLT_BASEDIR}/analysis > /dev/null
    
    LOG_HANDLER --log="checkoutlog-HLT-${IDATE}"

    if [ ${modeTEST} ] ; then 
	if [ ${versionHLT} = "HEAD_${IDATE}" ] ; then
	    export OPTION_CHECKOUT="https://alisoft.cern.ch/AliRoot/trunk/HLT -r {${YESTERDAY}}"
	else
	    export OPTION_CHECKOUT="https://alisoft.cern.ch/AliRoot/branches/${versionHLT}/HLT -r {${YESTERDAY}}"
	fi
	export OPTION_TRACK="Experimental"
    else
	if [ ${versionHLT} = "HEAD_${IDATE}" ] ; then
	    eval svn co https://alisoft.cern.ch/AliRoot/trunk/HLT ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	else
	    eval svn co https://alisoft.cern.ch/AliRoot/branches/${versionHLT}/HLT ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	fi

	ERROR_HANDLER  

	mv HLT ${targetHLT}
    fi

    popd > /dev/null
    
    ADD_TO_REMOVE ${HLT_BASEDIR}/analysis/${targetHLT}
}

###################################################################
#                                                                 #
# -- COMPILE FASTJET                                              #
#                                                                 #
###################################################################

COMPILE_FASTJET () {

    return

    INFO COMPILE "FASTJET  : ${versionFASTJET}"
    
    sleep ${SLEEPTIME}
    
    # debug version
    M_BUILD_TYPE="debug"
    
    COMPILE_HLTANALYSIS_INTERNAL

    # prod version
    M_BUILD_TYPE="prod"
    
    COMPILE_HLTANALYSIS_INTERNAL
}

###################################################################
#                                                                 #
# -- COMPILE HLT ANALYSIS  -- INTERNAL --                         #
#                                                                 #
###################################################################

COMPILE_FASTJET_INTERNAL () {
    


    module unload HLT  2> /dev/null
    module load HLT/${versionHLT}-${M_BUILD_TYPE}  2> /dev/null

    if [ ${modeTEST} ] ; then 
	
	if [ ${M_BUILD_TYPE} = "prod" ] ; then
	    export OPTION_CONFIGURE="--disable-debug --enable-optimization=3 --prefix ${ALIHLT_TOPDIR}/build-${M_BUILD_TYPE} --enable-doc=mono"
	else
	    export OPTION_CONFIGURE="--enable-debug --disable-doc --disable-optimization --prefix ${ALIHLT_TOPDIR}/build-${M_BUILD_TYPE} "
	fi
	
	export OPTION_BUILD=${MAKEOPT}
	
	export OPTION_BUILD_INSTALL=${MAKEOPT}

	${CTEST} -VV -S ${HLTTESTSERVER_BASEDIR}/config/CTestConfig/analysis/analysis.cmake

	unset OPTION_CHECKOUT
	unset OPTION_CONFIGURE
	unset OPTION_TRACK
	unset OPTION_BUILD
	unset OPTION_BUILD_INSTALL

    else
	pushd ${ALIHLT_TOPDIR} > /dev/null

	if [ ${M_BUILD_TYPE} = "debug" ] ; then
	    LOG_HANDLER --log="autoreconf-HLT-${IDATE}"
	    eval autoreconf -f -i ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	    ERROR_HANDLER
	fi
	
	if [ ! -d "${ALIHLT_TOPDIR}/build-${M_BUILD_TYPE}" ] ; then
	    mkdir -p ${ALIHLT_TOPDIR}/build-${M_BUILD_TYPE}
	fi
	
	cd ${ALIHLT_TOPDIR}/build-${M_BUILD_TYPE}
	
	if [ ${M_BUILD_TYPE} = "prod" ] ; then
	    OPT="--disable-debug --enable-optimization=3 --prefix ${ALIHLT_TOPDIR}/build-${M_BUILD_TYPE} --enable-doc=mono"
	else
	    OPT="--enable-debug --disable-doc --disable-optimization --prefix ${ALIHLT_TOPDIR}/build-${M_BUILD_TYPE} "
	fi
	
	LOG_HANDLER --log="configure-HLT-${M_BUILD_TYPE}-${IDATE}"
	eval ../configure ${OPT} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	ERROR_HANDLER 
	
	LOG_HANDLER --log="make-HLT-${M_BUILD_TYPE}-${IDATE}"
	eval make ${MAKEOPT} ${REDIRECT_A} ${LOG_TARGET}  ${REDIRECT_B}
        ERROR_HANDLER
	
	LOG_HANDLER --log="make-install-HLT-${M_BUILD_TYPE}-${IDATE}"
	eval make install ${MAKEOPT} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	ERROR_HANDLER

	popd > /dev/null
    fi

    module unload HLT 2> /dev/null
}

###################################################################
#                                                                 #
# --  CREATE MODULE HLT ANALYSIS                                  #
#                                                                 #
###################################################################

MODULE_FASTJET () {

    return

    INIT_MODULE_CREATION ${MODULEDIR}/HLTANALYSIS ${versionHLT}
    
    cat <<EOF > ${MODULEDIR}/HLTANALYSIS/${versionHLT}
#%Module1.0#####################################################################
##
## HLT - versions modulefile
##

proc ModulesHelp { } {
        global version
	puts stderr "This module is HLT analysis module of the HLT."
}

set 	version 	1.2

module-whatis	"HLT versions module for the HLT "

####################################################

## -- HLT - ALIROOT --
setenv		ALIHLT_TOPDIR	${HLT_BASEDIR}/analysis/HLT-${versionHLT}

setenv		ALIHLT_BASEDIR	$::env(ALIHLT_TOPDIR)
setenv		ALIHLT_LIBDIR	$::env(ALIHLT_BASEDIR)/build-$::env(HLTBUILDTYPE)/lib
prepend-path	LD_LIBRARY_PATH	$::env(ALIHLT_LIBDIR)
EOF

}