###################################################################
#                                                                 #
# -- CHECKOUT INSTALLATION                                        #
#                                                                 #
###################################################################

CHECKOUT_INSTALLATION () {

    INFO CHECKOUT "Installation  : ${versionINSTALLATION}"
    
    sleep ${SLEEPTIME}
    
    pushd ${HLT_BASEDIR}/installation > /dev/null

    pushd archive > /dev/null

    if [ ! -f "${targetINSTALLATION}.tar.gz" ] ; then
	wget ${QUIET_WGET} http://www.ift.uib.no/~kjeks/download/${targetINSTALLATION}.tar.gz
	if [ $? -ne 0 ] ; then
	    ERROR_HANDLER --error="wget failed to get aliroot installer version ${versionINSTALLATION}."
	fi
    else
	if [ "${versionINSTALLATION}" == "HEAD" ] ; then
	    mv ${targetINSTALLATION}.tar.gz ${targetINSTALLATION}.tar.gz.before-${IDATE}
	    wget ${QUIET_WGET} http://www.ift.uib.no/~kjeks/download/${targetINSTALLATION}.tar.gz
	    if [ $? -ne 0 ] ; then
		ERROR_HANDLER --error="wget failed to get aliroot installer version ${versionINSTALLATION}."
	    fi
	fi
    fi

    popd > /dev/null
    
    if [ "${versionINSTALLATION}" == "HEAD" ] ; then
	mkdir tmp_${IDATE}

	pushd tmp_${IDATE} > /dev/null
	tar xzf archive/${targetINSTALLATION}.tar.gz
	TMP_INSTALLATION=`ls`	
	popd > /dev/null

	if [ -f "${TMP_INSTALLATION}" ] ; then
	    rm -rf tmp_${IDATE}
	else
	    mv tmp_${IDATE}/${TMP_INSTALLATION} .
	    rm -rf tmp_${IDATE}
	    enableINSTALLATION=1
	fi
    else
	tar xzf archive/${targetINSTALLATION}.tar.gz
	enableINSTALLATION=1
    fi
    
    if [ ${enableINSTALLATION} ] ; then
    
	if [ -h current ] ; then
	    rm current
	fi
	
	ln -s ${targetINSTALLATION} current
	
	pushd current > /dev/null
		
	LOG_HANDLER --log="installerlog-CONFIGURE-${IDATE}"
	eval ./configure --prefix=${HLT_BASEDIR}/installation/current \
	    --with-aliroot-basedir=${ALICE_BASEDIR} \
	    --with-root-basedir=${ROOT_BASEDIR} \
	    --with-geant3-basedir=${GEANT3_BASEDIR} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	ERROR_HANDLER
	
	LOG_HANDLER --log="installerlog-MAKE-${IDATE}"
	eval make ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	ERROR_HANDLER

	LOG_HANDLER --log="installerlog-MAKE_INSTALL-${IDATE}"
	eval make install ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	ERROR_HANDLER

	popd > /dev/null

	ADD_TO_REMOVE ${HLT_BASEDIR}/installation/current
	ADD_TO_REMOVE ${HLT_BASEDIR}/installation/${targetINSTALLATION}
    fi

    popd > /dev/null
}


###################################################################
#                                                                 #
# -- CREATE MODULE BASE                                           #
#                                                                 #
###################################################################

MODULE_BASE () {

    INIT_MODULE_CREATION ${MODULEDIR}/BASE ${versionBASE}

    if [ ! -f "${MODULEDIR}/BASE/${versionBASE}" ] ; then 
	cat <<EOF > ${MODULEDIR}/BASE/${versionBASE}	
#%Module1.0#####################################################################
##
## HLT - BASE modulefile
##

proc ModulesHelp { } {
        global version
	puts stderr "This module is the BASE module of the HLT."
}

set 	version 	1.3

module-whatis	"BASE module for the HLT"

## -- BASE DIRECTORYS --
setenv		ALICE_BASEDIR	           ${HLT_BASEDIR}/aliroot
setenv          ALIEN_BASEDIR     	   ${HLT_BASEDIR}/alien
setenv		ROOT_BASEDIR	           ${HLT_BASEDIR}/root
setenv		GEANT3_BASEDIR	           ${HLT_BASEDIR}/geant3
setenv		FASTJET_BASEDIR	           ${HLT_BASEDIR}/fastjet
setenv		DC_BASEDIR	           ${HLT_BASEDIR}/data-transport
setenv          HLTECS_BASEDIR             ${HLT_BASEDIR}/interfaces/ecs-proxy
setenv          HLTTAXI_BASEDIR            ${HLT_BASEDIR}/interfaces/taxi
setenv          HLTPENDOLINO_BASEDIR       ${HLT_BASEDIR}/interfaces/pendolino
setenv          HLTHCDBMANAGER_BASEDIR     ${HLT_BASEDIR}/interfaces/HCDBManager
setenv		HLTCONTROL_DIR	           ${HLT_BASEDIR}/control
setenv		HLTCONTROL_BASEDIR	   ${HLT_BASEDIR}/control/releases

setenv          ALIHLT_HCDBDIR             /opt/HCDB
setenv          ALIHLT_T_HCDBDIR           /opt/T-HCDB

setenv          HLTTESTSERVER_BASEDIR      /afs/.alihlt.cern.ch/testServer
setenv          HLTTEST_BASEDIR            /opt/HLT-test

## -- SET TOOLS DIRECTORYS --
append-path     PATH                       /opt/HLT/tools/bin
append-path     LD_LIBRARY_PATH            /opt/HLT/tools/lib

## -- XERCES --
setenv		XERCESCDIR	           /usr/

## -- QT --
setenv		QTDIR		           /usr/share/qt3

prepend-path	PATH		           $::env(QTDIR)/bin
prepend-path	LD_LIBRARY_PATH	           $::env(QTDIR)/lib
EOF

        if [ "$clusterUSAGE" == "1" ] ; then
	    cat <<EOF >> ${MODULEDIR}/BASE/${versionBASE}	
## -- Info Logger --
setenv          HLTINFOLOGGER_LIBDIR       ${HLT_BASEDIR}/tools/lib
setenv          DATE_SITE_LOGS             /tmp/infoLogger
setenv          DATE_INFOLOGGER_LOGHOST    infologger
setenv          DATE_SITE                  standalone
setenv          DATE_SOCKET_INFOLOG_RX     6001
setenv          DATE_SOCKET_INFOLOG_TX     6002
append-path     LD_LIBRARY_PATH            /usr/local/lib

## -- Repositories --
setenv		HLTSVN_BASEDIR		   file:///afs/.alihlt.cern.ch/repository/svn
setenv		HLTSVN_BASEURL		   svn+ssh://dev0/afs/.alihlt.cern.ch/repository/svn

setenv          HLTGIT_BASEDIR		   ssh://dev0/afs/.alihlt.cern.ch/repository/git
setenv          HLTGIT_BASEURL		   file:///afs/.alihlt.cern.ch/repository/git
EOF
        else	
	cat <<EOF >> ${MODULEDIR}/BASE/${versionBASE}
## -- Repositories --
setenv		HLTSVN_BASEDIR		   svn+ssh://alihlt-gw1.cern.ch/afs/.alihlt.cern.ch/repository/svn
setenv		HLTSVN_BASEURL		   svn+ssh://alihlt-gw1.cern.ch/afs/.alihlt.cern.ch/repository/svn
EOF

        fi
    fi
}

###################################################################
#                                                                 #
# -- CREATE MODULE DEFINITION                                     #
#                                                                 #
###################################################################

MODULE_DEFINITION () {

    INIT_MODULE_CREATION ${MODULEDIR}/DEFINITION ${versionDEFINITION}

    if [ ! -f "${MODULEDIR}/DEFINITION/${versionDEFINITION}" ] ; then 
	cat <<EOF > ${MODULEDIR}/DEFINITION/${versionDEFINITION}
#%Module1.0#####################################################################
##
## HLT - DEFINITION modulefile
##

proc ModulesHelp { } {
        global version
	puts stderr "This module is a the DEFINITION module of the HLT."
}

set 	version 	1.0

module-whatis	"DEFINITION module for the HLT "

## ----------------
## -- BASE PORTS --
## ----------------
setenv		ALIHLT_PORT_BASE_RUNCONTROL             20000

setenv		ALIHLT_DETECTOR_IDENTIFIER_ALICE            0
setenv		ALIHLT_DETECTOR_IDENTIFIER_TPC              1
setenv		ALIHLT_DETECTOR_IDENTIFIER_TRD              2
setenv		ALIHLT_DETECTOR_IDENTIFIER_PHOS             3
setenv		ALIHLT_DETECTOR_IDENTIFIER_DIMUON           4
setenv		ALIHLT_DETECTOR_IDENTIFIER_ITS              5
setenv		ALIHLT_DETECTOR_IDENTIFIER_DEV              6
setenv		ALIHLT_DETECTOR_IDENTIFIER_TEST             7
setenv		ALIHLT_DETECTOR_IDENTIFIER_EMCAL            8

setenv          ALIHLT_PORTRANGE_RUNMGR 		  100
setenv          ALIHLT_PORTRANGE_DETECTOR 		 2000

## ---------------------
## -- PENDOLINO PORTS --
## ---------------------
setenv		ALIHLT_PORTOFFSET_PEND_1   		    1
setenv		ALIHLT_PORTOFFSET_PEND_2   		    2
setenv		ALIHLT_PORTOFFSET_PEND_3   		    3
setenv		ALIHLT_PORTOFFSET_PEND_SL   		    4
setenv		ALIHLT_PORTOFFSET_PEND_MGR   		    5

## ---------------------
## -- ECS PROXY PORTS --
## ---------------------
setenv		ALIHLT_PORTOFFSET_ECSLIB   		   10
EOF
    fi
}

###################################################################
#                                                                 #
# -- CREATE MODULE HLT                                            #
#                                                                 #
###################################################################

MODULE_HLT () {

    INIT_MODULE_CREATION ${MODULEDIR}/HLT ${MODULE_HLT_VERSION}-${M_BUILD_TYPE}

    cat <<EOF > ${MODULEDIR}/HLT/${MODULE_HLT_VERSION}-${M_BUILD_TYPE}
#%Module1.0#####################################################################
##
## HLT - versions modulefile
##

proc ModulesHelp { } {
        global version
	puts stderr "This module is THE module of the HLT."
}

set 	version 	1.3

module-whatis	"HLT versions module for the HLT "

####################################################
set 	hlt_build_type		${M_BUILD_TYPE}
set	hlt_version		${versionHLT}

set	base_version		${versionBASE}
set     definition_version      ${versionDEFINITION}
set     alien_version           ${versionALIEN}
set	aliroot_version		${versionALIROOT}
set     geant_version           ${versionGEANT3}
set	root_version		${versionROOT}
set	fastjet_version		${versionFASTJET}
set	data_transport_version	${versionDATATRANSPORT}
EOF

    if [ "$clusterUSAGE" == "1" ] ; then
	cat <<EOF >> ${MODULEDIR}/HLT/${MODULE_HLT_VERSION}-${M_BUILD_TYPE}
set     ecsproxy_version        ${versionECSPROXY}
set     taxi_version            ${versionTAXI}
set     pendolino_version       ${versionPENDOLINO}
set     hcdbmanager_version     ${versionHCDBMANAGER}
set     control_version         ${versionCONTROL}
EOF
    fi

    cat <<EOF >> ${MODULEDIR}/HLT/${MODULE_HLT_VERSION}-${M_BUILD_TYPE}
####################################################

## set type to "prod" or "debug"
## -------------------------------------
setenv  HLTBUILDTYPE	\$hlt_build_type
## -------------------------------------

set	osname		[uname sysname]
set 	osarchitecture	[uname machine]

if { \$osarchitecture == "x86_64" } {
 	setenv	ALICE_TARGET	linuxx8664gcc
        	
	if { \$hlt_build_type == "prod" } {
		setenv	HLT_TARGET	   \$osname-\$osarchitecture-release
                setenv	ALICE_TARGET_EXT   linuxx8664gcc
 	} else {
		setenv	HLT_TARGET	   \$osname-\$osarchitecture-\$::env(HLTBUILDTYPE)
                setenv	ALICE_TARGET_EXT   linuxx8664gccDEBUG
	}
} elseif { \$osarchitecture == "i686" } {
 	setenv	ALICE_TARGET	linux
        	
	if { \$hlt_build_type == "prod" } {
		setenv	HLT_TARGET	   \$osname-\$osarchitecture-release
                setenv	ALICE_TARGET_EXT   linux
 	} else {
		setenv	HLT_TARGET	   \$osname-\$osarchitecture-\$::env(HLTBUILDTYPE)
                setenv	ALICE_TARGET_EXT   linuxDEBUG
	}
} else {
	puts stderr "\tYour architecture is \$osarchitecture. Please use x86_64 or i686 architecture"
	break
}

if {[info exists env(MODULE_INFO_SUPRESS)]} {	
	set supress	1
} else {	
 	set supress	0
}

## Actions on load / unload
## ------------------------
if { [module-info mode load] } {
        if { \$supress == "0" } { 
   	    puts stderr "\tHLT version loaded: (GLOBAL)\t\$hlt_version-\$::env(HLTBUILDTYPE)"
	    puts stderr "\t\t      AliRoot version:\t\$aliroot_version"
            puts stderr "\t\t        Alien version:\t\$alien_version"
            puts stderr "\t\t         Root version:\t\$root_version"
            puts stderr "\t\t       Geant3 version:\t\$geant_version"
            puts stderr "\t\t      FastJet version:\t\$fastjet_version"
            puts stderr "\t\tDataTransport version:\t\$data_transport_version"
            puts stderr "\t\t HLT analysis version:\t\$hlt_version"
EOF
    
    if [ "$clusterUSAGE" == "1" ] ; then
	cat <<EOF >> ${MODULEDIR}/HLT/${MODULE_HLT_VERSION}-${M_BUILD_TYPE}
            puts stderr "\t\t    ECS proxy version:\t\$ecsproxy_version"
            puts stderr "\t\t         Taxi version:\t\$taxi_version"
            puts stderr "\t\t    Pendolino version:\t\$pendolino_version"
            puts stderr "\t\t  HCDBManager version:\t\$hcdbmanager_version"
            puts stderr "\t\t      Control version:\t\$control_version"
EOF
    fi
    
    cat <<EOF >> ${MODULEDIR}/HLT/${MODULE_HLT_VERSION}-${M_BUILD_TYPE}
        }

	module load BASE/\$base_version
	module load DEFINITION/\$definition_version
	module load ALIEN/\$alien_version
	module load FASTJET/\$fastjet_version
	module load ALIROOT/\$aliroot_version
	module load ROOT/\$root_version
	module load GEANT3/\$geant_version
	module load DATATRANSPORT/\$data_transport_version
EOF

    if [ "$clusterUSAGE" == "1" ] ; then
	cat <<EOF >> ${MODULEDIR}/HLT/${MODULE_HLT_VERSION}-${M_BUILD_TYPE}
        module load TAXI/\$taxi_version-\$aliroot_version
        module load ECSPROXY/\$ecsproxy_version-\$data_transport_version
        module load PENDOLINO/\$pendolino_version-\$aliroot_version
        module load HCDBMANAGER/\$hcdbmanager_version-\$aliroot_version
        module load CONTROL/\$control_version
EOF
    fi

    cat <<EOF >> ${MODULEDIR}/HLT/${MODULE_HLT_VERSION}-${M_BUILD_TYPE}
	if { ![is-loaded HLTdevel] } { 	module load HLTANALYSIS/\$hlt_version }

} elseif { [module-info mode remove] } {
        if { \$supress == "0" } { 
	    puts stderr "\tHLT version unloaded: (GLOBAL)\t\$hlt_version-$::env(HLTBUILDTYPE)" 
        }
EOF

    if [ "$clusterUSAGE" == "1" ] ; then
	cat <<EOF >> ${MODULEDIR}/HLT/${MODULE_HLT_VERSION}-${M_BUILD_TYPE}

        if { [is-loaded ECSPROXY] } { module unload ECSPROXY/\$ecsproxy_version-\$data_transport_version }
        if { [is-loaded TAXI] } { module unload TAXI/\$taxi_version-\$aliroot_version }
        if { [is-loaded PENDOLINO] } { module unload PENDOLINO/\$pendolino_version-\$aliroot_version }
        if { [is-loaded HCDBMANAGER] } { module unload HCDBMANAGER/\$hcdbmanager_version-\$aliroot_version }
        if { [is-loaded CONTROL] } { module unload CONTROL/\$control_version }
EOF
    fi
	cat <<EOF >> ${MODULEDIR}/HLT/${MODULE_HLT_VERSION}-${M_BUILD_TYPE}
	if { [is-loaded ALIROOT] } { module unload ALIROOT/\$aliroot_version }
	if { [is-loaded ROOT] } { module unload ROOT/\$root_version }
	if { [is-loaded GEANT3] } { module unload GEANT3/\$geant_version }
	if { [is-loaded FASTJET] } { module unload FASTJET/\$fastjet_version }
	if { [is-loaded DATATRANSPORT] } { module unload DATATRANSPORT/\$data_transport_version }
	if { [is-loaded HLTANALYSIS] } { module unload HLTANALYSIS/\$hlt_version }
	if { [is-loaded ALIEN] } { module unload ALIEN/\$alien_version }
	if { [is-loaded DEFINITION] } { module unload DEFINITION/\$definition_version }
	if { [is-loaded BASE] } { module unload BASE/\$base_version }
}       
EOF
 
}
