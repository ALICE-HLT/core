###################################################################
#                                                                 #
# -- TESTMODE PENDOLINO                                           #
#                                                                 #
###################################################################

TESTMODE_PENDOLINO () {
 
    INFO TESTMODE "PENDOLINO  : ${versionPENDOLINO}"
    
    SET_BASE_MODULES
    export OPTION_CHECKOUT="${HLTSVN_BASEURL}/interfaces/pendolino/tags/release-${versionPENDOLINO} -r {${YESTERDAY}}"
    export OPTION_TRACK="Experimental"
    export OPTION_BUILD=""
#${MAKEOPT}	
    UNSET_BASE_MODULES

    # prod version
    M_BUILD_TYPE="prod"
    
    module unload HLT  2> /dev/null
    module load HLT/${versionHLT}-${M_BUILD_TYPE}  2> /dev/null

    ${CTEST} -S ${HLTTESTSERVER_BASEDIR}/config/CTestConfig/pendolino/pendolino.cmake
    
    module unload HLT  2> /dev/null
    
    unset OPTION_CHECKOUT
    unset OPTION_TRACK
    unset OPTION_BUILD
    
    ADD_TO_REMOVE ${HLT_BASEDIR}/interfaces/pendolino/${targetPENDOLINO}

}

###################################################################
#                                                                 #
# -- CHECKOUT PENDOLINO                                           #
#                                                                 #
###################################################################

CHECKOUT_PENDOLINO () {
  
    INFO CHECKOUT "PENDOLINO  : ${versionPENDOLINO}"
    
    sleep ${SLEEPTIME}
    
    SET_BASE_MODULES

    if [ ${modeTEST} ] ; then 
	export OPTION_CHECKOUT="${HLTSVN_BASEURL}/interfaces/pendolino/tags/release-${versionPENDOLINO} -r {${YESTERDAY}}"
	export OPTION_TRACK="Experimental"
    else
	pushd ${HLT_BASEDIR}/interfaces/pendolino > /dev/null
    
	LOG_HANDLER --log="checkoutlog-PENDOLINO-${IDATE}"
	eval svn co ${HLTSVN_BASEURL}/interfaces/pendolino/tags/release-${versionPENDOLINO} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	ERROR_HANDLER

	mv release-${versionPENDOLINO} ${targetPENDOLINO}

	popd > /dev/null
    fi
    
    UNSET_BASE_MODULES
    
    ADD_TO_REMOVE ${HLT_BASEDIR}/interfaces/pendolino/${targetPENDOLINO}
}

###################################################################
#                                                                 #
# -- COMPILE PENDOLINO                                            #
#                                                                 #
###################################################################

COMPILE_PENDOLINO () {

    INFO COMPILE "PENDOLINO  : ${versionPENDOLINO}"
    
    sleep ${SLEEPTIME}
    
    # prod version
    M_BUILD_TYPE="prod"
    
    module unload HLT  2> /dev/null
    module load HLT/${versionHLT}-${M_BUILD_TYPE}  2> /dev/null

    if [ ${modeTEST} ] ; then 

	export OPTION_BUILD=${MAKEOPT}
	
	${CTEST} -VV -S ${HLTTESTSERVER_BASEDIR}/config/CTestConfig/pendolino/pendolino.cmake

	unset OPTION_CHECKOUT
	unset OPTION_TRACK
	unset OPTION_BUILD
    else
	pushd ${HLTPENDOLINO_TOPDIR} > /dev/null

	LOG_HANDLER --log="compilelog-PENDOLINO-${IDATE}"
	make ${MAKEOPT} 2>&1 | tee ${LOG_TARGET}
	ERROR_HANDLER
	
	popd > /dev/null	
    fi   
    
    module unload HLT  2> /dev/null
}

###################################################################
#                                                                 #
# -- CREATE MODULE PENDOLINO                                      #
#                                                                 #
###################################################################

MODULE_PENDOLINO () {

    INIT_MODULE_CREATION ${MODULEDIR}/PENDOLINO ${versionPENDOLINO}-${versionALIROOT}

    cat <<EOF > ${MODULEDIR}/PENDOLINO/${versionPENDOLINO}-${versionALIROOT}
#%Module1.0#####################################################################
##
## HLT - versions modulefile
##

proc ModulesHelp { } {
        global version
	puts stderr "This module is a module of the HLT for the PENDOLINO."
}

set 	version 	1.4

module-whatis	"PENDOLINO versions module for the HLT "

####################################################

## -- PENDOLINO --
setenv          HLTPENDOLINO_TOPDIR    $::env(HLTPENDOLINO_BASEDIR)/${versionPENDOLINO}-${versionALIROOT}
append-path     PATH                   $::env(HLTPENDOLINO_BASEDIR)/${versionPENDOLINO}-${versionALIROOT}/bin
EOF

}