 
###################################################################
#                                                                 #
# -- TOOL : Get Operator                                          #
#                                                                 #
###################################################################
GETOPERATOR () {
# Sets the "OPERATOR" variable
    case "$1" in
	ALICE )  OPERATOR=hlt    ;;
        TPC )    OPERATOR=tpc    ;;
	TRD )    OPERATOR=trd    ;;
	PHOS )   OPERATOR=phos   ;;
	DIMUON ) OPERATOR=dimuon ;;
	ITS )    OPERATOR=its    ;;
	DEV )    OPERATOR=dev    ;;
	TEST )   OPERATOR=test   ;;
        EMCAL )  OPERATOR=emcal  ;;
    esac
}
 
###################################################################
#                                                                 #
# -- PREPARE CECKOUT CONTROL                                      #
#                                                                 #
###################################################################

CHECKOUT_CONTROL_PREPARE () {
# update common directories

    if [[ "${ADMIN_USER}" == "1" && "${HLT_BASEDIR}" == "/opt/HLT" ]] ; then
	DETECTOR_LIST="ALICE TPC TRD PHOS DIMUON ITS DEV TEST EMCAL"
    else
	DETECTOR_LIST="DEV TEST"
    fi

    #---------------------------------------------------
    # -- create run directory and set access rights
    #---------------------------------------------------

    if [[ "${ADMIN_USER}" == "1" && "${HLT_BASEDIR}" == "/opt/HLT" ]] ; then
	if [ ! -d ${HLT_BASEDIR}/control/run ] ; then
	    fs mkm ${HLT_BASEDIR}/control/run cont.detector
	fi
    else
	if [ ! -d ${HLT_BASEDIR}/control/run ] ; then
	    mkdir ${HLT_BASEDIR}/control/run 
	fi
    fi

    for d in ${DETECTOR_LIST} ; do 
	GETOPERATOR $d
	    
	if [[ "${ADMIN_USER}" == "1" && "${HLT_BASEDIR}" == "/opt/HLT" ]] ; then
	    if [ ! -d ${HLT_BASEDIR}/control/run/$d ] ; then
		fs mkm ${HLT_BASEDIR}/control/run/$d cont.det.$d
	    fi
	    
	    fs sa ${HLT_BASEDIR}/control/run/$d -a ${OPERATOR}-operator write
	else
	    if [ ! -d ${HLT_BASEDIR}/control/run/$d ] ; then
		mkdir ${HLT_BASEDIR}/control/run/$d
	    fi
	fi

	if [ ! -d ${HLT_BASEDIR}/control/run/$d/.verify ] ; then
	    mkdir -p ${HLT_BASEDIR}/control/run/$d/.verify
	fi
	
	if [ ! -d ${HLT_BASEDIR}/control/run/$d/config ] ; then
	    mkdir -p ${HLT_BASEDIR}/control/run/$d/config
	fi
	
	if [ ! -d ${HLT_BASEDIR}/control/run/$d/setup ] ; then
	    mkdir -p ${HLT_BASEDIR}/control/run/$d/setup
	fi
	
	chown -R ${OPERATOR}-operator:hlt-operators ${HLT_BASEDIR}/control/run/$d
	chmod -R 755 ${HLT_BASEDIR}/control/run/$d
    done
    
    #---------------------------------------------------
    # -- create results directory and set access rights
    #---------------------------------------------------

    if [[ "${ADMIN_USER}" == "1" && "${HLT_BASEDIR}" == "/opt/HLT" ]] ; then
	if [ ! -d ${HLT_BASEDIR}/control/results ] ; then
	    fs mkm ${HLT_BASEDIR}/control/results cont.results
	fi
    else
	if [ ! -d ${HLT_BASEDIR}/control/results ] ; then
	    mkdir ${HLT_BASEDIR}/control/results
	fi
    fi

    for d in ${DETECTOR_LIST} ; do 
	GETOPERATOR $d
	
	if [ ! -d ${HLT_BASEDIR}/control/results/$d ] ; then
	    mkdir -p ${HLT_BASEDIR}/control/results/$d
	    
	    chown -R ${OPERATOR}-operator:hlt-operators ${HLT_BASEDIR}/control/results/$d
	    chmod -R 755 ${HLT_BASEDIR}/control/results/$d
	
	    if [[ "${ADMIN_USER}" == "1" && "${HLT_BASEDIR}" == "/opt/HLT" ]] ; then
		fs sa ${HLT_BASEDIR}/control/results/$d -a ${OPERATOR}-operator write
	    fi
	fi
    done
	
    #---------------------------------------------------
    # -- create detector directory and set access rights
    #---------------------------------------------------

    if [ ! -d ${HLT_BASEDIR}/control/detector ] ; then
	mkdir -p ${HLT_BASEDIR}/control/detector
    fi

    for d in ${DETECTOR_LIST} ; do 
	GETOPERATOR $d
	
        ## - create FOLDER
	if [ ! -d ${HLT_BASEDIR}/control/detector/$d ] ; then
	    mkdir -p ${HLT_BASEDIR}/control/detector/$d
	fi  
	
	if [[ "${ADMIN_USER}" == "1" && "${HLT_BASEDIR}" == "/opt/HLT" ]] ; then
	    fs sa ${HLT_BASEDIR}/control/detector/$d -a ${OPERATOR}-operator write
	fi
	
	chown -R ${OPERATOR}-operator:hlt-operators ${HLT_BASEDIR}/control/detector/$d
	chmod -R 755 ${HLT_BASEDIR}/control/detector/$d

        ## - create link to run	    
	pushd ${HLT_BASEDIR}/control/detector/$d/ > /dev/null
	if [ ! -h run ] ; then
	    ln -s ../../run/$d run
	fi
	popd > /dev/null

	## - checkout / update config
	if [ ! -d ${HLT_BASEDIR}/control/detector/$d/config ] ; then
	    LOG_HANDLER --log="checkoutlog-CONTROL-$d-config-${IDATE}"
	    eval svn ${QUIET_SVN} co ${HLTSVN_BASEURL}/control/trunk/detector/$d/config ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	    ERROR_HANDLER
	else
	    LOG_HANDLER --log="checkoutlog-CONTROL-$d-config-${IDATE}"
	    eval svn ${QUIET_SVN} up ${HLT_BASEDIR}/control/detector/$d/config ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	    ERROR_HANDLER
	fi

	pushd  ${HLT_BASEDIR}/control/detector/$d/config > /dev/null
	find -name "pp-*" -type d -exec rm -rf "{}" +
	find -name "AA-*" -type d -exec rm -rf "{}" +
	popd > /dev/null

        ## - checkout / update setup
	if [ ! -d ${HLT_BASEDIR}/control/detector/$d/setup ] ; then
	    LOG_HANDLER --log="checkoutlog-CONTROL-$d-setup-${IDATE}"
	    eval svn ${QUIET_SVN} co ${HLTSVN_BASEURL}/control/trunk/detector/$d/setup ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	    ERROR_HANDLER
	else
	    LOG_HANDLER --log="checkoutlog-CONTROL-$d-setup-update-${IDATE}"   
	    eval svn ${QUIET_SVN} up ${HLT_BASEDIR}/control/detector/$d/setup ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	    ERROR_HANDLER
	fi

	pushd ${HLT_BASEDIR}/control/detector/$d/setup > /dev/null
	find -name "*.xml" -type f -exec rm -rf "{}" +
	popd > /dev/null
		
    done

    #------------------------------------------------------------
    # -- create hlt-configuration directory and set access rights
    #------------------------------------------------------------
    
    if [ ! -d ${HLT_BASEDIR}/control/hlt-configuration ] ; then
	LOG_HANDLER --log="checkoutlog-CONTROL-hlt-configuration-${IDATE}"
	eval svn ${QUIET_SVN} co ${HLTSVN_BASEURL}/control/trunk/hlt-configuration ${HLT_BASEDIR}/control/hlt-configuration ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	ERROR_HANDLER
    else
	LOG_HANDLER --log="checkoutlog-CONTROL-hlt-configuration-setup-${IDATE}"
	eval svn ${QUIET_SVN} up ${HLTSVN_BASEURL}/control/trunk/hlt-configuration ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	ERROR_HANDLER
    fi

    for d in ${DETECTOR_LIST} ; do 
	GETOPERATOR $d

	# -- Check for all operators
	if [ ! -d ${HLT_BASEDIR}/control/hlt-configuration/partitions/$d ] ; then
	    svn ${QUIET_SVN} mkdir ${HLT_BASEDIR}/control/hlt-configuration/partitions/$d
	    svn ${QUIET_SVN} mkdir ${HLT_BASEDIR}/control/hlt-configuration/partitions/$d/production
	    svn ${QUIET_SVN} mkdir ${HLT_BASEDIR}/control/hlt-configuration/partitions/$d/devel
	    svn ${QUIET_SVN} mkdir ${HLT_BASEDIR}/control/hlt-configuration/partitions/$d/valid-test

	    cp  ${HLT_BASEDIR}/control/hlt-configuration/partitions/ALICE/allowed_types ${HLT_BASEDIR}/control/hlt-configuration/partitions/$d/
	    svn ${QUIET_SVN} add ${HLT_BASEDIR}/control/hlt-configuration/partitions/$d/allowed_types
	    svn ${QUIET_SVN} ci ${HLT_BASEDIR}/control/hlt-configuration/partitions/$d -m "Added new partition"
	    pushd ${HLT_BASEDIR}/control/hlt-configuration/partitions/ > /dev/null
	    svn ${QUIET_SVN} up ${HLT_BASEDIR}/control/hlt-configuration/partitions/$d
	    popd > /dev/null
	fi
	
	# -- Set rights
	if [ -d ${HLT_BASEDIR}/control/hlt-configuration/partitions/$d/devel ] ; then

	    if [[ "${ADMIN_USER}" == "1" && "${HLT_BASEDIR}" == "/opt/HLT" ]] ; then
		for ii in `find ${HLT_BASEDIR}/control/hlt-configuration/partitions/$d/devel -type d` ; do fs sa $ii -a ${OPERATOR}-operator write ; done           
	    fi
	 
	    chown -R ${OPERATOR}-operator:hlt-operators ${HLT_BASEDIR}/control/hlt-configuration/partitions/$d/devel
	    chmod -R 755 ${HLT_BASEDIR}/control/hlt-configuration/partitions/$d/devel
	    
	fi  
    done
}
 
###################################################################
#                                                                 #
# -- CHECKOUT CONTROL                                             #
#                                                                 #
###################################################################

CHECKOUT_CONTROL () {
  
    INFO CHECKOUT "CONTROL  : ${versionCONTROL}"

    sleep ${SLEEPTIME}
    
    SET_BASE_MODULES
    
    # -- set base ACLs
    if [ "${ADMIN_USER}" == "1" ] ; then
	fs sa ${HLT_BASEDIR}/control -clear -a system:administrators all  \
	    hlt-admin write \
	    hlt-operators read \
	    host_gui read \
	    system:authuser read 2> /dev/null
    fi
    
    if [ ! -d ${HLT_BASEDIR}/control/releases ] ; then 
	mkdir -p ${HLT_BASEDIR}/control/releases
    fi

    #---------------------------------------------------
    # -- Checkout common 
    #---------------------------------------------------

    CHECKOUT_CONTROL_PREPARE

    #---------------------------------------------------
    # -- Checkout control
    #---------------------------------------------------

    if [ ${modeTEST} ] ; then 

	if [ "${versionCoCONTROL}" == "trunk" ] ; then
	    export OPTION_CHECKOUT="${HLTSVN_BASEURL}/control/trunk -r ${YESTERDAY}"	    
	else
	    export OPTION_CHECKOUT="${HLTSVN_BASEURL}/control/tags/release-${versionCONTROL} -r ${YESTERDAY}"	    
	fi
    else
	pushd ${HLT_BASEDIR}/control/releases > /dev/null

	LOG_HANDLER --log="checkoutlog-CONTROL-${IDATE}"

	if [ "${versionCoCONTROL}" == "trunk" ] ; then
	    eval svn ${QUIET_SVN} co ${HLTSVN_BASEURL}/control/trunk ${targetCONTROL} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	else
	    eval svn ${QUIET_SVN} co ${HLTSVN_BASEURL}/control/tags/release-${versionCoCONTROL} ${targetCONTROL} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	fi

	ERROR_HANDLER

	popd > /dev/null
    fi
    
    if [ -d  ${HLT_BASEDIR}/control/releases/${targetCONTROL}/detector ] ; then 
	rm -rf ${HLT_BASEDIR}/control/releases/${targetCONTROL}/detector
    fi

    if [ -d  ${HLT_BASEDIR}/control/releases/${targetCONTROL}/hlt-configuration ] ; then 
	rm -rf ${HLT_BASEDIR}/control/releases/${targetCONTROL}/hlt-configuration
    fi


    #---------------------------------------------------
    # -- run
    #---------------------------------------------------
    pushd ${HLT_BASEDIR}/control/releases/${targetCONTROL} > /dev/null
    ln -s ../../run
    popd > /dev/null

    UNSET_BASE_MODULES
    
    ADD_TO_REMOVE ${HLTCONTROL_BASEDIR}/${targetCONTROL}
}

###################################################################
#                                                                 #
# -- COMPILE CONTROL                                              #
#                                                                 #
###################################################################

COMPILE_CONTROL () {
    DUMMY=dummy
}

###################################################################
#                                                                 #
# -- CREATE MODULE CONTROL                                        #
#                                                                 #
###################################################################

MODULE_CONTROL () {

    INIT_MODULE_CREATION ${MODULEDIR}/CONTROL ${versionCONTROL}

    cat <<EOF > ${MODULEDIR}/CONTROL/${versionCONTROL}
#%Module1.0#####################################################################
##
## HLT - versions modulefile
##

proc ModulesHelp { } {
        global version
	puts stderr "This module is a module of the HLT for the CONTROL."
}

set 	version 	1.4

module-whatis	"CONTROL versions module for the HLT "

####################################################

## -- CONTROL --
setenv		HLTCONTROL_TOPDIR	   $::env(HLTCONTROL_BASEDIR)/${versionCONTROL}
setenv		HLTCONTROL_SETUPDIR	   $::env(HLTCONTROL_TOPDIR)/setup
append-path     PATH                       $::env(HLTCONTROL_TOPDIR)/bin/operator
EOF

}

