###################################################################
#                                                                 #
# -- TESTMODE ROOT                                                #
#                                                                 #
###################################################################

TESTMODE_ROOT () {

    INFO TESTMODE "ROOT  : ${versionROOT}"

    # -- prod version
    M_BUILD_TYPE="prod"
    
    module unload HLT  2> /dev/null
    module load HLT/${versionHLT}-${M_BUILD_TYPE}  2> /dev/null

    export OPTION_MODULE="${MODULEDIR}/HLT/${versionHLT}-${M_BUILD_TYPE}"

    if [ "${versionCoROOT}" == "head" ] ; then
	export OPTION_CHECKOUT="https://root.cern.ch/svn/root/trunk -r {${YESTERDAY}}"
    else
	export OPTION_CHECKOUT="https://root.cern.ch/svn/root/tags/${versionROOT} -r {${YESTERDAY}}"
    fi

    export OPTION_TRACK="Experimental"
    export OPTION_BUILD="-j 16"

    # -- configure options
#    OPTION_BASE="--enable-cern --enable-rfio --enable-mathmore --enable-mathcore --enable-roofit --enable-asimage --enable-minuit2 --enable-qt --enable-fftw3 --enable-table --with-f77=${OPT_F77} --enable-globus --with-pythia6-libdir=${ALICE_ROOT}/lib"
#    OPTION_BASE=" --enable-roofit --enable-minuit2 --enable-qt --enable-table --with-f77=${OPT_F77} --enable-globus --with-pythia6-uscore=SINGLE "
    OPTION_BASE="--with-f77=${OPT_F77} --with-pythia6-uscore=SINGLE "

    
    if [ ${ALIEN_ROOT} ] ; then
#	OPTION_ALIEN="--enable-alien --with-alien-incdir=${ALIEN_ROOT}/api/include --with-alien-libdir=${ALIEN_ROOT}/api/lib --enable-ssl --with-ssl-incdir=${ALIEN_ROOT}/include --with-ssl-libdir=${ALIEN_ROOT}/lib"
	OPTION_ALIEN=""
    fi

    export OPTION_CONFIGURE="${ALICE_TARGET} ${OPTION_BASE} ${OPTION_ALIEN}"
        
    ${CTEST} -VV -S ${HLTTESTSERVER_BASEDIR}/config/CTestConfig/root/root.cmake

    ## -- create the link to the aliroot version
    pushd $ROOT_BASEDIR > /dev/null
    ln -s ${targetROOT} ${targetALIROOT}
    popd > /dev/null
	


#    pushd ${HLT_BASEDIR}/root > /dev/null
#    if [ ! -h ${HLT_BASEDIR}/root/${targetALIROOT} ] ; then 
#	ln -s ${ROOTSYS} ${HLT_BASEDIR}/root/${targetALIROOT}
#	echo	"ln -s root_${versionROOT} ${HLT_BASEDIR}/root/${targetALIROOT}"
#    fi
#    popd > /dev/null

    module unload HLT 2> /dev/null

    unset OPTION_CONFIGURE
    unset OPTION_CHECKOUT
    unset OPTION_TRACK
    unset OPTION_BUILD
    unset OPTION_MODULE

    ADD_TO_REMOVE ${HLT_BASEDIR}/root/${targetROOT}
}

###################################################################
#                                                                 #
# -- CREATE MODULE ROOT                                           #
#                                                                 #
###################################################################

MODULE_ROOT () {
 
    INIT_MODULE_CREATION ${MODULEDIR}/ROOT ${versionROOT}

    cat <<EOF > ${MODULEDIR}/ROOT/${versionROOT}
#%Module1.0#####################################################################
##
## HLT - ROOT modulefile
##

proc ModulesHelp { } {
        global version
	puts stderr "This module is a module of the HLT for ROOT."
}

set 	version 	1.2

module-whatis	"ROOT versions module for the HLT "

## -- VERSION --
setenv		ROOT_RELEASE	${versionROOT}

setenv		ROOTSYS		$::env(ROOT_BASEDIR)/root_$::env(ROOT_RELEASE)

append-path	PATH		$::env(ROOTSYS)/bin
prepend-path	LD_LIBRARY_PATH	$::env(ROOTSYS)/lib
EOF

}