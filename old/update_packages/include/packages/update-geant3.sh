###################################################################
#                                                                 #
# -- TESTMODE GEANT3                                              #
#                                                                 #
###################################################################

TESTMODE_GEANT3 () {

    INFO TESTMODE "GEANT3  : ${versionGEANT3}"

    # -- prod version
    M_BUILD_TYPE="prod"
    
    module unload HLT  2> /dev/null
    module load HLT/${versionHLT}-${M_BUILD_TYPE}  2> /dev/null

    export OPTION_MODULE="${MODULEDIR}/HLT/${versionHLT}-${M_BUILD_TYPE}"

    if [ "${versionCoGEANT3}" == "head" ] ; then
	export OPTION_CHECKOUT="https://root.cern.ch/svn/geant3/trunk -r {${YESTERDAY}}"
    else
	export OPTION_CHECKOUT="https://root.cern.ch/svn/geant3/tags/${versionGEANT3} -r {${YESTERDAY}}"
    fi

    export OPTION_TRACK="Experimental"
    export OPTION_BUILD="-j 16"

    ${CTEST} -VV -S ${HLTTESTSERVER_BASEDIR}/config/CTestConfig/geant3/geant3.cmake

    ## -- create the link to the aliroot version
    pushd $GEANT3_BASEDIR > /dev/null
    mkdir -p libs_${targetROOT}/lib
    mkdir -p ${targetALIROOT}
    pushd ${targetALIROOT} > /dev/null
    ln -s ../${targetGEANT3} Geant3_version
    ln -s ../libs_${targetROOT}/lib lib
    ln -s ../${targetGEANT3}/TGeant3 TGeant3
    popd > /dev/null
    pushd libs_${targetROOT} > /dev/null
    cp -a ../${targetGEANT3}/lib .
    popd > /dev/null
    popd > /dev/null
    
    module unload HLT 2> /dev/null

    unset OPTION_CHECKOUT
    unset OPTION_TRACK
    unset OPTION_BUILD
    unset OPTION_MODULE

    ADD_TO_REMOVE ${HLT_BASEDIR}/root/${targetROOT}
}

###################################################################
#                                                                 #
# -- CREATE MODULE GEANT3                                         #
#                                                                 #
###################################################################

MODULE_GEANT3 () {
 
    INIT_MODULE_CREATION ${MODULEDIR}/GEANT3 ${versionGEANT3}

    cat <<EOF > ${MODULEDIR}/GEANT3/${versionGEANT3}
#%Module1.0#####################################################################
##
## HLT - ROOT modulefile
##

proc ModulesHelp { } {
        global version
	puts stderr "This module is a module of the HLT for GEANT3."
}

set 	version 	1.2

module-whatis	"ROOT versions module for the HLT "

## -- VERSION --
setenv		GEANT3_RELEASE	${versionGEANT3}

## -- GEANT3 --
setenv 		PLATFORM	$::env(ALICE_TARGET)
append-path	LD_LIBRARY_PATH	$::env(GEANT3_BASEDIR)/AliRoot_$::env(ALIROOT_RELEASE)/lib/tgt_$::env(ALICE_TARGET)
EOF

}