###################################################################
#                                                                 #
# -- CHECKOUT ALIEN                                               #
#                                                                 #
###################################################################

CHECKOUT_ALIEN () {

    INFO CHECKOUT "ALIEN  : ${versionALIEN}"

    sleep ${SLEEPTIME}
    
    pushd ${HLT_BASEDIR}/alien > /dev/null

    if [ ! -d ${HLT_BASEDIR}/alien/installer ] ; then 
	mkdir ${HLT_BASEDIR}/alien/installer
    fi

    if [ ! -f ./installer/alien-installer_${versionALIEN} ] ; then

	pushd ${HLT_BASEDIR}/alien/installer > /dev/null

	wget ${QUIET_WGET} http://alien.cern.ch/alien-installer
	if [ $? -ne 0 ] ; then
	    ERROR_HANDLER --error="wget failed to get alien installer version ${versionALIEN}."
	fi

	chmod +x alien-installer
	mv alien-installer alien-installer_${versionALIEN} 
	
	popd > /dev/null

	if [ -h alien-installer ] ; then
	    rm alien-installer
	fi

	ln -s installer/alien-installer_${versionALIEN} alien-installer
    fi
    
    if [ -d .alien ] ; then
	rm -rf .alien/*
    else
	mkdir .alien
	chmod 777 .alien
    fi

    if [ -d ${HOME}/.alien ] ; then
	rm -rf ${HOME}/.alien/*
    else
	mkdir ${HOME}/.alien
	chmod 777 .alien
    fi

    ## -- NOT NEEDED for new alien starting v2-17.84 --
 #   echo "ALIEN_INSTALLER_HOME=${HLT_BASEDIR}/alien/.alien/cache
 # ALIEN_INSTALLER_PREFIX=${HLT_BASEDIR}/alien/${versionALIEN}
 # ALIEN_INSTALLER_AUTODETECT=false
 # ALIEN_INSTALLER_PLATFORM=x86_64-unknown-linux-gnu
 # ALIEN_DIALOG=dialog
 # ALIEN_RELEASE=${versionALIEN}
 # ALIEN_BITS_URL=http://pcalibuildamd.cern.ch:8880/x86_64-unknown-linux-gnu/${versionALIEN}/download/
 #" > $HOME/.alien/installer.rc

 #ALIEN_INSTALLER_TYPE="client+gshell"
    ## -- NOT NEEDED for new alien starting v2-17.84 --

    popd > /dev/null

    ADD_TO_REMOVE ${HLT_BASEDIR}/alien/${targetALIEN}
}

###################################################################
#                                                                 #
# -- COMPILE ALIEN                                                #
#                                                                 #
###################################################################

COMPILE_ALIEN () {

    INFO COMPILE "ALIEN  : ${versionALIEN}"
    
    sleep ${SLEEPTIME}
    
    # prod version
    M_BUILD_TYPE="prod"

    module unload HLT  2> /dev/null
    module load HLT/${versionHLT}-${M_BUILD_TYPE}  2> /dev/null

    pushd ${HLT_BASEDIR}/alien > /dev/null

    LOG_HANDLER --log="installlog-ALIEN-${IDATE}"
    eval ./alien-installer -install-dir ${HLT_BASEDIR}/alien/${versionALIEN} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
    ERROR_HANDLER 

    if [ -h $HOME/bin/aliensh ] ; then
	rm $HOME/bin/aliensh
    fi

    if [ -f $HOME/bin/alien-token-init ] ; then
	rm $HOME/bin/alien-token-init
    fi

    popd > /dev/null
}

###################################################################
#                                                                 #
# -- CREATE MODULE ALIEN                                          #
#                                                                 #
###################################################################

MODULE_ALIEN () {
    
    INIT_MODULE_CREATION ${MODULEDIR}/ALIEN ${versionALIEN}
    
    cat <<EOF > ${MODULEDIR}/ALIEN/${versionALIEN}
#%Module1.0#####################################################################
##
## HLT - ALIEN modulefile
##

proc ModulesHelp { } {
        global version
        puts stderr "This module is a module of the HLT for ALIEN."
}

set     version         1.3

module-whatis   "ALIEN versions module for the HLT "

####################################################

## -- VERSION
set		alien_version           	${versionALIEN}

## -- ALIEN --
setenv          ALIEN_ROOT      		$::env(ALIEN_BASEDIR)/\$alien_version
setenv		ALIEN_INSTALLER_PLATFORM	x86_64-unknown-linux-gnu
setenv 		GSHELL_GCC			/usr/bin/gcc

append-path	LD_LIBRARY_PATH			$::env(ALIEN_ROOT)/api/lib

append-path 	PATH				$::env(ALIEN_ROOT)/api/bin

## -- Globus Location --
setenv		GLOBUS_LOCATION			$::env(ALIEN_ROOT)/globus

## -- User --
EOF

if [ "$clusterUSAGE" == "1" ] ; then
    echo "setenv		alien_API_USER 			roehrich
    " >> ${MODULEDIR}/ALIEN/${versionALIEN}
else
    echo "setenv		alien_API_USER 			`whoami`
    " >> ${MODULEDIR}/ALIEN/${versionALIEN}
fi

cat <<EOF >> ${MODULEDIR}/ALIEN/${versionALIEN}
## -- LHC Periode --
setenv		LHC_PERIOD_OCDB_PATH 		/alice/data/2010/LHC10e/OCDB
setenv		LHC_PERIOD	 		LHC10e
EOF

}
