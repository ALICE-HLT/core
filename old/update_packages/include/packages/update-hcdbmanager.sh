###################################################################
#                                                                 #
# -- TESTMODE HCDBManager                                         #
#                                                                 #
###################################################################

TESTMODE_HCDBMANAGER () {
    
    INFO TESTMODE "HCDBManager : ${versionHCDBMANAGER}"
    
    SET_BASE_MODULES
    export OPTION_CHECKOUT="${HLTSVN_BASEURL}/interfaces/HCDBManager/tags/release-${versionHCDBMANAGER} -r {${YESTERDAY}}"
    export OPTION_TRACK="Experimental"
    export OPTION_BUILD=${MAKEOPT}
    UNSET_BASE_MODULES
    
    # prod version
    M_BUILD_TYPE="prod"
    
    module unload HLT  2> /dev/null
    module load HLT/${versionHLT}-${M_BUILD_TYPE}  2> /dev/null
    
    ${CTEST} -S ${HLTTESTSERVER_BASEDIR}/config/CTestConfig/hcdbmanager/hcdbmanager.cmake
    
    module unload HLT  2> /dev/null

    unset OPTION_CHECKOUT
    unset OPTION_TRACK
    unset OPTION_BUILD
    
    ADD_TO_REMOVE ${HLT_BASEDIR}/interfaces/HCDBManager/${targetHCDBMANAGER} 
}


###################################################################
#                                                                 #
# -- CHECKOUT HCDBManager                                         #
#                                                                 #
###################################################################

CHECKOUT_HCDBMANAGER () {

    INFO CHECKOUT "HCDBManager : ${versionHCDBMANAGER}"

    sleep ${SLEEPTIME}

    SET_BASE_MODULES

    if [ ${modeTEST} ] ; then 
	export OPTION_CHECKOUT="${HLTSVN_BASEURL}/interfaces/HCDBManager/tags/release-${versionHCDBMANAGER} -r {${YESTERDAY}}"
	export OPTION_TRACK="Experimental"
    else
	pushd ${HLT_BASEDIR}/interfaces/HCDBManager > /dev/null
  
	LOG_HANDLER --log="checkoutlog-HCDBMANAGER-${IDATE}"
	eval svn co ${HLTSVN_BASEURL}/interfaces/HCDBManager/tags/release-${versionHCDBMANAGER} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	ERROR_HANDLER

	mv release-${versionHCDBMANAGER} ${targetHCDBMANAGER}
    
	popd > /dev/null
    fi

    UNSET_BASE_MODULES

    ADD_TO_REMOVE ${HLT_BASEDIR}/interfaces/HCDBManager/${targetHCDBMANAGER} 
}

###################################################################
#                                                                 #
# -- COMPILE HCDBMANAGER                                          #
#                                                                 #
###################################################################

COMPILE_HCDBMANAGER () {

    INFO COMPILE "HCDBManager : ${versionHCDBMANAGER}"
    
    sleep ${SLEEPTIME}
    
    # prod version
    M_BUILD_TYPE="prod"
      
    module unload HLT  2> /dev/null
    module load HLT/${versionHLT}-${M_BUILD_TYPE}  2> /dev/null

    if [ ${modeTEST} ] ; then 
	export OPTION_BUILD=${MAKEOPT}
	
	${CTEST} -S ${HLTTESTSERVER_BASEDIR}/config/CTestConfig/hcdbmanager/hcdbmanager.cmake

	unset OPTION_CHECKOUT
	unset OPTION_TRACK
	unset OPTION_BUILD
    else
	pushd ${HLTHCDBMANAGER_TOPDIR} > /dev/null

	LOG_HANDLER --log="compilelog-HCDBMANAGER-${IDATE}"
	make ${MAKEOPT} 2>&1 | tee ${LOG_TARGET} 
	ERROR_HANDLER 

	popd > /dev/null
    fi

    module unload HLT  2> /dev/null
}

###################################################################
#                                                                 #
# -- CREATE MODULE HCDBMANAGER                                    #
#                                                                 #
###################################################################

MODULE_HCDBMANAGER () {

    INIT_MODULE_CREATION ${MODULEDIR}/HCDBMANAGER ${versionHCDBMANAGER}-${versionALIROOT}

    cat <<EOF > ${MODULEDIR}/HCDBMANAGER/${versionHCDBMANAGER}-${versionALIROOT}
#%Module1.0#####################################################################
##
## HLT - versions modulefile
##

proc ModulesHelp { } {
        global version
	puts stderr "This module is a module of the HLT for the HCDBMANAGER."
}

set 	version 	1.4

module-whatis	"HCDBManager versions module for the HLT "

####################################################

## -- HCDBManager --
setenv          HLTHCDBMANAGER_TOPDIR  $::env(HLTHCDBMANAGER_BASEDIR)/${versionHCDBMANAGER}-${versionALIROOT}
append-path     PATH                   $::env(HLTHCDBMANAGER_BASEDIR)/${versionHCDBMANAGER}-${versionALIROOT}
EOF

}