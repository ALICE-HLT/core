###################################################################
#                                                                 #
# -- CHECKOUT ECS PROXY                                           #
#                                                                 #
###################################################################

CHECKOUT_ECSPROXY () {

    INFO CHECKOUT "ECSPROXY  : ${versionECSPROXY}"
    
    sleep ${SLEEPTIME}

    SET_BASE_MODULES

    if [ ${modeTEST} ] ; then 
	export OPTION_CHECKOUT="${HLTSVN_BASEURL}/interfaces/ecs-proxy/tags/release-${versionECSPROXY} -r ${YESTERDAY}"
    else
	pushd ${HLT_BASEDIR}/interfaces/ecs-proxy > /dev/null

	LOG_HANDLER --log="checkoutlog-ECSPROXY-${IDATE}"
	eval svn co ${HLTSVN_BASEURL}/interfaces/ecs-proxy/tags/release-${versionECSPROXY} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	ERROR_HANDLER

	mv release-${versionECSPROXY} ${targetECSPROXY}

	popd > /dev/null
    fi
	
    UNSET_BASE_MODULES

    ADD_TO_REMOVE ${HLT_BASEDIR}/interfaces/ecs-proxy/${targetECSPROXY} 
}

###################################################################
#                                                                 #
# -- COMPILE ECSPROXY  -- PREPARE --                              #
#                                                                 #
###################################################################

COMPILE_ECSPROXY_PREPARE () {
  
    # -- prepare DIM
    if [ ! -d ${DIMDIR}/${ODIR} ]; then
	mkdir -p ${DIMDIR}/${ODIR}
    fi

    # -- prepare SMI 
    if [ ! -d ${SMIDIR}/${ODIR} ] ; then
	mkdir -p ${SMIDIR}/${ODIR}
    fi

    # -- creating "bin", "lib" and "doc" folder --
    if [ ! -d ${HLTECS_TOPDIR}/bin ] ; then
	mkdir -p ${HLTECS_TOPDIR}/bin
    fi

    if [ ! -d ${HLTECS_TOPDIR}/doc ] ; then
	mkdir -p ${HLTECS_TOPDIR}/doc
    fi

    if [ ! -d ${HLTECS_TOPDIR}/lib ] ; then
	mkdir -p ${HLTECS_TOPDIR}/lib
    fi

    # -- create links of taskmanager libs in standard nomenclature --
    if [ ! -h ${HLTECS_LIBDIR}/libSlaveControlInterface.so ] ; then
	ln -s ${TASK_MANAGER_LIBS}/SlaveControlInterfaceLib.so ${HLTECS_LIBDIR}/libSlaveControlInterface.so
    fi
    
    if [ ! -h ${HLTECS_LIBDIR}/libTM.so ] ; then
	ln -s ${TASK_MANAGER_LIBS}/TMLib.so ${HLTECS_LIBDIR}/libTM.so
    fi
}

###################################################################
#                                                                 #
# -- COMPILE ECSPROXY                                             #
#                                                                 #
###################################################################

COMPILE_ECSPROXY () {

    INFO COMPILE "ECSPROXY  : ${versionECSPROXY}"
    
    sleep ${SLEEPTIME}
    
    # prod version
    M_BUILD_TYPE="prod"
    
    module unload HLT  2> /dev/null
    module load HLT/${versionHLT}-${M_BUILD_TYPE}  2> /dev/null

    COMPILE_ECSPROXY_PREPARE

    if [ ${modeTEST} ] ; then 
	export OPTION_BUILD=${MAKEOPT}

##	ctest -VV -S ${HLTTESTSERVER_BASEDIR}/config/CTestConfig/ecsproxy/csproxy.cmake

	unset OPTION_CHECKOUT
	unset OPTION_BUILD
    else
	pushd ${HLTECS_TOPDIR} > /dev/null

        # -- compile DIM
	pushd ${DIMDIR} > /dev/null 

	LOG_HANDLER --log="compilelog-ECSPROXY-DIM-${IDATE}"
	make realclean 
	eval make ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	ERROR_HANDLER
    
        popd > /dev/null

        # -- compile SMI
	pushd ${SMIDIR} > /dev/null
	
	LOG_HANDLER --log="compilelog-ECSPROXY-SMI-${IDATE}"
	make clean
	eval make ${MAKEOPT} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	ERROR_HANDLER
	
	popd > /dev/null
    
        # -- compile ECSPROXY
	LOG_HANDLER --log="compilelog-ECSPROXY-${IDATE}"
	eval make ${MAKEOPT} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	ERROR_HANDLER
    
        # -- compile ECSGUI
	LOG_HANDLER --log="compilelog-ECSPROXY-GUI-${IDATE}"
	eval make gui ${MAKEOPT} ${REDIRECT_A} ${LOG_TARGET} ${REDIRECT_B}
	ERROR_HANDLER
	popd > /dev/null
    fi

    module unload HLT  2> /dev/null
}

###################################################################
#                                                                 #
# -- CREATE MODULE ECSPROXY                                       #
#                                                                 #
###################################################################

MODULE_ECSPROXY () {

    INIT_MODULE_CREATION ${MODULEDIR}/ECSPROXY ${versionECSPROXY}-${versionDATATRANSPORT}

    cat <<EOF > ${MODULEDIR}/ECSPROXY/${versionECSPROXY}-${versionDATATRANSPORT}
#%Module1.0#####################################################################
##
## HLT - versions modulefile
##

proc ModulesHelp { } {
        global version
	puts stderr "This module is a module of the HLT for the ECS proxy."
}

set 	version 	1.2

module-whatis	"ECS proxy versions module for the HLT "

####################################################

set	        nodename             [uname nodename]

####################################################

## -- ECS PROXY --
setenv          HLTECS_TOPDIR        $::env(HLTECS_BASEDIR)/${versionECSPROXY}-${versionDATATRANSPORT}
setenv          HLT_PROXY_DIR        $::env(HLTECS_TOPDIR)

setenv          HLTECS_LIBDIR        $::env(HLTECS_TOPDIR)/lib
setenv          TASK_MANAGER_LIBS    $::env(ALIHLT_DC_DIR)/lib/$::env(HLT_TARGET)

append-path     LD_LIBRARY_PATH      $::env(HLTECS_LIBDIR)

if { \$nodename == "portal-ecs0" } {
        setenv  HLTECS_MODULE        HLT
} elseif { \$nodename == "portal-ecs1" } {
        setenv  HLTECS_MODULE        HLT_BAK
} 

## -- Setup DIM --
setenv          OS                   Linux
setenv          DIMDIR               $::env(HLTECS_TOPDIR)/dim
setenv          ODIR                 linux
append-path    LD_LIBRARY_PATH      $::env(DIMDIR)/$::env(ODIR)

## -- Setup SMI --
setenv          SMIDIR               $::env(HLTECS_TOPDIR)/smi
setenv          SMIRTLDIR            $::env(SMIDIR)

append-path     PATH                 $::env(SMIDIR)/$::env(ODIR)
append-path    LD_LIBRARY_PATH      $::env(SMIDIR)/$::env(ODIR)
EOF

}
