#!/bin/bash
# 
# * Checkout / Install of the HLT packages
# * Compile of the HLT packages
# * Create module files
# * Check packages consistency
#
# File   : update-packages.sh
# Author : Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 2010-01-09
#
# History: 
#   1.0   First Version
#   1.1   ECS Proxy Added
#   1.2   Changed datatransport download -> added type ( stable or current )
#   1.3   Added Pendolino interface and configuration file
#   1.4   Added Error Handler
#   1.5   Changed Structure of files and added ctest capabilities
#   1.6   Integrated consistency check of installed packages and modules
#
INSTALLER_VERSION=1.5
###################################################################
# include functions

if [ -h $0 ] ; then 
    T0=`which $0`;T1=`dirname $T0`;T2=`readlink $T0`;T3=`dirname $T2`
    pushd ${T1} >/dev/null ; cd $T3; INCLUDE_DIR=`pwd`/include ; popd >/dev/null
else
    pushd `dirname $0` >/dev/null ; INCLUDE_DIR=`pwd`/include ; popd >/dev/null
fi

source ${INCLUDE_DIR}/update-setup.sh
source ${INCLUDE_DIR}/update-functions.sh
source ${INCLUDE_DIR}/update-common.sh
source ${INCLUDE_DIR}/update-consistency-functions.sh
source ${INCLUDE_DIR}/update-consistency.sh
source ${INCLUDE_DIR}/packages/update-base.sh
source ${INCLUDE_DIR}/packages/update-alien.sh
source ${INCLUDE_DIR}/packages/update-root.sh
source ${INCLUDE_DIR}/packages/update-geant3.sh
source ${INCLUDE_DIR}/packages/update-aliroot.sh
source ${INCLUDE_DIR}/packages/update-fastjet.sh
source ${INCLUDE_DIR}/packages/update-datatransport.sh
source ${INCLUDE_DIR}/packages/update-hltanalysis.sh
source ${INCLUDE_DIR}/packages/update-ecsproxy.sh
source ${INCLUDE_DIR}/packages/update-taxi.sh
source ${INCLUDE_DIR}/packages/update-pendolino.sh
source ${INCLUDE_DIR}/packages/update-hcdbmanager.sh
source ${INCLUDE_DIR}/packages/update-control.sh

###################################################################

###################################################################
#                                                                 #
# -- CHECK COMMAND LINE / ARGUMENTS / BASEDIRS                    #
#                                                                 #
###################################################################
CMD_LINE=$*

CHECK_COMMANDLINE
CHECK_ARGUMENTS
CHECK_BASEDIRS

###################################################################
#                                                                 #
# -- SETUP  ENVIRONMENT / LOGGING / MODULES / FILES / TARGETS     #
#                                                                 #
###################################################################

SETUP_MODULES
SETUP_ENVIRONMENT
SETUP_LOGGING
SETUP_FILES
SETUP_TARGETS 

###################################################################
#                                                                 #
# -- CHECK TARGETS                                                #
#                                                                 #
###################################################################

CHECK_TARGETS 

###################################################################
#                                                                 #
# -- CHECKOUT and COMPILE                                         #
#                                                                 #
###################################################################

CHECKOUT_COMPILE

###################################################################
#                                                                 #
# -- CHECK CONSISTENCY                                            #
#                                                                 #
###################################################################

CHECK_CONSISTENCY

###################################################################
#                                                                 #
# -- DONE                                                         #
#                                                                 #
###################################################################

cecho "###################################################################" $bgreen
cecho "#                                                                 #" $bgreen

if [ ${runTYPE} == "checkout" ] ; then
    cecho "#                    --- CHECKOUT DONE ---                        #" $bgreen
elif [ ${runTYPE} == "check" ] ; then
    cecho "#                      --- CHECK DONE ---                         #" $bgreen
elif [ ${runTYPE} == "compile" ] ; then
    cecho "#                     --- COMPILE DONE ---                        #" $bgreen
elif [ ${runTYPE} == "all" ] ; then
    cecho "#                      --- UPDATE DONE ---                        #" $bgreen
elif [ ${runTYPE} == "consistency" ] ; then
    cecho "#                 --- CONSISTENCY CHECK DONE ---                  #" $bgreen
fi

cecho "#                                                                 #" $bgreen
cecho "###################################################################" $bgreen
