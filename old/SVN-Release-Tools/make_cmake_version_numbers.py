#!/usr/bin/env python

import os, string, re, sys, os.path, optparse

minor_pat = re.compile( "( *SET\( *CPACK_PACKAGE_VERSION_MINOR +\")[0-9]+(\" *\).*)", re.IGNORECASE )
patch_pat = re.compile( "( *SET\( *CPACK_PACKAGE_VERSION_PATCH +\")[0-9]+(\" *\).*)", re.IGNORECASE )

cmake_file_basename = "CMakeLists.txt"

def make_cmake_filename( directory ):
    return os.path.join( directory, cmake_file_basename )

def is_cmake_directory( directory ):
    files = os.listdir( directory )
    if cmake_file_basename in files:
        return True
    return False

def update_version_info( directory, major, minor, patch ):
    lib_minor = "%d" % (1000*int(major)+int(minor))
    if patch[-1] in [ chr(ord('a')+x) for x in range(26) ]:
        lib_patch = patch[:-1]
    else:
        lib_patch = patch[:]

    filename = make_cmake_filename( directory )
    
    infile = open( filename, "r" )
    lines = infile.readlines()
    infile.close()

    outfile = open( filename, "w" )
    for line in lines:
        match = minor_pat.search( line )
        if match!=None:
            line = "%s%s%s\n" % ( match.group(1), lib_minor, match.group(2) )
        match = patch_pat.search( line )
        if match!=None:
            line = "%s%s%s\n" % ( match.group(1), lib_patch, match.group(2) )
        outfile.write( line )
    outfile.close()
    

if __name__ == "__main__":
    usage = "usage: %prog [options] <release-ID>"
    cliParser = optparse.OptionParser(usage=usage)
    cliParser.add_option( "-v", "--verbose", action="store_true", dest="verbose", default=False, help="Be more verbose" )
    cliParser.add_option( "-d", "--directory",
                          action="store", type="string", dest="directory",
                          default=".",
                          help="Define the directory of the project to be versioned" )

    ( options, args ) = cliParser.parse_args( sys.argv[1:] )

    if len(args)<=0:
        cliParser.error( "Missing release ID argument" )
        sys.exit(1)

    directory = options.directory

    release = args[0]
    if not is_cmake_directory( directory ):
        sys.stderror.write( "Directory '"+directory+"' does not contain a cmake project.\n" )
        sys.exit(1)

    versionNrs = release.split( "." )
    if len(versionNrs)!=3:
        sys.stderror.write( "Format of release version number must be <major>.<minor>.<patchlevel>.\n" )
        sys.exit(1)

    try:
        major = int(versionNrs[0])
        minor = int(versionNrs[1])
        patch = int(versionNrs[2])
    except ValueError:
        sys.stderror.write( "Format of release version number must be <major>.<minor>.<patchlevel>.\n" )
        sys.exit(1)
    
    filename = os.path.join( directory, cmake_file )

    update_version_info( directory, major, minor, patch )

        
    
#SET(CPACK_PACKAGE_VERSION_MAJOR "0")
#SET(CPACK_PACKAGE_VERSION_MINOR "5")
#SET(CPACK_PACKAGE_VERSION_PATCH "2")
