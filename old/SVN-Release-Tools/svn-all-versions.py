#!/usr/bin/env python

import os, string, re, sys, os.path, optparse

import svnutils

def read_packages( file ):
    infile = open( file, "r" )
    packages = []
    line=infile.readline()
    while line != "":
        packages += line.split()
        line=infile.readline()
    infile.close()
    return packages

usage = "usage: %prog [options] <project-directories>"
cliParser = optparse.OptionParser(usage=usage)
cliParser.add_option( "--nohead", action="store_true", dest="nohead", default=False, help="Do not try to determine version of HEAD/trunk" )
cliParser.add_option( "-v", "--verbose", action="store_true", dest="verbose", default=False, help="Be more verbose" )
( options, dirs ) = cliParser.parse_args( sys.argv[1:] )

if len(dirs)<=0:
    dirs.append( "." )

types = []
if not options.nohead:
    types.append( "trunk" )
types += [ os.path.join( "branches", x ) for x in [ "current", "stable" ] ]

for d in dirs:
    try:
        files = os.listdir( d )
    except OSError,e:
        if e.errno==2:
            sys.stderr.write( "Directory "+d+" does not exist - skipping...\n" )
            continue
        raise
    if not "Packages" in files:
        sys.stderr.write( "Cannot find Packages file in directory "+d+" - skipping...\n" )
        continue
    print "======================================================"
    print "======================================================"
    print d
    print
    packages = read_packages( os.path.join( d, "Packages" ) )

    if "Packages.ROOT" in files:
        packages += read_packages( os.path.join( d, "Packages.ROOT" ) )

    for package in packages:
        if not svnutils.is_svn_directory( os.path.join( d, package ) ):
            sys.stderr.write( "Directory '"+os.path.join( d, package )+"' is not under svn control - skipping...\n" )
            continue
        print "======================================================"
        print d, " --- ", package
        thisURL = svnutils.get_repository_url( os.path.join( d, package ) )
        ( baseURL, dirType ) = svnutils.split_repository_url( thisURL )
        if baseURL==None:
            sys.stderr.write( "Cannot determine base URL for project in directory '"+os.path.join( d, package )+"' - skipping...\n" )
            continue
        if options.verbose:
            print "    Base URL: "+baseURL
            print "    Dir Type: "+dirType
        releases = svnutils.get_releases( baseURL )
        
        rels = {}
        for dirType in types:
            release = svnutils.find_release( baseURL, dirType, releases, options.verbose )
            rels[dirType] = release

        for dirType in types:
            print dirType+": "+rels[dirType]
        print
        print
    print
    print
    
            
        
        
    
