#!/usr/bin/env python

import os, string, re, sys, os.path, optparse

import svnutils

try:
    import make_cmake_version_numbers
    cmake_module_loaded=True
except ImportError:
    cmake_module_loaded=False
    print "Warning, cmake module could not be loaded"
    
    

usage = "usage: %prog [options] <release-ID>"
cliParser = optparse.OptionParser(usage=usage)
cliParser.add_option( "-v", "--verbose", action="store_true", dest="verbose", default=False, help="Be more verbose" )
cliParser.add_option( "-d", "--directory",
                      action="store", type="string", dest="directory",
                      default=".",
                      help="Define the directory of the project to be tagged" )
cliParser.add_option( "-f", "--force", action="store_true", dest="force", default=False, help="Force the tag even if there are uncommitted local changes" )

( options, args ) = cliParser.parse_args( sys.argv[1:] )

directory = options.directory

if not svnutils.is_svn_directory( directory ):
    sys.stderr.write( "Directory '"+directory+"' is not under svn control - skipping...\n" )
    sys.exit(1)

diffs = svnutils.diff( directory )
if len(diffs)>0 and not options.force:
    sys.stderr.write( "Directory '"+directory+"' has uncommitted local changes. Please commit or use -f/--force flag.\n" )
    sys.exit(1)

thisURL = svnutils.get_repository_url( directory )

( baseURL, dirType ) = svnutils.split_repository_url( thisURL )

if baseURL==None:
    sys.stderr.write( "Cannot determine base URL for project in directory '"+directory+"' - skipping...\n" )
    sys.exit(1)

release = args[0]
tag = "release-"+release
rk = svnutils.get_release_keys( tag )


    
if options.verbose:
    print "Base URL: "+baseURL
    print "Dir Type: "+dirType

releases = svnutils.get_releases( baseURL )

if tag in releases:
    sys.stderr.write( "Release "+release+" already exists for project in "+directory+"\n" )
    sys.exit(1)

if cmake_module_loaded and make_cmake_version_numbers.is_cmake_directory( directory ):
    cmake_file = make_cmake_version_numbers.make_cmake_filename( directory )
    versionNrs = release.split( "." )
    try:
        major = versionNrs[0]
        minor = versionNrs[1]
        patch = versionNrs[2]
    except ValueError:
        sys.stderror.write( "Format of release version number must be <major>.<minor>.<patchlevel>.\n" )
        sys.exit(1)
    make_cmake_version_numbers.update_version_info( directory, major, minor, patch )
    svnutils.commit( cmake_file, "Updated version identifier in CMake file to %s.%s.%s" % ( major, minor, patch ) )

ok = svnutils.make_tag( baseURL, dirType, release )

if ok!="":
    sys.stderr.write( "Error making release "+release+" for project in "+directory+"\n" )
    sys.stderr.write( ok+"\n" )
    sys.exit(1)

sys.exit(0)






