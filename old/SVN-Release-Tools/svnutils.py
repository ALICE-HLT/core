#!/usr/bin/env python

import os, string, re, sys, os.path


def get_release_keys( x ):
    k = x.split("-")[-1].split(".")
    b = k[-1]
    last = 0
    for c in range(len(b)):
        if b[c] in [ chr(ord('0')+x) for x in range(10) ]:
            last=c+1
        else:
            break
    return [ int(x) for x in k[:-1] ] + [ int(b[:last]), b[last:] ]

def is_svn_directory( d ):
    try:
        files=os.listdir( d )
    except OSError,e:
        return False
    return ( ".svn" in files )

def get_repository_url( directory ):
    lines=[]
    inputPipe = os.popen( "LANG=C svn info "+directory+" 2>&1" )
    line=inputPipe.readline()
    while line!="":
        lines.append(line)
        line=inputPipe.readline()
    inputPipe.close()
    baseURL = None
    thisURL = None
    for line in lines:
        if line[-1]=="\n":
            line = line[:-1]
        if line.split(" ")[0]=="URL:":
            return line.split(" ")[1]
    return ""
    

def split_repository_url( reposURL ):
    if os.path.split( reposURL )[1]=="trunk":
        baseURL = os.path.split( reposURL )[0]
        dirType = os.path.split( reposURL )[1]
    elif os.path.split( os.path.split( reposURL )[0] )[1] in [ "branches", "tags" ]:
        baseURL = os.path.split( os.path.split( reposURL )[0] )[0]
        dirType = os.path.join( os.path.split( os.path.split( reposURL )[0] )[1], os.path.split( reposURL )[1] )
    else:
        baseURL = None
        dirType = None
    return ( baseURL, dirType )
        

def get_releases( baseURL ):
    tagsDir = os.path.join( baseURL, "tags" )
    lines = []
    inputPipe = os.popen( "LANG=C svn list "+tagsDir+" 2>&1" )
    line=inputPipe.readline()
    while line!="":
        if not line[:len("release-")]=="release-":
            line=inputPipe.readline()
            continue
        if line[-1]=="\n":
            lines.append(line[:-2])
        else:
            lines.append(line[:-1])
        line=inputPipe.readline()
    inputPipe.close()

    lines.sort( key=get_release_keys )
    
    return lines


def get_branches( baseURL ):
    branchDir = os.path.join( baseURL, "branches" )
    lines = []
    inputPipe = os.popen( "LANG=C svn list "+branchDir+" 2>&1" )
    line=inputPipe.readline()
    while line!="":
        if line[-1]=="\n":
            lines.append(line[:-2])
        else:
            lines.append(line[:-1])
        line=inputPipe.readline()
    inputPipe.close()

    lines.sort()
    
    return lines

def diff_with_release( baseURL, dirType, release ):
    #print "Command:","LANG=C svn diff "+os.path.join( baseURL, dirType )+" "+os.path.join( baseURL, "tags", release )
    inputPipe = os.popen( "LANG=C svn diff "+os.path.join( baseURL, dirType )+" "+os.path.join( baseURL, "tags", release )+" 2>&1" )
    lines = []
    line=inputPipe.readline()
    diff = False
    while line != "":
        diff = True
        line=inputPipe.readline()
    inputPipe.close()
    return diff

def find_release( baseURL, dirType, releases, verbose=False ):
    tmpReleases = releases[:]
    tmpReleases.sort( key=get_release_keys, reverse=True )
    for r in tmpReleases:
        if verbose:
            print "Diff'ing "+dirType+" with release "+r
        if not diff_with_release( baseURL, dirType, r ):
            return r
    return ""



def copy_repository( source, target, comment ):
    inputPipe = os.popen( "LANG=C svn copy "+source+" "+target+" -m \""+comment+"\""+" 2>&1" )
    line=inputPipe.readline()
    text = line
    while line != "":
        line=inputPipe.readline()
        text += line
    inputPipe.close()
    if text[:len("\nCommitted")]!="\nCommitted":
        return text
    return ""


def delete_repository( baseURL, victim, comment ):
    inputPipe = os.popen( "LANG=C svn remove "+os.path.join( baseURL, victim )+" -m \""+comment+"\""+" 2>&1" )
    line=inputPipe.readline()
    text = line
    while line != "":
        line=inputPipe.readline()
        text += line
    inputPipe.close()
    if text[:len("\nCommitted")]!="\nCommitted":
        return text
    return ""


def make_tag( baseURL, dirType, release ):
    tag = "release-"+release
    return copy_repository( os.path.join( baseURL, dirType ), os.path.join( baseURL, "tags", tag ), "Making tag release "+release+" from "+dirType )

def tag_to_branch( baseURL, release, branch ):
    tag = "release-"+release
    branchDir = os.path.join( "branches", branch )
    branches = get_branches( baseURL )
    if branch in branches:
        text = delete_repository( baseURL, branchDir, "Removing old "+branch+" branch" )
        if text!="":
            return text
    return copy_repository( os.path.join( baseURL, "tags", tag ), os.path.join( baseURL, branchDir ), "Making "+branch+" branch from release "+release )


def make_current( baseURL, release ):
    return tag_to_branch( baseURL, release, "current" )


def make_stable( baseURL, release ):
    return tag_to_branch( baseURL, release, "stable" )

    
def diff( directory ):
    lines = []
    inputPipe = os.popen( "LANG=C svn diff "+directory+" 2>&1" )
    lines=inputPipe.readlines()
    inputPipe.close()
    return lines

def commit( filename, message ):
    inputPipe = os.popen( "LANG=C svn commit -m \""+message+"\" "+filename+" 2>&1" )
    line=inputPipe.readline()
    text = line
    while line != "":
        line=inputPipe.readline()
        text += line
    inputPipe.close()
    print "svn commit text:",text
    if text[:len("\nCommitted")]!="\nCommitted":
        return text
    return ""
    
