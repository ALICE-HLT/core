#!/usr/bin/env python

import os, string, re, sys, os.path, optparse

import svnutils

usage = "usage: %prog [options] <release-ID>"
cliParser = optparse.OptionParser(usage=usage)
cliParser.add_option( "-v", "--verbose", action="store_true", dest="verbose", default=False, help="Be more verbose" )
cliParser.add_option( "-d", "--directory",
                      action="store", type="string", dest="directory",
                      default=".",
                      help="Define the directory of the project to be tagged" )

( options, args ) = cliParser.parse_args( sys.argv[1:] )

directory = options.directory

if not svnutils.is_svn_directory( directory ):
    sys.stderr.write( "Directory '"+directory+"' is not under svn control - skipping...\n" )
    sys.exit(1)

thisURL = svnutils.get_repository_url( directory )

( baseURL, dirType ) = svnutils.split_repository_url( thisURL )

if baseURL==None:
    sys.stderr.write( "Cannot determine base URL for project in directory '"+directory+"' - skipping...\n" )
    sys.exit(1)

release = args[0]
tag = "release-"+release
rk = svnutils.get_release_keys( tag )

    
if options.verbose:
    print "Base URL: "+baseURL
    print "Dir Type: "+dirType

releases = svnutils.get_releases( baseURL )

if not tag in releases:
    sys.stderr.write( "Release "+release+" does not exist for project in "+directory+"\n" )
    sys.exit(1)

ok = svnutils.make_current( baseURL, release )

if ok!="":
    sys.stderr.write( "Error making current branch from release "+release+" for project in "+directory+"\n" )
    sys.stderr.write( ok+"\n" )
    sys.exit(1)

sys.exit(0)
