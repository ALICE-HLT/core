#!/usr/bin/env python

import os, string, re, sys, os.path, optparse

import svnutils

usage = "usage: %prog [options] <svn-project-directories>"
cliParser = optparse.OptionParser(usage=usage)
#cliParser.add_option( "--nohead", action="store_true", dest="nohead", default=False, help="Do not try to determine version of HEAD/trunk" )
cliParser.add_option( "-v", "--verbose", action="store_true", dest="verbose", default=False, help="Be more verbose" )
( options, dirs ) = cliParser.parse_args( sys.argv[1:] )



if len(dirs)<=0:
    dirs.append( "." )

for d in dirs:
    try:
        files=os.listdir( d )
    except OSError,e:
        if e.errno==2:
            sys.stderr.write( "Directory "+d+" does not exist - skipping...\n" )
            continue
        raise
    if not svnutils.is_svn_directory( d ):
        sys.stderr.write( "Directory '"+d+"' is not under svn control - skipping...\n" )
        continue
    thisURL = svnutils.get_repository_url( d )

    ( baseURL, dirType ) = svnutils.split_repository_url( thisURL )
    if baseURL==None:
        sys.stderr.write( "Cannot determine base URL for project in directory '"+d+"' - skipping...\n" )
        continue
    if options.verbose:
        print "Base URL: "+baseURL
        print "Dir Type: "+dirType

    releases = svnutils.get_releases( baseURL )
    for release in releases:
        print release
        
    
    
