#!/usr/bin/perl 

use Net::LDAP;

$verbose=0;
$doit=0;

$newlogingroup="none";

$getadduserlist=0;
$getdeluserlist=0;
@adduserlist=();
@deluserlist=();


foreach (@ARGV) {

   if ($getadduserlist) 
   {
     if ( /^-/ ) { $getadduserlist=0; }
     else { push(@adduserlist,$_); next; }
   }
   if ($getdeluserlist) 
   {
     if ( /^-/ ) { $getdeluserlist=0; }
     else { push(@deluserlist,$_); next; }
   }

   if ( /-(quiet|q)/i	  ) { $verbose = 0; }
   if ( /^-(verbose|v)$/i   ) { $verbose++;   }

   if ( /^-?(closed|maintenance|open|hlt-restricted|restricted|ready|run)$/i ) { $newlogingroup = $1; }

   if ( /^-?adduser$/i ) { $getadduserlist++; }
   if ( /^-?deluser$/i ) { $getdeluserlist++; }
   if ( /^-?\?$/i ) { usage(); }

}

$newlogingroup =~ tr/A-Z/a-z/;
print "new mode : $newlogingroup\n\n" if $verbose;

if ($verbose) {
foreach (@adduserlist) { print "add user : $_\n"; }
foreach (@deluserlist) { print "remove user : $_\n"; }
}

# -----------------------------------------------------------------------------------------
my $ldap = Net::LDAP->new(
	'ldap://localhost',
	version => 3
	) or die "Can't bind to ldap: $!\n";

my $root = "dc=alihlt,dc=cern,dc=ch";

my $manager = "cn=DHCPagent,ou=LDAPUsers,dc=alihlt,dc=cern,dc=ch";
my $password = "ldap..pass";
my $asadmin=1;

if ($asadmin) {
  if ($password eq "") {
    print "Please enter the password for '$manager': ";
    system "stty -echo";
    chop ($password = <STDIN>);
    system "stty echo";
    print "\n";
  }
  $mesg = $ldap->bind(
  	dn       => $manager,
	password => $password
	 );
  $mesg->code && die $mesg->error;
}
else 
{
 $manager = "$root";
 $mesg = $ldap->bind();
 $mesg->code && die $mesg->error;
}

# -----------------------------------------------------------------------------------------

# Gruppen einlesen

my($mesg) = $ldap->search(
	base   => 'ou=group,dc=alihlt,dc=cern,dc=ch',
	filter => '(objectClass=posixGroup)',
	sizelimit => 3000
	);
$mesg->code && die "LDAP-SEARCH-ERROR:\n" . $mesg->error;
my @entries = $mesg->entries;
my $countentries = @entries ;
print "$countentries groups found.\n" if $verbose>1;
my %groups;
LOOP:
foreach $entry (@entries) {

  my($ldapdn) = $entry->dn;
  print "dn = $ldapdn\n" if $verbose>2;
  @cn = @{ ( $entry->get_value('cn', asref=>1) ) };
  $ldapcn = $cn[0];
  
  print "cn = $ldapcn\n" if $verbose>1;
  my @members=();
  if ( $entry->exists('memberUid') ) {
     @members = @{ ( $entry->get_value('memberUid', asref=>1) ) };
     }
     
  if ($verbose) {
  foreach (@members)
    {
      print "\tmember = $_\n">1; 
    }
    }
  $groups{$ldapcn} = [ @members ];

}

# -----------------------------------------------------------------------------------------

$groups{'none'} = [ () ];
$groups{'all'} = [ () ];

# -----------------------------------------------------------------------------------------
my @allusers=();
my($mesg) = $ldap->search(
	base   => 'ou=people,dc=alihlt,dc=cern,dc=ch',
	filter => '(objectClass=posixAccount)',
	sizelimit => 3000
	);
$mesg->code && die "LDAP-SEARCH-ERROR:\n" . $mesg->error;
@entries = $mesg->entries;
$countentries = @entries ;
print "$countentries users found for 'all'.\n" if $verbose>1;
LOOP:
foreach $entry (@entries) {

  my($ldapdn) = $entry->dn;
  @uid = @{ ( $entry->get_value('uid', asref=>1) ) };
  $ldapuid = $uid[0];
  push(@allusers,$ldapuid);
}
$groups{'all'} = [ @allusers ];

# -----------------------------------------------------------------------------------------

#   if ( /^(closed|maintenance|open|hlt-restricted|restricted|ready|run)$/i ) { $newlogingroup = $1; }

@usegroups = ('login');

if ( $newlogingroup eq 'open' ) { @usegroups = ('all'); }
if ( $newlogingroup eq 'maintenance' ) { @usegroups = ('all'); }

if ( $newlogingroup eq 'closed' ) { @usegroups = ('admin'); }

if ( $newlogingroup =~ 'restricted' ) { @usegroups = ('hlt-user','hlt-operators','hlt-admin'); }
if ( $newlogingroup eq 'ready' ) { @usegroups = ('hlt-operators','hlt-admin'); }
if ( $newlogingroup eq 'run' ) { @usegroups = ('hlt-operators','hlt-experts','hlt-admin'); }

# -----------------------------------------------------------------------------------------

my %newusers;
my @newuser=();

foreach (@usegroups) {
  print "adding group $_\n" if $verbose;
  @g = @{$groups{$_}};
  foreach $u ( @g ) {
#     if ( ! defined($newusers{$u}) ) {  print "  adding user $u\n" if $verbose; }
     $newusers{$u} = 1;
  } }

if ( @adduserlist ) {
  foreach (@adduserlist) {
    print " add to userlist : $_\n" if $verbose;
    $newusers{$_} = 1;
  }
}

if ( @deluserlist ) {
  foreach (@deluserlist) {
    if ( defined($newusers{$_}) ) {
    print " remove from userlist : $_\n" if $verbose;
    $newusers{$_} = 0;
    }
  }
}

@new = ();
foreach (keys %newusers) {
  print " userlist : $_\n" if $verbose;
  if ( $newusers{$_} == 1 ) { push(@new,$_); }
  }

# -----------------------------------------------------------------------------------------

# and now create the new member list in group 'login'

@old = @{$groups{'login'}};
$ldapdn = 'cn=login,ou=group,dc=alihlt,dc=cern,dc=ch';

#print "old @old new @new\n" if $verbose;

if ( @old ) {

$mesg = $ldap->modify( $ldapdn, replace => { 'memberUid' => [ @new ] } );
$mesg->code && die "LDAP-REPLACE-ERROR:\n" . $mesg->error;

}
else
{

$mesg = $ldap->modify( $ldapdn, add => { 'memberUid' => [ @new ] } );
$mesg->code && die "LDAP-ADD-ERROR:\n" . $mesg->error;

}


# -----------------------------------------------------------------------------------------



sub usage {


print "
usage $0 [-q] [-v [-v]] [MODE]  [-adduser user1 [user2 ..]] [-deluser user3 [user4 ..]]

  -q    quiet
  -v    increase verbosity
  
  MODE  Which mode to set :
        (closed|maintenance|open|hlt-restricted|restricted|ready|run)
  
  -adduser  additional users to add
  -deluser  which users to remove

";

exit;

}




