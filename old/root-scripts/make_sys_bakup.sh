#!/bin/bash


aptitude clean

DATE=`date +"%Y-%m-%d"`

HOSTNAME=`hostname`

EXCLUDEFILE=/root/exclude

cd /

echo "./backup-$HOSTNAME-$DATE.tgz" > $EXCLUDEFILE
echo ./local >> $EXCLUDEFILE
echo ./tmp >> $EXCLUDEFILE
echo ./var/tmp >> $EXCLUDEFILE
echo ./var/spool/squid >> $EXCLUDEFILE
ls -1 ./backup*.tgz >> $EXCLUDEFILE
find ./var/log/ -name "*.gz" >> $EXCLUDEFILE
for n in 0 1 2 3 4 5 6 7 8 9 ; do 
find ./var/log/ -name "*.$n" >> $EXCLUDEFILE
done
find ./var/www/fastmotion -name "*.mpg" >> $EXCLUDEFILE
find ./var/www/fastmotion -name "*.avi" >> $EXCLUDEFILE
echo "./var/log/apache2/error_log" >> $EXCLUDEFILE
echo "./root/exclude" >> $EXCLUDEFILE

tar -cPp -v -z --numeric-owner --one-file-system -X $EXCLUDEFILE -f /backup-$HOSTNAME-$DATE.tgz .

tar -cPp -v -z --numeric-owner --one-file-system -f /backup-$HOSTNAME-$DATE-dev.tgz ./dev

tar -cPp -v -z --numeric-owner --one-file-system -f /backup-$HOSTNAME-$DATE-local.tgz ./local/cert \
./local/data ./local/charm ./local/etherlock ./local/home ./local/rrd_graphs ./local/src 

echo .
echo .
echo . remember:
echo .
echo "su - alihltg0 rfcp /backup-$HOSTNAME-$DATE.tgz /castor/cern.ch/user/a/alihltg0/backup/gw0/"
echo "su - alihltg0 rfcp /backup-$HOSTNAME-$DATE-dev.tgz /castor/cern.ch/user/a/alihltg0/backup/gw0/"
echo "su - alihltg0 rfcp /backup-$HOSTNAME-$DATE-local.tgz /castor/cern.ch/user/a/alihltg0/backup/gw0/"
echo .
echo .




