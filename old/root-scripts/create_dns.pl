#!/usr/bin/perl -w

use Net::LDAP;

$logging=1;

$verbose=1;
$doit=0;

$datapath="/etc/bind/";

$dorestart=0;


foreach (@ARGV) {

   if ( /-doit/i      ) { $doit    = 1; }
   if ( /-(quiet|q)/i     ) { $verbose = 0; }
   if ( /^-(verbose|v)$/i   ) { $verbose++;   }
   if ( /-nologging/i ) { $logging = 0; }
   if ( /-(force|f)$/i ) { $dorestart = 1; }

}


$info1 = "device_type";
$info2 = "operating_system";


# -----------------------------------------------------------------------------------------
my $ldap = Net::LDAP->new(
#	'ldaps://ldap.kip.uni-heidelberg.de',
	'ldap://localhost',
	version => 3
	) or die "Can't bind to ldap: $!\n";

my $root = "dc=alihlt,dc=cern,dc=ch";

my $manager = "cn=DHCPagent,ou=LDAPUsers,dc=alihlt,dc=cern,dc=ch";
my $password = "ldap..pass";
my $asadmin=1;

if ($asadmin) {
  if ($password eq "") {
    print "Please enter the password for '$manager': ";
    system "stty -echo";
    chop ($password = <STDIN>);
    system "stty echo";
    print "\n";
  }
  $mesg = $ldap->bind(
  	dn       => $manager,
	password => $password
	 );
  $mesg->code && die $mesg->error;
}
else 
{
 $manager = "$root";
 $mesg = $ldap->bind();
 $mesg->code && die $mesg->error;
}

# -----------------------------------------------------------------------------------------

$net="192.168";
$domain="internal";

# -----------------------------------------------------------------------------------------

#  add here for example "10.xx" ...

@networks = ( "10.162" ,  "192.168" );
@outfiles = ();

# -----------------------------------------------------------------------------------------


foreach $net ( @networks ) {


($neta,$netb) = $net =~ /^(\d+)\.(\d+)/;


print "\nnetwork $net.*\n\n" if $verbose>1;

# -----------------------------------------------------------------------------------------
$dbfile="$datapath/db.$net";
$namefile="$datapath/db.$domain.$net";
%dbdata = () ;
%names = ();

my($mesg) = $ldap->search(
	base   => 'ou=dhcp,dc=alihlt,dc=cern,dc=ch',
	filter => "(ipHostNumber=*)",
	sizelimit => 3000
	);


$mesg->code && die "LDAP-SEARCH-ERROR:\n" . $mesg->error;
my @entries = $mesg->entries;
my $countentries = @entries ;


LOOP:
foreach $entry (@entries) {

  my($ldapdn) = $entry->dn;
  print "dn = $ldapdn\n" if $verbose>1;

  my @ips = @{ ( $entry->get_value('ipHostNumber', asref=>1) ) };
  my $ip="";
  foreach ( @ips ) { print "\tldap-ip = $_\n" if $verbose>2; if ( /^$net/ ) { $ip=$_; } }
  
  next if $ip eq "";
  
  my @cn = @{ ( $entry->get_value('cn', asref=>1) ) };

  if ($verbose>1)
  {
   foreach ( @cn ) { print "\tcn = $_\n"; }
  }

  print "\tip = $ip\n" if $verbose>1;

  ($byte1,$byte2,$byte3,$byte4) = $ip =~ /(\d+)\.(\d+)\.(\d+)\.(\d+)/;
  
  next LOOP if ( $byte1 ne $neta ) or ( $byte2 ne $netb ) ; 

  next LOOP if ( $byte3>255 ) or ( $byte4>255 );
  
  ($name) = $ldapdn =~ /^cn=\s*([a-zA-Z0-9\-_]+)/;
  $name =~ s/_/-/g;
  print "\tname = $name\n" if $verbose>1;
  
#  fuehrende Nullen loeschen
#  $byte4 =~ s/^0+0/0/;
  $byte4 =~ s/^0+([0-9])/$1/;
  $byte3 =~ s/^0+([0-9])/$1/;
  
  $sortfield ="";
  $sortfield .= '0' if $byte3<10;
  $sortfield .= '0' if $byte3<100;
  $sortfield .= $byte3;
  $sortfield .= '0' if $byte4<10;
  $sortfield .= '0' if $byte4<100;
  $sortfield .= $byte4;

  $dbdata{$sortfield} = "$byte4.$byte3\tIN PTR\t$name.$domain.\n";

  if ( $entry->exists('dhcpstatements') )
  {
  my @dhcpst = @{ ( $entry->get_value('dhcpstatements', asref=>1) ) };
  foreach ( @dhcpst ) {
       print "\tdhcpstatements = $_\n" if $verbose>1;
       if ( /$ip/ ) {
         print "\tinsert into names-list!\n" if $verbose>1;
         $names{$sortfield} =";\n$name\tIN A\t$byte1.$byte2.$byte3.$byte4\n";
         $names{$sortfield} .= "\t\tIN HINFO\t$info1\t$info2\n";
	 foreach ( @cn ) { 
	  if ( $_ ne $name ) {
	      $names{$sortfield} .= "$_\tIN CNAME\t$name\n";
	    }
	  }
	 last;
	 }
       }
  }
  else
  {
   # es gibt KEIN 'dhcpstatements'
   # hier geht es also nur um einen nameserver-eintrag

    $names{$sortfield} =";\n$name\tIN A\t$ip\n";
    $names{$sortfield} .= "\t\tIN HINFO\t$info1\t$info2\n";
    foreach ( @cn ) { 
     if ( $_ ne $name ) {
    	 $names{$sortfield} .= "$_\tIN CNAME\t$name\n";
       }
     }
 
  }

}




push(@outfiles,$dbfile);
push(@outfiles,$namefile);

print "writing file '${dbfile}.tmp'\n" if $verbose;

open(DBCONF, ">${dbfile}.tmp") || die "Can't create outputfile: $!\n";
 print DBCONF ";-----------------------------------------------------------------------\n;\n";
 print DBCONF ";     file name:     db.$net\n";
 print DBCONF ";\n;     purpose:       address to name mapping\n;\n";
 print DBCONF ";     this file was automagically created - do not change!\n;\n";
 print DBCONF ";     domain : $domain\n;\n";
 print DBCONF ";-----------------------------------------------------------------------\n;\n;\n";
 print DBCONF "\$INCLUDE ${datapath}named.soa\n;\n;\n";
 
 foreach ( sort keys %dbdata ) {
     print DBCONF $dbdata{$_};
 }
 
 print DBCONF ";\n;     end of file db.$net\n";
 close(DBCONF);


print "writing file '${namefile}.tmp'\n" if $verbose;



 open(NAMECONF, ">${namefile}.tmp") || die "Can't create outputfile: $!\n";
 print NAMECONF ";-----------------------------------------------------------------------\n;\n";
 print NAMECONF ";     file name:     db.$domain.$net\n";
 print NAMECONF ";\n;     purpose:       name to address mapping\n;\n";
 print NAMECONF ";     this file was automagically created - do not change!\n;\n";
 print NAMECONF ";     domain : $domain\n;\n";
 print NAMECONF ";-----------------------------------------------------------------------\n;\n;\n";
#print NAMECONF "\$INCLUDE ${datapath}named.soa\n";
 print NAMECONF ";\n;\n";

 foreach ( sort keys %names ) {
     print NAMECONF $names{$_};
 }

 print NAMECONF ";\n;     end of file db.$domain.$net\n";
 close(NAMECONF);


}

 $md5file = "$datapath/md5sums.txt";
 $md5sums = `cat $md5file 2>/dev/null`;

 @dbsumsold = split "\n", $md5sums;
 open(MFILE,">$md5file") or die "unable to write to '$md5file'\n$!\n";

$i=0;
foreach ( @outfiles ) {
 $f = $_ . ".tmp";
 $sum = `md5sum $f`;
 $sum =~ s/\s.*\n?//m;
 
 if ( $dbsumsold[$i] ne "$sum\t$_" ) {
    print "\t'" . $dbsumsold[$i] ."' != '$sum\t$_'\n" if $verbose>1;
    $dorestart++;
    system("cp $f $_");
    }
 print MFILE "$sum\t$_\n";
 $i++;
}

 close(MFILE);

 if ( $dorestart) {
  print "restart is necessary!\n" if $verbose;
 }
 else
 {
  print "NO restart necessary!\n" if $verbose;
  exit;
 }

#  do a restart ...

$soafile=$datapath."named.soa";

$buffer=`cat $soafile`;

($oldserial) = $buffer =~ /^\s+(\d+)\s*;\s*serial/m;

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);  
$year+=1900;
$mon++;
$mday="0".$mday if $mday<10;
$mon ="0".$mon  if $mon<10;
$hour="0".$hour if $hour<10;
$min ="0".$min  if $min<10;
$sec ="0".$sec  if $sec<10;

$newserial = $year . $mon . $mday ."0";

while ( $newserial <= $oldserial ) {

  $newserial++;
  print "old $oldserial  new $newserial\n" if $verbose>1;

}

$buffer =~ s/^(\s+)(\d+)(\s*;\s*serial)/$1$newserial$3/m;

open(SOAFILE,">${soafile}") or die "unable to write to '$soafile'\n$!\n";
print SOAFILE $buffer;
close(SOAFILE);

#system("/etc/init.d/bind9 restart");
system("/usr/sbin/rndc reload");


open(F,">>${datapath}named.reloaded") or die "unable to write to '${datapath}named.reloaded'\n$!\n";
print F "named reloaded at $year-$mon-$mday $hour:$min:$sec    serial $newserial\n";
close(F);



