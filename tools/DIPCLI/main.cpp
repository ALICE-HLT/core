/* Project : DCLI
 * Filename: main.cpp
 * Author  : Jochen Ulrich
 * Created : 02.08.2011 13:58:20
 *
 * DIP Commandline-Interface
 *
 * Used to retrieve values of DIP publications from the console.
 * For invocation help run with parameter "--help".
 *
 * To compile and run, DIPCLI requires the DIP C++ library (libdip.so or Dip.dll) from one of the release packages.
 * DIPCLI was written with DIP 5.4.2.
 * See http://j2eeps.cern.ch/wikis/display/EN/DIP+and+DIM
 */

#include "DIP/Dip.h"
#include "DIP/DipBrowser.h"
#include "DIP/DipSubscription.h"
#include "DIP/DipSubscriptionListener.h"
#include "DIP/DipDataType.h"
#include "getopt_pp/getopt_pp.h"
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <errno.h>
#ifdef WIN32
#include <windows.h>
#include <process.h>
#else
#include <unistd.h>
#include <sys/types.h>
#ifndef _getpid
#define _getpid getpid
#endif
#endif

class DataListener : public DipSubscriptionListener
{
public:

	DataListener(DipDataType type, std::string fieldName = std::string()) : p_type(type), p_fieldName(fieldName), p_alreadyRun(false) {}

	virtual ~DataListener() {}

	void handleMessage(DipSubscription* subscription, DipData& message)
	{
		// Run only once
		if (p_alreadyRun)
			return;
		else
			p_alreadyRun = true;

		if (p_fieldName.empty())
		{
			switch (p_type)
			{
			case TYPE_BOOLEAN:	std::cout << message.extractBool(); break;
			case TYPE_BYTE:		std::cout << message.extractByte();	break;
			case TYPE_SHORT:	std::cout << message.extractShort(); break;
			case TYPE_INT:		std::cout << message.extractInt(); break;
			case TYPE_LONG:		std::cout << message.extractLong(); break;
			case TYPE_FLOAT:	std::cout << message.extractFloat(); break;
			case TYPE_DOUBLE:	std::cout << message.extractDouble(); break;
			case TYPE_STRING:	std::cout << message.extractString(); break;

			case TYPE_NULL:
				// TODO: What means NULL and how to handle it?
				break;

			default:
			{
				int arraySize;

				switch (p_type)
				{
				case TYPE_BOOLEAN_ARRAY:
				{
					const DipBool* array = message.extractBoolArray(arraySize);
					for (int i=0; i < arraySize-1; ++i)
						std::cout << array[i] << ",";
					std::cout << array[arraySize-1];
					break;
				}
				case TYPE_BYTE_ARRAY:
				{
					const DipByte* array = message.extractByteArray(arraySize);
					for (int i=0; i < arraySize-1; ++i)
						std::cout << array[i] << ",";
					std::cout << array[arraySize-1];
					break;
				}
				case TYPE_SHORT_ARRAY:
				{
					const DipShort* array = message.extractShortArray(arraySize);
					for (int i=0; i < arraySize-1; ++i)
						std::cout << array[i] << ",";
					std::cout << array[arraySize-1];
					break;
				}
				case TYPE_INT_ARRAY:
				{
					const DipInt* array = message.extractIntArray(arraySize);
					for (int i=0; i < arraySize-1; ++i)
						std::cout << array[i] << ",";
					std::cout << array[arraySize-1];
					break;
				}
				case TYPE_LONG_ARRAY:
				{
					const DipLong* array = message.extractLongArray(arraySize);
					for (int i=0; i < arraySize-1; ++i)
						std::cout << array[i] << ",";
					std::cout << array[arraySize-1];
					break;
				}
				case TYPE_FLOAT_ARRAY:
				{
					const DipFloat* array = message.extractFloatArray(arraySize);
					for (int i=0; i < arraySize-1; ++i)
						std::cout << array[i] << ",";
					std::cout << array[arraySize-1];
					break;
				}
				case TYPE_DOUBLE_ARRAY:
				{
					const DipDouble* array = message.extractDoubleArray(arraySize);
					for (int i=0; i < arraySize-1; ++i)
						std::cout << array[i] << ",";
					std::cout << array[arraySize-1];
					break;
				}
				case TYPE_STRING_ARRAY:
				{
					const std::string* array = message.extractStringArray(arraySize);
					for (int i=0; i < arraySize-1; ++i)
						std::cout << array[i] << ",";
					std::cout << array[arraySize-1];
					break;
				}
				default:
					// Unknown type
					break;
				}
			} // end of default
			break;
			}
		}
		else
		{
			switch (p_type)
			{
			case TYPE_BOOLEAN:	std::cout << message.extractBool(p_fieldName.c_str()); break;
			case TYPE_BYTE:		std::cout << message.extractByte(p_fieldName.c_str());	break;
			case TYPE_SHORT:	std::cout << message.extractShort(p_fieldName.c_str()); break;
			case TYPE_INT:		std::cout << message.extractInt(p_fieldName.c_str()); break;
			case TYPE_LONG:		std::cout << message.extractLong(p_fieldName.c_str()); break;
			case TYPE_FLOAT:	std::cout << message.extractFloat(p_fieldName.c_str()); break;
			case TYPE_DOUBLE:	std::cout << message.extractDouble(p_fieldName.c_str()); break;
			case TYPE_STRING:	std::cout << message.extractString(p_fieldName.c_str()); break;

			case TYPE_NULL:
				// TODO: What means NULL and how to handle it?
				break;

			default:
			{
				int arraySize;

				switch (p_type)
				{
				case TYPE_BOOLEAN_ARRAY:
				{
					const DipBool* array = message.extractBoolArray(arraySize,p_fieldName.c_str());
					for (int i=0; i < arraySize-1; ++i)
						std::cout << array[i] << ",";
					std::cout << array[arraySize-1];
					break;
				}
				case TYPE_BYTE_ARRAY:
				{
					const DipByte* array = message.extractByteArray(arraySize,p_fieldName.c_str());
					for (int i=0; i < arraySize-1; ++i)
						std::cout << array[i] << ",";
					std::cout << array[arraySize-1];
					break;
				}
				case TYPE_SHORT_ARRAY:
				{
					const DipShort* array = message.extractShortArray(arraySize,p_fieldName.c_str());
					for (int i=0; i < arraySize-1; ++i)
						std::cout << array[i] << ",";
					std::cout << array[arraySize-1];
					break;
				}
				case TYPE_INT_ARRAY:
				{
					const DipInt* array = message.extractIntArray(arraySize,p_fieldName.c_str());
					for (int i=0; i < arraySize-1; ++i)
						std::cout << array[i] << ",";
					std::cout << array[arraySize-1];
					break;
				}
				case TYPE_LONG_ARRAY:
				{
					const DipLong* array = message.extractLongArray(arraySize,p_fieldName.c_str());
					for (int i=0; i < arraySize-1; ++i)
						std::cout << array[i] << ",";
					std::cout << array[arraySize-1];
					break;
				}
				case TYPE_FLOAT_ARRAY:
				{
					const DipFloat* array = message.extractFloatArray(arraySize,p_fieldName.c_str());
					for (int i=0; i < arraySize-1; ++i)
						std::cout << array[i] << ",";
					std::cout << array[arraySize-1];
					break;
				}
				case TYPE_DOUBLE_ARRAY:
				{
					const DipDouble* array = message.extractDoubleArray(arraySize,p_fieldName.c_str());
					for (int i=0; i < arraySize-1; ++i)
						std::cout << array[i] << ",";
					std::cout << array[arraySize-1];
					break;
				}
				case TYPE_STRING_ARRAY:
				{
					const std::string* array = message.extractStringArray(arraySize,p_fieldName.c_str());
					for (int i=0; i < arraySize-1; ++i)
						std::cout << array[i] << ",";
					std::cout << array[arraySize-1];
					break;
				}
				default:
					// Unknown type
					break;
				}
			} // end of default
			break;
			}
		}

		std::cout << std::endl;
	}

	void connected(DipSubscription* subscription)
	{
		subscription->requestUpdate();
	}

	void disconnected(DipSubscription* subscription, char* message)
	{
		std::cerr << "CONNECTION ERROR: " << message << std::endl;
		exit(-1);
	}

	void handleException(DipSubscription* subscription, DipException& ex)
	{
		std::cerr << "ERROR: " << ex.what() << std::endl;
		exit(-1);
	}

private:
	DipDataType p_type;
	std::string p_fieldName;
	bool p_alreadyRun;
};

void showUsage()
{
	std::cout	<< "\n"
				<< "USAGE: dipcli [OPTIONS] PUB[:FIELD]" << "\n"
				<< "\n"
				<< "PUB is the full name of the publication. The optional FIELD is the name of the field whose value"
						"shall be returned in case that PUB is a complex structure." << "\n"
				<< "\n"
				<< "OPTIONS:" << "\n"
				<< "\n"
				<< "-n, --DIP_DNS DNS1[,DNS2...]" << "\n"
				<< "\n"
				<< "\t" << "Sets the list of DIP DNS servers to look for the publication."
						"If several servers are given they must be separated by commas."
						"This option overwrite the value given in the environment variable"
						"\"DIP_DNS\". If neither the option nor the environment variable is present, "
						"the default value \"dipnsgpn1,dipnsgpn2\" is used." << "\n"
				<< "\n"
				<< "-t, --timeout TIMEOUT" << "\n"
				<< "\n"
				<< "\t" << "Sets the timeout (in milliseconds) for retrieving the data of the publication to TIMEOUT."
						"Default value is 1000 (1 second). The execution of dipcli will always take this long even if "
						"the data is retrieved faster." << "\n"
				<< std::endl;
}

void gen_random_str(char *s, const int len)
{
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < len; ++i) {
        s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }

    s[len] = 0;
}

unsigned fnv_hash ( const void *key, int len )
{
	const unsigned char *p = (const unsigned char*)key;
 	unsigned h = 2166136261;
 	int i;

 	for ( i = 0; i < len; i++ )
 		h = ( h * 16777619 ) ^ p[i];

	return h;
}

int main(int argc, char* argv[])
{
	//******* Parse Command Line Parameters *******//
	std::string dns;
	std::string publication;
	std::string field;
	int timeout;

	GetOpt::GetOpt_pp opts(argc,argv, GetOpt::Include_Environment);
	std::vector<std::string> globalOpts;

	if (argc == 1 || opts >> GetOpt::OptionPresent('h',"help"))
	{
		std::cout << "Command-line interface for DIP. Returns the current value of a given DIP publication." << "\n";
		showUsage();
		exit(0);
	}

	opts >> GetOpt::Option('n',"DIP_DNS",dns,"dipnsgpn1,dipnsgpn2");
	opts >> GetOpt::Option('t',"timeout",timeout,1000);
	opts >> GetOpt::GlobalOption(globalOpts);

//	std::cout << "DNS: " << dns << std::endl;

	if (globalOpts.size() == 0)
	{
		std::cerr << "ERROR: Publication name must be given." << "\n";
		showUsage();
		exit(EINVAL);
	}

	if (globalOpts.size() > 1)
	{
		std::cerr << "ERROR: Too many arguments. Only one publication name must be given." << "\n";
		showUsage();
		exit(E2BIG);
	}

	std::string::size_type fieldSepPosition = globalOpts.at(0).find_last_of(':');
	if (fieldSepPosition != std::string::npos)
	{
		publication = globalOpts.at(0).substr(0,fieldSepPosition);
		field = globalOpts.at(0).substr(fieldSepPosition+1);
	}
	else
		publication = globalOpts.at(0);


	//******* Create DIP Factory *******//
	DipFactory* dip;

	// Generate a unique name for this instance to avoid error messages from DIM "Some Services already known to DNS"
	char randomStr[11];
	srand((unsigned int) time(0) + fnv_hash(publication.c_str(),publication.size())); // Seed with current time plus hash of parameter
	gen_random_str(randomStr,10);
	std::ostringstream dipFactoryNameStream;
	dipFactoryNameStream << "dipcli" << "_" << _getpid() << "_" << randomStr;
	std::string dipFactoryName = dipFactoryNameStream.str();
//	std::cout << "DipFactoryName: " << dipFactoryName << std::endl;

	dip = Dip::create(dipFactoryName.c_str());
	dip->setDNSNode(dns.c_str());


	//******* Determine Type of Publication *******//
	DipDataType type;
	DipBrowser* browser;
	browser = dip->createDipBrowser();
	unsigned int fieldCount;
	const char** fieldNames;
	try
	{
		fieldNames = browser->getTags(publication.c_str(),fieldCount);
	}
	catch (DipException ex)
	{
		std::cerr << "ERROR: " << ex.what() << std::endl;
		exit(EFAULT);
	}

	if (fieldCount == 0)
	{
		std::cerr << "ERROR: Publication has not published any data yet." << std::endl;
		exit(EAGAIN);
	}

	try
	{
		if (field.empty())
			type = (DipDataType)browser->getType();
		else
			type = (DipDataType)browser->getType(field.c_str());
	}
	catch (DipException ex)
	{
		std::cerr << "ERROR: " << ex.what() << std::endl;
		exit(EINVAL);
	}


	//******* Subscribe to Publication *******//
	DataListener* handler;
	handler = new DataListener(type,field);

	DipSubscription* sub;
	sub = dip->createDipSubscription(publication.c_str(), handler);

#ifdef WIN32
		Sleep(timeout);
#else
		sleep(timeout/1000);
#endif

	dip->destroyDipSubscription(sub);
	delete handler;
	delete dip;

	return 0;
}

