#!/bin/bash

# USAGE : <cmd> <port>

SlaveController tcpmsg://:57762 tcpmsg://`hostname`:$2 cmd $1 | grep -v `hostname`

