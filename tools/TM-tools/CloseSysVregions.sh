#!/bin/bash
for i in `ipcs | awk '{print $2}'|grep -E '^[0-9]+$'` ; do
        ipcrm -m $i
        if test $? -ne 0 ; then
                echo ipcrm failed on node $HOSTNAME
        fi
done
