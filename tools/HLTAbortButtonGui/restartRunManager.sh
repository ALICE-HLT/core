#!/bin/bash

# This script should restart the RunManager process on portal-ecs1 running under the
# 'runmanager' screen session.
# This script is necessary because the RunManager.sh script does not return immediately.

# Check if there is already a screen session, if not then start a new session.
if test `screen -list | grep runmanager | wc -l` -eq 0 ; then
	echo "Starting a new screen session for 'runmanager'."
	screen -d -m -S runmanager
fi

# Send the stop command to the first window i.e. "-p 0", if this is not specified then the
# screen session terminal ignores the commands.
screen -S runmanager -p 0 -X stuff "RunManager-with-pendolino.sh stop"`echo -ne '\015'`
sleep 1

# Now send the start command.
screen -S runmanager -p 0 -X stuff "RunManager-with-pendolino.sh start"`echo -ne '\015'`

sleep 3
exit 0
