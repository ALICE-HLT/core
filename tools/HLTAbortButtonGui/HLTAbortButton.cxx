/**************************************************************************
 * This file is property of and copyright by the ALICE HLT Project        *
 * All rights reserved.                                                   *
 *                                                                        *
 * Primary Authors:                                                       *
 *   Artur Szostak <artursz@iafrica.com>                                  *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

/**
 * This program implements an abort button for the HLT. This is for use by the
 * shifters within the ACR. It performs the standard RunManager ABORT command and
 * calls the emergency_stop.sh script as a last resort to bring the chain down.
 * It should be started within the operator account that is to be controlled by
 * this abort button.
 */

#include "TApplication.h"
#include "GuiMainWindow.h"
#include "TSystem.h"
#include "TString.h"
#include "Riostream.h"
#include <cstdlib>

int main(int argc, char** argv)
{
	bool simulationMode = false;
	for (int i = 1; i < argc; ++i)
	{
		if (TString(argv[i]) == "-simulate") simulationMode = true;
	}

	TApplication app("HLTAbortButton", &argc, argv);
	GuiMainWindow* gui = new GuiMainWindow(gSystem->DirName(argv[0]), simulationMode);

	// kTRUE makes the application return from the event loop on termination.
	app.Run(kTRUE);

	delete gui;

	return EXIT_SUCCESS;
}
