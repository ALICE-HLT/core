/**************************************************************************
 * This file is property of and copyright by the ALICE HLT Project        *
 * All rights reserved.                                                   *
 *                                                                        *
 * Primary Authors:                                                       *
 *   Artur Szostak <artursz@iafrica.com>                                  *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

#include "GuiMainWindow.h"
#include "TApplication.h"
#include "TGButton.h"
#include "TGFont.h"
#include "TGMsgBox.h"
#include "TDatime.h"
#include "TSystem.h"
#include "TRegexp.h"
#include "Riostream.h"


ClassImp(GuiMainWindow)


GuiMainWindow::GuiMainWindow(const char* startDir, bool simulationMode) :
	TGMainFrame(gClient->GetRoot(), 10, 10, kChildFrame | kFixedSize | kVerticalFrame | kHorizontalFrame),
	fButton(new TGTextButton(this, "ABORT HLT")),
	fStartDir(startDir),
	fSimulationMode(simulationMode)
{
	if (fStartDir == "") fStartDir = ".";
	TGFont* font = gClient->GetFont("-*-helvetica-bold-r-*-*-34-*-*-*-*-*-*-*");
	if (font != NULL) fButton->SetFont(font->GetFontStruct());
	ULong_t yellow, red;
	gClient->GetColorByName("#FFFF00", yellow);
	gClient->GetColorByName("#CE0000", red);
	fButton->SetTextColor(yellow);
	fButton->SetBackgroundColor(red);
	SetCleanup(kDeepCleanup);
	AddFrame(fButton, new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX | kLHintsExpandY, 0, 0, 1, 1));
	fButton->Connect("Clicked()", "GuiMainWindow", this, "ButtonClicked()");
	MapSubwindows();
	Resize(GetDefaultSize());
	MapWindow();
	Resize(330,60);
}


GuiMainWindow::~GuiMainWindow()
{
	UnmapWindow();
	Cleanup();
}


void GuiMainWindow::CloseWindow()
{
	gApplication->Terminate();
}


int GuiMainWindow::ExecuteCommand(const char* cmd, TString& output)
{
	if (fSimulationMode) return 0;

	FILE* pipe = gSystem->OpenPipe(cmd, "r");
	if (!pipe) return -1;
	TString line;
	while (line.Gets(pipe))
	{
		output += line;
		output += "\n";
	}
	return gSystem->ClosePipe(pipe);
}


int GuiMainWindow::ExecuteCommand(const char* cmd)
{
	if (fSimulationMode) return 0;
	return gSystem->Exec(cmd);
}


int GuiMainWindow::FindFreePort()
{
	for (int port = 2000; port < 60000; ++port)
	{
		int sock = gSystem->AnnounceTcpService(port, kTRUE, 0);
		if (sock >= 0)
		{
			gSystem->CloseConnection(sock);
			return port;
		}
	}
	return -1;
}


/**
 * Fetches the state of the RunManager using the SlaveController tool.
 * \param state [out] Gets filled with the current state of the RunManager.
 * \param port [in] The TCP port number to start using as the local binding port for SlaveController.
 *                  If the port is already being used then this method will retry using the next port number.
 * \param noOutput [in] If set to true then this method will not generate any output to STDOUT nor
 *                      generate any message boxes.
 * \returns true if the command executed correctly and false otherwise.
 */
bool GuiMainWindow::GetTaskManagerState(TString& state, int port, bool noOutput = false)
{
	TString output;
	int maxPort = port + 100;
	for (; port <= maxPort; ++port)
	{
		output = "";
		TString cmd = Form("SlaveController tcpmsg://:%d tcpmsg://portal-ecs1:$ALIHLT_PORT_BASE_RUNMGR query state", port);
		int result = ExecuteCommand(cmd.Data(), output);
		if (result != 0)
		{
			// Check if the address was in use already and try the next port in such a case.
			if (output.Contains("Address already in use (98)") and port < maxPort) continue;

			if (noOutput) return false;
			cerr << "ERROR: Error executing the SlaveController command. Output from command follows:" << endl;
			cerr << output.Data() << endl;
			TString msg = "There was a problem querying the state of the RunManager.\nPlease contact the HLT on-call expert.\nOutput from the failed command:\n";
			msg += cmd;
			msg += "\n";
			msg += output;
			new TGMsgBox(gClient->GetRoot(),
				gClient->GetRoot(),
				"HLT Abort Error",
				msg.Data(),
				kMBIconExclamation,
				kMBOk,
				NULL,
				kVerticalFrame,
				kTextLeft
				);
			return false;
		}
		else
		{
			break; // If we succeeded we can exit the loop.
		}
	}
	
	TRegexp regexp(".+TMControlMaster\\.cpp.+");
	TString line;
	Ssiz_t from = 0;
	while (output.Tokenize(line, from, "\n"))
	{
		Ssiz_t len = 0;
		if (regexp.Index(line, &len) == kNPOS)
		{
			state = line;
			break;
		}
	}
	return true;
}


void GuiMainWindow::AbortHLT()
{
	int result = 0;
	int port = FindFreePort();
	if (port == -1)
	{
		cerr << TDatime().AsString() << "\tERROR: Could not find a free TCP/IP port." << endl;
		return;
	}
	
	// Check if RunManager is there and try restart it if we get no response.
	TString output;
	if (not GetTaskManagerState(output, port, true)) // 'true' will suppress any output.
	{
		cout << TDatime().AsString() << "\tCould not connect to the RunManager to query the state."
			<< " It might not be running so will try and restart it automatically."
			<< endl;
		result = ExecuteCommand(Form("ssh portal-ecs1 %s/restartRunManager.sh", fStartDir.Data()));
		if (result != 0)
		{
			cerr << "ERROR: Error executing the restartRunManager.sh script on portal-ecs1." << endl;
			return;
		}
	}
	
	TString tmState;
	if (not GetTaskManagerState(tmState, port)) return;
	cout << TDatime().AsString() << "\tFound RunManager in state = " << tmState.Data() << endl;
	if (tmState == "slaves_dead")
	{
		cout << TDatime().AsString() << "\tHLT chain is already down. Skipping abort." << endl;
		return;
	}
	
	if (tmState == "running")
	{
		Int_t retval = 0;
		new TGMsgBox(gClient->GetRoot(),
			     gClient->GetRoot(),
			     "HLT Still Running!",
			     "The HLT is still in the running mode.   \nAre you sure you want to abort?",
			     kMBIconExclamation,
			     kMBYes | kMBNo,
			     &retval,
			     kVerticalFrame,
			     kTextLeft
			);
		if (retval != kMBYes) return;
		retval = 0;
		new TGMsgBox(gClient->GetRoot(),
			     gClient->GetRoot(),
			     "HLT Still Running!",
			     "Last chance to change your mind. Do you want to cancel the abort?       ",
			     kMBIconExclamation,
			     kMBYes | kMBNo,
			     &retval,
			     kVerticalFrame,
			     kTextLeft | kTextCenterY
			);
		if (retval != kMBNo) return;
	}
	
	cout << TDatime().AsString() << "\tStarting the HLT abort..." << endl;
	
	cout << TDatime().AsString() << "\tSending Abort command to the RunManager on portal-ecs1." << endl;
	TString cmd = Form("%s/execSlaveControllerCmd.sh %d ABORT", fStartDir.Data(), port);
	output = "";
	result = ExecuteCommand(cmd.Data(), output);
	if (result != 0)
	{
		cerr << TDatime().AsString() << "\tERROR: Error executing the SlaveController command. Output from command follows:" << endl;
		cerr << output.Data() << endl;
		ExecuteCommand("DATE_FACILITY=\"HLTAbortButton\" log -f \"HLTAbortButton: Failure in SlaveController ABORT command.\"");
		TString msg = "There was a problem aborting the HLT with the SlaveController ABORT command.          \nPlease contact the HLT on-call expert.\nOutput from the failed command:\n";
		msg += cmd;
		msg += "\n";
		msg += output;
		new TGMsgBox(gClient->GetRoot(),
			     gClient->GetRoot(),
			     "HLT Abort Error",
			     msg.Data(),
			     kMBIconExclamation,
			     kMBOk,
			     NULL,
			     kVerticalFrame,
			     kTextLeft
			);
		return;
	}
	gSystem->Sleep(10000);
	
	cout << TDatime().AsString() << "\tRestarting the RunManager on portal-ecs1." << endl;
	result = ExecuteCommand(Form("ssh portal-ecs1 %s/restartRunManager.sh", fStartDir.Data()));
	if (result != 0)
	{
		cerr << TDatime().AsString() << "\tERROR: Error executing the restartRunManager.sh script on portal-ecs1." << endl;
	}
	if (not GetTaskManagerState(tmState, port)) return;
	if (tmState != "slaves_dead")
	{
		cout << TDatime().AsString() << "\tThe RunManager still seems to be stuck. Might need to be restarted manually." << endl;
		ExecuteCommand("DATE_FACILITY=\"HLTAbortButton\" log -f \"HLTAbortButton: RunManager seems stuck, manual restart required?\"");
		TString msg = "The RunManager seems to be stuck.\nYou should login to portal-ecs1 and restart the RunManager. If unsure please contact the HLT on-call expert.        ";
		new TGMsgBox(gClient->GetRoot(),
			gClient->GetRoot(),
			"HLT Abort Failed?",
			msg.Data(),
			kMBIconExclamation,
			kMBOk,
			NULL,
			kVerticalFrame,
			kTextLeft
			);
	}
	
	output = "";
	cout << TDatime().AsString() << "\tRunning the emergency_stop.sh script to force cleanup on child machines." << endl;
	result = ExecuteCommand("emergency_stop.sh", output);
	if (result != 0)
	{
		cerr << TDatime().AsString() << "\tERROR: Error executing the emergency_stop script. Output from script follows:" << endl;
		cerr << output.Data() << endl;
		ExecuteCommand("DATE_FACILITY=\"HLTAbortButton\" log -f \"HLTAbortButton: Failure during emergency_stop.sh script execution.\"");
		TString msg = "There was a problem running the emergency_stop.sh script.\nPlease contact the HLT on-call expert.";
		new TGMsgBox(gClient->GetRoot(),
			gClient->GetRoot(),
			"HLT Abort Error",
			msg.Data(),
			kMBIconExclamation,
			kMBOk,
			NULL,
			kVerticalFrame,
			kTextLeft
			);
		return;
	}
	cout << "============================= emergency_stop.sh output =========================" << endl;
	cout << output.Data() << endl;
	cout << "================================================================================" << endl;
	if (not GetTaskManagerState(tmState, port)) return;
	
	cout << TDatime().AsString() << "\tAfter abort procedure, found RunManager in state = " << tmState.Data() << endl;
	if (tmState != "slaves_dead")
	{
		cout << TDatime().AsString() << "\tAbort procedure seems to have failed." << endl;
		TString msg = "The abort procedure for the HLT seems to have failed.\nYou can try one more time. If that fails again then please contact the HLT on-call expert.        ";
		new TGMsgBox(gClient->GetRoot(),
			gClient->GetRoot(),
			"HLT Abort Failed?",
			msg.Data(),
			kMBIconExclamation,
			kMBOk,
			NULL,
			kVerticalFrame,
			kTextLeft
			);
	}
	else
	{
		cout << TDatime().AsString() << "\tAbort procedure done." << endl;
	}
}


void GuiMainWindow::ButtonClicked()
{
	fButton->SetText("ABORTING...");
	gSystem->ProcessEvents();  // update new button text
	ExecuteCommand("DATE_FACILITY=\"HLTAbortButton\" log -e \"HLTAbortButton executed\"");
	AbortHLT();
	fButton->SetText("ABORT HLT");
}
