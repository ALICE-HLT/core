#ifndef GUIMAINWINDOW_H
#define GUIMAINWINDOW_H
/**************************************************************************
 * This file is property of and copyright by the ALICE HLT Project        *
 * All rights reserved.                                                   *
 *                                                                        *
 * Primary Authors:                                                       *
 *   Artur Szostak <artursz@iafrica.com>                                  *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

#include "TGFrame.h"
#include "TString.h"
#include <vector>

class TGTextButton;

class GuiMainWindow : public TGMainFrame
{
public:
	
	GuiMainWindow(const char* startDir = NULL, bool simulationMode = false);
	virtual ~GuiMainWindow();

	// Override.
	virtual void CloseWindow();
	
	// Slots for signals.
	void ButtonClicked();

private:

	int ExecuteCommand(const char* cmd, TString& output);
        int ExecuteCommand(const char* cmd);
        int FindFreePort();
	bool GetTaskManagerState(TString& state, int port, bool noOutput);
	
	void AbortHLT();
	
	TGTextButton* fButton;
	TString fStartDir;
	bool fSimulationMode;
	
	ClassDef(GuiMainWindow, 0) // Main gui window for HLT abort button application.
};

#endif // GUIMAINWINDOW_H
