#!/bin/bash

# This script is necessary because the SlaveController program does not return after sending the command.

if test -z $1 ; then
	echo "ERROR: Must specifiy a port number as the first argument." 1>&2
	exit 1
fi
if test -z $2 ; then
	echo "ERROR: Must specifiy a command as the second argument." 1>&2
	exit 1
fi

echo SlaveController tcpmsg://:$1 tcpmsg://portal-ecs1:$ALIHLT_PORT_BASE_RUNMGR cmd $2
SlaveController tcpmsg://:$1 tcpmsg://portal-ecs1:$ALIHLT_PORT_BASE_RUNMGR cmd $2 &
RESULT=$?
if test $RESULT -ne 0 ; then
	exit $RESULT
fi
sleep 3
killall SlaveController &> /dev/null
exit 0

