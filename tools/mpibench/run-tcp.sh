#!/bin/bash

BINDIR="$(dirname $(readlink -f $0))"
mpirun -N 1 -hostfile nodes.txt \
    --mca btl tcp,self --mca btl_tcp_if_include ib0 \
    "$BINDIR"/mpibench $@
