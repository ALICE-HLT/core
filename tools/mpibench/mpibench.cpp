#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "timer.h"

#define BENCHMARK_SIZE (1024 * 1024 * 128)
#define ITERATIONS 2
#define ITERATIONS_COLLECTIVE 20
#define ITERATIONS_ALLCOLLECTIVE 2
#define REPEATS 4
#define SKIP 1

void *buffer, *buffer2;

void DoTest(MPI_Comm comm, int function)
{
    int nNodes, nRank;
    MPI_Comm_size(MPI_COMM_WORLD, &nNodes);
    MPI_Comm_rank(MPI_COMM_WORLD, &nRank);

    HighResTimer Timer;
    double fullTime = 0.;
    
    int collectiveCall = 0;
    double bandwidthFactor = 1.;

    if (nRank == 0)
    {
	if (function == 0)
	{
	    printf("Performing one to one tests (MPI_Send/MPI_Recv)...\n");
	}
	else if (function == 1)
	{
	    printf("Performing one to all tests (MPI_Bcast)...\n");
	}
	else if (function == 2)
	{
	    printf("Doing all to all tests (MPI_Send/MPI_Recv)...\n");
	}
	else
	{
	    printf("Invalid function\n");
	    exit(1);
	}
    }
    
    if (function == 1)
    {
        collectiveCall = 1;
    }
    else if (function == 2)
    {
        collectiveCall = 2;
        bandwidthFactor = nNodes - 1;
    }
    
    int nIterations = collectiveCall == 2 ? ITERATIONS_ALLCOLLECTIVE : (collectiveCall ? ITERATIONS_COLLECTIVE : ITERATIONS);
    
    char output[1024];
    for (int i = 0;i < (collectiveCall ? 1 : nNodes);i++)
    {
	for (int j = 0;j < (collectiveCall >= 2 ? 1 : nNodes);j++)
	{
	    if (i == j && collectiveCall == 0) continue;
	    if (j == nRank)
	    {
		if (collectiveCall == 0)
		{
		    sprintf(output, "Rank %d to %d:", i, j);
		}
		else if (collectiveCall == 1)
		{
		    sprintf(output, "Source Rank %d:", j);
		}
		else if (collectiveCall == 2)
		{
		    output[0] = 0;
		}
		fullTime = 0.;
	    }
	    for (int r = 0;r < REPEATS;r++)
	    {
		MPI_Barrier(comm);
		if (i == nRank && collectiveCall == 0)
		{
		    for (int k = 0;k < nIterations;k++)
		    {
			if (function == 0)
			{
			    MPI_Send(buffer, BENCHMARK_SIZE, MPI_BYTE, j, k, comm);
			}
		    }
		}
		else if (j == nRank || collectiveCall)
		{
		    Timer.Reset();
		    Timer.Start();
		    for (int k = 0;k < nIterations;k++)
		    {
			if (function == 0)
			{
			    MPI_Recv(buffer, BENCHMARK_SIZE, MPI_BYTE, i, k, comm, MPI_STATUS_IGNORE);
			}
			else if (function == 1)
			{
			    MPI_Bcast(buffer, BENCHMARK_SIZE, MPI_BYTE, j, comm);
			}
			else if (function == 2)
			{
			    MPI_Request request;
			    for (int ii = 1;ii <= nNodes;ii++)
			    {
				MPI_Irecv(buffer2, BENCHMARK_SIZE, MPI_BYTE, (nRank + nNodes - ii) % nNodes, nNodes * k + ii, comm, &request);
				MPI_Send(buffer, BENCHMARK_SIZE, MPI_BYTE, (nRank + ii) % nNodes, nNodes * k + ii, comm);
				MPI_Wait(&request, MPI_STATUS_IGNORE);
				if (nRank == 0) fprintf(stderr, "%d.", ii);
			    }
			    if (nRank == 0) fprintf(stderr, "\n");
			}
		    }
		    Timer.Stop();
		    if(r>=SKIP) {
		      fullTime += Timer.GetElapsedTime();
		      sprintf(output + strlen(output), " %7.2f MB/s", bandwidthFactor * (double) nIterations * (double) BENCHMARK_SIZE / Timer.GetElapsedTime() / 1024. / 1024.);
		    }
		}
		MPI_Barrier(comm);
	    }
	    if (nRank == j)
	    {
	      sprintf(output + strlen(output), " - Total: %7.2f MB/s (%1.3f sec)", bandwidthFactor * (double) (REPEATS - SKIP) * (double) nIterations * (double) BENCHMARK_SIZE / fullTime / 1024. / 1024., fullTime);
		if (j != 0) MPI_Send(output, 1024, MPI_BYTE, 0, 0, comm);
	    }
	    
	    if (nRank == 0)
	    {
		if (j != 0) MPI_Recv(output, 1024, MPI_BYTE, j, 0, comm, MPI_STATUS_IGNORE);
		printf("%s\n", output);
	    }
	}
    }
    if (nRank == 0) printf("\n");
} 

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);
    
    int nNodes, nRank;
    MPI_Comm_size(MPI_COMM_WORLD, &nNodes);
    MPI_Comm_rank(MPI_COMM_WORLD, &nRank);
    
    if (nRank == 0) printf("Running MPI Benchmark (%d nodes)\n\n", nNodes);
    
    buffer = malloc(2 * BENCHMARK_SIZE);
    buffer2 = (void*) (((char*) buffer) + BENCHMARK_SIZE);
    if (buffer == NULL)
    {
	printf("Memory Allocation Error\n");
	return(1);
    }
    memset(buffer, 0, 2 * BENCHMARK_SIZE);
    if (argc > 1)
    {
	for (int i = 1;i < argc;i++)
	{
	    const int tmp = atoi(argv[i]);
	    DoTest(MPI_COMM_WORLD, tmp);
	}
    }
    else
    {
	for (int i = 0;i < 3;i++)
	{
	    DoTest(MPI_COMM_WORLD, i);
	}
    }
    
    free(buffer);
    
    if (nRank == 0) printf("MPI Benchmark finished\n");
    
    MPI_Finalize();
    
    return(0);
}
