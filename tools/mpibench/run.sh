#!/bin/bash

BINDIR="$(dirname $(readlink -f $0))"
mpirun -N 1 -hostfile nodes.txt "$BINDIR"/mpibench $@
