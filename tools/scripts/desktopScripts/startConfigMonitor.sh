#!/bin/bash

# Fix for SSH-bug in Ubuntu
export SSH_AUTH_SOCK=0

ssh -X hlt-operator@portal-ecs1 'killall runConfigMonitor.sh &> /dev/null; sleep 1; /home/hlt-operator/bin/runConfigMonitor.sh'

