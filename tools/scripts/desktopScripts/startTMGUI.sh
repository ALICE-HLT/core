#!/bin/bash

# Fix for SSH-bug in Ubuntu
export SSH_AUTH_SOCK=0

ssh -X hlt-operator@hlt-prod "ssh -X portal-ecs1 \"TMGUI.sh\"" &


# ALIHLT_PORT_BASE_RUNMGR=20000
# ALIHLT_PORTRANGE_DETECTOR=2000
# 
# # --------------------
# let "TOPPORT = $ALIHLT_PORT_BASE_RUNMGR + $ALIHLT_PORTRANGE_DETECTOR"
# 
# for ((offset=1 ; offset<99 ; offset=offset+1)) ; do
#     let "PORT = $TOPPORT -$offset"
#     echo $PORT
#     RESVAL=`netstat -aptunv 2>&1 | grep $PORT`
#     if [ ! "${RESVAL}" ] ; then
#         TMPORT=$PORT
#         break
#     fi
# done
# 
# MASTERNODE=portal-ecs1
# RUNARGS=
# 
# ./invokeAsHLTOperator.sh <<EOF
# TMGUI.py -address tcpmsg://`hostname -s`:${TMPORT} -control tcpmsg://${MASTERNODE}:${ALIHLT_PORT_BASE_RUNMGR} -unconnected ${RUNARGS} -runmanagercontrol
# EOF


