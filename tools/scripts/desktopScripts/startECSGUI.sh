#!/bin/bash

# Fix for SSH-bug in Ubuntu
export SSH_AUTH_SOCK=0

if test -z $1 ; then
	PART=PHYSICS_1
else
	PART=$1
fi

# Changed to run the GUI on portal-dcs1 which has proper communication with the ECS machine on the DAQ side.
#ssh -X hlt-operator@hlt-prod "ssh -X portal-ecs1 \"export DIM_DNS_NODE=192.168.2.1 ; ECSGUI.sh -partition=$PART\"" &

ssh -X hlt-operator@hlt-prod "ssh -X portal-dcs1 \"export DIM_DNS_NODE=aldaqecs01.cern.ch ; /home/hlt-operator/bin/ECSGUI.sh -partition=$PART\"" &

