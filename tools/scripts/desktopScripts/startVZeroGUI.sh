#!/bin/bash

# Fix for SSH-bug in Ubuntu
export SSH_AUTH_SOCK=0

ssh -X hlt-operator@hlt-prod "/home/hlt-operator/VZEROMonitorGui/trunk/VZEROMonitorGui -source fepfmdaccorde 49152 -maxentries 100000 -homerpollperiod 25 -homertimeout 1000" &
