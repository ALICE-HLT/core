#!/bin/bash

# Fix for SSH-bug in Ubuntu
export SSH_AUTH_SOCK=0

ssh -X hlt-operator@hlt-prod "ssh -X portal-ecs1 /opt/HLT/tools/bin/infoBrowser.sh" &
