#!/bin/bash

cd /tmp
# Fix for SSH-bug in Ubuntu
export SSH_AUTH_SOCK=0

if test -z $1 ; then
	ssh hlt-operator@hlt-prod
else
	HOSTTOCONNECTTO=$1
	ssh hlt-operator@hlt-prod "ssh $HOSTTOCONNECTTO"
fi

