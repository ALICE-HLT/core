#!/bin/bash

# Fix for SSH-bug in Ubuntu
export SSH_AUTH_SOCK=0

# Stopped working 19 April 2011
# Issues with installed software on hlt-prod
#ssh -X hlt-operator@hlt-prod "/home/hlt-operator/HLTAbortButtonGui/trunk/HLTAbortButton 2>&1 | tee -a /tmp/HLTAbortButtonLog.log"

# The following is for the latest release version of the abort button.
#ssh -X hlt-operator@portal-ecs1 'killall HLTAbortButton &> /dev/null; sleep 1; export PATH=/home/hlt-operator/bin/:$PATH ; /home/hlt-operator/HLTAbortButtonGui/branches/release-1.1/HLTAbortButton 2>&1 | tee -a /tmp/HLTAbortButtonLog.log'

# Using trunk abort button for now.
ssh -X hlt-operator@portal-ecs1 'killall HLTAbortButton &> /dev/null; sleep 1; export PATH=/home/hlt-operator/bin/:$PATH ; /home/hlt-operator/HLTAbortButtonGui/trunk/HLTAbortButton 2>&1 | tee -a /tmp/HLTAbortButtonLog.log'

