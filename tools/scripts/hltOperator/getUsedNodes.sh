#!/bin/bash

die() {
	echo $@ >&2
	exit 1
} 

NODELIST="/opt/HLT/control/hlt-configuration/partitions/ALICE/devel/partitionnodelist.xml"

HOSTS="$(echo -e "ls /SimpleChainConfig2NodeList/Node/Hostname" | xmllint --shell "$NODELIST"   | awk '{print $3}' | grep -v ls | sort | grep -v ecs )"

[[ -z "$HOSTS" ]] && die "Could not find any hosts in $NODELIST"

HOSTOPT=""

for host in $HOSTS
do
	HOSTOPT="$HOSTOPT -m $host"
done

if [[ "$1" == "dsh" ]]; then
	echo $HOSTOPT
else
	for host in $HOSTS; do
		echo $host
	done
fi

