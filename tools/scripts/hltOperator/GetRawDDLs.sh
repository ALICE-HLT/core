#!/bin/bash

die(){
    echo "$@" >&2
    exit 1
}

getDDLs(){
    grep "<Node>$(hostname)</Node>" /home/hlt-operator/control/run/setup/ddllist.xml | sed -e 's/.*ID=\"//' | sed -e 's/\".*//'
}

getFiles(){
    for ddl in $@
    do
	echo "$(find $BASEDIR/nope -name *_${ddl}.ddl -exec basename '{}' \; )"
    done
}
BASEDIR="$1"

if [ -z "$BASEDIR" ]
then
    die "Please provide base directory."
fi

if [ ! -d "$BASEDIR" ]
then
    die "$BASEDIR is not a directory."
fi

BASENAME="$(basename $BASEDIR)"
TARGET="/opt/HLT-data/local/${BASENAME}"
rm -rf "${TARGET}/*"
mkdir -p "${TARGET}" || die "cannot create ${TARGET} "

cd $BASEDIR
DDLs="$(getDDLs)"

FILES="$(getFiles $DDLs)"

for raw in raw*
do
    mkdir -p "${TARGET}/${raw}"
    for file in $FILES
    do
	if [[ -f ${raw}/${file} ]]
	then
	    cp ${raw}/${file} "${TARGET}/${raw}/"
	else
	    #echo "nope event copied for $raw"
	    cp nope/${file} "${TARGET}/${raw}/"
	fi
    done
done
