#!/bin/bash

now=$(date +%s)
let target=now+10
dsh -F200 -M -g fep "~/bin/sleep.sh $target; /opt/HLT/tools/bin/hrorc_handler.py --mode Start --ddllist /opt/HLT/control/hlt-configuration/setup/globalddllist.xml --startmode loop --waittime 2660000"
