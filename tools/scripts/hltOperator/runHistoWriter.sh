#!/bin/bash

# This script runs the histoWriter server on hlt-prod under a screen session.

SCREEN_NAME=histowriter

if test "$HOSTNAME" != "hlt-prod" ; then
	echo "Switching to hlt-prod to run script..."
	ssh hlt-prod $0
	exit
fi

# Check if there is already a screen session, if not then start a new session.
if test `screen -list | grep $SCREEN_NAME | wc -l` -eq 0 ; then
	echo "Starting a new screen session for '$SCREEN_NAME'."
	screen -d -m -S $SCREEN_NAME
fi

sleep 1

# Send a few control C characters to stop any process that is running on the screen session.
for ((I=0;I<5;I++)) ; do
	screen -S $SCREEN_NAME -p 0 -X stuff `echo -ne '\003'`
done

sleep 1

# Now send the commands to start the histoWriter.
screen -S $SCREEN_NAME -p 0 -X stuff "cd /home/hlt-operator/histoWriter"`echo -ne '\015'`
screen -S $SCREEN_NAME -p 0 -X stuff "./bin/runHistoWriter fepfmdaccorde:49001 fepfmdaccorde:49152 cn045:49153 cn033:49000 cn033:49001 cn033:49002 cn033:49003 cn033:49004 cn033:49005 cn033:49006 cn033:49007 cn033:49008 cn033:49009"`echo -ne '\015'`

