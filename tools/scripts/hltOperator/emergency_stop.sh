#!/bin/bash
# 
# Kills all processes belonging to the current user on all FEP and CN machines.
# Then go in and cleanup shared memory regions on all noded and bigphys regions
# on the FEPs.
#
# Author: Artur Szostak <artursz@iafrica.command>
#

#dsh -g fep -g cn kill -9 -1
#dsh -g fep psi_cleanup -all
#dsh -g fep -g cn CloseSysVregions.sh

dsh -f ~/bin/allmachinefile.txt kill -9 -1  2>&1
dsh -f ~/bin/fepmachinefile.txt psi_cleanup -all   2>&1
dsh -f ~/bin/allmachinefile.txt CloseSysVregions.sh   2>&1

