import os

files=os.listdir('.')

for f in files:
	if f.endswith('.bin'):
		parts=f.split('-')
		ddl=int(parts[0])
		evtId=int('0x' + parts[3][9:], 16)
		newf="raw%d/TPC_%d.ddl" % (evtId, ddl)
		try:
			os.mkdir("raw%d" % (evtId))
		except OSError:
			pass
		os.rename(f, newf)
	else:
		if f.endswith('.ets') or f.endswith('sedd'):
			os.remove(f)



