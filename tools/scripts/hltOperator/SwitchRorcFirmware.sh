#!/bin/bash

# Script to switch the RORC firmware on the FEP TPC machines.
# The script tries to serialise the steps and put a 1 second delay between commands.
# This is done to try and increase the chance of the operation succeeding. The switch
# procedure is attempted a few times before giving up.
#
# Author: Artur Szostak <artursz@iafrica.com>

# Select the design to switch to in the DESIGN variable, valid choices are [1..3].
DESIGN=1
DESIGNVERSION=0xfcf01356


# The amount of time to sleep between calling commands to modify firmware in seconds:
SLEEPTIME=1

# The maximum number of attempts to try switch firmware design:
MAXATTEMPTS=5


MYHOSTNAME=`hostname | sed 's/.*\(feptpc\).*/\1/g'`
if test "$MYHOSTNAME" != "feptpc" ; then
	echo "ERROR: This script should only be run on a FEP TPC machine." 1>&2
	exit 1
fi

SLOTSET1='02:05.0 Co-processor: Xilinx Corporation Device dead (rev 01)
03:04.0 Co-processor: Xilinx Corporation Device dead (rev 01)'
SLOTSET2='02:07.0 Co-processor: Xilinx Corporation Device dead (rev 01)
03:06.0 Co-processor: Xilinx Corporation Device dead (rev 01)'

PCIOUTPUT=`lspci | grep Xilinx`

if test "$PCIOUTPUT" = "$SLOTSET1" ; then
	SLOTNAME1="02:05.0"
	SLOTID1="2 5 0"
	SLOTNAME2="03:04.0"
	SLOTID2="3 4 0"
elif test "$PCIOUTPUT" = "$SLOTSET2" ; then
	SLOTNAME1="02:07.0"
	SLOTID1="2 7 0"
	SLOTNAME2="03:06.0"
	SLOTID2="3 6 0"
else
	echo "ERROR: This machine has its RORC cards in PCI slots that are not recognised or the cards are missing." 1>&2
	exit 2
fi

EXPECTEDCOUNTERSTATES="DMA eventcounter: 0x00000000 (00000000)
DIU eventcounter: 0x00000000 (00000000)"

for CARDNO in 0 1 ; do

	if test $CARDNO -eq 0 ; then
		SLOTNAME=$SLOTNAME1
		SLOTID=$SLOTID1
	else
		SLOTNAME=$SLOTNAME2
		SLOTID=$SLOTID2
	fi

	ATTEMPT=0
	SUCCESS=0
	while test $SUCCESS -eq 0 -a $ATTEMPT -lt $MAXATTEMPTS ; do
		let ATTEMPT++
		echo "Switching to firmware design $DESIGN on $HOSTNAME for RORC in $SLOTNAME, attempt $ATTEMPT ..."

		rflash -i$CARDNO -u$DESIGN -a -q
		sleep $SLEEPTIME
		rflash -i$CARDNO -r -q
		sleep $SLEEPTIME
		pci_device_restore_state -slot $SLOTID -refresh
		sleep $SLEEPTIME
		rflash -i$CARDNO -w
		sleep $SLEEPTIME

		RORCVERSION=`rorc_status -a -i$CARDNO | grep version`
		RORCCOUNTERSTATES=`rorc_status -a -i$CARDNO | grep eventcounter`
		if test "$RORCVERSION" = "H-RORC version ID: 0xffffffff" -o "$RORCCOUNTERSTATES" != "$EXPECTEDCOUNTERSTATES" ; then
			SUCCESS=0
		else
			SUCCESS=1
		fi
		if test -n $DESIGNVERSION ; then
			if test "$RORCVERSION" != "H-RORC version ID: $DESIGNVERSION" ; then
				SUCCESS=0
			fi
		fi
	done

	if test $SUCCESS -eq 1 ; then
		echo "Success."
	else
		echo "Failure."
		echo "WARNING: Could not successfully switch firmware for $SLOTNAME on $HOSTNAME. Continuing with other cards." 1>&2
	fi
done
