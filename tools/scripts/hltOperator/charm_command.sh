#!/bin/bash

CHARMCOMMAND="true"
node="none"

if [[ -n "$1" ]]; then
        node="$1"
else
        exit 1
fi

shift

if [[ -n "$1" ]]; then
	CHARMCOMMAND="$@"
else
	exit 1
fi



expect << EOF
stty -echo
spawn ssh -1 root@$node $CHARMCOMMAND
expect "password: "
send "excalibur\r"
stty echo
expect eof
EOF
