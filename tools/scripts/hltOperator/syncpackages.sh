#!/bin/bash

NODELIST="/opt/HLT/control/hlt-configuration/partitions/ALICE/devel/partitionnodelist.xml"
BASEPATH="/opt/HLT"
#BASEPATH="/tmp"
#TREE_SYNC="${HLTCONTROL_TOPDIR}/bin/system/tree_sync.py"
TREE_SYNC="/opt/HLT/control/releases/HEAD_2011-04-01/bin/system/tree_sync.py"

usage() {
    [[ -n "$@" ]] && echo -e "$@\n" >&2
    echo "$0 <dir>" >&2
    echo "$0 ALL" >&2
    echo >&2
    echo "Sync ${BASEPATH}/<dir> or everything in ${BASEPATH} to all nodes in the nodelist \"$NODELIST\"" >&2
    exit 1
}


sync_packages() {
    HOSTS="$(echo -e "ls /SimpleChainConfig2NodeList/Node/Hostname" | xmllint --shell "$NODELIST"   | awk '{print $3}' | grep -v ls | sort | grep -v ecs )"
    #HOSTS="hlt-prod $HOSTS"  # Add hlt-prod to the list of hosts to sync with.
    [[ -z "$HOSTS" ]] && usage "Could not find any hosts in $NODELIST"
    HOSTOPT=""
    for host in $HOSTS
    do
	HOSTOPT="$HOSTOPT -n $host"
    done

    echo "time ${TREE_SYNC} --rsync-options='-aH --delete' ${HOSTOPT} --path ${BASEPATH}/${1}"
    time ${TREE_SYNC} --rsync-options='-aH --delete' ${HOSTOPT} --path ${BASEPATH}/${1}
    exit 0
}

[[ -f "$NODELIST" ]] || usage "Cannot find nodelist file $NODELIST"
[[ -n "$1" ]] || usage
[[ -x ${TREE_SYNC} ]] || usage "cannot execute $TREE_SYNC"


[[ "$1" == "ALL" ]] && sync_packages 
[[ -d "${BASEPATH}/${1}" ]] && sync_packages "${1}"
