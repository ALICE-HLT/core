#!/bin/sh

# Script for running HLT DIM server on the HLT DCS portal machine.
# In the future this should all be handled in the proper modules
# and resource management system.
#
# Author: Artur Szostak <artursz@iafrica.com>

export DIM_DNS_NODE=alidcsdimdns.cern.ch
export DIM_DNS_PORT=2505
export DIM_HOST_NODE=alihlt-dcs0.cern.ch
hltdimserver -s cn045 49153

