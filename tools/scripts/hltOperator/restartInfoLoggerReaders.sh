#!/bin/sh

# Script for restarting the infoLoggerReader daemons on the FEP and CN nodes.
# We have to perform multiple retry attempts because sometimes the /etc/init.d/infologger-reader
# script fails or declares that it succeeds but has failed anyway.
#
# Author: Artur Szostak <artursz@iafrica.com>

MAXRETRYCOUNT=10

FAILED=1
RETRYCOUNT=$MAXRETRYCOUNT
echo "Restarting infoLoggerReader daemons..."
while test "x$FAILED" = "x1" ; do
	let RETRYCOUNT--;
	if test $RETRYCOUNT -le 0 ; then
		echo "ERROR: Could not restart all infoLoggerReader daemons on FEP and CN nodes after $MAXRETRYCOUNT attempts." 1>&2
		exit 1
	fi
#	OUTPUT=`dsh -g fep -g cn --forklimit 1024 /etc/init.d/infologger-reader restart | grep fail`
	OUTPUT=`dsh -f /afsuser/hlt-operator/bin/allmachinefile.txt --forklimit 1024 /etc/init.d/infologger-reader restart | grep fail`
	if test -n "$OUTPUT" ; then
		echo "Restart failed with the following output:"
		echo "$OUTPUT"
		echo "Trying again..."
		FAILED=1
		continue
	fi
#	OUTPUT=`dsh -g fep -g cn --forklimit 1024 'if test -z "\`ps ax | grep infoLoggerRead | grep -v grep\`"; then echo not_running ; fi'`
	OUTPUT=`dsh -f /afsuser/hlt-operator/bin/allmachinefile.txt --forklimit 1024 'if test -z "\`ps ax | grep infoLoggerRead | grep -v grep\`"; then echo not_running ; fi'`
	if test -z "$OUTPUT" ; then
		echo "All infoLoggerReader daemons restarted on FEP and CN nodes."
		FAILED=0
	else
		echo "Restart failed to bring up the daemons on the following nodes:"
		echo "$OUTPUT"
		echo "Trying again..."
		FAILED=1
	fi
done

