#!/bin/bash

hosts="$(cat  /home/hlt-operator/control/run/config/config-created/ALICE-SCC1-Generate.xml  | grep 'Node ID="' | sed -e 's:.* hostname="::' | sed -e 's:".*::' | grep -v ecs0 | grep fep | grep -v hltout)"

gparam=""


for h in $hosts; do
  gparam="$gparam -m $h"
done


if [[ -n "$gparam" ]]; then
	dsh -o -x -F 1000 $gparam  'rorc_status -i0 -n1 -a | grep -e deadtime -e fifo; rorc_status -i1 -n1 -a | grep -e deadtime -e fifo; rorc_status -i0 -n0 -a | grep -e deadtime -e fifo; rorc_status -i1 -n0 -a | grep -e deadtime -e fifo;' | grep -v 0x00000000 | grep -v "full: 0" | sort
fi

