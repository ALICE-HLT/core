#!/bin/sh

# Simple script for displaying the current overlay configuration setup for the ACR console.
# This is called by hlt-prod:/home/acr/startConfigMonitor.sh
#
# Author: Artur Szostak <artursz@iafrica.com>

CFGFILE=/opt/HLT/control/hlt-configuration/partitions/ALICE/allowed_types

NO_TPC_CONFIG="devel/NoTPC
devel/compression
devel"

TPC_ONLY_CONFIG="devel/TPCOnly
devel/compression
devel"

SKIP_TRACKING_CONFIG="devel/SkipTracking
devel/compression
devel"

REDUCED_TRACKING_CONFIG="devel/ReducedTracking
devel/compression
devel"

STANDARD_CONFIG="devel/compression
devel"


while true ; do
	echo;echo;echo;echo;echo;echo;echo;echo;

	CONFIG=`cat $CFGFILE`

	if test "$CONFIG" = "$NO_TPC_CONFIG" ; then
		echo "      No TPC"
	elif test "$CONFIG" = "$TPC_ONLY_CONFIG" ; then
		echo "      TPC Only"
	elif test "$CONFIG" = "$SKIP_TRACKING_CONFIG" ; then
		echo "      No Barrel Tracking"
	elif test "$CONFIG" = "$REDUCED_TRACKING_CONFIG" ; then
		echo "      Reduced Tracking"
	elif test "$CONFIG" = "$STANDARD_CONFIG" ; then
		echo "      Standard"
	else
		echo "      UNKNOWN"
	fi

	sleep 1
done

