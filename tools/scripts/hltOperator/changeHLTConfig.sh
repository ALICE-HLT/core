#!/bin/bash

# This script is used as a helper script for updating the HLT overlay configuration
# manually from the ACR using desktop buttons.
#
# Author: Artur Szostak

# Fix for SSH-bug in Ubuntu
export SSH_AUTH_SOCK=0

CFGFILE=/opt/HLT/control/hlt-configuration/partitions/ALICE/allowed_types

CONFIG_CHOSEN=$1

if test "$CONFIG_CHOSEN" = "NoTPC" ; then

	cat > $CFGFILE <<EOF
devel/NoTPC
devel/compression
devel
EOF

elif test "$CONFIG_CHOSEN" = "TPCOnly" ; then

	cat > $CFGFILE <<EOF
devel/TPCOnly
devel/compression
devel
EOF

elif test "$CONFIG_CHOSEN" = "SkipTracking" ; then

	cat > $CFGFILE <<EOF
devel/SkipTracking
devel/compression
devel
EOF

elif test "$CONFIG_CHOSEN" = "ReducedTracking" ; then

	cat > $CFGFILE <<EOF
devel/ReducedTracking
devel/compression
devel
EOF

elif test "$CONFIG_CHOSEN" = "Standard" ; then

	cat > $CFGFILE <<EOF
devel/compression
devel
EOF

else
	echo "ERROR: No configuration selected to change to." 1>&2
	exit 1
fi

