#!/bin/bash

## -- Load Modules --

# The following enables slower glibc memory management system, but which is more resiliant and with sanity checks.
# see "man malloc" for more details.
#export MALLOC_CHECK_=3

# The following is necessary to have core dumps enabled.
ulimit -c unlimited
# not neccessary any more
#if [ "${HOSTNAME%[0-9][0-9][0-9]}" = "cngpu" ] ; then
#	ulimit -c 1024
#fi

## Uncomment next line to supress the info of loaded HLT modules
export MODULE_INFO_SUPRESS=1


if [ -f /etc/profile.modules ] ; then
    . /etc/profile.modules
     # load actual HLT modules
     #module unload HLT
     #module unload HLTdevel

#     module load FILESYSTEM/GLUSTERFS

     #module load HLT/v5-01-Rev-01-prod
     #module load HLT/v5-01-Rev-07.dEdxpatch-prod
     #module load HLT/v5-01-Rev-07-r2-prod
     #module load HLT/v5-01-Rev-07-r3-prod
     #module load HLT/v5-01-Rev-07-r4-prod
     module load HLT/v5-01-Rev-07-r6-prod

     ## - load global HLT module
     ## Comment next line and uncomment the following line, if you'd like to
     ## use not the globally updated HLT module. BEAWARE: Then YOU have to 
     ## take care of updateing the according module files.

     #. /opt/HLT/control/releases/setup/.loadModules.sh


     #module load HLT/v4-20-Rev-31-prod
     #module load HLT/v4-20-Rev-23-prod

     #export ALIHLT_AUTOLOAD_INTERFACE_DEFAULT_LIBRARIES=1


     #module unload DATATRANSPORT
     #module   load DATATRANSPORT/20110810.114339


     #FIXME: This is a temporary solution to override incorrect setting in the module file.
     #       The module sets HLTECS_MODULE=HLT_BAK which was for a failover, but that is not working yet.
     #export HLTECS_MODULE=HLT

     #export ALIHLT_HCDBDIR="/mnt/fhgfs/data/T-HCDB"
     #export ALIHLT_HCDBDIR="/opt/HLT/HCDB_154808"
     #export ALIHLT_HCDBDIR="/mnt/fhgfs/HCDB_139437"
     #export ALIHLT_HCDBDIR="/mnt/fhgfs/HCDB_154808"
     #export ALIHLT_HCDBDIR="/mnt/fhgfs/HCDB_TEST"
     #export ALIHLT_HCDBDIR="/opt/T-HCDB"

     ## - load private HLT-ANALYSIS module
     ## If you like to use a privat HLT analysis installation, uncomment 
     ## the next line.

     #module use --append $HOME/modules-HLT

     ## then enter modules here:

     #  module unload  HLTANALYSIS
     #  module load HLTdevel/HEAD_2010-07-29-prod
     #module load HLTdevel/HEAD_v4-20-Rev-01-prod



fi

# For the DCS DIM server
# FIXME: Unfortunately the following does not work in this file for some reason,
# so it has been moved to ~/.bashrc
#if [ `hostname` = "portal-dcs0" -o `hostname` = "portal-dcs1" ] ; then
#  export DIM_DNS_NODE=alidcsdimdns.cern.ch
#  export DIM_DNS_PORT=2505
#  export DIM_HOST_NODE=alihlt-dcs0.cern.ch 
#fi

# For GPU tracker
if [ "${HOSTNAME%[0-9][0-9][0-9]}" = "cngpu" ] ; then
	#module load CAGPU/Rev243-v4-20-Rev-31
	#module load CAGPU/Rev296-v5-01-Rev-01
	#module load CAGPU/Rev296-v5-01-Rev-07
	#module load CAGPU/Rev298-v5-01-Rev-07
	module load CAGPU/Rev311-v5-01-Rev-07-r6
fi
