#!/bin/bash


check_node() {
	ping -q -c 1 -W 1 $1 > /dev/null 
	return $?
}


ALLNODES=$(echo -e "ls /SimpleChainConfig2NodeList/Node/Hostname" | xmllint --shell "/opt/HLT/control/hlt-configuration/partitions/ALICE/devel/partitionnodelist.xml"   | awk '{print $3}' | grep -v ls | sort -u | grep -e fep -e cn | sed -e 's/-ib$//')

FEPBMC=""
CHARM=""
CNBMC=""


for node in $ALLNODES; do
	if host ${node}-mgmt | grep -q bmc ; then
		if echo $node | grep -q ^fep; then
			FEPBMC="$FEPBMC ${node}-bmc"
		else
			CNBMC="$CNBMC ${node}-bmc"
		fi
	else
		CHARM="$CHARM ${node}-charm"
	fi
done


if [[ "$1" == "check" ]]; then
	for node in $FEPBMC $CNBMC $CHARM; do
		check_node $node && echo $node ok || echo $node not reachable
	done
	exit 0
fi

if [[ "$1" == "checkhost" ]]; then
        for node in $ALLNODES; do
                check_node $node > /dev/null && echo $node ok || echo $node not reachable
        done
        exit 0
fi



BMCCOMMAND="on"
CHARMCOMMAND="hostPowerOn"

if [[ "$1" == "status" ]]; then
	BMCCOMMAND="status"
	CHARMCOMMAND="hostStatus"
fi

for node in $FEPBMC; do
	echo $node:
	check_node $node && ipmitool -H $node -U Admin -P Admin power $BMCCOMMAND || echo $node not reachable
done

for node in $CNBMC; do
	echo $node:
        check_node $node && ipmitool -H $node -U ADMIN -P ADMIN power $BMCCOMMAND || echo $node not reachable
done

for node in $CHARM; do
echo $node:
if check_node $node ; then
expect << EOF
spawn ssh -1 root@$node $CHARMCOMMAND
expect "password: "
send "excalibur\r"
expect eof
EOF
else
echo $node not reachable
fi
done


