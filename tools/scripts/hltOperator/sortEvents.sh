#!/bin/bash

#BASEDIR="10000127715002.10"
BASEDIR="."
OUTFILE="list"
OUTDIR="run"

for dir in $(find $BASDIR -name 'raw*' | sort)
do
  pushd "$dir" &> /dev/null
  found=0
  for file in "ITSSPD_*.ddl" "TPC_*.ddl" "MUONTRG_*.ddl" "MUONTRK_*.ddl" "EMCAL_*.ddl" "TRD_*.ddl" "PHOS_*.ddl" "ITSSSD_*.ddl" "VZERO_3584.ddl" "ZDC_3840.ddl" "T0_3328.ddl"
  do
    f="$(find . -name "${file}"|head -n1)"
    if [[ -f ${f} ]]
    then 
      echo "$(eventid ${f}) ${dir}"
      found=1
      break
    fi
  done
#  if [[ $found -eq 0 ]]
#  then echo "-1 ${dir}"
#  fi
  popd &> /dev/null
done | sort -n > "$OUTFILE"

exit 0

cat "$OUTFILE" | \
while read eventid dir
do
  mkdir -p "${OUTDIR}/raw${eventid}"
  cp -f ${dir}/* "${OUTDIR}/raw${eventid}/"
done
