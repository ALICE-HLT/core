#!/bin/bash

# Fetches the and counts the number of XOFF messages found in the DAQ infoLogger messages.
# The script takes one parameter which is the run number to count from.

if test -z $1 ; then
	RUN=152370
else
	RUN=$1
fi

echo "run	partition	No. of XOFF messages"
mysql -h 10.161.36.8 -u dateLogBrw -pqQ5NdNCp DATE_LOG -B -N -e "select run, partition, count(message) from messages where run>$RUN and message like \"%XOFF%\" group by run order by run"

