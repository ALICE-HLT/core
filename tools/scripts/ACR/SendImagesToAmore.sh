#!/bin/bash

FIGURE_ID=0
RUN_NUMBER=1

while true; do
if [ -e /tmp/HLT_EventDisplay/current/*.png ];
then
    if test $FIGURE_ID -eq 99;
	then FIGURE_ID=0
    fi
    FIGURE_ID=$(expr $FIGURE_ID + 1)
    FILEPATH="/tmp/HLT_EventDisplay/current/*.png"
    FILENAME=$(expr $FILEPATH | awk '{print substr($0,31)}')
    RUN_NUMBER=$(expr $FILENAME | awk '{print substr($0,5,6)}')
    if [ $HOSTNAME = "hlt-prod" ]; then
	scp $FILEPATH hlt-operator@portal-dcs1:/tmp/HLT_EventDisplay/current/.
	rm $FILEPATH
    fi	
    if [ $HOSTNAME = "portal-dcs1" ]; then
        scp -P2222 $FILEPATH hlt@localhost:/tmp/HLT_EventDisplay/current/.
	rm $FILEPATH
    fi
    if [ $HOSTNAME = "aldaqacr03" ]; then
	echo "Sending Image with ID " $FIGURE_ID "and Image Name " $FILEPATH "for run number " $RUN_NUMBER "to amore"
        /opt/amore/bin/SendImageToAmore $FIGURE_ID $FILEPATH $RUN_NUMBER
        rm $FILEPATH
    fi

          
fi
done
