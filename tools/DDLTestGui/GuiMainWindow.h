#ifndef GUIMAINWINDOW_H
#define GUIMAINWINDOW_H
/**************************************************************************
 * This file is property of and copyright by the ALICE HLT Project        *
 * All rights reserved.                                                   *
 *                                                                        *
 * Primary Authors:                                                       *
 *   Artur Szostak <artursz@iafrica.com>                                  *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

#include "TGFrame.h"
#include <vector>

class TGLabel;
class TGTab;
class TGListView;
class TGLVContainer;
class TGPicture;
class TTimer;
class AliHLTHOMERReader;


class GuiMainWindow : public TGMainFrame
{
public:
	
	GuiMainWindow(const char* hostname = "portal-ecs0", unsigned short port = 49152);
	virtual ~GuiMainWindow();

	// Override.
	virtual void CloseWindow();
	
	// Slots for signals.
	void ChangeTab(Int_t i);
	void UpdateDDLView();

private:

	struct DDLSummaryInfo
	{
		Int_t fExpectedDDL;
		Int_t fReceivedDDL;
		// The following are NULL terminated strings.
		char fExpectedPlug[1024];
		char fReceivedPlug[1024];
	};
	
	void FetchData();

	void AddDDLsForSPD();
	void AddDDLsForSDD();
	void AddDDLsForSSD();
	void AddDDLsForTPC();
	void AddDDLsForTPCA();
	void AddDDLsForTPCC();
	void AddDDLsForTRD();
	void AddDDLsForTOF();
	void AddDDLsForHMPID();
	void AddDDLsForPHOS();
	void AddDDLsForCPV();
	void AddDDLsForPMD();
	void AddDDLsForDIMUON();
	void AddDDLsForFMD();
	void AddDDLsForT0();
	void AddDDLsForV0();
	void AddDDLsForZDC();
	void AddDDLsForACO();
	void AddDDLsForTRG();
	void AddDDLsForEMCAL();

	void IgnoreDDLsForSPD();
	void IgnoreDDLsForSDD();
	void IgnoreDDLsForSSD();
	void IgnoreDDLsForTPCA();
	void IgnoreDDLsForTPCC();
	void IgnoreDDLsForTRD();
	void IgnoreDDLsForTOF();
	void IgnoreDDLsForHMPID();
	void IgnoreDDLsForPHOS();
	void IgnoreDDLsForCPV();
	void IgnoreDDLsForPMD();
	void IgnoreDDLsForDIMUON();
	void IgnoreDDLsForFMD();
	void IgnoreDDLsForT0();
	void IgnoreDDLsForV0();
	void IgnoreDDLsForZDC();
	void IgnoreDDLsForACO();
	void IgnoreDDLsForTRG();
	void IgnoreDDLsForEMCAL();
	
	void AddDDL(Int_t ddlid);
	
	void IgnoreDDL(Int_t ddlid);

	TGLabel* fLabel;
	TGTab* fTabBar;
	TGListView* fListView;
	TGLVContainer* fListContents;
	const TGPicture* fEmptyPic;
	const TGPicture* fGreenPic;
	const TGPicture* fRedPic;
	const TGPicture* fYellowPic;
	TTimer* fTimer;
	const char* fHostname;
	unsigned short fPort;
	AliHLTHOMERReader* fHomerReader;
	std::vector<const DDLSummaryInfo*> fTestedDDLs, fUnhandledDDLs;
	bool fProblemWithDDLs;
	
	ClassDef(GuiMainWindow, 0)
};

#endif // GUIMAINWINDOW_H
