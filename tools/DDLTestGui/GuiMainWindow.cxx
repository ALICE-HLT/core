/**************************************************************************
 * This file is property of and copyright by the ALICE HLT Project        *
 * All rights reserved.                                                   *
 *                                                                        *
 * Primary Authors:                                                       *
 *   Artur Szostak <artursz@iafrica.com>                                  *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

#include "GuiMainWindow.h"
#include "TApplication.h"
#include "TGFrame.h"
#include "TGListView.h"
#include "TGLabel.h"
#include "TGTab.h"
#include "TGPicture.h"
#include "TGButton.h"
#include "TString.h"
#include "TTimer.h"
#include "Riostream.h"
#include "AliHLTHOMERReader.h"
#include "AliHLTDataTypes.h"
#include <cerrno>


#define NOT_RUNNING_MSG "Test not running or tcp connection problem."
#define BAD_DDLS_MSG    "There is a problem with the DDL connections."
#define DDLS_OK_MSG     "DDLs OK."


// The XPM image icons to use in TGListView.

/* XPM */
static const char * empty_xpm[] = {
"1 1 1 1",
"       g None",
" "};

/* XPM */
static const char * green_xpm[] = {
"16 16 43 1",
" 	c None",
".	c #00EC00",
"+	c #00E600",
"@	c #00E000",
"#	c #00CB00",
"$	c #00EB00",
"%	c #00ED00",
"&	c #00FB00",
"*	c #00FF00",
"=	c #00FA00",
"-	c #00EF00",
";	c #00C700",
">	c #00A800",
",	c #00E500",
"'	c #00F900",
")	c #00D900",
"!	c #00A300",
"~	c #00D100",
"{	c #00F600",
"]	c #00FC00",
"^	c #00D200",
"/	c #00A100",
"(	c #00E200",
"_	c #00FD00",
":	c #00EE00",
"<	c #00B100",
"[	c #009400",
"}	c #00F200",
"|	c #00CF00",
"1	c #00DD00",
"2	c #00E300",
"3	c #00D800",
"4	c #00C500",
"5	c #00BC00",
"6	c #00E400",
"7	c #00A900",
"8	c #00F100",
"9	c #00BD00",
"0	c #009A00",
"a	c #00B300",
"b	c #00D300",
"c	c #00D600",
"d	c #00C400",
"     .+@#       ",
"   $%&**=-;>    ",
"  ,'******&)!   ",
" ~{********]^/  ",
" (_*********:<[ ",
" }**********_|[ ",
"['***********1[[",
"[************2[[",
"[************3[[",
"[+**********=4[[",
" 5**********67[ ",
" !3********890[ ",
" [/3:****=290[[ ",
"  [/ab12cd>0[[  ",
"   [[[[[[[[[[   ",
"     [[[[[[     "};

/* XPM */
static const char * red_xpm[] = {
"16 16 43 1",
" 	c None",
".	c #DF0000",
"+	c #F10000",
"@	c #FF0000",
"#	c #E10000",
"$	c #D00000",
"%	c #D20000",
"&	c #B10000",
"*	c #CA0000",
"=	c #A80000",
"-	c #F70000",
";	c #E70000",
">	c #AB0000",
",	c #B60000",
"'	c #9F0000",
")	c #BC0000",
"!	c #EB0000",
"~	c #BF0000",
"{	c #B80000",
"]	c #E20000",
"^	c #F50000",
"/	c #FA0000",
"(	c #B00000",
"_	c #E00000",
":	c #A60000",
"<	c #C90000",
"[	c #F00000",
"}	c #F90000",
"|	c #C60000",
"1	c #A10000",
"2	c #EC0000",
"3	c #FD0000",
"4	c #FB0000",
"5	c #D30000",
"6	c #AA0000",
"7	c #B50000",
"8	c #DB0000",
"9	c #F30000",
"0	c #ED0000",
"a	c #CE0000",
"b	c #A50000",
"c	c #C30000",
"d	c #CF0000",
"       .        ",
"    +@@@@@#$    ",
"   @@@@@@@@@%&  ",
"  @@@@@@@@@@@*= ",
" -@@@@@@@@@@@;> ",
" @@@@@@@@@@@@@,'",
" @@@@@@@@@@@@@)'",
"!@@@@@@@@@@@@@~'",
"!@@@@@@@@@@@@@{'",
"]^@@@@@@@@@@@/('",
" _@@@@@@@@@@@;:'",
" <[@@@@@@@@@}|1'",
"  |23@@@@@@45:' ",
"  67894@@40a:'' ",
"   'b&cddc&b''  ",
"     '''''''    "};

/* XPM */
static const char * yellow_xpm[] = {
"16 16 29 1",
" 	c None",
".	c #EDEB00",
"+	c #EBE900",
"@	c #EAE800",
"#	c #E7E400",
"$	c #E5E200",
"%	c #F3F200",
"&	c #F5F400",
"*	c #FFFF00",
"=	c #ECEA00",
"-	c #F2F100",
";	c #E3E000",
">	c #F0EF00",
",	c #F6F500",
"'	c #FEFE00",
")	c #F4F300",
"!	c #FCFC00",
"~	c #F9F900",
"{	c #F0EE00",
"]	c #F6F600",
"^	c #FAFA00",
"/	c #E8E600",
"(	c #EEEC00",
"_	c #F7F600",
":	c #FDFD00",
"<	c #F1F000",
"[	c #F9F800",
"}	c #FBFA00",
"|	c #E8E500",
"     .+@#$      ",
"   %&*****%=    ",
"  -*********+;  ",
" >***********+  ",
" ,***********%; ",
" *************; ",
"=*************;;",
"-'************;;",
")*************;;",
")************!;;",
">~***********{; ",
" ]**********^/; ",
" (_*******:^/;  ",
"  .<[**:!}{/;;  ",
"    |//#$;;;    ",
"      ;;;;      "};


ClassImp(GuiMainWindow)


GuiMainWindow::GuiMainWindow(const char* hostname, unsigned short port) :
	TGMainFrame(gClient->GetRoot(), 10, 10, kMainFrame | kVerticalFrame),
	fLabel(new TGLabel(this, NOT_RUNNING_MSG)),
	fTabBar(new TGTab(this)),
	fListView(new TGListView(this, 10, 10)),
	fListContents(NULL),
	fEmptyPic(gClient->GetPicturePool()->GetPicture("emptyIcon", (char**)empty_xpm)),
	fGreenPic(gClient->GetPicturePool()->GetPicture("greenIcon", (char**)green_xpm)),
	fRedPic(gClient->GetPicturePool()->GetPicture("redIcon", (char**)red_xpm)),
	fYellowPic(gClient->GetPicturePool()->GetPicture("yellowIcon", (char**)yellow_xpm)),
	fTimer(new TTimer(1000)),
	fHostname(hostname),
	fPort(port),
	fHomerReader(new AliHLTHOMERReader(hostname, port)),
	fTestedDDLs(),
	fProblemWithDDLs(false)
{
	SetCleanup(kDeepCleanup);
	
	AddFrame(fLabel, new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX, 0, 0, 1, 1));
	AddFrame(fTabBar, new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX, 0, 0, 1, 1));
	AddFrame(fListView, new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX | kLHintsExpandY, 0, 0, 1, 1));

	Pixel_t yellow;
	gClient->GetColorByName("yellow", yellow);
	Pixel_t white;
	gClient->GetColorByName("white", white);

	fLabel->SetBackgroundColor(yellow);
	fLabel->SetTextJustify(kTextCenterX | kTextCenterY);
	
	fTabBar->AddTab("ALL");
	fTabBar->AddTab("ITS");
	fTabBar->AddTab("ITS-SPD");
	fTabBar->AddTab("ITS-SDD");
	fTabBar->AddTab("ITS-SSD");
	fTabBar->AddTab("TPC");
	fTabBar->AddTab("TPC-A side");
	fTabBar->AddTab("TPC-C side");
	fTabBar->AddTab("TRD");
	fTabBar->AddTab("TOF");
	fTabBar->AddTab("HMPID");
	fTabBar->AddTab("PHOS");
	fTabBar->AddTab("CPV");
	fTabBar->AddTab("PMD");
	fTabBar->AddTab("DIMUON");
	fTabBar->AddTab("FMD");
	fTabBar->AddTab("T0");
	fTabBar->AddTab("V0");
	fTabBar->AddTab("ZDC");
	fTabBar->AddTab("ACORDE");
	fTabBar->AddTab("TRG");
	fTabBar->AddTab("EMCAL");

	fTabBar->Connect("Selected(Int_t)", "GuiMainWindow", this, "ChangeTab(Int_t)");
	
	fListContents = new TGLVContainer(fListView, kSunkenFrame, white);
	fListContents->SetHeaders(5);
	fListContents->SetHeader("Expected DDL ID", kTextLeft, kTextLeft, 0);
	fListContents->SetHeader("Received DDL ID", kTextLeft, kTextLeft, 1);
	fListContents->SetHeader("Expected Plug ID", kTextLeft, kTextLeft, 2);
	fListContents->SetHeader("Received Plug ID", kTextLeft, kTextLeft, 3);
	fListContents->SetHeader("Comment", kTextLeft, kTextLeft, 4);
	
	AddDDLsForSPD();
	AddDDLsForSDD();
	AddDDLsForSSD();
	AddDDLsForTPCA();
	AddDDLsForTPCC();
	AddDDLsForTRD();
	AddDDLsForTOF();
	AddDDLsForHMPID();
	AddDDLsForPHOS();
	AddDDLsForCPV();
	AddDDLsForPMD();
	AddDDLsForDIMUON();
        AddDDLsForFMD();
	AddDDLsForT0();
	AddDDLsForV0();
	AddDDLsForZDC();
	AddDDLsForACO();
	AddDDLsForTRG();
	AddDDLsForEMCAL();
	
	fListView->SetViewMode(kLVDetails);
	fListView->ResizeColumns();
	
	MapSubwindows();
	Resize(GetDefaultSize());
	MapWindow();
	Resize(1166,600);
	
	fTimer->Connect("Timeout()", "GuiMainWindow", this, "UpdateDDLView()");
	fTimer->TurnOn();
}


GuiMainWindow::~GuiMainWindow()
{
	delete fTimer;
	delete fHomerReader;
	UnmapWindow();
	gClient->GetPicturePool()->FreePicture(fEmptyPic);
	gClient->GetPicturePool()->FreePicture(fGreenPic);
	gClient->GetPicturePool()->FreePicture(fRedPic);
	gClient->GetPicturePool()->FreePicture(fYellowPic);
	Cleanup();
}


void GuiMainWindow::CloseWindow()
{
	gApplication->Terminate();
}


void GuiMainWindow::ChangeTab(Int_t tabIndex)
{
	fListContents->RemoveAll();
	fProblemWithDDLs = false;
	
	fUnhandledDDLs = fTestedDDLs;
	
	switch (tabIndex)
	{
	case 0: // All tab
		AddDDLsForSPD();
		AddDDLsForSDD();
		AddDDLsForSSD();
		AddDDLsForTPC();
		AddDDLsForTRD();
		AddDDLsForTOF();
		AddDDLsForHMPID();
		AddDDLsForPHOS();
		AddDDLsForCPV();
		AddDDLsForPMD();
		AddDDLsForDIMUON();
		AddDDLsForFMD();
		AddDDLsForT0();
		AddDDLsForV0();
		AddDDLsForZDC();
		AddDDLsForACO();
		AddDDLsForTRG();
		AddDDLsForEMCAL();
		break;
	case 1: // ITS tab
		AddDDLsForSPD();
		AddDDLsForSDD();
		AddDDLsForSSD();
		IgnoreDDLsForTPCA();
		IgnoreDDLsForTPCC();
		IgnoreDDLsForTRD();
		IgnoreDDLsForTOF();
		IgnoreDDLsForHMPID();
		IgnoreDDLsForPHOS();
		IgnoreDDLsForCPV();
		IgnoreDDLsForPMD();
		IgnoreDDLsForDIMUON();
		IgnoreDDLsForFMD();
		IgnoreDDLsForT0();
		IgnoreDDLsForV0();
		IgnoreDDLsForZDC();
		IgnoreDDLsForACO();
		IgnoreDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 2: // SPD tab
		AddDDLsForSPD();
		IgnoreDDLsForSDD();
		IgnoreDDLsForSSD();
		IgnoreDDLsForTPCA();
		IgnoreDDLsForTPCC();
		IgnoreDDLsForTRD();
		IgnoreDDLsForTOF();
		IgnoreDDLsForHMPID();
		IgnoreDDLsForPHOS();
		IgnoreDDLsForCPV();
		IgnoreDDLsForPMD();
		IgnoreDDLsForDIMUON();
		IgnoreDDLsForFMD();
		IgnoreDDLsForT0();
		IgnoreDDLsForV0();
		IgnoreDDLsForZDC();
		IgnoreDDLsForACO();
		IgnoreDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 3: // SDD tab
		IgnoreDDLsForSPD();
		AddDDLsForSDD();
		IgnoreDDLsForSSD();
		IgnoreDDLsForTPCA();
		IgnoreDDLsForTPCC();
		IgnoreDDLsForTRD();
		IgnoreDDLsForTOF();
		IgnoreDDLsForHMPID();
		IgnoreDDLsForPHOS();
		IgnoreDDLsForCPV();
		IgnoreDDLsForPMD();
		IgnoreDDLsForDIMUON();
		IgnoreDDLsForFMD();
		IgnoreDDLsForT0();
		IgnoreDDLsForV0();
		IgnoreDDLsForZDC();
		IgnoreDDLsForACO();
		IgnoreDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 4: // SSD tab
		IgnoreDDLsForSPD();
		IgnoreDDLsForSDD();
		AddDDLsForSSD();
		IgnoreDDLsForTPCA();
		IgnoreDDLsForTPCC();
		IgnoreDDLsForTRD();
		IgnoreDDLsForTOF();
		IgnoreDDLsForHMPID();
		IgnoreDDLsForPHOS();
		IgnoreDDLsForCPV();
		IgnoreDDLsForPMD();
		IgnoreDDLsForDIMUON();
		IgnoreDDLsForFMD();
		IgnoreDDLsForT0();
		IgnoreDDLsForV0();
		IgnoreDDLsForZDC();
		IgnoreDDLsForACO();
		IgnoreDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 5: // TPC tab
		IgnoreDDLsForSPD();
		IgnoreDDLsForSDD();
		IgnoreDDLsForSSD();
		AddDDLsForTPC();
		IgnoreDDLsForTRD();
		IgnoreDDLsForTOF();
		IgnoreDDLsForHMPID();
		IgnoreDDLsForPHOS();
		IgnoreDDLsForCPV();
		IgnoreDDLsForPMD();
		IgnoreDDLsForDIMUON();
		IgnoreDDLsForFMD();
		IgnoreDDLsForT0();
		IgnoreDDLsForV0();
		IgnoreDDLsForZDC();
		IgnoreDDLsForACO();
		IgnoreDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 6: // TPC-A side tab
		IgnoreDDLsForSPD();
		IgnoreDDLsForSDD();
		IgnoreDDLsForSSD();
		AddDDLsForTPCA();
		IgnoreDDLsForTPCC();
		IgnoreDDLsForTRD();
		IgnoreDDLsForTOF();
		IgnoreDDLsForHMPID();
		IgnoreDDLsForPHOS();
		IgnoreDDLsForCPV();
		IgnoreDDLsForPMD();
		IgnoreDDLsForDIMUON();
		IgnoreDDLsForFMD();
		IgnoreDDLsForT0();
		IgnoreDDLsForV0();
		IgnoreDDLsForZDC();
		IgnoreDDLsForACO();
		IgnoreDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 7: // TPC-C side tab
		IgnoreDDLsForSPD();
		IgnoreDDLsForSDD();
		IgnoreDDLsForSSD();
		IgnoreDDLsForTPCA();
		AddDDLsForTPCC();
		IgnoreDDLsForTRD();
		IgnoreDDLsForTOF();
		IgnoreDDLsForHMPID();
		IgnoreDDLsForPHOS();
		IgnoreDDLsForCPV();
		IgnoreDDLsForPMD();
		IgnoreDDLsForDIMUON();
		IgnoreDDLsForFMD();
		IgnoreDDLsForT0();
		IgnoreDDLsForV0();
		IgnoreDDLsForZDC();
		IgnoreDDLsForACO();
		IgnoreDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 8: // TRD tab
		IgnoreDDLsForSPD();
		IgnoreDDLsForSDD();
		IgnoreDDLsForSSD();
		IgnoreDDLsForTPCA();
		IgnoreDDLsForTPCC();
		AddDDLsForTRD();
		IgnoreDDLsForTOF();
		IgnoreDDLsForHMPID();
		IgnoreDDLsForPHOS();
		IgnoreDDLsForCPV();
		IgnoreDDLsForPMD();
		IgnoreDDLsForDIMUON();
		IgnoreDDLsForFMD();
		IgnoreDDLsForT0();
		IgnoreDDLsForV0();
		IgnoreDDLsForZDC();
		IgnoreDDLsForACO();
		IgnoreDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 9: // TOF tab
		IgnoreDDLsForSPD();
		IgnoreDDLsForSDD();
		IgnoreDDLsForSSD();
		IgnoreDDLsForTPCA();
		IgnoreDDLsForTPCC();
		IgnoreDDLsForTRD();
		AddDDLsForTOF();
		IgnoreDDLsForHMPID();
		IgnoreDDLsForPHOS();
 		IgnoreDDLsForCPV();
		IgnoreDDLsForPMD();
		IgnoreDDLsForDIMUON();
		IgnoreDDLsForFMD();
		IgnoreDDLsForT0();
		IgnoreDDLsForV0();
		IgnoreDDLsForZDC();
		IgnoreDDLsForACO();
		IgnoreDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 10: // HMPID tab
		IgnoreDDLsForSPD();
		IgnoreDDLsForSDD();
		IgnoreDDLsForSSD();
		IgnoreDDLsForTPCA();
		IgnoreDDLsForTPCC();
		IgnoreDDLsForTRD();
		IgnoreDDLsForTOF();
		AddDDLsForHMPID();
		IgnoreDDLsForPHOS();
		IgnoreDDLsForCPV();
		IgnoreDDLsForPMD();
		IgnoreDDLsForDIMUON();
		IgnoreDDLsForFMD();
		IgnoreDDLsForT0();
		IgnoreDDLsForV0();
		IgnoreDDLsForZDC();
		IgnoreDDLsForACO();
		IgnoreDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 11: // PHOS tab
		IgnoreDDLsForSPD();
		IgnoreDDLsForSDD();
		IgnoreDDLsForSSD();
		IgnoreDDLsForTPCA();
		IgnoreDDLsForTPCC();
		IgnoreDDLsForTRD();
		IgnoreDDLsForTOF();
		IgnoreDDLsForHMPID();
		AddDDLsForPHOS();
  		IgnoreDDLsForCPV();
		IgnoreDDLsForPMD();
		IgnoreDDLsForDIMUON();
		IgnoreDDLsForFMD();
		IgnoreDDLsForT0();
		IgnoreDDLsForV0();
		IgnoreDDLsForZDC();
		IgnoreDDLsForACO();
		IgnoreDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 12: // CPV tab
		IgnoreDDLsForSPD();
		IgnoreDDLsForSDD();
		IgnoreDDLsForSSD();
		IgnoreDDLsForTPCA();
		IgnoreDDLsForTPCC();
		IgnoreDDLsForTRD();
		IgnoreDDLsForTOF();
		IgnoreDDLsForHMPID();
		IgnoreDDLsForPHOS();
  		AddDDLsForCPV();
		IgnoreDDLsForPMD();
		IgnoreDDLsForDIMUON();
		IgnoreDDLsForFMD();
		IgnoreDDLsForT0();
		IgnoreDDLsForV0();
		IgnoreDDLsForZDC();
		IgnoreDDLsForACO();
		IgnoreDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 13: // PMD tab
		IgnoreDDLsForSPD();
		IgnoreDDLsForSDD();
		IgnoreDDLsForSSD();
		IgnoreDDLsForTPCA();
		IgnoreDDLsForTPCC();
		IgnoreDDLsForTRD();
		IgnoreDDLsForTOF();
		IgnoreDDLsForHMPID();
		IgnoreDDLsForPHOS();
  		IgnoreDDLsForCPV();
		AddDDLsForPMD();
		IgnoreDDLsForDIMUON();
		IgnoreDDLsForFMD();
		IgnoreDDLsForT0();
		IgnoreDDLsForV0();
		IgnoreDDLsForZDC();
		IgnoreDDLsForACO();
		IgnoreDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 14: // DIMUON tab
		IgnoreDDLsForSPD();
		IgnoreDDLsForSDD();
		IgnoreDDLsForSSD();
		IgnoreDDLsForTPCA();
		IgnoreDDLsForTPCC();
		IgnoreDDLsForTRD();
		IgnoreDDLsForTOF();
		IgnoreDDLsForHMPID();
		IgnoreDDLsForPHOS();
		AddDDLsForDIMUON();
		IgnoreDDLsForFMD();
		IgnoreDDLsForT0();
		IgnoreDDLsForV0();
		IgnoreDDLsForZDC();
		IgnoreDDLsForACO();
		IgnoreDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 15: // FMD tab
		IgnoreDDLsForSPD();
		IgnoreDDLsForSDD();
		IgnoreDDLsForSSD();
		IgnoreDDLsForTPCA();
		IgnoreDDLsForTPCC();
		IgnoreDDLsForTRD();
		IgnoreDDLsForTOF();
		IgnoreDDLsForHMPID();
		IgnoreDDLsForPHOS();
		IgnoreDDLsForDIMUON();
		AddDDLsForFMD();
		IgnoreDDLsForT0();
		IgnoreDDLsForV0();
		IgnoreDDLsForZDC();
		IgnoreDDLsForACO();
		IgnoreDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 16: // T0 tab
		IgnoreDDLsForSPD();
		IgnoreDDLsForSDD();
		IgnoreDDLsForSSD();
		IgnoreDDLsForTPCA();
		IgnoreDDLsForTPCC();
		IgnoreDDLsForTRD();
		IgnoreDDLsForTOF();
		IgnoreDDLsForHMPID();
		IgnoreDDLsForPHOS();
		IgnoreDDLsForDIMUON();
		IgnoreDDLsForFMD();
	        AddDDLsForT0();
		IgnoreDDLsForV0();
		IgnoreDDLsForZDC();
		IgnoreDDLsForACO();
		IgnoreDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 17: // V0 tab
		IgnoreDDLsForSPD();
		IgnoreDDLsForSDD();
		IgnoreDDLsForSSD();
		IgnoreDDLsForTPCA();
		IgnoreDDLsForTPCC();
		IgnoreDDLsForTRD();
		IgnoreDDLsForTOF();
		IgnoreDDLsForHMPID();
		IgnoreDDLsForPHOS();
		IgnoreDDLsForDIMUON();
		IgnoreDDLsForFMD();
		IgnoreDDLsForT0();
		AddDDLsForV0();
		IgnoreDDLsForZDC();
		IgnoreDDLsForACO();
		IgnoreDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 18: // ZDC tab
		IgnoreDDLsForSPD();
		IgnoreDDLsForSDD();
		IgnoreDDLsForSSD();
		IgnoreDDLsForTPCA();
		IgnoreDDLsForTPCC();
		IgnoreDDLsForTRD();
		IgnoreDDLsForTOF();
		IgnoreDDLsForHMPID();
		IgnoreDDLsForPHOS();
		IgnoreDDLsForDIMUON();
		IgnoreDDLsForFMD();
		IgnoreDDLsForT0();
		IgnoreDDLsForV0();
		AddDDLsForZDC();
		IgnoreDDLsForACO();
		IgnoreDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 19: // ACORDE tab
		IgnoreDDLsForSPD();
		IgnoreDDLsForSDD();
		IgnoreDDLsForSSD();
		IgnoreDDLsForTPCA();
		IgnoreDDLsForTPCC();
		IgnoreDDLsForTRD();
		IgnoreDDLsForTOF();
		IgnoreDDLsForHMPID();
		IgnoreDDLsForPHOS();
		IgnoreDDLsForDIMUON();
		IgnoreDDLsForFMD();
		IgnoreDDLsForT0();
		IgnoreDDLsForV0();
		IgnoreDDLsForZDC();
		AddDDLsForACO();
		IgnoreDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 20: // TRG tab
		IgnoreDDLsForSPD();
		IgnoreDDLsForSDD();
		IgnoreDDLsForSSD();
		IgnoreDDLsForTPCA();
		IgnoreDDLsForTPCC();
		IgnoreDDLsForTRD();
		IgnoreDDLsForTOF();
		IgnoreDDLsForHMPID();
		IgnoreDDLsForPHOS();
		IgnoreDDLsForDIMUON();
		IgnoreDDLsForFMD();
		IgnoreDDLsForT0();
		IgnoreDDLsForV0();
		IgnoreDDLsForZDC();
		IgnoreDDLsForACO();
		AddDDLsForTRG();
		IgnoreDDLsForEMCAL();
		break;
	case 21: // EMCAL tab
		IgnoreDDLsForSPD();
		IgnoreDDLsForSDD();
		IgnoreDDLsForSSD();
		IgnoreDDLsForTPCA();
		IgnoreDDLsForTPCC();
		IgnoreDDLsForTRD();
		IgnoreDDLsForTOF();
		IgnoreDDLsForHMPID();
		IgnoreDDLsForPHOS();
		IgnoreDDLsForDIMUON();
		IgnoreDDLsForFMD();
		IgnoreDDLsForT0();
		IgnoreDDLsForV0();
		IgnoreDDLsForZDC();
		IgnoreDDLsForACO();
		IgnoreDDLsForTRG();
		AddDDLsForEMCAL();
		break;

	default:
		cerr << "ERROR: Unknown tab selection: " << tabIndex << endl;
	};
	
	// Now add all the left over DDLs.
	for (size_t i = 0; i < fUnhandledDDLs.size(); i++)
	{
		const DDLSummaryInfo* info = fUnhandledDDLs[i];
		TString name;
		name += info->fExpectedDDL;
		TString gotDDL;
		gotDDL += info->fReceivedDDL;
		TString plugExp;
		plugExp += (const char*)info->fExpectedPlug;
		TString plugGot;
		plugGot += (const char*)info->fReceivedPlug;
		TString comment = "Received a DDL equipment ID outside the valid range.";
		TGLVEntry* e = new TGLVEntry(fListContents, name, name);
		e->SetSubnames(gotDDL, plugExp, plugGot, comment);
		e->SetPictures(fYellowPic, fYellowPic);
		fListContents->AddItem(e);
		fProblemWithDDLs = true;
	}
	
	if (fHomerReader->GetConnectionStatus() == 0)
	{
		if (fProblemWithDDLs or fUnhandledDDLs.size() > 0)
		{
			Pixel_t red;
			gClient->GetColorByName("red", red);
			fLabel->SetText(BAD_DDLS_MSG);
			fLabel->SetBackgroundColor(red);
		}
		else
		{
			Pixel_t green;
			gClient->GetColorByName("green", green);
			fLabel->SetText(DDLS_OK_MSG);
			fLabel->SetBackgroundColor(green);
		}
	}
	
	// Need to add empty element since the list view truncates the scroll box.
	TGLVEntry* e = new TGLVEntry(fListContents, "", "");
	e->SetSubnames("", "", "", "");
	e->SetPictures(fEmptyPic, fEmptyPic);
	fListContents->AddItem(e);
	
	fListView->Resize();
}


void GuiMainWindow::UpdateDDLView()
{
	FetchData();
	
	// Now update the DDL lists.
	ChangeTab(fTabBar->GetCurrent());
}


void GuiMainWindow::FetchData()
{
	// Try reconnect if there is a problem with the connection.
	if (fHomerReader->GetConnectionStatus() != 0)
	{
		delete fHomerReader;
		fHomerReader = new AliHLTHOMERReader(fHostname, fPort);
	}
	// Now check if we still have a connection problem.
	if (fHomerReader->GetConnectionStatus() != 0)
	{
		cerr << "ERROR: Could not connect to HOMER port " << int(fPort)
			<< " on host " << fHostname << ". Error code = "
			<< fHomerReader->GetConnectionStatus() << " ("
			<< strerror(fHomerReader->GetConnectionStatus()) << ")" << endl;
		Pixel_t yellow;
		gClient->GetColorByName("yellow", yellow);
		fLabel->SetText(NOT_RUNNING_MSG);
		fLabel->SetBackgroundColor(yellow);
		return;
	}
	
	int result = fHomerReader->ReadNextEvent(1000);
	if (result == ETIMEDOUT or result == EINTR)
	{
		// If we timed out or were interupted then just exit
		// and we will try again later (next time the fTimer expires).
		return;
	}
	if (result != 0)
	{
		cerr << "ERROR: Could not read next event from HOMER connection. Error code = "
			<< fHomerReader->GetConnectionStatus() << " ("
			<< strerror(fHomerReader->GetConnectionStatus()) << ")" << endl;
		Pixel_t yellow;
		gClient->GetColorByName("yellow", yellow);
		fLabel->SetText(NOT_RUNNING_MSG);
		fLabel->SetBackgroundColor(yellow);
		return;
	}
	
	// Clear the list of tested DDLs and fill in the information again.
	fTestedDDLs.clear();
	unsigned long blockCount = fHomerReader->GetBlockCnt();
	for (unsigned long i = 0; i < blockCount; i++)
	{
		unsigned long size = fHomerReader->GetBlockDataLength(i);
		const void* buffer = fHomerReader->GetBlockData(i);
		if (size != sizeof(DDLSummaryInfo))
		{
			cerr << "ERROR: Block size for block " << i << " of " << size
				<< " bytes does not correspond to the expected size of "
				<< sizeof(DDLSummaryInfo)
				<< " bytes. The block is corrupt so we will skip it." << endl;
			continue;
		}
		const DDLSummaryInfo* info = reinterpret_cast<const DDLSummaryInfo*>(buffer);
		fTestedDDLs.push_back(info);
	}
}


/////////////////////////////////////////////////////////////////////
// For reference the DDL ranges are:
//
// SPD
// 0 - 19
//
// SDD
// 256 - 279
//
// SSD
// 512 - 527
//
// TPC A side
// 768 - 803  840 - 911
//
// TPC C side
// 804 - 839  912 - 983
//
// TRD
// 1024 - 1041
//
// TOF
// 1280-1351
//
// HMPID
// 1536 - 1555
//
// PHOS
// 1792 - 1811
// 
// CPV
// 2048 - 2057
//
// PMD
// 2304 - 2309
//
// DIMUON
// 2560 - 2579  2816 - 2817
//
// FMD
// 3072 - 3074
//
// T0
// 3328
//
// V0
// 3584
//
// ZDC
// 3840
//
// ACORDE
// 4096
// 
// TRG
// 4352
//
// EMCAL
// 4608 - 4631
//
/////////////////////////////////////////////////////////////////////

void GuiMainWindow::AddDDLsForSPD()
{
	for (Int_t i = 0; i <= 19; i++) AddDDL(i);
}

void GuiMainWindow::AddDDLsForSDD()
{
	for (Int_t i = 256; i <= 279; i++) AddDDL(i);
}

void GuiMainWindow::AddDDLsForSSD()
{
	for (Int_t i = 512; i <= 527; i++) AddDDL(i);
}

void GuiMainWindow::AddDDLsForTPC()
{
	for (Int_t i = 768; i <= 983; i++) AddDDL(i);
}

void GuiMainWindow::AddDDLsForTPCA()
{
	for (Int_t i = 768; i <= 803; i++) AddDDL(i);
	for (Int_t i = 840; i <= 911; i++) AddDDL(i);
}

void GuiMainWindow::AddDDLsForTPCC()
{
	for (Int_t i = 804; i <= 839; i++) AddDDL(i);
	for (Int_t i = 912; i <= 983; i++) AddDDL(i);
}

void GuiMainWindow::AddDDLsForTRD()
{
	for (Int_t i = 1024; i <= 1041; i++) AddDDL(i);
}

void GuiMainWindow::AddDDLsForTOF()
{
	for (Int_t i = 1280; i <= 1351; i++) AddDDL(i);
}

void GuiMainWindow::AddDDLsForHMPID()
{
	for (Int_t i = 1536; i <= 1555; i++) AddDDL(i);
}

void GuiMainWindow::AddDDLsForPHOS()
{
	for (Int_t i = 1792; i <= 1811; i++) AddDDL(i);
}

void GuiMainWindow::AddDDLsForCPV()
{
	for (Int_t i = 2048; i <= 2057; i++) AddDDL(i);
}

void GuiMainWindow::AddDDLsForPMD()
{
	for (Int_t i = 2304; i <= 2309; i++) AddDDL(i);
}

void GuiMainWindow::AddDDLsForDIMUON()
{
	for (Int_t i = 2560; i <= 2579; i++) AddDDL(i);
	for (Int_t i = 2816; i <= 2817; i++) AddDDL(i);
}

void GuiMainWindow::AddDDLsForFMD()
{
	for (Int_t i = 3072; i <= 3074; i++) AddDDL(i);
}

void GuiMainWindow::AddDDLsForT0()
{
	AddDDL(3328);
}

void GuiMainWindow::AddDDLsForV0()
{
	AddDDL(3584);
}

void GuiMainWindow::AddDDLsForZDC()
{
	AddDDL(3840);
}

void GuiMainWindow::AddDDLsForACO()
{
	AddDDL(4096);
}

void GuiMainWindow::AddDDLsForTRG()
{
	AddDDL(4352);
}

void GuiMainWindow::AddDDLsForEMCAL()
{
	for (Int_t i = 4608; i <= 4631; i++) AddDDL(i);
}


void GuiMainWindow::IgnoreDDLsForSPD()
{
	for (Int_t i = 0; i <= 19; i++) IgnoreDDL(i);
}

void GuiMainWindow::IgnoreDDLsForSDD()
{
	for (Int_t i = 256; i <= 279; i++) IgnoreDDL(i);
}

void GuiMainWindow::IgnoreDDLsForSSD()
{
	for (Int_t i = 512; i <= 527; i++) IgnoreDDL(i);
}

void GuiMainWindow::IgnoreDDLsForTPCA()
{
	for (Int_t i = 768; i <= 803; i++) IgnoreDDL(i);
	for (Int_t i = 840; i <= 911; i++) IgnoreDDL(i);
}

void GuiMainWindow::IgnoreDDLsForTPCC()
{
	for (Int_t i = 804; i <= 839; i++) IgnoreDDL(i);
	for (Int_t i = 912; i <= 983; i++) IgnoreDDL(i);
}

void GuiMainWindow::IgnoreDDLsForTRD()
{
	for (Int_t i = 1024; i <= 1041; i++) IgnoreDDL(i);
}

void GuiMainWindow::IgnoreDDLsForTOF()
{
	for (Int_t i = 1280; i <= 1351; i++) IgnoreDDL(i);
}

void GuiMainWindow::IgnoreDDLsForHMPID()
{
	for (Int_t i = 1536; i <= 1555; i++) IgnoreDDL(i);
}

void GuiMainWindow::IgnoreDDLsForPHOS()
{
	for (Int_t i = 1792; i <= 1811; i++) IgnoreDDL(i);
}

void GuiMainWindow::IgnoreDDLsForCPV()
{
	for (Int_t i = 2048; i <= 2057; i++) IgnoreDDL(i);
}

void GuiMainWindow::IgnoreDDLsForPMD()
{
	for (Int_t i = 2304; i <= 2309; i++) IgnoreDDL(i);
}


void GuiMainWindow::IgnoreDDLsForDIMUON()
{
	for (Int_t i = 2560; i <= 2579; i++) IgnoreDDL(i);
	for (Int_t i = 2816; i <= 2817; i++) IgnoreDDL(i);
}

void GuiMainWindow::IgnoreDDLsForFMD()
{
	for (Int_t i = 3072; i <= 3074; i++) IgnoreDDL(i);
}

void GuiMainWindow::IgnoreDDLsForT0()
{
	IgnoreDDL(3328);
}

void GuiMainWindow::IgnoreDDLsForV0()
{
	IgnoreDDL(3584);
}

void GuiMainWindow::IgnoreDDLsForZDC()
{
	IgnoreDDL(3840);
}

void GuiMainWindow::IgnoreDDLsForACO()
{
	IgnoreDDL(4096);
}

void GuiMainWindow::IgnoreDDLsForTRG()
{
	IgnoreDDL(4352);
}

void GuiMainWindow::IgnoreDDLsForEMCAL()
{
	for (Int_t i = 4608; i <= 4631; i++) IgnoreDDL(i);
}


void GuiMainWindow::AddDDL(Int_t ddlid)
{
	// Find the ddlID in the fUnhandledDDLs list and remove the entries.
	std::vector<const DDLSummaryInfo*> foundList;
	size_t total = fUnhandledDDLs.size();
	for (size_t i = 0; i < total; i++)
	{
		if (total < i+1) continue;
		if (fUnhandledDDLs[total-i-1]->fExpectedDDL == ddlid)
		{
			foundList.push_back( fUnhandledDDLs[total-i-1] );
			fUnhandledDDLs.erase(fUnhandledDDLs.begin() + (total-i-1));
		}
	}
	// Check that all blocks are the same if we got more than one.
	// Take care to check if there are multiple info blocks for the specified
	// DDL ID and see if any of the other blocks are problematic.
	bool gotMultipleBlocks = false;
	const DDLSummaryInfo* info = NULL;
	if (foundList.size() > 0) info = foundList[0];
	total = fUnhandledDDLs.size();
	for (size_t i = 1; i < foundList.size(); i++)
	{
		if (total < 1+i) continue;
		size_t pos = total - 1 - i;
		if (fUnhandledDDLs[pos]->fExpectedDDL == ddlid)
		{
			if (info != NULL)
			{
				gotMultipleBlocks = true;
				if (fUnhandledDDLs[pos]->fReceivedDDL != info->fReceivedDDL and
				    (fUnhandledDDLs[pos]->fReceivedDDL != fUnhandledDDLs[pos]->fExpectedDDL or
				     fUnhandledDDLs[pos]->fReceivedPlug != fUnhandledDDLs[pos]->fExpectedPlug
				   ))
				{
					info = fUnhandledDDLs[pos];
				}
			}
			else
			{
				info = fUnhandledDDLs[pos];
			}
			fUnhandledDDLs.erase(fUnhandledDDLs.begin() + pos);
		}
	}
	if (info == NULL)
	{
		TString name;
		name += ddlid;
		TGLVEntry* e = new TGLVEntry(fListContents, name, name);
		e->SetSubnames("-", "-", "-", "Did not receive any data over this DDL.");
		e->SetPictures(fYellowPic, fYellowPic);
		fListContents->AddItem(e);
		fProblemWithDDLs = true;
		return;
	}

	TString name;
	name += info->fExpectedDDL;
	TString gotDDL;
	gotDDL += info->fReceivedDDL;
	TString plugExp;
	plugExp += (const char*)info->fExpectedPlug;
	TString plugGot;
	plugGot += (const char*)info->fReceivedPlug;
	
	bool ddlHasProblem = false;
	TString comment = "DDL OK";
	if (gotMultipleBlocks)
	{
		comment = "Received multiple summary information data blocks.";
	}
	for (int n = 0; n < 1024; n++)
	{
		if (info->fExpectedPlug[n] != info->fReceivedPlug[n])
		{
			comment = "Expected and received plugs are not the same.";
			fProblemWithDDLs = true;
			ddlHasProblem = true;
			break;
		}
	}
	if (info->fExpectedDDL != info->fReceivedDDL)
	{
		comment = "Expected and received DDL equipment IDs are not the same.";
		fProblemWithDDLs = true;
		ddlHasProblem = true;
	}
	
	TGLVEntry* e = new TGLVEntry(fListContents, name, name);
	e->SetSubnames(gotDDL, plugExp, plugGot, comment);
	if (ddlHasProblem)
		e->SetPictures(fRedPic, fRedPic);
	else
		e->SetPictures(fGreenPic, fGreenPic);
	fListContents->AddItem(e);
}


void GuiMainWindow::IgnoreDDL(Int_t ddlid)
{
	// Find the ddlID in the fUnhandledDDLs list and remove the entry.
	// Do not stop once the entry is removed to make sure we remove duplicates also.
	size_t total = fUnhandledDDLs.size();
	for (size_t i = 0; i < total; i++)
	{
		if (total < 1+i) continue;
		size_t pos = total - 1 - i;
		if (fUnhandledDDLs[pos]->fExpectedDDL == ddlid)
		{
			fUnhandledDDLs.erase(fUnhandledDDLs.begin() + pos);
		}
	}
}

