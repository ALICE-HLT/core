/**************************************************************************
 * This file is property of and copyright by the ALICE HLT Project        *
 * All rights reserved.                                                   *
 *                                                                        *
 * Primary Authors:                                                       *
 *   Artur Szostak <artursz@iafrica.com>                                  *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

#include "TApplication.h"
#include "GuiMainWindow.h"
#include "Riostream.h"
#include <cstdlib>

int main(int argc, char** argv)
{
	const char* hostname = "fephltout0";
	unsigned short port = 49152;
	
	int reducedArgc = 1;
	typedef char* PChar;
	char** reducedArgv = new PChar[argc];
	reducedArgv[0] = argv[0];
	
	// Parse the remaining command line arguments looking for -host and -port fields.
	for (int i = 1; i < argc; i++)
	{
		if (strcmp(argv[i], "-host") == 0)
		{
			if (++i >= argc)
			{
				cerr << "ERROR: No hostname given for parameter '-host'." << endl;
				delete [] reducedArgv;
				return EXIT_FAILURE;
			}
			hostname = argv[i];
			continue;
		}
		if (strcmp(argv[i], "-port") == 0)
		{
			if (++i >= argc)
			{
				cerr << "ERROR: No port number given for parameter '-port'." << endl;
				delete [] reducedArgv;
				return EXIT_FAILURE;
			}
			char* end = NULL;
			long num = strtol(argv[i], &end, 0);
			if (end == NULL or *end != '\0')
			{
				cerr << "ERROR: Expected a port number in the range [0..65535], but received '"
					<< argv[i] << "'." << endl;
				delete [] reducedArgv;
				return EXIT_FAILURE;
			}
			if (num < 0 or 65535 < num)
			{
				cerr << "ERROR: Expected a port number in the range [0..65535], but received '"
					<< argv[i] << "'." << endl;
				delete [] reducedArgv;
				return EXIT_FAILURE;
			}
			port = (unsigned short)num;
			continue;
		}
		
		reducedArgv[reducedArgc] = argv[i];
		reducedArgc++;
	}

	TApplication app("DDLTestGUI", &reducedArgc, reducedArgv);
	GuiMainWindow* gui = new GuiMainWindow(hostname, port);
	gui->SetWindowName("DDL Test GUI");

	// kTRUE makes the application return from the event loop on termination.
	app.Run(kTRUE);

	delete gui;
	delete [] reducedArgv;

	return EXIT_SUCCESS;
}
