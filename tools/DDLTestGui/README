The DDLTestGui utility is used in conjunction with the DDLConnectionTester
component that is distributed with the HLT Publisher/Subscriber data transport
framework. The test performed will indicate if there is connectivity between
the DAQ and the HLT over a particular DDL and if the HLT and DAQ agree on the
equipment ID of the DDL (i.e. is the DDL the expected one). This kind of test
is very useful to identify swapped or incorrect cabling.

The DDLConnectionTester component must be setup in a HLT chain. One such component
must be connected to each RORCPublisher. For example, the following process tag
should be used in the HLT chain configuration XML at the partition/ddl level:

<Process ID="DDLCHK" type="prc">
  <Command>DDLConnectionTester -ddlid ${DDL}</Command>
  <Parent>DDL</Parent>
  <Shm blocksize="4k" blockcount="10"/>
  <ForceFEP/>
</Process>

The output from these components should all be collected and merged together
to a TCPDumpSubscriber component. This can be done by adding the Relay component
to the XML chain config and subscribing it to all the DDLConnectionTester
components. The DDLTestGui should then try to connect to the TCPDumpSubscriber
component while the chain is running to be able to display the results of the
DDL connection test. The HOMER interface is used to connect to the TCPDumpSubscriber
over the network and fetch the results.

There are two command line options for the DDLTestGui program:
-host <name>    Allows one to specify the hostname on which the TCPDumpSubscriber
                has opened up its server socket connection.
-port <number>  The port number on which the TCPDumpSubscriber is listening for
                new incoming connections.

An important note: The RORCPublisher must have the following options set,
"-disableddlheadersearch -publishcorruptevents -enumerateeventids".
These are important because the set pattern sent by DAQ to the HLT are not in the
normal DDL format so the DDL header is missing in the data stream.
The format of the event data sent is as follows: 100 32-bit little endian words,
where the first word always contains 0x1 and the remaining 99 words contain the DDL
equipment ID as known by DAQ for the DDL over which it sent the event.

