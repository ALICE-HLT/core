#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>

#define handle_error(msg)				\
  do { perror(msg); exit(EXIT_FAILURE); } while (0)

int copy(char* addr, int out_fd, off_t length);

int
main(int argc, char *argv[])
{
  char *addr;
  int fd, out_fd;
  struct stat sb;
  off_t length;
  int s;

  if (argc != 3) {
    fprintf(stderr, "%s infile outfile\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  fd = open(argv[1], O_RDONLY);
  if (fd == -1)
    handle_error("open infile");

  if (fstat(fd, &sb) == -1)           /* To obtain file size */
    handle_error("fstat");

  length = sb.st_size;

  addr = mmap(NULL, length, PROT_READ,
	      MAP_PRIVATE, fd, 0);
  if (addr == MAP_FAILED)
    handle_error("mmap");

  out_fd = creat(argv[2], sb.st_mode);
  if (out_fd == -1)
    handle_error("open outfile");
  
  
  s = copy(addr, out_fd, length);
  if (s != length+8) {
    if (s == -1)
      handle_error("write");

    fprintf(stderr, "partial write");
    exit(EXIT_FAILURE);
  }

  exit(EXIT_SUCCESS);
}

int copy(char* addr, int out_fd, off_t length) {
  int ret=0;
  uint32_t word;
  const uint32_t version = 0x3;
  uint32_t* p;

  if(length < (8*4))
    return ret;

  // first word
  ret=write(out_fd, addr, 4);
  if(ret != 4)
    return ret;
  addr+=4;

  p=(uint32_t*)addr;

  word=(*p) | (version << 24);

  // write version = 3
  ret+=write(out_fd, (char*) &word, 4);
  if(ret != 8)
    return ret;
  //write next 6 words
  addr+=4;
  ret+=write(out_fd, addr, 24);
  if(ret!=32)
    return ret;
  //write 2 extra words
  word=0x0;
  ret+=write(out_fd, (char*) &word, 4);
  if(ret != 36)
    return ret;
  ret+=write(out_fd, (char*) &word, 4);
  if(ret != 40)
    return ret;
  //write rest
  addr+=24;
  ret+=write(out_fd, addr, length-32);
  return ret;
}
