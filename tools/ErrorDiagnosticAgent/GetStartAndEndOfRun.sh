#!/bin/bash

# Author: Artur Szostak
# Date: 27 July 2011
#
# Returns run number, time stamp and message for messages of the form "%Starting%_RUN%" and "%END of%_RUN%".
# These are searched for in the DAQ logs and allows one to identify the start and end times of a run
# initiated by ECS.


# Declare a function to print the usage of this script. i.e. print the command line options.
function PrintUsage
{
	cat <<EOF
Usage: GetStartAndEndOfRun.sh [-h | --help] [-s=<timestamp> | --start=<timestamp>]
    [-e=<timestamp> | --end=<timestamp>] [-t=<seconds> | --period=<seconds>] [-p=<name> | --partition=<name>]

where
  --start=<timestamp> | -s=<timestamp>
    The start time stamp of the earliest message to look as an integer.
  --end=<timestamp> | -e=<timestamp>
    The end time stamp of the last message to look for as an integer.
  --period=<seconds> | -t=<seconds>
    If this is specified instead then the messages for the last <seconds> number of seconds will be fetched.
  --partition=<name> | -p=<name>
    The name of the partition for which to find messages.

By default, if no options are specified then the PHYSICS_1 partition is used and messages are searched for
in the last 24 hours.
EOF
}


# Parse the command line parameters.
for CMD in $* ; do
	# Parse the option from the string of the form "-*=*"
	ARG=`echo $CMD | sed 's/-[-_a-zA-Z0-9]*=//'`

	case "$CMD" in
		--help | -h )
			PrintUsage
			exit 0
			;;

		--start=* | -s=* )
			if test -n "$START_TIME" ; then
				echo "ERROR: Option -start|-s already used earlier." 1>&2
				exit 2
			fi
			if test -z "$ARG" ; then
				echo "ERROR: No argument given for option $CMD" 1>&2
				exit 3
			fi
			START_TIME=$ARG
			;;

		--end=* | -e=* )
			if test -n "$END_TIME" ; then
				echo "ERROR: Option -end|-e already used earlier." 1>&2
				exit 2
			fi
			if test -z "$ARG" ; then
				echo "ERROR: No argument given for option $CMD" 1>&2
				exit 3
			fi
			END_TIME=$ARG
			;;

		--period=* | -t=* )
			if test -n "$DELTA" ; then
				echo "ERROR: Option -period|-t already used earlier." 1>&2
				exit 2
			fi
			if test -z "$ARG" ; then
				echo "ERROR: No argument given for option $CMD" 1>&2
				exit 3
			fi
			DELTA=$ARG
			;;

		--partition=* | -p=* )
			if test -n "$PARTITION" ; then
				echo "ERROR: Option -partition|-p already used earlier." 1>&2
				exit 2
			fi
			if test -z "$ARG" ; then
				echo "ERROR: No argument given for option $CMD" 1>&2
				exit 3
			fi
			PARTITION=$ARG
			;;

		* )
			echo "ERROR: Unknown option '$CMD'" 1>&2
			PrintUsage
			exit 1
	esac
done

# Set some default values if not already given on the command line.
if test -z "$PARTITION" ; then
	PARTITION=PHYSICS_1
fi
if test -n "$DELTA" -a -n "$START_TIME" -o -n "$DELTA" -a -n "$END_TIME" ; then
	echo "ERROR: Both -period|-t and -start|-s or -end|-e options have been used. If the period option is used then the start and end time options cannot be used. They are mutually exclusive." 1>&2
	exit 4
fi
NOW=`date +%s`
if test -n "$DELTA" ; then
	let START_TIME=$NOW-$DELTA
        END_TIME=$NOW
fi
if test -z "$START_TIME" -a -z "$END_TIME" ; then
	let START_TIME=$NOW-86400
	END_TIME=$NOW
fi
if test -z "$START_TIME" ; then
	let START_TIME=0
fi
if test -z "$END_TIME" ; then
	END_TIME=$NOW
fi


# Setup the DATE environment variables used in the mysql command to follow.
. /opt/dateHI/date_HI.sh -q

mysql -h $DATE_INFOLOGGER_MYSQL_HOST -u $DATE_INFOLOGGER_MYSQL_USER -p$DATE_INFOLOGGER_MYSQL_PWD $DATE_INFOLOGGER_MYSQL_DB -B -N -e\
 "select run,timestamp,message from messages where timestamp>=$START_TIME and timestamp<=$END_TIME and facility=\"PCA\" and partition=\"$PARTITION\"\
 and (message like \"%Starting%_RUN%\" or message like \"%END of%_RUN%\") order by timestamp;"

