#!/bin/bash

# Author: Artur Szostak
# Date: 8 July 2011
#
# This script is useful to setup the environment for the ErrorDiagnosticAgent.C macro
# to use root. It is necessary only when the software is not automatically loaded via
# module files for example. This is the case right now on hlt-prod for example.
# It should be used as follows:
#
#   $ source setenv.sh
#   $ root -b -q -l ErrorDiagnosticAgent.C+
#
# Modifications might be necessary if the root version changes on the cluster.

export ROOTSYS=/opt/HLT/root/root_v5-27-06d
export PATH=$PATH:$ROOTSYS/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOTSYS/lib

export ALICE_ROOT=/opt/HLT/aliroot/AliRoot_v4-20-Rev-01
export ALICE_TARGET=linuxx8664gcc
export PATH=$PATH:$ALICE_ROOT/bin/tgt_$ALICE_TARGET/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ALICE_ROOT/lib/tgt_$ALICE_TARGET/
