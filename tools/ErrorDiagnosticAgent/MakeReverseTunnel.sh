#!/bin/bash

# Author: Artur Szostak
# Date: 6 July 2011
#
# Make a reverse SSH tunnel to the portal-dcs1 machine (alihlt-dcs0).
# This is to create a connection between the HLT cluster and the HLT ACR machine
# on the DAQ network.

while true ; do ssh -N -R 2222:localhost:22 acr@alihlt-dcs0 ; done
