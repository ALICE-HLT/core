#!/bin/bash

# Author: Artur Szostak
# Date: 6 July 2011
#
# Returns the number of fatal error messages for the HLT in the last few seconds found
# in the DAQ log books.
# One command line parameter is taken D, the number of seconds D as an integer
# If this parameter is specified then the error message count is returned in the
# last D seconds.

if test -z $1 ; then
	# Set some default value like the last 60 seconds
	DELTA=60
else
	DELTA=$1
fi
NOW=`date +%s`
let TIME=$NOW-$DELTA

if test -z $2 ; then
	PARTITION=PHYSICS_1
else
	PARTITION=$2
fi

# Setup the DATE environment variables used in the mysql command to follow.
. /opt/dateHI/date_HI.sh -q

mysql -h $DATE_INFOLOGGER_MYSQL_HOST -u $DATE_INFOLOGGER_MYSQL_USER -p$DATE_INFOLOGGER_MYSQL_PWD $DATE_INFOLOGGER_MYSQL_DB -B -N -e\
 "select count(message) from messages where timestamp>=$TIME and severity=\"F\" and facility=\"PCA\" and partition=\"$PARTITION\" and message like \"%HLT%\";"
