#!/bin/bash

# Author: Artur Szostak
# Date: 6 July 2011
#
# Returns the time stamp of the message "Taking data" for a given run in the DAQ log.
# This can be used to check if the current run is a real DAQ/ECS run or just a HLT standalone run.

if test -z $1 ; then
	echo "ERROR: No run number specified." 1>&2
	exit 1
fi

# Setup the DATE environment variables used in the mysql command to follow.
. /opt/dateHI/date_HI.sh -q

mysql -h $DATE_INFOLOGGER_MYSQL_HOST -u $DATE_INFOLOGGER_MYSQL_USER -p$DATE_INFOLOGGER_MYSQL_PWD $DATE_INFOLOGGER_MYSQL_DB -B -N -e\
 "select timestamp from messages where facility=\"PCA\" and run=\"$1\" and message = \"Taking data\";"
