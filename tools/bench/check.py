#!/usr/bin/env python

import sys
import RunConfig

#Run configuration; set before importing Components..
RunConfig.iterations=1
RunConfig.runnumber=0
RunConfig.beamtype='pp'
RunConfig.verbosity='0x78'
#RunConfig.eventDir='./0/raw/raw0'
RunConfig.hcdbDir='./0/HCDB'
#RunConfig.outDir='out0'
#RunConfig.eventTriggerFile=RunConfig.outDir + "/etd.bin"

i=0
if len(sys.argv)>1:
    i=int(sys.argv[1])

RunConfig.eventDir='./0/raw/raw' + str(i)
RunConfig.outDir='out0/raw' + str(i)
RunConfig.eventTriggerFile=RunConfig.outDir + "/etd.bin"
print "processing event " + str(i)



#some SPD ddls are missing (7,9,17,19), exclude them
#RunConfig.DDLs['ISPD']=range(0, 7) + [8] + range(10,17) + [18]


#now import pre-defined components
import TPC
#import ITS
import Global



def main():
    #TPC.HWClusterFinderEmulator.run(quiet=False, debug=True)
    #TPC.HWClusterFinderEmulator.run(quiet=False)
    #TPC.HWClusterDecoder.run(quiet=False)
    #TPC.CATracker.run(deps=True)
    TPC.CATracker.run()
    TPC.CATrackerOpenCL.run()

if __name__ == "__main__":
    main()
