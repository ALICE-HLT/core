#!/usr/bin/env python

import sys
import RunConfig
import os

#Run configuration; set before importing Components..
RunConfig.iterations=1
RunConfig.runnumber=230454
RunConfig.beamtype='pp'
RunConfig.verbosity='0x78'
#RunConfig.eventDir='/home/hlt-test/HLT/operator/data/225589/sample/raw'
RunConfig.hcdbDir='/opt/HLT-DEV/HCDB'
#RunConfig.outDir='/home/hlt-test/HLT/operator/data/225589/sample/hwclust1'
#RunConfig.eventTriggerFile=RunConfig.outDir + "/etd.bin"

#some SPD ddls are missing (7,9,17,19), exclude them
#RunConfig.DDLs['ISPD']=range(0, 7) + [8] + range(10,17) + [18]
RunConfig.DDLs['TPC']=set(RunConfig.DDLs['TPC']) - set([820, 821, 944, 945, 946, 947])

#now import pre-defined components
import TPC
#import ITS
#import Global

baseDir='/home/data/evtrpl_data/230454/sample'
eventDir = baseDir+'/raw'
outDir   = baseDir+'/hwclust1'

def main():
  for root, dirnames, filenames in os.walk(eventDir):
    for rawdir in dirnames:
      RunConfig.eventDir = os.path.join(eventDir, rawdir)
      RunConfig.outDir = os.path.join(outDir, rawdir)
      RunConfig.eventTriggerFile=RunConfig.outDir + "/etd.bin"
      print "preparing",RunConfig.outDir
      TPC.HWClusterFinderEmulator.run(quiet=True, fork=True)

if __name__ == "__main__":
    main()
