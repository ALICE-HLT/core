#!/usr/bin/env python

import subprocess
import os
import sys
import re
import threading

import RunConfig

class Component:
    wrapperbin='SimpleComponentWrapper'

    def __init__ (self,
                  prepend_cmd=[],
                  componentid=None,
                  componentlibrary=None,
                  componentargs=None,
                  addlibrary=[],
                  parents=[],
                  inData=[],
                  outData=[] ):
        self.prepend_cmd=prepend_cmd
        self.componentid=componentid
        self.componentlibrary=componentlibrary
        self.componentargs=componentargs
        self.addlibrary=addlibrary
        self.parents=parents
        self.inData=inData
        self.outData=outData

    def getFilePath(self, origin, dataType, spec):
        if dataType == 'DDL_RAW':
            #fix for ITS: e.g. replace ISPD with ITSSPD
            return RunConfig.eventDir + '/' + re.sub(r'^IS(.)D', r'ITSS\1D',origin) + '_' +  spec + '.ddl'
        else:
            return RunConfig.outDir + '/' + origin + '_' + dataType + '_' + spec

    def getInputArgs(self):
        ret=[]
        data=list(self.inData)
        for parent in self.parents:
            data += parent.outData
        for (dataType, origin, spec) in data:
            infile=self.getFilePath(origin, dataType, spec)
            #if not os.path.isfile(infile):
                #continue
            #check if spec is actually a specification (i.e. hex, '0x....')
            if len(spec) > 2 and spec[0:2] == '0x':
                ret += ['-datatype', dataType, origin, '-dataspec', spec, '-infile', infile]
            #otherwise it's a ddl
            else:
                ret += ['-ddlid', spec, '-datatype' , dataType, origin, '-infile', infile]
        return ret

    def getOutputArgs(self):
        ret=[]
        for (dataType, origin, spec) in self.outData:
            outfile=self.getFilePath(origin, dataType, spec)
            ret += ['-outfile', outfile]
        return ret

    def getIOStats(self):
        inSize=0.
        outSize=0.
        ratio=0.
        data=list(self.inData)
        for parent in self.parents:
            data += parent.outData
        for (dataType, origin, spec) in data:
            infile=self.getFilePath(origin, dataType, spec)
            if os.path.exists(infile):
                inSize+=os.path.getsize(infile)
        for (dataType, origin, spec) in self.outData:
            outfile=self.getFilePath(origin, dataType, spec)
            if os.path.exists(outfile):
                outSize+=os.path.getsize(outfile)
        if outSize != 0.:
            ratio=outSize/inSize
        return [inSize, outSize, ratio]

    def run (self, quiet=True, debug=False, deps=False, runlist=[], fork=False):
        self.ret=0
        if deps:
            if self.componentid in runlist:
                #already done (multiple occurence)
                return 0
            for comp in self.parents:
                self.ret=comp.run(quiet, debug, deps)
                runlist.append(comp.componentid)
                if self.ret != 0:
                    return self.ret
        cmd=[]
        cmd+=self.prepend_cmd
        if len(self.prepend_cmd)>0:
            quiet=False
        cmd+=[Component.wrapperbin, 
              '-componentid', self.componentid, 
              '-componentlibrary', self.componentlibrary]
        if self.componentargs:
            cmd += ['-componentargs',self.componentargs]
        for lib in self.addlibrary:
            cmd += ['-addlibrary', lib]
        cmd += self.getInputArgs() + self.getOutputArgs()
        if RunConfig.eventTriggerFile != "":
            if len(self.parents) == 0:
                cmd += ['-eventtriggerfrominfile',
                        '-eventtriggeroutfile', RunConfig.eventTriggerFile]
            else:
                cmd += ['-eventtriggerinfile', RunConfig.eventTriggerFile]
        cmd += ['-nextevent', 
                '-eventiterations', str(RunConfig.iterations), 
                '-runnumber', str(RunConfig.runnumber), 
                '-beamtype', RunConfig.beamtype,
                '-ecsparameters', RunConfig.getECSParams(),
                '-V', RunConfig.verbosity]
        if debug:
            print ''.join(x + ' ' for x in cmd)

        if not os.path.isdir(RunConfig.outDir):
            os.makedirs(RunConfig.outDir)

        proc_env=os.environ
        proc_env['ALIHLT_HCDBDIR']=RunConfig.hcdbDir

        if quiet:
            thr=threading.Thread(target=self.runThread, args=(cmd,proc_env,))
            thr.start()
            if fork:
                return thr
            else:
                thr.join()
                return self.ret 
        else:
            self.ret=subprocess.call(cmd,env=proc_env)
        if self.ret != 0:
            print 'Failed running ' + self.componentid
        return self.ret


    def runThread(self, cmd, proc_env):
        proc=subprocess.Popen(cmd,
                              stdout=subprocess.PIPE,
                              stderr=subprocess.PIPE,
                              env=proc_env)
        (out, err)=proc.communicate()
        ret=proc.wait()
        if ret != 0:
            print out
            print err
            print self.componentid + " returned an error: " + str(ret)
            self.ret=ret
            return
        sys.stderr.write(err)
        initTime=0.
        for line in out.splitlines():
            if line[0:5] == 'Init:':
                initTime=float(line.split(' ')[8])
            if line[0:8] == 'Event 1:':
                time=float(line.split(' ')[9])
                stats=self.getIOStats()
                print "%s: %.2f ms proc, %.2f ms init, %.0f kB in, %.0f kB out, %.2f ratio" % (self.componentid, time, initTime, stats[0]/1024., stats[1]/1024., stats[2])
                self.ret=ret
                return
