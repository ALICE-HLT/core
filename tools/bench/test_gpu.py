#!/usr/bin/env python

import sys
import RunConfig
import threading

#Run configuration; set before importing Components..
RunConfig.iterations=100
RunConfig.runnumber=169838
RunConfig.beamtype='AA'
RunConfig.verbosity='0x78'
RunConfig.eventDir='./169838/raw/raw70'
RunConfig.hcdbDir='./169838/HCDB'
RunConfig.outDir='/tmp/gpu-test'
RunConfig.eventTriggerFile=RunConfig.outDir + "/etd.bin"

#some SPD ddls are missing (7,9,17,19), exclude them
RunConfig.DDLs['ISPD']=range(0, 7) + [8] + range(10,17) + [18]

#now import pre-defined components
import TPC
import ITS
import Global



def main():
    numThreads=1
    threads=[]
    #RunConfig.iterations=1
    #TPC.CATrackerOpenCL.run(deps=True)
    TPC.CATrackerOpenCL.componentargs='-allowGPU -GPUHelperThreads 1 -GlobalTracking -GPULibrary libAliHLTTPCCAGPUOpenCL.so'
    for i in range(0,numThreads):
        thr=threading.Thread(target=TPC.CATrackerOpenCL.run)
        thr.start()
        threads.append(thr)
    for thr in threads:
        thr.join()

if __name__ == "__main__":
    main()
