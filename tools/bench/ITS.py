#!/usr/bin/env python
import sys
import RunConfig
from Component import Component

ClusterFinderSPD=Component(
    componentid='ITSClusterFinderSPD',
    componentlibrary='libAliHLTITS.so',
    inData=RunConfig.makeDDLData('DDL_RAW', 'ISPD'),
    outData=RunConfig.makeDDLData('CLUSTERS', 'ISPD')
)

ClusterFinderSDD=Component(
    componentid='ITSClusterFinderSDD',
    componentlibrary='libAliHLTITS.so',
    inData=RunConfig.makeDDLData('DDL_RAW', 'ISDD'),
    outData=RunConfig.makeDDLData('CLUSTERS', 'ISDD')
)

ClusterFinderSSD=Component(
    componentid='ITSClusterFinderSSD',
    componentlibrary='libAliHLTITS.so',
    inData=RunConfig.makeDDLData('DDL_RAW', 'ISSD'),
    outData=RunConfig.makeDDLData('CLUSTERS', 'ISSD')
)

VertexerSPD=Component(
    componentid='ITSVertexerSPD',
    componentlibrary='libAliHLTITS.so',
    parents=[ClusterFinderSPD],
    outData=[('ESDVTXV0','ISPD','0x00000000')]
)

SAPTracker=Component(
    componentid='ITSSAPTracker',
    componentlibrary='libAliHLTITS.so',
    parents=[ClusterFinderSPD, ClusterFinderSDD, ClusterFinderSSD, VertexerSPD],
    outData=[('SAPTRACK','ITS','0xFFFFFFFF'), ('FLATVTX0','ITS','0xFFFFFFFF')]
)

def runAll():
    ret=SAPTracker.run(deps=True)
    return ret


if __name__ == "__main__":
    ret=runAll()
    sys.exit(ret)
