#!/usr/bin/env python
import sys
import RunConfig
from Component import Component


def makeTPCSectorData(dataType):
    data=[]
    for sector in RunConfig.Sectors['TPC']:
        specification="0x%0.2X%0.2X0500" % (sector,sector)
        data.append((dataType, 'TPC', specification))
    return data

HWClusterFinderEmulator=Component(
    componentid='TPCHWClusterFinderEmulator', 
    componentlibrary='libAliHLTTPC.so',
    inData=RunConfig.makeDDLData('DDL_RAW','TPC'),
    outData=RunConfig.makeDDLData('HWCLUST1','TPC')
)

HWClusterDecoder=Component(
    componentid='TPCHWClusterDecoder',
    componentlibrary='libAliHLTTPC.so',
    parents=[HWClusterFinderEmulator],
    outData=RunConfig.makeDDLData('CLUSTRAW', 'TPC') + [('CLRAWDSC', 'TPC', '0xFFFFFFFF')]
)

DataCompressor=Component(
    componentid='TPCDataCompressor',
    componentlibrary='libAliHLTTPC.so',
    componentargs='-deflater-mode 2 -mode 1',
    parents=[HWClusterDecoder],
    outData=RunConfig.makeDDLData('REMCLSCM', 'TPC') + [('COMPDESC', 'TPC', '0xFFFFFFFF')]
)

DataCompressorMonitor=Component(
    componentid='TPCDataCompressorMonitor',
    componentlibrary='libAliHLTTPC.so',
    componentargs='-pushback-period=10',
    parents=[HWClusterFinderEmulator,HWClusterDecoder,DataCompressor],
    #outData=RunConfig.makeDDLData('REMCLSCM', 'TPC') + [('COMPDESC', 'TPC', '0xFFFFFFFF')]
)

ClusterTransformation=Component(
    componentid='TPCClusterTransformation',
    componentlibrary='libAliHLTTPC.so',
    parents=[HWClusterDecoder],
    outData=RunConfig.makeDDLData('CLUSTERS','TPC')
)

CATracker=Component(
    componentid='TPCCATracker',
    componentlibrary='libAliHLTTPC.so',
    componentargs='-GlobalTracking',
    parents=[ClusterTransformation],
    outData=makeTPCSectorData('CATRACKL')
)

CATrackerOpenCL=Component(
    componentid='TPCCATracker',
    componentlibrary='libAliHLTTPC.so',
    componentargs='-allowGPU -GPUHelperThreads 1 -GlobalTracking -GPULibrary libAliHLTTPCCAGPUOpenCL.so',
    #addlibrary=['libamdocl64.so'],
    parents=[ClusterTransformation],
    outData=makeTPCSectorData('CATRACKL')
)

CATrackerCUDA=Component(
    componentid='TPCCATracker',
    componentlibrary='libAliHLTTPC.so',
    componentargs='-allowGPU -GPUHelperThreads 1 -GlobalTracking -GPULibrary libAliHLTTPCCAGPU.so',
    #addlibrary=['libamdocl64.so'],
    parents=[ClusterTransformation],
    outData=makeTPCSectorData('CATRACKL')
)

CAGlobalMerger=Component(
    componentid='TPCCAGlobalMerger',
    componentlibrary='libAliHLTTPC.so',
    parents=[CATracker],
    outData=[('HLTTRACK', 'TPC', '0x23000500')]
)

DataCompressorMonitorOutData=[]
for i in range(0, 105):
    DataCompressorMonitorOutData.append(('ROOTHIST', 'TPC', str(i)))

DataCompressorMonitor=Component(
    componentid='TPCDataCompressorMonitor',
    componentlibrary='libAliHLTTPC.so',
    #componentargs='-pushback-period=200 -publishing-mode off',
    parents=[DataCompressor, HWClusterFinderEmulator],
    outData=DataCompressorMonitorOutData
)

DataCompressorMonitorWriter=Component(
    componentid='ROOTFileWriter',
    componentlibrary='libAliHLTUtil.so',
    componentargs='-datafile out/HIST.root',
    parents=[DataCompressorMonitor],
    outData=[]
)

def runAll():
    ret=CAGlobalMerger.run(deps=True)
    if ret != 0:
        return ret
    ret=DataCompressor.run()
    return ret

if __name__ == "__main__":
    ret=runAll()
    sys.exit(ret)
