MYPATH=`pwd`
RAWDIRS=`ls raw`

pushd . > /dev/null
cd $ALIHLT_CORE_DIR/tools/bench/

echo -e "all: TARGET\n" > Makefile
TARGETS=
for i in $RAWDIRS; do
  cat prepare_hwclust.py | sed "s,XXXX,$MYPATH/raw/$i,g" | sed "s,YYYY,$MYPATH/hwclust1/$i,g" > prepare_hwclust_tmp_$i.py
  chmod +x prepare_hwclust_tmp_$i.py
  echo -e "hwclust/$i:\n\t./prepare_hwclust_tmp_$i.py\n" >> Makefile
  TARGETS+="hwclust/$i "
done

echo -e "TARGET: $TARGETS\n" >> Makefile

make -j 32

rm -f Makefile

for i in $RAWDIRS; do
  rm prepare_hwclust_tmp_$i.py
done

popd > /dev/null
for i in $RAWDIRS; do
  for j in `ls hwclust1/$i | grep -v etd.bin`; do
    mv hwclust1/$i/$j hwclust1/$i/`echo $j | sed "s/_HWCLUST1_/_/g"`.ddl
  done
done
