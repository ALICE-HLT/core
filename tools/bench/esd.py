#!/usr/bin/env python

import sys
import RunConfig

#Run configuration; set before importing Components..
RunConfig.iterations=1
RunConfig.runnumber=169838
RunConfig.beamtype='AA'
RunConfig.verbosity='0x78'
RunConfig.eventDir='./169838/raw/raw135'
RunConfig.hcdbDir='./169838/HCDB'
RunConfig.outDir='out135'
RunConfig.eventTriggerFile=RunConfig.outDir + "/etd.bin"

#some SPD ddls are missing (7,9,17,19), exclude them
RunConfig.DDLs['ISPD']=range(0, 7) + [8] + range(10,17) + [18]


#now import pre-defined components
import TPC
import ITS
import Global



def main():
    #RunConfig.iterations=10
    #ITS.SAPTracker.run(quiet=False)
    #Global.ITSTracker.run(quiet=False)
    #Global.EsdConverter.run(quiet=True, deps=True)
    Global.EsdConverter.run(deps=False, quiet=True)
    Global.FlatEsdConverter.run(deps=False, quiet=True)
    #Global.TPCCalibManagerComponent.prepend_cmd=['gdb','--args']  
    Global.TPCCalibManagerComponent.run(deps=False, quiet=True)
    Global.TPCCalibManagerComponentFlat.run(deps=False, quiet=True)
    #Global.TPCCalibManagerComponent.run(deps=True)
    #Global.ROOTFileWriterESD.run(deps=False, quiet=False)
    
if __name__ == "__main__":
    main()
