#!/usr/bin/env python

import sys
import RunConfig

#Run configuration; set before importing Components..
RunConfig.iterations=1
RunConfig.runnumber=169838
RunConfig.beamtype='AA'
RunConfig.verbosity='0x78'
RunConfig.eventDir='./169838/raw/raw70'
RunConfig.hcdbDir='./169838/HCDB'
RunConfig.outDir='/tmp/out'
RunConfig.eventTriggerFile=RunConfig.outDir + "/etd.bin"

#some SPD ddls are missing (7,9,17,19), exclude them
RunConfig.DDLs['ISPD']=range(0, 7) + [8] + range(10,17) + [18]


#now import pre-defined components
import TPC
import ITS
import Global



def main():
    Global.GlobalTrigger.run(deps=True)

if __name__ == "__main__":
    main()
