#!/usr/bin/env python
import sys
import RunConfig
from Component import Component
import TPC
import ITS

ITSTracker=Component(
    componentid='ITSTracker',
    componentlibrary='libAliHLTITS.so',
    parents=[ITS.ClusterFinderSPD,
            ITS.ClusterFinderSDD,
            ITS.ClusterFinderSSD,
            TPC.CAGlobalMerger],
    outData=[('HLTTRACK','ITS','0xFFFFFFFF'), ('HLTTRACK','ITSO','0xFFFFFFFF')]
)

Vertexer=Component(
    componentid='GlobalVertexer',
    componentlibrary='libAliHLTGlobal.so',
    parents=[ITSTracker],
    outData=[('GLBVTXV0','HLT','0xFFFFFFFF'),('ESDVTXV0','HLT','0x00000000')]
)

EsdConverter=Component(
    componentid='GlobalEsdConverter',
    componentlibrary='libAliHLTGlobal.so',
    parents=[TPC.CAGlobalMerger,
             TPC.ClusterTransformation,
             ITSTracker,
             ITS.VertexerSPD,
             ITS.SAPTracker,
             Vertexer],
    #outData=[('ALIESDV0','HLT','0x00000000'),('ESDFRND0','HLT','0x00000000')]
    outData=[('ALIESDV0','HLT','0x00000000')]
)

FlatEsdConverter=Component(
    componentid='GlobalFlatEsdConverter',
    componentlibrary='libAliHLTGlobal.so',
    parents=[TPC.CAGlobalMerger,
             TPC.ClusterTransformation,
             ITSTracker,
             ITS.VertexerSPD,
             ITS.SAPTracker,
             Vertexer],
    outData=[('FLATESD0','HLT','0xFFFFFFFF'),('FLATFRND','HLT','0xFFFFFFFF')]
    #outData=[('FLATESD0','HLT','0xFFFFFFFF')]
)

TPCCalibManagerComponent=Component(
    componentid='TPCCalibManagerComponent',
    componentlibrary='libAliHLTGlobal.so',
    parents=[EsdConverter,
             ITS.ClusterFinderSPD],
    #componentargs="fTPCcalibConfigString='TPCCalib TPCAlign'"
    #componentargs="fTPCcalibConfigString=TPCCalib"
)

TPCCalibManagerComponentFlat=Component(
    componentid='TPCCalibManagerComponent',
    componentlibrary='libAliHLTGlobal.so',
    parents=[FlatEsdConverter,
             ITS.ClusterFinderSPD],
    #componentargs="fTPCcalibConfigString='TPCCalib TPCAlign'"
    #componentargs="fTPCcalibConfigString=TPCCalib"
)

ROOTFileWriterESD=Component(
    componentid='ROOTFileWriter',
    componentlibrary='libAliHLTUtil.so',
    componentargs='-datafile ' + RunConfig.outDir + '/ALIESDV0',
    parents=[EsdConverter],
    outData=[]
)

ROOTFileWriterVtx=Component(
    componentid='ROOTFileWriter',
    componentlibrary='libAliHLTUtil.so',
    componentargs='-datafile ' + RunConfig.outDir + '/VTX',
    parents=[Vertexer],
    outData=[]
)

BarrelMultiplicityTrigger=Component(
    componentid='BarrelMultiplicityTrigger',
    componentlibrary='libAliHLTTrigger.so',
    parents=[EsdConverter],
    outData=[('ROOTTOBJ','HLT','0xFFFFFFFF'),('HLTRDLST','HLT','0xFFFFFFFF')]
)

GlobalTrigger=Component(
    componentid='HLTGlobalTrigger',
    componentlibrary='libAliHLTTrigger.so',
    #componentargs='-process-all-events -monitoring=1 -include AliHLTMuonSpectroScalars.h',
    parents=[BarrelMultiplicityTrigger],
    outData=[('GLOBTRIG','HLT','0xFFFFFFFF'),('HLTRDLST','HLT','0x00000001')]
)

def runAll():
    ret=GlobalTrigger.run(deps=True)
    return ret


if __name__ == "__main__":
    ret=runAll()
    sys.exit(ret)
