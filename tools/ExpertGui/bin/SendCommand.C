
void SendCommand(
		const char* command = "getSysVShmInfo",
		const char* host = "dev0",
		Int_t port = 1111
	)
{
	gSystem->Load("libNet.so");
	gSystem->Load("libCommandDaemon.so");

	ThinDaemonClient client;
	client.ConnectToDaemon(host, port);
	client.SendCommand(command, 1000);
	cout << "Number of errors = " << client.ErrorCount() << endl;
	for (UInt_t i = 0; i < client.ErrorCount(); i++)
	{
		TString message, server;
		Int_t port;
		client.GetError(i, message, server, port);
		cerr << message << endl;
	}
	client.DisconnectAll();
}
