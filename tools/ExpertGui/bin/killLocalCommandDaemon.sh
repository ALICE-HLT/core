#!/bin/bash
PROGS=`pgrep CommandDaemon`
if test -n "$PROGS" ; then
	kill $PROGS
else
	echo >&2 "WARNING: No CommandDaemon was not found on $HOSTNAME"
fi
