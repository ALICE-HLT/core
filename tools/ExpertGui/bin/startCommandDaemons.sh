#!/bin/bash

###############################################################################
# Node listing

NODES=""
#for N in 00 02 04 06 08 10 12 14 16 ; do
#	NODES="$NODES feptpcai$N"
#	NODES="$NODES feptpcci$N"
#done
#for N in 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 ; do
#	NODES="$NODES feptpcao$N"
#done
#for N in 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 ; do
#	NODES="$NODES feptpcao$N"
#	NODES="$NODES feptpcco$N"
#done
#for ((N=0;N<=4;N++)) ; do
#	NODES="$NODES fepphos$N"
#done
#NODES="$NODES fepdimutrg"
#for ((N=1;N<=5;N++)) ; do
#	NODES="$NODES fepdimutrk$N"
#done
#NODES="$NODES feptrd00 feptrd04 feptrd08 feptrd10 feptrd14"
#NODES="$NODES fepspd0 fepspd1 fepspd2 fepspd3 fepspd4 fepssd0 fepssd1 fepssd2 fepssd3"
#NODES="$NODES fephmpid0 fephmpid1 fephmpid2 fephmpid3"
#NODES="$NODES fephltout0 fephltout1 fepfmdaccorde"
#NODES="$NODES fephltout0 fephltout1"
NODES="$NODES fepfmdaccorde feptriggerdet"

###############################################################################


TOPDIR=$HOME/ExpertGui/bin

trap "{ $TOPDIR/killCommandDaemons.sh; }" SIGINT SIGTERM

for NODE in $NODES ; do
	if test "quiet" = "$1" ; then
		ssh $NODE "cd $TOPDIR; export LD_LIBRARY_PATH=/afsuser/$USER/ExpertGui/lib:\$LD_LIBRARY_PATH; $TOPDIR/CommandDaemon -port 1111 -quiet" &
	else
		ssh $NODE "cd $TOPDIR; export LD_LIBRARY_PATH=/afsuser/$USER/ExpertGui/lib:\$LD_LIBRARY_PATH; $TOPDIR/CommandDaemon -port 1111" &
	fi
done

wait
echo "Finished."
