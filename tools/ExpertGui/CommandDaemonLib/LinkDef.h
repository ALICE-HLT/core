#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class SysVShmInfo+;
#pragma link C++ class SysVShmRecord+;
#pragma link C++ class HRORCInfo+;
#pragma link C++ class HeartBeatInfo+;
#pragma link C++ class DaemonServer+;
#pragma link C++ class ThinDaemonClient+;

#endif // __CINT__
