/*
 * Author: Artur Szostak <artursz@iafrica.com>
 */

#include "ShmInfo.hpp"
#include "Utils.hpp"

#include <stdlib.h>

#include "TSystem.h"
#include "TObjArray.h"
#include "TList.h"
#include "TThread.h"
#include "TObjString.h"
#include "TSocket.h"
#include "TMonitor.h"
#include "TServerSocket.h"
#include "TTimer.h"
#include "Riostream.h"


ClassImp(SysVShmInfo)


SysVShmInfo::SysVShmInfo() :
	TObject(),
	fHostname(""),
	fSegmentsAllocated(0),
	fPagesAllocated(0),
	fPagesResident(0),
	fPagesSwapped(0),
	fMaxSegments(0),
	fMinSegmentSize(0),
	fMaxSegmentSize(0),
	fTotalSize(0)
{
}

bool SysVShmInfo::Fill()
{
	fHostname = gSystem->HostName();

	TString result;
	int returnCode;
	const char* cmd = "ipcs -m -u";
	bool ok = ExecCommand(cmd, result, returnCode);
	if (not ok) return false;
	if (returnCode != 0)
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Command \"" << cmd << "\" did not succeed. (process return code = "
			<< returnCode << ")" << endl;
		cerr << result << endl;
		return false;
	}
	
	TObjArray* strings = result.Tokenize("\n");
	TObjString* strobj1 = (TObjString*)strings->At(1);
	TObjString* strobj2 = (TObjString*)strings->At(2);
	TObjString* strobj3 = (TObjString*)strings->At(3);
	TObjString* strobj4 = (TObjString*)strings->At(4);
	if (strobj1 == NULL or strobj2 == NULL or strobj3 == NULL or strobj4 == NULL)
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Got unexpected output for command \"" << cmd << "\"" << endl;
		cerr << result << endl;
		delete strings;
		return false;
	}
	TString str1 = strobj1->GetString();
	TString str2 = strobj2->GetString();
	TString str3 = strobj3->GetString();
	TString str4 = strobj4->GetString();
	delete strings;

	strings = str1.Tokenize(" ");
	strobj1 = (TObjString*)strings->At(2);
	if (strobj1 == NULL)
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Got unexpected output for command \"" << cmd << "\"" << endl;
		cerr << result << endl;
		delete strings;
		return false;
	}
	str1 = strobj1->GetString();
	delete strings;

	strings = str2.Tokenize(" ");
	strobj2 = (TObjString*)strings->At(2);
	if (strobj2 == NULL)
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Got unexpected output for command \"" << cmd << "\"" << endl;
		cerr << result << endl;
		delete strings;
		return false;
	}
	str2 = strobj2->GetString();
	delete strings;

	strings = str3.Tokenize(" ");
	strobj3 = (TObjString*)strings->At(2);
	if (strobj3 == NULL)
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Got unexpected output for command \"" << cmd << "\"" << endl;
		cerr << result << endl;
		delete strings;
		return false;
	}
	str3 = strobj3->GetString();
	delete strings;

	strings = str4.Tokenize(" ");
	strobj4 = (TObjString*)strings->At(2);
	if (strobj4 == NULL)
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Got unexpected output for command \"" << cmd << "\"" << endl;
		cerr << result << endl;
		delete strings;
		return false;
	}
	str4 = strobj4->GetString();
	delete strings;
	
	if (not str1.IsDigit() or not str2.IsDigit() or not str3.IsDigit() or not str4.IsDigit())
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Got unexpected output for command \"" << cmd << "\"" << endl;
		cerr << result << endl;
		return false;
	}

	fSegmentsAllocated = str1.Atoll();
	fPagesAllocated = str2.Atoll();
	fPagesResident = str3.Atoll();
	fPagesSwapped = str4.Atoll();

	const char* cmd2 = "ipcs -m -l";
	ok = ExecCommand(cmd2, result, returnCode);
	if (not ok) return false;
	if (returnCode != 0)
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Command \"" << cmd2 << "\" did not succeed. (process return code = "
			<< returnCode << ")" << endl;
		cerr << result << endl;
		return false;
	}
	
	strings = result.Tokenize("\n");
	strobj1 = (TObjString*)strings->At(1);
	strobj2 = (TObjString*)strings->At(2);
	strobj3 = (TObjString*)strings->At(3);
	strobj4 = (TObjString*)strings->At(4);
	if (strobj1 == NULL or strobj2 == NULL or strobj3 == NULL or strobj4 == NULL)
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Got unexpected output for command \"" << cmd2 << "\"" << endl;
		cerr << result << endl;
		delete strings;
		return false;
	}
	str1 = strobj1->GetString();
	str2 = strobj2->GetString();
	str3 = strobj3->GetString();
	str4 = strobj4->GetString();
	delete strings;

	strings = str1.Tokenize(" ");
	strobj1 = (TObjString*)strings->At(5);
	if (strobj1 == NULL)
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Got unexpected output for command \"" << cmd2 << "\"" << endl;
		cerr << result << endl;
		delete strings;
		return false;
	}
	str1 = strobj1->GetString();
	delete strings;

	strings = str2.Tokenize(" ");
	strobj2 = (TObjString*)strings->At(5);
	if (strobj2 == NULL)
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Got unexpected output for command \"" << cmd2 << "\"" << endl;
		cerr << result << endl;
		delete strings;
		return false;
	}
	str2 = strobj2->GetString();
	delete strings;

	strings = str3.Tokenize(" ");
	strobj3 = (TObjString*)strings->At(6);
	if (strobj3 == NULL)
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Got unexpected output for command \"" << cmd2 << "\"" << endl;
		cerr << result << endl;
		delete strings;
		return false;
	}
	str3 = strobj3->GetString();
	delete strings;

	strings = str4.Tokenize(" ");
	strobj4 = (TObjString*)strings->At(5);
	if (strobj4 == NULL)
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Got unexpected output for command \"" << cmd2 << "\"" << endl;
		cerr << result << endl;
		delete strings;
		return false;
	}
	str4 = strobj4->GetString();
	delete strings;
	
	if (not str1.IsDigit() or not str2.IsDigit() or not str3.IsDigit() or not str4.IsDigit())
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Got unexpected output for command \"" << cmd2 << "\"" << endl;
		cerr << result << endl;
		return false;
	}

	fMaxSegments = str1.Atoll();
	fMaxSegmentSize = str2.Atoll();
	fTotalSize = str3.Atoll();
	fMinSegmentSize = str4.Atoll();

	return true;
}


void SysVShmInfo::Print(Option_t* /*option*/) const
{
	cout << "Shared memory usage on " << fHostname << ":" << endl;
	cout << "       Segments allocated = " << fSegmentsAllocated << endl;
	cout << "          Pages allocated = " << fPagesAllocated << endl;
	cout << "           Pages resident = " << fPagesResident << endl;
	cout << "            Pages swapped = " << fPagesSwapped << endl;
	cout << " Maximum segments allowed = " << fMaxSegments << endl;
	cout << "     Minimum segment size = " << fMinSegmentSize << endl;
	cout << "     Maximum segment size = " << fMaxSegmentSize << endl;
	cout << "       Total size allowed = " << fTotalSize << endl;
}


// Only compare hostname.
Int_t SysVShmInfo::Compare(const TObject* obj) const
{
	if (obj->IsA() != SysVShmInfo::Class())
		return 0;

	const SysVShmInfo* rhs = static_cast<const SysVShmInfo*>(obj);
	if (this->fHostname < rhs->fHostname) return -1;
	if (this->fHostname > rhs->fHostname) return 1;
	return 0;
}

///////////////////////////////////////////////////////////////////////////////


/*

status
pci_device_rw -slot 2 5 0 0 0xC4 4 1
pci_device_rw -slot 3 4 0 0 0xC0 4 1

ipcs -m

       -t     time
       -p     pid
       -c     creator
       -l     limits
       -u     summary

 */

ClassImp(SysVShmRecord)
