/*
 * Author: Artur Szostak <artursz@iafrica.com>
 */

#include "Utils.hpp"

#include <stdio.h>

#include "TSystem.h"
#include "Riostream.h"


bool ExecCommand(const char* command, TString& output, int& returnCode, bool printError)
{
	FILE* pipe = gSystem->OpenPipe(command, "r");
	if (pipe == NULL)
	{
		if (printError)
		{
			cerr << "[DaemonServer on " << gSystem->HostName()
				<< "] ERROR: Could not open pipe for command = \""
				<< command << "\"" << endl;
		}
		return false;
	}
	output = "";
	char buf[1024*256];
	while (not feof(pipe))
	{
		size_t bytesread = fread(buf, sizeof(char), sizeof(buf)-1, pipe);
		if (ferror(pipe))
		{
			returnCode = gSystem->ClosePipe(pipe);
			if (printError)
			{
				cerr << "[DaemonServer on " << gSystem->HostName()
					<< "] ERROR: Error reading from pipe for command = \""
					<< command << "\"" << endl;
			}
			return false;
		}
		buf[bytesread] = '\0';
		output += buf;
	}
	returnCode = gSystem->ClosePipe(pipe);
	return true;
}


ClassImp(HeartBeatInfo)


void HeartBeatInfo::Fill(Int_t port)
{
	fHostname = gSystem->HostName();
	fPort = port;
	fTimeStamp = TTimeStamp();  // get current time.
}


void HeartBeatInfo::Print(Option_t* /*option*/) const
{
	cout << "Daemon on " << fHostname << ":" << fPort
		<< " is still alive at " << fTimeStamp.AsString() << endl;
}


Int_t HeartBeatInfo::Compare(const TObject* obj) const
{
	if (obj->IsA() != HeartBeatInfo::Class())
		return 0;

	const HeartBeatInfo* rhs = static_cast<const HeartBeatInfo*>(obj);
	if (this->fHostname < rhs->fHostname) return -1;
	if (this->fHostname > rhs->fHostname) return 1;
	if (this->fPort < rhs->fPort) return -1;
	if (this->fPort > rhs->fPort) return 1;
	if (this->fTimeStamp < rhs->fTimeStamp) return -1;
	if (this->fTimeStamp > rhs->fTimeStamp) return 1;
	return 0;
}
