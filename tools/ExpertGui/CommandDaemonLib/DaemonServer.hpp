#ifndef DAEMONSERVER_HPP
#define DAEMONSERVER_HPP
/*
 * Author: Artur Szostak <artursz@iafrica.com>
 */

#include "Rtypes.h"
#include "TObject.h"
#include "TString.h"
#include "TSocket.h"
#include "TMonitor.h"
#include "TServerSocket.h"
#include "TTimer.h"

class DaemonServer : public TObject
{
public:

	DaemonServer(Int_t port = 1111);
	bool Run();

	bool fPrintHeartBeat;
	bool fPrintTrace;

private:

	void HandleCommand(TString& command, TSocket* client);
	void Send(const char* message, TSocket* client);
	void Send(TString& message, TSocket* client);
	void Send(TObject* object, TSocket* client);
	void AddClient(TSocket* client);
	void RemoveClient(TSocket* client);

	Bool_t HandleTimer(TTimer* timer);  // Heart beat timer handler.

	TMonitor fMonitor;
	TServerSocket fListenSocket;
	TTimer fHeartBeatTimer;
	

	ClassDef(DaemonServer, 0)
};

#endif // DAEMONSERVER_HPP
