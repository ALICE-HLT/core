/*
 * Author: Artur Szostak <artursz@iafrica.com>
 */

#include "HRORCInfo.hpp"
#include "Utils.hpp"

#include <stdlib.h>
#include <errno.h>

#include "TSystem.h"
#include "TObjArray.h"
#include "TList.h"
#include "TThread.h"
#include "TObjString.h"
#include "TSocket.h"
#include "TMonitor.h"
#include "TServerSocket.h"
#include "TTimer.h"
#include "Riostream.h"


bool PCIReadAddress(Int_t bus, Int_t device, Int_t func, Int_t bar, UInt_t address, UInt_t& resultValue)
{
	char cmd[1024];
	sprintf(cmd, "pci_device_rw -slot %d %d %d %d 0x%X 4 1", bus, device, func, bar, address);

	TString result;
	int returnCode;
	bool ok = ExecCommand(cmd, result, returnCode);
	if (not ok) return false;
	if (returnCode != 0)
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Command \"" << cmd << "\" did not succeed. (process return code = "
			<< returnCode << ")" << endl;
		cerr << result << endl;
		return false;
	}
	
	TObjArray* strings = result.Tokenize("\n");
	if (strings->At(2) == NULL)
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Got unexpected output for command \"" << cmd << "\"" << endl;
		cerr << result << endl;
		delete strings;
		return false;
	}

	TString str = static_cast<TObjString*>(strings->At(2))->GetString();
	delete strings;
	
	strings = str.Tokenize(":");
	if (strings->At(1) == NULL)
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Got unexpected output for command \"" << cmd << "\"" << endl;
		cerr << "Line 3: " << str << endl;
		delete strings;
		return false;
	}

	str = static_cast<TObjString*>(strings->At(1))->GetString();
	delete strings;

	errno = 0;
	char* end = NULL;
	resultValue = strtoul(str.Data(), &end, 0);
	if (end == NULL or *end != '\0' or errno == ERANGE)
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Expected a 32bit hex integer for command \"" << cmd << "\" "
			<< " at line 3: " << str << endl;
		return false;
	}

	return true;
}


bool CollectHRORCInfo(TList& list)
{
	TString result;
	int returnCode;
	const char* cmd = "lspci | grep \"Xilinx Corporation: Unknown device dead\"";
	bool ok = ExecCommand(cmd, result, returnCode);
	if (not ok) return false;
	if (returnCode != 0)
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Command \"" << cmd << "\" did not succeed. (process return code = "
			<< returnCode << ")" << endl;
		cerr << result << endl;
		return false;
	}
	
	TObjArray* strings = result.Tokenize("\n");
	for (Int_t i = 0; i < strings->GetEntriesFast(); i++)
	{
		TObjString* strobj = (TObjString*)strings->At(i);
		TObjArray* strings2 = strobj->GetString().Tokenize(" ");
		if (strings2->At(0) == NULL)
		{
			cerr << "[DaemonServer on " << gSystem->HostName()
				<< "] ERROR: Got unexpected output for command \"" << cmd << "\"" << endl;
			cerr << "Line " << i << ": " << strobj->GetString() << endl;
			delete strings2;
			delete strings;
			return false;
		}

		TString slotstr = static_cast<TObjString*>(strings2->At(0))->GetString();
		delete strings2;

		strings2 = slotstr.Tokenize(":");
		if (strings2->At(1) == NULL or strings2->At(2) == NULL)
		{
			cerr << "[DaemonServer on " << gSystem->HostName()
				<< "] ERROR: Got unexpected output for command \"" << cmd << "\"" << endl;
			cerr << "Line " << i << ", fragment: " << slotstr << endl;
			delete strings2;
			delete strings;
			return false;
		}

		TString bus = static_cast<TObjString*>(strings2->At(1))->GetString();
		TString devicefunc = static_cast<TObjString*>(strings2->At(2))->GetString();
		delete strings2;

		strings2 = devicefunc.Tokenize(".");
		if (strings2->At(0) == NULL or strings2->At(1) == NULL)
		{
			cerr << "[DaemonServer on " << gSystem->HostName()
				<< "] ERROR: Got unexpected output for command \"" << cmd << "\"" << endl;
			cerr << "Line " << i << ", fragment: " << devicefunc << endl;
			delete strings2;
			delete strings;
			return false;
		}

		TString device = static_cast<TObjString*>(strings2->At(0))->GetString();
		TString func = static_cast<TObjString*>(strings2->At(1))->GetString();
		delete strings2;

		if (not bus.IsDigit() or not device.IsDigit() or not func.IsDigit())
		{
			cerr << "[DaemonServer on " << gSystem->HostName()
				<< "] ERROR: Got unexpected output for command \"" << cmd << "\"" << endl;
			cerr << "Expected digits but got: " << bus << " " << device << " " << func << endl;
			delete strings;
			return false;
		}
		
		HRORCInfo* rorc1 = new HRORCInfo;
		HRORCInfo* rorc2 = new HRORCInfo;
		if (not rorc1->Fill(bus.Atoi(), device.Atoi(), func.Atoi(), 0)
		    or not rorc2->Fill(bus.Atoi(), device.Atoi(), func.Atoi(), 1))
		{
			delete strings;
			return false;
		}

		list.Add(rorc1);
		list.Add(rorc2);
	}

	delete strings;
	return true;
}

///////////////////////////////////////////////////////////////////////////////

ClassImp(HRORCInfo)


HRORCInfo::HRORCInfo() :
	TObject(),
	fHostname(""),
	fBus(-1),
	fDevice(-1),
	fFunction(-1),
	fBAR(-1),
	fVersionAddress(0xFFFFFFFF),
	fVersion(0xFFFFFFFF),
	fEventCounter(0xFFFFFFFF),
	fDMAEventCounter(0xFFFFFFFF),
	fStatus(0xFFFFFFFF),
	fLinkStatus(0xFFFFFFFF)
{
}


bool HRORCInfo::Fill(Int_t bus, Int_t device, Int_t func, Int_t bar)
{
	fHostname = gSystem->HostName();

	fBus = bus;
	fDevice = device;
	fFunction = func;
	fBAR = bar;

	UInt_t versionReg1;
	if (not PCIReadAddress(bus, device, func, 0, 0x84, versionReg1)) return false;
	UInt_t versionReg2;
	if (not PCIReadAddress(bus, device, func, 0, 0x90, versionReg2)) return false;
	
	if (versionReg1 != 0 and versionReg2 == 0)
	{
		fVersionAddress = 0x84;
		fVersion = versionReg1;
	}
	else if (versionReg1 == 0 and versionReg2 != 0)
	{
		fVersionAddress = 0x90;
		fVersion = versionReg2;
	}
	else
	{
		fVersionAddress = 0xFFFFFFFF;
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Could not determine the correct version of the firmware on slot "
			<< bus << " " << device << " " << func << " " << bar
			<< ". Got the following values:" << endl;
		cerr << "On register 0x84: " << versionReg1 << endl;
		cerr << "On register 0x90: " << versionReg2 << endl;
		return false;
	}

	if (not PCIReadAddress(bus, device, func, bar, 0xCC, fEventCounter)) return false;
	if (not PCIReadAddress(bus, device, func, bar, 0x38, fDMAEventCounter)) return false;
	if (not PCIReadAddress(bus, device, func, bar, 0xC4, fStatus)) return false;
	if (not PCIReadAddress(bus, device, func, bar, 0xD0, fLinkStatus)) return false;

	return true;
}


void HRORCInfo::Print(Option_t* /*option*/) const
{
	cout << "HRORC information on " << fHostname << " slot "
		<< fBus << " " << fDevice << " " << fFunction << " " << fBAR << " :" << endl;
	cout << "             Version address = "; printf("0x%8.8X", fVersionAddress); cout << endl;
	cout << "                     Version = "; printf("0x%8.8X", fVersion); cout << endl;
	cout << "        Event counter (0xCC) = "; printf("0x%8.8X", fEventCounter); cout << endl;
	cout << "    DMS event counter (0x38) = "; printf("0x%8.8X", fDMAEventCounter); cout << endl;
	cout << "      Status register (0xC4) = "; printf("0x%8.8X", fStatus); cout << endl;
	cout << " Link status register (0xD0) = "; printf("0x%8.8X", fLinkStatus); cout << endl;

	if (InputRORC()) cout << "This is an input H-RORC" << endl;
	if (OutputRORC()) cout << "This is an output H-RORC" << endl;

	if (OutputRORC())
	{
		if (ChannelOpen())
			cout << "SIU channel is open." << endl;
		else
			cout << "SIU channel is closed." << endl;
	}

	if (LinkUp())
		cout << "DDL link is up." << endl;
	else
		cout << "DDL link is down." << endl;
}


// Only compare hostname and slot values.
Int_t HRORCInfo::Compare(const TObject* obj) const
{
	if (obj->IsA() != HRORCInfo::Class())
		return 0;

	const HRORCInfo* rhs = static_cast<const HRORCInfo*>(obj);
	if (this->fHostname < rhs->fHostname) return -1;
	if (this->fHostname > rhs->fHostname) return 1;
	if (this->fBus < rhs->fBus) return -1;
	if (this->fBus > rhs->fBus) return 1;
	if (this->fDevice < rhs->fDevice) return -1;
	if (this->fDevice > rhs->fDevice) return 1;
	if (this->fFunction < rhs->fFunction) return -1;
	if (this->fFunction > rhs->fFunction) return 1;
	if (this->fBAR < rhs->fBAR) return -1;
	if (this->fBAR > rhs->fBAR) return 1;
	return 0;
}
