/*
 * Author: Artur Szostak <artursz@iafrica.com>
 */

#include "DaemonServer.hpp"
#include "ShmInfo.hpp"
#include "HRORCInfo.hpp"
#include "Utils.hpp"

#include "TSystem.h"
#include "TObjArray.h"
#include "TList.h"
#include "TThread.h"
#include "TObjString.h"
#include "Riostream.h"


ClassImp(DaemonServer)


DaemonServer::DaemonServer(Int_t port) :
	fPrintHeartBeat(true),
	fPrintTrace(true),
	fMonitor(),
	fListenSocket(port, kTRUE),
	fHeartBeatTimer(this, 2000)
{
	if (not fListenSocket.IsValid())
	{
		cerr << "[DaemonServer on " << gSystem->HostName()
			<< "] ERROR: Could not create socket on port = "
			<< port << endl;
		return;
	}
}


bool DaemonServer::Run()
{
	if (not fListenSocket.IsValid()) return false; // Error already printed in constructor.

	if (fPrintTrace)
	{
		cout << "[DaemonServer on " << gSystem->HostName()
			<< "] Started on port: " << fListenSocket.GetLocalPort() << endl;
	}

	fMonitor.Add(&fListenSocket);
	fHeartBeatTimer.Start();

	bool quit = false;
	do
	{
		TSocket* socket = fMonitor.Select();
		if (socket == NULL)
		{
			cerr << "[DaemonServer on " << gSystem->HostName()
				<< "] ERROR: Could not accept a new connection." << endl;
			continue;
		}

		if (socket == (TSocket*)-1) // timeout
		{
			continue;
		}

		// Handle server socket.
		if (socket == &fListenSocket)
		{
			socket = fListenSocket.Accept();
			AddClient(socket);
			continue;
		}

		// The following handles normal client connections.
		char buf[1024*4];
		Int_t bytesread = socket->Recv(buf, sizeof(buf));
		if (bytesread <= 0)
		{
			if (bytesread < 0)
			{
				cerr << "[DaemonServer on " << gSystem->HostName()
					<< "] ERROR: Could not read from socket connected to "
					<< socket->GetInetAddress().GetHostName()
					<< " (code = " << bytesread << ")" << endl;
			}
			RemoveClient(socket);
		}
		else // bytesread > 0
		{
			TString command = buf;
			if (fPrintTrace)
			{
				cout << "[DaemonServer on " << gSystem->HostName()
					<< "] Received command = \"" << command
					<< "\"" << endl;
			}
			if (command == "quit")
				quit = true;
			else
				HandleCommand(command, socket);
		}
	}
	while (not quit);

	if (fPrintTrace)
	{
		cout << "[DaemonServer on " << gSystem->HostName()
			<< "] Terminating now..." << endl;
	}

	// Send a termination message to all the clients.
	TList* socketlist = fMonitor.GetListOfActives();
	TListIter next(socketlist);
	TObject* obj = NULL;
	while ((obj = next()) != NULL)
	{
		TSocket* socket = static_cast<TSocket*>(obj);
		if (socket == &fListenSocket) continue;  // Skip server socket.
		socket->Send("Terminating");
		RemoveClient(socket);
	}
	delete socketlist;

	return true;
}


void DaemonServer::HandleCommand(TString& command, TSocket* client)
{
	if (command == "getSysVShmInfo")
	{
		SysVShmInfo info;
		if (not info.Fill())
		{
			Send("ERROR: Problem while fetching SysV shared memory information.", client);
			return;
		}
		Send(&info, client);
		return;
	}
	if (command == "getHRORCInfo")
	{
		TList rorcs;
		if (not CollectHRORCInfo(rorcs))
		{
			Send("ERROR: Problem while fetching H-RORC information.", client);
			return;
		}
		Send(&rorcs, client);
		return;
	}

	TString msg = "ERROR: Unknown command \"";
	msg += command;
	msg += "\".";
	Send(msg.Data(), client);
}


void DaemonServer::Send(const char* message, TSocket* client)
{
	Int_t byteswritten = client->Send(message);
	if (byteswritten <= 0)
	{
		if (byteswritten < 0)
		{
			cerr << "[DaemonServer on " << gSystem->HostName()
				<< "] ERROR: Could not write to socket connected to "
				<< client->GetInetAddress().GetHostName()
				<< ". Removing socket. (error code = "
				<< byteswritten << ")" << endl;
		}
		RemoveClient(client);
	}
}


void DaemonServer::Send(TString& message, TSocket* client)
{
	Int_t byteswritten = client->Send(message);
	if (byteswritten <= 0)
	{
		if (byteswritten < 0)
		{
			cerr << "[DaemonServer on " << gSystem->HostName()
				<< "] ERROR: Could not write to socket connected to "
				<< client->GetInetAddress().GetHostName()
				<< ". Removing socket. (error code = "
				<< byteswritten << ")" << endl;
		}
		RemoveClient(client);
	}
}


void DaemonServer::Send(TObject* object, TSocket* client)
{
	Int_t byteswritten = client->SendObject(object);
	if (byteswritten <= 0)
	{
		if (byteswritten < 0)
		{
			cerr << "[DaemonServer on " << gSystem->HostName()
				<< "] ERROR: Could not write to socket connected to "
				<< client->GetInetAddress().GetHostName()
				<< ". Removing socket. (error code = "
				<< byteswritten << ")" << endl;
		}
		RemoveClient(client);
	}
}


void DaemonServer::AddClient(TSocket* client)
{
	if (fPrintTrace)
	{
		cout << "[DaemonServer on " << gSystem->HostName()
			<< "] New client connection from "
			<< client->GetInetAddress().GetHostName() << ":"
			<< client->GetInetAddress().GetPort() << endl;
	}
	client->SetOption(kNoBlock, 1);
	fMonitor.Add(client);
}


void DaemonServer::RemoveClient(TSocket* client)
{
	if (fPrintTrace)
	{
		cout << "[DaemonServer on " << gSystem->HostName()
			<< "] Connection to client "
			<< client->GetInetAddress().GetHostName()
			<< ":" << client->GetInetAddress().GetPort()
			<< " was closed." << endl;
	}
	fMonitor.Remove(client);
	delete client;
}


Bool_t DaemonServer::HandleTimer(TTimer* /*timer*/)
{
	// Print a heart beat message and send timestamp to clients.
	HeartBeatInfo heartBeat;
	heartBeat.Fill(fListenSocket.GetLocalPort());

	TString msg = gSystem->HostName();
	msg += " is still alive at ";
	msg += heartBeat.fTimeStamp.AsString();
	msg += ", with ";
	msg += fMonitor.GetActive() - 1;  // Less one because it is the server socket.
	msg += " active clients.";

	if (fPrintHeartBeat)
	{
		cout << msg << endl;
	}

	TList* socketlist = fMonitor.GetListOfActives();
	TListIter next(socketlist);
	TObject* obj = NULL;
	while ((obj = next()) != NULL)
	{
		TSocket* socket = static_cast<TSocket*>(obj);
		if (socket == &fListenSocket) continue;  // Skip server socket.
		Int_t written = socket->SendObject(&heartBeat);
		if (written <= 0) RemoveClient(socket);
	}
	delete socketlist;

	return kTRUE;
}
