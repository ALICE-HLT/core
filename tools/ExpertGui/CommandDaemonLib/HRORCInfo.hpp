#ifndef HRORCINFO_HPP
#define HRORCINFO_HPP
/*
 * Author: Artur Szostak <artursz@iafrica.com>
 */

#include "Rtypes.h"
#include "TObject.h"
#include "TString.h"
#include "TTimeStamp.h"
#include "TList.h"


bool CollectHRORCInfo(TList& list);

class HRORCInfo : public TObject
{
public:

	HRORCInfo();
	bool Fill(Int_t bus, Int_t device, Int_t func, Int_t bar);
	virtual void Print(Option_t* option = "") const;
	virtual Bool_t IsSortable() const { return kTRUE; }
	virtual Int_t Compare(const TObject* obj) const;

	bool InputRORC() const { return fVersionAddress == 0x90; }
	bool OutputRORC() const { return fVersionAddress == 0x84; }
	bool ChannelOpen() const { return ((fStatus >> 31) & 0x1) == 1; }
	bool LinkUp() const { return ((fLinkStatus >> 31) & 0x1) == 1; }

	TString fHostname;
	Int_t fBus;
	Int_t fDevice;
	Int_t fFunction;
	Int_t fBAR;

	UInt_t fVersionAddress;
	UInt_t fVersion;
	UInt_t fEventCounter;
	UInt_t fDMAEventCounter;
	UInt_t fStatus;
	UInt_t fLinkStatus;
	
	// 0x30 - DMA fifo register
	// bit 31 - DMA fifo full
	// bit 30 - DMA fifo programmable empty (watermark)
	// bit 29 - DMA fifo empty
	// bits 9..0 - # of 64bit words in DMA fifo

	ClassDef(HRORCInfo, 1)
};

#endif // HRORCINFO_HPP
