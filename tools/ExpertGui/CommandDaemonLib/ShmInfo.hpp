#ifndef SHMINFO_HPP
#define SHMINFO_HPP
/*
 * Author: Artur Szostak <artursz@iafrica.com>
 */

#include "Rtypes.h"
#include "TObject.h"
#include "TString.h"
#include "TTimeStamp.h"

class SysVShmInfo : public TObject
{
public:

	SysVShmInfo();
	bool Fill();
	virtual void Print(Option_t* option = "") const;
	virtual Bool_t IsSortable() const { return kTRUE; }
	virtual Int_t Compare(const TObject* obj) const;

	TString fHostname;
	ULong64_t fSegmentsAllocated;
	ULong64_t fPagesAllocated;
	ULong64_t fPagesResident;
	ULong64_t fPagesSwapped;
	ULong64_t fMaxSegments;
	ULong64_t fMinSegmentSize;  // bytes
	ULong64_t fMaxSegmentSize;  // kbytes
	ULong64_t fTotalSize;       // total size in pages

	ClassDef(SysVShmInfo, 1)
};


class SysVShmRecord : public TObject
{
public:

	UInt_t fId;
	TString fCUid;  // owner
	TString fCGid;
	//TString fLUid;  // owner
	//TString fLGid;
	Int_t fCpid;
	Int_t fLpid;
	TTimeStamp fAttached;
	TTimeStamp fDetached;
	TTimeStamp fChanged;

	ClassDef(SysVShmRecord, 1)
};

#endif // SHMINFO_HPP
