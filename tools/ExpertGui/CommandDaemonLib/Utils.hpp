#ifndef UTILS_HPP
#define UTILS_HPP
/*
 * Author: Artur Szostak <artursz@iafrica.com>
 */

#include "Rtypes.h"
#include "TObject.h"
#include "TString.h"
#include "TTimeStamp.h"


bool ExecCommand(const char* command, TString& output, int& returnCode, bool printError = true);


class HeartBeatInfo : public TObject
{
public:

	HeartBeatInfo() : TObject(), fHostname(""), fPort(-1) {};
	void Fill(Int_t port = -1);
	virtual void Print(Option_t* option = "") const;
	virtual Bool_t IsSortable() const { return kTRUE; }
	virtual Int_t Compare(const TObject* obj) const;

	TString fHostname;
	Int_t fPort;
	TTimeStamp fTimeStamp;

	ClassDef(HeartBeatInfo, 1)
};


#endif // UTILS_HPP
