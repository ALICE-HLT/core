#ifndef THINDAEMONCLIENT_HPP
#define THINDAEMONCLIENT_HPP
/*
 * Author: Artur Szostak <artursz@iafrica.com>
 */

#include "Rtypes.h"
#include "TObject.h"
#include "TQObject.h"
#include "TString.h"
#include "TSocket.h"
#include "TMonitor.h"
#include "TServerSocket.h"
#include "TTimer.h"
#include "TList.h"
#include "ShmInfo.hpp"
#include "HRORCInfo.hpp"
#include "Utils.hpp"
#include <vector>

class TMessage;


class ThinDaemonClient : public TObject
{
public:

	ThinDaemonClient();
	virtual ~ThinDaemonClient();

	bool ConnectToDaemon(const char* hostname, Int_t port = 1111);

	bool ConnectToDaemon(const TString& hostname, Int_t port = 1111)
	{
		return ConnectToDaemon(hostname.Data(), port);
	}

	bool DisconnectFromDaemon(const char* hostname, Int_t port = 1111);

	bool DisconnectFromDaemon(const TString& hostname, Int_t port = 1111)
	{
		return DisconnectFromDaemon(hostname.Data(), port);
	}

	void DisconnectAll();

	bool IsConnected(const char* hostname, Int_t port = 1111) const;

	bool IsConnected(const TString& hostname, Int_t port = 1111) const
	{
		return IsConnected(hostname.Data(), port);
	}

	// Returns the number of daemons we are connected to.
	UInt_t DaemonCount() const { return fMonitor.GetActive(); }

	// Returns the number of heart beats received.
	UInt_t ListenForHeartBeat();

	// Sends the command to all servers and waits for replys.
	// The network communication is performed asynchronously within this method call
	// but it will not return until the timeout period specified.
	// If timeout is negative then we only return once all servers have replyed
	// or the connections are broken.
	// The timeout is specified in milli-seconds. (default = 5 seconds)
	bool SendCommand(const char* command, Long_t timeout = 5000);

	bool SendCommand(const TString& command, Long_t timeout = 5000)
	{
		return SendCommand(command.Data(), timeout);
	}

	UInt_t ErrorCount() const { return UInt_t(fErrors.size()); };
	bool GetError(UInt_t i, TString& message, TString& server, Int_t& port) const;

	UInt_t HeartBeatResultCount() const { return UInt_t(fHeartBeats.GetSize()); }
	const HeartBeatInfo* GetHeartBeatResult(UInt_t i) const
	{
		return static_cast<const HeartBeatInfo*>(fHeartBeats.At(i));
	}

	UInt_t SysVShmResultCount() const { return UInt_t(fSysVShmResults.GetSize()); }
	const SysVShmInfo* GetSysVShmResult(UInt_t i) const
	{
		return static_cast<const SysVShmInfo*>(fSysVShmResults.At(i));
	}

	UInt_t HRORCInfoResultCount() const { return UInt_t(fHRORCInfoResults.GetSize()); }
	const HRORCInfo* GetHRORCInfoResult(UInt_t i) const
	{
		return static_cast<const HRORCInfo*>(fHRORCInfoResults.At(i));
	}


	bool fPrintTrace;

private:

	bool HandleMessage(TMessage* message, TSocket* server, const char* command);
	bool HandleObject(TObject* object, TSocket* server, const char* command);

	void ClearArrays();
	Int_t Send(const char* message, TSocket* server);
	Int_t ReceiveFrom(TSocket* server, const char* command);
	void AddServer(TSocket* server);
	void RemoveServer(TSocket* server);

	TMonitor fMonitor;
	TList fRemoveList;
	TList fHeartBeats;
	TList fSysVShmResults;
	TList fHRORCInfoResults;

	struct ErrorInfo
	{
		TString fMessage;
		TString fServer;
		Int_t fPort;
	};

	std::vector<ErrorInfo> fErrors;
	
	ClassDef(ThinDaemonClient, 0)
};

#endif // THINDAEMONCLIENT_HPP
