/*
 * Author: Artur Szostak <artursz@iafrica.com>
 */

#include "ThinDaemonClient.hpp"
#include "ShmInfo.hpp"
#include "HRORCInfo.hpp"
#include "Utils.hpp"

#include "TSystem.h"
#include "TMessage.h"
#include "TObjArray.h"
#include "TThread.h"
#include "TObjString.h"
#include "Riostream.h"


ClassImp(ThinDaemonClient)


ThinDaemonClient::ThinDaemonClient() :
	fPrintTrace(true),
	fMonitor(),
	fRemoveList(),
	fHeartBeats(),
	fSysVShmResults(),
	fHRORCInfoResults(),
	fErrors()
{
	fHeartBeats.SetOwner(kTRUE);
	fSysVShmResults.SetOwner(kTRUE);
	fHRORCInfoResults.SetOwner(kTRUE);
}


ThinDaemonClient::~ThinDaemonClient()
{
	DisconnectAll();
}


bool ThinDaemonClient::ConnectToDaemon(const char* hostname, Int_t port)
{
	// First check if we are already connected. No need to connect more than once.
	if (IsConnected(hostname, port))
	{
		if (fPrintTrace)
		{
			cerr << "[ThinDaemonClient on " << gSystem->HostName()
				<< "] WARNING: Already connected to " << hostname << ":" << port << endl;
		}
		return false;
	}

	// Have not connected to server yet so try connect now.
	TSocket* socket = new TSocket(hostname, port);
	if (socket == NULL)
	{
		if (fPrintTrace)
		{
			cerr << "[ThinDaemonClient on " << gSystem->HostName()
				<< "] ERROR: Could not connect to " << hostname << ":" << port << endl;
		}
		return false;
	}
	if (not socket->IsValid())
	{
		if (fPrintTrace)
		{
			cerr << "[ThinDaemonClient on " << gSystem->HostName()
				<< "] ERROR: Could not connect to " << hostname << ":" << port << endl;
		}
		return false;
	}
	AddServer(socket);
	return true;
}


bool ThinDaemonClient::DisconnectFromDaemon(const char* hostname, Int_t port)
{
	TInetAddress addr = gSystem->GetHostByName(hostname);
	TList* socketlist = fMonitor.GetListOfActives();
	TListIter next(socketlist);
	TObject* obj = NULL;
	bool result = false;
	while ((obj = next()) != NULL)
	{
		TSocket* socket = static_cast<TSocket*>(obj);
		if (strcmp(socket->GetInetAddress().GetHostName(), addr.GetHostName()) == 0
			and socket->GetInetAddress().GetPort() == port)
		{
			RemoveServer(socket);
			result = true;
		}
	}
	delete socketlist;
	if (result == false and fPrintTrace)
	{
		cerr << "[ThinDaemonClient on " << gSystem->HostName()
			<< "] ERROR: Could not find socket connect to " << hostname << ":"
			<< port << " to disconnect." << endl;
	}
	return result;
}


void ThinDaemonClient::DisconnectAll()
{
	TList* socketlist = fMonitor.GetListOfActives();
	TListIter next(socketlist);
	TObject* obj = NULL;
	while ((obj = next()) != NULL)
	{
		TSocket* socket = static_cast<TSocket*>(obj);
		RemoveServer(socket);
	}
	delete socketlist;
}


bool ThinDaemonClient::IsConnected(const char* hostname, Int_t port) const
{
	TInetAddress addr = gSystem->GetHostByName(hostname);
	TList* socketlist = fMonitor.GetListOfActives();
	TListIter next(socketlist);
	TObject* obj = NULL;
	while ((obj = next()) != NULL)
	{
		TSocket* socket = static_cast<TSocket*>(obj);
		if (strcmp(socket->GetInetAddress().GetHostName(), addr.GetHostName()) == 0
			and socket->GetInetAddress().GetPort() == port)
		{
			delete socketlist;
			return true;
		}
	}
	delete socketlist;
	return false;
}


UInt_t ThinDaemonClient::ListenForHeartBeat()
{
	ClearArrays();

	TList readReady;
	TList* socketList = fMonitor.GetListOfActives();

	// Reset the interest to read for all sockets.
	TSocket* socket = NULL;
	TListIter nextSocket(socketList);
	while ((socket = static_cast<TSocket*>(nextSocket())) != NULL)
	{
		fMonitor.SetInterest(socket, TMonitor::kRead);
	};

	// Now handle the communication
	Int_t result = fMonitor.Select(&readReady, NULL, 1);
	if (result < 0)
	{
		ErrorInfo error;
		error.fMessage = "ERROR: Select command failed. (error code = ";
		error.fMessage += result;
		error.fMessage += ")";
		error.fServer = gSystem->HostName();
		error.fPort = 0;

		if (fPrintTrace)
		{
			cerr << "[ThinDaemonClient on " << gSystem->HostName()
				<< "] " << error.fMessage << endl;
		}
		delete socketList;
		return 0;
	}

	// Handle responces from servers.
	nextSocket = TListIter(&readReady);
	while ((socket = static_cast<TSocket*>(nextSocket())) != NULL)
	{
		result = ReceiveFrom(socket, "");
		// -4 means the socket would have blocked and -100 that we read an empty message.
		// -200 means we got a message but not the expected one.
		if (result <= 0 and result != -4 and result != -100 and result != -200)
		{
			// Error on socket or connection reset so remove it from
			// the fMonitor list and delete.
			RemoveServer(socket);
		}
	}

	// Finally go through the removal list and remove the sockets.
	// This was filled by HandleMessage for daemons that terminated gracefully.
	nextSocket = TListIter(&fRemoveList);
	while ((socket = static_cast<TSocket*>(nextSocket())) != NULL)
	{
		RemoveServer(socket);
	}
	
	delete socketList;
	return HeartBeatResultCount();
}


bool ThinDaemonClient::SendCommand(const char* command, Long_t timeout)
{
	ClearArrays();

	if (timeout < 0) timeout = 0x7FFFFFFF;
	double start = double(TTimeStamp());
	double now = start;

	TList readReady, writeReady;
	TList* socketsToWrite = fMonitor.GetListOfActives();
	TList socketsToRead;
	socketsToRead.AddAll(socketsToWrite);

	// Reset the interest to read and write for all sockets.
	TSocket* socket = NULL;
	TListIter nextSocket(socketsToWrite);
	while ((socket = static_cast<TSocket*>(nextSocket())) != NULL)
	{
		fMonitor.SetInterest(socket, TMonitor::kRead | TMonitor::kWrite);
	};

	// Now handle the communication.
	while ((socketsToWrite->GetSize() > 0 or socketsToRead.GetSize() > 0)
		and (now - start) * 1000 < timeout
	      )
	{
		Long_t timeoutVal = timeout - Long_t((now - start) * 1000);

		// Note: Select clears / resets readReady and writeReady
		Int_t result = fMonitor.Select(&readReady, &writeReady, timeoutVal);
		now = double(TTimeStamp());

		if (result < 0)
		{
			ErrorInfo error;
			error.fMessage = "ERROR: Select command failed. (error code = ";
			error.fMessage += result;
			error.fMessage += ")";
			error.fServer = gSystem->HostName();
			error.fPort = 0;
	
			if (fPrintTrace)
			{
				cerr << "[ThinDaemonClient on " << gSystem->HostName()
					<< "] " << error.fMessage << endl;
			}
			break;
		}

		// Handle sockets that are ready for writing.
		nextSocket = TListIter(&writeReady);
		while ((socket = static_cast<TSocket*>(nextSocket())) != NULL)
		{
			fMonitor.SetInterest(socket, TMonitor::kRead);

			socketsToWrite->Remove(socket);
			result = Send(command, socket);
			if (result <= 0 and result != -4)  // -4 means the socket would have blocked
			{
				// Must also remove from the ready to read lists since we delete the socket.
				socketsToRead.Remove(socket);
				readReady.Remove(socket);
				RemoveServer(socket);
			}
		}

		// Handle responces from servers.
		nextSocket = TListIter(&readReady);
		while ((socket = static_cast<TSocket*>(nextSocket())) != NULL)
		{
			result = ReceiveFrom(socket, command);
			// -4 means the socket would have blocked and -100 that we read an empty message.
			// -200 means we got a message but not the expected one.
			if (result <= 0 and result != -4 and result != -100 and result != -200)
			{
				// Error on socket or connection reset so remove it from
				// read ready, write ready and fMonitor lists and delete.
				socketsToWrite->Remove(socket);
				socketsToRead.Remove(socket);
				RemoveServer(socket);
			}

			if (result > 0)
			{
				socketsToRead.Remove(socket);
			}
		}
	}

	// Handle sockets that have not responded or we are still waiting to write to.
	nextSocket = TListIter(socketsToWrite);
	while ((socket = static_cast<TSocket*>(nextSocket())) != NULL)
	{
		ErrorInfo error;
		error.fServer = socket->GetInetAddress().GetHostName();
		error.fPort = socket->GetInetAddress().GetPort();
		error.fMessage = "ERROR: Waiting too long to write command to ";
		error.fMessage += error.fServer;
		error.fMessage += ":";
		error.fMessage += error.fPort;
		fErrors.push_back(error);

		if (fPrintTrace)
		{
			cerr << "[ThinDaemonClient on " << gSystem->HostName()
				<< "] " << error.fMessage << endl;
		}
	}
	nextSocket = TListIter(&socketsToRead);
	while ((socket = static_cast<TSocket*>(nextSocket())) != NULL)
	{
		ErrorInfo error;
		error.fServer = socket->GetInetAddress().GetHostName();
		error.fPort = socket->GetInetAddress().GetPort();
		error.fMessage = "ERROR: No response received from ";
		error.fMessage += error.fServer;
		error.fMessage += ":";
		error.fMessage += error.fPort;
		fErrors.push_back(error);

		if (fPrintTrace)
		{
			cerr << "[ThinDaemonClient on " << gSystem->HostName()
				<< "] " << error.fMessage << endl;
		}
	}

	// Finally go through the removal list and remove the sockets.
	// This was filled by HandleMessage for daemons that terminated gracefully.
	nextSocket = TListIter(&fRemoveList);
	while ((socket = static_cast<TSocket*>(nextSocket())) != NULL)
	{
		RemoveServer(socket);
	}
	
	delete socketsToWrite;
	return ErrorCount() == 0;
}


bool ThinDaemonClient::GetError(UInt_t i, TString& message, TString& server, Int_t& port) const
{
	if (i >= ErrorCount())
	{
		if (fPrintTrace)
		{
			cerr << "[ThinDaemonClient on " << gSystem->HostName()
				<< "] ERROR: Trying to access error message #" << i
				<< " which does not exist. Error count = "
				<< ErrorCount() << ":" << endl;
		}
		return false;
	}

	message = fErrors[i].fMessage;
	server = fErrors[i].fServer;
	port = fErrors[i].fPort;

	return true;
}


// Returns false if the message is not of the expected type.
bool ThinDaemonClient::HandleMessage(TMessage* message, TSocket* server, const char* command)
{
	bool result = false;
	switch ( message->What() )
	{
	case kMESS_STRING:
		{
		char str[1024*256];
		message->ReadString(str, sizeof(str));
		if (TString(str) == "Terminating" and TString(command) == "quit")
		{
			fRemoveList.Add(server);
			result = true;
		}
		else if (TString(str).Contains("ERROR", TString::kIgnoreCase))
		{
			ErrorInfo error;
			error.fServer = server->GetInetAddress().GetHostName();
			error.fPort = server->GetInetAddress().GetPort();
			error.fMessage = TString(str);
			fErrors.push_back(error);

			// Not really what we expected but return true because we
			// will probably not get anything else.
			result = true;
		}
		else
		{
			ErrorInfo error;
			error.fServer = server->GetInetAddress().GetHostName();
			error.fPort = server->GetInetAddress().GetPort();
			error.fMessage = "WARNING: Received unexpected message \"";
			error.fMessage += str;
			error.fMessage += "\".";
			fErrors.push_back(error);

			if (fPrintTrace)
			{
				cerr << "[ThinDaemonClient on " << gSystem->HostName()
					<< "] " << error.fMessage << endl;
			}
		}
		}
		break;

	case kMESS_OBJECT:
		{
		TObject* obj = static_cast<TObject*>( message->ReadObjectAny(TObject::Class()) );
		if (obj != NULL)
		{
			result = HandleObject(obj, server, command);
		}
		else
		{
			ErrorInfo error;
			error.fServer = server->GetInetAddress().GetHostName();
			error.fPort = server->GetInetAddress().GetPort();
			error.fMessage = "ERROR: Received a message with a NULL object";
			fErrors.push_back(error);

			if (fPrintTrace)
			{
				cerr << "[ThinDaemonClient on " << gSystem->HostName()
					<< "] " << error.fMessage << endl;
			}
		}
		}
		break;

	default:
		cerr << "[ThinDaemonClient on " << gSystem->HostName()
			<< "] WARNING: Received a message with unknown type = " << message->What() << endl;
	}

	return result;
}


// Returns false if we did not get an object of the expected type for the
// given command.
bool ThinDaemonClient::HandleObject(TObject* object, TSocket* server, const char* command)
{
	if (object->IsA() == HeartBeatInfo::Class())
	{
		fHeartBeats.Add(object);
		return false;
	}
	if (TString(command) == "getSysVShmInfo" and object->IsA() == SysVShmInfo::Class())
	{
		fSysVShmResults.Add(object);
		return true;
	}
	if (TString(command) == "getHRORCInfo" and object->IsA() == TList::Class())
	{
		TObject* obj = NULL;
		TList* list = static_cast<TList*>(object);
		list->SetOwner(kFALSE);
		TListIter next(list);

		bool result = true;

		while ((obj = next()) != NULL)
		{
			if (obj->IsA() != HRORCInfo::Class())
			{
				ErrorInfo error;
				error.fServer = server->GetInetAddress().GetHostName();
				error.fPort = server->GetInetAddress().GetPort();
				error.fMessage = "WARNING: Received a HRORC info message with an unknown object class ";
				error.fMessage += obj->ClassName();
				fErrors.push_back(error);
			
				if (fPrintTrace)
				{
					cerr << "[ThinDaemonClient on " << gSystem->HostName()
						<< "] " << error.fMessage << endl;
					object->Print();
				}

				delete obj;
				result = false;
			}
			else
			{
				fHRORCInfoResults.Add(obj);
			}
		};

		delete object;
		return result;
	}

	ErrorInfo error;
	error.fServer = server->GetInetAddress().GetHostName();
	error.fPort = server->GetInetAddress().GetPort();
	error.fMessage = "WARNING: Received a message with an unknown object class ";
	error.fMessage += object->ClassName();
	fErrors.push_back(error);

	if (fPrintTrace)
	{
		cerr << "[ThinDaemonClient on " << gSystem->HostName()
			<< "] " << error.fMessage << endl;
		object->Print();
	}
	return false;
}


void ThinDaemonClient::ClearArrays()
{
	fErrors.clear();
	fRemoveList.Clear();
	fHeartBeats.Clear();
	fSysVShmResults.Clear();
	fHRORCInfoResults.Clear();
}


Int_t ThinDaemonClient::Send(const char* message, TSocket* server)
{
	Int_t byteswritten = server->Send(message);
	if (byteswritten <= 0 and byteswritten != -4)  // -4 if socket would block.
	{
		ErrorInfo error;
		error.fServer = server->GetInetAddress().GetHostName();
		error.fPort = server->GetInetAddress().GetPort();

		if (byteswritten < 0)
		{
			if (fPrintTrace)
			{
				cerr << "[ThinDaemonClient on " << gSystem->HostName()
					<< "] ERROR: Could not write to socket connected to "
					<< server->GetInetAddress().GetHostName()
					<< " (error code = " << byteswritten
					<< ")." << endl;
			}
			
			error.fMessage = "ERROR: Could not write to socket connected to ";
		}
		else
		{
			error.fMessage = "ERROR: Connection closed by server ";
		}
		
		error.fMessage += error.fServer;
		error.fMessage += ":";
		error.fMessage += error.fPort;
		error.fMessage += " (error code = ";
		error.fMessage += byteswritten;
		error.fMessage += ")";
		fErrors.push_back(error);
	}

	return byteswritten;
}


// Returns -100 if read an empty message. < 0 if read failed. 0 if connection dropped.
// And number of bytes read on success.
Int_t ThinDaemonClient::ReceiveFrom(TSocket* server, const char* command)
{
	TMessage* message = NULL;
	Int_t bytesread = server->Recv(message);
	if (bytesread > 0 and message != NULL)
	{
		if (not HandleMessage(message, server, command))
		{
			bytesread = -200;
		}
	}
	else if (bytesread <= 0 and bytesread != -4)  // -4 if socket would block.
	{
		ErrorInfo error;
		error.fServer = server->GetInetAddress().GetHostName();
		error.fPort = server->GetInetAddress().GetPort();

		if (bytesread < 0)
		{
			if (fPrintTrace)
			{
				cerr << "[ThinDaemonClient on " << gSystem->HostName()
					<< "] ERROR: Could not read from socket connected to "
					<< server->GetInetAddress().GetHostName()
					<< " (error code = " << bytesread
					<< ")." << endl;
			}
			
			error.fMessage = "ERROR: Could not read from socket connected to ";
		}
		else
		{
			error.fMessage = "ERROR: Connection closed by server ";
		}
		
		error.fMessage += error.fServer;
		error.fMessage += ":";
		error.fMessage += error.fPort;
		error.fMessage += " (error code = ";
		error.fMessage += bytesread;
		error.fMessage += ")";
		fErrors.push_back(error);
	}
	else if (bytesread > 0 and message == NULL)
	{
		ErrorInfo error;
		error.fServer = server->GetInetAddress().GetHostName();
		error.fPort = server->GetInetAddress().GetPort();
		error.fMessage = "ERROR: Read an empty message from server ";
		error.fMessage += error.fServer;
		error.fMessage += ":";
		error.fMessage += error.fPort;
		fErrors.push_back(error);

		if (fPrintTrace)
		{
			cerr << "[ThinDaemonClient on " << gSystem->HostName()
				<< "] " << error.fMessage << endl;
		}
		
		bytesread = -100;
	}

	if (message != NULL) delete message;
	return bytesread;
}


void ThinDaemonClient::AddServer(TSocket* server)
{
	if (fPrintTrace)
	{
		cout << "[ThinDaemonClient on " << gSystem->HostName()
			<< "] New connection to server "
			<< server->GetInetAddress().GetHostName()
			<< "." << endl;
	}
	server->SetOption(kNoBlock, 1);
	fMonitor.Add(server);
}


void ThinDaemonClient::RemoveServer(TSocket* server)
{
	if (fPrintTrace)
	{
		cout << "[ThinDaemonClient on " << gSystem->HostName()
			<< "] Connection to server "
			<< server->GetInetAddress().GetHostName()
			<< " was closed." << endl;
	}
	fMonitor.Remove(server);
	server->Close();
	delete server;
}
