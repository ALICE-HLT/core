/*
 * Author: Artur Szostak <artursz@iafrica.com>
 */

#include "TROOT.h"
#include "TSystem.h"
#include "TString.h"
#include "Riostream.h"
#include "Utils.hpp"
#include "DaemonServer.hpp"
#include "ShmInfo.hpp"
#include <cstdlib>


void PrintUsage(bool printToStdError = false)
{
	ostream& out = printToStdError ? cerr : cout;
	out << "Usage: CommandDaemon [-port <port-number>] [-quiet] [-h|--help]" << endl;
	out << "  -port   This specifies the port number to use by the daemon on which it will listen to incomming connections."
		" It must be followed by a integer number." << endl;
	out << "  -quiet  Specifies that the program should not print all trace information." << endl;
	out << "  -h | --help  Will print this usage message and exit." << endl;
}


int main(int argc, char** argv)
{
	Int_t port = 1111;
	bool quiet = false;

	// Parse command line parameters.
	for (int i = 1; i < argc; i++)
	{
		if (TString(argv[i]) == "-port")
		{
			i++;
			if (i >= argc)
			{
				cerr << "ERROR: Must have a number after -port option." << endl;
				return EXIT_FAILURE;
			}
			TString number = argv[i];
			if (not number.IsDigit())
			{
				cerr << "ERROR: Expected a number after -port, but got: "
					<< argv[i] << endl;
				return EXIT_FAILURE;
			}
			port = number.Atoi();
			if (port < 0 or port > 65535)
			{
				cerr << "ERROR: The port number must be in the range [0..65535], but got: "
					<< argv[i] << endl;
				return EXIT_FAILURE;
			}
		}
		else if (TString(argv[i]) == "-quiet")
		{
			quiet = true;
		}
		else if (TString(argv[i]) == "-h" or TString(argv[i]) == "--help")
		{
			PrintUsage(false);
			return EXIT_SUCCESS;
		}
		else
		{
			cerr << "ERROR: Unknown command line option: " << argv[i] << endl;
			PrintUsage(true);
			return EXIT_FAILURE;
		}
	}

	DaemonServer server(port);
	if (quiet)
	{
		server.fPrintTrace = false;
		server.fPrintHeartBeat = false;
	}
	bool result = server.Run();

	return result ? EXIT_SUCCESS : EXIT_FAILURE;
}
