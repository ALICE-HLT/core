#!/usr/bin/env python

### TODO ###
# SendCommand also has heartbeat, if used more than twice every second might not need to call
# Do not alternate connect in ui when not managing to connect
### DONE LINE ###
# update slider after disconnect, reconnect
# ErrorCount
# GetError
# Use ip instead of hostname to identify the nodes
# Flush out all info in widget when there is no input


import sys, os, socket
import dbus
import dbus.mainloop.qt
from PyQt4 import QtCore, QtGui, uic
from subprocess import Popen

from ROOT import gSystem, Long, TString

gSystem.Load("/afsuser/dimuon-operator/ExpertGui/lib/libCommandDaemon.so")

from ROOT import ThinDaemonClient

app = QtGui.QApplication(sys.argv)
main_window, main_base_class = uic.loadUiType("shmem.ui")

port = 1111

#nodes = ["feptrd00", "feptrd04", "feptrd08", "feptrd10", 
        #"feptrd14", "fepspd0", "fepspd1", "fepspd2", 
        #"fepspd3", "fepspd4", "fepssd0", "fepssd1", 
        #"fepssd2", "fepssd3","fephmpid0", "fephmpid1", 
        #"fephmpid2", "fephmpid3", "fepdimutrg", "fepdimutrk1",
        #"fepdimutrk2", "fepdimutrk3", "fepdimutrk4", "fepdimutrk5",
        #"fephltout1", "fephltout2", "fepfmdaccorde"]

nodes = ["dev1.internal"]

class ShMemGui(QtGui.QMainWindow, main_window):
    
    def __init__(self, *args):
        QtGui.QWidget.__init__(self, *args)
        
        self.setupUi(self)
        
        # Create table model
        self.tableModel = QtGui.QStandardItemModel(0, 9)
        self.tableModel.setHeaderData(0, QtCore.Qt.Horizontal, QtCore.QVariant("Ip address"))
        self.tableModel.setHeaderData(1, QtCore.Qt.Horizontal, QtCore.QVariant("Hostname"))
        self.tableModel.setHeaderData(2, QtCore.Qt.Horizontal, QtCore.QVariant("Segments allocated"))
        self.tableModel.setHeaderData(3, QtCore.Qt.Horizontal, QtCore.QVariant("Pages allocated"))
        self.tableModel.setHeaderData(4, QtCore.Qt.Horizontal, QtCore.QVariant("Pages resident"))
        self.tableModel.setHeaderData(5, QtCore.Qt.Horizontal, QtCore.QVariant("Pages swapped"))
        self.tableModel.setHeaderData(6, QtCore.Qt.Horizontal, QtCore.QVariant("Max no. segments"))
        self.tableModel.setHeaderData(7, QtCore.Qt.Horizontal, QtCore.QVariant("Segment size min (byte)"))
        self.tableModel.setHeaderData(8, QtCore.Qt.Horizontal, QtCore.QVariant("Segment size max (kB)"))
        self.tableModel.setHeaderData(9, QtCore.Qt.Horizontal, QtCore.QVariant("Total size max"))
        
        self.tableView.setModel(self.tableModel)
        
        self.daemonClient = ThinDaemonClient()
        
        self.connect(self.connectButton, QtCore.SIGNAL("clicked()"), self.toggleDaemonConnection)
        self.connect(self.pullInfoButton, QtCore.SIGNAL("clicked()"), self.updateShmInfo)
        self.connect(self.timeMultiplierSlider, QtCore.SIGNAL("valueChanged(int)"), self.setTimeMultiplier)
        
        self.timer = QtCore.QTimer(self)
        self.connect(self.timer, QtCore.SIGNAL("timeout()"), self.timeoutTest)
        
        self.timeoutCounter = 100
        self.timeMultiplier = 1
        
        self.connected = False
        
        self.updateString = "Updating every %s second(s)."
        
        #self.heartbeatTimer = QtCore.QTimer(self)
        #self.connect(self.heartbeatTimer, QtCore.SIGNAL("timeout()"), self.update
    
    def toggleDaemonConnection(self):
        if self.connected:
            self.disconnectAllDaemons()
            self.connected = False
            self.connectButton.setText("Connect")
        else:
            self.connectAllDaemons()
            self.connected = True
            self.connectButton.setText("Disconnect")
    
    def setTimeMultiplier(self, sliderValue):
        self.timeMultiplier = sliderValue
        self.updateText.setText(self.updateString % sliderValue)
    
    def timeoutTest(self):
        if not self.timeMultiplier == 0:
            if self.timeoutCounter > 0:
                self.timeoutCounter -= (100 / self.timeMultiplier)
            elif self.timeoutCounter <= 0:
                self.timeoutCounter = 100
                self.updateShmInfo()
            else:
                print "ERROR: Unexpected timerCounter value: ", self.timerCounter
            self.timeoutProgressBar.setValue(self.timeoutCounter)
    
    def updateShmInfo(self):
        
        for node in nodes:
            if not self.daemonClient.IsConnected(node):
                ret = self.daemonClient.ConnectToDaemon(node, port)
                #if ret:
                    #print "Node %s reconnected" % node
                #else:
                    #print "Could not reconnect node %s." % node
        
        self.daemonClient.SendCommand("getSysVShmInfo")
        
        # If errors
        errors = self.daemonClient.ErrorCount()
        if errors > 0:
            for error in range(errors):
                msg = TString("")
                host = TString("")
                errPort = Long(0)
                self.daemonClient.GetError(error, msg, host, errPort)
                if not self.daemonClient.IsConnected(host):
                    print "Host not connected: %s " % str(host)
                    self.removeHostFromTableModel(socket.gethostbyname(str(host)))
                print "ERROR at '" + str(host) + ":" + str(errPort) + "':" + str(msg)
        
        resultCount = self.daemonClient.SysVShmResultCount()
        
        # n -> resultIndex
        for n in range(resultCount):
            
            shmResult = self.daemonClient.GetSysVShmResult(n)
            hostRow = self.tableModel.findItems(socket.gethostbyname(shmResult.fHostname))
            
            # if row with host exist, update, else create new
            if len(hostRow) == 1:
                rowIndex = self.tableModel.indexFromItem(hostRow[0])
                self.insertDataInTableModel(rowIndex.row(), shmResult)
            elif len(hostRow) == 0:
                newRowIndex = self.tableModel.rowCount()
                self.tableModel.insertRow(newRowIndex, QtCore.QModelIndex())
                self.insertDataInTableModel(newRowIndex, shmResult)
            else:
                print "ERROR: More than one host of same hostname: ", len(hostRow)
    
    def removeHostFromTableModel(self, hostaddress):
        tableItem = self.tableModel.findItems(hostaddress)
        self.tableModel.removeRow(tableItem[0].row())
    
    def insertDataInTableModel(self, rowIndex, shmResult):
        modelIndex = self.tableModel.index(rowIndex, 0, QtCore.QModelIndex())
        self.tableModel.setData(modelIndex, QtCore.QVariant(socket.gethostbyname(shmResult.fHostname)))
        
        modelIndex = self.tableModel.index(rowIndex, 1, QtCore.QModelIndex())
        self.tableModel.setData(modelIndex, QtCore.QVariant(shmResult.fHostname))
        
        modelIndex = self.tableModel.index(rowIndex, 2, QtCore.QModelIndex())
        self.tableModel.setData(modelIndex, QtCore.QVariant(shmResult.fSegmentsAllocated))
        
        modelIndex = self.tableModel.index(rowIndex, 3, QtCore.QModelIndex())
        self.tableModel.setData(modelIndex, QtCore.QVariant(shmResult.fPagesAllocated))
        
        modelIndex = self.tableModel.index(rowIndex, 4, QtCore.QModelIndex())
        self.tableModel.setData(modelIndex, QtCore.QVariant(shmResult.fPagesResident))
        
        modelIndex = self.tableModel.index(rowIndex, 5, QtCore.QModelIndex())
        self.tableModel.setData(modelIndex, QtCore.QVariant(shmResult.fPagesSwapped))
        
        modelIndex = self.tableModel.index(rowIndex, 6, QtCore.QModelIndex())
        self.tableModel.setData(modelIndex, QtCore.QVariant(shmResult.fMaxSegments))
        
        modelIndex = self.tableModel.index(rowIndex, 7, QtCore.QModelIndex())
        self.tableModel.setData(modelIndex, QtCore.QVariant(shmResult.fMinSegmentSize))
        
        modelIndex = self.tableModel.index(rowIndex, 8, QtCore.QModelIndex())
        self.tableModel.setData(modelIndex, QtCore.QVariant(shmResult.fMaxSegmentSize))
        
        modelIndex = self.tableModel.index(rowIndex, 9, QtCore.QModelIndex())
        self.tableModel.setData(modelIndex, QtCore.QVariant(shmResult.fTotalSize))
        

    
    def connectAllDaemons(self):
        for node in nodes:
            ret = self.daemonClient.ConnectToDaemon(node, port)
            if ret:
                if self.timeMultiplier == 0:
                    self.setTimeMultiplier(1)
                self.updateShmInfo()
                self.timer.start(1000)
                #print "Successfully connected: " + str(ret)
            else:
                print "Error: Failed to connect: " + str(ret)
    
    def disconnectAllDaemons(self):
        ret = self.daemonClient.DisconnectAll()
        #print "Disconnecting from all daemons..." + str(ret)
        self.timer.stop()

def main():
    window = ShMemGui()
    window.show()
    app.exec_()
    

if __name__ == "__main__":
    main()
