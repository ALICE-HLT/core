#!/usr/bin/env python

import sys
import dbus, dbus.mainloop.qt
from PyQt4 import QtCore, QtGui, uic
import avahi

from ROOT import gSystem

gSystem.Load("/afsuser/dimuon-operator/ExpertGUI/libCommandDaemon.so")

from ROOT import ThinDaemonClient

g_iface = avahi.IF_UNSPEC
g_proto = avahi.PROTO_UNSPEC
g_stype = "_cmd._tcp"
g_domain = "local"
g_flags = dbus.UInt32(0)

app = QtGui.QApplication(sys.argv)
main_window, main_base_class = uic.loadUiType("qt4_gui.ui")


class CommandDaemonDiscover(QtGui.QMainWindow, main_window):
    
    def __init__(self, *args):
        QtGui.QWidget.__init__(self, *args)
        
        self.setupUi(self)
        
        self.system_bus = dbus.SystemBus()
        self.avahi_server = dbus.Interface(self.system_bus.get_object(avahi.DBUS_NAME, avahi.DBUS_PATH_SERVER), avahi.DBUS_INTERFACE_SERVER)
        self.service_browser_obj = self.system_bus.get_object(avahi.DBUS_NAME, self.avahi_server.ServiceBrowserNew(g_iface, g_proto, g_stype, g_domain, g_flags))
        
        self.service_browser = dbus.Interface(self.service_browser_obj, avahi.DBUS_INTERFACE_SERVICE_BROWSER)
        self.service_browser.connect_to_signal('ItemNew', self.newService)
        
        self.daemonClient = ThinDaemonClient()
        
        self.connect(self.connectButton, QtCore.SIGNAL("clicked()"), self.connectDaemonClient)
        self.connect(self.disconnectButton, QtCore.SIGNAL("clicked()"), self.disconnectDaemonClient)
        self.connect(self.lineEdit, QtCore.SIGNAL("returnPressed()"), self.sendCommand)
        self.connect(self.sendCmdButton, QtCore.SIGNAL("clicked()"), self.sendCommand)
        
        #QTimer *timer = new QTimer(this);
     #connect(timer, SIGNAL(timeout()), this, SLOT(update()));
     #timer->start(1000);
    
    def sendCommand(self):
        cmd = self.lineEdit.text()
        self.lineEdit.clear()
        self.consoleTextEdit.append("cmd: " + cmd)
        self.daemonClient.SendCommand(str(cmd))
        self.daemonClient.ErrorCount()
    
    def connectDaemonClient(self):
        ret = self.daemonClient.ConnectToDaemon("dev0", 6000)
        if ret:
            self.consoleTextEdit.append("Successfully connected: " + str(ret))
        else:
            self.consoleTextEdit.append("Failed to connect: " + str(ret))
    
    def disconnectDaemonClient(self):
        ret = self.daemonClient.DisconnectAll()
        self.consoleTextEdit.append("Disconnecting from daemon..." + str(ret))
    
    def newService(self, iface, proto, name, stype, domain, flags):
        service_resolver_obj = self.system_bus.get_object(avahi.DBUS_NAME, self.avahi_server.ServiceResolverNew(iface, proto, name, stype, domain, proto, flags))
    
        self.service_resolver = dbus.Interface(service_resolver_obj, avahi.DBUS_INTERFACE_SERVICE_RESOLVER)
        self.service_resolver.connect_to_signal('Found', self.serviceResolved)
    
    def serviceResolved(self, iface, proto, name, stype, domain, host, aproto, adr, port, txt, flags):
        self.hostListWidget.addItem(host)

def main():
    dbus.mainloop.qt.DBusQtMainLoop(set_as_default=True)
    
    commandWindow = CommandDaemonDiscover()
    
    commandWindow.show()
    app.exec_()

if __name__ == "__main__":
    main()