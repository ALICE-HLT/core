#!/usr/bin/env python

import sys, os

sys.path.append(os.getcwd())

from InfoLoggerLogServer import InfoLoggerLogServer, MLUCLog
from InfoLoggerLogServerTest import InfoLoggerLogServerTest, MLUCLog

# Test variables
origin = "origin"       #const char* origin,
keyword = "keyword"     #const char* keyword,
file = "file"          #const char* file,
linenr = 1              #int linenr,
compile_date = "01.01.99"   #const char* compile_date,
compile_time = "11:11"      #const char* compile_time,
logLvl = MLUCLog.kError     #MLUCLog::TLogLevel logLvl,
hostname = "somehost"   #const char* hostname,
Id = "someID"          #const char* id,
ldate = 1               #uint32 ldate,
ltime_s = 2             #uint32 ltime_s,
ltime_us = 3            #uint32 ltime_us,
msgNr = 4               #uint32 msgNr,
line = "Log messagees"  #const char* line

# Testting extremly long facility name
infoLogLib = InfoLoggerLogServer("Thaaaaaaaaaas iiiiiiiiis aaaaaaaaaaaaa veeeeeeeeeeeeery loooooooooooooong faaaaaaaaaaaaaaacility striiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiyyyyyyyyyyyiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiyyyyyyyyyyyyyyyyiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiing")

infoLogLib.LogLine(origin, keyword, file, linenr, compile_date, compile_time, MLUCLog.kError, hostname, Id, ldate, ltime_s, ltime_us, msgNr, line)

#infoLogLib.LogLine(origin, keyword, iFile, linenr, compile_date, compile_time, logLvl, hostname, iId, ldate, ltime_s, ltime_us, msgNr, line)

# Exercise the Test version of InfoLoggerLogServer
infoLogLibTest = InfoLoggerLogServerTest("Test_facility")

#infoLogLibTest.LogLine(origin, keyword, file, linenr, compile_date, compile_time, logLvl, hostname, Id, ldate, ltime_s, ltime_us, msgNr, line)

# Benchmark
infoLogLibTest.LogLine(origin, keyword, file, linenr, compile_date, compile_time, MLUCLog.kBenchmark, hostname, Id, ldate, ltime_s, ltime_us, msgNr, line)

# Debug
infoLogLibTest.LogLine(origin, keyword, file, linenr, compile_date, compile_time, MLUCLog.kDebug, hostname, Id, ldate, ltime_s, ltime_us, msgNr, line)

# Information
infoLogLibTest.LogLine(origin, keyword, file, linenr, compile_date, compile_time, MLUCLog.kInformational, hostname, Id, ldate, ltime_s, ltime_us, msgNr, line)

# Warning
infoLogLibTest.LogLine(origin, keyword, file, linenr, compile_date, compile_time, MLUCLog.kWarning, hostname, Id, ldate, ltime_s, ltime_us, msgNr, line)

# Error
infoLogLibTest.LogLine(origin, keyword, file, linenr, compile_date, compile_time, MLUCLog.kError, hostname, Id, ldate, ltime_s, ltime_us, msgNr, line)

# Fatal
infoLogLibTest.LogLine(origin, keyword, file, linenr, compile_date, compile_time, MLUCLog.kFatal, hostname, Id, ldate, ltime_s, ltime_us, msgNr, line)

# (Primary) Important?
#infoLogLibTest.LogLine(origin, keyword, file, linenr, compile_date, compile_time, MLUCLog.kFPrimary, hostname, Id, ldate, ltime_s, ltime_us, msgNr, line)

