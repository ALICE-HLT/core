%module InfoLoggerLogServer

%{
#include "InfoLoggerLogServer.hpp"
#include "MLUCLog.hpp"

typedef unsigned int uint32;
%}

/* Defining a new type... */
typedef unsigned int uint32;

class MLUCLog {
    public:
        enum TLogLevel { kNone = 0, kBenchmark = 0x01, kDebug = 0x02, kInformational = 0x04, kWarning = 0x08,
                         kError = 0x10 , kFatal = 0x20, kPrimary = 0x80, kAll = 0xBF };
};

class InfoLoggerLogServer: public MLUCLogServer {
    public:
        InfoLoggerLogServer(const char* pFacilityName);
        
        void LogLine( const char* origin, const char* keyword, const char* file,
                              int linenr, const char* compile_date, const char* compile_time,
                              MLUCLog::TLogLevel logLvl, const char* hostname, const char* id,
                              uint32 ldate, uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line );
        
        void LogLine( const char* origin, const char* keyword, const char* file,
                              int linenr, const char* compile_date, const char* compile_time,
                              MLUCLog::TLogLevel, const char* hostname, const char* id,
                              uint32 ldate, uint32 ltime_s, uint32 ltime_us, uint32 msgNr,
                              uint32 subMsgNr, const char* line );
};
