#include "InfoLoggerLogServerTest.hpp"

InfoLoggerLogServerTest::InfoLoggerLogServerTest(const char* pFacilityName)
{
    facilityName = pFacilityName;
    infoSetFacility(facilityName);
}

InfoLoggerLogServerTest::~InfoLoggerLogServerTest(){}

void InfoLoggerLogServerTest::LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
                                   const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl,
                                   const char* hostname, const char* id, uint32 ldate, uint32 ltime_s, 
                                   uint32 ltime_us, uint32 msgNr, const char* line )
{
    switch (logLvl)
    {
        case MLUCLog::kBenchmark: infoLogLevel = LOG_BENCHMARK; break;
        case MLUCLog::kDebug: infoLogLevel = LOG_DDEBUG; break;
        case MLUCLog::kInformational: infoLogLevel = LOG_INFO; break;
        case MLUCLog::kWarning: infoLogLevel = LOG_WARNING; break;
        case MLUCLog::kError: infoLogLevel = LOG_ERROR; break;
        case MLUCLog::kFatal: infoLogLevel = LOG_FATAL; break;
        case MLUCLog::kImportant: infoLogLevel = LOG_IMPORTANT; break;
        default: return;
    }
    
    // Convert integer to string and build return string
    convertStream << linenr;
    convertStream >> lineNrStr;
    retLine = string(line) + " -> " + string(file) + ":" + lineNrStr;
    
    infoLogS_f(infoLogLevel, retLine.c_str());
}

void InfoLoggerLogServerTest::LogLine( const char* origin, const char* keyword, const char* file, int linenr,
                                       const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl,
                                       const char* hostname, const char* id, uint32 ldate, uint32 ltime_s,
                                       uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, const char* line )
{
    switch (logLvl)
    {
        case MLUCLog::kBenchmark: infoLogLevel = LOG_BENCHMARK; break;
        case MLUCLog::kDebug: infoLogLevel = LOG_DDEBUG; break;
        case MLUCLog::kInformational: infoLogLevel = LOG_INFO; break;
        case MLUCLog::kWarning: infoLogLevel = LOG_WARNING; break;
        case MLUCLog::kError: infoLogLevel = LOG_ERROR; break;
        case MLUCLog::kFatal: infoLogLevel = LOG_FATAL; break;
        case MLUCLog::kImportant: infoLogLevel = LOG_IMPORTANT; break;
        default: return;
    }
    
    // Convert integer to string and build return string
    convertStream << linenr;
    convertStream >> lineNrStr;
    retLine = string(line) + " -> " + string(file) + ":" + lineNrStr;
    
    infoLogS_f(infoLogLevel, retLine.c_str());
}

MLUCLogServer* CreateLogServer(const char* pFacilityName)
{
    return new InfoLoggerLogServerTest(pFacilityName);
}
