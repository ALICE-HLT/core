#include "MLUCLogServer.hpp"
#include <string>
#include <sstream>

#ifdef LOG
#undef LOG
#endif

extern "C" {
#include "infoLogger.h"
    MLUCLogServer* CreateLogServer(const char* pFacilityName);
}

class InfoLoggerLogServerTest: public MLUCLogServer
{
    private:
        const char* facilityName;
        char infoLogLevel;
        string retLine, lineNrStr;
        stringstream convertStream;
    
    public:
        InfoLoggerLogServerTest(const char* pFacilityName);
        ~InfoLoggerLogServerTest();
        
        virtual void LogLine( const char* origin, const char* keyword, const char* file,
                              int linenr, const char* compile_date, const char* compile_time,
                              MLUCLog::TLogLevel logLvl, const char* hostname, const char* id,
                              uint32 ldate, uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line );
        
        virtual void LogLine( const char* origin, const char* keyword, const char* file,
                              int linenr, const char* compile_date, const char* compile_time,
                              MLUCLog::TLogLevel, const char* hostname, const char* id,
                              uint32 ldate, uint32 ltime_s, uint32 ltime_us, uint32 msgNr,
                              uint32 subMsgNr, const char* line );
};
