#include <iostream>
#include "DrawESD.h"
#include "TRint.h"

DrawESD* gDraw;

int main(int argc, char** argv){
  if(argc < 3){
    std::cerr << "Usage: " << argv[0] << " <input file> <output dir> [debug]" << std::endl;
    return 1;
  }
  int c=2;
  char *rargs[c];
  rargs[0]=argv[0];
  rargs[1]="-l";
  TRint  *app = new TRint("App", &c, rargs);

  bool debug=false;
  if(argc > 3) debug=true;

  gDraw = DrawESD::Instance(debug);
  std::cout << "processing \"" << argv[1] << "\"" << std::endl;

  gDraw->ProcessEsdFile(argv[1], argv[2]);

  if(argc > 4){
    
    int min=832;
    int max=833;
    for (int i=min; i<=max; ++i){
      gDraw->ProcessClusters(Form("%s/TPC_%d.clusters",argv[4],i),i);
    }
    
    min=968;
    max=970;
    for (int i=min; i<=max; ++i){
      gDraw->ProcessClusters(Form("%s/TPC_%d.clusters",argv[4],i),i);
    }
  }
  gDraw->PlotChargeDist();

  if(debug)
    app->Run(kTRUE);
  
  //app->Terminate(0);

  delete gDraw;
}
