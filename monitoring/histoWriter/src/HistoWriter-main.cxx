#include "TROOT.h"
#include "TStyle.h"

Int_t HistoWriter(bool writeRootFiles, int argc, char** argv);

int main(int argc, char** argv){
  /*
  gROOT->SetStyle("Plain");
  gStyle->SetPalette(1);
  */
  gROOT->SetStyle("Plain");
  gStyle->SetPalette(1);
  gStyle->SetFillColor(-1);
  gStyle->SetFillStyle(4000); 
  gStyle->SetTitleBorderSize(0);
  //gStyle->SetStatBorderSize(0);
  //gStyle->SetLegendBorderSize(0);
  //gStyle->SetOptStat(10);

  return HistoWriter(true, argc, argv);

}
