
#ifndef DRAWESD_H
#define DRAWESD_H

#include "TSystem.h"

class TEvePointSet;
class AliESDtrack;
class AliEveTrack;
class TEveTrackList;
class TEveTrackPropagator;
class TString;
class AliExternalTrackParam;
class AliESDEvent;
class TEveElementList;
class TEveMagField;
class TEvePointSetArray;
class TH2F;

class DrawESD {

public:
  
  static DrawESD* Instance(bool debug=false);
  
  /** Destructor **/
  ~DrawESD();
  
  void ProcessEsdFile( Char_t* file, Char_t* outdir );
  void ProcessEsdEvent( AliESDEvent * esd, Char_t* outdir );
  void ProcessClusters(Char_t* file, Int_t ddl);
  void setCamera(Double_t horizontalRotation,Double_t verticalRotation);
  void saveViewer(Char_t* outdir);

  void PlotChargeDist();

private:
  DrawESD();
  DrawESD(bool debug);

  /** copy constructor prohibited */
  DrawESD(const DrawESD&);
  /** assignment operator prohibited */
  DrawESD& operator = (const DrawESD );

  void setLight();

  void geomGentleTPC();

  /*Create the pointset for the display */
  void CreateTrackList();

  // Make a standard track representation and put it into given container.
  // Choose which parameters to use a track's starting point.
  // If gkFixFailedITSExtr is TRUE (FALSE by default) and
  // if ITS refit failed, take track parameters at inner TPC radius.
  AliEveTrack * MakeEsdTrack(AliESDtrack *at, TEveTrackList* cont);

  //Set up the track propagator
  void SetUpTrackPropagator(TEveTrackPropagator* trkProp, Float_t magF, Float_t maxR);

  //Create a title for the track
  TString CreateTrackTitle(AliESDtrack* t);
  
  TEvePointSetArray* CreatePointSetArray();
  TEvePointSet* CreatePointSet();
  
  ///Create the pointset to display primary vertex
  void CreateVertexPointSet();

  //Add track param to AliEveTrack
  void AddTrackParamToTrack(AliEveTrack* track, const AliExternalTrackParam* tp);

  //Create eve tracks and put them in track list
  void FillTrackList(AliESDtrack * esdTrack);

  //Create track lists
  void CreateTrackLists();

  //Get color from pt
  Color_t GetColor(Float_t pt);
  //Get color bin track belongs to
  Int_t GetColorBin(Float_t pt);


  Bool_t fTrueField;        //Use true field?
  Bool_t fUseIpOnFailedITS; // Use IP as origin if ITS refit fails?
  Bool_t fUseRkStepper;    // Use Runge Kutta for something something?

  TEveTrackList * fTrackList;  //Eve tracklist 
  TEveElementList * fTrackLists; //Holder for tracklists
  TEvePointSet * fPointSetVertex;      //Display primary vertex
  
  Int_t fNTrackBins;
  Int_t fTrackcount[10];
  
  Int_t fRunNo;
  ULong_t fEventId;
  TString fDate;
  TString fTime;
  bool fDebug;
  TEveMagField* fMagField;
  TEvePointSetArray * fEveColClusters;
  TEvePointSet * fEveClusters;
  static DrawESD* fInstance;

  TH2F* fChargeDist;
};

#endif
