#include "DrawESD.h"

#include "TEveManager.h"
#include "TStyle.h"
#include "TEveViewer.h"
#include "TGLViewer.h"
#include "TGLLightSet.h"
#include "TEveTrack.h"
#include "TFile.h"
#include "TEveGeoShapeExtract.h"
#include "TEveGeoShape.h"
#include "TTimeStamp.h"
#include "AliEveTrack.h"
#include "TEveTrackPropagator.h"
#include "TEveVSDStructs.h"
#include "TH2.h"
#include "AliESDEvent.h"
#include "AliEveMagField.h"
#include "AliCDBManager.h"
#include "TPCLib/tracking-ca/AliHLTTPCCATrackParam.h"
#include "TPCLib/tracking-ca/AliHLTTPCCATrackConvertor.h"
#include "TEvePointSet.h"
#include <iostream>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include "AliHLTTPCClusterDataFormat.h"

using namespace std;

DrawESD* DrawESD::fInstance=NULL;

DrawESD* DrawESD::Instance(bool debug){
  if (! fInstance)
    fInstance=new DrawESD(debug);
  return fInstance;
}


DrawESD::DrawESD() {
  DrawESD(false);
}

DrawESD::DrawESD(bool debug) : 
  fTrueField(kFALSE),
  fUseIpOnFailedITS(kFALSE),
  fUseRkStepper(kFALSE),
  fTrackList(NULL),
  fTrackLists(NULL),
  fPointSetVertex(NULL),
  fNTrackBins(10),
  fRunNo(0),
  fEventId(0),
  fDate(),
  fTime(),
  fDebug(debug),
  fMagField(NULL),
  fEveColClusters(NULL),
  fEveClusters(NULL)
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  srand(tv.tv_sec);
  if(fDebug)
    TEveManager::Create();
  else
    TEveManager::Create(kFALSE, "V");

  
  gStyle->SetPalette(1, 0);

  // -- Background color
  gEve->GetDefaultViewer()->GetGLViewer()->SetClearColor(kGray+3);

  // -- Turn and off light
  setLight();
  
  for (int i=0; i<10; ++i)
     fTrackcount[i] = 0;


  fChargeDist=new TH2F("Charge distribution","Charge distribution",
		   100, 0., 200.,
		   100, 50., 250.);
  //fChargeDist->SetOption("colz");
  //fChargeDist->SetLogz();

  /*
  const char* hcdb = gSystem->Getenv("ALIHLT_T_HCDBDIR");
  if(hcdb){
    cout << "Setting OCDB path to \"" << hcdb << "\"" << endl;
    fTrueField=kTRUE;
    TString hcdbpath;
    hcdbpath.Form("local://%s", hcdb);
    AliCDBManager::Instance()->SetDefaultStorage(hcdbpath.Data());
  } else {
    cout << "$ALIHLT_T_HCDBDIR not found, using manual magnetic field (might produce funny results)." << endl;
  }
  */
}
///___________________________________________________________________
DrawESD::~DrawESD()
{
  delete fChargeDist;

  //Destructor, not implemented
  if(fTrackList)
    delete fTrackList;
  fTrackList = NULL;

  if(fTrackLists)
    delete fTrackLists;
  fTrackLists = NULL;

  if(fEveColClusters)
    delete fEveColClusters;
  fEveColClusters=NULL;

  if(fEveClusters)
    delete fEveClusters;
  fEveClusters=NULL;

  if(! fDebug)
    TEveManager::Terminate();
}

void DrawESD::setLight() {
  // -- Turn and off light
  //    Set light power [0.,1.]

  gEve->GetDefaultViewer()->GetGLViewer()->GetLightSet()->SetLight(TGLLightSet::kLightFront,    0);
  gEve->GetDefaultViewer()->GetGLViewer()->GetLightSet()->SetFrontPower(0.8);       // [0.,1.]
  gEve->GetDefaultViewer()->GetGLViewer()->GetLightSet()->SetLight(TGLLightSet::kLightTop,      0);
  gEve->GetDefaultViewer()->GetGLViewer()->GetLightSet()->SetLight(TGLLightSet::kLightBottom,   0);
  gEve->GetDefaultViewer()->GetGLViewer()->GetLightSet()->SetLight(TGLLightSet::kLightLeft,     1);
  gEve->GetDefaultViewer()->GetGLViewer()->GetLightSet()->SetLight(TGLLightSet::kLightRight,    1);
  gEve->GetDefaultViewer()->GetGLViewer()->GetLightSet()->SetSidePower(0.5);       // [0.,1.]

  gEve->GetDefaultViewer()->GetGLViewer()->GetLightSet()->SetUseSpecular(kFALSE);
  gEve->GetDefaultViewer()->GetGLViewer()->GetLightSet()->SetLight(TGLLightSet::kLightSpecular, 1);
  gEve->GetDefaultViewer()->GetGLViewer()->GetLightSet()->SetSpecularPower(1.);    // [0.,1.]

  gEve->Redraw3D(kFALSE);

  return;
}

// ################################################################################
void DrawESD::setCamera(Double_t horizontalRotation,Double_t verticalRotation) {



  gEve->FullRedraw3D(kTRUE);

  gEve->GetDefaultViewer()->GetGLViewer()->CurrentCamera().RotateRad(horizontalRotation,verticalRotation);
  gEve->FullRedraw3D(kFALSE);

  return;
}

void DrawESD::geomGentleTPC() {
  // Simple geometry


  TFile* geom = TFile::Open("$ALICE_ROOT/EVE/alice-data/gentle_geo.root", "CACHEREAD");
  if (!geom)
    return;
  
  TEveGeoShapeExtract* gse = (TEveGeoShapeExtract*) geom->Get("Gentle");
  TEveGeoShape* gsre = TEveGeoShape::ImportShapeExtract(gse, 0);
  geom->Close();
  delete geom;
  
  gEve->AddGlobalElement(gsre);
  
  TEveElement* elTRD = gsre->FindChild("TRD+TOF");
  elTRD->SetRnrState(kFALSE);
  
  TEveElement* elPHOS = gsre->FindChild("PHOS");
  elPHOS->SetRnrState(kFALSE);
  
  TEveElement* elHMPID = gsre->FindChild("HMPID");
  elHMPID->SetRnrState(kFALSE);

  //TEveElement* elITS = gsre->FindChild("ITS");
  //elITS->SetRnrState(kFALSE);
  
  /*
  //new TGLAnnotation(gEve->GetDefaultViewer()->GetGLViewer(), "Test text asklfhla", 20., 10.);
  Float_t width      = 4096.;
  Float_t height     = 3048.;
  Float_t boxwidth   = 800.;
  Float_t boxheight  = 800.;
  
 
  TGLOverlayButton* infoText = new TGLOverlayButton(gEve->GetDefaultViewer()->GetGLViewer(), "Test text asklfhla\r\n\rnewline",
						    width - 10. - boxwidth,height - 10. - boxheight, boxwidth, boxheight );
  infoText->SetBackColor(kBlack);
  */
  
  return;
}

// ###############################################################################
void DrawESD::saveViewer(Char_t* outdir) {

  TString eventName(Form("%s/event_%d_0x%lX_%s_%s.png", outdir, fRunNo, fEventId, fDate.Data(), fTime.Data()));


  Int_t   width  = 1600;

  gEve->GetDefaultViewer()->GetGLViewer()->SavePictureWidth(eventName, width);

  gEve->Redraw3D(kFALSE);

  return;
}

///________________________________________________________________________________________
void DrawESD::CreateTrackList() {
  //See header file for documentation
  fTrackList = new TEveTrackList("ESD Tracks");
  fTrackList->SetMainColor(kOrange);
  gEve->AddElement(fTrackList);
}

///________________________________________________________________________________________
void DrawESD::CreateTrackLists() {
  //See header file for documentation
  fTrackLists = new TEveElementList("ESD Track lists");
  gEve->AddElement(fTrackLists);
  for(Int_t i = 0; i < 10; i++) {
    TEveTrackList * trackList = new TEveTrackList(Form("Tracks_%d", i));
    trackList->SetMainColor(kOrange-i);
    fTrackLists->AddElement(trackList);
  }
}


///_________________________________________________________________________________________
void DrawESD::CreateVertexPointSet() {
  //See header file for documentation
  fPointSetVertex = new TEvePointSet("Primary Vertex");
  fPointSetVertex->SetMainColor(6);
  fPointSetVertex->SetMarkerStyle((Style_t)kFullStar);

  gEve->AddElement(fPointSetVertex);
}


TEvePointSetArray * DrawESD::CreatePointSetArray(){
  //See header file for documentation
  Int_t fNColorBins=15;
  TEvePointSetArray * cc = new TEvePointSetArray("TPC Clusters Colorized");
  cc->SetMainColor(kRed);
  cc->SetMarkerStyle(4); // antialiased circle
  cc->SetMarkerSize(0.4);
  cc->SetDefPointSetCapacity(1000000);
  cc->InitBins("Cluster Charge", fNColorBins, 0., fNColorBins*20.);
  
  const Int_t nCol = TColor::GetNumberOfColors();
  
  for (Int_t ii = 0; ii < fNColorBins + 1; ++ii) {
    cc->GetBin(ii)->SetMainColor(TColor::GetColorPalette(ii * nCol / (fNColorBins+2)));
  }
  gEve->AddElement(cc);

  return cc;
     
}

TEvePointSet * DrawESD::CreatePointSet() {
  //See header file for documentation

  TEvePointSet * ps = new TEvePointSet("TPC Clusters");
  ps->SetMainColor(kRed);
  ps->SetMarkerStyle((Style_t)kFullDotSmall);

  gEve->AddElement(ps);

  return ps;

}
///_________________________________________________________________________________
Color_t DrawESD::GetColor(Float_t pt) {
  //See header file
  Color_t baseColor = kOrange;
  
  Float_t binlimit = 0.1;
  for(Int_t i = 0; i< 10; i++) {
   
    if (pt < binlimit)
      return baseColor - i;
    
    binlimit +=0.1;
  }
  
  return baseColor - 9;

}
///_________________________________________________________________________________
Int_t DrawESD::GetColorBin(Float_t pt) {
  //See header file
  
  Float_t binlimit = 0.1;
  for(Int_t i = 0; i< 10; i++) {
   
    if (pt < binlimit)
      return i;
    
    binlimit +=0.1;
  }
  
  return 9;
  
}

static inline Int_t getSliceFromDDL(Int_t ddl){
  Int_t idx=ddl - 768;
  if(idx < 72)
    return idx / 2;
  else{
    idx=idx-72;
    return idx / 4;
  }
}

void DrawESD::PlotChargeDist(){
  fChargeDist->Draw("colz,logz");
}

void DrawESD::ProcessClusters(Char_t* file, Int_t ddl){
  if(!fEveColClusters){
    fEveColClusters = CreatePointSetArray();
  }
  /*
  if(!fEveClusters){
    fEveClusters = CreatePointSet();
  }
  */
  Int_t slice=getSliceFromDDL(ddl);
  cout << "DDL: " << ddl << " -> Slice: " << slice << endl;
  Float_t phi   = ( slice + 0.5 ) * TMath::Pi() / 9.0;  
  Float_t cos   = TMath::Cos( phi );
  Float_t sin   = TMath::Sin( phi );
  
  int fd= open(file, O_RDONLY);
  if (fd == -1){
    cout << "Cannot open file \"" << file << "\"" << endl;
    return;
  }
  struct stat sb;
  if (fstat(fd, &sb) == -1){
    cout << "Cannot get file stats" << endl;
    close(fd);
    return;
  }

  void *addr = mmap(NULL,sb.st_size, PROT_READ,MAP_PRIVATE, fd, 0);
  if (addr == MAP_FAILED){
    cout << "Cannot map file" << endl;
    close(fd);
    return;
  }

  AliHLTTPCClusterData *cd = reinterpret_cast<AliHLTTPCClusterData*> (addr);
  UChar_t *data            = reinterpret_cast<UChar_t*> (cd->fSpacePoints);
  cout << "Processing " << cd->fSpacePointCnt << " clusters" << endl;
  if ( cd->fSpacePointCnt != 0 ) {
    for (UInt_t iter = 0; iter < cd->fSpacePointCnt; ++iter, data += sizeof(AliHLTTPCSpacePointData)) {
      //if(rand() % 100 != 0)
      //continue;
      AliHLTTPCSpacePointData *sp = reinterpret_cast<AliHLTTPCSpacePointData*> (data);
      fEveColClusters->Fill(cos*sp->fX - sin*sp->fY, sin*sp->fX + cos*sp->fY, sp->fZ, sp->fCharge);
      //      fEveClusters->SetNextPoint(cos*sp->fX - sin*sp->fY, sin*sp->fX + cos*sp->fY, sp->fZ);
      fChargeDist->Fill(sp->fCharge, TMath::Sqrt(sp->fX * sp->fX + sp->fY * sp->fY));
    }
  }

  fEveColClusters->ElementChanged();
  //  fEveClusters->ElementChanged();

  munmap(data, sb.st_size);
  close(fd);
}

///____________________________________________________________________
void DrawESD::ProcessEsdFile( Char_t* file, Char_t* outdir ) {

   TFile *inFile = TFile::Open(file);
   AliESDEvent* esd = dynamic_cast<AliESDEvent*>(inFile->Get("AliESDEvent"));
   esd->GetStdContent();
   ProcessEsdEvent( esd, outdir );
}

///____________________________________________________________________
void DrawESD::ProcessEsdEvent( AliESDEvent * esd, Char_t* outdir ) {
  //See header file for documentation
  if(!fPointSetVertex) CreateVertexPointSet();
  if(!fTrackLists) CreateTrackLists();

  UInt_t year  = 0;
  UInt_t month = 0; 
  UInt_t day   = 0;
  
  UInt_t hour = 0;
  UInt_t min  = 0;
  UInt_t sec  = 0;

  esd->GetStdContent();
  
  TTimeStamp timeStamp(esd->GetTimeStamp());
  timeStamp.GetDate(kFALSE,10,&year,&month,&day);
  timeStamp.GetTime(kFALSE, 0, &hour, &min, &sec);
  
  fDate = Form("%d-%02d-%02d", year, month, day);
  fTime = Form("%02d:%02d:%02d", hour, min, sec);
  fRunNo = esd->GetRunNumber();
  if(AliCDBManager::Instance()->GetDefaultStorage())
    AliCDBManager::Instance()->SetRun(fRunNo);
  UShort_t bx = esd->GetBunchCrossNumber();
  UInt_t orbit = esd->GetOrbitNumber();
  UInt_t period = esd->GetPeriodNumber();
  fEventId = bx | (orbit << 12) | ((ULong_t)period << 36);
  
  
  cout << "ProcessESDEvent() :"<< esd->GetEventNumberInFile()<< "  " << esd->GetNumberOfCaloClusters() << " tracks : " << esd->GetNumberOfTracks() << endl;

  //fEventManager->SetRunNumber(esd->GetRunNumber());

  Double_t vertex[3];
  const AliESDVertex * esdVertex = esd->GetPrimaryVertex();
  
  if(esdVertex) {
    esdVertex->GetXYZ(vertex);
    fPointSetVertex->SetNextPoint(vertex[0], vertex[1], vertex[2]);
  }
  
  TEveTrackList * trackList = dynamic_cast<TEveTrackList*>(fTrackLists->FirstChild());
  if(trackList) SetUpTrackPropagator(trackList->GetPropagator(),-0.1*esd->GetMagneticField(), 520);
  for (Int_t iter = 0; iter < esd->GetNumberOfTracks(); ++iter) {

   AliESDtrack * esdTrack = dynamic_cast<AliESDtrack*>(esd->GetTrack(iter));
   FillTrackList(esdTrack);
   
  }
  
  //BALLE hardcoded size
  for(Int_t il = 0; il < fNTrackBins; il++) {
    trackList = dynamic_cast<TEveTrackList*>(fTrackLists->FindChild(Form("Tracks_%d", il)));
    if(trackList && fTrackcount[il])
      trackList->MakeTracks();
  }
  
     // -- Load geometry
   geomGentleTPC();
   
   // -- Set camera position
   setCamera(-0.2, 1.1);
  
   saveViewer(outdir);
}
///__________________________________________________________________________
void DrawESD::FillTrackList(AliESDtrack * esdTrack) {
  //See header file for documentation
  TEveTrackList * trackList = dynamic_cast<TEveTrackList*>(fTrackLists->FirstChild());
  if (!trackList) return;
  
  AliEveTrack* track = dynamic_cast<AliEveTrack*>(MakeEsdTrack(esdTrack, trackList));        
  Int_t bin = GetColorBin(esdTrack->Pt());
  trackList = dynamic_cast<TEveTrackList*>(fTrackLists->FindChild(Form("Tracks_%d", bin)));
  if(trackList) {
    track->SetAttLineAttMarker(trackList);
    trackList->AddElement(track);
    ++fTrackcount[bin];
  } else cout << "BALLE"<<endl;


}



AliEveTrack* DrawESD::MakeEsdTrack (AliESDtrack *at, TEveTrackList* cont) {
  //See header file for documentation
  
    



  const double kCLight = 0.000299792458;
  double bz = - kCLight*10.*( fMagField->GetFieldD(0,0,0).fZ);

  Bool_t innerTaken = kFALSE;
  if ( ! at->IsOn(AliESDtrack::kITSrefit) && fUseIpOnFailedITS)
  {
    //tp = at->GetInnerParam();
    innerTaken = kTRUE;
  }

  // Add inner/outer track parameters as path-marks.

  Double_t     pbuf[3], vbuf[3];

  AliExternalTrackParam trackParam = *at;

  // take parameters constrained to vertex (if they are)

  if( at->GetConstrainedParam() ){
    trackParam = *at->GetConstrainedParam();
  }
  else if( at->GetInnerParam() ){
    float x = at->GetX();
    float y = at->GetY();
    if(sqrt(x*x+y*y)>.5 ) trackParam = *(at->GetInnerParam());
  }
  if( at->GetStatus()&AliESDtrack::kTRDin ){
    // transport to TRD in
    trackParam = *at;
    trackParam.PropagateTo( 290.45, -10.*( fMagField->GetFieldD(0,0,0).fZ) );
  }

  TEveRecTrack rt;
  {
    rt.fLabel  = at->GetLabel();
    rt.fIndex  = (Int_t) at->GetID();
    rt.fStatus = (Int_t) at->GetStatus();
    rt.fSign   = (Int_t) trackParam.GetSign();  
    trackParam.GetXYZ(vbuf);
    trackParam.GetPxPyPz(pbuf);    
    rt.fV.Set(vbuf);
    rt.fP.Set(pbuf);
    Double_t ep = at->GetP(), mc = at->GetMass();
    rt.fBeta = ep/TMath::Sqrt(ep*ep + mc*mc);
  }

  AliEveTrack* track = new AliEveTrack(&rt, cont->GetPropagator());
  track->SetName(Form("AliEveTrack %d", at->GetID()));
  track->SetElementTitle(CreateTrackTitle(at));
  track->SetSourceObject(at);


  // Set reference points along the trajectory
  // and the last point

  { 
    TEvePathMark startPoint(TEvePathMark::kReference);
    trackParam.GetXYZ(vbuf);
    trackParam.GetPxPyPz(pbuf);    
    startPoint.fV.Set(vbuf);
    startPoint.fP.Set(pbuf);
    rt.fV.Set(vbuf);
    rt.fP.Set(pbuf);
    Double_t ep = at->GetP(), mc = at->GetMass();
    rt.fBeta = ep/TMath::Sqrt(ep*ep + mc*mc);

    track->AddPathMark( startPoint );    
  }

  bool ok = 1;
  if( at->GetOuterParam() && at->GetTPCPoints(2)>80 ){

    //
    // use AliHLTTPCCATrackParam propagator 
    // since AliExternalTrackParam:PropagateTo()
    // has an offset at big distances
    //
    double rot = at->GetOuterParam()->GetAlpha() - trackParam.GetAlpha();
    double crot = cos(rot), srot = sin(rot);
    double xEnd = at->GetTPCPoints(2)*crot -  at->GetTPCPoints(3)*srot;
  // take parameters constrained to vertex (if they are)
 

    AliHLTTPCCATrackParam t;
    AliHLTTPCCATrackConvertor::SetExtParam( t, trackParam );
    
    Double_t x0 = trackParam.GetX();
    Double_t dx = xEnd - x0;
    
    if( dx<0 ) ok = 0;
    //
    // set a reference at the half of trajectory for better drawing
    //
    
    for( double dxx=dx/2; ok && TMath::Abs(dxx)>=1.; dxx*=.9 ){

      if( !t.TransportToX(x0+dxx, bz, .999 ) ){
	ok = 0;
	break;
      }
      AliExternalTrackParam tt;
      AliHLTTPCCATrackConvertor::GetExtParam( t, tt, trackParam.GetAlpha() ); 
      tt.GetXYZ(vbuf);
      tt.GetPxPyPz(pbuf);
      TEvePathMark midPoint(TEvePathMark::kReference);
      midPoint.fV.Set(vbuf);
      midPoint.fP.Set(pbuf);    
      track->AddPathMark( midPoint );
      break;
    }
 
   
    //
    // Set a reference at the end of the trajectory
    // and a "decay point", to let the event display know where the track ends
    //
    
    for( ; ok; dx*=.9 ){

      if( !t.TransportToX(x0+dx, bz, .999 ) ){
	ok = 0; 
	break;
	if( TMath::Abs(dx)<1. ) break;      
	continue;
      }
      break;
    }

    {
      if( !ok ){ 
	AliHLTTPCCATrackConvertor::SetExtParam( t, trackParam );
      }
      AliExternalTrackParam tt;
      AliHLTTPCCATrackConvertor::GetExtParam( t, tt, trackParam.GetAlpha() ); 
      tt.GetXYZ(vbuf);
      tt.GetPxPyPz(pbuf);
      TEvePathMark endPoint(TEvePathMark::kReference);
      TEvePathMark decPoint(TEvePathMark::kDecay);
      endPoint.fV.Set(vbuf);
      endPoint.fP.Set(pbuf);
      decPoint.fV.Set(vbuf);
      decPoint.fP.Set(pbuf);
      track->AddPathMark( endPoint );
      track->AddPathMark( decPoint );
    }  
  }

  if ( ok &&  at->IsOn(AliESDtrack::kTPCrefit))
  {
    if ( ! innerTaken)
    {
      AddTrackParamToTrack(track, at->GetInnerParam());
    }
    AddTrackParamToTrack(track, at->GetOuterParam());
  }
  
  return track;
}

void DrawESD::SetUpTrackPropagator(TEveTrackPropagator* trkProp, Float_t magF, Float_t maxR) {
  //See header file for documentation

  if(! fMagField) {
    if (fTrueField) {
      fMagField = new AliEveMagField;
    } else {
      fMagField = new TEveMagFieldConst(0,0,magF);
    }
  }
  trkProp->SetMagFieldObj(fMagField);
 
  if (fUseRkStepper) {
    trkProp->SetStepper(TEveTrackPropagator::kRungeKutta);
  }

  trkProp->SetMaxR(maxR);
}


void DrawESD::AddTrackParamToTrack(AliEveTrack* track, const AliExternalTrackParam* tp) {
  //See header file for documentation

  if (tp == 0)
    return;

  Double_t pbuf[3], vbuf[3];
  tp->GetXYZ(vbuf);
  tp->GetPxPyPz(pbuf);

  TEvePathMark pm(TEvePathMark::kReference);
  pm.fV.Set(vbuf);
  pm.fP.Set(pbuf);
  track->AddPathMark(pm);
}



TString DrawESD::CreateTrackTitle(AliESDtrack* t) {
  // Add additional track parameters as a path-mark to track.

  TString s;

  Int_t label = t->GetLabel(), index = t->GetID();
  TString idx(index == kMinInt ? "<undef>" : Form("%d", index));
  TString lbl(label == kMinInt ? "<undef>" : Form("%d", label));

  Double_t p[3], v[3];
  t->GetXYZ(v);
  t->GetPxPyPz(p);
  Double_t pt    = t->Pt();
  Double_t ptsig = TMath::Sqrt(t->GetSigma1Pt2());
  Double_t ptsq  = pt*pt;
  Double_t ptm   = pt / (1.0 + pt*ptsig);
  Double_t ptM   = pt / (1.0 - pt*ptsig);

  s = Form("Index=%s, Label=%s\nChg=%d, Pdg=%d\n"
	   "pT = %.3f + %.3f - %.3f [%.3f]\n"
           "P  = (%.3f, %.3f, %.3f)\n"
           "V  = (%.3f, %.3f, %.3f)\n",
	   idx.Data(), lbl.Data(), t->Charge(), 0,
	   pt, ptM - pt, pt - ptm, ptsig*ptsq,
           p[0], p[1], p[2],
           v[0], v[1], v[2]);

  Int_t   o;
  s += "Det (in,out,refit,pid):\n";
  o  = AliESDtrack::kITSin;
  s += Form("ITS (%d,%d,%d,%d)  ",  t->IsOn(o), t->IsOn(o<<1), t->IsOn(o<<2), t->IsOn(o<<3));
  o  = AliESDtrack::kTPCin;
  s += Form("TPC(%d,%d,%d,%d)\n",   t->IsOn(o), t->IsOn(o<<1), t->IsOn(o<<2), t->IsOn(o<<3));
  o  = AliESDtrack::kTRDin;
  s += Form("TRD(%d,%d,%d,%d) ",    t->IsOn(o), t->IsOn(o<<1), t->IsOn(o<<2), t->IsOn(o<<3));
  o  = AliESDtrack::kTOFin;
  s += Form("TOF(%d,%d,%d,%d)\n",   t->IsOn(o), t->IsOn(o<<1), t->IsOn(o<<2), t->IsOn(o<<3));
  o  = AliESDtrack::kHMPIDout;
  s += Form("HMPID(out=%d,pid=%d)\n", t->IsOn(o), t->IsOn(o<<1));
  s += Form("ESD pid=%d", t->IsOn(AliESDtrack::kESDpid));

  if (t->IsOn(AliESDtrack::kESDpid))
  {
    Double_t pid[5];
    t->GetESDpid(pid);
    s += Form("\n[%.2f %.2f %.2f %.2f %.2f]", pid[0], pid[1], pid[2], pid[3], pid[4]);
  }

  return s;
}


