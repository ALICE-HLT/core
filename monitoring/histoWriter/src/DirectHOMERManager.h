//-*- Mode: C++ -*-
#ifndef DIRECTHOMERMANAGER_H
#define DIRECTHOMERMANAGER_H


#include "AliHLTHOMERReader.h"
#include "TString.h"
#include "TList.h"
#include <list>


class DirectHOMERManager {
public:

  DirectHOMERManager(int argc, char** argv);  
  ~DirectHOMERManager();

  TList* GetNextBlocks();

private:
  DirectHOMERManager();

  TString MakeOrigin(homer_uint32 origin);
  TString MakeDataType(homer_uint64 dataType);


  typedef std::pair<TString, unsigned short> connectionID;
  std::list<connectionID> fconnectionList;

  TList* fBlockList;

};

#endif //DIRECTHOMERMANAGER_H
