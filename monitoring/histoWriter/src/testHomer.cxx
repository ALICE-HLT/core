#include "DirectHOMERManager.h"
#include "AliHLTHOMERBlockDesc.h"
#include <iostream>

int main(int argc, char** argv){

  DirectHOMERManager man(argc, argv);
  TIter next(man.GetNextBlocks());
  AliHLTHOMERBlockDesc* block = NULL;
  while ((block = (AliHLTHOMERBlockDesc*)next())) {
    std::cout << block->GetDetector().Data() << " " 
	      << block->GetDataType().Data() << " " 
	      << std::hex << "0x" << block->GetSpecification() << std::dec;
    TObject* o=NULL;
    if( (o = block->GetTObject()) ) 
      std::cout << " " << o->ClassName();
    std::cout << std::endl;
  }
}
