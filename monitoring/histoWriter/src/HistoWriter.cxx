//-*- Mode: C++ -*-
// $Id: CorrHistoWriter.C  $

#include "TSystem.h"
#include "TCanvas.h"
#include "TCollection.h"
#include "TList.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TProfile.h"
#include "AliHLTHOMERBlockDesc.h"
#include "DirectHOMERManager.h"
#include "AliHLTHOMERReader.h"
#include "AliESDEvent.h"
#include "AliCDBEntry.h"
#include "AliHLTMiscImplementation.h"
#include "TFile.h"
#include "TStyle.h"
#include "TVirtualPad.h"
#include "TTimeStamp.h"
#include "AliHLTTriggerCounters.h"
#include <fstream>
#include <vector>

using namespace std;


//in HistoWriterTRD.cxx:
void plotTRDHistos(TObjArray* o, const char* dataDir, Int_t runnumber);

void AddHistogramToCanvas(TH1 *hist, TCanvas *canvas, Int_t &cdCount, Bool_t logscale = true, Bool_t zoom = false);
TH1* FindHistogram(TCollection* coll, const char* name);
Int_t MakeDirs(Int_t runnumber);
void HandleEMCA(AliHLTHOMERBlockDesc* block, Int_t runnumber, bool writeRootFiles = true);
void HandlePHOS(AliHLTHOMERBlockDesc* block, Int_t runnumber, bool writeRootFiles = true);
void HandleTRD(AliHLTHOMERBlockDesc* block, Int_t runnumber, bool writeRootFiles = true);
void HandleTriggerCounter(AliHLTHOMERBlockDesc* block, Int_t runnumber, bool writeRootFiles = true);
//void HandleMUON(AliHLTHOMERBlockDesc* block, Int_t runnumber, bool writeRootFiles = true);
void HandleHLT(TList* histos, Int_t runnumber, bool writeRootFiles = true);
void HandleTPC(TList* histos, Int_t runnumber, bool writeRootFiles = true);
//void HandleISPD(AliHLTHOMERBlockDesc* block, Int_t runnumber, bool writeRootFiles = true);
void HandleCorrelations(TList* corrRest, Int_t runnumber, bool writeRootFiles = true);
void WriteRootFile(AliHLTHOMERBlockDesc* block, Int_t runnumber);
void WriteRootFile(TObject* obj, const char* detector, const char* dataType, Int_t runnumber);
void MakePng(TList* histos, const char* description, const char* title, Int_t runnumber, Bool_t logscale = true);
void MakePng(TList* histos, const char* description, const char* title, Int_t runnumber, vector<Bool_t>& logscale);

static const char* dataDir = "data/NEW";
static const char* streamerInfoFile = "data/StreamerInfo/HLT_StreamerInfo.root";

void InitStreamerInfo() {
  TFile *file = TFile::Open(streamerInfoFile);
  if(file) {
    AliCDBEntry* entry=dynamic_cast<AliCDBEntry*>(file->Get("AliCDBEntry"));
    if(entry) {
      TObjArray* schemas = dynamic_cast<TObjArray*>(entry->GetObject());
      if(schemas) {
	std::cout << "Initializing streamer infos..." << std::endl;
	AliHLTMiscImplementation misc;
	misc.InitStreamerInfos(schemas);
	file->Close();
	return;
      }
    }
  }
  std::cerr << "No additional StreamerInfo available, please check file: \""
	    << streamerInfoFile << "\"" << std::endl;
  if (file) file->Close();
  return;
}

Int_t HistoWriter(bool writeRootFiles, int argc, char** argv) {
  
  InitStreamerInfo();
    Int_t iResult = 0;

    DirectHOMERManager hM(argc, argv);


    TList* blocks = hM.GetNextBlocks();

    

    AliESDEvent* esd = NULL;

    Int_t runnumber = 0;
    ULong_t eventid = 0;
    if ( ! (blocks == 0 || blocks->IsEmpty()) ){
      TIter next(blocks);
      AliHLTHOMERBlockDesc* block = NULL;
      while ((block = (AliHLTHOMERBlockDesc*)next())) {
	if ( ! block->GetDetector().CompareTo("HLT") ) {
	  // -- ESDTREE
	  if ( ! block->GetDataType().CompareTo("ALIESDV0") ) {
	    esd = dynamic_cast<AliESDEvent*> ( block->GetTObject() );
	    break;
	  }
	  else {
	    std::cout << "Found block with DataType: " << block->GetDataType().Data() << std::endl;
	  }
	}
      }
    } else {
      std::cout << "No blocks found, exiting." << std::endl;
      return -1;
    }

    if(esd){
      esd->GetStdContent();
      runnumber = esd->GetRunNumber();
      UShort_t bx = esd->GetBunchCrossNumber();
      UInt_t orbit = esd->GetOrbitNumber();
      UInt_t period = esd->GetPeriodNumber();
      eventid = bx | (orbit << 12) | ((ULong_t)period << 36);   
    }
    else {
      printf("No ESD found, cannot determine RunNumber!!\n");
    }
    
    Int_t result = MakeDirs(runnumber);
    if( result ){
      return result;
    }
    /*
    if( esd && writeRootFiles ){
      TString detector = "HLT";
      TString dataType;
      dataType.Form("ESD_0x%lx", eventid);
      WriteRootFile(esd, detector.Data(), dataType.Data(), runnumber);
    }
    */
    //    TList* corrFirst = NULL;
    TList* corrRest = new TList();
    TList* hltGlobal = new TList();
    TList* tpcGlobal = new TList();

    if ( ! (blocks == NULL || blocks->IsEmpty()) ){
      TIter next(blocks);
      AliHLTHOMERBlockDesc* block = NULL;

      while ((block = (AliHLTHOMERBlockDesc*)next())) {
	std::cout << "Next block: Detector \"" << block->GetDetector()
		  << "\", Data Type \"" << block->GetDataType() << "\"" << std::endl;
	if (!block->GetDetector().CompareTo("EMCA")){
	  HandleEMCA(block, runnumber, writeRootFiles);
	  continue;
	}
	if (!block->GetDetector().CompareTo("PHOS")) {
	  HandlePHOS(block, runnumber, writeRootFiles);
	  continue;
	}
	if (!block->GetDetector().CompareTo("TRD")) {
	  HandleTRD(block, runnumber, writeRootFiles);
	  continue;
	}
	/*
	if (!block->GetDetector().CompareTo("ISPD")) {
	  HandleISPD(block, runnumber, writeRootFiles);
	  continue;
	}
	if (!block->GetDetector().CompareTo("MUON")) {
	  HandleMUON(block, runnumber, writeRootFiles);
	  continue;
	}
	*/
	if (!block->GetDetector().CompareTo("TPC")) {
	  tpcGlobal->Add(block->GetTObject());
	  continue;
	}
	
	if (!block->GetDataType().CompareTo("ROOTHIST")) {
	  hltGlobal->Add(block->GetTObject());
	  continue;
	}
	TObject* bObj = block->GetTObject();
	if (! bObj){
	  if(!block->GetDataType().CompareTo("METADATA")) {
	    AliHLTEventTriggerData* etd = (AliHLTEventTriggerData*) block->GetData();
	    std::cout << "CDH Length: " << etd->fCommonHeaderWordCnt << std::endl;
	    for (unsigned int i=0; i<8; ++i) {
		std::cout << "CDH word " << i << ": 0x" << std::hex << (unsigned long)etd->fCommonHeader[i] << std::dec << std::endl;
	    }
	  }
	  else if(!block->GetDataType().CompareTo("ECSPARAM")) {
	    //ULong_t blength = block->GetSize();
	    //char* buf=new char[blength];
	    //memcpy(buf, block->GetData(), blength );
	    std::cout << "ECS Parameters:" << std::endl;
	    std::cout << (const char*)(block->GetData()) << std::endl;
	    //std::cout << buf << std::endl;
	    //delete [] buf;
	  }
	  else
	    std::cerr << "Cannot handle this..." << std::endl;
	  continue;
	}
	TString name = bObj->GetName();
	if (! name.CompareTo("MultiplicityCorrelations") || ! name.CompareTo("TList")){
	  //	  if(!corrFirst)
	  //	    corrFirst=dynamic_cast<TList*>(block->GetTObject());
	  //	  else
	  corrRest->Add(dynamic_cast<TList*>(block->GetTObject()));
	  continue;
	}
	//}
	if ( (! block->GetDataType().CompareTo("INTRGCNT")) 
	     || (! block->GetDataType().CompareTo("OTTRGCNT")) ) {
	  HandleTriggerCounter(block, runnumber, writeRootFiles);
	  continue;
	}
	std::cerr << "Cannot handle this: " << std::endl
		  << "Detector: " << block->GetDetector().Data() << std::endl
		  << "DataType: " << block->GetDataType().Data() << std::endl
		  << "Name: " << block->GetTObject()->GetName()  << std::endl;
	WriteRootFile(block, runnumber);
      }
      if(corrRest)
	HandleCorrelations(corrRest, runnumber, writeRootFiles);
      if(hltGlobal->GetSize())
	HandleHLT(hltGlobal, runnumber, writeRootFiles);
      if(tpcGlobal->GetSize())
	HandleTPC(tpcGlobal, runnumber, writeRootFiles);
    }
    delete corrRest;
    delete hltGlobal;
    delete tpcGlobal;
    

    return iResult;
}

Int_t MakeDirs(Int_t runnumber){
  TString dir;
  dir.Form("%s/%d", dataDir, runnumber);
  const char* pwd=gSystem->pwd();
  if(gSystem->cd(dir.Data())){
    // directory exists
    gSystem->cd(pwd);
    return 0;
  }
  Int_t result = 0;
  result = gSystem->mkdir( dir.Data() );
  if( result ) {
    std::cerr << "Cannot create \"" << dir.Data() << "\"" << std::endl;
  }
  return result;

}

void AddHistogramToCanvas(TH1 *hist, TCanvas *canvas, Int_t &cdCount, Bool_t logscale, Bool_t zoom)
{
  if (hist) {
    TPad * pad = dynamic_cast<TPad*>(canvas->cd(cdCount));
    TH2 *h2;
    if ( (h2 = dynamic_cast<TH2*>(hist)) ) {
      if(logscale)
	pad->SetLogz();
      if (zoom) {
	h2->GetXaxis()->SetRange(0, h2->GetMaximumBin() + h2->GetMaximumBin()*0.2);
	h2->GetYaxis()->SetRange(0, h2->GetMaximumBin() + h2->GetMaximumBin()*0.2);
      }
      h2->Draw("COLZ");
      return;
    }
    TH1 *h;
    if ( (h = dynamic_cast<TH1*>(hist)) ) {
      if(logscale)
	pad->SetLogy();
      if (zoom){
	h->GetXaxis()->SetRange(0, h->GetMaximumBin() + h->GetMaximumBin()*0.2);
      }
      h->Draw();
      return;
    }
  }
}


TH1* FindHistogram(TCollection* coll, const char* name)
{
    TH1 *hist = dynamic_cast<TH1*>(coll->FindObject(name));
    if(! hist)
      std::cerr << "Cannot find " << name << " in Collection " << coll->GetName() << std::endl;
    return hist;
}

void WriteRootFile(AliHLTHOMERBlockDesc* block, Int_t runnumber){
  WriteRootFile(block->GetTObject(),
		block->GetDetector().Data(), 
		block->GetDataType().Data(),
		runnumber);
}

void WriteRootFile(TObject* obj, const char* detector, 
		   const char* dataType, Int_t runnumber){
  TString filename;
  filename.Form("%s/%d/%s_%s_%s.root", dataDir, runnumber,
		detector, dataType, obj->GetName());
  std::cout << "Creating \"" << filename.Data() << "\"." << std::endl;
  TFile *f = TFile::Open(filename, "RECREATE");
  obj->Write();
  f->Close();
}

void HandleEMCA(AliHLTHOMERBlockDesc* block, Int_t runnumber, bool writeRootFiles) {
  if(writeRootFiles)
    WriteRootFile(block, runnumber);

  TObjArray* obj = dynamic_cast<TObjArray*>(block->GetTObject());
  TList* histos = new TList();
  /*
  histos->Add(FindHistogram(obj, "EMCAL fHistClusterEnergy"));
  histos->Add(FindHistogram(obj, "EMCAL_fHistMatchedEnergy"));
  histos->Add(FindHistogram(obj, "EMCAL_fHistUnMatchedEnergy"));
  histos->Add(FindHistogram(obj, "EMCAL fHistClusterEnergyVsNCells"));
  histos->Add(FindHistogram(obj, "EMCAL fHistClusterEnergyDepositedEtaPhi"));
  histos->Add(FindHistogram(obj, "EMCAL_fHistdXYdZ"));

  MakePng(histos, "EMCALQA", "EMCAL QA", runnumber);
  histos->Clear();

  histos->Add(FindHistogram(obj, "EMCAL fHistTwoClusterInvMass1"));
  histos->Add(FindHistogram(obj, "EMCAL fHistTwoClusterInvMass2"));
  histos->Add(FindHistogram(obj, "EMCAL fHistTwoClusterInvMass3"));
  histos->Add(FindHistogram(obj, "EMCAL fHistTwoClusterInvMass4"));

  MakePng(histos, "EMCALIM", "EMCAL IM", runnumber);
  histos->Clear();
  */
  vector<Bool_t> logscale(5,false);
  histos->Add(FindHistogram(obj, "hClusterEne"));
  logscale[0]=true;
  histos->Add(FindHistogram(obj, "hClusterEneVsTime"));
  histos->Add(FindHistogram(obj, "hClusterCells"));
  logscale[2]=true;
  histos->Add(FindHistogram(obj, "hClusterEneCells"));
  histos->Add(FindHistogram(obj, "hClusterEtaVsPhi"));

  MakePng(histos, "EMCALQA", "EMCAL QA", runnumber,logscale);
  histos->Clear();

  delete histos;
}

void HandlePHOS(AliHLTHOMERBlockDesc* block, Int_t runnumber, bool writeRootFiles) {
  if(writeRootFiles)
    WriteRootFile(block, runnumber);

  TObjArray* obj = dynamic_cast<TObjArray*>(block->GetTObject());
  TList* histos = new TList();

  histos->Add(FindHistogram(obj, "PHOS fHistClusterEnergy"));
  histos->Add(FindHistogram(obj, "PHOS_fHistMatchedEnergy"));
  histos->Add(FindHistogram(obj, "PHOS_fHistUnMatchedEnergy"));
  histos->Add(FindHistogram(obj, "PHOS fHistClusterEnergyVsNCells"));
  histos->Add(FindHistogram(obj, "PHOS fHistClusterEnergyDepositedEtaPhi"));
  histos->Add(FindHistogram(obj, "PHOS_fHistdXYdZ"));

  MakePng(histos, "PHOSQA", "PHOS QA", runnumber);
  histos->Clear();

  histos->Add(FindHistogram(obj, "PHOS fHistTwoClusterInvMass1"));
  histos->Add(FindHistogram(obj, "PHOS fHistTwoClusterInvMass2"));
  histos->Add(FindHistogram(obj, "PHOS fHistTwoClusterInvMass3"));
  histos->Add(FindHistogram(obj, "PHOS fHistTwoClusterInvMass4"));

  MakePng(histos, "PHOSIM", "PHOS IM", runnumber);
  histos->Clear();
  delete histos;
}

void HandleTRD(AliHLTHOMERBlockDesc* block, Int_t runnumber, bool writeRootFiles) {
  if(writeRootFiles)
    WriteRootFile(block, runnumber);

  TObjArray* obj = dynamic_cast<TObjArray*>(block->GetTObject());

  plotTRDHistos(obj, dataDir, runnumber);

  /*
  TList* histos = new TList();

  histos->Add(FindHistogram(obj, "hist_tracklets_y"));
  histos->Add(FindHistogram(obj, "hist_tracklets_dy"));
  histos->Add(FindHistogram(obj, "hist_tracklets_z"));
  histos->Add(FindHistogram(obj, "hist_tracklets_pid"));
  histos->Add(FindHistogram(obj, "hist_tracklets_y_dy"));
  histos->Add(FindHistogram(obj, "hist_tracklets_hc"));
  histos->Add(FindHistogram(obj, "hist_tracklets_bad_y"));
  histos->Add(FindHistogram(obj, "hist_tracklets_bad_pid"));

  MakePng(histos, "TRDTRACKLETS", "TRD Tracklets", runnumber, false);
  histos->Clear();

  histos->Add(FindHistogram(obj, "hist_first_tracklet_time"));
  histos->Add(FindHistogram(obj, "hist_last_tracklet_time"));
  histos->Add(FindHistogram(obj, "hist_tmu_time"));
  histos->Add(FindHistogram(obj, "hist_smu_time"));

  MakePng(histos, "TRDTIMING", "TRD Timing", runnumber, false);
  histos->Clear();

  histos->Add(FindHistogram(obj, "hist_tracks_pt"));
  histos->Add(FindHistogram(obj, "hist_tracks_pid"));
  histos->Add(FindHistogram(obj, "hist_tracks_stack"));
  histos->Add(FindHistogram(obj, "hist_tracklets_track"));
  histos->Add(FindHistogram(obj, "hist_tracklets_track_hpt"));
  histos->Add(FindHistogram(obj, "hist_trigger_contribs"));

  MakePng(histos, "TRDTRACKS", "TRD Tracks", runnumber, false);
  histos->Clear();

  delete histos;
  */
}


void HandleTriggerCounter(AliHLTHOMERBlockDesc* block, Int_t runnumber, bool writeRootFiles) {
  if(writeRootFiles)
    WriteRootFile(block, runnumber);
  TString str;
  AliHLTTriggerCounters* tcounter = static_cast<AliHLTTriggerCounters*>(block->GetTObject());
  for (UInt_t i = 0; i < tcounter->NumberOfCounters(); ++i) {
    AliHLTTriggerCounters::AliCounter& counter = tcounter->GetCounterN(i) ;
    //counter->Description();
    str += Form("<tr><td>%s</td><td>%llu</td><td>%f</td><td>%s</td></tr>\n", counter.Name(), counter.Counter(), counter.Rate(), counter.Description()  );
  }
  TString filename = Form("%s/%d/%s.txt", dataDir, runnumber, block->GetDataType().Data());
  fstream f;
  f.open(filename.Data(), fstream::out | fstream::trunc);
  if (f.is_open())
    f << str.Data();
  else
    cerr << "Cannot open file \"" << filename.Data() << "\"" << endl;
  f.close();
}
/*
void HandleMUON(AliHLTHOMERBlockDesc* block, Int_t runnumber, bool writeRootFiles) {
  if(writeRootFiles)
    WriteRootFile(block, runnumber);
  TH1F* obj = dynamic_cast<TH1F*>(block->GetTObject());
  TList* histos = new TList();
  histos->Add(obj);
  MakePng(histos, "MUONClust", "MUON Number of Clusters", runnumber);
  histos->Clear();
  delete histos;
}

void HandleISPD(AliHLTHOMERBlockDesc* block, Int_t runnumber, bool writeRootFiles) {
  if(writeRootFiles)
    WriteRootFile(block, runnumber);
}
*/
void HandleHLT(TList* hists, Int_t runnumber, bool writeRootFiles) {
  TIter histoIter(hists);
  TH1* hist=NULL;
  std::map<std::string, TList*> m;
  while( (hist = (TH1*)(histoIter.Next()))  ){
    std::string s=hist->GetName();
    if(! m[s] )
      m[s]= new TList();
    m[s]->Add(hist);
  }
  std::map<std::string, TH1*> res;
  std::map<std::string, TList*>::iterator iter;
  for(iter=m.begin(); iter != m.end(); ++iter){
    TList* l = iter->second;
    TH1* h = (TH1*) (l->First());
    l->RemoveFirst(); 
    if(l->GetSize()>0){
      TTimeStamp t1;
      Long64_t result=h->Merge(l);
      TTimeStamp t2;
      std::cout << std::string("Merging time of ") + h->GetName() + ": " << t2.GetSec() - t1.GetSec() << ". Entries: " << result << std::endl;
    }
    res[h->GetName()]=h;
    if(writeRootFiles){
      WriteRootFile(h, "HLT", "ROOTHIST", runnumber);
      if(l->GetSize()>0){
	l->SetName(h->GetName());
	WriteRootFile(l, "HLT", "LIST", runnumber);
      }
    }
    delete l;
  }

  TList* histos = new TList();
  vector<Bool_t> logscaleV(6,false);
  histos->Add(res["VertexX"]);
  logscaleV[0]=true;
  histos->Add(res["VertexY"]);
  logscaleV[1]=true;
  histos->Add(res["VertexZ"]);
  logscaleV[2]=true;
  histos->Add(res["VertexTrendX"]);
  histos->Add(res["VertexTrendY"]);
  histos->Add(res["VertexXY"]);

  MakePng(histos, "Vertex", "Primary Vertex Distribution", runnumber, logscaleV);
  histos->Clear();
  
  histos->Add(res["pt1square"]);
  histos->Add(res["pt2"]);
  histos->Add(res["UPC"]);

  MakePng(histos, "Others", "Others", runnumber, false);
  histos->Clear();

  vector<Bool_t> logscaleT(9,false);
  histos->Add(res["TrackCharge"]);
  histos->Add(res["TrackDCAr"]);
  logscaleT[1]=true;
  histos->Add(res["TrackEta"]);
  histos->Add(res["TrackMultiplicity"]);
  if( res.find("TrackNclusters") != res.end() )
    histos->Add(res["TrackNclusters"]);
  else
    histos->Add(res["TrackTPCclus"]);
  histos->Add(res["TrackITSclus"]);
  histos->Add(res["TrackPhi"]);
  histos->Add(res["TrackPt"]);
  logscaleT[7]=true;
  histos->Add(res["TrackTheta"]);

  MakePng(histos, "TPCQA", "TPC QA", runnumber, logscaleT);
  histos->Clear();

  vector<Bool_t> logscaleM(6,false);
  histos->Add(res["TrackMultiplicity"]);
  histos->Add(res["TrackMultiplicityPrimary"]);
  histos->Add(res["TrackMultiplicityBackground"]);
  TH2* tmp=NULL;
  if((tmp=dynamic_cast<TH2*>(res["TrackMultiplicityTrend"])))
    histos->Add(tmp->ProfileX("TM_pfx"));
  if((tmp=dynamic_cast<TH2*>(res["TrackMultiplicityPrimaryTrend"])))
    histos->Add(tmp->ProfileX("TMP_pfx"));
  if((tmp=dynamic_cast<TH2*>(res["TrackMultiplicityBackgroundTrend"])))
    histos->Add(tmp->ProfileX("TMB_pfx"));


  MakePng(histos, "TPCTrend", "TPC Trend", runnumber, logscaleM);
  histos->Clear();

  histos->Add(res["fNumberOfClusters"]);
  histos->Add(res["fChargePerClusterBending"]);

  MakePng(histos, "MUONClust", "MUON Number of Clusters", runnumber, true);
  histos->Clear();

  histos->Add(res["spdVertexXY"]);
  histos->Add(res["spdVertexZ"]);
  histos->Add(res["spdVertexX"]);
  histos->Add(res["spdVertexY"]);

  MakePng(histos, "SPDVertex", "SPD Vertex Distribution", runnumber, false);
  histos->Clear();

  histos->Add(res["hHLT"]);

  MakePng(histos, "TPCdEdx", "TPC dE/dx", runnumber, false);
  histos->Clear();

  
  delete histos;

}

void HandleTPC(TList* hists, Int_t runnumber, bool writeRootFiles) {
  TIter histoIter(hists);
  TH1* hist=NULL;
  std::map<std::string, TList*> m;
  while( (hist = (TH1*)(histoIter.Next()))  ){
    std::string s=hist->GetName();
    if(! m[s] )
      m[s]= new TList();
    m[s]->Add(hist);
  }
  std::map<std::string, TH1*> res;
  std::map<std::string, TList*>::iterator iter;
  for(iter=m.begin(); iter != m.end(); ++iter){
    TList* l = iter->second;
    TH1* h = (TH1*) (l->First());
    l->RemoveFirst(); 
    if(l->GetSize()>0){
      TTimeStamp t1;
      Long64_t result=h->Merge(l);
      TTimeStamp t2;
      std::cout << std::string("Merging time of ") + h->GetName() + ": " << t2.GetSec() - t1.GetSec() << ". Entries: " << result << std::endl;
    }
    res[h->GetName()]=h;
    if(writeRootFiles){
      WriteRootFile(h, "TPC", "ROOTHIST", runnumber);
      if(l->GetSize()>0){
	l->SetName(h->GetName());
	WriteRootFile(l, "TPC", "LIST", runnumber);
      }
    }
    delete l;
  }
  
  TList* histos = new TList();
  vector<Bool_t> logscale(7);
  histos->Add(res["pad"]);
  logscale[0]=false;
  histos->Add(res["padrow"]);
  if(res["padrow"]) res["padrow"]->SetMinimum(0);
  logscale[1]=false;
  histos->Add(res["timebin"]);
  logscale[2]=false;
  histos->Add(res["charge"]);
  logscale[3]=true;
  histos->Add(res["qmax"]);
  logscale[4]=true;
  histos->Add(res["sigmaY2"]);
  logscale[5]=true;
  histos->Add(res["sigmaZ2"]);
  logscale[6]=true;

  MakePng(histos, "TPCcomp1", "TPC Compression QA (1)", runnumber, logscale);
  histos->Clear();

  histos->Add(res["d_pad"]);
  histos->Add(res["d_padrow"]);
  histos->Add(res["d_time"]);
  histos->Add(res["d_charge"]);
  histos->Add(res["d_qmax"]);
  histos->Add(res["d_sigmaY2"]);
  histos->Add(res["d_sigmaZ2"]);

  MakePng(histos, "TPCcomp2", "TPC Compression QA (2)", runnumber, logscale);
  histos->Clear();

  histos->Add(res["qmaxsector"]);
  histos->Add(res["sigmaY2sector"]);
  histos->Add(res["sigmaZ2sector"]);
  histos->Add(res["qmaxsector_pfx"]);
  histos->Add(res["sigmaY2sector_pfx"]);
  histos->Add(res["sigmaZ2sector_pfx"]);

  MakePng(histos, "TPCcomp3", "TPC Compression QA (3)", runnumber, false);
  histos->Clear();

  /*
  histos->Add(res["HWCFDataSize"]);
  histos->Add(res["HWCFReductionFactor"]);
  histos->Add(res["NofClusters"]);
  */



  histos->Add(res["TotalReductionFactor"]);
  histos->Add(res["ResError"]);
  MakePng(histos, "TPCcomp4", "TPC Compression QA (4)", runnumber, false);
  histos->Clear();


  histos->Add(res["HWCFReductionFactor"]);
  histos->Add(res["ReductionFactorVsNofClusters"]);
  histos->Add(res["HWCFDataSize"]);
  histos->Add(res["NofClusters"]);
  MakePng(histos, "TPCcomp5", "TPC Compression QA (5)", runnumber, false);
  histos->Clear();

  /*
  TH3* padrowpadsector = dynamic_cast<TH3*>(res["padrowpadsector"]);
  Int_t firstBin = padrowpadsector->GetXaxis()->GetFirst();
  Int_t lastBin = padrowpadsector->GetXaxis()->GetLast();
  if(padrowpadsector){
    for (Int_t i=firstBin; i <= lastBin; ++i){
      padrowpadsector->GetXaxis()->SetRange(i,i);
      TH2D* sector = dynamic_cast<TH2D*>(padrowpadsector->Project3D("zy"));
      if(sector) {
	sector->SetName(Form("Sector %d",i));
	histos->Add(sector);
      }
    }
  }
  */
  /*
  for(Int_t i=1; i<=72; ++i){
    TH2* padrowpadsector = dynamic_cast<TH2*>(res[Form("padrowpadsector_zy_%d", i)]);
    if(padrowpadsector){
      padrowpadsector->SetName(Form("Sector %d",i-1));
      histos->Add(padrowpadsector);
    }
  }
  */
  
  histos->Add(res["XYA"]);
  histos->Add(res["XYC"]);

  MakePng(histos, "TPCcomp6", "TPC Compression QA (6)", runnumber, false);
  histos->Clear();

  
  delete histos;
  
}


void HandleCorrelations(TList* corrRest, Int_t runnumber, bool writeRootFiles) {
  /*
  if(corrRest->GetSize()>0){
    TTimeStamp t1;
    corrFirst->Merge(corrRest);
    TTimeStamp t2;
    std::cout << "Merging time: " << t2.GetSec() - t1.GetSec() << "." << std::endl;
  }
  if(writeRootFiles) {
    WriteRootFile(corrFirst, "HLT", "ROOTTOBJ", runnumber);
  }
  */
  TList* corrFirst = new TList();

  TIter iter(corrRest);
  TObject* next=NULL;
  while( (next=iter.Next()) ){
    TList* corr = dynamic_cast<TList*>(next);
    if(corr){
      TIter i(corr);
      TObject* n=NULL;
      while( (n=i.Next()) ){
	TH1* h = dynamic_cast<TH1*>(n);
	if(h && h->GetEntries()>0){
	  corrFirst->Add(n);
	}
      }
    } 
  }
  if(corrFirst->GetEntries()==0){
    delete corrFirst;
    return;
  }
  corrFirst->SetName("MultiplicityCorrelations");
  WriteRootFile(corrFirst, "HLT", "ROOTTOBJ", runnumber);
  TList* histos = new TList();

  histos->Add(FindHistogram(corrFirst, "fVzeroMult"));
  histos->Add(FindHistogram(corrFirst, "fVzeroMultAC"));
  histos->Add(FindHistogram(corrFirst, "fVzeroTriggerMult"));
  histos->Add(FindHistogram(corrFirst, "fVzeroTriggerMultAC"));
  MakePng(histos, "V0Mult", "V0 Multiplicities", runnumber);
  histos->Clear();

  histos->Add(FindHistogram(corrFirst, "fZdcEzdc"));
  histos->Add(FindHistogram(corrFirst, "fZdcEzdcAEzdcC"));
  histos->Add(FindHistogram(corrFirst, "fZdcEzemEzdc"));
  MakePng(histos, "ZDCMult", "ZDC Multiplicities", runnumber);
  histos->Clear();

  histos->Add(FindHistogram(corrFirst, "fTpcNch2"));
  histos->Add(FindHistogram(corrFirst, "fTpcNch3"));
  histos->Add(FindHistogram(corrFirst, "fSpdNClusters"));
  histos->Add(FindHistogram(corrFirst, "fSpdNClustersInner"));
  histos->Add(FindHistogram(corrFirst, "fSpdNClustersOuter"));
  MakePng(histos, "TrackMult", "Track Multiplicities", runnumber);
  histos->Clear();

  histos->Add(FindHistogram(corrFirst, "fCorrEzdcTpc"));
  histos->Add(FindHistogram(corrFirst, "fCorrEzdcVzero"));
  histos->Add(FindHistogram(corrFirst, "fCorrEzdcTriggerVzero"));
  histos->Add(FindHistogram(corrFirst, "fCorrVzeroTpc"));
  histos->Add(FindHistogram(corrFirst, "fCorrTriggerVzeroTpc"));
  histos->Add(FindHistogram(corrFirst, "fCorrSpdOuterTpc"));

  MakePng(histos, "MultCorr", "Multiplicity Correlations", runnumber);
  histos->Clear();
  /*
  histos->Add(FindHistogram(corrFirst, "fCorrZdcTotEvsPhosTotEt"));
  histos->Add(FindHistogram(corrFirst, "fCorrZdcTotEvsEmcalTotEt"));
  histos->Add(FindHistogram(corrFirst, "fCorrZdcTotEvsTotEt"));
  histos->Add(FindHistogram(corrFirst, "fCorrVzerovsPhosTotEt"));
  histos->Add(FindHistogram(corrFirst, "fCorrVzerovsEmcalTotEt"));
  histos->Add(FindHistogram(corrFirst, "fCorrVzerovsTotEt"));
  MakePng(histos, "ETCorr", "ET Correlations", runnumber);
  histos->Clear();
  */
  
  histos->Add(FindHistogram(corrFirst, "fCorrVzeroSpdOuter"));
  histos->Add(FindHistogram(corrFirst, "fCorrVzeroTriggerSpdOuter"));
  histos->Add(FindHistogram(corrFirst, "fCorrEzdcSpdOuter"));
  MakePng(histos, "ZDCV0vsSPD", "ZDC/V0 vs. SPD", runnumber);
  histos->Clear();
  
  histos->Add(FindHistogram(corrFirst, "fVzeroCentV0M"));
  histos->Add(FindHistogram(corrFirst, "fVzeroCentV0M_minbias"));
  histos->Add(FindHistogram(corrFirst, "fVzeroCentV0M_semicentral"));
  histos->Add(FindHistogram(corrFirst, "fVzeroCentV0M_central"));
  MakePng(histos, "CentralityV0M", "Centrality V0M", runnumber);
  histos->Clear();

  delete corrFirst;
  delete histos;
}

void MakePng(TList* histos, const char* description, const char* title, Int_t runnumber, vector<Bool_t>& logscale) {
  TCanvas *canvas = new TCanvas(description, title);
  canvas->SetCanvasSize(1600, 900);
  Int_t size = histos->GetSize();
  Int_t a = 1, b = 1;
  while(a*b < size) {
    if(a<=b)
      ++a;
    else
      ++b;
  }
  canvas->Divide(a,b);
  Int_t histoCnt=0;
  for(Int_t cd = 1; cd <= size; ++cd){
    TH1* hist = dynamic_cast<TH1*>(histos->At(cd-1));
    if(hist && hist->GetEntries() > 0.){
      AddHistogramToCanvas(hist, canvas, cd, logscale[cd-1]);
      ++histoCnt;
    }
  }
  if(histoCnt){
    canvas->Update();
    TString filename;
    filename.Form("%s/%d/%s.png", dataDir, runnumber, description);
    std::cout << "Creating \"" << filename.Data() << "\"." << std::endl;
    canvas->Print(filename.Data(), "png");
  }
  delete canvas;
}

void MakePng(TList* histos, const char* description, const char* title, Int_t runnumber, Bool_t logscale) {
  TCanvas *canvas = new TCanvas(description, title);
  canvas->SetCanvasSize(1600, 900);
  Int_t size = histos->GetSize();
  Int_t a = 1, b = 1;
  while(a*b < size) {
    if(a<=b)
      ++a;
    else
      ++b;
  }
  canvas->Divide(a,b);
  Int_t histoCnt=0;
  for(Int_t cd = 1; cd <= size; ++cd){
    TH1* hist = dynamic_cast<TH1*>(histos->At(cd-1));
    if(hist && hist->GetEntries() > 0.){
      AddHistogramToCanvas(hist, canvas, cd, logscale);
      ++histoCnt;
    }
  }
  if(histoCnt){
    canvas->Update();
    TString filename;
    filename.Form("%s/%d/%s.png", dataDir, runnumber, description);
    std::cout << "Creating \"" << filename.Data() << "\"." << std::endl;
    canvas->Print(filename.Data(), "png");
  }
  delete canvas;
}
