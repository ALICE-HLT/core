#include "TSystem.h"
#include "TCanvas.h"
#include "TCollection.h"
#include "TList.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include "TStyle.h"
#include "TVirtualPad.h"
#include "TLegend.h"
#include "TObjArray.h"
#include "TGraph.h"
#include <fstream>

using namespace std;


//  plot.C
//
//  Generate some plots from the HLT TRD component output
// provided as-is from Felix
// integrated in HistoWriter without caring about memory leaks
// from non-freed objects and the like.
// Needs major cleanup at some point.

// Some ugly globals 
static int divs_x=1;
static int divs_y=1;

static Float_t pgTrackingTimeout=5.65;
//static Color_t line_colors[5] = {kBlue, kGreen-2, kMagenta, kAzure-3, kViolet-1};

static TObjArray* f;


// The pad seems not to tell it's division setting, thus rember it externally
void set_pad_divs(int div_x, int div_y){
  gPad->Divide(div_x, div_y);
  gPad->Update();
  divs_x = div_x;
  divs_y = div_y;
}

//
// Plot: tracklet y-positions
//
void plot_tracklet_y(TCanvas *c, int div_x=-1, int div_y=-1){

  if ((div_x >= 0) && (div_y >= 0))
    c->cd(div_x + div_y*divs_x+1);
  else
    c->cd(0);
  TLegend* legend = new TLegend(0.63, 0.75, 0.885, 0.88, "");

  TH1I *fHistTrackletY = (TH1I*)f->FindObject("hist_tracklets_y");
  legend->SetTextSize(0.04);
  gStyle->SetTitleSize(0.07, "t"); 
  gPad->SetLogy(kTRUE);
  if (fHistTrackletY){
    fHistTrackletY->SetTitle("Y position of online trackles");
    fHistTrackletY->GetYaxis()->SetTitle("Count"); 
    fHistTrackletY->GetXaxis()->SetTitle("Online tracklet y-position [160um bin]"); 
    fHistTrackletY->SetStats(kTRUE);
    // fHistTrackletY->SetAxisRange(-4096, 4096, "X");
    fHistTrackletY->SetLineColor(kBlack);
    fHistTrackletY->SetLineStyle(1);
    fHistTrackletY->Draw("");
    legend->AddEntry(fHistTrackletY, "All pt", "l");
  } else
    printf("hist_tracklets_y not found!\n");

  TGraph *g_left = new TGraph();  // outer border of layer 5
  g_left->SetPoint(0, -3681., 0.);
  g_left->SetPoint(1, -3681., 100000000000.);
  g_left->SetLineStyle(7);
  g_left->Draw();

  TGraph *g_left2 = new TGraph();  // outer border of layer 0
  g_left2->SetPoint(0, -2988., 0.);
  g_left2->SetPoint(1, -2988., 100000000000.);
  g_left2->SetLineStyle(7);
  g_left2->Draw();

  TGraph *g_right = new TGraph();
  g_right->SetPoint(0, +3681., 0.);
  g_right->SetPoint(1, +3681., 100000000000.);
  g_right->SetLineStyle(7);
  g_right->Draw();

  TGraph *g_right2 = new TGraph();
  g_right2->SetPoint(0, +2988., 0.);
  g_right2->SetPoint(1, +2988., 100000000000.);
  g_right2->SetLineStyle(7);
  g_right2->Draw();


  // legend->Draw();
  gPad->Update();
  c->Update();
}

//
// Plot: tracklet deflections
//
void plot_tracklet_dy(TCanvas *c, int div_x=-1, int div_y=-1){

  if ((div_x >= 0) && (div_y >= 0))
    c->cd(div_x + div_y*divs_x+1);
  else
    c->cd(0);
  TLegend* legend = new TLegend(0.63, 0.75, 0.885, 0.88, "");

  TH1I *fHistTrackletDy = (TH1I*)f->FindObject("hist_tracklets_dy");
  legend->SetTextSize(0.04);
  gStyle->SetTitleSize(0.07, "t");
  gPad->SetLogy(kFALSE);
  if (fHistTrackletDy){
    fHistTrackletDy->SetTitle("Deflection of online trackles");
    fHistTrackletDy->GetYaxis()->SetTitle("Count");
    fHistTrackletDy->GetXaxis()->SetTitle("Online tracklet deflection [140um bin]");
    fHistTrackletDy->SetStats(kTRUE);
    fHistTrackletDy->SetLineColor(kBlack);
    fHistTrackletDy->SetLineStyle(1);
    fHistTrackletDy->Draw("");
    legend->AddEntry(fHistTrackletDy, "All pt", "l");
  } else
    printf("hist_tracklets_dy not found!\n");

  TGraph *g_left = new TGraph();
  g_left->SetPoint(0, 0., 0.);
  g_left->SetPoint(1, 0., 100000000000.);
  g_left->SetLineStyle(7);
  g_left->Draw();

  gPad->Update();
  c->Update();
}

//
// Plot: tracklet z-positions
//
void plot_tracklet_z(TCanvas *c, int div_x=-1, int div_y=-1){

  if ((div_x >= 0) && (div_y >= 0))
    c->cd(div_x + div_y*divs_x+1);
  else
    c->cd(0);
  TLegend* legend = new TLegend(0.63, 0.75, 0.885, 0.88, "");

  TH1I *fHistTrackletZ = (TH1I*)f->FindObject("hist_tracklets_z");
  legend->SetTextSize(0.04);
  gStyle->SetTitleSize(0.07, "t");
  gPad->SetLogy(kFALSE);
  if (fHistTrackletZ){
    fHistTrackletZ->SetTitle("Z position of online trackles");
    fHistTrackletZ->GetYaxis()->SetTitle("Count");
    fHistTrackletZ->GetXaxis()->SetTitle("z");
    fHistTrackletZ->SetStats(kTRUE);
    fHistTrackletZ->SetLineColor(kBlack);
    fHistTrackletZ->SetLineStyle(1);
    fHistTrackletZ->Draw("");
    legend->AddEntry(fHistTrackletZ, "All pt", "l");
  } else
    printf("hist_tracklets_z not found!\n");

  TGraph *g_left = new TGraph();
  g_left->SetPoint(0, 0., 0.);
  g_left->SetPoint(1, 0., 100000000000.);
  g_left->SetLineStyle(7);
  g_left->Draw();

  //  legend->Draw();                                                                                                                                         
  gPad->Update();
  c->Update();
}

//
// Plot: tracklet PID values
//
void plot_tracklet_pid(TCanvas *c, int div_x=-1, int div_y=-1){

  if ((div_x >= 0) && (div_y >= 0))
    c->cd(div_x + div_y*divs_x+1);
  else
    c->cd(0);
  TLegend* legend = new TLegend(0.63, 0.75, 0.885, 0.88, "");

  TH1I *fHistTrackletPID = (TH1I*)f->FindObject("hist_tracklets_pid");
  legend->SetTextSize(0.04);
  gStyle->SetTitleSize(0.07, "t");
  gPad->SetLogy(kTRUE);
  if (fHistTrackletPID){
    fHistTrackletPID->SetTitle("PID value of online trackles");
    fHistTrackletPID->GetYaxis()->SetTitle("Count");
    fHistTrackletPID->GetXaxis()->SetTitle("PID");
    fHistTrackletPID->SetStats(kTRUE);
    fHistTrackletPID->SetLineColor(kBlack);
    fHistTrackletPID->SetLineStyle(1);
    fHistTrackletPID->Draw("");
    //legend->AddEntry(fHistTrackletZ, "All pt", "l");
  } else
    printf("hist_tracklets_pid not found!\n");

  //  legend->Draw();                                                                                                                                                               gPad->Update();
  c->Update();
}


//
// Plot: Tracklet and tracking timing
//
void plot_tracklet_tracking_timing(TCanvas *c, int div_x=-1, int div_y=-1){

  if ((div_x >= 0) && (div_y >= 0))
    c->cd(div_x + div_y*divs_x+1);
  else
    c->cd(0);
  TLegend* legend = new TLegend(0.15, 0.65, 0.4, 0.88, "");

  TH1F *fHistLastTrackletTime = (TH1F*)f->FindObject("hist_last_tracklet_time");
  TH1F *fHistFirstTrackletTime = (TH1F*)f->FindObject("hist_first_tracklet_time");
  TH1F *fHistTmuTime = (TH1F*)f->FindObject("hist_tmu_time");
  TH1F *fHistSmuTime = (TH1F*)f->FindObject("hist_smu_time");

  legend->SetTextSize(0.04);
  gStyle->SetTitleSize(0.07, "t");
  gPad->SetLogy(kTRUE);

  if (fHistLastTrackletTime){
    fHistLastTrackletTime->SetTitle("Tracklet and tracking timing");
    fHistLastTrackletTime->GetYaxis()->SetTitle("Count");
    fHistLastTrackletTime->SetAxisRange(3., 6., "X");
    fHistLastTrackletTime->GetXaxis()->SetTitle("Time after L0 [us]");
    fHistLastTrackletTime->SetStats(kFALSE);
    fHistLastTrackletTime->SetLineColor(kBlue);
    fHistLastTrackletTime->SetLineStyle(1);
    fHistLastTrackletTime->Draw("");
    legend->AddEntry(fHistLastTrackletTime, "Last tracklet", "l");
  } else
    printf("hist_last_tracklet_time not found!\n");

  if (fHistFirstTrackletTime){
    fHistFirstTrackletTime->SetStats(kFALSE);
    fHistFirstTrackletTime->SetLineColor(kGreen+2);
    fHistFirstTrackletTime->SetLineStyle(1);
    fHistFirstTrackletTime->Draw("same");
    legend->AddEntry(fHistFirstTrackletTime, "First tracklet", "l");
  } else
    printf("hist_first_tracklet_time not found!\n");

  if (fHistTmuTime){
    fHistTmuTime->SetStats(kFALSE);
    fHistTmuTime->SetLineColor(kViolet);
    fHistTmuTime->SetLineStyle(1);
    fHistTmuTime->Draw("same");
    legend->AddEntry(fHistTmuTime, "TMU tracking done", "l");
  } else
    printf("hist_tmu_time not found!\n");

  if (fHistSmuTime){
    fHistSmuTime->SetStats(kFALSE);
    fHistSmuTime->SetLineColor(kRed);
    fHistSmuTime->SetLineStyle(1);
    fHistSmuTime->Draw("same");
    legend->AddEntry(fHistSmuTime, "SMU tracking done", "l");
  } else
    printf("hist_smu_time not found!\n");


  TGraph *g_timeout = new TGraph();
  g_timeout->SetPoint(0, pgTrackingTimeout, 0.);
  g_timeout->SetPoint(1, pgTrackingTimeout, 100000000000.);
  g_timeout->SetLineColor(kRed);
  g_timeout->SetLineStyle(7);
  g_timeout->Draw();

  legend->Draw();                                                                                                                                                              
  gPad->Update();
  c->Update();
}

//
// Plot: tracklet numbers by half-chamber
//
void plot_tracklet_hc(TCanvas *c, int div_x=-1, int div_y=-1){

  if ((div_x >= 0) && (div_y >= 0))
    c->cd(div_x + div_y*divs_x+1);
  else
    c->cd(0);

  TH2I *fHistTrackletHC = (TH2I*) f->FindObject("hist_tracklets_hc");
  gStyle->SetTitleSize(0.07, "t"); 
  c->SetLogy(kFALSE);
  if (fHistTrackletHC){
    fHistTrackletHC->SetStats(kTRUE);
    fHistTrackletHC->SetTitle("Online tracklet numbers per half-chamber");
    fHistTrackletHC->GetXaxis()->SetTitle("Sector"); 
    fHistTrackletHC->GetYaxis()->SetTitle("Halfchamber"); 
    fHistTrackletHC->GetXaxis()->SetBit(TAxis::kCenterLabels);
    // fHistTrackletHC->GetYaxis()->SetBit(TAxis::kCenterLabels);
    fHistTrackletHC->GetXaxis()->SetNdivisions(18);
    //    fHistTrackletHC->GetYaxis()->SetNdivisions(6);
    fHistTrackletHC->Draw("COLZ");
  }

  TGraph *g_grid[5] = {0};
  for (Int_t ticks=1; ticks<5; ticks++){
    g_grid[ticks] = new TGraph(); 
    g_grid[ticks]->SetPoint(0, 0., 12*ticks);
    g_grid[ticks]->SetPoint(1, 18, 12*ticks);
    g_grid[ticks]->SetLineColor(kBlack);
    g_grid[ticks]->SetLineStyle(1);
    g_grid[ticks]->Draw();
  }

  TGraph *g_grid2[18] = {0};
  for (Int_t ticks=1; ticks<17; ticks++){
    g_grid2[ticks] = new TGraph();
    g_grid2[ticks]->SetPoint(0, ticks, 0.);
    g_grid2[ticks]->SetPoint(1, ticks, 60.);
    g_grid2[ticks]->SetLineColor(kBlack);
    g_grid2[ticks]->SetLineStyle(1);
    g_grid2[ticks]->Draw();
  }

  gPad->Update();
  c->Update();

}

//
// Plot: tracklets with bad y-position by stack
//
void plot_tracklet_bad_y(TCanvas *c, int div_x=-1, int div_y=-1){

  if ((div_x >= 0) && (div_y >= 0))
    c->cd(div_x + div_y*divs_x+1);
  else
    c->cd(0);

  TH2I *fHistTrackletBadY = (TH2I*) f->FindObject("hist_tracklets_bad_y");
  gStyle->SetTitleSize(0.07, "t");
  c->SetLogy(kFALSE);
  c->SetLogz(kTRUE);
  if (fHistTrackletBadY){
    fHistTrackletBadY->SetStats(kTRUE);
    fHistTrackletBadY->SetTitle("Online tracklet numbers with invalid y-position per stack");
    fHistTrackletBadY->GetXaxis()->SetTitle("Sector");
    fHistTrackletBadY->GetYaxis()->SetTitle("Stack");
    fHistTrackletBadY->GetXaxis()->SetBit(TAxis::kCenterLabels);
    fHistTrackletBadY->GetYaxis()->SetBit(TAxis::kCenterLabels);                                                                                                       
    fHistTrackletBadY->GetXaxis()->SetNdivisions(18);
    fHistTrackletBadY->GetYaxis()->SetNdivisions(5);
    fHistTrackletBadY->Draw("COLZ");
  }

  gPad->Update();
  c->Update();

}

//
// Plot: tracklets with bad PID value by stack
void plot_tracklet_bad_pid(TCanvas *c, int div_x=-1, int div_y=-1){

  if ((div_x >= 0) && (div_y >= 0))
    c->cd(div_x + div_y*divs_x+1);
  else
    c->cd(0);

  TH2I *fHistTrackletBadPID = (TH2I*) f->FindObject("hist_tracklets_bad_pid");
  gStyle->SetTitleSize(0.07, "t");
  c->SetLogy(kFALSE);
  c->SetLogz(kTRUE);
  if (fHistTrackletBadPID){
    fHistTrackletBadPID->SetStats(kTRUE);
    fHistTrackletBadPID->SetTitle("Online tracklet numbers with invalid PID value per stack");
    fHistTrackletBadPID->GetXaxis()->SetTitle("Sector");
    fHistTrackletBadPID->GetYaxis()->SetTitle("Stack");
    fHistTrackletBadPID->GetXaxis()->SetBit(TAxis::kCenterLabels);
    fHistTrackletBadPID->GetYaxis()->SetBit(TAxis::kCenterLabels);
    fHistTrackletBadPID->GetXaxis()->SetNdivisions(18);
    fHistTrackletBadPID->GetYaxis()->SetNdivisions(5);
    fHistTrackletBadPID->Draw("COLZ");
  }
  gPad->Update();
  c->Update();
}

//
// Plot: GTU track pt spectrum

void plot_track_pt(TCanvas *c, int div_x=-1, int div_y=-1){

  if ((div_x >= 0) && (div_y >= 0))
    c->cd(div_x + div_y*divs_x+1);
  else
    c->cd(0);
  TLegend* legend = new TLegend(0.63, 0.75, 0.885, 0.88, "");

  TH1F *fHistTrackPt = (TH1F*)f->FindObject("hist_tracks_pt");
  legend->SetTextSize(0.04);
  gStyle->SetTitleSize(0.07, "t");
  gPad->SetLogy(kTRUE);
  if (fHistTrackPt){
    fHistTrackPt->SetTitle("Track p_t");
    fHistTrackPt->GetYaxis()->SetTitle("Count");
    fHistTrackPt->GetXaxis()->SetTitle("p_t [GeV/c]");
    fHistTrackPt->SetStats(kTRUE);
    fHistTrackPt->SetLineColor(kBlack);
    fHistTrackPt->SetLineStyle(1);
    fHistTrackPt->Draw("");
    legend->AddEntry(fHistTrackPt, "GTU tracks", "l");
  } else
    printf("hist_tracks_pt not found!\n");

  gPad->Update();
  c->Update();
}

//
// Plot: track PID
//
void plot_track_pid(TCanvas *c, int div_x=-1, int div_y=-1){

  if ((div_x >= 0) && (div_y >= 0))
    c->cd(div_x + div_y*divs_x+1);
  else
    c->cd(0);
  TLegend* legend = new TLegend(0.3, 0.75, 0.885, 0.88, "");

  TH1I *fHistTrackPID = (TH1I*)f->FindObject("hist_tracks_pid");
  TH1I *fHistTrackletsPID = (TH1I*)f->FindObject("hist_tracklets_pid");
  legend->SetTextSize(0.04);
  gStyle->SetTitleSize(0.07, "t");
  gPad->SetLogy(kTRUE);
  if (fHistTrackletsPID){
    fHistTrackletsPID->SetTitle("PID value of tracklets and GTU tracks");
    fHistTrackletsPID->GetYaxis()->SetTitle("Count");
    fHistTrackletsPID->GetXaxis()->SetTitle("PID");
    fHistTrackletsPID->SetStats(kTRUE);
    fHistTrackletsPID->SetLineColor(kBlack);
    fHistTrackletsPID->SetLineStyle(1);
    fHistTrackletsPID->Draw("");
    legend->AddEntry(fHistTrackletsPID, "tracklets", "l");
  } else
    printf("hist_tracklets_pid not found!\n");
  if (fHistTrackPID){
    fHistTrackPID->SetLineColor(kRed);
    fHistTrackPID->SetLineStyle(1);
    fHistTrackPID->Draw("same");
    legend->AddEntry(fHistTrackPID, "GTU tracks", "l");
  } else
    printf("hist_tracklets_pid not found!\n");

  legend->Draw();
  gPad->Update();                                                                                                                                            
  c->Update();
}

// 
// Plot: Tracks per stack
//
void plot_tracks_stack(TCanvas *c, int div_x=-1, int div_y=-1){

  if ((div_x >= 0) && (div_y >= 0))
    c->cd(div_x + div_y*divs_x+1);
  else
    c->cd(0);

  TH2I *fHistTracksStack = (TH2I*) f->FindObject("hist_tracks_stack");
  gStyle->SetTitleSize(0.07, "t");
  c->SetLogy(kFALSE);
  if (fHistTracksStack){
    fHistTracksStack->SetStats(kTRUE);
    fHistTracksStack->SetTitle("Tracks per Stack");
    fHistTracksStack->GetXaxis()->SetTitle("Sector");
    fHistTracksStack->GetYaxis()->SetTitle("Stack");
    fHistTracksStack->GetXaxis()->SetBit(TAxis::kCenterLabels); 
    fHistTracksStack->GetYaxis()->SetBit(TAxis::kCenterLabels); 
    fHistTracksStack->GetXaxis()->SetNdivisions(18); 
    fHistTracksStack->GetYaxis()->SetNdivisions(5); 
    fHistTracksStack->Draw("COLZ");
  }

  gPad->Update();
  c->Update();

}

//
// Plot: tracklets per track
//
void plot_tracklets_track(TCanvas *c, int div_x=-1, int div_y=-1){

  if ((div_x >= 0) && (div_y >= 0))
    c->cd(div_x + div_y*divs_x+1);
  else
    c->cd(0);
  TLegend* legend = new TLegend(0.2, 0.6, 0.5, 0.75, "");

  TH1I *fHistTrackletsTrack = (TH1I*)f->FindObject("hist_tracklets_track");
  legend->SetTextSize(0.04);
  gStyle->SetTitleSize(0.07, "t");
  gPad->SetLogy(kTRUE);
  if (fHistTrackletsTrack){
    fHistTrackletsTrack->SetTitle("Tracklets per GTU track");
    fHistTrackletsTrack->GetYaxis()->SetTitle("Count");
    fHistTrackletsTrack->GetXaxis()->SetTitle("Number of contributing layers");
    fHistTrackletsTrack->GetXaxis()->SetBit(TAxis::kCenterLabels);
    fHistTrackletsTrack->SetMinimum(1);
    fHistTrackletsTrack->SetStats(kTRUE);
    fHistTrackletsTrack->SetLineColor(kBlack);
    fHistTrackletsTrack->SetLineStyle(1);
    fHistTrackletsTrack->Draw("");
    legend->AddEntry(fHistTrackletsTrack, "All pt", "l");
  } else
    printf("hist_tracklets_track not found!\n");

  TH1I *fHistTrackletsTrackHpt = (TH1I*)f->FindObject("hist_tracklets_track_hpt");
  if (fHistTrackletsTrackHpt){
    fHistTrackletsTrackHpt->SetLineColor(kRed);
    fHistTrackletsTrackHpt->SetLineStyle(1);
    fHistTrackletsTrackHpt->Draw("same");
    legend->AddEntry(fHistTrackletsTrackHpt, "high-pt", "l");
  } else
    printf("hist_tracklets_track_hpt not found!\n");

  legend->Draw();
  gPad->Update();
  c->Update();
}

//
// Plot: Segment-level trigger contributions
void plot_trigger_contribs_per_segment(TCanvas *c, int div_x=-1, int div_y=-1){

  if ((div_x >= 0) && (div_y >= 0))
    c->cd(div_x + div_y*divs_x+1);
  else
    c->cd(0);

  TH2I *fHistTriggerContribs = (TH2I*)f->FindObject("hist_trigger_contribs");
  gStyle->SetTitleSize(0.07, "t"); 
  c->SetLogy(kFALSE);
  c->SetLogz(kTRUE);
  if (fHistTriggerContribs){
    fHistTriggerContribs->SetTitle("Trigger contributions per sector");
    fHistTriggerContribs->GetXaxis()->SetBit(TAxis::kCenterLabels); 
    fHistTriggerContribs->GetYaxis()->SetBit(TAxis::kCenterLabels); 
    fHistTriggerContribs->GetXaxis()->SetNdivisions(18); 
    fHistTriggerContribs->GetYaxis()->SetNdivisions(12); 
    fHistTriggerContribs->GetXaxis()->SetTitle("Sector"); 
    fHistTriggerContribs->GetYaxis()->SetTitle("Contribution"); 
    fHistTriggerContribs->ResetStats();
    fHistTriggerContribs->SetStats(kTRUE);
    fHistTriggerContribs->Draw("COLZ");
  }
  gPad->Update();
  c->Update();
}

void plot_tracklets_y_dy(TCanvas *c, int div_x=-1, int div_y=-1){

  if ((div_x >= 0) && (div_y >= 0))
    c->cd(div_x + div_y*divs_x+1);
  else
    c->cd(0);

  TH2I *fHistTrackletsYDy = (TH2I*) f->FindObject("hist_tracklets_y_dy");
  gStyle->SetTitleSize(0.07, "t");
  c->SetLogz(kTRUE);
  if (fHistTrackletsYDy){
    fHistTrackletsYDy->SetStats(kTRUE);
    fHistTrackletsYDy->SetTitle("Tracklet deflection vs. y-position");
    fHistTrackletsYDy->GetXaxis()->SetTitle("y-position [160um bin]");
    fHistTrackletsYDy->GetYaxis()->SetTitle("y-deflection [140um bin]");
    fHistTrackletsYDy->Draw("COLZ");
  } else
    printf("Tracklet deflection vs. position plot not found.\n");

  gPad->Update();
  c->Update();

}


void plotTRDHistos(TObjArray* o, const char* dataDir, Int_t runnumber)
{
  /*
  // Some rendering and style related things
  gROOT->SetStyle("Plain");
  gStyle->SetPalette(1);
  gStyle->SetFillColor(-1);
  gStyle->SetFillStyle(4000); 
  gStyle->SetTitleBorderSize(0);
  gStyle->SetStatBorderSize(0);
  gStyle->SetLegendBorderSize(0);
  gStyle->SetOptStat(10);
  //  gStyle->SetPadRightMargin(0.1);
  */
  //Double_t w = 1200;
  //Double_t h = 800;
  //TCanvas* c = new TCanvas("Test", "Test", w, h);
  //TLegend *legend = 0;

  
  
  if (o) {

    f = o;
    /*
    // Single plots
    TCanvas *c_single =  new TCanvas("c_single", "Test2", 1024, 1024*16/26);
    
    plot_tracklet_y(c_single, -1, -1);    
    c_single->SaveAs("tracklet_y.pdf");

    plot_tracklet_dy(c_single, -1, -1);    
    c_single->SaveAs("tracklet_dy.pdf");

    plot_tracklet_z(c_single, -1, -1);
    c_single->SaveAs("tracklet_z.pdf");

    plot_tracklet_pid(c_single, -1, -1);
    c_single->SaveAs("tracklet_pid.pdf");

    plot_tracklets_y_dy(c_single, -1, -1);
    c_single->SaveAs("tracklet_y_dy.pdf");

    plot_tracklet_hc(c_single, -1, -1);
    c_single->SaveAs("tracklet_hc.pdf");

    plot_tracklet_bad_y(c_single, -1, -1);
    c_single->SaveAs("tracklet_bad_y.pdf");

    plot_tracklet_bad_pid(c_single, -1, -1);
    c_single->SaveAs("tracklet_bad_pid.pdf");

    plot_tracklet_tracking_timing(c_single, -1, -1);
    c_single->SaveAs("timing.pdf");

    plot_track_pt(c_single, -1, -1);
    c_single->SaveAs("track_pt.pdf");

    plot_track_pid(c_single, -1, -1);
    c_single->SaveAs("track_pid.pdf");

    plot_tracks_stack(c_single, -1, -1);
    c_single->SaveAs("tracks_stack.pdf");

    plot_tracklets_track(c_single, -1, -1);
    c_single->SaveAs("tracklets_track.pdf");

    plot_trigger_contribs_per_segment(c_single, -1, -1);
    c_single->SaveAs("trigger_contribs.pdf");
    */
    // Plot summary tables  #sss
    TCanvas *c_comp =  new TCanvas("c_comp", "Test2");//1024, 1024*16/26);
    c_comp->SetCanvasSize(1600, 900);
    TString dir;
    dir.Form("%s/%d/", dataDir, runnumber);
    TString file;
    // Control plot - tracklet properties
    c_comp->Clear();
    gPad->Clear();
    set_pad_divs(3, 2);
    plot_tracklet_y(c_comp, 0, 0);
    plot_tracklet_dy(c_comp, 1, 0);
    plot_tracklet_z(c_comp, 2, 0);
    plot_tracklet_pid(c_comp, 0, 1);
    plot_tracklets_y_dy(c_comp, 1, 1);
    plot_tracklet_hc(c_comp, 2, 1);
    file = dir + "control_tracklet_properties.png";
    c_comp->SaveAs(file.Data());

    // Control plot - tracklet anomalies
    c_comp->Clear();
    gPad->Clear();
    set_pad_divs(2, 1);
    plot_tracklet_bad_y(c_comp, 0, 0);
    plot_tracklet_bad_pid(c_comp, 1, 0);
    file = dir + "control_tracklet_anomalies.png";
    c_comp->SaveAs(file.Data());

    // Control plot - tracking
    c_comp->Clear();
    gPad->Clear();
    set_pad_divs(3, 2);
    plot_track_pt(c_comp, 0, 0);
    plot_track_pid(c_comp, 1, 0);
    plot_tracklets_track(c_comp, 2, 0);
    plot_tracklet_tracking_timing(c_comp, 0, 1);
    plot_tracks_stack(c_comp, 1, 1);
    plot_trigger_contribs_per_segment(c_comp, 2, 1);
    file = dir + "control_tracking.png";
    c_comp->SaveAs(file.Data());


  } // end file protection

}
