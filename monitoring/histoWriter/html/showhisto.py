from twisted.web.resource import Resource
import os
from os import listdir
import re

class ShowHisto(Resource):
    isLeaf = True
    def getChild(self, name, request):
        if name == '':
            return self
        return Resource.getChild(self, name, request)

    histos = [
        "SPDVertex",
        "Vertex",
        "TPCcomp1",
        "TPCcomp2",
        "TPCcomp3",
        "TPCcomp4",
        "TPCcomp5",
        "TPCcomp6",
        "TPCdEdx",
        "TPCQA",
        "TPCTrend",
        "control_tracking",
        "control_tracklet_anomalies",
        "control_tracklet_properties",
        "EMCALIM",
        "EMCALQA",
        "PHOSIM",
        "PHOSQA",
        "MUONClust",
        "MultCorr",
        "CentralityV0M",
        "TrackMult",
        "V0Mult",
        "ZDCMult",
        "ZDCV0vsSPD",
        "Others"]

    histoTitles = [ 
        "SPD Vertex Distribution",
        "Primary Vertex Distribution",
        "TPC Compression QA (1)",
        "TPC Compression QA (2)",
        "TPC Compression QA (3)",
        "TPC Compression QA (4)",
        "TPC Compression QA (5)",
        "TPC Compression QA (6)",
        "TPC dE/dx",
        "TPC QA",
        "TPC Multiplicity Trends",
        "TRD Tracking",
        "TRD Tracklet Anomalies",
        "TRD Tracklet Properties",
        "EMCAL Invariant Mass", 
        "EMCAL QA",
        "PHOS Invariant Mass",
        "PHOS QA",
        "MUON Clusters",
        "Correlations",
        "Centrality V0M",
        "Track/SPD Multiplicity",
        "VZERO Multiplicity",
        "ZDC Multiplicity",
        "ZDC/VZERO SPD Correlations",
        "Others"] 
    
    def getRuns(self):
        ret=["Current"]
        pattern=re.compile("[0-9]{6}")
        dirs=sorted(listdir("PHYSICS"),reverse=True)
        for entry in dirs:
            if pattern.match(entry):
                ret.append(entry)
        return ret

    def getRunInfo(self, run):
        runtype=None
        timestamp=""
        try:
            f=open(os.path.join('PHYSICS',run,'runtype'), 'r')
            runtype=f.read()
            f.close()
        except IOError:
            pass
        try:
            f=open(os.path.join('PHYSICS',run,'timestamp'), 'r')
            timestamp=f.read()
            f.close()
        except IOError:
            pass
        return (runtype, timestamp)

    def mkHtml(self, args):
        histooptions=""
        for i in range(0, len(self.histos)):
            histooptions=histooptions + "\t\t<option value=\"" + self.histos[i] + "\">" + self.histoTitles[i] + "</option>\n"
        runs = self.getRuns()
        runoptions=""
        for run in runs:
            runoptions=runoptions + "\t\t<option value=\"" + run + "\">" + run + "</option>\n"

        thisrun=runs[0]
        thishisto=self.histos[0]
        if "run" in args:
            thisrun=args["run"][0]
        if "histo" in args:
            thishisto=args["histo"][0]

        runIdx=0
        histoIdx=0

        if thisrun in runs:
            runIdx = runs.index(thisrun)
        else:
            runIdx=0

        runname=thisrun

        if runIdx==0:
            thisrun = runs[1]
        if thishisto in self.histos:
            histoIdx=self.histos.index(thishisto)
        else:
            thishisto=self.histos[0]
            histoIdx=0
            
        (runtype, timestamp)=self.getRunInfo(thisrun)
        thishisto= "PHYSICS/" + thisrun + "/" + thishisto + ".png"
        thishistotitle=self.histoTitles[histoIdx] + " (Run " + thisrun 
        if runtype != None:
            thishistotitle += ", " + runtype + ")"
        else:
            thishistotitle += ")"

        autoRefresh="checked"
        if "refresh" in args and args["refresh"][0] == "false":
            autoRefresh=""

        html=("""\
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
       "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

</head>

<script type="text/javascript">

function Refresh() {
  var select = window.document.getElementById('Run');
  var run=select.options[select.options.selectedIndex].value;

  select = window.document.getElementById('Histo');
  var histo=select.options[select.options.selectedIndex].value;

  var checkbox = window.document.getElementById('autoRefresh');
  var autoRefresh = checkbox.checked;

  window.location.href=window.location.pathname + '?run=' + run + '&histo=' + histo + '&refresh=' + autoRefresh ;
}

function AutoRefresh() {
  var checkbox = window.document.getElementById('autoRefresh');
  var autoRefresh = checkbox.checked;
  if(autoRefresh){
    Refresh();
  }
}

function Init(runIdx, histoIdx){
  var select = window.document.getElementById('Histo');
  select.options.selectedIndex=histoIdx;

  select = window.document.getElementById('Run');
  select.options.selectedIndex=runIdx;

  window.setTimeout("AutoRefresh()", 25000);
}

</script>

<body onload="Init(%(runIdx)s, %(histoIdx)s )">


<table border="1" cellpadding="0" cellspacing="0" >
  <colgroup>
    <col>
    <col>
    <col width="70%%">
    <col>
  </colgroup>
  <tr>
    <td valign="top" align="left">
      <p><h3>Choose Run:</h3></p>
      <p>
	<select id="Run" name="Run" onchange="Refresh()">
%(runoptions)s
	</select>
      </p>
    </td>
    <td valign="top" align="left">
      <p><h3>Choose Histogram:</h3></p>
      <p>
	<select id="Histo" name="Histo" onchange="Refresh()">
%(histooptions)s
	</select>
      </p>
    </td>
    <td>
        <h1 id="histoTitle" align="center">%(thishistotitle)s</h1>
        <h3 id="histoTitle" align="center">%(timestamp)s</h3>
    </td>
    <td>
        <input type="checkbox" id="autoRefresh" name="autoRefresh" value="autoRefresh" onchange="AutoRefresh()" %(autoRefresh)s>Refresh<br /><br />
        <!-- <a href="event?run=%(runname)s">Go to event view</a><br /> -->
        <a href="PHYSICS/%(thisrun)s">Get source root files</a>
        <a href="trend">Trends</a>
    </td>
  </tr>
  <tr>
    <td colspan="4">
        <p><img src="%(thishisto)s" alt="not available for this run" name="histo"/></p>
    </td>
  </tr>
</table>

</body>
</html>""" % vars())
        return html

    def render_GET(self, request):
        args=request.args
        return self.mkHtml(args)
