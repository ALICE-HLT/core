from twisted.web.resource import Resource
from os import listdir
import re

class ShowEvent(Resource):
    isLeaf = True
    def getChild(self, name, request):
        if name == '':
            return self
        return Resource.getChild(self, name, request)

    def getRuns(self):
        ret=["Current"]
        pattern=re.compile("[0-9]{6}")
        dirs=sorted(listdir("PHYSICS"),reverse=True)
        for entry in dirs:
            if pattern.match(entry):
                ret.append(entry)
        return ret

    def getEvent(self, run):
        ret='N/A_N/A_N/A_N/A_N/A'
        pattern=re.compile("event_.*\.png")
        dirs=listdir("PHYSICS/" + run)
        for entry in dirs:
            if pattern.match(entry):
                ret=entry 
        return ret


    def mkHtml(self, args):
        histooptions=""

        runs = self.getRuns()
        runoptions=""
        for run in runs:
            runoptions=runoptions + "\t\t<option value=\"" + run + "\">" + run + "</option>\n"

        thisrun=runs[0]

        if "run" in args:
            thisrun=args["run"][0]

        runIdx=0

        if thisrun in runs:
            runIdx = runs.index(thisrun)
        else:
            runIdx=0

        runname=thisrun

        if runIdx==0:
            thisrun = runs[1]


        event = self.getEvent(thisrun)
        info = event.split('.')[0].split('_')
        eventid=info[2]
        eventdate=info[3]
        eventtime=info[4]

        eventfile="../PHYSICS/" + thisrun + "/" + event
        title= "Run " + thisrun
        subtitle = "Event ID: " + eventid + " &nbsp;&nbsp;&nbsp; "+ eventdate + " &nbsp;&nbsp;&nbsp; " + eventtime

        autoRefresh="checked"
        if "refresh" in args and args["refresh"][0] == "false":
            autoRefresh=""

        html=("""\
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
       "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

</head>

<script type="text/javascript">

function Refresh() {
  var select = window.document.getElementById('Run');
  var run=select.options[select.options.selectedIndex].value;

  var checkbox = window.document.getElementById('autoRefresh');
  var autoRefresh = checkbox.checked;

  window.location.href=window.location.pathname + '?run=' + run + '&refresh=' + autoRefresh ;
}

function AutoRefresh() {
  var checkbox = window.document.getElementById('autoRefresh');
  var autoRefresh = checkbox.checked;
  if(autoRefresh){
    Refresh();
  }
}

function Init(runIdx){

  select = window.document.getElementById('Run');
  select.options.selectedIndex=runIdx;

  window.setTimeout("AutoRefresh()", 25000);
}

</script>

<body onload="Init(%(runIdx)s )">


<table border="1" cellpadding="0" cellspacing="0" >
  <colgroup>
    <col>
    <col width="70%%">
    <col>
  </colgroup>
  <tr>
    <td valign="top" align="left">
      <p><h3>Choose Run:</h3></p>
      <p>
	<select id="Run" name="Run" onchange="Refresh()">
%(runoptions)s
	</select>
      </p>
    </td>
    <td>
        <h1 id="Title" align="center">%(title)s</h1>
        <h2 id="SubTitle" align="center">%(subtitle)s</h2>
    </td>
    <td>
        <input type="checkbox" id="autoRefresh" name="autoRefresh" value="autoRefresh" onchange="AutoRefresh()" %(autoRefresh)s>Refresh<br /><br />
        <a href="../?run=%(runname)s">Go to histogram view</a>
    </td>
  </tr>
  <tr>
    <td colspan="3">
        <p><img src="%(eventfile)s" alt="not available for this run" name="event"/></p>
    </td>
  </tr>
</table>

</body>
</html>""" % vars())
        return html

    def render_GET(self, request):
        args=request.args
        return self.mkHtml(args)
