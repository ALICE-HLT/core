#!/usr/bin/python

import logbook

import sys
import os
import time


def mkRunFile(data, runfilename):
  datadir="data"
  #histofile="HLT_ROOTHIST_TrackMultiplicity.root"
  runs={}
  runs[runfilename]=[]
  lines=data.split('\n')
  for line in lines:
    fields=line.split(';')
    if len(fields) < 11:
      continue
    checkfile=os.path.join(datadir,"PHYSICS", fields[1])
    if os.path.exists(checkfile):
      runs[runfilename].append(fields[1])
      if not fields[19] in runs:
        runs[fields[19]]=[]
      runs[fields[19]].append(fields[1])
  for period in runs.keys():
    runfile=open(os.path.join(datadir,'trends', period + ".txt"), 'w')
    print "Writing %s" % runfile.name
    for run in sorted(runs[period]):
      runfile.write( run + '\n')
    runfile.close()


print "Fetching runs..."
sess=logbook.LogBookSession()
ret=sess.query(partition=",34", period="LHC12%,", with_beam="Yes",
               duration="5 m,,", runtype="PHYSICS")
print "...done."
#print_nice(ret)
mkRunFile(ret, "LHC12")
