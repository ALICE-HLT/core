#!/usr/bin/python

import re
import sys
import os
import time
import logbook


def moveData(run, datadir):
  fromdir=os.path.join(datadir,"NEW", run)
  todir=os.path.join(datadir,"PHYSICS", run)
  if not os.path.exists(todir):
    os.mkdir(todir)
  hfiles=os.listdir(fromdir)
  for hfile in hfiles:
    try:
      fromfile=os.path.join(fromdir, hfile)
      tofile=os.path.join(todir, hfile)
      os.rename(fromfile, tofile)
    except OSError as ose:
      print "OSError: ", ose
  
def makeRunInfo(run, datadir, runtype):
  currentTime=time.strftime("%a, %d %b %Y %H:%M:%S")
  f=open(os.path.join(datadir,"NEW", run,"timestamp"), 'w')
  f.write(currentTime)
  f.close()
  f=open(os.path.join(datadir,"NEW", run,"runtype"), 'w')
  f.write(runtype)
  f.close()

def checkDataDir(datadir):
  runs = os.listdir(os.path.join(datadir,"NEW"))
  runString = ""
  runPattern=re.compile("[0-9]{6}")
  for run in runs:
    if not runPattern.match(run):
      continue
    if len(runString) == 0:
      runString=",," + run
    else:
      runString=runString + " " + run
  if len(runString) == 0:
    print "No new runs, exiting."
    return
  print "checking logbook for runs", runString
  sess=logbook.LogBookSession()
  log=sess.query(runs=runString)
  sess.print_nice( log )
  lines=log.split('\n')
  for line in lines[1:]:
    cols=line.split(';')
    if cols[0]:
      run=cols[1]
      runType=cols[12]
      endTime=cols[4]
      if runType != 'PHYSICS' and runType != 'TECHNICAL':
        print "Skipping run", run, ", runtype ", runType
        continue
      if endTime:
        print "Skipping run", run, ", already ended at ", endTime
        continue
      print 'moving ', run, '...'
      makeRunInfo(run, datadir, runType)
      moveData(run, datadir)
      print '...done'

checkDataDir("data")

