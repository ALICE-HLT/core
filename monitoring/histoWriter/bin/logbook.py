#!/usr/bin/python

import mechanize
import urllib
import xml.sax.saxutils
import cookielib
from os.path import exists


class LogBookSession:
  #first some constants
  authargs={
    "wa" : "wsignin1.0",
    "wreply" : "https://alice-logbook.cern.ch/Shibboleth.sso/ADFS",
    "wctx" : "cookie",
    "wtrealm" : "https://alice-logbook.cern.ch/Shibboleth.sso/ADFS"
    }

  defaultargs = {
    "p_cont"     : "sb",
    "p_rspn"     : "1",
    "p_rsob"     : "l.run",
    "p_rsob_dir" : "DESC",
    "p_output"   : "TXT",
    "p_oh"       : "yes",
    "p_od"       : "2",
    "p_os"       : "1",
    "p_fields"   : "run;DAQ_time_start;DAQ_time_end;duration;partition;run_type;LHCperiod"
    }

  keyfile="certs/key.pem"
  certfile="certs/cert.pem"

  cookiefile='cookies.txt'

  def createBrowser(self):
    self.br = mechanize.Browser()
    self.cj = cookielib.LWPCookieJar()
    self.br.set_cookiejar(self.cj)
    self.br.set_handle_equiv(True)
    self.br.set_handle_redirect(True)
    self.br.set_handle_referer(True)
    self.br.set_handle_robots(False)
    # Follows refresh 0 but not hangs on refresh > 0
    self.br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)

    # Want debugging messages?
    #self.br.set_debug_http(True)
    #self.br.set_debug_redirects(True)
    #self.br.set_debug_responses(True)

    # User-Agent (this is cheating, ok?)
    self.br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]

    self.br.add_client_certificate('login.cern.ch:443', self.keyfile, self.certfile)

    #load cookies
    if exists(self.cookiefile):
      print "Loading cookies..."
      self.cj.load(self.cookiefile,ignore_discard=True, ignore_expires=True)

  def __init__(self):
    self.createBrowser()

  def getPostMsg(self, s):
    #some magic going on here, to work around a bug(?)
    p=s.split("><input")
    args={}
    entities={"&quot;" : "\""}
    the_value=""
    for i in p:
      k=i.split("\" ")
      name=""
      value=""
      for l in k:
        m=l.split("=\"")
        if "name" == m[0]:
          name=m[1]
        if "value" == m[0]:
          value=xml.sax.saxutils.unescape( m[1], entities ) 

      if name and value and name == "wresult":
        the_value=value
    return the_value

  def login(self):
    self.cj.clear()
    r = self.br.open("https://login.cern.ch/adfs/ls/auth/sslclient/?" + urllib.urlencode(self.authargs) )
    self.br.select_form(nr=0)
    ctl = self.br.find_control(name="wresult", type="hidden")
    ctl.readonly = False
    ctl.value = self.getPostMsg( r.get_data() )
    r1=self.br.submit()

  def query(self, runs=None, with_beam=None, partition=None, period=None, duration=None, runtype=None):
    queryargs=dict(self.defaultargs)
    if runs == None and period == None:
      print 'At least one of "runs" or "period" has to be specified'
      return None
    if runs != None:
      queryargs["prsf_rn"]=runs
    if with_beam != None:
      queryargs["prsf_rwb"]=with_beam
    if partition != None:
      queryargs["prsf_rpart"]=partition
    if period != None:
      queryargs["prsf_rlp"]=period
    if duration != None:
      queryargs["prsf_rdur"]=duration
    if runtype != None:
      queryargs["prsf_rtype"]=runtype
  
    #requesting data
    r=self.br.open("https://alice-logbook.cern.ch/logbook/date_online.php?" + urllib.urlencode(queryargs))
    responseType=r.info().getsubtype()
    if responseType != 'plain':
      print "Not logged in, doing it now..."
      self.login()
    #trying again
    r=self.br.open("https://alice-logbook.cern.ch/logbook/date_online.php?" + urllib.urlencode(queryargs))
    responseType=r.info().getsubtype()
    if responseType != 'plain':
      print "Still not logged in, giving up."
      return None
    print "Saving cookies..."
    self.cj.save(self.cookiefile, ignore_discard=True, ignore_expires=True)
    return r.get_data()

  def print_nice(self, data):
    lines=data.split('\n')
    for line in lines:
      fields=line.split(';')
      if len(fields) < 20:
        continue
      print "%10s | %7s | %19s | %19s | %14s | %10s | %10s | %9s" % \
          (fields[0],fields[1],fields[2],fields[4],fields[7],fields[12],fields[13],fields[19])

