/**************************************************************************
 * Copyright(c) 1998-2011, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

/**
 * \date 8 Oct 2011
 * \author Artur Szostak <artursz@iafrica.com>
 * \brief Contains the main entry point of the chainHandler process.
 */

#include <cstdlib>

int main(int argc, const char** argv)
{
	return EXIT_SUCCESS;
}
