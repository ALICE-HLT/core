#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "TQtWidget.h"
#include "TTree.h"
#include "TH1F.h"
#include "TGraph.h"
#include "TGraphAsymmErrors.h"
#include "TTimeStamp.h"
#include "TStyle.h"
#include "TDirectory.h"
#include "AliHLTTreeMonitor.h"
#include "AliHLTEventProcessor.h"
#include "AliHLTTimeFunctions.h"
#include <QTimer>
#include <QHash>
#include <QFileDialog>
#include <QRegExp>
#include <cerrno>
#include <QMessageBox>


enum
{
	kPlotUpdateInterval = 3000,  // Automatic plot update interval in milliseconds.
	kProgressBarHideWait = 500   // Number of milliseconds to wait before hiding the progress bar after completion of task.
};


MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	fStatusMessage(),
	fEventCountMessage(),
	fTimer(0),
	fNeventsOffset(0),
	fMaxEntries(1000000),
	fTree(NULL),
	fEventProcessor(NULL),
	fInhibitAutoDraw(false),
	fUserSetTimeRange(false)
{
	ui->setupUi(this);
	fStatusMessage.setAlignment(Qt::AlignLeft);
	fEventCountMessage.setAlignment(Qt::AlignRight);
	ui->fStatusBar->addWidget(&fStatusMessage, 1);
	ui->fStatusBar->addWidget(&fEventCountMessage, 1);
	ui->fProgressBar->hide();
	QList<int> sizeList;
	sizeList.push_back(240);
	sizeList.push_back(380*2);
	ui->fSplitter->setSizes(sizeList);
	sizeList.clear();
	sizeList.push_back(350);
	sizeList.push_back(200);
	ui->fSubSplitter1->setSizes(sizeList);

	// The ROOT context menu does not behave properly under Qt so suppress it completely.
	ui->fUserCanvas->setContextMenuPolicy(Qt::NoContextMenu);
	ui->fAutoCanvas->setContextMenuPolicy(Qt::NoContextMenu);
	ui->fTrendCanvas->setContextMenuPolicy(Qt::NoContextMenu);
	
	// The following is a workaround for broken rendering of TQtWidget in ROOT v5-30-04.
	// Without forcing the fill style and color the canvas will not render properly.
	// The previous rendering shows through. Note, other values than 4100 and 10 dont
	// seem to work properly.
	ui->fUserCanvas->GetCanvas()->SetFillStyle(4100);
	ui->fUserCanvas->GetCanvas()->SetFillColor(10);
	ui->fAutoCanvas->GetCanvas()->SetFillStyle(4100);
	ui->fAutoCanvas->GetCanvas()->SetFillColor(10);
	ui->fTrendCanvas->GetCanvas()->SetFillStyle(4100);
	ui->fTrendCanvas->GetCanvas()->SetFillColor(10);

	ui->fUserCanvas->GetCanvas()->SetTitle("VZERO distribution");
	ui->fUserCanvas->GetCanvas()->SetName("vzerodistrib");
	ui->fAutoCanvas->GetCanvas()->SetTitle("Automatic Canvas");
	ui->fAutoCanvas->GetCanvas()->SetName("autocanvas");
	ui->fTrendCanvas->GetCanvas()->SetTitle("Trend Canvas");
	ui->fTrendCanvas->GetCanvas()->SetName("trendcanvas");

	connect(ui->fButtonCTPSelectToggle, SIGNAL(clicked()), SLOT(ToggleSelectedCTPList()));
	connect(ui->fButtonUpdateCanvas, SIGNAL(clicked()), SLOT(DrawUserHisto()));
	connect(ui->fCheckBoxUseLastSeconds, SIGNAL(clicked()), SLOT(UpdateTimeInputStates()));
	connect(ui->fSpinBoxMinX, SIGNAL(editingFinished()), SLOT(UpdateSpinBoxMaxX()));
	connect(ui->fSpinBoxMaxX, SIGNAL(editingFinished()), SLOT(UpdateSpinBoxMinX()));
	connect(ui->fSpinBoxMinY, SIGNAL(editingFinished()), SLOT(UpdateSpinBoxMaxY()));
	connect(ui->fSpinBoxMaxY, SIGNAL(editingFinished()), SLOT(UpdateSpinBoxMinY()));
	connect(ui->fSpinBoxMinV0A, SIGNAL(editingFinished()), SLOT(UpdateSpinBoxMaxV0A()));
	connect(ui->fSpinBoxMaxV0A, SIGNAL(editingFinished()), SLOT(UpdateSpinBoxMinV0A()));
	connect(ui->fSpinBoxMinV0C, SIGNAL(editingFinished()), SLOT(UpdateSpinBoxMaxV0C()));
	connect(ui->fSpinBoxMaxV0C, SIGNAL(editingFinished()), SLOT(UpdateSpinBoxMinV0C()));
	connect(ui->fButtonSave, SIGNAL(clicked()), SLOT(SaveCanvas()));
	connect(ui->fDateTimeEditMin, SIGNAL(editingFinished()), SLOT(UserSetTimeRange()));
	connect(ui->fDateTimeEditMax, SIGNAL(editingFinished()), SLOT(UserSetTimeRange()));
	connect(ui->fButtonClear, SIGNAL(clicked()), SLOT(ClearTTree()));

	fTimer = startTimer(kPlotUpdateInterval);
}

MainWindow::~MainWindow()
{
	if (fTimer != 0) killTimer(fTimer);
	delete ui;
}


void MainWindow::timerEvent(QTimerEvent* event)
{
	CheckTimeRange();

	if (event->timerId() != fTimer) return;
	if (fTree == NULL) return;
	if (fInhibitAutoDraw) return;
	if (ui->fCheckBoxAutoUpdate->checkState() != Qt::Checked) return;

	ULong64_t ctpclasses = 0x0;
	for (int i = 0; i < ui->fCTPList->count(); ++i)
	{
		QListWidgetItem* item = ui->fCTPList->item(i);
		if (item->checkState() != Qt::Checked) continue;
		int index = item->data(Qt::UserRole).value<int>();
		ctpclasses |= (ULong64_t(0x1) << Long64_t(index));
	}

	// Use these values for limiting the automatic scatter plot:
	double minV0A = ui->fSpinBoxMinX->value();
	double maxV0A = ui->fSpinBoxMaxX->value();
	double minV0C = ui->fSpinBoxMinY->value();
	double maxV0C = ui->fSpinBoxMaxY->value();

	// First generate the auto update plot.
	gStyle->SetOptTitle(0);
	gStyle->SetTitleOffset(1.5, "Y");
	ui->fAutoCanvas->GetCanvas()->cd();
	Long64_t maxEntries = fMaxEntries;
	Long64_t firstEntry = fTree->GetEntriesFast() - maxEntries;
	if (firstEntry < 0) firstEntry = 0;
	Long64_t count = fTree->Draw(
			"V0C:V0A",
			Form(
				"%.17e < V0A && V0A < %.17e && %.17e < V0C && V0C < %.17e && (CTPtrigger & 0x%llX) != 0",
				minV0A, maxV0A, minV0C, maxV0C, ctpclasses
			    ),
			"", // Draw options
			maxEntries,
			firstEntry
		);
	fEventCountMessage.setText(Form(
			"Displaying %llu from a total %llu sampled events in the automatic canvas.",
			count,
			Long64_t(fTree->GetEntriesFast())
		));
	// Fetch the histogram and add units.
	TH1* hist = dynamic_cast<TH1*>( gPad->GetPrimitive("htemp") );
	if (hist != NULL)
	{
		TString title = hist->GetXaxis()->GetTitle();
		if (not title.Contains("(ns)")) title += " (ns)";
		hist->GetXaxis()->SetTitle(title);
		title = hist->GetYaxis()->GetTitle();
		if (not title.Contains("(ns)")) title += " (ns)";
		hist->GetYaxis()->SetTitle(title);
	}
	gPad->SetTopMargin(0.03);
	gPad->SetLeftMargin(0.13);
	gPad->SetRightMargin(0.04);
	ui->fAutoCanvas->GetCanvas()->Update();
	ui->fAutoCanvas->Refresh();

	// Now use proper values for the trend plot:
	minV0A = ui->fSpinBoxMinV0A->value();
	maxV0A = ui->fSpinBoxMaxV0A->value();
	minV0C = ui->fSpinBoxMinV0C->value();
	maxV0C = ui->fSpinBoxMaxV0C->value();

	// Now generate the trend plot
	gStyle->SetTitleOffset(0.7, "Y");
	ui->fTrendCanvas->GetCanvas()->cd();

	double period = ui->fSpinBoxIntegrationTime->value();
	int bins = ui->fSpinBoxTrendBinning->value();
	double now = AliHLT::CurrentTimestampSec();
	double max = TMath::Floor(now / period) * period;
	double min = max - bins * period;
	fTree->Draw(
			Form("time>>trendPlotTotal(%d, %.17e, %.17e)", bins, min, max),
			Form("(CTPtrigger & 0x%llX) != 0", ctpclasses),
			"goff",  // Draw options
			maxEntries,
			firstEntry
		);
	TH1F* trendPlotTotal = dynamic_cast<TH1F*>( gDirectory->Get("trendPlotTotal") );
	fTree->Draw(
			Form("time>>trendPlot(%d, %.17e, %.17e)", bins, min, max),
			Form(
				"%.17e < V0A && V0A < %.17e && %.17e < V0C && V0C < %.17e && (CTPtrigger & 0x%llX) != 0",
				minV0A, maxV0A, minV0C, maxV0C, ctpclasses
			    ),
			"goff",  // Draw options
			maxEntries,
			firstEntry
		);
	TH1* trendPlot = dynamic_cast<TH1*>( gDirectory->Get("trendPlot") );
	if (trendPlotTotal != NULL and trendPlot != NULL)
	{
		static TGraphAsymmErrors trendGraph;
		trendGraph.Divide(trendPlot, trendPlotTotal);
		if (trendGraph.GetN() > 0)
		{
			for (int i = 0; i < trendGraph.GetN(); ++i)
			{
				trendGraph.GetY()[i] *= 100;
				trendGraph.SetPointEYhigh(i, trendGraph.GetErrorYhigh(i) * 100);
				trendGraph.SetPointEYlow(i, trendGraph.GetErrorYlow(i) * 100);
			}
			trendGraph.SetMinimum(-3);
			trendGraph.SetMaximum(103);
			trendGraph.GetXaxis()->SetTimeDisplay(kTRUE);
			trendGraph.GetXaxis()->SetTimeFormat("%H:%M:%S");
			trendGraph.GetXaxis()->SetTimeOffset(TTimeStamp(0,0,0,0,0,0).AsDouble(), "local");
			trendGraph.GetXaxis()->SetLabelOffset(0.015);
			trendGraph.GetHistogram()->SetYTitle("Percentage selected %");
			trendGraph.Draw("ap");
			gPad->SetGridy();
			gPad->SetTopMargin(0.03);
			gPad->SetBottomMargin(0.075);
			gPad->SetLeftMargin(0.06);
			gPad->SetRightMargin(0.01);
			ui->fTrendCanvas->GetCanvas()->Update();
			ui->fTrendCanvas->Refresh();
		}
	}
}


void MainWindow::DrawUserHisto()
{
	CheckTimeRange();

	if (fTree == NULL) return;
	fInhibitAutoDraw = true;

	// We set the maximum value in the progress bar to 2 times nevents since we
	// will be processing the tree twice in DrawUserHisto().
	ui->fProgressBar->setMaximum(fTree->GetEntriesFast()*2);
	ui->fProgressBar->setValue(0);
	ui->fProgressBar->show();
	QApplication::processEvents(QEventLoop::ExcludeUserInputEvents);

	AliHLTTreeMonitor::ResetPublishTimer();
	AliHLTTreeMonitor::Register();  // Register to get status monitoring just for the user histogram.

	double now = TTimeStamp().AsDouble();  // Get the timestamp early in this routine since the tree copy could be long.

	// We have to work off a copy because the original can be updated in the middle
	// of our drawing, giving incorrect results.
	TTree* tree = fTree->CloneTree();

	ULong64_t ctpclasses = 0x0;
	for (int i = 0; i < ui->fCTPList->count(); ++i)
	{
		QListWidgetItem* item = ui->fCTPList->item(i);
		if (item->checkState() != Qt::Checked) continue;
		int index = item->data(Qt::UserRole).value<int>();
		ctpclasses |= (ULong64_t(0x1) << Long64_t(index));
	}

	gStyle->SetOptTitle(0);
	gStyle->SetOptStat("emr");
	gStyle->SetPalette(1);
	gStyle->SetTitleOffset(1.5, "Y");
	gStyle->SetStatX(0.85);
	gStyle->SetStatW(0.3);
	ui->fUserCanvas->GetCanvas()->cd();

	double mintime = AliHLT::QDateTimeToSec(ui->fDateTimeEditMin->dateTime());
	double maxtime = AliHLT::QDateTimeToSec(ui->fDateTimeEditMax->dateTime());
	if (ui->fCheckBoxUseLastSeconds->checkState() == Qt::Checked)
	{
		mintime = now - ui->fSpinBoxSeconds->value();
		maxtime = now +1;
	}

	int bins = ui->fSpinBoxBinning->value();
	double minX = ui->fSpinBoxMinX->value();
	double maxX = ui->fSpinBoxMaxX->value();
	double minY = ui->fSpinBoxMinY->value();
	double maxY = ui->fSpinBoxMaxY->value();
	double minV0A = ui->fSpinBoxMinV0A->value();
	double maxV0A = ui->fSpinBoxMaxV0A->value();
	double minV0C = ui->fSpinBoxMinV0C->value();
	double maxV0C = ui->fSpinBoxMaxV0C->value();

	TString histoDrawString = ui->fComboBoxPlotValue->currentText().toAscii().data();
	fNeventsOffset = 0; // First time we process the tree the offset for the progress bar update is zero.
	Long64_t total = tree->Draw(
			histoDrawString,
			Form(
				"%.17e <= time && time <= %.17e && (CTPtrigger & 0x%llX) != 0",
				mintime, maxtime, ctpclasses
			    ),
			"goff"  // Draw options.
		);

	Long64_t count = 0;
	// Second time we process the tree the fNeventsOffset should be the previous status bar value.
	fNeventsOffset = ui->fProgressBar->value();
	TH1* hist = NULL;
	if (ui->fCheckBoxScatterPlot->checkState() == Qt::Checked)
	{
		count = tree->Draw(
				histoDrawString,
				Form(
					"%.17e <= time && time <= %.17e && %.17e < V0A && V0A < %.17e && %.17e < V0C && V0C < %.17e && (CTPtrigger & 0x%llX) != 0",
					mintime, maxtime, minV0A, maxV0A, minV0C, maxV0C, ctpclasses
				    ),
				""  // Draw options.
			);
		hist = dynamic_cast<TH1*>( gPad->GetPrimitive("htemp") );
		gPad->SetTopMargin(0.03);
		gPad->SetLeftMargin(0.13);
		gPad->SetRightMargin(0.04);
	}
	else
	{
		// Check if we are dealing with a 2D or 1D histogram.
		TString option = "colz";
		if (histoDrawString.Contains(":"))
		{
			histoDrawString += Form(">>userhist(%d,%.17e,%.17e,%d,%.17e,%.17e)", bins, minX, maxX, bins, minY, maxY);
			option = "colz";
		}
		else
		{
			histoDrawString += Form(">>userhist(%d,%.17e,%.17e)", bins, minX, maxX);
			option = "";
		}

		count = tree->Draw(
				histoDrawString,
				Form(
					"%.17e <= time && time <= %.17e && %.17e < V0A && V0A < %.17e && %.17e < V0C && V0C < %.17e && (CTPtrigger & 0x%llX) != 0",
					mintime, maxtime, minV0A, maxV0A, minV0C, maxV0C, ctpclasses
				    ),
				option
			);
		gPad->SetTopMargin(0.03);
		gPad->SetLeftMargin(0.13);
		gPad->SetRightMargin(0.13);
		hist = dynamic_cast<TH1*>( gPad->GetPrimitive("userhist") );
		if (hist != NULL)
		{
			Ssiz_t pos = histoDrawString.Index(">>");
			if (pos != kNPOS) histoDrawString.Remove(pos, histoDrawString.Length() - pos);
			TObjArray* tokens = histoDrawString.Tokenize(":");
			if (tokens != NULL)
			{
				if (tokens->GetEntriesFast() == 1)
				{
					hist->SetXTitle( tokens->UncheckedAt(0)->GetName() );
				}
				else if (tokens->GetEntriesFast() > 1)
				{
					hist->SetYTitle( tokens->UncheckedAt(0)->GetName() );
					hist->SetXTitle( tokens->UncheckedAt(1)->GetName() );
				}
				if (tokens->GetEntriesFast() > 2) hist->SetZTitle( tokens->UncheckedAt(2)->GetName() );
			}
			delete tokens;
		}
	}

	// Set the units in the axis titles.
	if (hist != NULL)
	{
		TAxis* axis[3] = {hist->GetXaxis(), hist->GetYaxis(), hist->GetZaxis()};
		for (int i = 0; i < 3; ++i)
		{
			TString title = axis[i]->GetTitle();
			// Only set nanoseconds if the title contains variables V0A or V0C.
			if (title.Contains("V0A") or title.Contains("V0C")) title += " (ns)";
			axis[i]->SetTitle(title);
		}
	}

	delete tree;

	double fractionSelected = double(count) / double(total > 0 ? total : 1) * 100;
	ui->fLabelFractionSelected->setText(Form("Fraction selected = %.2f%%", fractionSelected));

	ui->fUserCanvas->GetCanvas()->Update();
	ui->fUserCanvas->Refresh();

	AliHLTTreeMonitor::Deregister();
	fInhibitAutoDraw = false;

	ui->fProgressBar->setValue(ui->fProgressBar->maximum());
	// We hide the progress bar only after some time to allow proper visual feedback.
	QTimer::singleShot(kProgressBarHideWait, this, SLOT(HideProgressBar()));
}


void MainWindow::BeginProcessingTree(int /*nevents*/)
{
}


void MainWindow::ProgressUpdateTree(int eventsComplete)
{
	ui->fProgressBar->setValue(eventsComplete + fNeventsOffset);
}


void MainWindow::EndProcessingTree()
{
}


void MainWindow::HideProgressBar()
{
	ui->fProgressBar->hide();
}


void MainWindow::UpdateCTPConfig(const QMap<QString, int>& triggerBits)
{
	// First save the check states of the existing items.
	QHash<QString, Qt::CheckState> oldtriggers;
	for (int i = 0; i < ui->fCTPList->count(); ++i)
	{
		QListWidgetItem* item = ui->fCTPList->item(i);
		oldtriggers[item->text()] = item->checkState();
	}
	// Now clear the list and fill it again with the new items setting the
	// check state to the previous value
	ui->fCTPList->clear();
	int index = 0;
	for (QMap<QString, int>::const_iterator ctpname = triggerBits.begin(); ctpname != triggerBits.end(); ++ctpname)
	{
		ui->fCTPList->addItem(ctpname.key());
		QListWidgetItem* item = ui->fCTPList->item(index++);
		item->setData(Qt::UserRole, ctpname.value());
		if (oldtriggers.contains(item->text()))
		{
			item->setCheckState(oldtriggers[item->text()]);
		}
		else
		{
			item->setCheckState(Qt::Checked);
		}
	}
	ui->fCTPList->update();

	// We can also update the min/max time information to be a more reasonable range.
	// Make the min time 1 second earlier than now in case of delays or jitter,
	// and the total period 8 hours. Unlikely runs last longer than this.
	// Anyway we increase this window if needed during canvas updates.
	QDateTime mintime = QDateTime::currentDateTime().addSecs(-1);
	QDateTime maxtime = mintime.addSecs(60*60*8);
	ui->fDateTimeEditMin->setDateTime(mintime);
	ui->fDateTimeEditMax->setDateTime(maxtime);
	fUserSetTimeRange = false;
}


void MainWindow::ToggleSelectedCTPList()
{
	QList<QListWidgetItem*> items = ui->fCTPList->selectedItems();
	for (QList<QListWidgetItem*>::iterator pitem = items.begin(); pitem != items.end(); ++pitem)
	{
		QListWidgetItem* item = *pitem;
		Qt::CheckState state = item->checkState();
		if (state == Qt::Checked)
		{
			item->setCheckState(Qt::Unchecked);
		}
		else if (state == Qt::Unchecked)
		{
			item->setCheckState(Qt::Checked);
		}
	}
	ui->fCTPList->update();
}


void MainWindow::UpdateHomerStatus(
		AliHLTHomerConnectionStatus status, int homerStatus,
		QString hostname, unsigned short port
	)
{
	QString message = hostname;
	message += ":";
	message += Form("%d", int(port));

	if (homerStatus != 0)
	{
		const char* errstr = strerror(homerStatus);
		// Set better messages if we can:
		if (homerStatus == EBADMSG)
		{
			errstr = "Parse error occurred on input HOMER stream";
		}
		message = message + ". " + errstr + ".";
	}

	switch (status)
	{
	case kHomerDisconnected:
		message = "Disconnected from HOMER server " + message;
		fStatusMessage.setStyleSheet("QLabel { color: blue }");
		break;
	case kHomerConnected:
		message = "Connected HOMER interface to " + message;
		fStatusMessage.setStyleSheet("QLabel { }");
		break;
	case kHomerError:
		message = "Error occured when connecting to " + message;
		fStatusMessage.setStyleSheet("QLabel { color: red }");
		break;
	default:
		fStatusMessage.setStyleSheet("QLabel { color: red }");
		message = Form("UNKNOWN status value %d.", int(status));
	}
	fStatusMessage.setText(message);
	ui->fStatusBar->update();
}


void MainWindow::UpdateTimeInputStates()
{
	// Updates the enabled states of the min/max date time input controls and the
	// seconds spin box as the fCheckBoxUseLastSeconds state changed.
	if (ui->fCheckBoxUseLastSeconds->checkState() == Qt::Checked)
	{
		ui->fSpinBoxSeconds->setEnabled(true);
		ui->fDateTimeEditMin->setEnabled(false);
		ui->fDateTimeEditMax->setEnabled(false);
	}
	else
	{
		ui->fSpinBoxSeconds->setEnabled(false);
		ui->fDateTimeEditMin->setEnabled(true);
		ui->fDateTimeEditMax->setEnabled(true);
	}
}


void MainWindow::UpdateSpinBoxMinX()
{
	if (ui->fSpinBoxMinX->value() > ui->fSpinBoxMaxX->value() - 0.0001)
	{
		ui->fSpinBoxMinX->setValue(ui->fSpinBoxMaxX->value() - 0.0001);
	}
}


void MainWindow::UpdateSpinBoxMaxX()
{
	if (ui->fSpinBoxMaxX->value() < ui->fSpinBoxMinX->value() + 0.0001)
	{
		ui->fSpinBoxMaxX->setValue(ui->fSpinBoxMinX->value() + 0.0001);
	}
}


void MainWindow::UpdateSpinBoxMinY()
{
	if (ui->fSpinBoxMinY->value() > ui->fSpinBoxMaxY->value() - 0.0001)
	{
		ui->fSpinBoxMinY->setValue(ui->fSpinBoxMaxY->value() - 0.0001);
	}
}


void MainWindow::UpdateSpinBoxMaxY()
{
	if (ui->fSpinBoxMaxY->value() < ui->fSpinBoxMinY->value() + 0.0001)
	{
		ui->fSpinBoxMaxY->setValue(ui->fSpinBoxMinY->value() + 0.0001);
	}
}


void MainWindow::UpdateSpinBoxMinV0A()
{
	if (ui->fSpinBoxMinV0A->value() > ui->fSpinBoxMaxV0A->value() - 0.0001)
	{
		ui->fSpinBoxMinV0A->setValue(ui->fSpinBoxMaxV0A->value() - 0.0001);
	}
}


void MainWindow::UpdateSpinBoxMaxV0A()
{
	if (ui->fSpinBoxMaxV0A->value() < ui->fSpinBoxMinV0A->value() + 0.0001)
	{
		ui->fSpinBoxMaxV0A->setValue(ui->fSpinBoxMinV0A->value() + 0.0001);
	}
}


void MainWindow::UpdateSpinBoxMinV0C()
{
	if (ui->fSpinBoxMinV0C->value() > ui->fSpinBoxMaxV0C->value() - 0.0001)
	{
		ui->fSpinBoxMinV0C->setValue(ui->fSpinBoxMaxV0C->value() - 0.0001);
	}
}


void MainWindow::UpdateSpinBoxMaxV0C()
{
	if (ui->fSpinBoxMaxV0C->value() < ui->fSpinBoxMinV0C->value() + 0.0001)
	{
		ui->fSpinBoxMaxV0C->setValue(ui->fSpinBoxMinV0C->value() + 0.0001);
	}
}


void MainWindow::SaveCanvas()
{
	QString fileName = QFileDialog::getSaveFileName(
			this,
			tr("Save Canvas As"),
			QString(),
			tr("All suported files(*.png *.ps *.eps *.pdf *.gif *.jpg *.tiff *.svg *.xpm *.C *.cxx *.root *.xml)"
			   ";;Post Script(*.eps *.ps)"
			   ";;Portable Document Format(*.pdf)"
			   ";;Images(*.png *.gif *.jpg *.tiff *.svg *.xpm)"
			   ";;ROOT macros(*.C *.cxx)"
			   ";;ROOT files(*.root)"
			   ";;XML files(*.xml)"
			  )
		);
	if (fileName.isEmpty()) return;

	// For some reason the file dialog selection filters are changed to lower case.
	// So we have to check if the .c file extention was used and replace it with
	// .C in such a case, otherwise a macro file will not be generated.
	QRegExp rx("*.c");
	rx.setPatternSyntax(QRegExp::Wildcard);
	if (rx.exactMatch(fileName))
	{
		fileName.replace(fileName.length()-1, 1, "C");
	}
	ui->fUserCanvas->GetCanvas()->SaveAs(fileName.toAscii().data());
}


void MainWindow::UserSetTimeRange()
{
	fUserSetTimeRange = true;
}


void MainWindow::CheckTimeRange()
{
	if (fTree == NULL) return;
	if (fEventProcessor == NULL) return;
	if (fUserSetTimeRange) return;

	double currentmax = AliHLT::QDateTimeToSec(ui->fDateTimeEditMax->dateTime());
	if (fEventProcessor->GetLastTimestamp() > currentmax)
	{
		QDateTime maxtime = AliHLT::SecToQDateTime(fEventProcessor->GetLastTimestamp());
		maxtime = maxtime.addSecs(60*60*8);
		ui->fDateTimeEditMax->setDateTime(maxtime);
	}
}


void MainWindow::ClearTTree()
{
	int result = QMessageBox::question(
			this,
			tr("Clear internal buffer?"),
			tr("Are you sure you want to clear the internal event buffer?\n"
			   "This operation cannot be undone and all sampled events will be lost."
			  ),
			QMessageBox::Yes | QMessageBox::No,
			QMessageBox::No
		);
	if (result == QMessageBox::Yes)
	{
		fTree->Reset();
	}
}
