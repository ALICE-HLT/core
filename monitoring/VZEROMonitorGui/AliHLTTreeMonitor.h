#ifndef ALIHLTTREEMONITOR_H
#define ALIHLTTREEMONITOR_H

#include <QObject>
#include "TVirtualMonitoring.h"

class AliHLTTreeMonitor : public QObject, public TVirtualMonitoringWriter
{
	Q_OBJECT

public:
	static void ResetPublishTimer();
	virtual Bool_t SendProcessingStatus(const char* status, Bool_t restarttimer = kFALSE);
	virtual Bool_t SendProcessingProgress(Double_t nevent, Double_t nbytes, Bool_t force = kFALSE);
	static void Register();
	static void Deregister();
	static AliHLTTreeMonitor& Instance() { return gInstance; }

signals:

	void BeginProcessing(int nevents);
	void ProgressUpdate(int eventsComplete);
	void EndProcessing();

private:

	static AliHLTTreeMonitor gInstance;
	static qint64 fLastEmitTime;

	AliHLTTreeMonitor();

	// Do not allow copying of this class.
	AliHLTTreeMonitor(const AliHLTTreeMonitor&);
	AliHLTTreeMonitor& operator = (const AliHLTTreeMonitor&);
};

#endif //ALIHLTTREEMONITOR_H
