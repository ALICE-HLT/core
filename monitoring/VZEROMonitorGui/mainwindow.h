#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include "AliHLTHomerReaderThread.h"

class TQtWidget;
class TTree;
class AliHLTEventProcessor;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

	TTree* Tree() const { return fTree; }
	void Tree(TTree* tree) { fTree = tree; }

	AliHLTEventProcessor* EventProcessor() const { return fEventProcessor; }
	void EventProcessor(AliHLTEventProcessor* processor) { fEventProcessor = processor; }
	
	Long64_t MaxEntries() const { return fMaxEntries; }
	void MaxEntries(Long64_t value) { fMaxEntries = value; }

public slots:

	void timerEvent(QTimerEvent* event);

	void BeginProcessingTree(int nevents);
	void ProgressUpdateTree(int eventsComplete);
	void EndProcessingTree();
	void HideProgressBar();

	void UpdateCTPConfig(const QMap<QString, int>& triggerBits);

	void ToggleSelectedCTPList();

	void UpdateHomerStatus(
			AliHLTHomerConnectionStatus status, int homerStatus,
			QString hostname, unsigned short port
		);

	void DrawUserHisto();
	void UpdateTimeInputStates();

	void UpdateSpinBoxMinX();
	void UpdateSpinBoxMaxX();
	void UpdateSpinBoxMinY();
	void UpdateSpinBoxMaxY();
	void UpdateSpinBoxMinV0A();
	void UpdateSpinBoxMaxV0A();
	void UpdateSpinBoxMinV0C();
	void UpdateSpinBoxMaxV0C();

	void SaveCanvas();
	void UserSetTimeRange();
	void ClearTTree();

private:

	void CheckTimeRange();

	Ui::MainWindow *ui;
	QLabel fStatusMessage;
	QLabel fEventCountMessage;
	int fTimer;
	int fNeventsOffset;
	Long64_t fMaxEntries;
	TTree* fTree;
	AliHLTEventProcessor* fEventProcessor;
	bool fInhibitAutoDraw;
	bool fUserSetTimeRange;
};

#endif // MAINWINDOW_H
