
#include <QApplication>
#include <QDateTime>
#include "AliHLTTreeMonitor.h"
#include "AliHLTTimeFunctions.h"
#include "TTree.h"

R__EXTERN  TTree* gTree;  // declared in TTreePlayer.cxx


enum
{
	kPublishingPeriod = 200 // Period between successive sending of signals in microseconds.
};


AliHLTTreeMonitor AliHLTTreeMonitor::gInstance;
qint64 AliHLTTreeMonitor::fLastEmitTime = 0;


AliHLTTreeMonitor::AliHLTTreeMonitor() :
	QObject(),
	TVirtualMonitoringWriter()
{
}


void AliHLTTreeMonitor::ResetPublishTimer()
{
	fLastEmitTime = AliHLT::CurrentTimestampMsec();
}


Bool_t AliHLTTreeMonitor::SendProcessingStatus(const char* status, Bool_t /*restarttimer*/)
{
	TString sstatus = status;
	if (sstatus == "STARTED")
	{
		int nevents = (gTree != NULL ? gTree->GetEntriesFast() : 0);
		emit BeginProcessing(nevents);
		QApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
	}
	else if (sstatus == "DONE")
	{
		emit EndProcessing();
		QApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
	}
	return kTRUE;
}


Bool_t AliHLTTreeMonitor::SendProcessingProgress(Double_t nevent, Double_t /*nbytes*/, Bool_t /*force*/)
{
	qint64 now = AliHLT::CurrentTimestampMsec();
	if (now >= fLastEmitTime + kPublishingPeriod)
	{
		fLastEmitTime = now;
		emit ProgressUpdate(nevent+1);
		QApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
	}
	return kTRUE;
}


void AliHLTTreeMonitor::Register()
{
	gMonitoringWriter = &gInstance;
}


void AliHLTTreeMonitor::Deregister()
{
	gMonitoringWriter = NULL;
}
