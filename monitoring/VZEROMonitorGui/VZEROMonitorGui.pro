include($(ROOTSYS)/include/rootcint.pri)

QT       += core gui

TARGET = VZEROMonitorGui
TEMPLATE = app


SOURCES += main.cxx\
        mainwindow.cxx \
    AliHLTHomerReaderThread.cxx \
    AliHLTEventProcessor.cxx \
    AliHLTTreeMonitor.cxx

HEADERS  += mainwindow.h \
    AliHLTHomerReaderThread.h \
    AliHLTEventProcessor.h \
    AliHLTTreeMonitor.h \
    AliHLTTimeFunctions.h

FORMS    += mainwindow.ui

QMAKE_CXXFLAGS += -g -fexceptions

INCLUDEPATH += $(ROOTSYS)/include $(ALICE_ROOT)/include $(ALICE_ROOT)/HLT/BASE/ $(ALICE_ROOT)/HLT/BASE/HOMER
LIBS += $(shell root-config --glibs) -L$(ALICE_ROOT)/lib/tgt_$(ALICE_TARGET) -lQtRoot -lMinuit -lMLP -lProof -lProofPlayer -lXMLParser -lXMLIO -lEG -lVMC -lGeom -lESD -lCDB -lSTEERBase -lSTEER -lRAWDatarec -lRAWDatabase -lAOD -lHLTbase -lHLTrec -lAliHLTHOMER -lAliHLTUtil -lANALYSIS -lANALYSISalice -lAliHLTTRD -lTRDbase -lTRDrec -lSTAT -lAliHLTMUON -lMUONcore -lMUONmapping -lRAWDatasim -lMUONraw -lMUONbase -lMUONcalib -lMUONtrigger -lMUONrec -lMUONevaluation -lMUONsim -lMUONgeometry

