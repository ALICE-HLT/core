#include <QtGui/QApplication>
#include <QThread>
#include <QBasicTimer>
#include "mainwindow.h"

#include "RConfig.h"
#include "TQtWidget.h"
#include "TH1D.h"
#include "TRandom3.h"
#include "TSystem.h"
#include "Riostream.h"

#include "AliESDEvent.h"
#include "AliHLTHOMERReader.h"
#include "AliHLTDataTypes.h"
#include "AliHLTOUT.h"
#include "AliHLTMessage.h"
#include "AliHLTMisc.h"
#include "AliESDRun.h"
#include "AliCDBManager.h"
#include <string>
#include <stdexcept>
#include <cerrno>

#include "AliHLTHomerReaderThread.h"
#include "AliHLTEventProcessor.h"
#include "AliHLTTreeMonitor.h"

Q_DECLARE_METATYPE(AliHLTHomerConnectionStatus)


namespace
{
	// Global properties parsed from command line:
	const char* gCDBPath = "local://$ALICE_ROOT/OCDB";
	Int_t gRunNumber = 0;
	const char* gHostname = "cn012";
	unsigned short gPort = 49000;
	int gPollInterval = 50;  // Data polling and update interval milliseconds.
	int gConnectionTimeout = 1000;  // The connection timeout in micro seconds.
	int gConnectionAttemptPeriod = 5000;  // The time between connection attempts in milliseconds.
	int gMaxEntries = 100000;  // Maximum number of entries processed by auto canvas and trend plot.

} // end of namespace


/**
 * Parses the command line values by extracting relevant parameters to global variables.
 * The strings passed to argv should remain valid for the lifetime of this binary.
 * They should be the values as received in the main() routine.
 * Only recognised arguments are processed and removed from the argument list.
 * \param [in,out] argc  The number of entries in argv.
 * \param [in,out] argv  The command line argument strings.
 * \returns true if the command line arguments were processed without problems and false otherwise.
 *     The caller should not continue using argv or argc if false is returned.
 */
bool ParseCommandLine(int& argc, char** argv)
{
	bool cdbpathSet = false;
	bool runSet = false;
	bool sourceSet = false;
	bool homerPollPeriodSet = false;
	bool homerTimeoutSet = false;
	bool homerStallPeriodSet = false;
	bool maxEntriesSet = false;

	for (int i = 1; i < argc; ++i)
	{
		if (strcmp(argv[i], "-cdbpath") == 0)
		{
			if (cdbpathSet)
			{
				cerr << "ERROR: -cdbpath has already been used with value = " << gCDBPath << endl;
				return false;
			}
			if (i+1 >= argc)
			{
				cerr << "ERROR: No value given for -cdbpath." << endl;
				return false;
			}
			gCDBPath = argv[i+1];
			argv[i] = NULL;
			argv[i+1] = NULL;
			++i;
			cdbpathSet = true;
		}
		else if (strcmp(argv[i], "-run") == 0)
		{
			if (runSet)
			{
				cerr << "ERROR: -run has already been used with value = " << gRunNumber << endl;
				return false;
			}
			if (i+1 >= argc)
			{
				cerr << "ERROR: No value given for -run. Expected a positive integer number." << endl;
				return false;
			}
			char* cpErr = NULL;
			Int_t num = Int_t( strtol(argv[i+1], &cpErr, 0) );
			if (cpErr == NULL or *cpErr != '\0' or num < 0)
			{
				cerr << "ERROR: Cannot convert '" << argv[i+1] << "' to a valid run number."
					" Expected a positive integer value." << endl;
				return false;
			}
			gRunNumber = num;
			argv[i] = NULL;
			argv[i+1] = NULL;
			++i;
			runSet = true;
		}
		else if (strcmp(argv[i], "-source") == 0)
		{
			if (sourceSet)
			{
				cerr << "ERROR: -source has already been used with value = " << gHostname << ":" << gPort << endl;
				return false;
			}
			if (i+1 >= argc)
			{
				cerr << "ERROR: No hostname given for -source." << endl;
				return false;
			}
			gHostname = argv[i+1];
			argv[i] = NULL;
			argv[i+1] = NULL;
			++i;
			if (i+1 >= argc)
			{
				cerr << "ERROR: No port number given for -source. Expected a positive integer in the range [0..65535]." << endl;
				return false;
			}
			char* cpErr = NULL;
			long num = strtol(argv[i+1], &cpErr, 0);
			if (cpErr == NULL or *cpErr != '\0' or num < 0 or num > 65535)
			{
				cerr << "ERROR: Cannot convert '" << argv[i+1] << "' to a valid port number."
					" Expected a positive integer in the range [0..65535]." << endl;
				return false;
			}
			gPort = (unsigned short)(num);
			argv[i+1] = NULL;
			++i;
			sourceSet = true;
		}
		else if (strcmp(argv[i], "-homerpollperiod") == 0)
		{
			if (homerPollPeriodSet)
			{
				cerr << "ERROR: -homerpollperiod has already been used with value = " << gPollInterval << endl;
				return false;
			}
			if (i+1 >= argc)
			{
				cerr << "ERROR: No value given for -homerpollperiod. Expected a positive integer number of milli-seconds." << endl;
				return false;
			}
			char* cpErr = NULL;
			long num = strtol(argv[i+1], &cpErr, 0);
			if (cpErr == NULL or *cpErr != '\0' or num < 0)
			{
				cerr << "ERROR: Cannot convert '" << argv[i+1] << "' to a valid number of milli-seconds."
					" Expected a positive integer number." << endl;
				return false;
			}
			gPollInterval = int(num);
			argv[i] = NULL;
			argv[i+1] = NULL;
			++i;
			homerPollPeriodSet = true;
		}
		else if (strcmp(argv[i], "-homertimeout") == 0)
		{
			if (homerTimeoutSet)
			{
				cerr << "ERROR: -homertimeout has already been used with value = " << gConnectionTimeout << endl;
				return false;
			}
			if (i+1 >= argc)
			{
				cerr << "ERROR: No value given for -homertimeout. Expected a positive integer number of micro-seconds." << endl;
				return false;
			}
			char* cpErr = NULL;
			long num = strtol(argv[i+1], &cpErr, 0);
			if (cpErr == NULL or *cpErr != '\0' or num < 0)
			{
				cerr << "ERROR: Cannot convert '" << argv[i+1] << "' to a valid number of micro-seconds."
					" Expected a positive integer number." << endl;
				return false;
			}
			gConnectionTimeout = int(num);
			argv[i] = NULL;
			argv[i+1] = NULL;
			++i;
			homerTimeoutSet = true;
		}
		else if (strcmp(argv[i], "-homerstallperiod") == 0)
		{
			if (homerStallPeriodSet)
			{
				cerr << "ERROR: -homerstallperiod has already been used with value = " << gConnectionAttemptPeriod << endl;
				return false;
			}
			if (i+1 >= argc)
			{
				cerr << "ERROR: No value given for -homerstallperiod. Expected a positive integer number of milli-seconds." << endl;
				return false;
			}
			char* cpErr = NULL;
			long num = strtol(argv[i+1], &cpErr, 0);
			if (cpErr == NULL or *cpErr != '\0' or num < 0)
			{
				cerr << "ERROR: Cannot convert '" << argv[i+1] << "' to a valid number of milli-seconds."
					" Expected a positive integer number." << endl;
				return false;
			}
			gConnectionAttemptPeriod = int(num);
			argv[i] = NULL;
			argv[i+1] = NULL;
			++i;
			homerStallPeriodSet = true;
		}
		else if (strcmp(argv[i], "-maxentries") == 0)
		{
			if (maxEntriesSet)
			{
				cerr << "ERROR: -maxentries has already been used with value = " << gMaxEntries << endl;
				return false;
			}
			if (i+1 >= argc)
			{
				cerr << "ERROR: No value given for -maxentries. Expected a positive integer number." << endl;
				return false;
			}
			char* cpErr = NULL;
			long num = strtol(argv[i+1], &cpErr, 0);
			if (cpErr == NULL or *cpErr != '\0' or num < 0)
			{
				cerr << "ERROR: Cannot convert '" << argv[i+1] << "' to a valid number."
					" Expected a positive integer number." << endl;
				return false;
			}
			gMaxEntries = int(num);
			argv[i] = NULL;
			argv[i+1] = NULL;
			++i;
			maxEntriesSet = true;
		}
	}

	// Compress out the NULL pointers in the argv array.
	int oldargc = argc;
	for (int i = oldargc - 1; i >= 1; --i)
	{
		if (argv[i] != NULL) continue;
		for (int j = i; j < argc - 1; ++j) argv[j] = argv[j+1];
		argv[argc-1] = NULL;
		--argc;
	}

	return true;
}


int main(int argc, char** argv)
{
	qRegisterMetaType<size_t>("size_t");
	qRegisterMetaType<homer_uint64>("homer_uint64");
	qRegisterMetaType<AliHLTHomerConnectionStatus>("AliHLTHomerConnectionStatus");

	if (not ParseCommandLine(argc, argv)) return EXIT_FAILURE;
	QApplication a(argc, argv);
	if (argc > 1)
	{
		cerr << "ERROR: Unknown argument '" << argv[1] << "'." << endl;
		return EXIT_FAILURE;
	}

	MainWindow w;
	QRect rect = w.geometry();
	rect.setWidth(1000);
	rect.setHeight(700);
	w.setGeometry(rect);
	w.show();

	AliCDBManager::Instance()->SetDefaultStorage(gCDBPath);
	AliCDBManager::Instance()->SetRun(gRunNumber);
	if (AliHLTMisc::Instance().InitStreamerInfos("HLT/Calib/StreamerInfo") != 0)
	{
		cerr << "ERROR: Could not load streamer information for HLT raw data from OCDB path "
			<< gCDBPath << " for run " << gRunNumber << "." << endl;
		return EXIT_FAILURE;
	}

	AliHLTHomerReaderThread readerThread;
	AliHLTEventProcessor eventProcessor;
	w.EventProcessor(&eventProcessor);

	readerThread.PollInterval(gPollInterval);
	readerThread.ConnectionTimeout(gConnectionTimeout);
	readerThread.ConnectionAttemptPeriod(gConnectionAttemptPeriod);
	w.MaxEntries(gMaxEntries);

	eventProcessor.connect(&readerThread, SIGNAL(BeginEvent(homer_uint64)), SLOT(ResetBuffers(homer_uint64)));
	eventProcessor.connect(&readerThread, SIGNAL(NewBuffer(void*,size_t)), SLOT(AddNewBuffer(void*,size_t)));
	eventProcessor.connect(&readerThread, SIGNAL(EndEvent(homer_uint64)), SLOT(ProcessEvent(homer_uint64)));

	w.connect(
			&readerThread,
			SIGNAL(StatusChanged(AliHLTHomerConnectionStatus,int,QString,unsigned short)),
			SLOT(UpdateHomerStatus(AliHLTHomerConnectionStatus,int,QString,unsigned short))
		);

	w.connect(&AliHLTTreeMonitor::Instance(), SIGNAL(BeginProcessing(int)), SLOT(BeginProcessingTree(int)));
	w.connect(&AliHLTTreeMonitor::Instance(), SIGNAL(ProgressUpdate(int)), SLOT(ProgressUpdateTree(int)));
	w.connect(&AliHLTTreeMonitor::Instance(), SIGNAL(EndProcessing()), SLOT(EndProcessingTree()));
	w.connect(&eventProcessor, SIGNAL(NewCTPConfig(QMap<QString,int>)), SLOT(UpdateCTPConfig(QMap<QString,int>)));

	w.Tree(&eventProcessor.GetTree());

	readerThread.Run(gHostname, gPort);

	int result = a.exec();

	readerThread.quit();
	readerThread.wait();

	return result;
}
