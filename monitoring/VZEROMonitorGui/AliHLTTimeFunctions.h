#ifndef ALIHLTTIMEFUNCTIONS_H
#define ALIHLTTIMEFUNCTIONS_H

#include <QDateTime>

namespace AliHLT
{

	inline qint64 CurrentTimestampMsec()
	{
#if QT_VERSION >= 0x040700
		return QDateTime::currentMSecsSinceEpoch();
#else
		QDateTime epoch(QDate(1970,1,1), QTime(0, 0, 0), Qt::UTC);
		return qint64(epoch.secsTo(QDateTime::currentDateTime())) * 1000;
#endif
	}


	inline qint64 QDateTimeToMsec(QDateTime t)
	{
#if QT_VERSION >= 0x040700
		return t.toMSecsSinceEpoch();
#else
		QDateTime epoch(QDate(1970,1,1), QTime(0, 0, 0), Qt::UTC);
		return qint64(epoch.secsTo(t)) * 1000;
#endif
	}


	inline QDateTime MsecToQDateTime(qint64 t)
	{
#if QT_VERSION >= 0x040700
		return QDateTime::fromMSecsSinceEpoch(t);
#else
		QDateTime epoch(QDate(1970,1,1), QTime(0, 0, 0), Qt::UTC);
		return epoch.addSecs(int(t / 1000));
#endif
	}


	inline double CurrentTimestampSec()
	{
		return double(CurrentTimestampMsec()) * 1e-3;
	}


	inline double QDateTimeToSec(QDateTime t)
	{
		return double(QDateTimeToMsec(t)) * 1e-3;
	}


	inline QDateTime SecToQDateTime(double t)
	{
		return MsecToQDateTime(qint64(t * 1e3));
	}

} // end of namespace AliHLT

#endif //ALIHLTTIMEFUNCTIONS_H
