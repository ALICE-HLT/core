#ifndef ALIHLTEVENTPROCESSOR_H
#define ALIHLTEVENTPROCESSOR_H

#include <QObject>
#include <QString>
#include <QMap>
#include "TTree.h"
#include "AliHLTLogging.h"
#include "AliHLTHOMERReader.h"

class AliESDEvent;
class AliHLTGlobalTriggerDecision;
class AliHLTCTPData;


class AliHLTEventProcessor : public QObject, public AliHLTLogging
{
	Q_OBJECT

public:

	AliHLTEventProcessor(QObject* parent = NULL);

	virtual ~AliHLTEventProcessor();

	TTree& GetTree() { return fTree; }
	double GetLastTimestamp() const { return fLastTimeStamp; }

public slots:

	void ResetBuffers(homer_uint64 eventId);
	void AddNewBuffer(void* buffer, size_t size);
	void ProcessEvent(homer_uint64 eventId);

signals:

	void NewCTPConfig(const QMap<QString, int>& triggerBits);

private:

	struct AliData
	{
		double fTimeStamp;
		float fV0A;
		float fV0C;
		Long64_t fTriggers;
	};

	homer_uint64 fEventId;
	void* fEsdBuffer;
	size_t fEsdBufferSize;
	AliESDEvent* fEsd;
	void* fDecisionBuffer;
	size_t fDecisionBufferSize;
	AliHLTGlobalTriggerDecision* fDecision;
	AliHLTUInt64_t fCTPTriggerMask;
	QString fCTPTriggerNames[gkNCTPTriggerClasses];
	TTree fTree;
	AliData fEventData;
	double fLastTimeStamp;
};

#endif //ALIHLTEVENTPROCESSOR_H
