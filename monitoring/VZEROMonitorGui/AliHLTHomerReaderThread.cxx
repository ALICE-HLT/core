
#include "AliHLTHomerReaderThread.h"
#include <QThread>
#include <QMutex>
#include <QDateTime>

#include "AliESDEvent.h"
#include "AliHLTHOMERReader.h"
#include "AliHLTDataTypes.h"
#include "AliHLTOUT.h"
#include "AliHLTMessage.h"
#include "AliHLTTimeFunctions.h"
#include <string>
#include <errno.h>


AliHLTHomerReaderThread::AliHLTHomerReaderThread() :
	QThread(),
	AliHLTLogging(),
	fPollInterval(50),
	fConnectionTimeout(100),
	fConnectionAttemptPeriod(5000),
	fTerminate(false),
	fHostname(""),
	fPort(),
	fReader(NULL),
	fLastEventID(-1),
	fLastStatus(kHomerInvalidStatus),
	fLastHomerStatus(-1),
	fLastTimeOfConnectionAttempt(0)
{
}


AliHLTHomerReaderThread::~AliHLTHomerReaderThread()
{
	// fReader created and deleted in the thread. i.e. AliHLTHomerReaderThread::run().
}


void AliHLTHomerReaderThread::Run(const char* hostname, unsigned short port)
{
	HLTDebug("Starting thread for HOMER reading on %s:%d", hostname, int(port));
	fHostname = hostname;
	fPort = port;
	start();
}

void AliHLTHomerReaderThread::quit()
{
	HLTDebug("Quiting thread for HOMER reading connected to %s:%d", fHostname.c_str(), int(fPort));
	QThread::quit();
	fTerminate = true;
}


void AliHLTHomerReaderThread::run()
{
	SendStatusChanged(kHomerDisconnected, 0);  // Announce initial status
	try
	{
		while (not fTerminate)
		{
			HandleReader();
			msleep(fPollInterval);
		}
	}
	catch (...)
	{
		delete fReader;
		throw;
	}
	delete fReader;
}


void AliHLTHomerReaderThread::HandleReader()
{
	qint64 now = AliHLT::CurrentTimestampMsec();
	if (now < fLastTimeOfConnectionAttempt + fConnectionAttemptPeriod) return;

	// Try connect to the HOMER port.
	if (fReader == NULL)
	{
		fReader = new AliHLTHOMERReader(fHostname.c_str(), fPort);

		// Check that opening the HOMER interface went well. If not delete the object.
		if (fReader == NULL)
		{
			HLTDebug("Could not connect to %s:%d, out of memory.", fHostname.c_str(), int(fPort));
			fLastTimeOfConnectionAttempt = now;
			SendStatusChanged(kHomerError, ENOMEM);
			return;
		}
		if (fReader->GetConnectionStatus() != 0)
		{
			HLTDebug("Could not connect to %s:%d via HOMER interface (error code = %d).",
				 fHostname.c_str(), int(fPort), fReader->GetConnectionStatus()
				);
			fLastTimeOfConnectionAttempt = now;
			SendStatusChanged(kHomerDisconnected, fReader->GetConnectionStatus());
			delete fReader;
			fReader = NULL;
			return;
		}
		SendStatusChanged(kHomerConnected, 0);
	}

	int status = fReader->ReadNextEvent(fConnectionTimeout);
	if (status != 0)
	{
		// Only emit the status change if it was not a timeout
		// since we only want to be notified on errors and connection
		// closures / aborts.
		if (status != ETIMEDOUT)
		{
			HLTDebug("Problem trying to read from HOMER on %s:%d (error code = %d)",
				 fHostname.c_str(), int(fPort), status
				);
			fLastTimeOfConnectionAttempt = AliHLT::CurrentTimestampMsec();
			SendStatusChanged(kHomerError, fReader->GetConnectionStatus());
			delete fReader;
			fReader = NULL;
		}
		return;
	}

	HLTDebug("Found new event (ID = 0x%016llX) on %s:%d", fReader->GetEventID(), fHostname.c_str(), int(fPort));
	if (fReader->GetEventID() == fLastEventID) return;
	fLastEventID = fReader->GetEventID();

	HLTDebug("Event (ID = 0x%016llX) was unique with %d blocks so processing...", fReader->GetEventID(), fReader->GetBlockCnt());
	emit BeginEvent(fLastEventID);

	for (unsigned long i = 0; i < fReader->GetBlockCnt(); ++i)
	{
		// Decode the data type and specification.
		AliHLTUInt64_t type = AliHLTOUT::ByteSwap64(fReader->GetBlockDataType(i));
		AliHLTUInt32_t origin = AliHLTOUT::ByteSwap32(fReader->GetBlockDataOrigin(i));
		AliHLTComponentDataType datatype;
		memcpy(&datatype.fID, &type, sizeof(type));
		memcpy(&datatype.fOrigin, &origin, sizeof(origin));

		HLTDebug("Block %lu for event 0x%016llX has data type = %s", i,
			 fReader->GetEventID(), AliHLTComponent::DataType2Text(datatype).c_str()
			);

		if (datatype == kAliHLTDataTypeESDObject or datatype == kAliHLTDataTypeGlobalTrigger)
		{
			size_t size = fReader->GetBlockDataLength(i);
			void* block = malloc(size);
			if (block == NULL)
			{
				fLastTimeOfConnectionAttempt = AliHLT::CurrentTimestampMsec();
				SendStatusChanged(kHomerError, ENOMEM);
				return;
			}
			memcpy(block, fReader->GetBlockData(i), size);

			emit NewBuffer(block, size);
		}
	}

	emit EndEvent(fLastEventID);
}


void AliHLTHomerReaderThread::SendStatusChanged(AliHLTHomerConnectionStatus status, int homerStatus)
{
	// Only emit a status change signal if there realy was a change.
	if (status != fLastStatus or homerStatus != fLastHomerStatus)
	{
		fLastStatus = status;
		fLastHomerStatus = homerStatus;
		emit StatusChanged(status, homerStatus, fHostname.c_str(), fPort);
	}
}
