
#include "AliHLTEventProcessor.h"
#include "AliESDEvent.h"
#include "AliESDVZERO.h"
#include "AliHLTGlobalTriggerDecision.h"
#include "AliHLTOUT.h"
#include "AliHLTMessage.h"
#include "AliHLTCTPData.h"
#include "TTimeStamp.h"


AliHLTEventProcessor::AliHLTEventProcessor(QObject* parent) :
	QObject(parent),
	AliHLTLogging(),
	fEventId(-1),
	fEsdBuffer(NULL),
	fEsdBufferSize(0),
	fEsd(NULL),
	fDecisionBuffer(NULL),
	fDecisionBufferSize(0),
	fDecision(NULL),
	fCTPTriggerMask(0x0),
	fTree("vzeroDataTree", "VZERO data"),
	fEventData(),
	fLastTimeStamp(0)
{
	fTree.Branch("vzeroEvent", &fEventData, "time/D:V0A/F:V0C/F:CTPtrigger/L");
}


AliHLTEventProcessor::~AliHLTEventProcessor()
{
	free(fEsdBuffer);
	free(fDecisionBuffer);
}


void AliHLTEventProcessor::ResetBuffers(homer_uint64 eventId)
{
	HLTDebug("Resetting buffers for new event ID = 0x%016llX.", eventId);
	// Cleanup any previously allocated memory and initialise to zero.
	free(fEsdBuffer);
	delete fEsd;  // Its not enought to delete just the buffer, memory was allocated when reading the object.
	fEsdBuffer = NULL;
	fEsdBufferSize = 0;
	fEsd = NULL;
	free(fDecisionBuffer);
	delete fDecision;
	fDecisionBuffer = NULL;
	fDecisionBufferSize = 0;
	fDecision = NULL;
	fEventId = eventId;
}


void AliHLTEventProcessor::AddNewBuffer(void* buffer, size_t size)
{
	HLTDebug("Adding and decoding buffer %p with size 0x%lu.", buffer, (unsigned long)size);

	// Unpack the object.
	TObject* obj = NULL;
	try
	{
		AliHLTMessage message(buffer, size);
		TClass* objclass = message.GetClass();
		if (objclass != NULL and objclass != reinterpret_cast<TClass*>(-1))
		{
			obj = reinterpret_cast<TObject*>( message.ReadObjectAny(objclass) );
		}
	}
	catch (...)
	{
		cerr << "Warning: Found problems decoding data buffer with ROOT object. This buffer will be skipped." << endl;
	}

	if (obj == NULL)
	{
		free(buffer);
		return;
	}
	AliESDEvent* esd = dynamic_cast<AliESDEvent*>(obj);
	if (esd != NULL)
	{
		if (fEsdBuffer == NULL)  // Prevent 2 copies, only use the first one found.
		{
			HLTDebug("Found an ESD object %p.", esd);
			fEsdBuffer = buffer;
			fEsdBufferSize = size;
			fEsd = esd;
			fEsd->GetStdContent();  // Needed to initialise reference pointers to data objects.
		}
		else
		{
			HLTWarning("Received second ESD object which will be discarded.");
			free(buffer);
		}
		return;
	}
	AliHLTGlobalTriggerDecision* decision = dynamic_cast<AliHLTGlobalTriggerDecision*>(obj);
	if (decision != NULL)
	{
		if (fDecisionBuffer == NULL)  // Prevent 2 copies, only use the first one found.
		{
			HLTDebug("Found a HLT global decision object %p.", decision);
			fDecisionBuffer = buffer;
			fDecisionBufferSize = size;
			fDecision = decision;
		}
		else
		{
			HLTWarning("Received second HLT global decision object which will be discarded.");
			free(buffer);
		}
		return;
	}
}


void AliHLTEventProcessor::ProcessEvent(homer_uint64 eventId)
{
	HLTDebug("Processing event ID = 0x%016llX.", eventId);
	fEventId = eventId;
	if (fEventId != eventId)
	{
		HLTWarning("Received a request to process an event for a different event ID = 0x%016llX, but expected event ID = 0x%016llX.",
			   eventId, fEventId
			);
	}
	if (fDecision == NULL)
	{
		HLTWarning("No global decision found; skipping event.");
		return;
	}
	if (fEsd == NULL)
	{
		HLTWarning("No ESD object found; skipping event.");
		return;
	}

	const AliHLTCTPData* ctpdata = NULL;
	for (int i = 0; i < fDecision->NumberOfInputObjects(); ++i)
	{
		if (fDecision->InputObject(i)->IsA()->GetBaseClass("AliHLTCTPData") != NULL)
		{
			ctpdata = (const AliHLTCTPData*)(fDecision->InputObject(i));
			break;
		}
	}
	if (ctpdata == NULL)
	{
		HLTWarning("No CTP trigger data found in global decision; skipping event.");
		return;
	}

	AliESDVZERO* v0data = NULL;
	v0data = dynamic_cast<AliESDVZERO*>(fEsd->GetVZEROData());
	if (v0data == NULL)
	{
		HLTWarning("No VZERO data found in ESD; skipping event.");
		return;
	}

	bool rebuildCTPlist = false;
	if (fCTPTriggerMask == ctpdata->Mask())
	{
		for (int i = 0; i < gkNCTPTriggerClasses; ++i)
		{
			QString ctpname = "";
			// Make sure we only use the CTP name if its valid, i.e. its bit in the mask is set.
			if (((fCTPTriggerMask >> i) & 0x1) == 0x1)
			{
				ctpname = ctpdata->Name(i);
			}
			if (fCTPTriggerNames[i] != ctpname)
			{
				rebuildCTPlist = true;
				break;
			}
		}
	}
	else
	{
		rebuildCTPlist = true;
	}
	if (rebuildCTPlist)
	{
		fCTPTriggerMask = ctpdata->Mask();
		QMap<QString, int> triggerBits;
		for (int i = 0; i < gkNCTPTriggerClasses; ++i)
		{
			if (((fCTPTriggerMask >> i) & 0x1) == 0x1)
			{
				fCTPTriggerNames[i] = ctpdata->Name(i);
				triggerBits.insert(ctpdata->Name(i), i);
			}
			else
			{
				fCTPTriggerNames[i] = "";
			}
		}

		HLTDebug("Received a new CTP configuration so reseting the tree and sending signal.");
		fTree.Reset();
		emit NewCTPConfig(triggerBits);
	}

	fLastTimeStamp = TTimeStamp().AsDouble();
	fEventData.fTimeStamp = fLastTimeStamp;
	fEventData.fTriggers = ctpdata->Triggers();
	fEventData.fV0A = v0data->GetV0ATime();
	fEventData.fV0C = v0data->GetV0CTime();
	fTree.Fill();
}
