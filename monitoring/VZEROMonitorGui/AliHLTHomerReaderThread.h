#ifndef ALIHLTHOMERREADERTHREAD_H
#define ALIHLTHOMERREADERTHREAD_H

#include <QThread>
#include <QMutex>
#include <QMetaType>

#include "AliESDEvent.h"
#include "AliHLTHOMERReader.h"
#include "AliHLTDataTypes.h"
#include "AliHLTOUT.h"
#include "AliHLTMessage.h"
#include <string>
#include <errno.h>


enum AliHLTHomerConnectionStatus
{
	kHomerInvalidStatus = -1,
	kHomerDisconnected = 0,
	kHomerConnected = 1,
	kHomerError = 2
};


class AliHLTHomerReaderThread : public QThread, public AliHLTLogging
{
	Q_OBJECT

public:

	AliHLTHomerReaderThread();

	virtual ~AliHLTHomerReaderThread();

	void Run(const char* hostname, unsigned short port);

	void quit();

	int PollInterval() const { return fPollInterval; }
	void PollInterval(int value) { fPollInterval = value; }
	int ConnectionTimeout() const { return fConnectionTimeout; }
	void ConnectionTimeout(int value) { fConnectionTimeout = value; }
	int ConnectionAttemptPeriod() const { return fConnectionAttemptPeriod; }
	void ConnectionAttemptPeriod(int value) { fConnectionAttemptPeriod = value; }

signals:

	void StatusChanged(AliHLTHomerConnectionStatus status, int homerStatus, QString hostname, unsigned short port);
	void BeginEvent(homer_uint64 eventId);
	void NewBuffer(void* buffer, size_t size);
	void EndEvent(homer_uint64 eventId);

private:

	void HandleReader();
	void run();
	void SendStatusChanged(AliHLTHomerConnectionStatus status, int homerStatus);

	int fPollInterval;  // Data polling and update interval milliseconds.
	int fConnectionTimeout;  // The connection timeout in micro seconds.
	int fConnectionAttemptPeriod;  // The time between connection attempts in milliseconds.
	bool fTerminate;
	std::string fHostname;
	unsigned short fPort;
	AliHLTHOMERReader* fReader;
	homer_uint64 fLastEventID;
	AliHLTHomerConnectionStatus fLastStatus;
	int fLastHomerStatus;
	qint64 fLastTimeOfConnectionAttempt;
};

#endif //ALIHLTHOMERREADERTHREAD_H
