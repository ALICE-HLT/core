/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/
#ifndef ALI_HLT_FED_PORTAL_COMPONENT_HPP
#define ALI_HLT_FED_PORTAL_COMPONENT_HPP


//#include "AliHLTTypes.h"


#include "AliHLTControlledComponent.hpp"
//#include "AliHLTControlledSinkComponent.hpp"


/**
 * This class glues the FED portal component to the HLT PubSub framework.
 *
 * @author Sebastian Bablok
 *
 * @date 2006-08-28
 */
class AliHLTFEDPortalComponent : public AliHLTControlledSinkComponent {
    public:

		/**
		 * Constructor for a AliHLTFEDPortalComponent
		 *
		 * @param name passed through to AliHLTControlledSinkComponent
		 * @param argc argument count of command line
		 * @param argv arguments of command line
		 */
		AliHLTFEDPortalComponent(char* name, int argc, char** argv);

		/**
		 * Destructor for a AliHLTFEDPortalComponent
		 */
		virtual ~AliHLTFEDPortalComponent();


    protected:
        /**
		 * Typedef for Parent class
		 */
		typedef AliHLTControlledSinkComponent TSuper;


    	/**
		 * Function to parse the entries of the command line
		 *
		 * @param argc argument count of command line
		 * @param argv pointer to the arguments of the command line
		 * @param i [in] number in argument list where to start with parsing;
		 *		[out] number where to start with next parsing round.
		 * @param errorArg [out] number of argument in list where parsing
		 *		error occured.
		 *
		 * @return NULL in case of no error, else error string
		 */
		virtual char* ParseCmdLine(int argc, char** argv, int& i, int& errorArg);

		/**
		 * Fucntion to print out the usage of this component.
		 *
		 * @param errorStr ?? (passed to parent class)
		 * @param errorArg ?? (passed to parent class)
		 * @param errorParam ?? (passed to parent class)
		 */
		virtual void PrintUsage(const char* errorStr, int errorArg, int errorParam);

		/**
		 * Function to create a AliHLTFEDSubscriber component.
		 *
		 * @return pointer to AliHLTFEDSubscriber component as a
		 *		AliHLTProcessingSubscriber*
		 */
		virtual AliHLTProcessingSubscriber* CreateSubscriber();

		/**
		 * Function to show the current configuration.
		 * Note: in case of configured for a DimService containing a string,
		 * the size of the provided value will vary during usage. Therefore
		 * it just shows a size of "-1".
		 */
		virtual void ShowConfiguration();



    private:// Neu ab hier
		/**
		 * String containing the name of the corresponding DimService name
		 */
		std::string fDimServiceName;

		/**
		 * The type of the FedChannel (SingleInt, SingleFloat, SingleChar or
		 * grouped) of the corresponding DimService as string representation.
		 */
		std::string fDimChannelType;

		/**
		 * The string containing the name of node running the DIM dns.
		 * This member is either set by command line parameter or, if not
		 * specified there, retrieved from the environmental variable
		 * DIM_DNS_NODE. If it is not specified in one of these places, the
		 * component cannot be intialized. An error msg will be produced.
		 */
		std::string fDimDnsNode;


};

#endif //ALI_HLT_FED_PORTAL_COMPONENT_HPP
