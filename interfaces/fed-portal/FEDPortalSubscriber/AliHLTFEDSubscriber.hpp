/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/
#ifndef ALI_HLT_FED_SUBSCRIBER_HPP
#define ALI_HLT_FED_SUBSCRIBER_HPP

//#include "AliHLTControlledComponent.hpp"

//#include <stdio.h>
//#include <stdlib.h>
//#include <unistd.h>
//#include <sys/types.h>
//#include <sys/stat.h>
//#include <fcntl.h>
//#include <errno.h>

#include <string>

#include "FedApiServiceChannel.hpp"

#include "AliHLTProcessingSubscriber.hpp"


/**
 * Define for Error due to size mismatch
 */
#define ERROR_SIZE_MISMATCH -16


/**
 * FED portal class to interface the FED-Api and connect it to the PubSub
 * system. Therefore it has to act as well as a DimServer.
 *
 * @author Sebastian Bablok
 *
 * @date 2006-08-28
 */
class AliHLTFEDSubscriber : public AliHLTProcessingSubscriber, public DimServer {
    public:
		/**
		 * Constructor for a AliHLTFEDSubscriber.
		 *
		 * @param name parameter required by the parent class
		 * @param sendEventDone parameter required by the parent class
		 * @param minBlockSize parameter required by the parent class
		 * @param dimServiceName name of the DimService to create
		 *		(!!! this parameter will change lateron !!!)
		 * @param dimChannelType Type of the FedApi channel
		 *		(!!! this parameter will change lateron !!!)
		 * @param dimDnsNode string containing the hostname of the DimDnsNode
		 * @param eventCountAlloc parameter required by the parent class
		 */
		AliHLTFEDSubscriber(const char* name, // for parent class
				bool sendEventDone, AliUInt32_t minBlockSize, // for parent class
				std::string dimServiceName, std::string dimChannelType,
				std::string dimDnsNode,
				int eventCountAlloc = -1); // for parent class

		/**
		 * Destructor for AliHLTFEDSubscriber
		 */
		virtual ~AliHLTFEDSubscriber();

		/**
		 * Define for the name of the FedApi Message Channel
		 */
		static const char* FedApiMessageChannelName;

		/**
		 * Define for the name of the FedApi Acknowledge Channel
		 */
		static const char* FedApiAcknowldegeChannelName;

		/**
		 * Define for the name of the FedApi Grouped Services Channel
		 */
		static const char* FedApiGroupedChannelName;

		/**
		 * Define for the name of the FedServer representing the DCSPortal
		 */
		static const char* FEDPortalFedServerName; //DCSPortalFedServerName;

		/**
		 * Define for the name of the FedApi ConfigureFeeCom Channel
		 */
		static const char* FedApiConfigureFeeComName;

		/**
		 * Define for channel type "Single Service Channel" with an "int"
		 * See FedApi for details
		 */
		static const char* FedApiSingleServiceChannelInt_def;

		/**
		 * Define for channel type "Single Service Channel" with an "float"
		 * See FedApi for details
		 */
		static const char* FedApiSingleServiceChannelFloat_def;

		/**
		 * Define for channel type "Single Service Channel" with "chars"
		 * See FedApi for details
		 */
		static const char* FedApiSingleServiceChannelChar_def;

		/**
		 * Define for channel type "Grouped Service Channel"
		 * See FedApi for details
		 */
		static const char* FedApiGroupedServiceChannel_def;

		/**
		 * Define for channel type "Message Channel"
		 * See FedApi for details
		 */
		static const char* FedApiMessageChannel_def;

		/**
		 * Define for channel type "Acknowledge Channel"
		 * See FedApi for details
		 */
		static const char* FedApiAcknowledgeChannel_def;


    protected:

		/**
		 * Function to process the data given from the PubSub framework.
		 * Here the mapping from data given from DAs through the PubSub
		 * framework to the FedApi is performed. The Dim Channels, representing
		 * the FedApi are feed with the given data.
		 * Possible data for the FedApi are: Single and Grouped Services as
		 * well as messages over the FedApi.
		 * <pre>
		 *
		 * Structure of the actual data provided by the data blocks:
		 * All payload blocks have a header and the actual payload:
		 *
		 * +------------------------------------------------------------------+
		 * | Offset{int32}|TypeOfPayload{char} | SizeOfPayload{int32}|payload |
		 * +------------------------------------------------------------------+
		 *
		 *	- Offset: size of this header in Bytes (offset to BlockData
		 *			pointer), stored in a 32bit integer (define:
		 *			FED_API_SERVICE_DATA_HEADER_OFFSET)
		 *	- TypeOfPayload: type of the payload as described below
		 *			(Single-, Grouped, or Message- Service)
		 *			('S' = Single, 'G' = Grouped, 'M' = Message);
		 *			stored in a single char (AliUInt8_t).
		 *	- SizeOfPayload: size of the payload in Bytes;
		 *			stored in a 32bit integer.
		 *	- payload: arbitrary amount of payload blocks, which are described
		 *			below (glued after each other). (the actual amount is given
		 *			by the payload size and the members in the payload itself)
		 *
		 * (All members are streamed after each other, blocks are glued after
		 * each other; no paddding)
		 *
		 * Payload blocks (structure):
		 *
		 * 1 + 2. Single and/or Grouped Services (its size is variable):  // extend with char !!!!!
		 *
		 * +------------------------------------------------------------------+
		 * |nameLength{int32}|intValue{int32}|floatValue|name{NULL terminated}|
		 * +------------------------------------------------------------------+
		 *
		 *	- nameLength: length of the name of the service (inclusive
		 *		Null-termination); stored in a 32bit integer.
		 *	- intValue: a 32bit integer representing the service; if the
		 *		service is NOT represented by an integer, set this to the
		 *		DON'T CARE value: -9999999 .
		 *	- floatValue: a float representing the service; if the
		 *		service is NOT represented by a float, set this to the
		 *		DON'T CARE value: -9999999.0 .
		 *	- name: char string with the name of the service, represented by
		 *		the before mentioned values; NULL terminated.
		 *
		 * 3. Message Service (fixed size: 540 Bytes):
		 *
		 *      +---------------------------------------------
		 *      | messageType{int32} | source{char[256]} |
		 *      +---------------------------------------------
		 *
		 *     --------------------------------------------------------+
		 *       messageDescription{char[256]} | timestamp{char[20]}   |
		 *     --------------------------------------------------------+
		 *
		 *	- messageType: a 32bit integer representing the type of the message,
		 *		(MSG_INFO = 1, MSG_WARNING = 2, MSG_ERROR = 4,
		 *		 MSG_FAILURE_AUDIT = 8, MSG_SUCCESS_AUDIT = 16,
		 *		 MSG_DEBUG = 32, MSG_ALARM = 64).
		 *	- source: the actual source of the message, here the different
		 *		detectors (like TPC, PHOS, TRD, ...) can encode as well there
		 *		detector name, as well a more specific description of where this
		 *		event has occured; char array of size 256; NULL terminated (!).
		 *	- messageDescription: the actual message, that shall be send; char
		 *		array of size 265; NULL terminated(!).
		 *	- timestamp: timestamp, when this message has been produced; has to
		 *		have the format: "YYYY-MM-DD hh:mm:ss\0"; char array of size 20;
		 *		NULL terminated.
		 *
		 * </pre>
		 *
		 * @param eventID structure with event ID
		 *			(function doesn't care)
		 * @param blockCnt number of blocks of type BlockData.
		 * @param blocks pointer to the first block of type BlockData given by
		 *			the framework (includes meta data to the actual data):
		 *			used members of this structure are:
		 *			- fData: pointer to the actual data (payload) (data that
		 *				shall be processed + additional meta data (specific for
		 *				DCSPortal component))
		 *			- fSize: size of the payload (fData) in Bytes
		 *			- fDataType: structure containing ID of the type of the
		 *				data block (fData), in this case the type in
		 *				fDataType.fID has to be FEDAPI_SERVICE_DATAID
		 *				(@see FEDAPI_SERVICE_DATAID)
		 *			- fAttributes: describing the attributes of the data (Byte
		 *				order, alignments, etc)
		 * @param sedd descriptor of blocks
		 *			(function doesn't care)
		 * @param etsp structure with trigger info
		 *			(function doesn't care)
		 * @param outputPtr pointer, that can be filled with return data
		 *			(function doesn't care)
		 * @param size size of output data; is set to 0
		 * @param outputBlocks can store with return data
		 *			(function doesn't care)
		 * @param edd can store optional "processing done" data
		 *			(function doesn't care)
		 *
		 * @return true, if processing has been successful or a NO retry shall
		 *		be made; false if processing has been NOT successful but a retry
		 *		could end in a successful processing of the data.
		 */
		virtual bool ProcessEvent(AliEventID_t eventID, AliUInt32_t blockCnt,
				AliHLTSubEventDescriptor::BlockData* blocks,
				const AliHLTSubEventDataDescriptor* sedd,
				const AliHLTEventTriggerStruct* etsp, AliUInt8_t* outputPtr,
				AliUInt32_t& size,
				vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks,
				AliHLTEventDoneData*& edd);

		/**
		 * Callback - Function called by the DIM framework to pass incoming
		 * commands to the this component.
		 */
		virtual void commandHandler();

		/**
		 * special DIM Server exit handler.
		 * This function has to take care of special exit calls issued bye the
		 * DIM_DNS as well. These can be identified by the exit code
		 *
		 * @param exitCode the given exit code from the framework
		 */
//		virtual void dimServerExitHandler(int exitCode);


    private:
    	/**
    	 * Function to extract data from prepared payload blocks of the PubSub
    	 * system and fill the Grouped Service channel with it.
    	 *
    	 * @param pointer to first element of real data (pointer to the first
    	 *		element of the first block of FedApiServiceData)
    	 * @param size of FedApiService data (can cover more than one blocks of
    	 *		FedApiServiceData, which are then glued after each other)
    	 *
    	 * @return 0, if successfull; else an error code is provided;
    	 *		ERROR_SIZE_MISMATCH is returned in case of a mismatch in the
    	 *		given size in the header and the actually found size
    	 */
    	int fillGroupedServiceChannel(AliUInt8_t* pFirstElem, AliUInt32_t size);

    	/**
		 * Function to extract data from prepared payload blocks of the PubSub
		 * system and fill the Single Service channel with it.
		 *
		 * @param pointer to first element of real data (pointer to the first
		 *		element of the first block of FedApiServiceData)
		 * @param size of FedApiService data (can cover more than one blocks of
		 *		FedApiServiceData, which are then glued after each other)
		 *
		 * @return 0, if successfull; else an error code is provided;
    	 *		ERROR_SIZE_MISMATCH is returned in case of a mismatch in the
    	 *		given size in the header and the actually found size
    	 */
    	int fillSingleServiceChannel(AliUInt8_t* pFirstElem, AliUInt32_t size);

    	/**
		 * Function to extract data from prepared payload blocks of the PubSub
		 * system and fill the Message channel with it.
		 *
		 * @param pointer to first element of real data (pointer to the first
		 *		element of the first block of FedApiServiceData)
		 * @param size of FedApiService data (can cover more than one blocks of
		 *		FedApiServiceData, which are then glued after each other)
		 *
		 * @return 0, if successfull; else an error code is provided;
    	 *		ERROR_SIZE_MISMATCH is returned in case of a mismatch in the
    	 *		given size in the header and the actually found size
    	 */
    	int fillMessageChannel(AliUInt8_t* pFirstElem, AliUInt32_t size);

    	/**
    	 * Create function to produce the desired DimService object
    	 *
    	 * @param name name of the service
    	 * @param type type of the service channel ("SingleInt", "SingleFloat"
    	 *			or "Grouped")
    	 *
    	 * @return the new created FedApiServiceChannel object
    	 */
    	FedApiServiceChannel* createDimServiceChannel(string name, string type);

    	/**
    	 * Setter function for the log level.
    	 * A check for a valid log level is performed for the log level is set.
    	 * NOTE: MSG_ALARM is always set
    	 *
    	 * @param logLevel the new loglevel to set
    	 *
    	 * @return 0 on success, else an appropriated error code
    	 *		(FED_API_INVALID_PARAMETER)
    	 */
    	int setLogLevel(int logLevel);

    	/**
    	 * Getter for current log level
    	 *
    	 * @return the current log level
    	 */
    	int getLogLevel();

		/**
		 * String containing the name of the corresponding DimService name
		 */
		std::string fDimServiceName;

		/**
		 * The channel type (Single or Grouped Service Channel) as string
		 * representation of the corresponding DimService. SeeFedApi for details
		 */
		std::string fDimChannelType;

		/**
		 * DimService channel for publishing the offered values.
		 */
		FedApiServiceChannel* fpFedChannel;

		/**
		 * DimService channel for publishing the offered values over the
		 * grouped value channel.
		 */
		FedApiServiceChannel* fpFedGroupedChannel;

		/**
		 * FedApi Message channel
		 */
		FedApiServiceChannel* fpFedMessage;

		/**
		 * FedApi ACK channel
		 */
		FedApiServiceChannel* fpFedAcknowledge;

		/**
		 * FedApi ConfigureFeeCom command channel
		 */
		DimCommand* fpFedConfigureFeeCom;

		/**
		 * Stores the current log level
		 */
		int fLogLevel;

		/**
		 * Static defining the sleep time after updating a channel
		 */
		static const unsigned long FedApiUpdateChannelSleep;

};

inline int AliHLTFEDSubscriber::getLogLevel() {
    return fLogLevel;
}

#endif // ALI_HLT_FED_SUBSCRIBER_HPP
