/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/
#include "AliHLTFEDPortalComponent.hpp"
#include "AliHLTFEDSubscriber.hpp"





AliHLTFEDPortalComponent::AliHLTFEDPortalComponent(char* name,
		int argc, char** argv) : TSuper(name, argc, argv) {

}


AliHLTFEDPortalComponent::~AliHLTFEDPortalComponent() {

}


void AliHLTFEDPortalComponent::ShowConfiguration() {
    TSuper::ShowConfiguration();
    LOG(AliHLTLog::kInformational,
    		"DCSPortalDimSubscriberComponent::ShowConfiguration", "DimServiceName, FedApiChannelType, DimDnsNode")
			<< "DimService name: '" << fDimServiceName.c_str() <<
			"', FedApi Channel type: '" << fDimChannelType.c_str() <<
			"', DimDnsNode: '" << fDimDnsNode.c_str() << "'." << ENDLOG;
}


char* AliHLTFEDPortalComponent::ParseCmdLine(int argc, char** argv,
		int& i, int& errorArg) {

	// DimService name
	if (!strcmp(argv[i], "-dimServiceName")) {
		if (argc <= (i + 1)) {
			return "Missing DimService name.";
		}
		fDimServiceName = argv[i + 1];
		i += 2;
		return NULL;
	}

	// Channel type [SingleServiceChannel|GroupedServiceChannel]
	if (!strcmp(argv[i], "-channelType")) {
		if (argc <= (i + 1)) {
		   return "Missing DimService data type.";
		}
		if (!strcmp(argv[i + 1],
				AliHLTFEDSubscriber::FedApiSingleServiceChannelInt_def)) {
			fDimChannelType =
					AliHLTFEDSubscriber::FedApiSingleServiceChannelInt_def;
			i += 2;
			return NULL;

		} else if (!strcmp(argv[i + 1],
				AliHLTFEDSubscriber::FedApiSingleServiceChannelFloat_def)) {
			fDimChannelType =
					AliHLTFEDSubscriber::FedApiSingleServiceChannelFloat_def;
			i += 2;
			return NULL;

		} else if (!strcmp(argv[i + 1],
				AliHLTFEDSubscriber::FedApiSingleServiceChannelChar_def)) {
			fDimChannelType =
					AliHLTFEDSubscriber::FedApiSingleServiceChannelChar_def;
			i += 2;
			return NULL;

		} else if (!strcmp(argv[i + 1],
				AliHLTFEDSubscriber::FedApiGroupedServiceChannel_def)) {
			fDimChannelType =
					AliHLTFEDSubscriber::FedApiGroupedServiceChannel_def;
			i += 2;
			return NULL;

		} else {
			errorArg = i + 1;
			return "Unknown DimService channel type.";
		}
		return NULL;
	}

	// DimDNS node
	if (!strcmp(argv[i], "-dimDnsNode")) {
		if (argc <= (i + 1)) {
			return "Missing Dim DNS node name.";
		}
		fDimDnsNode = argv[i + 1];
		i += 2;
		return NULL;
	}

	return TSuper::ParseCmdLine(argc, argv, i, errorArg);
}



void AliHLTFEDPortalComponent::PrintUsage(const char* errorStr, int errorArg,
		int errorParam) {
	TSuper::PrintUsage(errorStr, errorArg, errorParam); // call to parent class
	LOG(AliHLTLog::kError, "Processing Component", "Command line usage")
			<< "-dimServiceName <name>: Name of the Dim service to create. "
			<< ENDLOG;
	LOG(AliHLTLog::kError, "Processing Component", "Command line usage")
			<< "-channelType [SingleServiceChannelInt|SingleServiceChannelFloat|GroupedServiceChannel]. Determine channel type of the FedApi (defines implicit the channel structure [I:1;F:1] for single int and float or [I:1;F:1;C] for grouped). "
			<< ENDLOG;
	LOG(AliHLTLog::kError, "Processing Component", "Command line usage")
			<< "{-dimDnsNode <node name>}. (semi-optional) Specifies the node where the Dim DNS is running. If not provided as command line parameter, it has to be set as environmental variable, else an error will occur. "
			<< ENDLOG;
}


AliHLTProcessingSubscriber* AliHLTFEDPortalComponent::CreateSubscriber() {
    return new AliHLTFEDSubscriber(fSubscriberID.c_str(), fSendEventDone,
    		fMinBlockSize, fDimServiceName, fDimChannelType, fDimDnsNode, fMaxPreEventCountExp2);
}



///////////////////////////////// -> MAIN <- //////////////////////////////////

int main(int argc, char** argv) {
    AliHLTFEDPortalComponent procComp("AliHLTFEDSubscriber", argc, argv);
    procComp.Run();
    return 0;
}

