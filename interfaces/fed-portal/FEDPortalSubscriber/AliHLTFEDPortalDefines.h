/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/
#ifndef ALI_HLT_FED_PORTAL_DEFINES_H
#define ALI_HLT_FED_PORTAL_DEFINES_H

/**
 * Define for payload of AliHLTSubEventDescriptor::BlockData block is of type
 * FedApi Service data.
 * This is either of type Single Service and/or Grouped Services and/or
 * Message Service (for details see AliHLTFedSubscriber::ProcessEvent(...)).
 *
 * @see AliHLTFedSubscriber::ProcessEvent
 */
#define FEDAPI_SERVICE_DATAID		(((AliUInt64_t)'FED-')<<32 | 'SRV ')


/**
 * Define for payload of AliHLTSubEventDescriptor::BlockData block is of type
 * FedApi Command data.
 *
 * @see AliHLTFedSubscriber
 */
#define FEDAPI_COMMAND_DATAID		(((AliUInt64_t)'FED-')<<32 | 'CMD ')

/**
 * Define for the offset of the header in FedApi Service data:
 * (Size of Offset) + (Size of Payload type) + (Size of PayloadSize)
 */
#define FED_API_SERVICE_DATA_HEADER_OFFSET (sizeof(AliUInt32_t)+sizeof(AliUInt8_t)+sizeof(AliUInt32_t))

/**
 * Define for the type "Single Service Data" in FedApi Service data payload.
 */
#define FED_API_SINGLE_SERVICE_DATA		((AliUInt8_t)'S')

/**
 * Define for the type "Grouped Service Data" in FedApi Service data payload.
 */
#define FED_API_GROUPED_SERVICE_DATA 	((AliUInt8_t)'G')

/**
 * Define for the type "Message Service Data" in FedApi Service data payload.
 */
#define FED_API_MESSAGE_SERVICE_DATA 	((AliUInt8_t)'M')


#endif //ALI_HLT_FED_PORTAL_DEFINES_H
