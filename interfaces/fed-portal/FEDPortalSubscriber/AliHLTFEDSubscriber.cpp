/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/
#include "AliHLTFEDSubscriber.hpp"
#include "AliHLTFEDPortalComponent.hpp"

#include "AliHLTFEDPortalDefines.h"
#include "AliHLTFEDPortalTypes.h"

#include "FedApiValue.hpp"
#include "FedApiCommand.hpp"

#include "dim/dis.hxx"

#include <iostream>

using namespace std;

const char* AliHLTFEDSubscriber::FedApiMessageChannelName = "HLT-FEDPortal_MSG";

const char* AliHLTFEDSubscriber::FedApiAcknowldegeChannelName = "HLT-FEDPortal_ACK";

const char* AliHLTFEDSubscriber::FedApiGroupedChannelName = "HLT-FEDPortal_Grouped_Services";

const char* AliHLTFEDSubscriber::FEDPortalFedServerName = "HLT-FedServer";

const char* AliHLTFEDSubscriber::FedApiConfigureFeeComName = "HLT-FEDPortal_ConfigureFeeCom";

const unsigned long AliHLTFEDSubscriber::FedApiUpdateChannelSleep = 50000;



const char* AliHLTFEDSubscriber::FedApiSingleServiceChannelInt_def
		= "SingleServiceChannelInt";

const char* AliHLTFEDSubscriber::FedApiSingleServiceChannelFloat_def
		= "SingleServiceChannelFloat";

const char* AliHLTFEDSubscriber::FedApiSingleServiceChannelChar_def
		= "SingleServiceChannelChar";

const char* AliHLTFEDSubscriber::FedApiGroupedServiceChannel_def
		= "GroupedServiceChannel";

const char* AliHLTFEDSubscriber::FedApiMessageChannel_def
		= "MessageChannel";

const char* AliHLTFEDSubscriber::FedApiAcknowledgeChannel_def
		= "AcknowledgeChannel";


AliHLTFEDSubscriber::AliHLTFEDSubscriber(const char* name, // for parent class
		bool sendEventDone,	AliUInt32_t minBlockSize, // for parent class
		string dimServiceName, string dimChannelType, string dimDnsNode,
		int eventCountAlloc) : // for parent class
    	AliHLTProcessingSubscriber(name, sendEventDone, minBlockSize, 0, 0, //is a sink -> 0
    			eventCountAlloc),
    	fDimServiceName(dimServiceName), fDimChannelType(dimChannelType),
    	fpFedChannel(0), fpFedConfigureFeeCom(0), fLogLevel(MSG_MAX_VAL) {

	// special DIM intialisations
	if (!dimDnsNode.empty()) {
		DimServer::setDnsNode(dimDnsNode.c_str());
	}
//	DimServer::addExitHandler(&dimServerExitHandler);
	//DimServer::addErrorHandler(DimErrorHandler *handler); // ??? think about useful implemenation of it

	// FedApi Service channels
	// create ordinary service channel(s)
	fpFedChannel = createDimServiceChannel(dimServiceName, dimChannelType);
	// create grouped services channel
	fpFedGroupedChannel = createDimServiceChannel(FedApiGroupedChannelName,
			FedApiGroupedServiceChannel_def);
	// create message channel
	fpFedMessage = createDimServiceChannel(FedApiMessageChannelName,
			FedApiMessageChannel_def);
	// create acknowledge channel
	fpFedAcknowledge = createDimServiceChannel(FedApiAcknowldegeChannelName,
			FedApiAcknowledgeChannel_def);

	// FedApi Command channels

	fpFedConfigureFeeCom = new DimCommand(FedApiConfigureFeeComName,
			FED_API_CONFIGURE_FEECOM_CMD_STRUCTURE,	this);

	// start FedServer (DimServer)
	start(FEDPortalFedServerName);
	DimServer::autoStartOn();
}


AliHLTFEDSubscriber::~AliHLTFEDSubscriber() {
	if (fpFedChannel != 0) {
		delete fpFedChannel;
	}
	if (fpFedMessage != 0) {
		delete fpFedMessage;
	}
	if (fpFedAcknowledge != 0) {
		delete fpFedAcknowledge;
	}
	if (fpFedGroupedChannel != 0) {
		delete fpFedGroupedChannel;
	}

	if (fpFedConfigureFeeCom != 0) {
		delete fpFedConfigureFeeCom;
	}

}





bool AliHLTFEDSubscriber::ProcessEvent(AliEventID_t, AliUInt32_t blockCnt,
		AliHLTSubEventDescriptor::BlockData* blocks,
		const AliHLTSubEventDataDescriptor*, const AliHLTEventTriggerStruct*,
		AliUInt8_t*, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>&,
		AliHLTEventDoneData*&) {

	size = 0; // no returned data -> size of retuned data is 0

	for (AliUInt32_t i = 0; i < blockCnt; i++) {
		AliUInt8_t* pBlockPayload = blocks[i].fData;
		AliUInt32_t blockPayloadSize = blocks[i].fSize;
		AliHLTEventDataType blockPayloadType = blocks[i].fDataType;
		//	AliUInt8_t* blockPayloadAttributes = blocks[i].fAttributes;  // check lateron for Byte ordering, etc

		AliUInt32_t currentSize = 0;	// keeps track about the currently read amount of payload data

		if (blockPayloadType.fID != FEDAPI_SERVICE_DATAID) {
			// data not for FED-Portal
			char tmp[9] = {0};
			for (unsigned aInt = 0; aInt < 8; aInt++) {
				tmp[aInt] = blockPayloadType.fDescr[aInt];
			}
			tmp[9] = 0;
			LOG(AliHLTLog::kDebug, "AliHLTFEDSubscriber::ProcessEvent", "Wrong data type")
					<< "received incorrect data: '" << tmp << "'." << ENDLOG;
			continue;
		}

		while ((currentSize + sizeof(FedApiServiceHeader)) < blockPayloadSize) {
			FedApiServiceHeader* pFASH = (FedApiServiceHeader*) (pBlockPayload + currentSize);
			// check for Offset: pFASH->fOffset == sizeof(FedApiServiceHeader)
			if (sizeof(FedApiServiceHeader) != pFASH->fOffset) {
				// corrupted data received
				LOG(AliHLTLog::kError, "AliHLTFEDSubscriber::ProcessEvent", "Corrupted data received")
						<< "received data does not match defined FED-Portal protocol (size)" << ENDLOG;
				// leave block, data is most likely corrupted
				break;
			}
			switch (pFASH->fPayloadType) {
				case (FED_API_SINGLE_SERVICE_DATA) :
					if (fillSingleServiceChannel((pBlockPayload +
							pFASH->fOffset), pFASH->fPayloadSize) != 0) {
						// error while sending data via single service channel
						LOG(AliHLTLog::kError, "AliHLTFEDSubscriber::ProcessEvent", "Single Service channel error")
								<< "Error while sending Single Service channel data over FED-Portal" << ENDLOG;
					}

					break;
				case (FED_API_GROUPED_SERVICE_DATA) :
					if (fillGroupedServiceChannel((pBlockPayload +
							pFASH->fOffset), pFASH->fPayloadSize) != 0) {
						// error while sending data via grouped service channel
						LOG(AliHLTLog::kError, "AliHLTFEDSubscriber::ProcessEvent", "Grouped Service channel error")
								<< "Error while sending Grouped Service channel data over FED-Portal" << ENDLOG;
					}

					break;
				case (FED_API_MESSAGE_SERVICE_DATA) :
					if (fillMessageChannel((pBlockPayload +
							pFASH->fOffset), pFASH->fPayloadSize) != 0) {
						// error while sending data via single service channel
						LOG(AliHLTLog::kError, "AliHLTFEDSubscriber::ProcessEvent", "Message Service channel error")
								<< "Error while sending Message Service channel data over FED-Portal" << ENDLOG;
					}
					break;
				default :
					LOG(AliHLTLog::kError, "AliHLTFEDSubscriber::ProcessEvent", "Unknown Service Type")
								<< "received unknown service channel type for FED-Portal '" << pFASH->fPayloadType << "'." << ENDLOG;
					// unknown FedApi Service data !!!
					break;

			} // end switch
			// jump to next payload block
			currentSize += sizeof(FedApiServiceHeader) + pFASH->fPayloadSize;

		} // end while-loop

	} // end for-loop

	return true;
}


int AliHLTFEDSubscriber::fillSingleServiceChannel(AliUInt8_t* pFirstElem,
		AliUInt32_t size) {
	AliUInt32_t counter = 0;
	while ((counter + (sizeof(AliUInt32_t) * 2) + sizeof(float)) < size) {
		FedApiServiceData* pFASD = (FedApiServiceData*) (pFirstElem + counter);
		// check if DON'T CARE value is set and if set only for one member
		if (((pFASD->fIntValue == FED_API_SERVICE_VALUE_INT_DONT_CARE) &&
				(pFASD->fFloatValue == FED_API_SERVICE_VALUE_FLOAT_DONT_CARE) &&
				(strcmp(pFASD->fCharValue, FED_API_SERVICE_VALUE_CHAR_DONT_CARE) == 0)) ||

				((pFASD->fIntValue != FED_API_SERVICE_VALUE_INT_DONT_CARE) &&
				(pFASD->fFloatValue != FED_API_SERVICE_VALUE_FLOAT_DONT_CARE) &&
				(strcmp(pFASD->fCharValue, FED_API_SERVICE_VALUE_CHAR_DONT_CARE) != 0))) {
			// log service values contain either both the DON'T CARE value or none
			LOG(AliHLTLog::kWarning, "AliHLTFEDSubscriber::fillSingleServiceChannel", "Single Service channel warning")
					<< "Received too many or too few 'DoNotCare-values' in Single Service channel protocol" << ENDLOG;
			// but send them anyway
		}

		// fill channel !!!

		// set counter to next block
		counter += (sizeof(AliUInt32_t) * 2) + sizeof(float) + pFASD->fNameLength;
		// short sleep before next possible update
		usleep(FedApiUpdateChannelSleep);
	}

	if (counter != size) {
		return ERROR_SIZE_MISMATCH;
	}
	return 0;
}


int AliHLTFEDSubscriber::fillGroupedServiceChannel(AliUInt8_t* pFirstElem,
		AliUInt32_t size) {
	AliUInt32_t counter = 0;
	FedApiValueGrouped* grp = 0;
	while ((counter + (sizeof(AliUInt32_t) * 2) + sizeof(float)) < size) {
		FedApiServiceData* pFASD = (FedApiServiceData*) (pFirstElem + counter);
		// check if DON'T CARE value is set and if set only for one member
		if (((pFASD->fIntValue == FED_API_SERVICE_VALUE_INT_DONT_CARE) &&
				(pFASD->fFloatValue == FED_API_SERVICE_VALUE_FLOAT_DONT_CARE) &&
				(strcmp(pFASD->fCharValue, FED_API_SERVICE_VALUE_CHAR_DONT_CARE) == 0)) ||
				((pFASD->fIntValue != FED_API_SERVICE_VALUE_INT_DONT_CARE) &&
				(pFASD->fFloatValue != FED_API_SERVICE_VALUE_FLOAT_DONT_CARE) &&
				(strcmp(pFASD->fCharValue, FED_API_SERVICE_VALUE_CHAR_DONT_CARE) != 0))) {
			// log service values contain either both the DON'T CARE value or none
			LOG(AliHLTLog::kWarning, "AliHLTFEDSubscriber::fillGroupedServiceChannel", "Grouped Service channel warning")
					<< "Received too many or too few 'DoNotCare-values' in Grouped Service channel protocol" << ENDLOG;

			// but send them anyway
		}

		// fill channel
		string name(pFASD->fName);
		grp = new FedApiValueGrouped(pFASD->fIntValue, pFASD->fFloatValue, 
				pFASD->fCharValue, name);
		fpFedGroupedChannel->updateVal((FedApiValue*) grp);

		// set counter to next block
		counter += (sizeof(AliUInt32_t) * 2) + sizeof(float) + pFASD->fNameLength;
		delete grp; // delete grouped service container
		// short sleep before next possible update
		usleep(FedApiUpdateChannelSleep);
	}

	if (counter != size) {
		return ERROR_SIZE_MISMATCH;
	}
	return 0;
}


int AliHLTFEDSubscriber::fillMessageChannel(AliUInt8_t* pFirstElem,
		AliUInt32_t size) {
	AliUInt32_t counter = 0;
	FedApiValueMSG* msg = 0;
	while (counter < size) {
		FedApiMessageData* pFAMD = (FedApiMessageData*) (pFirstElem + counter);
		// make local log entry, before check for FedChannel !!! ???

		// if log level does not match go to next
		if (!(pFAMD->fMessageType & fLogLevel)) {
			counter += sizeof(FedApiMessageData);
    		continue;
		}

		if (pFAMD->fDate[0] != 0) {
			string date(pFAMD->fDate);
			msg = new FedApiValueMSG(pFAMD->fMessageType, pFAMD->fSource,
					pFAMD->fDescription, date);
		} else {
			msg = new FedApiValueMSG(pFAMD->fMessageType, pFAMD->fSource,
					pFAMD->fDescription);
		}

		// fill channel and trigger update
		fpFedMessage->updateVal((FedApiValue*) msg);

		// set counter to next block
		counter += sizeof(FedApiMessageData);
		delete msg;	// delete message container
		// short sleep before next possible update
		usleep(FedApiUpdateChannelSleep);
	}

	if (counter != size) {
		return ERROR_SIZE_MISMATCH;
	}
	return 0;
}


//// DIM related functions ///////
/*
void AliHLTFEDSubscriber::dimServerExitHandler(int exitCode) {

	// check error code given by framework
	switch(exitCode) {
		case(DIMDNSUNDEF):
			cout << "DIM_DNS_NODE undefined, exiting component ..." << endl;
			// !!! log entry and make an clean exit
			break;
		case(DIMDNSREFUS):
			cout << "DIM_DNS refuses connection, exiting component ..." << endl;
			// !!! log entry and make an clean exit
			break;
		case(DIMDNSDUPLC):
			cout << "Service already exists in DNS,  exiting component ..." << endl;
			// !!! log entry and make an clean exit
			break;
		case(DIMDNSEXIT):
			cout << "DNS requests server to EXIT, exiting component ..." << endl;
			// !!! log entry and make an clean exit
			break;
		case(DIMDNSCNERR):
			cout << "Connection to DNS failed, exiting component ..." << endl;
			// !!! log entry and make an clean exit
			break;
		default:
			cout << "Received exit command via DIM with unknown reason, ignoring command ..." << endl;
			break;
	}

}
*/

FedApiServiceChannel* AliHLTFEDSubscriber::createDimServiceChannel(string name,
			string type) {
	FedApiServiceChannel* channel = 0;
	FedApiValue* pChannelValue = 0;

	if (type.compare(FedApiSingleServiceChannelInt_def) == 0) {
		pChannelValue = new FedApiValueInt();
		channel = new FedApiSingleServiceChannelInt(name, pChannelValue);

	} else if (type.compare(FedApiSingleServiceChannelFloat_def) == 0) {
		pChannelValue = new FedApiValueFloat();
		channel = new FedApiSingleServiceChannelFloat(name, pChannelValue);

	} else if (type.compare(FedApiSingleServiceChannelChar_def) == 0) {
		pChannelValue = new FedApiValueChar();
		channel = new FedApiSingleServiceChannelChar(name, pChannelValue);

	} else if (type.compare(FedApiGroupedServiceChannel_def) == 0) {
		pChannelValue = new FedApiValueGrouped();
		channel = new FedApiGroupedServiceChannel(name, pChannelValue);

	} else if (type.compare(FedApiMessageChannel_def) == 0) {
		pChannelValue = new FedApiValueMSG();
		channel = new FedApiMessageChannel(name, pChannelValue);

	} else if (type.compare(FedApiAcknowledgeChannel_def) == 0) {
		pChannelValue = new FedApiValueACK();
		channel = new FedApiGroupedServiceChannel(name, pChannelValue);

	} else {
		LOG(AliHLTLog::kError, "AliHLTFEDSubscriber::createDimServiceChannel", "unknown channel type")
				<< "FED-Portal received parameter to create Service channel with unknown type" << ENDLOG;

	// throw an exception, cause of unknown type !!!

	}
	if (pChannelValue != 0) {
		delete pChannelValue;
	}

	return channel;
}


void AliHLTFEDSubscriber::commandHandler() {
	// fill with logic
	DimCommand* cmnd = getCommand();
//	int commandSize = cmnd->getSize();
	int nRet = 0;
	ConfigureFeeCom* data = (ConfigureFeeCom*) cmnd->getData();
	FedApiValueACK* faa = 0;
	char resData[20];

	switch (data->commandType) {
		case (FED_API_SET_LOG_LEVEL):
			nRet = setLogLevel(data->iVal);
			faa = new FedApiValueACK(nRet);
			// set ACK
			fpFedAcknowledge->updateVal((FedApiValue*) faa);
			break;
		case (FED_API_GET_LOG_LEVEL):
			resData[sprintf(resData, "%d", getLogLevel())] = 0;
			faa = new FedApiValueACK(0, resData,
					strlen(resData) + 1);
			// set ACK
			fpFedAcknowledge->updateVal((FedApiValue*) faa);
			break;
		default:
			LOG(AliHLTLog::kWarning, "AliHLTFEDSubscriber::commandHandler", "unknown command type")
					<< "FED-Portal received unknown or unimplemented FED-Command" << ENDLOG;
			// not defined ...
			//send error over ACK channel
			resData[sprintf(resData, "Invalid Command")] = 0;
			faa = new FedApiValueACK(FED_API_INVALID_COMMAND,
					resData, strlen(resData) + 1);
			// set ACK
			fpFedAcknowledge->updateVal((FedApiValue*) faa);

	}
	if (faa != 0) {
		delete faa;
	}
}

int AliHLTFEDSubscriber::setLogLevel(int logLevel) {
	if ((logLevel > MSG_MAX_VAL) || (logLevel < 0)) {
		LOG(AliHLTLog::kWarning, "AliHLTFEDSubscriber::setLogLevel", "wrong log level command")
				<< "FED-Server received wrong log level ('" << logLevel << "') to set in FED-Portal." << ENDLOG;
		// wrong log Level, discard setting,
		return FED_API_INVALID_PARAMETER;
	} else {
		fLogLevel = logLevel | MSG_ALARM;
		LOG(AliHLTLog::kInformational, "AliHLTFEDSubscriber::setLogLevel", "new log level set")
				<< "FED-Server received new log level ('" << logLevel << "') for FED-Portal." << ENDLOG;
    }
    return 0;
}


