/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/
#ifndef ALI_HLT_FED_PORTAL_TYPES_H
#define ALI_HLT_FED_PORTAL_TYPES_H

#include "fee_loglevels.h"

#ifdef __cplusplus
extern "C" {
#endif


#if SHRT_MAX==2147483647
typedef signed short AliSInt32_t;
#else // SHRT_MAX==2147483647

#if INT_MAX==2147483647
typedef signed int AliSInt32_t;
#else // INT_MAX==2147483647

#if LONG_MAX==2147483647
typedef signed long AliSInt32_t;
#else // LONG_MAX==2147483647

#error Could not typedef AliSInt32_t

#endif // LONG_MAX==2147483647

#endif // INT_MAX==2147483647

#endif // SHRT_MAX==2147483647




/**
 * This struct represents the header FedApi Service data
 *
 * @see DCSPortalFedApiDefines.h for more details
 */
typedef struct FedApiServiceHeader {
	/**
	 * Contains the size of the header offset in Bytes.
	 */
	AliUInt32_t fOffset;

	/**
	 * Defines the type of FedApi Service data in the payload.
	 */
	AliUInt8_t fPayloadType;

	/**
	 * Contains the size of the Payload in Bytes
	 */
	 AliUInt32_t fPayloadSize;

} FedApiServiceHeader;


/**
 * This struct represents the structure of FedApi Service data in the Pubsub
 * system. Note: The mapping to the datatypes used in the actual Dim channel
 * is performed in the DCSPortal component, therefore two datatype definitions
 * of the FedApiServiceData exists (one using the AliUInt32_t the other normal
 * int). This is necessary, because the PubSub system maybe distributed over a
 * heterogeneous cluster.
 *
 * @see DCSPortalFedApiDefines.h for more details
 */
typedef struct FedApiServiceData {
	/**
	 * length of the name of the service (value) (inclusive Null-termination)
	 */
	AliUInt32_t fNameLength;  // remove that... in new desing not required

	/**
	 * A 32bit integer representing the service; if the service is NOT
	 * represented by an integer, this value should contain the DON'T CARE
	 * value: -9999999 [FED_API_SERVICE_VALUE_INT_DONT_CARE]
	 *
	 * @see FedApiValue.hpp
	 */
	 AliSInt32_t fIntValue;

	/**
	 * A float representing the service; if the service is NOT
	 * represented by a float, this value should contain the DON'T CARE
	 * value: -9999999.0 [FED_API_SERVICE_VALUE_FLOAT_DONT_CARE]
	 *
	 * @see FedApiValue.hpp
	 */
	float fFloatValue;

	/**
	 * The name tof the represented value (service).
	 * Must be NULL terminated.
	 *
	 */
	char fName[MAX_SERVICE_NAME_LENGTH];

	/**
	 * A char array representing the service; if the service is NOT
	 * represented by a char, this value should contain the DON'T CARE
	 * value: "NONE" [FED_API_SERVICE_VALUE_CHAR_DONT_CARE]
	 *
	 * @see FedApiValue.hpp
	 */
	char fCharValue[MAX_CHAR_SERVICE_LENGTH];



} FedApiServiceData;


/**
 * This struct represents the structure of FedApi Message data on the
 * PubSub system. Note: The mapping to the datatypes used in the actual Dim
 * channel is performed in the DCSPortal component, therefore two datatype
 * definitions of the "FedApiMessageData"/"FedApiMessage" exists (one using
 * the AliUInt32_t the other normal int). This is necessary, because the PubSub
 * system maybe distributed over a heterogeneous cluster.
 * NOTE: this struct contains not the DETECTOR member, which is also mandatory
 * for the data type used in the DIM channel. Since all messages via this
 * portal are from detector "HLT" this is set for all automatically in the
 * portal.
 *
 * @see DCSPortalFedApiDefines.h for more details
 */
typedef struct FedApiMessageData {
	/**
	 * The type of event (MSG_INFO, MSG_ERROR, etc)
	 */
	AliUInt32_t fMessageType;

	/**
	 * The origin of this message.
	 */
	char fSource[MSG_SOURCE_SIZE];

	/**
	 * The message itself.
	 */
	char fDescription[MSG_DESCRIPTION_SIZE];

	/**
	 * The date of the message ("YYYY-MM-DD hh:mm:ss\0").
	 */
	char fDate[MSG_DATE_SIZE];

} FedApiMessageData;



#ifdef __cplusplus
}
#endif

#endif //ALI_HLT_FED_PORTAL_TYPES_H
