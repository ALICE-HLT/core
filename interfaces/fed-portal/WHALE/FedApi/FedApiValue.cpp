/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/
#include "FedApiValue.hpp"

#include <time.h>

using namespace std;


////////////////////////////////// INT VALUE //////////////////////////////////
FedApiValueInt::FedApiValueInt() {
	mSize = sizeof(SingleVal);
	mChannelData.iVal = FED_API_SERVICE_VALUE_INT_INIT_VALUE;
	mChannelData.fVal = FED_API_SERVICE_VALUE_FLOAT_DONT_CARE;
	strcpy(mChannelData.cVal, FED_API_SERVICE_VALUE_CHAR_DONT_CARE);
}

FedApiValueInt::FedApiValueInt(int aInt) {
	mSize = sizeof(SingleVal);
	mChannelData.iVal = aInt;
	mChannelData.fVal = FED_API_SERVICE_VALUE_FLOAT_DONT_CARE;
	strcpy(mChannelData.cVal, FED_API_SERVICE_VALUE_CHAR_DONT_CARE);
}


FedApiValueInt::~FedApiValueInt() {

}


unsigned int FedApiValueInt::getSize() {
	return mSize;
}


void* FedApiValueInt::getValueData() {
	return (void*) &mChannelData;
}


void FedApiValueInt::setValueData(FedApiValue* value) {
	mChannelData.iVal = ((FedApiValueInt*) value)->getChannelIntVal();
}


int FedApiValueInt::getChannelIntVal() {
	return mChannelData.iVal;
}


///////////////////////////////// FLOAT VALUE /////////////////////////////////
FedApiValueFloat::FedApiValueFloat() {
	mSize = sizeof(SingleVal);
	mChannelData.fVal = FED_API_SERVICE_VALUE_FLOAT_INIT_VALUE;
	mChannelData.iVal = FED_API_SERVICE_VALUE_INT_DONT_CARE;
	strcpy(mChannelData.cVal, FED_API_SERVICE_VALUE_CHAR_DONT_CARE);
}

FedApiValueFloat::FedApiValueFloat(float aFloat) {
	mSize = sizeof(SingleVal);
	mChannelData.fVal = aFloat;
	mChannelData.iVal = FED_API_SERVICE_VALUE_INT_DONT_CARE;
	strcpy(mChannelData.cVal, FED_API_SERVICE_VALUE_CHAR_DONT_CARE);
}


FedApiValueFloat::~FedApiValueFloat() {
}


unsigned int FedApiValueFloat::getSize() {
	return mSize;
}


void* FedApiValueFloat::getValueData() {
	return (void*) &mChannelData;
}


void FedApiValueFloat::setValueData(FedApiValue* value) {
	mChannelData.fVal = ((FedApiValueFloat*) value)->getChannelFloatVal();
}


float FedApiValueFloat::getChannelFloatVal() {
	return mChannelData.fVal;
}

////////////////////////////////// CHAR VALUE //////////////////////////////////
FedApiValueChar::FedApiValueChar() {
	mSize = sizeof(SingleVal);
	mChannelData.iVal = FED_API_SERVICE_VALUE_INT_DONT_CARE;
	mChannelData.fVal = FED_API_SERVICE_VALUE_FLOAT_DONT_CARE;
	strcpy(mChannelData.cVal, FED_API_SERVICE_VALUE_CHAR_INIT_VALUE);
}

FedApiValueChar::FedApiValueChar(char* aChar) {
	mSize = sizeof(SingleVal);
	mChannelData.iVal = FED_API_SERVICE_VALUE_INT_DONT_CARE;
	mChannelData.fVal = FED_API_SERVICE_VALUE_FLOAT_DONT_CARE;
	if (strlen(aChar) > MAX_CHAR_SERVICE_LENGTH) {
		strcpy(mChannelData.cVal, FED_API_SERVICE_VALUE_CHAR_INIT_VALUE);
	} else {
		strcpy(mChannelData.cVal, aChar);
	}
}


FedApiValueChar::~FedApiValueChar() {

}


unsigned int FedApiValueChar::getSize() {
	return mSize;
}


void* FedApiValueChar::getValueData() {
	return (void*) &mChannelData;
}


void FedApiValueChar::setValueData(FedApiValue* value) {
	strcpy(mChannelData.cVal, ((FedApiValueChar*) value)->getChannelCharVal());
}


char* FedApiValueChar::getChannelCharVal() {
	return mChannelData.cVal;
}


///////////////////////////////// GROUPED VALUE /////////////////////////////////
FedApiValueGrouped::FedApiValueGrouped() {
	mChannelData.setVal(FED_API_SERVICE_VALUE_INT_INIT_VALUE,
			FED_API_SERVICE_VALUE_FLOAT_INIT_VALUE,
			FED_API_SERVICE_VALUE_CHAR_INIT_VALUE, "INIT");
	mSize = mChannelData.getFullSize();

}


FedApiValueGrouped::FedApiValueGrouped(int aInt, string name) {
	mChannelData.setVal(aInt, FED_API_SERVICE_VALUE_FLOAT_DONT_CARE,
			FED_API_SERVICE_VALUE_CHAR_INIT_VALUE, name.c_str());
	mSize = mChannelData.getFullSize();

}


FedApiValueGrouped::FedApiValueGrouped(float aFloat, string name) {
	mChannelData.setVal(FED_API_SERVICE_VALUE_INT_DONT_CARE, aFloat,
			FED_API_SERVICE_VALUE_CHAR_INIT_VALUE, name.c_str());
	mSize = mChannelData.getFullSize();

}


FedApiValueGrouped::FedApiValueGrouped(char* aCharVal, string name) {
	mChannelData.setVal(FED_API_SERVICE_VALUE_INT_DONT_CARE,
			FED_API_SERVICE_VALUE_FLOAT_DONT_CARE, aCharVal,
			name.c_str());
	mSize = mChannelData.getFullSize();

}


FedApiValueGrouped::FedApiValueGrouped(int aInt, float aFloat, char* aCharVal, string name) {
	mChannelData.setVal(aInt, aFloat, aCharVal, name.c_str());
	mSize = mChannelData.getFullSize();

}


FedApiValueGrouped::~FedApiValueGrouped() {
}


unsigned int FedApiValueGrouped::getSize() {
	return mChannelData.getFullSize();
}


void* FedApiValueGrouped::getValueData() {
	return mChannelData.marshallData();
}

void FedApiValueGrouped::setValueData(FedApiValue* value) {
	mChannelData = ((FedApiValueGrouped*) value)->getChannelDataObj();
	mSize = mChannelData.getFullSize();
}

GroupedVal FedApiValueGrouped::getChannelDataObj() {
	return mChannelData;

}

string FedApiValueGrouped::getCurrentChannelValues(int* intVal, float* floatVal, char** charVal) {
	string name;
	const char* pTemp = 0;

	if ((intVal == 0) || (floatVal == 0) || (*charVal == 0)) {
		return "NULL pointer entered";
	}

	*charVal = new char[MAX_CHAR_SERVICE_LENGTH];

	*intVal = mChannelData.getIntVal();
	*floatVal = mChannelData.getFloatVal();
	strcpy(*charVal, mChannelData.getCharVal());
	pTemp = mChannelData.getName();

	if (pTemp == 0) {
		name = "No name given";
	} else {
		name = pTemp;
	}

	return name;
}


////////////////////////////////// MSG VALUE //////////////////////////////////
FedApiValueMSG::FedApiValueMSG() {
	time_t timeVal; // needed for timestamp
	struct tm* now; // needed for timestamp

	time(&timeVal); // retrieve current time
	now = localtime(&timeVal);

	mSize = sizeof(FedApiMessage);

	// fill MessageStruct
	mChannelData.eventType = MSG_INFO;
	mChannelData.detector[sprintf(mChannelData.detector, "HLT")] = 0;
	mChannelData.source[sprintf(mChannelData.source, "DCSPortal component")] = 0;
	mChannelData.description[sprintf(mChannelData.description,
			"HLT cluster has started the DCSPortal component, ready to serve as FedApi.")]
			= 0;
	mChannelData.date[strftime(mChannelData.date, 20, "%Y-%m-%d %H:%M:%S", now)]
			= 0;

}


FedApiValueMSG::FedApiValueMSG(unsigned int msgType, string origin,
		string description, string date) {
	time_t timeVal; // needed for timestamp
	struct tm* now; // needed for timestamp

	time(&timeVal); // retrieve current time
	now = localtime(&timeVal);

	mSize = sizeof(FedApiMessage);

	// fill MessageStruct
	mChannelData.eventType = msgType;
	mChannelData.detector[sprintf(mChannelData.detector, "HLT")] = 0;
	if (origin.length() < 256) {
		mChannelData.source[sprintf(mChannelData.source, "%s", origin.c_str())] = 0;
	} else {
		strncpy(mChannelData.source, origin.c_str(), 255);
		mChannelData.source[255] = 0;
	}
	if (origin.length() < 256) {
		mChannelData.description[sprintf(mChannelData.description, "%s",
				description.c_str())] = 0;
	} else {
		strncpy(mChannelData.description, description.c_str(), 255);
		mChannelData.description[255] = 0;
	}

	if ((date.compare("NULL") == 0) || (date.length() >= 20)) {
		mChannelData.date[strftime(mChannelData.date, 20, "%Y-%m-%d %H:%M:%S", now)]
				= 0;
	} else {
		mChannelData.date[sprintf(mChannelData.date, "%s", date.c_str())] = 0;
	}
}


FedApiValueMSG::~FedApiValueMSG() {
}


unsigned int FedApiValueMSG::getSize() {
	return mSize;
}


void* FedApiValueMSG::getValueData() {
	return (void*) &mChannelData;
}


void FedApiValueMSG::setValueData(FedApiValue* value) {
	mChannelData = ((FedApiValueMSG*) value)->getChannelVal();
}


FedApiMessage FedApiValueMSG::getChannelVal() {
	return mChannelData;
}


////////////////////////////////// ACK VALUE //////////////////////////////////
FedApiValueACK::FedApiValueACK() {
	mChannelData.setVal(0, 0, 0);
	mSize = mChannelData.getFullSize();
}


FedApiValueACK::FedApiValueACK(int resCode, char* resData,
		unsigned int size) {

	if (mChannelData.setVal(resCode, resData, size) == ERROR_NULL_POINTER) {
		mChannelData.setVal(resCode, 0, 0);
	}

	mSize = mChannelData.getFullSize();
}


FedApiValueACK::~FedApiValueACK() {
}


unsigned int FedApiValueACK::getSize() {
	return mChannelData.getFullSize();
}


void* FedApiValueACK::getValueData() {
	return mChannelData.marshallData();
}


void FedApiValueACK::setValueData(FedApiValue* value) {
	mChannelData = ((FedApiValueACK*) value)->getChannelDataObj();
	mSize = mChannelData.getFullSize();
}


FedApiAcknowledge FedApiValueACK::getChannelDataObj() {
	return mChannelData;
}

