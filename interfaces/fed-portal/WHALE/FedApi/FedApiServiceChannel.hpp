/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/
#ifndef FED_API_SERVICE_CHANNEL_HPP
#define FED_API_SERVICE_CHANNEL_HPP

#include "dim/dis.hxx"
#include "FedApiValue.hpp"

#include <string>


/**
 * Define for the structure of a SingleService Channel (FedApi)
 */
#define FED_API_SINGLE_SERVICE_CHANNEL_STRUCTURE "I:1;F:1;C:256"

/**
 * Define for the structure of a GroupedService Channel (FedApi)
 */
#define FED_API_GROUPED_SERVICE_CHANNEL_STRUCTURE "I:1;F:1;C:256;C:256"

/**
 * Define for the structure of a Message Channel (FedApi)
 */
#define FED_API_MESSAGE_CHANNEL_STRUCTURE "I:1;C:4;C:256;C:256;C:20"

/**
 * Define for the structure of a Acknowledge Channel (FedApi)
 */
#define FED_API_ACKNOWLEDGE_CHANNEL_STRUCTURE "I:1;C"


/////////////////////////////////// CHANNEL ///////////////////////////////////
/**
 * This class is the parent for FedApi Service Channels
 *
 * @author Sebastian Bablok
 *
 * @date 2006-09-12
 */
class FedApiServiceChannel {
    public:
		/**
		 * Constructor for a FedApiServiceChannel
		 *
		 * @param name name of the channel
		 * @param structure structure of the channel
		 */
		FedApiServiceChannel(std::string name, std::string structure) :
				mChannelName(name), mChannelStructure(structure) {};

		/**
		 * Destructor for a FedApiServiceChannel
		 */
		virtual ~FedApiServiceChannel() {};


		/**
		 * Function to set a new Service Channel value
		 *
		 * @param value the value to set
		 *
		 * @return 0 in case of success, else an error code
		 */
		virtual int updateVal(FedApiValue* value) = 0;

		/**
		 * Getter for the FedApiValue
		 *
		 * @return pointer to the FedApiValue corresponding to this channel
		 */
		virtual const FedApiValue* getChannelValue() = 0;



    protected:
    	/**
    	 * The DimService object representing the channel
    	 */
    	DimService* mpDimService;

    	/**
		 * The structure of thius Dim Channel
		 */
    	std::string mChannelName;

		/**
		 * The structure of thius Dim Channel
		 */
    	std::string mChannelStructure;





    private:

}; // end of FedApiServiceChannel





/////////////////////////////////// SINGLE ////////////////////////////////////
/**
 * This class represents a Single Service Channel of the FedApi
 *
 * @author Sebastian Bablok
 *
 * @date 2006-09-12
 */
class FedApiSingleServiceChannel : public FedApiServiceChannel {
    public:
		/**
		 * Constructor for a FedApiSingleServiceChannel
		 *
		 * @param name name of the channel
		 * @param structure structure of the channel
		 */
		FedApiSingleServiceChannel(std::string name, std::string structure):
				FedApiServiceChannel(name, structure) {};

		/**
		 * Destructor for a FedApiSingleServiceChannel
		 */
		virtual ~FedApiSingleServiceChannel() {};

		/**
		 * Function to set a new Service Channel value
		 *
		 * @param value the value to set
		 *
		 * @return 0 in case of success, else an error code
		 */
		virtual int updateVal(FedApiValue* value) = 0;

		/**
		 * Getter for the FedApiValue
		 *
		 * @return pointer to the FedApiValue corresponding to this channel
		 */
		virtual const FedApiValue* getChannelValue() = 0;


    protected:



    private:


}; // end of FedApiSingleServiceChannel


///////////////////////////////// SINGLE INT //////////////////////////////////
/**
 * This class represents a Single Service Channel with an "int" as value for
 * the FedApi.
 *
 * @author Sebastian Bablok
 *
 * @date 2006-09-12
 */
class FedApiSingleServiceChannelInt : public FedApiSingleServiceChannel {
    public:
		/**
		 * Constructor for a FedApiSingleServiceChannelInt
		 *
		 * @param name name of the channel
		 * @param initValue the initial value
		 */
		FedApiSingleServiceChannelInt(std::string name, FedApiValue* initValue);

		/**
		 * Destructor for a FedApiSingleServiceChannelInt
		 */
		virtual ~FedApiSingleServiceChannelInt();

		/**
		 * Function to set a new Service Channel value
		 *
		 * @param value the value to set
		 *
		 * @return 0 in case of success, else an error code
		 */
		virtual int updateVal(FedApiValue* value);

		/**
		 * Getter for the FedApiValue
		 *
		 * @return pointer to the FedApiValue corresponding to this channel
		 */
		virtual const FedApiValue* getChannelValue();


    protected:
    	/**
    	 * Value for this channel
    	 */
    	FedApiValueInt mValue;




    private:


}; // end of FedApiSingleServiceChannelInt


//////////////////////////////// SINGLE FLOAT /////////////////////////////////
/**
 * This class represents a Single Service Channel with an "float" as value for
 * the FedApi.
 *
 * @author Sebastian Bablok
 *
 * @date 2006-09-12
 */
class FedApiSingleServiceChannelFloat : public FedApiSingleServiceChannel {
    public:
		/**
		 * Constructor for a FedApiSingleServiceChannelFloat
		 *
		 * @param name name of the channel
		 * @param initValue the initial value
		 */
		FedApiSingleServiceChannelFloat(std::string name, FedApiValue* initValue);

		/**
		 * Destructor for a FedApiSingleServiceChannelFloat
		 */
		virtual ~FedApiSingleServiceChannelFloat();

		/**
		 * Function to set a new Service Channel value
		 *
		 * @param value the value to set
		 *
		 * @return 0 in case of success, else an error code
		 */
		virtual int updateVal(FedApiValue* value);

		/**
		 * Getter for the FedApiValue
		 *
		 * @return pointer to the FedApiValue corresponding to this channel
		 */
		virtual const FedApiValue* getChannelValue();


    protected:
		/**
		 * Value for this channel
		 */
    	FedApiValueFloat mValue;



    private:


}; // end of FedApiSingleServiceChannelFloat


//////////////////////////////// SINGLE CHAR /////////////////////////////////
/**
 * This class represents a Single Service Channel with an char array as value
 * for the FedApi.
 *
 * @author Sebastian Bablok
 *
 * @date 2006-09-12
 */
class FedApiSingleServiceChannelChar : public FedApiSingleServiceChannel {
    public:
		/**
		 * Constructor for a FedApiSingleServiceChannelChar
		 *
		 * @param name name of the channel
		 * @param initValue the initial value
		 */
		FedApiSingleServiceChannelChar(std::string name, FedApiValue* initValue);

		/**
		 * Destructor for a FedApiSingleServiceChannelChar
		 */
		virtual ~FedApiSingleServiceChannelChar();

		/**
		 * Function to set a new Service Channel value
		 *
		 * @param value the value to set
		 *
		 * @return 0 in case of success, else an error code
		 */
		virtual int updateVal(FedApiValue* value);

		/**
		 * Getter for the FedApiValue
		 *
		 * @return pointer to the FedApiValue corresponding to this channel
		 */
		virtual const FedApiValue* getChannelValue();


    protected:
		/**
		 * Value for this channel
		 */
    	FedApiValueChar mValue;



    private:


}; // end of FedApiSingleServiceChannelChar


/////////////////////////////////// GROUPED ///////////////////////////////////
/**
 * This class represents a Grouped Service Channel of the FedApi
 *
 * @author Sebastian Bablok
 *
 * @date 2006-09-12
 */
class FedApiGroupedServiceChannel : public FedApiServiceChannel {
    public:
		/**
		 * Constructor for a FedApiGroupedServiceChannel
		 *
		 * @param name name of the channel
		 * @param initValue the initial value
		 */
		FedApiGroupedServiceChannel(std::string name, FedApiValue* initValue);

		/**
		 * Destructor for a FedApiGroupedServiceChannel
		 */
		virtual ~FedApiGroupedServiceChannel();

		/**
		 * Function to set a new Service Channel value
		 *
		 * @param value the value to set
		 *
		 * @return 0 in case of success, else an error code
		 */
		virtual int updateVal(FedApiValue* value);

		/**
		 * Getter for the FedApiValue
		 *
		 * @return pointer to the FedApiValue corresponding to this channel
		 */
		virtual const FedApiValue* getChannelValue();


    protected:
		/**
		 * Value for this channel
		 */
    	FedApiValueGrouped mValue;


    private:


}; // end of FedApiGroupedServiceChannel



///////////////////////////////////// MSG /////////////////////////////////////
/**
 * This class represents a Message Channel of the FedApi
 *
 * @author Sebastian Bablok
 *
 * @date 2006-09-12
 */
class FedApiMessageChannel : public FedApiServiceChannel {
    public:
		/**
		 * Constructor for a FedApiMessageChannel
		 *
		 * @param name name of the channel
		 * @param initValue the initial value
		 */
		FedApiMessageChannel(std::string name, FedApiValue* initValue);

		/**
		 * Destructor for a FedApiMessageChannel
		 */
		virtual ~FedApiMessageChannel();

		/**
		 * Function to set a new MSG Channel value
		 *
		 * @param value the value to set
		 *
		 * @return 0 in case of success, else an error code
		 */
		virtual int updateVal(FedApiValue* value);

		/**
		 * Getter for the FedApiValue
		 *
		 * @return pointer to the FedApiValue corresponding to this channel
		 */
		virtual const FedApiValue* getChannelValue();


    protected:
		/**
		 * Value for this channel
		 */
    	FedApiValueMSG mValue;


    private:


}; // end of FedApiMessageChannel


///////////////////////////////////// ACK /////////////////////////////////////
/**
 * This class represents a Acknowledge Channel of the FedApi
 *
 * @author Sebastian Bablok
 *
 * @date 2006-09-12
 */
class FedApiAcknowledgeChannel : public FedApiServiceChannel {
    public:
		/**
		 * Constructor for a FedApiAcknowledgeChannel
		 *
		 * @param name name of the channel
		 * @param initValue the initial value
		 */
		FedApiAcknowledgeChannel(std::string name, FedApiValue* initValue);

		/**
		 * Destructor for a FedApiAcknowledgeChannel
		 */
		virtual ~FedApiAcknowledgeChannel();

		/**
		 * Function to set a new ACK Channel value
		 *
		 * @param value the value to set
		 *
		 * @return 0 in case of success, else an error code
		 */
		virtual int updateVal(FedApiValue* value);

		/**
		 * Getter for the FedApiValue
		 *
		 * @return pointer to the FedApiValue corresponding to this channel
		 */
		virtual const FedApiValue* getChannelValue();


    protected:
		/**
		 * Value for this channel
		 */
    	FedApiValueMSG mValue;


    private:


}; // end of FedApiAcknowledgeChannel


#endif //FED_API_SERVICE_CHANNEL_HPP
