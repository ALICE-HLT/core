/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/
#ifndef FED_API_VALUE_HPP
#define FED_API_VALUE_HPP

#include "fee_loglevels.h"

#include <string>


/**
 * Define for "Don't care" of his 'int' value
 */
#define FED_API_SERVICE_VALUE_INT_DONT_CARE -9999999

/**
 * Define for "Don't care" of this 'float' value
 */
#define FED_API_SERVICE_VALUE_FLOAT_DONT_CARE -9999999.0

/**
 * Define for "init value" of 'int' value for Service channel
 */
#define FED_API_SERVICE_VALUE_INT_INIT_VALUE 0

/**
 * Define for "init value" of 'float' value for Service channel
 */
#define FED_API_SERVICE_VALUE_FLOAT_INIT_VALUE 0.0

/**
 * Define for Error code cause of NULL Pointer
 */
#define ERROR_NULL_POINTER -10

/**
 * Define for Error code cause of char array too long
 */
#define ERROR_ARRAY_TOO_LONG -13


/**
 * Define for the maximum name length of a service
 */
#define MAX_SERVICE_NAME_LENGTH 256

/**
 * Define for the maximum data length of a char ervice
 */
#define MAX_CHAR_SERVICE_LENGTH 256

/**
 * Define for "init value" of 'char' value for Service channel
 */
#define FED_API_SERVICE_VALUE_CHAR_INIT_VALUE "\0"

/**
 * Define for "Don't care" of his 'char' value
 */
#define FED_API_SERVICE_VALUE_CHAR_DONT_CARE "NONE"

/**
 * Define for "init value" for Service name
 */
#define FED_API_SERVICE_NAME_INIT_VALUE "NoName"

/**
 *  * Define of Grouped Service channel
 *   */
#define FED_API_GROUPED_SERVICE_CHANNEL_SIZE (sizeof(int)+sizeof(float)+MAX_SERVICE_NAME_LENGTH+MAX_CHAR_SERVICE_LENGTH)


/**
 * struct containing the channel data for single value
 */
typedef struct SingleValStruct {
	/**
	 * int val of Single Service channel
	 */
	int iVal;

	/**
	 * float val of Single Service channel
	 */
	float fVal;

	/**
	 * char val of Single Service channel
	 */
	char cVal[MAX_CHAR_SERVICE_LENGTH];
} SingleVal;


/**
 * Struct for a message over the FedApi
 */
typedef struct MessageStruct {
	/**
	 * The type of event (MSG_INFO, MSG_ERROR, etc)
	 */
	unsigned int eventType;

	/**
	 * The detector type (TRD, TPC, SPD, HLT)
	 */
	char detector[MSG_DETECTOR_SIZE];

	/**
	 * The origin of this message.
	 */
	char source[MSG_SOURCE_SIZE];

	/**
	 * The message itself.
	 */
	char description[MSG_DESCRIPTION_SIZE];

	/**
	 * The date of the message ("YYYY-MM-DD hh:mm:ss\0").
	 */
	char date[MSG_DATE_SIZE];
} FedApiMessage;


/**
 * class containing the channel data for the ACK channel
 */
class FedApiAcknowledge {
	private:
		/**
		 * integer value representing the result code
		 */
		int resultCode;

		/**
		 * pointer to optional / additonal result data
		 */
		char* resultData;

		/**
		 * Stores the actual used result data size
		 */
		unsigned int realResultDataSize;

		/**
		 * the size of the result data
		 */
		unsigned int resultDataMemSize;

		/**
		 * Container storing the marshalled channel data
		 */
		unsigned char* marshalledData;

		/**
		 * Size of the memory allocated for marshalled data
		 */
		unsigned int marshalledDataMemSize;

		/**
		 * Function to set new result data
		 *
		 * @param resData the new name
		 * @param size size of the result data
		 *
		 * @return 0 in case of success, else error code
		 */
		int setNewResultData(const char* resData, unsigned int size) {
			realResultDataSize = size;
			if (size == 0) {
				return 0;
			}

			// check only if size is not 0
			if (resData == 0) {
				return ERROR_NULL_POINTER;
			}

			if (resultDataMemSize < size) {
				if (resultData != 0) {
					delete[] resultData;
				}
				resultData = new char[size];
				resultDataMemSize = size;
			}

			memcpy(resultData, resData, size);
			return 0;
		};

	public:
		/**
		 * Constructor for FedApiAcknowledge value
		 */
		FedApiAcknowledge() {
			resultData = 0;
			resultDataMemSize = 0;
			marshalledData = 0;
			marshalledDataMemSize = 0;
			resultCode = 0;
			realResultDataSize = 0;
		};

		/**
		 * Constructor for FedApiAcknowledge val
		 *
		 * @param resCode the current result code
		 * @param resData optional result data
		 * @param size size of the result data (0 if no result data)
		 */
		FedApiAcknowledge(int resCode, char* resData = 0, unsigned int size = 0) {
			resultCode = resCode;
			marshalledData = 0;
			marshalledDataMemSize = 0;
			realResultDataSize = size;

			if (size != 0) {
				resultDataMemSize = size;
				resultData = new char[resultDataMemSize];
				memcpy(resultData, resData, realResultDataSize);
			} else {
				resultData = 0;
				resultDataMemSize = 0;
			}
		};

		/**
		 * Copy Constructor for FedApiAcknowledge Val
		 *
		 * @param ackRef FedApiAcknowledge Val object to make a copy of
		 */
		FedApiAcknowledge(const FedApiAcknowledge& ackRef) {
			resultCode = ackRef.resultCode;
			resultDataMemSize = ackRef.resultDataMemSize;
			marshalledDataMemSize = ackRef.marshalledDataMemSize;
			realResultDataSize = ackRef.realResultDataSize;

			if (resultDataMemSize > 0) {
				resultData = new char[resultDataMemSize];
				memcpy(resultData, ackRef.resultData, resultDataMemSize);
			} else {
				resultData = 0;
			}

			if (marshalledDataMemSize > 0) {
				marshalledData = new unsigned char[marshalledDataMemSize];
				memcpy(marshalledData, ackRef.marshalledData,
						marshalledDataMemSize);
			} else {
				marshalledData = 0;
			}

		};

		/**
		 * Assigment operator for FedApiAcknowledge Val
		 *
		 * @param rhs the FedApiAcknowledge val object that shall be copied
		 *
		 * @return reference to the copy of the FedApiAcknowledge object
		 */
		FedApiAcknowledge& operator=(const FedApiAcknowledge& rhs) {
			if (this == &rhs) {
				return *this;
			}

			if (resultData != 0) {
				delete[] resultData;
			}
			if (marshalledData != 0) {
				delete[]  marshalledData;
			}

			resultCode = rhs.resultCode;
			resultDataMemSize = rhs.resultDataMemSize;
			marshalledDataMemSize = rhs.marshalledDataMemSize;
			realResultDataSize = rhs.realResultDataSize;

			if (resultDataMemSize > 0) {
				resultData = new char[resultDataMemSize];
				memcpy(resultData, rhs.resultData, resultDataMemSize);
			} else {
				resultData = 0;
			}

			if (marshalledDataMemSize > 0) {
				marshalledData = new unsigned char[marshalledDataMemSize];
				memcpy(marshalledData, rhs.marshalledData,
						marshalledDataMemSize);
			} else {
				marshalledData = 0;
			}

			return *this;
		};

		/**
		 * Destructor for FedApiAcknowledge Val
		 */
		~FedApiAcknowledge() {
			if (resultData != 0) {
				delete[] resultData;
			}
			if (marshalledData != 0) {
				delete[] marshalledData;
			}
		};

		/**
		 * Function to retrieve the current size of FedApiAcknowledge Value
		 * This also includes the length of the actual used result data
		 *
		 * @return full size of the FedApiAcknowledge value (int +
		 *		length of the actual used result data)
		 */
		unsigned int getFullSize() {
			return (sizeof(int) + realResultDataSize);
		};

		/**
		 * Retrieves the marshalled data for the FedApiAcknowledge channel
		 *
		 * @return the marshalled data of the FedApiAcknowledge Channel
		 */
		unsigned char* marshallData() {
			unsigned int reqSize = 0;

			reqSize = getFullSize();

			if (marshalledDataMemSize < reqSize) {
				if (marshalledData != 0) {
					delete[] marshalledData;
				}
				marshalledData = new unsigned char[reqSize];
				marshalledDataMemSize = reqSize;
			}

			memcpy(marshalledData, &resultCode, sizeof(resultCode));
			memcpy(marshalledData + sizeof(resultCode), resultData,
					realResultDataSize);

			return marshalledData;
		};

		/**
		 * Function to set a new ACK Value
		 *
		 * @param resCode the new result code value
		 * @param resData the new result data of the value,
		 *			which FedApiAcknowledge Val now represents
		 * @param resSize the size of resData (can be 0)
		 *
		 * @return 0 in case of success, else error code
		 */
		int setVal(int resCode, const char* resData, unsigned int resSize) {
			resultCode = resCode;
			return setNewResultData(resData, resSize);
		};

		/**
		 * Getter for the current result code of FedApiAcknowledge Val
		 *
		 * @return int value of Grouped Val
		 */
		int getResultCode() {
			return resultCode;
		};

}; // end of FedApiAcknowledge


/**
 * class containing the channel data for grouped values
 */
class GroupedVal {
	private:
		/**
		 * int val of Grouped Service channel
		 */
		int iVal;

		/**
		 * float val of Grouped Service channel
		 */
		float fVal;

		/**
		 * Pointer to the name of the currently offered value
		 *
		 */
		char valName[MAX_SERVICE_NAME_LENGTH];

		/**
		 * char val of Grouped Service channel
		 */
		char cVal[MAX_CHAR_SERVICE_LENGTH];

		/**
		 * Size of the memory allocated for valName (Note: can be more than
		 * strlen of valName!)
		 */
		unsigned int nameMemSize;

		/**
		 * Container storing the marshalled channel data
		 */
		unsigned char* marshalledData;

		/**
		 * Size of the memory allocated for marshalled data
		 */
		unsigned int marshalledDataMemSize;

		/**
		 * Function to set a new name
		 *
		 * @param name the new name
		 *
		 * @return 0 in case of success, else error code
		 */
		int setNewName(const char* name) {
//			unsigned int reqSize = 0;
			if (name == 0) {
				return ERROR_NULL_POINTER;
			}

			if (strlen(name) > MAX_SERVICE_NAME_LENGTH) {
				return ERROR_ARRAY_TOO_LONG;
			}

			strcpy(valName, name);

			return 0;
		};

	public:
		/**
		 * Constructor for Grouped Val
		 */
		GroupedVal() {
			strcpy(valName, FED_API_SERVICE_NAME_INIT_VALUE);
			nameMemSize = MAX_SERVICE_NAME_LENGTH;
			marshalledData = 0;
			marshalledDataMemSize = 0;
			iVal = FED_API_SERVICE_VALUE_INT_INIT_VALUE;
			fVal = FED_API_SERVICE_VALUE_FLOAT_INIT_VALUE;
			strcpy(cVal, FED_API_SERVICE_VALUE_CHAR_INIT_VALUE);
		};

		/**
		 * Constructor for Grouped val
		 *
		 * @param aInt int value to set
		 * @param aFloat float value to set
		 * @param aCharVal char array value to set
		 * @param aName name of the value to set
		 */
		GroupedVal(int aInt, float aFloat, char* aCharVal, char* aName = 0) {
			iVal = aInt;
			fVal = aFloat;
			marshalledData = 0;
			marshalledDataMemSize = 0;
			nameMemSize = MAX_SERVICE_NAME_LENGTH;

			if ((aName != 0) && (strlen(aName) <= MAX_SERVICE_NAME_LENGTH)) {
				strcpy(valName, aName);
			} else {
				strcpy(valName, FED_API_SERVICE_NAME_INIT_VALUE);
			}

			if ((aCharVal != 0) && (strlen(aCharVal) <= MAX_CHAR_SERVICE_LENGTH)) {
				strcpy(cVal, aCharVal);
			} else {
				strcpy(cVal, FED_API_SERVICE_VALUE_CHAR_INIT_VALUE);
			}
		};

		/**
		 * Copy Constructor for Grouped Val
		 *
		 * @param gvRef Grouped Val object to make a copy of
		 */
		GroupedVal(const GroupedVal& gvRef) {
			iVal = gvRef.iVal;
			fVal = gvRef.fVal;
			nameMemSize = gvRef.nameMemSize;
			marshalledDataMemSize = gvRef.marshalledDataMemSize;

			strcpy(cVal, gvRef.cVal);
			strcpy(valName, gvRef.valName);
			nameMemSize = gvRef.nameMemSize;

			if (gvRef.marshalledDataMemSize > 0) {
				marshalledData = new unsigned char[gvRef.marshalledDataMemSize];
				memcpy(marshalledData, gvRef.marshalledData,
						gvRef.marshalledDataMemSize);
			} else {
				marshalledData = 0;
			}

		};

		/**
		 * Assigment operator for Grouped Val
		 *
		 * @param rhs the Grouped val object that shall be copied
		 *
		 * @return reference to the copy of the GroupedVal object
		 */
		GroupedVal& operator=(const GroupedVal& rhs) {
			if (this == &rhs) {
				return *this;
			}

			if (marshalledData != 0) {
				delete[] marshalledData;
			}

			iVal = rhs.iVal;
			fVal = rhs.fVal;
			nameMemSize = rhs.nameMemSize;
			marshalledDataMemSize = rhs.marshalledDataMemSize;

			strcpy(cVal, rhs.cVal);
			strcpy(valName, rhs.valName);

			if (rhs.marshalledDataMemSize > 0) {
				marshalledData = new unsigned char[rhs.marshalledDataMemSize];
				memcpy(marshalledData, rhs.marshalledData,
						rhs.marshalledDataMemSize);
			} else {
				marshalledData = 0;
			}

			return *this;
		};

		/**
		 * Destructor for Grouped Val
		 */
		~GroupedVal() {
			if (marshalledData != 0) {
				delete[] marshalledData;
			}
		};

		/**
		 * Function to retrieve the current size of Grouped Value
		 * This also includes the length of the value name
		 *
		 * @return full size of the grouped value (int and float member +
		 *		name length (without terminating NULL))
		 */
		unsigned int getFullSize() {
			return (sizeof(int) + sizeof(float) + sizeof(valName) + sizeof(cVal));
		};

		/**
		 * Retrieves the marshalled data for the grouped channel
		 *
		 * @return the marshalled data of the Grouped Channel
		 */
		unsigned char* marshallData() {
			unsigned int reqSize = 0;

			reqSize = sizeof(int) + sizeof(float) + sizeof(valName) + sizeof(cVal) + 1;

			if (marshalledDataMemSize < reqSize) {
				if (marshalledData != 0) {
					delete[] marshalledData;
				}
				marshalledData = new unsigned char[reqSize];
				marshalledDataMemSize = reqSize;

			}

			memcpy(marshalledData, &iVal, sizeof(iVal));
			memcpy(marshalledData + sizeof(iVal), &fVal, sizeof(fVal));
			memcpy(marshalledData + sizeof(iVal) + sizeof(fVal), valName,
					sizeof(valName));
			memcpy(marshalledData + sizeof(iVal) + sizeof(fVal) + sizeof(valName), cVal,
					sizeof(cVal));
			marshalledData[reqSize] = 0;

			return marshalledData;
		};

		/**
		 * Function to set a new int Value
		 *
		 * @param intVal the new int value
		 * @param name the new name of the value, which Grouped Val now
		 *		represents
		 *
		 * @return 0 in case of success, else error code
		 */
		int setIntVal(int intVal, const char* name) {
			iVal = intVal;
			fVal = FED_API_SERVICE_VALUE_FLOAT_DONT_CARE;
			strcpy(cVal, FED_API_SERVICE_VALUE_CHAR_DONT_CARE);
			return setNewName(name);
		};

		/**
		 * Function to set a new float Value
		 *
		 * @param floatVal the new float value
		 * @param name the new name of the value, which Grouped Val now
		 *		represents
		 *
		 * @return 0 in case of success, else error code
		 */
		int setFloatVal(float floatVal, const char* name) {
			iVal = FED_API_SERVICE_VALUE_INT_DONT_CARE;
			fVal = floatVal;
			strcpy(cVal, FED_API_SERVICE_VALUE_CHAR_DONT_CARE);
			return setNewName(name);
		};

		/**
		 * Function to set a new char Value
		 *
		 * @param charVal the new char value
		 * @param name the new name of the value, which Grouped Val now
		 *		represents
		 *
		 * @return 0 in case of success, else error code
		 */
		int setFloatVal(char* charVal, const char* name) {
			if (strlen(charVal) > MAX_CHAR_SERVICE_LENGTH) {
				return ERROR_ARRAY_TOO_LONG;
			}

			iVal = FED_API_SERVICE_VALUE_INT_DONT_CARE;
			fVal = FED_API_SERVICE_VALUE_FLOAT_DONT_CARE;
			strcpy(cVal, charVal);
			return setNewName(name);
		};

		/**
		 * Function to set a new Values for Grouped val.
		 * This function set both, int and float (be sure you want to have
		 * both valid - for now not foreseen (except for init) !)
		 *
		 * @param intVal the new int value
		 * @param floatVal the new float value
		 * @param charVal the new CharVal
		 * @param name the new name of the value, which Grouped Val now
		 *		represents
		 *
		 * @return 0 in case of success, else error code
		 */
		int setVal(int intVal, float floatVal, char* charVal, const char* name) {
			if (strlen(charVal) > MAX_CHAR_SERVICE_LENGTH) {
				return ERROR_ARRAY_TOO_LONG;
			}

			iVal = intVal;
			fVal = floatVal;
			strcpy(cVal, charVal);
			return setNewName(name);
		};

		/**
		 * Getter for the current int value of Grouped Val
		 *
		 * @return int value of Grouped Val
		 */
		int getIntVal() {
			return iVal;
		};

		/**
		 * Getter for the current float value of Grouped Val
		 *
		 * @return float value of Grouped Val
		 */
		float getFloatVal() {
			return fVal;
		};

		/**
		 * Getter for the current char value of Grouped Val
		 *
		 * @return char value of Grouped Val
		 */
		const char* getCharVal() {
			return cVal;
		};

		/**
		 * Getter for the current name of Grouped Val
		 *
		 * @return name of Grouped Val
		 */
		const char* getName() {
			return valName;
		};
}; // end of GroupedVal




//////////////////////////////////// VALUE ////////////////////////////////////
/**
 * This class is the parent for FedApi Values
 *
 * @author Sebastian Bablok
 *
 * @date 2006-09-12
 */
class FedApiValue {
    public:

		/**
		 * Constructor for a FedApiValue
		 */
		FedApiValue() {};

		/**
		 * Destructor for a FedApiValue
		 */
		virtual ~FedApiValue() {};

		/**
		 * Function to request the size of the service value
		 *
		 * @return the size of the Service Value
		 */
		virtual unsigned int getSize() = 0;

		/**
		 * Function to get the data of this value
		 *
		 * @return pointer to the data for this value
		 */
		virtual void* getValueData() = 0;

		/**
		 * Function to set the data of this value
		 *
		 * @param value pointer to new value data for this FedApiValue
		 */
		virtual void setValueData(FedApiValue* value) = 0;



    protected:
		/**
		 * Size of the Channel value(s)
		 */
		unsigned int mSize;


    private:

}; // end of FedApiValue


////////////////////////////////// INT VALUE //////////////////////////////////
/**
 * This class represents wrapper for FedApiInt Values
 *
 * @author Sebastian Bablok
 *
 * @date 2006-09-12
 */
class FedApiValueInt : public FedApiValue {
    public:

		/**
		 * Constructor for a FedApiValueInt
		 */
		FedApiValueInt();

		/**
		 * Constructor for a FedApiValueInt
		 *
		 * @param aInt integer value to set
		 */
		FedApiValueInt(int aInt);

		/**
		 * Destructor for a FedApiValueInt
		 */
		virtual ~FedApiValueInt();

		/**
		 * Function to request the size of the service value
		 *
		 * @return the size of the Service Value
		 */
		virtual unsigned int getSize();

		/**
		 * Function to get the data of this value
		 *
		 * @return pointer to the data for this value
		 */
		virtual void* getValueData();

		/**
		 * Function to set the data of this value
		 *
		 * @param value new data for this FedApiValue
		 */
		virtual void setValueData(FedApiValue* value);

		/**
		 * Getter for int value of member mChannelData
		 */
		int getChannelIntVal();


    protected:
		/**
		 * struct of the actual channel data
		 */
		SingleVal mChannelData;


    private:

}; // end of FedApiValueInt


///////////////////////////////// FLOAT VALUE /////////////////////////////////
/**
 * This class represents wrapper for FedApiFloat Values
 *
 * @author Sebastian Bablok
 *
 * @date 2006-09-12
 */
class FedApiValueFloat : public FedApiValue {
    public:

		/**
		 * Constructor for a FedApiValueFloat
		 */
		FedApiValueFloat();

		/**
		 * Constructor for a FedApiValueFloat
		 *
		 * @param aFloat float value to set
		 */
		FedApiValueFloat(float aFloat);

		/**
		 * Destructor for a FedApiValueFloat
		 */
		virtual ~FedApiValueFloat();

		/**
		 * Function to request the size of the service value
		 *
		 * @return the size of the Service Value
		 */
		virtual unsigned int getSize();

		/**
		 * Function to get the data of this value
		 *
		 * @return pointer to the data for this value
		 */
		virtual void* getValueData();

		/**
		 * Function to set the data of this value
		 *
		 * @param value new data for this FedApiValue
		 */
		virtual void setValueData(FedApiValue* value);

		/**
		 * Getter for float value of member mChannelData
		 */
		float getChannelFloatVal();


    protected:
		/**
		 * struct of the actual channel data
		 */
		SingleVal mChannelData;


    private:

}; // end of FedApiValueFloat

////////////////////////////////// CHAR VALUE //////////////////////////////////
/**
 * This class represents wrapper for FedApiChar Values
 *
 * @author Sebastian Bablok
 *
 * @date 2006-09-12
 */
class FedApiValueChar : public FedApiValue {
    public:

		/**
		 * Constructor for a FedApiValueChar
		 */
		FedApiValueChar();

		/**
		 * Constructor for a FedApiValueChar
		 *
		 * @param aChar integer value to set
		 */
		FedApiValueChar(char* aChar);

		/**
		 * Destructor for a FedApiValueChar
		 */
		virtual ~FedApiValueChar();

		/**
		 * Function to request the size of the service value
		 *
		 * @return the size of the Service Value
		 */
		virtual unsigned int getSize();

		/**
		 * Function to get the data of this value
		 *
		 * @return pointer to the data for this value
		 */
		virtual void* getValueData();

		/**
		 * Function to set the data of this value
		 *
		 * @param value new data for this FedApiValue
		 */
		virtual void setValueData(FedApiValue* value);

		/**
		 * Getter for char value of member mChannelData
		 */
		char* getChannelCharVal();


    protected:
		/**
		 * struct of the actual channel data
		 */
		SingleVal mChannelData;


    private:

}; // end of FedApiValueInt


///////////////////////////////// GROUPED VALUE /////////////////////////////////
/**
 * This class represents wrapper for FedApiGrouped Values
 *
 * @author Sebastian Bablok
 *
 * @date 2006-09-12
 */
class FedApiValueGrouped : public FedApiValue {
    public:

		/**
		 * Constructor for a FedApiValueGrouped
		 */
		FedApiValueGrouped();

		/**
		 * Constructor for a FedApiValueGrouped
		 *
		 * @param aInt integer value to set
		 * @param name the name of the value
		 */
		FedApiValueGrouped(int aInt, std::string name);

		/**
		 * Constructor for a FedApiValueGrouped
		 *
		 * @param aFloat float value to set
		 * @param name the name of the value
		 */
		FedApiValueGrouped(float aFloat, std::string name);

		/**
		 * Constructor for a FedApiValueGrouped
		 *
		 * @param aCharVal char array value to set
		 * @param name the name of the value
		 */
		FedApiValueGrouped(char* aCharVal, std::string name);

		/**
		 * Constructor for a FedApiValueGrouped
		 *
		 * @param aInt integer value to set
		 * @param aFloat float value to set
		 * @param aCharVal char array value to set
		 * @param name the name of the value
		 */
		FedApiValueGrouped(int aInt, float aFloat, char* aCharVal, std::string name);

		/**
		 * Destructor for a FedApiValueGrouped
		 */
		virtual ~FedApiValueGrouped();

		/**
		 * Function to request the size of the service value
		 *
		 * @return the size of the Service Value
		 */
		virtual unsigned int getSize();

		/**
		 * Function to get the data of this value
		 *
		 * @return pointer to the data for this value
		 */
		virtual void* getValueData();

		/**
		 * Function to set the data of this value
		 *
		 * @param value new data for this FedApiValue
		 */
		virtual void setValueData(FedApiValue* value);

		/**
		 * Getter for the Grouped Val object, containing the values for
		 * the Grouped Channel.
		 *
		 * @return copy of the Grouped Val object representing the data of
		 *			the Grouped Channel.
		 */
		virtual GroupedVal getChannelDataObj();

		/**
		 * Getter for current values of member GroupedVal (represents the data
		 * of the Grouped Channel)
		 *
		 * @param intVal [out parameter] pointer to int, where int value of
		 *			GroupedVal will be copied to
		 * @param floatVal [out parameter] pointer to float, where float value
		 *			of GroupedVal will be copied to
		 * @param charVal [out parameter] pointer to char array, where char value
		 *			of GroupedVal will be copied to
		 *
		 * @return the name of the current active value
		 */
		virtual std::string getCurrentChannelValues(int* intVal, float* floatVal, char** charVal);


    protected:
		/**
		 * struct of the actual channel data
		 */
		GroupedVal mChannelData;


    private:

}; // end of FedApiValueFloat


////////////////////////////////// MSG VALUE //////////////////////////////////
/**
 * This class represents wrapper for FedApiMSG Values
 *
 * @author Sebastian Bablok
 *
 * @date 2006-09-12
 */
class FedApiValueMSG : public FedApiValue {
    public:

		/**
		 * Constructor for a FedApiValueMSG
		 */
		FedApiValueMSG();

		/**
		 * Constructor for a FedApiValueMSG to set a new value
		 *
		 * @param msgType the message type of the msg (MSG_INFO, MSG_WARNING,
		 *			MSG_ERROR, MSG_ALARM, MSG_DEBUG, MSG_FAILURE_AUDIT,
		 *			MSG_SUCCESS_AUDIT)
		 * @param origin the origin of the message
		 * @param description the actual message
		 * @param date (optional) the date when event of message has occured,
		 *			the format should be: ("YYYY-MM-DD hh:mm:ss\0").
		 *			If no date is given the current time will be included
		 */
		FedApiValueMSG(unsigned int msgType, std::string origin,
				std::string description, std::string date = "NULL");

		/**
		 * Destructor for a FedApiValueMSG
		 */
		virtual ~FedApiValueMSG();

		/**
		 * Function to request the size of the service value
		 *
		 * @return the size of the Service Value
		 */
		virtual unsigned int getSize();

		/**
		 * Function to get the data of this value
		 *
		 * @return pointer to the data for this value
		 */
		virtual void* getValueData();

		/**
		 * Function to set the data of this value
		 *
		 * @param value new data for this FedApiValue
		 */
		virtual void setValueData(FedApiValue* value);

		/**
		 * Getter for message struct representing the Channel Data
		 */
		FedApiMessage getChannelVal();


    protected:
		/**
		 * struct of the actual channel data
		 */
		FedApiMessage mChannelData;


    private:

}; // end of FedApiValueMSG


////////////////////////////////// ACK VALUE //////////////////////////////////
/**
 * This class represents wrapper for FedApiMSG Values
 *
 * @author Sebastian Bablok
 *
 * @date 2006-09-12
 */
class FedApiValueACK : public FedApiValue {
    public:

		/**
		 * Constructor for a FedApiValueMSG
		 */
		FedApiValueACK();

		/**
		 * Constructor for a FedApiValueMSG to set a new value
		 *
		 * @param resCode result or error code for the before issued command
		 * @param resData result data, if any (if this is NULL, the size must
		 *			be also 0)
		 * @param size the size of the result data
		 */
		FedApiValueACK(int resCode, char* resData = 0,
				unsigned int size = 0);

		/**
		 * Destructor for a FedApiValueMSG
		 */
		virtual ~FedApiValueACK();

		/**
		 * Function to request the size of the service value
		 *
		 * @return the size of the Service Value
		 */
		virtual unsigned int getSize();

		/**
		 * Function to get the data of this value
		 *
		 * @return pointer to the data for this value
		 */
		virtual void* getValueData();

		/**
		 * Function to set the data of this value
		 *
		 * @param value new data for this FedApiValue
		 */
		virtual void setValueData(FedApiValue* value);

		/**
		 * Getter for message struct representing the Channel Data
		 */
		FedApiAcknowledge getChannelDataObj();


    protected:
		/**
		 * struct of the actual channel data
		 */
		FedApiAcknowledge mChannelData;


    private:

}; // end of FedApiValueACK


#endif //FED_API_VALUE_HPP
