/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/
#include "FedApiServiceChannel.hpp"


using namespace std;


///////////////////////////////// SINGLE INT //////////////////////////////////
FedApiSingleServiceChannelInt::FedApiSingleServiceChannelInt(string name,
		FedApiValue* initValue) :
			FedApiSingleServiceChannel(name, FED_API_SINGLE_SERVICE_CHANNEL_STRUCTURE)
		{

	mValue.setValueData(initValue);
	mpDimService = new DimService(name.c_str(),
			FED_API_SINGLE_SERVICE_CHANNEL_STRUCTURE,
			mValue.getValueData(), mValue.getSize());
}


FedApiSingleServiceChannelInt::~FedApiSingleServiceChannelInt() {
	if (mpDimService != 0) {
		delete mpDimService;
	}
}


int FedApiSingleServiceChannelInt::updateVal(FedApiValue* value) {
	mValue.setValueData(value);
	mpDimService->updateService(); // ??? check here if memory loc is still valid after update !!!
	return 0;
}


const FedApiValue* FedApiSingleServiceChannelInt::getChannelValue() {
	return &mValue;
}

//////////////////////////////// SINGLE FLOAT /////////////////////////////////
FedApiSingleServiceChannelFloat::FedApiSingleServiceChannelFloat(string name,
		FedApiValue* initValue) :
			FedApiSingleServiceChannel(name, FED_API_SINGLE_SERVICE_CHANNEL_STRUCTURE)
		{

	mValue.setValueData(initValue);
	mpDimService = new DimService(name.c_str(),
			FED_API_SINGLE_SERVICE_CHANNEL_STRUCTURE,
			mValue.getValueData(), mValue.getSize());

}


FedApiSingleServiceChannelFloat::~FedApiSingleServiceChannelFloat() {
	if (mpDimService != 0) {
		delete mpDimService;
	}
}

int FedApiSingleServiceChannelFloat::updateVal(FedApiValue* value) {
	mValue.setValueData(value);
	mpDimService->updateService(); // ??? check here if memory loc is still valid after update !!!
	return 0;
}


const FedApiValue* FedApiSingleServiceChannelFloat::getChannelValue() {
	return &mValue;
}


//////////////////////////////// SINGLE CHAR /////////////////////////////////
FedApiSingleServiceChannelChar::FedApiSingleServiceChannelChar(string name,
		FedApiValue* initValue) :
			FedApiSingleServiceChannel(name, FED_API_SINGLE_SERVICE_CHANNEL_STRUCTURE)
		{

	mValue.setValueData(initValue);
	mpDimService = new DimService(name.c_str(),
			FED_API_SINGLE_SERVICE_CHANNEL_STRUCTURE,
			mValue.getValueData(), mValue.getSize());

}


FedApiSingleServiceChannelChar::~FedApiSingleServiceChannelChar() {
	if (mpDimService != 0) {
		delete mpDimService;
	}
}

int FedApiSingleServiceChannelChar::updateVal(FedApiValue* value) {
	mValue.setValueData(value);
	mpDimService->updateService(); // ??? check here if memory loc is still valid after update !!!
	return 0;
}


const FedApiValue* FedApiSingleServiceChannelChar::getChannelValue() {
	return &mValue;
}


/////////////////////////////////// GROUPED ///////////////////////////////////
FedApiGroupedServiceChannel::FedApiGroupedServiceChannel(string name,
		FedApiValue* initValue) :
			FedApiServiceChannel(name, FED_API_GROUPED_SERVICE_CHANNEL_STRUCTURE)
		{

	mValue.setValueData(initValue);
	mpDimService = new DimService(name.c_str(),
			FED_API_GROUPED_SERVICE_CHANNEL_STRUCTURE,
			mValue.getValueData(), mValue.getSize());

}


FedApiGroupedServiceChannel::~FedApiGroupedServiceChannel() {
	if (mpDimService != 0) {
		delete mpDimService;
	}
}


int FedApiGroupedServiceChannel::updateVal(FedApiValue* value) {
	mValue.setValueData(value);
	mpDimService->updateService(mValue.getValueData(), mValue.getSize());
	return 0;
}


const FedApiValue* FedApiGroupedServiceChannel::getChannelValue() {
	return &mValue;
}



///////////////////////////////////// MSG /////////////////////////////////////
FedApiMessageChannel::FedApiMessageChannel(string name,
		FedApiValue* initValue) :
			FedApiServiceChannel(name, FED_API_MESSAGE_CHANNEL_STRUCTURE)
		{

	mValue.setValueData(initValue);
	mpDimService = new DimService(name.c_str(),
			FED_API_MESSAGE_CHANNEL_STRUCTURE,
			mValue.getValueData(), mValue.getSize());

}


FedApiMessageChannel::~FedApiMessageChannel() {
	if (mpDimService != 0) {
		delete mpDimService;
	}
}

int FedApiMessageChannel::updateVal(FedApiValue* value) {
	mValue.setValueData(value);
	mpDimService->updateService(); // ??? check here if memory loc is still valid after update !!!
	return 0;
}


const FedApiValue* FedApiMessageChannel::getChannelValue() {
	return &mValue;
}


///////////////////////////////////// ACK /////////////////////////////////////
FedApiAcknowledgeChannel::FedApiAcknowledgeChannel(string name,
		FedApiValue* initValue) :
			FedApiServiceChannel(name, FED_API_ACKNOWLEDGE_CHANNEL_STRUCTURE)
		{

	mValue.setValueData(initValue);
	mpDimService = new DimService(name.c_str(),
			FED_API_ACKNOWLEDGE_CHANNEL_STRUCTURE,
			mValue.getValueData(), mValue.getSize());

}


FedApiAcknowledgeChannel::~FedApiAcknowledgeChannel() {
	if (mpDimService != 0) {
		delete mpDimService;
	}
}

int FedApiAcknowledgeChannel::updateVal(FedApiValue* value) {
	mValue.setValueData(value);
	mpDimService->updateService(mValue.getValueData(), mValue.getSize()); // ??? check here if memory loc is still valid after update !!!
	return 0;
}


const FedApiValue* FedApiAcknowledgeChannel::getChannelValue() {
	return &mValue;
}


