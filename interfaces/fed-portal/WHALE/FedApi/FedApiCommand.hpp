/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/
#ifndef FED_API_COMMAND_HPP
#define FED_API_COMMAND_HPP



/**
 * Define for "Don't care" of his 'int' value
 */
#define FED_API_COMMAND_VALUE_INT_DONT_CARE -9999999

/**
 * Define for "Don't care" of this 'float' value
 */
#define FED_API_COMMAND_VALUE_FLOAT_DONT_CARE -9999999.0

/**
 * Define for "init value" of 'int' value for Service channel
 */
#define FED_API_COMMAND_VALUE_INT_INIT_VALUE 0

/**
 * Define for "init value" of 'float' value for Service channel
 */
#define FED_API_COMMAND_VALUE_FLOAT_INIT_VALUE 0.0

/**
 * Define for error code: invalid parameter given by command
 */
#define FED_API_INVALID_PARAMETER -11

/**
 * Define for error code: invalid parameter given by command
 */
#define FED_API_INVALID_COMMAND -20

/**
 * Define for the structure defining a ConfigureFeeCom command channel
 */
#define FED_API_CONFIGURE_FEECOM_CMD_STRUCTURE "I:1;I:1;F:1;C"

/**
 * Define for the command SET LOG LEVEL in FedServer
 */
#define FED_API_SET_LOG_LEVEL 0x0003

/**
 * Define for the command GET LOG LEVEL in FedServer
 */
#define FED_API_GET_LOG_LEVEL 0x0004

/**
 * struct containing the channel data for single value
 */
typedef struct ConfigureFeeComStruct {
	/**
	 * integer defining the type of command (see FedApi for details)
	 */
	int commandType;

	/**
	 * int val of ConfigureFeeCom command channel
	 * (optional: has not to be valid)
	 */
	int iVal;

	/**
	 * float val of ConfigureFeeCom command channel
	 * (optional: has not to be valid)
	 */
	float fVal;

	/**
	 * Target name: the target for which the issued command should be destinated
	 */
	char target[];

} ConfigureFeeCom;

#endif // FED_API_COMMAND_HPP
