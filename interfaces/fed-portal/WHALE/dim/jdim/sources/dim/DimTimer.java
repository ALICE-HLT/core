package dim;

public class DimTimer implements DimTimerHandler
{

	long id;
	public DimTimer(int secs)
	{
		start(secs);
	}
	public DimTimer()
	{
	}

	public static native long start(DimTimer theTimer, int secs);
	public static native int stop(long id);
	
	public void start(int secs)
	{
		id = start(this, secs);
	}
	
	public int stop()
	{
		return stop(id);
	}
	
	public static void sleep(int secs)
	{
  		try
        {
			Thread/*.currentThread()*/.sleep(secs*1000);
        }
        catch (Exception e) {}
	}
	public void timerHandler() {};
}

interface DimTimerHandler
{
	void timerHandler();
}