#ifdef __CINT__
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */

/* $Id: LinkDef.h,v 1.5 2005/08/18 17:32:16 byordano Exp $ */

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ global gAlice;
#pragma link C++ global gMC;

#pragma link C++ class	TaxiListReader;


#endif
