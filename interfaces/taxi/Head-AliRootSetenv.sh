# ROOT
export ROOTSYS=/afsuser/sebastian/ROOT/root   #/opt/taxi/root
export PATH=$PATH:$ROOTSYS/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOTSYS/lib

# AliRoot
export ALICE=/afsuser/sebastian/ROOT/alice   #/opt/taxi/alice
export ALICE_ROOT=$ALICE/AliRoot
export ALICE_TARGET=`root-config --arch`

export PATH=$PATH:$ALICE_ROOT/bin/tgt_${ALICE_TARGET}
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ALICE_ROOT/lib/tgt_${ALICE_TARGET}


# GEANT 3
export PLATFORM=`root-config --arch`
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ALICE/geant3/lib/tgt_${ALICE_TARGET}

# AliEn
export ALIEN_ROOT=/afsuser/sebastian/ROOT/alien   #/opt/alien
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ALIEN_ROOT/lib:$ALIEN_ROOT/api/lib #:$ALIEN_ROOT/lib64
export PATH=$PATH:$ALIEN_ROOT/api/bin
export ALIEN_INSTALLER_PLATFORM=x86_64-unknown-linux-gnu
export GSHELL_GCC=/usr/bin/gcc

# AliEn environment at CERN vars
# Globus Location
export GLOBUS_LOCATION=$ALIEN_ROOT/globus
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$GLOBUS_LOCATION/lib
#export PATH=$PATH:$GLOBUS_LOCATION/bin #not allowed for recomplie of api src
export alien_API_USER=bablok

# This one might be changed in the next release of the AliEn Client
#unset alien_API_HOST
#unset GCLIENT_SERVER_LIST
#export alien_API_HOST='pcapiserv04.cern.ch'
#export GCLIENT_SERVER_LIST="pcapiserv04.cern.ch:10000|pcapiserv05.cern.ch:10000|pcapiserv06.cern.ch:10000"


