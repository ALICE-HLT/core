/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>

#include "TaxiListReader.h"

ClassImp(TaxiListReader)

using namespace std;
//using namespace alice::hlt::taxi;

//typedef pair <string, string> String_Pair;


const std::string TaxiListReader::ListFileName = "TaxiInclFiles";


TaxiListReader::TaxiListReader() {
	mValid = false;
}


TaxiListReader::TaxiListReader(const char* filename) {
	mValid = false;
	if (readListFromFile(filename)) {
		mValid = true;
	} else {
		// log that filename does ot matches a file
	}
}


TaxiListReader::~TaxiListReader() {
	if (mValid) {
		mCalibObjList.clear();
	}
}


int TaxiListReader::readListFromFile(const std::string filename) {
	return readListFromFile(filename.c_str());
}


int TaxiListReader::readListFromFile(const char* filename) {
	ifstream pfile;
	int count = 0;

	// open property file
	if (filename == "") {
		cout << "   *** Filename for calibration object list is empty" << endl;
//		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY,
//				"PropertyReader received empty filename to read properties.");
		return -1;
	}

	pfile.open(filename, ios_base::in);
	if (!pfile) {
		cout << "   *** ERROR while opening list file \"" << filename << 
				"\" or file does not exist." << endl;
//		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY,
//				"PropertyReader is unable to open property file '" +
//				ProxyLogger::catos(filename) + "'.");
		return -1;
	}
//	ProxyLogger::getLogger()->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY,
//			"PropertyReader uses property file '" +	ProxyLogger::catos(filename)
//			+ "'.");

	while ((!pfile.eof()) && (!pfile.bad())) {
		char line[MAX_LINE_LENGTH];
		char* ptrDelimiter;

		pfile.getline(line, MAX_LINE_LENGTH);

		// skip empty lines
		if (strlen(line) == 0) {
			continue;
		}
		// skip comment lines (beginning with '#') and lines with blank chars
		// at beginnig
		if ((line[0] == '#') || (line[0] == ' ')) {
			continue;
		}

		// cut off comments at end
		ptrDelimiter = strstr(line, "#");
		if (ptrDelimiter != 0) {
			*ptrDelimiter = '\0';
		}
		// cut off end of line
		ptrDelimiter = strstr(line, " ");
		if (ptrDelimiter != 0) {
			*ptrDelimiter = '\0';
		}

		// file calibration object name in local vector
		string name(line);
		mCalibObjList.push_back(name);
		++count;

/*
		ptrDelimiter = strstr(line, "=");
		if (ptrDelimiter != 0) {
			*ptrDelimiter = '\0';
			ptrDelimiter++;
			if ((ptrDelimiter[0] != ' ') && (ptrDelimiter[0] != ' ')) {
				string name(line);
				string value(ptrDelimiter);
				mProperties.insert(String_Pair(name, value));
				++count;
			} else {
//				ProxyLogger::getLogger()->createLogMessage(MSG_WARNING,
//						LOG_SOURCE_PROXY,
//						"PropertyReader encountered empty property for '" +
//						ProxyLogger::catos(line) + "'.");
				cout << "Missing property value for " << line << endl;
			}
		}
*/

	}
//	ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY,
//			"PropertyReader has read " + ProxyLogger::itos(count) +
//			" properties from file '" +	ProxyLogger::catos(filename) + "'.");

	// maybe check also for .bad() for returning a false
	pfile.close();
	mValid = true;
	return count;
}

int TaxiListReader::readLists(const char* path, const char* ending) {
	string pathStr = path;
	string endingStr = ending;
	return readLists(pathStr, endingStr);
}

int TaxiListReader::readLists(const string path, const string ending) {
	int counter = 0;
	
// Solved differently in starting script, feature not required in
// TaxiListReader (maybe in later version)
/*		
	int entryCount = 0;
	string executeString;
    fstream pfile;

	// make list of to include list files
	executeString = "find " + path + " -name \"*" + ending + "\" > " + 
			TaxiListReader::ListFileName;

//	exec(executeString.c_str());
	cout << executeString << endl;

    // open list file
    pfile.open(TaxiListReader::ListFileName.c_str(), ios_base::in);
    if (!pfile) {
        cout << "   *** Error while opening list includes file: " << 
				TaxiListReader::ListFileName << endl;
        return -1;
    }

    while ((!pfile.eof()) && (!pfile.bad())) {
        char line[MAX_LINE_LENGTH];
		int nRet = 0;	
	
		pfile.getline(line, MAX_LINE_LENGTH);
		
		// skip empty lines
		if (strlen(line) == 0) {
            continue;
        }

		// read entries from list file
		nRet = readListFromFile(line);
		if (nRet > 0) {
			cout << "   +++ Read successfully list from file \"" << line << "\""
					<< endl;
			++counter;
			entryCount += nRet;
		} else if (nRet == 0) {
			cout << "   *** ERROR File \"" << line << 
					"\" does not contain any calibration list entries." << endl;
		} else {
			cout << "   *** ERROR Unable to read file \"" << line << "\"" << endl;
		}
	}
	
	pfile.close();
	cout << "   --- ListReader has retrieved " << entryCount << 
			" list entries from " << counter << " files." << endl;
*/
	return counter;
}


int TaxiListReader::retrieveLastRunNumber(char* path) {
	string strPath = path;
	return TaxiListReader::retrieveLastRunNumber(strPath);
}


int TaxiListReader::retrieveLastRunNumber(string path) {
	int runNumber = 0;
	ifstream pfile;
	string filename;
	int sleepTime = 5;
	char line[25];

	filename = path + "/lastRunNumber";
	cout << "   --- PATH to lastRunNumber is " << filename << endl;
	
	pfile.open(filename.c_str(), ios_base::in);
	if (!pfile) {
		cout << "   ~~~ Unable to open lastRunNumber file, trying in '" <<
				sleepTime << "' seconds again.." << endl;
		sleep(sleepTime);
		pfile.open(filename.c_str(), ios_base::in);
		if (!pfile) {
			cout << "   *** Error opening lastRunNumber file or file does not exist. Using '0' instead."
					<< endl;
			return runNumber;
		}
	}
	if ((!pfile.eof()) && (!pfile.bad())) {
		pfile.getline(line, 25);
		runNumber = atoi(line);
	}

	return runNumber;
}


