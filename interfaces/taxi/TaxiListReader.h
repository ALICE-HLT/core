#ifndef ALICE_HLT_TAXI_LIST_READER_HPP
#define ALICE_HLT_TAXI_LIST_READER_HPP

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2007
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <vector>
#include <string>

#include <TObject.h>


//namespace alice { namespace hlt { namespace taxi {

#define MAX_LINE_LENGTH 256

/**
 * This class reads the file(s) containing the list(s) of calibration objects 
 * the TAXI shall fetch.
 *
 * @author Sebastian Bablok
 *
 * @date 2007-02-23
 */
class TaxiListReader : public TObject {
	public:

		/**
		 * Constructor for the TaxiListReader.
		 */
		TaxiListReader();

		/**
		 * Constructor for TaxiListReader, which also loads the list from file
		 *
		 * @param filename of the list file to read given as char*
		 */
		TaxiListReader(const char* filename);

		/**
		 * Destructor for the TaxiListReader.
		 */
		virtual ~TaxiListReader();

		/**
		 * Reads the list from the file given by filename
		 * and stores the calibration object names in this class/object.
		 *
		 * @param filename of the list file to read given as char*
		 *
		 * @return number of read list entries or "<0" if unable to read file
		 */
		int readListFromFile(const char* filename);

		/**
		 * Reads the list from the file given by filename
		 * and stores the calibration object names in this class/object.
		 *
		 * @param filename of the list file to read given as string
		 *
		 * @return number of read list entries or "<0" if unable to read file
		 */
		int readListFromFile(const std::string filename);

		/**
		 * Reads all elements in files with the provided ending in the given
		 * folder and stores them to the local (object owned) list.
		 *
		 * @param path the path to the folder containing lists to be read.
		 * @param ending the ending, that the desired list files have 
		 * 			(default = ".list")
		 *
		 * @return number of found list files, error code (<0) in case of error.
		 */
		int readLists(const char* path, const char* ending = ".list");

		/**
		 * Reads all elements in files with the provided ending in the given
		 * folder and stores them to the local (object owned) list.
         *
         * @param path the path to the folder containing lists to be read.
         * @param ending the ending, that the desired list files have 
         *          (default = ".list")
         *
         * @return number of found list files, error code (<0) in case of error.
         */
        int readLists(const std::string path, const std::string ending = ".list");


		/**
		 * Function to get the list of calibration object names.
		 *
		 * @return a vector containing the the list (copy)
		 */
		std::vector<std::string>& getCalibObjList();

		/**
		 * Function to retrieve the latest run number
		 *
		 * @param path path to the directory containing the lastRunNumber file,
		 *			NOTE: the file name must not be included to the path
		 *
		 * @return latest run number
		 */
		static int retrieveLastRunNumber(std::string path);

		/**
         * Function to retrieve the latest run number
         *
         * @param path path to the directory containing the lastRunNumber file,
         *          NOTE: the file name must not be included to the path
         *
         * @return latest run number
         */
		static int retrieveLastRunNumber(char* path);

// check here !!!
		/**
		 * Retrieves and returns the value of a given property name.
		 *
		 * @param propertyName the name of the requested property (as char)
		 * @param propertyValue [out] pointer to the string representing the
		 *			value of the requested property
		 *
		 * @return true, if requested property has been found in the class, else
		 * 			false (false also when PropertyReader is not valid or
		 *			propertyName is NULL)
		 */
//		bool retrievePropertyValue(const char* propertyName,
//					std::string** propertyValue);

		/**
		 * Retrieves and returns the value of a given property name.
		 *
		 * @param propertyName pointer to the string containing the name of
		 *			the requested property
		 * @param propertyValue [out] pointer to the string representing the
		 *			value of the requested property
		 *
		 * @return true, if requested property has been found in the class, else
		 * 			false (false also when PropertyReader is not valid or
		 *			propertyName is NULL)
		 */
//		bool retrievePropertyValue(const std::string* propertyName,
//			std::string** propertyValue);


	private:

        /**
         * Flag, indicating if list has already been read from file.
         */
        bool mValid;

        /**
         * Vector containing the list of calibration objects
         */
		std::vector<std::string> mCalibObjList;

		/**
		 * Define for the filename of the list of to include files.
		 */
		static const std::string ListFileName;
		

		/**
		 * Map containing all PropertyName PropertyValue pairs read from the
		 * property file.
		 */
//		std::map<std::string, std::string> mProperties;

        /**
         * AliRoot required stuff
         */
        ClassDef(TaxiListReader, 2);
}; //end of class


inline std::vector<std::string>& TaxiListReader::getCalibObjList() {
	return mCalibObjList;
}


//} } } // end namespaces "alice", "hlt" and "taxi"


#endif
