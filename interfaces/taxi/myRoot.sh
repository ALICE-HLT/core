#!/bin/bash

fakepythia()
{
    [ -r lib ] || mkdir lib
    rm -rf lib/libPythia6.a
    rm -rf /tmp/p1.o /tmp/p1.c
    rm -rf /tmp/p2.o /tmp/p2.c


    cat > /tmp/p1.c <<EOF
    void nevercalled() {}
EOF
    gcc -fPIC -c -o /tmp/p1.o /tmp/p1.c

    cat > /tmp/p2.c <<EOF
    void pythia6_common_block_address () {;}
    void pythia6_common_address () {;}
    //void pythia6_common_block_address_ () {;}
    //void pythia6_common_block_address__ () {;}
EOF

    gcc -fPIC -c -o /tmp/p2.o /tmp/p2.c

    ar rv lib/libPythia.a  /tmp/p1.o
    ranlib lib/libPythia.a

    ar rv lib/libPythia6.a /tmp/p2.o
    ranlib lib/libPythia6.a

    export PYTHIA6DIR=$ROOTSYS/lib

    return 0
}

postfakepythia()
{
    ar rv lib/libPythia6.a /tmp/p1.o
    ranlib lib/libPythia6.a
    rm /tmp/p1.* /tmp/p2.*

    return 0
}

if [ -z "$ROOTSYS" ] ; then
    echo "Set the ROOTSYS first..."
    exit -1
fi

# removed because no QT support needed ...
#if [ -z "$QTDIR" ] ; then
#    echo "Set the QTDIR first..."
#    exit -1
#fi

cd $ROOTSYS

fakepythia

ARCH="linux"
BIT="32"
if [ `uname -m` = "x86_64" ] ; then
    ARCH="linuxx8664gcc"
    BIT="64"
fi

ALIENENABLE=""
if [ $ALIEN_ROOT ];
then
    if [ -z "$GLOBUS_LOCATION" ] ;
        then
#        export GLOBUS_LOCATION=/home/sebastian/taxi/portal-taxi0/alien/globus
			export GLOBUS_LOCATION=$ALIEN_ROOT/globus
    fi
    ALIENENABLE="--enable-alien --with-alien-incdir=$ALIEN_ROOT/api/include --with-alien-libdir=$ALIEN_ROOT/api/lib"
	echo "AliEn enabled, ALIEN_ROOT: $ALIEN_ROOT !!"
fi

./configure $ARCH \
    --enable-pythia6 --with-pythia6-libdir=$PYTHIA6DIR \
    --enable-cern --enable-opengl --enable-minuit2 \
    --enable-roofit --enable-ssl \
    --enable-mathcore --enable-roofit --enable-qtgsi \
    --enable-table \
    --enable-mathmore --with-gsl-incdir=/usr/include/gsl --with-gsl-libdir=/usr/lib \
    --enable-python \
    $ALIENENABLE \
	--with-ssl-incdir=$ALIEN_ROOT/include --with-ssl-libdir=$ALIEN_ROOT/lib
# add this line to configure if you want QT support in root (and remove comment for check above)
#    --enable-qt --with-qt-libdir=$QTDIR/lib --with-qt-incdir=$QTDIR/include \

postfakepythia

#echo "execute make && make cintdlls && make install"
echo "execute make && make install"

make -j3

