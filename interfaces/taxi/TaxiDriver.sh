#!/bin/bash

#/****************************************************************************/
#**
#**
#** This file is property of and copyright by the Department of Physics
#** Institute for Physic and Technology, University of Bergen,
#** Bergen, Norway, 2007
#** This file has been written by Sebastian Bablok,
#** sebastian.bablok@ift.uib.no
#**
#** Important: This file is provided without any warranty, including
#** fitness for any particular purpose.
#**
#**
#******************************************************************************/

## This script sets the required environment variables and prepares the needed
## AliEn stuff (grid-proxy, alien-token). Then it starts the Taxi. This is 
## repeated in regular time intervals given by a parameter. After each run, a 
## filelist of the current content of the HCDB is placed in the HCDB root 
## directory.

VersionNumber="2.2"

## definition of variables
timeInterval=0
sleepTime=0
AliEnUserName=""
OCDB_path=""
T_HCDB_PATH=""
ListFileName=""
MergedList=""
Merged_filename="ListMerged.txt"
PW_file=""
listHCDBName_dummy="FileList.txt-bak"
listHCDBName="FileList.txt"
localFolder=""
isInit=0
recursiveCounter=0
retVal=0
timestamp=`date "+%Y-%m-%d %H:%M:%S"`
requestType="alien"


## prints out the usage of this script
function usage {
    echo "Usage (Version: $VersionNumber):"
    echo "  $0 <AliEn_user> <OCDB_path> <THCDB_path> <Calibration_list> <PW_file> "
	echo "                      <Request_type> [Time_interval]"
    echo ""
    echo "    AliEn_user: the name of the corresponding AliEn user" 
	echo "                (registered for the used GRID certificate)."
    echo ""
    echo "    OCDB_path: path to the used OCDB file catalogue in AliEn (GRID), or"
	echo "                to a local file catalogue storing entries to be fetched."
	echo "                Which type (AliEn-GRID or local) is defined by parameter"
	echo "                '<Request_type>'."
	echo "                To take the path matching for the current LHC period use the"
	echo "                environment variable 'LHC_PERIOD_OCDB_PATH' ."          
    echo ""
    echo "    THCDB_path: path to the local THCDB file catalogue (T stands for Taxi)."
	echo "                This should be set to the environment variable 'ALIHLT_T_HCDBDIR'"
	echo ""
	echo "    Calibration_list: filename or foldername containing the list(s) of to be"
	echo "                fetched calibration objects. If folder name is given, the"
	echo "                lists from this folder are are merged."
	echo "                Since version 2.0, the Taxi can handle the wildcards '*' in the"
	echo "                entries of the lists. The '*' can be written instead of a"
	echo "                complete folder name. It can be used to fetch all entries for a"
	echo "                detector (e.g. \"TPC/*\"). "
	echo "                NOTE: This applies for the list entries, not for the parameter"
	echo "                'Calibration_list'."
	echo ""
	echo "    PW_file: file containing the certificate passphrase (incl. path)."
	echo ""
	echo "    Request_type: Defines if a AliEn-GRID request or local request shall be"
	echo "                made. If AliEn-GRID, the parameter has to be set to 'alien',"
	echo "                if local, the parameter has to be 'local'. For both types the"
	echo "                path for the request is given by the parameter 'OCDB_path'."
	echo ""
	echo "    Time_interval: the time interval, after which the Taxi should repeat its"
	echo "                task. The interval is given in minutes."
	echo "                (optional parameter - if it is left empty or is set to 0,"
	echo "                then Taxi exits after a single turn - no automatic repetition.)"
    echo ""
    echo " e.g. $0 bablok /alice/data/2008/LHC08d/OCDB /opt/T-HCDB /opt/T-HCDB/lists/lists-taxi /path/xY alien"
	echo "      or"
	echo "      $0 roehrich \$LHC_PERIOD_OCDB_PATH /opt/T-HCDB /opt/T-HCDB/lists/lists-taxi /path/xY alien 30"
	echo ""
	echo "    NOTE: In order to have the Taxi monitored by SysMES, the command line output"
	echo "          of the Taxi has to be piped to the following file:"
	echo "          /tmp/taxi.log . Monitoring is only done, if the Taxi runs on"
	echo "          'portal-taxi0' or 'portal-taxi1'. Use the following command:"
	echo "  $0 roehrich /alice/data/2008/LHC08d/OCDB /opt/T-HCDB /opt/T-HCDB/lists/lists-taxi /path/xY alien 30 >> /tmp/taxi.log"
	echo "    (If you want to life monitor it on the terminal use the tee command.)"
    echo ""
    exit 0
}

## function to intialize all required AliEn stuff
function initAliEn {
	echo "   --- Initialize AliEn."
	# init alien-token
	if [ ! -f $PW_file ]; then
		echo "   *** Grid Certificate Passphrase file does not exists. Exiting..."
		exit 9
	fi

	cat $PW_file | $ALIEN_ROOT/globus/bin/grid-proxy-init -pwstdin
	alienRetVal=$?
	if [ $alienRetVal -eq 0 ]; then
		echo "   --- Done Grid Proxy Init ($alienRetVal)."
	else
		echo "   *** Grid Proxy Init failed ($alienRetVal)."
		exit 9
	fi

	alien-token-init $AliEnUserName
	alienRetVal=$?
    if [ $alienRetVal -eq 0 ]; then
		echo "   --- Done AliEn Token Init ($alienRetVal)."
	else
		echo "   *** AliEn Token Init failed ($alienRetVal)."
		exit 9
	fi

#	source /tmp/gclient_env_$UID    # usage helps, but not required
	isInit=1

	# reset wrong settings due to bug in alien-token 
	# (migth change after next AliEn release) 
	# -> indeed has changed, don't use the next lines
#	unset alien_API_HOST
#	unset GCLIENT_SERVER_LIST
#	export alien_API_HOST='pcapiserv05.cern.ch'
#	export GCLIENT_SERVER_LIST='pcapiserv06.cern.ch:10000|pcapiserv05.cern.ch:10000'

	echo "   --- Set additional environmental variables."
}

## function to merge the list(s) of Calibration objects
function mergeLists {
	if [ $recursiveCounter -gt 20 ]; then
		echo "   *** Preparing of Lists failed $recursiveCounter times. Exiting..."
		cleanUp		# Exit if counter grater than 20
	fi
	pushd `dirname $0`
	./mergeLists.sh $ListFileName
	retVal=$?
	popd
    
	if [ $retVal == 1 ]; then
        MergedList=$ListFileName/$Merged_filename
		recursiveCounter=0	# set counter back to 0
    elif [ $retVal == 2 ]; then
        MergedList=${ListFileName%/*}/$Merged_filename
		recursiveCounter=0  # set counter back to 0
	else
		echo "   *** Merging of Lists failed. Retrying in five minutes..."
		sleep 300
		let recursiveCounter=$recursiveCounter+1
		mergeLists		# Recursive call of merge lists, 
						# if it failes 20 times, Taxi is aborted
    fi

}

## function to deinit AliEn stuff
function deinitAliEn {
	if [ $isInit -eq 1 ]; then
		echo "   --- De-initialize AliEn."
		# deinit alien-token
		alien-token-destroy
		$ALIEN_ROOT/globus/bin/grid-proxy-destroy
		isInit=0
	fi
}

## clean up after interrupt
function cleanUp {
	# make clean exit
	deinitAliEn

	echo "   --- TAXI driver script stopped ---"
	exit 0
}

## function to update the timestamp in timestamp var
function setTimeStamp {
	timestamp=`date "+%Y-%m-%d %H:%M:%S"`
} 



## main part of script

## check for provided parameters
if [ $1 ] && [ $2 ] && [ $3 ] && [ $4 ] && [ $5 ] && [ $6 ]; then
    if [ $7 ]; then
        timeInterval=$7 #uses default (30) if not set
    fi

	#set variables from parameters
	AliEnUserName="$1"
	OCDB_path="$2"
	T_HCDB_PATH="$3"
	ListFileName="$4"
	PW_file="$5"
	requestType="$6"

	# test if parameter is valid
	if [ $requestType != "alien" ] && [ $requestType != "local" ]; then
		echo "   *** Parameter for <alien / local> wrong ($requestType)."
		echo "   *** Has to either 'alien' or 'local'; exiting ..."
		echo ""
		usage
	fi 

	echo ""
	echo "   --- TAXI driver script started ($timestamp); Version: $VersionNumber ---"
	echo ""

	# register interupt handler
	trap 'cleanUp' INT

	# set environemt Variables
## Should be sourced by operator before
#	echo "   --- sourcing environment vars."
#	source Head-AliRootSetenv.sh 
#	source ./addEnv.sh

	# think of maybe make clean and make of AliHLTTaxiLib before start Taxi driver

	# print out settings
	echo "   --- Current settings:"
	echo "       AliEn user (registred for certificate): $AliEnUserName"
    echo "       Path to OCDB (in AliEn): $OCDB_path"
    echo "       Path to HCDB (local): $T_HCDB_PATH"
    echo "       File (Folder) name of calibration list(s): $ListFileName"
#	echo "       Filename of certificate passphrase: $PW_file"
	echo "       Request is: $requestType"
	echo "       Time interval: $timeInterval"
	echo ""


	# start loop for repetition of TAXI
	while [ true ]; do
		if [ $requestType == "alien" ]; then
			initAliEn		# initializes AliEn
		fi
		mergeLists		# produces the merged list of Calibration objects

		# run TAXI
		setTimeStamp	# update timestamp
		echo "   --- Running Taxi now ($timestamp) [starting of AliRoot can take some time] ---"
		echo ""
	
		pushd `dirname $0`	
		aliroot -q -b -l "contactOCDB.C(\"$AliEnUserName\", \"$OCDB_path\", \"$T_HCDB_PATH\", \"$MergedList\", \"$requestType\")"
		retVal=$?
		popd

		if [ $retVal -eq 10 ]; then
			echo "   --- Taxi (AliRoot) exited with '$retVal' (SUCCESS). ---"
		elif [ $retVal -eq 0 ]; then
			echo "   *** Taxi (AliRoot) exited with a segmentation fault or macro missing. Stopped"
			exit 6
		else
			echo "   *** Taxi (AliRoot) encountered an error while executing: '$retVal'."
		fi

		# produce current file list
		echo "   --- Producing new object list and storing it in $T_HCDB_PATH/$listHCDBName."
		localFolder=`pwd`
		cd $T_HCDB_PATH
		find . -name "*root" >> $listHCDBName_dummy
		chmod +r $listHCDBName_dummy
		mv $listHCDBName_dummy $listHCDBName
		cd $localFolder

		deinitAliEn

		# exit if time to sleep is 0
		if [ $timeInterval -eq 0 ]; then
			exit 0
		fi

		# sleep time interval
		setTimeStamp		#update timestamp
		echo "   --- Repeting procedure in $timeInterval minutes, sleeping ($timestamp)..."
		let sleepTime=$timeInterval*60
		sleep $sleepTime

		setTimeStamp		#update timestamp
		echo ""
		echo "   --- =Restarting Procedure= ($timestamp) --- "   # required by SysMes logging monitor
	done

else
	## not all necessary parameters provided
    echo "   *** Parameters missing, see usage ..."
    usage
fi

## this should never been reached
exit 7

## end of script

