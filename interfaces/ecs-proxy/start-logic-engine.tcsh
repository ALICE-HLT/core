#!/bin/tcsh

#/************************************************************************
#**
#**
#** This file is property of and copyright by the Department of Physics
#** Institute for Physic and Technology, University of Bergen,
#** Bergen, Norway, 2006
#** This file has been written by Sebastian Bablok,
#** sebastian.bablok@ift.uib.no
#**
#** Important: This file is provided without any warranty, including
#** fitness for any particular purpose.
#**
#**
#*************************************************************************/

## This script prepares the logic engine for the HLT-proxy and starts it.
## Afterwards it monitors its process. Should the HLT-proxy logic engine
## process die, the script restarts the logic engine automatically. Only if
## this script receives an interrrupt signal, it kills the logic engine and
## exits afterwards.

# set environment
#source .setup-all.tcsh

# Here is my state machine (logic engine)
# translate for logic engine
smiTrans ./src/hlt-states.smi
mv ./src/hlt-states.sobj ./bin/hlt-states.sobj

# set deafult for domain name
set DomainName = ALICE_HLT

# set the desired domain name, if provided
if ( $1 != "" ) then

	set DomainName = $1
	
endif

echo "   Domain name is set to $DomainName."
echo "   Starting logic engine now ..."
echo ""


# start logic engine
smiSM $DomainName ./bin/hlt-states.sobj &
set SMpid = $!
#catch interupt signals and jump to clean up
onintr CleanUp

echo HLT-proxy logic engine has PID $SMpid
echo

sleep 5

#start monitor of smi SM process to restart in case of missing process
while (1) 
	if ! ( `ps -C 'smiSM $DomainName ./bin/hlt-states.sobj' -o pid=` ) then 
		#secure CleanUp, in case restart fails
		set SMpid = 0 		
		echo HLT-proxy logic engine no longer running, trying restart ...
		smiSM $DomainName ./bin/hlt-states.sobj &
		set SMpid = $!
		echo HLT-proxy logic engine has PID $SMpid
		echo

	endif

	sleep 5

end

CleanUp:
if ($SMpid) then
	kill $SMpid
	echo Killed smiSM
endif

echo Exiting HLT-proxy logic engine

