# This is the property file, which contains the current settings of HLT proxy
# NOTE: There is only a "=" and no blank between a property name and its value

# Version of Property-file
[VersionNumber]=0.3

# Log level in the HLT Proxy. Possible are combinations (add the values) of:
# 1 (MSG_INFO), 2 (MSG_WARNING), 4 (MSG_ERROR), 32 (MSG_DEBUG), 64 (MSG_ALARM)
# e.g. MSG_INFO and MSG_DEBUG = 33; Note: MSG_ALARM is always set.
[LogLevel]=103

# Number of Master TaskManager
# This number here has to match the number of described Master TM connections
[NumberOfTM]=1
# The sleep time between each state polling of the TaskManagers (RunManager)
# The value is given in microseconds (NOTE: system automatically changes to 1
# second, if usleep is not supported.)
[TMPollingSleep]=500000

# Master TaskManager porperties
# the template for describing properties for the connection to TaskManager
# uses two digits and an underscore in the beginning to indicate the number
# of the TaskManager. The numbering starts with "01".
# Don't forget the "://" after the protocol type
[01_MasterTMProtocolType]=tcpmsg://
[01_MasterTMHostname]=portal-ecs
[01_MasterTMDomain]=internal
[01_MasterTMPort]=20000

# InterfaceLib Properties
# Don't forget the "://" after the protocol type
[InterfaceLibProtocolType]=tcpmsg://
[InterfaceLibLocalHostname]=portal-ecs
[InterfaceLibLocalDomain]=internal
[InterfaceLibLocalPort]=42024

# Configure parameter defaults
[DetectorListDefault]=ALICE_HLT
[HLTModePossibleValues]=ABCDE
[BeamTypeDefault]=pp
[DataFormatVersionDefault]=1
[HLTTriggerCodeDefault]=1
[CTPTriggerClassDefault]=
[HLTInDDLListDefault]=0
[HLTOutDDLListDefault]=0
[RunTypeDefault]=PHYSICS

