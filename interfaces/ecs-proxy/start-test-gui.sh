#!/bin/bash

#/************************************************************************
#**
#**
#** This file is property of and copyright by the Department of Physics
#** Institute for Physic and Technology, University of Bergen,
#** Bergen, Norway, 2006
#** This file has been written by Sebastian Bablok,
#** sebastian.bablok@ift.uib.no
#**
#** Important: This file is provided without any warranty, including
#** fitness for any particular purpose.
#**
#**
#*************************************************************************/

# bash version of start test gui script

# set environment
#. .setup-all.sh

# set default for domain name
DomainNameTop="ALICE_HLT::HLT"
DomainNameCon="ALICE_HLT::HLT_FWM"
DomainObj="HLT"
DomainLock="HLT_FWM"

# set the desired domain name, if provided
if [ $1 ]; then
	if [ $2 ]; then
		DomainNameTop="$1::$2"
		DomainNameCon="$1::$2_FWM"
	else
		DomainNameTop="$1::$DomainObj"
		DomainNameCon="$1::$DomainLock"
	fi	
fi

# set domain environment for gui
export HLTTOP=$DomainNameTop
export HLTCON=$DomainNameCon

echo "   Domain name is set to $HLTTOP."
echo "   Starting test gui now ..."
echo ""   
   
# start proxy
cd ./bin
./ecs_gui &
cd ..


