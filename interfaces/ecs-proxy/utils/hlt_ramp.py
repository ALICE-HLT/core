"""
python script to ramp up/down HLT
"""

import smi_connector
import time
from optparse import OptionParser
from threading import Lock

connector = None
run = True
terminate = False
hlt_handler_function = None
handler_lock = Lock()

def hlt_handler_up(state):
    """
    ramp HLT up to RUNNING state
    """
    handler_lock.acquire()
    global terminate
    print state
    if state.state == 'OFF':
        connector.send_command("ALICE_HLT::HLT", "INITIALIZE")
    if state.state == 'INITIALIZED':
        connector.send_command("ALICE_HLT::HLT", "CONFIGURE/RUN_TYPE=PHYSICS/HLT_IN_DDL_LIST=1/DATA_FORMAT_VERSION=1/BEAM_TYPE=A+A/HLT_OUT_DDL_LIST=1/HLT_TRIGGER_CODE=1/DETECTOR_LIST=1")
    if state.state == 'CONFIGURED':
        connector.send_command("ALICE_HLT::HLT", "ENGAGE/RUN_NUMBER=1/CTP_TRIGGER_CLASS=1/HLT_MODE=E")
    if state.state == 'READY':
        connector.send_command("ALICE_HLT::HLT", "START")
    if state.state == 'RUNNING':
        terminate = True
    handler_lock.release()
    if terminate:
        connector.get_status("ALICE_HLT::HLT_FWM")

def hlt_handler_down(state):
    """
    ramp hlt down to OFF state
    """
    handler_lock.acquire()
    global terminate
    print state
    if state.state == 'OFF':
        terminate = True
    if state.state == 'INITIALIZED':
        connector.send_command("ALICE_HLT::HLT", "SHUTDOWN")
    if state.state == 'CONFIGURED':
        connector.send_command("ALICE_HLT::HLT", "RESET")
    if state.state == 'READY':
        connector.send_command("ALICE_HLT::HLT", "DISENGAGE")
    if state.state == 'RUNNING':
        connector.send_command("ALICE_HLT::HLT", "STOP")
    handler_lock.release()
    if terminate:
        connector.get_status("ALICE_HLT::HLT_FWM")

def lock_handler(state):
    """
    handle lock operations - include/exclude
    """
    handler_lock.acquire()
    global run
    if state.state == 'EXCLUDED':
        if terminate:
            run = False
        else:
            print 'getting control'
            connector.send_command("ALICE_HLT::HLT_FWM", "INCLUDE/LOCKER=hlt_auto_ramp")
    if state.state == 'INCLUDED':
        if state.params['LOCKEDBY'].value == 'hlt_auto_ramp':
            if terminate:
                print "Releasing control"
                connector.send_command("ALICE_HLT::HLT_FWM", "EXCLUDE")
                run = False
            else:
                print "register hlt handler"
                connector.callback_handler("ALICE_HLT::HLT", hlt_handler_function)
        else:
            print 'Locked by %s - can not take control'%(state.params['LOCKEDBY'].value)
            run = False
    handler_lock.release()

def main():
    """
    ramp HLT to RUNNING or OFF
    """
    global connector
    global hlt_handler_function

    parser = OptionParser()
    parser.add_option("-u", "--up", help="ramp HLT to running", action="store_true", dest="ramp_up")
    parser.add_option("-d", "--down", help="ramp HLT to off", action="store_false", dest="ramp_up")
    (options, args) = parser.parse_args()
    if options.ramp_up:
        print "Ramping HLT up to RUNNING"
        hlt_handler_function = hlt_handler_up
    else:
        print "Ramping HLT down to OFF"
        hlt_handler_function = hlt_handler_down

    connector = smi_connector.SmiConnector()
    connector.connect("ALICE_HLT")
    connector.callback_handler("ALICE_HLT::HLT_FWM", lock_handler)
    while run:
        time.sleep(1)

if __name__ == "__main__":
    main()
