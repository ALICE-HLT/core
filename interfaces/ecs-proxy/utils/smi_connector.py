"""
smi_connector - python interface to the SMI state machine
"""

import ctypes
import time

class SmiState(object):
    """
    SmiState - SMI State object
    """
    class SmiParam(object):
        """
        SmiParam - SMI Parameter object
        """
        def __init__(self):
            self.type = -1
            self.name = None
            self.default = None
            self.value = None
    class SmiAction(object):
        """
        SmiAction - SMI Action object
        """
        def __init__(self):
            self.name = 'unknown'
            self.params = {}
    def __init__(self):
        self.object = None
        self.state = 'unknown'
        self.busy = False
        self.execute = 'unknown'
        self.params = {}
        self.actions = {}
    def __repr__(self):
        repr_string = 'SMI object: %s - '%(self.object)
        if self.busy:
            repr_string += ' busy executing %s\n'%(self.execute)
        else:
            repr_string += ' state: %s\n'%(self.state)
        for param in self.params:
            repr_string += '- parameter %s = %s\n'%(param, self.params[param].value)
        for action in self.actions:
            repr_string += '- action %s\n'%(action)
            for param in self.actions[action].params:
                repr_string += '  + parameter %s, default %s\n'%(param,
			self.actions[action].params[param].default)
        return repr_string
    def to_json(self):
        """
        returns a representation of the object suitable for serialization via JSON
        """
        repr_string = {}
        repr_string["OBJECT"] = self.object
        repr_string["STATE"] = self.state
        params = []
        for param in self.params:
            params.append([param, self.params[param].value])
        repr_string["PARAMS"] = params
        actions = []
        for action in self.actions:
            params = []
            for param in self.actions[action].params:
                params.append([self.actions[action].params[param].name, self.actions[action].params[param].default])
            actions.append({"NAME": action, "PARAMETERS": params})
        repr_string["ACTIONS"] = actions
        return repr_string


class SmiConnector(object):
    """
    SmiConnector - class handling the SMI connections
    """
    def __init__(self):
        self.dim = ctypes.CDLL("libdim.so", ctypes.RTLD_GLOBAL)
        self.smiui = ctypes.CDLL("libsmiui.so", ctypes.RTLD_GLOBAL)
        self.handler_func = {}
        self.callback_id = {}

    def connect(self, domain):
        """
        connect to SMI domain
        """
        self.smiui.smiui_connect_domain(domain)

    def get_status(self, domain_object):
        """
        trigger a call to the callback handler function of the specified
        domain_object to get a status update.
        Requires a defined callback handler before!
        """
        func = self.handler_func[domain_object]
        callback_id = self.callback_id[domain_object]
        if func and callback_id:
            c_id = ctypes.c_int(callback_id)
            c_param = ctypes.c_long(0)
            func(c_id, c_param)

    def callback_method(self, fct):
        """
        create callback method, calls fct
        """
        def callback(connection_id, param):
            """
            SMI callback method, reading state from SMI and calling handler function
            """
            state = SmiState()
            obj = ctypes.create_string_buffer(100)
            self.smiui.smiui_get_name(connection_id[0], obj)
            state.object = obj.value
            param = ctypes.create_string_buffer(2048)
            param_type = ctypes.c_int()
            value_size = ctypes.c_int()
            while self.smiui.smiui_get_next_obj_param(connection_id[0], param, ctypes.byref(param_type), ctypes.byref(value_size)):
                obj_param = SmiState.SmiParam()
                obj_param.name = param.value
                obj_param.type = param_type.value
                value = ctypes.create_string_buffer(2048)
                self.smiui.smiui_get_obj_param_value(connection_id[0], value)
                obj_param.value = value.value
                state.params[obj_param.name] = obj_param
            busy = ctypes.c_int()
            curr_state = ctypes.create_string_buffer(100)
            nac = ctypes.c_int()
            self.smiui.smiui_get_state(connection_id[0], ctypes.byref(busy),
					curr_state, ctypes.byref(nac))
            state.busy = busy.value
            action = ctypes.create_string_buffer(1024)
            if state.busy:
                self.smiui.smiui_get_action_in_progress(connection_id[0], action)
                state.state = "BUSY"
                state.execute = action.value
            else:
                state.state = curr_state.value
                while self.smiui.smiui_get_next_action(connection_id[0],
						action, ctypes.byref(nac)):
                    obj_action = SmiState.SmiAction()
                    obj_action.name = action.value
                    while self.smiui.smiui_get_next_param(connection_id[0], param, ctypes.byref(param_type), ctypes.byref(value_size)):
                        act_param = SmiState.SmiParam()
                        act_param.name = param.value
                        act_param.type = param_type.value
                        if value_size.value > 0:
                            self.smiui.smiui_get_param_default_value(connection_id[0], param)
                            act_param.default = param.value
                        obj_action.params[act_param.name] = act_param
                    state.actions[action.value] = obj_action
            fct(state)
        return callback

    def callback_handler(self, domain_object, func):
        """
        creates a ctype wrapper around the handler_func
        """
        handler_func = self.callback_method(func)
        param = ctypes.c_long(0)
        c_handler_function = ctypes.CFUNCTYPE(None, ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_long))
        self.handler_func[domain_object] = c_handler_function(handler_func)
        self.callback_id[domain_object] = self.smiui.smiui_book_statechange(domain_object, self.handler_func[domain_object], param)

    def send_command(self, domain_object, command):
        """
        Sends an SMI command to the specified domain_object
        """
        self.smiui.smiui_send_command(domain_object, command)

def test_fct(state):
    """
    test function for callback, simply printing the object
    """
    print state

def main():
    """
    simple build in test function
    """
    connector = SmiConnector()
    connector.connect("ALICE_HLT")
    connector.callback_handler("ALICE_HLT::HLT_FWM", test_fct)
    connector.callback_handler("ALICE_HLT::HLT", test_fct)
    while True:
        time.sleep(120)

if __name__ == "__main__":
    main()

