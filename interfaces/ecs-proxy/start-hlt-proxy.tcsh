#!/bin/tcsh

#/************************************************************************
#**
#**
#** This file is property of and copyright by the Department of Physics
#** Institute for Physic and Technology, University of Bergen,
#** Bergen, Norway, 2006
#** This file has been written by Sebastian Bablok,
#** sebastian.bablok@ift.uib.no
#**
#** Important: This file is provided without any warranty, including
#** fitness for any particular purpose.
#**
#**
#*************************************************************************/

# set environment
#source .setup-all.tcsh

# set default domain name
set DomainName = ALICE_HLT

# set the desired domain name, if provided
if ( $1 != "" ) then

	set DomainName = $1
	
endif

echo "   Domain name is set to $DomainName."
echo "   Starting Proxy now ..."
echo ""

# start proxy
./bin/hlt_SM_proxy --domain $DomainName

