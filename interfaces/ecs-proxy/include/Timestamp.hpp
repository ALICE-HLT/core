#ifndef ALICE_HLT_PROXY_TIMESTAMP_HPP
#define ALICE_HLT_PROXY_TIMESTAMP_HPP

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <string>
#include <time.h>
#include <sys/time.h>
#include <cstdio>

#include "log_levels.h"


namespace alice { namespace hlt { namespace proxy {


/**
 * Represents a timestamp for log entry.
 * The format of the date should be unique for all log entries.
 * So the creation of the log entry should be done with this struct.
 *
 * @author Christian Kofler, Sebastian Bablok
 *
 * @date 2004-04-06
 */
struct Timestamp { 
    /**
     * Constructs the Timestamp with the current date.
     * The format should be adapted here.
     */
    Timestamp() {
		timeval timeVal;
		struct tm* now;
		char tempDate[MSG_DATE_SIZE];  // (+4 for millisec) = 24 (incl '\0')
		char tempMsec[5];   // (+4 for millisec) 
		int nRet;

		nRet = gettimeofday(&timeVal, NULL);

		if (nRet != 0) {
			date = "No Timestamp available.";
		} else {
			now = localtime(&timeVal.tv_sec);

			int length = strftime(tempDate, MSG_DATE_SIZE, "%Y-%m-%d %H:%M:%S", now);
			// terminate with '\0'
			tempDate[length] = 0;
		    date = tempDate;
			tempMsec[sprintf(tempMsec, "-%3ld", (timeVal.tv_usec / 1000))] = 0;
			date.append(tempMsec);
		}


/*		// old version without milliseconds
  	    time_t timeVal;
		struct tm* now;
		char tempDate[MSG_DATE_SIZE];
	    
		time(&timeVal);
		now = localtime(&timeVal);
  	
		int length = strftime(tempDate, MSG_DATE_SIZE, "%Y-%m-%d %H:%M:%S", now);
        // terminate with '\0'
        tempDate[length] = 0;
		date = tempDate;
*/
    };

    /**
     * Destructs the timestamp
     */
    ~Timestamp() {};
    
    /**
     * Stores the timestamp in a string
     */
	std::string date;

	/**
	 * Stores the timestamp with milliseconds precision in a string
	 */
	std::string milliDate;

}; // end of struct


} } } // end namespaces "alice", "hlt" and "proxy"


#endif  // ALICE_HLT_PROXY_TIMESTAMP_HPP

