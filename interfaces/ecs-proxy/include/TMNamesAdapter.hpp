#ifndef ALICE_HLT_PROXY_TM_NAMES_ADAPTER_HPP
#define ALICE_HLT_PROXY_TM_NAMES_ADAPTER_HPP

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/
#include <string>


namespace alice { namespace hlt { namespace proxy {

/** 
 * Define return value to signal: "set the kill_slaves flag".
 */
#define HLT_SET_KILL_SLAVES_FLAG 20

/** 
 * Define return value to signal: "check the kill_slaves flag".
 */
#define HLT_CHECK_KILL_SLAVES_FLAG 10

/**
 * Define return value, when unknown (Master)TaskManager state name has been
 * inserted.
 */
#define HLT_UNKNOWN_TM_STATE -10

/**
 * Define return value, when unknown HLT-proxy transition command name has 
 * been given.
 */
#define	HLT_UNKNOWN_HLT_PROXY_COMMAND -20

/**
 * This class adaptes the names for states and transition commands of the
 * HLT-proxy to the names used in the (Master)TaskManager.
 *
 * @author Sebastian Bablok
 *
 * @date 2006-08-01
 */
class TMNamesAdapter {
	public:
		/**
		 * Destructor for the TMNamesAdapter.
		 */
		virtual ~TMNamesAdapter();

		/**
		 * Function to look up the name of a given TaskManager state name
		 * to the corresponding HLT-proxy state name. If a state cannot be
		 * mapped directly, the return value will indicate the further action
		 * that has to be taken.
		 *
		 * @param tmStateName string representing the state name of the 
		 *			(Master)TaskManager, that shall be mapped to a state name
		 *			of the HLT-proxy
		 * @param proxyStateName reference to the string, which shall be filled
		 *			with the corresponding HLT-proxy state name.
		 * 
		 * @return 0, if mapping has been successful and no further action has 
		 *			to be taken.<br>
		 *			<b>A positive value</b> indicates that further action
		 *			has to be taken to map/reach the appropriate state:<br>
		 *			HLT_CHECK_INITIALIZED_FLAG indicates that a check for the 
		 *			"kill_slaves" flag must be performed.<br>
		 *			<b>A negative value</b> indicates an error while mapping:<br>
		 *			ALICE_HLT_PROXY_NULL_POINTER is given and proxyStateName is
		 *			set to "ERROR", when char pointer is NULL.<br>
		 *			HLT_UNKNOWN_TM_STATE indicates that an unknown state name 
		 *			has been entered (proxyStateName is returned then as an
		 *			empty string).
		 */
		static int adaptTMStateName(const std::string tmStateName,
				std::string& proxyStateName);


		/**
		 * Function to look up the name of a given TaskManager state name
		 * to the corresponding HLT-proxy state name. If a state cannot be
		 * mapped directly, the return value will indicate the further action
		 * that has to be taken.
		 *
		 * @param tmStateName char array representing the state name of the 
		 *			(Master)TaskManager, that shall be mapped to a state name
		 *			of the HLT-proxy
		 * @param proxyStateName reference to the string, which shall be filled
		 *			with the corresponding HLT-proxy state name.
		 * 
		 * @return 0, if mapping has been successful and no further action has 
		 *			to be taken.<br>
		 *			<b>A positive value</b> indicates that further action
		 *			has to be taken to map/reach the appropriate state:<br>
		 *			HLT_CHECK_INITIALIZED_FLAG indicates that a check for the 
		 *			"kill_slaves" flag must be performed.<br>
		 *			<b>A negative value</b> indicates an error while mapping:<br>
		 *			ALICE_HLT_PROXY_NULL_POINTER is given and proxyStateName is
		 *			set to "ERROR", when char pointer is NULL.<br>
		 *			HLT_UNKNOWN_TM_STATE indicates that an unknown state name 
		 *			has been entered (proxyStateName is returned then as an
		 *			empty string).
		 */
		static int adaptTMStateName(const char* const tmStateName,
				std::string& proxyStateName);

		/**
		 * Function to look up the name of a given HLT-proxy (transition)
		 * command name to the corresponding (Master)TaskManager transition
		 * command name. The return value will indicate, if further action
		 * has to be taken to accomplish this command.
		 *
		 * @param proxyCommand the name of the (transition) command of the HLT-
		 *			proxy that shall be mapped.
		 * @param tmCommand reference to the string, which shall be filled with
		 *			the corresponding (Master)TaskManager (transition) command.
		 * @param tmState the current state of the (Master)TaskManager to which
		 *			the command will be sent.
		 *
		 * @return 0, if mapping has been successful and no further action has 
		 *			to be taken.<br>
		 *			<b>A positive value</b> indicates that further action
		 *			has to be taken to map/reach the appropriate state:<br>
		 *			<b>A negative value</b> indicates an error while mapping:<br>
		 *			ALICE_HLT_PROXY_NULL_POINTER is given and proxyStateName is
		 *			set to "ERROR", when char pointer is NULL.<br>
		 *			HLT_UNKNOWN_HLT_PROXY_COMMAND indicates that an unknown 
		 *			state name has been entered (proxyStateName is returned 
		 *			then as an empty string).
		 */
		static int adaptHLTProxyCommand(const std::string proxyCommand,
				std::string& tmCommand, std::string tmState);

		/**
		 * Function to look up the name of a given HLT-proxy (transition)
		 * command name to the corresponding (Master)TaskManager transition
		 * command name. The return value will indicate, if further action
		 * has to be taken to accomplish this command.
		 *
		 * @param proxyCommand the name of the (transition) command of the HLT-
		 *			proxy that shall be mapped.
		 * @param tmCommand reference to the string, which shall be filled with
		 *			the corresponding (Master)TaskManager (transition) command.
		 * @param tmState the current state of the (Master)TaskManager to which
		 *			the command will be sent.
		 *
		 * @return 0, if mapping has been successful and no further action has 
		 *			to be taken.<br>
		 *			<b>A positive value</b> indicates that further action
		 *			has to be taken to map/reach the appropriate state:<br>
		 *			<b>A negative value</b> indicates an error while mapping:<br>
		 *			ALICE_HLT_PROXY_NULL_POINTER is given and proxyStateName is
		 *			set to "ERROR", when char pointer is NULL.<br>
		 *			HLT_UNKNOWN_HLT_PROXY_COMMAND indicates that an unknown 
		 *			state name has been entered (proxyStateName is returned 
		 *			then as an empty string).
		 */
		static int adaptHLTProxyCommand(const char* const proxyCommand,
				std::string& tmCommand, std::string tmState);

	private:
		/**
		 * Constructor for Adapter of TaskManager state names and transition
		 * commands.
		 */
		TMNamesAdapter();



}; //end of class

} } } // end namespaces "alice", "hlt" and "proxy"


#endif
