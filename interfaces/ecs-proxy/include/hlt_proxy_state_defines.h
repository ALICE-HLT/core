#ifndef ALICE_HLT_PROXY_STATE_DEFINES_H
#define ALICE_HLT_PROXY_STATE_DEFINES_H

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

//----- States of the HLT-Proxy -----//

/** Define for HLT proxy state "OFF" */
#define HLT_PROXY_STATE_OFF "OFF"

/** Define for HLT proxy state "INITIALIZING" */
#define HLT_PROXY_STATE_INITIALIZING "INITIALIZING"

/** Define for HLT proxy state "INITIALIZED" */
#define HLT_PROXY_STATE_INITIALIZED "INITIALIZED"

/** Define for HLT proxy state "CONFIGURING" */
#define HLT_PROXY_STATE_CONFIGURING "CONFIGURING"

/** Define for HLT proxy state "CONFIGURED" */
#define HLT_PROXY_STATE_CONFIGURED "CONFIGURED"

/** Define for HLT proxy state "ENGAGING" */
#define HLT_PROXY_STATE_ENGAGING "ENGAGING"

/** Define for HLT proxy state "READY" */
#define HLT_PROXY_STATE_READY "READY"

/** Define for HLT proxy state "STARTING" */
#define HLT_PROXY_STATE_STARTING "STARTING"

/** Define for HLT proxy state "RUNNING" */
#define HLT_PROXY_STATE_RUNNING "RUNNING"

/** Define for HLT proxy state "COMPLETING" */
#define HLT_PROXY_STATE_COMPLETING "COMPLETING"

/** Define for HLT proxy state "DISENGAGING" */
#define HLT_PROXY_STATE_DISENGAGING "DISENGAGING"

/** Define for HLT proxy state "DECONFIGURING" */
#define HLT_PROXY_STATE_DECONFIGURING "DECONFIGURING"

/** Define for HLT proxy state "RAMPING_DOWN" */
#define HLT_PROXY_STATE_DEINITIALIZING "DEINITIALIZING"

/** Define for HLT proxy state "ERROR" */
#define HLT_PROXY_STATE_ERROR "ERROR"


//----- States of the (Master)TaskManager -----//

/** 
 * Define for TaskManager state really off.
 * This name doesn't correspond to a state name in the TaskManager
 */
#define HLT_TASKMANAGER_STATE_REAL_OFF "off"

/** Define for TaskManager state "slaves_dead" */
#define HLT_TASKMANAGER_STATE_OFF "slaves_dead"

/** Define for TaskManager transition state "INITIALIZING" */
#define HLT_TASKMANAGER_STATE_INITIALIZING "starting_slaves" // new state in TaskManager 

/** Define for TaskManager transition state "INITIALIZING" after ERROR state */
#define HLT_TASKMANAGER_STATE_KILLING_PROC "killing_processes" // new state in TaskManager

/** Define for TaskManager state "processes_dead" */
#define HLT_TASKMANAGER_STATE_INITIALIZED "processes_dead"

/** Define for TaskManager transition state "CONFIGURING" */
#define HLT_TASKMANAGER_STATE_CONFIGURING "starting" // new state in TaskManager

/** 
 * Define for TaskManager transition state "DECONFIGURING" 
 * Since ver 1.3 this is as well a own transition state in HLT proxy.
 */
#define HLT_TASKMANAGER_STATE_DECONFIGURING "stopping" // new state in TaskManager

/** Define for TaskManager state "ready_local" */
#define HLT_TASKMANAGER_STATE_CONFIGURED "ready_local"

/** Define for TaskManager transition state "ENGAGING" */
#define HLT_TASKMANAGER_STATE_ENGAGING "connecting" // new state in TaskManager

/** Define for TaskManager state "ready" */
#define HLT_TASKMANAGER_STATE_READY "ready"

/** Define for TaskManager state "running" */
#define HLT_TASKMANAGER_STATE_RUNNING "running"

/** Define for TaskManager state "busy" - equals to running */
#define HLT_TASKMANAGER_STATE_BUSY "busy"

/** Define for TaskManager state "paused" - equals to running */
#define HLT_TASKMANAGER_STATE_PAUSED "paused"

/** Define for TaskManager transition state "COMPLETING" */
#define HLT_TASKMANAGER_STATE_COMPLETING "stopping_run" // new state in TaskManager

/**
 * Define for TaskManager transition state towards "RUNNING"
 * Since ver 1.3 this is as well a own transition state in HLT proxy: STARTING 
 */
#define HLT_TASKMANAGER_STATE_START_RUNNING "starting_run" // new state in TaskManager

/** Define for TaskManager transition state "DISENGAGING" */
#define HLT_TASKMANAGER_STATE_DISENGAGING "disconnecting" // new state in TaskManager

/** Define for TaskManager transition state "DEINITIALIZING" */
#define HLT_TASKMANAGER_STATE_DEINITIALIZING "slave_kill" // new state in TaskManager

/** Define for TaskManager state "error" */
#define HLT_TASKMANAGER_STATE_ERROR "error"



//----- (Transition) commands of the HLT-proxy -----//

/** Define for HLT-proxy transition command "INITIALIZE" */
#define HLT_PROXY_COMMAND_INITIALIZE "INITIALIZE"

/** Define for HLT-proxy transition command "CONFIGURE" */
#define HLT_PROXY_COMMAND_CONFIGURE "CONFIGURE"

/** Define for HLT-proxy transition command "ENGAGE" */
#define HLT_PROXY_COMMAND_ENGAGE "ENGAGE"

/** Define for HLT-proxy transition command "START" */
#define HLT_PROXY_COMMAND_START "START"

/** Define for HLT-proxy transition command "STOP" */
#define HLT_PROXY_COMMAND_STOP "STOP"

/** Define for HLT-proxy transition command "DISENGAGE" */
#define HLT_PROXY_COMMAND_DISENGAGE "DISENGAGE"

/** Define for HLT-proxy transition command "RESET" */
#define HLT_PROXY_COMMAND_RESET "RESET"

/** Define for HLT-proxy transition command "SHUTDOWN" */
#define HLT_PROXY_COMMAND_SHUTDOWN "SHUTDOWN"



/** Define for HLT-proxy notification command "SET_ACTIVE" */
#define HLT_PROXY_COMMAND_ACTIVE "SET_ACTIVE"

/** Define for HLT-proxy notification command "SET_PASSIVE" */
#define HLT_PROXY_COMMAND_PASSIVE "SET_PASSIVE"


//----- (Transition) commands for the (Master)TaskManager -----//

/** Define for TaskManager transition command "start_slaves" */
#define HLT_TASKMANAGER_COMMAND_INITIALIZE "start_slaves"

/** Define for TaskManager transition command "start" */
#define HLT_TASKMANAGER_COMMAND_CONFIGURE "start"

/** Define for TaskManager transition command "connect" */
#define HLT_TASKMANAGER_COMMAND_ENGAGE "connect"

/** Define for TaskManager transition command "start_run" */
#define HLT_TASKMANAGER_COMMAND_START "start_run"

/** Define for TaskManager transition command "stop_run" */
#define HLT_TASKMANAGER_COMMAND_STOP "stop_run"

/** Define for TaskManager transition command "disconnect" */
#define HLT_TASKMANAGER_COMMAND_DISENGAGE "disconnect"

/** Define for TaskManager transition command "stop" */
#define HLT_TASKMANAGER_COMMAND_RESET "stop"

/** Define for TaskManager transition command "kill_slaves" */
#define HLT_TASKMANAGER_COMMAND_SHUTDOWN "kill_slaves"

/** 
 * Define for TaskManager transition command "kill_processes" 
 * SHUTDOWN from ERROR state.
 */
#define HLT_TASKMANAGER_COMMAND_RECOVER "kill_processes"



/** Define for TaskManager notification command "SET_ACTIVE" */
#define HLT_TASKMANAGER_COMMAND_ACTIVE "set_active"

/** Define for TaskManager notification command "SET_PASSIVE" */
#define HLT_TASKMANAGER_COMMAND_PASSIVE "set_passive"


#endif

