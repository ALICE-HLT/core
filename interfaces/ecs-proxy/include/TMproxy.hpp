#ifndef ALICE_HLT_PROXY_TMPROXY_HPP
#define ALICE_HLT_PROXY_TMPROXY_HPP

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <vector>
#include <string>
#include <pthread.h>

#include "HLTProxyProperties.hpp"
#include "PropertyReader.hpp"


namespace alice { namespace hlt { namespace proxy {

/** Define for the Property file name */
#define HLT_PROXY_PROPERTY_FILENAME "Properties.txt"

/** Define for the separator between the configure parameters */
#define HLT_CONFIGURE_PARAMETER_SEPARATOR ';'

/** 
 * Define for the sleep between requests in state request thread in 
 * microseconds. NOTE: if system does not support usleep() with more than a
 * second as value. Then HLT_MAX_SLEEP_TIME is used instead.
 *
 * Since version 1.1.0 this is been covered by a Property of the property file.
 * -> the value here is now only used as initial value  
 */
#define HLT_REQUEST_STATE_THREAD_SLEEP 500000 //5000000

/** 
 * Define for the max sleep time, if the current system does not support 
 * usleep() with more than a second.
 */
#define HLT_MAX_SLEEP_TIME 1000000

/** Define for retry attemps when sending a Command via InterfaceLib */
#define HLT_TM_SEND_COMMAND_RETRIES 3

/** 
 * Define for sleep between retries of sending command incase of 
 * error during sending. NOTE: value should be <= 1000000
 */
#define HLT_TM_SEND_COMMAND_RETRY_SLEEP 500000

class HLT;
class Parameters;
class MasterTM;

/**
 * This class takes care of the contact to the (multiple) master TaskManagers
 * of the HLT cluster.
 *
 * ...more explanation ... when more logic is included...
 *
 * @author Sebastian Bablok
 *
 * @date 2006-03-01
 */
class TMproxy {
	public:
		/**
		 * Constructor for the (master) TaskManager proxy.
		 * If the TM proxy cannot be properly initialized, its flag "valid"
		 * gets the value false and the proxy cannot be
		 * used to contact the master TMs. Look in the log file to find the
		 * error, most likely something wrong with the property file.
		 *
		 * @param pSmiProxy pointer to the HLT-proxy object
		 */
		TMproxy(HLT* pSmiProxy);

		/**
		 * Destructor for the TaskManager proxy
		 * !!! Pay attention to sequence in which all objects are cleared 
		 * here. All InterfaceLib parameters have to be released before the
		 * lib is terminated. !!!
		 */
		virtual ~TMproxy();

		/**
		 * Send a transition command to the master TaskManager, with parameters.
		 *
		 * @param command the transition command to send
		 * @param parameters pointer to the Parameter object containing the given 
		 *		parameters for this command. 
		 *
		 * @return 0 on success, else the appropriate error code
		 */
		int sendTransitionCommand(char* command, Parameters* parameters);

		/**
		 * Send a transition command to the master TaskManager, without parameters.
		 *
		 * @param command the transition command to send
		 *
		 * @return 0 on success, else the appropriate error code
		 */
		int sendTransitionCommand(char* command);

		/**
		 * Retrieves if the TM proxy is valid (properly initialized).
		 *
		 * @return true if TM proxy is valid, else false (see the Log file)
		 */
		bool isValid();

		/** 
		 * Passes the pointer of the InterfaceLib data (filled by the 
		 * InterfaceLib with the Initialize(...) call). When not properly
		 * initialized, this pointer can be NULL, so check it.
		 *
		 * @return pointer of the InterfaceLib data
		 */
		void* getPointerInterfaceLib_data();

		/** 
		 * Getter for the Property container
		 *
		 * @return copy of the property container
		 */
		HLTProxyProperties getPropertyContainer();

		/** 
		 * Updates the Property by re-reading the property file. This only  
		 * updates the default (and possible) values for the command 
		 * parameters. It also affects LogLevel and deplay between TM state
		 * polling, but NOT the number of TaskManager and there connection
		 * details.
		 *
		 * @return ture if update has been successful
		 */
		bool updatePropertiesFromFile();

		/**
		 * Sets all MasterTaskManager to a error equivalent state.
		 *
		 * @return number of MasterTaskManager, that has been set to error
		 *		equivalent state (Note: this number doesn't include Master
		 *		TaskManager, that are already in error (equivalent state))
		 */
		int setTMsToErrorEquivalentState();

		/**
		 * Static member to identify the callback of an interrupt.
		 */
		static int msInterrupt_Wait_Error;

		/**
		 * Static member to identify the callback of an interrupt.
		 */
		static int msInterrupt_Wait_Clean_Exit;

	protected:


	private:
		/**
		 * Constructs the local address from the corresponding values of the
		 * property file.
		 *
		 * @return char string representing the local address in the format:
		 *			<b>protocoltype://hostname.domain:portnumber</b>.
		 *			The memory for the char string is allocated inside this
		 *			function and has to be freed before the pointer is lost. 
		 *			<br> NOTE: If something goes wrong while constructing, a 
		 *			NULL pointer is returned.
		 */
		char* allocateLocalListenAddress();

		/**
		 * Constructs the address of one master TM from the corresponding 
		 * values of the property file. Which master TM is defined by the 
		 * number given to this methode, that corresponds to the number in the 
		 * property file.
		 *
		 * @param number the number of the master TaskManager (corresponds
		 *			to the number in the property file). 0 and numbers greater
		 *			than 99 are not allowed, and result in a return of NULL.
		 *			
		 *
		 * @return pointer to the string, which represents the desired master
		 *			TaskManager address in the format:
		 *			<b>protocoltype://hostname.domain:portnumber</b>.
		 *			The memory for the string is allocated inside this
		 *			function and has to be freed again before the pointer is
		 *			lost. <br>
		 *			NOTE: If something goes wrong while constructing, a NULL
		 *			pointer is returned.
		 */
		std::string* allocateMasterTMAddress(unsigned short number);

		/**
		 * Function to look up a given address in binary form for its string
		 * equivalent in the hosted MasterTMs. If the bin address can not be
		 * found in the hosted MasterTMs, "Unknown address" will be returned.
		 * NOTE: This function only returns the name of the address, NOT the
		 * complete string representation (not protocol and port)!
		 *
		 * @param processAddress_bin binary version of the address that shall
		 *			be looked up.
		 *
		 * @return string containing the corresponding string representation
		 *			for the given address, if it can be found in the hosted 
		 *			MasterTMs, if not: "Unknown address". NOTE: This function
		 *			only returns the name of the address, NOT the complete 
		 *			string representation (not protocol and port)!
		 */
		std::string convertBinAddressToName(void* processAddress_bin);

		/**
		 * Function to relay the recived command to all registered Master TMs.
		 *
		 * @param command the command to relay
		 * @param additionalData parameters given to a transition command. 
		 *			This has to be a NULL terminated string! Default = 0
		 *
		 * @return 0 for proper execution, else an error code
		 */
		int relayCommand(char* command, const char* additionalData = 0);
		
		/** 
		 * Function to signal a new state of the master TM(s). A evaluation of 
		 * all master TMs shall be performed before.
		 *
		 * @param newState the new state the masterTM has signaled
		 *
		 * @return true, if the transition is allowed, else false
		 */
		bool signalNewState(const std::string newState);

		/**
		 * Function, which runs in a separated thread and requests the state of
		 * all registered master TMs in regular time intervals.
		 *
		 * @param threadParam pointer to the calling/creating TMProxy object,
		 *			this paramter is given to the thread, that this static 
		 *			function can get access to the object members.
		 *
		 * @return exit value of the request state thread, 
		 *		(!!! think of possible values)
		 */
		static void* requestTMStateThread(void* threadParam);

		/**
		 * Function, which runs in a separated thread and starts the interrupt
		 * wait of the InterfaceLib. This needs to run in a seperate thread,
		 * because it will execute an endless loop.
		 *
		 * @param threadParam pointer to the calling/creating TMProxy object,
		 *			this paramter is given to the thread, that this static 
		 *			function can get access to the object members.
		 *
		 * @return exit value of the request state thread, 
		 *		(!!! think of possible values)
		 */
		static void* interruptWaitThread(void* threadParam);

		/**
		 * Callback routine for the InterruptWait of the InterfaceLib.
		 * For now it is empty (required just as input parameter for
		 * InterruptWait), later on it may contain some logic.
		 * Maybe this function will later on a friend instead of a member?!
		 *
		 * @param opaque_arg unchaged parameter passed through from InterruptWait
		 *			by the InterfaceLib. Possibility to foreward parameter(s).
		 * @param pid Unix process ID that triggered the interrupt
		 * @param process_id TaskManager internal process id of the process
		 *			which triggered the interrupt
		 * @param process_address_bin binary form of the address of the 
		 *			process which triggered the interrupt
		 * @param notificationData additional notification data, can be NULL
		 */
		static void interruptCallBack(void* opaque_arg, pid_t pid,
				const char* process_id, void* process_address_bin,
				const char* notificationData);

		/**
		 * Function to lock the protection mutex for the sleep time
		 */
		void lockSleepMutex();

		/**
		 * Function to unlock the protection mutex for the sleep time
		 */
		void unlockSleepMutex();

		/**
		 * Function to check, if TMProxy has connection any subscribed TaskManager.
		 *
		 * @return true, if TMProxy has connection to at least one TaskManager; else
		 *				false.
		 */
		bool hasConnection();

		/**
		 * Pointer to the smni proxy for the HLT
		 */
		HLT* mpHLTproxy;

		/**
		 * This variable indicates, if the TM - proxy has been properly 
		 * initialized. If not, this value is false and the proxy cannot be
		 * used to contact the master TMs. Look in the log file to find the
		 * error, most likely something wrong with the property file.
		 */
		bool mValid;

		/**
		 * Handle for the thread requesting the states of the master TMs.
		 */
		pthread_t mRequestTMsThreadHandle;

		/**
		 * Handle for the InterruptWait thread for the InterfaceLib.
		 */
		pthread_t mInterruptWaitThreadHandle;

		/**
		 * The PropertyReader object, which reads the properties of the
		 * HLT-proxy from file.
		 */
		PropertyReader* mpPR;

		/**
		 * Struct, that stores the properties for the contact to the master TMs
		 */
		HLTProxyProperties mProperties;

		/**
		 * Collection containing the objects corresponding to each connected 
		 * master TM.
		 */
		std::vector<MasterTM> mMasterTMCollection;

		/** 
		 * This pointer points to the intial data, produced by the InterfacLib
		 * during its Initialize(...) call.
		 */
		void* mpInterfaceLib_data;

		/**
		 * Local address of the TM proxy, on which it listens for interrupts
		 * and LAMs (Look-At-Me).
		 * The address has the format: 
		 * <b>protocoltype://hostname.domain:portnumber</b>
		 */
		char* mpLocal_listen_address;

		/**
		 * Stores the number of TaskManagers to contact to
		 * This is given by a Property value from file
		 */
		unsigned int mNumTMs;

		/**
		 * Stores the sleep time while polling TM states
		 */
		unsigned long mPollingSleep;

		/**
		 * Mutex to protect the polling sleep
		 */
		pthread_mutex_t mSleepMutex;


///////////////////////////////////////////////////////////////////////////////

		// intermediate dummy function
//		void emulateTMs(char* state);

		/**
		 * Function to get the current TaskManager settings from the PropertyReader
		 */
//		void retrieveTMsettings();


}; //end of class

inline bool TMproxy::isValid() {
	return mValid;
}

inline void* TMproxy::getPointerInterfaceLib_data() {
	return mpInterfaceLib_data;
}

inline HLTProxyProperties TMproxy::getPropertyContainer() {
	return mProperties;
}

} } } // end namespaces "alice", "hlt" and "proxy"

#endif

