#ifndef ALICE_HLT_PROXY_PROPERTY_DEFINES_H
#define ALICE_HLT_PROXY_PROPERTY_DEFINES_H

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

		
#define ALICE_HLT_PROXY_PROPERTY_VERSION_NUMBER "[VersionNumber]"

#define ALICE_HLT_PROXY_PROPERTY_LOG_LEVEL "[LogLevel]"

#define ALICE_HLT_PROXY_NUMBER_OF_TM "[NumberOfTM]"

#define ALICE_HLT_PROXY_TM_POLLING_SLEEP "[TMPollingSleep]"


#define ALICE_HLT_PROXY_MASTER_TM_PROTOCOL_TYPE_TEMPLATE "[**_MasterTMProtocolType]"

#define ALICE_HLT_PROXY_MASTER_TM_HOST_TEMPLATE "[**_MasterTMHostname]"

#define ALICE_HLT_PROXY_MASTER_TM_DOMAIN_TEMPLATE "[**_MasterTMDomain]"

#define ALICE_HLT_PROXY_MASTER_TM_PORT_TEMPLATE "[**_MasterTMPort]"


#define ALICE_HLT_PROXY_INTERFACE_LIB_PROTOCOL_TYPE "[InterfaceLibProtocolType]"

#define ALICE_HLT_PROXY_INTERFACE_LIB_LOCAL_HOST "[InterfaceLibLocalHostname]"

#define ALICE_HLT_PROXY_INTERFACE_LIB_LOCAL_DOMAIN "[InterfaceLibLocalDomain]"

#define ALICE_HLT_PROXY_INTERFACE_LIB_LOCAL_PORT "[InterfaceLibLocalPort]"


#define ALICE_HLT_PROXY_DETECTOR_LIST_DEFAULT "[DetectorListDefault]"

#define ALICE_HLT_PROXY_HLT_MODE_POSSIBLE_VALUES "[HLTModePossibleValues]"

#define ALICE_HLT_PROXY_BEAM_TYPE_DEFAULT "[BeamTypeDefault]"

#define ALICE_HLT_PROXY_DATA_FORMAT_VERSION_DEFAULT "[DataFormatVersionDefault]"

#define ALICE_HLT_PROXY_HLT_TRIGGER_CODE_DEFAULT "[HLTTriggerCodeDefault]"

#define ALICE_HLT_PROXY_CTP_TRIGGER_CLASS_DEFAULT "[CTPTriggerClassDefault]"

#define ALICE_HLT_PROXY_HLT_IN_DDL_LIST_DEFAULT "[HLTInDDLListDefault]"

#define ALICE_HLT_PROXY_HLT_OUT_DDL_LIST_DEFAULT "[HLTOutDDLListDefault]"

#define ALICE_HLT_PROXY_RUN_TYPE_DEFAULT "[RunTypeDefault]"


#endif
