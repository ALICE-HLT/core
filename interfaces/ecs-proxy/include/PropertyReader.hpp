#ifndef ALICE_HLT_PROXY_PROPERTY_READER_HPP
#define ALICE_HLT_PROXY_PROPERTY_READER_HPP

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <map>
#include <string>

#include "HLTProxyProperties.hpp"

namespace alice { namespace hlt { namespace proxy {


/**
 * Define for the maximum line length that can be read in the Property-file
 */
#define MAX_LINE_LENGTH 5120

/**
 * This class reads the properties for the HLT proxy for a given property file.
 * The PropertyReader is a Singleton, because there should be only one object,
 * which initially reads the Property file and stores its values.
 *
 * @author Sebastian Bablok
 *
 * @date 2006-03-02
 */
class PropertyReader {
	public:

		/**
		 * Getter function for the sinlgeton. This function checks, if an
		 * PropertyReader already exists and returns a pointer to that object.
		 * If not, the PropertyReader is constructed and then a pointer to it
		 * returned.
		 *
		 * @return pointer to the one and only PropertyReader object.
		 */
		static PropertyReader* getPropertyReader();

		/**
		 * Destructor for the PropertyReader of the HLT - TM poxy.
		 */
		virtual ~PropertyReader();

		/**
		 * Reads the properties from the property file of the HLT - TM - proxy
		 * and stores the name - value pairs in the map of this class.
		 *
		 * @param filename of the property file to read
		 *
		 * @return false, if file does not exist or file is empty, else true
		 */
		bool readPropertiesFromFile(const char* filename);

		/**
		 * Retrieves and returns the value of a given property name.
		 *
		 * @param propertyName the name of the requested property (as char)
		 * @param propertyValue [out] pointer to the string representing the 
		 *			value of the requested property
		 *
		 * @return true, if requested property has been found in the class, else
		 * 			false (false also when PropertyReader is not valid or
		 *			propertyName is NULL)
		 */
		bool retrievePropertyValue(const char* propertyName,
					std::string** propertyValue);

		/**
		 * Retrieves and returns the value of a given property name.
		 *
		 * @param propertyName pointer to the string containing the name of 
		 *			the requested property
		 * @param propertyValue [out] pointer to the string representing the 
		 *			value of the requested property
		 *
		 * @return true, if requested property has been found in the class, else
		 * 			false (false also when PropertyReader is not valid or
		 *			propertyName is NULL)
		 */
		bool retrievePropertyValue(const std::string* propertyName,
			std::string** propertyValue);

		/**
		 * Function to write all stored property name - value pairs to file.
		 * The output will look like: <br><b>[PropertyName]=PropertyValue</b>
		 *
		 * @param filename the name of the file to store the properties in
		 *
		 * @return false, if the filename is empty or the file could not be
		 *			opened to write to. (false also when PropertyReader is not
		 *			valid)
		 */
		bool writePropertiesToFile(const char* filename);

		/**
		 * Function to fill the HLTProxyProperties - struct with the read values.
		 *
		 * @param pHPP pointer to HLTProxyProperties - struct to fill
		 *
		 * @return if an error occurs during filling an appropriated error code
		 *			(negative value) is returned, else the number of set properties
		 */
		int setHLTProxyProperties(HLTProxyProperties* pHPP);

	private:
		/**
		 * Constructor for the PropertyReader of the HLT - TM poxy.
		 */
		PropertyReader();

		/**
		 * Function to convert a std string to upper cases
		 * 
		 * @param s string to convert
		 *
		 * @return true
		 */
		bool transformToUpper(std::string &s);

		/**
		 * This is the one and only PropertyReader - object.
		 * (see Singleton, GoF, Desing Patterns).
		 */
        static PropertyReader* mpInstance;

        /**
         * Flag, indicating if properties have already been read from file.
         */
        bool mValid;

		/**
		 * Map containing all PropertyName PropertyValue pairs read from the
		 * property file.
		 */
		std::map<std::string, std::string> mProperties;


}; //end of class

} } } // end namespaces "alice", "hlt" and "proxy"


#endif
