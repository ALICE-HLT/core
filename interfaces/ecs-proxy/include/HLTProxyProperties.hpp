#ifndef ALICE_HLT_PROXY_PROPERTIES_HPP
#define ALICE_HLT_PROXY_PROPERTIES_HPP

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <string>

namespace alice { namespace hlt { namespace proxy {

/**
 * Define for the currently required version number of the Property-file
 */
#define HLT_PROPERTY_FILE_VERSION_NUMBER "0.3"

/**
 * Represents a struct containing all properties concerning the HLT-proxy
 *
 * @author Sebastian Bablok
 *
 * @date 2006-03-01
 */
struct HLTProxyProperties {

	/**
	 * Version number required of the corresponding Property-file
	 */
	std::string mVersionNumber;

	/**
	 * The number of master TaskManager to contact
	 */
//	unsigned int mNumberOfTMs;  //-> removed in version 1.1.1

	/**
	 * The sleep time between each state polling of the TaskManagers
	 * in microseconds.
	 */
	unsigned long mTMPollingSleep;

	/**
	 * String containing the default list of detectors from the Property file.
	 */
	std::string mDetectorListDefault;

	/**
	 * String containing the possible values for the HLT mode, filled
	 * from Property file.
	 */
	std::string mPossibleHLTMode;

	/**
	 * String containing the default value for the beam type.
	 */
	std::string mBeamTypeDefault;

	/**
	 * String containing the default value for the data format version
	 */
	std::string mDataFormatVersionDefault;

	/**
	 * String containing the default value for the HLT trigger code
	 */
	std::string mHLTTriggerCodeDefault;

   	/**
     * String containing the default value for the CTP Trigger class
     */
    std::string mCTPTriggerDefault;

	/**
	 * String containing the default value for the HLT IN DDL list
	 */
	std::string mHLTInDDLListDefault;

	/**
	 * String containing the default value for the HLT OUT DDL list
	 */
	std::string mHLTOutDDLListDefault;

	/**
	 * String containing the default value for the Run Type
	 */
	std::string mRunTypeDefault;

	HLTProxyProperties() {
		mVersionNumber = HLT_PROPERTY_FILE_VERSION_NUMBER;
//		mNumberOfTMs = 0;
		mTMPollingSleep = 500000;
		mDetectorListDefault = "";
		mPossibleHLTMode = "";
		mBeamTypeDefault = "";
		mDataFormatVersionDefault = "";
		mHLTTriggerCodeDefault = "";
		mCTPTriggerDefault = "";
		mHLTInDDLListDefault = "";
		mHLTOutDDLListDefault = "";
		mRunTypeDefault = "";
	};
		
	/**
	 * Copy Constructor for a HLTProxyProperties struct
	 *
	 * @param hppRef HLTProxyProperties struct, which shall be copied
	 */
	HLTProxyProperties(const HLTProxyProperties& hppRef) {
		mVersionNumber =hppRef.mVersionNumber;
//		mNumberOfTMs = hppRef.mNumberOfTMs;
		mTMPollingSleep = hppRef.mTMPollingSleep;
		mDetectorListDefault = hppRef.mDetectorListDefault;
		mPossibleHLTMode = hppRef.mPossibleHLTMode;
		mBeamTypeDefault = hppRef.mBeamTypeDefault;
		mDataFormatVersionDefault = hppRef.mDataFormatVersionDefault;
		mHLTTriggerCodeDefault = hppRef.mHLTTriggerCodeDefault;
		mCTPTriggerDefault = hppRef.mCTPTriggerDefault;
		mHLTInDDLListDefault = hppRef.mHLTInDDLListDefault;
		mHLTOutDDLListDefault = hppRef.mHLTOutDDLListDefault;	
		mRunTypeDefault = hppRef.mRunTypeDefault;
	};

	/**
	 * Assignment operator for a HLTProxyProperties struct
	 *
	 * @param rhs HLTProxyProperties struct, which shall be copied
	 *
	 * @return reference to the copy of the HLTProxyProperties struct
	 */
	HLTProxyProperties& operator=(const HLTProxyProperties& rhs) {
		if (this == &rhs) {
			return *this;
		}
		mVersionNumber = rhs.mVersionNumber;
//		mNumberOfTMs = rhs.mNumberOfTMs;
		mTMPollingSleep = rhs.mTMPollingSleep;
		mDetectorListDefault = rhs.mDetectorListDefault;
		mPossibleHLTMode = rhs.mPossibleHLTMode;
		mBeamTypeDefault = rhs.mBeamTypeDefault;
		mDataFormatVersionDefault = rhs.mDataFormatVersionDefault;
		mHLTTriggerCodeDefault = rhs.mHLTTriggerCodeDefault;
		mCTPTriggerDefault = rhs.mCTPTriggerDefault;
		mHLTInDDLListDefault = rhs.mHLTInDDLListDefault;
		mHLTOutDDLListDefault = rhs.mHLTOutDDLListDefault;
		mRunTypeDefault = rhs.mRunTypeDefault;
		return *this;
	};

	~HLTProxyProperties() {

	};

}; //end of struct

} } } // end namespaces "alice", "hlt" and "proxy"


#endif // ALICE_HLT_PROXY_PROPERTIES_HPP

