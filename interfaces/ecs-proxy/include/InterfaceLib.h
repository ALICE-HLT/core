#ifndef _TMINTERFACELIBRARY_H_
#define _TMINTERFACELIBRARY_H_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author: bablok $ - Initial Version by Timm Morten Steinbeck
**
** $Id: InterfaceLib.h,v 1.2 2006/08/29 14:46:41 bablok Exp $ 
**
***************************************************************************
*/

/* Interface Library Functions: */

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

    /** 
     * Startup, initialize any static structures needed by the library. 
     * 
     * @param private_lib_data : void* output parameter. Will point to memory
	 *			allocated inside Initialize function. Used by library to store
	 *			internal data across calls. Has to be passed to every further 
	 *			library call.
     * @param interrupt_listen_address : A string with the address on which the
	 *			calling entity will listen for interrupts from controlled 
	 *			programs.
	 *
	 * @return in case of error != 0, errno can be fetched the normal linux way
     */
    int Initialize(void** private_lib_data, const char* interrupt_listen_address);

    /**
     * End, cleanup of library data
     * 
     * @param private_lib_data : The private library data allocated in Initialize.
	 *
	 * @return in case of error != 0, errno can be fetched the normal linux way
     */
    int Terminate(void* private_lib_data);

    /** 
     * Convert a given address string into a more compact binary
     * representation for immediate usability.
     * This can also be a copy of the given string if this is the most directly
     * useful representation.
     * The returned binary address will be released by a call to 
     * ReleaseResource (below).
     * 
     * @param private_lib_data : The private library data allocated in Initialize.
     * @param process_address_str : Input parameter, string form of the address
	 *			to be converted.
     * @param process_address_bin : Output parameter, binary form (void*) of the
	 *			converted address.
     *
	 * @return in case of error != 0, errno can be fetched the normal linux way
	 */
    int ConvertAddress(void* private_lib_data, const char* process_address_str,
			void** process_address_bin);

    /**
     * Compare two given binary address representations. 
     * Return 0 if they are equal, -1 if addr1 is less than addr2, 
     * +1 if addr1 is greater than addr2. If the magnitude comparisons
     * do not apply return unequal 0 for inequality. 
     * 
     * @param private_lib_data : The private library data allocated in Initialize.
     * @param addr1 : Input parameter, the first address to be compared
     * @param addr2 : Input parameter, the second address to be compared
	 *
     * @return in case of equality 0, else unequal 0
	 */
    int CompareAddresses(void* private_lib_data, void* addr1, void* addr2);

    /**
     * Query the state of a specific process state is allocated as a string in
	 * the function and has to be released via ReleaseResource (below) 
     * 
     * @param private_lib_data : The private library data allocated in Initialize.
     * @param pid : Input parameter, Unix pocess id of the process whose state is 
     *			to be queried.
     * @param process_id : Input parameter, TaskManager internal process id of the 
     *			process whose state is to be queried.
     * @param process_address_bin : Input parameter, Binary form of the address of 
     *			the process whose state is to be queried.
     * @param state : Output parameter, a char* to a string with the queried state.
     *
	 * @return in case of error != 0, errno can be fetched the normal linux way
	 */
    int QueryState(void* private_lib_data, pid_t pid, const char* process_id, 
			void* process_address_bin, char** state);

    /** 
     * Obtain additional status data from a specific process.
     * stat_data is allocated as a string in the function and has to be 
	 * released via Releaseresource (below)
     * 
     *  @param private_lib_data : The private library data allocated in 
	 *			Initialize.
     *  @param pid : Input parameter, Unix pocess id of the process whose 
     *			state is to be queried.
     *  @param process_id : Input parameter, TaskManager internal process 
     *          id of the process whose state is to be queried.
     *  @param process_address_bin : Input parameter, Binary form of the 
	 *			address of the process whose state is to be queried.
     *  @param stat_data : Output parameter, a char* to a string with the
	 *			queried status data.
     *
	 * @return in case of error != 0, errno can be fetched the normal linux way
	 */
    int QueryStatusData(void* private_lib_data, pid_t pid, 
			const char* process_id, void* process_address_bin, char** stat_data);

    /**
     * Release a resource allocated in another library call.
     * 
     * @param private_lib_data : The private library data allocated in Initialize.
     * @param resource : Input parameter, the resource to be freed.
     */
    void ReleaseResource(void* private_lib_data, void* resource);

    /** 
     * Send the specified command to the given process.
     * 
     * @param private_lib_data : The private library data allocated in 
	 *			Initialize.
     * @param pid : Input parameter, Unix pocess id of the process whose
	 *			state is to be queried.
     * @param process_id : Input parameter, TaskManager internal process
	 *			id of the process whose state is to be queried.
     * @param process_address_bin : Input parameter, binary form of the address
	 *			of the process whose state is to be queried.
     * @param command : Input parameter, a string with the command to be sent.
     *
	 * @return in case of error != 0, errno can be fetched the normal linux way
	 */
    int SendCommand(void* private_lib_data, pid_t pid, const char* process_id, 
		     void* process_address_bin, const char* command );

    /**
     * Callback function for the interrupt wait function below.
     * The last three parameters are used to identify the process
     * that triggered the interrupt. Only one of them (whichever
     * suits the library implementation best) has to be present. 
     * notificationData can be NULL, if no data was passed along
     * with the interrupt. 
     * 
     * @param opaque_arg : The second parameter passed to InterruptWait below,
     *			this is passed through to this function unchanged.
     * @param pid : Input parameter, Unix pocess id of the process that 
     *			triggered the interrupt.
     * @param process_id : Input parameter, TaskManager internal process id of 
     *			the process that triggered the interrupt.
     * @param process_address_bin : Input parameter, binary form of the address
	 *			of the process that triggered the interrupt.
     * @param notificationData : Input parameter, a char* with a string
	 *			containing additional notification data. May be NULL.
	 *
	 * @return in case of error != 0, errno can be fetched the normal linux way
     */
    typedef void (*InterruptTriggerCallback_t)(void* opaque_arg, pid_t pid,
			const char* process_id,	void* process_address_bin, 
			const char* notificationData);

    /** 
     * Enter a wait that calls the specified callback when a process 
     * has triggered an interrupt (LAM).
     * The address of the triggering process is passed in the call 
     * to the specified callback function.
     * This is called in its own thread to run as an endless loop and only 
	 * returns when the StopInterruptWait function (see below) is called.
     * 
     * @param private_lib_data : The private library data allocated in 
	 *			Initialize.
     * @param opaque_arg : Input parameter, this parameter is passed through
	 *			unchanged to the InterruptTriggerCallback_t function (see above)
	 *			passed as the last parameter to this function.
     * @param listen_address : Input parameter, string form of the address on
	 *			which the function should listen for incoming interrupts.
     * @param callback : Input parameter, a callback function that is called 
	 *			for each received interrupt.
     *
	 * @return in case of error != 0, errno can be fetched the normal linux way
	 */
    int InterruptWait(void* private_lib_data, void* opaque_arg, 
		       const char* listen_address, InterruptTriggerCallback_t callback);

    /** 
     * Stop the interrupt wait loop gracefully. 
     * 
     * @param private_lib_data : The private library data allocated in Initialize.
     *
	 * @return in case of error != 0, errno can be fetched the normal linux way
	 */
    int StopInterruptWait(void* private_lib_data);

    /**
	 * Connection function. Establish a permanent connection to the given 
	 * process.This function is optional and need not be supplied in the 
	 * interface library for compatibility reasons.
     * 
     * @param private_lib_data : The private library data allocated in
	 *			Initialize.
     * @param pid : Input parameter, Unix pocess id of the process to whom the 
     *			connection is to be established
     * @param process_id : Input parameter, TaskManager internal process id of
	 *			the process to whom the connection is to be established.
     * @param process_address_bin : Input parameter, binary form of the address
	 *			of the process to whom the connection is to be established.
     *
	 * @return in case of error != 0, errno can be fetched the normal linux way
	 */
    int ConnectToProcess(void* private_lib_data, pid_t pid,
			const char* process_id, void* process_address_bin);

    /**
	 * Disconnection function. Severe a previously established permanent
	 * connection to the given process.This function is optional and need not
	 * be supplied in the interface library for compatibility reasons.
     * 
     * @param private_lib_data : The private library data allocated in 
	 *			Initialize.
     * @param pid : Input parameter, Unix pocess id of the process whose 
     *			connection is to be severed
     * @param process_id : Input parameter, TaskManager internal process id of
	 *			the process whose connection is to be severed.
     * @param process_address_bin: Input parameter, binary form of the address
	 *			of the process whose connection is to be severed.
     *
	 * @return in case of error != 0, errno can be fetched the normal linux way
	 */
    int DisconnectFromProcess(void* private_lib_data, pid_t pid, 
			const char* process_id, void* process_address_bin);


#ifdef __cplusplus
}
#endif


/*
***************************************************************************
**
** $Author: bablok $ - Initial Version by Timm Morten Steinbeck
**
** $Id: InterfaceLib.h,v 1.2 2006/08/29 14:46:41 bablok Exp $ 
**
***************************************************************************
*/

#endif // _TMINTERFACELIBRARY_H_
