#ifndef ALICE_HLT_PROXY_ADDITIONAL_LOGGER_BASE_HPP
#define ALICE_HLT_PROXY_ADDITIONAL_LOGGER_BASE_HPP

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2008
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <string>


#include "log_levels.h"
#include "Timestamp.hpp"

namespace alice { namespace hlt { namespace proxy {


class ProxyLogger;


/**
 * Base class for additional logger classes, that shall be linked to the
 * ProxyLogger. All classes, that shall receive logging messages from the
 * ProxyLogger as well have to inherit from this class and must register in 
 * the ProxyLogger class via the dedicated function.
 *
 * @author Sebastian Bablok
 *
 * @date 2008-01-20
 */
class AdditionalLoggerBase {
	public:
		/**
		 * Constructor for the Additional Logger Base
		 *
		 * @param loggerName the name of the Logger
		 */
		AdditionalLoggerBase(std::string loggerName) : mLoggerName(loggerName) {};

		/**
		 * Copy-Constructor for the Additional Logger Base
		 * 
		 * @param albRef AdditionalLoggerBase reference from which the copy 
		 *			shall be made
		 */
		AdditionalLoggerBase(const AdditionalLoggerBase& albRef);

		/**
		 * Assignment operator for the Additional Logger Base
		 *
		 * @param rhs reference to AdditionalLoggerBase, from which the assignemnt 
		 *			shall be made.
		 * @return assigned AdditionalLoggerBase.
		 */
		virtual AdditionalLoggerBase& operator=(const AdditionalLoggerBase& rhs);

		/**
		 * Destructor for the Additional Logger Base
		 */
		virtual ~AdditionalLoggerBase() {};
    
		/**
		 * Function to relay a log message to the inherited logger class. 
		 * (Pure virtual)
		 *
		 * @param stamp the time stamp of the log message
		 * @param type the event type of this message
		 * @param origin the source of this message
		 * @param description the message body
		 *
		 * @return true if relaying has been successful, else false.
		 */
		virtual bool relayLogMessage(const Timestamp& stamp, Log_Levels type, 
				std::string origin, std::string description) = 0;

		/**
		 * Function to get the name of the logger (this means the name of the
		 * inherited logger module.)
		 *
		 * @return name of the logger (name of the inherited logger module)
		 */
		virtual std::string getLoggerName();

		/**
		 * Function to set the reference to the hosting ProxyLogger
		 *
		 * @param ref pointer to the hosting ProxyLogger
		 */
		void setProxyLoggerRef(ProxyLogger* ref);

	protected:
		/**
		 * Helper function to convert a binary representation of a 
		 * loglevel to a string. Can be used in inherited classes.
		 *
		 * @param logLevel the log level in binary format
		 *
		 * @return string representing the log level
		 */
		std::string convertLogLevel(Log_Levels logLevel);

		/** 
		 * Helper function to convert a int to a string.
		 * Can be used in inherited classes.
		 * 
		 * @param num integer number to convert
		 *
		 * @return string containing the converted integer
		 */
		std::string itos(int num);

		/** 
		 * Helper function to convert a float to a string.
		 * Can be used in inherited classes.
		 * 
		 * @param num float number to convert
		 *
		 * @return string containing the converted float
		 */
		std::string ftos(float num);

		/** 
		 * Helper function to convert a char array to a string.
		 * Note: the array has to be NULL terminated.
		 * Can be used in inherited classes.
		 * 
		 * @param buf char array to convert
		 *
		 * @return string containing the converted char array, if the given 
		 *			char pointer is 0, the string will contain "NULL".
		 */
		std::string catos(const char* const buf);

		/** 
		 * Helper function to convert a single char to a string.
		 * Can be used in inherited classes.
		 * 
		 * @param buf character to convert
		 *
		 * @return string containing the converted character
		 */
		std::string ctos(char buf);

		/** 
		 * helper function to convert a given error number from errno.h
		 * to a string. Can be used in inherited classes.
		 *
		 * @param num the error number that shall be converted
		 *
		 * @return string containing the converted error number, if the error
		 *			number is unknown "UNKNOWN-ERROR" is returned
		 */
		std::string errnotos(int num);


	private:

		/**
		 * Standard Constructor for the AdditionalLoggerBase 
		 */
		AdditionalLoggerBase() {};

		/**
		 * Name of the logger (from inherited logger module)
		 */
		std::string mLoggerName;

		/**
		 * Reference to the hosting ProxyLogger.
		 */
		ProxyLogger* mpProxyLog;


}; //end of class


inline std::string AdditionalLoggerBase::getLoggerName() {
	return mLoggerName;
}

inline void AdditionalLoggerBase::setProxyLoggerRef(ProxyLogger* ref) {
	mpProxyLog = ref;
}


} } } // end namespaces "alice", "hlt" and "proxy"

#endif // ALICE_HLT_PROXY_ADDITIONAL_LOGGER_BASE_HPP

