#ifndef ALICE_HLT_PROXY_HLT_HXX
#define ALICE_HLT_PROXY_HLT_HXX

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <smirtl.hxx>
#include "HLT-Proxy-Version.h"


class HLTProxy : public SmiProxy {
	protected:
	// Actions
		virtual void INITIALIZE() {};
		virtual void CONFIGURE() {};
		virtual void SHUTDOWN() {};
		virtual void ENGAGE() {};
		virtual void RESET() {};
		virtual void START() {};
		virtual void DISENGAGE() {};
		virtual void STOP() {};

	// active - passive commands
		virtual void SET_ACTIVE() {};
		virtual void SET_PASSIVE() {};

	// Parameters of action CONFIGURE
		char *getParDETECTOR_LIST() {
			return getParameterString("DETECTOR_LIST");
		};

		char *getParHLT_MODE() {
			return getParameterString("HLT_MODE");
		};

		char *getParBEAM_TYPE() {
			return getParameterString("BEAM_TYPE");
		};

		char *getParRUN_NUMBER() {
			return getParameterString("RUN_NUMBER");
		};

		char *getParDATA_FORMAT_VERSION() {
			return getParameterString("DATA_FORMAT_VERSION");
		};

		char *getParHLT_TRIGGER_CODE() {
			return getParameterString("HLT_TRIGGER_CODE");
		};

		char *getParCTP_TRIGGER_CLASS() {
			return getParameterString("CTP_TRIGGER_CLASS");
		};

		char *getParHLT_IN_DDL_LIST() {
			return getParameterString("HLT_IN_DDL_LIST");
		};

		char *getParHLT_OUT_DDL_LIST() {
			return getParameterString("HLT_OUT_DDL_LIST");
		};

        char *getParRUN_TYPE() {
            return getParameterString("RUN_TYPE");
        };

	// Command Handler
		void smiCommandHandler() {
			if(testAction("INITIALIZE"))
				INITIALIZE();
			else if(testAction("CONFIGURE"))
				CONFIGURE();
			else if(testAction("SHUTDOWN"))
				SHUTDOWN();
			else if(testAction("ENGAGE"))
				ENGAGE();
			else if(testAction("RESET"))
				RESET();
			else if(testAction("START"))
				START();
			else if(testAction("DISENGAGE"))
				DISENGAGE();
			else if(testAction("STOP"))
				STOP();
			else if(testAction("SET_ACTIVE"))
				SET_ACTIVE();
			else if(testAction("SET_PASSIVE"))
				SET_PASSIVE();
		};

	protected: // has been public before ...
	// States
		void setOFF() {
			setState("OFF");
		};

		void setINITIALIZING() {
			setState("INITIALIZING");
		};

		void setINITIALIZED() {
			setState("INITIALIZED");
		};

		void setCONFIGURING() {
			setState("CONFIGURING");
		};

		void setCONFIGURED() {
			setState("CONFIGURED");
		};

		void setENGAGING() {
			setState("ENGAGING");
		};

		void setREADY() {
			setState("READY");
		};
		
		void setSTARTING() {
			setState("STARTING");
		};

		void setRUNNING() {
			setState("RUNNING");
		};

		void setCOMPLETING() {
			setState("COMPLETING");
		};

		void setDISENGAGING() {
			setState("DISENGAGING");
		};

		void setDECONFIGURING() {
			setState("DECONFIGURING");
		};

		void setDEINITIALIZING() {
			setState("DEINITIALIZING");
		};

		void setERROR() {
			setState("ERROR");
		};
	public:
	// Object Parameters
	// Constructors
		HLTProxy(char *domain): SmiProxy(getenv(PROXY_MODULE_NAME_ENV)) { //SmiProxy("HLT") {
			attach(domain);
		};
};


/*
// implementation moved inside the class
void HLTProxy::smiCommandHandler() {
	if(testAction("INITIALIZE"))
		INITIALIZE();
	else if(testAction("CONFIGURE"))
		CONFIGURE();
	else if(testAction("SHUTDOWN"))
		SHUTDOWN();
	else if(testAction("ENGAGE"))
		ENGAGE();
	else if(testAction("RESET"))
		RESET();
	else if(testAction("START"))
		START();
	else if(testAction("DISENGAGE"))
		DISENGAGE();
	else if(testAction("STOP"))
		STOP();
}
*/

#endif

