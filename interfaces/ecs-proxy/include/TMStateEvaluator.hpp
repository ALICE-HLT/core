#ifndef ALICE_HLT_PROXY_TM_STATE_EVALUATOR_HPP
#define ALICE_HLT_PROXY_TM_STATE_EVALUATOR_HPP

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <map>
#include <string>

namespace alice { namespace hlt { namespace proxy {


/**
 * This class evaluates the current state of the HLT proxy according to the
 * states of the different master TaskManager states.
 * --> make sure that here is a queuing of the received state changes and
 * --> synchronize them
 *
 * ...more explanation ... when more logic is included... !!!
 *
 * @author Sebastian Bablok
 *
 * @date 2006-03-09
 */
class TMStateEvaluator {
	public:

		/**
		 * Constructor for the state evaluator
		 */
		TMStateEvaluator();

		/**
		 * Destructor of the state evaluator
		 */
		~TMStateEvaluator();

		/**
		 * ....fill when evaluation has been discussed!!!
		 */
//		bool signalStateChange(const char* newState, );

	private:
		/**
		 * Container for the evaluating rules
		 */
		std::map<std::string, std::string> mRules;

		/**
		 * Number of master TMs (think of making it const)
		 */
		unsigned int mNumberOfValidTMs;

		// store pointers to HLT-proxy and TMRepresentation (maybe also TMproxy)

}; //end of class

} } } // end namespaces "alice", "hlt" and "proxy"

#endif
