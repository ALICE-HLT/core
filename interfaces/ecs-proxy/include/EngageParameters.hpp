#ifndef ALICE_HLT_PROXY_ENGAGE_PARAMETERS_HPP
#define ALICE_HLT_PROXY_ENGAGE_PARAMETERS_HPP

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2008
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <string>

#include "Parameters.hpp"

namespace alice { namespace hlt { namespace proxy {

/** Define for the paramter descsribing HLT_MODE. */
#define ALICE_HLT_PROXY_HLT_MODE "HLT_MODE="

/** Define for the paramter descsribing RUN_NUMBER. */
#define ALICE_HLT_PROXY_RUN_NUMBER "RUN_NUMBER="

/** Define for the paramter descsribing CTP_TRIGGER_CLASS. */
#define ALICE_HLT_PROXY_CTP_TRIGGER_CLASS "CTP_TRIGGER_CLASS="


/**
 * This class contains the parameters, given by the ENGAGE command for the
 * HLT proxy.
 *
 * @author Sebastian Bablok
 *
 * @date 2008-01-18
 */
class EngageParameters : public Parameters {
	public:
		/**
		 * Constructor for a EngageParameters object
		 *
		 * @param mode the initial value for the mode
		 * @param runNumber run number of the current run (from ECS)
		 * @param ctpTrigger the initial value for the CTP Tigger classes
		 */
		EngageParameters(char* mode, unsigned int runNumber, char* ctpTrigger);

		/**
		 * Copy Constructor for a EngageParameters object
		 *
		 * @param epRef EngageParameters object, which shall be copied
		 */
		EngageParameters(const EngageParameters& epRef);

		/**
		 * Assignment operator for a EngageParameters object
		 *
		 * @param rhs EngageParameters object, which shall be copied
		 *
		 * @return reference to the copy of the EngageParameters object
		 */
		EngageParameters& operator=(const EngageParameters& rhs);

		/**
		 * Destructor for a EngageParameters object
		 */
		virtual ~EngageParameters();

        /**
         * This function replaces members, with "DEFAULT" as value with their
         * default value. If a real value is already provided, the member
         * remains unchanged.
		 * NOTE: No Parameter is changed since Engage parameters don't have a 
		 * DEFAULT value.
         *
         * @param hpp ref to the HLTProxyProperties struct containing the 
		 *			properties with the default values.
         *
         * @return zero, since Engage parameters don't have a DEFAULT value.
         */
        virtual int replaceDefaultValues(const HLTProxyProperties& hpp);
		
		/**
		 * Getter for the mode
		 *
		 * @return string representing the given HLT mode
		 */
		std::string getHLTMode();

		/**
		 * Getter for the run number
		 *
		 * @return the run number of the current run
		 */
		unsigned int getRunNumber();

		/**
		 * Getter for the CTP trigger
		 *
		 * @return string representing the given CTP trigger
		 */
		std::string getCTPTrigger();

		/**
		 * Function creates a string of all ENGAGE parameters in the order:
		 * HLT_MODE, RUN_NUMBER, CTP_TRIGGER_CLASS.  
		 * The separator can be defined via a parameter.
		 *
		 * @param separator a char defining the desired separator
		 * 
		 * @return string containing the concatanated parameters
		 */
		virtual std::string parToString(char separator);

		/**
		 * Function to print the currently stored parameters to the command line
		 */
		virtual void printParameters();

	private:
		/**
		 * String, containing the given HLT mode.
		 */
		std::string mHLTMode;

		/**
		 * Unsigned integer, containing the run number of the current run. 
		 * There will be no default value be allowed, this parameter has to
		 * be given by ECS.
		 */
		unsigned int mRunNumber;

		/**
		 * String, containing the given CTP (Central Trigger Processor )trigger
         * class(es). The CTP generates the level 1 and 2 trigger.  (If
         * "default", its value will be changed to the default value during
         * process).
		 */
		std::string mCTPTrigger;

        /**
         * Flag indicating, if members have been checked and replaced for
         * default values. Value is true after call of replaceDefaultValues(..)
         */
        bool mIsChecked;

}; //end of class


inline std::string EngageParameters::getHLTMode() {
	return mHLTMode;
}

inline unsigned int EngageParameters::getRunNumber() {
	return mRunNumber;
}

inline std::string EngageParameters::getCTPTrigger() {
    return mCTPTrigger;
}


} } } // end namespaces "alice", "hlt" and "proxy"


#endif // ALICE_HLT_PROXY_ENGAGE_PARAMETERS_HPP

