#ifndef ALICE_HLT_PROXY_HLT_PROXY_HPP
#define ALICE_HLT_PROXY_HLT_PROXY_HPP

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <string>

#include "hlt.hxx"
#include "ProxyLogger.hpp"


#ifndef __EMULATOR
#include "MLUCLogServer.hpp"
#endif

namespace alice { namespace hlt { namespace proxy {


class TMproxy;
//class ConfigureParameters;

/**
 * This class implements the HLT proxy, which interfaces the ECS via
 * Finite State Machines using SMI++.
 * It derives it function to conatc the SM (State Machine) from HLTProxy,
 * which integrates this proxy into the SMI++ HLT-domain.
 *
 * @author Sebastian Bablok
 *
 * @date 2006-01-10
 */
class HLT : public HLTProxy {

	public:
		// Constructors
		/**
		 * Constructor for the HLT object. This object is the core of the proxy.
		 *
		 * @param domain defines the domain the proxy is assigned to
		 * @param debug indicates if certain debug output shall be printed by
         *          TMproxy
		 */
		HLT(char* domain, bool debug = false) : HLTProxy(domain), mDomain(domain) {
			msDebugOutput = debug;
			initTMproxy();
		};

		/**
		 * Destructor for the HLT object.
		 */
		virtual ~HLT();

		/**
		 * Function to start the Proxy program
		 */
		void startProxy();

		/**
		 * Function to inform the state machine proxy, that the master TM(s)
		 * are in state OFF. (The evaluation of the different master TMs has
		 * been done before).
		 *
		 * @return true if the transition to this state is possible, else false
		 */
		bool informTMstateOFF();

		/**
		 * Function to inform the state machine proxy, that the master TM(s)
		 * are in state INITIALIZING. (The evaluation of the different master
		 * TMs has been done before).
		 *
		 * @return true if the transition to this state is possible, else false
		 */
		bool informTMstateINITIALIZING();

		/**
		 * Function to inform the state machine proxy, that the master TM(s)
		 * are in state INITIALIZED. (The evaluation of the different master
		 * TMs has been done before).
		 *
		 * @return true if the transition to this state is possible, else false
		 */
		bool informTMstateINITIALIZED();

		/**
		 * Function to inform the state machine proxy, that the master TM(s)
		 * are in state CONFIGURING. (The evaluation of the different master
		 * TMs has been done before).
		 *
		 * @return true if the transition to this state is possible, else false
		 */
		bool informTMstateCONFIGURING();

		/**
		 * Function to inform the state machine proxy, that the master TM(s)
		 * are in state CONFIGURED. (The evaluation of the different master
		 * TMs has been done before).
		 *
		 * @return true if the transition to this state is possible, else false
		 */
		bool informTMstateCONFIGURED();

		/**
		 * Function to inform the state machine proxy, that the master TM(s)
		 * are in state ENGAGING. (The evaluation of the different master
		 * TMs has been done before).
		 *
		 * @return true if the transition to this state is possible, else false
		 */
		bool informTMstateENGAGING();

		/**
		 * Function to inform the state machine proxy, that the master TM(s)
		 * are in state READY. (The evaluation of the different master
		 * TMs has been done before).
		 *
		 * @return true if the transition to this state is possible, else false
		 */
		bool informTMstateREADY();
	
		/**
		 * Function to inform the state machine proxy, that the master TM(s)
		 * are in state STARTING. (The evaluation of the different master
		 * TMs has been done before).
		 *
		 * @return true if the transition to this state is possible, else false
		 */
		bool informTMstateSTARTING(); 	

		/**
		 * Function to inform the state machine proxy, that the master TM(s)
		 * are in state RUNNING. (The evaluation of the different master
		 * TMs has been done before).
		 *
		 * @return true if the transition to this state is possible, else false
		 */
		bool informTMstateRUNNING();

		/**
		 * Function to inform the state machine proxy, that the master TM(s)
		 * are in state COMPLETING. (The evaluation of the different master
		 * TMs has been done before).
		 *
		 * @return true if the transition to this state is possible, else false
		 */
		bool informTMstateCOMPLETING();

		/**
		 * Function to inform the state machine proxy, that the master TM(s)
		 * are in state DISENGAGING. (The evaluation of the different master
		 * TMs has been done before).
		 *
		 * @return true if the transition to this state is possible, else false
		 */
		bool informTMstateDISENGAGING();

		/**
		 * Function to inform the state machine proxy, that the master TM(s)
		 * are in state DECONFIGURING. (The evaluation of the different master
		 * TMs has been done before).
		 *
		 * @return true if the transition to this state is possible, else false
		 */
        bool informTMstateDECONFIGURING();

		/**
		 * Function to inform the state machine proxy, that the master TM(s)
		 * are in state DEINITIALIZING. (The evaluation of the different master
		 * TMs has been done before).
		 *
		 * @return true if the transition to this state is possible, else false
		 */
		bool informTMstateDEINITIALIZING();

		/**
		 * Function to inform the state machine proxy, that the master TM(s)
		 * are in state ERROR. (The evaluation of the different master
		 * TMs has been done before).
		 *
		 * @return true if the transition to this state is possible, else false
		 */
		bool informTMstateERROR();

		/**
		 * Function to retrieve flag indicating if debug output is desired.
		 *
		 * @return true, if debug output shall be printed to terminal
		 */
		static bool isDebugOn();

	protected: // has been public before ...
		/** 
		 * Function to set the proxy to state OFF.
		 * Function makes a log entry amd then call the setOFF() of its 
		 * parent class.
		 */
		void setOFF();
		
		/** 
		 * Function to set the proxy to state INITIALIZING.
		 * Function makes a log entry amd then call the setINITIALIZING() 
		 * of its parent class.
		 */
		void setINITIALIZING();


		/** 
		 * Function to set the proxy to state INITIALIZED.
		 * Function makes a log entry amd then call the setINITIALIZED()
		 * of its parent class.
		 */
		void setINITIALIZED();

		/** 
		 * Function to set the proxy to state CONFIGURING.
		 * Function makes a log entry amd then call the setCONFIGURING() 
		 * of its parent class.
		 */
		void setCONFIGURING();

		/** 
		 * Function to set the proxy to state CONFIGURED.
		 * Function makes a log entry amd then call the setCONFIGURED()  
		 * of its parent class.
		 */
		void setCONFIGURED();

		/** 
		 * Function to set the proxy to state ENGAGING.
		 * Function makes a log entry amd then call the setENGAGING() of its 
		 * parent class.
		 */
		void setENGAGING();

		/** 
		 * Function to set the proxy to state READY.
		 * Function makes a log entry amd then call the setREADY() of its 
		 * parent class.
		 */
		void setREADY();

		/**
 		 * Function to set the proxy to state STARTING.
 		 * Function makes a log entry amd then call the setREADY() of its
 		 * parent class.
 		 */
		void setSTARTING();	

		/** 
		 * Function to set the proxy to state RUNNING.
		 * Function makes a log entry amd then call the setRUNNING() 
		 * of its parent class.
		 */
		void setRUNNING();

		/** 
		 * Function to set the proxy to state COMPLETING.
		 * Function makes a log entry amd then call the setCOMPLETING() 
		 * of its parent class.
		 */
		void setCOMPLETING();

		/** 
		 * Function to set the proxy to state DISENGAGING.
		 * Function makes a log entry amd then call the setDISENGAGING() 
		 * of its parent class.
		 */
		void setDISENGAGING();

		/**
		 * Function to set the proxy to state DECONFIGURING.
		 * Function makes a log entry amd then call the setDECONFIGURING()
		 * of its parent class.
		 */
		void setDECONFIGURING();

		/** 
		 * Function to set the proxy to state DEINITIALIZING.
		 * Function makes a log entry amd then call the setDEINITIALIZING() 
		 * of its parent class.
		 */
		void setDEINITIALIZING();

		/** 
		 * Function to set the proxy to state ERROR.
		 * Function makes a log entry amd then call the setERROR() of its 
		 * parent class.
		 */
		void setERROR();

		/**
		 * Function to reset current state.
		 * This function is needed, when a command shall be ignored: SMI 
		 * expects a setSTATE call after each command. With this command,
		 * The proxy remains in the same state and SMI receives the required
		 * setSTATE call.
		 */
		void resetCurrentState();

//	protected:
		// the actions performed as transitions between different states

		/**
		 * Command for transition from <b>"OFF"</b> to <b>"INITIALIZING"</b>.
		 * When <b>"INITIALIZING"</b> has been finished (and no error occured),
		 * an implicit transition to <b>"INITIALIZED"</b> is performed.
		 *
		 * ...Some more documentation about initializing ...
		 */
		virtual void INITIALIZE();

		/**
		 * Command for transition from <b>"INITIALIZED"</b> or <b>"CONFIGURED"</b>
		 * to <b>"CONFIGURING"</b>. When <b>"CONFIGURING"</b> has been finished
		 * (and no error occured), an implicit transition to <b>"CONFIGURED"</b>
		 * is performed.
		 *
		 * ...Some more documentation about configuring ...
		 */
		virtual void CONFIGURE();

		/**
		 * Command for transition from <b>"INITIALIZED"</b> or <b>"ERROR"</b>
		 * to <b>"RAMPING_DOWN"</b>.
		 * When <b>"RAMPING_DOWN"</b> has been finished, an implicit transition
		 * to <b>"OFF"</b> is performed.
		 *
		 * ...Some more documentation about shuting down ...
		 */
		virtual void SHUTDOWN();

		/**
		 * Command for transition from <b>"CONFIGURED"</b> to <b>"ENGAGING"</b>.
		 * When <b>"ENGAGING"</b> has been finished (and no error occured),
		 * an implicit transition to <b>"READY"</b> is performed.
		 *
		 * ...Some more documentation about engaging ...
		 */
		virtual void ENGAGE();

		/**
		 * Command for transition from <b>"CONFIGURED"</b> or <b>"ERROR"</b>
		 * to <b>"INITIALIZING"</b>.
		 * When <b>"INITIALIZING"</b> has been finished (and no error occured),
		 * an implicit transition to <b>"INITIALIZED"</b> is performed.
		 *
		 * ...Some more documentation about reseting ...
		 */
		virtual void RESET();

		/**
		 * Command for transition from <b>"READY"</b> to <b>"RUNNING"</b>.
		 *
		 * ...Some more documentation about starting ...
		 */
		virtual void START();

		/**
		 * Command for transition from <b>"READY"</b> to <b>"DISENGAGING"</b>.
		 * When <b>"DISENGAGING"</b> has been finished (and no error occured),
		 * an implicit transition to <b>"CONFIGURED"</b> is performed.
		 *
		 * ...Some more documentation about disengaging ...
		 */
		virtual void DISENGAGE();

		/**
		 * Command for transition from <b>"RUNNING"</b> to <b>"COMPLETING"</b>.
		 * When <b>"COMPLETING"</b> has been finished (and no error occured),
		 * an implicit transition to <b>"READY"</b> is performed.
		 *
		 * ...Some more documentation about stoping ...
		 */
		virtual void STOP();

		/**
		 * Command change the corresponding RunManager to be the ACTIVE one.
		 * This is used to switch between the production setup (ECS-proxy and 
		 * RunManager) to the fail-over (backup) setup. Only the states of the
		 * ACTIVE setup is regarded as valid.
		 */
		virtual void SET_ACTIVE();

		/**
		 * Command change the corresponding RunManager to be the PASSIVE one.
		 * This is used to switch between the production setup (ECS-proxy and 
		 * RunManager) to the fail-over (backup) setup. Only the states of the
		 * ACTIVE setup is regarded as valid, the PASSIVE one is discarded.
		 */
		virtual void SET_PASSIVE();

	private:
		/**
		 * Initializes the TaskManager proxy
		 */
		void initTMproxy();

		/**
		 * Function to transform all characters of a char-string to upper cases
		 * NOTE: the char-string has to be NULL terminated!
		 *
		 * @param str char-stream to convert
		 *
		 * @return true, if no error occured during conversion, else false
		 */
		bool transformToUpperCase(char* str);

		/**
		 * Function to print some debug output, if debug mode is set on.
		 * The function can be called, when a state transition is issued to 
		 * print out the old and new state of the HLT-Proxy. The print out is
		 * performed independent of the validity of the transition; this means
		 * the transition might not be executed, if transition is not valid - 
		 * another output should be made accordingly ...
		 *
		 * @param oldState the old state of the HLT - Proxy
		 * @param newState the new state of the HLT - Proxy
		 */
		void printStates(const char* oldState, const char* newState);

		/**
		 * The object taking care of the contact to the (master) TaskManagers.
		 */
		TMproxy* mpTaskManagerProxy;

		/**
		 * The logger for the HLT proxy. This logger can be accessed in all proxy 
		 * classes, as singleton. The pointer has to be assigned during the 
		 * construction of the HLTProxy and has to be valid until its destructor
		 * is called.
		 */
		ProxyLogger* mpLogger;

#ifndef __EMULATOR
		/**
		 * Stores the desiered MLUC additional logging server
		 * Can be one of the different types of the MLUCLogServers
		 */
		MLUCLogServer* mpMLUCLogger;

        /**
         * Stores the an additional MLUC server (for InfoLogger system)
         */
        MLUCLogServer* mpInfoLogger;
#endif

		/**
		 * Var to indicate if certain debug output is desired.
		 */
		static bool msDebugOutput;

		/**
		 * Stores the domain name of the corresponding partition.
		 */
		std::string mDomain;	


}; //end of class


inline void HLT::startProxy() {
	setOFF();
	// if proxy has to automatically restart and continue with state before 
	// restart/crash-restart maybe this is a solution:
	// getCurrentState() // from logic-enige or MasterTMs (??)
	// compare == dead (logic-engine) -> setState: OFF; else state of logic-engine
	// set state to MasterTM-state equivalent state!
}


inline bool HLT::isDebugOn() {
    return msDebugOutput;
}


} } } // end namespaces "alice", "hlt" and "proxy"

#endif
