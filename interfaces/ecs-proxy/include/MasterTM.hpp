#ifndef ALICE_HLT_PROXY_MASTER_TM_HPP
#define ALICE_HLT_PROXY_MASTER_TM_HPP

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <string>

namespace alice { namespace hlt { namespace proxy {

class TMproxy;

/** Define for return value MasterTaskManager is in new state */
#define MASTER_TM_HAS_NEW_STATE 10

/** Define for return value MasterTaskManager not valid */
#define MASTER_TM_NOT_VALID -10

/** Define for return value MasterTaskManager returned invalid state name */
#define MASTER_TM_INVALID_STATE_NAME -11

/** Define for return value Interface Lib returned error while requesting service */
#define MASTER_TM_INTERFACELIB_ERROR -12

/** Define for return value MasterTaskManager returned empty state string */
#define MASTER_TM_EMPTY_STATE_STRING -13

/** Define for max retries when QueryState fails. */
#define HLT_TM_QUERY_STATE_RETRIES 5

/**
 * ???
 *
 * @author Sebastian Bablok
 *
 * @date 2006-04-06
 */
class MasterTM {
	public:

		/**
		 * Constructor for a master TaskManager object
		 * 
		 * @param tmAddress the address of the corresponding Master 
		 *			TaskManager. This has the form: 
		 *			<b>protocoltype://hostname.domain:portnumber</b>
		 * @param proxy pointer to the hosting TaskManager - proxy.
		 */
		MasterTM(std::string tmAddress, TMproxy* proxy);

		/**
		 * Copy constructor for a master TaskManager object.
		 * <b>NOTE:</b> The object is only copied if the given MasterTM object
		 * is valid (member of it indicates that), else the created object
		 * is uninitialized and no values are copied from the original!
		 *
		 * @param mtm the master TM object to make a copy of
		 */
		MasterTM(const MasterTM& mtm);

		/**
		 * Assignment operator for a master TaskManager object.
		 * <b>NOTE:</b> The object is only assigned if the given MasterTM 
		 * object is valid (member of it indicates that), else the object to
		 * which the original shall be assigned to, remains uninitialized and
		 * no values will be copied from the original! Check the valid flag
		 * before usage!
		 *
		 * @param rhs the master TM object which shall be assinged to
		 */
		MasterTM operator=(const MasterTM& rhs); 

		/** 
		 * Destructor for the Master TM object
		 */
		virtual ~MasterTM();

		/**
		 * Getter for the MasaterTaskManager name.
		 * Will be most likely equivalent to the hostname or hostname and
		 * domainname (hostname.domainname), see constructor.
		 *
		 * @return name of the corresponding MasterTaskManager
		 */
		std::string getTMName();

		/**
		 * Getter for current state of the master TaskManager
		 *
		 * @return string representing the current state of the TM
		 */
		std::string getTMState();

		/**
		 * Getter of pointer to the binary version of the master TaskManager
		 * address. (The convertion is done by the InterfaceLib).
		 *
		 * @return TaskManager address as binary version
		 */
		void* getTMaddress_bin();

		/**
		 * Getter for flag, indicating if this master TaskManager is valid
		 *
		 * @return true, if corresponding master TM is valid
		 */
		bool isValid();

		/**
		 * Getter for the Connected flag, indicating if connect call has been
		 * successful. (@see mConnected and see InterfaceLib)
		 *
		 * @return true, if ConnectToProcess(...) has been successful, else false
		 */
		bool isConnected();

		/**
		 * Function set the "kill_slaves" flag.
		 */
		void setKillSlavesFlag();

		/**
		 * Function to set the corresponding MasterTaskManager in an error-
		 * equivalent state.
		 *
		 * @return true when successful
		 */
		bool setToErrorEquivalentState();

		/** 
		 * Function to set MasterTM to invalid
		 */
		void setInvalid();

		/**
		 * Function to trigger the sending of a command to the corresponding
		 * (Master)TaskManager.
		 *
		 * @param command the command to send
		 *
		 * @return 0, in case of success, else an error code is returned.
		 */
		int sendCommand(const char* command);

		/**
		 * Function to query the state of the corresponding 
		 * (Master)TaskManager.
		 *
		 * @param tmState outparameter, where the requested state of the 
		 *			corresponding (Master)TaskManager is stored/returned.
		 *
		 * @return 0, in case of success, else an error code is returned.
		 */
		int queryState(std::string& tmState);

		/**
		 * Function to request the "kill_slaves" flag.
		 *
		 * @return true, if the "kill_slaves" flag is set, else false.
		 */
//		bool requestKillSlavesFlag();


	private:
		/**
		 * Setter of new state of masterTM
		 *
		 * @param newState new state to set
		 */
		void setTMState(const std::string newState);

		/**
		 * Function to unset the "kill_slaves" flag.
		 */
		void unsetKillSlavesFlag();

		/**
		 * Function to test if the corresponding MasterTaskManager has signaled
		 * a new state.
		 *
		 * @param newState the newly requested state
		 * @param adapterRetVal the return value of the adapter call to 
		 *			transform the state name of the MasterTM to the 
		 *			corresponding of the Proxy (@see TMNamesAdapter)
		 *
		 * @return false, if the old state of the MasterTaskManager is the same
		 *			like the given one; true, when the state is new.
		 */
		bool isNewState(const std::string newState, int adapterRetVal);

		/**
		 * Address of the master TaskManger.
		 * The address has the format: 
		 * <b>protocoltype://hostname.domain:portnumber</b>
		 */
		char* mpTMaddress_str;

		/**
		 * This pointer contains the binary version of the master TaskManager
		 * address. (The convertion is done by the InterfaceLib).
		 */
		void* mpTMaddress_bin;

		/**
		 * String representation of the corresponding MasterTaskManager name.
		 * Will be most likely equivalent to the hostname or hostname and
		 * domainname (hostname.domainname), see constructor.
		 */
		std::string mTMName;

		/**
		 * Stores the current state of the master TaskManager
		 */
		std::string mTMState;

		/**
		 * Flag to indicate that an "kill_slaves" command shall be triggered
		 * when this MasterTaskManager has reached the state "INITIALIZED".
		 */
		bool mKillSlavesFlag;

		/**
		 * Flag, indicating if this master TaskManager is valid
		 */
		bool mValid;

		/**
		 * Flag, indicates if ConnectToProcess(...) call was successful during
		 * start up. Only if yes, a DisconnectFromProcess(...) should be called.
		 * NOTE: Disconnect should NOT be done in Destructor, because object 
		 * might be copied with Copy-Constructor and the original destroyed 
		 * afterwards without explicitly seen in source; call disconnect when
		 * hosting object (TMproxy) explicitly deletes its MasterTM objects 
		 * -> in Destructor of TMproxy.
		 * Note: even with a failed ConnectToProcess(...) call the MasterTM 
		 * might work correctly.
		 */
		bool mConnected;

		/** 
		 * This variable holds the number of failed QueryState calls.
		 */
		unsigned int mQueryStateRetryCount;

		/**
		 * Pointer to the TM proxy, which initializes the InterfaceLib
		 * and contains needed data for the communication.
		 */
		TMproxy* mpTMproxy;


}; //end of class

inline std::string MasterTM::getTMState() {
	return mTMState;
}

inline bool MasterTM::isValid() {
	return mValid;
}

inline void MasterTM::setTMState(std::string newState) {
	mTMState = newState;
}

inline std::string MasterTM::getTMName() {
	return mTMName;
}

inline void MasterTM::setKillSlavesFlag() {
	mKillSlavesFlag = true;
}

inline void MasterTM::unsetKillSlavesFlag() {
	mKillSlavesFlag = false;
}

inline bool MasterTM::isConnected() {
	return mConnected;
}

inline void* MasterTM::getTMaddress_bin() {
	return mpTMaddress_bin;
}


/*
inline bool MasterTM::requestKillSlavesFlag() {
	return mKillSlavesFlag;
}
*/

} } } // end namespaces "alice", "hlt" and "proxy"


#endif
