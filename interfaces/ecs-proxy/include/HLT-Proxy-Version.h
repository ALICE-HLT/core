#ifndef ALICE_HLT_PROXY_VERSION_NUMBER_H
#define ALICE_HLT_PROXY_VERSION_NUMBER_H

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2008
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/



/**
 * Defines the current version number of the HLT-proxy
 */
#define ALICE_HLT_PROXY_VERSION_NUMBER "1.5.4"

/**
 * Defines the environment variable conatining the proxy module name 
 * (HLT /HLT_BCK)
 */
#define PROXY_MODULE_NAME_ENV "HLTECS_MODULE"   // "ALIHLT_PROXY_MOD"

#endif // ALICE_HLT_PROXY_VERSION_NUMBER_H

