#ifndef ALICE_HLT_PROXY_PARAMETERS_HPP
#define ALICE_HLT_PROXY_PARAMETERS_HPP

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2008
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <string>

#include "HLTProxyProperties.hpp"

namespace alice { namespace hlt { namespace proxy {

/**
 * This class represents the base class for the parameters, passed to the
 * ECS proxy.
 *
 * @author Sebastian Bablok
 *
 * @date 2008-01-18
 */
class Parameters {
	public:
		/**
		 * Constructor of Parameters 
		 */
		Parameters();

		/**
		 * Copy Constructor of Parameters
		 *
		 * @param parRef Parameters object, which shall be copied
		 */
		Parameters(const Parameters& parRef);

		/**
		 * Assignment operator of Parameters 
		 *
		 * @param rhs Parameters object, which shall be copied
		 *
		 * @return reference to the copy of the Parameters object
		 */
		Parameters& operator=(const Parameters& rhs);

		/**
		 * Destructor for a Parameters object
		 */
		virtual ~Parameters();

		/**
		 * This function replaces members, with "DEFAULT" as value with their
		 * default value. If a real value is already provided, the member
		 * remains unchanged.
		 *
		 * @param hpp ref to the HLTProxyProperties struct containing the 
		 *			properties with the default values.
		 *
		 * @return the number of values, that have been changed. A negative
		 *      value indicates an error.
		 */
		virtual int replaceDefaultValues(const HLTProxyProperties& hpp) = 0;


		/**
		 * Pure virual function to convert parameters to concatenated string.
		 * To be implmented in child classes.
		 *
		 * @param separator a char defining the desired separator
		 * 
		 * @return string containing the concatanated parameters
		 */
		virtual std::string parToString(char separator) = 0;

		/**
		 * Pure virtual function to print the currently stored parameters 
		 * to the command line. To be implmented in child classes.
		 */
		virtual void printParameters() = 0;

	private:

}; //end of class


} } } // end namespaces "alice", "hlt" and "proxy"


#endif // ALICE_HLT_PROXY_PARAMETERS_HPP

