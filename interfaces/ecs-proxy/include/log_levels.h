#ifndef ALICE_HLT_PROXY_LOG_LEVELS_H
#define ALICE_HLT_PROXY_LOG_LEVELS_H

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

/**
 * Define for the even - type "INFO" .
 */
#define MSG_INFO    1

/**
 * Define for the even - type "WARNING" .
 */
#define MSG_WARNING 2

/**
 * Define for the even - type "ERROR" .
 */
#define MSG_ERROR   4

/**
 * Define for the even - type "DEBUG" .
 */
#define MSG_DEBUG   32

/**
 * Define for the even - type "ALARM" .
 */
#define MSG_ALARM   64

/**
 * Defines the maximum value of cumulated event - types.
 */
#define MSG_MAX_VAL 103

/**
 * Size of the date field in messages (in byte).
 */
#define MSG_DATE_SIZE           20

/**
 * Define for HLT proxy as log message source.
 */
#define LOG_SOURCE_PROXY "HLT-Proxy"

/**
 * Define for the default log level for the proxy.
 */
#define DEFAULT_LOCAL_LOGLEVEL MSG_INFO+MSG_WARNING+MSG_ERROR+MSG_ALARM+MSG_DEBUG


/**
 * Typedef for a log level
 */
typedef unsigned int Log_Levels;

#endif
