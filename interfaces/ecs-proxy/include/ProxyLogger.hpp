#ifndef ALICE_HLT_PROXY_LOGGER_HPP
#define ALICE_HLT_PROXY_LOGGER_HPP

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <string>
#include <fstream>
#include <time.h>
#include <sys/time.h>
#include <pthread.h>

#include <vector>

#include "log_levels.h"
#include "Timestamp.hpp"

#include "AdditionalLoggerBase.hpp"
#include "HLT-Proxy-Version.h"


namespace alice { namespace hlt { namespace proxy {


/**
 * This class represents the primary logger class for the HLT-ECS-Proxy.
 * Logging is done to a dedicated log file. If an additional logging facility
 * shall be included, the according logger, which has to be inherited from
 * AdditionalLoggerBase class, must be registered via registerAdditionalLogger()
 * function. Several additional logger at the same time are possible.
 *
 * @author Sebastian Bablok
 *
 * @date 2006-04-06 (update 2008-01-20)
 */
class ProxyLogger {
	public:

		/** Static string defining marker for MSG_INFO */
		static const std::string cmMSG_INFO_STRING;

		/** Static string defining marker for MSG_WARNING */
		static const std::string cmMSG_WARNING_STRING;

		/** Static string defining marker for MSG_ERROR */
		static const std::string cmMSG_ERROR_STRING;

		/** Static string defining marker for MSG_DEBUG */
		static const std::string cmMSG_DEBUG_STRING;

		/** Static string defining marker for MSG_ALARM */
		static const std::string cmMSG_ALARM_STRING;

		/**
		 * Destructor for the Logger
		 */
		virtual ~ProxyLogger();
    
		/**
		 * Prepares a logging message for saving to file and ev. sending it to
		 * the ECS via its message system. 
		 *
		 * @param type the event type of this message
		 * @param origin the source of this message
		 * @param description the message body
		 *
		 * @throw An exception is thrown, when this function is called, but
		 *			no Instance of ProxyLogger exists (mpInstance = 0)
		 *			!!! Not yet completely implemented !!!
		 */
		virtual void createLogMessage(Log_Levels type, std::string origin,
				std::string description);

    	/**
     	 * Setter of the LogLevel for log file. 
    	 * A check for a valid LogLevel value is also done (if not valid, a
		 * message is send). [<b>NOTE</b>: The log level <b>MSG_ALARM</b> is 
		 * always set!)
     	 * A valid LogLevel can be any combination of:<br><ul>
     	 * <li> INFO [1] </li>
     	 * <li> WARNING [2] </li>
     	 * <li> ERROR [4] </li>
     	 * <li> FAILURE AUDIT [8] </li>
     	 * <li> SUCCESS AUDIT [16] </li>
     	 * <li> DEBUG [32] </li>
     	 * <li> ALARM [64] </li></ul><br>
     	 *
    	 * @param logLevel the desired logLevel.
		 *
		 * @return true if logLevel is a valid log level else false
		 *
		 * @throw An exception is thrown, when this function is called, but
		 *			no Instance of ProxyLogger exists (mpInstance = 0)
		 *			!!! Not yet completely implemented !!!
    	 */
	    virtual bool setLogLevel(Log_Levels logLevel);

		/**
		 * Getter for current log level
		 *
		 * @return current logLevel
		 */
		virtual Log_Levels getLogLevel();

		/**
		 * Creates the Logger, if not already created.
		 * Note: if already created, the given filename is discarded and the 
		 * original one is used further (but you will find an entry in your 
		 * logfile).
		 *
		 * @param filename the desired filename (only used, if Logger is not
		 *			already existing)
		 *
		 * @return pointer to the ProxyLogger object
		 */
		static ProxyLogger* createLogger(std::string filename);

		/**
		 * Returns a pointer to the ProxyLogger, if it exists.
		 * If not, it creates a new logger with a predefined log file name.
		 *
		 * @return pointer to the ProxyLogger 
		 */
		static ProxyLogger* getLogger();

		/**
		 * Static helper function to convert a binary representation of a 
		 * loglevel to a string.
		 *
		 * @param logLevel the log level in binary format
		 *
		 * @return string representing the log level
		 */
		static std::string convertLogLevel(Log_Levels logLevel);

		/** 
		 * Static helper function to convert a int to a string.
		 * 
		 * @param num integer number to convert
		 *
		 * @return string containing the converted integer
		 */
		static std::string itos(int num);

		/** 
		 * Static helper function to convert a float to a string.
		 * 
		 * @param num float number to convert
		 *
		 * @return string containing the converted float
		 */
		static std::string ftos(float num);

		/** 
		 * Static helper function to convert a char array to a string.
		 * Note: the array has to be NULL terminated.
		 * 
		 * @param buf char array to convert
		 *
		 * @return string containing the converted char array, if the given 
		 *			char pointer is 0, the string will contain "NULL".
		 */
		static std::string catos(const char* const buf);

		/** 
		 * Static helper function to convert a single char to a string.
		 * 
		 * @param buf character to convert
		 *
		 * @return string containing the converted character
		 */
		static std::string ctos(char buf);

		/** 
		 * Static helper function to convert a given error number from errno.h
		 * to a string.
		 *
		 * @param num the error number that shall be converted
		 *
		 * @return string containing the converted error number, if the error
		 *			number is unknown "UNKNOWN-ERROR" is returned
		 */
		static std::string errnotos(int num);

		/**
		 * Function to register an additional Logger. The additional logger has
		 * to be inherited from AdditionalLoggerBase class. <br>
		 * NOTE: The additional logger does not replace the main ProxyLogger;
		 * log message are still printed to file by ProxyLogger. <br>
		 * NOTE: If the ProxyLogger is destroyed, the additional logger classes
		 * are deleted as well. After the ProxyLogger is destroyed, they cannot 
		 * be used anymore.
		 *
		 * @param addLogger pointer of the additional Logger class, that shall 
		 *			be included
		 *
		 * @return true, if registering has been successful, els false
		 */
		bool registerAdditionalLogger(AdditionalLoggerBase* addLogger);

		/**
		 * Function to get the number of additional logger classes, that are
		 * registered at teh ProxyLogger. Might be zero.
		 *
		 * @return the number of additional logger classes
		 */
		unsigned int getAdditionalLoggerNumber();

		/**
		 * Function to get names of all additional logger classes that are 
		 * registeresd. In case of none additional logger class "NONE; " is
		 * returned as string.
		 *
		 * @return names of all additional loggers that are registered. 
		 *			Each name is separeted by "; ". In case of no additional
		 *			logger "NONE; " is returned.
		 */
		std::string getAdditionalLoggerNames();


	private:

		/**
		 * Constructor for the Logger, private to create a sinlgeton
		 *
		 * @param filename name of the logfile
		 */
		ProxyLogger(std::string filename);

		/** 
		 * Function to lock the logging mutex.
		 */
		void lockLoggingMutex();

		/** 
		 * Function to unlock the logging mutex.
		 */
		void unlockLoggingMutex();

		/**
		 * Function to relay log message to additional registered logger 
		 * classes.
		 * 
		 * @param stamp the time stamp of the log message
		 * @param type the event type (log level) of the message
		 * @param origin the origin (module name), which produced the message
		 * @param description the actual log message
		 * 
		 * @return number of additional loggers, that have received this log
		 *			messages (is zero, if no additional logger is registerd).
		 */
		unsigned int relayLogMessageToAdditionalLoggers(const Timestamp& stamp,
				Log_Levels type, std::string origin, std::string description);

		/**
		 * This is the one and only ProxyLogger - object.
		 * (see Singleton, GoF, Desing Patterns).
		 */
		static ProxyLogger* mpInstance;

		/**
 		 * Filename of the logfile.
		 */
		std::string mFileName;

    	/**
     	 * Pointer to FileHandle for LogFile.
     	 */
    	std::ofstream* mpLogFile;

		/**
		 * Stores the current og level
		 */
		Log_Levels mLogLevel;

		/**
		 * Logging mutex to protect the Logger against concurrent access.
		 * This mutex protects the Logger in the createLogMessage() function
		 * to prevent more than one access to the log file at a time.
		 */
		pthread_mutex_t mLoggingMutex;

		/**
		 * Container for the additional logger classes which might be registered
		 */
		std::vector<AdditionalLoggerBase* > mAdditionalLoggers;

}; //end of class

// inliner to get created Logger
inline ProxyLogger* ProxyLogger::getLogger() {
    if (mpInstance == 0) {
        createLogger("HLT-Proxy.log");
    }
    return mpInstance;
}

inline Log_Levels ProxyLogger::getLogLevel() {
	return mLogLevel;
}

inline unsigned int ProxyLogger::getAdditionalLoggerNumber() {
	return mAdditionalLoggers.size();
}


} } } // end namespaces "alice", "hlt" and "proxy"


#endif // ALICE_HLT_PROXY_LOGGER_HPP


