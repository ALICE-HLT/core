#ifndef ALICE_HLT_PROXY_ERROR_CODES_H
#define ALICE_HLT_PROXY_ERROR_CODES_H

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

/**
 * Defines the success of execution
 */
#define ALICE_HLT_PROXY_SUCCESS 0

/**
 * Defines the error code for a NULL pointer
 */
#define ALICE_HLT_PROXY_NULL_POINTER -1

/**
 * Defines the error code for an invalid transition command parameter
 */
#define ALICE_HLT_PROXY_INVALID_TRANSITION_PARAMETER -10

/**
 * Defines the error code for transition command to TaskManager failed.
 */
#define ALICE_HLT_PROXY_TRANSITION_COMMAND_FAILED -20


#endif
