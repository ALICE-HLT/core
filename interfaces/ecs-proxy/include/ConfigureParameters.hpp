#ifndef ALICE_HLT_PROXY_CONFIGURE_PARAMETERS_HPP
#define ALICE_HLT_PROXY_CONFIGURE_PARAMETERS_HPP

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <string>

#include "Parameters.hpp"

namespace alice { namespace hlt { namespace proxy {

/** Define for the paramter descsribing DETECTOR_LIST. */
#define ALICE_HLT_PROXY_DETECTOR_LIST "DETECTOR_LIST="

/** Define for the paramter descsribing BEAM_TYPE. */
#define ALICE_HLT_PROXY_BEAM_TYPE "BEAM_TYPE="

/** Define for the paramter descsribing DATA_FORMAT_VERSION. */
#define ALICE_HLT_PROXY_DATA_FORMAT_VERSION "DATA_FORMAT_VERSION="

/** Define for the paramter descsribing HLT_TRIGGER_CODE. */
#define ALICE_HLT_PROXY_HLT_TRIGGER_CODE "HLT_TRIGGER_CODE="

/** Define for the paramter descsribing HLT_IN_DDL_LIST. */
#define ALICE_HLT_PROXY_HLT_IN_DDL_LIST "HLT_IN_DDL_LIST="

/** Define for the paramter descsribing HLT_OUT_DDL_LIST. */
#define ALICE_HLT_PROXY_HLT_OUT_DDL_LIST "HLT_OUT_DDL_LIST="

/** Define for the paramter describing RUN_TYPE. */
#define ALICE_HLT_PROXY_RUN_TYPE "RUN_TYPE="


/**
 * This class contains the parameters, given by the CONFIGURE command for the
 * HLT proxy.
 * Update in ConfigureParameters: The removal of HLT_MODE and RUN_NUMBER 
 * required slightly refactoring of this class. All part concerning these
 * two parameters have been removed and placed inside the class EngageParameters.
 * Since v 1.3 CTP_TRIGGER_CLASS has also been moved to EngageParameters.
 * Both classes are now inherited from the newe base class Parameters.
 *
 * @author Sebastian Bablok
 *
 * @date 2006-03-28 (update 2008-01-18)
 */
class ConfigureParameters : public Parameters {
	public:
		/**
		 * Constructor for a ConfigureParameters object
		 *
		 * @param detectors the inital list of the participating detectors
		 * @param type the initial value for the beam type
		 * @param format the initial value for the format version
		 * @param triggerClasses the initial value for the trigger classes
		 * @param inList the initial value for the IN-DDL list
		 * @param outList the initial value for the OUT-DDL list
		 * @param runType the initial value for the run type
		 */
		ConfigureParameters(char* detectors, char* type, char* format, 
				char* triggerClasses, char* inList, char* outList, 
				char* runType);

		/**
		 * Copy Constructor for a ConfigureParameters object
		 *
		 * @param cpRef ConfigureParameters object, which shall be copied
		 */
		ConfigureParameters(const ConfigureParameters& cpRef);

		/**
		 * Assignment operator for a ConfigureParameters object
		 *
		 * @param rhs ConfigureParameters object, which shall be copied
		 *
		 * @return reference to the copy of the ConfigureParameters object
		 */
		ConfigureParameters& operator=(const ConfigureParameters& rhs);

		/**
		 * Destructor for a ConfigureParameters object
		 */
		virtual ~ConfigureParameters();

        /**
         * This function replaces members, with "DEFAULT" as value with their
         * default value. If a real value is already provided, the member
         * remains unchanged.
         *
         * @param hpp ref to the HLTProxyProperties struct containing the 
		 * 			properties with the default values.
         *
         * @return the number of values, that have been changed. A negative
         *      value indicates an error.
         */
        virtual int replaceDefaultValues(const HLTProxyProperties& hpp);

		/**
		 * This function replaces members, with "DEFAULT" as value with their
		 * default value. If a real value is already provided, the member
		 * remains unchanged.
		 *
		 * @param defaultDetectors the given default list for detectors
		 * @param defaultType the given default value for the beam type
		 * @param defaultFormatVersion the given default value for the
		 *			format version
		 * @param defaultTriggerClasses the given default value for the
		 *			HLT trigger classes
		 * @param defaultInDDLList the given default value for the list of
		 *			IN DDLs
		 * @param defaultOutDDLList the given default value for the list of
		 *			OUT DDLs
		 * @param defaultRunType the given default value for the Run Type
		 *
		 * @return the number of values, that have been changed. A negative
		 *		value indicates an error.
		 */
		int replaceDefaultValues(std::string defaultDetectors, 
				std::string defaultType, std::string defaultFormatVersion, 
				std::string defaultTriggerClasses, std::string defaultInDDLList,
				std::string defaultOutDDLList, std::string defaultRunType);

		/**
		 * Getter for the detector list
		 *
		 * @return string representing the given detector list
		 */
		std::string getDetectorList();
		
		/**
		 * Getter for the beam type
		 *
		 * @return string representing the given beam type
		 */
		std::string getBeamType();

		/**
		 * Getter for the data format version
		 *
		 * @return string representing the given data format version
		 */
		std::string getDataFormatVersion();

		/**
		 * Getter for the HLT trigger classes
		 *
		 * @return string representing the given HLT trigger classes
		 */
		std::string getTriggerClasses();

		/**
		 * Getter for the HLT IN-DDL list
		 *
		 * @return string representing the given list of the IN-DDLs
		 */
		std::string getHLTInDDLList();

		/**
		 * Getter for the HLT OUT-DDL list
		 *
		 * @return string representing the given list of the OUT-DDLs
		 */
		std::string getHLTOutDDLList();

		/**
		 * Getter for Run Type
		 * @return string representing the given run type
		 */
		std::string getRunType();

		/**
		 * Function creates a string of all CONFIGURE parameters in the order:
		 * DETECTOR_LIST, BEAM_TYPE, DATA_FORMAT_VERSION, HLT_TRIGGER_CODE, 
		 * HLT_IN_DDL_LIST, HLT_OUT_DDL_LIST, RUN_TYPE.
		 * The separator can be defined via a parameter.
		 *
		 * @param separator a char defining the desired separator
		 * 
		 * @return string containing the concatanated parameters
		 */
		virtual std::string parToString(char separator);

		/**
		 * Function to print the currently stored CONFIGURE parameters to the 
		 * command line.
		 */
		virtual void printParameters();

	private:
		/**
		 * String, containing the given list of detectors.
		 */
		std::string mDetectorList;

		/**
		 * String, containing the given beam type (if "default", its value will
		 * be changed to the default value during process).
		 */
		std::string mBeamType;

		/**
		 * String, containing the given output data format version (if
		 * "default", its value will be changed to the default value during
		 * process).
		 */
		std::string mDataFormatVersion;

		/**
		 * String, containing the given trigger classes (if "default", its
		 * value will be changed to the default value during process).
		 */
		std::string mTriggerClasses;

		/**
		 * String, containing the given list of IN DDLs (if "default", its
		 * value will be changed to the default value during process).
		 */
		std::string mHLTInDDLList;

		/**
		 * String, containing the given list of OUT DDLs  (if "default", its
		 * value will be changed to the default value during process).
		 */
		std::string mHLTOutDDLList;

        /**
         * String, containing the given run type (if "default", its
         * value will be changed to the default value during process).
         */
		std::string mRunType;

		/**
		 * Flag indicating, if members have been checked and replaced for
		 * default values. Value is true after call of replaceDefaultValues(..)
		 */
		bool mIsChecked;

}; //end of class

inline std::string ConfigureParameters::getDetectorList(){
	return mDetectorList;
}

inline std::string ConfigureParameters::getBeamType() {
	return mBeamType;
}

inline std::string ConfigureParameters::getDataFormatVersion() {
	return mDataFormatVersion;
}

inline std::string ConfigureParameters::getTriggerClasses() {
	return mTriggerClasses;
}

inline std::string ConfigureParameters::getHLTInDDLList() {
	return mHLTInDDLList;
}

inline std::string ConfigureParameters::getHLTOutDDLList() {
	return mHLTOutDDLList;
}

inline std::string ConfigureParameters::getRunType() {
	return mRunType;
}


} } } // end namespaces "alice", "hlt" and "proxy"


#endif // ALICE_HLT_PROXY_CONFIGURE_PARAMETERS_HPP

