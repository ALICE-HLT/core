#ifndef ALICE_HLT_PROXY_ADDITIONAL_LOGGER_DUMMY_HPP
#define ALICE_HLT_PROXY_ADDITIONAL_LOGGER_DUMMY_HPP

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2008
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <string>
#include <fstream>

#include "AdditionalLoggerBase.hpp"


namespace alice { namespace hlt { namespace proxy {



/**
 * Dummy class, that can be registered as additional logging module to the 
 * ProxyLogger. Its main purpose is to test the new "Additional-logging" 
 * framework. After registering this class the HLT-ECS-Proxy log messages
 * are received. These log messsage are written to a seperated file.
 *
 * @author Sebastian Bablok
 *
 * @date 2008-01-20
 */
class AdditionalLoggerDummy : public AdditionalLoggerBase {
	public:
		/**
		 * Constructor for the Additional Logger Dummy
		 *
		 * @param filename where the log message shall be stored 
		 */
		AdditionalLoggerDummy(std::string filename);

		/**
		 * Copy-Constructor for the Additional Logger Dummy
		 * 
		 * @param aldRef AdditionalLoggerDummy reference from which the copy 
		 *			shall be made
		 */
		AdditionalLoggerDummy(const AdditionalLoggerDummy& aldRef);

		/**
		 * Assignment operator for the Additional Logger Dummy
		 *
		 * @param rhs reference to AdditionalLoggerDummy, from which the 
		 * 			assignemnt shall be made.
		 * @return assigned AdditionalLoggerDummy
		 */
		virtual AdditionalLoggerDummy& operator=(const AdditionalLoggerDummy& rhs);

		/**
		 * Destructor for the Additional Logger Dummy
		 */
		virtual ~AdditionalLoggerDummy();
    
		/**
		 * Function where this logging module receives the log messages of 
		 * the Proxy Logger.
		 *
		 * @param stamp the time stamp of the log message
		 * @param type the event type of this message
		 * @param origin the source of this message
		 * @param description the message body
		 *
		 * @return true if processing of log message has been successful, 
		 *			else false.
		 */
		virtual bool relayLogMessage(const Timestamp& stamp, Log_Levels type, 
				std::string origin, std::string description);

	
	private:

		/**
		 * Standard Constructor for the AdditionalLoggerDummy
		 */
//		AdditionalLoggerDummy();

		/**
		 * Name of the file where log messages are stored 
		 */
		std::string mFilename;

        /**
         * Pointer to FileHandle for the LogFile.
         */
        std::ofstream* mpLogFile;

		/**
		 * Defines the name of this additional logger module
		 */
		static const std::string cmLOGGER_NAME;


}; //end of class


} } } // end namespaces "alice", "hlt" and "proxy"

#endif // ALICE_HLT_PROXY_ADDITIONAL_LOGGER_DUMMY_HPP

