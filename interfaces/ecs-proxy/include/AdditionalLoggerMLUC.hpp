#ifndef ALICE_HLT_PROXY_ADDITIONAL_LOGGER_MLUC_HPP
#define ALICE_HLT_PROXY_ADDITIONAL_LOGGER_MLUC_HPP

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2008
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

//#include <string>

#include <vector>

#include "AdditionalLoggerBase.hpp"

#include "MLUCLog.hpp"					// from HLT - framework
#include "MLUCLogServer.hpp"			// from HLT - framework



namespace alice { namespace hlt { namespace proxy {



/**
 * MLUC logging adapter, that can be registered as additional logging module to 
 * the ProxyLogger. After registering this class HLT-ECS-Proxy log messages
 * are received. These log messsage are relayed to the central logging facility
 * of the TaskManager system.
 *
 * @author Sebastian Bablok
 *
 * @date 2008-01-22
 */
class AdditionalLoggerMLUC : public AdditionalLoggerBase {
	public:
		/**
		 * Constructor for the Additional Logger Dummy
		 *
		 * @param logServer pointer to the desired type ot MLUC logging server
		 */
		AdditionalLoggerMLUC(MLUCLogServer* logServer);

		/**
		 * Copy-Constructor for the Additional Logger Dummy
		 * 
		 * @param almRef AdditionalLoggerMLUC reference from which the copy 
		 *			shall be made
		 */
		AdditionalLoggerMLUC(const AdditionalLoggerMLUC& almRef);

		/**
		 * Assignment operator for the Additional Logger MLUC
		 *
		 * @param rhs reference to AdditionalLoggerMLUC, from which the 
		 * 			assignemnt shall be made.
		 * @return assigned AdditionalLoggerMLUC
		 */
		virtual AdditionalLoggerMLUC& operator=(const AdditionalLoggerMLUC& rhs);

		/**
		 * Destructor for the Additional Logger MLUC
		 */
		virtual ~AdditionalLoggerMLUC();
    
		/**
		 * Function where this logging module receives the log messages of 
		 * the Proxy Logger.
		 *
		 * @param stamp the time stamp of the log message
		 * @param type the event type of this message
		 * @param origin the source of this message
		 * @param description the message body
		 *
		 * @return true if processing of log message has been successful, 
		 *			else false.
		 */
		virtual bool relayLogMessage(const Timestamp& stamp, Log_Levels type, 
				std::string origin, std::string description);

		/**
		 * Function to add an additional MLUCLogServer
		 * 
		 * @param logServer pointer to a MLUCLogServer to add
		 */
		virtual void addLogServer(MLUCLogServer* logServer);

	protected:
		/**
		 * Function to map the Proxy log level to the one used in the MLUC lib.
		 * 
		 * @param type the given Proxy log level
		 *
		 * @return the mapped log level for the MLUC lib
		 */
		MLUCLog::TLogLevel mapLogLevel(Log_Levels type); 
	
	private:

		/**
		 * Standard Constructor for the AdditionalLoggerDummy
		 */
//		AdditionalLoggerDummy();


		/**
		 * Pointer to the choosen log component for this MLUC adapter
		 */
//		MLUCLogServer* mpLogComp;

		/**
		 * Container for storing all added MLUCLogServer
		 */
		std::vector<MLUCLogServer*> mLogServContainer;

		/**
		 * Defines the name of this additional logger module
		 */
		static const std::string cmLOGGER_NAME;


}; //end of class


} } } // end namespaces "alice", "hlt" and "proxy"

#endif // ALICE_HLT_PROXY_ADDITIONAL_LOGGER_MLUC_HPP

