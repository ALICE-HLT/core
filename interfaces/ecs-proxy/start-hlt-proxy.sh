#!/bin/bash

#/************************************************************************
#**
#**
#** This file is property of and copyright by the Department of Physics
#** Institute for Physic and Technology, University of Bergen,
#** Bergen, Norway, 2006
#** This file has been written by Sebastian Bablok,
#** sebastian.bablok@ift.uib.no
#**
#** Important: This file is provided without any warranty, including
#** fitness for any particular purpose.
#**
#**
#*************************************************************************/

# bash version of the start hlt proxy script

# set environment
#. .setup-all.sh

# set default domain name
#DomainName="ALICE_HLT"	# old version style
DomainName=""

#proxy_PID=0	# PID of the started proxy

# set the desired domain name, if provided
#if [ $1 ]; then		# old version style
#
#	DomainName="$1"
#	
#fi

function usage {
	echo "Usage:"
    echo "  $0 [--domain <domain_name> --dimDnsNode <node_name> --propertyFile <file_name> --logFile <file_name> --debug --help] [-h]"
	echo ""
	echo "    GENEREL REMARK: All parameters are optional and passed to 'hlt_SM_proxy', except"
	echo "    for '-h', which prints out this usage. The script has a default for the domain"
	echo "    ('ALICE_HLT'), which is set, if absolutely no parameter is provided. As soon"
	echo "    as one parameter is set, the '--domain <domain_name>' has to be set as well."
	echo ""
	echo "    --domain: gives the domain name for the Proxy. If no parameter is given"
	echo "            'ALICE_HLT' is assumed and added to the call of 'hlt_SM_proxy'"
	echo ""
	echo "    --dimDnsNode: defines the node, where the DIM DNS is runnning."
	echo "            (Can be also set as environmental variable 'DIM_DNS_NODE')"
	echo ""
	echo "    --propertyFile: defines the name of the Property File."
	echo "            (Can be also set as environmental variable 'HLT_PROXY_PROPERTY_FILE')"
	echo ""
	echo "    --logFile: defines the name of the log file."
	echo "            (Can be also set as environmental variable 'HLT_PROXY_LOGFILE_NAME')"
	echo ""
	echo "    --debug: if provided, more debug output is switched on."
	echo "            (This parameter must not have any value.)"
	echo ""
	echo "    --help: prints out the help of 'hlt_SM_proxy' (other parameter are ignored)."
	echo ""
	echo "    -h: prints out this usage. (all other parameter are ignored)"
	echo ""
	exit 1
}

## NOT used in the current mechanism -> works also with old version
# function to kill the proxy on received SIGINT
#function killProxy {
#	kill -a -s SIGINT $proxy_PID
#	exit 0
#}

if [ ! $1 ]; then
	DomainName="--domain ALICE_HLT"
fi

if [ $1 ] && [ $1 == "-h" ]; then
	usage
fi

## if only one paramter is provided and this is not domain name then set it to default
if [ $1 ] && [ $1 == "--debug" ] && [ ! $2 ]; then
	DomainName="--domain ALICE_HLT"
fi

echo ""
echo "   Parameters provided to hlt_SM_proxy: '$* $DomainName'."
echo "   Starting Proxy now ..."
echo ""


# start proxy
#echo "./bin/hlt_SM_proxy $* $DomainName"
./bin/hlt_SM_proxy $* $DomainName #&


## this mechanism is not necessary -> works also with old version
#proxy_PID=$?

#trap 'killProxy' INT 

## wait until SIGINT is received
#while true
#do
#    sleep 2
#done


