/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/
#include <iostream>
#include <unistd.h>

#include "HLTProxy.hpp"
#include "MasterTM.hpp"
#include "TMproxy.hpp"
#include "hlt_proxy_state_defines.h"
#include "ProxyLogger.hpp"
#include "TMNamesAdapter.hpp"

#ifdef __EMULATOR
#include "InterfaceLib.h"
#else
#include <InterfaceLib.h>
#endif

using namespace std;

using namespace alice::hlt::proxy;

MasterTM::MasterTM(string tmAddress, TMproxy* proxy) {
	int nRet = 0;
	char* state = 0;
	// init member vars
	mValid = false;
	mConnected = false;
	mTMState = HLT_PROXY_STATE_ERROR;
	mpTMproxy = 0;
	mpTMaddress_str = 0;
	mpTMaddress_bin = 0;
	mKillSlavesFlag = false;
	mQueryStateRetryCount = 0;
	string::size_type pos1;
	string::size_type pos2;

	if (proxy == 0) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ALARM, LOG_SOURCE_PROXY, 
				"MasterTaskManager object has NULL pointer to TaskManager proxy.");
		return;
	}

	// set values of member vars
	mpTMproxy = proxy;
	mpTMaddress_str = new char[tmAddress.length() + 1];
	strcpy(mpTMaddress_str, tmAddress.c_str());
	 
	// extract "hostname.domainname" from address (
	// assuming: protocoltype://hostname.domain:portnumber)
	pos1 = tmAddress.find_last_of('/');
	pos2 = tmAddress.find_last_of(':');
	if ((pos1 == string::npos) || (pos1 == string::npos) || ((pos1 + 1) >= pos2)) {
		cout << "[MasterTaskManager]: Error while extracting name of MasterTaskManager." << endl;	
		ProxyLogger::getLogger()->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"Error during extracting MasterTaskManager name from " +
				tmAddress);
		mTMName = "NameExtractError";
	} else {
		char* buf = new char[(pos2 - pos1)];
		buf[tmAddress.copy(buf, (pos2 - (pos1 + 1)), (pos1 + 1))] = 0;
		mTMName = buf;
		delete[] buf;
		ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY, 
				"Extracted MasterTaskManager name '" + mTMName + "' from " +
				tmAddress);
	}

	nRet = ConvertAddress(mpTMproxy->getPointerInterfaceLib_data(),
				mpTMaddress_str, &mpTMaddress_bin);
	if (nRet != 0) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
				"InterfaceLib is unable to convert " + tmAddress + 
				" to binary representation (" + ProxyLogger::errnotos(nRet) +
				").");
	} else {

		// call Connect for each MasterTM, NOTE: Disconnect should NOT be done
		// in Destructor, because object might be copied with Copy-Constructor
		// and the original destroyed afterwards without explicitly seen in 
		// source; implement disconnect when hosting object (TMproxy) 
		// explicitly deletes its MasterTM objects -> in Destructor of TMproxy
		nRet = ConnectToProcess(mpTMproxy->getPointerInterfaceLib_data(), 0, 0,
				mpTMaddress_bin);
		if (nRet != 0) {
			ProxyLogger::getLogger()->createLogMessage(MSG_WARNING, 
					LOG_SOURCE_PROXY, "MasterTaskManager '" + mTMName +
					"' is unable to connect to its corresponding process ("
					+ ProxyLogger::errnotos(nRet) + "); trying without ...");
		} else {
			mConnected = true;
			ProxyLogger::getLogger()->createLogMessage(MSG_INFO, 
					LOG_SOURCE_PROXY, "MasterTaskManager '" + mTMName +
					"' successfully connected to its corresponding process.");
		}

		nRet = QueryState(mpTMproxy->getPointerInterfaceLib_data(), 0, 0,
				mpTMaddress_bin, &state);

		if (nRet != 0) {
			ProxyLogger::getLogger()->createLogMessage(MSG_WARNING, 
					LOG_SOURCE_PROXY, "MasterTaskManager '" + mTMName +
					"' is unable to request its initial state via InterfaceLib ("
					+ ProxyLogger::errnotos(nRet) + ").");
			++mQueryStateRetryCount;
			// !!! decide which state in that case applies ???
			// !!! Do I have to release resource state anyway ???
			mValid = true; //even the queryState fails, set object to valid
			// var indicating lost contacts set to false
			mConnected = false;
		} else {
			mConnected = true;
			nRet = TMNamesAdapter::adaptTMStateName(state, mTMState);
			if (nRet < 0) {
				mTMState = HLT_PROXY_STATE_ERROR;
				ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, 
						LOG_SOURCE_PROXY, "MasterTaskManager '" + mTMName +
						"' has strange initial state name: " + 
						ProxyLogger::catos(state)  + ".");
			}

			ReleaseResource(mpTMproxy->getPointerInterfaceLib_data(), state);
				// this is correct version to free resource "state"
			mValid = true; // ??? What if not valid state name of MasterTM ???
		}
	}
}

MasterTM::MasterTM(const MasterTM &mtm) {
	int nRet = 0;
	// init member vars	
	this->mpTMaddress_str = 0;
	this->mpTMaddress_bin = 0;
	this->mTMState = HLT_PROXY_STATE_ERROR;
	this->mValid = false;
	this->mConnected = false;
	this->mKillSlavesFlag = false;
	this->mpTMproxy = 0;
	this->mQueryStateRetryCount = 0;

	if ((!mtm.mValid) || (mtm.mpTMproxy == 0)) {
		ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY, 
				"MasterTaskManager copy constructor received invalid original.");
		return;
	}

	// set values of member vars
	this->mpTMproxy = mtm.mpTMproxy;
	this->mKillSlavesFlag = mtm.mKillSlavesFlag;
	this->mpTMaddress_str = new char[strlen(mtm.mpTMaddress_str) + 1];
	strcpy(this->mpTMaddress_str, mtm.mpTMaddress_str);
	this->mTMName = mtm.mTMName;
	this->mQueryStateRetryCount = mtm.mQueryStateRetryCount;

	nRet = ConvertAddress(this->mpTMproxy->getPointerInterfaceLib_data(),
				this->mpTMaddress_str, &(this->mpTMaddress_bin));
	if (nRet != 0) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
				"InterfaceLib is unable to convert " + 
				ProxyLogger::catos(this->mpTMaddress_str) +
				" to binary representation (in copy-ctor) (" + 
				ProxyLogger::errnotos(nRet) + ").");
		this->mpTMproxy = 0;
		delete[] this->mpTMaddress_str;
		return;
	}

	this->mTMState = mtm.mTMState;
	this->mValid = true;
	this->mConnected = mtm.mConnected;
}


MasterTM MasterTM::operator=(const MasterTM& rhs) {
	if (this == &rhs) {
		return *this;
	}
	// clear former used memory of members
	if (this->mpTMaddress_str != 0) {
		delete[] mpTMaddress_str;
	}
	if ((this->mpTMaddress_bin != 0) && (this->mpTMproxy != 0)) {
		ReleaseResource(this->mpTMproxy->getPointerInterfaceLib_data(), 
				this->mpTMaddress_bin);
	} else {
		ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY, 
				"Assigning fresh data to invalid MasterTaskManager object.");
	}

	// init member vars
	int nRet = 0;
	this->mTMState = HLT_PROXY_STATE_ERROR;
	this->mValid = false;
	this->mConnected = false;
	this->mKillSlavesFlag = false;
	this->mQueryStateRetryCount = 0;
	this->mpTMproxy = 0;
	this->mpTMaddress_bin = 0; // its former content has been released
	this->mpTMaddress_str = 0; // its former content has been released

	if ((!rhs.mValid) || (rhs.mpTMproxy == 0)) {
		ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY, 
				"MasterTaskManager assignment operator received invalid original.");
		return *this;
	}

	// set values of member vars
	this->mpTMaddress_str = new char[strlen(rhs.mpTMaddress_str) + 1];
	strcpy(this->mpTMaddress_str, rhs.mpTMaddress_str);
	this->mTMName = rhs.mTMName;
	
	nRet = ConvertAddress(this->mpTMproxy->getPointerInterfaceLib_data(),
				this->mpTMaddress_str, &(this->mpTMaddress_bin));
	if (nRet != 0) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
				"InterfaceLib is unable to convert " + 
				ProxyLogger::catos(this->mpTMaddress_str) +
				" to binary representation (in =op) (" + 
				ProxyLogger::errnotos(nRet) + ").");
		delete[] this->mpTMaddress_str;
		this->mpTMaddress_str = 0;
		this->mpTMaddress_bin = 0;
		return *this;
	}

	this->mpTMproxy = rhs.mpTMproxy;
	this->mTMState = rhs.mTMState;
	this->mKillSlavesFlag = rhs.mKillSlavesFlag;
	this->mValid = true;
	this->mConnected = rhs.mConnected;
	this->mQueryStateRetryCount = rhs.mQueryStateRetryCount;
	
	return *this;
}

MasterTM::~MasterTM() {
	if ((mpTMaddress_bin != 0) && (mpTMproxy != 0)) {
		ReleaseResource(mpTMproxy->getPointerInterfaceLib_data(), mpTMaddress_bin);
	} else {
		ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY, 
				"Cleaning up invalid MasterTaskManager '" + mTMName + "'.");
	}
	if (mpTMaddress_str != 0) {
		delete[] mpTMaddress_str;
	}
}

int MasterTM::sendCommand(const char *command) {
	int nRet = 0;

	if (!mValid) {
		return MASTER_TM_NOT_VALID;
	}

	nRet = SendCommand(mpTMproxy->getPointerInterfaceLib_data(), 0, 0, 
			mpTMaddress_bin, command);
	if (nRet != 0) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
				"MasterTaskManager '" + mTMName + "' is unable to send command '"
				+ ProxyLogger::catos(command) + "' via InterfaceLib (" + 
				ProxyLogger::errnotos(nRet) + ").");
		cout << "[TM-proxy] MasterTM '" << mTMName << "' is unable to send command '"
				<< command << "' to corresponding MasterTaskManager (" <<
				ProxyLogger::errnotos(nRet) << ")." << endl;
	} else {
		ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY, 
				"MasterTM has sent '" + ProxyLogger::catos(command) + "' to " +
				ProxyLogger::catos(mpTMaddress_str) + ".");
		cout << "Relayed command '" << command <<"' to " << mpTMaddress_str <<
				"." << endl;
	}
	return nRet;
}

int MasterTM::queryState(string &tmState) {
	int nRet = 0;
	int adapterRetVal = -1;
	char* masterTMstate = 0;

	if (mValid) {
		nRet = QueryState(mpTMproxy->getPointerInterfaceLib_data(), 0, 0,
				mpTMaddress_bin, &masterTMstate);
		if (nRet != 0) {
			ProxyLogger::getLogger()->createLogMessage(MSG_WARNING, 
					LOG_SOURCE_PROXY, "MasterTaskManager '" + mTMName +
					"' is unable to query its current state via InterfaceLib ("
					+ ProxyLogger::errnotos(nRet) + ").");
			cout << "[TM-proxy] MasterTM '" << mTMName << 
					"' is unable to query state from corresponding MasterTaskManager (" 
					<< ProxyLogger::errnotos(nRet) << ")." << endl;
//			++mQueryStateRetryCount;  // shall query forever at the moment
			if (mQueryStateRetryCount >= HLT_TM_QUERY_STATE_RETRIES) {
				// unable to queryState too often -> set TM to invalid
				setInvalid();
			}
			if (masterTMstate != 0) {
				ReleaseResource(mpTMproxy->getPointerInterfaceLib_data(), masterTMstate); 
			}
			// set to false, because unable to request state
			mConnected = false;
			return MASTER_TM_INTERFACELIB_ERROR;
		}
		// set to true -> state request successful
		mConnected = true;
		mQueryStateRetryCount = 0;

		if (masterTMstate != 0) {
			if (HLT::isDebugOn()) {
				cout << "[MasterTaskManager]: Query of Master TM '" << mTMName << 
						"' returned state (unmapped): " << masterTMstate << endl;
			}
			if (strcmp(masterTMstate, "") == 0) {
				if (HLT::isDebugOn()) {
					cout << "[MasterTaskManager]: Master TM '" << mTMName <<
							"' returned empty state string, - ignoring request."
							<< endl;
				}
				nRet = MASTER_TM_EMPTY_STATE_STRING;
			} else {
				adapterRetVal = TMNamesAdapter::adaptTMStateName(masterTMstate,
						tmState);
				if (HLT::isDebugOn()) {
                	cout << "[MasterTaskManager]: Master TM '" << mTMName << 
							"' mapped states: " << mTMState << " (old), " << tmState
							<< " (new), " << adapterRetVal << " (mapping flag)." << endl;
            	}
				if (adapterRetVal < 0) {
					tmState = HLT_PROXY_STATE_ERROR;  //means state "ERROR"
					isNewState(tmState, 0);
//					setTMState("ERROR"); // ??? maybe set to error-equivalent ???
// use isNewState() instead of setTMState() -> new state is set automatically inside
					ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, 
							LOG_SOURCE_PROXY, "MasterTaskManager " + mTMName + 
							" has strange state name: " + 
							ProxyLogger::catos(masterTMstate) + ".");
					nRet = MASTER_TM_INVALID_STATE_NAME;
				} else {
					// !!! more check for retVal !!!
			
					// check now for state change
					if (isNewState(tmState, adapterRetVal)) {
						nRet = MASTER_TM_HAS_NEW_STATE;
					}
				}
			}
		} else {
			if (HLT::isDebugOn()) {
				cout << "[MasterTaskManager]: Master TM '" << mTMName <<
						"' returned empty state string, - ignoring request."
						<< endl;
			}
			nRet =  MASTER_TM_EMPTY_STATE_STRING;
// behaviour changed ...
//			tmState = HLT_PROXY_STATE_ERROR;  //means state "ERROR"
//			isNewState(tmState, 0);
//			setTMState("ERROR");  // ??? maybe set to error equivalent ???
// use isNewState() instead of setTMState() -> new state is set automatically inside
//			nRet = MASTER_TM_INVALID_STATE_NAME;
		}
		ReleaseResource(mpTMproxy->getPointerInterfaceLib_data(), masterTMstate); // release of pointer correct !

	} else {
		nRet = MASTER_TM_NOT_VALID;
	}
	return nRet;
}


bool MasterTM::isNewState(const string newState, int adapterRetVal) {
	bool bRet = false;

	if (mTMState.compare(newState) != 0) {
		// => MasterTaskManager has new state
		cout << "[TM-proxy] MasterTM '" << mTMName << "' has state: "
				<< newState << endl;
		ProxyLogger::getLogger()->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
				"MasterTaskManager '" + mTMName + "' changed state from " +
				mTMState + " to " + newState + ".");
		setTMState(newState);
		bRet = true;
		
		// for now only with one master TM -> no evaluation with other
		// this will change !!!
		if ((adapterRetVal == HLT_CHECK_KILL_SLAVES_FLAG) && (mKillSlavesFlag)) {
			int nRet = 0;
			int retryCount = 0;
			do {
				nRet = SendCommand(mpTMproxy->getPointerInterfaceLib_data(), 0, 0,
						mpTMaddress_bin, HLT_TASKMANAGER_COMMAND_SHUTDOWN);
				if (nRet != 0) {
					ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, 
							LOG_SOURCE_PROXY, "MasterTaskManager '" + mTMName +
							"is unable to send command '" + 
							ProxyLogger::catos(HLT_TASKMANAGER_COMMAND_SHUTDOWN)
							+ "' via InterfaceLib (" + ProxyLogger::errnotos(nRet)
							+ ").");
				}
				++retryCount;
				usleep(HLT_TM_SEND_COMMAND_RETRY_SLEEP);
			} while ((nRet != 0) && (retryCount < HLT_TM_SEND_COMMAND_RETRIES));
			if (nRet != 0) { 
				// unable to send command to MasterTM several times -> set TM to invalid
				setInvalid();
			} else {
				ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG,
						LOG_SOURCE_PROXY, "TaskManager-Proxy has sent '" + 
						ProxyLogger::catos(HLT_TASKMANAGER_COMMAND_SHUTDOWN)
						+ "' to '" + mTMName + "' to finalize shutdown procedure.");
				cout << "Issued '" << HLT_TASKMANAGER_COMMAND_SHUTDOWN << 
						"' to " << mTMName << " to finalize shutdown." << endl;
				unsetKillSlavesFlag();
				bRet = false;
			}
		}
	}
	return bRet;
}


bool MasterTM::setToErrorEquivalentState() {
	setTMState(HLT_PROXY_STATE_ERROR);

	/// !!! to be filled !!!
	return true;
}

void MasterTM::setInvalid() {
	mValid = false;
	ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
			"MasterTaskManager '" + mTMName + "' has been set to INVALID.");
	cout << "[TM-proxy] MasterTM '" << mTMName << "' has been set to invalid."
			<< endl;
}

