/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

///////////////////////////////////////////////////////////////////////////////
//                                  MAIN                                     //
///////////////////////////////////////////////////////////////////////////////

#include <signal.h>
#include <unistd.h>
#include <iostream>
#include <string>

#include "HLTProxy.hpp"

/** static var to indicate if process shall end */
static int isNotAbort = 1;

/** static var to store the domain name */
static char* domainName = 0;

/** static var to store flag, indicating if debug output shall be switched on.*/
static bool debug = false;

/** 
 * Signal handler function to catch interrupt signal
 *
 * @param sig the signal that has been caught
 */
void catchIt(int sig);

/**
 * Function to parse the given command line parameters.
 *
 * @param arguments pointer to the list of arguments
 * @param count the amount of parameters to parse
 *
 * @return <0 when parsing fails or "--help" is given as parameter
 */
int parseArgs(char** arguments, unsigned int count);

/**
 * Function to write the usage of the program to the stdout.
 *
 * @param processName the name of the current process
 */
void printUsage(char* processName);


int main(int argc, char** argv) {
	int nRet = 0;

	if (argc < 2) {
		printUsage(argv[0]);
		return -1;
	}

	if (argc > 1) {
		nRet = parseArgs((argv + 1), (argc - 1));
		if (nRet < 0) {
			printUsage(argv[0]);
			return nRet;
		}
	}

	if (domainName == 0) {
		std::cout << "Domain name is missing - mandatory parameter."
				<< std::endl;
		printUsage(argv[0]);
		return -1;
	}

	if (!getenv(PROXY_MODULE_NAME_ENV)) {
		std::cout << "The environment variable (" << PROXY_MODULE_NAME_ENV
				<< ") for setting the module name (HLT or HLT_BCK) is missing; aborting."
				<< std::endl;
		return -3;
	}

	signal(SIGINT, catchIt);
	alice::hlt::proxy::HLT hlt(domainName, debug);

	hlt.startProxy(); // ==	hlt.setOFF();
	while(isNotAbort) {
		pause();
	}

	std::cout << "... Exiting now!" << std::endl;
	if (domainName != 0) {
		delete[] domainName;
	}

	return 0;
}

void catchIt(int sig) {
	std::cout << std::endl << "Caught an Interrupt signal, preparing exit ..."
			<< std::endl << std::endl;
	isNotAbort = 0;
}

void printUsage(char* processName) {
	std::cout << std::endl << "USAGE:" << std::endl;
	std::cout << "  " << processName << 
			" --domain <domain name> [--dimDnsNode <node name> --propertyFile <file name>" 
			<< " --logFile <file name> --debug --help]"
			<< std::endl;
	std::cout << std::endl << 
			"\t--domain: necessary parameter, gives the domain name for the Proxy (mandatory)." 
			<< std::endl;
	std::cout << std::endl << 
			"\t--dimDnsNode: defines the node, where the DIM DNS is runnning." 
			<< std::endl << 
			"\t\t(Can be also set as environmental variable 'DIM_DNS_NODE')"
			<< std::endl;
	std::cout << std::endl << 
			"\t--propertyFile: defines the name of the Property File."
			<< std::endl << 
			"\t\t(Can be also set as environmental variable 'HLT_PROXY_PROPERTY_FILE')"
			<< std::endl;
	std::cout << std::endl << 
			"\t--logFile: defines the name of the log file."
			<< std::endl << 
			"\t\t(Can be also set as environmental variable 'HLT_PROXY_LOGFILE_NAME')"
			<< std::endl;
    std::cout << std::endl <<
            "\t--debug: optional parameter, if provided, then debug output is switched on in the Proxy."
            << std::endl;
	std::cout << std::endl << 
			"\t--help: prints out this help (all other parameter are ignored)." 
			<< std::endl << std::endl << std::endl;
}

int parseArgs(char** arguments, unsigned int count) {
	unsigned int i = 0;
	while (count > i) {
		// Domain name
		if (!strcmp(arguments[i], "--domain")) {
			if (count <= (i + 1)) {
				std::cout << "\t--domain misses argument defining the name of the domain (mandatory)."
						<< std::endl;
				return -1;
			}
			domainName = new char[strlen(arguments[i + 1]) + 1];
			strcpy(domainName, arguments[i + 1]);
			i += 2;
			continue;
		}
		
		// DNS node name
		if (!strcmp(arguments[i], "--dimDnsNode")) {
			if (count <= (i + 1)) {
				std::cout << "\t--dimDnsNode misses argument defining the dimDnsNode."
						<< std::endl;
				return -1;
			}
			setenv("DIM_DNS_NODE", arguments[i + 1], 1);
			i += 2;
			continue;
		}

		// property file name
		if (!strcmp(arguments[i], "--propertyFile")) {
			if (count <= (i + 1)) {
				std::cout << "\t--propertyFile misses argument defining name of the property file."
						<< std::endl;
				return -1;
			}
			setenv("HLT_PROXY_PROPERTY_FILE", arguments[i + 1], 1);
			i += 2;
			continue;
		}

		// property file name
		if (!strcmp(arguments[i], "--logFile")) {
			if (count <= (i + 1)) {
				std::cout << "\t--logFile misses argument defining name of the log file."
						<< std::endl;
				return -1;
			}
			setenv("HLT_PROXY_LOGFILE_NAME", arguments[i + 1], 1);
			i += 2;
			continue;
		}

        // debug output 
        if (!strcmp(arguments[i], "--debug")) {
            debug = true;
			i += 1;
            continue;
        }

		// help
		if (!strcmp(arguments[i], "--help")) {
			return -1;
		}
		// unknown parameter, exiting parse function and print usage
		std::cout << "\t received unknown parameter ... - exiting."
						<< std::endl;
		return -1;
	}

	return 0;
}

