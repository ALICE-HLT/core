/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2008
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include "AdditionalLoggerMLUC.hpp"


//#include "ProxyLogger.hpp"


//#include <iostream>


using namespace std;

using namespace alice::hlt::proxy;

const std::string AdditionalLoggerMLUC::cmLOGGER_NAME = "LoggerAdapterMLUC";

AdditionalLoggerMLUC::AdditionalLoggerMLUC(MLUCLogServer* logServer) :
			AdditionalLoggerBase(cmLOGGER_NAME) {
	gLogLevel = MLUCLog::kAll;
	mLogServContainer.push_back(logServer);
//	mpLogComp = logServer;

	gLog.AddServer(logServer);
}


AdditionalLoggerMLUC::AdditionalLoggerMLUC(const AdditionalLoggerMLUC& almRef) :
			AdditionalLoggerBase(almRef) {
	gLogLevel = MLUCLog::kAll;

    // TODO is this required
	for (unsigned int i = 0; i < almRef.mLogServContainer.size(); i++) {
		gLog.AddServer(almRef.mLogServContainer[i]);
		mLogServContainer.push_back(almRef.mLogServContainer[i]);
	}
//    vector<MLUCLogServer* >::iterator it = almRef.mLogServContainer.begin();
//    while (it != almRef.mLogServContainer.end()) {
//		gLog.AddServer((MLUCLogServer*) (*it));
//		mLogServContainer.push_back((MLUCLogServer*) (*it));
//		it++;
//    }
//	mpLogComp = almRef.mpLogComp;
//	gLog.AddServer(mpLogComp);

}

AdditionalLoggerMLUC& AdditionalLoggerMLUC::operator=(
			const AdditionalLoggerMLUC& rhs) {
    if (this == &rhs) {
        return *this;
    }
	AdditionalLoggerBase::operator=(rhs);

	gLogLevel = MLUCLog::kAll;

	// TODO is this required ???
    for (unsigned int i = 0; i < rhs.mLogServContainer.size(); i++) {
        gLog.AddServer(rhs.mLogServContainer[i]);
        mLogServContainer.push_back(rhs.mLogServContainer[i]);
    }
//	vector<MLUCLogServer* >::iterator it = rhs.mLogServContainer.begin();
//	while (it != rhs.mLogServContainer.end()) {
//		gLog.AddServer((MLUCLogServer*) (*it));
//		mLogServContainer.push_back((MLUCLogServer*) (*it));
//		it++;
//	}
//  mpLogComp = rhs.mpLogComp;
//	gLog.AddServer(mpLogComp);

    return *this;
}

AdditionalLoggerMLUC::~AdditionalLoggerMLUC() {
	vector<MLUCLogServer* >::iterator it = mLogServContainer.begin();
	while (it != mLogServContainer.end()) {
		gLog.DelServer((MLUCLogServer*) (*it));
		it++;
	}
	mLogServContainer.clear();
}

void AdditionalLoggerMLUC::addLogServer(MLUCLogServer* logServer) {
	gLog.AddServer(logServer);
	mLogServContainer.push_back(logServer);
}

bool AdditionalLoggerMLUC::relayLogMessage(const Timestamp& stamp, 
			Log_Levels type, string origin, string description) {
	bool bRet = false;
	string msg("[" + stamp.date + "] " + description);

	LOG(mapLogLevel(type), origin.c_str(), "ECS-Proxy") << 
			msg.c_str() << ENDLOG;
	bRet = true;
	
	return bRet;
}


MLUCLog::TLogLevel AdditionalLoggerMLUC::mapLogLevel(Log_Levels type) {
	MLUCLog::TLogLevel mlucLevel = MLUCLog::kNone;

	switch (type) {
		case (MSG_INFO):
			mlucLevel = MLUCLog::kInformational;
			break;
        case (MSG_WARNING):
            mlucLevel = MLUCLog::kWarning;
            break;
        case (MSG_ERROR):
            mlucLevel = MLUCLog::kError;
            break;
        case (MSG_DEBUG):
            mlucLevel = MLUCLog::kDebug;
            break;
        case (MSG_ALARM):
            mlucLevel = MLUCLog::kFatal;
            break;
		default:
			mlucLevel = MLUCLog::kNone;
	}

	return mlucLevel;
}


