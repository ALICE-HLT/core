/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2008
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include "Parameters.hpp"


using namespace alice::hlt::proxy;

Parameters::Parameters() {

}

Parameters::Parameters(const Parameters& parRef) {

}

Parameters& Parameters::operator=(const Parameters& rhs) {
	if (this == &rhs) {
		return *this;
	}
	
	return *this;
}

Parameters::~Parameters() {

}


