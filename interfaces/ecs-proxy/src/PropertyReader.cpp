/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

#include <cctype> // for toupper
#include <algorithm>

#include "PropertyReader.hpp"
#include "property_defines.h"
#include "ProxyLogger.hpp"

using namespace std;
using namespace alice::hlt::proxy;

typedef pair <string, string> String_Pair;

PropertyReader* PropertyReader::mpInstance = 0;

PropertyReader* PropertyReader::getPropertyReader() {
    if (mpInstance == 0) {
        mpInstance = new PropertyReader();
    }
    return mpInstance;
}

PropertyReader::PropertyReader() {
	mValid = false;
};

PropertyReader::~PropertyReader() {
	// leave destructor, if already destroyed
	if (mpInstance == 0) {
		return;
    }

	mProperties.clear();
	mpInstance = 0;
};


bool PropertyReader::readPropertiesFromFile(const char* filename) {
	ifstream pfile;
	unsigned int count = 0;

	// open property file
	if (filename == "") {
		cout << "Filename for properties empty" << endl;
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY,
				"PropertyReader received empty filename to read properties.");
		return false;
	}

	// clean up container for properties
	mProperties.clear();

	pfile.open(filename, ios_base::in);
	if (!pfile) {
		cout << "Error while opening property file or file does not exist." 
				<< endl;
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY,
				"PropertyReader is unable to open property file '" +
				ProxyLogger::catos(filename) + "'.");
		return false;
	}
	ProxyLogger::getLogger()->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY,
			"PropertyReader uses property file '" +	ProxyLogger::catos(filename)
			+ "'.");

	while ((!pfile.eof()) && (!pfile.bad())) {
		char line[MAX_LINE_LENGTH];
		char* ptrDelimiter;
		mValid = true;

		pfile.getline(line, MAX_LINE_LENGTH);

		// skip empty lines
		if (strlen(line) == 0) {
			continue;
		}
		// skip comment lines (beginning with '#') and lines with blank chars
		// at beginnig
		if ((line[0] == '#') || (line[0] == ' ')) {
			continue;
		}

		// cut off comments at end
		ptrDelimiter = strstr(line, "#");
		if (ptrDelimiter != 0) {
			*ptrDelimiter = '\0';
		}
		// cut off end of line
		ptrDelimiter = strstr(line, " ");
		if (ptrDelimiter != 0) {
			*ptrDelimiter = '\0';
		}

		ptrDelimiter = strstr(line, "=");
		if (ptrDelimiter != 0) {
			*ptrDelimiter = '\0';
			ptrDelimiter++;
//			if ((ptrDelimiter[0] != ' ') && (ptrDelimiter[0] != ' ')) {
			if ((ptrDelimiter[0] != ' ') && (ptrDelimiter[0] != '\0')) {
				if (ptrDelimiter[0] == '$') { // take value from env
					ptrDelimiter++; // step one char further to skip the '$' sign
					string name(line);
					char* tmpEnv = getenv(ptrDelimiter);
					if ((tmpEnv == 0) || (tmpEnv[0] == ' ')) { // env var is empty
						ProxyLogger::getLogger()->createLogMessage(MSG_WARNING,
								LOG_SOURCE_PROXY,
								"PropertyReader encountered empty property from environment var (" +
								ProxyLogger::catos(ptrDelimiter) + ") for '" + 
								ProxyLogger::catos(line) + "'.");
						cout << "Missing or empty property value for " << line << 
								" from env: '" << ptrDelimiter << "'." << endl;
//						mValid = false;   
// no error, since RunManager requires default parameters to be empty
						string value("");
						mProperties.insert(String_Pair(name, value));
						++count;																		
					} else {
						string value(tmpEnv);
						mProperties.insert(String_Pair(name, value));
						++count;
					}
				} else { // property directly given from property file
					string name(line);
					string value(ptrDelimiter);
					mProperties.insert(String_Pair(name, value));
					++count;
				}
			} else {
				ProxyLogger::getLogger()->createLogMessage(MSG_WARNING,
						LOG_SOURCE_PROXY, 
						"PropertyReader encountered empty property for '" +
						ProxyLogger::catos(line) + "'.");
				cout << "Missing or empty property value for " << line << endl;
//				mValid = false;
// no error, since RunManager requires default parameters to be empty
				string value("");
				string name(line);
				mProperties.insert(String_Pair(name, value));
				++count;
			}
		}
	}
	ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY,
			"PropertyReader has read " + ProxyLogger::itos(count) + 
			" properties from file '" +	ProxyLogger::catos(filename) + "'.");

	// maybe check also for .bad() for returning a false
	pfile.close();
	return mValid;
}

bool PropertyReader::retrievePropertyValue(const string* propertyName,
		string** propertyValue) {
	if (propertyName == 0) {
		ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY,
				"PropertyReader has been requested empty property.");
		propertyValue = 0;
		return false;
	}
	return retrievePropertyValue(propertyName->c_str(), propertyValue);
}

bool PropertyReader::retrievePropertyValue(const char* propertyName,
		string** propertyValue) {
	// check, if property file has been read
	if ((!mValid) || (propertyName == 0)) {	
		ProxyLogger::getLogger()->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY,
				"PropertyReader has been requested empty property or PropertyReader not valid.");
		propertyValue = 0;
		return false;
	}
	string tmp(propertyName);

// no need to solve it that way; -> map has key to search for the proper value
/*
	map<string, string>::iterator mapIt;
	for (mapIt = mProperties.begin(); mapIt != mProperties.end(); mapIt++) {

		if (strcmp(propertyName, mapIt.first) == 0) {
			// ... think about memory management
			*propertyValue = new char[strlen(mapIt.second) + 1];
			strcpy(*propertyValue, mapIt.second);

			return true;
		}
	}
*/

// ??? check if this works ??? (finding of desired string object)
	map<string, string>::iterator mapIt = mProperties.find(tmp);

	if (mapIt == mProperties.end()) {
		ProxyLogger::getLogger()->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY,
				"PropertyReader contains no property named '" + tmp + "'.");
		propertyValue = 0; // return a NULL pointer for propertyValue
		return false; // no value for given name found
	} else {
		*propertyValue = &(mapIt->second);
		return true;
	}
}


int PropertyReader::setHLTProxyProperties(HLTProxyProperties* pHPP) {
	int count = 0;
	map<string, string>::iterator mapIt;

	// check, if property file has been read
	if ((!mValid) || (pHPP == 0)) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY,
				"PropertyReader has received NULL pointer for HLTProxyProperties or PropertyReader not valid.");
		return -1; // property reader not valid or pHPP == 0 !!! find proper error code
	}

	// check if property file has the correct version number
	mapIt = mProperties.find(ALICE_HLT_PROXY_PROPERTY_VERSION_NUMBER);
	if (mapIt == mProperties.end()) {
        ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY,
                "PropertyReader has no property named '" +
                ProxyLogger::catos(ALICE_HLT_PROXY_PROPERTY_VERSION_NUMBER) +
                "' to fill HLTProxyProperties.");
        return -2; // no version number in property file -> ERROR
	}
	if (pHPP->mVersionNumber.compare(mapIt->second) != 0) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY,
				"Property-file has wrong version number; required is version '"
				+ pHPP->mVersionNumber + 
				"'. Use correct property-file and restart HLT-proxy.");
		return -3;
	}

//	// get number of TaskManager  
//	// -> removed from HLTProxyProperties since version 1.1.0, handled separately
//	mapIt = mProperties.find(ALICE_HLT_PROXY_NUMBER_OF_TM);
//	if (mapIt == mProperties.end()) {
//		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY,
//				"PropertyReader has no property named '" + 
//				ProxyLogger::catos(ALICE_HLT_PROXY_NUMBER_OF_TM) +
//				"' to fill HLTProxyProperties.");
//		return -1; // no value for given name found !!! find proper error code
//	} else {
//		sscanf(mapIt->second.c_str(), "%d", &(pHPP->mNumberOfTMs));
//		ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY,
//				"PropertyReader filled property '" + 
//				ProxyLogger::catos(ALICE_HLT_PROXY_NUMBER_OF_TM) +
//				"' with value: '" + ProxyLogger::itos(pHPP->mNumberOfTMs) + "'.");
//		count++;
//	}

	// get sleep time during polling TM states
    mapIt = mProperties.find(ALICE_HLT_PROXY_TM_POLLING_SLEEP);
    if (mapIt == mProperties.end()) {
        ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY,
                "PropertyReader has no property named '" +
                ProxyLogger::catos(ALICE_HLT_PROXY_TM_POLLING_SLEEP) +
                "' to fill HLTProxyProperties.");
        return -1; // no value for given name found !!! find proper error code
    } else {
        sscanf(mapIt->second.c_str(), "%ld", &(pHPP->mTMPollingSleep));
        ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY,
                "PropertyReader filled property '" +
                ProxyLogger::catos(ALICE_HLT_PROXY_TM_POLLING_SLEEP) +
                "' with value: '" + ProxyLogger::itos(pHPP->mTMPollingSleep) + "'.");
        count++;
    }

	// get detector list default
	mapIt = mProperties.find(ALICE_HLT_PROXY_DETECTOR_LIST_DEFAULT);
	if (mapIt == mProperties.end()) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY,
				"PropertyReader has no property named '" + 
				ProxyLogger::catos(ALICE_HLT_PROXY_DETECTOR_LIST_DEFAULT) + 
				"' to fill HLTProxyProperties.");
		return -1; // no value for given name found !!! find proper error code
	} else {
		pHPP->mDetectorListDefault = mapIt->second;
		ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY,
				"PropertyReader filled property '" + 
				ProxyLogger::catos(ALICE_HLT_PROXY_DETECTOR_LIST_DEFAULT) +
				"' with value: '" +	pHPP->mDetectorListDefault + "'.");
		count++;
	}

	// get HLT possible modes
	mapIt = mProperties.find(ALICE_HLT_PROXY_HLT_MODE_POSSIBLE_VALUES);
	if (mapIt == mProperties.end()) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY,
				"PropertyReader has no property named '" + 
				ProxyLogger::catos(ALICE_HLT_PROXY_HLT_MODE_POSSIBLE_VALUES) + 
				"' to fill HLTProxyProperties.");
		return -1; // no value for given name found !!! find proper error code
	} else {
		pHPP->mPossibleHLTMode = mapIt->second;
		transformToUpper(pHPP->mPossibleHLTMode);
		ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY,
				"PropertyReader filled property '" + 
				ProxyLogger::catos(ALICE_HLT_PROXY_HLT_MODE_POSSIBLE_VALUES) +
				"' with value: '" +	pHPP->mPossibleHLTMode + "'.");
		count++;
	}

	// get get beam type default
	mapIt = mProperties.find(ALICE_HLT_PROXY_BEAM_TYPE_DEFAULT);
	if (mapIt == mProperties.end()) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY,
				"PropertyReader has no property named '" + 
				ProxyLogger::catos(ALICE_HLT_PROXY_BEAM_TYPE_DEFAULT) + 
				"' to fill HLTProxyProperties.");
		return -1; // no value for given name found !!! find proper error code
	} else {
		pHPP->mBeamTypeDefault = mapIt->second;
		transformToUpper(pHPP->mBeamTypeDefault);
		ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY,
				"PropertyReader filled property '" + 
				ProxyLogger::catos(ALICE_HLT_PROXY_BEAM_TYPE_DEFAULT) +
				"' with value: '" +	pHPP->mBeamTypeDefault + "'.");
		count++;
	}

	// get data format version default
	mapIt = mProperties.find(ALICE_HLT_PROXY_DATA_FORMAT_VERSION_DEFAULT);
	if (mapIt == mProperties.end()) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY,
				"PropertyReader has no property named '" + 
				ProxyLogger::catos(ALICE_HLT_PROXY_DATA_FORMAT_VERSION_DEFAULT) 
				+ "' to fill HLTProxyProperties.");
		return -1; // no value for given name found !!! find proper error code
	} else {
		pHPP->mDataFormatVersionDefault = mapIt->second;
		transformToUpper(pHPP->mDataFormatVersionDefault);
		ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY,
				"PropertyReader filled property '" + 
				ProxyLogger::catos(ALICE_HLT_PROXY_DATA_FORMAT_VERSION_DEFAULT)
				+ "' with value: '" + pHPP->mDataFormatVersionDefault + "'.");
		count++;
	}

	// get HLT - trigger code default
	mapIt = mProperties.find(ALICE_HLT_PROXY_HLT_TRIGGER_CODE_DEFAULT);
	if (mapIt == mProperties.end()) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY,
				"PropertyReader has no property named '" + 
				ProxyLogger::catos(ALICE_HLT_PROXY_HLT_TRIGGER_CODE_DEFAULT) + 
				"' to fill HLTProxyProperties.");
		return -1; // no value for given name found !!! find proper error code
	} else {
		pHPP->mHLTTriggerCodeDefault = mapIt->second;
		transformToUpper(pHPP->mHLTTriggerCodeDefault);
		ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY,
				"PropertyReader filled property '" + 
				ProxyLogger::catos(ALICE_HLT_PROXY_HLT_TRIGGER_CODE_DEFAULT) +
				"' with value: '" + pHPP->mHLTTriggerCodeDefault + "'.");
		count++;
	}

	// get CTP - trigger class default
    mapIt = mProperties.find(ALICE_HLT_PROXY_CTP_TRIGGER_CLASS_DEFAULT);
    if (mapIt == mProperties.end()) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY,
				"PropertyReader has no property named '" + 
				ProxyLogger::catos(ALICE_HLT_PROXY_CTP_TRIGGER_CLASS_DEFAULT) + 
				"' to fill HLTProxyProperties.");
        return -1; // no value for given name found !!! find proper error code
    } else {
        pHPP->mCTPTriggerDefault = mapIt->second;
		transformToUpper(pHPP->mCTPTriggerDefault);
		ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY,
				"PropertyReader filled property '" + 
				ProxyLogger::catos(ALICE_HLT_PROXY_CTP_TRIGGER_CLASS_DEFAULT) +
				"' with value: '" +	pHPP->mCTPTriggerDefault + "'.");
        count++;
    }

	// get HLT IN DDL list default
	mapIt = mProperties.find(ALICE_HLT_PROXY_HLT_IN_DDL_LIST_DEFAULT);
	if (mapIt == mProperties.end()) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY,
				"PropertyReader has no property named '" + 
				ProxyLogger::catos(ALICE_HLT_PROXY_HLT_IN_DDL_LIST_DEFAULT) + 
				"' to fill HLTProxyProperties.");
		return -1; // no value for given name found !!! find proper error code
	} else {
		pHPP->mHLTInDDLListDefault = mapIt->second;
		transformToUpper(pHPP->mHLTInDDLListDefault);
		ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY,
				"PropertyReader filled property '" + 
				ProxyLogger::catos(ALICE_HLT_PROXY_HLT_IN_DDL_LIST_DEFAULT) +
				"' with value: '" +	pHPP->mHLTInDDLListDefault + "'.");
		count++;
	}

	// get HLT OUT DDL list default 
	mapIt = mProperties.find(ALICE_HLT_PROXY_HLT_OUT_DDL_LIST_DEFAULT);
	if (mapIt == mProperties.end()) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY,
				"PropertyReader has no property named '" + 
				ProxyLogger::catos(ALICE_HLT_PROXY_HLT_OUT_DDL_LIST_DEFAULT) + 
				"' to fill HLTProxyProperties.");
		return -1; // no value for given name found !!! find proper error code
	} else {
		pHPP->mHLTOutDDLListDefault = mapIt->second;
		transformToUpper(pHPP->mHLTOutDDLListDefault);
		ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY,
				"PropertyReader filled property '" + 
				ProxyLogger::catos(ALICE_HLT_PROXY_HLT_OUT_DDL_LIST_DEFAULT) +
				"' with value: '" +	pHPP->mHLTOutDDLListDefault + "'.");
		count++;
	}

    // get Run Type default
    mapIt = mProperties.find(ALICE_HLT_PROXY_RUN_TYPE_DEFAULT);
    if (mapIt == mProperties.end()) {
        ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY,
                "PropertyReader has no property named '" +
                ProxyLogger::catos(ALICE_HLT_PROXY_RUN_TYPE_DEFAULT) +
                "' to fill HLTProxyProperties.");
        return -1; // no value for given name found !!! find proper error code
    } else {
        pHPP->mRunTypeDefault = mapIt->second;
        transformToUpper(pHPP->mRunTypeDefault);
        ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY,
                "PropertyReader filled property '" +
                ProxyLogger::catos(ALICE_HLT_PROXY_RUN_TYPE_DEFAULT) +
                "' with value: '" + pHPP->mRunTypeDefault + "'.");
        count++;
    }

	ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY,
			"PropertyReader has filled HLTProxyProperties with '" +
			ProxyLogger::itos(count) + "' values.");
	return count;
}


bool PropertyReader::transformToUpper(string &s) {
    transform(s.begin(), s.end(), s.begin(), (int(*)(int)) toupper);
    return true;
/*
    if (!(transform(s.begin(), s.end(), s.begin(), (int(*)(int)) toupper) == s.begin + (s.end() - s.end()))) {
        //ERROR ...
    }
*/

}


//////// Helper function during development -> no logging needed !!  //////////

bool PropertyReader::writePropertiesToFile(const char* filename) {
	ofstream pfile;

	// check, if property file has been read
	if (!mValid) {
		return false;
	}

	// open property file
	if (filename == "") {
		cout << "Filename for properties empty" << endl;
		return false;
	}
	pfile.open(filename, ios_base::out);
	if (!pfile) {
		cout << "Unable to open new property file." << endl;
		return false;
	}

	map<std::string, std::string>::iterator it;
	for (it = mProperties.begin(); it != mProperties.end(); ++it) {
		//pfile << (it->first).c_str() << "=" << (it->second).c_str() << endl;
		pfile << it->first << "=" << it->second << endl;
	}

	pfile.close();
	return true;
}


