/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include "TMNamesAdapter.hpp"
#include "hlt_proxy_state_defines.h"
#include "error_codes.h"
#include "HLTProxy.hpp"

#include <iostream>


using namespace std;

using namespace alice::hlt::proxy;

int TMNamesAdapter::adaptHLTProxyCommand(const string proxyCommand,
		string& tmCommand, string tmState) {
	int retVal = 0;

	if (proxyCommand.compare(HLT_PROXY_COMMAND_INITIALIZE) == 0) {
		tmCommand = HLT_TASKMANAGER_COMMAND_INITIALIZE;
		// is here additional action required ???

	} else if (proxyCommand.compare(HLT_PROXY_COMMAND_CONFIGURE) == 0) {
		tmCommand = HLT_TASKMANAGER_COMMAND_CONFIGURE;
		// is here additional action required ???

	} else if (proxyCommand.compare(HLT_PROXY_COMMAND_ENGAGE) == 0) {
		tmCommand = HLT_TASKMANAGER_COMMAND_ENGAGE;
		// is here additional action required ???

	} else if (proxyCommand.compare(HLT_PROXY_COMMAND_START) == 0) {
		tmCommand = HLT_TASKMANAGER_COMMAND_START;
		// is here additional action required ???

	} else if (proxyCommand.compare(HLT_PROXY_COMMAND_STOP) == 0) {
		tmCommand = HLT_TASKMANAGER_COMMAND_STOP;
		// is here additional action required ???

	} else if (proxyCommand.compare(HLT_PROXY_COMMAND_DISENGAGE) == 0) {
		tmCommand = HLT_TASKMANAGER_COMMAND_DISENGAGE;
		// is here additional action required ???

	} else if (proxyCommand.compare(HLT_PROXY_COMMAND_RESET) == 0) {
		if (tmState.compare(HLT_PROXY_STATE_CONFIGURED) == 0) {
			tmCommand = HLT_TASKMANAGER_COMMAND_RESET;
		} else if (tmState.compare(HLT_PROXY_STATE_ERROR) == 0) {
			tmCommand = HLT_TASKMANAGER_COMMAND_RECOVER;
			// no flag has to be set
		} else {
			// ??? command does not fit to current state, what next ???
		}
		// is here additional action required ???

	} else if (proxyCommand.compare(HLT_PROXY_COMMAND_SHUTDOWN) == 0) {
		if (tmState.compare(HLT_PROXY_STATE_INITIALIZED) == 0) {
			tmCommand = HLT_TASKMANAGER_COMMAND_SHUTDOWN;
		} else if (tmState.compare(HLT_PROXY_STATE_ERROR) == 0) {
			tmCommand = HLT_TASKMANAGER_COMMAND_RECOVER;
			retVal = HLT_SET_KILL_SLAVES_FLAG;
		} else {
			// ??? command does not fit to current state, what next ???
		}
		// is here additional action required ???

	// command for setting active / passive mode
	} else if (proxyCommand.compare(HLT_PROXY_COMMAND_ACTIVE) == 0) {
		tmCommand = HLT_TASKMANAGER_COMMAND_ACTIVE;

	}else if (proxyCommand.compare(HLT_PROXY_COMMAND_PASSIVE) == 0) {
		tmCommand = HLT_TASKMANAGER_COMMAND_PASSIVE;

	} else {
		tmCommand = "";
		retVal = HLT_UNKNOWN_HLT_PROXY_COMMAND; 
		if (HLT::isDebugOn()) {
			cout << "[TM-Names Mapper]: Received unknown command '" << 
					proxyCommand << "' for mapping." << endl;
		}
	}

	return retVal;
}

int TMNamesAdapter::adaptHLTProxyCommand(const char* const proxyCommand,
		string& tmCommand, string tmState) {
	if (proxyCommand == 0) {
		tmCommand = "";
		return ALICE_HLT_PROXY_NULL_POINTER;
	}
	string stringWrapper(proxyCommand);
	return TMNamesAdapter::adaptHLTProxyCommand(stringWrapper, tmCommand, tmState);
}


int TMNamesAdapter::adaptTMStateName(const string tmStateName,
		string& proxyStateName) {
	int retVal = 0;

	if (tmStateName.compare(HLT_TASKMANAGER_STATE_OFF) == 0) {
		proxyStateName = HLT_PROXY_STATE_OFF;
		// is here additional action required ???

	} else if (tmStateName.compare(HLT_TASKMANAGER_STATE_INITIALIZED) == 0) {
		proxyStateName = HLT_PROXY_STATE_INITIALIZED;
		retVal = HLT_CHECK_KILL_SLAVES_FLAG;
		// is here additional action required ???

	} else if (tmStateName.compare(HLT_TASKMANAGER_STATE_CONFIGURED) == 0) {
		proxyStateName = HLT_PROXY_STATE_CONFIGURED;
		// is here additional action required ???

	} else if (tmStateName.compare(HLT_TASKMANAGER_STATE_READY) == 0) {
		proxyStateName = HLT_PROXY_STATE_READY;
		// is here additional action required ???

	} else if (tmStateName.compare(HLT_TASKMANAGER_STATE_RUNNING) == 0) {
		proxyStateName = HLT_PROXY_STATE_RUNNING;
		// is here additional action required ???

	} else if (tmStateName.compare(HLT_TASKMANAGER_STATE_BUSY) == 0) {
		proxyStateName = HLT_PROXY_STATE_RUNNING;	
		// is here additional action required ???

    } else if (tmStateName.compare(HLT_TASKMANAGER_STATE_PAUSED) == 0) {
        proxyStateName = HLT_PROXY_STATE_RUNNING;
        // is here additional action required ???

	} else if (tmStateName.compare(HLT_TASKMANAGER_STATE_ERROR) == 0) {
		proxyStateName = HLT_PROXY_STATE_ERROR;
		// is here additional action required ???

// Following come all transition (intermediated) states of TaskManager  

    } else if (tmStateName.compare(HLT_TASKMANAGER_STATE_INITIALIZING) == 0) {
        proxyStateName = HLT_PROXY_STATE_INITIALIZING;
        // is here additional action required ???

	} else if (tmStateName.compare(HLT_TASKMANAGER_STATE_KILLING_PROC) == 0) {
		proxyStateName = HLT_PROXY_STATE_INITIALIZING; 
		// coming from ERROR going to INITIALIZING
		// is here additional action required ???

    } else if (tmStateName.compare(HLT_TASKMANAGER_STATE_CONFIGURING) == 0) {
        proxyStateName = HLT_PROXY_STATE_CONFIGURING;
        // is here additional action required ???

    } else if (tmStateName.compare(HLT_TASKMANAGER_STATE_ENGAGING) == 0) {
        proxyStateName = HLT_PROXY_STATE_ENGAGING;
        // is here additional action required ???

    } else if (tmStateName.compare(HLT_TASKMANAGER_STATE_COMPLETING) == 0) {
        proxyStateName = HLT_PROXY_STATE_COMPLETING;
        // is here additional action required ???

    } else if (tmStateName.compare(HLT_TASKMANAGER_STATE_DISENGAGING) == 0) {
        proxyStateName = HLT_PROXY_STATE_DISENGAGING;
        // is here additional action required ???

    } else if (tmStateName.compare(HLT_TASKMANAGER_STATE_DEINITIALIZING) == 0) {
        proxyStateName = HLT_PROXY_STATE_DEINITIALIZING;
        // is here additional action required ???

    } else if (tmStateName.compare(HLT_TASKMANAGER_STATE_DECONFIGURING) == 0) {
		// this state is no longer mapped to INITIALIZING 
		// now it has its own state 
        proxyStateName = HLT_PROXY_STATE_DECONFIGURING;
        // is here additional action required ???

	} else if (tmStateName.compare(HLT_TASKMANAGER_STATE_START_RUNNING) == 0) {
        // since ver 1.3.1 this state also exists as intermediate state in 
		// the proxy
        proxyStateName = HLT_PROXY_STATE_STARTING;
        // is here additional action required ???

	} else {
		proxyStateName = "";
		retVal = HLT_UNKNOWN_TM_STATE; 
		if (HLT::isDebugOn()) {
            cout << "[TM-Names Mapper]: Received unknown state name '" <<
                    tmStateName << "' for mapping." << endl;
        }
	}
	return retVal;
}

int TMNamesAdapter::adaptTMStateName(const char* const tmStateName,
		string& proxyStateName) {
	if (tmStateName == 0) {
		proxyStateName = HLT_PROXY_STATE_ERROR;
		return ALICE_HLT_PROXY_NULL_POINTER;
	}
	string stringWrapper(tmStateName);
	return TMNamesAdapter::adaptTMStateName(stringWrapper, proxyStateName);
}

