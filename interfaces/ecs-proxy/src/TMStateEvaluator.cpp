/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/



#include "TMStateEvaluator.hpp"

using namespace std;
using namespace alice::hlt::proxy;

TMStateEvaluator::TMStateEvaluator() {

}



TMStateEvaluator::~TMStateEvaluator() {
	mRules.clear();
}
