/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include "ConfigureParameters.hpp"
#include "ProxyLogger.hpp"

#include <iostream>


using namespace std;
using namespace alice::hlt::proxy;

ConfigureParameters::ConfigureParameters(char* detectors, char* type, 
			char* format, char* triggerClasses, char* inList, char* outList,
			char* runType) {
	// !!! Check for NULL pointer here first !!!
	mDetectorList = detectors;
	mBeamType = type;
	mDataFormatVersion = format;
	mTriggerClasses = triggerClasses;
	mHLTInDDLList = inList;
	mHLTOutDDLList = outList;
	mRunType = runType;

	mIsChecked = false;

}

ConfigureParameters::ConfigureParameters(const ConfigureParameters& cpRef) {
	mDetectorList = cpRef.mDetectorList;
	mBeamType = cpRef.mBeamType;
	mDataFormatVersion = cpRef.mDataFormatVersion;
	mTriggerClasses = cpRef.mTriggerClasses;
	mHLTInDDLList = cpRef.mHLTInDDLList;
	mHLTOutDDLList = cpRef.mHLTOutDDLList;
	mRunType = cpRef.mRunType;

	mIsChecked = cpRef.mIsChecked;
}

ConfigureParameters& ConfigureParameters::operator=(const ConfigureParameters& rhs) {
	if (this == &rhs) {
		return *this;
	}

	mDetectorList = rhs.mDetectorList;
	mBeamType = rhs.mBeamType;
	mDataFormatVersion = rhs.mDataFormatVersion;
	mTriggerClasses = rhs.mTriggerClasses;
	mHLTInDDLList = rhs.mHLTInDDLList;
	mHLTOutDDLList = rhs.mHLTOutDDLList;
	mRunType = rhs.mRunType;

	mIsChecked = rhs.mIsChecked;

	return *this;
}

ConfigureParameters::~ConfigureParameters() {

}

int ConfigureParameters::replaceDefaultValues(const HLTProxyProperties& hpp) {
    int count = 0;

    if (mDetectorList == "DEFAULT") {
        mDetectorList = hpp.mDetectorListDefault;
        count++;
    }

    if (mBeamType == "DEFAULT") {
        mBeamType = hpp.mBeamTypeDefault;
        count++;
    }

    if (mDataFormatVersion == "DEFAULT") {
        mDataFormatVersion = hpp.mDataFormatVersionDefault;
        count++;
    }

    if (mTriggerClasses == "DEFAULT") {
        mTriggerClasses = hpp.mHLTTriggerCodeDefault;
        count++;
    }

    if (mHLTInDDLList == "DEFAULT") {
        mHLTInDDLList = hpp.mHLTInDDLListDefault;
        count++;
    }

    if (mHLTOutDDLList == "DEFAULT") {
        mHLTOutDDLList = hpp.mHLTOutDDLListDefault;
        count++;
    }

    if (mRunType == "DEFAULT") {
        mRunType = hpp.mRunTypeDefault;
        count++;
    }

    mIsChecked = true;
    ProxyLogger::getLogger()->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY,
            "Number of CONFIGURE parameters replaced by their default: " +
            ProxyLogger::itos(count) + " .");
    return count;

}

int ConfigureParameters::replaceDefaultValues(string defaultDetectors,
		string defaultType,	string defaultFormatVersion, 
		string defaultTriggerClasses, string defaultInDDLList, 
		string defaultOutDDLList, string defaultRunType) {
	int count = 0;

	if (mDetectorList == "DEFAULT") {
		mDetectorList = defaultDetectors;
		count++;
	}

	if (mBeamType == "DEFAULT") {
		mBeamType = defaultType;
		count++;
	}

	if (mDataFormatVersion == "DEFAULT") {
		mDataFormatVersion = defaultFormatVersion;
		count++;
	}

	if (mTriggerClasses == "DEFAULT") {
		mTriggerClasses = defaultTriggerClasses;
		count++;
	}

	if (mHLTInDDLList == "DEFAULT") {
		mHLTInDDLList = defaultInDDLList;
		count++;
	}

	if (mHLTOutDDLList == "DEFAULT") {
		mHLTOutDDLList = defaultOutDDLList;
		count++;
	}

    if (mRunType == "DEFAULT") {
        mRunType = defaultRunType;
        count++;
    }

	mIsChecked = true;
	ProxyLogger::getLogger()->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"Number of CONFIGURE parameters replaced by their default: " +
			ProxyLogger::itos(count) + " .");
	return count;
}

string ConfigureParameters::parToString(char separator) {
	string parString(ALICE_HLT_PROXY_DETECTOR_LIST + mDetectorList + separator +
			ALICE_HLT_PROXY_BEAM_TYPE + mBeamType + separator +
			ALICE_HLT_PROXY_DATA_FORMAT_VERSION + mDataFormatVersion + separator +
			ALICE_HLT_PROXY_HLT_TRIGGER_CODE + mTriggerClasses + separator + 
			ALICE_HLT_PROXY_HLT_IN_DDL_LIST + mHLTInDDLList + separator +
			ALICE_HLT_PROXY_HLT_OUT_DDL_LIST + mHLTOutDDLList + separator +
			ALICE_HLT_PROXY_RUN_TYPE + mRunType);
	return parString;
}


void ConfigureParameters::printParameters() {
	cout << "[CONFIGURE-Parameters]: " << endl;
	cout << "    Detector-List:\t" << mDetectorList << endl;
	cout << "    BEAM-TYPE:\t\t" << mBeamType << endl;
	cout << "    FORMAT-VERSION:\t" << mDataFormatVersion << endl;
	cout << "    HLT-TRIGGER-CLASS:\t" << mTriggerClasses << endl;
	cout << "    IN-DDL-LIST:\t" << mHLTInDDLList << endl;
	cout << "    OUT-DDL-LIST:\t" << mHLTOutDDLList << endl;
	cout << "    RUN-TYPE:\t\t" << mRunType << endl;
	cout << endl;
}

