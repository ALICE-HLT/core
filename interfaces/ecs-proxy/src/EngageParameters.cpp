/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2008
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include "EngageParameters.hpp"
#include "ProxyLogger.hpp"

#include <iostream>


using namespace std;
using namespace alice::hlt::proxy;

EngageParameters::EngageParameters(char* mode, unsigned int runNumber, 
			char* ctpTrigger) {
	// !!! Check for NULL pointer here first !!!
	mHLTMode = mode;
	mRunNumber = runNumber;
	mCTPTrigger = ctpTrigger;
	
	mIsChecked = false;
}

EngageParameters::EngageParameters(const EngageParameters& epRef) {
	mHLTMode = epRef.mHLTMode;
	mRunNumber = epRef.mRunNumber;
	mCTPTrigger = epRef.mCTPTrigger;

	mIsChecked = epRef.mIsChecked;
}

EngageParameters& EngageParameters::operator=(const EngageParameters& rhs) {
	if (this == &rhs) {
		return *this;
	}

	mHLTMode = rhs.mHLTMode;
	mRunNumber = rhs.mRunNumber;
	mCTPTrigger = rhs.mCTPTrigger;

	mIsChecked = rhs.mIsChecked;

	return *this;
}

EngageParameters::~EngageParameters() {

}


int EngageParameters::replaceDefaultValues(const HLTProxyProperties& hpp) {
	int count = 0;

	if (mCTPTrigger == "DEFAULT") {
        mCTPTrigger = hpp.mCTPTriggerDefault;
        count++;
    }

	mIsChecked = true;
    ProxyLogger::getLogger()->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY,
            "Number of ENGAGE parameters replaced by their default: " +
            ProxyLogger::itos(count) + " .");
    return count;
}


string EngageParameters::parToString(char separator) {
	string parString(ALICE_HLT_PROXY_HLT_MODE + mHLTMode + separator +
			ALICE_HLT_PROXY_RUN_NUMBER + ProxyLogger::itos(mRunNumber) + separator +
			ALICE_HLT_PROXY_CTP_TRIGGER_CLASS + mCTPTrigger);
	return parString;
}


void EngageParameters::printParameters() {
	cout << "[ENGANGE-Parameters]: " << endl;
	cout << "    HLT-MODE:\t\t" << mHLTMode << endl;
	cout << "    RUN-NUMBER:\t\t" << mRunNumber << endl;
	cout << "    CTP-TRIGGER:\t" << mCTPTrigger << endl;
	cout << endl;
}

