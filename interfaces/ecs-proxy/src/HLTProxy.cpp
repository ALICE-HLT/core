/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <iostream>

#include "HLTProxy.hpp"
#include "TMproxy.hpp"
#include "ConfigureParameters.hpp"
#include "EngageParameters.hpp"
#include "HLTProxyProperties.hpp"
#include "hlt_proxy_state_defines.h"

#ifdef __EMULATOR
#include "AdditionalLoggerDummy.hpp"
#else
#include "AdditionalLoggerMLUC.hpp"
#include "MLUCSysMESLogServer.hpp"
#include "MLUCDynamicLibraryLogServer.hpp"
#endif


using namespace std;

using namespace alice::hlt::proxy;

bool HLT::msDebugOutput = false;


HLT::~HLT() {
	if (mpTaskManagerProxy != 0) {
		delete mpTaskManagerProxy;
	}
	if (mpLogger != 0) {
		delete mpLogger;
	}

#ifndef __EMULATOR
	if (mpMLUCLogger != 0) {
		delete mpMLUCLogger;
	}
	if (mpInfoLogger != 0) {
		delete mpInfoLogger;
	}
#endif

}

void HLT::initTMproxy() {
	char* logname = 0;
	mpLogger = 0;
	mpTaskManagerProxy = 0;

	// create logger
	logname = getenv("HLT_PROXY_LOGFILE_NAME");
	if (logname == 0) {
		mpLogger = ProxyLogger::createLogger("HLT-Proxy.log");
	} else {		
		mpLogger = ProxyLogger::createLogger(logname);
	}

#ifdef __EMULATOR
	AdditionalLoggerDummy* addLogger = new 
			AdditionalLoggerDummy("DummyLogfile-HLT-Proxy.log");
#else
	mpMLUCLogger = new MLUCSysMESLogServer();
//	mpMLUCLogger = new MLUCStdoutLogServer();
    AdditionalLoggerMLUC* addLogger = new
			AdditionalLoggerMLUC(mpMLUCLogger);

	// info logger connection
	mpInfoLogger = 0;
	char* envLibPath = getenv("ALIHLT_DC_LIBDIR");
	if (envLibPath != 0) {
		string libPath(envLibPath);
		string facility(LOG_SOURCE_PROXY);
		libPath += "/libInfoLoggerLogServer.so";	// HLT team said: shall be hard coded
		facility += "_";
		facility += mDomain;
		mpInfoLogger = new MLUCDynamicLibraryLogServer(libPath.c_str(), facility.c_str());
		addLogger->addLogServer(mpInfoLogger);
	} else {
		cout << "ERROR: Environment Var 'ALIHLT_DC_LIBDIR' is empty, running without connection to infoLogger..." 
				<< endl;
    mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY,
            "Env Var 'ALIHLT_DC_LIBDIR' is empty, no connection to infoLogger.");
	}
#endif
	mpLogger->registerAdditionalLogger(addLogger);

	cout << endl << "   +++ Starting HLT - proxy version: " <<
			ALICE_HLT_PROXY_VERSION_NUMBER << " +++ " << endl << endl;
	mpTaskManagerProxy = new TMproxy(this);
	// finished initialization
	cout << "Initialized HLT - TM proxy ..." << endl; 
	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"Initialization of HLT-Proxy finished.");
}


void HLT::INITIALIZE() {
	if (strcmp(getState(), HLT_PROXY_STATE_OFF) != 0) {
		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"HLT-Proxy received 'INITIALIZE' command in state '" + 
				ProxyLogger::catos(getState()) + 
				"'. Not accepted in this state - ignoring command.");
		resetCurrentState();
		return;
	}
	// due to a more time consumer procedure state is now set to intermediate
	// state "INITIALIZING"
	setINITIALIZING();

	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"Proxy received 'INITIALIZE' command from ECS.");
	cout << "[HLT - proxy]: HLT proxy received INITIALIZE command." << endl;
	cout << "[HLT - proxy]: Ramping up and initializing the HLT cluster now." 
			<< endl << endl;

	if (mpTaskManagerProxy->sendTransitionCommand("INITIALIZE") != 0) {
		// something went wrong --> do appropriate stuff
		setERROR();
	}
}


void HLT::CONFIGURE() {
	char* detectorList = 0;
//	char* hltMode = 0;			// not a CONFIGURE Parameter since v 1.1
	char* collisionType = 0;
//	char* runNumber_c = 0; 		// not a CONFIGURE Parameter since v 1.1
//	int runNumber_i = 0;		// not a CONFIGURE Parameter since v 1.1
	char* outputFormat = 0;
	char* triggerClasses = 0;
//	char* ctpTrigger = 0;
	char* listOfInDDLs = 0;
	char* listOfOutDDLs = 0;
	char* runType = 0;			// new CONFIGURE parameter since version 1.1.0
//	string posHLTModes;			// not a CONFIGURE Parameter since v 1.1
	int nRet = 0;

	if (strcmp(getState(), HLT_PROXY_STATE_INITIALIZED) != 0) {
		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"HLT-Proxy received 'CONFIGURE' command in state '" + 
				ProxyLogger::catos(getState()) + 
				"'. Not accepted in this state - ignoring command.");
		resetCurrentState();
		return;
	}

	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"Proxy received 'CONFIGURE' command from ECS.");
	cout << "[HLT - proxy]: HLT proxy received CONFIGURE command." << endl << endl;

	// get parameters
	detectorList = getParDETECTOR_LIST();
//	hltMode = getParHLT_MODE();
	collisionType = getParBEAM_TYPE();
//	runNumber_c = getParRUN_NUMBER();
	outputFormat = getParDATA_FORMAT_VERSION();
	triggerClasses = getParHLT_TRIGGER_CODE();
//	ctpTrigger = getParCTP_TRIGGER_CLASS();
	listOfInDDLs = getParHLT_IN_DDL_LIST();
	listOfOutDDLs = getParHLT_OUT_DDL_LIST();
	runType = getParRUN_TYPE();

//	runNumber_i = atoi(runNumber_c);

	if (msDebugOutput) {
		cout << endl << "[DEBUG]: "  << endl;
		cout << " [DEBUG] Detector-List: " << detectorList << endl;
//		cout << " [DEBUG] HLT-Mode: " << hltMode << endl;
		cout << " [DEBUG] BeamType: " << collisionType << endl;
//		cout << " [DEBUG] Run-Number: " << runNumber_c << endl;
		cout << " [DEBUG] DataFormat: " << outputFormat << endl;
		cout << " [DEBUG] HLT-TriggerClasses: " << triggerClasses << endl;
//		cout << " [DEBUG] CTP-TriggerClasses: " << ctpTrigger << endl;
		cout << " [DEBUG] ListInDDL: " << listOfInDDLs << endl;
		cout << " [DEBUG] ListOutDDL: " << listOfOutDDLs << endl;
		cout << " [DEBUG] RunType: " << runType << endl << endl;
	}

	// check for NULL pointer, convert to upper cases
	if (!transformToUpperCase(detectorList)) {
		// make log entry about error during transforimg to Upper !!!
		cout << "ERROR occured while transforming string to upper cases, ignoring command!" 
				<< endl;
		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"Unable to handle CONFIGURE parameter '" + 
				ProxyLogger::catos(detectorList) + "' - Command ignored.");
//		setERROR();  // ### Don't go to ERROR state, if par is wrong ... 
		// just ignore command and make a log entry (has to be sent to ECS as well !!!)
		resetCurrentState();
		return;
	}

//	if (!transformToUpperCase(hltMode)) {
//		// make log entry about error during transforimg to Upper !!!
//		cout << "ERROR occured while transforming string to upper cases!" << endl;
//		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
//				"Unable to handle CONFIGURE parameter '" + 
//				ProxyLogger::catos(hltMode) + "'.");
//		setERROR();  // ### Don't go to ERROR state, if par is wrong ... 
//		// just ignore command and make a log entry (has to be sent to ECS as well !!!)
//		resetCurrentState();
//		return;
//	}

	if (!transformToUpperCase(collisionType)) {
		// make log entry about error during transforimg to Upper !!!
		cout << "ERROR occured while transforming string to upper cases, ignoring command!" 
				<< endl;
		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"Unable to handle CONFIGURE parameter '" +
				ProxyLogger::catos(collisionType) + "' - Command ignored.");
//		setERROR();  // ### Don't go to ERROR state, if par is wrong ... 
		// just ignore command and make a log entry (has to be sent to ECS as well !!!)
		resetCurrentState();
		return;
	}
	if (!transformToUpperCase(outputFormat)) {
		// make log entry about error during transforimg to Upper !!!
		cout << "ERROR occured while transforming string to upper cases, ignoring command!" 
				<< endl;
		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"Unable to handle CONFIGURE parameter '" +
				ProxyLogger::catos(outputFormat) + "' - Command ignored.");
//		setERROR();  // ### Don't go to ERROR state, if par is wrong ... 
		// just ignore command and make a log entry (has to be sent to ECS as well !!!)
		resetCurrentState();
		return;
	}
	if (!transformToUpperCase(triggerClasses)) {
		// make log entry about error during transforimg to Upper !!!
		cout << "ERROR occured while transforming string to upper cases, ignoring command!" 
				<< endl;
		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"Unable to handle CONFIGURE parameter '" + 
				ProxyLogger::catos(triggerClasses) + "' - Command ignored.");
//		setERROR();  // ### Don't go to ERROR state, if par is wrong ... 
		// just ignore command and make a log entry (has to be sent to ECS as well !!!)
		resetCurrentState();
		return;
	}

//	if (!transformToUpperCase(ctpTrigger)) {
//      // make log entry about error during transforimg to Upper !!!
//      cout << "ERROR occured while transforming string to upper cases, ignoring command!" 
//				<< endl;
//		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
//				"Unable to handle CONFIGURE parameter '" +
//				ProxyLogger::catos(ctpTrigger) + "' - Command ignored.");
//		setERROR();  // ### Don't go to ERROR state, if par is wrong ... 
//		// just ignore command and make a log entry (has to be sent to ECS as well !!!)
//		resetCurrentState();
//		return;
//	}

	if (!transformToUpperCase(listOfInDDLs)) {
		// make log entry about error during transforimg to Upper !!!
		cout << "ERROR occured while transforming string to upper cases, ignoring command!" 
				<< endl;
		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"Unable to handle CONFIGURE parameter '" + 
				ProxyLogger::catos(listOfInDDLs) + "' - Command ignored.");
//		setERROR();  // ### Don't go to ERROR state, if par is wrong ... 
		// just ignore command and make a log entry (has to be sent to ECS as well !!!)
		resetCurrentState();
		return;
	}
	if (!transformToUpperCase(listOfOutDDLs)) {
		// make log entry about error during transforimg to Upper !!!
		cout << "ERROR occured while transforming string to upper cases, ignoring command!" 
				<< endl;
		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"Unable to handle CONFIGURE parameter '" +
				ProxyLogger::catos(listOfOutDDLs) + "' - Command ignored.");
//		setERROR();  // ### Don't go to ERROR state, if par is wrong ... 
		// just ignore command and make a log entry (has to be sent to ECS as well !!!)
		resetCurrentState();
		return;
	}
    if (!transformToUpperCase(runType)) {
        // make log entry about error during transforimg to Upper !!!
        cout << "ERROR occured while transforming string to upper cases, ignoring command!"
                << endl;
        mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY,
                "Unable to handle CONFIGURE parameter '" +
                ProxyLogger::catos(runType) + "' - Command ignored.");
//      setERROR();  // ### Don't go to ERROR state, if par is wrong ...
        // just ignore command and make a log entry (has to be sent to ECS as well !!!)
        resetCurrentState();
        return;
    }


//	if ((runNumber_i < 0) || (runNumber_i == 0) && (runNumber_c[0] != '0')) {
//		cout << "ERROR occured while reading CONFIGURE parameter RUN_NUMBER: " 
//				<< runNumber_c << "!" << endl;
//		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
//				"The string given as RUN_NUMBER cannot be converted to an unsigned integer: "
//				+ ProxyLogger::catos(runNumber_c) + ".");
//		setERROR();  // ### Don't go to ERROR state, if par is wrong ... 
//		// just ignore command and make a log entry (has to be sent to ECS as well !!!)
//		resetCurrentState();
//		return;
//	}
		
//	// test of HLT mode is an allowed value 
//	posHLTModes = mpTaskManagerProxy->getPropertyContainer().mPossibleHLTMode;
//	if (strcspn(hltMode, posHLTModes.c_str()) != 0) {
//		cout << endl;
//		cout << "[HLT - proxy]: Received WRONG mode for HLT, ignoring command!" << endl;
//		cout << "[HLT - proxy]: Possible mode values are: " << posHLTModes << endl;
//		cout << endl;
//		ProxyLogger::getLogger()->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
//				"TaskManager-Proxy received wrong HLT mode: " +
//				ProxyLogger::catos(hltMode) + ", (pos. " + posHLTModes +
//				"). Command ignored.");
//		resetCurrentState();
//		return;
//	}

	// due to a more time consumer procedure state is now set to intermediate
	// state "CONFIGURING"
	setCONFIGURING();

	// create ConfigureParameters object
//	ConfigureParameters parameters(detectorList, hltMode, collisionType, runNumber_i,
//			outputFormat, triggerClasses, ctpTrigger, listOfInDDLs, listOfOutDDLs);

	// update properties from file
	mpTaskManagerProxy->updatePropertiesFromFile();

	ConfigureParameters parameters(detectorList, collisionType, outputFormat, 
			triggerClasses, /*ctpTrigger,*/ listOfInDDLs, listOfOutDDLs, runType);

	mpLogger->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY, 
		"CONFIGURE parameters (DETECTOR_LIST; BEAM_TYPE; DATA_FORMAT_VERSION; HLT_TRIGGER_CODE; HLT_IN_DDL_LIST; HLT_OUT_DDL_LIST): "
			+ parameters.parToString(';') + ".");
	parameters.printParameters();

	// changed method to make it more polymorph
//	HLTProxyProperties prop = mpTaskManagerProxy->getPropertyContainer();
//	nRet = parameters.replaceDefaultValues(prop.mDetectorListDefault,
//			prop.mBeamTypeDefault, prop.mDataFormatVersionDefault,
//			prop.mHLTTriggerCodeDefault, prop.mCTPTriggerDefault,
//			prop.mHLTInDDLListDefault, prop.mHLTOutDDLListDefault
//			prop.mRunTypeDefault);

	nRet = parameters.replaceDefaultValues(
			mpTaskManagerProxy->getPropertyContainer());
	cout << "[HLT - proxy]: Changed " << nRet <<
			" 'CONFIGURE' parameters to default value." << endl;
	parameters.printParameters();
	ProxyLogger::getLogger()->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY,
			"'CONFIGURE' parameters after 'DEFAULT' conversion: "
			+ parameters.parToString(HLT_CONFIGURE_PARAMETER_SEPARATOR) + ".");
	
	if (mpTaskManagerProxy->sendTransitionCommand("CONFIGURE", &parameters) != 0) {
		// something went wrong --> do appropriate stuff ???
		setERROR();
	}
}


void HLT::SHUTDOWN() {
	if ((strcmp(getState(), HLT_PROXY_STATE_INITIALIZED) != 0) &&
			(strcmp(getState(), HLT_PROXY_STATE_ERROR) != 0)) {
		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"HLT-Proxy received 'SHUTDOWN' command in state '" + 
				ProxyLogger::catos(getState()) + 
				"'. Not accepted in this state - ignoring command.");
		resetCurrentState();
		return;
	}

	// due to a more time consumer procedure state is now set to intermediate
	// state "RAMPING_DOWN"
	setDEINITIALIZING();

	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"Proxy received 'SHUTDOWN' command from ECS.");
	cout << "[HLT - proxy]: HLT proxy received SHUTDOWN command." << endl;
	cout << "[HLT - proxy]: Ramping down the HLT cluster now." << endl << endl;

	if (mpTaskManagerProxy->sendTransitionCommand("SHUTDOWN") != 0) {
		// something went wrong --> do appropriate stuff
		setERROR();
	}
}


void HLT::ENGAGE() {
	char* hltMode = 0;          // introduced as ENGAGE Parameter since v 1.1
    char* runNumber_c = 0;  	// introduced as ENGAGE Parameter since v 1.1
	int runNumber_i = 0;		// introduced as ENGAGE Parameter since v 1.1
	string posHLTModes;			// introduced as ENGAGE Parameter since v 1.1
	char* ctpTrigger = 0;		// moved to ENGAGE parameter since v 1.3
	int nRet = 0;

	if (strcmp(getState(), HLT_PROXY_STATE_CONFIGURED) != 0) {
		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"HLT-Proxy received 'ENGAGE' command in state '" + 
				ProxyLogger::catos(getState()) + 
				"'. Not accepted in this state - ignoring command.");
		resetCurrentState();
		return;
	}

	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"Proxy received 'ENGAGE' command from ECS.");
	cout << "[HLT - proxy]: HLT proxy received ENGAGE command." << endl << endl;

	hltMode = getParHLT_MODE();
	runNumber_c = getParRUN_NUMBER();
	runNumber_i = atoi(runNumber_c);
	ctpTrigger = getParCTP_TRIGGER_CLASS();

	if (msDebugOutput) {
		cout << endl << "[DEBUG]: "  << endl;
		cout << " [DEBUG] HLT-Mode: " << hltMode << endl;
		cout << " [DEBUG] Run-Number: " << runNumber_c << endl;
		cout << " [DEBUG] CTP-TriggerClasses: " << ctpTrigger << endl;
	}

	if (!transformToUpperCase(hltMode)) {
		// make log entry about error during transforimg to Upper !!!
		cout << "ERROR occured while transforming string to upper cases [in ENGAGE cmd], ignoring command!" 
				<< endl;
		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY,
				"Unable to handle ENGAGE parameter [HLT_MODE] '" +
				ProxyLogger::catos(hltMode) + "' - Command ignored.");
//		setERROR();  // ### Don't go to ERROR state, if par is wrong ...
		// just ignore command and make a log entry (has to be sent to ECS as well !!!)
		resetCurrentState();
		return;
	}

	if ((runNumber_i < 0) || (runNumber_i == 0) && (runNumber_c[0] != '0')) {
		cout << "ERROR occured while reading ENGAGE parameter [RUN_NUMBER]: "
				<< runNumber_c << " - Not allowed value, ignoring command!" 
				<< endl;
		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY,
				"String given as RUN_NUMBER [ENGAGE parameter] cannot be converted to unsigned integer: "
				+ ProxyLogger::catos(runNumber_c) + " - Command ignored.");
//		setERROR();  // ### Don't go to ERROR state, if par is wrong ...
		// just ignore command and make a log entry (has to be sent to ECS as well !!!)
		resetCurrentState();
		return;
	}

	if (!transformToUpperCase(ctpTrigger)) {
		// make log entry about error during transforimg to Upper !!!
		cout << "ERROR occured while transforming string to upper cases, ignoring command!"
				<< endl;
		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY,
				"Unable to handle CONFIGURE parameter '" +
				ProxyLogger::catos(ctpTrigger) + "' - Command ignored.");
//      setERROR();  // ### Don't go to ERROR state, if par is wrong ...
		// just ignore command and make a log entry (has to be sent to ECS as well !!!)
		resetCurrentState();
		return;
	}


	// test of HLT mode is an allowed value
	posHLTModes = mpTaskManagerProxy->getPropertyContainer().mPossibleHLTMode;
	if ((strlen(hltMode) != 1) || (strcspn(hltMode, posHLTModes.c_str()) != 0)) {
		cout << endl;
		cout << "ERROR: HLT-proxy received WRONG mode '" << hltMode << 
				"' for HLT during ENGAGE cmd, ignoring command!" << endl;
		cout << "Possible mode values are (must be only ONE character): "
				<< posHLTModes << endl;
		ProxyLogger::getLogger()->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY,
				"TaskManager-Proxy received wrong HLT_MODE during ENGAGE command: " +
				ProxyLogger::catos(hltMode) + ", (possible values: " + 
				posHLTModes + "). Command ignored.");
		resetCurrentState();
		return;
	}

    // due to a more time consumer procedure state is now set to intermediate
    // state "ENGAGING"
    setENGAGING();

	// update properties from file
	mpTaskManagerProxy->updatePropertiesFromFile();

    EngageParameters engPars(hltMode, runNumber_i, ctpTrigger);
    mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY,
			"ENGAGE parameters (HLT_MODE; RUN_NUMBER, CTP_TRIGGER_CLASS): " + 
			engPars.parToString(';') + ".");
	engPars.printParameters();

    nRet = engPars.replaceDefaultValues(
            mpTaskManagerProxy->getPropertyContainer());
    cout << "[HLT - proxy]: Changed " << nRet <<
            " 'ENGAGE' parameters to default value." << endl;
    engPars.printParameters();
    ProxyLogger::getLogger()->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY,
            "'ENGAGE' parameters after 'DEFAULT' conversion: "
            + engPars.parToString(';') + ".");

    if (mpTaskManagerProxy->sendTransitionCommand("ENGAGE", &engPars) != 0) {
        // something went wrong --> do appropriate stuff ???
        setERROR();
    }

//	if (mpTaskManagerProxy->sendTransitionCommand("ENGAGE") != 0) {
//		// something went wrong --> do appropriate stuff
//		setERROR();
//	}
}


void HLT::RESET() {
	if ((strcmp(getState(), HLT_PROXY_STATE_CONFIGURED) != 0) &&
			(strcmp(getState(), HLT_PROXY_STATE_ERROR) != 0)) {
		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"HLT-Proxy received 'RESET' command in state '" + 
				ProxyLogger::catos(getState()) + 
				"'. Not accepted in this state - ignoring command.");
		resetCurrentState();
		return;
	}	
	
	// due to a more time consumer procedure state is now set to intermediate state
	if (strcmp(getState(), HLT_PROXY_STATE_CONFIGURED) == 0) {
		setDECONFIGURING(); // state "DECONFIGURING"
	} else {
		setINITIALIZING(); // state "INITIALIZING"
	}

	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"Proxy received 'RESET' command from ECS.");
	cout << "[HLT - proxy]: HLT proxy received RESET command." << endl;
	cout << "[HLT - proxy]: Reinitializing HLT cluster ..." << endl << endl;

//	if (mpTaskManagerProxy->sendTransitionCommand("INITIALIZE") != 0) {
	if (mpTaskManagerProxy->sendTransitionCommand("RESET") != 0) {
		// something went wrong --> do appropriate stuff
		setERROR();
	}
}


void HLT::START() {
	if (strcmp(getState(), HLT_PROXY_STATE_READY) != 0) {
		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"HLT-Proxy received 'START' command in state '" + 
				ProxyLogger::catos(getState()) + 
				"'. Not accepted in this state - ignoring command.");
		resetCurrentState();
		return;
	}

	// due to a more time consumer procedure state is now set to intermediate
	// state "STARTING"
	setSTARTING();

	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"Proxy received 'START' command from ECS.");
	cout << "[HLT - proxy]: HLT proxy received START command." << endl;
	cout << "[HLT - proxy]: HLT cluster will start and deliver trigger decisions now." 
			<< endl << endl;

	if (mpTaskManagerProxy->sendTransitionCommand("START") != 0) {
		// something went wrong --> do appropriate stuff
		setERROR();
	}
}


void HLT::DISENGAGE() {	
	if (strcmp(getState(), HLT_PROXY_STATE_READY) != 0) {
		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"HLT-Proxy received 'DISENGAGE' command in state '" + 
				ProxyLogger::catos(getState()) + 
				"'. Not accepted in this state - ignoring command.");
		resetCurrentState();
		return;
	}

	// due to a more time consumer procedure state is now set to intermediate
	// state "DISENGAGING"
	setDISENGAGING();

	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"Proxy received 'DISENGAGE' command from ECS.");
	cout << "[HLT - proxy]: HLT proxy received DISENGAGE command." << endl << endl;

	if (mpTaskManagerProxy->sendTransitionCommand("DISENGAGE") != 0) {
		// something went wrong --> do appropriate stuff
		setERROR();
	}
}


void HLT::STOP() {
	if (strcmp(getState(), HLT_PROXY_STATE_RUNNING) != 0) {
		mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"HLT-Proxy received 'STOP' command in state '" + 
				ProxyLogger::catos(getState()) + 
				"'. Not accepted in this state - ignoring command.");
		resetCurrentState();
		return;
	}

	// due to a more time consumer procedure state is now set to intermediate
	// state "COMPLETING"
	setCOMPLETING();

	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"Proxy received 'STOP' command from ECS.");
	cout << "[HLT - proxy]: HLT proxy received STOP command." << endl << endl;

	if (mpTaskManagerProxy->sendTransitionCommand("STOP") != 0) {
		// something went wrong --> do appropriate stuff
		setERROR();
	}
}


void HLT::SET_ACTIVE() {
	// ToDo check for stable state, if design decision is made

	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"Proxy received 'SET_ACTIVE' command from ECS.");
	cout << "[HLT - proxy]: HLT proxy received SET_ACTIVE command." << endl << endl;

	if (mpTaskManagerProxy->sendTransitionCommand("SET_ACTIVE") != 0) {
		// something went wrong --> do appropriate stuff
		setERROR();
	}
	resetCurrentState();
}


void HLT::SET_PASSIVE() {
	// ToDo check for stable state, if design decision is made

	mpLogger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
			"Proxy received 'SET_PASSIVE' command from ECS.");
	cout << "[HLT - proxy]: HLT proxy received SET_PASSIVE command." << endl << endl;

	if (mpTaskManagerProxy->sendTransitionCommand("SET_PASSIVE") != 0) {
		// something went wrong --> do appropriate stuff
		setERROR();
	}
	resetCurrentState();
}


//////////////////////////////// TM -> informs ////////////////////////////////
bool HLT::informTMstateOFF() {
	char* currentState = getState();
	printStates(currentState, HLT_PROXY_STATE_OFF);
	// if already in right state do nothing and return true
	if (strcmp(currentState, HLT_PROXY_STATE_OFF) == 0) {
		return true;
	}
	// check current state -> only perform transition if allowed 
	if ((strcmp(currentState, HLT_PROXY_STATE_DEINITIALIZING) == 0) || 
			(strcmp(currentState,HLT_PROXY_STATE_INITIALIZED) == 0) ||
			(strcmp(currentState,HLT_PROXY_STATE_ERROR) == 0)) {
		setOFF();
		return true;
	}
	return false;
}


bool HLT::informTMstateINITIALIZING() {
	char* currentState = getState();
	printStates(currentState, HLT_PROXY_STATE_INITIALIZING);
	// if already in right state do nothing and return true
	if (strcmp(currentState, HLT_PROXY_STATE_INITIALIZING) == 0) {
		return true;
	}
	// check current state -> only perform transition if allowed
	if ((strcmp(currentState, HLT_PROXY_STATE_OFF) == 0) ||
			(strcmp(currentState, HLT_PROXY_STATE_ERROR) == 0) ||
			(strcmp(currentState, HLT_PROXY_STATE_CONFIGURED) == 0)) {
		setINITIALIZING();
		return true;
	}

    // this is only included to map  the correct state when Shutdown has been
    // signaled from ERROR state
	if (strcmp(currentState, HLT_PROXY_STATE_DEINITIALIZING) == 0) {
		// allow this transition since this intermediate state is required by 
		// the TaskManager to transit from ERROR to OFF (remain in DEINITIALIZING)
		return true; 
	}

	return false;
}


bool HLT::informTMstateINITIALIZED() {
	char* currentState = getState();
	printStates(currentState, HLT_PROXY_STATE_INITIALIZED);
	// if already in right state do nothing and return true
	if (strcmp(currentState, HLT_PROXY_STATE_INITIALIZED) == 0) {
		return true;
	}
	// check current state -> only perform transition if allowed
	if ((strcmp(currentState, HLT_PROXY_STATE_INITIALIZING) == 0) ||
			(strcmp(currentState, HLT_PROXY_STATE_CONFIGURED) == 0) ||
			(strcmp(currentState, HLT_PROXY_STATE_ERROR) == 0) ||
			(strcmp(currentState, HLT_PROXY_STATE_OFF) == 0) ||
			(strcmp(currentState, HLT_PROXY_STATE_DECONFIGURING) == 0)) {
		setINITIALIZED();
		return true;
	}

	// this is only included to map  the correct state when Shutdown has been 
	// signaled from ERROR state
	if (strcmp(currentState, HLT_PROXY_STATE_DEINITIALIZING) == 0) {
        // allow this transition since this intermediate state is required by
        // the TaskManager to transit from ERROR to OFF (remain in DEINITIALIZING)
        return true;
    }

	return false;
}


bool HLT::informTMstateCONFIGURING() {
	char* currentState = getState();
	printStates(currentState, HLT_PROXY_STATE_CONFIGURING);
	// if already in right state do nothing and return true
	if (strcmp(currentState, HLT_PROXY_STATE_CONFIGURING) == 0) {
		return true;
	}
	// check current state -> only perform transition if allowed
	if (strcmp(currentState, HLT_PROXY_STATE_INITIALIZED) == 0) {
		setCONFIGURING();
		return true;
	}
	return false;
}


bool HLT::informTMstateCONFIGURED() {
	char* currentState = getState();
	printStates(currentState, HLT_PROXY_STATE_CONFIGURED);
	// if already in right state do nothing and return true
	if (strcmp(currentState, HLT_PROXY_STATE_CONFIGURED) == 0) {
		return true;
	}
	// check current state -> only perform transition if allowed
	if ((strcmp(currentState, HLT_PROXY_STATE_CONFIGURING) == 0) ||
			(strcmp(currentState, HLT_PROXY_STATE_DISENGAGING) == 0) ||
			(strcmp(currentState, HLT_PROXY_STATE_INITIALIZED) == 0) ||
			(strcmp(currentState, HLT_PROXY_STATE_READY) == 0)) {
		setCONFIGURED();
		return true;
	}
	return false;
}


bool HLT::informTMstateENGAGING() {
	char* currentState = getState();
	printStates(currentState, HLT_PROXY_STATE_ENGAGING);
	// if already in right state do nothing and return true
	if (strcmp(currentState, HLT_PROXY_STATE_ENGAGING) == 0) {
		return true;
	}
	// check current state -> only perform transition if allowed
	if (strcmp(currentState, HLT_PROXY_STATE_CONFIGURED) == 0) {
		setENGAGING();
		return true;
	}
	return false;
}


bool HLT::informTMstateREADY() {
	char* currentState = getState();
	printStates(currentState, HLT_PROXY_STATE_READY);
	// if already in right state do nothing and return true
	if (strcmp(currentState, HLT_PROXY_STATE_READY) == 0) {
		return true;
	}
	// check current state -> only perform transition if allowed
	if ((strcmp(currentState, HLT_PROXY_STATE_ENGAGING) == 0) ||
			(strcmp(currentState, HLT_PROXY_STATE_RUNNING) == 0) ||
			(strcmp(currentState, HLT_PROXY_STATE_COMPLETING) == 0) ||
			(strcmp(currentState, HLT_PROXY_STATE_CONFIGURED) == 0)) {
		setREADY();
		return true;
	}
	return false;
}


bool HLT::informTMstateSTARTING() {
    char* currentState = getState();
    printStates(currentState, HLT_PROXY_STATE_STARTING);
    // if already in right state do nothing and return true
    if (strcmp(currentState, HLT_PROXY_STATE_STARTING) == 0) {
        return true;
    }
    // check current state -> only perform transition if allowed
    if (strcmp(currentState, HLT_PROXY_STATE_READY) == 0) {
        setSTARTING();
        return true;
    }
    return false;
}



bool HLT::informTMstateDISENGAGING() {
	char* currentState = getState();
	printStates(currentState, HLT_PROXY_STATE_DISENGAGING);
	// if already in right state do nothing and return true
	if (strcmp(currentState, HLT_PROXY_STATE_DISENGAGING) == 0) {
		return true;
	}
	// check current state -> only perform transition if allowed
	if (strcmp(currentState, HLT_PROXY_STATE_READY) == 0) {
		setDISENGAGING();
		return true;
	}
	return false;
}


bool HLT::informTMstateRUNNING() {
	char* currentState = getState();
	printStates(currentState, HLT_PROXY_STATE_RUNNING);
	// if already in right state do nothing and return true
	if (strcmp(currentState, HLT_PROXY_STATE_RUNNING) == 0) {
		return true;
	}
	// check current state -> only perform transition if allowed
	if ((strcmp(currentState, HLT_PROXY_STATE_READY) == 0) ||
			(strcmp(currentState, HLT_PROXY_STATE_STARTING) == 0)) {
		setRUNNING();
		return true;
	}
	return false;
}


bool HLT::informTMstateCOMPLETING() {
	char* currentState = getState();
	printStates(currentState, HLT_PROXY_STATE_COMPLETING);
	// if already in right state do nothing and return true
	if (strcmp(currentState, HLT_PROXY_STATE_COMPLETING) == 0) {
		return true;
	}
	// check current state -> only perform transition if allowed
	if (strcmp(currentState, HLT_PROXY_STATE_RUNNING) == 0) {
		setCOMPLETING();
		return true;
	}
	return false;
}


bool HLT::informTMstateDECONFIGURING() {
    char* currentState = getState();
    printStates(currentState, HLT_PROXY_STATE_DECONFIGURING);
    // if already in right state do nothing and return true
    if (strcmp(currentState, HLT_PROXY_STATE_DECONFIGURING) == 0) {
        return true;
    }
    // check current state -> only perform transition if allowed
    if (strcmp(currentState, HLT_PROXY_STATE_CONFIGURED) == 0) {
        setDECONFIGURING();
        return true;
    }
    return false;
}


bool HLT::informTMstateDEINITIALIZING() {
	char* currentState = getState();
	printStates(currentState, HLT_PROXY_STATE_DEINITIALIZING);
	// if already in right state do nothing and return true
	if (strcmp(currentState, HLT_PROXY_STATE_DEINITIALIZING) == 0) {
		return true;
	}
	// check current state -> only perform transition if allowed
	if ((strcmp(currentState, HLT_PROXY_STATE_ERROR) == 0) ||
			(strcmp(currentState, HLT_PROXY_STATE_INITIALIZED) == 0)) {
		setDEINITIALIZING();
		return true;
	}
	return false;
}


bool HLT::informTMstateERROR() {
	printStates(getState(), HLT_PROXY_STATE_ERROR);
	setERROR();
	return true;
}


bool HLT::transformToUpperCase(char* str) {
	if (str == 0) {
		return false;
	}

	while (*str != '\0') {
		*str = toupper(*str);
		str++;
	}
	return true;
}


void HLT::setOFF() {
	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"HLT-Proxy is set to 'OFF'.");
	HLTProxy::setState("OFF");
}

void HLT::setINITIALIZING() {
	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"HLT-Proxy is set to 'INITIALIZING'.");
	HLTProxy::setState("INITIALIZING");
}

void HLT::setINITIALIZED() {
	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"HLT-Proxy is set to 'INITIALIZED'.");
	HLTProxy::setState("INITIALIZED");
}

void HLT::setCONFIGURING() {
	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"HLT-Proxy is set to 'CONFIGURING'.");
	HLTProxy::setState("CONFIGURING");
}

void HLT::setCONFIGURED() {
	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"HLT-Proxy is set to 'CONFIGURED'.");
	HLTProxy::setState("CONFIGURED");
}

void HLT::setENGAGING() {
	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"HLT-Proxy is set to 'ENGAGING'.");
	HLTProxy::setState("ENGAGING");
}

void HLT::setREADY() {
	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"HLT-Proxy is set to 'READY'.");
	HLTProxy::setState("READY");
}

void HLT::setSTARTING() {
	 mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY,
			"HLT-Proxy is set to 'STARTING'.");
	HLTProxy::setState("STARTING");
}

void HLT::setRUNNING() {
	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"HLT-Proxy is set to 'RUNNING'.");
	HLTProxy::setState("RUNNING");
}

void HLT::setCOMPLETING() {
	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"HLT-Proxy is set to 'COMPLETING'.");
	HLTProxy::setState("COMPLETING");
}

void HLT::setDISENGAGING() {
	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"HLT-Proxy is set to 'DISENGAGING'.");
	HLTProxy::setState("DISENGAGING");
}

void HLT::setDECONFIGURING() {
    mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY,
            "HLT-Proxy is set to 'DECONFIGURING'.");
    HLTProxy::setState("DECONFIGURING");
}

void HLT::setDEINITIALIZING() {
	mpLogger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"HLT-Proxy is set to 'DEINITIALIZING'.");
	HLTProxy::setState("DEINITIALIZING");
}

void HLT::setERROR() {
	HLTProxy::setState("ERROR");
	int num = mpTaskManagerProxy->setTMsToErrorEquivalentState();
	mpLogger->createLogMessage(MSG_ALARM, LOG_SOURCE_PROXY, 
			"HLT-Proxy is set to 'ERROR' (" + ProxyLogger::itos(num) + ").");
}

void HLT::resetCurrentState() {
	char* currentState = getState();
	mpLogger->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY, 
			"HLT-Proxy is reseting current state: " + 
			ProxyLogger::catos(currentState) + ".");
	HLTProxy::setState(currentState);
}


void HLT::printStates(const char* oldState, const char* newState) {
	if (msDebugOutput) {
		cout << " [DEBUG] HLT-Proxy state transition: " << oldState << " (old), "
				<< newState << " (new)." << endl;
	}
}

