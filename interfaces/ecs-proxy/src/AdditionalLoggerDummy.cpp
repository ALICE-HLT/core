/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2008
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include "AdditionalLoggerDummy.hpp"

#include "ProxyLogger.hpp"


#include <iostream>


using namespace std;

using namespace alice::hlt::proxy;

const std::string AdditionalLoggerDummy::cmLOGGER_NAME = "LoggerDummy";

AdditionalLoggerDummy::AdditionalLoggerDummy(string filename) :
			AdditionalLoggerBase(cmLOGGER_NAME), mFilename(filename) {
    mpLogFile = 0;
    Timestamp now;

    // get log file handle
    mpLogFile = new ofstream(mFilename.c_str(), ios_base::out | ios_base::app);

    if (!(*mpLogFile)) {
        cout << "Unable to create logfile (" << mFilename << 
				") for Additional Logger: " << cmLOGGER_NAME << endl;
    } else {
        string msg;
        *mpLogFile << endl;
        *mpLogFile << "   --- LOG FILE for Additional Logger '" << 
				cmLOGGER_NAME << "' started at " << now.date << " ---" << endl;

#ifdef __EMULATOR
        msg = now.date + ProxyLogger::cmMSG_WARNING_STRING + 
				"- HLT-Proxy: *** NOTE that proxy has been compiled with the emulation of (Master)TaskManager! ***";
        *mpLogFile << msg << endl;
#endif

    }
}


AdditionalLoggerDummy::AdditionalLoggerDummy(const AdditionalLoggerDummy& aldRef) :
			AdditionalLoggerBase(aldRef), mFilename(aldRef.mFilename) {
	mpLogFile = new ofstream(aldRef.mFilename.c_str(), 
			ios_base::out | ios_base::app);

    if (!(*mpLogFile)) {
        cout << "Unable to acquire handle for logfile (" << aldRef.mFilename <<
                ") for Additional Logger: " << cmLOGGER_NAME << endl;
    }
}

AdditionalLoggerDummy& AdditionalLoggerDummy::operator=(
			const AdditionalLoggerDummy& rhs) {
    if (this == &rhs) {
        return *this;
    }
	AdditionalLoggerBase::operator=(rhs);

	mFilename = rhs.mFilename;
	mpLogFile = new ofstream(rhs.mFilename.c_str(), 
			ios_base::out | ios_base::app);

    if (!(*mpLogFile)) {
        cout << "Unable to acquire handle for logfile (" << rhs.mFilename <<
                ") for Additional Logger: " << cmLOGGER_NAME << endl;
    }


    return *this;
}

AdditionalLoggerDummy::~AdditionalLoggerDummy() {

    if (*mpLogFile) {
        Timestamp now;
		string msg;
        msg = now.date + ProxyLogger::cmMSG_INFO_STRING +
				"- Closing LOG FILE for Additional Logger: " + cmLOGGER_NAME;
        *mpLogFile << msg << endl;
        *mpLogFile << "   --- LOG FILE stopped at " << now.date << " ---" <<
                endl << endl;
        // close log file
        delete mpLogFile;
    }
}

bool AdditionalLoggerDummy::relayLogMessage(const Timestamp& stamp, 
			Log_Levels type, string origin, string description) {
	bool bRet = false;
	
	if (*mpLogFile) {
//		string msg;
//		msg = stamp.date + convertLogLevel(type) + "- " + origin + ": " + description;
		*mpLogFile << stamp.date << convertLogLevel(type) << "- " << origin <<
				": " << description << endl;
		bRet = true;
	}
	
	return bRet;
}


