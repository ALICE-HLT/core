/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <iostream>
#include <errno.h>


#include "TMproxy.hpp"
#include "HLTProxy.hpp"
#include "MasterTM.hpp"
#include "PropertyReader.hpp"
#include "Parameters.hpp"
#include "ConfigureParameters.hpp"
#include "EngageParameters.hpp"
#include "TMNamesAdapter.hpp"
#include "error_codes.h"
#include "property_defines.h"
#include "hlt_proxy_state_defines.h"

#ifdef __EMULATOR
#include "InterfaceLib.h"
#else
#include <InterfaceLib.h>
#endif


using namespace std;
using namespace alice::hlt::proxy;


int TMproxy::msInterrupt_Wait_Error = -13;

int TMproxy::msInterrupt_Wait_Clean_Exit = ALICE_HLT_PROXY_SUCCESS;


TMproxy::TMproxy(HLT* pSmiProxy) {
	int nRet = 0;
	pthread_attr_t threadAttr;
	pthread_attr_t threadAttrInterrupt;
	string* tempLog;
	char* propFileName = 0;
	unsigned int tempLevel = 127;
	mPollingSleep = HLT_REQUEST_STATE_THREAD_SLEEP;

	// initialize member vars
	mpHLTproxy = pSmiProxy;
	mValid = false;
	mpPR = 0;
	mpLocal_listen_address = 0;
	mpInterfaceLib_data = 0;
	mNumTMs = 0;
	
	// assign member vars
	// get the PropertyReader and load the property file
	mpPR = PropertyReader::getPropertyReader();
	propFileName = getenv("HLT_PROXY_PROPERTY_FILE");
	if (propFileName == 0) {
		propFileName =HLT_PROXY_PROPERTY_FILENAME;
	}
	if (!mpPR->readPropertiesFromFile(propFileName)) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy cannot read from property file");
		cout << "ERROR while reading property file." << endl;
		exit(-1);
//		return;
	}

	if (mpPR->retrievePropertyValue(ALICE_HLT_PROXY_PROPERTY_LOG_LEVEL, &tempLog)) {
		sscanf(tempLog->c_str(), "%d", &tempLevel);
		ProxyLogger::getLogger()->setLogLevel(tempLevel);
	} else {
		ProxyLogger::getLogger()->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy cannot retrieve log level from properties, keeping current level.");
		cout << "ERROR while reading log level property." << endl;
	}

	// get the properties for the HLT - proxy (defaults for configure command)
	nRet = mpPR->setHLTProxyProperties(&mProperties);
	if (nRet < 0) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy cannot set properties in HLT-Proxy");
		cout << "ERROR while setting HLT - proxy properties." << endl;
		if (nRet < -1) {
			cout << "ERROR: Property file has wrong version number; required is version: '"
					<< mProperties.mVersionNumber << "'." << endl;
			cout << " -> Use correct property-file and restart HLT-ECS-proxy!" << endl;
		}
		exit(-1);
//		return;
	}

	// set sleep time
	pthread_mutex_init(&mSleepMutex, 0);
	mPollingSleep = mProperties.mTMPollingSleep;

	// get properties for TM - proxy
	mpLocal_listen_address = allocateLocalListenAddress();
	if (mpLocal_listen_address == 0) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy cannot set local listen address (InterfaceLib) from properties");
		cout << "ERROR while setting local address from properties." << endl;
		exit(-1);
//		return;
	}
	ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY, 
			"TaskManager-Proxy has local listen address: " + 
			ProxyLogger::catos(mpLocal_listen_address) + " .");
	//cout << "Debug: local listen address: " << mpLocal_listen_address << endl; /// !!!

	// initialize InterfaceLib
	nRet = Initialize(&mpInterfaceLib_data, mpLocal_listen_address);
	if (nRet != 0) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy cannot initialize InterfaceLib - Error: '"
				+ ProxyLogger::errnotos(nRet) + "' - LocalListenAddress: '" +
				ProxyLogger::catos(mpLocal_listen_address) + "'.");
		cout << "ERROR while initializing InterfaceLib: " << 
				ProxyLogger::errnotos(nRet) << "." << endl;
		mpInterfaceLib_data = 0;
		exit(-1);
//		return;
	}
	ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY, 
			"TaskManager-Proxy has initialized InterfaceLib.");


	// start InterruptWait -> required to retrieve QueryStates
	// this has to be called in own thread (InterruptWait has an endless loop)
	// !!! check here for log msgs and exit points !!!
	cout << "Initializing interrupt wait thread ..." << endl;
	nRet = pthread_attr_init(&threadAttrInterrupt);
	if (nRet != 0) {
		cout << "Init thread (interrupt wait) attribute error: " << nRet << endl;
	}
	nRet = pthread_attr_setdetachstate(&threadAttrInterrupt,
			PTHREAD_CREATE_DETACHED);
	if (nRet != 0) {
		cout << "Set thread attribute (interrupt wait) error: " << nRet << endl;
	}
	nRet = pthread_create(&mInterruptWaitThreadHandle, &threadAttrInterrupt,
			&interruptWaitThread, (void*) this);
	if (nRet != 0) {
		cout << "Create thread for interrupt wait error: " << nRet << endl;
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy cannot create thread for InterruptWait of InterfaceLib - Error: "
				+ ProxyLogger::errnotos(errno) + ".");
	}
	nRet = pthread_attr_destroy(&threadAttrInterrupt);
	if (nRet != 0) {
		cout << "Destroy thread attribute (interrupt wait) error: " << nRet << endl;
	}
	

	// build all masterTM objects with info from the property file
	if (mpPR->retrievePropertyValue(ALICE_HLT_PROXY_NUMBER_OF_TM, &tempLog)) {
		sscanf(tempLog->c_str(), "%d", &mNumTMs);
		ProxyLogger::getLogger()->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy will connect to '" + 
				ProxyLogger::itos(mNumTMs) + "' TaskManager instances.");
	} else {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy cannot retrieve number of TaskManager to connect to. Update property-file!");
		cout << "ERROR while reading number of TMs in property-file. Update property-file and restart proxy"
				<< endl;
		exit(-1);
	}

	for (unsigned int i = 1; i <= mNumTMs; i++) {
		string* tmAddress = allocateMasterTMAddress(i);
		if (tmAddress == 0) {
			ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
					"TaskManager-Proxy cannot allocate address of MasterTaskManager '"
					+ ProxyLogger::itos(i) + "' ."); 
			// !!! (test if this check work properly)
			cout << "Error while allocating address, skipping TM no. " << i << endl;
			continue;
		}
		MasterTM masterTM(*tmAddress, this);
		ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy will try to contact MasterTaskManager '" +
				masterTM.getTMName() + "' at " + *tmAddress + " .");
		//cout << i << ". MasterTM created at address " << *tmAddress << endl; /// !!!
		// check if Copy Construtor for materTMs works properly
		mMasterTMCollection.push_back(masterTM);
		delete tmAddress;
	}

	// TM-proxy is valid, if at least one master TM has been initialized correctly
	if (mMasterTMCollection.size() > 0) {
		mValid = true;

		// !!! check here for log msgs and exit points !!!

		// start thread to request master TM states:
		cout << "Initializing request thread ..." << endl;
		nRet = pthread_attr_init(&threadAttr);
		if (nRet != 0) {
			cout << "Init thread attribute error: " << nRet << endl;
		}
		nRet = pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_DETACHED);
		if (nRet != 0) {
			cout << "Set thread attribute error: " << nRet << endl;
		}
		nRet = pthread_create(&mRequestTMsThreadHandle, &threadAttr, &requestTMStateThread, (void*) this);
		if (nRet != 0) {
			cout << "Create thread to request TM states error: " << nRet << endl;
			ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
					"TaskManager-Proxy cannot create thread for MasterTaskManager requests - Error: "
					+ ProxyLogger::errnotos(errno) + ".");
		}
		nRet = pthread_attr_destroy(&threadAttr);
		if (nRet != 0) {
			cout << "Destroy thread attribute error: " << nRet << endl;
		}
		//////////////////////////
	} else {
		ProxyLogger::getLogger()->createLogMessage(MSG_ALARM, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy has no MasterTaskManager to contact.");
		// !!! continue here !!!
	}
}


TMproxy::~TMproxy() {
	int nRet = 0;

	// stop request master TMs state thread
	nRet = pthread_cancel(mRequestTMsThreadHandle);
	if (nRet != 0) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy cannot cancel thread for MasterTaskManager requests - Error: "
				+ ProxyLogger::errnotos(nRet) + "'.");
		cout << "Error while canceling request thread: " <<
				ProxyLogger::errnotos(nRet) << endl;
	}

	// disconnect of MasterTM connections. NOTE: Disconnect should NOT be done
	// in Destructor of MasterTMs, because object might be copied with Copy-
	// Constructor and the original destroyed afterwards without explicitly seen  
	// in source; call of disconnect for all MasterTMs in TMproxy Destructor 
	// before vector of MasterTMs is cleared, avoids the resulting problem
	vector<MasterTM>::iterator it;
	for (it = mMasterTMCollection.begin(); it != mMasterTMCollection.end(); ++it) {
		if (it->isConnected()) {
			nRet = DisconnectFromProcess(mpInterfaceLib_data, 0, 0,	
					it->getTMaddress_bin());
			if (nRet != 0) {
				ProxyLogger::getLogger()->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
						"TaskManager-Proxy is unable to disconnect MasterTaskManager '"
						+ it->getTMName() + "' from its corresponding process ("
						+ ProxyLogger::errnotos(nRet) + "); discarding disconnect ...");
			} else {
				ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, 
						LOG_SOURCE_PROXY, "MasterTaskManager '" + it->getTMName() +
						"' has been successfully disconnected to its corresponding process.");
			}
		}
	}
	
	// delete the content of the vector of TM parameters
	mMasterTMCollection.clear();

	if (mpLocal_listen_address != 0) {
		delete[] mpLocal_listen_address;
	}

	nRet = StopInterruptWait(mpInterfaceLib_data);
	if (nRet != 0) {
		ProxyLogger::getLogger()->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy encountered an error while stopping InterruptWait of InterfaceLib - Error: '"
				+ ProxyLogger::errnotos(nRet) + "'.");
		// Error while terminating InterfaceLib
		cout << "ERROR while stopping InterruptWait of InterfaceLib - Error: " <<
				ProxyLogger::errnotos(nRet) << "." << endl;
	}

	// stop InterruptWait thread, if not yet finished
	nRet = pthread_cancel(mInterruptWaitThreadHandle);
	if (nRet != 0) {
		ProxyLogger::getLogger()->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy cannot cancel InterruptWait thread, most likely already terminated: "
				+ ProxyLogger::errnotos(nRet) + "'.");
		cout << "Canceling InterruptWait thread failed, most likely already terminated: " <<
				ProxyLogger::errnotos(nRet) << endl;
	}

	// deinitialize the InterfaceLib 
	if (mpInterfaceLib_data != 0) {
		nRet = Terminate(mpInterfaceLib_data);
		if (nRet != 0) {
			ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
					"TaskManager-Proxy encountered an error while terminating the InterfaceLib - Error: '"
					+ ProxyLogger::errnotos(nRet) + "'.");
			// Error while terminating InterfaceLib
			cout << "ERROR while terminating InterfaceLib - Error: " <<
					ProxyLogger::errnotos(nRet) << "." << endl;
		}
	}
	if (mpPR != 0) {
		delete mpPR;
	}

	pthread_mutex_destroy(&mSleepMutex);
}

char* TMproxy::allocateLocalListenAddress() {
	string* tmp;
	string address;
	char* localAddress = 0;
	address.clear();

	// get protocol type
	if (!mpPR->retrievePropertyValue(ALICE_HLT_PROXY_INTERFACE_LIB_PROTOCOL_TYPE, &tmp)) {
		return 0;
	}
	address.append(*tmp);

	// get hostname
	if (!mpPR->retrievePropertyValue(ALICE_HLT_PROXY_INTERFACE_LIB_LOCAL_HOST, &tmp)) {
		return 0;
	}
	address.append(*tmp);
	address.append(".");

	// get domain
	if (!mpPR->retrievePropertyValue(ALICE_HLT_PROXY_INTERFACE_LIB_LOCAL_DOMAIN, &tmp)) {
		return 0;
	}
	address.append(*tmp);
	address.append(":");

	// get portnumber
	if (!mpPR->retrievePropertyValue(ALICE_HLT_PROXY_INTERFACE_LIB_LOCAL_PORT, &tmp)) {
		return 0;
	}
	address.append(*tmp);

	localAddress = new char[address.length() + 1];
	strcpy(localAddress, address.c_str());

	return localAddress;
}


string* TMproxy::allocateMasterTMAddress(unsigned short number) {
	if ((number <= 0) || (number > 99)) {
		ProxyLogger::getLogger()->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy asks for out-of-range number for MasterTaskManager (range 1-99).");
		return 0;
	}
//// !!! ??? This is the part which should be checked again 
	string* tmp = 0;
	string address;
	string* tmAddress = 0;
	char insertNum[2] = {'0', '0'};
	string properties[4] = {ALICE_HLT_PROXY_MASTER_TM_PROTOCOL_TYPE_TEMPLATE, 
			ALICE_HLT_PROXY_MASTER_TM_HOST_TEMPLATE, 
			ALICE_HLT_PROXY_MASTER_TM_DOMAIN_TEMPLATE,
			ALICE_HLT_PROXY_MASTER_TM_PORT_TEMPLATE};

	sprintf(insertNum, "%02d", number);
	address.clear();

	for (int i = 0; i < 4; i++) {
		// look for better string function to do this operation !!!
		properties[i][1] = insertNum[0];
        properties[i][2] = insertNum[1];
		
		// retrieve property value from PropertyReader
		if (!mpPR->retrievePropertyValue(&(properties[i]), &tmp)) {
			// !!! make log entry about ERROR
        	return 0;
    	}
    	address.append(*tmp);

		// check for adding "." or ":"
		switch (i) {
			case 1:
				address.append(".");
				break;
			case 2:
				address.append(":");
				break;
			default: // do nothing
				break;
		}
	} 
	
	tmAddress = new string(address);
	return tmAddress;
}


int TMproxy::sendTransitionCommand(char* command, Parameters* parameters) {
	int nRet = 0;
	string temp = command;

	if (!hasConnection()) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
				"[TaskManager-Proxy]: received transition command '" + temp +
				"', but has no connection to any TaskManager. Going to ERROR state.");
		cout << "TaskManager-Proxy received transition command '" << command <<
				"', but has no connection to any TaskManager. Going to ERROR state."
				<< endl;
		// set to ERROR state done in HLTProxy
		return ALICE_HLT_PROXY_TRANSITION_COMMAND_FAILED;
	}

	if (command == 0) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy received NULL pointer as command.");
		cout << "Received NULL pointer as command!" << endl;
		return ALICE_HLT_PROXY_NULL_POINTER;
	}

	ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY, 
			"TaskManager-Proxy received '" + temp + "' as transition command.");
	cout << "[TaskManager - proxy]: Received \"" << command << "\" command." << endl;

// moved the whole if part to HLT-proxy !!!
//	if (strcmp(command, "CONFIGURE") == 0) {
//		parameters->printParameters();
//
//		ConfigureParameters* confPar = dynamic_cast<ConfigureParameters*> (parameters);
//		if (confPar) {
//			nRet = confPar->replaceDefaultValues(mProperties.mDetectorListDefault,
//					mProperties.mBeamTypeDefault,
//					mProperties.mDataFormatVersionDefault,
//					mProperties.mHLTTriggerCodeDefault,
//					mProperties.mCTPTriggerDefault,
//					mProperties.mHLTInDDLListDefault,
//					mProperties.mHLTOutDDLListDefault);
//			cout << "[TaskManager - proxy]: Changed " << nRet << 
//					" CONFIGURE parameters to default value." << endl;
//			parameters->printParameters();
//			ProxyLogger::getLogger()->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
//					"TaskManager-Proxy has 'CONFIGURE' parameters after conversion: "
//					+ parameters->parToString(HLT_CONFIGURE_PARAMETER_SEPARATOR)
//					+ ".");
//		} else {
//			cout << "[TaskManager - proxy]: Unable to cast given Parameter object to ConfigureParamters."
//					<< endl;
//			ProxyLogger::getLogger()->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY,
//					"TaskManager-Proxy is unable cast given 'CONFIGURE' parameters in according object. Trying to use anyway: "
//					+ parameters->parToString(HLT_CONFIGURE_PARAMETER_SEPARATOR)
//					+ ".");
//		}
//		
//	} else if (strcmp(command, "ENGAGE") == 0) {
//		parameters->printParameters();
//	} else {
//		//strange command, at least something that is not defined yet or wrong call for command
//		return -2; // ??? change here to more meaningful
//	}

	relayCommand(command, 
			parameters->parToString(HLT_CONFIGURE_PARAMETER_SEPARATOR).c_str());
	return nRet;
}

int TMproxy::sendTransitionCommand(char* command) {
	int nRet = 0;
	string temp = command;

	if (!hasConnection()) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
				"[TaskManager-Proxy]: received transition command '" + temp +
				"', but has no connection to any TaskManager. Going to ERROR state.");
		cout << "TaskManager-Proxy received transition command '" << command <<
				"', but has no connection to any TaskManager. Going to ERROR state."
				<< endl;
		// set to ERROR state done in HLTProxy
		return ALICE_HLT_PROXY_TRANSITION_COMMAND_FAILED;
	}

	if (command == 0) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy received NULL pointer as command.");
		cout << "Received NULL pointer as command!" << endl;
		return ALICE_HLT_PROXY_NULL_POINTER;
	}

	ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY, 
			"TaskManager-Proxy received " + ProxyLogger::catos(command) +
			" as transition command.");
	cout << "[TaskManager - proxy]: Received \"" << command << "\" command." << endl;

	relayCommand(command);
	return nRet;
}


int TMproxy::relayCommand(char* command, const char* additionalData) {
	int adapterRetVal = 0;
	char* commandAssembly = 0;
	int nRet = 0;
	int retryCount;
	string translatedCommand;
	
	// loop over all master TMs to send transition command
	vector<MasterTM>::iterator it;
	for (it = mMasterTMCollection.begin(); it != mMasterTMCollection.end(); ++it) {
		retryCount = 0;
		// perform a check of the state first !!!

		if (it->isValid()) {		
			adapterRetVal = TMNamesAdapter::adaptHLTProxyCommand(command,
					translatedCommand, it->getTMState());
			if (adapterRetVal < 0) {
				ProxyLogger::getLogger()->createLogMessage(MSG_WARNING, 
						LOG_SOURCE_PROXY, "TaskManager-Proxy received unknown command ("
						+ ProxyLogger::catos(command) + ") for MasterTaskManager '" +
						it->getTMName() + "' - ignoring command.");
				continue;
			}
			if (adapterRetVal == HLT_SET_KILL_SLAVES_FLAG) {
				it->setKillSlavesFlag();
				cout << "TaskManager has set 'kill_slaves' flag to MasterTM." << endl;
			}
			if (additionalData != 0) {
				commandAssembly = new char[translatedCommand.length() + strlen(additionalData) + 2];
				// +2 -> separator sign ';' and NULL termination
#ifdef __EMULATOR
				commandAssembly[sprintf(commandAssembly, "%s%c%s", translatedCommand.c_str(),
							'\0', additionalData)] = 0; 
							// don't pass the parameters when using the emulator
#else
				commandAssembly[sprintf(commandAssembly, "%s;%s", translatedCommand.c_str(),
							additionalData)] = 0; // append conf parameters to command
#endif
			} else {
				commandAssembly	= new char[translatedCommand.length() + 1];
				strcpy(commandAssembly, translatedCommand.c_str());
			}
			do {
				nRet = it->sendCommand(commandAssembly);
				++retryCount;
				usleep(HLT_TM_SEND_COMMAND_RETRY_SLEEP);
			} while ((nRet != 0) && (retryCount < HLT_TM_SEND_COMMAND_RETRIES));
			if (nRet != 0) { 
				// unable to send command to MasterTM several times -> set TM to invalid
				it->setInvalid();
			}

			if (commandAssembly != 0) {
				delete[] commandAssembly;
			}
		}
	}
	return 0; // !!! change to more meaningful return value
}

bool TMproxy::signalNewState(const string newState) {

	// check if HLT-proxy is in error state, then TM states are ignored 
	if (strcmp(mpHLTproxy->getState(), HLT_PROXY_STATE_ERROR) == 0) {
		if (newState.compare("INITIALIZING") == 0) {
			return mpHLTproxy->informTMstateINITIALIZING();
		} else if (newState.compare("DEINITIALIZING") == 0) {
			return mpHLTproxy->informTMstateDEINITIALIZING();
		} else if (newState.compare("OFF") == 0) {
			return mpHLTproxy->informTMstateOFF();
		}  else if (newState.compare("INITIALIZED") == 0) {
			return mpHLTproxy->informTMstateINITIALIZED();
		}
		return true; 	
		// state change is ignored when HLT-proxy is in state ERROR and no recovering state is announced
	}
	
	if (newState.compare("INITIALIZING") == 0) {
		return mpHLTproxy->informTMstateINITIALIZING();

	} else if (newState.compare("INITIALIZED") == 0) {
		return mpHLTproxy->informTMstateINITIALIZED();

	} else if (newState.compare("CONFIGURING") == 0) {
		return mpHLTproxy->informTMstateCONFIGURING();

	} else if (newState.compare("CONFIGURED") == 0) {
		return mpHLTproxy->informTMstateCONFIGURED();

	} else if (newState.compare("ENGAGING") == 0) {
		return mpHLTproxy->informTMstateENGAGING();

	} else if (newState.compare("READY") == 0) {
		return mpHLTproxy->informTMstateREADY();

	} else if (newState.compare("STARTING") == 0) {
		return mpHLTproxy->informTMstateSTARTING();

	} else if (newState.compare("RUNNING") == 0) {
		return mpHLTproxy->informTMstateRUNNING();

	} else if (newState.compare("COMPLETING") == 0) {
		return mpHLTproxy->informTMstateCOMPLETING();

	} else if (newState.compare("DISENGAGING") == 0) {
		return mpHLTproxy->informTMstateDISENGAGING();

	} else if (newState.compare("DECONFIGURING") == 0) {
		return mpHLTproxy->informTMstateDECONFIGURING();

	} else if (newState.compare("OFF") == 0) {
		return mpHLTproxy->informTMstateOFF();

	} else if (newState.compare("DEINITIALIZING") == 0) {
		return mpHLTproxy->informTMstateDEINITIALIZING();	

	} else if (newState.compare("ERROR") == 0) {
		return mpHLTproxy->informTMstateERROR();	

	} else {
		cout << "[TaskManager - proxy]: Unknown state of master TM, switching to ERROR state ..." 
				<< endl;
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
					"TaskManager-Proxy wants to signal unknown state.");
		return false;
	}
}

int TMproxy::setTMsToErrorEquivalentState() {
	int number = 0;

	// loop over all master TMs to set them in error equivalent state
	vector<MasterTM>::iterator it;
	for (it = mMasterTMCollection.begin(); it != mMasterTMCollection.end(); ++it) {

		// perform a check of the state first !!!
		if ((it->isValid()) && (it->getTMState().compare("ERROR") != 0) && 
				(it->getTMState().compare("OFF") != 0)) {
			if (it->setToErrorEquivalentState()) {
				++number;

				ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG,
						LOG_SOURCE_PROXY, "TaskManager-Proxy has set '" + 
						it->getTMName() + "' to error equivalent state.");
				cout << "[TaskManager-Proxy]: " << it->getTMName() << 
						" has been set to error equivalent state." << endl;
			}
		}
	}

	return number;
}

bool TMproxy::updatePropertiesFromFile() {
	bool bRet = false;
	mpPR = PropertyReader::getPropertyReader();
	char* propFileName = 0;
	string* tempLog = 0;
	unsigned int tempLevel = 127;
	int nRet = 0;

	propFileName = getenv("HLT_PROXY_PROPERTY_FILE");
	if (propFileName == 0) {
		propFileName = HLT_PROXY_PROPERTY_FILENAME;
	}
	// update values from file
	if (!mpPR->readPropertiesFromFile(propFileName)) {
		ProxyLogger::getLogger()->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy cannot update values from property file '"
				+ ProxyLogger::catos(propFileName) + "', using old values.");
		cout << "[TaskManager-Proxy] Unable to update values from property file, using old values." 
				<< endl;
		return bRet;
	}

	// update log level
	if (mpPR->retrievePropertyValue(ALICE_HLT_PROXY_PROPERTY_LOG_LEVEL, &tempLog)) {
		sscanf(tempLog->c_str(), "%d", &tempLevel);
		if (tempLevel != ProxyLogger::getLogger()->getLogLevel()) {
			ProxyLogger::getLogger()->setLogLevel(tempLevel);
		}
	} else {
		ProxyLogger::getLogger()->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy cannot retrieve log level update, keeping current level.");
		cout << "ERROR while updating log level property, remaining with old one." 
				<< endl;
	}

	// update the properties for the defaults for command parameters
	nRet = mpPR->setHLTProxyProperties(&mProperties);
	if (nRet < 0) {
		ProxyLogger::getLogger()->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy cannot update properties for command parameter defaults in HLT-Proxy");
		cout << "[TaskManager-Proxy]: Unable to update properties for command parameter defaults, using old ones."
				<< endl;
		if (nRet < -1) {
			ProxyLogger::getLogger()->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY,  
					"Unable to update properties, Property-file has wrong version number, required: '" 
					+ mProperties.mVersionNumber + "' - used file:" + 
					ProxyLogger::catos(propFileName));
			cout << "ERROR: Property file has wrong version number; required is version: '"
					<< mProperties.mVersionNumber << "', used file:"
					<< ProxyLogger::catos(propFileName) << endl;
			cout << " -> Use correct property-file and restart HLT-ECS-proxy!" << endl;
		}
		return bRet;
	} else {
		
		ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY,  
				"Updated '" + ProxyLogger::itos(nRet) + 
				"' default command parameters from Property-file (incl. polling sleep)."); 
		if (mpHLTproxy->isDebugOn()) {
			cout << "[TaskManager-Proxy]: Updated '" << nRet <<
					"' default command parameters from Property-file (incl. polling sleep)."
					<< endl;
			cout << "[DEBGU] - HLT-Properties:" << endl;
			cout << " [DEBGU] Polling sleep time:" << 
					mProperties.mTMPollingSleep << endl;
			cout << " [DEBUG] Detector-List: " << 
					mProperties.mDetectorListDefault << endl;
			cout << " [DEBUG] HLT-Mode: " << 
					mProperties.mPossibleHLTMode << endl;
			cout << " [DEBUG] BeamType: " << 
					mProperties.mBeamTypeDefault << endl;
			cout << " [DEBUG] DataFormat: " << 
					mProperties.mDataFormatVersionDefault << endl;
			cout << " [DEBUG] HLT-TriggerClasses: " << 
					mProperties.mHLTTriggerCodeDefault << endl;
			cout << " [DEBUG] CTP-TriggerClasses: " << 
					mProperties.mCTPTriggerDefault << endl;
			cout << " [DEBUG] ListInDDL: " << 
					mProperties.mHLTInDDLListDefault << endl;
			cout << " [DEBUG] ListOutDDL: " << 
					mProperties.mHLTInDDLListDefault << endl;
			cout << " [DEBUG] RunType: " << 
					mProperties.mRunTypeDefault << endl << endl;
		}

		bRet = true;
	}
	
	// update polling time delay -> protected by mutex
	lockSleepMutex();
	mPollingSleep = mProperties.mTMPollingSleep;
	unlockSleepMutex();
	
	return bRet;
}


string TMproxy::convertBinAddressToName(void* processAddress_bin) {
	string convertedAddress("Unknown address");

	vector<MasterTM>::iterator it;
	for (it = mMasterTMCollection.begin(); it != mMasterTMCollection.end(); ++it) {
		if (CompareAddresses(mpInterfaceLib_data, processAddress_bin, 
				it->getTMaddress_bin()) == 0) {
			convertedAddress = it->getTMName();
			break;
		}
	}
	ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY,
			"TaskManager-Proxy has converted binary address to '" + 
			convertedAddress + "'.");
	return convertedAddress;
}

void TMproxy::lockSleepMutex() {
	int status = 0;
	status = pthread_mutex_lock(&mSleepMutex);
	if (status != 0) {
		cout << "Lock sleep time mutex error: " << status << endl;
	}
}

void TMproxy::unlockSleepMutex() {
	int status = 0;
	status = pthread_mutex_unlock(&mSleepMutex);
	if (status != 0) {
		cout << "Unlock sleep time mutex error: " << status << endl;
	}
}


//////////////////////// -> Request State - Thread <- ////////////////////////
void* TMproxy::requestTMStateThread(void* threadParam) {
	TMproxy* tmProxy = (TMproxy*) threadParam;
	string currentState;
	string newState;
	int nRet = 0;
	unsigned long sleepTime = HLT_REQUEST_STATE_THREAD_SLEEP;

	cout << "[TaskManager-Proxy]: Request thread started ..." << endl;
	ProxyLogger::getLogger()->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"TaskManager-Proxy has started MasterTaskManager request thread.");

	// protect with mutex
	tmProxy->lockSleepMutex();
	sleepTime = tmProxy->mPollingSleep;
	tmProxy->unlockSleepMutex();

	nRet = usleep(sleepTime);
	if ((nRet != 0) && (errno == EINVAL)) {
		sleepTime = HLT_MAX_SLEEP_TIME;
	}

	// NOW dummy implementation to check the state of the master TM and waiting
	// for correct changes, this has to changed soon !!!
	// loops again to check for state change (for now assuming only one masterTM)
	vector<MasterTM>::iterator it;
	while (true) {	
		for (it = tmProxy->mMasterTMCollection.begin(); it != tmProxy->mMasterTMCollection.end(); ++it) {
			// perform a check of the state first !!!

			// request state
			nRet = it->queryState(newState);

			if (nRet == MASTER_TM_HAS_NEW_STATE) {
				if (tmProxy->signalNewState(newState)) {
					ProxyLogger::getLogger()->createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY, 
						"TaskManager-Proxy signaled state change for " +
						it->getTMName() + " to " + newState + " has been successfully.");
				} else {
					// set REAL MasterTM (node) as well to error
					it->setToErrorEquivalentState(); // ??? maybe check retval
					// temporarily assuming only one master TM
					cout << "[HLT - proxy]: WRONG state for transition ! Going to ERROR state..." 
								<< endl;
					ProxyLogger::getLogger()->createLogMessage(MSG_ALARM, LOG_SOURCE_PROXY, 
							"TaskManager-Proxy has received invalid state transmission, switching to ERROR state.");

					// !!! only now as long as only one MasterTM and no evaluator
					tmProxy->mpHLTproxy->informTMstateERROR();

					// TODO: continue here with evaluator when implemented
				}
			} else if (nRet == MASTER_TM_INVALID_STATE_NAME) {
				// going to state error -> this should always work !!
				tmProxy->signalNewState(newState);
				ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY,
                		"TaskManager-Proxy signaled state change of " +
                        it->getTMName() + " to " + newState + 
						", due to invalid state name of Master TaskManager.");

			}  	// if - newState 
				// (if state is "MASTER_TM_EMPTY_STATE_STRING" just ignore request)

		} // for - loop

		// setting of polling sleep and sleep
		tmProxy->lockSleepMutex();
		sleepTime = tmProxy->mPollingSleep;
		tmProxy->unlockSleepMutex();
		nRet = usleep(sleepTime);
		if ((nRet != 0) && (errno == EINVAL)) {
			sleepTime = HLT_MAX_SLEEP_TIME;
		}

		if (tmProxy->mpHLTproxy->isDebugOn()) {
			cout << "[TaskManager-Proxy]: Request thread sleep ..." << endl;
		}
	} // while-loop
}


//////////////////////// -> InterruptWait - Thread <- ////////////////////////
void* TMproxy::interruptWaitThread(void* threadParam) {
	TMproxy* tmProxy = (TMproxy*) threadParam;
	int nRet = 0;

	cout << "[TaskManager-Proxy]: InterruptWait thread started ..." << endl;
	ProxyLogger::getLogger()->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"TaskManager-Proxy has started the InterruptWait thread for the InterfaceLib.");

	nRet = InterruptWait(tmProxy->mpInterfaceLib_data, (void*) threadParam,
			tmProxy->mpLocal_listen_address, interruptCallBack);
	if (nRet != 0) {
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy call for InterruptWait() of InterfaceLib failed - Error: '"
				+ ProxyLogger::errnotos(nRet) + "' - LocalListenAddress: '" +
				ProxyLogger::catos(tmProxy->mpLocal_listen_address) + "'.");
		cout << "[TaskManager-Proxy]: ERROR while calling InterruptWait() of InterfaceLib: " << 
				ProxyLogger::errnotos(nRet) << "." << endl;
		return &msInterrupt_Wait_Error;
	}

	ProxyLogger::getLogger()->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"TaskManager-Proxy exits thread for InterruptWait of InterfaceLib.");
	return &msInterrupt_Wait_Clean_Exit;
}


////////////////////// -> Interrupt Callback Routine <- //////////////////////
void TMproxy::interruptCallBack(void *opaque_arg, pid_t pid,
		const char *process_id, void *process_address_bin,
		const char *notificationData) {
	TMproxy* tmProxy = (TMproxy*) opaque_arg;
	string triggerProcessAddress = tmProxy->convertBinAddressToName(process_address_bin);

	cout << "[TaskManager-Proxy]: Interrupt callback routine called ..." << endl;
	ProxyLogger::getLogger()->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
			"InterruptCallback Routine of the TaskManager-Proxy has been called by '"
			+ triggerProcessAddress + "' - additional Data: '" +
			ProxyLogger::catos(notificationData) + "'.");
	// some more stuff ....
}


bool TMproxy::hasConnection() {
	bool bRet = false;
	
	// loop over all subscribed TaskManager
	vector<MasterTM>::iterator it;
	for (it = mMasterTMCollection.begin(); it != mMasterTMCollection.end(); ++it) {
		if (it->isConnected()) {
			bRet = true;
			break;
		}
	}

	if (!bRet) {
		ProxyLogger::getLogger()->createLogMessage(MSG_WARNING, 
				LOG_SOURCE_PROXY, "[TaskManager-Proxy]: No connection to any TaskManager.");
	}
	return bRet;
}
