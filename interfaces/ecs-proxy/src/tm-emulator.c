/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <iostream>
#include <cstring>
#include <pthread.h>
#include <unistd.h>

#include "InterfaceLib.h"
#include "hlt_proxy_state_defines.h"


#define STATE_OFF HLT_TASKMANAGER_STATE_OFF
#define STATE_INITIALIZING HLT_TASKMANAGER_STATE_INITIALIZING
#define STATE_INITIALIZED HLT_TASKMANAGER_STATE_INITIALIZED
#define STATE_CONFIGURING HLT_TASKMANAGER_STATE_CONFIGURING
#define STATE_CONFIGURED HLT_TASKMANAGER_STATE_CONFIGURED
#define STATE_ENGAGING HLT_TASKMANAGER_STATE_ENGAGING
#define STATE_READY HLT_TASKMANAGER_STATE_READY
//#define STATE_STARTING HLT_TASKMANAGER_STATE_STARTING  // -> see  STATE_STARTING_RUNNING
#define STATE_RUNNING HLT_TASKMANAGER_STATE_RUNNING
#define STATE_BUSY HLT_TASKMANAGER_STATE_BUSY
#define STATE_PAUSED HLT_TASKMANAGER_STATE_PAUSED
#define STATE_COMPLETING HLT_TASKMANAGER_STATE_COMPLETING
#define STATE_DISENGAGING HLT_TASKMANAGER_STATE_DISENGAGING
#define STATE_DEINITIALIZING HLT_TASKMANAGER_STATE_DEINITIALIZING
#define STATE_ERROR HLT_TASKMANAGER_STATE_ERROR

//new states
#define STATE_DECONFIGURING HLT_TASKMANAGER_STATE_DECONFIGURING
#define STATE_STARTING_RUNNING HLT_TASKMANAGER_STATE_START_RUNNING


#define COMMAND_INITIALZE "start_slaves"
#define COMMAND_CONFIGURE "start"
#define COMMAND_ENGAGE "connect"
#define COMMAND_START "start_run"
#define COMMAND_STOP "stop_run"
#define COMMAND_DISENGAGE "disconnect"
#define COMMAND_RESET "stop"
#define COMMAND_SHUTDOWN "kill_slaves"
#define COMMAND_RECOVER "kill_processes"

#define COMMAND_ACTIVE "set_active"
#define COMMAND_PASSIVE "set_passive"

using namespace std;


/// NOTE: this file is only to emulate the InterfaceLib and a
// MasterTaskManager, the production system cannot run with that !!!

static string EM_state = STATE_OFF;
static char EM_state_char[20] = STATE_OFF;
static char EM_command[20];

pthread_mutex_t EM_state_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_t EM_thread_handle;



void set_state(string new_state, string old_state = "") {
	pthread_mutex_lock(&EM_state_mutex);
	if ((old_state.compare("") == 0) || (old_state.compare(EM_state) == 0)) {
		EM_state = new_state;
	} 	
	pthread_mutex_unlock(&EM_state_mutex);
}


void emulator(const char* state) {
	//check command
	if (strcmp(state, COMMAND_INITIALZE) == 0) {
		cout << "[TaskManager - proxy - emulation]: Initializing cluster ..." << endl;
		set_state(STATE_INITIALIZING);
		sleep(5);
		cout << "[TaskManager - proxy - emulation]: DONE !" << endl;
		set_state(STATE_INITIALIZED);

	} else if (strcmp(state, COMMAND_CONFIGURE) == 0) {
		cout << "[TaskManager - proxy - emulation]: Configuring cluster ..." << endl;
		set_state(STATE_CONFIGURING);
		sleep(10);
		cout << "[TaskManager - proxy - emulation]: DONE !" << endl;
		set_state(STATE_CONFIGURED);

	} else if (strcmp(state, COMMAND_SHUTDOWN) == 0) {
		cout << "[TaskManager - proxy - emulation]: Ramping down cluster ..." << endl;
		set_state(STATE_DEINITIALIZING);
		sleep(5);
		cout << "[TaskManager - proxy - emulation]: DONE !" << endl;
		set_state(STATE_OFF);

	} else if (strcmp(state, COMMAND_RESET) == 0) {
		cout << "[TaskManager - proxy - emulation]: Reseting system (Initializing) ..." << endl;
		set_state(STATE_DECONFIGURING);
		sleep(5);
		cout << "[TaskManager - proxy - emulation]: DONE !" << endl;
		set_state(STATE_INITIALIZED);

	} else if (strcmp(state, COMMAND_ENGAGE) == 0) {
		cout << "[TaskManager - proxy - emulation]: Engaging cluster nodes ..." << endl;
		set_state(STATE_ENGAGING);
		sleep(7);
		cout << "[TaskManager - proxy - emulation]: DONE !" << endl;
		set_state(STATE_READY);

	} else if (strcmp(state, COMMAND_START) == 0) {
		cout << "[TaskManager - proxy - emulation]: Starting HLT - tasks (data analysis) ..." << endl;
		set_state(STATE_STARTING_RUNNING);
		sleep(2);

		cout << "[TaskManager - proxy - emulation]: Emulating 'Running' !" << endl;
		set_state(STATE_RUNNING);
		sleep(6);

		cout << "[TaskManager - proxy - emulation]: Emulating 'Paused' (ignored if already in 'COMPLETING')!" 
				<< endl;
		set_state(STATE_PAUSED, STATE_RUNNING);
		sleep(8);

		cout << "[TaskManager - proxy - emulation]: Emulating 'Busy' (ignored if already in 'COMPLETING')!" 
				<< endl;
		set_state(STATE_BUSY, STATE_PAUSED);
		sleep(7);

		set_state(STATE_RUNNING, STATE_BUSY);
		cout << "[TaskManager - proxy - emulation]: DONE ('Running') (ignored if already in 'COMPLETING')!" 
			<< endl;

	} else if (strcmp(state, COMMAND_STOP) == 0) {
		cout << "[TaskManager - proxy - emulation]: Stoping data analysis ..." << endl;
		set_state(STATE_COMPLETING);
		sleep(6);
		cout << "[TaskManager - proxy - emulation]: DONE !" << endl;
		set_state(STATE_READY);

	} else if (strcmp(state, COMMAND_DISENGAGE) == 0) {
		cout << "[TaskManager - proxy - emulation]: Disengaging cluster nodes ..." << endl;
		set_state(STATE_DISENGAGING);
		sleep(3);
		cout << "[TaskManager - proxy - emulation]: DONE !" << endl;
		set_state(STATE_CONFIGURED);
		sleep(3);
//		set_state(STATE_ERROR);

	} else if (strcmp(state, COMMAND_RECOVER) == 0) {
		cout << "[TaskManager - proxy - emulation]: Recovering after ERROR state ..." << endl;
		set_state(STATE_INITIALIZING);
		sleep(3);
		cout << "[TaskManager - proxy - emulation]: DONE !" << endl;
		set_state(STATE_INITIALIZED);
		
/*
	} else if (strcmp(state, "DEINITIALIZING") == 0) {
		cout << "[TaskManager - proxy - emulation]: Deinitializing cluster..." << endl;
//		set_state(STATE_DEINITIALIZING);
		sleep(2);
		cout << "[TaskManager - proxy - emulation]: DONE !" << endl;
		set_state(STATE_OFF);
*/
	// commands for ACTIVE / PASSIVE
	} else if (strcmp(state, COMMAND_ACTIVE) == 0) {
		cout << "[TaskManager - proxy - emulation]: Received 'SET_ACTIVE' command ..." << endl;

	} else if (strcmp(state, COMMAND_PASSIVE) == 0) {
		cout << "[TaskManager - proxy - emulation]: Received 'SET_PASSIVE' command ..." << endl;

	} else {
		cout << "[TaskManager - proxy - emulation]: Unknown command, switching to ERROR state ..." << endl;
		set_state(STATE_ERROR);
	}
}

void* emulator_thread(void* threadParam) {
	emulator(EM_command);
	return 0;
}

/* Send the specified command to the given process */
int SendCommand( void* private_lib_data,
		 pid_t pid,
		 const char* process_id,
		 void* process_address_bin,
		 const char* command ) {

	int nRet = 0;
	pthread_attr_t threadAttr;
	strcpy(EM_command, command);

	nRet = pthread_attr_init(&threadAttr);
	if (nRet != 0) {
		cout << "EMULATE thread attribute error: " << nRet << endl;
	}
	nRet = pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_DETACHED);
	if (nRet != 0) {
		cout << "Set thread attribute error: " << nRet << endl;
	}
	nRet = pthread_create(&EM_thread_handle, &threadAttr, &emulator_thread, 0);
	if (nRet != 0) {
		cout << "Create thread to request TM states error: " << nRet << endl;
		/*
		ProxyLogger::getLogger()->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
				"TaskManager-Proxy cannot create thread for MasterTaskManager requests - Error: ...");
		*/
	}
	nRet = pthread_attr_destroy(&threadAttr);
	if (nRet != 0) {
		cout << "Destroy thread attribute error: " << nRet << endl;
	}
	return 0;
}


/* Query the state of a specific process
** state is allocated as a string in the function and
** has to be released via ReleaseResource (below) */
int QueryState( void* private_lib_data,
		pid_t pid,
		const char* process_id,
		void* process_address_bin,
		char** state ) {
	pthread_mutex_lock(&EM_state_mutex);
	strcpy(EM_state_char, EM_state.c_str());
	*state = EM_state_char;
	pthread_mutex_unlock(&EM_state_mutex);
	return 0;
}






static void* EM_lib_data = 0;
static void* EM_process_address_bin = 0;
static char* EM_state_data = 0;

static bool EM_gRun = true;
static bool EM_gRunning = false;


int Initialize( void** private_lib_data,
    			const char* interrupt_listen_address ) {
	EM_lib_data = new char[2];
	*private_lib_data = EM_lib_data;
	return 0;
}

/* End, cleanup of library data */
int Terminate( void* private_lib_data ) {
	if (EM_lib_data !=0) {
		delete[] (char*) EM_lib_data;
		private_lib_data = 0;
		return 0;
	}
	return -1;
}

/* Convert a given address string into a more compact binary
   representation for immediate usability.
   This can also be a copy of the given string if this is the most directly
   useful representation.
   The returned binary address will be released by a call to
   ReleaseResource (below) */
int ConvertAddress( void* private_lib_data, const char* process_address_str,
			void** process_address_bin ) {
	if (EM_process_address_bin == 0) {
		EM_process_address_bin = new char[2];
	}
	*process_address_bin = EM_process_address_bin;
	return 0;
}

/*
  Compare two given binary address representations.
  Return 0 if they are equal, -1 if addr1 is less than addr2,
  +1 if addr1 is greater than addr2. If the magnitude comparisons
  do not apply return unequal 0 for inequality. */
int CompareAddresses( void* private_lib_data, void* addr1, void* addr2 ) {
	return 0;
}



/* Obtain additional status data from a
** specific process.
** stat_data is allocated as a string in the
*+ function and has to be released via
** ReleaseResource (below)
*/
int QueryStatusData( void* private_lib_data,
		 pid_t pid,
		 const char* process_id,
		 void* process_address_bin,
		 char** stat_data ) {
	if (EM_state_data == 0) {
		EM_state_data = new char[2];
	}
	*stat_data = EM_state_data;


	return 0;
}

void ReleaseResource( void* private_lib_data, void* resource ) {
	return;
}

/* Callback function for the interrupt wait function below.
** The last three parameters are used to identify the process
** that triggered the interrupt. Only one of them (whichever
** suits the library implementation best) has to present. */
/*
typedef void (*InterruptTriggerCallback_t)( void* opaque_arg,
					pid_t pid,
					const char* process_id,
					void* process_address_bin,
					const char* notificationData);
*/


/* Enter a wait that calls the specified callback when a process
** has triggered an interrupt  (LAM).
** The address of the triggering process is passed in the call
** to the specified callback function.
** This is called in its own thread to run as an endless loop
** and only returns when the StopInterruptWait function
** (see below) is called.. */
int InterruptWait( void* private_lib_data,
		   void* opaque_arg,
		   const char* listen_address,
		   InterruptTriggerCallback_t callback ) {
	EM_gRun = true;
	EM_gRunning = true;
	while ( EM_gRun ) {
		sleep( 1 );
	}
	EM_gRunning = false;
	return 0;
}

/* Stop the interrupt wait loop gracefully. */
int StopInterruptWait( void* private_lib_data ) {
	EM_gRun = false;
	while ( EM_gRunning ) {
		sleep( 1 );
	}
	return 0;
}

/* Connection function.
   Establish a permanent connection to the given process.
   This function is optional and need not be supplied in the interface
   library for compatibility reasons.
*/
int ConnectToProcess( void* private_lib_data,
		  pid_t pid,
		  const char* process_id,
		  void* process_address_bin ) {

	return 0;
}

/* Disconnection function.
   Severe a previously established permanent connection to the given
   process.
   This function is optional and need not be supplied in the interface
   library for compatibility reasons.
*/
int DisconnectFromProcess( void* private_lib_data,
			   pid_t pid,
			   const char* process_id,
			   void* process_address_bin ) {

	return 0;
}

