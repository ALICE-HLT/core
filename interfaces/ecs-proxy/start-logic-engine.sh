#!/bin/bash

#/************************************************************************
#**
#**
#** This file is property of and copyright by the Department of Physics
#** Institute for Physic and Technology, University of Bergen,
#** Bergen, Norway, 2006
#** This file has been written by Sebastian Bablok,
#** sebastian.bablok@ift.uib.no
#**
#** Important: This file is provided without any warranty, including
#** fitness for any particular purpose.
#**
#**
#*************************************************************************/

# bash version of the start logic engine script

## This script prepares the logic engine for the HLT-proxy and starts it.
## Afterwards it monitors its process. Should the HLT-proxy logic engine
## process die, the script restarts the logic engine automatically. Only if
## this script receives an interrrupt signal, it kills the logic engine and
## exits afterwards.

# set environment
#. .setup-all.sh

# Here is my state machine (logic engine)
# translate for logic engine ---- moved to the makefile
#smiTrans ./src/hlt-states.smi
#mv ./src/hlt-states.sobj ./bin/hlt-states.sobj

# set deafult for domain name
DomainName="ALICE_HLT"

# set the desired domain name, if provided
if [ $1 ]; then

	DomainName="$1"
	
fi

echo "   Domain name is set to $DomainName."
echo "   Starting logic engine now ..."
echo ""


# start logic engine
smiSM $DomainName ./bin/hlt-states.sobj &
SMpid=$!

# signal handler function
function CleanUp {
	if [ $SMpid ]; then
    	kill $SMpid
    	echo "Killed smiSM on PID $SMpid."
	fi
	echo "Exiting HLT-proxy logic engine."
	exit 0
}


#catch interupt signals and jump to clean up
trap 'CleanUp' 2 3 15  # catch SIGINT, SIGQUIT and SIGTERM

echo "HLT-proxy logic engine has PID $SMpid."
echo

sleep 5

#start monitor of smi SM process to restart in case of missing process
while true
do 
	if [ ! "`ps -C 'smiSM $DomainName ./bin/hlt-states.sobj' -o pid=`" ] ; then 
		#secure CleanUp, in case restart fails
		SMpid=0 		
		echo "HLT-proxy logic engine no longer running, trying restart ..."
		smiSM $DomainName ./bin/hlt-states.sobj &
		SMpid=$!
		echo "HLT-proxy logic engine has PID $SMpid."
		echo

	fi

	sleep 5

done


