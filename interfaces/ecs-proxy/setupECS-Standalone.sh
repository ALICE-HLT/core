#!/bin/bash
# 
# * Set DIM env variables for standalone running
#
# File   : start_ECS-Standalone.sh
#
# Author : Jochen Thaeder  thaeder _at_ kip.uni-heidelberg.de
# Date   : 12.01.2009
#
###################################################################

HOSTNAME=`hostname -s`

DETECTOR=${DETECTOR_CONTROL}

if [ "$HOSTNAME" == "portal-ecs0" ] ; then
    HOST_IP="10.162.48.101"
elif [ "$HOSTNAME" == "portal-ecs1" ] ; then
    HOST_IP="10.162.48.102"
else
    HOST_IP="127.0.0.1"
fi

if [ "${DETECTOR}" == "DEV" ] ; then
    # "DAQ-SIDE" - portal-dev (msdev0)
    export DIM_DNS_NODE=10.162.13.111
    # "HLT-SIDE" - portal-dev (msdev0)
    export DIM_DNS_HOST=10.162.13.111
elif [ "${DETECTOR}" == "TEST" ] ; then
    # "DAQ-SIDE"
    export DIM_DNS_NODE=${HOST_IP}
    # "HLT-SIDE" 
    export DIM_DNS_HOST=${HOST_IP}
else
    # "DAQ-SIDE"
    export DIM_DNS_NODE=${HOST_IP}
    # "HLT-SIDE" 
    export DIM_DNS_HOST=${HOST_IP}
fi
