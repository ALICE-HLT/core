#/************************************************************************
#**
#**
#** This file is property of and copyright by the Department of Physics
#** Institute for Physic and Technology, University of Bergen,
#** Bergen, Norway, 2006
#** This file has been written by Sebastian Bablok,
#** sebastian.bablok@ift.uib.no
#**
#** Important: This file is provided without any warranty, including
#** fitness for any particular purpose.
#**
#**
#*************************************************************************/

                -->         VERSION 1.5.4       <--


NOTE: Major change in VERSION 1.1.0: Two CONFIGURE Parameters have been
moved to the ENGAGE command - HLT_MODE and RUN_NUMBER are now ENGAGE
Parameters. As well a new CONFIGURE parameter has been introduced: RUN_TYPE,
which can receive also DEFAULT to set the default value from the HLT-proxy
properties. Unfortunately this implied, that this new version is no longer
backwards compatible in the interface to ECS!!! 
So, be sure which version is used.

-> Therefore no real version 1.0 has been released. The planned release of
VERSION 1.0 has become obsulete bye this major change!!!

NOTE: Version 0.9 includes new SMI++ (v32r4) and DIM (v16r13) version. No
other changes have been made.

NOTE: Since version 0.8, the scripts have been rewritten in bash. The old
tcshell scripts have been renamed to ".tcsh". The usage of the new scripts
is similar to the usage of the tcshell ones, except of the start script of
hlt-proxy ('start-hlt-proxy.sh'). Type './start-hlt-proxy.sh -h' to print 
out the new usage.

This file describes very briefly how to build the second draft of the 
ALICE HTL proxy.

1) Requirement & Preparations:
==============================
- DIM
- SMI++
- TaskManager InterfaceLib (AliHLT)
- xerces

Build the DIM and SMI framework in the "dim" and "smi" folders according
to the description mentioned there.

NOTE: The following is only required, if plan to use a real Master 
TaskManager of the HLT cluster system. There exists now a emulator, 
which you can use instead for testing the Proxy only. If you plan to
use this emulator, you have to call 'make emulate' instead of 'make' 
(see below).

In addition you need to build the InterfaceLib of the AliHLT TaskManager
system. This can be acquired via the HLT-wikipages of the KIP in 
Heidelberg (Germany):
http://www.kip.uni-heidelberg.de/wiki/HLT/index.php/Software-Download
(Login required)
To build the needed libraries of the TaskManager, you need to build the
xerces lib first:
http://xml.apache.org/xerces-c/download.cgi
(see these webpages for the building the xerces lib.)
After building the xerces libs, export the XERCESCDIR to the xerces root
folder (folder, that hosts the 'lib/' -folder as well as the 'include/' 
-folder) and include the xerces lib folder (${XERCESCDIR}/lib) in the
LD_LIBRARY_PATH :

If you are using bash:
  export XERCESCDIR=<xerces_root_dir>
  export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${XERCESCDIR}/lib:

when using tcsh:
  setenv XERCESCDIR <xerces_root_dir>
  setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:${XERCESCDIR}/lib:


When all these packages are created, copy the following libraries in the 
'lib/' directory of the HLT-Proxy (this directory is in the same directory
like this ReadMe):
libBCL.so.0.5.3
libMLUC.so.0.5.1
SlaveControlInterfaceLib.so
TMLib.so
libxerces-c.so.27.0

You can also avoid to copy these libraries, when you change the variables
declared in the 1. Section of the .setup-all.sh script:
TASK_MANAGER_LIBS <path to the TaskManager libraries>
XERCES_C_LIB <path to the XERCES-C library>

When changing these variables, you can also adapt the var DIM_DNS_NODE
to where your Dim Name Server is running.

The required links to different libs (aliases) are created when running
the setup-script. (If libBCL, libMLUC or libxerces-c have other version
numbers, take the new versions and modify the setup-script to correct the
link creation.)

The following description assumes the usage of a tcsh!


2) Intial setup:
================
Go back to the parent folder and set the environmental variable for the
DIM_DNS_NODE in the setup file .setup-all.sh (.setup-all.tcsh).

Then set the other setup with:

for bash:
    $ . .setup-al.sh

for tcsh:
    $ source .setup-all.tcsh


3) Build process:
=================
To build the HLT proxy:

    make

To build the HLT proxy with an emulated InterfaceLib and one emulated
MasterTaskManager (this doesn't require all the above mentioned libs
for contacting the MasterTaskManager). If you using this, make sure that
the property file has only entries for one MaterTaskManager. This 
version is proposed for testing:

    make emulate


To create the test GUI, which emulates an ECS panel to control the proxy:

    make gui

To generate the documentation of the source code (using doxygen):

    make docu
		
The documentation can be found then in the doc - folder, adaptions in the
configuration for doxygen can be made in the file "Doxyfile".

To clean up the build and documentation:
	
	make clean

Some other possible make commands are:
    make unix               -> converts source files to unix format
    make package            -> produces a tarball of the whole package
                               (This implictely cleans up the Dim and SMI dir,
                               you have to re"make" them; see below!)
    make docu-dist          -> extracts the documentation and 
                               makes a tarball of it
    make clean-dim          -> cleans up Dim (Note: you have to re"make" Dim
                               afterwards with "cd dim; make; cd ..;")
    make clean-dim          -> cleans up SMI (Note: you have to re"make" SMI
                               afterwards with "cd smi; make; cd ..;")


4) Start of the State-Machine:
==============================
If you want to have real contact to (a/the) MasterTaskManager(s), you have 
to set up and start the desired MasterTaskManager first and adapt the 
"Properties.txt" file to the appropriated settings. The name for the proxy
logfile is taken from the environmental variable "HLT_PROXY_LOGFILE_NAME",
if it is empty, the proxy creates a log file with the name "HLT-Proxy.log".
The starting loglevel is (MSG_INFO + MSG_WARNING + MSG_ERROR + MSG_ALARM +
MSG_DEBUG), but as soon as the property file is read, the log level is 
changed the setting there. (If you are using the emulator of the Master
TaskManager, make sure only one is mentioned in the property file.)


Remember to start the DIM dns on the mentioned node (see DIM_DNS_NODE).
To start the logic engine (state machine) - the starting script now also
monitors the logic engine and restarts it if its process dies (killing
the starting script will kill as well the logic engine - the Domain_Name
parameter is optional, see desription of start-hlt-proxy.sh script below):

bash:
    $ ./start-logic-engine.sh [Domain_Name] 

tcsh:
    $ ./start-logic-engine.tcsh [Domain_Name]

To start the HLT proxy (the passing of a Domain_Name parameter is optional,
if none is provided the default ["ALICE_HLT"] is taken):
NOTE: If not using the default domain name, the same domain name has to be
provided to the logic engine and the test gui as well! For the bash script
'--domain ' has to be put in advance of the 'Domain_Name'; but only for 
that script!
NOTE: Since version 1.5 an additional environment variable is needed:
'ALIHLT_PROXY_MOD'. It defines if the started proxy is the real or the backup
system. Has to be set to 'HLT' for the real system and to 'HLT_BCK' for the
failover setup.

bash:
    $ ./start-hlt-proxy.sh <Parameters>
(See usage ('./start-hlt-proxy.sh -h') for details on Parameters)

tcsh:
    $ ./start-hlt-proxy.tcsh [Domain_Name]


To start the test panel (GUI - the Domain_Name parameter is optional, see
desription of start-hlt-proxy.sh script above; the Module_Name defines, if 
the GUI shall contact the real proxy or the backup setup. 'HLT' is used for
the real, 'HLT_BCK' for the backup. If no 2. parameter is given, 'HLT' is 
used. If [Module_Name] is used, then [Domain_Name] has to be given first.):

bash:
    $ ./start-test-gui.sh [Domain_Name] [Module_Name]

tcsh:
	$ ./start-test-gui.tcsh [Domain_Name] [Module_Name]


5) Writing an own start script:
===============================
The HLT proxy allows now the following environmental settings to be given
as command line parameters:
(The following is more or less also the usage of the bash script to start
the hlt proxy ('start-hlt-proxy.sh')).

USAGE:
./hlt_SM_proxy --domain <domain name> [--dimDnsNode <node name>
        --propertyFile <file name> --logFile <file name> --help]


 --domain: necessary parameter, gives the domain name for the Proxy
        (mandatory).
 --dimDnsNode: defines the node, where the DIM DNS is runnning.
        (Can be also set as environmental variable 'DIM_DNS_NODE')
 --propertyFile: defines the name of the Property File.
        (Can be also set as environmental variable
         'HLT_PROXY_PROPERTY_FILE')
 --logFile: defines the name of the log file.
        (Can be also set as environmental variable
         'HLT_PROXY_LOGFILE_NAME')
 --debug: if provided, more debug output is switched on in the Proxy.
        (This parameter must not have any value.)
 --help: prints out this help (all other parameter are ignored).



6) Changes per version (since ver 0.9.3):
=========================================

ver 0.9.3:
  - start parameter '--debug' introduced for more debug output on the command
    line.
  - intermediate states are no longer emulated in HLT proxy; now they are
    really mapped from the RunManager / TaskManager states. 
  - debugged error state handling
  - minor change in log file output
  - introduced new 'RUNNING' equivalent state for the RunManager / TaskManager: 
    'paused'. This is mapped to 'RUNNING'  as well.
  - enhanced the parameter relay in the start up script.

ver 0.9.4:
  - logging has now a timestamp with milliseconds precision.

ver 1.1.0:
  [MAJOR change in Interface to ECS]
  - Moving of HLT_MODE and RUN_NUMBER to ENGAGE command, both parameters
    are no longer sent together with the CONFIGURE command, but now with
    the ENGAGE command.
  - clean up in ProxyLogger class -> moved Timestamp struct to own headerfile.
  - Implemented framework to attach additional logger modules (see
    AdditionalLoggerBase class for details) -> test module implemented in
    AdditionalLoggerDummy.
  - Corrected 'DEFAULT' check for HLT_MODE parameter -> no 'DEFAULT' is
    allowed.
  - Enlarged allowed max line length of property file lines to 5120 chars.
  - Added additional Logger Adapter to connect to MLUC Logging facility of the
    HLT framework -> AdditionalLoggerMLUC. 
  - Added new CONFIGURE parameter RUN_TYPE (with default) -> this requires a
    change in the Property-file. New version number of Property file is 0.3.
    NOTE: Porperty file with version number 0.3 is required to run HLT-proxy
    1.1.0 or higher !
  - Checks for the right Property-file version number.

ver 1.1.1:
  - change of command parameter default now also possible during run, update
    is done before evaluating CONFIGURE - and ENGAGE parameters. The update
    also affects the sleep between polling for TM states.

ver 1.1.2:
  - additional logger adapter is now linked to infoLogger as well.
    (requires the setting of the environment variable $ALIHLT_DC_LIBDIR as
    path to the library folder inside th HLT framework).

ver 1.1.3:
  - Missing transition state of TaskManager included.
  - now possible to recover an ERROR state from RunManager Gui side (but only
    transitions to OFF, INITIALIZING, INITIALIZED and DEINITIALIZING are
    possible).
  - HLT-Proxy version number is now written to log file at start up.
  - moved version number to separate header file.

ver 1.2.0:
  - MLUCLogSever for infoLogger now takes HLT-proxy as log source argument.

ver 1.2.1:
  - minor correction in log messages
  - included domain name to facility name in info logger

ver 1.3.0:
  - introduced new intermediate state: DECONFIGURING
    This state is used to indicate the transition from CONFIGURED to 
    INITIALIZED (before this was mapped to INITIALIZING, but it now has its
    own transition state)
  - moved the CTP_TRIGGER_CLASS from CONFIGURE parameters to ENGAGE
    parameters. This became necessary, since the CTP trigger classes will be 
    generated very late in start up process. (A default value is still allowed
    fort this parameter).
  - Fixed a state check when going from ERROR to OFF.

ver 1.3.1:
  - introduced new intermediate state: STARTING
    This state is used to indicate the transition from READY to RUNNING.
    (before this was mapped back to READY, but it now has its own transition 
    state)

ver 1.4.0:
  - The Proxy now checks, if a connection to a TaskManager exists, when a 
    transition command is received. If not, the Proxy is set to ERROR state.

ver 1.5.0:
  - Implemented structure to use as backup proxy together with RunManager:
    xxx_HLT::HLT and xxx_HLT::HLT_BCK
    The module name (HLT / HLT_BCK) is taken from the Environment var 
    HLTECS_MODULE (check for existance performed in advance).
    NOTE: Don't forget to set the variable before starting the proxy.
  - Two new commands added: SET_ACTIVE and SET_PASSIVE. Used to switch between
    the two ECS-proxies as valid ones.

ver 1.5.1:
  - The property file accepts now environment variables: if a property value
    starts with a '$' the according value is retrieved from the environment
    variables (e.g. [01_MasterTMPort]=$ALIHLT_TM_PORT).  

ver 1.5.3:
  - Empty Properties are not seen as ERROR any longer, but a WARNING is given.

ver 1.5.4:
  - added setup script for standalone running : set DIM_DNS_HOST and DIM_DNS_NODE
  - moved creation of hlt-states.sobj from start-logic-engine.sh to makefile
