#!/bin/tcsh

#/************************************************************************
#**
#**
#** This file is property of and copyright by the Department of Physics
#** Institute for Physic and Technology, University of Bergen,
#** Bergen, Norway, 2006
#** This file has been written by Sebastian Bablok,
#** sebastian.bablok@ift.uib.no
#**
#** Important: This file is provided without any warranty, including
#** fitness for any particular purpose.
#**
#**
#*************************************************************************/

# set environment
#source .setup-all.tcsh

# set default for domain name
set DomainNameTop=ALICE_HLT::HLT
set DomainNameCon=ALICE_HLT::HLT_FWM
set DomainObj=HLT
set DomainLock=HLT_FWM

# set the desired domain name, if provided
if ( $1 != "" ) then
	if ( $2 != "" ) then

		set DomainNameTop=$1"::"$2
		set DomainNameCon=$1"::"$2"_FWM"

	else

		set DomainNameTop=$1"::"$DomainObj
		set DomainNameCon=$1"::"$DomainLock
	
endif

# set domain environment for gui
setenv HLTTOP $DomainNameTop
setenv HLTCON $DomainNameCon

echo "   Domain name is set to $HLTTOP."
echo "   Starting test gui now ..."
echo ""   
   
# start proxy
cd ./bin
./ecs_gui&
cd ..

