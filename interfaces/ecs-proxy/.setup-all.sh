#!/bin/bash

#/************************************************************************
#**
#**
#** This file is property of and copyright by the Department of Physics
#** Institute for Physic and Technology, University of Bergen,
#** Bergen, Norway, 2006
#** This file has been written by Sebastian Bablok,
#** sebastian.bablok@ift.uib.no
#**
#** Important: This file is provided without any warranty, including
#** fitness for any particular purpose.
#**
#**
#*************************************************************************/

# bash version of the start logic engine script
# changes to the settings should be done (only) in the first section!

##############################################################################
## 1. Section:
# Setting some environmental vars, adapt according to local setup !

export XERCES_C_LIB=/usr/lib
#export XERCES_C_LIB=~/HLT-PubSub/TMSimplechain/xerces-c-src_2_7_0/lib

#set the path to the HLT-Framework package base dir
#export ALIHLT_DC_DIR=~/HLT-PubSub/TMSimplechain/HLT-current-20060518.141400

#export TASK_MANAGER_LIBS=/home/sebastian/ECS-proxy/portal-ecs/TaskManager/HLT-current-20061120.115252/lib/Linux-x86_64
#export TASK_MANAGER_LIBS=$ALIHLT_DC_DIR/lib/Linux-i686



# setting DIM_DNS_NODE
#export DIM_DNS_NODE=kjekspc9
export DIM_DNS_NODE=`hostname`

# setting OS env
export OS=Linux

# end 1. Section

##############################################################################

## 2. Section
#check for existence of provided path and required files
DO_EXIT=0

if [ ! -e $ALIHLT_DC_DIR ]; then
    echo "ERROR: Path to HLT framework base ($ALIHLT_DC_DIR) doesn't exist."
    DO_EXIT=1
fi

if [ ! -e $TASK_MANAGER_LIBS ]; then
    echo "ERROR: Path to TaskManager libs ($TASK_MANAGER_LIBS) doesn't exist."
    DO_EXIT=1
fi

LIB_LIST="libBCL.so libMLUC.so SlaveControlInterfaceLib.so TMLib.so"
for LIB_NAME in $LIB_LIST
do
    if [ ! -e ${TASK_MANAGER_LIBS}/${LIB_NAME} ]; then
        echo "ERROR: Lib $LIB_NAME does not exist in the provided path or has been renamed."
        echo "Path is $TASK_MANAGER_LIBS ."
        DO_EXIT=1
    fi
done

if [ ! -e ${XERCES_C_LIB}/libxerces-c.so ]; then
    echo "ERROR: XERCES-C-LIB (${XERCES_C_LIB}/libxerces-c.so) doesn't exist."
    DO_EXIT=1
fi

if [ $DO_EXIT == 1 ]; then
    echo "Exiting due to missing files. Correct Errors and re-run setup script."
	return -1
#	exit -1
fi

# end 2. section

##############################################################################

# 3. section
# all the rest stuff, that has to be set, if check above has been succesfull

# setting DIM environment
cd dim/
. setup.sh
cd ..

# setting SMI environment
cd smi/
. .setup
if [ $? -ne 0 ]; then
        echo " *** Something failed in setup SMI script. Exiting setup... ***"
        cd ..
        return 1
fi
cd ..


# output
echo
echo "DIMDIR is set to $DIMDIR"
echo "SMIDIR is set to $SMIDIR"
echo "DIM_DNS_NODE is set to $DIM_DNS_NODE"

# creating "bin", "lib" and "doc" folder
echo
echo "creating the \"./bin\",  \"./lib\" and \"./doc\" folder"
if [ ! -d ./bin ]; then
    mkdir ./bin
fi

if [ ! -d ./doc ]; then
    mkdir ./doc
fi

if [ ! -d ./lib ]; then
    mkdir ./lib
fi


# include SMI tools to path
export PATH=${PATH}:${SMIDIR}/linux
echo "Path is now: $PATH"

# set LD_LIBRARY_PATH
export HLT_PROXY_DIR=`pwd`
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${HLT_PROXY_DIR}/lib
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${TASK_MANAGER_LIBS}:${XERCES_C_LIB}
echo "LD_LIBRARY_PATH is now: $LD_LIBRARY_PATH"

# prepare libs
#echo Preparing links to used libs
cd lib/
if [ ! -e ./libSlaveControlInterface.so ]; then
        ln -s ${TASK_MANAGER_LIBS}/SlaveControlInterfaceLib.so libSlaveControlInterface.so
fi
if [ ! -e ./libTM.so ]; then
        ln -s ${TASK_MANAGER_LIBS}/TMLib.so libTM.so
fi

#ln -s libxerces-c.so.27 libxerces-c.so
#ln -s libxerces-c.so.27.0 libxerces-c.so.27
#ln -s libBCL.so.0 libBCL.so
#ln -s libBCL.so.0.5.3 libBCL.so.0
#ln -s libMLUC.so.0 libMLUC.so
#ln -s libMLUC.so.0.5.1 libMLUC.so.0

cd ..

