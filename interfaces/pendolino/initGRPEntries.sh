#!/bin/bash


#/****************************************************************************/
#**
#**
#** This file is property of and copyright by the Department of Physics
#** Institute for Physic and Technology, University of Bergen,
#** Bergen, Norway, 2008
#** This file has been written by Sebastian Bablok,
#** sebastian.bablok@ift.uib.no
#**
#** Important: This file is provided without any warranty, including
#** fitness for any particular purpose.
#**
#**
#******************************************************************************/

## This script set the initial GRP entries to T-HCDB

function usage {
    echo "Usage:"
    echo "  $0 <Current_Run_Number <Detector_List> <Beam_Type> <Run_Type>"
    echo ""
	echo "    Current_Run_Number: The run number of the current run."
    echo "                (has to be provided by the RunManager)"
    echo ""
	echo "    Detector_List: List of participating detectors, used for the initial"
    echo "                GRP T-HCDB entry (this is given by ECS)."
    echo ""
	echo "    Beam_Type: The beam type, used for the initial GRP T-HCDB entries"
    echo "                (this is given by ECS)."
    echo ""
    echo "    Run_Type: The current run type, used for the initial GRP T-HCDB"
    echo "                entries (this is given by ECS)."
    echo ""
	echo ""
    echo " e.g. $0 66 TPC,TRD,PHOS pp PHYSICS"
    echo ""
    exit 9
}



if [ $1 ] && [ "$2" ] && [ $3 ] && [ $4 ]; then
    if [ $1 = "--help" ]; then
        usage
    fi
	runNumber=$1
	detectorList=$2
	beamType=$3
	runType=$4

else
    # not all necessary parameters provided
    echo "   *** Parameters missing, see usage ... ***"
    usage
fi

echo "   --- Starting insertion script."
aliroot -q -b -l "setGRPVal.C($runNumber, \"$detectorList\", \"$beamType\", \"$runType\")"

retVal=$?
if [ $retVal -eq 10 ]; then
	echo "   --- AliRoot initial GRP insertion successfull."
else
	echo "   *** AliRoot initial GRP insertion failed (retVal: '$retVal')."
fi




