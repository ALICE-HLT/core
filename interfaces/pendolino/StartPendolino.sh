#!/bin/bash

#/****************************************************************************/
#**
#**
#** This file is property of and copyright by the Department of Physics
#** Institute for Physic and Technology, University of Bergen,
#** Bergen, Norway, 2007
#** This file has been written by Sebastian Bablok,
#** sebastian.bablok@ift.uib.no
#**
#** Important: This file is provided without any warranty, including
#** fitness for any particular purpose.
#**
#**
#******************************************************************************/

## This script initializes the HCDB, releases the HCDB to DA nodes and starts 
## and controls the Pendolinos.

## definition of variables
## Please adapt these vars if you want to run the pendolino in differnet setup

VersionNumber="v1.3.0"
            # version number of the Pendolino
Pendolino_test=true #false #true
            # Variable is used to flag that this is only a test run, that shall
            # not interfere with the current system -> no clean up of HCDB,
            # no update of HCDB folder on computing nodes, usage of HCDB test
            # folder, no TaskManager notification; HCDB_Test_Folder is used
            # instead of real HCDB_base for HCDB.
            # Use 'false' for real system !!


# settings for fast Pendolino
ListsFast="fast" #"/opt/T-HCDB/lists/lists-pendolino/fast"
			# folder containing the list of DP for fast Pendolino, this is
			# prepended by the ListfileBase
IntervalFast=60
			# time interval for the fast Pendolino
usePend1=true
			# toggle fast Pendolino on / off (true / false)
logFile_Pendolino_fast="/tmp/pendolino-fast.log"
			# log file name for the fast Pendolino


# settings for normal Pendolino
ListsNormal="normal"
			# folder containing the list of DP for normal Pendolino, this is
			# prepended by the ListfileBase
IntervalNormal=300
			# time interval for the normal Pendolino
usePend2=true
			# toggle normal Pendolino on / off (true / false)
logFile_Pendolino_normal="/tmp/pendolino-normal.log"
            # log file name for the normal Pendolino


# settings for slow Pendolino
ListsSlow="slow"
			# folder containing the list of DP for slow Pendolino, this is
			# prepended by the ListfileBase 
IntervalSlow=600
			# time interval for the slow Pendolino
usePend3=true
			# toggle slow Pendolino on / off (true / false)
logFile_Pendolino_slow="/tmp/pendolino-slow.log"
            # log file name for the slow Pendolino


# generel settings
Amanda_host="alidcsamanda.cern.ch"		#old: "10.160.33.92"
            # the IP address of the Amanda server in the DCS CR (10.160.33.92)
Amanda_port=1337						#old: 4242
            # the port number of the Amanda server (4242)
Merged_filename="ListMerged.txt"
			# name of the file containing the merged lists of DP per Pendolino
			# !! make sure that this file never uses the same suffix like the 
			# unmerged lists (Default suffix for unmerged lists: '.list') !!
HCDB_base="/opt/HCDB"
			# the path to the base folder of the HCDB (normally '/opt/HCDB')
			# if according env var is set, this is later in the script replaced
			# by the env var
if [ $ALIHLT_T_HCDBDIR ]; then
	T_HCDB_base=$ALIHLT_T_HCDBDIR
else
	T_HCDB_base="/opt/T-HCDB"
fi
			# the path to the base folder of the T-HCDB (normally '/opt/T-HCDB')
ListFileBase="$T_HCDB_base/lists/lists-pendolino"
			# Base folder for the list files of the Pendolino
filename_lastRunNumber="lastRunNumber"
			# name of the file containing the last run number for the Taxi

SlaveLib_listenAddress_protocol="tcpmsg://"
			# The protocol for the local listen address of the SlaveControl lib,
			# used to contact the TaskManager (notifying about HCDB update).
SlaveLib_listenAddress_append=".internal:42015"
			# string that shall be appended to the local listen address of the 
			# SlaveControl lib after te hostname. The end of address 
			# represents domain and port number.
			# complete address example: [tcpmsg://portal-dcs0.internal:42025]
			# Don't use 42024 as port; this is used by HCDBManager script, which
			# inserts configuration entries to HCDB 
TaskManager_address_default="tcpmsg://portal-ecs0.internal:20100"
			# address of the TaskManger that shall be notified
			# [tcpmsg://portal-ecs0.internal:20100]
#TM_notifyCommand="trigger_reconfigure_event temporary"
			# command that shall be used to notify the TaskManager about the
			# HCDB update, has to be synchonized with TaskManager 
			# [remove '_test' for real event].
TM_notifyCommand_HCDBready="hcdb_prepared"
            # command that shall be used to notify the TaskManager about the
            # HCDB has been prepared and is ready to use

HLT_PermanentFolder="HLTPermanent"
			# folder name of the permanent HLT folder in HCDB, which shall NOT
			# be cleaned up at start up
Num_Pendolinos_default=1 #3
			# default number of Pendolinos, that shall be used (max number is 3)

HCDB_Test_Folder="/tmp/HCDB-test"
			# Defines the test folder for the HCDB, when running in test mode
logFile_Pendolino_fast_test="/tmp/pendolino-fast-test.log"
            # log file name for the fast Pendolino
logFile_Pendolino_normal_test="/tmp/pendolino-normal-test.log"
            # log file name for the fast Pendolino
logFile_Pendolino_slow_test="/tmp/pendolino-slow-test.log"
            # log file name for the fast Pendolino
ListFileBase_test="$T_HCDB_base/lists/lists-P-Test"
			# Folder containing the lists for a Pendolino test run
mainOperator="hlt-operator"
            # name of the main operator; all others are regarded as test operators
operatorName=$mainOperator
			# the operator running the Pendolino; for any other operator, 
			# except the $mainOperator, only dummy log files and a test-HCDB are
			# used (logfiles: "/tmp/pendolino-<type>-<operatorName>.log", HCDB
			# created in the home of the operator: "~/HCDB"
EnvSource="/opt/HLT/control/operator.bin/setup/.setbash.sh"
			# script to be sourced to get the correct envs
SlaveLib_listenAddress_port=42020


# more vars
Own_PID=$$
PID_Pend1=0
PID_Pend2=0
PID_Pend3=0
retVal=0
ListsFast_merged=""
ListsNormal_merged=""
ListsSlow_merged=""
currentRunNumber=0
exitFlag=false
timestamp=`date "+%Y-%m-%d %H:%M:%S"`
counter=0
TaskManager_address=""
Num_Pendolinos=0
ListsFast_path=""
ListsNormal_path=""
ListsSlow_path=""
localHCDBs=false
PortNum=0

beamType="INIT"
runType="INIT"
detectorList="INIT"


## Functions
## prints out the usage of this script
function usage {
	echo "Usage ($VersionNumber):"
	echo "  $0 <Current_Run_Number> <Master_TaskManager_Address> [Num_of_Pendolinos]"
	echo "                [Beam_Type] [Run_Type] [Detector_List]"
	echo ""
	echo "    Current_Run_Number: The run number of the current run," 
	echo "                (should be provided by the starting RunManager)."
	echo ""
	echo "    Master_TaskManager_Address (optional): The address, where the RunManager"
	echo "                can be contacted for notifying about HCDB updates. When this "
	echo "                parameter is not provided, the default is taken."
	echo "                (Default: $TaskManager_address_default)"
	echo ""
	echo "    Num_of_Pendolinos (optional): The number of Pendolinos, that shall be started."
	echo "                Only a number of 1 to 3 Pendolinos are accepted; default is $Num_Pendolinos_default."
	echo "                Note: If the number of Pendolinos is provided, the"
	echo "                'Master_TaskManager_Address' has to be provided as well."
	echo ""
	echo "    Beam_Type, Run_Type, Detector_List (optional): Initial run parameters given by"
	echo "                ECS at start up. NOTE: When these paramerts are given, you have to"
	echo "                provide 'Master_TaskManager_Address' and 'Num_of_Pendolinos' as well."
	echo ""
	echo ""
	echo "  e.g. $0 66 $TaskManager_address_default 2 pp PHYSICS ALICE_ALL"
	echo ""
	echo "    NOTE: In order to have the Pendolino starter log monitored by SysMES,"
	echo "          the command line output has to be piped to:"
	echo "          '/tmp/pendolino-starter.log'. (Monitoring only on 'portal-dcs0'"
	echo "          and 'portal-dcs1'.)"
	echo "          The actual Pendolinos store their log output in:"
	echo "          '/tmp/pendolino-<type>.log', where <type> is either 'fast',"
	echo "          'normal' or 'slow'. These files are monitored by SysMES as well."
	echo ""
	echo "  e.g. $0 8 $TaskManager_address_default 3 pp PHYSICS ALICE_ALL > /tmp/pendolino-starter.log 2>&1"
	echo ""
	echo "    Additional remark: When running the Pendolino from any operator, but"
	echo "          the hlt-operator, dedicated operator logfiles for the actual"
	echo "          Pendolinos are used:"
	echo "          '/tmp/pendolino-<type>-test.log-<operatorName>'. "
	echo "          In addition the real HCDB ('/tmp/HCDB') is only used for the"
	echo "          hlt-operator; other operators get a HCDB copy in their home "
	echo "          folder: '/afsuser/<operator>/HCDB'. The environment variable"
	echo "          'ALIHLT_HCDBDIR' normally defines the used path to the HCDB."
	echo ""
	exit 9
}

## function to update the timestamp in timestamp var
function setTimeStamp {
    timestamp=`date "+%Y-%m-%d %H:%M:%S"`
}

## Function to set the links for entries in the T-HCDB to the HCDB
function setTHCDBLinks {
	# script that copies folder-structure of T-HCDB to the HCDB and
	# sets links for each file
	./PrepareHCDB.sh $T_HCDB_base $HCDB_base
	if [ $? -ne 0 ]; then
		echo "   ~~~ ERROR while preparing HCDB, trying to run without proper HCDB."
	fi
}

## signal handler function for self generated exit signal (SIGUSR1)
function catchIt {
	echo "   --- Caught own SIGUSR1 signal ---"
}


## function to clean up HCDB before initiating new one for next run (SoR)
function cleanHCDB {
	echo "   --- Cleaning up HCDB ($HCDB_base) now."
	# clean up of HCDB
	# check if $HCDB_base is really set to prevent deleting in root dir 
	if [ $HCDB_base ] && [ $HCDB_base != "/" ] && [ -e $HCDB_base ]; then
		for i in $( ls $HCDB_base ); do
			if [ $i != $HLT_PermanentFolder ]; then 
				rm -rf --preserve-root $HCDB_base/$i  
				if [ $? -ne 0 ]; then
					echo "   *** Unable to clean up $HCDB_base/$i in HCDB."
				else
					let counter++
				fi
			else
				echo "   --- Omiting $HCDB_base/$HLT_PermanentFolder in clean up HCDB."
			fi
		done
		echo "   +++ Clean up of HCDB finished ($counter folders)."
	else
		echo "   *** HCDB_base is not set (or \"/\"), omiting clean up of HCDB."
		echo "   *** Unable to clean up HCDB -> Analysis can be corrupted by old data."
	fi
}

## function to signal all subprocesses (Pendolinos) to exit
function stopPendolinos {
	echo ""
	echo "   --- stopping TM_Notifier and all Pendolinos..."
#	kill -a -s 10 -$$ # sending signal to all subprocesses of StartPendolino script
	if [ $PID_Pend1 -ne 0 ]; then
		kill -a -s 10 $PID_Pend1
		echo "   ### killing Pendolino 1 ($PID_Pend1)"
	fi
	if [ $PID_Pend2 -ne 0 ]; then
		kill -a -s 10 $PID_Pend2
		echo "   ### killing Pendolino 2 ($PID_Pend2)"
	fi
	if [ $PID_Pend3 -ne 0 ]; then
		kill -a -s 10 $PID_Pend3
		echo "   ### killing Pendolino 3 ($PID_Pend3)"
	fi
}

## function to catch a SIGINT and refer it to subprocesses
function cleanUp {
	trap '' 1 2 3 6 12 15 # disable SIGUSR2 (12) no release HCDB needed
    stopPendolinos

    ## wait for subprocesses (Pendolinos) to quit
    echo "   --- Waiting for Pendolinos to finish... "
    wait # this should wait for all child processes to be terminated, if not use:
    #wait $PID_Pend1
    #wait $PID_Pend2
    #wait $PID_Pend3
    sleep 1     # give some time for output of the last finishing Pendolino

	## Clean up HCDB
#	cleanHCDB  # clean up of HCDB not performed at End, but at Start!
	setTimeStamp			# update time stamp
	echo "   +++ Finished StartPendolino script ($timestamp) +++"	
    exit 0
}

function setExitFlag {
	echo "   --- Caught exit signal while releasing HCDB, setting exit flag"
	# if second try to perform exit, jump directly to cleanUp
	if [ $exitFlag = true ]; then
		echo "   --- Exit directly, ignoring release of HCDB."
		cleanUp
	fi 
	exitFlag=true
}

## Function to set the current run number in the according file for the TAXI
function setRunNumber {
	## Inform Taxi about current Run number
	echo "   --- Storing current run number ($currentRunNumber) in '$T_HCDB_base/$filename_lastRunNumber' for Taxi"
	# check if $filename_lastRunNumber is not '*'
	if [ $filename_lastRunNumber == '*' ]; then
    	echo "   *** Invalid name for file containing last run number '$filename_lastRunNumber'."
    	echo "   *** Leaving Pendolino due to error!"
    	exit 8
	fi
	rm -f --preserve-root $T_HCDB_base/$filename_lastRunNumber
	if [ $? -eq 0 ]; then
    	echo "$currentRunNumber" >> $T_HCDB_base/$filename_lastRunNumber
	else
    	echo "   *** Unable to clean and reset run number file for Taxi."
	fi
}


# NOTE: changed since version 0.9.3
## function that is executed when SIGUSR2 is received (This signals an update
## of the HCDB, so a new release can be triggered. In order to have a 
## serialized call of the release function and to avoid concurrency probs this
## has been moved from the several subprocesses to the parent process (one).
#function setReleaseHCDBFlag {
#	trap '' 12						# disable reaction on SIGUSR2
#
#	# trap exit signals and only set exit flag 
#	trap 'setExitFlag' 1 2 3 6 15
#
#	echo "   --- releasing HCDB after update ..."
#	# release HCDB after update
#
#	if [ $Pendolino_test != true ]; then	
#		/opt/HLT/tools/bin/release-HCDB.sh
#	fi
#	# check for exit value of scripts -> but what can be done else ?
#	retVal=$?
#
#	# set trap for exit signals back to cleanup func
#	trap 'cleanUp' 1 2 3 6 15
#
#	# check if exit flag is true, if so call cleanup
#	if [ $exitFlag = true ]; then
#		cleanUp
#	fi
#
#	echo "   --- Releasing of HCDB returned '$retVal' ('0' means success)"
#
#	if [ $Pendolino_test != true ]; then
#		setTimeStamp			# update time stamp
#		echo "   --- notifying TaskManager after release of HCDB ($timestamp)"
#		# inform PubSub about new update, SlaveControlLib wrapper listens on
#		# on SIGUSR2 (12)
#		kill -a -s 12 $PID_tm_notifier
#	fi
#
#	trap 'setReleaseHCDBFlag' 12	# enable trapping of SIGUSR2 again
#}


## main part of script
## check if parameter is provided and if usage shall be printed
if [ $1 ]; then
	if [ $1 = "--help" ]; then
		usage	# exits after printing out usage
	fi
	currentRunNumber=$1
	if [ $2 ]; then
		TaskManager_address=$2
		if [ $3 ]; then
			Num_Pendolinos=$3
		else 
			Num_Pendolinos=$Num_Pendolinos_default
		fi
		case $Num_Pendolinos in
			1) 	usePend1=true
				usePend2=false
				usePend3=false
				;;
			2) 	usePend1=true
           		usePend2=true
           		usePend3=false
				;;
			3) 	usePend1=true
           		usePend2=true
           		usePend3=true
				;;
			*) 	echo " Only up to 3 Pendolinos are possible. See usage..."
				echo ""
				usage	 # exits after printing out usage
				;;
		esac	

		if [ $4 ]; then
			beamType=$4
		fi

		if [ $5 ]; then
			runType=$5
		fi

		if [ $6 ]; then
			detectorList=$6
		fi

	else
		TaskManager_address=$TaskManager_address_default
	fi
else
	echo "   *** Paramter missing, see usage..."
	echo ""
	usage	# exits after printing out usage
fi


echo ""
echo " Init Pendolinos script ($VersionNumber) started! - $timestamp"
echo ""


pushd /tmp
if [ -r $EnvSource ]; then
	. $EnvSource
	echo "   +++ Sourced required envs from $EnvSource."
else
	echo "   *** Unable to source required envs from $EnvSource, file not existing. Exit."
	exit 9
fi
popd


operatorName=`whoami`
echo "   ### Running as user: $operatorName"

if [ "$operatorName" != "$mainOperator" ]; then
	
	echo "   --- Using test-operator specific logfiles."
    # set log file to operator log file name
    logFile_Pendolino_fast=$logFile_Pendolino_fast_test-$operatorName
    logFile_Pendolino_normal=$logFile_Pendolino_normal_test-$operatorName
    logFile_Pendolino_slow=$logFile_Pendolino_slow_test-$operatorName
	localHCDBs=true
fi


## set HCDB path
if [ $ALIHLT_HCDBDIR ]; then
    HCDB_base=$ALIHLT_HCDBDIR
fi

if [ $Pendolino_test = true ]; then
	if [ ! -e $HCDB_Test_Folder ]; then
		mkdir $HCDB_Test_Folder
		if [ $? -ne 0 ]; then
			echo "   *** Unable to create Test folder for HCDB ($HCDB_Test_Folder), exiting..."
			exit 9
		fi
	fi
	HCDB_base=$HCDB_Test_Folder
	echo "   ### Using Pendolino in test mode: HCDB test folder: $HCDB_base"
	
	# Preparing list files pathes
	ListsFast_path="$ListFileBase_test/$ListsFast"
	ListsNormal_path="$ListFileBase_test/$ListsNormal"
	ListsSlow_path="$ListFileBase_test/$ListsSlow"

	# set log file to test log file name
	logFile_Pendolino_fast=$logFile_Pendolino_fast_test
	logFile_Pendolino_normal=$logFile_Pendolino_normal_test
	logFile_Pendolino_slow=$logFile_Pendolino_slow_test

else
	# Preparing list files pathes
	ListsFast_path="$ListFileBase/$ListsFast"
	ListsNormal_path="$ListFileBase/$ListsNormal"
	ListsSlow_path="$ListFileBase/$ListsSlow"

    if [ $Pendolino_test != true ] && [ $localHCDBs != true ]; then
		## Inform Taxi about current Run number
    	setRunNumber
	fi

fi


## register signal handler for catching SIGUSR1 (10) signals (send to all
## process of this process group to signal a proper exit -> 
## PendolinoDriver.sh can perform proper exit).
trap 'catchIt' 10

## register handler for SIGHUP, SIGINT, SIGQUIT, SIGABRT and SIGTERM
## to perform clean exit of Pendolinos as well (SIGUSR2 added)
trap 'cleanUp' 1 2 3 6 12 15

## register signal handler for SIGUSR2 (this signal will be send by
## subprocesses to signal the release of an updated HCDB)
#trap 'setReleaseHCDBFlag' 12 ## NOTE: changed since version 0.9.3


## Cleaning up old HCDB folder before releasing T-HCDB and filling new data to HCDB
cleanHCDB  # this was formerly performed after end of run, but better at start up!


if [ $localHCDBs != true ] && [ $Pendolino_test != true ]; then
	## Release Taxi-HCDB (Read-only) version to Pendolino node (AFS)
	echo "   --- Releasing T-HCDB ($T_HCDB_base). "
	/opt/HLT/tools/bin/release-T-HCDB.sh
	# check for exit value of scripts -> but what can be done if failed ?
	retVal=$?
	#echo "   --- Releasing of HCDB returned '$retVal' ('0' means success)"
else
	echo "   ### Skipping release of T-HCDB, due to use of test-operator."
fi

## Set links from Taxi-HCDB to HCDB on Pendoliono node 
if [ $localHCDBs != true ]; then
	setTHCDBLinks
	echo "   +++ Using real HCDB ($HCDB_base)."
else
	
    if [ ! -e $HCDB_base ]; then
        mkdir $HCDB_base
        if [ $? -ne 0 ]; then
            echo "   *** Unable to create operator specific HCDB ($HCDB_base), exiting..."
            exit 9
        fi
    fi
	
	# copy latest objects from T-HCDB to operator-specific HCDB using a AliRoot macro
	aliroot -q -b -l "setHCDB.C(\"$T_HCDB_base\" , \"$HCDB_base\" , $currentRunNumber)"
	retVal=$?
	if [ $retVal -ne 10 ]; then
		echo "   *** Preparing HCDB ($HCDB_base) with T-HCDB ($T_HCDB_base) failed: $retVal."
	else
		echo "   +++ Using operator-specific HCDB ($HCDB_base)"
	fi
fi

## release HCDB (from Pendolino) to DA nodes (AFS)
if [ $Pendolino_test != true ] && [ $localHCDBs != true ]; then
	echo "   --- Releasing HCDB ($HCDB_base). "
	/opt/HLT/tools/bin/release-HCDB.sh
	# check for exit value of scripts -> but what can be done if failed ?
	retVal=$?
	#echo "   --- Releasing of HCDB returned '$retVal' ('0' means success)"
fi

# NOTE: changed since v 0.9.3
## prepare addresses for Master TaskManager notification
#SlaveLib_listenAddress=$SlaveLib_listenAddress_protocol`hostname`$SlaveLib_listenAddress_append
#echo "   ### SlaveLib listen address: $SlaveLib_listenAddress"
#echo "   ### Master TaskManager address: $TaskManager_address"
## start TaskManager notifier (Wrapper for SlaveControl Lib)
#echo "   --- Starting wrapper for Slave Control Lib (TM notifier)."
#./pet/TM_Notifier $SlaveLib_listenAddress $TaskManager_address "$TM_notifyCommand" &
#$ALIHLT_DC_BINDIR/TM_Notifier $SlaveLib_listenAddress $TaskManager_address "$TM_notifyCommand" &
#PID_tm_notifier=$!
#sleep 1


SlaveLib_listenAddress=$SlaveLib_listenAddress_protocol`hostname`$SlaveLib_listenAddress_append
echo "   ### SlaveLib listen address: $SlaveLib_listenAddress"
echo "   ### Master TaskManager address: $TaskManager_address"
$ALIHLT_DC_BINDIR/TM_Notifier $SlaveLib_listenAddress $TaskManager_address $TM_notifyCommand_HCDBready "--direct" 
retVal=$?
if [ $retVal -ne 0 ]; then
	echo "   *** Error in notification about 'HCDB ready': $retVal ."
else
	echo "   +++ Notification for 'HCDB ready' returned: $retVal ."
fi

export START_PENDOLINO_SCRIPT_PID=$Own_PID


## start Pendolinos
# 1. Pendolino (fast)
if [ $usePend1 = true ]; then
	let PortNum=$SlaveLib_listenAddress_port+1
	./mergeLists.sh $ListsFast_path
	retVal=$?
	if [ $retVal == 1 ]; then
		ListsFast_merged=$ListsFast_path/$Merged_filename
	elif [ $retVal == 2 ]; then  
		ListsFast_merged=${ListsFast_path%/*}/$Merged_filename
	fi

	if [ $retVal != 9 ]; then
		if [ $Pendolino_test != true ]; then
			# Real run
			./PendolinoDriver.sh $ListsFast_merged $Amanda_host $Amanda_port $HCDB_base $currentRunNumber $TaskManager_address $PortNum $IntervalFast $beamType $runType $detectorList > $logFile_Pendolino_fast 2>&1 &
			PID_Pend1=$! 
		else
			# Test run
			( ./PendolinoDriver.sh $ListsFast_merged $Amanda_host $Amanda_port $HCDB_base $currentRunNumber $TaskManager_address $PortNum $IntervalFast $beamType $runType $detectorList ; PID_Pend1=$! ) | tee -a $logFile_Pendolino_fast 2>&1 &
## !! Does not work to get PID of ./PendolinoDriver.sh  :-(, but not important
		fi

		# Maybe implement test, if script has started sucessfully ??
		setTimeStamp            # update time stamp
		echo "   --- Fast Pendolino ($PID_Pend1) started at '$timestamp'; logfile: '$logFile_Pendolino_fast'."
		sleep 1 # allow a sleep of seconds before next Pendolino is started
	else
		echo "   *** Producing of list file failed ($ListsFast_path); cancel start of fast Pendolino."
	fi
	echo ""
fi

# 2. Pendolino (normal)
if [ $usePend2 = true ]; then
	let PortNum=$SlaveLib_listenAddress_port+2
    ./mergeLists.sh $ListsNormal_path
    retVal=$?
    if [ $retVal == 1 ]; then
        ListsNormal_merged=$ListsNormal_path/$Merged_filename
    elif [ $retVal == 2 ]; then 
        ListsNormal_merged=${ListsNormal_path%/*}/$Merged_filename
    fi

    if [ $retVal != 9 ]; then
		if [ $Pendolino_test != true ]; then
			# Real run
			./PendolinoDriver.sh $ListsNormal_merged $Amanda_host $Amanda_port $HCDB_base $currentRunNumber $TaskManager_address $PortNum $IntervalNormal $beamType $runType $detectorList > $logFile_Pendolino_normal 2>&1 &
			PID_Pend2=$!
		else
			# Test run
			( ./PendolinoDriver.sh $ListsNormal_merged $Amanda_host $Amanda_port $HCDB_base $currentRunNumber $TaskManager_address $PortNum $IntervalNormal $beamType $runType $detectorList ; PID_Pend2=$! ) | tee -a $logFile_Pendolino_normal 2>&1 &
## !! Does not work to get PID of ./PendolinoDriver.sh  :-(, but not important
		fi

		# Maybe implement test, if script has started sucessfully ??
        setTimeStamp            # update time stamp
		echo "   --- Normal Pendolino ($PID_Pend2) started at '$timestamp'; logfile: '$logFile_Pendolino_normal'."
		sleep 1 # allow a sleep of seconds before next Pendolino is started
	else
       echo "   *** Producing of list file failed ($ListsNormal_path); cancel start of normal Pendolino."
    fi
    echo""
fi

# 3. Pendolino (slow)
if [ $usePend3 = true ]; then
	let PortNum=$SlaveLib_listenAddress_port+3
    ./mergeLists.sh $ListsSlow_path
    retVal=$?
    if [ $retVal == 1 ]; then
        ListsSlow_merged=$ListsSlow_path/$Merged_filename
    elif [ $retVal == 2 ]; then 
        ListsSlow_merged=${ListsSlow_path%/*}/$Merged_filename
    fi

    if [ $retVal != 9 ]; then
		if [ $Pendolino_test != true ]; then
			# Real run
			./PendolinoDriver.sh $ListsSlow_merged $Amanda_host $Amanda_port $HCDB_base $currentRunNumber $TaskManager_address $PortNum $IntervalSlow $beamType $runType $detectorList > $logFile_Pendolino_slow 2>&1 &
			PID_Pend3=$!
		else
			# Test run
			( ./PendolinoDriver.sh $ListsSlow_merged $Amanda_host $Amanda_port $HCDB_base $currentRunNumber $TaskManager_address $PortNum $IntervalSlow $beamType $runType $detectorList ; PID_Pend3=$! ) | tee -a $logFile_Pendolino_slow 2>&1 &
## !! Does not work to get PID of ./PendolinoDriver.sh  :-(, but not important
		fi

		# Maybe implement test, if script has started sucessfully ??
        setTimeStamp            # update time stamp
		echo "   --- Slow Pendolino ($PID_Pend3) started at '$timestamp'; logfile: '$logFile_Pendolino_slow'."
		sleep 1 # allow a sleep of seconds before next Pendolino is started
    else
       echo "   *** Producing of list file failed ($ListsSlow_path); cancel start of slow Pendolino."
    fi
    echo""
fi

#echo ""
echo "   --- All Pendolinos started. Waiting for End-of-Run (EoR)...."
echo ""

## wait for EoR - requires communication with TaskManager system
## the EoR will be snet as signal SIGQUIT to this process 
## -> handled with signal handler above
while true 
do
	sleep 1 
done


## --> since this should never be reached, it is handled with handler above 
## clean up HCDB and exit
#cleanHCDB

echo "   +++ Init Pendolinos script finished +++"


## end of script

