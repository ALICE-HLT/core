#!/bin/bash

#/****************************************************************************/
#**
#**
#** This file is property of and copyright by the Department of Physics
#** Institute for Physic and Technology, University of Bergen,
#** Bergen, Norway, 2008
#** This file has been written by Sebastian Bablok,
#** sebastian.bablok@ift.uib.no
#**
#** Important: This file is provided without any warranty, including
#** fitness for any particular purpose.
#**
#**
#******************************************************************************/

## This script cleans up HCDB and prepares it for usage without Pendolinos 

## definition of variables

HCDB_base="/opt/HCDB"
			# the path to the base folder of the HCDB (normally '/opt/HCDB')
			# if according env var is set, this is later in the script replaced
			# by the env var
if [ $ALIHLT_T_HCDBDIR ]; then
	T_HCDB_base=$ALIHLT_T_HCDBDIR
else
	T_HCDB_base="/opt/T-HCDB"
fi
			# the path to the base folder of the T-HCDB (normally '/opt/T-HCDB')
SlaveLib_listenAddress_protocol="tcpmsg://"
            # The protocol for the local listen address of the SlaveControl lib,
            # used to contact the TaskManager (notifying about HCDB update).
SlaveLib_listenAddress_append=".internal:42016"
            # string that shall be appended to the local listen address of the
            # SlaveControl lib after te hostname. The end of address
            # represents domain and port number.
            # complete address example: [tcpmsg://portal-dcs0.internal:42025]
            # Don't use 42024 as port; this is used by HCDBManager script, which
            # inserts configuration entries to HCDB
TaskManager_address_default="tcpmsg://portal-ecs0.internal:20100"
            # address of the TaskManger that shall be notified
            # [tcpmsg://portal-ecs0.internal:20100]
TM_notifyCommand_HCDBready="hcdb_prepared"
			# command that shall be used to notify the TaskManager about the
			# HCDB has been prepared and is ready to use 

HLT_PermanentFolder="HLTPermanent"
			# folder name of the permanent HLT folder in HCDB, which shall NOT
			# be cleaned up at start up
filename_lastRunNumber="lastRunNumber"
			# name of the file containing the last run number for the Taxi

# more vars
retVal=0
timestamp=`date "+%Y-%m-%d %H:%M:%S"`
counter=0
TaskManager_address=""
localHCDBs=false


## Functions
## prints out the usage of this script
function usage {
	echo "Usage:"
	echo "  $0 <Current_Run_Number> [Master_TaskManager_Address] "
	echo ""
	echo "    Current_Run_Number: The run number of the current run." 
	echo "                (should be provided by TaskManager)"
	echo ""
	echo "    Master_TaskManager_Address (optional): The address where to contact the"
	echo "                Master TaskManager for notifying of updates. When this "
	echo "                parameter is not provided, the default is taken."
	echo "                (Default: $TaskManager_address_default)"
	echo ""
	echo ""
	echo " e.g. $0 66 $TaskManager_address_default "
	echo ""
	exit 9
}

## function to update the timestamp in timestamp var
function setTimeStamp {
    timestamp=`date "+%Y-%m-%d %H:%M:%S"`
}

## Function to set the links for entries in the T-HCDB to the HCDB
function setTHCDBLinks {
	# script that copies folder-structure of T-HCDB to the HCDB and
	# sets links for each file
	./PrepareHCDB.sh $T_HCDB_base $HCDB_base
	if [ $? -ne 0 ]; then
		echo "   ~~~ ERROR while preparing HCDB, trying to run without proper HCDB."
	fi
}


## function to clean up HCDB before initiating new one for next run (SoR)
function cleanHCDB {
	echo "   --- Cleaning up HCDB ($HCDB_base) now."
	# clean up of HCDB
	# check if $HCDB_base is really set to prevent deleting in root dir 
	if [ $HCDB_base ] && [ $HCDB_base != "/" ] && [ -e $HCDB_base ]; then
		for i in $( ls $HCDB_base ); do
			if [ $i != $HLT_PermanentFolder ]; then 
				rm -rf --preserve-root $HCDB_base/$i  
				if [ $? -ne 0 ]; then
					echo "   *** Unable to clean up $HCDB_base/$i in HCDB."
				else
					let counter++
				fi
			else
				echo "   --- Omiting $HCDB_base/$HLT_PermanentFolder in clean up HCDB."
			fi
		done
		echo "   +++ Clean up of HCDB finished ($counter folders)."
	else
		echo "   *** HCDB_base is not set (or \"/\"), omiting clean up of HCDB."
		echo "   *** Unable to clean up HCDB -> Analysis can be corrupted by old data."
	fi
}


## Function to set the current run number in the according file for the TAXI
function setRunNumber {
	## Inform Taxi about current Run number
	echo "   --- Storing current run number ($currentRunNumber) in '$T_HCDB_base/$filename_lastRunNumber' for Taxi"
	# check if $filename_lastRunNumber is not '*'
	if [ $filename_lastRunNumber == '*' ]; then
    	echo "   *** Invalid name for file containing last run number '$filename_lastRunNumber'."
    	echo "   *** Leaving Pendolino due to error!"
    	exit 8
	fi
	rm -f --preserve-root $T_HCDB_base/$filename_lastRunNumber
	if [ $? -eq 0 ]; then
    	echo "$currentRunNumber" >> $T_HCDB_base/$filename_lastRunNumber
	else
    	echo "   *** Unable to clean and reset run number file for Taxi."
	fi
}


## main part of script
## check if parameter is provided and if usage shall be printed
if [ $1 ]; then
	if [ $1 = "--help" ]; then
		usage	# exits after printing out usage
	fi
	currentRunNumber=$1
	if [ $2 ]; then
		TaskManager_address=$2
	else
		TaskManager_address=$TaskManager_address_default
	fi
else
	echo "   *** Paramter missing, see usage..."
	echo ""
	usage	# exits after printing out usage
fi


echo ""
echo " Cleanup HCDB script started ! - $timestamp"
echo ""

pushd `dirname $0`
operatorName=`whoami`

echo "   ### Running as user: $operatorName"

if [ "$operatorName" != "$mainOperator" ]; then
	localHCDBs=true
fi


## set HCDB path
if [ $ALIHLT_HCDBDIR ]; then
    HCDB_base=$ALIHLT_HCDBDIR
fi


if [ $localHCDBs != true ]; then
	## Inform Taxi about current Run number
    setRunNumber
fi

## Cleaning up old HCDB folder before releasing T-HCDB and filling new data to HCDB
cleanHCDB  # this was formerly performed after end of run, but better at start up!


if [ $localHCDBs != true ]; then
	## Release Taxi-HCDB (Read-only) version to Pendolino node (AFS)
	echo "   --- Releasing T-HCDB ($T_HCDB_base). "
	/opt/HLT/tools/bin/release-T-HCDB.sh
	# check for exit value of scripts -> but what can be done if failed ?
	retVal=$?
	#echo "   --- Releasing of HCDB returned '$retVal' ('0' means success)"
else
	echo "   ### Skipping release of T-HCDB, due to use of test-operator."
fi

## Set links from Taxi-HCDB to HCDB on Pendoliono node 
if [ $localHCDBs != true ]; then
	setTHCDBLinks
	echo "   +++ Using real HCDB ($HCDB_base)."
else
	
    if [ ! -e $HCDB_base ]; then
        mkdir $HCDB_base
        if [ $? -ne 0 ]; then
            echo "   *** Unable to create operator specific HCDB ($HCDB_base), exiting..."
            exit 9
        fi
    fi
	
	# copy latest objects from T-HCDB to operator-specific HCDB using a AliRoot macro
	aliroot -q -b -l "setHCDB.C(\"$T_HCDB_base\" , \"$HCDB_base\" , $currentRunNumber)"
	retVal=$?
	if [ $retVal -ne 10 ]; then
		echo "   *** Preparing HCDB ($HCDB_base) with T-HCDB ($T_HCDB_base) failed: $retVal."
	else
		echo "   +++ Using operator-specific HCDB ($HCDB_base)"
	fi
fi

## release HCDB (from Pendolino) to DA nodes (AFS)
if [ $localHCDBs != true ]; then
	echo "   --- Releasing HCDB ($HCDB_base). "
	/opt/HLT/tools/bin/release-HCDB.sh
	# check for exit value of scripts -> but what can be done if failed ?
	retVal=$?
	#echo "   --- Releasing of HCDB returned '$retVal' ('0' means success)"
fi


## Notify RunManager that HCDB is prepared.
SlaveLib_listenAddress=$SlaveLib_listenAddress_protocol`hostname`$SlaveLib_listenAddress_append
echo "   ### SlaveLib listen address: $SlaveLib_listenAddress"
echo "   ### Master TaskManager address: $TaskManager_address"
$ALIHLT_DC_BINDIR/TM_Notifier $SlaveLib_listenAddress $TaskManager_address $TM_notifyCommand_HCDBready "--direct"
retVal=$?
if [ $retVal -ne 0 ]; then
	echo "   *** Error in notification about 'HCDB ready': $retVal ."
else
	echo "   +++ Notification for 'HCDB ready' returned: $retVal ."
fi


echo "   +++ Clean up HCDB script finished; when started manually exit with 'Ctrl-C' now.  +++"

popd 
while true
do
	sleep 1
done

## end of script

