/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2007
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>

#include "AliHLTPendolinoFileCatalogueFactory.h"

ClassImp(AliHLTPendolinoFileCatalogueFactory)

using namespace std;
//using namespace alice::hlt::pendolino;



AliHLTPendolinoFileCatalogueFactory::AliHLTPendolinoFileCatalogueFactory() {
	mBasePath = "./";
}


AliHLTPendolinoFileCatalogueFactory::AliHLTPendolinoFileCatalogueFactory
		(const char* basePath) {
	mBasePath = basePath;
	mBasePath.append("/");
}


AliHLTPendolinoFileCatalogueFactory::~AliHLTPendolinoFileCatalogueFactory() {

}

int AliHLTPendolinoFileCatalogueFactory::storeDCSValue(const char* valueName,
		const char* detectorSubPath, float value, UInt_t timestamp) {
	string vName = valueName;
	string dsPath = detectorSubPath;
	return storeDCSValue(vName, dsPath, value, timestamp);
}

int AliHLTPendolinoFileCatalogueFactory::storeDCSValue(string valueName,
		string detectorSubPath, float value, UInt_t timestamp) {
	string valString;
//	valString = "" + value + " Timestamp: " << timestamp;
	return storeDCSValue(valueName, detectorSubPath, valString);
}

int AliHLTPendolinoFileCatalogueFactory::storeDCSValue(string valueName,
		string detectorSubPath, string value) {
	Int_t retval = 0;
	string filename;
	ofstream pfile;

	// open property file
	if (valueName == "") {
		cout << "Filename for storing DCS value is empty" << endl;
		return -1;
	}

	filename = mBasePath + "/" + detectorSubPath + "/" + valueName;

	pfile.open(filename.c_str(), ios::out | ios::app);
    if (!pfile) {
		cout << "Error while opening file for DCS value: " << valueName << endl;
		cout << "Path is: " << filename << endl;
		return -2;
	}

	pfile << value << endl;
			// << " - timestamp: " << timestamp << endl;

	
	pfile.close();		
	return retval;
}

