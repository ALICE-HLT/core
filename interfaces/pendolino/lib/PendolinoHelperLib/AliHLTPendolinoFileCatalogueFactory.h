#ifndef ALI_HLT_PENDOLINO_FILECATALOGUE_FACTORY_HPP
#define ALI_HLT_PENDOLINO_FILECATALOGUE_FACTORY_HPP

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2007
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include <string>

#include <TObject.h>


//namespace alice { namespace hlt { namespace pendolino {


/**
 * This class stores the DCS values fetched by the Pendolino to file.
 *
 * @author Sebastian Bablok
 *
 * @date 2007-02-23
 */
class AliHLTPendolinoFileCatalogueFactory : public TObject {
	public:

		/**
		 * Standart constructor for the AliHLTPendolinoFileCatalogueFactory.
		 * Here the base path for the PendolinoFileCatalogue is set to "./".
		 */
		AliHLTPendolinoFileCatalogueFactory();

		/**
		 * Constructor for AliHLTPendolinoFileCatalogueFactory, which also sets
		 * the base path for the file catalogue
		 *
		 * @param basePath base path for the file catalogue
		 */
		AliHLTPendolinoFileCatalogueFactory(const char* basePath);

		/**
		 * Destructor for the AliHLTPendolinoFileCatalogueFactory.
		 */
		virtual ~AliHLTPendolinoFileCatalogueFactory();

		/**
		 * Stores the given DCS values under the given subpath and file name.
		 * The timestamp for the value is also added to the file
		 *
		 * @param valueName the value name which will be used as filename
		 * @param detectorSubPath subpath name for storing
		 * @param value the value to store
		 * @param timestamp the timestamp to store
		 *
		 * @return 0 on success, else an error code 
		 */
		int storeDCSValue(const char* valueName, const char* detectorSubPath,
				float value, UInt_t timestamp);

		/**
		 * Stores the given DCS values under the given subpath and file name.
         * The timestamp for the value is also added to the file
         *
         * @param valueName the value name which will be used as filename
         * @param detectorSubPath subpath name for storing
         * @param value the value to store
         * @param timestamp the timestamp to store
         *
         * @return 0 on success, else an error code
		 */
		int storeDCSValue(std::string valueName, std::string detectorSubPath, 
				float value, UInt_t timestamp);

		/**
		 * Stores a DCS values given as string under the given subpath and file
         * name. The timestamp for the value is also added to the file
         *
         * @param valueName the value name which will be used as filename
         * @param detectorSubPath subpath name for storing
         * @param value the value to store
         *
         * @return 0 on success, else an error code
		 */
		int storeDCSValue(std::string valueName, std::string detectorSubPath,
				std::string value);

	private:

        /**
         * Base path for the pendolino file catalogue.
         */
		std::string mBasePath;

        /**
         * AliRoot required stuff
         */
        ClassDef(AliHLTPendolinoFileCatalogueFactory, 1);
}; //end of class


//} } } // end namespaces "alice", "hlt" and "pendolino"


#endif

