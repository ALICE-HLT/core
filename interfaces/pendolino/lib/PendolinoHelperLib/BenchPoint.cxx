/**************************************************************************
 * This file is property of and copyright by the ALICE HLT Project        * 
 * All rights reserved.                                                   *
 *                                                                        *
 * Primary Authors: Sebastian Bablok <Sebastian.Bablok@uib.no>            *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          * 
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

#include "BenchPoint.h"

#include <sstream>


using namespace std;


ClassImp(BenchPoint)

BenchPoint::BenchPoint(string startStr) {
	mStartpoint = TTimeStamp();
	mStartString = startStr;
	mStopString = "";
	mStopped = false;
}

BenchPoint::~BenchPoint() {

}

Bool_t BenchPoint::setStoppoint(string stopStr) {
	mStoppoint = TTimeStamp();
	mStopString = stopStr;
	mStopped = true;
	return true;
}

TString BenchPoint::writeBenchOutput() {
/*	stringstream output;
	char time[30];
	char start[30];
	char stop[30];

	time[sprintf(time, "%lf", calculateTimeDiff())] = 0;

	start[sprintf(start, "%.8s - %ld us", 
			asctime(localtime(&(mStartpoint.tv_sec))) + 11, 
			mStartpoint.tv_usec)] = 0;

	stop[sprintf(stop, "%.8s - %ld us",
            asctime(localtime(&(mStoppoint.tv_sec))) + 11,
            mStoppoint.tv_usec)] = 0;

	output << time << " (DURATION); " << start << " (START: " << mStartString <<
			"); " << stop << " (STOP: " << mStopString << ").";

	return output.str();	
*/
	TString output;
	output += (mStoppoint.AsDouble() -  mStartpoint.AsDouble());
	output += "\t(DURATION sec); ";
	output += mStartpoint.AsString("c");
	output += " (START: " + mStartString + "); ";
	output += mStoppoint.AsString("c");
	output += " (STOP: " + mStopString + ").";
	return output;
}


BenchPoint::BenchPoint() {
}

/*
Double_t BenchPoint::calculateTimeDiff() {
	// Compute diffTime in microseconds
	double diffTime = ((mStoppoint.tv_sec * 1000000 + mStoppoint.tv_usec) -
			(mStartpoint.tv_sec * 1000000 + mStartpoint.tv_usec));

	// Convert back to milliseconds
	return (diffTime / 1000);
}
*/


