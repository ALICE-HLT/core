/**************************************************************************
 * This file is property of and copyright by the ALICE HLT Project        *
 * All rights reserved.                                                   *
 *                                                                        *
 * Primary Authors: Sebastian Bablok <Sebastian.Bablok@uib.no>            *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

#include "Benchmarker.h"

#include <fstream>
//#include <stdlib.h>

using namespace std;

ClassImp(Benchmarker)


Benchmarker::Benchmarker() {
	mIsInit = false;
}

//Benchmarker(const char* filename);

Benchmarker::~Benchmarker() {
		
}

Int_t Benchmarker::init(string filename, string openingText) {
	mFilename = filename;
    ofstream pFile;
	TTimeStamp stamp;

	if (mFilename == "") {
		return -1;
	}

	pFile.open(mFilename.c_str(), ios_base::out | ios_base::app);
	if (!pFile) {
		return -1;
	}

	pFile << " --- Benchmark file started at " << stamp.AsString("s") << endl;
	pFile << openingText << endl;
	pFile.close();
	
	mIsInit = true;		
	return 0;
}
		
BenchPoint_t* Benchmarker::setBenchmarkPoint(string startString) {
	return new BenchPoint(startString);
}

Int_t Benchmarker::stopBenchmarkPoint(BenchPoint_t* point, 
		string stopString) {
	if (!point) {
		return -3;
	}
	if (!(point->setStoppoint(stopString))) {
		return -1;
	}

	if (!(writeToFile(point->writeBenchOutput()))) {
		return -2;
	}
	delete point;
	return 0;
}

void Benchmarker::deinit() {
    ofstream pFile;
    TTimeStamp stamp;
	mIsInit = false;
	
	if (mFilename == "") {
    	return;
	}

	pFile.open(mFilename.c_str(), ios_base::out | ios_base::app);
	if (!pFile) {
		mFilename = "";
		return;
	}

	pFile << " +++ Benchmark file stopped at " << stamp.AsString("s") << endl;
	pFile << endl;
	pFile.close();

	mFilename = "";	
}

/*
Int_t Benchmarker::writeBenchmark(BenchPoint_t point) {
		
}
*/

Bool_t Benchmarker::writeToFile(TString benchString) {
	ofstream pFile;

	if (!mIsInit) {
		return false;
	}

	pFile.open(mFilename.c_str(), ios_base::out | ios_base::app);
    if (!pFile) {
		return false;
	}

	pFile << benchString << endl;
	pFile.close();
	return true;
}

