#ifndef ALICE_HLT_BENCHPOINT_HPP
#define ALICE_HLT_BENCHPOINT_HPP

/**************************************************************************
 * This file is property of and copyright by the ALICE HLT Project        * 
 * All rights reserved.                                                   *
 *                                                                        *
 * Primary Authors: Sebastian Bablok <Sebastian.Bablok@uib.no>            *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          * 
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

#include <string>
//#include <sys/time.h>

#include <TObject.h>
#include <TTimeStamp.h>
#include <TString.h>

//namespace alice { namespace hlt { namespace taxi {


/**
 * This class represents a Benchmark point with a start string and a timestamp
 *
 * @author Sebastian Bablok
 *
 * @date 2007-08-01
 */
class BenchPoint : public TObject {
	public:

		/**
		 * Constructor for the BenchPoint
		 */
		BenchPoint(std::string startStr);

		/**
		 * Destructor for the BenchPoint.
		 */
		virtual ~BenchPoint();

		virtual Bool_t setStoppoint(std::string stopStr);

		virtual TString writeBenchOutput();


	protected:

		BenchPoint();
		

	private:

//		Double_t calculateTimeDiff();

		std::string mStartString;

		TTimeStamp mStartpoint;

		std::string mStopString;

		TTimeStamp mStoppoint;

		Bool_t mStopped;


        ClassDef(BenchPoint, 1);
}; //end of class




#endif // ALICE_HLT_BENCHPOINT_HPP

