#ifndef ALICE_HLT_BENCHMARKER_HPP
#define ALICE_HLT_BENCHMARKER_HPP

/**************************************************************************
 * This file is property of and copyright by the ALICE HLT Project        *
 * All rights reserved.                                                   *
 *                                                                        *
 * Primary Authors: Sebastian Bablok <Sebastian.Bablok@uib.no>            *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

#include <string>

#include <TObject.h>
#include <TString.h>

#include "BenchPoint.h"

//namespace alice { namespace hlt { namespace taxi {

typedef class BenchPoint BenchPoint_t;

/**
 * This class allows to set benchmark points in order to benchamrk the 
 * Pendolino. 
 *
 * @author Sebastian Bablok
 *
 * @date 2007-08-01
 */
class Benchmarker : public TObject {
	public:

		/**
		 * Constructor for the Benchmarker.
		 */
		Benchmarker();

		/**
		 * Constructor for Benchmarker, which also provides a file to write 
		 * the benchmark output to.
		 *
		 * @param filename filename to benchamrk to
		 */
//		Benchmarker(const char* filename);

		/**
		 * Destructor for the Benchmarker.
		 */
		virtual ~Benchmarker();

		/**
		 * Initializes the Benchmarker
		 *
		 * @param filename where benchmark output shall be stored
		 * @param openingText start text in benchmark file
		 *
		 * @return zero, if successful else an error code (negative)
		 */
		virtual Int_t init(std::string filename, std::string openingText);
		
		/**
		 * Function to set a benchmark point; the returned BenchPoint pointer
		 * will be cleaned up in the corresponding stopBenchmarkPoint(..) call
		 *
		 * @param startString string defining this Becnhpoint
		 * 
		 * @return pointer to this BenchPoint
		 */
		virtual BenchPoint_t* setBenchmarkPoint(std::string startString = "");

		/**
		 * Function to stop a Benchmark point, the handed in Benchpoint is 
		 * cleaned up during the call and therefore invalid afterwards
		 *
		 * @param point BenchPoint that shall be stopped; the objct will be 
		 * 			cleaned up before leaving the function
		 * @param stopString string defining the end reason of this BenchPoint
		 *
		 * @return zero on success, else an error code (negative)
		 */
		virtual Int_t stopBenchmarkPoint(BenchPoint_t* point, 
				std::string stopString = "");

		/**
		 * Function to deinitialize Benchmarker
		 */
		virtual void deinit();

	protected:

//		virtual Int_t writeBenchmark(BenchPoint_t point);

	private:

		Bool_t writeToFile(TString benchString);

		Bool_t mIsInit;

		std::string mFilename;

//		TList mBenchPointList;

        ClassDef(Benchmarker, 2);
}; //end of class


#endif // ALICE_HLT_BENCHMARKER_HPP

