#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
 
#pragma link C++ global gAlice;
#pragma link C++ global gMC;
 
#pragma link C++ class AliDCSMessage;
#pragma link C++ class AliDCSClient;

#endif
