#ifndef ALI_HLT_PREDICTION_PROCESSOR_TPCBFIELD_H
#define ALI_HLT_PREDICTION_PROCESSOR_TPCBFIELD_H

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2007
** This file has been written by Sebastian Bablok, and a little bit by Gaute Ovrebekk
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include "AliHLTPredictionProcessorInterface.h"

//new
#include <TObjString.h>


/**
 * Predition Processor for prepering b-field values for TPC components. 
 *
 * @author Sebastian Bablok, Gaute Ovrebekk
 *
 * @date 2007-10-24
 */
class AliHLTPredictionProcessorTPCBField : public AliHLTPredictionProcessorInterface {
    public:
	
		/**
		 * Constructor for AliHLTPredictionProcessorTPCBField
		 *
		 * @param detector string defining the detector to which the 
		 * 			PredictionProcessor belongs to
		 * @param pendolino pointer to the hosting pendolino (derived from 
		 * 			AliShuttleInterface)
		 */
		AliHLTPredictionProcessorTPCBField(const char* detector, 
						AliHLTPendolino* pendolino);

		/**
		 * Destructor for AliHLTPredictionProcessorTPCBField
		 */
		virtual ~AliHLTPredictionProcessorTPCBField();
		
		/**
		 * Virtual function to force the Prediction Processor to implement
		 * a function to flag that prediction making is required.
		 * This function is called by the Pendolino before the fetched DCS data
		 * is handed in for (prediction) processing.
		 *
		 * @param doPrediction if true, prediction making shall be switched on,
		 * 			if false, switched off.
		 * 
		 * @return 0 on success; a value greater than 0 refers to an error
		 */
		virtual UInt_t makePrediction(Bool_t doPrediction = true);

		/**
		 * Virtual function, implemented in the detector specific 
		 * PredictionProcessors to initialize them.
		 *
		 * @param run run number
		 * @param startTime start time of data
		 * @param endTime end time of data
		 */
		virtual void Initialize(Int_t run, UInt_t startTime, UInt_t endTime);

		/**
		 * Function called by the Pendolino for each participating subdetector
		 * producing the required condition settings. This includes the 
		 * encoding of a prediction to the values due to the fact, that only 
		 * data up to now can be fetched from DCS (the latest data can also be 
		 * up to 2 min old until it is received on HLT side). To write the data
		 * to the HCDB, the detector specific implementation of this class has 
		 * to call the appropriated storing function provided by the interface.
		 *
		 * @param dcsAliasMap the map containing aliases and corresponding DCS
		 * 			values and timestamps
		 *
		 * @return 0 on success; a value greater than 0 refers to an error
		 */
		virtual UInt_t Process(TMap* dcsAliasMap);

		/**
		 * Indicates if DCS data shall be processed.
		 * NOTE: should always return true, since it is used as prediction 
		 * processor, which will only process DCS data
		 *
		 * @return true if DCS data can be processed, else false. Note: if false
		 * 				the Pendolino would stop, so make sure that it is true.
		 */
		virtual Bool_t ProcessDCS();

        /**
         * Function to let the PredictionProcessor produce dummy input data,
         * that can be used in Pendolino tests, where no DCS Archieve DB is
         * contacted. This function is called by the Pendolino, the result is
         * given back to the PredictionProcessor via the Process(...) call.
         * Since the DCSMaps requested from DCS are detector specific, the
         * PredictionProcessor should know, how the maps look like, that it
         * expects.
         * NOTE: The clean - up (delete) of the TMap will be performed by the
         * Pendolino after the usage. The PredictionProcessor has never to
         * call delete on the returned TMap pointer.
         *
         * @param aliasName optional parameter, that can be given in order to
         *          create a DCSMap for dedicated aliases. For a general test
         *          this paramter should be empty. If more than one alias name
         *          shall be specified, they are separated by blanks " ".
         *
         * @return DCSMap containing dummy data for testing the Pendolino.
         */
        virtual TMap* produceTestData(TString aliasName = "");


        protected:

	private:
		/**
		 * Disabled Copy constructor 
		 * (parent class is disabled so derived class does the same)
		 */
		AliHLTPredictionProcessorTPCBField(
						const AliHLTPredictionProcessorTPCBField& predictPro);

		/**
		 * Disabled Assignment operator
		 * (parent class is disabled so derived class does the same)
		 */
		AliHLTPredictionProcessorTPCBField& operator=(
						const AliHLTPredictionProcessorTPCBField& rhs);


		TObjString mBField;

		/**
		 * Stores if prediction shall be made
		 */
		Bool_t mPredict;

		/**
		 * Stores the run number
		 */
		Int_t mRun;
	
	 	/**
		 * Stores the start time of the to process DCS data
		 */	
		UInt_t mStartTime;
	
	 	/**
		 * Stores the end time of the to process DCS data
		 */	
		UInt_t mEndTime;

		UInt_t ExtractBField(TMap* dcsAliasMap);

		Bool_t GetSensorValue(TMap* dcsAliasMap,const char* stringId, Float_t * value);
		
		ClassDef(AliHLTPredictionProcessorTPCBField, 0);
	
};


inline Bool_t AliHLTPredictionProcessorTPCBField::ProcessDCS() {
	return true;
}

#endif


