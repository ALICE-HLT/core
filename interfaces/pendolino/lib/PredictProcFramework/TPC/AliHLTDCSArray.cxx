/**************************************************************************
 * Copyright(c) 2007, ALICE Experiment at CERN, All rights reserved.      *
 *                                                                        *
 * Author: The ALICE HLT and Off-line Projects.                           *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

#include "TROOT.h"
#include "TArrayF.h"
#include "AliHLTDCSArray.h"


//______________________________________________________________________________________________

AliHLTDCSArray::AliHLTDCSArray(Int_t entries): TObject(),
   fTimeStamp(0),
   fValues(0)
{
 // constructor
 
    fValues.Set(entries);
}

//______________________________________________________________________________________________

AliHLTDCSArray::~AliHLTDCSArray()
{
 // destructor
} 

