#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
 
#pragma link C++ global gAlice;
#pragma link C++ global gMC;
 
// PredictionProcessors TPC
#pragma link C++ class AliHLTPredictionProcessorTPC;
//#pragma link C++ class AliHLTPredictionProcessorTPCBField;
// -> moved to HLT PredictProc
#pragma link C++ class AliHLTDCSArray;

#endif
