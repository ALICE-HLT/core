#ifndef ALIHLTDCSARRAY_H
#define ALIHLTDCSARRAY_H

#include "TArrayF.h"

class AliHLTDCSArray : public TObject {

 public:
   AliHLTDCSArray(Int_t entries=0);
   virtual ~AliHLTDCSArray();
   void SetTime(UInt_t time) {fTimeStamp=time;}
   const UInt_t GetTime() const {return fTimeStamp;}
   void SetValue(Int_t i, Float_t val) {fValues[i]=val;}
   Float_t GetValue(Int_t i) const {return fValues[i];}
   
 protected:  
   UInt_t      fTimeStamp;    // time of DCS reading
   TArrayF     fValues;       // array holding DCS readings
   
 ClassDef(AliHLTDCSArray,1)
};

#endif   

 
