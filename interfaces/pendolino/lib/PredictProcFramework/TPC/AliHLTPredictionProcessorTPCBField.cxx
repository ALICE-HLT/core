#include "AliHLTPredictionProcessorTPCBField.h"

#include <AliCDBMetaData.h>
#include <AliCDBEntry.h>

// new
#include <TObjArray.h>
#include <AliDCSValue.h>

#include <TTimeStamp.h>
#include <TObjString.h>

ClassImp(AliHLTPredictionProcessorTPCBField)

AliHLTPredictionProcessorTPCBField::AliHLTPredictionProcessorTPCBField(
			const char* detector, AliHLTPendolino* pendolino) :
				AliHLTPredictionProcessorInterface(detector, pendolino) {
	mPredict = false;
	mRun = 0;
	mStartTime = 0;
	mEndTime = 0;
	mBField = 0;
}


AliHLTPredictionProcessorTPCBField::~AliHLTPredictionProcessorTPCBField() {

}


UInt_t AliHLTPredictionProcessorTPCBField::makePrediction(Bool_t doPrediction) {
	Log("Prediction switched on");
	mPredict = doPrediction;
	return 0;
}


void AliHLTPredictionProcessorTPCBField::Initialize(Int_t run, UInt_t startTime, 
			UInt_t endTime) {
	mRun = run;
	mStartTime = startTime;
	mEndTime = endTime;

	TString msg("Initialized TPCBField PredictProc. Run: ");
	msg += mRun;
	msg += ", start time: ";
	msg += mStartTime;
	msg += ", end time: ";
	msg += mEndTime;
	msg += ".";	
	Log(msg.Data());

	if (mPredict) {
		Log("TPCBField PredictProc has prediction switched ON.");
	} else {
		Log("Prediction is switched OFF.");
	}
}


UInt_t AliHLTPredictionProcessorTPCBField::Process(TMap* dcsAliasMap) {
  
  if (!dcsAliasMap) return 9;
  if (dcsAliasMap->GetEntries() == 0 ) return 9;

  UInt_t retVal = 0;
  Int_t start = 0;
  TString path2("Config");
  TString path3("BField");
  
  UInt_t BFieldResult = ExtractBField(dcsAliasMap);

  if (BFieldResult != 0) {
	Log(" *** Extraction of BField failed - no entry for HCDB!!");
	return 8;
  }


  //transform dcsAliasMap to ROOT object 
  TString comment("BField");
  AliCDBMetaData meta(this->GetName(), 0, "unknownAliRoot",comment.Data());
  
  if (Store(path2.Data(),path3.Data(),(TObject*) &mBField,&meta,start,kTRUE)) {
    Log(" +++ Successfully stored object ;-)");
  } else {
    Log(" *** Storing of OBJECT failed!!");
    retVal = 7;
  }
  
  return retVal;
}

UInt_t AliHLTPredictionProcessorTPCBField::ExtractBField(TMap* dcsAliasMap){
  
//  TString stringId = "dcs_magnet:Magnet/ALICESolenoid.Current"; // old name
	TString stringId = "L3Current";
  
  Float_t BField = 0; 
  Bool_t bRet = GetSensorValue(dcsAliasMap,stringId.Data(),&BField);

  if(bRet){
	// new	 
    BField = BField/60000; // If we get field, take this away and change SensorValue
   	TString dummy("-bfield ");
   	dummy += BField;
	TObjString dummy2(dummy.Data());
	mBField = dummy2;
	
//    mBField="-bfield ";
//    mBField += BField;
//    
	//new
    Log(Form("BField set to %s",mBField.String().Data()));
    return 0; 
  }
  
  return 1;

}

Bool_t AliHLTPredictionProcessorTPCBField::GetSensorValue(TMap* dcsAliasMap,
							  const char* stringId, Float_t *value)
{
  // return last value read from sensor specified by stringId
  
  TObjArray* valueSet;
  TPair* pair = (TPair*)dcsAliasMap->FindObject(stringId);
  if (pair) {
    valueSet = (TObjArray*)pair->Value();
    Int_t nentriesDCS = valueSet->GetEntriesFast() - 1;
    if(nentriesDCS>=0){
	AliDCSValue *val = (AliDCSValue *)valueSet->At(nentriesDCS);
	//new
	*value=val->GetFloat();
	return kTRUE;
    }
  }
  return kFALSE;
}

TMap* AliHLTPredictionProcessorTPCBField::produceTestData(TString aliasName) {
    TMap* resultMap = 0;

    // here has to come real dummy data :-)
    resultMap = new TMap();
    TTimeStamp tt;
	Float_t fval = 33.3;
    TObjString* name = new TObjString("DummyData");
    AliDCSValue* val = new AliDCSValue(fval, tt.GetTime());
    TObjArray* arr = new TObjArray();
    arr->Add(val);
    resultMap->Add(name, arr);

    return resultMap;
}


