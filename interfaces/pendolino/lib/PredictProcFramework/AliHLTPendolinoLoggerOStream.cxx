#include "AliHLTPendolinoLoggerOStream.h"

#include <iostream>

using namespace std;


ClassImp(AliHLTPendolinoLoggerOStream)


AliHLTPendolinoLoggerOStream::AliHLTPendolinoLoggerOStream() {
	// C-tor of AliHLTPendolinoLoggerOStream

}

AliHLTPendolinoLoggerOStream::~AliHLTPendolinoLoggerOStream() {
	// D-tor of AliHLTPendolinoLoggerOStream

}

void AliHLTPendolinoLoggerOStream::log(const char* detector, const char* msg) {
	// logging function, which prints message to command line
	cout << "   === " << detector << ": " << msg << endl;

}

