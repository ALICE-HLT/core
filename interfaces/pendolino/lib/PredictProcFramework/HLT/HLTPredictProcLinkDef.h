#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
 
#pragma link C++ global gAlice;
#pragma link C++ global gMC;
 
// PredictionProcessors HLT
#pragma link C++ class AliHLTPredicProcTempMonitor;
#pragma link C++ class AliHLTPredictionProcessorHLTBField;

#endif
