#include "AliHLTPredictionProcessorHLTBField.h"

#include <AliCDBMetaData.h>
#include <AliCDBEntry.h>

// new
#include <TObjArray.h>
#include <AliDCSValue.h>

#include <TTimeStamp.h>
#include <TObjString.h>


ClassImp(AliHLTPredictionProcessorHLTBField)

AliHLTPredictionProcessorHLTBField::AliHLTPredictionProcessorHLTBField(
			const char* detector, AliHLTPendolino* pendolino) :
				AliHLTPredictionProcessorInterface(detector, pendolino) {
	// C-tor for AliHLTPredictionProcessorHLTBField
	mPredict = false;
	mRun = 0;
	mStartTime = 0;
	mEndTime = 0;
	mBField = 0;
}


AliHLTPredictionProcessorHLTBField::~AliHLTPredictionProcessorHLTBField() {
	// D-tor for AliHLTPredictionProcessorHLTBField
}


UInt_t AliHLTPredictionProcessorHLTBField::makePrediction(Bool_t doPrediction) {
	// switch for prediction making
	Log("Prediction switched on");
	mPredict = doPrediction;
	return 0;
}


void AliHLTPredictionProcessorHLTBField::Initialize(Int_t run, UInt_t startTime, 
			UInt_t endTime) {
	// initializes AliHLTPredictionProcessorHLTBField
	mRun = run;
	mStartTime = startTime;
	mEndTime = endTime;

	TString msg("Initialized HLTBField PredictProc. Run: ");
	msg += mRun;
	msg += ", start time: ";
	msg += mStartTime;
	msg += ", end time: ";
	msg += mEndTime;
	msg += ".";	
	Log(msg.Data());

	if (mPredict) {
		Log("HLTBField PredictProc has prediction switched ON.");
	} else {
		Log("Prediction is switched OFF.");
	}
}


UInt_t AliHLTPredictionProcessorHLTBField::Process(TMap* dcsAliasMap) {
	// processes the DCS value map
  
  if (!dcsAliasMap) return 9;
  if (dcsAliasMap->GetEntries() == 0 ) return 9;

  UInt_t retVal = 0;
  Int_t start = 0;
  TString path2("ConfigHLT"); // "Config" 
  TString path3("SolenoidBz"); // "BField"
  
  UInt_t BFieldResult = ExtractBField(dcsAliasMap);

  if (BFieldResult != 0) {
	Log(" *** Extraction of BField failed - no entry for HCDB!!");
	return 8;
  }


  //transform dcsAliasMap to ROOT object 
  TString comment("BField");
  AliCDBMetaData meta(this->GetName(), 0, "unknownAliRoot",comment.Data());
  
  if (Store(path2.Data(),path3.Data(),(TObject*) &mBField,&meta,start,kTRUE)) {
    Log(" +++ Successfully stored object ;-)");
  } else {
    Log(" *** Storing of OBJECT failed!!");
    retVal = 7;
  }
  
  return retVal;
}

UInt_t AliHLTPredictionProcessorHLTBField::ExtractBField(TMap* dcsAliasMap){
  // extracts the b-field from DCS value map
//  TString stringId = "dcs_magnet:Magnet/ALICESolenoid.Current"; // old name
	TString stringId = "L3Current";
  
  Float_t BField = 0; 
  Bool_t bRet = GetSensorValue(dcsAliasMap,stringId.Data(),&BField);

  if(bRet){
	// new	 
    BField = BField/60000; // If we get field, take this away and change SensorValue
   	TString dummy("-bfield ");
   	dummy += BField;
	TObjString dummy2(dummy.Data());
	mBField = dummy2;
	
//    mBField="-bfield ";
//    mBField += BField;
//    
	//new
    Log(Form("BField set to %s",mBField.String().Data()));
    return 0; 
  }
  
  return 1;

}

Bool_t AliHLTPredictionProcessorHLTBField::GetSensorValue(TMap* dcsAliasMap,
							  const char* stringId, Float_t *value)
{
	// extracts the sensor value
  // return last value read from sensor specified by stringId
  
  TObjArray* valueSet;
  TPair* pair = (TPair*)dcsAliasMap->FindObject(stringId);
  if (pair) {
    valueSet = (TObjArray*)pair->Value();
    Int_t nentriesDCS = valueSet->GetEntriesFast() - 1;
    if(nentriesDCS>=0){
	AliDCSValue *val = (AliDCSValue *)valueSet->At(nentriesDCS);
	//new
	*value=val->GetFloat();
	return kTRUE;
    }
  }
  return kFALSE;
}

TMap* AliHLTPredictionProcessorHLTBField::produceTestData(TString aliasName) {
	// produces test data for AliHLTPredictionProcessorHLTBField
    TMap* resultMap = 0;

    // here has to come real dummy data :-)
    resultMap = new TMap();
    TTimeStamp tt;
	Float_t fval = 33.3;
    TObjString* name = new TObjString("L3Current");
    AliDCSValue* val = new AliDCSValue(fval, tt.GetTime());
    TObjArray* arr = new TObjArray();
    arr->Add(val);
    resultMap->Add(name, arr);

    return resultMap;
}


