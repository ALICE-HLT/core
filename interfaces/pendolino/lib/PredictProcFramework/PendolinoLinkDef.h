#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
 
#pragma link C++ global gAlice;
#pragma link C++ global gMC;
 
#pragma link C++ class AliHLTPendolino;
#pragma link C++ class AliHLTPredictionProcessorInterface;
#pragma link C++ class AliHLTPendolinoLogger;

#pragma link C++ class AliHLTPendolinoLoggerOStream;
//#pragma link C++ class AliHLTPredicProcTempMonitor;

#pragma link C++ class AliHLTPredictionProcessorDummy;
#pragma link C++ class AliHLTPendolinoLoggerDump;

//PredictionProcessors
//#pragma link C++ class AliHLTPredictionProcessorTPCBField;

#endif
