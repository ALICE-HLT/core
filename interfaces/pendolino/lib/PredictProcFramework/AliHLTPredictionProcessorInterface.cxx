#include "AliHLTPredictionProcessorInterface.h"
#include "AliHLTPendolino.h"
//#include "AliShuttleInterface.h"


ClassImp(AliHLTPredictionProcessorInterface)

AliHLTPredictionProcessorInterface::AliHLTPredictionProcessorInterface(
			const char* detector, AliHLTPendolino* pendolino) : 
			AliPreprocessor(detector, reinterpret_cast<AliShuttleInterface*>
					(pendolino)) {
	mPend = pendolino;
}


AliHLTPredictionProcessorInterface::~AliHLTPredictionProcessorInterface() {

}

Int_t AliHLTPredictionProcessorInterface::GetRunNumber() {
	return mPend->GetRunNumber();
}


Bool_t AliHLTPredictionProcessorInterface::includeAliCDBEntryInList(
            const TString& entryPath) {

    return mPend->includeAliCDBEntryInList(entryPath);
}



