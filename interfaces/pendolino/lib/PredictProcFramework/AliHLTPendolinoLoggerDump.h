#ifndef ALI_HLT_PENDOLINO_LOGGER_DUMP_H
#define ALI_HLT_PENDOLINO_LOGGER_DUMP_H

/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2007
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/


#include "AliHLTPendolinoLogger.h"



/**
 * Class that implements the interface for a Pendolino Logger and dumps 
 * the log messages. No output is made! 
 *
 * @author Sebastian Bablok
 *
 * @date 2007-10-29
 */
class AliHLTPendolinoLoggerDump : public AliHLTPendolinoLogger {
    public:

		/**
		 * Constructor for AliHLTPendolinoLoggerDump
		 */
		AliHLTPendolinoLoggerDump();

		/**
		 * Destructor for AliHLTPendolinoLoggerDump
		 */
		virtual ~AliHLTPendolinoLoggerDump();

		/**
		 * Implementated logging interface
		 *
		 * @param detector the detector for which the log entry shall be made.
		 * @param msg the log message
		 */
		virtual void log(const char* detector, const char* msg);

	protected:

	private:

		ClassDef(AliHLTPendolinoLoggerDump, 0);
};

#endif

