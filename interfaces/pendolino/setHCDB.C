/************************************************************************
 **
 **
 ** This file is property of and copyright by the Department of Physics
 ** Institute for Physic and Technology, University of Bergen,
 ** Bergen, Norway, 2008
 ** This file has been written by Sebastian Bablok,
 ** sebastian.bablok@ift.uib.no
 **
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 *************************************************************************/


#include <iostream>


using namespace std;


/** Static string to define a local storage for the OCDB contact. */
static TString LOCAL_STORAGE_DEFINE = "local://";

/** Name of the local folder, where the HCDB is located */
static TString THCDBFolder = "/opt/T-HCDB";

/** WildCard define for the detector subfolders */
static TString wildCard = "/*";

/** list storing the detector folder names. */
static TObjArray detList;


/**
* Function to fill the detector list
*/
void FillList() {
	detList.Add(new TObjString("HLT"));
	detList.Add(new TObjString("ITS"));
	detList.Add(new TObjString("TPC"));
	detList.Add(new TObjString("TRD"));
	detList.Add(new TObjString("TOF"));
	detList.Add(new TObjString("PHOS"));
	detList.Add(new TObjString("HMPID"));
	detList.Add(new TObjString("EMCAL"));
	detList.Add(new TObjString("MUON"));
	detList.Add(new TObjString("FMD"));
	detList.Add(new TObjString("ZDC"));
	detList.Add(new TObjString("PMD"));
	detList.Add(new TObjString("T0"));
	detList.Add(new TObjString("VZERO"));
	detList.Add(new TObjString("GRP"));
	detList.Add(new TObjString("ACORDE"));
}

/**
 * Function to delete detector list
 */
void DeleteList() {
	detList.Delete();
}

/**
 * Prepares a test-HCDB for a given partition. This can be one of the different
 * operators (but not the global hlt-operator - he has to use the real HCDB).
 *
 * @param THCDBbase base folder of the THCDB
 * @param HCDBbase base folder of the HCDB
 * @param runNumber the current run number
 *        
 * @return 10 on success, else an error code [NOTE: 0 is returned by AliRoot,
 * 			when a segmentation fault occured].
 * 			10:	Success (or an error which is not notable by application)
 * 			7:  Unable to get CDB Manager Instance
 * 			6:  Unable to reach T-HCDB storage
 * 			5:  Unable to set HCDB storage
 * 			4:  Unable to fetch entries form T-HCDB
 * 			0:	Segmentation fault
 */
Int_t setHCDB(TString THCDBbase, TString HCDBbase, Int_t runNumber) {

	if (!THCDBbase.IsNull()) {
		THCDBbase = THCDBFolder;
	}
	
	// setting the different path to storages
	char THCDBpath[150] = (LOCAL_STORAGE_DEFINE + THCDBbase).Data();
	char HCDBpath[150] = (LOCAL_STORAGE_DEFINE + HCDBbase).Data();
	
	// init calibration object vars
	Int_t totalFileCounter = 0;
	Int_t storeCounter = 0;
	AliCDBEntry *calibObject = 0;

	FillList();

	// get CDB manager
    AliCDBManager *man = AliCDBManager::Instance();
    if (man) {
		cout << "    --- Got a CDB Manager reference. " << endl;
	} else {
		cout << "    *** ERROR cannot obtain a CDB Manager reference." << endl;
		cout << "    *** Exiting now " << endl << endl;
		return 7;
	}

	// get T-HCDB storage
	AliCDBStorage *thcdb_store = man->GetStorage(THCDBpath);
	if (thcdb_store) {
		cout << "    --- Contacted T-HCDB storage: " << THCDBpath << endl;
	} else {
		cout << "    *** ERROR contacting T-HCDB storage:" << THCDBpath << endl;
		cout << "    *** Exiting now " << endl << endl;
		return 6;
	}

	// set HCDB storage 
	AliCDBStorage *hcdb_store = man->GetStorage(HCDBpath);
	if (hcdb_store) {
		cout << "    --- Set HCDB storage:" << HCDBpath << endl;
	} else {
		cout << "    *** ERROR in initiating HCDB storage:" << HCDBpath << endl;
		cout << "    *** Exiting now " << endl << endl;
		man->DestroyActiveStorages();
		return 5;
	}
	// Print info about aquired storages
	man->Print();
	// --- Initialisation finished ---

	// give run number for requests
	thcdb_store->QueryCDB(runNumber);
	cout << "    --- Query T-HCDB for Run nr. " << runNumber << endl;

	for (Int_t i = 0; i < detList.GetEntries(); i++) {
	
		TList* objList;
		AliCDBEntry* fetchedObject;
	
		// get all entries
		char* detPath = ((TObjString*) (detList.At(i)))->GetString().Append(wildCard).Data();
		AliCDBPath reqID(detPath);
		cout << "    ### requesting: " << detPath << ", runNumber: " << runNumber << endl;
		objList = thcdb_store->GetAll(reqID, runNumber);
		if (!objList) {
			cout << "    *** Unable to fetch collection of calibration objects for '"
					<< detPath << " from " << thcdb_store->GetURI() << endl;
			continue;
		} // end of check if list is valid
	
		for (Int_t j = 0; j < objList->GetSize(); j++) {
			totalFileCounter++;
			fetchedObject = dynamic_cast<AliCDBEntry*> (objList->At(j));
			if (!fetchedObject) {
				cout << "    *** List of fetched calibration objects for '"  << 
						detPath << " from " << thcdb_store->GetURI() << 
						" contains an empty element; skipping..." << endl;
				continue;
			} // end of check for empty list element
				
			AliCDBId query = fetchedObject->GetId();
			cout << "    ### Inserting new object (" << query.GetPath() << 
					") from list of fetched calibration objects." << endl;
		
			// store Calib object locally
			if (hcdb_store->Put(fetchedObject)) {
				cout << "    +++ Copied '"  << query.GetPath() << "' to HCDB." << endl;
				++storeCounter;
			} else {
				cout << "    *** Unable to store '" << query.GetPath() << 
						"' to HCDB" << endl;
			} // end of putting back object					
		} // end of test for local object
	} // and of list loop

	DeleteList();

	// summary:
	cout << "    === Summary: ===" << endl;
	cout << "    Files fetched from T-HCDB: " << totalFileCounter << ";" << endl;
	cout << "    Files copied to HCDB: " << storeCounter << ";" << endl;
	cout << "    Nr. of Errors during HCDB setup: " << 
			(totalFileCounter - storeCounter) << "." << endl;
	cout << endl;

	// clean up in CDB Manager
	cout << "   --- Cleaning up STORAGE Managers ... " << endl;
	man->DestroyActiveStorages();
	man->Print();

	return 10;
}

