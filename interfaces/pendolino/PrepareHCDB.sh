#!/bin/bash

#/****************************************************************************/
#**
#**
#** This file is property of and copyright by the Department of Physics
#** Institute for Physic and Technology, University of Bergen,
#** Bergen, Norway, 2007
#** This file has been written by Sebastian Bablok,
#** sebastian.bablok@ift.uib.no
#**
#** Important: This file is provided without any warranty, including
#** fitness for any particular purpose.
#**
#**
#******************************************************************************/


## This script prepares the HCDB: copying folder-structure of T-HCDB to the 
## HCDB and setting of links for each entry (files) in T_HCDB to the HCDB.
## The script calls itself recursively to move along the folder structure.
## The first call should be done with only two parameters (see usage below),
## the recursive call passes in a recursiveCounter in addition. In each call
## this counter is incremented. To avoid an endless loop, the procedure is 
## aborted, when recursiveCounter exceeds maxRecursiveCount.


## definition of variables
# variables that can be adjusted before calling
maxRecursiveCount=20	# maximum allowed recursive calls

# script vars
THCDBpath=""
HCDBbase=""
recursiveCounter=0
timestamp=`date "+%Y-%m-%d %H:%M:%S"`


## Functions
## prints out the usage of this script
function usage {
    echo "Usage:"
    echo "  $0 <THCDB_path> <HCDB_base>"
    echo ""
    echo "    THCDB_path: The path to the T-HCDB (should be given as absolute path)."
    echo ""
    echo "    HCDB_base: The path to the base folder of the HCDB."
    echo "                (should be given as absolute path)"
    echo ""
    echo ""
    echo " e.g. $0 /opt/T-HCDB /opt/HCDB"
    echo ""
    exit 9
}

## function to update the timestamp in timestamp var
function setTimeStamp {
    timestamp=`date "+%Y-%m-%d %H:%M:%S"`
}


## main of the script
# check for required parameters
if [ $1 ] && [ $2 ]; then
	if [ $1 = "--help" ]; then
        usage   # exits after printing out usage
    fi

    THCDBpath=$1
    HCDBbase=$2
	if [ $3 ]; then
		recursiveCounter=$3
	else
		echo ""
		echo " Script for preparing HCDB started!"
		echo ""

	fi
else
	echo "   *** Paramter missing, see usage..."
	echo ""
    usage   # exits after printing out usage
fi

let "recursiveCounter++"

# Debug output
#echo "   --- Settings: THCDBpath=$THCDBpath; HCDBbase=$HCDBbase; counter=$recursiveCounter"

# abort if too many recursive calls
if [ $recursiveCounter -gt $maxRecursiveCount ]; then
	setTimeStamp			# update time stamp
	echo "   *** ERROR: Recursive counter exceeded maximum ($maxRecursiveCount), exiting... ($timestamp)"
	exit 7
fi

# mount through folder structure and prepare HCDB
for i in $( ls $THCDBpath ); do
	path=$THCDBpath/$i
	if [ -d $path ]; then
#		echo "   --- Creating folder '$HCDBbase/$i'"
		mkdir $HCDBbase/$i
		# recursive call
		./PrepareHCDB.sh $path $HCDBbase/$i  $recursiveCounter
		retVal=$?
		if [ $retVal -ne 0 ]; then
			exit $retVal
		fi
	elif [ -f $path ]; then
#		echo "   --- Setting link '$HCDBbase/$i' to '$path'"
		ln --symbolic $path $HCDBbase/$i
	else
		echo "   ~~~ Unknown type ($path) in T-HCDB, ignoring entry."
	fi 
done

if [ $recursiveCounter -eq 1 ]; then
	setTimeStamp			# update time stamp
	echo "   +++ Preparing HCDB script finished ($timestamp) +++"
	echo ""
fi

exit 0

## end of script

