#!/bin/bash


#/****************************************************************************/
#**
#**
#** This file is property of and copyright by the Department of Physics
#** Institute for Physic and Technology, University of Bergen,
#** Bergen, Norway, 2007
#** This file has been written by Sebastian Bablok,
#** sebastian.bablok@ift.uib.no
#**
#** Important: This file is provided without any warranty, including
#** fitness for any particular purpose.
#**
#**
#******************************************************************************/

## This script is used to wrap the start script of the Pendolino for allowing
## the RunManager to start the Pendolino and redirect the log file output.


## Vars
versionNumber="1.3.0"
runNumber=0
TMaddress=""
numPendolinos=0
logFile=""
logFileDefault="/tmp/pendolino-starter.log"
opName=""
sleepTime=0
PendoPid=0

detectorList="INIT"
beamType="INIT"
runType="INIT"


## Functions
function usage {
    echo "Usage ($versionNumber):"
    echo "  $0 <Current_Run_Number> <Master_TaskManager_Address> <Num_of_Pendolinos>"
	echo "             <Log_File_Name> [Beam_Type] [Run_Type] [Detector_List]"
    echo ""
    echo "    Current_Run_Number: The run number of the current run."
    echo "                (has to be provided by the RunManager)"
    echo ""
    echo "    Master_TaskManager_Address: The address, where the RunManager can"
	echo "                 be contacted for notifying about HCDB updates."
    echo ""
    echo "    Num_of_Pendolinos: The number of Pendolinos, that shall be started."
    echo "                Only a number of 1 to 3 Pendolinos are accepted. "
    echo ""
	echo "    Log_File_Name: The log file name, where the Pendolino starter"
	echo "                script puts its log output. Normally it should be"
	echo "                set to '$logFileDefault'."
	echo ""
	echo "    Beam_Type: The beam type, used for the initial GRP T-HCDB entries"
	echo "                (this is given by ECS)."
	echo ""
	echo "    Run_Type: The current run type, used for the initial GRP T-HCDB"
	echo "                entries (this is given by ECS)."
	echo ""
    echo "    Detector_List: List of participating detectors, used for the initial"
    echo "                GRP T-HCDB entry (this is given by ECS)."
	echo ""
	echo ""
    echo " e.g. $0 66 tcpmsg://portal-ecs0.internal:20100 2 $logFileDefault pp PHYSICS ALICE_ALL"
	echo ""
	exit 9
}

# relay signal for exit to StartPendolino.sh
function signalExit {
	trap '' 1 2 3 6 15  # disable signals

	echo "   ### Caught exit signal .... killing $PendoPid "
	kill -a -s 12 $PendoPid # signal exit to StartPendolino.sh 
	# has to use signal 12 (SIGUSR2), else it is not received in subscript :-(
	echo "   ### after killing"

	wait # wait for all child process to be finished
	echo "   ### finished waiting"
	sleep 1
	exit 0
}


## Start of main
if [ $1 ] && [ $2 ] && [ $3 ]; then
    if [ $1 = "--help" ]; then
        usage
    fi
	runNumber=$1
	TMaddress=$2
	numPendolinos=$3
    if [ $4 ]; then
        logFile=$4 
	else
		logFile=$logFileDefault  #uses default 
    fi

# if more paramter the GRP intial preparation should be started
	if [ $5 ] && [ $6 ] && [ $7 ]; then   # more might come
		beamType=$5
		runType=$6
		detectorList=$7
		# prepare the initial GRP entries
		./initGRPEntries.sh $runNumber $detectorList $beamType $runType
	fi

	opName=`whoami`
    if [ $opName != "hlt-operator" ]; then
        logFile=$logFile-$opName
    fi
else
    # not all necessary parameters provided
    echo "   *** Parameters missing, see usage ... ***"
    usage
fi

trap 'signalExit' 1 2 3 6 15

pushd `dirname $0`
./StartPendolino.sh $runNumber $TMaddress $numPendolinos $beamType $runType $detectorList > $logFile 2>&1 &
PendoPid=$!
popd

echo "   ### Started Startup-script (v. $versionNumber)"

let sleepTime=$numPendolinos+1

while true
do
	sleep $sleepTime  #5 
done

#end


