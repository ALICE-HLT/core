#!/bin/bash

#/****************************************************************************/
#**
#**
#** This file is property of and copyright by the Department of Physics
#** Institute for Physic and Technology, University of Bergen,
#** Bergen, Norway, 2007
#** This file has been written by Sebastian Bablok,
#** sebastian.bablok@ift.uib.no
#**
#** Important: This file is provided without any warranty, including
#** fitness for any particular purpose.
#**
#**
#******************************************************************************/

## This script sets the required environment variables and prepares the needed
## AliRoot stuff to start the Pendolino. Afterwards it starts the Pendolino.

## definition of variables
ListFile=""
HCDBBase=""
AmandaHost="alidcsamanda.cern.ch"		# old test setup: "10.160.33.92"
AmandaPort=1337							# old test setup: 4242
timeInterval=60
currentRunNumber=0

sleepTime=0
startShift=0
endShift=0

maxTest=100
testCounter=0
sleepCounter=0

#ParentPID=$PPID
ParentPID=$START_PENDOLINO_SCRIPT_PID
retVal=0
errorCounter=0
maxErrorCounter=100
timestamp=`date "+%Y-%m-%d %H:%M:%S"`
BenchmarkFile="/tmp/Pendolino-Benchmark.txt"

beamType="INIT"
runType="INIT"
detectorList="INIT"


SlaveLib_listenAddress_protocol="tcpmsg://"
            # The protocol for the local listen address of the SlaveControl lib,
            # used to contact the TaskManager (notifying about HCDB update).
SlaveLib_listenAddress_append=".internal:"
            # string that shall be appended to the local listen address of the
            # SlaveControl lib after the hostname. The end of address
            # represents domain.
            # complete address example: [tcpmsg://portal-dcs0.internal:42025]
SlaveLib_listenAddress_port_base=42025 
			# The port number base of the listen address. In order to have 
			# different TMNotifiers running at the same time (for the different
			# Pendolino: fast, normal, slow) this base is added to the 
			# timeinterval of the corresponding Pendolino. E.g. fast Pendolino
			# sleeps 60 secs -> portnumber: 42085 
            # Don't use 42024 as port; this is used by HCDBManager script, which
            # inserts configuration entries to HCDB
TaskManager_address=""
            # address of the TaskManger that shall be notified
            # [tcpmsg://portal-ecs0.internal:20100]
TM_notifyCommand="trigger_reconfigure_event temporary"
            # command that shall be used to notify the TaskManager about the
            # HCDB update, has to be synchonized with TaskManager
            # [remove '_test' for real event].
TM_Command=""
			# Stores the complete command, that the Pendolino has to execute
			# for notifying the TaskManager about HCDB update
SlaveLib_listenAddress_port=0
			# stores the compiled listen port number 
initialTimeOffset=300
			# initial request time offset for the first Pendolino request
			# This ensures, that the request will cover the SOR update
			# of the DCS Archive DB, if it was already included in DB.
			# 5 min (300 sec) have been considered as save time limits.
PVSSshippingTime=70
			# this time is substracted from the request starting point,
			# therefore increasing the time interval. The timestamp of DCS 
			# values is attached to them, when they are recoreded in PVSS,
			# not when included in the DCS Archive DB. The shipping from
			# PVSS to the DB can take some time. The maximum shipping time
			# is measuremed with 60 secs. In order to have the correct time
			# interval covered, the request interval has to be extended to 
			# the past. 70 sec give a safety buffer for the request time
			# (60 sec shipping time + 10 sec safety buffer)


## Functions

## prints out the usage of this script
function usage {
	echo "Usage:"
	echo "  $0 <DPlist_file> <Amanda_host> <Amanda_port> <HCDB_path> <Run> "
	echo "                   <TaskManager_address> <SL_port> [Time_interval]"
	echo "                   [Beam_Type] [Run_Type] [Detector_List]"
	echo ""
	echo "    DPlist_file: name of file containing the merged lists of Datapoints."
	echo "                All Datapoints listed in that file are tried to be fetched"
	echo "                by the Pendolino. If the file is locted in a different"
	echo "                folder, the complete path has to be prefixed to the name."
	echo ""
	echo "    Amanda_host: the IP of the Amanda server on DCS side."
	echo "                (Default is 'alidcsamanda.cern.ch'.)"
	echo ""
	echo "    Amanda_port: the port number of the Amanda server."
	echo "                (Default is '1337'.)" 
	echo ""
	echo "    HCDB_path: the path to the base folder of the HCDB (or where the values"
	echo "                shall be stored.)"
	echo ""
	echo "    Run: the current run number."
	echo ""
	echo "    TaskManager_address: The address where the TaskManager can be contacted."
	echo ""
	echo "    SL_port: The port that shall be used for the TaskManager SlaveLibrary"
	echo "                for listening."
	echo ""
	echo "    Time_interval: the time intervals after which the Pendolino should repeat"
	echo "                its job of requesting the DCS Archive DB - given in seconds."
	echo "                Optional! Default is 60 seconds."
	echo ""
	echo "    Beam_Type, Run_Type, Detector_List (optional): Initial run parameters given by"
	echo "                ECS at start up. NOTE: When these paramerts are given, you have to"
	echo "                provide 'Time_interval' as well."
	echo ""
	echo ""
	echo " e.g. $0 /opt/T-HCDB/lists/lists-pendolino/fast/ListMerged.txt alidcsamanda.cern.ch 1337 /opt/HCDB 666 60"
	echo ""
	exit 9
}

## function to update the timestamp in timestamp var
function setTimeStamp {
    timestamp=`date "+%Y-%m-%d %H:%M:%S"`
}


## function to intialize all required AliRoot stuff
#function initAliRoot {
## NOT needed, modules shall be sourced by operator in advance
#	echo "   --- Initialize AliRoot."
#	echo ""
#	source ~/addEnv.sh
#}


#function signalUpdatedHCDB {
#	echo "   --- signaling HCDB updated to parent process ($ParentPID)"
#	kill -a -s 12 $ParentPID
#}


## clean up after interrupt
function cleanUp {
	# make clean exit
	setTimeStamp				# update time stamp
	echo "   +++ Pendolino driver script ($$) stopped by signal ($timestamp) +++"
	exit 0
}



## main part of script

## check for provided parameters
if [ $1 ] && [ $2 ] && [ $3 ] && [ $4 ] && [ $5 ] && [ $6 ] && [ $7 ]; then
    if [ $1 = "--help" ]; then
        usage
    fi
	if [ $8 ]; then
        timeInterval=$8 #uses default (60) if not set
    fi
	# set variables from parameters
	ListFile="$1"
	AmandaHost="$2"
	AmandaPort=$3
	HCDBBase="$4"
	currentRunNumber=$5
	TaskManager_address=$6
	SlaveLib_listenAddress_port=$7
	let startShift=`date +%s`-$initialTimeOffset		#60000 # UTC time is used	
	endShift=`date +%s`		# UTC time is used, default when using "+%s"

	if [ $9 ]; then
		beamType=$9
	fi

	if [ ${10} ]; then
		runType=${10}
	fi

	if [ ${11} ]; then
		detectorList=${11}
	fi

else
    # not all necessary parameters provided
    echo "   *** Parameters missing, see usage ..."
    usage
fi


echo ""
echo " Pendolino driver script ($$) started! ($timestamp)"
echo ""

# Prepare TM command
#let SlaveLib_listenAddress_port=$SlaveLib_listenAddress_port_base+$timeInterval # given from parent script
## prepare addresses for Master TaskManager notification
SlaveLib_listenAddress=$SlaveLib_listenAddress_protocol`hostname`$SlaveLib_listenAddress_append$SlaveLib_listenAddress_port
TM_Command="$ALIHLT_DC_BINDIR/TM_Notifier $SlaveLib_listenAddress $TaskManager_address "
#echo "   ### TM Command: $TM_Command"
#echo "   ### SlaveLib listen address: $SlaveLib_listenAddress"
#echo "   ### Master TaskManager address: $TaskManager_address"


## register signal handler for catching all SIGUSR1 (10) signal to perform a
## clean exit - the same for SIGINT (2)
trap 'cleanUp' 10 2

## Initialize environmental vars for AliRoot
#initAliRoot

#################
# this loop is only used during benchmarking time
#while [ $testCounter -lt $maxTest ]; do
#	let testCounter=$testCounter+1
#	echo ""
#			
#	echo "Test-Run: $testCounter of $maxTest"
#	echo ""
#################

## provide settings for Pendolino
echo "   --- Settings (PID: $$):"
echo "       Listfile: '$ListFile', Interval: $timeInterval sec,"
echo "       Host: '$AmandaHost', Port: $AmandaPort,"
echo "       HCDB base: '$HCDBBase', Run: $currentRunNumber."
echo "       TM Notifier command base: $TM_Command"
echo "       initial time offset in first request: $initialTimeOffset"
echo "       time offset for shipping data (PVSS->DB): $PVSSshippingTime"

echo "   --- Run Parameter (PID: $$):"
echo "       Beam Type: $beamType, Run Type: $runType, Detectors: $detectorList"


## start loop for repetition of Pendolino
while true
do

	## run Pendolino
	setTimeStamp				# update time stamp
	echo ""
	echo "   --- (Re-) Running Pendolino now ($timestamp); shift $startShift - $endShift ."
	aliroot -q -b -l "runPendolino.C($currentRunNumber, $startShift, $endShift, \"$TM_Command\",  \"$ListFile\", \"$AmandaHost\", $AmandaPort, \"$HCDBBase\", \"$beamType\", \"$runType\", \"$detectorList\")"
#	aliroot -q -b -l "runPendolino.C($currentRunNumber, $startShift, $endShift, \"$ListFile\", \"$AmandaHost\", $AmandaPort, \"$HCDBBase\", \"$beamType\", \"$runType\", \"$detectorList\", \"$BenchmarkFile\")"

	retVal=$?
	if [ $retVal -eq 0 ]; then
		echo "   --- AliRoot Pendolino (PID: $$) call returned with value '$retVal'."
	else
		echo "   ~~~ AliRoot Pendolino (PID: $$) encountered errors (retVal: '$retVal'), but might be able to continue."
	fi
	
	# check of return value: if errorCounter > maxErrorCounter exit this Pendolino
	# the PendolinoDP.C macro return error code numbers ordered by there severity
	# if the accumulated errors exceed a certain number a restart makes no sence
# Taken out, since there might just one detector PredictionProcessors,
# not working correctly the rest might 
#	let errorCounter=$errorCounter+$retVal
#	if [ $errorCounter -gt $maxErrorCounter ]; then
#		setTimeStamp                # update time stamp
#		echo "   *** Pendolino (PID: $$) encountered too many errors; exiting for this run ($timestamp)."
#		exit 9
#	fi

# NOTE: since v0.9.3 this is done inside the Pendolino AliRoot macro!
#	## release updated HCDB to DA nodes (AFS)
#	# release HCDB to DA nodes --> moved to parent script to have a serialized call
#	## notify PubSub of new data in HCDB
#	# notifyPubSub  # --> moved to parent script to have a serialized call
#	if [ $retVal -eq 0 ]; then 		
#		# call signal func only when aliroot-Pendolino call was successfull
#		signalUpdatedHCDB	# signals the update of HCDB to parent process 
#							# release and notification are performed there.
#	fi

	## sleep time interval
	echo "   --- Repeating Pendolino in $timeInterval seconds, sleeping ..."
	let sleepTime=$timeInterval
	let sleepCounter=0
	while [ $sleepCounter -lt $sleepTime ]
	do
		sleep 1 #$sleepTime
		let ++sleepCounter
	done

	# set new borders of request timeinterval 
	startShift=$endShift-$PVSSshippingTime  # increased timeinterval
	endShift=`date +%s`      # uses UTC time
	
done


## This part should never be reached.
echo "   *-* Pendolino driver script ($$) finished -*-"


## end of script

