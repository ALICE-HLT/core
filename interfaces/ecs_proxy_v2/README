	
ECS HLT Proxy 
=============

This package contains the tools needed on HLT side to for interfacing 
the ECS. These are:

	1) ecs_proxy
	2) ecs_gui
	3) rm_proxy



GENERAL DESCRIPTION
-------------------

On ECS side there is an SMI server that hosts a state machine encoding the 
states the HLT can be in. State transitions ('commands') are published to 
interested parties using the DIM transport protocol. One such interested 
party is the ecs_proxy. The ecs_proxy initiates state transitions in HLT 
and reports new states back to the SMI server.  

The ecs_proxy does not execute state transitions itself, but merely acts as 
a proxy on HLT side for interfacing the SMI server. All received commands 
are forwarded to rm_proxy, which interfaces and steers the HLT runmanager. 
Once the HLT runmanager has executed a command and HLT is in a new state, 
the rm_proxy sends back the new state to ecs_proxy which in turn announces 
it to ECS. ecs_proxy is written in C++ while rm_proxy is written in Python.
Both communicate via XML-RPC. 

The ecs_gui is a tool used for stand-alone testing and development. Unless ECS
is running and HLT is steered by it, the ecs_gui can be used to simulate the 
ECS and its steering of the HLT. The ecs_gui therefore offers a graphical 
interface which lets the user initiate state transitions in the SMI server, 
thereby forcing the SMI server to send commands to the ecs_proxy.



INSTALLATION
------------

Please use make to build the tools, executables will are put in ./bin dir.
C++11 compiler support is needed as well as a Python interpreter (>= 2.7).  
The following OS packages are required:  

	boost
	boost-devel
	xmlrpc-c-devel
	xmlrpc-c-client++
	xmlrpc-c-c++
	tcl
	tk
	tk-devel
	python-twisted

SMI header files are needed, too. You can find them here:

        http://dim.web.cern.ch/dim/dim_v20r7.zip
        http://smi.web.cern.ch/smi/smixx_v46r3.zip

Unzip these files, modify the local export file (./setenv.sh)
accordingly and source it. Now you are safe to compile. 


HOW TO RUN
----------

For running fully stand-alone without ECS interaction it is required to 
run a DIM server and an SMI server. Please find them here:

	http://dim.web.cern.ch/dim/dim_v20r7.zip
	http://smi.web.cern.ch/smi/smixx_v46r3.zip

Unzip these files and modify the local export file (./setenv.sh). 
Then you can the DIM server with:

	source ./setenv.sh
	dns -d 

In stand-alone mode the SMI server needs the state machine encoding which
is stored in 
	
	./conf/hlt-states.smi

At compile time the encoding is translated to an sobj file, put in ./bin and
can be used by the SMI server:

	source ./setenv.sh
	smiSM -d 9  ALICE_HLT ./bin/hlt-states.sobj

Once SMI server and DIM server are started the tools can be used. The 
proposed starting order is as follows:

	# start the ecs_gui (tentative: in current implementation starting 
	# ecs_gui is path dependent, please change to ./bin dir) 
	cd bin
	HLTCON=ALICE_HLT::HLT_FWM HLTTOP=ALICE_HLT::HLT ./ecs_gui  

	# start the ecs_proxy
	./bin/ecs_proxy --domain ALICE_HLT
	
	# start the rm_proxy
	python ./bin/RMProxy.py

Both ECS and HLT hold stateful information about the assumed state the HLT is
in. Their representatives (ecs_proxy resp. rm_proxy) are decoupled and running
in different processes (and potentially on different nodes). There is no 
overlay making sure that both representatives are in sync at all times, i.e. a
crash or temporal unavailability of either representative will go unnoticed to
the communication partner unless one of the partners fails to contact the other
via the standard communication paths which are:

	ecs_proxy ----sends transition---> rm_proxy
	rm_proxy  ----sends new state----> ecs_proxy

If one such communication attempt fails the respective partner will go in ERROR 
state. For the starting order that means the following:
1) It is advised to start ecs_proxy before rm_proxy
2) rm_proxy can be restarted at runtime of ecs_proxy (ECS state!=OFF), however 
   both ecs_proxy and rm_proxy will then go into ERROR state. A RESET command
   by ECS will bring both to state INITIALIZED. 
3) If ecs_proxy is restarted at runtime of rm_proxy (HLT state != OFF) this will 
   not be noticed by rm_proxy immediately. As soon as the first ECS command 
   after restart of ecs_proxy is sent, both ecs_proxy and rm_proxy will go into 
   ERROR state. Again, a RESET command will bring both to state INITIALIZED. You 
   can avoid having to send one ECS command before both go to ERROR if you 
   always adhere to the starting order [ecs_proxy, rm_proxy].
   
	



