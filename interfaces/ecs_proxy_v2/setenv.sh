export HLTBASEDIR="/home/boettger"
export DIMVERSION="v20r7"
export SMIVERSION="v46r3"
export PROXYVERSION="v2.0.0"

export OS="Linux"
export DIMDIR="${HLTBASEDIR}/dim/dim_${DIMVERSION}"
export ODIR="linux"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${DIMDIR}/${ODIR}"
export PATH="${PATH}:${DIMDIR}/${ODIR}"
export SMIDIR="${HLTBASEDIR}/smi/smixx_${SMIVERSION}"
export SMIRTLDIR="${SMIDIR}"
export PATH="${PATH}:${SMIDIR}/${ODIR}"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${SMIDIR}/${ODIR}"

export ECS_PROXY_TOPDIR="${HLTBASEDIR}/alice-hlt-core/interfaces/ecs_proxy_v2"
export ECS_PROXY_DIR="$ECS_PROXY_TOPDIR"

export DIM_DNS_NODE=cn56.internal
export DIM_DNS_HOST=cn56.internal
export HLTECS_MODULE=HLT

