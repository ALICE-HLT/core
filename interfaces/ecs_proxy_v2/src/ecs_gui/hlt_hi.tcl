proc set_hltConnectivity {name index op} {

   global env hltConnectivity
   
   if { $hltConnectivity} {
      set cmd INCLUDE/LOCKER=SERVICE($env(HLTTOP)_HI[pid]&[pid]@[info hostname])
      C_SMI_send_command $env(HLTCON) $cmd
   } else {
      C_SMI_send_command $env(HLTCON) "EXCLUDE"
   }   
}

proc set_hltConnectivityUpdate {} {

   global hltConnectivityEnable hltConnectivity 
   
   if { $hltConnectivityEnable == -1 } {
       set hltConnectivity 0
   }
   if { $hltConnectivityEnable == 1 || $hltConnectivity } {
      .mbar.connectivity.menu entryconfigure "HLT Control" -state normal 
   } else {
      .mbar.connectivity.menu entryconfigure "HLT Control" -state disabled 
   }
   if { $hltConnectivity } {
#       Green lock   
      pack forget .systems.clg
      pack forget .systems.clr
      pack forget .systems.cu
      pack .systems.clg -side left -after .systems.hlt 
      set lastEntry [.systems.hlt.menu entrycget last -label] 
      if { $lastEntry == "" } {
         .systems.hlt configure -state normal -bg "#d9d9d9" -activebackground "#d9d9d9"
      } else {
         .systems.hlt configure -state normal -bg green -activebackground green
      }   
  } elseif { $hltConnectivityEnable == 1 } {
#       Unlock
      pack forget .systems.clg
      pack forget .systems.clr
      pack forget .systems.cu
      pack .systems.cu -side left -after .systems.hlt 
      .systems.hlt configure -state disabled -bg "#d9d9d9" -activebackground "#d9d9d9"
   } else {
#       Red lock   
      pack forget .systems.clg
      pack forget .systems.clr
      pack forget .systems.cu
      pack .systems.clr -side left -after .systems.hlt 
      .systems.hlt configure -state disabled -bg "#d9d9d9" -activebackground "#d9d9d9"
   }   
}

proc accept_hlt_command {command param} {

   global env
   
   set smi_command "$command"
   set tokens [split $param ,]
   foreach token $tokens {
      global t$token
      eval set smi_command $smi_command/$token=\$t$token
   }
   destroy .parameters  
   C_SMI_send_command $env(HLTTOP) $smi_command
      
}

proc cancel_hlt_command {} {

   destroy .parameters
   
}

proc fill_hlt_command {param} {

   set command [.systems.hlt.menu entrycget active -label]

   source $::env(ECS_PROXY_DIR)/bin/ini_reader.tcl
   if { [ catch { cfg::parse_file $::env(ECS_COMMAND_PARAMS) } fid ]} {puts "Could not find command parameters file or ECS_COMMAND_PARAMS not defined. Using default command parameters." }
   frame .parameters
   label .parameters.tit -text "Parameters for $command" -bg yellow -relief raised -font {-family courier -size 10 -weight bold}
   pack .parameters.tit -side top -expand true -fill x -pady 5
   set tokens [split $param ,]
   foreach token $tokens {
      global t$token
      if { [ catch { eval set t$token [cfg::getvar $token $command]} fid ]} { eval set t$token DEFAULT } 
      frame .parameters.f$token 
      label .parameters.f$token.lab -text $token -font {-family courier -size 10 -weight bold}
      entry .parameters.f$token.txt -textvariable t$token -background white
      pack .parameters.f$token.lab -side left 
      pack .parameters.f$token.txt -side right
      pack .parameters.f$token -side top -expand true -fill x 
   }
   frame .parameters.buttons
   button .parameters.buttons.ok -text OK -font {-family helvetica -size 10 -weight bold} -command "accept_hlt_command $command $param"
   button .parameters.buttons.cancel -text CANCEL -font {-family helvetica -size 10 -weight bold} -command cancel_hlt_command
   pack .parameters.buttons.ok .parameters.buttons.cancel -side left -padx 10
   pack .parameters.buttons -side bottom -fill x -pady 5   
   pack .parameters
}

proc send_hlt_command {} {
   global env
   set command [.systems.hlt.menu entrycget active -label] 
   C_SMI_send_command $env(HLTTOP) $command
}

proc SET_STATUS_HLT {status actions} {

   global STATUS_HLT hltConnectivity
   
   set STATUS_HLT $status
   set tokens [split $actions ^]
   .systems.hlt.menu delete 0 end
   destroy .parameters
   set n 0
   foreach token $tokens {
      set tokens2 [split $token :]
      set action [lindex $tokens2 0]
      set param [lindex $tokens2 1]
      if { $action != "NONE" } {
         incr n
         if { $param == "NONE" } {
	    .systems.hlt.menu add command -label $action -font {-family helvetica -size 10 -weight bold} -command "destroy .parameters;send_hlt_command"
         } else {
	    .systems.hlt.menu add command -label $action -font {-family helvetica -size 10 -weight bold} -command "destroy .parameters;fill_hlt_command $param"
         }   
      }   
   }
   if { $n != 0 && $hltConnectivity } {
      .systems.hlt configure -bg green -activebackground green 
   } else {
      .systems.hlt configure -bg "#d9d9d9" -activebackground "#d9d9d9"
   }   
}

proc update_smi_queue {} {
   global STATUS_HLT 
   C_exec_smi_command
   after 50 update_smi_queue
}

proc quit {} {

   global hltConnectivity env
   
   if { $hltConnectivity} {
      C_SMI_send_command $env(HLTCON) "EXCLUDE"
      C_sleep 1
   }   
   C_STOP_locking
   exit   
}

wm title . "HLT"
set geomMain +0+0
wm geometry . $geomMain
wm resizable . 0 0
wm protocol . WM_DELETE_WINDOW { quit }
image create photo lock -file  $::env(ECS_PROXY_DIR)/bin/locked.gif
image create photo unlock -file  $::env(ECS_PROXY_DIR)/bin/unlocked.gif

set STATUS_HLT UNKNOWN 

frame .mbar -relief raised -bd 2
pack .mbar -side top -fill x
menubutton .mbar.connectivity -text "Connectivity" -font {-family helvetica -size 10 -weight bold} -menu .mbar.connectivity.menu -underline 0
pack .mbar.connectivity -side left
menu .mbar.connectivity.menu -tearoff 0
global hltConnectivity
set hltConnectivity 0
.mbar.connectivity.menu add checkbutton -label "HLT Control" -font {-family helvetica -size 10 -weight bold} -variable hltConnectivity -selectcolor green
trace variable hltConnectivity w set_hltConnectivity

frame .status -relief groove -bd 2
label .status.hlt -textvariable STATUS_HLT  -relief sunken -bg white -font {-family courier -size 10 -weight bold} -width 15
pack .status.hlt -side left -padx 5 -expand true -fill x
pack .status  -side bottom  -pady 10 -expand true -fill x

frame .systems -relief flat -bd 2
menubutton .systems.hlt -text $env(HLTTOP) -font {-family helvetica -size 10 -weight bold} -menu .systems.hlt.menu -state normal -relief raised 
menu .systems.hlt.menu -tearoff 0
pack .systems.hlt -side left -padx 5 -expand true -fill x
pack .systems -side top -expand true -fill x
label .systems.cu -image unlock -background white -width 20 -height 20 
label .systems.clg -image lock -background green -width 20 -height 20 
label .systems.clr -image lock -background red -width 20 -height 20 

update_smi_queue
