#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>

# include <sys/types.h>
# include <sys/ipc.h>
# include <sys/sem.h>

#include <dis.h>

#include "tk.h"
#include <smiuirtl.h>

#define SMI_MAX_CHAR 1024*1024
#define SMI_MAX_COMMAND 50

static int nexec_smi_command=-1;
static char smi_command[SMI_MAX_COMMAND][SMI_MAX_CHAR];
static char obj[SMI_MAX_CHAR];
static char cmnd[SMI_MAX_CHAR];
unsigned int servicePid;
static char  CONTROLPID[64];
int semSetId;
int hltConnectivityEnable=0;

int C_exec_smi_command(ClientData clientData,Tcl_Interp *tclInterp,int argc,const char** argv);
int C_sleep(ClientData clientData,Tcl_Interp *tclInterp,int argc,const char** argv);
int C_SMI_send_command(ClientData clientData,Tcl_Interp *tclInterp,int argc,const char** argv);
int C_STOP_locking(ClientData clientData,Tcl_Interp *tclInterp,int argc,const char** argv);
void state_handler (int *ident, int *param);
void state_handle_con (int *ident, int *param);
int lock_queue ();
int unlock_queue ();

int C_exec_smi_command(ClientData clientData,Tcl_Interp *tclInterp,int argc,
	      const char** argv) {

   char command[SMI_MAX_CHAR];
   int i;

   if ( nexec_smi_command >= 0 ) {
      i=lock_queue();
      if ( i < 0 ) {
         printf("Cannot lock queue from C_exec_smi_command\n");
      }
      strncpy(command,smi_command[0],sizeof(command));
      for (i=1;i<=nexec_smi_command;i++) {
	strncpy(smi_command[i-1],smi_command[i],sizeof(smi_command[i-1]));
      }
      nexec_smi_command=nexec_smi_command-1;
      i=unlock_queue();
      if ( i < 0 ) {
         printf("Cannot unlock queue from C_exec_smi_command\n");
      }
      if ( Tcl_Eval(tclInterp,command) != TCL_OK) {
	printf("Tcl error when executing '%s': %s",command,Tcl_GetStringResult(tclInterp));
      } else {
	C_exec_smi_command(clientData,tclInterp,argc,argv);
      }
   }
   return TCL_OK;
}

int C_sleep(ClientData clientData,Tcl_Interp *tclInterp,int argc,const char** argv) {

   int ns;
   sscanf(argv[1],"%d",&ns);
   sleep(ns);
   return TCL_OK;
}

int C_SMI_send_command(ClientData clientData,Tcl_Interp *tclInterp,int argc,const char** argv) {

   if (argc <= 2) {
      Tcl_SetResult(tclInterp,"C_SMI_send_command requires 2 parameters: SMI object name and command",TCL_STATIC);
      return TCL_ERROR;
   }
   snprintf(obj,sizeof(obj),argv[1]);
   snprintf(cmnd,sizeof(cmnd),argv[2]);
   smiui_send_command(obj,cmnd);

   return TCL_OK;
}

int C_STOP_locking(ClientData clientData,Tcl_Interp *tclInterp,int argc,const char** argv) {

   int i;

   i=semctl(semSetId,0,IPC_RMID,1);
   if ( i == -1 ) {
      printf("Cannot remove semaphore set\n");
   }
   return TCL_OK;
}

void state_handler (int *ident, int *param) {

   char state[256], msg[256], actions[2048], action[64], paraname[64];
   int busy, naction, i, j, nparams, ptype, defvalsize, ifaction;

   smiui_get_state (*ident, &busy, state, &naction);

   if ( busy == 0 ) {
      strncpy(msg,"SET_STATUS_HLT ", sizeof(msg)-1);
      if ( strcmp(state,"") == 0 ) {
	strncpy (state,"UNKNOWN",sizeof(state));
      }
      strncat(msg,state,sizeof(msg)-strlen(msg)-1);
      strncat(msg," ",sizeof(msg)-strlen(msg)-1);
      ifaction=0;
      actions[0]='\0';
      if ( naction > 0 ) {
         smiui_get_first_action(*ident, action, &nparams);
         if (strncmp(action,"NV_",3) != 0 && action[0] != '&') {
            ifaction=1;
	    strncpy(actions,action,sizeof(actions)-1);
	    strncat(actions,":",sizeof(actions)-strlen(actions)-1);
            if ( nparams == 0 ) {
	       strncat(actions,"NONE",sizeof(actions)-strlen(actions)-1);
            } else {
               smiui_get_next_param(*ident, paraname, &ptype, &defvalsize);
               strncat(actions,paraname,sizeof(actions)-strlen(actions)-1);
               for (j=2;j<=nparams;j++) {
                  smiui_get_next_param(*ident, paraname, &ptype, &defvalsize);
		  strncat(actions,",",sizeof(actions)-strlen(actions)-1);
		  strncat(actions,paraname,sizeof(actions)-strlen(actions)-1);
               }
            }
         }
         for (i=2;i<=naction;i++) {
            smiui_get_next_action(*ident, action, &nparams);
            if (strncmp(action,"NV_",3) != 0 ) {
               if ( ifaction != 0 ) {
		 strncat(actions,"^",sizeof(actions)-strlen(actions)-1);
               }
               ifaction=1;
	       strncat(actions,action,sizeof(actions)-strlen(actions)-1);
	       strncat(actions,":",sizeof(actions)-strlen(actions)-1);
               if ( nparams == 0 ) {
 		  strncat(actions,"NONE",sizeof(actions)-strlen(actions)-1);
               } else {
                  smiui_get_next_param(*ident, paraname, &ptype, &defvalsize);
                  strcat(actions,paraname);
                  for (j=2;j<=nparams;j++) {
                     smiui_get_next_param(*ident, paraname, &ptype, &defvalsize);
		     strncat(actions,",",sizeof(actions)-strlen(actions)-1);
		     strncat(actions,paraname,sizeof(actions)-strlen(actions)-1);
                  }
               }
            }
         }
      }
      if ( strcmp (actions,"") == 0 ) {
	 strncpy(actions,"NONE:NONE",sizeof(actions)-1);
      }
      strncat(msg,actions,sizeof(msg)-strlen(msg)-1);
      i=lock_queue();
      if ( i < 0 ) {
         printf("Cannot lock queue from state_handler\n");
      }
      nexec_smi_command=nexec_smi_command+1;
      if ( nexec_smi_command >= SMI_MAX_COMMAND ) {
         fprintf(stderr,"SMI command buffer overflow - Panic\n");
         i=semctl(semSetId,0,IPC_RMID,1);
         if ( i == -1 ) {
            printf("Cannot remove semaphore set\n");
         }
         exit(0);
      }
      strncpy(smi_command[nexec_smi_command],msg,sizeof(smi_command[nexec_smi_command]));
      i=unlock_queue();
      if ( i < 0 ) {
         printf("Cannot unlock queue from state_handler\n");
      }
   }
}

void state_handler_con (int *ident, long *param) {

   char state[256];
   int busy, naction, i;

   smiui_get_state (*ident, &busy, state, &naction);

   if ( busy == 0 ) {
      if (strcmp(state,"EXCLUDED") == 0 ) {
         hltConnectivityEnable=1;
      } else if (strcmp(state,"DEAD") == 0 || strcmp(state,"") == 0) {
	 hltConnectivityEnable=-1;
      } else {
         hltConnectivityEnable=0;
      }
      i=lock_queue();
      if ( i < 0 ) {
         printf("Cannot lock queue from state_handler_con\n");
      }
      nexec_smi_command=nexec_smi_command+1;
      if ( nexec_smi_command >= SMI_MAX_COMMAND ) {
         fprintf(stderr,"SMI command buffer overflow - Panic\n");
         i=semctl(semSetId,0,IPC_RMID,1);
         if ( i == -1 ) {
            printf("Cannot remove semaphore set\n");
         }
         exit(0);
      }
      strncpy(smi_command[nexec_smi_command],"set_hltConnectivityUpdate",sizeof(smi_command[nexec_smi_command]));
      i=unlock_queue();
      if ( i < 0 ) {
         printf("Cannot unlock queue from state_handler_con\n");
      }
   }
}

int lock_queue ()
{
   int i;
   struct sembuf sops[1];

   sops[0].sem_num=0;
   sops[0].sem_op=-1;
   sops[0].sem_flg=SEM_UNDO;
   i=semop(semSetId,sops,1);
   if (i == -1) {
      return -3;
   }

   return 0;
}

int unlock_queue ()
{
   int i;
   struct sembuf sops[1];

   sops[0].sem_num=0;
   sops[0].sem_op=+1;
   sops[0].sem_flg=SEM_UNDO;
   i=semop(semSetId,sops,1);
   if (i == -1) {
      return -3;
   }
   return 0;
}

void UPPER (char *name) {

   char *p;

   p=name;
   while ( *p != '\0' ) {
      *p=toupper(*p);
      p++;
   }
}

// main: main program for the application.
// remark: Tk_Main never returns, so this procedure never returns either.
//
int main(int argc,char **argv) {

   char host[32];
   char *p;
   int i;

   setenv("HLTTOP","ALICE_HLT::HLT",0);
   setenv("HLTCON","ALICE_HLT::HLT_FWM",0);

   semSetId=semget(IPC_PRIVATE,1,6*64+6*8+6);
   if ( semSetId == -1 ) {
      printf("Cannot create semaphore set\n");
      return -1;
   }
   i=semctl(semSetId,0,SETVAL,1);
   if ( i == -1 ) {
      printf("Cannot initialize semaphore set\n");
      return -2;
   }

   strncpy(obj,getenv("HLTTOP"),sizeof(obj));
   smiui_book_statechange(obj,state_handler,0);

   smiui_book_statechange(getenv("HLTCON"),state_handler_con,0);

   snprintf(obj,sizeof(obj),"%s_HI%d",getenv("HLTTOP"),getpid());
   UPPER(obj);
   gethostname(host,sizeof(host));
   snprintf (CONTROLPID,sizeof(CONTROLPID),"%d@%s",getpid(),host);
   servicePid=dis_add_service (obj,0,CONTROLPID,sizeof(CONTROLPID),NULL,0);
   dis_start_serving(obj);

   argc=1;
   Tk_Main(argc, argv, Tcl_AppInit);
   return 0;	/* Needed only to prevent compiler warning. */
}

//
// Tcl_AppInit: performs application-specific initialization.
// returns a standard Tcl completion code, and leaves an error
//	   message in interp->result if an error occurs.
//
int Tcl_AppInit (Tcl_Interp *interp) {
  char buffer[1024];
  char* current=buffer;
  char* proxy_dir=getenv("ECS_PROXY_DIR"); 
  if(proxy_dir) {
    current+=snprintf(buffer, 1000, proxy_dir);
    current+=sprintf(current, "/bin/");
  }
  sprintf(current, "hlt_hi.tcl");

   if (Tcl_Init(interp) == TCL_ERROR) {
      return TCL_ERROR;
   }
   if (Tk_Init(interp) == TCL_ERROR) {
      return TCL_ERROR;
   }

   Tcl_CreateCommand(interp,
		     "C_exec_smi_command",
		     (Tcl_CmdProc*)C_exec_smi_command,
		     (ClientData) NULL,
		     (Tcl_CmdDeleteProc*)NULL);

   Tcl_CreateCommand(interp,
		     "C_sleep",
		     C_sleep,
		     (ClientData) NULL,
		     (Tcl_CmdDeleteProc*)NULL);

   Tcl_CreateCommand(interp,
		     "C_SMI_send_command",
		     C_SMI_send_command,
		     (ClientData) NULL,
		     (Tcl_CmdDeleteProc*)NULL);

   Tcl_CreateCommand(interp,
		     "C_STOP_locking",
		     C_STOP_locking,
		     (ClientData) NULL,
		     (Tcl_CmdDeleteProc*)NULL);

    Tcl_LinkVar(interp,"hltConnectivityEnable",(char*)&hltConnectivityEnable,TCL_LINK_INT);
    Tcl_StaticPackage(interp, "Tk", Tk_Init, (Tcl_PackageInitProc *) NULL);
    Tcl_SetVar(interp, "tcl_rcFileName", buffer , TCL_GLOBAL_ONLY);
    return TCL_OK;
}
