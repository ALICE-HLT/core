from fysom import Fysom, FysomError
import logging
import logimportant
import time
import threading
import Queue
from subprocess32 import Popen, PIPE
import os
import TMControlInterface
import exceptions
import libxml2
import signal
import tempfile
import zmq
import o2header

START_SLAVE_IN_CONFIGURE = 1 #Option to start the slave task managers during configure. Increases configure time but reduces engage time.
KEEP_SLAVE_IN_DISENGAGE = 1  #Do not kill and restart slaves during disengage (Only relevant when START_SLAVE_IN_CONFIGURE is set.) Reduces disengage time.

def signal_numtoname (num):
    name = []
    for key in signal.__dict__.keys():
        if key.startswith("SIG") and getattr(signal, key) == num:
            name.append (key)
    if len(name) == 1:
        return name[0]
    else:
        return str(num)

def getReturnCodeExplanation(num):
    explanation='None'
    if num != None:
        if num < 0:
            explanation='process received ' + signal_numtoname(-num) + '(' + str(-num) + ').'
        else:
            explanation='process returned ' + str(num) + '.'
    return explanation


ECS_DEFAULTS = {
    'DETECTOR_LIST'       : 'TPC',
    'BEAM_TYPE'           : 'pp',
    'DATA_FORMAT_VERSION' : '5',
    'HLT_TRIGGER_CODE'    : 'HLT_DEFAULT_TRIGGER',
    'HLT_IN_DDL_LIST'     : '0:0-0,1:0-1,2:0-2,3:0-3,4:0-4,5:0-5,6:0-6,8:0-8,10:0-10,11:0-11,12:0-12,13:0-13,14:0-14,15:0-15,16:0-16,18:0-18,256:1-0,257:1-1,258:1-2,259:1-3,260:1-4,261:1-5,262:1-6,263:1-7,264:1-8,265:1-9,266:1-10,267:1-11,268:1-12,269:1-13,270:1-14,271:1-15,272:1-16,273:1-17,274:1-18,275:1-19,276:1-20,277:1-21,278:1-22,279:1-23,512:2-0,513:2-1,514:2-2,515:2-3,516:2-4,517:2-5,518:2-6,519:2-7,520:2-8,521:2-9,522:2-10,523:2-11,524:2-12,525:2-13,526:2-14,527:2-15,768:3-0,769:3-1,770:3-2,771:3-3,772:3-4,773:3-5,774:3-6,775:3-7,776:3-8,777:3-9,778:3-10,779:3-11,780:3-12,781:3-13,782:3-14,783:3-15,784:3-16,785:3-17,786:3-18,787:3-19,788:3-20,789:3-21,790:3-22,791:3-23,792:3-24,793:3-25,794:3-26,795:3-27,796:3-28,797:3-29,798:3-30,799:3-31,800:4-0,801:4-1,802:4-2,803:4-3,804:4-4,805:4-5,806:4-6,807:4-7,808:4-8,809:4-9,810:4-10,811:4-11,812:4-12,813:4-13,814:4-14,815:4-15,816:4-16,817:4-17,818:4-18,819:4-19,820:4-20,821:4-21,822:4-22,823:4-23,824:4-24,825:4-25,826:4-26,827:4-27,828:4-28,829:4-29,830:4-30,831:4-31,832:5-0,833:5-1,834:5-2,835:5-3,836:5-4,837:5-5,838:5-6,839:5-7,840:5-8,841:5-9,842:5-10,843:5-11,844:5-12,845:5-13,846:5-14,847:5-15,848:5-16,849:5-17,850:5-18,851:5-19,852:5-20,853:5-21,854:5-22,855:5-23,856:5-24,857:5-25,858:5-26,859:5-27,860:5-28,861:5-29,862:5-30,863:5-31,864:6-0,865:6-1,866:6-2,867:6-3,868:6-4,869:6-5,870:6-6,871:6-7,872:6-8,873:6-9,874:6-10,875:6-11,876:6-12,877:6-13,878:6-14,879:6-15,880:6-16,881:6-17,882:6-18,883:6-19,884:6-20,885:6-21,886:6-22,887:6-23,888:6-24,889:6-25,890:6-26,891:6-27,892:6-28,893:6-29,894:6-30,895:6-31,896:7-0,897:7-1,898:7-2,899:7-3,900:7-4,901:7-5,902:7-6,903:7-7,904:7-8,905:7-9,906:7-10,907:7-11,908:7-12,909:7-13,910:7-14,911:7-15,912:7-16,913:7-17,914:7-18,915:7-19,916:7-20,917:7-21,918:7-22,919:7-23,920:7-24,921:7-25,922:7-26,923:7-27,924:7-28,925:7-29,926:7-30,927:7-31,928:8-0,929:8-1,930:8-2,931:8-3,932:8-4,933:8-5,934:8-6,935:8-7,936:8-8,937:8-9,938:8-10,939:8-11,940:8-12,941:8-13,942:8-14,943:8-15,944:8-16,945:8-17,946:8-18,947:8-19,948:8-20,949:8-21,950:8-22,951:8-23,952:8-24,953:8-25,954:8-26,955:8-27,956:8-28,957:8-29,958:8-30,959:8-31,960:9-0,961:9-1,962:9-2,963:9-3,964:9-4,965:9-5,966:9-6,967:9-7,968:9-8,969:9-9,970:9-10,971:9-11,972:9-12,973:9-13,974:9-14,975:9-15,976:9-16,977:9-17,978:9-18,979:9-19,980:9-20,981:9-21,982:9-22,983:9-23',
    'HLT_OUT_DDL_LIST'    : '7680:31-00,7681:31-01,7682:31-02,7683:31-03,7684:31-04,7685:31-05,7686:31-06,7687:31-07,7688:31-08,7689:31-09,7690:31-10,7691:31-11,7692:31-12,7693:31-13,7694:31-14,7695:31-15,7696:31-16,7697:31-17,7698:31-18,7699:31-19,7700:31-20,7701:31-21,7702:31-22,7703:31-23,7704:31-24,7705:31-25,7706:31-26,7707:31-27',
    'RUN_TYPE'            : 'PHYSICS',
    'HLT_MODE'            : 'E',
    'RUN_NUMBER'          : '300000',
    'CTP_TRIGGER_CLASS'   : '00:CLASS00:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,01:CLASS01:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,02:CLASS02:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,03:CLASS03:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,04:CLASS04:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,05:CLASS05:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,06:CLASS06:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,07:CLASS07:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,08:CLASS08:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,09:CLASS09:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,10:CLASS10:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,11:CLASS11:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,12:CLASS12:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,13:CLASS13:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,14:CLASS14:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,15:CLASS15:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,16:CLASS16:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,17:CLASS17:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,18:CLASS18:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,19:CLASS19:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,20:CLASS20:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,21:CLASS21:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,22:CLASS22:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,23:CLASS23:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,24:CLASS24:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,25:CLASS25:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,26:CLASS26:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,27:CLASS27:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,28:CLASS28:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,29:CLASS29:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,30:CLASS30:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,31:CLASS31:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,32:CLASS32:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,33:CLASS33:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,34:CLASS34:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,35:CLASS35:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,36:CLASS36:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,37:CLASS37:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,38:CLASS38:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,39:CLASS39:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,40:CLASS40:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,41:CLASS41:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,42:CLASS42:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,43:CLASS43:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,44:CLASS44:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,45:CLASS45:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,46:CLASS46:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,47:CLASS47:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,48:CLASS48:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,49:CLASS49:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,50:CLASS50:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,51:CLASS51:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,52:CLASS52:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,53:CLASS53:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,54:CLASS54:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,55:CLASS55:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,56:CLASS56:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,57:CLASS57:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18,58:CLASS58:00-01-02-03-04-05-06-07-08-09-10-11-12-13-14-15-16-17-18',
    'EOR_MODE'           : 'FULL',
}

ZMQ_SEND_RESET_MESSAGE = 0
os_username = os.getenv('USERNAME')
if os_username == None:
    os_username = os.getenv('USER')
if os_username == None:
    print "Cannot determine username"
    sys.exit(1)

if os_username == "hlt-operator":
    ZMQ_SEND_RESET_MESSAGE = 1

if ZMQ_SEND_RESET_MESSAGE:
    context = zmq.Context()
    socket = context.socket(zmq.PUB)
    socket.bind("tcp://*:60221")
    resetMessage = [ o2header.make("INFO","HLT",5),"run=0" ]

class RMStateMachine:

    initialState = 'OFF'
    allStates = [
        'OFF',
        'INITIALIZING',
        'INITIALIZED',
        'CONFIGURING',
        'CONFIGURED',
        'ENGAGING',
        'READY',
        'STARTING',
        'RUNNING',
        'PRE_PARING',
        'POST_PARING',
        'COMPLETING',
        'DISENGAGING',
        'DECONFIGURING',
        'DEINITIALIZING',
        'ERROR',
    ]
    events = [
        {'name': 'INITIALIZE', 'src': 'OFF',            'dst': 'INITIALIZING'},
        {'name': 'SUCCESS',    'src': 'INITIALIZING',   'dst': 'INITIALIZED'},
        {'name': 'CONFIGURE',  'src': 'INITIALIZED',    'dst': 'CONFIGURING'},
        {'name': 'SUCCESS',    'src': 'CONFIGURING',    'dst': 'CONFIGURED'},
        {'name': 'ENGAGE',     'src': 'CONFIGURED',     'dst': 'ENGAGING'},
        {'name': 'SUCCESS',    'src': 'ENGAGING',       'dst': 'READY'},
        {'name': 'START',      'src': 'READY',          'dst': 'STARTING'},
        {'name': 'SUCCESS',    'src': 'STARTING',       'dst': 'RUNNING'},
        {'name': 'PRE_PAR',    'src': 'RUNNING',        'dst': 'PRE_PARING'},
        {'name': 'SUCCESS',    'src': 'PRE_PARING',     'dst': 'RUNNING'},
        {'name': 'POST_PAR',   'src': 'RUNNING',        'dst': 'POST_PARING'},
        {'name': 'SUCCESS',    'src': 'POST_PARING',    'dst': 'RUNNING'},
        {'name': 'STOP',       'src': 'RUNNING',        'dst': 'COMPLETING'},
        {'name': 'SUCCESS',    'src': 'COMPLETING',     'dst': 'READY'},
        {'name': 'DISENGAGE',  'src': 'READY',          'dst': 'DISENGAGING'},
        {'name': 'SUCCESS',    'src': 'DISENGAGING',    'dst': 'CONFIGURED'},
        {'name': 'RESET',      'src': 'CONFIGURED',     'dst': 'DECONFIGURING'},
        {'name': 'SUCCESS',    'src': 'DECONFIGURING',  'dst': 'INITIALIZED'},
        {'name': 'SHUTDOWN',   'src': 'INITIALIZED',    'dst': 'DEINITIALIZING'},
        {'name': 'SUCCESS',    'src': 'DEINITIALIZING', 'dst': 'OFF'},
        {'name': 'ERROR',      'src': '*',              'dst': 'ERROR'},
        {'name': 'SUCCESS',    'src': 'ERROR',          'dst': 'ERROR'},
        {'name': 'RESET',      'src': 'ERROR',          'dst': 'INITIALIZING'},
        {'name': 'SHUTDOWN',   'src': 'ERROR',          'dst': 'DEINITIALIZING'},
    ]

    def __init__(self, myRMProxy):
        self.RMProxy = myRMProxy
	self.logger = logging.getLogger('RunManager')
        self.logger.important("Starting RunManager.")
        self.commandParams={}
        self.commandQueue=Queue.Queue(1)
        self.commandThread=threading.Thread(target=self.execCommands)
        self.commandThread.daemon=True
        self.commandThread.start()
        self.controlThread=TMControlThread(self.logger, self.triggerError)
        self.fsmDef = {
            'initial' : RMStateMachine.initialState,
            'events'  : RMStateMachine.events,
            'callbacks' : {
                'onchangestate' : self.onchangestate,
                'onenterERROR'       : self.ERROR,
            }
        }
        # add reflexive transition events SET_[ACTIVE,PASSIVE]
        # and associated reenter callbacks
        for state in RMStateMachine.allStates:
            self.fsmDef['events'].append({'name': 'SET_ACTIVE' , 'src': state, 'dst': state})
            self.fsmDef['events'].append({'name': 'SET_PASSIVE', 'src': state, 'dst': state})
            if state != 'ERROR':
                self.fsmDef['callbacks']['onreenter'+state]=self.onreenterstate
        self.fsm = Fysom(self.fsmDef)
        self.compMonitorProc = None
        self.inError = False

    def cleanup(self):
        self.commandQueue.put('EXIT')
        self.commandThread.join()
        self.controlThread.cleanup()

    def execCommands(self):
        while True:
            command = self.commandQueue.get()
            if command == 'EXIT':
                self.commandQueue.task_done()
                break
            params = self.commandParams[command]
            paramStr='".'
            #maybe too long for infologger, might lead to crash
            #if params != None and len(params) > 0:
            #    paramStr='" with parameters: ' + str(params)
            self.logger.important('Executing command "' + command + paramStr)
            sTime=time.time()
            try:
                self.inError = False
                getattr(self, command)()
                if self.inError:
                    self.logger.error('Command "' + command + '" failed.')
                    self.triggerError()
                else:
                    eTime=time.time()
                    self.logger.important('Command "' + command + '" took ' + "{0:.2f}".format(eTime-sTime) + 's.')
                    self.triggerSuccess()
                self.commandQueue.task_done()
            except Exception as exc:
                self.logger.error('Execution of RunManager command '+ command +' failed (' + str(type(exc)) + '): \"' + str(exc) + '\". Setting HLT to ERROR state.')
                self.commandQueue.task_done()
                self.triggerError()

    def onchangestate(self, e):
        if e.event == 'ERROR':
            return
        if e.event != 'SUCCESS' and e.src != 'none' :
            self.commandQueue.put(e.event)
        self.RMProxy.sendNewStateToECS(e.dst)
        self.logger.info('New state is ' + e.dst)

    def onreenterstate(self, e):
        self.RMProxy.sendNewStateToECS(e.dst)
        if hasattr(self, e.event):
            self.logger.important('Executing command "' + e.event + '".')
            try:
                getattr(self, e.event)()
            except:
                self.triggerError()

    def replaceDefaults(self, params):
        ret=list(params)
        for i in range(0, len(ret)):
            if ret[i][1] == 'DEFAULT' and ret[i][0] in ECS_DEFAULTS:
                ret[i][1]=ECS_DEFAULTS[ret[i][0]]
                self.logger.important('Replacing default ECS parameter "' + ret[i][0] + '" with value "' + ret[i][1] + '".')
        return ret

    def executeECSCommand(self, command, params):
        try:
            if self.fsm.cannot(command):
                raise Exception('ECS Command '+command+' cannot be executed while RunManager is in state '+self.fsm.current)
            self.commandParams[command]=self.replaceDefaults(params)
            self.fsm.trigger(command)
        except Exception as e:
            self.logger.error('Exception: ' + str(e))
            raise

    def triggerError(self):
        self.fsm.trigger('ERROR')

    def triggerSuccess(self):
        self.fsm.trigger('SUCCESS')


    def getParam(self, command, param):
        return [x[1] for x in self.commandParams[command] if x[0] == param][0]

    def ERROR(self, e):
        if ZMQ_SEND_RESET_MESSAGE:
          self.logger.info('Resetting ZMQ mergers.')
          socket.send_multipart(resetMessage)
        self.logger.error('ERROR command received, going into state ERROR.')
        self.RMProxy.sendNewStateToECS('ERROR')
        self.controlThread.run = False
        self.inError = True
        self.commandQueue.join()

    def SET_ACTIVE(self):
        pass

    def SET_PASSIVE(self):
        pass

    def INITIALIZE(self):
        self.RESET()

    def CONFIGURE(self):
        workdir     = os.environ['HLT_RUN_CONFIG_DIR']
        config_src  = os.environ['HLT_CONFIG_SRCDIR']
        run_dir     = os.environ['HLT_RUN_LOGDIR']
        base_socket = os.environ['HLT_RUN_BASEPORT']
        info_lib    = os.environ['INFOLOGGERLIB_DIR']
        if not os.access(workdir, os.X_OK):
            os.makedirs(workdir)
        in_ddl_list  = self.getParam('CONFIGURE', 'HLT_IN_DDL_LIST')
        out_ddl_list = self.getParam('CONFIGURE', 'HLT_OUT_DDL_LIST')
        format_version = self.getParam('CONFIGURE', 'DATA_FORMAT_VERSION')
        prog=[ 'MakeTaskManagerConfig2.py',
               '-masternode'        , 'master',
               '-siteconfig'        , config_src + '/siteconfig.xml',
               '-nodelist'          , config_src + '/nodelist.xml',
               '-ddllist'           , config_src + '/ddllist.xml',
               '-rundir'            , run_dir,
               '-config'            , config_src + '/input.xml',
               '-config'            , config_src + '/its.xml',
               '-config'            , config_src + '/tpc.xml',
               '-config'            , config_src + '/trd.xml',
               '-config'            , config_src + '/tof.xml',
               '-config'            , config_src + '/muon.xml',
               '-config'            , config_src + '/hmpid.xml',
               '-config'            , config_src + '/phos.xml',
               '-config'            , config_src + '/vzero.xml',
               '-config'            , config_src + '/emcal.xml',
               '-config'            , config_src + '/calib.xml',
               '-config'            , config_src + '/main.xml',
               '-config'            , config_src + '/output.xml',
               '-config'            , config_src + '/fxs.xml',
               '-basesocket'        , base_socket,
               '-infologger'        , info_lib + '/lib/libInfoLoggerLogServer.so',
               '-ecsinputlist'      , in_ddl_list,
               '-readoutlistversion', format_version,
               '-output'            , 'TM',
           ]
        if out_ddl_list != None and len(out_ddl_list)>0:
            prog+=['-ecsoutputlist' , out_ddl_list]
        configureProc = ProgRunner(prog, self.logger, workdir=workdir)
        self.runProg(['sync_HLT_install.sh'])
        while configureProc.poll() == None and self.fsm.current != 'ERROR':
            time.sleep(.001)
        ret = configureProc.stopProcess()
        if ret != 0:
            raise Exception('Command "' + prog[0] + '" unsuccessful: ' + getReturnCodeExplanation(ret))
        if START_SLAVE_IN_CONFIGURE:
            self.controlThread.configSynched = 1
            configSyncProc = ProgRunner(['sync_HLT_config.sh'], self.logger, workdir=workdir)
            self.controlThread.start()
            self.controlThread.setTargetStateAsync(TMControlThread.UP, 'processes_dead',
                                              self.commandParams['CONFIGURE']) #ENGAGE parameters are not needed until start of processes
            while configSyncProc.poll() == None and self.fsm.current != 'ERROR':
                time.sleep(.001)
            ret = configSyncProc.stopProcess()
            if ret != 0:
                raise Exception('Command "sync_HLT_config.sh" unsuccessful: ' + getReturnCodeExplanation(ret))
            self.controlThread.configSynched = 1
        else:
            self.runProg(['sync_HLT_config.sh'])
        self.runProg(['HLT_version_dump.sh'])
        if START_SLAVE_IN_CONFIGURE:
            self.controlThread.setTargetStateJoin()

    def ENGAGE(self):
        self.controlThread.grpAvailable = 0
        self.controlThread.configSynched = 1
        if not START_SLAVE_IN_CONFIGURE:
            self.controlThread.start()
        self.controlThread.setTargetStateAsync(TMControlThread.UP, 'ready',
                                          self.commandParams['CONFIGURE'] +
                                          self.commandParams['ENGAGE'])
        ret = self.runProg(['HLT_create_GRP.sh', self.getParam('ENGAGE', 'HLT_MODE'), 
                                           self.getParam('ENGAGE', 'RUN_NUMBER'),
                                           self.getParam('CONFIGURE', 'RUN_TYPE'),
                                           self.getParam('CONFIGURE','BEAM_TYPE'),
                                           self.getParam('CONFIGURE','DETECTOR_LIST') ], None, 1 )
        if ret != 0:
            self.controlThread.grpAvailable = 2 #Mark an error during GRP creation!
            self.controlThread.setTargetStateJoin()
            raise Exception('Command "HLT_create_GRP.sh" unsuccessful')
        self.controlThread.grpAvailable = 1
        self.controlThread.setTargetStateJoin()
        self.compMonitorProc = ProgRunner(['HLT_rrd_logger.sh'], self.logger)

    def START(self):
        self.controlThread.setTargetState(TMControlThread.UP, 'running', [])

    def PRE_PAR(self):
        time.sleep(0.1)

    def POST_PAR(self):
        self.runProg(['HLT_PARwait.sh'])

    def STOP(self):
        self.controlThread.setTargetState(TMControlThread.DOWN, 'ready', [])

    def DISENGAGE(self):
        if ZMQ_SEND_RESET_MESSAGE:
          self.logger.info('Resetting ZMQ mergers.')
          socket.send_multipart(resetMessage)
        if self.compMonitorProc:
          self.compMonitorProc.stopProcess()
        if START_SLAVE_IN_CONFIGURE:
          self.controlThread.setTargetState(TMControlThread.DOWN, 'processes_dead', self.commandParams['CONFIGURE'])
        else:
          self.controlThread.setTargetState(TMControlThread.DOWN, 'slaves_dead', [])
        if KEEP_SLAVE_IN_DISENGAGE and START_SLAVE_IN_CONFIGURE:
            self.runProg(['HLT_fastKillProcesses.sh', 'worker_cleanup'])
            self.runProg(['HLT_archive.sh'])
        else:
            #Alternative where slaves are killed and restarted. Would be safer perhaps but is not necessary
            self.controlThread.stop()
            self.runProg(['HLT_fastKillProcesses.sh'])
            self.runProg(['HLT_archive.sh'])
            if START_SLAVE_IN_CONFIGURE:
                self.controlThread.start()  #We stop and restart the slaves, as this seems to cause problems otherwise
                self.controlThread.setTargetState(TMControlThread.UP, 'processes_dead', self.commandParams['CONFIGURE'])

    def RESET(self):
        if ZMQ_SEND_RESET_MESSAGE:
          self.logger.info('Resetting ZMQ mergers.')
          socket.send_multipart(resetMessage)
        if self.compMonitorProc:
          self.compMonitorProc.stopProcess()
        self.controlThread.stop()
        self.runProg(['HLT_fastKillProcesses.sh'])
        self.runProg(['HLT_archive.sh','--clean'])
        self.runProg(['HLT_kill.sh'])

    def SHUTDOWN(self):
        self.RESET()

    def runProg(self, prog, wd=None, suppressError=0):
        p = ProgRunner(command=prog, logger=self.logger, workdir=wd, outputfilter=True)
        while p.poll() == None and self.fsm.current != 'ERROR':
            time.sleep(.2)
        ret = p.stopProcess()
        if ret != 0 and suppressError == 0:
            raise Exception('Command "' + prog[0] + '" unsuccessful: ' + getReturnCodeExplanation(ret))
        return ret


class ProgRunner:
  def __init__(self, command, logger, workdir=None, outputfilter=False):
    self.command = command
    self.cwd = workdir
    self.logger = logger
    self.outputfilter = outputfilter
    self.maxFilterBufferLines = 1000
    self.startTime = time.time()

    self.stdoutfile = tempfile.TemporaryFile()
    self.stderrfile = tempfile.TemporaryFile()
    self.logger.important('Starting process "' + command[0] + '"')
    self.proc = Popen(self.command, stdout=self.stdoutfile, stderr=self.stderrfile, cwd=self.cwd)

  def poll(self):
    return self.proc.poll()

  def stopProcess(self):
    if not self.proc:
      if not self.stdoutfile.closed:
        self.stdoutfile.close()
      if not self.stderrfile.closed:
        self.stderrfile.close()
      return 0
    i = 0
    while self.proc.poll() == None:
      if i==0:
        self.logger.important('Terminating process ' + self.command[0] + ' ...')
        self.proc.terminate()
      elif i==5:
        self.logger.warning('Process ' + self.command[0]+ ' still running, trying to kill...')
        self.proc.kill()
      elif i==10:
        raise Exception('Process ' + self.command[0] + \
            ' still running even after killing, nothing else to do..')
      i += 1
      time.sleep(1.)
    endTime = time.time()
    self.logger.important('Command "' + self.command[0] + '" execution time: ' + \
        "{0:.2f}".format(endTime-self.startTime) + 's.')
    ret = self.proc.poll()
    self.dumpLogs(self.stdoutfile, self.logger.info, outputfilter=self.outputfilter)
    self.stdoutfile.close()
    self.dumpLogs(self.stderrfile, self.logger.warning, outputfilter=self.outputfilter)
    self.stderrfile.close()
    return ret

  def dumpLogs(self, logfile, dumpfct, outputfilter=False):
    if not logfile.closed:
      logfile.seek(0)
      filterbuf = []
      for line in logfile.readlines():
        line = line[:-1] # strip newline
        if not line in filterbuf:
          dumpfct('[' + self.command[0] + ']:' + line)
          filterbuf.append(line)
          if len(filterbuf) > self.maxFilterBufferLines:
            filterbuf = filterbuf[-self.maxFilterBufferLines:]

class TMControlThread:

    UP   = 0
    DOWN = 1

    TMStates={
        'slaves_dead'    : ['start_slaves', 'quit'       ],
        'processes_dead' : ['start'       , 'kill_slaves'],
        'ready_local'    : ['connect'     , 'stop'       ],
        'ready'          : ['start_run'   , 'disconnect' ],
        'running'        : [None          , 'stop_run'   ],
        'busy'           : [None          , 'stop_run'   ],
    }


    def __init__(self, logger, errorCallback):
        self.logger=logger
        self.localaddress='tcpmsg://:' + os.environ['HLT_RUN_CONTROL_PORT']
        self.remoteport=os.environ['HLT_RUN_BASEPORT']
        self.remoteaddress=''
        self.nodeList=os.environ['HLT_NODELIST_XML']
        self.TMcommand='TaskManager'
        self.configDir=os.environ['HLT_RUN_CONFIG_DIR']
        self.runDir=os.environ['HLT_RUN_LOGDIR']
        self.TMverbosity='0x78'
        self.prog=None
        self.pollIntervall=.1
        self.controlThread=None
        self.errorCallback=errorCallback
        self.targetQueue=Queue.Queue()
        self.run=False
        self.currentState='UNKNOWN'
        self.lastTime = None
        self.grpAvailable = 0
        self.configSynched = 0
        TMControlInterface.Init( self.localaddress )

    def cleanup(self):
        self.stop()
        TMControlInterface.Terminate()

    def start(self):
        self.stop()
        self.run=True
        self.master=self.getMasterNode()
        self.remoteaddress='tcpmsg://' + self.master + ':' + self.remoteport
        self.masterXML=self.configDir + '/ALICE-master-' + self.master + '-Master.xml'
        prog=[self.TMcommand,
              self.masterXML,
              '-V', self.TMverbosity,
              '-nostdout' ]
        self.logger.important('Starting Master Task Manager:' + str(prog))
        self.prog=Popen(prog, stdout=PIPE, stderr=PIPE, cwd=self.runDir)
        self.controlThread=threading.Thread(target=self.control)
        self.controlThread.daemon=True
        self.controlThread.start()

    def stop(self):
        self.run=False
        if self.controlThread != None:
            self.controlThread.join()
            self.controlThread = None
        if self.prog == None:
            return
        self.logger.important('Stopping Master Task Manager [PID ' + str(self.prog.pid) + ']...')
        i=0
        while self.prog.poll() == None:
            if i==0:
                self.prog.terminate()
            elif i==10:
                self.logger.warning('Master Task Manager still running, trying to kill ...')
                self.prog.kill()
            elif i==20:
                raise Exception('Master Task Manager [PID ' + str(self.prog.pid) + '] still running even after killing, nothing else to do ...')
            i+=1
            time.sleep(.5)
        self.prog=None

    def setTargetState(self, up_down, state, params):
        self.targetQueue.put((up_down, state, params))
        self.targetQueue.join()

    def setTargetStateAsync(self, up_down, state, params):
        self.targetQueue.put((up_down, state, params))

    def setTargetStateJoin(self):
        self.targetQueue.join()

    def getFullCommand(self, command, params):
        ret=command
        if command != 'start':
            return ret
        for item in params:
            if item[1] != None and len(item[1])>0:
                ret += ';' + item[0] + '=' + item[1]
        return ret

    def control(self):
        self.logger.info("TMcontrol: Starting MasterTM control thread.")
        time.sleep(1.)
        maxErrorCount=3
        errorCount=0
        targetState=None
        prev_cmd=None
        cmd=None
        up_down=None
        params=None
        while self.run:
            try:
                ret=self.prog.poll()
                if ret != None:
                    raise Exception('TMcontrol: MasterTM no longer running: ' + getReturnCodeExplanation(ret))
                tmpState=TMControlInterface.QueryState( self.remoteaddress )
                if tmpState=='busy':
                    tmpState='running'
                self.currentState=tmpState
                if self.currentState=='error':
                    raise Exception('TMcontrol: MasterTM in ERROR state, exiting TMControl thread.')
                if targetState == self.currentState:
                    if self.lastTime != None:
                        self.logger.important('TMcontrol: Command ' + prev_cmd + ' execution time: ' + str(time.time() - self.lastTime))
                        self.lastTime = None
                    self.logger.info('TMcontrol: MasterTM reached target state "' + targetState + '".')
                    self.targetQueue.task_done()
                    targetState = None
                if targetState == None:
                    try:
                        (up_down, targetState, params)=self.targetQueue.get(block=False)
                    except Queue.Empty:
                        pass
                elif self.currentState in TMControlThread.TMStates.keys():
                    cmd=TMControlThread.TMStates[self.currentState][up_down]
                    if prev_cmd != cmd:
                        if self.lastTime != None and prev_cmd != None:
                            self.logger.important('TMcontrol: Command ' + prev_cmd + ' execution time: ' + str(time.time() - self.lastTime))
                        prev_cmd=cmd
                        if cmd == "start":
                            while self.grpAvailable == 0:
                                time.sleep(0.001)
                            if self.grpAvailable == 2:
                                self.logger.important('TMcontrol: Terminating due to GRP error.')
                                break
                        if cmd == "start_slaves":
                            while self.configSynched == 0:
                                time.sleep(0.001)
                        self.logger.important('TMcontrol: Sending command "' + cmd + '" to MasterTM.')
                        self.lastTime = time.time()
                        #self.logger.info('TMcontrol: Full command with params: "' + self.getFullCommand(cmd, params) + '".')
                        TMControlInterface.SendCmd(self.remoteaddress, self.getFullCommand(cmd, params))
                errorCount=0
            except SystemError as e:
                self.logger.warning('TMcontrol: Exception caught (' + str(type(e)) + '): ' + str(e))
                errorCount+=1
                if errorCount >= maxErrorCount:
                    self.logger.fatal('TMcontrol: Error threshold reached reached, exiting TMControl thread.')
                    break
            except Exception as e:
                self.logger.fatal('TMcontrol: Exception caught (' + str(type(e)) + '): ' + str(e))
                break
            time.sleep(self.pollIntervall)
        # draining targetQueue to unblock any waiting threads
        try:
            while True:
                self.targetQueue.get(block=False)
        except Queue.Empty:
            pass
        try:
            while True:
                self.targetQueue.task_done()
        except:
            pass
        # we're here although we should still be running
        # an error must have occurred
        if self.run:
            self.errorCallback()
        self.logger.info("TMcontrol: Stopping MasterTM control thread.")

    def getState(self):
        return self.currentState

    def getMasterNode(self):
        xmlFile = libxml2.parseFile(self.nodeList)
        xPathContext = xmlFile.xpathNewContext()
        if len(xPathContext.xpathEval("/SimpleChainConfig2NodeList"))!=1:
            raise Exception(self.nodeList + ": Wrong type of file, no XML nodelist...")
        nodes = xPathContext.xpathEval("/SimpleChainConfig2NodeList/Node")
        for node in nodes:
            master=node.xpathEval("Master")
            if master and master[0].content=="1":
                return node.xpathEval("Hostname")[0].content
        raise Exception("No master node found in " + self.nodeList)
