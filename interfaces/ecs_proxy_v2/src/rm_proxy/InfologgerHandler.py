#!/usr/bin/python

import infoLogger
import logging
import logimportant

class InfologgerHandler(logging.Handler):

    severity={
        logging.DEBUG     : 'D',
        logging.INFO      : 'I',
        logging.WARN      : 'W',
        logging.ERROR     : 'E',
        logging.FATAL     : 'F',
        logging.IMPORTANT : 'M',
    }

    def __init(self):
        logging.Handler.__init__(self)
        infoLogger.infoOpen()

    def __del__(self):
        infoLogger.infoClose()

    def emit(self, record):
        try:
            infoLogger.infoLog(record.name, InfologgerHandler.severity[record.levelno], record.getMessage())
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)



if __name__ == "__main__":
    logger = logging.getLogger('InfologgerTest')
    logger.setLevel(logging.DEBUG)
    handler = InfologgerHandler()
    logger.addHandler(handler)
    
    for level in sorted(InfologgerHandler.severity.keys()):
        logger.log(level, 'This is a ' + logging.getLevelName(level) + ' log message.')

