#!/usr/bin/python

from twisted.web import xmlrpc, server
from twisted.internet import reactor
import xmlrpclib
from threading import Thread
import sys 
import os
import Queue
import time
import logging
import logging.config
import logimportant
import ConfigParser
import signal
from RunManager import RMStateMachine
import errno
from socket import error as socket_error

# Override for ConfigParser.SafeConfigParser to check for an environment
# variable of the requested name first. If variable exists, return variable,
# else use value from ini file.
class EnvConfigParser(ConfigParser.SafeConfigParser):

  def get(self, value1, value2):
    envVarName="%s_%s" % (value1.upper(), value2.upper())
    if envVarName in os.environ.keys():
      return os.environ[envVarName]
    else:
      return ConfigParser.SafeConfigParser.get(self, value1, value2)


class RMComm:

    def __init__(self, myRMProxy):
        self.RMProxy = myRMProxy
	commServerThread = Thread(target = self._runServerThread, args = ())
    	commServerThread.daemon = True
	commServerThread.start()
	
    def _runServerThread(self):
	try:
	    myServer = RMCommServer()
	    myServer.handOver(self.RMProxy)
	    reactor.listenTCP(int(RM_PROXY_LISTENER_PORT), server.Site(myServer))
            logger.important('Listener for communication with ECS proxy established on port '+str(RM_PROXY_LISTENER_PORT))
	    reactor.run(installSignalHandlers=0)
	except:
	    logger.error('Listener for communication with ECS proxy failed.')
	    raise 
	
    def triggerStateChange(self, newState):
	logger.info('HLT is now in state: '+newState+'. Propagating to ECS proxy.')

        
	s = xmlrpclib.Server('http://'+str(ECS_PROXY_LISTENER_HOST)+':'+str(ECS_PROXY_LISTENER_PORT)+'/RPC2')
        if s.signalStateChange(newState) != 'OK':
	    raise Exception('ECS Proxy failed to process new HLT state '+newState+'. Incorrect Message format.')

class RMCommServer(xmlrpc.XMLRPC):

    def xmlrpc_forwardECSCommand(self, command, params):
	try:
	    logger.info('RMProxy received new command '+command+' from ECS Proxy. Relaying to RunManager.')
            self.RMProxy.sendNewCommandToHLT(command, params)
	except Exception as e:
	    logger.error('Failed to relay ECS command '+command+' to RunManager: '+str(e))
	    return 'NOK'
	return 'OK'

    def handOver(self, myRMProxy):
	self.RMProxy = myRMProxy

class RMProxy:
    
    def __init__(self):
	self.commServer = RMComm(self)
	self.toHLTQueue =  Queue.Queue()
	self.fromHLTQueue =  Queue.Queue()
	self.RMStateMachine = RMStateMachine(self)
	logger.info('RunManager successfully started.')
	self.receiveCmdThread = Thread(target = self._receiveCmdThread, args = ())
        self.receiveCmdThread.daemon = True
        self.receiveCmdThread.start()
	self.sendStateThread = Thread(target = self._sendStateThread, args = ())
        self.sendStateThread.daemon = True
        self.sendStateThread.start()
	signal.signal(signal.SIGINT, self.signalHandler)
	signal.signal(signal.SIGTERM, self.signalHandler)
	logger.info('RunManager proxy successfully started.')

    def cleanup(self):
        self.toHLTQueue.put('EXIT')
        self.fromHLTQueue.put('EXIT')

        self.toHLTQueue.join()
        self.receiveCmdThread.join()
        self.fromHLTQueue.join()
        self.sendStateThread.join()

        self.RMStateMachine.cleanup()

    def go(self):
	try: 
	    while True:
		time.sleep(1)
	except Exception as e:
   	    logger.important(str(e)+'Will shutdown after propagating ERROR state to ECS.')
	    try:
	      	self.commServer.triggerStateChange('ERROR')
	    except:
                logger.warning('ECS Proxy not reachable. Continuing with shutdown.')
        # do cleanup
        self.cleanup()
        logger.important('Shutdown of RMProxy complete.')
        #sys.exit(0)

    @staticmethod
    def signalHandler(signal, frame):
	raise Exception('SIGNAL caught. ')

    def _sendStateThread(self):	
	while True:
	    newState = self.fromHLTQueue.get()
            if newState == 'EXIT':
                self.fromHLTQueue.task_done()
                break
            while True:
                try:
                    self.commServer.triggerStateChange(newState)
                    break
                except Exception as e:
                    logger.warning('Could not propagate new HLT state to ECS proxy: '+str(e)+'.')
                    if type(e)==socket_error and e.errno == errno.ECONNREFUSED:
                        logger.warning('ECS proxy not available, trying again...')
                        time.sleep(1.)
                    else:
                        if not self.RMStateMachine.fsm.current == 'ERROR':
                            self.sendNewCommandToHLT('ERROR', '')
                        break
            self.fromHLTQueue.task_done()

    def _receiveCmdThread(self):
	while True:
	    nextCommandWithParams = self.toHLTQueue.get()
            if nextCommandWithParams == 'EXIT':
                self.toHLTQueue.task_done()
                break
	    self.RMStateMachine.executeECSCommand(nextCommandWithParams[0], nextCommandWithParams[1])
            self.toHLTQueue.task_done()

    def sendNewCommandToHLT(self, command, params):
	self.toHLTQueue.put([command, params]) 

    def sendNewStateToECS(self, newState):
	self.fromHLTQueue.put(newState)


if __name__ == "__main__":   

    try: 
	basedir = os.environ["ECS_PROXY_DIR"]
    except:
	print 'Could not retrieve environment variable ECS_PROXY_DIR. Defaulting to ./conf as config file path.'	
	basedir = './conf'
    try:
	config = EnvConfigParser()
	config.read(basedir+'/conf/proxy.conf')
	ECS_PROXY_LISTENER_HOST=config.get('ECSProxy', 'listener_host')
	ECS_PROXY_LISTENER_PORT=config.get('ECSProxy', 'listener_port')
    	RM_PROXY_LISTENER_PORT=config.get('RMProxy', 'listener_port')
    except:
	print 'Could not parse config file '+basedir+'/conf/proxy.conf. Exiting.'
	sys.exit(1)
    try:
	logconfigfile = basedir+'/conf/'+config.get('RMProxy', 'log_config_file')
    except:
	print 'Could not find parameter log_config_file in section RMProxy of config file '+basedir+'/conf/proxy.conf. Defaulting to'+basedir+'/conf/RMProxy_logging.conf.'
	logconfigfile = basedir+'/conf/RMProxy_logging.conf'	
    try:
        logging.config.fileConfig(logconfigfile)
    except Exception as e:
        print 'Exception caught while parsing logging config file:', e
        print 'Failed to parse logging config file '+logconfigfile+'. Defaulting to stdout logging.'
	logger = logging.getLogger('RMProxy')
	logger.setLevel(logging.DEBUG)
	ch = logging.StreamHandler()
	ch.setLevel(logging.DEBUG)
	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	ch.setFormatter(formatter)
	logger.addHandler(ch)
    else:
	logger = logging.getLogger('RMProxy')	
    
    
    logger.important('Starting RunManager proxy')


    RMProxy().go()
