import logging

logging.IMPORTANT = 60

logging.addLevelName(logging.IMPORTANT, 'IMPORTANT')


def __important(self, msg, *args, **kwargs):
    if self.isEnabledFor(logging.IMPORTANT):
        self._log(logging.IMPORTANT, msg, args, **kwargs)

logging.Logger.important = __important
