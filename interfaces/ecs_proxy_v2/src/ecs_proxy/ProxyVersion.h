#ifndef ALICE_HLT_PROXY_VERSION_NUMBER_H
#define ALICE_HLT_PROXY_VERSION_NUMBER_H

/**
 * Defines the current version number of hlt-proxy
 */
#define ALICE_HLT_PROXY_VERSION_NUMBER "2.0.0"

#endif // ALICE_HLT_PROXY_VERSION_NUMBER_H

