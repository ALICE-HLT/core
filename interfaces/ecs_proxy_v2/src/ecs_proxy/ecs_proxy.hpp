#include <smirtl.hxx>
#include <mutex>
#include "ProxyLogger.hpp"
#include "ecs_proxy_comm.hpp"

namespace alice { namespace hlt { namespace proxy {

      class ecs_proxy: public SmiProxy, DimErrorHandler, DimExitHandler
{
public:
	ecs_proxy(char *domain);
	~ecs_proxy();
	void signalStateChange(std::string newState);
        virtual void errorHandler(int severity, int errorcode, char* reason);
        virtual void exitHandler(int code);
protected:
	void execAction(char *action);
	void smiCommandHandler();

private:
	ProxyLogger* logger;
	bool errorLock;
	std::mutex accessToErrorLock;
	ecs_proxy_config config; 
	ecs_proxy_comm* comm;
	
};

}}}
