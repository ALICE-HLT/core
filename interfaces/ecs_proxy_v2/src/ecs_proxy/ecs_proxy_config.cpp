#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/algorithm/string.hpp>
#include "ecs_proxy_config.hpp"
#include <iostream>

using namespace alice::hlt::proxy;

ecs_proxy_config::ecs_proxy_config() {
        
	boost::property_tree::ptree pt;
	
	char* ECSProxyPath=getenv("ECS_PROXY_DIR");
        if (ECSProxyPath==0) {
                ECSProxyPath=".";
                std::cout << std::endl << "   +++ Failed to retrieve $ECS_PROXY_DIR from environment for loading the config file, defaulting to execution path (./) as base dir +++  " << std::endl << std::endl;
        }

	std::string configFile = std::string(ECSProxyPath)+"/"+std::string(ALICE_HLT_PROXY_CONFIG_FILE_PATH)+"/"+std::string(ALICE_HLT_PROXY_CONFIG_FILE);
        
	try {
		boost::property_tree::ini_parser::read_ini(configFile, pt);
	} catch (std::exception &ex) {
		throw std::runtime_error("Could not parse config file "+configFile+" : "+ex.what());
 	}
	
    	for (auto& section : pt)
    	{
        	std::string currSection = section.first;
        	for (auto& key : section.second) {
            		std::string currKey = key.first;
			std::string currVal = key.second.get_value<std::string>();
			this->configSettings.insert(std::make_pair(currSection+"."+currKey, currVal));
		}
    	}

};


std::string ecs_proxy_config::getConfigValue(std::string configParameter){
	std::string envPar=boost::to_upper_copy<std::string>(configParameter);
	std::replace(envPar.begin(), envPar.end(), '.', '_');
	char* envVal=getenv(envPar.c_str());
	if(envVal){
		return std::string(envVal);
	}
	std::map<std::string, std::string>::iterator iter = this->configSettings.find(configParameter);
  	if(iter != this->configSettings.end()) return iter->second;
	else throw std::runtime_error("The requested parameter "+configParameter+" does not exist in the config file.");
	
};
