#include <iostream>
#include <cassert>
#include "ecs_proxy.hpp"

using namespace alice::hlt::proxy;


ecs_proxy_comm::ecs_proxy_comm(ecs_proxy* myProxy, ecs_proxy_config* myConfig, ProxyLogger* myLogger) : proxy(myProxy), config(myConfig), logger(myLogger) {
	
	try{
		ECSProxyListenerPort = atoi(this->config->getConfigValue("ECSProxy.listener_port").c_str());
		if (ECSProxyListenerPort == 0) throw std::runtime_error("The config parameter ECSProxy.listener_port \
				does not hold a valid port number.");
		RMProxyListenerPort = atoi(this->config->getConfigValue("RMProxy.listener_port").c_str());
		if (RMProxyListenerPort == 0) throw std::runtime_error("The config parameter RMProxy.listener_port \
				does not hold a valid port number.");
		RMProxyListenerHost = this->config->getConfigValue("ECSProxy.listener_host");

		commServerThread = std::thread(&ecs_proxy_comm::runServerThread, this);
	
	} catch (std::exception &ex) {
 		std::cout << std::endl << "   +++ Failed to start server for communication with RunManager +++ " << std::endl;
       		this->logger->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, "Failed to start server for communication with RunManager");
		this->logger->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, ex.what());
		throw;
	} 
	std::cout << std::endl << "   +++ Listener for communication with RunManager established on port " << ECSProxyListenerPort 
				<< " +++ " << std::endl;
        this->logger->createLogMessage(MSG_IMPORTANT, LOG_SOURCE_PROXY, "Listener for communication with RunManager established on port " 
				+ std::to_string(ECSProxyListenerPort));
}




ecs_proxy_comm::~ecs_proxy_comm() {
	if (this->abyssServer) {
		this->abyssServer->terminate();
		delete this->abyssServer;
		abyssServer = 0;
	}
	this->commServerThread.join();
};

void ecs_proxy_comm::runServerThread(){

	try{
		xmlrpc_c::registry myRegistry;
		ecs_proxy_comm_server tmp(this->proxy, this->logger);
		xmlrpc_c::methodPtr const ecs_proxy_comm_serverP(&tmp);
		myRegistry.addMethod("signalStateChange", ecs_proxy_comm_serverP);
		// im afraid this http server implementation causes signalhandling in the rest of the program to fail
		abyssServer = new xmlrpc_c::serverAbyss(xmlrpc_c::serverAbyss::constrOpt().registryP(&myRegistry).portNumber(this->ECSProxyListenerPort));
		abyssServer->run();
	} catch (std::exception &ex) {
                std::cout << std::endl << "   +++ Listener for communication with RunManager failed +++ " << std::endl;
                this->logger->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, "Listener for communication with RunManager failed.");
		throw;
	}
}

void ecs_proxy_comm::relayECSCommand(std::string action, std::map<char*,char*> parMap){ 

	xmlrpc_c::paramList params;
        xmlrpc_c::value retval;
	try{
	        params.add(xmlrpc_c::value_string(action));
        	std::vector<xmlrpc_c::value> allAsVector;

        	for ( std::map< char*, char* >::const_iterator iter = parMap.begin(); iter != parMap.end(); ++iter ) {
                	std::vector<xmlrpc_c::value> currAsVector;
	                currAsVector.push_back(xmlrpc_c::value_string(std::string(iter->first)));
        	        currAsVector.push_back(xmlrpc_c::value_string(std::string(iter->second)));
                	xmlrpc_c::value_array currAsArray(currAsVector);
                	allAsVector.push_back(xmlrpc_c::value_array(currAsArray));
        	}
	        
		xmlrpc_c::value_array allAsArray(allAsVector);
        	params.add(allAsArray);
	} catch (std::exception &ex) {
                std::cout << std::endl << "   +++ Failed to serialize ECS Action command for sending it to RunManager +++ " << std::endl;
                this->logger->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, "Failed to serialize ECS Action command \
				for sending it to RunManager");
                this->logger->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, ex.what());
                throw;
        }
	try{
	        std::string const serverUrl("http://" + std::string(this->RMProxyListenerHost) + ":" + std::to_string(this->RMProxyListenerPort) + "/");
        	std::string const methodName("forwardECSCommand");
	        xmlrpc_c::clientSimple myClient;
	        myClient.call(serverUrl, methodName, params, &retval);
	} catch (std::exception &ex) {
                std::cout << std::endl << "   +++ Failed to send ECS Action command to RunManager +++ " << std::endl;
                this->logger->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, "Failed to send ECS Action command to RunManager.");
                this->logger->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, ex.what());
                throw;
        }
	std::string const myres = xmlrpc_c::value_string(retval); 
	if (myres.compare("NOK")== 0) throw std::runtime_error("RunManager failed to process ECS command.");
}



ecs_proxy_comm_server::ecs_proxy_comm_server(ecs_proxy* myProxy, ProxyLogger* myLogger): proxy(myProxy), logger(myLogger){};

void ecs_proxy_comm_server::execute(xmlrpc_c::paramList const& paramList, xmlrpc_c::value * const retvalP) {
	
	// here the error handling is split: communication partner is notified via return value about correct 
	// transmission if the payload contains exactly one item;  
	// any further errors in setting this new state in the proxy/ECS are not communicated via return value but via ERROR command
	
	if (paramList.size() == 1) {
		std::string const newState = paramList.getString(0);
		*retvalP = xmlrpc_c::value_string("OK");
		this->proxy->signalStateChange(newState);
	        
	} else {
                std::cout << std::endl << "   +++ Received syntactically incorrect state response from RunManager +++ " << std::endl;
                this->logger->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, "Received syntactically incorrect \
				state response from RunManager.");
                *retvalP = xmlrpc_c::value_string("NOK");
		return;
        }
};






