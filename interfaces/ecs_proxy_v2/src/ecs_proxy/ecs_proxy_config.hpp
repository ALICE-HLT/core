#ifndef ALICE_HLT_PROXY_CONFIG_HPP
#define ALICE_HLT_PROXY_CONFIG_HPP

// config file relative to ECS_PROXY_DIR
#define ALICE_HLT_PROXY_CONFIG_FILE "proxy.conf"
#define ALICE_HLT_PROXY_CONFIG_FILE_PATH "./conf"

#include <map>
#include <string>

namespace alice { namespace hlt { namespace proxy {

class ecs_proxy_config
{
public:
   ecs_proxy_config();
   std::string getConfigValue(std::string configParameter);
private:
   std::map<std::string, std::string> configSettings;
};

}}}

#endif
