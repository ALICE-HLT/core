#ifndef ALICE_HLT_PROXY_INFOLOGGER_PROXY_LOGGER_HPP
#define ALICE_HLT_PROXY_INFOLOGGER_PROXY_LOGGER_HPP

#ifdef LIB_INFO

#include "AdditionalLoggerBase.hpp"

namespace alice { namespace hlt { namespace proxy {

      class InfologgerProxyLogger : public AdditionalLoggerBase {
      public:	
	InfologgerProxyLogger(std::string loggerName) : AdditionalLoggerBase(loggerName) {Init();};
	InfologgerProxyLogger(const InfologgerProxyLogger& albRef) : AdditionalLoggerBase(albRef) {Init();};
	virtual ~InfologgerProxyLogger() {};
	virtual bool relayLogMessage(const Timestamp& stamp, Log_Levels type, 
				     std::string origin, std::string description);
      private:
	void Init();
	InfologgerProxyLogger();
}; //end of class


} } } // end namespaces "alice", "hlt" and "proxy"

#endif // LIB_INFO

#endif // ALICE_HLT_PROXY_INFOLOGGER_PROXY_LOGGER_HPP

