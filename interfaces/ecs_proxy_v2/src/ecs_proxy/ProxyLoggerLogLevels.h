#ifndef ALICE_HLT_PROXY_LOG_LEVELS_H
#define ALICE_HLT_PROXY_LOG_LEVELS_H

/**
 * Define for the event - type "INFO" .
 */
#define MSG_INFO    1

/**
 * Define for the event - type "WARNING" .
 */
#define MSG_WARNING 2

/**
 * Define for the event - type "ERROR" .
 */
#define MSG_ERROR   4

/**
 * Define for the event - type "ERROR" .
 */
#define MSG_FATAL   8

/**
 * Define for the event - type "ERROR" .
 */
#define MSG_IMPORTANT   16

/**
 * Define for the event - type "DEBUG" .
 */
#define MSG_DEBUG   32

/**
 * Define for the event - type "ALARM" .
 */
#define MSG_ALARM   64

/**
 * Defines the maximum value of cumulated event - types.
 */
#define MSG_MAX_VAL 127

/**
 * Size of the date field in messages (in byte).
 */
#define MSG_DATE_SIZE           20

/**
 * Default Log source.
 */
#define LOG_SOURCE_PROXY "ECSProxy"

/**
 * Typedef for a log level
 */
typedef unsigned int Log_Levels;

#endif
