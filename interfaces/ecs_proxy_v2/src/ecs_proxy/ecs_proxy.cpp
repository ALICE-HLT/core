#include <iostream>
#include "ecs_proxy.hpp"


using namespace alice::hlt::proxy;

ecs_proxy::ecs_proxy(char *domain): SmiProxy(getenv("HLTECS_MODULE")), DimErrorHandler(), DimExitHandler() {

	errorLock = false;
	std::mutex accessToErrorLock;

	std::cout << std::endl << "   +++ ECS-Proxy version: " << ALICE_HLT_PROXY_VERSION_NUMBER << " +++ " << std::endl << std::endl;
	logger = ProxyLogger::getLogger();

	// DIM error/exit handler
	DimServer::addErrorHandler(this);
	DimServer::addExitHandler(this);

        this->logger->createLogMessage(MSG_IMPORTANT, LOG_SOURCE_PROXY, "ECS-Proxy version: " + std::string(ALICE_HLT_PROXY_VERSION_NUMBER));
        this->logger->createLogMessage(MSG_IMPORTANT, LOG_SOURCE_PROXY, "ECS-Proxy: attaching to domain " + std::string(domain));
	attach(domain);	
	// this assumes that ECSProxy is always started before RMProxy and RMProxy initializes to state UNDEFINED
        // currently we can restart ECSProxy during runtime of RMProxy (e.g. rm state==CONFIGURED), 
	// however upon first ECS command RMProxy will go to ERROR and we lose the RM state. 
	// if we want to be able to restart ECSProxy without losing the state of an active RMProxy session (i.e. 
	// setting the HLT state as ECSProxy start state upon restart of ECSProxy) we need to actively query 
	// RMProxy for its current state upon ECSProxy startup which is not implemented atm
        setState("UNDEFINED");

	try{
		comm =  new ecs_proxy_comm(this, &config, logger);
	} catch (std::exception &ex) {
        	std::cout << std::endl << "   +++ Failed to initialize ECS-Proxy +++ " << std::endl;
        	this->logger->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, "Failed to initialize ECS-Proxy.");
		throw;
        }

	try{
	  execAction("SET_ACTIVE");
	} catch (girerr::error &ex) {
	  this->logger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, "XMLRPC communication with RunManager failed.");
	  setState("ERROR");
	}
	std::cout << std::endl << "   +++ Initialization of ECS-Proxy finished +++ " << std::endl;
	this->logger->createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, "Initialization of ECS-Proxy finished.");
};

ecs_proxy::~ecs_proxy() {
	
	// logger is a singleton; this way of destroying is bad
	if (this->logger) {
                delete this->logger;
                this->logger = 0;
        }
	if (this->comm) {
                delete this->comm;
                this->comm = 0;
        }

}; 

void ecs_proxy::errorHandler(int severity, int code, char* reason){
  if (logger){
    int logLevel=MSG_IMPORTANT;
    switch(severity){
    case DIM_WARNING:
      logLevel=MSG_WARNING;
      break;
    case DIM_ERROR:
      logLevel=MSG_ERROR;
      break;
    case DIM_FATAL:
      logLevel=MSG_FATAL;
      break;
    default:
      logLevel=MSG_INFO;
    }
    logger->createLogMessage(logLevel, LOG_SOURCE_PROXY,
			      "ECS-Proxy: DIM Message (severity=" + std::to_string(severity) +
			      ",code=" + std::to_string(code) +
			      "): " + std::string(reason));
  }
}

void ecs_proxy::exitHandler(int code){
  if (logger){
    logger->createLogMessage(MSG_IMPORTANT, LOG_SOURCE_PROXY,
			     "ECS-Proxy: DIM EXIT HANDLER called, exiting with code: " + std::to_string(code) );
  }
  raise(SIGINT);
}

void ecs_proxy::execAction(char *action){

	std::cout << std::endl << "   +++ Proxy received " << action << " command from ECS +++ " << std::endl;
	this->logger->createLogMessage(MSG_IMPORTANT, LOG_SOURCE_PROXY, "Proxy received '"+std::string(action)+"' command from ECS.");
	
	char* currPar = 0;
	char* currParVal = 0;
	std::map<char*,char*> parMap;

	try{
		while(true) { 
	    		currPar = getNextParameter();
	    		if (currPar != 0) {
				currParVal = getParameterString(currPar);
				std::cout << std::endl << "   +++ Proxy received parameter " << currPar << "  :  " 
					<< currParVal << " from ECS +++ " << std::endl;
	        		this->logger->createLogMessage(MSG_IMPORTANT, LOG_SOURCE_PROXY, "ECS-Proxy received parameter " 
					+ std::string(currPar) + " :  " + std::string(currParVal) + " from ECS.");
				parMap.insert(std::make_pair(currPar,currParVal));
  	    		}
            		else break;
		}
	}catch (std::exception &ex){
		std::cout << std::endl << "   +++ Failed to extract ECS command parameters +++ " << std::endl;
                this->logger->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, "Failed to extract ECS command parameters.");
		this->logger->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, ex.what());
                throw;	  
	}


	this->comm->relayECSCommand(action, parMap);
 		
}

void ecs_proxy::smiCommandHandler(){
	try{
		execAction(getAction());
	}catch (std::exception &ex) {
		this->logger->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, "Relaying ECS command to RunManager failed. Propagating ERROR state to ECS.");
	        std::cout << std::endl << "   +++ Relaying ECS command to RunManager failed. Propagating ERROR state to ECS  +++ " 
				<< std::endl;
		// ignoring the error lock "feature" for now
                //this->accessToErrorLock.lock();
		//this->errorLock=true; 
		setState("ERROR"); 
		//this->accessToErrorLock.unlock();
	}
}
	
void ecs_proxy::signalStateChange(std::string newState) {

	this->accessToErrorLock.lock();
	this->logger->createLogMessage(MSG_IMPORTANT, LOG_SOURCE_PROXY, "Proxy received feedback from RunManager - new HLT state is "
                                + std::string(newState));
        std::cout << std::endl << "   +++ Proxy received feedback from RunManager - new HLT state is " 
				<< newState << " +++ " << std::endl;

	// if we are in ERROR due to failed communication then accept no new HLT states until ERROR has been acknowledged by HLT
        if(this->errorLock){
		if (!strcmp(getState(), "ERROR") && !strcmp(newState.c_str(), "ERROR")) this->errorLock = false;
                else {
                        try {
                                this->logger->createLogMessage(MSG_IMPORTANT, LOG_SOURCE_PROXY, "ECS is in ERROR state, trying to synchronize RunManager first before accepting new states from RunManager.");
                                std::cout << std::endl << "   +++ ECS is in ERROR state, trying to synchronize RunManager first "
					<< "before accepting new states from RunManager. +++ " << std::endl << std::endl;
                                std::map<char*,char*> parMap;
                                this->comm->relayECSCommand("ERROR",parMap);
                        }catch (std::exception &ex) {
                                this->logger->createLogMessage(MSG_IMPORTANT, LOG_SOURCE_PROXY, "Failed to inform RunManager on \
					ECS ERROR state, retaining ECS ERROR state.");
                                std::cout << std::endl << "   +++ Failed to inform RunManager on ECS ERROR state, " 
					<< "retaining ECS ERROR state.  +++ " << std::endl << std::endl;
                        }
                }
	}else{
		// we are not in error
	        if (!strcmp(newState.c_str(), "UNDEFINED")) {
			// Runmanager is ramping up
			if (!strcmp(getState(), "UNDEFINED")) {
				// ECS is in OFF state, initializing runmanager state machine
				try{
					std::map<char*,char*> parMap;
                         		this->comm->relayECSCommand("INITRM",parMap);
                        	}catch (std::exception &ex) {
                                	this->logger->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, 
						"Failed to properly initialize RunManager, setting ECS ERROR state.");
                                	std::cout << std::endl << "   +++ Failed to properly initialize RunManager, " 
						<< "setting ECS ERROR state.  +++ " << std::endl << std::endl;
					this->errorLock = true;
					setState("ERROR");
                        	}
			}else{
				// ECS is not in OFF state, setting ERROR and sending state to runmanager
				this->logger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, "RMProxy is ramping up but ECS is in "
					 + std::string(getState())+ " state. Propagating ERROR to RunManager.");
		                std::cout << std::endl << "   +++ RMProxy is ramping up but ECS is in " << getState()
	                                << " state. Propagating ERROR to RunManager. +++ " << std::endl;
                		try{
                        		std::map<char*,char*> parMap;
                        		setState("ERROR");
                        		this->errorLock=true;
                        		this->comm->relayECSCommand("ERROR",parMap);
                		}catch (std::exception &ex) {
                        		this->logger->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY,
                                		"Failed to inform RunManager on ECS ERROR state, retaining ECS ERROR state.");
                        		std::cout << std::endl << "   +++ Failed to inform RunManager on ECS ERROR state, " 
						<< "retaining ECS ERROR state.  +++ " << std::endl << std::endl;
                		}
			}	
		}else{
			// Default branch
			try{
                        	setState(const_cast<char*>(newState.c_str()));
                	}catch (std::exception &ex){
                        	// this path is a crash, ecs-side debugging needed to find out why setting the state failed
                        	this->logger->createLogMessage(MSG_ERROR, LOG_SOURCE_PROXY, ex.what());
                        	std::cout << std::endl << "   +++ " << ex.what() << " +++ " << std::endl;
                        	setState("ERROR");
                        	this->errorLock=true;
                        	std::map<char*,char*> parMap;
                        	try{
                                	this->comm->relayECSCommand("ERROR",parMap);
                        	}catch (std::exception &ex) {
                                	this->logger->createLogMessage(MSG_IMPORTANT, LOG_SOURCE_PROXY, "Failed to inform RunManager \
						on ECS ERROR state, retaining ECS ERROR state.");
                                	std::cout << std::endl << "   +++ Failed to inform RunManager on ECS ERROR state, " 
						<< "retaining ECS ERROR state.  +++ " << std::endl << std::endl;
                        	}
			}
		}
        }
        this->accessToErrorLock.unlock();
}



