/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2006
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include "ProxyLogger.hpp"


#include <iostream>
#include <sstream>
#include <cstring>
#include <cstdlib>

using namespace std;

using namespace alice::hlt::proxy;


const string ProxyLogger::cmMSG_INFO_STRING      = " [MSG_INFO] ";

const string ProxyLogger::cmMSG_WARNING_STRING   = " [MSG_WARN] ";

const string ProxyLogger::cmMSG_ERROR_STRING     = " [MSG_ERROR] ";

const string ProxyLogger::cmMSG_FATAL_STRING     = " [MSG_FATAL] ";

const string ProxyLogger::cmMSG_IMPORTANT_STRING = " [MSG_IMPT] ";

const string ProxyLogger::cmMSG_DEBUG_STRING     = " [MSG_DEBG] ";

const string ProxyLogger::cmMSG_ALARM_STRING     = " [MSG_ALARM] ";



ProxyLogger* ProxyLogger::mpInstance = 0;


ProxyLogger* ProxyLogger::createLogger(string filename) {
	if (mpInstance != 0) {
		// make log entry
		mpInstance->createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
			"Trying to create Logger, but Logger already exists. Using old one (filename not changed).");
		return mpInstance;
	}
	mpInstance = new ProxyLogger(filename);
	return mpInstance;
}

ProxyLogger::ProxyLogger(string filename) {
    
	ecs_proxy_config config; 
        mLogLevel = 63;
        try{
                mLogLevel = atoi(config.getConfigValue("ECSProxy.log_level").c_str());
        }catch (std::exception &ex) {
                std::cout << std::endl << "   +++ Failed to load config parameter ECSProxy.log_level, defaulting to log_level 63 (DEBUG+INFO+WARNING+ERROR) +++ " << std::endl << std::endl;
        }
	mpLogFile = 0;

	// init the logging mutex
	pthread_mutex_init(&mLoggingMutex, 0);

	Timestamp now;
	time_t timeVal;
    time(&timeVal);
    struct tm* timeStruct = localtime(&timeVal);

    // get log file handle
	if ((filename.length() == 0) && (timeStruct != NULL)) { 
		char tempName[40];
		tempName[sprintf(tempName, "ECSProxy-%04d-%02d-%02d-(%02d-%02d).log",
                (timeStruct->tm_year + 1900), (timeStruct->tm_mon + 1),
                timeStruct->tm_mday, timeStruct->tm_hour,
                timeStruct->tm_min)] = 0;
		mFileName = tempName;
    } else { 
        mFileName = filename;
    }

	mpLogFile = new ofstream(mFileName.c_str(), ios_base::out | ios_base::app);

    if (!(*mpLogFile)) {
        cout << "Unable to create logfile (" << mFileName << ")." << endl;
    } else {
        string msg;
        *mpLogFile << endl;
		*mpLogFile << "   --- LOG FILE started at " << now.date << 
				", version: " << ALICE_HLT_PROXY_VERSION_NUMBER << " ---" << endl;
//		msg = now.date + cmMSG_INFO_STRING + "- ECSProxy: '";
//        *mpLogFile << msg << getenv("ECSHLT_MODULE") << 
//				"' started." << endl;

#ifdef __EMULATOR
		msg = now.date + cmMSG_WARNING_STRING + 
				"- ECS-Proxy: *** NOTE that proxy has been compiled with the emulation of (Master)TaskManager! ***";
		*mpLogFile << msg << endl;
		msg = now.date + cmMSG_WARNING_STRING + 
				"- ECS-Proxy: *** NOTE there will be no contact to real (Master)TaskManager! ***";
		*mpLogFile << msg << endl;
#endif

    }

#ifdef __EMULATOR
	cout << endl << 
			" NOTE: This proxy has been compiled with the emulation of (Master)TaskManager!" 
			<< endl;
	cout << "  --> No contact to real (Master)TaskManager will be established!! <--" 
			<< endl << endl;
#endif

}

ProxyLogger::~ProxyLogger() {
	unsigned int num = 0;
	string msg;

	// leave if already destroyed - should not be possible
	if (mpInstance == 0) {
		return;
    }

	// clean up of additional loggers
	num = getAdditionalLoggerNumber();
	vector<AdditionalLoggerBase* >::iterator it = mAdditionalLoggers.begin();
    while (it != mAdditionalLoggers.end() ) {
		if (*it) {
			delete ((AdditionalLoggerBase*) (*it));
		}
		it++;
    }
	mAdditionalLoggers.clear();
	msg.append("Cleaned up list of ");
	msg.append(itos(num) + " additional logger(s) registerred in ProxyLogger.");
	createLogMessage(MSG_DEBUG, LOG_SOURCE_PROXY, msg);

	if (*mpLogFile) {
		Timestamp now;
        msg = now.date + cmMSG_INFO_STRING + "- ECS-Proxy: ECS-Proxy is exiting.";
		*mpLogFile << msg << endl;
		*mpLogFile << "   --- LOG FILE stopped at " << now.date << " ---" <<
				endl << endl;
		// close log file
		delete mpLogFile;
	}
	pthread_mutex_destroy(&mLoggingMutex);
	mpInstance = 0;
}

bool ProxyLogger::setLogLevel(Log_Levels logLevel) {
	bool bRet = false;
/*  !!! think about solution and then refactor this part
	if (mpInstance == 0) {
		throw exception();
	}
*/
	if ((logLevel > 0) && (logLevel <= MSG_MAX_VAL)) {
		createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, 
				"Loglevel in Proxy set to " + convertLogLevel(logLevel) + ".");
		// set new loglevel after writing out info (at beginning all are set)
		mLogLevel = logLevel | MSG_ALARM;
		bRet = true;
	} else {
		createLogMessage(MSG_WARNING, LOG_SOURCE_PROXY, 
			"Received invalid log Level: " + itos(logLevel) + ".");
		bRet = false;
	}
	return bRet;
}

void ProxyLogger::createLogMessage(Log_Levels type, string origin,
								   string description) {
	Timestamp now;

/*  !!! think about solution and then refactor this part
	if (mpInstance == 0) {
		throw exception();
	}
*/

	// check if msg log level is in logger log level
	if (!(type & mLogLevel)) {
		return;
	}

	// write log message to file after making call thread safe
	lockLoggingMutex();
	if (*mpLogFile) {
		string msg;
		msg = now.date + convertLogLevel(type) + "- " + origin + ": " + description;
        *mpLogFile << msg << endl;
	}
	
	// relay log message to registered additional loggers
	relayLogMessageToAdditionalLoggers(now, type, origin, description);	
	
	// release mutex
	unlockLoggingMutex();
}

void ProxyLogger::lockLoggingMutex() {
	int status = 0;
	status = pthread_mutex_lock(&mLoggingMutex);
	if (status != 0) {
		cout << "Lock logging dog mutex error: " << status << endl;
	}
}

void ProxyLogger::unlockLoggingMutex() {
	int status = 0;
	status = pthread_mutex_unlock(&mLoggingMutex);
	if (status != 0) {
		cout << "Unlock logging dog mutex error: " << status << endl;
	}
}


string ProxyLogger::convertLogLevel(Log_Levels logLevel) {
	string logString;
	if ((logLevel <= 0) || (logLevel > MSG_MAX_VAL)) {
		logString = "Convert ERROR: " + itos(logLevel);
		return logString;
	}
	if (logLevel & MSG_INFO) {
		logString.append(cmMSG_INFO_STRING);
	} 
	if (logLevel & MSG_WARNING) {
		if (!logString.empty()) {
			logString.append(", ");
		}
		logString.append(cmMSG_WARNING_STRING);
	} 
	if (logLevel & MSG_ERROR) {
		if (!logString.empty()) {
			logString.append(", ");
		}
		logString.append(cmMSG_ERROR_STRING);
	} 
	if (logLevel & MSG_FATAL) {
		if (!logString.empty()) {
			logString.append(", ");
		}
		logString.append(cmMSG_FATAL_STRING);
	} 
	if (logLevel & MSG_IMPORTANT) {
		if (!logString.empty()) {
			logString.append(", ");
		}
		logString.append(cmMSG_IMPORTANT_STRING);
	} 
	if (logLevel & MSG_ALARM) {
		if (!logString.empty()) {
			logString.append(", ");
		}
		logString.append(cmMSG_ALARM_STRING);
	} 
	if (logLevel & MSG_DEBUG) {
		if (!logString.empty()) {
			logString.append(", ");
		}
		logString.append(cmMSG_DEBUG_STRING);
	}
	return logString;
}


string ProxyLogger::itos(int num) {
	ostringstream stream;
	stream << num;
	return stream.str();
}

string ProxyLogger::ftos(float num) {
	ostringstream stream;
	stream << num;
	return stream.str();
}

string ProxyLogger::catos(const char* const buf) {
	if (buf == 0) {
		string temp("NULL");
		return temp;
	}
	ostringstream stream;
	stream << buf;
	return stream.str();
}

string ProxyLogger::ctos(char buf) {
	ostringstream stream;
	stream << buf;
	return stream.str();
}

string ProxyLogger::errnotos(int num) {
	char buf[32];
	ostringstream stream;
	char* buf2 = strerror_r(num, buf, 32); 
	// NOTE: implementation of strerror_r(...) differs from explanation in man pages
	if (buf2 == 0) {
		stream << "UNKNOWN-ERROR";
	} else {
		stream << buf2;
	}
	return stream.str();
}

string ProxyLogger::getAdditionalLoggerNames() {
	string loggerNames;

	if (mAdditionalLoggers.empty()) {
		loggerNames = "NONE; ";
		return loggerNames;
	}

	vector<AdditionalLoggerBase* >::iterator it = mAdditionalLoggers.begin();
	while (it != mAdditionalLoggers.end() ) {
		loggerNames.append(((AdditionalLoggerBase*) (*it))->getLoggerName());
		loggerNames.append("; ");
		it++;
	}
	return loggerNames;
}

unsigned int ProxyLogger::relayLogMessageToAdditionalLoggers(const Timestamp& stamp,
			Log_Levels type, std::string origin, std::string description) {
	unsigned int count = 0;

    vector<AdditionalLoggerBase* >::iterator it = mAdditionalLoggers.begin();
    while (it != mAdditionalLoggers.end() ) {
		if (((AdditionalLoggerBase*) (*it))->relayLogMessage(stamp, type, 
				origin,	description)) {
	     	count++;
		} else {
			cout << "[Proxy-Logger] Failed to relay log messsage (\"" <<
					description << "\") to '" <<
					((AdditionalLoggerBase*) (*it))->getLoggerName() << "'." <<
					endl;
		}
		it++;
    }
	return count;
}


bool ProxyLogger::registerAdditionalLogger(AdditionalLoggerBase* addLogger) {
	bool bRet = true;
	string msg;
	
	mAdditionalLoggers.push_back(addLogger);
	addLogger->setProxyLoggerRef(this);

	cout << "[Proxy-Logger] Registered additional logging module: '" << 
			addLogger->getLoggerName() << "'." << endl;

	msg.append("Added additional logger '");
	msg.append(addLogger->getLoggerName() + "' to ProxyLogger. List of loggers: ");
	msg.append(getAdditionalLoggerNames() + ".");
	createLogMessage(MSG_INFO, LOG_SOURCE_PROXY, msg);
	return bRet;
}


