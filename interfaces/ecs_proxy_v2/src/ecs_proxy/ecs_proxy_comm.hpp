#ifndef ALICE_HLT_PROXY_COMM_HPP
#define ALICE_HLT_PROXY_COMM_HPP

#include <thread>
#include <xmlrpc-c/base.hpp>
#include <xmlrpc-c/registry.hpp>
#include <xmlrpc-c/server_abyss.hpp> 
#include <xmlrpc-c/girerr.hpp>
#include <xmlrpc-c/client_simple.hpp>
#include "ProxyLogger.hpp"
#include "ecs_proxy_config.hpp"

namespace alice { namespace hlt { namespace proxy {

class ecs_proxy;


class ecs_proxy_comm
{
	public:
                ecs_proxy_comm(ecs_proxy* proxy, ecs_proxy_config* config, ProxyLogger* logger);
		void relayECSCommand(std::string action, std::map<char*,char*> parMap);
		~ecs_proxy_comm();
	private:
		ecs_proxy* proxy;
		ecs_proxy_config* config;
		ProxyLogger* logger;
		xmlrpc_c::serverAbyss* abyssServer;
		int ECSProxyListenerPort;
		int RMProxyListenerPort;
		std::string RMProxyListenerHost;
		std::thread commServerThread;
		void runServerThread();
};

class ecs_proxy_comm_server : public xmlrpc_c::method 
{
	public:
		ecs_proxy_comm_server(ecs_proxy* proxy, ProxyLogger* logger);
		void execute(xmlrpc_c::paramList const& paramList, xmlrpc_c::value * const retvalP); 
	private:
		ecs_proxy* proxy;
		ProxyLogger* logger;
		
};

}}};

#endif
