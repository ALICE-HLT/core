/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2008
** This file has been written by Sebastian Bablok,
** sebastian.bablok@ift.uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/

#include "AdditionalLoggerBase.hpp"

#include "ProxyLogger.hpp"


using namespace std;

using namespace alice::hlt::proxy;


AdditionalLoggerBase::AdditionalLoggerBase(const AdditionalLoggerBase& albRef) {
    mLoggerName = albRef.mLoggerName;
    mpProxyLog = albRef.mpProxyLog;
}

AdditionalLoggerBase& AdditionalLoggerBase::operator=(
			const AdditionalLoggerBase& rhs) {
    if (this == &rhs) {
        return *this;
    }

	mLoggerName = rhs.mLoggerName;
	mpProxyLog = rhs.mpProxyLog;

    return *this;
}


string AdditionalLoggerBase::convertLogLevel(Log_Levels logLevel) {
	return ProxyLogger::convertLogLevel(logLevel);
}

string AdditionalLoggerBase::itos(int num) {
	return ProxyLogger::itos(num);
}

string AdditionalLoggerBase::ftos(float num) {
	return ProxyLogger::ftos(num);
}

string AdditionalLoggerBase::catos(const char* const buf) {
	return ProxyLogger::catos(buf);
}

string AdditionalLoggerBase::ctos(char buf) {
	return ProxyLogger::ctos(buf);
}

string AdditionalLoggerBase::errnotos(int num) {
	return ProxyLogger::errnotos(num);
}


