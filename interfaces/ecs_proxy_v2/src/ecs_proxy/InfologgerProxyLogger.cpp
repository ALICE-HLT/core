
#ifdef LIB_INFO

extern "C" {
#include "infoLogger.h"
}

#include "InfologgerProxyLogger.hpp"

using namespace std;

using namespace alice::hlt::proxy;

void InfologgerProxyLogger::Init(){
  infoSetFacility(LOG_SOURCE_PROXY);
}

bool InfologgerProxyLogger::relayLogMessage(const Timestamp& /* stamp */, Log_Levels type,
		     std::string /* origin */, std::string description) {

  char infoLogLevel=LOG_DDEBUG;
  if      (type & MSG_ALARM)     infoLogLevel=LOG_IMPORTANT;
  else if (type & MSG_IMPORTANT) infoLogLevel=LOG_IMPORTANT;
  else if (type & MSG_FATAL)     infoLogLevel=LOG_FATAL;
  else if (type & MSG_ERROR)     infoLogLevel=LOG_ERROR;
  else if (type & MSG_WARNING)   infoLogLevel=LOG_WARNING;
  else if (type & MSG_INFO)      infoLogLevel=LOG_INFO;

  infoLogS_f(infoLogLevel, description.c_str());

  return true;
}

#endif //LIB_INFO
