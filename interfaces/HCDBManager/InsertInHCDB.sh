#!/bin/bash

#/****************************************************************************/
#**
#**
#** This file is property of and copyright by the Department of Physics
#** Institute for Physic and Technology, University of Bergen,
#** Bergen, Norway, 2008
#** This file has been written by Sebastian Bablok,
#** sebastian.bablok@ift.uib.no
#**
#** Important: This file is provided without any warranty, including
#** fitness for any particular purpose.
#**
#**
#******************************************************************************/


## This script allows to insert configuartion objects into the HCDB

## Vars
## generall settings
HCDBBase="/opt/HCDB"
			# the HCDB Base folder
HLTTempPath="HLT"
			# Detector path in HCDB (1. part of path) for temporary storage
HLTPermPath="HLTPermanent"
			# Detector path in HCDB (1. part of path) for permanent storage
ConfigFolder="Config"		
			# HCDB subfolder where configuration objects are inserted,
			# will be suffixed with detector code
SlaveLib_listenAddress_protocol="tcpmsg://"
            # The protocol for the local listen address of the SlaveControl lib,
            # used to contact the TaskManager (notifying about HCDB update).
SlaveLib_listenAddress_append=".internal:42024"
            # string that shall be appended to the local listen address of the
            # SlaveControl lib after te hostname. The end of address
            # represents domain and port number.
            # complete address example: [tcpmsg://portal-dcs0.internal:42024]
			# Don't use 42025 as port number, this is used by the Pendolino.
TaskManager_address_base="tcpmsg://portal-ecs0.internal:"
            # address of the TaskManger that shall be notified
            # [tcpmsg://portal-ecs0.internal:]
			# change it to 'tcpmsg://portal-ecs1.internal:' if the second 
			# portal is used by the Master TaskManager.
TaskManager_config_base="${HLTCONTROL_TOPDIR}/configs/" #"/opt/HLT/control/configs"
			# the base path were the configurations for the Master 
			# TaskManager are stored
TaskManager_config_file="settings"
			# the file name, in which for each Master TaskManager the settings
			# are stored.
TM_notifyCommand_base="trigger_reconfigure_event" # "trigger_reconfigure_event_test"
			# base command to notify Master TaskManager about new HCDB entry
			# [ 'trigger_reconfigure_event' real command ]
			# [ 'trigger_reconfigure_event_test' used for testing ]
VersionNumber="v1.5.1"
			# version number of the script


## other vars
method="none"
confString=""
fileName=""
objectName=""
userName=""
startRun=0
endRun=0
fullPath=""
permanent="no"
detectorName=""

ComponentList=""
completeComponentString=""
SlaveLib_listenAddress=""
TaskManager_address=""
TaskManager_address_port=0
TaskManager_detector="ALICE"
TaskManager_config_complete_path=""
PID_tm_notifier=0
temp_perm=""
directPar="--direct"


# List of detectors
# "ITS", "ITS", "ITS", "TPC", "TRD", "TOF", "PHOS", "PHOS", "HMPID", "EMCAL", "MUON", "MUON", "FMD", "ZDC", "PMD", "T0", "VZERO", "GRP"


## Functions
## prints out the usage of this script
function usage {
    echo "Usage ($VersionNumber):"
    echo "  $0 <[--string <Configuration_String>] [--file <filename>]> <--name ObjectName> "
	echo "     <--range startRun endRun> <--detector DetectorName> [--permanent] [--help]"
    echo ""
	echo "    This command line tool allows to enter Configuration entries in the HCDB."
	echo "    After inserting the entry into the HCDB, the script prompts to let the "
	echo "    user specify which component shall be notified about the new entry by the"
	echo "    Master TaskManager. This can be a single component or a list of components"
	echo "    separated by a blank ' '. If no component shall be specified, just hit"
	echo "    'RETURN' when asked for the components. This specification is relayed to"
	echo "    the components via the Master TaskManager by a special software event."
	echo "    Wildcards are possible."
	echo "    In addition the partition / detector corresponding the current Master"
	echo "    TaskManager has to be specified; for a global run use 'ALICE'."
	echo ""
	echo ""
    echo "    --string: Following parameter is a configuration string, that shall be"
    echo "                converted to a Configuration Object. A string containing"
	echo "                blanks has to be surrounded by ' ' ."
	echo "                (e.g. --string 'HV=5 LV=3')"
    echo ""
    echo "    --file: Following parameter is ROOT-filename (incl. path; the path has to"
	echo "                be the absolute path (mandatory) !!), that shall be inserted"
    echo "                to the HCDB. (e.g. --file /tmp/TrackerConf.root)"
	echo "                NOTE: The name of TObject inside the ROOT-file has to be the"
	echo "                same like the 'ObjectName' provided by paramter '--name'"
	echo "                -> the ConfigurationObject name will be the same like the"
	echo "                name of the inserted TObject."
	echo ""
	echo "    --name: Following parameter is the name that the Configuration object, that"
	echo "                shall be used in the HCDB for this object."
	echo "                NOTE: The name will be prefixed by the following subfolder"
	echo "                structure in the HCDB: "
	echo "                $HLTTempPath/$ConfigFolder<DetectorName>,"
	echo "                in case of storage in temporary mode; else $HLTTempPath is"
	echo "                replaced by $HLTPermPath, (see below)."
	echo "                Accessable by: "
	echo "                $HLTTempPath/$ConfigFolder<DetectorName>/<ObjectName>."
	echo "                NOTE: The first part of the path will be either $HLTTempPath "
	echo "                for temporary storage in HCDB (deleted before next run) or "
	echo "                $HLTPermPath for permanent storage inside the HCDB "
	echo "                (see also '--permanent')"
	echo ""
	echo "    --range: Following two parameters define the run range ('startRun' til" 
	echo "                'endRun'), for which the configuration object shall be valid."
	echo "                (e.g. --range 13 678; use '-1' as endRun for infinite validity)"
	echo ""
	echo "    --detector: Following parameter defines the detector for which this"
	echo "                Configuration object is designed. The detector name is added to"
	echo "                the subPath2 in the HCDB folder structure; see description of "
	echo "                '--name' above. Use the detector abbreviations (TPC, TRD, ...)."
	echo ""
	echo "    --permanent (optional): Defines, if Configuration object shall be stored to"
	echo "                the permanent section inside the HCDB. When provided, the "
	echo "                permanent storage will be used. Temporary storage path is"
	echo "                '$HLTTempPath/$ConfigFolder<DetectorName>/<ObjectName>'"
	echo "                permanent storage path is"
	echo "                '$HLTPermPath/$ConfigFolder<DetectorName>/<ObjectName>'"
	echo ""
	echo "    --help: Prints out this usage."
	echo ""
	echo "  Either '--string <Configuration_String>' OR '--file <filename>' has to be"
	echo "  provided together with '--name ObjectName'."
	echo "  NOTE: The order of the given parameters is important: "
	echo "  First '--string' (or '--file'), then '--name' followed by '--range' and "
	echo "  '--detector', as last (optionally) '--permanent'." 
	echo ""
	echo "  NOTE: The environment variable 'ALIHLT_HCDBDIR' has to be set in order to run"
	echo "        the script. Current value is: $ALIHLT_HCDBDIR ."
    echo ""
    echo ""
    echo " e.g. $0 --string 'HV=5 LV=3' --name TrackerVoltageSetting --range 13 678 --detector MUON"
	echo ""
	echo " e.g. $0 --file /tmp/AliESDs.root --name esdTree --range 12 -1 --detector PHOS --permanent"
    echo ""
    exit 9
}

## Function to print debug output
function debug {
	echo "Settings:"
	echo "  HCDBBase=$HCDBBase; ConfigFolder=$ConfigFolder"
	echo "  method=$method; confString='$confString'"
	echo "  fileName=$fileName; objectName=$objectName"
	echo "  userName=$userName; startRun=$startRun; endRun=$endRun"
	echo "  HLTTempPath=$HLTTempPath, HLTPermPath=$HLTPermPath, fullPath=$fullPath"
	echo "  permanent=$permanent, detectorName=$detectorName"
	echo ""
}

## function to intialize all required AliRoot stuff
function initAliRoot {
## Shall be set bet operator in advance
#	echo "   --- Initialize AliRoot."
#   source ~/addEnv.sh
	echo ""
}

## function to update HCDB on computing nodes
function releaseHCDB {
	# call script of Jochen to release HCDB
	# release HCDB after update
	if [ $USER == "hlt-operator" ]; then
    	/opt/HLT/tools/bin/release-HCDB.sh
    	# check for exit value of scripts -> but what can be done else ?
    	retVal=$?
	else
		echo "   ### No release of real HCDB, user is not hlt-operator, using local HCDB."
	fi
	informMasterTM
	echo "   +++ Notification done, finished...."
}

## function to inform Master TaskManager
function informMasterTM {
    # interactive user entry for Master TaskManager contact
	echo ""
    echo " Please specify the component(s) [separated by ' ' (blank space)], that shall"
    echo " be notified about the new entry - if left blank, all components are notified."
	echo " (Don't use quotes around the names.)"
    echo "->: " 
	read ComponentList
	echo ""
    echo " And now please enter the partition (detector) of the corresponding Master"
	echo " TaskManager; for global run enter 'ALICE' (if left blank 'ALICE' is used)."
	echo " (Don't use quotes around the name; if 'no' is given, no notification is sent.)"
	echo "->: " 
	read TaskManager_detector


	if [ ! $TaskManager_detector ]; then
		TaskManager_detector="ALICE"
	fi

	# leave function if 'no' is given
	if [ $TaskManager_detector == "no" ]; then
		return
	fi

	# prepare Component list string:
	for s in ${ComponentList}; do
#    	echo "String: $s"
    	completeComponentString="id:$s $completeComponentString"
	done

	# prepare contact
	TaskManager_config_complete_path=$TaskManager_config_base/$TaskManager_detector/$TaskManager_config_file
#	echo " DEBUG: $TaskManager_config_complete_path"
	if [ ! -e $TaskManager_config_complete_path ]; then
		echo "   *** ERROR: configuration for given partition / detector does not exist."
		echo "   *** Complete search path is: $TaskManager_config_complete_path"
		echo "   *** Exiting without notifying Master TaskManager about new entry."
		exit 9
	fi
	source $TaskManager_config_complete_path
	TaskManager_address_port=$MasterTaskManagerBasePort	

	# prepare local listen address
	SlaveLib_listenAddress=$SlaveLib_listenAddress_protocol`hostname`$SlaveLib_listenAddress_append
	TaskManager_address=$TaskManager_address_base$TaskManager_address_port

# temp_perm shall now include the complete path of the entry (2008-02-13)
#	if [ $permanent = "yes" ]; then
#		temp_perm="permanent"
#	else
#		temp_perm="temporary"
#	fi

	temp_perm="path:$fullPath"
	TM_notifyCommand="$TM_notifyCommand_base $temp_perm $completeComponentString"
	
#	## DEBUG output
#	echo " [DEBUG:] local listen address: $SlaveLib_listenAddress"
#	echo " [DEBUG:] Master TaskManager address: $TaskManager_address"
#	echo " [DEBUG:] Notification command: '$TM_notifyCommand'"
#	echo ""

	# inform TaskManager
	echo "   --- Informing TaskManager about new Entry in HCDB"
#	./pet/TM_Notifier $SlaveLib_listenAddress $TaskManager_address "$TM_notifyCommand" $directPar # &
	$ALIHLT_DC_BINDIR/TM_Notifier $SlaveLib_listenAddress $TaskManager_address "$TM_notifyCommand" $directPar

# Not used due to direct sending of command...
#	PID_tm_notifier=$!
#	sleep 1
#	kill -a -s 12 $PID_tm_notifier
#	sleep 1
#	kill -a -s 10 $PID_tm_notifier
#	wait $PID_tm_notifier
}


## main part of script
## check for provided parameters and print usage if required
if [ $1 ]; then
    if [ $1 = "--help" ]; then
        usage   # exits after printing out usage
	elif [ $1 = "--string" ]; then
		method="string"
		echo "$2"
		if [ "$2" ] && [ "$2" != "--name" ] && [ "$2" != "--range" ]; then
			confString=$2
		else
		    echo "   *** Paramter missing, see usage..."
			echo ""
			usage   # exits after printing out usage
		fi
	elif [ $1 = "--file" ]; then
		method="file"
		if [ $2 ] && [ $2 != "--name" ] && [ $2 != "--range" ]; then
			fileName=$2
        else
			echo "   *** Paramter missing, see usage..."
		    echo ""
            usage   # exits after printing out usage
        fi
	else
		echo "   *** Paramter missing, see usage..."
    	echo "" 
		usage   # exits after printing out usage
    fi

	if [ $3 ] && [ $3 = "--name" ]; then
		if [ $4 ] && [ $4 != "--range" ]; then
			objectName=$4
		else
			echo "   *** Paramter missing, see usage..."
            echo ""
            usage   # exits after printing out usage
        fi
	else
	    echo "   *** Paramter missing, see usage..."
	    echo ""
		usage	# exits after printing out usage
	fi

    if [ $5 ] && [ $5 = "--range" ]; then
        if [ $6 ] && [ $7 ]; then
            startRun=$6
			endRun=$7
			if [ $6 -lt 0 ]; then
				echo "   *** Invalid start of run number."
				echo ""
				exit 2
			fi
			if [ $7 -gt 0 ] && [ $6 -gt $7 ]; then
				echo "   *** Wrong order of Run range, see usage..."
				echo ""
				exit 2
			fi
        else
            echo "   *** Paramter missing, see usage..."
            echo ""
            usage   # exits after printing out usage
        fi
    else
        echo "   *** Paramter missing, see usage..."
        echo ""
        usage   # exits after printing out usage
    fi

	if [ $8 ] && [ $8 = "--detector" ]; then
		if [ $9 ] && [ $9 != "--permanent" ]; then
            detectorName=$9
        else
            echo "   *** Paramter missing, see usage..."
            echo ""
            usage   # exits after printing out usage
        fi
	else
        echo "   *** Paramter missing, see usage..."
        echo ""
        usage   # exits after printing out usage
    fi

	if [ ${10} ]; then
		if [ ${10} == "--permanent" ]; then
			permanent="yes"
		else
			echo "   *** Paramter missing, see usage..."
            echo ""
            usage   # exits after printing out usage
		fi
	fi


else
    echo "   *** Paramter missing, see usage..."
    echo ""
    usage   # exits after printing out usage
fi


userName=`whoami`


## compile path
if [ $permanent = "yes" ]; then
	fullPath=$HLTPermPath/$ConfigFolder$detectorName/$objectName
else
	fullPath=$HLTTempPath/$ConfigFolder$detectorName/$objectName
fi

## set HCDB path
if [ $ALIHLT_HCDBDIR ]; then
	HCDBBase=$ALIHLT_HCDBDIR	
else 
	echo "   *** Environment var 'ALIHLT_HCDBDIR' is not set, exiting..."
	echo ""
	exit 8
fi

## Print debug output
#debug
echo ""
echo " Running HCDBManager script $VersionNumber"
echo ""

## init envs for AliRoot
initAliRoot

if [ $method = "string" ]; then
	echo "   --- Inserting '$confString' as '$fullPath' in HCDB ($HCDBBase);"
	echo "   --- owner: $userName; valid ($startRun - $endRun)."
	echo "   --- Please be patient: starting of AliRoot can take some time!"
#	pwd ## Debug
	pushd `dirname $0`
#	pwd ## Debug
	aliroot -q -b -l "HCDBStringInsert.C(\"$confString\", \"$fullPath\", \"$userName\", $startRun, $endRun, \"$HCDBBase\")"
    retVal=$?
	popd
#	pwd ## Debug
	if [ $retVal -eq 10 ]; then
    	echo "   +++ Insertion successful."
		releaseHCDB
	else
    	echo "   *** Error occured during insertion ($retVal)."
		echo "   *** If library version does not match AliRoot version:"
		echo "       try to recompile lib (call make) and run srcipt again."
	fi

elif [ $method = "file" ]; then
	if [ -f $fileName ] && [ -r $fileName ]; then
		echo "   --- Inserting '$objectName' from '$fileName' in HCDB ($HCDBBase)"
		echo "   --- as $fullPath; owner: $userName; valid ($startRun - $endRun)."
		echo "   --- Please be patient: starting of AliRoot can take some time!"
		pushd `dirname $0`
		aliroot -q -b -l "HCDBFileInsert.C(\"$fileName\", \"$fullPath\", \"$objectName\", \"$userName\", $startRun, $endRun, \"$HCDBBase\")"
		retVal=$?
		popd
    	if [ $retVal -eq 10 ]; then
        	echo "   +++ Insertion successful."
			releaseHCDB
   		else
        	echo "   *** Error occured during insertion ($retVal)."
    	    echo "   *** If library version does not match AliRoot version:"
	        echo "       try to recompile lib (call make) and run srcipt again."

	    fi

	else
		echo "   *** File '$fileName' doesn't exist or not readable; exiting..."
		echo "" 
	fi
else
	echo "   *** Error in script: don't know which insert-method to choose."
	echo ""
fi


