#include "AliHLTHCDBConfigLogger.h"

#include <TTimeStamp.h>

#include <iostream>

using namespace std;

ClassImp(AliHLTHCDBConfigLogger)

AliHLTHCDBConfigLogger::AliHLTHCDBConfigLogger() {
	
}

AliHLTHCDBConfigLogger::~AliHLTHCDBConfigLogger() {

}


void AliHLTHCDBConfigLogger::Log(const TString& msg) {
	TTimeStamp time;

	cout << msg.Data() << " (" << time.AsString("s") << ")." << endl;

}

