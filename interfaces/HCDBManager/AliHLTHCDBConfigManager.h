#ifndef ALI_HLT_HCDB_CONFIG_MANAGER_H
#define ALI_HLT_HCDB_CONFIG_MANAGER_H

/************************************************************************
 * **
 * **
 * ** This file is property of and copyright by the Department of Physics
 * ** Institute for Physic and Technology, University of Bergen,
 * ** Bergen, Norway, 2008
 * ** This file has been written by Sebastian Bablok,
 * ** sebastian.bablok@ift.uib.no
 * **
 * ** Important: This file is provided without any warranty, including
 * ** fitness for any particular purpose.
 * **
 * **
 * *************************************************************************/

#include <TObject.h>
#include <TString.h>

#include "AliCDBRunRange.h"
#include "AliCDBMetaData.h"
#include "AliCDBPath.h"
#include "AliCDBEntry.h"
#include "AliCDBManager.h"
#include "AliCDBStorage.h"
#include "AliCDBId.h"

#include "AliHLTHCDBConfigLogger.h"

//class AliCDBRunRange;
//class AliCDBMetaData;
//class AliCDBPath;
//class AliCDBEntry;
//class AliCDBManager;
//class AliCDBStorage;
//class AliCDBId;


class AliHLTHCDBConfigManager {
	public:
		/**
		 * Static defining the path prefix for requests
		 */
		static const TString kLOCAL_STORAGE_DEFINE;
			
		/**
		 * Constructor for AliHLTHCDBConfigManager
		 *
		 * @param basePath the basePath to use
		 * @param logger the logger class pointer you want to register for logging
		 */
		AliHLTHCDBConfigManager(TString basePath, AliHLTHCDBConfigLogger* logger);

		/**
		 * Destructor for AliHLTHCDBConfigManager
		 */
		virtual ~AliHLTHCDBConfigManager();

		/**
		 * Function to set the base path for the HCDB
		 *
		 * @param basePath the basePath to use
		 *
		 * @return true if successful, else false
		 */
		Bool_t SetBasePath(const TString basePath);

		// --- *** Store functions *** --- //
		/**
		 * Function to store an TObject in the HCDB.
		 * In general a path looks like the following: "DET/SubPath2/SubPath3";
		 * where "DET" means the detector, "Subpath2" -> "Config" or "Calib" or
		 * "Align" and "SubPath3" is the actual name of the Configuration object.
		 *
		 * @param detector the detector for which this object is
		 * @param pathSub2 the sub path part 2 of the entry
		 * @param pathSub3 the sub path part 3 of the entry
		 * @param object the actual object to store to the HCDB
		 * @param startRun run number from which on this object shall be valid
		 * @param endRun run number, when validity of object shall end 
		 * 				(for infinity use AliCDBRunRange::Infinity())
		 * @param responsible responsible person for this object
		 * @param beamPeriod the beam Period (exact purpose I will check with 
		 * 				Offline) - default possible
		 * @param alirootVersion used AliRoot version - default possible
		 * @param comment comment describing the object - default possible
		 *
		 * @return 0 on success else an error code
		 */
		virtual UInt_t Store(const TString& detector, const TString& pathSub2, 
				const TString& pathSub3, TObject* object, Int_t startRun, 
				Int_t endRun, const char* responsible, UInt_t beamPeriod = 0, 
				const char* alirootVersion = "", const char* comment = "");

		/**
		 * Function to store an TObject in the HCDB.
		 * In general a path looks like the following: "DET/SubPath2/SubPath3";
		 * where "DET" means the detector, "Subpath2" -> "Config" or "Calib" or
		 * "Align" and "SubPath3" is the actual name of the Configuration object.
		 *
		 * @param detector the detector for which this object is
		 * @param pathSub2 the sub path part 2 of the entry
		 * @param pathSub3 the sub path part 3 of the entry
		 * @param object the actual object to store to the HCDB
		 * @param startRun run number from which on this object shall be valid
		 * @param endRun run number, when validity of object shall end 
		 * 				(for infinity use AliCDBRunRange::Infinity())
		 * @param metaData meta Data describing the object 
		 * 				(similar to function above)
		 *
		 * @return 0 on success else an error code
		 */		
		virtual UInt_t Store(const TString& detector, const TString& pathSub2, 
				const TString& pathSub3, TObject* object, Int_t startRun, 
				Int_t endRun, AliCDBMetaData* metaData);

		/**
		 * Function to store an TObject in the HCDB.
		 * In general a path looks like the following: "DET/SubPath2/SubPath3";
		 * where "DET" means the detector, "Subpath2" -> "Config" or "Calib" or
		 * "Align" and "SubPath3" is the actual name of the Configuration object.
		 *
		 * @param path the path under which the object shall be stored
		 * @param object the actual object to store to the HCDB
		 * @param startRun run number from which on this object shall be valid
		 * @param endRun run number, when validity of object shall end 
		 * 				(for infinity use AliCDBRunRange::Infinity())
		 * @param responsible responsible person for this object
		 * @param beamPeriod the beam Period - default possible
		 * @param alirootVersion used AliRoot version - default possible
		 * @param comment comment describing the object - default possible
		 *
		 * @return 0 on success else an error code
		 */
		virtual UInt_t Store(const AliCDBPath& path, TObject* object, 
				Int_t startRun, Int_t endRun, const char* responsible, 
				UInt_t beamPeriod = 0, const char* alirootVersion = "", 
				const char* comment = "");

		/**
		 * Function to store an TObject in the HCDB.
		 * In general a path looks like the following: "DET/SubPath2/SubPath3";
		 * where "DET" means the detector, "Subpath2" -> "Config" or "Calib" or
		 * "Align" and "SubPath3" is the actual name of the Configuration object.
		 *
		 * @param path the path under which the object shall be stored
		 * @param object the actual object to store to the HCDB
		 * @param startRun run number from which on this object shall be valid
		 * @param endRun run number, when validity of object shall end 
		 * 				(for infinity use AliCDBRunRange::Infinity())
		 * @param metaData meta Data describing the object 
		 * 				(similar to function above)
		 *
		 * @return 0 on success else an error code
		 */		      
		virtual UInt_t Store(const AliCDBPath& path, TObject* object, 
				Int_t startRun, Int_t endRun, AliCDBMetaData* metaData);

		/**
		 * Function to store an TObject in the HCDB.
		 * In general a path looks like the following: "DET/SubPath2/SubPath3";
		 * where "DET" means the detector, "Subpath2" -> "Config" or "Calib" or
		 * "Align" and "SubPath3" is the actual name of the Configuration object.
		 *
		 * @param path the path under which the object shall be stored
		 * @param object the actual object to store to the HCDB
		 * @param startRun run number from which on this object shall be valid
		 * @param endRun run number, when validity of object shall end 
		 * 				(for infinity use AliCDBRunRange::Infinity())
		 * @param metaData meta Data describing the object 
		 * 				(similar to function above)
		 *
		 * @return 0 on success else an error code
		 */		
		virtual UInt_t Store(const TString& path, TObject* object, 
				Int_t startRun, Int_t endRun, AliCDBMetaData* metaData);

		/**
		 * Function to store an TObject in the HCDB.
		 * In general a path looks like the following: "DET/SubPath2/SubPath3";
		 * where "DET" means the detector, "Subpath2" -> "Config" or "Calib" or
		 * "Align" and "SubPath3" is the actual name of the Configuration object.
		 *
		 * @param path the path under which the object shall be stored
		 * @param object the actual object to store to the HCDB
		 * @param runRange the run range for which the object shall be valid.
		 * @param metaData meta Data describing the object
		 *          	(similar to function above)
		 *          
		 * @return 0 on success else an error code
		 */
		virtual UInt_t Store(const AliCDBPath& path, TObject* object, 
				const AliCDBRunRange& range, AliCDBMetaData* meta);



		// --- *** Getter *** --- //
		/**
		 * Function to retrieve an entry from the HCDB.
		 * In general a path looks like the following: "DET/SubPath2/SubPath3";
		 * where "DET" means the detector, "Subpath2" -> "Config" or "Calib" or 
		 * "Align" and "SubPath3" is the actual name of the Configuration object.
		 *
		 * @param detector the name of the detector to which the object 
		 * 				(AliCDBEntry) belongs to.
		 * @param pathSub2 the sub path part 2 of the entry
		 * @param pathSub3 the sub path part 3 of the entry
		 * @param runNumber the run number for which the object (AliCDBEntry) 
		 * 				shall be valid.
		 * @param version the desired version number 
		 * 				(default is -1 => latest version)
		 * @param subVersion the desired sub version number 
		 * 				(default is -1 => the latest)
		 *
		 * @return pointer to the requested AliCDBEntry object or 
		 * 				NULL in case of failure.
		 */
		virtual AliCDBEntry* GetHCDBEntry(const TString& detector, 
				const TString& pathSub2, const TString& pathSub3, 
				Int_t runNumber, Int_t version = -1, Int_t subVersion = -1);

		/**
		 * Function to retrieve an entry from the HCDB.
		 *
		 * @param detector the name of the detector to which the object 
		 * 				(AliCDBEntry) belongs to.
		 * @param pathSub2 the sub path part 2 of the entry
		 * @param pathSub3 the sub path part 3 of the entry
		 * @param runRange the run range for which the object (AliCDBEntry) 
		 * 				shall be valid.
		 * @param version the desired version number 
		 * 				(default is -1 => latest version)
		 * @param subVersion the desired sub version number 
		 * 				(default is -1 => the latest)
		 *
		 * @return pointer to the requested AliCDBEntry object or 
		 * 				NULL in case of failure.
		 */		
		virtual AliCDBEntry* GetHCDBEntry(const TString& detector, 
				const TString& pathSub2, const TString& pathSub3, 
				const AliCDBRunRange& runRange, Int_t version = -1, 
				Int_t subVersion = -1);

		/**
		 * Function to retrieve an entry from the HCDB.
		 *
		 * @param detector the name of the detector to which the object 
		 * 				(AliCDBEntry) belongs to.
		 * @param path the path of the entry in HCDB
		 * @param runNumber the run number for which the object (AliCDBEntry) 
		 * 				shall be valid.
		 * @param version the desired version number 
		 * 				(default is -1 => latest version)
		 * @param subVersion the desired sub version number 
		 * 				(default is -1 => the latest)
		 *
		 * @return pointer to the requested AliCDBEntry object or 
		 * 				NULL in case of failure.
		 */
		virtual AliCDBEntry* GetHCDBEntry(const TString& detector, 
				const AliCDBPath& path, Int_t runNumber, Int_t version = -1, 
				Int_t subVersion = -1);

		/**
		 * Function to retrieve an entry from the HCDB.
		 *
		 * @param detector the name of the detector to which the object 
		 * 				(AliCDBEntry) belongs to.
		 * @param path the path of the entry in HCDB
		 * @param runRange the run range for which the object (AliCDBEntry) 
		 * 				shall be valid.
		 * @param version the desired version number 
		 * 				(default is -1 => latest version)
		 * @param subVersion the desired sub version number 
		 * 				(default is -1 => the latest)
		 *
		 * @return pointer to the requested AliCDBEntry object or 
		 * 				NULL in case of failure.
		 */		
		virtual AliCDBEntry* GetHCDBEntry(const TString& detector, 
				const AliCDBPath& path, const AliCDBRunRange& runRange,	
				Int_t version = -1, Int_t subVersion = -1);

		/**
		 * Function to retrieve an entry from the HCDB.
		 *
		 * @param query the AliCDBId object describing the object that shall 
		 * 				be queried.
		 *
		 * @return pointer to the requested AliCDBEntry object or 
		 * 				NULL in case of failure.
		 */		
		virtual AliCDBEntry* GetHCDBEntry(const AliCDBId& query);



		// --- *** More Getters (which might be usefull) *** --- //
		/**
		 * Funtion to request the latest version number of an HCDB entry.
		 *
		 * @param path the path of the entry to request version number
		 * @param run the run for which this object shall be valid.
		 *
		 * @return the latest version number; error indicated by <0
		 */
		virtual Int_t GetLatestVersion(const char* path, Int_t run);

		/**
		 * Funtion to request the latest sub version number of an HCDB entry.
		 *
		 * @param path the path of the entry to request version number
		 * @param run the run for which this object shall be valid.
		 * @param the version number of the requested object 
		 * 				(default -1 => latest)
		 *
		 * @return the latest sub version number; error indicated by <0
		 */
		virtual Int_t GetLatestSubVersion(const char* path, Int_t run, 
				Int_t version = -1);

		/**
		 * Function request the bas folder name which is used by the Manager
		 *
		 * @return the name of the used HCDB base folder.
		 */
		virtual const TString& GetBasePath();

		/**
		 * Function to check if storage is valid.
		 *
		 * @return true if valid else false
		 */
		Bool_t HasValidStorage();

		/**
		 * ???? Have to check -> this I have taken from AliCDBManger 
		 * can it be interessting for the GUI???
		 */
//		AliCDBManager::DataType GetDataType();

		/**
		 * ???? Have to check -> this I have taken from AliCDBManger 
		 * can it be interessting for the GUI???
		 */
//		TObjArray* GetQueryCDBList();

	protected:


	private:
		/**
		 * Disabled Standart C-tor
		 */
		AliHLTHCDBConfigManager();

		/** Stores pointer to registered logger */
		AliHLTHCDBConfigLogger* mrLogger;
		
		/** Stores the base path of the HCDB */
		TString mBasePath;

		/** Stores the AliCDBManager reference */
		AliCDBManager* mpManager;

		/** Stores the AliCDBStorage reference */
		AliCDBStorage* mpStore;

		/** Indicates if storage is valid */
		Bool_t mValidStore;

		ClassDef(AliHLTHCDBConfigManager, 1)
};


inline Bool_t AliHLTHCDBConfigManager::HasValidStorage() {
	return mValidStore;
}


#endif // ALI_HLT_HCDB_CONFIG_MANAGER_H


