#include "AliHLTHCDBConfigManager.h"

//#include <AliCDBId.h>
//#include <AliCDBRunRange.h>
//#include <AliCDBMetaData.h>
//#include <AliCDBPath.h>
//#include <AliCDBEntry.h>
//#include <AliCDBManager.h>
//#include <AliCDBStorage.h>


ClassImp(AliHLTHCDBConfigManager)


const TString AliHLTHCDBConfigManager::kLOCAL_STORAGE_DEFINE = "local://";

AliHLTHCDBConfigManager::AliHLTHCDBConfigManager(TString basePath, 
			AliHLTHCDBConfigLogger* logger) {
	mBasePath = kLOCAL_STORAGE_DEFINE + basePath;
	mrLogger = logger;
	mValidStore = kFALSE;
	mpManager = AliCDBManager::Instance();
	if (mpManager == 0) {
		mrLogger->Log("   *** Unable to get AliCDBManager instance.");
		// error cannot get storage	
		return;	
	}

	mpStore = mpManager->GetStorage(mBasePath.Data());
    if (mpStore == 0) {
        mrLogger->Log("   *** Provided bas path has no valid storage.");
        // error cannot get storage
        mValidStore = kFALSE;
    } else {
		mrLogger->Log("   --- HCDB base Path is set to " + basePath);
	    mValidStore = kTRUE;
	}
	
}

AliHLTHCDBConfigManager::~AliHLTHCDBConfigManager() {
	mpManager->DestroyActiveStorages();
}
		
UInt_t AliHLTHCDBConfigManager::Store(const TString& detector, 
			const TString& pathSub2, const TString& pathSub3, TObject* object,
			Int_t startRun, Int_t endRun, const char* responsible, 
			UInt_t beamPeriod, const char* alirootVersion, const char* comment) {
	AliCDBPath path(detector.Data(), pathSub2.Data(), pathSub3.Data());
	AliCDBRunRange range(startRun, endRun);
	AliCDBMetaData meta(responsible, beamPeriod, alirootVersion, comment);
	
	return Store(path, object, range, &meta);
}
	
UInt_t AliHLTHCDBConfigManager::Store(const TString& detector, 
			const TString& pathSub2, const TString& pathSub3, TObject* object, 
			Int_t startRun, Int_t endRun, AliCDBMetaData* metaData) {
	AliCDBPath path(detector.Data(), pathSub2.Data(), pathSub3.Data());
	AliCDBRunRange range(startRun, endRun);
		
	return Store(path, object, range, metaData);
}

UInt_t AliHLTHCDBConfigManager::Store(const AliCDBPath& path, TObject* object,
			Int_t startRun, Int_t endRun, const char* responsible, 
			UInt_t beamPeriod, const char* alirootVersion,
			const char* comment) {
	AliCDBRunRange range(startRun, endRun);
	AliCDBMetaData meta(responsible, beamPeriod, alirootVersion, comment);

	return Store(path, object, range, &meta);
}
		      
UInt_t AliHLTHCDBConfigManager::Store(const AliCDBPath& path, TObject* object,
			Int_t startRun, Int_t endRun, AliCDBMetaData* metaData) {
	AliCDBRunRange range(startRun, endRun);
		
	return Store(path, object, range, metaData);
}
		
UInt_t AliHLTHCDBConfigManager::Store(const TString& path, TObject* object, 
			Int_t startRun, Int_t endRun, AliCDBMetaData* metaData) {
	AliCDBPath aliPath(path);
	AliCDBRunRange range(startRun, endRun);

	return Store(aliPath, object, range, metaData);
}

// To this function all store funstions shall be referred
UInt_t AliHLTHCDBConfigManager::Store(const AliCDBPath& path, TObject* object, 
			const AliCDBRunRange& range, AliCDBMetaData* meta) {
	UInt_t retVal = 0;
	AliCDBId entryID(path, range);
	if (!mValidStore) {
		mrLogger->Log("   *** No valid storage provided, discarding call.");
		return 1;
	}
   	
	if (mpStore->Put(object, entryID, meta)) {
		mrLogger->Log("   +++ Storing of object successful.");
		Int_t ver = mpStore->GetLatestVersion(path.GetPath().Data(), 
				range.GetFirstRun());
		Int_t subver = mpStore->GetLatestSubVersion(path.GetPath().Data(), 
				range.GetFirstRun(), ver);
		TString msg("   --- VersionNumber for '");
		msg += path.GetPath();
		msg += "': ";
		msg += Form("%d.%d", ver, subver);
		mrLogger->Log(msg);
		retVal = 0;
	} else {
		 mrLogger->Log("   *** Error: storing of object failed.");
		retVal = 1;
	}

	return retVal;
}
		

//  ----------------- GETTER for AliCDBEntry  -------------//
AliCDBEntry* AliHLTHCDBConfigManager::GetHCDBEntry(const TString& detector, 
			const TString& pathSub2, const TString& pathSub3, Int_t runNumber,
			Int_t version, Int_t subVersion) {
	AliCDBPath path(detector.Data(), pathSub2.Data(), pathSub3.Data());
    AliCDBId id(path, runNumber, version, subVersion);

    return GetHCDBEntry(id);
}

	
AliCDBEntry* AliHLTHCDBConfigManager::GetHCDBEntry(const TString& detector, 
			const TString& pathSub2, const TString& pathSub3, 
			const AliCDBRunRange& runRange, Int_t version, Int_t subVersion) {
    AliCDBPath path(detector.Data(), pathSub2.Data(), pathSub3.Data());
	AliCDBId id(path, runRange, version, subVersion);

    return GetHCDBEntry(id);
}

AliCDBEntry* AliHLTHCDBConfigManager::GetHCDBEntry(const TString& detector, 
			const AliCDBPath& path, Int_t runNumber, Int_t version, 
			Int_t subVersion) {
    AliCDBId id(path, runNumber, version, subVersion);

    return GetHCDBEntry(id);	
}

AliCDBEntry* AliHLTHCDBConfigManager::GetHCDBEntry(const TString& detector, 
			const AliCDBPath& path, const AliCDBRunRange& runRange, 
			Int_t version, Int_t subVersion) {
	AliCDBId id(path, runRange, version, subVersion);
		
	return GetHCDBEntry(id);
}

// To this function all store funstions shall be referred
AliCDBEntry* AliHLTHCDBConfigManager::GetHCDBEntry(const AliCDBId& query) {	
	if (!mValidStore) {
		mrLogger->Log("   *** No valid storage provided, discarding call.");
		return 0;
	}

	mrLogger->Log("   +++ Requesting '" + query.GetPath() + "'.");
	return mpStore->Get(query);
}



// ------------------------ other Getter and Setter -------------//
Int_t AliHLTHCDBConfigManager::GetLatestVersion(const char* path, Int_t run) {
    if (!mValidStore) {
        mrLogger->Log("   *** No valid storage provided, discarding call.");
        return 0;
    }

    return mpStore->GetLatestVersion(path, run);
}


Int_t AliHLTHCDBConfigManager::GetLatestSubVersion(const char* path, Int_t run, 
			Int_t version) {
    if (!mValidStore) {
        mrLogger->Log("   *** No valid storage provided, discarding call.");
        return 0;
    }

    return mpStore->GetLatestSubVersion(path, run, version);
}

const TString& AliHLTHCDBConfigManager::GetBasePath() {
	return mBasePath;
}

Bool_t AliHLTHCDBConfigManager::SetBasePath(const TString basePath) {
	mBasePath = kLOCAL_STORAGE_DEFINE + basePath;

	if (mValidStore) {
		// clean up old storage
		mpManager->DestroyActiveStorages();
		mValidStore = kFALSE;
	}
	
	// get new one
	if (mpManager == 0) {
		mpManager = AliCDBManager::Instance();
		if (mpManager == 0) {
			mrLogger->Log("   *** Unable to get AliCDBManager Instance.");
			return kFALSE;
		}
	}
	mpStore = mpManager->GetStorage(mBasePath.Data());
	if (mpStore == 0) {
		mrLogger->Log("   *** Provided bas path has no valid storage.");
		// error cannot get storage
		mValidStore = kFALSE;
		return kFALSE;
	}
	mrLogger->Log("   --- HCDB base Path is set to " + basePath);
	mValidStore = kTRUE;
	return kTRUE;
}


//AliCDBManager::DataType AliHLTHCDBConfigManager::GetDataType() {
//
//}


