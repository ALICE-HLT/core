#ifndef ALI_HLT_HCDB_CONFIG_LOGGER_H
#define ALI_HLT_HCDB_CONFIG_LOGGER_H

/************************************************************************
 * **
 * **
 * ** This file is property of and copyright by the Department of Physics
 * ** Institute for Physic and Technology, University of Bergen,
 * ** Bergen, Norway, 2008
 * ** This file has been written by Sebastian Bablok,
 * ** sebastian.bablok@ift.uib.no
 * **
 * ** Important: This file is provided without any warranty, including
 * ** fitness for any particular purpose.
 * **
 * **
 * *************************************************************************/


#include <TString.h>

class AliHLTHCDBConfigLogger {
	public:
			
		/**
		 * Constructor for AliHLTHCDBConfigLogger
		 */
		AliHLTHCDBConfigLogger();

		/**
		 * Destructor for AliHLTHCDBConfigLogger
		 */
		virtual ~AliHLTHCDBConfigLogger();

		
		/**
		 * Function print out a log message (a very easy and simple form -
		 * - more advanced might follow when required). Components, that what 
		 * to request these messages, should inherit from this class, and 
		 * register there class as logger. Important is the implementation 
		 * of there own log(const TString& msg) function.
		 * This simple version only prints the message out to screen, a time
		 * stamp is added to the log message.
		 *
		 * @param msg the log message
		 */
		virtual void Log(const TString& msg);

	private:

		ClassDef(AliHLTHCDBConfigLogger, 1)
};



#endif // ALI_HLT_HCDB_CONFIG_LOGGER_H


