/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTRandomDataPublisher.cpp 2199 2007-08-17 10:42:24Z timm $ 
**
***************************************************************************
*/

#include "AliHLTRandomDataPublisher.hpp"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTEventDataType.h"
#include "AliHLTLog.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTBufferManager.hpp"
#include "AliHLTShmManager.hpp"
#include "AliHLTStackTrace.hpp"
#include "AliHLTSubEventDescriptor.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "AliHLTDDLHeaderData.h"
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <cstdlib>




AliHLTRandomDataPublisher::AliHLTRandomDataPublisher( const char* name, AliUInt32_t time_betw_evts_usec,
						      AliUInt32_t eventNrOffset, 
						      unsigned long minEvtSize, unsigned long maxEvtSize,
						      int eventSlotsPowerOfTwo ):
    AliHLTDetectorPublisher( name, eventSlotsPowerOfTwo ),
    fEventTime( fLocalEventTime ),
    fCreateEvents( fLocalCreateEvents ), fEventNrOffset( fLocalEventNrOffset ),
    fAddEventIDCounter( kAliEventTypeUnknown, (~(AliUInt64_t)0) << 36 ),
    fHLTEventTriggerDataCache( eventSlotsPowerOfTwo ),
    fHLTEventTriggerData( 8, eventSlotsPowerOfTwo ),
    fMinEventSize( minEvtSize ),
    fMaxEventSize( maxEvtSize )
    {
    fWaitSleepCount = 0;
    fQuitEventLoop = false;
    fEventLoopQuitted = false;
    fEventCounter=0;
    fCreateEvents = 1;
    fEventNrOffset = eventNrOffset;

    fOldestEventBirth = 0;

    fEventTime = time_betw_evts_usec;

    //     fETPOffset = GetFreeBlock( sizeof(AliHLTEventTriggerStruct), fETPIndex );
    //     if ( fETPOffset == (AliUInt32_t)~0 )
    // 	{
    fETP = &fETS;
    // 	}
    //     else
    // 	{
    // 	AliUInt8_t* ptr = (AliUInt8_t*)GetShmPtr(fETPIndex);
    // 	ptr += fETPOffset;
    // 	fETP = (AliHLTEventTriggerStruct*)ptr;
    // 	}
    fETP->fHeader.fLength = sizeof(AliHLTEventTriggerStruct);
    fETP->fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
    fETP->fHeader.fSubType.fID = 0;
    fETP->fHeader.fVersion = 1;
    fETP->fDataWordCount = 0;

    fLastEventStart.tv_sec = fLastEventStart.tv_usec = 0;

    fLastDoneEventID = ~(AliUInt64_t)0;
    gettimeofday( &fLastEventDoneTime, NULL );
    fPollSleepTime = 20000;
    fBusyPollTime = 1;
    fPollSleepModulo = 10;
//     fPollSleepTime = 10000;
//     fBusyPollTime = 5;
//     fPollSleepModulo = 1000;
    fGetBlockRetryCnt = 10000;
    fLastReleaseRequestTime.tv_sec = fLastReleaseRequestTime.tv_usec = 0;
    fReleaseRequestTimeDiff = 500000; // by default send at most 2 EventReleaseRequest message per second.
    fEventDataType.fID = UNKNOWN_DATAID;
    fEventDataOrigin.fID = UNKNOWN_DATAORIGIN;
    fEventDataSpecification = ~(AliUInt32_t)0;
    fEventTimeJustSleep = false;
    fLastAnnounceStart.tv_sec = fLastAnnounceStart.tv_usec = 0;
    fEventCounterSinceLastStart = 0;

    fSORSent = false;
    fEORSent0 = false;
    fEORSent1 = false;
    fSendEOR = false;

    fNoSOREOREvents = false;

    pthread_mutex_init( &fHLTEventTriggerDataLock, NULL );
    fDisableHLTEventTriggerData = false;
    srand( time(NULL) );
    }

AliHLTRandomDataPublisher::~AliHLTRandomDataPublisher()
    {
    pthread_mutex_destroy( &fHLTEventTriggerDataLock );
    }

void AliHLTRandomDataPublisher::StopPublishing()
    {
    QuitEventLoop();
    fEventAnnouncer.Quit();
    struct timespec ts;
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 50000000;
    gettimeofday( &start, NULL );
    while ( !QuittedEventLoop() && deltaT<timeLimit )
	{
	ts.tv_sec = 0;
	ts.tv_nsec = 500000;
	nanosleep( &ts, NULL );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    }


void AliHLTRandomDataPublisher::StartAnnouncing()
    {
    gettimeofday( &fLastAnnounceStart, NULL );
    fEventCounterSinceLastStart = 0;
    fSendEOR = false;
    fSORSent = false;
    fEORSent0 = false;
    fEORSent1 = false;
    AliHLTDetectorPublisher::StartAnnouncing();
    }

void AliHLTRandomDataPublisher::StopAnnouncing()
    {
    fSendEOR = true;
    unsigned cnt=0;
    const unsigned cntLimit = fEventTime/1000*10;
    while ( !fEORSent1 && cnt++<cntLimit )
	usleep( 0 );
    AliHLTDetectorPublisher::StopAnnouncing();
    }

void AliHLTRandomDataPublisher::PauseProcessing()
    {
    AliHLTDetectorPublisher::PauseProcessing();
    }

void AliHLTRandomDataPublisher::ResumeProcessing()
    {
    gettimeofday( &fLastAnnounceStart, NULL );
    fEventCounterSinceLastStart = 0;
    AliHLTDetectorPublisher::ResumeProcessing();
    }


void AliHLTRandomDataPublisher::EventFinished( AliEventID_t eventID, AliHLTSubEventDataDescriptor& sedd, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    if ( eventID.fType != kAliEventTypeStartOfRun && eventID.fType != kAliEventTypeStartOfRun )
	{
	AliUInt32_t i, ndx;
	if ( !fBufferManager )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRandomDataPublisherDisk::EventFinished", "No Buffer Manager" )
		<< "No buffer manager specified." << ENDLOG;
	    }
	if ( !fShmManager )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRandomDataPublisherDisk::EventFinished", "No Shm Manager" )
		<< "No shared memory manager specified." << ENDLOG;
	    }
	AliUInt32_t baseLimit=0;
	if ( sedd.fDataBlockCount>GetAdditionalBlockCount() )
	    baseLimit = sedd.fDataBlockCount-GetAdditionalBlockCount();
	for ( i = 0; i < sedd.fDataBlockCount; i++ )
	    {
	    if ( i<baseLimit || !ReleaseAdditionalBlock( sedd.fDataBlocks[i] ) )
		{
		if ( fBufferManager )
		    {
		    ndx = fBufferManager->GetBufferIndex( sedd.fDataBlocks[i].fShmID );
		    if ( ndx != ~(AliUInt32_t)0 )
			fBufferManager->ReleaseBlock( ndx, sedd.fDataBlocks[i].fBlockOffset );
		    }
		if ( fShmManager )
		    fShmManager->ReleaseShm( sedd.fDataBlocks[i].fShmID );
		}
	    }
	}

    if ( eventID.fType == kAliEventTypeStartOfRun )
	{
	unsigned long n = GetAdditionalBlockCount();
	if ( sedd.fDataBlockCount>=n )
	    {
	    for ( unsigned long ii=sedd.fDataBlockCount-n; ii<sedd.fDataBlockCount; ii++ )
		{
		ReleaseAdditionalBlock( sedd.fDataBlocks[ii] );
		}
	    }
	ReleaseSORBlocks();
	}
    else if ( eventID.fType == kAliEventTypeStartOfRun )
	{
	unsigned long n = GetAdditionalBlockCount();
	if ( sedd.fDataBlockCount>=n )
	    {
	    for ( unsigned long ii=sedd.fDataBlockCount-n; ii<sedd.fDataBlockCount; ii++ )
		{
		ReleaseAdditionalBlock( sedd.fDataBlocks[ii] );
		}
	    }
	ReleaseEORBlocks();
	}
    EventFinished( eventID, eventDoneData );
    }

void AliHLTRandomDataPublisher::EventFinished( AliEventID_t eventID, vector<AliHLTEventDoneData*>& )
    {
    ; // Non conditional block following...
	{
	struct timeval ed;
	gettimeofday( &ed, NULL );
	//vector<EventUsageData>::iterator iter, end;
	fLastDoneEventID = eventID;

	}

    pthread_mutex_lock( &fHLTEventTriggerDataLock );
    unsigned long ndx;
    if ( (fAliceHLT && !fDisableHLTEventTriggerData) || fHLTEventTriggerData.GetCnt()>0 ) // fHLTEventTriggerData.GetCnt()>0 means there are events in there, maybe because the trigger data was switched off after some time of operation.
	{
	if ( fHLTEventTriggerData.FindElement( eventID, ndx ) )
	    {
	    fHLTEventTriggerDataCache.Release( fHLTEventTriggerData.Get(ndx) );
	    fHLTEventTriggerData.Remove( ndx );
	    }
	else if ( eventID.fType != kAliEventTypeTick )
	    {
	    AliHLTLog::TLogLevel lvl;
	    if ( !fDisableHLTEventTriggerData )
		lvl = AliHLTLog::kError;
	    else
		lvl = AliHLTLog::kWarning;
	    LOG( lvl, "AliHLTRandomDataPublisher::EventFinished", "Cannot find HLTEventTriggerData" )
		<< "Unable to find HLT event trigger data object for event 0x" 
		<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ")." << ENDLOG;
	    }
	}
    pthread_mutex_unlock( &fHLTEventTriggerDataLock );
    }



int AliHLTRandomDataPublisher::GetBroadcastEvent( bool SOR, AliEventID_t& eventID, 
					     AliHLTSubEventDataDescriptor*& sbevent,
					     AliHLTEventTriggerStruct*& trg )
    {
    vector<AliHLTSubEventDataBlockDescriptor> blocks;
    if ( SOR )
	{
	CreateSORBlocks( blocks );
	eventID.fType = kAliEventTypeStartOfRun;
	}
    else
	{
	CreateEORBlocks( blocks );
	eventID.fType = kAliEventTypeEndOfRun;
	}
    eventID.fNr = 0;
    pthread_mutex_lock( &fSEDDMutex );
    sbevent = fDescriptors->GetFreeEventDescriptor( eventID, blocks.size()+GetAdditionalBlockCount() );
    pthread_mutex_unlock( &fSEDDMutex );
    if ( sbevent )
	{
	struct timeval now;
	gettimeofday( &now, NULL );
	sbevent->fEventID = eventID;
	sbevent->fStatusFlags |= ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST;
	sbevent->fEventBirth_s = now.tv_sec;
	sbevent->fEventBirth_us = now.tv_usec;
	sbevent->fOldestEventBirth_s = fOldestEventBirth;
	sbevent->fDataType.fID = COMPOSITE_DATAID;
	unsigned addBlockOut = 0;
	for ( addBlockOut = 0; addBlockOut < blocks.size(); addBlockOut++ )
	    sbevent->fDataBlocks[addBlockOut] = blocks[addBlockOut];
	
	for ( unsigned addBlock=0; addBlock<GetAdditionalBlockCount(); addBlock++ )
	    {
	    if ( GetAdditionalBlock( addBlock, sbevent->fDataBlocks[addBlockOut] ) )
		++addBlockOut;
	    }

	if ( fAliceHLT )
	    GetHLTTriggerData( eventID, trg, NULL );
	else
	    trg = fETP;
	

	return 1;
	}
    return 0;
    }



void AliHLTRandomDataPublisher::QuitEventLoop()
    {
    fQuitEventLoop = true;
    }

void AliHLTRandomDataPublisher::StartEventLoop()
    {
    fEventLoopQuitted = fQuitEventLoop = false;
    if ( fPersistentState )
	fEventCounter = fPersistentState->fLastEventID.fNr+(unsigned)1;
    }

void AliHLTRandomDataPublisher::EndEventLoop()
    {
    }


int AliHLTRandomDataPublisher::GetHLTTriggerData( AliEventID_t eventID, AliHLTEventTriggerStruct*& trg, void const* eventData )
    {
    pthread_mutex_lock( &fHLTEventTriggerDataLock );
    AliHLTHLTEventTriggerData* hltTrg = fHLTEventTriggerDataCache.Get();
    if ( !hltTrg )
	{
	LOG( AliHLTLog::kError, "AliHLTRandomDataPublisher::GetHLTTriggerData", "out of memory (HLTEventTriggerData)" )
	    << "Out of memory trying to allocate HLT event trigger data object - using empty trigger data." << ENDLOG;
	trg = NULL;
	pthread_mutex_unlock( &fHLTEventTriggerDataLock );
	return ENOMEM;
	}
    else
	{
	fHLTEventTriggerData.Add( hltTrg, eventID );
	if ( eventData )
	    memcpy( hltTrg->fCommonHeader, (AliUInt8_t const*)( eventData ), sizeof(hltTrg->fCommonHeader[0])*hltTrg->fCommonHeaderWordCnt );
	else
	    {
	    memset( hltTrg->fCommonHeader, 0, sizeof(hltTrg->fCommonHeader[0])*hltTrg->fCommonHeaderWordCnt );
	    AliHLTSetDDLHeaderEventID( hltTrg->fCommonHeader, eventID );
	    }
	FillHLTEventTriggerDataDDLList( hltTrg );
	trg = &(hltTrg->fETS);
	}
    pthread_mutex_unlock( &fHLTEventTriggerDataLock );
    return 0;
    }



int AliHLTRandomDataPublisher::WaitForEvent( AliEventID_t& eventID, AliHLTSubEventDataDescriptor*& sbevent,
					   AliHLTEventTriggerStruct*& trg )
    {
    Tick( fEventCounter );
    sbevent = NULL;
    trg = NULL;
    bool blockOk = false;
    unsigned long offset=0, size=0;
    //fQuitEventLoop = false;
    //fEventLoopQuitted = false;
    AliUInt8_t* ptr = NULL;
    struct timeval now;
    //#define DO_BENCHMARK
#if __GNUC__>=3
    AliHLTShmID shmKey = { kInvalidShmType, {~(AliUInt64_t)0 } };
#else
    AliHLTShmID shmKey = { kInvalidShmType, {{~(AliUInt64_t)0 }} };
#endif


    if ( !fBufferManager )
	{
	LOG( AliHLTLog::kError, "AliHLTRandomDataPublisher::WaitForEvent", "No Buffer Manager" )
	    << "No buffer manager specified. Aborting..." << ENDLOG;
	return 0;
	}
    if ( !fShmManager )
	{
	LOG( AliHLTLog::kError, "AliHLTRandomDataPublisher::WaitForEvent", "No Shm Manager" )
	    << "No shared memory manager specified. Aborting..." << ENDLOG;
	// ...
	return 0;
	}
    if ( !fDescriptors )
	{
	LOG( AliHLTLog::kError, "AliHLTRandomDataPublisher::WaitForEvent", "No Descriptor Handler" )
	    << "No descriptor handler specified. Aborting..." << ENDLOG;
	return 0;
	}

    if ( !fQuitEventLoop )
	{
	while ( !fCreateEvents && !fQuitEventLoop )
	    {
	    usleep( 10000 );
	    //usleep( 0 );
	    }
	AliUInt32_t tmpCount=0;
	unsigned long long avg = 0xFFFFFFFFFFFFFFFFULL;
	do
	    {
	    unsigned long long tdiff;
	    gettimeofday( &now, NULL );
	    tdiff  = now.tv_sec-fLastAnnounceStart.tv_sec;
	    tdiff *= 1000000;
	    tdiff += now.tv_usec-fLastAnnounceStart.tv_usec;
	    if ( fEventCounterSinceLastStart!=0 )
		avg = tdiff / fEventCounterSinceLastStart;
	    // avg is average time between events since last start/resume
	    if ( avg < fEventTime && !fQuitEventLoop && ( fNoSOREOREvents || (fSORSent && !fSendEOR) ) )
		usleep( 0 );
	    }
	while ( avg < fEventTime && !fQuitEventLoop && ( fNoSOREOREvents || (fSORSent && !fSendEOR) ) );
	    
	if ( !fNoSOREOREvents )
	    {

	    if ( !fSORSent)
		{
		fSORSent = true;
		return GetBroadcastEvent( true, eventID, 
					  sbevent,
					  trg );
		}
	    if ( fSendEOR )
		{
		if ( !fEORSent0 )
		    {
		    fEORSent0 = true;
		    return GetBroadcastEvent( false, eventID, 
					      sbevent,
					      trg );
		    }
		// Two way confirmation necessary, because EOR has to be sent completely before quitting event loop.
		fEORSent1 = true;
		sbevent = NULL;
		trg = NULL;
		return 1;
		}
	    }

	tmpCount = 0;
	struct timespec ts;
	AliUInt32_t bufferNdx;
	unsigned long evtSize;
	do
	    {
	    if ( fQuitEventLoop )
 		{
		offset = (AliUInt32_t)~0;
		break;
		}
	    evtSize = fMinEventSize + (unsigned long)( ( ( ((double)rand())/((double)RAND_MAX) )) * (double)(fMaxEventSize-fMinEventSize) );
	    evtSize = (evtSize/4)*4;
	    size  = evtSize;
	    blockOk = fBufferManager->GetBlock( bufferNdx, offset, size );

	    if ( !blockOk )
		{
		UpdateEventAge();
		if ( !(tmpCount % fPollSleepModulo) )
		    {
		    //ts.tv_sec = ts.tv_nsec = 0;
		    //ts.tv_nsec = 1000000;
		    ts.tv_sec = fPollSleepTime/1000000;
		    ts.tv_nsec = (fPollSleepTime%1000000)*1000;
		    nanosleep( &ts, NULL );
		    UpdateEventAge();
		    fWaitSleepCount++;
		    }
		else
		    {
		    struct timeval tts, tte;
		    AliUInt32_t dtt;
		    gettimeofday( &tts, NULL );
		    do
			{
			gettimeofday( &tte, NULL );
			dtt = (tte.tv_sec-tts.tv_sec)*1000000+(tte.tv_usec-tts.tv_usec);
			}
		    while ( dtt < fBusyPollTime );
		    }
		}
	    }
	while ( !blockOk && tmpCount++<fGetBlockRetryCnt );
	if ( fQuitEventLoop )
	    {
	    fEventLoopQuitted = true;
	    return 0;
	    }
	
	if ( tmpCount > 1 )
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTRandomDataPublisher::WaitForEvent", "Wait states before event" )
		<< "Needed " << AliHLTLog::kDec << tmpCount << " waitstates before starting event... "
		<< ENDLOG;
	    }
	gettimeofday( &fLastEventStart, NULL );

	if ( blockOk )
	    shmKey = fBufferManager->GetShmKey( bufferNdx );
	else
	    {
	    shmKey.fShmType = kInvalidShmType;
	    shmKey.fKey.fID = ~(AliUInt64_t)0;
	    }
	if ( shmKey.fShmType != kInvalidShmType )
	    {
	    if ( fStatus )
		{
		AliUInt64_t tm;
		tm = fLastEventStart.tv_sec;
		tm *= 1000000;
		tm += fLastEventStart.tv_usec;
		pthread_mutex_lock( &fStatusMutex );
		fStatus->fHLTStatus.fCurrentEventStart = tm;
		pthread_mutex_unlock( &fStatusMutex );
		}
		
	    ptr = fShmManager->GetShm( shmKey );
	    if ( ptr )
		ptr += offset;
	    }
	else
	    ptr = NULL;
	unsigned long sz = 0;
	if ( ptr )
	    {
	    unsigned long wordCnt = evtSize/sizeof(AliUInt32_t);
	    while ( sz < wordCnt*sizeof(AliUInt32_t) )
		{
		*((AliUInt32_t*)(ptr+sz)) = rand();
		sz += sizeof(AliUInt32_t);
		}
	    while ( sz < evtSize )
		{
		*((AliUInt8_t*)(ptr+sz)) = (AliUInt8_t)rand();
		sz += sizeof(AliUInt8_t);
		}

	    eventID.fType = kAliEventTypeData;
	    eventID.fNr = fEventCounter++;
	    eventID.fNr += fEventNrOffset;
	    fEventCounterSinceLastStart++;
	    
	    pthread_mutex_lock( &fSEDDMutex );
	    sbevent = fDescriptors->GetFreeEventDescriptor( eventID, 1+GetAdditionalBlockCount() );
	    pthread_mutex_unlock( &fSEDDMutex );

	    if ( !sbevent )
		{
		fEventCounter--;
		fEventCounterSinceLastStart--;
		}
	    }

	if ( sbevent )
	    {
	    



	    gettimeofday( &now, NULL );
	    sbevent->fEventID = eventID;
	    sbevent->fEventBirth_s = now.tv_sec;
	    sbevent->fEventBirth_us = now.tv_usec;
	    sbevent->fOldestEventBirth_s = fOldestEventBirth;
	    sbevent->fDataBlocks[0].fBlockOffset = offset;
	    sbevent->fDataBlocks[0].fBlockSize = sz;
	    sbevent->fDataBlocks[0].fShmID.fShmType = shmKey.fShmType;
	    sbevent->fDataBlocks[0].fShmID.fKey.fID = shmKey.fKey.fID;
	    sbevent->fDataBlocks[0].fDataType.fID = fEventDataType.fID; //ADCCOUNTS_DATAID;
	    sbevent->fDataBlocks[0].fDataOrigin.fID = fEventDataOrigin.fID;
	    sbevent->fDataBlocks[0].fDataSpecification = fEventDataSpecification;
	    sbevent->fDataBlocks[0].fStatusFlags = 0;
	    AliHLTSubEventDescriptor::FillBlockAttributes( sbevent->fDataBlocks[0].fAttributes );
	    

	    sbevent->fDataBlocks[0].fProducerNode = fNodeID;


	    for ( unsigned addBlock=0, addBlockOut=1; addBlock<GetAdditionalBlockCount(); addBlock++ )
		{
		if ( GetAdditionalBlock( addBlock, sbevent->fDataBlocks[addBlockOut] ) )
		    ++addBlockOut;
		}

	    sbevent->fDataType.fID = COMPOSITE_DATAID;

	    if ( fAliceHLT && !fDisableHLTEventTriggerData )
		GetHLTTriggerData( eventID, trg, ptr );
	    else
		trg = fETP;





	    LOG( AliHLTLog::kDebug, "AliHLTRandomDataPublisher", "WaitForEvent" )
		<< "Created " << AliHLTLog::kDec << evtSize << " random bytes." << ENDLOG;
	    }
	else // of if ( offset == ~0 )
	    {
	    if ( !fQuitEventLoop )
		{
		unsigned long long tdiff;
		LOG( AliHLTLog::kInformational, "AliHLTRandomDataPublisher", "Getting shared memory block" )
		    << "Unable to get shared memory block of size " << AliHLTLog::kDec << size << " bytes " 
		    << evtSize << "."
		    << ENDLOG;
		gettimeofday( &now, NULL );
		tdiff = (now.tv_sec-fLastReleaseRequestTime.tv_sec)*1000000+(now.tv_usec-fLastReleaseRequestTime.tv_usec);
		if ( tdiff > fReleaseRequestTimeDiff )
		    {
		    RequestEventRelease();
		    fLastReleaseRequestTime = now;
		    }
		}
	    //PingActive();
	    }
	return 1;
	}
    else
	{
	fEventLoopQuitted = true;
	return 0;
	}
    }







/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTRandomDataPublisher.cpp 2199 2007-08-17 10:42:24Z timm $ 
**
***************************************************************************
*/
