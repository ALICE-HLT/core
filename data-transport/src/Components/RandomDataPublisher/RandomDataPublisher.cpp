/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: RandomDataPublisher.cpp 1867 2007-04-05 12:49:10Z timm $ 
**
***************************************************************************
*/

#include "AliHLTRandomDataPublisher.hpp"
//#include "AliHLTControlledProcessComponent.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTLog.hpp"
#include <vector>
#include <glob.h>

class RandomDataPublisherComponent: public AliHLTControlledSourceComponent
    {
    public:

	RandomDataPublisherComponent( const char* name, int argc, char** argv );
	virtual ~RandomDataPublisherComponent() {};


    protected:

	virtual AliHLTDetectorPublisher* CreatePublisher();
	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

	virtual void ShowConfiguration();

	virtual bool SetupComponents();
	virtual void DestroyParts();

	unsigned long fEventTime;
	unsigned long fEventNrOffset;

	bool fEventTimeJustSleep;

	bool fNoSOREOREvents;

	unsigned long fMinEventSize;
	unsigned long fMaxEventSize;

    private:
    };


RandomDataPublisherComponent::RandomDataPublisherComponent( const char* name, int argc, char** argv ):
    AliHLTControlledSourceComponent( name, argc, argv )
    {
    fAllowDataTypeOriginSpecSet = true;
    fEventTime = 0; // By default publish as fast as possible
    fEventNrOffset = 0; // By default start at the beginning
    fEventTimeJustSleep = false;
    fNoSOREOREvents = false;
    fMinEventSize = 1024;
    fMaxEventSize = 4*1024*1024;
    }


AliHLTDetectorPublisher* RandomDataPublisherComponent::CreatePublisher()
    {
    return new AliHLTRandomDataPublisher( fOwnPublisherName.c_str(), fEventTime,
					  fEventNrOffset, fMinEventSize, fMaxEventSize, fMaxPreEventCountExp2 );
    }


const char* RandomDataPublisherComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cerr = NULL;
    if ( !strcmp( argv[i], "-eventtime" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing event interval time specifier.";
	    }
	fEventTime = strtoul( argv[i+1], &cerr, 0 );
	if ( *cerr )
	    {
	    return "Error converting event interval time specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventnr" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing event nr offset specifier.";
	    }
	fEventNrOffset = strtoul( argv[i+1], &cerr, 0 );
	if ( *cerr )
	    {
	    return "Error converting event nr offset specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-mineventsize" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing minimum event size specifier.";
	    }
	fMinEventSize = strtoul( argv[i+1], &cerr, 0 );
	if ( *cerr )
	    {
	    return "Error converting minimum event size specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-maxeventsize" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing maximum event size specifier.";
	    }
	fMaxEventSize = strtoul( argv[i+1], &cerr, 0 );
	if ( *cerr )
	    {
	    return "Error converting maximum event size specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-sleeptillevent" ) )
	{
	fEventTimeJustSleep = true;
	i += 1;
	return NULL;
	}
    if ( !strcmp( argv[i], "-nosoreorevents" ) )
	{
	fNoSOREOREvents = true;
	i += 1;
	return NULL;
	}
    return AliHLTControlledSourceComponent::ParseCmdLine( argc, argv, i, errorArg );
    }

void RandomDataPublisherComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    AliHLTControlledSourceComponent::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-eventtime <interval-microsec.>: Specify the time between the publishing of two events (in microseconds). (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-eventnr <event-nr-offset.>: Specify an offset to the starting event number/ID. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-nosoreorevents: Inhibit the creation of start-of-run/end-of-run broadcast events. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-mineventsize <minimum-event-size.>: Specify a minimum size for events. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-maxeventsize <maximum-event-size.>: Specify a maximum size for events. (Optional)" << ENDLOG;
    }

void RandomDataPublisherComponent::ShowConfiguration()
    {
    AliHLTControlledComponent::ShowConfiguration();
    LOG( AliHLTLog::kInformational, "RandomDataPublisherComponent::ShowConfiguration", "Event Time Configuration" )
	<< "Event time: " << AliHLTLog::kDec << fEventTime << " (0x" << AliHLTLog::kHex
	<< fEventTime << ")." << ENDLOG;
    LOG( AliHLTLog::kInformational, "RandomDataPublisherComponent::ShowConfiguration", "Event Size Configuration" )
	<< "Minimum event size: " << AliHLTLog::kDec << fMinEventSize << " (0x" << AliHLTLog::kHex
	<< fMinEventSize << ") - Maximum event size: " << AliHLTLog::kDec << fMaxEventSize << " (0x" << AliHLTLog::kHex
	<< fMaxEventSize << ")." << ENDLOG;
    }

bool RandomDataPublisherComponent::SetupComponents()
    {
    if ( !AliHLTControlledComponent::SetupComponents() )
	return false;
    ((AliHLTRandomDataPublisher*)fPublisher)->SetEventDataType( fEventDataType );
    ((AliHLTRandomDataPublisher*)fPublisher)->SetEventDataOrigin( fEventDataOrigin );
    ((AliHLTRandomDataPublisher*)fPublisher)->SetEventDataSpecification( fEventDataSpecification );
    if ( fEventTimeJustSleep )
	((AliHLTRandomDataPublisher*)fPublisher)->EventTimeJustSleep();
    if ( fNoSOREOREvents )
	((AliHLTRandomDataPublisher*)fPublisher)->NoSOREOREvents();
    return true;
    }

void RandomDataPublisherComponent::DestroyParts()
    {
#ifdef PROFILING
    if ( fPublisher )
	((AliHLTRandomDataPublisher*)fPublisher)->WriteHistograms( fComponentName );
#endif
    AliHLTControlledComponent::DestroyParts();
    }



int main( int argc, char** argv )
    {
    RandomDataPublisherComponent procComp( "RandomDataPublisher", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }




/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: RandomDataPublisher.cpp 1867 2007-04-05 12:49:10Z timm $ 
**
***************************************************************************
*/
