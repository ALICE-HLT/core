/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3RORCPUBLISHER_HPP_
#define _ALIL3RORCPUBLISHER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#if 0
#define OLD_EVENTTRIGGER_DATA
#endif

#include "AliHLTDetectorPublisher.hpp"
#include "AliHLTCRORCHandlerInterface.hpp"
#include "AliHLTCRORC1Handler.hpp"
#ifndef OLD_EVENTTRIGGER_DATA
#include "AliHLTHLTEventTriggerData.hpp"
#endif
#include "MLUCHistogram.hpp"
#include "MLUCVector.hpp"
#include <MLUCObjectCache.hpp>
#include <MLUCIndexedVector.hpp>
#include <pthread.h>
#include <vector>


class AliHLTCRORCPublisher: public AliHLTDetectorPublisher
    {
    public:

	AliHLTCRORCPublisher( const char* name, int eventSlotsPowerOfTwo = -1 );
	virtual ~AliHLTCRORCPublisher();

	void StopPublishing();

	virtual bool MustContinue();

	void SetRORCHandler( AliHLTCRORCHandlerInterface* rorcHandler )
		{
		fRORCHandler = rorcHandler;
		}

	void SetSleep( bool do_sleep )
		{
		fDoSleep = do_sleep;
		}
	void SetSleepTime( unsigned long sleepTime )
		{
		fSleepTime = sleepTime;
		}

	void SetShm( AliHLTShmID shmKey, unsigned long offset, volatile AliUInt8_t* ptr, AliUInt32_t size )
		{
		fShmKey = shmKey;
		fShmOffset = offset;
		fShmPtr = ptr;
		fShmSize = size;
		}

	void SetEventDataType( AliHLTEventDataType datatype, AliHLTEventDataType splitDatatype )
		{
		fEventDataType.fID = datatype.fID;
		fSplitEventDataType.fID = splitDatatype.fID;
		}

	void SetEventDataOrigin( AliHLTEventDataOrigin dataorigin )
		{
		fEventDataOrigin.fID = dataorigin.fID;
		}

	void SetEventDataSpecification( AliUInt32_t dataspec )
		{
		fEventDataSpecification = dataspec;
		}

	void PublishCorruptEvents( bool pce )
		{
		fPublishCorruptEvents = pce;
		}
	void SuppressHWSuppressedEvents( bool pse )
		{
		fSuppressHWSuppressedEvents = pse;
		}
	void SuppressGoodEvents( bool sge )
		{
		fSuppressGoodEvents = sge;
		}
        void TruncateCorruptEvents( bool truncate )
                {
                fTruncateCorruptEvents = truncate;
                }

	void SetEnableCorruptEventsAutoDump( bool enable )
		{
		fEnableCorruptEventsAutoDump = enable;
		}
	void SetCorruptEventsAutoDumpLimit( unsigned long limit )
		{
		fCorruptEventsAutoDumpLimit = limit;
		}
	void SetCorruptEventsAutoDumpPrefix( const char* prefix )
		{
		fCorruptEventsAutoDumpPrefix = prefix;
		}

	void NoStrictSOR()
		{
		fNoStrictSOR = true;
		}
	void StrictSOR()
		{
		fNoStrictSOR = false;
		}


#ifdef PROFILING
	void WriteHistograms( const char* filenameprefix );
#endif





    protected:

	virtual int WaitForEvent( AliEventID_t& eventID, AliHLTSubEventDataDescriptor*& sbevent,
				  AliHLTEventTriggerStruct*& trg ); // Called with no locked mutexes
	virtual void EventFinished( AliEventID_t, AliHLTSubEventDataDescriptor& sbevent, vector<AliHLTEventDoneData*>& eventDoneData ); // Called with no locked mutexes
	virtual void EventFinished( AliEventID_t, vector<AliHLTEventDoneData*>& eventDoneData ); // Called with no locked mutexes
	virtual void QuitEventLoop(); // Called with no locked mutexes
	virtual void StartEventLoop();
	virtual void EndEventLoop();

	virtual void PauseProcessing();
	virtual void ResumeProcessing();

        virtual void EventRateTimerExpired();

        void DumpEvent(AliHLTSubEventDataDescriptor *&sbevent,
                       AliEventID_t eventID, AliUInt64_t dataOffset,
                       AliUInt64_t dataSize);

        bool fQuitEventLoop;

	bool fDoSleep;
	unsigned long fSleepTime;

	AliUInt32_t fOldestEventBirth;

	AliHLTCRORCHandlerInterface* fRORCHandler;
	pthread_mutex_t fRORCHandlerMutex;

	AliHLTShmID fShmKey;
	unsigned long fShmOffset;
	volatile AliUInt8_t* fShmPtr;
	AliUInt32_t fShmSize;

	AliHLTEventTriggerStruct fETS;
	AliHLTEventTriggerStruct* fETP;
	AliUInt32_t fETPOffset;
	AliUInt32_t fETPIndex;

	AliHLTEventDataType fEventDataType;
	AliHLTEventDataType fSplitEventDataType;
	AliHLTEventDataOrigin fEventDataOrigin;
	AliUInt32_t fEventDataSpecification;

	struct timeval fLastNoEventFoundLog;

#ifndef OLD_EVENTTRIGGER_DATA
	MLUCObjectCache<AliHLTHLTEventTriggerData> fHLTEventTriggerDataCache;
	MLUCIndexedVector<AliHLTHLTEventTriggerData*,AliEventID_t> fHLTEventTriggerData;
	pthread_mutex_t fHLTEventTriggerDataLock;
	bool fDisableHLTEventTriggerData;
#endif

        struct timeval fLastTimerExpire;
        AliUInt32_t fLastEventCount;

#ifdef PROFILING
	MLUCHistogram fAnnounceEventTimeSetupHisto;
	MLUCHistogram fAnnounceEventTimeFindSubscribersHisto;
	MLUCHistogram fAnnounceEventTimeStoreEventHisto;
	MLUCHistogram fAnnounceEventTimeFindEventSlotHisto;
	MLUCHistogram fAnnounceEventTimeAnnounceEventHisto;
	MLUCHistogram fAnnounceEventTimeCallbacksHisto;
	MLUCHistogram fAnnounceEventTimeSetTimerHisto;
	MLUCHistogram fAnnounceEventTimeFindTimerHisto;
#endif

	bool fPublishCorruptEvents;
	bool fSuppressGoodEvents;
	bool fSuppressHWSuppressedEvents; // events suppressed in hardware
	bool fTruncateCorruptEvents;

	bool fReceivedEvents;

	bool fStartOfRunEventReceived;
	bool fEndOfRunEventReceived;
	bool fDataEventWithoutStartOfRunWarning;

	bool fEnableCorruptEventsAutoDump;
	unsigned long fCorruptEventsAutoDumpLimit;
	unsigned long fCorruptEventsAutoDumpCnt;
	MLUCString fCorruptEventsAutoDumpPrefix;

        AliUInt32_t fLastStatusXoffCount;
        AliUInt64_t fTotalDdlXoffCount;
        AliUInt64_t fLastReportedXoffCount;
        struct timeval fLastReportedXoffTime;
        unsigned long  fXoffActiveCount;
        struct timeval fXoffActiveStarttime;
        AliUInt64_t fXoffActiveStartvalue;
        bool fEorXoffStats;

        unsigned long fLastFcfRcuProtocolErrorCnt;
        unsigned long fLastFcfAltroChannelErrorCnt;
        AliUInt64_t fTotalNopeCount;
        struct timeval fLastFcfErrorLog;


	bool fNoStrictSOR;

    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3RORCPUBLISHER_HPP_
