/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTDummyCRORCHandler.hpp"
#include "AliHLTLog.hpp"
#include <errno.h>

AliHLTDummyCRORCHandler::AliHLTDummyCRORCHandler( unsigned long evtBufferSize, int eventSlotsExp2 ):
    fEvents( eventSlotsExp2 )
    {
    fMaxEventCount = evtBufferSize/128;
    if ( eventSlotsExp2>=0 && fMaxEventCount > (unsigned long)(1<<eventSlotsExp2) )
	fMaxEventCount = (1<<eventSlotsExp2);
    fEventCount = 0;
    fEventCounter = 0;
    fEvent = AliEventID_t();
    fEventBufferSize = evtBufferSize;
    if ( fEventBufferSize % 128 )
	fEventBufferSize -= (fEventBufferSize % 128);
    fActive = false;
    fOffset = 0;
    pthread_mutex_init( &fEventMutex, NULL );
    fBlockSize = 1052;
    fHeaderSearch = true;
    }

AliHLTDummyCRORCHandler::~AliHLTDummyCRORCHandler()
    {
    pthread_mutex_destroy( &fEventMutex );
    }


int AliHLTDummyCRORCHandler::Open( uint8_t, uint32_t, AliHLTRORCDataDirection, AliHLTRORCDataSource )
    {
    return 0;
    }

int AliHLTDummyCRORCHandler::Close()
    {
    return 0;
    }


int AliHLTDummyCRORCHandler::SetEventBuffer( AliHLTShmID shmKey )
    {
    return 0;
    }


int AliHLTDummyCRORCHandler::InitializeRORC()
    {
    fEventCount = 0;
    fEventCounter = 0;
    return 0;
    }

int AliHLTDummyCRORCHandler::DeinitializeRORC()
    {
    return 0;
    }


int AliHLTDummyCRORCHandler::InitializeFromRORC( bool )
    {
    return 0;
    }


int AliHLTDummyCRORCHandler::SetOptions( const vector<MLUCString>&, char*&, unsigned& )
    {
    return 0;
    }

void AliHLTDummyCRORCHandler::GetAllowedOptions( vector<MLUCString>& )
    {
    }


int AliHLTDummyCRORCHandler::ActivateRORC()
    {
    fActive = true;
    return 0;
    }

int AliHLTDummyCRORCHandler::DeactivateRORC()
    {
    fActive = false;
    return 0;
    }


int AliHLTDummyCRORCHandler::PollForEvent( AliEventID_t& eventID, 
					  AliUInt64_t& dataOffset, AliUInt64_t& dataSize,
					  bool& dataDefinitivelyCorrupt, bool& eventSuppressed, AliUInt64_t& unsuppressedDataSize )
    {
    dataDefinitivelyCorrupt = false;
    eventSuppressed = false;
    if ( !fActive )
	return EAGAIN;
    if ( fEventCount>=fMaxEventCount )
	return EAGAIN;
    eventID = ++fEventCounter;
    dataOffset = fOffset;
    dataSize = fBlockSize;
    unsuppressedDataSize = dataSize;
    //fOffset += dataSize+headerSize;
    fOffset += dataSize;

    if ( fOffset % 128 )
	{
	fOffset += 128 - (fOffset % 128);
	}
    LOG( AliHLTLog::kDebug, "AliHLTDummyCRORCHandler::PollForEvent", "Event Announced" )
	<< "'Polled' event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << ") with data size " << dataSize << " bytes at offset " << dataOffset 
	<< ". New offset: " 
	<< fOffset << "." << ENDLOG;
    pthread_mutex_lock( &fEventMutex );
    fEvents.Add( eventID );
    fEventCount++;
    pthread_mutex_unlock( &fEventMutex );

    return 0;
    }


int AliHLTDummyCRORCHandler::ReleaseEvent( AliEventID_t eventID )
    {
    unsigned long ndx;
    pthread_mutex_lock( &fEventMutex );
    if ( MLUCVectorSearcher<AliEventID_t,AliEventID_t>::FindElement( fEvents, &EventDataSearchFunc, eventID, ndx ) )
	{
	fEvents.Remove( ndx );
	fEventCount--;
	LOG( AliHLTLog::kDebug, "AliHLTDummyCRORCHandler::ReleaseEvent", "Releasing event" )
	    << "Releasing event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ")." << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTDummyCRORCHandler::ReleaseEvent", "Cannot find event" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") to be released could not be found." << ENDLOG;
	}
    pthread_mutex_unlock( &fEventMutex );
    return 0;
    }



/*********** HLT-OUT specific ***************/

int AliHLTDummyCRORCHandler::SubmitEvent( AliEventID_t eventID, const AliHLTSubEventDescriptor::BlockData& block,
				 const AliHLTSubEventDataDescriptor*, const AliHLTEventTriggerStruct* )
    {
    LOG( AliHLTLog::kDebug, "AliHLTDummyCRORCHandler::SubmitEvent", "Event submitted" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << ") submitted: block.fOffset: "
	<< block.fOffset << " - block.fSize: " << block.fSize << "." << ENDLOG;
    pthread_mutex_lock( &fEventMutex );
    fEvent = eventID;
    pthread_mutex_unlock( &fEventMutex );
    return 0;
    }

int AliHLTDummyCRORCHandler::PollForDoneEvent()
    {
    if ( !fActive )
	return EAGAIN;
    bool found=false;
    pthread_mutex_lock( &fEventMutex );
    if ( fEvent!=AliEventID_t() )
	{
	found=true;
	LOG( AliHLTLog::kDebug, "AliHLTDummyCRORCHandler::PollForDoneEvent", "Done event polled" )
	    << "Polled done event 0x" << AliHLTLog::kHex << fEvent
	    << " (" << AliHLTLog::kDec << fEvent << ")." << ENDLOG;
	fEvent = AliEventID_t();
	}
    pthread_mutex_unlock( &fEventMutex );
    if ( found )
	return 0;
    else
	return EAGAIN;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
