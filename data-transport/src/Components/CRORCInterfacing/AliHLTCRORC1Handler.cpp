/**
 * @file AliHLTCRORC1Handler.cpp
 * @author Heiko Engel <hengel@cern.ch>
 * @date 2014-07-17
 *
 * Based on AliHLTRORC6Handler.cpp by Timm Morten Steinbeck, 2001
 **/

#include "AliHLTCRORC1Handler.hpp"
#include "AliHLTDDLHeader.hpp"
#include "AliHLTDDLHeaderData.h"
#include "AliHLTLog.hpp"
#include "AliHLTConfig.hpp"
#include "NOPEEvents.hpp"
#include <errno.h>
#include <cmath>

#ifdef SIMULATE
#warning SIMULATE option not implemented for AliHLTCRORC1Handler.

void TriggerSignalHandler(int) {
  LOG(AliHLTLog::kError,
      "AliHLTCRORC1Handler::TriggerSignalHandler", "Not Implemented")
      << "TriggerSignalHandler() is not implemented for C-RORC!" << ENDLOG;
}
#endif

#define ORBIT_PERIOD_SECONDS_DOUBLE 1492.0

AliHLTCRORC1Handler::AliHLTCRORC1Handler(unsigned long dataBufferSize,
                                         int eventSlotsExp2)
    : fEvents(eventSlotsExp2), fNextInvalidEventID(kAliEventTypeCorruptID, 0) {
  pthread_mutex_init(&fEventMutex, NULL);

  fEnumerateEventIDs = false;
  fNextEventID = 0;

  fLastRORCEventID = AliEventID_t();
  fAddEventIDCounter = (~(AliUInt64_t)0) << 36;
  fLastLogMessageEventIDCounter = fAddEventIDCounter;

  fLastEventIDWrapAround.tv_sec = fLastEventIDWrapAround.tv_usec = 0;

  fHeaderSearch = true;
  fReportWordConsistencyCheck = false;

#ifdef SIMULATE
  signal(TRIGGER_SIGNAL, TriggerSignalHandler);
#endif
  fRORCBlockSizeOverrideSet = false;
  fRORCBlockSizeOverride = 0;
  fRORCBlockSize = 0;
  fRORCBlock32bitWords = 0;
  fEventCounter = 0;
  fEventBufferId = 0;
  fEventBufferSet = false;
  fNoFlowControl = false;
  fEventSuppression_Exp2 = 0;
  fEventSuppressionActive = false;
  fEnableHWCoProc = false;
  fSinglePadSuppression = false;
  fBypassMerger = false;
  fDeconvPad = true;
  fClusterLowerLimit = 10;
  fSingleSeqLimit = 0;
  fMergerDistance = 4;
  fMergerAlgorithmFollow = true;
  fChargeTolerance = 0;
  fNoiseSuppression = 0;
  fNoiseSuppressionMinimum = 0;
  fNoiseSuppressionNeighbor = 0;
  fClusterQmaxLowerLimit = 0;
  fTagBorderClusters = 0;
  fCorrectEdgeClusters = 0;
  fTagDeconvolutedClusters = 0;
  fNoPeriodCounterLogModulo = 1;
  fPeriodCounterLogCounter = 0;
  fFcfAltroChannelErrorCount = 0;
  fFcfRcuProtocolErrorCount = 0;

  fEventStream = NULL;
  fDiuInstance = NULL;
  fSiuInstance = NULL;
  fGtxInstance = NULL;
  fFastClusterFinder = NULL;
  fRawcopyInstance = NULL;
  fEventFilter = NULL;
  fDataSource = kRemoteDiu;
  fDataDirection = kDMAToHost;
  fIsRemoteDataSource = true;
  fHLTOutDmaTestMode = false;
  fOutBuffer = NULL;
  fSodReceived = false;
  fLinkspeed = 0;
  fDdlClkPeriod = 0;
  fFwDate = 0;
}

int AliHLTCRORC1Handler::Open(uint8_t deviceId, uint32_t channelId,
                              AliHLTRORCDataDirection dataDirection,
                              AliHLTRORCDataSource dataSource) {
  fDataSource = dataSource;
  if (dataSource == AliHLTCRORCHandlerInterface::kRemoteDiu ||
      dataSource == AliHLTCRORCHandlerInterface::kRemoteSiu) {
    fIsRemoteDataSource = true;
  } else {
    fIsRemoteDataSource = false;
  }

  librorc::EventStreamDirection esType;
  switch (dataDirection) {
  case kDMAToHost:
    esType = librorc::kEventStreamToHost;
    break;
  case kDMAToDevice:
    esType = librorc::kEventStreamToDevice;
    break;
  default:
    LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::Open", "Eval data direction")
        << "Invalid RORC data direction specifier" << ENDLOG;
    return EINVAL;
  }

  try {
    fEventStream = new librorc::event_stream(deviceId, channelId, esType);
  } catch (int e) {
    LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::Open",
        "event_stream initialization")
        << librorc::errMsg(e) << ENDLOG;
    return ENODEV;
  }
  fFwDate = fEventStream->m_sm->FwBuildDate();

  // init datastream from device to host:
  // {DIU|DDR|PG|RAW} -> [FCF] -> Filter -> DMA
  if (esType == librorc::kEventStreamToHost) {
    fDiuInstance = fEventStream->getDiu();
    fRawcopyInstance = fEventStream->getRawReadout();

    if (!fDiuInstance && !fRawcopyInstance) {
      LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::Open", "ES to Host")
          << "No input data source available for this channel" << ENDLOG;
      return ENODEV;
    }

    fFastClusterFinder = fEventStream->getFastClusterFinder();
    fEventFilter = new librorc::eventfilter(fEventStream->m_link);
  }

  // init datastream from host to device
  // PCIe -> SIU
  if (esType == librorc::kEventStreamToDevice) {
    fSiuInstance = fEventStream->getSiu();
    if (!fSiuInstance) {
      LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::Open", "getSiu")
          << "No SIU available for this channel" << ENDLOG;
      return ENODEV;
    }
  }

  fGtxInstance = new librorc::gtx(fEventStream->m_link);
  ReadLinkspeed();
  return 0;
}

AliHLTCRORC1Handler::~AliHLTCRORC1Handler() {
  pthread_mutex_destroy(&fEventMutex);
}

int AliHLTCRORC1Handler::Close() {
  fRORCBlockSize = 0;
  fRORCBlock32bitWords = 0;

  if (fGtxInstance) {
    delete fGtxInstance;
  }
  if (fRawcopyInstance) {
    delete fRawcopyInstance;
  }
  if (fFastClusterFinder) {
    delete fFastClusterFinder;
  }
  if (fDiuInstance) {
    delete fDiuInstance;
  }
  if (fSiuInstance) {
    delete fSiuInstance;
  }
  if (fEventFilter) {
    delete fEventFilter;
  }
  if (fOutBuffer) {
    delete fOutBuffer;
    fOutBuffer = NULL;
  }
  if (fEventStream) {
    delete fEventStream;
  }
  return 0;
}

void AliHLTCRORC1Handler::ReadLinkspeed() {
  librorc::gtxpll_settings pllcfg = fGtxInstance->drpGetPllConfig();
  pllcfg.refclk = fEventStream->m_sm->refclkFreq();
  fLinkspeed = 2 * pllcfg.refclk * pllcfg.n1 * pllcfg.n2 / pllcfg.m / pllcfg.d;
  if (fFastClusterFinder) {
    fDdlClkPeriod = fLinkspeed / 10 / 4; // FCF has an MMCM, using DDLCLK = GTXLK/2
  } else {
    fDdlClkPeriod = fLinkspeed / 10 / 2; // no MMCM, using DDLCLK = GTXCLK
  }
}


int AliHLTCRORC1Handler::SetEventBuffer(AliHLTShmID shmKey) {
  if (fEventStream == NULL) {
    LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::SetEventBuffer",
        "check fEventStream")
        << "Event stream not initialized!" << ENDLOG;
    return EINVAL;
  }

  if (shmKey.fShmType != kLibrorcShmType) {
    LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::SetEventBuffer",
        "check SHM Buffer Type")
        << "SHM Buffer is not a librorc buffer - can't use it with a C-RORC"
        << ENDLOG;
    return EINVAL;
  }

  // get DeviceId and BufferID from shmKey
  uint32_t shmDeviceId = AliHLTShmIDGetLibrorcDeviceId( shmKey );
  if (fEventStream->m_dev->getDeviceId() != shmDeviceId) {
    LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::SetEventBuffer",
        "check DeviceId")
        << "provided buffer does not belong to selected device" << ENDLOG;
    return ENODEV;
  }

  fEventBufferId = AliHLTShmIDGetLibrorcBufferId( shmKey );
  fEventBufferSet = true;
  return 0;
}

int AliHLTCRORC1Handler::InitializeRORC() {
  // make sure an EventBuffer was set
  if (!fEventBufferSet) {
    LOG(AliHLTLog::kError,
        "AliHLTCRORC1Handler::InitializeRORC", "check event buffer set")
        << "event buffer is not set. Call SetEventBuffer() before "
           "InitializeRORC()." << ENDLOG;
    return ENODEV;
  }

  // if an override value for rorcBlockSize was specified, propagate it to the
  // RORC. Else get the default block size from device.
  if (fRORCBlockSizeOverrideSet) {
    if (fEventStream->overridePciePacketSize(fRORCBlockSizeOverride) != 0) {
      LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::InitializeRORC",
          "override PCIe packet size")
          << "Invalid RORC Block Size" << ENDLOG;
      return EINVAL;
    }
    fRORCBlockSize = fRORCBlockSizeOverride;
  } else {
    /**
     * Default block size is PCIe MaxPayloadSize for DMA to host and PCIe
     * MaxReadRequestSize for DMA to device operation.
     **/
    if (fDataDirection == kDMAToHost) {
      fRORCBlockSize = fEventStream->m_dev->maxPayloadSize();
    } else {
      fRORCBlockSize = fEventStream->m_dev->maxReadRequestSize();
    }
  }
  fRORCBlock32bitWords = fRORCBlockSize / 4;

  // Initialize the ReportBuffer and configure the RORC DMA Engine
  int result = fEventStream->initializeDma(fEventBufferId, 0);
  if (result != 0) {
    LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::InitializeRORC",
        "initializeDma")
        << librorc::errMsg(result) << ": EventBufferId=" << fEventBufferId << ENDLOG;
    return result;
  }

  fEventBufferSize = fEventStream->m_eventBuffer->getPhysicalSize();
  fEvent = AliEventID_t();

  LOG(AliHLTLog::kInformational, "AliHLTCRORC1Handler::InitializeRORC",
      "Status")
      << "Initialized C-RORC DMA Channel with librorcBufferID="
      << AliHLTLog::kDec << fEventBufferId
      << ", Size=" << (fEventBufferSize >> 20)
      << "MB using RORCBlockSize=" << LOG_HEX_DEC(fRORCBlockSize) << "Byte"
      << ENDLOG;

#ifdef MODELSIM
  fEventStream->m_link->waitForGTXDomain();
  while(!fEventStream->m_link->isDdlDomainReady());
#endif
  // check if clocks are running
  if (!fEventStream->m_link->isGtxDomainReady()) {
    LOG(AliHLTLog::kError,
        "AliHLTCRORC1Handler::Initialize", "isGtxDomainReady")
        << "Clocks are down" << ENDLOG;
    return EBUSY;
  }

  // disable backend for a clean startup
  fEventStream->m_link->setChannelActive(0);
  // set flow-control
  fEventStream->m_link->setFlowControlEnable(fNoFlowControl == true ? 0 : 1);
  // clear event counter in DMA engine
  fEventStream->m_channel->clearEventCount();
  // clear PCIe stall counter in DMA engine
  fEventStream->m_channel->clearStallCount();
  // clear software pointer stall flags
  fEventStream->m_channel->readAndClearPtrStallFlags();

  if (fDiuInstance) {
    fDiuInstance->setEnable(0);
    fDiuInstance->setReset(1);
    fDiuInstance->clearDmaDeadtime();
    fDiuInstance->clearDdlDeadtime();
    fDiuInstance->clearEventcount();
    ConfigureEventSuppression();
    fDiuInstance->setReset(0);
    fDiuInstance->clearInterfaceCounters();
  }

  if (fRawcopyInstance) {
    fRawcopyInstance->setEnable(0);
    fRawcopyInstance->setReset(0);
    fRawcopyInstance->clearDmaDeadtime();
    ConfigureEventSuppression();
    fRawcopyInstance->setReset(0);
  }

  if (fFastClusterFinder) {
    fFastClusterFinder->setReset(1);
    fFastClusterFinder->setEnable(0);
    fFastClusterFinder->clearErrors();

    if (fEnableHWCoProc) {
        /** clusterfinder is available and will be used **/
        fFastClusterFinder->setBypass(0);
        InitializeHWCoProcParams();
    } else {
      /**
       * clusterfinder is available, but fEnableHWCoProc is not set. So the
       * clusterfinder will be set to bypass mode which pushes any DDL data
       * through without touching the data.
       **/
        fFastClusterFinder->setBypass(1);
    }

  } else if (fEnableHWCoProc) {
      LOG(AliHLTLog::kError,
              "AliHLTCRORC1Handler::InitializeRORC", "Cannot enable HWCoProc")
          << "HWCoProc/FastClusterFinder not available" << ENDLOG;
      return ENODEV;
  }

  if (!fHLTOutDmaTestMode && fSiuInstance) {
    fSiuInstance->setReset(0);
    fSiuInstance->setEnable(0);
    fSiuInstance->clearEventcount();
    fSiuInstance->clearDmaDeadtime();
    fSiuInstance->clearDdlDeadtime();
    fSiuInstance->clearInterfaceCounters();
  }

  return 0;
}

void AliHLTCRORC1Handler::ConfigureEventSuppression() {
  uint32_t filtermask = 0;
  if (fEventSuppressionActive) {
    /**
    * The lower 24 bit of this filter mask are logically AND'ed with the
    * event orbit ID from CDH. If the result is 0 the event is fully read
    * out. If the result is !=0 the event payload is dropped and the CDH is
    * modified to signal this change. Set to 0 to read out all events, set 1
    * to read out every second event, 3 for every fourth, etc.
    **/
    if (fEventSuppression_Exp2 > 23) {
      LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::ConfigureEventSuppression",
          "Suppression value sanity check")
          << "suppression value out of bounds - disabling event suppression"
          << ENDLOG;
    } else {
      filtermask = (1 << fEventSuppression_Exp2) - 1;
    }
  }
  fEventFilter->setFilterMask(filtermask);
  fEventFilter->setFilterAll(false);
}

int AliHLTCRORC1Handler::DeinitializeRORC() {
  // TODO: this is very similar to DeactivateRORC()?!
  fEventStream->m_link->setFlowControlEnable(0);
  fEventStream->m_link->setChannelActive(0);

  if (fDiuInstance) {
    fDiuInstance->setEnable(0);
  }

  if (fRawcopyInstance) {
    fRawcopyInstance->setEnable(0);
  }

  if (fFastClusterFinder) {
    fFastClusterFinder->setReset(1);
    fFastClusterFinder->setEnable(0);
  }

  if (fSiuInstance) {
    fSiuInstance->setEnable(0);
  }

  fEventStream->m_channel->disable();
  return 0;
}

int AliHLTCRORC1Handler::InitializeFromRORC(bool first) {
  LOG(AliHLTLog::kError,
      "AliHLTCRORC1Handler::InitializeFromRORC", "Not Implemented")
      << "InitializeFromRORC() is not implemented for C-RORC" << ENDLOG;
  return 0;
}

int AliHLTCRORC1Handler::SetOptions(const vector<MLUCString> &, char *&,
                                    unsigned &) {
  LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::SetOptions", "Not Implemented")
      << "SetOptions() is not implemented for C-RORC" << ENDLOG;
  return 0;
}

void AliHLTCRORC1Handler::GetAllowedOptions(vector<MLUCString> &options) {
  options.clear();
}

// Write a FCF ConfigRAM entry to RORC.
// This is called by AliHLTCRORCTPCMapping::WriteData
int AliHLTCRORC1Handler::WriteConfigWord(unsigned long wordIndex,
                                         AliUInt32_t word, bool doVerify) {
  if (wordIndex >= 4096) {
    return EINVAL;
  }

  if (!fFastClusterFinder) {
    LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::WriteConfigWord", "check")
        << "FastClusterFinder not available" << ENDLOG;
    return EIO;
  }
  fFastClusterFinder->writeMappingRamEntry(wordIndex, word);

  if (doVerify) {
    // the edge flag (bit29) is only available in FW images newer than 2017
    uint32_t mask = (fFwDate > 0x20170000) ? 0x3fffffff : 0x1fffffff;
    uint32_t readback = fFastClusterFinder->readMappingRamEntry(wordIndex);
    if ((readback & mask) != (word & mask)) {
      return EIO;
    }
  }

  return 0;
}

int AliHLTCRORC1Handler::ActivateRORC() {
  switch (fDataSource) {
  case AliHLTCRORCHandlerInterface::kRemoteDiu:
  case AliHLTCRORCHandlerInterface::kRemoteSiu:
  case AliHLTCRORCHandlerInterface::kLocalPcie:
    fEventStream->m_link->setDefaultDataSource();
    break;
  case AliHLTCRORCHandlerInterface::kLocalDdr:
    fEventStream->m_link->setDataSourceDdr3DataReplay();
    LOG(AliHLTLog::kImportant,
        "AliHLTCRORC1Handler::ActivateRORC", "Set Data Source")
        << "Using onboard DDR3 memory as data source" << ENDLOG;
    break;
  case AliHLTCRORCHandlerInterface::kLocalPg:
    fEventStream->m_link->setDataSourcePatternGenerator();
    LOG(AliHLTLog::kImportant,
        "AliHLTCRORC1Handler::ActivateRORC", "Set Data Source")
        << "Using onboard pattern generator as data source" << ENDLOG;
    break;
  default:
    LOG(AliHLTLog::kError,
        "AliHLTCRORC1Handler::ActivateRORC", "Set Data Source")
        << "Invalid/Unknown data source selected: " << fDataSource << ENDLOG;
    return EINVAL;
    break;
  }

  /**
   * enable channel for all cases except if we are in HLT_OUT mode with
   * HLTOutDmaTestMode enabled.
   **/
  uint32_t channel_active = 1;
  if (fHLTOutDmaTestMode && fSiuInstance) {
    channel_active = 0;
  }
  // activate channel
  fEventStream->m_link->setChannelActive(channel_active);

  if (fFastClusterFinder) {
    fFastClusterFinder->setReset(0);
    fFastClusterFinder->setEnable(1);
  } else if (fEnableHWCoProc) {
    LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::ActivateRORC",
        "EnableHWCoProc")
        << "FastClusterFinder requested but not available!" << ENDLOG;
  }

  /**
   * Note: the return value of ActivateRORC() is not checked in
   * AliHLTCRORCPublisher::StartEventLoop(). If errors are observed here,
   * implement a GTX RX reset and a retry on prepareForDiuData() /
   * prepareForSiuData()
   **/

  if ((fDiuInstance && fIsRemoteDataSource) ||
      (!fHLTOutDmaTestMode && fSiuInstance)) {
    // Data is going or coming via GTX, so check if the low level link is up
    if (!fGtxInstance->isLinkUp()) {
      LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::ActivateRORC",
          "check Transceiver")
          << "Optical link is down at the transceiver level. C-RORC will not "
             "be able to send or receive DDL data." << ENDLOG;
      return EFAULT;
    }
  }

  if (fDiuInstance && fIsRemoteDataSource ) {

    // initialize remote link partner (if applicable)
    if (fDataSource == AliHLTCRORCHandlerInterface::kRemoteDiu) {
      if (fDiuInstance->prepareForDiuData() < 0) {
        LOG(AliHLTLog::kError,
            "AliHLTCRORC1Handler::ActivateRORC", "prepareForDiuData")
            << "Timeout resetting remote DIU to local DIU chain" << ENDLOG;
        return EFAULT;
      }
      fDiuInstance->setEnable(1);
    } else if (fDataSource == AliHLTCRORCHandlerInterface::kRemoteSiu) {
      if (fDiuInstance->prepareForSiuData() < 0) {
        LOG(AliHLTLog::kError,
            "AliHLTCRORC1Handler::ActivateRORC", "prepareForSiuData")
            << "Timeout resetting remote SIU to local DIU chain" << ENDLOG;
        return EFAULT;
      }
      fDiuInstance->setEnable(1);
      if (fDiuInstance->sendFeeReadyToReceiveCmd()) {
        LOG(AliHLTLog::kError,
            "AliHLTCRORC1Handler::ActivateRORC", "prepareForSiuData")
            << "Timeout sending RDYRX to remote SIU" << ENDLOG;
        return EFAULT;
      }
    }
    fDiuInstance->readAndClearDiuInterfaceStatus();
  }

  if (fRawcopyInstance) {
      fRawcopyInstance->setEnable(1);
  }

  if (!fHLTOutDmaTestMode && fSiuInstance) {
    fSiuInstance->setEnable(1);
  }

  fGtxInstance->clearErrorCounters();
  return 0;
}

int AliHLTCRORC1Handler::DeactivateRORC() {
  fEventStream->m_link->setFlowControlEnable(0);
  fEventStream->m_link->setChannelActive(0);

  if (fDiuInstance) {
    if (fDataSource == AliHLTCRORCHandlerInterface::kRemoteSiu) {
      fDiuInstance->sendFeeEndOfBlockTransferCmd();
    }
    fDiuInstance->setEnable(0);
  }

  if (fFastClusterFinder) {
    fFastClusterFinder->setReset(1);
    fFastClusterFinder->setEnable(0);
  }

  if (fSiuInstance) {
    fSiuInstance->setEnable(0);
    if(!fHLTOutDmaTestMode && fSiuInstance->totalWordsTransmitted() == 0) {
      LOG( AliHLTLog::kWarning, "AliHLTCRORC1Handler::DeactivateRORC",
          "TotalWordCount") << "No data sent to DAQ" << ENDLOG;
    }
  }

  if (fRawcopyInstance) {
    fRawcopyInstance->setEnable(0);
  }

  if (fEventStream->m_channel->disable() != 0) {
    LOG(AliHLTLog::kWarning,
        "AliHLTCRORC1Handler::DeactivateRORC", "disable DMA channel")
        << "Timeout trying to disable DMA channel gracefully - forced disable"
        << ENDLOG;
  }

  /** get number of clock cycles sending XOFF to DAQ **/
  if (fDiuInstance && fIsRemoteDataSource) {
    if(fDiuInstance->totalWordsReceived() == 0) {
      LOG( AliHLTLog::kError, "AliHLTCRORC1Handler::DeactivateRORC",
          "TotalWordCount") << "No data received from DAQ" << ENDLOG;
    }
  }

  /** get number of clock cycles where PCIe bus was busy **/
  AliUInt32_t pciStallCnt = fEventStream->m_channel->stallCount();
  AliUInt32_t pciStallCntMs = pciStallCnt / 250000; // clock: 4ns / 250 MHz
  if (pciStallCntMs > 10) {
    LOG(AliHLTLog::kWarning, "AliHLTCRORC1Handler::DeactivateRORC",
        "PCIe Stall Count")
        << "PCIe link was throtteling DMA transfer " << LOG_DEC_HEX(pciStallCnt)
        << " clock cycles (" << pciStallCntMs
        << " ms) between DMA enable and disable." << ENDLOG;
  }

  /** compare event counts of DMA engine and DDL interface **/
  AliUInt32_t dmaEventCnt = fEventStream->m_channel->eventCount();
  AliUInt32_t ddlEventCnt = dmaEventCnt;
  if (fDiuInstance && fIsRemoteDataSource) {
    ddlEventCnt = fDiuInstance->getEventcount();
  } else if (fSiuInstance && !fHLTOutDmaTestMode) {
    ddlEventCnt = fSiuInstance->getEventcount();
  }
  if (dmaEventCnt != ddlEventCnt) {
    LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::DeactivateRORC",
        "Event Count Check")
        << "Event count mismatch: DMA engine processed "
        << LOG_DEC_HEX(dmaEventCnt)
        << " events, but DDL interface reports "
        << LOG_DEC_HEX(ddlEventCnt) << " events." << ENDLOG;
  }

  return 0;
}

/**
 * NOTE: the size reported here is the EventBuffer size minus the size of
 * pending events. This is not necessarily the buffer size available for RORC
 * DMA transfers, because the RORC may already have filled this buffer whith
 * events that are not yet registered by PollForEvent()
 **/
uint64_t AliHLTCRORC1Handler::GetFreeBufferSize() {
  uint64_t release_offset = fEventStream->getEBReleaseOffset();
  uint64_t receive_offset = fEventStream->getEBReceiveOffset();
  if (receive_offset >= release_offset) {
    return fEventBufferSize - (receive_offset - release_offset);
  } else {
    return release_offset - receive_offset;
  }
}

int AliHLTCRORC1Handler::PollForEvent(AliEventID_t &eventID,
                                      AliUInt64_t &dataOffset,
                                      AliUInt64_t &dataSize,
                                      bool &dataDefinitivelyCorrupt,
                                      bool &eventSuppressed,
                                      AliUInt64_t &unsuppressedDataSize) {
  librorc::EventDescriptor *report = NULL;
  const uint32_t *event = 0;
  uint64_t librorcEventReference = 0;

  if (!fEventStream->getNextEvent(&report, &event, &librorcEventReference)) {
    return EAGAIN;
  }

  /**
   * - 'report' points to received entry in ReportBuffer, is a struct of
   *   { uint64_t offset, uint32_t calc_event_size, uint32_t
   *     reported_event_size }
   * - 'event' points to first DW of the received event in EventBuffer
   * - 'librorcEventReference' - librorc-internal event reference, use this to
   *   free the event again
   **/
  LOG(AliHLTLog::kDebug, "AliHLTCRORC1Handler::PollForEvent", "getNextEvent")
      << "Event Received: Offset=" << LOG_HEX_DEC(report->offset)
      << " ReportedSizeField=" << LOG_HEX_DEC(report->reported_event_size)
      << " CalculatedSizeField=" << LOG_HEX_DEC(report->calc_event_size)
      << ENDLOG;

  // optional: update librorc-internal statistics
  fEventStream->updateChannelStatus(report);

  // reported/calculated sizes are in DWs, the upper two bits are flags
  AliUInt32_t dmaWords = (report->calc_event_size & 0x3fffffff);
  AliUInt32_t diuWords = (report->reported_event_size & 0x3fffffff);
  AliUInt32_t diuErrorFlag = (report->reported_event_size >> 30) & 1;
  AliUInt32_t words = dmaWords;
  dataSize = words * 4;
  unsuppressedDataSize = diuWords * 4;

  // make sure offset is still in buffer
  if (report->offset > fEventStream->m_eventBuffer->getPhysicalSize()) {
    LOG(AliHLTLog::kFatal, "AliHLTCRORC1Handler::PollForEvent",
        "Event Inconsistency")
        << "Reported event offset exceeds event buffer size"
        << " - offset 0x" << report->offset << " - buffer size 0x"
        << fEventStream->m_eventBuffer->size() << "." << ENDLOG;
    fEventStream->releaseEvent(librorcEventReference);
    return EFAULT;
  }

  // check if event is a NOPE event: release & return
  if (dataSize == sizeof(gNOPEEventIdentifier) &&
      (*(AliVolatileUInt64_t *)event) == gNOPEEventIdentifier) {
    LOG(AliHLTLog::kInformational, "AliHLTCRORC1Handler::PollForEvent",
        "NOPE event found")
        << "NOPE event found, event will be ignored." << ENDLOG;
    fEventStream->releaseEvent(librorcEventReference);
    if (fEnumerateEventIDs) {
      eventID = fNextEventID;
      ++fNextEventID;
    }
    return ENOMSG;
  }

  // The FW always transfers in multiples of pciePacketSize. Following an
  // event, one additional data word is written containing the reported event
  // size.
  words += 1;

  // Data is always transfered in blocks of fRORCBlockSize bytes. partially
  // used blocks are filled with dummy data.
  unsigned long blocks = words / fRORCBlock32bitWords;
  if (words % fRORCBlock32bitWords) {
    blocks++;
  }

  LOG(AliHLTLog::kDebug, "AliHLTCRORC1Handler::PollForEvent", "Found event")
      << "Event found with " << AliHLTLog::kDec << LOG_DEC_HEX(dataSize)
      << " bytes and " << LOG_DEC_HEX(blocks) << " blocks." << ENDLOG;

  AliHLTDDLHeader ddlHeader;
  bool corruptEventID = false;

  // Note: This DIU error flag was not read out with the H-RORC
  if (diuErrorFlag) {
    LOG(AliHLTLog::kWarning, "AliHLTCRORC1Handler::PollForEvent",
        "getNextEvent")
        << "Received event with DIU error flag set."
        << ENDLOG;
    dataDefinitivelyCorrupt = true;
  }

  if (fEnableHWCoProc) {
    uint32_t hwcoproc_flags = (report->calc_event_size >> 30) & 0x3;
    if (hwcoproc_flags & 0x02) {
      fFcfAltroChannelErrorCount++;
    }
    if (hwcoproc_flags & 0x01) {
      fFcfRcuProtocolErrorCount++;
    }
  }

  /**
   * dirty dirty workaround to handle a leading zero coming via DIU:
   * first CDH word should never be zero, must be 0xffffffff or event size
   **/
  bool skipLeadingZero = false;
  AliUInt32_t defaultCdhWords =
      (AliHLTGetDDLHeaderVersionSize(DDL_HEADER_DEFAULT_VERSION) / 4);
  if (event[0] == 0x00000000 &&
      ((dmaWords > diuWords) ||
       (fSodReceived == false && diuWords == defaultCdhWords + 1))) {
      LOG(AliHLTLog::kWarning, "AliHLTCRORC1Handler::PollForEvent",
          "leading Zero check")
          << "Found event with leading zero - "
          << " Reference: " << LOG_DEC_HEX(librorcEventReference)
          << " EB-Offset: " << LOG_DEC_HEX(report->offset)
          << " DIU-Words: " << LOG_DEC_HEX(diuWords)
          << " DMA-Words: " << LOG_DEC_HEX(dmaWords)
          << " DIU-Error: " << diuErrorFlag << ENDLOG;
      skipLeadingZero = true;
      // skip first word
      event++;
      // decrement #DMA words to avoid DIU vs. DMA word count check to fail
      if (!fSodReceived && dmaWords == diuWords) {
        diuWords--;
      }
      dmaWords--;
      // decrement dataSize, but avoid underflow
      dataSize = (dataSize>4) ? (dataSize-4) : 0;
  }

  // make sure the transfered event size is at least the minimum CDH size before
  // evaluating CDH elements
  if (dataSize < AliHLTMinDDLHeaderSize) {
    LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::PollForEvent",
        "Event Inconsistency")
        << "Received event is smaller than minimum DDL header size"
        << " - received 0x" << (dmaWords << 2) << "bytes." << ENDLOG;
    dataDefinitivelyCorrupt = true;
    corruptEventID = true;
  } else if (fHeaderSearch && !ddlHeader.Set((AliUInt32_t *)event)) {
    LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::PollForEvent",
        "Error reading DDL header")
        << "Error reading DDL header number. Unknown header version or out "
           "of memory. Header version: " << AliHLTLog::kDec
        << (unsigned)AliHLTGetDDLHeaderVersion(event)
        << " - header size: " << AliHLTGetDDLHeaderSize(event)
        << " DIU-Words: " << LOG_DEC_HEX(diuWords)
        << " DMA-Words: " << LOG_DEC_HEX(dmaWords) << ENDLOG;
    dataDefinitivelyCorrupt = true;
    corruptEventID = true;
  }

  // event suppression is now indicated in CDH Word 4 Bit 27
  // Status & Error Bits are in DW4, bits [27:12]
  eventSuppressed = fHeaderSearch && !corruptEventID &&
                    (((AliHLTGetDDLHeaderStatusError(event) >> 15) & 1) != 0);

  if (!dataDefinitivelyCorrupt && !eventSuppressed) {
    // compare calculated and reported event sizes for non-suppressed events
    if (dmaWords != diuWords) {
      LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::PollForEvent",
          "Event Inconsistency")
          << "Error: Found inconsistency in event "
          << LOG_DEC_HEX(fEventCounter)
          << " for event size: DIU Word count: " << LOG_DEC_HEX(diuWords)
          << " - DMA Word count: " << LOG_DEC_HEX(dmaWords)
          << ". Continuing with RORC reported DMA size." << ENDLOG;
      dataDefinitivelyCorrupt = true;
    }
  }

  EventData ed;
  ed.fSize = blocks * fRORCBlockSize;
  ed.fOffset = dataOffset = report->offset;
  if (skipLeadingZero) {
      ed.fOffset = dataOffset = report->offset + 4;
  }
  // fWrappedSize is not required for C-RORC because the buffer is overmapped.
  ed.fWrappedSize = 0;
  // new: EventData.reference stores the librorc-internal event reference,
  // which can directly be used to release the event again.
  ed.reference = librorcEventReference;

  if (fHeaderSearch) {
    if (corruptEventID) {
      eventID = fNextInvalidEventID;
      ++fNextInvalidEventID;
    } else {
      unsigned long headerSize = ddlHeader.GetHeaderSize();

      if (headerSize > dataSize) {
        LOG(AliHLTLog::kWarning, "AliHLTCRORC1Handler::PollForEvent",
            "Data smaller than DDL header")
            << "Data received is smaller than reported DDL header size "
            << AliHLTLog::kDec << headerSize << " - header version: "
            << (unsigned)AliHLTGetDDLHeaderVersion(event) << "." << ENDLOG;
        eventID = fNextInvalidEventID;
        ++fNextInvalidEventID;
        dataDefinitivelyCorrupt = true;
      } else {
        eventID = ddlHeader.GetEventID();

        AliUInt32_t L1msg = ddlHeader.GetL1TriggerType();
        if ((L1msg & 0x1) && (((L1msg & 0x3C) >> 2) == 0xE)) {
          eventID.fType = kAliEventTypeStartOfRun;
	  fSodReceived = true;
        } else if ((L1msg & 0x1) && (((L1msg & 0x3C) >> 2) == 0xF)) {
          eventID.fType = kAliEventTypeEndOfRun;
        } else if ((L1msg & 0x1) && (((L1msg & 0x3C) >> 2) == 0xD)) {
          eventID.fType = kAliEventTypeSync;
        } else {
          eventID.fType = kAliEventTypeData;
        }
      }
    }
  } // fHeaderSearch

  if (fEnumerateEventIDs) {
    LOG(AliHLTLog::kInformational, "AliHLTCRORC1Handler::PollForEvent",
        "Enumerating found event")
        << "Enumerating found event " << LOG_HEX_DEC(eventID) << " to 0x"
        << LOG_HEX_DEC(fNextEventID) << "." << ENDLOG;
    fNextEventID.fType = eventID.fType; //keep current event type
    eventID = fNextEventID;
    ++fNextEventID;
  } else if (!corruptEventID) {
    if (fLastEventIDWrapAround.tv_sec == 0 &&
        fLastEventIDWrapAround.tv_usec == 0) {
      gettimeofday(&fLastEventIDWrapAround, NULL);
    }
    if (eventID.fNr <= fLastRORCEventID.fNr) {
      struct timeval now;
      gettimeofday(&now, NULL);
      double delta = (now.tv_sec - fLastEventIDWrapAround.tv_sec) * 1000000 +
                     now.tv_usec - fLastEventIDWrapAround.tv_usec;
      int64_t cnt =
          (int64_t)round(delta / (ORBIT_PERIOD_SECONDS_DOUBLE * 1000000.0));
      if (cnt <= 0) {
        cnt = 1;
      }
      AliEventID_t tmpID(kAliEventTypeUnknown, cnt);
      tmpID.fNr <<= 36;
      fAddEventIDCounter.fNr += tmpID.fNr;
      fLastEventIDWrapAround = now;
      if (fNoPeriodCounterLogModulo &&
          !(fPeriodCounterLogCounter % fNoPeriodCounterLogModulo)) {
        unsigned long long diff;
        diff =
            (fAddEventIDCounter.fNr - fLastLogMessageEventIDCounter.fNr) >> 36;
        LOG(AliHLTLog::kImportant, "AliHLTCRORC1Handler::PollForEvent",
            "Period counter")
            << "Period counter set to " << AliHLTLog::kDec
            << (fAddEventIDCounter.fNr >> 36) << " (" << fAddEventIDCounter.fNr
            << ") (increased by " << diff
            << " since last message (last increment: " << cnt
            << ", last delta: " << delta << " microsec.))." << ENDLOG;
        fLastLogMessageEventIDCounter = fAddEventIDCounter;
      }
      ++fPeriodCounterLogCounter;
      // fAddEventIDCounter &= 0xFFFFFFFFFF000000ULL;
    }
    if (eventID.fType == kAliEventTypeStartOfRun) {
      fLastLogMessageEventIDCounter.fNr = fAddEventIDCounter.fNr = 0;
      gettimeofday(&fLastEventIDWrapAround, NULL);
    }
    fLastRORCEventID = eventID;
    eventID.fNr |= fAddEventIDCounter.fNr;
  }

  ed.fEventID = eventID;

  ++fEventCounter;

  pthread_mutex_lock(&fEventMutex);
  fEvents.Add(ed);
  pthread_mutex_unlock(&fEventMutex);

  return 0;
}

int AliHLTCRORC1Handler::ReleaseEvent(AliEventID_t eventID) {
  unsigned long ndx;
  pthread_mutex_lock(&fEventMutex);
  if (MLUCVectorSearcher<EventData, AliEventID_t>::FindElement(
          fEvents, &EventDataSearchFunc, eventID, ndx)) {
    EventData *edp = fEvents.GetPtr(ndx);
    LOG(AliHLTLog::kDebug,
        "AliHLTCRORC1Handler::ReleaseEvent", "Released event data")
        << "Event " << LOG_HEX_DEC(eventID) << ": " << LOG_DEC_HEX(edp->fSize)
        << " bytes - offset " << LOG_HEX_DEC(edp->fOffset)
        << " - reference " << LOG_DEC_HEX(edp->reference) << "." << ENDLOG;

    if (fEventStream->releaseEvent(edp->reference)) {
      LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::ReleaseEvent",
          "releaseEvent")
          << "invalid event reference, cannot be released: "
          << "Event " << LOG_HEX_DEC(eventID) << ": " << LOG_DEC_HEX(edp->fSize)
          << " bytes - offset " << LOG_HEX_DEC(edp->fOffset) << " - reference "
          << LOG_DEC_HEX(edp->reference) << "." << ENDLOG;
    }

    fEvents.Remove(ndx);
    pthread_mutex_unlock(&fEventMutex);
    return 0;
  } else {
    LOG(AliHLTLog::kError,
        "AliHLTCRORC1Handler::ReleaseEvent", "Event not found")
        << "Event " << LOG_HEX_DEC(eventID)
        << " to be released could not be found in list. Giving up..."
        << ENDLOG;
    pthread_mutex_unlock(&fEventMutex);
    return EIO;
  }
  return 0;
}

// not needed on C-RORC
int AliHLTCRORC1Handler::FlushEvents() { return 0; }

int
AliHLTCRORC1Handler::ReadRORCLinkStatus(struct AliHLTRORCLinkStatus &status) {
  if (fEventStream == NULL) {
    return ENODEV;
  }
  /**
   * Optical link status is only relevant if the data is actually
   * coming from a remote source. Otherwise return default values.
   **/
  status.transceiver_up = true;
  status.ddl_up = true;
  status.transceiver_errors = false;
  status.ddl_xoff_count = 0;
  status.gtx_rxlossofsync_count = 0;
  status.gtx_rxdispartity_count = 0;
  status.gtx_rxnotintable_count = 0;
  status.dma_stall_flags = fEventStream->m_channel->readAndClearPtrStallFlags();
  status.dma_event_count = fEventStream->m_channel->eventCount();
  status.fcf_altro_channel_errors = fFcfAltroChannelErrorCount;
  status.fcf_rcu_protocol_errors = fFcfRcuProtocolErrorCount;
  status.events_in_ringbuffer = fEventStream->getRingbufferFillCount();

  bool report_gtx_status = false;
  if (fDiuInstance && fIsRemoteDataSource) {
    status.ddl_up = fDiuInstance->linkUp();
    /** DIU: number of clock cycles where HLT was sending XOFF to DAQ **/
    status.ddl_xoff_count = fDiuInstance->getDdlDeadtime();
    report_gtx_status = true;
  }
  if (fSiuInstance && !fHLTOutDmaTestMode) {
    /** SIU: number of clock cycles where DAQ was sending XOFF to HLT **/
    status.ddl_xoff_count = fSiuInstance->getDdlDeadtime();
    report_gtx_status = true;
  }

  if (report_gtx_status) {
    status.transceiver_up = fGtxInstance->isLinkUp();
    if (status.transceiver_up) {
      status.gtx_rxlossofsync_count = fGtxInstance->getRxLossOfSyncErrorCount();
      status.gtx_rxdispartity_count = fGtxInstance->getDisparityErrorCount();
      status.gtx_rxnotintable_count = fGtxInstance->getRxNotInTableErrorCount();

      if (status.gtx_rxlossofsync_count || status.gtx_rxdispartity_count ||
          status.gtx_rxnotintable_count) {
        status.transceiver_errors = true;
        fGtxInstance->clearErrorCounters();
      }
    }
  }

  return 0;
}

bool AliHLTCRORC1Handler::ReleaseBlock(EventData *edp) { return true; }

void AliHLTCRORC1Handler::InitializeHWCoProcParams() {
  /**
   * set all FCF parameters. If the values were not overridden by command line
   * arguments of CRORCPublisher the default values from the AliHLTCRORC1Handler
   * constructor are used.
   **/
  fFastClusterFinder->setSinglePadSuppression(
      fSinglePadSuppression == true ? 1 : 0);
  fFastClusterFinder->setBypassMerger(fBypassMerger == true ? 1 : 0);
  fFastClusterFinder->setDeconvPad(fDeconvPad == true ? 1 : 0);
  fFastClusterFinder->setSingleSeqLimit(fSingleSeqLimit);
  fFastClusterFinder->setClusterLowerLimit(fClusterLowerLimit);
  fFastClusterFinder->setMergerDistance(fMergerDistance);
  fFastClusterFinder->setMergerAlgorithm(fMergerAlgorithmFollow == true ? 1
                                                                        : 0);
  fFastClusterFinder->setChargeTolerance(fChargeTolerance);
  fFastClusterFinder->setBranchOverride(fBranchOverride);
  if (fFwDate > 0x20170000) {
    // these options are only available in firmware images newer than 2017
    fFastClusterFinder->setNoiseSuppression(fNoiseSuppression);
    fFastClusterFinder->setNoiseSuppressionNeighbor(fNoiseSuppressionNeighbor);
    fFastClusterFinder->setNoiseSuppressionMinimum(fNoiseSuppressionMinimum);
    fFastClusterFinder->setClusterQmaxLowerLimit(fClusterQmaxLowerLimit);
    fFastClusterFinder->setTagBorderClusters(fTagBorderClusters);
    fFastClusterFinder->setCorrectEdgeClusters(fCorrectEdgeClusters);
    fFastClusterFinder->setTagDeconvolutedClusters(fTagDeconvolutedClusters);
  }
}

void AliHLTCRORC1Handler::SignalErrorToRORC() {
  LOG(AliHLTLog::kError,
      "AliHLTCRORC1Handler::SignalErrorToRORC", "Not Implemented")
      << "SignalErrorToRORC() is not implemented on C-RORC" << ENDLOG;
}

void AliHLTCRORC1Handler::InsertBlock(TBlockData &blocks, BlockData &block) {
  LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::InsertBlock", "Not Implemented")
      << "InsertBlock() is not implemented on C-RORC" << ENDLOG;
}

int AliHLTCRORC1Handler::SetRORCBlockSize(uint32_t rorcBlockSize) {
  fRORCBlockSizeOverrideSet = true;
  fRORCBlockSizeOverride = rorcBlockSize;
  if (fEventStream != NULL && fEventStream->m_reportBuffer != NULL) {
    LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::SetRORCBlockSize",
        "ReportBuffer is already initialized")
        << "Cannot set Block Size. "
        << "Call SetRORCBlockSize() before InitializeRORC()!" << ENDLOG;
    return EEXIST;
  }
  return 0;
}

int AliHLTCRORC1Handler::SubmitEvent(
    AliEventID_t eventID, const AliHLTSubEventDescriptor::BlockData &block,
    const AliHLTSubEventDataDescriptor *, const AliHLTEventTriggerStruct *) {
  if (!fHLTOutDmaTestMode && !fSiuInstance->linkOpen()) {
    LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::SubmitEvent",
        "Check Link State")
        << "SIU link is not open - no RDYRX received from DAQ" << ENDLOG;
    return EIO;
  }

  if (block.fShmKey.fShmType != kLibrorcShmType) {
    LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::SubmitEvent", "Check SHM Type")
        << "buffer is no librorc buffer - cannot submit event to RORC"
        << ENDLOG;
    return EIO;
  }

  uint32_t BlockDeviceId = AliHLTShmIDGetLibrorcDeviceId(block.fShmKey);
  uint32_t BlockBufferId = AliHLTShmIDGetLibrorcBufferId(block.fShmKey);
  uint32_t BlockBufferOvermap = AliHLTShmIDGetLibrorcOvermapFlag(block.fShmKey);

  if (BlockDeviceId != fEventStream->m_dev->getDeviceId()) {
    LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::SubmitEvent", "Check SHM ID")
        << "block buffer does not belong to selected RORC device - cannot "
           "submit event to RORC" << ENDLOG;
    return EIO;
  }

  bool reinit_outbuffer = true;
  if( fOutBuffer ) {
      // check if we can reuse the previous buffer instance
      if( fOutBuffer->getID() == BlockBufferId ) {
          reinit_outbuffer = false;
      }
  }

  if (reinit_outbuffer) {
    if (fOutBuffer) {
      delete fOutBuffer;
      fOutBuffer = NULL;
    }
    try {
      fOutBuffer = new librorc::buffer(fEventStream->m_dev, BlockBufferId,
                                       BlockBufferOvermap);
    } catch (int e) {
      LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::SubmitEvent",
          "Connect to buffer")
          << librorc::errMsg(e) << ": fShmKey=" << block.fShmKey.fKey.fID
          << ", librorc dev:" << BlockDeviceId << " buf:" << BlockBufferId
          << " om:" << BlockBufferOvermap << ENDLOG;
      return EIO;
    }
  }

  std::vector<librorc::ScatterGatherEntry> event_sglist;
  if (!fOutBuffer->composeSglistFromBufferSegment(block.fOffset, block.fSize,
                                                   &event_sglist)) {
    LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::SubmitEvent",
        "composeSglistFromBufferSegment")
        << "Failed to compose scatter-gather-list for event:"
        << ", librorc dev:" << BlockDeviceId << " buf:" << BlockBufferId
        << " om:" << BlockBufferOvermap
        << ", offset=" << LOG_HEX_DEC(block.fOffset)
        << " size=" << LOG_HEX_DEC(block.fSize) << ENDLOG;
    return EIO;
  }

  /**
   * NOTE: no need to check if sufficient FIFO space is available because only
   * one event is submitted at a time with the current implementation.
   * If Queueing is enabled in the future, add a check for this here.
   **/
  fEventStream->m_channel->announceEvent(event_sglist);

  pthread_mutex_lock(&fEventMutex);
  fEvent = eventID;
  pthread_mutex_unlock(&fEventMutex);
  return 0;
}

int AliHLTCRORC1Handler::PollForDoneEvent() {
  librorc::EventDescriptor *report = NULL;
  const uint32_t *event = 0;
  uint64_t librorcEventReference = 0;

  if (fEventStream->getNextEvent(&report, &event, &librorcEventReference)) {

    // optional: update librorc-internal statistics
    fEventStream->updateChannelStatus(report);
    bool cmpl_failed = (((report->calc_event_size) >> 30)!=0);
    int release_result = fEventStream->releaseEvent(librorcEventReference);

    if (cmpl_failed) {
      LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::PollForDoneEvent",
          "Completion Status")
          << "PCIe completion failed, event may only partly be transmitted"
          << ENDLOG;
      return EIO;
    }

    if (release_result != 0) {
      LOG(AliHLTLog::kError, "AliHLTCRORC1Handler::PollForDoneEvent",
          "releaseEvent")
          << "invalid event reference, cannot be released." << ENDLOG;
      return EIO;
    }

    pthread_mutex_lock(&fEventMutex);
    if (fEvent != AliEventID_t()) {
      fEvent = AliEventID_t();
      pthread_mutex_unlock(&fEventMutex);
      return 0;
    } else {
      LOG(AliHLTLog::kWarning,
          "AliHLTCRORC1Handler::PollForDoneEvent", "check AliEventID_t")
          << "unexpected fEvent " << fEvent << "matching AliEventID_t()"
          << ENDLOG;
      pthread_mutex_unlock(&fEventMutex);
      return EIO;
    }
  }
  return EAGAIN;
}
