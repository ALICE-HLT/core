/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck,
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$
**
***************************************************************************
*/

#include "AliHLTCRORCPublisher.hpp"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTSubEventDescriptor.hpp"
#include "AliHLTEventDataType.h"
#include "AliHLTLog.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTBufferManager.hpp"
#include "AliHLTShmManager.hpp"
#include "AliHLTStackTrace.hpp"
#include "AliHLTDDLHeader.hpp"
#include "AliHLTDDLHeaderData.h"
#include "AliHLTEventWriter.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

/**
 * Threshold for XOFF reporting at end of run in number of clock cycles.
 **/
#define DDL_XOFF_TOTAL_THRESHOLD 100

/**
 * Minimum time to wait between log outputs about FCF/RCU protocol errors in us.
 **/
#define FCF_ERROR_LOG_WAITTIME_US 60000000 // 1 min



#ifdef PROFILING
#define STDHISTO(histo) histo(0,10000,1)
#define ADDHISTO(histos,name) histos.insert( histos.end(), &name )
#else
#define STDHISTO(histo)
#define ADDHISTO(histos,name)
#endif


#define RORCCORRUPT_DATAID             (((AliUInt64_t)'CRPT')<<32 | 'DATA')

inline long long timediff_us (struct timeval from, struct timeval to) {
    return ((long long)(to.tv_sec - from.tv_sec) * 1000000LL +
            (long long)(to.tv_usec - from.tv_usec));
}


AliHLTCRORCPublisher::AliHLTCRORCPublisher( const char* name, int eventSlotsPowerOfTwo ):
    AliHLTDetectorPublisher( name, eventSlotsPowerOfTwo )
#ifndef OLD_EVENTTRIGGER_DATA
,
    fHLTEventTriggerDataCache( eventSlotsPowerOfTwo ),
    fHLTEventTriggerData( 8, eventSlotsPowerOfTwo )
#endif
#ifdef PROFILING
    ,
    STDHISTO( fAnnounceEventTimeSetupHisto ),
    STDHISTO( fAnnounceEventTimeFindSubscribersHisto ),
    STDHISTO( fAnnounceEventTimeStoreEventHisto ),
    STDHISTO( fAnnounceEventTimeFindEventSlotHisto ),
    STDHISTO( fAnnounceEventTimeAnnounceEventHisto ),
    STDHISTO( fAnnounceEventTimeCallbacksHisto ),
    STDHISTO( fAnnounceEventTimeSetTimerHisto ),
    STDHISTO( fAnnounceEventTimeFindTimerHisto )
#endif
    {
    fRORCHandler = NULL;
    fETP = &fETS;
    fETP->fHeader.fLength = sizeof(AliHLTEventTriggerStruct);
    fETP->fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
    fETP->fHeader.fSubType.fID = 0;
    fETP->fHeader.fVersion = 1;
    fETP->fDataWordCount = 0;

    fDoSleep = false;
    fSleepTime = 10000;

    fQuitEventLoop = false;

    fShmPtr = NULL;
    fShmSize = 0;

    fOldestEventBirth = 0;

    fEventDataType.fID = UNKNOWN_DATAID;
    fEventDataOrigin.fID = UNKNOWN_DATAORIGIN;
    fEventDataSpecification = ~(AliUInt32_t)0;
    pthread_mutex_init( &fRORCHandlerMutex, NULL );

#ifndef OLD_EVENTTRIGGER_DATA
    pthread_mutex_init( &fHLTEventTriggerDataLock, NULL );
    fDisableHLTEventTriggerData = false;
#endif

    gettimeofday( &fLastNoEventFoundLog, NULL );

    fPublishCorruptEvents = false;
    fSuppressHWSuppressedEvents = false;
    fSuppressGoodEvents = false;
    fTruncateCorruptEvents = false;

    fReceivedEvents = false;

    fStartOfRunEventReceived = false;
    fEndOfRunEventReceived = false;
    fDataEventWithoutStartOfRunWarning = false;

    fEnableCorruptEventsAutoDump = true;
    fCorruptEventsAutoDumpLimit = 100;
    fCorruptEventsAutoDumpPrefix = ".";
    fCorruptEventsAutoDumpCnt = 0;

    fLastStatusXoffCount = 0;
    fTotalDdlXoffCount = 0;
    fXoffActiveCount = 0;
    fLastReportedXoffCount = 0;
    fEorXoffStats = false;
    fLastFcfRcuProtocolErrorCnt = 0;
    fLastFcfAltroChannelErrorCnt = 0;
    fLastFcfErrorLog = {0, 0};
    fTotalNopeCount = 0;

    fShmOffset = 0ul;

    fNoStrictSOR = false;
    fLastTimerExpire = fLastNoEventFoundLog;
    fLastEventCount = 0;

    ADDHISTO( fAnnounceEventTimeSetupHistos, fAnnounceEventTimeSetupHisto );
    ADDHISTO( fAnnounceEventTimeFindSubscribersHistos, fAnnounceEventTimeFindSubscribersHisto );
    ADDHISTO( fAnnounceEventTimeStoreEventHistos, fAnnounceEventTimeStoreEventHisto );
    ADDHISTO( fAnnounceEventTimeFindEventSlotHistos, fAnnounceEventTimeFindEventSlotHisto );
    ADDHISTO( fAnnounceEventTimeAnnounceEventHistos, fAnnounceEventTimeAnnounceEventHisto );
    ADDHISTO( fAnnounceEventTimeCallbacksHistos, fAnnounceEventTimeCallbacksHisto );
    ADDHISTO( fAnnounceEventTimeSetTimerHistos, fAnnounceEventTimeSetTimerHisto );
    ADDHISTO( fAnnounceEventTimeFindTimerHistos, fAnnounceEventTimeFindTimerHisto );

    // disable suppression of similar LOG messages
    gLog.SetStackTraceComparisonDepth(0);
    }

AliHLTCRORCPublisher::~AliHLTCRORCPublisher()
    {
    pthread_mutex_destroy( &fRORCHandlerMutex );
#ifndef OLD_EVENTTRIGGER_DATA
    pthread_mutex_destroy( &fHLTEventTriggerDataLock );
#endif
    }

void AliHLTCRORCPublisher::StopPublishing()
    {
    //fRORCHandler->DeactivateRORC();
    QuitEventLoop();
    fEventAnnouncer.Quit();
    }

bool AliHLTCRORCPublisher::MustContinue()
    {
    if ( !fQuitEventLoop )
	return false; // False if loop has not to be ended, there is no need to continue with publishing (no urgent end-of-event which has to be honored...)
    if ( fEndOfRunEventReceived )
        return false;
    return true;
    }

void AliHLTCRORCPublisher::DumpEvent(AliHLTSubEventDataDescriptor *&sbevent,
                                     AliEventID_t eventID,
                                     AliUInt64_t dataOffset,
                                     AliUInt64_t dataSize) {
  LOG(AliHLTLog::kWarning, "AliHLTCRORCPublisher::DumpEvent",
      "Dumping corrupt Event")
      << "Dumping corrupt Event 0x" << AliHLTLog::kHex << eventID << " ("
      << AliHLTLog::kDec << eventID << ") to disk or debugging." << ENDLOG;

  pthread_mutex_lock(&fSEDDMutex);
  sbevent = fDescriptors->GetFreeEventDescriptor(eventID, 1);
  pthread_mutex_unlock(&fSEDDMutex);

  struct timeval now;
  gettimeofday(&now, NULL);
  sbevent->fEventID = eventID;
  sbevent->fEventBirth_s = now.tv_sec;
  sbevent->fEventBirth_us = now.tv_usec;
  sbevent->fOldestEventBirth_s = fOldestEventBirth;
  sbevent->fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_DATA_CORRUPT;
  sbevent->fDataBlocks[0].fBlockOffset = dataOffset;
  sbevent->fDataBlocks[0].fBlockSize = dataSize;
  sbevent->fDataBlocks[0].fShmID.fShmType = fShmKey.fShmType;
  sbevent->fDataBlocks[0].fShmID.fKey.fID = fShmKey.fKey.fID;
  sbevent->fDataBlocks[0].fDataType.fID = fEventDataType.fID;
  sbevent->fDataBlocks[0].fDataOrigin.fID = fEventDataOrigin.fID;
  sbevent->fDataBlocks[0].fDataSpecification = fEventDataSpecification;
  sbevent->fDataBlocks[0].fStatusFlags =
      ALIHLTSUBEVENTDATADESCRIPTOR_DATA_CORRUPT;
  sbevent->fDataBlocks[0].fAttributes[kAliHLTSEDBDByteOrderAttributeIndex] =
      kAliHLTLittleEndianByteOrder;
  for (int j = kAliHLTSEDBDAlignmentAttributeIndexStart;
       j <= kAliHLTSEDBDLastAlignmentAttributeIndex; j++)
    sbevent->fDataBlocks[0].fAttributes[j] = kAliHLTUnknownAttribute;
  sbevent->fDataBlocks[0].fProducerNode = fNodeID;

  AliHLTSubEventDescriptor sed(sbevent);
  std::vector<AliHLTSubEventDescriptor::BlockData> blocks;
  sed.Dereference(fShmManager, blocks);

  if (!AliHLTEventWriter::WriteEvent(eventID, blocks, sbevent, fETP, fRunNumber,
                                     fCorruptEventsAutoDumpPrefix.c_str())) {
    LOG(AliHLTLog::kError, "AliHLTCRORCPublisher::DumpEvent",
        "Cannot dump corrupt event")
        << "Cannot dump corrupt event 0x" << AliHLTLog::kHex << eventID << " ("
        << AliHLTLog::kDec << eventID << ")." << ENDLOG;
  }
  ++fCorruptEventsAutoDumpCnt;

  pthread_mutex_lock(&fSEDDMutex);
  fDescriptors->ReleaseEventDescriptor(eventID);
  pthread_mutex_unlock(&fSEDDMutex);
};


int AliHLTCRORCPublisher::WaitForEvent( AliEventID_t& eventID, AliHLTSubEventDataDescriptor*& sbevent,
					   AliHLTEventTriggerStruct*& trg )
    {
    Tick( fReceivedEvents );
    sbevent = NULL;
    trg = NULL;
    AliUInt64_t dataOffset, dataSize;
    bool dataCorrupt = false;
    bool eventSuppressed = false;
    AliUInt64_t unsuppressedDataSize;
    int ret;
// 	printf( "%s:%d\n", __FILE__, __LINE__ );
    if ( !fQuitEventLoop || MustContinue() )
	{
	do
	    {
	    pthread_mutex_lock( &fRORCHandlerMutex );
	    dataCorrupt = false;
	    ret = fRORCHandler->PollForEvent( eventID,
					      dataOffset, dataSize, dataCorrupt,
					      eventSuppressed, unsuppressedDataSize );
	    pthread_mutex_unlock( &fRORCHandlerMutex );
	    if ( ret==EFAULT )
	        {
		LOG( AliHLTLog::kFatal, "AliHLTCRORCPublisher::WaitForEvent", "Illegal address reading event" )
		  << "Illegal address encountered retrieving event from RORC event buffer. Giving up..."
		  << ENDLOG;
		if ( fStatus )
  		    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		return 0;
		}
	    if ( !ret && eventSuppressed )
		{
		// Event suppressed already in hardware
		LOG( AliHLTLog::kDebug, "AliHLTCRORCPublisher::WaitForEvent", "Suppressed event found" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ") suppred in hardware- Original event data Size: "
		    << AliHLTLog::kDec << unsuppressedDataSize << " (0x"
		    << AliHLTLog::kHex << unsuppressedDataSize << ")." << ENDLOG;
		// Does not need to be released.
		if ( fStatus )
		    fStatus->fHLTStatus.fTotalProcessedOutputDataSize += unsuppressedDataSize;
		}
	    if ( !ret && dataCorrupt && fEnableCorruptEventsAutoDump && fCorruptEventsAutoDumpCnt<fCorruptEventsAutoDumpLimit )
		{
                DumpEvent(sbevent, eventID, dataOffset, dataSize);
		}
            if ( !ret && dataCorrupt && fTruncateCorruptEvents )
                {
                // Event is marked corrupt but fTruncateCorruptEvents is enabled:
                // if CDH can be parsed, truncate the event to only its CDH.
                AliHLTDDLHeader ddlHeader;
                if ( ddlHeader.Set( (AliUInt32_t*)(fShmPtr+dataOffset) ) && ddlHeader.GetMBZ() == 0 )
                    {
                    AliUInt64_t headerSize = ddlHeader.GetHeaderSize();
                    ddlHeader.SetBlockLength( headerSize );
                    dataSize = headerSize;
                    dataCorrupt = false;
                    LOG( AliHLTLog::kWarning, "AliHLTCRORCPublisher::WaitForEvent", "Corrupt Event")
                        << "Received corrupt event, but CDH looks OK - truncating event 0x"
                        << AliHLTLog::kHex << eventID << AliHLTLog::kDec << " to its CDH." << ENDLOG;
                    }
                else
                    {
                    LOG(AliHLTLog::kError, "AliHLTCRORCPublisher::WaitForEvent", "Corrupt Event")
                        << "Failed to parse CDH of corrupt event, discarding event..." << ENDLOG;
                    }

                }

	    if ( !ret && ( (!dataCorrupt && !fSuppressGoodEvents) || (dataCorrupt && fPublishCorruptEvents) || (eventSuppressed && !fSuppressHWSuppressedEvents) ) )
		{
		LOG( AliHLTLog::kInformational, "AliHLTCRORCPublisher::WaitForEvent", "Event found" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ") found: Data Offset " << dataOffset << " (0x" << AliHLTLog::kHex
		    << dataOffset << ") - Data Size: " << AliHLTLog::kDec << dataSize << " (0x"
		    << AliHLTLog::kHex << dataSize << ")." << ENDLOG;
		fReceivedEvents = true;
		if ( eventSuppressed )
		    {
		    AliHLTDDLHeader ddlHeader;
		    if ( !ddlHeader.Set( (AliUInt32_t*)(fShmPtr+dataOffset) ) )
			{
			LOG( AliHLTLog::kError, "AliHLTCRORCPublisher::WaitForEvent", "Error reading DDL header" )
			    << "Error reading DDL header number. Unknown header version or out of memory. Header version: "
			    << AliHLTLog::kDec
			    << (unsigned)AliHLTGetDDLHeaderVersion(fShmPtr+dataOffset) << " - header version byte index: "
			    << (unsigned)DDLHEADERVERSIONINDEX << " - header size: " << AliHLTGetDDLHeaderSize(fShmPtr+dataOffset)
			    << ENDLOG;
			}
		    else
			{
			// Set total block length in header to only header size for suppressed events (which are reduced to the header)
			ddlHeader.SetBlockLength( ddlHeader.GetHeaderSize() );
			}
		    }
		//fRORCHandler->ReleaseEvent( eventID );
		}
	    else if ( !ret && ( (dataCorrupt && !fPublishCorruptEvents) || (!dataCorrupt && fSuppressGoodEvents) || (eventSuppressed && fSuppressHWSuppressedEvents) ) )
		{
		if ( eventSuppressed )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTCRORCPublisher::WaitForEvent", "Event supressed in RORC" )
			<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ") already suppressed in RORC: Data Offset " << dataOffset << " (0x" << AliHLTLog::kHex
			<< dataOffset << ") - Data Size: " << AliHLTLog::kDec << dataSize << " (0x"
			<< AliHLTLog::kHex << dataSize << "). - Will not be published" << ENDLOG;
		    }
		else if ( dataCorrupt )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTCRORCPublisher::WaitForEvent", "Corrupt Event found" )
			<< "Corrupt Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ") found: Data Offset " << dataOffset << " (0x" << AliHLTLog::kHex
			<< dataOffset << ") - Data Size: " << AliHLTLog::kDec << dataSize << " (0x"
			<< AliHLTLog::kHex << dataSize << "). - Will not be published" << ENDLOG;
		    }
		else
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTCRORCPublisher::WaitForEvent", "Suppressing good event" )
			<< "Good Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ") suppressed: Data Offset " << dataOffset << " (0x" << AliHLTLog::kHex
			<< dataOffset << ") - Data Size: " << AliHLTLog::kDec << dataSize << " (0x"
			<< AliHLTLog::kHex << dataSize << "). - Will not be published" << ENDLOG;
		    }
		fRORCHandler->ReleaseEvent( eventID );
		}

	    if ( fDoSleep && ( ret == EAGAIN ) )
                {
		usleep( fSleepTime );
                }
            if ( ret == ENOMSG )
                {
                // non-standard event received, e.g. NOPE - already handled by fRORCHandler
                // Continue to poll for regular events...
                ret = EAGAIN;
                fTotalNopeCount++;
                }
            if ( !ret )
                UpdateEventAge();
	    }
	while ( !fQuitEventLoop && ( ret==EAGAIN || (dataCorrupt && !fPublishCorruptEvents) || (!dataCorrupt && fSuppressGoodEvents) ) );
	if ( fStatus )
// 	printf( "%s:%d\n", __FILE__, __LINE__ );
	if ( ret )
	    return 0;
// 	printf( "%s:%d\n", __FILE__, __LINE__ );
	if ( fQuitEventLoop )
	    return 1;
// 	printf( "%s:%d\n", __FILE__, __LINE__ );

	int cnt = 1;
	vector<AliHLTSubEventDataBlockDescriptor> addBlocks;
	if ( eventID.fType==kAliEventTypeStartOfRun )
	    {
	    LOG( AliHLTLog::kImportant, "AliHLTCRORCPublisher::WaitForEvent", "Start-Of-Data/-Run event received" )
		<< "Start-Of-Data/-Run event 0x" << AliHLTLog::kHex << eventID << " ("
		<< AliHLTLog::kDec << eventID << ") received." << ENDLOG;
	    fStartOfRunEventReceived = true;
	    CreateSORBlocks( addBlocks );
	    }
	else if ( eventID.fType==kAliEventTypeSync )
	    {
	    LOG( AliHLTLog::kImportant, "AliHLTCRORCPublisher::WaitForEvent", "SYNC event received" )
		<< "SYNC event 0x" << AliHLTLog::kHex << eventID << " ("
		<< AliHLTLog::kDec << eventID << ") received." << ENDLOG;
	    }
	else if ( eventID.fType==kAliEventTypeEndOfRun )
	    {
	    LOG( AliHLTLog::kImportant, "AliHLTCRORCPublisher::WaitForEvent", "End-Of-Data/-Run event received" )
		<< "End-Of-Data/-Run event 0x" << AliHLTLog::kHex << eventID << " ("
		<< AliHLTLog::kDec << eventID << ") received." << ENDLOG;
	    fEndOfRunEventReceived = true;
	    CreateEORBlocks( addBlocks );
	    }
	else if ( !fStartOfRunEventReceived && !fDataEventWithoutStartOfRunWarning && fAliceHLT )
	    {
	    LOG( fNoStrictSOR ? AliHLTLog::kInformational : AliHLTLog::kWarning, "AliHLTCRORCPublisher::WaitForEvent", "Data event without Start-Of-Data/-Run event" )
		<< "First data event 0x" << AliHLTLog::kHex << eventID << " ("
		<< AliHLTLog::kDec << eventID << ") received without preceeding Start-Of-Data/-Run event." << ENDLOG;
	    fDataEventWithoutStartOfRunWarning = true;
	    }

	pthread_mutex_lock( &fSEDDMutex );
	sbevent = fDescriptors->GetFreeEventDescriptor( eventID, cnt+GetAdditionalBlockCount()+addBlocks.size() );
	pthread_mutex_unlock( &fSEDDMutex );

// 	printf( "%s:%d\n", __FILE__, __LINE__ );

	struct timeval now;
	gettimeofday( &now, NULL );
	sbevent->fEventID = eventID;
	sbevent->fEventBirth_s = now.tv_sec;
	sbevent->fEventBirth_us = now.tv_usec;
	sbevent->fOldestEventBirth_s = fOldestEventBirth;
	if ( !dataCorrupt )
	    sbevent->fStatusFlags = 0;
	else
	    sbevent->fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_DATA_CORRUPT;
	if ( eventID.fType==kAliEventTypeStartOfRun || eventID.fType==kAliEventTypeEndOfRun || eventID.fType==kAliEventTypeSync)
	    sbevent->fStatusFlags |= ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST;
	if ( fStatus && !eventSuppressed )
	    fStatus->fHLTStatus.fTotalProcessedOutputDataSize += dataSize;
	unsigned long n = 0;
	if ( dataSize )
	    {
	    sbevent->fDataBlocks[n].fBlockOffset = dataOffset;
	    sbevent->fDataBlocks[n].fBlockSize = dataSize;
	    sbevent->fDataBlocks[n].fShmID.fShmType = fShmKey.fShmType;
            sbevent->fDataBlocks[n].fShmID.fKey.fID = fShmKey.fKey.fID;
	    sbevent->fDataBlocks[n].fDataType.fID = fEventDataType.fID;
	    sbevent->fDataBlocks[n].fDataOrigin.fID = fEventDataOrigin.fID;
	    sbevent->fDataBlocks[n].fDataSpecification = fEventDataSpecification;
	    if ( !dataCorrupt )
		sbevent->fDataBlocks[n].fStatusFlags = 0;
	    else
		sbevent->fDataBlocks[n].fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_DATA_CORRUPT;
	    //sbevent->fDataBlocks[n].fByteOrder = kAliHLTNativeByteOrder;
	    sbevent->fDataBlocks[n].fAttributes[kAliHLTSEDBDByteOrderAttributeIndex] = kAliHLTLittleEndianByteOrder;
	    for ( int j = kAliHLTSEDBDAlignmentAttributeIndexStart; j <= kAliHLTSEDBDLastAlignmentAttributeIndex; j++ )
		sbevent->fDataBlocks[n].fAttributes[j] = kAliHLTUnknownAttribute;
	    sbevent->fDataBlocks[n].fProducerNode = fNodeID;
	    n++;
	    }
// 	printf( "%s:%d\n", __FILE__, __LINE__ );

	for ( unsigned addBlock=0; addBlock<GetAdditionalBlockCount(); addBlock++ )
	    {
	    if ( GetAdditionalBlock( addBlock, sbevent->fDataBlocks[n] ) )
		++n;
	    }
	if ( eventID.fType==kAliEventTypeStartOfRun || eventID.fType==kAliEventTypeEndOfRun )
	    {
	    for ( unsigned addBlock = 0; addBlock<addBlocks.size(); addBlock++ )
		{
		sbevent->fDataBlocks[n] = addBlocks[addBlock];
		n++;
		}
	    }

	sbevent->fDataBlockCount = n;
	sbevent->fDataType.fID = COMPOSITE_DATAID;
	trg = NULL;
#ifndef OLD_EVENTTRIGGER_DATA
	if ( !fAliceHLT || fDisableHLTEventTriggerData )
	    {
	    trg = fETP;
	    }
	else
	    {
	    pthread_mutex_lock( &fHLTEventTriggerDataLock );
	    AliHLTHLTEventTriggerData* hltTrg = fHLTEventTriggerDataCache.Get();
	    if ( !hltTrg )
		{
		LOG( AliHLTLog::kError, "AliHLTCRORCPublisher::WaitForEvent", "out of memory (HLTEventTriggerData)" )
		    << "Out of memory trying to allocate HLT event trigger data object - using empty trigger data." << ENDLOG;
		trg = fETP;
		}
	    else
		{
		fHLTEventTriggerData.Add( hltTrg, eventID );
		memcpy( hltTrg->fCommonHeader, (const AliUInt8_t*)( fShmPtr+dataOffset ), sizeof(hltTrg->fCommonHeader[0])*hltTrg->fCommonHeaderWordCnt );
		FillHLTEventTriggerDataDDLList( hltTrg );
		trg = &(hltTrg->fETS);
		}
	    pthread_mutex_unlock( &fHLTEventTriggerDataLock );
	    }
#else
	trg = fETP;
#endif

// 	printf( "%s:%d\n", __FILE__, __LINE__ );
	return 1;
	}
    else
	{
	return 0;
	}
    }

void AliHLTCRORCPublisher::EventFinished( AliEventID_t eventID, AliHLTSubEventDataDescriptor& sedd, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    unsigned long n = GetAdditionalBlockCount();
    if ( sedd.fDataBlockCount>=n )
	{
	for ( unsigned long ii=sedd.fDataBlockCount-n; ii<sedd.fDataBlockCount; ii++ )
	    {
	    ReleaseAdditionalBlock( sedd.fDataBlocks[ii] );
	    }
	}
    EventFinished( eventID, eventDoneData );
    }

void AliHLTCRORCPublisher::EventFinished( AliEventID_t eventID, vector<AliHLTEventDoneData*>& )
    {
    if ( eventID.fType == kAliEventTypeStartOfRun )
	{
	ReleaseSORBlocks();
	}
    else if ( eventID.fType == kAliEventTypeEndOfRun )
	{
	ReleaseEORBlocks();
	}
    pthread_mutex_lock( &fRORCHandlerMutex );
    fRORCHandler->ReleaseEvent( eventID );
    pthread_mutex_unlock( &fRORCHandlerMutex );
#ifndef OLD_EVENTTRIGGER_DATA
    pthread_mutex_lock( &fHLTEventTriggerDataLock );
    unsigned long ndx;
    if ( (fAliceHLT && !fDisableHLTEventTriggerData) || fHLTEventTriggerData.GetCnt()>0 ) // fHLTEventTriggerData.GetCnt()>0 means there are events in there, maybe because the trigger data was switched off after some time of operation.
	{
	if ( fHLTEventTriggerData.FindElement( eventID, ndx ) )
	    {
	    fHLTEventTriggerDataCache.Release( fHLTEventTriggerData.Get(ndx) );
	    fHLTEventTriggerData.Remove( ndx );
	    }
	else
	    {
	    AliHLTLog::TLogLevel lvl;
	    if ( !fDisableHLTEventTriggerData )
		lvl = AliHLTLog::kError;
	    else
		lvl = AliHLTLog::kWarning;
	    LOG( lvl, "AliHLTCRORCPublisher::EventFinished", "Cannot find HLTEventTriggerData" )
		<< "Unable to find HLT event trigger data object for event 0x"
		<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ")." << ENDLOG;
	    }
	}
    pthread_mutex_unlock( &fHLTEventTriggerDataLock );
#endif
    }


void AliHLTCRORCPublisher::QuitEventLoop()
    {
    fQuitEventLoop = true;
    }

void AliHLTCRORCPublisher::StartEventLoop()
    {
    fStartOfRunEventReceived = false;
    fEndOfRunEventReceived = false;
    fDataEventWithoutStartOfRunWarning = false;
    fRORCHandler->ActivateRORC();
    fQuitEventLoop = false;
    }

void AliHLTCRORCPublisher::EndEventLoop()
    {
    fRORCHandler->DeactivateRORC();
    if (!fEndOfRunEventReceived && fStartOfRunEventReceived && fAliceHLT) {
      LOG(AliHLTLog::kWarning,
          "AliHLTCRORCPublisher::EndEventLoop", "No End-Of-Data/-Run event")
          << "No End-Of-Data/-Run event received" << ENDLOG;
    }
    if (fEorXoffStats) {
      uint64_t xoff_ms = fRORCHandler->XoffToUs(fTotalDdlXoffCount) / 1000;
      LOG(AliHLTLog::kImportant,
          "AliHLTCRORCPublisher::EndEventLoop", "XOFF Count")
          << "XOFF to DAQ: Sent a total of " << xoff_ms << " ms XOFF to DAQ ["
          << LOG_DEC_HEX(fTotalDdlXoffCount) << "]." << ENDLOG;
    }
    }

void AliHLTCRORCPublisher::PauseProcessing()
    {
    fRORCHandler->DeactivateRORC();
    AliHLTDetectorPublisher::PauseProcessing();
    }

void AliHLTCRORCPublisher::ResumeProcessing()
    {
    AliHLTDetectorPublisher::ResumeProcessing();
    fRORCHandler->ActivateRORC();
    }

#ifdef PROFILING
#define WRITEHISTO(histoname) name=filenameprefix;name+="-"#histoname".histo";f##histoname.Write(name.c_str());

void AliHLTCRORCPublisher::WriteHistograms( const char* filenameprefix )
    {
    MLUCString name;

    WRITEHISTO( AnnounceEventTimeSetupHisto );
    WRITEHISTO( AnnounceEventTimeFindSubscribersHisto );
    WRITEHISTO( AnnounceEventTimeStoreEventHisto );
    WRITEHISTO( AnnounceEventTimeFindEventSlotHisto );
    WRITEHISTO( AnnounceEventTimeAnnounceEventHisto );
    WRITEHISTO( AnnounceEventTimeCallbacksHisto );
    WRITEHISTO( AnnounceEventTimeSetTimerHisto );
    WRITEHISTO( AnnounceEventTimeFindTimerHisto );
    }
#endif

void AliHLTCRORCPublisher::EventRateTimerExpired() {
  AliHLTDetectorPublisher::EventRateTimerExpired();

  if (!fRORCHandler) {
    return;
  }

  struct timeval now;
  gettimeofday(&now, NULL);

  struct AliHLTRORCLinkStatus status;
  if (fRORCHandler->ReadRORCLinkStatus(status) != 0) {
    return;
  }

  if (fStatus) {
    // using RECEIVE metrics for input rate and event count on C-RORC
    uint32_t eventcount_diff = status.dma_event_count - fLastEventCount;
    fStatus->fHLTStatus.fCurrentReceivedEventCount = eventcount_diff;
    double tdiff = timediff_us(fLastTimerExpire, now) / 1000000.0;
    fStatus->fHLTStatus.fCurrentReceiveRate =
        (tdiff > 0.0) ? (eventcount_diff / tdiff) : 0.0;
    fStatus->fHLTStatus.fReceivedEventCount += eventcount_diff;
    fStatus->fHLTStatus.fFreeOutputBuffer = fRORCHandler->GetFreeBufferSize();
    fLastEventCount = status.dma_event_count;
  }

  // check if DMA transfer was stalled due to full DMA buffers since the
  // last timer expiry.
  if (status.dma_stall_flags) {
    if (fStatus) {
      uint64_t pendingForAnnouncement =
          fStatus->fHLTStatus.fReceivedEventCount - fTotalNopeCount -
          fStatus->fHLTStatus.fAnnouncedEventCount;
      LOG(AliHLTLog::kWarning, "AliHLTCRORCPublisher::EventRateTimerExpired",
          "DMA Status")
          << "C-RORC DMA buffer ran full! "
          << fStatus->fHLTStatus.fPendingOutputEventCount
          << " events waiting for done signal, " << pendingForAnnouncement
          << " events in DMA buffer waiting to be announced"
          << ENDLOG;
    } else {
      LOG(AliHLTLog::kWarning,
          "AliHLTCRORCPublisher::EventRateTimerExpired", "DMA Status")
          << "C-RORC DMA buffer ran full!" << ENDLOG;
    }
  }

  if (!status.transceiver_up) {
    LOG(AliHLTLog::kError,
        "AliHLTCRORCPublisher::EventRateTimerExpired", "Transceiver Status")
        << "DDL down on transceiver level - check fiber" << ENDLOG;
  } else {
    if (status.transceiver_errors) {
      LOG(AliHLTLog::kError,
          "AliHLTCRORCPublisher::EventRateTimerExpired", "Transceiver Status")
          << "Lossy DDL connection - check fiber. Loss-Of-Sync: "
          << LOG_DEC_HEX(status.gtx_rxlossofsync_count)
          << ", Not-In-Table: " << LOG_DEC_HEX(status.gtx_rxnotintable_count)
          << ", Disparity-Errors: "
          << LOG_DEC_HEX(status.gtx_rxdispartity_count) << ENDLOG;
    }

    if (!status.ddl_up) {
      LOG(AliHLTLog::kError,
          "AliHLTCRORCPublisher::EventRateTimerExpired", "DDL Status")
          << "DDL down" << ENDLOG;
    }

    if (status.ddl_xoff_count != fLastStatusXoffCount) {
      fEorXoffStats = true;
      if (fXoffActiveCount == 0) {
        fXoffActiveStarttime = now;
        fXoffActiveStartvalue = fTotalDdlXoffCount;
      }

      /**
      * ddl_xoff_count is a 32b integer that (in worst case) could wrap
      * every ~20 seconds (0xffffffff/DDL_CLOCK_RATE). Keeping track of the
      * total deadtime in SW...
      **/
      fTotalDdlXoffCount += (status.ddl_xoff_count - fLastStatusXoffCount);

      if (fXoffActiveCount < 4 || (fXoffActiveCount & 0x3) == 0x0) {
        AliUInt64_t xoff_diff = fTotalDdlXoffCount - fLastReportedXoffCount;
        AliUInt64_t xoffTime_us = fRORCHandler->XoffToUs(xoff_diff);
        AliUInt64_t diffTime_us = (fXoffActiveCount > 0)
                                      ? timediff_us(fLastReportedXoffTime, now)
                                      : timediff_us(fLastTimerExpire, now);
        AliUInt64_t xoff_percent =
            (diffTime_us > 0) ? (100 * xoffTime_us / diffTime_us) : 100;
        AliUInt64_t totalXoffTime_ms =
            fRORCHandler->XoffToUs(fTotalDdlXoffCount) / 1000;
        LOG(AliHLTLog::kWarning, "AliHLTCRORCPublisher::EventRateTimerExpired",
            "Fifo Full Count")
            << AliHLTLog::kDec << "XOFF to DAQ - " << xoffTime_us
            << " us during the last " << diffTime_us << " us (" << xoff_percent
            << "%), " << LOG_DEC_HEX(xoff_diff)
            << " clock cycles, total value:" << totalXoffTime_ms << " ms, "
            << LOG_DEC_HEX(fTotalDdlXoffCount) << " cycles" << ENDLOG;
        fLastReportedXoffCount = fTotalDdlXoffCount;
	fLastReportedXoffTime = now;
      }
      fLastStatusXoffCount = status.ddl_xoff_count;
      fXoffActiveCount++;
    } else if (fXoffActiveCount > 0) {
      fXoffActiveCount = 0;
      uint64_t totalTime_us = timediff_us(fXoffActiveStarttime, now);
      uint64_t totalXoff_us =
          fRORCHandler->XoffToUs(fTotalDdlXoffCount - fXoffActiveStartvalue);
      LOG(AliHLTLog::kImportant,
          "AliHLTCRORCPublisher::EventRateTimerExpired", "Fifo Full Count")
          << AliHLTLog::kDec << "XOFF to DAQ cleared! We were sending "
          << (totalXoff_us / 1000) << " ms XOFF within the last "
          << (totalTime_us / 1000) << " ms." << ENDLOG;
    }
  }

  if ((status.fcf_rcu_protocol_errors != fLastFcfRcuProtocolErrorCnt ||
       status.fcf_altro_channel_errors != fLastFcfAltroChannelErrorCnt) &&
      (timediff_us(fLastFcfErrorLog, now) > FCF_ERROR_LOG_WAITTIME_US)) {
    LOG(AliHLTLog::kWarning, "AliHLTCRORCPublisher::EventRateTimerExpired",
        "HWCoProc Flags")
        << (status.fcf_rcu_protocol_errors - fLastFcfRcuProtocolErrorCnt)

        << " RCU protocol errors and "
        << (status.fcf_altro_channel_errors - fLastFcfAltroChannelErrorCnt)
        << " ALTRO channel errors since last message, "
        << status.fcf_rcu_protocol_errors << "/"
        << status.fcf_altro_channel_errors << " ("
        << (100.0 * status.fcf_rcu_protocol_errors /
            fStatus->fHLTStatus.fReceivedEventCount)
        << "%/" << (100.0 * status.fcf_altro_channel_errors /
                    fStatus->fHLTStatus.fReceivedEventCount)
        << "%) since start of run." << ENDLOG;
    fLastFcfRcuProtocolErrorCnt = status.fcf_rcu_protocol_errors;
    fLastFcfAltroChannelErrorCnt = status.fcf_altro_channel_errors;
    fLastFcfErrorLog = now;
  }

  fLastTimerExpire = now;
}

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$
**
***************************************************************************
*/
