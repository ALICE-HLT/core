#ifndef _ALIHLTDUMMYCRORCHANDLER_HPP_
#define _ALIHLTDUMMYCRORCHANDLER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTCRORCHandlerInterface.hpp"
#include "MLUCVector.hpp"
#include <pthread.h>
#include <librorc.h>

class AliHLTDummyCRORCHandler: public AliHLTCRORCHandlerInterface
    {
    public:

	AliHLTDummyCRORCHandler( unsigned long evtBufferSize, int eventSlotsExp2=-1 );
	virtual ~AliHLTDummyCRORCHandler();

	void SetHeaderSearch( bool search )
		{
		fHeaderSearch = search;
		}

	virtual int Open( uint8_t deviceId, uint32_t channelId, AliHLTRORCDataDirection dataDirection, AliHLTRORCDataSource dataSource );
	virtual int Close();

        virtual int SetEventBuffer( AliHLTShmID shmKey );

	virtual int InitializeRORC();
	virtual int DeinitializeRORC();

	virtual int InitializeFromRORC( bool first = true );

	virtual int SetOptions( const vector<MLUCString>& options, char*& errorMsg, unsigned& errorArgNr );
	virtual void GetAllowedOptions( vector<MLUCString>& options );

	virtual int WriteConfigWord( unsigned long /*wordIndex*/, AliUInt32_t /*word*/, bool /*doVerify*/ )
		{
		return 0;
		}

	virtual int ActivateRORC();
	virtual int DeactivateRORC();

	virtual int PollForEvent( AliEventID_t& eventID, 
				  AliUInt64_t& dataOffset, AliUInt64_t& dataSize,
				  bool& dataDefinitivelyCorrupt, bool& eventSuppressed, AliUInt64_t& unsuppressedDataSize );

	virtual int ReleaseEvent( AliEventID_t eventID );

	virtual int FlushEvents()
		{
		return 0;
		}


        /*********** HLT-OUT specific ***************/

        virtual int SubmitEvent
            (
             AliEventID_t eventID,
             const AliHLTSubEventDescriptor::BlockData& block,
             const AliHLTSubEventDataDescriptor* sedd,
             const AliHLTEventTriggerStruct* ets
            );

        virtual int PollForDoneEvent();

    protected:

	AliEventID_t fEventCounter;
	AliEventID_t fEvent;
	MLUCVector<AliEventID_t> fEvents;
	pthread_mutex_t fEventMutex;
	unsigned long fMaxEventCount;
	unsigned long fEventCount;
	unsigned long fEventBufferSize;
	bool fActive;
	unsigned long fOffset;

	unsigned long fBlockSize;

	bool fHeaderSearch;

	static bool EventDataSearchFunc( const AliEventID_t& ed, const AliEventID_t& searchData )
		{
		return ed == searchData;
		}

    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTDUMMYCRORCHANDLER_HPP_
