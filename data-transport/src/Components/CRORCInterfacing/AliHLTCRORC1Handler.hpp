/**
 * @file AliHLTCRORC1Handler.cpp
 * @author Heiko Engel <hengel@cern.ch>
 * @date 2014-07-17
 *
 * Based on AliHLTRORC6Handler.hpp by Timm Morten Steinbeck, 2001
 **/

#ifndef _ALIHLTCRORC1HANDLER_HPP_
#define _ALIHLTCRORC1HANDLER_HPP_

#include "AliHLTCRORCHandlerInterface.hpp"
#include "MLUCString.hpp"
#include <librorc.h>
#include <list>

class AliHLTCRORC1Handler : public AliHLTCRORCHandlerInterface {
public:
  typedef volatile AliUInt8_t AliVolatileUInt8_t;
  typedef volatile AliUInt32_t AliVolatileUInt32_t;
  typedef volatile AliUInt64_t AliVolatileUInt64_t;

  AliHLTCRORC1Handler(unsigned long dataBufferSize, int eventSlotsExp2 = -1);

  virtual ~AliHLTCRORC1Handler();

  virtual int Open(uint8_t deviceId, uint32_t channelId,
                   AliHLTRORCDataDirection dataDirection,
                   AliHLTRORCDataSource dataSource);
  virtual int Close();

  virtual int SetEventBuffer(AliHLTShmID shmKey);

  virtual int InitializeRORC();
  virtual int DeinitializeRORC();

  virtual int InitializeFromRORC(bool first = true);

  virtual int SetOptions(const vector<MLUCString> &options, char *&errorMsg,
                         unsigned &errorArgNr);
  virtual void GetAllowedOptions(vector<MLUCString> &options);

  virtual int WriteConfigWord(unsigned long wordIndex, AliUInt32_t word,
                              bool doVerify);

  virtual int ActivateRORC();
  virtual int DeactivateRORC();

  virtual int PollForEvent(AliEventID_t &eventID, AliUInt64_t &dataOffset,
                           AliUInt64_t &dataSize, bool &dataDefinitivelyCorrupt,
                           bool &eventSuppressed,
                           AliUInt64_t &unsuppressedDataSize);

  virtual int ReleaseEvent(AliEventID_t eventID);

  virtual int FlushEvents();

  virtual int ReadRORCLinkStatus(struct AliHLTRORCLinkStatus &status);

  virtual void SetNoPeriodCounterLogModulo(unsigned noPeriodCounterLogModulo) {
    fNoPeriodCounterLogModulo = noPeriodCounterLogModulo;
  }

  virtual uint64_t GetTotalBufferSize() { return fEventBufferSize; }
  virtual uint64_t GetFreeBufferSize();

  virtual int SubmitEvent(AliEventID_t eventID,
                          const AliHLTSubEventDescriptor::BlockData &block,
                          const AliHLTSubEventDataDescriptor *sedd,
                          const AliHLTEventTriggerStruct *ets);

  virtual int PollForDoneEvent();


  /***************************************************************************/

  void EnumerateEventIDs(bool enumerate) { fEnumerateEventIDs = enumerate; }

  void SetHeaderSearch(bool search) { fHeaderSearch = search; }

  void SetReportWordConsistencyCheck(bool check) {
    fReportWordConsistencyCheck = check;
  }

  void SetNoFlowControl(bool noFlowControl) { fNoFlowControl = noFlowControl; }

  int SetRORCBlockSize(uint32_t rorcBlockSize);

  void SetEventSuppression(unsigned short suppression) {
    fEventSuppression_Exp2 = suppression;
    fEventSuppressionActive = true;
  }

  void SetEnableHWCoProc(bool enableHWCoProc) {
    fEnableHWCoProc = enableHWCoProc;
  }
  // FCF settings
  void SetSinglePadSuppression(bool singlePadSuppression) {
    fSinglePadSuppression = singlePadSuppression;
  }
  void SetBypassMerger(bool bypassMerger) {
    fBypassMerger = bypassMerger;
  }
  void SetDeconvPad(bool deconvPad) {
    fDeconvPad = deconvPad;
  }
  void SetClusterLowerLimit(AliUInt16_t clusterLowerLimit) {
    fClusterLowerLimit = clusterLowerLimit;
  }
  void SetSingleSeqLimit(AliUInt8_t singleSeqLimit) {
    fSingleSeqLimit = singleSeqLimit;
  }

  void SetMergerDistance(AliUInt8_t mergerDistance) {
    fMergerDistance = mergerDistance;
  }

  void SetMergerAlgorithmFollow(AliUInt8_t mergerAlgorithmFollow) {
    fMergerAlgorithmFollow = mergerAlgorithmFollow;
  }

  void SetChargeTolerance(AliUInt8_t chargeTolerance) {
    fChargeTolerance = chargeTolerance;
  }

  void SetBranchOverride(AliUInt8_t ovrd) {
    fBranchOverride = ovrd;
  }

  void SetNoiseSuppression(AliUInt8_t noise_suppression) {
    fNoiseSuppression = noise_suppression;
  }

  void SetNoiseSuppressionMinimum(AliUInt8_t noise_suppression) {
    fNoiseSuppressionMinimum = noise_suppression;
  }

  void SetNoiseSuppressionNeighbor(AliUInt8_t noise_suppression) {
    fNoiseSuppressionNeighbor = noise_suppression;
  }

  void SetClusterQmaxLowerLimit(uint16_t limit) {
    fClusterQmaxLowerLimit = limit;
  }

  void SetTagBorderClusters(AliUInt32_t tag) {
    fTagBorderClusters = tag;
  }

  void SetCorrectEdgeClusters(AliUInt32_t value) {
    fCorrectEdgeClusters = value;
  }

  void SetTagDeconvolutedClusters(AliUInt32_t tag) {
    fTagDeconvolutedClusters = tag;
  }

  void SetHLTOutDmaTestModeEnable(bool enable) { fHLTOutDmaTestMode = enable; }

  void SignalErrorToRORC();

  uint32_t GetDmaEventCount();

  uint64_t GetLinkspeed() { return fLinkspeed; }
  uint64_t GetDdlClkPeriod() { return fDdlClkPeriod; }
  uint64_t XoffToUs(uint64_t value) {
    return (fDdlClkPeriod) ? (value * 1000 * 1000 / fDdlClkPeriod) : 0;
  }

protected:
  struct EventData {
    AliEventID_t fEventID;
    AliUInt32_t fOffset;
    AliUInt32_t fSize;
    AliUInt32_t fWrappedSize;
    AliUInt64_t reference;
  };

  bool ReleaseBlock(EventData *edp);

  void InitializeHWCoProcParams();
  void ConfigureEventSuppression();
  void ReadLinkspeed();

  MLUCVector<EventData> fEvents;
  pthread_mutex_t fEventMutex;

  AliUInt64_t fEventBufferSize;

  librorc::event_stream *fEventStream;
  librorc::gtx *fGtxInstance;
  librorc::diu *fDiuInstance;
  librorc::siu *fSiuInstance;
  librorc::fastclusterfinder *fFastClusterFinder;
  librorc::ddl *fRawcopyInstance;
  librorc::eventfilter *fEventFilter;
  AliHLTRORCDataSource fDataSource;
  AliHLTRORCDataDirection fDataDirection;
  bool fIsRemoteDataSource;
  bool fHLTOutDmaTestMode;
  librorc::buffer *fOutBuffer;

  bool fEventBufferSet;
  uint64_t fEventBufferId;

  struct BlockData {
    AliUInt64_t fOffset;
    AliUInt64_t fSize;
  };

  typedef list<BlockData> TBlockData;
  typedef TBlockData::iterator TBlockIterator;

  static bool EventDataSearchFunc(const EventData &ed,
                                  const AliEventID_t &searchData) {
    return ed.fEventID == searchData;
  }

  bool fEnumerateEventIDs;
  AliEventID_t fNextEventID;
  AliEventID_t fEvent;

  AliEventID_t fLastRORCEventID;
  AliEventID_t fLastWrapRORCEventID;
  AliEventID_t fAddEventIDCounter;
  struct timeval fLastEventIDWrapAround;

  bool fHeaderSearch;
  bool fReportWordConsistencyCheck;
  bool fNoFlowControl;

  AliEventID_t fNextInvalidEventID;

  uint32_t fRORCBlockSize;
  bool fRORCBlockSizeOverrideSet;
  uint32_t fRORCBlockSizeOverride;
  uint32_t fRORCBlock32bitWords;

  unsigned long long fEventCounter;

  unsigned short fEventSuppression_Exp2;
  bool fEventSuppressionActive;

  bool fEnableHWCoProc;
  // FastClusterFinder settings
  bool fSinglePadSuppression;
  bool fBypassMerger;
  bool fDeconvPad;
  AliUInt16_t fClusterLowerLimit;
  AliUInt8_t fSingleSeqLimit;
  AliUInt8_t fMergerDistance;
  bool fMergerAlgorithmFollow;
  AliUInt8_t fChargeTolerance;
  AliUInt8_t fBranchOverride;
  AliUInt8_t fNoiseSuppression;
  AliUInt8_t fNoiseSuppressionMinimum;
  AliUInt8_t fNoiseSuppressionNeighbor;
  AliUInt16_t fClusterQmaxLowerLimit;
  AliUInt32_t fTagBorderClusters;
  AliUInt32_t fCorrectEdgeClusters;
  AliUInt32_t fTagDeconvolutedClusters;
  unsigned long fFcfAltroChannelErrorCount;
  unsigned long fFcfRcuProtocolErrorCount;

  unsigned fNoPeriodCounterLogModulo;
  unsigned long fPeriodCounterLogCounter;
  AliEventID_t fLastLogMessageEventIDCounter;

  void InsertBlock(TBlockData &blocks, BlockData &block);

  bool fSodReceived;

  uint64_t fLinkspeed;
  uint64_t fDdlClkPeriod;
  uint32_t fFwDate;

private:
};

#endif // _ALIHLTCRORC1HANDLER_HPP_
