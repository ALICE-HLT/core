/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTCRORCOutWriterSubscriber.hpp"
//#include "AliHLTControlledProcessComponent.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTCRORC1Handler.hpp"
#include "AliHLTDummyCRORCHandler.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTReadoutList.hpp"
#include <vector>

class CRORCOutWriterSubscriberComponent: public AliHLTControlledDefaultSinkComponent<AliHLTCRORCOutWriterSubscriber>
    {
    public:

	CRORCOutWriterSubscriberComponent( const char* name, int argc, char** argv );
	virtual ~CRORCOutWriterSubscriberComponent() {};


    protected:

	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

	virtual bool CheckConfiguration();
	virtual void ShowConfiguration();

	virtual bool CreateParts();
	virtual void DestroyParts();
	virtual bool SetupComponents();

	bool fDoSleep;
	unsigned long fSleepTime;
	bool fRORCDeviceInfoSet;
	bool fRORCChannelInfoSet;
        AliUInt32_t fRORCDeviceID;
        AliUInt32_t fRORCChannelID;

	bool fFirstStart;

	bool fDummy;

	AliHLTCRORCHandlerInterface* fRORCHandler;

	unsigned long fHLTOutVersion;

	unsigned long fDDLID;

	unsigned long fNotDoneWarnTime_us;
	bool fNotDoneWarnRepeat;
	unsigned long fNotDoneAbortTime_us;

	bool fBroadcastEventMaster;

	bool fDoEventLog;

	bool fDumpSuspiciousEvents;

	unsigned long long fReadoutListOutputReduction;

        bool fHLTOutNoDMA;

    private:
    };


CRORCOutWriterSubscriberComponent::CRORCOutWriterSubscriberComponent( const char* name, int argc, char** argv ):
    AliHLTControlledDefaultSinkComponent<AliHLTCRORCOutWriterSubscriber>( name, argc, argv )
    {
    fDoSleep = false;
    fSleepTime = 10000;
    fRORCDeviceInfoSet = false;
    fRORCDeviceID = 0xFFFFFFFF;
    fRORCChannelID = 0xFFFFFFFF;
    fRORCHandler = NULL;
    fRORCChannelInfoSet = false;
    fFirstStart = true;
    fDummy = false;
    fHLTOutVersion = 1;
    fDDLID=~(unsigned long)0;

    fNotDoneWarnTime_us = 0;
    fNotDoneWarnRepeat = false;
    fNotDoneAbortTime_us = 0;

    fBroadcastEventMaster = false;

    fDoEventLog = false;
    fDumpSuspiciousEvents = false;
    fReadoutListOutputReduction = 0;
    fHLTOutNoDMA = false;
    }




const char* CRORCOutWriterSubscriberComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cpErr = NULL;
    if ( !strcmp( argv[i], "-device" ) )
    {
        if ( argc <= i+1 )
        { return "Missing pci device information parameter"; }
        fRORCDeviceID = strtoul( argv[i+1], &cpErr, 0 );
        if ( *cpErr )
        {
            errorArg = i+1;
            return "Error converting pci vendor specifier.";
        }
        fRORCDeviceInfoSet = true;
        i += 2;
        return NULL;
    }
    if ( !strcmp( argv[i], "-channel" ) )
    {
        if ( argc <= i+1 )
        { return "Missing RORC channel information parameter"; }
        fRORCChannelID = strtoul( argv[i+1], &cpErr, 0 );
        if ( *cpErr )
        {
            errorArg = i+1;
            return "Error converting RORC channel specifier.";
        }
        fRORCChannelInfoSet = true;
        i += 2;
        return NULL;
    }
    if ( !strcmp( argv[i], "-sleep" ) )
	{
	fDoSleep = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-restart" ) )
	{
	fFirstStart = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-dummydevice" ) )
	{
	fDummy = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-sleeptime" ) )
	{
	if ( argc <= i+1 )
	    return "Missing sleep time specifier.";
	fSleepTime = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting sleep time specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-hltoutinterface" ) )
	{
	if ( argc <= i+1 )
	    return "Missing HLTOut version specifier.";
	fHLTOutVersion = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting HLTOut version specifier.";
	    }
	if ( fHLTOutVersion!=1 )
	    {
	    errorArg = i+1;
	    return "Invalid HLTOut version specifier, allowed values: 1, 2.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-ddlid" ) )
	{
	if ( argc <= i+1 )
	    return "Missing DDL ID specifier.";
	fDDLID = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting DDL ID specifier.";
	    }
	if ( AliHLTReadoutList::GetDetectorID(fDDLID)!=kHLT )
	    {
	    errorArg = i+1;
	    return "Specified DDL ID is not part of HLT.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-broadcasteventmaster" ) )
	{
	fBroadcastEventMaster = true;
	++i;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventdonepollwarning" ) )
	{
	if ( argc <= i+1 )
	    return "Missing HLTOut event done poll warning interval specifier.";
	fNotDoneWarnTime_us = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting HLTOut event done poll warning interval specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-repeateventdonepollwarning" ) )
	{
	fNotDoneWarnRepeat = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventdonepolltimeout" ) )
	{
	if ( argc <= i+1 )
	    return "Missing HLTOut event done poll timeout specifier.";
	fNotDoneAbortTime_us = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting HLTOut event done poll timeout specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventlog" ) )
	{
	fDoEventLog = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-noeventlog" ) )
	{
	fDoEventLog = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-dumpsuspiciousevents" ) )
	{
	fDumpSuspiciousEvents = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-readoutlistoutput" ) )
	{
	if ( argc <= i+1 )
	    return "Missing readout list output reduction specifier.";
	fReadoutListOutputReduction = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting readout list output reduction specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-hltoutnodma" ) )
	{
	fHLTOutNoDMA = true;
	i++;
	return NULL;
	}


    
    return AliHLTControlledDefaultSinkComponent<AliHLTCRORCOutWriterSubscriber>::ParseCmdLine( argc, argv, i, errorArg );
    }

void CRORCOutWriterSubscriberComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    AliHLTControlledDefaultSinkComponent<AliHLTCRORCOutWriterSubscriber>::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-device <RORC DeviceID>: Specify the C-RORC device to use. (Required)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-channel <RORC ChannelID>: Specify the C-RORC channel to use. (Required)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-sleep: Sleep for a specified time when no event is available. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-sleeptime <sleeptime_usec>: Specify the time in microseconds to sleep (with -sleep) when no event is available. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-restart: Specify that this is a restart, do not initialize HLTOut and erase memories. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-dummydevice: Specify to simulate a HLTOut device and not use a real one. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-hltoutinterface <HLTOut-Firmware-Interface-Version>: Specify the version number of the HLTOut firmware to use. (Optional; Allowed 1; Default 1)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-ddlid <ID of used DDL>: Specify the ID of the HLT output DDL which is fed by this component. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-alloutputddlids <DDL-ID-list>: Specify a list of all DDL HLT output IDs (comma separated list of DDL IDs). (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-broadcasteventmaster: Specify that this component is the master among equal partners of the same type. (Optional, should be activated only for one component instance)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-eventdonepollwarning <event-done-poll-warning-time-microsec.>: If an event that has been submitted to the HLTOut is not reported as done in the given time interval in microseconds a warning will be printed. (Optional, 0 to disable)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-repeateventdonepollwarning: Repeat the warning if an event that has been submitted to the HLTOut is not reported as done regularly. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-eventdonepolltimeout <event-done-poll-timeout-time-microsec.>: If an event that has been submitted to the HLTOut is not reported as done in the given time interval in microseconds it will be aborted. (Optional, 0 to disable)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-eventlog: Do log every event to avoid writing events out multiple times. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-noeventlog: Do not log every event to avoid writing events out multiple times. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-dumpsuspiciousevents: Dump events to disk which have some strangeness in their data. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-readoutlistoutput <readout list output reduction specifier>: Log the readout list for events, parameters is a reduction number (first N events are shown, then N*2, N*4, N*8, ...). (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-hltoutnodma: Don't send events to the C-RORC, discard them in software (Optional)" << ENDLOG;
    }

bool CRORCOutWriterSubscriberComponent::CheckConfiguration()
    {
    if ( fMinBlockSize > fBufferSize )
	{
	LOG( AliHLTLog::kWarning, "CRORCOutWriterSubscriberComponent::CheckConfiguration", "Block size bigger than buffer size" )
	    << "Specified min. block size of " << AliHLTLog::kDec << fMinBlockSize << " bytes is greater than buffer size of "
	    << fBufferSize << " bytes. - Reducing block size to buffer size." << ENDLOG;
	fMinBlockSize = fBufferSize;
	}
    if ( !AliHLTControlledComponent::CheckConfiguration() )
	return false;
    if ( !fRORCChannelInfoSet || !fRORCDeviceInfoSet)
	{
	LOG( AliHLTLog::kError, "CRORCOutWriterSubscriberComponent::CheckConfiguration", "No device or channel specified" )
	    << "Must specify the librorc device and channel to be used (using the -device and -channel command line option.)"
	    << ENDLOG;
	return false;
	}
    return true;
    }

void CRORCOutWriterSubscriberComponent::ShowConfiguration()
{
    AliHLTControlledComponent::ShowConfiguration();
    LOG( AliHLTLog::kInformational, "CRORCOutWriterSubscriberComponent::ShowConfiguration", "PCI Device Configuration" )
        << "PCI device: Device ID: 0x" << AliHLTLog::kDec << fRORCDeviceID << " - Channel: " << fRORCChannelID
        << ENDLOG;
    LOG( AliHLTLog::kInformational, "CRORCOutWriterSubscriberComponent::ShowConfiguration", "Start Mode" )
	<< "Start Mode: " << (fFirstStart ? "First Start" : "Restart" ) << "." << ENDLOG;
    LOG( AliHLTLog::kInformational, "CRORCOutWriterSubscriberComponent::ShowConfiguration", "HLTOut Firmware Version" )
	<< "Using HLTOut firmware version " << AliHLTLog::kDec << fHLTOutVersion << "." << ENDLOG;
    if ( fDummy )
	{
	LOG( AliHLTLog::kImportant, "CRORCOutWriterSubscriberComponent::ShowConfiguration", "Dummy Mode" )
	    << "Using dummy device, no data will be sent to C-RORCs." << ENDLOG;
	}
    if ( !fDoEventLog )
	{
	LOG( AliHLTLog::kInformational, "CRORCOutWriterSubscriberComponent::ShowConfiguration", "Event log disabled" )
	    << "Event logging disabled." << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kInformational, "CRORCOutWriterSubscriberComponent::ShowConfiguration", "Event log enabled" )
	    << "Event logging enabled." << ENDLOG;
	}
    if ( fDumpSuspiciousEvents )
	{
	LOG( AliHLTLog::kInformational, "CRORCOutWriterSubscriberComponent::ShowConfiguration", "Dump suspicious events" )
	    << "Suspicious events will be dumped to disk." << ENDLOG;
	}
    if ( fHLTOutNoDMA )
	{
	LOG( AliHLTLog::kInformational, "CRORCOutWriterSubscriberComponent::ShowConfiguration", "HLT_OUT DMA disabled" )
	    << "DMA transfer to C-RORC disabled, events will be discarded in software." << ENDLOG;
	}
    }

bool CRORCOutWriterSubscriberComponent::CreateParts()
    {
    if ( !AliHLTControlledComponent::CreateParts() )
	return false;
    if ( !fDummy )
	{
	switch ( fHLTOutVersion )
	    {
	    case 1:
		fRORCHandler = new AliHLTCRORC1Handler( fMaxPreEventCountExp2 );
		break;
	    }
	}
    else
	fRORCHandler = new AliHLTDummyCRORCHandler( fBufferSize, fMaxPreEventCountExp2 );
    if ( !fRORCHandler )
	{
	LOG( AliHLTLog::kError, "CRORCOutWriterSubscriberComponent::CreateParts", "Unable to create HLTOut handler object" )
	    << "Unable to create HLTOut handler object." << ENDLOG;
	return false;
	}
    return true;
    }

void CRORCOutWriterSubscriberComponent::DestroyParts()
{
    AliHLTControlledComponent::DestroyParts();
    if( fRORCHandler )
    {
        fRORCHandler->DeinitializeRORC();
        fRORCHandler->Close();
        delete fRORCHandler;
        fRORCHandler = NULL;
    }
}

bool CRORCOutWriterSubscriberComponent::SetupComponents()
    {
    if ( !AliHLTControlledComponent::SetupComponents() )
    { return false; }
    
    AliUInt8_t* eventBuffer;
    eventBuffer = fShmManager->GetShm( fShmKey );

    int ret = fRORCHandler->Open( fRORCDeviceID, fRORCChannelID,
            AliHLTCRORCHandlerInterface::kDMAToDevice, AliHLTCRORCHandlerInterface::kLocalPcie );
    if ( ret )
    {
        LOG( AliHLTLog::kError, "CRORCPublisherComponent::SetupComponents", "Error opening RORC" )
            << "Error opening RORC device: " << strerror(ret) << " (" << AliHLTLog::kDec
            << ret << ")." << ENDLOG;
        return false;
    }

    if(!fDummy) {
      ret = dynamic_cast<AliHLTCRORC1Handler*>(fRORCHandler)->SetEventBuffer( fShmKey );
      if ( ret )
      {
          LOG( AliHLTLog::kError, "CRORCPublisherComponent::SetupComponents",
                  "SetEventBuffer" )
              << "SetEventBuffer failed with: " << strerror(ret)
              << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
          return false;
      }

      /**
       * C-RORC PCIe core seems to have problems with read-request sizes of
       * bigger than 128 byte. So we don't use the max_read_req size reported
       * by the PCIe subsystem but override with a custom packet size here.
       **/
      ret = dynamic_cast<AliHLTCRORC1Handler*>(fRORCHandler)->SetRORCBlockSize( 128 );
      if ( ret )
      {
          LOG( AliHLTLog::kError, "CRORCPublisherComponent::SetupComponents",
                  "SetRORCBlockSize" )
              << "SetRORCBlockSize failed with: " << strerror(ret)
              << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
          return false;
      }

      /**
       * HLT_OUT DMA Test Mode: fully functional DMA interface where events are
       * sent to the C-RORC but are discarded in right before they would be pushed
       * into the SIU.
       * This is automatically enabled for HLT modes A, D, or E.
       **/
      bool dma_test_mode = false;
      if (fHLTMode == 1 || fHLTMode == 4 || fHLTMode == 5) {
          LOG(AliHLTLog::kImportant,
                  "CRORCOutWriterSubscriberComponent::SetupComponents",
                  "HLT_OUT DMA Test Mode")
              << "Discarding events in the FPGA because of HLT mode: "
              << (char)('A' + (fHLTMode-1)) << "(" << AliHLTLog::kDec
              << fHLTMode << ")." << ENDLOG;
          dma_test_mode = true;
      }
      dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)
          ->SetHLTOutDmaTestModeEnable(dma_test_mode);

      /**
       * HLT_OUT No-DMA Mode: if enabled, the events are already discarded in
       * AliHLTCRORCOutWriterSubscriber::ProcessEvent() before they are transfered
       * to the C-RORC.
       * When enabled, the HLT_OUT DMA Test Mode setting has no effect.
       **/
      if ( fHLTOutNoDMA ) {
          LOG(AliHLTLog::kImportant,
                  "CRORCOutWriterSubscriberComponent::SetupComponents",
                  "HLT_OUT DMA disabled")
              << "Events are not sent to the C-RORC but discarded already "
              << "in software." << ENDLOG;
          dynamic_cast<AliHLTCRORCOutWriterSubscriber*>(fSubscriber)->Disable();
      }
    }

    ret = fRORCHandler->InitializeRORC();
    if ( ret )
    {
        LOG( AliHLTLog::kError, "CRORCPublisherComponent::SetupComponents", "Error initializing RORC" )
            << "Error initializing RORC hardware: " << strerror(ret) << " (" << AliHLTLog::kDec
            << ret << ")." << ENDLOG;
        return false;
    }

    dynamic_cast<AliHLTCRORCOutWriterSubscriber*>(fSubscriber)->SetSleep( fDoSleep );
    dynamic_cast<AliHLTCRORCOutWriterSubscriber*>(fSubscriber)->SetSleepTime( fSleepTime );
    dynamic_cast<AliHLTCRORCOutWriterSubscriber*>(fSubscriber)->SetHLTOutHandler( fRORCHandler );
    dynamic_cast<AliHLTCRORCOutWriterSubscriber*>(fSubscriber)->SetShm( fShmKey, eventBuffer, fBufferSize );
    dynamic_cast<AliHLTCRORCOutWriterSubscriber*>(fSubscriber)->SetNotDoneWarnTime( fNotDoneWarnTime_us, fNotDoneWarnRepeat );
    dynamic_cast<AliHLTCRORCOutWriterSubscriber*>(fSubscriber)->SetNotDoneAbortTime( fNotDoneAbortTime_us );
    dynamic_cast<AliHLTCRORCOutWriterSubscriber*>(fSubscriber)->BroadcastEventMaster( fBroadcastEventMaster );
    if ( fReadoutListOutputReduction )
	dynamic_cast<AliHLTCRORCOutWriterSubscriber*>(fSubscriber)->SetReadoutListOutputReduction( fReadoutListOutputReduction );
    if ( fDDLID != ~(unsigned long)0 )
	dynamic_cast<AliHLTCRORCOutWriterSubscriber*>(fSubscriber)->SetDDLID( fDDLID );

    dynamic_cast<AliHLTCRORCOutWriterSubscriber*>(fSubscriber)->SetEventLog( fDoEventLog );
    if ( fDumpSuspiciousEvents )
	dynamic_cast<AliHLTCRORCOutWriterSubscriber*>(fSubscriber)->DumpSuspiciousEvents( fDumpSuspiciousEvents );

    return true;
    }

int main( int argc, char** argv )
    {
    CRORCOutWriterSubscriberComponent procComp( "CRORCOutWriterSubscriber", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
