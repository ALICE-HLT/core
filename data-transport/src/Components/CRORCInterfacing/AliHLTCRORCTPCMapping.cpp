/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck,
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$
**
***************************************************************************
*/

#include "AliHLTCRORCTPCMapping.hpp"
#include "AliHLTCRORCHandlerInterface.hpp"
#include <algorithm> // for std::sort()
#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

static const unsigned gkPatchStartRow[] = {0, 30, 63, 90, 117, 139};

AliHLTCRORCTPCMapping::AliHLTCRORCTPCMapping(unsigned patchNr,
                                             unsigned rcuVersion)
    : fPatchNr(patchNr), fRcuVersion(rcuVersion), fLastError("Success") {
  memset(fConfigWords, 0, gkConfigWordCnt * sizeof(fConfigWords[0]));
}

AliHLTCRORCTPCMapping::~AliHLTCRORCTPCMapping() {}

int AliHLTCRORCTPCMapping::Read(const char *filename) {
  if (!filename) {
    fLastError = "Invalid filename pointer";
    return EFAULT;
  }
  std::ifstream infile(filename);
  if (infile.bad() or infile.fail()) {
    fLastError = "Error opening file";
    return EIO;
  }
  const unsigned bufSize = 4096;
  char buf[bufSize];
  MLUCString line;
  unsigned long lineCnt = 0;
  vector<AliUInt32_t> rowBranchPadHw;
  while (infile.good()) {
    line = "";
    unsigned count = 0;
    do {
      infile.getline(buf, bufSize);
      count = infile.gcount();
      if (count > 0)
        line += buf;
    } while (count == bufSize - 1 && infile.fail() && !infile.eof() &&
             !infile.bad()); // fail bit set if function stops extracing because
                             // buffer size limit is reached...
    ++lineCnt;
    if (line.Length() <= 0) {
      continue;
    }
    char lineStr[1024];
    snprintf(lineStr, 1024, "%lu", lineCnt);
    std::vector<MLUCString> tokens;
    line.Split(tokens, " \t");
    char *cpErr = NULL;
    if (tokens.size() == 0) {
      continue;
    }
    if (tokens.size() < 2) {
      fLastError = "Invalid format (missing row number or pad count) in line ";
      fLastError += lineStr;
      return EINVAL;
    }
    // first word is number of this row
    unsigned long rowNr = strtoul(tokens[0].c_str(), &cpErr, 0);
    if (*cpErr != '\0') {
      fLastError = "Invalid row number '";
      fLastError += tokens[0];
      fLastError += "' in line ";
      fLastError += lineStr;
      return EINVAL;
    }
    rowNr -= gkPatchStartRow[fPatchNr];

    // second word is number of pads in this row
    unsigned long padCnt = strtoul(tokens[1].c_str(), &cpErr, 0);
    if (*cpErr != '\0') {
      fLastError = "Invalid pad count number '";
      fLastError += tokens[1];
      fLastError += "' in line ";
      fLastError += lineStr;
      return EINVAL;
    }
    if (padCnt + 2 != tokens.size()) {
      fLastError = "Pad count does not match number of tokens in line ";
      fLastError += lineStr;
      return EINVAL;
    }

    for (unsigned pad = 0; pad < padCnt; ++pad) {
      char padStr[1024];
      snprintf(padStr, 1024, "%ud", pad);
      unsigned long hwAddress = strtoul(tokens[pad + 2].c_str(), &cpErr, 0);
      if (*cpErr != '\0') {
        fLastError = "Invalid hardware address '";
        fLastError += tokens[1];
        fLastError += "' for pad ";
        fLastError += padStr;
        fLastError += " in line ";
        fLastError += lineStr;
        return EINVAL;
      }
      unsigned long patchNr = (hwAddress & ~0xFFF) >> 12;
      if (patchNr != fPatchNr) {
        continue;
      }

      // Currently all channels are always active
      bool active = true;

      // Gain calibration identical for all pads: 1.0 as 13 bit fixed
      // point, with 1 bit position before decimal point
      AliUInt32_t gainCalib = (1 << 12);

      // first and last pad in a row are flagged as edge pads
      bool isEdgePad = (pad == 0) || (pad == (padCnt - 1));


      AliUInt32_t configWord =
          (gainCalib << 16) | ((rowNr & 0x3F) << 8) | (pad & 0xFF);
      if (active) {
        configWord |= (1 << 15);
      }
      if (isEdgePad) {
        configWord |= (1 << 29);
      }
      fConfigWords[hwAddress & 0xFFF] = configWord;
      AliUInt32_t branch = (hwAddress >> 11) & 1;
      if (fRcuVersion == 2) {
        branch = 0;
      }

      rowBranchPadHw.push_back((rowNr << 25) | (branch << 24) | (pad << 16) |
                               (hwAddress & 0xfff));
    } // pad loop
  } // while(infile.good())

  // mark pads at borders of A/B branches
  sort(rowBranchPadHw.begin(), rowBranchPadHw.end());
  int rowBranchPadLast = -2;
  for (unsigned int i = 0; i < rowBranchPadHw.size(); i++) {
    int rowBranchPad = rowBranchPadHw[i] >> 16;
    if (rowBranchPad != rowBranchPadLast + 1) {
      fConfigWords[rowBranchPadHw[i] & 0xFFF] |= (1 << 14);
      if (i > 0) {
        fConfigWords[rowBranchPadHw[i - 1] & 0xFFF] |= (1 << 14);
      }
    }
    rowBranchPadLast = rowBranchPad;
  }
  if (rowBranchPadHw.size()) {
    int lastHwAddr = rowBranchPadHw[rowBranchPadHw.size() - 1] & 0xFFF;
    fConfigWords[lastHwAddr] |= (1 << 14);
  }
  return 0;
}

int AliHLTCRORCTPCMapping::WriteData(AliHLTCRORCHandlerInterface *interface) {
  if (!interface) {
    return EFAULT;
  }
  int ret;
  for (unsigned nn = 0; nn < gkConfigWordCnt; nn++) {
    ret = interface->WriteConfigWord(nn, fConfigWords[nn], true);
    if (ret) {
      return ret;
    }
  }
  return 0;
}

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$
**
***************************************************************************
*/
