#ifndef _ALIHLTHLTOutWRITERSUBSCRIBER_HPP_
#define _ALIHLTHLTOutWRITERSUBSCRIBER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTProcessingSubscriber.hpp"
#include "AliHLTCRORCHandlerInterface.hpp"

class AliHLTCRORCOutWriterSubscriber: public AliHLTProcessingSubscriber
    {
    public:

	AliHLTCRORCOutWriterSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize,
				      AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, int eventCountAlloc = -1 );
	virtual ~AliHLTCRORCOutWriterSubscriber();

        virtual void EventRateTimerExpired();
	virtual void StopProcessing();

	void SetHLTOutHandler( AliHLTCRORCHandlerInterface* rosiHandler )
		{
		fHLTOutHandler = rosiHandler;
		}
	void SetSleep( bool do_sleep )
		{
		fDoSleep = do_sleep;
		}
	void SetSleepTime( unsigned long sleepTime )
		{
		fSleepTime = sleepTime;
		}

	void SetShm( AliHLTShmID shmKey, AliUInt8_t* ptr, AliUInt32_t size )
		{
		fShmKey = shmKey;
		fShmPtr = ptr;
		fShmSize = size;
		}

	void SetDDLID( unsigned long ddlID )
		{
		fDDLID = ddlID;
		}

	void BroadcastEventMaster( bool master )
		{
		fBroadcastEventMaster = master;
		}

	virtual void StartProcessing()
		{
		fQuitDonePoll = false;
		fHLTOutHandler->ActivateRORC();
		AliHLTProcessingSubscriber::StartProcessing();
		}

	void SimpleMode()
		{
		fSimpleMode=true;
		}

	void Disable()
		{
		fDisable = true;
		}

	void SetNotDoneWarnTime( unsigned long warn_time_us, bool repeat = false )
		{
		fNotDoneWarnTime_us = warn_time_us;
		fNotDoneWarnRepeat = repeat;
		}

	void SetNotDoneAbortTime( unsigned long abort_time_us )
		{
		fNotDoneAbortTime_us = abort_time_us;
		}

	void SetEventLog( bool eventLog )
		{
		fDoEventLog = eventLog;
		}
	
	void DumpSuspiciousEvents( bool dump = true )
		{
		fDumpSuspiciousEvents = dump;
		}

	void SetReadoutListOutputReduction( unsigned long long readoutListOutputReduction )
		{
		fReadoutListOutputReduction = readoutListOutputReduction;
		}

    protected:

	virtual bool ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, 
				   const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* etsp,
				   AliUInt8_t* outputPtr, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, AliHLTEventDoneData*& edd );
        void UpdateEventStatistics( const AliHLTSubEventDataDescriptor* sedd );

	bool fDoSleep;
	unsigned long fSleepTime;

	AliHLTCRORCHandlerInterface* fHLTOutHandler;
        AliUInt32_t fLastDdlXoffCount;
        AliUInt64_t fTotalDdlXoffCount;
        struct timeval fLastTimerExpired;

	bool fQuitDonePoll;

	AliHLTShmID fShmKey;
	AliUInt8_t* fShmPtr;
	AliUInt32_t fShmSize;

	bool fSimpleMode;

	unsigned long fDDLID;

	bool fDisable;
	bool fEorXoffStats;

	unsigned long fNotDoneWarnTime_us;
	bool fNotDoneWarnRepeat;
	unsigned long fNotDoneAbortTime_us;

	bool fBroadcastEventMaster;

	MLUCIndexedVector<AliEventID_t, AliEventID_t> fEventLog;
	bool fDoEventLog;

	bool fDumpSuspiciousEvents;

#define EVENT_BACKLOG_EXP2 15
#define EVENT_BACKLOG_SIZE (1 << EVENT_BACKLOG_EXP2)
#define EVENT_BACKLOG_MASK (EVENT_BACKLOG_SIZE-1)
	AliEventID_t fEventBackLog[EVENT_BACKLOG_SIZE];
	unsigned long fEventBackLogIndex;

	unsigned long long fReadoutListOutputCnt;
	unsigned long long fReadoutListOutputReduction;
	unsigned long long fReadoutListOutputReductionFactor;

        long long fCurrentMaxEventLifetime_us;
        long long fCurrentMinEventLifetime_us;
        long long fCurrentSumOfLifetimes_us;
        bool fClearCurrentLifetimeCounters;
        AliEventID_t fHighestEventId;

    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTHLTOutWRITERSUBSCRIBER_HPP_
