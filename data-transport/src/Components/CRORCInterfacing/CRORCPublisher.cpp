/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck,
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$
**
***************************************************************************
*/

#include "AliHLTCRORCPublisher.hpp"
//#include "AliHLTControlledProcessComponent.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTDummyCRORCHandler.hpp"
#include "AliHLTCRORC1Handler.hpp"
#include "AliHLTCRORCMapping.hpp"
#include "AliHLTCRORCTPCMapping.hpp"
#include "AliHLTLog.hpp"
#include <vector>

#define CRORC_DEFAULT_VERSION 1
#define CRORC_MAX_VERSION 1
#define RCU_DEFAULT_VERSION 1
#define RCU_MAX_VERSION 2

#define COMMAND_LINE_USAGE(msg)                                                \
  LOG(AliHLTLog::kError, "Processing Component", "Command line usage")         \
      << msg << ENDLOG;

template <typename Type>
int parse_argument_range(int argc, char *argv[], int &i, string name,
                         Type &value, unsigned long limit, int &errorArg,
                         string &errorStr) {
  string err;
  if (argc <= i + 1) {
    errorStr = "Missing value for " + name;
    return -1;
  }
  char *cpErr = NULL;
  unsigned long tmpval = strtoul(argv[i + 1], &cpErr, 0);
  if (*cpErr) {
    errorArg = i + 1;
    errorStr = "Error converting " + name;
    return -1;
  }
  if (tmpval > limit) {
    errorArg = i + 1;
    errorStr = "Value given for " + name + " exceeds limit (" +
               to_string(limit) + ")";
    return -1;
  }
  value = (Type)tmpval;
  i += 2;
  return 0;
}

class AliHLTRORCBufferManager: public AliHLTBufferManager
    {
    public:

	AliHLTRORCBufferManager():
	    fRORCHandler(NULL)
		{
		}
	~AliHLTRORCBufferManager()
		{
		}

	void SetRORCHandler( AliHLTCRORCHandlerInterface* rorcHandler )
		{
		fRORCHandler = rorcHandler;
		}

	virtual unsigned long long GetTotalBufferSize()
		{
		return fRORCHandler ? fRORCHandler->GetTotalBufferSize() : 0;
		}
	virtual unsigned long long GetFreeBufferSize()
		{
		return fRORCHandler ? fRORCHandler->GetFreeBufferSize() : 0;
		}


	virtual bool AddBuffer( AliHLTShmID, unsigned long )
		{
		return false;
		}
	virtual bool DeleteBuffer( AliHLTShmID, bool )
		{
		return false;
		}

	virtual bool CanGetBlock()
		{
		return false;
		}

	virtual bool GetBlock( AliUInt32_t&, unsigned long&,
				unsigned long& )
		{
		return false;
		}

	virtual bool AdaptBlock( AliUInt32_t, unsigned long,
				 unsigned long )
		{
		return false;
		}

	virtual bool AdaptBlock( AliUInt32_t, unsigned long,
				 unsigned long, unsigned long )
		{
		return false;
		}

	virtual bool ReleaseBlock( AliUInt32_t, unsigned long, bool /*tmp=false*/ )
		{
		return false;
		}

    protected:

	AliHLTCRORCHandlerInterface* fRORCHandler;

    };

class CRORCPublisherComponent: public AliHLTControlledSourceComponent
    {
    public:

	CRORCPublisherComponent( const char* name, int argc, char** argv );
	virtual ~CRORCPublisherComponent() {};

    protected:

	virtual AliHLTDetectorPublisher* CreatePublisher();
	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

	virtual void ResetConfiguration();
	virtual bool CheckConfiguration();
	virtual void ShowConfiguration();
	virtual void Configure();

	virtual bool CreateParts();
	virtual void DestroyParts();
	virtual bool SetupComponents();

	virtual AliHLTBufferManager* CreateBufferManager();

        string fErrorStr;

	bool fDoSleep;
	unsigned long fSleepTime;
	bool fRORCDeviceInfoSet;
	bool fRORCChannelInfoSet;
        AliUInt32_t fRORCDeviceID;
        AliUInt32_t fRORCChannelID;

	unsigned long fRORCVersion;

	bool fFirstStart;

	bool fDummy;
	bool fEnumerateEventIDs;

	bool fDDLHeaderSearch;

	bool fReportWordConsistencyCheck;

	unsigned long fOldBufferSize;

	AliHLTCRORCHandlerInterface* fRORCHandler;

	unsigned long fRORCBlockSize;

	bool fPublishCorruptEvents;
	bool fSuppressHWSuppressedEvents;
	bool fSuppressGoodEvents;
	bool fTruncateCorruptEvents;

	bool fNoFlowControl;

	unsigned short fEventSuppression_Exp2;
	bool fEventSuppressionActive;

	AliHLTEventDataType fSplitEventDataType;
	bool fSplitEventDataTypeSet;

	bool fEnableCorruptEventsAutoDump;
	unsigned long fCorruptEventsAutoDumpLimit;
	MLUCString fCorruptEventsAutoDumpDir;

	unsigned long fRCUVersion;
	AliHLTCRORCMapping* fRORCMapping;
	const char* fRORCMappingFilename;
	unsigned fRORCTPCMappingPatchNr;

	bool fSinglePadSuppression;
	bool fSinglePadSuppressionSet;
	bool fBypassMerger;
	bool fBypassMergerSet;
	bool fDeconvPad;
	bool fDeconvPadSet;
	AliUInt16_t fClusterLowerLimit;
	bool fClusterLowerLimitSet;
	AliUInt8_t fSingleSeqLimit;
	bool fSingleSeqLimitSet;
        AliUInt8_t fMergerDistance;
        bool fMergerDistanceSet;
        bool fMergerAlgorithmFollow;
        bool fMergerAlgorithmFollowSet;
        AliUInt8_t fChargeTolerance;
        bool fChargeToleranceSet;
        AliUInt8_t fNoiseSuppression;
        bool fNoiseSuppressionSet;
        AliUInt8_t fNoiseSuppressionMinimum;
        bool fNoiseSuppressionMinimumSet;
        AliUInt8_t fNoiseSuppressionNeighbor;
        bool fNoiseSuppressionNeighborSet;
        AliUInt16_t fClusterQmaxLowerLimit;
        bool fClusterQmaxLowerLimitSet;
        AliUInt32_t fTagBorderClusters;
        bool fTagBorderClustersSet;
        AliUInt32_t fCorrectEdgeClusters;
        bool fCorrectEdgeClustersSet;
        AliUInt32_t fTagDeconvolutedClusters;
        bool fTagDeconvolutedClustersSet;

	unsigned fNoPeriodCounterLogModulo;

	bool fNoStrictSOR;
        AliHLTCRORCHandlerInterface::AliHLTRORCDataSource fDataSource;

    private:
    };


CRORCPublisherComponent::CRORCPublisherComponent( const char* name, int argc, char** argv ):
  AliHLTControlledSourceComponent( name, argc, argv ), fErrorStr()
    {
    fAllowDataTypeOriginSpecSet = true;
    fDoSleep = false;
    fSleepTime = 10000;
    fRORCDeviceInfoSet = false;
    fRORCDeviceID = 0xFFFFFFFF;
    fRORCChannelID = 0xFFFFFFFF;
    fRORCHandler = NULL;
    fFirstStart = true;
    fDummy = false;
    fEnumerateEventIDs = false;
    fDDLHeaderSearch = true;
    fMaxPreBlockCount = 3;
    fOldBufferSize = 0;
    fReportWordConsistencyCheck = false;
    fRORCVersion = CRORC_DEFAULT_VERSION;
    fRORCBlockSize = 0; // 0: get default block size from PCIe subsystem
    fPublishCorruptEvents = false;
    fSuppressHWSuppressedEvents = false;
    fSuppressGoodEvents = false;
    fTruncateCorruptEvents = false;
    fNoFlowControl = false;
    fEventSuppression_Exp2 = 0;
    fEventSuppressionActive = false;
    fSplitEventDataTypeSet = false;
    fEnableCorruptEventsAutoDump = true;
    fCorruptEventsAutoDumpLimit = 100;
    fCorruptEventsAutoDumpDir = ".";
    fRCUVersion = RCU_DEFAULT_VERSION;
    fRORCMapping = NULL;
    fRORCTPCMappingPatchNr = ~0U;
    fRORCMappingFilename = NULL;
    fSinglePadSuppression = false;
    fSinglePadSuppressionSet = false;
    fBypassMerger = false;
    fBypassMergerSet = false;
    fDeconvPad = true;
    fDeconvPadSet = false;
    fClusterLowerLimit = 10;
    fClusterLowerLimitSet = false;
    fSingleSeqLimit = 0;
    fSingleSeqLimitSet = false;
    fMergerDistance = 4;
    fMergerDistanceSet = false;
    fMergerAlgorithmFollow = true;
    fMergerAlgorithmFollowSet = false;
    fChargeTolerance = 0;
    fChargeToleranceSet = false;
    fNoiseSuppression = 0;
    fNoiseSuppressionSet = false;
    fNoiseSuppressionMinimum = 0;
    fNoiseSuppressionMinimumSet = false;
    fNoiseSuppressionNeighbor = 0;
    fNoiseSuppressionNeighborSet = false;
    fClusterQmaxLowerLimit = 0;
    fClusterQmaxLowerLimitSet = false;
    fTagBorderClusters = 0;
    fTagBorderClustersSet = false;
    fCorrectEdgeClusters = 0;
    fCorrectEdgeClustersSet = false;
    fTagDeconvolutedClusters = 0;
    fTagDeconvolutedClustersSet = false;
    fNoPeriodCounterLogModulo = 1;
    fNoStrictSOR = false;
    fDataSource = AliHLTCRORCHandlerInterface::kRemoteDiu;
    fRORCChannelInfoSet = false;
    }


AliHLTDetectorPublisher* CRORCPublisherComponent::CreatePublisher()
    {
    return new AliHLTCRORCPublisher( fOwnPublisherName.c_str(), fMaxPreEventCountExp2 );
    }


const char* CRORCPublisherComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    string errorStr;
    char* cpErr = NULL;
    if ( !strcmp( argv[i], "-device" ) )
	{
	if ( argc <= i+1 )
	    return "Missing pci device information parameter";
        fRORCDeviceID = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting pci vendor specifier.";
	    }
	fRORCDeviceInfoSet = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-channel" ) )
	{
	if ( argc <= i+1 )
	    return "Missing RORC channel information parameter";
        fRORCChannelID = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting RORC channel specifier.";
	    }
	fRORCChannelInfoSet = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-sleep" ) )
	{
	fDoSleep = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-restart" ) )
	{
	fFirstStart = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-dummydevice" ) )
	{
	fDummy = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-enumerateeventids" ) )
	{
	fEnumerateEventIDs = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-disableddlheadersearch" ) )
	{
	fDDLHeaderSearch = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-sleeptime" ) )
	{
	if ( argc <= i+1 )
	    return "Missing sleep time specifier.";
	fSleepTime = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting sleep time specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-rorcinterface" ) )
	{
	if ( argc <= i+1 )
	    return "Missing RORC version specifier.";
	fRORCVersion = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting RORC version specifier.";
	    }
	if ( fRORCVersion<1 || fRORCVersion>CRORC_MAX_VERSION )
	    {
	    errorArg = i+1;
	    return "Invalid RORC version specifier, allowed values: 1.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-rcuversion" ) )
	{
	if ( argc <= i+1 )
	    return "Missing RCU version specifier.";
	fRCUVersion = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting RCU version specifier.";
	    }
	if ( fRCUVersion < 1 || fRCUVersion > RCU_MAX_VERSION)
	    {
	    errorArg = i+1;
	    return "Invalid RCU version specifier, allowed values: 1,2";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-checkreportwordconsistency" ) )
	{
	fReportWordConsistencyCheck = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-rorcblocksize" ) )
	{
	if ( argc <= i+1 )
	    return "Missing RORC block size specifier.";
	fRORCBlockSize = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting RORC block size specifier.";
	    }
	if ( fRORCBlockSize % 4 )
	    {
	    errorArg = i+1;
	    return "RORC block size must be divisible by 4.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-publishcorruptevents" ) )
	{
	fPublishCorruptEvents = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-suppressedhardwaresuppressedevents" ) )
	{
	fSuppressHWSuppressedEvents = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-suppressgoodevents" ) )
	{
	fSuppressGoodEvents = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-truncatecorruptevents" ) )
	{
	fTruncateCorruptEvents = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-noflowcontrol" ) )
	{
	fNoFlowControl = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventsuppression" ) )
	{
	if ( argc <= i+1 )
	    return "Missing event suppression specifier (exponent to 2).";
	fEventSuppression_Exp2 = strtoul( argv[i+1], &cpErr, 0 );
	fEventSuppressionActive = true;
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting event suppression specifier.";
	    }
	if ( fEventSuppression_Exp2>=16 )
	    {
	    errorArg = i+1;
	    return "Event suppression specifier must be less than 16.";
	    }
	i += 2;
	return NULL;
	}
	if ( !strcmp( argv[i], "-splitdatatype" ) )
	    {
	    if ( argc <= i +1 )
		{
		return "Missing split datatype specifier.";
		}
	    if ( strlen( argv[i+1])>8 )
		{
		return "Maximum allowed length for split datatype specifier is 8 characters.";
		}
	    AliUInt64_t tmpDT = 0;
	    unsigned tmpN;
	    for( tmpN = 0; tmpN < strlen( argv[i+1]); tmpN++ )
		{
		tmpDT = (tmpDT << 8) | (AliUInt64_t)(argv[i+1][tmpN]);
		}
	    for ( ; tmpN < 8; tmpN++ )
		{
		tmpDT = (tmpDT << 8) | (AliUInt64_t)(' ');
		}
	    fSplitEventDataType.fID = tmpDT;
	    fSplitEventDataTypeSet = true;
	    i += 2;
	    return NULL;
	    }
    if ( !strcmp( argv[i], "-disablecorruptautodump" ) )
	{
	fEnableCorruptEventsAutoDump = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-corruptautodumplimit" ) )
	{

	if ( argc <= i+1 )
	    return "Missing corrupt event auto dump event count limit.";
	fCorruptEventsAutoDumpLimit = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting corrupt event auto dump event count limit.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-corruptautodumpdir" ) )
	{
	if ( argc <= i+1 )
	    return "Missing corrupt event auto dump directory.";
	fCorruptEventsAutoDumpDir = argv[i+1];
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcmappingfile" ) )
	{
	LOG( AliHLTLog::kWarning, "", "Deprectated Option" )
	    << "Option '-tpcmappingfile' is deprecated. Please use option '-tpcmappingslice' with same syntax or '-tpcmapping' without patch number." << ENDLOG;
	if ( argc <= i+1 )
	    return "Missing patch number and TPC mapping file indicator.";
	if ( argc <= i+2 )
	    return "Missing TPC mapping file indicator.";
	fRORCTPCMappingPatchNr = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting TPC mapping patch number.";
	    }
	if ( fRORCTPCMappingPatchNr>5 )
	    {
	    errorArg = i+1;
	    return "Illegal value for TPC mapping patch number, only values from 0-5 are allowed.";
	    }
	fRORCMappingFilename = argv[i+2];
	i += 3;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcmappingslice" ) )
	{
	if ( argc <= i+1 )
	    return "Missing patch number and TPC mapping file indicator.";
	if ( argc <= i+2 )
	    return "Missing TPC mapping file indicator.";
	fRORCTPCMappingPatchNr = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting TPC mapping patch number.";
	    }
	if ( fRORCTPCMappingPatchNr>5 )
	    {
	    errorArg = i+1;
	    return "Illegal value for TPC mapping patch number, only values from 0-5 are allowed.";
	    }
	fRORCMappingFilename = argv[i+2];
	i += 3;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcmapping" ) )
	{
	if ( argc <= i+1 )
	    return "Missing TPC mapping file indicator.";
	fRORCMappingFilename = argv[i+1];
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcsinglepadsuppressiondisable" ) )
	{
	fSinglePadSuppression = false;
	fSinglePadSuppressionSet = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcsinglepadsuppressionenable" ) )
	{
	fSinglePadSuppression = true;
	fSinglePadSuppressionSet = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcbypassmergerdisable" ) )
	{
	fBypassMerger = false;
	fBypassMergerSet = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcbypassmergerenable" ) )
	{
	fBypassMerger = true;
	fBypassMergerSet = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcdeconvpaddisable" ) )
	{
	fDeconvPad= false;
	fDeconvPadSet = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcdeconvpadenable" ) )
	{
	fDeconvPad= true;
	fDeconvPadSet = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcclusterlowerlimit" ) )
	{
	if ( argc <= i+1 )
	    return "Missing TPC hardware co-processor lower cluster limit.";
	unsigned long tmp = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting TPC hardware co-processor lower cluster limit.";
	    }
	if ( tmp>0xFFFF )
	    {
	    errorArg = i+1;
	    return "Value given for TPC hardware co-processor lower cluster limit is larger than 16 bit limit (0-65535).";
	    }
	fClusterLowerLimit = (AliUInt16_t)tmp;
	fClusterLowerLimitSet = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcsinglesequencelimit" ) )
	{
	if ( argc <= i+1 )
	    return "Missing TPC hardware co-processor single sequence limit.";
	unsigned long tmp = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting TPC hardware co-processor single sequence limit.";
	    }
	if ( tmp>0xFF )
	    {
	    errorArg = i+1;
	    return "Value given for TPC hardware co-processor single sequence limit is larger than 8 bit limit (0-255).";
	    }
	fSingleSeqLimit = (AliUInt8_t)tmp;
	fSingleSeqLimitSet = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcmergerdistance" ) )
	{
	if ( argc <= i+1 )
	    return "Missing TPC hardware co-processor merger distance.";
	unsigned long tmp = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting TPC hardware co-processor merger distance.";
	    }
	if ( tmp>0xF )
	    {
	    errorArg = i+1;
	    return "Value given for TPC hardware co-processor merger distance is larger than 4 bit limit (0-15).";
	    }
	fMergerDistance = (AliUInt8_t)tmp;
	fMergerDistanceSet = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcmergeralgorithmfollowenable" ) )
	{
	fMergerAlgorithmFollow= true;
	fMergerAlgorithmFollowSet = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcmergeralgorithmfollowdisable" ) )
	{
	fMergerAlgorithmFollow= false;
	fMergerAlgorithmFollowSet = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcchargetolerance" ) )
	{
	if ( argc <= i+1 )
	    return "Missing TPC hardware co-processor charge tolerance.";
	unsigned long tmp = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting TPC hardware co-processor charge tolerance.";
	    }
	if ( tmp>0xF )
	    {
	    errorArg = i+1;
	    return "Value given for TPC hardware co-processor charge tolerance is larger than 4 bit limit (0-15).";
	    }
	fChargeTolerance = (AliUInt8_t)tmp;
	fChargeToleranceSet = true;
	i += 2;
	return NULL;
	}
    if (!strcmp(argv[i], "-tpcnoisesuppression")) {
      if (parse_argument_range<AliUInt8_t>(argc, argv, i, "-tpcnoisesuppression",
                               fNoiseSuppression, 0xf, errorArg, fErrorStr)) {
        return fErrorStr.c_str();
      }
      fNoiseSuppressionSet = true;
      return NULL;
    }
    if (!strcmp(argv[i], "-tpcnoisesuppressionminimum")) {
      if (parse_argument_range<AliUInt8_t>(argc, argv, i, "-tpcnoisesuppressionminimum",
                               fNoiseSuppressionMinimum, 0xf, errorArg, fErrorStr)) {
        return fErrorStr.c_str();
      }
      fNoiseSuppressionMinimumSet = true;
      return NULL;
    }
    if (!strcmp(argv[i], "-tpcnoisesuppressionneighbor")) {
      if (parse_argument_range<AliUInt8_t>(argc, argv, i, "-tpcnoisesuppressionneighbor",
                               fNoiseSuppressionNeighbor, 0x3, errorArg, fErrorStr)) {
        return fErrorStr.c_str();
      }
      fNoiseSuppressionNeighborSet = true;
      return NULL;
    }
    if (!strcmp(argv[i], "-tpcclusterqmaxlowerlimit")) {
      if (parse_argument_range<AliUInt16_t>(argc, argv, i, "-tpcclusterqmaxlowerlimit",
                               fClusterQmaxLowerLimit, 0xffff, errorArg, fErrorStr)) {
        return fErrorStr.c_str();
      }
      fNoiseSuppressionNeighborSet = true;
      return NULL;
    }
    if (!strcmp(argv[i], "-tpctagborderclusters")) {
      if (parse_argument_range<AliUInt32_t>(argc, argv, i, "-tpctagborderclusters",
                               fTagBorderClusters, 1, errorArg, fErrorStr)) {
        return fErrorStr.c_str();
      }
      fTagBorderClustersSet = true;
      return NULL;
    }
    if (!strcmp(argv[i], "-tpccorrectedgeclusters")) {
      if (parse_argument_range<AliUInt32_t>(argc, argv, i, "-tpccorrectedgeclusters",
                               fCorrectEdgeClusters, 1, errorArg, fErrorStr)) {
        return fErrorStr.c_str();
      }
      fCorrectEdgeClustersSet = true;
      return NULL;
    }
    if (!strcmp(argv[i], "-tpctagdeconvolutedclusters")) {
      if (parse_argument_range<AliUInt32_t>(argc, argv, i, "-tpctagdeconvolutedclusters",
                               fTagDeconvolutedClusters, 2, errorArg, fErrorStr)) {
        return fErrorStr.c_str();
      }
      fTagDeconvolutedClustersSet = true;
      return NULL;
    }
    if ( !strcmp( argv[i], "-periodcounterlogmodulo" ) )
	{
	if ( argc <= i+1 )
	    return "Missing period counter log message modulo.";
	unsigned long tmp = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting period counter log message modulo.";
	    }
	if ( tmp>~(unsigned)0 )
	    {
	    errorArg = i+1;
	    return "Period counter log message modulo parameter is too large (max. (2^32)-1).";
	    }
	fNoPeriodCounterLogModulo = (unsigned)tmp;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-nostrictsor" ) )
	{
	fNoStrictSOR = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-strictsor" ) )
	{
	fNoStrictSOR = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-datasource" ) )
    {
	if ( argc <= i+1 )
	    return "Missing datasource specifier.";
        i+=1;
        if ( !strcmp( argv[i], "diu") )
        {
            fDataSource = AliHLTCRORCHandlerInterface::kRemoteDiu;
        }
        else if ( !strcmp( argv[i], "siu") )
        {
            fDataSource = AliHLTCRORCHandlerInterface::kRemoteSiu;
        }
        else if ( !strcmp( argv[i], "ddr") )
        {
            fDataSource = AliHLTCRORCHandlerInterface::kLocalDdr;
        }
        else if ( !strcmp( argv[i], "pg") )
        {
            fDataSource = AliHLTCRORCHandlerInterface::kLocalPg;
        }
        else
        {
            return "Invalid datasource specifier.";
        }
        i++;
        return NULL;
    }

    return AliHLTControlledSourceComponent::ParseCmdLine( argc, argv, i, errorArg );
    }

void CRORCPublisherComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    AliHLTControlledSourceComponent::PrintUsage( errorStr, errorArg, errorParam );

    COMMAND_LINE_USAGE(
        "-device <RORC DeviceID>: Specify the C-RORC device to use. (Required)")
    COMMAND_LINE_USAGE("-channel <RORC ChannelID>: Specify the C-RORC channel "
                       "to use. (Required)")
    COMMAND_LINE_USAGE("-rorcinterface <RORC-Firmware-Interface-Version>: "
                       "Specify the version number of the RORC firmware to "
                       "use. (Optional; Allowed: 1; Default: "
                       << AliHLTLog::kDec << CRORC_DEFAULT_VERSION << ")")
    COMMAND_LINE_USAGE("-sleep: Sleep for a specified time when no event is "
                       "available. (Optional)")
    COMMAND_LINE_USAGE("-sleeptime <sleeptime_usec>: Specify the time in "
                       "microseconds to sleep (with -sleep) when no event is "
                       "available. (Optional)")
    COMMAND_LINE_USAGE("-restart: Specify that this is a restart, do not "
                       "initialize RORC and erase memories. (Optional)")
    COMMAND_LINE_USAGE("-dummydevice: Specify to simulate a RORC device and "
                       "not use a real one. (Optional)")
    COMMAND_LINE_USAGE("-enumerateeventids: Specify to generate enumerated "
                       "event IDs and not use real ones from the RORC. "
                       "(Optional)")
    COMMAND_LINE_USAGE("-disableddlheadersearch: Specify to disable the search "
                       "for DDL headers in received data. (Optional)")
    COMMAND_LINE_USAGE("-checkreportwordconsistency: Check the report word "
                       "data size and block counts for consistency. (Optional)")
    COMMAND_LINE_USAGE("-rorcblocksize <RORC-Block-Size>: Specify the size of "
                       "blocks the RORC transfers via DMA into memory. "
                       "(Optional; Only supported for interface v4; Default "
                       "256; Must be divisible by 4)")
    COMMAND_LINE_USAGE("-publishcorruptevents: Publish also events detected a "
                       "corrupted during the transfer from the RORC "
                       "(suppressed by default). (Optional)")
    COMMAND_LINE_USAGE("-suppressedhardwaresuppressedevents: Do not publish "
                       "events that have been suppressed already in the RORC "
                       "hardware (except for the header) (suppressed by "
                       "default). (Optional)")
    COMMAND_LINE_USAGE("-suppressgoodevents: Do not publish events that were "
                       "not marked as corrupted during the transfer from the "
                       "RORC (disabled by default, mainly for debugging, to "
                       "receive ONLY corrupt events). (Optional)")
    COMMAND_LINE_USAGE("-truncatecorruptevents: Try to truncate corrupt events "
                       "to their CDH if possible (disabled by default). "
                       "(Optional)")
    COMMAND_LINE_USAGE("-noflowcontrol: Specify that the flow control on the "
                       "C-RORC is disabled. (Optional)")
    COMMAND_LINE_USAGE("-eventsuppression <event-suppression-specifier>: "
                       "Enable event suppression in hardware, the specifier "
                       "will be used as an exponent for 2, the coresponding "
                       "power of two will be used in a modulo operation with "
                       "the event ID (in the RORC hardware). (Optional, "
                       "allowed values for the specifier are 0-15)")
    COMMAND_LINE_USAGE("-splitdatatype <type>: Specify the type as which the "
                       "data is to be specified in the case of split data "
                       "blocks. (Optional)")
    COMMAND_LINE_USAGE("-disablecorruptautodump: Disable the automatic dump to "
                       "disk of corrupt received events. (Optional, default is "
                       "enabled)")
    COMMAND_LINE_USAGE("-corruptautodumplimit <event-count-limit>: Specify the "
                       "maximum number of events after which the automatic "
                       "dump to disk of corrupt received events will stop. "
                       "(Optional, default 100)")
    COMMAND_LINE_USAGE("-corruptautodumpdir <directory>: Specify the directory "
                       "into which the automatic dump to disk of corrupt "
                       "received events will be done. (Optional, default "
                       "current directory)")
    COMMAND_LINE_USAGE("-tpcmappingfile <TPC patch number> <RORC TPC mapping "
                       "filename>: Specify the patch number and mapping file "
                       "to use for RORC TPC mapping for the fast hardware "
                       "clusterfinder. Automatically enables mapping. "
                       "(Optional, deprecated)")
    COMMAND_LINE_USAGE("-tpcmappingslice <TPC patch number> <RORC TPC mapping "
                       "filename>: Specify the patch number and mapping file "
                       "to use for RORC TPC mapping for the fast hardware "
                       "clusterfinder. Automatically enables mapping. "
                       "(Optional)")
    COMMAND_LINE_USAGE("-tpcmapping <RORC TPC mapping filename>: Specify a "
                       "mapping file to use for RORC TPC mapping for the fast "
                       "hardware clusterfinder. Automatically enables mapping. "
                       "Patch number is take from supplied DDL number. "
                       "(Optional, only in special ALICE HLT mode)")
    COMMAND_LINE_USAGE("-tpcsinglepadsuppressiondisable: Disable the single "
                       "pad cluster suppression in the hardware fast TPC "
                       "clusterfinder in the RORC. (Optional, only valid when "
                       "hardware co-processor is enabled)")
    COMMAND_LINE_USAGE("-tpcsinglepadsuppressionenable: Enable the single pad "
                       "cluster suppression in the hardware fast TPC "
                       "clusterfinder in the RORC. (Optional, only valid when "
                       "hardware co-processor is enabled)")
    COMMAND_LINE_USAGE("-tpcbypassmergerdisable: Disable the merger bypass for "
                       "the hardware fast TPC clusterfinder in the RORC. "
                       "(Optional, only valid when hardware co-processor is "
                       "enabled)")
    COMMAND_LINE_USAGE("-tpcbypassmergerenable: Enable the merger bypass for "
                       "the hardware fast TPC clusterfinder in the RORC. "
                       "(Optional, only valid when hardware co-processor is "
                       "enabled)")
    COMMAND_LINE_USAGE("-tpcdeconvpaddisable: Disable the deconvolution in pad "
                       "direction for the hardware fast TPC clusterfinder in "
                       "the RORC. (Optional, only valid when hardware "
                       "co-processor is enabled)")
    COMMAND_LINE_USAGE("-tpcdeconvpadenable: Enable the deconvolution in pad "
                       "direction for the hardware fast TPC clusterfinder in "
                       "the RORC. (Optional, only valid when hardware "
                       "co-processor is enabled)")
    COMMAND_LINE_USAGE("-tpcclusterlowerlimit <lower-cluster-limit-16bit>: "
                       "Specify the lower cluster charge limit for the "
                       "hardware fast TPC clusterfinder in the RORC. "
                       "(Optional, 16 bit number (0-65535), only valid when "
                       "hardware co-processor is enabled)")
    COMMAND_LINE_USAGE("-tpcsinglesequencelimit <single-sequence-limit-8bit>: "
                       "Specify the single sequence cluster charge limit for "
                       "the hardware fast TPC clusterfinder in the RORC. "
                       "(Optional, 8 bit number (0-255), only valid when "
                       "hardware co-processor is enabled)")
    COMMAND_LINE_USAGE("-tpcmergerdistance <merger-distance-4bit>: Specify the "
                       "merger distance for the hardware fast TPC "
                       "clusterfinder in the RORC. (Optional, 4 bit number "
                       "(0-15), only valid when hardware co-processor is "
                       "enabled)")
    COMMAND_LINE_USAGE("-tpcmergeralgorithmfollowdisable: Disable the Megrer "
                       "Algorithm Follow (?) for the hardware fast TPC "
                       "clusterfinder in the RORC. (Optional, only valid when "
                       "hardware co-processor is enabled)")
    COMMAND_LINE_USAGE("-tpcmergeralgorithmfollowenable: Enable the Megrer "
                       "Algorithm Follow (?) for the hardware fast TPC "
                       "clusterfinder in the RORC. (Optional, only valid when "
                       "hardware co-processor is enabled)")
    COMMAND_LINE_USAGE("-tpcchargetolerance <charge-tolerance-4bit>: Specify "
                       "the charge tolerance for the hardware fast TPC "
                       "clusterfinder in the RORC. (Optional, 4 bit number "
                       "(0-15), only valid when hardware co-processor is "
                       "enabled)")
    COMMAND_LINE_USAGE("-tpcnoisesuppression <noise-suppression-4bit>: Specify "
                       "noise suppression value for the TPC clusterfinder. "
                       "(Optional, 4 bit number)")
    COMMAND_LINE_USAGE("-tpcnoisesuppressionminimum "
                       "<noise-suppression-minimum-4bit>: Specify noise "
                       "suppression minimum value for the TPC clusterfinder. "
                       "(Optional, 4 bit number)")
    COMMAND_LINE_USAGE("-tpcnoisesuppressionneighbor "
                       "<noise-suppression-neighbor-2bit>: Specify noise "
                       "suppression neighbor value for the TPC clusterfinder. "
                       "(Optional, 2 bit number)")
    COMMAND_LINE_USAGE("-tpcclusterqmaxlowerlimit "
                       "<cluster-qmax-lower-limit-16bit>: Specify cluster Qmax "
                       "lower limit for the TPC clusterfinder. (Optional, 16 "
                       "bit number)")
    COMMAND_LINE_USAGE("-tpctagborderclusters <tag-border-clusters-1bit>: "
                       "Enable/Disable tagging of border clusters for the TPC "
                       "clusterfinder. (Optional, 0 or 1)")
    COMMAND_LINE_USAGE("-tpccorrectedgeclusters <correct-edge-clusters-1bit>: "
                       "Enable/Disable correction of edge clusters for the TPC "
                       "clusterfinder. (Optional, 0 or 1)")
    COMMAND_LINE_USAGE("-tpctagdeconvolutedclusters "
                       "<tag-deconvoluted-clusters-2bit>: Specify tagging of "
                       "deconvoluted clusters for the TPC clusterfinder. "
                       "(Optional, 0 or 1)")
    COMMAND_LINE_USAGE("-periodcounterlogmodulo "
                       "<period-counter-log-message-modulo>: Specify a modulo "
                       "to be applied for period counter change log messages. "
                       "(Optional, default 1)")
    COMMAND_LINE_USAGE("-nostrictsor: Relaxed requirements for receiving "
                       "Start-Of-Run/Data event before first real data event. "
                       "(Optional, only has effect in special ALICE HLT mode)")
    COMMAND_LINE_USAGE("-strictsor: Strict requirements for receiving "
                       "Start-Of-Run/Data event before first real data event. "
                       "(Optional, only has effect in special ALICE HLT mode)");
    COMMAND_LINE_USAGE("-datasource: specify RORC data source. Possible values "
                       "are 'diu', 'siu', 'ddr', and 'pg'. (Optional, default "
                       "is 'diu')")
    }

void CRORCPublisherComponent::ResetConfiguration()
    {
    AliHLTControlledComponent::ResetConfiguration();
    fAllowDataTypeOriginSpecSet = true;
    fDoSleep = false;
    fSleepTime = 10000;
    fRORCDeviceInfoSet = false;
    fRORCChannelInfoSet = false;
    fRORCDeviceID = 0xFFFFFFFF;
    fRORCHandler = NULL;
    fFirstStart = true;
    fDummy = false;
    fEnumerateEventIDs = false;
    fDDLHeaderSearch = true;
    fMaxPreBlockCount = 3;
    fReportWordConsistencyCheck = false;
    fRORCVersion = CRORC_DEFAULT_VERSION;
    fRORCBlockSize = 0; // 0: get default block size from PCIe subsystem
    fPublishCorruptEvents = false;
    fSuppressHWSuppressedEvents = false;
    fSuppressGoodEvents = false;
    fTruncateCorruptEvents = false;
    fNoFlowControl = false;
    fEventSuppression_Exp2 = 0;
    fEventSuppressionActive = false;
    fSplitEventDataTypeSet = false;
    fEnableCorruptEventsAutoDump = true;
    fCorruptEventsAutoDumpLimit = 100;
    fCorruptEventsAutoDumpDir = ".";
    fRCUVersion = RCU_DEFAULT_VERSION;
    fRORCMapping = NULL;
    fRORCTPCMappingPatchNr = ~0U;
    fRORCMappingFilename = NULL;
    fSinglePadSuppression = false;
    fSinglePadSuppressionSet = false;
    fBypassMerger = false;
    fBypassMergerSet = false;
    fDeconvPad = true;
    fDeconvPadSet = false;
    fClusterLowerLimit = 10;
    fClusterLowerLimitSet = false;
    fSingleSeqLimit = 0;
    fSingleSeqLimitSet = false;
    fMergerDistance = 4;
    fMergerDistanceSet = false;
    fMergerAlgorithmFollow = true;
    fMergerAlgorithmFollowSet = false;
    fChargeTolerance = 0;
    fChargeToleranceSet = false;
    fNoStrictSOR = false;
    }

bool CRORCPublisherComponent::CheckConfiguration()
    {
    if ( !AliHLTControlledComponent::CheckConfiguration() )
	return false;
    if ( !fRORCDeviceInfoSet )
	{
	LOG( AliHLTLog::kError, "CRORCPublisherComponent::CheckConfiguration", "No RORC device specified" )
	    << "Must specify the RORC device to be used (e.g. using the -device command line option."
	    << ENDLOG;
	return false;
	}
    if ( !fRORCChannelInfoSet )
	{
	LOG( AliHLTLog::kError, "CRORCPublisherComponent::CheckConfiguration", "No RORC channel specified" )
	    << "Must specify the RORC channel to be used (e.g. using the -channel command line option."
	    << ENDLOG;
	return false;
	}
    if ( fRORCVersion > CRORC_MAX_VERSION )
	{
	LOG( AliHLTLog::kError, "CRORCPublisherComponent::CheckConfiguration", "Too high RORC firmware version" )
	    << "Specified RORC firmware version " << AliHLTLog::kDec << fRORCVersion << " is too large. Allowed maximum is "
	    << CRORC_MAX_VERSION << "." << ENDLOG;
	return false;
	}
    if ( fRORCBlockSize>256 || (fRORCBlockSize % 4) )
	{
	LOG( AliHLTLog::kError, "CRORCPublisherComponent::CheckConfiguration", "invalid RORC block size" )
	    << "Specified RORC firmware version " << AliHLTLog::kDec << fRORCVersion << " requires a RORC block size less or equal 256B and divisible by 4." << ENDLOG;
	return false;
	}
    if ( fHardwareCoProcessor && fRORCVersion!=1 )
	{
	LOG( AliHLTLog::kError, "CRORCPublisherComponent::CheckConfiguration", "RORC hardware co-processor only supported in interface version 1" )
	    << "Specified RORC firmware version " << AliHLTLog::kDec << fRORCVersion << " does not support hardware co-processors." << ENDLOG;
	return false;
	}

    if ( fCorruptEventsAutoDumpLimit==0 )
	{
	LOG( AliHLTLog::kError, "CRORCPublisherComponent::CheckConfiguration", "Corrupt event auto dump event count limit" )
	    << "Corrupt event auto dump event count limit must be greater than 0." << ENDLOG;
	return false;
	}
    if (fRCUVersion > RCU_MAX_VERSION)
        {
	LOG( AliHLTLog::kError, "CRORCPublisherComponent::CheckConfiguration", "Invalid RCU version" )
	    << "Specified RCU version " << AliHLTLog::kDec << fRCUVersion << " is too large. Allowed maximum is "
	    << RCU_MAX_VERSION << "." << ENDLOG;
	return false;
	}



    return true;
    }

void CRORCPublisherComponent::ShowConfiguration()
    {
    AliHLTControlledComponent::ShowConfiguration();
    LOG( AliHLTLog::kInformational, "CRORCPublisherComponent::ShowConfiguration", "RORC Device Configuration" )
        << "RORC Device Number: " << AliHLTLog::kDec << fRORCDeviceID << " - "
        << "RORC Channel Number: " << AliHLTLog::kDec << fRORCChannelID << ENDLOG;
    LOG( AliHLTLog::kInformational, "CRORCPublisherComponent::ShowConfiguration", "RORC Firmware Version" )
	<< "Using RORC firmware version " << AliHLTLog::kDec << fRORCVersion << "." << ENDLOG;
    LOG( AliHLTLog::kInformational, "CRORCPublisherComponent::ShowConfiguration", "Start Mode" )
	<< "Start Mode: " << (fFirstStart ? "First Start" : "Restart" ) << "." << ENDLOG;
    if ( fDummy )
	{
	LOG( AliHLTLog::kWarning, "CRORCPublisherComponent::ShowConfiguration", "Dummy Mode" )
	    << "Using dummy device, no real data will be available." << ENDLOG;
	}
    if ( fDoSleep )
	{
	LOG( AliHLTLog::kInformational, "CRORCPublisherComponent::ShowConfiguration", "Sleep Mode" )
	    << "Sleeping between events for " << AliHLTLog::kDec << fSleepTime << " microseconds." << ENDLOG;

	}
    if ( fEnumerateEventIDs )
	{
	LOG( AliHLTLog::kWarning, "CRORCPublisherComponent::ShowConfiguration", "Enumerating Event IDs" )
	    << "Enumerating event IDs, no real event IDs are used." << ENDLOG;
	}

    if ( !fDDLHeaderSearch )
	{
	LOG( AliHLTLog::kWarning, "CRORCPublisherComponent::ShowConfiguration", "Not searching for DDL headers" )
	    << "Search for DDL headers in received data is disabled." << ENDLOG;
	}
    if ( fReportWordConsistencyCheck )
	{
	LOG( AliHLTLog::kInformational, "CRORCPublisherComponent::ShowConfiguration", "Report Word Consistency" )
	    << "Report word data size and block count are checked for consistency." << ENDLOG;
	}
    if ( fNoFlowControl )
	{
	LOG( AliHLTLog::kInformational, "CRORCPublisherComponent::ShowConfiguration", "No flow control" )
	    << "Flow control deactivated." << ENDLOG;
	}
    if ( fEventSuppressionActive )
	{
	LOG( AliHLTLog::kInformational, "CRORCPublisherComponent::ShowConfiguration", "Event suppression" )
	    << "Event suppression: " << AliHLTLog::kDec << fEventSuppression_Exp2 << "." << ENDLOG;
	}


    if ( fEnableCorruptEventsAutoDump )
	{
	LOG( AliHLTLog::kInformational, "CRORCPublisherComponent::ShowConfiguration", "Corrupt event auto dump" )
	    << "Automatic dumping of corrupt is enabled. Limit: " << AliHLTLog::kDec << fCorruptEventsAutoDumpLimit
	    << " - directory for dumped files: " << fCorruptEventsAutoDumpDir.c_str() << ENDLOG;
	}

    switch( fDataSource )
        {
        case AliHLTCRORCHandlerInterface::kRemoteSiu:
            LOG( AliHLTLog::kInformational, "CRORCPublisherComponent::ShowConfiguration", "Data Source SIU")
                << "using SIU as data source." << ENDLOG;
            break;
        case AliHLTCRORCHandlerInterface::kLocalDdr:
            LOG( AliHLTLog::kInformational, "CRORCPublisherComponent::ShowConfiguration", "Data Source DDR3")
                << "using DDR3 memory as data source." << ENDLOG;
            break;
        case AliHLTCRORCHandlerInterface::kLocalPg:
            LOG( AliHLTLog::kInformational, "CRORCPublisherComponent::ShowConfiguration", "Data Source PG")
                << "using Pattern Generator as data source." << ENDLOG;
            break;
        default:
            LOG( AliHLTLog::kInformational, "CRORCPublisherComponent::ShowConfiguration", "Data Source DIU")
                << "using DIU as data source (default)." << ENDLOG;
            break;
        }
    }

void CRORCPublisherComponent::Configure()
    {
    AliHLTControlledComponent::Configure();
    }



bool CRORCPublisherComponent::CreateParts()
    {
    if ( !AliHLTControlledComponent::CreateParts() )
	return false;
    if ( !fDummy )
    {
        switch ( fRORCVersion )
        {
            case 1:
                { fRORCHandler = new AliHLTCRORC1Handler( fBufferSize, fMaxPreEventCountExp2 ); }
                break;
        }
    }
    else
	fRORCHandler = new AliHLTDummyCRORCHandler( fBufferSize, fMaxPreEventCountExp2 );

    if ( !fRORCHandler )
	{
	LOG( AliHLTLog::kError, "CRORCPublisherComponent::CreateParts", "Unable to create RORC handler object" )
	    << "Unable to create RORC handler object." << ENDLOG;
	return false;
	}

    fRORCHandler->SetNoPeriodCounterLogModulo(fNoPeriodCounterLogModulo);

    if ( fRORCMappingFilename && ( (fRORCTPCMappingPatchNr!=~0U) || fAliceHLT  ) )
	{
	if ( fRORCTPCMappingPatchNr==~0U )
	    {
	    if ( AliHLTReadoutList::GetDetectorID( fDDLID )!=kTPC )
		{
		LOG( AliHLTLog::kError, "CRORCPublisherComponent::CreateParts", "RORC mapping only supported for TPC" )
		    << "RORC mapping files are currently only supported for TPC DDLs." << ENDLOG;
		return false;
		}
	    fRORCTPCMappingPatchNr = AliHLTReadoutList::GetTPCPatch( fDDLID );
	    }
	else if ( fAliceHLT && fRORCTPCMappingPatchNr!=AliHLTReadoutList::GetTPCPatch( fDDLID ) )
	    {
	    LOG( AliHLTLog::kWarning, "CRORCPublisherComponent::CreateParts", "RORC TPC mapping patch number mismatch" )
		<< "Specified RORC TPC mapping patch number " << AliHLTLog::kDec << fRORCTPCMappingPatchNr
		<< " does not match patch number " << AliHLTReadoutList::GetTPCPatch( fDDLID ) << " extracted from DDL ID " << fDDLID
		<< "." << ENDLOG;
	    }
        fRORCMapping = new AliHLTCRORCTPCMapping(fRORCTPCMappingPatchNr, fRCUVersion);
	if ( !fRORCMapping )
	    {
	    LOG( AliHLTLog::kError, "CRORCPublisherComponent::CreateParts", "Unable to create TPC RORC mapping object" )
		<< "Unable to create RORC TPC mapping object." << ENDLOG;
	    return false;
	    }
	}

    return true;
    }

void CRORCPublisherComponent::DestroyParts()
    {
    if ( fBufferManager )
	reinterpret_cast<AliHLTRORCBufferManager*>( fBufferManager )->SetRORCHandler( NULL );
    if ( fRORCHandler && fShmManager )
	{
	fShmManager->ReleaseShm( fShmKey );
	}
    reinterpret_cast<AliHLTCRORCPublisher*>(fPublisher)->SetRORCHandler( NULL );
#ifdef PROFILING
    if ( fPublisher )
	reinterpret_cast<AliHLTCRORCPublisher*>(fPublisher)->WriteHistograms( fComponentName );
#endif
    AliHLTControlledComponent::DestroyParts();
    if ( fRORCMapping )
	{
	delete fRORCMapping;
	fRORCMapping = NULL;
	}
    if ( fRORCHandler )
	{
	fRORCHandler->DeinitializeRORC();
	fRORCHandler->Close();
	delete fRORCHandler;
	fRORCHandler = NULL;
	}
    }

bool CRORCPublisherComponent::SetupComponents()
    {
    if ( !AliHLTControlledComponent::SetupComponents() )
	return false;

    if ( fPersistentState && fPersistentState->DidExist() && (*fPersistentState) &&
	 (*reinterpret_cast< AliHLTPersistentState<AliHLTDetectorPublisher::State>* >(fPersistentState))->fLastEventID!=AliEventID_t() )
	fFirstStart = false;

    if ( fBufferManager )
	reinterpret_cast<AliHLTRORCBufferManager*>( fBufferManager )->SetRORCHandler( fRORCHandler );

    // allocate eventbuffer via AliHLTSharedMemory
    // GetShm uses command line paramters '-shm [type] [id] [size]'
    AliUInt8_t* eventBuffer=NULL;
    eventBuffer = fShmManager->GetShm( fShmKey );

    int ret = fRORCHandler->Open( fRORCDeviceID, fRORCChannelID,
            AliHLTCRORCHandlerInterface::kDMAToHost, fDataSource );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "CRORCPublisherComponent::SetupComponents", "Error opening RORC" )
	    << "Error opening RORC device: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return false;
	}

    if ( fRORCVersion==1 )
    {
        if ( fRORCHandler->SetEventBuffer( fShmKey ) != 0 )
        {
            LOG( AliHLTLog::kError, "CRORCPublisherComponent::SetupComponents",
                    "SetEventBuffer") << "Failed to bind event buffer to C-RORC"
                 << ENDLOG;
            return false;
        }

        dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)
            ->SetNoFlowControl(fNoFlowControl);
        if (fEventSuppressionActive) {
          dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)
              ->SetEventSuppression(fEventSuppression_Exp2);
        }

        reinterpret_cast<AliHLTCRORC1Handler *>(fRORCHandler)
            ->SetEnableHWCoProc(fHardwareCoProcessor);

        if ( fHardwareCoProcessor )
        {
            if ( fSinglePadSuppressionSet)
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->SetSinglePadSuppression( fSinglePadSuppression );
            if ( fBypassMergerSet )
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->SetBypassMerger( fBypassMerger );
            if ( fDeconvPadSet )
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->SetDeconvPad( fDeconvPad );
            if ( fClusterLowerLimitSet )
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->SetClusterLowerLimit( fClusterLowerLimit );
            if ( fSingleSeqLimitSet )
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->SetSingleSeqLimit( fSingleSeqLimit );
            if ( fMergerDistanceSet )
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->SetMergerDistance( fMergerDistance );
            if ( fMergerAlgorithmFollowSet )
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->SetMergerAlgorithmFollow( fMergerAlgorithmFollow );
            if ( fChargeToleranceSet )
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->SetChargeTolerance( fChargeTolerance );
            if ( fNoiseSuppressionSet )
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->SetNoiseSuppression( fNoiseSuppression );
            if ( fNoiseSuppressionMinimumSet )
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->SetNoiseSuppressionMinimum( fNoiseSuppressionMinimum );
            if ( fNoiseSuppressionNeighborSet )
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->SetNoiseSuppressionNeighbor( fNoiseSuppressionNeighbor );
            if ( fClusterQmaxLowerLimitSet )
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->SetClusterQmaxLowerLimit( fClusterQmaxLowerLimit );
            if ( fTagBorderClustersSet )
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->SetTagBorderClusters( fTagBorderClusters );
            if ( fCorrectEdgeClustersSet )
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->SetCorrectEdgeClusters( fCorrectEdgeClusters );
            if ( fTagDeconvolutedClustersSet )
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->SetTagDeconvolutedClusters( fTagDeconvolutedClusters );
            if (fRCUVersion == 2)
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->SetBranchOverride(1);
            else
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->SetBranchOverride(0);
        }
    }


    ret = fRORCHandler->InitializeRORC();
    if ( ret )
    {
        LOG( AliHLTLog::kError, "CRORCPublisherComponent::SetupComponents", "InitializeRORC()" )
            << "Error initializing RORC hardware: " << strerror(ret) << " (" << AliHLTLog::kDec
            << ret << ")." << ENDLOG;
        return false;
    }

    if ( !fSplitEventDataTypeSet )
	fSplitEventDataType.fID = fEventDataType.fID;
    if ( fAliceHLT )
	fSplitEventDataType.fID = DDLRAWDATASPLIT_DATAID;


    dynamic_cast<AliHLTCRORCPublisher *>(fPublisher)->SetEventDataType( fEventDataType, fSplitEventDataType );
    dynamic_cast<AliHLTCRORCPublisher *>(fPublisher)->SetEventDataOrigin( fEventDataOrigin );
    dynamic_cast<AliHLTCRORCPublisher *>(fPublisher)->SetEventDataSpecification( fEventDataSpecification );
    dynamic_cast<AliHLTCRORCPublisher *>(fPublisher)->SetSleep( fDoSleep );
    dynamic_cast<AliHLTCRORCPublisher *>(fPublisher)->SetShm( fShmKey, 0, eventBuffer, fBufferSize );
    if ( fNoStrictSOR )
	dynamic_cast<AliHLTCRORCPublisher *>(fPublisher)->NoStrictSOR();

    dynamic_cast<AliHLTCRORCPublisher *>(fPublisher)->SetSleepTime( fSleepTime );
    dynamic_cast<AliHLTCRORCPublisher *>(fPublisher)->SetRORCHandler( fRORCHandler );
    dynamic_cast<AliHLTCRORCPublisher *>(fPublisher)->PublishCorruptEvents( fPublishCorruptEvents );
    dynamic_cast<AliHLTCRORCPublisher *>(fPublisher)->SuppressHWSuppressedEvents( fSuppressHWSuppressedEvents );
    dynamic_cast<AliHLTCRORCPublisher *>(fPublisher)->SuppressGoodEvents( fSuppressGoodEvents );
    dynamic_cast<AliHLTCRORCPublisher *>(fPublisher)->TruncateCorruptEvents( fTruncateCorruptEvents );
   if ( !fDummy )
	{
	switch ( fRORCVersion )
	    {
	    case 1:
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->EnumerateEventIDs( fEnumerateEventIDs );
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->SetHeaderSearch( fDDLHeaderSearch );
                dynamic_cast<AliHLTCRORC1Handler *>(fRORCHandler)->SetReportWordConsistencyCheck( fReportWordConsistencyCheck );
		break;
	    }
	}
    else
	dynamic_cast<AliHLTDummyCRORCHandler *>(fRORCHandler)->SetHeaderSearch( fDDLHeaderSearch );

    dynamic_cast<AliHLTCRORCPublisher *>(fPublisher)->SetEnableCorruptEventsAutoDump( fEnableCorruptEventsAutoDump );
    if ( fEnableCorruptEventsAutoDump )
	{
	dynamic_cast<AliHLTCRORCPublisher *>(fPublisher)->SetCorruptEventsAutoDumpLimit( fCorruptEventsAutoDumpLimit );
	MLUCString prefix( fCorruptEventsAutoDumpDir );
	prefix += "/";
	prefix += fComponentName;
	prefix += "-CorruptEvent";
	dynamic_cast<AliHLTCRORCPublisher *>(fPublisher)->SetCorruptEventsAutoDumpPrefix( prefix.c_str() );
	}

    if ( fRORCMappingFilename && fRORCMapping )
	{
	ret = fRORCMapping->Read( fRORCMappingFilename );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "CRORCPublisherComponent::SetupComponents", "Error reading RORC mapping" )
		<< "Error reading RORC mapping table from file " << fRORCMappingFilename
		<< ": "  << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ") - " << fRORCMapping->GetLastError() << "." << ENDLOG;
	    return false;
	    }
	ret = fRORCMapping->WriteData( fRORCHandler );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "CRORCPublisherComponent::SetupComponents", "Write FCF Mapping Data" )
		<< "Error writing mapping table to RORC device: "  << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    return false;
	    }
	}

    fStatus->fHLTStatus.fTotalOutputBuffer = reinterpret_cast<AliHLTRORCBufferManager*>( fBufferManager )->GetTotalBufferSize();
    fStatus->fHLTStatus.fFreeOutputBuffer = reinterpret_cast<AliHLTRORCBufferManager*>( fBufferManager )->GetFreeBufferSize();

    return true;
    }


AliHLTBufferManager* CRORCPublisherComponent::CreateBufferManager()
    {
    return new AliHLTRORCBufferManager;
    }




int main( int argc, char** argv )
    {
    CRORCPublisherComponent procComp( "CRORCPublisher", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$
**
***************************************************************************
*/
