#ifndef _ALIHLTCRORCHANDLERINTERFACE_HPP_
#define _ALIHLTCRORCHANDLERINTERFACE_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliEventID.h"
#include "AliHLTShmID.h"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTSubEventDescriptor.hpp"
#include "MLUCVector.hpp"
#include "MLUCString.hpp"
#include <vector>
#include <sys/time.h>
#include <cerrno>
#include <librorc.h>

#define LOG_DEC_HEX(x)                                                         \
  AliHLTLog::kDec << (x) << "(0x" << AliHLTLog::kHex << (x) << ")"             \
  << AliHLTLog::kDec

#define LOG_HEX_DEC(x)                                                         \
  AliHLTLog::kHex << "0x" << (x) << "(" << AliHLTLog::kDec << (x) << ")"

struct AliHLTRORCLinkStatus {
    bool transceiver_up;
    bool ddl_up;
    bool transceiver_errors;
    uint32_t ddl_xoff_count;
    uint16_t gtx_rxlossofsync_count;
    uint16_t gtx_rxdispartity_count;
    uint16_t gtx_rxnotintable_count;
    uint32_t dma_stall_flags;
    uint32_t dma_event_count;
    unsigned long fcf_rcu_protocol_errors;
    unsigned long fcf_altro_channel_errors;
    unsigned long events_in_ringbuffer;
};

class AliHLTCRORCHandlerInterface
    {
    public:

        enum AliHLTRORCDataDirection { kDMAToHost = 0, kDMAToDevice = 1 };
        enum AliHLTRORCDataSource { kRemoteDiu = 0, kRemoteSiu = 1, kLocalDdr = 2, kLocalPg = 3, kLocalPcie = 4 };

	virtual ~AliHLTCRORCHandlerInterface() {};

        virtual int Open( uint8_t deviceId, uint32_t channelId,
                          AliHLTRORCDataDirection dataDirection,
                          AliHLTRORCDataSource dataSource ) = 0;
        virtual int Close() = 0;

        virtual int SetEventBuffer( AliHLTShmID shmKey ) = 0;

	virtual int InitializeRORC() = 0;
	virtual int DeinitializeRORC() = 0;

	virtual int InitializeFromRORC( bool first = true ) = 0;

	virtual int SetOptions( const vector<MLUCString>& options, char*& errorMsg, unsigned& errorArgNr ) = 0;
	virtual void GetAllowedOptions( vector<MLUCString>& options ) = 0;

	virtual int WriteConfigWord( unsigned long wordIndex, AliUInt32_t word, bool doVerify ) = 0;

	virtual int ActivateRORC() = 0;
	virtual int DeactivateRORC() = 0;

	virtual int PollForEvent( AliEventID_t& eventID,
				  AliUInt64_t& dataOffset, AliUInt64_t& dataSize,
				  bool& dataDefinitivelyCorrupt, bool& eventSuppressed, AliUInt64_t& unsuppressedDataSize ) = 0;

	virtual int ReleaseEvent( AliEventID_t eventID ) = 0;

	virtual int FlushEvents() = 0;

        virtual int ReadRORCLinkStatus( struct AliHLTRORCLinkStatus& )
                {
                return ENOSYS;
                }

	virtual void SetNoPeriodCounterLogModulo(unsigned) {}

	virtual uint64_t GetTotalBufferSize()
		{
		return 0;
		}
	virtual uint64_t GetFreeBufferSize()
		{
		return 0;
		}

        virtual uint64_t XoffToUs(uint64_t value) { return 0; }

        /*********** HLT-OUT specific ***************/

        virtual int SubmitEvent
            (
             AliEventID_t eventID,
             const AliHLTSubEventDescriptor::BlockData& block,
             const AliHLTSubEventDataDescriptor* sedd,
             const AliHLTEventTriggerStruct* ets
            ) = 0;

        virtual int PollForDoneEvent() = 0;

    protected:
    private:
    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTCRORCHANDLERINTERFACE_HPP_
