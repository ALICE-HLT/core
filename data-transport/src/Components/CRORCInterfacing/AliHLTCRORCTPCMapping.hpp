#ifndef _ALIHLTCRORCTPCMAPPING_HPP_
#define _ALIHLTCRORCTPCMAPPING_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck,
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliHLTCRORCMapping.hpp"
#include "AliHLTCRORCHandlerInterface.hpp"
#include "MLUCString.hpp"

const unsigned gkConfigWordCnt = 4096;



class AliHLTCRORCTPCMapping: public AliHLTCRORCMapping
    {
    public:

      AliHLTCRORCTPCMapping( unsigned patchNr, unsigned rcuVersion = 1 );
	virtual ~AliHLTCRORCTPCMapping();

	virtual int Read( const char* filename );

	AliUInt32_t operator[]( unsigned ndx )
		{
		return (ndx<gkConfigWordCnt) ? fConfigWords[ndx] : ~(AliUInt32_t)0;
		}

	virtual const char* GetLastError()
		{
		return fLastError.c_str();
		}

	virtual int WriteData( AliHLTCRORCHandlerInterface* interface );


    protected:

	AliUInt32_t fConfigWords[gkConfigWordCnt];

	unsigned fPatchNr;
        unsigned fRcuVersion;

	MLUCString fLastError;

    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$
**
***************************************************************************
*/

#endif // _ALIHLTCRORCTPCMAPPING_HPP_
