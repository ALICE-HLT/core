/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#include "AliHLTCRORCOutWriterSubscriber.hpp"
#include "AliHLTDDLHeader.hpp"
#include "AliHLTReadoutList.hpp"
#include "AliHLTDDLHeaderData.h"
#include "AliHLTDDLHeader.hpp"
#include "AliHLTEventWriter.hpp"
#include <errno.h>

/**
 * Threshold for XOFF Warnings in percent of busy time since last measurement:
 * The bandwidth to the SIU is slightly higher than the DDL throughput. This
 * is caused by DDL protocol overhead and is in the order of ~5%.
 **/
#define DDL_XOFF_PERCENT_THRESHOLD 5

/**
 * Threshold for XOFF reporting at end of run in number of clock cycles:
 * DDL initialization usually generates 1 cycle of XOFF
 **/
#define DDL_XOFF_TOTAL_THRESHOLD 100

#define LHC_ORBIT_PERIOD_SECONDS 1492

inline long long timediff_us (struct timeval from, struct timeval to) {
    return ((long long)(to.tv_sec - from.tv_sec) * 1000000LL +
            (long long)(to.tv_usec - from.tv_usec));
}

const char *ShmTypeToString(AliHLTShmID shmId) {
    switch (shmId.fShmType) {
        case kBigPhysShmType:
            return "bigphys";
        case kPhysMemShmType:
            return "physmem";
        case kSysVShmType:
            return "sysv";
        case kSysVShmPrivateType:
            return "sysv-private";
        case kLibrorcShmType:
            return "librorc";
        default:
            return "invalid";
    }
}


AliHLTCRORCOutWriterSubscriber::AliHLTCRORCOutWriterSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize,
							    AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, int eventCountAlloc ):
    AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc ),
    fEventLog( 10, eventCountAlloc, true )
    {
    fDoSleep = true;
    fSleepTime = 10000;
    
    fShmPtr = NULL;
    fShmSize = 0;

    fHLTOutHandler = NULL;
    fLastDdlXoffCount = 0xffffffff;
    fLastTimerExpired.tv_sec = fLastTimerExpired.tv_usec = 0;
    fTotalDdlXoffCount = 0;

    fQuitDonePoll = true;

    fSimpleMode = false;

    fDDLID = ~0UL;

    fDisable = false;

    fNotDoneWarnTime_us = 0;
    fNotDoneWarnRepeat = false;
    fNotDoneAbortTime_us = 0;

    fBroadcastEventMaster = false;

    fDoEventLog = false;

    fDumpSuspiciousEvents = false;

    fEventBackLogIndex = 0;
    fReadoutListOutputCnt = 0;
    fReadoutListOutputReduction = 0;
    fReadoutListOutputReductionFactor = 1;

    fCurrentMaxEventLifetime_us = 0;
    fCurrentMinEventLifetime_us = 0;
    fCurrentSumOfLifetimes_us = 0;
    fClearCurrentLifetimeCounters = false;
    fHighestEventId = 0;
    fEorXoffStats = false;

    // disable suppression of similar LOG messages
    gLog.SetStackTraceComparisonDepth(0);
    }

AliHLTCRORCOutWriterSubscriber::~AliHLTCRORCOutWriterSubscriber()
    {
    }


bool AliHLTCRORCOutWriterSubscriber::ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, 
						 const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* etsp,
						 AliUInt8_t*, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>&, 
						 AliHLTEventDoneData*& )
    {
    bool isSuspiciousEvent = false;
    size = 0;
    if ( fDisable )
	{
	LOG( AliHLTLog::kDebug, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "Output disabled" )
	    << "Output is explicitly disabled!" << ENDLOG;
	return true;
	}
    if ( !fHLTOutHandler )
	{
	LOG( AliHLTLog::kError, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "No HLTOut Handler" )
	    << "No HLTOut handler object configured." << ENDLOG;
	return false;
	}


    if ( eventID.fType != kAliEventTypeStartOfRun && eventID.fType != kAliEventTypeData && eventID.fType != kAliEventTypeEndOfRun && eventID.fType != kAliEventTypeSync )
	{
	LOG( AliHLTLog::kInformational, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "Non-(SOR/Data/EOR/SYNC) Event" )
	    << "Non-(SOR/Data/EOR/SYNC) Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") will not be written to HLTOut." << ENDLOG;
	size = 0;
	return true;
	}

    if ( fDoEventLog )
	{
	unsigned long ndx;
	if ( fEventLog.FindElement( eventID, ndx ) )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "Event already processed" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") has already been received before. Event will not be processed again."
		<< ENDLOG;
	    return true;
	    }
	}
    else
	{
	;
	for ( unsigned long backLogCnt=0;  backLogCnt<EVENT_BACKLOG_SIZE; ++backLogCnt )
	    {
	    if ( fEventBackLog[backLogCnt]==eventID )
		{
		LOG( AliHLTLog::kWarning, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "Event already processed" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ") has already been received before. Event will not be processed again."
		    << ENDLOG;
		return true;
		}
	    }
	}


    bool broadcastEventMaster = fBroadcastEventMaster;

    int ret;

    bool tmpFound=false;
    unsigned long n, hltOutBlockNdx = ~(unsigned long)0;
    bool broadcastEvent = (eventID.fType==kAliEventTypeStartOfRun || eventID.fType==kAliEventTypeEndOfRun || eventID.fType==kAliEventTypeSync);
    for ( n = 0; n < blockCnt; n++ )
	{
	if ( blocks[n].fDataType.fID == HLTOUTPUT_DATAID )
	    {
	    if ( blocks[n].fShmKey.fShmType==kInvalidShmType )
		{
		LOG( AliHLTLog::kWarning, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "Invalid HLT Output data blocks shm" )
		    << "Invalid shared memory type (" << AliHLTLog::kDec << blocks[n].fShmKey.fShmType
		    << ") for found HLTOutput block for event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ")." << ENDLOG;
		continue;
		}
	    if ( hltOutBlockNdx==~(unsigned long)0  ||
		 ( !tmpFound && (blocks[n].fShmKey.fShmType==kLibrorcShmType) &&
		   blocks[hltOutBlockNdx].fShmKey.fShmType!=kLibrorcShmType ) )
		hltOutBlockNdx = n; // Precaution, in case no fully  matching block is found
	    if ( broadcastEvent )
		{
		AliUInt8_t* data = (AliUInt8_t*)blocks[n].fData;
		AliUInt64_t cdhStatusError = AliHLTGetDDLHeaderStatusError( data );
		bool decisionOrDataIncluded =  ( cdhStatusError & (0x40|0x80) );
		if ( broadcastEventMaster!=decisionOrDataIncluded )
		    continue;
		}
	    if ( tmpFound )
		{
		if ( !broadcastEvent || broadcastEventMaster )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "Multiple matching HLT Output data blocks" )
			<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ") contains more than one HLT output data block"
			<< (broadcastEvent ? (broadcastEventMaster ? " with HLT decision or data" : " without HLT decision or data") : "")
			<< ". Only one will be sent." << ENDLOG;
		    }
		isSuspiciousEvent = true;
		//return true;
		if ( (blocks[n].fShmKey.fShmType==kLibrorcShmType) &&
		     blocks[hltOutBlockNdx].fShmKey.fShmType!=kLibrorcShmType )
		    hltOutBlockNdx = n;
		}
	    else
		{
		tmpFound = true;
		hltOutBlockNdx = n;
		}
	    }
	}
    if ( broadcastEvent && !tmpFound && hltOutBlockNdx!=~(unsigned long)0 )
	{
	LOG( AliHLTLog::kWarning, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "Empty event" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") contains no HLT output data blocks"
	    << (broadcastEventMaster ? " with HLT decision or data" : " without HLT decision or data")
	    << " - using first found output block (block " << AliHLTLog::kDec << hltOutBlockNdx << ")..."
	    << ENDLOG;
	tmpFound = true;
	isSuspiciousEvent = true;
	}
    if ( !tmpFound )
	{
	LOG( AliHLTLog::kError, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "Empty event" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") contains no HLT output data blocks at all"
	    << "." << ENDLOG;
	return true;
	}


    AliHLTSubEventDescriptor::BlockData outputBlock = blocks[hltOutBlockNdx];
    AliUInt8_t* data = (AliUInt8_t*)outputBlock.fData;
    AliUInt64_t cdhStatusError = AliHLTGetDDLHeaderStatusError( data );
    if ( cdhStatusError & 0x40 )
	{
	AliVolatileUInt32_t* readoutListData = (AliVolatileUInt32_t*)(data+AliHLTGetDDLHeaderSize(data)+sizeof(AliUInt32_t)*4);
	AliHLTReadoutList readoutList( gReadoutListVersion, readoutListData );
	bool tmp=false;
	readoutList.GetDetectorDDLs( "HLT", tmp ); // Check whether any bit is set in data coming from HLTOutFormatter ; No bit set signals full reject
	if ( tmp )
	    {
	    readoutList.SetDetectorDDLs( "HLT", false ); // Reset all other bits and set only our own bit
	    if ( fDDLID != ~0UL && !readoutList.SetDDL( fDDLID ) )
		{
		LOG( AliHLTLog::kError, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "Error setting DDL bit" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		    << "): Error setting HLT output DDL bit 0x" << AliHLTLog::kHex
		    << fDDLID << " (readout list data: 0x" << (unsigned long)readoutListData << "." << ENDLOG;
		}
	    }
	}
    if ( eventID.fType==kAliEventTypeData )
	{
	if ( fReadoutListOutputReduction && ((fReadoutListOutputCnt<fReadoutListOutputReduction) || ((fReadoutListOutputCnt % fReadoutListOutputReduction*fReadoutListOutputReductionFactor)==0)) )
	    {
	    if ( cdhStatusError & 0x40 )
		{
		AliVolatileUInt32_t* readoutListData = (AliVolatileUInt32_t*)(data+AliHLTGetDDLHeaderSize(data)+sizeof(AliUInt32_t)*4);
		AliHLTReadoutList readoutList( gReadoutListVersion, readoutListData );
		MLUCString msg;
		for ( unsigned nn=0; nn<readoutList.GetReadoutListSize(); nn += sizeof(hltreadout_uint32_t) )
		    {
		    char tmp[256];
		    snprintf( tmp, 256, "%02u : 0x%08X", nn, readoutListData[nn] );
		    if ( nn )
			msg += " - ";
		    msg += tmp;
		    }
		LOG( AliHLTLog::kImportant, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "Readout list" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << ") readout list: " << msg.c_str() << "." << ENDLOG;
		}
	    else
		{
		LOG( AliHLTLog::kImportant, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "No readout list" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << ") has no readout list." << ENDLOG;
		}
	    if ( fReadoutListOutputCnt>=fReadoutListOutputReduction )
		fReadoutListOutputReductionFactor *= 2;
	    }
	++fReadoutListOutputCnt;
	      
	// fReadoutListOutputCnt = 0;
	// fReadoutListOutputReduction = 1;
	}

    // Determine first DDL data block.
    const char* outputBlockShmType = ShmTypeToString(outputBlock.fShmKey);

    LOG( AliHLTLog::kDebug, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "Submitting event to HLTOut" )
	<< "Submitting event event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << ") . Using output block " << hltOutBlockNdx << " - output block shm type: "
	<< outputBlockShmType << " - output block size: " << outputBlock.fSize << ENDLOG;
    ret = fHLTOutHandler->SubmitEvent( eventID, outputBlock, sedd, etsp );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "Error submitting event" )
	    << "Error submitting first HLT output data block (block "
	    << AliHLTLog::kDec << n << ") from event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") to HLTOut handler: " << strerror(ret) << " ("
	    << AliHLTLog::kDec << ret << ")." << ENDLOG;
	return false;
	}
    if ( fDoEventLog )
	fEventLog.Add( eventID, eventID );
    else
	{
	fEventBackLog[fEventBackLogIndex] = eventID;
	fEventBackLogIndex = (fEventBackLogIndex+1) & EVENT_BACKLOG_MASK;
	}
    if ( isSuspiciousEvent && fDumpSuspiciousEvents )
	{
	MLUCString prefix = GetName();
	prefix += "_SuspiciousEvent";
	AliHLTEventWriter::WriteEvent( eventID, blockCnt, blocks, sedd, etsp, fRunNumber, prefix.c_str() );
	}
    struct timeval start, lastWarn;
    gettimeofday( &start, NULL );
    lastWarn.tv_sec = lastWarn.tv_usec = 0;
    while ( !fQuitDonePoll )
	{
	ret =fHLTOutHandler->PollForDoneEvent();
	if ( ret && ret!=EAGAIN )
	    {
	    LOG( AliHLTLog::kError, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "Error polling for done event" )
		<< "Error polling for done event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") at HLTOut handler: " << strerror(ret) << " ("
		<< AliHLTLog::kDec << ret << ")." << ENDLOG;
	    // We return true, as we have to presume the event has already been sent and we don't want a retry...
	    // XXX ???
	    return true;
	    }
	if ( !ret )
	    {
	    if ( eventID.fType==kAliEventTypeStartOfRun )
		{
		LOG( AliHLTLog::kImportant, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "Start-Of-Data/-Run event set" )
		    << "Start-Of-Data/-Run event 0x" << AliHLTLog::kHex << eventID << " ("
		    << AliHLTLog::kDec << eventID << ") successfully sent." << ENDLOG;
		}
	    if ( eventID.fType==kAliEventTypeSync )
		{
		LOG( AliHLTLog::kImportant, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "SYNC event set" )
		    << "SYNC event 0x" << AliHLTLog::kHex << eventID << " ("
		    << AliHLTLog::kDec << eventID << ") successfully sent." << ENDLOG;
		}
	    if ( eventID.fType==kAliEventTypeEndOfRun )
		{
		LOG( AliHLTLog::kImportant, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "End-Of-Data/-Run event set" )
		    << "End-Of-Data/-Run event 0x" << AliHLTLog::kHex << eventID << " ("
		    << AliHLTLog::kDec << eventID << ") successfully sent." << ENDLOG;
                }
            UpdateEventStatistics(sedd);
	    return true;
	    }
	if ( fNotDoneAbortTime_us || ( fNotDoneWarnTime_us && (fNotDoneWarnRepeat || (!lastWarn.tv_sec && !lastWarn.tv_usec)) ) )
	    {
	    struct timeval now;
	    gettimeofday( &now, NULL );
	    unsigned long long tdiff;
	    tdiff = (now.tv_sec-start.tv_sec)*1000000ULL+(now.tv_usec-start.tv_usec);
	    if ( fNotDoneAbortTime_us && tdiff>fNotDoneAbortTime_us )
		{
		LOG( AliHLTLog::kError, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "Timeout polling for done event" )
		    << "Timeout polling for done event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ") at HLTOut handler after " << MLUCLog::kDec << fNotDoneAbortTime_us << " microsecs.. Aborting poll for done event..." << ENDLOG;
		return true;
		}
	    if ( fNotDoneWarnRepeat && (lastWarn.tv_sec || lastWarn.tv_usec) )
		tdiff = (now.tv_sec-lastWarn.tv_sec)*1000000ULL+(now.tv_usec-lastWarn.tv_usec);
	    if ( fNotDoneWarnTime_us && tdiff>fNotDoneWarnTime_us && (fNotDoneWarnRepeat || (!lastWarn.tv_sec && !lastWarn.tv_usec)) )
		{
		LOG( AliHLTLog::kWarning, "AliHLTCRORCOutWriterSubscriber::ProcessEvent", "Polling for done event" )
		    << "No done for event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ") at HLTOut handler after " << MLUCLog::kDec << fNotDoneWarnTime_us << " microsecs.. Continuing waiting..." << ENDLOG;
		lastWarn = now;
		}
	    }
	if ( fDoSleep )
	    usleep( fSleepTime );
	}

    return true;
}

void AliHLTCRORCOutWriterSubscriber::UpdateEventStatistics(
    const AliHLTSubEventDataDescriptor *sedd) {
  if (!fStatus || sedd->fEventID.fType != kAliEventTypeData) {
    return;
  }

  /** event lifetime statistics **/
  if (sedd->fEventBirth_s && sedd->fEventBirth_us) {
    struct timeval now;
    gettimeofday(&now, NULL);
    long long tdiff = (now.tv_sec - sedd->fEventBirth_s) * 1000000ULL +
                      (now.tv_usec - sedd->fEventBirth_us);
    if (tdiff < 0) {
      tdiff = 0;
    }
    if (fCurrentMaxEventLifetime_us == 0 || fClearCurrentLifetimeCounters ||
        fCurrentMaxEventLifetime_us < tdiff) {
      fCurrentMaxEventLifetime_us = tdiff;
    }
    if (fCurrentMinEventLifetime_us == 0 || fClearCurrentLifetimeCounters ||
        fCurrentMinEventLifetime_us > tdiff) {
      fCurrentMinEventLifetime_us = tdiff;
    }

    if (fClearCurrentLifetimeCounters) {
      fClearCurrentLifetimeCounters = false;
      fCurrentSumOfLifetimes_us = 0;
    } else {
      fCurrentSumOfLifetimes_us += tdiff;
    }

    if (fStatus->fHLTStatus.fTotalMaxEventLifetime <
        fCurrentMaxEventLifetime_us) {
      fStatus->fHLTStatus.fTotalMaxEventLifetime = fCurrentMaxEventLifetime_us;
    }
    if ((fCurrentMinEventLifetime_us <
        fStatus->fHLTStatus.fTotalMinEventLifetime) ||
        fStatus->fHLTStatus.fTotalMinEventLifetime==0) {
      fStatus->fHLTStatus.fTotalMinEventLifetime = fCurrentMinEventLifetime_us;
    }
  }

  /** maximum out-of-order event ID distance **/
  if (fHighestEventId < sedd->fEventID) {
    fHighestEventId = sedd->fEventID;
  }
  if (sedd->fEventID < fHighestEventId) {
    unsigned long long eventIdDiff = ((fHighestEventId - sedd->fEventID) >> 12);
    if (eventIdDiff > fStatus->fHLTStatus.fMaxOutOfOrderOrbits) {
      fStatus->fHLTStatus.fMaxOutOfOrderOrbits = eventIdDiff;
    }
  }
}

void AliHLTCRORCOutWriterSubscriber::StopProcessing() {
  fQuitDonePoll = true;
  fHLTOutHandler->DeactivateRORC();
  AliHLTProcessingSubscriber::StopProcessing();

  if (fStatus && fEorXoffStats) {
    AliUInt64_t xoff_ms = fHLTOutHandler->XoffToUs(fTotalDdlXoffCount) / 1000;
    LOG(AliHLTLog::kImportant,
        "AliHLTCRORCOutWriterSubscriber::StopProcessing", "XOFF Count")
        << "XOFF from DAQ: Received a total of " << xoff_ms
        << " ms XOFF from DAQ [" << LOG_DEC_HEX(fTotalDdlXoffCount) << "]."
        << ENDLOG;
  }
}

void AliHLTCRORCOutWriterSubscriber::EventRateTimerExpired() {
  AliHLTProcessingSubscriber::EventRateTimerExpired();

  struct timeval now;
  gettimeofday(&now, NULL);

  struct AliHLTRORCLinkStatus status;
  if (fHLTOutHandler->ReadRORCLinkStatus(status) != 0) {
    return;
  }

  if (!status.transceiver_up) {
    LOG(AliHLTLog::kError, "AliHLTCRORCPublisher::EventRateTimerExpired",
        "Transceiver Status")
        << "DDL down on transceiver level - check fiber" << ENDLOG;
  } else {
    if (status.transceiver_errors) {
      LOG(AliHLTLog::kError, "AliHLTCRORCPublisher::EventRateTimerExpired",
          "Transceiver Status")
          << "Lossy DDL connection - check fiber. Loss-Of-Sync: "
          << LOG_DEC_HEX(status.gtx_rxlossofsync_count)
          << ", Not-In-Table: " << LOG_DEC_HEX(status.gtx_rxnotintable_count)
          << ", Disparity-Errors: "
          << LOG_DEC_HEX(status.gtx_rxdispartity_count) << ENDLOG;
    }

    AliUInt32_t xoff_diff = status.ddl_xoff_count - fLastDdlXoffCount;
    unsigned long long xoff_diff_ms =
        fHLTOutHandler->XoffToUs(xoff_diff) / 1000;
    unsigned long long time_diff_ms =
        timediff_us(fLastTimerExpired, now) / 1000;
    int busy_percent = xoff_diff_ms * 100 / time_diff_ms;

    if ((fLastDdlXoffCount != 0xffffffff) &&
        busy_percent > DDL_XOFF_PERCENT_THRESHOLD) {
      fEorXoffStats = true;
      LOG(AliHLTLog::kWarning, "AliHLTCRORCPublisher::EventRateTimerExpired",
          "XOFF")
          << "XOFF from DAQ: " << xoff_diff_ms << " ms (~" << busy_percent
          << "%) in the last " << time_diff_ms << " ms." << ENDLOG;
    }

    /**
    * ddl_xoff_count is a 32b integer that (in worst case) could wrap
    * every ~16 seconds (0xffffffff/DDL_CLOCK_RATE). Keeping track of the
    * total deadtime in SW...
    **/
    fTotalDdlXoffCount += xoff_diff;
  }

  fLastDdlXoffCount = status.ddl_xoff_count;

  if (fStatus) {
      if(fStatus->fHLTStatus.fCurrentProcessedEventCount) {
        fStatus->fHLTStatus.fCurrentAvgEventLifetime = fCurrentSumOfLifetimes_us / fStatus->fHLTStatus.fCurrentProcessedEventCount;
      } else {
        fStatus->fHLTStatus.fCurrentAvgEventLifetime = 0;
      }
      fClearCurrentLifetimeCounters = true;
  }
  fLastTimerExpired = now;
}

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
