#ifndef _ALIHLTDDLHEADERPUBLISHER_HPP_
#define _ALIHLTDDLHEADERPUBLISHER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTDetectorPublisher.hpp"
#include "AliHLTDDLHeader.hpp"
#include "AliHLTDDLHeaderData.h"

class AliHLTDDLHeaderPublisher: public AliHLTDetectorPublisher
    {
    public:

	AliHLTDDLHeaderPublisher( const char* name, int eventSlotsPowerOfTwo = -1 );
	virtual ~AliHLTDDLHeaderPublisher();

    protected:

	AliUInt32_t fCreateEvents;
	AliUInt64_t fEventCounter;

	virtual int WaitForEvent( AliEventID_t& eventID, AliHLTSubEventDataDescriptor*& sbevent,
				  AliHLTEventTriggerStruct*& trg ); // Called with no locked mutexes
	virtual void EventFinished( AliEventID_t, AliHLTSubEventDataDescriptor& sbevent, vector<AliHLTEventDoneData*>& eventDoneData ); // Called with no locked mutexes
	virtual void EventFinished( AliEventID_t, vector<AliHLTEventDoneData*>& eventDoneData ); // Called with no locked mutexes
	virtual void QuitEventLoop(); // Called with no locked mutexes
	bool QuittedEventLoop()
		{
		return fEventLoopQuitted;
		}
	virtual void StartEventLoop();
	virtual void EndEventLoop();



	bool fQuitEventLoop;
	bool fEventLoopQuitted;

	AliHLTNodeID_t fNodeID;

	AliHLTEventTriggerStruct fETS;
	AliHLTEventTriggerStruct* fETP;
	AliUInt32_t fETPOffset;
	AliUInt32_t fETPIndex;

	struct EventData
	    {
		AliEventID_t fEventID;
		AliUInt32_t fBufferNdx;
		AliUInt32_t fBufferOffset;
	    };

	MLUCVector<EventData> fEventData;
	pthread_mutex_t fEventDataMutex;

	static bool FindEventData( const EventData& ed, const AliEventID_t& referenceEvent )
		{
		return ed.fEventID == referenceEvent;
		}
	
    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTDDLHEADERPUBLISHER_HPP_
