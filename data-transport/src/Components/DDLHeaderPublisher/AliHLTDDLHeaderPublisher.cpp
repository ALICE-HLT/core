/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTDDLHeaderPublisher.hpp"
#include "AliHLTDDLHeaderData.h"
#include "AliHLTDDLHeader.hpp"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTEventDataType.h"
#include "AliHLTLog.hpp"
#include "AliHLTGetNodeID.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTBufferManager.hpp"
#include "AliHLTShmManager.hpp"
#include "AliHLTSubEventDescriptor.hpp"
#include <time.h>
#include <unistd.h>
#include <sys/types.h>

AliHLTDDLHeaderPublisher::AliHLTDDLHeaderPublisher( const char* name, int eventSlotsPowerOfTwo ):
    AliHLTDetectorPublisher( name, eventSlotsPowerOfTwo )
    {
    fQuitEventLoop = false;
    fEventLoopQuitted = false;
    fEventCounter=0;
    fCreateEvents = 1;

    fETP = &fETS;
    fETP->fHeader.fLength = sizeof(AliHLTEventTriggerStruct);
    fETP->fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
    fETP->fHeader.fSubType.fID = 0;
    fETP->fHeader.fVersion = 1;
    fETP->fDataWordCount = 0;

    fNodeID = AliHLTGetNodeID();

    pthread_mutex_init( &fEventDataMutex, NULL );
    }

AliHLTDDLHeaderPublisher::~AliHLTDDLHeaderPublisher()
    {
    pthread_mutex_destroy( &fEventDataMutex );
    }

int AliHLTDDLHeaderPublisher::WaitForEvent( AliEventID_t& eventID, AliHLTSubEventDataDescriptor*& sbevent,
				  AliHLTEventTriggerStruct*& trg )
    {
    sbevent = NULL;
    trg = NULL;
    unsigned long offset=0, size=0;
    AliUInt8_t* ptr = NULL;
    bool found = false;
    AliUInt32_t bufferNdx, bufferSize;
    

#if __GNUC__>=3
    AliHLTShmID shmKey = { kInvalidShmType, {~(AliUInt64_t)0 } };
#else
    AliHLTShmID shmKey = { kInvalidShmType, {{~(AliUInt64_t)0 }} };
#endif

    if ( !fBufferManager )
	{
	LOG( AliHLTLog::kError, "AliHLTMultFileBenchPublisher::WaitForEvent", "No Buffer Manager" )
	    << "No buffer manager specified. Aborting..." << ENDLOG;
	return 0;
	}
    if ( !fShmManager )
	{
	LOG( AliHLTLog::kError, "AliHLTMultFileBenchPublisher::WaitForEvent", "No Shm Manager" )
	    << "No shared memory manager specified. Aborting..." << ENDLOG;
	// ...
	return 0;
	}
    if ( !fDescriptors )
	{
	LOG( AliHLTLog::kError, "AliHLTMultFileBenchPublisher::WaitForEvent", "No Descriptor Handler" )
	    << "No descriptor handler specified. Aborting..." << ENDLOG;
	return 0;
	}

    if ( !fQuitEventLoop )
	{
	while ( !fCreateEvents && !fQuitEventLoop )
	    {
	    usleep( 10000 );
	    //usleep( 0 );
	    }
	found = false;
	do
	    {
	    size = bufferSize = AliHLTDDLHeaderDataV1Size;
	    found = fBufferManager->GetBlock( bufferNdx, offset, size );
	    if ( found && size < bufferSize )
		{
		found = false;
		fBufferManager->ReleaseBlock( bufferNdx, offset );
		}
	    if ( !found && !fQuitEventLoop )
		usleep( 0 );
	    }
	while ( !found && !fQuitEventLoop );
	if ( found )
	    {
	    eventID = fEventCounter;
	    shmKey = fBufferManager->GetShmKey( bufferNdx );
	    ptr = fShmManager->GetShm( shmKey );
	    ptr += offset;
	    memset( ptr, 0, bufferSize );
	    
	    AliHLTSetDDLHeaderBlockLength( ptr, 0xFFFFFFFF );
	    AliHLTSetDDLHeaderVersion( ptr, 1 );
	    AliHLTSetDDLHeaderL1TriggerType( ptr, 1 );
	    AliUInt32_t eventID1 = eventID.fNr & (unsigned)0xFFF;
	    AliHLTSetDDLHeaderEventID1( ptr, eventID1 );
	    AliUInt32_t eventID2 = (eventID.fNr & 0xFFFFFF000ULL) >> (unsigned)12;
	    AliHLTSetDDLHeaderEventID2( ptr, eventID2 );
	    AliHLTSetDDLHeaderPartSubDet( ptr, 1 );
	    AliHLTSetDDLHeaderBlockAttr( ptr, 0 );
	    AliHLTSetDDLHeaderMiniEventID( ptr, eventID1 );
	    AliHLTSetDDLHeaderStatusError( ptr, 0 );
	    AliHLTSetDDLHeaderTriggerClassesLow50( ptr, 1ULL );
	    AliHLTSetDDLHeaderTriggerClassesHigh50( ptr, 0ULL );
	    AliHLTSetDDLHeaderROI( ptr, 0ULL );

	    pthread_mutex_lock( &fSEDDMutex );
	    sbevent = fDescriptors->GetFreeEventDescriptor( eventID, 1 );
	    pthread_mutex_unlock( &fSEDDMutex );

	    struct timeval now;
	    gettimeofday( &now, NULL );
	    sbevent->fEventID = eventID;
	    sbevent->fEventBirth_s = now.tv_sec;
	    sbevent->fEventBirth_us = now.tv_usec;
	    sbevent->fOldestEventBirth_s = 0; // fOldestEventBirth;
	    sbevent->fDataBlocks[0].fBlockOffset = offset;
	    sbevent->fDataBlocks[0].fBlockSize = bufferSize;
	    sbevent->fDataBlocks[0].fShmID.fShmType = shmKey.fShmType;
	    sbevent->fDataBlocks[0].fShmID.fKey.fID = shmKey.fKey.fID;
	    sbevent->fDataBlocks[0].fDataType.fID = DDLHEADER_DATAID;
	    sbevent->fDataBlocks[0].fDataOrigin.fID = TRD_DATAORIGIN;
	    sbevent->fDataBlocks[0].fDataSpecification = 0;
	    sbevent->fDataBlocks[0].fStatusFlags = 0;
	    //sbevent->fDataBlocks[0].fByteOrder = kAliHLTNativeByteOrder;
	    AliHLTSubEventDescriptor::FillBlockAttributes( sbevent->fDataBlocks[0].fAttributes );
	    sbevent->fDataBlocks[0].fProducerNode = fNodeID;
	    sbevent->fDataType.fID = COMPOSITE_DATAID;
	    trg = fETP;

	    fEventCounter++;

	    struct EventData ed;
	    ed.fEventID = eventID;
	    ed.fBufferNdx = bufferNdx;
	    ed.fBufferOffset = offset;
	    pthread_mutex_lock( &fEventDataMutex );
	    fEventData.Add( ed );
	    pthread_mutex_unlock( &fEventDataMutex );
	    }
	else // of if ( found )
	    {
	    if ( !fQuitEventLoop )
		{
		LOG( AliHLTLog::kWarning, "AliHLTDDLHeaderPublisher", "Getting shared memory block" )
		    << "Unable to get shared memory block of size " << AliHLTLog::kDec << bufferSize << " bytes."
		    << ENDLOG;
		}
	    }
	return 1;
	}
    else
	{
	fEventLoopQuitted = true;
	return 0;
	}
    }

void AliHLTDDLHeaderPublisher::EventFinished( AliEventID_t eventID, AliHLTSubEventDataDescriptor&, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    EventFinished( eventID, eventDoneData );
    }

void AliHLTDDLHeaderPublisher::EventFinished( AliEventID_t eventID, vector<AliHLTEventDoneData*>& )
    {
    unsigned long ndx;
    pthread_mutex_lock( &fEventDataMutex );
    if ( MLUCVectorSearcher<EventData,AliEventID_t>::FindElement( fEventData, &FindEventData, eventID, ndx ) )
	{
	EventData* ped;
	ped = fEventData.GetPtr( ndx );
#if __GNUC__>=3
	AliHLTShmID shmKey = { kInvalidShmType, {~(AliUInt64_t)0 } };
#else
	AliHLTShmID shmKey = { kInvalidShmType, {{~(AliUInt64_t)0 }} };
#endif
	if ( fBufferManager )
	    {
	    shmKey = fBufferManager->GetShmKey( ped->fBufferNdx );
	    fBufferManager->ReleaseBlock( ped->fBufferNdx, ped->fBufferOffset );
	    }
	if ( fShmManager && shmKey.fShmType != kInvalidShmType )
	    fShmManager->ReleaseShm( shmKey );
	fEventData.Remove( ndx );
	}
    pthread_mutex_unlock( &fEventDataMutex );
    }

void AliHLTDDLHeaderPublisher::QuitEventLoop()
    {
    fQuitEventLoop = true;
    }

void AliHLTDDLHeaderPublisher::StartEventLoop()
    {
    if ( fPersistentState )
	fEventCounter = fPersistentState->fLastEventID.fNr+(unsigned)1;
    }

void AliHLTDDLHeaderPublisher::EndEventLoop()
    {
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
