CMAKE_MINIMUM_REQUIRED(VERSION 2.6)

PROJECT(DDLHeaderPublisher)

ADD_DEFINITIONS(-Wno-multichar)

INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/MLUC/include
  ${CMAKE_SOURCE_DIR}/BCL/include
  ${CMAKE_BINARY_DIR}/BCL/src
  ${CMAKE_SOURCE_DIR}/TaskManager/include
  ${CMAKE_SOURCE_DIR}/PSI2/include
  ${CMAKE_SOURCE_DIR}/Util/HOMER/include
  ${CMAKE_SOURCE_DIR}/Util/HLTReadoutList
  ${CMAKE_SOURCE_DIR}/Util/NOPEEvent/include
  ${CMAKE_SOURCE_DIR}/Framework/Trigger/include
  ${CMAKE_SOURCE_DIR}/Framework/Base/include
  ${CMAKE_SOURCE_DIR}/Framework/DDL/include
  ${CMAKE_SOURCE_DIR}/Framework/SC/include
  ${CMAKE_BINARY_DIR}/Framework/SC/src
  ${CMAKE_SOURCE_DIR}/Framework/WorkerComp/include
  ${CMAKE_SOURCE_DIR}/Framework/PubSub/include
  )


ADD_EXECUTABLE(DDLHeaderPublisher DDLHeaderPublisher.cpp AliHLTDDLHeaderPublisher.cpp)

TARGET_LINK_LIBRARIES(DDLHeaderPublisher MLUC BCL Base PubSub SC Trigger DDL WorkerComp HLTReadoutList)

INSTALL(FILES AliHLTDDLHeaderPublisher.hpp DESTINATION include/ddlheaderpublisher)
INSTALL(TARGETS DDLHeaderPublisher DESTINATION bin)
