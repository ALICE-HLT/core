/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTDDLHeaderPublisher.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTLog.hpp"
#include <vector>

class DDLHeaderPublisherComponent: public AliHLTControlledSourceComponent
    {
    public:

	DDLHeaderPublisherComponent( const char* name, int argc, char** argv );
	virtual ~DDLHeaderPublisherComponent() {};


    protected:

	virtual AliHLTDetectorPublisher* CreatePublisher();

    private:
    };


DDLHeaderPublisherComponent::DDLHeaderPublisherComponent( const char* name, int argc, char** argv ):
    AliHLTControlledSourceComponent( name, argc, argv )
    {
    }


AliHLTDetectorPublisher* DDLHeaderPublisherComponent::CreatePublisher()
    {
    return new AliHLTDDLHeaderPublisher( fOwnPublisherName.c_str(), fMaxPreEventCountExp2 );
    }


int main( int argc, char** argv )
    {
    DDLHeaderPublisherComponent procComp( "DDLHeaderPublisher", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
