/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: EventDoneRequestTestDummy.cpp 1730 2007-03-23 11:42:00Z timm $ 
**
***************************************************************************
*/

#include "AliHLTProcessingSubscriber.hpp"
//#include "AliHLTControlledProcessComponent.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTTimer.hpp"


class EventDoneRequestTestDummy: public AliHLTProcessingSubscriber
    {
    public:
	EventDoneRequestTestDummy( const char* name, bool sendEventDone, AliUInt32_t minBlockSize,
				   AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, 
				   bool requestReplays, 
				   bool requestFirstOrbitEvents,
				   unsigned long orbitEventsRequestTime_us,
				   unsigned long fOrbitEventsRequestInterval_us,
				   unsigned long fOrbitEventsRequestModulo,
				   int eventCountAlloc ):
	    AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc ),
		fRequestReplays(requestReplays),
		fRequestFirstOrbitEvents(requestFirstOrbitEvents),
	    fOrbitEventsRequestTime_us(orbitEventsRequestTime_us),
	    fOrbitEventsRequestInterval_us(fOrbitEventsRequestInterval_us),
	    fOrbitEventsRequestModulo(fOrbitEventsRequestModulo),
	    fDataEventCount(0)
		{
		fLastEvent.tv_sec = fLastEvent.tv_usec = 0;
		}

	

    protected:

	bool ProcessEvent( AliEventID_t eventID, AliUInt32_t, AliHLTSubEventDescriptor::BlockData*, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct*,
			   AliUInt8_t*, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>&, AliHLTEventDoneData*& edd )
		{
		if ( eventID.fType == kAliEventTypeDataReplay )
		    {
		    LOG( AliHLTLog::kInformational, "EventDoneRequestTestDummy::ProcessEvent", "Replay event received" )
			<< "Received replay event 0x" << AliHLTLog::kHex << eventID.fNr << " (" << AliHLTLog::kDec
			<< eventID.fNr << ")." << ENDLOG;
		    }
		if ( eventID.fType==kAliEventTypeData && (sedd->fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_FULLPASSTHROUGH) )
		    {
		    LOG( AliHLTLog::kInformational, "EventDoneRequestTestDummy::ProcessEvent", "Monitor event received" )
			<< "Received monitor event 0x" << AliHLTLog::kHex << eventID.fNr << " (" << AliHLTLog::kDec
			<< eventID.fNr << ")." << ENDLOG;
		    }
		unsigned long reqWordCount=0;
		bool requestReplay=false;
		bool requestMonitor=false;
		if ( eventID.fType==kAliEventTypeData && fRequestReplays )
		    {
		    reqWordCount += 3;
		    requestReplay = true;
		    }
		AliUInt64_t reqOrbit = ( (eventID.fNr + ( (fOrbitEventsRequestTime_us / 88) << 12 ) ) & 0xFFFFFFFFFFFFF000ULL );
		if ( ( eventID.fType==kAliEventTypeData || eventID.fType==kAliEventTypeTick)  && fRequestFirstOrbitEvents && reqOrbit>0 )
		    {
		    if ( !fOrbitEventsRequestInterval_us && !fOrbitEventsRequestModulo && eventID.fType==kAliEventTypeData )
			requestMonitor = true;
		    if ( fOrbitEventsRequestInterval_us )
			{
			if ( !fLastEvent.tv_sec && !fLastEvent.tv_usec )
			    {
			    gettimeofday( &fLastEvent, NULL );
			    requestMonitor = true;
			    }
			else
			    {
			    struct timeval now;
			    gettimeofday( &now, NULL );
			    unsigned long long tdiff = now.tv_sec-fLastEvent.tv_sec;
			    tdiff += 1000000;
			    tdiff += now.tv_usec-fLastEvent.tv_usec;
			    if ( tdiff >= fOrbitEventsRequestInterval_us )
				{
				requestMonitor = true;
				fLastEvent = now;
				}
			    }
			}
		    if ( eventID.fType==kAliEventTypeData && fOrbitEventsRequestModulo && !(fDataEventCount % fOrbitEventsRequestModulo) )
			requestMonitor = true;
		    if ( requestMonitor )
			reqWordCount += 3;
		    }
		if ( reqWordCount )
		    {
		    edd = GetEventDoneData(eventID, reqWordCount );
		    if ( !edd )
			return false;
		    unsigned ii=0;
		    if ( requestReplay )
			{
			edd->fDataWords[ii++] = kAliHLTEventDoneMonitorReplayEventIDCmd;
			edd->fDataWords[ii++] = (AliUInt32_t)( eventID.fNr & 0xFFFFFFFF );
			edd->fDataWords[ii++] = (AliUInt32_t)( (eventID.fNr & 0xFFFFFFFF00000000ULL) >> 32 );
			LOG( AliHLTLog::kDebug, "EventDoneRequestTestDummy::ProcessEvent", "Replay event" )
			    << "Added 0x" << AliHLTLog::kHex << eventID.fNr << " (" << AliHLTLog::kDec
			    << eventID.fNr << ") as replay event request." << ENDLOG;
			}
		    if ( requestMonitor )
			{
			edd->fDataWords[ii++] = kAliHLTEventDoneMonitorFirstOrbitEventCmd;
			edd->fDataWords[ii++] = (AliUInt32_t)( reqOrbit & 0xFFFFFFFF );
			edd->fDataWords[ii++] = (AliUInt32_t)( (reqOrbit & 0xFFFFFFFF00000000ULL) >> 32 );
			LOG( AliHLTLog::kDebug, "EventDoneRequestTestDummy::ProcessEvent", "First orbit event" )
			    << "Added 0x" << AliHLTLog::kHex << eventID.fNr << " (" << AliHLTLog::kDec
			    << eventID.fNr << ") as first orbit event request (0x" << AliHLTLog::kHex
			    << reqOrbit << " (" << AliHLTLog::kDec << reqOrbit << "))." << ENDLOG;
			}
		    }
		if ( eventID.fType==kAliEventTypeData )
		    ++fDataEventCount;
		    
		size = 0;
		return true;
		}

	bool fRequestReplays;
	bool fRequestFirstOrbitEvents;
	unsigned long fOrbitEventsRequestTime_us;
	unsigned long fOrbitEventsRequestInterval_us;
	unsigned long fOrbitEventsRequestModulo;

	unsigned long long fDataEventCount;
	struct timeval fLastEvent;

    };


class EventDoneRequestTestDummyComponent: public AliHLTControlledSinkComponent
    {
    public:

	EventDoneRequestTestDummyComponent( const char* name, int argc, char** argv );
	virtual ~EventDoneRequestTestDummyComponent() {};


    protected:

	//virtual AliHLTProcessingSubscriber* CreateSubscriber();
	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

	virtual AliHLTProcessingSubscriber* CreateSubscriber();

	bool fRequestReplays;
	bool fRequestFirstOrbitEvents;
	unsigned long fOrbitEventsRequestTime_us;
	unsigned long fOrbitEventsRequestInterval_us;
	unsigned long fOrbitEventsRequestModulo;

    private:
    };


EventDoneRequestTestDummyComponent::EventDoneRequestTestDummyComponent( const char* name, int argc, char** argv ):
    AliHLTControlledSinkComponent( name, argc, argv ),
    fRequestReplays( false ),
    fRequestFirstOrbitEvents( false ),
    fOrbitEventsRequestTime_us(  0 ),
    fOrbitEventsRequestInterval_us(0),
    fOrbitEventsRequestModulo(0)
    {
    }


const char* EventDoneRequestTestDummyComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cpErr = NULL;
    if ( !strcmp( argv[i], "-replays" ) )
	{
	fRequestReplays = true;
	i += 1;
	return NULL;
	}
    if ( !strcmp( argv[i], "-monitor" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing monitor request advance time specifier.";
	    }
	fRequestFirstOrbitEvents = true;
	fOrbitEventsRequestTime_us = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting monitor request advance time specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-monitorinterval" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing monitor request interval time specifier.";
	    }
	fOrbitEventsRequestInterval_us = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting monitor request interval time specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-monitormodulo" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing monitor event modulo counter specifier.";
	    }
	fOrbitEventsRequestModulo = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting monitor event modulo counter specifier.";
	    }
	i += 2;
	return NULL;
	}
    return AliHLTControlledSinkComponent::ParseCmdLine( argc, argv, i, errorArg );
    }

void EventDoneRequestTestDummyComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    AliHLTControlledSinkComponent::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-replay: Specify to request a replay foe ach event. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-monitor <request-advance-time-us>: Specify to request monitoring events and the advance time to request (in microseconds). (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-monitorinterval <request-interval-time-us>: Specify the interval (in microseconds) in which to request monitoring events. (Optional, default 0 (every event))" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-monitormodulo <event-modulo-counter>: Specify a modulo counter with which to request monitoring events. (Optional, default 0 (every event))" << ENDLOG;
    }

AliHLTProcessingSubscriber* EventDoneRequestTestDummyComponent::CreateSubscriber()
    {
    return new EventDoneRequestTestDummy( fSubscriberID.c_str(), fSendEventDone, fMinBlockSize, 
					  fPerEventFixedSize, fPerBlockFixedSize, fSizeFactor, 
					  fRequestReplays,
					  fRequestFirstOrbitEvents,
					  fOrbitEventsRequestTime_us,
					  fOrbitEventsRequestInterval_us,
					  fOrbitEventsRequestModulo,
					  fMaxPreEventCountExp2 );
    }


int main( int argc, char** argv )
    {
    EventDoneRequestTestDummyComponent procComp( "EventDoneRequestTestDummyComponent", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }


/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: EventDoneRequestTestDummy.cpp 1730 2007-03-23 11:42:00Z timm $ 
**
***************************************************************************
*/

