/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTProcessingSubscriber.hpp"
//#include "AliHLTControlledProcessComponent.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTTimer.hpp"


class SingleBlockUnwrapperSubscriber: public AliHLTProcessingSubscriber
    {
    public:
	SingleBlockUnwrapperSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize, 
					AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, int eventCountAlloc ):
	    AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc )
		{
		}

    protected:

	void StartProcessing()
		{
		AliHLTProcessingSubscriber::StartProcessing();
		}

	bool ProcessEvent( AliEventID_t, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, const AliHLTSubEventDataDescriptor*, const AliHLTEventTriggerStruct*,
			   AliUInt8_t* outputPtr, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, AliHLTEventDoneData*& )
		{
		if ( blockCnt < 2 )
		    {
		    AliUInt32_t n;
		    for ( n = 0; n < blockCnt; n++ )
			outputBlocks.insert( outputBlocks.end(), blocks[n] );
		    return true;
		    }
		AliUInt32_t n = 0;
		unsigned long long usedSize = 0;
		while ( n < blockCnt-1 )
		    {
		    bool attributesEqual = true;
		    if ( blocks[n].fAttributes[kAliHLTSEDBDByteOrderAttributeIndex]!=blocks[n+1].fAttributes[kAliHLTSEDBDByteOrderAttributeIndex] ||
			 blocks[n].fAttributes[kAliHLTSEDBDByteOrderAttributeIndex]==kAliHLTUnknownAttribute ||
			 blocks[n].fAttributes[kAliHLTSEDBDByteOrderAttributeIndex]==kAliHLTUnspecifiedAttribute )
			attributesEqual = false;
		    for ( int j = kAliHLTSEDBDAlignmentAttributeIndexStart; j <= kAliHLTSEDBDLastAlignmentAttributeIndex; j++ )
			{
			if ( blocks[n].fAttributes[j]!=blocks[n+1].fAttributes[j] ||
			     blocks[n].fAttributes[j]==kAliHLTUnknownAttribute ||
			     blocks[n].fAttributes[j]==kAliHLTUnspecifiedAttribute )
			    attributesEqual = false;
			}
		    if ( blocks[n+1].fOffset==0 && 
			 blocks[n].fShmKey.fShmType == blocks[n+1].fShmKey.fShmType &&
			 blocks[n].fShmKey.fKey.fID == blocks[n+1].fShmKey.fKey.fID &&
			 blocks[n].fProducerNode == blocks[n+1].fProducerNode &&
			 attributesEqual &&
			 blocks[n].fDataType.fID==blocks[n+1].fDataType.fID &&
			 blocks[n].fDataOrigin.fID==blocks[n+1].fDataOrigin.fID && 
			 blocks[n].fDataSpecification==blocks[n+1].fDataSpecification )
			{
			if ( usedSize+blocks[n].fSize+blocks[n+1].fSize<=size )
			    {
			    memcpy( outputPtr, blocks[n].fData, blocks[n].fSize );
			    memcpy( outputPtr+blocks[n].fSize, blocks[n+1].fData, blocks[n+1].fSize );
			    AliHLTSubEventDescriptor::BlockData obd;
			    obd.fShmKey.fShmType = kInvalidShmType;
			    obd.fShmKey.fKey.fID = (AliUInt64_t)-1;
			    obd.fOffset = usedSize;
			    obd.fSize = blocks[n].fSize+blocks[n+1].fSize;
			    obd.fProducerNode = blocks[n].fProducerNode;
			    obd.fDataType = blocks[n].fDataType;
			    obd.fDataOrigin = blocks[n].fDataOrigin;
			    obd.fDataSpecification = blocks[n].fDataSpecification;
			    //obd.fByteOrder = blocks[n].fByteOrder;
			    for ( int j = 0; j < kAliHLTSEDBDAttributeCount; j++ )
				obd.fAttributes[j] = blocks[n].fAttributes[j];
			    outputBlocks.insert( outputBlocks.end(), obd );
			    usedSize += obd.fSize;
			    }
			else
			    {
			    LOG( AliHLTLog::kWarning, "SingleBlockUnwrapperSubscriber::ProcessEvent", "Output block too small" )
				<< "Output buffer too small (" << AliHLTLog::kDec << size
				<< " (0x" << AliHLTLog::kHex << size << ") bytes) to serialize blocks "
				<< AliHLTLog::kDec << n << " and " << n+1 << " of size " 
				<< AliHLTLog::kDec << blocks[n].fSize << " (0x" << AliHLTLog::kHex << blocks[n].fSize
				<< ") bytes and " << AliHLTLog::kDec << blocks[n+1].fSize << " (0x" << AliHLTLog::kHex
				<< blocks[n+1].fSize << ") bytes with previously serialized blocks of " << AliHLTLog::kDec
				<< usedSize << " (0x" << AliHLTLog::kHex << usedSize << ") bytes." << ENDLOG;
			    return false;
			    }
			n += 2;
			}
		    else
			{
			outputBlocks.insert( outputBlocks.end(), blocks[n] );
			n++;
			}
		    }
		while ( n < blockCnt )
		    {
		    outputBlocks.insert( outputBlocks.end(), blocks[n] );
		    n++;
		    }
		size = usedSize;
		return true;
		}

    };


class SingleBlockUnwrapperComponent: public AliHLTControlledProcessorComponent
    {
    public:

	SingleBlockUnwrapperComponent( const char* name, int argc, char** argv );
	virtual ~SingleBlockUnwrapperComponent() {};


    protected:

	virtual AliHLTProcessingSubscriber* CreateSubscriber();

    private:
    };


SingleBlockUnwrapperComponent::SingleBlockUnwrapperComponent( const char* name, int argc, char** argv ):
    AliHLTControlledProcessorComponent( name, argc, argv )
    {
    }


AliHLTProcessingSubscriber* SingleBlockUnwrapperComponent::CreateSubscriber()
    {
    return new SingleBlockUnwrapperSubscriber( fSubscriberID.c_str(), fSendEventDone, fMinBlockSize, 
					       fPerEventFixedSize, fPerBlockFixedSize, fSizeFactor, fMaxPreEventCountExp2 );
    }


int main( int argc, char** argv )
    {
    SingleBlockUnwrapperComponent procComp( "SingleBlockUnwrapperComponent", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

