#include "Avahi.hpp"
#include <pthread.h>
#include "AliHLTLog.hpp"
#include <iostream>

#ifdef USE_AVAHI

#if 0
AvahiWatch* watch_new(const AvahiPoll *api, int fd, AvahiWatchEvent event, AvahiWatchCallback callback, void *userdata)
#else
AvahiWatch* AvahiBase::watch_new(const AvahiPoll *api, int fd, AvahiWatchEvent event, AvahiWatchCallback callback, void *userdata)
#endif
{
    AvahiWatch* w=new AvahiWatch;
    w->dead=false;
    w->callback=callback;
    w->userdata=userdata;
    w->event=event;
    w->fd=fd;
    ((AvahiBase*)api->userdata)->watches.push_back(w);
    return w;
}

#if 0
void watch_update(AvahiWatch* w,AvahiWatchEvent event)
#else
void AvahiBase::watch_update(AvahiWatch* w,AvahiWatchEvent event)
#endif
{
    w->event=event;
}

#if 0
AvahiWatchEvent watch_get_events(AvahiWatch *w)
#else
AvahiWatchEvent AvahiBase::watch_get_events(AvahiWatch *w)
#endif
{
    return w->event;
}

#if 0
void watch_free(AvahiWatch* w)
#else
void AvahiBase::watch_free(AvahiWatch* w)
#endif
{
    w->dead=true;
}

#if 0
AvahiTimeout* timeout_new(const AvahiPoll* api, const struct timeval *tv, AvahiTimeoutCallback callback, void *userdata)
#else
AvahiTimeout* AvahiBase::timeout_new(const AvahiPoll* api, const struct timeval *tv, AvahiTimeoutCallback callback, void *userdata)
#endif
{
	AvahiTimeout* t=new AvahiTimeout;
	if(tv)
	{
		t->expiry=*tv;
		t->enabled=true;
	}
	else
	{
		t->enabled=false;
	}
		
	t->callback=callback;
	t->dead=false;
	t->userdata=userdata;
	((AvahiBase*)api->userdata)->timeouts.push_back(t);
	return t;
}

#if 0
void timeout_update(AvahiTimeout* t, const struct timeval *tv)
#else
void AvahiBase::timeout_update(AvahiTimeout* t, const struct timeval *tv)
#endif
{
	if(tv)
	{
		t->expiry=*tv;
		t->enabled=true;
	}
	else
	{
		t->enabled=false;
	}
}

#if 0
void timeout_free(AvahiTimeout *t)
#else
void AvahiBase::timeout_free(AvahiTimeout *t)
#endif
{
	t->dead=true;
}

#if 0
static void client_callback(AvahiClient* c,AvahiClientState state,void* userdata)
#else
void AvahiBase::client_callback(AvahiClient* c,AvahiClientState state,void* userdata)
#endif
{
	//This is just a wrapper
	((AvahiBase*)userdata)->ClientCallback(c,state);
}

#if 0
static void entry_group_callback(AvahiEntryGroup *g, AvahiEntryGroupState state, void *userdata)
#else
void AvahiBase::entry_group_callback(AvahiEntryGroup *g, AvahiEntryGroupState state, void *userdata)
#endif
{
	//Anoter wrapper
	((AvahiBase*)userdata)->EntryGroupCallback(g,state);
}

void AvahiBase::ClientCallback(AvahiClient */*c*/, AvahiClientState state)
{
	int error;
	switch (state)
	{
        case AVAHI_CLIENT_S_RUNNING:
			pthread_mutex_lock(&ServiceMutex);
			this->state=STATE_OK;
			BreakSelect();
			pthread_mutex_unlock(&ServiceMutex);
            break;

        case AVAHI_CLIENT_FAILURE:
			if(client && avahi_client_errno(client) == AVAHI_ERR_DISCONNECTED)
			{
				LOG( AliHLTLog::kDebug, "AvahiBase::ClientCallback", "Client Failure" )
					<< "Disconnected from avahi deamon, trying to reconnect"<< ENDLOG;
				this->state=STATE_WAITING;
				ServicesChanged=true;
				avahi_client_free(client);
				client=0;
				if (group)
				    avahi_entry_group_free(group);
				group=0;
				client=avahi_client_new(&avahi_poll,AVAHI_CLIENT_NO_FAIL ,client_callback,this,&error);
				if(!client)
				{
					LOG( AliHLTLog::kError, "AvahiBase::ClientCallback", "Creating avahi client" )
						<< "avahi_client_new exited with error" << avahi_strerror(error) << ENDLOG;
					this->state=STATE_QUITED;						 
				}
			}
			else
			{
				this->state=STATE_QUITED;
				LOG( AliHLTLog::kError, "AvahiBase::ClientCallback", "Client Failure" )
					<< "Got AVAHI_CLIENT_FAILURE state change. errno says "<<avahi_strerror(avahi_client_errno(client))
					<< ENDLOG;
				break;
			}

        case AVAHI_CLIENT_S_COLLISION:
        case AVAHI_CLIENT_S_REGISTERING:
			this->state=STATE_WAITING;
            if (group)
                avahi_entry_group_reset(group);
            break;

        case AVAHI_CLIENT_CONNECTING:
			this->state=STATE_WAITING;
			break;
	default:
            ;
    }
}

void AvahiBase::EntryGroupCallback(AvahiEntryGroup *g, AvahiEntryGroupState state)
{
	group = g;
	char *n;

    switch (state) {
        case AVAHI_ENTRY_GROUP_ESTABLISHED :
			LOG( AliHLTLog::kDebug, "AvahiBase::EntryGroupCallback", "Avahi service established" )
				<<"Service '"<<GroupName.c_str()<<"' successfully established"<<ENDLOG;
            break;

        case AVAHI_ENTRY_GROUP_COLLISION :
			//pick a new name
			n=avahi_alternative_service_name(GroupName.c_str());
			GroupName=n;
			delete [] n;
			ServicesChanged=true;
			BreakSelect();
            break;

        case AVAHI_ENTRY_GROUP_FAILURE :
			LOG( AliHLTLog::kError, "AvahiBase::EntryGroupCallback", "Avahi entry group failure" )
				<<"Entry group failure: "<<avahi_strerror(avahi_client_errno(avahi_entry_group_get_client(g)))<<ENDLOG;
            break;

        case AVAHI_ENTRY_GROUP_UNCOMMITED:
        case AVAHI_ENTRY_GROUP_REGISTERING:
            ;
    }
}

void AvahiBase::CreateServices()
{
    int ret;
    char* n;

    if (!group)
    {
        if (!(group = avahi_entry_group_new(client, entry_group_callback, this)))
        {
            state=STATE_QUITED;
            LOG( AliHLTLog::kError, "AvahiBase::CreateServices", "Group Creation failed" )
                << "avahi_entry_group_new() failed: " << avahi_strerror(avahi_client_errno(client)) << ENDLOG;
            return;
        }
    }
    else
    {
        avahi_entry_group_reset(group);
    }
    
    std::map<int,ServiceDescription>::iterator i = Services.begin();    
    for(;i!=Services.end();++i)
    {       
        LOG( AliHLTLog::kDebug, "AvahiBase::CreateServices", "Adding service" )
            <<"Adding "<<i->second.Type.c_str()<<" on port "<<i->second.Port<<" to "<<GroupName.c_str()<<ENDLOG;
        
        
        AvahiStringList * stringList = avahi_string_list_new(NULL, NULL);
        
        std::vector<std::string> txt = i->second.Txt;
        for (int iter = 0; iter < (int)txt.size(); iter++)
        {
            stringList = avahi_string_list_add(stringList, txt[iter].c_str());
        }
        
        ret = avahi_entry_group_add_service_strlst(group,
                                                   AVAHI_IF_UNSPEC,
                                                   AVAHI_PROTO_UNSPEC,
                                                   AVAHI_PUBLISH_UPDATE,
                                                   GroupName.c_str(),
                                                   i->second.Type.c_str(),
                                                   NULL, NULL,
                                                   i->second.Port,
                                                   stringList);
        
        avahi_string_list_free( stringList );
        
        if (ret < 0)
        {
            if (ret == AVAHI_ERR_COLLISION)
            {
                //Pick alternative name
                n=avahi_alternative_service_name(GroupName.c_str());
                GroupName=n;
                delete [] n;
                ServicesChanged=true;
                BreakSelect();
                return;
            }
            LOG( AliHLTLog::kError, "AvahiBase::CreateServices", "Adding service failed" )
                <<"Failed to service:"<<avahi_strerror(ret)<<ENDLOG;
            state=STATE_QUITED;
            return;
        }
    }
    if ((ret = avahi_entry_group_commit(group)) < 0) {
        LOG( AliHLTLog::kError, "AvahiBase::CreateServices", "Adding service failed" )
            <<"Failed to commit entry group:"<<avahi_strerror(ret)<<ENDLOG;
        state=STATE_QUITED;
        return;
    }
    
    return;
}

AvahiBase::AvahiBase()
{
    avahi_poll.userdata=this;
    avahi_poll.watch_new=watch_new;
    avahi_poll.watch_update=watch_update;
    avahi_poll.watch_free=watch_free;
    avahi_poll.watch_get_events=watch_get_events;
    avahi_poll.timeout_new=timeout_new;
    avahi_poll.timeout_free=timeout_free;
    avahi_poll.timeout_update=timeout_update;
    client=0;
    group=0;
    watches.clear();
    timeouts.clear();
    select_break[0]=-1;
    select_break[1]=-1;
    pthread_mutex_init(&ServiceMutex,0);
    state=STATE_QUITED;
    ServicesChanged=false;
}

AvahiBase::~AvahiBase()
{
    Release();
}

bool AvahiBase::Initialize(const char* name)
{
    Release();
    
    GroupName=name;
    state=STATE_WAITING;
    
    int error;
    client=avahi_client_new(&avahi_poll,AVAHI_CLIENT_NO_FAIL ,client_callback,this,&error);
    if(!client)
    {
        LOG( AliHLTLog::kError, "AvahiBase::Initialize", "Creating avahi client" )
            << "avahi_client_new exited with error" << avahi_strerror(error) << ENDLOG;
        return false;
    }
    
    pipe(select_break);
    
    return true;
}

void AvahiBase::Release()
{
    pthread_mutex_lock(&ServiceMutex);
    state=STATE_QUITED;
    
    while(!watches.empty())
    {
        delete *watches.begin();
        watches.pop_front();
    }
    while(!timeouts.empty())
    {
        delete *timeouts.begin();
        timeouts.pop_front();
    }
    if(group)
    {
        avahi_entry_group_free(group);
        group=0;
    }
    if(client)
    {
        avahi_client_free(client);
        client=0;
    }
    if(select_break[0]!=-1)
    {
        close(select_break[0]);
        close(select_break[1]);
        select_break[0]=-1;
        select_break[1]=-1;
    }
    Services.clear();
    pthread_mutex_unlock(&ServiceMutex);
}

int AvahiBase::AddService(const char* type, int port, int id)
{
    pthread_mutex_lock(&ServiceMutex);
    if(id == AVAHI_ID_ANY)
        id = NextFreeServiceId();
    ServiceDescription sd;
    sd.Type = type;
    sd.Port = port;
    sd.Txt = std::vector<std::string>();
    ServicesChanged = true;
    Services[id] = sd;
    BreakSelect();
    pthread_mutex_unlock(&ServiceMutex);
    return id;
}

void AvahiBase::DeleteService(int id)
{
    pthread_mutex_lock(&ServiceMutex);
    Services.erase(id);
    ServicesChanged=true;
    BreakSelect();
    pthread_mutex_unlock(&ServiceMutex);
}

void AvahiBase::ModifyService(int id, const char* type, int port, std::vector<std::string> txt)
{
    pthread_mutex_lock(&ServiceMutex);
    if(Services.find(id) != Services.end())
    {
        Services[id].Type = type;
        Services[id].Port = port;
        Services[id].Txt = txt;
        ServicesChanged = true;
    }
    pthread_mutex_unlock(&ServiceMutex);
}

int AvahiBase::GetServiceCount()
{
    return Services.size();
}

int AvahiBase::NextFreeServiceId()
{
    int id=0;
    while(Services.find(id) != Services.end())
        ++id;
    return id;
}

void AvahiBase::PrintAllServiceInformation()
{
    for (std::map<int, ServiceDescription>::iterator it = Services.begin(); it != Services.end(); it++) {
        std::cout << "service map elem: " << (*it).first << " : " << (*it).second.Type << " : " << (*it).second.Port << std::endl;
        
        for (std::vector<std::string>::iterator txtIt = (*it).second.Txt.begin(); txtIt != (*it).second.Txt.end(); txtIt++) {
            std::cout << "txt elems: " << (*txtIt) << std::endl;
        }
    }
}

std::vector<std::string> AvahiBase::GetSpecString(int serviceId)
{
    return Services[serviceId].Txt;
}

int AvahiBase::SetSockets(fd_set* readSockets,fd_set* writeSockets)
{
	int highest_sock=0;
	//The Avahi watches
	std::list<AvahiWatch*>::iterator i;
	i=watches.begin();
	while(i!=watches.end())
	{
		if((*i)->dead)
		{
			delete (*i);
			i=watches.erase(i);
			continue;
		}
		if((*i)->event==AVAHI_WATCH_OUT)
			FD_SET((*i)->fd,writeSockets);
		else
			FD_SET((*i)->fd,readSockets);
		
		if((*i)->fd> highest_sock)
			highest_sock=(*i)->fd;
		++i;
	}
	FD_SET(select_break[0],readSockets);
	if(select_break[0]>highest_sock)
		highest_sock=select_break[0];
	return highest_sock;
}

void AvahiBase::AdjustTimeval(timeval* tv)
{
	std::list<AvahiTimeout*>::iterator i;
	i=timeouts.begin();
	timeval current_time;
	gettimeofday(&current_time,0);
	while(i!=timeouts.end())
	{
		if((*i)->dead)
		{
			delete (*i);
			i=timeouts.erase(i);
			continue;
		}
		if(!(*i)->enabled)
		{
			++i;
			continue;
		}
		if(avahi_age(&(*i)->expiry)>=0)
		{
			tv->tv_sec=0;
			tv->tv_usec=0;
			return;
		}
		
		timeval diff;
		timersub(&(*i)->expiry,&current_time,&diff);
		if(avahi_timeval_compare(tv,&diff)<0)
			(*tv)=diff;
	}
}

void AvahiBase::UpdateAndClearSockets(fd_set* readSockets,fd_set* writeSockets)
{
	std::list<AvahiWatch*>::iterator i;
	i=watches.begin();
	while(i!=watches.end())
	{
		if((*i)->dead)
		{
			delete (*i);
			i=watches.erase(i);
			continue;
		}
		if((*i)->event & AVAHI_WATCH_OUT)
			if(FD_ISSET((*i)->fd,writeSockets))
			{
				FD_CLR((*i)->fd,writeSockets);
				(*i)->callback((*i),(*i)->fd,(*i)->event,(*i)->userdata);
			}
		if((*i)->event & AVAHI_WATCH_IN)
			if(FD_ISSET((*i)->fd,readSockets))
			{
				FD_CLR((*i)->fd,readSockets);
				(*i)->callback((*i),(*i)->fd,(*i)->event,(*i)->userdata);
				//Determine the type of event
				//int r_ret;
				//r_ret=read((*i)->fd,0,0);
				//if(r_ret=)
			}
		++i;
	}
	if(FD_ISSET(select_break[0],readSockets))
	{
		FD_CLR(select_break[0],readSockets);
		pthread_mutex_lock(&ServiceMutex);
		UnbreakSelect();
		pthread_mutex_unlock(&ServiceMutex);
	}
	if(state==STATE_OK)
	{
		pthread_mutex_lock(&ServiceMutex);
		if(ServicesChanged)
		{
			ServicesChanged=false;
			CreateServices();
		}
		pthread_mutex_unlock(&ServiceMutex);
	}

}

void AvahiBase::UpdateTimeouts()
{
	std::list<AvahiTimeout*>::iterator i=timeouts.begin();
	while(i!=timeouts.end())
	{
		if((*i)->dead)
		{
			delete (*i);
			i=timeouts.erase(i);
			continue;
		}
		if(!(*i)->enabled)
		{
			++i;
			continue;
		}
		if(avahi_age(&(*i)->expiry)>0)
		{
			(*i)->enabled=false;
			(*i)->callback(*i,(*i)->userdata);
		}
		++i;
	}
}

void AvahiBase::BreakSelect()
{
	write(select_break[1],"\0",1);
}

void AvahiBase::UnbreakSelect()
{
	char buffer[1];
	read(select_break[0],buffer,1);
}

void AvahiIntegrated::PrepareSelect(int &highest_sock,fd_set* readSocket,fd_set* writeSocket,timeval* tv)
{
    if(state==STATE_QUITED)
        return;
    highest_sock=SetSockets(readSocket,writeSocket);
    AdjustTimeval(tv);
}

void AvahiIntegrated::PostSelect(fd_set *readSocket,fd_set* writeSocket)
{
    if(state==STATE_QUITED)
        return;
    UpdateTimeouts();
    UpdateAndClearSockets(readSocket,writeSocket);
}

// A wrapper for the thread
#if 0
void* thread_func(void* c)
#else
void* AvahiThreaded::thread_func(void* c)
#endif
{
    ((AvahiThreaded*)c)->MainThread();
    return 0;
}

bool AvahiThreaded::Initialize(const char* name)
{
	if(!AvahiBase::Initialize(name))
		return false;
	if(0!=pthread_create( &thread, NULL, thread_func, this))
	{
		AvahiBase::Release();
		return false;
	}
	return true;
}

void AvahiThreaded::Release()
{
    state=STATE_QUITED;
    BreakSelect();
    pthread_mutex_unlock(&ServiceMutex);
    if(ThreadRunning)
        pthread_join(thread,NULL);
    AvahiBase::Release();
}

void AvahiThreaded::MainThread()
{
    ThreadRunning=true;
    while(state!=STATE_QUITED)
    {
        timeval timeout;
        timeout.tv_sec=36000;
        timeout.tv_usec=0;
        fd_set readSockets;
        fd_set writeSockets;
        FD_ZERO(&readSockets);
        FD_ZERO(&writeSockets);
        
        int highest_sock=SetSockets(&readSockets,&writeSockets);
        AdjustTimeval(&timeout);
        
        select(highest_sock+1,&readSockets,&writeSockets,NULL,&timeout);
        
        UpdateTimeouts();
        UpdateAndClearSockets(&readSockets,&writeSockets);
    }
    ThreadRunning=false;
}



#else //USE_AVAHI
//Empty interface
	
AvahiBase::AvahiBase(){}
AvahiBase::~AvahiBase(){}

bool AvahiBase::Initialize(const char* /*name*/){ return false; }
int AvahiBase::AddService(const char*, int, int){ return -1;}
void AvahiBase::DeleteService(int /*id*/){}
void AvahiBase::ModifyService(int /*id*/, const char* /*type*/, int /*port*/, std::vector<std::string> /*txt*/){}
void AvahiBase::Release(){}

void AvahiIntegrated::PrepareSelect(int &/*highest_sock*/,fd_set */*readSockets*/,fd_set */*writeSockets*/,timeval */*tv*/){}
void AvahiIntegrated::PostSelect(fd_set* /*readSockets*/,fd_set */*writeSockets*/){}

bool AvahiThreaded::Initialize(const char* /*name*/){return false;}
void AvahiThreaded::Release(){}

#endif //USE_AVAHI


