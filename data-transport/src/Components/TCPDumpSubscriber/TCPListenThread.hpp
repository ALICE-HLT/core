#ifndef _TCPLISTENTHREAD_HPP_
#define _TCPLISTENTHREAD_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCThread.hpp"
#include "MLUCString.hpp"
#include "MLUCDynamicAllocCache.hpp"
#include "MLUCMutex.hpp"
#include "AliHLTTypes.h"
#include <pthread.h>
#include <sys/uio.h>
#include <vector>
#include "AliHLTSubEventDescriptor.hpp"
//#include "Avahi.hpp"
#include <map>
#include <string>

class TCPListenThread: public MLUCThread
    {
    public:

	struct TConnectionData
	    {
		TConnectionData( int sock, bool bin ):
		    fSocket( sock ),
		    fBinary( bin )
			{}
// 		TConnectionData()
// 			{
// 			fSocket = -1;
// 			fBinary = false;
// 			}
		int fSocket;
		bool fBinary;
	    };

	TCPListenThread( unsigned short portNr, bool allowBlockingMode );
	virtual ~TCPListenThread();

	void Quit();
	bool Quitted();

	void SetTimeout( unsigned long timeout_us )
		{
		fTimeout = timeout_us;
		}

// 	void GetNonblockingConnectionSockets( std::vector<TConnectionData>& sockets );
// 	void GetBlockingConnectionSockets( std::vector<TConnectionData>& sockets );

	void CloseConnection( int sock );

	virtual TState Start();
	
	void Write( unsigned blockcnt, iovec* blocks, AliUInt64_t eventID=~(AliUInt64_t)0, bool reply=false, bool replayEvent=false, bool storeAdvance=true );

	void GetReplayIDRequests( std::vector<AliUInt64_t>& replayIDs );
	void GetFirstOrbitEventMonitorRequests( std::vector<AliUInt64_t>& orbits );

	void ClearReplayIDRequests();
	void ClearFirstOrbitEventMonitorRequests();

	void SetSentReplayIDRequests( std::vector<AliUInt64_t> const & replayIDs );
	void SetSentFirstOrbitEventMonitorRequests( std::vector<AliUInt64_t> const & orbits );


    protected:

	enum TSendMode { kNoData = 0, kNextEvent = 1, kMultipleEvents = 2, kAllEvents = 3 };
	enum TDataMode { kBinary = 0, kASCII = 1 };

	struct TPublicWriteData
	    {
		unsigned fBlockCnt;
		iovec* fBlocks;
		unsigned long fConnections;
		unsigned long fConnectionsDone;
	    };
	
	struct TPrivateWriteData
	    {
		bool fBinary;
		TPublicWriteData* fPublic;
		unsigned long fWritten;
	    };
	
	struct TReadData
	    {
		MLUCString fCmd;
 		unsigned fBufferSize;
		unsigned fBufferStart;
		AliUInt8_t fBuffer[64];
	    };

	struct TConnection
	    {
		int fSocket;
		TSendMode fSendMode;
		TDataMode fDataMode;
		TReadData fReadData;
		std::vector<TPrivateWriteData> fWriteData;
		std::vector<AliUInt64_t> fReplayIDRequests;
		std::vector<AliUInt64_t> fFirstOrbitEventRequests;
	    };

	virtual void Run();

	int OpenSocket();
	int CloseSocket();

	int Listen( int& newConnection, std::vector<TConnection>& readSockets, std::vector<TConnection>& writeSockets );

	int ReadCommand( TConnection& connData, MLUCString& cmd, bool first, bool& moreData );
	bool ProcessCommand( TConnection& connData );

	void AddConnection( int sock );
// 	bool GetConnection( int sock, TConnection& connData );
	bool UpdateConnection( const TConnection& connData );

	//void GetAllConnectionSockets( std::vector<int>& sockets );
	void GetAllConnectionSockets( std::vector<TConnection>& sockets );

	int WriteToConnection( TConnection& connData, const TPublicWriteData& pwd, bool blocking );
	int WriteToConnection( int connSock, unsigned blockcnt, iovec* blocks, bool blocking, unsigned long& written );

	bool PushData( TConnection& connData, TPublicWriteData*& pwd, unsigned blockcnt, iovec* blocks );

	iovec* CloneBlocks( unsigned blockCnt, const iovec* blocks );

	void ReleaseBlocks( unsigned blockCnt, iovec* blocks );

	void Release( TPublicWriteData* pwd );

	unsigned short fListenPort;

	int fListenSocket;

	unsigned long fTimeout; // In microsecs.

	std::vector<TConnection> fSockets;
	pthread_mutex_t fSocketMutex;
	pthread_mutex_t fWriteDataMutex;

	bool fQuit;
	bool fQuitted;

	bool fBlockingModeAllowed;

	MLUCDynamicAllocCache fAllocCache;

	std::vector<AliUInt64_t> fReplayIDRequests;
	std::vector<AliUInt64_t> fReplayIDRequestsSent;
	pthread_mutex_t fReplayIDRequestMutex;

	std::vector<AliUInt64_t> fFirstOrbitEventRequests;
	std::vector<AliUInt64_t> fFirstOrbitEventRequestsSent;
	pthread_mutex_t fFirstOrbitEventRequestMutex;

	TPublicWriteData* fAdvanceSupplyEvent; // Protected by fSocketMutex

	MLUCMutex fAdvanceSupplyEventMutex;


    private:
      //        AvahiIntegrated Avahi;
//         AvahiThreaded Avahi;
        
        bool fServiceTypeSet, serviceChanged, updateSpecMap;
        
        map < vector <string>, AliUInt32_t > detSpecMap;
        map < vector <string>, AliUInt32_t >::iterator detSpecMapIter;
        
        vector <string> txtList;
        vector <string>::iterator txtListIter;
        
        string dataOriginStr, dataTypeStr;
        
        AliUInt16_t sector;
        AliUInt8_t maxsector, minsector, maxpatch, minpatch, tmpMaxpatch, tmpMinpatch;
        
        char strBuff[28];
    public:
        void SetServiceType(AliHLTSubEventDescriptor::BlockData* blocks, AliUInt32_t blockCnt);
        bool ServiceChanged();
        int SpecMapSize();
        void PrintSpecMap();
        AliUInt32_t GetSpecification(vector<string> specKey);
    };


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _TCPLISTENTHREAD_HPP_
