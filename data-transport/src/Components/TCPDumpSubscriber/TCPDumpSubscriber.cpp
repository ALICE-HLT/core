/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTProcessingSubscriber.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTAlignment.h"
#include "HOMERData.h"
#include "TCPListenThread.hpp"
#include "AliHLTDataTypes.h"
#include "AliHLTEventDataType.h"
#include "AliHLTHLTEventTriggerData.hpp"
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/uio.h>
#include <limits.h>
#include <netinet/in.h>
#include <vector>

class TCPDumpSubscriber: public AliHLTProcessingSubscriber
    {
    public:
	TCPDumpSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize,
			   AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, int eventCountAlloc = -1 );
	virtual ~TCPDumpSubscriber();

	void SetTCPThread( TCPListenThread* writer )
		{
		fWriter = writer;
		}

	void SetEventIDsPerTimeInterval( unsigned long timeInterval, unsigned long eventIDChangePerTimeInterval )
		{
		fTimeInterval_us = timeInterval;
		fEventIDChangePerTimeInterval = eventIDChangePerTimeInterval;
		}

	void OnlyMonitorEvents()
		{
		fOnlyMonitorEvents = true;
		}

	void SetMonitorEventRequestTimeInterval( unsigned long ti )
		{
		fMonitorEventRequestTimeInterval = ti;
		}

	void OnlyAdvanceMonitorEvents()
		{
		fOnlyAdvanceMonitorEvents = true;
		}
	
    protected:
	
	virtual bool ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, 
				   const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* etsp,
				   AliUInt8_t* outputPtr, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, AliHLTEventDoneData*& edd );

	TCPListenThread* fWriter;

	unsigned long fTimeInterval_us;
	unsigned long fEventIDChangePerTimeInterval;

	bool fOnlyMonitorEvents;

	unsigned long fMonitorEventRequestTimeInterval;
	
	struct timeval fLastMonitorEventRequest;

	bool fOnlyAdvanceMonitorEvents;

	MLUCDynamicAllocCache fAllocCache;

        char*  fECSParamBlock;
        unsigned int fECSParamBlockSize;

    private:
      void FillTriggerData(AliEventID_t& eventID, AliHLTEventTriggerData& etd, const AliHLTEventTriggerStruct* ets);

      uint64 fTriggerDataType;
      uint64 fTriggerDataOrigin;
      uint64 fTriggerDataSpec;

      uint64 fECSParamDataType;
      uint64 fECSParamOrigin;
    };

TCPDumpSubscriber::TCPDumpSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize,
				      AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, int eventCountAlloc ):
    AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc ),
    fTimeInterval_us(1000),
    fEventIDChangePerTimeInterval(1),
    fOnlyMonitorEvents(false),
    fMonitorEventRequestTimeInterval(0),
    fOnlyAdvanceMonitorEvents(false),
    fAllocCache( 1, true, false ),
    fECSParamBlock(NULL),
    fECSParamBlockSize(0),
    fTriggerDataType(METADATA_DATAID),
    fTriggerDataOrigin(PRIV_DATAORIGIN),
    fTriggerDataSpec(0x0),
    fECSParamDataType(ECSPARAM_DATAID),
    fECSParamOrigin(PRIV_DATAORIGIN)
    {
      fWriter = NULL;
      RequestTickEvents();
      fLastMonitorEventRequest.tv_sec = fLastMonitorEventRequest.tv_usec = 0;
    }

TCPDumpSubscriber::~TCPDumpSubscriber()
    {
      if (fECSParamBlock)
	delete[] fECSParamBlock;
    }

bool TCPDumpSubscriber::ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, 
					    AliHLTSubEventDescriptor::BlockData* blocks, 
					    const AliHLTSubEventDataDescriptor* sedd, 
					    const AliHLTEventTriggerStruct* ets,
					    AliUInt8_t*, AliUInt32_t& outSize, 
					    vector<AliHLTSubEventDescriptor::BlockData>&, 
					    AliHLTEventDoneData*& edd )
    {
    outSize = 0;
    if ( !fWriter )
	{
	LOG( AliHLTLog::kError, "TCPDumpSubscriber::ProcessEvent", "No TCP writer object" )
	    << "No TCP writer object specified." << ENDLOG;
	return true;
	}
    
    // Update avahi service information
    if ( eventID.fType == kAliEventTypeData )
    {
        fWriter->SetServiceType(blocks, blockCnt);
    }

    if ( eventID.fType == kAliEventTypeStartOfRun ) {
      for ( unsigned int b = 0; b < blockCnt; b++ ) {
	if ( blocks[b].fDataType.fID == fECSParamDataType ) {
	  fECSParamBlockSize=blocks[b].fSize;
	  fECSParamBlock = new char[fECSParamBlockSize+1];
	  memcpy(fECSParamBlock, blocks[b].fData, fECSParamBlockSize);
	  fECSParamBlock[fECSParamBlockSize]='\0';
	  LOG( AliHLTLog::kInformational, "TCPDumpSubscriber::ProcessEvent", "ECSParamBlock" )
	    << "ECS Parameter block found: \"" << fECSParamBlock << "\"" << ENDLOG;
	  break;
	}
      }
    }

    if ( eventID.fType == kAliEventTypeData && (!fOnlyMonitorEvents || (sedd->fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_MONITOR) ) )
	{
	unsigned long n;
	unsigned long extraBlocks = 2; //Header + TriggerData
	if( fECSParamBlock )
	  extraBlocks += 1;

	struct iovec ioBlocks[ blockCnt+extraBlocks ];
	unsigned long offset = 0;
    
	// Copy sub-event descriptor
	
	struct timeval now;
	gettimeofday( &now, NULL );
	AliUInt8_t* ribd = fAllocCache.Get( HOMERBlockDescriptor::GetHOMERBlockDescriptorSize()*(blockCnt+extraBlocks) );
	if ( !ribd )
	    {
	    LOG( AliHLTLog::kError, "TCPDumpSubscriber::ProcessEvent", "Out of memory" )
		<< "Out of memory allocating ribd block of " << AliHLTLog::kDec << HOMERBlockDescriptor::GetHOMERBlockDescriptorSize()*(blockCnt+extraBlocks)
		<< " bytes size for event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		<< ")." << ENDLOG;
	    return true;
	    }
	    
	memset( ribd, 0, HOMERBlockDescriptor::GetHOMERBlockDescriptorSize()*(blockCnt+extraBlocks) );
	ioBlocks[0].iov_base = ribd;
	ioBlocks[0].iov_len = HOMERBlockDescriptor::GetHOMERBlockDescriptorSize()*(blockCnt+extraBlocks);
	HOMERBlockDescriptor homerBlock;
	homerBlock.UseHeader( reinterpret_cast<void*>( ribd ) );
	homerBlock.Initialize();
	homerBlock.SetUInt64Alignment( AliHLTDetermineUInt64Alignment() );
	homerBlock.SetUInt32Alignment( AliHLTDetermineUInt32Alignment() );
	homerBlock.SetUInt16Alignment( AliHLTDetermineUInt16Alignment() );
	homerBlock.SetUInt8Alignment( AliHLTDetermineUInt8Alignment() );
	homerBlock.SetDoubleAlignment( AliHLTDetermineDoubleAlignment() );
	homerBlock.SetFloatAlignment( AliHLTDetermineFloatAlignment() );
	homerBlock.SetType( (uint64)eventID.fType );
	homerBlock.SetSubType1( eventID.fNr );
	homerBlock.SetSubType2( blockCnt+extraBlocks-1 );
	homerBlock.SetBirth_s( now.tv_sec );
	homerBlock.SetBirth_us( now.tv_usec );
	homerBlock.SetProducerNode( (uint64)fNodeID );
	homerBlock.SetBlockOffset( offset+homerBlock.GetHeaderLength() );
	homerBlock.SetBlockSize( HOMERBlockDescriptor::GetHOMERBlockDescriptorSize()*(blockCnt+extraBlocks-1) );
	offset += ioBlocks[0].iov_len;
	
//     fWriter->SetServiceType(blocks->fDataType, blocks->fDataOrigin, blocks->fDataSpecification);
	
	for ( n = 0; n < blockCnt; n++ )
	    {
	    homerBlock.UseHeader( ribd + HOMERBlockDescriptor::GetHOMERBlockDescriptorSize()*(n+1) );
	    homerBlock.Initialize();
	    homerBlock.SetByteOrder( blocks[n].fAttributes[kAliHLTSEDBDByteOrderAttributeIndex] );
	    homerBlock.SetUInt64Alignment( blocks[n].fAttributes[kAliHLTSEDBD64BitAlignmentAttributeIndex] );
	    homerBlock.SetUInt32Alignment( blocks[n].fAttributes[kAliHLTSEDBD32BitAlignmentAttributeIndex] );
	    homerBlock.SetUInt16Alignment( blocks[n].fAttributes[kAliHLTSEDBD16BitAlignmentAttributeIndex] );
	    homerBlock.SetUInt8Alignment( blocks[n].fAttributes[kAliHLTSEDBD8BitAlignmentAttributeIndex] );
	    homerBlock.SetDoubleAlignment( blocks[n].fAttributes[kAliHLTSEDBDDoubleAlignmentAttributeIndex] );
	    homerBlock.SetFloatAlignment( blocks[n].fAttributes[kAliHLTSEDBDFloatAlignmentAttributeIndex] );
	    homerBlock.SetType( blocks[n].fDataType.fID );
	    homerBlock.SetSubType1( (uint64)blocks[n].fDataOrigin.fID );
	    homerBlock.SetSubType2( (uint64)blocks[n].fDataSpecification );
	    homerBlock.SetBirth_s( sedd->fEventBirth_s );
	    homerBlock.SetBirth_us( sedd->fEventBirth_us );
	    homerBlock.SetProducerNode( (uint64)blocks[n].fProducerNode );
	    homerBlock.SetBlockOffset( offset );
	    homerBlock.SetBlockSize( blocks[n].fSize );
	    ioBlocks[n+1].iov_base = blocks[n].fData;
	    ioBlocks[n+1].iov_len = blocks[n].fSize;
	    offset += ioBlocks[n+1].iov_len;
	    }

	AliHLTEventTriggerData etd;
	FillTriggerData(eventID, etd, ets);

	homerBlock.UseHeader( ribd + HOMERBlockDescriptor::GetHOMERBlockDescriptorSize()*(n+1) );
	homerBlock.Initialize();
	homerBlock.SetByteOrder( etd.fAttributes[kAliHLTBlockDByteOrderAttributeIndex] );
	homerBlock.SetUInt64Alignment( etd.fAttributes[kAliHLTBlockD64BitAlignmentAttributeIndex] );
	homerBlock.SetUInt32Alignment( etd.fAttributes[kAliHLTBlockD32BitAlignmentAttributeIndex] );
	homerBlock.SetUInt16Alignment( etd.fAttributes[kAliHLTBlockD16BitAlignmentAttributeIndex] );
	homerBlock.SetUInt8Alignment( etd.fAttributes[kAliHLTBlockD8BitAlignmentAttributeIndex] );
	homerBlock.SetDoubleAlignment( etd.fAttributes[kAliHLTBlockDDoubleAlignmentAttributeIndex] );
	homerBlock.SetFloatAlignment( etd.fAttributes[kAliHLTBlockDFloatAlignmentAttributeIndex] );
	homerBlock.SetType( fTriggerDataType );
	homerBlock.SetSubType1( fTriggerDataOrigin );
	homerBlock.SetSubType2( fTriggerDataSpec );
	homerBlock.SetBirth_s( now.tv_sec );
	homerBlock.SetBirth_us( now.tv_usec );
	homerBlock.SetProducerNode( (uint64)fNodeID );
	homerBlock.SetBlockOffset( offset );
	homerBlock.SetBlockSize( sizeof(etd) );
	ioBlocks[n+1].iov_base = &etd;
	ioBlocks[n+1].iov_len = sizeof(etd);
	offset += ioBlocks[n+1].iov_len;
	n++;
	if( fECSParamBlock ) {
	homerBlock.UseHeader( ribd + HOMERBlockDescriptor::GetHOMERBlockDescriptorSize()*(n+1) );
	homerBlock.Initialize();
	homerBlock.SetUInt64Alignment( AliHLTDetermineUInt64Alignment() );
	homerBlock.SetUInt32Alignment( AliHLTDetermineUInt32Alignment() );
	homerBlock.SetUInt16Alignment( AliHLTDetermineUInt16Alignment() );
	homerBlock.SetUInt8Alignment( AliHLTDetermineUInt8Alignment() );
	homerBlock.SetDoubleAlignment( AliHLTDetermineDoubleAlignment() );
	homerBlock.SetFloatAlignment( AliHLTDetermineFloatAlignment() );
	homerBlock.SetType( fECSParamDataType );
	homerBlock.SetSubType1( fECSParamOrigin );
	homerBlock.SetSubType2( 0x0 );
	homerBlock.SetBirth_s( now.tv_sec );
	homerBlock.SetBirth_us( now.tv_usec );
	homerBlock.SetProducerNode( (uint64)fNodeID );
	homerBlock.SetBlockOffset( offset );
	homerBlock.SetBlockSize( fECSParamBlockSize );
	ioBlocks[n+1].iov_base = fECSParamBlock;
	ioBlocks[n+1].iov_len = fECSParamBlockSize;
	offset += ioBlocks[n+1].iov_len;
	n++;
	}
	fWriter->Write( blockCnt+extraBlocks, ioBlocks, eventID.fNr, 
			sedd->fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_FULLPASSTHROUGH, 
			eventID.fType==kAliEventTypeDataReplay,
			!fOnlyAdvanceMonitorEvents || (sedd->fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_MONITOR) );
	fAllocCache.Release( ribd );
	ribd = NULL;
	}
    
    std::vector<AliUInt64_t> replayIDs;
    std::vector<AliUInt64_t> orbits;
    fWriter->GetReplayIDRequests( replayIDs );
    fWriter->GetFirstOrbitEventMonitorRequests( orbits );
    struct timeval now;
    gettimeofday( &now, NULL );
    unsigned long long tdiff = (now.tv_sec-fLastMonitorEventRequest.tv_sec)*1000000 + (now.tv_usec-fLastMonitorEventRequest.tv_usec);

    const bool requestMonitorEvent = ( fMonitorEventRequestTimeInterval && tdiff>=fMonitorEventRequestTimeInterval );

    edd = GetEventDoneData( eventID, (replayIDs.size()+orbits.size()+ ( requestMonitorEvent ? 1 : 0 ) )*3 );
    if ( edd )
	{
	std::vector<AliUInt64_t> replayIDsSent;
	std::vector<AliUInt64_t> orbitsSent;

	fWriter->ClearReplayIDRequests();
	fWriter->ClearFirstOrbitEventMonitorRequests();

	std::vector<AliUInt64_t>::iterator iter, end;
	unsigned long ii=0;
	iter = replayIDs.begin();
	end = replayIDs.end();
	while ( iter != end )
	    {
	    edd->fDataWords[ii] = kAliHLTEventDoneMonitorReplayEventIDCmd;
	    edd->fDataWords[ii+1] = (AliUInt32_t)( *iter & 0xFFFFFFFF );
	    edd->fDataWords[ii+2] = (AliUInt32_t)( (*iter & 0xFFFFFFFF00000000ULL) >> 32 );
	    ii += 3;
	    replayIDsSent.push_back( *iter );
	    LOG( AliHLTLog::kDebug, "TCPDumpSubscriber::ProcessEvent", "Replay event" )
		<< "Added 0x" << AliHLTLog::kHex << *iter << " (" << AliHLTLog::kDec
		<< *iter << ") to replay event IDs." << ENDLOG;
	    iter++;
	    }
	iter = orbits.begin();
	end = orbits.end();
	while ( iter != end )
	    {
	    //AliUInt64_t reqOrbit = ( (eventID.fNr + (*iter)) & 0xFFFFFFFFFFFFF000ULL );
	    
	    AliUInt64_t reqOrbit = (eventID.fNr + ( ((*iter)/fTimeInterval_us)*fEventIDChangePerTimeInterval ));
	    if ( fAliceHLT )
		{
		reqOrbit += 0x1000;
		reqOrbit &= 0xFFFFFFFFFFFFF000ULL;
		}
	    if ( reqOrbit>0 )
		{
		edd->fDataWords[ii] = kAliHLTEventDoneMonitorFirstOrbitEventCmd;
		edd->fDataWords[ii+1] = (AliUInt32_t)( reqOrbit & 0xFFFFFFFF );
		edd->fDataWords[ii+2] = (AliUInt32_t)( (reqOrbit & 0xFFFFFFFF00000000ULL) >> 32 );
		ii += 3;
		orbitsSent.push_back( reqOrbit );
		LOG( AliHLTLog::kDebug, "TCPDumpSubscriber::ProcessEvent", "Orbit first event requests" )
		    << "Added 0x" << AliHLTLog::kHex << reqOrbit << " (" << AliHLTLog::kDec
		    << reqOrbit << ") to requests for first orbit events." << ENDLOG;
		}
	    iter++;
	    }
	if ( requestMonitorEvent )
	    {
	    AliUInt64_t reqOrbit = (eventID.fNr + ( (1000000/fTimeInterval_us)*fEventIDChangePerTimeInterval ));
	    if ( fAliceHLT )
		{
		reqOrbit += 0x1000;
		reqOrbit &= 0xFFFFFFFFFFFFF000ULL;
		}
	    if ( reqOrbit>0 )
		{
		edd->fDataWords[ii] = kAliHLTEventDoneMonitorFirstOrbitEventCmd;
		edd->fDataWords[ii+1] = (AliUInt32_t)( reqOrbit & 0xFFFFFFFF );
		edd->fDataWords[ii+2] = (AliUInt32_t)( (reqOrbit & 0xFFFFFFFF00000000ULL) >> 32 );
		ii += 3;
		orbitsSent.push_back( reqOrbit );
		LOG( AliHLTLog::kDebug, "TCPDumpSubscriber::ProcessEvent", "Orbit first event requests" )
		    << "Added 0x" << AliHLTLog::kHex << reqOrbit << " (" << AliHLTLog::kDec
		    << reqOrbit << ") to requests for first orbit events." << ENDLOG;
		}
	    }

	fWriter->SetSentReplayIDRequests( replayIDsSent );
	fWriter->SetSentFirstOrbitEventMonitorRequests( orbitsSent );
	}

    

    return true;
    }

void TCPDumpSubscriber::FillTriggerData(AliEventID_t& eventID, AliHLTEventTriggerData& etd, const AliHLTEventTriggerStruct* ets) {
  bool reset=false;
  const char* resetReason="";
  if ( ets->fHeader.fLength != sizeof(AliHLTHLTEventTriggerData) )
    {
      LOG( AliHLTLog::kWarning, "TCPDumpSubscriberComponent::FillTriggerData", "Trigger Data" )
	<< "Trigger data size mismatch for event 0x"
	<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec 
	<< eventID << "). Present : " << AliHLTLog::kDec
	<< ets->fHeader.fLength << " - Expected: "
	<< sizeof(AliHLTHLTEventTriggerData) << ". - Assuming empty trigger data..." << ENDLOG;
      reset = true;
    }
  else
    {
      if ( gkAliHLTBlockDAttributeCount!=kAliHLTBlockDAttributeCount )
	{
	  reset = true;
	  resetReason = " (mismatch in Block Attribute Count).";
	}
      else
	memcpy( etd.fAttributes, ((AliHLTHLTEventTriggerData*)ets)->fAttributes, sizeof(etd.fAttributes) );
      etd.fHLTStatus = ((AliHLTHLTEventTriggerData*)ets)->fHLTStatus;
      etd.fCommonHeaderWordCnt = ((AliHLTHLTEventTriggerData*)ets)->fCommonHeaderWordCnt;
      if ( (unsigned)gkAliHLTCommonHeaderCount != ((AliHLTHLTEventTriggerData*)ets)->fCommonHeaderWordCnt )
	{
	  reset = true;
	  resetReason = " (Mismatch in trigger header word count).";
	}
      else{
	memcpy( etd.fCommonHeader, ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader, sizeof(etd.fCommonHeader) );
      }

      etd.fReadoutList.fCount = ((AliHLTHLTEventTriggerData*)ets)->fReadoutList[0];
      if ( (AliUInt32_t)gkAliHLTDDLListSize != ((AliHLTHLTEventTriggerData*)ets)->fReadoutList[0] )
	{
	  unsigned interfaceReadoutListVersion = AliHLTReadoutList::GetReadoutlistVersionFromWordCount( gkAliHLTDDLListSize+1 );
	  if ( !AliHLTReadoutList::IsVersionSupported( interfaceReadoutListVersion ) )
	    {
	      LOG( AliHLTLog::kWarning, "TCPDumpSubscriberComponent::FillTriggerData", "Mismatch in readout list word count" )
		<< "Mismatch in readout list word count - AliRoot interface version readout list size" << AliHLTLog::kDec
		<< gkAliHLTDDLListSize << " (" << gkAliHLTDDLListSize+1 << ") - interface readout list version determined "
		<< interfaceReadoutListVersion << "." << ENDLOG;
	      reset = true;
	      // resetReason = " (Mismatch in readout list word count (incompatible version found)).";
		    
	    }
	  else
	    {
	      AliHLTReadoutListData interfaceReadoutListData;
	      AliHLTReadoutList interfaceReadoutList( interfaceReadoutListVersion, interfaceReadoutListData );
	      interfaceReadoutList.Reset();
	      if ( !interfaceReadoutList.Import( ((AliHLTHLTEventTriggerData*)ets)->fReadoutList, gReadoutListVersion ) )
		{
		  reset = true;
		  resetReason = " (Error importing mismatching readout list version).";
		}
	      else
		memcpy( etd.fReadoutList.fList, &(interfaceReadoutListData[1]), interfaceReadoutListData[0] );
	    }
	}
      else
	memcpy( etd.fReadoutList.fList, ((AliHLTHLTEventTriggerData*)ets)->fReadoutList+1, ((AliHLTHLTEventTriggerData*)ets)->fReadoutList[0] );
    }

if ( reset )
  {
    if ( eventID.fType==kAliEventTypeStartOfRun || eventID.fType==kAliEventTypeData || eventID.fType==kAliEventTypeEndOfRun || eventID.fType==kAliEventTypeSync )
      {
	LOG( AliHLTLog::kWarning, "TCPDumpSubscriberComponent::FillTriggerData", "Trigger Data" )
	  << "Trigger data format error for event 0x"
	  << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec 
	  << eventID << ")." << resetReason << " Resetting trigger data to empty state..." << ENDLOG;
      }
    memset( etd.fAttributes, 0, gkAliHLTBlockDAttributeCount*sizeof(etd.fAttributes[0]) );
	
    etd.fHLTStatus = 0;
    etd.fCommonHeaderWordCnt = gkAliHLTCommonHeaderCount;
    memset( etd.fCommonHeader, 0, gkAliHLTCommonHeaderCount*sizeof(etd.fCommonHeader[0]) );
    etd.fReadoutList.fCount = gkAliHLTDDLListSize;
    memset( etd.fReadoutList.fList, 0, gkAliHLTDDLListSize*sizeof(etd.fReadoutList.fList[0]) );
  }
}

class TCPDumpSubscriberComponent: public AliHLTControlledDefaultSinkComponent<TCPDumpSubscriber>
    {
    public:
  
	TCPDumpSubscriberComponent( const char* compName, int argc, char** argv );
	virtual ~TCPDumpSubscriberComponent();
  
    protected:

	virtual bool CheckConfiguration();
	virtual void ShowConfiguration();
  
	virtual bool CreateParts();
	virtual void DestroyParts();
  
	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
  
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );
  
	virtual bool SetupComponents();

	TCPListenThread* fTCPListenThread;
	unsigned short fListenPort;

	bool fAllowBlockingMode;

	unsigned long fTimeInterval_us;
	unsigned long fEventIDChangePerTimeInterval;

	bool fOnlyMonitorEvents;

unsigned long fMonitorEventRequestTimeInterval;

	bool fOnlyAdvanceMonitorEvents;

    private:
    };


TCPDumpSubscriberComponent::TCPDumpSubscriberComponent( const char* compName, int argc, char** argv ):
    AliHLTControlledDefaultSinkComponent<TCPDumpSubscriber>( compName, argc, argv ),
    fTimeInterval_us(1000),
    fEventIDChangePerTimeInterval(1),
    fOnlyMonitorEvents(false),
    fMonitorEventRequestTimeInterval(0),
    fOnlyAdvanceMonitorEvents(false)
    {
    fTCPListenThread = NULL;
    fListenPort = 0;
    fAllowBlockingMode = false;
    }

TCPDumpSubscriberComponent::~TCPDumpSubscriberComponent()
    {
    }
  
bool TCPDumpSubscriberComponent::CheckConfiguration()
    {
    if ( !AliHLTControlledSinkComponent::CheckConfiguration() )
	return false;
    if ( !fListenPort )
	{
	LOG( AliHLTLog::kError, "TCPDumpSubscriberComponent::CheckConfiguration", "No TCP listen port specified" )
	    << "Must specify a TCP listen port to be used > 0 (e.g. using the -port command line option."
	    << ENDLOG;
	return false;
	}
    return true;
    }

void TCPDumpSubscriberComponent::ShowConfiguration()
    {
    AliHLTControlledComponent::ShowConfiguration();
    LOG( AliHLTLog::kInformational, "TCPDumpSubscriberComponent::ShowConfiguration", "TCP Listen Port" )
	<< "TCP Listen Port: " << AliHLTLog::kDec << (unsigned)fListenPort << " (0x"
	<< AliHLTLog::kHex << (unsigned)fListenPort << ")." << ENDLOG;
    LOG( AliHLTLog::kInformational, "TCPDumpSubscriberComponent::ShowConfiguration", "Blocking Mode" )
	<< "Blocking mode is  "
	<< (fAllowBlockingMode ? "" : "not ") << "allowed." << ENDLOG;
    LOG( AliHLTLog::kInformational, "TCPDumpSubscriberComponent::ShowConfiguration", "Event IDs Per Time Interval" )
	<< AliHLTLog::kDec << fEventIDChangePerTimeInterval << " event ID change per "
	<< fEventIDChangePerTimeInterval << " microsecs." << ENDLOG;
    if ( fOnlyMonitorEvents )
	{
	LOG( AliHLTLog::kInformational, "TCPDumpSubscriberComponent::ShowConfiguration", "Only Full Passthrough Events" )
	    << "Only full passthrough events will be used." << ENDLOG;
	}
    if ( fOnlyAdvanceMonitorEvents )
	{
	LOG( AliHLTLog::kInformational, "TCPDumpSubscriberComponent::ShowConfiguration", "Only monitor events in advance store Events" )
	    << "Only store monitor flagged events in advance." << ENDLOG;
	}
    LOG( AliHLTLog::kInformational, "TCPDumpSubscriberComponent::ShowConfiguration", "Monitor Event Request Interval" )
	<< "Monitor event request interval: " << MLUCLog::kDec << fMonitorEventRequestTimeInterval << " microsecs." << ENDLOG;
    }

bool TCPDumpSubscriberComponent::CreateParts()
    {
    if ( !AliHLTControlledComponent::CreateParts() )
	return false;
    fTCPListenThread = new TCPListenThread( fListenPort, fAllowBlockingMode );
    if ( !fTCPListenThread )
	{
	LOG( AliHLTLog::kError, "TCPDumpSubscriberComponent::CreateParts", "Unable to create listen thread" )
	    << "Unable to create TCP listen thread." << ENDLOG;
	return false;
	}
    return true;
    }

void TCPDumpSubscriberComponent::DestroyParts()
    {
    if ( fTCPListenThread )
	{
	fTCPListenThread->Quit();
	struct timeval start, now;
	unsigned long long deltaT = 0;
	const unsigned long long timeLimit = 1000000;
	gettimeofday( &start, NULL );
	while ( deltaT<timeLimit && !fTCPListenThread->Quitted() )
	    {
	    usleep( 0 );
	    gettimeofday( &now, NULL );
	    deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	    }
	if ( !fTCPListenThread->Quitted() )
	    fTCPListenThread->Abort();
	else
	    fTCPListenThread->Join();
	delete fTCPListenThread;
	fTCPListenThread = NULL;
	}
    }

const char* TCPDumpSubscriberComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cpErr = NULL;
    if ( !strcmp( argv[i], "-port" ) )
	{
	if ( argc <= i+1 )
	    return "Missing TCP listen port number specification parameter";
	unsigned long tmpPort;
	tmpPort = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting TCP listen port number specifier.";
	    }
	if ( tmpPort > USHRT_MAX )
	    {
	    errorArg = i+1;
	    return "TCP listen port number too large.";
	    }
	fListenPort = tmpPort;
	i += 2;
	return NULL;
	}
    else if ( !strcmp( argv[i], "-allowblocking" ) )
	{
	fAllowBlockingMode = true;
	i++;
	return NULL;
	}
    else if ( !strcmp( argv[i], "-eventidspertime" ) )
	{
	if ( argc <= i+2 )
	    return "Missing event ID change and time interval specification parameters";
	fTimeInterval_us = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting time interval specification parameters";
	    }
	fEventIDChangePerTimeInterval = strtoul( argv[i+2], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+2;
	    return "Error converting event ID change specification parameters";
	    }
	i += 3;
	return NULL;
	}
    else if ( !strcmp( argv[i], "-alicehlt" ) )
	{
	fTimeInterval_us = 88;
	fEventIDChangePerTimeInterval = 0x1000;
	// No return and i++ because this is only intercepted and handled back to the parent class
	}
    else if ( !strcmp( argv[i], "-onlymonitorevents" ) )
	{
	fOnlyMonitorEvents = true;
	i++;
	return NULL;
	}
    else if ( !strcmp( argv[i], "-monitoreventrequestinterval" ) )
	{
	if ( argc <= i+1 )
	    return "Missing monitor event request interval specification parameter";
	fMonitorEventRequestTimeInterval = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting monitor event request interval specifier.";
	    }
	i += 2;
	return NULL;
	}
    else if ( !strcmp( argv[i], "-onlyadvancemonitorevents" ) )
	{
	fOnlyAdvanceMonitorEvents = true;
	i++;
	return NULL;
	}
    return AliHLTControlledDefaultSinkComponent<TCPDumpSubscriber>::ParseCmdLine( argc, argv, i, errorArg );
    }
  
void TCPDumpSubscriberComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    AliHLTControlledDefaultSinkComponent<TCPDumpSubscriber>::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-port <port-number>: Specify the TCP port to listen on for incoming connections. (Mandatory)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-allowblocking: Allow remote clients to select a blocking mode which can slow down a full chain. (Optional, dangerous)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-eventidspertime <time-interval_us> <event-ID-change>: Specify the event-ID-Change per time-interval_us (in microseconds) ratio. (This is used to calculate how much an event ID has to be increased when a full monitoring event is requested.) (Optional, defaults 1000 microsec. and 1 event ID change, i.e. event ID increased by 1 every millisecond)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-onlymonitorevents: Specify that only events with the full passthrough status flag set are forwarded to requesting clients. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-monitoreventrequestinterval <interval-microsec.>: Specify an interval in microseconds in which monitor events are requested, even if no client actually requested one (for advance storage and instantaneous delivery). (Optional, default disabled)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-onlyadvancemonitorevents: Specify that only events that are flagged as monitor events are stored in advance before a request. (Optional)" << ENDLOG;
    }
 

bool TCPDumpSubscriberComponent::SetupComponents()
    {
    if ( !AliHLTControlledComponent::SetupComponents() )
	return false;
    ((TCPDumpSubscriber*)fSubscriber)->SetTCPThread( fTCPListenThread );
    ((TCPDumpSubscriber*)fSubscriber)->SetEventIDsPerTimeInterval( fTimeInterval_us, fEventIDChangePerTimeInterval );
    if ( fOnlyMonitorEvents )
	((TCPDumpSubscriber*)fSubscriber)->OnlyMonitorEvents();
    if ( fOnlyAdvanceMonitorEvents )
	((TCPDumpSubscriber*)fSubscriber)->OnlyAdvanceMonitorEvents();
    ((TCPDumpSubscriber*)fSubscriber)->SetMonitorEventRequestTimeInterval( fMonitorEventRequestTimeInterval );
    fTCPListenThread->Start();

    return true;
    }



int main( int argc, char** argv )
    {
    TCPDumpSubscriberComponent procComp( "TCPDumpSubscriber", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
