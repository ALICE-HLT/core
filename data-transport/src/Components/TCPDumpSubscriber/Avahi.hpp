#ifndef _AVAHI_HPP_
#define _AVAHI_HPP_

#include <string>
#include <list>
#include <vector>
#include <map>

#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#ifdef USE_AVAHI
#include <avahi-client/client.h>
#include <avahi-client/publish.h>

#include <avahi-common/alternative.h>
#include <avahi-common/simple-watch.h>
#include <avahi-common/malloc.h>
#include <avahi-common/error.h>
#include <avahi-common/timeval.h>


struct AvahiWatch
{
//     AvahiComponent* component;
    AvahiWatchCallback callback;
    void *userdata;
    AvahiWatchEvent event;
    int fd;
    bool dead;
};

struct AvahiTimeout
{
//     AvahiComponent* component;
    AvahiTimeoutCallback callback;
    void *userdata;
    struct timeval expiry;
    bool dead;
    bool enabled;
};

struct ServiceDescription
{
    std::string Type;
    int Port;
    std::vector<std::string> Txt;
};

#if 0
static AvahiWatch* watch_new(const AvahiPoll *api, int fd, AvahiWatchEvent event, AvahiWatchCallback callback, void *userdata);
static void watch_update(AvahiWatch* w,AvahiWatchEvent event);
static AvahiWatchEvent watch_get_events(AvahiWatch *w);
static void watch_free(AvahiWatch* w);
static AvahiTimeout* timeout_new(const AvahiPoll* api, const struct timeval *tv, AvahiTimeoutCallback callback, void *userdata);
static void timeout_update(AvahiTimeout*, const struct timeval *tv);
static void timeout_free(AvahiTimeout *t);
static void entry_group_callback(AvahiEntryGroup *g, AvahiEntryGroupState state, void *userdata);
static void client_callback(AvahiClient* c,AvahiClientState state,void* userdata);
void *thread_func( void *ptr );
#endif


#endif //USE_AVAHI
///Base class for Avahi integration.
/**This is the base class for integrating Avahi. It provides initialization and releasing of the component and creation of services.
 * The integration into the main loop is done in AvahiThreaded nad AvahiIntegrated.
 */
class AvahiBase
{
public:
#ifdef USE_AVAHI
#if 0
	friend AvahiWatch* watch_new(const AvahiPoll *api, int fd, AvahiWatchEvent event, AvahiWatchCallback callback, void *userdata);
	friend AvahiTimeout* timeout_new(const AvahiPoll* api, const struct timeval *tv, AvahiTimeoutCallback callback, void *userdata);
	friend void entry_group_callback(AvahiEntryGroup *g, AvahiEntryGroupState state, void *userdata);
	friend void client_callback(AvahiClient* c,AvahiClientState state,void* userdata);
#endif
    static AvahiWatch* watch_new(const AvahiPoll *api, int fd, AvahiWatchEvent event, AvahiWatchCallback callback, void *userdata);
    static void watch_update(AvahiWatch* w,AvahiWatchEvent event);
    static AvahiWatchEvent watch_get_events(AvahiWatch *w);
    static void watch_free(AvahiWatch* w);
    static AvahiTimeout* timeout_new(const AvahiPoll* api, const struct timeval *tv, AvahiTimeoutCallback callback, void *userdata);
    static void timeout_update(AvahiTimeout*, const struct timeval *tv);
    static void timeout_free(AvahiTimeout *t);
    static void entry_group_callback(AvahiEntryGroup *g, AvahiEntryGroupState state, void *userdata);
    static void client_callback(AvahiClient* c,AvahiClientState state,void* userdata);

#endif //USE_AVAHI
	///Constructor.
	AvahiBase();
	///Destructor.
	virtual ~AvahiBase();

	///Initialize Avahi.
	/**This method has to be executed and return true before any services can be added.
	 * @param name The group name under which services are registered.
	 * @return true in case of success, false in case of failure.
	 */
	virtual bool Initialize(const char* name);
#define AVAHI_ID_ANY -1
#define AVAHI_ID_ALREADY_USED -2
	///Register a service.
	/**Initialize has to be executed and return true before this function does anything. Registers a service of given type and port to
	 * by the name given to Initialize in the avahi network. To later modify the service with AvahiBase::ModifyService give it a unique
	 * identification number with the id parameter. You can pass AVAHI_ID_ANY to set it to any unused number. If a service with the id
	 * already exists, AVAHI_ID_ALREADY_USED is returned and no service is created.
	 * @param type The type the service.
	 * @param port The port the service waits on.
	 * @param id A unique identification number for the service. Give AVAHI_ID_ANY if you just want to set is to any unused number.
	 * @return The identification number given to the service. If id!=AVAHI_ID_ANY, this is the same as id. If a service with the id
	 * already exists, AVAHI_ID_ALREADY_USED is returned and no service is created.
	 */
// 	int AddService(const char* type,int port,std::string *txt=NULL,int num_txt=0,int id=AVAHI_ID_ANY);
    int AddService(const char* type, int port, int id=AVAHI_ID_ANY);
	///Delete a previous registered service.
	/**Deletes a service which has been added with AvahiBase::AddService.
	 * @param id The unique identification number given to AvahiBase::AddService or returned by it if AVAHI_ID_ANY has been used.
	 */
	void DeleteService(int id);
	///Modify a service.
	/** Modifies a service added by Avahibase::AddService.
	 * @param id The unique identification number given to AvahiBase::AddService or returned by it if AVAHI_ID_ANY has been used.
	 * @param type The new type of the service.
	 * @param port The new port the service waits on.
	 * @param txt Information appearing in the TXT Data field. This is a vector of strings with an entry for every txt entry to be made
	 */
    void ModifyService(int id, const char* type, int port, std::vector<std::string> txt);
	///Returns the number of registered services.
	/** @return The number of registered services.*/
	int GetServiceCount();
    ///Returns the next free service id
    /** @return Id of next free service id */
    int NextFreeServiceId();
    ///Print all service information
    void PrintAllServiceInformation();
    ///Get the specification string for a given service id
    /** @return Specification string for a given service id */
    std::vector<std::string> GetSpecString(int serviceId);
	///Release avahi.
	/**Unregisters all services and does anything that has do be done before the application quits. Call this method when you do
	 * not need avahi anymore.
	 */
	virtual void Release();
    
#ifdef USE_AVAHI
protected:
	///Sets the sockets avahi wants to poll on.
	/**Adds the sockets avahi needs to poll on to the provided sets. It is expected that select is executed on this sockets afterwards.
	 * Is used in AvahiIntegrated::PrepareSelect and AvahiThreaded::MainThread and should not be used directly.
	 * @param readSockets The fd_set to which the sockets Avahi wants to poll on for reading are added.
	 * @param writeSockets The fd_set to which the sockets Avahi wants to poll on for writing are added.
	 * @return The highest socket Avahi added to the sets.
	 */
	int SetSockets(fd_set* readSockets,fd_set* writeSockets);
	///Adjust the timeval for Avahi.
	/**Adjusts the timeval so that it is small enough for Avahi to react to its timeouts. Avahi expects AvahiBase::UpdateTimeouts to be
	 * called within this timeval. This method is used in AvahiIntegrated::PrepareSelect and AvahiThreaded::MainThread and should not be
	 * used directly.
	 * @param tv Pointer to the timeval that will be adjusted.
	 */
	void AdjustTimeval(timeval* tv);
	///Reacts to Avahis timeouts.
	/**Calls all of Avahis timeouts which have be expired. This method is used in AvahiIntegrated::PrepareSelect and
	 * AvahiThreaded::MainThread and should not be used directly.
	 */
	void UpdateTimeouts();
	///Reacts to the polled sockets and removes them from the sets.
	/**After select has polled on the sockets from AvahiBase::SetSockets, this method reacts to the returned sockets and remove them from
	 * the sets so that the original main loop does not need to worry about sockets being set that it does not know.
	 * This method is used in AvahiIntegrated::PrepareSelect and AvahiThreaded::MainThread and should not be
	 * used directly.
	 * @param readSockets The fd_set that has been given to AvahiBase::SetSockets as first parameter.
	 * @param writeSockets The fd_set that has been given to AvahiBase::SetSockets as second parameter.
	 */
	void UpdateAndClearSockets(fd_set* readSockets,fd_set *writeSockets);

	///Different client states.
	/**Enumerates the states this client can be in.*/
	enum states
	{
		///Quited or not jet started.
		STATE_QUITED,
		///Everything ok, service registered.
		STATE_OK,
		///Started, but waiting for avahi-deamon to start.
		STATE_WAITING 
	};
	///The state this client is currently in.
	states state;

	///Mutex protecting the services to register.
	/** This Mutex makes sure multiple threads are not accessing AvahiBase::Services at the same time.*/
	pthread_mutex_t ServiceMutex;
    
private:
	///See avahi documentation for the purpose of this.
	void ClientCallback(AvahiClient *c,AvahiClientState state);
	///See avahi documentation for the purpose of this.
	void EntryGroupCallback(AvahiEntryGroup *g,AvahiEntryGroupState state);
	///(Re)creates and registers all services.
	/**Registers all services in the AvahiBase::Service list with the avahi-daemon. Should only be called when state==STATE_OK and ServicesChanged==true are fulfilled.*/
	void CreateServices();
	
	std::list<AvahiWatch*> watches;
	std::list<AvahiTimeout*> timeouts;
	AvahiPoll avahi_poll;
	AvahiClient* client;
	AvahiEntryGroup* group;

	///Set to true when AvahiBase::CreateService has been called.
	bool ServicesChanged;
	///Services registered or to be registered with the avahi-deamon.
	std::map<int, ServiceDescription> Services;

	///Group name for all services.
	std::string GroupName;
	///Pipe for breaking select.
	int select_break[2];
    protected:
	///Break the current select.
	/**When AvahiBase::CreateService is called it my be needed to break to current select (so that the new service can be created).
	 * AvahiBase::select_break contains a pipe select always sits on for reading. This function simple writes a character to that pipe
	 * making select to stop blocking.
	 */
	void BreakSelect();
	///Reverse the effect of BreakSelect.
	/**After Break select has been called, UnbreakSelect should be called for further selects to block again.
	 */
	void UnbreakSelect();
#endif //USE_AVAHI
};

///Integrate Avahi into existing main loop.
/**This calls is for integrating avahi into an existing main loop based on select.
 * See the Avahi tutorial for details on how to use this class.
 */

class AvahiIntegrated : public AvahiBase
{
public:
	///Prepare a select.
	/**In the main loop before select this function should be executed to set the sockets Avahi wants to
	 * poll on and reduce the timeout so that Avahis timeouts can be met.
	 * @param highest_sock This should contain the highest file desciptor in readSockets and writeSockets and will be changed reflecting the sockets that will be added.
	 * @param readSockets The fd_set to which the sockets Avahi wants to poll on for reading are added.
	 * @param writeSockets The fd_set to which the sockets Avahi wants to poll on for writing are added.
	 * @param tv Pointer to the timeval that will be adjusted to meet Avahis timeouts.
	 * 
	 */
	void PrepareSelect(int &highest_sock,fd_set *readSockets,fd_set *writeSockets,timeval *tv);
	///React to select.
	/** After a select returns, this method should be executed for Avahi to call its timeouts and work on its sockets.
	 * It also removes them from the sets so that the original main loop does not need to worry about sockets being set
	 * that it does not know.
	 * @param readSockets The fd_set that has been given to AvahiBase::PrepareSelect as second parameter.
	 * @param writeSockets The fd_set that has been given to AvahiBase::PreapareSelect as third parameter.
	 */
	void PostSelect(fd_set *readSockets,fd_set *writeSockets);
};

///Integrate Avahi using separate thread.
/**This class is for integrating Avahi as simple as possible using a separate thread for the main loop.
 * See the Avahi tutorial for details on how to use this class.
 */

class AvahiThreaded : public AvahiBase
{
public:
	friend void* thread_func(void *ptr);
	///Constructor
	AvahiThreaded() : AvahiBase(){
		ThreadRunning=false;
	}
	///Specialized Initialization.
	/**Does the same as AvahiBase::Initialize and starts the main loop thread.
	 */
	virtual bool Initialize(const char* name);
	///Specialized Release.
	/**Does the same as AvahiBase::Release and quits the main loop thread.
	 */
	virtual void Release();
    void GetServices();
private:
	pthread_t thread;
	bool ThreadRunning;
	///The main Thread.
	/**The thread started by AvahiThreaded::Initialize. The main loop is in here.
	 */
	void MainThread();
        static void *thread_func( void *ptr );
};

#endif // _AVAHI_HPP_
