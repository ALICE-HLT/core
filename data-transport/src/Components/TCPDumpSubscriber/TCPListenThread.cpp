/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TCPListenThread.hpp"
#include "AliHLTLog.hpp"
#include <errno.h>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstdlib>

#include <iostream>

TCPListenThread::TCPListenThread( unsigned short portNr, bool allowBlockingMode ):
    fAllocCache( 6, true, false )
    {
    fListenPort = portNr;
    fQuit = false;
    fQuitted = true;
    fTimeout = 0;
    fBlockingModeAllowed = allowBlockingMode;
    pthread_mutex_init( &fSocketMutex, NULL );
    pthread_mutex_init( &fWriteDataMutex, NULL );
    pthread_mutex_init( &fReplayIDRequestMutex, NULL );
    pthread_mutex_init( &fFirstOrbitEventRequestMutex, NULL );
    fAdvanceSupplyEvent = NULL;
    }

TCPListenThread::~TCPListenThread()
    {
    pthread_mutex_destroy( &fSocketMutex );
    pthread_mutex_destroy( &fWriteDataMutex );
    pthread_mutex_destroy( &fReplayIDRequestMutex );
    pthread_mutex_destroy( &fFirstOrbitEventRequestMutex );
    while ( !fSockets.empty() )
	{
	while ( !fSockets.begin()->fWriteData.empty() )
	    {
	    if ( fSockets.begin()->fWriteData.begin()->fPublic )
		Release( fSockets.begin()->fWriteData.begin()->fPublic );
	    fSockets.begin()->fWriteData.erase( fSockets.begin()->fWriteData.begin() );
	    }
	close( fSockets.begin()->fSocket );
	fSockets.erase( fSockets.begin() );
	}
    close( fListenSocket );
    }

void TCPListenThread::Quit()
    {
    fQuit = true;
    }

bool TCPListenThread::Quitted()
    {
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 2000000;
    gettimeofday( &start, NULL );
    fQuit = true;
    while ( deltaT<timeLimit && !fQuitted )
	{
	usleep( 10000 );
	fQuit = true;
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    return fQuitted;
    }

// void TCPListenThread::GetBlockingConnectionSockets( std::vector<TConnectionData>& sockets )
//     {
//     std::vector<TConnection>::iterator iter, end;
//     pthread_mutex_lock( &fSocketMutex );
//     iter = fSockets.begin();
//     end = fSockets.end();
//     while ( iter != end )
// 	{
// 	if ( iter->fSendMode == kAllEvents )
// 	    socket.push_back( TConnectionData( iter->fSocket, iter->fDataMode==kBinary ) );
// 	iter++;
// 	}
//     pthread_mutex_unlock( &fSocketMutex );
//     }

// void TCPListenThread::GetNonblockingConnectionSockets( std::vector<TConnectionData>& sockets )
//     {
//     std::vector<TConnection>::iterator iter, end;
//     pthread_mutex_lock( &fSocketMutex );
//     iter = fSockets.begin();
//     end = fSockets.end();
//     while ( iter != end )
// 	{
// 	if ( iter->fSendMode == kNextEvent )
// 	    socket.push_back( TConnectionData( iter->fSocket, iter->fDataMode==kBinary ) );
// 	iter++;
// 	}
//     pthread_mutex_unlock( &fSocketMutex );
//     }

//void TCPListenThread::GetAllConnectionSockets( std::vector<int>& sockets )
void TCPListenThread::GetAllConnectionSockets( std::vector<TConnection>& sockets )
    {
    pthread_mutex_lock( &fSocketMutex );
    sockets = fSockets;
    pthread_mutex_unlock( &fSocketMutex );
    }

MLUCThread::TState TCPListenThread::Start()
    {
    int ret;
    ret = OpenSocket();
    if ( ret )
	return MLUCThread::kFailed;
    return MLUCThread::Start();
    }

void TCPListenThread::Write( unsigned blockCnt, iovec* blocks, AliUInt64_t eventID, bool reply, bool replayEvent, bool storeAdvance )
    {
    LOG( AliHLTLog::kDebug, "TCPListenThread::Write", "Event received" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << "): " << (reply ? "reply event" : "normal event") << " "
	<< (replayEvent ? "replay event" : "data event") << "." << ENDLOG;
    TPublicWriteData* pwd=NULL;
    std::vector<TConnection> blockingConnections;
    std::vector<TConnection>::iterator iter, end;
    pthread_mutex_lock( &fSocketMutex );
    iter = fSockets.begin();
    end = fSockets.end();
    while ( iter != end )
	{
	bool sendBackground=false;
	if ( ( iter->fSendMode == kNextEvent || iter->fSendMode == kMultipleEvents ) &&
	       iter->fWriteData.empty() )
	    sendBackground = true;
	if ( eventID!=~(AliUInt64_t)0 && reply )
	    {
	    std::vector<AliUInt64_t>::iterator idIter, idEnd;
	    if ( replayEvent && !fReplayIDRequestsSent.empty() )
		{
		bool found=true;
		while ( found )
		    {
		    found=false;
		    idIter = fReplayIDRequestsSent.begin();
		    idEnd = fReplayIDRequestsSent.end();
		    while ( idIter != idEnd )
			{
			if ( *idIter == eventID )
			    {
			    sendBackground = true;
			    fReplayIDRequestsSent.erase( idIter );
			    found=true;
			    break;
			    }
			idIter++;
			}
		    }
		}
	    if ( !replayEvent && !fFirstOrbitEventRequestsSent.empty() )
		{
		bool found=true;
		while ( found )
		    {
		    found=false;
		    idIter = fFirstOrbitEventRequestsSent.begin();
		    idEnd = fFirstOrbitEventRequestsSent.end();
		    while ( idIter != idEnd )
			{
			if ( *idIter <= (eventID & 0xFFFFFFFFFFFFF000ULL) )
			    {
			    sendBackground = true;
			    fFirstOrbitEventRequestsSent.erase( idIter );
			    found=true;
			    break;
			    }
			idIter++;
			}
		    }
		}
	    }
	if ( sendBackground )
	    {
#if 1
	    if ( !PushData( *iter, pwd, blockCnt, blocks ) )
		{
		iter++;
		continue;
		}
#else
	    TPrivateWriteData writeData;
	    if ( !pwd )
		{
		pwd = reinterpret_cast<TPublicWriteData*>( fAllocCache.Get( sizeof(TPublicWriteData) ) );
		if ( !pwd )
		    {
		    LOG( AliHLTLog::kError, "TCPListenThread::Write", "Out of memory" )
			<< "Out of memory allocating public write data for connection with socket "
			<< AliHLTLog::kDec << iter->fSocket << "." << ENDLOG;
		    iter++; 
		    continue;
		    }
		pwd->fBlockCnt = blockCnt;
		pwd->fBlocks = CloneBlocks( blockCnt, blocks );
		if ( !pwd->fBlocks )
		    {
		    LOG( AliHLTLog::kError, "TCPListenThread::Write", "Out of memory" )
			<< "Out of memory allocating block copy for connection with socket "
			<< iter->fSocket << "." << ENDLOG;
		    fAllocCache.Release( (uint8*)pwd );
		    iter++; 
		    continue;
		    }
		pwd->fConnections = 0;
		pwd->fConnectionsDone = 0;
		}
	    writeData.fBinary = (iter->fDataMode==kBinary);
	    writeData.fPublic = pwd;
	    writeData.fWritten = 0;
	    if ( iter->fSendMode == kNextEvent )
		iter->fSendMode = kNoData;
	    pwd->fConnections++;
	    iter->fWriteData.push_back( writeData );
	    LOG( AliHLTLog::kDebug, "TCPListenThread::Write", "Data marked" )
		<< "Data marked for sending to connection with socket "
		<< AliHLTLog::kDec << iter->fSocket << " (connection "
		<< pwd->fConnections << ")." << ENDLOG;
#endif
	    }
	if ( iter->fSendMode == kAllEvents )
	    blockingConnections.push_back( *iter );
	iter++;
	}

    if ( storeAdvance )
	{
	MLUCMutex::TLocker AdvanceSupplyEventLocker( fAdvanceSupplyEventMutex );
	if ( fAdvanceSupplyEvent )
	    {
	    Release( fAdvanceSupplyEvent );
	    fAdvanceSupplyEvent = NULL;
	    }
	fAdvanceSupplyEvent = reinterpret_cast<TPublicWriteData*>( fAllocCache.Get( sizeof(TPublicWriteData) ) );
	if ( !fAdvanceSupplyEvent )
	    {
	    LOG( AliHLTLog::kError, "TCPListenThread::Write", "Out of memory" )
		<< "Out of memory allocating advance supply write data." << ENDLOG;
	    }
	if ( fAdvanceSupplyEvent )
	    {
	    LOG( AliHLTLog::kDebug, "TCPListenThread::Write", "Storing Advance Supply Event" )
		<< "Storing event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") as advance supply event." << ENDLOG;
	    fAdvanceSupplyEvent->fBlockCnt = blockCnt;
	    fAdvanceSupplyEvent->fBlocks = CloneBlocks( blockCnt, blocks );
	    if ( !fAdvanceSupplyEvent->fBlocks )
		{
		LOG( AliHLTLog::kError, "TCPListenThread::Write", "Out of memory" )
		    << "Out of memory allocating advance supply block copy." << ENDLOG;
		fAllocCache.Release( (uint8*)fAdvanceSupplyEvent );
		fAdvanceSupplyEvent = NULL;
		}
	    else
		{
		fAdvanceSupplyEvent->fConnections = 1;
		fAdvanceSupplyEvent->fConnectionsDone = 0;
		}
	    }
	}
    pthread_mutex_unlock( &fSocketMutex );

    TPublicWriteData spwd;
    spwd.fBlockCnt = blockCnt;
    spwd.fBlocks = blocks;

    iter = blockingConnections.begin();
    end = blockingConnections.end();
    int ret;
    unsigned long successes=0;
    while ( iter != end )
	{
	ret = WriteToConnection( *iter, spwd, true );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "TCPListenThread::Write", "Error writing data" )
		<< "Error writing data to blocking connection with socket "
		<< AliHLTLog::kDec << iter->fSocket << ": " << strerror(ret)
		<< " (" << ret << ")." << ENDLOG;
	    }
	else
	    successes++;
	iter++;
	}
    
    LOG( AliHLTLog::kDebug, "TCPListenThread::Write", "Data written" )
	<< "Data written to " << AliHLTLog::kDec << successes
	<< " of " << blockingConnections.size() << " blocking connections."
	<< ENDLOG;
    }

void TCPListenThread::GetReplayIDRequests( std::vector<AliUInt64_t>& replayIDs )
    {
    replayIDs.clear();
    pthread_mutex_lock( &fReplayIDRequestMutex );
    replayIDs = fReplayIDRequests;
    pthread_mutex_unlock( &fReplayIDRequestMutex );
    }

void TCPListenThread::GetFirstOrbitEventMonitorRequests( std::vector<AliUInt64_t>& orbits )
    {
    orbits.clear();
    pthread_mutex_lock( &fFirstOrbitEventRequestMutex );
    orbits = fFirstOrbitEventRequests;
    pthread_mutex_unlock( &fFirstOrbitEventRequestMutex );
    }

void TCPListenThread::ClearReplayIDRequests()
    {
    pthread_mutex_lock( &fReplayIDRequestMutex );
    fReplayIDRequests.clear();
    pthread_mutex_unlock( &fReplayIDRequestMutex );
    }

void TCPListenThread::ClearFirstOrbitEventMonitorRequests()
    {
    pthread_mutex_lock( &fFirstOrbitEventRequestMutex );
    fFirstOrbitEventRequests.clear();
    pthread_mutex_unlock( &fFirstOrbitEventRequestMutex );
    }

void TCPListenThread::SetSentReplayIDRequests( std::vector<AliUInt64_t> const & replayIDs )
    {
    pthread_mutex_lock( &fReplayIDRequestMutex );
    fReplayIDRequestsSent.insert( fReplayIDRequestsSent.end(), replayIDs.begin(), replayIDs.end() );
    pthread_mutex_unlock( &fReplayIDRequestMutex );
    }

void TCPListenThread::SetSentFirstOrbitEventMonitorRequests( std::vector<AliUInt64_t> const & orbits )
    {
    pthread_mutex_lock( &fFirstOrbitEventRequestMutex );
    fFirstOrbitEventRequestsSent.insert( fFirstOrbitEventRequestsSent.end(), orbits.begin(), orbits.end() );
    pthread_mutex_unlock( &fFirstOrbitEventRequestMutex );
    }

bool TCPListenThread::ServiceChanged()
{
    return serviceChanged;
}

int TCPListenThread::SpecMapSize()
{
    return detSpecMap.size();
}

AliUInt32_t TCPListenThread::GetSpecification(vector<string> specKey)
{
    if (detSpecMap.find(specKey) != detSpecMap.end())
        return detSpecMap[specKey];
    return 0xFFFFFFFF;
}

void TCPListenThread::PrintSpecMap()
{
    for (map<vector<string>, AliUInt32_t>::iterator it = detSpecMap.begin(); it != detSpecMap.end(); it++) {
        cout << "origin: " << (*it).first[0] << " type: " << (*it).first[1] << " sector: " << (*it).first[2]
            << " spec: " << (*it).second << endl;
    }
}

void TCPListenThread::SetServiceType(AliHLTSubEventDescriptor::BlockData* blocks, AliUInt32_t blockCnt)
    {
    if ( blocks )
    {
    for ( unsigned nn=0; nn<blockCnt; nn++ )
        {
        if (blocks[nn].fDataType.fID==(((AliUInt64_t)'STAR')<<32 | 'TOFR'))
            return;
        }
    }
    
    serviceChanged = false;
    updateSpecMap = false;
    
    vector <string> dataOriginType(3);
    dataOriginStr = std::string("O=1234");
    dataTypeStr = std::string("T=12345678");
    
    // For each bloc, store key/value pairs with key being origin, type and sector.
    // Sector currently only used for TPC CLUSTER and are set to 0 for anything else.
    // The value is the specification.
    // Could also store origin, type and specification as an element in a set. 
    // The set would make sure all elements were unique and avhi would then be updated 
    // only if the set changed.
    for ( unsigned long blockIdx = 0; blockIdx < blockCnt; blockIdx++ ) {
        
        for(int i=0;i<4;++i)
            dataOriginStr[strlen("O=")+i]=blocks[blockIdx].fDataOrigin.fDescr[3-i];
        
        for(int i=0;i<8;++i)
            dataTypeStr[strlen("T=")+i]=blocks[blockIdx].fDataType.fDescr[7-i];
        
        dataOriginType[0] = dataOriginStr;
        dataOriginType[1] = dataTypeStr;
        
        if ((dataTypeStr.find("CLUSTERS") != string::npos) && (dataOriginStr.find("TPC") != string::npos)) {
            
            sector = (blocks[blockIdx].fDataSpecification >> 16);
            sprintf(strBuff, "%d", sector);
            
            dataOriginType[2] = strBuff;
            
            if (detSpecMap.find(dataOriginType) == detSpecMap.end()) {
                detSpecMap.insert(pair<vector<string>, AliUInt32_t>(dataOriginType, blocks[blockIdx].fDataSpecification));
                serviceChanged = true;
            } else {
                maxsector = (blocks[blockIdx].fDataSpecification >> 24);
                minsector = ((blocks[blockIdx].fDataSpecification & 0x00FF0000) >> 16);
                maxpatch = ((blocks[blockIdx].fDataSpecification & 0x0000FF00) >> 8);
                minpatch = (blocks[blockIdx].fDataSpecification & 0x000000FF);
                
                if (maxsector == minsector) {
                    
                    tmpMaxpatch = ((detSpecMap[dataOriginType] & 0xFF00) >> 8);
                    tmpMinpatch = (detSpecMap[dataOriginType] & 0x00FF);
                    
                    if (tmpMaxpatch < maxpatch) {
                        tmpMaxpatch = maxpatch;
                        updateSpecMap = true;
                    }
                    if (tmpMinpatch > minpatch) {
                        tmpMinpatch = minpatch;
                        updateSpecMap = true;
                    }
                    if (updateSpecMap) {
                        detSpecMap[dataOriginType] = (((sector) << 16) | ((tmpMaxpatch & 0xFF) << 8) | (tmpMinpatch & 0xFF));
                        updateSpecMap = false;
                        serviceChanged = true;
                        
                    }
                }
            }
        } else {
            // Dummy. Sector id only needed for TPC CLUSTERS
            sprintf(strBuff, "%d", 0x0000);
            dataOriginType[2] = strBuff;
            
            // Insert origin, type and specification if not already exists.
            if (detSpecMap.find(dataOriginType) == detSpecMap.end()) {
                detSpecMap.insert(pair<vector<string>, AliUInt32_t>(dataOriginType, blocks[blockIdx].fDataSpecification));
                serviceChanged = true;
            } else {
                // Update specification if changes with new information.
                if ( (detSpecMap[dataOriginType] | blocks[blockIdx].fDataSpecification) != detSpecMap[dataOriginType] ) {
                    detSpecMap[dataOriginType] = (detSpecMap[dataOriginType] | blocks[blockIdx].fDataSpecification);
                    serviceChanged = true;
                }
            }
        }
    }
#ifdef USE_AVAHI
    // If service info changed, update avahi.
    if (serviceChanged) {
      //LOG(AliHLTLog::kImportant, "TCPListenThread::SetService", "Service change")
      //    << "Service changed updating Avahi." << ENDLOG;
        
        txtList.clear();
        
        for (detSpecMapIter = detSpecMap.begin(); detSpecMapIter != detSpecMap.end(); detSpecMapIter++) {
            sprintf(strBuff, "%d", (*detSpecMapIter).second);
            txtList.push_back((*detSpecMapIter).first[0] + " " + (*detSpecMapIter).first[1] + " S=" + strBuff);
        }
        
        for (txtListIter =  txtList.begin(); txtListIter != txtList.end(); txtListIter++) {
            LOG(AliHLTLog::kImportant, "TCPListenThread::SetService", "TXT string")
                << (*txtListIter).c_str() << ENDLOG;
        }
        
        //Avahi.ModifyService(0, "_tcpdump._tcp", fListenPort, txtList);
    }
#else
    if (serviceChanged) {
      LOG(AliHLTLog::kImportant, "TCPListenThread::SetService", "Service change")
	<< "Service changed, publishing the following blocks on port " << AliHLTLog::kDec << fListenPort << ":" << ENDLOG;
        
      for (detSpecMapIter = detSpecMap.begin(); detSpecMapIter != detSpecMap.end(); detSpecMapIter++) {
	LOG(AliHLTLog::kInformational, "TCPListenThread::SetService", "TXT string")
	  << (*detSpecMapIter).first[0].c_str() << " "
	  << (*detSpecMapIter).first[1].c_str() << " "
	  << "S=0x"   << AliHLTLog::kHex << (*detSpecMapIter).second << AliHLTLog::kDec
	  << ENDLOG;
      }
    }
#endif // USE_AVAHI
}

void TCPListenThread::Run()
    {
    fQuitted = false;
    fQuit = false;

    fServiceTypeSet = false;
    //Avahi.Initialize("TCPDumpSubscriber");
    //Avahi.AddService("_tcpdump._tcp", fListenPort, 0);
    int ret;
    int newConnection;
    std::vector<TConnection> readSockets;
    std::vector<TConnection> writeSockets;
    std::vector<int> closedSockets;
    while ( !fQuit )
    {
    closedSockets.clear();
    ret = Listen( newConnection, readSockets, writeSockets );
    std::vector<TConnection>::iterator iter, end;
    if ( readSockets.size()>0 )
    {
        iter = readSockets.begin();
        end = readSockets.end();
        while ( iter != end )
		{
		MLUCString cmd;
		bool moreData;
		bool first = true;
		TConnection connData = *iter;
		do
		    {
		    cmd = "";
		    ret = ReadCommand( connData, cmd, first, moreData );
		    LOG( AliHLTLog::kDebug, "TCPListenThread::Run", "ReadCommand" )
			<< AliHLTLog::kDec << "ret: " << ret << " - cmd: '" << cmd.c_str()
			<< "' - first: " << (first ? "yes" : "no") << " - moreData: "
			<< (moreData ? "yes" : "no" ) << "." << ENDLOG;
		    first = false;
		    if ( ret && ret!=EAGAIN && ret!=EINTR && ret!=ECONNABORTED )
			{
			LOG( AliHLTLog::kError, "TCPListenThread::Run", "Error reading socket" )
			    << "Error reading command from socket " << AliHLTLog::kDec << iter->fSocket
			    << ": " << strerror(ret) << " (" << ret << ")." << ENDLOG;
			CloseConnection( iter->fSocket );
			closedSockets.push_back( iter->fSocket );
			}
		    else if ( ret==ECONNABORTED )
			{
			LOG( AliHLTLog::kDebug, "TCPListenThread::Run", "Connection closed" )
			    << "Connection from socket " << AliHLTLog::kDec << iter->fSocket
			    << " closed by remote side." << ENDLOG;
			CloseConnection( iter->fSocket );
			closedSockets.push_back( iter->fSocket );
			}
		    else if ( (ret==EAGAIN || ret==EINTR) && cmd.Length()>0 )
			{
			connData.fReadData.fCmd += cmd;
			LOG( AliHLTLog::kDebug, "TCPListenThread::Run", "Command fragment" )
			    << "Received command fragment: '" << cmd.c_str() << "'." << ENDLOG;
			}
		    else if ( cmd.Length()>0 )
			{
			LOG( AliHLTLog::kDebug, "TCPListenThread::Run", "Command fragment" )
			    << "Received command fragment: '" << cmd.c_str() << "'." << ENDLOG;
			connData.fReadData.fCmd += cmd;
			LOG( AliHLTLog::kDebug, "TCPListenThread::Run", "Command received" )
			    << "Received command: '" << connData.fReadData.fCmd.c_str() << "'." << ENDLOG;
			if ( !ProcessCommand( connData ) )
			    {
			    LOG( AliHLTLog::kError, "TCPListenThread::Run", "Error processing command" )
				<< "Error processing command '" << cmd.c_str() << "' from socket " << AliHLTLog::kDec << iter->fSocket
				<< "." << ENDLOG;
			    CloseConnection( iter->fSocket );
			    closedSockets.push_back( iter->fSocket );
			    }
			connData.fReadData.fCmd = "";
			}
		    // else nothing read
		    }
		while ( moreData && !ret );

		if ( !UpdateConnection( connData ) )
		    {
#if 0
		    // Do not release the write data.
		    // If the update failed the socket has been closed by a CloseConnection call above, and this involves releasing occupied resources.
		    while ( !connData.fWriteData.empty() )
			{
			if ( connData.fWriteData.begin()->fPublic )
			    Release( connData.fWriteData.begin()->fPublic );
			connData.fWriteData.erase( connData.fWriteData.begin() );
			}
#endif
		    // Most likely this socket was already added to the closedSockets earlier after the CloseConnection call,
		    // but just in case check if we have to add it again
		    bool foundClosed=false;
		    std::vector<int>::iterator ciIter, ciEnd;
		    ciIter = closedSockets.begin();
		    ciEnd = closedSockets.end();
		    while ( ciIter != ciEnd )
			{
			if ( *ciIter == connData.fSocket )
			    {
			    foundClosed = true;
			    break;
			    }
			ciIter++;
			}
		    if ( !foundClosed )
			closedSockets.push_back( connData.fSocket );
		    }
		// XXX TODO: After failed update and if data to write in background, release pwd
		iter++;
		}
	    }
	if ( writeSockets.size()>0 )
	    {
	    if ( !closedSockets.empty() )
		{
		std::vector<int>::iterator ciIter, ciEnd;
		ciIter = closedSockets.begin();
		ciEnd = closedSockets.end();
		while ( ciIter != ciEnd )
		    {
		    iter = writeSockets.begin();
		    end = writeSockets.end();
		    while ( iter != end )
			{
			if ( *ciIter==iter->fSocket )
			    {
			    writeSockets.erase( iter );
			    break;
			    }
			iter++;
			}
		    ciIter++;
		    }
		}
	    iter = writeSockets.begin();
	    end = writeSockets.end();
	    while ( iter != end )
		{
		TConnection &connData = *iter;
		
		if ( !connData.fWriteData.empty() )
		    {
		    int ret;
		    LOG( AliHLTLog::kDebug, "TCPListenThread::Run", "Writing data" )
			<< "Writing background data to socket " << AliHLTLog::kDec << iter->fSocket 
			<< ": " << AliHLTLog::kDec << connData.fWriteData.begin()->fPublic->fBlockCnt
			<< " blocks - mode " << ( connData.fWriteData.begin()->fBinary ? "binary" : "ascii" )
			<< " - " << connData.fWriteData.begin()->fWritten << " bytes already written." << ENDLOG;
		    ret = WriteToConnection( connData, *connData.fWriteData.begin()->fPublic, false );
		    LOG( AliHLTLog::kDebug, "TCPListenThread::Run", "Data written" )
			<< "Wrote background data: " << AliHLTLog::kDec << connData.fWriteData.begin()->fWritten 
			<< " bytes written now." << ENDLOG;
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "TCPListenThread::Run", "Error writing to connection" )
			    << "Error writing to connection for socket " << AliHLTLog::kDec << iter->fSocket
			    << ": " << strerror(ret) << " (" << ret << ")." << ENDLOG;
			CloseConnection( connData.fSocket );
			}
		    
		    if ( !UpdateConnection( connData ) )
			{
#if 0
			// Do not release the write data.
			// If the update failed the socket has been closed by a CloseConnection call above, and this involves releasing occupied resources.
			while ( !connData.fWriteData.empty() )
			    {
			    if ( connData.fWriteData.begin()->fPublic )
				Release( connData.fWriteData.begin()->fPublic );
			    connData.fWriteData.erase( connData.fWriteData.begin() );
			    }
#endif
			}
		    }
		iter++;
		}
	    }
	if ( newConnection!=-1 )
	    AddConnection( newConnection );
	}
    CloseSocket();
    //Avahi.Release();
    fQuitted = true;
    }

int TCPListenThread::ReadCommand( TConnection& connData, MLUCString& cmd, bool first, bool& moreData )
    {
    cmd = "";
    if ( connData.fReadData.fBufferSize == 0 )
	{
	moreData = false;
	int ret;
	ret = read( connData.fSocket, connData.fReadData.fBuffer, 64 );
	if ( ret<0 )
	    return errno;
	if ( ret==0 && first )
	    return ECONNABORTED;
	connData.fReadData.fBufferSize = ret;
	connData.fReadData.fBufferStart = 0;
	}
    bool cmdFull=true;
    if ( connData.fReadData.fBufferSize > 0 )
	{
	unsigned n = connData.fReadData.fBufferStart;
	while ( n<connData.fReadData.fBufferSize+connData.fReadData.fBufferStart && connData.fReadData.fBuffer[n] != '\n' )
	    {
	    cmd += connData.fReadData.fBuffer[n];
	    n++;
	    }
	if ( n<connData.fReadData.fBufferSize+connData.fReadData.fBufferStart && connData.fReadData.fBuffer[n]=='\n' )
	    n++;
	else
	    cmdFull = false;
	connData.fReadData.fBufferSize -= (n-connData.fReadData.fBufferStart);
	connData.fReadData.fBufferStart = n;
	}
    if ( connData.fReadData.fBufferSize>0 )
	moreData = true;
    else
	moreData = false;
    if ( cmdFull )
	return 0;
    else
	return EAGAIN;
    }

bool TCPListenThread::ProcessCommand( TConnection& connData )
    {
    if ( connData.fReadData.fCmd == "GET ONE" )
	{
	connData.fSendMode = kNextEvent;
	LOG( AliHLTLog::kDebug, "TCPListenThread::ProcessCommand", "Next Event Mode" )
	    << "Connection for socket " << AliHLTLog::kDec << connData.fSocket
	    << " switching to NextEvent Mode." << ENDLOG;
	MLUCMutex::TLocker AdvanceSupplyEventLocker( fAdvanceSupplyEventMutex );
	if ( fAdvanceSupplyEvent )
	    {
	    PushData( connData, fAdvanceSupplyEvent, fAdvanceSupplyEvent->fBlockCnt, fAdvanceSupplyEvent->fBlocks );
	    LOG( AliHLTLog::kDebug, "TCPListenThread::Write", "Providing Advance Supply Event" )
		<< "Fulfilling event request from advance supply event." << ENDLOG;
	    }
	}
    else if ( connData.fReadData.fCmd == "GET ALL" && fBlockingModeAllowed )
	{
	connData.fSendMode = kAllEvents;
	LOG( AliHLTLog::kDebug, "TCPListenThread::ProcessCommand", "All Events Mode" )
	    << "Connection for socket " << AliHLTLog::kDec << connData.fSocket
	    << " switching to AllEvents Mode." << ENDLOG;
	}
    else if ( connData.fReadData.fCmd == "GET NON" )
	{
	connData.fSendMode = kNoData;
	LOG( AliHLTLog::kDebug, "TCPListenThread::ProcessCommand", "No Event Mode" )
	    << "Connection for socket " << AliHLTLog::kDec << connData.fSocket
	    << " switching to NoEvent Mode." << ENDLOG;
	}
    else if ( connData.fReadData.fCmd == "MOD ASC" )
	{
	connData.fDataMode = kASCII;
	LOG( AliHLTLog::kDebug, "TCPListenThread::ProcessCommand", "ASCII Mode" )
	    << "Connection for socket " << AliHLTLog::kDec << connData.fSocket
	    << " switching to ASCII Mode." << ENDLOG;
	}
    else if ( connData.fReadData.fCmd == "MOD BIN" )
	{
	connData.fDataMode = kBinary;
	LOG( AliHLTLog::kDebug, "TCPListenThread::ProcessCommand", "Binary Mode" )
	    << "Connection for socket " << AliHLTLog::kDec << connData.fSocket
	    << " switching to Binary Mode." << ENDLOG;
	}
    else if ( connData.fReadData.fCmd.Substr( 0, strlen("REPLAY ID ") ) == "REPLAY ID " )
	{
	char* cpErr;
	AliUInt64_t eventID = strtoul( connData.fReadData.fCmd.Substr( strlen("REPLAY ID ") ).c_str(), &cpErr, 0 );
	if ( *cpErr != '\0' )
	    {
	    return false;
	    }
	pthread_mutex_lock( &fReplayIDRequestMutex );
	fReplayIDRequests.push_back( eventID );
	pthread_mutex_unlock( &fReplayIDRequestMutex );
	connData.fReplayIDRequests.push_back( eventID );
	}
    else if ( connData.fReadData.fCmd.Substr( 0, strlen("FIRST ORBIT EVENT ") ) == "FIRST ORBIT EVENT " )
	{
	char* cpErr;
	AliUInt64_t orbit = strtoul( connData.fReadData.fCmd.Substr( strlen("FIRST ORBIT EVENT ") ).c_str(), &cpErr, 0 );
	if ( *cpErr != '\0' )
	    {
	    return false;
	    }
	pthread_mutex_lock( &fFirstOrbitEventRequestMutex );
	fFirstOrbitEventRequests.push_back( orbit );
	pthread_mutex_unlock( &fFirstOrbitEventRequestMutex );
	connData.fFirstOrbitEventRequests.push_back( orbit );
	}
    else
	{
	return false;
	}
    return true;
    }


int TCPListenThread::OpenSocket()
    {
    if ( fListenSocket == -1 )
	{
	close( fListenSocket );
	fListenSocket = -1;
	}
    int ret;
    struct protoent* proto;
    proto = getprotobyname( "tcp" );
    if ( !proto )
	{
	ret = errno;
	LOG( AliHLTLog::kError, "TCPListenThread::OpenSocket", "Protocol File Error" )
	    << "Error reading TCP protocol number from /etc/protocols file: " 
	    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	return ret;
	}
    int one=1;
    fListenSocket = socket( AF_INET, SOCK_STREAM, proto->p_proto );
    if ( fListenSocket == -1 )
	{
	ret = errno;
	LOG( AliHLTLog::kError, "TCPListenThread::OpenSocket", "Socket Creation Error" )
	    << "Error creating TCP socket: " 
	    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	return ret;
	}
    
    ret = setsockopt( fListenSocket, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one) );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "TCPListenThread::OpenSocket", "Socket reuse addr" )
	    << "Error setting sockets options to reuse addr: "
	    << strerror(errno) << " (" << AliHLTLog::kDec << errno << ")." << ENDLOG;
	}

    struct sockaddr_in sock_addr;
    sock_addr.sin_family = AF_INET;
    sock_addr.sin_port = htons( fListenPort );
    sock_addr.sin_addr.s_addr = htonl( INADDR_ANY );
    memset(&(sock_addr.sin_zero), '\0', 8);
    ret = bind( fListenSocket,  (const sockaddr*)&sock_addr, sizeof(struct sockaddr) );
    if ( ret==-1 )
	{
	ret = errno;
	LOG( AliHLTLog::kError, "TCPListenThread::OpenSocket", "Socket Bind Error" )
	    << "Error bind'ing TCP socket on port " << AliHLTLog::kDec << fListenPort
	    << " (0x" << AliHLTLog::kHex << fListenPort << "): "
	    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	close( fListenSocket );
	fListenSocket = -1;
	return ret;
	}

    ret = listen( fListenSocket, 5 );
    if ( ret == -1 )
	{
	ret = errno;
	LOG( AliHLTLog::kError, "TCPListenThread::OpenSocket", "Socket Listen Error" )
	    << "Error listen'ing with TCP socket on port " << AliHLTLog::kDec << fListenPort
	    << " (0x" << AliHLTLog::kHex << fListenPort << "): "
	    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	close( fListenSocket );
	fListenSocket = -1;
	return ret;
	}
    return 0;
    }

int TCPListenThread::CloseSocket()
    {
    close( fListenSocket );
    fListenSocket = -1;
    return 0;
    }

int TCPListenThread::Listen( int& newConnection, std::vector<TConnection>& readSocks, std::vector<TConnection>& writeSocks )
    {
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 100000;

    fd_set readSockets, writeSockets;
    int ret, highest_sock;
    readSocks.clear();
    writeSocks.clear();

    FD_ZERO( &readSockets);
    FD_ZERO( &writeSockets);
    FD_SET( fListenSocket, &readSockets );
    highest_sock = fListenSocket;

    //Avahi.PrepareSelect(highest_sock,&readSockets,&writeSockets,&tv);
	
    std::vector<TConnection> socketList;
    GetAllConnectionSockets( socketList );
    std::vector<TConnection>::iterator iter, end;
    iter = socketList.begin();
    end = socketList.end();
    while ( iter != end )
	{
	FD_SET( iter->fSocket, &readSockets );
 	if ( iter->fSocket > highest_sock )
	    highest_sock = iter->fSocket;
	if ( !iter->fWriteData.empty() )
	    {
	    FD_SET( iter->fSocket, &writeSockets );
	    LOG( AliHLTLog::kDebug, "TCPListenThread::Listen", "Adding write socket" )
		<< "Adding socket " << AliHLTLog::kDec << iter->fSocket << " to write candidate list." << ENDLOG;
	    }
	iter++;
	}

    newConnection = -1;
    readSocks.clear();
    writeSocks.clear();

    ret = select( highest_sock+1, &readSockets, &writeSockets, NULL, &tv );
    if ( ret<0 )
	return ret;
    //Avahi.PostSelect(&readSockets,&writeSockets);
    if ( ret==0 )
	return 0;
    
    if ( FD_ISSET( fListenSocket, &readSockets ) )
	{
	struct sockaddr_in addr;
	socklen_t addrLen;
	addrLen = sizeof(addr);
	newConnection = accept( fListenSocket, (struct sockaddr*)&addr, &addrLen );
	}
    
    iter = socketList.begin();
    end = socketList.end();
    while ( iter != end && (readSocks.size()+writeSocks.size()<(unsigned)ret) )
	{
	if ( FD_ISSET( iter->fSocket, &readSockets ) )
	    {
	    readSocks.push_back( *iter );
	    }
	if ( FD_ISSET( iter->fSocket, &writeSockets ) )
	    {
	    LOG( AliHLTLog::kDebug, "TCPListenThread::Listen", "Adding write socket" )
		<< "Adding socket " << AliHLTLog::kDec << iter->fSocket << " to writable list." << ENDLOG;
	    writeSocks.push_back( *iter );
	    }
	iter++;
	}
    return 0;
    }
 
void TCPListenThread::AddConnection( int sock )
    {
    // XXX TODO: Initialize read/write data
    fcntl( sock, F_SETFL, O_NONBLOCK );
    TConnection connData;
    connData.fSocket = sock;
    connData.fSendMode = kNoData;
    connData.fDataMode = kASCII;
    connData.fReadData.fCmd = "";
    connData.fReadData.fBufferSize = 0;
    connData.fReadData.fBufferStart = 0;
    pthread_mutex_lock( &fSocketMutex );
    fSockets.push_back( connData );
    pthread_mutex_unlock( &fSocketMutex );
    }

// bool TCPListenThread::GetConnection( int sock, TConnection& connData )
//     {
//     std::vector<TConnection>::iterator iter, end;
//     bool found=false;
//     pthread_mutex_lock( &fSocketMutex );
//     iter = fSockets.begin();
//     end = fSockets.end();
//     while ( iter != end )
// 	{
// 	if ( iter->fSocket == sock )
// 	    {
// 	    connData = *iter;
// 	    found=true;
// 	    break;
// 	    }
// 	iter++;
// 	}
//     pthread_mutex_unlock( &fSocketMutex );
//     return found;
//     }

bool TCPListenThread::UpdateConnection( const TConnection& connData )
    {
    std::vector<TConnection>::iterator iter, end;
    bool found=false;
    pthread_mutex_lock( &fSocketMutex );
    iter = fSockets.begin();
    end = fSockets.end();
    while ( iter != end )
	{
	if ( iter->fSocket == connData.fSocket )
	    {
	    *iter = connData;
	    found=true;
	    break;
	    }
	iter++;
	}
    pthread_mutex_unlock( &fSocketMutex );
    return found;
    }


void TCPListenThread::CloseConnection( int sock )
    {
    pthread_mutex_lock( &fSocketMutex );
    std::vector<TConnection>::iterator iter, end;
    iter = fSockets.begin();
    end = fSockets.end();
    while ( iter != end )
	{
	if ( iter->fSocket == sock )
	    {
	    while ( !iter->fWriteData.empty() )
		{
		if ( iter->fWriteData.begin()->fPublic )
		    Release( iter->fWriteData.begin()->fPublic );
		iter->fWriteData.erase( iter->fWriteData.begin() );
		}
	    fSockets.erase( iter );
	    break;
	    }
	iter++;
	}
    pthread_mutex_unlock( &fSocketMutex );
    close( sock );
    }

int TCPListenThread::WriteToConnection( TConnection& connData, const TPublicWriteData& pwd, bool blocking )
    {
    if ( connData.fWriteData.empty() )
	return 0;
    iovec ioVecs[pwd.fBlockCnt+1]; // 1 more for initial size block.
    AliUInt32_t totalSize=0, totalSizeNBO, realTotalSize;
    char totalSizeStr[ 64 ];
    for ( unsigned n = 0; n < pwd.fBlockCnt; n++ )
	{
	totalSize += pwd.fBlocks[n].iov_len;
	ioVecs[n+1] = pwd.fBlocks[n];
	}
    // totalSize does NOT include itself!!!
    // But realTotalSize does
    realTotalSize = totalSize;
    if ( connData.fWriteData.begin()->fBinary )
	{
	totalSizeNBO = htonl( totalSize );
	ioVecs[0].iov_base = &totalSizeNBO;
	ioVecs[0].iov_len = sizeof(totalSizeNBO);
	}
    else
	{
	sprintf( totalSizeStr, "%u\n", totalSize );
	ioVecs[0].iov_base = totalSizeStr;
	ioVecs[0].iov_len = strlen(totalSizeStr);
	}
    realTotalSize += ioVecs[0].iov_len;

    unsigned long written=0;
    if ( !blocking )
	written = connData.fWriteData.begin()->fWritten;

    int ret = WriteToConnection( connData.fSocket, pwd.fBlockCnt+1, ioVecs, blocking, written );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "TCPListenThread::WriteToConnection", "Write error" )
	    << "Write error writing data to connection with socket " << AliHLTLog::kDec
	    << connData.fSocket << ": " << strerror(ret) << " (" << ret << ")." << ENDLOG;
	}
    else if ( !blocking && written == realTotalSize )
	{
	Release( connData.fWriteData.begin()->fPublic );
	connData.fWriteData.erase( connData.fWriteData.begin() );
	}
    else if ( !blocking )
	{
	connData.fWriteData.begin()->fWritten = written;
	}
    
    return ret;
    }

int TCPListenThread::WriteToConnection( int connSock, unsigned blockcnt, iovec* blocks, bool blocking, unsigned long& szWritten )
    {
    unsigned nextBuffer = 0;
    uint32 toWrite[ blockcnt ];
    toWrite[0] = blocks[0].iov_len;
	LOG( AliHLTLog::kDebug, "TCPWriter::WriteToConnection", "Blocks" )
	    << "Block 0 size: " << AliHLTLog::kDec
	    << blocks[0].iov_len << " - to write: " << toWrite[0]
	    << ENDLOG;
    for ( unsigned ic=1; ic<blockcnt; ic++ )
	{
	toWrite[ic] = toWrite[ic-1]+blocks[ic].iov_len;
	LOG( AliHLTLog::kDebug, "TCPWriter::WriteToConnection", "Blocks" )
	    << "Block " << AliHLTLog::kDec << ic << " size: " 
	    << blocks[ic].iov_len << " - to write: " << toWrite[ic]
	    << ENDLOG;
	}
    uint32 totalToWrite = toWrite[blockcnt-1];
    bool retry=false;
    unsigned tempCount, bi, tempOffset;
    struct iovec ioVecs[blockcnt];
    bool first = true;
    int ret, retval = 0;

    if ( szWritten >= totalToWrite )
	return 0;
    while ( nextBuffer<blockcnt && toWrite[nextBuffer]<=szWritten )
	nextBuffer++;
    LOG( AliHLTLog::kDebug, "TCPWriter::WriteToConnection", "Start/Resume" )
	<< "szWritten: " << AliHLTLog::kDec << szWritten << " - nextBuffer: "
	<< nextBuffer << ENDLOG;

    while ( szWritten < totalToWrite )
	{
	retry = true;
	retval=0;
	do
	    {
	    tempCount=0;
	    bi = nextBuffer;
	    if ( bi>0 )
		tempOffset = szWritten-toWrite[bi-1];
	    else
		tempOffset = szWritten;
	    while ( bi<blockcnt )
		{
		LOG( AliHLTLog::kDebug, "TCPWriter::WriteToConnection", "Buffer offsets" )
		    << "Block " << AliHLTLog::kDec << (unsigned)bi << "(" << tempCount << ") is being written from offset "
		    << tempOffset 
		    << " - Length to be written: " << (unsigned)(blocks[bi].iov_len-tempOffset) << " of "
		    << (unsigned)(blocks[bi].iov_len) << " bytes." << ENDLOG;
		ioVecs[tempCount].iov_base = (void*)( ((uint8*)blocks[bi].iov_base)+tempOffset );
		ioVecs[tempCount].iov_len = blocks[bi].iov_len-tempOffset;
		tempCount++;
		bi++;
		tempOffset = 0;
		}
	    ret = writev( connSock, ioVecs, tempCount );
	    LOG( AliHLTLog::kDebug, "TCPWriter::WriteToConnection", "writev" )
		<< "writev return value: " << AliHLTLog::kDec << ret << ENDLOG;
	    if ( ret > 0 )
		{
		first = true;
		szWritten += ret;
		if ( szWritten<totalToWrite )
		    {
		    while ( nextBuffer<blockcnt && toWrite[nextBuffer]<=szWritten )
			nextBuffer++;
		    }
		}
	    if ( ret==0 && !first )
		{
		retval = EPIPE;
		ret = -1;
		LOG( AliHLTLog::kError, "TCPWriter::WriteToConnection", "Write error" )
		    << "Connection error on writev call: " << strerror(retval) << " (" 
		    << AliHLTLog::kDec << retval << ")." << ENDLOG;
		break;
		}
	    if ( ret < 0 && errno!=EAGAIN && errno!=EINTR )
		{
		retval = errno;
		LOG( AliHLTLog::kError, "TCPWriter::WriteToConnection", "Write error" )
		    << "Error on writev call: " << strerror(retval) << " (" 
		    << AliHLTLog::kDec << retval << ")." << ENDLOG;
		break;
		}
	    if ( ret==0 || (ret < 0 && errno==EAGAIN) || !blocking )
		retry = false;
	    }
	while ( retry && szWritten < totalToWrite );
	if ( szWritten >= totalToWrite )
	    break;
	if ( retval != 0 )
	    break;
	if ( !blocking )
	    break;
	do
	    {
	    fd_set sockets;
	    struct timeval tv, *ptv;
	    FD_ZERO( &sockets );
	    FD_SET( connSock, &sockets );
	    if ( fTimeout )
		{
		tv.tv_sec = fTimeout/1000000;
		tv.tv_usec = fTimeout-tv.tv_sec*1000000;
		ptv = &tv;
		}
	    else
		ptv = NULL;
	    errno = 0;
	    ret = select( connSock+1, NULL, &sockets, NULL, ptv );
	    }
	while ( ret<=0 && (errno==EINTR || errno==EAGAIN) );
	if ( ret == 0 )
	    {
	    LOG( AliHLTLog::kError, "TCPWriter::WriteToConnection", "Timeout expired" )
		<< "Timeout expired while trying to do transfer: " << strerror(errno) << " (" 
		    << AliHLTLog::kDec << errno << ")." << ENDLOG;
	    retval = ETIMEDOUT;
	    break;
	    }
	if ( ret < 0 )
	    {
	    LOG( AliHLTLog::kError, "TCPWriter::WriteToConnection", "Select error" )
		<< "Error on select while trying to do transfer: " << strerror(errno) << " (" 
		<< AliHLTLog::kDec << errno << ")." << ENDLOG;
	    retval =  ENXIO;
	    break;
	    }
	first = false;
	}
    return retval;
    }


bool TCPListenThread::PushData( TConnection& connData, TPublicWriteData*& pwd, unsigned blockCnt, iovec* blocks )
    {
    TPrivateWriteData writeData;
    if ( !pwd )
	{
	pwd = reinterpret_cast<TPublicWriteData*>( fAllocCache.Get( sizeof(TPublicWriteData) ) );
	if ( !pwd )
	    {
	    LOG( AliHLTLog::kError, "TCPListenThread::Write", "Out of memory" )
		<< "Out of memory allocating public write data for connection with socket "
		<< AliHLTLog::kDec << connData.fSocket << "." << ENDLOG;
	    return false;
	    }
	pwd->fBlockCnt = blockCnt;
	pwd->fBlocks = CloneBlocks( blockCnt, blocks );
	if ( !pwd->fBlocks )
	    {
	    LOG( AliHLTLog::kError, "TCPListenThread::Write", "Out of memory" )
		<< "Out of memory allocating block copy for connection with socket "
		<< connData.fSocket << "." << ENDLOG;
	    fAllocCache.Release( (uint8*)pwd );
	    return false;
	    }
	pwd->fConnections = 0;
	pwd->fConnectionsDone = 0;
	}
    writeData.fBinary = (connData.fDataMode==kBinary);
    writeData.fPublic = pwd;
    writeData.fWritten = 0;
    if ( connData.fSendMode == kNextEvent )
	connData.fSendMode = kNoData;
    pwd->fConnections++;
    connData.fWriteData.push_back( writeData );
    LOG( AliHLTLog::kDebug, "TCPListenThread::Write", "Data marked" )
	<< "Data marked for sending to connection with socket "
	<< AliHLTLog::kDec << connData.fSocket << " (connection "
	<< pwd->fConnections << ")." << ENDLOG;
    return true;
    }



iovec* TCPListenThread::CloneBlocks( unsigned blockCnt, const iovec* blocks )
    {
    if ( !blockCnt || ! blocks )
	return NULL;
    iovec* newBlocks = reinterpret_cast<iovec*>( fAllocCache.Get( blockCnt*sizeof(iovec) ) );
    if ( !newBlocks )
	return NULL;
    memset( newBlocks, 0, blockCnt*sizeof(iovec) );
    for ( unsigned n = 0; n < blockCnt; n++ )
	{
	newBlocks[n].iov_base = reinterpret_cast<void*>( fAllocCache.Get( blocks[n].iov_len ) );
	if ( !newBlocks[n].iov_base && blocks[n].iov_len )
	    {
	    ReleaseBlocks( n, newBlocks );
	    return NULL;
	    }
	if ( newBlocks[n].iov_base && blocks[n].iov_len )
	    memcpy( newBlocks[n].iov_base, blocks[n].iov_base, blocks[n].iov_len );
	newBlocks[n].iov_len = blocks[n].iov_len;
	}
    return newBlocks;
    }

void TCPListenThread::ReleaseBlocks( unsigned blockCnt, iovec* blocks )
    {
    if ( blocks )
	{
	for ( unsigned n = 0; n < blockCnt; n++ )
	    {
	    if ( blocks[n].iov_base )
		fAllocCache.Release( (uint8*)blocks[n].iov_base );
	    }
	fAllocCache.Release( (uint8*)blocks );
	}
    }

void TCPListenThread::Release( TPublicWriteData* pwd )
    {
    if ( pwd )
	{
	pthread_mutex_lock( &fWriteDataMutex );
	pwd->fConnectionsDone++;
	if ( pwd->fConnectionsDone == pwd->fConnections )
	    {
	    ReleaseBlocks( pwd->fBlockCnt, pwd->fBlocks );
	    fAllocCache.Release( (uint8*)pwd );
	    }
	pthread_mutex_unlock( &fWriteDataMutex );
	}
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
