/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TCPWriter.hpp"
#include "TCPListenThread.hpp"
#include "AliHLTLog.hpp"
#include <vector>
#include <errno.h>

TCPWriter::TCPWriter():
    fAllocCache( 6, true, false ),
    fSignal( 6 )
    {
    fListenThread = NULL;
    fTimeout = 0;
    }

unsigned TCPWriter::WriteToAllConnections( unsigned blockcnt, iovec* blocks )
    {
    if ( !fListenThread )
	{
	LOG( AliHLTLog::kError, "TCPWriter::WriteToAllConnections", "No Listen Thread" )
	    << "Listen thread not configured for TCP writer." << ENDLOG;
	return EFAULT;
	}
    std::vector<int> connections;
    fListenThread->GetConnectionSockets( connections );
    std::vector<int>::iterator iter, end;
    iter = connections.begin();
    end = connections.end();
    int ret;
    unsigned successes = 0;

    while ( iter != end )
	{
	ret = WriteToConnection( *iter, blockcnt, blocks );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "TCPWriter::WriteToAllConnections", "Error writing to connection" )
		<< "Error writing to connection (Socket " << AliHLTLog::kDec << *iter
		<< "). Closing connection." << ENDLOG;
	    fListenThread->CloseConnection( *iter );
	    }
	else
	    successes++;
	iter++;
	}
    return successes;
    }

int TCPWriter::WriteToConnection( int connSock, unsigned blockcnt, iovec* blocks )
    {
    unsigned nextBuffer = 0;
    uint32 szWritten = 0;
    uint32 toWrite[ blockcnt ];
    toWrite[0] = blocks[0].iov_len;
    for ( unsigned ic=1; ic<blockcnt; ic++ )
	{
	toWrite[ic] = toWrite[ic-1]+blocks[ic].iov_len;
	}
    uint32 totalToWrite = toWrite[blockcnt-1];
    bool retry=false;
    unsigned tempCount, bi, tempOffset;
    struct iovec ioVecs[blockcnt];
    bool first = true;
    int ret, retval = 0;

    while ( szWritten < totalToWrite )
	{
	retry = true;
	retval=0;
	do
	    {
	    tempCount=0;
	    bi = nextBuffer;
	    if ( bi>0 )
		tempOffset = szWritten-toWrite[bi-1];
	    else
		tempOffset = szWritten;
	    while ( bi<blockcnt )
		{
		LOG( MLUCLog::kDebug, "TCPWriter::WriteToConnection", "Buffer offsets" )
		    << "Block " << MLUCLog::kDec << (unsigned)bi << "(" << tempCount << ") is being written from offset "
		    << tempOffset 
		    << " - Length to be written: " << (unsigned)(blocks[bi].iov_len-tempOffset) << " of "
		    << (unsigned)(blocks[bi].iov_len) << " bytes." << ENDLOG;
		ioVecs[tempCount].iov_base = (void*)( ((uint8*)blocks[bi].iov_base)+tempOffset );
		ioVecs[tempCount].iov_len = blocks[bi].iov_len-tempOffset;
		tempCount++;
		bi++;
		tempOffset = 0;
		}
	    ret = writev( connSock, ioVecs, tempCount );
	    if ( ret > 0 )
		{
		first = true;
		szWritten += ret;
		if ( szWritten<totalToWrite )
		    {
		    while ( nextBuffer<blockcnt && toWrite[nextBuffer]<=szWritten )
			nextBuffer++;
		    }
		}
	    if ( ret==0 && !first )
		{
		retval = EPIPE;
		ret = -1;
		LOG( MLUCLog::kError, "TCPWriter::WriteToConnection", "Write error" )
		    << "Connection error on writev call: " << strerror(retval) << " (" 
		    << MLUCLog::kDec << retval << ")." << ENDLOG;
		break;
		}
	    if ( ret < 0 && errno!=EAGAIN && errno!=EINTR )
		{
		retval = errno;
		LOG( MLUCLog::kError, "TCPWriter::WriteToConnection", "Write error" )
		    << "Error on writev call: " << strerror(retval) << " (" 
		    << MLUCLog::kDec << retval << ")." << ENDLOG;
		break;
		}
	    if ( ret==0 || (ret < 0 && errno==EAGAIN) )
		retry = false;
	    }
	while ( retry && szWritten < totalToWrite );
	if ( szWritten >= totalToWrite )
	    break;
	if ( retval != 0 )
	    break;
	do
	    {
	    fd_set sockets;
	    struct timeval tv, *ptv;
	    FD_ZERO( &sockets );
	    FD_SET( connSock, &sockets );
	    if ( fTimeout )
		{
		tv.tv_sec = fTimeout/1000000;
		tv.tv_usec = fTimeout-tv.tv_sec*1000000;
		ptv = &tv;
		}
	    else
		ptv = NULL;
	    errno = 0;
	    ret = select( connSock+1, NULL, &sockets, NULL, ptv );
	    }
	while ( ret<=0 && (errno==EINTR || errno==EAGAIN) );
	if ( ret == 0 )
	    {
	    LOG( MLUCLog::kError, "TCPWriter::WriteToConnection", "Timeout expired" )
		<< "Timeout expired while trying to do transfer: " << strerror(errno) << " (" 
		    << MLUCLog::kDec << errno << ")." << ENDLOG;
	    retval = ETIMEDOUT;
	    break;
	    }
	if ( ret < 0 )
	    {
	    LOG( MLUCLog::kError, "TCPWriter::WriteToConnection", "Select error" )
		<< "Error on select while trying to do transfer: " << strerror(errno) << " (" 
		<< MLUCLog::kDec << errno << ")." << ENDLOG;
	    retval =  ENXIO;
	    break;
	    }
	first = false;
	}
    return retval;
    }

	





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
