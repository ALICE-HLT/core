#include "TCPListenThread.hpp"
#include <iostream>
#include <assert.h>
#include <iomanip>

// void printBlocks()
// {
//     for (int i = 0; i < blockCnt; i++) {
//         std::cout << blocks[i].fDataType.fDescr << " : " 
//             << blocks[i].fDataOrigin.fDescr << " : "
//             << hex << std::showbase << std::setw(8) << blocks[i].fDataSpecification << endl;
//         std::cout << dec << std::noshowbase;
//     }    
// }

int main()
{
    // Create tpc test blocks
    AliUInt32_t blockCnt = 2;
    AliHLTSubEventDescriptor::BlockData tpcBlocks[blockCnt];
    
    tpcBlocks[0].fDataType.fID = CLUSTERS_DATAID;
    tpcBlocks[0].fDataOrigin.fID = TPC_DATAORIGIN;
    tpcBlocks[0].fDataSpecification = 0x00000000;
    
    tpcBlocks[1].fDataType.fID = CLUSTERS_DATAID;
    tpcBlocks[1].fDataOrigin.fID = TPC_DATAORIGIN;
    tpcBlocks[1].fDataSpecification = 0x00000001;
    
    // Create and start tcpdump listener
    unsigned short fListenPort = 0;
    bool fAllowBlockingMode = false;
    TCPListenThread* listenThread = new TCPListenThread( fListenPort, fAllowBlockingMode );
    listenThread->Start();
    
    // 1. Adding two tpc cluster specifications that should be merged.
    listenThread->SetServiceType(tpcBlocks, blockCnt);
    assert(listenThread->ServiceChanged());                                     // The service info should have changed
    assert(listenThread->SpecMapSize() == 1);                                   // We expect one item in the map.
    
    vector <string> specKey1;
    specKey1.push_back("O=TPC ");
    specKey1.push_back("T=CLUSTERS");
    specKey1.push_back("0000");
    
    // 0000 is wrong, 0 is right. Will return 0xFFFFFFFF to indicate error
    assert(listenThread->GetSpecification(specKey1) == 0xFFFFFFFF);

    vector <string> specKey2;
    specKey2.push_back("O=TPC ");
    specKey2.push_back("T=CLUSTERS");
    specKey2.push_back("0");
    
    assert(listenThread->GetSpecification(specKey2) == 0x00000000);
    assert(listenThread->SpecMapSize() == 1);
    
    // 2. The specification should change to 0x00000100 still only one item in the map.
    tpcBlocks[1].fDataSpecification = 0x00000101;
    listenThread->SetServiceType(tpcBlocks, blockCnt);
    assert(listenThread->ServiceChanged());
    assert(listenThread->SpecMapSize() == 1);
    assert(listenThread->GetSpecification(specKey2) == 0x00000100);
    
    // 3. One spec set to 0x00010100. There should now be two elements in the map since specs are grouped by sector.
    tpcBlocks[1].fDataSpecification = 0x00010100;
    listenThread->SetServiceType(tpcBlocks, blockCnt);
    assert(listenThread->ServiceChanged());
    assert(listenThread->SpecMapSize() == 2);
    vector <string> specKey3;
    specKey3.push_back("O=TPC ");
    specKey3.push_back("T=CLUSTERS");
    specKey3.push_back("1");
    assert(listenThread->GetSpecification(specKey3) == 0x00010100);
    
    // Create trd test blocks
    AliHLTSubEventDescriptor::BlockData trdBlocks[blockCnt];
    
    trdBlocks[0].fDataType.fID = CLUSTERS_DATAID;
    trdBlocks[0].fDataOrigin.fID = TRD_DATAORIGIN;
    trdBlocks[0].fDataSpecification = 0x00000000;
    
    trdBlocks[1].fDataType.fID = CLUSTERS_DATAID;
    trdBlocks[1].fDataOrigin.fID = TRD_DATAORIGIN;
    trdBlocks[1].fDataSpecification = 0x00000001;
    
    listenThread->SetServiceType(trdBlocks, blockCnt);
    
    vector <string> specKeyTrd;
    specKeyTrd.push_back("O=TRD ");
    specKeyTrd.push_back("T=CLUSTERS");
    specKeyTrd.push_back("0");
    
    assert(listenThread->ServiceChanged());
    assert(listenThread->SpecMapSize() == 3);
    assert(listenThread->GetSpecification(specKeyTrd) == 0x00000001);
    
    trdBlocks[1].fDataSpecification = 0x00000006;
    
    listenThread->SetServiceType(trdBlocks, blockCnt);
    
    assert(listenThread->ServiceChanged());
    assert(listenThread->SpecMapSize() == 3);
    assert(listenThread->GetSpecification(specKeyTrd) == 0x00000007);
    
//     listenThread->PrintSpecMap();
    
    // Done. Stopping the thread.
    listenThread->Quit();
    
    return 0;
}
