#include "Avahi.hpp"
#include "time.h"
#include <assert.h>
#include <string>
#include <vector>
#include <iostream>

int main()
{
    AvahiThreaded avahi;
    avahi.Initialize("TCPDumpSubscriber");
    
    int port = 49152;
//     std::cout << "Using " << port << " as port" << std::endl;
    
    std::vector<std::string> specStrings0, specStrings1, specStrings2;
    
    assert(avahi.GetServiceCount() == 0);
    assert(avahi.NextFreeServiceId() == 0);
    
    assert(AVAHI_ID_ANY == -1);
    
    // Need to set the last argument, otherwise defaults to AVAHI_ID_ANY (-1) (SHOULD NOT BE TRUE!)
//     avahi.AddService("_tcpdumptest._tcp", port, AVAHI_ID_ANY);
    avahi.AddService("_tcpdumptest._tcp", port, 0);
    
    assert(avahi.GetServiceCount() == 1);
    assert(avahi.NextFreeServiceId() == 1);
    
    specStrings1.push_back("O=TPC  T=CLUSTERS S=256");
    specStrings1.push_back("O=TPC  T=CLUSTERS S=65792");
    
    avahi.ModifyService(0, "_tcpdumptest._tcp", port, specStrings1);
    
    assert(avahi.GetServiceCount() == 1);
    assert(avahi.NextFreeServiceId() == 1);
    assert(avahi.GetSpecString(0) != specStrings0);
    assert(avahi.GetSpecString(0) == specStrings1);
    
    specStrings2 = specStrings1;
    
    specStrings2.push_back("O=TPC  T=CLUSTERS S=0");
    specStrings2.push_back("O=TPC  T=CLUSTERS S=1");

    avahi.ModifyService(0, "_tcpdumptest._tcp", port, specStrings2);
    
    assert(avahi.GetServiceCount() == 1);
    assert(avahi.NextFreeServiceId() == 1);
    assert(avahi.GetSpecString(0) != specStrings1);
    assert(avahi.GetSpecString(0) == specStrings2);
    
    avahi.DeleteService(0);
    avahi.Release();
    
    assert(avahi.GetServiceCount() == 0);
    assert(avahi.NextFreeServiceId() == 0);
    
    return 0;
}
