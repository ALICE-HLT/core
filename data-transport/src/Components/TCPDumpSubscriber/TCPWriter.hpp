#ifndef _TCPWRITER_HPP_
#define _TCPWRITER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCThread.hpp"
#include "MLUCDynamicAllocCache.hpp"
#include <pthread.h>
#include <sys/uio.h>

class TCPListenThread;

class TCPWriter: public MLUCThread
    {
    public:

	TCPWriter();
	virtual ~TCPWriter();

	void Quit();
	bool Quitted();

	unsigned WriteToAllConnections( unsigned totalSize, unsigned blockcnt, iovec* blocks );
	
	void SetListenThread( TCPListenThread* listenThread )
		{
		fListenThread = listenThread;
		}

	void SetTimeout( unsigned long timeout_us )
		{
		fTimeout = timeout_us;
		}

    protected:

	void Run();

	int WriteToConnection( int connSock, bool binary, unsigned totalSize, unsigned blockcnt, iovec* blocks );

	TCPListenThread* fListenThread;

	unsigned long fTimeout; // In microsecs.

	MLUCDynamicAllocCache fAllocCache;
	MLUCConditionSem fSignal;

	struct TWriteData
	    {
		int fSocket;
		bool fBinary;
		unsigned fTotalSize;
		unsigned fBlockCnt;
		iovec* fBlocks;
	    };

	bool fQuit;
	bool fQuitted;
	
    private:
    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _TCPWRITER_HPP_
