#!/bin/bash

AVAHI_FLAG_ERROR=`pkg-config --cflags --libs avahi-client 2>&1 >/dev/null`
#AVAHI_FLAG_ERROR=`pkg-config --cflags --libs avahi-client 1>/dev/null` # <- don't we want to see errors like this

if [ -z "${AVAHI_FLAG_ERROR}" ]; then

  #AVAHI_FLAGS=`pkg-config --cflags --libs avahi-client 2>/dev/null` # <- either this is wrong
  AVAHI_FLAGS=`pkg-config --cflags 2>/dev/null`
    
  # or linking is missing here..
  FOUND_AVAHI=`echo '#include <avahi-client/client.h>
int main(){ AvahiClient* c=0; return 0; }'|g++ -x c++ -o tmp.o -c  ${AVAHI_FLAGS} ${INCLUDE} ${DEFINES} ${CCOPT} -w - 2>&1 ; if -e [ tmp.o ] ; then rm tmp.o ; fi >/dev/null 2>/dev/null`

  if [ -z "${FOUND_AVAHI}"  ] ; then
   echo -n -DUSE_AVAHI" "
  fi
fi
