/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTProcessingSubscriber.hpp"
//#include "AliHLTControlledProcessComponent.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTTimer.hpp"


class EventRateSubscriber: public AliHLTProcessingSubscriber
    {
    public:
	EventRateSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize,
			     AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, 
			     unsigned long intervalTime, 
			     int eventCountAlloc ):
	    AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc ),
	    fRateTimerCallback( this ),
	    fIntervalTime( intervalTime )
		{
		fCount = 0;
		fLastCount = 0;
		gettimeofday( &fGlobalStart, NULL );
		fIntervalStart = fGlobalStart;
// 		fTimer.AddTimeout( fIntervalTime, &fTimerCallback, 0 );
		}

    protected:

	void StartProcessing()
		{
		AliHLTProcessingSubscriber::StartProcessing();
		fTimer.AddTimeout( fIntervalTime, &fRateTimerCallback, 0 );
		}

	bool ProcessEvent( AliEventID_t, AliUInt32_t, AliHLTSubEventDescriptor::BlockData*, const AliHLTSubEventDataDescriptor*, const AliHLTEventTriggerStruct*,
			   AliUInt8_t*, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>&, AliHLTEventDoneData*& edd )
		{
		edd = NULL;
		size = 0;
		if ( !fCount )
		    gettimeofday( &fFirstEventTime, NULL );
		fCount++;
		return true;
		}

	void RateTimerExpired()
		{
		unsigned long long curCount = fCount;
		struct timeval now;
		double tdiff, gtdiff, fetdiff;
		double rate, grate, ferate;
		bool firstEvent = (fCount>0);
		    
		gettimeofday( &now, NULL );
		tdiff = now.tv_sec;
		tdiff -= fIntervalStart.tv_sec;
		tdiff *=1000000;
		tdiff += now.tv_usec;
		tdiff -= fIntervalStart.tv_usec;
		gtdiff = now.tv_sec;
		gtdiff -= fGlobalStart.tv_sec;
		gtdiff *=1000000;
		gtdiff += now.tv_usec;
		gtdiff -= fGlobalStart.tv_usec;
		if ( firstEvent )
		    {
		    fetdiff = now.tv_sec;
		    fetdiff -= fFirstEventTime.tv_sec;
		    fetdiff *=1000000;
		    fetdiff += now.tv_usec;
		    fetdiff -= fFirstEventTime.tv_usec;
		    }
		//gtdiff = (now.tv_sec-fGlobalStart.tv_sec)*1000000+(now.tv_usec-fGlobalStart.tv_usec);
		rate = (((double)(curCount-fLastCount))/tdiff)*1000000.0;
		grate = (((double)curCount)/gtdiff)*1000000.0;
		if ( firstEvent )
		    ferate = (((double)(curCount))/fetdiff)*1000000.0;
		LOG( AliHLTLog::kBenchmark, "EventRateSubscriber::TimerExpired", "Event Rate Results" )
		    << "Current Event Rate during last " << AliHLTLog::kDec << tdiff << " milliseconds ("
		    << curCount-fLastCount << " events): "
		    << rate << " Hz == " << (rate/1000.0) << " kHz == " << (rate/1000000.0) << " MHz."
		    << ENDLOG;
		LOG( AliHLTLog::kBenchmark, "EventRateSubscriber::TimerExpired", "Event Rate Results" )
		    << "Total Global Event Rate after " << AliHLTLog::kDec << curCount << " events: "
		    << grate << " Hz == " << (grate/1000.0) << " kHz == " << (grate/1000000.0) << " MHz."
		    << ENDLOG;
		if ( firstEvent )
		    {
		    LOG( AliHLTLog::kBenchmark, "EventRateSubscriber::TimerExpired", "Event Rate Results" )
			<< "Global Event Rate since first event after " << AliHLTLog::kDec << curCount << " events: "
			<< ferate << " Hz == " << (ferate/1000.0) << " kHz == " << (ferate/1000000.0) << " MHz."
			<< ENDLOG;
		    }
		fIntervalStart = now;
		fLastCount = curCount;
		fTimer.AddTimeout( fIntervalTime, &fRateTimerCallback, 0 );
		}
	class RateTimerCallback;
	    friend class RateTimerCallback;
	class RateTimerCallback: public AliHLTTimerCallback
	    {
	    public:
		RateTimerCallback( EventRateSubscriber* parent ):
		    fParent( parent )
			{
			}

		virtual void TimerExpired( AliUInt64_t, unsigned long )
			{
			fParent->RateTimerExpired();
			}

	    protected:
		EventRateSubscriber* fParent;
	    };

	RateTimerCallback fRateTimerCallback;
	    
	unsigned long fLastCount;
	unsigned long fCount;

	struct timeval fGlobalStart;
	struct timeval fIntervalStart;
	struct timeval fFirstEventTime;
	
	unsigned long fIntervalTime; // millisec.

    };


class EventRateComponent: public AliHLTControlledSinkComponent
    {
    public:

	EventRateComponent( const char* name, int argc, char** argv );
	virtual ~EventRateComponent() {};


    protected:

	//virtual AliHLTProcessingSubscriber* CreateSubscriber();
	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

	virtual AliHLTProcessingSubscriber* CreateSubscriber();

	unsigned long fIntervalTime; // millisec.

    private:
    };


EventRateComponent::EventRateComponent( const char* name, int argc, char** argv ):
    AliHLTControlledSinkComponent( name, argc, argv )
    {
    fIntervalTime = 1000;
    }


// AliHLTProcessingSubscriber* EventRateComponent::CreateSubscriber()
//     {
//     return new AliHLTEventRateSubscriber( fSubscriberName.c_str(), fSendEventDone, fMinBlockSize, fLoadType, 
// 					 fProcTimeFunc, fProcTimeFuncParam, fDataOutputRate, fMaxPreEventCountExp2 );
//     }

const char* EventRateComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cpErr = NULL;
    if ( !strcmp( argv[i], "-interval" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing interval time specifier.";
	    }
	fIntervalTime = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting interval time specifier..";
	    }
	i += 2;
	return NULL;
	}
    return AliHLTControlledSinkComponent::ParseCmdLine( argc, argv, i, errorArg );
    }

void EventRateComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    AliHLTControlledSinkComponent::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-interval <interval-time-ms>: Specify the update time for the event rate (in milliseconds). (Optional, default 1s)" << ENDLOG;
    }

AliHLTProcessingSubscriber* EventRateComponent::CreateSubscriber()
    {
    return new EventRateSubscriber( fSubscriberID.c_str(), fSendEventDone, fMinBlockSize, 
				    fPerEventFixedSize, fPerBlockFixedSize, fSizeFactor, 
				    fIntervalTime, fMaxPreEventCountExp2 );
    }


int main( int argc, char** argv )
    {
    EventRateComponent procComp( "EventRateComponent", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

