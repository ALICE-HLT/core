/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTControlledComponent.hpp"
#include "AliHLTProcessingSubscriber.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

class ASCIIDataWriter: public AliHLTProcessingSubscriber
    {
    public:

	ASCIIDataWriter( const char* name, 
			 const char* filenamePrefix, 
			 bool simpleFormat, 
			 bool sendEventDone, AliUInt32_t minBlockSize,
			 AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, 
			 int eventCountAlloc = -1 );
  
	virtual ~ASCIIDataWriter();

    protected:

	virtual bool ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* etsp,
				   AliUInt8_t* outputPtr, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, AliHLTEventDoneData*& edd );


	int WriteData( int fh, void* data, unsigned long size );
	int AddBuffer( char* buffer, unsigned long bufferLen, unsigned long& bufferOffset,
		       const char* tmpBuffer, bool flush = false );
	
	MLUCString fFilenamePrefix;

	unsigned long fFileIndex;

	unsigned long fFileLength;
	unsigned long fMaxFileLength;

	int fFileHandle;
	MLUCString fFilename;
	bool fSimpleFormat;

    private:
    };




ASCIIDataWriter::ASCIIDataWriter( const char* name, 
				  const char* filenamePrefix, 
				  bool simpleFormat, 
				  bool sendEventDone, AliUInt32_t minBlockSize,
				  AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, 
				  int eventCountAlloc ):
    AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc )
    {
    fFilenamePrefix = filenamePrefix;
    fFileLength = 0;
    fMaxFileLength = 1 << 30; // max file size 1 GB
    fFileHandle = -1;
    fFileIndex = 0;
    fSimpleFormat = simpleFormat;
    }

ASCIIDataWriter::~ASCIIDataWriter()
    {
    if ( fFileHandle != -1 )
	close( fFileHandle );
    }

#define COLUMNWIDTH 8
#define BUFFERLEN 16384

bool ASCIIDataWriter::ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, const AliHLTSubEventDataDescriptor*, const AliHLTEventTriggerStruct*,
					       AliUInt8_t*, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>&, AliHLTEventDoneData*& )
    {
    size = 0;
    unsigned long i;
    if ( blockCnt<=0 )
	return true;

    char buffer[ BUFFERLEN+1 ];
    unsigned long bufferOffset = 0;
    if ( fSimpleFormat )
	{
	fFilename = fFilenamePrefix;
	fFilename += "-";
	
	char tmp[ 257 ];
	sprintf( tmp, "0x%016LX", (unsigned long long)eventID.fNr );
	fFilename += tmp;
	
	fFilename += ".raw";
	
	fFileHandle = open( fFilename.c_str(), O_WRONLY|O_CREAT|O_TRUNC, 0666 );
	if ( fFileHandle == -1 )
	    {
	    LOG( AliHLTLog::kError, "ASCIIDataWriter::ProcessEvent", "Cannot Open File" )
		<< "Unable to open file '" << fFilename.c_str() << "': "
		<< strerror( errno ) << " (" << AliHLTLog::kDec << errno
		<< ")." << ENDLOG;
	    return true;
	    }

	char tmpBuffer[1024];
	unsigned long n, blockSize, wordCount;
	AliUInt32_t* blockData;
	MLUCString output;

	time_t mytime;
	time( &mytime );
	sprintf( buffer, "# %s %s\n\n# Event %Lu\n\n", 
		 fFilename.c_str(), ctime( &mytime ),
		 (unsigned long long)eventID.fNr );
	bufferOffset = strlen(buffer );
	
#if 0
	if ( WriteData( fFileHandle, buffer, bufferOffset ) )
	    {
	    LOG( AliHLTLog::kError, "ASCIIDataWriter::ProcessEvent", "Error Writing to File" )
		<< "Error writing to file '" << fFilename.c_str() << "': "
		<< strerror( errno ) << " (" << AliHLTLog::kDec << errno
		<< ")." << ENDLOG;
	    close( fFileHandle  );
	    fFileHandle = -1;
	    return true;
	    }
#endif
	for ( i = 0; i < blockCnt; i++ )
	    {
	    blockData = (AliUInt32_t*)(blocks[i].fData);
	    blockSize = blocks[i].fSize;
	    wordCount = blockSize / 4;
	    sprintf( tmpBuffer, "0x%08lx\n\n", (unsigned long)wordCount );
	    if ( AddBuffer( buffer, BUFFERLEN, bufferOffset, tmpBuffer ) )
		return true;
	    for ( n = 0; n < wordCount; n++ )
		{
		sprintf( tmpBuffer, "0x%08lx\n", (unsigned long)blockData[n] );
		if ( AddBuffer( buffer, BUFFERLEN, bufferOffset, tmpBuffer ) )
		    return true;
		}
	    }
	if ( AddBuffer( buffer, BUFFERLEN, bufferOffset, "", true ) )
	    return true;
	close( fFileHandle  );
	fFileHandle = -1;
	fFileLength = 0;
	}
    else
	{

	if ( fFileHandle == -1 )
	    {
	    fFilename = fFilenamePrefix;
	    fFilename += "-";

	    char tmp[ 255 ];
	    sprintf( tmp, "0x%08lX", fFileIndex );
	    fFilename += tmp;
	    fFileIndex++;
	
	    fFilename += ".text";
	
	    fFileHandle = open( fFilename.c_str(), O_WRONLY|O_CREAT|O_TRUNC, 0666 );
	    if ( fFileHandle == -1 )
		{
		LOG( AliHLTLog::kError, "ASCIIDataWriter::ProcessEvent", "Cannot Open File" )
		    << "Unable to open file '" << fFilename.c_str() << "': "
		    << strerror( errno ) << " (" << AliHLTLog::kDec << errno
		    << ")." << ENDLOG;
		return true;
		}
	    }

	char tmpBuffer[ 1024 ];
	unsigned long long count = 0;
	unsigned long n, blockSize;
	AliUInt8_t* blockData;
	MLUCString output, output2, output3;
	const char* eventType = "Unknown";
	switch ( eventID.fType )
	    {
	    case kAliEventTypeStartOfRun: 
		eventType = "Start-Of-Run";
		break;
	    case kAliEventTypeData:
		eventType = "Data";
		break;
	    case kAliEventTypeEndOfRun:
		eventType = "End-Of-Run";
		break;
	    case kAliEventTypeSync:
		eventType = "SYNC";
		break;
	    }
	sprintf( buffer, "Event Type: %s - Nr: 0x%016LX /%Lu)\n", eventType, (unsigned long long)eventID.fNr, (unsigned long long)eventID.fNr );
	bufferOffset = strlen( buffer );
	count = bufferOffset;
	for ( i = 0; i < blockCnt; i++ )
	    {
	    blockData = blocks[i].fData;
	    blockSize = blocks[i].fSize;
	    output = "";
	    sprintf( tmpBuffer, "  Block 0x%08lX / %lu\n", i, i );
	    if ( AddBuffer( buffer, BUFFERLEN, bufferOffset, tmpBuffer ) )
		return true;
	    count += strlen( tmpBuffer );
	    sprintf( tmpBuffer, "  Block size: %lu ( 0x%08lX )\n", blockSize, blockSize );
	    if ( AddBuffer( buffer, BUFFERLEN, bufferOffset, tmpBuffer ) )
		return true;
	    count += strlen( tmpBuffer );
	    char tmpBuffer1[ 4096 ];
	    char tmpBuffer2[ 4096 ];
	    char tmpBuffer3[ 4096 ];
	    tmpBuffer1[0] = 0;
	    tmpBuffer2[0] = 0;
	    tmpBuffer3[0] = 0;
	    for ( n = 0; n < blockSize; n++ )
		{
		if ( !(n % COLUMNWIDTH) )
		    {
		    sprintf( tmpBuffer, "    0x%08lX /     %010lu      ", n, n );
		    if ( AddBuffer( buffer, BUFFERLEN, bufferOffset, tmpBuffer ) )
			return true;
		    count += strlen( tmpBuffer );
		    }
		sprintf( tmpBuffer, "%02lX", (unsigned long)( blockData[n] ) );
		strcat( tmpBuffer1, tmpBuffer );
		sprintf( tmpBuffer, "%03lu", (unsigned long)( blockData[n] ) );
		strcat( tmpBuffer2, tmpBuffer );
		sprintf( tmpBuffer, "%c", ( (blockData[n] < 32) ? '.' : blockData[n] ) );
		strcat( tmpBuffer3, tmpBuffer );
		if ( (n % COLUMNWIDTH)==(COLUMNWIDTH-1) || n==blockSize-1 )
		    {
		    if ( AddBuffer( buffer, BUFFERLEN, bufferOffset, tmpBuffer1 ) )
			return true;
		    count += strlen( tmpBuffer1 );
		    const char* tmpChar = "      ";
		    if ( AddBuffer( buffer, BUFFERLEN, bufferOffset, tmpChar ) )
			return true;
		    count += strlen( tmpChar );
		    if ( AddBuffer( buffer, BUFFERLEN, bufferOffset, tmpBuffer2 ) )
			return true;
		    count += strlen( tmpBuffer2 );
		    if ( AddBuffer( buffer, BUFFERLEN, bufferOffset, tmpChar ) )
			return true;
		    count += strlen( tmpChar );
		    if ( AddBuffer( buffer, BUFFERLEN, bufferOffset, tmpBuffer3 ) )
			return true;
		    count += strlen( tmpBuffer3 );
		    tmpChar = "\n";
		    if ( AddBuffer( buffer, BUFFERLEN, bufferOffset, "\n" ) )
			return true;
		    count += strlen( tmpChar );
		    if ( n == blockSize-1 && AddBuffer( buffer, BUFFERLEN, bufferOffset, tmpChar ) )
			return true;
		    count += strlen( tmpChar );
		    tmpBuffer1[0] = 0;
		    tmpBuffer2[0] = 0;
		    tmpBuffer3[0] = 0;
		    }
		else if ( (n % COLUMNWIDTH)==(COLUMNWIDTH/2-1) )
		    {
		    strcat( tmpBuffer1, "  " );
		    strcat( tmpBuffer2, "  " );
		    strcat( tmpBuffer3, "  " );
		    }
		else
		    {
		    strcat( tmpBuffer1, " " );
		    strcat( tmpBuffer2, " " );
		    }
		}
	    if ( AddBuffer( buffer, BUFFERLEN, bufferOffset, "\n" ) )
		return true;
	    count += strlen( "\n" );
	    }
	if ( AddBuffer( buffer, BUFFERLEN, bufferOffset, "", true ) )
	    return true;

	fFileLength += count;
	if ( fFileLength >= fMaxFileLength )
	    {
	    close( fFileHandle );
	    fFileHandle = -1;
	    fFileLength = 0;
	    }
	}
    return true;
    }


int ASCIIDataWriter::WriteData( int fh, void* data, unsigned long size )
    {
    unsigned long bytesWritten = 0;
    while ( bytesWritten < size )
	{
	int ret = write( fh, ((AliUInt8_t*)data)+bytesWritten, size-bytesWritten );
	if ( ret < 0 )
	    return errno;
	bytesWritten += ret;
	}
    return 0;
    }

int ASCIIDataWriter::AddBuffer( char* buffer, unsigned long bufferLen, unsigned long& bufferOffset,
				const char* tmpBuffer, bool flush )
    {
    int ret;
    if ( strlen(tmpBuffer)+bufferOffset>bufferLen || flush )
	{
	ret = WriteData( fFileHandle, buffer, bufferOffset );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "ASCIIDataWriter::AddBuffer", "Error Writing to File" )
		<< "Error writing to file '" << fFilename.c_str() << "': "
		<< strerror( errno ) << " (" << AliHLTLog::kDec << errno
		<< ")." << ENDLOG;
	    close( fFileHandle  );
	    fFileHandle = -1;
	    return ret;
	    }
	strcpy( buffer, tmpBuffer );
	bufferOffset = strlen(buffer );
	}
    else
	{
	strcat( buffer, tmpBuffer );
	bufferOffset += strlen(tmpBuffer);
	}
    return 0;
    }


class ASCIIDataWriterComponent: public AliHLTControlledSinkComponent
    {
    public:

	ASCIIDataWriterComponent( const char* name, int argc, char** argv );
	virtual ~ASCIIDataWriterComponent() {};


    protected:

	virtual void ShowConfiguration();

	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

	virtual ASCIIDataWriter* CreateSubscriber();

	MLUCString fFilenamePrefix;
	bool fSimpleFormat;

	typedef AliHLTControlledSinkComponent TSuper;

    private:
    };


ASCIIDataWriterComponent::ASCIIDataWriterComponent( const char* name, int argc, char** argv ):
    TSuper( name, argc, argv )
    {
    fFilenamePrefix = "EventData";
    fSimpleFormat = false;
    }

void ASCIIDataWriterComponent::ShowConfiguration()
    {
    TSuper::ShowConfiguration();
    LOG( AliHLTLog::kInformational, "ASCIIDataWriterComponent::ShowConfiguration", "Filename prefix" )
	<< "Output file name prefix: '" << fFilenamePrefix.c_str() << "'." << ENDLOG;
    if ( fSimpleFormat )
	{
	LOG( AliHLTLog::kInformational, "ASCIIDataWriterComponent::ShowConfiguration", "Simple Data Format" )
	    << "Using simple data format." << ENDLOG;
	}
    }

const char* ASCIIDataWriterComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    if ( !strcmp( argv[i], "-filenameprefix" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing output file name prefix specifier.";
	    }
	fFilenamePrefix = argv[i+1];
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-simpleformat" ) )
	{
	fSimpleFormat = true;
	i++;
	return NULL;
	}
    return TSuper::ParseCmdLine( argc, argv, i, errorArg );
    }

void ASCIIDataWriterComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    TSuper::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-filenameprefix: Specify the prefix to be used for output file names. (Optional)" 
	<< ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-simpleformat: Specify to use a simple output format. (Optional)" 
	<< ENDLOG;
    }


ASCIIDataWriter* ASCIIDataWriterComponent::CreateSubscriber()
    {
    return new ASCIIDataWriter( fSubscriberID.c_str(), 
				fFilenamePrefix.c_str(),
				fSimpleFormat, 
				fSendEventDone, fMinBlockSize, 
				fPerEventFixedSize, fPerBlockFixedSize, fSizeFactor, 
				fMaxPreEventCountExp2 );
    }



int main( int argc, char** argv )
    {
    ASCIIDataWriterComponent procComp( "ASCIIDataWriter", argc, argv );
    procComp.Run();
    return 0;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
