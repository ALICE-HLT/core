/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTFilePublisher.hpp"
#include "AliHLTFilePublisherMemory.hpp"
#include "AliHLTFilePublisherDisk.hpp"
//#include "AliHLTControlledProcessComponent.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTLog.hpp"
#include <vector>
#include <glob.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>


class FilePublisherComponent: public AliHLTControlledSourceComponent
    {
    public:

	FilePublisherComponent( const char* name, int argc, char** argv );
	virtual ~FilePublisherComponent() {};


    protected:

	virtual AliHLTDetectorPublisher* CreatePublisher();
	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

	virtual void ResetConfiguration();
	virtual bool CheckConfiguration();
	virtual void ShowConfiguration();

	virtual bool SetupComponents();
	virtual void DestroyParts();

	vector<MLUCString> fFilenames;
	vector<bool> fNewFileFormat;
	unsigned long fEventTime;
	unsigned long fEventNrOffset;

	bool fEventTimeJustSleep;

	bool fOldFileParamUsed;

	bool fUseDiskPublisher;

	bool fEventIDFromFile;

	bool fEventTypeFromFile;

	bool fNoSOREOREvents;

    private:
    };


FilePublisherComponent::FilePublisherComponent( const char* name, int argc, char** argv ):
    AliHLTControlledSourceComponent( name, argc, argv )
    {
    fAllowDataTypeOriginSpecSet = true;
    fEventTime = 0; // By default publish as fast as possible
    fEventNrOffset = 0; // By default start at the beginning
    fEventTimeJustSleep = false;
    fOldFileParamUsed = false;
    fUseDiskPublisher = false;
    fEventIDFromFile = false;
    fEventTypeFromFile = false;
    fNoSOREOREvents = false;
    fAdaptBlockFreeUpdate = false;
    fAllowIgnoreEventTriggerCommands = true;
    }


AliHLTDetectorPublisher* FilePublisherComponent::CreatePublisher()
    {
    if ( fUseDiskPublisher )
	return new AliHLTFilePublisherDisk( fOwnPublisherName.c_str(), fEventTime,
					    fEventNrOffset, fFilenames, fNewFileFormat, fEventIDFromFile, fEventTypeFromFile, fMaxPreEventCountExp2 );
    else
	return new AliHLTFilePublisherMemory( fOwnPublisherName.c_str(), fEventTime,
					      fEventNrOffset, fFilenames, fNewFileFormat, fEventIDFromFile, fEventTypeFromFile, fMaxPreEventCountExp2 );
    }


const char* FilePublisherComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cerr = NULL;
    if ( !strcmp( argv[i], "-file" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing filename specifier.";
	    }
	MLUCString tmp( argv[i+1] );
	fFilenames.insert( fFilenames.end(), tmp );
	fNewFileFormat.insert( fNewFileFormat.end(), false );
	fOldFileParamUsed = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-datafile" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing filename specifier.";
	    }
	MLUCString tmp( argv[i+1] );
	fFilenames.insert( fFilenames.end(), tmp );
	fNewFileFormat.insert( fNewFileFormat.end(), true );
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-datafilelist" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing filename list specifier.";
	    }
	glob_t globDat;
	globDat.gl_offs = 0;
	globDat.gl_pathc = 0;
	globDat.gl_pathv = NULL;
	int ret = glob( argv[i+1], GLOB_MARK, NULL, &globDat );
	if ( ret )
	    {
	    errorArg = i+1;
	    return "Error expanding filename list";
	    }
	for ( unsigned n=0; n<globDat.gl_pathc; n++ )
	    {
	    MLUCString tmp( globDat.gl_pathv[n] );
	    fFilenames.insert( fFilenames.end(), tmp );
	    fNewFileFormat.insert( fNewFileFormat.end(), true );
	    }
	globfree( &globDat );
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-olddatafile" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing filename specifier.";
	    }
	MLUCString tmp( argv[i+1] );
	fFilenames.insert( fFilenames.end(), tmp );
	fNewFileFormat.insert( fNewFileFormat.end(), false );
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-olddatafilelist" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing old filename list specifier.";
	    }
	glob_t globDat;
	globDat.gl_offs = 0;
	globDat.gl_pathc = 0;
	globDat.gl_pathv = NULL;
	int ret = glob( argv[i+1], GLOB_MARK, NULL, &globDat );
	if ( ret )
	    {
	    errorArg = i+1;
	    return "Error expanding filename list";
	    }
	for ( unsigned n=0; n<globDat.gl_pathc; n++ )
	    {
	    MLUCString tmp( globDat.gl_pathv[n] );
	    fFilenames.insert( fFilenames.end(), tmp );
	    fNewFileFormat.insert( fNewFileFormat.end(), false );
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventtime" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing event interval time specifier.";
	    }
	fEventTime = strtoul( argv[i+1], &cerr, 0 );
	if ( *cerr )
	    {
	    return "Error converting event interval time specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventnr" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing event nr offset specifier.";
	    }
	fEventNrOffset = strtoul( argv[i+1], &cerr, 0 );
	if ( *cerr )
	    {
	    return "Error converting event nr offset specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-sleeptillevent" ) )
	{
	fEventTimeJustSleep = true;
	i += 1;
	return NULL;
	}
    if ( !strcmp( argv[i], "-diskpublisher" ) )
	{
	fUseDiskPublisher = true;
	i += 1;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventidfromfile" ) )
	{
	fEventIDFromFile = true;
	i += 1;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventtypefromfile" ) )
	{
	fEventTypeFromFile = true;
	i += 1;
	return NULL;
	}
    if ( !strcmp( argv[i], "-nosoreorevents" ) )
	{
	fNoSOREOREvents = true;
	i += 1;
	return NULL;
	}
//     if ( !strcmp( argv[i], "-slotslog2" ) )
// 	{
// 	if ( argc < i +1 )
// 	    {
// 	    return "Missing slot-count specifier.";
// 	    }
// 	fPreEvtPower2 = strtol( argv[i+1], &cerr, 0 );
// 	    if ( *cerr )
// 		{
// 		errorStr = "Error converting slot-count specifier..";
// 		break;
// 		}
// 	    preEvtCnt = 1 << preEvtPower2;
// 	    i += 2;
// 	    continue;
// 	    }
    return AliHLTControlledSourceComponent::ParseCmdLine( argc, argv, i, errorArg );
    }

void FilePublisherComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    AliHLTControlledSourceComponent::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-datafile <filename>: Specify the name of a file that contains data to publish (uses new format, no special file conventions). (Mandatory optional, can be used multiple times)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-datafilelist <filename-pattern>: Specify a wildcard pattern of files that contain data to publish (uses new format, no special file conventions). (Mandatory optional, can be used multiple times)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-olddatafile <filename>: Specify the name of a file that contains data to publish (uses old format with initial size word contained in file). (Mandatory optional, can be used multiple times)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-olddatafilelist <filename-pattern>: Specify a wildcard pattern of files that contain data to publish (uses old format with initial size word contained in file). (Mandatory optional, can be used multiple times)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-file <filename>: Specify the name of a file that contains data to publish (uses old format with initial size word contained in file). (Deprecated) (Mandatory optional, can be used multiple times)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "      At least one of -datafile, -olddatafile, or -file (deprecated) is mandatory." << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-eventtime <interval-microsec.>: Specify the time between the publishing of two events (in microseconds). (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-eventnr <event-nr-offset.>: Specify an offset to the starting event number/ID. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-diskpublisher: Do not read all data at once into memory, but instead read each event when it is needed (even multiple times). (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-eventidfromfile: Read in the event IDs from the files (assumes DDL headers). (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-eventtypefromfile: Read in the event type (data, start/end-of-run) from the files (assumes DDL headers). (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-nosoreorevents: Inhibit the creation of start-of-run/end-of-run broadcast events. (Optional)" << ENDLOG;
    }

void FilePublisherComponent::ResetConfiguration()
    {
    AliHLTControlledSourceComponent::ResetConfiguration();
    fAllowIgnoreEventTriggerCommands = true;
    }


bool FilePublisherComponent::CheckConfiguration()
    {
    if ( !AliHLTControlledComponent::CheckConfiguration() )
	return false;
    if ( fFilenames.size()<=0 )
	{
	LOG( AliHLTLog::kError, "FilePublisherComponent::CheckConfiguration", "No file specified" )
	    << "Must specify at least one file to publish (e.g. using the -file command line option."
	    << ENDLOG;
	return false;
	}
    if ( fOldFileParamUsed )
	{
	LOG( AliHLTLog::kWarning, "FilePublisherComponent::CheckConfiguration", "Old file option" )
	    << "-file parameter is deprecated and may be removed in the future. Please use -datafile or -olddatafile." 
	    << ENDLOG;
	}
    vector<MLUCString>::iterator filenameIter, filenameEnd;
    vector<bool>::iterator fileFormatIter, fileFormatEnd;
    filenameIter = fFilenames.begin();
    filenameEnd = fFilenames.end();
    fileFormatIter = fNewFileFormat.begin();
    fileFormatEnd = fNewFileFormat.end();
    while ( filenameIter!=filenameEnd )
	{
	int fd;
	unsigned long size = 0;
	fd = open( filenameIter->c_str(), O_RDONLY );
	if ( fd == -1 )
	    {
	    LOG( AliHLTLog::kError, "FilePublisherComponent::CheckConfiguration", "File open" )
		<< "Unable to open file '" << filenameIter->c_str() << "': " << strerror(errno)
		<< " (" << AliHLTLog::kDec << errno << ")." << ENDLOG;
	    return false;
	    }
	if ( *fileFormatIter )
	    {
	    // New format
	    // All file contents are read in.
	    off_t insize;
	    insize = lseek( fd, 0, SEEK_END );
	    if ( insize==(off_t)-1 )
		{
		LOG( AliHLTLog::kError, "FilePublisherComponent::CheckConfiguration", "File size determination" )
		    << "Unable to determine size of new format file '" << filenameIter->c_str() << "': " << strerror(errno)
		    << " (" << AliHLTLog::kDec << errno << ")." << ENDLOG;
		close(fd);
		return false;
		}
	    lseek( fd, 0, SEEK_SET );
	    size = insize;
	    }
	else
	    {
	    // Old format
	    // Initial 32 bit word holds number of following 32 bit words in file
	    int ret=0, readBytes=0;
	    unsigned long count=0;
	    do
		{
		ret = read( fd, &count, 4 );
		if ( ret>0 )
		    readBytes += 4;
		if ( ret<=0 && errno!=EAGAIN && errno!=EINTR )
		    break;
		}
	    while ( readBytes<4 );
	    if ( readBytes>=4 )
		size = count*sizeof(AliUInt32_t);
	    else
		{
		LOG( AliHLTLog::kError, "FilePublisherComponent::CheckConfiguration", "File size determination" )
		    << "Unable to determine size of old format file '" << filenameIter->c_str() << "': " << strerror(errno)
		    << " (" << AliHLTLog::kDec << errno << ")." << ENDLOG;
		close(fd);
		return false;
		}
	    }
	close(fd);
	if ( size>fBufferSize )
	    {
	    LOG( AliHLTLog::kError, "FilePublisherComponent::CheckConfiguration", "File too big for buffer" )
		<< "Size of file '" << filenameIter->c_str() << "' ("
		<< AliHLTLog::kDec << size << " bytes) is bigger than given buffer size of "
		<< fBufferSize << " bytes."<< ENDLOG;
	    return false;
	    }
	filenameIter++;
	     fileFormatIter++;
	}
    if ( fEventTypeFromFile && !fNoSOREOREvents )
	{
	LOG( AliHLTLog::kWarning, "FilePublisherComponent::CheckConfiguration", "Reading event type from file and generation of S/E-O-R/D events not suggested" )
	    << "Reading event type from file (-eventtypefromfile) in combination with automatic generation of Start/End-Of-Run/Data events (suppressed by -nosoreorevents) is not suggested."
	    << ENDLOG;
	}
    return true;
    }

void FilePublisherComponent::ShowConfiguration()
    {
    AliHLTControlledComponent::ShowConfiguration();
    LOG( AliHLTLog::kInformational, "FilePublisherComponent::ShowConfiguration", "Event Time Configuration" )
	<< "Event time: " << AliHLTLog::kDec << fEventTime << " (0x" << AliHLTLog::kHex
	<< fEventTime << ")." << ENDLOG;
    if ( fUseDiskPublisher )
	{
	LOG( AliHLTLog::kInformational, "FilePublisherComponent::ShowConfiguration", "Disk Publisher" )
	    << "Using disk publisher" << ENDLOG;
	}
    if ( fEventIDFromFile )
	{
	LOG( AliHLTLog::kInformational, "FilePublisherComponent::ShowConfiguration", "Event ID from File" )
	    << "Reading event IDs from data files." << ENDLOG;
	}
    if ( fEventTypeFromFile )
	{
	LOG( AliHLTLog::kInformational, "FilePublisherComponent::ShowConfiguration", "Event Type from File" )
	    << "Reading event Types from data files." << ENDLOG;
	}
    vector<MLUCString>::iterator iter, end;
    iter = fFilenames.begin();
    end = fFilenames.end();
    vector<bool>::iterator biter, bend;
    biter = fNewFileFormat.begin();
    bend = fNewFileFormat.end();
    while ( iter != end )
	{
	const char* format = "unknown";
	if ( biter != bend )
	    format = (*biter) ? "new" : "old";
	LOG( AliHLTLog::kInformational, "FilePublisherComponent::ShowConfiguration", "Reading in file" )
	    << "Using " << format << " format file '" << iter->c_str() << "'" << ENDLOG;
	if ( biter != bend )
	    biter++;
	iter++;
	}
    }

bool FilePublisherComponent::SetupComponents()
    {
    if ( !AliHLTControlledComponent::SetupComponents() )
	return false;
    if ( ((AliHLTFilePublisher*)fPublisher)->GetLoadedFileCount()<=0 )
	{
	LOG( AliHLTLog::kError, "FilePublisherComponent::SetupComponents", "No file loaded" )
	    << "No file could be loaded successfully (with size>0)"
	    << ENDLOG;
	return false;
	}
    ((AliHLTFilePublisher*)fPublisher)->SetEventDataType( fEventDataType );
    ((AliHLTFilePublisher*)fPublisher)->SetEventDataOrigin( fEventDataOrigin );
    ((AliHLTFilePublisher*)fPublisher)->SetEventDataSpecification( fEventDataSpecification );
    if ( fEventTimeJustSleep )
	((AliHLTFilePublisher*)fPublisher)->EventTimeJustSleep();
    if ( fNoSOREOREvents )
	((AliHLTFilePublisher*)fPublisher)->NoSOREOREvents();
    return true;
    }

void FilePublisherComponent::DestroyParts()
    {
#ifdef PROFILING
    if ( fPublisher )
	((AliHLTFilePublisher*)fPublisher)->WriteHistograms( fComponentName );
#endif
    AliHLTControlledComponent::DestroyParts();
    }



int main( int argc, char** argv )
    {
    FilePublisherComponent procComp( "FilePublisher", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
