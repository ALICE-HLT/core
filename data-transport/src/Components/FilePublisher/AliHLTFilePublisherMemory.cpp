/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTFilePublisherMemory.hpp"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTEventDataType.h"
#include "AliHLTLog.hpp"
#include "AliHLTGetNodeID.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTBufferManager.hpp"
#include "AliHLTShmManager.hpp"
#include "AliHLTStackTrace.hpp"
#include "AliHLTSubEventDescriptor.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "AliHLTDDLHeader.hpp"
#include "NOPEEvents.hpp"
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>



#ifdef PROFILING
#define STDHISTO(histo) histo(0,10000,1)
#define ADDHISTO(histos,name) histos.insert( histos.end(), &name )
#else
#define STDHISTO(histo)
#define ADDHISTO(histos,name)
#endif


AliHLTFilePublisherMemory::AliHLTFilePublisherMemory( const char* name, AliUInt32_t time_betw_evts_usec,
						      AliUInt32_t eventNrOffset, vector<MLUCString> filenames,
						      vector<bool> newFileFormat, bool eventIDFromFile, bool eventTypeFromFile, 
						      int eventSlotsPowerOfTwo ):
    AliHLTFilePublisher( name, time_betw_evts_usec,
			 eventNrOffset, filenames,
			 newFileFormat, eventIDFromFile, eventTypeFromFile, 
			 eventSlotsPowerOfTwo )
    {
    vector<MLUCString>::iterator fileIter, fileEnd;
    vector<bool>::iterator formatIter, formatEnd;
    fileIter = filenames.begin();
    fileEnd = filenames.end();
    formatIter = newFileFormat.begin();
    formatEnd = newFileFormat.end();
    AliUInt8_t* data;
    AliUInt32_t size;
    AliUInt32_t offset = 0;
    MLUCString filename;
    while ( fileIter != fileEnd )
	{
	data = NULL;
	int fd;
	AliUInt32_t count;
	bool success = true;
	fd = open( fileIter->c_str(), O_RDONLY );
	if ( fd == -1 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTFilePublisherMemory Constructor", "File open" )
		<< "Unable to open file '" << fileIter->c_str() << "': " << strerror(errno)
		<< " (" << AliHLTLog::kDec << errno << ")." << ENDLOG;
	    success = false;
	    }

	if ( formatIter!=formatEnd && *(formatIter) )
	    {
	    // New format
	    // All file contents are read in.
	    off_t insize;
	    insize = lseek( fd, 0, SEEK_END );
	    if ( insize==(off_t)-1 )
		{
		size = 0;
		success=false;
		}
	    else
		{
		lseek( fd, 0, SEEK_SET );
		size = insize;
		}
	    }
	else
	    {
	    // Old format
	    // Initial 32 bit word holds number of following 32 bit words in file
	    	    int ret=0, readBytes=0;
	    do
		{
		ret = read( fd, &count, 4 );
		if ( ret>0 )
		    readBytes += 4;
		if ( ret<=0 && errno!=EAGAIN && errno!=EINTR )
		    break;
		}
	    while ( readBytes<4 );
	    if ( readBytes>=4 )
		size = count*sizeof(AliUInt32_t);
	    else
		{
		size = 0;
		success = false;
		}
	    }
	if ( size <= 0 )
	    success = false;
	if ( success )
	    data = new AliUInt8_t[ size ];
	if ( !data )
	    success = false;
	else
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTFilePublisherMemory::AliHLTFilePublisherMemory", "File size" )
		<< "Attempting to read " << AliHLTLog::kDec << size << " bytes from file " 
		<< fileIter->c_str() << "." << ENDLOG;
	    int ret;
	    unsigned readData = 0;
	    while ( readData < size )
		{
		ret = read( fd, data+readData, size-readData );
		if ( ret <= 0 && errno!=EAGAIN && errno!=EINTR )
		    {
		    LOG( AliHLTLog::kError, "AliHLTFilePublisherMemory::AliHLTFilePublisherMemory", "Cannot read whole file" )
			<< "Error reading in data from file " << fileIter->c_str()
			<< "." << ENDLOG;
		    success = false;
		    break;
		    }
		readData += ret;
		}
	    }
	close( fd );
	if ( success )
	    {
	    filename = *fileIter;
	    fDatas.insert( fDatas.end(), data );
	    fSizes.insert( fSizes.end(), size );
	    fOffsets.insert( fOffsets.end(), offset );
	    fCopied.insert( fCopied.end(), false );
	    if ( size % gMemoryAlignment )
		size += gMemoryAlignment - (size % gMemoryAlignment);
	    offset += size;
	    }
	else 
	    {
	    if ( data )
		delete [] data;
	    }
	fileIter++;
	if ( formatIter!=formatEnd )
	    formatIter++;
	}
    }

AliHLTFilePublisherMemory::~AliHLTFilePublisherMemory()
    {
    while ( fDatas.size()>0 )
	{
	if ( *(fDatas.begin()) )
	    delete [] *(fDatas.begin());
	fDatas.erase( fDatas.begin() );
	}
    }

unsigned long AliHLTFilePublisherMemory::GetLoadedFileCount() const
    {
    unsigned long cnt=0;
    vector<AliUInt32_t>::const_iterator iter, end;
    iter = fSizes.begin();
    end = fSizes.end();
    while ( iter != end )
	{
	cnt += (*iter ? 1 : 0);
	iter++;
	}
    return cnt;
    }


int AliHLTFilePublisherMemory::WaitForEvent( AliEventID_t& eventID, AliHLTSubEventDataDescriptor*& sbevent,
					   AliHLTEventTriggerStruct*& trg )
    {
    Tick( fEventCounter );
    sbevent = NULL;
    trg = NULL;
    AliUInt32_t offset=0, size=0;
    bool copied = false;
    //fQuitEventLoop = false;
    //fEventLoopQuitted = false;
    AliUInt8_t* ptr = NULL;
    AliUInt32_t filenumber = 0, startfilenumber;
    bool found = false;
    AliUInt32_t bufferSize;
    struct timeval now;
    //#define DO_BENCHMARK
#ifdef DO_BENCHMARK
    struct timeval t_1, t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12;
#endif
#if 0
    AliUInt32_t dt;
#endif
#if __GNUC__>=3
    AliHLTShmID shmKey = { kInvalidShmType, {~(AliUInt64_t)0 } };
#else
    AliHLTShmID shmKey = { kInvalidShmType, {{~(AliUInt64_t)0 }} };
#endif
    AliUInt32_t maxCount;

#ifdef DO_BENCHMARK
    if ( fBenchmark )
	gettimeofday( &t_1, NULL );
#endif
#ifdef DO_BENCHMARK
    if ( fBenchmark )
	{
	gettimeofday( &t0, NULL );
	dt = (t0.tv_sec-t_1.tv_sec)*1000000+(t0.tv_usec-t_1.tv_usec);
	LOG( AliHLTLog::kBenchmark, "AliHLTFilePublisherMemory::WaitForEvent", "Benchmark 0" )
	    << "Time from -1 to 0: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
	gettimeofday( &t2, NULL );
	}
#endif

#ifdef DO_BENCHMARK
    if ( fBenchmark )
	gettimeofday( &t1, NULL );
#endif

    if ( !fBufferManager )
	{
	LOG( AliHLTLog::kError, "AliHLTFilePublisherMemory::WaitForEvent", "No Buffer Manager" )
	    << "No buffer manager specified. Aborting..." << ENDLOG;
	return 0;
	}
    if ( !fShmManager )
	{
	LOG( AliHLTLog::kError, "AliHLTFilePublisherMemory::WaitForEvent", "No Shm Manager" )
	    << "No shared memory manager specified. Aborting..." << ENDLOG;
	// ...
	return 0;
	}
    if ( !fDescriptors )
	{
	LOG( AliHLTLog::kError, "AliHLTFilePublisherMemory::WaitForEvent", "No Descriptor Handler" )
	    << "No descriptor handler specified. Aborting..." << ENDLOG;
	return 0;
	}

// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker eventLoopQuitLock( fEventLoopQuitMutex );
    if ( !fQuitEventLoop || fNoSOREOREvents || (fSORSent && fSendEOR) )
	{
	while ( !fCreateEvents && !fQuitEventLoop && ( fNoSOREOREvents || (fSORSent && !fSendEOR) ) )
	    {
	    eventLoopQuitLock.Unlock();
	    usleep( 10000 );
	    eventLoopQuitLock.Lock();
	    //usleep( 0 );
	    }
	AliUInt32_t tmpCount=0;
#if 1
	unsigned long long avg = 0xFFFFFFFFFFFFFFFFULL;
	do
	    {
	    eventLoopQuitLock.Unlock();
	    unsigned long long tdiff;
	    gettimeofday( &now, NULL );
	    tdiff  = now.tv_sec-fLastAnnounceStart.tv_sec;
	    tdiff *= 1000000;
	    tdiff += now.tv_usec-fLastAnnounceStart.tv_usec;
	    if ( fEventCounterSinceLastStart!=0 )
		avg = tdiff / fEventCounterSinceLastStart;
	    // avg is average time between events since last start/resume
	    eventLoopQuitLock.Lock();
	    if ( avg < fEventTime && !fQuitEventLoop && ( fNoSOREOREvents || (fSORSent && !fSendEOR) ) )
		{
		eventLoopQuitLock.Unlock();
		usleep( 1000 );
		eventLoopQuitLock.Lock();
		}
	    }
	while ( avg < fEventTime && !fQuitEventLoop && ( fNoSOREOREvents || (fSORSent && !fSendEOR) ) );
	eventLoopQuitLock.Unlock();
#else
	do
	    {
	    gettimeofday( &now, NULL );
	    dt = (now.tv_sec-fLastEventStart.tv_sec)*1000000 + (now.tv_usec-fLastEventStart.tv_usec);
	    if ( !fEventTimeJustSleep )
		{
		if ( dt < fEventTime && fEventTime-dt >= 100 )
		    {
		    tmpCount++;
		    usleep( 100 );
		    }
		}
	    else
		{
		if ( dt<fEventTime )
		    usleep( fEventTime-dt-1 );
		}
	    }
	while ( dt < fEventTime );
#endif
#ifdef DO_BENCHMARK
	if ( fBenchmark )
	    {
	    gettimeofday( &t2, NULL );
	    dt = (t2.tv_sec-t1.tv_sec)*1000000+(t2.tv_usec-t1.tv_usec);
	    LOG( AliHLTLog::kBenchmark, "AliHLTFilePublisherMemory::WaitForEvent", "Benchmark 1" )
		<< "Time from 1 to 2: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
	    gettimeofday( &t2, NULL );
	    }
#endif

	if ( !fNoSOREOREvents )
	    {

	    if ( !fSORSent)
		{
		fSORSent = true;
		return GetBroadcastEvent( true, eventID, 
					  sbevent,
					  trg );
		}
	    if ( fSendEOR )
		{
		if ( !fEORSent0 )
		    {
		    fEORSent0 = true;
		    return GetBroadcastEvent( false, eventID, 
					      sbevent,
					      trg );
		    }
		// Two way confirmation necessary, because EOR has to be sent completely before quitting event loop.
		fEORSent1 = true;
		sbevent = NULL;
		trg = NULL;
		return 1;
		}
	    }

	maxCount = fEvents.GetOrigSize();
	if ( !maxCount )
	    maxCount = 16384;
	tmpCount = 0;
#if 0
	struct timespec ts;
#endif
// 	LOG( AliHLTLog::kDebug, "AliHLTFilePublisherMemory::WaitForEvent", "Count" )
// 	    << AliHLTLog::kDec << maxCount << " event slots available. Slots used: "
// 	    << GetPendingEventCount() << "." << ENDLOG;
	eventLoopQuitLock.Lock();
	do
	    {
	    if ( GetPendingEventCount()>=maxCount && !fQuitEventLoop)
		{
		eventLoopQuitLock.Unlock();
#if 1
		usleep( 0);
		UpdateEventAge();
#else
		tmpCount++;
		UpdateEventAge();
		if ( !(tmpCount % fPollSleepModulo) )
		    {
		    //ts.tv_sec = ts.tv_nsec = 0;
		    //ts.tv_nsec = 1000000;
		    ts.tv_sec = fPollSleepTime/1000000;
		    ts.tv_nsec = (fPollSleepTime%1000000)*1000;
		    nanosleep( &ts, NULL );
		    UpdateEventAge();
		    fWaitSleepCount++;
		    }
		else
		    {
		    struct timeval tts, tte;
		    AliUInt32_t dtt;
		    gettimeofday( &tts, NULL );
		    do
			{
			gettimeofday( &tte, NULL );
			dtt = (tte.tv_sec-tts.tv_sec)*1000000+(tte.tv_usec-tts.tv_usec);
			}
		    while ( dtt < fBusyPollTime );
		    }
#endif
		eventLoopQuitLock.Lock();
		}
	    }
	while ( GetPendingEventCount()>=maxCount && tmpCount<fGetBlockRetryCnt && !fQuitEventLoop);
	if ( fQuitEventLoop )
	    {
	    fEventLoopQuitted = true;
	    return 0;
	    }
	eventLoopQuitLock.Unlock();

	LOG( AliHLTLog::kDebug, "AliHLTFilePublisherMemory::WaitForEvent", "Counts" )
	    << "GetPendingEventCount(): " << AliHLTLog::kDec << GetPendingEventCount()
	    << " - slot count: " << maxCount << "." << ENDLOG;
	    
	if ( tmpCount > 0 )
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTFilePublisherMemory::WaitForEvent", "Wait states before event" )
		<< "Needed " << AliHLTLog::kDec << tmpCount << " waitstates before starting event... "
		<< ENDLOG;
	    }
	gettimeofday( &fLastEventStart, NULL );

	if ( GetPendingEventCount() < maxCount )
	    {
	    if ( fStatus )
		{
		AliUInt64_t tm;
		tm = fLastEventStart.tv_sec;
		tm *= 1000000;
		tm += fLastEventStart.tv_usec;
		pthread_mutex_lock( &fStatusMutex );
// XXXX TODO LOCKING XXXX
		    {
		MLUCMutex::TLocker stateLock( fStatus->fStatusDataMutex );
		fStatus->fHLTStatus.fCurrentEventStart = tm;
		    }
		pthread_mutex_unlock( &fStatusMutex );
		}
		
	    startfilenumber = filenumber = fEventCounter % fDatas.size();
	    bufferSize = fBufferManager->GetBufferSize( 0 );
	    shmKey = fBufferManager->GetShmKey( 0 );
	    ptr = fShmManager->GetShm( shmKey );
	    do
		{
		size = fSizes[filenumber];
		copied = fCopied[filenumber];
		offset = fOffsets[filenumber];
		found = true;
		if ( offset+size > bufferSize )
		    {
		    LOG( AliHLTLog::kError, "AliHLTFilePublisherMemory::WaitForEvent", "Buffer too small" )
			<< "Buffer 0 too small for file " << AliHLTLog::kDec << filenumber << " (scheduled "
			<< startfilenumber << ") trying next file." << ENDLOG;
		    found = false;
		    }
		else if ( !copied && fSizes[filenumber] )
		    {
		    if ( ptr )
			ptr += offset;
		    if ( ptr )
			{
#ifndef DO_BENCHMARK
			LOG( AliHLTLog::kDebug, "AliHLTFilePublisherMemory::WaitForEvent", "Copying file event data into buffer" )
			    << "Copying file " << AliHLTLog::kDec << filenumber << " data into buffer." << ENDLOG;
			memcpy( ptr, fDatas[filenumber], fSizes[filenumber] );
#endif
			if ( fSizes[filenumber]==sizeof(gNOPEEventIdentifier) && (*(AliUInt64_t*)ptr)==gNOPEEventIdentifier )
			    {
			    LOG( AliHLTLog::kWarning, "AliHLTFilePublisherMemory::WaitForEvent", "NOPE event detected/handling not implemented" )
				<< "NOPE event detected which should be skipped in event " << AliHLTLog::kDec << filenumber << " but handling not implemented in memory file publisher yet. EVENT WILL BE PUBLISHED. If possible try the -diskpublisher option." << ENDLOG;
			    }
			copied = fCopied[filenumber] = true;
			//fStatus->fHLTStatus.fFreeOutputBuffer XXX
			if ( fStatus )
			    {
			    pthread_mutex_lock( &fStatusMutex );
// XXXX TODO LOCKING XXXX
			    MLUCMutex::TLocker stateLock( fStatus->fStatusDataMutex );
			    fStatus->fHLTStatus.fFreeOutputBuffer -= fSizes[filenumber];
			    pthread_mutex_unlock( &fStatusMutex );
			    }
			}
		    }
		if ( !found )
		    {
		    filenumber++;
		    if ( filenumber==startfilenumber )
			{
			LOG( AliHLTLog::kError, "AliHLTFilePublisherMemory::WaitForEvent", "Buffer completely too small" )
			    << "Buffer 0 too small for any of the " << AliHLTLog::kDec << fDatas.size() << " files." 
			    << ENDLOG;
			found = true;
			if ( fStatus )
			    {
			    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			    fStatus->fProcessStatus.fState |= kAliHLTPCProducerErrorStateFlag;
			    }
			}
		    }
		}
	    while ( !found );
	    }

	if ( copied || !fSizes[filenumber] )
	    { // Copied

#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		{
		gettimeofday( &t5, NULL );
		dt = (t5.tv_sec-t4.tv_sec)*1000000+(t5.tv_usec-t4.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTFilePublisherMemory::WaitForEvent", "Benchmark 3" )
		    << "Time from 4 to 5: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		gettimeofday( &t5, NULL );
		}
#endif

	    eventID.fType = kAliEventTypeData;
	    eventID.fNr = fEventCounter++;
	    eventID.fNr += fEventNrOffset;
	    fEventCounterSinceLastStart++;

	    if ( fEventIDFromFile )
		{
		AliHLTDDLHeader ddlHeader;
		if ( !ddlHeader.Set( (AliUInt32_t*)ptr ) )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTFilePublisherDisk::WaitForEvent", "Cannot read event ID" )
			<< "Cannot read event ID from file '" << fFilenames[filenumber].c_str() << "'." << ENDLOG;
		    }
		else
		    {
		    eventID.fNr = ddlHeader.GetEventID();
		    if ( eventID.fNr<=fLastEventID.fNr )
			{
			AliEventID_t tmpID( kAliEventTypeUnknown, 1 );
			tmpID.fNr <<= 36;
			fAddEventIDCounter.fNr += tmpID.fNr;
			//fAddEventIDCounter &= 0xFFFFFFFFFF000000ULL;
			}
		    fLastEventID = eventID;
		    eventID.fNr |= fAddEventIDCounter.fNr;
		    }
		}

	    
#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		{
		gettimeofday( &t8, NULL );
		dt = (t8.tv_sec-t7.tv_sec)*1000000+(t8.tv_usec-t7.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTFilePublisherMemory::WaitForEvent", "Benchmark 6" )
		    << "Time from 7 to 8: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		gettimeofday( &t8, NULL );
		}
#endif

	    pthread_mutex_lock( &fSEDDMutex );
	    sbevent = fDescriptors->GetFreeEventDescriptor( eventID, 1+GetAdditionalBlockCount() );
	    pthread_mutex_unlock( &fSEDDMutex );

#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		{
		gettimeofday( &t9, NULL );
		dt = (t9.tv_sec-t8.tv_sec)*1000000+(t9.tv_usec-t8.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTFilePublisherMemory::WaitForEvent", "Benchmark 7" )
		    << "Time from 8 to 9: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		gettimeofday( &t9, NULL );
		}
#endif

	    gettimeofday( &now, NULL );
	    sbevent->fEventID = eventID;
	    sbevent->fEventBirth_s = now.tv_sec;
	    sbevent->fEventBirth_us = now.tv_usec;
	    sbevent->fOldestEventBirth_s = fOldestEventBirth;
	    sbevent->fDataBlocks[0].fBlockOffset = offset;
	    sbevent->fDataBlocks[0].fBlockSize = fSizes[filenumber];
	    sbevent->fDataBlocks[0].fShmID.fShmType = shmKey.fShmType;
	    sbevent->fDataBlocks[0].fShmID.fKey.fID = shmKey.fKey.fID;
	    sbevent->fDataBlocks[0].fDataType.fID = fEventDataType.fID; //ADCCOUNTS_DATAID;
	    sbevent->fDataBlocks[0].fDataOrigin.fID = fEventDataOrigin.fID;
	    sbevent->fDataBlocks[0].fDataSpecification = fEventDataSpecification;
	    sbevent->fDataBlocks[0].fStatusFlags = 0;
	    AliHLTSubEventDescriptor::FillBlockAttributes( sbevent->fDataBlocks[0].fAttributes );
	    
	    if ( fStatus )
		{
		pthread_mutex_lock( &fStatusMutex );
// XXXX TODO LOCKING XXXX
		MLUCMutex::TLocker stateLock( fStatus->fStatusDataMutex );
		fStatus->fHLTStatus.fTotalProcessedOutputDataSize += fSizes[filenumber];
		pthread_mutex_unlock( &fStatusMutex );
		}

#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		{
		gettimeofday( &t10, NULL );
		dt = (t10.tv_sec-t9.tv_sec)*1000000+(t10.tv_usec-t9.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTFilePublisherMemory::WaitForEvent", "Benchmark 8" )
		    << "Time from 9 to 10: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		gettimeofday( &t10, NULL );
		}
#endif

	    sbevent->fDataBlocks[0].fProducerNode = fNodeID;

#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		{
		gettimeofday( &t11, NULL );
		dt = (t11.tv_sec-t10.tv_sec)*1000000+(t11.tv_usec-t10.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTFilePublisherMemory::WaitForEvent", "Benchmark 9" )
		    << "Time from 10 to 11: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		gettimeofday( &t11, NULL );
		}
#endif

	    for ( unsigned addBlock=0, addBlockOut=1; addBlock<GetAdditionalBlockCount(); addBlock++ )
		{
		if ( GetAdditionalBlock( addBlock, sbevent->fDataBlocks[addBlockOut] ) )
		    ++addBlockOut;
		}

	    sbevent->fDataType.fID = COMPOSITE_DATAID;

	    if ( fAliceHLT && !fDisableHLTEventTriggerData )
		GetHLTTriggerData( eventID, trg, ptr );
	    else
		trg = fETP;


#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		{
		gettimeofday( &t12, NULL );
		dt = (t12.tv_sec-t11.tv_sec)*1000000+(t12.tv_usec-t11.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTFilePublisherMemory::WaitForEvent", "Benchmark 10" )
		    << "Time from 11 to 12: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		gettimeofday( &t12, NULL );
		}
#endif



	    LOG( AliHLTLog::kDebug, "AliHLTFilePublisherMemory", "WaitForEvent" )
		<< "Copied " << AliHLTLog::kDec << fSizes[filenumber] << " bytes." << ENDLOG;
#ifdef  DO_BOOKKEPPING
		{
		EventUsageData eud;
		eud.fEventID = eventID;
		gettimeofday( &(eud.fEventAnnounce), NULL );
		// 	    LOG( AliHLTLog::kDebug, "AliHLTFilePublisherMemory", "WaitForEvent" )
		// 		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		// 		<< ") Time: " << eud.fEventAnnounce.tv_sec << "." << eud.fEventAnnounce.tv_usec 
		// 		<< "." << ENDLOG;
		eud.fUsageCount = 1;
		pthread_mutex_lock( &fEventDataMutex );
		//fEventData.insert( fEventData.end(), eud );
		fEventData.Add( eud );
		pthread_mutex_unlock( &fEventDataMutex );
		}
#endif // DO_BOOKKEPPING
	    }
	else // of if ( offset == ~0 )
	    {
	    eventLoopQuitLock.Lock();
	    if ( !fQuitEventLoop )
		{
		eventLoopQuitLock.Unlock();
		unsigned long long tdiff;
		LOG( AliHLTLog::kInformational, "AliHLTFilePublisherMemory", "Getting shared memory block" )
		    << "Unable to get shared memory block of size " << AliHLTLog::kDec << size << " bytes (" << ( copied ? "copied" : "not copied") << ") " 
		    << filenumber << " " << fSizes[filenumber] << "."
		    << ENDLOG;
		gettimeofday( &now, NULL );
		tdiff = (now.tv_sec-fLastReleaseRequestTime.tv_sec)*1000000+(now.tv_usec-fLastReleaseRequestTime.tv_usec);
		if ( tdiff > fReleaseRequestTimeDiff )
		    {
		    RequestEventRelease();
		    fLastReleaseRequestTime = now;
		    }
		}
	    else
		eventLoopQuitLock.Unlock();
	    //PingActive();
	    }
	return 1;
	}
    else
	{
	fEventLoopQuitted = true;
	eventLoopQuitLock.Unlock();
	return 0;
	}
    }











/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
