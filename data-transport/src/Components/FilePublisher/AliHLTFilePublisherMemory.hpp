/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIHLTFILEPUBLISHERMEMORY_HPP_
#define _ALIHLTFILEPUBLISHERMEMORY_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTFilePublisher.hpp"
#include "AliHLTDetectorPublisher.hpp"
#include "MLUCHistogram.hpp"
#include "MLUCVector.hpp"
#include <pthread.h>
#include <vector>

class AliHLTFilePublisherMemory: public AliHLTFilePublisher
    {
    public:

	AliHLTFilePublisherMemory( const char* name, AliUInt32_t time_betw_evts_usec,
				   AliUInt32_t eventNrOffset, vector<MLUCString> filenames, 
				   vector<bool> newFileFormat, bool eventIDFromFile, bool eventTypeFromFile, 
				   int eventSlotsPowerOfTwo = -1 );
	virtual ~AliHLTFilePublisherMemory();

	virtual unsigned long GetLoadedFileCount() const;

    protected:

	virtual int WaitForEvent( AliEventID_t& eventID, AliHLTSubEventDataDescriptor*& sbevent,
				  AliHLTEventTriggerStruct*& trg ); // Called with no locked mutexes


	vector<AliUInt8_t*> fDatas;
	vector<AliUInt32_t> fOffsets;
	vector<AliUInt32_t> fSizes;
	vector<bool> fCopied;

    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTFILEPUBLISHERMEMORY_HPP_
