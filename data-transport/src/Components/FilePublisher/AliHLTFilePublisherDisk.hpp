/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIHLTFILEPUBLISHERDISK_HPP_
#define _ALIHLTFILEPUBLISHERDISK_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTFilePublisher.hpp"
#include "AliHLTDetectorPublisher.hpp"
#include "MLUCHistogram.hpp"
#include "MLUCVector.hpp"
#include <pthread.h>
#include <vector>

class AliHLTFilePublisherDisk: public AliHLTFilePublisher
    {
    public:

	AliHLTFilePublisherDisk( const char* name, AliUInt32_t time_betw_evts_usec,
				 AliUInt32_t eventNrOffset, vector<MLUCString> filenames, 
				 vector<bool> newFileFormat, bool eventIDFromFile, bool eventTypeFromFile, 
				 int eventSlotsPowerOfTwo = -1 );
	virtual ~AliHLTFilePublisherDisk();

	virtual unsigned long GetLoadedFileCount() const;

    protected:

	virtual int WaitForEvent( AliEventID_t& eventID, AliHLTSubEventDataDescriptor*& sbevent,
				  AliHLTEventTriggerStruct*& trg ); // Called with no locked mutexes

	void EventFinished( AliEventID_t eventID, AliHLTSubEventDataDescriptor& sedd, vector<AliHLTEventDoneData*>& eventDoneData );
	vector<AliUInt32_t> fSizes;

    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTFILEPUBLISHERDISK_HPP_
