/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIHLTFILEPUBLISHER_HPP_
#define _ALIHLTFILEPUBLISHER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTDetectorPublisher.hpp"
#include "AliHLTHLTEventTriggerData.hpp"
#include "MLUCHistogram.hpp"
#include "MLUCVector.hpp"
#include "MLUCObjectCache.hpp"
#include <pthread.h>
#include <vector>

class AliHLTFilePublisher: public AliHLTDetectorPublisher
    {
    public:

	AliHLTFilePublisher( const char* name, AliUInt32_t time_betw_evts_usec,
			     AliUInt32_t eventNrOffset, vector<MLUCString> filenames, 
			     vector<bool> newFileFormat, bool eventIDFromFile, bool eventTypeFromFile, 
			     int eventSlotsPowerOfTwo = -1 );
	virtual ~AliHLTFilePublisher();

	void StopPublishing();

	AliUInt32_t& fEventTime; // in usec

	AliUInt32_t& fCreateEvents;

	AliUInt32_t& fEventNrOffset;

	AliUInt32_t GetEventCounter()
		{
		return fEventCounter;
		}

#ifdef PROFILING
	virtual void WriteHistograms( const char* filenameprefix );
#else
	virtual void WriteHistograms( const char* )
		{
		}
#endif

	unsigned long GetWaitSleepCount()
		{
		return fWaitSleepCount;
		}

	void SetPollSleepTime( unsigned long pst )
		{
		fPollSleepTime = pst;
		}

	void SetBusyPollTime( unsigned long bpt )
		{
		fBusyPollTime = bpt;
		}

	void SetGetBlockRetryCnt( unsigned long gbrt )
		{
		fGetBlockRetryCnt = gbrt;
		}

	void SetPollSleepModulo( unsigned long mod )
		{
		fPollSleepModulo = mod;
		}

	void SetEventDataType( AliHLTEventDataType datatype )
		{
		fEventDataType.fID = datatype.fID;
		}

	void SetEventDataOrigin( AliHLTEventDataOrigin dataorigin )
		{
		fEventDataOrigin.fID = dataorigin.fID;
		}

	void SetEventDataSpecification( AliUInt32_t dataspec )
		{
		fEventDataSpecification = dataspec;
		}

	void EventTimeJustSleep()
		{
		fEventTimeJustSleep = true;
		}

	void NoSOREOREvents()
		{
		fNoSOREOREvents = true;
		}

	virtual void StartAnnouncing();
	virtual void StopAnnouncing();

	virtual void PauseProcessing();
	virtual void ResumeProcessing();

	virtual unsigned long GetLoadedFileCount() const = 0;



    protected:

// 	virtual int WaitForEvent( AliEventID_t& eventID, AliHLTSubEventDataDescriptor*& sbevent,
// 				  AliHLTEventTriggerStruct*& trg ); // Called with no locked mutexes
	virtual void EventFinished( AliEventID_t, AliHLTSubEventDataDescriptor& sbevent, vector<AliHLTEventDoneData*>& eventDoneData ); // Called with no locked mutexes
	virtual void EventFinished( AliEventID_t, vector<AliHLTEventDoneData*>& eventDoneData ); // Called with no locked mutexes
	virtual void QuitEventLoop(); // Called with no locked mutexes
	bool QuittedEventLoop()
		{
		return fEventLoopQuitted;
		}
	virtual void StartEventLoop();
	virtual void EndEventLoop();

	int GetBroadcastEvent( bool SOR, AliEventID_t& eventID, 
			       AliHLTSubEventDataDescriptor*& sbevent,
			       AliHLTEventTriggerStruct*& trg );

	int GetHLTTriggerData( AliEventID_t eventID, AliHLTEventTriggerStruct*& trg, void const* eventData ); 

	bool GetEventData( AliEventID_t& eventID, AliHLTSubEventDataDescriptor*& sbevent, 
			   AliUInt8_t* data, unsigned long offset, unsigned long size, 
			   AliHLTShmID shmKey, char const* filename );

	virtual bool MustContinue()
		{
		return !fNoSOREOREvents && fSendEOR && !fEORSent1;
		}

// XXXX TODO LOCKING XXXX
	MLUCMutex fEventLoopQuitMutex;
	volatile bool fQuitEventLoop;
	volatile bool fEventLoopQuitted;

	unsigned long fWaitSleepCount;

	unsigned long fPollSleepTime; // usec
	unsigned long fBusyPollTime;  // usec
	unsigned long fGetBlockRetryCnt;
	unsigned long fPollSleepModulo;

	struct timeval fLastAnnounceStart;
	AliUInt64_t fEventCounterSinceLastStart;

	AliUInt64_t fEventCounter;

	AliHLTEventTriggerStruct fETS;
	AliHLTEventTriggerStruct* fETP;
	AliUInt32_t fETPOffset;
	AliUInt32_t fETPIndex;

	AliUInt32_t fLocalEventTime; // in usec

	bool fEventTimeJustSleep;
	
	AliUInt32_t fLocalCreateEvents;

	AliUInt32_t fLocalEventNrOffset;

	struct timeval fLastEventStart;

	AliUInt32_t fOldestEventBirth;

	vector<MLUCString> fFilenames;
	vector<bool> fNewFileFormat;


	//#define DO_BOOKKEPPING
#ifdef  DO_BOOKKEPPING
	struct EventUsageData
	    {
		AliEventID_t fEventID;
		struct timeval fEventAnnounce;
		struct timeval fEventDone;
		int fUsageCount;
	    };

	//vector<EventUsageData> fEventData;
	MLUCVector<EventUsageData> fEventData;
	pthread_mutex_t fEventDataMutex;

	static bool FindEventData( const EventUsageData& eud, uint64 referenceEvent )
		{
		return eud.fEventID == (AliEventID_t)referenceEvent;
		}
#endif // DO_BOOKKEPPING

// XXXX TODO LOCKING XXXX
	MLUCMutex fLastDoneEventIDMutex;
	AliEventID_t fLastDoneEventID;
	struct timeval fLastEventDoneTime;

#ifdef PROFILING
	MLUCHistogram fAnnounceEventTimeSetupHisto;
	MLUCHistogram fAnnounceEventTimeFindSubscribersHisto;
	MLUCHistogram fAnnounceEventTimeStoreEventHisto;
	MLUCHistogram fAnnounceEventTimeFindEventSlotHisto;
	MLUCHistogram fAnnounceEventTimeAnnounceEventHisto;
	MLUCHistogram fAnnounceEventTimeCallbacksHisto;
	MLUCHistogram fAnnounceEventTimeSetTimerHisto;
	MLUCHistogram fAnnounceEventTimeFindTimerHisto;

	MLUCHistogram fEventTimeHisto1;
	MLUCHistogram fEventTimeHisto2;
	MLUCHistogram fEventRateHisto1;
	MLUCHistogram fEventRateHisto2;
	MLUCHistogram fEventRateHisto3;
#endif

	

	struct timeval fLastReleaseRequestTime;
	unsigned long fReleaseRequestTimeDiff;

	AliHLTEventDataType fEventDataType;
	AliHLTEventDataOrigin fEventDataOrigin;
	AliUInt32_t fEventDataSpecification;

	bool fEventIDFromFile;

	bool fEventTypeFromFile;

	AliEventID_t fLastEventID;
	AliEventID_t fAddEventIDCounter;

// XXXX TODO LOCKING XXXX
	MLUCMutex fxORSendMutex;
	volatile bool fSORSent;
	volatile bool fEORSent0;
	volatile bool fEORSent1;
	volatile bool fSendEOR;

	bool fNoSOREOREvents;

	MLUCObjectCache<AliHLTHLTEventTriggerData> fHLTEventTriggerDataCache;
	MLUCIndexedVector<AliHLTHLTEventTriggerData*,AliEventID_t> fHLTEventTriggerData;
	pthread_mutex_t fHLTEventTriggerDataLock;
	bool fDisableHLTEventTriggerData;

    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTFILEPUBLISHER_HPP_
