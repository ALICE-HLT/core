/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTFilePublisherDisk.hpp"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTEventDataType.h"
#include "AliHLTLog.hpp"
#include "AliHLTGetNodeID.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTBufferManager.hpp"
#include "AliHLTShmManager.hpp"
#include "AliHLTStackTrace.hpp"
#include "AliHLTSubEventDescriptor.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "AliHLTDDLHeader.hpp"
#include "NOPEEvents.hpp"
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>





AliHLTFilePublisherDisk::AliHLTFilePublisherDisk( const char* name, AliUInt32_t time_betw_evts_usec,
							    AliUInt32_t eventNrOffset, vector<MLUCString> filenames,
							    vector<bool> newFileFormat, bool eventIDFromFile, bool eventTypeFromFile, 
							    int eventSlotsPowerOfTwo ):
    AliHLTFilePublisher( name, time_betw_evts_usec,
			 eventNrOffset, filenames,
			 newFileFormat, eventIDFromFile, eventTypeFromFile, 
			 eventSlotsPowerOfTwo )
    {
    vector<MLUCString>::iterator fileIter, fileEnd;
    vector<bool>::iterator formatIter, formatEnd;
    fileIter = filenames.begin();
    fileEnd = filenames.end();
    formatIter = newFileFormat.begin();
    formatEnd = newFileFormat.end();
    AliUInt32_t size;
    while ( fileIter != fileEnd )
	{
	int fd;
	AliUInt32_t count=0;
	bool success = true;
	fd = open( fileIter->c_str(), O_RDONLY );
	if ( fd == -1 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTFilePublisherMemory Constructor", "File open" )
		<< "Unable to open file '" << fileIter->c_str() << "': " << strerror(errno)
		<< " (" << AliHLTLog::kDec << errno << ")." << ENDLOG;
	    success = false;
	    }

	if ( formatIter!=formatEnd && *(formatIter) )
	    {
	    // New format
	    // All file contents are read in.
	    off_t insize;
	    insize = lseek( fd, 0, SEEK_END );
	    if ( insize==(off_t)-1 )
		success=false;
	    lseek( fd, 0, SEEK_SET );
	    size = insize;
	    }
	else
	    {
	    // Old format
	    // Initial 32 bit word holds number of following 32 bit words in file
	    int ret=0, readBytes=0;
	    do
		{
		ret = read( fd, &count, 4 );
		if ( ret>0 )
		    readBytes += 4;
		if ( ret<=0 && errno!=EAGAIN && errno!=EINTR )
		    break;
		}
	    while ( readBytes<4 );
	    if ( readBytes>=4 )
		size = count*sizeof(AliUInt32_t);
	    else
		{
		size = 0;
		success = false;
		}
	    }
	close( fd );
	if ( success )
	    {
	    fSizes.insert( fSizes.end(), size );
	    if ( size % gMemoryAlignment )
		size += gMemoryAlignment - (size % gMemoryAlignment);
	    }
	else
	    fSizes.insert( fSizes.end(), 0 );
	fileIter++;
	if ( formatIter!=formatEnd )
	    formatIter++;
	}
    }

AliHLTFilePublisherDisk::~AliHLTFilePublisherDisk()
    {
    }


unsigned long AliHLTFilePublisherDisk::GetLoadedFileCount() const
    {
    unsigned long cnt=0;
    vector<AliUInt32_t>::const_iterator iter, end;
    iter = fSizes.begin();
    end = fSizes.end();
    while ( iter != end )
	{
	cnt += (*iter ? 1 : 0);
	iter++;
	}
    return cnt;
    }

int AliHLTFilePublisherDisk::WaitForEvent( AliEventID_t& eventID, AliHLTSubEventDataDescriptor*& sbevent,
					   AliHLTEventTriggerStruct*& trg )
    {
#if 0
    if ( fQuitEventLoop || fSendEOR )
	{
#define LOGFLAG(flag) << " " << #flag << ": " << ( (flag) ? "true" : "false" )
	LOG( AliHLTLog::kWarning, "AliHLTFilePublisherDisk::WaitForEvent", "Flag debugging" )
	    << "Flag debugging: "
	    LOGFLAG(fQuitEventLoop)
	    LOGFLAG(fNoSOREOREvents)
	    LOGFLAG(fSORSent)
	    LOGFLAG(fSendEOR)
	    LOGFLAG(fCreateEvents)
	    << ENDLOG;
	}
#endif
    Tick( fEventCounter );
    sbevent = NULL;
    trg = NULL;
    bool blockOk = false;
    unsigned long offset=0, size=0;
    //fQuitEventLoop = false;
    //fEventLoopQuitted = false;
    AliUInt8_t* ptr = NULL;
    AliUInt32_t filenumber = 0;
    struct timeval now;
    //#define DO_BENCHMARK
#ifdef DO_BENCHMARK
    struct timeval t_1, t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12;
#endif
#if 0
    AliUInt32_t dt;
    bool blockOk = false;
#endif
#if __GNUC__>=3
    AliHLTShmID shmKey = { kInvalidShmType, {~(AliUInt64_t)0 } };
#else
    AliHLTShmID shmKey = { kInvalidShmType, {{~(AliUInt64_t)0 }} };
#endif

#ifdef DO_BENCHMARKc
    if ( fBenchmark )
	gettimeofday( &t_1, NULL );
#endif
#ifdef DO_BENCHMARK
    if ( fBenchmark )
	{
	gettimeofday( &t0, NULL );
	dt = (t0.tv_sec-t_1.tv_sec)*1000000+(t0.tv_usec-t_1.tv_usec);
	LOG( AliHLTLog::kBenchmark, "AliHLTFilePublisherDisk::WaitForEvent", "Benchmark 0" )
	    << "Time from -1 to 0: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
	gettimeofday( &t2, NULL );
	}
#endif

#ifdef DO_BENCHMARK
    if ( fBenchmark )
	gettimeofday( &t1, NULL );
#endif

    if ( !fBufferManager )
	{
	LOG( AliHLTLog::kError, "AliHLTFilePublisherDisk::WaitForEvent", "No Buffer Manager" )
	    << "No buffer manager specified. Aborting..." << ENDLOG;
	return 0;
	}
    if ( !fShmManager )
	{
	LOG( AliHLTLog::kError, "AliHLTFilePublisherDisk::WaitForEvent", "No Shm Manager" )
	    << "No shared memory manager specified. Aborting..." << ENDLOG;
	// ...
	return 0;
	}
    if ( !fDescriptors )
	{
	LOG( AliHLTLog::kError, "AliHLTFilePublisherDisk::WaitForEvent", "No Descriptor Handler" )
	    << "No descriptor handler specified. Aborting..." << ENDLOG;
	return 0;
	}

// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker eventLoopQuitLock( fEventLoopQuitMutex );
    if ( !fQuitEventLoop || fNoSOREOREvents || (fSORSent && fSendEOR) ) 
	{
	while ( !fCreateEvents && !fQuitEventLoop && ( fNoSOREOREvents || (fSORSent && !fSendEOR) ) )
	    {
	    eventLoopQuitLock.Unlock();
	    usleep( 10000 );
	    //usleep( 0 );
	    eventLoopQuitLock.Lock();
	    }
	AliUInt32_t tmpCount=0;
	unsigned long sz = 0;
	do
	    {
#if 1
	    unsigned long long avg = 0xFFFFFFFFFFFFFFFFULL;
// XXXX TODO LOCKING XXXX
	    MLUCMutex::TLocker xORLock( fxORSendMutex );
	    do
		{
		xORLock.Unlock();
		eventLoopQuitLock.Unlock();
		unsigned long long tdiff;
		gettimeofday( &now, NULL );
		if ( fStartTimestamp )
		    {
		    if ( (unsigned long)now.tv_sec >= fStartTimestamp )
			{
			tdiff  = now.tv_sec-fStartTimestamp;
			tdiff *= 1000000;
			tdiff += now.tv_usec;
			}
		    else
			{
			tdiff = 0;
			if ( (unsigned long)fStartTime.tv_sec != fStartTimestamp || fStartTime.tv_usec != 0 )
			    {
			    fStartTime.tv_sec = fStartTimestamp;
			    fStartTime.tv_usec = 0;
			    }
			if ( (unsigned long)fResumeTime.tv_sec != fStartTimestamp || fResumeTime.tv_usec != 0 )
			    {
			    fResumeTime.tv_sec = fStartTimestamp;
			    fResumeTime.tv_usec = 0;
			    }
			}
		    }
		else
		    {
		    tdiff  = now.tv_sec-fLastAnnounceStart.tv_sec;
		    tdiff *= 1000000;
		    tdiff += now.tv_usec-fLastAnnounceStart.tv_usec;
		    }
		if ( fEventCounterSinceLastStart!=0 )
		    avg = tdiff / fEventCounterSinceLastStart;
		// avg is average time between events since last start/resume
		eventLoopQuitLock.Lock();
		xORLock.Lock();
#if 0
		if ( fQuitEventLoop || fSendEOR )
		    {
#define LOGFLAG(flag) << " " << #flag << ": " << ( (flag) ? "true" : "false" )
		    LOG( AliHLTLog::kWarning, "AliHLTFilePublisherDisk::WaitForEvent", "Flag debugging 2" )
			<< "Flag debugging 2: "
			LOGFLAG( (avg < fEventTime) )
			<< ENDLOG;
		    }
#endif
		if ( ((fStartTimestamp && (unsigned long)now.tv_sec<fStartTimestamp) || avg < fEventTime) && !fQuitEventLoop && ( fNoSOREOREvents || (fSORSent && !fSendEOR) ) )
		    {
		    xORLock.Unlock();
		    eventLoopQuitLock.Unlock();
		    usleep( 1000 );
		    eventLoopQuitLock.Lock();
		    xORLock.Lock();
		    }
		
		}
	    while ( ((fStartTimestamp && (unsigned long)now.tv_sec<fStartTimestamp) || avg < fEventTime) && !fQuitEventLoop && ( fNoSOREOREvents || (fSORSent && !fSendEOR) ) );
	    xORLock.Unlock();
	    eventLoopQuitLock.Unlock();
#else
	    do
		{
		gettimeofday( &now, NULL );
		dt = (now.tv_sec-fLastEventStart.tv_sec)*1000000 + (now.tv_usec-fLastEventStart.tv_usec);
		if ( !fEventTimeJustSleep )
		    {
		    if ( dt < fEventTime && fEventTime-dt >= 100 )
			{
			tmpCount++;
			usleep( 100 );
			}
		    }
		else
		    {
		    if ( dt<fEventTime && ( fNoSOREOREvents || (fSORSent && !fSendEOR) ) )
			usleep( fEventTime-dt-1 );
		    }
		}
	    while ( dt < fEventTime );
#endif
#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		{
		gettimeofday( &t2, NULL );
		dt = (t2.tv_sec-t1.tv_sec)*1000000+(t2.tv_usec-t1.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTFilePublisherDisk::WaitForEvent", "Benchmark 1" )
		    << "Time from 1 to 2: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		gettimeofday( &t2, NULL );
		}
#endif
	    
	    xORLock.Lock();
	    if ( !fNoSOREOREvents )
		{
		
		if ( !fSORSent)
		    {
		    fSORSent = true;
		    xORLock.Unlock();
		    return GetBroadcastEvent( true, eventID, 
					      sbevent,
					      trg );
		    }
		if ( fSendEOR )
		    {
		    if ( !fEORSent0 )
			{
			fEORSent0 = true;
			xORLock.Unlock();
			return GetBroadcastEvent( false, eventID, 
						  sbevent,
						  trg );
			}
		    // Two way confirmation necessary, because EOR has to be sent completely before quitting event loop.
		    fEORSent1 = true;
		    sbevent = NULL;
		    trg = NULL;
		    return 1;
		    }
		}
	    xORLock.Unlock();
	    
	    tmpCount = 0;
	    struct timespec ts;
	    AliUInt32_t bufferNdx;
	    
	    filenumber = fEventCounter % fSizes.size();
	    do
		{
		eventLoopQuitLock.Lock();
		if ( fQuitEventLoop )
		    {
		    offset = (AliUInt32_t)~0;
		    eventLoopQuitLock.Unlock();
		    break;
		    }
		eventLoopQuitLock.Unlock();
#ifdef DO_BENCHMARK
		if ( fBenchmark )
		    gettimeofday( &t3, NULL );
#endif
		size = fSizes[filenumber];
		blockOk = fBufferManager->GetBlock( bufferNdx, offset, size );
#ifdef DO_BENCHMARK
		if ( fBenchmark )
		    {
		    gettimeofday( &t4, NULL );
		    dt = (t4.tv_sec-t3.tv_sec)*1000000+(t4.tv_usec-t3.tv_usec);
		    LOG( AliHLTLog::kBenchmark, "AliHLTMultFileBenchPublisher::WaitForEvent", "Benchmark 2" )
			<< "Time from 3 to 4: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		    gettimeofday( &t4, NULL );
		    }
#endif
		if ( fStatus )
		    {
		    pthread_mutex_lock( &fStatusMutex );
// XXXX TODO LOCKING XXXX
		    MLUCMutex::TLocker stateLock( fStatus->fStatusDataMutex );
		    fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
		    pthread_mutex_unlock( &fStatusMutex );
		    }
		if ( !blockOk )
		    {
		    UpdateEventAge();
		    if ( !(tmpCount % fPollSleepModulo) )
			{
			//ts.tv_sec = ts.tv_nsec = 0;
			//ts.tv_nsec = 1000000;
			ts.tv_sec = fPollSleepTime/1000000;
			ts.tv_nsec = (fPollSleepTime%1000000)*1000;
			nanosleep( &ts, NULL );
			UpdateEventAge();
			fWaitSleepCount++;
			}
		    else
			{
			struct timeval tts, tte;
			AliUInt32_t dtt;
			gettimeofday( &tts, NULL );
			do
			    {
			    gettimeofday( &tte, NULL );
			    dtt = (tte.tv_sec-tts.tv_sec)*1000000+(tte.tv_usec-tts.tv_usec);
			    }
			while ( dtt < fBusyPollTime && ( fNoSOREOREvents || (fSORSent && !fSendEOR) ) );
			}
		    }
		}
	    while ( !blockOk && tmpCount++<fGetBlockRetryCnt && ( fNoSOREOREvents || (fSORSent && !fSendEOR) ) );
	    eventLoopQuitLock.Lock();
	    if ( fQuitEventLoop )
		{
		fEventLoopQuitted = true;
		eventLoopQuitLock.Unlock();
		return 0;
		}
	    eventLoopQuitLock.Unlock();
	    
	    if ( tmpCount > 1 )
		{
		LOG( AliHLTLog::kDebug, "AliHLTFilePublisherDisk::WaitForEvent", "Wait states before event" )
		    << "Needed " << AliHLTLog::kDec << tmpCount << " waitstates before starting event... "
		    << ENDLOG;
		}
	    gettimeofday( &fLastEventStart, NULL );
	    
	    if ( blockOk )
		shmKey = fBufferManager->GetShmKey( bufferNdx );
	    else
		{
		shmKey.fShmType = kInvalidShmType;
		shmKey.fKey.fID = ~(AliUInt64_t)0;
		}
	    if ( shmKey.fShmType != kInvalidShmType )
		{
		if ( fStatus )
		    {
		    AliUInt64_t tm;
		    tm = fLastEventStart.tv_sec;
		    tm *= 1000000;
		    tm += fLastEventStart.tv_usec;
		    pthread_mutex_lock( &fStatusMutex );
// XXXX TODO LOCKING XXXX
			{
			MLUCMutex::TLocker stateLock( fStatus->fStatusDataMutex );
			fStatus->fHLTStatus.fCurrentEventStart = tm;
			}
			pthread_mutex_unlock( &fStatusMutex );
		    }
		
		ptr = fShmManager->GetShm( shmKey );
		if ( ptr )
		    ptr += offset;
		}
	    else
		ptr = NULL;
	    sz = 0;
	    if ( ptr )
		{
		int ret;
		int fh;
		unsigned long readIn=0;
		sz = fSizes[filenumber];
		fh = open( fFilenames[filenumber].c_str(), O_RDONLY );
		if ( fh==-1 )
		    {
		    ret = errno;
		    sz = 0;
		    LOG( AliHLTLog::kWarning, "AliHLTFilePublisherDisk::WaitForEvent", "Error opening file" )
			<< "Error opening file " << fFilenames[filenumber].c_str() << ": " << strerror(ret)
			<< " (" << AliHLTLog::kDec << ret << "). Publishing as empty..." << ENDLOG;
		    }
		while ( readIn<sz )
		    {
		    ret = read( fh, ptr+readIn, sz-readIn );
		    if ( ret <=0 )
			{
			ret = errno;
			sz = 0;
			LOG( AliHLTLog::kWarning, "AliHLTFilePublisherDisk::WaitForEvent", "Error reading file" )
			    << "Error reading from file " << fFilenames[filenumber].c_str() << ": " << strerror(ret)
			    << " (" << AliHLTLog::kDec << ret << "). Publishing as empty..." << ENDLOG;
			break;
			}
		    readIn += ret;
		    }
		close( fh );
		if ( sz==sizeof(gNOPEEventIdentifier) && (*(AliUInt64_t*)ptr)==gNOPEEventIdentifier )
		    {
		    // Found "NOPE" event
		    fEventCounter++;
		    fEventCounterSinceLastStart++;
		    fBufferManager->ReleaseBlock( bufferNdx, offset );
		    }
		else
		    {

#ifdef DO_BENCHMARK
		    if ( fBenchmark )
			{
			gettimeofday( &t5, NULL );
			dt = (t5.tv_sec-t4.tv_sec)*1000000+(t5.tv_usec-t4.tv_usec);
			LOG( AliHLTLog::kBenchmark, "AliHLTFilePublisherDisk::WaitForEvent", "Benchmark 3" )
			    << "Time from 4 to 5: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
			gettimeofday( &t5, NULL );
			}
#endif
		    eventID.fType = kAliEventTypeData;
		    eventID.fNr = fEventCounter++;
		    eventID.fNr += fEventNrOffset;
		    fEventCounterSinceLastStart++;
	    
		    if ( fEventIDFromFile )
			{
			AliHLTDDLHeader ddlHeader;
			if ( !ddlHeader.Set( (AliUInt32_t*)ptr ) )
			    {
			    LOG( AliHLTLog::kWarning, "AliHLTFilePublisherDisk::WaitForEvent", "Cannot read event ID" )
				<< "Cannot read event ID from file '" << fFilenames[filenumber].c_str() << "'." << ENDLOG;
			    }
			else
			    {
			    eventID.fNr = ddlHeader.GetEventID();
			    if ( eventID.fNr<=fLastEventID.fNr )
				{
				AliEventID_t tmpID( kAliEventTypeUnknown, 1 );
				tmpID.fNr <<= 36;
				fAddEventIDCounter.fNr += tmpID.fNr;
#if 0
				LOG( AliHLTLog::kWarning, "AliHLTFilePublisherDisk::WaitForEvent", "Period Counter" )
				    << "Period counter now " << AliHLTLog::kDec << periodCounter << " (0x"
				    << AliHLTLog::kHex << periodCounter << ")." << ENDLOG;
#endif
#if 0
				unsigned long long periodCounter = fAddEventIDCounter.fNr >> 36;
				struct timeval pcNow;
				gettimeofday( &pcNow, NULL );
				MLUCString time_str = MLUCString::TimeStamp2String( pcNow );
				MLUCString filename;
				filename = GetName();
				filename += "-PeriodCounter.log";
				FILE* fp = fopen( filename.c_str(), "a" );
				fprintf( fp, "%s - %Lu\n", time_str.c_str(), periodCounter );
				fclose( fp );
#endif
				
				//fAddEventIDCounter &= 0xFFFFFFFFFF000000ULL;
				}
			    fLastEventID = eventID;
			    eventID.fNr |= fAddEventIDCounter.fNr;
			    }
			}

		    pthread_mutex_lock( &fSEDDMutex );
		    sbevent = fDescriptors->GetFreeEventDescriptor( eventID, 1+GetAdditionalBlockCount() );
		    pthread_mutex_unlock( &fSEDDMutex );

		    if ( !sbevent )
			{
			fEventCounter--;
			fEventCounterSinceLastStart--;
			}
		    }
		}
	    }
	while ( sz==sizeof(gNOPEEventIdentifier) && ptr && (*(AliUInt64_t*)ptr)==gNOPEEventIdentifier );

	if ( sbevent )
	    {
	    
#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		{
		gettimeofday( &t8, NULL );
		dt = (t8.tv_sec-t7.tv_sec)*1000000+(t8.tv_usec-t7.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTFilePublisherDisk::WaitForEvent", "Benchmark 6" )
		    << "Time from 7 to 8: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		gettimeofday( &t8, NULL );
		}
#endif


#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		{
		gettimeofday( &t9, NULL );
		dt = (t9.tv_sec-t8.tv_sec)*1000000+(t9.tv_usec-t8.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTFilePublisherDisk::WaitForEvent", "Benchmark 7" )
		    << "Time from 8 to 9: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		gettimeofday( &t9, NULL );
		}
#endif

	    gettimeofday( &now, NULL );
	    sbevent->fEventID = eventID;
	    sbevent->fEventBirth_s = now.tv_sec;
	    sbevent->fEventBirth_us = now.tv_usec;
	    sbevent->fOldestEventBirth_s = fOldestEventBirth;
	    sbevent->fDataBlocks[0].fBlockOffset = offset;
	    sbevent->fDataBlocks[0].fBlockSize = sz;
	    sbevent->fDataBlocks[0].fShmID.fShmType = shmKey.fShmType;
	    sbevent->fDataBlocks[0].fShmID.fKey.fID = shmKey.fKey.fID;
	    sbevent->fDataBlocks[0].fDataType.fID = fEventDataType.fID; //ADCCOUNTS_DATAID;
	    sbevent->fDataBlocks[0].fDataOrigin.fID = fEventDataOrigin.fID;
	    sbevent->fDataBlocks[0].fDataSpecification = fEventDataSpecification;
	    sbevent->fDataBlocks[0].fStatusFlags = 0;
	    AliHLTSubEventDescriptor::FillBlockAttributes( sbevent->fDataBlocks[0].fAttributes );
	    
	    if ( fStatus )
		{
		pthread_mutex_lock( &fStatusMutex );
// XXXX TODO LOCKING XXXX
		MLUCMutex::TLocker stateLock( fStatus->fStatusDataMutex );
		fStatus->fHLTStatus.fTotalProcessedOutputDataSize += sz;
		pthread_mutex_unlock( &fStatusMutex );
		}

#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		{
		gettimeofday( &t10, NULL );
		dt = (t10.tv_sec-t9.tv_sec)*1000000+(t10.tv_usec-t9.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTFilePublisherDisk::WaitForEvent", "Benchmark 8" )
		    << "Time from 9 to 10: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		gettimeofday( &t10, NULL );
		}
#endif

	    sbevent->fDataBlocks[0].fProducerNode = fNodeID;

#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		{
		gettimeofday( &t11, NULL );
		dt = (t11.tv_sec-t10.tv_sec)*1000000+(t11.tv_usec-t10.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTFilePublisherDisk::WaitForEvent", "Benchmark 9" )
		    << "Time from 10 to 11: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		gettimeofday( &t11, NULL );
		}
#endif

	    for ( unsigned addBlock=0, addBlockOut=1; addBlock<GetAdditionalBlockCount(); addBlock++ )
		{
		if ( GetAdditionalBlock( addBlock, sbevent->fDataBlocks[addBlockOut] ) )
		    ++addBlockOut;
		}

	    sbevent->fDataType.fID = COMPOSITE_DATAID;

	    if ( fAliceHLT && !fDisableHLTEventTriggerData )
		GetHLTTriggerData( eventID, trg, ptr );
	    else
		trg = fETP;


#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		{
		gettimeofday( &t12, NULL );
		dt = (t12.tv_sec-t11.tv_sec)*1000000+(t12.tv_usec-t11.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTFilePublisherDisk::WaitForEvent", "Benchmark 10" )
		    << "Time from 11 to 12: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		gettimeofday( &t12, NULL );
		}
#endif



	    LOG( AliHLTLog::kDebug, "AliHLTFilePublisherDisk", "WaitForEvent" )
		<< "Copied " << AliHLTLog::kDec << fSizes[filenumber] << " bytes." << ENDLOG;
#ifdef  DO_BOOKKEPPING
		{
		EventUsageData eud;
		eud.fEventID = eventID;
		gettimeofday( &(eud.fEventAnnounce), NULL );
		// 	    LOG( AliHLTLog::kDebug, "AliHLTFilePublisherDisk", "WaitForEvent" )
		// 		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		// 		<< ") Time: " << eud.fEventAnnounce.tv_sec << "." << eud.fEventAnnounce.tv_usec 
		// 		<< "." << ENDLOG;
		eud.fUsageCount = 1;
		pthread_mutex_lock( &fEventDataMutex );
		//fEventData.insert( fEventData.end(), eud );
		fEventData.Add( eud );
		pthread_mutex_unlock( &fEventDataMutex );
		}
#endif // DO_BOOKKEPPING
	    }
	else // of if ( offset == ~0 )
	    {
	    eventLoopQuitLock.Lock();
	    if ( !fQuitEventLoop )
		{
		eventLoopQuitLock.Unlock();
		unsigned long long tdiff;
		LOG( AliHLTLog::kInformational, "AliHLTFilePublisherDisk", "Getting shared memory block" )
		    << "Unable to get shared memory block of size " << AliHLTLog::kDec << size << " bytes " 
		    << " for file number "
		    << filenumber << " " << fSizes[filenumber] << "."
		    << ENDLOG;
		if ( !fBufferManager->IsBlockFeasible( fSizes[filenumber] ) )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTFilePublisherDisk", "Can never get shared memory block" )
			<< "Permamently unable to get shared memory block of size " << AliHLTLog::kDec << size << " bytes " 
		    << " for file number "
			<< filenumber << " " << fSizes[filenumber] << " as it is larger than any available buffer."
			<< " Skipping this file..."
			<< ENDLOG;
		    fEventCounter++;
		    }
		gettimeofday( &now, NULL );
		tdiff = (now.tv_sec-fLastReleaseRequestTime.tv_sec)*1000000+(now.tv_usec-fLastReleaseRequestTime.tv_usec);
		if ( tdiff > fReleaseRequestTimeDiff )
		    {
		    RequestEventRelease();
		    fLastReleaseRequestTime = now;
		    }
		}
	    else
		eventLoopQuitLock.Unlock();
	    //PingActive();
	    }
	return 1;
	}
    else
	{
	fEventLoopQuitted = true;
	return 0;
	}
    }


void AliHLTFilePublisherDisk::EventFinished( AliEventID_t eventID, AliHLTSubEventDataDescriptor& sbevent, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    if ( eventID.fType != kAliEventTypeStartOfRun && eventID.fType != kAliEventTypeEndOfRun )
	{
	AliUInt32_t i, ndx;
	if ( !fBufferManager )
	    {
	    LOG( AliHLTLog::kError, "AliHLTFilePublisherDisk::EventFinished", "No Buffer Manager" )
		<< "No buffer manager specified." << ENDLOG;
	    }
	if ( !fShmManager )
	    {
	    LOG( AliHLTLog::kError, "AliHLTFilePublisherDisk::EventFinished", "No Shm Manager" )
		<< "No shared memory manager specified." << ENDLOG;
	    }
	AliUInt32_t baseLimit=0;
	if ( sbevent.fDataBlockCount>GetAdditionalBlockCount() )
	    baseLimit = sbevent.fDataBlockCount-GetAdditionalBlockCount();
	for ( i = 0; i < sbevent.fDataBlockCount; i++ )
	    {
	    if ( i<baseLimit || !ReleaseAdditionalBlock( sbevent.fDataBlocks[i] ) )
		{
		if ( fBufferManager )
		    {
		    ndx = fBufferManager->GetBufferIndex( sbevent.fDataBlocks[i].fShmID );
		    if ( ndx != ~(AliUInt32_t)0 )
			fBufferManager->ReleaseBlock( ndx, sbevent.fDataBlocks[i].fBlockOffset );
		    }
		if ( fShmManager )
		    fShmManager->ReleaseShm( sbevent.fDataBlocks[i].fShmID );
		}
	    }
	if ( fStatus )
	    {
	    pthread_mutex_lock( &fStatusMutex );
// XXXX TODO LOCKING XXXX
	    MLUCMutex::TLocker stateLock( fStatus->fStatusDataMutex );
	    fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
	    pthread_mutex_unlock( &fStatusMutex );
	    }
	}
    AliHLTFilePublisher::EventFinished( eventID, sbevent, eventDoneData );
    }









/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
