/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTFilePublisher.hpp"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTEventDataType.h"
#include "AliHLTLog.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTBufferManager.hpp"
#include "AliHLTShmManager.hpp"
#include "AliHLTStackTrace.hpp"
#include "AliHLTSubEventDescriptor.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "AliHLTDDLHeaderData.h"
#include "AliHLTDDLHeader.hpp"
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>



#ifdef PROFILING
#define STDHISTO(histo) histo(0,10000,1)
#define ADDHISTO(histos,name) histos.insert( histos.end(), &name )
#else
#define STDHISTO(histo)
#define ADDHISTO(histos,name)
#endif


AliHLTFilePublisher::AliHLTFilePublisher( const char* name, AliUInt32_t time_betw_evts_usec,
					  AliUInt32_t eventNrOffset, vector<MLUCString> filenames,
					  vector<bool> newFileFormat, 
					  bool eventIDFromFile, 
					  bool eventTypeFromFile, 
					  int eventSlotsPowerOfTwo ):
    AliHLTDetectorPublisher( name, eventSlotsPowerOfTwo ),
    fEventTime( fLocalEventTime ),
    fCreateEvents( fLocalCreateEvents ), fEventNrOffset( fLocalEventNrOffset ),
    fFilenames( filenames ),
    fNewFileFormat( newFileFormat ),
#ifdef  DO_BOOKKEPPING
    fEventData( eventSlotsPowerOfTwo, true ),
#endif  // DO_BOOKKEPPING
#ifdef PROFILING
    STDHISTO( fAnnounceEventTimeSetupHisto ),
    STDHISTO( fAnnounceEventTimeFindSubscribersHisto ),
    STDHISTO( fAnnounceEventTimeStoreEventHisto ),
    STDHISTO( fAnnounceEventTimeFindEventSlotHisto ),
    STDHISTO( fAnnounceEventTimeAnnounceEventHisto ),
    STDHISTO( fAnnounceEventTimeCallbacksHisto ),
    STDHISTO( fAnnounceEventTimeSetTimerHisto ),
    STDHISTO( fAnnounceEventTimeFindTimerHisto ),
    fEventTimeHisto1( 0.0, 1000000.0, 100 ), fEventTimeHisto2( 0, 50000.0, 100 ), 
    fEventRateHisto1( 0.0, 100000, 10 ), fEventRateHisto2( 0.0, 1500, 10 ), fEventRateHisto3( 0.0, 500, 10 )
    ,
#endif
    //fMultRoot( "Alil3MultRootMultFileBenchPublisher", "A publisher of data read in from root files" )
    fEventIDFromFile( eventIDFromFile ),
    fEventTypeFromFile( eventTypeFromFile ),
    fAddEventIDCounter( kAliEventTypeUnknown, (~(AliUInt64_t)0) << 36 ),
    fHLTEventTriggerDataCache( eventSlotsPowerOfTwo ),
    fHLTEventTriggerData( 8, eventSlotsPowerOfTwo )
    {
    fWaitSleepCount = 0;
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker eventLoopQuitLock( fEventLoopQuitMutex );
    fQuitEventLoop = false;
    fEventLoopQuitted = false;
    eventLoopQuitLock.Unlock();
    fEventCounter=0;
    fCreateEvents = 1;
    fEventNrOffset = eventNrOffset;

    fOldestEventBirth = 0;

#ifdef  DO_BOOKKEPPING
    pthread_mutex_init( &fEventDataMutex, NULL );
#endif // DO_BOOKKEPPING

    fEventTime = time_betw_evts_usec;

    //     fETPOffset = GetFreeBlock( sizeof(AliHLTEventTriggerStruct), fETPIndex );
    //     if ( fETPOffset == (AliUInt32_t)~0 )
    // 	{
    fETP = &fETS;
    // 	}
    //     else
    // 	{
    // 	AliUInt8_t* ptr = (AliUInt8_t*)GetShmPtr(fETPIndex);
    // 	ptr += fETPOffset;
    // 	fETP = (AliHLTEventTriggerStruct*)ptr;
    // 	}
    fETP->fHeader.fLength = sizeof(AliHLTEventTriggerStruct);
    fETP->fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
    fETP->fHeader.fSubType.fID = 0;
    fETP->fHeader.fVersion = 1;
    fETP->fDataWordCount = 0;

    fLastEventStart.tv_sec = fLastEventStart.tv_usec = 0;

// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker lastDoneEventIDLock( fLastDoneEventIDMutex );
    fLastDoneEventID = ~(AliUInt64_t)0;
    gettimeofday( &fLastEventDoneTime, NULL );
    lastDoneEventIDLock.Unlock();
    fPollSleepTime = 20000;
    fBusyPollTime = 1;
    fPollSleepModulo = 10;
//     fPollSleepTime = 10000;
//     fBusyPollTime = 5;
//     fPollSleepModulo = 1000;
    fGetBlockRetryCnt = 10000;
    fLastReleaseRequestTime.tv_sec = fLastReleaseRequestTime.tv_usec = 0;
    fReleaseRequestTimeDiff = 500000; // by default send at most 2 EventReleaseRequest message per second.
    fEventDataType.fID = UNKNOWN_DATAID;
    fEventDataOrigin.fID = UNKNOWN_DATAORIGIN;
    fEventDataSpecification = ~(AliUInt32_t)0;
    fEventTimeJustSleep = false;
    fLastAnnounceStart.tv_sec = fLastAnnounceStart.tv_usec = 0;
    fEventCounterSinceLastStart = 0;

// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker xORLock( fxORSendMutex );
    fSORSent = false;
    fEORSent0 = false;
    fEORSent1 = false;
    fSendEOR = false;

    fNoSOREOREvents = false;
    xORLock.Unlock();

    pthread_mutex_init( &fHLTEventTriggerDataLock, NULL );
    fDisableHLTEventTriggerData = false;



    ADDHISTO( fAnnounceEventTimeSetupHistos, fAnnounceEventTimeSetupHisto );
    ADDHISTO( fAnnounceEventTimeFindSubscribersHistos, fAnnounceEventTimeFindSubscribersHisto );
    ADDHISTO( fAnnounceEventTimeStoreEventHistos, fAnnounceEventTimeStoreEventHisto );
    ADDHISTO( fAnnounceEventTimeFindEventSlotHistos, fAnnounceEventTimeFindEventSlotHisto );
    ADDHISTO( fAnnounceEventTimeAnnounceEventHistos, fAnnounceEventTimeAnnounceEventHisto );
    ADDHISTO( fAnnounceEventTimeCallbacksHistos, fAnnounceEventTimeCallbacksHisto );
    ADDHISTO( fAnnounceEventTimeSetTimerHistos, fAnnounceEventTimeSetTimerHisto );
    ADDHISTO( fAnnounceEventTimeFindTimerHistos, fAnnounceEventTimeFindTimerHisto );
    }

AliHLTFilePublisher::~AliHLTFilePublisher()
    {
#ifdef  DO_BOOKKEPPING
    pthread_mutex_destroy( &fEventDataMutex );
#endif // DO_BOOKKEPPING
    pthread_mutex_destroy( &fHLTEventTriggerDataLock );
    }

void AliHLTFilePublisher::StopPublishing()
    {
    QuitEventLoop();
    fEventAnnouncer.Quit();
    struct timespec ts;
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker eventLoopQuitLock( fEventLoopQuitMutex );
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 50000000;
    gettimeofday( &start, NULL );
    while ( !QuittedEventLoop() && deltaT<timeLimit )
	{
	eventLoopQuitLock.Unlock();
	ts.tv_sec = 0;
	ts.tv_nsec = 500000;
	nanosleep( &ts, NULL );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	eventLoopQuitLock.Lock();
	}
    
    }

#ifdef PROFILING
#define WRITEHISTO(histoname) name=filenameprefix;name+="-"#histoname".histo";f##histoname.Write(name.c_str());

void AliHLTFilePublisher::WriteHistograms( const char* filenameprefix )
    {
    MLUCString name;

    WRITEHISTO( AnnounceEventTimeSetupHisto );
    WRITEHISTO( AnnounceEventTimeFindSubscribersHisto );
    WRITEHISTO( AnnounceEventTimeStoreEventHisto );
    WRITEHISTO( AnnounceEventTimeFindEventSlotHisto );
    WRITEHISTO( AnnounceEventTimeAnnounceEventHisto );
    WRITEHISTO( AnnounceEventTimeCallbacksHisto );
    WRITEHISTO( AnnounceEventTimeSetTimerHisto );
    WRITEHISTO( AnnounceEventTimeFindTimerHisto );
    WRITEHISTO( EventTimeHisto1 );
    WRITEHISTO( EventTimeHisto2 );
    WRITEHISTO( EventRateHisto1 );
    WRITEHISTO( EventRateHisto2 );
    WRITEHISTO( EventRateHisto3 );
//     name = GetName();
//     name += "-EventTime1.histo";
//     fEventTimeHisto1.Write( name.c_str() );
//     name = GetName();
//     name += "-EventTime2.histo";
//     fEventTimeHisto2.Write( name.c_str() );
//     name = GetName();
//     name += "-EventRate1.histo";
//     fEventRateHisto1.Write( name.c_str() );
//     name = GetName();
//     name += "-EventRate2.histo";
//     fEventRateHisto2.Write( name.c_str() );
//     name = GetName();
//     name += "-EventRate3.histo";
//     fEventRateHisto3.Write( name.c_str() );
    }
#endif


void AliHLTFilePublisher::StartAnnouncing()
    {
    gettimeofday( &fLastAnnounceStart, NULL );
    fEventCounterSinceLastStart = 0;
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker xORLock( fxORSendMutex );
    fSendEOR = false;
    fSORSent = false;
    fEORSent0 = false;
    fEORSent1 = false;
    xORLock.Unlock();
    AliHLTDetectorPublisher::StartAnnouncing();
    }

void AliHLTFilePublisher::StopAnnouncing()
    {
#if 0
    LOG( AliHLTLog::kWarning, "AliHLTFilePublisher::StopAnnouncing", "Enforcing EOR event" )
	<< "Trying to enforce sending of EOR event" << ENDLOG;
#endif
    if ( fStatus && !fNoSOREOREvents && !fEORSent1 )
	{
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCBusyStateFlag;
	}
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker xORLock( fxORSendMutex );
    fSendEOR = true;
// XXXX TODO LOCKING XXXX
    xORLock.Unlock();
    fPauseSignal.Signal();
#if 1
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long minTimeLimit = 10000000ULL;
    const unsigned long long timeLimit = ( ( fEventTime*10 > minTimeLimit) ? fEventTime*10 : minTimeLimit );
    gettimeofday( &start, NULL );
#if 0
    unsigned count=0;
#endif
// XXXX TODO LOCKING XXXX
    xORLock.Lock();
    while ( !fNoSOREOREvents && !fEORSent1 && deltaT<timeLimit )
	{
// XXXX TODO LOCKING XXXX
	xORLock.Unlock();
	usleep( 20000 );
	xORLock.Lock();
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
#if 0
	++count;
#endif
	}
// XXXX TODO LOCKING XXXX
#else
    unsigned cnt=0;
    const unsigned maxCntLimit = 10000;
    const unsigned cntLimit = ( (fEventTime/1000*10)<maxCntLimit ? maxCntLimit : fEventTime/1000*10 );
    while ( !fNoSOREOREvents && !fEORSent1 && cnt++<cntLimit )
	{
// XXXX TODO LOCKING XXXX
	xORLock.Unlock();
	usleep( 0 );
	xORLock.Lock();
	}
// XXXX TODO LOCKING XXXX
#endif
    xORLock.Unlock();
    AliHLTDetectorPublisher::StopAnnouncing();
#if 1
    if ( !fNoSOREOREvents && !fEORSent1 )
	{
	LOG( AliHLTLog::kWarning, "AliHLTFilePublisher::StopAnnouncing", "No EOR event sent" )
	    << "No EOR event sent after " << AliHLTLog::kDec << deltaT
	    << "microsec. (timelimit: " << timeLimit << " microsec. "
#if 0
	    << "(Iterations: " << count << ") "
#endif
	    << "(Debug Info: fNoSOREOREvents: "
	    << (fNoSOREOREvents ? "true" : "false") << " - fEORSent0: "
	    << (fEORSent0 ? "true" : "false") << " - fEORSent1: "
	    << (fEORSent1 ? "true" : "false") << " - fSendEOR: "
	    << (fSendEOR ? "true" : "false") << ")." << ENDLOG;
	}
#else
    if ( !fNoSOREOREvents && !fEORSent1 )
	{
	LOG( AliHLTLog::kWarning, "AliHLTFilePublisher::StopAnnouncing", "No EOR event sent" )
	    << "No EOR event sent - fNoSOREOREvents: "
	    << (fNoSOREOREvents ? "true" : "false") << " - fEORSent1: "
	    << (fEORSent1 ? "true" : "false") << " - cnt: "
	    << AliHLTLog::kDec << cnt << " - cntLimit: "
	    << AliHLTLog::kDec << cntLimit << ENDLOG;
	}
#endif
    }

void AliHLTFilePublisher::PauseProcessing()
    {
    AliHLTDetectorPublisher::PauseProcessing();
    }

void AliHLTFilePublisher::ResumeProcessing()
    {
    gettimeofday( &fLastAnnounceStart, NULL );
    fEventCounterSinceLastStart = 0;
    AliHLTDetectorPublisher::ResumeProcessing();
    }


void AliHLTFilePublisher::EventFinished( AliEventID_t eventID, AliHLTSubEventDataDescriptor& sedd, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    if ( eventID.fType == kAliEventTypeStartOfRun )
	{
	unsigned long n = GetAdditionalBlockCount();
	if ( sedd.fDataBlockCount>=n )
	    {
	    for ( unsigned long ii=sedd.fDataBlockCount-n; ii<sedd.fDataBlockCount; ii++ )
		{
		ReleaseAdditionalBlock( sedd.fDataBlocks[ii] );
		}
	    }
	ReleaseSORBlocks();
	}
    else if ( eventID.fType == kAliEventTypeEndOfRun )
	{
	unsigned long n = GetAdditionalBlockCount();
	if ( sedd.fDataBlockCount>=n )
	    {
	    for ( unsigned long ii=sedd.fDataBlockCount-n; ii<sedd.fDataBlockCount; ii++ )
		{
		ReleaseAdditionalBlock( sedd.fDataBlocks[ii] );
		}
	    }
	ReleaseEORBlocks();
	}
    EventFinished( eventID, eventDoneData );
    }

void AliHLTFilePublisher::EventFinished( AliEventID_t eventID, vector<AliHLTEventDoneData*>& )
    {
    //     LOG( AliHLTLog::kError, "AliHLTFilePublisher::EventFinished", "No Descriptor" )
    // 	<< "Event finished called without a descriptor handler for event 0x" 
    // 	<< AliHLTLog::kHex << eventID << " (" 
    // 	<< AliHLTLog::kDec << eventID << ")..." << ENDLOG;
#ifdef  DO_BOOKKEPPING
	bool found = false;
#endif // DO_BOOKKEPPING

    ; // Non conditional block following...
	{
	struct timeval ed;
	gettimeofday( &ed, NULL );
	//vector<EventUsageData>::iterator iter, end;
#ifdef  DO_BOOKKEPPING
	EventUsageData* iter = NULL;
	pthread_mutex_lock( &fEventDataMutex );
	unsigned long ndx;
	
	if ( fEventData.FindElement( &FindEventData, (uint64)eventID, ndx ) )
	    {
	    found = true;
	    iter = fEventData.GetPtr( ndx );
	    iter->fUsageCount--;
	    if ( iter->fUsageCount < 0 )
		{
// XXXX TODO LOCKING XXXX
		MLUCMutex::TLocker lastDoneEventIDLock( fLastDoneEventIDMutex );
		LOG( AliHLTLog::kError, "AliHLTFilePublisher::EventFinished", "Multiple Event Done" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		    << ") done multiple times. Last done event: 0x" << AliHLTLog::kHex 
		    << fLastDoneEventID << " (" << AliHLTLog::kDec << fLastDoneEventID
		    << ")." << ENDLOG;
		AliHLTStackTrace( "AliHLTFilePublisher::EventFinished", AliHLTLog::kError, 10 );
		}
	    iter->fEventDone.tv_sec = ed.tv_sec;
	    iter->fEventDone.tv_usec = ed.tv_usec;

#ifdef DO_BENCHMARK
		{
		AliUInt32_t dt;
		dt = (iter->fEventDone.tv_sec-iter->fEventAnnounce.tv_sec)*1000000
		     +(iter->fEventDone.tv_usec-iter->fEventAnnounce.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTFilePublisher::EventFinished", "Benchmark" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		    << ") time: " << dt << " musec." << ENDLOG;
#ifdef PROFILING
		fEventTimeHisto1.Add( dt );
		fEventTimeHisto2.Add( dt );
#endif
		}
#endif

#define DONT_KEEP_ALL
#ifdef DONT_KEEP_ALL
		fEventData.Remove( ndx );
#endif
	    }



#ifdef DO_BENCHMARK
	    {
	    AliUInt32_t dt = (ed.tv_sec-fLastEventDoneTime.tv_sec)*1000000+
			     ed.tv_usec-fLastEventDoneTime.tv_usec;
	    LOG( AliHLTLog::kBenchmark, "AliHLTFilePublisher::EventFinished", "Benchmark" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		<< ") time diff: " << dt << " musec." << ENDLOG;
	    fLastEventDoneTime = ed;
#ifdef PROFILING
	    fEventRateHisto1.Add( dt );
	    fEventRateHisto2.Add( dt );
	    fEventRateHisto3.Add( dt );
#endif
	    }
#endif

	pthread_mutex_unlock( &fEventDataMutex );
	if ( !found )
	    {
// XXXX TODO LOCKING XXXX
	    MLUCMutex::TLocker lastDoneEventIDLock( fLastDoneEventIDMutex );
	    LOG( AliHLTLog::kError, "AliHLTFilePublisher::EventFinished", "Unknown Event Done" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		<< ") done multiple times. Last done event: 0x" << AliHLTLog::kHex 
		<< fLastDoneEventID << " (" << AliHLTLog::kDec << fLastDoneEventID
		<< ")." << ENDLOG;
	    AliHLTStackTrace( "AliHLTFilePublisher::EventFinished", AliHLTLog::kError, 10 );
	    }
#endif // DO_BOOKKEPPING
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker lastDoneEventIDLock( fLastDoneEventIDMutex );
	fLastDoneEventID = eventID;

	}

    pthread_mutex_lock( &fHLTEventTriggerDataLock );
    unsigned long ndx;
    if ( (fAliceHLT && !fDisableHLTEventTriggerData) || fHLTEventTriggerData.GetCnt()>0 ) // fHLTEventTriggerData.GetCnt()>0 means there are events in there, maybe because the trigger data was switched off after some time of operation.
	{
	if ( fHLTEventTriggerData.FindElement( eventID, ndx ) )
	    {
	    fHLTEventTriggerDataCache.Release( fHLTEventTriggerData.Get(ndx) );
	    fHLTEventTriggerData.Remove( ndx );
	    }
	else if ( eventID.fType != kAliEventTypeTick )
	    {
	    AliHLTLog::TLogLevel lvl;
	    if ( !fDisableHLTEventTriggerData )
		lvl = AliHLTLog::kError;
	    else
		lvl = AliHLTLog::kWarning;
	    LOG( lvl, "AliHLTFilePublisher::EventFinished", "Cannot find HLTEventTriggerData" )
		<< "Unable to find HLT event trigger data object for event 0x" 
		<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ")." << ENDLOG;
	    }
	}
    pthread_mutex_unlock( &fHLTEventTriggerDataLock );

	
    }



int AliHLTFilePublisher::GetBroadcastEvent( bool SOR, AliEventID_t& eventID, 
					     AliHLTSubEventDataDescriptor*& sbevent,
					     AliHLTEventTriggerStruct*& trg )
    {
    vector<AliHLTSubEventDataBlockDescriptor> blocks;
    if ( SOR )
	{
	CreateSORBlocks( blocks );
	eventID.fType = kAliEventTypeStartOfRun;
	}
    else
	{
	CreateEORBlocks( blocks );
	eventID.fType = kAliEventTypeEndOfRun;
	}
    eventID.fNr = 0;
    pthread_mutex_lock( &fSEDDMutex );
    sbevent = fDescriptors->GetFreeEventDescriptor( eventID, blocks.size()+GetAdditionalBlockCount() );
    pthread_mutex_unlock( &fSEDDMutex );
    if ( sbevent )
	{
	struct timeval now;
	gettimeofday( &now, NULL );
	sbevent->fEventID = eventID;
	sbevent->fStatusFlags |= ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST;
	sbevent->fEventBirth_s = now.tv_sec;
	sbevent->fEventBirth_us = now.tv_usec;
	sbevent->fOldestEventBirth_s = fOldestEventBirth;
	sbevent->fDataType.fID = COMPOSITE_DATAID;
	unsigned addBlockOut = 0;
	for ( addBlockOut = 0; addBlockOut < blocks.size(); addBlockOut++ )
	    sbevent->fDataBlocks[addBlockOut] = blocks[addBlockOut];
	
	for ( unsigned addBlock=0; addBlock<GetAdditionalBlockCount(); addBlock++ )
	    {
	    if ( GetAdditionalBlock( addBlock, sbevent->fDataBlocks[addBlockOut] ) )
		++addBlockOut;
	    }

	if ( fAliceHLT )
	    GetHLTTriggerData( eventID, trg, NULL );
	else
	    trg = fETP;
	

	return 1;
	}
    return 0;
    }



void AliHLTFilePublisher::QuitEventLoop()
    {
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker eventLoopQuitLock( fEventLoopQuitMutex );
    fQuitEventLoop = true;
    }

void AliHLTFilePublisher::StartEventLoop()
    {
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker eventLoopQuitLock( fEventLoopQuitMutex );
    fEventLoopQuitted = fQuitEventLoop = false;
    eventLoopQuitLock.Unlock();
    if ( fPersistentState )
	fEventCounter = fPersistentState->fLastEventID.fNr+(unsigned)1;
    }

void AliHLTFilePublisher::EndEventLoop()
    {
    }


int AliHLTFilePublisher::GetHLTTriggerData( AliEventID_t eventID, AliHLTEventTriggerStruct*& trg, void const* eventData )
    {
    pthread_mutex_lock( &fHLTEventTriggerDataLock );
    AliHLTHLTEventTriggerData* hltTrg = fHLTEventTriggerDataCache.Get();
    if ( !hltTrg )
	{
	LOG( AliHLTLog::kError, "AliHLTFilePublisher::GetHLTTriggerData", "out of memory (HLTEventTriggerData)" )
	    << "Out of memory trying to allocate HLT event trigger data object - using empty trigger data." << ENDLOG;
	trg = NULL;
	pthread_mutex_unlock( &fHLTEventTriggerDataLock );
	return ENOMEM;
	}
    else
	{
	fHLTEventTriggerData.Add( hltTrg, eventID );
	if ( eventData ) {
	  hltTrg->SetCommonHeader(eventData);
	}
	else
	    {
	    memset( hltTrg->fCommonHeader, 0, sizeof(hltTrg->fCommonHeader[0])*hltTrg->fCommonHeaderWordCnt );
	    }
	AliHLTSetDDLHeaderEventID( hltTrg->fCommonHeader, eventID );
	AliHLTSetDDLHeaderBlockLength( hltTrg->fCommonHeader, (sizeof(hltTrg->fCommonHeader[0])*hltTrg->fCommonHeaderWordCnt) );
	if ( eventID.fType==kAliEventTypeStartOfRun || eventID.fType==kAliEventTypeEndOfRun || eventID.fType==kAliEventTypeSync)
	    {
	    AliUInt32_t l1Msg = AliHLTGetDDLHeaderL1TriggerType( hltTrg->fCommonHeader );
	    l1Msg |= 0x1;
	    if ( eventID.fType==kAliEventTypeStartOfRun )
		l1Msg |= (0xE << 2);
	    if ( eventID.fType==kAliEventTypeEndOfRun )
		l1Msg |= (0xF << 2);
	    if ( eventID.fType==kAliEventTypeSync )
		l1Msg |= (0xD << 2);
	    AliHLTSetDDLHeaderL1TriggerType( hltTrg->fCommonHeader, l1Msg );
	    }
	FillHLTEventTriggerDataDDLList( hltTrg );
	trg = &(hltTrg->fETS);
	}
    pthread_mutex_unlock( &fHLTEventTriggerDataLock );
    return 0;
    }


bool AliHLTFilePublisher::GetEventData( AliEventID_t& eventID, AliHLTSubEventDataDescriptor*& sbevent, 
					AliUInt8_t* data, unsigned long offset, unsigned long size, 
					AliHLTShmID shmKey, char const* filename )
    {
    eventID.fType = kAliEventTypeData;
    eventID.fNr = fEventCounter++;
    eventID.fNr += fEventNrOffset;
    fEventCounterSinceLastStart++;
    
    AliHLTDDLHeader ddlHeader;
    bool ddlHeaderSet = false;
    if ( fEventIDFromFile || fEventTypeFromFile )
	{
	if ( !ddlHeader.Set( (AliUInt32_t*)data ) )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTFilePublisher::GetEventData", "Cannot read event ID" )
		<< "Cannot read event ID or type from file '" << filename << "'." << ENDLOG;
	    }
	else
	    ddlHeaderSet = true;
	}
    if ( fEventIDFromFile && ddlHeaderSet )
	{
	eventID.fNr = ddlHeader.GetEventID();
	if ( eventID.fNr<=fLastEventID.fNr )
	    {
	    AliEventID_t tmpID( kAliEventTypeUnknown, 1 );
	    tmpID.fNr <<= 36;
	    fAddEventIDCounter.fNr += tmpID.fNr;
	    //fAddEventIDCounter &= 0xFFFFFFFFFF000000ULL;
	    }
	fLastEventID = eventID;
	eventID.fNr |= fAddEventIDCounter.fNr;
	}
    if ( fEventTypeFromFile && ddlHeaderSet )
	{
	AliUInt32_t L1msg = ddlHeader.GetL1TriggerType();
	if ( (L1msg & 0x1) && ( ((L1msg & 0x3C) >> 2) == 0xE ) )
	    eventID.fType = kAliEventTypeStartOfRun;
	else if ( (L1msg & 0x1) && ( ((L1msg & 0x3C) >> 2) == 0xF ) )
	    eventID.fType = kAliEventTypeEndOfRun;
	else if ( (L1msg & 0x1) && ( ((L1msg & 0x3C) >> 2) == 0xD ) )
	    eventID.fType = kAliEventTypeSync;
	else
	    eventID.fType = kAliEventTypeData;
	}
    
    pthread_mutex_lock( &fSEDDMutex );
    sbevent = fDescriptors->GetFreeEventDescriptor( eventID, 1+GetAdditionalBlockCount() );
    pthread_mutex_unlock( &fSEDDMutex );
    
    if ( !sbevent )
	{
	fEventCounter--;
	fEventCounterSinceLastStart--;
	return false;
	}
    struct timeval now;
    gettimeofday( &now, NULL );
    sbevent->fEventID = eventID;
    sbevent->fEventBirth_s = now.tv_sec;
    sbevent->fEventBirth_us = now.tv_usec;
    sbevent->fOldestEventBirth_s = fOldestEventBirth;
    sbevent->fDataBlocks[0].fBlockOffset = offset;
    sbevent->fDataBlocks[0].fBlockSize = size;
    sbevent->fDataBlocks[0].fShmID.fShmType = shmKey.fShmType;
    sbevent->fDataBlocks[0].fShmID.fKey.fID = shmKey.fKey.fID;
    sbevent->fDataBlocks[0].fDataType.fID = fEventDataType.fID; //ADCCOUNTS_DATAID;
    sbevent->fDataBlocks[0].fDataOrigin.fID = fEventDataOrigin.fID;
    sbevent->fDataBlocks[0].fDataSpecification = fEventDataSpecification;
    sbevent->fDataBlocks[0].fStatusFlags = 0;
    AliHLTSubEventDescriptor::FillBlockAttributes( sbevent->fDataBlocks[0].fAttributes );
    
    sbevent->fDataBlocks[0].fProducerNode = fNodeID;

    for ( unsigned addBlock=0, addBlockOut=1; addBlock<GetAdditionalBlockCount(); addBlock++ )
	{
	if ( GetAdditionalBlock( addBlock, sbevent->fDataBlocks[addBlockOut] ) )
	    ++addBlockOut;
	}
    
    sbevent->fDataType.fID = COMPOSITE_DATAID;
    return true;
    }






/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
