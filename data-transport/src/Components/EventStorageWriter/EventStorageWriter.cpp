/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTProcessingSubscriber.hpp"
//#include "AliHLTControlledProcessComponent.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTEventStorageInterface.hpp"
#include "AliHLTSimpleFileEventStorage.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include <errno.h>


class EventStorageSubscriber: public AliHLTProcessingSubscriber
    {
    public:
	EventStorageSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize,
				AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, int eventCountAlloc ):
	    AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc )
		{
		fStorage = NULL;
		}

	void SetStorage( AliHLTEventStorageInterface* storage )
		{
		fStorage = storage;
		}

    protected:

	bool ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, 
			   const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* etsp,
			   AliUInt8_t*, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>&, AliHLTEventDoneData*& edd )
		{
		edd = NULL;
		size = 0;
		int ret;
		if ( blockCnt>0 )
		    {
		    LOG( AliHLTLog::kDebug, "EventStorageSubscriber::ProcessEvent", "Event sizes" )
			<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID 
			<< ") block size: " << blocks[0].fSize << "." << ENDLOG;
		    }
		if ( fStorage )
		    {
		    ret = fStorage->WriteEvent( sedd, blockCnt, blocks, etsp );
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "EventStorageSubscriber::ProcessEvent", "Error writing event" )
			    << "Error writing event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			    << eventID << " to storage: " << strerror(ret) << " (" << ret << ")." << ENDLOG;
			}
		    }
		return true;
		}

	AliHLTEventStorageInterface* fStorage;

    };


//class EventStorageComponent: public AliHLTControlledSinkComponent
class EventStorageComponent: public AliHLTControlledDefaultSinkComponent<EventStorageSubscriber>
    {
    public:

	typedef AliHLTControlledDefaultSinkComponent<EventStorageSubscriber> TSuper;

	EventStorageComponent( const char* name, int argc, char** argv );
	virtual ~EventStorageComponent() {};

    protected:

	virtual bool CheckConfiguration();
	virtual void ShowConfiguration();

	virtual bool CreateParts();
	virtual void DestroyParts();

	virtual bool SetupComponents();

// 	virtual AliHLTProcessingSubscriber* CreateSubscriber();
	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

// 	virtual AliHLTProcessingSubscriber* CreateSubscriber();

	AliHLTEventStorageInterface* fStorage;
	MLUCString fStorageName;
	MLUCString fStorageType;

    private:
    };


EventStorageComponent::EventStorageComponent( const char* name, int argc, char** argv ):
    TSuper( name, argc, argv )
    {
    fStorage = NULL;
    }


bool EventStorageComponent::CheckConfiguration()
    {
    if ( !TSuper::CheckConfiguration() )
	return false;
    if ( fStorageType=="" )
	{
	LOG( AliHLTLog::kError, "EventStorageComponent::CheckConfiguration", "No storage type specified" )
	    << "Must specify a storage type to use. (e.g. using the -storage command line option."
	    << ENDLOG;
	return false;
	}
    if ( fStorageName=="" )
	{
	LOG( AliHLTLog::kError, "EventStorageComponent::CheckConfiguration", "No storage name specified" )
	    << "Must specify a storage name to use. (e.g. using the -storage command line option."
	    << ENDLOG;
	return false;
	}
    if ( fStorageType!="simplefile" )
	{
	LOG( AliHLTLog::kError, "EventStorageComponent::CheckConfiguration", "Wrong storage type specified" )
	    << "An unsupported storage type (" << fStorageType.c_str() 
	    << ") was specified. Currently supported are 'simplefile' storages."
	    << ENDLOG;
	return false;
	}
    return true;
    }

void EventStorageComponent::ShowConfiguration()
    {
    TSuper::ShowConfiguration();
    LOG( AliHLTLog::kInformational, "EventStorageComponent::ShowConfiguration", "Storage config" )
	<< "Storage Type: '" << fStorageType.c_str() << "' - Storage Name: '" 
	<< fStorageName.c_str() << "'." << ENDLOG;
    }

bool EventStorageComponent::CreateParts()
    {
    if ( !TSuper::CreateParts() )
	return false;
    if ( fStorageType=="simplefile" )
	{
	fStorage = new AliHLTSimpleFileEventStorage();
	if ( !fStorage )
	    {
	    LOG( AliHLTLog::kError, "EventStorageComponent::CreateParts", "Out of memory" )
		<< "Out of memory while creating storage of type '" << fStorageType.c_str() << "'." << ENDLOG;
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return false;
	    }
	}
    return true;
    }

void EventStorageComponent::DestroyParts()
    {
    if ( fStorage )
	{
	fStorage->CloseStorage();
	delete fStorage;
	fStorage = NULL;
	}
    TSuper::DestroyParts();
    }

bool EventStorageComponent::SetupComponents()
    {
    if ( !TSuper::SetupComponents() )
	return false;
    int ret=EIO;
    if ( fStorage )
	ret = fStorage->OpenStorage( fStorageName.c_str(), true );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "EventStorageComponent::SetupComponents", "Error opening storage" )
	    << "Error opening storage '" << fStorageName.c_str() 
	    << "' of type '" << fStorageType.c_str() << "': " 
	    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
// 	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	return false;
	}
    if ( fSubscriber && fStorage )
	reinterpret_cast<EventStorageSubscriber*>( fSubscriber )->SetStorage( fStorage );
    return true;
    }


const char* EventStorageComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    if ( !strcmp( argv[i], "-storage" ) )
	{
	if ( argc <= i +2 )
	    {
	    errorArg = i;
	    return "Missing storage type or storage name specifier.";
	    }
	fStorageType = argv[i+1];
	if ( strcmp( argv[i+1], "simplefile") )
	    {
	    errorArg = i+1;
	    return "Unsupported or unknown storage type specified.";
	    }
	fStorageName = argv[i+2];
	i += 3;
	return NULL;
	}
    return TSuper::ParseCmdLine( argc, argv, i, errorArg );
    }

void EventStorageComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    TSuper::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-storage <storage_type> <storage_name>: Specify the type of storage to be used and its name (Supported types currently are 'simplefile'). (Mandatory)" 
	<< ENDLOG;
    }

// AliHLTProcessingSubscriber* EventStorageComponent::CreateSubscriber()
//     {
//     return new EventStorageSubscriber( fSubscriberID.c_str(), fSendEventDone, fMinBlockSize,
// 				    fMaxPreEventCountExp2 );
//     }


int main( int argc, char** argv )
    {
    EventStorageComponent procComp( "EventStorageComponent", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

