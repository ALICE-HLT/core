/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTBlockCompareSubscriber.hpp"
#include "AliHLTLog.hpp"

AliHLTBlockCompareSubscriber::AliHLTBlockCompareSubscriber( const char* name, bool sendEventDone, 
							    AliUInt32_t minBlockSize, 
							    AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, 
							    int eventCountAlloc ):
  AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc )
{
}

  
AliHLTBlockCompareSubscriber::~AliHLTBlockCompareSubscriber()
{
}


bool AliHLTBlockCompareSubscriber::ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, 
						AliHLTSubEventDescriptor::BlockData* blocks, 
						const AliHLTSubEventDataDescriptor*, const AliHLTEventTriggerStruct*, AliUInt8_t*, 
						AliUInt32_t& size, 
						vector<AliHLTSubEventDescriptor::BlockData>&, 
						 AliHLTEventDoneData*& )
{
  AliUInt32_t i, j;
  AliUInt8_t *p1, *p2;
  AliUInt32_t *lp1, *lp2;
  AliUInt32_t len1, len2;
  AliUInt32_t nlp, nl, n;
  bool ok;
  size = 0;

  for ( i = 0; i < blockCnt; i++ )
    {
      p1 = (AliUInt8_t*)blocks[i].fData;
      lp1 = (AliUInt32_t*)p1;
      len1 = blocks[i].fSize;
      nlp = len1/4;
      nl = len1;
      for ( j = i+1; j < blockCnt; j++ )
	{
	  ok = true;
	  p2 = (AliUInt8_t*)blocks[j].fData;
	  lp2 = (AliUInt32_t*)p2;
	  len2 = blocks[j].fSize;
	  if ( len1 != len2 )
	    {
	      LOG( AliHLTLog::kError, "AliHLTBlockCompareSubscriber::ProcessEvent", "Event Block Size Differs" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		<< ") blocks " << i << " and " << j << " differ in size ( " << len1 << " <-> "
		<< len2 << " ) ( 0x" << AliHLTLog::kHex << len1 << " <-> 0x" << len2 << " )." 
		<< ENDLOG;
	      if ( len2 < len1 )
		{
		nlp = len2/4;
		nl = len2;
		}
	      //ok = false;
	    }
	  for ( n = 0; n < nlp; n++ )
	    {
	      lp1 = (AliUInt32_t*)( p1 + n*4 );
	      lp2 = (AliUInt32_t*)( p2 + n*4 );
	      if ( *lp1 != *lp2 )
		{
		  LOG( AliHLTLog::kError, "AliHLTBlockCompareSubscriber::ProcessEvent", "Event Blocks Differ" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		    << ") blocks " << i << " and " << j << " differ at position " << n*4
		    << ": " << *lp1 << " <-> " << *lp2 << " ( 0x" << AliHLTLog::kHex << *lp1 
		    << " <-> 0x" << *lp2 << " )." 
		    << ENDLOG;
		  ok = false;
		  break;
		}
	    }
	  if ( ok )
	    {
	      for ( n = nlp*4; n < nl; n++ )
		{
		  if ( *(p1+n) != *(p2+n) )
		    {
		      LOG( AliHLTLog::kError, "AliHLTBlockCompareSubscriber::ProcessEvent", "Event Blocks Differ" )
			<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
			<< ") blocks " << i << " and " << j << " differ at position " << n
			<< ": " << *(p1+n) << " <-> " << *(p2+n) << " ( 0x" << AliHLTLog::kHex << *(p1+n) 
			<< " <-> 0x" << *(p2+n) << " )." 
			<< ENDLOG;
		      ok = false;
		      break;
		    }
		}
	    }
	  if ( ok )
	      {
	      LOG( AliHLTLog::kInformational, "AliHLTBlockCompareSubscriber::ProcessEvent", "No Block Difference" )
		  << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		  << ") blocks " << i << " and " << j << " do not differ in their common data." << ENDLOG;
	      }
	}
    }
  return true;
}




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
