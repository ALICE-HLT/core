/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author: $ - Initial Version by Artur Szostak
**
** $Id: DDLConnectionTester.cpp 123 2008-06-05 15:04:00Z timm $ 
**
***************************************************************************
*/

#include "AliHLTControlledComponent.hpp"
#include "AliHLTProcessingSubscriber.hpp"
#include "AliHLTAlignment.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string>
#include <algorithm>


/**
 * Subscriber class which implements actual processing for the
 * DDLConnectionTester component.
 * The processing involves checking that the incomming data contains data blocks
 * with the following format:
 *   The size of the data block should be 100 32-bit little endian words.
 *   The first word should be set to 0x1 and the remaining 99 words should
 *   contain the DDL equipment ID as known to DAQ and send by DAQ over the
 *   fibre.
 * The DDL equipment ID is checked, including the plug string if it was set
 * on the command line. The result is packed into the following data structure
 * and returned as a test summary data block.
 *
 *   struct {
 *      AliUInt32_t fExpectedDDL;
 *      AliUInt32_t fReceivedDDL;
 *      char fExpectedPlug[1024];  // NULL terminated string.
 *      char fReceivedPlug[1024];  // NULL terminated string.
 *   };
 *
 * The data type, origin and specification fields are copied to the output
 * data block.
 * 
 */
class AliHLTDDLConnectionTesterSubscriber : public AliHLTProcessingSubscriber
{
public:

	AliHLTDDLConnectionTesterSubscriber(
			const char* name,
			bool sendEventDone,
			AliUInt32_t minBlockSize, 
			AliUInt32_t perEventFixedSize,
			AliUInt32_t perBlockFixedSize,
			double sizeFactor,
			int eventCountAlloc = -1
		) :
		AliHLTProcessingSubscriber(name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc),
		fExpectedDDL(0xFFFFFFFF),
		fExpectedPlug(""),
		fReceivedPlug("")
	{
	}
	
	void SetParameters(
			AliUInt32_t expectedDDL,
			const char* expectedPlug = NULL,
			const char* receivedPlug = NULL
		)
	{
		fExpectedDDL = expectedDDL;
		if (expectedPlug != NULL)
			fExpectedPlug = expectedPlug;
		else
			fExpectedPlug = "";
		if (receivedPlug != NULL)
			fReceivedPlug = receivedPlug;
		else
			fReceivedPlug = "";
	}

	// Method inherited from AliHLTProcessingSubscriber:
	virtual bool ProcessEvent(
			AliEventID_t eventID,
			AliUInt32_t blockCnt,
			AliHLTSubEventDescriptor::BlockData* blocks,
			const AliHLTSubEventDataDescriptor* /*sedd*/,
			const AliHLTEventTriggerStruct* /*ets*/,
			AliUInt8_t* outputPtr,
			AliUInt32_t& size,
			vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks,
			AliHLTEventDoneData*& /*doneData*/
		);
protected:

	AliUInt32_t fExpectedDDL;  // The expected DDL equipment ID number.
	std::string fExpectedPlug;  // The expected DDL link plug string.
	std::string fReceivedPlug;  // The received DDL link plug string.
};


struct AliHLTSummaryInfo
{
	AliUInt32_t fExpectedDDL;
	AliUInt32_t fReceivedDDL;
	char fExpectedPlug[1024];  // NULL terminated string.
	char fReceivedPlug[1024];  // NULL terminated string.
};


bool AliHLTDDLConnectionTesterSubscriber::ProcessEvent(
		AliEventID_t eventID,
		AliUInt32_t blockCnt,
		AliHLTSubEventDescriptor::BlockData* blocks,
		const AliHLTSubEventDataDescriptor* /*sedd*/,
		const AliHLTEventTriggerStruct* /*ets*/,
		AliUInt8_t* outputPtr,
		AliUInt32_t& size,
		vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks,
		AliHLTEventDoneData*& /*doneData*/
	)
{
	AliHLTSummaryInfo* summary = reinterpret_cast<AliHLTSummaryInfo*>(outputPtr);
	if (size < sizeof(AliHLTSummaryInfo))
	{
		LOG(AliHLTLog::kError, "AliHLTDDLConnectionTesterSubscriber::ProcessEvent", "Buffer Size")
			<< "The output buffer size is too small. Require at least"
			<< sizeof(AliHLTSummaryInfo) << " bytes, but the buffer is only "
			<< size << " bytes big."
			<< ENDLOG;
		return false;
	}
	
	// Initialise the summary structure.
	memset(summary, 0x0, sizeof(AliHLTSummaryInfo));  // This will automatically NULL terminate the strings.
	summary->fExpectedDDL = fExpectedDDL;
	summary->fReceivedDDL = 0xFFFFFFFF;
	size_t len = std::min(fExpectedPlug.length(), size_t(1023));
	for (size_t i = 0; i < len; i++)
		summary->fExpectedPlug[i] = fExpectedPlug[i];
	len = std::min(fReceivedPlug.length(), size_t(1023));
	for (size_t i = 0; i < len; i++)
		summary->fReceivedPlug[i] = fReceivedPlug[i];
	
	AliHLTEventDataType dataType;
	dataType.fID = UNKNOWN_DATAID;
	AliHLTEventDataOrigin dataOrigin;
	dataOrigin.fID = UNKNOWN_DATAORIGIN;
	AliUInt32_t dataSpec = 0;
	
	if (blockCnt > 1)
	{
		LOG(AliHLTLog::kWarning, "AliHLTDDLConnectionTesterSubscriber::ProcessEvent", "Data Check Error")
			<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
			<< ") has multiple data blocks, but only expect one."
			<< ENDLOG;
	}
	
	if (fExpectedPlug != fReceivedPlug)
	{
		LOG(AliHLTLog::kWarning, "AliHLTDDLConnectionTesterSubscriber::ProcessEvent", "Data Check Error")
			<< "The received DDL connection plug string \"" << fReceivedPlug.c_str()
			<< "\" does not correspond to the expected value of \"" << fExpectedPlug.c_str()
			<< "\"." << ENDLOG;
	}
		
	AliUInt32_t expectedSize = sizeof(AliUInt32_t) * 100;
	for (AliUInt32_t n = 0; n < blockCnt; n++)
	{
		if (n == 0)
		{
			// Grab the first block's data type, origin and specification to use
			// for our output data block.
			dataType = blocks[n].fDataType;
			dataOrigin = blocks[n].fDataOrigin;
			dataSpec = blocks[n].fDataSpecification;
		}
	
		if (blocks[n].fSize != expectedSize)
		{
			LOG(AliHLTLog::kError, "AliHLTDDLConnectionTesterSubscriber::ProcessEvent", "Data Check Error")
				<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
				<< "), data block 0x" << AliHLTLog::kHex << n << " (" << AliHLTLog::kDec << n
				<< ") has an incorrect size of " << AliHLTLog::kHex << blocks[n].fSize << " ("
				<< AliHLTLog::kDec << blocks[n].fSize
				<< ") bytes. It is not " << expectedSize << " bytes big as expected."
				<< ENDLOG;
		}
		
		AliUInt32_t* word = reinterpret_cast<AliUInt32_t*>(blocks[n].fData);
		AliUInt32_t wordCount = blocks[n].fSize / sizeof(AliUInt32_t);
		
		if (word[0] != 0x00000001)
		{
			LOG(AliHLTLog::kError, "AliHLTDDLConnectionTesterSubscriber::ProcessEvent", "Data Check Error")
				<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
				<< "), data block 0x" << AliHLTLog::kHex << n << " (" << AliHLTLog::kDec << n
				<< ") does not have the first word set to 0x00000001. Received 0x"
				<< AliHLTLog::kHex << word[0] << " (" << AliHLTLog::kDec << word[0] << ")."
				<< ENDLOG;
		}
		for (AliUInt32_t i = 1; i < wordCount; i++)
		{
			// Copy the received DDL number only if it was not set
			// yet, or if it is not the value we expected.
			if (summary->fReceivedDDL == 0xFFFFFFFF)
				summary->fReceivedDDL = word[i];
			if (word[i] != fExpectedDDL)
				summary->fReceivedDDL = word[i];
			
			if (word[i] != fExpectedDDL)
			{
				LOG(AliHLTLog::kError, "AliHLTDDLConnectionTesterSubscriber::ProcessEvent", "Data Check Error")
					<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
					<< "), data block 0x" << AliHLTLog::kHex << n << " (" << AliHLTLog::kDec << n
					<< ") does not contain the expected value for the DDL equipment ID. Received 0x"
					<< AliHLTLog::kHex << word[i] << " (" << AliHLTLog::kDec << word[i]
					<< ") but expected 0x" << AliHLTLog::kHex << fExpectedDDL << " ("
					<< AliHLTLog::kDec << fExpectedDDL << ")."
					<< ENDLOG;
				break;
			}
		}
	}
	
	AliHLTSubEventDescriptor::BlockData dataBlock;
	dataBlock.fShmKey.fShmType = kInvalidShmType;
	dataBlock.fShmKey.fKey.fID = (AliUInt64_t)-1;
	dataBlock.fOffset = 0;
	dataBlock.fSize = sizeof(AliHLTSummaryInfo);
	dataBlock.fProducerNode = fNodeID;
	dataBlock.fDataType = dataType;
	dataBlock.fDataOrigin = dataOrigin;
	dataBlock.fDataSpecification = dataSpec;
	dataBlock.fStatusFlags = 0;
	dataBlock.fAttributes[kAliHLTBlockDByteOrderAttributeIndex] = kAliHLTNativeByteOrder;
	dataBlock.fAttributes[kAliHLTBlockD64BitAlignmentAttributeIndex] = AliHLTDetermineUInt64Alignment();
	dataBlock.fAttributes[kAliHLTBlockD32BitAlignmentAttributeIndex] = AliHLTDetermineUInt32Alignment();
	dataBlock.fAttributes[kAliHLTBlockD16BitAlignmentAttributeIndex] = AliHLTDetermineUInt16Alignment();
	dataBlock.fAttributes[kAliHLTBlockD8BitAlignmentAttributeIndex] = AliHLTDetermineUInt8Alignment();
	dataBlock.fAttributes[kAliHLTBlockDFloatAlignmentAttributeIndex] = AliHLTDetermineDoubleAlignment();
	dataBlock.fAttributes[kAliHLTBlockDDoubleAlignmentAttributeIndex] = AliHLTDetermineFloatAlignment();
	outputBlocks.push_back(dataBlock);
	size = sizeof(AliHLTSummaryInfo);
	return true;
}
	    

/**
 * Component class for DDLConnectionTester, which is used to check if a DDL
 * connection is working and that it is being correctly identified.
 *
 * The command line options for this component are:
 *   -ddlid <number>
 *        This is the expected DDL equipment ID number for the DDL to which this
 *        component is connected.
 *   -expected-plug <string>
 *        Specifies the name of physical DDL plug connection we expect the DDL
 *        is connected to in the counting room CR2.
 *   -received-plug <string>
 *        Specifies the name of physical DDL plug connection for the DDL as given
 *        by the ECS system via the ECS proxy.
 */
class DDLConnectionTester : public AliHLTControlledDefaultProcessorComponent<AliHLTDDLConnectionTesterSubscriber>
{
public:
	
	typedef AliHLTControlledDefaultProcessorComponent<AliHLTDDLConnectionTesterSubscriber> BaseClass;

	DDLConnectionTester(const char* compName, int argc, char** argv) :
		BaseClass(compName, argc, argv),
		fExpectedDDL(0xFFFFFFFF),
		fExpectedPlug(NULL),
		fReceivedPlug(NULL)
	{
	}

protected:

	// Methods inherited from AliHLTControlledDefaultProcessorComponent:
	virtual const char* ParseCmdLine(int argc, char** argv, int& i, int& errorArg);
	virtual void PrintUsage(const char* errorStr, int errorArg, int errorParam);
	virtual void ShowConfiguration();
	virtual bool CheckConfiguration();
	virtual bool SetupComponents();
	
	AliUInt32_t fExpectedDDL;  // The expected DDL equipment ID number parsed from command line.
	const char* fExpectedPlug;  // The expected DDL link plug string parsed from command line.
	const char* fReceivedPlug;  // The received DDL link plug string parsed from command line.
};


const char* DDLConnectionTester::ParseCmdLine(int argc, char** argv, int& i, int& errorArg)
{
	if (strcmp(argv[i], "-ddlid" ) == 0)
	{
		if (i+1 >= argc)
			return "Missing expected DDL equipment ID number.";
		
		char* end = NULL;
		fExpectedDDL = (AliUInt32_t) strtoul(argv[i+1], &end, 0);
		if (*end != '\0')
		{
			errorArg = i+1;
			return "Error converting the DDL equipment ID to a number.";
		}
		i += 2;
		
		return NULL;
	}
	if (strcmp(argv[i], "-expected-plug" ) == 0)
	{
		if (i+1 >= argc)
			return "Missing expected plug string.";
		fExpectedPlug = argv[i+1];
		i += 2;
		return NULL;
	}
	if (strcmp(argv[i], "-received-plug" ) == 0)
	{
		if (i+1 >= argc)
			return "Missing received plug string.";
		fReceivedPlug = argv[i+1];
		i += 2;
		return NULL;
	}
	
	return BaseClass::ParseCmdLine(argc, argv, i, errorArg);
}


void DDLConnectionTester::PrintUsage(const char* errorStr, int errorArg, int errorParam)
{
	BaseClass::PrintUsage( errorStr, errorArg, errorParam );
	LOG(AliHLTLog::kError, "Processing Component", "Command line usage")
		<< "-ddlid <ddl-equipment-id>: Used to specify what the expected"
		" DDL equipment ID this component is connected to. (Optional)"
		<< ENDLOG;
	LOG(AliHLTLog::kError, "Processing Component", "Command line usage")
		<< "-expected-plug <plug-name-string>: Used to specify what the expected"
		" physical plug the DDL is connected to in counting room CR2."
		" (Mandatory if -received-plug set)"
		<< ENDLOG;
	LOG(AliHLTLog::kError, "Processing Component", "Command line usage")
		<< "-received-plug <plug-name-string>: Used to specify what plug name"
		" was received for the DDL from ECS via the ECS proxy."
		" (Mandatory if -expected-plug set)"
		<< ENDLOG;
}


void DDLConnectionTester::ShowConfiguration()
{
	BaseClass::ShowConfiguration();
	LOG(AliHLTLog::kInformational, "Processing Component", "Configuration")
		<< "The expected DDL equipment ID is set to: " << fExpectedDDL
		<< ENDLOG;
	LOG(AliHLTLog::kInformational, "Processing Component", "Configuration")
		<< "The expected DDL plug connection string is: "
		<< (fExpectedPlug != NULL ? fExpectedPlug : "(not set)" )
		<< ENDLOG;
	LOG(AliHLTLog::kInformational, "Processing Component", "Configuration")
		<< "The received DDL plug connection string given by ECS is: "
		<< (fReceivedPlug != NULL ? fReceivedPlug : "(not set)" )
		<< ENDLOG;
}


bool DDLConnectionTester::CheckConfiguration()
{
	// Test endianess of this machine during runtime and print warning if
	// it is not little endian.
	union
	{
		int dword;
		char byte[4];
	} endianTest;
	endianTest.dword = 0x1;
	if (endianTest.byte[0] != 0x1)
	{
		LOG(AliHLTLog::kWarning, "Processing Component", "Machine endianess")
			<< "!! WARNING !! This machine is not a little endian machine."
			" This code was not written to handle big endian machines. !! WARNING !!"
			<< ENDLOG;
	}

	// Check the rest of the configuration as per normal.
	if (not BaseClass::CheckConfiguration())
		return false;
	if (fExpectedPlug != NULL and fReceivedPlug == NULL)
	{
		LOG( AliHLTLog::kError, "Processing Component", "Check configuration" )
			<< "The expected DDL plug connection was set but the received"
			" plug string was not specified. Both parameters must be set."
			<< ENDLOG;
		return false;
	}
	if (fExpectedPlug == NULL and fReceivedPlug != NULL)
	{
		LOG( AliHLTLog::kError, "Processing Component", "Check configuration" )
			<< "The received DDL plug connection was set but the expected"
			" plug string was not specified. Both parameters must be set."
			<< ENDLOG;
		return false;
	}
	return true;
}


bool DDLConnectionTester::SetupComponents()
{
	if (not BaseClass::SetupComponents())
		return false;
	((AliHLTDDLConnectionTesterSubscriber*)fSubscriber)->SetParameters(fExpectedDDL, fExpectedPlug, fReceivedPlug);
	return true;
}


int main(int argc, char** argv)
{
	DDLConnectionTester procComp("DDLConnectionTester", argc, argv);
	procComp.Run();
	return 0;
}

