/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTRORC5Handler.hpp"
#include "AliHLTDDLHeader.hpp"
#include "AliHLTDDLHeaderData.h"
#include "AliHLTLog.hpp"
#include <netinet/in.h>
#include <errno.h>
#include <cmath>


#if 0
#define REPORTBUFFERSIZEWORKAROUND 1
#endif
#define REPORTBUFFERMAXSIZE 65520


//#define RORC_DEBUG_USE_INTERNAL_PATTERNGENERATOR
#ifdef RORC_DEBUG_USE_INTERNAL_PATTERNGENERATOR
#warning RORC_DEBUG_USE_INTERNAL_PATTERNGENERATOR is enabled
#endif

#if 0
#define SIMULATE
#endif

#ifdef SIMULATE
#warning Only simulating RORC accesses via signals. Disable the SIMULATE define two lines above to get real hardware access.
#include <signal.h>
static AliUInt32_t* gReportBufferReadPtr = 0;
static AliHLTRORC5Handler::AliVolatileUInt32_t* gAdminBufferReadPtr = 0;
static unsigned long gEventBufferBusAddress = 0;
static unsigned long gEventBufferSize = 0;
static AliHLTRORC5Handler::AliVolatileUInt8_t* gEventBufferPtr = 0;
static unsigned long gOffsetWriteEnd = 0;
static unsigned long gOffsetWrite = 0;
static AliUInt64_t gEventID = 0;
#define TRIGGER_SIGNAL SIGUSR1

#define SIMULATED_EVENT_32BIT_WORD_CNT 0x20000
#define SIMULATED_EVENT_SIZE_BYTES_REAL1 ( SIMULATED_EVENT_32BIT_WORD_CNT*sizeof(AliUInt32_t)+sizeof(AliUInt32_t)+( SIMULATED_EVENT_32BIT_WORD_CNT&1 ? 0 : sizeof(AliUInt32_t) ) )
#define BLOCKSIZE 128
#define BLOCKCOUNT ( (SIMULATED_EVENT_SIZE_BYTES_REAL1 / BLOCKSIZE) + ( (SIMULATED_EVENT_SIZE_BYTES_REAL1 % BLOCKSIZE) ? 1 : 0 ) )
#define SIMULATED_EVENT_SIZE_BYTES_REAL ( BLOCKCOUNT*BLOCKSIZE )

static AliHLTDDLHeaderDataV1 gCommonDataFormatHeader;

void TriggerSignalHandler( int )
    {
    bool wrote = false;
    LOG( AliHLTLog::kDebug, "TriggerSignalHandler", "Signal received" )
	<< "Received signal for simulated event generation - gOffsetWrite: "
	<< AliHLTLog::kDec << gOffsetWrite << " - gOffsetWriteEnd: "
	<< gOffsetWriteEnd << " - gReportBufferReadPtr: 0x"
	<< AliHLTLog::kHex << (unsigned long)gReportBufferReadPtr << " - gAdminBufferReadPtr: "
	<< (unsigned long)gAdminBufferReadPtr << " - gEventBufferBusAddress: 0x"
	<< gEventBufferBusAddress << " - gEventBufferPtr: 0x"
	<< (unsigned long)gEventBufferPtr << " - gEventID: 0x" << gEventID << AliHLTLog::kDec
	<< "/" << gEventID << " - SIMULATED_EVENT_32BIT_WORD_CNT: "
	<< SIMULATED_EVENT_32BIT_WORD_CNT << " - SIMULATED_EVENT_SIZE_BYTES_REAL: "
	<< SIMULATED_EVENT_SIZE_BYTES_REAL << "." << ENDLOG;
    unsigned long oldOffsetWrite = gOffsetWrite;
    if ( SIMULATED_EVENT_SIZE_BYTES_REAL > gEventBufferSize )
	return;
    // Events (event IDs) will be lost, if the buffer is full. This is in purpose as a marker for such conditions.
    AliHLTSetDDLHeaderEventID1( &gCommonDataFormatHeader, gEventID & 0x0000000000000FFFULL );
    AliHLTSetDDLHeaderEventID2( &gCommonDataFormatHeader, (gEventID & 0x0000000FFFFFF000ULL) >> 12 );
    AliHLTSetDDLHeaderMiniEventID( &gCommonDataFormatHeader, gEventID & 0x0000000000000FFFULL );
    gEventID = (gEventID+1) & 0x0000000FFFFFFFFFULL;
    if ( gReportBufferReadPtr && *(AliHLTRORC5Handler::AliVolatileUInt32_t*)(gReportBufferReadPtr)==0xFFFFFFFF )
	{

	if ( gOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL < gEventBufferSize )
	    {
	    // write pointer will  not make wrap around
	    if ( gOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL >= gOffsetWriteEnd && gOffsetWrite<gOffsetWriteEnd )
		{
		// write pointer will stay less than write end pointer, or write end pointer has made wrap around and write pointer will not.
		// update write end pointer
		gOffsetWriteEnd = ((unsigned long)(*((AliHLTRORC5Handler::AliVolatileUInt32_t*)gAdminBufferReadPtr))) - gEventBufferBusAddress;
		}
	    // try again
	    if ( gOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL < gOffsetWriteEnd || gOffsetWrite>gOffsetWriteEnd )
		{
		// write pointer will stay less than write end pointer, or write end pointer has made wrap around and write pointer will not.
		memcpy( (AliUInt8_t*)gEventBufferPtr+gOffsetWrite, &gCommonDataFormatHeader, sizeof(gCommonDataFormatHeader) );
		AliHLTRORC5Handler::AliVolatileUInt32_t* start = (AliHLTRORC5Handler::AliVolatileUInt32_t*)(gEventBufferPtr+gOffsetWrite+sizeof(gCommonDataFormatHeader));
		for ( unsigned long i=0; i<SIMULATED_EVENT_32BIT_WORD_CNT-sizeof(gCommonDataFormatHeader)/sizeof(AliUInt32_t); i++ )
		    *(start+i) = i;
		gOffsetWrite += SIMULATED_EVENT_SIZE_BYTES_REAL;
		wrote = true;
		}
	    }
	else
	    {
	    // write pointer will make wrap around.
	    if ( gOffsetWrite<gOffsetWriteEnd || gOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL>=gEventBufferSize+gOffsetWriteEnd )
		{
		// write end pointer has not made wrap around yet or write wrap around would go beyond wrapped write end pointer
		// update write end pointer
		gOffsetWriteEnd = ((unsigned long)(*((AliHLTRORC5Handler::AliVolatileUInt32_t*)gAdminBufferReadPtr))) - gEventBufferBusAddress;
		}
	    // try again
	    if ( gOffsetWrite>=gOffsetWriteEnd && gOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL<gEventBufferSize+gOffsetWriteEnd )
		{
		if ( gOffsetWrite+sizeof(gCommonDataFormatHeader) <= gEventBufferSize )
		    {
		    memcpy( (AliUInt8_t*)gEventBufferPtr+gOffsetWrite, &gCommonDataFormatHeader, sizeof(gCommonDataFormatHeader) );
		    AliHLTRORC5Handler::AliVolatileUInt32_t* start = (AliHLTRORC5Handler::AliVolatileUInt32_t*)(gEventBufferPtr+gOffsetWrite+sizeof(gCommonDataFormatHeader));
		    unsigned long firstWordCnt = (gEventBufferSize - (gOffsetWrite+sizeof(gCommonDataFormatHeader))) / sizeof(AliUInt32_t);
		    for ( unsigned long i=0; i<firstWordCnt; i++ )
			*(start+i) = i;
		    start = (AliHLTRORC5Handler::AliVolatileUInt32_t*)gEventBufferPtr;
		    for ( unsigned long i=0; i<SIMULATED_EVENT_32BIT_WORD_CNT-firstWordCnt-sizeof(gCommonDataFormatHeader)/sizeof(AliUInt32_t); i++ )
			*(start+i) = i+firstWordCnt;
		    }
		else
		    {
		    unsigned long firstPart = gOffsetWrite+sizeof(gCommonDataFormatHeader)-gEventBufferSize;
		    memcpy( (AliUInt8_t*)gEventBufferPtr+gOffsetWrite, &gCommonDataFormatHeader, firstPart );
		    memcpy( (AliUInt8_t*)gEventBufferPtr, ((AliUInt8_t*)&gCommonDataFormatHeader)+firstPart, sizeof(gCommonDataFormatHeader)-firstPart );
		    AliHLTRORC5Handler::AliVolatileUInt32_t* start = (AliHLTRORC5Handler::AliVolatileUInt32_t*)(gEventBufferPtr+sizeof(gCommonDataFormatHeader)-firstPart);
		    for ( unsigned long i=0; i<SIMULATED_EVENT_32BIT_WORD_CNT-sizeof(gCommonDataFormatHeader)/sizeof(AliUInt32_t); i++ )
			*(start+i) = i;
		    }
		gOffsetWrite = gOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL-gEventBufferSize;
		wrote = true;
		}
	    }

	//printf( "Force triggering event at address 0x%08lX\n", (unsigned long)gReportBufferReadPtr );
	if ( wrote )
	    {
	    *(AliHLTRORC5Handler::AliVolatileUInt32_t*)(gReportBufferReadPtr) = gEventBufferBusAddress+oldOffsetWrite;
	    *(((AliHLTRORC5Handler::AliVolatileUInt32_t*)gReportBufferReadPtr)+1) = gEventBufferBusAddress+oldOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL;
	    *(((AliHLTRORC5Handler::AliVolatileUInt32_t*)gReportBufferReadPtr)+2) = SIMULATED_EVENT_32BIT_WORD_CNT;
	    *(((AliHLTRORC5Handler::AliVolatileUInt32_t*)gReportBufferReadPtr)+3) = SIMULATED_EVENT_32BIT_WORD_CNT;
	    gReportBufferReadPtr += 4;
	    }
	}
    LOG( AliHLTLog::kDebug, "TriggerSignalHandler", "Event Written" )
	<< "Received signal for simulated event generation - gOffsetWrite: "
	<< AliHLTLog::kDec << gOffsetWrite << " - gOffsetWriteEnd: "
	<< gOffsetWriteEnd << " - gReportBufferReadPtr: 0x"
	<< AliHLTLog::kHex << (unsigned long)gReportBufferReadPtr << " - gAdminBufferReadPtr: "
	<< (unsigned long)gAdminBufferReadPtr << " - gEventBufferBusAddress: 0x"
	<< gEventBufferBusAddress << " - gEventBufferPtr: 0x"
	<< (unsigned long)gEventBufferPtr << " - gEventID: 0x" << gEventID << AliHLTLog::kDec
	<< "/" << gEventID << " - SIMULATED_EVENT_32BIT_WORD_CNT: "
	<< SIMULATED_EVENT_32BIT_WORD_CNT << " - SIMULATED_EVENT_SIZE_BYTES_REAL: "
	<< SIMULATED_EVENT_SIZE_BYTES_REAL << " - wrote: "
	<< ( wrote ? "true" : "false" ) << "." << ENDLOG;
    }
#endif


#if 0 // old
const AliUInt32_t AliHLTRORC5Handler::kEVTBUFPTR          = 0x00;
const AliUInt32_t AliHLTRORC5Handler::kEVTBUFSIZ          = 0x04;

const AliUInt32_t AliHLTRORC5Handler::kREPBUFPTR          = 0x08;
const AliUInt32_t AliHLTRORC5Handler::kREPBUFSIZ          = 0x0C;

const AliUInt32_t AliHLTRORC5Handler::kADMBUFPTRREAD      = 0x10;
const AliUInt32_t AliHLTRORC5Handler::kEVTBUFPTRREAD      = 0x14;

const AliUInt32_t AliHLTRORC5Handler::kLINKREG            = 0xC0;

const AliUInt32_t AliHLTRORC5Handler::kRDYRX              = 0x14;

const AliUInt32_t AliHLTRORC5Handler::kDMAPGENABLE        = 0x80;

const AliUInt32_t AliHLTRORC5Handler::kRESET              = 0x84;

const AliUInt32_t AliHLTRORC5Handler::kFIFORST            = 0x88;

const AliUInt32_t AliHLTRORC5Handler::kDIUENABLE          = 0xE8;

const AliUInt32_t AliHLTRORC5Handler::kENABLELINK         = 0x14;
const AliUInt32_t AliHLTRORC5Handler::kDISABLELINK        = 0xB4;
#else // new

const AliUInt32_t AliHLTRORC5Handler::kEVTBUFPTR          = 0x00;
const AliUInt32_t AliHLTRORC5Handler::kEVTBUFSIZ          = 0x04;

const AliUInt32_t AliHLTRORC5Handler::kREPBUFPTR          = 0x08;
const AliUInt32_t AliHLTRORC5Handler::kREPBUFSIZ          = 0x0C;

const AliUInt32_t AliHLTRORC5Handler::kADMBUFPTRREAD      = 0x10;
const AliUInt32_t AliHLTRORC5Handler::kEVTBUFPTRREAD      = 0x14;

const AliUInt32_t AliHLTRORC5Handler::kDMAENABLE = 0x1C;
const AliUInt32_t AliHLTRORC5Handler::kDIUENABLE = 0xE8;
const AliUInt32_t AliHLTRORC5Handler::kFIFOFLUSH = 0x18;
const AliUInt32_t AliHLTRORC5Handler::kEVENTCOUNTERRESET = 0xD4;
const AliUInt32_t AliHLTRORC5Handler::kENABLEFLOWCONTROL = 0xD0;


#endif

#define ORBIT_PERIOD_SECONDS_DOUBLE 1492.0

AliHLTRORC5Handler::AliHLTRORC5Handler( unsigned long dataBufferSize, unsigned long rorcBlockSize, bool twoLinks, int eventSlotsExp2 ):
    fEvents( eventSlotsExp2 ),
    fTwoLinks( twoLinks ),
    fNextInvalidEventID( kAliEventTypeCorruptID, 0 )
    {
#ifdef SIMULATE
    if ( rorcBlockSize != BLOCKSIZE )
	{
	LOG( AliHLTLog::kWarning, "AliHLTRORC5Handler::AliHLTRORC5Handler", "Simulation blocksize setting" )
	    << "Fixing simulated RORC block size to " << AliHLTLog::kDec << BLOCKSIZE << " B." << ENDLOG;
	rorcBlockSize = BLOCKSIZE;
	}
#endif
    fEventBufferSize = dataBufferSize;

    fReportBufferSize = (dataBufferSize/rorcBlockSize)*sizeof(AliUInt32_t)*4;
    if ( dataBufferSize % rorcBlockSize )
	fReportBufferSize += sizeof(AliUInt32_t)*4;
#ifdef REPORTBUFFERSIZEWORKAROUND
    if ( fReportBufferSize > REPORTBUFFERMAXSIZE )
      {
	LOG( AliHLTLog::kWarning, "AliHLTRORC5Handler::AliHLTRORC5Handler", "Data Buffer Size Truncating Workaround" )
	  << "Truncating data buffer size as workaround to "
	  << AliHLTLog::kDec << REPORTBUFFERMAXSIZE << " bytes. (From "
	  << AliHLTLog::kDec << fReportBufferSize << " (0x"
	  << AliHLTLog::kHex << fReportBufferSize << "))." << ENDLOG;
	fReportBufferSize = REPORTBUFFERMAXSIZE;
      }
#endif
    fAdminBufferSize = 4096;
    pthread_mutex_init( &fEventMutex, NULL );

    fDataBufferSize = fEventBufferSize+fReportBufferSize+fAdminBufferSize;
    if ( fTwoLinks )
	fDataBufferSize <<= 1;

    fEnumerateEventIDs = false;
    fNextEventID = 0;

    fLastRORCEventID = AliEventID_t();
    fAddEventIDCounter = (~(AliUInt64_t)0) << 36;
    fLastEventIDWrapAround.tv_sec = fLastEventIDWrapAround.tv_usec = 0;

    fHeaderSearch = true;
    fReportWordConsistencyCheck = false;
    fReportWordUseBlockSize = false;

    fTPCDataFix = false;

#ifdef SIMULATE
    signal( TRIGGER_SIGNAL, TriggerSignalHandler );
    memset( &gCommonDataFormatHeader, 0, sizeof(gCommonDataFormatHeader) );
    AliHLTSetDDLHeaderBlockLength( &gCommonDataFormatHeader, SIMULATED_EVENT_32BIT_WORD_CNT*sizeof(AliUInt32_t) );
    AliHLTSetDDLHeaderVersion( &gCommonDataFormatHeader, 1 );
#endif
#ifdef COMPARE_COUNTER_PATTERN_TAGGED
    fCompareHighTag = 0;
    fDoCompareCounterPattern = false;
#endif
    fRORCBlockSize = rorcBlockSize;
    fRORCBlock32bitWords = fRORCBlockSize / 4;
    fEventCounter = 0;
    }

AliHLTRORC5Handler::~AliHLTRORC5Handler()
    {
    pthread_mutex_destroy( &fEventMutex );
    }

int AliHLTRORC5Handler::Open( bool vendorDeviceIndexOrBusSlotFunction, AliUInt16_t pciVendorIDOrBus, AliUInt16_t pciDeviceIDOrSlot, AliUInt16_t pciDeviceIndexOrFunction, AliUInt16_t pciDeviceBarIndex )
    {
    char tmp[ 512+1 ];
    PSI_Status    status;

    if ( vendorDeviceIndexOrBusSlotFunction )
	{
	fBarID = "/dev/psi/vendor/"; // 0x%04hX/device/0x%04hX/0/base0
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fBarID += tmp;
	fBarID += "/device/";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fBarID += tmp;
	fBarID += "/";
	snprintf( tmp, 512, "%04X", pciDeviceIndexOrFunction );
	fBarID += tmp;
	fBarID += "/base";
	snprintf( tmp, 512, "%d", pciDeviceBarIndex );
	fBarID += tmp;
	
	fDataBufferID = "/dev/psi/bigphys/HLT-RORC-DMA-VDI-";
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fDataBufferID += tmp;
	fDataBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fDataBufferID += tmp;
	fDataBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIndexOrFunction );
	fDataBufferID += tmp;
	
	}
    else
	{
	fBarID = "/dev/psi/bus/"; // 0x%04hX/device/0x%04hX/0/base0
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fBarID += tmp;
	fBarID += "/slot/";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fBarID += tmp;
	fBarID += "/function/";
	snprintf( tmp, 512, "%04X", pciDeviceIndexOrFunction );
	fBarID += tmp;
	fBarID += "/base";
	snprintf( tmp, 512, "%d", pciDeviceBarIndex );
	fBarID += tmp;
	
	fDataBufferID = "/dev/psi/bigphys/HLT-RORC-DMA-BSF-";
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fDataBufferID += tmp;
	fDataBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fDataBufferID += tmp;
	fDataBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIndexOrFunction );
	fDataBufferID += tmp;
	
	}

#ifdef SIMULATE
    LOG( AliHLTLog::kWarning, "AliHLTRORC5Handler::Open", "SIMULATING RORC" )
	<< "SIMULATING RORC, NOT ACCESSING ANY REAL HARDWARE." << ENDLOG;
#endif
#ifndef SIMULATE
    status = PSI_openRegion( &fBarRegion, fBarID.c_str() );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Open", "Unable to open RORC device bar region" )
	    << "Unable to open RORC device bar " << AliHLTLog::kDec << pciDeviceBarIndex << " region '" << fBarID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif


    status = PSI_openRegion( &fDataBufferRegion, fDataBufferID.c_str() );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Open", "Unable to open data buffer shm region" )
	    << "Unable to open data buffer shm region '" << fDataBufferID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBarRegion );
	return EIO;
	}


    
    unsigned long cur_size = fDataBufferSize;

    status = PSI_sizeRegion( fDataBufferRegion, &cur_size );
    if ( status != PSI_OK && status != PSI_SIZE_SET )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Open", "Unable to set size for data buffer shm region" )
	    << "Unable to set size for data buffer shm region '" << fDataBufferID.c_str()
	    << "' to size " << AliHLTLog::kDec << fDataBufferSize << " (0x" << AliHLTLog::kHex
	    << fDataBufferSize << "): " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBarRegion );
	PSI_closeRegion( fDataBufferRegion );
	return EIO;
	}

    if ( cur_size != fDataBufferSize )
	{
	LOG( AliHLTLog::kInformational, "AliHLTRORC5Handler::Open", "Data buffer size changed" )
	    << "Data buffer size is now " << AliHLTLog::kDec << cur_size << " (0x" << AliHLTLog::kHex
	    << cur_size << ") instead of the specified " << AliHLTLog::kDec << fDataBufferSize << " (0x" << AliHLTLog::kHex
	    << fDataBufferSize << ")." << ENDLOG;
	fDataBufferSize = cur_size;
	fEventBufferSize = (AliUInt32_t)( (double)(fDataBufferSize-fAdminBufferSize) / (1.0+16.0/(double)fRORCBlockSize) );
	if ( fTwoLinks )
	    fEventBufferSize >>= 1;
	fReportBufferSize = (fEventBufferSize/fRORCBlockSize)*sizeof(AliUInt32_t)*4;
	if ( fEventBufferSize % fRORCBlockSize )
	    fReportBufferSize += sizeof(AliUInt32_t)*4;
	if ( fEventBufferSize+fReportBufferSize+fAdminBufferSize > fDataBufferSize )
	    {
	    LOG( AliHLTLog::kFatal, "AliHLTRORC5Handler::Open", "Internal Size Calculation Error" )
		<< "Internal error calculating new buffer sizes: fAdminBufferSize: " << AliHLTLog::kDec
		<< fAdminBufferSize << " - fReportBufferSize: " << fReportBufferSize
		<< " - fEventBufferSize: " << fEventBufferSize << " - fDataBufferSize: "
		<< fDataBufferSize << "." << ENDLOG;
	    return ENOMEM;
	    }
#ifdef REPORTBUFFERSIZEWORKAROUND
	if ( fReportBufferSize > REPORTBUFFERMAXSIZE )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTRORC5Handler::AliHLTRORC5Handler", "Report Buffer Size Truncating Workaround" )
		<< "Truncating report buffer size as workaround to "
		<< AliHLTLog::kDec << REPORTBUFFERMAXSIZE << " bytes. (From "
		<< AliHLTLog::kDec << fReportBufferSize << " (0x"
		<< AliHLTLog::kHex << fReportBufferSize << "))." << ENDLOG;
	    fReportBufferSize = REPORTBUFFERMAXSIZE;
	    }
#endif
	}
    else
	{
	LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::Open", "Data buffer size" )
	    << "Data buffer size is " << AliHLTLog::kDec << fDataBufferSize << " (0x" << AliHLTLog::kHex
	    << fDataBufferSize << ")." << ENDLOG;
	}
    //status = PSI_mapRegion( fDataBufferRegion, reinterpret_cast<void**>(&fReportBuffer) );
    status = PSI_mapRegion( fDataBufferRegion, (void**)&fDataBuffer );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Open", "Unable to map data buffer shm region" )
	    << "Unable to map data buffer shm region '" << fDataBufferID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBarRegion );
	PSI_closeRegion( fDataBufferRegion );
	return EIO;
	}
    status = PSI_getPhysAddress( (void*)fDataBuffer, (void**)&fDataBufferPhysAddress, reinterpret_cast<void**>(&fDataBufferBusAddress) );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Open", "Unable to obtain data buffer physical/bus address" )
	    << "Unable to obtain physical and bus address for data buffer: " 
	    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	//PSI_unmapRegion( fDataBufferRegion, reinterpret_cast<void*>(fReportBuffer) );
	PSI_unmapRegion( fDataBufferRegion, (void*)fDataBuffer );
	PSI_closeRegion( fBarRegion );
	PSI_closeRegion( fDataBufferRegion );
	return EIO;
	}
    unsigned long dataBufferOffset;
    if ( !fTwoLinks || pciDeviceBarIndex==0 )
	dataBufferOffset = 0;
    else
	dataBufferOffset = fDataBufferSize>>1;

    fMyDataBuffer = fDataBuffer+dataBufferOffset;
    fMyDataBufferPhysAddress = fDataBufferPhysAddress+dataBufferOffset;
    fMyDataBufferBusAddress = fDataBufferBusAddress+dataBufferOffset;

    LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::Open", "Data buffer" )
	<< "Data buffer: 0x" << AliHLTLog::kHex << (unsigned long)fDataBuffer
	<< " - My data buffer: 0x" << AliHLTLog::kHex << (unsigned long)fMyDataBuffer
	<< " - DataBuffer Phys: 0x"
	<< (unsigned long)fMyDataBufferPhysAddress << " - DataBuffer Bus: 0x" << (unsigned long)fMyDataBufferBusAddress
	<< " - DataBuffer size: 0x" << fDataBufferSize << " (" << AliHLTLog::kDec << fDataBufferSize << ")." << ENDLOG;

    status = PSI_mapDMA( fDataBufferRegion, fBarRegion, _bidirectional_, &fDataBufferDMAAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Open", "Unable to perform DMA mapping" )
	    << "Unable to perform DMA mapping: " 
	    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	//PSI_unmapRegion( fDataBufferRegion, reinterpret_cast<void*>(fReportBuffer) );
	PSI_unmapRegion( fDataBufferRegion, (void*)fDataBuffer );
	PSI_closeRegion( fBarRegion );
	PSI_closeRegion( fDataBufferRegion );
	return EIO;
	}
    LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::Open", "DMA mapping" )
	<< "Mapped DMA data buffer address: 0x" << AliHLTLog::kHex << (unsigned long)fDataBufferDMAAddress
	<< "." << ENDLOG;
    fMyDataBufferBusAddress = fDataBufferDMAAddress+dataBufferOffset;

    fEventBufferReadPtr = fEventBuffer = fMyDataBuffer;
    fEventBufferEnd = fEventBuffer + fEventBufferSize;

    fEventBufferPhysAddress = fMyDataBufferPhysAddress;
    fEventBufferBusAddress = fMyDataBufferBusAddress;

    MLUCString physmemEventBufferID;
    fPhysmemEventBufferID = "/dev/psi/physmem/"; // 0x%04hX/device/0x%04hX/0/base0
    snprintf( tmp, 512, "0x%016lX", fEventBufferPhysAddress );
    fPhysmemEventBufferID += tmp;
    status = PSI_openRegion( &fPhysmemEventBufferRegion, fPhysmemEventBufferID.c_str() );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Open", "Unable to open physmem event buffer shm region" )
	    << "Unable to open physmem event buffer shm region '" << fPhysmemEventBufferID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_unmapDMA( fDataBufferRegion );
	PSI_unmapRegion( fDataBufferRegion, (void*)fDataBuffer );
	PSI_closeRegion( fBarRegion );
	PSI_closeRegion( fDataBufferRegion );
	return EIO;
	}
    cur_size = fEventBufferSize;

    status = PSI_sizeRegion( fPhysmemEventBufferRegion, &cur_size );
    if ( status != PSI_OK && status != PSI_SIZE_SET )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Open", "Unable to set size for physmem event buffer shm region" )
	    << "Unable to set size for physmem buffer shm region '" << fPhysmemEventBufferID.c_str()
	    << "' to size " << AliHLTLog::kDec << fEventBufferSize << " (0x" << AliHLTLog::kHex
	    << fEventBufferSize << "): " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_unmapDMA( fDataBufferRegion );
	PSI_unmapRegion( fDataBufferRegion, (void*)fDataBuffer );
	PSI_closeRegion( fBarRegion );
	PSI_closeRegion( fDataBufferRegion );
	PSI_closeRegion( fPhysmemEventBufferRegion );
	return EIO;
	}
    

#ifdef SIMULATE
    gEventBufferPtr = fEventBufferReadPtr;
    gOffsetWriteEnd = fEventBufferSize;
    gEventBufferBusAddress = fEventBufferBusAddress;
    gEventBufferSize = fEventBufferSize;
#endif
    fEventBufferReadPtrPhysAddress = fEventBufferPhysAddress;
    fEventBufferEndPhysAddress = fEventBufferPhysAddress + fEventBufferSize;
    fEventBufferReadPtrBusAddress = fEventBufferBusAddress;
    fEventBufferEndBusAddress = fEventBufferBusAddress + fEventBufferSize;
    fEventBufferReleasedOffset = 0;
        
    LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::Open", "Event buffer pointers" )
	<< "EventBuffer: 0x" << AliHLTLog::kHex << (unsigned long)fEventBuffer << " - EventBuffer Phys: 0x"
	<< (unsigned long)fEventBufferPhysAddress << " - EventBuffer Bus: 0x" << (unsigned long)fEventBufferBusAddress
	<< " - EventBuffer size: 0x" << fEventBufferSize << " (" << AliHLTLog::kDec << fEventBufferSize << ")." << ENDLOG;
    

    fReportBuffer = fEventBuffer+fEventBufferSize;
    fReportBufferEnd = fReportBuffer + fReportBufferSize;
    fReportBufferReadPtr = fReportBuffer;
#ifdef SIMULATE
    gReportBufferReadPtr = (AliUInt32_t*)fReportBufferReadPtr;
#endif
    fReportBufferOldReadPtr = NULL;
    //status = PSI_getPhysAddress( reinterpret_cast<void*>(fReportBuffer), reinterpret_cast<void**>(&fReportBufferPhysAddress), reinterpret_cast<void**>(&fReportBufferBusAddress) );
    fReportBufferPhysAddress = fEventBufferPhysAddress + fEventBufferSize;
    fReportBufferBusAddress = fEventBufferBusAddress + fEventBufferSize;
    LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::Open", "Data buffer pointers" )
	<< "ReportBuffer: 0x" << AliHLTLog::kHex << (unsigned long)fReportBuffer << " - ReportBuffer Phys: 0x"
	<< (unsigned long)fReportBufferPhysAddress << " - ReportBuffer Bus: 0x" << (unsigned long)fReportBufferBusAddress
	<< "." << ENDLOG;


    fAdminBuffer = fReportBuffer + fReportBufferSize;
#ifdef SIMULATE
    gAdminBufferReadPtr = (AliVolatileUInt32_t*)fAdminBuffer;
#endif
    fAdminBufferPhysAddress = fReportBufferPhysAddress + fReportBufferSize;
    fAdminBufferBusAddress = fReportBufferBusAddress + fReportBufferSize;
    LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::Open", "Admin buffer pointers" )
	<< "AdminBuffer: 0x" << AliHLTLog::kHex << (unsigned long)fAdminBuffer << " - AdminBuffer Phys: 0x"
	<< (unsigned long)fAdminBufferPhysAddress << " - AdminBuffer Bus: 0x" << (unsigned long)fAdminBufferBusAddress
	<< "." << ENDLOG;

    return 0;
    }
	
int AliHLTRORC5Handler::Close()
    {
    PSI_unmapDMA( fDataBufferRegion );
    PSI_unmapRegion( fDataBufferRegion, (void*)fDataBuffer );
    PSI_closeRegion( fBarRegion );
    PSI_closeRegion( fDataBufferRegion );
    PSI_closeRegion( fPhysmemEventBufferRegion );
    return 0;
    }

int AliHLTRORC5Handler::SetEventBuffer( AliUInt8_t* /*dataBuffer*/, unsigned long /*dataBufferSize*/, bool first )
    {
    
    if ( first )
	*(AliVolatileUInt32_t*)fAdminBuffer = (AliUInt32_t)fEventBufferReadPtrBusAddress;

    return 0;
    }

int AliHLTRORC5Handler::InitializeRORC( bool first )
    {
    PSI_Status    status;
    unsigned long wr;

#if 0 // old
    // disable DMA
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAPGENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    /*
     * FiFo reset
     */
    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kFIFORST, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    /*
     * RORC reset, // do it twice
     */
    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_write( fBarRegion, (AliUInt32_t)kFIFORST, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_write( fBarRegion, (AliUInt32_t)kFIFORST, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    /*
     *   invalidate DMA and report buffer
     */
    if ( first )
	{
	memset( (void*)fReportBuffer, 0xff, fReportBufferSize );
	memset( (void*)fEventBuffer, 0, fEventBufferSize );
	memset( (void*)fAdminBuffer, 0xff, fAdminBufferSize );
	}
    
    /*
     *   fill DMA descriptor
     */
    //  dma_ctrl[1] = 0x2800;
    //  dma_ctrl[2] = 320;
    //     unsigned long dma_ctrl[4];
    //     dma_ctrl[1] = dmabuf_size;
    //     dma_ctrl[2] = rptbuf_size;
	
    //printf("fill DMA descriptor...\n");
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kEVTBUFPTR, _dword_, 4, (void*)&fEventBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_write( fBarRegion, (AliUInt32_t)kEVTBUFSIZ, _dword_, 4, (void*)&fEventBufferSize );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_write( fBarRegion, (AliUInt32_t)kREPBUFPTR, _dword_, 4, (void*)&fReportBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_write( fBarRegion, (AliUInt32_t)kREPBUFSIZ, _dword_, 4, (void*)&fReportBufferSize );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
	
	
    /*
     *  read pointer
     */
    status = PSI_write( fBarRegion, (AliUInt32_t)kEVTBUFPTRREAD, _dword_, 4, (void*)&fEventBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_write( fBarRegion, (AliUInt32_t)kADMBUFPTRREAD, _dword_, 4, (void*)&fAdminBufferBusAddress );  
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    *(AliVolatileUInt32_t*)fAdminBuffer = (AliUInt32_t)fEventBufferReadPtrBusAddress;
	
	
	
	
#if 0
    // Moved to ActivateRORC
    // enable DMA 
    wr = 0x00000041;
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAPGENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif



#else // new

    // disable DMA, do it three times, in the hopes that at least one will get through
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    // disable DIU interface
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDIUENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    unsigned long cnt=0;
    unsigned long rd=0;
    do
	{
	// flush FIFO
	wr = 0x00000001;
#ifndef SIMULATE
	status = PSI_write( fBarRegion, (AliUInt32_t)kFIFOFLUSH, _dword_, 4, (void*)&wr );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
		<< "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		<< ")." << ENDLOG;
	    return EIO;
	    }
#endif
	rd=0;
	// read FIFO status to verify flush (content can be reinserted after flush by internal FIFO on DIU...)
#ifndef SIMULATE
	status = PSI_read( fBarRegion, (AliUInt32_t)kFIFOFLUSH, _dword_, 4, (void*)&rd );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Read Error" )
		<< "PCI read error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		<< ")." << ENDLOG;
	    return EIO;
	    }
#endif
	}
    while ( rd != 0x6000000 && cnt++<1000 );
    
    
    // reset event counter logic
    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kEVENTCOUNTERRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    /*
     *   invalidate DMA and report buffer
     */
    if ( first )
	{
	memset( (void*)fReportBuffer, 0xff, fReportBufferSize );
	memset( (void*)fEventBuffer, 0, fEventBufferSize );
	memset( (void*)fAdminBuffer, 0xff, fAdminBufferSize );
	}
    
    /*
     *   fill DMA descriptor
     */
    //  dma_ctrl[1] = 0x2800;
    //  dma_ctrl[2] = 320;
    //     unsigned long dma_ctrl[4];
    //     dma_ctrl[1] = dmabuf_size;
    //     dma_ctrl[2] = rptbuf_size;
	
    //printf("fill DMA descriptor...\n");
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kEVTBUFPTR, _dword_, 4, (void*)&fEventBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_write( fBarRegion, (AliUInt32_t)kEVTBUFSIZ, _dword_, 4, (void*)&fEventBufferSize );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_write( fBarRegion, (AliUInt32_t)kREPBUFPTR, _dword_, 4, (void*)&fReportBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_write( fBarRegion, (AliUInt32_t)kREPBUFSIZ, _dword_, 4, (void*)&fReportBufferSize );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
	
	
    /*
     *  read pointer
     */
    status = PSI_write( fBarRegion, (AliUInt32_t)kEVTBUFPTRREAD, _dword_, 4, (void*)&fEventBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_write( fBarRegion, (AliUInt32_t)kADMBUFPTRREAD, _dword_, 4, (void*)&fAdminBufferBusAddress );  
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    *(AliVolatileUInt32_t*)fAdminBuffer = (AliUInt32_t)fEventBufferReadPtrBusAddress;

    // enable flow control
    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kENABLEFLOWCONTROL, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    // enable DIU interface
    wr = 0x00000003;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDIUENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    // DMA enable in Activate RORC

#endif

    return 0;
    }

int AliHLTRORC5Handler::DeinitializeRORC()
    {
    PSI_Status    status;
    unsigned long wr;

#if 0 // old

    // disable DMA
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAPGENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    /*
     * FiFo reset
     */
    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kFIFORST, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    /*
     * RORC reset, // do it twice
     */
    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif


#else // new

    // disable DMA, do it three times, in the hopes that at least one will get through
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif


    unsigned long cnt=0;
    unsigned long rd=0;
    do
	{
	// flush FIFO
	wr = 0x00000001;
#ifndef SIMULATE
	status = PSI_write( fBarRegion, (AliUInt32_t)kFIFOFLUSH, _dword_, 4, (void*)&wr );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC5Handler::DeinitializeRORC", "Write Error" )
		<< "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		<< ")." << ENDLOG;
	    return EIO;
	    }
#endif

	// read FIFO status to verify flush (content can be reinserted after flush by internal FIFO on DIU...)
	rd=0;
#ifndef SIMULATE
	status = PSI_read( fBarRegion, (AliUInt32_t)kFIFOFLUSH, _dword_, 4, (void*)&rd );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC5Handler::DeinitializeRORC", "Read Error" )
		<< "PCI read error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		<< ")." << ENDLOG;
	    return EIO;
	    }
#endif
	}
    while ( rd != 0x6000000 && cnt++<1000 );

    // reset event counter logic
    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kEVENTCOUNTERRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    // disable flow control
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kENABLEFLOWCONTROL, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    // disable DIU interface
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDIUENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif


#endif
    return 0;
    }

int AliHLTRORC5Handler::InitializeFromRORC( bool first )
    {
    if ( !first )
	{
	while ( *(AliVolatileUInt32_t*)fReportBufferReadPtr == 0xFFFFFFFF && fReportBufferReadPtr<fReportBufferEnd )
	    fReportBufferReadPtr += 4;
	if ( fReportBufferReadPtr>=fReportBufferEnd )
	    fReportBufferReadPtr = fReportBuffer;
#ifdef SIMULATE
	gReportBufferReadPtr = (AliUInt32_t*)fReportBufferReadPtr;
#endif
	fReportBufferOldReadPtr = NULL;

	// set physical address (signalling end of read data)
	fEventBufferReadPtrBusAddress = *(AliVolatileUInt32_t*)fAdminBuffer;
	fEventBufferReleasedOffset = fEventBufferReadPtrBusAddress-fEventBufferBusAddress;
	fEventBufferReadPtr = fEventBuffer+fEventBufferReleasedOffset;
	fEventBufferReadPtrPhysAddress = fEventBufferPhysAddress+fEventBufferReleasedOffset;
	if ( fEventBufferReadPtrBusAddress<fEventBufferBusAddress || fEventBufferReadPtrBusAddress>fEventBufferEndBusAddress )
	    {
	    fEventBufferReleasedOffset = 0;
	    fEventBufferReadPtrBusAddress = fEventBufferBusAddress;
	    fEventBufferReadPtr = fEventBuffer;
	    fEventBufferReadPtrPhysAddress = fEventBufferPhysAddress;
	    }
	*(AliVolatileUInt32_t*)fAdminBuffer = (AliUInt32_t)fEventBufferReadPtrBusAddress;
	}

    return 0;
    }

int AliHLTRORC5Handler::SetOptions( const vector<MLUCString>&, char*&, unsigned& )
    {
    return 0;
    }

void AliHLTRORC5Handler::GetAllowedOptions( vector<MLUCString>& options )
    {
    options.clear();
    }


int AliHLTRORC5Handler::ActivateRORC()
    {
    PSI_Status    status;
    unsigned long wr;
#if 0 // old 
#if 0 
    //Obsolete
    /*
     * Enable DDL link, allow events to arrive.
     */
    wr = kENABLELINK;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kLINKREG, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::ActivateRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    // enable DMA
    wr = 0x00000003;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAPGENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::ActivateRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    // enable DIU
    wr = 0x00000003;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDIUENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::ActivateRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

#else // new

    // enable DMA
    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::ActivateRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

#endif

    return 0;
    }

int AliHLTRORC5Handler::DeactivateRORC()
    {
    PSI_Status    status;
    unsigned long wr;
    
#if 0 // old    
#if 0 
    //Obsolete
    /*
     * Disable DDL link, stop events from arriving.
     */
    wr = kDISABLELINK;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kLINKREG, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::DeactivateRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    // disable DMA
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAPGENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::DeactivateRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif


#else // new

    // disable DMA, do it three times, in the hopes that at least one will get through
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::DeactivateRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::DeactivateRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::DeactivateRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

#endif


    return 0;
    }


int AliHLTRORC5Handler::PollForEvent( AliEventID_t& eventID, 
				      AliUInt64_t& dataOffset, AliUInt64_t& dataSize, AliUInt64_t& dataWrappedSize,
				      bool& dataDefinitivelyCorrupt, bool& eventSuppressed, AliUInt64_t& unsuppressedDataSize )
    {
    dataDefinitivelyCorrupt = false;
    eventSuppressed = false;
    unsuppressedDataSize = 0;
    unsigned long blocks;
    if ( fReportBufferOldReadPtr != fReportBufferReadPtr )
	{
	LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::PollForEvent", "Polling for event" )
	    << "Polling for event at location 0x" << AliHLTLog::kHex << (unsigned long)fReportBufferReadPtr
	    << " (bus address: 0x" << AliHLTLog::kHex 
	    << (unsigned long)(fReportBufferBusAddress)+(unsigned long)(fReportBufferReadPtr-fReportBuffer)
	    << ") (offset 0x" << (unsigned long)(fReportBufferReadPtr-fReportBuffer) << " ("
	    << AliHLTLog::kDec << (unsigned long)(fReportBufferReadPtr-fReportBuffer) 
	    << ")) in report buffer." << ENDLOG;
	fReportBufferOldReadPtr = fReportBufferReadPtr;
	}
    if ( *(AliVolatileUInt32_t*)fReportBufferReadPtr!=0xFFFFFFFF )
	{
	EventData ed;
	LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::PollForEvent", "Found event" )
	    << "(*(AliVolatileUInt32_t*)fReportBufferReadPtr): 0x" << AliHLTLog::kHex
	    << *((AliVolatileUInt32_t*)fReportBufferReadPtr) << ENDLOG;
	AliUInt32_t readAddr = (*(AliVolatileUInt32_t*)fReportBufferReadPtr);
	//AliVolatileUInt8_t* readAddrPtr = (AliVolatileUInt8_t*)(unsigned long)readAddr;
	AliVolatileUInt8_t* readAddrPtr = (AliVolatileUInt8_t*)( fEventBuffer + ( ((unsigned long)readAddr) - fEventBufferBusAddress ) );
	AliUInt32_t endAddr = (*(((AliVolatileUInt32_t*)fReportBufferReadPtr)+1));
	//AliVolatileUInt8_t* endAddrPtr = (AliVolatileUInt8_t*)(unsigned long)endAddr;
	AliUInt32_t dmaWords = (*(((AliVolatileUInt32_t*)fReportBufferReadPtr)+2)); 
	AliUInt32_t diuWords = (*(((AliVolatileUInt32_t*)fReportBufferReadPtr)+3)); 
	AliUInt32_t words = dmaWords;
	//AliUInt32_t dmaSize = endAddr - readAddr;
	if ( (unsigned long)readAddr != fEventBufferReadPtrBusAddress )
	    {
	    LOG( AliHLTLog::kFatal, "AliHLTRORC5Handler::PollForEvent", "Event Inconsistency" )
		<< "Error: Found inconsistency in event " << AliHLTLog::kDec << fEventCounter
		<< " (0x" << AliHLTLog::kHex << fEventCounter << ") for event buffer read ptr: fEventBufferReadPtrBusAddress 0x"
		<< AliHLTLog::kHex << (unsigned long)fEventBufferReadPtrBusAddress << " (" << AliHLTLog::kDec
		<< (unsigned long)fEventBufferReadPtrBusAddress << " - Address reported in event: 0x"
		<< AliHLTLog::kHex << (unsigned long)readAddr << " (" << AliHLTLog::kDec << (unsigned long)readAddr
		<< ") bytes - Continuing with value reported by card (Report words: 0x"
		<< AliHLTLog::kHex << readAddr << " (" << AliHLTLog::kDec << readAddr << ") - 0x"
		<< AliHLTLog::kHex << endAddr << " (" << AliHLTLog::kDec << endAddr << ") - 0x"
		<< AliHLTLog::kHex << dmaWords << " (" << AliHLTLog::kDec << dmaWords << ") - 0x"
		<< AliHLTLog::kHex << diuWords << " (" << AliHLTLog::kDec << diuWords << ")." << ENDLOG;
	    unsigned long oldOffset = ((unsigned long)fEventBufferReadPtrBusAddress)-((unsigned long)fEventBufferBusAddress);
	    unsigned long offset = ((unsigned long)readAddr)-((unsigned long)fEventBufferBusAddress);
	    fEventBufferReadPtr = fEventBuffer+offset;
	    fEventBufferReadPtrPhysAddress = fEventBufferPhysAddress+offset;
	    fEventBufferReadPtrBusAddress = fEventBufferBusAddress+offset;
	    //fEventBufferReadPtr = readAddrPtr;
	    // Add a block into the free space accounting for the mismatched block.
	    // Otherwise the block will be lost (not claimed by any event) and will not be freed.
	    // then at some point the read pointer for the RORC in the admin buffer cannot be updated
	    // any more and the system will stall.
	    EventData edTmp;
	    edTmp.fEventID = ~(AliUInt64_t)0;
	    edTmp.fOffset = oldOffset;
	    if ( oldOffset <= offset ) // Note: == should actually never happen...
		{
		edTmp.fSize = offset-oldOffset;
		edTmp.fWrappedSize = 0;
		}
	    else
		{
		edTmp.fSize = offset+fEventBufferSize-oldOffset;
		edTmp.fWrappedSize = offset;
		}
	    pthread_mutex_lock( &fEventMutex );
	    ReleaseBlock( &edTmp );
	    pthread_mutex_unlock( &fEventMutex );
	    }
	dataSize = words*4;
	// Firmware transfers two 32 bit words with lengths of transferred data in a 64 bit transfer, after the actual payload data
	// For transfers with odd number of payload data words one fillword is inserted between the last payload data word and the two length words
	// For transfers with even number of payload data words the two length words are directly appended after the last payload word
	if ( words & 1 )
	    words++;
	words += 2;
	blocks = words / fRORCBlock32bitWords; // One block is always the specified amount of words
	// Test for a partially filled block.
	if ( words % fRORCBlock32bitWords )
	    blocks++;
	AliUInt32_t calcEndAddr = readAddr+blocks*fRORCBlockSize;
	if ( calcEndAddr > fEventBufferEndBusAddress )
	    {
	    calcEndAddr = fEventBufferBusAddress + ( calcEndAddr - fEventBufferEndBusAddress );
	    }
	if ( endAddr != calcEndAddr )
	    {
	    LOG( AliHLTLog::kFatal, "AliHLTRORC5Handler::PollForEvent", "Event Inconsistency" )
		<< "Error: Found inconsistency in event " << AliHLTLog::kDec << fEventCounter
		<< " (0x" << AliHLTLog::kHex << fEventCounter << ") for event size: DMA Word count: " << AliHLTLog::kDec
		<< dmaWords << " (0x" << AliHLTLog::kHex << dmaWords << ") - end address derived from that: "
		<< AliHLTLog::kDec << calcEndAddr << "(0x" << AliHLTLog::kHex << calcEndAddr
		<< ") - end address reported by RORC: " << AliHLTLog::kDec << endAddr << "(0x"
		<< AliHLTLog::kHex << endAddr << ") (DIU Word count: " << AliHLTLog::kDec
		<< diuWords << " (0x" << AliHLTLog::kHex << diuWords 
		<< ")). Continuing with RORC reported DMA size (Report words: 0x"
		<< AliHLTLog::kHex << readAddr << " (" << AliHLTLog::kDec << readAddr << ") - 0x"
		<< AliHLTLog::kHex << endAddr << " (" << AliHLTLog::kDec << endAddr << ") - 0x"
		<< AliHLTLog::kHex << dmaWords << " (" << AliHLTLog::kDec << dmaWords << ") - 0x"
		<< AliHLTLog::kHex << diuWords << " (" << AliHLTLog::kDec << diuWords << ")."
		<< ENDLOG;
	    unsigned long dmaSize = endAddr-readAddr;
	    if ( endAddr<readAddr )
		dmaSize = fEventBufferSize+endAddr-readAddr; // fEventBufferEndBusAddress-readAddr + endAddr-fEventBufferBusAddress;
	    blocks = dmaSize / fRORCBlockSize;
	    dataSize = dmaSize; // Fo debugging write out all the data that was dma'd into memory.
	    dataDefinitivelyCorrupt = true;
	    }
	if ( dmaWords != diuWords )
	    {
	    LOG( AliHLTLog::kFatal, "AliHLTRORC5Handler::PollForEvent", "Event Inconsistency" )
		<< "Error: Found inconsistency in event " << AliHLTLog::kDec << fEventCounter
		<< " (0x" << AliHLTLog::kHex << fEventCounter << ") for event size: DIU Word count: " << AliHLTLog::kDec
		<< diuWords << " (0x" << AliHLTLog::kHex << diuWords << ") - DMA Word count: " << AliHLTLog::kDec
		<< dmaWords << " (0x" << AliHLTLog::kHex << dmaWords << "). Continuing with RORC reported DMA size (Report words: 0x"
		<< AliHLTLog::kHex << readAddr << " (" << AliHLTLog::kDec << readAddr << ") - 0x"
		<< AliHLTLog::kHex << endAddr << " (" << AliHLTLog::kDec << endAddr << ") - 0x"
		<< AliHLTLog::kHex << dmaWords << " (" << AliHLTLog::kDec << dmaWords << ") - 0x"
		<< AliHLTLog::kHex << diuWords << " (" << AliHLTLog::kDec << diuWords << ")."
		<< ENDLOG;
	    unsigned long dmaSize = endAddr-readAddr;
	    if ( endAddr<readAddr )
		dmaSize = fEventBufferSize+endAddr-readAddr; // fEventBufferEndBusAddress-readAddr + endAddr-fEventBufferBusAddress;
	    blocks = dmaSize / fRORCBlockSize;
	    dataSize = dmaSize; // Fo debugging write out all the data that was dma'd into memory.
	    dataDefinitivelyCorrupt = true;
	    }

	if ( dataDefinitivelyCorrupt && fTPCDataFix )
	    {
	    AliVolatileUInt32_t* epDMA = ((AliVolatileUInt32_t*)readAddrPtr)+dmaWords;
	    while ( (AliVolatileUInt8_t*)epDMA >= fEventBufferEnd )
		{
		epDMA = (AliVolatileUInt32_t*)( fEventBuffer + (unsigned long)epDMA-(unsigned long)fEventBufferEnd );
		}
	    AliVolatileUInt32_t* epDIU = ((AliVolatileUInt32_t*)readAddrPtr)+diuWords;
	    while ( (AliVolatileUInt8_t*)epDIU >= fEventBufferEnd )
		{
		epDIU = (AliVolatileUInt32_t*)( fEventBuffer + (unsigned long)epDIU-(unsigned long)fEventBufferEnd );
		}
	    AliVolatileUInt32_t *ep1, *ep2;
	    bool found1=false, found2=false;
	    unsigned long dataWords;
	    
	    if ( epDMA<epDIU )
		{
		ep1 = epDMA;
		ep2 = epDIU;
		}
	    else
		{
		ep1 = epDIU;
		ep2 = epDMA;
		}
	    if ( (AliVolatileUInt8_t*)ep1>fEventBuffer && (AliVolatileUInt8_t*)ep1<fEventBufferEnd && *ep1==diuWords && ((*(ep1-1)) & 0xFFF00000)==0x00000000 )
		{
		found1=true;
		dataWords = (ep1-(AliUInt32_t*)readAddrPtr);
		dataSize = dataWords*sizeof(AliUInt32_t);
		}
	    if ( (AliVolatileUInt8_t*)ep1==fEventBuffer && *ep1==diuWords && ((*(((AliVolatileUInt32_t*)fEventBufferEnd)-1)) & 0xFFF00000)==0x00000000 ) // Wrap around just before last word
                {
		found1=true;
		dataWords = (((AliVolatileUInt32_t*)fEventBufferEnd)+1-(AliVolatileUInt32_t*)readAddrPtr);
		dataSize = dataWords*sizeof(AliUInt32_t);
		}
	    if ( !found1 && (AliVolatileUInt8_t*)ep2>fEventBuffer && (AliVolatileUInt8_t*)ep2<fEventBufferEnd && *ep2==diuWords && ((*(ep2-1)) & 0xFFF00000)==0x00000000 )
		{
		found2=true;
		dataWords = (ep2-(AliUInt32_t*)readAddrPtr);
		dataSize = dataWords*sizeof(AliUInt32_t);
		}
	    if ( !found1 && (AliVolatileUInt8_t*)ep2==fEventBuffer && *ep2==diuWords && ((*(((AliVolatileUInt32_t*)fEventBufferEnd)-1)) & 0xFFF00000)==0x00000000 ) // Wrap around just before last word
		{
		found2=true;
		dataWords = (((AliVolatileUInt32_t*)fEventBufferEnd)+1-(AliVolatileUInt32_t*)readAddrPtr);
		dataSize = dataWords*sizeof(AliUInt32_t);
		}
	    if ( found1 || found2 )
		{
		LOG( AliHLTLog::kWarning, "AliHLTRORC5Handler::PollForEvent", "Possible TPC Fix" )
		<< "Possible fix for data size inconsistency found. Assuming event size to be "
		<< AliHLTLog::kDec << dataSize << "(0x" << AliHLTLog::kHex
		<< dataSize << ") - Found at " << (found1 ? "first" : "second")
		<< " possible position." << ENDLOG;
		}
	    }
#ifdef COMPARE_COUNTER_PATTERN_TAGGED
        if ( fDoCompareCounterPattern )
            {
            AliUInt32_t counterVal = ((AliUInt32_t)fCompareHighTag) << 28;
            AliUInt32_t eventBufferWordCnt = fEventBufferSize / sizeof(AliUInt32_t);
            for ( AliUInt32_t counter = 0; counter < diuWords; counter++ )
                {
                if ( ((AliUInt32_t*)fEventBufferReadPtr)[counter % eventBufferWordCnt] != counterVal )
                    {
                    LOG( AliHLTLog::kError, "AliHLTRORC5Handler::PollForEvent", "Data Check Error" )
                        << "Data Error: Offset 0x" << AliHLTLog::kHex << counter % eventBufferWordCnt << " (" << AliHLTLog::kDec
                        << counter % eventBufferWordCnt << ") Expected: 0x" << AliHLTLog::kHex << counterVal << " ("
                        << AliHLTLog::kDec << counterVal << ") - Found: 0x" << AliHLTLog::kHex
                        << ((AliUInt32_t*)fEventBufferReadPtr)[counter % eventBufferWordCnt] << " (" << AliHLTLog::kDec
                        << ((AliUInt32_t*)fEventBufferReadPtr)[counter % eventBufferWordCnt] << ")." << ENDLOG;
                    if ( (((AliUInt32_t*)fEventBufferReadPtr)[counter % eventBufferWordCnt] & 0xF0000000) == (((AliUInt32_t)fCompareHighTag) << 28) )
                        {
                        LOG( AliHLTLog::kError, "AliHLTRORC5Handler::PollForEvent", "Data Check Error" )
                            << "Resuming counter as 0x" << AliHLTLog::kHex << ((AliUInt32_t*)fEventBufferReadPtr)[counter % eventBufferWordCnt]
                            << " (" << AliHLTLog::kDec << ((AliUInt32_t*)fEventBufferReadPtr)[counter % eventBufferWordCnt] << ")."
                            << ENDLOG;
                        }
                    counterVal = ((AliUInt32_t*)fEventBufferReadPtr)[counter % eventBufferWordCnt];
                    }
                counterVal++;
                }
            }
#endif

	if ( fReportWordConsistencyCheck || fReportWordUseBlockSize )
	    {
	    unsigned long blocksReported;
	    blocksReported = (*(AliVolatileUInt32_t*)fReportBufferReadPtr) & 0x00001FFF;
	    if ( fReportWordConsistencyCheck && blocksReported != blocks )
		{
		LOG( AliHLTLog::kError, "AliHLTRORC5Handler::PollForEvent", "Inconsistent block size" )
		    << "Found event has inconsistent sizes: Data size from report word: 0x" << AliHLTLog::kHex << dataSize 
		    << " (" << AliHLTLog::kDec << dataSize << ") - Block count calculated from data size: 0x"
		    << AliHLTLog::kHex << blocks << " (" << AliHLTLog::kDec << blocks 
		    << ") - Block count from report word: 0x" << AliHLTLog::kHex << blocksReported
		    << " (" << AliHLTLog::kDec << blocksReported << ")." << ENDLOG;
		}
	    if ( fReportWordUseBlockSize )
	        {
		if ( blocks != blocksReported )
		    {
		    LOG( AliHLTLog::kError, "AliHLTRORC5Handler::PollForEvent", "Inconsistent block count" )
			<< "Found event has inconsistent block counts: Data size from report word: 0x" << AliHLTLog::kHex << dataSize 
			<< " (" << AliHLTLog::kDec << dataSize << ") - Block count calculated from data size: 0x"
			<< AliHLTLog::kHex << blocks << " (" << AliHLTLog::kDec << blocks 
			<< ") - Block count from report word: 0x" << AliHLTLog::kHex << blocksReported
			<< " (" << AliHLTLog::kDec << blocksReported << ")." << ENDLOG;
		    dataSize = blocksReported*fRORCBlockSize;
		    }
		blocks = blocksReported;
	        }
	    }
		
	ed.fSize = blocks*fRORCBlockSize;

	LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::PollForEvent", "Found event" )
	    << "Event found with " << AliHLTLog::kDec << dataSize << " (0x" << AliHLTLog::kHex << dataSize
	    << ") bytes and " << AliHLTLog::kDec << blocks << " blocks." << ENDLOG;

	bool showErrorData = false;

#ifdef RORC_DEBUG_USE_INTERNAL_PATTERNGENERATOR
	if ( dataSize != 1052 || blocks!=9 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC5Handler::PollForEvent", "Wrong event size" )
		<< "Found event has wrong event size " << AliHLTLog::kDec << dataSize << AliHLTLog::kHex
		<< " (0x" << dataSize << ") instead of 1052 or data block count " << AliHLTLog::kDec
		<< blocks << " instead of 9." << ENDLOG;
	    showErrorData = true;
	    }
#endif

#if 0
	AliUInt32_t tmp;
	//#define DDL_HEADER_BYTE_ORDER_LSB
#ifdef DDL_HEADER_BYTE_ORDER_LSB
	tmp = /*ntohl*/ ( (*(AliVolatileUInt32_t*)( fEventBufferReadPtr + 4 )) );
	if ( ((tmp & 0xFF000000) >> 24) != 1 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC5Handler::PollForEvent", "Unknown DDL header version number" )
		<< "Unknown DDL version header number found: " << AliHLTLog::kDec << ((tmp & 0xFF000000) >> 24)
		<< " - I only understand version 1. Now pretending it is version 1." << ENDLOG;
	    }
	tmp = /*ntohl*/ ( (*(AliVolatileUInt32_t*)( fEventBufferReadPtr + 8 )) );
	//eventID = ((tmp & 0x00FFFFFF) << 12);
	eventID = tmp & 0x00FFFFFF;
	eventID <<= 12;
	tmp = /*ntohl*/ ( (*(AliVolatileUInt32_t*)( fEventBufferReadPtr + 4 )) );
	eventID |=  (tmp & 0x00000FFF);

#else
	tmp = ntohl ( (*(AliVolatileUInt32_t*)( fEventBufferReadPtr + 4 )) );
	if ( ((tmp & 0xFF000000) >> 24) != 1 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC5Handler::PollForEvent", "Unknown DDL header version number" )
		<< "Unknown DDL version header number found: " << AliHLTLog::kDec << ((tmp & 0xFF000000) >> 24)
		<< " - I only understand version 1. Now pretending it is version 1." << ENDLOG;
	    }
	tmp = ntohl ( (*(AliVolatileUInt32_t*)( fEventBufferReadPtr + 8 )) );
	//eventID = ((tmp & 0x00FFFFFF) << 12);
	eventID = tmp & 0x00FFFFFF;
	eventID <<= 12;
	tmp = ntohl ( (*(AliVolatileUInt32_t*)( fEventBufferReadPtr + 4 )) );
	eventID |=  (tmp & 0x00000FFF);

#endif

#else

	AliHLTDDLHeader ddlHeader;
	bool corruptEventID = false;
	if ( fHeaderSearch )
	    {
	    if ( dataDefinitivelyCorrupt )
		{
		eventID = fNextInvalidEventID;
		++fNextInvalidEventID;
		}
	    else if ( dataSize < AliHLTMinDDLHeaderSize )
		{
		LOG( AliHLTLog::kWarning, "AliHLTRORC5Handler::PollForEvent", "Data smaller than DDL header" )
		    << "Data received is smaller than minimum DDL header size " << AliHLTLog::kDec 
		    << AliHLTMinDDLHeaderSize << "." << ENDLOG;
		dataDefinitivelyCorrupt = true;
		eventID = fNextInvalidEventID;
		++fNextInvalidEventID;
		corruptEventID = true;
		}
	    else if ( !ddlHeader.Set( (AliUInt32_t*)(AliVolatileUInt32_t*)fEventBufferReadPtr ) )
		{
		LOG( AliHLTLog::kError, "AliHLTRORC5Handler::PollForEvent", "Error reading DDL header" )
		    << "Error reading DDL header number. Unknown header version or out of memory. Header version: " 
		    << AliHLTLog::kDec 
		    << (unsigned)AliHLTGetDDLHeaderVersion(fEventBufferReadPtr) << " - header version byte index: "
		    << (unsigned)DDLHEADERVERSIONINDEX << " - header size: " << AliHLTGetDDLHeaderSize(fEventBufferReadPtr)
		    << ENDLOG;
		eventID = fNextInvalidEventID;
		++fNextInvalidEventID;
		dataDefinitivelyCorrupt = true;
		corruptEventID = true;
		}
	    else
		{
		LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::PollForEvent", "DDL header" )
		    << "Found DDL header version number. : " 
		    << AliHLTLog::kDec 
		    << (unsigned)AliHLTGetDDLHeaderVersion(fEventBufferReadPtr) << " - header version byte index: "
		    << (unsigned)DDLHEADERVERSIONINDEX << " - header size: " << AliHLTGetDDLHeaderSize(fEventBufferReadPtr)
		    << ENDLOG;
		LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::PollForEvent", "DDL header data" )
		    << "DDL header: Word 0: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(0)
		    << " - Word 1: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(1)
		    << " - Word 2: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(2)
		    << " - Word 3: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(3)
		    << " - Word 4: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(4)
		    << " - Word 5: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(5)
		    << " - Word 6: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(6)
		    << " - Word 7: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(7)
		    << ENDLOG;
		unsigned long headerSize = ddlHeader.GetHeaderSize();
		if ( headerSize > dataSize )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTRORC5Handler::PollForEvent", "Data smaller than DDL header" )
			<< "Data received is smaller than reported DDL header size " << AliHLTLog::kDec 
			<< headerSize << " - header version: " << (unsigned)AliHLTGetDDLHeaderVersion(fEventBufferReadPtr) 
			<< "." << ENDLOG;
		    eventID = fNextInvalidEventID;
		    ++fNextInvalidEventID;
		    dataDefinitivelyCorrupt = true;
		    }
		else
		    {
		    eventID = ddlHeader.GetEventID();
		    AliUInt32_t L1msg = ddlHeader.GetL1TriggerType();
		    if ( (L1msg & 0x1) && ( ((L1msg & 0x3C) >> 2) == 0xE ) )
			eventID.fType = kAliEventTypeStartOfRun;
		    else if ( (L1msg & 0x1) && ( ((L1msg & 0x3C) >> 2) == 0xF ) )
			eventID.fType = kAliEventTypeEndOfRun;
		    else
			eventID.fType = kAliEventTypeData;
		    }
		}
	    }

#endif

	if ( fEnumerateEventIDs )
	    {
	    LOG( AliHLTLog::kInformational, "AliHLTRORC5Handler::PollForEvent", "Enumerating found event" )
		<< "Enumerating found event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") to 0x" << AliHLTLog::kHex << fNextEventID << " (" << AliHLTLog::kDec
		<< fNextEventID << ")." << ENDLOG;
	    eventID = fNextEventID;
	    ++fNextEventID;
	    }
	else if ( !corruptEventID && !dataDefinitivelyCorrupt )
	    {
	    if ( fLastEventIDWrapAround.tv_sec == 0 &&
		 fLastEventIDWrapAround.tv_usec == 0 )
		gettimeofday( &fLastEventIDWrapAround, NULL );
	    if ( eventID.fNr<=fLastRORCEventID.fNr )
		{
		struct timeval now;
		gettimeofday( &now, NULL );
		double delta = (now.tv_sec-fLastEventIDWrapAround.tv_sec)*1000000+now.tv_usec-fLastEventIDWrapAround.tv_usec;
		unsigned long cnt = (unsigned long)round( delta/(ORBIT_PERIOD_SECONDS_DOUBLE*1000000.0) );
		if ( cnt<=0 )
		    cnt = 1;
		LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::PollForEvent", "Orbit counter" )
		    << "Orbit counter increased by " << AliHLTLog::kDec
		    << cnt << " (delta: " << delta << " microsec.)." << ENDLOG;
		AliEventID_t tmpID( kAliEventTypeUnknown, cnt );
		tmpID.fNr <<= 36;
		fAddEventIDCounter.fNr += tmpID.fNr;
		fLastEventIDWrapAround = now;
		//fAddEventIDCounter &= 0xFFFFFFFFFF000000ULL;
		}
	    fLastRORCEventID = eventID;
	    eventID.fNr |= fAddEventIDCounter.fNr;
	    }

	ed.fEventID = eventID;

	++fEventCounter;

	//ed.fEventID = eventID = ((*(AliVolatileUInt32_t*)fEventBufferReadPtr) & 0xFFFFFF00) >> 8;
	ed.fOffset = dataOffset = fEventBufferReadPtr - fEventBuffer;
	LOG( (showErrorData ? AliHLTLog::kError : AliHLTLog::kDebug), "AliHLTRORC5Handler::PollForEvent", "Event found" )
	    << "Found event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ")." << ENDLOG;
	LOG( (showErrorData ? AliHLTLog::kError : AliHLTLog::kDebug), "AliHLTRORC5Handler::PollForEvent", "Found event" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << "): Data: " << dataSize << " (0x" << AliHLTLog::kHex << dataSize
	    << ") bytes (" << AliHLTLog::kDec << blocks << " blocks) - data offset 0x" 
	    << AliHLTLog::kHex << dataOffset << " (" << AliHLTLog::kDec
	    << dataOffset << ") - report buffer location 0x" << AliHLTLog::kHex << (unsigned long)fReportBufferReadPtr
	    << " (offset 0x" << (unsigned long)(fReportBufferReadPtr-fReportBuffer) << " ("
	    << AliHLTLog::kDec << (unsigned long)(fReportBufferReadPtr-fReportBuffer) 
	    << "))." << ENDLOG;
	AliVolatileUInt8_t* oldEventBufferReadPtr = fEventBufferReadPtr;
	fEventBufferReadPtr += blocks*fRORCBlockSize;
	fEventBufferReadPtrPhysAddress += blocks*fRORCBlockSize;
	fEventBufferReadPtrBusAddress += blocks*fRORCBlockSize;
	ed.fWrappedSize = dataWrappedSize = 0;
	if ( fEventBufferReadPtr >= fEventBufferEnd )
	    {
	    ed.fWrappedSize = fEventBufferReadPtr - fEventBufferEnd;
	    if ( oldEventBufferReadPtr+dataSize >= fEventBufferEnd )
		dataWrappedSize = oldEventBufferReadPtr+dataSize - fEventBufferEnd;
	    LOG( (showErrorData ? AliHLTLog::kError : AliHLTLog::kDebug), "AliHLTRORC5Handler::PollForEvent", "Event wrapped" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") is wrapped. Wrapped block size: " << ed.fWrappedSize << " (0x"
		<< AliHLTLog::kHex << ed.fWrappedSize << "). Wrapped data size: " << dataWrappedSize << " (0x"
		<< AliHLTLog::kHex << dataWrappedSize << ")." << ENDLOG;
	    fEventBufferReadPtr = fEventBuffer+ed.fWrappedSize;
	    fEventBufferReadPtrPhysAddress = fEventBufferPhysAddress+ed.fWrappedSize;
	    fEventBufferReadPtrBusAddress = fEventBufferBusAddress+ed.fWrappedSize;
	    }
	pthread_mutex_lock( &fEventMutex );
	fEvents.Add( ed );
	pthread_mutex_unlock( &fEventMutex );

    
#if 1
	*(AliVolatileUInt32_t*)fReportBufferReadPtr = 0xFFFFFFFF;
	*(((AliVolatileUInt32_t*)fReportBufferReadPtr)+1) = 0xFFFFFFFF;
#endif
	fReportBufferReadPtr += 16;
	if ( fReportBufferReadPtr >= fReportBufferEnd )
	    fReportBufferReadPtr = fReportBuffer;
#ifdef SIMULATE
#if 0
	gReportBufferReadPtr = (AliUInt32_t*)fReportBufferReadPtr;
#endif
#endif
	LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::PollForEvent", "New buffer ptrs" )
	    << "New report buffer ptr: 0x" << AliHLTLog::kHex << (unsigned long)fReportBufferReadPtr
	    << " (offset " << AliHLTLog::kDec << (unsigned long)(fReportBufferReadPtr-fReportBuffer)
	    << " (0x" << AliHLTLog::kHex << (unsigned long)(fReportBufferReadPtr-fReportBuffer)
	    << ")) - New event buffer ptr: 0x" << AliHLTLog::kHex << (unsigned long)fEventBufferReadPtr
	    << " (offset " << AliHLTLog::kDec << (unsigned long)(fEventBufferReadPtr-fEventBuffer)
	    << " (0x" << AliHLTLog::kHex << (unsigned long)(fEventBufferReadPtr-fEventBuffer)
	    << ")) - New event buffer bus address: 0x" << AliHLTLog::kHex << (unsigned long)fEventBufferReadPtrBusAddress
	    << " (offset " << AliHLTLog::kDec << (unsigned long)(fEventBufferReadPtrBusAddress-fEventBufferBusAddress)
	    << " (0x" << AliHLTLog::kHex << (unsigned long)(fEventBufferReadPtrBusAddress-fEventBufferBusAddress)
	    << "))." << ENDLOG;


	return 0;
	}
    return EAGAIN;
    }

int AliHLTRORC5Handler::ReleaseEvent( AliEventID_t eventID )
    {
    unsigned long ndx;
    EventData* edp;
    LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::ReleaseEvent", "Releasing event" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << ") to be released." << ENDLOG;
    pthread_mutex_lock( &fEventMutex );
    if ( MLUCVectorSearcher<EventData,AliEventID_t>::FindElement( fEvents, &EventDataSearchFunc, eventID, ndx ) )
	{
	edp = fEvents.GetPtr( ndx );
	LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::ReleaseEvent", "Released event data" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << "): " << edp->fSize << " (0x" << AliHLTLog::kHex << edp->fSize
	    << ") bytes - wrapped size: " << AliHLTLog::kDec << edp->fWrappedSize
	    << " (0x" << AliHLTLog::kHex << edp->fWrappedSize << ") - offset 0x" 
	    << AliHLTLog::kHex << edp->fOffset << " (" << AliHLTLog::kDec
	    << edp->fOffset << ") - current event buffer release offset: 0x"
	    << AliHLTLog::kHex << fEventBufferReleasedOffset << " (" << AliHLTLog::kDec
	    << fEventBufferReleasedOffset << ")." << ENDLOG;
	ReleaseBlock( edp );
	fEvents.Remove( ndx );
	pthread_mutex_unlock( &fEventMutex );
	return 0;
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTRORC5Handler::ReleaseEvent", "Event not found" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") to be released could not be found in list. Giving up..." 
	    << ENDLOG;
	pthread_mutex_unlock( &fEventMutex );
	return EIO;
	}
    }


bool AliHLTRORC5Handler::ReleaseBlock( EventData* edp )
    {
    vector<BlockData>::iterator iter, end;
    if ( edp->fWrappedSize )
	edp->fSize -= edp->fWrappedSize;
    if ( fEventBufferReleasedOffset == edp->fOffset )
	{
	// The new block to be released follows directly after the 
	// point up to which the data is reported as released to the RORC.
	// So we can directly report this block as released to the RORC as well.
	if ( edp->fWrappedSize )
	    fEventBufferReleasedOffset = edp->fWrappedSize;
	else
	    fEventBufferReleasedOffset += edp->fSize;
	if ( fEventBufferReleasedOffset == fEventBufferSize )
	    fEventBufferReleasedOffset = 0;
	// Check to see whether we have any released blocks that are adjacent to
	// the newly released block. In that case we can update the RORCs read
	// pointer even further.
	iter = fBlocks.begin();
	while ( iter!=fBlocks.end() && iter->fOffset==fEventBufferReleasedOffset )
	    {
	    fEventBufferReleasedOffset += iter->fSize;
	    fBlocks.erase( iter );
	    if ( fEventBufferReleasedOffset >= fEventBufferSize )
		fEventBufferReleasedOffset = fEventBufferReleasedOffset-fEventBufferSize;
	    iter = fBlocks.begin();
	    }
	LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::ReleaseBlock", "New release offset" )
	    << "New event buffer release offset: 0x" << AliHLTLog::kHex 
	    << fEventBufferReleasedOffset << " (" << AliHLTLog::kDec
	    << fEventBufferReleasedOffset << ") - *(AliVolatileUInt32_t*)fAdminBuffer: "
	    << (unsigned long)(fEventBufferBusAddress+fEventBufferReleasedOffset) << "." << ENDLOG;
	*(AliVolatileUInt32_t*)fAdminBuffer = (unsigned long)(fEventBufferBusAddress+fEventBufferReleasedOffset);
	}
    else
	{
	// the block to be released is somewhere in the middle of the used region. 
	// So we have to keep track of it for further released when the RORC's reelase
	// address as advanced sufficiently.
	bool repeat = false;
	do
	    {
	    repeat = false;
	    iter = fBlocks.begin();
	    end = fBlocks.end();
	    if ( edp->fOffset < fEventBufferReleasedOffset )
		{
		LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::ReleaseBlock", "Wrapped block" )
		    << "Skipping for wrapped block part." << ENDLOG;
		if ( iter != end )
		    iter++;
		while ( iter != end )
		    {
		    if ( iter==end || (iter-1)->fOffset>iter->fOffset )
			break;
		    iter++;
		    }
		}
	    bool inserted = false;
	    BlockData bd;
	    // Find the right position (ordered by offset), taking into account wrap arounds
	    // and see if we have an adjacent block to merge with.
	    while ( iter != end )
		{
		if ( edp->fOffset+edp->fSize == iter->fOffset )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::ReleaseBlock", "Merging block" )
			<< "Merging block at beginning: (" << AliHLTLog::kDec
			<< edp->fOffset << "/" << edp->fSize << ") + (" << iter->fOffset
			<< "/" << iter->fSize << ") == (" << edp->fOffset << "/"
			<< iter->fSize+edp->fSize << ")." << ENDLOG;
		    iter->fOffset = edp->fOffset;
		    iter->fSize += edp->fSize;
		    inserted = true;
		    if ( iter!=fBlocks.begin() && (iter-1)->fOffset+(iter-1)->fSize==iter->fOffset )
			{
			LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::ReleaseBlock", "Merging block" )
			    << "Merging in preceeding block at beginning: (" << AliHLTLog::kDec
			    << (iter-1)->fOffset << "/" << (iter-1)->fSize << ") + (" << iter->fOffset
			    << "/" << iter->fSize << ") == (" << (iter-1)->fOffset << "/"
			    << iter->fSize+(iter-1)->fSize << ")." << ENDLOG;
			(iter-1)->fSize += iter->fSize;
			fBlocks.erase( iter );
			}
		    break;
		    }
		if ( iter->fOffset+iter->fSize == edp->fOffset )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::ReleaseBlock", "Merging block" )
			<< "Merging block at end: (" << AliHLTLog::kDec
			<< iter->fOffset << "/" << iter->fSize << ") + (" << edp->fOffset 
			<< "/" << edp->fSize << ") == (" << iter->fOffset << "/"
			<< iter->fSize+edp->fSize << ")." << ENDLOG;
		    iter->fSize += edp->fSize;
		    inserted = true;
		    if ( (iter+1)!=end && (iter+1)->fOffset==iter->fOffset+iter->fSize )
			{
			LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::ReleaseBlock", "Merging block" )
			    << "Merging in succeeding block at end: (" << AliHLTLog::kDec
			    << iter->fOffset << "/" << iter->fSize << ") + (" << (iter+1)->fOffset
			    << "/" << (iter+1)->fSize << ") == (" << iter->fOffset << "/"
			    << iter->fSize+(iter+1)->fSize << ")." << ENDLOG;
			iter->fSize += (iter+1)->fSize;
			fBlocks.erase( iter+1 );
			}
		    break;
		    }
		if ( edp->fOffset < iter->fOffset )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::ReleaseBlock", "Inserting block" )
			<< "Inserting block (" << AliHLTLog::kDec
			<< edp->fOffset << "/" << edp->fSize << ") before (" << iter->fOffset 
			<< "/" << iter->fSize << ")." << ENDLOG;
		    bd.fOffset = edp->fOffset;
		    bd.fSize = edp->fSize;
		    fBlocks.insert( iter, bd );
		    inserted = true;
		    break;
		    }
		iter++;
		}
	    if ( !inserted )
		{
		// Nothing found where to insert or merge, so we append it at the end.
		LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::ReleaseBlock", "Appending block" )
		    << "Appending block: (" << AliHLTLog::kDec
		    << edp->fOffset << "/" << edp->fSize 
		    << ") at end." << ENDLOG;
		bd.fOffset = edp->fOffset;
		bd.fSize = edp->fSize;
		fBlocks.insert( end, bd );
		}
	    if ( edp->fWrappedSize )
		{
		// If we had a wrapped block, we do the same for the wrapped around part 
		// at the beginning of the buffer as well.
		LOG( AliHLTLog::kDebug, "AliHLTRORC5Handler::ReleaseBlock", "Wrapping block" )
		    << "Wrapping block. (" << AliHLTLog::kDec
		    << edp->fOffset << "/" << edp->fSize << ") -> (0/"
		    << edp->fWrappedSize << ")." << ENDLOG;
		repeat = true;
		edp->fOffset = 0;
		edp->fSize = edp->fWrappedSize;
		edp->fWrappedSize = 0;
		}
	    }
	while ( repeat );
	}
    return true;
    }


void AliHLTRORC5Handler::GetEventBuffer( AliHLTShmID& shmID, unsigned long& offset, volatile uint8*& ptr, unsigned long& size )
    {
    shmID.fShmType = kPhysMemShmType;
    shmID.fKey.fID = fEventBufferPhysAddress;
    offset = 0;
    ptr = fEventBuffer;
    size = fEventBufferSize;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
