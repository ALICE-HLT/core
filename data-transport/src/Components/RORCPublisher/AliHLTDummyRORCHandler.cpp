/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTDummyRORCHandler.hpp"
#include "AliHLTLog.hpp"
#include <errno.h>

AliHLTDummyRORCHandler::AliHLTDummyRORCHandler( unsigned long evtBufferSize, int eventSlotsExp2 ):
    fEvents( eventSlotsExp2 )
    {
    fMaxEventCount = evtBufferSize/128;
    if ( eventSlotsExp2>=0 && fMaxEventCount > (unsigned long)(1<<eventSlotsExp2) )
	fMaxEventCount = (1<<eventSlotsExp2);
    fEventCount = 0;
    fEventCounter = 0;
    fEventBufferSize = evtBufferSize;
    if ( fEventBufferSize % 128 )
	fEventBufferSize -= (fEventBufferSize % 128);
    fActive = false;
    fOffset = 0;
    pthread_mutex_init( &fEventMutex, NULL );
    fBlockSize = 1052;
    fHeaderSearch = true;
    }

AliHLTDummyRORCHandler::~AliHLTDummyRORCHandler()
    {
    pthread_mutex_destroy( &fEventMutex );
    }


int AliHLTDummyRORCHandler::Open( bool, AliUInt16_t, AliUInt16_t, AliUInt16_t, AliUInt16_t )
    {
    return 0;
    }

int AliHLTDummyRORCHandler::Close()
    {
    return 0;
    }


int AliHLTDummyRORCHandler::SetEventBuffer( AliUInt8_t*, unsigned long, bool )
    {
    return 0;
    }


int AliHLTDummyRORCHandler::InitializeRORC( bool )
    {
    fEventCount = 0;
    fEventCounter = 0;
    return 0;
    }

int AliHLTDummyRORCHandler::DeinitializeRORC()
    {
    return 0;
    }


int AliHLTDummyRORCHandler::InitializeFromRORC( bool )
    {
    return 0;
    }


int AliHLTDummyRORCHandler::SetOptions( const vector<MLUCString>&, char*&, unsigned& )
    {
    return 0;
    }

void AliHLTDummyRORCHandler::GetAllowedOptions( vector<MLUCString>& )
    {
    }


int AliHLTDummyRORCHandler::ActivateRORC()
    {
    fActive = true;
    return 0;
    }

int AliHLTDummyRORCHandler::DeactivateRORC()
    {
    fActive = false;
    return 0;
    }


int AliHLTDummyRORCHandler::PollForEvent( AliEventID_t& eventID, 
					  AliUInt64_t& dataOffset, AliUInt64_t& dataSize, AliUInt64_t& dataWrappedSize, 
					  bool& dataDefinitivelyCorrupt, bool& eventSuppressed, AliUInt64_t& unsuppressedDataSize )
    {
    dataDefinitivelyCorrupt = false;
    eventSuppressed = false;
    if ( !fActive )
	return EAGAIN;
    if ( fEventCount>=fMaxEventCount )
	return EAGAIN;
    eventID = ++fEventCounter;
    dataOffset = fOffset;
    dataSize = fBlockSize;
    unsuppressedDataSize = dataSize;
    dataWrappedSize = 0;
    //fOffset += dataSize+headerSize;
    if ( fOffset+dataSize >= fEventBufferSize )
	{
	dataWrappedSize = fOffset+dataSize-fEventBufferSize;
	fOffset = dataWrappedSize;
	}
    else
	fOffset += dataSize;

    if ( fOffset % 128 )
	{
	fOffset += 128 - (fOffset % 128);
	}
    LOG( AliHLTLog::kDebug, "AliHLTDummyRORCHandler::PollForEvent", "Event Announced" )
	<< "'Polled' event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << ") with data size " << dataSize << " (" << dataWrappedSize
	<< " wrapped) bytes at offset " << dataOffset 
	<< ". New offset: " 
	<< fOffset << "." << ENDLOG;
    pthread_mutex_lock( &fEventMutex );
    fEvents.Add( eventID );
    fEventCount++;
    pthread_mutex_unlock( &fEventMutex );

    return 0;
    }


int AliHLTDummyRORCHandler::ReleaseEvent( AliEventID_t eventID )
    {
    unsigned long ndx;
    pthread_mutex_lock( &fEventMutex );
    if ( MLUCVectorSearcher<AliEventID_t,AliEventID_t>::FindElement( fEvents, &EventDataSearchFunc, eventID, ndx ) )
	{
	fEvents.Remove( ndx );
	fEventCount--;
	LOG( AliHLTLog::kDebug, "AliHLTDummyRORCHandler::ReleaseEvent", "Releasing event" )
	    << "Releasing event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ")." << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTDummyRORCHandler::ReleaseEvent", "Cannot find event" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") to be released could not be found." << ENDLOG;
	}
    pthread_mutex_unlock( &fEventMutex );
    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
