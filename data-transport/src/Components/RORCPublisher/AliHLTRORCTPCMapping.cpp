/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTRORCTPCMapping.hpp"
#include "AliHLTRORCHandlerInterface.hpp"
#include <cstdio>
#include <cerrno>
#include <fstream>
#include <vector>
#include <iostream>
#include <cstdlib>

static const unsigned gkPatchStartRow[] = { 0, 30, 63, 90, 117, 139 };


AliHLTRORCTPCMapping::AliHLTRORCTPCMapping( unsigned patchNr ):
    fPatchNr( patchNr ),
    fLastError( "Success" )
    {
    memset( fConfigWords, 0, gkConfigWordCnt*sizeof(fConfigWords[0]) );
    }

AliHLTRORCTPCMapping::~AliHLTRORCTPCMapping()
    {
    }

int AliHLTRORCTPCMapping::Read( const char* filename )
    {
    if ( !filename )
	{
	fLastError = "Invalid filename pointer";
	return EFAULT;
	}
    std::ifstream infile( filename );
    if ( infile.bad() or infile.fail() )
        {
	fLastError = "Error opening file";
	return EIO;
        }
    const unsigned bufSize = 4096;
    char buf[bufSize];
    MLUCString line;
    unsigned long lineCnt=0;
    while ( infile.good() )
	{
	line = "";
	unsigned count=0;
	do
	    {
	    infile.getline( buf, bufSize );
	    count = infile.gcount();
	    if ( count>0 )
		line += buf;
	    }
	while ( count==bufSize-1 && infile.fail() && !infile.eof() && !infile.bad() ); // fail bit set if function stops extracing because buffer size limit is reached...
	++lineCnt;
	if ( line.Length()<=0 )
	    continue;
	char lineStr[1024];
	snprintf( lineStr, 1024, "%lu", lineCnt );
	std::vector<MLUCString> tokens;
	line.Split( tokens, " \t" );
	char* cpErr=NULL;
	if ( tokens.size()==0 )
	    continue;
	if ( tokens.size()<2 )
	    {
	    fLastError = "Invalid format (missing row number or pad count) in line ";
	    fLastError += lineStr;
	    return EINVAL;
	    }
	unsigned long rowNr = strtoul( tokens[0].c_str(), &cpErr, 0 ); // first word is number of this row
	if ( *cpErr!='\0' )
	    {
	    fLastError = "Invalid row number '";
	    fLastError += tokens[0];
	    fLastError += "' in line ";
	    fLastError += lineStr;
	    return EINVAL;
	    }
	rowNr -= gkPatchStartRow[fPatchNr];
	unsigned long padCnt = strtoul( tokens[1].c_str(), &cpErr, 0 ); // second word is number of pads in this row
	if ( *cpErr!='\0' )
	    {
	    fLastError = "Invalid pad count number '";
	    fLastError += tokens[1];
	    fLastError += "' in line ";
	    fLastError += lineStr;
	    return EINVAL;
	    }
	if ( padCnt+2 != tokens.size() )
	    {
	    fLastError = "Pad count does not match number of tokens in line ";
	    fLastError += lineStr;
	    return EINVAL;
	    }
	unsigned long lastHWAddress = ~(0UL);
	bool active = true; // Currently all channels are always active
	AliUInt32_t  gainCalib = (1 << 12); // Gain calib identical for all pads currently, 1.0 as 13 bit fixed point, with 1 bit position before decimal point
	for ( unsigned pad=0; pad<padCnt; ++pad )
	    {
	    char padStr[1024];
	    snprintf( padStr, 1024, "%d", pad );
	    unsigned long hwAddress = strtoul( tokens[pad+2].c_str(), &cpErr, 0 );
	    if ( *cpErr!='\0' )
		{
		fLastError = "Invalid hardware address '";
		fLastError += tokens[1];
		fLastError += "' for pad ";
		fLastError += padStr;
		fLastError += " in line ";
		fLastError += lineStr;
		return EINVAL;
		}
	    unsigned long patchNr = (hwAddress & ~0xFFF) >> 12;
	    unsigned long lastPatchNr = (lastHWAddress & ~0xFFF) >> 12;
	    if ( patchNr != fPatchNr )
		{
		if ( lastHWAddress!=~(0UL) && lastPatchNr == fPatchNr ) 
		    {
#if 1
		    lastHWAddress &= 0xFFF;
		    fConfigWords[lastHWAddress] |= (1 << 14); // Change in patch means this pad (which we ignore since it is in the wrong patch) and the preceeding one, must be border pads
#endif
		    }
		lastHWAddress = hwAddress;
		continue;
		}
	    bool isBorderPad = (lastHWAddress == ~(0UL)) || ( ( (hwAddress>=lastHWAddress) ? (hwAddress-lastHWAddress) : (lastHWAddress-hwAddress) ) >= 2048 ); // First pad in a padrow is always border pad, or if hwaddress differs by at least 2048 from preceeding hw address.
	    hwAddress &= 0xFFF; // Clear patch nr from HW address, do this only after the difference has been checked above.
	    lastHWAddress &= 0xFFF;
	    AliUInt32_t configWord = (pad & 0xFF) | ( (rowNr & 0x3F) << 8 );
	    if ( active )
		configWord |= (1 << 15);
#if 1
	    if ( isBorderPad )
		configWord |= (1 << 14);
#endif
#if 1
	    if ( isBorderPad && lastHWAddress!=~(0UL) && lastPatchNr == fPatchNr )
		{
		// Only do this, if there was a previous pad and it belongs to this patch
		fConfigWords[lastHWAddress] |= (1 << 14); // This pad is a border pad, therefore the previous one has to have been one as well...
		}
#endif
	    configWord |= (gainCalib & 0x1FFF) << 16;
	    fConfigWords[hwAddress] = configWord;
	    lastHWAddress = hwAddress | (fPatchNr << 12);
	    }
	unsigned long lastPatchNr = (lastHWAddress & ~0xFFF) >> 12;
	if ( lastHWAddress!=~(0UL) && lastPatchNr == fPatchNr )
	    {
	    lastHWAddress &= 0xFFF;
#if 1
	    fConfigWords[lastHWAddress] |= (1 << 14); // Last pad is always border pad by definition
#endif
	    }
	}
    return 0;
    }
	


int AliHLTRORCTPCMapping::WriteData( AliHLTRORCHandlerInterface* interface )
    {
    if ( !interface )
	return EFAULT;
    int ret;
    for ( unsigned nn=0; nn<gkConfigWordCnt; nn++ )
	{
	ret = interface->WriteConfigWord( nn, fConfigWords[nn], true );
	if ( ret )
	    return ret;
	}
    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
