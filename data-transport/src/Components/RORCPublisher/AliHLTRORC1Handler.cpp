/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTRORC1Handler.hpp"
#include "AliHLTDDLHeader.hpp"
#include "AliHLTDDLHeaderData.h"
#include "AliHLTLog.hpp"
#include <netinet/in.h>
#include <errno.h>

//#define RORC_DEBUG_USE_INTERNAL_PATTERNGENERATOR
#ifdef RORC_DEBUG_USE_INTERNAL_PATTERNGENERATOR
#warning RORC_DEBUG_USE_INTERNAL_PATTERNGENERATOR is enabled
#endif

//#define SIMULATE
#ifdef SIMULATE
#warning Only simulating RORC accesses via signals. Disable the SIMULATE define two lines above to get real hardware access.
#include <signal.h>
static AliUInt32_t* gReportBufferReadPtr = 0;
#define TRIGGER_SIGNAL SIGUSR1
void TriggerSignalHandler( int )
    {
    if ( gReportBufferReadPtr && *(AliHLTRORC1Handler::AliVolatileUInt32_t*)(gReportBufferReadPtr)==0xFFFFFFFF )
	{
	//printf( "Force triggering event at address 0x%08lX\n", (unsigned long)gReportBufferReadPtr );
	*(AliHLTRORC1Handler::AliVolatileUInt32_t*)(gReportBufferReadPtr) = 0x00020000;
	}
    }
#endif


const AliUInt32_t AliHLTRORC1Handler::kEVTBUFPTR          = 0x00;
const AliUInt32_t AliHLTRORC1Handler::kEVTBUFSIZ          = 0x04;
const AliUInt32_t AliHLTRORC1Handler::kREPBUFPTR          = 0x08;
const AliUInt32_t AliHLTRORC1Handler::kLINKREG            = 0x78;
const AliUInt32_t AliHLTRORC1Handler::kRDYRX              = 0x14;
const AliUInt32_t AliHLTRORC1Handler::kDMAPGENABLE        = 0x18;
const AliUInt32_t AliHLTRORC1Handler::kRESET              = 0x1C;
const AliUInt32_t AliHLTRORC1Handler::kFIFORST            = 0x38;
const AliUInt32_t AliHLTRORC1Handler::kPRGPG1             = 0x20;
const AliUInt32_t AliHLTRORC1Handler::kPRGPG2             = 0x24;
const AliUInt32_t AliHLTRORC1Handler::kADMBUFPTRREAD      = 0x2C;
const AliUInt32_t AliHLTRORC1Handler::kEVTBUFPTRREAD      = 0x30;
const AliUInt32_t AliHLTRORC1Handler::kEOBTR              = 0x34;
const AliUInt32_t AliHLTRORC1Handler::kREPBUFSIZ          = 0x34;

const AliUInt32_t AliHLTRORC1Handler::kENABLELINK         = 0x14;
const AliUInt32_t AliHLTRORC1Handler::kDISABLELINK        = 0xB4;

AliHLTRORC1Handler::AliHLTRORC1Handler( unsigned long dataBufferSize, int eventSlotsExp2 ):
    fEvents( eventSlotsExp2 )
    {
    fReportBufferSize = (dataBufferSize/128)*sizeof(AliUInt32_t);
    if ( dataBufferSize % 128 )
	fReportBufferSize += sizeof(AliUInt32_t);
    fAdminBufferSize = 4096;
    pthread_mutex_init( &fEventMutex, NULL );

    fEnumerateEventIDs = false;
    fNextEventID = 0;

    fLastRORCEventID = AliEventID_t();
    fAddEventIDCounter = 0;

    fHeaderSearch = true;
    fReportWordConsistencyCheck = false;
    fReportWordUseBlockSize = false;

#ifdef COMPARE_SOFTWARE_EVENTBUFFERREADPTR_CARD_DMA_ADDR
    fBar0Ptr = NULL;
#endif
#ifdef SIMULATE
    signal( TRIGGER_SIGNAL, TriggerSignalHandler );
#endif
#ifdef COMPARE_COUNTER_PATTERN_TAGGED
    fCompareHighTag = 0;
    fDoCompareCounterPattern = false;
#endif
    }

AliHLTRORC1Handler::~AliHLTRORC1Handler()
    {
    pthread_mutex_destroy( &fEventMutex );
    }

int AliHLTRORC1Handler::Open( bool vendorDeviceIndexOrBusSlotFunction, AliUInt16_t pciVendorIDOrBus, AliUInt16_t pciDeviceIDOrSlot, AliUInt16_t pciDeviceIndexOrFunction, AliUInt16_t /*pciDeviceBarIndex*/ )
    {
    char tmp[ 512+1 ];
    PSI_Status    status;

    if ( vendorDeviceIndexOrBusSlotFunction )
	{
	fBar0ID = "/dev/psi/vendor/"; // 0x%04hX/device/0x%04hX/0/base0
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fBar0ID += tmp;
	fBar0ID += "/device/";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fBar0ID += tmp;
	fBar0ID += "/";
	snprintf( tmp, 512, "%04X", pciDeviceIndexOrFunction );
	fBar0ID += tmp;
	fBar0ID += "/base0";
	
	fReportBufferID = "/dev/psi/bigphys/HLT-RORC-Report-VDI-";
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIndexOrFunction );
	fReportBufferID += tmp;
	
	fAdminBufferID = "/dev/psi/bigphys/HLT-RORC-Admin-VDI-";
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fAdminBufferID += tmp;
	fAdminBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fAdminBufferID += tmp;
	fAdminBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIndexOrFunction );
	fAdminBufferID += tmp;
	}
    else
	{
	fBar0ID = "/dev/psi/bus/"; // 0x%04hX/device/0x%04hX/0/base0
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fBar0ID += tmp;
	fBar0ID += "/slot/";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fBar0ID += tmp;
	fBar0ID += "/function/";
	snprintf( tmp, 512, "%04X", pciDeviceIndexOrFunction );
	fBar0ID += tmp;
	fBar0ID += "/base0";
	
	fReportBufferID = "/dev/psi/bigphys/HLT-RORC-Report-BSF-";
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIndexOrFunction );
	fReportBufferID += tmp;
	
	fAdminBufferID = "/dev/psi/bigphys/HLT-RORC-Admin-BSF-";
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fAdminBufferID += tmp;
	fAdminBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fAdminBufferID += tmp;
	fAdminBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIndexOrFunction );
	fAdminBufferID += tmp;
	}
	
#ifdef SIMULATE
    LOG( AliHLTLog::kWarning, "AliHLTRORC1Handler::Open", "SIMULATING RORC" )
	<< "SIMULATING RORC, NOT ACCESSING ANY REAL HARDWARE." << ENDLOG;
#endif
#ifndef SIMULATE
    status = PSI_openRegion( &fBar0Region, fBar0ID.c_str() );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Open", "Unable to open RORC device bar 0 region" )
	    << "Unable to open RORC device bar 0 region '" << fBar0ID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

#ifdef COMPARE_SOFTWARE_EVENTBUFFERREADPTR_CARD_DMA_ADDR
#ifndef SIMULATE
    status = PSI_mapRegion( fBar0Region, (void**)&fBar0Ptr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Open", "Unable to map RORC device bar 0 region" )
	    << "Unable to map RORC device bar 0 region '" << fBar0ID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    status = PSI_openRegion( &fReportBufferRegion, fReportBufferID.c_str() );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Open", "Unable to open report buffer shm region" )
	    << "Unable to open report buffer shm region '" << fReportBufferID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBar0Region );
	return EIO;
	}

    status = PSI_openRegion( &fAdminBufferRegion, fAdminBufferID.c_str() );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Open", "Unable to open admin buffer shm region" )
	    << "Unable to open admin buffer shm region '" << fAdminBufferID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBar0Region );
	PSI_closeRegion( fReportBufferRegion );
	return EIO;
	}

    unsigned long cur_size = fReportBufferSize;

    status = PSI_sizeRegion( fReportBufferRegion, &cur_size );
    if ( status != PSI_OK && status != PSI_SIZE_SET )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Open", "Unable to set size for report buffer shm region" )
	    << "Unable to set size for report buffer shm region '" << fReportBufferID.c_str()
	    << "' to size " << AliHLTLog::kDec << fReportBufferSize << " (0x" << AliHLTLog::kHex
	    << fReportBufferSize << "): " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBar0Region );
	PSI_closeRegion( fReportBufferRegion );
	PSI_closeRegion( fAdminBufferRegion );
	return EIO;
	}

    if ( cur_size != fReportBufferSize )
	{
	LOG( AliHLTLog::kInformational, "AliHLTRORC1Handler::Open", "Report buffer size changed" )
	    << "Report buffer size is now " << AliHLTLog::kDec << cur_size << " (0x" << AliHLTLog::kHex
	    << cur_size << ") instead of the specified " << AliHLTLog::kDec << fReportBufferSize << " (0x" << AliHLTLog::kHex
	    << fReportBufferSize << ")." << ENDLOG;
	fReportBufferSize = cur_size;
	}
    else
	{
	LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::Open", "Report buffer size" )
	    << "Report buffer size is " << AliHLTLog::kDec << fReportBufferSize << " (0x" << AliHLTLog::kHex
	    << fReportBufferSize << ")." << ENDLOG;
	}
    //status = PSI_mapRegion( fReportBufferRegion, reinterpret_cast<void**>(&fReportBuffer) );
    status = PSI_mapRegion( fReportBufferRegion, (void**)&fReportBuffer );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Open", "Unable to map report buffer shm region" )
	    << "Unable to map report buffer shm region '" << fReportBufferID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBar0Region );
	PSI_closeRegion( fReportBufferRegion );
	PSI_closeRegion( fAdminBufferRegion );
	return EIO;
	}
    fReportBufferEnd = fReportBuffer + fReportBufferSize;
    fReportBufferReadPtr = fReportBuffer;
#ifdef SIMULATE
    gReportBufferReadPtr = (AliUInt32_t*)fReportBufferReadPtr;
#endif
    fReportBufferOldReadPtr = NULL;
    //status = PSI_getPhysAddress( reinterpret_cast<void*>(fReportBuffer), reinterpret_cast<void**>(&fReportBufferPhysAddress), reinterpret_cast<void**>(&fReportBufferBusAddress) );
    status = PSI_getPhysAddress( (void*)fReportBuffer, (void**)&fReportBufferPhysAddress, reinterpret_cast<void**>(&fReportBufferBusAddress) );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Open", "Unable to obtain report buffer physical/bus address" )
	    << "Unable to obtain physical and bus address for report buffer: " 
	    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	//PSI_unmapRegion( fReportBufferRegion, reinterpret_cast<void*>(fReportBuffer) );
	PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
	PSI_closeRegion( fBar0Region );
	PSI_closeRegion( fReportBufferRegion );
	PSI_closeRegion( fAdminBufferRegion );
	return EIO;
	}
    LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::Open", "Report buffer pointers" )
	<< "ReportBuffer: 0x" << AliHLTLog::kHex << (unsigned long)fReportBuffer << " - ReportBuffer Phys: 0x"
	<< (unsigned long)fReportBufferPhysAddress << " - ReportBuffer Bus: 0x" << (unsigned long)fReportBufferBusAddress
	<< "." << ENDLOG;

    cur_size = fAdminBufferSize;
    status = PSI_sizeRegion( fAdminBufferRegion, &cur_size );
    if ( status != PSI_OK && status != PSI_SIZE_SET )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Open", "Unable to set size for admin buffer shm region" )
	    << "Unable to set size for admin buffer shm region '" << fAdminBufferID.c_str()
	    << "' to size " << AliHLTLog::kDec << fAdminBufferSize << " (0x" << AliHLTLog::kHex
	    << fAdminBufferSize << "): " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	//PSI_unmapRegion( fReportBufferRegion, fReportBuffer );
	PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
	PSI_closeRegion( fBar0Region );
	PSI_closeRegion( fReportBufferRegion );
	PSI_closeRegion( fAdminBufferRegion );
	return EIO;
	}

    if ( cur_size != fAdminBufferSize )
	{
	LOG( AliHLTLog::kInformational, "AliHLTRORC1Handler::Open", "Admin buffer size changed" )
	    << "Admin buffer size is now " << AliHLTLog::kDec << cur_size << " (0x" << AliHLTLog::kHex
	    << cur_size << ") instead of the specified " << AliHLTLog::kDec << fAdminBufferSize << " (0x" << AliHLTLog::kHex
	    << fAdminBufferSize << ")." << ENDLOG;
	fAdminBufferSize = cur_size;
	}
    else
	{
	LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::Open", "Admin buffer size" )
	    << "Admin buffer size is " << AliHLTLog::kDec << fAdminBufferSize << " (0x" << AliHLTLog::kHex
	    << fAdminBufferSize << ")." << ENDLOG;
	}
    //status = PSI_mapRegion( fAdminBufferRegion, reinterpret_cast<void**>(&fAdminBuffer) );
    status = PSI_mapRegion( fAdminBufferRegion, (void**)&fAdminBuffer );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Open", "Unable to map admin buffer shm region" )
	    << "Unable to map admin buffer shm region '" << fAdminBufferID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	//PSI_unmapRegion( fReportBufferRegion, fReportBuffer );
	PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
	PSI_closeRegion( fBar0Region );
	PSI_closeRegion( fReportBufferRegion );
	PSI_closeRegion( fAdminBufferRegion );
	return EIO;
	}
    //status = PSI_getPhysAddress( fAdminBuffer, reinterpret_cast<void**>(&fAdminBufferPhysAddress), reinterpret_cast<void**>(&fAdminBufferBusAddress) );
    status = PSI_getPhysAddress( (void*)fAdminBuffer, (void**)&fAdminBufferPhysAddress, (void**)&fAdminBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Open", "Unable to obtain admin buffer physical/bus address" )
	    << "Unable to obtain physical and bus address for admin buffer: " 
	    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	//PSI_unmapRegion( fReportBufferRegion, fReportBuffer );
	PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
	//PSI_unmapRegion( fAdminBufferRegion, fAdminBuffer );
	PSI_unmapRegion( fAdminBufferRegion, (void*)fAdminBuffer );
	PSI_closeRegion( fBar0Region );
	PSI_closeRegion( fReportBufferRegion );
	PSI_closeRegion( fAdminBufferRegion );
	return EIO;
	}
    LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::Open", "Admin buffer pointers" )
	<< "AdminBuffer: 0x" << AliHLTLog::kHex << (unsigned long)fAdminBuffer << " - AdminBuffer Phys: 0x"
	<< (unsigned long)fAdminBufferPhysAddress << " - AdminBuffer Bus: 0x" << (unsigned long)fAdminBufferBusAddress
	<< "." << ENDLOG;

    return 0;
    }

int AliHLTRORC1Handler::Close()
    {
    //PSI_unmapRegion( fReportBufferRegion, fReportBuffer );
    PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
    //PSI_unmapRegion( fAdminBufferRegion, fAdminBuffer );
    PSI_unmapRegion( fAdminBufferRegion, (void*)fAdminBuffer );
    PSI_closeRegion( fBar0Region );
    PSI_closeRegion( fReportBufferRegion );
    PSI_closeRegion( fAdminBufferRegion );
    return 0;
    }

int AliHLTRORC1Handler::SetEventBuffer( AliUInt8_t* dataBuffer, unsigned long dataBufferSize, bool first )
    {
    PSI_Status    status;

    fEventBufferReadPtr = fEventBuffer = dataBuffer;
    fEventBufferSize = dataBufferSize;
    fEventBufferEnd = fEventBuffer + fEventBufferSize;
    //status = PSI_getPhysAddress( fEventBuffer, reinterpret_cast<void**>(&fEventBufferPhysAddress), reinterpret_cast<void**>(&fEventBufferBusAddress) );
    status = PSI_getPhysAddress( (void*)fEventBuffer, (void**)&fEventBufferPhysAddress, (void**)&fEventBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::SetEventBuffer", "Unable to obtain event buffer physical/bus address" )
	    << "Unable to obtain physical and bus address for event buffer: " 
	    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    fEventBufferReadPtrPhysAddress = fEventBufferPhysAddress;
    fEventBufferEndPhysAddress = fEventBufferPhysAddress + fEventBufferSize;
    fEventBufferReadPtrBusAddress = fEventBufferBusAddress;
    fEventBufferEndBusAddress = fEventBufferBusAddress + fEventBufferSize;
    fEventBufferReleasedOffset = 0;
        
    LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::SetEventBuffer", "Event buffer pointers" )
	<< "EventBuffer: 0x" << AliHLTLog::kHex << (unsigned long)fEventBuffer << " - EventBuffer Phys: 0x"
	<< (unsigned long)fEventBufferPhysAddress << " - EventBuffer Bus: 0x" << (unsigned long)fEventBufferBusAddress
	<< " - EventBuffer size: 0x" << fEventBufferSize << " (" << AliHLTLog::kDec << fEventBufferSize << ")." << ENDLOG;
    
    if ( first )
	*(AliVolatileUInt32_t*)fAdminBuffer = (AliUInt32_t)fEventBufferReadPtrBusAddress;

    return 0;
    }

int AliHLTRORC1Handler::InitializeRORC( bool first )
    {
    PSI_Status    status;
    unsigned long wr;

    /*
     * RORC reset, do it twice
     */
    wr = 0xaffed00f;
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    wr = 0xaffed00f;
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    /*
     * FiFo reset
     */
    wr = 0xaffed00f;
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kFIFORST, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    /*
     *   invalidate DMA and report buffer
     */
    if ( first )
	{
	memset( (void*)fReportBuffer, 0xff, fReportBufferSize );
	memset( (void*)fEventBuffer, 0, fEventBufferSize );
	memset( (void*)fAdminBuffer, 0xff, fAdminBufferSize );
	}
    
    /*
     *   fill DMA descriptor
     */
    //  dma_ctrl[1] = 0x2800;
    //  dma_ctrl[2] = 320;
    //     unsigned long dma_ctrl[4];
    //     dma_ctrl[1] = dmabuf_size;
    //     dma_ctrl[2] = rptbuf_size;
	
    //printf("fill DMA descriptor...\n");
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kEVTBUFPTR, _dword_, 4, (void*)&fEventBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_write( fBar0Region, (AliUInt32_t)kEVTBUFSIZ, _dword_, 4, (void*)&fEventBufferSize );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_write( fBar0Region, (AliUInt32_t)kREPBUFPTR, _dword_, 4, (void*)&fReportBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_write( fBar0Region, (AliUInt32_t)kREPBUFSIZ, _dword_, 4, (void*)&fReportBufferSize );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
	
	
    /*
     *  read pointer
     */
    status = PSI_write( fBar0Region, (AliUInt32_t)kEVTBUFPTRREAD, _dword_, 4, (void*)&fEventBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_write( fBar0Region, (AliUInt32_t)kADMBUFPTRREAD, _dword_, 4, (void*)&fAdminBufferBusAddress );  
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    *(AliVolatileUInt32_t*)fAdminBuffer = (AliUInt32_t)fEventBufferReadPtrBusAddress;
	
    /*
     *  program PG
     */
    wr = 0x000000ff;
    status = PSI_write( fBar0Region, (AliUInt32_t)kPRGPG1, _dword_, 4, (void*)&wr );  
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = 0x00000001;
    status = PSI_write( fBar0Region, (AliUInt32_t)kPRGPG2, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = 0x00000000;
    status = PSI_write( fBar0Region, (AliUInt32_t)kPRGPG2, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
	
	
    // enable DMA 
    wr = 0x00000041;
    status = PSI_write( fBar0Region, (AliUInt32_t)kDMAPGENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    return 0;
    }

int AliHLTRORC1Handler::DeinitializeRORC()
    {
    PSI_Status    status;
    unsigned long wr;

    /*
     * RORC reset, do it twice
     */
    wr = 0xaffed00f;
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    wr = 0xaffed00f;
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    return 0;
    }

int AliHLTRORC1Handler::InitializeFromRORC( bool first )
    {
    if ( !first )
	{
	while ( *(AliVolatileUInt32_t*)fReportBufferReadPtr == 0xFFFFFFFF && fReportBufferReadPtr<fReportBufferEnd )
	    fReportBufferReadPtr += 4;
	if ( fReportBufferReadPtr>=fReportBufferEnd )
	    fReportBufferReadPtr = fReportBuffer;
#ifdef SIMULATE
	gReportBufferReadPtr = (AliUInt32_t*)fReportBufferReadPtr;
#endif
	fReportBufferOldReadPtr = NULL;

	// set physical address (signalling end of read data)
	fEventBufferReadPtrBusAddress = *(AliVolatileUInt32_t*)fAdminBuffer;
	fEventBufferReleasedOffset = fEventBufferReadPtrBusAddress-fEventBufferBusAddress;
	fEventBufferReadPtr = fEventBuffer+fEventBufferReleasedOffset;
	fEventBufferReadPtrPhysAddress = fEventBufferPhysAddress+fEventBufferReleasedOffset;
	if ( fEventBufferReadPtrBusAddress<fEventBufferBusAddress || fEventBufferReadPtrBusAddress>fEventBufferEndBusAddress )
	    {
	    fEventBufferReleasedOffset = 0;
	    fEventBufferReadPtrBusAddress = fEventBufferBusAddress;
	    fEventBufferReadPtr = fEventBuffer;
	    fEventBufferReadPtrPhysAddress = fEventBufferPhysAddress;
	    }
	*(AliVolatileUInt32_t*)fAdminBuffer = (AliUInt32_t)fEventBufferReadPtrBusAddress;
	}

    return 0;
    }

int AliHLTRORC1Handler::SetOptions( const vector<MLUCString>&, char*&, unsigned& )
    {
    return 0;
    }

void AliHLTRORC1Handler::GetAllowedOptions( vector<MLUCString>& options )
    {
    options.clear();
    }


int AliHLTRORC1Handler::ActivateRORC()
    {
    PSI_Status    status;
    unsigned long wr;
    /*
     * Enable DDL link, allow events to arrive.
     */
    wr = kENABLELINK;
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kLINKREG, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::ActivateRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    return 0;
    }

int AliHLTRORC1Handler::DeactivateRORC()
    {
    PSI_Status    status;
    unsigned long wr;
    /*
     * Disable DDL link, stop events from arriving.
     */
    wr = kDISABLELINK;
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kLINKREG, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::DeactivateRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    return 0;
    }

int AliHLTRORC1Handler::PollForEvent( AliEventID_t& eventID, 
				      AliUInt64_t& dataOffset, AliUInt64_t& dataSize, AliUInt64_t& dataWrappedSize, 
				      bool& dataDefinitivelyCorrupt, bool& eventSuppressed, AliUInt64_t& unsuppressedDataSize )
    {
    dataDefinitivelyCorrupt = false;
    eventSuppressed = false;
    unsuppressedDataSize = 0;
    unsigned long blocks;
    if ( fReportBufferOldReadPtr != fReportBufferReadPtr )
	{
	LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::PollForEvent", "Polling for event" )
	    << "Polling for event at location 0x" << AliHLTLog::kHex << (unsigned long)fReportBufferReadPtr
	    << " (bus address: 0x" << AliHLTLog::kHex 
	    << (unsigned long)(fReportBufferBusAddress)+(unsigned long)(fReportBufferReadPtr-fReportBuffer)
	    << ") (offset 0x" << (unsigned long)(fReportBufferReadPtr-fReportBuffer) << " ("
	    << AliHLTLog::kDec << (unsigned long)(fReportBufferReadPtr-fReportBuffer) 
	    << ")) in report buffer." << ENDLOG;
	fReportBufferOldReadPtr = fReportBufferReadPtr;
	}
    if ( *(AliVolatileUInt32_t*)fReportBufferReadPtr!=0xFFFFFFFF )
	{
	EventData ed;
	LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::PollForEvent", "Found event" )
	    << "(*(AliVolatileUInt32_t*)fReportBufferReadPtr): 0x" << AliHLTLog::kHex
	    << *((AliVolatileUInt32_t*)fReportBufferReadPtr) << ENDLOG;
	unsigned long words = (*(AliVolatileUInt32_t*)fReportBufferReadPtr);
	dataSize = words*4;
	// Firmware transfers length of transferred data always in high word of 64 bit transfer
	// For transfers with odd number of payload data words the high word of the final 64 bit transfer is used
	// For transfers with even number of payload data words a further 64 bit transfer is appended with only the high word used, the low word of this transfer is undefined
	if ( words & 1 )
	    words++;
	else
	    words += 2;
	blocks = words / 32; // One block is always 128B or 32 32bit-words
	// Test for a partially filled block.
	if ( words & 31 )
	    blocks++;
#ifdef COMPARE_COUNTER_PATTERN_TAGGED
        if ( fDoCompareCounterPattern )
            {
            uint32 counterVal = ((uint32)fCompareHighTag) << 28;
            uint32 eventBufferWordCnt = fEventBufferSize / sizeof(uint32);
            for ( uint32 counter = 0; counter < words; counter++ )
                {
                if ( ((uint32*)fEventBufferReadPtr)[counter % eventBufferWordCnt] != counterVal )
                    {
                    LOG( AliHLTLog::kError, "AliHLTRORC1Handler::PollForEvent", "Data Check Error" )
                        << "Data Error: Offset 0x" << AliHLTLog::kHex << counter % eventBufferWordCnt << " (" << AliHLTLog::kDec
                        << counter % eventBufferWordCnt << ") Expected: 0x" << AliHLTLog::kHex << counterVal << " ("
                        << AliHLTLog::kDec << counterVal << ") - Found: 0x" << AliHLTLog::kHex
                        << ((uint32*)fEventBufferReadPtr)[counter % eventBufferWordCnt] << " (" << AliHLTLog::kDec
                        << ((uint32*)fEventBufferReadPtr)[counter % eventBufferWordCnt] << ")." << ENDLOG;
                    if ( (((uint32*)fEventBufferReadPtr)[counter % eventBufferWordCnt] & 0xF0000000) == (((uint32)fCompareHighTag) << 28) )
                        {
                        LOG( AliHLTLog::kError, "AliHLTRORC1Handler::PollForEvent", "Data Check Error" )
                            << "Resuming counter as 0x" << AliHLTLog::kHex << ((uint32*)fEventBufferReadPtr)[counter % eventBufferWordCnt]
                            << " (" << AliHLTLog::kDec << ((uint32*)fEventBufferReadPtr)[counter % eventBufferWordCnt] << ")."
                            << ENDLOG;
			counterVal = ((uint32*)fEventBufferReadPtr)[counter % eventBufferWordCnt];
                        }
                    }
                counterVal++;
                }
            }
#endif
	if ( fReportWordConsistencyCheck || fReportWordUseBlockSize )
	    {
	    unsigned long blocksReported;
	    blocksReported = (*(AliVolatileUInt32_t*)fReportBufferReadPtr) & 0x00001FFF;
	    if ( fReportWordConsistencyCheck && blocksReported != blocks )
		{
		LOG( AliHLTLog::kError, "AliHLTRORC1Handler::PollForEvent", "Inconsistent block size" )
		    << "Found event has inconsistent sizes: Data size from report word: 0x" << AliHLTLog::kHex << dataSize 
		    << " (" << AliHLTLog::kDec << dataSize << ") - Block count calculated from data size: 0x"
		    << AliHLTLog::kHex << blocks << " (" << AliHLTLog::kDec << blocks 
		    << ") - Block count from report word: 0x" << AliHLTLog::kHex << blocksReported
		    << " (" << AliHLTLog::kDec << blocksReported << ")." << ENDLOG;
		}
	    if ( fReportWordUseBlockSize )
	        {
		if ( blocks != blocksReported )
		    {
		    LOG( AliHLTLog::kError, "AliHLTRORC1Handler::PollForEvent", "Inconsistent block count" )
			<< "Found event has inconsistent block counts: Data size from report word: 0x" << AliHLTLog::kHex << dataSize 
			<< " (" << AliHLTLog::kDec << dataSize << ") - Block count calculated from data size: 0x"
			<< AliHLTLog::kHex << blocks << " (" << AliHLTLog::kDec << blocks 
			<< ") - Block count from report word: 0x" << AliHLTLog::kHex << blocksReported
			<< " (" << AliHLTLog::kDec << blocksReported << ")." << ENDLOG;
		    dataSize = blocksReported*128;
		    }
		blocks = blocksReported;
	        }
	    }
		
	ed.fSize = blocks*128;

	LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::PollForEvent", "Found event" )
	    << "Event found with " << AliHLTLog::kDec << dataSize << " (0x" << AliHLTLog::kHex << dataSize
	    << ") bytes and " << AliHLTLog::kDec << blocks << " blocks." << ENDLOG;

	bool showErrorData = false;

#ifdef RORC_DEBUG_USE_INTERNAL_PATTERNGENERATOR
	if ( dataSize != 1052 || blocks!=9 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC1Handler::PollForEvent", "Wrong event size" )
		<< "Found event has wrong event size " << AliHLTLog::kDec << dataSize << AliHLTLog::kHex
		<< " (0x" << dataSize << ") instead of 1052 or data block count " << AliHLTLog::kDec
		<< blocks << " instead of 9." << ENDLOG;
	    showErrorData = true;
	    }
#endif

#if 0
	AliUInt32_t tmp;
	//#define DDL_HEADER_BYTE_ORDER_LSB
#ifdef DDL_HEADER_BYTE_ORDER_LSB
	tmp = /*ntohl*/ ( (*(AliVolatileUInt32_t*)( fEventBufferReadPtr + 4 )) );
	if ( ((tmp & 0xFF000000) >> 24) != 1 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC1Handler::PollForEvent", "Unknown DDL header version number" )
		<< "Unknown DDL version header number found: " << AliHLTLog::kDec << ((tmp & 0xFF000000) >> 24)
		<< " - I only understand version 1. Now pretending it is version 1." << ENDLOG;
	    }
	tmp = /*ntohl*/ ( (*(AliVolatileUInt32_t*)( fEventBufferReadPtr + 8 )) );
	//eventID = ((tmp & 0x00FFFFFF) << 12);
	eventID = tmp & 0x00FFFFFF;
	eventID <<= 12;
	tmp = /*ntohl*/ ( (*(AliVolatileUInt32_t*)( fEventBufferReadPtr + 4 )) );
	eventID |=  (tmp & 0x00000FFF);

#else
	tmp = ntohl ( (*(AliVolatileUInt32_t*)( fEventBufferReadPtr + 4 )) );
	if ( ((tmp & 0xFF000000) >> 24) != 1 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC1Handler::PollForEvent", "Unknown DDL header version number" )
		<< "Unknown DDL version header number found: " << AliHLTLog::kDec << ((tmp & 0xFF000000) >> 24)
		<< " - I only understand version 1. Now pretending it is version 1." << ENDLOG;
	    }
	tmp = ntohl ( (*(AliVolatileUInt32_t*)( fEventBufferReadPtr + 8 )) );
	//eventID = ((tmp & 0x00FFFFFF) << 12);
	eventID = tmp & 0x00FFFFFF;
	eventID <<= 12;
	tmp = ntohl ( (*(AliVolatileUInt32_t*)( fEventBufferReadPtr + 4 )) );
	eventID |=  (tmp & 0x00000FFF);

#endif

#else

	AliHLTDDLHeader ddlHeader;
	if ( fHeaderSearch )
	    {
	    if ( dataSize < AliHLTMinDDLHeaderSize )
		{
		LOG( AliHLTLog::kWarning, "AliHLTRORC1Handler::PollForEvent", "Data smaller than DDL header" )
		    << "Data received is smaller than minimum DDL header size " << AliHLTLog::kDec 
		    << AliHLTMinDDLHeaderSize << "." << ENDLOG;
		}
	    else if ( !ddlHeader.Set( (AliUInt32_t*)(AliVolatileUInt32_t*)fEventBufferReadPtr ) )
		{
		LOG( AliHLTLog::kError, "AliHLTRORC1Handler::PollForEvent", "Error reading DDL header" )
		    << "Error reading DDL header number. Unknown header version or out of memory. Header version: " 
		    << AliHLTLog::kDec 
		    << (unsigned)AliHLTGetDDLHeaderVersion(fEventBufferReadPtr) << " - header version byte index: "
		    << (unsigned)DDLHEADERVERSIONINDEX << " - header size: " << AliHLTGetDDLHeaderSize(fEventBufferReadPtr)
		    << ENDLOG;
		eventID = 0;
		}
	    else
		{
		LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::PollForEvent", "DDL header" )
		    << "Found DDL header version number. : " 
		    << AliHLTLog::kDec 
		    << (unsigned)AliHLTGetDDLHeaderVersion(fEventBufferReadPtr) << " - header version byte index: "
		    << (unsigned)DDLHEADERVERSIONINDEX << " - header size: " << AliHLTGetDDLHeaderSize(fEventBufferReadPtr)
		    << ENDLOG;
		LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::PollForEvent", "DDL header data" )
		    << "DDL header: Word 0: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(0)
		    << " - Word 1: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(1)
		    << " - Word 2: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(2)
		    << " - Word 3: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(3)
		    << " - Word 4: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(4)
		    << " - Word 5: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(5)
		    << " - Word 6: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(6)
		    << ENDLOG;
		unsigned long headerSize = ddlHeader.GetHeaderSize();
		if ( headerSize > dataSize )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTRORC1Handler::PollForEvent", "Data smaller than DDL header" )
			<< "Data received is smaller than reported DDL header size " << AliHLTLog::kDec 
			<< headerSize << " - header version: " << (unsigned)AliHLTGetDDLHeaderVersion(fEventBufferReadPtr) 
			<< "." << ENDLOG;
		    eventID = 0;
		    }
		else
		    {
		    eventID = ddlHeader.GetEventID();
		    }
		}
	    }

#endif

	if ( fEnumerateEventIDs )
	    {
	    LOG( AliHLTLog::kInformational, "AliHLTRORC1Handler::PollForEvent", "Enumerating found event" )
		<< "Enumerating found event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") to 0x" << AliHLTLog::kHex << fNextEventID << " (" << AliHLTLog::kDec
		<< fNextEventID << ")." << ENDLOG;
	    eventID = fNextEventID;
	    ++fNextEventID;
	    }
	else
	    {
	    if ( eventID.fNr<=fLastRORCEventID.fNr )
		{
		AliEventID_t tmpID( kAliEventTypeUnknown, 1 );
		tmpID.fNr <<= 36;
		fAddEventIDCounter.fNr += tmpID.fNr;
		//fAddEventIDCounter &= 0xFFFFFFFFFF000000ULL;
		}
	    fLastRORCEventID = eventID;
	    eventID.fNr |= fAddEventIDCounter.fNr;
	    }

	ed.fEventID = eventID;

	//ed.fEventID = eventID = ((*(AliVolatileUInt32_t*)fEventBufferReadPtr) & 0xFFFFFF00) >> 8;
	ed.fOffset = dataOffset = fEventBufferReadPtr - fEventBuffer;
	LOG( (showErrorData ? AliHLTLog::kError : AliHLTLog::kDebug), "AliHLTRORC1Handler::PollForEvent", "Event found" )
	    << "Found event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ")." << ENDLOG;
	LOG( (showErrorData ? AliHLTLog::kError : AliHLTLog::kDebug), "AliHLTRORC1Handler::PollForEvent", "Found event" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << "): Data: " << dataSize << " (0x" << AliHLTLog::kHex << dataSize
	    << ") bytes (" << AliHLTLog::kDec << blocks << " blocks) - data offset 0x" 
	    << AliHLTLog::kHex << dataOffset << " (" << AliHLTLog::kDec
	    << dataOffset << ") - report buffer location 0x" << AliHLTLog::kHex << (unsigned long)fReportBufferReadPtr
	    << " (offset 0x" << (unsigned long)(fReportBufferReadPtr-fReportBuffer) << " ("
	    << AliHLTLog::kDec << (unsigned long)(fReportBufferReadPtr-fReportBuffer) 
	    << "))." << ENDLOG;
	AliVolatileUInt8_t* oldEventBufferReadPtr = fEventBufferReadPtr;
	fEventBufferReadPtr += blocks*128;
	fEventBufferReadPtrPhysAddress += blocks*128;
	fEventBufferReadPtrBusAddress += blocks*128;
	ed.fWrappedSize = dataWrappedSize = 0;
	if ( fEventBufferReadPtr >= fEventBufferEnd )
	    {
	    ed.fWrappedSize = fEventBufferReadPtr - fEventBufferEnd;
	    if ( oldEventBufferReadPtr+dataSize >= fEventBufferEnd )
		dataWrappedSize = oldEventBufferReadPtr+dataSize - fEventBufferEnd;
	    LOG( (showErrorData ? AliHLTLog::kError : AliHLTLog::kDebug), "AliHLTRORC1Handler::PollForEvent", "Event wrapped" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") is wrapped. Wrapped block size: " << ed.fWrappedSize << " (0x"
		<< AliHLTLog::kHex << ed.fWrappedSize << "). Wrapped data size: " << dataWrappedSize << " (0x"
		<< AliHLTLog::kHex << dataWrappedSize << ")." << ENDLOG;
	    fEventBufferReadPtr = fEventBuffer+ed.fWrappedSize;
	    fEventBufferReadPtrPhysAddress = fEventBufferPhysAddress+ed.fWrappedSize;
	    fEventBufferReadPtrBusAddress = fEventBufferBusAddress+ed.fWrappedSize;
	    }
	pthread_mutex_lock( &fEventMutex );
	fEvents.Add( ed );
	pthread_mutex_unlock( &fEventMutex );

#ifdef COMPARE_SOFTWARE_EVENTBUFFERREADPTR_CARD_DMA_ADDR
	volatile unsigned long dma_addr;
	dma_addr      = *((volatile unsigned long*)((volatile AliUInt8_t*)fBar0Ptr+0x00));
	if ( dma_addr != fEventBufferReadPtrPhysAddress )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTRORC1Handler::PollForEvent", "New buffer ptrs" )
		<< "Card DMA address and software physical event buffer read ptr do not match: 0x"
		<< AliHLTLog::kHex << dma_addr << " <-> 0x" << fEventBufferReadPtrPhysAddress
		<< " ("
		<< AliHLTLog::kDec << dma_addr << " <-> " << fEventBufferReadPtrPhysAddress
		<< ")." << ENDLOG;
	    }
#endif
    
#if 1
	*(AliVolatileUInt32_t*)fReportBufferReadPtr = 0xFFFFFFFF;
#endif
	fReportBufferReadPtr += 4;
	if ( fReportBufferReadPtr >= fReportBufferEnd )
	    fReportBufferReadPtr = fReportBuffer;
#ifdef SIMULATE
	gReportBufferReadPtr = (AliUInt32_t*)fReportBufferReadPtr;
#endif
	LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::PollForEvent", "New buffer ptrs" )
	    << "New report buffer ptr: 0x" << AliHLTLog::kHex << (unsigned long)fReportBufferReadPtr
	    << " (offset " << AliHLTLog::kDec << (unsigned long)(fReportBufferReadPtr-fReportBuffer)
	    << " (0x" << AliHLTLog::kHex << (unsigned long)(fReportBufferReadPtr-fReportBuffer)
	    << ")) - New event buffer ptr: 0x" << AliHLTLog::kHex << (unsigned long)fEventBufferReadPtr
	    << " (offset " << AliHLTLog::kDec << (unsigned long)(fEventBufferReadPtr-fEventBuffer)
	    << " (0x" << AliHLTLog::kHex << (unsigned long)(fEventBufferReadPtr-fEventBuffer)
	    << ")) - New event buffer bus address: 0x" << AliHLTLog::kHex << (unsigned long)fEventBufferReadPtrBusAddress
	    << " (offset " << AliHLTLog::kDec << (unsigned long)(fEventBufferReadPtrBusAddress-fEventBufferBusAddress)
	    << " (0x" << AliHLTLog::kHex << (unsigned long)(fEventBufferReadPtrBusAddress-fEventBufferBusAddress)
	    << "))." << ENDLOG;


	return 0;
	}
    return EAGAIN;
    }

int AliHLTRORC1Handler::ReleaseEvent( AliEventID_t eventID )
    {
    unsigned long ndx;
    EventData* edp;
    vector<BlockData>::iterator iter, end;
    LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::ReleaseEvent", "Releasing event" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << ") to be released." << ENDLOG;
    pthread_mutex_lock( &fEventMutex );
    if ( MLUCVectorSearcher<EventData,AliEventID_t>::FindElement( fEvents, &EventDataSearchFunc, eventID, ndx ) )
	{
	edp = fEvents.GetPtr( ndx );
	LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::ReleaseEvent", "Released event data" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << "): " << edp->fSize << " (0x" << AliHLTLog::kHex << edp->fSize
	    << ") bytes - wrapped size: " << AliHLTLog::kDec << edp->fWrappedSize
	    << " (0x" << AliHLTLog::kHex << edp->fWrappedSize << ") - offset 0x" 
	    << AliHLTLog::kHex << edp->fOffset << " (" << AliHLTLog::kDec
	    << edp->fOffset << ") - current event buffer release offset: 0x"
	    << AliHLTLog::kHex << fEventBufferReleasedOffset << " (" << AliHLTLog::kDec
	    << fEventBufferReleasedOffset << ")." << ENDLOG;
	if ( edp->fWrappedSize )
	    edp->fSize -= edp->fWrappedSize;
	if ( fEventBufferReleasedOffset == edp->fOffset )
	    {
	    if ( edp->fWrappedSize )
		{
		fEventBufferReleasedOffset = edp->fWrappedSize;
		}
	    else
		{
		fEventBufferReleasedOffset += edp->fSize;
		}
	    if ( fEventBufferReleasedOffset == fEventBufferSize )
		fEventBufferReleasedOffset = 0;
	    iter = fBlocks.begin();
	    while ( iter!=fBlocks.end() && iter->fOffset==fEventBufferReleasedOffset )
		{
		fEventBufferReleasedOffset += iter->fSize;
		fBlocks.erase( iter );
		if ( fEventBufferReleasedOffset >= fEventBufferSize )
		    fEventBufferReleasedOffset = fEventBufferReleasedOffset-fEventBufferSize;
		iter = fBlocks.begin();
		}
	    LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::ReleaseEvent", "New release offset" )
		<< "New event buffer release offset: 0x" << AliHLTLog::kHex 
		<< fEventBufferReleasedOffset << " (" << AliHLTLog::kDec
		<< fEventBufferReleasedOffset << ") - *(AliVolatileUInt32_t*)fAdminBuffer: "
		<< (unsigned long)(fEventBufferBusAddress+fEventBufferReleasedOffset) << "." << ENDLOG;
	    *(AliVolatileUInt32_t*)fAdminBuffer = (unsigned long)(fEventBufferBusAddress+fEventBufferReleasedOffset);
	    }
	else
	    {
	    bool repeat = false;
	    do
		{
		repeat = false;
		iter = fBlocks.begin();
		end = fBlocks.end();
		if ( edp->fOffset < fEventBufferReleasedOffset )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::ReleaseEvent", "Wrapped block" )
			<< "Skipping for wrapped block part." << ENDLOG;
		    if ( iter != end )
			iter++;
		    while ( iter != end )
			{
			if ( iter==end || (iter-1)->fOffset>iter->fOffset )
			    break;
			iter++;
			}
		    }
		bool inserted = false;
		BlockData bd;
		while ( iter != end )
		    {
		    if ( edp->fOffset+edp->fSize == iter->fOffset )
			{
			LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::ReleaseEvent", "Merging block" )
			    << "Merging block at beginning: (" << AliHLTLog::kDec
			    << edp->fOffset << "/" << edp->fSize << ") + (" << iter->fOffset
			    << "/" << iter->fSize << ") == (" << edp->fOffset << "/"
			    << iter->fSize+edp->fSize << ")." << ENDLOG;
			iter->fOffset = edp->fOffset;
			iter->fSize += edp->fSize;
			inserted = true;
			if ( iter!=fBlocks.begin() && (iter-1)->fOffset+(iter-1)->fSize==iter->fOffset )
			    {
			    LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::ReleaseEvent", "Merging block" )
				<< "Merging in preceeding block at beginning: (" << AliHLTLog::kDec
				<< (iter-1)->fOffset << "/" << (iter-1)->fSize << ") + (" << iter->fOffset
				<< "/" << iter->fSize << ") == (" << (iter-1)->fOffset << "/"
				<< iter->fSize+(iter-1)->fSize << ")." << ENDLOG;
			    (iter-1)->fSize += iter->fSize;
			    fBlocks.erase( iter );
			    }
			break;
			}
		    if ( iter->fOffset+iter->fSize == edp->fOffset )
			{
			LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::ReleaseEvent", "Merging block" )
			    << "Merging block at end: (" << AliHLTLog::kDec
			    << iter->fOffset << "/" << iter->fSize << ") + (" << edp->fOffset 
			    << "/" << edp->fSize << ") == (" << iter->fOffset << "/"
			    << iter->fSize+edp->fSize << ")." << ENDLOG;
			iter->fSize += edp->fSize;
			inserted = true;
			if ( (iter+1)!=end && (iter+1)->fOffset==iter->fOffset+iter->fSize )
			    {
			    LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::ReleaseEvent", "Merging block" )
				<< "Merging in succeeding block at end: (" << AliHLTLog::kDec
				<< iter->fOffset << "/" << iter->fSize << ") + (" << (iter+1)->fOffset
				<< "/" << (iter+1)->fSize << ") == (" << iter->fOffset << "/"
				<< iter->fSize+(iter+1)->fSize << ")." << ENDLOG;
			    iter->fSize += (iter+1)->fSize;
			    fBlocks.erase( iter+1 );
			    }
			break;
			}
		    if ( edp->fOffset < iter->fOffset )
			{
			LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::ReleaseEvent", "Inserting block" )
			    << "Inserting block (" << AliHLTLog::kDec
			    << edp->fOffset << "/" << edp->fSize << ") before (" << iter->fOffset 
			    << "/" << iter->fSize << ")." << ENDLOG;
			bd.fOffset = edp->fOffset;
			bd.fSize = edp->fSize;
			fBlocks.insert( iter, bd );
			inserted = true;
			break;
			}
		    iter++;
		    }
		if ( !inserted )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::ReleaseEvent", "Appending block" )
			<< "Appending block: (" << AliHLTLog::kDec
			<< edp->fOffset << "/" << edp->fSize 
			<< ") at end." << ENDLOG;
		    bd.fOffset = edp->fOffset;
		    bd.fSize = edp->fSize;
		    fBlocks.insert( end, bd );
		    }
		if ( edp->fWrappedSize )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTRORC1Handler::ReleaseEvent", "Wrapping block" )
			<< "Wrapping block. (" << AliHLTLog::kDec
			<< edp->fOffset << "/" << edp->fSize << ") -> (0/"
			<< edp->fWrappedSize << ")." << ENDLOG;
		    repeat = true;
		    edp->fOffset = 0;
		    edp->fSize = edp->fWrappedSize;
		    edp->fWrappedSize = 0;
		    }
		}
	    while ( repeat );
	    }
	fEvents.Remove( ndx );
	pthread_mutex_unlock( &fEventMutex );
	return 0;
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTRORC1Handler::ReleaseEvent", "Event not found" )
	    << "Event ox" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") to be released could not be found in list. Giving up..." 
	    << ENDLOG;
	pthread_mutex_unlock( &fEventMutex );
	return EIO;
	}
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
