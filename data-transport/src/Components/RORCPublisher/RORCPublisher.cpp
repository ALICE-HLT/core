/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTRORCPublisher.hpp"
//#include "AliHLTControlledProcessComponent.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTDummyRORCHandler.hpp"
#include "AliHLTRORC1Handler.hpp"
#include "AliHLTRORC2Handler.hpp"
#include "AliHLTRORC3Handler.hpp"
#include "AliHLTRORC4Handler.hpp"
#include "AliHLTRORC5Handler.hpp"
#include "AliHLTRORC6Handler.hpp"
#include "AliHLTRORCMapping.hpp"
#include "AliHLTRORCTPCMapping.hpp"
#include "AliHLTLog.hpp"
#include <vector>

#define RORC_DEFAULT_VERSION 6
#define RORC_MAX_VERSION 6

class AliHLTRORCBufferManager: public AliHLTBufferManager
    {
    public:

	AliHLTRORCBufferManager():
	    fRORCHandler(NULL)
		{
		}
	~AliHLTRORCBufferManager()
		{
		}

	void SetRORCHandler( AliHLTRORCHandlerInterface* rorcHandler )
		{
		fRORCHandler = rorcHandler;
		}

	virtual unsigned long long GetTotalBufferSize()
		{
		return fRORCHandler ? fRORCHandler->GetTotalBufferSize() : 0;
		}
	virtual unsigned long long GetFreeBufferSize()
		{
		return fRORCHandler ? fRORCHandler->GetFreeBufferSize() : 0;
		}


	virtual bool AddBuffer( AliHLTShmID, unsigned long )
		{
		return false;
		}
	virtual bool DeleteBuffer( AliHLTShmID, bool )
		{
		return false;
		}

	virtual bool CanGetBlock()
		{
		return false;
		}

	virtual bool GetBlock( AliUInt32_t&, unsigned long&,
				unsigned long& )
		{
		return false;
		}

	virtual bool AdaptBlock( AliUInt32_t, unsigned long,
				 unsigned long )
		{
		return false;
		}

	virtual bool AdaptBlock( AliUInt32_t, unsigned long,
				 unsigned long, unsigned long )
		{
		return false;
		}

	virtual bool ReleaseBlock( AliUInt32_t, unsigned long, bool /*tmp=false*/ )
		{
		return false;
		}

    protected:

	AliHLTRORCHandlerInterface* fRORCHandler;

    };

class RORCPublisherComponent: public AliHLTControlledSourceComponent
    {
    public:
	
	RORCPublisherComponent( const char* name, int argc, char** argv );
	virtual ~RORCPublisherComponent() {};
	
    protected:

	virtual AliHLTDetectorPublisher* CreatePublisher();
	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

	virtual void ResetConfiguration();
	virtual bool CheckConfiguration();
	virtual void ShowConfiguration();
	virtual void Configure();

	virtual bool CreateParts();
	virtual void DestroyParts();
	virtual bool SetupComponents();

	virtual AliHLTBufferManager* CreateBufferManager();

	bool fDoSleep;
	unsigned long fSleepTime;
	bool fPCIDeviceInfoSet;
	bool fVendorDeviceIndexOrBusSlotFunction;
	AliUInt16_t fPCIVendorIDOrBus;
	AliUInt16_t fPCIDeviceIDOrSlot;
	AliUInt16_t fPCIDeviceIndexOrFunction;
	AliUInt16_t fPCIDeviceBarIndex;

	unsigned long fRORCVersion;

	bool fFirstStart;

	bool fDummy;
	bool fEnumerateEventIDs;

	bool fDDLHeaderSearch;

	bool fReportWordConsistencyCheck;
	bool fReportWordUseBlockSize;

	AliUInt32_t fWrapAreaSize;
	bool fWrapAreaSizeSet;
	unsigned long fOldBufferSize;

	AliHLTRORCHandlerInterface* fRORCHandler;

	bool fTPCDataFix;

#ifdef COMPARE_COUNTER_PATTERN_TAGGED
	uint8 fCompareHighTag;
	bool fDoCompareCounterPattern;
#endif

	unsigned long fRORCBlockSize;

	bool fPublishCorruptEvents;
	bool fSuppressHWSuppressedEvents;
	bool fSuppressGoodEvents;

	bool fTwoLinks;

	bool fNoFlowControl;

	unsigned short fEventSuppression_Exp2;
	bool fEventSuppressionActive;

	AliHLTEventDataType fSplitEventDataType;
	bool fSplitEventDataTypeSet;

	bool fEnableCorruptEventsAutoDump;
	unsigned long fCorruptEventsAutoDumpLimit;
	MLUCString fCorruptEventsAutoDumpDir;

	AliHLTRORCMapping* fRORCMapping;
	const char* fRORCMappingFilename;
	unsigned fRORCTPCMappingPatchNr;

	bool fSinglePadSuppression;
	bool fSinglePadSuppressionSet;
	bool fBypassMerger;
	bool fBypassMergerSet;
	bool fDeconvPad;
	bool fDeconvPadSet;
	AliUInt16_t fClusterLowerLimit;
	bool fClusterLowerLimitSet;
	AliUInt8_t fSingleSeqLimit;
	bool fSingleSeqLimitSet;
        AliUInt8_t fMergerDistance;
        bool fMergerDistanceSet;
        bool fMergerAlgorithmFollow;
        bool fMergerAlgorithmFollowSet;
        AliUInt8_t fChargeTolerance;
        bool fChargeToleranceSet;

	unsigned fNoPeriodCounterLogModulo;

	bool fNoStrictSOR;
	
    private:
    };


RORCPublisherComponent::RORCPublisherComponent( const char* name, int argc, char** argv ):
    AliHLTControlledSourceComponent( name, argc, argv )
    {
    fAllowDataTypeOriginSpecSet = true;
    fDoSleep = false;
    fSleepTime = 10000;
    fPCIDeviceInfoSet = false;
    fVendorDeviceIndexOrBusSlotFunction = true;
    fPCIVendorIDOrBus = 0xFFFF;
    fPCIDeviceIDOrSlot = 0xFFFF;
    fPCIDeviceIndexOrFunction = 0xFFFF;
    fPCIDeviceBarIndex = 0xFFFF;
    fRORCHandler = NULL;
    fFirstStart = true;
    fDummy = false;
    fEnumerateEventIDs = false;
    fDDLHeaderSearch = true;
    fMaxPreBlockCount = 3;
    fWrapAreaSize = 0;
    fWrapAreaSizeSet = false;
    fOldBufferSize = 0;
    fReportWordConsistencyCheck = false;
    fReportWordUseBlockSize = false;
    fRORCVersion = RORC_DEFAULT_VERSION;
    fTPCDataFix = false;
#ifdef COMPARE_COUNTER_PATTERN_TAGGED
    fCompareHighTag = 0;
    fDoCompareCounterPattern = false;;
#endif
    fRORCBlockSize = 256;
    fPublishCorruptEvents = false;
    fSuppressHWSuppressedEvents = false;
    fSuppressGoodEvents = false;
    fTwoLinks = false;
    fNoFlowControl = false;
    fEventSuppression_Exp2 = 0;
    fEventSuppressionActive = false;
    fSplitEventDataTypeSet = false;
    fEnableCorruptEventsAutoDump = true;
    fCorruptEventsAutoDumpLimit = 100;
    fCorruptEventsAutoDumpDir = ".";
    fRORCMapping = NULL;
    fRORCTPCMappingPatchNr = ~0U;
    fRORCMappingFilename = NULL;
    fSinglePadSuppression = false;
    fSinglePadSuppressionSet = false;
    fBypassMerger = false;
    fBypassMergerSet = false;
    fDeconvPad = true;
    fDeconvPadSet = false;
    fClusterLowerLimit = 0;
    fClusterLowerLimitSet = false;
    fSingleSeqLimit = 0;
    fSingleSeqLimitSet = false;
    fMergerDistance = 4;
    fMergerDistanceSet = false;
    fMergerAlgorithmFollow = true;
    fMergerAlgorithmFollowSet = false;
    fChargeTolerance = 0;
    fChargeToleranceSet = false;
    fNoPeriodCounterLogModulo = 1;
    fNoStrictSOR = false;
    }


AliHLTDetectorPublisher* RORCPublisherComponent::CreatePublisher()
    {
    return new AliHLTRORCPublisher( fOwnPublisherName.c_str(), fMaxPreEventCountExp2 );
    }


const char* RORCPublisherComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cpErr = NULL;
#ifdef COMPARE_COUNTER_PATTERN_TAGGED
    if ( !strcmp( argv[i], "-comparecounter" ) )
	{
	fDoCompareCounterPattern = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-comparehightag" ) )
	{
	if ( argc <= i+1 )
	    return "Missing comparison high tag specifier.";
	fCompareHighTag = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting comparison high tag specifier.";
	    }
	if ( fCompareHighTag > 0xF )
	    {
	    errorArg = i+1;
	    return "Comparison high tag specifier is too large (Max. allowed value 0xF).";
	    }
	i += 2;
	return NULL;
	}
#endif
    if ( !strcmp( argv[i], "-device" ) )
	{
	if ( argc <= i+4 )
	    return "Missing pci device information parameter";
	fPCIVendorIDOrBus = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting pci vendor specifier.";
	    }
	fPCIDeviceIDOrSlot = strtoul( argv[i+2], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+2;
	    return "Error converting pci device specifier.";
	    }
	fPCIDeviceIndexOrFunction = strtoul( argv[i+3], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+3;
	    return "Error converting pci device index specifier.";
	    }
	fPCIDeviceBarIndex = strtoul( argv[i+4], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+4;
	    return "Error converting pci device bar index specifier.";
	    }
	fVendorDeviceIndexOrBusSlotFunction = true;
	fPCIDeviceInfoSet = true;
	i += 5;
	return NULL;
	}
    if ( !strcmp( argv[i], "-slot" ) )
	{
	if ( argc <= i+4 )
	    return "Missing pci geographical address parameter";
	fPCIVendorIDOrBus = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting pci bus specifier.";
	    }
	fPCIDeviceIDOrSlot = strtoul( argv[i+2], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+2;
	    return "Error converting pci slot specifier.";
	    }
	fPCIDeviceIndexOrFunction = strtoul( argv[i+3], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+3;
	    return "Error converting pci device function specifier.";
	    }
	fPCIDeviceBarIndex = strtoul( argv[i+4], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+4;
	    return "Error converting pci device bar index specifier.";
	    }
	fVendorDeviceIndexOrBusSlotFunction = false;
	fPCIDeviceInfoSet = true;
	i += 5;
	return NULL;
	}
    if ( !strcmp( argv[i], "-sleep" ) )
	{
	fDoSleep = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-restart" ) )
	{
	fFirstStart = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-dummydevice" ) )
	{
	fDummy = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-enumerateeventids" ) )
	{
	fEnumerateEventIDs = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-disableddlheadersearch" ) )
	{
	fDDLHeaderSearch = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-addhigheventidpart" ) )
	{
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-sleeptime" ) )
	{
	if ( argc <= i+1 )
	    return "Missing sleep time specifier.";
	fSleepTime = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting sleep time specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-wrapareasize" ) )
	{
	if ( argc <= i+1 )
	    return "Missing wrap area size specifier.";
	fWrapAreaSize = strtoul( argv[i+1], &cpErr, 0 );
	fWrapAreaSizeSet = true;
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting wrap area size specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-rorcinterface" ) )
	{
	if ( argc <= i+1 )
	    return "Missing RORC version specifier.";
	fRORCVersion = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting RORC version specifier.";
	    }
	if ( fRORCVersion<1 || fRORCVersion>RORC_MAX_VERSION )
	    {
	    errorArg = i+1;
	    return "Invalid RORC version specifier, allowed values: 1, 2, 3, 4, 5.";
	    }
// 	if ( fRORCVersion>=5 )
// 	    fPrivateSharedMemory = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-checkreportwordconsistency" ) )
	{
	fReportWordConsistencyCheck = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-usereportwordblockcount" ) )
	{
	fReportWordUseBlockSize = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcdatafix" ) )
	{
	fTPCDataFix = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-rorcblocksize" ) )
	{
	if ( argc <= i+1 )
	    return "Missing RORC block size specifier.";
	fRORCBlockSize = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting RORC block size specifier.";
	    }
	if ( fRORCBlockSize % 4 )
	    {
	    errorArg = i+1;
	    return "RORC block size must be divisible by 4.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-publishcorruptevents" ) )
	{
	fPublishCorruptEvents = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-suppressedhardwaresuppressedevents" ) )
	{
	fSuppressHWSuppressedEvents = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-suppressgoodevents" ) )
	{
	fSuppressGoodEvents = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-noflowcontrol" ) )
	{
	fNoFlowControl = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-twolinks" ) )
	{
	fTwoLinks = true;
	++i;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventsuppression" ) )
	{
	if ( argc <= i+1 )
	    return "Missing event suppression specifier (exponent to 2).";
	fEventSuppression_Exp2 = strtoul( argv[i+1], &cpErr, 0 );
	fEventSuppressionActive = true;
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting event suppression specifier.";
	    }
	if ( fEventSuppression_Exp2>=16 )
	    {
	    errorArg = i+1;
	    return "Event suppression specifier must be less than 16.";
	    }
	i += 2;
	return NULL;
	}
	if ( !strcmp( argv[i], "-splitdatatype" ) )
	    {
	    if ( argc <= i +1 )
		{
		return "Missing split datatype specifier.";
		}
	    if ( strlen( argv[i+1])>8 )
		{
		return "Maximum allowed length for split datatype specifier is 8 characters.";
		}
	    AliUInt64_t tmpDT = 0;
	    unsigned tmpN;
	    for( tmpN = 0; tmpN < strlen( argv[i+1]); tmpN++ )
		{
		tmpDT = (tmpDT << 8) | (AliUInt64_t)(argv[i+1][tmpN]);
		}
	    for ( ; tmpN < 8; tmpN++ )
		{
		tmpDT = (tmpDT << 8) | (AliUInt64_t)(' ');
		}
	    fSplitEventDataType.fID = tmpDT;
	    fSplitEventDataTypeSet = true;
	    i += 2;
	    return NULL;
	    }
    if ( !strcmp( argv[i], "-disablecorruptautodump" ) )
	{
	fEnableCorruptEventsAutoDump = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-corruptautodumplimit" ) )
	{
	
	if ( argc <= i+1 )
	    return "Missing corrupt event auto dump event count limit.";
	fCorruptEventsAutoDumpLimit = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting corrupt event auto dump event count limit.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-corruptautodumpdir" ) )
	{
	if ( argc <= i+1 )
	    return "Missing corrupt event auto dump directory.";
	fCorruptEventsAutoDumpDir = argv[i+1];
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcmappingfile" ) )
	{
	LOG( AliHLTLog::kWarning, "", "Deprectated Option" )
	    << "Option '-tpcmappingfile' is deprecated. Please use option '-tpcmappingslice' with same syntax or '-tpcmapping' without patch number." << ENDLOG;
	if ( argc <= i+1 )
	    return "Missing patch number and TPC mapping file indicator.";
	if ( argc <= i+2 )
	    return "Missing TPC mapping file indicator.";
	fRORCTPCMappingPatchNr = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting TPC mapping patch number.";
	    }
	if ( fRORCTPCMappingPatchNr>5 )
	    {
	    errorArg = i+1;
	    return "Illegal value for TPC mapping patch number, only values from 0-5 are allowed.";
	    }
	fRORCMappingFilename = argv[i+2];
	i += 3;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcmappingslice" ) )
	{
	if ( argc <= i+1 )
	    return "Missing patch number and TPC mapping file indicator.";
	if ( argc <= i+2 )
	    return "Missing TPC mapping file indicator.";
	fRORCTPCMappingPatchNr = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting TPC mapping patch number.";
	    }
	if ( fRORCTPCMappingPatchNr>5 )
	    {
	    errorArg = i+1;
	    return "Illegal value for TPC mapping patch number, only values from 0-5 are allowed.";
	    }
	fRORCMappingFilename = argv[i+2];
	i += 3;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcmapping" ) )
	{
	if ( argc <= i+1 )
	    return "Missing TPC mapping file indicator.";
	fRORCMappingFilename = argv[i+1];
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcsinglepadsuppressiondisable" ) )
	{
	fSinglePadSuppression = false;
	fSinglePadSuppressionSet = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcsinglepadsuppressionenable" ) )
	{
	fSinglePadSuppression = true;
	fSinglePadSuppressionSet = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcbypassmergerdisable" ) )
	{
	fBypassMerger = false;
	fBypassMergerSet = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcbypassmergerenable" ) )
	{
	fBypassMerger = true;
	fBypassMergerSet = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcdeconvpaddisable" ) )
	{
	fDeconvPad= false;
	fDeconvPadSet = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcdeconvpadenable" ) )
	{
	fDeconvPad= true;
	fDeconvPadSet = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcclusterlowerlimit" ) )
	{
	if ( argc <= i+1 )
	    return "Missing TPC hardware co-processor lower cluster limit.";
	unsigned long tmp = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting TPC hardware co-processor lower cluster limit.";
	    }
	if ( tmp>0xFFFF )
	    {
	    errorArg = i+1;
	    return "Value given for TPC hardware co-processor lower cluster limit is larger than 16 bit limit (0-65535).";
	    }
	fClusterLowerLimit = (AliUInt16_t)tmp;
	fClusterLowerLimitSet = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcsinglesequencelimit" ) )
	{
	if ( argc <= i+1 )
	    return "Missing TPC hardware co-processor single sequence limit.";
	unsigned long tmp = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting TPC hardware co-processor single sequence limit.";
	    }
	if ( tmp>0xFF )
	    {
	    errorArg = i+1;
	    return "Value given for TPC hardware co-processor single sequence limit is larger than 8 bit limit (0-255).";
	    }
	fSingleSeqLimit = (AliUInt8_t)tmp;
	fSingleSeqLimitSet = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcmergerdistance" ) )
	{
	if ( argc <= i+1 )
	    return "Missing TPC hardware co-processor merger distance.";
	unsigned long tmp = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting TPC hardware co-processor merger distance.";
	    }
	if ( tmp>0xF )
	    {
	    errorArg = i+1;
	    return "Value given for TPC hardware co-processor merger distance is larger than 4 bit limit (0-15).";
	    }
	fMergerDistance = (AliUInt8_t)tmp;
	fMergerDistanceSet = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcmergeralgorithmfollowenable" ) )
	{
	fMergerAlgorithmFollow= true;
	fMergerAlgorithmFollowSet = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcmergeralgorithmfollowdisable" ) )
	{
	fMergerAlgorithmFollow= false;
	fMergerAlgorithmFollowSet = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-tpcchargetolerance" ) )
	{
	if ( argc <= i+1 )
	    return "Missing TPC hardware co-processor charge tolerance.";
	unsigned long tmp = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting TPC hardware co-processor charge tolerance.";
	    }
	if ( tmp>0xF )
	    {
	    errorArg = i+1;
	    return "Value given for TPC hardware co-processor charge tolerance is larger than 4 bit limit (0-15).";
	    }
	fChargeTolerance = (AliUInt8_t)tmp;
	fChargeToleranceSet = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-periodcounterlogmodulo" ) )
	{
	if ( argc <= i+1 )
	    return "Missing period counter log message modulo.";
	unsigned long tmp = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting period counter log message modulo.";
	    }
	if ( tmp>~(unsigned)0 )
	    {
	    errorArg = i+1;
	    return "Period counter log message modulo parameter is too large (max. (2^32)-1).";
	    }
	fNoPeriodCounterLogModulo = (unsigned)tmp;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-nostrictsor" ) )
	{
	fNoStrictSOR = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-strictsor" ) )
	{
	fNoStrictSOR = false;
	i++;
	return NULL;
	}
    
    return AliHLTControlledSourceComponent::ParseCmdLine( argc, argv, i, errorArg );
    }

void RORCPublisherComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    AliHLTControlledSourceComponent::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-device <pciVendorID> <pciDeviceID> <pciDeviceIndex> <pciDeviceBarIndex>: Specify the PCI device to use. (Mandatory Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-slot <pciBusNo> <pciSlotNo> <pciFunctionNo> <pciDeviceBarIndex>: Specify the PCI device to use. (Mandatory Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-rorcinterface <RORC-Firmware-Interface-Version>: Specify the version number of the RORC firmware to use. (Optional; Allowed 1, 2, 3, 4, 5, 6; Default " 
	<< AliHLTLog::kDec << RORC_DEFAULT_VERSION << ")" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-sleep: Sleep for a specified time when no event is available. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-sleeptime <sleeptime_usec>: Specify the time in microseconds to sleep (with -sleep) when no event is available. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-datatype <type>: Specify the type as which the data is to be specified. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-dataorigin <type>: Specify the origin to be specified for the data. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-dataspec <type>: Specify the specifier that is to be used to characterize the data. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-restart: Specify that this is a restart, do not initialize RORC and erase memories. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-dummydevice: Specify to simulate a RORC device and not use a real one. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-enumerateeventids: Specify to generate enumerated event IDs and not use real ones from the RORC. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-addhigheventidpart: Specify to add a part to the top 28 bits of an event ID read from the rorc to make it unique in case of 36 bit id wrap arounds. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-disableddlheadersearch: Specify to disable the search for DDL headers in received data. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-wrapareasize <wrap-area-size>: Specify the size of the area used to unwrap blocks wrapped in the RORC buffer. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-checkreportwordconsistency: Check the report word data size and block counts for consistency. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-usereportwordblockcount: Use the block count as contained in the report word directly, not the one calculated from the report word data size. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-tpcdatafix: Activate search for a fix for TPC data problems. (Optional)" << ENDLOG;
#ifdef COMPARE_COUNTER_PATTERN_TAGGED
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-comparecounter: Activate comparison of data received from RORC with a counter pattern of 32 bit words. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-comparehightag <tag>: Set the highest nibble (tag) of the 32 bit counter words for comparison. (Optional)" << ENDLOG;
#endif
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-rorcblocksize <RORC-Block-Size>: Specify the size of blocks the RORC transfers via DMA into memory. (Optional; Only supported for interface v4; Default 256; Must be divisible by 4)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-publishcorruptevents: Publish also events detected a corrupted during the transfer from the RORC (suppressed by default). (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-suppressedhardwaresuppressedevents: Do not publish events that have been suppressed already in the RORC hardware (except for the header) (suppressed by default). (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-suppressgoodevents: Do not publish events that were not marked as corrupted during the transfer from the RORC (disabled by default, mainly for debugging, to receive ONLY corrupt events). (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-twolinks: Specify that the RORC is used in a two link configuration, with a second RORCPublisher responsible for the second link. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-noflowcontrol: Specify that the flow control on the H-ROC is disabled. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-eventsuppression <event-suppression-specifier>: Enable event suppression in hardware, the specifier will be used as an exponent for 2, the coresponding power of two will be used in a modulo operation with the event ID (in the RORC hardware). (Optional, allowed values for the specifier are 0-15)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-splitdatatype <type>: Specify the type as which the data is to be specified in the case of split data blocks. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-disablecorruptautodump: Disable the automatic dump to disk of corrupt received events. (Optional, default is enabled)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-corruptautodumplimit <event-count-limit>: Specify the maximum number of events after which the automatic dump to disk of corrupt received events will stop. (Optional, default 100)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-corruptautodumpdir <directory>: Specify the directory into which the automatic dump to disk of corrupt received events will be done. (Optional, default current directory)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-tpcmappingfile <TPC patch number> <RORC TPC mapping filename>: Specify the patch number and mapping file to use for RORC TPC mapping for the fast hardware clusterfinder. Automatically enables mapping. (Optional, deprecated)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-tpcmappingslice <TPC patch number> <RORC TPC mapping filename>: Specify the patch number and mapping file to use for RORC TPC mapping for the fast hardware clusterfinder. Automatically enables mapping. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-tpcmapping <RORC TPC mapping filename>: Specify a mapping file to use for RORC TPC mapping for the fast hardware clusterfinder. Automatically enables mapping. Patch number is take from supplied DDL number. (Optional, only in special ALICE HLT mode)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-tpcsinglepadsuppressiondisable: Disable the single pad cluster suppression in the hardware fast TPC clusterfinder in the RORC. (Optional, only valid when hardware co-processor is enabled)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-tpcsinglepadsuppressionenable: Enable the single pad cluster suppression in the hardware fast TPC clusterfinder in the RORC. (Optional, only valid when hardware co-processor is enabled)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-tpcbypassmergerdisable: Disable the merger bypass for the hardware fast TPC clusterfinder in the RORC. (Optional, only valid when hardware co-processor is enabled)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-tpcbypassmergerenable: Enable the merger bypass for the hardware fast TPC clusterfinder in the RORC. (Optional, only valid when hardware co-processor is enabled)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-tpcdeconvpaddisable: Disable the deconvolution in pad direction for the hardware fast TPC clusterfinder in the RORC. (Optional, only valid when hardware co-processor is enabled)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-tpcdeconvpadenable: Enable the deconvolution in pad direction for the hardware fast TPC clusterfinder in the RORC. (Optional, only valid when hardware co-processor is enabled)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-tpcclusterlowerlimit <lower-cluster-limit-16bit>: Specify the lower cluster charge limit for the hardware fast TPC clusterfinder in the RORC. (Optional, 16 bit number (0-65535), only valid when hardware co-processor is enabled)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-tpcsinglesequencelimit <single-sequence-limit-8bit>: Specify the single sequence cluster charge limit for the hardware fast TPC clusterfinder in the RORC. (Optional, 8 bit number (0-255), only valid when hardware co-processor is enabled)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-tpcmergerdistance <merger-distance-4bit>: Specify the merger distance for the hardware fast TPC clusterfinder in the RORC. (Optional, 4 bit number (0-15), only valid when hardware co-processor is enabled)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-tpcmergeralgorithmfollowdisable: Disable the Megrer Algorithm Follow (?) for the hardware fast TPC clusterfinder in the RORC. (Optional, only valid when hardware co-processor is enabled)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-tpcmergeralgorithmfollowenable: Enable the Megrer Algorithm Follow (?) for the hardware fast TPC clusterfinder in the RORC. (Optional, only valid when hardware co-processor is enabled)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-tpcchargetolerance <charge-tolerance-4bit>: Specify the charge tolerance for the hardware fast TPC clusterfinder in the RORC. (Optional, 4 bit number (0-15), only valid when hardware co-processor is enabled)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-periodcounterlogmodulo <period-counter-log-message-modulo>: Specify a modulo to be applied for period counter change log messages. (Optional, default 1)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-nostrictsor: Relaxed requirements for receiving Start-Of-Run/Data event before first real data event. (Optional, only has effect in special ALICE HLT mode)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-strictsor: Strict requirements for receiving Start-Of-Run/Data event before first real data event. (Optional, only has effect in special ALICE HLT mode)" << ENDLOG;
    }

void RORCPublisherComponent::ResetConfiguration()
    {
    AliHLTControlledComponent::ResetConfiguration();
    fAllowDataTypeOriginSpecSet = true;
    fDoSleep = false;
    fSleepTime = 10000;
    fPCIDeviceInfoSet = false;
    fVendorDeviceIndexOrBusSlotFunction = true;
    fPCIVendorIDOrBus = 0xFFFF;
    fPCIDeviceIDOrSlot = 0xFFFF;
    fPCIDeviceIndexOrFunction = 0xFFFF;
    fPCIDeviceBarIndex = 0xFFFF;
    fRORCHandler = NULL;
    fFirstStart = true;
    fDummy = false;
    fEnumerateEventIDs = false;
    fDDLHeaderSearch = true;
    fMaxPreBlockCount = 3;
    fWrapAreaSize = 0;
    fWrapAreaSizeSet = false;
    fReportWordConsistencyCheck = false;
    fReportWordUseBlockSize = false;
    fRORCVersion = RORC_DEFAULT_VERSION;
    fTPCDataFix = false;
#ifdef COMPARE_COUNTER_PATTERN_TAGGED
    fCompareHighTag = 0;
    fDoCompareCounterPattern = false;;
#endif
    fRORCBlockSize = 256;
    fPublishCorruptEvents = false;
    fSuppressHWSuppressedEvents = false;
    fSuppressGoodEvents = false;
    fNoFlowControl = false;
    fEventSuppression_Exp2 = 0;
    fEventSuppressionActive = false;
    fSplitEventDataTypeSet = false;
    fEnableCorruptEventsAutoDump = true;
    fCorruptEventsAutoDumpLimit = 100;
    fCorruptEventsAutoDumpDir = ".";
    fRORCMapping = NULL;
    fRORCTPCMappingPatchNr = ~0U;
    fRORCMappingFilename = NULL;
    fSinglePadSuppression = false;
    fSinglePadSuppressionSet = false;
    fBypassMerger = false;
    fBypassMergerSet = false;
    fDeconvPad = true;
    fDeconvPadSet = false;
    fClusterLowerLimit = 0;
    fClusterLowerLimitSet = false;
    fSingleSeqLimit = 0;
    fSingleSeqLimitSet = false;
    fMergerDistance = 4;
    fMergerDistanceSet = false;
    fMergerAlgorithmFollow = true;
    fMergerAlgorithmFollowSet = false;
    fChargeTolerance = 0;
    fChargeToleranceSet = false;
    fNoStrictSOR = false;
    }

bool RORCPublisherComponent::CheckConfiguration()
    {
    if ( !AliHLTControlledComponent::CheckConfiguration() )
	return false;
    if ( !fPCIDeviceInfoSet )
	{
	LOG( AliHLTLog::kError, "RORCPublisherComponent::CheckConfiguration", "No pci device specified" )
	    << "Must specify the PCI device to be used (e.g. using the -device command line option."
	    << ENDLOG;
	return false;
	}
    if ( fRORCVersion<=3 && fPCIDeviceBarIndex!=0 )
	{
	LOG( AliHLTLog::kError, "RORCPublisherComponent::CheckConfiguration", "Invalid PCI device bar index" )
	    << "RORC Interfaces 1 - 3 only support bar 0." << ENDLOG;
	return false;
	}
    unsigned long wrapAreaSize = fWrapAreaSize;
    if ( !fWrapAreaSizeSet )
	wrapAreaSize = fMinBlockSize;
    if ( wrapAreaSize >= fBufferSize && fRORCVersion!=5)
	{
	LOG( AliHLTLog::kError, "RORCPublisherComponent::CheckConfiguration", "Wrap Area Too Large" )
	    << (fWrapAreaSizeSet ? "Wrap area size specified" : "Default wrap area size (minimum block size)")
	    << " is at least as large as buffer size. It must be smaller."
	    << ENDLOG;
	return false;
	}
    if ( fRORCVersion > RORC_MAX_VERSION )
	{
	LOG( AliHLTLog::kError, "RORCPublisherComponent::CheckConfiguration", "Too high RORC firmware version" )
	    << "Specified RORC firmware version " << AliHLTLog::kDec << fRORCVersion << " is too large. Allowed maximum is "
	    << RORC_MAX_VERSION << "." << ENDLOG;
	return false;
	}
    if ( fRORCVersion<4 && fRORCBlockSize!=256 )
	{
	LOG( AliHLTLog::kError, "RORCPublisherComponent::CheckConfiguration", "RORC block size only variable in interface version 4 or later" )
	    << "Specified RORC firmware version " << AliHLTLog::kDec << fRORCVersion << " does not allow setting the RORC block size." << ENDLOG;
	return false;
	}
    if ( fHardwareCoProcessor && fRORCVersion<6 )
	{
	LOG( AliHLTLog::kError, "RORCPublisherComponent::CheckConfiguration", "RORC hardware co-processor only supported in interface version 6 or later" )
	    << "Specified RORC firmware version " << AliHLTLog::kDec << fRORCVersion << " does not support hardware co-processors." << ENDLOG;
	return false;
	}
    
    if ( fCorruptEventsAutoDumpLimit<=0 )
	{
	LOG( AliHLTLog::kError, "RORCPublisherComponent::CheckConfiguration", "Corrupt event auto dump event count limit" )
	    << "Corrupt event auto dump event count limit must be greater than 0." << ENDLOG;
	return false;
	}
    return true;
    }

void RORCPublisherComponent::ShowConfiguration()
    {
    AliHLTControlledComponent::ShowConfiguration();
    if ( fVendorDeviceIndexOrBusSlotFunction )
	{
	LOG( AliHLTLog::kInformational, "RORCPublisherComponent::ShowConfiguration", "PCI Device Configuration" )
	    << "PCI device: Vendor ID: 0x" << AliHLTLog::kHex << fPCIVendorIDOrBus << " - " << " Device ID: 0x"
	    << fPCIDeviceIDOrSlot << " - Device Index: " << AliHLTLog::kDec << fPCIDeviceIndexOrFunction
	    << " - Device Bar Index: " << AliHLTLog::kDec << fPCIDeviceBarIndex << "." << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kInformational, "RORCPublisherComponent::ShowConfiguration", "PCI Device Configuration" )
	    << "PCI geographical address: Bus: 0x" << AliHLTLog::kHex << fPCIVendorIDOrBus << " - " << " Slot: 0x"
	    << fPCIDeviceIDOrSlot << " - Function: " << AliHLTLog::kDec << fPCIDeviceIndexOrFunction
	    << " - Device Bar Index: " << AliHLTLog::kDec << fPCIDeviceBarIndex << "." << ENDLOG;
	}
    LOG( AliHLTLog::kInformational, "RORCPublisherComponent::ShowConfiguration", "RORC Firmware Version" )
	<< "Using RORC firmware version " << AliHLTLog::kDec << fRORCVersion << "." << ENDLOG;
    LOG( AliHLTLog::kInformational, "RORCPublisherComponent::ShowConfiguration", "Start Mode" )
	<< "Start Mode: " << (fFirstStart ? "First Start" : "Restart" ) << "." << ENDLOG;
    if ( fDummy )
	{
	LOG( AliHLTLog::kWarning, "RORCPublisherComponent::ShowConfiguration", "Dummy Mode" )
	    << "Using dummy device, no real data will be available." << ENDLOG;
	}
    if ( fDoSleep )
	{
	LOG( AliHLTLog::kInformational, "RORCPublisherComponent::ShowConfiguration", "Sleep Mode" )
	    << "Sleeping between events for " << AliHLTLog::kDec << fSleepTime << " microseconds." << ENDLOG;
	
	}
    if ( fEnumerateEventIDs )
	{
	LOG( AliHLTLog::kWarning, "RORCPublisherComponent::ShowConfiguration", "Enumerating Event IDs" )
	    << "Enumerating event IDs, no real event IDs are used." << ENDLOG;
	}
    
    if ( !fDDLHeaderSearch )
	{
	LOG( AliHLTLog::kWarning, "RORCPublisherComponent::ShowConfiguration", "Not searching for DDL headers" )
	    << "Search for DDL headers in received data is disabled." << ENDLOG;
	}
    if ( !fWrapAreaSizeSet )
	{
	LOG( AliHLTLog::kWarning, "RORCPublisherComponent::ShowConfiguration", "Wrap Area" )
	    << "Wrap area size not set. Using minimum block size as default." << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kInformational, "RORCPublisherComponent::ShowConfiguration", "Wrap Area" )
	    << "Wrap area size set to 0x" << AliHLTLog::kHex << fWrapAreaSize << " ("
	    << AliHLTLog::kDec << fWrapAreaSize << ")." << ENDLOG;
	}
    if ( fReportWordConsistencyCheck )
	{
	LOG( AliHLTLog::kInformational, "RORCPublisherComponent::ShowConfiguration", "Report Word Consistency" )
	    << "Report word data size and block count are checked for consistency." << ENDLOG;
	}
    if ( fReportWordUseBlockSize )
	{
	LOG( AliHLTLog::kInformational, "RORCPublisherComponent::ShowConfiguration", "Use Report Word Block Count" )
	    << "Block count is used directly as contained in the report word, not calculated from the report word data size." << ENDLOG;
	}
    if ( fTPCDataFix )
	{
	LOG( AliHLTLog::kInformational, "RORCPublisherComponent::ShowConfiguration", "TPC Data Fix" )
	    << "TPC Data fix search activated." << ENDLOG;
	}
    if ( fNoFlowControl )
	{
	LOG( AliHLTLog::kInformational, "RORCPublisherComponent::ShowConfiguration", "No flow control" )
	    << "Flow control deactivated." << ENDLOG;
	}
#ifdef COMPARE_COUNTER_PATTERN_TAGGED
    if ( fDoCompareCounterPattern )
	{
	LOG( AliHLTLog::kInformational, "RORCPublisherComponent::ShowConfiguration", "Counter comparison" )
	    << "Performing counter comparison. WARNING: Slow" << ENDLOG;
	LOG( AliHLTLog::kInformational, "RORCPublisherComponent::ShowConfiguration", "Comparison high tag" )
	    << "Comparison high tag: 0x" << AliHLTLog::kHex << fCompareHighTag << "." << ENDLOG;
	}
#endif
    if ( fEventSuppressionActive )
	{
	LOG( AliHLTLog::kInformational, "RORCPublisherComponent::ShowConfiguration", "Event suppression" )
	    << "Event suppression: " << AliHLTLog::kDec << fEventSuppression_Exp2 << "." << ENDLOG;
	}


    if ( fEnableCorruptEventsAutoDump )
	{
	LOG( AliHLTLog::kInformational, "RORCPublisherComponent::ShowConfiguration", "Corrupt event auto dump" )
	    << "Automatic dumping of corrupt is enabled. Limit: " << AliHLTLog::kDec << fCorruptEventsAutoDumpLimit
	    << " - directory for dumped files: " << fCorruptEventsAutoDumpDir.c_str() << ENDLOG;
	}

    }

void RORCPublisherComponent::Configure()
    {
    if ( !fWrapAreaSizeSet )
	fWrapAreaSize = fMinBlockSize;
    if ( fRORCVersion==5 )
	{
	fOldBufferSize = fBufferSize;
	fBufferSize = fWrapAreaSize;
	}
    fWrapAreaSizeSet = true;
    AliHLTControlledComponent::Configure();
    }



bool RORCPublisherComponent::CreateParts()
    {
    if ( !AliHLTControlledComponent::CreateParts() )
	return false;
    if ( !fDummy )
	{
	switch ( fRORCVersion )
	    {
	    case 1:
		fRORCHandler = new AliHLTRORC1Handler( fBufferSize, fMaxPreEventCountExp2 );
		break;
	    case 2:
		fRORCHandler = new AliHLTRORC2Handler( fBufferSize, fMaxPreEventCountExp2 );
		break;
	    case 3:
		fRORCHandler = new AliHLTRORC3Handler( fBufferSize, fMaxPreEventCountExp2 );
		break;
	    case 4:
		fRORCHandler = new AliHLTRORC4Handler( fBufferSize, fRORCBlockSize, fMaxPreEventCountExp2 );
		break;
	    case 5:
		fRORCHandler = new AliHLTRORC5Handler( fOldBufferSize, fRORCBlockSize, fTwoLinks, fMaxPreEventCountExp2 );
		break;
	    case 6:
		fRORCHandler = new AliHLTRORC6Handler( fBufferSize, fRORCBlockSize, fMaxPreEventCountExp2 );
		break;
	    }
	}
    else
	fRORCHandler = new AliHLTDummyRORCHandler( fBufferSize, fMaxPreEventCountExp2 );
    if ( !fRORCHandler )
	{
	LOG( AliHLTLog::kError, "RORCPublisherComponent::CreateParts", "Unable to create RORC handler object" )
	    << "Unable to create RORC handler object." << ENDLOG;
	return false;
	}

    if ( fRORCMappingFilename && ( (fRORCTPCMappingPatchNr!=~0U) || fAliceHLT  ) )
	{
	if ( fRORCTPCMappingPatchNr==~0U )
	    {
	    if ( AliHLTReadoutList::GetDetectorID( fDDLID )!=kTPC )
		{
		LOG( AliHLTLog::kError, "RORCPublisherComponent::CreateParts", "RORC mapping only supported for TPC" )
		    << "RORC mapping files are currently only supported for TPC DDLs." << ENDLOG;
		return false;
		}
	    fRORCTPCMappingPatchNr = AliHLTReadoutList::GetTPCPatch( fDDLID );
	    }
	else if ( fAliceHLT && fRORCTPCMappingPatchNr!=AliHLTReadoutList::GetTPCPatch( fDDLID ) )
	    {
	    LOG( AliHLTLog::kWarning, "RORCPublisherComponent::CreateParts", "RORC TPC mapping patch number mismatch" )
		<< "Specified RORC TPC mapping patch number " << AliHLTLog::kDec << fRORCTPCMappingPatchNr
		<< " does not match patch number " << AliHLTReadoutList::GetTPCPatch( fDDLID ) << " extracted from DDL ID " << fDDLID
		<< "." << ENDLOG;
	    }
	fRORCMapping = new AliHLTRORCTPCMapping( fRORCTPCMappingPatchNr );
	if ( !fRORCMapping )
	    {
	    LOG( AliHLTLog::kError, "RORCPublisherComponent::CreateParts", "Unable to create TPC RORC mapping object" )
		<< "Unable to create RORC TPC mapping object." << ENDLOG;
	    return false;
	    }
	}

    return true;
    }

void RORCPublisherComponent::DestroyParts()
    {
    if ( fBufferManager )
	reinterpret_cast<AliHLTRORCBufferManager*>( fBufferManager )->SetRORCHandler( NULL );
    if ( fRORCHandler && fShmManager )
	{
	fShmManager->ReleaseShm( fShmKey );
	}
    ((AliHLTRORCPublisher*)fPublisher)->SetRORCHandler( NULL );
#ifdef PROFILING
    if ( fPublisher )
	((AliHLTRORCPublisher*)fPublisher)->WriteHistograms( fComponentName );
#endif
    AliHLTControlledComponent::DestroyParts();
    if ( fRORCMapping )
	{
	delete fRORCMapping;
	fRORCMapping = NULL;
	}
    if ( fRORCHandler )
	{
	fRORCHandler->DeinitializeRORC();
	fRORCHandler->Close();
	delete fRORCHandler;
	fRORCHandler = NULL;
	}
    }

bool RORCPublisherComponent::SetupComponents()
    {
    if ( !AliHLTControlledComponent::SetupComponents() )
	return false;

    if ( fPersistentState && fPersistentState->DidExist() && (*fPersistentState) && 
	 (*reinterpret_cast< AliHLTPersistentState<AliHLTDetectorPublisher::State>* >(fPersistentState))->fLastEventID!=AliEventID_t() )
	fFirstStart = false;

    if ( fBufferManager )
	reinterpret_cast<AliHLTRORCBufferManager*>( fBufferManager )->SetRORCHandler( fRORCHandler );

    AliUInt8_t* eventBuffer=NULL;
    eventBuffer = fShmManager->GetShm( fShmKey );
    int ret;
    ret = fRORCHandler->Open( fVendorDeviceIndexOrBusSlotFunction, fPCIVendorIDOrBus, fPCIDeviceIDOrSlot, fPCIDeviceIndexOrFunction, fPCIDeviceBarIndex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "RORCPublisherComponent::SetupComponents", "Error opening RORC" )
	    << "Error opening RORC device: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return false;
	}
    if ( fRORCVersion==4 )
	{
	tRegion eventBufferRegion;
	if ( fSharedMemory->GetPSIRegion( fShmKey, eventBufferRegion ) )
	    ((AliHLTRORC4Handler*)fRORCHandler)->SetEventBufferRegion( eventBufferRegion );
	}
    else if ( fRORCVersion==6 )
	{
	tRegion eventBufferRegion;
	if ( fSharedMemory->GetPSIRegion( fShmKey, eventBufferRegion ) )
	    ((AliHLTRORC6Handler*)fRORCHandler)->SetEventBufferRegion( eventBufferRegion );
	((AliHLTRORC6Handler*)fRORCHandler)->SetNoFlowControl( fNoFlowControl );
	if ( fEventSuppressionActive )
	    ((AliHLTRORC6Handler*)fRORCHandler)->SetEventSuppression( fEventSuppression_Exp2 );

	((AliHLTRORC6Handler*)fRORCHandler)->SetEnableHWCoProc( fHardwareCoProcessor );
	
	if ( fHardwareCoProcessor )
	    {
	    if ( fSinglePadSuppressionSet)
	        ((AliHLTRORC6Handler*)fRORCHandler)->SetSinglePadSuppression( fSinglePadSuppression );
	    if ( fBypassMergerSet )
		((AliHLTRORC6Handler*)fRORCHandler)->SetBypassMerger( fBypassMerger );
	    if ( fDeconvPadSet )
		((AliHLTRORC6Handler*)fRORCHandler)->SetDeconvPad( fDeconvPad );
	    if ( fClusterLowerLimitSet )
		((AliHLTRORC6Handler*)fRORCHandler)->SetClusterLowerLimit( fClusterLowerLimit );
	    if ( fSingleSeqLimitSet )
		((AliHLTRORC6Handler*)fRORCHandler)->SetSingleSeqLimit( fSingleSeqLimit );
	    if ( fMergerDistanceSet )
		((AliHLTRORC6Handler*)fRORCHandler)->SetMergerDistance( fMergerDistance );
	    if ( fMergerAlgorithmFollowSet )
		((AliHLTRORC6Handler*)fRORCHandler)->SetMergerAlgorithmFollow( fMergerAlgorithmFollow );
	    if ( fChargeToleranceSet )
		((AliHLTRORC6Handler*)fRORCHandler)->SetChargeTolerance( fChargeTolerance );
	    }
	}
    ret = fRORCHandler->SetEventBuffer( eventBuffer, fBufferSize-fWrapAreaSize, fFirstStart );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "RORCPublisherComponent::SetupComponents", "Error obtaining event buffer shared memory" )
	    << "Error obtaining event buffer shared memory: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return false;
	}
    fStatus->fHLTStatus.fTotalOutputBuffer = fBufferManager->GetTotalBufferSize();
    fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
    ret = fRORCHandler->InitializeRORC( fFirstStart );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "RORCPublisherComponent::SetupComponents", "Error initializing RORC" )
	    << "Error initializing RORC hardware: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return false;
	}
    ret = fRORCHandler->InitializeFromRORC( fFirstStart );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "RORCPublisherComponent::SetupComponents", "Error initializing from RORC" )
	    << "Error initializing RORC handler object from RORC hardware: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return false;
	}
    
    if ( !fSplitEventDataTypeSet )
	fSplitEventDataType.fID = fEventDataType.fID;
    if ( fAliceHLT )
	fSplitEventDataType.fID = DDLRAWDATASPLIT_DATAID;
    
	    
    ((AliHLTRORCPublisher*)fPublisher)->SetEventDataType( fEventDataType, fSplitEventDataType );
    ((AliHLTRORCPublisher*)fPublisher)->SetEventDataOrigin( fEventDataOrigin );
    ((AliHLTRORCPublisher*)fPublisher)->SetEventDataSpecification( fEventDataSpecification );
    ((AliHLTRORCPublisher*)fPublisher)->SetSleep( fDoSleep );
    if ( fNoStrictSOR )
	((AliHLTRORCPublisher*)fPublisher)->NoStrictSOR();
    if ( fRORCVersion != 5 )
	{
	((AliHLTRORCPublisher*)fPublisher)->SetShm( fShmKey, 0, eventBuffer, fBufferSize-fMinBlockSize );
	((AliHLTRORCPublisher*)fPublisher)->SetWrapCopyArea( fShmKey, fBufferSize-fWrapAreaSize, eventBuffer+(fBufferSize-fWrapAreaSize), fWrapAreaSize );
	}
    else
	{
	unsigned long size=0, offset=0;
	AliHLTShmID shmID;
	volatile uint8* ptr;
	switch ( fRORCVersion )
	    {
	    case 5:
		reinterpret_cast<AliHLTRORC5Handler*>( fRORCHandler )->GetEventBuffer( shmID, offset, ptr, size );
		((AliHLTRORCPublisher*)fPublisher)->SetShm( shmID, offset, ptr, size );
		((AliHLTRORCPublisher*)fPublisher)->SetWrapCopyArea( fShmKey, 0, eventBuffer, fWrapAreaSize );
		break;
	    }
	}
    ((AliHLTRORCPublisher*)fPublisher)->SetSleepTime( fSleepTime );
    ((AliHLTRORCPublisher*)fPublisher)->SetRORCHandler( fRORCHandler );
    ((AliHLTRORCPublisher*)fPublisher)->PublishCorruptEvents( fPublishCorruptEvents );
    ((AliHLTRORCPublisher*)fPublisher)->SuppressHWSuppressedEvents( fSuppressHWSuppressedEvents );
    ((AliHLTRORCPublisher*)fPublisher)->SuppressGoodEvents( fSuppressGoodEvents );
    if ( !fDummy )
	{
	switch ( fRORCVersion )
	    {
	    case 1:
		((AliHLTRORC1Handler*)fRORCHandler)->EnumerateEventIDs( fEnumerateEventIDs );
		((AliHLTRORC1Handler*)fRORCHandler)->SetHeaderSearch( fDDLHeaderSearch );
		((AliHLTRORC1Handler*)fRORCHandler)->SetReportWordConsistencyCheck( fReportWordConsistencyCheck );
		((AliHLTRORC1Handler*)fRORCHandler)->SetReportWordUseBlockSize( fReportWordUseBlockSize );
#ifdef COMPARE_COUNTER_PATTERN_TAGGED
		if ( fDoCompareCounterPattern )
		    {
		    ((AliHLTRORC1Handler*)fRORCHandler)->CompareCounterPattern( fDoCompareCounterPattern );
		    ((AliHLTRORC1Handler*)fRORCHandler)->SetCompareHighTag( fCompareHighTag );
		    }
#endif
		break;
	    case 2:
		((AliHLTRORC2Handler*)fRORCHandler)->EnumerateEventIDs( fEnumerateEventIDs );
		((AliHLTRORC2Handler*)fRORCHandler)->SetHeaderSearch( fDDLHeaderSearch );
		((AliHLTRORC2Handler*)fRORCHandler)->SetReportWordConsistencyCheck( fReportWordConsistencyCheck );
		((AliHLTRORC2Handler*)fRORCHandler)->SetReportWordUseBlockSize( fReportWordUseBlockSize );
#ifdef COMPARE_COUNTER_PATTERN_TAGGED
		if ( fDoCompareCounterPattern )
		    {
		    ((AliHLTRORC2Handler*)fRORCHandler)->CompareCounterPattern( fDoCompareCounterPattern );
		    ((AliHLTRORC2Handler*)fRORCHandler)->SetCompareHighTag( fCompareHighTag );
		    }
#endif
		break;
	    case 3:
		((AliHLTRORC3Handler*)fRORCHandler)->EnumerateEventIDs( fEnumerateEventIDs );
		((AliHLTRORC3Handler*)fRORCHandler)->SetHeaderSearch( fDDLHeaderSearch );
		((AliHLTRORC3Handler*)fRORCHandler)->SetReportWordConsistencyCheck( fReportWordConsistencyCheck );
		((AliHLTRORC3Handler*)fRORCHandler)->SetReportWordUseBlockSize( fReportWordUseBlockSize );
		((AliHLTRORC3Handler*)fRORCHandler)->SetTPCDataFix( fTPCDataFix );
		break;
	    case 4:
		((AliHLTRORC4Handler*)fRORCHandler)->EnumerateEventIDs( fEnumerateEventIDs );
		((AliHLTRORC4Handler*)fRORCHandler)->SetHeaderSearch( fDDLHeaderSearch );
		((AliHLTRORC4Handler*)fRORCHandler)->SetReportWordConsistencyCheck( fReportWordConsistencyCheck );
		((AliHLTRORC4Handler*)fRORCHandler)->SetReportWordUseBlockSize( fReportWordUseBlockSize );
		((AliHLTRORC4Handler*)fRORCHandler)->SetTPCDataFix( fTPCDataFix );
		break;
	    case 6:
		((AliHLTRORC6Handler*)fRORCHandler)->EnumerateEventIDs( fEnumerateEventIDs );
		((AliHLTRORC6Handler*)fRORCHandler)->SetHeaderSearch( fDDLHeaderSearch );
		((AliHLTRORC6Handler*)fRORCHandler)->SetReportWordConsistencyCheck( fReportWordConsistencyCheck );
		((AliHLTRORC6Handler*)fRORCHandler)->SetReportWordUseBlockSize( fReportWordUseBlockSize );
		((AliHLTRORC6Handler*)fRORCHandler)->SetTPCDataFix( fTPCDataFix );

		break;
	    }
	}
    else
	((AliHLTDummyRORCHandler*)fRORCHandler)->SetHeaderSearch( fDDLHeaderSearch );

    ((AliHLTRORCPublisher*)fPublisher)->SetEnableCorruptEventsAutoDump( fEnableCorruptEventsAutoDump );
    if ( fEnableCorruptEventsAutoDump )
	{
	((AliHLTRORCPublisher*)fPublisher)->SetCorruptEventsAutoDumpLimit( fCorruptEventsAutoDumpLimit );
	MLUCString prefix( fCorruptEventsAutoDumpDir );
	prefix += "/";
	prefix += fComponentName;
	prefix += "-CorruptEvent";
	((AliHLTRORCPublisher*)fPublisher)->SetCorruptEventsAutoDumpPrefix( prefix.c_str() );
	}

    if ( fRORCMappingFilename && fRORCMapping )
	{
	ret = fRORCMapping->Read( fRORCMappingFilename );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "RORCPublisherComponent::SetupComponents", "Error reading RORC mapping" )
		<< "Error reading RORC mapping table from file " << fRORCMappingFilename
		<< ": "  << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ") - " << fRORCMapping->GetLastError() << "." << ENDLOG;
	    return false;
	    }
	ret = fRORCMapping->WriteData( fRORCHandler );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "RORCPublisherComponent::SetupComponents", "Error writing RORC mapping to RORC device" )
		<< "Error writing RORC mapping table to RORC device: "  << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ") - " << fRORCMapping->GetLastError() << "." << ENDLOG;
	    return false;
	    }
	}

    if ( fHardwareCoProcessor && fRORCVersion>=6 )
      {
	if ( fSinglePadSuppressionSet)
	  ((AliHLTRORC6Handler*)fRORCHandler)->SetSinglePadSuppression( fSinglePadSuppression );
	if ( fBypassMergerSet )
	  ((AliHLTRORC6Handler*)fRORCHandler)->SetBypassMerger( fBypassMerger );
	if ( fDeconvPadSet )
	  ((AliHLTRORC6Handler*)fRORCHandler)->SetDeconvPad( fDeconvPad );
	if ( fClusterLowerLimitSet )
	  ((AliHLTRORC6Handler*)fRORCHandler)->SetClusterLowerLimit( fClusterLowerLimit );
	if ( fSingleSeqLimitSet )
	  ((AliHLTRORC6Handler*)fRORCHandler)->SetSingleSeqLimit( fSingleSeqLimit );
	if ( fMergerDistanceSet )
	  ((AliHLTRORC6Handler*)fRORCHandler)->SetMergerDistance( fMergerDistance );
	if ( fMergerAlgorithmFollowSet )
	  ((AliHLTRORC6Handler*)fRORCHandler)->SetMergerAlgorithmFollow( fMergerAlgorithmFollow );
	if ( fChargeToleranceSet )
	  ((AliHLTRORC6Handler*)fRORCHandler)->SetChargeTolerance( fChargeTolerance );
      }



    return true;
    }


AliHLTBufferManager* RORCPublisherComponent::CreateBufferManager()
    {
    return new AliHLTRORCBufferManager;
    }




int main( int argc, char** argv )
    {
    RORCPublisherComponent procComp( "RORCPublisher", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
