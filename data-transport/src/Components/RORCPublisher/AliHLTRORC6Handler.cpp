/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTRORC6Handler.hpp"
#include "AliHLTDDLHeader.hpp"
#include "AliHLTDDLHeaderData.h"
#include "AliHLTLog.hpp"
#include "AliHLTConfig.hpp"
#include "NOPEEvents.hpp"
#include <netinet/in.h>
#include <errno.h>
#include <cmath>


#if 0
#define REPORTBUFFERSIZEWORKAROUND 1
#endif
#define REPORTBUFFERMAXSIZE 65520


//#define RORC_DEBUG_USE_INTERNAL_PATTERNGENERATOR
#ifdef RORC_DEBUG_USE_INTERNAL_PATTERNGENERATOR
#warning RORC_DEBUG_USE_INTERNAL_PATTERNGENERATOR is enabled
#endif

#if 0
#define SIMULATE
#endif

#if 1
#define VERIFIED_WRITE
#endif

#ifdef SIMULATE
#warning Only simulating RORC accesses via signals. Disable the SIMULATE define two lines above to get real hardware access.
#include <signal.h>
static AliUInt64_t* gReportBufferReadPtr = 0;
static AliHLTRORC6Handler::AliVolatileUInt64_t* gAdminBufferReadPtr = 0;
static AliUInt64_t gEventBufferBusAddress = 0;
static AliUInt64_t gEventBufferSize = 0;
static AliHLTRORC6Handler::AliVolatileUInt8_t* gEventBufferPtr = 0;
static AliUInt64_t gOffsetWriteEnd = 0;
static AliUInt64_t gOffsetWrite = 0;
static AliUInt64_t gEventID = 0;
#define TRIGGER_SIGNAL SIGUSR1

#define SIMULATED_EVENT_32BIT_WORD_CNT 0x20000
#define SIMULATED_EVENT_SIZE_BYTES_REAL1 ( SIMULATED_EVENT_32BIT_WORD_CNT*sizeof(AliUInt32_t)+sizeof(AliUInt32_t)+( SIMULATED_EVENT_32BIT_WORD_CNT&1 ? 0 : sizeof(AliUInt32_t) ) )
#define BLOCKSIZE 128
#define BLOCKCOUNT ( (SIMULATED_EVENT_SIZE_BYTES_REAL1 / BLOCKSIZE) + ( (SIMULATED_EVENT_SIZE_BYTES_REAL1 % BLOCKSIZE) ? 1 : 0 ) )
#define SIMULATED_EVENT_SIZE_BYTES_REAL ( BLOCKCOUNT*BLOCKSIZE )

static AliHLTDDLHeaderDataV1 gCommonDataFormatHeader;

void TriggerSignalHandler( int )
    {
    bool wrote = false;
    LOG( AliHLTLog::kDebug, "TriggerSignalHandler", "Signal received" )
	<< "Received signal for simulated event generation - gOffsetWrite: "
	<< AliHLTLog::kDec << gOffsetWrite << " - gOffsetWriteEnd: "
	<< gOffsetWriteEnd << " - gReportBufferReadPtr: 0x"
	<< AliHLTLog::kHex << (unsigned long)gReportBufferReadPtr << " - gAdminBufferReadPtr: "
	<< (unsigned long)gAdminBufferReadPtr << " - gEventBufferBusAddress: 0x"
	<< gEventBufferBusAddress << " - gEventBufferPtr: 0x"
	<< (unsigned long)gEventBufferPtr << " - gEventID: 0x" << gEventID << AliHLTLog::kDec
	<< "/" << gEventID << " - SIMULATED_EVENT_32BIT_WORD_CNT: "
	<< SIMULATED_EVENT_32BIT_WORD_CNT << " - SIMULATED_EVENT_SIZE_BYTES_REAL: "
	<< SIMULATED_EVENT_SIZE_BYTES_REAL << "." << ENDLOG;
    AliUInt64_t oldOffsetWrite = gOffsetWrite;
    if ( SIMULATED_EVENT_SIZE_BYTES_REAL > gEventBufferSize )
	return;
    // Events (event IDs) will be lost, if the buffer is full. This is in purpose as a marker for such conditions.
    AliHLTSetDDLHeaderEventID1( &gCommonDataFormatHeader, gEventID & 0x0000000000000FFFULL );
    AliHLTSetDDLHeaderEventID2( &gCommonDataFormatHeader, (gEventID & 0x0000000FFFFFF000ULL) >> 12 );
    AliHLTSetDDLHeaderMiniEventID( &gCommonDataFormatHeader, gEventID & 0x0000000000000FFFULL );
    gEventID = (gEventID+1) & 0x0000000FFFFFFFFFULL;
    if ( gReportBufferReadPtr && *(AliHLTRORC6Handler::AliVolatileUInt64_t*)(gReportBufferReadPtr)==0xFFFFFFFFFFFFFFFFULL )
	{

	if ( gOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL < gEventBufferSize )
	    {
	    // write pointer will  not make wrap around
	    if ( gOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL >= gOffsetWriteEnd && gOffsetWrite<gOffsetWriteEnd )
		{
		// write pointer will stay less than write end pointer, or write end pointer has made wrap around and write pointer will not.
		// update write end pointer
		gOffsetWriteEnd = ((AliUInt64_t)(*((AliHLTRORC6Handler::AliVolatileUInt64_t*)gAdminBufferReadPtr))) - gEventBufferBusAddress;
		}
	    // try again
	    if ( gOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL < gOffsetWriteEnd || gOffsetWrite>gOffsetWriteEnd )
		{
		// write pointer will stay less than write end pointer, or write end pointer has made wrap around and write pointer will not.
		memcpy( (AliUInt8_t*)gEventBufferPtr+gOffsetWrite, &gCommonDataFormatHeader, sizeof(gCommonDataFormatHeader) );
		AliHLTRORC6Handler::AliVolatileUInt32_t* start = (AliHLTRORC6Handler::AliVolatileUInt32_t*)(gEventBufferPtr+gOffsetWrite+sizeof(gCommonDataFormatHeader));
		for ( unsigned long i=0; i<SIMULATED_EVENT_32BIT_WORD_CNT-sizeof(gCommonDataFormatHeader)/sizeof(AliUInt32_t); i++ )
		    *(start+i) = i;
		gOffsetWrite += SIMULATED_EVENT_SIZE_BYTES_REAL;
		wrote = true;
		}
	    }
	else
	    {
	    // write pointer will make wrap around.
	    if ( gOffsetWrite<gOffsetWriteEnd || gOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL>=gEventBufferSize+gOffsetWriteEnd )
		{
		// write end pointer has not made wrap around yet or write wrap around would go beyond wrapped write end pointer
		// update write end pointer
		gOffsetWriteEnd = ((AliUInt64_t)(*((AliHLTRORC6Handler::AliVolatileUInt64_t*)gAdminBufferReadPtr))) - gEventBufferBusAddress;
		}
	    // try again
	    if ( gOffsetWrite>=gOffsetWriteEnd && gOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL<gEventBufferSize+gOffsetWriteEnd )
		{
		if ( gOffsetWrite+sizeof(gCommonDataFormatHeader) <= gEventBufferSize )
		    {
		    memcpy( (AliUInt8_t*)gEventBufferPtr+gOffsetWrite, &gCommonDataFormatHeader, sizeof(gCommonDataFormatHeader) );
		    AliHLTRORC6Handler::AliVolatileUInt32_t* start = (AliHLTRORC6Handler::AliVolatileUInt32_t*)(gEventBufferPtr+gOffsetWrite+sizeof(gCommonDataFormatHeader));
		    unsigned long firstWordCnt = (gEventBufferSize - (gOffsetWrite+sizeof(gCommonDataFormatHeader))) / sizeof(AliUInt32_t);
		    for ( unsigned long i=0; i<firstWordCnt; i++ )
			*(start+i) = i;
		    start = (AliHLTRORC6Handler::AliVolatileUInt32_t*)gEventBufferPtr;
		    for ( unsigned long i=0; i<SIMULATED_EVENT_32BIT_WORD_CNT-firstWordCnt-sizeof(gCommonDataFormatHeader)/sizeof(AliUInt32_t); i++ )
			*(start+i) = i+firstWordCnt;
		    }
		else
		    {
		    unsigned long firstPart = gOffsetWrite+sizeof(gCommonDataFormatHeader)-gEventBufferSize;
		    memcpy( (AliUInt8_t*)gEventBufferPtr+gOffsetWrite, &gCommonDataFormatHeader, firstPart );
		    memcpy( (AliUInt8_t*)gEventBufferPtr, ((AliUInt8_t*)&gCommonDataFormatHeader)+firstPart, sizeof(gCommonDataFormatHeader)-firstPart );
		    AliHLTRORC6Handler::AliVolatileUInt32_t* start = (AliHLTRORC6Handler::AliVolatileUInt32_t*)(gEventBufferPtr+sizeof(gCommonDataFormatHeader)-firstPart);
		    for ( unsigned long i=0; i<SIMULATED_EVENT_32BIT_WORD_CNT-sizeof(gCommonDataFormatHeader)/sizeof(AliUInt32_t); i++ )
			*(start+i) = i;
		    }
		gOffsetWrite = gOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL-gEventBufferSize;
		wrote = true;
		}
	    }

	//printf( "Force triggering event at address 0x%08lX\n", (unsigned long)gReportBufferReadPtr );
	if ( wrote )
	    {
	    *(AliHLTRORC6Handler::AliVolatileUInt64_t*)(gReportBufferReadPtr) = gEventBufferBusAddress+oldOffsetWrite;
	    *(((AliHLTRORC6Handler::AliVolatileUInt64_t*)gReportBufferReadPtr)+1) = gEventBufferBusAddress+oldOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL;
	    *(((AliHLTRORC6Handler::AliVolatileUInt64_t*)gReportBufferReadPtr)+2) = ( ((AliUInt64_t)SIMULATED_EVENT_32BIT_WORD_CNT) << 32 ) | ((AliUInt64_t)SIMULATED_EVENT_32BIT_WORD_CNT);
	    //*(((AliHLTRORC6Handler::AliVolatileUInt32_t*)gReportBufferReadPtr)+3) = SIMULATED_EVENT_32BIT_WORD_CNT;
	    gReportBufferReadPtr += sizeof(AliUInt64_t)*3;
	    }
	}
    LOG( AliHLTLog::kDebug, "TriggerSignalHandler", "Event Written" )
	<< "Received signal for simulated event generation - gOffsetWrite: "
	<< AliHLTLog::kDec << gOffsetWrite << " - gOffsetWriteEnd: "
	<< gOffsetWriteEnd << " - gReportBufferReadPtr: 0x"
	<< AliHLTLog::kHex << (unsigned long)gReportBufferReadPtr << " - gAdminBufferReadPtr: "
	<< (unsigned long)gAdminBufferReadPtr << " - gEventBufferBusAddress: 0x"
	<< gEventBufferBusAddress << " - gEventBufferPtr: 0x"
	<< (unsigned long)gEventBufferPtr << " - gEventID: 0x" << gEventID << AliHLTLog::kDec
	<< "/" << gEventID << " - SIMULATED_EVENT_32BIT_WORD_CNT: "
	<< SIMULATED_EVENT_32BIT_WORD_CNT << " - SIMULATED_EVENT_SIZE_BYTES_REAL: "
	<< SIMULATED_EVENT_SIZE_BYTES_REAL << " - wrote: "
	<< ( wrote ? "true" : "false" ) << "." << ENDLOG;
    }
#endif


const AliUInt32_t AliHLTRORC6Handler::kEVTBUFPTRLOW           = 0x00;
const AliUInt32_t AliHLTRORC6Handler::kEVTBUFPTRHIGH          = 0x04;
const AliUInt32_t AliHLTRORC6Handler::kEVTBUFENDLOW           = 0x08;
const AliUInt32_t AliHLTRORC6Handler::kEVTBUFENDHIGH          = 0x0C;

const AliUInt32_t AliHLTRORC6Handler::kREPBUFPTRLOW           = 0x10;
const AliUInt32_t AliHLTRORC6Handler::kREPBUFPTRHIGH          = 0x14;
const AliUInt32_t AliHLTRORC6Handler::kREPBUFENDLOW           = 0x18;
const AliUInt32_t AliHLTRORC6Handler::kREPBUFENDHIGH          = 0x1C;

const AliUInt32_t AliHLTRORC6Handler::kADMBUFPTRREADLOW       = 0x20;
const AliUInt32_t AliHLTRORC6Handler::kADMBUFPTRREADHIGH      = 0x24;
const AliUInt32_t AliHLTRORC6Handler::kEVTBUFPTRREADLOW       = 0x28;
const AliUInt32_t AliHLTRORC6Handler::kEVTBUFPTRREADHIGH      = 0x2C;

const AliUInt32_t AliHLTRORC6Handler::kFIFOFLUSH = 0x30;
const AliUInt32_t AliHLTRORC6Handler::kDMAENABLE = 0x34;
const AliUInt32_t AliHLTRORC6Handler::kFIFOFULLCOUNT = 0xC8;
const AliUInt32_t AliHLTRORC6Handler::kENABLEFLOWCONTROL = 0xD0;
const AliUInt32_t AliHLTRORC6Handler::kEVENTCOUNTERRESET = 0xD4;
const AliUInt32_t AliHLTRORC6Handler::kDDLCONTROL = 0xE8;

const AliUInt32_t AliHLTRORC6Handler::kFIFOFLUSHEMPTY         = 1 << 29;


const AliUInt32_t AliHLTRORC6Handler::kFCFCONTROL          = 0x100;
const AliUInt32_t AliHLTRORC6Handler::kFCFCONTROLEXT       = 0x114;
const AliUInt32_t AliHLTRORC6Handler::kFCFCONFIGMEMADDRESS = 0x108;
const AliUInt32_t AliHLTRORC6Handler::kFCFCONFIGMEMDATAWR  = 0x10C;
const AliUInt32_t AliHLTRORC6Handler::kFCFCONFIGMEMDATARD  = 0x110;

const AliUInt32_t AliHLTRORC6Handler::kFCFCONTROLRESET                    = 1 << 31;
const AliUInt32_t AliHLTRORC6Handler::kFCFCONTROLFLOWCONTROL              = 1 << 29;
const AliUInt32_t AliHLTRORC6Handler::kFCFCONTROLENABLE                   = 1 << 28;
const AliUInt32_t AliHLTRORC6Handler::kFCFCONTROLMERGERBYPASS             = 1 << 27;
const AliUInt32_t AliHLTRORC6Handler::kFCFCONTROLSINGLEPADDSUPPRESSION    = 1 << 26;
const AliUInt32_t AliHLTRORC6Handler::kFCFCONTROLDECONVPAD                = 1 << 25;

const AliUInt32_t AliHLTRORC6Handler::kFCFCONTROLCLUSTERLOWERLIMITMASK    = 0xFFFF;
const AliUInt32_t AliHLTRORC6Handler::kFCFCONTROLCLUSTERLOWERLIMITSHIFT   = 8;
const AliUInt32_t AliHLTRORC6Handler::kFCFCONTROLSINGLESEQUENCELIMITMASK  = 0xFF;
const AliUInt32_t AliHLTRORC6Handler::kFCFCONTROLSINGLESEQUENCELIMITSHIFT = 0;

const AliUInt32_t AliHLTRORC6Handler::kFCFCONTROLMERGERDISTANCEMASK       = 0xF;
const AliUInt32_t AliHLTRORC6Handler::kFCFCONTROLMERGERDISTANCESHIFT      = 0;
const AliUInt32_t AliHLTRORC6Handler::kFCFCONTROLMERGERALGORITHMFOLLOW    = 1 << 4;
const AliUInt32_t AliHLTRORC6Handler::kFCFCONTROLCHARGETOLERANCEMASK      = 0xF;
const AliUInt32_t AliHLTRORC6Handler::kFCFCONTROLCHARGETOLERANCESHIFT     = 5;


const AliUInt32_t AliHLTRORC6Handler::kREPORTERROR = 0x100;



#define ORBIT_PERIOD_SECONDS_DOUBLE 1492.0

AliHLTRORC6Handler::AliHLTRORC6Handler( unsigned long dataBufferSize, unsigned long rorcBlockSize, int eventSlotsExp2 ):
    fEvents( eventSlotsExp2 ),
    fNextInvalidEventID( kAliEventTypeCorruptID, 0 )
    {
#ifdef SIMULATE
    if ( rorcBlockSize != BLOCKSIZE )
	{
	LOG( AliHLTLog::kWarning, "AliHLTRORC6Handler::AliHLTRORC6Handler", "Simulation blocksize setting" )
	    << "Fixing simulated RORC block size to " << AliHLTLog::kDec << BLOCKSIZE << " B." << ENDLOG;
	rorcBlockSize = BLOCKSIZE;
	}
#endif
    fReportBufferSize = (dataBufferSize/rorcBlockSize)*sizeof(AliUInt64_t)*3;
    if ( dataBufferSize % rorcBlockSize )
	fReportBufferSize += sizeof(AliUInt64_t)*3;
#ifdef REPORTBUFFERSIZEWORKAROUND
    if ( fReportBufferSize > REPORTBUFFERMAXSIZE )
      {
	LOG( AliHLTLog::kWarning, "AliHLTRORC6Handler::AliHLTRORC6Handler", "Report Buffer Size Truncating Workaround" )
	  << "Truncating report buffer size as workaround to "
	  << AliHLTLog::kDec << REPORTBUFFERMAXSIZE << " bytes. (From "
	  << AliHLTLog::kDec << fReportBufferSize << " (0x"
	  << AliHLTLog::kHex << fReportBufferSize << "))." << ENDLOG;
	fReportBufferSize = REPORTBUFFERMAXSIZE;
      }
#endif
    fAdminBufferSize = 4096;
    pthread_mutex_init( &fEventMutex, NULL );

    fEnumerateEventIDs = false;
    fNextEventID = 0;

    fLastRORCEventID = AliEventID_t();
    fAddEventIDCounter = (~(AliUInt64_t)0) << 36;
    fLastLogMessageEventIDCounter = fAddEventIDCounter;

    fLastEventIDWrapAround.tv_sec = fLastEventIDWrapAround.tv_usec = 0;

    fHeaderSearch = true;
    fReportWordConsistencyCheck = false;
    fReportWordUseBlockSize = false;

    fTPCDataFix = false;

#ifdef COMPARE_SOFTWARE_EVENTBUFFERREADPTR_CARD_DMA_ADDR
    fBarPtr = NULL;
#endif
#ifdef SIMULATE
    signal( TRIGGER_SIGNAL, TriggerSignalHandler );
    memset( &gCommonDataFormatHeader, 0, sizeof(gCommonDataFormatHeader) );
    AliHLTSetDDLHeaderBlockLength( &gCommonDataFormatHeader, SIMULATED_EVENT_32BIT_WORD_CNT*sizeof(AliUInt32_t) );
    AliHLTSetDDLHeaderVersion( &gCommonDataFormatHeader, 1 );
#endif
#ifdef COMPARE_COUNTER_PATTERN_TAGGED
    fCompareHighTag = 0;
    fDoCompareCounterPattern = false;
#endif
    fRORCBlockSize = rorcBlockSize;
    fRORCBlock32bitWords = fRORCBlockSize / 4;
    fEventCounter = 0;
    fEventBufferSet = false;
    fEventBufferRegionSet = false;
    fNoFlowControl = false;
    fEventSuppression_Exp2 = 0;
    fEventSuppressionActive = false;
    fEnableHWCoProc = false;
    fSinglePadSuppression = false;
    fSinglePadSuppressionSet = false;
    fBypassMerger = false;
    fBypassMergerSet = false;
    fDeconvPad = true;
    fDeconvPadSet = false;
    fClusterLowerLimit = 0;
    fClusterLowerLimitSet = false;
    fSingleSeqLimit = 0;
    fSingleSeqLimitSet = false;
    fMergerDistance = 4;
    fMergerDistanceSet = false;
    fMergerAlgorithmFollow = true;
    fMergerAlgorithmFollowSet = false;
    fChargeTolerance = 0;
    fChargeToleranceSet = false;
    fNoPeriodCounterLogModulo = 1;
    fPeriodCounterLogCounter = 0;
    }

AliHLTRORC6Handler::~AliHLTRORC6Handler()
    {
    pthread_mutex_destroy( &fEventMutex );
    }

int AliHLTRORC6Handler::Open( bool vendorDeviceIndexOrBusSlotFunction, AliUInt16_t pciVendorIDOrBus, AliUInt16_t pciDeviceIDOrSlot, AliUInt16_t pciDeviceIndexOrFunction, AliUInt16_t pciDeviceBarIndex )
    {
    char tmp[ 512+1 ];
    PSI_Status    status;

    if ( vendorDeviceIndexOrBusSlotFunction )
	{
	fBarID = "/dev/psi/vendor/"; // 0x%04hX/device/0x%04hX/0/base0
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fBarID += tmp;
	fBarID += "/device/";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fBarID += tmp;
	fBarID += "/";
	snprintf( tmp, 512, "%04X", pciDeviceIndexOrFunction );
	fBarID += tmp;
	fBarID += "/base";
	snprintf( tmp, 512, "%d", pciDeviceBarIndex );
	fBarID += tmp;
	
	fReportBufferID = "/dev/psi/bigphys/HLT-RORC-Report-VDI-";
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIndexOrFunction );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%d", pciDeviceBarIndex );
	fReportBufferID += tmp;
	
	fAdminBufferID = "/dev/psi/bigphys/HLT-RORC-Admin-VDI-";
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fAdminBufferID += tmp;
	fAdminBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fAdminBufferID += tmp;
	fAdminBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIndexOrFunction );
	fAdminBufferID += tmp;
	fAdminBufferID += "-";
	snprintf( tmp, 512, "0x%d", pciDeviceBarIndex );
	fAdminBufferID += tmp;
	}
    else
	{
	fBarID = "/dev/psi/bus/"; // 0x%04hX/device/0x%04hX/0/base0
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fBarID += tmp;
	fBarID += "/slot/";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fBarID += tmp;
	fBarID += "/function/";
	snprintf( tmp, 512, "%04X", pciDeviceIndexOrFunction );
	fBarID += tmp;
	fBarID += "/base";
	snprintf( tmp, 512, "%d", pciDeviceBarIndex );
	fBarID += tmp;
	
	fReportBufferID = "/dev/psi/bigphys/HLT-RORC-Report-BSF-";
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIndexOrFunction );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%d", pciDeviceBarIndex );
	fReportBufferID += tmp;
	
	fAdminBufferID = "/dev/psi/bigphys/HLT-RORC-Admin-BSF-";
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fAdminBufferID += tmp;
	fAdminBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fAdminBufferID += tmp;
	fAdminBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIndexOrFunction );
	fAdminBufferID += tmp;
	fAdminBufferID += "-";
	snprintf( tmp, 512, "0x%d", pciDeviceBarIndex );
	fAdminBufferID += tmp;
	}

#ifdef SIMULATE
    LOG( AliHLTLog::kWarning, "AliHLTRORC6Handler::Open", "SIMULATING RORC" )
	<< "SIMULATING RORC, NOT ACCESSING ANY REAL HARDWARE." << ENDLOG;
#endif
#ifndef SIMULATE
    status = PSI_openRegion( &fBarRegion, fBarID.c_str() );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Open", "Unable to open RORC device bar region" )
	    << "Unable to open RORC device bar " << AliHLTLog::kDec << pciDeviceBarIndex << " region '" << fBarID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_setDMAMask( fBarRegion, 0xFFFFFFFFFFFFFFFFULL );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Open", "Unable to set DMA mask for PCI device" )
	    << "Unable to set DMA mask for RORC device: '" << fBarID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

#ifdef COMPARE_SOFTWARE_EVENTBUFFERREADPTR_CARD_DMA_ADDR
#ifndef SIMULATE
    status = PSI_mapRegion( fBarRegion, (void**)&fBarPtr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Open", "Unable to map RORC device bar 0 region" )
	    << "Unable to map RORC device bar 0 region '" << fBarID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    status = PSI_openRegion( &fReportBufferRegion, fReportBufferID.c_str() );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Open", "Unable to open report buffer shm region" )
	    << "Unable to open report buffer shm region '" << fReportBufferID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBarRegion );
	return EIO;
	}

    status = PSI_openRegion( &fAdminBufferRegion, fAdminBufferID.c_str() );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Open", "Unable to open admin buffer shm region" )
	    << "Unable to open admin buffer shm region '" << fAdminBufferID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBarRegion );
	PSI_closeRegion( fReportBufferRegion );
	return EIO;
	}

    unsigned long cur_size = (unsigned long)fReportBufferSize;

    status = PSI_sizeRegion( fReportBufferRegion, &cur_size );
    if ( status != PSI_OK && status != PSI_SIZE_SET )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Open", "Unable to set size for report buffer shm region" )
	    << "Unable to set size for report buffer shm region '" << fReportBufferID.c_str()
	    << "' to size " << AliHLTLog::kDec << fReportBufferSize << " (0x" << AliHLTLog::kHex
	    << fReportBufferSize << "): " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBarRegion );
	PSI_closeRegion( fReportBufferRegion );
	PSI_closeRegion( fAdminBufferRegion );
	return EIO;
	}

    if ( cur_size != fReportBufferSize )
	{
	LOG( AliHLTLog::kInformational, "AliHLTRORC6Handler::Open", "Report buffer size changed" )
	    << "Report buffer size is now " << AliHLTLog::kDec << cur_size << " (0x" << AliHLTLog::kHex
	    << cur_size << ") instead of the specified " << AliHLTLog::kDec << fReportBufferSize << " (0x" << AliHLTLog::kHex
	    << fReportBufferSize << ")." << ENDLOG;
	fReportBufferSize = cur_size;
#ifdef REPORTBUFFERSIZEWORKAROUND
	if ( fReportBufferSize > REPORTBUFFERMAXSIZE )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTRORC6Handler::AliHLTRORC6Handler", "Report Buffer Size Truncating Workaround" )
		<< "Truncating report buffer size as workaround to "
		<< AliHLTLog::kDec << REPORTBUFFERMAXSIZE << " bytes. (From "
		<< AliHLTLog::kDec << fReportBufferSize << " (0x"
		<< AliHLTLog::kHex << fReportBufferSize << "))." << ENDLOG;
	    fReportBufferSize = REPORTBUFFERMAXSIZE;
	    }
#endif
	}
    else
	{
	LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::Open", "Report buffer size" )
	    << "Report buffer size is " << AliHLTLog::kDec << fReportBufferSize << " (0x" << AliHLTLog::kHex
	    << fReportBufferSize << ")." << ENDLOG;
	}
    //status = PSI_mapRegion( fReportBufferRegion, reinterpret_cast<void**>(&fReportBuffer) );
    status = PSI_mapRegion( fReportBufferRegion, (void**)&fReportBuffer );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Open", "Unable to map report buffer shm region" )
	    << "Unable to map report buffer shm region '" << fReportBufferID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBarRegion );
	PSI_closeRegion( fReportBufferRegion );
	PSI_closeRegion( fAdminBufferRegion );
	return EIO;
	}
    fReportBufferEnd = fReportBuffer + fReportBufferSize;
    fReportBufferReadPtr = fReportBuffer;
#ifdef SIMULATE
    gReportBufferReadPtr = (AliUInt64_t*)fReportBufferReadPtr;
#endif
    fReportBufferOldReadPtr = NULL;
    //status = PSI_getPhysAddress( reinterpret_cast<void*>(fReportBuffer), reinterpret_cast<void**>(&fReportBufferPhysAddress), reinterpret_cast<void**>(&fReportBufferBusAddress) );
    unsigned long busAddress=0;
    status = PSI_mapDMA( fReportBufferRegion, fBarRegion, _fromDevice_, &busAddress );
    fReportBufferBusAddress = busAddress;
    fReportBufferEndBusAddress = fReportBufferBusAddress + fReportBufferSize;
    status = PSI_getPhysAddress( (void*)fReportBuffer, (void**)&fReportBufferPhysAddress, reinterpret_cast<void**>(&busAddress) );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Open", "Unable to obtain report buffer physical/bus address" )
	    << "Unable to obtain physical and bus address for report buffer: " 
	    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	//PSI_unmapRegion( fReportBufferRegion, reinterpret_cast<void*>(fReportBuffer) );
	PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
	PSI_closeRegion( fBarRegion );
	PSI_closeRegion( fReportBufferRegion );
	PSI_closeRegion( fAdminBufferRegion );
	return EIO;
	}
    LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::Open", "Report buffer pointers" )
	<< "ReportBuffer: 0x" << AliHLTLog::kHex << (unsigned long)fReportBuffer << " - ReportBuffer Phys: 0x"
	<< (unsigned long)fReportBufferPhysAddress << " - ReportBuffer Bus: 0x" << (unsigned long)fReportBufferBusAddress
	<< "." << ENDLOG;

    cur_size = (unsigned long)fAdminBufferSize;
    status = PSI_sizeRegion( fAdminBufferRegion, &cur_size );
    if ( status != PSI_OK && status != PSI_SIZE_SET )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Open", "Unable to set size for admin buffer shm region" )
	    << "Unable to set size for admin buffer shm region '" << fAdminBufferID.c_str()
	    << "' to size " << AliHLTLog::kDec << fAdminBufferSize << " (0x" << AliHLTLog::kHex
	    << fAdminBufferSize << "): " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	//PSI_unmapRegion( fReportBufferRegion, fReportBuffer );
	PSI_unmapDMA( fReportBufferRegion );
	PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
	PSI_closeRegion( fBarRegion );
	PSI_closeRegion( fReportBufferRegion );
	PSI_closeRegion( fAdminBufferRegion );
	return EIO;
	}

    if ( cur_size != fAdminBufferSize )
	{
	LOG( AliHLTLog::kInformational, "AliHLTRORC6Handler::Open", "Admin buffer size changed" )
	    << "Admin buffer size is now " << AliHLTLog::kDec << cur_size << " (0x" << AliHLTLog::kHex
	    << cur_size << ") instead of the specified " << AliHLTLog::kDec << fAdminBufferSize << " (0x" << AliHLTLog::kHex
	    << fAdminBufferSize << ")." << ENDLOG;
	fAdminBufferSize = cur_size;
	}
    else
	{
	LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::Open", "Admin buffer size" )
	    << "Admin buffer size is " << AliHLTLog::kDec << fAdminBufferSize << " (0x" << AliHLTLog::kHex
	    << fAdminBufferSize << ")." << ENDLOG;
	}
    //status = PSI_mapRegion( fAdminBufferRegion, reinterpret_cast<void**>(&fAdminBuffer) );
    status = PSI_mapRegion( fAdminBufferRegion, (void**)&fAdminBuffer );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Open", "Unable to map admin buffer shm region" )
	    << "Unable to map admin buffer shm region '" << fAdminBufferID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	//PSI_unmapRegion( fReportBufferRegion, fReportBuffer );
	PSI_unmapDMA( fReportBufferRegion );
	PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
	PSI_closeRegion( fBarRegion );
	PSI_closeRegion( fReportBufferRegion );
	PSI_closeRegion( fAdminBufferRegion );
	return EIO;
	}
#ifdef SIMULATE
    gAdminBufferReadPtr = (AliVolatileUInt64_t*)fAdminBuffer;
#endif
    //status = PSI_getPhysAddress( fAdminBuffer, reinterpret_cast<void**>(&fAdminBufferPhysAddress), reinterpret_cast<void**>(&fAdminBufferBusAddress) );
    busAddress = 0;
    status = PSI_mapDMA( fAdminBufferRegion, fBarRegion, _toDevice_, &busAddress );
    fAdminBufferBusAddress = busAddress;
    status = PSI_getPhysAddress( (void*)fAdminBuffer, (void**)&fAdminBufferPhysAddress, (void**)&fAdminBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Open", "Unable to obtain admin buffer physical/bus address" )
	    << "Unable to obtain physical and bus address for admin buffer: " 
	    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	//PSI_unmapRegion( fReportBufferRegion, fReportBuffer );
	PSI_unmapDMA( fReportBufferRegion );
	PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
	//PSI_unmapRegion( fAdminBufferRegion, fAdminBuffer );
	PSI_unmapRegion( fAdminBufferRegion, (void*)fAdminBuffer );
	PSI_closeRegion( fBarRegion );
	PSI_closeRegion( fReportBufferRegion );
	PSI_closeRegion( fAdminBufferRegion );
	return EIO;
	}
    LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::Open", "Admin buffer pointers" )
	<< "AdminBuffer: 0x" << AliHLTLog::kHex << (unsigned long)fAdminBuffer << " - AdminBuffer Phys: 0x"
	<< (unsigned long)fAdminBufferPhysAddress << " - AdminBuffer Bus: 0x" << (unsigned long)fAdminBufferBusAddress
	<< "." << ENDLOG;

    return 0;
    }
	
int AliHLTRORC6Handler::Close()
    {
    //PSI_unmapRegion( fReportBufferRegion, fReportBuffer );
    if ( fEventBufferSet )
	PSI_unmapDMA( fEventBufferRegion );
    fEventBufferSet = false;
    PSI_unmapDMA( fAdminBufferRegion );
PSI_unmapDMA( fReportBufferRegion );
    PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
    //PSI_unmapRegion( fAdminBufferRegion, fAdminBuffer );
    PSI_unmapRegion( fAdminBufferRegion, (void*)fAdminBuffer );
    PSI_closeRegion( fBarRegion );
    PSI_closeRegion( fReportBufferRegion );
    PSI_closeRegion( fAdminBufferRegion );
    return 0;
    }

void AliHLTRORC6Handler::SetEventBufferRegion( tRegion eventBufferRegion )
    {
    fEventBufferRegionSet = true;
    fEventBufferRegion = eventBufferRegion;
    }

int AliHLTRORC6Handler::SetEventBuffer( AliUInt8_t* dataBuffer, unsigned long dataBufferSize, bool first )
    {
    PSI_Status    status;

    fEventBufferReadPtr = fEventBuffer = dataBuffer;
    fEventBufferSize = dataBufferSize;
    fFreeBufferSize = fEventBufferSize;
    fEventBufferEnd = fEventBuffer + fEventBufferSize;
    fEventBufferSet = true;
    //status = PSI_getPhysAddress( fEventBuffer, reinterpret_cast<void**>(&fEventBufferPhysAddress), reinterpret_cast<void**>(&fEventBufferBusAddress) );
    if ( fEventBufferRegionSet )
	{
	unsigned long busAddress=0;
	status = PSI_mapDMA( fEventBufferRegion, fBarRegion, _fromDevice_, &busAddress );
	fEventBufferBusAddress = busAddress;
	}
    status = PSI_getPhysAddress( (void*)fEventBuffer, (void**)&fEventBufferPhysAddress, (void**)&fEventBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::SetEventBuffer", "Unable to obtain event buffer physical/bus address" )
	    << "Unable to obtain physical and bus address for event buffer: " 
	    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#ifdef SIMULATE
    gEventBufferPtr = fEventBufferReadPtr;
    gOffsetWriteEnd = fEventBufferSize;
    gEventBufferBusAddress = fEventBufferBusAddress;
    gEventBufferSize = fEventBufferSize;
#endif
    fEventBufferReadPtrPhysAddress = fEventBufferPhysAddress;
    fEventBufferEndPhysAddress = fEventBufferPhysAddress + fEventBufferSize;
    fEventBufferReadPtrBusAddress = fEventBufferBusAddress;
    fEventBufferEndBusAddress = fEventBufferBusAddress + fEventBufferSize;
    fEventBufferReleasedOffset = 0;
        
    LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::SetEventBuffer", "Event buffer pointers" )
	<< "EventBuffer: 0x" << AliHLTLog::kHex << (unsigned long)fEventBuffer << " - EventBuffer Phys: 0x"
	<< (unsigned long)fEventBufferPhysAddress << " - EventBuffer Bus: 0x" << (unsigned long)fEventBufferBusAddress
	<< " - EventBuffer size: 0x" << fEventBufferSize << " (" << AliHLTLog::kDec << fEventBufferSize << ")." << ENDLOG;
    
    if ( first )
	*(AliVolatileUInt64_t*)fAdminBuffer = (AliUInt64_t)fEventBufferReadPtrBusAddress;

    return 0;
    }

int AliHLTRORC6Handler::InitializeRORC( bool first )
    {
    PSI_Status    status;
    AliUInt32_t wr;
    int ret;

#ifdef VERIFIED_WRITE
    // disable DMA
    ret = Write32Control( (AliUInt32_t)kDMAENABLE, 0x00000000, true, 0x00000000, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#else

    // disable DMA, do it three times, in the hopes that at least one will get through
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    // disable DIU interface
#ifdef VERIFIED_WRITE
    ret = Write32Control( (AliUInt32_t)kDDLCONTROL, 0x00000000, true, 0x00000000, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#else
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDDLCONTROL, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    if ( fEnableHWCoProc )
	{
	// Reset HW Co-proc.
	/* Cannot verify anymore, register is filled with default values
#ifdef VERIFIED_WRITE
	ret = Write32Control( (AliUInt32_t)kFCFCONTROL, (AliUInt32_t)kFCFCONTROLRESET, true, (AliUInt32_t)0x00000000, (AliUInt32_t)0xFFFFFFFF, true, status );
	if ( ret || status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
		<< "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
		<< ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		<< ")." << ENDLOG;
	    return EIO;
	    }
	ret = Write32Control( (AliUInt32_t)kFCFCONTROL, (AliUInt32_t)0x00000000, true, (AliUInt32_t)0x00000000, (AliUInt32_t)0xFFFFFFFF, true, status );
	if ( ret || status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
		<< "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
		<< ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		<< ")." << ENDLOG;
	    return EIO;
	    }
#else
	*/
	wr=kFCFCONTROLRESET;
#ifndef SIMULATE
	status = PSI_write( fBarRegion, (AliUInt32_t)kFCFCONTROL, _dword_, 4, (void*)&wr );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
		<< "PCI address write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		<< ")." << ENDLOG;
	    return EIO;
	    }
#endif
	/*
	wr=0x00000000;
#ifndef SIMULATE
	status = PSI_write( fBarRegion, (AliUInt32_t)kFCFCONTROL, _dword_, 4, (void*)&wr );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
		<< "PCI address write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		<< ")." << ENDLOG;
	    return EIO;
	    }
#endif
#endif
	*/
	}

    unsigned long cnt=0;
    unsigned long rd=0;
    do
	{
	// flush FIFO
	wr = 0x00000001;
#ifndef SIMULATE
	status = PSI_write( fBarRegion, (AliUInt32_t)kFIFOFLUSH, _dword_, 4, (void*)&wr );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
		<< "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		<< ")." << ENDLOG;
	    return EIO;
	    }
#endif
	rd=0;
	// read FIFO status to verify flush (content can be reinserted after flush by internal FIFO on DIU...)
#ifndef SIMULATE
	status = PSI_read( fBarRegion, (AliUInt32_t)kFIFOFLUSH, _dword_, 4, (void*)&rd );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Read Error" )
		<< "PCI read error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		<< ")." << ENDLOG;
	    return EIO;
	    }
#endif
	}
    while ( (rd & kFIFOFLUSHEMPTY) != kFIFOFLUSHEMPTY && cnt++<1000 );

    if ( (rd & kFIFOFLUSHEMPTY)  != kFIFOFLUSHEMPTY )
	{
	LOG( AliHLTLog::kWarning, "AliHLTRORC6Handler::Initialize", "Cannot empty FIFOs" )
	    << "Cannot empty FIFOs by flushing after " << AliHLTLog::kDec << cnt
	    << " attempts. Old data might still be in the pipes somewhere." << ENDLOG;
	}

    // reset DDL module
#ifdef VERIFIED_WRITE
    ret = Write32Control( (AliUInt32_t)kDDLCONTROL, (AliUInt32_t)0x80000000, true, (AliUInt32_t)0x00000000, (AliUInt32_t)0xFFFFFFFF, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#else
    wr = 0x80000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDDLCONTROL, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif
    
    
    // reset event counter logic
#ifdef VERIFIED_WRITE
    ret = Write32Control( (AliUInt32_t)kEVENTCOUNTERRESET, (AliUInt32_t)0x00000001, true, (AliUInt32_t)0x00000000, (AliUInt32_t)0xFFFFFFFF, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#else
    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kEVENTCOUNTERRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    /*
     *   invalidate DMA and report buffer
     */
    if ( first )
	{
#ifdef USE_SPECIAL_REPORT_OVERWRITES
	AliVolatileUInt64_t* reportWord = (AliVolatileUInt64_t*)fReportBuffer;
	while ( reportWord < (AliVolatileUInt64_t*)fReportBufferEnd )
	    *reportWord++ = 0xFFFFFFF0FFFFFFF0ULL;
#else
	memset( (void*)fReportBuffer, 0xff, fReportBufferSize );
#endif
	memset( (void*)fEventBuffer, 0, fEventBufferSize );
	memset( (void*)fAdminBuffer, 0xff, fAdminBufferSize );
	}
    
    /*
     *   fill DMA descriptor
     */
    //  dma_ctrl[1] = 0x2800;
    //  dma_ctrl[2] = 320;
    //     unsigned long dma_ctrl[4];
    //     dma_ctrl[1] = dmabuf_size;
    //     dma_ctrl[2] = rptbuf_size;
	
    //printf("fill DMA descriptor...\n");
#ifdef VERIFIED_WRITE
    wr = fEventBufferBusAddress & 0x00000000FFFFFFFFULL;
    ret = Write32Control( (AliUInt32_t)kEVTBUFPTRLOW, wr, true, wr, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = (fEventBufferBusAddress >> 32) & 0x00000000FFFFFFFFULL;
    ret = Write32Control( (AliUInt32_t)kEVTBUFPTRHIGH, wr, true, wr, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = fEventBufferEndBusAddress & 0x00000000FFFFFFFFULL;
    ret = Write32Control( (AliUInt32_t)kEVTBUFENDLOW, wr, true, wr, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = (fEventBufferEndBusAddress >> 32) & 0x00000000FFFFFFFFULL;
    ret = Write32Control( (AliUInt32_t)kEVTBUFENDHIGH, wr, true, wr, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = fReportBufferBusAddress & 0x00000000FFFFFFFFULL;
    ret = Write32Control( (AliUInt32_t)kREPBUFPTRLOW, wr, true, wr, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = (fReportBufferBusAddress >> 32) & 0x00000000FFFFFFFFULL;
    ret = Write32Control( (AliUInt32_t)kREPBUFPTRHIGH, wr, true, wr, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = fReportBufferEndBusAddress & 0x00000000FFFFFFFFULL;
    ret = Write32Control( (AliUInt32_t)kREPBUFENDLOW, wr, true, wr, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = (fReportBufferEndBusAddress >> 32) & 0x00000000FFFFFFFFULL;
    ret = Write32Control( (AliUInt32_t)kREPBUFENDHIGH, wr, true, wr, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}


#else
#ifndef SIMULATE
    wr = fEventBufferBusAddress & 0x00000000FFFFFFFFULL;
    status = PSI_write( fBarRegion, (AliUInt32_t)kEVTBUFPTRLOW, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = (fEventBufferBusAddress >> 32) & 0x00000000FFFFFFFFULL;
    status = PSI_write( fBarRegion, (AliUInt32_t)kEVTBUFPTRHIGH, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = fEventBufferEndBusAddress & 0x00000000FFFFFFFFULL;
    status = PSI_write( fBarRegion, (AliUInt32_t)kEVTBUFENDLOW, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = (fEventBufferEndBusAddress >> 32) & 0x00000000FFFFFFFFULL;
    status = PSI_write( fBarRegion, (AliUInt32_t)kEVTBUFENDHIGH, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}

    wr = fReportBufferBusAddress & 0x00000000FFFFFFFFULL;
    status = PSI_write( fBarRegion, (AliUInt32_t)kREPBUFPTRLOW, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = (fReportBufferBusAddress >> 32) & 0x00000000FFFFFFFFULL;
    status = PSI_write( fBarRegion, (AliUInt32_t)kREPBUFPTRHIGH, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = fReportBufferEndBusAddress & 0x00000000FFFFFFFFULL;
    status = PSI_write( fBarRegion, (AliUInt32_t)kREPBUFENDLOW, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = (fReportBufferEndBusAddress >> 32) & 0x00000000FFFFFFFFULL;
    status = PSI_write( fBarRegion, (AliUInt32_t)kREPBUFENDHIGH, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

	
    /*
     *  read pointer
     */
    AliVolatileUInt64_t readAddress = fEventBufferReadPtrBusAddress-fRORCBlockSize;
    LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::Initialize", "Readpointer" )
	<< "readAddress: 0x" << AliHLTLog::kHex << readAddress << " - fEventBufferReadPtrBusAddress: 0x"
	<< fEventBufferReadPtrBusAddress << " - fRORCBlockSize: 0x" << fRORCBlockSize
	<< " - fEventBufferBusAddress: 0x" << fEventBufferBusAddress << " - fEventBufferEndBusAddress: 0x"
	<< fEventBufferEndBusAddress << "." << ENDLOG;
    if ( readAddress > fEventBufferEndBusAddress )
	readAddress = fEventBufferBusAddress+readAddress-fEventBufferEndBusAddress;
    if ( readAddress <= fEventBufferBusAddress )
	readAddress = fEventBufferEndBusAddress - (fEventBufferBusAddress-readAddress);
    LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::Initialize", "Readpointer" )
	<< "readAddress: 0x" << AliHLTLog::kHex << readAddress << "." << ENDLOG;

#ifdef VERIFIED_WRITE
    wr = readAddress & 0x00000000FFFFFFFFULL;
    ret = Write32Control( (AliUInt32_t)kEVTBUFPTRREADLOW, wr, true, wr, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = (readAddress >> 32) & 0x00000000FFFFFFFFULL;
    ret = Write32Control( (AliUInt32_t)kEVTBUFPTRREADHIGH, wr, true, wr, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = fAdminBufferBusAddress & 0x00000000FFFFFFFFULL;
    ret = Write32Control( (AliUInt32_t)kADMBUFPTRREADLOW, wr, true, wr, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = (fAdminBufferBusAddress >> 32) & 0x00000000FFFFFFFFULL;
    ret = Write32Control( (AliUInt32_t)kADMBUFPTRREADHIGH, wr, true, wr, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}

#else
#ifndef SIMULATE
    wr = readAddress & 0x00000000FFFFFFFFULL;
    status = PSI_write( fBarRegion, (AliUInt32_t)kEVTBUFPTRREADLOW, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = (readAddress >> 32) & 0x00000000FFFFFFFFULL;
    status = PSI_write( fBarRegion, (AliUInt32_t)kEVTBUFPTRREADHIGH, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}

    wr = fAdminBufferBusAddress & 0x00000000FFFFFFFFULL;
    status = PSI_write( fBarRegion, (AliUInt32_t)kADMBUFPTRREADLOW, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = (fAdminBufferBusAddress >> 32) & 0x00000000FFFFFFFFULL;
    status = PSI_write( fBarRegion, (AliUInt32_t)kADMBUFPTRREADHIGH, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}


#endif
#endif
#if 0
    *(AliVolatileUInt64_t*)fAdminBuffer = (AliUInt64_t)fEventBufferReadPtrBusAddress;
#else
    *(AliVolatileUInt64_t*)fAdminBuffer = readAddress;
#endif

    // enable or disable flow control, depending on fNoFlowControl flag
    if ( fNoFlowControl )
	wr = 0x00000000;
    else
	wr = 0x00000001;

#ifdef VERIFIED_WRITE
    ret = Write32Control( (AliUInt32_t)kENABLEFLOWCONTROL, wr, true, wr, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#else
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kENABLEFLOWCONTROL, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    if ( fEnableHWCoProc )
	{
	ret = ActivateHWCoProc();
	if ( ret )
	    return ret;
	}


    // enable DIU interface
    wr = 0x00000003;
    if ( fEventSuppressionActive )
	wr |= fEventSuppression_Exp2 << 4;
    LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::Initialize", "DIU Enable" )
	<< "Writing 0x" << AliHLTLog::kHex << wr << " to DIU enable address 0x" 
	<< (AliUInt32_t)kDDLCONTROL << "." << ENDLOG;
#ifdef VERIFIED_WRITE
    ret = Write32Control( (AliUInt32_t)kDDLCONTROL, wr, true, wr, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#else
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDDLCONTROL, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    // DMA enable in is done in Activate RORC

    

    return 0;
    }

int AliHLTRORC6Handler::DeinitializeRORC()
    {
    PSI_Status    status;
    int ret;
    unsigned long wr;

#ifdef VERIFIED_WRITE
    // disable DMA
    ret = Write32Control( (AliUInt32_t)kDMAENABLE, 0x00000000, true, 0x00000000, false, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Deinitialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#else
    // disable DMA, do it three times, in the hopes that at least one will get through
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    // Flush FIFOs to suck remaining data from DDL.
    unsigned long cnt=0;
    unsigned long rd=0;
    do
	{
	// flush FIFO
	wr = 0x00000001;
#ifndef SIMULATE
	status = PSI_write( fBarRegion, (AliUInt32_t)kFIFOFLUSH, _dword_, 4, (void*)&wr );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC6Handler::DeinitializeRORC", "Write Error" )
		<< "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		<< ")." << ENDLOG;
	    return EIO;
	    }
#endif

	// read FIFO status to verify flush (content can be reinserted after flush by internal FIFO on DIU...)
	rd=0;
#ifndef SIMULATE
	status = PSI_read( fBarRegion, (AliUInt32_t)kFIFOFLUSH, _dword_, 4, (void*)&rd );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC6Handler::DeinitializeRORC", "Read Error" )
		<< "PCI read error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		<< ")." << ENDLOG;
	    return EIO;
	    }
#endif
	}
    while ( (rd & kFIFOFLUSHEMPTY) != kFIFOFLUSHEMPTY && cnt++<1000 );

    // disable DIU interface
#ifdef VERIFIED_WRITE
    ret = Write32Control( (AliUInt32_t)kDDLCONTROL, 0x00000000, true, 0x00000000, false, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Deinitialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#else
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDDLCONTROL, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    // and again flush FIFOs the ensure they are empty.
    cnt=0;
    rd=0;
    do
	{
	// flush FIFO
	wr = 0x00000001;
#ifndef SIMULATE
	status = PSI_write( fBarRegion, (AliUInt32_t)kFIFOFLUSH, _dword_, 4, (void*)&wr );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC6Handler::DeinitializeRORC", "Write Error" )
		<< "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		<< ")." << ENDLOG;
	    return EIO;
	    }
#endif

	// read FIFO status to verify flush (content can be reinserted after flush by internal FIFO on DIU...)
	rd=0;
#ifndef SIMULATE
	status = PSI_read( fBarRegion, (AliUInt32_t)kFIFOFLUSH, _dword_, 4, (void*)&rd );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC6Handler::DeinitializeRORC", "Read Error" )
		<< "PCI read error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		<< ")." << ENDLOG;
	    return EIO;
	    }
#endif
	}
    while ( (rd & kFIFOFLUSHEMPTY) != kFIFOFLUSHEMPTY && cnt++<1000 );



    // reset event counter logic
#ifdef VERIFIED_WRITE
    ret = Write32Control( (AliUInt32_t)kEVENTCOUNTERRESET, 0x00000001, true, (AliUInt32_t)0x00000000, (AliUInt32_t)0xFFFFFFFF, false, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Deinitialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#else
    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kEVENTCOUNTERRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    // disable flow control
#ifdef VERIFIED_WRITE
    ret = Write32Control( (AliUInt32_t)kENABLEFLOWCONTROL, 0x00000000, true, 0x00000000, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Deinitialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#else
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kENABLEFLOWCONTROL, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    if ( fEnableHWCoProc )
	{
	ret = DeactivateHWCoProc();
	if ( ret )
	    return ret;
	}


    return 0;
    }

int AliHLTRORC6Handler::InitializeFromRORC( bool first )
    {
    if ( !first )
	{
	while ( *(AliVolatileUInt64_t*)fReportBufferReadPtr == 0xFFFFFFFFFFFFFFFFULL && fReportBufferReadPtr<fReportBufferEnd )
	    fReportBufferReadPtr += 24;
	if ( fReportBufferReadPtr>=fReportBufferEnd )
	    fReportBufferReadPtr = fReportBuffer;
#ifdef SIMULATE
	gReportBufferReadPtr = (AliUInt64_t*)fReportBufferReadPtr;
#endif
	fReportBufferOldReadPtr = NULL;

	// set physical address (signalling end of read data)
	fEventBufferReadPtrBusAddress = *(AliVolatileUInt64_t*)fAdminBuffer;
	fEventBufferReleasedOffset = fEventBufferReadPtrBusAddress-fEventBufferBusAddress;
	fEventBufferReadPtr = fEventBuffer+fEventBufferReleasedOffset;
	fEventBufferReadPtrPhysAddress = fEventBufferPhysAddress+fEventBufferReleasedOffset;
	if ( fEventBufferReadPtrBusAddress<fEventBufferBusAddress || fEventBufferReadPtrBusAddress>fEventBufferEndBusAddress )
	    {
	    fEventBufferReleasedOffset = 0;
	    fEventBufferReadPtrBusAddress = fEventBufferBusAddress;
	    fEventBufferReadPtr = fEventBuffer;
	    fEventBufferReadPtrPhysAddress = fEventBufferPhysAddress;
	    }
	*(AliVolatileUInt64_t*)fAdminBuffer = (AliUInt64_t)fEventBufferReadPtrBusAddress;
	}

    return 0;
    }

int AliHLTRORC6Handler::SetOptions( const vector<MLUCString>&, char*&, unsigned& )
    {
    return 0;
    }

void AliHLTRORC6Handler::GetAllowedOptions( vector<MLUCString>& options )
    {
    options.clear();
    }

int AliHLTRORC6Handler::WriteConfigWord( unsigned long wordIndex, AliUInt32_t word, bool doVerify )
    {
    if ( wordIndex>=4096 )
	return EINVAL;
    PSI_Status    status;
    int ret;
    unsigned long wr;

    AliUInt32_t oldConfig=0;
#ifndef SIMULATE
    status = PSI_read( fBarRegion, (AliUInt32_t)kFCFCONTROL, _dword_, 4, (void*)&oldConfig );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::WriteConfigWord", "Read Error" )
	    << "PCI address read error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    AliUInt32_t newConfig = oldConfig & ~kFCFCONTROLENABLE;
#ifdef VERIFIED_WRITE
    ret = Write32Control( (AliUInt32_t)kFCFCONTROL, newConfig, doVerify, newConfig, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::WriteConfigWord", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#else
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kFCFCONTROL, _dword_, 4, (void*)&newConfig );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::WriteConfigWord", "Write Error" )
	    << "PCI address write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    unsigned cnt=0;
    bool verified = true;
    AliUInt32_t rd=word;
    
    do
	{
#ifdef VERIFIED_WRITE
	ret = Write32Control( (AliUInt32_t)kFCFCONFIGMEMADDRESS, wordIndex, doVerify, wordIndex, true, status );
	if ( ret || status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC6Handler::WriteConfigWord", "Write Error" )
		<< "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
		<< ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		<< ")." << ENDLOG;
	    return EIO;
	    }
#else
	wr = wordIndex;
#ifndef SIMULATE
	status = PSI_write( fBarRegion, (AliUInt32_t)kFCFCONFIGMEMADDRESS, _dword_, 4, (void*)&wr );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC6Handler::WriteConfigWord", "Write Error" )
		<< "PCI address write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		<< ")." << ENDLOG;
	    return EIO;
	    }
#endif
#endif
	
	wr = word;
#ifndef SIMULATE
	status = PSI_write( fBarRegion, (AliUInt32_t)kFCFCONFIGMEMDATAWR, _dword_, 4, (void*)&wr );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC6Handler::WriteConfigWord", "Write Error" )
		<< "PCI address write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		<< ")." << ENDLOG;
	    return EIO;
	    }
#endif
	
	if ( doVerify )
	    {
#ifdef VERIFIED_WRITE
	    ret = Write32Control( (AliUInt32_t)kFCFCONFIGMEMADDRESS, wordIndex, doVerify, wordIndex, true, status );
	    if ( ret || status != PSI_OK )
		{
		LOG( AliHLTLog::kError, "AliHLTRORC6Handler::WriteConfigWord", "Write Error" )
		    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
		    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		    << ")." << ENDLOG;
		return EIO;
		}
#else
	    wr = wordIndex;
#ifndef SIMULATE
	    status = PSI_write( fBarRegion, (AliUInt32_t)kFCFCONFIGMEMADDRESS, _dword_, 4, (void*)&wr );
	    if ( status != PSI_OK )
		{
		LOG( AliHLTLog::kError, "AliHLTRORC6Handler::WriteConfigWord", "Write Error" )
		    << "PCI address write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		    << ")." << ENDLOG;
		return EIO;
		}
#endif
#endif

#ifndef SIMULATE
	    status = PSI_read( fBarRegion, (AliUInt32_t)kFCFCONFIGMEMDATARD, _dword_, 4, (void*)&rd );
	    if ( status != PSI_OK )
		{
		LOG( AliHLTLog::kError, "AliHLTRORC6Handler::WriteConfigWord", "Write Error" )
		    << "PCI address write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		    << ")." << ENDLOG;
		return EIO;
		}
#endif
	    if ( rd == word )
		verified = true;
	    else
		verified = false;
	    }
	++cnt;
	}
    while ( !verified && cnt<1000 );

    if ( !verified )
	{
	LOG( AliHLTLog::kWarning, "AliHLTRORC6Handler::WriteConfigWord", "Write/Verify Error" )
	    << "Could not verify written config word " << AliHLTLog::kHex << word << " at index " << wordIndex
	    << " after " << AliHLTLog::kDec << cnt << " tries - Value to write: 0x"
	    << AliHLTLog::kHex << word << " - Value read: 0x" << rd << "." << ENDLOG;
#if 0
	return ENXIO;
#endif
	}


#ifdef VERIFIED_WRITE
    ret = Write32Control( (AliUInt32_t)kFCFCONTROL, oldConfig, doVerify, oldConfig, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::WriteConfigWord", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#else
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kFCFCONTROL, _dword_, 4, (void*)&oldConfig );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::WriteConfigWord", "Write Error" )
	    << "PCI address write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif


    return 0;
    
    }



int AliHLTRORC6Handler::ActivateRORC()
    {
    PSI_Status    status;
    int ret;
#ifndef VERIFIED_WRITE
    unsigned long wr;
#endif

    if ( fEnableHWCoProc )
	{
	ret = ActivateHWCoProc();
	if ( ret )
	    return ret;
	}

    // enable DMA
#ifdef VERIFIED_WRITE
    ret = Write32Control( (AliUInt32_t)kDMAENABLE, 0x00000001, true, 0x00000001, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::ActivateRORC", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#else
    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::ActivateRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif


    return 0;
    }

int AliHLTRORC6Handler::DeactivateRORC()
    {
    PSI_Status    status;
    int ret;
#ifndef VERIFIED_WRITE
    unsigned long wr;
#endif

#ifdef VERIFIED_WRITE
    // disable DMA
    ret = Write32Control( (AliUInt32_t)kDMAENABLE, 0x00000000, true, 0x00000000, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::DeactivateRORC", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#else
    // disable DMA, do it three times, in the hopes that at least one will get through
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::DeactivateRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::DeactivateRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::DeactivateRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    if ( fEnableHWCoProc )
	{
	DeactivateHWCoProc();
	}


    AliUInt32_t fifoFullCnt = 0;
    if ( !ReadRORCBusyCounter( fifoFullCnt ) && fifoFullCnt>2 )
	{
	LOG( AliHLTLog::kImportant, "AliHLTRORC6Handler::DeactivateRORC", "Fifo Full Count" )
	    << "FIFO was full " << AliHLTLog::kDec << fifoFullCnt << " (0x" << AliHLTLog::kHex << fifoFullCnt
	    << ") times between DMA enable and disable." << ENDLOG;
	}



    return 0;
    }

int AliHLTRORC6Handler::PollForEvent( AliEventID_t& eventID, 
				      AliUInt64_t& dataOffset, AliUInt64_t& dataSize, AliUInt64_t& dataWrappedSize,
				      bool& dataDefinitivelyCorrupt, bool& eventSuppressed, AliUInt64_t& unsuppressedDataSize )
    {
    dataDefinitivelyCorrupt = false;
    eventSuppressed = false;
    unsigned long blocks;
    if ( fReportBufferOldReadPtr != fReportBufferReadPtr )
	{
	LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::PollForEvent", "Polling for event" )
	    << "Polling for event at location 0x" << AliHLTLog::kHex << (unsigned long)fReportBufferReadPtr
	    << " (bus address: 0x" << AliHLTLog::kHex 
	    << (unsigned long)(fReportBufferBusAddress)+(unsigned long)(fReportBufferReadPtr-fReportBuffer)
	    << ") (offset 0x" << (unsigned long)(fReportBufferReadPtr-fReportBuffer) << " ("
	    << AliHLTLog::kDec << (unsigned long)(fReportBufferReadPtr-fReportBuffer) 
	    << ")) in report buffer." << ENDLOG;
	fReportBufferOldReadPtr = fReportBufferReadPtr;
	}
#ifdef USE_SPECIAL_REPORT_OVERWRITES
    if ( *(AliVolatileUInt64_t*)fReportBufferReadPtr!=0xFFFFFFF0FFFFFFF0ULL )
#else
    if ( *(AliVolatileUInt64_t*)fReportBufferReadPtr!=0xFFFFFFFFFFFFFFFFULL )
#endif
	{



	struct timeval now, then;
	gettimeofday( &now, NULL );
#ifdef USE_SPECIAL_REPORT_OVERWRITES
	while ( *(((AliVolatileUInt64_t*)fReportBufferReadPtr)+1)==0xFFFFFFF0FFFFFFF0ULL ||
		*(((AliVolatileUInt64_t*)fReportBufferReadPtr)+2)==0xFFFFFFF0FFFFFFF0ULL )
#else
	while ( *(((AliVolatileUInt64_t*)fReportBufferReadPtr)+1)==0xFFFFFFFFFFFFFFFFULL || 
		*(((AliVolatileUInt64_t*)fReportBufferReadPtr)+2)==0xFFFFFFFfFFFFFFFFULL )
#endif
	    {
	    gettimeofday( &then, NULL );
	    if ( then.tv_usec >= now.tv_usec+1000000 ||
		 ( then.tv_sec>now.tv_sec && 
		   then.tv_usec+(then.tv_sec-now.tv_sec)*1000000 >= now.tv_usec+1000000 ) )
		{
		LOG( AliHLTLog::kError, "AliHLTRORC6Handler::PollForEvent", "Timeout waiting for report words" )
		    << "(*(AliVolatileUInt64_t*)fReportBufferReadPtr): 0x" << AliHLTLog::kHex
		    << *((AliVolatileUInt64_t*)fReportBufferReadPtr) << ENDLOG;
#ifdef USE_SPECIAL_REPORT_OVERWRITES
		*(AliVolatileUInt64_t*)fReportBufferReadPtr = 0xFFFFFFF0FFFFFFF0ULL;
		*(((AliVolatileUInt64_t*)fReportBufferReadPtr)+1) = 0xFFFFFFF0FFFFFFF0ULL;
		*(((AliVolatileUInt64_t*)fReportBufferReadPtr)+2) = 0xFFFFFFF0FFFFFFF0ULL;
#else
		*(AliVolatileUInt64_t*)fReportBufferReadPtr = 0xFFFFFFFFFFFFFFFFULL;
		*(((AliVolatileUInt64_t*)fReportBufferReadPtr)+1) = 0xFFFFFFFFFFFFFFFFULL;
		*(((AliVolatileUInt64_t*)fReportBufferReadPtr)+2) = 0xFFFFFFFFFFFFFFFFULL;
#endif
		fReportBufferReadPtr += 24;
		if ( fReportBufferReadPtr >= fReportBufferEnd )
		    {
		    fReportBufferReadPtr = fReportBuffer;
		    }
#ifdef SIMULATE
#if 0
		gReportBufferReadPtr = (AliUInt64_t*)fReportBufferReadPtr;
#endif
#endif
		return ENOMSG;
		}
	    }



	EventData ed;
	LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::PollForEvent", "Found event" )
	    << "(*(AliVolatileUInt64_t*)fReportBufferReadPtr): 0x" << AliHLTLog::kHex
	    << *((AliVolatileUInt64_t*)fReportBufferReadPtr) << ENDLOG;
	AliUInt64_t readAddr = (*(AliVolatileUInt64_t*)fReportBufferReadPtr);
	//AliVolatileUInt8_t* readAddrPtr = (AliVolatileUInt8_t*)(unsigned long)readAddr;
	AliVolatileUInt8_t* readAddrPtr = (AliVolatileUInt8_t*)( fEventBuffer + ( ((unsigned long)readAddr) - fEventBufferBusAddress ) );
	AliUInt64_t endAddr = (*(((AliVolatileUInt64_t*)fReportBufferReadPtr)+1));
	//AliVolatileUInt8_t* endAddrPtr = (AliVolatileUInt8_t*)(unsigned long)endAddr;
	AliUInt32_t diuWords = (*(((AliVolatileUInt32_t*)fReportBufferReadPtr)+4)); 
	AliUInt32_t dmaWords = (*(((AliVolatileUInt32_t*)fReportBufferReadPtr)+5)); 

	eventSuppressed = diuWords & 0x80000000UL;


	LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::PollForEvent", "Event report words" )
	    << "Event " << AliHLTLog::kDec << fEventCounter
	    << " (0x" << AliHLTLog::kHex << fEventCounter << ") report words: 0x"
	    << AliHLTLog::kHex << readAddr << " (" << AliHLTLog::kDec << readAddr << ") - 0x"
	    << AliHLTLog::kHex << endAddr << " (" << AliHLTLog::kDec << endAddr << ") - 0x"
	    << AliHLTLog::kHex << dmaWords << " (" << AliHLTLog::kDec << dmaWords << ") - 0x"
	    << AliHLTLog::kHex << diuWords << " (" << AliHLTLog::kDec << diuWords 
	    << " - event suppressed: " << (eventSuppressed ? "yes" : "no" )
	    << "." << ENDLOG;

	if ( readAddr<fEventBufferBusAddress || readAddr>fEventBufferEndBusAddress )
	    {
	    LOG( AliHLTLog::kFatal, "AliHLTRORC6Handler::PollForEvent", "Event Inconsistency" )
	      << "Error: Found illegal DMA read start address supplied by RORC. RORC read start Address: 0x" << AliHLTLog::kHex << readAddr
	      << " - event buffer start bus address 0x" << fEventBufferBusAddress
	      << " - event buffer end bus address 0x" << fEventBufferEndBusAddress
	      << "." << ENDLOG;
	    return EFAULT;
	    }
	if ( endAddr<fEventBufferBusAddress || endAddr>fEventBufferEndBusAddress )
	    {
	    LOG( AliHLTLog::kFatal, "AliHLTRORC6Handler::PollForEvent", "Event Inconsistency" )
	      << "Error: Found illegal DMA read end address supplied by RORC. RORC read end Address: 0x" << AliHLTLog::kHex << endAddr
	      << " - event buffer start bus address 0x" << fEventBufferBusAddress
	      << " - event buffer end bus address 0x" << fEventBufferEndBusAddress
	      << "." << ENDLOG;
	    return EFAULT;
	    }


	diuWords &= 0x7FFFFFFFUL; // Mask out top bit used as flag
	dmaWords &= 0x7FFFFFFFUL; // Mask out top bit used as flag

	unsuppressedDataSize = diuWords;

	AliUInt32_t words = dmaWords;

	//AliUInt32_t dmaSize = endAddr - readAddr;
	if ( (unsigned long)readAddr != fEventBufferReadPtrBusAddress )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC6Handler::PollForEvent", "Event Inconsistency" )
		<< "Error: Found inconsistency in event " << AliHLTLog::kDec << fEventCounter
		<< " (0x" << AliHLTLog::kHex << fEventCounter << ") for event buffer read ptr: fEventBufferReadPtrBusAddress 0x"
		<< AliHLTLog::kHex << (unsigned long)fEventBufferReadPtrBusAddress << " (" << AliHLTLog::kDec
		<< (unsigned long)fEventBufferReadPtrBusAddress << " - Address reported in event: 0x"
		<< AliHLTLog::kHex << (unsigned long)readAddr << " (" << AliHLTLog::kDec << (unsigned long)readAddr
		<< ") bytes - Continuing with value reported by card (Report words: 0x"
		<< AliHLTLog::kHex << readAddr << " (" << AliHLTLog::kDec << readAddr << ") - 0x"
		<< AliHLTLog::kHex << endAddr << " (" << AliHLTLog::kDec << endAddr << ") - 0x"
		<< AliHLTLog::kHex << dmaWords << " (" << AliHLTLog::kDec << dmaWords << ") - 0x"
		<< AliHLTLog::kHex << diuWords << " (" << AliHLTLog::kDec << diuWords << ")." << ENDLOG;
	    unsigned long oldOffset = ((unsigned long)fEventBufferReadPtrBusAddress)-((unsigned long)fEventBufferBusAddress);
	    unsigned long offset = ((unsigned long)readAddr)-((unsigned long)fEventBufferBusAddress);
	    fEventBufferReadPtr = fEventBuffer+offset;
	    fEventBufferReadPtrPhysAddress = fEventBufferPhysAddress+offset;
	    fEventBufferReadPtrBusAddress = fEventBufferBusAddress+offset;
	    //fEventBufferReadPtr = readAddrPtr;
	    // Add a block into the free space accounting for the mismatched block.
	    // Otherwise the block will be lost (not claimed by any event) and will not be freed.
	    // then at some point the read pointer for the RORC in the admin buffer cannot be updated
	    // any more and the system will stall.
	    EventData edTmp;
	    edTmp.fEventID = ~(AliUInt64_t)0;
	    edTmp.fOffset = oldOffset;
	    if ( oldOffset <= offset ) // Note: == should actually never happen...
		{
		edTmp.fSize = offset-oldOffset;
		edTmp.fWrappedSize = 0;
		}
	    else
		{
		edTmp.fSize = offset+fEventBufferSize-oldOffset;
		edTmp.fWrappedSize = offset;
		}
	    pthread_mutex_lock( &fEventMutex );
	    ReleaseBlock( &edTmp );
	    pthread_mutex_unlock( &fEventMutex );
	    }
	dataSize = words*4;
	// Firmware transfers two 32 bit words with lengths of transferred data in a 64 bit transfer, after the actual payload data
	// For transfers with odd number of payload data words one fillword is inserted between the last payload data word and the two length words
	// For transfers with even number of payload data words the two length words are directly appended after the last payload word
	if ( words & 1 )
	    words++;
	words += 2;
	blocks = words / fRORCBlock32bitWords; // One block is always the specified amount of words
	// Test for a partially filled block.
	if ( words % fRORCBlock32bitWords )
	    blocks++;
	AliUInt64_t calcEndAddr = readAddr+blocks*fRORCBlockSize;
#if 0
	if ( eventSuppressed )
	    {
	    eventID = AliEventID_t();
	    dataOffset = 0;
	    dataSize = dmaWords*4;
	    dataWrappedSize = 0;
	    dataDefinitivelyCorrupt = true;
	    return 0;
	    }
#endif
	if ( calcEndAddr > fEventBufferEndBusAddress )
	    {
	    calcEndAddr = fEventBufferBusAddress + ( calcEndAddr - fEventBufferEndBusAddress );
	    }
	if ( eventSuppressed )
	    {
	    AliHLTDDLHeader ddlHeader;
	    if ( !ddlHeader.Set( (AliUInt32_t*)(AliVolatileUInt32_t*)fEventBufferReadPtr ) )
		{
		LOG( AliHLTLog::kError, "AliHLTRORC6Handler::PollForEvent", "Error reading DDL header" )
		    << "Error reading DDL header number. Unknown header version or out of memory. Header version: " 
		    << AliHLTLog::kDec 
		    << (unsigned)AliHLTGetDDLHeaderVersion(fEventBufferReadPtr) << " - header version byte index: "
		    << (unsigned)DDLHEADERVERSIONINDEX << " - header size: " << AliHLTGetDDLHeaderSize(fEventBufferReadPtr)
		    << ENDLOG;
		eventSuppressed = false;
		dataDefinitivelyCorrupt = true;
		}
	    else
		{
		dataSize = ddlHeader.GetHeaderSize();
		words = dataSize / 4;

		}
	    unsigned long dmaSize = endAddr-readAddr;
	    if ( endAddr<readAddr )
		dmaSize = fEventBufferSize+endAddr-readAddr; // fEventBufferEndBusAddress-readAddr + endAddr-fEventBufferBusAddress;
	    blocks = dmaSize / fRORCBlockSize;
	    }
	else
	    {
	    if ( endAddr != calcEndAddr )
		{
		LOG( AliHLTLog::kError, "AliHLTRORC6Handler::PollForEvent", "Event Inconsistency" )
		    << "Error: Found inconsistency in event " << AliHLTLog::kDec << fEventCounter
		    << " (0x" << AliHLTLog::kHex << fEventCounter << ") for event size: DMA Word count: " << AliHLTLog::kDec
		    << dmaWords << " (0x" << AliHLTLog::kHex << dmaWords << ") - end address derived from that: "
		    << AliHLTLog::kDec << calcEndAddr << "(0x" << AliHLTLog::kHex << calcEndAddr
		    << ") - end address reported by RORC: " << AliHLTLog::kDec << endAddr << "(0x"
		    << AliHLTLog::kHex << endAddr << ") (DIU Word count: " << AliHLTLog::kDec
		    << diuWords << " (0x" << AliHLTLog::kHex << diuWords 
		    << ")). Continuing with RORC reported DMA size (Report words: 0x"
		    << AliHLTLog::kHex << readAddr << " (" << AliHLTLog::kDec << readAddr << ") - 0x"
		    << AliHLTLog::kHex << endAddr << " (" << AliHLTLog::kDec << endAddr << ") - 0x"
		    << AliHLTLog::kHex << dmaWords << " (" << AliHLTLog::kDec << dmaWords << ") - 0x"
		    << AliHLTLog::kHex << diuWords << " (" << AliHLTLog::kDec << diuWords << ")."
		    << ENDLOG;
		unsigned long dmaSize = endAddr-readAddr;
		if ( endAddr<readAddr )
		    dmaSize = fEventBufferSize+endAddr-readAddr; // fEventBufferEndBusAddress-readAddr + endAddr-fEventBufferBusAddress;
		blocks = dmaSize / fRORCBlockSize;
		dataSize = dmaSize; // Fo debugging write out all the data that was dma'd into memory.
		dataDefinitivelyCorrupt = true;
		}
	    if ( dmaWords != diuWords )
		{
		LOG( AliHLTLog::kError, "AliHLTRORC6Handler::PollForEvent", "Event Inconsistency" )
		    << "Error: Found inconsistency in event " << AliHLTLog::kDec << fEventCounter
		    << " (0x" << AliHLTLog::kHex << fEventCounter << ") for event size: DIU Word count: " << AliHLTLog::kDec
		    << diuWords << " (0x" << AliHLTLog::kHex << diuWords << ") - DMA Word count: " << AliHLTLog::kDec
		    << dmaWords << " (0x" << AliHLTLog::kHex << dmaWords << "). Continuing with RORC reported DMA size (Report words: 0x"
		    << AliHLTLog::kHex << readAddr << " (" << AliHLTLog::kDec << readAddr << ") - 0x"
		    << AliHLTLog::kHex << endAddr << " (" << AliHLTLog::kDec << endAddr << ") - 0x"
		    << AliHLTLog::kHex << dmaWords << " (" << AliHLTLog::kDec << dmaWords << ") - 0x"
		    << AliHLTLog::kHex << diuWords << " (" << AliHLTLog::kDec << diuWords << ")."
		    << ENDLOG;
		unsigned long dmaSize = endAddr-readAddr;
		if ( endAddr<readAddr )
		    dmaSize = fEventBufferSize+endAddr-readAddr; // fEventBufferEndBusAddress-readAddr + endAddr-fEventBufferBusAddress;
		blocks = dmaSize / fRORCBlockSize;
		dataSize = dmaSize; // Fo debugging write out all the data that was dma'd into memory.
		dataDefinitivelyCorrupt = true;
		}
	    }

	if ( dataDefinitivelyCorrupt && fTPCDataFix )
	    {
	    AliVolatileUInt32_t* epDMA = ((AliVolatileUInt32_t*)readAddrPtr)+dmaWords;
	    while ( (AliVolatileUInt8_t*)epDMA >= fEventBufferEnd )
		{
		epDMA = (AliVolatileUInt32_t*)( fEventBuffer + (unsigned long)epDMA-(unsigned long)fEventBufferEnd );
		}
	    AliVolatileUInt32_t* epDIU = ((AliVolatileUInt32_t*)readAddrPtr)+diuWords;
	    while ( (AliVolatileUInt8_t*)epDIU >= fEventBufferEnd )
		{
		epDIU = (AliVolatileUInt32_t*)( fEventBuffer + (unsigned long)epDIU-(unsigned long)fEventBufferEnd );
		}
	    AliVolatileUInt32_t *ep1, *ep2;
	    bool found1=false, found2=false;
	    unsigned long dataWords;
	    
	    if ( epDMA<epDIU )
		{
		ep1 = epDMA;
		ep2 = epDIU;
		}
	    else
		{
		ep1 = epDIU;
		ep2 = epDMA;
		}
	    if ( (AliVolatileUInt8_t*)ep1>fEventBuffer && (AliVolatileUInt8_t*)ep1<fEventBufferEnd && *ep1==diuWords && ((*(ep1-1)) & 0xFFF00000)==0x00000000 )
		{
		found1=true;
		dataWords = (ep1-(AliUInt32_t*)readAddrPtr);
		dataSize = dataWords*sizeof(AliUInt32_t);
		}
	    if ( (AliVolatileUInt8_t*)ep1==fEventBuffer && *ep1==diuWords && ((*(((AliVolatileUInt32_t*)fEventBufferEnd)-1)) & 0xFFF00000)==0x00000000 ) // Wrap around just before last word
                {
		found1=true;
		dataWords = (((AliVolatileUInt32_t*)fEventBufferEnd)+1-(AliVolatileUInt32_t*)readAddrPtr);
		dataSize = dataWords*sizeof(AliUInt32_t);
		}
	    if ( !found1 && (AliVolatileUInt8_t*)ep2>fEventBuffer && (AliVolatileUInt8_t*)ep2<fEventBufferEnd && *ep2==diuWords && ((*(ep2-1)) & 0xFFF00000)==0x00000000 )
		{
		found2=true;
		dataWords = (ep2-(AliUInt32_t*)readAddrPtr);
		dataSize = dataWords*sizeof(AliUInt32_t);
		}
	    if ( !found1 && (AliVolatileUInt8_t*)ep2==fEventBuffer && *ep2==diuWords && ((*(((AliVolatileUInt32_t*)fEventBufferEnd)-1)) & 0xFFF00000)==0x00000000 ) // Wrap around just before last word
		{
		found2=true;
		dataWords = (((AliVolatileUInt32_t*)fEventBufferEnd)+1-(AliVolatileUInt32_t*)readAddrPtr);
		dataSize = dataWords*sizeof(AliUInt32_t);
		}
	    if ( found1 || found2 )
		{
		LOG( AliHLTLog::kWarning, "AliHLTRORC6Handler::PollForEvent", "Possible TPC Fix" )
		<< "Possible fix for data size inconsistency found. Assuming event size to be "
		<< AliHLTLog::kDec << dataSize << "(0x" << AliHLTLog::kHex
		<< dataSize << ") - Found at " << (found1 ? "first" : "second")
		<< " possible position." << ENDLOG;
		}
	    }
#ifdef COMPARE_COUNTER_PATTERN_TAGGED
        if ( fDoCompareCounterPattern )
            {
            AliUInt32_t counterVal = ((AliUInt32_t)fCompareHighTag) << 28;
            AliUInt32_t eventBufferWordCnt = fEventBufferSize / sizeof(AliUInt32_t);
            for ( AliUInt32_t counter = 0; counter < diuWords; counter++ )
                {
                if ( ((AliUInt32_t*)fEventBufferReadPtr)[counter % eventBufferWordCnt] != counterVal )
                    {
                    LOG( AliHLTLog::kError, "AliHLTRORC6Handler::PollForEvent", "Data Check Error" )
                        << "Data Error: Offset 0x" << AliHLTLog::kHex << counter % eventBufferWordCnt << " (" << AliHLTLog::kDec
                        << counter % eventBufferWordCnt << ") Expected: 0x" << AliHLTLog::kHex << counterVal << " ("
                        << AliHLTLog::kDec << counterVal << ") - Found: 0x" << AliHLTLog::kHex
                        << ((AliUInt32_t*)fEventBufferReadPtr)[counter % eventBufferWordCnt] << " (" << AliHLTLog::kDec
                        << ((AliUInt32_t*)fEventBufferReadPtr)[counter % eventBufferWordCnt] << ")." << ENDLOG;
                    if ( (((AliUInt32_t*)fEventBufferReadPtr)[counter % eventBufferWordCnt] & 0xF0000000) == (((AliUInt32_t)fCompareHighTag) << 28) )
                        {
                        LOG( AliHLTLog::kError, "AliHLTRORC6Handler::PollForEvent", "Data Check Error" )
                            << "Resuming counter as 0x" << AliHLTLog::kHex << ((AliUInt32_t*)fEventBufferReadPtr)[counter % eventBufferWordCnt]
                            << " (" << AliHLTLog::kDec << ((AliUInt32_t*)fEventBufferReadPtr)[counter % eventBufferWordCnt] << ")."
                            << ENDLOG;
                        }
                    counterVal = ((AliUInt32_t*)fEventBufferReadPtr)[counter % eventBufferWordCnt];
                    }
                counterVal++;
                }
            }
#endif

	if ( fReportWordConsistencyCheck || fReportWordUseBlockSize )
	    {
	    unsigned long blocksReported;
	    blocksReported = (*(AliVolatileUInt64_t*)fReportBufferReadPtr) & 0x00001FFF;
	    if ( fReportWordConsistencyCheck && blocksReported != blocks )
		{
		LOG( AliHLTLog::kError, "AliHLTRORC6Handler::PollForEvent", "Inconsistent block size" )
		    << "Found event has inconsistent sizes: Data size from report word: 0x" << AliHLTLog::kHex << dataSize 
		    << " (" << AliHLTLog::kDec << dataSize << ") - Block count calculated from data size: 0x"
		    << AliHLTLog::kHex << blocks << " (" << AliHLTLog::kDec << blocks 
		    << ") - Block count from report word: 0x" << AliHLTLog::kHex << blocksReported
		    << " (" << AliHLTLog::kDec << blocksReported << ")." << ENDLOG;
		}
	    if ( fReportWordUseBlockSize )
	        {
		if ( blocks != blocksReported )
		    {
		    LOG( AliHLTLog::kError, "AliHLTRORC6Handler::PollForEvent", "Inconsistent block count" )
			<< "Found event has inconsistent block counts: Data size from report word: 0x" << AliHLTLog::kHex << dataSize 
			<< " (" << AliHLTLog::kDec << dataSize << ") - Block count calculated from data size: 0x"
			<< AliHLTLog::kHex << blocks << " (" << AliHLTLog::kDec << blocks 
			<< ") - Block count from report word: 0x" << AliHLTLog::kHex << blocksReported
			<< " (" << AliHLTLog::kDec << blocksReported << ")." << ENDLOG;
		    dataSize = blocksReported*fRORCBlockSize;
		    }
		blocks = blocksReported;
	        }
	    }
		
	ed.fSize = blocks*fRORCBlockSize;

	LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::PollForEvent", "Found event" )
	    << "Event found with " << AliHLTLog::kDec << dataSize << " (0x" << AliHLTLog::kHex << dataSize
	    << ") bytes and " << AliHLTLog::kDec << blocks << " blocks." << ENDLOG;

	bool showErrorData = false;

#ifdef RORC_DEBUG_USE_INTERNAL_PATTERNGENERATOR
	if ( dataSize != 1052 || blocks!=9 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC6Handler::PollForEvent", "Wrong event size" )
		<< "Found event has wrong event size " << AliHLTLog::kDec << dataSize << AliHLTLog::kHex
		<< " (0x" << dataSize << ") instead of 1052 or data block count " << AliHLTLog::kDec
		<< blocks << " instead of 9." << ENDLOG;
	    showErrorData = true;
	    }
#endif
	bool nopeEvent = false;
	if ( dataSize==sizeof(gNOPEEventIdentifier) && (*(AliVolatileUInt64_t*)fEventBufferReadPtr)==gNOPEEventIdentifier )
	    {
	    LOG( AliHLTLog::kInformational, "AliHLTRORC6Handler::PollForEvent", "NOPE event found" )
		<< "NOPE event found, event will be ignored." << ENDLOG;
	    nopeEvent = true;
	    }

	AliHLTDDLHeader ddlHeader;
	bool corruptEventID = false;
	if ( fHeaderSearch && !nopeEvent )
	    {
	    if ( dataDefinitivelyCorrupt )
		{
		eventID = fNextInvalidEventID;
		++fNextInvalidEventID;
		}
	    else if ( dataSize < AliHLTMinDDLHeaderSize )
		{
		LOG( AliHLTLog::kWarning, "AliHLTRORC6Handler::PollForEvent", "Data smaller than DDL header" )
		    << "Data received is smaller than minimum DDL header size " << AliHLTLog::kDec 
		    << AliHLTMinDDLHeaderSize << "." << ENDLOG;
		dataDefinitivelyCorrupt = true;
		eventID = fNextInvalidEventID;
		++fNextInvalidEventID;
		corruptEventID = true;
		}
	    else if ( !ddlHeader.Set( (AliUInt32_t*)(AliVolatileUInt32_t*)fEventBufferReadPtr ) )
		{
		LOG( AliHLTLog::kError, "AliHLTRORC6Handler::PollForEvent", "Error reading DDL header" )
		    << "Error reading DDL header number. Unknown header version or out of memory. Header version: " 
		    << AliHLTLog::kDec 
		    << (unsigned)AliHLTGetDDLHeaderVersion(fEventBufferReadPtr) << " - header version byte index: "
		    << (unsigned)DDLHEADERVERSIONINDEX << " - header size: " << AliHLTGetDDLHeaderSize(fEventBufferReadPtr)
		    << ENDLOG;
		eventID = fNextInvalidEventID;
		++fNextInvalidEventID;
		dataDefinitivelyCorrupt = true;
		corruptEventID = true;
		}
	    else
		{
		LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::PollForEvent", "DDL header" )
		    << "Found DDL header version number. : " 
		    << AliHLTLog::kDec 
		    << (unsigned)AliHLTGetDDLHeaderVersion(fEventBufferReadPtr) << " - header version byte index: "
		    << (unsigned)DDLHEADERVERSIONINDEX << " - header size: " << AliHLTGetDDLHeaderSize(fEventBufferReadPtr)
		    << ENDLOG;
		unsigned long headerSize = ddlHeader.GetHeaderSize();
#if 0
		LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::PollForEvent", "DDL header data" )
		    << "DDL header: Word 0: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(0)
		    << " - Word 1: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(1)
		    << " - Word 2: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(2)
		    << " - Word 3: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(3)
		    << " - Word 4: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(4)
		    << " - Word 5: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(5)
		    << " - Word 6: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(6)
		    << " - Word 7: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(7)
		    << ENDLOG;
#else
		for ( unsigned jj=0; jj<headerSize/4; jj++ )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::PollForEvent", "DDL header data" )
			<< "DDL header: Word " << AliHLTLog::kDec << jj << ": 0x" << AliHLTLog::kHex << ddlHeader.GetWord(jj)
			<< ENDLOG;
		    }
#endif
		if ( headerSize > dataSize )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTRORC6Handler::PollForEvent", "Data smaller than DDL header" )
			<< "Data received is smaller than reported DDL header size " << AliHLTLog::kDec 
			<< headerSize << " - header version: " << (unsigned)AliHLTGetDDLHeaderVersion(fEventBufferReadPtr) 
			<< "." << ENDLOG;
		    eventID = fNextInvalidEventID;
		    ++fNextInvalidEventID;
		    dataDefinitivelyCorrupt = true;
		    }
		else
		    {
		    eventID = ddlHeader.GetEventID();

		    AliUInt32_t L1msg = ddlHeader.GetL1TriggerType();
		    if ( (L1msg & 0x1) && ( ((L1msg & 0x3C) >> 2) == 0xE ) )
			eventID.fType = kAliEventTypeStartOfRun;
		    else if ( (L1msg & 0x1) && ( ((L1msg & 0x3C) >> 2) == 0xF ) )
			eventID.fType = kAliEventTypeEndOfRun;
		    else
			eventID.fType = kAliEventTypeData;
		    }
		}
	    }

	if ( !nopeEvent )
	    {
	    if ( fEnumerateEventIDs )
		{
		LOG( AliHLTLog::kInformational, "AliHLTRORC6Handler::PollForEvent", "Enumerating found event" )
		    << "Enumerating found event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ") to 0x" << AliHLTLog::kHex << fNextEventID << " (" << AliHLTLog::kDec
		    << fNextEventID << ")." << ENDLOG;
		eventID = fNextEventID;
		++fNextEventID;
		}
	    else if ( !corruptEventID && !dataDefinitivelyCorrupt )
		{
		if ( fLastEventIDWrapAround.tv_sec == 0 &&
		     fLastEventIDWrapAround.tv_usec == 0 )
		    gettimeofday( &fLastEventIDWrapAround, NULL );
		if ( eventID.fNr<=fLastRORCEventID.fNr )
		    {
		    struct timeval now;
		    gettimeofday( &now, NULL );
		    double delta = (now.tv_sec-fLastEventIDWrapAround.tv_sec)*1000000+now.tv_usec-fLastEventIDWrapAround.tv_usec;
		    unsigned long cnt = (unsigned long)round( delta/(ORBIT_PERIOD_SECONDS_DOUBLE*1000000.0) );
		    if ( cnt<=0 )
			cnt = 1;
		    AliEventID_t tmpID( kAliEventTypeUnknown, cnt );
		    tmpID.fNr <<= 36;
		    fAddEventIDCounter.fNr += tmpID.fNr;
		    fLastEventIDWrapAround = now;
		    if ( fNoPeriodCounterLogModulo && !(fPeriodCounterLogCounter % fNoPeriodCounterLogModulo) )
			{
			unsigned long long diff;
			diff = (fAddEventIDCounter.fNr - fLastLogMessageEventIDCounter.fNr) >> 36;
			LOG( AliHLTLog::kImportant, "AliHLTRORC6Handler::PollForEvent", "Period counter" )
			    << "Period counter set to " << AliHLTLog::kDec << (fAddEventIDCounter.fNr >> 36)
			    << " (" << fAddEventIDCounter.fNr
			    << ") (increased by " 
			    << diff << " since last message (last increment: " << cnt << ", last delta: " << delta << " microsec.))." << ENDLOG;
			fLastLogMessageEventIDCounter = fAddEventIDCounter;
			}
		    ++fPeriodCounterLogCounter;
		    //fAddEventIDCounter &= 0xFFFFFFFFFF000000ULL;
		    }
		if ( eventID.fType == kAliEventTypeStartOfRun )
		    {
		    fLastLogMessageEventIDCounter.fNr = fAddEventIDCounter.fNr = 0;
		    gettimeofday( &fLastEventIDWrapAround, NULL );
		    }
		fLastRORCEventID = eventID;
		eventID.fNr |= fAddEventIDCounter.fNr;
		}
	    
	    ed.fEventID = eventID;
	    }
	else
	    ed.fEventID = ~(AliUInt64_t)0;

	++fEventCounter;

	//ed.fEventID = eventID = ((*(AliVolatileUInt32_t*)fEventBufferReadPtr) & 0xFFFFFF00) >> 8;
	ed.fOffset = dataOffset = fEventBufferReadPtr - fEventBuffer;
	LOG( (showErrorData ? AliHLTLog::kError : AliHLTLog::kDebug), "AliHLTRORC6Handler::PollForEvent", "Event found" )
	    << "Found event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ")." << ENDLOG;
	LOG( (showErrorData ? AliHLTLog::kError : AliHLTLog::kDebug), "AliHLTRORC6Handler::PollForEvent", "Found event" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << "): Data: " << dataSize << " (0x" << AliHLTLog::kHex << dataSize
	    << ") bytes (" << AliHLTLog::kDec << blocks << " blocks) - data offset 0x" 
	    << AliHLTLog::kHex << dataOffset << " (" << AliHLTLog::kDec
	    << dataOffset << ") - report buffer location 0x" << AliHLTLog::kHex << (unsigned long)fReportBufferReadPtr
	    << " (offset 0x" << (unsigned long)(fReportBufferReadPtr-fReportBuffer) << " ("
	    << AliHLTLog::kDec << (unsigned long)(fReportBufferReadPtr-fReportBuffer) 
	    << "))." << ENDLOG;
	AliVolatileUInt8_t* oldEventBufferReadPtr = fEventBufferReadPtr;
	fEventBufferReadPtr += blocks*fRORCBlockSize;
	fEventBufferReadPtrPhysAddress += blocks*fRORCBlockSize;
	fEventBufferReadPtrBusAddress += blocks*fRORCBlockSize;
	ed.fWrappedSize = dataWrappedSize = 0;
	if ( fEventBufferReadPtr >= fEventBufferEnd )
	    {
	    ed.fWrappedSize = fEventBufferReadPtr - fEventBufferEnd;
	    if ( oldEventBufferReadPtr+dataSize >= fEventBufferEnd )
		dataWrappedSize = oldEventBufferReadPtr+dataSize - fEventBufferEnd;
	    LOG( (showErrorData ? AliHLTLog::kError : AliHLTLog::kDebug), "AliHLTRORC6Handler::PollForEvent", "Event wrapped" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") is wrapped. Wrapped block size: " << ed.fWrappedSize << " (0x"
		<< AliHLTLog::kHex << ed.fWrappedSize << "). Wrapped data size: " << dataWrappedSize << " (0x"
		<< AliHLTLog::kHex << dataWrappedSize << ")." << ENDLOG;
	    fEventBufferReadPtr = fEventBuffer+ed.fWrappedSize;
	    fEventBufferReadPtrPhysAddress = fEventBufferPhysAddress+ed.fWrappedSize;
	    fEventBufferReadPtrBusAddress = fEventBufferBusAddress+ed.fWrappedSize;
	    }

	if ( nopeEvent )
	    {
	    pthread_mutex_lock( &fEventMutex );
	    fFreeBufferSize -= ed.fSize;
	    ReleaseBlock( &ed );
	    pthread_mutex_unlock( &fEventMutex );
	    }
	else
	    {
	    pthread_mutex_lock( &fEventMutex );
	    fEvents.Add( ed );
	    fFreeBufferSize -= ed.fSize;
	    pthread_mutex_unlock( &fEventMutex );
	    }

#ifdef COMPARE_SOFTWARE_EVENTBUFFERREADPTR_CARD_DMA_ADDR
	volatile unsigned long dma_addr;
	dma_addr      = *((volatile unsigned long*)((volatile AliUInt8_t*)fBarPtr+0x00));
	if ( dma_addr != fEventBufferReadPtrPhysAddress )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTRORC6Handler::PollForEvent", "New buffer ptrs" )
		<< "Card DMA address and software physical event buffer read ptr do not match: 0x"
		<< AliHLTLog::kHex << dma_addr << " <-> 0x" << fEventBufferReadPtrPhysAddress
		<< " ("
		<< AliHLTLog::kDec << dma_addr << " <-> " << fEventBufferReadPtrPhysAddress
		<< ")." << ENDLOG;
	    }
#endif
    
#if 1
#ifdef USE_SPECIAL_REPORT_OVERWRITES
	*(AliVolatileUInt64_t*)fReportBufferReadPtr = 0xFFFFFFF0FFFFFFF0ULL;
	*(((AliVolatileUInt64_t*)fReportBufferReadPtr)+1) = 0xFFFFFFF0FFFFFFF0ULL;
	*(((AliVolatileUInt64_t*)fReportBufferReadPtr)+2) = 0xFFFFFFF0FFFFFFF0ULL;
#else
	*(AliVolatileUInt64_t*)fReportBufferReadPtr = 0xFFFFFFFFFFFFFFFFULL;
	*(((AliVolatileUInt64_t*)fReportBufferReadPtr)+1) = 0xFFFFFFFFFFFFFFFFULL;
	*(((AliVolatileUInt64_t*)fReportBufferReadPtr)+2) = 0xFFFFFFFFFFFFFFFFULL;
#endif
#endif
	fReportBufferReadPtr += 24;
	if ( fReportBufferReadPtr >= fReportBufferEnd )
	    {
	    fReportBufferReadPtr = fReportBuffer;
	    }
#ifdef SIMULATE
#if 0
	gReportBufferReadPtr = (AliUInt64_t*)fReportBufferReadPtr;
#endif
#endif
	LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::PollForEvent", "New buffer ptrs" )
	    << "New report buffer ptr: 0x" << AliHLTLog::kHex << (unsigned long)fReportBufferReadPtr
	    << " (offset " << AliHLTLog::kDec << (unsigned long)(fReportBufferReadPtr-fReportBuffer)
	    << " (0x" << AliHLTLog::kHex << (unsigned long)(fReportBufferReadPtr-fReportBuffer)
	    << ")) - New event buffer ptr: 0x" << AliHLTLog::kHex << (unsigned long)fEventBufferReadPtr
	    << " (offset " << AliHLTLog::kDec << (unsigned long)(fEventBufferReadPtr-fEventBuffer)
	    << " (0x" << AliHLTLog::kHex << (unsigned long)(fEventBufferReadPtr-fEventBuffer)
	    << ")) - New event buffer bus address: 0x" << AliHLTLog::kHex << (unsigned long)fEventBufferReadPtrBusAddress
	    << " (offset " << AliHLTLog::kDec << (unsigned long)(fEventBufferReadPtrBusAddress-fEventBufferBusAddress)
	    << " (0x" << AliHLTLog::kHex << (unsigned long)(fEventBufferReadPtrBusAddress-fEventBufferBusAddress)
	    << "))." << ENDLOG;

	if ( nopeEvent )
	    return EAGAIN;
	
	return 0;
	}
    return EAGAIN;
    }

int AliHLTRORC6Handler::ReleaseEvent( AliEventID_t eventID )
    {
    unsigned long ndx;
    EventData* edp;
    LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::ReleaseEvent", "Releasing event" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << ") to be released." << ENDLOG;
    pthread_mutex_lock( &fEventMutex );
    if ( MLUCVectorSearcher<EventData,AliEventID_t>::FindElement( fEvents, &EventDataSearchFunc, eventID, ndx ) )
	{
	edp = fEvents.GetPtr( ndx );
	LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::ReleaseEvent", "Released event data" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << "): " << edp->fSize << " (0x" << AliHLTLog::kHex << edp->fSize
	    << ") bytes - wrapped size: " << AliHLTLog::kDec << edp->fWrappedSize
	    << " (0x" << AliHLTLog::kHex << edp->fWrappedSize << ") - offset 0x" 
	    << AliHLTLog::kHex << edp->fOffset << " (" << AliHLTLog::kDec
	    << edp->fOffset << ") - current event buffer release offset: 0x"
	    << AliHLTLog::kHex << fEventBufferReleasedOffset << " (" << AliHLTLog::kDec
	    << fEventBufferReleasedOffset << ")." << ENDLOG;
	ReleaseBlock( edp );
	//fFreeBufferSize += edp->fSize;
	fEvents.Remove( ndx );
	pthread_mutex_unlock( &fEventMutex );
	return 0;
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::ReleaseEvent", "Event not found" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") to be released could not be found in list. Giving up..." 
	    << ENDLOG;
	pthread_mutex_unlock( &fEventMutex );
	return EIO;
	}
    }

int AliHLTRORC6Handler::FlushEvents()
    {
    PSI_Status    status;
    int ret;
#ifndef VERIFIED_WRITE
    unsigned long wr;
#endif

    // enable DMA and flush last event out of queue
#ifdef VERIFIED_WRITE
    ret = Write32Control( (AliUInt32_t)kDMAENABLE, 0x00000003, true, 0x00000003, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::FlushEvents", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#else
    wr = 0x00000003;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kDMAENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::FlushEvent", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif


    return 0;
    }

int AliHLTRORC6Handler::ReadRORCBusyCounter( AliUInt32_t& fifoFullCnt )
    {
    PSI_Status    status;

#ifndef SIMULATE
    status = PSI_read( fBarRegion, (AliUInt32_t)kFIFOFULLCOUNT, _dword_, 4, (void*)&fifoFullCnt );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::ReadRORCBusyCounter", "Read Error" )
	    << "FIFO full count PCI read error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#else
    fifoFullCnt = ~(AliUInt32_t)0;
    return ENOSYS;
#endif
    return 0;
    }


bool AliHLTRORC6Handler::ReleaseBlock( EventData* edp )
    {
    TBlockIterator iter, end;
    if ( edp->fWrappedSize )
	edp->fSize -= edp->fWrappedSize;
    bool wrapped = false;
    if ( fEventBufferReleasedOffset == edp->fOffset )
	{
	unsigned releaseEventCnt = 1;
	unsigned long releaseDataSize = edp->fSize;
	// The new block to be released follows directly after the 
	// point up to which the data is reported as released to the RORC.
	// So we can directly report this block as released to the RORC as well.
	fFreeBufferSize += edp->fSize; // +edp->fWrappedSize; // edp->fWrappedSize not needed, as edp->fSize should include everything...
	if ( edp->fWrappedSize )
	    {
	    wrapped = true;
	    fEventBufferReleasedOffset = edp->fWrappedSize;
	    }
	else
	    fEventBufferReleasedOffset += edp->fSize;
	if ( fEventBufferReleasedOffset == fEventBufferSize )
	    {
	    fEventBufferReleasedOffset = 0;
	    wrapped = true;
	    }
	// Check to see whether we have any released blocks that are adjacent to
	// the newly released block. In that case we can update the RORCs read
	// pointer even further.
	iter = fBlocks.begin();
	while ( iter!=fBlocks.end() && iter->fOffset==fEventBufferReleasedOffset )
	    {
	    fFreeBufferSize += iter->fSize;
	    fEventBufferReleasedOffset += iter->fSize;
	    releaseEventCnt++;
	    releaseDataSize += iter->fSize;
	    fBlocks.erase( iter );
	    if ( fEventBufferReleasedOffset >= fEventBufferSize )
		{
		fEventBufferReleasedOffset = fEventBufferReleasedOffset-fEventBufferSize;
		wrapped = true;
		}
	    iter = fBlocks.begin();
	    }
	if ( wrapped && !fBlocks.empty() )
	    {
	    LOG( AliHLTLog::kFatal, "AliHLTRORC6Handler::ReleaseBlock", "Inconsistency detected" )
		<< "Release buffer wrap around detected but non-wrapped block list is not empty!" << ENDLOG;
	    }
	if ( wrapped )
	    {
	    iter = fWrapBlocks.begin();
	    while ( iter!=fWrapBlocks.end() && iter->fOffset==fEventBufferReleasedOffset )
		{
		fFreeBufferSize += iter->fSize;
		fEventBufferReleasedOffset += iter->fSize;
		releaseEventCnt++;
		releaseDataSize += iter->fSize;
		fWrapBlocks.erase( iter );
		iter = fWrapBlocks.begin();
		}
	    fBlocks = fWrapBlocks;
	    fWrapBlocks.clear();
	    }
	AliVolatileUInt64_t readAddress = fEventBufferBusAddress+fEventBufferReleasedOffset-fRORCBlockSize;
	if ( readAddress < fEventBufferBusAddress )
	    readAddress = fEventBufferEndBusAddress - (fEventBufferBusAddress-readAddress);
	if ( readAddress >= fEventBufferEndBusAddress )
	    readAddress = fEventBufferBusAddress+readAddress-fEventBufferEndBusAddress;
	LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::ReleaseBlock", "New release offset" )
	    << AliHLTLog::kDec << releaseEventCnt << " events released with " << releaseDataSize
	    << " bytes. - "
	    << "New event buffer release offset: 0x" << AliHLTLog::kHex 
	    << fEventBufferReleasedOffset << " (" << AliHLTLog::kDec
	    << fEventBufferReleasedOffset << ") - *(AliVolatileUInt64_t*)fAdminBuffer: 0x"
	    << AliHLTLog::kHex << (unsigned long)(readAddress) << " ("
	    << AliHLTLog::kDec << (unsigned long)(readAddress)
	    << ")." << ENDLOG;
	*(AliVolatileUInt64_t*)fAdminBuffer = readAddress;
	}
    else
	{
	// the block to be released is somewhere in the middle of the used region. 
	// So we have to keep track of it for further released when the RORC's reelase
	// address as advanced sufficiently.
	BlockData bd;
	bd.fOffset = edp->fOffset;
	bd.fSize = edp->fSize;
	if ( bd.fOffset>fEventBufferReleasedOffset )
	    {
	    InsertBlock( fBlocks, bd );
	    if ( edp->fWrappedSize )
		{
		bd.fOffset = 0;
		bd.fSize = edp->fWrappedSize;
		InsertBlock( fWrapBlocks, bd );
		}
	    }
	else
	    InsertBlock( fWrapBlocks, bd );
	}
#if 0
    if ( fBlocks.empty() )
	{
	LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::ReleaseBlock", "Block list empty" )
	    << "Block list is empty" << ENDLOG;
	}
    else
	{
	iter = fBlocks.begin();
	end = fBlocks.end();
	MLUCString blocksStr;
	char tmp[4096];
	while ( iter != end )
	    {
	    snprintf( tmp, 4096, " - Block offset: %lu - block size: %lu",
		      (unsigned long)iter->fOffset, (unsigned long)iter->fSize );
	    blocksStr += tmp;
	    iter++;
	    }
	LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::ReleaseBlock", "Block list" )
	    << "Block list contains " << AliHLTLog::kDec << fBlocks.size() << " blocks" 
	    << blocksStr.c_str() << ENDLOG;
	}
    if ( fWrapBlocks.empty() )
	{
	LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::ReleaseBlock", "Wrap Block list empty" )
	    << "Wrap Block list is empty" << ENDLOG;
	}
    else
	{
	iter = fWrapBlocks.begin();
	end = fWrapBlocks.end();
	MLUCString blocksStr;
	char tmp[4096];
	while ( iter != end )
	    {
	    snprintf( tmp, 4096, " - Block offset: %lu - block size: %lu",
		      (unsigned long)iter->fOffset, (unsigned long)iter->fSize );
	    blocksStr += tmp;
	    iter++;
	    }
	LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::ReleaseBlock", "Wrap Block list" )
	    << "Wrap Block list contains " << AliHLTLog::kDec << fWrapBlocks.size() << " blocks" 
	    << blocksStr.c_str() << ENDLOG;
	}
#endif
    return true;
    }

int AliHLTRORC6Handler::Write32Control( const AliUInt32_t index, const AliUInt32_t value, const bool verify, const AliUInt32_t verifyValue, const AliUInt32_t verifyMask, bool verifyFailError, PSI_Status& status )
    {
    AliUInt32_t wr;
    unsigned cnt=0;
    bool verified=true;
    AliUInt32_t rd=0;
    status = PSI_OK;
    do
	{
	wr = value;
#ifndef SIMULATE
	status = PSI_write( fBarRegion, index, _dword_, 4, (void*)&wr );
	if ( status != PSI_OK )
	    {
	    return EIO;
	    }
#endif

	if ( verify )
	    {
#ifndef SIMULATE
	    status = PSI_read( fBarRegion, index, _dword_, 4, (void*)&rd );
	    if ( status != PSI_OK )
		{
		return EIO;
		}
	    if ( (rd & verifyMask) == (verifyValue & verifyMask) )
		verified = true;
	    else
		verified = false;
#endif
	    }

	++cnt;
	}
    while ( !verified && cnt<1000 );
    if ( !verified )
	{
	LOG( verifyFailError ? AliHLTLog::kError : AliHLTLog::kWarning, "AliHLTRORC6Handler::Write32Control", "Error verifying written value" )
	    << "Cannot verify value written to address 0x" << AliHLTLog::kHex << index << " after "
	    << AliHLTLog::kDec << cnt << " tries - Value to write: 0x" << AliHLTLog::kHex
	    << value << " - value read: 0x" << rd << "." << ENDLOG;
	if ( verifyFailError )
	    return ENXIO;
	}
    return 0;
    }

int AliHLTRORC6Handler::ActivateHWCoProc()
    {
    PSI_Status    status;
    int ret;
    AliUInt32_t config=0;
#ifndef SIMULATE
    status = PSI_read( fBarRegion, (AliUInt32_t)kFCFCONTROL, _dword_, 4, (void*)&config );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Read Error" )
	    << "PCI address read error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    config |= kFCFCONTROLENABLE;
    if ( !fNoFlowControl )
	config |= kFCFCONTROLFLOWCONTROL;
    else
	config &= ~kFCFCONTROLFLOWCONTROL;
    if ( fSinglePadSuppressionSet )
      {
	if ( fSinglePadSuppression )
	  config |= kFCFCONTROLSINGLEPADDSUPPRESSION;
	else
	  config &= ~kFCFCONTROLSINGLEPADDSUPPRESSION;
      }
    if ( fBypassMergerSet )
	{
	if ( fBypassMerger )
	    config |= kFCFCONTROLMERGERBYPASS;
	else
	    config &= ~kFCFCONTROLMERGERBYPASS;
	}
    if ( fDeconvPadSet )
	{
	if ( fDeconvPad )
	    config |= kFCFCONTROLDECONVPAD;
	else
	    config &= ~kFCFCONTROLDECONVPAD;
	}
     if ( fClusterLowerLimitSet )
	{
	config &= ~(kFCFCONTROLCLUSTERLOWERLIMITMASK << kFCFCONTROLCLUSTERLOWERLIMITSHIFT);
	config |= (fClusterLowerLimit & kFCFCONTROLCLUSTERLOWERLIMITMASK) << kFCFCONTROLCLUSTERLOWERLIMITSHIFT;
	}
    if ( fSingleSeqLimitSet )
	{
	config &= ~(kFCFCONTROLSINGLESEQUENCELIMITMASK << kFCFCONTROLSINGLESEQUENCELIMITSHIFT);
	config |= (fSingleSeqLimit & kFCFCONTROLSINGLESEQUENCELIMITMASK) << kFCFCONTROLSINGLESEQUENCELIMITSHIFT;
	}
#ifdef VERIFIED_WRITE
    ret = Write32Control( (AliUInt32_t)kFCFCONTROL, config, true, config, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#else
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kFCFCONTROL, _dword_, 4, (void*)&config );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI address write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif
    // extra configurations in separate register:
    config=0;
#ifndef SIMULATE
    status = PSI_read( fBarRegion, (AliUInt32_t)kFCFCONTROLEXT, _dword_, 4, (void*)&config );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Read Error" )
	    << "PCI address read error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    if ( fMergerDistanceSet )
	{
	config &= ~(kFCFCONTROLMERGERDISTANCEMASK << kFCFCONTROLMERGERDISTANCESHIFT);
	config |= (fMergerDistance & kFCFCONTROLMERGERDISTANCEMASK) << kFCFCONTROLMERGERDISTANCESHIFT;
	}
    if ( fMergerAlgorithmFollowSet )
	{
	if ( fMergerAlgorithmFollow )
	    config |= kFCFCONTROLMERGERALGORITHMFOLLOW;
	else
	    config &= ~kFCFCONTROLMERGERALGORITHMFOLLOW;
	}
    if ( fChargeToleranceSet )
	{
	config &= ~(kFCFCONTROLCHARGETOLERANCEMASK << kFCFCONTROLCHARGETOLERANCESHIFT);
	config |= (fChargeTolerance & kFCFCONTROLCHARGETOLERANCEMASK) << kFCFCONTROLCHARGETOLERANCESHIFT;
	}
#ifdef VERIFIED_WRITE
    ret = Write32Control( (AliUInt32_t)kFCFCONTROLEXT, config, true, config, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#else
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kFCFCONTROLEXT, _dword_, 4, (void*)&config );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Initialize", "Write Error" )
	    << "PCI address write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    return 0;
    }

int AliHLTRORC6Handler::DeactivateHWCoProc()
    {
    PSI_Status    status;
    //int ret;
    // Reset/disable HW Co-proc.
    /*
#ifdef VERIFIED_WRITE
    ret = Write32Control( (AliUInt32_t)kFCFCONTROL, (AliUInt32_t)kFCFCONTROLRESET, true, (AliUInt32_t)0x00000000, (AliUInt32_t)0xFFFFFFFF, false, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Deinitialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    ret = Write32Control( (AliUInt32_t)kFCFCONTROL, (AliUInt32_t)0x00000000, true, (AliUInt32_t)0x00000000, (AliUInt32_t)0xFFFFFFFF, false, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Deinitialize", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#else
    */
    AliUInt32_t wr=kFCFCONTROLRESET;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kFCFCONTROL, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Deinitialize", "Write Error" )
	    << "PCI address write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    /*
    wr=0;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kFCFCONTROL, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::Deinitialize", "Write Error" )
	    << "PCI address write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif
    */
    return 0;
    }

void AliHLTRORC6Handler::SignalErrorToRORC()
    {
    PSI_Status    status;
    int ret;
#ifndef VERIFIED_WRITE
    unsigned long wr;
#endif

#ifdef VERIFIED_WRITE
    ret = Write32Control( (AliUInt32_t)kREPORTERROR, 0x00000000, true, 0x00000000, true, status );
    if ( ret || status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::SignalErrorToRORC", "Write Error" )
	    << "PSI write error - Status: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ") PSI Status: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	}
#else
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBarRegion, (AliUInt32_t)kREPORTERROR, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC6Handler::SignalErrorToRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	}
#endif
#endif
    }


void AliHLTRORC6Handler::InsertBlock( TBlockData& blocks, BlockData& block )
    {
    TBlockIterator iter, end, prevIter;
    iter = blocks.begin();
    end = blocks.end();
    bool inserted = false;
    BlockData bd;
    // Find the right position (ordered by offset), taking into account wrap arounds
    // and see if we have an adjacent block to merge with.
    while ( iter != end )
	{
	if ( block.fOffset+block.fSize == iter->fOffset )
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::InsertBlock", "Merging block" )
		<< "Merging block at beginning: (" << AliHLTLog::kDec
		<< block.fOffset << "/" << block.fSize << ") + (" << iter->fOffset
		<< "/" << iter->fSize << ") == (" << block.fOffset << "/"
		<< iter->fSize+block.fSize << ")." << ENDLOG;
	    iter->fOffset = block.fOffset;
	    iter->fSize += block.fSize;
	    inserted = true;
	    if ( iter!=blocks.begin() && (prevIter)->fOffset+(prevIter)->fSize==iter->fOffset )
		{
		LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::InsertBlock", "Merging block" )
		    << "Merging in preceeding block at beginning: (" << AliHLTLog::kDec
		    << (prevIter)->fOffset << "/" << (prevIter)->fSize << ") + (" << iter->fOffset
		    << "/" << iter->fSize << ") == (" << (prevIter)->fOffset << "/"
		    << iter->fSize+(prevIter)->fSize << ")." << ENDLOG;
		(prevIter)->fSize += iter->fSize;
		blocks.erase( iter );
		}
	    break;
	    }
	if ( iter->fOffset+iter->fSize == block.fOffset )
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::InsertBlock", "Merging block" )
		<< "Merging block at end: (" << AliHLTLog::kDec
		<< iter->fOffset << "/" << iter->fSize << ") + (" << block.fOffset 
		<< "/" << block.fSize << ") == (" << iter->fOffset << "/"
		<< iter->fSize+block.fSize << ")." << ENDLOG;
	    iter->fSize += block.fSize;
	    inserted = true;
	    TBlockIterator nextIter = iter;
	    nextIter++;
	    if ( (nextIter)!=end && (nextIter)->fOffset==iter->fOffset+iter->fSize )
		{
		LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::InsertBlock", "Merging block" )
		    << "Merging in succeeding block at end: (" << AliHLTLog::kDec
		    << iter->fOffset << "/" << iter->fSize << ") + (" << (nextIter)->fOffset
		    << "/" << (nextIter)->fSize << ") == (" << iter->fOffset << "/"
		    << iter->fSize+(nextIter)->fSize << ")." << ENDLOG;
		iter->fSize += (nextIter)->fSize;
		blocks.erase( nextIter );
		}
	    break;
	    }
	if ( block.fOffset < iter->fOffset )
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::InsertBlock", "Inserting block" )
		<< "Inserting block (" << AliHLTLog::kDec
		<< block.fOffset << "/" << block.fSize << ") before (" << iter->fOffset 
		<< "/" << iter->fSize << ")." << ENDLOG;
	    bd.fOffset = block.fOffset;
	    bd.fSize = block.fSize;
	    blocks.insert( iter, bd );
	    inserted = true;
	    break;
	    }
	prevIter = iter;
	iter++;
	}
    if ( !inserted )
	{
	// Nothing found where to insert or merge, so we append it at the end.
	LOG( AliHLTLog::kDebug, "AliHLTRORC6Handler::InsertBlock", "Appending block" )
	    << "Appending block: (" << AliHLTLog::kDec
	    << block.fOffset << "/" << block.fSize 
	    << ") at end." << ENDLOG;
	bd.fOffset = block.fOffset;
	bd.fSize = block.fSize;
	blocks.insert( end, bd );
	}
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
