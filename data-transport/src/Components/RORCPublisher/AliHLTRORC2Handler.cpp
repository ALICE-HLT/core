/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTRORC2Handler.hpp"
#include "AliHLTDDLHeader.hpp"
#include "AliHLTDDLHeaderData.h"
#include "AliHLTLog.hpp"
#include <netinet/in.h>
#include <errno.h>


#define REPORTBUFFERSIZEWORKAROUND 1



//#define RORC_DEBUG_USE_INTERNAL_PATTERNGENERATOR
#ifdef RORC_DEBUG_USE_INTERNAL_PATTERNGENERATOR
#warning RORC_DEBUG_USE_INTERNAL_PATTERNGENERATOR is enabled
#endif

#if 0
#define SIMULATE
#endif

#ifdef SIMULATE
#warning Only simulating RORC accesses via signals. Disable the SIMULATE define two lines above to get real hardware access.
#include <signal.h>
static AliUInt32_t* gReportBufferReadPtr = 0;
static AliHLTRORC2Handler::AliVolatileUInt32_t* gAdminBufferReadPtr = 0;
static unsigned long gEventBufferBusAddress = 0;
static unsigned long gEventBufferSize = 0;
static AliHLTRORC2Handler::AliVolatileUInt8_t* gEventBufferPtr = 0;
static unsigned long gOffsetWriteEnd = 0;
static unsigned long gOffsetWrite = 0;
static AliUInt64_t gEventID = 0;
#define TRIGGER_SIGNAL SIGUSR1

#define SIMULATED_EVENT_32BIT_WORD_CNT 0x20000
#define SIMULATED_EVENT_SIZE_BYTES_REAL1 ( SIMULATED_EVENT_32BIT_WORD_CNT*sizeof(AliUInt32_t)+sizeof(AliUInt32_t)+( SIMULATED_EVENT_32BIT_WORD_CNT&1 ? 0 : sizeof(AliUInt32_t) ) )
#define BLOCKSIZE 128
#define BLOCKCOUNT ( (SIMULATED_EVENT_SIZE_BYTES_REAL1 / BLOCKSIZE) + ( (SIMULATED_EVENT_SIZE_BYTES_REAL1 % BLOCKSIZE) ? 1 : 0 ) )
#define SIMULATED_EVENT_SIZE_BYTES_REAL ( BLOCKCOUNT*BLOCKSIZE )

static AliHLTDDLHeaderDataV1 gCommonDataFormatHeader;

void TriggerSignalHandler( int )
    {
    bool wrote = false;
    LOG( AliHLTLog::kDebug, "TriggerSignalHandler", "Signal received" )
	<< "Received signal for simulated event generation - gOffsetWrite: "
	<< AliHLTLog::kDec << gOffsetWrite << " - gOffsetWriteEnd: "
	<< gOffsetWriteEnd << " - gReportBufferReadPtr: 0x"
	<< AliHLTLog::kHex << (unsigned long)gReportBufferReadPtr << " - gAdminBufferReadPtr: "
	<< (unsigned long)gAdminBufferReadPtr << " - gEventBufferBusAddress: 0x"
	<< gEventBufferBusAddress << " - gEventBufferPtr: 0x"
	<< (unsigned long)gEventBufferPtr << " - gEventID: 0x" << gEventID << AliHLTLog::kDec
	<< "/" << gEventID << " - SIMULATED_EVENT_32BIT_WORD_CNT: "
	<< SIMULATED_EVENT_32BIT_WORD_CNT << " - SIMULATED_EVENT_SIZE_BYTES_REAL: "
	<< SIMULATED_EVENT_SIZE_BYTES_REAL << "." << ENDLOG;
    unsigned long oldOffsetWrite = gOffsetWrite;
    if ( SIMULATED_EVENT_SIZE_BYTES_REAL > gEventBufferSize )
	return;
    // Events (event IDs) will be lost, if the buffer is full. This is in purpose as a marker for such conditions.
    AliHLTSetDDLHeaderEventID1( &gCommonDataFormatHeader, gEventID & 0x0000000000000FFFULL );
    AliHLTSetDDLHeaderEventID2( &gCommonDataFormatHeader, (gEventID & 0x0000000FFFFFF000ULL) >> 12 );
    AliHLTSetDDLHeaderMiniEventID( &gCommonDataFormatHeader, gEventID & 0x0000000000000FFFULL );
    gEventID = (gEventID+1) & 0x0000000FFFFFFFFFULL;
    if ( gReportBufferReadPtr && *(AliHLTRORC2Handler::AliVolatileUInt32_t*)(gReportBufferReadPtr)==0xFFFFFFFF )
	{

	if ( gOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL < gEventBufferSize )
	    {
	    // write pointer will  not make wrap around
	    if ( gOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL >= gOffsetWriteEnd && gOffsetWrite<gOffsetWriteEnd )
		{
		// write pointer will stay less than write end pointer, or write end pointer has made wrap around and write pointer will not.
		// update write end pointer
		gOffsetWriteEnd = ((unsigned long)(*((AliHLTRORC2Handler::AliVolatileUInt32_t*)gAdminBufferReadPtr))) - gEventBufferBusAddress;
		}
	    // try again
	    if ( gOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL < gOffsetWriteEnd || gOffsetWrite>gOffsetWriteEnd )
		{
		// write pointer will stay less than write end pointer, or write end pointer has made wrap around and write pointer will not.
		memcpy( (AliUInt8_t*)gEventBufferPtr+gOffsetWrite, &gCommonDataFormatHeader, sizeof(gCommonDataFormatHeader) );
		AliHLTRORC2Handler::AliVolatileUInt32_t* start = (AliHLTRORC2Handler::AliVolatileUInt32_t*)(gEventBufferPtr+gOffsetWrite+sizeof(gCommonDataFormatHeader));
		for ( unsigned long i=0; i<SIMULATED_EVENT_32BIT_WORD_CNT-sizeof(gCommonDataFormatHeader)/sizeof(AliUInt32_t); i++ )
		    *(start+i) = i;
		gOffsetWrite += SIMULATED_EVENT_SIZE_BYTES_REAL;
		wrote = true;
		}
	    }
	else
	    {
	    // write pointer will make wrap around.
	    if ( gOffsetWrite<gOffsetWriteEnd || gOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL>=gEventBufferSize+gOffsetWriteEnd )
		{
		// write end pointer has not made wrap around yet or write wrap around would go beyond wrapped write end pointer
		// update write end pointer
		gOffsetWriteEnd = ((unsigned long)(*((AliHLTRORC2Handler::AliVolatileUInt32_t*)gAdminBufferReadPtr))) - gEventBufferBusAddress;
		}
	    // try again
	    if ( gOffsetWrite>=gOffsetWriteEnd && gOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL<gEventBufferSize+gOffsetWriteEnd )
		{
		if ( gOffsetWrite+sizeof(gCommonDataFormatHeader) <= gEventBufferSize )
		    {
		    memcpy( (AliUInt8_t*)gEventBufferPtr+gOffsetWrite, &gCommonDataFormatHeader, sizeof(gCommonDataFormatHeader) );
		    AliHLTRORC2Handler::AliVolatileUInt32_t* start = (AliHLTRORC2Handler::AliVolatileUInt32_t*)(gEventBufferPtr+gOffsetWrite+sizeof(gCommonDataFormatHeader));
		    unsigned long firstWordCnt = (gEventBufferSize - (gOffsetWrite+sizeof(gCommonDataFormatHeader))) / sizeof(AliUInt32_t);
		    for ( unsigned long i=0; i<firstWordCnt; i++ )
			*(start+i) = i;
		    start = (AliHLTRORC2Handler::AliVolatileUInt32_t*)gEventBufferPtr;
		    for ( unsigned long i=0; i<SIMULATED_EVENT_32BIT_WORD_CNT-firstWordCnt-sizeof(gCommonDataFormatHeader)/sizeof(AliUInt32_t); i++ )
			*(start+i) = i+firstWordCnt;
		    }
		else
		    {
		    unsigned long firstPart = gOffsetWrite+sizeof(gCommonDataFormatHeader)-gEventBufferSize;
		    memcpy( (AliUInt8_t*)gEventBufferPtr+gOffsetWrite, &gCommonDataFormatHeader, firstPart );
		    memcpy( (AliUInt8_t*)gEventBufferPtr, ((AliUInt8_t*)&gCommonDataFormatHeader)+firstPart, sizeof(gCommonDataFormatHeader)-firstPart );
		    AliHLTRORC2Handler::AliVolatileUInt32_t* start = (AliHLTRORC2Handler::AliVolatileUInt32_t*)(gEventBufferPtr+sizeof(gCommonDataFormatHeader)-firstPart);
		    for ( unsigned long i=0; i<SIMULATED_EVENT_32BIT_WORD_CNT-sizeof(gCommonDataFormatHeader)/sizeof(AliUInt32_t); i++ )
			*(start+i) = i;
		    }
		gOffsetWrite = gOffsetWrite+SIMULATED_EVENT_SIZE_BYTES_REAL-gEventBufferSize;
		wrote = true;
		}
	    }

	//printf( "Force triggering event at address 0x%08lX\n", (unsigned long)gReportBufferReadPtr );
	if ( wrote )
	    {
	    *(((AliHLTRORC2Handler::AliVolatileUInt32_t*)gReportBufferReadPtr)+1) = SIMULATED_EVENT_32BIT_WORD_CNT;
	    *(AliHLTRORC2Handler::AliVolatileUInt32_t*)(gReportBufferReadPtr) = gEventBufferBusAddress+oldOffsetWrite;
	    gReportBufferReadPtr += 2;
	    }
	}
    LOG( AliHLTLog::kDebug, "TriggerSignalHandler", "Event Written" )
	<< "Received signal for simulated event generation - gOffsetWrite: "
	<< AliHLTLog::kDec << gOffsetWrite << " - gOffsetWriteEnd: "
	<< gOffsetWriteEnd << " - gReportBufferReadPtr: 0x"
	<< AliHLTLog::kHex << (unsigned long)gReportBufferReadPtr << " - gAdminBufferReadPtr: "
	<< (unsigned long)gAdminBufferReadPtr << " - gEventBufferBusAddress: 0x"
	<< gEventBufferBusAddress << " - gEventBufferPtr: 0x"
	<< (unsigned long)gEventBufferPtr << " - gEventID: 0x" << gEventID << AliHLTLog::kDec
	<< "/" << gEventID << " - SIMULATED_EVENT_32BIT_WORD_CNT: "
	<< SIMULATED_EVENT_32BIT_WORD_CNT << " - SIMULATED_EVENT_SIZE_BYTES_REAL: "
	<< SIMULATED_EVENT_SIZE_BYTES_REAL << " - wrote: "
	<< ( wrote ? "true" : "false" ) << "." << ENDLOG;
    }
#endif


const AliUInt32_t AliHLTRORC2Handler::kEVTBUFPTR          = 0x00;
const AliUInt32_t AliHLTRORC2Handler::kEVTBUFSIZ          = 0x04;
const AliUInt32_t AliHLTRORC2Handler::kREPBUFPTR          = 0x08;
const AliUInt32_t AliHLTRORC2Handler::kLINKREG            = 0x78;
const AliUInt32_t AliHLTRORC2Handler::kRDYRX              = 0x14;
const AliUInt32_t AliHLTRORC2Handler::kDMAPGENABLE        = 0x18;
const AliUInt32_t AliHLTRORC2Handler::kRESET              = 0x1C;
const AliUInt32_t AliHLTRORC2Handler::kFIFORST            = 0x38;
const AliUInt32_t AliHLTRORC2Handler::kPRGPG1             = 0x20;
const AliUInt32_t AliHLTRORC2Handler::kPRGPG2             = 0x24;
const AliUInt32_t AliHLTRORC2Handler::kADMBUFPTRREAD      = 0x2C;
const AliUInt32_t AliHLTRORC2Handler::kEVTBUFPTRREAD      = 0x30;
const AliUInt32_t AliHLTRORC2Handler::kEOBTR              = 0x34;
const AliUInt32_t AliHLTRORC2Handler::kREPBUFSIZ          = 0x34;

const AliUInt32_t AliHLTRORC2Handler::kENABLELINK         = 0x14;
const AliUInt32_t AliHLTRORC2Handler::kDISABLELINK        = 0xB4;

AliHLTRORC2Handler::AliHLTRORC2Handler( unsigned long dataBufferSize, int eventSlotsExp2 ):
    fEvents( eventSlotsExp2 )
    {
    fReportBufferSize = (dataBufferSize/128)*sizeof(AliUInt32_t)*2;
    if ( dataBufferSize % 128 )
	fReportBufferSize += sizeof(AliUInt32_t)*2;
#ifdef REPORTBUFFERSIZEWORKAROUND
    if ( fReportBufferSize > 65520 )
	{
	LOG( AliHLTLog::kWarning, "AliHLTRORC2Handler::AliHLTRORC2Handler", "Report Buffer Size Truncating Workaround" )
	    << "Truncating report buffer size as workaround to 65520 bytes. (From "
	    << AliHLTLog::kDec << fReportBufferSize << " (0x"
	    << AliHLTLog::kHex << fReportBufferSize << "))." << ENDLOG;
	fReportBufferSize = 65520;
	}
#endif
    fAdminBufferSize = 4096;
    pthread_mutex_init( &fEventMutex, NULL );

    fEnumerateEventIDs = false;
    fNextEventID = 0;

    fLastRORCEventID = AliEventID_t();
    fAddEventIDCounter = (~(AliUInt64_t)0) << 36;

    fHeaderSearch = true;
    fReportWordConsistencyCheck = false;
    fReportWordUseBlockSize = false;

#ifdef COMPARE_SOFTWARE_EVENTBUFFERREADPTR_CARD_DMA_ADDR
    fBar0Ptr = NULL;
#endif
#ifdef SIMULATE
    signal( TRIGGER_SIGNAL, TriggerSignalHandler );
    memset( &gCommonDataFormatHeader, 0, sizeof(gCommonDataFormatHeader) );
    AliHLTSetDDLHeaderBlockLength( &gCommonDataFormatHeader, SIMULATED_EVENT_32BIT_WORD_CNT*sizeof(AliUInt32_t) );
    AliHLTSetDDLHeaderVersion( &gCommonDataFormatHeader, 1 );
#endif
#ifdef COMPARE_COUNTER_PATTERN_TAGGED
    fCompareHighTag = 0;
    fDoCompareCounterPattern = false;
#endif
    }

AliHLTRORC2Handler::~AliHLTRORC2Handler()
    {
    pthread_mutex_destroy( &fEventMutex );
    }

int AliHLTRORC2Handler::Open( bool vendorDeviceIndexOrBusSlotFunction, AliUInt16_t pciVendorIDOrBus, AliUInt16_t pciDeviceIDOrSlot, AliUInt16_t pciDeviceIndexOrFunction, AliUInt16_t /*pciDeviceBarIndex*/ )
    {
    char tmp[ 512+1 ];
    PSI_Status    status;

    if ( vendorDeviceIndexOrBusSlotFunction )
	{
	fBar0ID = "/dev/psi/vendor/"; // 0x%04hX/device/0x%04hX/0/base0
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fBar0ID += tmp;
	fBar0ID += "/device/";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fBar0ID += tmp;
	fBar0ID += "/";
	snprintf( tmp, 512, "%04X", pciDeviceIndexOrFunction );
	fBar0ID += tmp;
	fBar0ID += "/base0";
	
	fReportBufferID = "/dev/psi/bigphys/HLT-RORC-Report-VDI-";
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIndexOrFunction );
	fReportBufferID += tmp;
	
	fAdminBufferID = "/dev/psi/bigphys/HLT-RORC-Admin-VDI-";
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fAdminBufferID += tmp;
	fAdminBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fAdminBufferID += tmp;
	fAdminBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIndexOrFunction );
	fAdminBufferID += tmp;
	}
    else
	{
	fBar0ID = "/dev/psi/bus/"; // 0x%04hX/device/0x%04hX/0/base0
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fBar0ID += tmp;
	fBar0ID += "/slot/";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fBar0ID += tmp;
	fBar0ID += "/function/";
	snprintf( tmp, 512, "%04X", pciDeviceIndexOrFunction );
	fBar0ID += tmp;
	fBar0ID += "/base0";
	
	fReportBufferID = "/dev/psi/bigphys/HLT-RORC-Report-BSF-";
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIndexOrFunction );
	fReportBufferID += tmp;
	
	fAdminBufferID = "/dev/psi/bigphys/HLT-RORC-Admin-BSF-";
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fAdminBufferID += tmp;
	fAdminBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fAdminBufferID += tmp;
	fAdminBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIndexOrFunction );
	fAdminBufferID += tmp;
	}

#ifdef SIMULATE
    LOG( AliHLTLog::kWarning, "AliHLTRORC2Handler::Open", "SIMULATING RORC" )
	<< "SIMULATING RORC, NOT ACCESSING ANY REAL HARDWARE." << ENDLOG;
#endif
#ifndef SIMULATE
    status = PSI_openRegion( &fBar0Region, fBar0ID.c_str() );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Open", "Unable to open RORC device bar 0 region" )
	    << "Unable to open RORC device bar 0 region '" << fBar0ID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

#ifdef COMPARE_SOFTWARE_EVENTBUFFERREADPTR_CARD_DMA_ADDR
#ifndef SIMULATE
    status = PSI_mapRegion( fBar0Region, (void**)&fBar0Ptr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Open", "Unable to map RORC device bar 0 region" )
	    << "Unable to map RORC device bar 0 region '" << fBar0ID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    status = PSI_openRegion( &fReportBufferRegion, fReportBufferID.c_str() );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Open", "Unable to open report buffer shm region" )
	    << "Unable to open report buffer shm region '" << fReportBufferID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBar0Region );
	return EIO;
	}

    status = PSI_openRegion( &fAdminBufferRegion, fAdminBufferID.c_str() );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Open", "Unable to open admin buffer shm region" )
	    << "Unable to open admin buffer shm region '" << fAdminBufferID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBar0Region );
	PSI_closeRegion( fReportBufferRegion );
	return EIO;
	}

    unsigned long cur_size = fReportBufferSize;

    status = PSI_sizeRegion( fReportBufferRegion, &cur_size );
    if ( status != PSI_OK && status != PSI_SIZE_SET )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Open", "Unable to set size for report buffer shm region" )
	    << "Unable to set size for report buffer shm region '" << fReportBufferID.c_str()
	    << "' to size " << AliHLTLog::kDec << fReportBufferSize << " (0x" << AliHLTLog::kHex
	    << fReportBufferSize << "): " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBar0Region );
	PSI_closeRegion( fReportBufferRegion );
	PSI_closeRegion( fAdminBufferRegion );
	return EIO;
	}

    if ( cur_size != fReportBufferSize )
	{
	LOG( AliHLTLog::kInformational, "AliHLTRORC2Handler::Open", "Report buffer size changed" )
	    << "Report buffer size is now " << AliHLTLog::kDec << cur_size << " (0x" << AliHLTLog::kHex
	    << cur_size << ") instead of the specified " << AliHLTLog::kDec << fReportBufferSize << " (0x" << AliHLTLog::kHex
	    << fReportBufferSize << ")." << ENDLOG;
	fReportBufferSize = cur_size;
#ifdef REPORTBUFFERSIZEWORKAROUND
    if ( fReportBufferSize > 65520 )
      {
	LOG( AliHLTLog::kWarning, "AliHLTRORC2Handler::Open", "Report Buffer Size Truncating Workaround" )
	  << "Truncating report buffer size as workaround to 65520 bytes. (From "
	  << AliHLTLog::kDec << fReportBufferSize << " (0x"
	  << AliHLTLog::kHex << fReportBufferSize << "))." << ENDLOG;
	fReportBufferSize = 65520;
      }
#endif
	}
    else
	{
	LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::Open", "Report buffer size" )
	    << "Report buffer size is " << AliHLTLog::kDec << fReportBufferSize << " (0x" << AliHLTLog::kHex
	    << fReportBufferSize << ")." << ENDLOG;
	}
    //status = PSI_mapRegion( fReportBufferRegion, reinterpret_cast<void**>(&fReportBuffer) );
    status = PSI_mapRegion( fReportBufferRegion, (void**)&fReportBuffer );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Open", "Unable to map report buffer shm region" )
	    << "Unable to map report buffer shm region '" << fReportBufferID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBar0Region );
	PSI_closeRegion( fReportBufferRegion );
	PSI_closeRegion( fAdminBufferRegion );
	return EIO;
	}
    fReportBufferEnd = fReportBuffer + fReportBufferSize;
    fReportBufferReadPtr = fReportBuffer;
#ifdef SIMULATE
    gReportBufferReadPtr = (AliUInt32_t*)fReportBufferReadPtr;
#endif
    fReportBufferOldReadPtr = NULL;
    //status = PSI_getPhysAddress( reinterpret_cast<void*>(fReportBuffer), reinterpret_cast<void**>(&fReportBufferPhysAddress), reinterpret_cast<void**>(&fReportBufferBusAddress) );
    status = PSI_getPhysAddress( (void*)fReportBuffer, (void**)&fReportBufferPhysAddress, reinterpret_cast<void**>(&fReportBufferBusAddress) );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Open", "Unable to obtain report buffer physical/bus address" )
	    << "Unable to obtain physical and bus address for report buffer: " 
	    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	//PSI_unmapRegion( fReportBufferRegion, reinterpret_cast<void*>(fReportBuffer) );
	PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
	PSI_closeRegion( fBar0Region );
	PSI_closeRegion( fReportBufferRegion );
	PSI_closeRegion( fAdminBufferRegion );
	return EIO;
	}
    LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::Open", "Report buffer pointers" )
	<< "ReportBuffer: 0x" << AliHLTLog::kHex << (unsigned long)fReportBuffer << " - ReportBuffer Phys: 0x"
	<< (unsigned long)fReportBufferPhysAddress << " - ReportBuffer Bus: 0x" << (unsigned long)fReportBufferBusAddress
	<< "." << ENDLOG;

    cur_size = fAdminBufferSize;
    status = PSI_sizeRegion( fAdminBufferRegion, &cur_size );
    if ( status != PSI_OK && status != PSI_SIZE_SET )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Open", "Unable to set size for admin buffer shm region" )
	    << "Unable to set size for admin buffer shm region '" << fAdminBufferID.c_str()
	    << "' to size " << AliHLTLog::kDec << fAdminBufferSize << " (0x" << AliHLTLog::kHex
	    << fAdminBufferSize << "): " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	//PSI_unmapRegion( fReportBufferRegion, fReportBuffer );
	PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
	PSI_closeRegion( fBar0Region );
	PSI_closeRegion( fReportBufferRegion );
	PSI_closeRegion( fAdminBufferRegion );
	return EIO;
	}

    if ( cur_size != fAdminBufferSize )
	{
	LOG( AliHLTLog::kInformational, "AliHLTRORC2Handler::Open", "Admin buffer size changed" )
	    << "Admin buffer size is now " << AliHLTLog::kDec << cur_size << " (0x" << AliHLTLog::kHex
	    << cur_size << ") instead of the specified " << AliHLTLog::kDec << fAdminBufferSize << " (0x" << AliHLTLog::kHex
	    << fAdminBufferSize << ")." << ENDLOG;
	fAdminBufferSize = cur_size;
	}
    else
	{
	LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::Open", "Admin buffer size" )
	    << "Admin buffer size is " << AliHLTLog::kDec << fAdminBufferSize << " (0x" << AliHLTLog::kHex
	    << fAdminBufferSize << ")." << ENDLOG;
	}
    //status = PSI_mapRegion( fAdminBufferRegion, reinterpret_cast<void**>(&fAdminBuffer) );
    status = PSI_mapRegion( fAdminBufferRegion, (void**)&fAdminBuffer );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Open", "Unable to map admin buffer shm region" )
	    << "Unable to map admin buffer shm region '" << fAdminBufferID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	//PSI_unmapRegion( fReportBufferRegion, fReportBuffer );
	PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
	PSI_closeRegion( fBar0Region );
	PSI_closeRegion( fReportBufferRegion );
	PSI_closeRegion( fAdminBufferRegion );
	return EIO;
	}
#ifdef SIMULATE
    gAdminBufferReadPtr = (AliVolatileUInt32_t*)fAdminBuffer;
#endif
    //status = PSI_getPhysAddress( fAdminBuffer, reinterpret_cast<void**>(&fAdminBufferPhysAddress), reinterpret_cast<void**>(&fAdminBufferBusAddress) );
    status = PSI_getPhysAddress( (void*)fAdminBuffer, (void**)&fAdminBufferPhysAddress, (void**)&fAdminBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Open", "Unable to obtain admin buffer physical/bus address" )
	    << "Unable to obtain physical and bus address for admin buffer: " 
	    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	//PSI_unmapRegion( fReportBufferRegion, fReportBuffer );
	PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
	//PSI_unmapRegion( fAdminBufferRegion, fAdminBuffer );
	PSI_unmapRegion( fAdminBufferRegion, (void*)fAdminBuffer );
	PSI_closeRegion( fBar0Region );
	PSI_closeRegion( fReportBufferRegion );
	PSI_closeRegion( fAdminBufferRegion );
	return EIO;
	}
    LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::Open", "Admin buffer pointers" )
	<< "AdminBuffer: 0x" << AliHLTLog::kHex << (unsigned long)fAdminBuffer << " - AdminBuffer Phys: 0x"
	<< (unsigned long)fAdminBufferPhysAddress << " - AdminBuffer Bus: 0x" << (unsigned long)fAdminBufferBusAddress
	<< "." << ENDLOG;

    return 0;
    }
	
int AliHLTRORC2Handler::Close()
    {
    //PSI_unmapRegion( fReportBufferRegion, fReportBuffer );
    PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
    //PSI_unmapRegion( fAdminBufferRegion, fAdminBuffer );
    PSI_unmapRegion( fAdminBufferRegion, (void*)fAdminBuffer );
    PSI_closeRegion( fBar0Region );
    PSI_closeRegion( fReportBufferRegion );
    PSI_closeRegion( fAdminBufferRegion );
    return 0;
    }

int AliHLTRORC2Handler::SetEventBuffer( AliUInt8_t* dataBuffer, unsigned long dataBufferSize, bool first )
    {
    PSI_Status    status;

    fEventBufferReadPtr = fEventBuffer = dataBuffer;
    fEventBufferSize = dataBufferSize;
    fEventBufferEnd = fEventBuffer + fEventBufferSize;
    //status = PSI_getPhysAddress( fEventBuffer, reinterpret_cast<void**>(&fEventBufferPhysAddress), reinterpret_cast<void**>(&fEventBufferBusAddress) );
    status = PSI_getPhysAddress( (void*)fEventBuffer, (void**)&fEventBufferPhysAddress, (void**)&fEventBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::SetEventBuffer", "Unable to obtain event buffer physical/bus address" )
	    << "Unable to obtain physical and bus address for event buffer: " 
	    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#ifdef SIMULATE
    gEventBufferPtr = fEventBufferReadPtr;
    gOffsetWriteEnd = fEventBufferSize;
    gEventBufferBusAddress = fEventBufferBusAddress;
    gEventBufferSize = fEventBufferSize;
#endif
    fEventBufferReadPtrPhysAddress = fEventBufferPhysAddress;
    fEventBufferEndPhysAddress = fEventBufferPhysAddress + fEventBufferSize;
    fEventBufferReadPtrBusAddress = fEventBufferBusAddress;
    fEventBufferEndBusAddress = fEventBufferBusAddress + fEventBufferSize;
    fEventBufferReleasedOffset = 0;
        
    LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::SetEventBuffer", "Event buffer pointers" )
	<< "EventBuffer: 0x" << AliHLTLog::kHex << (unsigned long)fEventBuffer << " - EventBuffer Phys: 0x"
	<< (unsigned long)fEventBufferPhysAddress << " - EventBuffer Bus: 0x" << (unsigned long)fEventBufferBusAddress
	<< " - EventBuffer size: 0x" << fEventBufferSize << " (" << AliHLTLog::kDec << fEventBufferSize << ")." << ENDLOG;
    
    if ( first )
	*(AliVolatileUInt32_t*)fAdminBuffer = (AliUInt32_t)fEventBufferReadPtrBusAddress;

    return 0;
    }

int AliHLTRORC2Handler::InitializeRORC( bool first )
    {
    PSI_Status    status;
    unsigned long wr;

    // disable DMA
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kDMAPGENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    /*
     * FiFo reset
     */
    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kFIFORST, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    /*
     * RORC reset, // do it twice
     */
    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    /*
     *   invalidate DMA and report buffer
     */
    if ( first )
	{
	memset( (void*)fReportBuffer, 0xff, fReportBufferSize );
	memset( (void*)fEventBuffer, 0, fEventBufferSize );
	memset( (void*)fAdminBuffer, 0xff, fAdminBufferSize );
	}
    
    /*
     *   fill DMA descriptor
     */
    //  dma_ctrl[1] = 0x2800;
    //  dma_ctrl[2] = 320;
    //     unsigned long dma_ctrl[4];
    //     dma_ctrl[1] = dmabuf_size;
    //     dma_ctrl[2] = rptbuf_size;
	
    //printf("fill DMA descriptor...\n");
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kEVTBUFPTR, _dword_, 4, (void*)&fEventBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_write( fBar0Region, (AliUInt32_t)kEVTBUFSIZ, _dword_, 4, (void*)&fEventBufferSize );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_write( fBar0Region, (AliUInt32_t)kREPBUFPTR, _dword_, 4, (void*)&fReportBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_write( fBar0Region, (AliUInt32_t)kREPBUFSIZ, _dword_, 4, (void*)&fReportBufferSize );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
	
	
    /*
     *  read pointer
     */
    status = PSI_write( fBar0Region, (AliUInt32_t)kEVTBUFPTRREAD, _dword_, 4, (void*)&fEventBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_write( fBar0Region, (AliUInt32_t)kADMBUFPTRREAD, _dword_, 4, (void*)&fAdminBufferBusAddress );  
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    *(AliVolatileUInt32_t*)fAdminBuffer = (AliUInt32_t)fEventBufferReadPtrBusAddress;
	
    /*
     *  program PG
     */
    wr = 0x000000ff;
    status = PSI_write( fBar0Region, (AliUInt32_t)kPRGPG1, _dword_, 4, (void*)&wr );  
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = 0x00000001;
    status = PSI_write( fBar0Region, (AliUInt32_t)kPRGPG2, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    wr = 0x00000000;
    status = PSI_write( fBar0Region, (AliUInt32_t)kPRGPG2, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
	
	
	
#if 0
    // Moved to ActivateRORC
    // enable DMA 
    wr = 0x00000041;
    status = PSI_write( fBar0Region, (AliUInt32_t)kDMAPGENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    return 0;
    }

int AliHLTRORC2Handler::DeinitializeRORC()
    {
    PSI_Status    status;
    unsigned long wr;

    // disable DMA
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kDMAPGENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    /*
     * FiFo reset
     */
    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kFIFORST, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    /*
     * RORC reset, // do it twice
     */
    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::DeinitializeRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    return 0;
    }

int AliHLTRORC2Handler::InitializeFromRORC( bool first )
    {
    if ( !first )
	{
	while ( *(AliVolatileUInt32_t*)fReportBufferReadPtr == 0xFFFFFFFF && fReportBufferReadPtr<fReportBufferEnd )
	    fReportBufferReadPtr += 4;
	if ( fReportBufferReadPtr>=fReportBufferEnd )
	    fReportBufferReadPtr = fReportBuffer;
#ifdef SIMULATE
	gReportBufferReadPtr = (AliUInt32_t*)fReportBufferReadPtr;
#endif
	fReportBufferOldReadPtr = NULL;

	// set physical address (signalling end of read data)
	fEventBufferReadPtrBusAddress = *(AliVolatileUInt32_t*)fAdminBuffer;
	fEventBufferReleasedOffset = fEventBufferReadPtrBusAddress-fEventBufferBusAddress;
	fEventBufferReadPtr = fEventBuffer+fEventBufferReleasedOffset;
	fEventBufferReadPtrPhysAddress = fEventBufferPhysAddress+fEventBufferReleasedOffset;
	if ( fEventBufferReadPtrBusAddress<fEventBufferBusAddress || fEventBufferReadPtrBusAddress>fEventBufferEndBusAddress )
	    {
	    fEventBufferReleasedOffset = 0;
	    fEventBufferReadPtrBusAddress = fEventBufferBusAddress;
	    fEventBufferReadPtr = fEventBuffer;
	    fEventBufferReadPtrPhysAddress = fEventBufferPhysAddress;
	    }
	*(AliVolatileUInt32_t*)fAdminBuffer = (AliUInt32_t)fEventBufferReadPtrBusAddress;
	}

    return 0;
    }

int AliHLTRORC2Handler::SetOptions( const vector<MLUCString>&, char*&, unsigned& )
    {
    return 0;
    }

void AliHLTRORC2Handler::GetAllowedOptions( vector<MLUCString>& options )
    {
    options.clear();
    }


int AliHLTRORC2Handler::ActivateRORC()
    {
    PSI_Status    status;
    unsigned long wr;
#if 0 
    //Obsolete
    /*
     * Enable DDL link, allow events to arrive.
     */
    wr = kENABLELINK;
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kLINKREG, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::ActivateRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    // enable DMA
    wr = 0x00000001;
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kDMAPGENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::ActivateRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif

    return 0;
    }

int AliHLTRORC2Handler::DeactivateRORC()
    {
    PSI_Status    status;
    unsigned long wr;
    
#if 0 
    //Obsolete
    /*
     * Disable DDL link, stop events from arriving.
     */
    wr = kDISABLELINK;
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kLINKREG, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::DeactivateRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
#endif

    // disable DMA
    wr = 0x00000000;
#ifndef SIMULATE
    status = PSI_write( fBar0Region, (AliUInt32_t)kDMAPGENABLE, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::DeactivateRORC", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
#endif
    return 0;
    }

int AliHLTRORC2Handler::PollForEvent( AliEventID_t& eventID, 
				      AliUInt64_t& dataOffset, AliUInt64_t& dataSize, AliUInt64_t& dataWrappedSize,
				      bool& dataDefinitivelyCorrupt, bool& eventSuppressed, AliUInt64_t& unsuppressedDataSize )
    {
    dataDefinitivelyCorrupt = false;
    eventSuppressed = false;
    unsuppressedDataSize = 0;
    unsigned long blocks;
    if ( fReportBufferOldReadPtr != fReportBufferReadPtr )
	{
	LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::PollForEvent", "Polling for event" )
	    << "Polling for event at location 0x" << AliHLTLog::kHex << (unsigned long)fReportBufferReadPtr
	    << " (bus address: 0x" << AliHLTLog::kHex 
	    << (unsigned long)(fReportBufferBusAddress)+(unsigned long)(fReportBufferReadPtr-fReportBuffer)
	    << ") (offset 0x" << (unsigned long)(fReportBufferReadPtr-fReportBuffer) << " ("
	    << AliHLTLog::kDec << (unsigned long)(fReportBufferReadPtr-fReportBuffer) 
	    << ")) in report buffer." << ENDLOG;
	fReportBufferOldReadPtr = fReportBufferReadPtr;
	}
    if ( *(AliVolatileUInt32_t*)fReportBufferReadPtr!=0xFFFFFFFF )
	{
	EventData ed;
	LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::PollForEvent", "Found event" )
	    << "(*(AliVolatileUInt32_t*)fReportBufferReadPtr): 0x" << AliHLTLog::kHex
	    << *((AliVolatileUInt32_t*)fReportBufferReadPtr) << ENDLOG;
	uint32 readAddr = (*(AliVolatileUInt32_t*)fReportBufferReadPtr);
	AliVolatileUInt8_t* readAddrPtr = (AliVolatileUInt8_t*)(unsigned long)readAddr;
	uint32 words = (*(((AliVolatileUInt32_t*)fReportBufferReadPtr)+1));
	if ( (unsigned long)readAddrPtr != fEventBufferReadPtrBusAddress )
	    {
	    LOG( AliHLTLog::kFatal, "AliHLTRORC2Handler::PollForEvent", "Event Inconsistency" )
		<< "Error: Found inconsistency for event buffer read ptr: fEventBufferReadPtrBusAddress 0x"
		<< AliHLTLog::kHex << (unsigned long)fEventBufferReadPtrBusAddress << " (" << AliHLTLog::kDec
		<< (unsigned long)fEventBufferReadPtrBusAddress << " - Address reported in event: 0x"
		<< AliHLTLog::kHex << (unsigned long)readAddrPtr << " (" << AliHLTLog::kDec << (unsigned long)readAddrPtr
		<< ") bytes - Continuing with value reported by card." << ENDLOG;
	    unsigned long oldOffset = ((unsigned long)fEventBufferReadPtrBusAddress)-((unsigned long)fEventBufferBusAddress);
	    unsigned long offset = ((unsigned long)readAddrPtr)-((unsigned long)fEventBufferBusAddress);
	    fEventBufferReadPtr = fEventBuffer+offset;
	    fEventBufferReadPtrPhysAddress = fEventBufferPhysAddress+offset;
	    fEventBufferReadPtrBusAddress = fEventBufferBusAddress+offset;
	    //fEventBufferReadPtr = readAddrPtr;
	    // Add a block into the free space accounting for the mismatched block.
	    // Otherwise the block will be lost (not claimed by any event) and will not be freed.
	    // then at some point the read pointer for the RORC in the admin buffer cannot be updated
	    // any more and the system will stall.
	    EventData edTmp;
	    edTmp.fEventID = ~(AliUInt64_t)0;
	    edTmp.fOffset = oldOffset;
	    if ( oldOffset <= offset ) // Note: == should actually never happen...
		{
		edTmp.fSize = offset-oldOffset;
		edTmp.fWrappedSize = 0;
		}
	    else
		{
		edTmp.fSize = offset+fEventBufferSize-oldOffset;
		edTmp.fWrappedSize = offset;
		}
	    pthread_mutex_lock( &fEventMutex );
	    ReleaseBlock( &edTmp );
	    pthread_mutex_unlock( &fEventMutex );
	    }
	dataSize = words*4;
	// Firmware transfers length of transferred data always in high word of 64 bit transfer
	// For transfers with odd number of payload data words the high word of the final 64 bit transfer is used
	// For transfers with even number of payload data words a further 64 bit transfer is appended with only the high word used, the low word of this transfer is undefined
	if ( words & 1 )
	    words++;
	else
	    words += 2;
	blocks = words / 32; // One block is always 128B or 32 32bit-words
	// Test for a partially filled block.
	if ( words & 31 )
	    blocks++;
#ifdef COMPARE_COUNTER_PATTERN_TAGGED
        if ( fDoCompareCounterPattern )
            {
            uint32 counterVal = ((uint32)fCompareHighTag) << 28;
            uint32 eventBufferWordCnt = fEventBufferSize / sizeof(uint32);
            for ( uint32 counter = 0; counter < words; counter++ )
                {
                if ( ((uint32*)fEventBufferReadPtr)[counter % eventBufferWordCnt] != counterVal )
                    {
                    LOG( AliHLTLog::kError, "AliHLTRORC2Handler::PollForEvent", "Data Check Error" )
                        << "Data Error: Offset 0x" << AliHLTLog::kHex << counter % eventBufferWordCnt << " (" << AliHLTLog::kDec
                        << counter % eventBufferWordCnt << ") Expected: 0x" << AliHLTLog::kHex << counterVal << " ("
                        << AliHLTLog::kDec << counterVal << ") - Found: 0x" << AliHLTLog::kHex
                        << ((uint32*)fEventBufferReadPtr)[counter % eventBufferWordCnt] << " (" << AliHLTLog::kDec
                        << ((uint32*)fEventBufferReadPtr)[counter % eventBufferWordCnt] << ")." << ENDLOG;
                    if ( (((uint32*)fEventBufferReadPtr)[counter % eventBufferWordCnt] & 0xF0000000) == (((uint32)fCompareHighTag) << 28) )
                        {
                        LOG( AliHLTLog::kError, "AliHLTRORC2Handler::PollForEvent", "Data Check Error" )
                            << "Resuming counter as 0x" << AliHLTLog::kHex << ((uint32*)fEventBufferReadPtr)[counter % eventBufferWordCnt]
                            << " (" << AliHLTLog::kDec << ((uint32*)fEventBufferReadPtr)[counter % eventBufferWordCnt] << ")."
                            << ENDLOG;
                        }
                    counterVal = ((uint32*)fEventBufferReadPtr)[counter % eventBufferWordCnt];
                    }
                counterVal++;
                }
            }
#endif

	if ( fReportWordConsistencyCheck || fReportWordUseBlockSize )
	    {
	    unsigned long blocksReported;
	    blocksReported = (*(AliVolatileUInt32_t*)fReportBufferReadPtr) & 0x00001FFF;
	    if ( fReportWordConsistencyCheck && blocksReported != blocks )
		{
		LOG( AliHLTLog::kError, "AliHLTRORC2Handler::PollForEvent", "Inconsistent block size" )
		    << "Found event has inconsistent sizes: Data size from report word: 0x" << AliHLTLog::kHex << dataSize 
		    << " (" << AliHLTLog::kDec << dataSize << ") - Block count calculated from data size: 0x"
		    << AliHLTLog::kHex << blocks << " (" << AliHLTLog::kDec << blocks 
		    << ") - Block count from report word: 0x" << AliHLTLog::kHex << blocksReported
		    << " (" << AliHLTLog::kDec << blocksReported << ")." << ENDLOG;
		}
	    if ( fReportWordUseBlockSize )
	        {
		if ( blocks != blocksReported )
		    {
		    LOG( AliHLTLog::kError, "AliHLTRORC2Handler::PollForEvent", "Inconsistent block count" )
			<< "Found event has inconsistent block counts: Data size from report word: 0x" << AliHLTLog::kHex << dataSize 
			<< " (" << AliHLTLog::kDec << dataSize << ") - Block count calculated from data size: 0x"
			<< AliHLTLog::kHex << blocks << " (" << AliHLTLog::kDec << blocks 
			<< ") - Block count from report word: 0x" << AliHLTLog::kHex << blocksReported
			<< " (" << AliHLTLog::kDec << blocksReported << ")." << ENDLOG;
		    dataSize = blocksReported*128;
		    }
		blocks = blocksReported;
	        }
	    }
		
	ed.fSize = blocks*128;

	LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::PollForEvent", "Found event" )
	    << "Event found with " << AliHLTLog::kDec << dataSize << " (0x" << AliHLTLog::kHex << dataSize
	    << ") bytes and " << AliHLTLog::kDec << blocks << " blocks." << ENDLOG;

	bool showErrorData = false;

#ifdef RORC_DEBUG_USE_INTERNAL_PATTERNGENERATOR
	if ( dataSize != 1052 || blocks!=9 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTRORC2Handler::PollForEvent", "Wrong event size" )
		<< "Found event has wrong event size " << AliHLTLog::kDec << dataSize << AliHLTLog::kHex
		<< " (0x" << dataSize << ") instead of 1052 or data block count " << AliHLTLog::kDec
		<< blocks << " instead of 9." << ENDLOG;
	    showErrorData = true;
	    }
#endif

	AliHLTDDLHeader ddlHeader;
	if ( fHeaderSearch )
	    {
	    if ( dataSize < AliHLTMinDDLHeaderSize )
		{
		LOG( AliHLTLog::kWarning, "AliHLTRORC2Handler::PollForEvent", "Data smaller than DDL header" )
		    << "Data received is smaller than minimum DDL header size " << AliHLTLog::kDec 
		    << AliHLTMinDDLHeaderSize << "." << ENDLOG;
		}
	    else if ( !ddlHeader.Set( (AliUInt32_t*)(AliVolatileUInt32_t*)fEventBufferReadPtr ) )
		{
		LOG( AliHLTLog::kError, "AliHLTRORC2Handler::PollForEvent", "Error reading DDL header" )
		    << "Error reading DDL header number. Unknown header version or out of memory. Header version: " 
		    << AliHLTLog::kDec 
		    << (unsigned)AliHLTGetDDLHeaderVersion(fEventBufferReadPtr) << " - header version byte index: "
		    << (unsigned)DDLHEADERVERSIONINDEX << " - header size: " << AliHLTGetDDLHeaderSize(fEventBufferReadPtr)
		    << ENDLOG;
		eventID = 0;
		}
	    else
		{
		LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::PollForEvent", "DDL header" )
		    << "Found DDL header version number. : " 
		    << AliHLTLog::kDec 
		    << (unsigned)AliHLTGetDDLHeaderVersion(fEventBufferReadPtr) << " - header version byte index: "
		    << (unsigned)DDLHEADERVERSIONINDEX << " - header size: " << AliHLTGetDDLHeaderSize(fEventBufferReadPtr)
		    << ENDLOG;
		LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::PollForEvent", "DDL header data" )
		    << "DDL header: Word 0: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(0)
		    << " - Word 1: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(1)
		    << " - Word 2: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(2)
		    << " - Word 3: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(3)
		    << " - Word 4: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(4)
		    << " - Word 5: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(5)
		    << " - Word 6: 0x" << AliHLTLog::kHex << ddlHeader.GetWord(6)
		    << ENDLOG;
		unsigned long headerSize = ddlHeader.GetHeaderSize();
		if ( headerSize > dataSize )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTRORC2Handler::PollForEvent", "Data smaller than DDL header" )
			<< "Data received is smaller than reported DDL header size " << AliHLTLog::kDec 
			<< headerSize << " - header version: " << (unsigned)AliHLTGetDDLHeaderVersion(fEventBufferReadPtr) 
			<< "." << ENDLOG;
		    eventID = 0;
		    }
		else
		    {
		    eventID = ddlHeader.GetEventID();
		    }
		}
	    }


	if ( fEnumerateEventIDs )
	    {
	    LOG( AliHLTLog::kInformational, "AliHLTRORC2Handler::PollForEvent", "Enumerating found event" )
		<< "Enumerating found event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") to 0x" << AliHLTLog::kHex << fNextEventID << " (" << AliHLTLog::kDec
		<< fNextEventID << ")." << ENDLOG;
	    eventID = fNextEventID;
	    ++fNextEventID;
	    }
	else
	    {
	    if ( eventID.fNr<=fLastRORCEventID.fNr )
		{
		AliEventID_t tmpID( kAliEventTypeUnknown, 1 );
		tmpID.fNr <<= 36;
		fAddEventIDCounter.fNr += tmpID.fNr;
		//fAddEventIDCounter &= 0xFFFFFFFFFF000000ULL;
		}
	    fLastRORCEventID = eventID;
	    eventID.fNr |= fAddEventIDCounter.fNr;
	    }

	ed.fEventID = eventID;

	//ed.fEventID = eventID = ((*(AliVolatileUInt32_t*)fEventBufferReadPtr) & 0xFFFFFF00) >> 8;
	ed.fOffset = dataOffset = fEventBufferReadPtr - fEventBuffer;
	LOG( (showErrorData ? AliHLTLog::kError : AliHLTLog::kDebug), "AliHLTRORC2Handler::PollForEvent", "Event found" )
	    << "Found event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ")." << ENDLOG;
	LOG( (showErrorData ? AliHLTLog::kError : AliHLTLog::kDebug), "AliHLTRORC2Handler::PollForEvent", "Found event" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << "): Data: " << dataSize << " (0x" << AliHLTLog::kHex << dataSize
	    << ") bytes (" << AliHLTLog::kDec << blocks << " blocks) - data offset 0x" 
	    << AliHLTLog::kHex << dataOffset << " (" << AliHLTLog::kDec
	    << dataOffset << ") - report buffer location 0x" << AliHLTLog::kHex << (unsigned long)fReportBufferReadPtr
	    << " (offset 0x" << (unsigned long)(fReportBufferReadPtr-fReportBuffer) << " ("
	    << AliHLTLog::kDec << (unsigned long)(fReportBufferReadPtr-fReportBuffer) 
	    << "))." << ENDLOG;
	AliVolatileUInt8_t* oldEventBufferReadPtr = fEventBufferReadPtr;
	fEventBufferReadPtr += blocks*128;
	fEventBufferReadPtrPhysAddress += blocks*128;
	fEventBufferReadPtrBusAddress += blocks*128;
	ed.fWrappedSize = dataWrappedSize = 0;
	if ( fEventBufferReadPtr >= fEventBufferEnd )
	    {
	    ed.fWrappedSize = fEventBufferReadPtr - fEventBufferEnd;
	    if ( oldEventBufferReadPtr+dataSize >= fEventBufferEnd )
		dataWrappedSize = oldEventBufferReadPtr+dataSize - fEventBufferEnd;
	    LOG( (showErrorData ? AliHLTLog::kError : AliHLTLog::kDebug), "AliHLTRORC2Handler::PollForEvent", "Event wrapped" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") is wrapped. Wrapped block size: " << ed.fWrappedSize << " (0x"
		<< AliHLTLog::kHex << ed.fWrappedSize << "). Wrapped data size: " << dataWrappedSize << " (0x"
		<< AliHLTLog::kHex << dataWrappedSize << ")." << ENDLOG;
	    fEventBufferReadPtr = fEventBuffer+ed.fWrappedSize;
	    fEventBufferReadPtrPhysAddress = fEventBufferPhysAddress+ed.fWrappedSize;
	    fEventBufferReadPtrBusAddress = fEventBufferBusAddress+ed.fWrappedSize;
	    }
	pthread_mutex_lock( &fEventMutex );
	fEvents.Add( ed );
	pthread_mutex_unlock( &fEventMutex );

#ifdef COMPARE_SOFTWARE_EVENTBUFFERREADPTR_CARD_DMA_ADDR
	volatile unsigned long dma_addr;
	dma_addr      = *((volatile unsigned long*)((volatile AliUInt8_t*)fBar0Ptr+0x00));
	if ( dma_addr != fEventBufferReadPtrPhysAddress )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTRORC2Handler::PollForEvent", "New buffer ptrs" )
		<< "Card DMA address and software physical event buffer read ptr do not match: 0x"
		<< AliHLTLog::kHex << dma_addr << " <-> 0x" << fEventBufferReadPtrPhysAddress
		<< " ("
		<< AliHLTLog::kDec << dma_addr << " <-> " << fEventBufferReadPtrPhysAddress
		<< ")." << ENDLOG;
	    }
#endif
    
#if 1
	*(AliVolatileUInt32_t*)fReportBufferReadPtr = 0xFFFFFFFF;
	*(((AliVolatileUInt32_t*)fReportBufferReadPtr)+1) = 0xFFFFFFFF;
#endif
	fReportBufferReadPtr += 8;
	if ( fReportBufferReadPtr >= fReportBufferEnd )
	    fReportBufferReadPtr = fReportBuffer;
#ifdef SIMULATE
#if 0
	gReportBufferReadPtr = (AliUInt32_t*)fReportBufferReadPtr;
#endif
#endif
	LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::PollForEvent", "New buffer ptrs" )
	    << "New report buffer ptr: 0x" << AliHLTLog::kHex << (unsigned long)fReportBufferReadPtr
	    << " (offset " << AliHLTLog::kDec << (unsigned long)(fReportBufferReadPtr-fReportBuffer)
	    << " (0x" << AliHLTLog::kHex << (unsigned long)(fReportBufferReadPtr-fReportBuffer)
	    << ")) - New event buffer ptr: 0x" << AliHLTLog::kHex << (unsigned long)fEventBufferReadPtr
	    << " (offset " << AliHLTLog::kDec << (unsigned long)(fEventBufferReadPtr-fEventBuffer)
	    << " (0x" << AliHLTLog::kHex << (unsigned long)(fEventBufferReadPtr-fEventBuffer)
	    << ")) - New event buffer bus address: 0x" << AliHLTLog::kHex << (unsigned long)fEventBufferReadPtrBusAddress
	    << " (offset " << AliHLTLog::kDec << (unsigned long)(fEventBufferReadPtrBusAddress-fEventBufferBusAddress)
	    << " (0x" << AliHLTLog::kHex << (unsigned long)(fEventBufferReadPtrBusAddress-fEventBufferBusAddress)
	    << "))." << ENDLOG;


	return 0;
	}
    return EAGAIN;
    }

int AliHLTRORC2Handler::ReleaseEvent( AliEventID_t eventID )
    {
    unsigned long ndx;
    EventData* edp;
    LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::ReleaseEvent", "Releasing event" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << ") to be released." << ENDLOG;
    pthread_mutex_lock( &fEventMutex );
    if ( MLUCVectorSearcher<EventData,AliEventID_t>::FindElement( fEvents, &EventDataSearchFunc, eventID, ndx ) )
	{
	edp = fEvents.GetPtr( ndx );
	LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::ReleaseEvent", "Released event data" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << "): " << edp->fSize << " (0x" << AliHLTLog::kHex << edp->fSize
	    << ") bytes - wrapped size: " << AliHLTLog::kDec << edp->fWrappedSize
	    << " (0x" << AliHLTLog::kHex << edp->fWrappedSize << ") - offset 0x" 
	    << AliHLTLog::kHex << edp->fOffset << " (" << AliHLTLog::kDec
	    << edp->fOffset << ") - current event buffer release offset: 0x"
	    << AliHLTLog::kHex << fEventBufferReleasedOffset << " (" << AliHLTLog::kDec
	    << fEventBufferReleasedOffset << ")." << ENDLOG;
	ReleaseBlock( edp );
	fEvents.Remove( ndx );
	pthread_mutex_unlock( &fEventMutex );
	return 0;
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTRORC2Handler::ReleaseEvent", "Event not found" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") to be released could not be found in list. Giving up..." 
	    << ENDLOG;
	pthread_mutex_unlock( &fEventMutex );
	return EIO;
	}
    }


bool AliHLTRORC2Handler::ReleaseBlock( EventData* edp )
    {
    vector<BlockData>::iterator iter, end;
    if ( edp->fWrappedSize )
	edp->fSize -= edp->fWrappedSize;
    if ( fEventBufferReleasedOffset == edp->fOffset )
	{
	// The new block to be released follows directly after the 
	// point up to which the data is reported as released to the RORC.
	// So we can directly report this block as released to the RORC as well.
	if ( edp->fWrappedSize )
	    fEventBufferReleasedOffset = edp->fWrappedSize;
	else
	    fEventBufferReleasedOffset += edp->fSize;
	if ( fEventBufferReleasedOffset == fEventBufferSize )
	    fEventBufferReleasedOffset = 0;
	// Check to see whether we have any released blocks that are adjacent to
	// the newly released block. In that case we can update the RORCs read
	// pointer even further.
	iter = fBlocks.begin();
	while ( iter!=fBlocks.end() && iter->fOffset==fEventBufferReleasedOffset )
	    {
	    fEventBufferReleasedOffset += iter->fSize;
	    fBlocks.erase( iter );
	    if ( fEventBufferReleasedOffset >= fEventBufferSize )
		fEventBufferReleasedOffset = fEventBufferReleasedOffset-fEventBufferSize;
	    iter = fBlocks.begin();
	    }
	LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::ReleaseBlock", "New release offset" )
	    << "New event buffer release offset: 0x" << AliHLTLog::kHex 
	    << fEventBufferReleasedOffset << " (" << AliHLTLog::kDec
	    << fEventBufferReleasedOffset << ") - *(AliVolatileUInt32_t*)fAdminBuffer: "
	    << (unsigned long)(fEventBufferBusAddress+fEventBufferReleasedOffset) << "." << ENDLOG;
	*(AliVolatileUInt32_t*)fAdminBuffer = (unsigned long)(fEventBufferBusAddress+fEventBufferReleasedOffset);
	}
    else
	{
	// the block to be released is somewhere in the middle of the used region. 
	// So we have to keep track of it for further released when the RORC's reelase
	// address as advanced sufficiently.
	bool repeat = false;
	do
	    {
	    repeat = false;
	    iter = fBlocks.begin();
	    end = fBlocks.end();
	    if ( edp->fOffset < fEventBufferReleasedOffset )
		{
		LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::ReleaseBlock", "Wrapped block" )
		    << "Skipping for wrapped block part." << ENDLOG;
		if ( iter != end )
		    iter++;
		while ( iter != end )
		    {
		    if ( iter==end || (iter-1)->fOffset>iter->fOffset )
			break;
		    iter++;
		    }
		}
	    bool inserted = false;
	    BlockData bd;
	    // Find the right position (ordered by offset), taking into account wrap arounds
	    // and see if we have an adjacent block to merge with.
	    while ( iter != end )
		{
		if ( edp->fOffset+edp->fSize == iter->fOffset )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::ReleaseBlock", "Merging block" )
			<< "Merging block at beginning: (" << AliHLTLog::kDec
			<< edp->fOffset << "/" << edp->fSize << ") + (" << iter->fOffset
			<< "/" << iter->fSize << ") == (" << edp->fOffset << "/"
			<< iter->fSize+edp->fSize << ")." << ENDLOG;
		    iter->fOffset = edp->fOffset;
		    iter->fSize += edp->fSize;
		    inserted = true;
		    if ( iter!=fBlocks.begin() && (iter-1)->fOffset+(iter-1)->fSize==iter->fOffset )
			{
			LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::ReleaseBlock", "Merging block" )
			    << "Merging in preceeding block at beginning: (" << AliHLTLog::kDec
			    << (iter-1)->fOffset << "/" << (iter-1)->fSize << ") + (" << iter->fOffset
			    << "/" << iter->fSize << ") == (" << (iter-1)->fOffset << "/"
			    << iter->fSize+(iter-1)->fSize << ")." << ENDLOG;
			(iter-1)->fSize += iter->fSize;
			fBlocks.erase( iter );
			}
		    break;
		    }
		if ( iter->fOffset+iter->fSize == edp->fOffset )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::ReleaseBlock", "Merging block" )
			<< "Merging block at end: (" << AliHLTLog::kDec
			<< iter->fOffset << "/" << iter->fSize << ") + (" << edp->fOffset 
			<< "/" << edp->fSize << ") == (" << iter->fOffset << "/"
			<< iter->fSize+edp->fSize << ")." << ENDLOG;
		    iter->fSize += edp->fSize;
		    inserted = true;
		    if ( (iter+1)!=end && (iter+1)->fOffset==iter->fOffset+iter->fSize )
			{
			LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::ReleaseBlock", "Merging block" )
			    << "Merging in succeeding block at end: (" << AliHLTLog::kDec
			    << iter->fOffset << "/" << iter->fSize << ") + (" << (iter+1)->fOffset
			    << "/" << (iter+1)->fSize << ") == (" << iter->fOffset << "/"
			    << iter->fSize+(iter+1)->fSize << ")." << ENDLOG;
			iter->fSize += (iter+1)->fSize;
			fBlocks.erase( iter+1 );
			}
		    break;
		    }
		if ( edp->fOffset < iter->fOffset )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::ReleaseBlock", "Inserting block" )
			<< "Inserting block (" << AliHLTLog::kDec
			<< edp->fOffset << "/" << edp->fSize << ") before (" << iter->fOffset 
			<< "/" << iter->fSize << ")." << ENDLOG;
		    bd.fOffset = edp->fOffset;
		    bd.fSize = edp->fSize;
		    fBlocks.insert( iter, bd );
		    inserted = true;
		    break;
		    }
		iter++;
		}
	    if ( !inserted )
		{
		// Nothing found where to insert or merge, so we append it at the end.
		LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::ReleaseBlock", "Appending block" )
		    << "Appending block: (" << AliHLTLog::kDec
		    << edp->fOffset << "/" << edp->fSize 
		    << ") at end." << ENDLOG;
		bd.fOffset = edp->fOffset;
		bd.fSize = edp->fSize;
		fBlocks.insert( end, bd );
		}
	    if ( edp->fWrappedSize )
		{
		// If we had a wrapped block, we do the same for the wrapped around part 
		// at the beginning of the buffer as well.
		LOG( AliHLTLog::kDebug, "AliHLTRORC2Handler::ReleaseBlock", "Wrapping block" )
		    << "Wrapping block. (" << AliHLTLog::kDec
		    << edp->fOffset << "/" << edp->fSize << ") -> (0/"
		    << edp->fWrappedSize << ")." << ENDLOG;
		repeat = true;
		edp->fOffset = 0;
		edp->fSize = edp->fWrappedSize;
		edp->fWrappedSize = 0;
		}
	    }
	while ( repeat );
	}
    return true;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
