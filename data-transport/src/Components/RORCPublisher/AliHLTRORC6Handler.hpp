#ifndef _ALIHLTRORC6HANDLER_HPP_
#define _ALIHLTRORC6HANDLER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTRORCHandlerInterface.hpp"
#include "MLUCString.hpp"
#include <psi.h>
#include <psi_error.h>
#include <list>


//#define COMPARE_SOFTWARE_EVENTBUFFERREADPTR_CARD_DMA_ADDR

#if 0
#define COMPARE_COUNTER_PATTERN_TAGGED
#endif

#if 1
#define USE_SPECIAL_REPORT_OVERWRITES
#endif


class AliHLTRORC6Handler: public AliHLTRORCHandlerInterface
    {
    public:

	typedef volatile AliUInt8_t AliVolatileUInt8_t;
	typedef volatile AliUInt32_t AliVolatileUInt32_t;
	typedef volatile AliUInt64_t AliVolatileUInt64_t;
	//typedef void* AliHLTVoidPtr;
	//typedef volatile AliHLTVoidPtr AliHLTVolatileVoidPtr;
	typedef volatile void* AliHLTVolatileVoidPtr;
	
	AliHLTRORC6Handler( unsigned long dataBufferSize, unsigned long rorcBlockSize, int eventSlotsExp2=-1 );
	
	virtual ~AliHLTRORC6Handler();

	virtual int Open( bool vendorDeviceIndexOrBusSlotFunction, AliUInt16_t pciVendorIDOrBus, AliUInt16_t pciDeviceIDOrSlot, AliUInt16_t pciDeviceIndexOrFunction, AliUInt16_t pciDeviceBarIndex );
	virtual int Close();

	// SetEventBufferRegion has to be called BEFORE SetEventBuffer for this interface version!!!
	virtual int SetEventBuffer( AliUInt8_t* dataBuffer, unsigned long dataBufferSize, bool first = true );

	virtual int InitializeRORC( bool first = true );
	virtual int DeinitializeRORC();

	virtual int InitializeFromRORC( bool first = true );

	virtual int SetOptions( const vector<MLUCString>& options, char*& errorMsg, unsigned& errorArgNr );
	virtual void GetAllowedOptions( vector<MLUCString>& options );

	virtual int WriteConfigWord( unsigned long wordIndex, AliUInt32_t word, bool doVerify );

	virtual int ActivateRORC();
	virtual int DeactivateRORC();

	virtual int PollForEvent( AliEventID_t& eventID, 
				  AliUInt64_t& dataOffset, AliUInt64_t& dataSize, AliUInt64_t& dataWrappedSize, 
				  bool& dataDefinitivelyCorrupt, bool& eventSuppressed, AliUInt64_t& unsuppressedDataSize );

	virtual int ReleaseEvent( AliEventID_t eventID );

	virtual int FlushEvents();

        virtual int ReadRORCBusyCounter( AliUInt32_t& fifoFullCnt );

	void EnumerateEventIDs( bool enumerate )
		{
		fEnumerateEventIDs = enumerate;
		}

	void SetHeaderSearch( bool search )
		{
		fHeaderSearch = search;
		}

	void SetReportWordConsistencyCheck( bool check )
		{
		fReportWordConsistencyCheck = check;
		}
	
	void SetReportWordUseBlockSize( bool use )
		{
		fReportWordUseBlockSize= use;
		}

	void SetTPCDataFix( bool fix )
		{
		fTPCDataFix = fix;
		}

	void SetNoFlowControl( bool noFlowControl )
		{
		fNoFlowControl = noFlowControl;
		}

#ifdef COMPARE_COUNTER_PATTERN_TAGGED
	void SetCompareHighTag( uint8 tag )
		{
		fCompareHighTag = tag;
		}

	void CompareCounterPattern( bool doCompare )
		{
		fDoCompareCounterPattern = doCompare;
		}
#endif

	void SetRORCBlockSize( unsigned long rorcBlockSize )
		{
		fRORCBlockSize = rorcBlockSize;
		fRORCBlock32bitWords = fRORCBlockSize / 4;
		}

	void SetEventSuppression( unsigned short suppression )
		{
		fEventSuppression_Exp2  = suppression;
		fEventSuppressionActive = true;
		}

	void SetEnableHWCoProc( bool enableHWCoProc )
		{
		fEnableHWCoProc = enableHWCoProc;
		}
	// FCF settings
	void SetSinglePadSuppression( bool singlePadSuppression )
		{
		fSinglePadSuppression = singlePadSuppression;
		fSinglePadSuppressionSet = true;
		}
	void SetBypassMerger( bool bypassMerger )
		{
		fBypassMerger = bypassMerger;
		fBypassMergerSet = true;
		}
	void SetDeconvPad( bool deconvPad )
		{
		fDeconvPad = deconvPad;
		fDeconvPadSet = true;
		}
	void SetClusterLowerLimit( AliUInt16_t clusterLowerLimit )
		{
		fClusterLowerLimit = clusterLowerLimit;
		fClusterLowerLimitSet = true;
		}
	void SetSingleSeqLimit( AliUInt8_t singleSeqLimit )
		{
		fSingleSeqLimit = singleSeqLimit;
		fSingleSeqLimitSet = true;
		}

	void SetMergerDistance( AliUInt8_t mergerDistance )
		{
		fMergerDistance = mergerDistance;
		fMergerDistanceSet = true;
		}

	void SetMergerAlgorithmFollow( AliUInt8_t mergerAlgorithmFollow )
		{
		fMergerAlgorithmFollow = mergerAlgorithmFollow;
		fMergerAlgorithmFollowSet = true;
		}

	void SetChargeTolerance( AliUInt8_t chargeTolerance )
		{
		fChargeTolerance = chargeTolerance;
		fChargeToleranceSet = true;
		}


	void SetNoPeriodCounterLogModulo( unsigned noPeriodCounterLogModulo )
		{
		fNoPeriodCounterLogModulo = noPeriodCounterLogModulo;
		}



	// Has to be called BEFORE SetEventBuffer!!!
	void SetEventBufferRegion( tRegion eventBufferRegion );

	void SignalErrorToRORC();


    protected:

	struct EventData
	    {
		AliEventID_t fEventID;
		AliUInt32_t fOffset;
		AliUInt32_t fSize;
		AliUInt32_t fWrappedSize;
	    };

	bool ReleaseBlock( EventData* edp );

	int Write32Control( const AliUInt32_t index, const AliUInt32_t value, PSI_Status& status )
		{
		return Write32Control( index, value, false, 0, 0, false, status );
		}
	int Write32Control( const AliUInt32_t index, const AliUInt32_t value, const bool verify, const AliUInt32_t verifyMask, bool verifyFailError, PSI_Status& status )
		{
		return Write32Control( index, value, verify, value, verifyMask, verifyFailError, status );
		}

	int Write32Control( const AliUInt32_t index, const AliUInt32_t value, const bool verify, const AliUInt32_t verifyValue, const AliUInt32_t verifyMask, bool verifyFailError, PSI_Status& status );

	int ActivateHWCoProc();
	int DeactivateHWCoProc();

	MLUCVector<EventData> fEvents;
	pthread_mutex_t fEventMutex;

	
	static const AliUInt32_t kDMAENABLE;
	static const AliUInt32_t kDDLCONTROL;
	static const AliUInt32_t kFIFOFLUSH;
	static const AliUInt32_t kEVENTCOUNTERRESET;
	static const AliUInt32_t kENABLEFLOWCONTROL;
	static const AliUInt32_t kFIFOFULLCOUNT;

	static const AliUInt32_t kFIFOFLUSHEMPTY;
	
	static const AliUInt32_t kFCFCONTROL;
        static const AliUInt32_t kFCFCONTROLEXT;
	static const AliUInt32_t kFCFCONFIGMEMADDRESS;
	static const AliUInt32_t kFCFCONFIGMEMDATAWR;
	static const AliUInt32_t kFCFCONFIGMEMDATARD;

	static const AliUInt32_t kFCFCONTROLRESET;
	static const AliUInt32_t kFCFCONTROLENABLE;
	static const AliUInt32_t kFCFCONTROLFLOWCONTROL;
	static const AliUInt32_t kFCFCONTROLMERGERBYPASS;
	static const AliUInt32_t kFCFCONTROLSINGLEPADDSUPPRESSION;
	static const AliUInt32_t kFCFCONTROLDECONVPAD;

	static const AliUInt32_t kFCFCONTROLCLUSTERLOWERLIMITMASK;
	static const AliUInt32_t kFCFCONTROLCLUSTERLOWERLIMITSHIFT;
	static const AliUInt32_t kFCFCONTROLSINGLESEQUENCELIMITMASK;
	static const AliUInt32_t kFCFCONTROLSINGLESEQUENCELIMITSHIFT;

	static const AliUInt32_t kFCFCONTROLMERGERDISTANCEMASK;
	static const AliUInt32_t kFCFCONTROLMERGERDISTANCESHIFT;
	static const AliUInt32_t kFCFCONTROLMERGERALGORITHMFOLLOW;
	static const AliUInt32_t kFCFCONTROLCHARGETOLERANCEMASK;
	static const AliUInt32_t kFCFCONTROLCHARGETOLERANCESHIFT;

	
	static const AliUInt32_t kEVTBUFPTRLOW;
	static const AliUInt32_t kEVTBUFPTRHIGH;
	static const AliUInt32_t kEVTBUFENDLOW;
	static const AliUInt32_t kEVTBUFENDHIGH;
	static const AliUInt32_t kREPBUFPTRLOW;
	static const AliUInt32_t kREPBUFPTRHIGH;
	static const AliUInt32_t kREPBUFENDLOW;
	static const AliUInt32_t kREPBUFENDHIGH;
	static const AliUInt32_t kEVTBUFPTRREADLOW;
	static const AliUInt32_t kEVTBUFPTRREADHIGH;
	static const AliUInt32_t kADMBUFPTRREADLOW;
	static const AliUInt32_t kADMBUFPTRREADHIGH;

	static const AliUInt32_t kREPORTERROR;

	

	AliUInt64_t fEventBufferSize;
	AliUInt64_t fReportBufferSize;
	AliUInt64_t fAdminBufferSize;

	
	MLUCString fBarID;
	MLUCString fReportBufferID;
	MLUCString fAdminBufferID;

	tRegion fBarRegion;
	tRegion fEventBufferRegion;
	tRegion fReportBufferRegion;
	tRegion fAdminBufferRegion;

#ifdef COMPARE_SOFTWARE_EVENTBUFFERREADPTR_CARD_DMA_ADDR
	volatile void* fBarPtr;
#endif

	AliUInt64_t fEventBufferPhysAddress;
	AliUInt64_t fEventBufferReadPtrPhysAddress;
	AliUInt64_t fEventBufferEndPhysAddress;
	AliUInt64_t fEventBufferBusAddress;
	AliUInt64_t fEventBufferReadPtrBusAddress;
	AliUInt64_t fEventBufferEndBusAddress;
	AliUInt64_t fReportBufferPhysAddress;
	AliUInt64_t fReportBufferBusAddress;
	AliUInt64_t fReportBufferEndBusAddress;
	AliUInt64_t fAdminBufferPhysAddress;
	AliUInt64_t fAdminBufferBusAddress;

	AliVolatileUInt8_t* fEventBuffer;
	AliVolatileUInt8_t* fEventBufferEnd;
	AliVolatileUInt8_t* fEventBufferReadPtr;
	AliVolatileUInt8_t* fReportBuffer;
	AliVolatileUInt8_t* fReportBufferEnd;
	AliVolatileUInt8_t* fReportBufferReadPtr;
	AliVolatileUInt8_t* fReportBufferOldReadPtr;
	AliVolatileUInt8_t* fAdminBuffer;

	AliUInt64_t fEventBufferReleasedOffset;

	bool fEventBufferSet;
	bool fEventBufferRegionSet;

	struct BlockData
	    {
		AliUInt64_t fOffset;
		AliUInt64_t fSize;
	    };

#if 1
	typedef list<BlockData> TBlockData;
	typedef TBlockData::iterator TBlockIterator;
#else
	typedef vector<BlockData> TBlockData;
	typedef TBlockData::iterator TBlockIterator;
#endif
	TBlockData fBlocks;
	TBlockData fWrapBlocks;
	// fBlocks and fWrapBlocks are protected by fEventMutex as well.
	bool fWrap;
	
	static bool EventDataSearchFunc( const EventData& ed, const AliEventID_t& searchData )
		{
		return ed.fEventID == searchData;
		}


	bool fEnumerateEventIDs;
	AliEventID_t fNextEventID;

	AliEventID_t fLastRORCEventID;
	AliEventID_t fLastWrapRORCEventID;
	AliEventID_t fAddEventIDCounter;
	struct timeval fLastEventIDWrapAround;

	bool fHeaderSearch;

	bool fReportWordConsistencyCheck;
	bool fReportWordUseBlockSize;

#ifdef COMPARE_COUNTER_PATTERN_TAGGED
	uint8 fCompareHighTag;
	bool fDoCompareCounterPattern;
#endif

	bool fTPCDataFix;

	bool fNoFlowControl;

	AliEventID_t fNextInvalidEventID;

	unsigned long fRORCBlockSize;
	unsigned long fRORCBlock32bitWords;

	unsigned long long fEventCounter;

	unsigned long fFreeBufferSize;


	virtual int GetTotalBufferSize()
		{
		return fEventBufferSize;
		}
	virtual int GetFreeBufferSize()
		{
		return fFreeBufferSize;
		}


	unsigned short fEventSuppression_Exp2;
	bool fEventSuppressionActive;

	bool fEnableHWCoProc;
	// FastCllusterFinder settings
	bool fSinglePadSuppression;
	bool fSinglePadSuppressionSet;
	bool fBypassMerger;
	bool fBypassMergerSet;
	bool fDeconvPad;
	bool fDeconvPadSet;
	AliUInt16_t fClusterLowerLimit;
	bool fClusterLowerLimitSet;
	AliUInt8_t fSingleSeqLimit;
	bool fSingleSeqLimitSet;
        AliUInt8_t fMergerDistance;
        bool fMergerDistanceSet;
        bool fMergerAlgorithmFollow;
        bool fMergerAlgorithmFollowSet;
        AliUInt8_t fChargeTolerance;
        bool fChargeToleranceSet;

	unsigned fNoPeriodCounterLogModulo;
	unsigned long fPeriodCounterLogCounter;
	AliEventID_t fLastLogMessageEventIDCounter;

	void InsertBlock( TBlockData& blocks, BlockData& block );


    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTRORC6HANDLER_HPP_
