#ifndef _ALIHLTRORC2HANDLER_HPP_
#define _ALIHLTRORC2HANDLER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTRORCHandlerInterface.hpp"
#include "MLUCString.hpp"
#include <psi.h>
#include <psi_error.h>


//#define COMPARE_SOFTWARE_EVENTBUFFERREADPTR_CARD_DMA_ADDR

#if 0
#define COMPARE_COUNTER_PATTERN_TAGGED
#endif

class AliHLTRORC2Handler: public AliHLTRORCHandlerInterface
    {
    public:

	typedef volatile AliUInt8_t AliVolatileUInt8_t;
	typedef volatile AliUInt32_t AliVolatileUInt32_t;
	//typedef void* AliHLTVoidPtr;
	//typedef volatile AliHLTVoidPtr AliHLTVolatileVoidPtr;
	typedef volatile void* AliHLTVolatileVoidPtr;

	AliHLTRORC2Handler( unsigned long dataBufferSize, int eventSlotsExp2=-1 );
	
	virtual ~AliHLTRORC2Handler();

	virtual int Open( bool vendorDeviceIndexOrBusSlotFunction, AliUInt16_t pciVendorIDOrBus, AliUInt16_t pciDeviceIDOrSlot, AliUInt16_t pciDeviceIndexOrFunction, AliUInt16_t pciDeviceBarIndex );
	virtual int Close();

	virtual int SetEventBuffer( AliUInt8_t* dataBuffer, unsigned long dataBufferSize, bool first = true );

	virtual int InitializeRORC( bool first = true );
	virtual int DeinitializeRORC();

	virtual int InitializeFromRORC( bool first = true );

	virtual int SetOptions( const vector<MLUCString>& options, char*& errorMsg, unsigned& errorArgNr );
	virtual void GetAllowedOptions( vector<MLUCString>& options );

	virtual int WriteConfigWord( unsigned long /*wordIndex*/, AliUInt32_t /*word*/, bool /*doVerify*/ )
		{
		return 0;
		}

	virtual int ActivateRORC();
	virtual int DeactivateRORC();

	virtual int PollForEvent( AliEventID_t& eventID, 
				  AliUInt64_t& dataOffset, AliUInt64_t& dataSize, AliUInt64_t& dataWrappedSize, 
				  bool& dataDefinitivelyCorrupt, bool& eventSuppressed, AliUInt64_t& unsuppressedDataSize );

	virtual int ReleaseEvent( AliEventID_t eventID );

	virtual int FlushEvents()
		{
		return 0;
		}

	void EnumerateEventIDs( bool enumerate )
		{
		fEnumerateEventIDs = enumerate;
		}

	void SetHeaderSearch( bool search )
		{
		fHeaderSearch = search;
		}

	void SetReportWordConsistencyCheck( bool check )
		{
		fReportWordConsistencyCheck = check;
		}
	
	void SetReportWordUseBlockSize( bool use )
		{
		fReportWordUseBlockSize= use;
		}


#ifdef COMPARE_COUNTER_PATTERN_TAGGED
	void SetCompareHighTag( uint8 tag )
		{
		fCompareHighTag = tag;
		}

	void CompareCounterPattern( bool doCompare )
		{
		fDoCompareCounterPattern = doCompare;
		}
#endif

    protected:

	struct EventData
	    {
		AliEventID_t fEventID;
		AliUInt32_t fOffset;
		AliUInt32_t fSize;
		AliUInt32_t fWrappedSize;
	    };

	bool ReleaseBlock( EventData* edp );

	MLUCVector<EventData> fEvents;
	pthread_mutex_t fEventMutex;

	static const AliUInt32_t kRDYRX;   //  0x14
	static const AliUInt32_t kRESET;   //  0x1C
	static const AliUInt32_t kFIFORST; //  0x38
	static const AliUInt32_t kEOBTR;   //  0x34
	static const AliUInt32_t kEVTBUFPTR;
	static const AliUInt32_t kEVTBUFSIZ;
	static const AliUInt32_t kREPBUFPTR;
	static const AliUInt32_t kREPBUFSIZ;
	static const AliUInt32_t kEVTBUFPTRREAD;
	static const AliUInt32_t kADMBUFPTRREAD;
	static const AliUInt32_t kPRGPG1;
	static const AliUInt32_t kPRGPG2;
	static const AliUInt32_t kDMAPGENABLE;
	static const AliUInt32_t kLINKREG;

	static const AliUInt32_t kENABLELINK;
	static const AliUInt32_t kDISABLELINK;

	AliUInt32_t fEventBufferSize;
	AliUInt32_t fReportBufferSize;
	AliUInt32_t fAdminBufferSize;

	
	MLUCString fBar0ID;
	MLUCString fReportBufferID;
	MLUCString fAdminBufferID;

	tRegion fBar0Region;
	tRegion fReportBufferRegion;
	tRegion fAdminBufferRegion;

#ifdef COMPARE_SOFTWARE_EVENTBUFFERREADPTR_CARD_DMA_ADDR
	volatile void* fBar0Ptr;
#endif

	unsigned long fEventBufferPhysAddress;
	unsigned long fEventBufferReadPtrPhysAddress;
	unsigned long fEventBufferEndPhysAddress;
	unsigned long fEventBufferBusAddress;
	unsigned long fEventBufferReadPtrBusAddress;
	unsigned long fEventBufferEndBusAddress;
	unsigned long fReportBufferPhysAddress;
	unsigned long fReportBufferBusAddress;
	unsigned long fAdminBufferPhysAddress;
	unsigned long fAdminBufferBusAddress;

	AliVolatileUInt8_t* fEventBuffer;
	AliVolatileUInt8_t* fEventBufferEnd;
	AliVolatileUInt8_t* fEventBufferReadPtr;
	AliVolatileUInt8_t* fReportBuffer;
	AliVolatileUInt8_t* fReportBufferEnd;
	AliVolatileUInt8_t* fReportBufferReadPtr;
	AliVolatileUInt8_t* fReportBufferOldReadPtr;
	AliVolatileUInt8_t* fAdminBuffer;

	unsigned long fEventBufferReleasedOffset;

	struct BlockData
	    {
		AliUInt32_t fOffset;
		AliUInt32_t fSize;
	    };
	vector<BlockData> fBlocks;
	// fBlocks is protected by fEventMutex as well.
	bool fWrap;
	
	static bool EventDataSearchFunc( const EventData& ed, const AliEventID_t& searchData )
		{
		return ed.fEventID == searchData;
		}


	bool fEnumerateEventIDs;
	AliEventID_t fNextEventID;

	AliEventID_t fLastRORCEventID;
	AliEventID_t fAddEventIDCounter;

	bool fHeaderSearch;

	bool fReportWordConsistencyCheck;
	bool fReportWordUseBlockSize;

#ifdef COMPARE_COUNTER_PATTERN_TAGGED
	uint8 fCompareHighTag;
	bool fDoCompareCounterPattern;
#endif

    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTRORC2HANDLER_HPP_
