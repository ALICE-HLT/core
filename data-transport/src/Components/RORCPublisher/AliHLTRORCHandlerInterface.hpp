#ifndef _ALIHLTRORCHANDLERINTERFACE_HPP_
#define _ALIHLTRORCHANDLERINTERFACE_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliEventID.h"
#include "AliHLTShmID.h"
#include "MLUCVector.hpp"
#include "MLUCString.hpp"
#include <vector>
#include <sys/time.h>
#include <cerrno>

class AliHLTRORCHandlerInterface
    {
    public:

	virtual ~AliHLTRORCHandlerInterface() {};

	virtual int Open( bool vendorDeviceIndexOrBusSlotFunction, AliUInt16_t pciVendorIDOrBus, AliUInt16_t pciDeviceIDOrSlot, AliUInt16_t pciDeviceIndexOrFunction, AliUInt16_t pciDeviceBarIndex ) = 0;
	virtual int Close() = 0;

	virtual int SetEventBuffer( AliUInt8_t* dataBuffer, unsigned long dataBufferSize, bool first = true ) = 0;

	virtual int InitializeRORC( bool first = true  ) = 0;
	virtual int DeinitializeRORC() = 0;

	virtual int InitializeFromRORC( bool first = true ) = 0;

	virtual int SetOptions( const vector<MLUCString>& options, char*& errorMsg, unsigned& errorArgNr ) = 0;
	virtual void GetAllowedOptions( vector<MLUCString>& options ) = 0;

	virtual int WriteConfigWord( unsigned long wordIndex, AliUInt32_t word, bool doVerify ) = 0;

	virtual int ActivateRORC() = 0;
	virtual int DeactivateRORC() = 0;

	virtual int PollForEvent( AliEventID_t& eventID,
				  AliUInt64_t& dataOffset, AliUInt64_t& dataSize, AliUInt64_t& dataWrappedSize, 
				  bool& dataDefinitivelyCorrupt, bool& eventSuppressed, AliUInt64_t& unsuppressedDataSize ) = 0;

	virtual int ReleaseEvent( AliEventID_t eventID ) = 0;

	virtual int FlushEvents() = 0;

        virtual int ReadRORCBusyCounter( AliUInt32_t& fifoFullCnt )
                {
		fifoFullCnt = ~(AliUInt32_t)0;
		return ENOSYS;
		}

	virtual int GetTotalBufferSize()
		{
		return 0;
		}
	virtual int GetFreeBufferSize()
		{
		return 0;
		}

    protected:
    private:
    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTRORCHANDLERINTERFACE_HPP_
