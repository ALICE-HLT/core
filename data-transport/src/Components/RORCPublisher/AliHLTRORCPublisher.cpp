/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTRORCPublisher.hpp"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTSubEventDescriptor.hpp"
#include "AliHLTEventDataType.h"
#include "AliHLTLog.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTBufferManager.hpp"
#include "AliHLTShmManager.hpp"
#include "AliHLTStackTrace.hpp"
#include "AliHLTDDLHeader.hpp"
#include "AliHLTDDLHeaderData.h"
#include "AliHLTEventWriter.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>


#ifdef PROFILING
#define STDHISTO(histo) histo(0,10000,1)
#define ADDHISTO(histos,name) histos.insert( histos.end(), &name )
#else
#define STDHISTO(histo)
#define ADDHISTO(histos,name)
#endif


#define RORCCORRUPT_DATAID             (((AliUInt64_t)'CRPT')<<32 | 'DATA')



AliHLTRORCPublisher::AliHLTRORCPublisher( const char* name, int eventSlotsPowerOfTwo ):
    AliHLTDetectorPublisher( name, eventSlotsPowerOfTwo )
#ifndef OLD_EVENTTRIGGER_DATA
,
    fHLTEventTriggerDataCache( eventSlotsPowerOfTwo ),
    fHLTEventTriggerData( 8, eventSlotsPowerOfTwo )
#endif
#ifdef PROFILING
    ,
    STDHISTO( fAnnounceEventTimeSetupHisto ),
    STDHISTO( fAnnounceEventTimeFindSubscribersHisto ),
    STDHISTO( fAnnounceEventTimeStoreEventHisto ),
    STDHISTO( fAnnounceEventTimeFindEventSlotHisto ),
    STDHISTO( fAnnounceEventTimeAnnounceEventHisto ),
    STDHISTO( fAnnounceEventTimeCallbacksHisto ),
    STDHISTO( fAnnounceEventTimeSetTimerHisto ),
    STDHISTO( fAnnounceEventTimeFindTimerHisto )
#endif
    {
    fRORCHandler = NULL;
    fETP = &fETS;
    fETP->fHeader.fLength = sizeof(AliHLTEventTriggerStruct);
    fETP->fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
    fETP->fHeader.fSubType.fID = 0;
    fETP->fHeader.fVersion = 1;
    fETP->fDataWordCount = 0;

    fDoSleep = false;
    fSleepTime = 10000;

    fQuitEventLoop = false;
    fEventLoopQuitted = true;
    
    fShmPtr = NULL;
    fShmSize = 0;

    fOldestEventBirth = 0;

    fEventDataType.fID = UNKNOWN_DATAID;
    fEventDataOrigin.fID = UNKNOWN_DATAORIGIN;
    fEventDataSpecification = ~(AliUInt32_t)0;
    pthread_mutex_init( &fRORCHandlerMutex, NULL );

#ifndef OLD_EVENTTRIGGER_DATA
    pthread_mutex_init( &fHLTEventTriggerDataLock, NULL );
    fDisableHLTEventTriggerData = false;
#endif

    gettimeofday( &fLastNoEventFoundLog, NULL );

    fWrapCopyAreaShmKey.fShmType = kInvalidShmType;
    fWrapCopyAreaShmKey.fKey.fID = ~(AliUInt64_t)0;;
    fWrapCopyAreaStartOffset = 0;
    fWrapCopyAreaStartPtr = NULL;
    fWrapCopyAreaSize = 0;

#ifdef DEBUG
    fEventCount = 0;
#endif

    fPublishCorruptEvents = false;
    fSuppressHWSuppressedEvents = false;
    fSuppressGoodEvents = false;

    fReceivedEvents = false;

    fStartOfRunEventReceived = false;
    fEndOfRunEventReceived = false;
    fDataEventWithoutStartOfRunWarning = false;

    fEnableCorruptEventsAutoDump = true;
    fCorruptEventsAutoDumpLimit = 100;
    fCorruptEventsAutoDumpPrefix = ".";
    fCorruptEventsAutoDumpCnt = 0;

    fPreviousRORCBusyCounter = 0;
    fRORCBusyCounterLogCnt = 0;
    fRORCBusyCounterLastLogCnt = 0;
    fNextRORCBusyCounterLogCntBaseLimit = 100;
    fNextRORCBusyCounterLogCntLimitFactor = 1;

    fNoStrictSOR = false;

    ADDHISTO( fAnnounceEventTimeSetupHistos, fAnnounceEventTimeSetupHisto );
    ADDHISTO( fAnnounceEventTimeFindSubscribersHistos, fAnnounceEventTimeFindSubscribersHisto );
    ADDHISTO( fAnnounceEventTimeStoreEventHistos, fAnnounceEventTimeStoreEventHisto );
    ADDHISTO( fAnnounceEventTimeFindEventSlotHistos, fAnnounceEventTimeFindEventSlotHisto );
    ADDHISTO( fAnnounceEventTimeAnnounceEventHistos, fAnnounceEventTimeAnnounceEventHisto );
    ADDHISTO( fAnnounceEventTimeCallbacksHistos, fAnnounceEventTimeCallbacksHisto );
    ADDHISTO( fAnnounceEventTimeSetTimerHistos, fAnnounceEventTimeSetTimerHisto );
    ADDHISTO( fAnnounceEventTimeFindTimerHistos, fAnnounceEventTimeFindTimerHisto );
    }

AliHLTRORCPublisher::~AliHLTRORCPublisher()
    {
    pthread_mutex_destroy( &fRORCHandlerMutex );
#ifndef OLD_EVENTTRIGGER_DATA
    pthread_mutex_destroy( &fHLTEventTriggerDataLock );
#endif
    }

void AliHLTRORCPublisher::StopPublishing()
    {
    //fRORCHandler->DeactivateRORC();
    fFlushCommandLastAction = false;
    gettimeofday( &fStopRunTimeoutStart, NULL );
    QuitEventLoop();
    fEventAnnouncer.Quit();
    struct timespec ts;
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 50000000;
    gettimeofday( &start, NULL );
    while ( !QuittedEventLoop() && deltaT<timeLimit )
	{
	ts.tv_sec = 0;
	ts.tv_nsec = 50000;
	nanosleep( &ts, NULL );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    
    }

bool AliHLTRORCPublisher::MustContinue()
    {
#if 0
    if ( !fQuitEventLoop )
	return true;
#else
    if ( !fQuitEventLoop )
	return false; // False if loop has not to be ended, there is no need to continue with publishing (no urgent end-of-event which has to be honored...)
#endif
    struct timeval now;
    gettimeofday( &now, NULL );
    unsigned long tdiff = (now.tv_sec-fStopRunTimeoutStart.tv_sec)*1000000+(now.tv_usec-fStopRunTimeoutStart.tv_usec);
    if ( tdiff > 200000 && !fFlushCommandLastAction )
	{
#if 0
	if ( fFlushCommandLastAction )
	    return false;
#endif
	gettimeofday( &fStopRunTimeoutStart, NULL );
	fFlushCommandLastAction = true;
	if ( fRORCHandler )
	    fRORCHandler->FlushEvents();
	}
    if ( (tdiff>=1000000 && fFlushCommandLastAction) || fEndOfRunEventReceived )
	return false;
    return true;
    }


int AliHLTRORCPublisher::WaitForEvent( AliEventID_t& eventID, AliHLTSubEventDataDescriptor*& sbevent,
					   AliHLTEventTriggerStruct*& trg )
    {
    Tick( fReceivedEvents );
    sbevent = NULL;
    trg = NULL;
    AliUInt64_t dataOffset, dataSize, dataWrappedSize;
    bool dataCorrupt = false;
    bool eventSuppressed = false;
    AliUInt64_t unsuppressedDataSize;
    int ret;
// 	printf( "%s:%d\n", __FILE__, __LINE__ );
    if ( !fQuitEventLoop || MustContinue() )
	{
	do
	    {
	    pthread_mutex_lock( &fRORCHandlerMutex );
	    dataCorrupt = false;
	    ret = fRORCHandler->PollForEvent( eventID, 
					      dataOffset, dataSize, dataWrappedSize,
					      dataCorrupt,
					      eventSuppressed, unsuppressedDataSize );
	    pthread_mutex_unlock( &fRORCHandlerMutex );
	    if ( ret==EFAULT )
	        {
		LOG( AliHLTLog::kFatal, "AliHLTRORCPublisher::WaitForEvent", "Illegal address reading event" )
		  << "Illegal address encountered retrieving event from RORC event buffer. Giving up..."
		  << ENDLOG;
		if ( fStatus )
  		    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		return 0;
		}
	    if ( !ret && eventSuppressed )
		{
		// Event suppressed already in hardware
		LOG( AliHLTLog::kDebug, "AliHLTRORCPublisher::WaitForEvent", "Suppressed event found" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ") suppred in hardware- Original event data Size: " 
		    << AliHLTLog::kDec << unsuppressedDataSize << " (0x"
		    << AliHLTLog::kHex << unsuppressedDataSize << ")." << ENDLOG;
		// Does not need to be released.
		if ( fStatus )
		    fStatus->fHLTStatus.fTotalProcessedOutputDataSize += unsuppressedDataSize;
		}
	    if ( !ret && dataCorrupt && fEnableCorruptEventsAutoDump && fCorruptEventsAutoDumpCnt<fCorruptEventsAutoDumpLimit )
		{
		LOG( AliHLTLog::kWarning, "AliHLTRORCPublisher::WaitForEvent", "Dumping corrupt Event" )
		    << "Dumping corrupt Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ") to disk or debugging." << ENDLOG;
		unsigned long cnt=1;
		if ( dataWrappedSize )
		    ++cnt;

		pthread_mutex_lock( &fSEDDMutex );
		sbevent = fDescriptors->GetFreeEventDescriptor( eventID, cnt );
		pthread_mutex_unlock( &fSEDDMutex );

		struct timeval now;
		gettimeofday( &now, NULL );
		sbevent->fEventID = eventID;
		sbevent->fEventBirth_s = now.tv_sec;
		sbevent->fEventBirth_us = now.tv_usec;
		sbevent->fOldestEventBirth_s = fOldestEventBirth;
		sbevent->fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_DATA_CORRUPT;

		sbevent->fDataBlocks[0].fBlockOffset = dataOffset;
		sbevent->fDataBlocks[0].fBlockSize = dataSize-dataWrappedSize;
		sbevent->fDataBlocks[0].fShmID.fShmType = fShmKey.fShmType;
		sbevent->fDataBlocks[0].fShmID.fKey.fID = fShmKey.fKey.fID;
		if ( !dataWrappedSize )
		    sbevent->fDataBlocks[0].fDataType.fID = fEventDataType.fID;
		else
		    sbevent->fDataBlocks[0].fDataType.fID = fSplitEventDataType.fID;
		sbevent->fDataBlocks[0].fDataOrigin.fID = fEventDataOrigin.fID;
		sbevent->fDataBlocks[0].fDataSpecification = fEventDataSpecification;
		sbevent->fDataBlocks[0].fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_DATA_CORRUPT;
		if ( dataWrappedSize )
		    sbevent->fDataBlocks[0].fStatusFlags |= ALIHLTSUBEVENTDATADESCRIPTOR_SPLITPART1;
		sbevent->fDataBlocks[0].fAttributes[kAliHLTSEDBDByteOrderAttributeIndex] = kAliHLTLittleEndianByteOrder;
		for ( int j = kAliHLTSEDBDAlignmentAttributeIndexStart; j <= kAliHLTSEDBDLastAlignmentAttributeIndex; j++ )
		    sbevent->fDataBlocks[0].fAttributes[j] = kAliHLTUnknownAttribute;
		sbevent->fDataBlocks[0].fProducerNode = fNodeID;

		if ( dataWrappedSize )
		    {
		    sbevent->fDataBlocks[1].fBlockOffset = 0;
		    sbevent->fDataBlocks[1].fBlockSize = dataWrappedSize;
		    sbevent->fDataBlocks[1].fShmID.fShmType = fShmKey.fShmType;
		    sbevent->fDataBlocks[1].fShmID.fKey.fID = fShmKey.fKey.fID;
		    sbevent->fDataBlocks[1].fDataType.fID = fSplitEventDataType.fID;
		    sbevent->fDataBlocks[1].fDataOrigin.fID = fEventDataOrigin.fID;
		    sbevent->fDataBlocks[1].fDataSpecification = fEventDataSpecification;
		    sbevent->fDataBlocks[1].fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_DATA_CORRUPT;
		    sbevent->fDataBlocks[1].fStatusFlags |= ALIHLTSUBEVENTDATADESCRIPTOR_SPLITPART2;
		    sbevent->fDataBlocks[1].fAttributes[kAliHLTSEDBDByteOrderAttributeIndex] = kAliHLTLittleEndianByteOrder;
		    for ( int j = kAliHLTSEDBDAlignmentAttributeIndexStart; j <= kAliHLTSEDBDLastAlignmentAttributeIndex; j++ )
			sbevent->fDataBlocks[1].fAttributes[j] = kAliHLTUnknownAttribute;
		    sbevent->fDataBlocks[1].fProducerNode = fNodeID;
		    }

		AliHLTSubEventDescriptor sed( sbevent );
		std::vector<AliHLTSubEventDescriptor::BlockData> blocks;
		sed.Dereference( fShmManager, blocks );
		

		if ( !AliHLTEventWriter::WriteEvent( eventID, blocks, sbevent, fETP, fRunNumber, fCorruptEventsAutoDumpPrefix.c_str() ) )
		    {
		    LOG( AliHLTLog::kError, "AliHLTRORCPublisher::WaitForEvent", "Cannot dump corrupt event" )
			<< "Cannot dump corrupt event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ")." << ENDLOG;
		    }
		++fCorruptEventsAutoDumpCnt;


		pthread_mutex_lock( &fSEDDMutex );
		fDescriptors->ReleaseEventDescriptor( eventID );
		pthread_mutex_unlock( &fSEDDMutex );
		

		}
	    if ( !ret && ( (!dataCorrupt && !fSuppressGoodEvents) || (dataCorrupt && fPublishCorruptEvents) || (eventSuppressed && !fSuppressHWSuppressedEvents) ) )
		{
		LOG( AliHLTLog::kInformational, "AliHLTRORCPublisher::WaitForEvent", "Event found" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ") found: Data Offset " << dataOffset << " (0x" << AliHLTLog::kHex
		    << dataOffset << ") - Data Size: " << AliHLTLog::kDec << dataSize << " (0x"
		    << AliHLTLog::kHex << dataSize << ") - Wrapped Data Size: " << AliHLTLog::kDec
		    << dataWrappedSize << "(0x" << AliHLTLog::kHex << dataWrappedSize 
		    << ")." << ENDLOG;
		fReceivedEvents = true;
		if ( eventSuppressed )
		    {
		    AliHLTDDLHeader ddlHeader;
		    if ( !ddlHeader.Set( (AliUInt32_t*)(fShmPtr+dataOffset) ) )
			{
			LOG( AliHLTLog::kError, "AliHLTRORCPublisher::WaitForEvent", "Error reading DDL header" )
			    << "Error reading DDL header number. Unknown header version or out of memory. Header version: " 
			    << AliHLTLog::kDec 
			    << (unsigned)AliHLTGetDDLHeaderVersion(fShmPtr+dataOffset) << " - header version byte index: "
			    << (unsigned)DDLHEADERVERSIONINDEX << " - header size: " << AliHLTGetDDLHeaderSize(fShmPtr+dataOffset)
			    << ENDLOG;
			}
		    else
			{
			// Set total block length in header to only header size for suppressed events (which are reduced to the header)
			ddlHeader.SetBlockLength( ddlHeader.GetHeaderSize() );
			}
		    }
		//fRORCHandler->ReleaseEvent( eventID );
		}
	    else if ( !ret && ( (dataCorrupt && !fPublishCorruptEvents) || (!dataCorrupt && fSuppressGoodEvents) || (eventSuppressed && fSuppressHWSuppressedEvents) ) )
		{
		if ( eventSuppressed )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTRORCPublisher::WaitForEvent", "Event supressed in RORC" )
			<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ") already suppressed in RORC: Data Offset " << dataOffset << " (0x" << AliHLTLog::kHex
			<< dataOffset << ") - Data Size: " << AliHLTLog::kDec << dataSize << " (0x"
			<< AliHLTLog::kHex << dataSize << ") - Wrapped Data Size: " << AliHLTLog::kDec
			<< dataWrappedSize << "(0x" << AliHLTLog::kHex << dataWrappedSize
			<< "). - Will not be published" << ENDLOG;
		    }
		else if ( dataCorrupt )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTRORCPublisher::WaitForEvent", "Corrupt Event found" )
			<< "Corrupt Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ") found: Data Offset " << dataOffset << " (0x" << AliHLTLog::kHex
			<< dataOffset << ") - Data Size: " << AliHLTLog::kDec << dataSize << " (0x"
			<< AliHLTLog::kHex << dataSize << ") - Wrapped Data Size: " << AliHLTLog::kDec
			<< dataWrappedSize << "(0x" << AliHLTLog::kHex << dataWrappedSize
			<< "). - Will not be published" << ENDLOG;
		    }
		else
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTRORCPublisher::WaitForEvent", "Suppressing good event" )
			<< "Good Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ") suppressed: Data Offset " << dataOffset << " (0x" << AliHLTLog::kHex
			<< dataOffset << ") - Data Size: " << AliHLTLog::kDec << dataSize << " (0x"
			<< AliHLTLog::kHex << dataSize << ") - Wrapped Data Size: " << AliHLTLog::kDec
			<< dataWrappedSize << "(0x" << AliHLTLog::kHex << dataWrappedSize
			<< "). - Will not be published" << ENDLOG;
		    }
		fRORCHandler->ReleaseEvent( eventID );
		}
	    else if ( ret==EAGAIN )
		{
		struct timeval now;
		gettimeofday( &now, NULL );
		unsigned long long dt;
		dt = now.tv_sec - fLastNoEventFoundLog.tv_sec;
		dt *= 1000000;
		dt += now.tv_usec - fLastNoEventFoundLog.tv_usec;
		if ( dt >= 1000000 )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTRORCPublisher::WaitForEvent", "No event found" )
			<< "No event found." << ENDLOG;
		    fLastNoEventFoundLog = now;
		    }
		}
	    else // if ( ret && ret!=EAGAIN )
		{
		LOG( AliHLTLog::kError, "AliHLTRORCPublisher::WaitForEvent", "Error reading event from RORC" )
		    << "Error reading event from RORC: " << strerror(ret) << " ("
		    << AliHLTLog::kDec << ret << ")." << ENDLOG;
		return 0;
		}
	    if ( fDoSleep && ( ret==EAGAIN ) )
		usleep( fSleepTime );
	    if ( !ret && eventID.fType==kAliEventTypeUnknown && dataOffset==0 && dataWrappedSize==0 && dataCorrupt )
		ret = EAGAIN;
	    UpdateEventAge();
	    }
	while ( !fQuitEventLoop && ( ret==EAGAIN || (dataCorrupt && !fPublishCorruptEvents) || (!dataCorrupt && fSuppressGoodEvents) ) );
	if ( fStatus )
	    fStatus->fHLTStatus.fFreeOutputBuffer = fRORCHandler->GetFreeBufferSize();
// 	printf( "%s:%d\n", __FILE__, __LINE__ );
	if ( ret )
	    return 0;
// 	printf( "%s:%d\n", __FILE__, __LINE__ );
	if ( fQuitEventLoop )
	    return 1;
// 	printf( "%s:%d\n", __FILE__, __LINE__ );
	
#ifdef DEBUG
	fEventCount++;
	if ( fEventCount>=4 )
	    {
	    fRORCHandler->DeactivateRORC();
	    }
#endif
	if ( fQuitEventLoop )
	    {
	    gettimeofday( &fStopRunTimeoutStart, NULL );
	    fFlushCommandLastAction = false;
	    }

	bool wrapAreaUsed=false;
	if ( dataWrappedSize )
	    {
	    if ( fWrapCopyAreaSize>=dataSize )
		{
		memcpy( fWrapCopyAreaStartPtr, (const AliUInt8_t*)( fShmPtr+dataOffset ), dataSize-dataWrappedSize );
		memcpy( fWrapCopyAreaStartPtr+dataSize-dataWrappedSize, (const AliUInt8_t*)fShmPtr, dataWrappedSize );
		dataOffset = fWrapCopyAreaStartOffset;
		dataWrappedSize = 0;
		wrapAreaUsed=true;
		}
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTRORCPublisher::WaitForEvent", "Cannot copy wrapped event data" )
		    << "Cannot copy wrapped event data for linearization. Wrap buffer is too small (" << AliHLTLog::kDec
		    << fWrapCopyAreaSize << " (0x" << AliHLTLog::kHex << fWrapCopyAreaSize << ") bytes - "
		    << AliHLTLog::kDec << dataSize << " (0x" << AliHLTLog::kHex << dataSize << ") bytes needed)." 
		    << ENDLOG;
		}
	    }

	int cnt = 1;
	if ( dataWrappedSize )
	    cnt = 2;
	vector<AliHLTSubEventDataBlockDescriptor> addBlocks;
	if ( eventID.fType==kAliEventTypeStartOfRun )
	    {
	    LOG( AliHLTLog::kImportant, "AliHLTRORCPublisher::WaitForEvent", "Start-Of-Data/-Run event received" )
		<< "Start-Of-Data/-Run event 0x" << AliHLTLog::kHex << eventID << " ("
		<< AliHLTLog::kDec << eventID << ") received." << ENDLOG;
	    fStartOfRunEventReceived = true;
	    CreateSORBlocks( addBlocks );
	    }
	else if ( eventID.fType==kAliEventTypeEndOfRun )
	    {
	    LOG( AliHLTLog::kImportant, "AliHLTRORCPublisher::WaitForEvent", "End-Of-Data/-Run event received" )
		<< "End-Of-Data/-Run event 0x" << AliHLTLog::kHex << eventID << " ("
		<< AliHLTLog::kDec << eventID << ") received." << ENDLOG;
	    fEndOfRunEventReceived = true;
	    CreateEORBlocks( addBlocks );
	    }
	else if ( !fStartOfRunEventReceived && !fDataEventWithoutStartOfRunWarning && fAliceHLT )
	    {
	    LOG( fNoStrictSOR ? AliHLTLog::kInformational : AliHLTLog::kWarning, "AliHLTRORCPublisher::WaitForEvent", "Data event without Start-Of-Data/-Run event" )
		<< "First data event 0x" << AliHLTLog::kHex << eventID << " ("
		<< AliHLTLog::kDec << eventID << ") received without preceeding Start-Of-Data/-Run event." << ENDLOG;
	    fDataEventWithoutStartOfRunWarning = true;
	    }

	pthread_mutex_lock( &fSEDDMutex );
	sbevent = fDescriptors->GetFreeEventDescriptor( eventID, cnt+GetAdditionalBlockCount()+addBlocks.size() );
	pthread_mutex_unlock( &fSEDDMutex );

// 	printf( "%s:%d\n", __FILE__, __LINE__ );

	struct timeval now;
	gettimeofday( &now, NULL );
	sbevent->fEventID = eventID;
	sbevent->fEventBirth_s = now.tv_sec;
	sbevent->fEventBirth_us = now.tv_usec;
	sbevent->fOldestEventBirth_s = fOldestEventBirth;
	if ( !dataCorrupt )
	    sbevent->fStatusFlags = 0;
	else
	    sbevent->fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_DATA_CORRUPT;
	if ( eventID.fType==kAliEventTypeStartOfRun || eventID.fType==kAliEventTypeEndOfRun )
	    sbevent->fStatusFlags |= ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST;
	if ( fStatus && !eventSuppressed )
	    fStatus->fHLTStatus.fTotalProcessedOutputDataSize += dataSize;
	unsigned long n = 0;
	if ( dataSize )
	    {
	    sbevent->fDataBlocks[n].fBlockOffset = dataOffset;
	    sbevent->fDataBlocks[n].fBlockSize = dataSize-dataWrappedSize;
	    if ( wrapAreaUsed )
		{
		sbevent->fDataBlocks[n].fShmID.fShmType = fWrapCopyAreaShmKey.fShmType;
		sbevent->fDataBlocks[n].fShmID.fKey.fID = fWrapCopyAreaShmKey.fKey.fID;
		}
	    else
		{
		sbevent->fDataBlocks[n].fShmID.fShmType = fShmKey.fShmType;
		sbevent->fDataBlocks[n].fShmID.fKey.fID = fShmKey.fKey.fID;
		}
	    if ( !dataWrappedSize )
		sbevent->fDataBlocks[n].fDataType.fID = fEventDataType.fID;
	    else
		sbevent->fDataBlocks[n].fDataType.fID = fSplitEventDataType.fID;
	    sbevent->fDataBlocks[n].fDataOrigin.fID = fEventDataOrigin.fID;
	    sbevent->fDataBlocks[n].fDataSpecification = fEventDataSpecification;
	    if ( !dataCorrupt )
		sbevent->fDataBlocks[n].fStatusFlags = 0;
	    else
		sbevent->fDataBlocks[n].fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_DATA_CORRUPT;
	    if ( dataWrappedSize )
		sbevent->fDataBlocks[n].fStatusFlags |= ALIHLTSUBEVENTDATADESCRIPTOR_SPLITPART1;
	    //sbevent->fDataBlocks[n].fByteOrder = kAliHLTNativeByteOrder;
	    sbevent->fDataBlocks[n].fAttributes[kAliHLTSEDBDByteOrderAttributeIndex] = kAliHLTLittleEndianByteOrder;
	    for ( int j = kAliHLTSEDBDAlignmentAttributeIndexStart; j <= kAliHLTSEDBDLastAlignmentAttributeIndex; j++ )
		sbevent->fDataBlocks[n].fAttributes[j] = kAliHLTUnknownAttribute;
	    sbevent->fDataBlocks[n].fProducerNode = fNodeID;
	    n++;
	    }
	if ( dataWrappedSize )
	    {
	    sbevent->fDataBlocks[n].fBlockOffset = 0;
	    sbevent->fDataBlocks[n].fBlockSize = dataWrappedSize;
	    sbevent->fDataBlocks[n].fShmID.fShmType = fShmKey.fShmType;
	    sbevent->fDataBlocks[n].fShmID.fKey.fID = fShmKey.fKey.fID;
	    sbevent->fDataBlocks[n].fDataType.fID = fSplitEventDataType.fID;
	    sbevent->fDataBlocks[n].fDataOrigin.fID = fEventDataOrigin.fID;
	    sbevent->fDataBlocks[n].fDataSpecification = fEventDataSpecification;
	    if ( !dataCorrupt )
		sbevent->fDataBlocks[n].fStatusFlags = 0;
	    else
		sbevent->fDataBlocks[n].fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_DATA_CORRUPT;
	    sbevent->fDataBlocks[n].fStatusFlags |= ALIHLTSUBEVENTDATADESCRIPTOR_SPLITPART2;
	    //sbevent->fDataBlocks[n].fByteOrder = kAliHLTNativeByteOrder;
	    sbevent->fDataBlocks[n].fAttributes[kAliHLTSEDBDByteOrderAttributeIndex] = kAliHLTLittleEndianByteOrder;
	    for ( int j = kAliHLTSEDBDAlignmentAttributeIndexStart; j <= kAliHLTSEDBDLastAlignmentAttributeIndex; j++ )
		sbevent->fDataBlocks[n].fAttributes[j] = kAliHLTUnknownAttribute;
	    sbevent->fDataBlocks[n].fProducerNode = fNodeID;
	    n++;
	    }
// 	printf( "%s:%d\n", __FILE__, __LINE__ );

	for ( unsigned addBlock=0; addBlock<GetAdditionalBlockCount(); addBlock++ )
	    {
	    if ( GetAdditionalBlock( addBlock, sbevent->fDataBlocks[n] ) )
		++n;
	    }
	if ( eventID.fType==kAliEventTypeStartOfRun || eventID.fType==kAliEventTypeEndOfRun )
	    {
	    for ( unsigned addBlock = 0; addBlock<addBlocks.size(); addBlock++ )
		{
		sbevent->fDataBlocks[n] = addBlocks[addBlock];
		n++;
		}
	    }

	sbevent->fDataBlockCount = n;
	sbevent->fDataType.fID = COMPOSITE_DATAID;
	trg = NULL;
#ifndef OLD_EVENTTRIGGER_DATA
	if ( !fAliceHLT || fDisableHLTEventTriggerData )
	    {
	    trg = fETP;
	    }
	else
	    {
	    pthread_mutex_lock( &fHLTEventTriggerDataLock );
	    AliHLTHLTEventTriggerData* hltTrg = fHLTEventTriggerDataCache.Get();
	    if ( !hltTrg )
		{
		LOG( AliHLTLog::kError, "AliHLTRORCPublisher::WaitForEvent", "out of memory (HLTEventTriggerData)" )
		    << "Out of memory trying to allocate HLT event trigger data object - using empty trigger data." << ENDLOG;
		trg = fETP;
		}
	    else
		{
		fHLTEventTriggerData.Add( hltTrg, eventID );
		if ( wrapAreaUsed )
		    memcpy( hltTrg->fCommonHeader, (const AliUInt8_t*)( fWrapCopyAreaStartPtr ), sizeof(hltTrg->fCommonHeader[0])*hltTrg->fCommonHeaderWordCnt );
		else
		    memcpy( hltTrg->fCommonHeader, (const AliUInt8_t*)( fShmPtr+dataOffset ), sizeof(hltTrg->fCommonHeader[0])*hltTrg->fCommonHeaderWordCnt );
		FillHLTEventTriggerDataDDLList( hltTrg );
		trg = &(hltTrg->fETS);
		}
	    pthread_mutex_unlock( &fHLTEventTriggerDataLock );
	    }
#else
	trg = fETP;
#endif

// 	printf( "%s:%d\n", __FILE__, __LINE__ );
	return 1;
	}
    else
	{
	fEventLoopQuitted = true;
	return 0;
	}
    }

void AliHLTRORCPublisher::EventFinished( AliEventID_t eventID, AliHLTSubEventDataDescriptor& sedd, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    unsigned long n = GetAdditionalBlockCount();
    if ( sedd.fDataBlockCount>=n )
	{
	for ( unsigned long ii=sedd.fDataBlockCount-n; ii<sedd.fDataBlockCount; ii++ )
	    {
	    ReleaseAdditionalBlock( sedd.fDataBlocks[ii] );
	    }
	}
    EventFinished( eventID, eventDoneData );
    }

void AliHLTRORCPublisher::EventFinished( AliEventID_t eventID, vector<AliHLTEventDoneData*>& )
    {
    if ( eventID.fType == kAliEventTypeStartOfRun )
	{
	ReleaseSORBlocks();
	}
    else if ( eventID.fType == kAliEventTypeEndOfRun )
	{
	ReleaseEORBlocks();
	}
    pthread_mutex_lock( &fRORCHandlerMutex );
    fRORCHandler->ReleaseEvent( eventID );
    pthread_mutex_unlock( &fRORCHandlerMutex );
    if ( fStatus )
	fStatus->fHLTStatus.fFreeOutputBuffer = fRORCHandler->GetFreeBufferSize();
#ifndef OLD_EVENTTRIGGER_DATA
    pthread_mutex_lock( &fHLTEventTriggerDataLock );
    unsigned long ndx;
    if ( (fAliceHLT && !fDisableHLTEventTriggerData) || fHLTEventTriggerData.GetCnt()>0 ) // fHLTEventTriggerData.GetCnt()>0 means there are events in there, maybe because the trigger data was switched off after some time of operation.
	{
	if ( fHLTEventTriggerData.FindElement( eventID, ndx ) )
	    {
	    fHLTEventTriggerDataCache.Release( fHLTEventTriggerData.Get(ndx) );
	    fHLTEventTriggerData.Remove( ndx );
	    }
	else
	    {
	    AliHLTLog::TLogLevel lvl;
	    if ( !fDisableHLTEventTriggerData )
		lvl = AliHLTLog::kError;
	    else
		lvl = AliHLTLog::kWarning;
	    LOG( lvl, "AliHLTRORCPublisher::EventFinished", "Cannot find HLTEventTriggerData" )
		<< "Unable to find HLT event trigger data object for event 0x" 
		<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ")." << ENDLOG;
	    }
	}
    pthread_mutex_unlock( &fHLTEventTriggerDataLock );
#endif
    }


void AliHLTRORCPublisher::QuitEventLoop()
    {
    fQuitEventLoop = true;
    }

void AliHLTRORCPublisher::StartEventLoop()
    {
    fStartOfRunEventReceived = false;
    fEndOfRunEventReceived = false;
    fDataEventWithoutStartOfRunWarning = false;
    fRORCHandler->ActivateRORC();
    fQuitEventLoop = false;
    }

void AliHLTRORCPublisher::EndEventLoop()
    {
    fRORCHandler->DeactivateRORC();
    if ( !fEndOfRunEventReceived && fStartOfRunEventReceived && fAliceHLT )
	{
	LOG( AliHLTLog::kWarning, "AliHLTRORCPublisher::EndEventLoop", "No End-Of-Data/-Run event" )
	    << "No End-Of-Data/-Run event received" << ENDLOG;
	}
    }

void AliHLTRORCPublisher::PauseProcessing()
    {
    fRORCHandler->DeactivateRORC();
    AliHLTDetectorPublisher::PauseProcessing();
    }

void AliHLTRORCPublisher::ResumeProcessing()
    {
    AliHLTDetectorPublisher::ResumeProcessing();
    fRORCHandler->ActivateRORC();
    }

#ifdef PROFILING
#define WRITEHISTO(histoname) name=filenameprefix;name+="-"#histoname".histo";f##histoname.Write(name.c_str());

void AliHLTRORCPublisher::WriteHistograms( const char* filenameprefix )
    {
    MLUCString name;

    WRITEHISTO( AnnounceEventTimeSetupHisto );
    WRITEHISTO( AnnounceEventTimeFindSubscribersHisto );
    WRITEHISTO( AnnounceEventTimeStoreEventHisto );
    WRITEHISTO( AnnounceEventTimeFindEventSlotHisto );
    WRITEHISTO( AnnounceEventTimeAnnounceEventHisto );
    WRITEHISTO( AnnounceEventTimeCallbacksHisto );
    WRITEHISTO( AnnounceEventTimeSetTimerHisto );
    WRITEHISTO( AnnounceEventTimeFindTimerHisto );
    }
#endif


void AliHLTRORCPublisher::EventRateTimerExpired()
    {
    AliHLTDetectorPublisher::EventRateTimerExpired();
    AliUInt32_t rorcBusyCounter = 0;
    if ( fRORCHandler && !fRORCHandler->ReadRORCBusyCounter( rorcBusyCounter ) && rorcBusyCounter!=fPreviousRORCBusyCounter && rorcBusyCounter>2)
        {
	bool doLog=false;
	if ( fRORCBusyCounterLogCnt<fNextRORCBusyCounterLogCntBaseLimit )
	    doLog = true;
	else if ( fRORCBusyCounterLogCnt>=fNextRORCBusyCounterLogCntBaseLimit*fNextRORCBusyCounterLogCntLimitFactor )
	    {
	    doLog = true;
	    fNextRORCBusyCounterLogCntLimitFactor*= 2;
	    }
	if ( doLog )
	    {
	    LOG( AliHLTLog::kImportant, "AliHLTRORCPublisher::EventRateTimerExpired", "Fifo Full Count" )
	      << "FIFO full counter has changed - new value: " << AliHLTLog::kDec << rorcBusyCounter
	      << " (0x" << AliHLTLog::kHex << rorcBusyCounter << ") - change since last message: "
	      << AliHLTLog::kDec << rorcBusyCounter-fPreviousRORCBusyCounter << " (0x"
	      << AliHLTLog::kHex << rorcBusyCounter-fPreviousRORCBusyCounter << ") - last value: "
	      << AliHLTLog::kDec << fPreviousRORCBusyCounter << " (0x" << AliHLTLog::kHex << fPreviousRORCBusyCounter
	      << ") - " << AliHLTLog::kDec << fRORCBusyCounterLogCnt-fRORCBusyCounterLastLogCnt-1
	      << " messages skipped since last message." << ENDLOG;
	    fRORCBusyCounterLastLogCnt = fRORCBusyCounterLogCnt;
	    fPreviousRORCBusyCounter = rorcBusyCounter;
  	  }
	++fRORCBusyCounterLogCnt;
	}
    }







/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
