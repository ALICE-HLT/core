/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTRORCTPCMapping.hpp"
#include <MLUCString.hpp>
#include <iostream>
#include <cstdlib>
#include <cstring>

int main( int argc, char** argv )
    {
    unsigned patchNr = ~0U;
    const char* filename = NULL;

    int i=1;
    const char* error = NULL;
    while ( i < argc && !error )
	{
	MLUCString arg( argv[i] );
	if ( arg == "-patch" )
	    {
	    if ( i+1 >= argc )
		{
		error = "Missing patch number argument.";
		break;
		}
	    char* cpErr = NULL;
	    patchNr = (unsigned)strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr != '\0' )
		{
		error = "Error converting patch number argument.";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( arg == "-filename" )
	    {
	    if ( i+1 >= argc )
		{
		error = "Missing patch number argument.";
		break;
		}
	    filename = argv[i+1];
	    i += 2;
	    continue;
	    }
	}
    
    if ( !error )
	{
	if ( patchNr == ~0U )
	    error = "Missing patch number specifier";
	else if ( filename == NULL )
	    error = "Missing mapping filename specifier.";
	}
    if ( error )
	{
	std::cerr << "Usage: " << argv[0] << " -patch <patch-r> -filename <mapping filename>" << std::endl;
	std::cerr << "Error: " << error << std::endl;
	return -1;
	}

    AliHLTRORCTPCMapping mapping( patchNr );

    int ret = mapping.Read( filename );
    if ( ret )
	{
	std::cerr << "Error reading mapping table from file '" << filename << "': " << strerror(ret) << " (" << ret << ") - " << mapping.GetLastError() << "." << std::endl;
	return -2;
	}

    for ( unsigned ndx=0; ndx<4096; ndx++ )
	{
	std::cout.setf ( std::ios::dec, std::ios::basefield );
	std::cout.fill( ' ' );
	std::cout.width( 4 );
	std::cout << ndx << " : ";
	std::cout.setf ( std::ios::hex, std::ios::basefield );
	std::cout << "0x";
	std::cout.width( 8 );
	std::cout.fill( '0' );
	std::cout << mapping[ndx];
	unsigned gainCalibInt = (mapping[ndx] >> 16) & 0x1FFF;
	double gainCalibFloat = ((double)gainCalibInt) / ((double)0x1000);
	std::cout.setf ( std::ios::dec, std::ios::basefield );
	std::cout.fill( ' ' );
	std::cout.width( 4 );
	std::cout << "- Gain calibration: ";
	std::cout.width( 4 );
	std::cout << gainCalibInt << " / 0x";
	std::cout.width( 4 );
	std::cout.fill( '0' );
	std::cout.setf ( std::ios::hex, std::ios::basefield );
	std::cout << gainCalibInt;
	std::cout.fill( ' ' );
	std::cout.width( 0 );
	std::cout << " / " << gainCalibFloat;
	std::cout << " - Channel active: " << ( (mapping[ndx] & (1<<15)) ? "yes" : "no " );
	std::cout << " - Border pad: " << ( (mapping[ndx] & (1<<14)) ? "yes" : "no " );
	std::cout << " - Row Nr: ";
	std::cout.fill( ' ' );
	std::cout.width( 2 );
	std::cout.setf ( std::ios::dec, std::ios::basefield );
	std::cout << ((mapping[ndx] >> 8) & 0x3F);
	std::cout << " - Pad Nr: ";
	std::cout.fill( ' ' );
	std::cout.width( 2 );
	std::cout.setf ( std::ios::dec, std::ios::basefield );
	std::cout << (mapping[ndx] & 0xFF);
	std::cout << std::endl;
	}

    return 0;
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
