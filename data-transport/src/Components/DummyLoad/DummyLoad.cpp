/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTDummyLoad.hpp"
//#include "AliHLTControlledProcessComponent.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTLog.hpp"


class DummyLoadComponent: public AliHLTControlledProcessorComponent
    {
    public:

	DummyLoadComponent( const char* name, int argc, char** argv );
	virtual ~DummyLoadComponent() {};


    protected:

	//virtual AliHLTProcessingSubscriber* CreateSubscriber();
	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

	virtual AliHLTProcessingSubscriber* CreateSubscriber();

	virtual bool SetupComponents();

	AliHLTDummyLoadSubscriber::TLoadType fLoadType;
	AliUInt32_t fDataOutputRate;
	AliHLTDummyLoadSubscriber::TProcessTimeCalcFunc fProcTimeFunc;
	void* fProcTimeFuncParam;
	AliUInt32_t fProcTimeConstTime;  // in usec
	AliUInt32_t fProcTimePropConst;  // in usec

	AliHLTEventDataType fEventDataType;
	bool fEventDataTypeSet;
	AliHLTEventDataOrigin fEventDataOrigin;
	bool fEventDataOriginSet;
	AliUInt32_t fEventDataSpecification;
	bool fEventDataSpecificationSet;

	bool fMergeMode;


    private:
    };


DummyLoadComponent::DummyLoadComponent( const char* name, int argc, char** argv ):
    AliHLTControlledProcessorComponent( name, argc, argv )
    {
    fLoadType = AliHLTDummyLoadSubscriber::kSleep;
    fDataOutputRate = 50;
    fProcTimeFunc = NULL;
    fProcTimeFuncParam = NULL;
    fProcTimeConstTime = 0;  // in usec
    fProcTimePropConst = 1;  // in usec
    fEventDataType.fID = UNKNOWN_DATAID;
    fEventDataTypeSet = false;
    fEventDataOrigin.fID = UNKNOWN_DATAORIGIN;
    fEventDataOriginSet = false;
    fEventDataSpecification = ~(AliUInt32_t)0;
    fEventDataSpecificationSet = false;
    fMergeMode = false;
    }


// AliHLTProcessingSubscriber* DummyLoadComponent::CreateSubscriber()
//     {
//     return new AliHLTDummyLoadSubscriber( fSubscriberName.c_str(), fSendEventDone, fMinBlockSize, fLoadType, 
// 					 fProcTimeFunc, fProcTimeFuncParam, fDataOutputRate, fMaxPreEventCountExp2 );
//     }

const char* DummyLoadComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cerr = NULL;
    if ( !strcmp( argv[i], "-loadtype" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing load type specifier.";
	    }
	if ( !strcmp( argv[i+1], "none" ) )
	    fLoadType = AliHLTDummyLoadSubscriber::kNone;
	else if ( !strcmp( argv[i+1], "sleep" ) )
	    fLoadType = AliHLTDummyLoadSubscriber::kSleep;
	else if ( !strcmp( argv[i+1], "busy" ) )
	    fLoadType = AliHLTDummyLoadSubscriber::kBusy;
	else if ( !strcmp( argv[i+1], "dataint" ) )
	    fLoadType = AliHLTDummyLoadSubscriber::kDataInt;
	else if ( !strcmp( argv[i+1], "datafloat" ) )
	    fLoadType = AliHLTDummyLoadSubscriber::kDataFloat;
	else
	    {
	    errorArg = i+1;
	    return "Unknown load type specifier";
	    }
	i += 2;
	return NULL;
	}
    // -proctime [const <const_time_usec>|linear <propConst_usec_per_kB>]	
    if ( !strcmp( argv[i], "-proctime" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing processing time function type specifier.";
	    }
	if ( !strcmp( argv[i+1], "const" ) )
	    {
	    if ( argc <= i +2 )
		{
		errorArg = i+1;
		return "Missing const processing time specifier.";
		}
	    fProcTimeFunc = (AliHLTDummyLoadSubscriber::TProcessTimeCalcFunc)&AliHLTDummyLoadSubscriber::FixedProcessingTime;
	    fProcTimeConstTime = strtoul( argv[i+2], &cerr, 0 );
	    fProcTimeFuncParam = (void*)&fProcTimeConstTime;
	    if ( *cerr )
		{
		errorArg = i+2;
		return "Error converting const processing time specifier..";
		}
	    i += 3;
	    return NULL;
	    }
	else if ( !strcmp( argv[i+1], "linear" ) )
	    {
	    if ( argc <= i +2 )
		{
		errorArg = i+1;
		return "Missing processing time proportional constant specifier.";
		}
	    fProcTimeFunc = (AliHLTDummyLoadSubscriber::TProcessTimeCalcFunc)&AliHLTDummyLoadSubscriber::LinearProcessingTime;
	    fProcTimePropConst = strtoul( argv[i+2], &cerr, 0 );
	    fProcTimeFuncParam = (void*)&fProcTimePropConst;
	    if ( *cerr )
		{
		errorArg = i+2;
		return "Error converting processing time proportional constant specifier..";
		}
	    i += 3;
	    return NULL;
	    }
	else
	    {
	    errorArg = i+1;
	    return "Unknown processing time function specifier.";
	    }
	return NULL;
	}
    if ( !strcmp( argv[i], "-dataoutput" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing data output rate (in %) specifier.";
	    }
	fDataOutputRate = strtoul( argv[i+1], &cerr, 0 );
	if ( *cerr )
	    {
	    errorArg = i+1;
	    return "Error converting data output rate (in %) specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-datatype" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing datatype specifier.";
	    }
	if ( strlen( argv[i+1])>8 )
	    {
	    return "Maximum allowed length for datatype specifier is 8 characters.";
	    }
	AliUInt64_t tmpDT = 0;
	unsigned tmpN;
	for( tmpN = 0; tmpN < strlen( argv[i+1]); tmpN++ )
	    {
	    tmpDT = (tmpDT << 8) | (AliUInt64_t)(argv[i+1][tmpN]);
	    }
	for ( ; tmpN < 8; tmpN++ )
	    {
	    tmpDT = (tmpDT << 8) | (AliUInt64_t)(' ');
	    }
	fEventDataType.fID = tmpDT;
	fEventDataTypeSet = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-dataorigin" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing dataorigin specifier.";
	    }
	if ( strlen( argv[i+1])>4 )
	    {
	    return "Maximum allowed length for dataorigin specifier is 4 characters.";
	    }
	AliUInt32_t tmpDO = 0;
	unsigned tmpN;
	for( tmpN = 0; tmpN < strlen( argv[i+1]); tmpN++ )
	    {
	    tmpDO = (tmpDO << 8) | (AliUInt32_t)(argv[i+1][tmpN]);
	    }
	for ( ; tmpN < 4; tmpN++ )
	    {
	    tmpDO = (tmpDO << 8) | (AliUInt32_t)(' ');
	    }
	fEventDataOrigin.fID = tmpDO;
	fEventDataOriginSet = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-dataspec" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing event data specification specifier.";
	    }
	AliUInt32_t tmpll = strtoul( argv[i+1], &cerr, 0 );
	if ( *cerr )
	    {
	    return "Error converting event data specification specifier..";
	    }
	fEventDataSpecification = tmpll;
	fEventDataSpecificationSet = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-mergemode" ) )
	{
	fMergeMode = true;
	i += 1;
	return NULL;
	}
    return AliHLTControlledProcessorComponent::ParseCmdLine( argc, argv, i, errorArg );
    }

void DummyLoadComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    AliHLTControlledProcessorComponent::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-loadtype [none|sleep|busy|dataint|datafloat]: Determine the type of dummy load to generate. " << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-dataoutput <outputpercentage>. Determine the percentage of the incoming data that is to be outputted." << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-proctime [const <const_time_usec>|linear <propConst_usec_per_kB>]: Determine the processing time to use for each event. Possible are a constant processing time or processing time proportional to the amount of incoming data." << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-datatype <type>: Specify the type as which the data is to be specified. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-dataorigin <type>: Specify the origin to be specified for the data. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-dataspec <type>: Specify the specifier that is to be used to characterize the data. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-mergemode: Activate merge mode where only one data block will be output per event instead of one output block per input block. (Optional)" << ENDLOG;
    }

AliHLTProcessingSubscriber* DummyLoadComponent::CreateSubscriber()
    {
    return new AliHLTDummyLoadSubscriber( fSubscriberID.c_str(), fSendEventDone, fMinBlockSize, 
					  fPerEventFixedSize, fPerBlockFixedSize, fSizeFactor, fLoadType, 
					  fProcTimeFunc, fProcTimeFuncParam, fDataOutputRate, 
					  fMaxPreEventCountExp2 );
    }

bool DummyLoadComponent::SetupComponents()
    {
    if ( !AliHLTControlledProcessorComponent::SetupComponents() )
	return false;

    if ( fEventDataTypeSet )
	((AliHLTDummyLoadSubscriber*)fSubscriber)->SetOutputDataType( fEventDataType );
    if ( fEventDataOriginSet )
	((AliHLTDummyLoadSubscriber*)fSubscriber)->SetOutputDataOrigin( fEventDataOrigin );
    if ( fEventDataSpecificationSet )
	((AliHLTDummyLoadSubscriber*)fSubscriber)->SetOutputDataSpecification( fEventDataSpecification );
    if ( fMergeMode )
	((AliHLTDummyLoadSubscriber*)fSubscriber)->SetMergeMode();
    return true;
    }



int main( int argc, char** argv )
    {
    DummyLoadComponent procComp( "DummyLoad", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

