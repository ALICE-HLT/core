/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

//#define DEBUG

#include "AliHLTDummyLoad.hpp"
#include "AliHLTLog.hpp"
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#ifdef DEBUG
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#endif

// int  nanosleep(const struct timespec *req, struct timespec
//        *rem);

#define BUSYLOOPITERATIONS 100


AliHLTDummyLoadSubscriber::AliHLTDummyLoadSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize,
						      AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, 
						      AliHLTDummyLoadSubscriber::TLoadType loadType, AliHLTDummyLoadSubscriber::TProcessTimeCalcFunc calcFunc, 
						    void* calcFuncParam, AliUInt32_t dataOutputPercentage, int eventCountAlloc ):
    AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc ),
    fLoadType( fLocalLoadType ), fDataOutputPercentage( fLocalDataOutputPercentage ), fTimeFunc( calcFunc ), fTimeFuncParam( calcFuncParam )
    {
    fLocalLoadType = loadType;
    fDataOutputPercentage = dataOutputPercentage;
    fDataType.fID = UNKNOWN_DATAID;
    fDataTypeSet = false;
    fDataOrigin.fID = UNKNOWN_DATAORIGIN;
    fDataOriginSet = false;
    fDataSpecification = ~(AliUInt32_t)0;
    fDataSpecificationSet = false;
    fMergeMode = false;
    }

// void AliHLTDummyLoadSubscriber::ProcessEvent( AliUInt8_t* dataIn, AliUInt32_t dataInSize,
// 				   AliUInt8_t* dataOut, AliUInt32_t dataOutMaxSize )
bool AliHLTDummyLoadSubscriber::ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, const AliHLTSubEventDataDescriptor*, const AliHLTEventTriggerStruct*,
				   AliUInt8_t* outputPtr, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, AliHLTEventDoneData*& )
    {
//     if ( fLoadType==kNone )
// 	{
// 	size = 0;
// 	return true;
// 	}

    if ( eventID.fType==kAliEventTypeStartOfRun || eventID.fType==kAliEventTypeEndOfRun || eventID.fType== kAliEventTypeSync )
	{
	outputBlocks.clear();
	size = 0;
	return true;
	}
    
    AliUInt32_t i, n; //, max;
    AliUInt8_t *dataIn=NULL, *dataOut=NULL, *ptrIn=NULL, *dataInEnd=NULL, *ptrOut=NULL, *dataOutEnd=NULL;
    AliUInt32_t dataInSize;
    AliUInt32_t ndx;
    struct timeval t0, t1, t2;
    AliUInt32_t td;
    AliUInt32_t outputSize = 0;
    unsigned long tmpSize;
    unsigned long long totalDataInSize = 0;

    dataOut = outputPtr;
    dataOutEnd = outputPtr+size;
    ptrOut = dataOut;
    gettimeofday( &t0, NULL );

    if ( !fMergeMode )
	outputBlocks.reserve( blockCnt );

    AliHLTSubEventDescriptor::BlockData dataBlock;
    for ( ndx = 0; ndx < blockCnt; ndx++ )
	{
#ifdef DEBUG
	if ( ndx==0 )
	    {
	    int fh;
	    fh = open( "DummyLoadInputDataDebug.data", O_WRONLY|O_CREAT|O_TRUNC, 0666 );
	    if ( fh==-1 )
		{
		LOG( AliHLTLog::kError, "AliHLTDummyLoadSubscriber::ProcessEvent", "Debug Data Write" )
		    << "Unable to open debug input data output file 'DummyLoadInputDataDebug.data': " 
		    << strerror(errno) << AliHLTLog::kDec << " (" << errno << ")." << ENDLOG;
		}
	    else
		{
		write( fh, blocks[ndx].fData, blocks[ndx].fSize );
		close( fh );
		}
	    }
#endif
	gettimeofday( &t1, NULL );
	dataIn = blocks[ndx].fData;
	dataInSize = blocks[ndx].fSize;
	totalDataInSize += dataInSize;
	dataInEnd = dataIn + dataInSize;
	tmpSize = (dataInSize*fDataOutputPercentage)/100;
	if ( tmpSize > size-outputSize )
	    tmpSize = size-outputSize;
#if 1
	if ( tmpSize<=0 && dataInSize>0 && fDataOutputPercentage>0 )
	    {
	    if ( fMergeMode )
		{
		if ( outputSize<8 )
		    tmpSize = 8-outputSize;
		else
		    tmpSize = 0;
		}
	    else
		tmpSize = 8;
	    }
#endif
	if ( !fMergeMode && tmpSize>0)
	    {
	    dataBlock = blocks[ndx];
	    dataBlock.fShmKey.fShmType = kInvalidShmType;
	    dataBlock.fShmKey.fKey.fID = ~(AliUInt64_t)0;
	    dataBlock.fOffset = outputSize;
	    dataBlock.fSize = tmpSize;
	    if ( !fDataTypeSet )
		dataBlock.fDataType = blocks[ndx].fDataType;
	    else
		dataBlock.fDataType = fDataType;
	    if ( !fDataOriginSet )
		dataBlock.fDataOrigin = blocks[ndx].fDataOrigin;
	    else
		dataBlock.fDataOrigin = fDataOrigin;
	    if ( !fDataSpecificationSet )
		dataBlock.fDataSpecification = blocks[ndx].fDataSpecification;
	    else
		dataBlock.fDataSpecification = fDataSpecification;
	    dataBlock.fStatusFlags = blocks[ndx].fStatusFlags;
	    outputBlocks.insert( outputBlocks.end(), dataBlock );
	    }
	// Copy once at beginning
	CopyData( ptrOut, tmpSize, dataIn, dataInSize );


	outputSize += tmpSize;
	ptrOut += tmpSize;

	}
    if ( fMergeMode && blockCnt>0 && outputSize>0 )
	{
	dataBlock = blocks[0];
	dataBlock.fShmKey.fShmType = kInvalidShmType;
	dataBlock.fShmKey.fKey.fID = ~(AliUInt64_t)0;
	dataBlock.fOffset = 0;
	dataBlock.fSize = outputSize;
	if ( fDataTypeSet )
	    dataBlock.fDataType = fDataType;
	if ( fDataOriginSet )
	    dataBlock.fDataOrigin = fDataOrigin;
	if ( fDataSpecificationSet )
	    dataBlock.fDataSpecification = fDataSpecification;
	outputBlocks.insert( outputBlocks.end(), dataBlock );
	}

    AliUInt32_t processingTime;
    if ( fTimeFunc )
	processingTime = fTimeFunc( fTimeFuncParam, totalDataInSize );
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTDummyLoadSubscriber::ProcessEvent", "No Processing Time Func" )
	    << "No processing time calculation function specified. Using 10 ms." << ENDLOG;
	processingTime = 10000;
	}
    switch ( fLoadType )
	{
	case kNone:
	    break;
	case kSleep:
	    {
	    gettimeofday( &t2, NULL );
	    td = (t2.tv_sec-t1.tv_sec)*1000000+(t2.tv_usec-t1.tv_usec);
	    while ( td < processingTime )
		{
		usleep( processingTime-td );
		gettimeofday( &t2, NULL );
		td = (t2.tv_sec-t1.tv_sec)*1000000+(t2.tv_usec-t1.tv_usec);
		}
	    break;
	    }
	case kBusy: 
	    {
	    gettimeofday( &t2, NULL );
	    td = (t2.tv_sec-t1.tv_sec)*1000000+(t2.tv_usec-t1.tv_usec);
	    while ( td < processingTime )
		{
		n = 0;
		for ( i = 0; i < BUSYLOOPITERATIONS; i++ )
		    n += i;
		gettimeofday( &t2, NULL );
		td = (t2.tv_sec-t1.tv_sec)*1000000+(t2.tv_usec-t1.tv_usec);
		}
	    break;
	    }
	case kDataInt: 
	    {
	    AliUInt32_t chkSum1 = 0, chkSum2 = 0;
	    gettimeofday( &t2, NULL );
	    td = (t2.tv_sec-t1.tv_sec)*1000000+(t2.tv_usec-t1.tv_usec);
	    ndx=0;
	    while ( td < processingTime )
		{
		ptrIn = blocks[ndx].fData;
		dataInEnd = ptrIn + blocks[ndx].fSize;
		while ( ptrIn < dataInEnd )
		    {
		    chkSum1 ^= *ptrIn;
		    chkSum2 += *ptrIn++;
		    }
		if ( ++ndx>=blockCnt )
		    ndx = 0;
		gettimeofday( &t2, NULL );
		td = (t2.tv_sec-t1.tv_sec)*1000000+(t2.tv_usec-t1.tv_usec);
		}
	    break;
	    }
	case kDataFloat: 
	    {
	    double chkSum = 0, d;
	    gettimeofday( &t2, NULL );
	    td = (t2.tv_sec-t1.tv_sec)*1000000+(t2.tv_usec-t1.tv_usec);
	    ndx=0;
	    while ( td < processingTime )
		{
		ptrIn = blocks[ndx].fData;
		dataInEnd = ptrIn + blocks[ndx].fSize;
		while ( ptrIn < dataInEnd )
		    {
		    d = *ptrIn++;
		    chkSum += d;
		    }
		if ( ++ndx>=blockCnt )
		    ndx = 0;
		gettimeofday( &t2, NULL );
		td = (t2.tv_sec-t1.tv_sec)*1000000+(t2.tv_usec-t1.tv_usec);
		}
	    break;
	    }
	}


    ptrOut = dataOut;
    unsigned long long tmpOutputSize=0;
    for ( ndx = 0; ndx < blockCnt; ndx++ )
	{
	dataIn = blocks[ndx].fData;
	dataInSize = blocks[ndx].fSize;
	tmpSize = (dataInSize*fDataOutputPercentage)/100;
	if ( tmpSize > size-tmpOutputSize )
	    tmpSize = size-tmpOutputSize;
	// Copy a second time at end, to test for errors that occur due to canceled events or things like that...
	CopyData( ptrOut, tmpSize, dataIn, dataInSize );
	ptrOut += tmpSize;
	tmpOutputSize += tmpSize;
	}
    
    if ( outputSize <= size )
	size = outputSize;
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTDummyLoad::ProcessEvent", "Too much output" )
	    << "Produced output of " << AliHLTLog::kDec << outputSize 
	    << " bytes while only allowed " << size << " bytes." << ENDLOG;
	}
    return true;
    }

AliUInt32_t AliHLTDummyLoadSubscriber::FixedProcessingTime( AliUInt32_t* constProcTime, AliUInt32_t )
    {
    return *constProcTime;
    }

AliUInt32_t AliHLTDummyLoadSubscriber::LinearProcessingTime( AliUInt32_t* propConstPerKB, AliUInt32_t eventSize )
    {
    return ( ((eventSize%1024)>=512) ?  ((eventSize/1024)+1) : (eventSize/1024) ) * (*propConstPerKB);
    }


void AliHLTDummyLoadSubscriber::CopyData( uint8* output, unsigned long outputSize, uint8* input, unsigned long inputSize )
    {
    unsigned long i = 0;
    while ( outputSize > 0 )
	{
	if ( outputSize > inputSize )
	    {
	    memcpy( output+i, input, inputSize );
	    i += inputSize;
	    outputSize -= inputSize;
	    }
	else
	    {
	    memcpy( output+i, input, outputSize );
	    i += outputSize;
	    outputSize = 0;
	    }
	}
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
