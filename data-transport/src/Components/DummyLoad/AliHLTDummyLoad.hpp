/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3DUMMYLOAD_HPP_
#define _ALIL3DUMMYLOAD_HPP_ 

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliHLTProcessingSubscriber.hpp"

class AliHLTDummyLoadSubscriber: public AliHLTProcessingSubscriber
    {
    public:

	typedef AliUInt32_t (*TProcessTimeCalcFunc)( void*, AliUInt32_t ); // Returns processing time in microsecs...
	//typedef unsigned long (*TProcessTimeCalcFunc)( void*, AliUInt32_t eventSize ); // Returns processing time in microsecs...

	enum TLoadType { kNone=0, kSleep=1, kBusy=2, kDataInt=3, kDataFloat=4 };

	AliHLTDummyLoadSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize, 
				   AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, 
				   TLoadType loadType, TProcessTimeCalcFunc, void* calcFuncParam, AliUInt32_t dataOutputPercentage,
				   int eventCountAlloc = -1 );

	virtual bool ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* etsp,
				   AliUInt8_t* outputPtr, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, AliHLTEventDoneData*& edd );
// 	virtual void ProcessEvent( AliUInt8_t* dataIn, AliUInt32_t dataInSize,
// 				   AliUInt8_t* dataOut, AliUInt32_t dataOutMaxsize );

	void SetOutputDataType( AliHLTEventDataType type )
		{
		fDataType.fID = type.fID;
		fDataTypeSet = true;
		}

	void SetOutputDataOrigin( AliHLTEventDataOrigin origin )
		{
		fDataOrigin.fID = origin.fID;
		fDataOriginSet = true;
		}

	void SetOutputDataSpecification( AliUInt32_t dataSpec )
		{
		fDataSpecification = dataSpec;
		fDataSpecificationSet = true;
		}

	void SetMergeMode()
		{
		fMergeMode = true;
		}

	TLoadType& fLoadType;

	AliUInt32_t& fDataOutputPercentage;

	static AliUInt32_t FixedProcessingTime( AliUInt32_t* constProcTime, AliUInt32_t eventSize );
	static AliUInt32_t LinearProcessingTime( AliUInt32_t* propConstPerKB, AliUInt32_t eventSize );


    protected:

	void CopyData( uint8* output, unsigned long outputSize, uint8* input, unsigned long inputSize );

	TLoadType fLocalLoadType;

	AliUInt32_t fLocalDataOutputPercentage;

	TProcessTimeCalcFunc fTimeFunc;
	void* fTimeFuncParam;

	AliHLTEventDataType fDataType;
	bool fDataTypeSet;

	AliHLTEventDataOrigin fDataOrigin;
	bool fDataOriginSet;

	AliUInt32_t fDataSpecification;
	bool fDataSpecificationSet;

	bool fMergeMode;

    private:
    };






/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3DUMMYLOAD_HPP_
