/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTProcessingSubscriber.hpp"
//#include "AliHLTControlledProcessComponent.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "AliHLTRelaySubscriber.hpp"
#include <errno.h>

class Relay: public AliHLTControlledDefaultProcessorComponent<AliHLTRelaySubscriber>
    {
    public:
	Relay( const char* compName, int argc, char** argv ):
	    AliHLTControlledDefaultProcessorComponent<AliHLTRelaySubscriber>( compName, argc, argv ),
	    fEventForwardModulo(1)
		{
		}

    protected:

	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
		{
		char* cpErr = NULL;
		if ( !strcmp( argv[i], "-eventforwardmodulo" ) )
		    {
		    if ( argc <= i+1 )
			return "Missing event forward modulo specifier.";
		    fEventForwardModulo = strtoul( argv[i+1], &cpErr, 0 );
		    if ( *cpErr )
			{
			errorArg = i+1;
			return "Error converting event forward modulo specifier.";
			}
		    if ( !fEventForwardModulo )
			{
			errorArg = i+1;
			return "Error, event forward modulo specifier must not be 0.";
			}
		    i += 2;
		    return NULL;
		    }
		return AliHLTControlledDefaultProcessorComponent<AliHLTRelaySubscriber>::ParseCmdLine( argc, argv, i, errorArg );
		}

	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam )
		{
		AliHLTControlledDefaultProcessorComponent<AliHLTRelaySubscriber>::PrintUsage( errorStr, errorArg, errorParam );
		LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
		    << "-eventforwardmodulo <modulo-count>: Specify that only the data blocks for every Nth event are forwarded. (Mandatory Optional)" << ENDLOG;
		}

	virtual bool SetupComponents()
		{
		if ( !AliHLTControlledDefaultProcessorComponent<AliHLTRelaySubscriber>::SetupComponents() )
		    return false;
		((AliHLTRelaySubscriber*)fSubscriber)->SetEventModulo( fEventForwardModulo );
		return true;
		}

	unsigned long fEventForwardModulo;

    };


int main( int argc, char** argv )
    {
    Relay procComp( "RelayComponent", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

