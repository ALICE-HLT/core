/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

//#define DEBUG

#include "AliHLTRelaySubscriber.hpp"
#include "AliHLTLog.hpp"


AliHLTRelaySubscriber::AliHLTRelaySubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize,
					      AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, int eventCountAlloc ):
    AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc ),
    fEventCnt(0),
    fEventModulo(1)
    {
    }

// void AliHLTRelaySubscriber::ProcessEvent( AliUInt8_t* dataIn, AliUInt32_t dataInSize,
// 				   AliUInt8_t* dataOut, AliUInt32_t dataOutMaxSize )
bool AliHLTRelaySubscriber::ProcessEvent( AliEventID_t, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, const AliHLTSubEventDataDescriptor*, const AliHLTEventTriggerStruct*,
				   AliUInt8_t*, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, AliHLTEventDoneData*& )
    {
    unsigned long ndx;
    if ( !( fEventCnt++ % fEventModulo ) )
	{
	outputBlocks.reserve( blockCnt );
	for ( ndx = 0; ndx < blockCnt; ndx++ )
	    {
	    outputBlocks.push_back( blocks[ndx] );
	    }
	}
    size = 0;
    return true;
    }

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
