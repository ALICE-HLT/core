/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: EventProtocolSubscriber.cpp 945 2006-05-10 12:36:55Z timm $ 
**
***************************************************************************
*/

#include "AliHLTControlledComponent.hpp"
#include "AliHLTProcessingSubscriber.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <glob.h>

class EventProtocolSubscriber: public AliHLTProcessingSubscriber
    {
    public:

	EventProtocolSubscriber( const char* name, 
				 bool sendEventDone, AliUInt32_t minBlockSize,
				 AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, 
				 int eventCountAlloc = -1 );
  
	virtual ~EventProtocolSubscriber();

    protected:

	virtual bool ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* etsp,
				   AliUInt8_t* outputPtr, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, AliHLTEventDoneData*& edd );


    private:
    };




EventProtocolSubscriber::EventProtocolSubscriber( const char* name, 
						  bool sendEventDone, AliUInt32_t minBlockSize,
						  AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, 
						  int eventCountAlloc ):
    AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc )
    {
    }

EventProtocolSubscriber::~EventProtocolSubscriber()
    {
    }

bool EventProtocolSubscriber::ProcessEvent( AliEventID_t eventID, AliUInt32_t /*blockCnt*/, AliHLTSubEventDescriptor::BlockData* /*blocks*/, const AliHLTSubEventDataDescriptor* /*sedd*/, const AliHLTEventTriggerStruct* /*ets*/,
					       AliUInt8_t*, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>&, AliHLTEventDoneData*& )
    {
    size = 0;
    LOG( AliHLTLog::kWarning, "EventProtocolSubscriber::ProcessEvent", "Event Received" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
	<< ") received." << ENDLOG;
    return true;
    }
	    




class EventProtocolSubscriberComponent: public AliHLTControlledSinkComponent
    {
    public:

	EventProtocolSubscriberComponent( const char* name, int argc, char** argv );
	virtual ~EventProtocolSubscriberComponent() {};


    protected:

	virtual void ShowConfiguration();

	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

	virtual EventProtocolSubscriber* CreateSubscriber();

	typedef AliHLTControlledSinkComponent TSuper;

    private:
    };


EventProtocolSubscriberComponent::EventProtocolSubscriberComponent( const char* name, int argc, char** argv ):
    TSuper( name, argc, argv )
    {
    }

void EventProtocolSubscriberComponent::ShowConfiguration()
    {
    TSuper::ShowConfiguration();
    }

const char* EventProtocolSubscriberComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    return TSuper::ParseCmdLine( argc, argv, i, errorArg );
    }

void EventProtocolSubscriberComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    TSuper::PrintUsage( errorStr, errorArg, errorParam );
    }


EventProtocolSubscriber* EventProtocolSubscriberComponent::CreateSubscriber()
    {
    return new EventProtocolSubscriber( fSubscriberID.c_str(), 
					fSendEventDone, fMinBlockSize, 
					fPerEventFixedSize, fPerBlockFixedSize, fSizeFactor, 
					fMaxPreEventCountExp2 );
    }



int main( int argc, char** argv )
    {
    EventProtocolSubscriberComponent procComp( "EventProtocolSubscriber", argc, argv );
    procComp.Run();
    return 0;
    }



/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: EventProtocolSubscriber.cpp 945 2006-05-10 12:36:55Z timm $ 
**
***************************************************************************
*/
