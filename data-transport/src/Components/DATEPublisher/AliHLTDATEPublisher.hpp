/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIHLTFILEPUBLISHER_HPP_
#define _ALIHLTFILEPUBLISHER_HPP_

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTDATEPublisher.hpp 1247 2006-09-24 10:25:16Z timm $ 
**
***************************************************************************
*/

#include "AliHLTDetectorPublisher.hpp"
#include "AliHLTHLTEventTriggerData.hpp"
#include "MLUCHistogram.hpp"
#include "MLUCVector.hpp"
#include "MLUCObjectCache.hpp"
#include "DATEDataReader.hpp"
#include <pthread.h>
#include <vector>

class AliHLTDATEPublisher: public AliHLTDetectorPublisher
    {
    public:

	AliHLTDATEPublisher( const char* name, AliUInt32_t time_betw_evts_usec,
			     vector<MLUCString> filenames, 
			     equipmentIdType equipmentID, 
			     int eventSlotsPowerOfTwo = -1 );
	virtual ~AliHLTDATEPublisher();

	void StopPublishing();

	AliUInt32_t& fEventTime; // in usec

	AliUInt32_t& fCreateEvents;

	AliUInt32_t GetEventCounter()
		{
		return fEventCounter;
		}

	unsigned long GetWaitSleepCount()
		{
		return fWaitSleepCount;
		}

	void SetPollSleepTime( unsigned long pst )
		{
		fPollSleepTime = pst;
		}

	void SetBusyPollTime( unsigned long bpt )
		{
		fBusyPollTime = bpt;
		}

	void SetGetBlockRetryCnt( unsigned long gbrt )
		{
		fGetBlockRetryCnt = gbrt;
		}

	void SetPollSleepModulo( unsigned long mod )
		{
		fPollSleepModulo = mod;
		}

	void SetEventDataType( AliHLTEventDataType datatype )
		{
		fEventDataType.fID = datatype.fID;
		}

	void SetEventDataOrigin( AliHLTEventDataOrigin dataorigin )
		{
		fEventDataOrigin.fID = dataorigin.fID;
		}

	void SetEventDataSpecification( AliUInt32_t dataspec )
		{
		fEventDataSpecification = dataspec;
		}

	void EventTimeJustSleep()
		{
		fEventTimeJustSleep = true;
		}

	virtual void StartAnnouncing();
	virtual void StopAnnouncing();

	virtual void PauseProcessing();
	virtual void ResumeProcessing();


    protected:

	virtual int WaitForEvent( AliEventID_t& eventID, AliHLTSubEventDataDescriptor*& sbevent,
				  AliHLTEventTriggerStruct*& trg ); // Called with no locked mutexes

	virtual void EventFinished( AliEventID_t, AliHLTSubEventDataDescriptor& sbevent, vector<AliHLTEventDoneData*>& eventDoneData ); // Called with no locked mutexes
	virtual void EventFinished( AliEventID_t, vector<AliHLTEventDoneData*>& eventDoneData ); // Called with no locked mutexes
	virtual void QuitEventLoop(); // Called with no locked mutexes
	bool QuittedEventLoop()
		{
		return fEventLoopQuitted;
		}
	virtual void StartEventLoop();
	virtual void EndEventLoop();

        DATEDataReader fReader;

      std::vector<MLUCString> fFilenames;

	bool fQuitEventLoop;
	bool fEventLoopQuitted;

	unsigned long fWaitSleepCount;

	unsigned long fPollSleepTime; // usec
	unsigned long fBusyPollTime;  // usec
	unsigned long fGetBlockRetryCnt;
	unsigned long fPollSleepModulo;

	struct timeval fLastAnnounceStart;
	AliUInt64_t fEventCounterSinceLastStart;

	AliUInt64_t fEventCounter;

	AliHLTEventTriggerStruct fETS;
	AliHLTEventTriggerStruct* fETP;
	AliUInt32_t fETPOffset;
	AliUInt32_t fETPIndex;

	AliUInt32_t fLocalEventTime; // in usec

	bool fEventTimeJustSleep;
	
	AliUInt32_t fLocalCreateEvents;

	struct timeval fLastEventStart;

	AliUInt32_t fOldestEventBirth;

	AliHLTNodeID_t fNodeID;

	struct timeval fLastReleaseRequestTime;
	unsigned long fReleaseRequestTimeDiff;

	AliHLTEventDataType fEventDataType;
	AliHLTEventDataOrigin fEventDataOrigin;
	AliUInt32_t fEventDataSpecification;

	AliEventID_t fLastEventID;
	AliEventID_t fAddEventIDCounter;

	MLUCObjectCache<AliHLTHLTEventTriggerData> fHLTEventTriggerDataCache;
	MLUCIndexedVector<AliHLTHLTEventTriggerData*,AliEventID_t> fHLTEventTriggerData;
	pthread_mutex_t fHLTEventTriggerDataLock;
	bool fDisableHLTEventTriggerData;

    private:
    };





/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTDATEPublisher.hpp 1247 2006-09-24 10:25:16Z timm $ 
**
***************************************************************************
*/

#endif // _ALIHLTFILEPUBLISHER_HPP_
