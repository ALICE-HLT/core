/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: DATEPublisher.cpp 1366 2006-10-09 06:44:03Z timm $ 
**
***************************************************************************
*/

#include "AliHLTDATEPublisher.hpp"
//#include "AliHLTControlledProcessComponent.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTLog.hpp"
#include <vector>
#include <glob.h>

class DATEPublisherComponent: public AliHLTControlledSourceComponent
    {
    public:

	DATEPublisherComponent( const char* name, int argc, char** argv );
	virtual ~DATEPublisherComponent() {};


    protected:

	virtual AliHLTDetectorPublisher* CreatePublisher();
	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

	virtual bool CheckConfiguration();
	virtual void ShowConfiguration();

	virtual bool SetupComponents();
	virtual void DestroyParts();

	vector<MLUCString> fFilenames;
	unsigned long fEventTime;
	AliHLTEventDataType fEventDataType;
	AliHLTEventDataOrigin fEventDataOrigin;
	AliUInt32_t fEventDataSpecification;

	bool fEventTimeJustSleep;

      equipmentIdType fEquipmentID;
	bool fEquipmentIDSet;

    private:
    };


DATEPublisherComponent::DATEPublisherComponent( const char* name, int argc, char** argv ):
    AliHLTControlledSourceComponent( name, argc, argv )
    {
    fEventTime = 0; // By default publish as fast as possible
    fEventDataType.fID = UNKNOWN_DATAID;
    fEventDataOrigin.fID = UNKNOWN_DATAORIGIN;
    fEventDataSpecification = ~(AliUInt32_t)0;
    fEventTimeJustSleep = false;
    fEquipmentID = 0;
    fEquipmentIDSet = false;
    }


AliHLTDetectorPublisher* DATEPublisherComponent::CreatePublisher()
    {
      return new AliHLTDATEPublisher( fOwnPublisherName.c_str(), fEventTime,
				      fFilenames, fEquipmentID, fMaxPreEventCountExp2 );
    }


const char* DATEPublisherComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cerr = NULL;
    if ( !strcmp( argv[i], "-datefile" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing filename specifier.";
	    }
	MLUCString tmp( argv[i+1] );
	fFilenames.insert( fFilenames.end(), tmp );
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-equipmentid" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing equipment id specifier.";
	    }
	fEquipmentID = strtoul( argv[i+1], &cerr, 0 );
	fEquipmentIDSet = true;
	if ( *cerr )
	    {
	    return "Error converting equipment ID specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventtime" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing event interval time specifier.";
	    }
	fEventTime = strtoul( argv[i+1], &cerr, 0 );
	if ( *cerr )
	    {
	    return "Error converting event interval time specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-datatype" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing datatype specifier.";
	    }
	if ( strlen( argv[i+1])>8 )
	    {
	    return "Maximum allowed length for datatype specifier is 8 characters.";
	    }
	AliUInt64_t tmpDT = 0;
	unsigned tmpN;
	for( tmpN = 0; tmpN < strlen( argv[i+1]); tmpN++ )
	    {
	    tmpDT = (tmpDT << 8) | (AliUInt64_t)(argv[i+1][tmpN]);
	    }
	for ( ; tmpN < 8; tmpN++ )
	    {
	    tmpDT = (tmpDT << 8) | (AliUInt64_t)(' ');
	    }
	fEventDataType.fID = tmpDT;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-dataorigin" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing dataorigin specifier.";
	    }
	if ( strlen( argv[i+1])>4 )
	    {
	    return "Maximum allowed length for dataorigin specifier is 4 characters.";
	    }
	AliUInt32_t tmpDO = 0;
	unsigned tmpN;
	for( tmpN = 0; tmpN < strlen( argv[i+1]); tmpN++ )
	    {
	    tmpDO = (tmpDO << 8) | (AliUInt32_t)(argv[i+1][tmpN]);
	    }
	for ( ; tmpN < 4; tmpN++ )
	    {
	    tmpDO = (tmpDO << 8) | (AliUInt32_t)(' ');
	    }
	fEventDataOrigin.fID = tmpDO;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-dataspec" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing event data specification specifier.";
	    }
	AliUInt32_t tmpll = strtoul( argv[i+1], &cerr, 0 );
	if ( *cerr )
	    {
	    return "Error converting event data specification specifier..";
	    }
	fEventDataSpecification = tmpll;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-sleeptillevent" ) )
	{
	fEventTimeJustSleep = true;
	i += 1;
	return NULL;
	}
    return AliHLTControlledSourceComponent::ParseCmdLine( argc, argv, i, errorArg );
    }

void DATEPublisherComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    AliHLTControlledSourceComponent::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-datefile <filename>: Specify the name of a file in DATE format that contains data to publish. (Mandatory, can be used multiple times)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-eventtime <interval-microsec.>: Specify the time between the publishing of two events (in microseconds). (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-equipmentid <equipment-id-nr>: Specify the ID of the equipment to be published for events (in microseconds). (Optional)" << ENDLOG;
    }

bool DATEPublisherComponent::CheckConfiguration()
    {
    if ( !AliHLTControlledComponent::CheckConfiguration() )
	return false;
    if ( fFilenames.size()<=0 )
	{
	LOG( AliHLTLog::kError, "DATEPublisherComponent::CheckConfiguration", "No file specified" )
	    << "Must specify at least one file to publish (using the -datefile command line option."
	    << ENDLOG;
	return false;
	}
    if ( !fEquipmentIDSet )
	{
	LOG( AliHLTLog::kError, "DATEPublisherComponent::CheckConfiguration", "No equipment ID specified" )
	    << "Must specify the equipment ID to publish (using the -equipmentid command line option."
	    << ENDLOG;
	return false;
	}
    return true;
    }

void DATEPublisherComponent::ShowConfiguration()
    {
    AliHLTControlledComponent::ShowConfiguration();
    LOG( AliHLTLog::kInformational, "DATEPublisherComponent::ShowConfiguration", "Event Time Configuration" )
	<< "Event time: " << AliHLTLog::kDec << fEventTime << " (0x" << AliHLTLog::kHex
	<< fEventTime << ")." << ENDLOG;
    vector<MLUCString>::iterator iter, end;
    iter = fFilenames.begin();
    end = fFilenames.end();
    while ( iter != end )
	{
	LOG( AliHLTLog::kInformational, "DATEPublisherComponent::ShowConfiguration", "Reading in file" )
	    << "Using DATE file '" << iter->c_str() << "'" << ENDLOG;
	iter++;
	}
    LOG( AliHLTLog::kInformational, "DATEPublisherComponent::ShowConfiguration", "Equipment ID Configuration" )
	<< "Equipment ID: " << AliHLTLog::kDec << fEquipmentID << " (0x" << AliHLTLog::kHex
	<< fEquipmentID << ")." << ENDLOG;
    }

bool DATEPublisherComponent::SetupComponents()
    {
    if ( !AliHLTControlledComponent::SetupComponents() )
	return false;
    ((AliHLTDATEPublisher*)fPublisher)->SetEventDataType( fEventDataType );
    ((AliHLTDATEPublisher*)fPublisher)->SetEventDataOrigin( fEventDataOrigin );
    ((AliHLTDATEPublisher*)fPublisher)->SetEventDataSpecification( fEventDataSpecification );
    if ( fEventTimeJustSleep )
	((AliHLTDATEPublisher*)fPublisher)->EventTimeJustSleep();
    return true;
    }

void DATEPublisherComponent::DestroyParts()
    {
    AliHLTControlledComponent::DestroyParts();
    }



int main( int argc, char** argv )
    {
    DATEPublisherComponent procComp( "DATEPublisher", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }




/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: DATEPublisher.cpp 1366 2006-10-09 06:44:03Z timm $ 
**
***************************************************************************
*/
