/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTDATEPublisher.cpp 1402 2006-10-12 09:29:42Z timm $ 
**
***************************************************************************
*/

#include "AliHLTDATEPublisher.hpp"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTEventDataType.h"
#include "AliHLTLog.hpp"
#include "AliHLTGetNodeID.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTBufferManager.hpp"
#include "AliHLTShmManager.hpp"
#include "AliHLTStackTrace.hpp"
#include "AliHLTSubEventDescriptor.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "AliHLTDDLHeader.hpp"
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>




AliHLTDATEPublisher::AliHLTDATEPublisher( const char* name, AliUInt32_t time_betw_evts_usec,
					  vector<MLUCString> filenames, 
					  equipmentIdType equipmentID, 
					  int eventSlotsPowerOfTwo ):
    AliHLTDetectorPublisher( name, eventSlotsPowerOfTwo ),
    fEventTime( fLocalEventTime ),
    fCreateEvents( fLocalCreateEvents ),
    fReader( filenames, equipmentID ),
    fFilenames( filenames ),
    fAddEventIDCounter( kAliEventTypeUnknown, (~(AliUInt64_t)0) << 36 ),
    fHLTEventTriggerDataCache( eventSlotsPowerOfTwo ),
    fHLTEventTriggerData( 8, eventSlotsPowerOfTwo )
    {
    fWaitSleepCount = 0;
    fQuitEventLoop = false;
    fEventLoopQuitted = false;
    fEventCounter=0;
    fCreateEvents = 1;
    fOldestEventBirth = 0;

    fEventTime = time_betw_evts_usec;

    fETP = &fETS;
    fETP->fHeader.fLength = sizeof(AliHLTEventTriggerStruct);
    fETP->fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
    fETP->fHeader.fSubType.fID = 0;
    fETP->fHeader.fVersion = 1;
    fETP->fDataWordCount = 0;

    fLastEventStart.tv_sec = fLastEventStart.tv_usec = 0;

    fNodeID = AliHLTGetNodeID();
    fPollSleepTime = 20000;
    fBusyPollTime = 1;
    fPollSleepModulo = 10;
    //     fPollSleepTime = 10000;
    //     fBusyPollTime = 5;
    //     fPollSleepModulo = 1000;
    fGetBlockRetryCnt = 10000;
    fLastReleaseRequestTime.tv_sec = fLastReleaseRequestTime.tv_usec = 0;
    fReleaseRequestTimeDiff = 500000; // by default send at most 2 EventReleaseRequest message per second.
    fEventDataType.fID = UNKNOWN_DATAID;
    fEventDataOrigin.fID = UNKNOWN_DATAORIGIN;
    fEventDataSpecification = ~(AliUInt32_t)0;
    fEventTimeJustSleep = false;
    fLastAnnounceStart.tv_sec = fLastAnnounceStart.tv_usec = 0;
    fEventCounterSinceLastStart = 0;
    pthread_mutex_init( &fHLTEventTriggerDataLock, NULL );
    fDisableHLTEventTriggerData = false;
    }

AliHLTDATEPublisher::~AliHLTDATEPublisher()
    {
    pthread_mutex_destroy( &fHLTEventTriggerDataLock );
    }

void AliHLTDATEPublisher::StopPublishing()
    {
    QuitEventLoop();
    fEventAnnouncer.Quit();
    struct timespec ts;
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 50000000;
    gettimeofday( &start, NULL );
    while ( !QuittedEventLoop() && deltaT<timeLimit )
	{
	ts.tv_sec = 0;
	ts.tv_nsec = 500000;
	nanosleep( &ts, NULL );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    
    }


void AliHLTDATEPublisher::StartAnnouncing()
    {
    gettimeofday( &fLastAnnounceStart, NULL );
    fEventCounterSinceLastStart = 0;
    AliHLTDetectorPublisher::StartAnnouncing();
    }

void AliHLTDATEPublisher::StopAnnouncing()
    {
    AliHLTDetectorPublisher::StopAnnouncing();
    }

void AliHLTDATEPublisher::PauseProcessing()
    {
    AliHLTDetectorPublisher::PauseProcessing();
    }

void AliHLTDATEPublisher::ResumeProcessing()
    {
    gettimeofday( &fLastAnnounceStart, NULL );
    fEventCounterSinceLastStart = 0;
    AliHLTDetectorPublisher::ResumeProcessing();
    }


void AliHLTDATEPublisher::EventFinished( AliEventID_t eventID, AliHLTSubEventDataDescriptor& sbevent, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    AliUInt32_t i, ndx;
    if ( !fBufferManager )
	{
	LOG( AliHLTLog::kError, "AliHLTDATEPublisherDisk::EventFinished", "No Buffer Manager" )
	    << "No buffer manager specified." << ENDLOG;
	}
    if ( !fShmManager )
	{
	LOG( AliHLTLog::kError, "AliHLTDATEPublisherDisk::EventFinished", "No Shm Manager" )
	    << "No shared memory manager specified." << ENDLOG;
	}
    AliUInt32_t baseLimit=0;
    if ( sbevent.fDataBlockCount>GetAdditionalBlockCount() )
	baseLimit = sbevent.fDataBlockCount-GetAdditionalBlockCount();
    for ( i = 0; i < sbevent.fDataBlockCount; i++ )
	{
	if ( i<baseLimit || !ReleaseAdditionalBlock( sbevent.fDataBlocks[i] ) )
	    {
	    if ( fBufferManager )
		{
		ndx = fBufferManager->GetBufferIndex( sbevent.fDataBlocks[i].fShmID );
		if ( ndx != ~(AliUInt32_t)0 )
		    fBufferManager->ReleaseBlock( ndx, sbevent.fDataBlocks[i].fBlockOffset );
		}
	    if ( fShmManager )
		fShmManager->ReleaseShm( sbevent.fDataBlocks[i].fShmID );
	    }
	}
    EventFinished( eventID, eventDoneData );
    }

void AliHLTDATEPublisher::EventFinished( AliEventID_t eventID, vector<AliHLTEventDoneData*>& )
    {
    pthread_mutex_lock( &fHLTEventTriggerDataLock );
    unsigned long ndx;
    if ( (fAliceHLT && !fDisableHLTEventTriggerData) || fHLTEventTriggerData.GetCnt()>0 ) // fHLTEventTriggerData.GetCnt()>0 means there are events in there, maybe because the trigger data was switched off after some time of operation.
	{
	if ( fHLTEventTriggerData.FindElement( eventID, ndx ) )
	    {
	    fHLTEventTriggerDataCache.Release( fHLTEventTriggerData.Get(ndx) );
	    fHLTEventTriggerData.Remove( ndx );
	    }
	else
	    {
	    AliHLTLog::TLogLevel lvl;
	    if ( !fDisableHLTEventTriggerData )
		lvl = AliHLTLog::kError;
	    else
		lvl = AliHLTLog::kWarning;
	    LOG( lvl, "AliHLTRORCPublisher::EventFinished", "Cannot find HLTEventTriggerData" )
		<< "Unable to find HLT event trigger data object for event 0x" 
		<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ")." << ENDLOG;
	    }
	}
    pthread_mutex_unlock( &fHLTEventTriggerDataLock );
    }


void AliHLTDATEPublisher::QuitEventLoop()
    {
    fQuitEventLoop = true;
    }

void AliHLTDATEPublisher::StartEventLoop()
    {
    fEventLoopQuitted = fQuitEventLoop = false;
    if ( fPersistentState )
	fEventCounter = fPersistentState->fLastEventID.fNr+(unsigned)1;
    }

void AliHLTDATEPublisher::EndEventLoop()
    {
    }



int AliHLTDATEPublisher::WaitForEvent( AliEventID_t& eventID, AliHLTSubEventDataDescriptor*& sbevent,
				       AliHLTEventTriggerStruct*& trg )
    {
    Tick( fEventCounter );
    sbevent = NULL;
    trg = NULL;
    bool blockOk = false;
    unsigned long offset=0, size=0;
    //fQuitEventLoop = false;
    //fEventLoopQuitted = false;
    AliUInt8_t* ptr = NULL;
    struct timeval now;
    //#define DO_BENCHMARK
#if __GNUC__>=3
    AliHLTShmID shmKey = { kInvalidShmType, {~(AliUInt64_t)0 } };
#else
    AliHLTShmID shmKey = { kInvalidShmType, {{~(AliUInt64_t)0 }} };
#endif

    if ( !fBufferManager )
	{
	LOG( AliHLTLog::kError, "AliHLTDATEPublisher::WaitForEvent", "No Buffer Manager" )
	    << "No buffer manager specified. Aborting..." << ENDLOG;
	return 0;
	}
    if ( !fShmManager )
	{
	LOG( AliHLTLog::kError, "AliHLTDATEPublisher::WaitForEvent", "No Shm Manager" )
	    << "No shared memory manager specified. Aborting..." << ENDLOG;
	// ...
	return 0;
	}
    if ( !fDescriptors )
	{
	LOG( AliHLTLog::kError, "AliHLTDATEPublisher::WaitForEvent", "No Descriptor Handler" )
	    << "No descriptor handler specified. Aborting..." << ENDLOG;
	return 0;
	}

    if ( !fQuitEventLoop )
	{
	while ( !fCreateEvents && !fQuitEventLoop )
	    {
	    usleep( 10000 );
	    //usleep( 0 );
	    }
	AliUInt32_t tmpCount=0;

	unsigned long long avg = 0xFFFFFFFFFFFFFFFFULL;
	do
	    {
	    unsigned long long tdiff;
	    gettimeofday( &now, NULL );
	    tdiff  = now.tv_sec-fLastAnnounceStart.tv_sec;
	    tdiff *= 1000000;
	    tdiff += now.tv_usec-fLastAnnounceStart.tv_usec;
	    if ( fEventCounterSinceLastStart!=0 )
		avg = tdiff / fEventCounterSinceLastStart;
	    // avg is average time between events since last start/resume
	    if ( avg < fEventTime && !fQuitEventLoop)
		usleep( 0 );
	    }
	while ( avg < fEventTime && !fQuitEventLoop );

	tmpCount = 0;
	struct timespec ts;
	AliUInt32_t bufferNdx;
	bool passed = false;
	do
	    {
	    if ( !fReader.NextEvent() )
		{
		if ( passed )
		    {
		    LOG( AliHLTLog::kError, "AliHLTDATEPublisher::WaitForEvent", "Cannot find event" )
			<< "Unable to read any event with size unequal 0." << ENDLOG;
		    return 0;
		    }
		passed = true;
		LOG( AliHLTLog::kInformational, "AliHLTDATEPublisher::WaitForEvent", "Cannot go to next event" )
		    << "Cannot go to next event. Resetting..." << ENDLOG;
		fReader.Reset();
		if ( !fReader.NextEvent() )
		    {
		    LOG( AliHLTLog::kError, "AliHLTDATEPublisher::WaitForEvent", "Error going to first event" )
			<< "Error going to first event." << ENDLOG;
		    return 0;
		    }
		}
	    size = fReader.GetEventSize();
	    LOG( AliHLTLog::kDebug, "AliHLTDATEPublisher::WaitForEvent", "Event size 0" )
		<< "Event size 0: " << AliHLTLog::kDec << size << " bytes." << ENDLOG;
	    }
	while ( !fQuitEventLoop && !size );
	do
	    {
	    if ( fQuitEventLoop )
		{
		offset = (AliUInt32_t)~0;
		break;
		}
	    blockOk = fBufferManager->GetBlock( bufferNdx, offset, size );
	    
	    if ( !blockOk )
		{
		UpdateEventAge();
		if ( !(tmpCount % fPollSleepModulo) )
		    {
		    //ts.tv_sec = ts.tv_nsec = 0;
		    //ts.tv_nsec = 1000000;
		    ts.tv_sec = fPollSleepTime/1000000;
		    ts.tv_nsec = (fPollSleepTime%1000000)*1000;
		    nanosleep( &ts, NULL );
		    UpdateEventAge();
		    fWaitSleepCount++;
		    }
		else
		    {
		    struct timeval tts, tte;
		    AliUInt32_t dtt;
		    gettimeofday( &tts, NULL );
		    do
			{
			gettimeofday( &tte, NULL );
			dtt = (tte.tv_sec-tts.tv_sec)*1000000+(tte.tv_usec-tts.tv_usec);
			}
		    while ( dtt < fBusyPollTime );
		    }
		}
	    }
	while ( !blockOk && tmpCount++<fGetBlockRetryCnt );
	if ( fQuitEventLoop )
	    {
	    fEventLoopQuitted = true;
	    return 0;
	    }
	
	if ( tmpCount > 1 )
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTDATEPublisher::WaitForEvent", "Wait states before event" )
		<< "Needed " << AliHLTLog::kDec << tmpCount << " waitstates before starting event... "
		<< ENDLOG;
	    }
	gettimeofday( &fLastEventStart, NULL );

	if ( blockOk )
	    shmKey = fBufferManager->GetShmKey( bufferNdx );
	else
	    {
	    shmKey.fShmType = kInvalidShmType;
	    shmKey.fKey.fID = ~(AliUInt64_t)0;
	    }
	if ( shmKey.fShmType != kInvalidShmType )
	    {
	    if ( fStatus )
		{
		AliUInt64_t tm;
		tm = fLastEventStart.tv_sec;
		tm *= 1000000;
		tm += fLastEventStart.tv_usec;
		pthread_mutex_lock( &fStatusMutex );
		fStatus->fHLTStatus.fCurrentEventStart = tm;
		pthread_mutex_unlock( &fStatusMutex );
		}
		
	    ptr = fShmManager->GetShm( shmKey );
	    if ( ptr )
		ptr += offset;
	    }
	else
	    ptr = NULL;
	unsigned long sz = 0;
	if ( ptr )
	    {
	    sz = fReader.GetEventSize();
	    LOG( AliHLTLog::kDebug, "AliHLTDATEPublisher::WaitForEvent", "Event size 1" )
		<< "Event size 1: " << AliHLTLog::kDec << sz << " bytes." << ENDLOG;
	    if ( sz > size )
		{
		LOG( AliHLTLog::kFatal, "AliHLTDATEPublisher::WaitForEvent", "INTERNAL ERROR" )
		    << "INTERNAL ERROR: sz>size (" << AliHLTLog::kDec << sz << ">" << size << ")." << ENDLOG;
		sz = size; // Should not happen...
		}
	    if ( !fReader.CopyEvent( ptr, sz ) )
		{
		LOG( AliHLTLog::kWarning, "AliHLTDATEPublisher::WaitForEvent", "Error Copying Data" )
		    << "Error copying " << AliHLTLog::kDec << sz << " bytes of data for event." << ENDLOG;
		sz = 0;
		}
	      
	    eventID = fEventCounter++;
	    fEventCounterSinceLastStart++;
	    
	    AliHLTDDLHeader ddlHeader;
	    if ( !ddlHeader.Set( (AliUInt32_t*)ptr ) )
		{
		LOG( AliHLTLog::kWarning, "AliHLTDATEPublisher::WaitForEvent", "Cannot read event ID" )
		    << "Cannot read event ID from data." << ENDLOG;
		}
	    else
		{
		eventID = ddlHeader.GetEventID();
		if ( eventID.fNr<=fLastEventID.fNr )
		    {
		    AliEventID_t tmpID( kAliEventTypeUnknown, 1 );
		    tmpID.fNr <<= 36;
		    fAddEventIDCounter.fNr += tmpID.fNr;
		    //fAddEventIDCounter &= 0xFFFFFFFFFF000000ULL;
		    }
		fLastEventID = eventID;
		eventID.fNr |= fAddEventIDCounter.fNr;
		}

	    pthread_mutex_lock( &fSEDDMutex );
	    sbevent = fDescriptors->GetFreeEventDescriptor( eventID, 1 );
	    pthread_mutex_unlock( &fSEDDMutex );

	    if ( !sbevent )
		{
		fEventCounter--;
		fEventCounterSinceLastStart--;
		}
	    }

	if ( sbevent )
	    {
	    gettimeofday( &now, NULL );
	    sbevent->fEventID = eventID;
	    sbevent->fEventBirth_s = now.tv_sec;
	    sbevent->fEventBirth_us = now.tv_usec;
	    sbevent->fOldestEventBirth_s = fOldestEventBirth;
	    sbevent->fDataBlocks[0].fBlockOffset = offset;
	    sbevent->fDataBlocks[0].fBlockSize = sz;
	    sbevent->fDataBlocks[0].fShmID.fShmType = shmKey.fShmType;
	    sbevent->fDataBlocks[0].fShmID.fKey.fID = shmKey.fKey.fID;
	    sbevent->fDataBlocks[0].fDataType.fID = fEventDataType.fID; //ADCCOUNTS_DATAID;
	    sbevent->fDataBlocks[0].fDataOrigin.fID = fEventDataOrigin.fID;
	    sbevent->fDataBlocks[0].fDataSpecification = fEventDataSpecification;
	    sbevent->fDataBlocks[0].fStatusFlags = 0;
	    AliHLTSubEventDescriptor::FillBlockAttributes( sbevent->fDataBlocks[0].fAttributes );
	    sbevent->fDataBlocks[0].fProducerNode = fNodeID;

	    sbevent->fDataType.fID = COMPOSITE_DATAID;
	    if ( fAliceHLT && !fDisableHLTEventTriggerData )
		{
		pthread_mutex_lock( &fHLTEventTriggerDataLock );
		AliHLTHLTEventTriggerData* hltTrg = fHLTEventTriggerDataCache.Get();
		if ( !hltTrg )
		    {
		    LOG( AliHLTLog::kError, "AliHLTDATEPublisher::GetHLTTriggerData", "out of memory (HLTEventTriggerData)" )
			<< "Out of memory trying to allocate HLT event trigger data object - using empty trigger data." << ENDLOG;
		    }
		else
		    {
		    fHLTEventTriggerData.Add( hltTrg, eventID );
		    memcpy( hltTrg->fCommonHeader, (AliUInt8_t const*)( ptr ), sizeof(hltTrg->fCommonHeader[0])*hltTrg->fCommonHeaderWordCnt );
		    FillHLTEventTriggerDataDDLList( hltTrg );
		    trg = &(hltTrg->fETS);
		    }
		pthread_mutex_unlock( &fHLTEventTriggerDataLock );
		}
	    if ( !trg )
		trg = fETP;


	    LOG( AliHLTLog::kDebug, "AliHLTDATEPublisher", "WaitForEvent" )
		<< "Copied " << AliHLTLog::kDec << sz << " bytes." << ENDLOG;
	    }
	else // of if ( offset == ~0 )
	    {
	    if ( !fQuitEventLoop )
		{
		unsigned long long tdiff;
		LOG( AliHLTLog::kInformational, "AliHLTDATEPublisher", "Getting shared memory block" )
		    << "Unable to get shared memory block of size " << AliHLTLog::kDec << size << " bytes."
		    << ENDLOG;
		gettimeofday( &now, NULL );
		tdiff = (now.tv_sec-fLastReleaseRequestTime.tv_sec)*1000000+(now.tv_usec-fLastReleaseRequestTime.tv_usec);
		if ( tdiff > fReleaseRequestTimeDiff )
		    {
		    RequestEventRelease();
		    fLastReleaseRequestTime = now;
		    }
		}
	    //PingActive();
	    }
	return 1;
	}
    else
	{
	fEventLoopQuitted = true;
	return 0;
	}
    }



/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTDATEPublisher.cpp 1402 2006-10-12 09:29:42Z timm $ 
**
***************************************************************************
*/
