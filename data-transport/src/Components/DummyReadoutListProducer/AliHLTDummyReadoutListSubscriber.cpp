/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTDummyReadoutListSubscriber.cpp 912 2006-04-10 11:14:35Z timm $ 
**
***************************************************************************
*/

//#define DEBUG

#include "AliHLTDummyReadoutListSubscriber.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTAlignment.h"


AliHLTDummyReadoutListSubscriber::AliHLTDummyReadoutListSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize,
								    AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, 
								    vector<ReadoutListData>& readoutLists, 
								    int eventCountAlloc ):
    AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc ),
    fEventCnt( 0ULL ),
    fReadoutLists( readoutLists ),
    fTotalWeightSum( 0 )
    {
    vector<ReadoutListData>::iterator iter, end;
    iter = fReadoutLists.begin();
    end = fReadoutLists.end();
    while ( iter != end )
	{
	fTotalWeightSum += iter->fWeight;
	fWeightSums.push_back( fTotalWeightSum );
	iter++;
	}
    }

// void AliHLTDummyReadoutListSubscriber::ProcessEvent( AliUInt8_t* dataIn, AliUInt32_t dataInSize,
// 				   AliUInt8_t* dataOut, AliUInt32_t dataOutMaxSize )
bool AliHLTDummyReadoutListSubscriber::ProcessEvent( AliEventID_t, AliUInt32_t, AliHLTSubEventDescriptor::BlockData*, const AliHLTSubEventDataDescriptor*, const AliHLTEventTriggerStruct*,
				   AliUInt8_t* outputPtr, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, AliHLTEventDoneData*& )
    {
    if ( size < AliHLTReadoutList::GetReadoutListSize( gReadoutListVersion ) )
	{
	LOG( AliHLTLog::kError, "AliHLTDummyReadoutListSubscriber::ProcessEvent", "Buffer too small" )
	    << "Output buffer too small. Need at least " << AliHLTLog::kDec 
	    << AliHLTReadoutList::GetReadoutListSize( gReadoutListVersion ) << " - Available: " << size << "." << ENDLOG;
	return false;
	}
    
    unsigned long nr = fWeightSums.size();
    unsigned long offset = fEventCnt % fTotalWeightSum;
    unsigned long ndx;
    for ( ndx=0; ndx<nr; ndx++ )
	{
	if ( offset<fWeightSums[ndx] )
	    break;
	}
    if ( ndx>=nr )
	{
	LOG( AliHLTLog::kFatal, "AliHLTDummyReadoutListSubscriber::ProcessEvent", "Internal Error" )
	    << "fEventCnt: " << AliHLTLog::kDec << fEventCnt << " - nr: " << nr 
	    << " - offset: " << offset << " - ndx: " << ndx << " - fTotalWeightSum: "
	    << fTotalWeightSum << "." << ENDLOG;
	return true; // Safety measure, should not happen
	}
    ReadoutListData& rld( fReadoutLists[ndx] );
    memcpy( outputPtr, rld.fData, AliHLTReadoutList::GetReadoutListSize( gReadoutListVersion ) );
    size = AliHLTReadoutList::GetReadoutListSize( gReadoutListVersion );
    
    AliHLTSubEventDescriptor::BlockData dataBlock;
    dataBlock.fShmKey.fShmType = kInvalidShmType;
    dataBlock.fShmKey.fKey.fID = (AliUInt64_t)-1;
    dataBlock.fOffset = 0;
    dataBlock.fSize = size;
    dataBlock.fProducerNode = fNodeID;
    dataBlock.fDataType.fID = HLTREADOUTLIST_DATAID;
    dataBlock.fDataOrigin.fID = HLT_DATAORIGIN;
    dataBlock.fDataSpecification = 0;
    dataBlock.fStatusFlags = 0;
    dataBlock.fAttributes[kAliHLTBlockDByteOrderAttributeIndex] = kAliHLTBigEndianByteOrder;
    dataBlock.fAttributes[kAliHLTBlockD64BitAlignmentAttributeIndex] = AliHLTDetermineUInt64Alignment();
    dataBlock.fAttributes[kAliHLTBlockD32BitAlignmentAttributeIndex] = AliHLTDetermineUInt32Alignment();
    dataBlock.fAttributes[kAliHLTBlockD16BitAlignmentAttributeIndex] = AliHLTDetermineUInt16Alignment();
    dataBlock.fAttributes[kAliHLTBlockD8BitAlignmentAttributeIndex] = AliHLTDetermineUInt8Alignment();
    dataBlock.fAttributes[kAliHLTBlockDFloatAlignmentAttributeIndex] = AliHLTDetermineDoubleAlignment();
    dataBlock.fAttributes[kAliHLTBlockDDoubleAlignmentAttributeIndex] = AliHLTDetermineFloatAlignment();
    outputBlocks.insert( outputBlocks.end(), dataBlock );


    fEventCnt++;
    return true;
    }

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTDummyReadoutListSubscriber.cpp 912 2006-04-10 11:14:35Z timm $ 
**
***************************************************************************
*/
