/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: DummyReadoutList.cpp 912 2006-04-10 11:14:35Z timm $ 
**
***************************************************************************
*/

#include "AliHLTProcessingSubscriber.hpp"
//#include "AliHLTControlledProcessComponent.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "AliHLTDummyReadoutListSubscriber.hpp"
#include "AliHLTReadoutList.hpp"
#include <errno.h>
#include <vector>

class DummyReadoutListComponent: public AliHLTControlledProcessorComponent
    {
    public:

	DummyReadoutListComponent( const char* name, int argc, char** argv );
	virtual ~DummyReadoutListComponent() {};


    protected:

	virtual bool CheckConfiguration();
	virtual bool CreateParts();
	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

	virtual AliHLTProcessingSubscriber* CreateSubscriber();

	std::vector<AliHLTDummyReadoutListSubscriber::ReadoutListData> fReadoutLists;

	unsigned long fWeight;
	AliHLTReadoutList *fReadoutList;
	AliHLTReadoutListData fReadoutListData;
	bool fReadoutListStarted;

	unsigned long fAddListCmdCnt;

	struct TReadoutListCommands
	    {
		TReadoutListCommands( const char* cmd ):fCmd(cmd){}
		TReadoutListCommands( const char* cmd, const char* param ):fCmd(cmd),fParam0(param){}
		TReadoutListCommands( const char* cmd, const char* param0, const char* param1 ):fCmd(cmd),fParam0(param0),fParam1(param1){}
		TReadoutListCommands( const char* cmd, const char* param0, const char* param1, const char* param2 ):fCmd(cmd),fParam0(param0),fParam1(param1),fParam2(param2){}
		MLUCString fCmd;
		MLUCString fParam0;
		MLUCString fParam1;
		MLUCString fParam2;
	    };
	std::vector<TReadoutListCommands> fReadoutListCommands;

    private:
    };


DummyReadoutListComponent::DummyReadoutListComponent( const char* name, int argc, char** argv ):
    AliHLTControlledProcessorComponent( name, argc, argv ),
    fWeight( 1 ),
    fReadoutList( NULL ), // ( fReadoutListData ),
    fReadoutListStarted( false ),
    fAddListCmdCnt( 0 )
    {
    //fReadoutList.Reset();
    }

bool DummyReadoutListComponent::CheckConfiguration()
    {
    if ( !AliHLTControlledProcessorComponent::CheckConfiguration() )
	return false;
    if ( fAddListCmdCnt<=0 )
	{
	LOG( AliHLTLog::kError, "DummyReadoutListComponent::CheckConfiguration", "No readout list" )
	    << "Must specify at least one readout list to be used (e.g. assembled through the -detectorword, -alldetector, and -all options, and finalised with the -addlist option."
	    << ENDLOG;
	return false;
	}
    if ( fReadoutListStarted )
	{
	LOG( AliHLTLog::kError, "DummyReadoutListComponent::CheckConfiguration", "Readout list started" )
	    << "A readout list was started to be assembled but not finalised with the -addlist option."
	    << ENDLOG;
	return false;
	}
    return true;
    }	

bool DummyReadoutListComponent::CreateParts()
    {
    fReadoutList = new AliHLTReadoutList( gReadoutListVersion, fReadoutListData );
    if ( !fReadoutList )
	{
	LOG( AliHLTLog::kError, "DummyReadoutListComponent::CreateParts", "Error creating readout list object" )
	    << "Error creating readout list object. (Out of memory?)." << ENDLOG;
	return false;
	}
    if ( !fReadoutList->Reset() )
	{
	LOG( AliHLTLog::kError, "DummyReadoutListComponent::CreateParts", "Error resetting readout list" )
	    << "Error resetting readout list initially." << ENDLOG;
	return false;
	}
    std::vector<TReadoutListCommands>::iterator iter, end;
    iter = fReadoutListCommands.begin();
    end = fReadoutListCommands.end();
    char* cerr=NULL;
    while ( iter != end )
	{
	if ( iter->fCmd=="-listweight" )
	    {
	    fWeight = strtoul( iter->fParam0.c_str(), &cerr, 0 );
	    if ( *cerr )
		{
		LOG( AliHLTLog::kError, "DummyReadoutListComponent::CreateParts", "Error converting weight specifier" )
		    << "Error converting list weight specifier '" << iter->fParam0.c_str() << "'." << ENDLOG;
		return false;
		}
	    }
	else if ( iter->fCmd=="-addlist" )
	    {
	    AliHLTDummyReadoutListSubscriber::ReadoutListData rld;
	    rld.fWeight = fWeight;
#if 0
	    if ( (kAliHLTNativeByteOrder!=kAliHLTBigEndianByteOrder) && !fReadoutList.RevertByteOrder() )
		return "Error reverting readout list byte order";
#endif
	    memcpy( rld.fData, fReadoutListData, sizeof(fReadoutListData) );
	    fReadoutLists.push_back( rld );
	    if ( !fReadoutList->Reset() )
		{
		LOG( AliHLTLog::kError, "DummyReadoutListComponent::CreateParts", "Error resetting readout list" )
		    << "Error resetting readout list." << ENDLOG;
		return false;
		}
	    }
	else if ( iter->fCmd=="-all" )
	    {
	    for ( unsigned id=(unsigned)kFIRSTID; id<(unsigned)kENDID; id++ )
		{
		for ( unsigned ddl=0; ddl<AliHLTReadoutList::GetReadoutListDetectorDDLCount( gReadoutListVersion, (TDetectorID)id ); ddl++ )
		    {
		    if ( !fReadoutList->SetDDL( (TDetectorID)id, ddl ) )	
			{
			LOG( AliHLTLog::kError, "DummyReadoutListComponent::CreateParts", "Error setting DDL bit in readout list" )
			    << "Error setting DDL bits in readout list for detector '" << AliHLTReadoutList::GetDetectorName( (TDetectorID)id )
			    << "'." << ENDLOG;
			return false;
			}
		    }
		}
	    }
	else if ( iter->fCmd=="-alldetector" )
	    {
	    bool found=false;
	    for ( unsigned id=(unsigned)kFIRSTID; id<(unsigned)kENDID; id++ )
		{
		if ( iter->fParam0==gkDetectorNames[id] ) // if ( !strcmp( gkDetectorNames[id], iter->fParam0 ) )
		    {
		    found = true;
		    for ( unsigned ddl=0; ddl<AliHLTReadoutList::GetReadoutListDetectorDDLCount( gReadoutListVersion, (TDetectorID)id ); ddl++ )
			{
			if ( !fReadoutList->SetDDL( (TDetectorID)id, ddl ) )
			    {
			    LOG( AliHLTLog::kError, "DummyReadoutListComponent::CreateParts", "Error setting DDL bit in readout list" )
				<< "Error setting DDL bits in readout list for detector '" << AliHLTReadoutList::GetDetectorName( (TDetectorID)id )
				<< "'." << ENDLOG;
			    return false;
			    }
			}
		    break;
		    }
		}
	    if ( !found )
		{
		LOG( AliHLTLog::kError, "DummyReadoutListComponent::CreateParts", "Unknown detector specifier" )
		    << "Unknown detector specifier '" << iter->fParam0.c_str()
		    << "'." << ENDLOG;
		return false;
		}
	    }
	else if ( iter->fCmd=="-detectorword" )
	    {
	    unsigned long word = strtoul( iter->fParam1.c_str(), &cerr, 0 );
	    if ( *cerr )
		{
		LOG( AliHLTLog::kError, "DummyReadoutListComponent::CreateParts", "Error converting detector word specifier" )
		    << "Error converting detector word specifier '" << iter->fParam1.c_str()
		    << "'." << ENDLOG;
		return false;
		}	
	    unsigned long value = strtoul( iter->fParam2.c_str(), &cerr, 0 );
	    if ( *cerr )
		{
		LOG( AliHLTLog::kError, "DummyReadoutListComponent::CreateParts", "Error converting detector word value" )
		    << "Error converting detector word value '" << iter->fParam2.c_str()
		    << "'." << ENDLOG;
		return false;
		}
	    bool found=false;
	    for ( unsigned id=(unsigned)kFIRSTID; id<(unsigned)kENDID; id++ )
		{
		if ( iter->fParam0==gkDetectorNames[id] ) // if ( !strcmp( gkDetectorNames[id], fParam0.c_str() ) )
		    {
		    found = true;
		    if ( word>=AliHLTReadoutList::GetReadoutListDetectorWordCount( gReadoutListVersion, (TDetectorID)id ) )
			{
			LOG( AliHLTLog::kError, "DummyReadoutListComponent::CreateParts", "Detector word value too large" )
			    << "Detector word specifier " << AliHLTLog::kDec << word << " too large for detector '"
			    << iter->fParam0.c_str()
			    << "'." << ENDLOG;
			return false;
			}	
		    if ( !fReadoutList->SetReadoutListMask( (TDetectorID)id, word, value ) )
			{
			LOG( AliHLTLog::kError, "DummyReadoutListComponent::CreateParts", "Error setting detector word" )
			    << "Error setting detector '" << iter->fParam0.c_str()
			    << "' word " << AliHLTLog::kDec << word << " value to " << value << "." << ENDLOG;
			return false;
			}
		    break;
		    }
		}
	    if ( !found )
		{
		LOG( AliHLTLog::kError, "DummyReadoutListComponent::CreateParts", "Unknown detector specifier" )
		    << "Unknown detector specifier '" << iter->fParam0.c_str()
		    << "'." << ENDLOG;
		return false;
		}
	    }
	++iter;
	}
    return AliHLTControlledProcessorComponent::CreateParts();
    }



const char* DummyReadoutListComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cerr = NULL;
    if ( !strcmp( argv[i], "-listweight" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing weight specifier.";
	    }
	fWeight = strtoul( argv[i+1], &cerr, 0 );
	if ( *cerr )
	    {
	    errorArg = i+1;
	    return "Error converting weight specifier.";
	    }
	fReadoutListCommands.push_back( TReadoutListCommands(argv[i],argv[i+1]) );
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-addlist" ) )
	{
#if 0
	AliHLTDummyReadoutListSubscriber::ReadoutListData rld;
	rld.fWeight = fWeight;
#if 0
	if ( (kAliHLTNativeByteOrder!=kAliHLTBigEndianByteOrder) && !fReadoutList.RevertByteOrder() )
	    return "Error reverting readout list byte order";
#endif
	memcpy( rld.fData, fReadoutListData, sizeof(fReadoutListData) );
	fReadoutLists.push_back( rld );
	if ( !fReadoutList.Reset() )
	    return "Error resetting readout list";
#endif
	fReadoutListCommands.push_back( TReadoutListCommands(argv[i]) );
	fReadoutListStarted = false;
	++fAddListCmdCnt;
	i += 1;
	return NULL;
	}
    if ( !strcmp( argv[i], "-all" ) )
	{
#if 0
	for ( unsigned id=(unsigned)kFIRSTID; id<(unsigned)kENDID; id++ )
	    {
	    for ( unsigned ddl=0; ddl<AliHLTReadoutList::GetReadoutListDetectorDDLCount( (TDetectorID)id ); ddl++ )
		{
		if ( !fReadoutList.SetDDL( (TDetectorID)id, ddl ) )	
		    return "Error setting DDL bit in readout list";
		}
	    }
#endif
	fReadoutListCommands.push_back( TReadoutListCommands(argv[i]) );
	fReadoutListStarted = true;
	i += 1;
	return NULL;
	}
    if ( !strcmp( argv[i], "-alldetector" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing detector specifier.";
	    }
#if 0
	bool found=false;
	for ( unsigned id=(unsigned)kFIRSTID; id<(unsigned)kENDID; id++ )
	    {
	    if ( !strcmp( gkDetectorNames[id], argv[i+1] ) )
		{
		found = true;
		for ( unsigned ddl=0; ddl<AliHLTReadoutList::GetReadoutListDetectorDDLCount( (TDetectorID)id ); ddl++ )
		    {
		    if ( !fReadoutList.SetDDL( (TDetectorID)id, ddl ) )
			return "Error setting DDL bit in readout list";
		    }
		break;
		}
	    }
	if ( !found )
	    {
	    errorArg = i+1;
	    return "Unknown detector specifier.";
	    }
#endif
	fReadoutListCommands.push_back( TReadoutListCommands(argv[i],argv[i+1]) );
	fReadoutListStarted = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-detectorword" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing detector specifier.";
	    }
	if ( argc <= i +2 )
	    {
	    return "Missing detector word specifier.";
	    }
	if ( argc <= i +3 )
	    {
	    return "Missing detector word value.";
	    }
	/*unsigned long word =*/ strtoul( argv[i+2], &cerr, 0 );
	if ( *cerr )
	    {
	    errorArg = i+2;
	    return "Error converting detector word specifier.";
	    }	
	/*unsigned long value =*/ strtoul( argv[i+3], &cerr, 0 );
	if ( *cerr )
	    {
	    errorArg = i+3;
	    return "Error converting detector word value.";
	    }
#if 0	
	bool found=false;
	for ( unsigned id=(unsigned)kFIRSTID; id<(unsigned)kENDID; id++ )
	    {
	    if ( !strcmp( gkDetectorNames[id], argv[i+1] ) )
		{
		found = true;
		if ( word>=AliHLTReadoutList::GetReadoutListDetectorWordCount( (TDetectorID)id ) )
		    {
		    errorArg = i+2;
		    return "Detector word specifier too large for detector.";
		    }	
		if ( !fReadoutList.SetReadoutListMask( (TDetectorID)id, word, value ) )
		    return "Error setting detector word in readout list";
		break;
		}
	    }
	if ( !found )
	    {
	    errorArg = i+1;
	    return "Unknown detector specifier.";
	    }
#endif
	fReadoutListCommands.push_back( TReadoutListCommands(argv[i],argv[i+1],argv[i+2],argv[i+3]) );
	fReadoutListStarted = true;
	i += 4;
	return NULL;
	}
    return AliHLTControlledProcessorComponent::ParseCmdLine( argc, argv, i, errorArg );
    }

void DummyReadoutListComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    AliHLTControlledProcessorComponent::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-listweight: Set the relative weight of the next readout list to add. (Optional, can be given multiple times) " << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-addlist: Add the readout list configured so far to the list of readout lists. (Mandatory, can be given multiple times) " << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-all: Set all DDL bits for all detectors to on. (Optional) " << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-alldetector <detector-id>: Set all DDL bits for the given detector to on. (Optional) " << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-detectorword <detector-id> <detector-word-index> <detector-word-value>: Set the readout list word with the given index for the given detector to the given value. (Optional) " << ENDLOG;
    MLUCString detIDs;
    for ( unsigned id=(unsigned)kFIRSTID; id<(unsigned)kMAXID; id++ )
	{
	detIDs += gkDetectorNames[id];
	detIDs += ", ";
	}
    detIDs += gkDetectorNames[kMAXID];
    
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< " Allowed detector IDs are: " << detIDs.c_str() << ENDLOG;
    }

AliHLTProcessingSubscriber* DummyReadoutListComponent::CreateSubscriber()
    {
    return new AliHLTDummyReadoutListSubscriber( fSubscriberID.c_str(), fSendEventDone, fMinBlockSize, 
						 fPerEventFixedSize, fPerBlockFixedSize, fSizeFactor, 
						 fReadoutLists, fMaxPreEventCountExp2 );
    }


int main( int argc, char** argv )
    {
    DummyReadoutListComponent procComp( "DummyReadoutList", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }


/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: DummyReadoutList.cpp 912 2006-04-10 11:14:35Z timm $ 
**
***************************************************************************
*/

