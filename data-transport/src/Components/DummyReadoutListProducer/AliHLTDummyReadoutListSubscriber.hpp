/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIHLTDUMMYREADOUTLISTSUBSCRIBER_HPP_
#define _ALIHLTDUMMYREADOUTLISTSUBSCRIBER_HPP_ 

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTDummyReadoutListSubscriber.hpp 912 2006-04-10 11:14:35Z timm $ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliHLTProcessingSubscriber.hpp"
#include <AliHLTReadoutList.hpp>


class AliHLTDummyReadoutListSubscriber: public AliHLTProcessingSubscriber
    {
    public:

	struct ReadoutListData
	    {
		unsigned fWeight;
		AliHLTReadoutListData fData;
	    };

	AliHLTDummyReadoutListSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize, 
					  AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, 
					  vector<ReadoutListData>& readoutLists, 
					  int eventCountAlloc = -1 );
	
	virtual bool ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* etsp,
				   AliUInt8_t* outputPtr, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, AliHLTEventDoneData*& edd );
    protected:

	unsigned long long fEventCnt;
	std::vector<ReadoutListData> fReadoutLists;
	std::vector<unsigned long> fWeightSums;
	unsigned long fTotalWeightSum;

    private:
    };






/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTDummyReadoutListSubscriber.hpp 912 2006-04-10 11:14:35Z timm $ 
**
***************************************************************************
*/

#endif // _ALIHLTDUMMYREADOUTLISTSUBSCRIBER_HPP_
