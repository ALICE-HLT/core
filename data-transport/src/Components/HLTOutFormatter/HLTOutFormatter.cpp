/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: HLTOutFormatter.cpp 905 2006-04-10 11:12:18Z timm $ 
**
***************************************************************************
*/

#include "AliHLTOutFormatter.hpp"
//#include "AliHLTControlledProcessComponent.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTLog.hpp"


class HLTOutFormatterComponent: public AliHLTControlledProcessorComponent
    {
    public:

	HLTOutFormatterComponent( const char* name, int argc, char** argv );
	virtual ~HLTOutFormatterComponent() {};


    protected:

	//virtual AliHLTProcessingSubscriber* CreateSubscriber();

	virtual AliHLTProcessingSubscriber* CreateSubscriber();

	virtual bool SetupComponents();

	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

	bool fBroadcastEventMaster;

	std::vector<MLUCString> fReadoutListAdditionalDetectors;

	unsigned long long fReadoutListOutputReduction;

    private:
    };


HLTOutFormatterComponent::HLTOutFormatterComponent( const char* name, int argc, char** argv ):
    AliHLTControlledProcessorComponent( name, argc, argv ),
    fBroadcastEventMaster(false)
    {
    fReadoutListOutputReduction = 0;
    }


// AliHLTProcessingSubscriber* HLTOutFormatterComponent::CreateSubscriber()
//     {
//     return new AliHLTHLTOutFormatterSubscriber( fSubscriberName.c_str(), fSendEventDone, fMinBlockSize, fLoadType, 
// 					 fProcTimeFunc, fProcTimeFuncParam, fDataOutputRate, fMaxPreEventCountExp2 );
//     }


AliHLTProcessingSubscriber* HLTOutFormatterComponent::CreateSubscriber()
    {
    return new AliHLTHLTOutFormatterSubscriber( fSubscriberID.c_str(), fSendEventDone, fMinBlockSize, 
						fPerEventFixedSize, fPerBlockFixedSize, fSizeFactor, fMaxPreEventCountExp2 );
    }

bool HLTOutFormatterComponent::SetupComponents()
    {
    fSecondary = false;
    if ( !AliHLTControlledProcessorComponent::SetupComponents() )
	return false;
    reinterpret_cast<AliHLTHLTOutFormatterSubscriber*>( fSubscriber )->SetupHOMERInterface();
    reinterpret_cast<AliHLTHLTOutFormatterSubscriber*>( fSubscriber )->BroadcastEventMaster( fBroadcastEventMaster );
    reinterpret_cast<AliHLTHLTOutFormatterSubscriber*>( fSubscriber )->SetReadoutListAdditionalDetectors( fReadoutListAdditionalDetectors );
    if ( fReadoutListOutputReduction )
	reinterpret_cast<AliHLTHLTOutFormatterSubscriber*>( fSubscriber )->SetReadoutListOutputReduction( fReadoutListOutputReduction );
    return true;
    }

const char* HLTOutFormatterComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cpErr = NULL;
    if ( !strcmp( argv[i], "-broadcasteventmaster" ) )
	{
	fBroadcastEventMaster = true;
	++i;
	return NULL;
	}
    if ( !strcmp( argv[i], "-setdetectorddls" ) )
	{
	if ( argc <= i+1 )
	    return "Missing detector ID parameter";
	TDetectorID id;
	for ( id=kFIRSTID; id<kENDID; id=(TDetectorID)((unsigned)id+1) )
	    {
	    if ( !strcmp( gkDetectorNames[id], argv[i+1] ) )
		break;
	    }
	if ( id > kMAXID )
	    {
	    errorArg = i+1;
	    return "Unknown detector ID parameter";
	    }
	fReadoutListAdditionalDetectors.push_back( MLUCString(argv[i+1]) );
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-readoutlistoutput" ) )
	{
	if ( argc <= i+1 )
	    return "Missing readout list output reduction specifier.";
	fReadoutListOutputReduction = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting readout list output reduction specifier.";
	    }
	i += 2;
	return NULL;
	}
    return AliHLTControlledProcessorComponent::ParseCmdLine( argc, argv, i, errorArg );
    }

void HLTOutFormatterComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    AliHLTControlledProcessorComponent::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-broadcasteventmaster: Specify that this component is the master among equal partners of the same type. (Optional, should be activated only for one component instance)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-setdetectorddls <detector-ID>: Specify a detector ID for which all DDLs should b set in the readout list. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-readoutlistoutput <readout list output reduction specifier>: Log the readout list for events, parameters is a reduction number (first N events are shown, then N*2, N*4, N*8, ...). (Optional)" << ENDLOG;
    }


int main( int argc, char** argv )
    {
    HLTOutFormatterComponent procComp( "HLTOutFormatter", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }



/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: HLTOutFormatter.cpp 905 2006-04-10 11:12:18Z timm $ 
**
***************************************************************************
*/

