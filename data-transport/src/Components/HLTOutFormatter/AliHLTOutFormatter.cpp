/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTHLTOutFormatter.cpp 905 2006-04-10 11:12:18Z timm $ 
**
***************************************************************************
*/

//#define DEBUG

#include "AliHLTOutFormatter.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTDDLHeaderData.h"
#include "AliHLTDDLHeader.hpp"
#include "AliHLTHLTEventTriggerData.hpp"
#include "AliHLTHOMERInterface.hpp"
#include "BCLNetworkData.hpp"
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#ifdef DEBUG
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#endif

#define HLTOUTDATA_FORMAT_VERSION 2


AliHLTHLTOutFormatterSubscriber::AliHLTHLTOutFormatterSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize,
								  AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, int eventCountAlloc ):
    AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc ),
    fBroadcastEventMaster(false)
    {
    fReadoutListOutputCnt = 0;
    fReadoutListOutputReduction = 0;
    fReadoutListOutputReductionFactor = 1;
    }

bool AliHLTHLTOutFormatterSubscriber::ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, 
						    const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* ets,
						    AliUInt8_t* outputPtr, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, 
						    AliHLTEventDoneData*& )
    {
    // HLT decision / readout list block
    bool readoutListBlockFound = false;
    unsigned long readoutListFirstBlockNdx=~(unsigned long)0;
    unsigned long ndx;
    bool dataFound = false;
    bool hltBitSet = false;
    bool originalDataBitSet = false;

    if ( eventID.fType != kAliEventTypeStartOfRun && eventID.fType != kAliEventTypeData && eventID.fType != kAliEventTypeEndOfRun && eventID.fType != kAliEventTypeSync )
	{
	LOG( AliHLTLog::kInformational, "AliHLTHLTOutFormatterSubscriber::ProcessEvent", "Non-(SOR/Data/EOR/SYNC) Event" )
	    << "Non-(SOR/Data/EOR/SYNC) Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") will not be formatted." << ENDLOG;
	size = 0;
	return true;
	}

    bool readoutListOutput=false;
    if ( eventID.fType==kAliEventTypeData )
	{
	if ( fReadoutListOutputReduction && ((fReadoutListOutputCnt<fReadoutListOutputReduction) || ((fReadoutListOutputCnt % fReadoutListOutputReduction*fReadoutListOutputReductionFactor)==0)) )
	    {
	    readoutListOutput=true;
	    if ( fReadoutListOutputCnt>=fReadoutListOutputReduction )
		fReadoutListOutputReductionFactor *= 2;
	    }
	++fReadoutListOutputCnt;
	      
	// fReadoutListOutputCnt = 0;
	// fReadoutListOutputReduction = 1;
	}


    if ( ( (eventID.fType!=kAliEventTypeStartOfRun) && (eventID.fType!=kAliEventTypeEndOfRun) && (eventID.fType!=kAliEventTypeSync) ) || fBroadcastEventMaster )
	{
	for ( ndx = 0; ndx < blockCnt; ndx++ )
	    {
	    if ( blocks[ndx].fDataType.fID==HLTREADOUTLIST_DATAID && eventID.fType!=kAliEventTypeStartOfRun && eventID.fType!=kAliEventTypeEndOfRun && eventID.fType!=kAliEventTypeSync )
		{
		unsigned rlv = AliHLTReadoutList::GetReadoutListVersion( (AliVolatileUInt32_t*)blocks[ndx].fData );
		if ( rlv )
		    {
		    // Only accept readout list if version can be identified by size
		    originalDataBitSet = blocks[ndx].fDataSpecification & 0x1;

		    readoutListBlockFound = true;
		    readoutListFirstBlockNdx = ndx;
		    bool tmp=false;
		    AliHLTReadoutList readoutListObj( rlv, (AliVolatileUInt32_t*)blocks[ndx].fData );
		    readoutListObj.GetDetectorDDLs( "HLT", tmp );
		    if ( tmp )
			hltBitSet = true;
		    if ( readoutListOutput )
			{
			AliVolatileUInt32_t* readoutListData = (AliVolatileUInt32_t*)blocks[ndx].fData;
			MLUCString msg;
			for ( unsigned nn=0; nn<readoutListObj.GetReadoutListSize(); nn += sizeof(hltreadout_uint32_t) )
			    {
			    char tmp[256];
			    snprintf( tmp, 256, "%02u : 0x%08X", nn, readoutListData[nn] );
			    if ( nn )
				msg += " - ";
			    msg += tmp;
			    }
			LOG( AliHLTLog::kImportant, "AliHLTHLTOutFormatterSubscriber::ProcessEvent", "Input readout list" )
			    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << ") readout list: " << msg.c_str() << "." << ENDLOG;
			}
		    }
		else
		    {
		    LOG( AliHLTLog::kError, "AliHLTHLTOutFormatterSubscriber::ProcessEvent", "Unable to identify readout list" )
			<< "Unable to identify readout list version from word count "
			<< AliHLTLog::kDec << ((AliVolatileUInt32_t*)blocks[ndx].fData)[0] << "." << ENDLOG;
		    }
		}
	    else 
		dataFound = true;
	    }
	
	if ( ets->fDataWordCount>0 )
	    dataFound = true;
	}

    if ( ( (eventID.fType==kAliEventTypeStartOfRun) || (eventID.fType==kAliEventTypeEndOfRun) || (eventID.fType==kAliEventTypeSync) ) && !fBroadcastEventMaster )
	dataFound = false;

    if ( !hltBitSet && eventID.fType!=kAliEventTypeStartOfRun && eventID.fType!=kAliEventTypeEndOfRun && eventID.fType!=kAliEventTypeSync ) // No HLT Bit set in any readout list received is the signal to suppress any HLT output data payload.
	dataFound = false;

    AliUInt32_t dataSize = 0;
    fHOMERInterface.Clear();
    if ( dataFound )
	{
	fHOMERInterface.SetEvent( eventID, ets, sedd );
	dataSize = fHOMERInterface.GetSize();
	}


    void* cdh = outputPtr;
    AliHLTSetDDLHeaderVersion( cdh, DDL_HEADER_DEFAULT_VERSION ); // Done again below
    AliUInt8_t* hltHeader = ((AliUInt8_t*)cdh)+AliHLTGetDDLHeaderSize(cdh);
    unsigned long hltHeaderSize = sizeof(AliUInt32_t)*4;
    unsigned long readoutListSize = 0;
    if ( readoutListBlockFound )
	readoutListSize = AliHLTReadoutList::GetReadoutListSize( gReadoutListVersion ); //  blocks[readoutListFirstBlockNdx].fSize;
    AliUInt8_t* readoutList = hltHeader + hltHeaderSize;
    AliUInt8_t* data = readoutList + readoutListSize;
    AliUInt32_t totalPayloadSize = dataSize+readoutListSize+hltHeaderSize;
    // 32 bit alignment
    if ( totalPayloadSize & 0x3 )
	totalPayloadSize += ( 4-(totalPayloadSize & 0x3) );
    AliUInt32_t totalSize = totalPayloadSize+AliHLTGetDDLHeaderSize(cdh);

    if ( totalSize > size )
	{
	if ( fBufferManager && fBufferManager->IsBlockFeasible( totalSize ) )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTHLTOutFormatterSubscriber::ProcessEvent", "Event too big" )
		<< "Event 0x" << AliHLTLog::kHex << (AliUInt64_t)eventID << " (" << AliHLTLog::kDec
		<< (AliUInt64_t)eventID << ") too big (" << totalSize << " bytes). Scheduling for retry."
		<< ENDLOG;
	    return false;
	    }
	else
	    {
	    LOG( AliHLTLog::kError, "AliHLTHLTOutFormatterSubscriber::ProcessEvent", "Event too big" )
		<< "Event 0x" << AliHLTLog::kHex << (AliUInt64_t)eventID << " (" << AliHLTLog::kDec
		<< (AliUInt64_t)eventID << ") permanently impossible to store. (" << totalSize << " bytes). Aborting."
		<< ENDLOG;
	    return true;
	    }
	}

    AliUInt64_t cdhStatusError = 0;
    if ( ets->fHeader.fLength != sizeof(AliHLTHLTEventTriggerData) )
	{
	LOG( AliHLTLog::kWarning, "AliHLTHLTOutFormatterSubscriber::ProcessEvent", "No Trigger Information" )
	    << "Event 0x" << AliHLTLog::kHex << (AliUInt64_t)eventID << " (" << AliHLTLog::kDec
	    << (AliUInt64_t)eventID << "): Trigger information has wrong size (expected: " << AliHLTLog::kDec << sizeof(AliHLTHLTEventTriggerData)
	    << " bytes - received: " << ets->fHeader.fLength << " bytes). Creating new Common Data Header" << ENDLOG;
	memset( cdh, 0, AliHLTGetDDLHeaderVersionSize(DDL_HEADER_DEFAULT_VERSION) );
	AliHLTSetDDLHeaderVersion( cdh, DDL_HEADER_DEFAULT_VERSION );
	AliHLTSetDDLHeaderEventID1( cdh, ((AliUInt64_t)eventID) & 0x00000FFFULL );
	AliHLTSetDDLHeaderMiniEventID( cdh, ((AliUInt64_t)eventID) & 0x00000FFFULL );
	AliHLTSetDDLHeaderEventID2( cdh, (((AliUInt64_t)eventID) >> 12) & 0x00FFFFFFULL );
	}
    else
	{
	memcpy( cdh, &(((AliHLTHLTEventTriggerData*)ets)->fCommonHeader), AliHLTGetDDLHeaderSize(&(((AliHLTHLTEventTriggerData*)ets)->fCommonHeader)) );
	// CDH status & error not copied, as this should have only those bits we excplicitly set and not copy anything from the detectors.
	// Same for block attributes
	}
    if ( eventID.fType == kAliEventTypeStartOfRun )
	{
	AliHLTSetDDLHeaderL1TriggerType( cdh, (AliUInt32_t)(0x38|0x01|AliHLTGetDDLHeaderL1TriggerType(cdh)) );
	originalDataBitSet = true;
	}
    if ( eventID.fType == kAliEventTypeEndOfRun )
	{
	AliHLTSetDDLHeaderL1TriggerType( cdh, (AliUInt32_t)(0x3C|0x01|AliHLTGetDDLHeaderL1TriggerType(cdh)) );
	originalDataBitSet = true;
	}
    if ( eventID.fType == kAliEventTypeSync )
	{
	AliHLTSetDDLHeaderL1TriggerType( cdh, (AliUInt32_t)(0x34|0x01|AliHLTGetDDLHeaderL1TriggerType(cdh)) );
	originalDataBitSet = true;
	}
    AliHLTSetDDLHeaderBlockLength( cdh, (AliUInt32_t)totalSize );
    AliHLTSetDDLHeaderBlockAttr( cdh, 0 ); // Set block attributes to 0

    AliHLTSetDDLHeaderMiniEventID( cdh, AliHLTGetDDLHeaderEventID1( cdh ) );

    LOG( AliHLTLog::kDebug, "AliHLTHLTOutFormatterSubscriber::ProcessEvent", "Status word 1" )
	<< "CDH status word: 0x" << AliHLTLog::kHex << cdhStatusError << ENDLOG;
    if ( readoutListBlockFound )
	cdhStatusError |= 0x40;

    LOG( AliHLTLog::kDebug, "AliHLTHLTOutFormatterSubscriber::ProcessEvent", "Status word 2" )
	<< "CDH status word: 0x" << AliHLTLog::kHex << cdhStatusError << ENDLOG;

    if ( dataFound )
	cdhStatusError |= 0x80;

    LOG( AliHLTLog::kDebug, "AliHLTHLTOutFormatterSubscriber::ProcessEvent", "Status word 3" )
	<< "CDH status word: 0x" << AliHLTLog::kHex << cdhStatusError << ENDLOG;

    if ( originalDataBitSet )
        cdhStatusError |= 0x8000;

    AliHLTSetDDLHeaderStatusError( cdh, cdhStatusError );
    //{ (((AliUInt32_t*)cdh)[ 4 ] = (AliUInt32_t)( (cdhStatusError << 12) & 0x0FFFF000) | ((AliUInt32_t*)cdh)[ 4 ] & 0x00000FFF); }

#if 0
    BCLNetworkData::Transform( totalPayloadSize, *(AliUInt32_t*)hltHeader, kAliHLTNativeByteOrder, kAliHLTNetworkByteOrder );
    BCLNetworkData::Transform( (AliUInt32_t)HLTOUTDATA_FORMAT_VERSION, *(((AliUInt32_t*)hltHeader)+1), kAliHLTNativeByteOrder, kAliHLTNetworkByteOrder );
    BCLNetworkData::Transform( (AliUInt64_t)eventID.fNr, *(AliUInt64_t*)(((AliUInt32_t*)hltHeader)+2), kAliHLTNativeByteOrder, kAliHLTNetworkByteOrder );
#else
    *(AliUInt32_t*)hltHeader = totalPayloadSize;
#if 0
    *(((AliUInt32_t*)hltHeader)+1) = (AliUInt32_t)HLTOUTDATA_FORMAT_VERSION;
#else
    *(((AliUInt32_t*)hltHeader)+1) = (AliUInt32_t)gReadoutListVersion;
#endif
#if 0
    *(AliUInt64_t*)(((AliUInt32_t*)hltHeader)+2) = (AliUInt64_t)eventID.fNr;
#else
    *(((AliUInt32_t*)hltHeader)+2) = (AliUInt32_t)(((AliUInt64_t)eventID.fNr) >> 32);
    *(((AliUInt32_t*)hltHeader)+3) = (AliUInt32_t)(((AliUInt64_t)eventID.fNr) & 0x00000000FFFFFFFFULL);
#endif
#endif
    if ( readoutListBlockFound )
	{
#if 0
	memcpy( readoutList, blocks[readoutListBlockNdx].fData, blocks[readoutListBlockNdx].fSize );
#else
	AliHLTReadoutList readoutListObj( gReadoutListVersion, (AliVolatileUInt32_t*)readoutList );
#if 0
	memset( readoutList, 0, readoutListSize );
	((AliUInt32_t*)readoutList)[0] = ((AliUInt32_t*)blocks[readoutListFirstBlockNdx].fData)[0];
	for ( ndx = 0; ndx < blockCnt; ndx++ )
	    {
	    if ( blocks[ndx].fDataType.fID == HLTREADOUTLIST_DATAID )
		{
		if ( blocks[ndx].fSize != readoutListSize )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTHLTOutFormatterSubscriber::ProcessEvent", "Readout list size mismatch" )
			<< "Readout list size mismatch. First readout list block has size " << AliHLTLog::kDec << readoutListSize
			<< ", while block " << ndx << " has size " << blocks[ndx].fSize << ". Ignoring block " << ndx
			<< "." << ENDLOG;
		    continue;
		    }
		for ( unsigned long word=1; word < readoutListSize/sizeof(AliUInt32_t); word++ )
		    ((AliUInt32_t*)readoutList)[word] |= ((AliUInt32_t*)blocks[ndx].fData)[word];
		}
	    }
#else
	readoutListObj.Reset();
	for ( ndx = 0; ndx < blockCnt; ndx++ )
	    {
	    if ( blocks[ndx].fDataType.fID == HLTREADOUTLIST_DATAID )
		{
		unsigned rlv = AliHLTReadoutList::GetReadoutListVersion( (AliVolatileUInt32_t*)blocks[ndx].fData );
		if ( rlv )
		    {
		    if ( blocks[ndx].fSize<AliHLTReadoutList::GetReadoutListSize(rlv) )
			{
			LOG( AliHLTLog::kWarning, "AliHLTHLTOutFormatterSubscriber::ProcessEvent", "Readout list size mismatch" )
			    << "Readout list size mismatch. Readout list block with block index "
			    << AliHLTLog::kDec << ndx << " has size " << blocks[ndx].fSize
			    << " B while at least " << AliHLTReadoutList::GetReadoutListSize(rlv)
			    << " B are expected for readout list version " << rlv 
			    << " (readout list word count: " << (unsigned long)(((AliUInt32_t*)blocks[ndx].fData)[0])
			    << ")." << ENDLOG;
			continue;
			}
		    if ( rlv==gReadoutListVersion )
			{
			AliHLTReadoutList inRdLst( rlv, (AliVolatileUInt32_t*)blocks[ndx].fData );
			readoutListObj |= inRdLst;
			}
		    else
			{
			AliVolatileUInt32_t inRdLstData[AliHLTReadoutList::GetReadoutListDDLWordCount(gReadoutListVersion)+1];
			AliHLTReadoutList inRdLst( gReadoutListVersion, inRdLstData );
			inRdLst.Reset();
			inRdLst.Import( (AliVolatileUInt32_t*)blocks[ndx].fData, rlv );
			readoutListObj |= inRdLst;
			}
		    }
		}
	    }
#endif
	std::vector<MLUCString>::iterator rladIter, rladEnd;
	if ( hltBitSet ) // if HLT bit set allow output on any link, actual link will be set in HLTOutWriterSubscriber; if no link set, disable HLT link being set in HOWS for full reject
	    readoutListObj.SetDetectorDDLs( "HLT", true );
	else
	    readoutListObj.SetDetectorDDLs( "HLT", false );
	rladIter = fReadoutListAdditionalDetectors.begin();
	rladEnd = fReadoutListAdditionalDetectors.end();
	while ( rladIter != rladEnd )
	    {
	    readoutListObj.SetDetectorDDLs( rladIter->c_str() );
	    rladIter++;
	    }
	
#endif
	if ( readoutListOutput )
	    {
	    AliVolatileUInt32_t* readoutListData = (AliVolatileUInt32_t*)readoutList;
	    MLUCString msg;
	    for ( unsigned nn=0; nn<readoutListObj.GetReadoutListSize(); nn += sizeof(hltreadout_uint32_t) )
		{
		char tmp[256];
		snprintf( tmp, 256, "%02u : 0x%08X", nn, readoutListData[nn] );
		if ( nn )
		    msg += " - ";
		msg += tmp;
		}
	    LOG( AliHLTLog::kImportant, "AliHLTHLTOutFormatterSubscriber::ProcessEvent", "Output readout list" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << ") readout list: " << msg.c_str() << "." << ENDLOG;
	    }
	}

    if ( dataFound )
	fHOMERInterface.Copy( data );

    AliHLTSubEventDescriptor::BlockData dataBlock;
    dataBlock.fShmKey.fShmType = kInvalidShmType;
    dataBlock.fShmKey.fKey.fID = (AliUInt64_t)-1;
    dataBlock.fOffset = 0;
    dataBlock.fSize = totalSize;
    dataBlock.fProducerNode = fNodeID;
    dataBlock.fDataType.fID = HLTOUTPUT_DATAID;
    dataBlock.fDataOrigin.fID = HLT_DATAORIGIN;
    dataBlock.fDataSpecification = 0;
    dataBlock.fStatusFlags = 0;
    dataBlock.fAttributes[kAliHLTBlockDByteOrderAttributeIndex] = kAliHLTNativeByteOrder;
    dataBlock.fAttributes[kAliHLTBlockD64BitAlignmentAttributeIndex] = AliHLTDetermineUInt64Alignment();
    dataBlock.fAttributes[kAliHLTBlockD32BitAlignmentAttributeIndex] = AliHLTDetermineUInt32Alignment();
    dataBlock.fAttributes[kAliHLTBlockD16BitAlignmentAttributeIndex] = AliHLTDetermineUInt16Alignment();
    dataBlock.fAttributes[kAliHLTBlockD8BitAlignmentAttributeIndex] = AliHLTDetermineUInt8Alignment();
    dataBlock.fAttributes[kAliHLTBlockDFloatAlignmentAttributeIndex] = AliHLTDetermineDoubleAlignment();
    dataBlock.fAttributes[kAliHLTBlockDDoubleAlignmentAttributeIndex] = AliHLTDetermineFloatAlignment();
    outputBlocks.insert( outputBlocks.end(), dataBlock );

    size = totalSize;

    return true;
    }


void AliHLTHLTOutFormatterSubscriber::SetupHOMERInterface()
    {
    if ( fShmManager )
	fHOMERInterface.SetShmManager( fShmManager );
    }




/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTHLTOutFormatter.cpp 905 2006-04-10 11:12:18Z timm $ 
**
***************************************************************************
*/
