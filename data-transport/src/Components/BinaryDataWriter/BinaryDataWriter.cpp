/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: BinaryDataWriter.cpp 945 2006-05-10 12:36:55Z timm $ 
**
***************************************************************************
*/

#include "AliHLTControlledComponent.hpp"
#include "AliHLTProcessingSubscriber.hpp"
#include "AliHLTEventWriter.hpp"

class BinaryDataWriter: public AliHLTProcessingSubscriber
    {
    public:

	BinaryDataWriter( const char* name, 
			 const char* filenamePrefix, 
			 bool sendEventDone, AliUInt32_t minBlockSize,
			 AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, 
			 int eventCountAlloc = -1 );
  
	virtual ~BinaryDataWriter();

    protected:

	virtual bool ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* etsp,
				   AliUInt8_t* outputPtr, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, AliHLTEventDoneData*& edd );
	
	MLUCString fFilenamePrefix;

    private:
    };




BinaryDataWriter::BinaryDataWriter( const char* name, 
				    const char* filenamePrefix, 
				    bool sendEventDone, AliUInt32_t minBlockSize,
				    AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, 
				    int eventCountAlloc ):
    AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc ),
    fFilenamePrefix(filenamePrefix)
    {
    }

BinaryDataWriter::~BinaryDataWriter()
    {
    }

bool BinaryDataWriter::ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* ets,
					       AliUInt8_t*, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>&, AliHLTEventDoneData*& )
    {
    size = 0;
    return AliHLTEventWriter::WriteEvent( eventID, 
					  blockCnt, blocks, sedd, ets,
					  fRunNumber, fFilenamePrefix.c_str() );
    }
	    

class BinaryDataWriterComponent: public AliHLTControlledSinkComponent
    {
    public:

	BinaryDataWriterComponent( const char* name, int argc, char** argv );
	virtual ~BinaryDataWriterComponent() {};


    protected:

	virtual void ShowConfiguration();

	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

	virtual BinaryDataWriter* CreateSubscriber();

	MLUCString fFilenamePrefix;

	typedef AliHLTControlledSinkComponent TSuper;

    private:
    };


BinaryDataWriterComponent::BinaryDataWriterComponent( const char* name, int argc, char** argv ):
    TSuper( name, argc, argv )
    {
    fFilenamePrefix = "EventData";
    }

void BinaryDataWriterComponent::ShowConfiguration()
    {
    TSuper::ShowConfiguration();
    LOG( AliHLTLog::kInformational, "BinaryDataWriterComponent::ShowConfiguration", "Filename prefix" )
	<< "Output file name prefix: '" << fFilenamePrefix.c_str() << "'." << ENDLOG;
    }

const char* BinaryDataWriterComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    if ( !strcmp( argv[i], "-filenameprefix" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing output file name prefix specifier.";
	    }
	fFilenamePrefix = argv[i+1];
	i += 2;
	return NULL;
	}
    return TSuper::ParseCmdLine( argc, argv, i, errorArg );
    }

void BinaryDataWriterComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    TSuper::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-filenameprefix: Specify the prefix to be used for output file names. (Optional)" 
	<< ENDLOG;
    }


BinaryDataWriter* BinaryDataWriterComponent::CreateSubscriber()
    {
    return new BinaryDataWriter( fSubscriberID.c_str(), 
				 fFilenamePrefix.c_str(),
				 fSendEventDone, fMinBlockSize, 
				 fPerEventFixedSize, fPerBlockFixedSize, fSizeFactor, 
				 fMaxPreEventCountExp2 );
    }



int main( int argc, char** argv )
    {
    BinaryDataWriterComponent procComp( "BinaryDataWriter", argc, argv );
    procComp.Run();
    return 0;
    }



/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: BinaryDataWriter.cpp 945 2006-05-10 12:36:55Z timm $ 
**
***************************************************************************
*/
