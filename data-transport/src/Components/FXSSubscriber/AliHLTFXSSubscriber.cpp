/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2007
** This file has been written by Sebastian Bablok,
** sebastian.bablok@uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/
#include "AliHLTFXSSubscriber.hpp"

#include "AliHLTShuttlePortalSubscriberComponent.hpp"
#include "AliHLTShuttlePortalSubscriberDefines.h"
#include "AliHLTShuttlePortalSubscriberTypes.h"

#include "AliHLTFXSProtocol.hpp"

#include <iostream>


using namespace std;


AliHLTFXSSubscriber::AliHLTFXSSubscriber(const char* name, // for parent class
		bool sendEventDone,	AliUInt32_t minBlockSize, // for parent class
		string fxsBase, string dbName, string dbHost, string dbUser,
		string dbPasswd, // Component specific data (FXS Subscriber relevant)
		int eventCountAlloc) : // for parent class

    	AliHLTProcessingSubscriber(name, sendEventDone, minBlockSize, 0, 0, 0, 
					// 0, 0, 0 :is sink -> no allocation of output memory needed
    			eventCountAlloc),
    	fFXSBasePath(fxsBase), fDBName(dbName), fDBHost(dbHost),
    	fDBUser(dbUser), fDBPasswd(dbPasswd), fShuttlePortal(&fFXSLogger), 
		fFXSLogger(), fRetryCount(0) {
		
	// connector components (init)
	int nRet = 0;
#ifdef __DEBUG
	cout << "*** Calling init of connector..." << endl;
#endif
	nRet = fShuttlePortal.init(fFXSBasePath, fDBName, fDBHost, fDBUser, fDBPasswd);
	if ((nRet != ALIHLT_OK) || (!fShuttlePortal.isInit())) {
		LOG(AliHLTLog::kError, "AliHLTFXSSubscriber::Constructor", "Unable to init Shuttle Portal")
				<< "Initialization of MySQL DB and/or FXS failed, error code: "
				<< nRet << " ." << ENDLOG;		
#ifdef __DEBUG
		cout << "ERROR: Shuttle-connector unable to initialize; error code: " << nRet << endl;
#endif
	} else {
		LOG(AliHLTLog::kInformational, "AliHLTFXSSubscriber::Constructor", "Init Shuttle Portal OK")
				<< "MySQL DB and FXS successfully initialized." << ENDLOG;
#ifdef __DEBUG
		cout << "INFO: Shuttle-module successfully initialized." << endl;
#endif
//		fShuttlePortal.deinit();
	}
	
}


AliHLTFXSSubscriber::~AliHLTFXSSubscriber() {

	if (fShuttlePortal.isInit()) {
		fShuttlePortal.deinit();
	}

}



bool AliHLTFXSSubscriber::ProcessEvent(AliEventID_t eventID, AliUInt32_t blockCnt,
		AliHLTSubEventDescriptor::BlockData* blocks,
		const AliHLTSubEventDataDescriptor*, const AliHLTEventTriggerStruct*,
		AliUInt8_t*, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>&,
		AliHLTEventDoneData*&) {

	LOG(AliHLTLog::kDebug, "AliHLTFXSSubscriber::ProcessEvent", "Started")
			<< "Process event 0x" << AliHLTLog::kHex << eventID << " ("
			<< AliHLTLog::kDec << eventID << ") started." << ENDLOG;
	
	int nRet = ALIHLT_OK;
	size = 0; // no returned data -> size of retuned data is 0

	if (!fShuttlePortal.isInit()) {
		// init Shuttle Portal
		nRet = fShuttlePortal.init(fFXSBasePath, fDBName, fDBHost, fDBUser, fDBPasswd);
	}
	
	if (nRet == ALIHLT_OK) {

		// loop over all blocks of data (amount = blockCnt)
		for (AliUInt32_t i = 0; i < blockCnt; i++) {
			AliHLTFXSProtocol* calibBlock = (AliHLTFXSProtocol*) (blocks[i].fData);
			AliUInt32_t blockPayloadSize = blocks[i].fSize;
			AliHLTEventDataType blockPayloadType = blocks[i].fDataType;

			//	AliUInt8_t* blockPayloadAttributes = blocks[i].fAttributes;
			// check lateron for Byte ordering, etc

 
			if (blockPayloadType.fID != ALI_HLT_TYPE_CALIBRATION_BLOCK) {
				// log wrong data type received !!!
				char tmp[9] = {0};
				for (unsigned aInt = 0; aInt < 8; aInt++) {
					tmp[aInt] = blockPayloadType.fDescr[aInt];
				}
				tmp[8] = 0;
				LOG(AliHLTLog::kDebug, "AliHLTFXSSubscriber::ProcessEvent", "Wrong data type")
						<< "received incorrect data: '" << tmp << "'." << ENDLOG;
				continue;
			}


			// required member for a calibration object
			AliUInt32_t headerVersion = calibBlock->fHeaderVersion;
			if (headerVersion != ALI_HLT_CALIBRATION_BLOCK_CURRENT_HEADER) {
				LOG(AliHLTLog::kError, "AliHLTFXSSubscriber::ProcessEvent", "Wrong FXS header version")
						<< "received wrong FXS header version (" << headerVersion 
						<< "), expected: " << ALI_HLT_CALIBRATION_BLOCK_CURRENT_HEADER 
						<< " ." << ENDLOG;
				continue;
			}
			AliUInt32_t runNumber = calibBlock->fRun;
			char* detector = calibBlock->fDetector;
			char* fileID = calibBlock->fFileID;
			char* ddlNumber = calibBlock->fDDLNumbers;
			AliUInt32_t blobSize = blockPayloadSize - sizeof(AliHLTFXSProtocol);
			void* blob = blocks[i].fData + sizeof(AliHLTFXSProtocol);	

			// create calibration object
			AliHLTCalibrationObject calib(runNumber, detector, fileID, ddlNumber,
					blob, blobSize);

			// Put Calibration object to FXS and Meta data to DB
			nRet = fShuttlePortal.storeCalibObject(calib);
			if (nRet != ALIHLT_OK) {
				LOG(AliHLTLog::kError, "AliHLTFXSSubscriber::ProcessEvent", "Cannot store calibration object")
			    		<< "Storing of calibration object failed, error code: " 
						<< nRet << " ." << ENDLOG;
			} else {
				LOG(AliHLTLog::kDebug, "AliHLTFXSSubscriber::ProcessEvent", "Completed")
						<< "FXS-Subscriber has successfully stored object to FXS; event 0x" 
						<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec 
						<< eventID << ")." << ENDLOG;
			} // end of check for correct storage procedure
		} // end for-loop


		// Deinit Shuttle portal
//		fShuttlePortal.deinit();
	} else {
		LOG(AliHLTLog::kError, "AliHLTFXSSubscriber::ProcessEvent", "Unable to init Shuttle Portal")
				<< "Initialization of MySQL DB and/or FXS failed, error code: "
				<< nRet << " ." << ENDLOG;
		fRetryCount++;
		if (fRetryCount < ALI_HLT_SHUTTLE_MAX_RETRY) {
			return false;
		}
	}

	return true;
}




