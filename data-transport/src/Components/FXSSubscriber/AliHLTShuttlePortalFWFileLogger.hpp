#ifndef ALI_HLT_SHUTTLE_PORTAL_FW_FILE_LOGGER_HPP
#define ALI_HLT_SHUTTLE_PORTAL_FW_FILE_LOGGER_HPP

#include "AliHLTShuttlePortalFileLogger.hpp"
#include "MLUCLog.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTStackTrace.hpp"

#include <string>


/**
 * Implementation of a Shuttle Portal logger. This logger adapts all messages
 * to the logger of the HLT framework and inherites the regular file logging
 * features from the file logger.
 *
 * @date 09.07.2007
 *
 * @author Sebastian Bablok <Sebastian.Bablok@uib.no>
 */
class AliHLTShuttlePortalFWFileLogger : public AliHLTShuttlePortalFileLogger {
	/**
	 * Typedef to access the parent class members
	 */
	typedef AliHLTShuttlePortalFileLogger base;
	
	public:
		/**
		 * Std-Constructor for creating a Shuttle portal framework Logger
		 */
		AliHLTShuttlePortalFWFileLogger();

		/**
		 * Constructor for creating a Shuttle portal Framework Logger which
		 * accepts a default log level
		 *
		 * @param logLevel the desired log level
		 */
		AliHLTShuttlePortalFWFileLogger(AliHLTShuttlePortalLogLevel logLevel);

		/**
		 * Constructor for setting logfilename and the desired log level.
		 * If no log level is provided, all levels are enabled.
		 *
		 * @param fileName the chosen log file name.
		 * @param logLevel the desired log level
		 */
		AliHLTShuttlePortalFWFileLogger(std::string fileName, 
				AliHLTShuttlePortalLogLevel logLevel = mAll);

		/**
		 * Destructur for the Shuttle portal File Logger.
		 */
		virtual ~AliHLTShuttlePortalFWFileLogger();


	protected:
		/**
		 * Pushes the log message to the desired output:
		 * Tries to log to file and gives messages to FW logger
		 *
		 * @param logLevel the log level of this message
		 * @param msg the message to be logged
		 *
		 * @return success state of the function call
		 */
		virtual int pushLogMessage(AliHLTShuttlePortalLogLevel logLevel, std::string msg);

		/**
		 * Pushes the log message to the desired output:
		 * Tries to log to file and gives messages to FW logger
		 *
		 * @param msg the message to be logged
		 *
		 * @return success state of the function call
		 */
		virtual int pushLogMessage(std::string msg);


	private:

};

#endif // ALI_HLT_SHUTTLE_PORTAL_FW_FILE_LOGGER_HPP
