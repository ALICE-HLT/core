/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2007
** This file has been written by Sebastian Bablok,
** sebastian.bablok@uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/
#ifndef ALI_HLT_SHUTTLE_PORTAL_SUBSCRIBER_COMPONENT_HPP
#define ALI_HLT_SHUTTLE_PORTAL_SUBSCRIBER_COMPONENT_HPP


//#include "AliHLTTypes.h"

#include <string>


#include "AliHLTControlledComponent.hpp"
//#include "AliHLTControlledSinkComponent.hpp"


/**
 * This class glues the Shuttle portal component to the HLT PubSub framework.
 *
 * @author Sebastian Bablok
 *
 * @date 2007-05-08
 */
class AliHLTShuttlePortalSubscriberComponent : public AliHLTControlledSinkComponent {
    public:

		/**
		 * Constructor for a AliHLTShuttlePortalSubscriberComponent
		 *
		 * @param name passed through to AliHLTControlledSinkComponent
		 * @param argc argument count of command line
		 * @param argv arguments of command line
		 */
		AliHLTShuttlePortalSubscriberComponent(const char* name, int argc, char** argv);

		/**
		 * Destructor for a AliHLTShuttlePortalSubscriberComponent
		 */
		virtual ~AliHLTShuttlePortalSubscriberComponent();


    protected:
        /**
		 * Typedef for Parent class
		 */
		typedef AliHLTControlledSinkComponent TSuper;


    	/**
		 * Function to parse the entries of the command line
		 *
		 * @param argc argument count of command line
		 * @param argv pointer to the arguments of the command line
		 * @param i [in] number in argument list where to start with parsing;
		 *		[out] number where to start with next parsing round.
		 * @param errorArg [out] number of argument in list where parsing
		 *		error occured.
		 *
		 * @return NULL in case of no error, else error string
		 */
		virtual const char* ParseCmdLine(int argc, char** argv, int& i, int& errorArg);

		/**
		 * Fucntion to print out the usage of this component.
		 *
		 * @param errorStr ?? (passed to parent class)
		 * @param errorArg ?? (passed to parent class)
		 * @param errorParam ?? (passed to parent class)
		 */
		virtual void PrintUsage(const char* errorStr, int errorArg, int errorParam);

		/**
		 * Function to create a AliHLTFXSSubscriber component.
		 *
		 * @return pointer to AliHLTFXSSubscriber component as a
		 *		AliHLTProcessingSubscriber*
		 */
		virtual AliHLTProcessingSubscriber* CreateSubscriber();

		/**
		 * Function to show the current configuration.
		 */
		virtual void ShowConfiguration();



    private:
    	/**
    	 * Stores the base path for the File Exchange Server
    	 */
    	std::string fFXSBasePath;

    	/**
    	 * Stores the data base name for the Shuttle portal
    	 */
    	std::string fDBName;

    	/**
    	 * Stores the host name of the data base of the Shuttle portal
    	 */
    	std::string fDBHost;

    	/**
		 * Stores the user name of the data base of the Shuttle portal
    	 */
    	std::string fDBUser;

    	/**
		 * Stores the password for the above mentionmed user of the data base
		 * of the Shuttle portal.
    	 */
    	std::string fDBPasswd;

};

#endif //ALI_HLT_SHUTTLE_PORTAL_SUBSCRIBER_COMPONENT_HPP
