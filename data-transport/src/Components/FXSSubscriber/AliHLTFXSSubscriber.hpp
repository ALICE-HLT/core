/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2007
** This file has been written by Sebastian Bablok,
** sebastian.bablok@uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/
#ifndef ALI_HLT_FXS_SUBSCRIBER_HPP
#define ALI_HLT_FXS_SUBSCRIBER_HPP

//#include "AliHLTControlledComponent.hpp"

//#include <stdio.h>
//#include <stdlib.h>
//#include <unistd.h>
//#include <sys/types.h>
//#include <sys/stat.h>
//#include <fcntl.h>
//#include <errno.h>


#include "AliHLTProcessingSubscriber.hpp"

#include "AliHLTShuttlePortal.hpp"
#include "AliHLTShuttlePortalFWFileLogger.hpp"
#include "AliHLTShuttlePortalFWLogger.hpp"

// if debug version use file- and framework- logger, else framework- only
#ifdef __LOG_DEBUG
#define LOGGER_MODULE AliHLTShuttlePortalFWFileLogger
#else
#define LOGGER_MODULE AliHLTShuttlePortalFWLogger
#endif

/**
 * DCS portal class to interface the FED-Api and connect it to the PubSub
 * system. Therefore it has to act as well as a DimServer.
 *
 * @author Sebastian Bablok
 *
 * @date 2007-05-08
 */
class AliHLTFXSSubscriber : public AliHLTProcessingSubscriber {
    public:
		/**
		 * Constructor for a AliHLTFedSubscriber.
		 *
		 * @param name parameter required by the parent class
		 * @param sendEventDone parameter required by the parent class
		 * @param minBlockSize parameter required by the parent class
		 * @param fxsBase the base path to the FileExchange Server
		 * @param dbName the name of the MySQL DB of the Shuttle portal
		 * @param dbHost the host name of the above mentioned DB
		 * @param dbUser the user to conntact the DB
		 * @param dbPasswd the password for the DB
		 * @param eventCountAlloc parameter required by the parent class
		 */
		AliHLTFXSSubscriber(const char* name, // for parent class
				bool sendEventDone, AliUInt32_t minBlockSize, // for parent class
				std::string fxsBase, std::string dbName, std::string dbHost,
				std::string dbUser, std::string dbPasswd,
				int eventCountAlloc = -1); // for parent class

		/**
		 * Destructor for AliHLTFedSubscriber
		 */
		virtual ~AliHLTFXSSubscriber();



    protected:

		/**
		 * Function to process the data given from the PubSub framework.
		 * Here the mapping from data given from DAs through the PubSub
		 * framework to the FedApi is performed. The Dim Channels, representing
		 * the FedApi are feed with the given data.
		 * Possible data for the FedApi are: Single and Grouped Services as
		 * well as messages over the FedApi.
		 * <pre>
		 *
		 * Structure of the actual data provided by the data blocks:
		 * All payload blocks have a header and the actual payload:
		 *
		 * +------------------------------------------------------------------+
		 * | Offset{int32}|TypeOfPayload{char} | SizeOfPayload{int32}|payload |
		 * +------------------------------------------------------------------+
		 *
		 *	- Offset: size of this header in Bytes (offset to BlockData
		 *			pointer), stored in a 32bit integer (define:
		 *			FED_API_SERVICE_DATA_HEADER_OFFSET)
		 *	- TypeOfPayload: type of the payload as described below
		 *			(Single-, Grouped, or Message- Service)
		 *			('S' = Single, 'G' = Grouped, 'M' = Message);
		 *			stored in a single char (AliUInt8_t).
		 *	- SizeOfPayload: size of the payload in Bytes;
		 *			stored in a 32bit integer.
		 *	- payload: arbitrary amount of payload blocks, which are described
		 *			below (glued after each other). (the actual amount is given
		 *			by the payload size and the members in the payload itself)
		 *
		 * (All members are streamed after each other, blocks are glued after
		 * each other; no paddding)
		 *
		 * Payload blocks (structure):
		 *
		 * 1 + 2. Single and/or Grouped Services (its size is variable):
		 *
		 * +------------------------------------------------------------------+
		 * |nameLength{int32}|intValue{int32}|floatValue|name{NULL terminated}|
		 * +------------------------------------------------------------------+
		 *
		 *	- nameLength: length of the name of the service (inclusive
		 *		Null-termination); stored in a 32bit integer.
		 *	- intValue: a 32bit integer representing the service; if the
		 *		service is NOT represented by an integer, set this to the
		 *		DON'T CARE value: -9999999 .
		 *	- floatValue: a float representing the service; if the
		 *		service is NOT represented by a float, set this to the
		 *		DON'T CARE value: -9999999.0 .
		 *	- name: char string with the name of the service, represented by
		 *		the before mentioned values; NULL terminated.
		 *
		 * 3. Message Service (fixed size: 540 Bytes):
		 *
		 *      +---------------------------------------------
		 *      | messageType{int32} | source{char[256]} |
		 *      +---------------------------------------------
		 *
		 *     --------------------------------------------------------+
		 *       messageDescription{char[256]} | timestamp{char[20]}   |
		 *     --------------------------------------------------------+
		 *
		 *	- messageType: a 32bit integer representing the type of the message,
		 *		(MSG_INFO = 1, MSG_WARNING = 2, MSG_ERROR = 4,
		 *		 MSG_FAILURE_AUDIT = 8, MSG_SUCCESS_AUDIT = 16,
		 *		 MSG_DEBUG = 32, MSG_ALARM = 64).
		 *	- source: the actual source of the message, here the different
		 *		detectors (like TPC, PHOS, TRD, ...) can encode as well there
		 *		detector name, as well a more specific description of where this
		 *		event has occured; char array of size 256; NULL terminated (!).
		 *	- messageDescription: the actual message, that shall be send; char
		 *		array of size 265; NULL terminated(!).
		 *	- timestamp: timestamp, when this message has been produced; has to
		 *		have the format: "YYYY-MM-DD hh:mm:ss\0"; char array of size 20;
		 *		NULL terminated.
		 *
		 * </pre>
		 *
		 * @param eventID structure with event ID
		 *			(function doesn't care)
		 * @param blockCnt number of blocks of type BlockData.
		 * @param blocks pointer to the first block of type BlockData given by
		 *			the framework (includes meta data to the actual data):
		 *			used members of this structure are:
		 *			- fData: pointer to the actual data (payload) (data that
		 *				shall be processed + additional meta data (specific for
		 *				DCSPortal component))
		 *			- fSize: size of the payload (fData) in Bytes
		 *			- fDataType: structure containing ID of the type of the
		 *				data block (fData), in this case the type in
		 *				fDataType.fID has to be FEDAPI_SERVICE_DATAID
		 *				(@see FEDAPI_SERVICE_DATAID)
		 *			- fAttributes: describing the attributes of the data (Byte
		 *				order, alignments, etc)
		 * @param sedd descriptor of blocks
		 *			(function doesn't care)
		 * @param etsp structure with trigger info
		 *			(function doesn't care)
		 * @param outputPtr pointer, that can be filled with return data
		 *			(function doesn't care)
		 * @param size size of output data; is set to 0
		 * @param outputBlocks can store with return data
		 *			(function doesn't care)
		 * @param edd can store optional "processing done" data
		 *			(function doesn't care)
		 *
		 * @return true, if processing has been successful or a NO retry shall
		 *		be made; false if processing has been NOT successful but a retry
		 *		could end in a successful processing of the data.
		 */
		virtual bool ProcessEvent(AliEventID_t eventID, AliUInt32_t blockCnt,
				AliHLTSubEventDescriptor::BlockData* blocks,
				const AliHLTSubEventDataDescriptor* sedd,
				const AliHLTEventTriggerStruct* etsp, AliUInt8_t* outputPtr,
				AliUInt32_t& size,
				vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks,
				AliHLTEventDoneData*& edd);



    private:
    	/**
    	 * Stores the base path for the File Exchange Server
    	 */
    	std::string fFXSBasePath;

    	/**
    	 * Stores the data base name for the Shuttle portal
    	 */
    	std::string fDBName;

    	/**
    	 * Stores the host name of the data base of the Shuttle portal
    	 */
    	std::string fDBHost;

    	/**
		 * Stores the user name of the data base of the Shuttle portal
    	 */
    	std::string fDBUser;

    	/**
		 * Stores the password for the above mentionmed user of the data base
		 * of the Shuttle portal.
    	 */
    	std::string fDBPasswd;

    	/**
    	 * Object for the portal tasks (DB and FXS access)
    	 */
    	AliHLTShuttlePortal fShuttlePortal;

        /**
		 * Logger for the Shuttle portal.
		 * What type of logger depends on the compile flags.
		 */
		LOGGER_MODULE fFXSLogger;
		
    	/**
    	 * Retry count
    	 */
    	int fRetryCount;

};


#endif // ALI_HLT_FXS_SUBSCRIBER_HPP
