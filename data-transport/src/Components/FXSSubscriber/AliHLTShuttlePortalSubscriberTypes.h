/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2007
** This file has been written by Sebastian Bablok,
** sebastian.bablok@uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/
#ifndef ALI_HLT_SHUTTLE_PORTAL_SUBSCRIBER_TYPES_H
#define ALI_HLT_SHUTTLE_PORTAL_SUBSCRIBER_TYPES_H



#ifdef __cplusplus
extern "C" {
#endif

/*
#if SHRT_MAX==2147483647
typedef signed short AliSInt32_t;
#else // SHRT_MAX==2147483647

#if INT_MAX==2147483647
typedef signed int AliSInt32_t;
#else // INT_MAX==2147483647

#if LONG_MAX==2147483647
typedef signed long AliSInt32_t;
#else // LONG_MAX==2147483647

#error Could not typedef AliSInt32_t

#endif // LONG_MAX==2147483647

#endif // INT_MAX==2147483647

#endif // SHRT_MAX==2147483647

*/


/**
 * This struct represents the header of a calibration block shipped by PubSub
 *
 * @author Sebastian Bablok < Sebastian.Bablok@uib.no>
 *
 * @date 2007-05-08
 */
typedef struct CalibrationObjectPayloadHeader {
	/**
	 * Stores the version number
	 */
	AliUInt32_t fHeaderVersion;

	/**
	 * Stores the run number
	 */
	AliUInt32_t fRunNumber;

	/**
	 * Stores the detector name
	 */
	char fDetector[4];

	/**
	 * Stores the file ID
	 */
	char fFileID[128];

	/**
	 * Stores the DDL numbers
	 */
	char fDDLNumber[64];

} CalibrationBlockHeader;




#ifdef __cplusplus
}
#endif

#endif //ALI_HLT_SHUTTLE_PORTAL_SUBSCRIBER_TYPES_H
