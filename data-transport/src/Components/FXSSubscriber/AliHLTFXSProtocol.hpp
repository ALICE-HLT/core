/************************************************************************
 * **
 * **
 * ** This file is property of and copyright by the Department of Physics
 * ** Institute for Physic and Technology, University of Bergen,
 * ** Bergen, Norway, 2007
 * ** This file has been written by Sebastian Bablok,
 * ** sebastian.bablok@uib.no
 * **
 * ** Important: This file is provided without any warranty, including
 * ** fitness for any particular purpose.
 * **
 * **
 * *************************************************************************/

#ifndef ALI_HLT_FXS_PROTOCOL_HPP
#define ALI_HLT_FXS_PROTOCOL_HPP

#ifdef __cplusplus
extern "C" {
#endif
		

/** Define for FXS protocol detector field size in header. */
#define ALIHLTFXSPROT_DETECTOR_SIZE 4

/** Define for FXS protocol fileId field size in header. */
#define ALIHLTFXSPROT_FILEID_SIZE 128

/** Define for FXS protocol DDL number field size in header. */
#define ALIHLTFXSPROT_DDLNUMBER_SIZE 64

/**
 * Struct for storing the header of a FXS protocol
 *
 * @author Sebastian Bablok <Sebastian.Balok@uib.no>
 *
 * @date 2007-06-24
 */ 
typedef struct AliHLTFXSProtocolHeader {
	/**
	 * Stores the header version
	 */
	AliUInt32_t fHeaderVersion;

	/**
	 * Stores the run number
	 */
	AliUInt32_t fRun;

	/**
	 * Stores the detector
	 */ 
	char fDetector[ALIHLTFXSPROT_DETECTOR_SIZE];

	/**
	 * Stores the File ID
	 */ 
	char fFileID[ALIHLTFXSPROT_FILEID_SIZE];

	/**
	 * Stores the DDL numbers
	 */ 
	char fDDLNumbers[ALIHLTFXSPROT_DDLNUMBER_SIZE];
} AliHLTFXSProtocol;


#ifdef __cplusplus
}
#endif

#endif  // ALI_HLT_FXS_PROTOCOL_HPP

