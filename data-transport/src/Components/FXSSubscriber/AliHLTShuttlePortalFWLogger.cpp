#include "AliHLTShuttlePortalFWLogger.hpp"


using namespace std;

AliHLTShuttlePortalFWLogger::AliHLTShuttlePortalFWLogger() {

}

AliHLTShuttlePortalFWLogger::AliHLTShuttlePortalFWLogger(
				AliHLTShuttlePortalLogLevel logLevel) :
				AliHLTShuttlePortalLogger(logLevel) {

}


AliHLTShuttlePortalFWLogger::~AliHLTShuttlePortalFWLogger() {

}


int AliHLTShuttlePortalFWLogger::pushLogMessage(
			AliHLTShuttlePortalLogLevel logLevel, string msg) {
	// check for log level
	if (checkLogLevel(logLevel) != ALI_HLT_LOGGING_OK) {
		return ALI_HLT_LOGGING_FAILED;
	}
				
	int nRet = ALI_HLT_LOGGING_OK;
	MLUCLog::TLogLevel fwLevel = AliHLTLog::kNone;
	string addInfo;
	bool stackTrace = false;

	// convert to HLT framework (PubSub) log level
	switch (logLevel) {
		case (mNone): fwLevel = AliHLTLog::kNone;
			break;
		case (mDebug): fwLevel = AliHLTLog::kDebug;
			break;
		case (mInfo): fwLevel = AliHLTLog::kInformational;
			break;
		case (mWarning): fwLevel = AliHLTLog::kWarning;
			break;
		case (mError): fwLevel = AliHLTLog::kError;
			stackTrace = true;
			break;
		case (mFatal): fwLevel = AliHLTLog::kFatal;
			stackTrace = true;
			break;
		case (mPrimary): fwLevel = AliHLTLog::kPrimary;
			break;
		case (mAll): fwLevel = AliHLTLog::kAll;
			break;
		default: fwLevel = AliHLTLog::kWarning;
			addInfo = "++ Received unknown log level, assuming WARNING ++ : ";
	}

	// push to framework logger
	LOG( fwLevel, "AliHLTFXSSubscriberLogger::Message", 0 ) << addInfo.c_str()
			<< msg.c_str() << ENDLOG;

	if (stackTrace) {
		AliHLTStackTrace("AliHLTFXSSubscriberLogger::Message", fwLevel, 5);
		// ??? ask Timm about 3. Param
	}

	return nRet;
}

int AliHLTShuttlePortalFWLogger::pushLogMessage(string msg) {
	return pushLogMessage(mWarning, msg);
}


