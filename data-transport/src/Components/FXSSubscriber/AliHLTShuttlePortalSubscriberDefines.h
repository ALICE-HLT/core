/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2007
** This file has been written by Sebastian Bablok,
** sebastian.bablok@uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/
#ifndef ALI_HLT_SHUTTLE_PORTAL_SUBSCRIBER_DEFINES_H
#define ALI_HLT_SHUTTLE_PORTAL_SUBSCRIBER_DEFINES_H

/**
 * Define for payload of AliHLTSubEventDescriptor::BlockData block is of type
 * FedApi Service data.
 * This is either of type Single Service and/or Grouped Services and/or
 * Message Service (for details see AliHLTFedSubscriber::ProcessEvent(...)).
 *
 * @see AliHLTFedSubscriber::ProcessEvent
 */
#define ALI_HLT_TYPE_CALIBRATION_BLOCK	(((AliUInt64_t)'FXS_')<<32 | 'CAL ')


/**
 * Define for the current Calibration block header version
 */
#define ALI_HLT_CALIBRATION_BLOCK_CURRENT_HEADER 1

/**
 * Define for the Maximal retries for calling FXS subscriber in case of
 * errors in ProcessEvent.
 */
#define ALI_HLT_SHUTTLE_MAX_RETRY 5


#endif //ALI_HLT_SHUTTLE_PORTAL_SUBSCRIBER_DEFINES_H
