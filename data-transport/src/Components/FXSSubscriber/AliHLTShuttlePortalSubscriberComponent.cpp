/************************************************************************
**
**
** This file is property of and copyright by the Department of Physics
** Institute for Physic and Technology, University of Bergen,
** Bergen, Norway, 2007
** This file has been written by Sebastian Bablok,
** sebastian.bablok@uib.no
**
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
*************************************************************************/
#include "AliHLTShuttlePortalSubscriberComponent.hpp"
#include "AliHLTFXSSubscriber.hpp"

using namespace std;


AliHLTShuttlePortalSubscriberComponent::AliHLTShuttlePortalSubscriberComponent(const char* name,
		int argc, char** argv) : TSuper(name, argc, argv) {
}


AliHLTShuttlePortalSubscriberComponent::~AliHLTShuttlePortalSubscriberComponent() {

}


void AliHLTShuttlePortalSubscriberComponent::ShowConfiguration() {
	TSuper::ShowConfiguration();
	LOG(AliHLTLog::kInformational,
    		"AliHLTShuttlePortalSubscriberComponent::ShowConfiguration", "FXS base path, Database, DB Host, User")
			<< "FXS base path: '" << fFXSBasePath.c_str()
			<< "', Database: '" << fDBName.c_str()
			<< "', DB Host: '" << fDBHost.c_str()
			<< "', User: '" << fDBUser.c_str()
			<< "." << ENDLOG;
}


const char* AliHLTShuttlePortalSubscriberComponent::ParseCmdLine(int argc, char** argv,
		int& i, int& errorArg) {

	// FXS base path
	if (!strcmp(argv[i], "-FXSBase")) {
		if (argc <= (i + 1)) {
			return "Missing File Exchange Server base path.";
		}
		fFXSBasePath = argv[i + 1];
		i += 2;
		return NULL;
	}

	// DB name
	if (!strcmp(argv[i], "-DBName")) {
		if (argc <= (i + 1)) {
			return "Missing Data base name.";
		}
		fDBName = argv[i + 1];
		i += 2;
		return NULL;
	}

	// DB host
	if (!strcmp(argv[i], "-DBHost")) {
		if (argc <= (i + 1)) {
			return "Missing DB host name.";
		}
		fDBHost = argv[i + 1];
		i += 2;
		return NULL;
	}

	// DB User
	if (!strcmp(argv[i], "-DBUser")) {
		if (argc <= (i + 1)) {
			return "Missing DB user name.";
		}
		fDBUser = argv[i + 1];
		i += 2;
		return NULL;
	}

	// DB Password
	if (!strcmp(argv[i], "-DBPasswd")) {
		if (argc <= (i + 1)) {
			return "Missing DB password.";
		}
		fDBPasswd = argv[i + 1];
		i += 2;
		return NULL;
	}

	return TSuper::ParseCmdLine(argc, argv, i, errorArg);
}



void AliHLTShuttlePortalSubscriberComponent::PrintUsage(const char* errorStr, int errorArg,
		int errorParam) {

	TSuper::PrintUsage(errorStr, errorArg, errorParam); // call to parent class
	LOG(AliHLTLog::kError, "Processing Component", "Command line usage")
			<< "-FXSBase <base path>: Base path of the File Exchange Server (most likely \"/opt/FXS\"). "
			<< ENDLOG;
	LOG(AliHLTLog::kError, "Processing Component", "Command line usage")
			<< "-DBName <data base name>: Data base name of the Shuttle portal (most likely \"hlt_logbook\"). "
			<< ENDLOG;
	LOG(AliHLTLog::kError, "Processing Component", "Command line usage")
			<< "-DBHost <DB host name>: Host name of the Shuttle portal DB (has to be \"localhost\"; DB server has probs with \"*.internal\"). "
			<< ENDLOG;
	LOG(AliHLTLog::kError, "Processing Component", "Command line usage")
			<< "{-DBUser <DB user name>}: User name for the DB of the Shuttle portal (use \"hlt\"). "
		<< ENDLOG;
	LOG(AliHLTLog::kError, "Processing Component", "Command line usage")
			<< "{-DBPasswd <DB password>}: Password for the DB of the Shuttle portal (ask admin). "
		<< ENDLOG;

}


AliHLTProcessingSubscriber* AliHLTShuttlePortalSubscriberComponent::CreateSubscriber() {
#ifdef __DEBUG
	cout << "Create the Subscriber now ..." << endl;    
#endif
	return new AliHLTFXSSubscriber(fSubscriberID.c_str(), fSendEventDone,
    		fMinBlockSize, fFXSBasePath, fDBName, fDBHost, fDBUser, fDBPasswd,
    		fMaxPreEventCountExp2);
}



///////////////////////////////// -> MAIN <- //////////////////////////////////

int main(int argc, char** argv) {
    AliHLTShuttlePortalSubscriberComponent procComp("AliHLTFXSSubscriber", argc, argv);
    procComp.Run();
    return 0;
}

