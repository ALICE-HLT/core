#ifndef ALI_HLT_SHUTTLE_PORTAL_FW_LOGGER_HPP
#define ALI_HLT_SHUTTLE_PORTAL_FW_LOGGER_HPP

#include "AliHLTShuttlePortalLogger.hpp"

#include "MLUCLog.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTStackTrace.hpp"

#include <string>


/**
 * Implementation of a Shuttle Portal logger. This logger adapts all messages
 * to the logger of the HLT framework.
 *
 * @date 24.06.2007
 *
 * @author Sebastian Bablok <Sebastian.Bablok@uib.no>
 */
class AliHLTShuttlePortalFWLogger : public AliHLTShuttlePortalLogger {
	public:
		/**
		 * Std-Constructor for creating a Shuttle portal framework Logger
		 */
		AliHLTShuttlePortalFWLogger();

		/**
		 * Constructor for creating a Shuttle portal Framework Logger which
		 * accepts a default log level
		 *
		 * @param logLevel the desired log level
		 */
		AliHLTShuttlePortalFWLogger(AliHLTShuttlePortalLogLevel logLevel);

		/**
		 * Destructur for the Shuttle portal File Logger.
		 */
		virtual ~AliHLTShuttlePortalFWLogger();


	protected:
		/**
		 * Pushes the log message to the desired output
		 *
		 * @param logLevel the log level of this message
		 * @param msg the message to be logged
		 *
		 * @return success state of the function call
		 */
		virtual int pushLogMessage(AliHLTShuttlePortalLogLevel logLevel, std::string msg);

		/**
		 * Pushes the log message to the desired output
		 *
		 * @param msg the message to be logged
		 *
		 * @return success state of the function call
		 */
		virtual int pushLogMessage(std::string msg);


	private:

};

#endif // ALI_HLT_SHUTTLE_PORTAL_FW_LOGGER_HPP
