/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTControlledComponent.hpp"
#include "ACEXHandler.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTDetectorPublisher.hpp"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTEventDataType.h"
#include "AliHLTLog.hpp"
#include "AliHLTGetNodeID.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTBufferManager.hpp"
#include "AliHLTShmManager.hpp"
#include "AliHLTStackTrace.hpp"
#include "MLUCVector.hpp"
#include <vector>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>




class AliHLTACEXPublisher: public AliHLTDetectorPublisher
    {
    public:

	AliHLTACEXPublisher( const char* name, int eventSlotsPowerOfTwo = -1 );
	virtual ~AliHLTACEXPublisher();

	void StopPublishing();

	void SetACEXHandler( AliHLTACEX1Handler* rorcHandler )
		{
		fACEXHandler = rorcHandler;
		}

	void SetSleep( bool do_sleep )
		{
		fDoSleep = do_sleep;
		}
	void SetSleepTime( unsigned long sleepTime )
		{
		fSleepTime = sleepTime;
		}

	void SetShm( AliHLTShmID shmKey, AliUInt8_t* ptr, AliUInt32_t size )
		{
		fShmKey = shmKey;
		fShmPtr = ptr;
		fShmSize = size;
		}

	void SetEventDataType( AliHLTEventDataType datatype )
		{
		fEventDataType.fID = datatype.fID;
		}

	void SetEventDataOrigin( AliHLTEventDataOrigin dataorigin )
		{
		fEventDataOrigin.fID = dataorigin.fID;
		}

	void SetEventDataSpecification( AliUInt32_t dataspec )
		{
		fEventDataSpecification = dataspec;
		}

	void SetWrapCopyArea( AliUInt8_t* ptr, AliUInt32_t offset, AliUInt32_t size )
		{
		fWrapCopyAreaStartPtr = ptr;
		fWrapCopyAreaStartOffset = offset;
		fWrapCopyAreaSize = size;
		}


    protected:

	virtual int WaitForEvent( AliEventID_t& eventID, AliHLTSubEventDataDescriptor*& sbevent,
				  AliHLTEventTriggerStruct*& trg ); // Called with no locked mutexes
	virtual void EventFinished( AliEventID_t, AliHLTSubEventDataDescriptor& sbevent, vector<AliHLTEventDoneData*>& eventDoneData ); // Called with no locked mutexes
	virtual void EventFinished( AliEventID_t, vector<AliHLTEventDoneData*>& eventDoneData ); // Called with no locked mutexes
	virtual void QuitEventLoop(); // Called with no locked mutexes
	bool QuittedEventLoop()
		{
		return fEventLoopQuitted;
		}
	virtual void StartEventLoop();
	virtual void EndEventLoop();

	virtual void PauseProcessing();
	virtual void ResumeProcessing();


	bool fQuitEventLoop;
	bool fEventLoopQuitted;

	bool fDoSleep;
	unsigned long fSleepTime;

	AliUInt32_t fOldestEventBirth;

	AliHLTACEX1Handler* fACEXHandler;
	pthread_mutex_t fACEXHandlerMutex;

	AliHLTShmID fShmKey;
	AliUInt8_t* fShmPtr;
	AliUInt32_t fShmSize;

	AliHLTEventTriggerStruct fETS;
	AliHLTEventTriggerStruct* fETP;
	AliUInt32_t fETPOffset;
	AliUInt32_t fETPIndex;

	AliHLTNodeID_t fNodeID;

	AliHLTEventDataType fEventDataType;
	AliHLTEventDataOrigin fEventDataOrigin;
	AliUInt32_t fEventDataSpecification;

	struct timeval fLastNoEventFoundLog;

	AliUInt32_t fWrapCopyAreaStartOffset;
	AliUInt8_t* fWrapCopyAreaStartPtr;
	AliUInt32_t fWrapCopyAreaSize;

	//#define DEBUG
#ifdef DEBUG
	unsigned long fEventCount;
#endif

    private:
    };




AliHLTACEXPublisher::AliHLTACEXPublisher( const char* name, int eventSlotsPowerOfTwo ):
    AliHLTDetectorPublisher( name, eventSlotsPowerOfTwo )
    {
    fACEXHandler = NULL;
    fETP = &fETS;
    fETP->fHeader.fLength = sizeof(AliHLTEventTriggerStruct);
    fETP->fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
    fETP->fHeader.fSubType.fID = 0;
    fETP->fHeader.fVersion = 1;
    fETP->fDataWordCount = 0;

    fDoSleep = false;
    fSleepTime = 10000;

    fQuitEventLoop = false;
    fEventLoopQuitted = true;
    
    fShmPtr = NULL;
    fShmSize = 0;

    fOldestEventBirth = 0;

    fNodeID = AliHLTGetNodeID();
    fEventDataType.fID = UNKNOWN_DATAID;
    fEventDataOrigin.fID = UNKNOWN_DATAORIGIN;
    fEventDataSpecification = ~(AliUInt32_t)0;
    pthread_mutex_init( &fACEXHandlerMutex, NULL );

    gettimeofday( &fLastNoEventFoundLog, NULL );

    fWrapCopyAreaStartOffset = 0;
    fWrapCopyAreaStartPtr = NULL;
    fWrapCopyAreaSize = 0;

#ifdef DEBUG
    fEventCount = 0;
#endif

    }

AliHLTACEXPublisher::~AliHLTACEXPublisher()
    {
    pthread_mutex_destroy( &fACEXHandlerMutex );
    }

void AliHLTACEXPublisher::StopPublishing()
    {
    //fACEXHandler->DeactivateACEX();
    QuitEventLoop();
    fEventAnnouncer.Quit();
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 5000000;
    gettimeofday( &start, NULL );
    struct timespec ts;
    while ( !QuittedEventLoop() && deltaT<timeLimit )
	{
	ts.tv_sec = 0;
	ts.tv_nsec = 500000;
	nanosleep( &ts, NULL );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    
    }


int AliHLTACEXPublisher::WaitForEvent( AliEventID_t& eventID, AliHLTSubEventDataDescriptor*& sbevent,
					   AliHLTEventTriggerStruct*& trg )
    {
    sbevent = NULL;
    trg = NULL;
    AliUInt32_t dataOffset, dataSize, dataWrappedSize;
    int ret;
// 	printf( "%s:%d\n", __FILE__, __LINE__ );
    if ( !fQuitEventLoop )
	{
	do
	    {
	    pthread_mutex_lock( &fACEXHandlerMutex );
	    ret = fACEXHandler->PollForEvent( eventID, 
					      dataOffset, dataSize, dataWrappedSize );
	    pthread_mutex_unlock( &fACEXHandlerMutex );
	    if ( !ret )
		{
		LOG( AliHLTLog::kInformational, "AliHLTACEXPublisher::WaitForEvent", "Event found" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ") found: Data Offset " << dataOffset << " (0x" << AliHLTLog::kHex
		    << dataOffset << ") - Data Size: " << AliHLTLog::kDec << dataSize << " (0x"
		    << AliHLTLog::kHex << dataSize << ") - Wrapped Data Size: " << AliHLTLog::kDec
		    << dataWrappedSize << "(0x" << AliHLTLog::kHex << dataWrappedSize 
		    << ")." << ENDLOG;
		//fACEXHandler->ReleaseEvent( eventID );
		}
	    else if ( ret==EAGAIN )
		{
		struct timeval now;
		gettimeofday( &now, NULL );
		unsigned long long dt;
		dt = now.tv_sec - fLastNoEventFoundLog.tv_sec;
		dt *= 1000000;
		dt += now.tv_usec - fLastNoEventFoundLog.tv_usec;
		if ( dt >= 1000000 )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTACEXPublisher::WaitForEvent", "No event found" )
			<< "No event found." << ENDLOG;
		    fLastNoEventFoundLog = now;
		    }
		}
	    else // if ( ret && ret!=EAGAIN )
		{
		LOG( AliHLTLog::kError, "AliHLTACEXPublisher::WaitForEvent", "Error reading event from ACEX" )
		    << "Error reading event from ACEX: " << strerror(ret) << " ("
		    << AliHLTLog::kDec << ret << ")." << ENDLOG;
		return 0;
		}
	    if ( fDoSleep && ret==EAGAIN )
		usleep( fSleepTime );
	    }
	while ( !fQuitEventLoop && ret==EAGAIN );
// 	printf( "%s:%d\n", __FILE__, __LINE__ );
	if ( ret )
	    return 0;
// 	printf( "%s:%d\n", __FILE__, __LINE__ );
	if ( fQuitEventLoop )
	    return 1;
// 	printf( "%s:%d\n", __FILE__, __LINE__ );
	
#ifdef DEBUG
	fEventCount++;
	if ( fEventCount>=4 )
	    {
	    fACEXHandler->DeactivateACEX();
	    }
#endif

	if ( dataWrappedSize )
	    {
	    if ( fWrapCopyAreaSize>=dataSize )
		{
		memcpy( fWrapCopyAreaStartPtr, fShmPtr+dataOffset, dataSize-dataWrappedSize );
		memcpy( fWrapCopyAreaStartPtr+dataSize-dataWrappedSize, fShmPtr, dataWrappedSize );
		dataOffset = fWrapCopyAreaStartOffset;
		dataWrappedSize = 0;
		}
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTACEXPublisher::WaitForEvent", "Cannot copy wrapped event data" )
		    << "Cannot copy wrapped event data for linearization. Wrap buffer is too small (" << AliHLTLog::kDec
		    << fWrapCopyAreaSize << " (0x" << AliHLTLog::kHex << fWrapCopyAreaSize << ") bytes - "
		    << AliHLTLog::kDec << dataSize << " (0x" << AliHLTLog::kHex << dataSize << ") bytes needed)." 
		    << ENDLOG;
		}
	    }

	int cnt = 1;
	if ( dataWrappedSize )
	    cnt = 2;
	pthread_mutex_lock( &fSEDDMutex );
	sbevent = fDescriptors->GetFreeEventDescriptor( eventID, cnt );
	pthread_mutex_unlock( &fSEDDMutex );

// 	printf( "%s:%d\n", __FILE__, __LINE__ );

	struct timeval now;
	gettimeofday( &now, NULL );
	sbevent->fEventID = eventID;
	sbevent->fEventBirth_s = now.tv_sec;
	sbevent->fEventBirth_us = now.tv_usec;
	sbevent->fOldestEventBirth_s = fOldestEventBirth;
	sbevent->fStatusFlags = 0;
	unsigned long n = 0;
	if ( dataSize )
	    {
	    sbevent->fDataBlocks[n].fBlockOffset = dataOffset;
	    sbevent->fDataBlocks[n].fBlockSize = dataSize-dataWrappedSize;
	    sbevent->fDataBlocks[n].fShmID.fShmType = fShmKey.fShmType;
	    sbevent->fDataBlocks[n].fShmID.fKey.fID = fShmKey.fKey.fID;
	    sbevent->fDataBlocks[n].fDataType.fID = fEventDataType.fID; //ADCCOUNTS_DATAID;
	    sbevent->fDataBlocks[n].fDataOrigin.fID = fEventDataOrigin.fID;
	    sbevent->fDataBlocks[n].fDataSpecification = fEventDataSpecification;
	    sbevent->fDataBlocks[n].fStatusFlags = 0;
	    //sbevent->fDataBlocks[n].fByteOrder = kAliHLTNativeByteOrder;
	    AliHLTSubEventDescriptor::FillBlockAttributes( sbevent->fDataBlocks[n].fAttributes );
	    sbevent->fDataBlocks[n].fProducerNode = fNodeID;
	    n++;
	    }
	if ( dataWrappedSize )
	    {
	    sbevent->fDataBlocks[n].fBlockOffset = 0;
	    sbevent->fDataBlocks[n].fBlockSize = dataWrappedSize;
	    sbevent->fDataBlocks[n].fShmID.fShmType = fShmKey.fShmType;
	    sbevent->fDataBlocks[n].fShmID.fKey.fID = fShmKey.fKey.fID;
	    sbevent->fDataBlocks[n].fDataType.fID = fEventDataType.fID; //ADCCOUNTS_DATAID;
	    sbevent->fDataBlocks[n].fDataOrigin.fID = fEventDataOrigin.fID;
	    sbevent->fDataBlocks[n].fDataSpecification = fEventDataSpecification;
	    sbevent->fDataBlocks[n].fStatusFlags = 0;
	    //sbevent->fDataBlocks[n].fByteOrder = kAliHLTNativeByteOrder;
	    AliHLTSubEventDescriptor::FillBlockAttributes( sbevent->fDataBlocks[n].fAttributes );
	    sbevent->fDataBlocks[n].fProducerNode = fNodeID;
	    n++;
	    }
// 	printf( "%s:%d\n", __FILE__, __LINE__ );

	sbevent->fDataBlockCount = n;
	sbevent->fDataType.fID = COMPOSITE_DATAID;
	trg = fETP;

// 	printf( "%s:%d\n", __FILE__, __LINE__ );
	return 1;
	}
    else
	{
	fEventLoopQuitted = true;
	return 0;
	}
    }

void AliHLTACEXPublisher::EventFinished( AliEventID_t eventID, AliHLTSubEventDataDescriptor& /*sbevent*/, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    EventFinished( eventID, eventDoneData );
    }

void AliHLTACEXPublisher::EventFinished( AliEventID_t eventID, vector<AliHLTEventDoneData*>& /*eventDoneData*/ )
    {
    pthread_mutex_lock( &fACEXHandlerMutex );
    fACEXHandler->ReleaseEvent( eventID );
    pthread_mutex_unlock( &fACEXHandlerMutex );
    }


void AliHLTACEXPublisher::QuitEventLoop()
    {
    fQuitEventLoop = true;
    }

void AliHLTACEXPublisher::StartEventLoop()
    {
    fACEXHandler->ActivateACEX();
    fQuitEventLoop = false;
    }

void AliHLTACEXPublisher::EndEventLoop()
    {
    fACEXHandler->DeactivateACEX();
    }

void AliHLTACEXPublisher::PauseProcessing()
    {
    fACEXHandler->DeactivateACEX();
    AliHLTDetectorPublisher::PauseProcessing();
    }

void AliHLTACEXPublisher::ResumeProcessing()
    {
    AliHLTDetectorPublisher::ResumeProcessing();
    fACEXHandler->ActivateACEX();
    }





class ACEXPublisherComponent: public AliHLTControlledSourceComponent
    {
    public:

	ACEXPublisherComponent( const char* name, int argc, char** argv );
	virtual ~ACEXPublisherComponent() {};

    protected:

	virtual AliHLTDetectorPublisher* CreatePublisher();
	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

	virtual bool CheckConfiguration();
	virtual void ShowConfiguration();

	virtual bool CreateParts();
	virtual void DestroyParts();
	virtual bool SetupComponents();

	AliHLTEventDataType fEventDataType;
	AliHLTEventDataOrigin fEventDataOrigin;
	AliUInt32_t fEventDataSpecification;
	bool fDoSleep;
	unsigned long fSleepTime;
	bool fPCIDeviceInfoSet;
	AliUInt16_t fPCIVendorID;
	AliUInt16_t fPCIDeviceID;
	AliUInt16_t fPCIDeviceIndex;

	bool fFirstStart;

	AliUInt32_t fWrapAreaSize;
	bool fWrapAreaSizeSet;

	AliHLTACEX1Handler* fACEXHandler;

    private:
    };


ACEXPublisherComponent::ACEXPublisherComponent( const char* name, int argc, char** argv ):
    AliHLTControlledSourceComponent( name, argc, argv )
    {
    fEventDataType.fID = UNKNOWN_DATAID;
    fEventDataOrigin.fID = UNKNOWN_DATAORIGIN;
    fEventDataSpecification = ~(AliUInt32_t)0;
    fDoSleep = false;
    fSleepTime = 10000;
    fPCIDeviceInfoSet = false;
    fPCIVendorID = 0xFFFF;
    fPCIDeviceID = 0xFFFF;
    fPCIDeviceIndex = 0xFFFF;
    fACEXHandler = NULL;
    fFirstStart = true;
    fMaxPreBlockCount = 3;
    fWrapAreaSize = 0;
    fWrapAreaSizeSet = false;
    }


AliHLTDetectorPublisher* ACEXPublisherComponent::CreatePublisher()
    {
    return new AliHLTACEXPublisher( fOwnPublisherName.c_str(), fMaxPreEventCountExp2 );
    }


const char* ACEXPublisherComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cpErr = NULL;
    if ( !strcmp( argv[i], "-device" ) )
	{
	if ( argc <= i+3 )
	    return "Missing pci device information parameter";
	fPCIVendorID = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting pci vendor specifier.";
	    }
	fPCIDeviceID = strtoul( argv[i+2], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+2;
	    return "Error converting pci device specifier.";
	    }
	fPCIDeviceIndex = strtoul( argv[i+3], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+3;
	    return "Error converting pci device index specifier.";
	    }
	fPCIDeviceInfoSet = true;
	i += 4;
	return NULL;
	}
    if ( !strcmp( argv[i], "-sleep" ) )
	{
	fDoSleep = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-restart" ) )
	{
	fFirstStart = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-sleeptime" ) )
	{
	if ( argc <= i+1 )
	    return "Missing sleep time specifier.";
	fSleepTime = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting sleep time specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-datatype" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing datatype specifier.";
	    }
	if ( strlen( argv[i+1])>8 )
	    {
	    return "Maximum allowed length for datatype specifier is 8 characters.";
	    }
	AliUInt64_t tmpDT = 0;
	unsigned tmpN;
	for( tmpN = 0; tmpN < strlen( argv[i+1]); tmpN++ )
	    {
	    tmpDT = (tmpDT << 8) | (AliUInt64_t)(argv[i+1][tmpN]);
	    }
	for ( ; tmpN < 8; tmpN++ )
	    {
	    tmpDT = (tmpDT << 8) | (AliUInt64_t)(' ');
	    }
	fEventDataType.fID = tmpDT;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-dataorigin" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing dataorigin specifier.";
	    }
	if ( strlen( argv[i+1])>4 )
	    {
	    return "Maximum allowed length for dataorigin specifier is 4 characters.";
	    }
	AliUInt32_t tmpDO = 0;
	unsigned tmpN;
	for( tmpN = 0; tmpN < strlen( argv[i+1]); tmpN++ )
	    {
	    tmpDO = (tmpDO << 8) | (AliUInt32_t)(argv[i+1][tmpN]);
	    }
	for ( ; tmpN < 4; tmpN++ )
	    {
	    tmpDO = (tmpDO << 8) | (AliUInt32_t)(' ');
	    }
	fEventDataOrigin.fID = tmpDO;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-dataspec" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing event data specification specifier.";
	    }
	AliUInt32_t tmpll = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting event data specification specifier..";
	    }
	fEventDataSpecification = tmpll;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-wrapareasize" ) )
	{
	if ( argc <= i+1 )
	    return "Missing wrap area size specifier.";
	fWrapAreaSize = strtoul( argv[i+1], &cpErr, 0 );
	fWrapAreaSizeSet = true;
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting wrap area size specifier.";
	    }
	i += 2;
	return NULL;
	}
    return AliHLTControlledSourceComponent::ParseCmdLine( argc, argv, i, errorArg );
    }

void ACEXPublisherComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    AliHLTControlledSourceComponent::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-device <pciVendorID> <pciDeviceID> <pciDeviceIndex>: Specify the PCI device to use. (Mandatory)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-sleep: Sleep for a specified time when no event is available. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-sleeptime <sleeptime_usec>: Specify the time in microseconds to sleep (with -sleep) when no event is available. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-datatype <type>: Specify the type as which the data is to be specified. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-dataorigin <type>: Specify the origin to be specified for the data. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-dataspec <type>: Specify the specifier that is to be used to characterize the data. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-restart: Specify that this is a restart, do not initialize ACEX and erase memories. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-wrapareasize <wrap-area-size>: Specify the size of the area used to unwrap blocks wrapped in the ACEX buffer. (Optional)" << ENDLOG;
    }

bool ACEXPublisherComponent::CheckConfiguration()
    {
    if ( !AliHLTControlledComponent::CheckConfiguration() )
	return false;
    if ( !fPCIDeviceInfoSet )
	{
	LOG( AliHLTLog::kError, "ACEXPublisherComponent::CheckConfiguration", "No pci device specified" )
	    << "Must specify the PCI device to be used (e.g. using the -device command line option."
	    << ENDLOG;
	return false;
	}
    unsigned long wrapAreaSize = fWrapAreaSize;
    if ( !fWrapAreaSizeSet )
	wrapAreaSize = fMinBlockSize;
    if ( wrapAreaSize >= fBufferSize )
	{
	LOG( AliHLTLog::kError, "ACEXPublisherComponent::CheckConfiguration", "Wrap Area Too Large" )
	    << (fWrapAreaSizeSet ? "Wrap area size specified" : "Default wrap area size (minimum block size)")
	    << " is at least as large as buffer size. It must be smaller."
	    << ENDLOG;
	return false;
	}
    return true;
    }

void ACEXPublisherComponent::ShowConfiguration()
    {
    AliHLTControlledComponent::ShowConfiguration();
    LOG( AliHLTLog::kInformational, "ACEXPublisherComponent::ShowConfiguration", "PCI Device Configuration" )
	<< "PCI device: Vendor ID: 0x" << AliHLTLog::kHex << fPCIVendorID << " - " << " Device ID: 0x"
	<< fPCIDeviceID << " - Device Index: " << AliHLTLog::kDec << fPCIDeviceIndex << "." << ENDLOG;
    LOG( AliHLTLog::kInformational, "ACEXPublisherComponent::ShowConfiguration", "Start Mode" )
	<< "Start Mode: " << (fFirstStart ? "First Start" : "Restart" ) << "." << ENDLOG;
    if ( fDoSleep )
	{
	LOG( AliHLTLog::kInformational, "ACEXPublisherComponent::ShowConfiguration", "Sleep Mode" )
	    << "Sleeping between events for " << AliHLTLog::kDec << fSleepTime << " microseconds." << ENDLOG;
	
	}
    if ( !fWrapAreaSizeSet )
	{
	LOG( AliHLTLog::kWarning, "ACEXPublisherComponent::ShowConfiguration", "Wrap Area" )
	    << "Wrap area size not set. Using minimum block size as default." << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kInformational, "ACEXPublisherComponent::ShowConfiguration", "Wrap Area" )
	    << "Wrap area size set to 0x" << AliHLTLog::kHex << fWrapAreaSize << " ("
	    << AliHLTLog::kDec << fWrapAreaSize << ")." << ENDLOG;
	}
    }

bool ACEXPublisherComponent::CreateParts()
    {
    if ( !AliHLTControlledComponent::CreateParts() )
	return false;
    fACEXHandler = new AliHLTACEX1Handler( fBufferSize, fMaxPreEventCountExp2 );
    if ( !fACEXHandler )
	{
	LOG( AliHLTLog::kError, "ACEXPublisherComponent::CreateParts", "Unable to create ACEX handler object" )
	    << "Unable to create ACEX handler object." << ENDLOG;
	return false;
	}
    return true;
    }

void ACEXPublisherComponent::DestroyParts()
    {
    if ( fACEXHandler )
	{
	fACEXHandler->DeinitializeACEX();
	fACEXHandler->Close();
	delete fACEXHandler;
	fACEXHandler = NULL;
	if ( fShmManager )
	    fShmManager->ReleaseShm( fShmKey );
	}
    AliHLTControlledComponent::DestroyParts();
    }

bool ACEXPublisherComponent::SetupComponents()
    {
    if ( !AliHLTControlledComponent::SetupComponents() )
	return false;

    if ( fPersistentState && fPersistentState->DidExist() && (*fPersistentState) && 
	 (*reinterpret_cast< AliHLTPersistentState<AliHLTDetectorPublisher::State>* >(fPersistentState))->fLastEventID!=AliEventID_t() )
	fFirstStart = false;

    if ( !fWrapAreaSizeSet )
	fWrapAreaSize = fMinBlockSize;
    AliUInt8_t* eventBuffer;
    eventBuffer = fShmManager->GetShm( fShmKey );
    int ret;
    ret = fACEXHandler->Open( fPCIVendorID, fPCIDeviceID, fPCIDeviceIndex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "ACEXPublisherComponent::SetupComponents", "Error opening ACEX" )
	    << "Error opening ACEX device: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return false;
	}
    ret = fACEXHandler->SetEventBuffer( eventBuffer, fBufferSize-fWrapAreaSize, fFirstStart );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "ACEXPublisherComponent::SetupComponents", "Error obtaining event buffer shared memory" )
	    << "Error obtaining event buffer shared memory: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return false;
	}
    ret = fACEXHandler->InitializeACEX( fFirstStart );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "ACEXPublisherComponent::SetupComponents", "Error initializing ACEX" )
	    << "Error initializing ACEX hardware: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return false;
	}
    ret = fACEXHandler->InitializeFromACEX( fFirstStart );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "ACEXPublisherComponent::SetupComponents", "Error initializing from ACEX" )
	    << "Error initializing ACEX handler object from ACEX hardware: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return false;
	}
    
    ((AliHLTACEXPublisher*)fPublisher)->SetEventDataType( fEventDataType );
    ((AliHLTACEXPublisher*)fPublisher)->SetEventDataOrigin( fEventDataOrigin );
    ((AliHLTACEXPublisher*)fPublisher)->SetEventDataSpecification( fEventDataSpecification );
    ((AliHLTACEXPublisher*)fPublisher)->SetSleep( fDoSleep );
    ((AliHLTACEXPublisher*)fPublisher)->SetShm( fShmKey, eventBuffer, fBufferSize-fMinBlockSize );
    ((AliHLTACEXPublisher*)fPublisher)->SetSleepTime( fSleepTime );
    ((AliHLTACEXPublisher*)fPublisher)->SetACEXHandler( fACEXHandler );
    ((AliHLTACEXPublisher*)fPublisher)->SetWrapCopyArea( eventBuffer+(fBufferSize-fWrapAreaSize), fBufferSize-fWrapAreaSize, fWrapAreaSize );
    return true;
    }



int main( int argc, char** argv )
    {
    ACEXPublisherComponent procComp( "ACEXPublisher", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }









/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
