/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "ACEXHandler.hpp"
#include "AliHLTDDLHeader.hpp"
#include "AliHLTDDLHeaderData.h"
#include "AliHLTLog.hpp"
#include <netinet/in.h>
#include <errno.h>


const AliUInt32_t AliHLTACEX1Handler::kRESET              = 0x00;
const AliUInt32_t AliHLTACEX1Handler::kLED                = 16;
const AliUInt32_t AliHLTACEX1Handler::kWRADDR             = 48;

#ifdef ACEX_ORI_READOUT
const AliUInt32_t AliHLTACEX1Handler::kDATAAREASTART      = 0x00080000; // Offset is at 0x20000 32 bit words
#else
const AliUInt32_t AliHLTACEX1Handler::kEOEMARKER          = 0x80000000;
const AliUInt32_t AliHLTACEX1Handler::kTIMEOUT            = 0x40000000;
const AliUInt32_t AliHLTACEX1Handler::kEOE_TIMEOUT        = AliHLTACEX1Handler::kEOEMARKER|AliHLTACEX1Handler::kTIMEOUT;
#endif
const AliUInt32_t AliHLTACEX1Handler::kWORDCNTMASK        = 0x0003FFFF; // Memory size is 2^17 32 bit words, word counter is in units of 16 bit words


#ifdef ACEX_ORI_READOUT
const unsigned long AliHLTACEX1Handler::kMaxPollInterval  = 300; // Microseconds, readout should be finished after 150 musec, so this should give enough safety margin
#endif



AliHLTACEX1Handler::AliHLTACEX1Handler( unsigned long /*dataBufferSize*/, int eventSlotsExp2 ):
    fEvents( eventSlotsExp2 )
    {
    pthread_mutex_init( &fEventMutex, NULL );

    fNextEventID = 0;

    fPollMsg = false;
    fDataAreaPtr = NULL;
    fDataAreaSize = 0;
    }

AliHLTACEX1Handler::~AliHLTACEX1Handler()
    {
    pthread_mutex_destroy( &fEventMutex );
    }

int AliHLTACEX1Handler::Open( AliUInt16_t pciVendorID, AliUInt16_t pciDeviceID, AliUInt16_t pciDeviceIndex )
    {
    char tmp[ 512+1 ];
    PSI_Status    status;

    fBar0ID = "/dev/psi/vendor/"; // 0x%04hX/device/0x%04hX/0/base0
    snprintf( tmp, 512, "0x%04X", pciVendorID );
    fBar0ID += tmp;
    fBar0ID += "/device/";
    snprintf( tmp, 512, "0x%04X", pciDeviceID );
    fBar0ID += tmp;
    fBar0ID += "/";
    snprintf( tmp, 512, "%04X", pciDeviceIndex );
    fBar0ID += tmp;
    fBar0ID += "/base0";

    LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::Open", "Opening region" )
      << "Opening region '" << fBar0ID.c_str() << "'." << ENDLOG;
  
    status = PSI_openRegion( &fBar0Region, fBar0ID.c_str() );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTACEX1Handler::Open", "Unable to open ACEX device bar 0 region" )
	    << "Unable to open ACEX device bar 0 region '" << fBar0ID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}

    status = PSI_mapRegion( fBar0Region, (void**)&fBar0Ptr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTACEX1Handler::Open", "Unable to map ACEX device bar 0 region" )
	    << "Unable to map ACEX device bar 0 region '" << fBar0ID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}

    tRegionStatus stat;
    status = PSI_getRegionStatus( fBar0Region, &stat );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTACEX1Handler::Open", "Unable to get ACEX device bar 0 region information" )
	    << "Unable to get information for ACEX device bar 0 region '" << fBar0ID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}

    fBar0Size = stat.size;
    if ( !fBar0Size )
	{
	LOG( AliHLTLog::kError, "AliHLTACEX1Handler::Open", "ACEX device bar 0 region size" )
	    << "ACEX device bar 0 region size is zero." << ENDLOG;
	return EIO;
	}

#ifdef ACEX_ORI_READOUT
    fDataAreaSize = fBar0Size-kDATAAREASTART;
    fDataAreaPtr = fBar0Ptr+kDATAAREASTART;
#else
    fDataAreaSize = fBar0Size/2;
    fDataAreaPtr = fBar0Ptr+fBar0Size/2;
#endif
    
    return 0;
    }
	
int AliHLTACEX1Handler::Close()
    {
    PSI_unmapRegion( fBar0Region, (void*)fBar0Ptr );
    PSI_closeRegion( fBar0Region );
    return 0;
    }

int AliHLTACEX1Handler::SetEventBuffer( AliUInt8_t* dataBuffer, unsigned long dataBufferSize, bool /*first*/ )
    {
    fEventBufferWritePtr = fEventBufferReadPtr = fEventBuffer = dataBuffer;
    fEventBufferSize = dataBufferSize;
    fEventBufferEnd = fEventBuffer + fEventBufferSize;
    fEventBufferReleasedOffset = 0;
        
    LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::SetEventBuffer", "Event buffer pointers" )
	<< "EventBuffer: 0x" << AliHLTLog::kHex << (unsigned long)fEventBuffer << "." << ENDLOG;
    
    return 0;
    }

int AliHLTACEX1Handler::InitializeACEX( bool first )
    {
    int ret;
    ret = ACEXReset();
    if ( ret )
	return ret;
    /*
     *   invalidate DMA and report buffer
     */
    if ( first )
	{
	memset( (void*)fEventBuffer, 0, fEventBufferSize );
	}
    
    return 0;
    }

int AliHLTACEX1Handler::DeinitializeACEX()
    {
    return 0;
    }

int AliHLTACEX1Handler::InitializeFromACEX( bool /*first*/ )
    {
    return 0;
    }

int AliHLTACEX1Handler::SetOptions( const vector<MLUCString>& /*options*/, char*& /*errorMsg*/, unsigned& /*errorArgNr*/ )
    {
    return 0;
    }

void AliHLTACEX1Handler::GetAllowedOptions( vector<MLUCString>& options )
    {
    options.clear();
    }


int AliHLTACEX1Handler::ActivateACEX()
    {
    return 0;
    }

int AliHLTACEX1Handler::DeactivateACEX()
    {
    return 0;
    }

int AliHLTACEX1Handler::PollForEvent( AliEventID_t& eventID, 
				      AliUInt32_t& dataOffset, AliUInt32_t& dataSize, AliUInt32_t& dataWrappedSize )
    {
    if ( !fPollMsg )
	{
	LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::PollForEvent", "Polling for event" )
	    << "Polling for event." << ENDLOG;
	fPollMsg = true;
	}
    AliUInt32_t writeAddr;
    writeAddr = *((AliVolatileUInt32_t*)( fBar0Ptr + kWRADDR ));
    //if ( (writeAddr & kEOE_TIMEOUT) != 0 )
    bool foundEvent = false;
#ifdef ACEX_ORI_READOUT
    if ( writeAddr != 0 )
	{
	AliUInt32_t oldWriteAddr;
	// fMaxPollInterval
	struct timeval t1,t2;
	unsigned long dt;
	gettimeofday( &t1, NULL );
	do
	    {
	    oldWriteAddr = writeAddr;
	    writeAddr = *((AliVolatileUInt32_t*)( fBar0Ptr + kWRADDR ));
	    gettimeofday( &t2, NULL );
	    if ( writeAddr != oldWriteAddr )
		{
		dt = 0;
		gettimeofday( &t1, NULL );
		}
	    else
		{
		gettimeofday( &t2, NULL );
		dt = (t2.tv_sec-t1.tv_sec)*1000000+(t2.tv_usec-t1.tv_usec);
		}
	    }
	while ( (writeAddr != oldWriteAddr) || (dt<kMaxPollInterval) );
	foundEvent = true;
	}
#else
    if ( (writeAddr & kEOEMARKER) != 0 )
	foundEvent = true;
#endif
    if ( foundEvent )
	{
	EventData ed;
	LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::PollForEvent", "Found event" )
	    << "Write address: 0x" << AliHLTLog::kHex << writeAddr << " ("
	    << AliHLTLog::kDec << writeAddr << ")." << ENDLOG;
	// write word contains data size in 16 bit words, dataSize should be in bytes.
	dataSize = (writeAddr & kWORDCNTMASK) << 1;
	if ( dataSize % 4 )
	  {
	    dataSize += 4 - ( dataSize % 4 );
	    LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::PollForEvent", "Adapting size" )
	      << "Adapting size to " << AliHLTLog::kDec << dataSize << " (4 byte alignment)."
	      << ENDLOG;
	  }
	unsigned long free, oldFree, used, oldUsed;
	oldFree = free = GetFree();
	oldUsed = used = GetUsed();
	if ( free < dataSize )
	    {
	    LOG( AliHLTLog::kInformational, "AliHLTACEX1Handler::PollForEvent", "Found event is too big" )
		<< "Found event of " << AliHLTLog::kDec << dataSize << " (0x" << AliHLTLog::kHex
		<< dataSize << ") is too big. fEventBuffer: 0x" << AliHLTLog::kHex
		<< (unsigned long)fEventBuffer << " - fEventBufferEnd 0x" << (unsigned long)fEventBufferEnd
		<< " - fEventBufferReadPtr: 0x" << (unsigned long)fEventBufferReadPtr
		<< " - fEventBufferWritePtr: 0x" << (unsigned long)fEventBufferWritePtr
		<< AliHLTLog::kDec << " - free: " << free << " - used: " 
		<< used << " - fEventBufferSize: " << fEventBufferSize
		<< "." << ENDLOG;
	    return EAGAIN;
	    }
	if ( dataSize <=0 )
	  {
	    LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::PollForEvent", "Event size is 0" )
	      << "Found event has size 0." << ENDLOG;
	    return EAGAIN;
	  }
	fPollMsg = false;

	ed.fSize = dataSize;
 	ed.fEventID = eventID = ++fNextEventID;

	LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::PollForEvent", "Found event" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") found with " << AliHLTLog::kDec << dataSize << " (0x" << AliHLTLog::kHex << dataSize
	    << ") bytes." << ENDLOG;

	AliVolatileUInt8_t* oldEventBufferWritePtr = fEventBufferWritePtr;

#if 0
	memcpy( (void*)fEventBufferWritePtr, (void*)fDataAreaPtr, dataSize );
	fEventBufferWritePtr += dataSize;
	if ( fEventBufferWritePtr>=fEventBufferEnd )
	    fEventBufferWritePtr = fEventBuffer+(fEventBufferWritePtr-fEventBufferEnd);

#else
	AliVolatileUInt32_t *evtWrite, *evtRead, *evtReadEnd;
	evtWrite = (AliVolatileUInt32_t*)fEventBufferWritePtr;
	evtRead = (AliVolatileUInt32_t*)fDataAreaPtr;
	evtReadEnd = (AliVolatileUInt32_t*)( fDataAreaPtr + dataSize );
	while ( evtRead < evtReadEnd )
	  {
	    *evtWrite++ = *evtRead++;
	    if ( (AliVolatileUInt8_t*)evtWrite == fEventBufferEnd )
	      evtWrite = (AliVolatileUInt32_t*)fEventBuffer;
	  }
	fEventBufferWritePtr = (AliVolatileUInt8_t*)evtWrite;
#endif

	ed.fOffset = dataOffset = oldEventBufferWritePtr - fEventBuffer;

// 	LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::PollForEvent", "Event found" )
// 	    << "Found event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
// 	    << eventID << ")." << ENDLOG;
	LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::PollForEvent", "Found event" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << "): Data: " << dataSize << " (0x" << AliHLTLog::kHex << dataSize
	    << ") bytes - data offset 0x" 
	    << AliHLTLog::kHex << dataOffset << " (" << AliHLTLog::kDec
	    << dataOffset << ")." << ENDLOG;

	dataWrappedSize = ed.fWrappedSize = 0;
	if ( fEventBufferWritePtr<oldEventBufferWritePtr )
	    {
	    ed.fWrappedSize = dataWrappedSize = fEventBufferWritePtr-fEventBuffer;
	    LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::PollForEvent", "Event wrapped" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") is wrapped. Wrapped size: " << ed.fWrappedSize << " (0x"
		<< AliHLTLog::kHex << ed.fWrappedSize << ")." << ENDLOG;
	    }

	pthread_mutex_lock( &fEventMutex );
	fEvents.Add( ed );
	pthread_mutex_unlock( &fEventMutex );

	free = GetFree();
	used = GetUsed();
	if ( free!=oldFree-dataSize || used!=oldUsed+dataSize )
	    {
	    LOG( AliHLTLog::kFatal, "AliHLTACEX1Handler::PollForEvent", "Internal Error" )
		<< "Internal buffer error: dataSize " << AliHLTLog::kDec << dataSize
		<< " - free: " << free << " - oldFree: " << oldFree << " - used: "
		<< used << " - oldUsed: " << oldUsed << " - fEventBuffer: 0x"
		<< AliHLTLog::kHex << (unsigned long)fEventBuffer << " - fEventBufferEnd: 0x"
		<< (unsigned long)fEventBufferEnd << " - fEventBufferWritePtr: 0x"
		<< (unsigned long)fEventBufferWritePtr << " - fEventBufferReadPtr: 0x"
		<< (unsigned long)fEventBufferReadPtr << "." << ENDLOG;
	    }

	return ACEXReset();
	}
    return EAGAIN;
    }

int AliHLTACEX1Handler::ReleaseEvent( AliEventID_t eventID )
    {
    unsigned long ndx;
    EventData* edp;
    vector<BlockData>::iterator iter, end;
    LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::ReleaseEvent", "Releasing event" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << ") to be released." << ENDLOG;
    pthread_mutex_lock( &fEventMutex );
    if ( MLUCVectorSearcher<EventData,AliEventID_t>::FindElement( fEvents, &EventDataSearchFunc, eventID, ndx ) )
	{
	edp = fEvents.GetPtr( ndx );
	LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::ReleaseEvent", "Released event data" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << "): " << edp->fSize << " (0x" << AliHLTLog::kHex << edp->fSize
	    << ") bytes - wrapped size: " << AliHLTLog::kDec << edp->fWrappedSize
	    << " (0x" << AliHLTLog::kHex << edp->fWrappedSize << ") - offset 0x" 
	    << AliHLTLog::kHex << edp->fOffset << " (" << AliHLTLog::kDec
	    << edp->fOffset << ") - current event buffer release offset: 0x"
	    << AliHLTLog::kHex << fEventBufferReleasedOffset << " (" << AliHLTLog::kDec
	    << fEventBufferReleasedOffset << ")." << ENDLOG;
	if ( fEventBufferReleasedOffset == edp->fOffset )
	    {
	    if ( edp->fWrappedSize )
		{
		fEventBufferReleasedOffset = edp->fWrappedSize;
		}
	    else
		{
		fEventBufferReleasedOffset += edp->fSize;
		}
	    if ( fEventBufferReleasedOffset == fEventBufferSize )
		fEventBufferReleasedOffset = 0;
	    iter = fBlocks.begin();
	    while ( iter!=fBlocks.end() && iter->fOffset==fEventBufferReleasedOffset )
		{
		fEventBufferReleasedOffset += iter->fSize;
		fBlocks.erase( iter );
		if ( fEventBufferReleasedOffset >= fEventBufferSize )
		    fEventBufferReleasedOffset = fEventBufferReleasedOffset-fEventBufferSize;
		iter = fBlocks.begin();
		}
	    LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::ReleaseEvent", "New release offset" )
		<< "New event buffer release offset: 0x" << AliHLTLog::kHex 
		<< fEventBufferReleasedOffset << " (" << AliHLTLog::kDec
		<< fEventBufferReleasedOffset << ") - fEventBufferReadPtr: 0x" << AliHLTLog::kHex
		<< (unsigned long)(fEventBuffer+fEventBufferReleasedOffset) << "." << ENDLOG;
	    fEventBufferReadPtr = fEventBuffer+fEventBufferReleasedOffset;
	    }
	else
	    {
	    bool repeat = false;
	    do
		{
		repeat = false;
		iter = fBlocks.begin();
		end = fBlocks.end();
		if ( edp->fOffset < fEventBufferReleasedOffset )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::ReleaseEvent", "Wrapped block" )
			<< "Skipping for wrapped block part." << ENDLOG;
		    while ( iter != end )
			{
			if ( iter+1==end || iter->fOffset>(iter+1)->fOffset )
			    break;
			iter++;
			}
		    }
		bool inserted = false;
		BlockData bd;
		while ( iter != end )
		    {
		    if ( edp->fOffset+edp->fSize == iter->fOffset )
			{
			LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::ReleaseEvent", "Merging block" )
			    << "Merging block at beginning: (" << AliHLTLog::kDec
			    << edp->fOffset << "/" << edp->fSize << ") + (" << iter->fOffset
			    << "/" << iter->fSize << ") == (" << edp->fOffset << "/"
			    << iter->fSize+edp->fSize << ")." << ENDLOG;
			iter->fOffset = edp->fOffset;
			iter->fSize += edp->fSize;
			inserted = true;
			if ( iter!=fBlocks.begin() && (iter-1)->fOffset+(iter-1)->fSize==iter->fOffset )
			    {
			    LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::ReleaseEvent", "Merging block" )
				<< "Merging in preceeding block at beginning: (" << AliHLTLog::kDec
				<< (iter-1)->fOffset << "/" << (iter-1)->fSize << ") + (" << iter->fOffset
				<< "/" << iter->fSize << ") == (" << (iter-1)->fOffset << "/"
				<< iter->fSize+(iter-1)->fSize << ")." << ENDLOG;
			    (iter-1)->fSize += iter->fSize;
			    fBlocks.erase( iter );
			    }
			break;
			}
		    if ( iter->fOffset+iter->fSize == edp->fOffset )
			{
			LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::ReleaseEvent", "Merging block" )
			    << "Merging block at end: (" << AliHLTLog::kDec
			    << iter->fOffset << "/" << iter->fSize << ") + (" << edp->fOffset 
			    << "/" << edp->fSize << ") == (" << iter->fOffset << "/"
			    << iter->fSize+edp->fSize << ")." << ENDLOG;
			iter->fSize += edp->fSize;
			inserted = true;
			if ( (iter+1)!=end && (iter+1)->fOffset==iter->fOffset+iter->fSize )
			    {
			    LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::ReleaseEvent", "Merging block" )
				<< "Merging in succeeding block at end: (" << AliHLTLog::kDec
				<< iter->fOffset << "/" << iter->fSize << ") + (" << (iter+1)->fOffset
				<< "/" << (iter+1)->fSize << ") == (" << iter->fOffset << "/"
				<< iter->fSize+(iter+1)->fSize << ")." << ENDLOG;
			    iter->fSize += (iter+1)->fSize;
			    fBlocks.erase( iter+1 );
			    }
			break;
			}
		    if ( edp->fOffset < iter->fOffset )
			{
			LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::ReleaseEvent", "Inserting block" )
			    << "Inserting block (" << AliHLTLog::kDec
			    << edp->fOffset << "/" << edp->fSize << ") before (" << iter->fOffset 
			    << "/" << iter->fSize << ")." << ENDLOG;
			bd.fOffset = edp->fOffset;
			bd.fSize = edp->fSize;
			fBlocks.insert( iter, bd );
			inserted = true;
			break;
			}
		    iter++;
		    }
		if ( !inserted )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::ReleaseEvent", "Appending block" )
			<< "Appending block: (" << AliHLTLog::kDec
			<< edp->fOffset << "/" << edp->fSize 
			<< ") at end." << ENDLOG;
		    bd.fOffset = edp->fOffset;
		    bd.fSize = edp->fSize;
		    fBlocks.insert( end, bd );
		    }
		if ( edp->fWrappedSize )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::ReleaseEvent", "Wrapping block" )
			<< "Wrapping block. (" << AliHLTLog::kDec
			<< edp->fOffset << "/" << edp->fSize << ") -> (0/"
			<< edp->fWrappedSize << ")." << ENDLOG;
		    repeat = true;
		    edp->fOffset = 0;
		    edp->fSize = edp->fWrappedSize;
		    edp->fWrappedSize = 0;
		    }
		}
	    while ( repeat );
	    }
	fEvents.Remove( ndx );
	pthread_mutex_unlock( &fEventMutex );
	return 0;
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTACEX1Handler::ReleaseEvent", "Event not found" )
	    << "Event ox" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") to be released could not be found in list. Giving up..." 
	    << ENDLOG;
	pthread_mutex_unlock( &fEventMutex );
	return EIO;
	}
    }


int AliHLTACEX1Handler::ACEXReset()
    {
    PSI_Status    status;
    unsigned long wr;

    LOG( AliHLTLog::kDebug, "AliHLTACEX1Handler::ACEXReset", "Resetting" )
      << "Resetting ACEX card." << ENDLOG;

    /*
     * ACEX reset
     */
    wr = 0;
    status = PSI_write( fBar0Region, (AliUInt32_t)kRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTACEX1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}

    /*
     * ACEX LED
     */
    wr = 0x55;
    status = PSI_write( fBar0Region, (AliUInt32_t)kLED, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTACEX1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    return 0;
    }


unsigned long AliHLTACEX1Handler::GetFree()
    {
    if ( fEventBufferWritePtr < fEventBufferReadPtr )
	return fEventBufferReadPtr-fEventBufferWritePtr-1;
    else
	return fEventBufferReadPtr + fEventBufferSize -fEventBufferWritePtr-1;
    }

unsigned long AliHLTACEX1Handler::GetUsed()
    {
    if ( fEventBufferReadPtr <= fEventBufferWritePtr )
	return fEventBufferWritePtr-fEventBufferReadPtr;
    else
	return fEventBufferWritePtr+fEventBufferSize-fEventBufferReadPtr;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
