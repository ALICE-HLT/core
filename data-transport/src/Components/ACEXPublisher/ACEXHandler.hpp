#ifndef _ALIHLTACEX1HANDLER_HPP_
#define _ALIHLTACEX1HANDLER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include <AliEventID.h>
#include "MLUCString.hpp"
#include "MLUCVector.hpp"
#include <vector>
#include <psi.h>
#include <psi_error.h>

using namespace std;

#define ACEX_ORI_READOUT

class AliHLTACEX1Handler
    {
    public:

	typedef volatile AliUInt8_t AliVolatileUInt8_t;
	typedef volatile AliUInt32_t AliVolatileUInt32_t;
	//typedef void* AliHLTVoidPtr;
	//typedef volatile AliHLTVoidPtr AliHLTVolatileVoidPtr;
	typedef volatile void* AliHLTVolatileVoidPtr;

	AliHLTACEX1Handler( unsigned long dataBufferSize, int eventSlotsExp2=-1 );
	
	virtual ~AliHLTACEX1Handler();

	virtual int Open( AliUInt16_t pciVendorID, AliUInt16_t pciDeviceID, AliUInt16_t pciDeviceIndex );
	virtual int Close();

	virtual int SetEventBuffer( AliUInt8_t* dataBuffer, unsigned long dataBufferSize, bool first = true );

	virtual int InitializeACEX( bool first = true );
	virtual int DeinitializeACEX();

	virtual int InitializeFromACEX( bool first = true );

	virtual int SetOptions( const vector<MLUCString>& options, char*& errorMsg, unsigned& errorArgNr );
	virtual void GetAllowedOptions( vector<MLUCString>& options );

	virtual int ActivateACEX();
	virtual int DeactivateACEX();

	virtual int PollForEvent( AliEventID_t& eventID, 
				  AliUInt32_t& dataOffset, AliUInt32_t& dataSize, AliUInt32_t& dataWrappedSize );

	virtual int ReleaseEvent( AliEventID_t eventID );

	int ACEXReset();

	unsigned long GetFree();
	unsigned long GetUsed();

    protected:

	struct EventData
	    {
		AliEventID_t fEventID;
		AliUInt32_t fOffset;
		AliUInt32_t fSize;
		AliUInt32_t fWrappedSize;
	    };

	MLUCVector<EventData> fEvents;
	pthread_mutex_t fEventMutex;


	static const AliUInt32_t kRESET;   //  0x00
	static const AliUInt32_t kLED;  //  4*4
	static const AliUInt32_t kWRADDR;  //  12*4

#ifdef ACEX_ORI_READOUT
	static const AliUInt32_t kDATAAREASTART; // 0x20000 32 bit words == 0x80000
#else
	static const AliUInt32_t kEOEMARKER; // 0x80000000
	static const AliUInt32_t kTIMEOUT; // 0x40000000;
	static const AliUInt32_t kEOE_TIMEOUT; // 0xC0000000;
#endif
	static const AliUInt32_t kWORDCNTMASK; // 0x0x0001FFFF;

#ifdef ACEX_ORI_READOUT
	static const unsigned long kMaxPollInterval;
#endif

	AliUInt32_t fEventBufferSize;

	MLUCString fBar0ID;

	tRegion fBar0Region;

	AliVolatileUInt8_t* fBar0Ptr;

	AliUInt32_t fBar0Size;

	AliVolatileUInt8_t* fEventBuffer;
	AliVolatileUInt8_t* fEventBufferEnd;
	AliVolatileUInt8_t* fEventBufferReadPtr;
	AliVolatileUInt8_t* fEventBufferWritePtr;

	AliVolatileUInt8_t* fDataAreaPtr;
	AliUInt32_t fDataAreaSize;


	unsigned long fEventBufferReleasedOffset;

	struct BlockData
	    {
		AliUInt32_t fOffset;
		AliUInt32_t fSize;
	    };
	vector<BlockData> fBlocks;
	// fBlocks is protected by fEventMutex as well.
	bool fWrap;
	
	static bool EventDataSearchFunc( const EventData& ed, const AliEventID_t& searchData )
		{
		return ed.fEventID == searchData;
		}


	AliEventID_t fNextEventID;

	bool fPollMsg;

    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTACEX1HANDLER_HPP_
