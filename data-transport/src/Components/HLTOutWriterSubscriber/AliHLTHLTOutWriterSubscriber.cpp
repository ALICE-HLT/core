/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#include "AliHLTHLTOutWriterSubscriber.hpp"
#include "AliHLTDDLHeader.hpp"
#include "AliHLTReadoutList.hpp"
#include "AliHLTDDLHeaderData.h"
#include "AliHLTDDLHeader.hpp"
#include "AliHLTEventWriter.hpp"
#include <errno.h>

AliHLTHLTOutWriterSubscriber::AliHLTHLTOutWriterSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize,
							    AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, int eventCountAlloc ):
    AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc ),
    fEventLog( 10, eventCountAlloc, true )
    {
    fDoSleep = true;
    fSleepTime = 10000;
    
    fShmPtr = NULL;
    fShmSize = 0;

    fHLTOutHandler = NULL;

    fQuitDonePoll = true;

    fSimpleMode = false;

    fDDLID = ~0UL;

    fDisable = false;

    fNotDoneWarnTime_us = 0;
    fNotDoneWarnRepeat = false;
    fNotDoneAbortTime_us = 0;

    fBroadcastEventMaster = false;

    fDoEventLog = false;

    fDumpSuspiciousEvents = false;

    fEventBackLogIndex = 0;
    fReadoutListOutputCnt = 0;
    fReadoutListOutputReduction = 0;
    fReadoutListOutputReductionFactor = 1;
    }

AliHLTHLTOutWriterSubscriber::~AliHLTHLTOutWriterSubscriber()
    {
    }


bool AliHLTHLTOutWriterSubscriber::ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, 
						 const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* etsp,
						 AliUInt8_t*, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>&, 
						 AliHLTEventDoneData*& )
    {
    bool isSuspiciousEvent = false;
    size = 0;
    if ( fDisable )
	{
	LOG( AliHLTLog::kDebug, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "Output disabled" )
	    << "Output is explicitly disabled!" << ENDLOG;
	return true;
	}
    if ( !fHLTOutHandler )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "No HLTOut Handler" )
	    << "No HLTOut handler object configured." << ENDLOG;
	return false;
	}


    if ( eventID.fType != kAliEventTypeStartOfRun && eventID.fType != kAliEventTypeData && eventID.fType != kAliEventTypeEndOfRun && eventID.fType != kAliEventTypeSync )
	{
	LOG( AliHLTLog::kInformational, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "Non-(SOR/Data/EOR/SYNC) Event" )
	    << "Non-(SOR/Data/EOR/SYNC) Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") will not be written to HLTOut." << ENDLOG;
	size = 0;
	return true;
	}

    if ( fDoEventLog )
	{
	unsigned long ndx;
	if ( fEventLog.FindElement( eventID, ndx ) )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "Event already processed" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") has already been received before. Event will not be processed again."
		<< ENDLOG;
	    return true;
	    }
	}
    else
	{
	;
	for ( unsigned long backLogCnt=0;  backLogCnt<EVENT_BACKLOG_SIZE; ++backLogCnt )
	    {
	    if ( fEventBackLog[backLogCnt]==eventID )
		{
		LOG( AliHLTLog::kWarning, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "Event already processed" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ") has already been received before. Event will not be processed again."
		    << ENDLOG;
		return true;
		}
	    }
	}


    bool broadcastEventMaster = fBroadcastEventMaster;

    int ret;

    bool tmpFound=false;
    unsigned long n, hltOutBlockNdx = ~(unsigned long)0;
    bool broadcastEvent = (eventID.fType==kAliEventTypeStartOfRun || eventID.fType==kAliEventTypeEndOfRun || eventID.fType==kAliEventTypeSync );
    for ( n = 0; n < blockCnt; n++ )
	{
	if ( blocks[n].fDataType.fID == HLTOUTPUT_DATAID )
	    {
	    if ( blocks[n].fShmKey.fShmType==kInvalidShmType )
		{
		LOG( AliHLTLog::kWarning, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "Invalid HLT Output data blocks shm" )
		    << "Invalid shared memory type (" << AliHLTLog::kDec << blocks[n].fShmKey.fShmType
		    << ") for found HLTOutput block for event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ")." << ENDLOG;
		continue;
		}
	    if ( hltOutBlockNdx==~(unsigned long)0  ||
		 ( !tmpFound && (blocks[n].fShmKey.fShmType==kBigPhysShmType || blocks[n].fShmKey.fShmType==kPhysMemShmType) && 
		   blocks[hltOutBlockNdx].fShmKey.fShmType!=kBigPhysShmType && blocks[hltOutBlockNdx].fShmKey.fShmType!=kPhysMemShmType ) )
		hltOutBlockNdx = n; // Precaution, in case no fully  matching block is found
	    if ( broadcastEvent )
		{
		AliUInt8_t* data = (AliUInt8_t*)blocks[n].fData;
		AliUInt64_t cdhStatusError = AliHLTGetDDLHeaderStatusError( data );
		bool decisionOrDataIncluded =  ( cdhStatusError & (0x40|0x80) );
		if ( broadcastEventMaster!=decisionOrDataIncluded )
		    continue;
		}
	    if ( tmpFound )
		{
		if ( !broadcastEvent || broadcastEventMaster )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "Multiple matching HLT Output data blocks" )
			<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ") contains more than one HLT output data block"
			<< (broadcastEvent ? (broadcastEventMaster ? " with HLT decision or data" : " without HLT decision or data") : "") 
			<< ". Only one will be sent." << ENDLOG;
		    }
		isSuspiciousEvent = true;
		//return true;
		if ( (blocks[n].fShmKey.fShmType==kBigPhysShmType || blocks[n].fShmKey.fShmType==kPhysMemShmType) && 
		     blocks[hltOutBlockNdx].fShmKey.fShmType!=kBigPhysShmType && blocks[hltOutBlockNdx].fShmKey.fShmType!=kPhysMemShmType )
		    hltOutBlockNdx = n;
		}
	    else
		{
		tmpFound = true;
		hltOutBlockNdx = n;
		}
	    }
	}
    if ( broadcastEvent && !tmpFound && hltOutBlockNdx!=~(unsigned long)0 )
	{
	LOG( AliHLTLog::kWarning, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "Empty event" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") contains no HLT output data blocks"
	    << (broadcastEventMaster ? " with HLT decision or data" : " without HLT decision or data") 
	    << " - using first found output block (block " << AliHLTLog::kDec << hltOutBlockNdx << ")..."
	    << ENDLOG;
	tmpFound = true;
	isSuspiciousEvent = true;
	}
    if ( !tmpFound )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "Empty event" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") contains no HLT output data blocks at all"
	    << "." << ENDLOG;
	return true;
	}


    AliHLTSubEventDescriptor::BlockData outputBlock = blocks[hltOutBlockNdx];
    
    if ( (outputBlock.fShmKey.fShmType!=kBigPhysShmType && outputBlock.fShmKey.fShmType!=kPhysMemShmType) && 
	 (fShmKey.fShmType==kBigPhysShmType || fShmKey.fShmType==kPhysMemShmType) 
       && fShmSize>=outputBlock.fSize )
	{
	LOG( AliHLTLog::kDebug, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "Copying event" )
	    << "Copying Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") into private shared memory area."
	    << "." << ENDLOG;
	memcpy( fShmPtr, outputBlock.fData, outputBlock.fSize );
	outputBlock.fShmKey = fShmKey;
	outputBlock.fData = fShmPtr;
	outputBlock.fOffset = 0;
	}


    if ( outputBlock.fShmKey.fShmType!=kBigPhysShmType && outputBlock.fShmKey.fShmType!=kPhysMemShmType )
	{
	const char* outputShmType;
	switch ( fShmKey.fShmType )
	    {
	    case kBigPhysShmType:
		outputShmType = "bigphys";
		break;
	    case kPhysMemShmType:
		outputShmType = "physmem";
		break;
	    case kSysVShmType:
		outputShmType = "sysv";
		break;
	    case kSysVShmPrivateType:
		outputShmType = "sysv-private";
		break;
	    default:
		outputShmType = "invalid";
		break;
	    }
	const char* outputBlockShmType;
	switch ( outputBlock.fShmKey.fShmType )
	    {
	    case kBigPhysShmType:
		outputBlockShmType = "bigphys";
		break;
	    case kPhysMemShmType:
		outputBlockShmType = "physmem";
		break;
	    case kSysVShmType:
		outputBlockShmType = "sysv";
		break;
	    case kSysVShmPrivateType:
		outputBlockShmType = "sysv-private";
		break;
	    default:
		outputBlockShmType = "invalid";
		break;
	    }
	LOG( AliHLTLog::kError, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "Cannot sumbit event" )
	    << "Cannot submit event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") to HLT output. Event has no output block located in suitable shared memory area (bigphys or physmem). "
	    << "Output block was not located in proper shared memory by original publisher and could not be copied into proper private shared memory area."
	    << " - Private shared memory type: " << outputShmType << " - size: " << AliHLTLog::kDec << fShmSize << " bytes"
	    << " - HLT output data block shared memory type: " << outputBlockShmType << " - block size: " << outputBlock.fSize
	    << "." << ENDLOG;
	return true;
	}

    AliUInt8_t* data = (AliUInt8_t*)outputBlock.fData;
    AliUInt64_t cdhStatusError = AliHLTGetDDLHeaderStatusError( data );
    if ( cdhStatusError & 0x40 )
	{
	AliVolatileUInt32_t* readoutListData = (AliVolatileUInt32_t*)(data+AliHLTGetDDLHeaderSize(data)+sizeof(AliUInt32_t)*4);
	AliHLTReadoutList readoutList( gReadoutListVersion, readoutListData );
	bool tmp=false;
	readoutList.GetDetectorDDLs( "HLT", tmp ); // Check whether any bit is set in data coming from HLTOutFormatter ; No bit set signals full reject
	if ( tmp )
	    {
	    readoutList.SetDetectorDDLs( "HLT", false ); // Reset all other bits and set only our own bit
	    if ( fDDLID != ~0UL && !readoutList.SetDDL( fDDLID ) )
		{
		LOG( AliHLTLog::kError, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "Error setting DDL bit" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		    << "): Error setting HLT output DDL bit 0x" << AliHLTLog::kHex
		    << fDDLID << " (readout list data: 0x" << (unsigned long)readoutListData << "." << ENDLOG;
		}
	    }
	}
    if ( eventID.fType==kAliEventTypeData )
	{
	if ( fReadoutListOutputReduction && ((fReadoutListOutputCnt<fReadoutListOutputReduction) || ((fReadoutListOutputCnt % fReadoutListOutputReduction*fReadoutListOutputReductionFactor)==0)) )
	    {
	    if ( cdhStatusError & 0x40 )
		{
		AliVolatileUInt32_t* readoutListData = (AliVolatileUInt32_t*)(data+AliHLTGetDDLHeaderSize(data)+sizeof(AliUInt32_t)*4);
		AliHLTReadoutList readoutList( gReadoutListVersion, readoutListData );
		MLUCString msg;
		for ( unsigned nn=0; nn<readoutList.GetReadoutListSize(); nn += sizeof(hltreadout_uint32_t) )
		    {
		    char tmp[256];
		    snprintf( tmp, 256, "%02u : 0x%08X", nn, readoutListData[nn] );
		    if ( nn )
			msg += " - ";
		    msg += tmp;
		    }
		LOG( AliHLTLog::kImportant, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "Readout list" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << ") readout list: " << msg.c_str() << "." << ENDLOG;
		}
	    else
		{
		LOG( AliHLTLog::kImportant, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "No readout list" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << ") has no readout list." << ENDLOG;
		}
	    if ( fReadoutListOutputCnt>=fReadoutListOutputReduction )
		fReadoutListOutputReductionFactor *= 2;
	    }
	++fReadoutListOutputCnt;
	      
	// fReadoutListOutputCnt = 0;
	// fReadoutListOutputReduction = 1;
	}

    // Determine first DDL data block.
    const char* outputBlockShmType;
    switch ( outputBlock.fShmKey.fShmType )
	{
	case kBigPhysShmType:
	    outputBlockShmType = "bigphys";
	    break;
	case kPhysMemShmType:
	    outputBlockShmType = "physmem";
	    break;
	case kSysVShmType:
	    outputBlockShmType = "sysv";
	    break;
	case kSysVShmPrivateType:
	    outputBlockShmType = "sysv-private";
	    break;
	default:
	    outputBlockShmType = "invalid";
	    break;
	}
    LOG( AliHLTLog::kDebug, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "Submitting event to HLTOut" )
	<< "Submitting event event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << ") . Using output block " << hltOutBlockNdx << " - output block shm type: " 
	<< outputBlockShmType << " - output block size: " << outputBlock.fSize << ENDLOG;
    ret = fHLTOutHandler->SubmitEvent( eventID, outputBlock, sedd, etsp );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "Error submitting event" )
	    << "Error submitting first HLT output data block (block "
	    << AliHLTLog::kDec << n << ") from event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") to HLTOut handler: " << strerror(ret) << " ("
	    << AliHLTLog::kDec << ret << ")." << ENDLOG;
	return false;
	}
    if ( fDoEventLog )
	fEventLog.Add( eventID, eventID );
    else
	{
	fEventBackLog[fEventBackLogIndex] = eventID;
	fEventBackLogIndex = (fEventBackLogIndex+1) & EVENT_BACKLOG_MASK;
	}
    if ( isSuspiciousEvent && fDumpSuspiciousEvents )
	{
	MLUCString prefix = GetName();
	prefix += "_SuspiciousEvent";
	AliHLTEventWriter::WriteEvent( eventID, blockCnt, blocks, sedd, etsp, fRunNumber, prefix.c_str() );
	}
    struct timeval start, lastWarn;
    gettimeofday( &start, NULL );
    lastWarn.tv_sec = lastWarn.tv_usec = 0;
    while ( !fQuitDonePoll )
	{
	ret =fHLTOutHandler->PollForDoneEvent();
	if ( ret && ret!=EAGAIN )
	    {
	    LOG( AliHLTLog::kError, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "Error polling for done event" )
		<< "Error polling for done event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") at HLTOut handler: " << strerror(ret) << " ("
		<< AliHLTLog::kDec << ret << ")." << ENDLOG;
	    // We return true, as we have to presume the event has already been sent and we don't want a retry...
	    // XXX ???
	    return true;
	    }
	if ( !ret )
	    {
	    if ( eventID.fType==kAliEventTypeStartOfRun )
		{
		LOG( AliHLTLog::kImportant, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "Start-Of-Data/-Run event set" )
		    << "Start-Of-Data/-Run event 0x" << AliHLTLog::kHex << eventID << " ("
		    << AliHLTLog::kDec << eventID << ") successfully sent." << ENDLOG;
		}
	    if ( eventID.fType==kAliEventTypeEndOfRun )
		{
		LOG( AliHLTLog::kImportant, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "End-Of-Data/-Run event set" )
		    << "End-Of-Data/-Run event 0x" << AliHLTLog::kHex << eventID << " ("
		    << AliHLTLog::kDec << eventID << ") successfully sent." << ENDLOG;
		}
	    if ( eventID.fType==kAliEventTypeSync )
		{
		LOG( AliHLTLog::kImportant, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "SYNC event set" )
		    << "SYNC event 0x" << AliHLTLog::kHex << eventID << " ("
		    << AliHLTLog::kDec << eventID << ") successfully sent." << ENDLOG;
		}
	    return true;
	    }
	if ( fNotDoneAbortTime_us || ( fNotDoneWarnTime_us && (fNotDoneWarnRepeat || (!lastWarn.tv_sec && !lastWarn.tv_usec)) ) )
	    {
	    struct timeval now;
	    gettimeofday( &now, NULL );
	    unsigned long long tdiff;
	    tdiff = (now.tv_sec-start.tv_sec)*1000000ULL+(now.tv_usec-start.tv_usec);
	    if ( fNotDoneAbortTime_us && tdiff>fNotDoneAbortTime_us )
		{
		LOG( AliHLTLog::kError, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "Timeout polling for done event" )
		    << "Timeout polling for done event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ") at HLTOut handler after " << MLUCLog::kDec << fNotDoneAbortTime_us << " microsecs.. Aborting poll for done event..." << ENDLOG;
		return true;
		}
	    if ( fNotDoneWarnRepeat && (lastWarn.tv_sec || lastWarn.tv_usec) )
		tdiff = (now.tv_sec-lastWarn.tv_sec)*1000000ULL+(now.tv_usec-lastWarn.tv_usec);
	    if ( fNotDoneWarnTime_us && tdiff>fNotDoneWarnTime_us && (fNotDoneWarnRepeat || (!lastWarn.tv_sec && !lastWarn.tv_usec)) )
		{
		LOG( AliHLTLog::kWarning, "AliHLTHLTOutWriterSubscriber::ProcessEvent", "Polling for done event" )
		    << "No done for event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ") at HLTOut handler after " << MLUCLog::kDec << fNotDoneWarnTime_us << " microsecs.. Continuing waiting..." << ENDLOG;
		lastWarn = now;
		}
	    }
	if ( fDoSleep )
	    usleep( fSleepTime );
	}
    
    return true;
    }







/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
