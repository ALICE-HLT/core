/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTHLTOut1Handler.hpp"
#include "AliHLTLog.hpp"
#include <psi.h>
#include <psi_error.h>
#include <errno.h>

const AliUInt32_t AliHLTHLTOut1Handler::kRESET=20; //  0x1C
const AliUInt32_t AliHLTHLTOut1Handler::kCLEARFIFO=24; //  0x1C
const AliUInt32_t AliHLTHLTOut1Handler::kREPBUFPTR=8;
const AliUInt32_t AliHLTHLTOut1Handler::kREPBUFSIZ=12;
const AliUInt32_t AliHLTHLTOut1Handler::kEVTPTR=0;
const AliUInt32_t AliHLTHLTOut1Handler::kEVTSIZ=4;


AliHLTHLTOut1Handler::AliHLTHLTOut1Handler( int eventSlotsExp2 )
    {
    fBar0ID = "";
    fBar0Region = 0;

    fReportBufferSize = (1<<eventSlotsExp2)*sizeof(AliUInt32_t);
    fReportBufferID = "";
    fReportBufferRegion = 0;

    fReportBufferPhysAddress = 0;
    fReportBufferBusAddress = 0;

    fReportBuffer = NULL;
    fReportBufferEnd = NULL;
    fReportBufferReadPtr = NULL;

    fEvent = AliEventID_t();
    pthread_mutex_init( &fEventMutex, NULL );
    }

AliHLTHLTOut1Handler::~AliHLTHLTOut1Handler()
    {
    //Close();
    pthread_mutex_destroy( &fEventMutex );
    }


int AliHLTHLTOut1Handler::Open( bool vendorDeviceIndexOrBusSlotFunction, AliUInt16_t pciVendorIDOrBus, AliUInt16_t pciDeviceIDOrSlot, AliUInt16_t pciDeviceIndexOrFunction, AliUInt16_t )
    {
    char tmp[ 512+1 ];
    PSI_Status    status;

    if ( vendorDeviceIndexOrBusSlotFunction )
	{
	fBar0ID = "/dev/psi/vendor/"; // 0x%04hX/device/0x%04hX/0/base0
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fBar0ID += tmp;
	fBar0ID += "/device/";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fBar0ID += tmp;
	fBar0ID += "/";
	snprintf( tmp, 512, "%04X", pciDeviceIndexOrFunction );
	fBar0ID += tmp;
	fBar0ID += "/base0";

	fReportBufferID = "/dev/psi/bigphys/HLT-HLTOut-Report-VDI-";
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIndexOrFunction );
	fReportBufferID += tmp;
	}
    else
	{
	fBar0ID = "/dev/psi/bus/"; // 0x%04hX/device/0x%04hX/0/base0
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fBar0ID += tmp;
	fBar0ID += "/slot/";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fBar0ID += tmp;
	fBar0ID += "/function/";
	snprintf( tmp, 512, "%04X", pciDeviceIndexOrFunction );
	fBar0ID += tmp;
	fBar0ID += "/base0";

	fReportBufferID = "/dev/psi/bigphys/HLT-HLTOut-Report-BSF-";
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIndexOrFunction );
	fReportBufferID += tmp;
	}
  
    status = PSI_openRegion( &fBar0Region, fBar0ID.c_str() );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut1Handler::Open", "Unable to open HLTOut device bar 0 region" )
	    << "Unable to open HLTOut device bar 0 region '" << fBar0ID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}

    status = PSI_openRegion( &fReportBufferRegion, fReportBufferID.c_str() );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut1Handler::Open", "Unable to open report buffer shm region" )
	    << "Unable to open report buffer shm region '" << fReportBufferID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBar0Region );
	return EIO;
	}

    unsigned long cur_size = fReportBufferSize;

    status = PSI_sizeRegion( fReportBufferRegion, &cur_size );
    if ( status != PSI_OK && status != PSI_SIZE_SET )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut1Handler::Open", "Unable to set size for report buffer shm region" )
	    << "Unable to set size for report buffer shm region '" << fReportBufferID.c_str()
	    << "' to size " << AliHLTLog::kDec << fReportBufferSize << " (0x" << AliHLTLog::kHex
	    << fReportBufferSize << "): " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBar0Region );
	PSI_closeRegion( fReportBufferRegion );
	return EIO;
	}

    if ( cur_size != fReportBufferSize )
	{
	LOG( AliHLTLog::kInformational, "AliHLTHLTOut1Handler::Open", "Report buffer size changed" )
	    << "Report buffer size is now " << AliHLTLog::kDec << cur_size << " (0x" << AliHLTLog::kHex
	    << cur_size << ") instead of the specified " << AliHLTLog::kDec << fReportBufferSize << " (0x" << AliHLTLog::kHex
	    << fReportBufferSize << ")." << ENDLOG;
	fReportBufferSize = cur_size;
	}
    else
	{
	LOG( AliHLTLog::kDebug, "AliHLTHLTOut1Handler::Open", "Report buffer size" )
	    << "Report buffer size is " << AliHLTLog::kDec << fReportBufferSize << " (0x" << AliHLTLog::kHex
	    << fReportBufferSize << ")." << ENDLOG;
	}
    //status = PSI_mapRegion( fReportBufferRegion, reinterpret_cast<volatile void**>(&fReportBuffer) );
status = PSI_mapRegion( fReportBufferRegion, (void**)(&fReportBuffer) );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut1Handler::Open", "Unable to map report buffer shm region" )
	    << "Unable to map report buffer shm region '" << fReportBufferID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBar0Region );
	PSI_closeRegion( fReportBufferRegion );
	return EIO;
	}
    fReportBufferEnd = fReportBuffer + fReportBufferSize;
    fReportBufferReadPtr = fReportBuffer;
    fReportBufferOldReadPtr = NULL;
    //status = PSI_getPhysAddress( fReportBuffer, reinterpret_cast<void**>(&fReportBufferPhysAddress), reinterpret_cast<void**>(&fReportBufferBusAddress) );
    status = PSI_getPhysAddress( (void*)fReportBuffer, reinterpret_cast<void**>(&fReportBufferPhysAddress), reinterpret_cast<void**>(&fReportBufferBusAddress) );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut1Handler::Open", "Unable to obtain report buffer physical/bus address" )
	    << "Unable to obtain physical and bus address for report buffer: " 
	    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBar0Region );
	//PSI_unmapRegion( fReportBufferRegion, fReportBuffer );
	PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
	PSI_closeRegion( fReportBufferRegion );
	return EIO;
	}
    fReportBufferEndBusAddress = fReportBufferBusAddress + fReportBufferSize;
    fReportBufferReadPtrBusAddress = fReportBufferBusAddress;
    LOG( AliHLTLog::kDebug, "AliHLTHLTOut1Handler::Open", "Report buffer pointers" )
	<< "ReportBuffer: 0x" << AliHLTLog::kHex << (unsigned long)fReportBuffer << " - ReportBuffer Phys: 0x"
	<< (unsigned long)fReportBufferPhysAddress << " - ReportBuffer Bus: 0x" << (unsigned long)fReportBufferBusAddress
	<< "." << ENDLOG;

    return 0;
    }

int AliHLTHLTOut1Handler::Close()
    {
    PSI_closeRegion( fBar0Region );
    //PSI_unmapRegion( fReportBufferRegion, fReportBuffer );
    PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
    PSI_closeRegion( fReportBufferRegion );
    return 0;
    }


int AliHLTHLTOut1Handler::InitializeHLTOut( bool )
    {
    PSI_Status    status;
    unsigned long wr;

    /*
     * HLTOut reset
     */
    wr = 0xaffed00f;
    status = PSI_write( fBar0Region, (unsigned long)kRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}

    /*
     * HLTOut clear fifo
     */
    wr = 0xaffed00f;
    status = PSI_write( fBar0Region, (unsigned long)kCLEARFIFO, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}

    /*
     * Clear report buffer
     */
    memset( (void*)fReportBuffer, 0xFF, fReportBufferSize );

    status = PSI_write( fBar0Region, (unsigned long)kREPBUFPTR, _dword_, 4, (void*)&fReportBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    status = PSI_write( fBar0Region, (unsigned long)kREPBUFSIZ, _dword_, 4, (void*)&fReportBufferSize );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut1Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
	
    fEvent = AliEventID_t();

    return 0;
    }

int AliHLTHLTOut1Handler::DeinitializeHLTOut()
    {
    return 0;
    }


int AliHLTHLTOut1Handler::InitializeFromHLTOut( bool first )
    {
    if ( !first )
	{
	while ( *(unsigned long*)fReportBufferReadPtr == 0xFFFFFFFF && fReportBufferReadPtr<fReportBufferEnd )
	    fReportBufferReadPtr += 4;
	if ( fReportBufferReadPtr>=fReportBufferEnd )
	    fReportBufferReadPtr = fReportBuffer;
	fReportBufferOldReadPtr = NULL;
	}
    return 0;
    }


int AliHLTHLTOut1Handler::SetOptions( const vector<MLUCString>&, char*&, unsigned& )
    {
    return 0;
    }

void AliHLTHLTOut1Handler::GetAllowedOptions( vector<MLUCString>& options )
    {
    options.clear();
    }


int AliHLTHLTOut1Handler::ActivateHLTOut()
    {
    return 0;
    }

int AliHLTHLTOut1Handler::DeactivateHLTOut()
    {
    return 0;
    }


int AliHLTHLTOut1Handler::SubmitEvent( AliEventID_t eventID, const AliHLTSubEventDescriptor::BlockData& block, 
				     const AliHLTSubEventDataDescriptor*, const AliHLTEventTriggerStruct* )
    {
    unsigned long shmBusAddr;
    AliUInt8_t* ptr = block.fData - block.fOffset; // Determine shm start address
    shmBusAddr = GetShmBusAddr( block.fShmKey, ptr );
    
    *(AliVolatileUInt32_t*)fReportBufferReadPtr = 0xFFFFFFFF;
 
    unsigned long evtBusAddr = shmBusAddr+block.fOffset;

    LOG( AliHLTLog::kInformational, "AliHLTHLTOut1Handler::SubmitEvent", "Submitting event" )
	<< "Submitting event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << "): Bus address: 0x" << AliHLTLog::kHex << (unsigned long)evtBusAddr
	<< " - size: " << AliHLTLog::kDec << block.fSize << "." << ENDLOG;

    
    PSI_Status    status;
    unsigned long wr;

    /*
     * event write, first bus address, then block size
     */
    wr = evtBusAddr;
    status = PSI_write( fBar0Region, (unsigned long)kEVTPTR, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut1Handler::SubmitEvent", "Write Error" )
	    << "PCI write error writing event pointer: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}

    wr = block.fSize;
    status = PSI_write( fBar0Region, (unsigned long)kEVTSIZ, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut1Handler::SubmitEvent", "Write Error" )
	    << "PCI write error writing event size: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    pthread_mutex_lock( &fEventMutex );
    fEvent = eventID;
    pthread_mutex_unlock( &fEventMutex );
    return 0;
    }

int AliHLTHLTOut1Handler::PollForDoneEvent()
    {
    unsigned long busAddr;
    if ( fReportBufferOldReadPtr != fReportBufferReadPtr )
	{
	LOG( AliHLTLog::kDebug, "AliHLTHLTOut1Handler::PollForDoneEvent", "Polling for event" )
	    << "Polling for event at location 0x" << AliHLTLog::kHex << (unsigned long)fReportBufferReadPtr
	    << " (offset 0x" << (unsigned long)(fReportBufferReadPtr-fReportBuffer) << " ("
	    << AliHLTLog::kDec << (unsigned long)(fReportBufferReadPtr-fReportBuffer) 
	    << ")) in report buffer." << ENDLOG;
	fReportBufferOldReadPtr = fReportBufferReadPtr;
	}
    if ( *(AliVolatileUInt32_t*)fReportBufferReadPtr!=0xFFFFFFFF )
	{
	LOG( AliHLTLog::kDebug, "AliHLTHLTOut1Handler::PollForDoneEvent", "Found done address" )
	    << "(*(unsigned long*)fReportBufferReadPtr): 0x" << AliHLTLog::kHex
	    << (*(AliVolatileUInt32_t*)fReportBufferReadPtr) << ENDLOG;
	busAddr = (*(AliVolatileUInt32_t*)fReportBufferReadPtr);

#if 1
	*(AliVolatileUInt32_t*)fReportBufferReadPtr = 0xFFFFFFFF;
#endif

	fReportBufferReadPtr += sizeof(unsigned long);
	if ( fReportBufferReadPtr >= fReportBufferEnd )
	    fReportBufferReadPtr = fReportBuffer;
	LOG( AliHLTLog::kDebug, "AliHLTHLTOut1Handler::PollForDoneEvent", "New buffer ptrs" )
	    << "New report buffer ptr: 0x" << AliHLTLog::kHex << (unsigned long)fReportBufferReadPtr
	    << " (offset " << AliHLTLog::kDec << (unsigned long)(fReportBufferReadPtr-fReportBuffer)
	    << " (0x" << AliHLTLog::kHex << (unsigned long)(fReportBufferReadPtr-fReportBuffer)
	    << "))." << ENDLOG;

	pthread_mutex_lock( &fEventMutex );
	if ( fEvent != AliEventID_t() )
	    {
	    fEvent = AliEventID_t();
	    pthread_mutex_unlock( &fEventMutex );
	    return 0;
	    }
	else
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTHLTOut1Handler::PollForDoneEvent", "New buffer ptrs" )
		<< "Could not find done event with bus address 0x" << AliHLTLog::kHex
		<< busAddr << " (" << AliHLTLog::kDec << busAddr << ")." << ENDLOG;
	    pthread_mutex_unlock( &fEventMutex );
	    return EIO;
	    }

	}
    return EAGAIN;
    }


unsigned long AliHLTHLTOut1Handler::GetShmBusAddr( AliHLTShmID shmKey, AliUInt8_t* virtPtr )
    {
    vector<ShmData>::iterator iter, end;
    iter = fShmData.begin();
    end = fShmData.end();

    while ( iter != end )
	{
	if ( iter->fShmKey.fShmType==shmKey.fShmType && iter->fShmKey.fKey.fID==shmKey.fKey.fID )
	    return iter->fBusAddress;
	iter++;
	}

    void *physAddress, *busAddress;
    PSI_Status    status;

    status = PSI_getPhysAddress( virtPtr, &physAddress, &busAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut1Handler::GetShmBusAddr", "Unable to obtain shared memory buffer physical/bus address" )
	    << "Unable to obtain physical and bus address for shared memory buffer: " 
	    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}

    ShmData sd;
    sd.fShmKey.fShmType = shmKey.fShmType;
    sd.fShmKey.fKey.fID = shmKey.fKey.fID;
    sd.fBusAddress = (unsigned long)busAddress;
    fShmData.insert( fShmData.end(), sd );
    return (unsigned long)busAddress;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
