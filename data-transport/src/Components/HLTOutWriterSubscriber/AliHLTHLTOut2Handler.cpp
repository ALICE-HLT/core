/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTHLTOut2Handler.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTSharedMemory.hpp"
#include <psi.h>
#include <psi_error.h>
#include <errno.h>

const AliUInt32_t AliHLTHLTOut2Handler::kEVTPTR=0x00;
const AliUInt32_t AliHLTHLTOut2Handler::kEVTSIZ=0x04;
const AliUInt32_t AliHLTHLTOut2Handler::kREPBUFPTR=0x08;
const AliUInt32_t AliHLTHLTOut2Handler::kRESET=0x88;
const AliUInt32_t AliHLTHLTOut2Handler::kENABLESIU=0xC0;


AliHLTHLTOut2Handler::AliHLTHLTOut2Handler( int /*eventSlotsExp2*/ )
    {
    fBarID = "";
    fBarRegion = 0;

    fReportBufferSize = 4096;
    fReportBufferID = "";
    fReportBufferRegion = 0;

    fReportBufferPhysAddress = 0;
    fReportBufferBusAddress = 0;

    fReportBuffer = NULL;
    fReportBufferEnd = NULL;
    fReportBufferReadPtr = NULL;

    fEvent = AliEventID_t();
    pthread_mutex_init( &fEventMutex, NULL );

    fSharedMemory = NULL;
    fLastDMAMapShmKey.fShmType = kInvalidShmType;
    fLastDMAMapShmKey.fKey.fID = ~0ULL;
    fLastDMAMapRegion = 0;
    fLastDMAMapBusAddress = 0;
    }

AliHLTHLTOut2Handler::~AliHLTHLTOut2Handler()
    {
    //Close();
    pthread_mutex_destroy( &fEventMutex );
    }


int AliHLTHLTOut2Handler::Open( bool vendorDeviceIndexOrBusSlotFunction, AliUInt16_t pciVendorIDOrBus, AliUInt16_t pciDeviceIDOrSlot, AliUInt16_t pciDeviceIndexOrFunction, AliUInt16_t bar )
    {
    char tmp[ 512+1 ];
    PSI_Status    status;

    if ( vendorDeviceIndexOrBusSlotFunction )
	{
	fBarID = "/dev/psi/vendor/"; // 0x%04hX/device/0x%04hX/0/base0
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fBarID += tmp;
	fBarID += "/device/";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fBarID += tmp;
	fBarID += "/";
	snprintf( tmp, 512, "%04X", pciDeviceIndexOrFunction );
	fBarID += tmp;

	fReportBufferID = "/dev/psi/bigphys/HLT-HLTOut-Report-VDI-";
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIndexOrFunction );
	fReportBufferID += tmp;
	}
    else
	{
	fBarID = "/dev/psi/bus/"; // 0x%04hX/device/0x%04hX/0/base0
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fBarID += tmp;
	fBarID += "/slot/";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fBarID += tmp;
	fBarID += "/function/";
	snprintf( tmp, 512, "%04X", pciDeviceIndexOrFunction );
	fBarID += tmp;

	fReportBufferID = "/dev/psi/bigphys/HLT-HLTOut-Report-BSF-";
	snprintf( tmp, 512, "0x%04X", pciVendorIDOrBus );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIDOrSlot );
	fReportBufferID += tmp;
	fReportBufferID += "-";
	snprintf( tmp, 512, "0x%04X", pciDeviceIndexOrFunction );
	fReportBufferID += tmp;
	}

    fBarID += "/base";
    snprintf( tmp, 512, "%d", bar );
    fBarID += tmp;

    fReportBufferID += "-";
    snprintf( tmp, 512, "0x%04X", bar );
    fReportBufferID += tmp;

    status = PSI_openRegion( &fBarRegion, fBarID.c_str() );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::Open", "Unable to open HLTOut device bar region" )
	    << "Unable to open HLTOut device bar " << AliHLTLog::kDec << bar << " region '" << fBarID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}

    status = PSI_openRegion( &fReportBufferRegion, fReportBufferID.c_str() );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::Open", "Unable to open report buffer shm region" )
	    << "Unable to open report buffer shm region '" << fReportBufferID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBarRegion );
	return EIO;
	}

    unsigned long cur_size = fReportBufferSize;

    status = PSI_sizeRegion( fReportBufferRegion, &cur_size );
    if ( status != PSI_OK && status != PSI_SIZE_SET )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::Open", "Unable to set size for report buffer shm region" )
	    << "Unable to set size for report buffer shm region '" << fReportBufferID.c_str()
	    << "' to size " << AliHLTLog::kDec << fReportBufferSize << " (0x" << AliHLTLog::kHex
	    << fReportBufferSize << "): " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBarRegion );
	PSI_closeRegion( fReportBufferRegion );
	return EIO;
	}

    if ( cur_size != fReportBufferSize )
	{
	LOG( AliHLTLog::kInformational, "AliHLTHLTOut2Handler::Open", "Report buffer size changed" )
	    << "Report buffer size is now " << AliHLTLog::kDec << cur_size << " (0x" << AliHLTLog::kHex
	    << cur_size << ") instead of the specified " << AliHLTLog::kDec << fReportBufferSize << " (0x" << AliHLTLog::kHex
	    << fReportBufferSize << ")." << ENDLOG;
	fReportBufferSize = cur_size;
	}
    else
	{
	LOG( AliHLTLog::kDebug, "AliHLTHLTOut2Handler::Open", "Report buffer size" )
	    << "Report buffer size is " << AliHLTLog::kDec << fReportBufferSize << " (0x" << AliHLTLog::kHex
	    << fReportBufferSize << ")." << ENDLOG;
	}
    //status = PSI_mapRegion( fReportBufferRegion, reinterpret_cast<volatile void**>(&fReportBuffer) );
status = PSI_mapRegion( fReportBufferRegion, (void**)(&fReportBuffer) );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::Open", "Unable to map report buffer shm region" )
	    << "Unable to map report buffer shm region '" << fReportBufferID.c_str()
	    << "': " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBarRegion );
	PSI_closeRegion( fReportBufferRegion );
	return EIO;
	}
    fReportBufferEnd = fReportBuffer + fReportBufferSize;
    fReportBufferReadPtr = fReportBuffer;
    fReportBufferOldReadPtr = NULL;
    //status = PSI_getPhysAddress( fReportBuffer, reinterpret_cast<void**>(&fReportBufferPhysAddress), reinterpret_cast<void**>(&fReportBufferBusAddress) );



    status = PSI_getPhysAddress( (void*)fReportBuffer, reinterpret_cast<void**>(&fReportBufferPhysAddress), reinterpret_cast<void**>(&fReportBufferBusAddress) );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::Open", "Unable to obtain report buffer physical/bus address" )
	    << "Unable to obtain physical and bus address for report buffer: " 
	    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	PSI_closeRegion( fBarRegion );
	//PSI_unmapRegion( fReportBufferRegion, fReportBuffer );
	PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
	PSI_closeRegion( fReportBufferRegion );
	return EIO;
	}
    if ( fSharedMemory )
        {
	unsigned long busAddress=0;
	status = PSI_mapDMA( fReportBufferRegion, fBarRegion, _fromDevice_, &busAddress );
	if ( status!=PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::Open", "Unable to obtain report buffer bus address" )
		<< "Unable to obtain bus address for report buffer: " 
		<< PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		<< ") (cannot DMA map region)." << ENDLOG;
	    PSI_closeRegion( fBarRegion );
	    PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
	    PSI_closeRegion( fReportBufferRegion );
	    return EIO;
	    }
	fReportBufferBusAddress = busAddress;
        }
    fReportBufferEndBusAddress = fReportBufferBusAddress + fReportBufferSize;
    fReportBufferReadPtrBusAddress = fReportBufferBusAddress;
    LOG( AliHLTLog::kDebug, "AliHLTHLTOut2Handler::Open", "Report buffer pointers" )
	<< "ReportBuffer: 0x" << AliHLTLog::kHex << (unsigned long)fReportBuffer << " - ReportBuffer Phys: 0x"
	<< (unsigned long)fReportBufferPhysAddress << " - ReportBuffer Bus: 0x" << (unsigned long)fReportBufferBusAddress
	<< "." << ENDLOG;

    return 0;
    }

int AliHLTHLTOut2Handler::Close()
    {
    PSI_unmapDMA( fReportBufferRegion );
    PSI_closeRegion( fBarRegion );
    //PSI_unmapRegion( fReportBufferRegion, fReportBuffer );
    PSI_unmapRegion( fReportBufferRegion, (void*)fReportBuffer );
    PSI_closeRegion( fReportBufferRegion );
    return 0;
    }


int AliHLTHLTOut2Handler::InitializeHLTOut( bool )
    {
    PSI_Status    status;
    unsigned long wr;

    /*
     * HLTOut reset
     */
    wr = 0x00000002;
    status = PSI_write( fBarRegion, (unsigned long)kRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}

    /*
     * HLTOut reset a second time
     */
    wr = 0x00000002;
    status = PSI_write( fBarRegion, (unsigned long)kRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}

    /*
     * Clear report buffer
     */
    memset( (void*)fReportBuffer, 0xFF, fReportBufferSize );

    status = PSI_write( fBarRegion, (unsigned long)kREPBUFPTR, _dword_, 4, (void*)&fReportBufferBusAddress );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
	
    fEvent = AliEventID_t();

    /*
     * HLTOut enable SIU
     */
#define ENABLE_CLOCKSHIFT
#ifdef  ENABLE_CLOCKSHIFT
     wr = 0x00000003; // 3 for clock phase shift, 1 is default without shift.
#else
#warning Deactivated clock phase shift
    wr = 0x00000001; // 3 for clock phase shift, 1 is default without shift.
#endif
    LOG( AliHLTLog::kDebug, "AliHLTHLTOut2Handler::Initialize", "SIU ENABLE" )
	<< "Writing " << AliHLTLog::kDec << wr << " to HLT out ENABLE SIU" << ENDLOG;
    status = PSI_write( fBarRegion, (unsigned long)kENABLESIU, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::InitializeHLTOut", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}

    return 0;
    }

int AliHLTHLTOut2Handler::DeinitializeHLTOut()
    {
    PSI_Status    status;
    unsigned long wr;

    /*
     * HLTOut disable SIU
     */
    wr = 0x00000000;
    status = PSI_write( fBarRegion, (unsigned long)kENABLESIU, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::DeactivateHLTOut", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}

    /*
     * HLTOut reset
     */
    wr = 0x00000002;
    status = PSI_write( fBarRegion, (unsigned long)kRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}

    /*
     * HLTOut reset a second time
     */
    wr = 0x00000002;
    status = PSI_write( fBarRegion, (unsigned long)kRESET, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::Initialize", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    return 0;
    }


int AliHLTHLTOut2Handler::InitializeFromHLTOut( bool first )
    {
    PSI_Status    status;
    unsigned long wr;

    if ( !first )
	{
	}
    /*
     * HLTOut enable SIU
     */
#ifdef ENABLE_CLOCKSHIFT
    wr = 0x00000003;
#else
     wr = 0x00000001;
#endif
    status = PSI_write( fBarRegion, (unsigned long)kENABLESIU, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::InitializeHLTOut", "Write Error" )
	    << "PCI write error: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    return 0;
    }


int AliHLTHLTOut2Handler::SetOptions( const vector<MLUCString>&, char*&, unsigned& )
    {
    return 0;
    }

void AliHLTHLTOut2Handler::GetAllowedOptions( vector<MLUCString>& options )
    {
    options.clear();
    }


int AliHLTHLTOut2Handler::ActivateHLTOut()
    {
    return 0;
    }

int AliHLTHLTOut2Handler::DeactivateHLTOut()
    {
    return 0;
    }


int AliHLTHLTOut2Handler::SubmitEvent( AliEventID_t eventID, const AliHLTSubEventDescriptor::BlockData& block, 
				     const AliHLTSubEventDataDescriptor*, const AliHLTEventTriggerStruct* )
    {
    unsigned long shmBusAddr;
    AliUInt8_t* ptr = block.fData - block.fOffset; // Determine shm start address
    shmBusAddr = GetShmBusAddr( block.fShmKey, ptr );
    if ( !shmBusAddr )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::SubmitEvent", "Error getting bus address" )
	    << "Error getting bus address for event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") data. Aborting..." << ENDLOG;
	return EIO;
	}
    
    *(AliVolatileUInt32_t*)fReportBufferReadPtr = 0xFFFFFFFF;
 
    unsigned long evtBusAddr = shmBusAddr+block.fOffset;

    LOG( AliHLTLog::kInformational, "AliHLTHLTOut2Handler::SubmitEvent", "Submitting event" )
	<< "Submitting event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << "): Bus address: 0x" << AliHLTLog::kHex << (unsigned long)evtBusAddr
	<< " - size: " << AliHLTLog::kDec << block.fSize << "." << ENDLOG;

    
    PSI_Status    status;
    unsigned long wr;

    /*
     * event write, first bus address, then block size
     */
    wr = evtBusAddr;
    status = PSI_write( fBarRegion, (unsigned long)kEVTPTR, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::SubmitEvent", "Write Error" )
	    << "PCI write error writing event pointer: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}

    // Send number of 32 bit words in data
    wr = block.fSize >> 2;
    // add some padding bytes if necessary
    if ( block.fSize & 0x3 )
	++wr;
    status = PSI_write( fBarRegion, (unsigned long)kEVTSIZ, _dword_, 4, (void*)&wr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::SubmitEvent", "Write Error" )
	    << "PCI write error writing event size: " << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
	    << ")." << ENDLOG;
	return EIO;
	}
    pthread_mutex_lock( &fEventMutex );
    fEvent = eventID;
    pthread_mutex_unlock( &fEventMutex );
    return 0;
    }

int AliHLTHLTOut2Handler::PollForDoneEvent()
    {
    unsigned long busAddr;
    if ( *(AliVolatileUInt32_t*)fReportBufferReadPtr!=0xFFFFFFFF )
	{
	LOG( AliHLTLog::kDebug, "AliHLTHLTOut2Handler::PollForDoneEvent", "Found done address" )
	    << "(*(unsigned long*)fReportBufferReadPtr): 0x" << AliHLTLog::kHex
	    << (*(AliVolatileUInt32_t*)fReportBufferReadPtr) << ENDLOG;
	busAddr = (*(AliVolatileUInt32_t*)fReportBufferReadPtr);

#if 1
	*(AliVolatileUInt32_t*)fReportBufferReadPtr = 0xFFFFFFFF;
#endif

	pthread_mutex_lock( &fEventMutex );
	if ( fEvent != AliEventID_t() )
	    {
	    fEvent = AliEventID_t();
	    pthread_mutex_unlock( &fEventMutex );
	    return 0;
	    }
	else
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTHLTOut2Handler::PollForDoneEvent", "New buffer ptrs" )
		<< "Could not find done event with bus address 0x" << AliHLTLog::kHex
		<< busAddr << " (" << AliHLTLog::kDec << busAddr << ")." << ENDLOG;
	    pthread_mutex_unlock( &fEventMutex );
	    return EIO;
	    }

	}
    return EAGAIN;
    }


unsigned long AliHLTHLTOut2Handler::GetShmBusAddr( AliHLTShmID shmKey, AliUInt8_t* virtPtr )
    {

    PSI_Status    status=PSI_OK;

    if ( fSharedMemory )
        {
	if ( (shmKey.fShmType!=fLastDMAMapShmKey.fShmType || shmKey.fKey.fID!=fLastDMAMapShmKey.fKey.fID) &&
	     fLastDMAMapShmKey.fShmType!=kInvalidShmType )
	    {
	    PSI_unmapDMA( fLastDMAMapRegion );
	    fLastDMAMapShmKey.fShmType = kInvalidShmType;
	    fLastDMAMapShmKey.fKey.fID = ~0ULL;
	    fLastDMAMapBusAddress = 0;
	    fLastDMAMapRegion = 0;
	    }
	if ( fLastDMAMapShmKey.fShmType==kInvalidShmType )
	    {
	    if ( !fSharedMemory->GetPSIRegion( shmKey, fLastDMAMapRegion ) )
		{
		LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::GetShmBusAddr", "Unable to obtain shared memory buffer bus address" )
		    << "Unable to obtain bus address for shared memory buffer: " 
		    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		    << ") (cannot find shm region)." << ENDLOG;
		return 0;
		}
	    unsigned long busAddress=0;
	    status = PSI_mapDMA( fLastDMAMapRegion, fBarRegion, _toDevice_, &busAddress );
	    if ( status!=PSI_OK )
		{
		LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::GetShmBusAddr", "Unable to obtain shared memory buffer bus address" )
		    << "Unable to obtain bus address for shared memory buffer: " 
		    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		    << ") (cannot DMA map region)." << ENDLOG;
		return 0;
		}
	    fLastDMAMapBusAddress = busAddress;
	    fLastDMAMapShmKey = shmKey;
	    }
	return fLastDMAMapBusAddress;
        }
    else
        {
	void *physAddress, *busAddress;
	vector<ShmData>::iterator iter, end;
	iter = fShmData.begin();
	end = fShmData.end();
	
	while ( iter != end )
	    {
	    if ( iter->fShmKey.fShmType==shmKey.fShmType && iter->fShmKey.fKey.fID==shmKey.fKey.fID )
		return iter->fBusAddress;
	    iter++;
	    }
	
	status = PSI_getPhysAddress( virtPtr, &physAddress, &busAddress );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTHLTOut2Handler::GetShmBusAddr", "Unable to obtain shared memory buffer physical/bus address" )
		<< "Unable to obtain physical and bus address for shared memory buffer: " 
		<< PSI_strerror(status) << " (" << AliHLTLog::kDec << status
		<< ")." << ENDLOG;
	    return 0;
	    }
	ShmData sd;
	sd.fShmKey.fShmType = shmKey.fShmType;
	sd.fShmKey.fKey.fID = shmKey.fKey.fID;
	sd.fBusAddress = (unsigned long)busAddress;
	fShmData.insert( fShmData.end(), sd );
	return (unsigned long)busAddress;
	}
    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
