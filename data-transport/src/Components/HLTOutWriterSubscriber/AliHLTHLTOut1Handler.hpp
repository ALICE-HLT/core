#ifndef _ALIHLTHLTOUT1HANDLER_HPP_
#define _ALIHLTHLTOUT1HANDLER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTHLTOutHandlerInterface.hpp"
#include "MLUCVector.hpp"
#include <psi.h>

class AliHLTHLTOut1Handler: public AliHLTHLTOutHandlerInterface
    {
    public:

	typedef volatile AliUInt8_t AliVolatileUInt8_t;
	typedef volatile AliUInt32_t AliVolatileUInt32_t;
	//typedef void* AliHLTVoidPtr;
	//typedef volatile AliHLTVoidPtr AliHLTVolatileVoidPtr;
	typedef volatile void* AliHLTVolatileVoidPtr;

 	AliHLTHLTOut1Handler( int eventSlotsExp2 );
	virtual ~AliHLTHLTOut1Handler();

	virtual int Open( bool vendorDeviceIndexOrBusSlotFunction, AliUInt16_t pciVendorIDOrBus, AliUInt16_t pciDeviceIDOrSlot, AliUInt16_t pciDeviceIndexOrFunction, AliUInt16_t bar );
	virtual int Close();

	virtual int InitializeHLTOut( bool first = true );
	virtual int DeinitializeHLTOut();

	virtual int InitializeFromHLTOut( bool first = true );

	virtual int SetOptions( const std::vector<MLUCString>& options, char*& errorMsg, unsigned& errorArgNr );
	virtual void GetAllowedOptions( std::vector<MLUCString>& options );

	virtual int ActivateHLTOut();
	virtual int DeactivateHLTOut();

	virtual int SubmitEvent( AliEventID_t eventID, const AliHLTSubEventDescriptor::BlockData& block, 
				 const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* ets );
	virtual int PollForDoneEvent();

    protected:

	unsigned long GetShmBusAddr( AliHLTShmID shmKey, AliUInt8_t* virtPtr );

	AliEventID_t fEvent;
	pthread_mutex_t fEventMutex;

	struct ShmData
	    {
		AliHLTShmID fShmKey;
		unsigned long fBusAddress;
	    };
	vector<ShmData> fShmData;

	static const AliUInt32_t kRESET;
	static const AliUInt32_t kCLEARFIFO;
	static const AliUInt32_t kREPBUFPTR;
	static const AliUInt32_t kREPBUFSIZ;
	static const AliUInt32_t kEVTPTR;
	static const AliUInt32_t kEVTSIZ;

	MLUCString fBar0ID;
	tRegion fBar0Region;

	AliUInt32_t fReportBufferSize;
	MLUCString fReportBufferID;
	tRegion fReportBufferRegion;

	unsigned long fReportBufferPhysAddress;
	unsigned long fReportBufferBusAddress;
	unsigned long fReportBufferEndBusAddress;
	unsigned long fReportBufferReadPtrBusAddress;

	AliVolatileUInt8_t* fReportBuffer;
	AliVolatileUInt8_t* fReportBufferEnd;
	AliVolatileUInt8_t* fReportBufferReadPtr;
	AliVolatileUInt8_t* fReportBufferOldReadPtr;

    private:
    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTHLTOUT1HANDLER_HPP_
