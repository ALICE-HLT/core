#ifndef _ALIHLTDUMMYHLTOUTHANDLER_HPP_
#define _ALIHLTDUMMYHLTOUTHANDLER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTHLTOutHandlerInterface.hpp"
#include "MLUCVector.hpp"
#include <pthread.h>

class AliHLTDummyHLTOutHandler: public AliHLTHLTOutHandlerInterface
    {
    public:

	AliHLTDummyHLTOutHandler();
	virtual ~AliHLTDummyHLTOutHandler();

	virtual int Open( bool vendorDeviceIndexOrBusSlotFunction, AliUInt16_t pciVendorIDOrBus, AliUInt16_t pciDeviceIDOrSlot, AliUInt16_t pciDeviceIndexOrFunction, AliUInt16_t bar );
	virtual int Close();

	virtual int InitializeHLTOut( bool first = true );
	virtual int DeinitializeHLTOut();

	virtual int InitializeFromHLTOut( bool first = true );

	virtual int SetOptions( const std::vector<MLUCString>& options, char*& errorMsg, unsigned& errorArgNr );
	virtual void GetAllowedOptions( std::vector<MLUCString>& options );

	virtual int ActivateHLTOut();
	virtual int DeactivateHLTOut();

	virtual int SubmitEvent( AliEventID_t eventID, const AliHLTSubEventDescriptor::BlockData& block, 
				 const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* ets );
	virtual int PollForDoneEvent();

    protected:

	AliEventID_t fEvent;
	pthread_mutex_t fEventMutex;

	bool fActive;


    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTDUMMYHLTOUTHANDLER_HPP_
