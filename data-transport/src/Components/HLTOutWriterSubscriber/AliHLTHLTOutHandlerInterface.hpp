#ifndef _ALIHLTHLTOUTHANDLERINTERFACE_HPP_
#define _ALIHLTHLTOUTHANDLERINTERFACE_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliEventID.h"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTSubEventDescriptor.hpp"
#include "AliHLTEventTriggerStruct.h"
#include "MLUCString.hpp"
#include <vector>

class AliHLTHLTOutHandlerInterface
    {
    public:

	virtual ~AliHLTHLTOutHandlerInterface() {};

	virtual int Open( bool vendorDeviceIndexOrBusSlotFunction, AliUInt16_t pciVendorIDOrBus, AliUInt16_t pciDeviceIDOrSlot, AliUInt16_t pciDeviceIndexOrFunction, AliUInt16_t bar ) = 0;
	virtual int Close() = 0;

	virtual int InitializeHLTOut( bool first = true ) = 0;
	virtual int DeinitializeHLTOut() = 0;

	virtual int InitializeFromHLTOut( bool first = true ) = 0;

	virtual int SetOptions( const std::vector<MLUCString>& options, char*& errorMsg, unsigned& errorArgNr ) = 0;
	virtual void GetAllowedOptions( std::vector<MLUCString>& options ) = 0;

	virtual int ActivateHLTOut() = 0;
	virtual int DeactivateHLTOut() = 0;

	virtual int SubmitEvent( AliEventID_t eventID, const AliHLTSubEventDescriptor::BlockData& block, 
				 const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* ets ) = 0;
	virtual int PollForDoneEvent() = 0;

    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTHLTOUTHANDLERINTERFACE_HPP_
