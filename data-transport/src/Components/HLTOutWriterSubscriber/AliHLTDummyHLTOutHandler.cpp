/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTDummyHLTOutHandler.hpp"
#include "AliHLTLog.hpp"
#include <errno.h>

AliHLTDummyHLTOutHandler::AliHLTDummyHLTOutHandler()
    {
    fActive = false;
    fEvent = AliEventID_t();
    pthread_mutex_init( &fEventMutex, NULL );
    }

AliHLTDummyHLTOutHandler::~AliHLTDummyHLTOutHandler()
    {
    pthread_mutex_destroy( &fEventMutex );
    }


int AliHLTDummyHLTOutHandler::Open( bool, AliUInt16_t, AliUInt16_t, AliUInt16_t, AliUInt16_t )
    {
    return 0;
    }

int AliHLTDummyHLTOutHandler::Close()
    {
    return 0;
    }


int AliHLTDummyHLTOutHandler::InitializeHLTOut( bool )
    {
    return 0;
    }

int AliHLTDummyHLTOutHandler::DeinitializeHLTOut()
    {
    return 0;
    }


int AliHLTDummyHLTOutHandler::InitializeFromHLTOut( bool )
    {
    return 0;
    }


int AliHLTDummyHLTOutHandler::SetOptions( const std::vector<MLUCString>&, char*&, unsigned& )
    {
    return 0;
    }

void AliHLTDummyHLTOutHandler::GetAllowedOptions( std::vector<MLUCString>& )
    {
    }


int AliHLTDummyHLTOutHandler::ActivateHLTOut()
    {
    fActive = true;
    return 0;
    }

int AliHLTDummyHLTOutHandler::DeactivateHLTOut()
    {
    fActive = false;
    return 0;
    }


int AliHLTDummyHLTOutHandler::SubmitEvent( AliEventID_t eventID, const AliHLTSubEventDescriptor::BlockData& block, 
				 const AliHLTSubEventDataDescriptor*, const AliHLTEventTriggerStruct* )
    {
    LOG( AliHLTLog::kDebug, "AliHLTDummyHLTOutHandler::SubmitEvent", "Event submitted" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << ") submitted: block.fOffset: "
	<< block.fOffset << " - block.fSize: " << block.fSize << "." << ENDLOG;
    pthread_mutex_lock( &fEventMutex );
    fEvent = eventID;
    pthread_mutex_unlock( &fEventMutex );
    return 0;
    }

int AliHLTDummyHLTOutHandler::PollForDoneEvent()
    {
    if ( !fActive )
	return EAGAIN;
    bool found=false;
    pthread_mutex_lock( &fEventMutex );
    if ( fEvent!=AliEventID_t() )
	{
	found=true;
	LOG( AliHLTLog::kDebug, "AliHLTDummyHLTOutHandler::PollForDoneEvent", "Done event polled" )
	    << "Polled done event 0x" << AliHLTLog::kHex << fEvent
	    << " (" << AliHLTLog::kDec << fEvent << ")." << ENDLOG;
	fEvent = AliEventID_t();
	}
    pthread_mutex_unlock( &fEventMutex );
    if ( found )
	return 0;
    else
	return EAGAIN;
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
