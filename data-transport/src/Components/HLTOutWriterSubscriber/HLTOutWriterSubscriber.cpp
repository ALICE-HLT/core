/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTHLTOutWriterSubscriber.hpp"
//#include "AliHLTControlledProcessComponent.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTHLTOut1Handler.hpp"
#include "AliHLTHLTOut2Handler.hpp"
#include "AliHLTDummyHLTOutHandler.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTReadoutList.hpp"
#include <vector>

class HLTOutWriterSubscriberComponent: public AliHLTControlledDefaultSinkComponent<AliHLTHLTOutWriterSubscriber>
    {
    public:

	HLTOutWriterSubscriberComponent( const char* name, int argc, char** argv );
	virtual ~HLTOutWriterSubscriberComponent() {};


    protected:

	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

	virtual bool CheckConfiguration();
	virtual void ShowConfiguration();

	virtual bool CreateParts();
	virtual void DestroyParts();
	virtual bool SetupComponents();

	bool fDoSleep;
	unsigned long fSleepTime;
	bool fPCIDeviceInfoSet;
	bool fVendorDeviceIndexOrBusSlotFunction;
	AliUInt16_t fPCIVendorIDOrBus;
	AliUInt16_t fPCIDeviceIDOrSlot;
	AliUInt16_t fPCIDeviceIndexOrFunction;
	AliUInt16_t fBar;

	bool fFirstStart;

	bool fSimpleMode;

	bool fDummy;

	AliHLTHLTOutHandlerInterface* fHLTOutHandler;

	unsigned long fHLTOutVersion;

	unsigned long fDDLID;

	unsigned long fNotDoneWarnTime_us;
	bool fNotDoneWarnRepeat;
	unsigned long fNotDoneAbortTime_us;

	bool fNoDMAMap;

	bool fBroadcastEventMaster;

	bool fDoEventLog;

	bool fDumpSuspiciousEvents;

	unsigned long long fReadoutListOutputReduction;

    private:
    };


HLTOutWriterSubscriberComponent::HLTOutWriterSubscriberComponent( const char* name, int argc, char** argv ):
    AliHLTControlledDefaultSinkComponent<AliHLTHLTOutWriterSubscriber>( name, argc, argv )
    {
    fDoSleep = false;
    fSleepTime = 10000;
    fPCIDeviceInfoSet = false;
    fVendorDeviceIndexOrBusSlotFunction = false;
    fPCIVendorIDOrBus = 0xFFFF;
    fPCIDeviceIDOrSlot = 0xFFFF;
    fPCIDeviceIndexOrFunction = 0xFFFF;
    fBar = 0xFFFF;
    fHLTOutHandler = NULL;
    fFirstStart = true;
    fDummy = false;
    fSimpleMode=false;
    fHLTOutVersion = 2;
    fDDLID=~(unsigned long)0;

    fNotDoneWarnTime_us = 0;
    fNotDoneWarnRepeat = false;
    fNotDoneAbortTime_us = 0;

    fNoDMAMap = false;

    fBroadcastEventMaster = false;

    fDoEventLog = false;
    fDumpSuspiciousEvents = false;
    fReadoutListOutputReduction = 0;
    }




const char* HLTOutWriterSubscriberComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cpErr = NULL;
    if ( !strcmp( argv[i], "-device" ) )
	{
	if ( argc <= i+4 )
	    return "Missing pci device information parameter";
	fPCIVendorIDOrBus = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting pci vendor specifier.";
	    }
	fPCIDeviceIDOrSlot = strtoul( argv[i+2], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+2;
	    return "Error converting pci device specifier.";
	    }
	fPCIDeviceIndexOrFunction = strtoul( argv[i+3], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+3;
	    return "Error converting pci device index specifier.";
	    }
	fBar = strtoul( argv[i+4], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+3;
	    return "Error converting pci bar specifier.";
	    }
	fPCIDeviceInfoSet = true;
	fVendorDeviceIndexOrBusSlotFunction = true;
	i += 5;
	return NULL;
	}
    if ( !strcmp( argv[i], "-slot" ) )
	{
	if ( argc <= i+4 )
	    return "Missing pci geographical address parameter";
	fPCIVendorIDOrBus = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting pci bus specifier.";
	    }
	fPCIDeviceIDOrSlot = strtoul( argv[i+2], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+2;
	    return "Error converting pci slot specifier.";
	    }
	fPCIDeviceIndexOrFunction = strtoul( argv[i+3], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+3;
	    return "Error converting pci function specifier.";
	    }
	fBar = strtoul( argv[i+4], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+3;
	    return "Error converting pci bar specifier.";
	    }
	fPCIDeviceInfoSet = true;
	fVendorDeviceIndexOrBusSlotFunction = false;
	i += 5;
	return NULL;
	}
    if ( !strcmp( argv[i], "-sleep" ) )
	{
	fDoSleep = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-simplemode" ) )
	{
	fSimpleMode = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-restart" ) )
	{
	fFirstStart = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-dummydevice" ) )
	{
	fDummy = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-sleeptime" ) )
	{
	if ( argc <= i+1 )
	    return "Missing sleep time specifier.";
	fSleepTime = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting sleep time specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-hltoutinterface" ) )
	{
	if ( argc <= i+1 )
	    return "Missing HLTOut version specifier.";
	fHLTOutVersion = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting HLTOut version specifier.";
	    }
	if ( fHLTOutVersion<1 || fHLTOutVersion>2 )
	    {
	    errorArg = i+1;
	    return "Invalid HLTOut version specifier, allowed values: 1, 2.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-ddlid" ) )
	{
	if ( argc <= i+1 )
	    return "Missing DDL ID specifier.";
	fDDLID = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting DDL ID specifier.";
	    }
	if ( AliHLTReadoutList::GetDetectorID(fDDLID)!=kHLT )
	    {
	    errorArg = i+1;
	    return "Specified DDL ID is not part of HLT.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-broadcasteventmaster" ) )
	{
	fBroadcastEventMaster = true;
	++i;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventdonepollwarning" ) )
	{
	if ( argc <= i+1 )
	    return "Missing HLTOut event done poll warning interval specifier.";
	fNotDoneWarnTime_us = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting HLTOut event done poll warning interval specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-repeateventdonepollwarning" ) )
	{
	fNotDoneWarnRepeat = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventdonepolltimeout" ) )
	{
	if ( argc <= i+1 )
	    return "Missing HLTOut event done poll timeout specifier.";
	fNotDoneAbortTime_us = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting HLTOut event done poll timeout specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-nodmamap" ) )
	{
	fNoDMAMap = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventlog" ) )
	{
	fDoEventLog = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-noeventlog" ) )
	{
	fDoEventLog = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-dumpsuspiciousevents" ) )
	{
	fDumpSuspiciousEvents = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-readoutlistoutput" ) )
	{
	if ( argc <= i+1 )
	    return "Missing readout list output reduction specifier.";
	fReadoutListOutputReduction = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting readout list output reduction specifier.";
	    }
	i += 2;
	return NULL;
	}

    
    return AliHLTControlledDefaultSinkComponent<AliHLTHLTOutWriterSubscriber>::ParseCmdLine( argc, argv, i, errorArg );
    }

void HLTOutWriterSubscriberComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    AliHLTControlledDefaultSinkComponent<AliHLTHLTOutWriterSubscriber>::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-device <pciVendorID> <pciDeviceID> <pciDeviceIndex> <bar-nr>: Specify the PCI device to use. (Mandatory optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-slot <pciBus> <pciSlot> <pciFunction> <bar-nr>: Specify the PCI device to use. (Mandatory optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-sleep: Sleep for a specified time when no event is available. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-sleeptime <sleeptime_usec>: Specify the time in microseconds to sleep (with -sleep) when no event is available. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-restart: Specify that this is a restart, do not initialize HLTOut and erase memories. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-dummydevice: Specify to simulate a HLTOut device and not use a real one. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-hltoutinterface <HLTOut-Firmware-Interface-Version>: Specify the version number of the HLTOut firmware to use. (Optional; Allowed 1, 2; Default 2)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-ddlid <ID of used DDL>: Specify the ID of the HLT output DDL which is fed by this component. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-alloutputddlids <DDL-ID-list>: Specify a list of all DDL HLT output IDs (comma separated list of DDL IDs). (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-broadcasteventmaster: Specify that this component is the master among equal partners of the same type. (Optional, should be activated only for one component instance)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-eventdonepollwarning <event-done-poll-warning-time-microsec.>: If an event that has been submitted to the HLTOut is not reported as done in the given time interval in microseconds a warning will be printed. (Optional, 0 to disable)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-repeateventdonepollwarning: Repeat the warning if an event that has been submitted to the HLTOut is not reported as done regularly. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-eventdonepolltimeout <event-done-poll-timeout-time-microsec.>: If an event that has been submitted to the HLTOut is not reported as done in the given time interval in microseconds it will be aborted. (Optional, 0 to disable)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-nodmamap: Do not perform DMA memory to device mapping, instead just try to use simple determination of memory bus address. (Optional, not used for interface version 1)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-eventlog: Do log every event to avoid writing events out multiple times. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-noeventlog: Do not log every event to avoid writing events out multiple times. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-dumpsuspiciousevents: Dump events to disk which have some strangeness in their data. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-readoutlistoutput <readout list output reduction specifier>: Log the readout list for events, parameters is a reduction number (first N events are shown, then N*2, N*4, N*8, ...). (Optional)" << ENDLOG;
    }

bool HLTOutWriterSubscriberComponent::CheckConfiguration()
    {
    if ( fMinBlockSize > fBufferSize )
	{
	LOG( AliHLTLog::kWarning, "HLTOutWriterSubscriberComponent::CheckConfiguration", "Block size bigger than buffer size" )
	    << "Specified min. block size of " << AliHLTLog::kDec << fMinBlockSize << " bytes is greater than buffer size of "
	    << fBufferSize << " bytes. - Reducing block size to buffer size." << ENDLOG;
	fMinBlockSize = fBufferSize;
	}
    if ( !AliHLTControlledComponent::CheckConfiguration() )
	return false;
    if ( !fPCIDeviceInfoSet )
	{
	LOG( AliHLTLog::kError, "HLTOutWriterSubscriberComponent::CheckConfiguration", "No pci device specified" )
	    << "Must specify the PCI device to be used (e.g. using the -device or -slot command line option."
	    << ENDLOG;
	return false;
	}
    if ( fBar!=0 && fHLTOutVersion==1 )
	{
	LOG( AliHLTLog::kError, "HLTOutWriterSubscriberComponent::CheckConfiguration", "HLTOut interface version 1 has only bar 0" )
	    << "HLTOut interface version 0 only supports 1 bar (bar 0).."
	    << ENDLOG;
	return false;
	}
    return true;
    }

void HLTOutWriterSubscriberComponent::ShowConfiguration()
    {
    AliHLTControlledComponent::ShowConfiguration();
    if ( fVendorDeviceIndexOrBusSlotFunction )
	{
	LOG( AliHLTLog::kInformational, "HLTOutWriterSubscriberComponent::ShowConfiguration", "PCI Device Configuration" )
	    << "PCI device: Vendor ID: 0x" << AliHLTLog::kHex << fPCIVendorIDOrBus << " - " << " Device ID: 0x"
	    << fPCIDeviceIDOrSlot << " - Device Index: " << AliHLTLog::kDec << fPCIDeviceIndexOrFunction << " - Bar "
	    << fBar << "." << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kInformational, "HLTOutWriterSubscriberComponent::ShowConfiguration", "PCI Device Configuration" )
	    << "PCI geographical address: Bus: 0x" << AliHLTLog::kHex << fPCIVendorIDOrBus << " - " << " Slot: 0x"
	    << fPCIDeviceIDOrSlot << " - Function: " << AliHLTLog::kDec << fPCIDeviceIndexOrFunction << " - Bar "
	    << fBar << "." << ENDLOG;
	}
    LOG( AliHLTLog::kInformational, "HLTOutWriterSubscriberComponent::ShowConfiguration", "Start Mode" )
	<< "Start Mode: " << (fFirstStart ? "First Start" : "Restart" ) << "." << ENDLOG;
    LOG( AliHLTLog::kInformational, "HLTOutWriterSubscriberComponent::ShowConfiguration", "HLTOut Firmware Version" )
	<< "Using HLTOut firmware version " << AliHLTLog::kDec << fHLTOutVersion << "." << ENDLOG;
    if ( fDummy )
	{
	LOG( AliHLTLog::kWarning, "HLTOutWriterSubscriberComponent::ShowConfiguration", "Dummy Mode" )
	    << "Using dummy device, no real data will be available." << ENDLOG;
	}
    if ( !fDoEventLog )
	{
	LOG( AliHLTLog::kInformational, "HLTOutWriterSubscriberComponent::ShowConfiguration", "Event log disabled" )
	    << "Event logging disabled." << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kInformational, "HLTOutWriterSubscriberComponent::ShowConfiguration", "Event log enabled" )
	    << "Event logging enabled." << ENDLOG;
	}
    if ( fDumpSuspiciousEvents )
	{
	LOG( AliHLTLog::kInformational, "HLTOutWriterSubscriberComponent::ShowConfiguration", "Dump suspicious events" )
	    << "Suspicious events will be dumped to disk." << ENDLOG;
	}
    }

bool HLTOutWriterSubscriberComponent::CreateParts()
    {
    if ( !AliHLTControlledComponent::CreateParts() )
	return false;
    if ( !fDummy )
	{
	switch ( fHLTOutVersion )
	    {
	    case 1:
		fHLTOutHandler = new AliHLTHLTOut1Handler( fMaxPreEventCountExp2 );
		break;
	    case 2:
		fHLTOutHandler = new AliHLTHLTOut2Handler( fMaxPreEventCountExp2 );
		break;
	    }
	}
    else
	fHLTOutHandler = new AliHLTDummyHLTOutHandler();
    if ( !fHLTOutHandler )
	{
	LOG( AliHLTLog::kError, "HLTOutWriterSubscriberComponent::CreateParts", "Unable to create HLTOut handler object" )
	    << "Unable to create HLTOut handler object." << ENDLOG;
	return false;
	}
    return true;
    }

void HLTOutWriterSubscriberComponent::DestroyParts()
    {
    if ( fHLTOutHandler )
	{
	fHLTOutHandler->DeinitializeHLTOut();
	fHLTOutHandler->Close();
	delete fHLTOutHandler;
	fHLTOutHandler = NULL;
	}
    AliHLTControlledComponent::DestroyParts();
    }

bool HLTOutWriterSubscriberComponent::SetupComponents()
    {
    if ( !AliHLTControlledComponent::SetupComponents() )
	return false;

    int ret;
    if ( !fDummy && !fNoDMAMap && fShmManager )
	{
	if ( fHLTOutVersion==2 )
	    ((AliHLTHLTOut2Handler*)fHLTOutHandler)->SetSharedMemory( fShmManager->GetSharedMemoryObject() );
	}


    ret = fHLTOutHandler->Open( fVendorDeviceIndexOrBusSlotFunction, fPCIVendorIDOrBus, fPCIDeviceIDOrSlot, fPCIDeviceIndexOrFunction, fBar );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "HLTOutWriterSubscriberComponent::SetupComponents", "Error opening HLTOut" )
	    << "Error opening HLTOut device: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return false;
	}
    ret = fHLTOutHandler->InitializeHLTOut( fFirstStart );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "HLTOutWriterSubscriberComponent::SetupComponents", "Error initializing HLTOut" )
	    << "Error initializing HLTOut hardware: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return false;
	}
    ret = fHLTOutHandler->InitializeFromHLTOut( fFirstStart );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "HLTOutWriterSubscriberComponent::SetupComponents", "Error initializing from HLTOut" )
	    << "Error initializing HLTOut handler object from HLTOut hardware: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return false;
	}
    
    AliUInt8_t* eventBuffer;
    eventBuffer = fShmManager->GetShm( fShmKey );

    if ( fSimpleMode )
	((AliHLTHLTOutWriterSubscriber*)fSubscriber)->SimpleMode();
    ((AliHLTHLTOutWriterSubscriber*)fSubscriber)->SetSleep( fDoSleep );
    ((AliHLTHLTOutWriterSubscriber*)fSubscriber)->SetSleepTime( fSleepTime );
    ((AliHLTHLTOutWriterSubscriber*)fSubscriber)->SetHLTOutHandler( fHLTOutHandler );
    ((AliHLTHLTOutWriterSubscriber*)fSubscriber)->SetShm( fShmKey, eventBuffer, fBufferSize );
    ((AliHLTHLTOutWriterSubscriber*)fSubscriber)->SetNotDoneWarnTime( fNotDoneWarnTime_us, fNotDoneWarnRepeat );
    ((AliHLTHLTOutWriterSubscriber*)fSubscriber)->SetNotDoneAbortTime( fNotDoneAbortTime_us );
    ((AliHLTHLTOutWriterSubscriber*)fSubscriber)->BroadcastEventMaster( fBroadcastEventMaster );
    if ( fReadoutListOutputReduction )
	((AliHLTHLTOutWriterSubscriber*)fSubscriber)->SetReadoutListOutputReduction( fReadoutListOutputReduction );
    if ( fHLTMode==1 || fHLTMode==4 || fHLTMode==5 )
	{
	LOG( AliHLTLog::kInformational, "HLTOutWriterSubscriberComponent::SetupComponents", "Disabling HLTOut output" )
	    << "Disabling HLT output because of HLT mode " << AliHLTLog::kDec << fHLTMode << "." << ENDLOG;
	((AliHLTHLTOutWriterSubscriber*)fSubscriber)->Disable();
	}
    if ( fDDLID != ~(unsigned long)0 )
	((AliHLTHLTOutWriterSubscriber*)fSubscriber)->SetDDLID( fDDLID );

    ((AliHLTHLTOutWriterSubscriber*)fSubscriber)->SetEventLog( fDoEventLog );
    if ( fDumpSuspiciousEvents )
	((AliHLTHLTOutWriterSubscriber*)fSubscriber)->DumpSuspiciousEvents( fDumpSuspiciousEvents );

    return true;
    }

int main( int argc, char** argv )
    {
    HLTOutWriterSubscriberComponent procComp( "HLTOutWriterSubscriber", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
