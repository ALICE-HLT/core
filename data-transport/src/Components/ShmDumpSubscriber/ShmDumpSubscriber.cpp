/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTProcessingSubscriber.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTAlignment.h"
#include "HOMERData.h"
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <errno.h>



class ShmDumpSubscriber: public AliHLTProcessingSubscriber
    {
    public:
	ShmDumpSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize,
			   AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, int eventCountAlloc = -1 );
	virtual ~ShmDumpSubscriber();
	
	void SetOutputDestination( AliUInt8_t* dumpDest, unsigned long dumpDestSize );

	void SetFillMode( bool fillMode )
		{
		fFillMode = fillMode; /* true means wait for free output shared memory buffer before filling event
				       * false means if output shared memory buffer is full skip the event */
		}
	
    protected:
	
	virtual bool ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, 
				   const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* etsp,
				   AliUInt8_t* outputPtr, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, AliHLTEventDoneData*& edd );
	
	AliUInt8_t* fDumpDest;
	unsigned long fDumpDestSize;

	bool fFillMode;
	
    private:
    };

ShmDumpSubscriber::ShmDumpSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize,
				      AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, int eventCountAlloc ):
    AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc )
    {
    fDumpDest = NULL;
    fDumpDestSize = 0;
    fFillMode = false;
    }

ShmDumpSubscriber::~ShmDumpSubscriber()
    {
    }

void ShmDumpSubscriber::SetOutputDestination( AliUInt8_t* dumpDest, unsigned long dumpDestSize )
    {
    fDumpDest = dumpDest;
    fDumpDestSize = dumpDestSize;
    LOG( AliHLTLog::kDebug, "ShmDumpSubscriber::SetOutputDestination", "Event target buffer" )
	<< "Event target buffer: 0x" << AliHLTLog::kHex << (unsigned long)fDumpDest << " - Size: "
	<< AliHLTLog::kDec << fDumpDestSize << " (0x" << AliHLTLog::kHex
	<< fDumpDestSize << "." << ENDLOG;
    }

bool ShmDumpSubscriber::ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, 
					    AliHLTSubEventDescriptor::BlockData* blocks, 
					    const AliHLTSubEventDataDescriptor* sedd, 
					    const AliHLTEventTriggerStruct*,
					    AliUInt8_t*, AliUInt32_t& outputSize, 
					    vector<AliHLTSubEventDescriptor::BlockData>&, 
					    AliHLTEventDoneData*& )
    {
    outputSize = 0;
    if ( !fDumpDest )
	{
	LOG( AliHLTLog::kError, "ShmDumpSubscriber::ProcessEvent", "No event target buffer" )
	    << "No event target buffer specified." << ENDLOG;
	return true;
	}
    if ( *(AliUInt32_t*)fDumpDest )
	{
	if ( fFillMode )
	    {
	    LOG( AliHLTLog::kDebug, "ShmDumpSubscriber::ProcessEvent", "Event buffer still full" )
		<< "Target event buffer still full. Deferring event 0x" 
		<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		<< ")." << ENDLOG;
	    return false;
	    }
	else
	    {
	    LOG( AliHLTLog::kDebug, "ShmDumpSubscriber::ProcessEvent", "Event buffer still full" )
		<< "Target event buffer still full. Skipping event 0x" 
		<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		<< ")." << ENDLOG;
	    return true;
	    }
	}
    unsigned long long totalSize=0;
    unsigned long n;
    //totalSize = sizeof(AliUInt32_t); // size word at beginning
    totalSize += HOMERBlockDescriptor::GetHOMERBlockDescriptorSize()*(blockCnt+1);
    for ( n = 0; n < blockCnt; n++ )
	totalSize += blocks[n].fSize;
    if ( totalSize > fDumpDestSize )
	{
	LOG( AliHLTLog::kWarning, "ShmDumpSubscriber::ProcessEvent", "Event too large" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") is too large (" << totalSize 
	    << " bytes) to be written into the event buffer ("
	    << fDumpDestSize << " bytes)." << ENDLOG;
	return true;
	}
    
    unsigned long long offset = 0; // sizeof(AliUInt32_t);
    LOG( AliHLTLog::kDebug, "ShmDumpSubscriber::ProcessEvent", "Offset" )
	<< "Offset: " << AliHLTLog::kDec << offset << " (0x" << AliHLTLog::kHex
	<< offset << ")." << ENDLOG;
    // Copy sub-event descriptor

    struct timeval now;
    gettimeofday( &now, NULL );
    AliUInt8_t* ribd;
    ribd = (AliUInt8_t*)( fDumpDest+offset+sizeof(AliUInt32_t) );
    memset( ribd, 0, HOMERBlockDescriptor::GetHOMERBlockDescriptorSize() );
    HOMERBlockDescriptor homerBlock;
    homerBlock.UseHeader( reinterpret_cast<void*>( ribd ) );
    homerBlock.Initialize();
    homerBlock.SetUInt64Alignment( AliHLTDetermineUInt64Alignment() );
    homerBlock.SetUInt32Alignment( AliHLTDetermineUInt32Alignment() );
    homerBlock.SetUInt16Alignment( AliHLTDetermineUInt16Alignment() );
    homerBlock.SetUInt8Alignment( AliHLTDetermineUInt8Alignment() );
    homerBlock.SetDoubleAlignment( AliHLTDetermineDoubleAlignment() );
    homerBlock.SetFloatAlignment( AliHLTDetermineFloatAlignment() );
    homerBlock.SetType( (uint64)eventID.fType );
    homerBlock.SetSubType1( eventID.fNr );
    homerBlock.SetSubType2( blockCnt );
    homerBlock.SetBirth_s( now.tv_sec );
    homerBlock.SetBirth_us( now.tv_usec );
    homerBlock.SetProducerNode( (uint64)fNodeID );
    homerBlock.SetBlockOffset( offset+homerBlock.GetHeaderLength() );
    homerBlock.SetBlockSize( HOMERBlockDescriptor::GetHOMERBlockDescriptorSize()*blockCnt );
    offset += homerBlock.GetHeaderLength();
    LOG( AliHLTLog::kDebug, "ShmDumpSubscriber::ProcessEvent", "Offset" )
	<< "Offset: " << AliHLTLog::kDec << offset << " (0x" << AliHLTLog::kHex
	<< offset << ")." << ENDLOG;

    unsigned long newOffset = offset + homerBlock.GetBlockSize();
    ribd = (AliUInt8_t*)( fDumpDest+offset+sizeof(AliUInt32_t) );
    offset = newOffset;
    LOG( AliHLTLog::kDebug, "ShmDumpSubscriber::ProcessEvent", "Offset" )
	<< "Offset: " << AliHLTLog::kDec << offset << " (0x" << AliHLTLog::kHex
	<< offset << ")." << ENDLOG;
    for ( n = 0; n < blockCnt; n++ )
	{
	memset( ribd+n*HOMERBlockDescriptor::GetHOMERBlockDescriptorSize(), 0, HOMERBlockDescriptor::GetHOMERBlockDescriptorSize() );
	homerBlock.UseHeader( ribd + HOMERBlockDescriptor::GetHOMERBlockDescriptorSize()*n );
	homerBlock.Initialize();
	homerBlock.SetByteOrder( blocks[n].fAttributes[kAliHLTSEDBDByteOrderAttributeIndex] );
	homerBlock.SetUInt64Alignment( blocks[n].fAttributes[kAliHLTSEDBD64BitAlignmentAttributeIndex] );
	homerBlock.SetUInt32Alignment( blocks[n].fAttributes[kAliHLTSEDBD32BitAlignmentAttributeIndex] );
	homerBlock.SetUInt16Alignment( blocks[n].fAttributes[kAliHLTSEDBD16BitAlignmentAttributeIndex] );
	homerBlock.SetUInt8Alignment( blocks[n].fAttributes[kAliHLTSEDBD8BitAlignmentAttributeIndex] );
	homerBlock.SetDoubleAlignment( blocks[n].fAttributes[kAliHLTSEDBDDoubleAlignmentAttributeIndex] );
	homerBlock.SetFloatAlignment( blocks[n].fAttributes[kAliHLTSEDBDFloatAlignmentAttributeIndex] );
	homerBlock.SetType( blocks[n].fDataType.fID );
	homerBlock.SetSubType1( (uint64)blocks[n].fDataOrigin.fID );
	homerBlock.SetSubType2( (uint64)blocks[n].fDataSpecification );
	homerBlock.SetBirth_s( sedd->fEventBirth_s );
	homerBlock.SetBirth_us( sedd->fEventBirth_us );
	homerBlock.SetProducerNode( (uint64)blocks[n].fProducerNode );
	homerBlock.SetBlockOffset( offset );
	homerBlock.SetBlockSize( blocks[n].fSize );
	LOG( AliHLTLog::kDebug, "ShmDumpSubscriber::ProcessEvent", "Data Block" )
	    << "Block " << AliHLTLog::kDec << n << " Offset: " << AliHLTLog::kDec << offset << " (0x" << AliHLTLog::kHex
	    << offset << ") - Size: " << AliHLTLog::kDec << blocks[n].fSize << " (0x"
	    << AliHLTLog::kHex << blocks[n].fSize << ")." << ENDLOG;
	memcpy( fDumpDest+offset+sizeof(AliUInt32_t), blocks[n].fData, blocks[n].fSize );
	offset += blocks[n].fSize;
	LOG( AliHLTLog::kDebug, "ShmDumpSubscriber::ProcessEvent", "Offset" )
	    << "Offset: " << AliHLTLog::kDec << offset << " (0x" << AliHLTLog::kHex
	    << offset << ")." << ENDLOG;
	}
    
    *(AliUInt32_t*)fDumpDest = totalSize;
    LOG( AliHLTLog::kDebug, "ShmDumpSubscriber::ProcessEvent", "Event written" )
	<< "Event 0x" 
	<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
	<< ") written into event buffer." << ENDLOG;
    
    return true;
    }


class ShmDumpSubscriberComponent: public AliHLTControlledDefaultSinkComponent<ShmDumpSubscriber>
    {
    public:
  
	ShmDumpSubscriberComponent( const char* compName, int argc, char** argv );
	virtual ~ShmDumpSubscriberComponent();
  
    protected:

	virtual bool CheckConfiguration();
	virtual void ShowConfiguration();
  
	virtual bool CreateParts();
	virtual void DestroyParts();
  
	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
  
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );
  
	virtual bool SetupComponents();

	unsigned long fOutShmSize;
	key_t fOutShmKey;
	int fOutShmID;
	AliUInt8_t* fOutShmPtr;

	bool fFillMode;

    private:
    };


ShmDumpSubscriberComponent::ShmDumpSubscriberComponent( const char* compName, int argc, char** argv ):
    AliHLTControlledDefaultSinkComponent<ShmDumpSubscriber>( compName, argc, argv )
    {
    fOutShmSize = 0;
    fOutShmKey = (key_t)-1;
    fOutShmID = -1;
    fOutShmPtr = NULL;
    fFillMode = false;
    }

ShmDumpSubscriberComponent::~ShmDumpSubscriberComponent()
    {
    }
  
bool ShmDumpSubscriberComponent::CheckConfiguration()
    {
    if ( !AliHLTControlledSinkComponent::CheckConfiguration() )
	return false;
    if ( !fOutShmSize )
	{
	LOG( AliHLTLog::kError, "ShmDumpSubscriberComponent::CheckConfiguration", "No output shared memory specified" )
	    << "Must specify a System V output shared memory to be used > 0 (e.g. using the -outputshm command line option."
	    << ENDLOG;
	return false;
	}
    return true;
    }

void ShmDumpSubscriberComponent::ShowConfiguration()
    {
    AliHLTControlledComponent::ShowConfiguration();
    LOG( AliHLTLog::kInformational, "ShmDumpSubscriberComponent::ShowConfiguration", "Output Shm Config" )
	<< "Output System V shm key: " << AliHLTLog::kDec << (unsigned)fOutShmKey << " (0x"
	<< AliHLTLog::kHex << (unsigned)fOutShmKey << ") - Output shm size: "
	<< AliHLTLog::kDec << fOutShmSize << "." << ENDLOG;
    LOG( AliHLTLog::kInformational, "ShmDumpSubscriberComponent::ShowConfiguration", "Fill Mode" )
	<< "New incoming events are " << (fFillMode ? "deferred" : "skipped" )
	<< " when output buffer is full." << ENDLOG;
    }

bool ShmDumpSubscriberComponent::CreateParts()
    {
    if ( !AliHLTControlledComponent::CreateParts() )
	return false;
    fOutShmID = shmget( fOutShmKey, fOutShmSize, IPC_CREAT|0660 );
    if ( fOutShmID == -1 )
	{
	LOG( AliHLTLog::kError, "ShmDumpSubscriberComponent::CreateParts", "Unable to create output shm" )
	    << "Unable to create output shared memory: " << strerror(errno) << " ("
	    << AliHLTLog::kDec << errno << ")." << ENDLOG;
	return false;
	}
    fOutShmPtr = reinterpret_cast<AliUInt8_t*>( shmat( fOutShmID, NULL, 0 ) );
    if ( !fOutShmPtr )
	{
	LOG( AliHLTLog::kError, "ShmDumpSubscriberComponent::CreateParts", "Unable to attach output shm" )
	    << "Unable to attach (map) output shared memory: " << strerror(errno) << " ("
	    << AliHLTLog::kDec << errno << ")." << ENDLOG;
	return false;
	}
    *(AliUInt32_t*)fOutShmPtr = 0;
    LOG( AliHLTLog::kDebug, "ShmDumpSubscriberComponent::CreateParts", "Output Shm" )
	<< "Output shared memory: 0x" << AliHLTLog::kHex << (unsigned long)fOutShmPtr << " - Size: "
	<< AliHLTLog::kDec << fOutShmSize << " (0x" << AliHLTLog::kHex
	<< fOutShmSize << "." << ENDLOG;
    return true;
    }

void ShmDumpSubscriberComponent::DestroyParts()
    {
    if ( fOutShmPtr )
	{
	shmdt( fOutShmPtr );
	fOutShmPtr = NULL;
	}
    if ( fOutShmID != -1 )
	{
	shmctl( fOutShmID, IPC_RMID, NULL );
	fOutShmID = -1;
	}
    }

const char* ShmDumpSubscriberComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cpErr = NULL;
    if ( !strcmp( argv[i], "-fillmode-wait" ) )
	{
	fFillMode = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-outputshm" ) )
	{
	if ( argc <= i+2 )
	    return "Missing output shared memory information parameter";
	fOutShmKey = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting output shared memory key specifier.";
	    }
	fOutShmSize = strtoul( argv[i+2], &cpErr, 0 );
	if ( *cpErr == 'k')
	    fOutShmSize *= 1024;
	else if ( *cpErr == 'M')
	    fOutShmSize *= 1024*1024;
	else if ( *cpErr == 'G')
	    fOutShmSize *= 1024*1024*1024;
	else if ( *cpErr )
	    {
	    errorArg = i+2;
	    return "Error converting output shared memory size specifier.";
	    }
	i += 3;
	return NULL;
	}
    return AliHLTControlledDefaultSinkComponent<ShmDumpSubscriber>::ParseCmdLine( argc, argv, i, errorArg );
    }
  
void ShmDumpSubscriberComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    AliHLTControlledDefaultSinkComponent<ShmDumpSubscriber>::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-outputshm <shm-key> <shm-size>: Specify the System V output shared memory to use. (Mandatory)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-fillmode-wait: Defer incoming events when output shm buffer is full for later retry (Default: events are skipped). (Optional)" << ENDLOG;
    }
 

bool ShmDumpSubscriberComponent::SetupComponents()
    {
    if ( !AliHLTControlledComponent::SetupComponents() )
	return false;
    ((ShmDumpSubscriber*)fSubscriber)->SetOutputDestination( fOutShmPtr, fOutShmSize );
    ((ShmDumpSubscriber*)fSubscriber)->SetFillMode( fFillMode );
    return true;
    }


int main( int argc, char** argv )
    {
    ShmDumpSubscriberComponent procComp( "ShmDumpSubscriber", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
