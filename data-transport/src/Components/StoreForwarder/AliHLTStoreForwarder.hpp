/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3DUMMYLOAD_HPP_
#define _ALIL3DUMMYLOAD_HPP_ 

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTStoreForwarder.hpp 1432 2006-11-01 13:53:05Z timm $ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliHLTProcessingSubscriber.hpp"

class AliHLTStoreForwarderSubscriber: public AliHLTProcessingSubscriber
    {
    public:

	AliHLTStoreForwarderSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize, 
					AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, int eventCountAlloc = -1 );

	virtual bool ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* etsp,
				   AliUInt8_t* outputPtr, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, AliHLTEventDoneData*& edd );

    protected:


    private:
    };






/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTStoreForwarder.hpp 1432 2006-11-01 13:53:05Z timm $ 
**
***************************************************************************
*/

#endif // _ALIL3DUMMYLOAD_HPP_
