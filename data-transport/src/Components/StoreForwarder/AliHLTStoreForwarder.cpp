/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTStoreForwarder.cpp 1534 2007-01-30 14:39:54Z timm $ 
**
***************************************************************************
*/

//#define DEBUG

#include "AliHLTStoreForwarder.hpp"
#include "AliHLTLog.hpp"
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#ifdef DEBUG
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#endif



AliHLTStoreForwarderSubscriber::AliHLTStoreForwarderSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize,
								AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, int eventCountAlloc ):
    AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc )
    {
    }

bool AliHLTStoreForwarderSubscriber::ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, const AliHLTSubEventDataDescriptor*, const AliHLTEventTriggerStruct*,
				   AliUInt8_t* outputPtr, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, AliHLTEventDoneData*& )
    {
    AliUInt8_t *dataIn=NULL, *dataOut=NULL, *ptrOut=NULL, *dataOutEnd=NULL;
    AliUInt32_t dataInSize;
    AliUInt32_t ndx;
    AliUInt32_t outputSize = 0;

    dataOut = outputPtr;
    dataOutEnd = outputPtr+size;
    ptrOut = dataOut;

    AliHLTSubEventDescriptor::BlockData dataBlock;
    for ( ndx = 0; ndx < blockCnt; ndx++ )
	{
	  if ( outputSize+blocks[ndx].fSize > size )
	    {
	      LOG( AliHLTLog::kWarning, "AliHLTStoreForwarder::ProcessEvent", "Too much output" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") block " << ndx << " would create output of " << AliHLTLog::kDec << outputSize+blocks[ndx].fSize 
		<< " bytes while only " << size << " bytes are allowed." << ENDLOG;
	      continue;
	    }
	dataIn = blocks[ndx].fData;
	dataInSize = blocks[ndx].fSize;

	dataBlock = blocks[ndx];
	dataBlock.fOffset = outputSize;
	dataBlock.fShmKey.fShmType = kInvalidShmType;
	dataBlock.fShmKey.fKey.fID = ~(AliUInt64_t)0;

	memcpy( ptrOut, dataIn, dataInSize );
	outputBlocks.insert( outputBlocks.end(), dataBlock );
	outputSize += dataBlock.fSize;
	ptrOut += dataBlock.fSize;

	}
    size = outputSize;
    return true;
    }


/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTStoreForwarder.cpp 1534 2007-01-30 14:39:54Z timm $ 
**
***************************************************************************
*/
