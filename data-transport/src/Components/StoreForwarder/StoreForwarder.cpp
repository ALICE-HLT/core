/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: StoreForwarder.cpp 905 2006-04-10 11:12:18Z timm $ 
**
***************************************************************************
*/

#include "AliHLTStoreForwarder.hpp"
//#include "AliHLTControlledProcessComponent.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTLog.hpp"


class StoreForwarderComponent: public AliHLTControlledProcessorComponent
    {
    public:

	StoreForwarderComponent( const char* name, int argc, char** argv );
	virtual ~StoreForwarderComponent() {};


    protected:

	virtual AliHLTProcessingSubscriber* CreateSubscriber();

    private:
    };


StoreForwarderComponent::StoreForwarderComponent( const char* name, int argc, char** argv ):
    AliHLTControlledProcessorComponent( name, argc, argv )
    {
    }


AliHLTProcessingSubscriber* StoreForwarderComponent::CreateSubscriber()
    {
    return new AliHLTStoreForwarderSubscriber( fSubscriberID.c_str(), fSendEventDone, fMinBlockSize, 
					       fPerEventFixedSize, fPerBlockFixedSize, fSizeFactor, fMaxPreEventCountExp2 );
    }


int main( int argc, char** argv )
    {
    StoreForwarderComponent procComp( "StoreForwarder", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }



/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: StoreForwarder.cpp 905 2006-04-10 11:12:18Z timm $ 
**
***************************************************************************
*/

