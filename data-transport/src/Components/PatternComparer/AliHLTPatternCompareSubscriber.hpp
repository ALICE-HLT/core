/**
 * @file AliHLTPatternCompareSubscriber.hpp
 * @author Heiko Engel <hengel@cern.ch>
 * @version 0.1
 * @date 2014-07-15
 *
 * @section LICENSE
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * */
#ifndef _ALIHLTPATTERNCOMPARESUBSCRIBER_HPP_
#define _ALIHLTPATTERNCOMPARESUBSCRIBER_HPP_

#include "AliHLTProcessingSubscriber.hpp"

class AliHLTPatternCompareSubscriber : public AliHLTProcessingSubscriber {
public:
  AliHLTPatternCompareSubscriber(const char *name, bool sendEventDone,
                                 AliUInt32_t minBlockSize,
                                 AliUInt32_t perEventFixedSize,
                                 AliUInt32_t perBlockFixedSize,
                                 double sizeFactor, int eventCountAlloc = -1);

  virtual ~AliHLTPatternCompareSubscriber();

  virtual void StartProcessing() {
    AliHLTProcessingSubscriber::StartProcessing();
  }

  virtual void StopProcessing() {
    AliHLTProcessingSubscriber::StopProcessing();
  }

  void setReferenceFileMap(uint32_t *FileMapping, size_t size);
  void unsetReferenceFileMap();

protected:
  virtual bool
  ProcessEvent(AliEventID_t eventID, AliUInt32_t blockCnt,
               AliHLTSubEventDescriptor::BlockData *blocks,
               const AliHLTSubEventDataDescriptor *sedd,
               const AliHLTEventTriggerStruct *etsp, AliUInt8_t *outputPtr,
               AliUInt32_t &size,
               vector<AliHLTSubEventDescriptor::BlockData> &outputBlocks,
               AliHLTEventDoneData *&edd);

  bool fCompareAgainstFile;
  uint32_t *fFileMapping;
  size_t fFileSize;
};

#endif // _ALIHLTPATTERNCOMPARESUBSCRIBER_HPP_
