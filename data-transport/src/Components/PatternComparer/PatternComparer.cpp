/**
 * @file PatternComparer.cpp
 * @author Heiko Engel <hengel@cern.ch>
 * @version 0.1
 * @date 2014-07-14
 *
 * @section LICENSE
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * */

#include "AliHLTPatternCompareSubscriber.hpp"
#include "AliHLTControlledComponent.hpp"
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>

class PatternComparerComponent : public AliHLTControlledDefaultSinkComponent<
                                     AliHLTPatternCompareSubscriber> {
public:
  PatternComparerComponent(const char *name, int argc, char **argv);

  virtual ~PatternComparerComponent(){};

protected:
  virtual void ShowConfiguration();
  virtual bool CheckConfiguration();

  virtual const char *ParseCmdLine(int argc, char **argv, int &i,
                                   int &errorArg);

  virtual void PrintUsage(const char *errorStr, int errorArg, int errorParam);

  virtual bool CreateParts();
  virtual void DestroyParts();
  virtual bool SetupComponents();

  char *fFilename;
  bool fFilenameSet;
  uint32_t *fFileMapping;
  size_t fFileSize;

private:
};

PatternComparerComponent::PatternComparerComponent(const char *name, int argc,
                                                   char **argv)
    : AliHLTControlledDefaultSinkComponent<AliHLTPatternCompareSubscriber>(
          name, argc, argv) {
  fFilenameSet = false;
  fFilename = NULL;
  fFileMapping = NULL;
  fFileSize = 0;
}

void PatternComparerComponent::ShowConfiguration() {
  if (fFilenameSet) {
    LOG(AliHLTLog::kInformational,
        "PatternComparerComponent::ShowConfiguration", "Filename")
        << "Input file name: '" << fFilename << "'." << ENDLOG;
  }
}

bool PatternComparerComponent::CheckConfiguration() {
  if (!AliHLTControlledComponent::CheckConfiguration()) {
    return false;
  }
  return true;
}

const char *PatternComparerComponent::ParseCmdLine(int argc, char **argv,
                                                   int &i, int &errorArg) {
  if (!strcmp(argv[i], "-file")) {
    if (argc <= i + 1) {
      return "Missing input file specifier.";
    }
    fFilename = argv[i + 1];
    fFilenameSet = true;
    i += 2;
    return NULL;
  }
  return AliHLTControlledDefaultSinkComponent<
      AliHLTPatternCompareSubscriber>::ParseCmdLine(argc, argv, i, errorArg);
}

void PatternComparerComponent::PrintUsage(const char *errorStr, int errorArg,
                                          int errorParam) {
  AliHLTControlledDefaultSinkComponent<
      AliHLTPatternCompareSubscriber>::PrintUsage(errorStr, errorArg,
                                                  errorParam);
  LOG(AliHLTLog::kError, "Processing Component", "Command line usage")
      << "-file: Specify a DDL file to be checked against event data (Optional)"
      << ENDLOG;
}

bool PatternComparerComponent::CreateParts() {
  if (!AliHLTControlledComponent::CreateParts()) {
    return false;
  }

  return true;
}

void PatternComparerComponent::DestroyParts() {
  AliHLTControlledComponent::DestroyParts();

  dynamic_cast<AliHLTPatternCompareSubscriber*>(fSubscriber)->unsetReferenceFileMap();
  if (fFileMapping) {
    munmap(fFileMapping, fFileSize);
  }
}

bool PatternComparerComponent::SetupComponents() {
  if (!AliHLTControlledComponent::SetupComponents()) {
    return false;
  }

  if (fFilenameSet) {
    int fd = open(fFilename, O_RDONLY);
    if (fd == -1) {
      LOG(AliHLTLog::kError, "PatternComparerComponent::SetupComponents()",
          "Open File")
          << "Failed to open reference file " << fFilename << ENDLOG;
      return false;
    }
    struct stat file_stat;
    if (fstat(fd, &file_stat) == -1) {
      LOG(AliHLTLog::kError, "PatternComparerComponent::SetupComponents()",
          "Stat File")
          << "Failed to stat reference file " << fFilename << ENDLOG;
      return false;
    }
    fFileSize = file_stat.st_size;

    fFileMapping =
        (uint32_t *)mmap(NULL, fFileSize, PROT_READ, MAP_SHARED, fd, 0);
    if (fFileMapping == MAP_FAILED) {
      LOG(AliHLTLog::kError, "PatternComparerComponent::SetupComponents()",
          "mmap File")
          << "Failed to mmap reference file " << fFilename << ENDLOG;
      return false;
    }

    dynamic_cast<AliHLTPatternCompareSubscriber*>(fSubscriber)
        ->setReferenceFileMap(fFileMapping, fFileSize);
  }

  return true;
}

int main(int argc, char **argv) {
  PatternComparerComponent procComp("PatternComparer", argc, argv);
  procComp.Run();
  return 0;
}
