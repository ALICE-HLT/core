/**
 * @file AliHLTPatternCompareSubscriber.cpp
 * @author Heiko Engel <hengel@cern.ch>
 * @version 0.1
 * @date 2014-07-15
 *
 * @section LICENSE
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * */

#include "AliHLTPatternCompareSubscriber.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTEventWriter.hpp"

AliHLTPatternCompareSubscriber::AliHLTPatternCompareSubscriber(
    const char *name, bool sendEventDone, AliUInt32_t minBlockSize,
    AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize,
    double sizeFactor, int eventCountAlloc)
    : AliHLTProcessingSubscriber(name, sendEventDone, minBlockSize,
                                 perEventFixedSize, perBlockFixedSize,
                                 sizeFactor, eventCountAlloc) {
  fCompareAgainstFile = false;
  fFileMapping = NULL;
  fFileSize = 0;
}

AliHLTPatternCompareSubscriber::~AliHLTPatternCompareSubscriber() {}

void AliHLTPatternCompareSubscriber::setReferenceFileMap(uint32_t *FileMapping,
                                                         size_t size) {
  fFileMapping = FileMapping;
  fFileSize = size;
  fCompareAgainstFile = true;
}

void AliHLTPatternCompareSubscriber::unsetReferenceFileMap() {
  fCompareAgainstFile = false;
  fFileMapping = NULL;
  fFileSize = 0;
}

bool AliHLTPatternCompareSubscriber::ProcessEvent(
    AliEventID_t eventID, AliUInt32_t blockCnt,
    AliHLTSubEventDescriptor::BlockData *blocks,
    const AliHLTSubEventDataDescriptor *sedd,
    const AliHLTEventTriggerStruct *ets, AliUInt8_t *, AliUInt32_t &size,
    vector<AliHLTSubEventDescriptor::BlockData> &, AliHLTEventDoneData *&) {
  LOG(AliHLTLog::kDebug, "AliHLTPatternCompareSubscriber::ProcessEvent",
      "Event Received")
      << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
      << eventID << ") " << blockCnt << " Blocks, size=" << size << ENDLOG;

  size = 0;

  if (eventID.fType != kAliEventTypeStartOfRun &&
      eventID.fType != kAliEventTypeData &&
      eventID.fType != kAliEventTypeSync &&
      eventID.fType != kAliEventTypeEndOfRun) {
    LOG(AliHLTLog::kInformational,
        "AliHLTPatternCompareSubscriber::ProcessEvent",
        "Non-(SOR/Data/EOR/SYNC) Event")
        << "Non-(SOR/Data/EOR/SYNC) Event 0x" << AliHLTLog::kHex << eventID << " ("
        << AliHLTLog::kDec << eventID << ")." << ENDLOG;
    return true;
  }

  for (uint32_t n = 0; n < blockCnt; n++) {
    AliHLTSubEventDescriptor::BlockData outputBlock = blocks[n];
    uint32_t *eventData = (uint32_t *)outputBlock.fData;

    if (!fCompareAgainstFile) {
      // check against static pattern
      for (uint32_t iter = 8; iter < (outputBlock.fSize >> 2); iter++) {
        if (eventData[iter] != (iter - 8)) {
          LOG(AliHLTLog::kError, "AliHLTPatternCompareSubscriber::ProcessEvent",
              "Check pattern")
              << "Pattern check failed: expected " << AliHLTLog::kHex
              << (iter - 8) << " received " << eventData[iter]
              << " for iter=" << AliHLTLog::kDec << iter
              << " block size=" << outputBlock.fSize << " offset=0x"
              << AliHLTLog::kHex << outputBlock.fOffset << " bufferId=0x"
              << AliHLTLog::kHex << outputBlock.fShmKey.fKey.fID << ENDLOG;
          AliHLTEventWriter::WriteEvent(eventID, blockCnt, blocks, sedd, ets, 0,
                                        "PatternComparer");
          break;
        }
      }
    } else {
      // check against file
      if (outputBlock.fSize != fFileSize) {
        LOG(AliHLTLog::kError, "AliHLTPatternCompareSubscriber::ProcessEvent",
            "Check Size")
            << "Event Size (" << outputBlock.fSize << ") and File Size ("
            << fFileSize << ") do not match." << ENDLOG;
      } else if (memcmp(eventData, fFileMapping, fFileSize) != 0) {
        LOG(AliHLTLog::kError, "AliHLTPatternCompareSubscriber::ProcessEvent",
            "Check File")
            << "Event does not match File." << ENDLOG;
      }
    }
  }
  return true;
}
