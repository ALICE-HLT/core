/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: EventDoneDataProducer.cpp 912 2006-04-10 11:14:35Z timm $ 
**
***************************************************************************
*/

#include "AliHLTProcessingSubscriber.hpp"
//#include "AliHLTControlledProcessComponent.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "AliHLTEventDoneData.hpp"
#include <errno.h>
#include <vector>


class AliHLTEventDoneDataProducerSubscriber: public AliHLTProcessingSubscriber
    {
    public:

	AliHLTEventDoneDataProducerSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize, 
					  AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, 
					  std::vector< std::vector<AliUInt32_t> > eventDoneDataList, 
					  int eventCountAlloc = -1 );
	
	virtual bool ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* etsp,
				   AliUInt8_t* outputPtr, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, AliHLTEventDoneData*& edd );
    protected:

	unsigned long long fEventCnt;

	std::vector< std::vector<AliUInt32_t> > fEventDoneDataList;

    private:
    };




AliHLTEventDoneDataProducerSubscriber::AliHLTEventDoneDataProducerSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize,
								    AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, 
								    std::vector< std::vector<AliUInt32_t> > eventDoneDataList, 
								    int eventCountAlloc ):
    AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc ),
    fEventCnt( 0ULL ),
    fEventDoneDataList( eventDoneDataList )
    {
    }

// void AliHLTEventDoneDataProducerSubscriber::ProcessEvent( AliUInt8_t* dataIn, AliUInt32_t dataInSize,
// 				   AliUInt8_t* dataOut, AliUInt32_t dataOutMaxSize )
bool AliHLTEventDoneDataProducerSubscriber::ProcessEvent( AliEventID_t eventID, AliUInt32_t, AliHLTSubEventDescriptor::BlockData*, const AliHLTSubEventDataDescriptor*, const AliHLTEventTriggerStruct*,
				   AliUInt8_t* outputPtr, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, AliHLTEventDoneData*& edd )
    {
#if 0
    // For testing specific behaviours, normally not needed
    if ( eventID.fType != kAliEventTypeData )
	{
	size = 0;
	edd = NULL;
	return true;
	}
#endif
    unsigned long totalSize = 0;
    unsigned long ndx = fEventCnt % fEventDoneDataList.size();

    std::vector<AliUInt32_t>& eddl( fEventDoneDataList[ndx] );
    unsigned long cnt = eddl.size();

    edd = GetEventDoneData( eventID, cnt );

    if ( !edd )
	{
	LOG( AliHLTLog::kError, "AliHLTEventDoneDataProducerSubscriber::ProcessEvent", "Cannot create EventDoneData" )
	    << "Unable to create event done data of " << AliHLTLog::kDec << cnt
	    << " 32 bit words." << ENDLOG;
	return false;
	}
    for ( unsigned long ii=0; ii<cnt; ii++ )
	{
	edd->fDataWords[ii] = eddl[ii];
	}
    edd->fDataWordCount = cnt;

    AliUInt32_t* data = (AliUInt32_t*)outputPtr;
    unsigned long n=0;
    if ( totalSize+sizeof(AliUInt32_t)<=size )
	{
	data[0] = cnt;
	totalSize += sizeof(AliUInt32_t);
	}
    while ( totalSize+sizeof(AliUInt32_t)<=size && n<cnt )
	{
	data[1+n] = edd->fDataWords[n];
	++n;
	totalSize += sizeof(AliUInt32_t);
	}

    AliHLTSubEventDescriptor::BlockData dataBlock;
    dataBlock.fShmKey.fShmType = kInvalidShmType;
    dataBlock.fShmKey.fKey.fID = ~(AliUInt64_t)0;
    dataBlock.fOffset = 0;
    dataBlock.fSize = totalSize;
    dataBlock.fDataType.fID = EVENTDONEDATA_DATAID;
    dataBlock.fDataOrigin.fID = HLT_DATAORIGIN;
    dataBlock.fDataSpecification = 0;
    dataBlock.fStatusFlags = 0;
    outputBlocks.insert( outputBlocks.end(), dataBlock );


    size = totalSize;
    fEventCnt++;
    return true;
    }




class EventDoneDataProducerComponent: public AliHLTControlledProcessorComponent
    {
    public:

	EventDoneDataProducerComponent( const char* name, int argc, char** argv );
	virtual ~EventDoneDataProducerComponent() {};


    protected:

	virtual bool CheckConfiguration();
	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

	virtual AliHLTProcessingSubscriber* CreateSubscriber();

	std::vector< std::vector<AliUInt32_t> > fEventDoneDataList;

	std::vector<AliUInt32_t> fEventDoneData;

    private:
    };


EventDoneDataProducerComponent::EventDoneDataProducerComponent( const char* name, int argc, char** argv ):
    AliHLTControlledProcessorComponent( name, argc, argv )
    {
    }

bool EventDoneDataProducerComponent::CheckConfiguration()
    {
    if ( !AliHLTControlledProcessorComponent::CheckConfiguration() )
	return false;
    if ( fEventDoneDataList.size()<=0 )
	{
	LOG( AliHLTLog::kError, "EventDoneDataProducerComponent::CheckConfiguration", "No readout list" )
	    << "Must specify at least one event done data list to be used (e.g. assembled through the -word options, and finalised with the -addlist option."
	    << ENDLOG;
	return false;
	}
    if ( fEventDoneData.size()>0 )
	{
	LOG( AliHLTLog::kError, "EventDoneDataProducerComponent::CheckConfiguration", "Readout list started" )
	    << "An event done data list was started to be assembled but not finalised with the -addlist option."
	    << ENDLOG;
	return false;
	}
    return true;
    }	


const char* EventDoneDataProducerComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cpErr = NULL;
    if ( !strcmp( argv[i], "-addword" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing event done data word specifier.";
	    }
	AliUInt32_t word = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr != '\0' )
	    {
	    return "Error parsing event done data word specifier.";
	    }
	fEventDoneData.push_back( word );
	i += 2;
	return  NULL;
	}
    if ( !strcmp( argv[i], "-addtextword" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing event done data text word specifier.";
	    }
	AliUInt32_t word = 0;
	unsigned int ii=0;
	while ( argv[i+1][ii]!='\0' && ii<4 )
	    {
	    word = (word <<8 ) | (AliUInt32_t)(argv[i+1][ii]);
	    ii++;
	    }
	if ( argv[i+1][ii] != '\0' )
	    {
	    return "Event done data text word specifier too long (max. 4 characters).";
	    }
	while ( ii<4 )
	    {
	    word = (word <<8 ) | (AliUInt32_t)('\0');
	    ii++;
	    }
	fEventDoneData.push_back( word );
	i += 2;
	return  NULL;
	}
    if ( !strcmp( argv[i], "-addlist" ) )
	{
	fEventDoneDataList.push_back( fEventDoneData );
	fEventDoneData.clear();
	i += 1;
	return NULL;
	}
    return AliHLTControlledProcessorComponent::ParseCmdLine( argc, argv, i, errorArg );
    }

void EventDoneDataProducerComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    AliHLTControlledProcessorComponent::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-addword <32 bit event done data word>: Add a 32 bit word to the current event done data list. (Optional, can be given multiple times) " << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-addtextword <integer character constant (<=4 characters)>: Add a 32 bit word to the current event done data list represented as a 4 (or less) byte character string. (Optional, can be given multiple times) " << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-addlist: Add the event done data list configured so far to the list of event done data lists. (Mandatory, can be given multiple times) " << ENDLOG;
    }

AliHLTProcessingSubscriber* EventDoneDataProducerComponent::CreateSubscriber()
    {
    return new AliHLTEventDoneDataProducerSubscriber( fSubscriberID.c_str(), fSendEventDone, fMinBlockSize, 
						      fPerEventFixedSize, fPerBlockFixedSize, fSizeFactor, 
						      fEventDoneDataList, fMaxPreEventCountExp2 );
    }


int main( int argc, char** argv )
    {
    EventDoneDataProducerComponent procComp( "EventDoneDataProducer", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }


/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: EventDoneDataProducer.cpp 912 2006-04-10 11:14:35Z timm $ 
**
***************************************************************************
*/

