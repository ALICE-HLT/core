/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/
/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliRootWrapperSubscriber.cpp,v 1.2 2005/02/09 13:47:49 timm Exp $ 
**
***************************************************************************
*/

#include "AliHLTProcessingSubscriber.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTHLTEventTriggerData.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTDataTypes.h"
#include "AliRootInterfaceLibrary.hpp"
#include "MLUCCmdlineParser.hpp"
#include "MLUCDynamicAllocCache.hpp"
#include "MLUCDynamicLibrary.hpp"
#include "MLUCRegEx.hpp"
#include <vector>
#include <cerrno>
#include <iostream>


int gAnalysisLogLevel = 0;




class AliRootWrapperSubscriber: public AliHLTProcessingSubscriber
    {
    public:
	AliRootWrapperSubscriber( const char* name, bool sendEventDone,
				  int eventCountAlloc = -1 );
	virtual ~AliRootWrapperSubscriber();

	void SetAliRootInterface( AliRootInterface* interface )
		{
		fAliRootInterface = interface;
		}
	
	void SetComponentID( const MLUCString& compID )
		{
		fComponentID = compID;
		}

	void SetComponentDataType( const AliHLTComponentDataType& dataType )
		{
		fComponentDataType = dataType;
		}

	void ForwardCorruptBlocks( bool forward )
		{
		fForwardCorruptBlocks = forward;
		}

	void SetComponentInfo( const char* info )
		{
		fComponentInfo = info;
		}


    protected:
	
	virtual bool ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, 
				   const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* etsp,
				   AliUInt8_t* outputPtr, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, AliHLTEventDoneData*& edd );

	static void* AllocMemory( void* param, unsigned long size );
	static void FreeMemory( void* param, void* ptr );
// 	static int MakeOutputDataBlockList( void* param, const vector<AliHLTComponentBlockData>& blocks, AliHLTUInt32_t* blockCount, AliHLTComponentBlockData** outputBlocks );
	static int GetEventDoneData( void* param, AliHLTEventID_t eventID, unsigned long size, AliHLTComponentEventDoneData** edd );
	static int Logging( void* param, AliHLTComponentLogSeverity severity, const char* origin, const char* keyword, const char* message );
	static const char* GetComponentInfo( void* param );

	    friend class AliRootWrapperSubscriberComponent;

	MLUCString fComponentID;

	MLUCDynamicAllocCache fAllocCache;

	AliHLTComponentEventData fCompEventData;

	AliHLTComponentTriggerData fCompTriggerData;

	AliHLTComponentDataType fComponentDataType;

	bool fForwardCorruptBlocks;

	MLUCString fComponentInfo;

	AliRootInterface* fAliRootInterface;


    private:

    };

AliRootWrapperSubscriber::AliRootWrapperSubscriber( const char* name, bool sendEventDone,
						    int eventCountAlloc ):
    AliHLTProcessingSubscriber( name, sendEventDone, 0, 0, 0.0, eventCountAlloc ),
    fAllocCache( 8, true, false ),
    fForwardCorruptBlocks( false ),
    fAliRootInterface( NULL )
    {
    fCompEventData.fStructSize = sizeof(AliHLTComponentEventData);
    fCompTriggerData.fStructSize = sizeof(AliHLTComponentTriggerData);
    }

AliRootWrapperSubscriber::~AliRootWrapperSubscriber()
    {
    }

bool AliRootWrapperSubscriber::ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, 
					     AliHLTSubEventDescriptor::BlockData* blocks, 
					     const AliHLTSubEventDataDescriptor* sedd, 
					     const AliHLTEventTriggerStruct* ets,
					     AliUInt8_t* outputPtr, AliUInt32_t& size, 
					     vector<AliHLTSubEventDescriptor::BlockData>& outBlocks, 
					     AliHLTEventDoneData*& edd )
    {
    fCompEventData.fEventID = (AliHLTEventID_t)eventID;
    fCompEventData.fEventCreation_s = sedd->fEventBirth_s;
    fCompEventData.fEventCreation_us = sedd->fEventBirth_us;
    fCompEventData.fBlockCnt = blockCnt;
    AliHLTComponentBlockData* bds = NULL;
    bds = reinterpret_cast<AliHLTComponentBlockData*>( fAllocCache.Get( sizeof(AliHLTComponentBlockData)*(blockCnt+1) ) );
    if ( !bds )
	{
	LOG( AliHLTLog::kError, "AliRootWrapperSubscriber::ProcessEvent", "Out of memory" )
	    << "Out of memory allocating block data for " << AliHLTLog::kDec << blockCnt << " blocks of "
	    << sizeof(AliHLTComponentBlockData)*blockCnt << " bytes." << ENDLOG;
	return false;
	}
    unsigned long totalSize=0;
    unsigned long corruptCnt = 0;
    unsigned long goodBlocks = 0;
    bds[goodBlocks].fStructSize = sizeof(AliHLTComponentBlockData);
    bds[goodBlocks].fShmKey.fStructSize = sizeof(AliHLTComponentShmData);
    bds[goodBlocks].fShmKey.fShmType = gkAliHLTComponentInvalidShmType;
    bds[goodBlocks].fShmKey.fShmID = gkAliHLTComponentInvalidShmID;
    bds[goodBlocks].fOffset = 0;
    bds[goodBlocks].fPtr = (void*)0;
    bds[goodBlocks].fSize = 0;
    AliUInt64_t dataType = EVENTTYPE_DATAID;
    for ( unsigned b = 0; b < 8; b++ )
	bds[goodBlocks].fDataType.fID[7-b] = ((char*)(&dataType))[b];
    //memcpy( bds[goodBlocks].fDataType.fID, &dataType, 8 );
    AliUInt32_t dataOrigin = PRIV_DATAORIGIN;
    for ( unsigned b = 0; b < 4; b++ )
	bds[goodBlocks].fDataType.fOrigin[3-b] = ((char*)(&dataOrigin))[b];
    //memcpy( bds[goodBlocks].fDataType.fOrigin, &dataOrigin, 4 );
    bds[goodBlocks].fSpecification = eventID.fType;
    goodBlocks++;

    bool abort = false;
#ifdef RECONFIGURE_EVENT_FILTER // Deactivated, reconfigure events are passed into components unconditionally, components have to decide, whether it applies to them.
    if ( eventID.fType == kAliEventTypeReconfigure )
	abort = true;
#endif

    
    for ( unsigned long n = 0; n < blockCnt; n++ )
	{
	if ( (blocks[n].fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_DATA_CORRUPT) && !fForwardCorruptBlocks )
	    {
	    corruptCnt++;
	    continue;
	    }
#ifdef RECONFIGURE_EVENT_FILTER // Deactivated, reconfigure events are passed into components unconditionally, components have to decide, whether it applies to them.
	if ( eventID.fType == kAliEventTypeReconfigure )
	    {
	    if ( blocks[n].fDataType.fID==COMPONENTCONFIGIDS_DATAID || blocks[n].fDataType.fID==COMPONENTCONFIGCOMPS_DATAID )
		{
		char const* blockType;
		switch ( blocks[n].fDataType.fID )
		    {
		    case COMPONENTCONFIGIDS_DATAID:
			blockType = "ID";
			break;
		    case COMPONENTCONFIGCOMPS_DATAID:
			blockType = "Component";
			break;
		    default:
			blockType = "UNKNOWN";
			break;
		    }
		if ( ((char*)blocks[n].fData)[blocks[n].fSize-1]!='\0' )
		    {
		    LOG( AliHLTLog::kWarning, "AliRootWrapperSubscriber::ProcessEvent", "Reconfiguration event ID block inconsistency" )
			<< "Reconfiguration event 0x" << AliHLTLog::kHex << eventID
			<< " (" << AliHLTLog::kDec << eventID << ") "
			<< blockType << " block (block nr "
			<< n << ") has inconsistent format."
			<< ENDLOG;
		    continue;
		    }
		vector<MLUCString> tokens;
		MLUCString dataStr( (char*)blocks[n].fData );
		dataStr.Split( tokens, ' ' );
		vector<MLUCString>::iterator iter, end;
		iter = tokens.begin();
		end = tokens.end();
		while ( iter != end )
		    {
		    MLUCRegEx regEx;
		    regEx.SetPattern( iter->c_str(), MLUCRegEx::kPFExtended );
		    if ( blocks[n].fDataType.fID==COMPONENTCONFIGIDS_DATAID && regEx.Search( GetName() ) )
			abort = false;
		    if ( blocks[n].fDataType.fID==COMPONENTCONFIGCOMPS_DATAID && regEx.Search( fComponentID.c_str() ) )
			abort = false;
		    iter++;
		    }
		continue;
		}
	    }
#endif
	bds[goodBlocks].fStructSize = sizeof(AliHLTComponentBlockData);
	bds[goodBlocks].fShmKey.fStructSize = sizeof(AliHLTComponentShmData);
	bds[goodBlocks].fShmKey.fShmType = blocks[n].fShmKey.fShmType;
	bds[goodBlocks].fShmKey.fShmID = blocks[n].fShmKey.fKey.fID;
	bds[goodBlocks].fOffset = blocks[n].fOffset;
	bds[goodBlocks].fPtr = (void*)blocks[n].fData;
	bds[goodBlocks].fSize = blocks[n].fSize;
	totalSize += blocks[n].fSize;
	bds[goodBlocks].fDataType.fStructSize = sizeof(AliHLTComponentDataType);
// 	memcpy( bds[n].fDataType.fID, blocks[n].fDataType.fDescr, 8 );
	for ( unsigned b = 0; b < 8; b++ )
	    bds[goodBlocks].fDataType.fID[7-b] = blocks[n].fDataType.fDescr[b];
// 	memcpy( bds[n].fDataType.fOrigin, blocks[n].fDataOrigin.fDescr, 4 );
	for ( unsigned b = 0; b < 4; b++ )
	    bds[goodBlocks].fDataType.fOrigin[3-b] = blocks[n].fDataOrigin.fDescr[b];
	bds[goodBlocks].fSpecification = blocks[n].fDataSpecification;
	goodBlocks++;
	}

    if ( abort )
	{
	LOG( AliHLTLog::kInformational, "AliRootWrapperSubscriber::ProcessEvent", "Aborting event" )
	    << "Aborting event 0x" << AliHLTLog::kHex << eventID
	    << " (" << AliHLTLog::kDec << eventID << ") as it does not apply to this component."
	    << ENDLOG;
	outBlocks.clear();
	size = 0;
	return true;
	}

    fCompEventData.fBlockCnt = goodBlocks;
    unsigned long constBasePerEvent=0;
    unsigned long constBasePerBlock=0;
    double inputMultiplier=0.0;
    int ret;
    ret = fAliRootInterface->GetOutputDataSize( constBasePerEvent, constBasePerBlock, inputMultiplier );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliRootWrapperSubscriber::ProcessEvent", "Component output size" )
	    << "Component '" << fComponentID.c_str() << "' AliRoot Interface GetOutputSize returned error for event 0x"
	    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << "): "
	    << strerror(ret) << " (" << ret << ")." << ENDLOG;
	//return false;
	}
    else
	{
	unsigned long totalOutSize = constBasePerEvent+constBasePerBlock*(fCompEventData.fBlockCnt-1)+(unsigned long)(totalSize*inputMultiplier); // fCompEventData.fBlockCnt-1 to take into account dummy block descriptor with event type added above, no real data block...
	LOG( AliHLTLog::kDebug, "AliRootWrapperSubscriber::ProcessEvent", "Block sizes" )
	    << "Block size: " << AliHLTLog::kDec << size << " - projected block size: " << totalOutSize
	    << " - per event fixed size: " << constBasePerEvent
	    << " - per block fixed size: " << constBasePerBlock << " - size factor: " << inputMultiplier
	    << " - block count: " << fCompEventData.fBlockCnt << " - total input data size: " << totalSize
	    << ENDLOG;
	if ( totalOutSize > size )
	    {
	    LOG( AliHLTLog::kDebug, "AliRootWrapperSubscriber::ProcessEvent", "Component output maybe too large" )
		<< "Output data size of " << AliHLTLog::kDec << totalOutSize << " bytes projected by component '" 
		<< fComponentID.c_str() << "' might be too large for available buffer (" << size << " bytes)." << ENDLOG;
	    //return false;
	    }
	}
    AliHLTEventTriggerData etd;
    fCompTriggerData.fDataSize = sizeof(etd);
    fCompTriggerData.fData = &etd;
    bool reset=false;
    const char* resetReason="";
    if ( eventID.fType==kAliEventTypeStartOfRun || eventID.fType==kAliEventTypeData || eventID.fType==kAliEventTypeEndOfRun || eventID.fType==kAliEventTypeSync )
	{
	if ( ets->fHeader.fLength != sizeof(AliHLTHLTEventTriggerData) )
	    {
	    LOG( AliHLTLog::kWarning, "AliRootWrapperSubscriber::ProcessEvent", "Trigger Data" )
		<< "Trigger data size mismatch for event 0x"
		<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec 
		<< eventID << "). Present : " << AliHLTLog::kDec
		<< ets->fHeader.fLength << " - Expected: "
		<< sizeof(AliHLTHLTEventTriggerData) << ". - Assuming empty trigger data..." << ENDLOG;
	    reset = true;
	    }
	else
	    {
	    if ( gkAliHLTBlockDAttributeCount!=kAliHLTBlockDAttributeCount )
		{
		reset = true;
		resetReason = " (mismatch in Block Attribute Count).";
		}
	    else
		memcpy( etd.fAttributes, ((AliHLTHLTEventTriggerData*)ets)->fAttributes, sizeof(etd.fAttributes) );
	    etd.fHLTStatus = ((AliHLTHLTEventTriggerData*)ets)->fHLTStatus;
	    etd.fCommonHeaderWordCnt = ((AliHLTHLTEventTriggerData*)ets)->fCommonHeaderWordCnt;
	    if ( (unsigned)gkAliHLTCommonHeaderCount != ((AliHLTHLTEventTriggerData*)ets)->fCommonHeaderWordCnt )
		{
		reset = true;
		resetReason = " (Mismatch in trigger header word count).";
		}
	    else
		memcpy( etd.fCommonHeader, ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader, sizeof(etd.fCommonHeader) );
	    etd.fReadoutList.fCount = ((AliHLTHLTEventTriggerData*)ets)->fReadoutList[0];
	    if ( (AliUInt32_t)gkAliHLTDDLListSize != ((AliHLTHLTEventTriggerData*)ets)->fReadoutList[0] )
		{
		unsigned interfaceReadoutListVersion = AliHLTReadoutList::GetReadoutlistVersionFromWordCount( gkAliHLTDDLListSize+1 );
#if 1
		if ( !AliHLTReadoutList::IsVersionSupported( interfaceReadoutListVersion ) )
		    {
		    LOG( AliHLTLog::kWarning, "AliRootWrapperSubscriber::ProcessEvent", "Mismatch in readout list word count" )
			<< "Mismatch in readout list word count - AliRoot interface version readout list size" << AliHLTLog::kDec
			<< gkAliHLTDDLListSize << " (" << gkAliHLTDDLListSize+1 << ") - interface readout list version determined "
			<< interfaceReadoutListVersion << "." << ENDLOG;
		    reset = true;
		    // resetReason = " (Mismatch in readout list word count (incompatible version found)).";
		    
		    }
		else
		    {
		    AliHLTReadoutListData interfaceReadoutListData;
		    AliHLTReadoutList interfaceReadoutList( interfaceReadoutListVersion, interfaceReadoutListData );
		    interfaceReadoutList.Reset();
		    if ( !interfaceReadoutList.Import( ((AliHLTHLTEventTriggerData*)ets)->fReadoutList, gReadoutListVersion ) )
			{
			reset = true;
			resetReason = " (Error importing mismatching readout list version).";
			}
		    else
			memcpy( etd.fReadoutList.fList, &(interfaceReadoutListData[1]), interfaceReadoutListData[0] );
		    }
#else
		    LOG( AliHLTLog::kWarning, "AliRootWrapperSubscriber::ProcessEvent", "Mismatch in readout list word count" )
			<< "Mismatch in readout list word count - AliRoot interface version readout list size" << AliHLTLog::kDec
			<< gkAliHLTDDLListSize << " (" << gkAliHLTDDLListSize+1 << ") - interface readout list version determined "
			<< interfaceReadoutListVersion << ". No attempt at recovery made" << ENDLOG;
		    reset = true;
#endif
		}
	    else
		memcpy( etd.fReadoutList.fList, ((AliHLTHLTEventTriggerData*)ets)->fReadoutList+1, ((AliHLTHLTEventTriggerData*)ets)->fReadoutList[0] );
	    }
	}
    else
	{
	reset = true;
	resetReason = " (Unexpected size of received event trigger data).";
	}
    if ( reset )
	{
	if ( eventID.fType==kAliEventTypeStartOfRun || eventID.fType==kAliEventTypeData || eventID.fType==kAliEventTypeEndOfRun || eventID.fType==kAliEventTypeSync )
	    {
	    LOG( AliHLTLog::kWarning, "AliRootWrapperSubscriber::ProcessEvent", "Trigger Data" )
	    << "Trigger data format error for event 0x"
	    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec 
	    << eventID << ")." << resetReason << " Resetting trigger data to empty state..." << ENDLOG;
	    }
	memset( etd.fAttributes, 0, gkAliHLTBlockDAttributeCount*sizeof(etd.fAttributes[0]) );
	
	etd.fHLTStatus = 0;
	etd.fCommonHeaderWordCnt = gkAliHLTCommonHeaderCount;
	memset( etd.fCommonHeader, 0, gkAliHLTCommonHeaderCount*sizeof(etd.fCommonHeader[0]) );
	etd.fReadoutList.fCount = gkAliHLTDDLListSize;
	memset( etd.fReadoutList.fList, 0, gkAliHLTDDLListSize*sizeof(etd.fReadoutList.fList[0]) );
	}
    AliHLTUInt32_t outputBlockCnt=0;
    AliHLTComponentBlockData* outputBlocks = NULL;
    AliHLTComponentEventDoneData* cedd = NULL;
    ret = fAliRootInterface->ProcessEvent( &fCompEventData, bds, 
					   &fCompTriggerData, outputPtr,
					   size, outputBlockCnt, 
					   outputBlocks,
					   cedd );
    LOG( AliHLTLog::kDebug, "AliRootWrapperSubscriber::ProcessEvent", "Component process event" )
	<< "size: " << AliHLTLog::kDec << size << " - outputBlockCnt: " << outputBlockCnt << "." << ENDLOG;
    outBlocks.clear();
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliRootWrapperSubscriber::ProcessEvent", "Component process event" )
	    << "Component '" << fComponentID.c_str() << "' AliRoot Interface ProcessEvent returned error for event 0x"
	    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << "): "
	    << strerror(ret) << " (" << ret << ")." << ENDLOG;
	}
    else
	{
	for ( unsigned long n = 0; n < outputBlockCnt; n++ )
	    {
	    AliHLTSubEventDescriptor::BlockData blk;
	    blk.fData =  (AliUInt8_t*)outputBlocks[n].fPtr;
	    blk.fShmKey.fShmType = outputBlocks[n].fShmKey.fShmType;
	    blk.fShmKey.fKey.fID = outputBlocks[n].fShmKey.fShmID;
	    blk.fOffset = outputBlocks[n].fOffset;
	    blk.fSize = outputBlocks[n].fSize;
	    blk.fStatusFlags = 0;
	    blk.fProducerNode = fNodeID;
	    bool unequal=false;
	    for ( unsigned b = 0; b < 8; b++ )
		{
		if ( outputBlocks[n].fDataType.fID[b] !='*' )
		    {
		    unequal=true;
		    break;
		    }
		}
	    for ( unsigned b = 0; b < 4; b++ )
		{
		if ( outputBlocks[n].fDataType.fOrigin[b] !='*' )
		    {
		    unequal=true;
		    break;
		    }
		}
	    if ( unequal )
		{
// 		memcpy( blk.fDataType.fDescr, outputBlocks[n].fDataType.fID, 8 );
		for ( unsigned b = 0; b < 8; b++ )
		    blk.fDataType.fDescr[7-b] = outputBlocks[n].fDataType.fID[b];
// 		memcpy( blk.fDataOrigin.fDescr, outputBlocks[n].fDataType.fOrigin, 4 );
		for ( unsigned b = 0; b < 4; b++ )
		    blk.fDataOrigin.fDescr[3-b] = outputBlocks[n].fDataType.fOrigin[b];
		}
	    else
		{
// 		memcpy( blk.fDataType.fDescr, fComponentDataType.fID, 8 );
		for ( unsigned b = 0; b < 8; b++ )
		    blk.fDataType.fDescr[7-b] = fComponentDataType.fID[b];
// 		memcpy( blk.fDataOrigin.fDescr, fComponentDataType.fOrigin, 4 );
		for ( unsigned b = 0; b < 4; b++ )
		    blk.fDataOrigin.fDescr[3-b] = fComponentDataType.fOrigin[b];
		}
	    blk.fDataSpecification = outputBlocks[n].fSpecification;
	    // XXX TODO Let the attributes be filled by AliRoot (AliHLTComponent???)
	    AliHLTSubEventDescriptor::FillBlockAttributes( blk.fAttributes );
	    outBlocks.push_back( blk );
	    }
	if ( cedd )
	    {
	    edd = AliHLTProcessingSubscriber::GetEventDoneData( eventID, (AliUInt32_t)( cedd->fDataSize/4 + ((cedd->fDataSize%4) ? 1 : 0) ) );
	    if ( !edd )
		{
		LOG( AliHLTLog::kError, "AliRootWrapperSubscriber::ProcessEvent", "Out of memory" )
		    << "Out of memory allocating event done data of "
		    << (cedd->fDataSize/4 + ((cedd->fDataSize%4) ? 1 : 0) ) << " words." << ENDLOG;
		}
	    else
	      memcpy( edd->fDataWords, cedd->fData, cedd->fDataSize );
	    }
	}
    if ( cedd )
	FreeMemory( (void*)this, cedd );
    if ( outputBlocks )
	FreeMemory( (void*)this, outputBlocks );
    if ( bds )
	fAllocCache.Release( reinterpret_cast<uint8*>( bds ) );
    return true;
    }

void* AliRootWrapperSubscriber::AllocMemory( void* param, unsigned long size )
    {
    // Note that param can be NULL if not called from actual component context (e.g system initialization log messages...
    if ( !param )
	return NULL;
    return (void*)reinterpret_cast<AliRootWrapperSubscriber*>( param )->fAllocCache.Get( size );
    }

void AliRootWrapperSubscriber::FreeMemory( void* param, void* ptr )
    {
    // Note that param can be NULL if not called from actual component context (e.g system initialization log messages...
    if ( param )
	reinterpret_cast<AliRootWrapperSubscriber*>( param )->fAllocCache.Release( (uint8*)ptr );
    }

// int AliRootWrapperSubscriber::MakeOutputDataBlockList( void* param, const vector<AliHLTComponentBlockData>& blocks, AliHLTUInt32_t* blockCount, AliHLTComponentBlockData** outputBlocks )
//     {
//     if ( !blockCount || !outputBlocks )
// 	return EFAULT;
//     AliHLTUInt32_t count = blocks.size();
//     if ( !count )
// 	{
// 	*blockCount = 0;
// 	*outputBlocks = NULL;
// 	return 0;
// 	}
//     *outputBlocks = reinterpret_cast<AliHLTComponentBlockData*>( AllocMemory( param, sizeof(AliHLTComponentBlockData)*count ) );
//     if ( !*outputBlocks )
// 	return ENOMEM;
//     for ( unsigned long i = 0; i < count; i++ )
// 	(*outputBlocks)[i] = blocks[i];
//     *blockCount = count;
//     return 0;
//     }

int AliRootWrapperSubscriber::GetEventDoneData( void* param, AliHLTEventID_t, unsigned long dataSize, AliHLTComponentEventDoneData** edd )
    {
    // Note that param can be NULL if not called from actual component context (e.g system initialization log messages...
    // Should not happen here though
    if ( !param )
	return EFAULT;
    //*edd = reinterpret_cast<AliHLTEventDoneData*>( reinterpret_cast<AliHLTProcessingSubscriber*>( param )->GetEventDoneData( (AliEventID_t)eventID, (AliUInt32_t)( size/4 + ((size%4) ? 1 : 0) ) ) );
    *edd = reinterpret_cast<AliHLTComponentEventDoneData*>( AllocMemory( param, sizeof(AliHLTComponentEventDoneData)+dataSize ) );
    if ( !*edd )
	return ENOMEM;
    (*edd)->fStructSize = sizeof(AliHLTComponentEventDoneData);
    (*edd)->fDataSize = dataSize;
    (*edd)->fData = (void*)( ((AliUInt8_t*)(*edd))+sizeof(AliHLTComponentEventDoneData) );
    return 0;
    }

int AliRootWrapperSubscriber::Logging( void* /* param */, AliHLTComponentLogSeverity severity, const char* origin, const char* keyword, const char* message )
    {
    // Note that param can be NULL if not called from actual component context (e.g system initialization log messages...
    LOGC( gAnalysisLogLevel, (AliHLTLog::TLogLevel)severity, origin, keyword )
	<<  message << ENDLOG;
    return 0;
    }


const char* AliRootWrapperSubscriber::GetComponentInfo( void* param )
    {
    return reinterpret_cast<AliRootWrapperSubscriber*>( param )->fComponentInfo.c_str();
    }




class AliRootWrapperSubscriberComponent: public AliHLTControlledProcessorComponent
    {
    public:
  
	AliRootWrapperSubscriberComponent( const char* compName, int argc, char** argv );
	virtual ~AliRootWrapperSubscriberComponent();
  
    protected:

	virtual bool CheckConfiguration();
	virtual void ShowConfiguration();

	virtual bool SetupStdLogging();
  
	virtual void DestroyParts();
  
	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
  
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );
  
	virtual bool SetupComponents();

	virtual AliHLTProcessingSubscriber* CreateSubscriber();

	AliRootInterface fAliRootInterface;


	MLUCString fComponentID;
	bool fComponentIDSet;
	MLUCString fComponentOptions;

#if 0
	MLUCString fLibraryName;
	bool fAdditionalLibrary;
	bool fAdditionalLibraryLoaded;
	vector<MLUCString> fCompOptions;
	vector<MLUCString> fAddShLibNames;
#endif

	bool fForwardCorruptBlocks;

	bool fAnalysisLogLevelSet;
        
    private:
    };


AliRootWrapperSubscriberComponent::AliRootWrapperSubscriberComponent( const char* compName, int argc, char** argv ):
    AliHLTControlledProcessorComponent( compName, argc, argv )
    {
    fForwardCorruptBlocks = false;

    fAnalysisLogLevelSet = false;
    }

AliRootWrapperSubscriberComponent::~AliRootWrapperSubscriberComponent()
    {
    }
  
bool AliRootWrapperSubscriberComponent::CheckConfiguration()
    {
    AliUInt32_t oldMinBlockSize = fMinBlockSize;
    AliUInt32_t oldPerEventFixedSize = fPerEventFixedSize;
    AliUInt32_t oldPerBlockFixedSize = fPerBlockFixedSize;
    double oldSizeFactor = fSizeFactor;
     // Fool CheckConfiguration into thinking size specifiers are set
    fMinBlockSize = 1;
    fPerEventFixedSize = 1;
    fPerBlockFixedSize = 1;
    fSizeFactor = 1.0;
    if ( !AliHLTControlledProcessorComponent::CheckConfiguration() )
	return false;
    fMinBlockSize = oldMinBlockSize;
    fPerEventFixedSize = oldPerEventFixedSize;
    fPerBlockFixedSize = oldPerBlockFixedSize;
    fSizeFactor = oldSizeFactor;
    if ( !fComponentIDSet )
	{
	LOG( AliHLTLog::kError, "AliRootWrapperSubscriberComponent::CheckConfiguration", "No AliRoot component ID" )
	    << "Must specify an AliRoot component ID, e.g. using the -componentid command line option."
	    << ENDLOG;
	return false;
	}
    return true;
    }

void AliRootWrapperSubscriberComponent::ShowConfiguration()
    {
    AliHLTControlledComponent::ShowConfiguration();
    LOG( AliHLTLog::kInformational, "AliRootWrapperSubscriberComponent::ShowConfiguration", "AliRoot component ID" )
	<< "Using AliRoot component ID: " << fComponentID.c_str()
	<< ENDLOG;
    }

bool AliRootWrapperSubscriberComponent::SetupStdLogging()
    {
    if ( !AliHLTControlledProcessorComponent::SetupStdLogging() )
	return false;
    gLog.SetStackTraceComparisonDepth( 4 );
    return true;
    }

void AliRootWrapperSubscriberComponent::DestroyParts()
    {
    // both interface functions could be uninitialized/invalid if the configure failed before the interface is set up...
    fAliRootInterface.Unload();

    UnregisterSubsystemVerbosity( "Analysis" );
    AliHLTControlledComponent::DestroyParts();
    }

const char* AliRootWrapperSubscriberComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cpErr = NULL;
    if ( !strcmp( argv[i], "-componentid" ) )
	{
	if ( argc <= i+1 )
	    return "Missing component ID parameter.";
	fComponentID = argv[i+1];
	fComponentIDSet = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-componentlibrary" ) )
	{
	if ( argc <= i+1 )
	    return "Missing component library path parameter.";
	fAliRootInterface.SetLibrary( argv[i+1] );
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-componentargs" ) )
	{
	if ( argc <= i+1 )
	    return "Missing component argument string parameter.";
	fComponentOptions = argv[i+1];
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-addlibrary" ) )
	{
	if ( argc <= i+1 )
	    return "Missing library name parameter.";
	fAliRootInterface.AddLibrary( argv[i+1] );
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-addlibrarybefore" ) )
	{
	if ( argc <= i+2 )
	    return "Missing \"before\" library name parameter.";
	if ( argc <= i+1 )
	    return "Missing additional and \"before\" library name parameter.";
	fAliRootInterface.AddLibrary( argv[i+1], argv[i+2] );
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-analysisverbosity" ) )
	{
	if ( argc <= i+1 )
	    return "Missing analysis verbosity parameter.";
	gAnalysisLogLevel = strtoul( argv[i+1], &cpErr, 0 );
	fAnalysisLogLevelSet = true;
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting analysis verbosity parameter";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-forwardcorruptblocks" ) )
	{
	fForwardCorruptBlocks = true;
	i += 1;
	return NULL;
	}
    return AliHLTControlledProcessorComponent::ParseCmdLine( argc, argv, i, errorArg );
    }
  
void AliRootWrapperSubscriberComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    AliHLTControlledProcessorComponent::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "AliRoot Wrapper Processing Component", "Command line usage" )
	<< "-componentid <component>: Specify the ID of the AliRoot component to be used for event processing. (Mandatory)" << ENDLOG;
    LOG( AliHLTLog::kError, "AliRoot Wrapper Processing Component", "Command line usage" )
	<< "-componentlibrary <library-path>: Specify the path to a dynamic component library from which to load the AliRoot component. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "AliRoot Wrapper Processing Component", "Command line usage" )
	<< "-componentargs <component-argument-string>: Specify a string of arguments to be passed to the AliRoot component. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "AliRoot Wrapper Processing Component", "Command line usage" )
	<< "-addlibrary <library-path>: Specify the path to a dynamic library that has to be loaded prior to the component library. (Optional, can be used multiple times)" << ENDLOG;
    LOG( AliHLTLog::kError, "AliRoot Wrapper Processing Component", "Command line usage" )
	<< "-forwardcorruptblocks: Forward also corrupt data blocks to the processing code. (Optional)" << ENDLOG;
    }
 

bool AliRootWrapperSubscriberComponent::SetupComponents()
    {
    if ( !AliHLTControlledComponent::SetupComponents() )
	return false;

    reinterpret_cast<AliRootWrapperSubscriber*>(fSubscriber)->SetAliRootInterface( &fAliRootInterface );

    if ( !fAnalysisLogLevelSet )
	gAnalysisLogLevel = gLogLevel;

    int ret;

    ret = fAliRootInterface.SetEnvironment( (void*)reinterpret_cast<AliRootWrapperSubscriber*>(fSubscriber),
				      &(AliRootWrapperSubscriber::AllocMemory),
				      &(AliRootWrapperSubscriber::GetEventDoneData),
				      &(AliRootWrapperSubscriber::Logging) );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliRootWrapperSubscriberComponent::SetupComponents", "Cannot set interface environment" )
	    << "Cannot set AliRoot interface environment: " << strerror(ret) << " ("
	    << AliHLTLog::kDec << ret << ")." << ENDLOG;
	return false;
	}

    MLUCString compInfo;
    compInfo = "chainid=";
    compInfo += fComponentName;
    ret = fAliRootInterface.SetComponent( fComponentID.c_str(), fComponentOptions.c_str(), compInfo.c_str() );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliRootWrapperSubscriberComponent::SetupComponents", "Cannot set interface component data" )
	    << "Cannot set component data for AliRoot interface: " << strerror(ret) << " ("
	    << AliHLTLog::kDec << ret << ")." << ENDLOG;
	return false;
	}

    ret = fAliRootInterface.SetRunInformation( fBeamType, fHLTMode, fRunNumber, fRunType.c_str() );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliRootWrapperSubscriberComponent::SetupComponents", "Cannot set interface run information" )
	    << "Cannot set run information for AliRoot interface: " << strerror(ret) << " ("
	    << AliHLTLog::kDec << ret << ")." << ENDLOG;
	return false;
	}
    
    ret = fAliRootInterface.Load();
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliRootWrapperSubscriberComponent::SetupComponents", "Cannot load AliRoot interface" )
	    << "Cannot load AliRoot interface: " << strerror(ret) << " ("
	    << AliHLTLog::kDec << ret << ")." << ENDLOG;
	return false;
	}

    if ( fPerEventFixedSize==0 && fPerBlockFixedSize==0 && fSizeFactor==0.0 )
	{
	unsigned long constBasePerBlock=0;
	unsigned long constBasePerEvent=0;
	double inputMultiplier=0.0;
	ret = fAliRootInterface.GetOutputDataSize( constBasePerEvent, constBasePerBlock, inputMultiplier );
	if ( !ret )
	    {
	    fPerEventFixedSize = constBasePerEvent;
	    fPerBlockFixedSize = constBasePerBlock;
	    fSizeFactor = inputMultiplier;
	    }
	else
	    {
	    LOG( AliHLTLog::kError, "AliRootWrapperSubscriberComponent::SetupComponents", "Cannot get output data size info" )
		<< "Cannot get output data predictors from AliRoot interface: " << strerror(ret) << " ("
		<< AliHLTLog::kDec << ret << ")." << ENDLOG;
	    return false;
	    }
	fSubscriber->SetOutputSizeParameters( fMinBlockSize, fPerEventFixedSize, fPerBlockFixedSize, fSizeFactor );
	}
        

    reinterpret_cast<AliRootWrapperSubscriber*>(fSubscriber)->SetComponentID( fComponentID );
    AliHLTComponentDataType dataType;
    ret = fAliRootInterface.GetOutputDataType( dataType );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliRootWrapperSubscriberComponent::SetupComponents", "Cannot get data type" )
	    << "Unable to query component output data type: " << strerror(ret) << " ("
	    << AliHLTLog::kDec << ret << ")." << ENDLOG;
	return false;
	}
    reinterpret_cast<AliRootWrapperSubscriber*>(fSubscriber)->SetComponentDataType( dataType );
    reinterpret_cast<AliRootWrapperSubscriber*>(fSubscriber)->ForwardCorruptBlocks( fForwardCorruptBlocks );

    reinterpret_cast<AliRootWrapperSubscriber*>(fSubscriber)->SetComponentInfo( compInfo.c_str() );

    RegisterSubsystemVerbosity( "Analysis", gAnalysisLogLevel );

    return true;
    }

AliHLTProcessingSubscriber* AliRootWrapperSubscriberComponent::CreateSubscriber()
    {
    return new AliRootWrapperSubscriber( fSubscriberID.c_str(), fSendEventDone, fMaxPreEventCountExp2 );
    }


int main( int argc, char** argv )
    {
    AliRootWrapperSubscriberComponent procComp( "AliRootWrapperSubscriber", argc, argv ); // , 4194304, 1048576 );
    procComp.Run();
    return 0;
    }

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliRootWrapperSubscriber.cpp,v 1.2 2005/02/09 13:47:49 timm Exp $ 
**
***************************************************************************
*/

