#!/usr/bin/env python

import sys, os, re, string

infiles = []
if len(sys.argv)<=1:
    infiles.append(sys.stdin)
else:
    for inf in sys.argv[1:]:
        if inf=="-":
            infiles.append(sys.stdin)
        try:
            infiles.append( open( inf, "r" ) )
        except IOError,e:
            print "Error opening input file '"+inf+"': "+str(e)
            sys.exit(1)

func_pat = re.compile( "^([^\(]*) ([_a-zA-Z][_a-zA-Z0-9]*)(\(.*)(#.*)?$" )

print "typedef void (*TVoidVoidDummyFunc)();"
for inf in infiles:
    line=inf.readline()
    while line!="":
        if line[0]=="#":
            line=inf.readline()
            continue
        if line[-1]=="\n":
            line=line[:-1]
        match = func_pat.search( line )
        if match != None:
            #print "Match:",line
            #print match.group(1)
            #print match.group(2)
            #print match.group(3)
            print "#ifdef __cplusplus"
            print "extern \"C\" {"
            print "#endif /* __cplusplus */"
            print "typedef "+match.group(1)+" (*T"+match.group(2)+"Func)"+match.group(3)+";"
            print "inline T"+match.group(2)+"Func Load"+match.group(2)+"Func( TVoidVoidDummyFunc (*loadFunc)(const char*) )"
            print "    {"
            print "    return reinterpret_cast<T"+match.group(2)+"Func>( loadFunc( \""+line+"\" ) );"
            print "    }"
            print "#ifdef __cplusplus"
            print "} /* extern \"C\" */"
            print "#endif /* __cplusplus */"
            print
            print
        line=inf.readline()
