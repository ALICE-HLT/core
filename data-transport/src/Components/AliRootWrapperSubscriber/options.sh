#!/bin/bash

FOUND_EVENTDATATYPE=`echo '#include "AliHLTDataTypes.h"
int main(){ AliHLTComponentDataType tmp; tmp = kAliHLTDataTypeEvent; return 0; }'|g++ -x c++ -o tmp.o -c  ${INCLUDE} ${DEFINES} ${CCOPT} -w - 2>&1 ; if -e [ tmp.o ] ; then rm tmp.o ; fi >/dev/null 2>/dev/null`

if [ -n "${FOUND_EVENTDATATYPE}"  ] ; then
 echo -n -DNEED_EVENTTYPE" "
fi


FOUND_HLTEVENTTRIGGER=`echo '#include "AliHLTDataTypes.h"
int main(){ AliHLTEventTriggerData tmp; return 0; }'|g++ -x c++ -o tmp.o -c  ${INCLUDE} ${DEFINES} ${CCOPT} -w - 2>&1 ; if -e [ tmp.o ] ; then rm tmp.o ; fi >/dev/null 2>/dev/null`

if [ -n "${FOUND_HLTEVENTTRIGGER}"  ] ; then
 echo -n -DNEED_HLTEVENTTRIGGER" "
fi


FOUND_COMPDESCRIPTIONCALLBACK=`echo '#include "AliHLTDataTypes.h"
int main(){ AliHLTComponentEnvironment tmp; tmp.fGetComponentDescription = 0; return 0; }'|g++ -x c++ -o tmp.o -c  ${INCLUDE} ${DEFINES} ${CCOPT} -w - 2>&1 ; if -e [ tmp.o ] ; then rm tmp.o ; fi >/dev/null 2>/dev/null`

if [ -n "${FOUND_COMPDESCRIPTIONCALLBACK}"  ] ; then
 echo -n -DDONTHAVE_COMPDESCRIPTIONCALLBACK" "
fi

