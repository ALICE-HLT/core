/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: RORCPGTestSubscriber.cpp 945 2006-05-10 12:36:55Z timm $ 
**
***************************************************************************
*/

#include "AliHLTControlledComponent.hpp"
#include "AliHLTProcessingSubscriber.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <glob.h>

class RORCPGTestSubscriber: public AliHLTProcessingSubscriber
    {
    public:

	enum TPatternType { kNone=0, kCounter=1, kDownCounter, kWalkingZero, kWalkingOne, kA55A, kConstant, kConstantHLTDDLTest, kHLTDDLTestSequence };


	RORCPGTestSubscriber( const char* name, 
			      TPatternType pattern,
			      uint32 compareHighTag,
			      uint32 counterStartValue,
			      uint32 constantValue,
			      bool expectheader,
			      bool sendEventDone, AliUInt32_t minBlockSize,
			      AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, 
			      int eventCountAlloc = -1 );
  
	virtual ~RORCPGTestSubscriber();

    protected:

	virtual bool ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* etsp,
				   AliUInt8_t* outputPtr, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, AliHLTEventDoneData*& edd );


	int WriteData( int fh, const void* data, unsigned long size );

	TPatternType fPattern;
	uint32 fCompareHighTag;
	uint32 fCounterStartValue;
	uint32 fConstantValue;
	bool fExpectDDLHeader;
	bool fLastEventWas10000Words;
	bool fResetEventCounter;

	unsigned long fEventCounter;
	
    private:
    };




RORCPGTestSubscriber::RORCPGTestSubscriber( const char* name, 
					    TPatternType pattern,
					    uint32 compareHighTag,
					    uint32 counterStartValue,
					    uint32 constantValue,
					    bool expectheader,
					    bool sendEventDone, AliUInt32_t minBlockSize,
					    AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, 
					    int eventCountAlloc ):
    AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize, perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc ),
    fPattern( pattern ),
    fCompareHighTag( compareHighTag ),
    fCounterStartValue( counterStartValue ),
    fConstantValue( constantValue ),
    fExpectDDLHeader(expectheader),
    fLastEventWas10000Words(false),
    fResetEventCounter(false),
    fEventCounter(0)
    {
    }

RORCPGTestSubscriber::~RORCPGTestSubscriber()
    {
    }

bool RORCPGTestSubscriber::ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, const AliHLTSubEventDataDescriptor*, const AliHLTEventTriggerStruct*,
					 AliUInt8_t*, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>&, AliHLTEventDoneData*& )
    {
    size = 0;
    unsigned long i;
    if ( blockCnt<=0 )
	return true;

    uint8* blockData;
    uint32 blockSize;

    for ( i = 0; i < blockCnt; i++ )
	{
	blockData = blocks[i].fData;
	blockSize = blocks[i].fSize;

	uint32* words = (uint32*)( blockData );
	uint32 wordCnt = (blockSize) / sizeof(uint32);
	if (fExpectDDLHeader)
	{
	    if ( blockSize <=32 )
	        continue; // Only CDH or less

	    words = (uint32*)( blockData+32 ); // Start of actual 32 bit words, after header.
	    wordCnt = (blockSize-32) / sizeof(uint32);
	}
		
	switch ( fPattern )
	    {
	    case kNone:
		break;
	    case kCounter:
		{

		uint32 counterVal = (((uint32)fCompareHighTag) << 28) + fCounterStartValue;
		for ( uint32 counter = 0; counter < wordCnt; counter++ )
		    {
		    if ( ((uint32*)words)[counter] != counterVal )
			{
			LOG( AliHLTLog::kError, "RORCPGTestSubscriber::ProcessEvent", "Data Check Error" )
			    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
			    << ") Data Error: Offset 0x" << AliHLTLog::kHex << counter << " (" << AliHLTLog::kDec
			    << counter << ") Expected: 0x" << AliHLTLog::kHex << counterVal << " ("
			    << AliHLTLog::kDec << counterVal << ") - Found: 0x" << AliHLTLog::kHex
			    << ((uint32*)words)[counter] << " (" << AliHLTLog::kDec
			    << ((uint32*)words)[counter] << ")." << ENDLOG;
			if ( (((uint32*)words)[counter] & 0xF0000000) == (((uint32)fCompareHighTag) << 28) )
			    {
			    LOG( AliHLTLog::kError, "RORCPGTestSubscriber::ProcessEvent", "Data Check Error" )
				<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
				<< "): Resuming counter as 0x" << AliHLTLog::kHex << ((uint32*)words)[counter]
				<< " (" << AliHLTLog::kDec << ((uint32*)words)[counter] << ")."
				<< ENDLOG;
			    counterVal = ((uint32*)words)[counter];
			    }
			}
		    counterVal++;
		    }
		break;
		}
	    case kDownCounter:
		{

		uint32 counterVal = (((uint32)fCompareHighTag) << 28) + fCounterStartValue;
		for ( uint32 counter = 0; counter < wordCnt; counter++ )
		    {
		    if ( ((uint32*)words)[counter] != counterVal )
			{
			LOG( AliHLTLog::kError, "RORCPGTestSubscriber::ProcessEvent", "Data Check Error" )
			    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
			    << ") Data Error: Offset 0x" << AliHLTLog::kHex << counter << " (" << AliHLTLog::kDec
			    << counter << ") Expected: 0x" << AliHLTLog::kHex << counterVal << " ("
			    << AliHLTLog::kDec << counterVal << ") - Found: 0x" << AliHLTLog::kHex
			    << ((uint32*)words)[counter] << " (" << AliHLTLog::kDec
			    << ((uint32*)words)[counter] << ")." << ENDLOG;
			if ( (((uint32*)words)[counter] & 0xF0000000) == (((uint32)fCompareHighTag) << 28) )
			    {
			    LOG( AliHLTLog::kError, "RORCPGTestSubscriber::ProcessEvent", "Data Check Error" )
				<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
				<< "): Resuming counter as 0x" << AliHLTLog::kHex << ((uint32*)words)[counter]
				<< " (" << AliHLTLog::kDec << ((uint32*)words)[counter] << ")."
				<< ENDLOG;
			    counterVal = ((uint32*)words)[counter];
			    }
			}
		    counterVal--;
		    }
		break;
		}
	    case kWalkingZero:
		{
		uint32 pattern = 0xFFFFFFFE;
		for ( uint32 counter = 0; counter < wordCnt; counter++ )
		    {
		    pattern = ~( (uint32)1 << (counter%32) );
		    if ( ((uint32*)words)[counter] != pattern )
			{
			LOG( AliHLTLog::kError, "RORCPGTestSubscriber::ProcessEvent", "Data Check Error" )
			    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
			    << ") Data Error: Offset 0x" << AliHLTLog::kHex << counter << " (" << AliHLTLog::kDec
			    << counter << ") Expected: 0x" << AliHLTLog::kHex << pattern << " ("
			    << AliHLTLog::kDec << pattern << ") - Found: 0x" << AliHLTLog::kHex
			    << ((uint32*)words)[counter] << " (" << AliHLTLog::kDec
			    << ((uint32*)words)[counter] << ")." << ENDLOG;
			for ( unsigned i=0; i< 32; i++ )
			    {
			    if ( ((uint32*)words)[counter] == ~( (uint32)1 << (counter%32)) )
				pattern = ((uint32*)words)[counter];
			    }
			
			}
		    }
		break;
		}
	    case kWalkingOne:
		{
		uint32 pattern = 1;
		for ( uint32 counter = 0; counter < wordCnt; counter++ )
		    {
		    pattern = (uint32)1 << (counter%32);
		    if ( ((uint32*)words)[counter] != pattern )
			{
			LOG( AliHLTLog::kError, "RORCPGTestSubscriber::ProcessEvent", "Data Check Error" )
			    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
			    << ") Data Error: Offset 0x" << AliHLTLog::kHex << counter << " (" << AliHLTLog::kDec
			    << counter << ") Expected: 0x" << AliHLTLog::kHex << pattern << " ("
			    << AliHLTLog::kDec << pattern << ") - Found: 0x" << AliHLTLog::kHex
			    << ((uint32*)words)[counter] << " (" << AliHLTLog::kDec
			    << ((uint32*)words)[counter] << ")." << ENDLOG;
			for ( unsigned i=0; i< 32; i++ )
			    {
			    if ( ((uint32*)words)[counter] == (uint32)1 << (counter%32) )
				pattern = ((uint32*)words)[counter];
			    }
			
			}
		    }
		break;
		}
	    case kA55A:
		{
		uint32 pattern = 0xA55AA55A;
		for ( uint32 counter = 0; counter < wordCnt; counter++ )
		    {
		    if ( counter & 1 )
			pattern = 0x5AA55AA5;
		    else
			pattern = 0xA55AA55A;
		    if ( ((uint32*)words)[counter] != pattern )
			{
			LOG( AliHLTLog::kError, "RORCPGTestSubscriber::ProcessEvent", "Data Check Error" )
			    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
			    << ") Data Error: Offset 0x" << AliHLTLog::kHex << counter << " (" << AliHLTLog::kDec
			    << counter << ") Expected: 0x" << AliHLTLog::kHex << pattern << " ("
			    << AliHLTLog::kDec << pattern << ") - Found: 0x" << AliHLTLog::kHex
			    << ((uint32*)words)[counter] << " (" << AliHLTLog::kDec
			    << ((uint32*)words)[counter] << ")." << ENDLOG;
			for ( unsigned i=0; i< 32; i++ )
			    {
			    if ( (((uint32*)words)[counter] == 0x5AA55AA5) || (((uint32*)words)[counter] == 0xA55AA55A) )
				pattern = ((uint32*)words)[counter];
			    }
			
			}
		    }
		break;
		}
	    case kConstant:
		{
		for ( uint32 counter = 0; counter < wordCnt; counter++ )
		    {
		    if ( ((uint32*)words)[counter] != fConstantValue )
			{
			LOG( AliHLTLog::kError, "RORCPGTestSubscriber::ProcessEvent", "Data Check Error" )
			    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
			    << ") Data Error: Offset 0x" << AliHLTLog::kHex << counter << " (" << AliHLTLog::kDec
			    << counter << ") Expected: 0x" << AliHLTLog::kHex << fConstantValue << " ("
			    << AliHLTLog::kDec << fConstantValue << ") - Found: 0x" << AliHLTLog::kHex
			    << ((uint32*)words)[counter] << " (" << AliHLTLog::kDec
			    << ((uint32*)words)[counter] << ")." << ENDLOG;
			}
		    }
		break;
		}
	    case kConstantHLTDDLTest:
		{
		if ( wordCnt != 100 )
		    {
		    LOG( AliHLTLog::kError, "RORCPGTestSubscriber::ProcessEvent", "Data Check Error" )
			<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
			<< ") has wrong size: Expected: 100 (0x64) - Found: 0x" << AliHLTLog::kHex
			    << wordCnt << " (" << AliHLTLog::kDec
			    << wordCnt << ")." << ENDLOG;
		    }
		if ( ((uint32*)words)[0] != 0x00000001 )
		    {
		    LOG( AliHLTLog::kError, "RORCPGTestSubscriber::ProcessEvent", "Data Check Error" )
			<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
			<< ") Data Error: Offset 0x0 (0) Expected: 0x00000001 (1) - Found: 0x" << AliHLTLog::kHex
			    << ((uint32*)words)[0] << " (" << AliHLTLog::kDec
			    << ((uint32*)words)[0] << ")." << ENDLOG;
		    
		    }
		uint32 constant = fConstantValue;
		uint32 size = wordCnt<100 ? wordCnt : 100;
		for ( uint32 counter = 1; counter < size; counter++ )
		    {
		    if ( ((uint32*)words)[counter] != constant )
			{
			LOG( AliHLTLog::kError, "RORCPGTestSubscriber::ProcessEvent", "Data Check Error" )
			    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
			    << ") Data Error: Offset 0x" << AliHLTLog::kHex << counter << " (" << AliHLTLog::kDec
			    << counter << ") Expected: 0x" << AliHLTLog::kHex << constant << " ("
			    << AliHLTLog::kDec << constant << ") - Found: 0x" << AliHLTLog::kHex
			    << ((uint32*)words)[counter] << " (" << AliHLTLog::kDec
			    << ((uint32*)words)[counter] << ")." << ENDLOG;
			constant = ((uint32*)words)[counter];
			}
		    }
		break;
		}
	    case  kHLTDDLTestSequence:
		{
		// We need to reset the fEventCounter the first time we start the test cycle because DAQ
		// could have been sending us events half way through the cycle.
		if (fLastEventWas10000Words && wordCnt == 101 && (! fResetEventCounter))
		    {
		    fEventCounter = 0;
		    fResetEventCounter = true;
		    LOG( AliHLTLog::kInformational, "RORCPGTestSubscriber::ProcessEvent", "Starting checking" )
			    << "Starting to validate events from event 0x" << AliHLTLog::kHex << eventID
			    << " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
		    }
		fLastEventWas10000Words = (wordCnt == 10000);
		if (! fResetEventCounter) break;  // Wait to start testing when the test cycle starts again.
		
		unsigned long modCount = fEventCounter % 300;
		unsigned long expectedWordCount=0;
		if ( modCount<100 )
		    expectedWordCount = 101;
		else if ( modCount<200 )
		    expectedWordCount = 102;
		else
		    expectedWordCount = 10000;
		uint32 size=expectedWordCount;
		if ( wordCnt != expectedWordCount )
		    {
		    LOG( AliHLTLog::kError, "RORCPGTestSubscriber::ProcessEvent", "Data Check Error" )
			<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
			<< ") has wrong size: Expected: 0x" << AliHLTLog::kHex
			<< expectedWordCount << " (" << AliHLTLog::kDec << expectedWordCount
			<< ") - Found: 0x" << AliHLTLog::kHex
			<< wordCnt << " (" << AliHLTLog::kDec
			<< wordCnt << ")." << ENDLOG;
		    if ( wordCnt<expectedWordCount )
			size = wordCnt;
		    }
		if ( (modCount < 100) && (((uint32*)words)[0] != 1))
		    {
		    LOG( AliHLTLog::kError, "RORCPGTestSubscriber::ProcessEvent", "Data Check Error" )
			<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
			<< ") Data Error: Offset 0x0 (0) Expected: " << AliHLTLog::kHex << 1
			<< " (" << AliHLTLog::kDec << (fEventCounter%100)+1 << ") - Found: 0x" << AliHLTLog::kHex
			<< ((uint32*)words)[0] << " (" << AliHLTLog::kDec
			<< ((uint32*)words)[0] << ")." << ENDLOG;
		    }
		if ( (! (modCount < 100)) && (((uint32*)words)[0] != (fEventCounter%100)+1) )
		    {
		    LOG( AliHLTLog::kError, "RORCPGTestSubscriber::ProcessEvent", "Data Check Error" )
			<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
			<< ") Data Error: Offset 0x0 (0) Expected: " << AliHLTLog::kHex << (fEventCounter%100)+1
			<< " (" << AliHLTLog::kDec << (fEventCounter%100)+1 << ") - Found: 0x" << AliHLTLog::kHex
			<< ((uint32*)words)[0] << " (" << AliHLTLog::kDec
			<< ((uint32*)words)[0] << ")." << ENDLOG;
		    }
		uint32 pattern = 0x55aa55aa;
		for ( uint32 counter = 1; counter < size; counter++ )
		    {
		    if ( counter & 1 )
			pattern = 0x55aa55aa;
		    else
			pattern = 0xaa55aa55;
		    if ( ((uint32*)words)[counter] != pattern )
			{
			LOG( AliHLTLog::kError, "RORCPGTestSubscriber::ProcessEvent", "Data Check Error" )
			    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
			    << ") Data Error: Offset 0x" << AliHLTLog::kHex << counter << " (" << AliHLTLog::kDec
			    << counter << ") Expected: 0x" << AliHLTLog::kHex << pattern << " ("
			    << AliHLTLog::kDec << pattern << ") - Found: 0x" << AliHLTLog::kHex
			    << ((uint32*)words)[counter] << " (" << AliHLTLog::kDec
			    << ((uint32*)words)[counter] << ")." << ENDLOG;
			
			}
		    }
		break;
		}
	    }
	}
	++fEventCounter;
    return true;
    }
	    




class RORCPGTestSubscriberComponent: public AliHLTControlledSinkComponent
    {
    public:

	RORCPGTestSubscriberComponent( const char* name, int argc, char** argv );
	virtual ~RORCPGTestSubscriberComponent() {};


    protected:

	virtual void ShowConfiguration();
	virtual bool CheckConfiguration();

	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

	virtual RORCPGTestSubscriber* CreateSubscriber();

	RORCPGTestSubscriber::TPatternType fPattern;
	uint32 fCompareHighTag;
	uint32 fCounterStartValue;
	uint32 fConstantValue;
	bool fConstantValueSet;
	bool fExpectDDLHeader;

	typedef AliHLTControlledSinkComponent TSuper;

    private:
    };


RORCPGTestSubscriberComponent::RORCPGTestSubscriberComponent( const char* name, int argc, char** argv ):
    TSuper( name, argc, argv ),
    fPattern( RORCPGTestSubscriber::kCounter ),
    fCompareHighTag( 0 ),
    fCounterStartValue( 0 ),
    fConstantValue( 0 ),
    fConstantValueSet( false ),
    fExpectDDLHeader( true )
    {
    }

void RORCPGTestSubscriberComponent::ShowConfiguration()
    {
    TSuper::ShowConfiguration();
    LOG( AliHLTLog::kInformational, "RORCPGTestSubscriberComponent::ShowConfiguration", "Comparison high tag" )
	<< "Comparison high tag: 0x" << AliHLTLog::kHex << fCompareHighTag << "." << ENDLOG;
    LOG( AliHLTLog::kInformational, "RORCPGTestSubscriberComponent::ShowConfiguration", "Counter start value" )
	<< "Counter start value: 0x" << AliHLTLog::kHex << fCounterStartValue << "." << ENDLOG;
    LOG( AliHLTLog::kInformational, "RORCPGTestSubscriberComponent::ShowConfiguration", "Constant value" )
	<< "Constant value: 0x" << AliHLTLog::kHex << fConstantValue << "." << ENDLOG;
    const char* mode = "UNKNOWN";
    switch ( fPattern )
	{
	case RORCPGTestSubscriber::kNone:
	    mode = "None";
	    break;
	case RORCPGTestSubscriber::kCounter:
	    mode = "Counter";
	    break;
	case RORCPGTestSubscriber::kDownCounter:
	    mode = "Down Counter";
	    break;
	case RORCPGTestSubscriber::kWalkingZero:
	    mode = "Walking Zero";
	    break;
	case RORCPGTestSubscriber::kWalkingOne:
	    mode = "Walking One";
	    break;
	case RORCPGTestSubscriber::kA55A:
	    mode = "0xA55AA55A/0x5AA55AA5";
	    break;
	case RORCPGTestSubscriber::kConstant:
	    mode = "Constant Value";
	    break;
	case RORCPGTestSubscriber::kConstantHLTDDLTest:
	    mode = "HLT DDL Link Identification Test";
	    break;
	case RORCPGTestSubscriber::kHLTDDLTestSequence:
	    mode = "HLT Link Test Sequence";
	    break;
	default:
	    mode = "UNKNOWN";
	    break;
	}
    LOG( AliHLTLog::kInformational, "RORCPGTestSubscriberComponent::ShowConfiguration", "Comparison mode" )
	<< "Comparison mode: " << mode << "." << ENDLOG;
    LOG( AliHLTLog::kInformational, "RORCPGTestSubscriberComponent::ShowConfiguration", "Expect DDL header" )
	<< "The 8 word / 32 byte DDL header is expected: " << (fExpectDDLHeader ? "true" : "false" ) << "." << ENDLOG;
    }

bool RORCPGTestSubscriberComponent::CheckConfiguration()
    {
    if ( !TSuper::CheckConfiguration() )
	return false;
    if ( (fPattern==RORCPGTestSubscriber::kConstant || fPattern==RORCPGTestSubscriber::kConstantHLTDDLTest) && !fConstantValue )
	{
	LOG( AliHLTLog::kError, "RORCPGTestSubscriberComponent::ShowConfiguration", "Constant value not set" )
	    << "Constant value has to be set via -constantvalue option when using comparison pattern constant" << ENDLOG;
	return false;
	}
    return true;
    }


const char* RORCPGTestSubscriberComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cpErr = NULL;
    if ( !strcmp( argv[i], "-comparehightag" ) )
	{
	if ( argc <= i+1 )
	    return "Missing comparison high tag specifier.";
	fCompareHighTag = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting comparison high tag specifier.";
	    }
	if ( fCompareHighTag > 0xF )
	    {
	    errorArg = i+1;
	    return "Comparison high tag specifier is too large (Max. allowed value 0xF).";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-constantvalue" ) )
	{
	if ( argc <= i+1 )
	    return "Missing constant value specifier.";
	fConstantValue = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting constant value specifier.";
	    }
	fConstantValueSet = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-counterstartvalue" ) )
	{
	if ( argc <= i+1 )
	    return "Missing counter start value specifier.";
	fCounterStartValue = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting counter start value specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-comparepattern" ) )
	{
	if ( argc <= i+1 )
	    return "Missing comparison pattern specifier.";
	if ( !strcmp( argv[i+1], "counter" ) )
	    fPattern = RORCPGTestSubscriber::kCounter;
	else if ( !strcmp( argv[i+1], "downcounter" ) )
	    fPattern = RORCPGTestSubscriber::kDownCounter;
	else if ( !strcmp( argv[i+1], "walkingzero" ) )
	    fPattern = RORCPGTestSubscriber::kWalkingZero;
	else if ( !strcmp( argv[i+1], "walkingone" ) )
	    fPattern = RORCPGTestSubscriber::kWalkingOne;
	else if ( !strcmp( argv[i+1], "a55a" ) )
	    fPattern = RORCPGTestSubscriber::kA55A;
	else if ( !strcmp( argv[i+1], "constant" ) )
	    fPattern = RORCPGTestSubscriber::kConstant;
	else if ( !strcmp( argv[i+1], "hltddlidentification" ) )
	    fPattern = RORCPGTestSubscriber::kConstantHLTDDLTest;
	else if ( !strcmp( argv[i+1], "hltlinktest" ) )
	    fPattern = RORCPGTestSubscriber::kHLTDDLTestSequence;
	else
	    return "Unknown comparison pattern specifier";
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-noheader" ) )
	{
	fExpectDDLHeader = false;
	i++;
	return NULL;
	}
    return TSuper::ParseCmdLine( argc, argv, i, errorArg );
    }

void RORCPGTestSubscriberComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    TSuper::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-comparepattern ( counter|downcounter|walkingzero|walkingone|a55a|constant|hltddlidentification|hltlinktest ): Set the pattern generated by the Pattern Generator to be compared against. (Optional, default value 'counter')" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-comparehightag <tag>: Set the highest nibble (tag) of the 32 bit counter words for comparison. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-counterstartvalue <start value>: Set the start value of the 32 bit counter words for comparison. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-constantvalue <constant value>: Set the 32 bit value to be used for comparison in constant pattern mode. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-noheader : If specified then events are assumed not to contain the 32 byte DDL header. (Optional)" << ENDLOG;
    }


RORCPGTestSubscriber* RORCPGTestSubscriberComponent::CreateSubscriber()
    {
    return new RORCPGTestSubscriber( fSubscriberID.c_str(), 
				     fPattern,
				     fCompareHighTag,
				     fCounterStartValue,
				     fConstantValue,
				     fExpectDDLHeader, 
				     fSendEventDone, fMinBlockSize, 
				     fPerEventFixedSize, fPerBlockFixedSize, fSizeFactor, 
				     fMaxPreEventCountExp2 );
    }



int main( int argc, char** argv )
    {
    RORCPGTestSubscriberComponent procComp( "RORCPGTestSubscriber", argc, argv );
    procComp.Run();
    return 0;
    }



/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: RORCPGTestSubscriber.cpp 945 2006-05-10 12:36:55Z timm $ 
**
***************************************************************************
*/
