\documentclass[a4paper]{scrartcl}

\usepackage[latin1]{inputenc}
\usepackage{hyperref}

\title{PCI and Shared Memory Interface\\ Second Generation}
\author{Florian Painke \and Timm Morten Steinbeck}
\date{May 2006}

\newcommand{\parbegin}{\vspace{3ex}\noindent}
\newcommand{\firstpar}{\noindent}
\newcommand{\subpar}{\\\indent}

\begin{document}

\maketitle
\thispagestyle{empty}


\begin{abstract}
\firstpar
The PCI and Shared Memory Interface, or short \emph{PSI}, is a set of libraries
and tools to access the system PCI bus and memory from user space. At the core
of PSI is a Linux kernel driver that exports the functionality needed.

\parbegin
The first generation of PSI was written by Timm M. Steinbeck as part of his
PhD thesis. It supports Linux kernels 2.2 and 2.4 on x86 systems. 

\parbegin
The second generation started as a port to kernel 2.6 and ended up as a 
complete rewrite of the kernel driver. As of now it supports kernels 2.4 
and 2.6 on 32 and 64 bit x86 systems and provides a few additional things 
like interrupt handling and a new DMA layer. 
It was written and is maintained by Florian Painke as part of his diploma 
thesis.

\parbegin
This document is intended to give an introduction to the PSI library and
the accompanying tools as well as some information about hardware
programming in general and specific problems of user space hardware access.
Little will be said about the core component of PSI, the kernel driver, as
most users will only interface to the driver by means of the library
functions.
\end{abstract}

\newpage

\section{Preface}

\subsection{Conventions}

\firstpar
Throughout this documentation, the \verb|typewriter| font is used to highlight
commands you should type on the command line or in a terminal window,
parameters to or output of such commands. It is also used to give examples for
programming.
\subpar Whenever a sequence of commands and their output is given on seperate
lines, the commands you should type are preceeded by \verb|>|, which suits as
placeholder for the command line prompt. For example, the contents of the
top-level directory of a fresh copy of the PSI sources as shown after a
\verb|ls| command, will be shown as:

\begin{verbatim}
> ls
doc  include  Makefile  Prefix.make  README  Rules.make  src
\end{verbatim}

\firstpar
Parts of text that should be substituted are shown in angled braces. For
example, the path to a memory region is defined as \verb|/dev/psi/mem/<name>|,
where \verb|<name>| should be substituted by whatever name you choose.

\parbegin
To highlight newly defined terms that will be used instead of a longer
description, the \emph{slanted} font is used. For example, in the abstract on
the front page, the term PSI was introduced as a short form for PCI and Shared
Memory Interface.

\subsection{Changes}

\firstpar
If you have used the first generation of PSI, you might want to skip the things
that have not changed. I recommend you read at least the sections
\ref{installation} (Installation) and \ref{advanced_hw} (Advanced Hardware
Access).

\parbegin
If you need to re-program your device and reboot the system due to your device
being disabled, check section \ref{hot-plugging} (Miscellanea) for a possible
cure.

\parbegin
In the time since I have been put in charge of PSI, I happend to come
accross some code which suggests you should be reading section \ref{mapping}
(Mapping Regions), too. At least read the notes about typing the pointer.

\section{Installation}
\label{installation}

\subsection{Prerequisites}

\firstpar
Since the core component of PSI is designed as a kernel module, you might want
to make shure you have a kernel with support for loadable modules, unless you
want hack the driver and patch your kernel. If you end up compiling a custom
kernel, you might also want to enable support to forcibly remove a kernel
module, just in case something goes wrong.

\parbegin
The PSI driver supports the bigphysarea patch, which is available serperately.
If you need large memory areas for DMA or want to share data between
applications, you might want to compile your kernel with this patch applied.
Please refer to the documentation that comes with the patch for information
on how to install and use it.

\subsection{Building and Installing}

\firstpar
Once you have obtained a copy of the PSI sources, you just have to \verb|cd|
into the PSI top-level directory, which I will refer to as the \emph{working
directory}, and call \verb|make| to build everything.
The build process creates several additional subdirectories to hold the driver,
libraries and tools, so after a successful build, the working directory should
look something like this:

\begin{verbatim}
> ls
bin  doc  include  lib  Makefile  Prefix.make  README  Rules.make  src
\end{verbatim}

\firstpar
If you want to install the PSI driver, library and tools, you can call
\verb|make install|. This will place the driver into the modules subdirectory of
the current kernel and register it with \verb|modules.dep|, so you can
\verb|modprobe| it. The tools, header files and libraries will be placed in
\verb|/usr/local/bin|, \verb|/usr/local/include| and \verb|/usr/local/lib|,
respectively.
\subpar Note that you will need administrator privileges if you want to install
to the default location.

\parbegin
If you want to install the PSI library and tools somewhere else than
the default location, you can set \verb|PREFIX| for \verb|make install|. 
For example, if you want to install to \verb|/opt/local/psi|, just call
\verb|make PREFIX=/opt/local/psi install|.
\subpar Note, however, that the driver will still be installed inside the
modules subdirectory of the current kernel.

\parbegin
Of course, you don't have to install everything. You can just use the library
and tools inside the working directory. You will have to tell your compiler
where to find header files and libraries for PSI, though.
\subpar Note that all platform dependent files are placed in platform
subdirectories. If, for example, you are working on a i686 Linux system with
kernel 2.6.12-plain, \verb|make| will create the platform subdirectories 
\verb|bin/Linux-i686| and \verb|lib/Linux-i686| for the tools and libraries, and
\verb|modules/Linux-i686/2.6.12-plain| for the driver. (You can use \verb|uname|
to get the identifiers for your system.)
\subpar You can still install the driver into the modules subdirectory of the
current kernel by calling \verb|make install-module|.

\section{The PSI Library}

\subsection{Region Handling}

\firstpar
The PSI library works mostly on \emph{regions}. Regions are simply handles to 
blocks of memory. But while the term memory usually refers to the RAM installed
in your computer, regions can also point to a block of memory or I/O ports of a
PCI device. This means, there are different types of regions.

\parbegin
Most functions of the PSI library return a result of type \verb|PSI_Status|. Be
shure to check for errors and act apropriately after each and every call
because things may eventually fail.

\subsubsection{Opening a Region}

\firstpar
First, you have to open a region. To open a region, you need a region path
which identifies the region. Depending on what type of region you want to open,
the region path varies.
\subpar A region is opened with a call to \verb|PSI_openRegion()| which is
passed a pointer to a region handle and the device path as parameters. Region
handles are of type \verb|tRegion|.

\parbegin
\textbf{Named memory regions} are identified by their name. You can choose
the name to be whatever comes to your mind, as long as the region path doesn't
exceed 100 characters, the terminating zero included.
\subpar The region path is \verb|/dev/psi/mem/<name>|. If you want to
specifically get memory from the bigphysarea, the region path should be
\verb|/dev/psi/bigphys/<name>|.

\parbegin
\textbf{Physical memory regions} are identified by their physical address. You
have to take care yourself, that the physical address exists and cannot be used
by the kernel or user space processes other than through PSI. Usually you will
reserve a block of the memory installed in your computer by passing the kernel a
\verb|mem| argument. See the kernel documentation for details.
\subpar The region path is \verb|/dev/psi/physmem/<address>|.

\parbegin
\textbf{Config space regions} point to the configuration space of a specific PCI
device. They can be identified by either the PCI address (bus, slot and
function number) or by the device id (vendor, device and index, where index is
used to identify a specific device if more than one devices use the same vendor
and device id).
\subpar Use either
\verb|/dev/psi/bus/<bus>/slot/<slot>/function/<function>/config| or
\\\verb|/dev/psi/vendor/<vendor>/device/<device>/<index>/config| as
region path, depending on whether you use the PCI address or the device id to
identify the device.

\parbegin
\textbf{Base address register regions} point to either memory or I/O ports on a
PCI device. Like config space regions, they can be identified by either the PCI
address or the device id. Additionally you have to provide the number of the
base address register which points to the memory or I/O ports on the device.
\subpar Use either
\verb|/dev/psi/bus/<bus>/slot/<slot>/function/<function>/base<bar>| or
\\\verb|/dev/psi/vendor/<vendor>/device/<device>/<index>/base<bar>| as
region path, depending on whether you use the PCI address or the device id to
identify the device.

\subsubsection{Sizing a Region}

\firstpar
After successfully opening a region, you have a valid region handle. While
config space and base address register regions have fixed sizes, named and
physical memory regions don't. You have to size a memory region before you can
actually use it. Once the size of a region has been set, it is fixed and can't
be changed throughout the lifetime of the region.
So another point of view would be, that regions for PCI devices are sized
during opening, while memory regions have to be sized manually.
\subpar You size a memory region by calling \verb|PSI_sizeRegion()| whis is
passed the region handle and a pointer to an unsigned long which holds the
requested size before the call and the granted size after the call. Note that
the granted size may differ from the requested size since memory is allocated
with a granularity of the page size defined by the system.

\parbegin
Note that regions can be opened by different processes. Of course only the
first process that opens a region has to size it. So if a call to
\verb|PSI_sizeRegion()| fails with \verb|PSI_SIZE_SET|, it has already been
sized by another process and you don't have to size it. Beware that you are
sharing the region with another process, though, and you might have to take
measures to synchronize shared access to the region. See the documentation that
comes with your development tools for details on inter application
communication.

\subsubsection{Closing a Region}

\firstpar
When you are finished using a region, you should always close it. The region
will be disposed of after all processes that have used it called
\verb|PSI_closeRegion()|. This function is passed a region handle as parameter.

\subsubsection{Preventing Disposal of Regions: Locking}

\firstpar
Sometimes you might want to make shure that a region is not disposed of even
after the last process that has used it closed it.
\subpar To lock a region, you call \verb|PSI_lockRegion()|. To unlock it, call
\verb|PSI_unlockRegion()|. You can also check the number of locks set on a
region with \verb|PSI_checkRegionLock()|. All of these functions are passed a
region handle as parameter. While the first and second function return a
result, \verb|PSI_checkRegionLock()| returns the number of locks set on the
region or a negative value if the region handle is invalid.

\subsection{Region Access}

\firstpar
Now that we have discussed the basics of handling regions, you may want to do
something useful with it. For most region types, you basically have two
choices for access to the memory the region points to.

\subsubsection{Read and Write}

\firstpar
All types of regions support read an write access through \verb|PSI_read()| and
\verb|PSI_write()|, respectively. Both functions are passed a region handle,
the byte offset to the start of the memory the region is pointing to, the size
of a single data element in bytes, the size of the block in bytes and finally a
pointer to the block of data as parameters.
\subpar The size of a single data element can be \verb|_byte_|, \verb|_word_|
or \verb|_dword_|, the block size and offset must be aligned accordingly.

\subsubsection{Mapping Regions}
\label{mapping}

\firstpar
Named, physical memory and memory base address register regions, support
mapping of the region into user space. After successfully mapping the region,
you can access the memory the region is pointing to through a pointer.
\subpar Beware that there are side effects on how you access the memory,
though. If you access memory base address register regions byte-wise, the PCI
bus will translate this to a 8-bit memory cycle and so on. This means you have
to carefully choose how to type the pointer you are using.
It is highly recommended not to use standard C types as these are not
guaranteed to have any fixed size. Rather you should include \verb|sys/types.h|
in your program and use \verb|int8_t|, \verb|u_int8_t| and the likes.

\parbegin
You can map a region by calling \verb|PSI_mapRegion()|. This function
is passed a region handle and a pointer to a pointer variable which will hold
the virtual address of the mapping after the call. To unmap the region again,
call \verb|PSI_unmapRegion()|, which is passed the region handle and the
pointer value returned by the call to map the region.
\subpar You can also map only a part of the region, called \emph{window}, by
calling \verb|PSI_mapWindow()|. You pass this function a region handle, the
byte offset to the start of the memory the region is pointing to, the size of
the window you want to map and, again, a pointer to a pointer variable. To
unmap the window, you call \verb|PSI_unmapWindow()|, which is passed the region
handle, the pointer value and the size of the window.

\parbegin
Config space and I/O base address register regions do not support mapping.

\subsection{Advanced Hardware Access}
\label{advanced_hw}

\firstpar
While the previous section introduced the basic means by which hardware can be
accessed, this section delves into some of the more advanced topics, such as
interrupts, direct memory access and timing issues.

\subsubsection{Interrupts}

\firstpar
When communicating with a device the need for synchronization soon becomes
evident. You can of course implement a register in your device design that your
program checks regularly, but the use of active waiting means unnecessary load
on both the processor and the PCI bus. This can be solved by implementing
interrupts in your device design.

\parbegin
With the advent of PSI2, the driver supports basic use of interrupts from user
space. There are, however, some things you have to consider, when implementing
interrupts in your device design and writing the program that will make use of
the interrupts fired by your device.
\subpar First of all, it is convenient to implement a register in your device
design that will control how the device drives the PCI interrupt pins. There
should at least be one flag to completely prevent interrupts and one flag to
reset the interrupt once fired. Your device should by default not fire any
interrupts unless the flag to allow interrupts is explicitly set, since
unhandled interrupts may confuse the system.
\subpar That said, the procedure for using interrupts for synchronization is as
follows: open a config region, tell your device it is allowed to fire
interrupts, wait for an interrupt to occur, reset the interrupt and tell your
device not to fire any more interrupts, then flush all pending interrupts.
If you need to wait for interrupts in a loop, start over with telling your
device it is allowed to fire interrupts again, wait for the interrupt to occur,
and so on.
\subpar As you can see from the above discussion it is indeed convenient to
implement both flags within a single register, as you won't need to write more
than one word through the PCI bus.

\parbegin
To wait for an interrupt, simply call \verb|PSI_waitInterrupt()|. This
function must be passed a handle to a config space region for the device that
fires the interrupt your program wants to listen to. Your program will be put
to sleep until the interrupt occurs. When your program receives a signal or the
config space region is forcibly closed, waiting will be aborted and an error
will be returned as result. So again, be shure to check the result before
assuming you got an interrupt.
\subpar To flush all pending interrupts, you call \verb|PSI_flushInterrupts()|
and pass it the handle to the same config space region you use for waiting.
This is necessary because the interrupt handling is devided into two parts, one
part in the kernel driver and one part in your user program. While the kernel
driver tells the kernel to wake up the waiting process and exits, some time
will pass until your program actually wakes up and is rescheduled---and resets
the interrupt. Since the interrupt handler of the kernel driver exited but the
interrupt pin is still driven by your device, the kernel thinks it sees
another interrupt. So there will be spurious pending interrupts before your
program regains control.

\subsubsection{Direct Memory Access}
\label{dma}

\firstpar
If you need fast data transfer between the memory in your computer and your
device without imposing a heavy load on the processor, you may choose to
implement direct memory access, or short \emph{DMA} in your device design.
\subpar While PSI supported DMA in the first generation, things have become
somewhat more complex over time. While on most x86 macines there was no
difference between the physical address of a block of memory in your computer
and the address of the same block as seen from the PCI bus, modern systems
sport a dedicated memory management unit for bus access and you can't just
simply tell your device the physical address of some memory block you have
allocated. You have to get a valid bus address that is assigned for a link
between a specific device and a specific block of memory.
\subpar To further complicate the matter, the block of memory, or \emph{DMA
buffer}, can only be used by either the device or your program, not by both at
a time. So basically to transfer data from your device to memory in your
computer, you open both a memory region that will be used as a DMA buffer and
a config space or base address register region for the device, map the DMA
buffer to the device, tell your device to start the transfer, wait for the
transfer to finish, unmap the buffer from the device and remap it into user
space. The transfer in the other direction works accordingly, first mapping
the buffer into user space, preparing the data to be transferred, unmap the
buffer from user space and map it to the device, tell your device to start the
transfer, wait for the transfer to finish and unmap the buffer from the device.
\subpar It is important that the mapping of the DMA buffer to the device is
held only as long as needed, as DMA mappings may be limited on some systems. So
while there exists a solution to leave a buffer mapped but suspend DMA to allow
access by your program, you should consider twice before implementing a series
of transfers that way.

\parbegin
To map a buffer to a device, call \verb|PSI_mapDMA()| and pass it the handles
to the memory and the device regions, the direction of the transfer and a
pointer to an unsigned long which will hold the bus address you can pass along
to your device if the call succeeds. The direction can be
\verb|_bidirectional_|, \verb|_fromDevice_| or \verb|_toDevice_|. While it may
be tempting to always use \verb|_bidirectional_|, this is not recommended as
there may be more overhead on some systems.
You can unmap the buffer from the device by calling \verb|PSI_unmapDMA()|,
passing it the handle to the memory region that is used as DMA buffer.
\subpar Suspending DMA for a buffer that is mapped to a device to allow access
by your program is done by calling \verb|PSI_suspendDMA()|. After you are
finished, call \verb|PSI_resumeDMA()| immediately. Both functions expect a
handle to the memory region that is used as DMA buffer.

\subsubsection{Timing Issues}

\firstpar
When dealing with hardware access you will often encounter timing
specifications that have to be met. While waiting for a few microseconds
without imposing serious load on the processor can be achieved through
\verb|usleep()|, this function can't guarantee you sub-millisecond granularity
or even microsecond granularity as the name suggests. The granularity of any
function that sleeps is given by the system tick, which is in the range of a
few milliseconds.
\subpar Note that in older kenrnels there was the possibility to get realtime
privileges and thus a much finer granularity through
\verb|sched_setscheduler()| and \verb|nanosleep()|, but this functionality
seems to be dysfunctional as of kernel 2.6.

\parbegin
This leaves you with little other choice than busy waiting if you need a better
resolution than a few milliseconds. Since this will probably be done by many a
program that uses the PSI library, you can simply call \verb|PSI_delay()| and
pass it the number of nanoseconds to wait. There is some overhead, so don't
expect it to give you nanosecond granularity, but it will most likely deliver
sub-microsecond granularity on modern systems.
\subpar Beware, however, that there's still the posibility that the scheduler
chooses to suspend your program at any time and select another process for
execution, so every now and then your program will fail if you need to
guarantee timing to be within a certain margin, so be prepared for this.

\subsection{Miscellanea}

\firstpar
There are some functions exported by the PSI library which did not fit in the
above categories. Some of these are supported by the PSI tools and can be
issued from the command line.

\parbegin
You can get a textual representation of mostly any error that can occur during
the use of the PSI library by calling \verb|PSI_strerror()| and passing it the
result of the operation which failed.

\parbegin
By calling \verb|PSI_getRegionStatus()|, you can get information about a
region. Pass it a region handle and a pointer to a tRegionStatus structure.
\subpar Note that only those fields have a defined value which are valid for the
type of region you passed a handle on. For example, the \verb|bus| and
\verb|devfn| fields will be undefined for memory regions and a config space
region has neither a \verb|name| nor \verb|address|.

\parbegin
\label{hot-plugging}
You can save and restore the config space of a PCI device by calling
\verb|PSI_saveState()| and \verb|PSI_restoreState()|, respectively. Both
functions expect, of course, a handle to a config space region and a pointer to
a buffer of at least 256 bytes to hold the config space data.
\subpar If you pass \verb|PSI_restoreState()| zero as pointer to the buffer,
the base address registers and IRQ line register will be refreshed from the
internal data the kernel stores about a device and the device is re-enabled.
\subpar This is what comes closest to \emph{hot-plugging} a PCI device. As long
as the interface of your device to the PCI bus doesn't change, you can
re-program your device design on a running system and re-enable the device
without rebooting.
\subpar Note that this is not real hot-plugging, as this is only an addition to
the PCI specification and requires support from the system BIOS. There is no
easy way to re-enable your device if you dropped, changed or added any base
address registers and there will probably never be.

\parbegin
The PSI driver does a lot of house-keeping and cleans up after processes that
leave anything open. However, if a region is locked, for example, PSI can't
guess when to release it, so the region is kept. Even worse, if a process hangs
and can't be killed, PSI can't clean up. You can call \verb|PSI_cleanup()| to
force clean up. The function expects a process id to clean up after, but it
will close all regions that are currently open if you pass zero as the process
id.

\parbegin
The second generation of PSI still supports the call
\verb|PSI_getPhysAddress()|, which was used to provide DMA support. The use of
this function is strongly discouraged. Please use the new DMA layer discussed
in section \ref{dma}.

\subsection{Reference}

\firstpar
To use the PSI library, make shure the driver is installed properly, include
\verb|psi.h| and \verb|psi_error.h| in your program and link with \verb|-lpsi|.

\subsubsection{Region Handling}

\begin{verbatim}
/* open a region */
PSI_Status PSI_openRegion( tRegion * pRegion, const char * regionPath );
\end{verbatim}

\begin{verbatim}
/* size a memory region */
PSI_Status PSI_sizeRegion( tRegion region, unsigned long * pSize );
\end{verbatim}

\begin{verbatim}
/* close a region */
PSI_Status PSI_closeRegion( tRegion region );
\end{verbatim}

\begin{verbatim}
/* region locking */
PSI_Status PSI_lockRegion( tRegion region );
PSI_Status PSI_unlockRegion( tRegion region );
int PSI_checkRegionLock( tRegion region );
\end{verbatim}

\subsubsection{Region Access}

\begin{verbatim}
/* data sizes */
#define _byte_  1
#define _word_  2
#define _dword_ 4
\end{verbatim}

\begin{verbatim}
/* reading/writing from/to a region */
PSI_Status PSI_read( tRegion, unsigned long offset, int dataSize,
                     unsigned long bufferSize, void * buffer );
PSI_Status PSI_write( tRegion, unsigned long offset, int dataSize,
                      unsigned long bufferSize, void * buffer );
\end{verbatim}

\begin{verbatim}
/* mapping regions to user space */
PSI_Status PSI_mapRegion( tRegion region, void ** pPtr );
PSI_Status PSI_unmapRegion( tRegion region, void * ptr );
PSI_Status PSI_mapWindow( tRegion region, unsigned long offset,
                          unsigned long size, void ** pPtr );
PSI_Status PSI_unmapWindow( tRegion region , void * ptr, unsigned long size );
\end{verbatim}

\subsubsection{Advanced Hardware Access}

\begin{verbatim}
/* dma mappings */
PSI_Status PSI_mapDMA( tRegion buffer, tRegion device, int direction
                       unsigned long * pAddress );
PSI_Status PSI_unmapDMA( tRegion buffer );
PSI_Status PSI_suspendDMA( tRegion buffer );
PSI_Status PSI_resumeDMA( tRegion buffer );
\end{verbatim}

\begin{verbatim}
/* interrupts */
PSI_Status PSI_waitInterrupt( tRegion device );
PSI_Status PSI_flushInterrupts( tRegion device );
\end{verbatim}

\begin{verbatim}
/* delay execution for given number of nanoseconds */
PSI_Status PSI_delay( unsigned long nsecs );
\end{verbatim}

\subsubsection{Miscellanea}

\begin{verbatim}
/* textual representation of PSI_Status results */
char *PSI_strerror( PSI_Status );
\end{verbatim}

\begin{verbatim}
/* get status information about the region */
PSI_Status PSI_getRegionStatus( tRegion region , tRegionStatus * pStatus );
\end{verbatim}

\begin{verbatim}
/* save/restore state of PCI device */
PSI_Status PSI_saveState( tRegion region, void * buffer );
PSI_Status PSI_restoreState( tRegion region, void * buffer );
\end{verbatim}

\begin{verbatim}
/* cleanup after process */
PSI_Status PSI_cleanup( pid_t pid );
\end{verbatim}

\end{document}
