/* 
   PCI and Shared Memory Interface, Version 2
   
   includes
   type bool
   
   Florian Painke <florian.painke@urz.uni-hd.de>
   read the file ../LICENSE for licensing and copyright information
   
   history
   13.03.2006 initial implementation
*/

#ifndef BOOL_H_INCLUDED
#define BOOL_H_INCLUDED



#ifdef __KERNEL__

#if (LINUX_VERSION_CODE < KERNEL_VERSION( 2, 6, 20 ))
#ifndef bool
enum bool { false, true };
typedef enum bool bool;
#endif
#endif

#else

#ifndef bool
enum bool { false, true };
typedef enum bool bool;
#endif

#endif



#endif /* BOOL_H_INCLUDED */
