/* 
   PCI and Shared Memory Interface, Version 2
   
   includes
   error values
   
   Timm Morten Steinbeck <timm@kip.uni-heidelberg.de>
   Florian Painke <florian.painke@urz.uni-hd.de>
   read the file ../LICENSE for licensing and copyright information
   
   history
   04.04.2006 initial implementation
   08.06.2010 shifted IOCTLs to 0x1000; IOCTL 2 was changed to 6 by kernel :-/
*/

#ifndef PSI_IOCTL_H_INCLUDED
#define PSI_IOCTL_H_INCLUDED



/* ioctl commands */
enum {
  PSI_NONE = 0x1000,
  PSI_OPEN_MEM,
  PSI_OPEN_BIGPHYS,
  PSI_OPEN_PHYSMEM,
  PSI_OPEN_BASE,
  PSI_OPEN_CONFIG,
  PSI_SIZE_REGION,
  PSI_FIND_DEVICE,
  PSI_CLOSE_REGION,
  PSI_GET_REGION_INFO,
  PSI_RESET,
  PSI_READ,
  PSI_WRITE,
  PSI_GET_PHYS_ADDR,
  PSI_MMAP,
  PSI_LOCK_REGION,
  PSI_UNLOCK_REGION,
  PSI_SAVE_STATE,
  PSI_RESTORE_STATE,
  PSI_CLEANUP,
  PSI_CHECK_REGION_LOCK,
  PSI_WAIT_IRQ,
  PSI_FLUSH_IRQ,
  PSI_MAP_DMA,
  PSI_UNMAP_DMA,
  PSI_SUSPEND_DMA,
  PSI_RESUME_DMA,
  PSI_DELAY_NSECS,
  PSI_REMOVE_DEVICE,
  PSI_INSERT_DEVICE,
  PSI_SET_DMA_MASK,
  PSI_RELEASE_MMAP,
  N_PSI_IOCTLS
};



/* cleanup locked regions */
#define PSI_CLEANUP_ALL_LOCKED (-1ul)



typedef struct sMem {
  /* return values */
  tRegion * region;
  /* input parameters */
  char * name;
} tMem;

typedef struct sPhysmem {
  /* return values */
  tRegion * region;
  /* input parameters */
  unsigned long address;
} tPhysmem;

typedef struct sBase {
  /* return values */
  tRegion * region;
  /* input parameters */
  char bus;
  unsigned int devfn;
  short base;
} tBase;

typedef struct sConfig {
  /* return values */
  tRegion * region;
  /* input parameters */
  char bus;
  unsigned int devfn;
} tConfig;

typedef struct sSize {
  /* input parameters */
  tRegion region;
  unsigned long size;
} tSize;

typedef struct sFind {
  /* input parameters */
  unsigned short vendor;
  unsigned short device;
  unsigned short index;
  /* return values */
  char bus;
  unsigned int devfn;
} tFind;

typedef struct sRegInfo {
  /* input parameters */
  tRegion region;
  /* return values */
  tRegionStatus status;
} tRegInfo;

typedef struct sReadWrite {
  /* input parameters */
  tRegion region;
  tStepSize dataSize;
  unsigned long offset;
  unsigned long size;
  void * buffer;
} tReadWrite;

typedef struct sMMap {
  /* input parameters */
  tRegion region;
  size_t size;
  int prot;
  int flags;
  int dummy;
  off_t offset;
  /* return values */
  void * ptr;
} tMMap;

typedef struct sMUnMap {
  /* input parameters */
  tRegion region;
  void * ptr;
} tMUnMap;

typedef struct sPhysAddr {
  /* input parameters */
  const void * ptr;
  /* return values */
  void * physAddr;
  void * busAddr;
} tPhysAddr;

typedef struct sDeviceState {
  /* input parameters */
  tRegion region;
  void * buffer;
} tDeviceState;

typedef struct sMapDMA {
  /* input perameters */
  tRegion device, buffer;
  tDirection direction;
  /* return values */
  unsigned long address;
} tMapDMA;

typedef struct sSetDMAMask {
  /* input perameters */
  tRegion device;
  u_int64_t mask;
} tSetDMAMask;

typedef struct sDeviceCtrl {
  /* input parameters */
  char bus;
  unsigned int devfn;
} tDeviceCtrl;



#endif /* PSI_IOCTL_H_INCLUDED */
