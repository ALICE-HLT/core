#ifndef _PCITOOLLIB_H_
#define _PCITOOLLIB_H_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: psiToolLib.h 862 2006-02-17 18:45:24Z timm $ 
**
***************************************************************************
*/

#include "psi.h"
#include "pcitypes.h"
#include <stdio.h>

/*
** Callback function, called when a comparison that should be equal is not.
** writevalue, readvalue, and rereadvalue are pointers to the values written, 
** read, and reread a second time respectively. region is the region where the 
** error happened, offset is the offset from the beginning of the region and 
** size is the size of the data in bytes.
*/
typedef void (*PTL_CompareCallbackFunc)( void* writevalue, void* readvalue, 
					 void* rereadvalue, tRegion region, 
					 unsigned long offset, unsigned long size );
/*
** Perform a read write test on size bytes in the given region starting at 
** offset. For each test a pattern will be written to memory and each write 
** access will be read back and compared to the original value.
** It is assumed that a region will start on a 32bit boundary.
**
** test specifies the test to be performed:
** 0: A Walking Ones pattern.
** 1: A Walking Zeroes pattern.
** 2: A Full Bits pattern (alternating 0xFF and 0x00)
** 3: A Flipping Bits pattern (alternating 0x55 and 0xAA )
*/
int PTL_testRegion( tRegion region, unsigned long offset, unsigned long size, 
		    int test, unsigned loop_count, 
		    PTL_CompareCallbackFunc compareCallback );




/*
** Print out a decoded PCI configuration space to stdout.
** This function will determine wether it is a PCI device
** or bridge CSR.
*/
void PTL_print_pci_csr( PCI_CSR* csr );
/*
** Print out a decoded PCI configuration space to the specified file.
** This function will determine wether it is a PCI device
** or bridge CSR.
*/
void PTL_fprint_pci_csr( FILE* file, PCI_CSR* csr );

/*
** Print out a decoded PCI device configuration space to stdout.
*/
void PTL_print_pci_device_csr( PCI_CSR* csr );
/*
** Print out a decoded PCI device configuration space to the specified file.
*/
void PTL_fprint_pci_device_csr( FILE* file, PCI_CSR* csr );

/*
** Print out a decoded PCI bridge configuration space to stdout.
*/
void PTL_print_pci_bridge_csr( PCI_BRIDGE_CSR* csr );
/*
** Print out a decoded PCI bridge configuration space to the specified file.
*/
void PTL_fprint_pci_bridge_csr( FILE* file, PCI_BRIDGE_CSR* csr );



/*
** Read out the configuration space from a logically specified card
** (vendor-/device-ID + card index (of those IDs)).
*/
PSI_Status PTL_read_log_pci_csr( u16 vendID, u16 devID, u32 ndx, 
				 PCI_CSR* csr );

/*
** Read out the configuration space from a physically specified card
** (bus nr, slot/device nr, function nr).
*/
PSI_Status PTL_read_phys_pci_csr( u16 bus, u16 dev, u16 func, 
				  PCI_CSR* csr );

/*
** Read out the configuration space from an already opened region.
** It is the users responsibility that there is really a configuration space
** in that region. 
*/
PSI_Status PTL_read_region_pci_csr( tRegion region, unsigned long offset, 
				    PCI_CSR* csr );



/*
** Find the physical address corresponding to the specified logical one.
** In other words: Find the bus and slot of the nth card with the specified
** vendor and device id.
** If vendor or device are invalid (0xFFFF) then any device will be considered.
*/
PSI_Status PTL_find_pci_device( u16 vendor, u16 device, u32 n, 
				u16* bus, u16* dev, u16* func );

/*
** Determine the amount of physical memory available in the system.
*/
PSI_Status PTL_get_memory_size( unsigned long* memsize );

/*
** Read out the base address registers from the specified device.
*/
PSI_Status PTL_get_device_bars( u16 bus, u16 dev, u16 func, PCI_BAR bar[6] ); 

/*
** Print out the base address register addresses and sizes to stdout.
*/
void PTL_print_bar( PCI_BAR *bar );
/*
** Print out the base address register addresses and sizes to the given file.
*/
void PTL_fprint_bar( FILE* file, PCI_BAR *bar );


/*
** Assign an address to a base address register of the given device. The size 
** of the window needed will have to be determined before and stored in the 
** corresponding bar structure's size field. 
** bars already used by this device must have the address and size fields in 
** the corresponding region set properly.
** The function will determine if the bar is a memory bar and then call 
** PTL_assign_bar_mem.
** IO bars are currently not yet implemented.
*/
PSI_Status PTL_assign_bar( u16 bus, u16 dev, u16 func, PCI_BAR bar[6], 
			   short bar_nr, int use64 );

/*
** Assign an address to a memory base address register of the given device. The size 
** of the window needed will have to be determined before and stored in the 
** corresponding bar structure's size field. 
** bars already used by this device must have the address and size fields in
** the corresponding region set properly.
** This function will scan all devices on the PCI bus to determine all the used
** memory regions.
*/
PSI_Status PTL_assign_bar_mem( u16 bus, u16 dev, u16 func, PCI_BAR bar[6], 
			       short bar_nr, int use64 );


/*
** Configure the base address registers in the specified devive.
** This function will try to autodetermine wether 64 bit addresses are
** supported and then call PTL_configure_bars_sz with the appropriate
** use64 parameter.
** bar_nr specifies the base address register to be configured (0-5) or
** -1 to configure all bars.
*/
PSI_Status PTL_configure_bars( u16 bus, u16 dev, u16 func, short bar_nr ); 

/*
** Configure the base address registers in the specified devive.
** This function just calls PTL_configure_bars_sz with the use64
** flag set to false.
** bar_nr specifies the base address register to be configured (0-5) or
** -1 to configure all bars.
*/
PSI_Status PTL_configure_bars_32( u16 bus, u16 dev, u16 func, short bar_nr );

/*
** Configure the base address registers in the specified devive.
** This function just calls PTL_configure_bars_sz with the use64
** flag set to true.
** bar_nr specifies the base address register to be configured (0-5) or
** -1 to configure all bars.
*/
PSI_Status PTL_configure_bars_64( u16 bus, u16 dev, u16 func, short bar_nr );

/*
** Configure the base address registers in the specified devive.
** The use64 flag specifies wether 64 bit addresses can be used.
** bar_nr specifies the base address register to be configured (0-5) or
** -1 to configure all bars.
*/
PSI_Status PTL_configure_bars_sz( u16 bus, u16 dev, u16 func, 
				  short bar_nr, int use64 ); 


/*
** Configure all bridges passed on the way to the specified device so that
** they allow data to the specified base address register to pass through.
*/
PSI_Status PTL_configure_bridges( u16 bus, u16 dev, u16 func, PCI_BAR bar );


/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: psiToolLib.h 862 2006-02-17 18:45:24Z timm $ 
**
***************************************************************************
*/

#endif /* _PCITOOLLIB_H_ */
