#ifndef PSIAPI_H
#define PSIAPI_H

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#ifndef __KERNEL__
#include <sys/types.h>
#endif

#ifdef __cplusplus
extern "C"
{
#endif

/*
 * PSI - PCI & Shared memory Interface
 */

  typedef enum {
    _none_ = -1,
    _mem_ = 0,
    _bigphys_ = 1,
    _physmem_ = 2,
    _base_ = 3,
    _baseIO_ = 4,
    _config_ = 5
  } tRegionTyp, *pRegionTyp;

  typedef struct sRegionStatus {
    /*
     * Number of users, physical address, size, name, ...
     */
    char name[100];		/* max 99 characters allowed */
    int users;
    unsigned long address;
    char bus;
    unsigned int devfn;
    char base;			/* bus = 0..5, conf = -1 */
    tRegionTyp typ;
    unsigned long size;
  }  tRegionStatus, *pRegionStatus;

  typedef long PSI_Status;
  typedef long tRegion;

  typedef enum {
    _byte_ = 1,
    _word_ = 2,
    _dword_ = 4
  } tStepSize, *pStepSize;

  typedef enum {
    _bidirectional_,
    _toDevice_,
    _fromDevice_
  } tDirection;

/*
 * Opens a PSI region described by regionName.
 * The handle of the region is placed in the variable pointed to by pRegion.
 *
 * Possible region types and their corresponding names:
 *   Ordinary shared memory: 
 *          "/dev/psi/mem/<shm-id>"
 *          This region type needs to be sized before it can be mapped or 
 *          read/written.
 *          shm-id is a string id.
 * 
 *   Shared memory in the big physical area space: 
 *          "/dev/psi/bigphys/<shm-id>"
 *          This region type needs to be sized before it can be mapped or 
 *          read/written.
 *          shm-id is a string id.
 * 
 *   Physical memory addresses: 
 *          "/dev/psi/physmem/<phys-address>"
 *          This region type needs to be sized before it can be mapped or 
 *          read/written.
 *          phys-address is a physical memory address anywhere in the 
 *          available address space.
 * 
 *   A PCI device's base address register, the device is specified by its 
 *   physical PCI address:
 *          "/dev/psi/bus/<bus-nr>/slot/<slot/device-nr>/function/<func-nr>/base<bar-nr>"
 *          This type of region is sized automatically.
 *          bus-nr is the id of the bus the device is located in. The PCI 
 *          spec allows up to 16 busses.
 *          slot/device-nr is the location of the device on its bus, 
 *          basically the slot a card is plugged in. The PCI spec allows 
 *          32 devices on a bus.
 *          func-nr is the number of sub-function of the device. func-nr 0 
 *          must be implemented. The PCI spec allows 8 functions for each device.
 *          bar-nr is the index of the base address register to open. 
 *          This can range from 0 to 5.
 * 
 *   A PCI device's configuration space, the device is specified by its 
 *          physical PCI address:
 *          "/dev/psi/bus/<bus-nr>/slot/<slot/device-nr>/function/<func-nr>/conf"
 *          This type of region is sized automatically.
 *          bus-nr is the id of the bus the device is located in. The PCI 
 *          spec allows up to 16 busses.
 *          slot/device-nr is the location of the device on its bus, 
 *          basically the slot a card is plugged in. The PCI spec allows 
 *          32 devices on a bus.
 *          func-nr is the number of sub-function of the device. func-nr 0 
 *          must be implemented. The PCI spec allows 8 functions for each device.
 *
 *   A PCI device's base address register, the device is specified by its 
 *   logical device ID:
 *          "/dev/psi/vendor/<vendor-id>/device/<devide-id>/<device-ndx>/base<bar-nr>"
 *          This type of region is sized automatically.
 *          vendor-id is the PCI id of the device's vendor. This is a 
 *          unique 16 bit number assigned by the PCI group.
 *          device-id is the PCI id of the device. This is a unique 16 bit 
 *          number specified by the device's vendor/manufacturer.
 *          device-ndx is used if multiple devices of the same time, with 
 *          identical vendor/device id, are present in a system. It is a 
 *          zero based index to identify one of these devices. If only one 
 *          device with the given vendor/device id is present it is 0.
 *          bar-nr is the index of the base address register to open. 
 *          This can range from 0 to 5.
 *
 *   A PCI device's configuration space, the device is specified by its 
 *   logical device ID:
 *          "/dev/psi/vendor/<vendor-id>/device/<devide-id>/<device-ndx>/base<bar-ndx>"
 *          This type of region is sized automatically.
 *          vendor-id is the PCI id of the device's vendor. This is a 
 *          unique 16 bit number assigned by the PCI group.
 *          device-id is the PCI id of the device. This is a unique 16 bit 
 *          number specified by the device's vendor/manufacturer.
 *          device-ndx is used if multiple devices of the same time, with 
 *          identical vendor/device id, are present in a system. It is a 
 *          zero based index to identify one of these devices. If only one 
 *          device with the given vendor/device id is present it is 0.
 *          bar-ndx is the index of the base address register to open. 
 *          This can range from 0 to 5.
 */
  PSI_Status PSI_openRegion( tRegion * pRegion, const char * regionName );


/*
 * Specify a size for an opened region. 
 * This has to be done manually for mem, physmem, and bigphys type regions.
 * For PCI base address and configuration space regions the size is determined
 * automatically.
 * The size is located in the variable pointed to by pSize.
 * If the region given is already sized the regions size will be placed in the 
 * variable pointed to by pSize.( The size will not be changed. )
 */
  PSI_Status PSI_sizeRegion( tRegion, unsigned long *pSize );


/*
 * After using a region it has to be closed with this call.
 */
  PSI_Status PSI_closeRegion( tRegion );


/*
 * Lock/Unlock region. This prevents a region from getting released after
 * all users closed it. Needed for example to keep PCI device state.
 * PSI_checkRegionLock returns the number of locks set on the region.
 */
  PSI_Status PSI_lockRegion( tRegion region );
  PSI_Status PSI_unlockRegion( tRegion region );
  int PSI_checkRegionLock( tRegion region );


/*
 * Read/Write from/to a given region.
 * offset is the starting offset from the beginning of the region for reading/
 * writing. This must be a multiple of stepSize.
 * stepSize specifies wether the data should be read/written byte-, word-, or
 * dword-wise (1, 2, or 4 in one read/write).
 * length is the amount of data to be read/written in bytes (!!). This must be 
 * a multiple of stepSize.
 * data is a pointer to the place where the data read is to be stored or where 
 * the data written is stored.
 */
  PSI_Status PSI_read( tRegion, unsigned long offset,
		       tStepSize stepSize, unsigned long length, void* data );
  PSI_Status PSI_write( tRegion, unsigned long offset,
			tStepSize stepSize, unsigned long length, void* data );


/*
 * Map a region's memory into user space.
 * The region has to be sized before. This has to be done manually for mem, 
 * physmem and bigphys regions.
 * Only these memory regions as well as base address memory regions can be 
 * mapped at all. Base address IO regions and configuration space regions 
 * cannot be mapped.
 * mapRegion maps the region with its entire specified size. 
 * mapWindow maps only a windows in the region starting at the specified 
 * offset and with the specified size.
 */
  PSI_Status PSI_mapRegion( tRegion, void **virtaddr );
  PSI_Status PSI_unmapRegion( tRegion, void *virtaddr );
  PSI_Status PSI_mapWindow( tRegion, unsigned long offset, unsigned long size,
			    void **virtaddr );
  PSI_Status PSI_unmapWindow( tRegion, void *virtaddr, unsigned long size );


/*
 * Generic PCI DMA layer. 
 * First, you have to map a dma buffer to a device. Provide a sized 
 * memory region for buffer and either a config or a base address 
 * region for device. If mapping is successful, address will hold
 * a valid bus address you can give your PCI device.
 * After the device finished using the DMA buffer, it must be released.
 * On some architectures DMA mappings are severely limited.
 * NEVER ACCESS THE DMA BUFFER FROM WITHIN YOUR PROGRAM BEFORE UNMAPPING
 * IT FROM THE DEVICE AGAIN! DEVICE HAS EXCLUSIVE ACCESS TO A MAPPED BUFFER!
 * That said, if you have to do this, suspend the buffer from DMA for access
 * and resume DMA immediately after access.
 */
  PSI_Status PSI_setDMAMask( tRegion device, u_int64_t mask );
  PSI_Status PSI_mapDMA( tRegion buffer, tRegion device, 
			 tDirection dir, unsigned long * address );
  PSI_Status PSI_unmapDMA( tRegion buffer );
  PSI_Status PSI_suspendDMA( tRegion buffer );
  PSI_Status PSI_resumeDMA( tRegion buffer );


/*
 * Interrupt interface for PCI device. Provide a config space region.
 * PSI_waitInterrupt waits for a single interrupt on a PCI device
 * PSI_flushInterrupts flushes all pending interrupts on a PCI device
 */
  PSI_Status PSI_waitInterrupt( tRegion region );
  PSI_Status PSI_flushInterrupts( tRegion region );


/*
 * Delay execution for given number of nanoseconds. 
 * Note that nanosecond granularity can't be guaranteed. Be happy if you get
 * microsecond granularity. This won't sleep, as sleeping has even worse
 * granulartiy (especially on 2.6 kernels where you can't seem to get
 * realtime privileges.)
 */
  PSI_Status PSI_delay( unsigned long nsecs );


/*
 * Get status information about the region.
 * This will fill the status structure pointed to by stat with the 
 * appropriate values of the specified region.
 */
  PSI_Status PSI_getRegionStatus( tRegion, tRegionStatus * stat );


/*
 * Save/Restore state of PCI device. 
 * Provide a config space region and a buffer that fits the region size.
 * If buffer is NULL for restoreState, the state will be refreshed from
 * device structure.
 */
  PSI_Status PSI_saveState( tRegion config, void * buffer );
  PSI_Status PSI_restoreState( tRegion config, void * buffer );


/*
 * Remove/insert PCI device (hot-plugging support).
 */
  PSI_Status PSI_removeDevice( char bus, unsigned int devfn  );
  PSI_Status PSI_insertDevice( char bus, unsigned int devfn  );


/*
 * Cleanup after process. Set pid to 0 to cleanup all, -1 to include locked.
 */
  PSI_Status PSI_cleanup( pid_t pid );


/*
 * Return a char* to an error message string for the given status.
 */
  char *PSI_strerror( PSI_Status );


/*
 * DEPRECATED! PLEASE USE THE GENERIC PCI DMA LAYER ABOVE!
 * Converts a user space virtual address to its physical address.
 * This works only on a page (typically 4kB) granularity.
 * this may not work for all kinds of addresses.
 * virtAddr is the user space virtual address as seen in a program.
 * physAddr is a pointer to a variable where the decoded physical address
 * will be stored.
 * busAddr is a pointer to a variable where the decoded bus address
 * will be stored. (A bus address is an address with which a device on a
 * systems extension bus (e.g. PCI) can access the given memory area. For
 * PCs physical and bus addresses are typically identical, on PowerPCs 
 * they are not AFAIK.)
 * Note, you have to take care yourself, that the specified virtual address
 * actually stays at the physical location returned. (Before calling this
 * function.)
 */
  PSI_Status PSI_getPhysAddress( const void *virtAddr, void **physAddr,
				 void **busAddr );


#ifdef __cplusplus
}
#endif

#endif
