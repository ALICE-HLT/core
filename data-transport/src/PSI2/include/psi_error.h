/* 
   PCI and Shared Memory Interface, Version 2
   
   driver module
   error values
   
   Timm Morten Steinbeck <timm@kip.uni-heidelberg.de>
   Florian Painke <florian.painke@urz.uni-hd.de>
   read the file LICENSE for licensing and copyright information
   
   history
   20.03.2006 initial implementation
   13.05.2006 additional error values
*/

#ifndef PSI1_ERRNO_H_INCLUDED
#define PSI1_ERRNO_H_INCLUDED



enum {
  PSI_OK,
  PSI_NOT_IMPL,
  PSI_DEV_NOT_READY,
  PSI_INVALID_REGION,

  /* for PSI_openRegion */
  PSI_OPEN_ERROR = 100,
  PSI_INVALID_PATH,
  PSI_BUS_OOR,
  PSI_SLOT_OOR,
  PSI_FNKT_OOR,
  PSI_BASE_OOR,
  PSI_VEND_NF,
  PSI_DEV_NF,
  PSI_NMBR_NF,
  PSI_INVALID_ADDR,
  PSI_MAX_REG,
  PSI_NOT_ALIGNED,
  PSI_DEVICE_NF,
  PSI_BASE_INV_FLAGS,
  PSI_BASE_TYPE_NF,

  /* for PSI_closeRegion */
  PSI_CLOSE_ERROR = 200,

  /* for PSI_sizeRegion */
  PSI_SIZE_ERROR = 300,
  PSI_SIZE_SET,
  PSI_SIZE_2_BIG,
  PSI_N_SIZEABLE,

  /* for PSI_mapRegion */
  PSI_MAP_ERROR = 400,
  PSI_N_MAPPABLE,
  PSI_N_SIZED,
  PSI_N_MAPPED,

  /* for PSI_unmapRegion */
  PSI_UNMAP_ERROR = 500,

  /* for PSI_read & write */
  PSI_RW_ERROR = 600,
  PSI_INV_LENGTH,
  PSI_OFFSET_OOR,

  /* for DMA and interrupts*/
  PSI_DMA_ERROR = 700,
  PSI_INV_DEVICE,
  PSI_INV_BUFFER,
  PSI_WAIT_ABORTED,
  PSI_IRQ_RELEASED,
  PSI_INV_DELAY,
  PSI_INV_MASK,

  /* for conversions */
  PSI_CONVERSION_ERROR = 800,
  PSI_INV_TYPE,

  /* internal errors */
  PSI_INTERNAL_ERROR = 1000,

  /* for any pcibios errno */
  PSI_PCI_ERRNO = 1500,

  /* for any system errno */
  PSI_SYS_ERRNO = 1500
};



#endif /* PSI1_ERRNO_H_INCLUDED */
