/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#include "psilib.h"

#include <sys/ioctl.h>
#include <errno.h>

extern int inode;

PSI_Status
PSI_sizeRegion (tRegion reg, unsigned long *size)
{
  PSI_Status retval;
  int trys = 0;
  tSize arg;
  arg.region = reg;
  arg.size = *size;
  do
    {
      errno = 0;
      retval = ioctl (inode, PSI_SIZE_REGION, &arg);
    }
  while (errno == EINTR && trys++ < EINTR_RETRYS);
  *size = arg.size;
  return retval;
}
