/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#include "psilib.h"

#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>

#include <linux/pci.h>
#include <sys/ioctl.h>
#include <errno.h>

#include <sys/time.h>
#include <time.h>

extern int inode;


/* lock region */
PSI_Status PSI_lockRegion( tRegion region )
{
  PSI_Status status = PSI_OK;
  int trys = 0;

  /* lock region */
  do {
    errno = 0;
    status = ioctl( inode, PSI_LOCK_REGION, region );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );

  return status;
}

/* unlock region */
PSI_Status PSI_unlockRegion( tRegion region )
{
  PSI_Status status = PSI_OK;
  int trys = 0;

  /* unlock region */
  do {
    errno = 0;
    status = ioctl( inode, PSI_UNLOCK_REGION, region );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );

  return status;
}

/* check number of locks on region */
int PSI_checkRegionLock( tRegion region )
{
  int result = 0;
  int trys = 0;

  /* unlock region */
  do {
    errno = 0;
    result = ioctl( inode, PSI_UNLOCK_REGION, region );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );

  return result;
}



/* save state of pci device; needs config space region */
PSI_Status PSI_saveState( tRegion region, void * buffer )
{
  PSI_Status status = PSI_OK;
  tDeviceState state;
  int trys = 0;

  /* save state */
  state.region = region;
  state.buffer = buffer;
  do {
    errno = 0;
    status = ioctl( inode, PSI_SAVE_STATE, & state );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );

  return status;
}

/* restore state of pci device; needs config space region */
PSI_Status PSI_restoreState( tRegion region, void * buffer )
{
  PSI_Status status = PSI_OK;
  tDeviceState state;
  int trys = 0;

  /* restore state */
  state.region = region;
  state.buffer = buffer;
  do {
    errno = 0;
    status = ioctl( inode, PSI_RESTORE_STATE, & state );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );
  
  return status;
}



/* remove device */
PSI_Status PSI_removeDevice( char bus, unsigned int devfn  )
{
  PSI_Status status = PSI_OK;
  tDeviceCtrl device;
  int trys = 0;

  /* check if we have to open the device */
  if ( inode == -1 ) {
    do {
      errno = 0;
      inode = open( INODE, O_RDWR );
    } while ( errno == EINTR && trys++ < EINTR_RETRYS );
    trys = 0;
  }
  if ( inode == -1 ) return PSI_DEV_NOT_READY;

  /* remove device */
  device.bus = bus;
  device.devfn = devfn;
  do {
    errno = 0;
    status = ioctl( inode, PSI_REMOVE_DEVICE, & device );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );
  
  return status;
}

/* insert device */
PSI_Status PSI_insertDevice( char bus, unsigned int devfn  )
{
  PSI_Status status = PSI_OK;
  tDeviceCtrl device;
  int trys = 0;

  /* check if we have to open the device */
  if ( inode == -1 ) {
    do {
      errno = 0;
      inode = open( INODE, O_RDWR );
    } while ( errno == EINTR && trys++ < EINTR_RETRYS );
    trys = 0;
  }
  if ( inode == -1 ) return PSI_DEV_NOT_READY;

  /* remove device */
  device.bus = bus;
  device.devfn = devfn;
  do {
    errno = 0;
    status = ioctl( inode, PSI_INSERT_DEVICE, & device );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );
  
  return status;
}



/* wait for interrupt; needs config space region */
PSI_Status PSI_waitInterrupt( tRegion region )
{
  PSI_Status status = PSI_OK;
  int trys = 0;

  /* wait for interrupt */
  do {
    errno = 0;
    status = ioctl( inode, PSI_WAIT_IRQ, region );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );
  
  return status;
}

/* flush pending interrupts; needs config space region */
PSI_Status PSI_flushInterrupts( tRegion region )
{
  PSI_Status status = PSI_OK;
  int trys = 0;

  /* flush pending interrupts */
  do {
    errno = 0;
    status = ioctl( inode, PSI_FLUSH_IRQ, region );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );
  
  return status;
}



/* set DMA mask for device */
PSI_Status PSI_setDMAMask( tRegion device, u_int64_t mask )
{
  PSI_Status status = PSI_OK;
  tSetDMAMask param;
  int trys = 0;

  /* set DMA mask */
  param.device = device;
  param.mask = mask;
  do {
    errno = 0;
    status = ioctl( inode, PSI_SET_DMA_MASK, & param );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );
  
  return status;
}

/* map DMA buffer to device */
PSI_Status 
PSI_mapDMA( tRegion buffer, tRegion device, 
	    tDirection dir, unsigned long * address )
{
  PSI_Status status = PSI_OK;
  tMapDMA map;
  int trys = 0;

  /* map DMA */
  map.buffer = buffer;
  map.device = device;
  map.direction = dir;
  map.address = 0;
  do {
    errno = 0;
    status = ioctl( inode, PSI_MAP_DMA, & map );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );
  
  (* address) = map.address;
  return status;
}

/* unmap DMA buffer from device */
PSI_Status PSI_unmapDMA( tRegion buffer )
{
  PSI_Status status = PSI_OK;
  int trys = 0;

  /* unmap dma buffer */
  do {
    errno = 0;
    status = ioctl( inode, PSI_UNMAP_DMA, buffer );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );
  
  return status;
}

/* suspend DMA for buffer */
PSI_Status PSI_suspendDMA( tRegion buffer )
{
  PSI_Status status = PSI_OK;
  int trys = 0;

  /* suspend dma for buffer */
  do {
    errno = 0;
    status = ioctl( inode, PSI_SUSPEND_DMA, buffer );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );
  
  return status;
}

/* resume DMA for buffer */
PSI_Status PSI_resumeDMA( tRegion buffer )
{
  PSI_Status status = PSI_OK;
  int trys = 0;

  /* resume dma for buffer */
  do {
    errno = 0;
    status = ioctl( inode, PSI_RESUME_DMA, buffer );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );
  
  return status;
}



/* delay execution for given number of nanoseconds */
PSI_Status PSI_delay( unsigned long nsecs )
{
  PSI_Status status = PSI_OK;
  int trys = 0;

  /* check if we have to open the device */
  if ( inode == -1 ) {
    do {
      errno = 0;
      inode = open( INODE, O_RDWR );
    } while ( errno == EINTR && trys++ < EINTR_RETRYS );
    trys = 0;
  }
  if ( inode == -1 ) return PSI_DEV_NOT_READY;

  /* delay */
  do {
    errno = 0;
    status = ioctl( inode, PSI_DELAY_NSECS, nsecs );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );

  return status;
}


 
/* close regions for specific/all processes */
PSI_Status PSI_cleanup( pid_t pid )
{
  PSI_Status status = PSI_OK;
  int trys = 0;

  /* check if we have to open the device */
  if ( inode == -1 ) {
    do {
      errno = 0;
      inode = open( INODE, O_RDWR );
    } while ( errno == EINTR && trys++ < EINTR_RETRYS );
    trys = 0;
  }
  if ( inode == -1 ) return PSI_DEV_NOT_READY;

  /* restore state */
  do {
    errno = 0;
    status = ioctl( inode, PSI_CLEANUP, pid );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );

  return status;
}
