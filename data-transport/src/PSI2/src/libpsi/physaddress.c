/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#include "psilib.h"
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

//#include "ioctrl.h"

extern int inode;

PSI_Status
PSI_getPhysAddress (const void *virtAddr, void **physAddr, void **busAddr)
{
  PSI_Status retval;
  tPhysAddr addr;
  int trys = 0;

  /* perhaps we still have to open the device inode */
  if (inode == -1)
    {
      do
	{
	  errno = 0;
	  inode = open (INODE, O_RDWR);
	}
      while (errno == EINTR && trys++ < EINTR_RETRYS);
      trys = 0;
    }
  if (inode == -1)
    return PSI_DEV_NOT_READY;

  addr.ptr = virtAddr;
#ifdef DEBUG
  printf ("Virtual address: %ph\n", addr.ptr);
#endif
  do
    {
      errno = 0;
      retval = ioctl (inode, PSI_GET_PHYS_ADDR, &addr);
    }
  while (errno == EINTR && trys++ < EINTR_RETRYS);

#ifdef DEBUG
  printf ("Physical address: %ph\n", addr.physAddr);
#endif
  if (physAddr)
    *physAddr = addr.physAddr;
  if (busAddr)
    *busAddr = addr.busAddr;
  return retval;
}
