/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#include "psilib.h"

#include <linux/pci.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <errno.h>
#ifdef PSI2_USE_ASM_PAGE_H
#include <asm/page.h>
#endif // PSI2_USE_ASM_PAGE_H
#include <sys/user.h>
#ifdef DEBUG
#include <stdio.h>
#include <string.h>
#endif

extern int inode;

PSI_Status
PSI_mapRegion( tRegion reg, void ** virtaddr )
{
  tRegionStatus rs;
  PSI_Status status = PSI_OK;
  tMMap mapData;
  int trys = 0;

  /* get the size */
  status = PSI_getRegionStatus( reg, & rs );
  if ( status != PSI_OK ) return status;

  /* exit if region not sized and it is not a PCI base region
   * (for those we can determine the size) */
  if ( rs.size == 0 && rs.typ != _base_ )
    return PSI_N_SIZED;

  /* exit if region is not mappable */
  switch ( rs.typ )
  {
  case _none_:
  case _baseIO_:
  case _config_:
    return PSI_N_MAPPABLE;
  default:
    break;
  }

  /* map region */
  mapData.region = reg;
  mapData.size = rs.size;
  mapData.prot = PROT_READ | PROT_WRITE;
  mapData.flags = MAP_SHARED;
  mapData.dummy = inode;
  mapData.offset = 0;
  mapData.ptr = 0;
  do {
    errno = 0;
    status = ioctl( inode, PSI_MMAP, & mapData );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );
  if ( status != PSI_OK ) return status;

*virtaddr = mapData.ptr;
#ifdef DEBUG
  fprintf( stderr, "mapped region %lxh to %p\n", reg, * virtaddr );
#endif
  return PSI_OK;
}

PSI_Status
PSI_unmapRegion (tRegion reg, void *virtaddr)
{
  tRegionStatus rs;
  PSI_Status status = PSI_OK;
  tMUnMap unmapData;
  int trys = 0;

 
  /* get the size */
  status = PSI_getRegionStatus( reg, & rs );
  if ( status != PSI_OK ) return status;

  /* unmap region */
  unmapData.region = reg;
  unmapData.ptr = virtaddr;
  do {
    errno = 0;
    status = ioctl( inode, PSI_RELEASE_MMAP, & unmapData );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );
  if ( status != PSI_OK ) return status;

  if ( munmap( virtaddr, rs.size ) == -1 ) return PSI_SYS_ERRNO + errno;

#ifdef DEBUG
  fprintf( stderr, "unmapped region %lxh\n", reg );
#endif
  return PSI_OK;
}


PSI_Status
PSI_mapWindow (tRegion reg, unsigned long offset, unsigned long size,
	       void **virtaddr)
{
  tRegionStatus rs;
  PSI_Status status = PSI_OK;
  tMMap mapData;
  int trys = 0;

#ifdef DEBUG
  printf ("mapWindow size: %lu - offset: %lu\n", size, offset);
#endif

  if ( size == 0 ) return PSI_INV_LENGTH;
  if ( offset & ~PAGE_MASK ) return PSI_NOT_ALIGNED;

  /* get the size */
  status = PSI_getRegionStatus (reg, &rs);
  if ( status != PSI_OK ) return status;
  if ( offset + size > rs.size ) return PSI_SIZE_2_BIG;

  /* exit if region not sized */
  if ( rs.size == 0 ) return PSI_N_SIZED;

  /* exit if region is not mappable */
  switch (rs.typ)
  {
  case _none_:
  case _baseIO_:
  case _config_:
    return PSI_N_MAPPABLE;
  default:
    break;
  }

  /* map region */
  mapData.region = reg;
  mapData.size = size;
  mapData.prot = PROT_READ | PROT_WRITE;
  mapData.flags = MAP_SHARED;
  mapData.dummy = inode;
  mapData.offset = offset;
  mapData.ptr = 0;
  do {
    errno = 0;
    status = ioctl( inode, PSI_MMAP, & mapData );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );
  if ( status != PSI_OK ) return status;
  if ( mapData.ptr == MAP_FAILED ) return PSI_SYS_ERRNO + errno;

  *virtaddr = mapData.ptr;
#ifdef DEBUG
  fprintf (stderr, "mapped region %lxh to %p\n", reg, *virtaddr);
#endif
  return PSI_OK;
}

PSI_Status
PSI_unmapWindow (tRegion reg, void *virtaddr, unsigned long size)
{
  tRegionStatus rs;
  PSI_Status status = PSI_OK;
  tMUnMap unmapData;
  int trys = 0;

 
  /* get the size */
  status = PSI_getRegionStatus( reg, & rs );
  if ( status != PSI_OK ) return status;

  /* unmap region */
  unmapData.region = reg;
  unmapData.ptr = virtaddr;
  do {
    errno = 0;
    status = ioctl( inode, PSI_RELEASE_MMAP, & unmapData );
  } while ( errno == EINTR && trys++ < EINTR_RETRYS );
  if ( status != PSI_OK ) return status;

  if ( munmap( virtaddr, size ) == -1 ) return PSI_SYS_ERRNO + errno;

#ifdef DEBUG
  fprintf( stderr, "unmapped region %lxh\n", reg );
#endif
  return PSI_OK;
}
