/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#include "psilib.h"

#include <string.h>
#include <stdio.h>

#include <linux/pci.h>
#include <sys/ioctl.h>
#include <errno.h>

extern int inode;

PSI_Status
PSI_getRegionStatus( tRegion reg, pRegionStatus rs )
{
  tRegInfo regs;
  PSI_Status status = PSI_OK;
  int trys = 0;

  /* get the status */
  regs.region = reg;
  do
    {
      errno = 0;
      status = ioctl (inode, PSI_GET_REGION_INFO, &regs);
    }
  while (errno == EINTR && trys++ < EINTR_RETRYS);
  /* fill the struct */
  sprintf (rs->name, "%s", regs.status.name);
  rs->users = regs.status.users;
  rs->address = regs.status.address;
  rs->bus = regs.status.bus;
  rs->devfn = regs.status.devfn;
  rs->users = regs.status.users;
  rs->base = regs.status.base;
  rs->typ = regs.status.typ;
  rs->size = regs.status.size;

  return status;
}
