# tragets
obj-m		:= psi.o

# install directory
INSTALL_ROOT	:= /lib/modules/$(KERNEL_RELEASE)
INSTALL_DIR	:= $(INSTALL_ROOT)/kernel/drivers/misc/psi

# compiler flags
EXTRA_CFLAGS	:= ${BASE_FLAGS}

export SOURCE_DIR

# rules

install-module: $(INSTALL_DIR)
	@cp $(BUILD_DIR)/psi.ko $(INSTALL_DIR)
	@cd $(INSTALL_ROOT); \
	 grep -v "/psi.ko:" modules.dep > modules.tmp; \
	 echo "$(INSTALL_DIR)/psi.ko:" >> modules.tmp; \
	 cp modules.dep /tmp/modules.dep.`date +%Y-%m-%d_%H-%M-%S`; \
	 mv -f modules.tmp modules.dep

uninstall:
	@cd $(INSTALL_ROOT); \
	 rm -f `grep "/psi.ko:" modules.dep | sed -nr "s/(.*):.*/\1/p"`; \
	 grep -v "/psi.ko:" modules.dep > modules.tmp; \
	 cp modules.dep /tmp/modules.dep.`date +%Y-%m-%d_%H-%M-%S`; \
	 mv -f modules.tmp modules.dep
	@rm -rf $(INSTALL_DIR)

psi-build: psi-pre-build
	@$(MAKE) -C $(KERNEL_DIR) M=$(BUILD_DIR) modules
	@cp $(BUILD_DIR)/psi.ko $(MODULE_DIR)

psi-pre-build:
	@cd $(BUILD_DIR); \
	 ln -fs $(SOURCE_DIR)/Makefile; \
	 for LINK in $(SOURCE_DIR)/*.c $(SOURCE_DIR)/*.h; do \
	   ln -sf $$LINK; \
	 done

