/* 
   PCI and Shared Memory Interface, Version 2
    
   driver module
   print macro

   Florian Painke <florian.painke@urz.uni-hd.de>
   read the file LICENSE for licensing and copyright information

   history
   14.03.2006 initial implementation
*/

#ifndef PRINT_H_INCLUDED
#define PRINT_H_INCLUDED

#include "module.h"



/* print message */
#ifdef PRINT
#undef PRINT
#endif
#define PRINT(msg...) printk( KERN_INFO MODULE_NAME ": " msg )



#endif /* PRINT_H_INCLUDED */
