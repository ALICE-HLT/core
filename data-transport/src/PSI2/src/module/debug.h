/* 
   PCI and Shared Memory Interface, Version 2
   
   driver module
   debugging macros
   
   Florian Painke <florian.painke@urz.uni-hd.de>
   read the file LICENSE for licensing and copyright information
   
   history
   14.03.2006 initial implementation
   17.07.2006 debug register
*/

#ifndef DEBUG_H_INCLUDED
#define DEBUG_H_INCLUDED

#include <asm/io.h>

#include "module.h"



#ifdef DEBUG

/* print debug message */
#define DPRINT(msg...) printk( KERN_INFO MODULE_NAME ": --- " msg )

/* assert condition */
#define ASSERT(cond) if ( cond ) ; else \
    printk( KERN_WARNING MODULE_NAME \
	    ": *** asssert failed (" #cond ", %s line %d)\n",	\
	    __FILE__, __LINE__ )

/* debug register */
#define DEBUG_REGISTER 0x334

/* enter/exit function */
#define ENTER(tag) outl( 0x100 | ((unsigned char) (tag)), DEBUG_REGISTER )
#define EXIT(tag) outl( 0x200 | ((unsigned char) (tag)), DEBUG_REGISTER )

#else /* ! DEBUG */

#define DPRINT(msg...)
#define ASSERT(cond)
#define ENTER(tag)
#define EXIT(tag)

#endif /* DEBUG */



#endif /* DEBUG_H_INCLUDED */
