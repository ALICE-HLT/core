/* 
   PCI and Shared Memory Interface, Version 2
   
   driver module
   region handling
   
   Florian Painke <florian.painke@urz.uni-hd.de>
   read the file LICENSE for licensing and copyright information
   
   history
   13.03.2006 initial implementation
   15.03.2006 memory regions should work
   21.03.2006 fixed mapping, added read/write and pci stuff
   22.03.2006 splitted up read/write into seperate functions
   27.03.2006 modifications for kernel version 2.4
   11.04.2006 added buffer to config space region in kernel version 2.4
   25.04.2006 fixed read/write from/to memory base address code
              fixed put/get_user use
   26.04.2006 added detection of bigphys area patch from kernel config
              added region verification, checked error handling
   27.04.2006 complete rewrite of locking
   28.04.2006 added interrupt handling
   11.05.2006 first attempt at doing something useful with physical memory
   12.05.2006 added dma layer
   13.05.2006 error handling changed, using PSI_* error values
   29.06.2006 using tgid instead of pid to allow sharing region handles
   	      between threads of a single process
   17.07.1006 send function tag to debug register for debugging race conditions
   30.05.2007 changed message to debug level in _psi_find_region
   31.05.2007 unmap dma for memory regions in _psi_release_resources
              changed message to debug level in _psi_find_user
   12.06.2007 added psi_set_dma_mask and psi_cleanup_regions_locked
              changed psi_cleanup_regions to nonlocked operation
   25.07.2007 fixed error handling in psi_set_dma_mask
   14.02.2008 added psi_release_region_map
   03.03.2008 fixed some kernel version 2.6.9 and 2.6.18 related issues
   06.04.2010 updated for kernel 2.6.24 and up
*/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/rwsem.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/pci.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/list.h>
#include <asm/io.h>
#include <asm/atomic.h>
#include <asm/string.h>
#include <asm/uaccess.h>

#if (LINUX_VERSION_CODE <= KERNEL_VERSION( 2, 6, 26))
#include <asm/semaphore.h>
#else
#include <linux/semaphore.h>
#endif

#include "config.h"

#if defined ( CONFIG_BIGPHYS_AREA ) && (CONFIG_BIGPHYS_AREA == 1)
#include <linux/bigphysarea.h>
#endif

#include <bool.h>
#include <psi.h>
#include <psi_error.h>

#include "print.h"
#include "debug.h"
#include "device.h"
#include "regions.h"



#if (LINUX_VERSION_CODE <= KERNEL_VERSION( 2, 5, 0))

/* older 2.4 kernels don't know about irq handler return values */
#ifndef IRQ_HANDLED
typedef void irqreturn_t;
#define IRQ_NONE
#define IRQ_HANDLED
#define IRQ_RETVAL( x )
#endif

/* older 2.4 kernels don't know about list_for_each_entry_safe 
   this is from 2.6 kernel headers */
#ifndef list_for_each_entry_safe
#define list_for_each_entry_safe( pos, n, head, member ) \
  for ( pos = list_entry( (head)->next, typeof ( * pos ), member ), \
        n = list_entry( pos->member.next, typeof ( * pos ), member ); \
        & pos->member != head; \
        pos = n, n = list_entry( n->member.next, typeof ( * n ), member ) )
#endif

/* older 2.4 kernels don't know about pci_name */
#if (LINUX_VERSION_CODE < KERNEL_VERSION( 2, 4, 28 ))
#define pci_name( dev ) (dev->slot_name)
#endif

/* 2.4 kernels don't know about dma mappings */
#if (LINUX_VERSION_CODE < KERNEL_VERSION( 2, 6, 0 ))
#define dma_map_single( dev, buffer, size, dir ) virt_to_bus( buffer )
#define dma_unmap_single( dev, addr, size, dir )
#define dma_sync_single_for_cpu( dev, addr, size, dir )
#define dma_sync_single_for_device( dev, addr, size, dir )
#endif

#endif


/* function tags */
#ifdef DEBUG
enum {
  TAG_NONE,
  PSI_CLEANUP_REGIONS,
  PSI_CLEANUP_REGIONS_NONLOCKED,
  PSI_CLEANUP_REGIONS_AFTER_PROCESS,
  PSI_GET_REGION,
  PSI_RELEASE_REGION,
  PSI_RESIZE_REGION,
  PSI_MAP_REGION,
  PSI_RELEASE_REGION_MAP,
  PSI_READ_REGION,
  PSI_WRITE_REGION,
  PSI_SET_DMA_MASK,
  PSI_MAP_DMA,
  PSI_UNMAP_DMA,
  PSI_LOCK_DMA,
  PSI_UNLOCK_DMA,
  PSI_LOCK_REGION,
  PSI_UNLOCK_REGION,
  PSI_CHECK_REGION_LOCK,
  PSI_WAIT_IRQ,
  PSI_FLUSH_IRQ,
  PSI_VIRTUAL_TO_REGION,
  PSI_GET_REGION_DESC,
  PSI_GET_REGION_SIZE,
  PSI_GET_REGION_USERS,
  PSI_GET_REGION_REFERENCES,
  PSI_GET_REGION_TOTAL_REFERENCES,
  PSI_REGION_TO_DEVICE,
  PSI_CLEANUP_REGIONS_FOR_DEVICE,
  PSI_REGION_TO_PHYSICAL_ADDRESS,
  PSI_PHYSICAL_ADDRESS_TO_REGION,
  PSI_REGION_TO_BUS_ADDRESS,
  PSI_BUS_ADDRESS_TO_REGION
};
#endif



/* region list and global r/w lock */
static LIST_HEAD( _psi_regions );
static DECLARE_RWSEM( _psi_regions_lock );

/* region */
struct psi_region {
  /* list */
  struct list_head node;

  /* region description */
  struct psi_region_desc desc;
  size_t size;

  /* handles */
  void * ptr;
  struct pci_dev * device;

  /* dma */
  unsigned long address;
  int dma_dir;
  bool dma_lock;
  
  /* interrupt handling */
  struct semaphore irq_sem;
  unsigned int irq;
  atomic_t waiting;

  /* users */
  unsigned int keep;
  struct list_head users;
};

/* user */
struct _psi_user {
  struct list_head node;
  pid_t pid;
  unsigned int ref_count, map_count;
  void * maps[MAX_MAPS];
};



/* find matching region for descriptor */
static struct psi_region * _psi_find_region( struct psi_region_desc * desc )
{
  struct psi_region * region;

  DPRINT( "_psi_find_region()\n" );
  ASSERT( desc != 0 );

  list_for_each_entry( region, & _psi_regions, node ) {
    if ( region->desc.type == desc->type ) {
      switch ( desc->type ) {
      case PSI_REGION_MEMORY:
	if ( ! strncmp( region->desc.id.memory.name, desc->id.memory.name,
			MAX_REGION_NAME_LEN ) ) {
	  if ( desc->id.memory.subtype == PSI_MEMORY_ANY )
	    return region;
	  else if ( region->desc.id.memory.subtype == desc->id.memory.subtype )
	    return region;
	}
	break;

      case PSI_REGION_PHYSICAL:
	if ( region->desc.id.physical == desc->id.physical )
	  return region;
	break;

      case PSI_REGION_CONFIG:
	if ( region->desc.id.config.bus == desc->id.config.bus &&
	     region->desc.id.config.devfn == desc->id.config.devfn )
	  return region;
	break;

      case PSI_REGION_BASE:
	if ( region->desc.id.base.bus == desc->id.base.bus &&
	     region->desc.id.base.devfn == desc->id.base.devfn &&
	     region->desc.id.base.index == desc->id.base.index ) {
	  if ( desc->id.base.subtype == PSI_BASE_ANY )
	    return region;
	  else if ( region->desc.id.base.subtype == desc->id.base.subtype )
	    return region;
	}
	break;
      }
    }
  }

  DPRINT( "no matching region found\n" );

  return 0;
}



/* find user */
static struct _psi_user * 
_psi_find_user( struct psi_region * region, pid_t pid )
{
  struct _psi_user * user;

  DPRINT( "_psi_find_user()\n" );
  ASSERT( region != 0 );

  list_for_each_entry( user, & region->users, node )
    if ( user->pid == pid )
      return user;

  DPRINT( "no matching user found\n" );

  return 0;
}

/* create new user and add it to the user list of the region */
static struct _psi_user * 
_psi_new_user( struct psi_region * region, pid_t pid )
{
  struct _psi_user * user;

  DPRINT( "_psi_new_user()\n" );
  ASSERT( region != 0 );

  /* allocate new user */
  user = kmalloc( sizeof ( struct _psi_user ), GFP_KERNEL );
  if ( ! user ) {
    PRINT( "not enough memory to create user structure\n" );
    return 0;
  }

  /* initialise user */
  INIT_LIST_HEAD( & user->node );
  user->pid = pid;
  user->ref_count = 1;
  user->map_count = 0;

  return user;
}



/* verify region */
static struct _psi_user * _psi_verify_region( struct psi_region * region )
{
  struct psi_region * tr;
  struct _psi_user * tu;
  pid_t pid = current->tgid;

  DPRINT( "_psi_verify_region()\n" );
  
  /* find region in region list */
  list_for_each_entry( tr, & _psi_regions, node )
    if ( region == tr ) {
      if ( (tu = _psi_find_user( tr, pid )) ) return tu;
      break;
    }

  PRINT( "region %ph is not a valid region\n", region );

  return 0;
}



/* interrupt handler */
#if (LINUX_VERSION_CODE < KERNEL_VERSION( 2, 6, 20 ))
static irqreturn_t 
_psi_irq_handler( int irq, void * dev_id, struct pt_regs * regs )
#else
static irqreturn_t 
_psi_irq_handler( int irq, void * dev_id )
#endif
{
  struct psi_region * region = dev_id;

  /* up irq semaphore */
  if ( region && irq == region->irq ) up( & region->irq_sem );

  return IRQ_HANDLED;
}



/* initialise resources */
static int
_psi_init_resources( struct psi_region * region, 
		     struct psi_region_desc * desc )
{
  struct pci_dev * device;
  int index, error;

  DPRINT( "_psi_init_resources()\n" );
  ASSERT( region != 0 && desc != 0 );

  region->desc.type = desc->type;
  switch ( desc->type ) {
  case PSI_REGION_MEMORY:
    region->desc.id.memory.subtype = desc->id.memory.subtype;
    strncpy( region->desc.id.memory.name, desc->id.memory.name, 
	     MAX_REGION_NAME_LEN );
    break;
    
  case PSI_REGION_PHYSICAL:
    region->desc.id.physical = desc->id.physical;
    break;
    
  case PSI_REGION_CONFIG:
    region->desc.id.config.bus = desc->id.config.bus;
    region->desc.id.config.devfn = desc->id.config.devfn;

    /* get pci device */
#if (LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 19 ))
    device = 
      pci_get_bus_and_slot( desc->id.config.bus, desc->id.config.devfn );
#else /* LINUX_VERSION_CODE < KERNEL_VERSION( 2, 6, 19 ) */
    device = pci_find_slot( desc->id.config.bus, desc->id.config.devfn );
#if (LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 0 ))
    if ( device ) device = pci_dev_get( device );
#endif /* LINUX_VERSION_CODE >= KERNEL_VERSION_CODE( 2, 6, 0 ) */
#endif /* LINUX_VERSION_CODE < KERNEL_VERSION( 2, 6, 19 ) */
    if ( ! device ) {
      PRINT( "no device found at %xh:%xh\n", 
	     desc->id.config.bus, desc->id.config.devfn );
      return PSI_DEVICE_NF;
    }
    region->device = device;
    region->size = CONFIG_SPACE_SIZE;
    
    /* check if device is associated with some driver already */
    if ( device->driver )
      PRINT( "warning: device %s is already used by a driver\n", 
	     pci_name( device ) );

    /* else install interrupt handler */
    else {
      DPRINT( "requesting irq %d for device %s\n", 
	      device->irq, pci_name( device ) );

      /* request irq */
      error = 
	request_irq( device->irq, _psi_irq_handler, 0, DEVICE_NAME, region );
      if ( error ) 
	PRINT( "failed to install irq %d handler for device %s\n",
	       device->irq, pci_name( device ) );
      else region->irq = device->irq;
    }
    break;
    
  case PSI_REGION_BASE:
    index = desc->id.base.index;
    region->desc.id.base.bus = desc->id.base.bus;
    region->desc.id.base.devfn = desc->id.base.devfn;
    region->desc.id.base.index = index;
    
    /* get pci device */
#if (LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 19 ))
    device = 
      pci_get_bus_and_slot( desc->id.base.bus, desc->id.base.devfn );
#else /* LINUX_VERSION_CODE < KERNEL_VERSION( 2, 6, 19 ) */
    device = pci_find_slot( desc->id.base.bus, desc->id.base.devfn );
#if (LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 0 ))
    if ( device ) device = pci_dev_get( device );
#endif /* LINUX_VERSION_CODE >= KERNEL_VERSION_CODE( 2, 6, 0 ) */
#endif /* LINUX_VERSION_CODE < KERNEL_VERSION( 2, 6, 19 ) */
    if ( ! device ) {
      PRINT( "no device found at %xh:%xh\n", 
	     desc->id.base.bus, desc->id.base.devfn );
      return PSI_DEVICE_NF;
    }
    region->device = device;
    
    /* get size */
    if ( index >= DEVICE_COUNT_RESOURCE ) {
      PRINT( "resource index %d out of range\n", desc->id.base.index );
      return PSI_BASE_OOR;
    }
    region->size = 
      device->resource[index].end - device->resource[index].start;
    
    /* get subtype */
    if ( device->resource[index].flags & IORESOURCE_MEM )
      region->desc.id.base.subtype = PSI_BASE_MEMORY;
    else if ( device->resource[index].flags & IORESOURCE_IO )
      region->desc.id.base.subtype = PSI_BASE_IO;
    else {
      PRINT( "invalid resource flags %lxh\n", device->resource[index].flags );
      return PSI_BASE_INV_FLAGS;
    }
    if ( desc->id.base.subtype != PSI_BASE_ANY && 
	 region->desc.id.base.subtype != desc->id.base.subtype ) {
      PRINT( "resource type %d doesn't match\n", 
	     region->desc.id.base.subtype );
      return PSI_BASE_TYPE_NF;
    }

    /* check if device is associated with some driver already */
    if ( device->driver )
      PRINT( "warning: device %s is already used by a driver\n", 
	     pci_name( device ) );
    break;
  }
  
  return 0;
}

/* release resources */
static void _psi_release_resources( struct psi_region * region )
{
  DPRINT( "_psi_release_resources()\n" );
  ASSERT( region != 0 );

  /* free allocated resources */
  switch ( region->desc.type ) {
  case PSI_REGION_MEMORY:
    if ( region->address ) {
      DPRINT( "unmapping dma buffer\n" );

      /* unmap dma buffer from device */
      dma_unmap_single( & region->device->dev, region->address, 
			region->size, region->dma_dir );
      region->device = 0;
      region->address = 0;
      region->dma_dir = 0;
      region->dma_lock = false;
    }
    if ( region->ptr ) {
      size_t temp, order = 0;

      DPRINT( "releasing memory at %ph\n", region->ptr );
      
      /* calculate shifted order */
      for ( temp = region->size; temp > 1; temp >>= 1 ) order++;
      order = (order > PAGE_SHIFT) ? (order - PAGE_SHIFT) : 0;

      /* release allocated memory */
#if defined ( CONFIG_BIGPHYS_AREA ) && (CONFIG_BIGPHYS_AREA == 1)
      if ( region->desc.id.memory.subtype == PSI_MEMORY_BIGPHYSAREA )
	bigphysarea_free_pages( region->ptr );
      else free_pages( (unsigned long) region->ptr, order );
#else
      free_pages( (unsigned long) region->ptr, order );
#endif
      region->ptr = 0;
      region->size = 0;
    }
    break;

  case PSI_REGION_PHYSICAL:
    if ( region->address ) {
      DPRINT( "unmapping dma buffer\n" );

      /* unmap dma buffer from device */
      dma_unmap_single( & region->device->dev, region->address, 
			region->size, region->dma_dir );
      region->device = 0;
      region->address = 0;
      region->dma_dir = 0;
      region->dma_lock = false;
    }
    if ( region->ptr ) {
      DPRINT( "releasing physical memory at %lxh\n", 
	      region->desc.id.physical );

      /* unmap physical memory */
      iounmap( region->ptr );
      region->ptr = 0;
      region->size = 0;
    }
    break;
    
  case PSI_REGION_CONFIG:
    if ( ! region->device ) break;
    
    /* remove intrupt handler */
    if ( region->irq ) {
      int n;

      DPRINT( "releasing irq %d\n", region->irq );

      free_irq( region->irq, region );
      region->irq = 0;
      n = atomic_read( & region->waiting );

      /* wake up any waiting processes */
      while ( n > 0 ) {
	up( & region->irq_sem );
	n--;
      }

      /* wait for processes to wake up */
      while ( atomic_read( & region->waiting ) > 0 ) schedule();
    }

    /* release device */
#if (LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 0 ))
    pci_dev_put( region->device );
#endif /* LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 0 ) */
    break;

  case PSI_REGION_BASE:
    if ( ! region->device ) break;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 0 ))
    pci_dev_put( region->device );
#endif /* LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 0 ) */
    break;
  }
}



/* get physical address of region */
static unsigned long 
_psi_region_to_physical_address( struct psi_region * region )
{
  DPRINT( "_psi_region_to_physical_address()\n" );
  ASSERT( region != 0 );

  switch ( region->desc.type ) {
  case PSI_REGION_MEMORY:
    if ( region->ptr ) return virt_to_phys( region->ptr );
    break;

  case PSI_REGION_PHYSICAL:
    return region->desc.id.physical;

  case PSI_REGION_BASE:
    return region->device->resource[region->desc.id.base.index].start;
  }

  return 0;
}

/* get bus address of region */
static unsigned long 
_psi_region_to_bus_address( struct psi_region * region )
{
  DPRINT( "_psi_region_to_bus_address()\n" );
  ASSERT( region != 0 );

  switch ( region->desc.type ) {
  case PSI_REGION_MEMORY:
    if ( region->address ) return region->address;
    if ( region->ptr ) return virt_to_bus( region->ptr );
    break;

  case PSI_REGION_PHYSICAL:
    if ( region->address ) return region->address;
    return region->desc.id.physical;

  case PSI_REGION_BASE:
    return region->device->resource[region->desc.id.base.index].start;
  }

  return 0;
}



/* read memory */
static int 
_psi_read_memory( struct psi_region * region, size_t offset, 
		  void __user * buffer, size_t length )
{
  int error;

  /* copy memory */
  error = copy_to_user( buffer, (char *) region->ptr + offset, length );
  if ( error ) {
    PRINT( "failed to copy %lxh bytes from region %ph, offset %lxh to user\n",
           (long) length, region, (long) offset );
    return PSI_INTERNAL_ERROR;
  }

  return 0;
}

/* write memory */
static int 
_psi_write_memory( struct psi_region * region, size_t offset,
		   void __user * buffer, size_t length )
{
  int error;

  /* copy memory */
  error = copy_from_user( (char *) region->ptr + offset, buffer, length );
  if ( error ) {
    PRINT( "failed to copy %lxh bytes from user to region %ph, offset %lxh\n",
	   (long) length, region, (long) offset );
    return PSI_INTERNAL_ERROR;
  }

  return 0;
}

/* read config space */
static int 
_psi_read_config( struct psi_region * region, size_t offset,
		  void __user * buffer, size_t size, size_t count )
{
  size_t length = size * count, index;
  int error = 0;
  u8 bval;
  u16 wval;
  u32 lval;

  /* check data size */
  if ( size != 4 && size != 2 && size != 1 ) {
    PRINT( "invalid data size %ld\n", (long) size );
    return PSI_INV_LENGTH;
  }

  /* check access */
  if ( ! access_ok( VERIFY_READ, buffer, length ) ) {
    PRINT( "failed to copy %lxh bytes from config space of device %s, "
	   "offset %lxh to user\n", 
	   (long) length, pci_name( region->device ), (long) offset );
    return PSI_INTERNAL_ERROR;
  }

  /* read config space */
  for ( index = 0; index < count; ++index ) {
    switch ( size ) {
    case 4:
      error = pci_read_config_dword( region->device, 
				     offset + size * index, & lval );
      if ( ! error ) error = put_user( lval, (u32 __user *) buffer + index );
      break;

    case 2:
      error = pci_read_config_word( region->device, 
				    offset + size * index, & wval );
      if ( ! error ) error = put_user( wval, (u16 __user *) buffer + index );
      break;

    case 1:
      error = pci_read_config_byte( region->device, 
				    offset + size * index, & bval );
      if ( ! error ) error = put_user( bval, (u8 __user *) buffer + index );
      break;
    }
    if ( error ) {
      PRINT( "failed to read from config space of device %s, offset %lxh\n", 
	     pci_name( region->device ), (long) (offset + size*index) );
      return PSI_INTERNAL_ERROR;
    }
  }

  return 0;
}

/* write config space */
static int 
_psi_write_config( struct psi_region * region, size_t offset,
		   void __user * buffer, size_t size, size_t count )
{
  size_t length = size * count, index;
  int error = 0;
  u8 bval;
  u16 wval;
  u32 lval;

  /* check data size */
  if ( size != 4 && size != 2 && size != 1 ) {
    PRINT( "invalid data size %ld\n", (long) size );
    return PSI_INV_LENGTH;
  }

  /* check access */
  if ( ! access_ok( VERIFY_READ, buffer, length ) ) {
    PRINT( "failed to copy %lxh bytes from user to config space of "
	   "device %s, offset %lxh\n", 
	   (long) length, pci_name( region->device ), (long) offset );
    return PSI_INTERNAL_ERROR;
  }

  /* write config space */
  for ( index = 0; index < count; ++index ) {
    switch ( size ) {
    case 4:
      error = get_user( lval, (u32 __user *) buffer + index );
      if ( ! error ) 
	error = pci_write_config_dword( region->device, 
					offset + size * index, lval );
      break;
      
    case 2:
      error = get_user( wval, (u16 __user *) buffer + index );
      if ( ! error )
	error = pci_write_config_word( region->device, 
				       offset + size * index, wval );
      break;
      
    case 1:
      error = get_user( bval, (u8 __user *) buffer + index );
      if ( ! error )
	error = pci_write_config_byte( region->device, 
				       offset + size * index, bval );
      break;
    }
    if ( error ) {
      PRINT( "failed to write to config space of device %s, offset %lxh", 
	     pci_name( region->device ), (long) (offset + size*index) );
      return PSI_INTERNAL_ERROR;
    }
  }

  return 0;
}

/* read base address register */
static int 
_psi_read_base( struct psi_region * region, size_t offset,
		void __user * buffer, size_t size, size_t count )
{
  size_t length = size * count, index;
  unsigned long address;
  void * memory;
  int error = 0;
  u8 bval;
  u16 wval;
  u32 lval;

  /* get physical address */
  address = _psi_region_to_physical_address( region );
  if ( ! address ) return PSI_INTERNAL_ERROR;

  /* check data size */
  if ( size != 4 && size != 2 && size != 1 ) {
    PRINT( "invalid data size %ld\n", (long) size );
    return PSI_INV_LENGTH;
  }

  /* check access */
  if ( ! access_ok( VERIFY_READ, buffer, length ) ) {
    PRINT( "failed to copy %lxh bytes from base address register %d "
	   "of device %s, offset %lxh to user\n", 
	   (long) length, region->desc.id.base.index, 
	   pci_name( region->device ), (long) offset );
    return PSI_INTERNAL_ERROR;
  }

  /* map memory for memory base address register */
  if ( region->desc.id.base.subtype == PSI_BASE_MEMORY ) {
    /* map memory base address register */
    memory = ioremap( address + offset, length );
    if ( ! memory ) {
      PRINT( "failed to map %lxh bytes of memory base address register %d "
	     "of device %s, offset %lxh\n", 
	     (long) length, region->desc.id.base.index,
	     pci_name( region->device ), (long) offset );
      return PSI_INTERNAL_ERROR;
    }
  }
  else memory = 0;

  /* copy memory */
  for ( index = 0; index < count; ++index ) {
    switch ( size ) {
    case 4:
      if ( region->desc.id.base.subtype == PSI_BASE_MEMORY )
	lval = readl( (u32 *) memory + index );
      else lval = inl( address + offset );
      error = put_user( lval, (u32 __user *) buffer + index );
      break;
      
    case 2:
      if ( region->desc.id.base.subtype == PSI_BASE_MEMORY )
	wval = readw( (u16 *) memory + index );
      else wval = inw( address + offset );
      error = put_user( wval, (u16 __user *) buffer + index );
      break;
      
    case 1:
      if ( region->desc.id.base.subtype == PSI_BASE_MEMORY )
	bval = readb( (u8 *) memory + index );
      else bval = inb( address + offset );
      error = put_user( bval, (u8 __user *) buffer + index );
      break;
    }
    if ( error ) {
      PRINT( "failed to read from base address register %d of device %s, "
	     "offset %lxh\n", region->desc.id.base.index, 
	     pci_name( region->device ), (long) (offset + size*index) );
      error = PSI_INTERNAL_ERROR;
      goto fail;
    }
  }

 fail:
  /* unmap memory */
  if ( memory ) iounmap( memory );

  return error;
}

/* write base address register */
static int 
_psi_write_base( struct psi_region * region, size_t offset,
		 void __user * buffer, size_t size, size_t count )
{
  size_t length = size * count, index;
  unsigned long address;
  void * memory;
  int error = 0;
  u8 bval;
  u16 wval;
  u32 lval;

  /* get physical address */
  address = _psi_region_to_physical_address( region );
  if ( ! address ) return PSI_INTERNAL_ERROR;

  /* check data size */
  if ( size != 4 && size != 2 && size != 1 ) {
    PRINT( "invalid data size %ld\n", (long) size );
    return PSI_INV_LENGTH;
  }
  
  /* check access */
  if ( ! access_ok( VERIFY_READ, buffer, length ) ) {
    PRINT( "failed to copy %lxh bytes from user to base address register %d "
	   "of device %s, offset %lxh\n", 
	   (long) length, region->desc.id.base.index, 
	   pci_name( region->device ), (long) offset );
    return PSI_INTERNAL_ERROR;
  }

  /* map memory for memory base address register */
  if ( region->desc.id.base.subtype == PSI_BASE_MEMORY ) {
    /* map memory base address register */
    memory = ioremap( address + offset, length );
    if ( ! memory ) {
      PRINT( "failed to map %lxh bytes of memory base address register %d "
	     "of device %s, offset %lxh\n", 
	     (long) length, region->desc.id.base.index,
	     pci_name( region->device ), (long) offset );
      return PSI_INTERNAL_ERROR;
    }
  }    
  else memory = 0;

  /* copy io memory */
  for ( index = 0; index < count; ++index ) {
    switch ( size ) {
    case 4:
      error = get_user( lval, (u32 __user *) buffer + index );
      if ( ! error ) {
	if ( region->desc.id.base.subtype == PSI_BASE_MEMORY )
	  writel( lval, (u32 *) memory + index );
	else outl( lval, address + offset );
      }
      break;
      
    case 2:
      error = get_user( wval, (u16 __user *) buffer + index );
      if ( ! error ) {
	if ( region->desc.id.base.subtype == PSI_BASE_MEMORY )
	  writew( wval, (u16 *) memory + index );
	else outw( wval, address + offset );
      }
      break;
      
    case 1:
      error = get_user( bval, (u8 __user *) buffer + index );
      if ( ! error ) {
	if ( region->desc.id.base.subtype == PSI_BASE_MEMORY )
	  writeb( bval, (u8 *) memory + index );
	else outb( bval, address + offset );
      }
      break;
    }
    if ( error ) {
      PRINT( "failed write to base address register %d of device %s, "
	     "offset %lxh\n", region->desc.id.base.index, 
	     pci_name( region->device ), (long) (offset + size*index) );
      error = PSI_INTERNAL_ERROR;
      goto fail;
    }
  }

 fail:
  /* unmap memory */
  if ( memory ) iounmap( memory );

  return 0;
}



#if (LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 24 ))

/* page fault handler */
static int 
_psi_map_nopage( struct vm_area_struct * vma, struct vm_fault * vmf )
{
  /* check if page is valid */
  if ( ! pfn_valid( vmf->pgoff ) ) {
    return  VM_FAULT_SIGBUS;
  }

  /* get page */
  vmf->page = pfn_to_page( vmf->pgoff );
  get_page( vmf->page );
  return VM_FAULT_MINOR;
}

#elif (LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 0 ))

/* page fault handler */
static struct page * 
_psi_map_nopage( struct vm_area_struct * vma, 
		 unsigned long address, int * type )
{
  unsigned long frame;
  struct page * page;

  /* check if page is valid */
  frame = 
    (address - vma->vm_start + (vma->vm_pgoff << PAGE_SHIFT)) >> PAGE_SHIFT;
  if ( ! pfn_valid( frame ) ) {
    if ( type ) (* type) = NOPAGE_SIGBUS;
    return NULL;
  }

  /* get page */
  page = pfn_to_page( frame );
  get_page( page );
  if ( type ) (* type) = VM_FAULT_MINOR;

  return page;
}

#else /* LINUX_VERSION_CODE < KERNEL_VERSION( 2, 6, 0 )) */

/* page fault handler */
static struct page * 
_psi_map_nopage( struct vm_area_struct * vma, 
		 unsigned long address, int write )
{
  struct page * page;
  unsigned long physical;

  /* get page */
  physical  = address - vma->vm_start + (vma->vm_pgoff << PAGE_SHIFT);
  page = virt_to_page( phys_to_virt( physical ) );
  get_page( page );

  return page;
}

#endif /* LINUX_VERSION_CODE < KERNEL_VERSION( 2, 6, 0 )) */

/* map operations */
static struct vm_operations_struct _psi_map_ops = {
#if (LINUX_VERSION_CODE <= KERNEL_VERSION( 2, 6, 24 )) 
  .nopage = _psi_map_nopage
#else
  .fault = _psi_map_nopage
#endif
};



/* cleanup regions */
void psi_cleanup_regions_locked( void )
{
  struct psi_region * region, * tr;
  struct _psi_user * user, * tu;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 4, 20 ))
  bool locked = false;
#endif

  ENTER( PSI_CLEANUP_REGIONS_LOCKED );
  DPRINT( "psi_cleanup_regions_locked()\n" );

  /* try to lock region list */
#if (LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 4, 20 ))
  if ( ! down_write_trylock( & _psi_regions_lock ) )
    PRINT( "lock on region list ignored\n" );
  else locked = true;
#endif

  /* release all regions */
  list_for_each_entry_safe( region, tr, & _psi_regions, node ) {
    /* release all users */
    list_for_each_entry_safe( user, tu, & region->users, node ) {
      DPRINT( "releasing user %ph\n", user );

      /* release user */
      list_del( & user->node );
      kfree( user );
    }

    DPRINT( "releasing region %ph\n", region );

    /* release region */
    list_del( & region->node );
    _psi_release_resources( region );
    kfree( region );
  }

  /* release lock on region list */
#if (LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 4, 20 ))
  if ( locked ) up_write( & _psi_regions_lock );
#endif
  EXIT( PSI_CLEANUP_REGIONS );
}

/* cleanup regions */
void psi_cleanup_regions( void )
{
  struct psi_region * region, * tr;
  struct _psi_user * user, * tu;

  ENTER( PSI_CLEANUP_REGIONS );
  DPRINT( "psi_cleanup_regions()\n" );

  /* lock region list */
  down_write( & _psi_regions_lock );

  /* release all regions */
  list_for_each_entry_safe( region, tr, & _psi_regions, node ) {
    /* check if region is locked, skip it */
    if ( region->keep ) continue;

    /* release all users */
    list_for_each_entry_safe( user, tu, & region->users, node ) {
      DPRINT( "releasing user %ph\n", user );

      /* release user */
      list_del( & user->node );
      kfree( user );
    }

    DPRINT( "releasing region %ph\n", region );

    /* release region */
    list_del( & region->node );
    _psi_release_resources( region );
    kfree( region );
  }

  /* release lock on region list */
  up_write( & _psi_regions_lock );
  EXIT( PSI_CLEANUP_REGIONS );
}

/* cleanup regions after process */
void psi_cleanup_regions_after_process( pid_t pid )
{
  struct psi_region * region, * temp;
  struct _psi_user * user;

  ENTER( PSI_CLEANUP_REGIONS_AFTER_PROCESS );
  DPRINT( "psi_cleanup_regions_after_process()\n" );

  /* lock region list */
  down_write( & _psi_regions_lock );

  /* scan region list for regions opend by user */
  list_for_each_entry_safe( region, temp, & _psi_regions, node ) {
    /* find and release user */
    user = _psi_find_user( region, pid );
    if ( user ) {
      DPRINT( "releasing user %ph\n", user );

      list_del( & user->node );
      kfree( user );

      /* if necessary, release region */
      if ( list_empty( & region->users ) && ! region->keep ) {
	DPRINT( "releasing region %ph\n", region );

	list_del( & region->node );
	_psi_release_resources( region );
	kfree( region );
	region = 0;
      }
    }
  }

  /* release lock on region list */
  up_write( & _psi_regions_lock );
  EXIT( PSI_CLEANUP_REGIONS_AFTER_PROCESS );
}



/* get region */
struct psi_region * 
psi_get_region( struct psi_region_desc * desc, int * _error )
{
  struct psi_region * region;
  struct _psi_user * user;
  pid_t pid = current->tgid;
  int error;

  ENTER( PSI_GET_REGION );
  DPRINT( "psi_get_region()\n" );
  ASSERT( desc != 0 );

  /* lock region list */
  down_write( & _psi_regions_lock );

  /* find region */
  region = _psi_find_region( desc );
  if ( region ) {
    DPRINT( "using existing region %ph\n", region );

    /* find user and increment number of references */
    user = _psi_find_user( region, pid );
    if ( user ) {
      DPRINT( "using existing user %ph\n", user );
      user->ref_count++;
    }

    /* if user not found, add new one */
    else {
      DPRINT( "creating new user\n" );

      /* get new user */
      error = PSI_MAX_REG;
      user = _psi_new_user( region, pid );
      if ( ! user ) goto fail;
      DPRINT( "using new user %ph\n", user );

      list_add( & user->node, & region->users );
    }
  }

  /* if region not found, add new one */
  else {
    DPRINT( "creating new region\n" );

    /* alloc region */
    region = kmalloc( sizeof ( struct psi_region ), GFP_KERNEL );
    if ( ! region ) {
      PRINT( "not enough memory to create new region\n" );
      error = PSI_MAX_REG;
      goto fail;
    }
    DPRINT( "using new region %ph\n", region );

    /* initialise region */
    memset( region, 0, sizeof ( struct psi_region ) );
    sema_init( & region->irq_sem, 0 );
    INIT_LIST_HEAD( & region->users );
    error = _psi_init_resources( region, desc );
    if ( error ) goto fail_reg_alloced;

    DPRINT( "creating new user\n" );

    /* get new user */
    user = _psi_new_user( region, pid );
    if ( ! user ) goto fail_reg_alloced;
    DPRINT( "using new user %ph\n", user );

    list_add( & user->node, & region->users );
    list_add( & region->node, & _psi_regions );
  }

  /* release lock on region list */
  up_write( & _psi_regions_lock );

  (* _error) = 0;
  EXIT( PSI_GET_REGION );
  return region;

 fail_reg_alloced:
  /* release region */
  kfree( region );
 fail:
  /* unlock regions */
  up_write( & _psi_regions_lock );

  (* _error) = error;
  EXIT( PSI_GET_REGION );
  return 0;
}



/* release region */
int psi_release_region( struct psi_region * region )
{
  struct _psi_user * user;
  int error;

  ENTER( PSI_RELEASE_REGION );
  DPRINT( "psi_release_region()\n" );
  ASSERT( region != 0 );

  /* lock region list */
  down_write( & _psi_regions_lock );

  /* verify region and get user */
  error = PSI_INVALID_REGION;
  user = _psi_verify_region( region );
  if ( ! user ) goto fail;

  /* decrement number of references and release user if necessary */
  user->ref_count--;
  if ( ! user->ref_count ) {
    DPRINT( "releasing user %ph\n", user );
	
    list_del( & user->node );
    kfree( user );
  }
#ifdef DEBUG
  else DPRINT( "%d references left\n", user->ref_count );
#endif

  /* if there are no more users, release region */
  if ( list_empty( & region->users ) && ! region->keep ) {
    DPRINT( "releasing region %ph\n", region );

    list_del( & region->node );
    _psi_release_resources( region );
    kfree( region );
  }
#ifdef DEBUG
  else {
    int users = 0;

    /* count number of users */
    list_for_each_entry( user, & region->users, node ) users++;
    DPRINT( "%d users left\n", users );
  }
#endif

  error = 0;

 fail:
  /* release lock on region list */
  up_write( & _psi_regions_lock );
  EXIT( PSI_RELEASE_REGION );
  return error;
}



/* resize region */
int psi_resize_region( struct psi_region * region, size_t size )
{
  int error;

  ENTER( PSI_RESIZE_REGION );
  DPRINT( "psi_resize_region()\n" );
  ASSERT( region != 0 && size != 0 );

  /* lock region list */
  down_write( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( region ) ) goto fail;
  
  /* currently, we only allow size, not resize */
  error = PSI_SIZE_SET;
  if ( region->size ) {
    PRINT( "region already sized\n" );
    goto fail;
  }

  switch ( region->desc.type ) {
  case PSI_REGION_MEMORY:
    {
      size_t pages, temp, order = 0;

      /* set size to multiple of PAGE_SIZE */
      pages = (size + (PAGE_SIZE-1)) / PAGE_SIZE;
      size = pages * PAGE_SIZE;

      /* calculate shifted order */
      for ( temp = size; temp > 1; temp >>= 1 ) order++;
      order = (order > PAGE_SHIFT) ? (order - PAGE_SHIFT) : 0;

      /* allocate pages */
      error = PSI_SIZE_2_BIG;
#if defined ( CONFIG_BIGPHYS_AREA ) && (CONFIG_BIGPHYS_AREA == 1)
      /* for explicit PSI_MEMORY_KERNEL, use __get_free_pages() */
      if ( region->desc.id.memory.subtype == PSI_MEMORY_KERNEL )
	region->ptr = (void *) __get_free_pages( GFP_KERNEL, order );

      else {
	/* for PSI_MEMORY_ANY, try __get_free_pages() first */
	if ( region->desc.id.memory.subtype == PSI_MEMORY_ANY &&
	     pages < BIGPHYSAREA_PAGES_THRESHOLD )
	  region->ptr = 
	    (void *) __get_free_pages( GFP_KERNEL, order );
	else region->ptr = 0;

	/* if memory could be alloc'd by __get_free_pages(), mark as KERNEL */
	if ( region->ptr )
	  region->desc.id.memory.subtype = PSI_MEMORY_KERNEL;

	/* else try bigphysarea */
	else {
	  region->ptr = 
	    (void *) bigphysarea_alloc_pages( pages, 0, GFP_KERNEL );
	  region->desc.id.memory.subtype = PSI_MEMORY_BIGPHYSAREA;
	}
      }
#else
      region->ptr = (void *) __get_free_pages( GFP_KERNEL, order );
      region->desc.id.memory.subtype = PSI_MEMORY_KERNEL;
#endif
      if ( ! region->ptr ) {
	PRINT( "not enough memory to resize region\n" );
	goto fail;
      }

#ifdef DEBUG
      (* (u32 *) region->ptr) = 0xaffed00f;
#endif

      /* set region size */
      region->size = size;
      DPRINT( "region resized to %lxh at %ph\n", (long) size, region->ptr );
    }
    break;

  case PSI_REGION_PHYSICAL:
    /* set size to multiple of PAGE_SIZE */
    size = ((size + (PAGE_SIZE-1)) / PAGE_SIZE) * PAGE_SIZE;

    /* map physical memory */
    error = PSI_INVALID_ADDR;
    region->ptr = ioremap( region->desc.id.physical, size );
    if ( ! region->ptr ) {
      PRINT( "failed to map physical memory at %lxh\n",
             region->desc.id.physical );
      goto fail;
    }

    /* set region size */
    region->size = size;
    DPRINT( "region resized to %lxh at %lxh\n", (long) size, 
	    region->desc.id.physical );
    break;

  default:
    PRINT( "region type %d can't be resized\n", region->desc.type );
    error = PSI_N_SIZEABLE;
    goto fail;
  }

  error = 0;

 fail:
  /* release lock on region list */
  up_write( & _psi_regions_lock );
  EXIT( PSI_RESIZE_REGION );
  return error;
}



#if (LINUX_VERSION_CODE < KERNEL_VERSION( 2, 6, 10))

/* remap page range */
static inline int 
remap_pfn_range( struct vm_area_struct * vma, unsigned long virt_addr, 
		 unsigned long pgoff, unsigned long len, pgprot_t prot )
{
  return remap_page_range( vma, virt_addr, pgoff << PAGE_SHIFT, len, prot );
}

#endif

/* map region */
int psi_map_region( struct psi_region * region, struct vm_area_struct * vma )
{
  unsigned long address = vma->vm_pgoff << PAGE_SHIFT;
  unsigned long length = vma->vm_end - vma->vm_start;
  struct _psi_user * user;
  int index, error;

  ENTER( PSI_MAP_REGION );
  DPRINT( "psi_map_region()\n" );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region and get user */
  error = -EFAULT;
  user = _psi_verify_region( region );
  if ( ! user ) goto fail;
  
  /* check size */
  error = -EINVAL;
  if ( ! region->size ) goto fail;

  /* set flags */
  if ( address >= __pa( high_memory ) ) vma->vm_flags |= VM_IO;
  vma->vm_flags |= VM_RESERVED;

  switch ( region->desc.type ) {
  case PSI_REGION_MEMORY:
    /* memory is mapped through a page fault handler */
    vma->vm_ops = & _psi_map_ops;
    break;

  case PSI_REGION_PHYSICAL:
    /* physical memory is mapped directly */
    error = 
      remap_pfn_range( vma, vma->vm_start, vma->vm_pgoff, length,
		       vma->vm_page_prot );
    if ( error ) {
      error = -EAGAIN;
      PRINT( "failed to remap %lxh bytes of physical memory at %lxh\n",
	     length, address );
      goto fail;
    }
    break;
		
  case PSI_REGION_BASE:
    /* only memory bar can be mapped */
    error = -EINVAL;
    if ( region->desc.id.base.subtype != PSI_BASE_MEMORY ) {
      PRINT( "only memory base address registers can be mapped\n" );
      goto fail;
    }

    /* memory base address register is mapped directly */
    error = 
      remap_pfn_range( vma, vma->vm_start, vma->vm_pgoff, length,
		       vma->vm_page_prot );
    if ( error ) {
      error = -EAGAIN;
      PRINT( "failed to remap %lxh bytes of memory base address register %d "
	     "of device %s at %lxh\n", length, region->desc.id.base.index,
	     pci_name( region->device ), address );
      goto fail;
    }
    break;

  default:
    error = -EINVAL;
    PRINT( "region type %d can't be mapped\n", region->desc.type );
    goto fail;
  }

  /* get free map */
  error = -ENOMEM;
  index = user->map_count;
  if ( index == MAX_MAPS ) {
    PRINT( "maximum number of maps for user %ph exceeded\n", user );
    goto fail;
  }

  /* add map */
  user->maps[index] = (void *) vma->vm_start;
  user->map_count++;

  error = 0;

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );
  EXIT( PSI_MAP_REGION );
  return error;
}

/* release region map */
int psi_release_region_map( struct psi_region * region, void * ptr )
{
  struct _psi_user * user;
  int index, error;

  ENTER( PSI_RELEASE_REGION_MAP );
  DPRINT( "psi_release_region_map()\n" );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region and get user */
  error = PSI_INVALID_REGION;
  user = _psi_verify_region( region );
  if ( ! user ) goto fail;
  
  /* find map */
  error = PSI_N_MAPPED;
  index = 0;
  while ( index < user->map_count ) {
    if ( user->maps[index] == ptr ) break;
    index++;
  }
  if ( index == user->map_count ) goto fail;

  /* remove map */
  user->maps[index] = 0;
  index++;
  while ( index < user->map_count ) {
    user->maps[index-1] = user->maps[index];
    user->maps[index] = 0;
    index++;
  }
  user->map_count--;
  
  error = 0;

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );
  EXIT( PSI_RELEASE_REGION_MAP );
  return error;
}



/* read from region */
int 
psi_read_region( struct psi_region * region, off_t offset,
		 void __user * buffer, size_t size, size_t count )
{
  size_t length = size * count;
  int error;

  ENTER( PSI_READ_REGION );
  DPRINT( "psi_read_region()\n" );
  ASSERT( region != 0 && buffer != 0 && length != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( region ) ) goto fail;
  
  /* check if read is valid */
  error = PSI_OFFSET_OOR;
  if ( region->size < offset || (region->size - offset) < length ) {
    PRINT( "read request beyond region boundary\n" );
    goto fail;
  }

  /* dispatch read */
  switch ( region->desc.type ) {
  case PSI_REGION_MEMORY:
  case PSI_REGION_PHYSICAL:
    error = _psi_read_memory( region, offset, buffer, length );
    break;

  case PSI_REGION_CONFIG:
    error = _psi_read_config( region, offset, buffer, size, count );
    break;

  case PSI_REGION_BASE:
    error = _psi_read_base( region, offset, buffer, size, count );
    break;

  default:
    error = PSI_RW_ERROR;
    PRINT( "region type %d can't be read from\n", region->desc.type );
  }

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );
  EXIT( PSI_READ_REGION );
  return error;
}

/* write to region */
int 
psi_write_region( struct psi_region * region, off_t offset,
		  void __user * buffer, size_t size, size_t count )
{
  size_t length = size * count;
  int error;

  ENTER( PSI_WRITE_REGION );
  DPRINT( "psi_write_region()\n" );
  ASSERT( region != 0 && buffer != 0 && length != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( region ) ) goto fail;
  
  /* check if write is valid */
  error = PSI_OFFSET_OOR;
  if ( region->size < offset || (region->size - offset) < length ) {
    PRINT( "write request beyond region boundary\n" );
    goto fail;
  }

  /* dispatch write */
  switch ( region->desc.type ) {
  case PSI_REGION_MEMORY:
  case PSI_REGION_PHYSICAL:
    error = _psi_write_memory( region, offset, buffer, length );
    break;

  case PSI_REGION_CONFIG:
    error = _psi_write_config( region, offset, buffer, size, count );
    break;

  case PSI_REGION_BASE:
    error = _psi_write_base( region, offset, buffer, size, count );
    break;

  default:
    error = PSI_RW_ERROR;
    PRINT( "region type %d can't be written to\n", region->desc.type );
  }

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );
  EXIT( PSI_WRITE_REGION );
  return error;
}



/* set dma mask */
int psi_set_dma_mask( struct psi_region * device, u64 mask )
{
  int error;

  ENTER( PSI_SET_DMA_MASK );
  DPRINT( "psi_set_dma_mask()\n" );
  ASSERT( device != 0 && mask != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( device ) ) goto fail;

  /* check region types */
  if ( device->desc.type != PSI_REGION_CONFIG && 
       device->desc.type != PSI_REGION_BASE ) {
    PRINT( "region %ph type %d is not associated with any device\n", 
	   device, device->desc.type );
    error = PSI_INV_DEVICE;
    goto fail;
  }

  /* set dma mask */
  error = PSI_INV_MASK;
  if ( dma_set_mask( & device->device->dev, mask ) ) goto fail;

  error = 0;

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );
  EXIT( PSI_MAP_DMA );
  return error;
}

/* map dma buffer to device */
unsigned long 
psi_map_dma( struct psi_region * device, struct psi_region * buffer, 
	     int direction, int * _error )
{
  unsigned long address = 0;
  int error;

  ENTER( PSI_MAP_DMA );
  DPRINT( "psi_map_dma()\n" );
  ASSERT( device != 0 && buffer != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify regions */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( device ) ) goto fail;
  if ( ! _psi_verify_region( buffer ) ) goto fail;  

  /* check region types */
  if ( device->desc.type != PSI_REGION_CONFIG && 
       device->desc.type != PSI_REGION_BASE ) {
    PRINT( "region %ph type %d is not associated with any device\n", 
	   device, device->desc.type );
    error = PSI_INV_DEVICE;
    goto fail;
  }
  if ( (buffer->desc.type != PSI_REGION_MEMORY &&
        buffer->desc.type != PSI_REGION_PHYSICAL) || ! buffer->ptr ) {
    PRINT( "region %ph type %d is not useable as dma buffer\n",
	   buffer, buffer->desc.type );
    error = PSI_INV_BUFFER;
    goto fail;
  }

  error = 0;

  /* check if dma buffer was already mapped */
  address = buffer->address;
  if ( address ) goto fail;

  /* map dma buffer to device */
  address = 
    dma_map_single( & device->device->dev, buffer->ptr, 
		    buffer->size, direction );
  if ( ! address ) {
    PRINT( "failed to map dma region %ph to device %s\n", 
	   buffer, pci_name( device->device ) );
    error = PSI_DMA_ERROR;
    goto fail;
  }
  buffer->device = device->device;
  buffer->address = address;
  buffer->dma_dir = direction;

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );

  (* _error) = error;
  EXIT( PSI_MAP_DMA );
  return address;
}

/* unmap dma buffer from device */
int psi_unmap_dma( struct psi_region * buffer )
{
  int error;

  ENTER( PSI_UNMAP_DMA );
  DPRINT( "psi_unmap_dma()\n" );
  ASSERT( buffer != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( buffer ) ) goto fail;

  /* check region type */
  if ( (buffer->desc.type != PSI_REGION_MEMORY &&
        buffer->desc.type != PSI_REGION_PHYSICAL) || ! buffer->address ) {
    PRINT( "region %ph type %d is not used as dma buffer\n",
	   buffer, buffer->desc.type );
    error = PSI_INV_BUFFER;
    goto fail;
  }

  /* unmap dma buffer from device */
  dma_unmap_single( & buffer->device->dev, buffer->address, 
		    buffer->size, buffer->dma_dir );
  buffer->device = 0;
  buffer->address = 0;
  buffer->dma_dir = 0;
  buffer->dma_lock = false;

  error = 0;

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );
  EXIT( PSI_UNMAP_DMA );
  return error;
}

/* lock dma buffer */
int psi_lock_dma( struct psi_region * buffer )
{
  int error;

  ENTER( PSI_LOCK_DMA );
  DPRINT( "psi_lock_dma()\n" );
  ASSERT( buffer != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( buffer ) ) goto fail;

  /* check region type */
  if ( (buffer->desc.type != PSI_REGION_MEMORY &&
        buffer->desc.type != PSI_REGION_PHYSICAL) || ! buffer->address ) {
    PRINT( "region %ph type %d is not used as dma buffer\n",
	   buffer, buffer->desc.type );
    error = PSI_INV_BUFFER;
    goto fail;
  }


  /* check if dma buffer was already locked */
  error = 0;
  if ( buffer->dma_lock ) goto fail;

  /* lock dma buffer */
  dma_sync_single_for_cpu( & buffer->device->dev, buffer->address, 
			   buffer->size, buffer->dma_dir );
  buffer->dma_lock = true;

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );
  EXIT( PSI_LOCK_DMA );
  return error;
}

/* unlock dma buffer */
int psi_unlock_dma( struct psi_region * buffer )
{
  int error;

  ENTER( PSI_UNLOCK_DMA );
  DPRINT( "psi_unlock_dma()\n" );
  ASSERT( buffer != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( buffer ) ) goto fail;

  /* check region type */
  if ( (buffer->desc.type != PSI_REGION_MEMORY &&
        buffer->desc.type != PSI_REGION_PHYSICAL) || ! buffer->address ) {
    PRINT( "region %ph type %d is not used as dma buffer\n",
	   buffer, buffer->desc.type );
    error = PSI_INV_BUFFER;
    goto fail;
  }


  /* check if dma buffer is locked */
  error = 0;
  if ( ! buffer->dma_lock ) goto fail;

  /* unlock dma buffer */
  dma_sync_single_for_device( & buffer->device->dev, buffer->address, 
			      buffer->size, buffer->dma_dir );
  buffer->dma_lock = false;

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );
  EXIT( PSI_UNLOCK_DMA );
  return error;
}



/* lock region */
int psi_lock_region( struct psi_region * region )
{
  int error;

  ENTER( PSI_LOCK_REGION );
  DPRINT( "psi_lock_region()\n" );
  ASSERT( region != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( region ) ) goto fail;
  
  /* increase keep counter */
  region->keep++;
  error = 0;

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );
  EXIT( PSI_LOCK_REGION );
  return error;
}

/* unlock region */
int psi_unlock_region( struct psi_region * region )
{
  int error;

  ENTER( PSI_UNLOCK_REGION );
  DPRINT( "psi_lock_region()\n" );
  ASSERT( region != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( region ) ) goto fail;
  
  /* decrease keep counter */
  if ( region->keep ) region->keep--;

  /* if there are no more users, release region */
  if ( list_empty( & region->users ) && ! region->keep ) {
    DPRINT( "releasing region %ph\n", region );

    list_del( & region->node );
    _psi_release_resources( region );
    kfree( region );
  }

  error = 0;

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );
  EXIT( PSI_UNLOCK_REGION );
  return error;
}

/* check region lock */
int psi_check_region_lock( struct psi_region * region, int * _error )
{
  int error, result = -1;

  ENTER( PSI_CHECK_REGION_LOCK );
  DPRINT( "psi_lock_region()\n" );
  ASSERT( region != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( region ) ) goto fail;
  
  result = region->keep;
  error = 0;

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );

  (* _error) =  error;
  EXIT( PSI_CHECK_REGION_LOCK );
  return result;
}



/* wait on irq for region */
int psi_wait_irq( struct psi_region * dev )
{
  int error;

  ENTER( PSI_WAIT_IRQ );
  DPRINT( "psi_wait_irq()\n" );
  ASSERT( dev != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( dev ) ) goto fail;
  
  /* check if region is config space region and irq handler was installed */
  if ( dev->desc.type != PSI_REGION_CONFIG || ! dev->irq ) {
    PRINT( "region %ph type %d has no interrupt handler\n", 
	   dev, dev->desc.type );
    error = PSI_INV_DEVICE;
    goto fail;
  }

  /* signal waiting */
  atomic_inc( & dev->waiting );

  /* release lock on region list */
  up_read( & _psi_regions_lock );

  /* wait for irq */
  error = down_interruptible( & dev->irq_sem );
  atomic_dec( & dev->waiting );
  if ( error ) {
    PRINT( "wait for interrupt on region %ph was aborted\n", dev );
    EXIT( PSI_WAIT_IRQ );
    return PSI_WAIT_ABORTED;
  }

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( dev ) ) goto fail;

  /* check if irq handler ist still installed */
  if ( ! dev->irq ) {
    PRINT( "interrupt handler for region %ph has been released", dev );
    error = PSI_IRQ_RELEASED;
    goto fail;
  }

  error = 0;

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );
  EXIT( PSI_WAIT_IRQ );
  return error;
}

/* flush pending irqs */
int psi_flush_irq( struct psi_region * dev )
{
  int error;

  ENTER( PSI_FLUSH_IRQ );
  DPRINT( "psi_flush_irq()\n" );
  ASSERT( dev != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( dev ) ) goto fail;
  
  /* check if region is config space region and irq handler was installed */
  if ( dev->desc.type != PSI_REGION_CONFIG || ! dev->irq ) {
    PRINT( "region %ph type %d has no interrupt handler\n", 
	   dev, dev->desc.type );
    error = PSI_INV_DEVICE;
    goto fail;
  }

  /* flush irqs */
  while ( ! down_trylock( & dev->irq_sem ) ) /* no op */;
  error = 0;

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );
  EXIT( PSI_FLUSH_IRQ );
  return error;
}



/* find region matching virtual address */
struct psi_region * psi_virtual_to_region( const void * ptr )
{
  struct psi_region * region;
  struct _psi_user * user;
  pid_t pid = current->tgid;
  int index;

  ENTER( PSI_VIRTUAL_TO_REGION );
  DPRINT( "psi_virtual_to_region()\n" );

  /* lock regions list */
  down_read( & _psi_regions_lock );

  /* search region list for matching memory map */
  list_for_each_entry( region, & _psi_regions, node ) {
    user = _psi_find_user( region, pid );
    if ( user )
      /* look for matching memory map */
      for ( index = 0; index < user->map_count; ++index )
	if ( user->maps[index] == ptr ) {
	  /* release lock on region list and return region */
	  up_read( & _psi_regions_lock );
	  EXIT( PSI_VIRTUAL_TO_REGION );
	  return region;
	}
  }    

  PRINT( "no matching region found for virtual address %ph\n", ptr );

  /* release lock on region list */
  up_read( & _psi_regions_lock );
  EXIT( PSI_VIRTUAL_TO_REGION );
  return 0;
}



/* get region descriptor for region */
int 
psi_get_region_desc( struct psi_region * region, 
		     struct psi_region_desc * desc )
{
  int error;

  ENTER( PSI_GET_REGION_DESC );
  DPRINT( "psi_get_region_desc()\n" );
  ASSERT( region != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( region ) ) goto fail;
  
  /* copy region descriptor */
  memcpy( desc, & region->desc, sizeof ( struct psi_region_desc ) );
  error = 0;

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );
  EXIT( PSI_GET_REGION_DESC );
  return error;
}

/* get region size */
size_t psi_get_region_size( struct psi_region * region, int * _error )
{
  int error, result = -1;

  ENTER( PSI_GET_REGION_SIZE );
  DPRINT( "psi_get_region_size()\n" );
  ASSERT( region != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( region ) ) goto fail;
  
  result = region->size;
  error = 0;

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );

  (* _error) = error;
  EXIT( PSI_GET_REGION_SIZE );
  return result;
}

/* get number of users for region */
int psi_get_region_users( struct psi_region * region, int * _error )
{
  struct list_head * node;
  int error, result = -1;

  ENTER( PSI_GET_REGION_USERS );
  DPRINT( "psi_get_region_users()\n" );
  ASSERT( region != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( region ) ) goto fail;
  
  /* count number of users */
  list_for_each( node, & region->users ) result++;
  error = 0;

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );

  (* _error) = error;
  EXIT( PSI_GET_REGION_USERS );
  return result;
}

/* get number of references for region */
int psi_get_region_references( struct psi_region * region, int * _error )
{
  struct _psi_user * user;
  int error, result = -1;

  ENTER( PSI_GET_REGION_REFERENCES );
  DPRINT( "psi_get_region_references()\n" );
  ASSERT( region != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region and get user */
  error = PSI_INVALID_REGION;
  user = _psi_verify_region( region );
  if ( ! user ) goto fail;
  
  result = user->ref_count;
  error = 0;

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );

  (* _error) =  error;
  EXIT( PSI_GET_REGION_REFERENCES );
  return result;
}

/* get number of total references for region */
int psi_get_region_total_references( struct psi_region * region, int * _error )
{
  struct _psi_user * user;
  int error, result = -1;

  ENTER( PSI_GET_REGION_TOTAL_REFERENCES );
  DPRINT( "psi_get_region_total_references()\n" );
  ASSERT( region != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( region ) ) goto fail;
  
  list_for_each_entry( user, & region->users, node )
    result += user->ref_count;
  error = 0;
  
 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );

  (* _error) = error;
  EXIT( PSI_GET_REGION_TOTAL_REFERENCES );
  return result;
}



/* get device associated with region */
struct pci_dev * 
psi_region_to_device( struct psi_region * region, int * _error )
{
  struct pci_dev * device = 0;
  int error;

  ENTER( PSI_REGION_TO_DEVICE );
  DPRINT( "psi_region_to_device()\n" );
  ASSERT( region != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( region ) ) goto fail;

  /* get device */  
  if ( region->desc.type == PSI_REGION_CONFIG || 
       region->desc.type == PSI_REGION_BASE )
    device = region->device;
  else {
    PRINT( "region %ph type %d is not associated with any device\n", 
	   region, region->desc.type );
    error = PSI_INV_DEVICE;
    goto fail;
  }

  error = 0;

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );

  (* _error) = error;
  EXIT( PSI_REGION_TO_DEVICE );
  return device;
}

/* cleanup regions associated with device */
void psi_cleanup_regions_for_device( struct pci_dev * device )
{
  struct psi_region * region, * temp;
  struct _psi_user * user, * tu;

  ENTER( PSI_CLEANUP_REGIONS_FOR_DEVICE );
  DPRINT( "psi_cleanup_regions_for_device()\n" );

  /* try to lock region list */
  down_write( & _psi_regions_lock );

  /* scan region list for regions associated with device */
  list_for_each_entry_safe( region, temp, & _psi_regions, node ) {
    if ( (region->desc.type == PSI_REGION_CONFIG ||
	  region->desc.type == PSI_REGION_BASE) &&  
	 device == region->device ) {
      /* release all users */
      list_for_each_entry_safe( user, tu, & region->users, node ) {
	DPRINT( "releasing user %ph\n", user );

	/* release user */
	list_del( & user->node );
	kfree( user );
      }

      DPRINT( "releasing region %ph\n", region );

      /* release region */
      list_del( & region->node );
      _psi_release_resources( region );
      kfree( region );
    }
  }

  /* release lock on region list */
  up_write( & _psi_regions_lock );
  EXIT( PSI_CLEANUP_REGIONS_FOR_DEVICE );
}



/* get physical address of region */
unsigned long 
psi_region_to_physical_address( struct psi_region * region, int * _error )
{
  unsigned long address = 0;
  int error;

  ENTER( PSI_REGION_TO_PHYSICAL_ADDRESS );
  DPRINT( "psi_region_to_physical_address()\n" );
  ASSERT( region != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( region ) ) goto fail;

  /* get physical address */  
  address = _psi_region_to_physical_address( region );
  if ( ! address ) {
    PRINT( "region %ph type %d has no physical address\n", 
	   region, region->desc.type );
    error = PSI_INV_TYPE;
    goto fail;
  }

  error = 0;

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );

  (* _error) = error;
  EXIT( PSI_REGION_TO_PHYSICAL_ADDRESS );
  return address;
}

/* find region matching physical address */
struct psi_region * psi_physical_address_to_region( unsigned long address )
{
  struct psi_region * region;

  ENTER( PSI_PHYSICAL_ADDRESS_TO_REGION );
  DPRINT( "psi_physical_address_to_region()\n" );
  ASSERT( address != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* search for region matching physical address */
  list_for_each_entry( region, & _psi_regions, node ) {
    if ( address == _psi_region_to_physical_address( region ) ) {
      /* release lock and return region */
      up_read( & _psi_regions_lock );
      EXIT( PSI_PHYSICAL_ADDRESS_TO_REGION );
      return region;
    }
  }

  PRINT( "no matching region found for physical address %lxh\n", address );

  /* release lock on region list */
  up_read( & _psi_regions_lock );
  EXIT( PSI_PHYSICAL_ADDRESS_TO_REGION );
  return 0;
}

/* get bus address of region */
unsigned long 
psi_region_to_bus_address( struct psi_region * region, int * _error )
{
  unsigned long address = 0;
  int error;

  ENTER( PSI_REGION_TO_BUS_ADDRESS );
  DPRINT( "psi_region_to_bus_address()\n" );
  ASSERT( region != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* verify region */
  error = PSI_INVALID_REGION;
  if ( ! _psi_verify_region( region ) ) goto fail;

  /* get bus address */  
  address = _psi_region_to_bus_address( region );
  if ( ! address ) {
    PRINT( "region %ph type %d has no bus address\n", 
	   region, region->desc.type );
    error = PSI_INV_TYPE;
    goto fail;
  }

  error = 0;

 fail:
  /* release lock on region list */
  up_read( & _psi_regions_lock );

  (* _error) = error;
  EXIT( PSI_REGION_TO_BUS_ADDRESS );
  return address;
}

/* find region matching bus address */
struct psi_region * psi_bus_address_to_region( unsigned long address )
{
  struct psi_region * region;

  ENTER( PSI_BUS_ADDRESS_TO_REGION );
  DPRINT( "psi_bus_address_to_region()\n" );
  ASSERT( address != 0 );

  /* lock region list */
  down_read( & _psi_regions_lock );

  /* search for region matching physical address */
  list_for_each_entry( region, & _psi_regions, node ) {
    if ( address == _psi_region_to_bus_address( region ) ) {
      /* release lock and return region */
      up_read( & _psi_regions_lock );
      EXIT( PSI_BUS_ADDRESS_TO_REGION );
      return region;
    }
  }

  PRINT( "no matching region found for bus address %lxh\n", address );

  /* release lock on region list */
  up_read( & _psi_regions_lock );
  EXIT( PSI_BUS_ADDRESS_TO_REGION );
  return 0;
}
