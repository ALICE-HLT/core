include $(KERNEL_DIR)/.config

# install directory
INSTALL_ROOT	:= /lib/modules/$(KERNEL_RELEASE)
INSTALL_DIR	:= $(INSTALL_ROOT)/kernel/drivers/misc

# compiler flags
CFLAGS		:= $(BASE_FLAGS) -D__KERNEL__ -DMODULE \
		   -I$(KERNEL_DIR)/include -O
ifdef CONFIG_SMP
CFLAGS		+= -D__SMP__
endif



# rules

install-module: $(INSTALL_DIR)
	@cp $(BUILD_DIR)/psi.o $(INSTALL_DIR)
	@cd $(INSTALL_ROOT); \
	 grep -v "/psi.o:" modules.dep > modules.tmp; \
	 echo "$(INSTALL_DIR)/psi.o:" >> modules.tmp; \
	 cp modules.dep /tmp/modules.dep.`date +%Y-%m-%d_%H-%M-%S`; \
	 mv -f modules.tmp modules.dep

uninstall:
	@cd $(INSTALL_ROOT); \
	 rm -f `grep "/psi.o:" modules.dep | sed -nr "s/(.*):.*/\1/p"`; \
	 grep -v "/psi.o:" modules.dep > modules.tmp; \
	 cp modules.dep /tmp/modules.dep.`date +%Y-%m-%d_%H-%M-%S`; \
	 mv -f modules.tmp modules.dep
	@rm -f $(INSTALL_DIR)/psi.o

psi-build:
	@$(MAKE) -C $(BUILD_DIR) -f $(SOURCE_DIR)/Makefile \
	         SOURCE_DIR=$(SOURCE_DIR) psi.o
	@cp $(BUILD_DIR)/psi.o $(MODULE_DIR)

psi.o: ${psi-objs}
	$(LD) -r -o $@ $^

$(psi-objs): %.o: $(SOURCE_DIR)/%.c
	$(CC) ${CFLAGS} -c $<

