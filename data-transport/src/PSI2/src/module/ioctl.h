/* 
   PCI and Shared Memory Interface, Version 2
   
   driver module
   device ioctls
   
   Florian Painke <florian.painke@urz.uni-hd.de>
   read the file LICENSE for licensing and copyright information
   
   history
   13.03.2006 initial implementation
   15.03.2006 removed magic number on region name array
   21.03.2006 added read/write and find device
   04.04.2006 renamed psi1* to psi*, default to old style ioctl
   11.04.2006 removed cleanup
*/

#ifndef IOCTL_H_INCLUDED
#define IOCTL_H_INCLUDED



/* device ioctl */
int psi_device_ioctl( struct inode * inode, struct file * file,
		      unsigned int cmd, unsigned long arg );



#endif /* IOCTL_H_INCLUDED */
