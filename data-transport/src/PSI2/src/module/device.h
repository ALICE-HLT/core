/* 
   PCI and Shared Memory Interface, Version 2
   
   driver module
   device handling
   
   Florian Painke <florian.painke@urz.uni-hd.de>
   read the file LICENSE for licensing and copyright information
   
   history
   13.03.2006 initial implementation
*/

#ifndef DEVICE_H_INCLUDED
#define DEVICE_H_INCLUDED



/* /dev node name */
#define DEVICE_NAME "psi"



/* un-/register device */
void psi_unregister_device( void );
int psi_register_device( void );



#endif /* DEVICE_H_INCLUDED */
