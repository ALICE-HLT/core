/* 
   PCI and Shared Memory Interface, Version 2
   
   driver module
   device ioctls
   
   Florian Painke <florian.painke@urz.uni-hd.de>
   read the file LICENSE for licensing and copyright information
   
   history
   20.03.2006 initial implementation (moved over from device.c)
   21.03.2006 added read/write and find device
   22.03.2006 added memory subtype
   03.04.2006 added _psi1_device_ioctl, made everything else static, fixed
              use of put_user
   04.04.2006 renamed psi1* to psi*, added suspend/resume
   25.04.2006 added _psi_cleanup
   26.04.2006 checked error handling
   28.04.2006 added interrupt handling
   13.05.2006 error handling changed
   12.06.2007 added _psi_set_dma_mask, changed _psi_cleanup
   14.02.2008 added _psi_release_mmap
   03.03.2008 fixed some kernel version 2.6.9 related issues
   06.04.2010 updated for kernel 2.6.24 and up
*/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/rwsem.h>
#include <linux/pci.h>
#include <linux/ioport.h>
#include <linux/sched.h>
#include <asm/mman.h>
#include <asm/uaccess.h>
#include <asm/delay.h>
#include <linux/delay.h>

#include "config.h"

#include <bool.h>
#include <psi.h>
#include <psi_error.h>
#include <psi_ioctl.h>

#include "print.h"
#include "debug.h"
#include "regions.h"
#include "ioctl.h"



/* older 2.4 kernels don't know about pci_name and ndelay */
#if (LINUX_VERSION_CODE < KERNEL_VERSION( 2, 4, 28 ))
#ifndef pci_name
#define pci_name( dev ) (dev->slot_name)
#endif
#ifndef ndelay
#define ndelay( x ) udelay( 1 )
#endif
#endif

/* 2.4 kernels don't know about pci_find_bus */
#if (LINUX_VERSION_CODE < KERNEL_VERSION( 2, 6, 0 ))
static struct pci_bus * pci_find_bus( int ignored, int busnr )
{
  struct pci_bus * bus, * child;

  pci_for_each_bus( bus ) {
    if ( busnr == bus->number ) return bus;

    list_for_each_entry( child, & bus->children, node )
      if ( busnr == child->number ) return child;
  }

  return 0;
}
#endif



/* cleanup */
static int _psi_cleanup( struct file * file, unsigned long arg )
{
  DPRINT( "cleaning up\n" );

  if ( ! arg ) psi_cleanup_regions();
  else if ( arg == PSI_CLEANUP_ALL_LOCKED ) psi_cleanup_regions_locked();
  else psi_cleanup_regions_after_process( (pid_t) arg );

  return 0;
}



/* open memory region */
static int _psi_open_memory( struct file * file, unsigned long arg )
{
  struct psi_region_desc desc;
  struct psi_region * region;
  tMem param;
  int error;

  /* get argument */
  error = copy_from_user( & param, (void __user *) arg, sizeof ( tMem ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }
  desc.type = PSI_REGION_MEMORY;
  desc.id.memory.subtype = PSI_MEMORY_ANY;
  error = copy_from_user( desc.id.memory.name, param.name,
			  1 + strnlen_user( param.name, 
					    MAX_REGION_NAME_LEN ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }
  desc.id.memory.name[MAX_REGION_NAME_LEN] = '\0';
  
  DPRINT( "opening memory region %s\n", desc.id.memory.name );
  
  /* get region */
  region = psi_get_region( & desc, & error );
  if ( ! region ) return error;

  /* return region handle to user */
  error = put_user( (tRegion) region, (tRegion __user *) param.region );
  if ( error ) {
    PRINT( "failed to copy return value to user\n" );
    psi_release_region( region );
    return PSI_INTERNAL_ERROR;
  }

  return 0;
}

/* open bigphys memory region */
static int _psi_open_bigphys( struct file * file, unsigned long arg )
{
  struct psi_region_desc desc;
  struct psi_region * region;
  tMem param;
  int error;

  /* get argument */
  error = copy_from_user( & param, (void __user *) arg, sizeof ( tMem ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }
  desc.type = PSI_REGION_MEMORY;
  desc.id.memory.subtype = PSI_MEMORY_BIGPHYSAREA;
  error = copy_from_user( desc.id.memory.name, param.name,
			  1 + strnlen_user( param.name, 
					    MAX_REGION_NAME_LEN ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }
  desc.id.memory.name[MAX_REGION_NAME_LEN] = '\0';
  
  DPRINT( "opening bigphys memory region %s\n", desc.id.memory.name );
  
  /* get region */
  region = psi_get_region( & desc, & error );
  if ( ! region ) return error;

  /* return region handle to user */
  error = put_user( (tRegion) region, (tRegion __user *) param.region );
  if ( error ) {
    PRINT( "failed to copy return value to user\n" );
    psi_release_region( region );
    return PSI_INTERNAL_ERROR;
  }

  return 0;
}

/* open physical memory region */
static int _psi_open_physical( struct file * file, unsigned long arg )
{
  struct psi_region_desc desc;
  struct psi_region * region;
  int error;
  tPhysmem param;

  /* get argument */
  error = copy_from_user( & param, (void __user *) arg, sizeof ( tPhysmem ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }
  desc.type = PSI_REGION_PHYSICAL;
  desc.id.physical = param.address;

  DPRINT( "opening physical memory region at %lxh\n", desc.id.physical );

  /* get region */
  region = psi_get_region( & desc, & error );
  if ( ! region ) return error;

  /* return region handle to user */
  error = put_user( (tRegion) region, (tRegion __user *) param.region );
  if ( error ) {
    PRINT( "failed to copy return value to user\n" );
    psi_release_region( region );
    return PSI_INTERNAL_ERROR;
  }

  return 0;
}

/* open config space region */
static int _psi_open_config( struct file * file, unsigned long arg )
{
  struct psi_region_desc desc;
  struct psi_region * region;
  int error;
  tConfig param;
      
  /* get argument */
  error = copy_from_user( & param, (void __user *) arg, sizeof ( tConfig ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }
  desc.type = PSI_REGION_CONFIG;
  desc.id.config.bus = param.bus;
  desc.id.config.devfn = param.devfn;

  DPRINT( "opening config space region at %xh:%xh\n",
	  (unsigned int) desc.id.config.bus, desc.id.config.devfn );
  
  /* get region */
  region = psi_get_region( & desc, & error );
  if ( ! region ) return error;
  
  /* return region handle to user */
  error = put_user( (tRegion) region, (tRegion __user *) param.region );
  if ( error ) {
    PRINT( "failed to copy return value to user\n" );
    psi_release_region( region );
    return PSI_INTERNAL_ERROR;
  }

  return 0;
}

/* open base address register region */
static int _psi_open_base( struct file * file, unsigned long arg )
{
  struct psi_region_desc desc;
  struct psi_region * region;
  int error;
  tBase param;

  /* get argument */
  error = copy_from_user( & param, (void __user *) arg, sizeof ( tBase ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }
  desc.type = PSI_REGION_BASE;
  desc.id.base.subtype = PSI_BASE_ANY;
  desc.id.base.bus = param.bus;
  desc.id.base.devfn = param.devfn;
  desc.id.base.index = param.base;

  DPRINT( "opening pci base address region %d at %xh:%xh\n",
	  desc.id.base.index, desc.id.base.bus, desc.id.base.devfn );

  /* get region */
  region = psi_get_region( & desc, & error );
  if ( ! region ) return error;

  /* return region handle to user */
  error = put_user( (tRegion) region, (tRegion __user *) param.region );
  if ( error ) {
    PRINT( "failed to copy return value to user\n" );
    psi_release_region( region );
    return PSI_INTERNAL_ERROR;
  }

  return 0;
}



/* size region */
static int _psi_size_region( struct file * file, unsigned long arg )
{
  int error;
  tSize param;

  /* get argument */
  error = copy_from_user( & param, (void __user *) arg, sizeof ( tSize ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }

  DPRINT( "resizing region %lxh to %lxh\n", param.region, (long) param.size );
  
  /* (re)size region */
  error = psi_resize_region( (struct psi_region *) param.region, param.size );

  return error;
}



/* close region */
static int _psi_close_region( struct file * file, unsigned long arg )
{
  tRegion region;
  int error;

  /* get argument */
  error = get_user( region, (tRegion __user *) arg );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }

  DPRINT( "closing region %lxh\n", region );
    
  /* release region */
  error = psi_release_region( (struct psi_region *) region );

  return error;
}



/* lock region */
static int _psi_lock_region( struct file * file, unsigned long arg )
{
  struct psi_region * region;
  int error;

  /* get argument */
  region = (struct psi_region *) arg;

  DPRINT( "locking region %ph\n", region );

  /* lock region */
  error = psi_lock_region( region );

  return error;
}

/* unlock region */
static int _psi_unlock_region( struct file * file, unsigned long arg )
{
  struct psi_region * region;
  int error;

  /* get argument */
  region = (struct psi_region *) arg;

  DPRINT( "unlocking region %ph\n", region );

  /* unlock region */
  error = psi_unlock_region( region );

  return error;
}

/* check region lock */
static int _psi_check_region_lock( struct file * file, unsigned long arg )
{
  struct psi_region * region;
  int error, result;

  /* get argument */
  region = (struct psi_region *) arg;

  DPRINT( "checking lock for region %ph\n", region );

  /* check region lock */
  result = psi_check_region_lock( region, & error );

  return result;
}



/* read from region */
static int _psi_read_region( struct file * file, unsigned long arg )
{
  int error;
  tReadWrite param;

  /* get argument */
  error = 
    copy_from_user( & param, (void __user *) arg, sizeof ( tReadWrite ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }

  DPRINT( "reading from region %lxh\n", param.region );
  
  /* read from region */
  error = 
    psi_read_region( (struct psi_region *) param.region, param.offset, 
		     param.buffer, param.dataSize, 
		     param.size / param.dataSize );

  return error;
}

/* write to region */
static int _psi_write_region( struct file * file, unsigned long arg )
{
  int error;
  tReadWrite param;

  /* get argument */
  error = 
    copy_from_user( & param, (void __user *) arg, sizeof ( tReadWrite ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }

  DPRINT( "writing to region %lxh\n", param.region );
  
  /* write to region */
  error = 
    psi_write_region( (struct psi_region *) param.region, param.offset, 
		      param.buffer, param.dataSize, 
		      param.size / param.dataSize );

  return error;
}

/* map region */
static int _psi_map_region( struct file * file, unsigned long arg )
{
  unsigned long address;
  int error;
  tMMap param;
    
  /* get argument */
  error = copy_from_user( & param, (void __user *) arg, sizeof ( tMMap ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }

  DPRINT( "mapping region %lxh\n", param.region );

  /* get address to map */
  address = 
    psi_region_to_physical_address( (struct psi_region *) param.region, 
				    & error );
  if ( ! address ) return error;

  DPRINT( "physical address %lxh\n", address );

  /* map region */
  down_read( & current->mm->mmap_sem );
  address = do_mmap( file, 0, param.size, param.prot, param.flags,
		     address + param.offset );
  up_read( & current->mm->mmap_sem );
  if ( address >= -EINVAL ) return PSI_INTERNAL_ERROR;

  /* return address */
  param.ptr = (void *) address;
  error = copy_to_user( (void __user *) arg, & param, sizeof ( tMMap ) );
  if ( error ) {
    PRINT( "failed to copy return value to user\n" );
    return PSI_INTERNAL_ERROR;
  }

  DPRINT( "mapped region %lxh to %lxh\n", param.region, address );

  return 0;
}

/* release region map */
static int _psi_release_region_map( struct file * file, unsigned long arg )
{
  int error;
  tMUnMap param;
    
  /* get argument */
  error = copy_from_user( & param, (void __user *) arg, sizeof ( tMUnMap ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }

  DPRINT( "releasing map %lxh for region %lxh\n", param.ptr, param.region );

  /* release region map */
  error = 
    psi_release_region_map( (struct psi_region *) param.region, param.ptr );

  return error;
}

/* set dma mask */
static int _psi_set_dma_mask( struct file * file, unsigned long arg )
{
  tSetDMAMask param;
  int error;

  /* get argument */
  error = 
    copy_from_user( & param, (void __user *) arg, sizeof ( tSetDMAMask ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }

  DPRINT( "setting dma mask for device %lxh to %lxh", 
	  param.device, param.mask );
  
  /* map dma buffer to device */
  error = psi_set_dma_mask( (struct psi_region *) param.device, param.mask );

  return error;
}

/* map dma buffer to device */
static int _psi_map_dma( struct file * file, unsigned long arg )
{
  unsigned long address;
  tMapDMA param;
  int error;

  /* get argument */
  error = copy_from_user( & param, (void __user *) arg, sizeof ( tMapDMA ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }

  DPRINT( "mapping dma buffer %lxh to device %lxh, direction %d\n", 
	  param.buffer, param.device, param.direction );
  
  /* map dma buffer to device */
  address = 
    psi_map_dma( (struct psi_region *) param.device, 
		 (struct psi_region *) param.buffer, 
		 param.direction, & error );
  if ( ! address ) return error;

  /* return address */
  param.address = address;
  error = copy_to_user( (void __user *) arg, & param, sizeof ( tMapDMA ) );
  if ( error ) {
    PRINT( "failed to copy return value to user\n" );
    return PSI_INTERNAL_ERROR;
  }

  return 0;
}

/* unmap dma buffer from device */
static int _psi_unmap_dma( struct file * file, unsigned long arg )
{
  int error;
  struct psi_region * region;

  /* get argument */
  region = (struct psi_region *) arg;

  DPRINT( "unmapping dma buffer %ph\n", region );
  
  /* unmap dma buffer from device */
  error = psi_unmap_dma( region );

  return error;
}

/* suspend dma for buffer */
static int _psi_suspend_dma( struct file * file, unsigned long arg )
{
  int error;
  struct psi_region * region;

  /* get argument */
  region = (struct psi_region *) arg;

  DPRINT( "suspending dma for buffer %ph\n", region );
  
  /* lock dma buffer */
  error = psi_lock_dma( region );

  return error;
}

/* resume dma for buffer */
static int _psi_resume_dma( struct file * file, unsigned long arg )
{
  int error;
  struct psi_region * region;

  /* get argument */
  region = (struct psi_region *) arg;

  DPRINT( "resuming dma for buffer %ph\n", region );
  
  /* unlock dma buffer */
  error = psi_unlock_dma( region );

  return error;
}



/* wait for interrupt */
static int _psi_wait_irq( struct file * file, unsigned long arg )
{
  struct psi_region * region;
  int error;

  /* get argument */
  region = (struct psi_region *) arg;

  DPRINT( "wait for irq on region %ph\n", region );

  /* wait for irq */
  error = psi_wait_irq( region );

  return error;
}

/* flush pending interrupts */
static int _psi_flush_irq( struct file * file, unsigned long arg )
{
  struct psi_region * region;
  int error;

  /* get argument */
  region = (struct psi_region *) arg;

  DPRINT( "wait for irq on region %ph\n", region );

  /* flush irqs */
  error = psi_flush_irq( region );

  return error;
}



/* delay execution for given number of nanoseconds */
int _psi_delay_nsecs( struct file * file, unsigned long arg )
{
  unsigned long x;

  /* delay must be less than one second */
  if ( arg > 999999999ul ) return PSI_INV_DELAY;

  /* get number of nanoseconds and wait */
  if ( ! arg ) return 0;
  x = arg % 1000;
  if ( x ) ndelay( x );

  /* get number of microseconds and wait */
  arg /= 1000;
  if ( ! arg ) return 0;
  x = arg % 1000;
  if ( x ) udelay( x );

  /* get number of milliseconds and wait */
  arg /= 1000;
  if ( ! arg ) return 0;
  mdelay( arg );

  return 0;
}



/* save state */
static int _psi_save_state( struct file * file, unsigned long arg )
{
  struct pci_dev * device;
  tDeviceState param;
  int index, error;
  u32 val;

  /* get argument */
  error = 
    copy_from_user( & param, (void __user *) arg, sizeof ( tDeviceState ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }

  DPRINT( "save state for region %lxh\n", param.region );

  /* get device */
  device = psi_region_to_device( (struct psi_region *) param.region, & error );
  if ( ! device ) return error;

  DPRINT( "region %lxh is device %s\n", param.region, pci_name( device ) );

  /* save config space */
  for ( index = 0; index < (CONFIG_SPACE_SIZE / 4) && ! error; ++index ) {
    error = pci_read_config_dword( device, 4 * index, & val );
    if ( ! error ) 
	error = put_user( val, (u32 __user *) param.buffer + index );
  }
  if ( error ) {
    PRINT( "failed to save state for device %s\n", pci_name( device ) );
    return PSI_INTERNAL_ERROR;
  }

  return 0;
}

/* restore state */
static int _psi_restore_state( struct file * file, unsigned long arg )
{
  struct pci_dev * device;
  tDeviceState param;
  int index, error;
  u32 val;

  /* get argument */
  error = 
    copy_from_user( & param, (void __user *) arg, sizeof ( tDeviceState ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }

  DPRINT( "restore state for region %lxh\n", param.region );

  /* get device */
  device = psi_region_to_device( (struct psi_region *) param.region, & error );
  if ( ! device ) return error;

  DPRINT( "region %lxh is device %s\n", param.region, pci_name( device ) );

  /* restore config space if buffer is non-zero ... */
  if ( param.buffer ) {
    for ( index = 0; index < (CONFIG_SPACE_SIZE / 4) && ! error; ++index ) {
      error = get_user( val, (u32 __user *) param.buffer + index );
      if ( ! error ) error = pci_write_config_dword( device, 4 * index, val );
    }
  }

  /* ... else refresh config space from device structure */ 
  else {
    for ( index = 0; index < 6 && ! error; ++index )
      error = 
	pci_write_config_dword( device, PCI_BASE_ADDRESS_0 + 4 * index,
				device->resource[index].start );
    if ( ! error ) 
      error = 
	pci_write_config_byte( device, PCI_INTERRUPT_LINE, device->irq );
  }

  /* check for error */
  if ( error ) {
    PRINT( "failed to restore state for device %s\n", pci_name( device ) );
    return PSI_INTERNAL_ERROR;
  }

  /* re-enable device */
  pci_disable_device( device );
  error = pci_enable_device( device );
  if ( error ) {
    PRINT( "failed to re-enable device %s\n", pci_name( device ) );
    return PSI_INTERNAL_ERROR;
  }

  return 0;
}

/* remove device */
static int _psi_remove_device( struct file * file, unsigned long arg )
{
  struct pci_driver * driver;
  struct pci_dev * device;
  tDeviceCtrl param;
  int error;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 24 ))
  struct pci_bus * bus;
#elif (LINUX_VERSION_CODE < KERNEL_VERSION( 2, 6, 0 ))
  int func;
  u8 hdr_type;
#endif

  /* get argument */
  error = 
    copy_from_user( & param, (void __user *) arg, sizeof ( tDeviceCtrl ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }

  DPRINT( "removing device %02x:%02x:%x\n", 
	  (int) param.bus, PCI_SLOT( param.devfn ), PCI_FUNC( param.devfn ) );

#if (LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 24 ))

  /* find bus */
  bus = pci_find_bus( 0, param.bus );
  if ( ! bus ) {
    PRINT( "bus %02x not found\n", (int) param.bus );
    return PSI_BUS_OOR;
  }

  /* get device */
  device = pci_get_slot( bus, param.devfn );
  if ( ! device ) {
    PRINT( "device %02x:%02x.%x not found\n",
	   (int) param.bus, PCI_SLOT( param.devfn ), PCI_FUNC( param.devfn ) );
    return PSI_DEVICE_NF;
  }

  /* remove device safely */
  driver = pci_dev_driver( device );
  if ( driver ) {
    if ( driver->remove ) driver->remove( device );
    else {
      PRINT( "failed to remove device %s\n", pci_name( device ) );
      return PSI_INTERNAL_ERROR;
    }
    device->driver = 0;
  }
  pci_remove_bus_device( device );
  pci_dev_put( device );

  /* cleanup all regions associated with device */
  psi_cleanup_regions_for_device( device );

#elif (LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 0 ))

  /* find device */
  device = pci_find_slot( param.bus, param.devfn );
  if ( ! device ) {
    PRINT( "device %02x:%02x.%x not found\n",
	   (int) param.bus, PCI_SLOT( param.devfn ), PCI_FUNC( param.devfn ) );
    return PSI_DEVICE_NF;
  }

  /* remove device safely */
  driver = pci_dev_driver( device );
  if ( driver ) {
    if ( driver->remove ) driver->remove( device );
    else {
      PRINT( "failed to remove device %s\n", pci_name( device ) );
      return PSI_INTERNAL_ERROR;
    }
    device->driver = 0;
  }
  pci_remove_bus_device( device );

  /* cleanup all regions associated with device */
  psi_cleanup_regions_for_device( device );

#else

  /* unlink all functions of the slot */
  for ( func = 0; func < 8; ++func ) {
    /* find device */
    device = 
      pci_find_slot( param.bus, PCI_DEVFN( PCI_SLOT( param.devfn ), func ) );
    if ( ! device || 
	 pci_read_config_byte( device, PCI_HEADER_TYPE, & hdr_type ) )
      break;

    /* remove device safely */
    driver = pci_dev_driver( device );
    if ( driver ) {
      if ( driver->remove ) driver->remove( device );
      else {
        PRINT( "failed to remove device %s\n", pci_name( device ) );
        return PSI_INTERNAL_ERROR;
      }
      device->driver = 0;
    }
    pci_remove_device( device );

    DPRINT( "function %d removed\n", func );

    /* cleanup all regions associated with device */
    psi_cleanup_regions_for_device( device );

    /* check if device supports multiple functions */
    if ( ! func && ! (hdr_type & 0x80) ) break;
  }

  if ( ! func && ! device ) {
    PRINT( "device %02x:%02x.%x not found\n",
	   (int) param.bus, PCI_SLOT( param.devfn ), PCI_FUNC( param.devfn ) );
    return PSI_DEVICE_NF;
  }

#endif
  
  return 0;
}

/* insert device */
static int _psi_insert_device( struct file * file, unsigned long arg )
{
  struct pci_dev * device;
  struct pci_bus * bus;
  tDeviceCtrl param;
  int error;

  /* get argument */
  error = 
    copy_from_user( & param, (void __user *) arg, sizeof ( tDeviceCtrl ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }

  DPRINT( "inserting device %02x:%02x:%x\n", 
	  (int) param.bus, PCI_SLOT( param.devfn ), PCI_FUNC( param.devfn ) );

#if (LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 24 ))
  
  /* find bus */
  bus = pci_find_bus( 0, param.bus );
  if ( ! bus ) {
      PRINT( "bus %02x not found\n", (int) param.bus );
      return PSI_BUS_OOR;
  } 

  /* check device */
  device = pci_get_slot( bus, param.devfn );
  if ( ! device ) {
    /* scan for device and make it available */
    device = pci_scan_single_device( bus, param.devfn );
    if ( device ) pci_bus_add_device( device );

    /* look for device again */
    device = pci_get_slot( bus, param.devfn );
    if ( ! device ) {
      PRINT( "device %02x:%02x.%x not found\n", 
	     (int) param.bus, PCI_SLOT( param.devfn ), 
	     PCI_FUNC( param.devfn ) );
      return PSI_DEVICE_NF;
    }

    /* re-enable device */
    pci_disable_device( device );
    error = pci_enable_device( device );
    pci_dev_put( device );
    if ( error ) {
      PRINT( "failed to enable device %s\n", pci_name( device ) );
      return PSI_INTERNAL_ERROR;
    }
  }
  else {
    PRINT( "device %s already present\n", pci_name( device ) );
    pci_dev_put( device );
  }

#else

  /* check device */
  device = pci_find_slot( param.bus, param.devfn );
  if ( ! device ) {
    /* find bus */
    bus = pci_find_bus( 0, param.bus );
    if ( ! bus ) {
      PRINT( "bus %02x not found\n", (int) param.bus );
      return PSI_BUS_OOR;
    }

    /* scan for device and make it available */
#if (LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 10 ))
    device = pci_scan_single_device( bus, param.devfn );
    if ( device ) pci_bus_add_device( device );
#else
    pci_scan_slot( bus, param.devfn );
    pci_bus_add_devices( bus );
#endif

    /* look for device again */
    device = pci_find_slot( param.bus, param.devfn );
    if ( ! device ) {
      PRINT( "device %02x:%02x.%x not found\n", 
	     (int) param.bus, PCI_SLOT( param.devfn ), 
	     PCI_FUNC( param.devfn ) );
      return PSI_DEVICE_NF;
    }

    /* re-enable device */
    pci_disable_device( device );
    error = pci_enable_device( device );
    if ( error ) {
      PRINT( "failed to enable device %s\n", pci_name( device ) );
      return PSI_INTERNAL_ERROR;
    }
  }
  else PRINT( "device %s already present\n", pci_name( device ) );

 #endif
 
  return 0;
}



/* get region info */
static int _psi_get_region_info( struct file * file, unsigned long arg )
{
  struct psi_region_desc desc;
  int error;
  tRegInfo param;

  /* get argument */
  error = copy_from_user( & param, (void __user *) arg, sizeof ( tRegInfo ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }

  DPRINT( "getting info for region %lxh\n", param.region );

  /* fill out info buffer */
  error = psi_get_region_desc( (struct psi_region *) param.region, & desc );
  if ( error ) return error;
  switch ( desc.type ) {
  case PSI_REGION_MEMORY:
    if ( desc.id.memory.subtype == PSI_MEMORY_BIGPHYSAREA )
      param.status.typ = _bigphys_;
    else param.status.typ = _mem_;
    strncpy( param.status.name, desc.id.memory.name, MAX_REGION_NAME_LEN );
    break;

  case PSI_REGION_PHYSICAL:
    param.status.typ = _physmem_;
    param.status.address = desc.id.physical;
    break;
    
  case PSI_REGION_CONFIG:
    param.status.typ = _config_;
    param.status.bus = desc.id.config.bus;
    param.status.devfn = desc.id.config.devfn;
    param.status.base = _none_;
    break;
    
  case PSI_REGION_BASE:
    if ( desc.id.base.subtype == PSI_BASE_MEMORY )
      param.status.typ = _base_;
    else param.status.typ = _baseIO_;
    param.status.bus = desc.id.base.bus;
    param.status.devfn = desc.id.base.devfn;
    param.status.base = desc.id.base.index;
    break;
  }
  param.status.users = 
    psi_get_region_total_references( (struct psi_region *) param.region, 
				     & error );
  param.status.size = 
    psi_get_region_size( (struct psi_region *) param.region, & error );
  
  /* return info */
  error = copy_to_user( (void __user *) arg, & param, sizeof ( tRegInfo ) );
  if ( error ) {
    PRINT( "failed to copy return value to user\n" );
    return PSI_INTERNAL_ERROR;
  }

  return 0;
}



/* find device */
static int _psi_find_device( struct file * file, unsigned long arg )
{
  struct pci_dev * device;
  int error, index;
  tFind param;

  /* get argument */
  error = copy_from_user( & param, (void __user *) arg, sizeof ( tFind ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }

  /* find device */
#if (LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 0 ))
  device = pci_get_device( param.vendor, param.device, 0 );
  for ( index = 0; device && index < param.index; ++index )
    device = pci_get_device( param.vendor, param.device, device );
#else /* LINUX_VERSION_CODE < KERNEL_VERSION( 2, 6, 0 ) */
  device = pci_find_device( param.vendor, param.device, 0 );
  for ( index = 0; device && index < param.index; ++index )
    device = pci_find_device( param.vendor, param.device, device );
#endif
  if ( ! device ) {
    PRINT( "vendor %xh, device %xh, index %d not found\n", 
	   param.vendor, param.device, param.index );
    return PSI_DEVICE_NF;
  }

  /* fill out device buffer */
  param.bus = device->bus->number;
  param.devfn = device->devfn;

  DPRINT( "vendor %xh, device %xh, index %d found: %s\n", 
	  param.vendor, param.device, param.index, pci_name( device ) );

#if (LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 0 ))
  /* release device */
  pci_dev_put( device );
#endif /* LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 0 ) */

  /* return info */
  error = copy_to_user( (void __user *) arg, & param, sizeof ( tFind ) );
  if ( error ) {
    PRINT( "failed to copy return value to user\n" );
    return PSI_INTERNAL_ERROR;
  }

  return 0;
}



/* get physical address */
static int _psi_get_physical_address( struct file * file, unsigned long arg )
{
  struct psi_region * region;
  int error;
  tPhysAddr param;

  /* get argument */
  error = copy_from_user( & param, (void __user *) arg, sizeof ( tPhysAddr ) );
  if ( error ) {
    PRINT( "failed to copy argument from user\n" );
    return PSI_INTERNAL_ERROR;
  }

  /* find region from virtual address */
  region = psi_virtual_to_region( param.ptr );
  if ( ! region ) return PSI_N_MAPPED;

  DPRINT( "getting bus address for region %ph\n", region );
  
  /* fill out address buffer */
  param.physAddr = (void *) psi_region_to_physical_address( region, & error );
  param.busAddr = (void *) psi_region_to_bus_address( region, & error );

  /* return info */
  error = copy_to_user( (void __user *) arg, & param, sizeof ( tPhysAddr ) );
  if ( error ) {
    PRINT( "failed to copy return value to user\n" );
    return PSI_INTERNAL_ERROR;
  }

  return 0;
}



/* device ioctl version 1 */
int psi_device_ioctl( struct inode * inode, struct file * file,
		      unsigned int cmd, unsigned long arg )
{
  int result;

  DPRINT( "psi_device_ioctl()\n" );
  DPRINT( "ioctl %xh\n", cmd );

  switch ( cmd ) {
  case PSI_DELAY_NSECS:
    result = _psi_delay_nsecs( file, arg );
    break;

  case PSI_READ:
    result = _psi_read_region( file, arg );
    break;

  case PSI_WRITE:
    result = _psi_write_region( file, arg );
    break;

  case PSI_MMAP:
    result = _psi_map_region( file, arg );
    break;

  case PSI_RELEASE_MMAP:
    result = _psi_release_region_map( file, arg );
    break;

  case PSI_SET_DMA_MASK:
    result = _psi_set_dma_mask( file, arg );
    break;

  case PSI_MAP_DMA:
    result = _psi_map_dma( file, arg );
    break;

  case PSI_UNMAP_DMA:
    result = _psi_unmap_dma( file, arg );
    break;

  case PSI_SUSPEND_DMA:
    result = _psi_suspend_dma( file, arg );
    break;

  case PSI_RESUME_DMA:
    result = _psi_resume_dma( file, arg );
    break;

  case PSI_WAIT_IRQ:
    result = _psi_wait_irq( file, arg );
    break;

  case PSI_FLUSH_IRQ:
    result = _psi_flush_irq( file, arg );
    break;

  case PSI_LOCK_REGION:
    result = _psi_lock_region( file, arg );
    break;

  case PSI_UNLOCK_REGION:
    result = _psi_unlock_region( file, arg );
    break;

  case PSI_CHECK_REGION_LOCK:
    result = _psi_check_region_lock( file, arg );
    break;

  case PSI_OPEN_MEM:
    result = _psi_open_memory( file, arg );
    break;

  case PSI_OPEN_BIGPHYS:
    result = _psi_open_bigphys( file, arg );
    break;

  case PSI_OPEN_PHYSMEM:
    result = _psi_open_physical( file, arg );
    break;

  case PSI_OPEN_CONFIG:
    result = _psi_open_config( file, arg );
    break;

  case PSI_OPEN_BASE:
    result = _psi_open_base( file, arg );
    break;

  case PSI_CLOSE_REGION:
    result = _psi_close_region( file, arg );
    break;

  case PSI_SIZE_REGION:
    result = _psi_size_region( file, arg );
    break;

  case PSI_FIND_DEVICE:
    result = _psi_find_device( file, arg );
    break;

  case PSI_GET_REGION_INFO:
    result = _psi_get_region_info( file, arg );
    break;

  case PSI_GET_PHYS_ADDR:
    result = _psi_get_physical_address( file, arg );
    break;

  case PSI_SAVE_STATE:
    result = _psi_save_state( file, arg );
    break;

  case PSI_RESTORE_STATE:
    result = _psi_restore_state( file, arg );
    break;

  case PSI_REMOVE_DEVICE:
    result = _psi_remove_device( file, arg );
    break;
  
  case PSI_INSERT_DEVICE:
    result = _psi_insert_device( file, arg );
    break;
  
  case PSI_RESET:
    result = 0;
    break;

  case PSI_CLEANUP:
    result = _psi_cleanup( file, arg );
    break;

  default:
    PRINT( "unknown ioctl command %xh\n", cmd );
    result = PSI_NOT_IMPL;
  }

  DPRINT( "ioctl result %d\n", result );
  return result;
}
