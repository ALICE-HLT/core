/* 
   PCI and Shared Memory Interface, Version 2
   
   driver module
   configuration
   
   Florian Painke <florian.painke@urz.uni-hd.de>
   read the file ../../LICENSE for licensing and copyright information
   
   history
   22.03.2006 initial implementation
   29.03.2006 moved to subdirectory psi
              moved to source directory
   26.04.2006 changed config names, use bigphys area kernel config  
*/

#ifndef CONFIG_H_INLCUDED
#define CONFIG_H_INLCUDED

#if ( LINUX_VERSION_CODE < KERNEL_VERSION( 2, 6, 16 ) )
#include <linux/config.h>
#elif ( LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 33) )
#include <linux/autoconf.h>
#else
#include <generated/autoconf.h>
#endif



/* use static major number when compiling for a 2.4 kernel without devfs */
#if ( LINUX_VERSION_CODE < KERNEL_VERSION( 2, 6, 0 ) )
#if ( ! defined( CONFIG_DEVFS ) && ! defined( CONFIG_PSI_STATIC_MAJOR ) )
#define CONFIG_PSI_STATIC_MAJOR 1
#endif
#endif

/* psi device major number */
#define PSI_MAJOR 100

/* verify user for each region access */
#ifndef CONFIG_PSI_VERIFY_USER
#define CONFIG_PSI_VERIFY_USER 1
#endif

/* page threshold for bigphysarea usage */
#define BIGPHYSAREA_PAGES_THRESHOLD 256

/* default mode of files created by driver */
#define DEFAULT_FILE_MODE 0664



#endif /* CONFIG_H_INLCUDED */
