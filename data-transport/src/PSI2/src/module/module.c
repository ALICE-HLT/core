/* 
   PCI and Shared Memory Interface, Version 2
   
   driver module
   module entry point
   
   Florian Painke <florian.painke@urz.uni-hd.de>
   read the file LICENSE for licensing and copyright information
   
   history
   13.03.2006 initial implementation
   15.03.2006 cleanup regions added to exit function
   27.03.2006 modifications for kernel version 2.4
   04.04.2006 cleanup ioctl added to exit function
   11.04.2006 removed cleanup ioctl
*/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/version.h>

#include "config.h"

#include <psi.h>

#include "module.h"
#include "print.h"
#include "debug.h"
#include "device.h"
#include "regions.h"
#include "ioctl.h"



/* module header */
MODULE_LICENSE( "GPL" );
MODULE_DESCRIPTION( "PCI and Shared Memory Interface" );
MODULE_AUTHOR( "Florian Painke <florian.painke@urz.uni-hd.de>" );
#if (LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 0 ))
MODULE_VERSION( MODULE_SHORT_VERSION );
#endif



/* deinitialise module */
static void __exit _psi_exit( void )
{
  DPRINT( "_psi_exit()\n" );

  /* unregister device */  
  psi_unregister_device();

  /* cleanup subsystems */
  psi_cleanup_regions();

  PRINT( "module unloaded\n" );
}

/* initialise module */
static int __init _psi_init( void )
{
  int error;

  DPRINT( "_psi_init()\n" );
  PRINT( "version %s (%s %s)\n", MODULE_SHORT_VERSION, __DATE__, __TIME__ );
  
  /* register device */
  error = psi_register_device();
  if ( error ) goto fail;

  PRINT( "module loaded\n" );

  return 0;

 fail:
  _psi_exit();

  return error;
}



/* module interface */
module_init( _psi_init );
module_exit( _psi_exit );
