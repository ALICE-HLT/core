#!/bin/sh

. /lib/lsb/init-functions

case "$1" in
  start)
    log_begin_msg "Starting PSI driver..."
    if modprobe psi; then
      sleep 1
      if [ -c /dev/psi ]; then chmod a+rw /dev/psi; fi
      log_end_msg 0
    else
      log_end_msg 1
    fi
    ;;

  stop)
    log_begin_msg "Stopping PSI driver..."
    if rmmod psi; then
      log_end_msg 0
    else
      if rmmod --force psi; then
        log_end_msg 0
      else
        log_end_msg 1
      fi
    fi
    ;;

  restart)
    log_begin_msg "Restarting PSI driver..."
    if ! rmmod psi; then rmmod --force psi; fi
    if modprobe psi; then
      sleep 1
      if [ -c /dev/psi ]; then chmod a+rw /dev/psi; fi
      log_end_msg 0
    else
      log_end_msg 1
    fi
    ;;
esac

