/* 
   PCI and Shared Memory Interface, Version 2
   
   driver module
   region handling
   
   Florian Painke <florian.painke@urz.uni-hd.de>
   read the file LICENSE for licensing and copyright information
   
   history
   13.03.2006 initial implementation
   15.03.2006 region accessors added
   13.05.2006 error handling changed
   14.02.2008 added psi_release_region_map
*/

#ifndef REGIONS_H_INCLUDED
#define REGIONS_H_INCLUDED

#ifndef pid_t
#include <linux/types.h>
#endif

#ifndef vm_area_struct
#include <linux/mm.h>
#endif

#ifndef __user
#include <linux/compiler.h>
#ifndef __user
#define __user
#endif
#endif



/* size of config space */
#define CONFIG_SPACE_SIZE 256

/* maximum number of maps */
#define MAX_MAPS 8

/* length of region name */
#define MAX_REGION_NAME_LEN 99



/* region */
struct psi_region;

/* region types */
enum {
  PSI_REGION_NONE,
  PSI_REGION_MEMORY,
  PSI_REGION_PHYSICAL,
  PSI_REGION_CONFIG,
  PSI_REGION_BASE
};

/* memory subtypes */
enum {
  PSI_MEMORY_ANY,
  PSI_MEMORY_KERNEL,
  PSI_MEMORY_BIGPHYSAREA
};

/* pci base subtypes */
enum {
  PSI_BASE_ANY,
  PSI_BASE_MEMORY,
  PSI_BASE_IO
};

/* region descriptor */
struct psi_region_desc {
  int type;
  union {
    struct {
      int subtype;
      char name[MAX_REGION_NAME_LEN+1];
    } memory;
    unsigned long physical;
    struct {
      unsigned char bus;
      unsigned int devfn;
    } config;
    struct {
      int subtype;
      unsigned char bus;
      unsigned int devfn;
      unsigned short index;
    } base;
  } id;
};



/* cleanup regions (including locked regions) */
void psi_cleanup_regions_locked( void );

/* cleanup regions */
void psi_cleanup_regions( void );

/* cleanup regions after process */
void psi_cleanup_regions_after_process( pid_t pid );



/* get region */
struct psi_region * 
psi_get_region( struct psi_region_desc * desc, int * error );

/* release region */
int psi_release_region( struct psi_region * region );



/* resize region */
int psi_resize_region( struct psi_region * region, size_t size );



/* map region/release region map */
int psi_map_region( struct psi_region * region, struct vm_area_struct * vma );
int psi_release_region_map( struct psi_region * region, void * ptr );

/* read/write from/to region */
int psi_read_region( struct psi_region * region, off_t offset,
		     void __user * buffer, size_t size, size_t count );
int psi_write_region( struct psi_region * region, off_t offset, 
		      void __user * buffer, size_t size, size_t count );



/* set dma mask */
int psi_set_dma_mask( struct psi_region * device, u64 mask );

/* map dma buffer to device */
unsigned long 
psi_map_dma( struct psi_region * device, struct psi_region * buffer, 
	     int direction, int * error );

/* unmap dma buffer from device */
int psi_unmap_dma( struct psi_region * buffer );

/* lock dma buffer */
int psi_lock_dma( struct psi_region * buffer );

/* unlock dma buffer */
int psi_unlock_dma( struct psi_region * buffer );



/* lock region */
int psi_lock_region( struct psi_region * region );

/* unlock region */
int psi_unlock_region( struct psi_region * region );

/* check region lock */
int psi_check_region_lock( struct psi_region * region, int * error );



/* wait for irq on region */
int psi_wait_irq( struct psi_region * region );

/* flush pending irqs */
int psi_flush_irq( struct psi_region * region );



/* find region matching virtual address */
struct psi_region * psi_virtual_to_region( const void * ptr );



/* get region descriptor for region */
int psi_get_region_desc( struct psi_region * region, 
			 struct psi_region_desc * desc );

/* get region size */
size_t psi_get_region_size( struct psi_region * region, int * error );

/* get number of users for region */
int psi_get_region_users( struct psi_region * region, int * error );

/* get number of references for region (current user only) */
int psi_get_region_references( struct psi_region * region, int * error );

/* get number of total references for region */
int psi_get_region_total_references( struct psi_region * region, int * error );



/* get device associated with region */
struct pci_dev * 
psi_region_to_device( struct psi_region * region, int * error );

/* cleanup regions associated with device */
void psi_cleanup_regions_for_device( struct pci_dev * device );



/* get physical address for region */
unsigned long 
psi_region_to_physical_address( struct psi_region * region, int * error );

/* find region matching physical address */
struct psi_region * psi_physical_address_to_region( unsigned long address );

/* get bus address for region */
unsigned long 
psi_region_to_bus_address( struct psi_region * region, int * error );

/* find region matching bus address */
struct psi_region * psi_bus_address_to_region( unsigned long address );



#endif /* REGIONS_H_INCLUDED */
