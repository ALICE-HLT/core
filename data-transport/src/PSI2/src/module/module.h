/* 
    PCI and Shared Memory Interface, Version 2
    
    driver module
    module definitions

    Florian Painke <florian.painke@urz.uni-hd.de>
    read the file LICENSE for licensing and copyright information

    history
    13.03.2006 initial implementation
*/

#ifndef PSI_MODULE_H_INLCUDED
#define PSI_MODULE_H_INLCUDED



/* module name */
#define MODULE_NAME "psi"

/* module version */
#define MODULE_SHORT_VERSION "2:0.56"



#endif /* PSI_MODULE_H_INLCUDED */
