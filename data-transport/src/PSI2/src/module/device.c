/* 
   PCI and Shared Memory Interface, Version 2
   
   driver module
   device handling
   
   Florian Painke <florian.painke@urz.uni-hd.de>
   read the file LICENSE for licensing and copyright information.
   
   history
   13.03.2006 initial implementation
   15.03.2006 basic version 1 ioctls work
   20.03.2006 moved psi1_* stuff to psi1_ioctl.c
   21.03.2006 added close, read/write and find device
   22.03.2006 added memory subtype, dev attribute (udev)
   27.03.2006 modifications for kernel version 2.4
   04.04.2006 removed sysfs driver and device stuff
   04.01.2007 removed sysfs stuff completely, registering misc device
              backported to kernel version 2.4
*/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/fs.h>
#include <linux/kdev_t.h>
#include <linux/mm.h>
#include <linux/errno.h>
#include <linux/pci.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/miscdevice.h>

#include "config.h"

#include <bool.h>
#include <psi.h>
#include <psi_ioctl.h>

#include "print.h"
#include "debug.h"
#include "device.h"
#include "regions.h"
#include "ioctl.h"



/* open device */
static int _psi_open_device( struct inode * inode, struct file * file )
{
  DPRINT( "_psi_open_device()\n" );

  return 0;
}

/* close device */
static int _psi_close_device( struct inode * inode, struct file * file )
{
  DPRINT( "_psi_close_device()\n" );

  psi_cleanup_regions_after_process( current->tgid );
  return 0;
}



/* device mmap */
static int _psi_device_mmap( struct file * file, struct vm_area_struct * vma )
{
  struct psi_region * region;

  DPRINT( "_psi_device_mmap()\n" );

  /* find region for address */
  region = psi_physical_address_to_region( vma->vm_pgoff << PAGE_SHIFT );
  if ( ! region ) return -EINVAL;

  /* map region */
  return psi_map_region( region, vma );
}



/* device operations */
static struct file_operations _psi_device_ops = {
  .owner = THIS_MODULE,
  .open = _psi_open_device,
  .release = _psi_close_device,
  .ioctl = psi_device_ioctl,
  .mmap = _psi_device_mmap
};



#if defined( CONFIG_PSI_STATIC_MAJOR ) && (CONFIG_PSI_STATIC_MAJOR == 1)

/* device handle */
static dev_t _psi_dev = 0;

#else

/* misc device entry */
static struct miscdevice _psi_misc_device = {
  .minor = MISC_DYNAMIC_MINOR,
  .name = DEVICE_NAME,
  .fops = & _psi_device_ops,
};
static bool _psi_misc_registered = false;

#endif



/* unregister device */
void psi_unregister_device( void )
{
  DPRINT( "psi_unregister_device()\n" );

#if defined( CONFIG_PSI_STATIC_MAJOR ) && (CONFIG_PSI_STATIC_MAJOR == 1)

  /* free major device number */
  if ( _psi_dev ) unregister_chrdev( MAJOR( _psi_dev ), DEVICE_NAME );

#else

  /* unregister misc device */
  if ( _psi_misc_registered ) misc_deregister( & _psi_misc_device );
  _psi_misc_registered = false;

#endif
}



/* register device */
int psi_register_device( void )
{
  int major, minor;
  int result;

  DPRINT( "psi_register_device()\n" );

#if defined( CONFIG_PSI_STATIC_MAJOR ) && (CONFIG_PSI_STATIC_MAJOR == 1)

  /* get major device number */
  result = register_chrdev( PSI_MAJOR, DEVICE_NAME, & _psi_device_ops );
  if ( result < 0 ) {
    PRINT( "failed to register device %d:0 with kernel\n", PSI_MAJOR );
    goto fail;
  }
  major = PSI_MAJOR;
  minor = 0;
  _psi_dev = MKDEV( major, minor );

#else

  /* register misc device with kernel (and devfs/sysfs/udev/...) */
  result = misc_register( & _psi_misc_device );
  if ( result ) {
    PRINT( "failed to register misc device with kernel\n" );
    goto fail;
  }
  _psi_misc_registered = true;
  major = MISC_MAJOR;
  minor = _psi_misc_device.minor;

#endif

  PRINT( "device registered at %d:%d\n", major, minor );

  return 0;

 fail:
  psi_unregister_device();
  
  return result;
}
