/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: datadump.c 862 2006-02-17 18:45:24Z timm $ 
**
***************************************************************************
*/

#include <pcitypes.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

int main( int argc, char** argv )
    {
    char* filename = NULL;
    short stepsize = 1;
    off_t offset = 0;
    unsigned long length = ~(unsigned long)0;
    char* convErrP = NULL;
    int fh, ret;
    off_t filelength;
    const unsigned blocksize = 4096;
    u8 block[ blocksize ];
    unsigned long bytesread = 0;
    int toread;
    int i;

    char* usage = "Usage: %s <filename> <stepsize> (<offset>) (<length>)\n"
		  "       <filename>: The name of file whose contents are to be dumped.\n"
		  "       <stepsize>: The number of bytes to dump together. can be 1 (byte), 2 (word), or 4 (doubleword).\n"
		  "       <offset>: The starting offset in the file. (Default: Beginning of file)\n"
		  "       <length>: The amount of bytes (!!) to dump. (Default: Entire file)\n";

    
    if ( argc<3 || argc>5 )
	{
	fprintf( stderr, usage, argv[0] );
	return -1;
	}

    filename = argv[1];
    
    stepsize = strtoul( argv[2], &convErrP, 0 );
    if ( *convErrP )
	{
	fprintf( stderr, usage, argv[0] );
	fprintf( stderr, "Error converting stepsize '%s'.\n", argv[2] );
	return -1;
	}
    if ( stepsize!=1 && stepsize!=2 && stepsize!=4 )
	{
	fprintf( stderr, usage, argv[0] );
	fprintf( stderr, "Only values of 1, 2, or 4 allowed for stepsize - %u given.\n", stepsize );
	return -1;
	}

    if ( argc>3 )
	{
	offset = strtoul( argv[3], &convErrP, 0 );
	if ( *convErrP )
	    {
	    fprintf( stderr, usage, argv[0] );
	    fprintf( stderr, "Error converting file offset '%s'.\n", argv[3] );
	    return -1;
	    }
	}

    if ( argc>4 )
	{
	length = strtoul( argv[4], &convErrP, 0 );
	if ( *convErrP )
	    {
	    fprintf( stderr, usage, argv[0] );
	    fprintf( stderr, "Error converting data length '%s'.\n", argv[4] );
	    return -1;
	    }
	}

    fh = open( filename, O_RDONLY );
    if ( fh==-1 )
	{
	fprintf( stderr, usage, argv[0] );
	fprintf( stderr, "Error opening file '%s': %s (%d)\n", filename,
		 strerror(errno), errno );
	return -1;
	}
    
    filelength = lseek( fh, 0, SEEK_END );
    if ( filelength == (off_t)-1 )
	{
	fprintf( stderr, usage, argv[0] );
	fprintf( stderr, "Error determining length of file '%s': %s (%d)\n", filename,
		 strerror(errno), errno );
	close( fh );
	return -1;
	}
    
    if ( (unsigned long)offset+length > (unsigned long)filelength )
	length = filelength-offset;

    length &= ~(stepsize-1);

    if ( offset >= filelength )
	{
	close( fh );
	return 0;
	}
	
    filelength = lseek( fh, offset, SEEK_SET );
    if ( filelength==(off_t)-1 )
	{
	fprintf( stderr, usage, argv[0] );
	fprintf( stderr, "Error positioning on offset 0x%08lX (%lu) in file '%s': %s (%d)\n", (unsigned long)offset, (unsigned long)offset, filename,
		 strerror(errno), errno );
	close( fh );
	return -1;
	}
	
    while ( bytesread < length )
	{
	if ( length-bytesread < blocksize )
	    toread = length-bytesread;
	else
	    toread = blocksize;

	ret = read( fh, block, toread );
	if ( ret != toread )
	    {
	    fprintf( stderr, "\n" );
	    fprintf( stderr, usage, argv[0] );
	    fprintf( stderr, "Error from file '%s': %s (%d)\n", filename,
		     strerror(errno), errno );
	    close( fh );
	    return -1;
	    }
	/*bytesread += toread;*/
	for ( i = 0; i < toread; i += stepsize, bytesread += stepsize )
	    {
	    switch ( stepsize )
		{
		case 1: printf( "0x%02lX", (unsigned long)*(u8*)(block+i) ); break;
		case 2: printf( "0x%04lX", (unsigned long)*(u16*)(block+i) ); break;
		case 4: printf( "0x%08lX", (unsigned long)*(u32*)(block+i) ); break;
		}
	    if ( bytesread < length )
		printf( " " );
	    }
	}
    printf( "\n" );
    close( fh );
    return 0;
    }




/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: datadump.c 862 2006-02-17 18:45:24Z timm $ 
**
***************************************************************************
*/
