#include <bool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/pci.h>
#include <psi.h>
#include <psiToolLib.h>
#include <psi_error.h>
#include <psi_ioctl.h>

#define PATH_DELIMITER '/'
#define DEFAULT_FILE_MODE 0666

/* get PCI address from device id 
   returns 0 on success, status code on failure */
int 
id_to_slot( unsigned int vendor, unsigned int device, unsigned int index,
	    unsigned int * bus, unsigned int * slot, unsigned int * func )
{
  PSI_Status result = 0;
  tRegionStatus stat;
  tRegion region;
  char path[100];

  /* open config space region for vendor and device id */
  sprintf( path, "/dev/psi/vendor/0x%x/device/0x%x/%d/config", 
	   vendor, device, index );
  result = PSI_openRegion( & region, path );
  if ( result ) {
    fprintf( stderr, "Failed to open %s:\n\t%s (%d)\n\n",
	     path, PSI_strerror( result ), (int) result );
    region = 0;
    goto fail;
  }

  /* get PCI address */
  result = PSI_getRegionStatus( region, & stat );
  if ( result ) {
    fprintf( stderr, "Failed to get info for %s:\n\t%s (%d)\n\n",
	     path, PSI_strerror( result ), (int) result );
    goto fail;
  }

  (* bus) = stat.bus;
  (* slot) = PCI_SLOT( stat.devfn );
  (* func) = PCI_FUNC( stat.devfn );

 fail:
  if ( region ) PSI_closeRegion( region );
  return (int) result;
}

/* get device from command line arguments 
   returns number of arguments consumed, 0 on failure */
int 
get_device( int argc, const char ** argv, 
	    unsigned int * _bus, unsigned int * _slot, unsigned int * _func )
{
  unsigned int vendor, device, index, bus, slot, func;
  int result, n;

  if ( ! argc ) return 0;

  /* get device by id */
  if ( ! strcmp( argv[0], "-id" ) ) {
    /* at least vendor and device id is needed */
    if ( argc < 3 ) return 0;
    if ( sscanf( argv[1], "%x", & vendor ) != 1 ) return 0;
    if ( sscanf( argv[2], "%x", & device ) != 1 ) return 0;

    /* additionally, device index can be given */
    n = 3;
    if ( argc > 3 && sscanf( argv[3], "%u", & index ) == 1 ) n = 4;
    else index = 0;

    result = id_to_slot( vendor, device, index, _bus, _slot, _func );
    if ( result ) return 0;

    return n;
  }

  /* get device by address */
  else if ( ! strcmp( argv[0], "-slot" ) ) {
    /* at least bus and slot number is needed */
    if ( argc < 3 ) return 0;
    if ( sscanf( argv[1], "%x", & bus ) != 1 ) return 0;
    if ( sscanf( argv[2], "%x", & slot ) != 1 ) return 0;

    /* additionally, function number can be given */
    n = 3;
    if ( argc > 3 && sscanf( argv[3], "%u", & func ) == 1 ) n = 4;
    else func = 0;

    (* _bus) = bus;
    (* _slot) = slot;
    (* _func) = func;

    return n;
  }

  return 0;
}

/* print help for get_device */
void help_get_device( void )
{
  fprintf( stderr, "<device> may be given in one of the following forms:\n" );
  fprintf( stderr, "\t-id <vendor-id> <device-id> [<number>]\n" );
  fprintf( stderr, "\t-slot <bus-nr> <slot-nr> [<function-nr>]\n" );
  fprintf( stderr, "all arguments except the device number are interpreted "
	   "as hexadecimal integers\n\n" );
}



/* get file name from command line arguments
   returns the number of arguments consumed, 0 on failure */
int get_file( int argc, const char ** argv, char * name, int length )
{
  if ( ! argc || ! length ) return 0;

  if ( ! strcmp( argv[0], "-file" ) ) {
    if ( argc < 2 ) return 0;
    strncpy( name, argv[1], length - 1 );
    if ( name[length-1] ) name[length-1] = 0;

    return 2;
  }

  return 0;
}



/* open config space region */
tRegion 
open_config_space( unsigned int bus, unsigned int slot, unsigned int func,
		   size_t * size )
{
  PSI_Status result;
  tRegion region;
  tRegionStatus stat;
  char path[100];

  /* open region */
  sprintf( path, "/dev/psi/bus/0x%x/slot/0x%x/function/%d/config",
	   bus, slot, func );
  result = PSI_openRegion( & region, path );
  if ( result ) {
    fprintf( stderr, "Failed to open %s:\n\t%s (%d)\n\n",
	     path, PSI_strerror( result ), (int) result );
    region = 0;
    goto fail;
  }

  /* get region size */
  result = PSI_getRegionStatus( region, & stat );
  if ( result ) {
    fprintf( stderr, "Failed to get info for %s:\n\t%s (%d)\n\n",
	     path, PSI_strerror( result ), (int) result );
    goto fail;
  }

  if ( size ) (* size) = stat.size;
  return region;

 fail:
  if ( region ) PSI_closeRegion( region );
  return 0;
}



/* save config space */
int save_config( int argc, const char ** argv )
{
  unsigned int bus, slot, func;
  int result, index = 0;
  size_t size;
  tRegion region;
  FILE * file = 0, * out;
  char name[256];
  void * buffer = 0;

  /* get device */
  result = get_device( argc, argv, & bus, & slot, & func );
  if ( ! result ) goto help;
  index += result;

  /* get file name (optional) */
  if ( index < argc ) {
    result = get_file( argc - index, & argv[index], name, 256 );
    if ( ! result ) name[0] = 0;
    index += result;
  }
  else name[0] = 0;

  if ( index < argc ) goto help;

  /* open region */
  result = ENXIO;
  region = open_config_space( bus, slot, func, & size );
  if ( ! region ) goto fail;

  /* allocate buffer */
  result = ENOMEM;
  buffer = malloc( size );
  if ( ! buffer ) {
    fprintf( stderr, "Failed to allocate buffer for device state\n" );
    goto fail;
  }

  /* save config space */
  result = PSI_saveState( region, buffer );
  if ( result ) {
    fprintf( stderr, "Failed to save device state:\n\t%s (%d)\n", 
	     PSI_strerror( result ), result );
    result = ENXIO;
    goto fail;
  }

  /* write buffer to file or stdout */
  if ( name[0] ) file = fopen( name, "w" );
  else file = 0;
  if ( file ) out = file;
  else out = stdout;

  for ( index = 0; (size_t)index < size; ++index ) {
    fprintf( out, "%02x", (unsigned int) ((u_int8_t *) buffer)[index] );
    if ( ! ((index + 1) % 16) ) fprintf( out, "\n" );
    else fprintf( out, " " );
  }

  result = 0;

 fail:
  if ( file ) fclose( file );
  if ( region ) PSI_closeRegion( region );
  return result;

 help:
  fprintf( stderr, "usage: pci_device_save_state <device> "
	   "[-file <file>]\n\n" );
  help_get_device();
  fprintf( stderr, "if no file argument is given, the data will be printed "
	   "to stdout\n\n" );

  return 1;
}

/* restore config space */
int restore_config( int argc, const char ** argv )
{
  unsigned int bus, slot, func, val;
  int result, index = 0;
  bool refresh = false;
  size_t size;
  tRegion region;
  FILE * file = 0, * in;
  char name[256];
  void * buffer = 0;

  /* get device */
  result = get_device( argc, argv, & bus, & slot, & func );
  if ( ! result ) goto help;
  index += result;

  /* get file name (optional) */
  if ( index < argc ) {
    result = get_file( argc - index, & argv[index], name, 256 );
    if ( ! result ) name[0] = 0;
    index += result;
  }
  else name[0] = 0;

  /* check if we only refresh config space from kernel device information */
  if ( index < argc ) {
    if ( ! strcmp( argv[index], "-refresh" ) ) {
      refresh = true;
      index++;
    }
  }

  if ( name[0] && refresh ) goto help;
  if ( index < argc ) goto help;

  /* open region */
  result = ENXIO;
  region = open_config_space( bus, slot, func, & size );
  if ( ! region ) goto fail;

  if ( ! refresh ) {
    /* allocate buffer */
    result = ENOMEM;
    buffer = malloc( size );
    if ( ! buffer ) {
      fprintf( stderr, "Failed to allocate buffer for device state\n" );
      goto fail;
    }

    /* read buffer from file or stdin */
    if ( name[0] ) file = fopen( name, "r" );
    if ( file ) in = file;
    else in = stdin;

    memset( buffer, 0, size );
    for ( index = 0; (size_t)index < size && ! feof( in ); ++index ) {
      if ( fscanf( in, "%x", & val ) == 1 )
	((u_int8_t *) buffer)[index] = val;
    }
  }

  /* restore config space */
  result = PSI_restoreState( region, buffer );
  if ( result ) {
    fprintf( stderr, "Failed to restore device state:\n\t%s (%d)\n",
	     PSI_strerror( result ), result );
    result = ENXIO;
    goto fail;
  }

  result = 0;

 fail:
  if ( file ) fclose( file );
  if ( buffer ) free( buffer );
  if ( region ) PSI_closeRegion( region );
  return result;

 help:
  fprintf( stderr, "usage: pci_device_restore_state <device> "
	   "[<options>]\n\n" );
  help_get_device();
  fprintf( stderr, "<options> can be one of the following:\n" );
  fprintf( stderr, "\t-f <file>\tread config space data from file\n" );
  fprintf( stderr, "\t-refresh\tuse config space data from kernel\n\n" );
  fprintf( stderr, "if no file argument is given, the data is read from "
	   "stdin\n data is interpreted as hexadecimal bytes\n" );

  return 1;
}



/* remove device */
int remove_device( int argc, const char ** argv )
{
  unsigned int bus, slot, func;
  int result;

  /* get device */
  result = get_device( argc, argv, & bus, & slot, & func );
  if ( ! result || result != argc ) goto help;

  /* remove device */
  result = PSI_removeDevice( bus, PCI_DEVFN( slot, func ) );
  if ( result ) {
    fprintf( stderr, "Failed to remove device:\n\t%s (%d)\n",
	     PSI_strerror( result ), result );
    return ENXIO;
  }

  return 0;

 help:
  fprintf( stderr, "usage: pci_device_remove <device>\n\n" );
  help_get_device();

  return 1;
}

/* insert device */
int insert_device( int argc, const char ** argv )
{
  unsigned int bus, slot, func;
  int result;

  /* get device */
  result = get_device( argc, argv, & bus, & slot, & func );
  if ( ! result || result != argc ) goto help;

  /* insert device */
  result = PSI_insertDevice( bus, PCI_DEVFN( slot, func ) );
  if ( result ) {
    fprintf( stderr, "Failed to insert device:\n\t%s (%d)\n",
	     PSI_strerror( result ), result );
    return ENXIO;
  }

  return 0;

 help:
  fprintf( stderr, "usage: pci_device_insert <device>\n\n" );
  help_get_device();

  return 1;
}



/* cleanp after processes */
int cleanup( int argc, const char ** argv )
{
  PSI_Status result;
  int pid;

  /* parse arguments */
  if ( argc < 1 || argc > 2 ) goto help;
  if ( ! strcmp( argv[0], "-all" ) ) {
    if ( argc == 2 ) {
    if ( ! strcmp( argv[1], "-locked" ) ) pid = (int)PSI_CLEANUP_ALL_LOCKED;
      else goto help;
    }
    else pid = 0;
  }
  else if ( ! strcmp( argv[0], "-pid" ) && argc == 2 ) {
    if ( sscanf( argv[1], "%d", & pid ) != 1 ) goto help;
  }
  else goto help;

  /* cleanup */
  result = PSI_cleanup( pid );
  if ( result )
    fprintf( stderr, "Failed to cleanup after %s\n%s (%d)\n", 
	     pid ? "process" : "all processes", 
	     PSI_strerror( result ), (int) result );

  return 0;

 help:
  fprintf( stderr, "usage: psi_cleanup -all [-locked] | -pid <pid>\n\n" );

  return 1;
}

int main( int argc, const char ** argv )
{
  const char * name;

  /* get name */
  name = strrchr( argv[0], PATH_DELIMITER );
  if ( ! name ) name = argv[0];
  else name++;

  /* check name */
  if ( ! strcmp( name, "pci_device_save_state" ) ) 
    return save_config( argc - 1, & argv[1] );
  if ( ! strcmp( name, "pci_device_restore_state" ) )
    return restore_config( argc - 1, & argv[1] );
  if ( ! strcmp( name, "pci_device_remove" ) )
    return remove_device( argc - 1, & argv[1] );
  if ( ! strcmp( name, "pci_device_insert" ) )
    return insert_device( argc - 1, & argv[1] );
  if ( ! strcmp( name, "psi_cleanup" ) )
    return cleanup( argc - 1, & argv[1] );

  /* check first argument */
  if ( argc <= 1 || ! strcmp( argv[1], "-h" ) || 
       ! strcmp( argv[1], "--help" ) )
    goto help;
  if ( ! strcmp( argv[1], "save" ) )
    return save_config( argc - 2, & argv[2] );
  if ( ! strcmp( argv[1], "restore" ) )
    return restore_config( argc - 2, & argv[2] );
  if ( ! strcmp( argv[1], "remove" ) )
    return remove_device( argc - 2, & argv[2] );
  if ( ! strcmp( argv[1], "insert" ) )
    return insert_device( argc - 2, & argv[2] );
  if ( ! strcmp( argv[1], "cleanup" ) )
    return cleanup( argc - 2, & argv[2] );

 help:
  fprintf( stderr, "usage: %s <command> <parameters>\n\n", name );
  fprintf( stderr, "<command> can be one of the following:\n" );
  fprintf( stderr, "\tsave\tsave device state\n" );
  fprintf( stderr, "\trestore\trestore device state\n" );
  fprintf( stderr, "\tremove\tremove device\n" );
  fprintf( stderr, "\tinsert\tinsert device\n" );
  fprintf( stderr, "\tcleanup\tcleanup after processes\n\n" );
  printf( "for <parameters> refer to %s <command> --help\n\n", name );

  return 1;
}
