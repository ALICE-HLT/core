/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: pci_configure.c 862 2006-02-17 18:45:24Z timm $ 
**
***************************************************************************
*/

#include <psiToolLib.h>
#include <psi.h>
#include <psi_error.h>
#include <stdlib.h>
#include <string.h>


int main( int argc, char** argv )
    {
    u16 vendor, device, nr;
    u16 pbus, pdev, pfunc;
    short bar = -1;
    int useid = 0, usephys = 0;
    int i;

    PSI_Status psiStatus;
    PCI_CSR csr;

    char* errorP = NULL;
    char* convErrP;
    int errorArg = -1;
    int base = 0;

    char* usage     = "Usage: %s (-h) (-d) (-o) (-p) (-bar <bar_nr>) [ \n"
		      "                               -id <vendor-id> <device-id> <card-nr> | \n"
		      "                               -slot <bus-nr> <device-nr> <function-nr> | \n"
		      "                             ]\n"
		      "       -h: Interpret all following numerical values as hexadecimal.\n"
		      "       -d: Interpret all following numerical values as decimal.\n"
		      "       -o: Interpret all following numerical values as octal.\n"
		      "       -p: Interpret all following numerical values according to their prefix (default).\n"
		      "       -bar: Configure only the base address register specified in bar_nr (0-5) (Default: Configure all bars).\n"
 "       -id <vendor-id> <device-id> <card-nr>: Access card with given vendor and device id. Use the nth (n==card-nr) card in the system.\n"
		      "       -slot <bus-nr> <device-nr> <function-nr>: Access device with the given bus/device/function physical PCI device offset.\n\n"
		      "By default all numerical values are interpreted according to their prefix, '0x' for hexadecimal, `0` for octal, anything else for decimal.\n"
		      "This can be overridden with the -h/-d/-o options.\n";
    

    /* Parse the command line arguments. */
    i = 1;
    while ( i < argc )
	{
	if ( !strcmp( argv[i], "-h" ) )
	    {
	    base = 16;
	    }
	else if ( !strcmp( argv[i], "-d" ) )
	    {
	    base = 10;
	    }
	else if ( !strcmp( argv[i], "-o" ) )
	    {
	    base = 8;
	    }
	else if ( !strcmp( argv[i], "-p" ) )
	    {
	    base = 0;
	    }
	else if ( !strcmp( argv[i], ".bar" ) )
	    {
	    if ( i+1 >= argc )
		{
		errorP = "Missing arguments for -bar parameter.";
		errorArg = i;
		}
	    bar = strtol( argv[i+1], &convErrP, base );
	    if ( *convErrP )
		{
		errorP = "Error converting bar nr.";
		errorArg = i+1;
		break;
		}
	    if ( bar<0 || bar>5 )
		{
		errorP = "Invalid bar nr. Only 0 to 5 are allowed.";
		errorArg = i+1;
		break;
		}
	    i += 1;
	    }
 	else if ( !strcmp( argv[i], "-id" ) )
	    {
	    if ( i+3 >= argc )
		{
		errorP = "Missing arguments for -id parameter.";
		errorArg = i;
		}
	    useid = 1;
	    vendor = strtoul( argv[i+1], &convErrP, base );
	    if ( *convErrP )
		{
		errorP = "Error converting vendor ID.";
		errorArg = i+1;
		break;
		}
	    device = strtoul( argv[i+2], &convErrP, base );
	    if ( *convErrP )
		{
		errorP = "Error converting device ID.";
		errorArg = i+2;
		break;
		}
	    nr = strtoul( argv[i+3], &convErrP, base );
	    if ( *convErrP )
		{
		errorP = "Error converting card nr.";
		errorArg = i+3;
		break;
		}
	    i += 3;
	    }
	else if ( !strcmp( argv[i], "-slot" ) )
	    {
	    if ( i+3 >= argc )
		{
		errorP = "Missing arguments for -slot parameter.";
		errorArg = i;
		}
	    usephys = 1;
	    pbus = strtoul( argv[i+1], &convErrP, base );
	    if ( *convErrP )
		{
		errorP = "Error converting bus nr.";
		errorArg = i+1;
		break;
		}
	    pdev = strtoul( argv[i+2], &convErrP, base );
	    if ( *convErrP )
		{
		errorP = "Error converting device nr.";
		errorArg = i+2;
		break;
		}
	    pfunc = strtoul( argv[i+3], &convErrP, base );
	    if ( *convErrP )
		{
		errorP = "Error converting function nr.";
		errorArg = i+3;
		break;
		}
	    i += 3;
	    }
	else
	    {
	    errorP = "Unknown parameter.";
	    errorArg = i;
	    break;
	    }
	i++;
	}


    if ( !errorP )
	{
	if ( useid+usephys!=1 )
	    errorP = "Must specify one of either -id or -slot parameter.";
	}

    if ( errorP )
	{
	fprintf( stderr, usage, argv[0] );
	fprintf( stderr, "Error: %s\n", errorP );
	if ( errorArg>=0 )
	    printf( "Offending argument: %s\n", argv[errorArg] );
	return -1;
	}


    if ( useid )
	{
	psiStatus = PTL_find_pci_device( vendor, device, nr, &pbus, &pdev, &pfunc );
	if ( psiStatus != PSI_OK )
	    {
	    fprintf( stderr, "Error finding logical device 0x%04hX/0x%04hX/0x%08X: %s (%d)\n",
		     vendor, device, nr, PSI_strerror(psiStatus), (int)psiStatus );
	    return -1;
	    }
	}

    psiStatus = PTL_read_phys_pci_csr( pbus, pdev, pfunc, &csr );
    if ( psiStatus != PSI_OK )
	{
	fprintf( stderr, "Error reading CSR from physical device 0x%04hX/0x%04hX/0x%04hX: %s (%d)\n",
		 pbus, pdev, pfunc, PSI_strerror(psiStatus), (int)psiStatus );
	return -1;
	}

    printf( "Configuring " );
    if ( bar==-1 )
	printf( "all base address registers" );
    else
	printf( "base address register %hd", bar );
    printf( " device %hu:%hu.%hu: 0x%04hX / 0x%04hX\n",
	    pbus, pdev, pfunc, csr.fVendorID_req, csr.fDeviceID_req );

    psiStatus = PTL_configure_bars( pbus, pdev, pfunc, bar );
    if ( psiStatus != PSI_OK )
	{
	fprintf( stderr, "Error configuring bar %hd on physical device 0x%04hX/0x%04hX/0x%04hX: %s (%d)\n",
		 bar, pbus, pdev, pfunc, PSI_strerror(psiStatus), (int)psiStatus );
	return -1;
	}
    return 0;
    }





/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: pci_configure.c 862 2006-02-17 18:45:24Z timm $ 
**
***************************************************************************
*/
