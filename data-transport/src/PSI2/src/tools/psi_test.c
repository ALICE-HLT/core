#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*
  #include "pci_usr.h"
  #include "pci_usr_rw_config.h"
  #include "pci_csrs.h"
*/
#include <psi.h>
#include <psi_error.h>
#include <pcitypes.h>
#include <psiToolLib.h>


void compareCallback( void* writevalue, void* readvalue, void* rereadvalue, 
		      tRegion region, unsigned long offset, 
		      unsigned long size );

int main( int argc, char* argv[] )
    {
    u16 vendor, device, nr;
    u32 pbus, pdev, pfunc;
    u32 offset, bar, size;
    int i;
    int error;
    int base = 0;
    int useid = 0, usephys = 0, usemem = 0;
    unsigned long s;
	
    char* errorP = NULL;
    char* convErrP;
    int errorArg = -1;
    int address_seen = 0;
    PSI_Status psiStatus;
    tRegion region;
    char deviceName[1024];
    int args_needed = 0;
    unsigned inner_loop = 1;
    unsigned olc, outer_loop = 1;

    int test;
    const int testcount = 4;
    char* testnames[] = {
	"Walking Ones",
	"Walking Zeroes",
	"Full Bits",
	"Flipping Bits"
    };
    int verbose = 0;
    

    int mode = 0; /* 0: generic, 1: configuration, 2: device bars, 3: physical memory */
	
    char* usage;
    char* usage_bar = "Usage: %s (-V) (-h) (-d) (-o) (-p) (-loopi <inner-loop-count>) (-loopo <outer-loop-count>) [ \n"
		      "                               -id <vendor-id> <device-id> <card-nr> <bar-nr> <offset> | \n"
		      "                               -slot <bus-nr> <device-nr> <function-nr> <bar-nr> <offset> \n"
		      "                             ] <size>\n"
		      "       -V: Be verbose.\n"
		      "       -h: Interpret all following numerical values as hexadecimal.\n"
		      "       -d: Interpret all following numerical values as decimal.\n"
		      "       -o: Interpret all following numerical values as octal.\n"
		      "       -p: Interpret all following numerical values according to their prefix (default).\n"
		      "       -loopi: The inner loop count. How often each pattern combination is written to each offset.\n"
		      "       -loopo: The outer loop. How often the succession of tests is performed in total.\n"
 "       -id <vendor-id> <device-id> <card-nr>: Access card with given vendor and device id. Use the nth (n==card-nr) card in the system.\n"
		      "       -slot <bus-nr> <device-nr> <function-nr>: Access device with the given bus/device/function physical PCI device offset.\n"
		      "       <bar>: Specifies the number of the base address register to access.\n"
		      "       <offset>: The offset to read from/write to.\n"
		      "       <size>: The number of bytes to read/write. Only 1, 2, and 4 are allowed here.\n"
		      "By default all numerical values are interpreted according to their prefix, '0x' for hexadecimal, `0` for octal, anything else for decimal.\n"
		      "This can be overridden with the -h/-d/-o options.\n";

    char* usage_cnf = "Usage: %s (-V ) (-h) (-d) (-o) (-p) (-loopi <inner-loop-count>) (-loopo <outer-loop-count>) [ \n"
		      "                               -id <vendor-id> <device-id> <card-nr> <offset> | \n"
		      "                               -slot <bus-nr> <device-nr> <function-nr> <offset> \n"
		      "                             ] <size>\n"
		      "       -V: Be verbose.\n"
		      "       -h: Interpret all following numerical values as hexadecimal.\n"
		      "       -d: Interpret all following numerical values as decimal.\n"
		      "       -o: Interpret all following numerical values as octal.\n"
		      "       -p: Interpret all following numerical values according to their prefix (default).\n"
		      "       -loopi: The inner loop count. How often each pattern combination is written to each offset.\n"
		      "       -loopo: The outer loop. How often the succession of tests is performed in total.\n"
 "       -id <vendor-id> <device-id> <card-nr>: Access card with given vendor and device id. Use the nth (n==card-nr) card in the system.\n"
		      "       -slot <bus-nr> <device-nr> <function-nr>: Access device with the given bus/device/function physical PCI device offset.\n"
		      "       <offset>: The offset to read from/write to.\n"
		      "       <size>: The number of bytes to read/write. Only 1, 2, and 4 are allowed here.\n"
		      "By default all numerical values are interpreted according to their prefix, '0x' for hexadecimal, `0` for octal, anything else for decimal.\n"
		      "This can be overridden with the -h/-d/-o options.\n";
	
    char* usage_mem = "Usage: %s (-V ) (-h) (-d) (-o) (-p) (-loopi <inner-loop-count>) (-loopo <outer-loop-count>) [ \n"
		      "                               -memory <address> \n"
		      "                             ] <size>\n"
		      "       -V: Be verbose.\n"
		      "       -h: Interpret all following numerical values as hexadecimal.\n"
		      "       -d: Interpret all following numerical values as decimal.\n"
		      "       -o: Interpret all following numerical values as octal.\n"
		      "       -p: Interpret all following numerical values according to their prefix (default).\n"
		      "       -loopi: The inner loop count. How often each pattern combination is written to each offset.\n"
		      "       -loopo: The outer loop. How often the succession of tests is performed in total.\n"
		      "       <address>: The physical memory address to access\n"
		      "       <size>: The number of bytes to read/write. Only 1, 2, and 4 are allowed here.\n"
		      "By default all numerical values are interpreted according to their prefix, '0x' for hexadecimal, `0` for octal, anything else for decimal.\n"
		      "This can be overridden with the -h/-d/-o options.\n";

    char* usage_gen = "Usage: %s (-V ) (-h) (-d) (-o) (-p) (-loopi <inner-loop-count>) (-loopo <outer-loop-count>) [ \n"
		      "                               -id <vendor-id> <device-id> <card-nr> [config | <bar-nr>] <offset> | \n"
		      "                               -slot <bus-nr> <device-nr> <function-nr> [config | <bar-nr>] <offset> | \n"
		      "                               -memory <address> \n"
		      "                             ] <size>\n"
		      "       -V: Be verbose.\n"
		      "       -h: Interpret all following numerical values as hexadecimal.\n"
		      "       -d: Interpret all following numerical values as decimal.\n"
		      "       -o: Interpret all following numerical values as octal.\n"
		      "       -p: Interpret all following numerical values according to their prefix (default).\n"
		      "       -loopi: The inner loop count. How often each pattern combination is written to each offset.\n"
		      "       -loopo: The outer loop. How often the succession of tests is performed in total.\n"
 "       -id <vendor-id> <device-id> <card-nr>: Access card with given vendor and device id. Use the nth (n==card-nr) card in the system.\n"
		      "       -slot <bus-nr> <device-nr> <function-nr>: Access device with the given bus/device/function physical PCI device offset.\n"
		      "       config: Specifies to access out the device's configuration space.\n"
		      "       <bar>: Specifies the number of the base address register to access.\n"
		      "       <offset>: The offset to read from/write to.\n"
		      "       <address>: The physical memory address to access\n"
		      "       <size>: The number of bytes to read/write. Only 1, 2, and 4 are allowed here.\n"
		      "By default all numerical values are interpreted according to their prefix, '0x' for hexadecimal, `0` for octal, anything else for decimal.\n"
		      "This can be overridden with the -h/-d/-o options.\n";


    if ( !strncmp( argv[0]+strlen(argv[0])-strlen("pci_config_test"), "pci_config_test", strlen("pci_config_test") ) )
	{
	mode = 1;
	usage = usage_cnf;
	args_needed = 4;
	}
    else if ( !strncmp( argv[0]+strlen(argv[0])-strlen("pci_device_test"), "pci_device_test", strlen("pci_device_test") ) )
	{
	mode = 2;
	usage = usage_bar;
	args_needed = 5;
	}
    else if ( !strncmp( argv[0]+strlen(argv[0])-strlen("mem_test"), "mem_test", strlen("mem_test") ) )
	{
	mode = 3;
	usage = usage_mem;
	args_needed = 1;
	}
    else
	{
	mode = 0;
	usage = usage_gen;
	args_needed = 5;
	}


    /* Parse the command line arguments. */
    i = 1;
    error = 0;
    while ( i < argc )
	{
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    verbose = 1;
	    }
	else if ( !strcmp( argv[i], "-h" ) )
	    {
	    base = 16;
	    }
	else if ( !strcmp( argv[i], "-d" ) )
	    {
	    base = 10;
	    }
	else if ( !strcmp( argv[i], "-o" ) )
	    {
	    base = 8;
	    }
	else if ( !strcmp( argv[i], "-p" ) )
	    {
	    base = 0;
	    }
	else if ( !strcmp( argv[i], "-loopi" ) )
	    {
	    if ( i+1 >= argc )
		{
		errorP = "Missing arguments for inner loop.";
		errorArg = i;
		}
	    inner_loop = strtoul( argv[i+1], &convErrP, base );
	    if ( *convErrP )
		{
		errorP = "Error converting inner loop count.";
		errorArg = i+1;
		break;
		}
	    i++;
	    }
	else if ( !strcmp( argv[i], "-loopo" ) )
	    {
	    if ( i+1 >= argc )
		{
		errorP = "Missing arguments for outer loop.";
		errorArg = i;
		}
	    outer_loop = strtoul( argv[i+1], &convErrP, base );
	    if ( *convErrP )
		{
		errorP = "Error converting outer loop count.";
		errorArg = i+1;
		break;
		}
	    i++;
	    }
	else if ( (!strcmp( argv[i], "-id" ) || !strcmp( argv[i], "-slot" )) &&
		  ( mode==0 || mode==1 || mode==2) )
	    {
	    if ( i+args_needed >= argc )
		{
		errorP = "Missing arguments for parameter.";
		errorArg = i;
		}
	    if ( !strcmp( argv[i], "-id" ) )
		{
		useid = 1;
		vendor = strtoul( argv[i+1], &convErrP, base );
		if ( *convErrP )
		    {
		    errorP = "Error converting vendor ID.";
		    errorArg = i+1;
		    break;
		    }
		device = strtoul( argv[i+2], &convErrP, base );
		if ( *convErrP )
		    {
		    errorP = "Error converting device ID.";
		    errorArg = i+2;
		    break;
		    }
		nr = strtoul( argv[i+3], &convErrP, base );
		if ( *convErrP )
		    {
		    errorP = "Error converting card nr.";
		    errorArg = i+3;
		    break;
		    }
		i += 4;
		}
	    else 
		{
		usephys = 1;
		pbus = strtoul( argv[i+1], &convErrP, base );
		if ( *convErrP )
		    {
		    errorP = "Error converting bus nr.";
		    errorArg = i+1;
		    break;
		    }
		pdev = strtoul( argv[i+2], &convErrP, base );
		if ( *convErrP )
		    {
		    errorP = "Error converting device nr.";
		    errorArg = i+2;
		    break;
		    }
		pfunc = strtoul( argv[i+3], &convErrP, base );
		if ( *convErrP )
		    {
		    errorP = "Error converting function nr.";
		    errorArg = i+3;
		    break;
		    }
		i += 4;
		}
	    if ( mode==0 )
		{
		if ( !strcmp( argv[i], "config" ) )
		    {
		    i++;
		    mode = 1;
		    }
		else
		    {
		    mode = 2;
		    }
		}
	    if ( mode==2 )
		{
		bar = strtoul( argv[i], &convErrP, base );
		if ( *convErrP )
		    {
		    errorP = "Error converting bar.";
		    errorArg = i;
		    break;
		    }
		i++;
		}
	    if ( mode==1 || mode==2 )
		{
		offset = strtoul( argv[i], &convErrP, base );
		if ( *convErrP )
		    {
		    errorP = "Error converting offset.";
		    errorArg = i;
		    break;
		    }
		i++;
		}
	    i--;
	    }
	else if ( !strcmp( argv[i], "-memory" ) &&
		  ( mode==0 || mode==3) )
	    {
	    mode=3;
	    usemem = 1;
	    if ( i+1 >= argc )
		{
		errorP = "Missing arguments for -slots parameter.";
		printf( "%d %d %d\n", i, i+3, argc );
		break;
		}
	    offset = strtoul( argv[i+1], &convErrP, base );
	    if ( *convErrP )
		{
		errorP = "Error converting address.";
		errorArg = i;
		break;
		}
	    i++;
	    }
	else
	    {
	    address_seen = 1;
	    size = strtoul( argv[i], &convErrP, base );
	    if ( *convErrP )
		{
		errorP = "Error converting size.";
		errorArg = i;
		break;
		}
	    i++;
	    }
	i++;
	}

    if ( !errorP )
	{
	if ( !address_seen )
	    {
	    errorP = "Must specify size parameter.\n";
	    }
	else if ( useid+usephys+usemem!=1 )
	    errorP = "Must specify one of either -id, -slot, or -memory parameter.";
	}

    if ( errorP )
	{
	fprintf( stderr, usage, argv[0] );
	fprintf( stderr, "Error: %s\n", errorP );
	if ( errorArg>=0 )
	    printf( "Offending argument: %s\n", argv[errorArg] );
	return -1;
	}
	

    /* Build the device name of the board according to the passed parameters. */
    if ( usemem )
	{
	sprintf( deviceName, "/dev/psi/physmem/0x%08lX", (long) offset );
	}
    else if ( useid )
	{
	switch ( mode )
	    {
	    case 1: sprintf( deviceName, "/dev/psi/vendor/0x%04lX/device/0x%04lX/0x%04lX/conf", (unsigned long)vendor, (unsigned long)device, (unsigned long)nr ); break;
	    case 2: sprintf( deviceName, "/dev/psi/vendor/0x%04lX/device/0x%04lX/0x%04lX/base%lu", (unsigned long)vendor, (unsigned long)device, (unsigned long)nr, (unsigned long)bar ); break;
	    }
	}
    else
	{
	switch ( mode )
	    {
	    case 1: sprintf( deviceName, "/dev/psi/bus/0x%04lX/slot/0x%04lX/function/0x%04lX/conf", (unsigned long)pbus, (unsigned long)pdev, (unsigned long)pfunc ); break;
	    case 2: sprintf( deviceName, "/dev/psi/bus/0x%04lX/slot/0x%04lX/function/0x%04lX/base%lu", (unsigned long)pbus, (unsigned long)pdev, (unsigned long)pfunc, (unsigned long)bar ); break;
	    }
	}

    psiStatus = PSI_openRegion( &region, deviceName );
    if ( psiStatus != PSI_OK )
	{
	fprintf( stderr, "PSI Error opening region '%s': %s (%d)\n", deviceName, PSI_strerror(psiStatus),
		 (int)psiStatus );
	return -1;
	}

    if ( usemem )
	{
	s = (unsigned long)size;
	psiStatus = PSI_sizeRegion( region, &s );
	if ( psiStatus == PSI_SIZE_SET && s >= (unsigned long)size )
	    psiStatus = PSI_OK;
	if ( psiStatus != PSI_OK )
	    {
	    fprintf( stderr, "PSI Error sizing region '%s' to %lu bytes: %s (%d) - Resulting size: %lu\n", deviceName, (unsigned long)size, 
		     PSI_strerror(psiStatus), (int)psiStatus, s );
	    return -1;
	    }
	printf( "Sized region to %lu of %lu bytes.\n", s, (unsigned long)size );
	}

    for ( olc = 0; olc < outer_loop; olc++ )
	{
	for ( test = 0; test < testcount; test++ )
	    {
	    /* Provide the user with some information about what we are going to do... */
	    printf( "Performing test %d (%s) on 0x%08lX / %lu bytes from offset 0x%08lX / %lu.\n", test, testnames[test], (long) size, (long) size, (long) offset, (long) offset );
	    
	    PTL_testRegion( region, offset, size, test, inner_loop, &compareCallback );

	    }
	
	}
    PSI_closeRegion( region );
	
    return 0;
    }


void compareCallback( void* writevalue, void* readvalue, void* rereadvalue, 
		      tRegion region, unsigned long offset, 
		      unsigned long size )
    {
    // Dummy tests to get rid of unused parameter warnings...
    // REMOVE THIS of offset or region have to be used below!!!
    if ( offset )
	offset = 0;
    if ( region )
	region = 0;
    unsigned long w, r, rr;
    switch ( size )
	{
	case 1: 
	    w = (unsigned long)*((u8*)writevalue);
	    r = (unsigned long)*((u8*)readvalue);
	    rr = (unsigned long)*((u8*)rereadvalue);
	    break;
	case 2: 
	    w = (unsigned long)*((u16*)writevalue);
	    r = (unsigned long)*((u16*)readvalue);
	    rr = (unsigned long)*((u16*)rereadvalue);
	    break;
	case 4: 
	    w = (unsigned long)*((u32*)writevalue);
	    r = (unsigned long)*((u32*)readvalue);
	    rr = (unsigned long)*((u32*)rereadvalue);
	    break;
	}
    fprintf( stderr, "ERROR: Read back pattern does not match written pattern.\n" );
    fprintf( stderr, "Pattern written; 0x%08lX / %lu.\n", w, w );
    fprintf( stderr, "Pattern read; 0x%08lX / %lu.\n", r, r );
    fprintf( stderr, "Pattern reread; 0x%08lX / %lu.\n", rr, rr );
    fprintf( stderr, "XOR: written/read 0x%08lX / %lu.\n", (w ^ r), (w ^ r ) );
    fprintf( stderr, "XOR: written/reread 0x%08lX / %lu.\n", (w ^ rr), (w ^ rr ) );
    fprintf( stderr, "XOR: read/reread 0x%08lX / %lu.\n", (r ^ rr), (r ^ rr ) );
    }
