/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: list_pci.c 862 2006-02-17 18:45:24Z timm $ 
**
***************************************************************************
*/

#include <psiToolLib.h>
#include <pcitypes.h>
#include <psi.h>
#include <psi_error.h>
#include <string.h>
#include <stdlib.h>

int main( int argc, char** argv )
    {
    u16 vendor, device, nr;
    u16 pbus, pdev, pfunc;
    int i;
    int read_all = 0;
    int useid = 0, usephys = 0;
    int read_size = 0;
    PCI_BAR bars[6];

    PSI_Status psiStatus;
    PCI_CSR csr;

    char* errorP = NULL;
    char* convErrP;
    int errorArg = -1;
    int base = 0;

    char* usage     = "Usage: %s (-h) (-d) (-o) (-p) (-s) [ \n"
		      "                               -id <vendor-id> <device-id> <card-nr> | \n"
		      "                               -slot <bus-nr> <device-nr> <function-nr> | \n"
		      "                               -A \n"
		      "                             ]\n"
		      "       -h: Interpret all following numerical values as hexadecimal.\n"
		      "       -d: Interpret all following numerical values as decimal.\n"
		      "       -o: Interpret all following numerical values as octal.\n"
		      "       -p: Interpret all following numerical values according to their prefix (default).\n"
		      "       -s: Determine and print out the sizes used by the bars.\n"
 "       -id <vendor-id> <device-id> <card-nr>: Access card with given vendor and device id. Use the nth (n==card-nr) card in the system.\n"
		      "       -slot <bus-nr> <device-nr> <function-nr>: Access device with the given bus/device/function physical PCI device offset.\n"
		      "       -A: Read out the CSRs of all devices present.\n\n"
		      "By default all numerical values are interpreted according to their prefix, '0x' for hexadecimal, `0` for octal, anything else for decimal.\n"
		      "This can be overridden with the -h/-d/-o options.\n";

    /* Parse the command line arguments. */
    i = 1;
    while ( i < argc )
	{
	if ( !strcmp( argv[i], "-h" ) )
	    {
	    base = 16;
	    }
	else if ( !strcmp( argv[i], "-d" ) )
	    {
	    base = 10;
	    }
	else if ( !strcmp( argv[i], "-o" ) )
	    {
	    base = 8;
	    }
	else if ( !strcmp( argv[i], "-p" ) )
	    {
	    base = 0;
	    }
	else if ( !strcmp( argv[i], "-s" ) )
	    {
	    read_size = 1;
	    }
 	else if ( !strcmp( argv[i], "-id" ) )
	    {
	    if ( i+3 >= argc )
		{
		errorP = "Missing arguments for -id parameter.";
		errorArg = i;
		}
	    useid = 1;
	    vendor = strtoul( argv[i+1], &convErrP, base );
	    if ( *convErrP )
		{
		errorP = "Error converting vendor ID.";
		errorArg = i+1;
		break;
		}
	    device = strtoul( argv[i+2], &convErrP, base );
	    if ( *convErrP )
		{
		errorP = "Error converting device ID.";
		errorArg = i+2;
		break;
		}
	    nr = strtoul( argv[i+3], &convErrP, base );
	    if ( *convErrP )
		{
		errorP = "Error converting card nr.";
		errorArg = i+3;
		break;
		}
	    i += 3;
	    }
	else if ( !strcmp( argv[i], "-slot" ) )
	    {
	    if ( i+3 >= argc )
		{
		errorP = "Missing arguments for -slot parameter.";
		errorArg = i;
		}
	    usephys = 1;
	    pbus = strtoul( argv[i+1], &convErrP, base );
	    if ( *convErrP )
		{
		errorP = "Error converting bus nr.";
		errorArg = i+1;
		break;
		}
	    pdev = strtoul( argv[i+2], &convErrP, base );
	    if ( *convErrP )
		{
		errorP = "Error converting device nr.";
		errorArg = i+2;
		break;
		}
	    pfunc = strtoul( argv[i+3], &convErrP, base );
	    if ( *convErrP )
		{
		errorP = "Error converting function nr.";
		errorArg = i+3;
		break;
		}
	    i += 3;
	    }
	else if ( !strcmp( argv[i], "-A" ) )
	    {
	    read_all=1;
	    }
	else
	    {
	    errorP = "Unknown parameter.";
	    errorArg = i;
	    break;
	    }
	i++;
	}
    
    if ( !errorP )
	{
	if ( useid+usephys+read_all!=1 )
	    errorP = "Must specify one of either -id, -slot, or -A parameter.";
	}

    if ( errorP )
	{
	fprintf( stderr, usage, argv[0] );
	fprintf( stderr, "Error: %s\n", errorP );
	if ( errorArg>=0 )
	    printf( "Offending argument: %s\n", argv[errorArg] );
	return -1;
	}

    if ( useid )
	{
	psiStatus = PTL_read_log_pci_csr( vendor, device, nr, &csr );
	if ( psiStatus != PSI_OK )
	    {
	    fprintf( stderr, "Error reading CSR from logical device 0x%04hX/0x%04hX/0x%08X: %s (%d)\n",
		     vendor, device, nr, PSI_strerror(psiStatus), (int)psiStatus );
	    return -1;
	    }
	PTL_print_pci_csr( &csr );
	if ( read_size )
	    {
	    psiStatus = PTL_find_pci_device( vendor, device, nr, &pbus, &pdev, &pfunc );
	    if ( psiStatus != PSI_OK )
		{
		fprintf( stderr, "Error finding logical device 0x%04hX/0x%04hX/0x%08X: %s (%d)\n",
			 vendor, device, nr, PSI_strerror(psiStatus), (int)psiStatus );
		return -1;
		}
	    psiStatus = PTL_get_device_bars( pbus, pdev, pfunc, bars );
	    if ( psiStatus != PSI_OK )
		{
		fprintf( stderr, "Error reading bars from logical device 0x%04hX/0x%04hX/0x%08X: %s (%d)\n",
			 vendor, device, nr, PSI_strerror(psiStatus), (int)psiStatus );
		return -1;
		}
	    for ( i = 0; i < 6; i++ )
		{
		if ( bars[i].fSize>0 )
		    PTL_print_bar( bars+i );
		}
	    }
	}

    if ( usephys )
	{
	psiStatus = PTL_read_phys_pci_csr( pbus, pdev, pfunc, &csr );
	if ( psiStatus != PSI_OK )
	    {
	    fprintf( stderr, "Error reading CSR from physical device 0x%04hX/0x%04hX/0x%04hX: %s (%d)\n",
		     pbus, pdev, pfunc, PSI_strerror(psiStatus), (int)psiStatus );
	    return -1;
	    }
	PTL_print_pci_csr( &csr );
	if ( read_size )
	    {
	    psiStatus = PTL_get_device_bars( pbus, pdev, pfunc, bars );
	    if ( psiStatus != PSI_OK )
		{
		fprintf( stderr, "Error reading bars from logical device 0x%04hX/0x%04hX/0x%08X: %s (%d)\n",
			 vendor, device, nr, PSI_strerror(psiStatus), (int)psiStatus );
		return -1;
		}
	    for ( i = 0; i < 6; i++ )
		{
		if ( bars[i].fSize>0 )
		    PTL_print_bar( bars+i );
		}
	    }
	}

    if ( read_all )
	{
	/* Loop over all the possible busses, devices and functions. */
	for ( pbus = 0; pbus < PCI_MAX_PHYS_BUS_NR; pbus++ )
	    {
	    for ( pdev = 0; pdev < PCI_MAX_PHYS_DEV_NR; pdev++ )
		{
		for ( pfunc = 0; pfunc < PCI_MAX_PHYS_FUNC_NR; pfunc++ )
		    {
		    psiStatus = PTL_read_phys_pci_csr( pbus, pdev, pfunc, &csr );
		    if ( psiStatus != PSI_OK )
			continue;
		    if ( csr.fVendorID_req!=0xFFFF && csr.fDeviceID_req!=0xFFFF )
			{
			printf( "Bus/Device/Function: 0x%02hX /  0x%02hX / 0x%02hX: \n",
				pbus, pdev, pfunc );
			PTL_print_pci_csr( &csr );
			if ( read_size )
			    {
			    psiStatus = PTL_get_device_bars( pbus, pdev, pfunc, bars );
			    if ( psiStatus == PSI_OK )
				{
				for ( i = 0; i < 6; i++ )
				    {
				    if ( bars[i].fSize > 0 )
					PTL_print_bar( bars+i );
				    }
				}
			    }
			printf( "\n" );
			}
		    }
		}
	    }
	}

    return 0;
    }


/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: list_pci.c 862 2006-02-17 18:45:24Z timm $ 
**
***************************************************************************
*/
