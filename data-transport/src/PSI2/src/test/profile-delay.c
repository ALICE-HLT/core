#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <limits.h>
#include <sched.h>
#include <errno.h>

#include <psi.h>

#define N_RUNS 1000
#define PRINT_RUNS (N_RUNS/10)
#define N_REPEAT 1000
#define SLEEP_NSEC 1500



int main( void )
{
  int i, j, n, val, min, max;
  long sum;
  struct timeval a, b;
  struct sched_param sched;
  
  printf( "profiling %d times %d nanosecond delay (%d milliseconds) ...\n", 
	  N_REPEAT, SLEEP_NSEC, ((N_REPEAT * SLEEP_NSEC)+999)/1000 );

  sched.sched_priority = 99;
  if ( sched_setscheduler( 0, SCHED_FIFO, & sched ) ) {
    i = errno;
    printf( "failed to set scheduling policy: %s\n", strerror( i ) );
    printf( "using default ...\n" );
  }
  
  sum = 0; n = 0; min = INT_MAX; max = INT_MIN;
  for ( i = 0; i < N_RUNS; ++i ) {
    gettimeofday( & a, 0 );
    for ( j = 0; j < N_REPEAT; ++j ) PSI_delay( SLEEP_NSEC );
    gettimeofday( & b, 0 );
    if ( n % PRINT_RUNS == 0 ) {
      printf( "." );
      fflush( stdout );
    }
    if ( b.tv_sec < a.tv_sec ) continue;
    val = ((b.tv_sec - a.tv_sec) * 1000000 + b.tv_usec) - a.tv_usec;
    if ( val > max ) max = val;
    if ( val < min ) min = val;
    sum += val; n++;
  }

  printf( "\nmean delay time in microseconds: %lf\n", 
          (double) sum / (double) n );
  printf( "count: %d, min: %d, max: %d\n", n, min, max );

  return 0;
}

