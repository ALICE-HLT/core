/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: memory.c 862 2006-02-17 18:45:24Z timm $ 
**
***************************************************************************
*/

#include "psiToolLib.h"
#include <psi_error.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <asm/mtrr.h>
#include <errno.h>
#include <sys/ioctl.h>

/*
** Determine the amount of physical memory available in the system.
*/
PSI_Status PTL_get_memory_size( unsigned long* memsize )
    {
    int fh;
    struct mtrr_gentry mtrr;
    int i;
    
    fh = open( "/proc/mtrr", O_RDONLY );
    if ( fh==-1 )
	{
	errno = PSI_SYS_ERRNO+ENODEV;
	return 0;
	}
    i = 0;
    while ( i >= 0 )
	{
	mtrr.regnum = i++;
	if ( ioctl( fh, MTRRIOC_GET_ENTRY, &mtrr )==-1 )
	    i = -1;
	if ( mtrr.size && !mtrr.base )
	    {
	    close( fh );
	    *memsize = mtrr.size;
	    return PSI_OK;
	    }
	if ( !mtrr.size )
	    i = -1;
	}
    close( fh );
    errno = PSI_SYS_ERRNO+ENXIO;
    return 0;
    }




/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: memory.c 862 2006-02-17 18:45:24Z timm $ 
**
***************************************************************************
*/
