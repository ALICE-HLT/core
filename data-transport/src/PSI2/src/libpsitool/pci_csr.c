/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: pci_csr.c 862 2006-02-17 18:45:24Z timm $ 
**
***************************************************************************
*/

#include "psiToolLib.h"
#include <psi.h>
#include <psi_error.h>
#include <sched.h>
#include <sys/types.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>


#define IsBitSet(bit) (bit ? '+' : '-')


/*
** Print out a decoded PCI configuration space.
** This function will determine wether it is a PCI device
** or bridge CSR.
*/
void PTL_print_pci_csr( PCI_CSR* csr )
    {
    PTL_fprint_pci_csr( stdout, csr );
    }

void PTL_fprint_pci_csr( FILE* file, PCI_CSR* csr )
    {
    if ( (csr->fClassCode_req & 0xFFFF00) == 0x060400 )
	PTL_fprint_pci_bridge_csr( file, (PCI_BRIDGE_CSR*)csr );
    else
	PTL_fprint_pci_device_csr( file, csr );
    }


/*
** Print out a decoded PCI device configuration space.
*/
void PTL_print_pci_device_csr( PCI_CSR* csr )
    {
    PTL_fprint_pci_device_csr( stdout, csr );
    }

void PTL_fprint_pci_device_csr( FILE* file, PCI_CSR* csr )
    {
    int i = 0 ;
    fprintf( file, "PCI bus device CSR\n" );
    fprintf( file, "  fVendorID_req     0x%04X\n", csr->fVendorID_req  );
    fprintf( file, "  fDeviceID_req     0x%04X\n", csr->fDeviceID_req  );
    fprintf( file, "  fCommandReg_req   0x%04X", csr->fCommandReg_req  );
    fprintf( file, " (IO%c Mem%c Master%c SpecialCycle%c MemWriteInv%c VGASnoop%c ParityErr%c Wait%c SysErr%c FastB2B%c)\n",
	     IsBitSet(csr->fCommandReg_req & 1), IsBitSet(csr->fCommandReg_req & 2), IsBitSet(csr->fCommandReg_req & 4), 
	     IsBitSet(csr->fCommandReg_req & 8), IsBitSet(csr->fCommandReg_req & 16), IsBitSet(csr->fCommandReg_req & 32), 
	     IsBitSet(csr->fCommandReg_req & 64), IsBitSet(csr->fCommandReg_req & 128), IsBitSet(csr->fCommandReg_req & 256),
	     IsBitSet(csr->fCommandReg_req & 512) );
    fprintf( file, "\n" );
    fprintf( file, "  fStatusReg_req    0x%04X", csr->fStatusReg_req  );
    fprintf( file, " (66MHz%c UDF%c FastB2B%c DataParity%c Devsel-",
	     IsBitSet(csr->fStatusReg_req & 32), IsBitSet(csr->fStatusReg_req & 64), 
	     IsBitSet(csr->fStatusReg_req & 128), IsBitSet(csr->fStatusReg_req & 256) );
    if ( csr->fStatusReg_req & 1024 )
	fprintf( file, "slow" );
    else if ( csr->fStatusReg_req & 512 )
	fprintf( file, "medium" );
    else
	fprintf( file, "fast" );
    fprintf( file, " SigTargetAbort%c RecvTargetAbort%c RecvMasterAbort%c SysErr%c ParityErr%c)",
	     IsBitSet(csr->fStatusReg_req & 2048), IsBitSet(csr->fStatusReg_req & 4096), 
	     IsBitSet(csr->fStatusReg_req & 8192), IsBitSet(csr->fStatusReg_req & 16384), 
	     IsBitSet(csr->fStatusReg_req & 32768) );
    fprintf( file, "\n" ); 
    fprintf( file, "  fRevID_req        0x%02X\n", csr->fRevID_req  );
    fprintf( file, "  fClassCode_req    0x%06X (", (unsigned int)csr->fClassCode_req );
    switch ( (csr->fClassCode_req & 0xFF0000) >> 16 )
	{
	case 0: 
	    fprintf( file, "Old device/" );
	    if ( csr->fClassCode_req & 0x000100 )
		fprintf( file, "VGA compatible" );
	    else
		fprintf( file, "Generic" );
	    break;
	case 1:
	    fprintf( file, "Mass Storage Controller/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: fprintf( file, "SCSI" ); break;
		case 1: 
		    fprintf( file, "IDE" );
		    fprintf( file, " (OpModePrim%c ProgIndPrim%c OpModeSec%c ProgIndSec%c Master%c)",
			     IsBitSet(csr->fClassCode_req & 1), IsBitSet(csr->fClassCode_req & 2),
			     IsBitSet(csr->fClassCode_req & 4), IsBitSet(csr->fClassCode_req & 8),
			     IsBitSet(csr->fClassCode_req & 128) );
		    break;
		case 2: fprintf( file, "Floppy Disk" ); break;
		case 3: fprintf( file, "IPI" ); break;
		case 4: fprintf( file, "RAID" ); break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 2: 
	    fprintf( file, "Network Controller/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: fprintf( file, "Ethernet" ); break;
		case 1: fprintf( file, "Token Ring" ); break;
		case 2: fprintf( file, "FDDI" ); break;
		case 3: fprintf( file, "ATM" ); break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 3: 
	    fprintf( file, "Display Controllers/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: 
		    if ( csr->fClassCode_req & 1 )
			fprintf( file, "8514 compatible" );
		    else
			fprintf( file, "GVA compatible" );
		    break;
		case 1: fprintf( file, "XGA" ); break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 4: 
	    fprintf( file, "Multimedia Device/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: fprintf( file, "Video" ); break;
		case 1: fprintf( file, "Audio" ); break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 5: 
	    fprintf( file, "Memory Controller/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: fprintf( file, "RAM" ); break;
		case 1: fprintf( file, "Flash" ); break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 6: /* We leave this here, as the function might be called directly. */
	    fprintf( file, "Bridge/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: fprintf( file, "Host-PCI" ); break;
		case 1: fprintf( file, "PCI-ISA" ); break;
		case 2: fprintf( file, "PCI-EISA" ); break;
		case 3: fprintf( file, "PCI/MCA" ); break;
		case 4: fprintf( file, "PCI-PCI" ); break;
		case 5: fprintf( file, "PCI-PCMCIA" ); break;
		case 6: fprintf( file, "PCI-NuBus" ); break;
		case 7: fprintf( file, "PCI-CardBus" ); break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 7:
	    fprintf( file, "Simple Com. Controller/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: 
		    switch ( (csr->fClassCode_req & 0x0000FF) )
			{
			case 0: fprintf( file, "XT serial" ); break;
			case 1: fprintf( file, "16450 serial" ); break;
			case 2: fprintf( file, "16550 serial" ); break;
			default: fprintf( file, "Other serial" ); break;
			}
		    break;
		case 1:
		    switch ( (csr->fClassCode_req & 0x0000FF) )
			{
			case 0: fprintf( file, "Unidir. parallel" ); break;
			case 1: fprintf( file, "Bidir. parallel" ); break;
			case 2: fprintf( file, "TCP 1.X parallel" ); break;
			default: fprintf( file, "Other parallel" ); break;
			}
		    break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 8: 
	    fprintf( file, "Base System Peripheral" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: 
		    switch ( (csr->fClassCode_req & 0x0000FF) )
			{
			case 0: fprintf( file, "8259 PIC" ); break;
			case 1: fprintf( file, "ISA PIC" ); break;
			case 2: fprintf( file, "EISA PIC" ); break;
			default: fprintf( file, "Other PIC" ); break;
			}
		    break;
		case 1:
		    switch ( (csr->fClassCode_req & 0x0000FF) )
			{
			case 0: fprintf( file, "8237 DMA controller" ); break;
			case 1: fprintf( file, "ISA DMA controller" ); break;
			case 2: fprintf( file, "EISA DMA controller" ); break;
			default: fprintf( file, "Other DMA controller" ); break;
			}
		    break;
		case 2: 
		    switch ( (csr->fClassCode_req & 0x0000FF) )
			{
			case 0: fprintf( file, "8254 timer" ); break;
			case 1: fprintf( file, "ISA system timer" ); break;
			case 2: fprintf( file, "EISA system timer" ); break;
			default: fprintf( file, "Other timer" ); break;
			}
		    break;
		case 3:
		    switch ( (csr->fClassCode_req & 0x0000FF) )
			{
			case 0: fprintf( file, "RTC controller" ); break;
			case 1: fprintf( file, "ISA RTC controller" ); break;
			default: fprintf( file, "Other RTC controller "); break;
			}
		    break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 9: 
	    fprintf( file, "Input Device/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: fprintf( file, "Keyboard controller" ); break;
		case 1: fprintf( file, "Digitizer/Pen" ); break;
		case 2: fprintf( file, "Mouse controller" ); break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 0xA:
	    fprintf( file, "Docking Station/" );
	    if ( !(csr->fClassCode_req & 0x00FF00) >> 8 )
		fprintf( file, "Generic" );
	    else
		fprintf( file, "Other" );
	    break;
	case 0xB:
	    fprintf( file, "Processor/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: fprintf( file, "386" ); break;
		case 1: fprintf( file, "486" ); break;
		case 2: fprintf( file, "Pentium" ); break;
		case 0x10: fprintf( file, "Alpha" ); break;
		case 0x20: fprintf( file, "PowerPC" ); break;
		case 0x40: fprintf( file, "Co-" ); break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 0xC:
	    fprintf( file, "Serial Bus Controllers/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: fprintf( file, "Firewire/IEEE 1394" ); break;
		case 1: fprintf( file, "ACCESS.bus" ); break;
		case 2: fprintf( file, "SSA" ); break;
		case 3: fprintf( file, "USB" ); break;
		case 4: fprintf( file, "Fibre Channel" ); break;
		default: fprintf( file, "Other" );
		}
	    break;
	case 0xFF: fprintf( file, "Other" ); break;
	default: fprintf( file, "Unknown" );
	}
    fprintf( file, ")\n" );
    fprintf( file, "  fCacheLineSize    0x%02X\n", csr->fCacheLineSize  );
    fprintf( file, "  fLatencyTimer     0x%02X\n", csr->fLatencyTimer  );
    fprintf( file, "  fHeadertype_req   0x%02X\n", csr-> fHeadertype_req );
    fprintf( file, "  fBIST             0x%02X\n", csr-> fBIST );
    for(i = 0 ; i < 6 ; i++)
	fprintf( file, "  fBaseAddr[%d]      0x%08X\n",i, csr->fBaseAddr[i] );
    fprintf( file, "  fCISPtr           0x%08X\n", csr->fCISPtr  );
    fprintf( file, "  fSubVendorID      0x%04X\n", csr->fSubVendorID  );
    fprintf( file, "  fSubSystemID      0x%04X\n", csr->fSubSystemID  );
    fprintf( file, "  fROMBaseAddr      0x%08X\n", csr-> fROMBaseAddr );
    for(i = 0 ; i < 2 ; i++)
	fprintf( file, "  fReserved[%d]      0x%08X\n",i, csr->fReserved[i]  );
    fprintf( file, "  fIRQLine          0x%02X\n", csr->fIRQLine  );
    fprintf( file, "  fIrqPin           0x%02X\n", csr-> fIrqPin );
    fprintf( file, "  fMinGnt           0x%02X\n", csr-> fMinGnt );
    fprintf( file, "  fMaxLat           0x%02X\n", csr->fMaxLat  );
    }

/*
** Print out a decoded PCI bridge configuration space.
*/
void PTL_print_pci_bridge_csr( PCI_BRIDGE_CSR* csr )
    {
    PTL_fprint_pci_bridge_csr( stdout, csr );
    }

void PTL_fprint_pci_bridge_csr( FILE* file, PCI_BRIDGE_CSR* csr )
    {
    int i = 0 ;
    fprintf( file, "PCI bus bridge CSR\n" );
    fprintf( file, "  fVendorID_req       0x%04X\n", csr->fVendorID_req  );
    fprintf( file, "  fDeviceID_req       0x%04X\n", csr->fDeviceID_req  );
    fprintf( file, "  fCommandReg_req     0x%04X", csr->fCommandReg_req  );
    fprintf( file, " (IO%c Mem%c Master%c SpecialCycle%c MemWriteInv%c VGASnoop%c ParityErr%c Wait%c SysErr%c FastB2B%c)\n",
	     IsBitSet(csr->fCommandReg_req & 1), IsBitSet(csr->fCommandReg_req & 2), IsBitSet(csr->fCommandReg_req & 4), 
	     IsBitSet(csr->fCommandReg_req & 8), IsBitSet(csr->fCommandReg_req & 16), IsBitSet(csr->fCommandReg_req & 32), 
	     IsBitSet(csr->fCommandReg_req & 64), IsBitSet(csr->fCommandReg_req & 128), IsBitSet(csr->fCommandReg_req & 256),
	     IsBitSet(csr->fCommandReg_req & 512) );
    fprintf( file, "  fStatusReg_req    0x%04X", csr->fStatusReg_req  );
    fprintf( file, " (66MHz%c UDF%c FastB2B%c DataParity%c Devsel-",
	     IsBitSet(csr->fStatusReg_req & 32), IsBitSet(csr->fStatusReg_req & 64), 
	     IsBitSet(csr->fStatusReg_req & 128), IsBitSet(csr->fStatusReg_req & 256) );
    if ( csr->fStatusReg_req & 1024 )
	fprintf( file, "slow" );
    else if ( csr->fStatusReg_req & 512 )
	fprintf( file, "medium" );
    else
	fprintf( file, "fast" );
    fprintf( file, " SigTargetAbort%c RecvTargetAbort%c RecvMasterAbort%c SysErr%c ParityErr%c)",
	     IsBitSet(csr->fStatusReg_req & 2048), IsBitSet(csr->fStatusReg_req & 4096), 
	     IsBitSet(csr->fStatusReg_req & 8192), IsBitSet(csr->fStatusReg_req & 16384), 
	     IsBitSet(csr->fStatusReg_req & 32768) );
    fprintf( file, "\n" ); 
    fprintf( file, "  fRevID_req          0x%02X\n", csr->fRevID_req  );
    fprintf( file, "  fClassCode_req      0x%06X (", (unsigned int)csr-> fClassCode_req );
    switch ( (csr->fClassCode_req & 0xFF0000) >> 16 )
	{
	case 0: 
	    fprintf( file, "Old device/" );
	    if ( csr->fClassCode_req & 0x000100 )
		fprintf( file, "VGA compatible" );
	    else
		fprintf( file, "Generic" );
	    break;
	case 1:
	    fprintf( file, "Mass Storage Controller/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: fprintf( file, "SCSI" ); break;
		case 1: 
		    fprintf( file, "IDE" );
		    fprintf( file, " (OpModePrim%c ProgIndPrim%c OpModeSec%c ProgIndSec%c Master%c)",
			     IsBitSet(csr->fClassCode_req & 1), IsBitSet(csr->fClassCode_req & 2),
			     IsBitSet(csr->fClassCode_req & 4), IsBitSet(csr->fClassCode_req & 8),
			     IsBitSet(csr->fClassCode_req & 128) );
		    break;
		case 2: fprintf( file, "Floppy Disk" ); break;
		case 3: fprintf( file, "IPI" ); break;
		case 4: fprintf( file, "RAID" ); break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 2: 
	    fprintf( file, "Network Controller/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: fprintf( file, "Ethernet" ); break;
		case 1: fprintf( file, "Token Ring" ); break;
		case 2: fprintf( file, "FDDI" ); break;
		case 3: fprintf( file, "ATM" ); break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 3: 
	    fprintf( file, "Display Controllers/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: 
		    if ( csr->fClassCode_req & 1 )
			fprintf( file, "8514 compatible" );
		    else
			fprintf( file, "GVA compatible" );
		    break;
		case 1: fprintf( file, "XGA" ); break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 4: 
	    fprintf( file, "Multimedia Device/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: fprintf( file, "Video" ); break;
		case 1: fprintf( file, "Audio" ); break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 5: 
	    fprintf( file, "Memory Controller/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: fprintf( file, "RAM" ); break;
		case 1: fprintf( file, "Flash" ); break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 6: /* We leave this here, as the function might be called directly. */
	    fprintf( file, "Bridge/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: fprintf( file, "Host-PCI" ); break;
		case 1: fprintf( file, "PCI-ISA" ); break;
		case 2: fprintf( file, "PCI-EISA" ); break;
		case 3: fprintf( file, "PCI/MCA" ); break;
		case 4: fprintf( file, "PCI-PCI" ); break;
		case 5: fprintf( file, "PCI-PCMCIA" ); break;
		case 6: fprintf( file, "PCI-NuBus" ); break;
		case 7: fprintf( file, "PCI-CardBus" ); break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 7:
	    fprintf( file, "Simple Com. Controller/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: 
		    switch ( (csr->fClassCode_req & 0x0000FF) )
			{
			case 0: fprintf( file, "XT serial" ); break;
			case 1: fprintf( file, "16450 serial" ); break;
			case 2: fprintf( file, "16550 serial" ); break;
			default: fprintf( file, "Other serial" ); break;
			}
		    break;
		case 1:
		    switch ( (csr->fClassCode_req & 0x0000FF) )
			{
			case 0: fprintf( file, "Unidir. parallel" ); break;
			case 1: fprintf( file, "Bidir. parallel" ); break;
			case 2: fprintf( file, "TCP 1.X parallel" ); break;
			default: fprintf( file, "Other parallel" ); break;
			}
		    break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 8: 
	    fprintf( file, "Base System Peripheral" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: 
		    switch ( (csr->fClassCode_req & 0x0000FF) )
			{
			case 0: fprintf( file, "8259 PIC" ); break;
			case 1: fprintf( file, "ISA PIC" ); break;
			case 2: fprintf( file, "EISA PIC" ); break;
			default: fprintf( file, "Other PIC" ); break;
			}
		    break;
		case 1:
		    switch ( (csr->fClassCode_req & 0x0000FF) )
			{
			case 0: fprintf( file, "8237 DMA controller" ); break;
			case 1: fprintf( file, "ISA DMA controller" ); break;
			case 2: fprintf( file, "EISA DMA controller" ); break;
			default: fprintf( file, "Other DMA controller" ); break;
			}
		    break;
		case 2: 
		    switch ( (csr->fClassCode_req & 0x0000FF) )
			{
			case 0: fprintf( file, "8254 timer" ); break;
			case 1: fprintf( file, "ISA system timer" ); break;
			case 2: fprintf( file, "EISA system timer" ); break;
			default: fprintf( file, "Other timer" ); break;
			}
		    break;
		case 3:
		    switch ( (csr->fClassCode_req & 0x0000FF) )
			{
			case 0: fprintf( file, "RTC controller" ); break;
			case 1: fprintf( file, "ISA RTC controller" ); break;
			default: fprintf( file, "Other RTC controller "); break;
			}
		    break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 9: 
	    fprintf( file, "Input Device/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: fprintf( file, "Keyboard controller" ); break;
		case 1: fprintf( file, "Digitizer/Pen" ); break;
		case 2: fprintf( file, "Mouse controller" ); break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 0xA:
	    fprintf( file, "Docking Station/" );
	    if ( !(csr->fClassCode_req & 0x00FF00) >> 8 )
		fprintf( file, "Generic" );
	    else
		fprintf( file, "Other" );
	    break;
	case 0xB:
	    fprintf( file, "Processor/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: fprintf( file, "386" ); break;
		case 1: fprintf( file, "486" ); break;
		case 2: fprintf( file, "Pentium" ); break;
		case 0x10: fprintf( file, "Alpha" ); break;
		case 0x20: fprintf( file, "PowerPC" ); break;
		case 0x40: fprintf( file, "Co-" ); break;
		default: fprintf( file, "Other" ); break;
		}
	    break;
	case 0xC:
	    fprintf( file, "Serial Bus Controllers/" );
	    switch ( (csr->fClassCode_req & 0x00FF00) >> 8 )
		{
		case 0: fprintf( file, "Firewire/IEEE 1394" ); break;
		case 1: fprintf( file, "ACCESS.bus" ); break;
		case 2: fprintf( file, "SSA" ); break;
		case 3: fprintf( file, "USB" ); break;
		case 4: fprintf( file, "Fibre Channel" ); break;
		default: fprintf( file, "Other" );
		}
	    break;
	case 0xFF: fprintf( file, "Other" ); break;
	default: fprintf( file, "Unknown" );
	}
    fprintf( file, ")\n" );
    fprintf( file, "  fCacheLineSize      0x%02X\n", csr->fCacheLineSize  );
    fprintf( file, "  fLatencyTimer       0x%02X\n", csr->fLatencyTimer  );
    fprintf( file, "  fHeadertype_req     0x%02X\n", csr-> fHeadertype_req );
    fprintf( file, "  fBIST               0x%02X\n", csr-> fBIST );
    for(i = 0 ; i < 2 ; i++)
	fprintf( file, "  fBaseAddr[%d]        0x%08X\n",i, csr->fBaseAddr[i] );
    fprintf( file, "  fPrimBusNo_req      0x%02X\n", (unsigned int)csr->fPrimBusNo_req );
    fprintf( file, "  fSecBusNo_req       0x%02X\n", (unsigned int)csr->fSecBusNo_req );
    fprintf( file, "  fSubordBusNo_req    0x%02X\n", (unsigned int)csr->fSubordBusNo_req );
    fprintf( file, "  fSecLatTimer_req    0x%02X\n", (unsigned int)csr->fSecLatTimer_req );
    fprintf( file, "  fIOBase             0x%02X\n", (unsigned int)csr->fIOBase );
    fprintf( file, "  fIOLimit            0x%02X\n", (unsigned int)csr->fIOLimit );
    fprintf( file, "  fSecStatus_req      0x%04X\n", csr->fSecStatus_req );
    fprintf( file, "  fMemBase_req        0x%04X\n", csr->fMemBase_req );
    fprintf( file, "  fMemLimit_req       0x%04X\n", csr->fMemLimit_req );
    fprintf( file, "  fPrefMemBase        0x%04X\n", csr->fPrefMemBase );
    fprintf( file, "  fPrefMemLimit       0x%04X\n", csr->fPrefMemLimit );
    fprintf( file, "  fPrefBaseUp         0x%08X\n", csr->fPrefBaseUp );
    fprintf( file, "  fPrefLimitUp        0x%08X\n", csr->fPrefLimitUp );
    fprintf( file, "  fIOBaseUp           0x%04X\n", csr->fIOBaseUp );
    fprintf( file, "  fIOLimitUp          0x%04X\n", csr->fIOLimitUp );
  
    fprintf( file, "  fReserved           0x%08X\n", csr->fReserved  );
    fprintf( file, "  fROMBaseAddr        0x%08X\n", csr-> fROMBaseAddr );
    fprintf( file, "  fIRQLine            0x%02X\n", csr->fIRQLine  );
    fprintf( file, "  fIrqPin             0x%02X\n", csr-> fIrqPin );
    fprintf( file, "  fBridgeControl_req  0x%04X\n", csr->fBridgeControl_req );
    }

/*
** Read out the configuration space from a logically specified card
** (vendor-/device-ID + card index (of those IDs)).
*/
PSI_Status PTL_read_log_pci_csr( u16 vendID, u16 devID, u32 ndx, PCI_CSR* csr )
    {
    tRegion region;
    PSI_Status psiStatus;
    char name[512];

    sprintf( name, "/dev/psi/vendor/0x%04hX/device/0x%04hX/0x%08lX/conf",
	    (short) vendID, (short) devID, (long) ndx );
    
    psiStatus = PSI_openRegion( &region, name );
    if ( psiStatus != PSI_OK )
	return psiStatus;
    psiStatus = PTL_read_region_pci_csr( region, 0, csr );
    PSI_closeRegion( region );
    return psiStatus;
    }

/*
** Read out the configuration space from a physically specified card
** (bus nr, slot/device nr, function nr).
*/
PSI_Status PTL_read_phys_pci_csr( u16 bus, u16 dev, u16 func, PCI_CSR* csr )
    {
    tRegion region;
    PSI_Status psiStatus;
    char name[512];

    sprintf( name, "/dev/psi/bus/0x%04hX/slot/0x%04hX/function/0x%04hX/conf",
	     bus, dev, func );
    
    psiStatus = PSI_openRegion( &region, name );
    if ( psiStatus != PSI_OK )
	return psiStatus;
    psiStatus = PTL_read_region_pci_csr( region, 0, csr );
    PSI_closeRegion( region );
    return psiStatus;
    }

/*
** Read out the configuration space from an already opened region.
** It is the users responsibility that there is really a configuration space
** in that region. 
*/
PSI_Status PTL_read_region_pci_csr( tRegion region, unsigned long offset, PCI_CSR* csr )
    {
    PSI_Status psiStatus;

    psiStatus = PSI_read( region, offset, 1, 64, csr );
    return psiStatus;
    }


/*
** Read out all the base address registers from the specified device.
*/
PSI_Status PTL_get_device_bars( u16 bus, u16 dev, u16 func, PCI_BAR bar[6] )
    {
    PSI_Status psiStatus;

    PCI_CSR csrData;
    PCI_CSR* csr = &csrData;
    PCI_BRIDGE_CSR *bridgeCSR = (PCI_BRIDGE_CSR*)csr;
    struct sched_param parm;
    u32 mask[6]= { 0xffffffff };
    u32 size[6];
    int tSize,i;
    int barCnt = 0;
    unsigned int classCode;
    char devName[512];
    tRegion region;
    u32 temp;
  
    unsigned long long HILFE = 0;
    memset(mask,0xff,6*4);
    tSize = 0 ;

    sprintf( devName, "/dev/psi/bus/0x%04hX/slot/0x%04hX/function/0x%04hX/conf",
	     bus, dev, func );
    psiStatus = PSI_openRegion( &region, devName );
    if ( psiStatus != PSI_OK )
	return psiStatus;

    psiStatus = PTL_read_region_pci_csr( region, 0, csr );
    if ( psiStatus != PSI_OK )
	{
	PSI_closeRegion( region );
	return psiStatus;
	}
    classCode = csr->fClassCode_req;

#ifdef DEBUG
    printf("PTL_get_device_bars: classCode 0x%06x  sizeof(PCI_BRIDGE_CSR) %d\n",classCode, (int) sizeof(PCI_BRIDGE_CSR)); fflush(stdout);
#endif 

#ifdef DEBUG
    PTL_print_pci_csr( csr );
#endif
    /* strategy: gain control and KEEP control  */
    /* We go to realtime mode and highest priority to prevent another process from
    ** accessing the device while we mess with its address registers. */
    parm.sched_priority = 99;
    sched_setscheduler(0, SCHED_RR,&parm);
    /* begin critical region  */

    /* Disable Memory IO to prevent the device from being addressed while 
    ** we mess with its registers. */
    temp = bridgeCSR->fCommandReg_req & ~2;
    PSI_write( region, 4, 1, 2, &temp );
    if ( (classCode & 0xFF0000) == 0x060400 )
	{
	/* This is a PCI bridge device, which has to be treated differently
	** from an ordinary device. */
	bridgeCSR = (PCI_BRIDGE_CSR*)csr;
	/* Test 2 base addr registers */
	for ( i = 0; i < 2; i++ )
	    {
	    PSI_write( region, 16+i*4, 4, 4, mask );
	    PSI_read( region, 16+i*4, 4, 4, size+i );
	    PSI_write( region, 16+i*4, 4, 4, &(bridgeCSR->fBaseAddr[i]) );
	    }
	// Test prefetchable memory base/limit
	PSI_write( region, 36, 4, 4, mask );
	PSI_read( region, 36, 4, 4, size+3 );
	PSI_write( region, 36, 4, 4, &(bridgeCSR->fPrefMemBase) );
	}
    else
	{
	// This is an ordinary (non-bridge) device
	for ( i = 0; i < 6; i++ )
	    {
	    PSI_write( region, 16+i*4, 4, 4, mask );
	    PSI_read( region, 16+i*4, 4, 4, size+i );
	    PSI_write( region, 16+i*4, 4, 4, &(bridgeCSR->fBaseAddr[i]) );
	    }
	}
    /* Reactivate memory IO. */
    PSI_write( region, 4, 1, 2, &bridgeCSR->fCommandReg_req );

    /* end critical region */
    parm.sched_priority = 0;
    sched_setscheduler(0, SCHED_OTHER,&parm);
    if ( (classCode & 0xFFFF00) == 0x060400 )
	{ /* Determine memory ranges occupied by bridges. */
#ifdef DEBUG
	    printf( "PTL_get_device_bars found a bridge device...at %d\n",__LINE__ ); fflush(stdout);
#endif
	/* unpack sizes  */
	for(i = 0; i < 2; i++)
	    { // for over bridge base addresses
	    bar[barCnt].fBridgeSecBus = -1;
	    bar[barCnt].fBridgePrimBus = -1;
	    if(size[i] & 1)
		{
		/* Bar is IO */
		bar[barCnt].fSize = 0;
		bar[barCnt].fAddress = 0;
		bar[barCnt].fIsMemory = 0;
		barCnt++;
		continue;
		}
	    /* BAR is memory */
	    bar[barCnt].fIsMemory = 1;
	    bar[barCnt].fIsPrefetchable = ((size[i] & 8) ? 1 : 0);
	    if((size[i] & 6) == 4) // 64 bit address !!!!
		{
		HILFE = size[i+1];
		HILFE <<= 32;
		HILFE |= (size[i] & ~0xF);
		HILFE = ~HILFE + 1;
		bar[barCnt].fSize = HILFE;
		bar[barCnt].fAddress = csr->fBaseAddr[i+1];
		bar[barCnt].fAddress <<= 32;
		bar[barCnt].fAddress |= csr->fBaseAddr[i];
		bar[barCnt].fIs64  = 1;
		tSize += bar[barCnt].fSize;
		barCnt++;
		i++;
#ifdef DEBUG
		    printf("PTL_get_device_bars found a 64bit addr!! \n");
#endif
		}
	    else
		{
		bar[barCnt].fSize = ~(size[i] & ~0xF) + 1;
		bar[barCnt].fAddress = csr->fBaseAddr[i] & (~0xf) ;
		bar[barCnt].fIs64  = 0;
		tSize += bar[barCnt].fSize;
		barCnt++;
		}
	    }
	if ( (bridgeCSR->fPrefMemBase & 0xFFF0) <= (bridgeCSR->fPrefMemLimit & 0xFFF0)  ) // (use "||" instead of "<"??)
	    {
	    // We have some prefetchable memory. 
	    // Now see, wether its 64 bit (lower hex bit of base and limit has to be one...)
#ifdef DEBUG
		printf( "PTL_get_device_bars: "
			"fPrefMemBase/fPrefMemLimit masked: "
			"0x%04lX -  0x%04lX\n", 
			(long) (size[3] & 0xFFFF), 
			(long) ((size[3] & 0xFFFF0000)>>16 ) );
#endif 
	    bar[barCnt].fBridgeSecBus = (short)bridgeCSR->fSecBusNo_req;
	    bar[barCnt].fBridgePrimBus = (short)bridgeCSR->fPrimBusNo_req;
	    bar[barCnt].fIsPrefetchable = 1;
	    if ( ((size[3] & 0x000F) == 0x0001) && ((size[3] & 0x000F0000) == 0x00010000) )
		{
		// 64 bit
#ifdef DEBUG
		    printf("PTL_get_device_bars found a 64bit bridge prefetchable memory region!! \n");
		    printf("PTL_get_device_bars: fPrefBaseUp/fPrefLimitup: 0x%08X - 0x%08X\n", 
			   bridgeCSR->fPrefBaseUp, bridgeCSR->fPrefLimitUp );
		    printf("PTL_get_device_bars: fPrefMemBase/fPrefMemLimit: 0x%04X - 0x%04X\n", 
			   bridgeCSR->fPrefMemBase, bridgeCSR->fPrefMemLimit );
#endif 
		bar[barCnt].fAddress = bridgeCSR->fPrefBaseUp;
		bar[barCnt].fAddress <<= 32;
		bar[barCnt].fAddress |= ((bridgeCSR->fPrefMemBase & 0xFFF0) << 16);
		/* Build the size. This is somewhat complicated, as what is actually coded
		** is the address of the last byte. And since it is 64 bit it is scattered
		** over two places. */
		bar[barCnt].fSize = bridgeCSR->fPrefLimitUp;
		bar[barCnt].fSize <<= 32;
		bar[barCnt].fSize |= ((bridgeCSR->fPrefMemLimit & 0xFFF0) << 16);
		bar[barCnt].fSize |= 0xFFFFF;
		bar[barCnt].fSize += 1;
		bar[barCnt].fSize -= bar[barCnt].fAddress;
		tSize += bar[barCnt].fSize;
		barCnt++;
		}
	    if ( !(size[3] & 0x000F) && !(size[3] & 0x000F0000) )
		{
		// Only 32 bit
#ifdef DEBUG
		    printf("PTL_get_device_bars: fPrefMemBase/fPrefMemLimit: 0x%04X - 0x%04X\n", 
			   bridgeCSR->fPrefMemBase, bridgeCSR->fPrefMemLimit );
#endif
		bar[barCnt].fAddress = (bridgeCSR->fPrefMemBase & 0xFFF0);
		bar[barCnt].fAddress <<= 16;
		/* Build the size. This is somewhat complicated, as what is actually coded
		** is the address of the last byte. */
		bar[barCnt].fSize = (bridgeCSR->fPrefMemLimit & 0xFFF0);
		bar[barCnt].fSize <<= 16;
		bar[barCnt].fSize |= 0xFFFFF;
		bar[barCnt].fSize += 1;
		bar[barCnt].fSize -= bar[barCnt].fAddress;
		tSize += bar[barCnt].fSize;
		barCnt++;
		}
	    }
	else
	    {
	    bar[barCnt].fBridgeSecBus = (short)bridgeCSR->fSecBusNo_req;
	    bar[barCnt].fBridgePrimBus = (short)bridgeCSR->fPrimBusNo_req;
	    bar[barCnt].fIsPrefetchable = 1;
	    bar[barCnt].fIs64 = 0;
	    bar[barCnt].fAddress = bar[barCnt].fSize = 0;
	    barCnt++;
	    }
	if ( bridgeCSR->fMemBase_req <= bridgeCSR->fMemLimit_req ) // (use "||" instead of "<"??)
	    {
	    // We have found a bridge memory region
#ifdef DEBUG
		printf("PTL_get_device_bars found a bridge memory region\n" );
		printf("PTL_get_device_bars: fMemBase/fMemLimit: 0x%04X - 0x%04X\n", 
		       bridgeCSR->fMemBase_req, bridgeCSR->fMemLimit_req );
#endif
 	    bar[barCnt].fBridgeSecBus = (short)bridgeCSR->fSecBusNo_req;
	    bar[barCnt].fBridgePrimBus = (short)bridgeCSR->fPrimBusNo_req;
	    bar[barCnt].fIsPrefetchable = 0;
	    bar[barCnt].fAddress = bridgeCSR->fMemBase_req;
	    bar[barCnt].fAddress <<= 16;
	    bar[barCnt].fSize = bridgeCSR->fMemLimit_req;
	    bar[barCnt].fSize <<= 16;
	    bar[barCnt].fSize |= 0xFFFFF;
	    bar[barCnt].fSize += 1;
	    bar[barCnt].fSize -= bar[barCnt].fAddress;
	    tSize += bar[barCnt].fSize;
	    barCnt++;
	    }
	else
	    {
 	    bar[barCnt].fBridgeSecBus = (short)bridgeCSR->fSecBusNo_req;
	    bar[barCnt].fBridgePrimBus = (short)bridgeCSR->fPrimBusNo_req;
	    bar[barCnt].fIsPrefetchable = 0;
	    bar[barCnt].fIs64 = 0;
	    bar[barCnt].fAddress = bar[barCnt].fSize = 0;
	    barCnt++;
	    }
	}
    else
	{ // Determine memory regions occupied by ordinary (non-bridge) device
	// unpack sizes 
	for(i = 0; i < 6; i++)
	    {
	    bar[barCnt].fBridgeSecBus = (short)-1;
	    bar[barCnt].fBridgePrimBus = (short)-1;
	    if(size[i] & 1)
		{
		/* IO BAR */
		bar[barCnt].fSize = 0;
		bar[barCnt].fAddress = 0;
		bar[barCnt].fIsMemory = 1;
		barCnt++;
		continue;
		}
	    /* BAR is memory */
	    bar[barCnt].fIsMemory = 1;
	    bar[barCnt].fIsPrefetchable = ((size[i] & 8) ? 1 : 0);
	    if((size[i] & 6) == 4) // 64 bit address !!!!
		{
#ifdef DEBUG
		  printf("PTL_get_device_bars found a 64bit memory region\n" );
#endif
			  
		HILFE = size[i+1];
		HILFE <<= 32;
		HILFE |= (size[i] & ~0xF);
		bar[barCnt].fSize = ~HILFE+1;
		bar[barCnt].fAddress = csr->fBaseAddr[i+1];
		bar[barCnt].fAddress <<= 32;
		bar[barCnt].fAddress |= csr->fBaseAddr[i];
		bar[barCnt].fIs64  = 1;
		tSize += bar[barCnt].fSize;
		barCnt++;
		i++;
#ifdef DEBUG
		printf("PTL_get_device_bars found a 64bit addr!! \n");
#endif
		}
	    else
		{
		bar[barCnt].fSize = ~(size[i] & ~0xF) + 1;
		bar[barCnt].fAddress = csr->fBaseAddr[i] & (~0xf) ;
		bar[barCnt].fIs64  = 0;
		tSize += bar[barCnt].fSize;
		barCnt++;
		}
	    }
	}
    PSI_closeRegion( region );
    return PSI_OK;
    }


/*
** Print out the base address register addresses and sizes to stdout.
*/
void PTL_print_bar( PCI_BAR *bar )
    {
    PTL_fprint_bar( stdout, bar );
    }

/*
** Print out the base address register addresses and sizes to the specified file.
*/
void PTL_fprint_bar( FILE* file, PCI_BAR *bar )
    {
    fprintf( file, "0x%016LX - 0x%016LX (%Lu) bytes - Memory%c 64bit%c Prefetchable%c",
	     bar->fAddress, bar->fSize, bar->fSize, IsBitSet(bar->fIsMemory),
	     IsBitSet(bar->fIs64), IsBitSet(bar->fIsPrefetchable) );
    if ( bar->fBridgePrimBus!=-1 || bar->fBridgeSecBus!=-1 )
	fprintf( file, " BridgePrimaryBus=%hd - BridgeSecondaryBus=%hd",
		 bar->fBridgePrimBus, bar->fBridgeSecBus );
    fprintf( file, "\n" );
    }


/*
** Helper comparison function for qsort function. */
int pci_compare_region( const void * p1, const void *p2 )
    {
    PCI_BAR* r1 = (PCI_BAR*)p1;
    PCI_BAR* r2 = (PCI_BAR*)p2;
    if ( r1->fAddress < r2->fAddress ) 
	return -1;
    if ( r1->fAddress > r2->fAddress ) 
	return 1;
    return 0;
    }



/*
** Assign an address to a base address register of the given device. The size of the
** window needed will have to be determined before and stored in the corresponding
** bar structure's size field.
** The function will determine if the bar is a memory bar and then call PTL_assign_bar_mem.
** IO bars are currently not yet implemented.
*/
PSI_Status PTL_assign_bar( u16 bus, u16 dev, u16 func, PCI_BAR bar[6], short bar_nr, int use64 )
    {
    if ( bar_nr >=6 )
	return EINVAL;
    if ( bar[bar_nr].fIsMemory ){
	return PTL_assign_bar_mem( bus, dev, func, bar, bar_nr, use64 );
    }
    else
	return ENOSYS; // XXX IO region allocating has to be implemented.
    }
/*
** Assign an address to a memory base address register of the given device. The size of the
** window needed will have to be determined before and stored in the corresponding
** bar structure's size field.
*/
PSI_Status PTL_assign_bar_mem( u16 bus, u16 dev, u16 func, PCI_BAR bar[6], short bar_nr, int use64 )
    {
#define BAR_COUNT 6
#define REGION_COUNT (4096*BAR_COUNT + 3)
    // not more than 4096 PCI devices by spec...
    PCI_BAR regions[BAR_COUNT]; 
    PCI_BAR sort_regions[REGION_COUNT];
    int deviceCount = 0;
    PCI_BAR free_regions[REGION_COUNT];
    int freg = 0;
    int currentFit = -1;
    int i,j , found;
    int bridgeFound;
    u16 b, d, f;
    u32 c = 0;
    unsigned char prefetch;
    short superBusses[16];
    u32 superBusCnt=0;
    unsigned long long addr,size, start, stop;
    unsigned long memSize;
    unsigned long long lowBridge, highBridge, lowFree, highFree;
    PSI_Status psiStatus;



    /* Paranoia */
    if ( bar_nr>=BAR_COUNT || !bar[bar_nr].fIsMemory )
	return PSI_SYS_ERRNO+EINVAL;

    /* first mark the phyiscal memory as used.*/
    psiStatus = PTL_get_memory_size( &memSize );
    if ( !memSize || psiStatus!=PSI_OK )
	{
	return PSI_SYS_ERRNO+ENXIO;
	}
    memset( &regions, 0, sizeof(regions[0])*BAR_COUNT );
    memset( &sort_regions, 0, sizeof(sort_regions[0])*REGION_COUNT );
    memset( &free_regions, 0, sizeof(free_regions[0])*REGION_COUNT );
    sort_regions[0].fAddress = 0;
    sort_regions[0].fSize = memSize;
    sort_regions[0].fIs64 = 1;
    sort_regions[0].fBridgeSecBus = (unsigned char)-1;
    sort_regions[0].fBridgePrimBus = (unsigned char)-1;
    sort_regions[0].fIsPrefetchable = 1;
    deviceCount++;
    prefetch = bar[bar_nr].fIsPrefetchable;


    if ( !bar[bar_nr].fSize )
	{
#ifdef DEBUG
	printf( "PTL_assign_bar_mem: No size needed for region %d\n", bar_nr );
#endif
	return PSI_OK;
	}

    /* loop over all devices and find their used address regions.*/
    while ( PTL_find_pci_device( 0xFFFF, 0xFFFF, c, &b, &d, &f )!=PSI_OK )
	{
#ifdef DEBUG
	printf( "PTL_assign_bar_mem: Accessing %d/%d/%d\n", b, d, f );
#endif
	
	PTL_get_device_bars( b, d, f, regions );
	for ( i = 0;  i < 6; i++ )
	    {
	    /* Insert all found used address regions in a list. */
	    /* printf("%1d 0x%016Lx  0x%016Lx \n",i,regions[i].addr,regions[i].size); */
	    if( (bus!=b && dev!=d && func!=f) && regions[i].fIsMemory && (regions[i].fSize || regions[i].fBridgePrimBus!=-1) ) 
		{
#ifdef DEBUG
		printf( "PTL_assign_bar_mem: Inserting region (0x%08LX / 0x%08LX / %s / %s)\n", regions[i].fAddress, regions[i].fSize, 
			regions[i].fIs64 ? "64" : "32", regions[i].fIsPrefetchable ? " P" : "np" );
#endif
		sort_regions[deviceCount++] = regions[i];
		}
	    if ( (bus==b && dev==d && func==f) && bar[i].fAddress && bar[i].fSize )
		{
#ifdef DEBUG
		printf( "PTL_assign_bar_mem: Inserting region (0x%08LX / 0x%08LX / %s / %s)\n", bar[i].fAddress, bar[i].fSize, 
			bar[i].fIs64 ? "64" : "32", bar[i].fIsPrefetchable ? " P" : "np" );
#endif
		sort_regions[deviceCount++] = bar[i];
		}
	    }
	c++;
	}


    if ( bus )
	{
	/* handle the case, that the board is not on the primary bus... */
	found = 0;
	bridgeFound = 0;
#ifdef DEBUG
	printf( "PTL_assign_bar_mem: prefetch needed: %d\n", prefetch );
#endif

	/* First find the order of busses that we need to traverse */
	superBusses[superBusCnt++] = bus;
	while ( !found )
	    {
	    for ( i = 0; i < deviceCount; i++ )
		{
		if ( sort_regions[i].fBridgeSecBus==superBusses[superBusCnt-1] )
		    {
#ifdef DEBUG
		    printf( "PTL_assign_bar_mem: Bridge found: %d -> %d - Prefetch: %d\n", sort_regions[i].fBridgePrimBus,
			    sort_regions[i].fBridgeSecBus, sort_regions[i].fIsPrefetchable );
#endif
		    }
		bridgeFound = 1;
		if ( sort_regions[i].fBridgeSecBus==superBusses[superBusCnt-1] &&
		     sort_regions[i].fIsPrefetchable==prefetch )
		    {
		    superBusses[superBusCnt++] = sort_regions[i].fBridgePrimBus;
		    if ( superBusses[superBusCnt-1]==0 )
			found = 1;
		    break;
		    }
		}
	    if ( superBusCnt==1 )
		found = 1;
	    }
		    
#ifdef DEBUG
	printf( "PTL_assign_bar_mem: Bus traversal: %d", superBusses[superBusCnt-1] );
	for ( i = superBusCnt-1; i >0; i-- )
	    {
	    printf( " -> " );
	    printf( "%d", superBusses[i-1] );
	    }
	printf( "\n" );
#endif

	/* Now determine the bounds of the bridge address regions */
	lowBridge = 0x0;
	highBridge = 0xFFFFFFFFFFFFFFFFULL;
	for ( i = 0; i < deviceCount; i++ )
	    {
	    if ( sort_regions[i].fBridgeSecBus!=-1 )
		{
		for ( j = 0; (u32)j < superBusCnt; j++ )
		    {
		    if ( superBusses[j]==sort_regions[i].fBridgeSecBus && 
			 sort_regions[i].fIsPrefetchable==prefetch )
			{
			/* we have found another bridge to traverse, let's see, 
			** if this has more strict bounds than we had previously. */
#ifdef DEBUG
			printf( "PTL_assign_bar_mem: Removing region (0x%08LX / 0x%08LX / %s / %s)\n", 
				sort_regions[i].fAddress, sort_regions[i].fSize, 
				sort_regions[i].fIs64 ? "64" : "32", sort_regions[i].fIsPrefetchable ? " P" : "np" );
#endif
			if ( sort_regions[i].fAddress > lowBridge )
			    lowBridge = sort_regions[i].fAddress;
			if ( sort_regions[i].fAddress+sort_regions[i].fSize < highBridge )
			    highBridge = sort_regions[i].fAddress+sort_regions[i].fSize;
			sort_regions[i].fAddress = sort_regions[i].fSize = 0;
			break;
			}
		    }
		}
	    }
	
#ifdef DEBUG
	printf( "PTL_assign_bar_mem: lowBridge: 0x%08LX - highBridge: 0x%08LX\n", lowBridge, highBridge );
#endif
	
	lowFree = 0;
	highFree = 0xFFFFFFFFFFFFFFFFULL;
	/* Now find the lowest/highest bounds of the pci cards next to the bridge.
	** This is because, if there is some free space next to the bridged region,
	** we could use that space as well and extend the bridge to that region. */
	if ( lowBridge>0 && highBridge<0xFFFFFFFFFFFFFFFFULL )
	    {
	    for ( i = 0; i < deviceCount; i++ )
		{
		found = 0;
		for ( j = 0; (u32)j < superBusCnt; j++ )
		    {
		    if ( sort_regions[i].fBridgeSecBus==superBusses[j] 
			 && sort_regions[i].fIsPrefetchable==prefetch )
			{
			found = 1;
			break;
			}
		    }
		if ( !found )
		    {
		    if ( sort_regions[i].fAddress+sort_regions[i].fSize > lowFree &&
			 sort_regions[i].fAddress+sort_regions[i].fSize <= lowBridge )
			{
#ifdef DEBUG
			printf( "PTL_assign_bar_mem: New lowFree: region %d, setting from 0x%016LX to 0x%016LX\n",
				i, lowFree, sort_regions[i].fAddress+sort_regions[i].fSize );
#endif
			lowFree = sort_regions[i].fAddress+sort_regions[i].fSize;
			}
		    if ( sort_regions[i].fAddress < highFree && sort_regions[i].fAddress >= highBridge )
			{
#ifdef DEBUG
			printf( "PTL_assign_bar_mem: New highFree: region %d, setting from 0x%016LX to 0x%016LX\n",
				i, highFree, sort_regions[i].fAddress );
#endif
			highFree = sort_regions[i].fAddress;
			}
		    }
		}
	    if ( (lowFree & 0xFFFFFFFFFFF00000ULL) == lowFree )
		lowFree &= 0xFFFFFFFFFFF00000ULL;
	    else
		{
		lowFree &= 0xFFFFFFFFFFF00000ULL;
		lowFree += 0x0000000000100000ULL;
		}
	    if ( highFree != 0xFFFFFFFFFFFFFFFFULL )
		{
		highFree &= 0xFFFFFFFFFFF00000ULL;
		}
	    }
	else
	    {
	    lowFree = 0;
	    highFree = 0xFFFFFFFFFFFFFFFFULL;
	    }
#ifdef DEBUG
	printf( "PTL_assign_bar_mem: lowFree: 0x%08LX - highFree: 0x%08LX\n", lowFree, highFree );
#endif
	/* Mark the area outside the bounds marked by lowFree, highFree as totally used. */
	sort_regions[deviceCount].fAddress = 0;
	sort_regions[deviceCount++].fSize = lowFree;
	sort_regions[deviceCount].fAddress = highFree;
	sort_regions[deviceCount++].fSize = 0xFFFFFFFFFFFFFFFFULL-highFree;
	}
    
    if ( !use64 )
	{
	/* Mark the high areas (above the 32 bit address space) as used. */
	sort_regions[deviceCount].fAddress = 0x100000000ULL;
	sort_regions[deviceCount].fSize = 0xFFFFFFFEFFFFFFFFULL;
	deviceCount++;
	}
    
    /* done with scanning the bus,now find free region starting from top. */

    /* First. sort regions by address. */
    qsort( sort_regions, deviceCount, sizeof(PCI_BAR), pci_compare_region);
    
#ifdef DEBUG
    printf("PTL_assign_bar_mem: find_free_region used regions: deviceCount %d \n", deviceCount );
#endif
    
    /* Merge overlapping regions. */
    for ( i = 0; i < deviceCount; i++ )
	{
	start = sort_regions[i].fAddress;
	stop = sort_regions[i].fAddress + sort_regions[i].fSize;
	found = 0;
	for ( j = i+1; j < deviceCount; j++ )
	    {
	    if ( sort_regions[j].fAddress < stop )
		{
		found++;
		if ( stop < sort_regions[j].fAddress+sort_regions[j].fSize )
		    stop = sort_regions[j].fAddress+sort_regions[j].fSize;
		}
	    else
		break ; /* XXX */
	    }
	sort_regions[i].fSize = stop-start;

	for ( j = i+1; j < i + found + 1; j++ )
	    {
	    sort_regions[j].fAddress = stop;
	    sort_regions[j].fSize = 0;
	    }
	i += found;
	}
    
    for(i = 0; i < (deviceCount-1); i++)
	{
#ifdef DEBUG
	printf("PTL_assign_bar_mem: %1d 0x%016Lx  0x%016Lx  (0x%016Lx) %s %s\n",i,sort_regions[i].fAddress,sort_regions[i].fAddress+sort_regions[i].fSize,
	       sort_regions[i].fSize,
	       sort_regions[i].fIs64 ? "64" : "32", sort_regions[i].fIsPrefetchable ? " P" : "np" );
#endif

	addr = sort_regions[i].fAddress + sort_regions[i].fSize;
      
	size = sort_regions[i+1].fAddress - addr;
	if( size )// some free space left 
	    {
	    free_regions[freg].fAddress = addr;
	    free_regions[freg++].fSize = size;
	    }
	}

#ifdef DEBUG
    printf("PTL_assign_bar_mem: %1d 0x%016Lx  0x%016Lx %s %s\n",i,sort_regions[i].fAddress, sort_regions[i].fSize,
	   sort_regions[i].fIs64 ? "64" : "32", sort_regions[i].fIsPrefetchable ? " P" : "np" );
#endif
    
	
    if ( free_regions[freg-1].fAddress+free_regions[freg-1].fSize < 0xFFFFFFFFFFFFFFFFULL )
	{
	free_regions[freg].fAddress = sort_regions[i].fAddress + sort_regions[i].fSize;
	free_regions[freg].fSize = 0xFFFFFFFF;
	free_regions[freg].fSize <<= 32;
	free_regions[freg].fSize |= 0xFFFFFFFF;
	free_regions[freg].fSize -= free_regions[freg].fAddress;
	if ( free_regions[freg].fSize )
	    freg++;
	}
    
#ifdef DEBUG
    printf("PTL_assign_bar_mem: free regions ! \n");
    for(i = 0; i < freg; i++) 
	printf("%1d 0x%016Lx  0x%016Lx  (0x%016Lx)\n",i,free_regions[i].fAddress,
	       free_regions[i].fAddress+free_regions[i].fSize,free_regions[i].fSize);
#endif

    /* now we can fit our regions in
    ** We try to find the smallest free region that we fit in. */
    currentFit = -1;
    bar[bar_nr].fAddress = 0;
    if ( bar[bar_nr].fSize )
	{
	currentFit = -1;
	for(j = 0; j < freg; j++)
	    {
	    if ( free_regions[j].fAddress % bar[bar_nr].fSize )
		addr = ((free_regions[j].fAddress / bar[bar_nr].fSize)+1)*bar[bar_nr].fSize;
	    else
		addr = free_regions[j].fAddress;
	    if ( addr >= free_regions[j].fAddress+free_regions[j].fSize )
		{
		size = 0;
		}
	    else
		size = free_regions[j].fSize - (addr-free_regions[j].fAddress);
	    if ( size >= bar[bar_nr].fSize )
		{
		if(currentFit < 0) 
		    { 
		    currentFit = j; 
		    continue; 
		    }
		if ( free_regions[j].fSize < free_regions[currentFit].fSize ) 
		    currentFit = j;
		}
	    }
	if(currentFit < 0)
	    {
#ifdef DEBUG
	    printf("PTL_assign_bar_mem: Can't find space in pci address space for base address %d requested size is 0x%016LX \n", bar_nr, bar[bar_nr].fSize);
#endif
	    return PSI_SYS_ERRNO+ENOSPC;
	    }
	if ( free_regions[currentFit].fAddress % bar[bar_nr].fSize )
	    bar[bar_nr].fAddress = ((free_regions[currentFit].fAddress / bar[bar_nr].fSize)+1)*bar[bar_nr].fSize;
	else 
	    bar[bar_nr].fAddress = free_regions[currentFit].fAddress;
		
	free_regions[currentFit].fSize -= (bar[bar_nr].fAddress + bar[bar_nr].fSize  -  free_regions[currentFit].fAddress);
	free_regions[currentFit].fAddress = bar[bar_nr].fAddress + bar[bar_nr].fSize;
	}
	
#ifdef DEBUG
    printf( "PTL_assign_bar_mem: Found region %d (Used %d): 0x%016LX - 0x%016LX (0x%016LX)\n", bar_nr, currentFit, bar[bar_nr].fAddress, 
	    bar[bar_nr].fAddress+bar[bar_nr].fSize, bar[bar_nr].fSize );
#endif

    return PSI_OK;
    }
    


/*
** Configure the base address registers in the specified devive.
** This function will try to autodetermine wether 64 bit addresses are
** supported and then call PTL_configure_bars_sz with the appropriate
** use64 parameter.
** bar_nr specifies the base address register to be configured (0-5) or
** -1 to configure all bars.
*/
PSI_Status PTL_configure_bars( u16 bus, u16 dev, u16 func, short bar_nr )
    {
#if !defined(USE_64BIT_ADDRESS) && !defined(NO_64BIT_ADDRESS)
    if ( sizeof(void*)>=8 )
	return PTL_configure_bars_sz( bus, dev, func, bar_nr, 1 );
    return PTL_configure_bars_sz( bus, dev, func, bar_nr, 0 );
#else
#ifdef NO_64BIT_ADDRESS
    return PTL_configure_bars_sz( bus, dev, func, bar_nr, 0 );
#else
    return PTL_configure_bars_sz( bus, dev, func, bar_nr, 1 );
#endif
#endif
    }

/*
** Configure the base address registers in the specified devive.
** This function just calls PTL_configure_bars_sz with the use64
** flag set to false.
** bar_nr specifies the base address register to be configured (0-5) or
** -1 to configure all bars.
*/
PSI_Status PTL_configure_bars_32( u16 bus, u16 dev, u16 func, short bar_nr )
    {
    return PTL_configure_bars_sz( bus, dev, func, bar_nr, 0 );
    }

/*
** Configure the base address registers in the specified devive.
** This function just calls PTL_configure_bars_sz with the use64
** flag set to true.
** bar_nr specifies the base address register to be configured (0-5) or
** -1 to configure all bars.
*/
PSI_Status PTL_configure_bars_64( u16 bus, u16 dev, u16 func, short bar_nr )
    {
    return PTL_configure_bars_sz( bus, dev, func, bar_nr, 1 );
    }



/*
** Configure the base address registers in the specified devive.
** The use64 flag specifies wether 64 bit addresses can be used.
** bar_nr specifies the base address register to be configured (0-5) or
** -1 to configure all bars.
*/
PSI_Status PTL_configure_bars_sz( u16 bus, u16 dev, u16 func, short bar_nr, int use64 )
    {
    PCI_BAR regions[BAR_COUNT];
    unsigned int wDataLow,wDataHigh,wData;
    unsigned char prefetch;
    int i , n;
    u16 cmd_reg;
    tRegion region;
    PSI_Status psiStatus;
    char devName[512];
    short bar_start, bar_end;

#ifdef DEBUG
    printf("PTL_configure_bars_sz for devID %hu/%hu/%hu/%hi (64 bit addresses %s)\n", bus, dev, func, bar_nr, (!use64 ? "no" : "yes"));
#endif

    memset( &regions, 0, sizeof(regions[0])*BAR_COUNT );

    if ( bar_nr == -1 )
	{
	bar_start = 0; 
	bar_end = 5;
	}
    else
	{
	if ( bar_nr < 0 || bar_nr >5 )
	    return PSI_SYS_ERRNO+EINVAL;
	bar_start = bar_end = bar_nr;
	}

    sprintf( devName, "/dev/psi/bus/0x%04hX/slot/0x%04hX/function/0x%04hX/conf",
	     bus, dev, func );
    psiStatus = PSI_openRegion( &region, devName );
    if ( psiStatus != PSI_OK )
	return psiStatus;

#ifndef DUMMY_CFG
    PSI_read( region, 4, 2, 2, &cmd_reg );
    cmd_reg &= ~2;
    PSI_write( region, 4, 2, 2, &cmd_reg );
#endif
    
    PTL_get_device_bars( bus, dev, func, regions );
#ifdef DEBUG
    for ( i = bar_start; i <= bar_end; i++ )
	{
	printf( "PTL_configure_bars_sz: Current: BaseAddr[%1d] 0X%016LX   Size:  0X%016LX \n",
	       i, regions[i].fAddress, regions[i].fSize );
	}
#endif
    
    for ( i = bar_start; i <= bar_end; i++ )
	regions[i].fAddress = 0;
    for ( i = bar_start; i <= bar_end; i++ )
	{
	prefetch = regions[i].fIsPrefetchable;
	do
	    {
	    psiStatus = PTL_assign_bar( bus, dev, func, regions, i, use64 );
	    if ( psiStatus!=PSI_OK && !prefetch )
		break;
	    if ( psiStatus!=PSI_OK && prefetch )
		{
#ifdef DEBUG
		printf( "PTL_configure_bars_sz: Could not find area on first try. Switching to non-prefetchable...\n" );
#endif
		regions[i].fIsPrefetchable = prefetch = 0;
		}
	    }
	while ( psiStatus != PSI_OK );

	if ( psiStatus != PSI_OK ) 
	    {
	    PSI_closeRegion( region );
	    return psiStatus;
	    }
	}
		
#ifdef DEBUG
    for(i = bar_start; i <= bar_end; i++)
	{
	printf("PTL_configure_bars_sz: New: BaseAddr[%1d] 0X%016LX   Size:  0X%016LX \n",
	       i,regions[i].fAddress,regions[i].fSize);
	}
#endif
    
    for( n = i =bar_start;  i <= bar_end; i++)
	{
	if ( regions[i].fSize <= 0 )
	    continue;
	if(regions[i].fIs64)
	    {
#ifdef DEBUG
	    printf("PTL_configure_bars_sz: Handling a 64bit address\n");
#endif
	    wDataLow  = (unsigned int)regions[i].fAddress;
	    wDataHigh = (unsigned int)( regions[i].fAddress >> 32);
#ifndef DUMMY_CFG
	    PSI_write( region, 16+n*4, 4, 4, &wDataLow );
	    PSI_write( region, 16+(n+1)*4, 4, 4, &wDataHigh );
#endif
	    n += 2;
	    }
	else
	    {
	    wData = (unsigned int)regions[i].fAddress;
#ifndef DUMMY_CFG
	    PSI_write( region, 16+n*4, 4, 4, &wData );
#endif
	    n++;
	    }
	if ( regions[i].fAddress > 0xFFFFFFFFl || regions[i].fAddress+regions[i].fSize > 0xFFFFFFFFl )
	    regions[i].fIs64 = 1;
	else
	    regions[i].fIs64 = 0;
	PTL_configure_bridges( bus, dev, func, regions[i] );
	}

    cmd_reg |= 2; // |4|128;

#ifndef DUMMY_CFG
    PSI_write( region, 4, 2, 2, &cmd_reg );
    cmd_reg = 64;
    PSI_write( region, 13, 1, 1, &cmd_reg );
#endif

    PSI_closeRegion( region );
    return PSI_OK;
    }


/*
** Configure all bridges passed on the way to the specified device so that
** they allow data to the specified base address register to pass through.
*/
PSI_Status PTL_configure_bridges( u16 bus, u16 dev, u16 func, PCI_BAR bar )
    {
    unsigned char prefetch;
    u16 b, d, f;
    u32 c = 0;
    PCI_CSR csr;
    PCI_BRIDGE_CSR* bridgeCSR;
    int done = 0;
    unsigned int j;
    unsigned long long brUpper, brLower;
    tRegion region;
    PSI_Status psiStatus;
    char devName[512];

    u32 superBusses[16], superBusCnt=0;
    if ( !bus )
	return PSI_OK;

#ifdef DEBUG
    printf("PTL_configure_bridges for devID %hu/%hu/%hu\n", bus, dev, func);
#endif

    sprintf( devName, "/dev/psi/bus/0x%04hX/slot/0x%04hX/function/0x%04hX/conf",
	     bus, dev, func );
    psiStatus = PSI_openRegion( &region, devName );
    if ( psiStatus != PSI_OK )
	return psiStatus;


    
    superBusses[superBusCnt++] = bus;
    prefetch = bar.fIsPrefetchable;

#ifdef DEBUG
    printf( "PTL_configure_bridges for device %d/%d/%d - 0x%016LX - 0x%016LX  (0x%016LX)  (%d/%s)\n", bus, dev, func,
	    bar.fAddress, bar.fAddress+bar.fSize, bar.fSize,
	    (bar.fIs64 ? 64 : 32), (bar.fIsPrefetchable ? "P" : "np") );
#endif
    
    while ( !done )
	{
	c=0;
	while ( !PTL_find_pci_device( 0xFFFF, 0xFFFF, c, &b, &d, &f ) )
	    {
#ifdef DEBUG
	    printf( "PTL_configure_bridges accessing %d/%d/%d\n", b, d, f );
#endif
	    if ( b!=bus && d!=dev && f!=func )
		{
		PTL_read_region_pci_csr( region, 0, &csr );
		if ( (csr.fClassCode_req & 0xFFFF00) == 0x060400 )
		    {
		    bridgeCSR = (PCI_BRIDGE_CSR*)&csr;
		    if ( bridgeCSR->fSecBusNo_req==superBusses[superBusCnt-1] )
			{
#ifdef DEBUG
			printf( "PTL_configure_bridges: "
				"Found next bus %d for bus %ld\n", 
				bridgeCSR->fPrimBusNo_req,
				(long) superBusses[superBusCnt-1] );
#endif
			superBusses[superBusCnt++]=bridgeCSR->fPrimBusNo_req;
			if ( superBusses[superBusCnt-1]==0 )
			    done = 1;
			break;
			}
		    }
		}
	    c++;
	    }
	if ( superBusCnt==1 )
	    done = 1;
	}

    if ( superBusCnt==1 )
	{
#ifdef DEBUG
	printf( "PTL_configure_bridges: No bus hierarchy found for bus %d. Assuming direct connection to host bus\n", bus );
#endif
	PSI_closeRegion( region );
	return PSI_OK;
	}
	
#ifdef DEBUG
    printf( "PTL_configure_bridges: Bus hierarchy for bus %d\n", bus );
    for ( c = superBusCnt; c > 0; c-- )
	{
	printf( "%ld", (long) superBusses[c-1] );
	if ( c > 1 )
	    printf( " -> " );
	}
    printf( "\n" );
#endif

    c=0;
    while ( !PTL_find_pci_device( 0xFFFF, 0xFFFF, c, &b, &d, &f ) )
	{
#ifdef DEBUG
	printf( "PTL_configure_bridges: Accessing %d/%d/%d\n", b, d, f );
#endif
	if ( b!=bus && d!=dev && f!=func )
	    {
	    PTL_read_region_pci_csr( region, 0, &csr );
	    if ( (csr.fClassCode_req & 0xFFFF00) == 0x060400 )
		{
		bridgeCSR = (PCI_BRIDGE_CSR*)&csr;
		for ( j = 0; j < superBusCnt; j++ )
		    {
		    if ( bridgeCSR->fSecBusNo_req==superBusses[j] )
			{
#ifdef DEBUG
			printf( "PTL_configure_bridges: Reconfiguring bridge %d/%d/%d from ",
				b, d, f );
#endif
			if ( prefetch )
			    {
			    if ( bar.fIs64 )
				{ // prefetch and 64 bit 
#ifdef DEBUG
				printf( "( P/64) 0x%08X%04X00000/0x%08X%04XFFFFF to ",
					bridgeCSR->fPrefBaseUp, bridgeCSR->fPrefMemBase,
					bridgeCSR->fPrefLimitUp, (bridgeCSR->fPrefMemLimit | 0xF) );
#endif
				if ( (bridgeCSR->fMemBase_req & 0xF) != 0 )
				    {
#ifdef DEBUG
				    printf( "\nPTL_configure_bridges: Could not configure bridge to support 64bit address\n" );
#endif
				    PSI_closeRegion( region );
				    return PSI_SYS_ERRNO+ERANGE; // No 64 bit addresses
				    }
				brLower = bridgeCSR->fPrefBaseUp;
				brLower <<= 32;
				brLower |= (bridgeCSR->fPrefMemBase<<16);
				brUpper = bridgeCSR->fPrefLimitUp;
				brUpper <<= 32;
				brUpper |= ((bridgeCSR->fPrefMemLimit<<16) | 0xFFFFF);
				brUpper += 1;

				if ( bar.fAddress < brLower )
				    {
				    bridgeCSR->fPrefMemBase = (bar.fAddress & 0x00000000FFF00000ULL)>>16;
				    bridgeCSR->fPrefBaseUp = (bar.fAddress & 0xFFFFFFFF00000000ULL) >> 32;
				    }
				// br64.size is really 
				if ( bar.fAddress+bar.fSize > brUpper )
				    {
				    if ( ((bar.fAddress+bar.fSize) & 0xFFFFFFFFFFF00000ULL) == (bar.fAddress+bar.fSize) )
					{
					bridgeCSR->fPrefMemLimit = ((bar.fAddress+bar.fSize-1) & 0x00000000FFF00000ULL)>>16;
					bridgeCSR->fPrefLimitUp = ((bar.fAddress+bar.fSize-1) & 0xFFFFFFFF00000000ULL) >> 32;
					}
				    else
					{
					bridgeCSR->fPrefMemLimit = ((bar.fAddress+bar.fSize) & 0x00000000FFF00000ULL)>>16;
					bridgeCSR->fPrefLimitUp = ((bar.fAddress+bar.fSize) & 0xFFFFFFFF00000000ULL) >> 32;
					}
				    }
#ifdef DEBUG
				printf( "0x%08X%04X00000/0x%08X%04XFFFFF\n",
					bridgeCSR->fPrefBaseUp, bridgeCSR->fPrefMemBase,
					bridgeCSR->fPrefLimitUp, (bridgeCSR->fPrefMemLimit < 0xF) );
#endif

#ifndef DUMMY_CFG
				PSI_write( region, (unsigned long)&(bridgeCSR->fPrefMemBase)-(unsigned long)bridgeCSR, 2, 2, &(bridgeCSR->fPrefMemBase) );
				PSI_write( region, (unsigned long)&(bridgeCSR->fPrefMemLimit)-(unsigned long)bridgeCSR, 2, 2, &(bridgeCSR->fPrefMemLimit) );
				PSI_write( region, (unsigned long)&(bridgeCSR->fPrefBaseUp)-(unsigned long)bridgeCSR, 4, 4, &(bridgeCSR->fPrefBaseUp) );
				PSI_write( region, (unsigned long)&(bridgeCSR->fPrefLimitUp)-(unsigned long)bridgeCSR, 4, 4, &(bridgeCSR->fPrefLimitUp) );
#endif
				}
			    else
				{ // prefetch and 64 bit
#ifdef DEBUG
				printf( "( P/32) 0x%04X00000/0x%04XFFFFF to ",
					bridgeCSR->fPrefMemBase, (bridgeCSR->fPrefMemLimit | 0xF) );
#endif
				brLower = bridgeCSR->fPrefMemBase << 16;
				brUpper = ((bridgeCSR->fPrefMemLimit << 16) | 0xFFFFF)+1;
				if ( bar.fAddress < brLower )
				    bridgeCSR->fPrefMemBase = (bar.fAddress & 0x00000000FFF00000ULL)>>16;
				if ( bar.fAddress+bar.fSize > brUpper )
				    {
				    if ( ((bar.fAddress+bar.fSize) & 0xFFF00000ULL) == (bar.fAddress+bar.fSize) )
					bridgeCSR->fPrefMemLimit = ((bar.fAddress+bar.fSize-1) & 0x00000000FFF00000ULL)>>16;
				    else
					bridgeCSR->fPrefMemLimit = ((bar.fAddress+bar.fSize) & 0x00000000FFF00000ULL)>>16;
				    }
#ifdef DEBUG
				printf( "0x%04X00000/0x%04XFFFFF\n",
					bridgeCSR->fPrefMemBase, (bridgeCSR->fPrefMemLimit | 0xF) );
#endif
#ifndef DUMMY_CFG
				PSI_write( region, (unsigned long)&(bridgeCSR->fPrefMemBase)-(unsigned long)bridgeCSR, 2, 2, &(bridgeCSR->fPrefMemBase) );
				PSI_write( region, (unsigned long)&(bridgeCSR->fPrefMemLimit)-(unsigned long)bridgeCSR, 2, 2, &(bridgeCSR->fPrefMemLimit) );
#endif
				}
			    }
			else
			    { // nonprefetch and 64 bit
#ifdef DEBUG
			    printf( "(np/32) 0x%04X00000/0x%04XFFFFF to ",
				    bridgeCSR->fMemBase_req, (bridgeCSR->fMemLimit_req | 0xF) );
#endif
			    brLower = bridgeCSR->fMemBase_req << 16;
			    brUpper = ((bridgeCSR->fMemLimit_req << 16) | 0xFFFFF) + 1;
			    if ( bar.fAddress < brLower )
				bridgeCSR->fMemBase_req = (bar.fAddress & 0x00000000FFF00000ULL)>>16;
			    if ( bar.fAddress+bar.fSize > brUpper )
				{
				if ( ((bar.fAddress+bar.fSize) & 0xFFF00000ULL) == (bar.fAddress+bar.fSize) )
				    bridgeCSR->fMemLimit_req = ((bar.fAddress+bar.fSize-1) & 0x00000000FFF00000ULL)>>16;
				else
				    bridgeCSR->fMemLimit_req = ((bar.fAddress+bar.fSize) & 0x00000000FFF00000ULL)>>16;
				}
#ifdef DEBUG
			    printf( "0x%04X00000/0x%04XFFFFF\n",
				    bridgeCSR->fMemBase_req, (bridgeCSR->fMemLimit_req | 0xF) );
#endif
#ifndef DUMMY_CFG
			    PSI_write( region, (unsigned long)&(bridgeCSR->fMemBase_req)-(unsigned long)bridgeCSR, 2, 2, &(bridgeCSR->fMemBase_req) );
			    PSI_write( region, (unsigned long)&(bridgeCSR->fMemLimit_req)-(unsigned long)bridgeCSR, 2, 2, &(bridgeCSR->fMemLimit_req) );
#endif
			    }
			break;
			}
		    }
		}
	    //break;
	    }
	c++;
	}

    PSI_closeRegion( region );
    return PSI_OK;
    }



/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: pci_csr.c 862 2006-02-17 18:45:24Z timm $ 
**
***************************************************************************
*/
