# subdirectories
SUBDIRS			:= $(shell ls */Makefile | sed -r 's/\/.*//')



# rules

subdirs:
	@for DIR in ${SUBDIRS}; do \
	   cd $$DIR; \
	   make ${MAKECMDGOALS}; \
	   cd ..; \
	 done

