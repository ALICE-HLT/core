# ids
KERNEL_ID	:= $(shell uname -s)
MACHINE_ID	:= $(shell uname -m)
ifdef PRODUCTIONDEBUG
BUILD_ID        := releasedebug
else
ifdef PRODUCTION
BUILD_ID        := release
else
BUILD_ID        := debug
endif
endif
ARCH_ID         := $(KERNEL_ID)-$(MACHINE_ID)-$(BUILD_ID)
KERNEL_RELEASE	:= $(shell uname -r)
KERNEL_VERSION	:= $(shell uname -r | sed -r 's/([0-9]+\.[0-9]+).*/\1/')
export KERNEL_ID MACHINE_ID ARCH_ID KERNEL_RELEASE KERNEL_VERSION

# installation directories
ifdef PREFIX
INSTALL_PREFIX	:= $(PREFIX)
endif
ifndef INSTALL_PREFIX
INSTALL_PREFIX	:= /usr/local
endif
INSTALL_BIN	:= $(INSTALL_PREFIX)/bin
INSTALL_LIB	:= $(INSTALL_PREFIX)/lib
INSTALL_INCLUDE	:= $(INSTALL_PREFIX)/include
export INSTALL_PREFIX INSTALL_BIN INSTALL_LIB INSTALL_INCLUDE

# directories
INCLUDE_DIR	:= $(TOP_DIR)/include
LIB_ROOT	:= $(TOP_DIR)/lib
LIB_DIR		:= $(LIB_ROOT)/$(ARCH_ID)
BIN_ROOT	:= $(TOP_DIR)/bin
BIN_DIR		:= $(BIN_ROOT)/$(ARCH_ID)
MODULE_ROOT	:= $(TOP_DIR)/modules
MODULE_DIR	:= $(MODULE_ROOT)/$(ARCH_ID)/$(KERNEL_RELEASE)
KERNEL_DIR	:= /lib/modules/$(KERNEL_RELEASE)/build
export TOP_DIR INCLUDE_DIR 
export LIB_ROOT LIB_DIR BIN_ROOT BIN_DIR MODULE_ROOT MODULE_DIR KERNEL_DIR

# compiler flags
BASE_FLAGS      := -Wall -I$(INCLUDE_DIR) -I$(KERNEL_DIR)/arch/x86/include
BUILD_FLAGS     := $(BASE_FLAGS) -Wextra -fPIC
LINK_FLAGS      := -L$(LIB_DIR)
export BASE_FLAGS BUILD_FLAGS LINK_FLAGS

# compiler
ifeq ($(strip $(shell which $(CC))),)
CC		:= gcc
endif
