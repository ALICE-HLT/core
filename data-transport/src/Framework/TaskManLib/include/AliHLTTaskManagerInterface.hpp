#ifndef _ALIHLTTASKMANAGERINTERFACE_HPP_
#define _ALIHLTTASKMANAGERINTERFACE_HPP_

/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCController.hpp"
#include "MLUCConditionSem.hpp"
#include "AliHLTSCLAMHandler.hpp"
#include "BCLAbstrAddress.hpp"
#include "MLUCString.hpp"
#include "MLUCCmdlineParser.hpp"

class AliHLTTaskManagerInterface
    {
    public:

	AliHLTTaskManagerInterface();
	
	AliHLTSCController fController;
	MLUCString fAddressStr;
	BCLAbstrAddressStruct* fAddress;

	MLUCConditionSem<char*> fSignal;

	class LAMHandler: public AliHLTSCLAMHandler
	    {
	    public:
		LAMHandler( AliHLTTaskManagerInterface* interface ):
		    fInterface( interface )
			{
			}
		virtual ~LAMHandler()
			{
			}
		virtual void LookAtMe( BCLAbstrAddressStruct* me );
	    protected:
		AliHLTTaskManagerInterface* fInterface;
	    };
	LAMHandler fLAM;

	MLUCCmdlineParser fCmdParser;

	bool fQuitInterruptWait;
	bool fInterruptWaitRunning;

    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif //  _ALIHLTTASKMANAGERINTERFACE_HPP_
