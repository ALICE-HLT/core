
$Id$

$Log: History.CVS,v $
Revision 1.8  2005/11/28 09:19:46  timm
Added the AliEventID.cpp file containing the event ID type strings.

Added more component status data structure filling.

Fixed an inconsistency in the usage of the status structure next structure
offset field.

Changed the status field delimiting in the TaskManLib from ; and : to
;; and :: .

Added event time handling in the status structure for
AliHLTMultFileBenchPublisher.

Fixed the pending event count field in the status structure for the
AliHLTProcessingSubscriber.

Revision 1.7  2005/11/16 08:53:40  timm
Small fix related to device simulation in the RORC1 handler class.

Revision 1.6  2005/11/16 08:39:55  timm

Modified AliEventID_t to be a structure holding a type (data,
start-/end-of-run) as well as the identifier.

Added total/free buffer size fields to buffer manager classes.

Adapted to latest changes in MLUCConditionSem, MLUCVector, and
MLUCIndexedVector classes. Changes some AliHLTTimerConditionSem instances
to MLUCConditionSem.

In the AliHLTEventMergerNew changed the vectors of present and expected parts
to bitmaps. Additions are now just array lookup and bit set operations.

Added some status data printing and filling for the application component
classes. This includes handling of that status data in the TaskManInterfaceLib.

Revision 1.5  2004/12/21 10:23:34  timm


Added the relay subscriber class and application to the worker components.

Disabled compilation of some old modules, applications, and source files to speed
up the compilation process.

Fixed a bug in AliHLTTimer which caused the destructor to hang when the timer
has not been run before it is destroyed.

Updated the BCLCheckURL calls to reflect the new parameters.

Disabled the SC controller functionality of connectivity to only one controlled
component. Now connections can be simultaneously established to any controlled
component. (This was necessary for proper TaskManager support)

Modified the TaskManager interface library to conform to the new interface
definitions, allowing for connections to controlled components.

Revision 1.4  2004/11/11 14:19:26  timm


Added the AliHLTAlignment file with several functions to the Base Module.
These functions allow to dynamically determine the alignment of different
datatypes in structures.

Added the ASCIIDataWriter and the DDLHeaderPublisher components.

Format corrections for the output of the EventStorageExtractor

Changed the parameters of some AliHLTDescriptorHandler methods to reflect
the changed inner workings.
Accordingy the calls of the functions concerned had to be modified as well.

Added the TRD data origin identifier.

Added a number of DDL header modification/query functions to
AliHLTDDLHeaderData.h.

Modified the AliHLTEventGathererNew, AliHLTEventMergerNew, and
AliHLTEventScattererNew, so that they are faster. (Mainly
reorganizing data structures so that searches can be reduced.)
Also, by handing around pointers to structures, instead of
event IDs with which the structures have to be searched.

Fixes for the AliHLTPublisherBridgeHead and AliHLTSubscriberBridgeHead
classes.

Small change set to the RORCPublisher component, and AliHLTRORCPublisher
and AliHLTRORC1Handler classes. Mainly for stuff concerned with the
TRD test beam.

Removed again a temporary parameter from an AliHLTPipeCom::Read
method, together with its calls.

Fixed (now really, hopefully) the fatal bug in the AliHLTSamplePublisher
when searching for free event slots.

AliHLTMultFilePublisher/MultFileBenchPublisher/ControlledFilePublisher:
  Modified parameters and support in the class, so that the old file format
  now will produce warnings and a new file format with corresponding parameters
  is supported. For new files the whole file content will just be read in and
  published, no special conventions are needed.

Modified the compile/links switches:
  Added (always) the -W warning switch.
  Added (optional, PARANOIDWARNINGS define) the
    -Wfloat-equal -Wshadow -Wunreachable-code
  warning switches.

Most other changes are for two reasons:
Compilation without warning with options -Wall and -W and different
gcc versions (tested: 2.95.3, 3.2.2, 3.2.3, 3.3.4, 3.4.0, 3.4.2)
Compilation without most warnings with additional warning options
  (see conf/Makefile.conf under PARANOIDWARNINGS)

Revision 1.3  2004/03/31 11:01:44  timm

Added the AliHLTPersistentState class which allows a process to save some
state between programm invocations into (currently only) a shared memory
area.

Changed the max. file sizes for AliHTLSimpleFileEventStorage from 2GB to
1GB.


Modifications to the controlled components (worker, dataflow, bridges):
 - StartProcessing requests for subscriptions now request events already
   present in the publisher component by default. (command line option to
   turn this off)
 - The Configure command upon receipt is now checked for new/additional
   configuration entries which are evaluated before configuration is actually
   performed.
 - Event pinning (no release even when no subscribers present) is now
   settable/unsettable via outside commands.

AliHLTControlledComponent: (controlled worker component)
 - More methods (e.g. CreateParts, SetupComponents) now return success/failure
   which is evaluated in caling code as well.
 - Support for persistent state (see above)

AliHLTRORC1Handler/AliHLTRORCHandlerInterface have been modified.
 - Bugfixes in code.
 - Interface has been cleaned up to allow object init from state of RORC

RORC programs (RORCInit, RORCDMAStart, RORCDMAStop, RORCEventReader,
RORCPublisher) have been adapted to the new RORC handler interface.

Additional bugfixes in RORCPublisher

Some new process control commands (see above)

Disabling of output code in IntegrTest-5 analysis code.

AliHLTDetectorPublisher/ControlledFilePublisher/RORCPublisher support for
persistent state abilities (see above).

Revision 1.2  2003/12/19 13:09:00  timm
Updated makefile version identification to version 0.9.10.

Revision 1.1  2003/09/16 13:25:58  timm

AliHLTTimer.cpp: Moved the timer ID determination in AddTimer into the mutex
protected code part.

Replaced the STL string class by the MLUCString class everywhere.

Added new controlled publisher and subscriber bridge head components.
( (Publisher|Subscriber)BridgeHeadNew )

Completion and fixes of AliHLTControlledDataFlowComponent and its derived
classes.

Small modifications and fixes to the new gatherer/merger/scatterer classes.

Small modifications to the AliHLTPublisherBridgeHead and
AliHLTSubscriberBrigeHead classes to make them work in the new controlled
bridge components.

Modified the IntegrTest-4 components to function as controlled components
using the new AliHLTControlledComponent class.

Added a number of valgrind pacifiers in several places. These will likely be
removed or disabled again later.

Small changes to the process control commands and state flags.

Adapted the AliHLTComponentInterface class to also work for bridge head
components.

Added the TaskManLib, a dynamic library that functions as the interface
of the KIP TaskManager to the framework components.

Enhanced the ControlledFilePublisher.

Small enhancements and fixes for the AliHLTControlledComponent.

Small fixes to the AliHLTProcessingSubscriber event retry handling.

Revision 1.1.1.1  2002/05/06 13:13:34  timm

Initial import of restructured sources from Alice L3 Cluster Prototype 4
(AL3C-P4) into this new project.





 You can delete anything after this line
-----------------------------------------
Tue Jun 16 16:52:26 CEST 2009
../../src/AliHLTTaskManagerInterface.cpp

