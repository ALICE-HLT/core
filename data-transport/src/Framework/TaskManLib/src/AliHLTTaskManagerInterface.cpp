/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "InterfaceLib.h"
#include "AliHLTTaskManagerInterface.hpp"
#include "AliHLTSCComponentInterface.hpp"
#include "AliHLTSCCommand.hpp"
#include "BCLURLAddressDecoder.hpp"
#include "AliHLTStatus.hpp"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>

/* Startup, initialize any static structures needed by the library. */
int Initialize( void** private_lib_data, const char* interrupt_listen_address )
    {
    int ret;
    AliHLTTaskManagerInterface* tmi;
    tmi = new AliHLTTaskManagerInterface;
    if ( !tmi )
	return ENOMEM;
    tmi->fAddressStr = interrupt_listen_address;
    tmi->fAddress = NULL;
    //printf( "TaskManLib::Initialize: interrupt_listen_address: %s\n", interrupt_listen_address );
    tmi->fController.SetReplyTimeout( 1000 ); // Timeout of 0.5s
    ret = tmi->fController.Bind( interrupt_listen_address );
    if ( ret )
	{
	tmi->fController.Unbind();
	*private_lib_data = NULL;
	delete tmi;
	return ret;
	}
    tmi->fController.Start();
    tmi->fController.AddLAMHandler( &tmi->fLAM );
    *private_lib_data = reinterpret_cast<void*>( tmi );
    return 0;
    }

/* End, cleanup of library data */
int Terminate( void* private_lib_data )
    {
    reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fController.Stop();
    reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fController.Unbind();
    delete reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data );
    return 0;
    }

/* Convert a given address string into a more compact binary
   representation for immediate usability.
   This can also be a copy of the given string if this is the most directly
   useful representation.
   The returned binary address is allocated in the function and has to be released by a call to 
   ReleaseResource (below) */
int ConvertAddress( void* private_lib_data, const char* process_address_str, void** process_address_bin )
    {
    if ( !process_address_str || !private_lib_data || !process_address_bin )
	return EFAULT;
    BCLAbstrAddressStruct* addr;
    *process_address_bin = NULL;
    int ret;
    ret = BCLDecodeRemoteURL( process_address_str, addr );
    if ( ret )
	return ret;
    unsigned long len = BCLGetAddressSize( addr );
    if ( !len )
	{
	BCLFreeAddress( addr );
	return EINVAL;
	}
    *process_address_bin = malloc( len );
    if ( !*process_address_bin )
	{
	BCLFreeAddress( addr );
	return ENOMEM;
	}
    memcpy( *process_address_bin, addr, len );
    BCLFreeAddress( addr );
    return 0;
    }

/*
  Compare two given binary address representations. 
  Return 0 if they are equal, -1 if addr1 is less than addr2, 
  +1 if addr1 is greater than addr2. If the magnitude comparisons
  do not apply return unequal 0 for inequality. */
int CompareAddresses( void* private_lib_data, void* addr1, void* addr2 )
    {
    if ( !private_lib_data || !addr1 || !addr2 )
	return EFAULT;
    return BCLCompareAddresses( reinterpret_cast<BCLAbstrAddressStruct*>( addr1 ), reinterpret_cast<BCLAbstrAddressStruct*>( addr2 ) );
    }



/* Query the state of a specific process 
** state is allocated as a string in the function and
** has to be released via ReleaseResource (below) */
int QueryState( void* private_lib_data,
		pid_t, 
		const char*, 
		void* process_address, 
		char** state )
    {
    int ret;
    MLUCString typeName, statusName;
    AliHLTSCStatusHeaderStruct* statusHeader;
    ret = reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fController.GetStatus( reinterpret_cast<BCLAbstrAddressStruct*>( process_address ), statusHeader );
    if ( ret )
	return ret;
    AliHLTSCComponentInterface::GetTypeName( statusHeader, typeName );
    AliHLTSCComponentInterface::GetStatusName( statusHeader, statusName );
    unsigned long len = typeName.Length()+statusName.Length()+2;
    *state = (char*)malloc( len );
    if ( *state )
	{
	strcpy( *state, typeName.c_str() );
	strcat( *state, ":" );
	strcat( *state, statusName.c_str() );
	}
    //printf( "%s:%s\n", typeName.c_str(), statusName.c_str() );
    reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fController.FreeStatus( statusHeader );
    return 0;
    }

/* Obtain additional status data from a
** specific process.
** stat_data is allocated as a string in the 
*+ function and has to be released via 
** ReleaseResource (below)
*/
int QueryStatusData( void* private_lib_data,
		     pid_t,
		     const char*, 
		     void* process_address, 
		     char** stat_data )
    {
    int ret;
    MLUCString typeName, statusName, statusData;
    AliHLTSCStatusHeaderStruct* statusHeader;
    ret = reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fController.GetStatus( reinterpret_cast<BCLAbstrAddressStruct*>( process_address ), statusHeader );
    if ( ret )
	return ret;
#if 1
    //time_t tt;
    struct timeval tv;
    if ( AliHLTSCComponentInterface::GetUpdateTime( statusHeader, tv ) )
	{
	struct tm* t;
	t = localtime( &(tv.tv_sec) );
	t->tm_mon++;
	t->tm_year += 1900;
	unsigned long ldate, ltime_s, ltime_us;
	ldate = t->tm_year*10000+t->tm_mon*100+t->tm_mday;
	ltime_s = t->tm_hour*10000+t->tm_min*100+t->tm_sec;
	ltime_us = tv.tv_usec;
	char tmpStr[24];
	sprintf( tmpStr, "%08lu.%06lu.%06lu", ldate, ltime_s, ltime_us );
	statusData += "LastUpdateTime=";
	statusData += tmpStr;
	statusData += ":";
	}
    AliHLTSCHLTStatusStruct status;
    if ( AliHLTSCComponentInterface::GetHLTStatus( statusHeader, status ) )
	{
	char tmpStr[256];
	sprintf( tmpStr, "TotalOutputBuffer=%Lu", (unsigned long long)status.fTotalOutputBuffer );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "FreeOutputBuffer=%Lu", (unsigned long long)status.fFreeOutputBuffer );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "PendingInputEventCount=%Lu", (unsigned long long)status.fPendingInputEventCount );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "PendingOutputEventCount=%Lu", (unsigned long long)status.fPendingOutputEventCount );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "CurrentReceiveRate=%f", status.fCurrentReceiveRate );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "AvgReceiveRate=%f", status.fAvgReceiveRate );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "ResumedAvgReceiveRate=%f", status.fResumedAvgReceiveRate );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "CurrentProcessRate=%f", status.fCurrentProcessRate );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "AvgProcessRate=%f", status.fAvgProcessRate );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "ResumedAvgProcessRate=%f", status.fResumedAvgProcessRate );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "CurrentAnnounceRate=%f", status.fCurrentAnnounceRate );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "AvgAnnounceRate=%f", status.fAvgAnnounceRate );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "ResumedAvgAnnounceRate=%f", status.fResumedAvgAnnounceRate );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "ReceivedEventCount=%Lu", (unsigned long long)status.fReceivedEventCount );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "CurrentReceivedEventCount=%Lu", (unsigned long long)status.fCurrentReceivedEventCount );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "ProcessedEventCount=%Lu", (unsigned long long)status.fProcessedEventCount );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "CurrentProcessedEventCount=%Lu", (unsigned long long)status.fCurrentProcessedEventCount );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "AnnouncedEventCount=%Lu", (unsigned long long)status.fAnnouncedEventCount );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "CurrentAnnouncedEventCount=%Lu", (unsigned long long)status.fCurrentAnnouncedEventCount );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "TotalProcessedInputDataSize=%Lu", (unsigned long long)status.fTotalProcessedInputDataSize );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "CurrentProcessedInputDataSize=%Lu", (unsigned long long)status.fCurrentProcessedInputDataSize );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "TotalProcessedOutputDataSize=%Lu", (unsigned long long)status.fTotalProcessedOutputDataSize );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "CurrentProcessedOutputDataSize=%Lu", (unsigned long long)status.fCurrentProcessedOutputDataSize );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "CurrentProcessedInputDataRate=%f", status.fCurrentProcessedInputDataRate );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "AvgProcessedInputDataRate=%f", status.fAvgProcessedInputDataRate );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "ResumedProcessedInputDataRate=%f", status.fResumedProcessedInputDataRate );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "CurrentProcessedOutputDataRate=%f", status.fCurrentProcessedOutputDataRate );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "AvgProcessedOutputDataRate=%f", status.fAvgProcessedOutputDataRate );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "ResumedProcessedOutputDataRate=%f", status.fResumedProcessedOutputDataRate );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "TotalInput2OutputDataSizeRatio=%f", status.fTotalInput2OutputDataSizeRatio );
	statusData += tmpStr;
	statusData += ":";
	sprintf( tmpStr, "CurrentInput2OutputDataSizeRatio=%f", status.fCurrentInput2OutputDataSizeRatio );
	statusData += tmpStr;
        statusData += ":";
	sprintf( tmpStr, "TotalMaxEventLifetime=%Lu", (unsigned long long)status.fTotalMaxEventLifetime );
	statusData += tmpStr;
        statusData += ":";
	sprintf( tmpStr, "TotalMinEventLifetime=%Lu", (unsigned long long)status.fTotalMinEventLifetime );
	statusData += tmpStr;
        statusData += ":";
	sprintf( tmpStr, "CurrentAvgEventLifetime=%Lu", (unsigned long long)status.fCurrentAvgEventLifetime );
	statusData += tmpStr;
        statusData += ":";
	sprintf( tmpStr, "MaxOutOfOrderOrbits=%Lu", (unsigned long long)status.fMaxOutOfOrderOrbits);
	statusData += tmpStr;
	}
    else
	statusData += "";
    *stat_data = (char*)malloc( statusData.Length()+1 );
    if ( !*stat_data )
	return ENOMEM;
    strcpy( *stat_data, statusData.c_str() );
#else
    unsigned long len = 23;
    *stat_data = (char*)malloc( len );
    if ( *stat_data )
	{
	struct tm* t;
	//time_t tt;
	struct timeval tv;
	unsigned long ldate, ltime_s, ltime_us;
// 	gettimeofday( &tv, NULL );
	if ( AliHLTSCComponentInterface::GetUpdateTime( statusHeader, tv ) )
	    {
	    t = localtime( &(tv.tv_sec) );
	    t->tm_mon++;
	    t->tm_year += 1900;
	    ldate = t->tm_year*10000+t->tm_mon*100+t->tm_mday;
	    ltime_s = t->tm_hour*10000+t->tm_min*100+t->tm_sec;
	    ltime_us = tv.tv_usec;
	    sprintf( *stat_data, "%08lu.%06lu.%06lu", ldate, ltime_s, ltime_us );
	    }
	else
	    {
	    free( *stat_data );
	    *stat_data = NULL;
	    }
	}
#endif
    reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fController.FreeStatus( statusHeader );
    return 0;
    }

void ReleaseResource( void*, void* resource )
    {
    if ( resource )
	free( resource );
    }

/* Send the specified command to the given process */
int SendCommand( void* private_lib_data,
		 pid_t pid, 
		 const char*, 
		 void* process_address, 
		 const char* command )
    {
    int ret;

    vector<MLUCString> tokens;
    MLUCString tmpStr;
    if ( !reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fCmdParser.ParseCmd( command, tokens ) )
	{
	return EINVAL;
	}
    AliUInt32_t cmd = kAliHLTSCSFirstProcessControlCmd;
    
    for ( cmd = kAliHLTSCSFirstProcessControlCmd; cmd < kAliHLTSCSLastProcessControlCmd; cmd++ )
	{
	AliHLTSCComponentInterface::GetCommandName( cmd, tmpStr );
	if ( tmpStr == tokens[0] )
	    break;
	}
    if ( cmd == kAliHLTSCSLastProcessControlCmd && tokens[0]=="DUMPSTACKTRACE" )
	{
	kill( pid, SIGUSR1 );
	return 0;
	}
    if ( cmd == kAliHLTSCSLastProcessControlCmd && tokens[0]=="SetVerbosity" )
	cmd = kAliHLTSCSetLogLevelSysCmd;
    if ( cmd == kAliHLTSCSLastProcessControlCmd )
	{
	return EINVAL;
	}

    AliHLTSCCommand cmdData;
    cmdData.GetData()->fCmd = cmd;
    unsigned ndx=1;
    
    char* cpErr;
    cmdData.GetData()->fParam0 = 0;
    cmdData.GetData()->fParam1 = 0;

    if ( (cmd==kAliHLTPCInitiateReconfigureEventCmd || cmd==kAliHLTPCInitiateUpdateDCSEventCmd) && tokens.size()>ndx )
	{
	AliUInt64_t eventNr = strtoull( tokens[ndx].c_str(), &cpErr, 0 );
	if ( !*cpErr )
	    {
	    cmdData.GetData()->fParam0 = (eventNr >> 32) & 0x00000000FFFFFFFFULL;
	    cmdData.GetData()->fParam1 = eventNr & 0x00000000FFFFFFFFULL;
	    ndx++;
	    }
	}
    else
	{
	if ( tokens.size()>ndx )
	    {
	    cmdData.GetData()->fParam0 = strtoul( tokens[ndx].c_str(), &cpErr, 0 );
	    if ( *cpErr )
		{
		cmdData.GetData()->fParam0 = 0;
		//return EINVAL;
		}
	    else
		ndx++;
	    }
	
	if ( tokens.size()>ndx )
	    {
	    cmdData.GetData()->fParam1 = strtoul( tokens[ndx].c_str(), &cpErr, 0 );
	    if ( *cpErr )
		{
		cmdData.GetData()->fParam1 = 0;
		//return EINVAL;
		}
	    else
		ndx++;
	    }
	}
    
    AliHLTSCCommandStruct *cmdStruct=NULL;
    
    MLUCString cmdstr;
    if ( tokens.size()>ndx )
	{
	for ( ; ndx < tokens.size(); ndx++ )
	    {
	    cmdstr += tokens[ndx];
	    if ( ndx<tokens.size()-1 )
		cmdstr += " ";
	    }
	}
    else
	cmdstr = "";
    unsigned long len = cmdData.GetData()->fLength+cmdstr.Length()+1;
    cmdStruct = (AliHLTSCCommandStruct*)new AliUInt8_t[ len ];
    if ( !cmdStruct )
	{
	return ENOMEM;
	}
    *cmdStruct = *(cmdData.GetData());
    strcpy( (char*)(cmdStruct->fData), cmdstr.c_str() );
    cmdStruct->fLength += cmdstr.Length()+1;
    cmdStruct->fDataCnt = 3;
//     }
// else
// cmdStruct = cmdData.GetData();

    ret = reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fController.SendCommand( reinterpret_cast<BCLAbstrAddressStruct*>( process_address ), cmdStruct );
    
    if ( cmdStruct != cmdData.GetData() )
	delete [] (AliUInt8_t*)cmdStruct;

    return ret;
    }

/* Callback function for the interrupt wait function below.
** The last three parameters are used to identify the process
** that triggered the interrupt. Only one of them (whichever
** suits the library implementation best) has to present. */
// typedef void (*InterruptTriggerCallback_t)( void* opaque_arg, 
// 					    pid_t pid,
// 					    const char* process_id,
// 					    const char* process_address );

/* Enter a wait that calls the specified callback when a process 
** has triggered an interrupt  (LAM).
** The address of the triggering process is passed in the call 
** to the specified callback function.
** This is called in its own thread to run as an endless loop
** and only returns when the StopInterruptWait function
** (see below) is called.. */
int InterruptWait( void* private_lib_data,
		   void* opaque_arg, 
		   const char*, 
		   InterruptTriggerCallback_t callback )
    {
    char* lamAddr;
    reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fQuitInterruptWait = false;
    reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fInterruptWaitRunning = true;
    reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fSignal.Lock();
    while ( !reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fQuitInterruptWait )
	{
	reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fSignal.Wait( 50 );
	if ( reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fQuitInterruptWait )
	    break;
	while ( reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fSignal.HaveNotificationData() && 
		!reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fQuitInterruptWait )
	    {
	    lamAddr = reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fSignal.PopNotificationData();
	    if ( !lamAddr )
		continue;
	    reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fSignal.Unlock();
	    callback( opaque_arg, 0, NULL, lamAddr, NULL );
	    BCLFreeEncodedAddress( lamAddr );
	    reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fSignal.Lock();
	    }
	}
    reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fInterruptWaitRunning = false;
    reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fSignal.Unlock();
    return 0;
    }

/* Stop the interrupt wait loop gracefully. */
int StopInterruptWait( void* private_lib_data )
    {
    reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fQuitInterruptWait = true;
    reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fSignal.Signal();
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    gettimeofday( &start, NULL );
    while ( reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fInterruptWaitRunning && deltaT<timeLimit )
	{
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fInterruptWaitRunning )
	return EIO;
    return 0;
    }

/* Connection function.
   Establish a permanent connection to the given process.
   This function is optional and need not be supplied in the interface
   library for compatibility reasons.
*/
int ConnectToProcess( void* private_lib_data,
		      pid_t, 
		      const char*, 
		      void* process_address )
    {
    int ret;
#if 1
    ret = reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fController.Connect( reinterpret_cast<BCLAbstrAddressStruct*>( process_address ), false );
#else
    ret = reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fController.Connect( reinterpret_cast<BCLAbstrAddressStruct*>( process_address ), true );
#endif
    return ret;
    }

/* Disconnection function.
   Severe a previously established permanent connection to the given 
   process.
   This function is optional and need not be supplied in the interface
   library for compatibility reasons.
*/
int DisconnectFromProcess( void* private_lib_data,
			   pid_t, 
			   const char*, 
			   void* process_address )
    {
    int ret;
    ret = reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fController.Disconnect( reinterpret_cast<BCLAbstrAddressStruct*>( process_address ) );
    return ret;
    }

int SuppressComErrorLogs( void* private_lib_data,
			  pid_t /*pid*/, 
			  const char* /*process_id*/, 
			  void* process_address_bin,
			  unsigned long timeout_us )
    {
    reinterpret_cast<AliHLTTaskManagerInterface*>( private_lib_data )->fController.SuppressComErrorLogs( reinterpret_cast<BCLAbstrAddressStruct*>( process_address_bin ), timeout_us );
    return 0;
    }





AliHLTTaskManagerInterface::AliHLTTaskManagerInterface():
    fSignal( 4 ),
    fLAM( this )
    {
    fQuitInterruptWait = true;
    fInterruptWaitRunning = false;
    }
	
void AliHLTTaskManagerInterface::LAMHandler::LookAtMe( BCLAbstrAddressStruct* me )
    {
    char* addr;
    BCLEncodeAddress( me, false, addr );
    if ( addr )
	{
	fInterface->fSignal.AddNotificationData( addr );
	fInterface->fSignal.Signal();
	}
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
