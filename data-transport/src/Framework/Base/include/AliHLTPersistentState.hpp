#ifndef _AliHLTPERSISTENTSTATE_HPP_
#define _AliHLTPERSISTENTSTATE_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include <sys/ipc.h>
#include <sys/shm.h>


class AliHLTPersistentStateBase
    {
    public:

	AliHLTPersistentStateBase( int key, int size, int perm = 0755 )
		{
		fExisted = false;
		fPtr = NULL;
		int ret;
		ret = shmget( (key_t)key, size, perm );
		if ( ret!=-1 )
		    fExisted = true;
		else
		    ret = shmget( (key_t)key, size, IPC_CREAT|perm );
		if ( ret==-1 )
		    {
		    return;
		    }
		fShmID = ret;
		fPtr = shmat( fShmID, NULL, 0 );
		if ( !fPtr )
		    {
// 		    shmctl( fShmID, IPC_RMID, NULL );
// 		    fShmID = -1;
		    return;
		    }
		}
	AliHLTPersistentStateBase( const AliHLTPersistentStateBase& org )
		{
		fExisted = false;
		fPtr = NULL;
		fShmID = org.fShmID;
		if ( fShmID!=-1 )
		    fPtr = shmat( fShmID, NULL, 0 );
		}
	~AliHLTPersistentStateBase()
		{
		if ( fPtr )
		    {
		    shmdt( fPtr );
		    fPtr = NULL;
		    }
		if ( fShmID!=-1 )
		    {
		    shmctl( fShmID, IPC_RMID, NULL );
		    fShmID = -1;
		    }
		}
	operator bool()
		{
		return (bool)fPtr;
		}
	operator void*()
		{
		return (void*)fPtr;
		}
	operator AliUInt8_t*()
		{
		return (AliUInt8_t*)fPtr;
		}
	
	AliHLTPersistentStateBase& operator=( const AliHLTPersistentStateBase& org )
		{
		fExisted = true;
		if ( fPtr )
		    {
		    shmdt( fPtr );
		    fPtr = NULL;
		    }
		if ( fShmID!=-1 )
		    {
		    shmctl( fShmID, IPC_RMID, NULL );
		    fShmID = -1;
		    }
		fShmID = org.fShmID;
		if ( fShmID!=-1 )
		    fPtr = shmat( fShmID, NULL, 0 );
		return *this;
		}

	void* GetBasePtr()
		{
		return fPtr;
		}
	bool DidExist() const
		{
		return fExisted;
		}

    protected:

	void* fPtr;
	int fShmID;
	bool fExisted;

    private:
    };


template<class T>
class AliHLTPersistentState: public AliHLTPersistentStateBase
    {
    public:

	AliHLTPersistentState( int key, int perm = 0755 ):
	    AliHLTPersistentStateBase( key, sizeof(T), perm )
		{
		T tmpT;
		if ( !fExisted )
		    *reinterpret_cast<T*>( fPtr ) = tmpT;
		}

	T* operator->()
		{
		return reinterpret_cast<T*>( fPtr );
		}
	T& operator*()
		{
		return *reinterpret_cast<T*>( fPtr );
		}
	T& operator []( int i )
		{
		return (reinterpret_cast<T*>( fPtr ))[i];
		}
	operator T*()
		{
		return reinterpret_cast<T*>( fPtr );
		}
	T* GetPtr()
		{
		return reinterpret_cast<T*>( fPtr );
		}

    protected:
    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _AliHLTPERSISTENTSTATE_HPP_
