/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3DESCRIPTORHANDLER_HPP_
#define _ALIL3DESCRIPTORHANDLER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTLog.hpp"
#include "MLUCIndexedVector.hpp"
#include "MLUCDynamicAllocCache.hpp"

class AliHLTDescriptorHandler
    {
    public:

	AliHLTDescriptorHandler( unsigned maxPreSubEventsPerBlockCountExp2, bool grow=true );
	virtual ~AliHLTDescriptorHandler();

	AliHLTSubEventDataDescriptor* GetFreeEventDescriptor( AliEventID_t eventID, AliUInt32_t blockCount );

	AliHLTSubEventDataDescriptor* CloneEventDescriptor( AliHLTSubEventDataDescriptor const* oldSEDD );
	AliHLTSubEventDataDescriptor* CloneEventDescriptor( AliHLTSubEventDataDescriptor const& oldSEDD )
		{
		return CloneEventDescriptor( &oldSEDD );
		}
	
	AliHLTSubEventDataDescriptor* GetEventDescriptor( unsigned long ndx );
	
	void ReleaseEventDescriptor( AliEventID_t eventID );

	//bool FindSEDD( AliEventID_t eventID, bool& isPre, unsigned long& ndx );
	bool FindSEDD( AliEventID_t eventID, unsigned long& ndx );

	operator bool();

    protected:

	struct SEDDData
	    {
		AliEventID_t fEventID;
		AliHLTSubEventDataDescriptor* fSEDDPtr;
	    };
	
	MLUCIndexedVector<SEDDData,AliEventID_t> fSEDDs;
	MLUCDynamicAllocCache fSEDDCache;

	static bool SearchSEDD( const SEDDData& bufferElement, const AliEventID_t& refID )
		{
		return ( bufferElement.fEventID == refID );
		}

    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3DESCRIPTORHANDLER_HPP_
