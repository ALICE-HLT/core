/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://wiki.kip.uni-heidelberg.de/ti/HLT/index.php/Main_Page
** or the corresponding page of the Heidelberg Alice HLT group.
**
*************************************************************************/
#ifndef _ALIHLTEVENTMEDATADATAOUTPUT_HPP_
#define _ALIHLTEVENTMEDATADATAOUTPUT_HPP_

/*
***************************************************************************
**
** $Author: artur $ - Initial Version by Timm Morten Steinbeck
**
** $Id:  $ 
**
***************************************************************************
*/

//#include "AliHLTSubEventDescriptor.hpp"
#include "AliHLTTypes.h"
#include "AliEventID.h"
#include "MLUCString.hpp"
class AliHLTSubEventDataDescriptor;
class AliHLTEventTriggerStruct;


/*
 * This is a simple helper class used to write event meta data (descriptor and data block descriptors) to
 * files on disk.
 */
class AliHLTEventMetaDataOutput
    {
    public:

	typedef MLUCString (*FormatEventTriggerData)( const AliHLTEventTriggerStruct* ets );

	AliHLTEventMetaDataOutput( const char* filenamePrefix, bool aliceHLT, unsigned long runNumber=0 );
	~AliHLTEventMetaDataOutput();

	void SetEventTriggerDataFormatFunction( FormatEventTriggerData formatFunc )
		{
		fEventTriggerFormatFunc = formatFunc;
		}

	int Open();
	int Close();

        bool WriteEvent( const AliEventID_t& eventID,
			 const AliHLTSubEventDataDescriptor* sedd,
			 const AliHLTEventTriggerStruct* ets, 
			 const char* classifier );


        bool WriteEvent( struct timeval timestamp,
			 const AliEventID_t& eventID,
			 const AliHLTSubEventDataDescriptor* sedd,
			 const AliHLTEventTriggerStruct* ets,
			 const char* classifier);
	
    protected:

	MLUCString fFilenamePrefix;
	bool fAliceHLT;
	unsigned long fRunNumber;

	int fFileHandle;

	FormatEventTriggerData fEventTriggerFormatFunc;
    
    };


/*
***************************************************************************
**
** $Author: artur $ - Initial Version by Timm Morten Steinbeck
**
** $Id:  $ 
**
***************************************************************************
*/
#endif // _ALIHLTEVENTMEDATADATAOUTPUT_HPP_
