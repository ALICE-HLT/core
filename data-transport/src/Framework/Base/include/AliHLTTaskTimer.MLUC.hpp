/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3TASKTIMER_HPP_
#define _ALIL3TASKTIMER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTimedTask.hpp"
#include "MLUCTimer.hpp"
#include "MLUCConditionSem.hpp"


typedef MLUCConditionSem AliHLTConditionSem;
typedef MLUCThread AliHLTThread;
typedef MLUCThreadFIFO AliHLTThreadFIFO;
typedef MLUCTimerCallback AliHLTTimerCallback;
typedef MLUCTimer AliHLTTimer;
typedef MLUCTimerSignal AliHLTTimerSignal;
typedef MLUCTimedTask AliHLTTimedTask;
typedef MLUCTimedTaskHandler AliHLTTimedTaskHandler;




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3TASKTIMER_HPP_
