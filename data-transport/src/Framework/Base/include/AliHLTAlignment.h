#ifndef _ALIHLTALIGNMENT_H_
#define _ALIHLTALIGNMENT_H_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"

#ifdef __cplusplus
extern "C" {
#endif

    AliUInt8_t AliHLTDetermineUInt64Alignment();
    AliUInt8_t AliHLTDetermineUInt32Alignment();
    AliUInt8_t AliHLTDetermineUInt16Alignment();
    AliUInt8_t AliHLTDetermineUInt8Alignment();
    AliUInt8_t AliHLTDetermineDoubleAlignment();
    AliUInt8_t AliHLTDetermineFloatAlignment();


#ifdef __cplusplus
}
#endif

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif /* _ALIHLTALIGNMENT_H_ */
