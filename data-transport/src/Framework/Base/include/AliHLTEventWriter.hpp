/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://wiki.kip.uni-heidelberg.de/ti/HLT/index.php/Main_Page
** or the corresponding page of the Heidelberg Alice HLT group.
**
*************************************************************************/
#ifndef _ALIHLTEVENTWRITER_HPP_
#define _ALIHLTEVENTWRITER_HPP_

/*
***************************************************************************
**
** $Author: artur $ - Initial Version by Artur Szostak
**
** $Id:  $ 
**
***************************************************************************
*/

#include "AliHLTSubEventDescriptor.hpp"
class AliHLTSubEventDataDescriptor;
class AliHLTEventTriggerStruct;


/*
 * This is a simple helper class used to write events and their data blocks to
 * files on disk.
 */
class AliHLTEventWriter
    {
    public:
    
        static bool WriteEvent(
			const AliEventID_t& eventID,
			const vector<AliHLTSubEventDescriptor::BlockData>& blocks,
			const AliHLTSubEventDataDescriptor* sedd,
			const AliHLTEventTriggerStruct* ets,
			unsigned long runNumber = 0,
			const char* filePrefix = ""
		);
	
        static bool WriteEvent(
			const AliEventID_t& eventID,
			AliUInt32_t blockCnt,
			const AliHLTSubEventDescriptor::BlockData* blocks,
			const AliHLTSubEventDataDescriptor* sedd,
			const AliHLTEventTriggerStruct* ets,
			unsigned long runNumber = 0,
			const char* filePrefix = ""
		);
	
	static bool WriteToFile(const char* filename, const void* data, unsigned long size);

    private:
    
    	// Should not need to create or destroy this class.
        AliHLTEventWriter() {}
        ~AliHLTEventWriter() {}
    };


/*
***************************************************************************
**
** $Author: artur $ - Initial Version by Artur Szostak
**
** $Id:  $ 
**
***************************************************************************
*/
#endif // _ALIHLTEVENTWRITER_HPP_
