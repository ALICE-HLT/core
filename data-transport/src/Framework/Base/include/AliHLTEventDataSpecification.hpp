#ifndef _ALIHLTEVENTDATASPECIFICATION_HPP_
#define _ALIHLTEVENTDATASPECIFICATION_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"

inline AliUInt16_t AliHLTDataSpecTPCGetMinSliceNr( AliUInt32_t dataSpec )
    {
    return (AliUInt16_t)( (dataSpec & 0x00FF0000) >> 16 );
    }

inline AliUInt16_t AliHLTDataSpecTPCGetMaxSliceNr( AliUInt32_t dataSpec )
    {
    return (AliUInt16_t)( (dataSpec & 0xFF000000) >> 24 );
    }

inline AliUInt16_t AliHLTDataSpecTPCGetMinPatchNr( AliUInt32_t dataSpec )
    {
    return (AliUInt16_t)( dataSpec & 0x000000FF );
    }

inline AliUInt16_t AliHLTDataSpecTPCGetMaxPatchNr( AliUInt32_t dataSpec )
    {
    return (AliUInt16_t)( (dataSpec & 0x0000FF00) >> 8 );
    }

inline AliUInt32_t AliHLTDataSpecMakeTPCDataSpecification( AliUInt16_t minSliceNr, 
								  AliUInt16_t maxSliceNr,
								  AliUInt16_t minPatchNr,
								  AliUInt16_t maxPatchNr )
    {
    return ( (maxSliceNr & 0xFF) << 24 ) | ( (minSliceNr & 0xFF) << 16 ) | ( (maxPatchNr & 0xFF) << 8 ) | (minPatchNr & 0xFF);
    }

inline AliUInt16_t AliHLTDataSpecDiMuonGetMinDDLNr( AliUInt32_t dataSpec )
    {
    return (AliUInt16_t)( dataSpec & 0x000000FF );
    }

inline AliUInt16_t AliHLTDataSpecDiMuonGetMaxDDLNr( AliUInt32_t dataSpec )
    {
    return (AliUInt16_t)( (dataSpec & 0x0000FF00) >> 8 );
    }

inline AliUInt32_t AliHLTDataSpecMakeDiMuonDataSpecification( AliUInt16_t minDDLNr, 
								  AliUInt16_t maxDDLNr )
    {
    return ( (maxDDLNr & 0xFF) << 8 ) | (minDDLNr & 0xFF);
    }

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTEVENTDATASPECIFICATION_HPP_
