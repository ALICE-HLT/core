#ifndef _ALIL3REPLYSIGNAL_HPP_
#define _ALIL3REPLYSIGNAL_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTimer.hpp"
#include "MLUCVector.hpp"
#include <pthread.h>

class AliHLTReplySignal: public AliHLTTimerConditionSem
    {
    public:

	AliHLTReplySignal();
	~AliHLTReplySignal();

	AliUInt64_t GetReplyNr();

	AliUInt64_t  WaitForReply( AliUInt64_t replyNr );

	void SignalReply( AliUInt64_t replyNr, AliUInt64_t retval );

    protected:

	struct ReplyData
	    {
		AliUInt64_t fReplyNr;
		AliUInt64_t fRetval;
	    };

	MLUCVector<ReplyData> fReplies;
	pthread_mutex_t fRepliesMutex;

	pthread_mutex_t fReplyNrMutex;
	AliUInt64_t fNextReplyNr;

	static bool ReplySearchFunc( const ReplyData& elem, const AliUInt64_t& reference )
		{
		return elem.fReplyNr == reference;
		}
	
    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3REPLYSIGNAL_HPP_
