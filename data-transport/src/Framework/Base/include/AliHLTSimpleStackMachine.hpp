#ifndef _ALIHLTSIMPLESTACKMACHINE_HPP_
#define _ALIHLTSIMPLESTACKMACHINE_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCSimpleStackMachine.hpp"

struct AliHLTEventTriggerStruct;

class AliHLTSimpleStackMachine: public MLUCSimpleStackMachine
    {
    public:


	AliHLTSimpleStackMachine():
	    fEventTriggerStruct(0)
		{ }
	virtual ~AliHLTSimpleStackMachine() {}

	void SetEventTriggerStruct( const struct AliHLTEventTriggerStruct* triggerData )
		{
		fEventTriggerStruct = triggerData;
		}


    protected:

	const struct AliHLTEventTriggerStruct* fEventTriggerStruct;


    private:
    };


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTSIMPLESTACKMACHINE_HPP_
