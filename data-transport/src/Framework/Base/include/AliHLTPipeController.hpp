/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3PIPECONTROLLER_HPP_
#define _ALIL3PIPECONTROLLER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliHLTThread.hpp"
#include "MLUCString.hpp"
#include <vector>
#include <pthread.h>

#if __GNUC__>=3
using namespace std;
#endif


class AliHLTPipeController: public AliHLTThread
    {
    public:

	AliHLTPipeController( const char* pipename );
	~AliHLTPipeController();

	void AddValue( AliUInt32_t& val );

	void SetTimeout( unsigned long comTimeout_usec ) // 0 disables.
		{
		fTimeout = comTimeout_usec;
		}

	void SetQuitValue( AliUInt32_t& val )
		{
		fQuitValue = &val;
		}

	bool HasQuitted()
		{
		return fQuitted;
		}

	void Quit();

    protected:

	virtual void Run();
	
	MLUCString fPipeName;
	int fPipeHandle;

	unsigned long fTimeout;

	AliUInt32_t* fQuitValue;
	AliUInt32_t fLocalQuitValue;
	bool fQuitted;

	vector<AliUInt32_t*> fValues;
	pthread_mutex_t fValueMutex;

    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3PIPECONTROLLER_HPP_
