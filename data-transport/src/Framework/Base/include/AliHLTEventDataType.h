/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3EVENTDATATYPE_H_
#define _ALIL3EVENTDATATYPE_H_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"

#ifdef __cplusplus
extern "C" {
#endif


    union AliHLTEventDataType
	{
	    AliUInt64_t     fID;
	    AliUInt8_t      fDescr[8];
	};

    typedef union AliHLTEventDataType AliHLTEventDataType;

#define UNKNOWN_DATAID               (((AliUInt64_t)'UNKN')<<32 | 'OWN ')
#define COMPOSITE_DATAID             (((AliUInt64_t)'COMP')<<32 | 'OSIT')
#define ADCCOUNTS_DATAID             (((AliUInt64_t)'ADCC')<<32 | 'OUNT')
#define ADCCOUNTS_UNPACKED_DATAID    (((AliUInt64_t)'ADCC')<<32 | 'NTUP')
#define CLUSTERS_DATAID              (((AliUInt64_t)'CLUS')<<32 | 'TERS')
#define SPACEPOINTS_DATAID           (((AliUInt64_t)'SPAC')<<32 | 'EPTS')
#define VERTEXDATA_DATAID            (((AliUInt64_t)'VRTX')<<32 | 'DATA')
#define TRACKSEGMENTS_DATAID         (((AliUInt64_t)'TRAC')<<32 | 'SEGS')
#define SLICETRACKS_DATAID           (((AliUInt64_t)'SLCT')<<32 | 'RCKS')
#define TRACKS_DATAID                (((AliUInt64_t)'TRAC')<<32 | 'KS  ')
#define NODELIST_DATAID              (((AliUInt64_t)'NODE')<<32 | 'LIST')
#define EVENTTRIGGER_DATAID          (((AliUInt64_t)'EVTT')<<32 | 'RG  ')
#define DDLHEADER_DATAID             (((AliUInt64_t)'DDLH')<<32 | 'EADR')
#define DDLDATA_DATAID               (((AliUInt64_t)'DDLD')<<32 | 'ATA ')
#define DDLRAWDATAPACKED_DATAID      (((AliUInt64_t)'DDL_')<<32 | 'RWPK')
#define DDLRAWDATA_DATAID            (((AliUInt64_t)'DDL_')<<32 | 'RAW ')
#define DDLRAWDATASPLIT_DATAID       (((AliUInt64_t)'DDL_')<<32 | 'SPLT')
#define DDLPREPROCDATA_DATAID        (((AliUInt64_t)'DDLP')<<32 | 'RPRC')
#define DDLLIST_DATAID               (((AliUInt64_t)'DDLL')<<32 | 'IST ')
#define HLTREADOUTLIST_DATAID        (((AliUInt64_t)'HLTR')<<32 | 'DLST')
#define HLTOUTPUT_DATAID             (((AliUInt64_t)'HLTO')<<32 | 'UTPT')
#define STARTOFRUN_DATAID            (((AliUInt64_t)'STAR')<<32 | 'TOFR')
#define ENDOFRUN_DATAID              (((AliUInt64_t)'ENDO')<<32 | 'FRUN')
#define RUNTYPE_DATAID               (((AliUInt64_t)'RUNT')<<32 | 'YPE ')
#define COMPONENTCONFIGIDS_DATAID    (((AliUInt64_t)'CONF')<<32 | 'IDS ')
#define COMPONENTCONFIGPATHS_DATAID  (((AliUInt64_t)'CONF')<<32 | 'PATH')
#define COMPONENTCONFIGCOMPS_DATAID  (((AliUInt64_t)'CONF')<<32 | 'COMP')
#define DCSUPDATE_DATAID             (((AliUInt64_t)'UPDT')<<32 | '_DCS')
#define EVENTTYPE_DATAID             (((AliUInt64_t)'EVEN')<<32 | 'TTYP')
#define ECSPARAM_DATAID              (((AliUInt64_t)'ECSP')<<32 | 'ARAM')
#define EVENTDONEDATA_DATAID         (((AliUInt64_t)'EVTD')<<32 | 'NDTA')
#define METADATA_DATAID              (((AliUInt64_t)'META')<<32 | 'DATA')

#define TPCTESTBEAM_ALTRODATAID          (((AliUInt64_t)'TTBA')<<32 | 'LTRO')
#define TPCTESTBEAM_UNPACKEDALTRODATAID  (((AliUInt64_t)'TTBU')<<32 | 'PALT')
#define TPCTESTBEAM_HLTALTRODATAID       (((AliUInt64_t)'TTBH')<<32 | 'LTAL')
#define TPCTESTBEAM_HLTCLUSTERSDATAID    (((AliUInt64_t)'TTBH')<<32 | 'LTCL')

    //#define ALL_DATAID                   (~(AliUInt64_t)0)
#define ALL_DATAID                   (((AliUInt64_t)'****')<<32 | '***\0')
#define NONE_DATAID                  ((AliUInt64_t)0)


#ifdef __cplusplus
}
#endif


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3EVENTDATATYPE_H_
