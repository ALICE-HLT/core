/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3BUFFERMANAGER1_HPP_
#define _ALIL3BUFFERMANAGER1_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTBufferManager.hpp"
#include "MLUCList.hpp"
#include "MLUCIndexedVector.hpp"
#include <vector>
#include <pthread.h>


class AliHLTLargestBlockBufferManager: public AliHLTBufferManager
    {
    public:

	AliHLTLargestBlockBufferManager( unsigned bufferSizeExp2 = 1 );
	AliHLTLargestBlockBufferManager( AliHLTShmID shmKey, unsigned long bufferSize, unsigned bufferSizeExp2 = 1 );

	virtual ~AliHLTLargestBlockBufferManager();

	virtual bool AddBuffer( AliHLTShmID shmKey, unsigned long bufferSize );

	virtual bool DeleteBuffer( AliHLTShmID shmKey, bool force = false );

	virtual bool CanGetBlock();

	virtual bool GetBlock( AliUInt32_t& bufferNdx, unsigned long& offset,
				unsigned long& size );

	virtual bool AdaptBlock( AliUInt32_t bufferNdx, unsigned long offset,
				 unsigned long newSize );

	virtual bool AdaptBlock( AliUInt32_t bufferNdx, unsigned long oldOffset,
				 unsigned long newOffset, unsigned long newSize );

	virtual bool ReleaseBlock( AliUInt32_t bufferNdx, unsigned long offset, bool noAdaptBlock=false );

	void SetAdaptBlockFreeUpdate( bool adaptBlockFreeUpdate )
		{
		fAdaptBlockFreeUpdate = adaptBlockFreeUpdate;
		}

    protected:


	vector<TBufferData> fBuffers;

	struct TBlockData
	    {
		AliUInt32_t fBufferNdx;
		unsigned long fOffset;
		unsigned long fSize;
	    };

	struct TBlockIndexType
	    {
		TBlockIndexType( AliUInt32_t bufferNdx, unsigned long offset )
			{
			fBufferNdx = bufferNdx;
			fOffset = offset;
			}
		TBlockIndexType()
			{
			fBufferNdx = ~(AliUInt32_t)0;
			fOffset = ~(unsigned long)0;
			}
		AliUInt32_t fBufferNdx;
		unsigned long fOffset;
		unsigned long operator & ( unsigned long mask ) const
			{
			return this->fOffset & mask;
			}
		unsigned long operator / ( unsigned long mask ) const
			{
			return this->fOffset / mask;
			}
		bool operator==( TBlockIndexType const& comp ) const
			{
			return fBufferNdx==comp.fBufferNdx && fOffset==comp.fOffset;
			}
		    friend MLUCString& operator<<( MLUCString& str, TBlockIndexType const& comp );
	    };

	//vector<TBlockData> fUsedBlocks;
	//vector<TBlockData> fFreeBlocks;
	//vector<TBlockData>::iterator fCurrentBlock;
	MLUCIndexedVector<TBlockData, TBlockIndexType> fUsedBlocks;
	MLUCList<TBlockData> fFreeBlocks;
	MLUCList<TBlockData>::TIterator fLargestFreeBlock;
	unsigned long fLargestSize;
	TBlockData* fCurrentBlock;
	//unsigned long fCurrentBlockIndex;
	bool fCurrentBlockSet;

	bool fAdaptBlockFreeUpdate;

	static bool BufferBlockSearchFunc( const TBlockData& el, const AliUInt32_t& searchBufferNdx );	    
		

	vector<TBufferData>::iterator fCurrentBuffer;

	pthread_mutex_t fBlockMutex;

	void InsertFree( TBlockData& block, bool updateFree=true );

	void DumpBlocks();

	friend class MLUCString& operator<<( MLUCString& str, AliHLTLargestBlockBufferManager::TBlockIndexType const& comp );

    };


inline MLUCString& operator<<( MLUCString& str, AliHLTLargestBlockBufferManager::TBlockIndexType const& comp ) 
    {
    char tmp[4096];
    sprintf( tmp, "%10lu:%16lu / 0x%08lX:0x%08lX", (unsigned long)comp.fBufferNdx, comp.fOffset, (unsigned long)comp.fBufferNdx, comp.fOffset );
    str += tmp;
    return str;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3BUFFERMANAGER1_HPP_
