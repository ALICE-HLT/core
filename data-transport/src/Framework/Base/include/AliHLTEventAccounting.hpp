#ifndef _ALIHLTEVENTACCOUNTING_HPP_
#define _ALIHLTEVENTACCOUNTING_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliEventID.h"
#include "MLUCString.hpp"
#include "MLUCMutex.hpp"
#include <list>
#include <sys/times.h>

struct AliHLTSubEventDataDescriptor;
class AliHLTShmManager;

class AliHLTEventAccounting
    {
    public:

	AliHLTEventAccounting( bool printImmediately=true );
	~AliHLTEventAccounting();

	void SetShmManager( AliHLTShmManager* shmMan )
		{
		fShmMan = shmMan;
		}
	
	void SetName( char const* name )
		{
		fName = name;
		}
	void SetDirectory( char const* directory )
		{
		fDirectory = directory;
		}
	void SetRun( unsigned long run )
		{
		fRun = run;
		}

	void NewEvent( char const* name, AliHLTSubEventDataDescriptor const * const sedd );
	void AnnouncedEvent( char const* name, AliHLTSubEventDataDescriptor const * const sedd );
	void EventProcessed( AliEventID_t eventID, unsigned outputBlockCnt, unsigned long totalOutputSize );
	void EventForwarded( AliEventID_t eventID );
	void EventFinished( char const* name, AliEventID_t eventID, unsigned forceForward=0, unsigned forwarded=0 );
	void EventDoneData( char const* name, AliEventID_t eventID, unsigned wordCount );
	
	void DumpEvents();

	void EventEntered( char const* name, AliEventID_t eventID );
	void EventLeft( char const* name, AliEventID_t eventID );

    protected:

	enum TEventAction { kAnnounced, kReceived, kProcessed, kForwarded, kFinished, kEventDoneData, kEntered, kLeft };

	struct TEventEvent
	    {
		struct timeval fTimestamp;
		TEventAction fActionType;
		MLUCString fName;
		unsigned fBlockCnt;
		unsigned long fTotalSize;
	    };
		
	struct TEventData
	    {
		AliEventID_t fEventID;
		struct timeval fEventBirthTime;
		std::list<TEventEvent> fEvents;
	    };

	int OpenFile();

	void DumpLine( AliEventID_t eventID, TEventAction actionType, const char* name, unsigned blockCnt, unsigned long totalSize, unsigned forceForward=0, unsigned forwarded=0 );

	void WriteTime( int fh, char const* text, struct timeval const& t );

#if 0
	std::list<TEventData>::iterator FindEvent( AliEventID_t eventID, std::list<TEventData>::iterator& lastFoundEvent );
#endif
	std::list<TEventData>::iterator FindEvent( AliEventID_t eventID, bool create=false );

	void NewEvent( char const* name, AliHLTSubEventDataDescriptor const * const sedd, TEventAction state );



	std::list<TEventData> fEvents;
	std::list<TEventData>::iterator fLastFoundEventNew;
	std::list<TEventData>::iterator fLastFoundEventProcessed;
	std::list<TEventData>::iterator fLastFoundEventFinished;

	MLUCString fDirectory;
	MLUCString fNode;
	MLUCString fName;

	unsigned long fRun;

	AliHLTShmManager* fShmMan;

	MLUCMutex fMutex;

	bool fPrintImmediately;

	int fFH;

	bool fTrackEventCount;
	MLUCMutex fEventCountMutex;
	unsigned long fEventCount;

    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTEVENTACCOUNTING_HPP_
