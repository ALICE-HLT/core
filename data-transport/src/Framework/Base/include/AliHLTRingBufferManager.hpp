/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3RINGBUFFERMANAGER_HPP_
#define _ALIL3RINGBUFFERMANAGER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTBufferManager.hpp"
#include <vector>
#include <pthread.h>


class AliHLTRingBufferManager: public AliHLTBufferManager
    {
    public:

	AliHLTRingBufferManager( unsigned long blockSize );
	AliHLTRingBufferManager( AliHLTShmID shmKey, unsigned long bufferSize, unsigned long blockSize );

	virtual ~AliHLTRingBufferManager();

	virtual bool AddBuffer( AliHLTShmID shmKey, unsigned long bufferSize );
	virtual bool DeleteBuffer( AliHLTShmID shmKey, bool force = false );

	virtual bool CanGetBlock();

	virtual bool GetBlock( AliUInt32_t& bufferNdx, unsigned long& offset,
				unsigned long& size );

	virtual bool AdaptBlock( AliUInt32_t bufferNdx, unsigned long offset,
				 unsigned long newSize );

	virtual bool AdaptBlock( AliUInt32_t bufferNdx, unsigned long oldOffset,
				 unsigned long newOffset, unsigned long newSize );

	virtual bool ReleaseBlock( AliUInt32_t bufferNdx, unsigned long offset, bool noAdaptBlock=false );

	virtual bool IsBlockFeasible( unsigned long size );

    protected:

	struct TBlockData
	    {
		bool fUsed;
		AliUInt32_t fBufferNdx;
		unsigned long fOffset;
	    };

	
// 	pthread_mutex_t fBufferMutex;
// 	vector<TBufferData> fBuffers;
	
	pthread_mutex_t fBlockMutex;
	vector<TBlockData> fBlocks;
// 	TBlockData* fBlockStart;

	TBlockData* fBlockEnd;
	TBlockData* fNextBlock;

	unsigned long fBlockSize;
	unsigned long fBlockCnt;
	
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3RINGBUFFERMANAGER_HPP_
