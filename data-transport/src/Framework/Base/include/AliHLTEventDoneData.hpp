#ifndef _ALIHLTEVENTDONEDATA_HPP_
#define _ALIHLTEVENTDONEDATA_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTEventDoneData.h"
#include <vector>


#if __GNUC__>=3
using namespace std;
#endif

const unsigned long gkAliHLTEventDoneBackPressureHighWaterMarkExpiryTime_s = 10;


inline void AliHLTMakeEventDoneData( AliHLTEventDoneData* edd, AliEventID_t eventID, AliUInt64_t statusFlags=0 )
    {
    edd->fHeader.fLength = sizeof(AliHLTEventDoneData);
    edd->fHeader.fType.fID = ALIL3EVENTDONEDATA_TYPE;
    edd->fHeader.fSubType.fID = 0;
    edd->fHeader.fVersion = 1;
    edd->fEventID = eventID;
    edd->fStatusFlags = statusFlags;
    edd->fDataWordCount = 0;
    }

/*
  Returns a NULL poitner when the number of data words in the collected input
  data is zero or if no input structures are present (or of course on allocation
  failure...).
*/
AliHLTEventDoneData* AliHLTMergeEventDoneData( AliEventID_t eventID, vector<AliHLTEventDoneData*>& eventDoneData );
unsigned long AliHLTGetMergedEventDoneDataSize( vector<AliHLTEventDoneData*>& eventDoneData );
void AliHLTMergeEventDoneData( AliEventID_t eventID, AliHLTEventDoneData* eddTarget, vector<AliHLTEventDoneData*>& eventDoneData );

void AliHLTFreeMergedEventDoneData( AliHLTEventDoneData* ed );


AliHLTEventDoneData* AliHLTMakeEventDoneData( AliUInt32_t dataWordCount, AliUInt64_t statusFlags=0 );
void AliHLTFreeEventDoneData( AliHLTEventDoneData* ed );

class AliHLTEventDoneDataProcessor
    {
    public:

	enum TStatus { kOk, kDone, kDataSizeError, kUnknownCommand };

	typedef bool (*EventDoneDataCommandProcessFunc)( AliUInt32_t const * const cmd, unsigned wordCount );

	AliHLTEventDoneDataProcessor():
	    fData(NULL),fPos(0) {}
	AliHLTEventDoneDataProcessor( AliHLTEventDoneData const*data ):
	    fData(data),fPos(0) {}

	void StartProcess( AliHLTEventDoneData const*data )
		{
		fData = data;
		fPos = 0;
		}

	TStatus NextCommand( AliUInt32_t const *& cmd, unsigned& wordCount );

	bool Process( AliHLTEventDoneData *data, EventDoneDataCommandProcessFunc processFunc );

    protected:

	AliHLTEventDoneData const*fData;
	unsigned long fPos;

    };


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTEVENTDONEDATA_HPP_
