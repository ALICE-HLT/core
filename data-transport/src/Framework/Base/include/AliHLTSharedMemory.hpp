/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3SHAREDMEMORY_HPP_
#define _ALIL3SHAREDMEMORY_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliHLTShmID.h"
#include <psi.h>
#include <pthread.h>
#include <vector>

#if __GNUC__>=3
using namespace std;
#endif

namespace librorc {
class device;
class buffer;
}

class AliHLTSharedMemory
    {
    public:

	struct ShmStatus
	    {
		AliUInt32_t fRefCount;
		AliUInt64_t fPhysAddr;
		AliUInt64_t fSize;
		bool fLocked;
	    };

	typedef struct ShmStatus ShmStatus;

        // Constructor
        AliHLTSharedMemory();
	virtual ~AliHLTSharedMemory();

        // Allocate a shared memory segment of the given
        // size and return its ID. shmKey is an identifier
        // for the block. The ID will be -1 in case of an error.
	// If failIfExisting is set to true, it will be tested if a
	// shared memory segment with the given key already exists.
	// If that is the case the call will fail with -1 returned. 
	// In order to be able to detect this case size will be set
	// to zero in addition.
        virtual AliUInt32_t GetShm( AliHLTShmID& shmKey, unsigned long& size, bool failIfExisting = false );

        // Lock a shared memory segment and get a pointer
        // on it.
        virtual void* LockShm( AliUInt32_t shmID );
 
        // Unlock a previously locked memory segment.
        virtual bool UnlockShm( AliUInt32_t shmID );

        // Release a shared memory segment.
        virtual bool ReleaseShm( AliUInt32_t shmID );

	// Get status information about a shared memory segment
	virtual bool GetShmStatus( AliUInt32_t shmID, ShmStatus* status );

	// Get the physical and bus addresses corresponding to the given virtual address...
	static bool GetPhysicalAddress( const void* virtAddr, void** physAddr, void** busAddr ); 

	// This API is unsupported and used for a hack.
	// It should actually be considered at least deprecated, even better as not present at all.
	// Just pretend you have not seen this!
	bool GetPSIRegion( AliHLTShmID shmKey, tRegion& region );


#ifndef NOLIBRORC
        bool AllocateLibrorcBuffer(AliHLTShmID shmKey, size_t size,
                                   librorc::device **device,
                                   librorc::buffer **buffer);

        bool GetExistingLibrorcBuffer(AliHLTShmID shmKey,
                                      librorc::buffer **buffer);
#endif

    protected:

	void CleanupRegion( tRegion region );
	void CleanupSysV( AliUInt32_t id );

	struct ShmData
	    {
		AliUInt32_t fShmType;
		AliUInt64_t      fKeyID;
		tRegion fRegion;
#ifndef NOLIBRORC
                librorc::buffer *fLibrorcBuffer;
                librorc::device *fLibrorcDevice;
#endif
		AliUInt32_t fID;
		unsigned long fSize;
		const char*       fName;
		AliUInt32_t fKey;
		void*       fPtr;
	    };
	typedef struct ShmData ShmData;

	vector<ShmData> fShms;

	const char *fErrorName;

	pthread_mutex_t fMutex;

	AliUInt32_t fNextKey;

    private:

    };


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3SHAREDMEMORY_HPP_
