/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3EVENTDATAORIGIN_H_
#define _ALIL3EVENTDATAORIGIN_H_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"

#ifdef __cplusplus
extern "C" {
#endif


    union AliHLTEventDataOrigin
	{
	    AliUInt32_t     fID;
	    AliUInt8_t      fDescr[4];
	};

    typedef union AliHLTEventDataOrigin AliHLTEventDataOrigin;

#define UNKNOWN_DATAORIGIN           ((AliUInt32_t)'UNKN')
#define TPC_DATAORIGIN               ((AliUInt32_t)'TPC ')
#define DIMUON_DATAORIGIN            ((AliUInt32_t)'DIMU')
#define TRD_DATAORIGIN               ((AliUInt32_t)'TRD ')
#define HLT_DATAORIGIN               ((AliUInt32_t)'HLT ')
#define PRIV_DATAORIGIN              ((AliUInt32_t)'PRIV')

    //#define ALL_DATAORIGIN               (~(AliUInt32_t)0)
#define ALL_DATAORIGIN               ((AliUInt32_t)'***\0')
#define NONE_DATAORIGIN              ((AliUInt32_t)0)


#ifdef __cplusplus
}
#endif


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3EVENTDATAORIGIN_H_
