/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3SHMID_H_
#define _ALIL3SHMID_H_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"

#ifdef __cplusplus
extern "C" {
#endif 

enum AliHLTShmIDType { kInvalidShmType = 0, kBigPhysShmType = 1, kPhysMemShmType = 2, kSysVShmType = 3, kSysVShmPrivateType = 4, kLibrorcShmType = 5 };

struct AliHLTShmID
    {
	AliUInt32_t fShmType;
	union 
	    {
		AliUInt64_t      fID;
		AliUInt8_t       fDescr[8];
	    } fKey;
    };

typedef struct AliHLTShmID AliHLTShmID;


/***************************************
 * get librorc-specific buffer ID
 * mappings from AliHLTShmID.fKey.fID
 **************************************/
inline AliUInt32_t AliHLTShmIDGetLibrorcDeviceId( AliHLTShmID shmID ) {
    return ((shmID.fKey.fID >> 16) & 0x7fff); // bits [30:16]
}

inline AliUInt32_t AliHLTShmIDGetLibrorcBufferId( AliHLTShmID shmID ) {
    return ((shmID.fKey.fID & 0xffff) << 1); // bits [15:0], multiplied by two
}

inline AliUInt32_t AliHLTShmIDGetLibrorcOvermapFlag( AliHLTShmID shmID ) {
    // bit [31], active low = enabled when bit is not set explicitly
    return (((~shmID.fKey.fID) >> 31) & 1);
}

#ifdef __cplusplus
}


inline bool operator==( AliHLTShmID const & id0, AliHLTShmID const & id1 )
    {
    return id0.fShmType==id1.fShmType && id0.fKey.fID==id1.fKey.fID;
    }

inline bool operator!=( AliHLTShmID const & id0, AliHLTShmID const & id1 )
    {
    return id0.fShmType!=id1.fShmType || id0.fKey.fID!=id1.fKey.fID;
    }

#endif



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif /* _ALIL3SHMID_H_ */
