// XEmacs -*-C++-*-
/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3SUBEVENTDATADESCRIPTOR_H_
#define _ALIL3SUBEVENTDATADESCRIPTOR_H_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliHLTBlockHeader.h"
#include "AliEventID.h"
#include "AliHLTEventDataType.h"
#include "AliHLTShmID.h"
#include "AliHLTEventDataOrigin.h"
#include "AliHLTBlockAttributes.h"


#ifdef __cplusplus
extern "C" {
#endif

    const int kAliHLTSEDBDByteOrderAttributeIndex        = kAliHLTBlockDByteOrderAttributeIndex;
    const int kAliHLTSEDBDAlignmentAttributeIndexStart   = kAliHLTBlockDAlignmentAttributeIndexStart;
    const int kAliHLTSEDBD64BitAlignmentAttributeIndex   = kAliHLTBlockDAlignmentAttributeIndexStart;
    const int kAliHLTSEDBD32BitAlignmentAttributeIndex   = kAliHLTBlockDAlignmentAttributeIndexStart+1;
    const int kAliHLTSEDBD16BitAlignmentAttributeIndex   = kAliHLTBlockDAlignmentAttributeIndexStart+2;
    const int kAliHLTSEDBD8BitAlignmentAttributeIndex    = kAliHLTBlockDAlignmentAttributeIndexStart+3;
    const int kAliHLTSEDBDFloatAlignmentAttributeIndex   = kAliHLTBlockDAlignmentAttributeIndexStart+4;
    const int kAliHLTSEDBDDoubleAlignmentAttributeIndex  = kAliHLTBlockDAlignmentAttributeIndexStart+5;
    const int kAliHLTSEDBDLastAlignmentAttributeIndex    = kAliHLTBlockDDoubleAlignmentAttributeIndex;
    const int kAliHLTSEDBDAlignmentAttributeCount        = kAliHLTBlockDLastAlignmentAttributeIndex-kAliHLTBlockDAlignmentAttributeIndexStart+1;
    const int kAliHLTSEDBDAttributeCount                 = kAliHLTBlockDAttributeCount;

struct AliHLTSubEventDataBlockDescriptor
    {
	AliHLTShmID           fShmID;
	AliUInt64_t           fBlockSize;
	AliUInt64_t           fBlockOffset;
	AliHLTNodeID_t        fProducerNode;
	AliHLTEventDataType   fDataType;
	AliHLTEventDataOrigin fDataOrigin;
	AliUInt32_t           fDataSpecification;
	AliUInt32_t           fStatusFlags;
	//AliUInt32_t           fByteOrder;
	AliUInt8_t            fAttributes[kAliHLTSEDBDAttributeCount]; // Byte order and alignment
    };

typedef struct AliHLTSubEventDataBlockDescriptor AliHLTSubEventDataBlockDescriptor;



    /*
     * version 2 of this structure adds the three EventBirth members.
     * version 3 of this structure adds the byte order member for the 
     *           AliHLTSubEventDataBlockDescriptor elements
     * version 4 of this structure adds the data origin and data
     *           specification fields to AliHLTSubEventDataBlockDescriptor
     * version 5 of this structure removes the byte order field and adds
     *           the attributes 
     */

struct AliHLTSubEventDataDescriptor
    {
	AliHLTBlockHeader         fHeader;
	AliEventID_t             fEventID;
	AliUInt32_t              fEventBirth_s;
	AliUInt32_t              fEventBirth_us;
	AliUInt32_t              fOldestEventBirth_s;
	AliHLTEventDataType       fDataType;
	AliUInt64_t              fStatusFlags;
	AliUInt32_t              fDataBlockCount;
	AliHLTSubEventDataBlockDescriptor   fDataBlocks[0];
    };

#define ALIHLTSUBEVENTDATADESCRIPTOR_DATA_CORRUPT    0x00000001
#define ALIHLTSUBEVENTDATADESCRIPTOR_PASSTHROUGH     0x00000002
#define ALIHLTSUBEVENTDATADESCRIPTOR_FULLPASSTHROUGH 0x00000004
#define ALIHLTSUBEVENTDATADESCRIPTOR_MONITOR         0x00000008
#define ALIHLTSUBEVENTDATADESCRIPTOR_SPLITPART1      0x00000010
#define ALIHLTSUBEVENTDATADESCRIPTOR_SPLITPART2      0x00000020
#define ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST       0x80000000
#define ALIHLTSUBEVENTDATADESCRIPTOR_BARRIER         0x40000000

typedef struct AliHLTSubEventDataDescriptor AliHLTSubEventDataDescriptor;


#define ALIL3SUBEVENTDATADESCRIPTOR_TYPE ((AliUInt32_t)'SEDD')




#ifdef __cplusplus
}
#endif

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif /* _ALIL3SUBEVENTDATADESCRIPTOR_H_ */
