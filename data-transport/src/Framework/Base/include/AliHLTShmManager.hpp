/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3SHMMANAGER_HPP_
#define _ALIL3SHMMANAGER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliHLTShmID.h"
#include "AliHLTSharedMemory.hpp"
#include <pthread.h>
#include <vector>

class AliHLTShmManager
    {
    public:
	
	AliHLTShmManager( AliUInt32_t maxNoUseCount );
	~AliHLTShmManager();

	AliUInt8_t* GetShm( AliHLTShmID const& shmKey );
	AliUInt8_t* GetShm( AliHLTShmID& shmKey );
	void ReleaseShm( AliHLTShmID shmKey );

	AliHLTSharedMemory* GetSharedMemoryObject()
		{
		return &fShm;
		}

    protected:

	bool LockShm( AliHLTShmID& shmKey, void*& ptr, AliUInt32_t& shmId );
	void UnlockShm( AliUInt32_t shmID );

	struct TShmData
	    {
		AliHLTShmID fShmKey;
		AliUInt32_t fRefCount;
		AliUInt32_t fNoUseCount;
		AliUInt32_t fShmID;
		void* fPtr;
	    };

	vector<TShmData> fShms;
	pthread_mutex_t fShmsMutex;

	AliUInt32_t fMaxShmNoUseCount;

	AliHLTSharedMemory fShm;
		
    private:
    };

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3SHMMANAGER_HPP_
