#ifndef _ALIHLTEVENTSTORAGEINTERFACE_HPP_
#define _ALIHLTEVENTSTORAGEINTERFACE_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTSubEventDescriptor.hpp"
#include "AliHLTEventTriggerStruct.h"
#include "AliHLTLog.hpp"
#include <vector>
#include <errno.h>

/**
 * A abstract interface for classes 
 */
class AliHLTEventStorageInterface
    {
    public:

	class EventLocationDescriptor
	    {
	    public:
		// Opaque holder for event location data.
		EventLocationDescriptor() {};
		virtual ~EventLocationDescriptor() {};
		
		virtual const char* GetStorageTypeName() const = 0;

	    protected:


		    friend class AliHLTEventStorageInterface;

	    };

	struct EventStorageMetaData
	    {
		AliUInt32_t              fEventWritten_s;
		AliUInt32_t              fEventWritten_us;
		AliHLTNodeID_t           fEventWriteNode;
		AliUInt32_t              fEventWriteNodeByteOrder;
	    };

	AliHLTEventStorageInterface()
		{
		};
	virtual ~AliHLTEventStorageInterface() {};

	virtual const char* GetStorageTypeName() const = 0;

	virtual int OpenStorage( const char* storageName, bool write = false ) = 0;

	virtual int CloseStorage() = 0;

	virtual void DumpEventLocation( AliHLTLog::TLogLevel lvl, const char* keyword, const EventLocationDescriptor* eld ) = 0;

	virtual int ResetEventLocation() = 0;
	virtual int GetNextEventLocation( EventLocationDescriptor* eld, AliUInt64_t eventSkipCount = 0 ) = 0;
	virtual int FindEventLocation( AliEventID_t eventID, EventLocationDescriptor* eld ) = 0;
	virtual EventLocationDescriptor* AllocEventLocation() = 0;
 	virtual void FreeEventLocation( EventLocationDescriptor* eld ) = 0;
	virtual int GetEventStorageMetaData( const EventLocationDescriptor* eld, EventStorageMetaData* esmd ) = 0;
	virtual int GetSubEventDataDescriptor( const EventLocationDescriptor* eld, AliHLTSubEventDataDescriptor* sedd ) = 0;
	virtual int GetEventBlockData( const EventLocationDescriptor* eld, std::vector<AliHLTSubEventDescriptor::BlockData>& dataBlocks ) = 0;
	virtual int GetEventData( const EventLocationDescriptor* eld, const std::vector<AliHLTSubEventDescriptor::BlockData>& dataBlocks ) = 0;
	virtual int GetEventTriggerSize( const EventLocationDescriptor* eld, unsigned long& size ) = 0;
	virtual int GetEventTriggerData( const EventLocationDescriptor* eld, AliHLTEventTriggerStruct* ets ) = 0;

	int ReadEventStorageMetaData( const EventLocationDescriptor* eld, EventStorageMetaData*& esmd )
		{
		esmd = new EventStorageMetaData;
		if ( !esmd )
		    return ENOMEM;
		return GetEventStorageMetaData( eld, esmd );
		}
	int ReleaseEventStorageMetaData( EventStorageMetaData* esmd )
		{
		if ( esmd )
		    delete esmd;
		return 0;
		}
	int ReadSubEventDataDescriptor( const EventLocationDescriptor* eld, AliHLTSubEventDataDescriptor*& sedd )
		{
		sedd = new AliHLTSubEventDataDescriptor;
		if ( !sedd )
		    return ENOMEM;
		return GetSubEventDataDescriptor( eld, sedd );
		}
	int ReleaseSubEventDataDescriptor( AliHLTSubEventDataDescriptor* sedd )
		{
		if ( sedd )
		    delete sedd;
		return 0;
		}
	int ReadEventData( const EventLocationDescriptor* eld, std::vector<AliHLTSubEventDescriptor::BlockData>& dataBlocks )
		{
		int ret;
		ret = GetEventBlockData( eld, dataBlocks );
		if ( ret )
		    return ret;
		std::vector<AliHLTSubEventDescriptor::BlockData>::iterator iter = dataBlocks.begin(), end = dataBlocks.end();
		while ( iter != end )
		    {
		    iter->fData = new AliUInt8_t[ iter->fSize ];
		    if ( !iter->fData )
			return ENOMEM;
		    iter++;
		    }
		return GetEventData( eld, dataBlocks );
		}
	int ReleaseEventData( std::vector<AliHLTSubEventDescriptor::BlockData>& dataBlocks )
		{
		std::vector<AliHLTSubEventDescriptor::BlockData>::iterator iter = dataBlocks.begin(), end = dataBlocks.end();
		while ( iter != end )
		    {
		    if ( iter->fData )
			delete [] iter->fData;
		    iter++;
		    }
		return 0;
		}
	int ReadEventTrigger( const EventLocationDescriptor* eld, AliHLTEventTriggerStruct*& ets )
		{
		unsigned long size;
		int ret;
		ret = GetEventTriggerSize( eld, size );
		if ( ret )
		    return ret;
		ets = reinterpret_cast<AliHLTEventTriggerStruct*>( new AliUInt8_t[ size ] );
		if ( !ets )
		    return ENOMEM;
		return GetEventTriggerData( eld, ets );
		}
	int ReleaseEventTrigger( AliHLTEventTriggerStruct*& ets )
		{
		if ( ets )
		    delete [] ets;
		return 0;
		}

	virtual int WriteEvent( const AliHLTSubEventDataDescriptor* sedd, unsigned long blockCnt, 
				AliHLTSubEventDescriptor::BlockData* blocks, 
				const AliHLTEventTriggerStruct* ets, EventLocationDescriptor* eld = NULL ) = 0;

    protected:
    private:
    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTEVENTSTORAGEINTERFACE_HPP_
