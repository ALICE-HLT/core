// XEmacs -*-C++-*-
/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIEVENTID_H_
#define _ALIEVENTID_H_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "MLUCLog.hpp"

//#define OLD
#ifdef OLD

#ifdef __cplusplus
extern "C" {
#endif
    
    
    
    typedef AliUInt64_t AliEventID_t;
    
#define BUNCHESFROMEVTID( evtID ) (evtID & 0xFFF)
#define ORBITSFROMEVTID( evtID ) ((evtID >> 14) & 0x3FFF)
#define SECSFROMEVTID( evtID ) (evtID >> 32 )
    
    
#else

    const AliUInt32_t kAliEventTypeUnknown = ~(AliUInt32_t)0;
    const AliUInt32_t kAliEventTypeStartOfRun=1;
    const AliUInt32_t kAliEventTypeData=2;
    const AliUInt32_t kAliEventTypeSync=3;
    const AliUInt32_t kAliEventTypeEndOfRun=4;
    const AliUInt32_t kAliEventTypeCorruptID=8;
    const AliUInt32_t kAliEventTypeCalibration=16;
#if 0
    const AliUInt32_t kAliEventTypeDataSubType_Physics_Reserved=18;
    const AliUInt32_t kAliEventTypeDataSubType_TPCLaser_Reserved=19;
    const AliUInt32_t kAliEventTypeDataSubType_Calibration_Reserved=20;
    const AliUInt32_t kAliEventTypeDataSubType_Software_Reserved=21;
#endif
    const AliUInt32_t kAliEventTypeDataReplay=32;
    const AliUInt32_t kAliEventTypeTick=33;
    const AliUInt32_t kAliEventTypeReconfigure=34;
    const AliUInt32_t kAliEventTypeDCSUpdate=35;
    const AliUInt32_t kAliEventTypeMax=35;
    // If you add something here remember to also update kAliEventTypeStrings in AliEventID.cpp!

    extern const char* kAliEventTypeStrings[];
    
    struct AliEventID_t
	{
	public:
	    
	    AliUInt32_t fType;
	    AliUInt64_t fNr;
	    AliEventID_t( AliUInt32_t type, AliUInt64_t nr ) { fType = type; fNr = nr; }
	    //AliEventID_t( AliUInt64_t nr ) { fType = kAliEventTypeData; fNr = nr; }
	    AliEventID_t() { fType = kAliEventTypeUnknown; fNr = ~(AliUInt64_t)0; }
	    AliEventID_t& operator=( const AliUInt64_t& nr ) { fNr = nr; if ( fType==kAliEventTypeUnknown ) fType=kAliEventTypeData; return *this; }
	    AliEventID_t& operator++() { fNr++; return *this; }
	    AliEventID_t& operator--() { fNr--; return *this; }
	    AliEventID_t& operator<<=( unsigned long n )  { fNr <<= n ; return *this; }
	    AliEventID_t& operator>>=( unsigned long n )  { fNr >>= n ; return *this; }
	    AliEventID_t& operator|=( AliUInt64_t n )  { fNr |= n ; return *this; }
	    AliEventID_t& operator&=( AliUInt64_t n )  { fNr &= n ; return *this; }
	    AliEventID_t& operator^=( AliUInt64_t n )  { fNr ^= n ; return *this; }
	    AliEventID_t& operator+=( AliUInt64_t n )  { fNr += n ; return *this; }
	    AliEventID_t& operator-=( AliUInt64_t n )  { fNr -= n ; return *this; }
	    AliEventID_t& operator*=( AliUInt64_t n )  { fNr *= n ; return *this; }
	    AliEventID_t& operator/=( AliUInt64_t n )  { fNr /= n ; return *this; }
	    AliEventID_t& operator%=( AliUInt64_t n )  { fNr %= n ; return *this; }
	    operator AliUInt64_t() const { return fNr; }
	    AliUInt64_t GetBunchCrossing() const { return fNr & 0xFFFULL; }
	    AliUInt64_t GetOrbit() const { return (fNr >> 12) & 0xFFFFFFULL; }
	    AliUInt64_t GetPeriod() const { return (fNr >> 36) & 0xFFFFFFFULL; }
	};

#ifdef __cplusplus
    inline bool operator== ( const AliEventID_t& id1, const AliEventID_t& id2 ) { return (id1.fType==id2.fType) && (id1.fNr==id2.fNr); }
    inline bool operator!= ( const AliEventID_t& id1, const AliEventID_t& id2 ) { return (id1.fType!=id2.fType) || (id1.fNr!=id2.fNr); }

    inline AliEventID_t operator+ ( const AliEventID_t& id, const unsigned long long& nr ) { return AliEventID_t( id.fType, id.fNr+nr ); }
    inline AliEventID_t operator+ ( const AliEventID_t& id, const unsigned int& nr ) { return AliEventID_t( id.fType, id.fNr+nr ); }
    inline AliEventID_t operator+ ( const AliEventID_t& id, const unsigned long& nr ) { return AliEventID_t( id.fType, id.fNr+nr ); }
    inline AliEventID_t operator+ ( const AliEventID_t& id, const long long& nr ) { return AliEventID_t( id.fType, id.fNr+nr ); }
    inline AliEventID_t operator+ ( const AliEventID_t& id, const int& nr ) { return AliEventID_t( id.fType, id.fNr+nr ); }
    inline AliEventID_t operator+ ( const AliEventID_t& id, const long& nr ) { return AliEventID_t( id.fType, id.fNr+nr ); }
    inline AliEventID_t operator+ ( const AliEventID_t& id, const AliEventID_t& id2 ) { return AliEventID_t( id.fType, id.fNr+id2.fNr ); }

    inline AliEventID_t operator- ( const AliEventID_t& id, const unsigned long long& nr ) { return AliEventID_t( id.fType, id.fNr-nr ); }
    inline AliEventID_t operator- ( const AliEventID_t& id, const unsigned int& nr ) { return AliEventID_t( id.fType, id.fNr-nr ); }
    inline AliEventID_t operator- ( const AliEventID_t& id, const unsigned long& nr ) { return AliEventID_t( id.fType, id.fNr-nr ); }
    inline AliEventID_t operator- ( const AliEventID_t& id, const long long& nr ) { return AliEventID_t( id.fType, id.fNr-nr ); }
    inline AliEventID_t operator- ( const AliEventID_t& id, const int& nr ) { return AliEventID_t( id.fType, id.fNr-nr ); }
    inline AliEventID_t operator- ( const AliEventID_t& id, const long& nr ) { return AliEventID_t( id.fType, id.fNr-nr ); }
    inline AliEventID_t operator- ( const AliEventID_t& id, const AliEventID_t& id2 ) { return AliEventID_t( id.fType, id.fNr-id2.fNr ); }

    inline AliEventID_t operator<< ( const AliEventID_t& id, const unsigned long long& nr ) { return AliEventID_t( id.fType, id.fNr << nr ); }
    inline AliEventID_t operator<< ( const AliEventID_t& id, const unsigned long& nr ) { return AliEventID_t( id.fType, id.fNr << nr ); }
    inline AliEventID_t operator<< ( const AliEventID_t& id, const unsigned int& nr ) { return AliEventID_t( id.fType, id.fNr << nr ); }
    inline AliEventID_t operator<< ( const AliEventID_t& id, const long long& nr ) { return AliEventID_t( id.fType, id.fNr << nr ); }
    inline AliEventID_t operator<< ( const AliEventID_t& id, const long& nr ) { return AliEventID_t( id.fType, id.fNr << nr ); }
    inline AliEventID_t operator<< ( const AliEventID_t& id, const int& nr ) { return AliEventID_t( id.fType, id.fNr << nr ); }

    inline AliEventID_t operator>> ( const AliEventID_t& id, const unsigned long long& nr ) { return AliEventID_t( id.fType, id.fNr >> nr ); }
    inline AliEventID_t operator>> ( const AliEventID_t& id, const unsigned long& nr ) { return AliEventID_t( id.fType, id.fNr >> nr ); }
    inline AliEventID_t operator>> ( const AliEventID_t& id, const unsigned int& nr ) { return AliEventID_t( id.fType, id.fNr >> nr ); }
    inline AliEventID_t operator>> ( const AliEventID_t& id, const long long& nr ) { return AliEventID_t( id.fType, id.fNr >> nr ); }
    inline AliEventID_t operator>> ( const AliEventID_t& id, const long& nr ) { return AliEventID_t( id.fType, id.fNr >> nr ); }
    inline AliEventID_t operator>> ( const AliEventID_t& id, const int& nr ) { return AliEventID_t( id.fType, id.fNr >> nr ); }

    inline AliEventID_t operator* ( const AliEventID_t& id, const unsigned long long& nr ) { return AliEventID_t( id.fType, id.fNr * nr ); }
    inline AliEventID_t operator* ( const AliEventID_t& id, const unsigned long& nr ) { return AliEventID_t( id.fType, id.fNr * nr ); }
    inline AliEventID_t operator* ( const AliEventID_t& id, const unsigned int& nr ) { return AliEventID_t( id.fType, id.fNr * nr ); }
    inline AliEventID_t operator* ( const AliEventID_t& id, const long long& nr ) { return AliEventID_t( id.fType, id.fNr * nr ); }
    inline AliEventID_t operator* ( const AliEventID_t& id, const long& nr ) { return AliEventID_t( id.fType, id.fNr * nr ); }
    inline AliEventID_t operator* ( const AliEventID_t& id, const int& nr ) { return AliEventID_t( id.fType, id.fNr * nr ); }

    inline AliEventID_t operator/ ( const AliEventID_t& id, const unsigned long long& nr ) { return AliEventID_t( id.fType, id.fNr / nr ); }
    inline AliEventID_t operator/ ( const AliEventID_t& id, const unsigned long& nr ) { return AliEventID_t( id.fType, id.fNr / nr ); }
    inline AliEventID_t operator/ ( const AliEventID_t& id, const unsigned int& nr ) { return AliEventID_t( id.fType, id.fNr / nr ); }
    inline AliEventID_t operator/ ( const AliEventID_t& id, const long long& nr ) { return AliEventID_t( id.fType, id.fNr / nr ); }
    inline AliEventID_t operator/ ( const AliEventID_t& id, const long& nr ) { return AliEventID_t( id.fType, id.fNr / nr ); }
    inline AliEventID_t operator/ ( const AliEventID_t& id, const int& nr ) { return AliEventID_t( id.fType, id.fNr / nr ); }

    inline AliEventID_t operator% ( const AliEventID_t& id, const unsigned long long& nr ) { return AliEventID_t( id.fType, id.fNr % nr ); }
    inline AliEventID_t operator% ( const AliEventID_t& id, const unsigned long& nr ) { return AliEventID_t( id.fType, id.fNr % nr ); }
    inline AliEventID_t operator% ( const AliEventID_t& id, const unsigned int& nr ) { return AliEventID_t( id.fType, id.fNr % nr ); }
    inline AliEventID_t operator% ( const AliEventID_t& id, const long long& nr ) { return AliEventID_t( id.fType, id.fNr % nr ); }
    inline AliEventID_t operator% ( const AliEventID_t& id, const long& nr ) { return AliEventID_t( id.fType, id.fNr % nr ); }
    inline AliEventID_t operator% ( const AliEventID_t& id, const int& nr ) { return AliEventID_t( id.fType, id.fNr % nr ); }

    inline AliEventID_t operator& ( const AliEventID_t& id, const unsigned long long& nr ) { return AliEventID_t( id.fType, id.fNr & nr ); }
    inline AliEventID_t operator& ( const AliEventID_t& id, const unsigned long& nr ) { return AliEventID_t( id.fType, id.fNr & nr ); }
    inline AliEventID_t operator& ( const AliEventID_t& id, const unsigned int& nr ) { return AliEventID_t( id.fType, id.fNr & nr ); }
    inline AliEventID_t operator& ( const AliEventID_t& id, const long long& nr ) { return AliEventID_t( id.fType, id.fNr & nr ); }
    inline AliEventID_t operator& ( const AliEventID_t& id, const long& nr ) { return AliEventID_t( id.fType, id.fNr & nr ); }
    inline AliEventID_t operator& ( const AliEventID_t& id, const int& nr ) { return AliEventID_t( id.fType, id.fNr & nr ); }

    inline AliEventID_t operator| ( const AliEventID_t& id, const unsigned long long& nr ) { return AliEventID_t( id.fType, id.fNr | nr ); }
    inline AliEventID_t operator| ( const AliEventID_t& id, const unsigned long& nr ) { return AliEventID_t( id.fType, id.fNr | nr ); }
    inline AliEventID_t operator| ( const AliEventID_t& id, const unsigned int& nr ) { return AliEventID_t( id.fType, id.fNr | nr ); }
    inline AliEventID_t operator| ( const AliEventID_t& id, const long long& nr ) { return AliEventID_t( id.fType, id.fNr | nr ); }
    inline AliEventID_t operator| ( const AliEventID_t& id, const long& nr ) { return AliEventID_t( id.fType, id.fNr | nr ); }
    inline AliEventID_t operator| ( const AliEventID_t& id, const int& nr ) { return AliEventID_t( id.fType, id.fNr | nr ); }

    inline AliEventID_t operator^ ( const AliEventID_t& id, const unsigned long long& nr ) { return AliEventID_t( id.fType, id.fNr ^ nr ); }
    inline AliEventID_t operator^ ( const AliEventID_t& id, const unsigned long& nr ) { return AliEventID_t( id.fType, id.fNr ^ nr ); }
    inline AliEventID_t operator^ ( const AliEventID_t& id, const unsigned int& nr ) { return AliEventID_t( id.fType, id.fNr ^ nr ); }
    inline AliEventID_t operator^ ( const AliEventID_t& id, const long long& nr ) { return AliEventID_t( id.fType, id.fNr ^ nr ); }
    inline AliEventID_t operator^ ( const AliEventID_t& id, const long& nr ) { return AliEventID_t( id.fType, id.fNr ^ nr ); }
    inline AliEventID_t operator^ ( const AliEventID_t& id, const int& nr ) { return AliEventID_t( id.fType, id.fNr ^ nr ); }

    inline AliEventID_t operator! ( const AliEventID_t& id ) { return AliEventID_t( id.fType, !id.fNr ); }

    inline AliEventID_t operator~ ( const AliEventID_t& id ) { return AliEventID_t( id.fType, ~id.fNr ); }

    inline MLUCLog::TLogMsg& operator<<( MLUCLog::TLogMsg& log, const AliEventID_t& id )
	{
	const char* type = ( id.fType <= kAliEventTypeMax ? kAliEventTypeStrings[id.fType] : "INVALID" );
	log << id.fNr << "/" << type;
	return log;
	}

#endif

typedef struct AliEventID_t AliEventID_t;

#if 0
#define BUNCHESFROMEVTID( evtID ) (evtID.fNr & 0xFFF)
#define ORBITSFROMEVTID( evtID ) ((evtID.fNr >> 14) & 0x3FFF)
#define SECSFROMEVTID( evtID ) (evtID.fNr >> 32 )
#endif

#endif

#undef OLD

#ifdef OLD
#ifdef __cplusplus
}
#endif
#endif

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIEVENTID_H_
