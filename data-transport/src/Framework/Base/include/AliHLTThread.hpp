/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _AliHLTTHREAD_HPP_
#define _AliHLTTHREAD_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#if 1

#include "MLUCThread.hpp"

typedef MLUCThread AliHLTThread;

#else

class AliHLTThread;

#include "AliHLTThreadFIFO.hpp"

/**
 * A class for encapsulating thread handling.
 * The thread class has an abstract method Run, which must be overridden 
 * by the user to actually do the real work.
 * Furthermore, each thread object has an associated AliHLTThreadFIFO object, 
 * via which messages can be send to it from the outside world. A thread thus
 * can be basically an independently runnning message handler.
 *
 * @short A class for encapsulating thread handling.
 * @author $Author$
 * @version $Id$
 */
class AliHLTThread
	{
	public:

		/**
		 * An enum for the various states which can be returned by this class' methods.
		 */
		enum TState { kFailed = 0, kStarted, kAborted, kRestarted, kJoined };
	
		/**
		 * Default Constructor. Does not start the thread.
		 * Allocates an ordinary and an emergency buffer in the FIFO with size 1024 and 256 respectively.
		 */
		AliHLTThread();

		/**
		 * Constructor. Does not start the thread.
		 *
		 * @param bufferSize The size of the (ordinary) message buffer in the FIFO object.
		 * @param emergencySize The size of the emergency (high priority) buffer in the FIFO object.
		 */
		AliHLTThread( AliUInt32_t bufferSize, AliUInt32_t emergencySize );

		/**
		 * Destructor.
		 */
		virtual ~AliHLTThread();

		/**
		 * Start this thread. This will result in this object's Run method
		 * being called.
		 *
		 * @return The status value of the thread, either kFailed ot kStarted
		 */
		virtual TState Start();

		/**
		 * Stop this thread. Aborts the execution of this thread.
		 *
		 * @return The status value of the abort process, either kFailed or kAborted
		 */
		virtual TState Abort();

		/**
		 * Does a quick restart of this thread, without actually really restarting it...
		 *
		 * @return The status value of the quick restart process, either kFailed or kRestarted
		 */
		virtual TState QuickRestart();

		/**
		 * Join with this thread, that is wait, until this thread has executed.
		 *
		 * @return The status value of the join process, either kFailed or kJoined
		 */
		virtual TState Join();

		/**
		 * The FIFO object used for sending messages to this thread. 
		 * Only the Write* methods should be used from outside the parent
		 * thread object.
		 */
		AliHLTThreadFIFO fFifo;

	protected:


		virtual void Run() = 0;

		static void* ThreadStart( void* obj );

		pthread_t fThread;
		pthread_attr_t fThreadAttrs;

	private:
	};

#endif



template<class T>
class AliHLTObjectThread: public AliHLTThread
    {
    public:
	
	AliHLTObjectThread( T* object, void (T::*method)( void ) ):
	    fObject( object ), fMethod( method )
		{
		fHasQuitted = true;
		}

	bool HasQuitted()
		{
		return fHasQuitted;
		}

    protected:

	T* fObject;
	void (T::*fMethod)(void);
	bool fHasQuitted;

	virtual void Run()
		{
		fHasQuitted = false;
		(fObject->*fMethod)();
		fHasQuitted = true;
		}
    };


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#endif // _AliHLTTHREAD_HPP_
