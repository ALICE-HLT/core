/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3TYPES_H_
#define _ALIL3TYPES_H_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#include <limits.h>

#ifdef USE_MPI
#include "mpi.h"
#endif // USE_MPI


#ifdef __cplusplus
extern "C" {
#endif


typedef unsigned char AliUInt8_t;



#if USHRT_MAX==65535
typedef unsigned short AliUInt16_t;
#else // USHRT_MAX==65535

#if UINT_MAX==65535
typedef unsigned int AliUInt16_t;
#else // UINT_MAX==65535

#if ULONG_MAX==65535l
typedef unsigned long AliUInt16_t;
#else // ULONG_MAX==65535l

#error Could not typedef AliUInt16_t

#endif // ULONG_MAX==65535l

#endif // UINT_MAX==65535


#endif // USHRT_MAX==65535

#if USHRT_MAX==4294967295
typedef unsigned short AliUInt32_t;
#else // USHRT_MAX==4294967295

#if UINT_MAX==4294967295
typedef unsigned int AliUInt32_t;
#else // UINT_MAX==4294967295

#if ULONG_MAX==4294967295l
typedef unsigned long AliUInt32_t;
#else // ULONG_MAX==4294967295l

#error Could not typedef AliUInt32_t

#endif // ULONG_MAX==4294967295l

#endif // UINT_MAX==4294967295

#endif // USHRT_MAX==4294967295

#if ULONG_MAX==18446744073709551615UL
typedef unsigned long AliUInt64_t;
#else // ULONG_MAX==18446744073709551615UL

#if defined __GNUC__
typedef unsigned long long AliUInt64_t;
#else // defined __GNUC__

#error Could not typedef AliUInt64_t

#endif // defined __GNUC__

#endif // ULONG_MAX==18446744073709551615UL


typedef AliUInt32_t AliHLTNodeID_t;


/* #ifdef USE_MPI */
/* typedef int TProcessID; */
/* #endif */
/* #ifdef USE_SOCKETS */
/* typedef struct sockaddr* TProcessID; */
/* #endif */
/* const TProcessID kNoProcess = (TProcessID)-1; */


/* #ifdef USE_MPI */
/* typedef MPI_Group TGroupID; */
/* #endif // USE_MPI */
/* #ifdef USE_SOCKETS */
/* typedef AliUInt32_t TGroupID; */
/* #endif */

/* const TGroupID kNoGroup = (TGroupID)-1; */

/* #ifdef USE_MPI */
/* const TGroupID kWorldGroup = MPI_COMM_WORLD; */
/* #endif  */
/* #ifdef USE_SOCKETS */
/* const TGroupID kWorldGroup = 0; */
/* #endif  */

/* #ifdef USE_MPI */
/* typedef int TCommID; */
/* #endif */
/* #ifdef USE_SOCKETS */
/* typedef int TCommID; */
/* #endif */
/* const TCommID kNoComID = (TCommID)-1; */

/* typedef AliUInt32_t TMsgType; */
/* const TMsgType kMsgNOP = (TMsgType)0; */



#ifdef __cplusplus
}
#endif



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3TYPES_H_
