/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3SUBEVENTDESCRIPTOR_HPP_
#define _ALIL3SUBEVENTDESCRIPTOR_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliHLTSubEventDataDescriptor.h"
#include <vector>

#if __GNUC__>=3
using namespace std;
#endif


class AliHLTShmManager;
struct AliHLTEventTriggerStruct;

class AliHLTSubEventDescriptor
    {
    public:

	
	struct BlockData
	    {
		AliUInt8_t*           fData;
		AliHLTShmID           fShmKey;
		AliUInt64_t           fOffset;
		AliUInt64_t           fSize;
		AliHLTNodeID_t        fProducerNode;
		AliHLTEventDataType   fDataType;
		AliHLTEventDataOrigin fDataOrigin;
		AliUInt32_t           fDataSpecification;
		AliUInt32_t           fStatusFlags;
		//AliUInt32_t           fByteOrder;
		AliUInt8_t            fAttributes[kAliHLTSEDBDAttributeCount]; // Byte order and alignment
	    };


	AliHLTSubEventDescriptor( const AliHLTSubEventDataDescriptor& data );
	AliHLTSubEventDescriptor( const AliHLTSubEventDataDescriptor* data );
        AliHLTSubEventDescriptor();
	~AliHLTSubEventDescriptor();

	void SetDescriptorData( const AliHLTSubEventDataDescriptor& data );
	void SetDescriptorData( const AliHLTSubEventDataDescriptor* data );

        AliEventID_t GetEventID() const;
        AliUInt32_t GetBlockCnt() const;
        AliUInt64_t GetEventStatusFlag() const;
	AliHLTEventDataType GetDataType() const;
        bool GetBlock( AliUInt32_t blockNdx, AliUInt64_t& offset, 
		       AliUInt64_t& size, AliHLTNodeID_t& producerNode, 
		       AliHLTEventDataType& dataType, AliHLTEventDataOrigin& dataOrigin,
		       AliUInt32_t& dataSpecification, AliUInt32_t& statusFlags, 
		       AliUInt8_t* attributes );

	AliUInt32_t CalcDataDescriptorSize( AliUInt32_t blockCount );

	// the dataBlocks vector is not cleared in this function.
	void Dereference( AliHLTShmManager* shmMan, vector<BlockData>& dataBlocks ) const;

	static const char* GetTypeForAlignmentIndex( int index );

	static void FillBlockAttributes( AliUInt8_t attributes[8] );
	static void FillUnspecifiedBlockAttributes( AliUInt8_t attributes[8] );
	static AliUInt8_t GetBlockAttribute( unsigned attributeIndex );

	static void Copy( BlockData const& bd, AliHLTSubEventDataBlockDescriptor& sedbd )
		{
		sedbd.fShmID.fShmType = bd.fShmKey.fShmType;
		sedbd.fShmID.fKey.fID = bd.fShmKey.fKey.fID;
		sedbd.fBlockOffset = bd.fOffset;
		sedbd.fBlockSize = bd.fSize;
		sedbd.fProducerNode = bd.fProducerNode;
		sedbd.fDataType = bd.fDataType;
		sedbd.fDataOrigin = bd.fDataOrigin;
		sedbd.fDataSpecification = bd.fDataSpecification;
		sedbd.fStatusFlags = bd.fStatusFlags;
		for ( int n = 0; n < kAliHLTSEDBDAttributeCount; n++ )
		    sedbd.fAttributes[n] = bd.fAttributes[n];
		}

	static void Copy( AliHLTSubEventDataBlockDescriptor const& sedbd, BlockData& bd )
		{
		bd.fShmKey.fShmType = sedbd.fShmID.fShmType;
		bd.fShmKey.fKey.fID = sedbd.fShmID.fKey.fID;
		bd.fOffset = sedbd.fBlockOffset;
		bd.fData = NULL;
		bd.fSize = sedbd.fBlockSize;
		bd.fProducerNode = sedbd.fProducerNode;
		bd.fDataType = sedbd.fDataType;
		bd.fDataOrigin = sedbd.fDataOrigin;
		bd.fDataSpecification = sedbd.fDataSpecification;
		bd.fStatusFlags = sedbd.fStatusFlags;
		for ( int n = 0; n < kAliHLTSEDBDAttributeCount; n++ )
		    bd.fAttributes[n] = sedbd.fAttributes[n];
		}

    protected:

	void Dereference( AliHLTShmManager* shmMan, const AliHLTSubEventDataDescriptor *sedd, vector<BlockData>& dataBlocks ) const;

	const AliHLTSubEventDataDescriptor *fData;

    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
#endif // _ALIL3SUBEVENTDESCRIPTOR_HPP_
