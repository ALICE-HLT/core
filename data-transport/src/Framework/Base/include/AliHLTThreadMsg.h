/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _AliHLTTHREADMSG_H_
#define _AliHLTTHREADMSG_H_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#include "AliHLTTypes.h"

extern "C" {

struct AliHLTThreadMsg
	{

/* 		enum TType { kNoMessage=0, kEnd,  */
/* 			     //kData, kGetProcessName, kProcessName, */
/* 			     kSetDataAddress, kSetStatusAddress, kWriteToAddress, */
/* 			     kLastMessage*//* No message types after this one */ /*}; */

		/**
		 * Size of the message in bytes!
		 */
		AliUInt32_t fLength;
		//TType fType;
		AliUInt32_t fType;
		AliUInt8_t fData[0];
	};

const AliUInt32_t kAliHLTThreadMsgBaseSize = sizeof(AliHLTThreadMsg);









} // extern "C"

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#endif // _AliHLTTHREADMSG_H_
