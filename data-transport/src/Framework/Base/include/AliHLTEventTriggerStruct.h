/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3EVENTTRIGGERSTRUCT_H_
#define _ALIL3EVENTTRIGGERSTRUCT_H_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliHLTBlockHeader.h"

#ifdef __cplusplus
extern "C" {
#endif

struct AliHLTEventTriggerStruct
    {
	AliHLTBlockHeader    fHeader;
	AliUInt32_t         fDataWordCount;
	AliUInt32_t         fDataWords[0];
    };

typedef struct AliHLTEventTriggerStruct AliHLTEventTriggerStruct;

#define ALIL3EVENTTRIGGERSTRUCT_TYPE ((AliUInt32_t)'EVTT')


bool CompareEventTriggerTypes( void* param, const AliHLTEventTriggerStruct* ett1, const AliHLTEventTriggerStruct* ett2 );

typedef bool (*AliHLTEventTriggerTypeCompareFunc)( void* param, const AliHLTEventTriggerStruct* ettSubscriber, const AliHLTEventTriggerStruct* ettEvent );



#ifdef __cplusplus
}
#endif


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif /* _ALIL3EVENTTRIGGERSTRUCT_H_ */
