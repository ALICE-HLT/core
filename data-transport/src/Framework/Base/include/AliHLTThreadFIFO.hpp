/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _AliHLTTHREADFIFO_HPP_
#define _AliHLTTHREADFIFO_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

class AliHLTPThreadMsg;

#include "AliHLTTypes.h"
#include "AliHLTThreadMsg.h"

#include <pthread.h>

/**
 * A fifo class for use in inter-thread communication
 *
 * The class provides actually two buffers, working as ring buffers,
 * which can basically be read/written in a FIFO scheme. One of the buffers
 * works for ordinary messages and another one for emergency high priority messages.
 * The emergency buffer is only able to hold ONE (1) message.
 * But, this distinction between ordinary and emergency messages is only made when
 * writing into the buffer. When reading no distinction is made and any message 
 * present in the emergency buffer will always be read out first. Only when no message 
 * is actually in the emergency buffer will the next message from the ordinary buffer
 + be read out.
 *
 * @short A fifo class for use in inter-thread communication
 * @author $Author$
 * @version $Id$
 */
class AliHLTThreadFIFO
	{
	public:

		/**
		 * An enum holding the various errors that can occur on this class.
		 * These are:
		 *
		 * kOk: No error
		 *
		 * kInternalError: Internal sanity checking has revealed an internal error which could
		 * not be recovered. This should onyl occur when compiling with PARANOID define'd.
		 *
		 * kBufferFull: The buffer into which the write would go is already full. A retry later might succeed,
		 * 
		 * kBufferEmpty: The buffer from which a read was attempted is empty.
		 * 
		 * kBufferTooSmall: Although the buffer is empty the message to write into the buffer is too large to fit in. 
		 * Even a later retry will not succeed
		 *
		 * kWrongMsgLength: The length of the message has a wrong value, most probably 0.
		 *
		 * kOutOfMemory: A dynamic memory allocation could not be carried out.
		 */
		enum TError { kOk=0, kInternalError, kBufferFull, kBufferEmpty, kBufferTooSmall, kWrongMsgLength,
			      kOutOfMemory, kWrongMsg };

		/**
		 * Constructor.
		 * Takes as arguments the size of the (normal) message buffer and the size
		 * of the buffer used for emergency messages, both of them given in dword counts.
		 *
		 * @param bufferSizeDWords The size of the (normal) message buffer in dword counts.
		 * @param emergencySizeDWords The size of the buffer for emergency (high priority) messages in dword counts.
		 */
		AliHLTThreadFIFO( AliUInt32_t bufferSizeDWords, AliUInt32_t emergencySizeDWords );

		/**
		 * Destructor.
		 * Cleans up the buffers allocated in the constructor.
		 */
		~AliHLTThreadFIFO();

		/**
		 * Write a message into the (normal) message buffer.
		 *
		 * Possible errors from this method are kOk, kBufferFull, kBufferTooSmall, and kWrongMsgLength
		 *
		 * @return A TError giving the error that occured or kOk if no error occured.
		 * @param msg (A pointer to) the message to write into the buffer.
		 */
		TError Write( const AliHLTThreadMsg* msg );

                /**
		 * Get a pointer to the next available message slot for writing.
		 * This locks the FIFO until CommitMsg is called.
		 *
		 * Possible errors from this method are kOk, kBufferFull, kBufferTooSmall, and kWrongMsgLength
		 *
		 * @return A TError giving the error that occured or kOk if no error occured.
		 * @param length The size of the message slot to reserve in bytes.
		 * @param ptr A reference to a pointer, where a pointer to the requested slots will be 
		 * returned or NULL in case of an error
		 */
   	        TError ReserveSlot( AliUInt32_t msgLength, AliHLTThreadMsg*& msgPtr );

	        /**
		 * Commit a message written into a slot reserved previously by 
		 * ReserveSlot. Also unlocks the FIFO locked by a call to ReserveSlot.
		 *
		 * Possible errors from this method are kOk, kBufferFull, kBufferTooSmall, and kWrongMsgLength
		 *
		 * @return A TError giving the error that occured or kOk if no error occured.
		 * @param length The size of the message slot to commit in bytes.
		 */
	        TError CommitMsg( AliUInt32_t msgLength );

		/**
		 * Wait until some data is available
		 *
		 * Possible errors from this method are kOk
		 *
		 * @return A TError giving the erroro that occured or kOk if no error occured.
		 */
		TError WaitForData();

		/**
		 * Read a message from the FIFO. If there is a message in the emergency buffer
		 * this will be read otherwise a message (if present) from the normal bessage 
		 * buffer is used. The memory space for the buffer will be allocated by this 
		 * method and the pointer will be stored in the passed reference argument. The user
		 * has to take the responsibility for later deleting the buffer when it is not 
		 * needed anymore.
		 *
		 * Possible errors from this method are kOk, or kBufferEmpty.
		 *
		 * @return A TError giving the error that occured or kOk if no error occured.
		 * @param msg A (reference to the) pointer where to store the message read 
		 * from the buffer.
		 */
		TError Read( AliHLTThreadMsg*& msg );


		// The following methods are sort of peek methods for looking at the next message
		// in the buffer without actually copying it from the buffer and for freeing it when 
		// finished.

		/**
		 * Get a pointer to the start of the next message in the buffer (either the emergency
		 * buffer if available or the normal one). No actual data is copied from the buffer 
		 * in this call.
		 *
		 * The memory occupied by this message should be freed by a call to FreeNextmsg
		 *
		 * @see FreeNextMsg.
		 * @return A pointer to the start of the next message in the buffer or NULL if the buffer is empty.
		 */
		AliHLTThreadMsg* GetNextMsg();

		/**
		 * Free the space occupied by the current (read-) message in the buffer.
		 *
		 * Possible errors from this method are kOk or kBufferEmpty.
		 *
		 * @see GetNextMsg.
		 * @return void
		 */
		TError FreeNextMsg( AliHLTThreadMsg* msg );

		/**
		 * Write a message into the emergency message buffer.
		 *
		 * Possible errors from this method are kOk, kBufferFull, kBufferTooSmall, and kWrongMsgLength
		 *
		 * @return A TError giving the error that occured or kOk if no error occured.
		 * @param msg (A pointer to) the message to write into the emergency buffer.
		 */
		TError WriteEmergency( const AliHLTThreadMsg* msg );
		
	protected:

		/**
		 * Method to read a message from the ordinary buffer.
		 * Called by Read when no message is in the emergency buffer.
		 *
		 * Possible errors from this method are kOk, or kBufferEmpty.
		 *
		 * @see Read
		 * @return A TError giving the error that occured or kOk if no error occured.
		 * @param msg A (reference to the) pointer where to store the message read 
		 * from the buffer.
		 */
		TError ReadOrdinary( AliHLTThreadMsg*& msg );

		/**
		 * Method to read a message from the emergency buffer.
		 * Called by Read when a message is in the emergency buffer.
		 *
		 * Possible errors from this method are kOk, or kBufferEmpty.
		 *
		 * @see Read
		 * @return A TError giving the error that occured or kOk if no error occured.
		 * @param msg A (reference to the) pointer where to store the message read 
		 * from the buffer.
		 */
		TError ReadEmergency( AliHLTThreadMsg*& msg );

		/**
		 * The (pointer to the) (normal) message buffer.
		 */
		AliUInt8_t* fBuffer;

		/**
		 * The size of the (normal) buffer allocated.
		 */
		AliUInt32_t fBufferSize;

		/**
		 * The read index into the buffer. This is incremented when reading 
		 * from the buffer and will never overtake the write index.
		 */
		AliUInt32_t fBufferRead;

		/**
		 * Index for the actual last written position in the buffer (This 
		 * means physically last position). 
		 *
		 * This is needed for wrap around ring buffer handling. When writing into the buffer,
		 * and a message would internally wrap around in the buffer, the rest of the buffer is
		 * left free, this index is set to the actual position where the message originally 
		 * would have started, and the message is stored beginning at the start of the buffer.
		 * 
		 * Thus if after reading the fBufferRead index equals this index, the fBufferRead index
		 * has to be set to 0 to start again from the beginning.
		 */
		AliUInt32_t fBufferWriteEnd;
		
		/**
		 * The write index into the buffer. This is incremented when writing to the buffer and
		 * will never overtake the read index.
		 */
		AliUInt32_t fBufferWrite;

// 		/**
// 		 * This is set to true when writing into the buffer and to false
// 		 * when reading from it. This is needed to distinguish the two cases
// 		 * buffer completely filled and buffer completely empty, since in both 
// 		 * cases fBufferRead and fBufferWrite will be equal.
// 		 */
// 		bool fLastBufferWrite;

	        /**
		 * This is increased by one after every write access.
		 * Together with fReadCnt this serves to determine wether
		 * a FIFO is completely empty or completely full.
		 */
	        AliUInt32_t fWriteCnt;

	        /**
		 * This is increased by one after every read access.
		 * Together with fWriteCnt this serves to determine wether
		 * a FIFO is completely empty or completely full.
		 */
	        AliUInt32_t fReadCnt;


		/**
		 * The (pointer to the) emergency message buffer.
		 */
		AliUInt8_t* fEmergency;

		/**
		 * The size of the emergency buffer allocated.
		 */
		AliUInt32_t fEmergencySize;

		/**
		 * The write index into the emergency buffer. This is incremented when writing to the buffer.
		 *
		 * The emergency buffer does not need a read index, since only one message ever will be stored in it.
		 */
		AliUInt32_t fEmergencyWrite;



		/**
		 * The mutex thread sempahore used to regulate access to
		 * the FIFO from separate threads.
		 */
		pthread_mutex_t fAccessSem;

		/**
		 * The mutex thread sempahore used to wait for data in the thread.
		 */
	    //pthread_mutex_t fDataSem;
	    pthread_cond_t fCond;
	    //pthread_mutex_t fCondSem;


	private:
	};







/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _AliHLTTHREADFIFO_HPP_
