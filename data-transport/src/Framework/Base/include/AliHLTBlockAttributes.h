/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIHLTBLOCKATTRIBUTES_H_
#define _ALIHLTBLOCKATTRIBUTES_H_

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTSubEventDataDescriptor.h 787 2006-02-17 19:21:26Z timm $ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"


#ifdef __cplusplus
extern "C" {
#endif

    const int kAliHLTBlockDByteOrderAttributeIndex        = 0;
    const int kAliHLTBlockDAlignmentAttributeIndexStart   = 2;
    const int kAliHLTBlockD64BitAlignmentAttributeIndex   = kAliHLTBlockDAlignmentAttributeIndexStart;
    const int kAliHLTBlockD32BitAlignmentAttributeIndex   = kAliHLTBlockDAlignmentAttributeIndexStart+1;
    const int kAliHLTBlockD16BitAlignmentAttributeIndex   = kAliHLTBlockDAlignmentAttributeIndexStart+2;
    const int kAliHLTBlockD8BitAlignmentAttributeIndex    = kAliHLTBlockDAlignmentAttributeIndexStart+3;
    const int kAliHLTBlockDFloatAlignmentAttributeIndex   = kAliHLTBlockDAlignmentAttributeIndexStart+4;
    const int kAliHLTBlockDDoubleAlignmentAttributeIndex  = kAliHLTBlockDAlignmentAttributeIndexStart+5;
    const int kAliHLTBlockDLastAlignmentAttributeIndex    = kAliHLTBlockDDoubleAlignmentAttributeIndex;
    const int kAliHLTBlockDAlignmentAttributeCount        = kAliHLTBlockDLastAlignmentAttributeIndex-kAliHLTBlockDAlignmentAttributeIndexStart+1;
    const int kAliHLTBlockDAttributeCount                 = 8;

    const AliUInt8_t kAliHLTUnknownAttribute = 0;
    const AliUInt8_t kAliHLTUnspecifiedAttribute = ~(AliUInt8_t)0;

    const AliUInt8_t kAliHLTUnknownAlignment = kAliHLTUnknownAttribute;
    const AliUInt8_t kAliHLTUnspecifiedAlignment = kAliHLTUnspecifiedAttribute;

    /* For byte order as well as alignment 255 means unspecified, 
       let it be filled in by the system to the current values.
       The unknown placeholder in contrast really mean unknown, 
       they won't be filled in by the system to something which is probably wrong.
    */

    /* Keep this consistent with BCLNetworkData.hpp kLittleEndian/kBigEndian */
    const AliUInt8_t kAliHLTUnknownByteOrder      = kAliHLTUnknownAttribute;
    const AliUInt8_t kAliHLTLittleEndianByteOrder = 1;
    const AliUInt8_t kAliHLTBigEndianByteOrder    = 2;
    const AliUInt8_t kAliHLTUnspecifiedByteOrder  = kAliHLTUnspecifiedAttribute;

    const AliUInt8_t kAliHLTNetworkByteOrder      = kAliHLTBigEndianByteOrder;

#if defined(__i386__) or defined(__arm__) or defined(__x86_64__)
    const AliUInt8_t kAliHLTNativeByteOrder = kAliHLTLittleEndianByteOrder;
#else
#if defined(__powerpc__)
    const AliUInt8_t kAliHLTNativeByteOrder = kAliHLTBigEndianByteOrder;
#else
#error Byte format (little/big endian) currently not defined for platforms other than intel i386 compatible...
#endif
#endif




#ifdef __cplusplus
}
#endif

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTSubEventDataDescriptor.h 787 2006-02-17 19:21:26Z timm $ 
**
***************************************************************************
*/

#endif /* _ALIHLTBLOCKATTRIBUTES_H_ */
