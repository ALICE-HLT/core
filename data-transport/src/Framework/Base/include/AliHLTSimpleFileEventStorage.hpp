#ifndef _ALIHLTSIMPLEFILEEVENTSTORAGE_HPP_
#define _ALIHLTSIMPLEFILEEVENTSTORAGE_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTEventStorageInterface.hpp"
#include "MLUCString.hpp"
#include "MLUCDynamicAllocCache.hpp"
#include <sys/types.h>


class AliHLTSimpleFileEventStorage: public AliHLTEventStorageInterface
    {
    public:

	class SimpleFileEventLocationDescriptor: public AliHLTEventStorageInterface::EventLocationDescriptor
	    {
	    public:
		// Opaque holder for event location data.
		SimpleFileEventLocationDescriptor():
			fNdxFileNr( 0 ),
			fFileOffset( 0 )
			{
			}
		SimpleFileEventLocationDescriptor( unsigned long ndxFileNr, off_t fileOffset ):
			fNdxFileNr( ndxFileNr),
			fFileOffset( fileOffset)
			{
			}
		virtual ~SimpleFileEventLocationDescriptor()
			{
			}

		unsigned long GetNdxFileNr() const
			{
			return fNdxFileNr;
			}

		off_t GetFileOffset() const 
			{
			return fFileOffset;
			}

		virtual const char* GetStorageTypeName() const
			{
			return "SimpleFileEventStorage";
			}

	    protected:

		
		void SetNdxFileNr( unsigned long ndxFileNr )
			{
			fNdxFileNr = ndxFileNr;
			}
		void SetFileOffset( off_t fileOffset )
			{
			fFileOffset = fileOffset;
			}

		unsigned long fNdxFileNr;
		off_t fFileOffset;

		    friend class AliHLTSimpleFileEventStorage;
		
	    };


	AliHLTSimpleFileEventStorage();

	virtual ~AliHLTSimpleFileEventStorage();

	virtual const char* GetStorageTypeName() const
		{
		return "SimpleFileEventStorage";
		}

	virtual int OpenStorage( const char* storageName, bool write = false );

	virtual int CloseStorage();

	virtual void DumpEventLocation( AliHLTLog::TLogLevel lvl, const char* keyword, const EventLocationDescriptor* eld );

	virtual int ResetEventLocation();
	virtual int GetNextEventLocation( EventLocationDescriptor* eld, AliUInt64_t eventSkipCount = 0 );
	virtual int FindEventLocation( AliEventID_t eventID, EventLocationDescriptor* eld );
	virtual EventLocationDescriptor* AllocEventLocation();
 	virtual void FreeEventLocation( EventLocationDescriptor* eld );
// 	virtual int GetEventDescriptor( const EventLocationDescriptor* eld, AliHLTSubEventDataDescriptor*& sedd );
// 	virtual int GetEventData( const EventLocationDescriptor* eld, AliHLTSubEventDataDescriptor* sedd, std::vector<AliUInt8_t*> dataBlockPtrs );
	virtual int GetEventStorageMetaData( const EventLocationDescriptor* eld, EventStorageMetaData* esmd );
	virtual int GetSubEventDataDescriptor( const EventLocationDescriptor* eld, AliHLTSubEventDataDescriptor* sedd );
	virtual int GetEventBlockData( const EventLocationDescriptor* eld, std::vector<AliHLTSubEventDescriptor::BlockData>& dataBlocks );
	virtual int GetEventData( const EventLocationDescriptor* eld, const std::vector<AliHLTSubEventDescriptor::BlockData>& dataBlocks );
	virtual int GetEventTriggerSize( const EventLocationDescriptor* eld, unsigned long& size );
	virtual int GetEventTriggerData( const EventLocationDescriptor* eld, AliHLTEventTriggerStruct* ets );

	virtual int WriteEvent( const AliHLTSubEventDataDescriptor* sedd, unsigned long blockCnt, 
				AliHLTSubEventDescriptor::BlockData* blocks, 
				const AliHLTEventTriggerStruct* ets, EventLocationDescriptor* eld = NULL );

	
    protected:


	struct FileEventDescriptor
	    {
		AliHLTBlockHeader         fHeader;
		AliEventID_t             fEventID;
		AliUInt32_t              fEventBirth_s;
		AliUInt32_t              fEventBirth_us;
		AliUInt32_t              fEventWritten_s;
		AliUInt32_t              fEventWritten_us;
		AliHLTNodeID_t           fEventWriteNode;
		AliUInt32_t              fEventWriteNodeByteOrder;
		AliHLTEventDataType      fDataType;
		AliUInt32_t              fDataBlockCount;
	    };

	struct FileEventDataBlockDescriptorV1
	    {
		AliUInt64_t           fFileNr;
		AliUInt32_t           fBlockSize;
		AliUInt64_t           fBlockOffset;
		AliHLTNodeID_t        fProducerNode;
		AliHLTEventDataType   fDataType;
		AliHLTEventDataOrigin fDataOrigin;
		AliUInt32_t           fDataSpecification;
		AliUInt32_t           fByteOrder;
	    };

	struct FileEventDataBlockDescriptorV2
	    {
		AliUInt64_t           fFileNr;
		AliUInt32_t           fBlockSize;
		AliUInt64_t           fBlockOffset;
		AliHLTNodeID_t        fProducerNode;
		AliHLTEventDataType   fDataType;
		AliHLTEventDataOrigin fDataOrigin;
		AliUInt32_t           fDataSpecification;
		AliUInt8_t            fAttributes[kAliHLTSEDBDAttributeCount];
	    };

	typedef FileEventDataBlockDescriptorV2 FileEventDataBlockDescriptor;

	struct FileEventTriggerDescriptor
	    {
		AliUInt64_t fFileNr;
		AliUInt32_t fBlockSize;
		AliUInt64_t fBlockOffset;
	    };

	struct FileEventData
	    {
		FileEventDescriptor fEvent;
		FileEventTriggerDescriptor fTrigger;
		FileEventDataBlockDescriptor fBlocks[];
	    };

	struct FileEventDataV1
	    {
		FileEventDescriptor fEvent;
		FileEventTriggerDescriptor fTrigger;
		FileEventDataBlockDescriptorV1 fBlocks[];
	    };

#define ALIHLTFILEEVENTDATA_TYPE ((AliUInt32_t)'FED ')

	void CloseRead();
	void CloseWrite();
	
	int OpenRead();
	int OpenWrite();

	int OpenNdxFileRead();
	int SeekNdxFileRead();
	int CloseNdxFileRead();

	int OpenNdxFileWrite();
	int CloseNdxFileWrite();

	int OpenDataFileRead();
	int SeekDataFileRead();
	int CloseDataFileRead();

	int OpenDataFileWrite();
	int CloseDataFileWrite();

	int OpenTriggerFileRead();
	int SeekTriggerFileRead();
	int CloseTriggerFileRead();

	int OpenTriggerFileWrite();
	int CloseTriggerFileWrite();

	int FillEventDescriptor( const AliHLTSubEventDataDescriptor* sedd, FileEventDescriptor& fed );
	int WriteTriggerData( const AliHLTEventTriggerStruct* ets, FileEventTriggerDescriptor& fetd );
	int WriteBlock( const AliHLTSubEventDescriptor::BlockData& block, FileEventDataBlockDescriptor& fedbd, bool final );
	int WriteFileEventData( const FileEventData* fed, SimpleFileEventLocationDescriptor* eld );

	int ReadFileEventData( FileEventData*& fed, SimpleFileEventLocationDescriptor* eld );
	int ReadFileEventData( const EventLocationDescriptor* eld, FileEventData*& fed );
	int FillEventLocationDescriptor( SimpleFileEventLocationDescriptor* eld );

	void TransformEventDescriptor( FileEventDescriptor& fed );
	void TransformTriggerData( FileEventTriggerDescriptor& fetd );
	void TransformBlock( FileEventDataBlockDescriptor& fedbd );

	void ConvertFileEventDataV1ToCurrent( FileEventDataV1* fed1, FileEventData*& fed );

	int Write( const int& fh, const void* buffer, int len );
	int Read( const int& fh, void* buffer, int len );

	uint32 fVersion;

	AliHLTNodeID_t fNodeID;

	MLUCString fStorageName;
	bool fWriteable;

	unsigned long fCurrentNdxFileReadNr;
	off_t fCurrentNdxFileReadOffset;

	AliUInt64_t fCurrentDataFileReadNr;
	off_t fCurrentDataFileReadOffset;

	AliUInt64_t fCurrentTriggerFileReadNr;
	off_t fCurrentTriggerFileReadOffset;

	int fNdxReadFH;
	int fDataReadFH;
	int fTriggerReadFH;

	unsigned long fCurrentNdxFileWriteNr;
	off_t fCurrentNdxFileWriteOffset;

	AliUInt64_t fCurrentDataFileWriteNr;
	off_t fCurrentDataFileWriteOffset;

	AliUInt64_t fCurrentTriggerFileWriteNr;
	off_t fCurrentTriggerFileWriteOffset;

	int fNdxWriteFH;
	int fDataWriteFH;
	int fTriggerWriteFH;

	off_t fMaxNdxFileSize;
	off_t fMaxDataFileSize;
	off_t fMaxTriggerFileSize;

	MLUCDynamicAllocCache fFEDCache;

	static const unsigned kPadSize;

    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTSIMPLEFILEEVENTSTORAGE_HPP_
