/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIHLTEVENTMODULOCALCULATION_H_
#define _ALIHLTEVENTMODULOCALCULATION_H_

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTShmID.h 3810 2008-05-28 10:05:12Z timm $ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"

class AliHLTEventModuloCalculation
    {
    public:
	AliHLTEventModuloCalculation( unsigned long preDivisor=1 ):
	    fEventModuloEventIDBased(true),
	    fEventModuloPreDivisor(preDivisor)
		{
		}

	void SetEventModuloPreDivisor( unsigned long preDivisor )
		{
		fEventModuloPreDivisor = preDivisor;
		}

	bool ModuloMatch( unsigned long count, AliEventID_t eventID, unsigned long modulo ) const
		{
		return (!fEventModuloEventIDBased && !((count / fEventModuloPreDivisor) % modulo))
		    || (fEventModuloEventIDBased && !((eventID.fNr / fEventModuloPreDivisor) % modulo));
		}
    protected:
    private:
	bool fEventModuloEventIDBased;
	unsigned long fEventModuloPreDivisor;
    };


/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: AliHLTShmID.h 3810 2008-05-28 10:05:12Z timm $ 
**
***************************************************************************
*/

#endif /* _ALIHLTEVENTMODULOCALCULATION_H_ */
