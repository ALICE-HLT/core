/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3TIMER_HPP_
#define _ALIL3TIMER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

//#define _XOPEN_SOURCE 500
#include "AliHLTThread.hpp"
#include "AliHLTTypes.h"
#include <vector>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>


class AliHLTTimerCallback;

#if __GNUC__>=3
using namespace std;
#endif

// Store debug information about a condition sem's owner.
#define OWNER_DEBUG

class AliHLTTimer
    {
    public:

	AliHLTTimer( int timerSlotAllocE2 = -1 );
	~AliHLTTimer();

	void Start();
	void Stop();
	AliUInt32_t AddTimeout( AliUInt32_t timeout_ms, AliHLTTimerCallback* callback, AliUInt64_t notData, unsigned int priority = 0 );
	bool CancelTimeout( AliUInt32_t id );
	bool NewTimeout( AliUInt32_t id, AliUInt32_t new_timeout_ms );

	unsigned long GetTimeoutCnt();

	void CancelAllTimeouts( vector<AliUInt64_t>& notificationData );
	void CancelTimeouts( AliHLTTimerCallback* callback, vector<AliUInt64_t>& notificationData );

    protected:

	void CancelTimeoutsFor( AliHLTTimerCallback* callback, vector<AliUInt64_t>& notificationData ); // callback==NULL means all timeouts.

	class AliHLTTimerThread: public AliHLTThread
	    {
	    public:

		AliHLTTimerThread( AliHLTTimer& timer );

	    protected:

		virtual void Run();

		AliHLTTimer &fTimer;

	    private:
	    };

	typedef struct 
	    {
		AliUInt32_t fID;
		struct timeval fTime;
		//AliUInt32_t fTimeout; // in ms;
		AliHLTTimerCallback* fCallback;
		AliUInt64_t fNotData;
		unsigned int fPriority;
		bool fValid;
	    }
	TimeoutData;

	void TimerLoop();

	bool InsertTimeout( TimeoutData& data );

	void Resize( bool grow );
	bool Compact( TimeoutData* data );

	void TimerExpired();

	AliUInt32_t DiffMS( struct timeval& tv1, struct timeval tv2 );

	AliHLTTimerThread fThread;


	AliUInt32_t fNextID;
	pthread_mutex_t fAccessSem;

	pthread_cond_t fCondition;
	pthread_mutex_t fConditionSem;

	unsigned long fTimeoutSize;
	unsigned long fOriginalTimeoutSize;
	unsigned long fTimeoutMask;
	unsigned long fTimeoutCnt;
	unsigned long fInvalidsCnt;
	unsigned long fFirstUsed;
	unsigned long fNextFree;
	vector<TimeoutData> fTimeouts;
	vector<TimeoutData>::iterator fTimeoutStart;

	//struct timeval fTimerStarted;


	bool fQuit;
	bool fQuitted;
	bool fInCallback;

	friend class AliHLTTimerThread;

	// Return -1 if t1<t2, +1 if t1>t2, 0 if t1==t2
	int Compare( const struct timeval& t1, const struct timeval& t2 );
// 		{
// 		if ( t1.tv_sec == t2.tv_sec )
// 		    {
// 		    if ( t1.tv_usec == t2.tv_usec )
// 			return 0;
// 		    if ( t1.tv_usec < t2.tv_usec )
// 			return -1;
// 		    return 1;
// 		    }
// 		else
// 		    {
// 		    if ( t1.tv_sec < t2.tv_sec )
// 			return -1;
// 		    return 1;
// 		    }
// 		}

    private:
    };


class AliHLTTimerCallback
    {
    public:

	virtual ~AliHLTTimerCallback() {};

	virtual void TimerExpired( AliUInt64_t notData, unsigned long priority ) = 0;

    };


class AliHLTTimerConditionSem: public AliHLTTimerCallback
    {
    public:

	//AliHLTTimerConditionSem();
	AliHLTTimerConditionSem( int notDataSlotCountExp2 = -1 , bool allowGrowth = false );
	virtual ~AliHLTTimerConditionSem();

	void Wait();
	bool WaitTime( AliUInt32_t timeout_ms ); // true when condition was signalled, false, when timeout expired; timeout given in ms.
	void Signal();
	virtual void TimerExpired( AliUInt64_t notData, unsigned long priority );
	void Lock();
	void Unlock();

	bool AddNotificationData( AliUInt64_t data );
	bool SneakNotificationData( AliUInt64_t data );
	bool HaveNotificationData();
	unsigned long GetNotificationDataCnt();
	AliUInt64_t PeekNotificationData();
	AliUInt64_t PopNotificationData();

    protected:

	pthread_cond_t fCondition;
	pthread_mutex_t fConditionSem;

	pthread_mutex_t fAccessSem;

	struct NotificationDataType
	    {
		bool fValid;
		AliUInt64_t fData;
	    };

	//vector<AliUInt64_t> fNotificationData;
	vector<NotificationDataType> fNotificationData;
	unsigned long fNotificationDataSize;
	unsigned long fNotificationDataOrigSize;
	unsigned long fNotificationDataMask;
	unsigned long fNotificationDataCnt;
	unsigned long fNotificationValidDataCnt;
	unsigned long fFirstUsed;
	unsigned long fNextToUse;
	//unsigned long fLastFreed, fLastUsed;
	//vector<AliUInt64_t>::iterator fNotificationStart;
	vector<NotificationDataType>::iterator fNotificationStart;
	bool fAllowGrowth;
	//bool fHasGrown;

	void Resize( bool grow );


#ifdef OWNER_DEBUG
	bool fOwned;
	pthread_t fOwnerThread;
	pid_t fOwnerPID;
#endif

    private:
    };
	



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3TIMER_HPP_
