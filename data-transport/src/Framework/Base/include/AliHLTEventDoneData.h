// XEmacs -*-C++-*-
/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3EVENTDONEDATA_H_
#define _ALIL3EVENTDONEDATA_H_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliEventID.h"
#include "AliHLTBlockHeader.h"

#ifdef __cplusplus
extern "C" {
#endif

struct AliHLTEventDoneData
    {
	AliHLTBlockHeader    fHeader;
	AliEventID_t         fEventID;
	AliUInt64_t          fStatusFlags;
	AliUInt32_t          fDataWordCount;
	AliUInt32_t          fDataWords[0];
    };

typedef struct AliHLTEventDoneData AliHLTEventDoneData;

#define ALIL3EVENTDONEDATA_TYPE ((AliUInt32_t)'EVDD')

    enum AliHLTEventDoneDataCommands { kAliHLTEventDoneNOPCmd=0, kAliHLTEventDoneMonitorReplayEventIDCmd=1, 
				       kAliHLTEventDoneMonitorFirstOrbitEventCmd=2, 
				       kAliHLTEventDoneReadoutListCmd=3,
				       kAliHLTEventDoneMonitorListCmd=4,
				       kAliHLTEventDoneFlagMonitorEventCmd=5,
				       kAliHLTEventDoneDebugCmd=6,
                                       kAliHLTEventDoneBackPressureHighWaterMark =7,
                                       kAliHLTEventDoneBackPressureLowWaterMark=8,
    };

#ifdef __cplusplus
}
#endif


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif /* _ALIL3EVENTDONEDATA_H_ */
