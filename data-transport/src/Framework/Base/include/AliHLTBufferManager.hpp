/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3BUFFERMANAGER_HPP_
#define _ALIL3BUFFERMANAGER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliHLTShmID.h"
#include "AliHLTConfig.hpp"
#include <vector>
#include <pthread.h>

#if __GNUC__>=3
using namespace std;
#endif


class AliHLTBufferManager
    {
    protected:

	struct TBufferData
	    {
		AliUInt32_t fBufferNdx;
		unsigned long fBufferSize;
		AliHLTShmID fShmKey;
	    };

    public:

	AliHLTBufferManager()
		{
		fDummyShmID.fShmType = kInvalidShmType;
		fDummyShmID.fKey.fID = (AliUInt64_t)-1;
		fNextBufferNdx = 0;
		fTotalBufferSize = fFreeBufferSize = 0;
		pthread_mutex_init( &fBufferMutex, NULL );
		}

	AliHLTBufferManager( AliHLTShmID shmKey, unsigned long bufferSize )
		{
		TBufferData buffer;
		if ( bufferSize % gMemoryAlignment )
		    bufferSize -= (bufferSize % gMemoryAlignment);
		buffer.fBufferSize = bufferSize;
		buffer.fShmKey.fShmType = shmKey.fShmType;
		buffer.fShmKey.fKey.fID = shmKey.fKey.fID;
		buffer.fBufferNdx = 0;
		fBuffers.insert( fBuffers.end(), buffer );
		fTotalBufferSize = fFreeBufferSize = bufferSize;
		fDummyShmID.fShmType = kInvalidShmType;
		fDummyShmID.fKey.fID = (AliUInt64_t)-1;
		fNextBufferNdx = 1;
		pthread_mutex_init( &fBufferMutex, NULL );
		}

	virtual ~AliHLTBufferManager()
		{
		pthread_mutex_destroy( &fBufferMutex );
		};

	const AliHLTShmID& GetShmKey( AliUInt32_t bufferNdx )
		{
		AliHLTShmID* retID = NULL;
		int ret;
		ret = pthread_mutex_lock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
	    
		//retID = (bufferNdx < fBuffers.size()) ? &(fBuffers[bufferNdx].fShmKey) : &(fDummyShmID);
		vector<TBufferData>::iterator iter, end;
		if ( bufferNdx < fBuffers.size() && fBuffers[bufferNdx].fBufferNdx==bufferNdx )
		    retID = &(fBuffers[bufferNdx].fShmKey);
		else
		    {
		    iter = fBuffers.begin();
		    end = fBuffers.end();
		    while ( iter != end )
			{
			if ( iter->fBufferNdx == bufferNdx )
			    {
			    retID = &(iter->fShmKey);
			    break;
			    }
			iter++;
			}
		    }
		if ( !retID )
		    retID = &(fDummyShmID);
		ret = pthread_mutex_unlock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		return *retID;
		}

	unsigned long GetBufferSize( AliUInt32_t bufferNdx )
		{
		unsigned long retSize = 0;
		int ret;
		ret = pthread_mutex_lock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
	    
		//retID = (bufferNdx < fBuffers.size()) ? &(fBuffers[bufferNdx].fShmKey) : &(fDummyShmID);
		vector<TBufferData>::iterator iter, end;
		if ( bufferNdx < fBuffers.size() && fBuffers[bufferNdx].fBufferNdx==bufferNdx )
		    retSize = fBuffers[bufferNdx].fBufferSize;
		else
		    {
		    iter = fBuffers.begin();
		    end = fBuffers.end();
		    while ( iter != end )
			{
			if ( iter->fBufferNdx == bufferNdx )
			    {
			    retSize = iter->fBufferSize;
			    break;
			    }
			iter++;
			}
		    }
		ret = pthread_mutex_unlock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		return retSize;
		}

	AliUInt32_t GetBufferIndex( AliHLTShmID shmKey )
		{
		int ret;
		vector<TBufferData>::iterator bufIter, bufEnd;
		AliUInt32_t retNdx = ~(AliUInt32_t)0;
		ret = pthread_mutex_lock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		bufIter = fBuffers.begin();
		bufEnd = fBuffers.end();
		
		while ( bufIter != bufEnd )
		    {
		    if ( bufIter->fShmKey.fShmType == shmKey.fShmType &&
			 bufIter->fShmKey.fKey.fID == shmKey.fKey.fID )
			{
			retNdx = bufIter->fBufferNdx;
			break;
			}
		    bufIter++;
		    }
		ret = pthread_mutex_unlock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		return retNdx;
		}

	bool HasBuffer( AliHLTShmID shmKey )
		{
		int ret;
		vector<TBufferData>::iterator bufIter, bufEnd;
		bool found=false;
		ret = pthread_mutex_lock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		bufIter = fBuffers.begin();
		bufEnd = fBuffers.end();
		
		while ( bufIter != bufEnd )
		    {
		    if ( bufIter->fShmKey.fShmType == shmKey.fShmType &&
			 bufIter->fShmKey.fKey.fID == shmKey.fKey.fID )
			{
			found = true;
			break;
			}
		    bufIter++;
		    }
		ret = pthread_mutex_unlock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		return found;
		}
	bool HasBuffer( AliUInt32_t bufferNdx )
		{
		int ret;
		vector<TBufferData>::iterator bufIter, bufEnd;
		bool found=false;
		ret = pthread_mutex_lock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		bufIter = fBuffers.begin();
		bufEnd = fBuffers.end();
		
		while ( bufIter != bufEnd )
		    {
		    if ( bufIter->fBufferNdx == bufferNdx )
			{
			found = true;
			break;
			}
		    bufIter++;
		    }
		ret = pthread_mutex_unlock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		return found;
		}


	virtual bool AddBuffer( AliHLTShmID shmKey, unsigned long bufferSize ) = 0;
	virtual bool DeleteBuffer( AliHLTShmID shmKey, bool force = false ) = 0;

	virtual bool CanGetBlock() = 0;

	virtual bool GetBlock( AliUInt32_t& bufferNdx, unsigned long& offset,
				unsigned long& size ) = 0;

	virtual bool AdaptBlock( AliUInt32_t bufferNdx, unsigned long offset,
				 unsigned long newSize ) = 0;

	virtual bool AdaptBlock( AliUInt32_t bufferNdx, unsigned long oldOffset,
				 unsigned long newOffset, unsigned long newSize ) = 0;


	virtual bool ReleaseBlock( AliUInt32_t bufferNdx, unsigned long offset, bool noAdaptBlock=false ) = 0;

	virtual bool IsBlockFeasible( unsigned long size )
		{
		bool feasible = false;
		int ret;
		ret = pthread_mutex_lock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		vector<TBufferData>::iterator iter, end;
		iter = fBuffers.begin();
		end = fBuffers.end();
		while ( iter != end )
		    {
		    if ( iter->fBufferSize >= size )
			feasible = true;
		    iter++;
		    }
		ret = pthread_mutex_unlock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		if ( !feasible )
		    return false;
		return true;
		}

	virtual unsigned long long GetTotalBufferSize()
		{
		int ret;
		ret = pthread_mutex_lock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		unsigned long long tmp = fTotalBufferSize;
		ret = pthread_mutex_unlock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		return tmp;
		}
	virtual unsigned long long GetFreeBufferSize()
		{
		int ret;
		ret = pthread_mutex_lock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		unsigned long long tmp = fFreeBufferSize;
		ret = pthread_mutex_unlock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		return tmp;
		}

    protected:

	void DecreaseFree( unsigned long amount )
		{
		int ret;
		ret = pthread_mutex_lock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		fFreeBufferSize -= amount;
		ret = pthread_mutex_unlock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		}
	void IncreaseFree( unsigned long amount )
		{
		int ret;
		ret = pthread_mutex_lock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		fFreeBufferSize += amount;
		ret = pthread_mutex_unlock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		}

	void InsertBuffer( AliHLTShmID shmKey, unsigned long bufferSize )
		{
		TBufferData buffer;
		buffer.fBufferSize = bufferSize;
		buffer.fShmKey.fShmType = shmKey.fShmType;
		buffer.fShmKey.fKey.fID = shmKey.fKey.fID;
		fTotalBufferSize += bufferSize;
		fFreeBufferSize += bufferSize;
		
		int ret;
		ret = pthread_mutex_lock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		buffer.fBufferNdx = fNextBufferNdx++;
		fBuffers.insert( fBuffers.end(), buffer );
		ret = pthread_mutex_unlock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		}

	void RemoveBuffer( AliHLTShmID shmKey )
		{
		int ret;
		ret = pthread_mutex_lock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		vector<TBufferData>::iterator iter, end;
		iter = fBuffers.begin();
		end = fBuffers.end();
		while ( iter != end )
		    {
		    if ( iter->fShmKey.fShmType == shmKey.fShmType && 
			 iter->fShmKey.fKey.fID == shmKey.fKey.fID )
			{
			fTotalBufferSize -= iter->fBufferSize;
			fFreeBufferSize -= iter->fBufferSize;
			fBuffers.erase( iter );
			break;
			}
		    iter++;
		    }
		ret = pthread_mutex_unlock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		}


	AliUInt32_t BufferNr2Ndx( AliUInt32_t bufferNdx )
		{
		int ret;
		AliUInt32_t ndx = 0;
		ret = pthread_mutex_lock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		vector<TBufferData>::iterator iter, end;
		iter = fBuffers.begin();
		end = fBuffers.end();
		while ( iter != end )
		    {
		    if ( iter->fBufferNdx == bufferNdx )
			{
			ret = pthread_mutex_unlock( &fBufferMutex );
			if ( ret )
			    {
			    // XXX
			    }
			return ndx;
			}
		    iter++;
		    ndx++;
		    }
		ret = pthread_mutex_unlock( &fBufferMutex );
		if ( ret )
		    {
		    // XXX
		    }
		return ~(AliUInt32_t)0;
		}



	vector<TBufferData> fBuffers;
	
	AliHLTShmID fDummyShmID;

	pthread_mutex_t fBufferMutex;
	AliUInt32_t fNextBufferNdx;

	unsigned long long fTotalBufferSize;
	unsigned long long fFreeBufferSize;

    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3BUFFERMANAGER_HPP_
