/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTTimer.hpp"

class TimerProxy: public AliHLTTimer
    {
    public:

	TimerProxy( int sz ):
	    AliHLTTimer( sz )
		{
		}

	bool InsertTimeout( TimeoutData& data );

	void Resize( bool grow );
	bool Compact( TimeoutData* data );

	void TimerExpired();

	void LogData();

    protected:
    private:
    };

bool TimerProxy::InsertTimeout( TimeoutData& data )
    {
    bool ret;
    pthread_mutex_lock( &fAccessSem );
    ret = AliHLTTimer::InsertTimeout( data );
    pthread_mutex_unlock( &fAccessSem );
    return ret;
    }

void TimerProxy::Resize( bool grow )
    {
    pthread_mutex_lock( &fAccessSem );
    AliHLTTimer::Resize( grow );
    pthread_mutex_unlock( &fAccessSem );
    }

bool TimerProxy::Compact( TimeoutData* data )
    {
    bool ret;
    pthread_mutex_lock( &fAccessSem );
    ret = AliHLTTimer::Compact( data );
    pthread_mutex_unlock( &fAccessSem );
    return ret;
    }

void TimerProxy::TimerExpired()
    {
    AliHLTTimer::TimerExpired();
    }


void TimerProxy::LogData()
    {
    if ( !fTimeoutSize )
	{
	LOG( AliHLTLog::kDebug, "TimerProxy::LogData", "Data" )
	    << "Using free size mode." << ENDLOG;
	}
    pthread_mutex_lock( &fAccessSem );
#if __GNUC__>=3
    LOG( AliHLTLog::kDebug, "TimerProxy::LogData", "Data" )
	<< "fFirstUsed: 0x" << AliHLTLog::kHex << AliHLTLog::kPrec << 8 << fFirstUsed
	<< " - fTimeoutCnt: 0x" << fTimeoutCnt << " - fInvalidsCnt: 0x" << fInvalidsCnt
	<< " - fNextFree: 0x" << fNextFree << " - fTimeoutSize: 0x" << fTimeoutSize
	<< " - fTimeoutMask: 0x" << fTimeoutMask << " - fTimeoutStart: 0x" << (unsigned long)fTimeoutStart.base()
	<< "." << ENDLOG;
#else
    LOG( AliHLTLog::kDebug, "TimerProxy::LogData", "Data" )
	<< "fFirstUsed: 0x" << AliHLTLog::kHex << AliHLTLog::kPrec << 8 << fFirstUsed
	<< " - fTimeoutCnt: 0x" << fTimeoutCnt << " - fInvalidsCnt: 0x" << fInvalidsCnt
	<< " - fNextFree: 0x" << fNextFree << " - fTimeoutSize: 0x" << fTimeoutSize
	<< " - fTimeoutMask: 0x" << fTimeoutMask << " - fTimeoutStart: 0x" << (unsigned long)fTimeoutStart
	<< "." << ENDLOG;
#endif
    unsigned long n;
    char val;
    char *sP, *eP;
    for ( n = 0; n < fTimeoutSize; n++ )
	{
	if ( (fTimeoutStart+n)->fValid )
	    val = '*';
	else
	    val = ' ';
	if ( n==fFirstUsed )
	    sP = "S";
	else
	    sP = " ";
	if ( n==fNextFree )
	    eP = "E->";
	else
	    eP = " ->";
	LOG( AliHLTLog::kDebug, "TimerProxy::LogData", "Data" )
	    << AliHLTLog::kHex << AliHLTLog::kPrec << 8
	    << "0x" << n << ": " << val << " " << sP << eP
	    << " " << AliHLTLog::kDec << AliHLTLog::kPrec << 10
	    << (fTimeoutStart+n)->fTime.tv_sec << "." 
	    << AliHLTLog::kPrec << 6
	    << (fTimeoutStart+n)->fTime.tv_usec
	    << AliHLTLog::kHex << AliHLTLog::kPrec << 8
	    << " - 0x" << (fTimeoutStart+n)->fID  
	    << AliHLTLog::kPrec << 16
	    << " - 0x" << (fTimeoutStart+n)->fNotData
	    << "." << ENDLOG;
	}
    pthread_mutex_unlock( &fAccessSem );
    }

int main( int, char** )
    {
    AliHLTStdoutLogServer stdOut;
    gLog.AddServer( stdOut );
    AliUInt32_t timerID2, timerID6, timerID7, timerID8, timerID32;

    TimerProxy timer( 4 );
    AliHLTTimerConditionSem signal;
    
    LOG( AliHLTLog::kDebug, "TimerTest", "Dump    1" )
	<< "Dump    1 ----------------------------------- " << ENDLOG;
    timer.LogData();
    timer.AddTimeout( 500, &signal, 1 );
    timerID2 = timer.AddTimeout( 8000, &signal, 2 );
    timer.AddTimeout( 5000, &signal, 3 );
    LOG( AliHLTLog::kDebug, "TimerTest", "Dump    2" )
	<< "Dump    2 ----------------------------------- " << ENDLOG;
    timer.LogData();

    usleep( 500000 );
    timer.TimerExpired();
    LOG( AliHLTLog::kDebug, "TimerTest", "Dump    3" )
	<< "Dump    3 ----------------------------------- " << ENDLOG;
    timer.LogData();

    timer.AddTimeout( 6000, &signal, 4 );
    timer.AddTimeout( 1000, &signal, 5 );
    LOG( AliHLTLog::kDebug, "TimerTest", "Dump    4" )
	<< "Dump    4 ----------------------------------- " << ENDLOG;
    timer.LogData();
    
    timerID6 = timer.AddTimeout( 5000, &signal, 6 );
    LOG( AliHLTLog::kDebug, "TimerTest", "Dump    5" )
	<< "Dump    5 ----------------------------------- " << ENDLOG;
    timer.LogData();
    timer.CancelTimeout( timerID6 );
    LOG( AliHLTLog::kDebug, "TimerTest", "Dump    6" )
	<< "Dump    6 ----------------------------------- " << ENDLOG;
    timer.LogData();
    
    timerID7 = timer.AddTimeout( 5000, &signal, 7 );
    LOG( AliHLTLog::kDebug, "TimerTest", "Dump    7" )
	<< "Dump    7 ----------------------------------- " << ENDLOG;
    timer.LogData();

    timer.CancelTimeout( timerID7 );
    timer.Compact( NULL );
    LOG( AliHLTLog::kDebug, "TimerTest", "Dump    8" )
	<< "Dump    8 ----------------------------------- " << ENDLOG;
    timer.LogData();

    timerID8 = timer.AddTimeout( 5000, &signal, 8 );
    for ( int i = 0; i < 11; i++ )
	timer.AddTimeout( 10000+i*100, &signal, 9+i );
    LOG( AliHLTLog::kDebug, "TimerTest", "Dump    9" )
	<< "Dump    9 ----------------------------------- " << ENDLOG;
    timer.LogData();
    timer.CancelTimeout( timerID8 );
    LOG( AliHLTLog::kDebug, "TimerTest", "Dump    10" )
	<< "Dump    10 ----------------------------------- " << ENDLOG;
    timer.LogData();
    timer.AddTimeout( 5000, &signal, 30 );
    LOG( AliHLTLog::kDebug, "TimerTest", "Dump    11" )
	<< "Dump    11 ----------------------------------- " << ENDLOG;
    timer.LogData();


    timer.AddTimeout( 6000, &signal, 31 );
    LOG( AliHLTLog::kDebug, "TimerTest", "Dump    12" )
	<< "Dump    12 ----------------------------------- " << ENDLOG;
    timer.LogData();



    timerID32 = timer.AddTimeout( 6000, &signal, 32 );
    LOG( AliHLTLog::kDebug, "TimerTest", "Dump    13" )
	<< "Dump    13 ----------------------------------- " << ENDLOG;
    timer.LogData();

//     timer.Resize( true );
//     LOG( AliHLTLog::kDebug, "TimerTest", "Dump    14" )
// 	<< "Dump    14 ----------------------------------- " << ENDLOG;
//     timer.LogData();

    usleep( 2500000 );
    timer.TimerExpired();
    LOG( AliHLTLog::kDebug, "TimerTest", "Dump    15" )
	<< "Dump    15 ----------------------------------- " << ENDLOG;
    timer.LogData();

    timer.CancelTimeout( timerID32 );
    LOG( AliHLTLog::kDebug, "TimerTest", "Dump    16" )
	<< "Dump    16 ----------------------------------- " << ENDLOG;
    timer.LogData();


    timer.CancelTimeout( timerID2 );
    LOG( AliHLTLog::kDebug, "TimerTest", "Dump    17" )
	<< "Dump    17 ----------------------------------- " << ENDLOG;
    timer.LogData();

    timer.Resize( true );
    LOG( AliHLTLog::kDebug, "TimerTest", "Dump    18" )
	<< "Dump    18 ----------------------------------- " << ENDLOG;
    timer.LogData();

    timer.Compact( NULL );
    LOG( AliHLTLog::kDebug, "TimerTest", "Dump    19" )
	<< "Dump    19 ----------------------------------- " << ENDLOG;
    timer.LogData();

    timer.Resize( false );
    LOG( AliHLTLog::kDebug, "TimerTest", "Dump    20" )
	<< "Dump    20 ----------------------------------- " << ENDLOG;
    timer.LogData();

    return 0;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
