/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include <fstream>


#define LOGFILE "LogTest.log"

#define LOGMULT 10

int main( int argc, char* argv[] )
	{
	if ( argc!=2 )
		{
		printf( "Usage %s size\n", argv[0] );
		return -1;
		}
	AliHLTDevNullLogServer devNull;
	AliHLTStdoutLogServer sOut;
	AliHLTStderrLogServer eOut;
	ofstream str( LOGFILE );
	AliHLTStreamLogServer strOut( str );
	struct timeval start, stop;
	AliUInt32_t dt;

	AliUInt32_t cnt, i;
	char* err;
	cnt = strtoul( argv[1], &err, 0 );
	if ( *err && !argv[1] )
		{
		printf( "Error in numeric size argument\n" );
		return -1;
		}
	gLogLevel = AliHLTLog::kAll; //  & ~AliHLTLog::kInformational;
	gLog.AddServer( sOut );
	LOG( AliHLTLog::kDebug, "LogBench", "Result" ) << "Test count: " << AliHLTLog::kDec << cnt << ENDLOG;
	LOG( AliHLTLog::kDebug, "LogBench", "Result" ) << "Testing DevNull LogServer..." << ENDLOG;
	gLog.DelServer( sOut );
	gLog.AddServer( devNull );
	gettimeofday( &start, NULL );

	for ( i = 0; i < cnt; i++ )
		{
		LOG( AliHLTLog::kDebug, "LogBench", "Test" ) << "Test log message " << i << ENDLOG;
		}

	gettimeofday( &stop, NULL );
	gLog.DelServer( devNull );
	gLog.AddServer( sOut );
	if ( stop.tv_usec < start.tv_usec )
		{
		stop.tv_usec += 1000000;
		stop.tv_sec -= 1;
		}
	dt = (stop.tv_sec-start.tv_sec)*1000000 + stop.tv_usec-start.tv_usec;
	LOG( AliHLTLog::kDebug, "LogBench", "Result" ) << "Time needed for " << cnt << " log messages: " << dt << " musec - " 
				<< ((double)dt)/cnt << " musec/msg" << ENDLOG;




	LOG( AliHLTLog::kDebug, "LogBench", "Result" ) << "Testing DevNull LogServer with suppressed messages..." << ENDLOG;
	gLog.DelServer( sOut );
	gLog.AddServer( devNull );
	gettimeofday( &start, NULL );

	for ( i = 0; i < cnt; i++ )
		{
		LOG( AliHLTLog::kInformational, "LogBench", "Test" ) << "Test log message " << i << ENDLOG;
		}

	gettimeofday( &stop, NULL );
	gLog.DelServer( devNull );
	gLog.AddServer( sOut );
	if ( stop.tv_usec < start.tv_usec )
		{
		stop.tv_usec += 1000000;
		stop.tv_sec -= 1;
		}
	dt = (stop.tv_sec-start.tv_sec)*1000000 + stop.tv_usec-start.tv_usec;
	LOG( AliHLTLog::kDebug, "LogBench", "Result" ) << "Time needed for " << cnt << " log messages: " << dt << " musec - " 
				<< ((double)dt)/cnt << " musec/msg" << ENDLOG;





	LOG( AliHLTLog::kDebug, "LogBench", "Result" ) << "Testing StdErr LogServer..." << ENDLOG;
	gLog.DelServer( sOut );
	gLog.AddServer( eOut );
	gettimeofday( &start, NULL );

	for ( i = 0; i < cnt; i++ )
		{
		LOG( AliHLTLog::kDebug, "LogBench", "Test" ) << "Test log message " << i << ENDLOG;
		}

	gettimeofday( &stop, NULL );
	gLog.DelServer( eOut );
	gLog.AddServer( sOut );
	if ( stop.tv_usec < start.tv_usec )
		{
		stop.tv_usec += 1000000;
		stop.tv_sec -= 1;
		}
	dt = (stop.tv_sec-start.tv_sec)*1000000 + stop.tv_usec-start.tv_usec;
	LOG( AliHLTLog::kDebug, "LogBench", "Result" ) << "Time needed for " << cnt << " log messages: " << dt << " musec - " 
				<< ((double)dt)/cnt << " musec/msg" << ENDLOG;




	LOG( AliHLTLog::kDebug, "LogBench", "Result" ) << "Testing stream (" << LOGFILE << ") LogServer..." << ENDLOG;
	gLog.DelServer( sOut );
	gLog.AddServer( strOut );
	gettimeofday( &start, NULL );

	for ( i = 0; i < cnt; i++ )
		{
		LOG( AliHLTLog::kDebug, "LogBench", "Test" ) << "Test log message " << i << ENDLOG;
		}

	gettimeofday( &stop, NULL );
	gLog.DelServer( strOut );
	gLog.AddServer( sOut );
	if ( stop.tv_usec < start.tv_usec )
		{
		stop.tv_usec += 1000000;
		stop.tv_sec -= 1;
		}
	dt = (stop.tv_sec-start.tv_sec)*1000000 + stop.tv_usec-start.tv_usec;
	LOG( AliHLTLog::kDebug, "LogBench", "Result" ) << "Time needed for " << cnt << " log messages: " << dt << " musec - " 
				<< ((double)dt)/cnt << " musec/msg" << ENDLOG;


	LOG( AliHLTLog::kDebug, "LogBench", "Result" ) << "Testing stream with modulo messages (" << LOGFILE << ") LogServer..." << ENDLOG;
	gLog.DelServer( sOut );
	gLog.AddServer( strOut );
	gettimeofday( &start, NULL );

	for ( i = 0; i < cnt*LOGMULT; i++ )
		{
		LOGM( AliHLTLog::kDebug, "LogBench", "Test", LOGMULT ) << "Test log message " << i << ENDLOG;
		}

	gettimeofday( &stop, NULL );
	gLog.DelServer( strOut );
	gLog.AddServer( sOut );
	if ( stop.tv_usec < start.tv_usec )
		{
		stop.tv_usec += 1000000;
		stop.tv_sec -= 1;
		}
	dt = (stop.tv_sec-start.tv_sec)*1000000 + stop.tv_usec-start.tv_usec;
	LOG( AliHLTLog::kDebug, "LogBench", "Result" ) << "Time needed for " << cnt*LOGMULT << " log messages: " << dt << " musec - " 
						      << ((double)dt*LOGMULT)/cnt << " musec/msg" << " - Streamed messages: " << cnt 
						      << " - " << ((double)dt)/cnt << " musec/msg" << ENDLOG;



	return 0;
	}
