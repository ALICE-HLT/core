/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSimpleFileEventStorage.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "MLUCString.hpp"
#include "BCLNetworkData.hpp"
#include <cstdlib>

const int kCmdNothing          = 0;
const int kCmdDataDump         = 1;
const int kCmdTriggerDump      = 2;
const int kCmdMetaDataDump     = 3;


int main( int argc, char** argv )
    {
    AliHLTFilteredStderrLogServer sErr;
    gLogLevel = AliHLTLog::kAll;
    gLogLevel &= ~AliHLTLog::kDebug;
    gLog.AddServer( sErr );

//     AliHLTSimpleFileEventStorage storage;
    AliHLTEventStorageInterface* storage = NULL;

    const char* usage = "-storagetype [simplefile] -storagename <storagename> [ -eventid <event-ID> | -eventnr <eventnr> ] [ -datadump (-block <blocknr>) |  -metadatadump (-block <blocknr>) | -triggerdump ] (-V <verbosity>)";
    int i = 1;
    const char* argError = NULL;
    int errorArg = -1;
    char* cpErr;

    const char* storageName=NULL;
    const char* storageType=NULL;
    AliEventID_t eventID( kAliEventTypeUnknown, ~0ULL );
    unsigned long long eventNr = ~(unsigned long long)0;
    unsigned long blockNr = ~(unsigned long)0;
    int cmd = kCmdNothing;

    while ( i<argc && !argError )
	{
	if ( !strcmp( argv[i], "-storagetype" ) )
	    {
	    if ( argc<=i+1 )
		{
		errorArg = i;
		argError = "Missing storage type specifier";
		break;
		}
	    storageType = argv[i+1];
	    if ( !strcmp( argv[i+1], "simplefile" ) )
		{
		storage = new AliHLTSimpleFileEventStorage();
		}
	    else
		{
		errorArg = i+1;
		argError = "Unknown storage type specifier";
		break;
		}
	    if ( !storage )
		{
		errorArg = i+1;
		argError = "Error creating storage object";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-storagename" ) )
	    {
	    if ( argc<=i+1 )
		{
		errorArg = i;
		argError = "Missing storage name specifier";
		break;
		}
	    storageName = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-eventid" ) )
	    {
	    if ( argc<=i+1 )
		{
		errorArg = i;
		argError = "Missing event ID specifier";
		break;
		}
	    eventID = strtoull( argv[i+1], &cpErr, 0 );
	    if ( *cpErr != '\0' )
		{
		errorArg = i+1;
		argError = "Error converting event ID specifier";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-eventnr" ) )
	    {
	    if ( argc<=i+1 )
		{
		errorArg = i;
		argError = "Missing event Nr specifier";
		break;
		}
	    eventNr = strtoull( argv[i+1], &cpErr, 0 );
	    if ( *cpErr != '\0' )
		{
		errorArg = i+1;
		argError = "Error converting event Nr specifier";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-datadump" ) )
	    {
	    if ( cmd != kCmdNothing )
		{
		errorArg = i;
		argError = "Must only specify one command ('-datadump', '-metadatadump' or '-triggerdump')";
		break;
		}
	    cmd = kCmdDataDump;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-metadatadump" ) )
	    {
	    if ( cmd != kCmdNothing )
		{
		errorArg = i;
		argError = "Must only specify one command ('-datadump', '-metadatadump' or '-triggerdump')";
		break;
		}
	    cmd = kCmdMetaDataDump;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-triggerdump" ) )
	    {
	    if ( cmd != kCmdNothing )
		{
		errorArg = i;
		argError = "Must only specify one command ('-datadump', '-metadatadump' or '-triggerdump')";
		break;
		}
	    cmd = kCmdTriggerDump;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-block" ) )
	    {
	    if ( argc<=i+1 )
		{
		errorArg = i;
		argError = "Missing block Nr specifier";
		break;
		}
	    blockNr = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr != '\0' )
		{
		errorArg = i+1;
		argError = "Error converting block Nr specifier";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc<=i+1 )
		{
		errorArg = i;
		argError = "Missing verbosity specifier";
		break;
		}
	    gLogLevel = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr != '\0' )
		{
		errorArg = i+1;
		argError = "Error converting verbosity specifier";
		break;
		}
	    i += 2;
	    continue;
	    }
	errorArg = i;
	argError = "Unknown argument";
	}
    if ( !argError )
	{
	if ( !storage || !storageType )
	    argError = "Storage type must be specified (using the '-storagetype' option')";
	else if ( !storageName )
	    argError = "Storage name must be specified (using the '-storagename' option')";
	else if ( cmd == kCmdNothing )
	    argError = "One command ('-datadump', '-metadatadump' or '-triggerdump') must be specified";
	else if ( eventID == AliEventID_t( kAliEventTypeUnknown, ~0ULL ) && eventNr == ~(unsigned long long)0 )
	    argError = "'-eventid' or '-eventnr' must be specified";
	else if ( cmd == kCmdTriggerDump && blockNr != ~(unsigned long)0 )
	    argError = "'-block' option must only be specified together with '-datadump' or '-metadatadump' command";
	}
    if ( argError )
	{
	LOG( AliHLTLog::kError, "EventStorageExtractor", "Arguments" )
	    << "Usage: " << argv[0] << usage << "." << ENDLOG;
	LOG( AliHLTLog::kError, "EventStorageExtractor", "Arguments" )
	    << "Error: " << argError << "." << ENDLOG;
	if ( errorArg != -1 && errorArg < argc )
	    {
	    LOG( AliHLTLog::kError, "EventStorageExtractor", "Arguments" )
		<< "Offending argument: " << argv[errorArg] << AliHLTLog::kDec << "( argument " << errorArg 
		<< ")." << ENDLOG;
	    }
	return -1;
	}

    int ret;
    
    ret = storage->OpenStorage( storageName );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "EventStorageExtractor", "Error Opening Storage" )
	    << "Error opening '" << storageType << "' storage '" << storageName
	    << "': " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << ")." << ENDLOG;
	return -1;
	}

    AliHLTEventStorageInterface::EventLocationDescriptor* eld = NULL;
    eld = storage->AllocEventLocation();
    if ( !eld )
	{
	LOG( AliHLTLog::kError, "EventStorageExtractor", "Out of memory" )
	    << "Out of memory trying to allocate event location descriptor from storage." << ENDLOG;
	storage->CloseStorage();
	return -1;
	}

//     LOG( AliHLTLog::kDebug, "EventStorageExtractor", "Event Nr" )
// 	<< "Event Nr: 0x" << AliHLTLog::kHex << eventNr
// 	<< " (" << AliHLTLog::kDec << eventNr << ")." << ENDLOG;
    if ( eventID != AliEventID_t( kAliEventTypeUnknown, ~0ULL ) )
	{
	ret = storage->FindEventLocation( eventID, eld );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "EventStorageExtractor", "Event not found" )
		<< "Error trying to determine location of event with ID 0x" << AliHLTLog::kHex
		<< eventID << " (" << AliHLTLog::kDec << eventID << ") in '" << storageType
		<< " 'storage '" << storageName << ": " << strerror(ret) << " ("
		<< ret << ")." << ENDLOG;
	    return -1;
	    }
	}
    else if ( eventNr != ~(unsigned long long)0 )
	{
	ret = storage->GetNextEventLocation( eld, eventNr );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "EventStorageExtractor", "Event not found" )
		<< "Error trying to determine location of event " << AliHLTLog::kDec
		<< eventID << " (0x" << AliHLTLog::kHex << eventID << ") in '" << storageType
		<< " 'storage '" << storageName << ": " << strerror(ret) << " ("
		<< ret << ")." << ENDLOG;
	    storage->CloseStorage();
	    return -1;
	    }
	}

//     storage->DumpEventLocation( AliHLTLog::kDebug, "Event Location", eld );
    
    switch ( cmd )
	{
	case kCmdDataDump:
	    {
	    std::vector<AliHLTSubEventDescriptor::BlockData> dataBlocks;
	    ret = storage->ReadEventData( eld, dataBlocks );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "EventStorageExtractor", "Error reading event data" )
		    << "Error reading event data from '" << storageType
		    << " 'storage '" << storageName << ": " << strerror(ret) << " ("
		    << ret << ")." << ENDLOG;
		storage->CloseStorage();
		return -1;
		}
	    unsigned long start = 0;
	    unsigned long end = dataBlocks.size();
	    if ( blockNr != ~(unsigned long)0 )
		{
		start = blockNr;
		end = blockNr+1;
		}
	    if ( end > dataBlocks.size() )
		end = dataBlocks.size();
	    unsigned long n;
	    for ( n = start; n < end; n++ )
		fwrite( dataBlocks[n].fData, dataBlocks[n].fSize, 1, stdout );
	    storage->ReleaseEventData( dataBlocks );
	    break;
	    }
	case kCmdMetaDataDump:
	    {
	    std::vector<AliHLTSubEventDescriptor::BlockData> dataBlocks;
	    AliHLTSubEventDataDescriptor* sedd;
	    AliHLTEventStorageInterface::EventStorageMetaData* esmd;
	    AliHLTNodeID_t tmpNodeID;
	    const char* byteorder;
	    ret = storage->ReadSubEventDataDescriptor( eld, sedd );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "EventStorageExtractor", "Error reading sub-event data descriptor" )
		    << "Error reading sub-event data descriptor from '" << storageType
		    << " 'storage '" << storageName << ": " << strerror(ret) << " ("
		    << ret << ")." << ENDLOG;
		storage->CloseStorage();
		return -1;
		}
	    printf( "fHeader.fLength: 0x%08X (%u)\n", sedd->fHeader.fLength, sedd->fHeader.fLength );
	    printf( "fHeader.fType.fID: 0x%08X (%u)\n", sedd->fHeader.fType.fID, sedd->fHeader.fType.fID );
	    printf( "fHeader.fSubType.fID: 0x%08X (%u)\n", sedd->fHeader.fSubType.fID, sedd->fHeader.fSubType.fID );
	    printf( "fHeader.fVersion: 0x%08X (%d)\n", sedd->fHeader.fVersion, sedd->fHeader.fVersion );
	    const char* eventType = "Unknown";
	    switch ( sedd->fEventID.fType )
		{
		case kAliEventTypeStartOfRun: 
		    eventType = "Start-Of-Run";
		    break;
		case kAliEventTypeData:
		    eventType = "Data";
		    break;
		case kAliEventTypeEndOfRun:
		    eventType = "End-Of-Run";
		    break;
		}
	    printf( "fEventID: Type: %s - Nr: 0x%016LX (%Lu)\n", eventType, 
		    (unsigned long long)sedd->fEventID.fNr, (unsigned long long)sedd->fEventID.fNr );
	    printf( "fEventBirth_s: 0x%08X (%u)\n", sedd->fEventBirth_s, sedd->fEventBirth_s );
	    printf( "fEventBirth_us: 0x%08X (%u)\n", sedd->fEventBirth_us, sedd->fEventBirth_us );
	    printf( "fDataType: %c%c%c%c%c%c%c%c (%c%c%c%c%c%c%c%c) (0x%016LX) (%Lu)\n", 
		    sedd->fDataType.fDescr[0], 
		    sedd->fDataType.fDescr[1], 
		    sedd->fDataType.fDescr[2], 
		    sedd->fDataType.fDescr[3], 
		    sedd->fDataType.fDescr[4], 
		    sedd->fDataType.fDescr[5], 
		    sedd->fDataType.fDescr[6], 
		    sedd->fDataType.fDescr[7], 
		    sedd->fDataType.fDescr[7], 
		    sedd->fDataType.fDescr[6], 
		    sedd->fDataType.fDescr[5], 
		    sedd->fDataType.fDescr[4], 
		    sedd->fDataType.fDescr[3], 
		    sedd->fDataType.fDescr[2], 
		    sedd->fDataType.fDescr[1], 
		    sedd->fDataType.fDescr[0], 
		    (unsigned long long)sedd->fDataType.fID, 
		    (unsigned long long)sedd->fDataType.fID );
	    printf( "fDataBlockCount: 0x%08X (%u)\n", sedd->fDataBlockCount, sedd->fDataBlockCount );
	    storage->ReleaseSubEventDataDescriptor( sedd );
	    ret = storage->ReadEventStorageMetaData( eld, esmd );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "EventStorageExtractor", "Error reading event storage meta data" )
		    << "Error reading event storage meta data from '" << storageType
		    << " 'storage '" << storageName << ": " << strerror(ret) << " ("
		    << ret << ")." << ENDLOG;
		storage->CloseStorage();
		return -1;
		}
	    printf( "fEventWritten_s: 0x%08X (%u)\n", esmd->fEventWritten_s, esmd->fEventWritten_s );
	    printf( "fEventWritten_us: 0x%08X (%u)\n", esmd->fEventWritten_us, esmd->fEventWritten_us );
	    BCLNetworkData::Transform( esmd->fEventWriteNode, tmpNodeID, kNativeByteOrder, kNetworkByteOrder );
	    printf( "fEventWriteNode: %d.%d.%d.%d (0x%08X) (%u)\n", ((tmpNodeID >> 24) & 0xFF), 
		    ((tmpNodeID >> 16) & 0xFF), 
		    ((tmpNodeID >> 8) & 0xFF), 
		    ((tmpNodeID >> 0) & 0xFF), 
		    esmd->fEventWriteNode, esmd->fEventWriteNode );
	    switch ( esmd->fEventWriteNodeByteOrder )
		{
		case kLittleEndian:
		    byteorder = "Little Endian";
		    break;
		case kBigEndian:
		    byteorder = "Big Endian";
		    break;
		default:
		    byteorder = "Unknown";
		    break;
		}
	    printf( "fEventWriteNodeByteOrder: %s (0x%08X) (%u)\n", byteorder, esmd->fEventWriteNodeByteOrder, esmd->fEventWriteNodeByteOrder );
	    storage->ReleaseEventStorageMetaData( esmd );

	    ret = storage->GetEventBlockData( eld, dataBlocks );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "EventStorageExtractor", "Error getting event block data" )
		    << "Error getting event block data from '" << storageType
		    << " 'storage '" << storageName << ": " << strerror(ret) << " ("
		    << ret << ")." << ENDLOG;
		storage->CloseStorage();
		return -1;
		}
	    unsigned long start = 0;
	    unsigned long end = dataBlocks.size();
	    if ( blockNr != ~(unsigned long)0 )
		{
		start = blockNr;
		end = blockNr+1;
		}
	    if ( end > dataBlocks.size() )
		end = dataBlocks.size();
	    unsigned long n;
	    for ( n = start; n < end; n++ )
		{
		printf( "fDataBlocks[%8lu].fSize: 0x%016LX (%Ld)\n", n, (unsigned long long)dataBlocks[n].fSize, (unsigned long long)dataBlocks[n].fSize );
		BCLNetworkData::Transform( dataBlocks[n].fProducerNode, tmpNodeID, kNativeByteOrder, kNetworkByteOrder );
		printf( "fDataBlocks[%8lu].fProducerNode: %d.%d.%d.%d (0x%08X) (%u)\n", n, ((tmpNodeID >> 24) & 0xFF), 
			((tmpNodeID >> 16) & 0xFF), 
			((tmpNodeID >> 8) & 0xFF), 
			((tmpNodeID >> 0) & 0xFF), 
			dataBlocks[n].fProducerNode, dataBlocks[n].fProducerNode );
		printf( "fDataBlocks[%8lu].fDataType: %c%c%c%c%c%c%c%c (%c%c%c%c%c%c%c%c) (0x%016LX) (%Lu)\n", n, 
			dataBlocks[n].fDataType.fDescr[0], 
			dataBlocks[n].fDataType.fDescr[1], 
			dataBlocks[n].fDataType.fDescr[2], 
			dataBlocks[n].fDataType.fDescr[3], 
			dataBlocks[n].fDataType.fDescr[4], 
			dataBlocks[n].fDataType.fDescr[5], 
			dataBlocks[n].fDataType.fDescr[6], 
			dataBlocks[n].fDataType.fDescr[7], 
			dataBlocks[n].fDataType.fDescr[7], 
			dataBlocks[n].fDataType.fDescr[6], 
			dataBlocks[n].fDataType.fDescr[5], 
			dataBlocks[n].fDataType.fDescr[4], 
			dataBlocks[n].fDataType.fDescr[3], 
			dataBlocks[n].fDataType.fDescr[2], 
			dataBlocks[n].fDataType.fDescr[1], 
			dataBlocks[n].fDataType.fDescr[0], 
			(unsigned long long)dataBlocks[n].fDataType.fID, (unsigned long long)dataBlocks[n].fDataType.fID );
		printf( "fDataBlocks[%8lu].fDataOrigin: %c%c%c%c (%c%c%c%c) (0x%08X) (%u)\n", n, 
			dataBlocks[n].fDataOrigin.fDescr[0], 
			dataBlocks[n].fDataOrigin.fDescr[1], 
			dataBlocks[n].fDataOrigin.fDescr[2], 
			dataBlocks[n].fDataOrigin.fDescr[3], 
			dataBlocks[n].fDataOrigin.fDescr[3], 
			dataBlocks[n].fDataOrigin.fDescr[2], 
			dataBlocks[n].fDataOrigin.fDescr[1], 
			dataBlocks[n].fDataOrigin.fDescr[0], 
			dataBlocks[n].fDataOrigin.fID, dataBlocks[n].fDataOrigin.fID );
		printf( "fDataBlocks[%8lu].fDataSpecification: 0x%08X (%u)\n", n, dataBlocks[n].fDataSpecification, dataBlocks[n].fDataSpecification );
		switch ( dataBlocks[n].fAttributes[kAliHLTSEDBDByteOrderAttributeIndex] )
		    {
		    case kLittleEndian:
			byteorder = "Little Endian";
			break;
		    case kBigEndian:
			byteorder = "Big Endian";
			break;
		    default:
			byteorder = "Unknown";
			break;
		    }
		printf( "fDataBlocks[%8lu] byte order: %s (0x%08X) (%u)\n", n, byteorder, dataBlocks[n].fAttributes[kAliHLTSEDBDByteOrderAttributeIndex], dataBlocks[n].fAttributes[kAliHLTSEDBDByteOrderAttributeIndex] );
		for ( int j = kAliHLTSEDBDAlignmentAttributeIndexStart; j <= kAliHLTSEDBDLastAlignmentAttributeIndex; j++ )
		    printf( "fDataBlocks[%8lu] %s alignment: %u (0x%08X)\n", n, AliHLTSubEventDescriptor::GetTypeForAlignmentIndex(j), (unsigned)dataBlocks[n].fAttributes[j], (unsigned)dataBlocks[n].fAttributes[j] );
		}
	    break;
	    }
	case kCmdTriggerDump:
	    {
	    AliHLTEventTriggerStruct* ets;
	    ret = storage->ReadEventTrigger( eld, ets );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "EventStorageExtractor", "Error reading event trigger data" )
		    << "Error reading event trigger data from '" << storageType
		    << " 'storage '" << storageName << ": " << strerror(ret) << " ("
		    << ret << ")." << ENDLOG;
		storage->CloseStorage();
		return -1;
		}
	    fwrite( ets, ets->fHeader.fLength, 1, stdout );
	    break;
	    }
	default:
	    break;
	}

    storage->FreeEventLocation( eld );
    storage->CloseStorage();
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
