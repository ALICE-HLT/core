/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTTypes.h"
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/time.h>



int main( int argc, char** argv )
    {
    int fd;
    char* usage1 = "Usage: ";
    char* usage2 = " controlpipe-id index value";
    char* cerr;
    AliHLTFilteredStdoutLogServer sOut;
    gLogLevel = AliHLTLog::kAll;
    gLog.AddServer( sOut );

    AliUInt32_t index, value;
    
    if ( argc < 4 )
	{
	LOG( AliHLTLog::kError, "PipeController", "Command line arguments" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	return -1;
	}


    index = strtoul( argv[2], &cerr, 0 );
    if ( *cerr )
	{
	LOG( AliHLTLog::kError, "PipeController", "Command line arguments" )
	    << usage1 << argv[0] << usage2 << ". Error converting index." << ENDLOG;
	return -1;
	}

    value = strtoul( argv[3], &cerr, 0 );
    if ( *cerr )
	{
	LOG( AliHLTLog::kError, "PipeController", "Command line arguments" )
	    << usage1 << argv[0] << usage2 << ". Error converting value." << ENDLOG;
	return -1;
	}


    MLUCString name;
    name = "/tmp/";
    name += argv[1];
    name += "-ControlPipe";
    fd = open ( name.c_str(), O_WRONLY ); // |O_NONBLOCK
    if ( fd == -1 )
	{
	LOG( AliHLTLog::kError, "PipeController", "Pipe open" )
	    << "Error opening pipe " << name.c_str() << ": " 
	    << strerror( errno ) << AliHLTLog::kDec << " (" << errno
	    << ")." << ENDLOG;
	return -1;
	}
    
    write( fd, &index, 4 );
    write( fd, &value, 4 );
    close( fd );

    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
