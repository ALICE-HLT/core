/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTThread.hpp"
#include "AliHLTTimer.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"


class SignalBenchClient
    {
    public:
	
	SignalBenchClient( AliHLTTimerConditionSem* signal ):
	    fSignal( signal ), fSleep( false ), fQuit( false )
		{
		}

	void RunClient();

	void SetSleep()
		{
		fSleep = true;
		}
	void SetQuit()
		{
		fQuit = true;
		}

    protected:

	AliHLTTimerConditionSem* fSignal;
	bool fSleep;
	bool fQuit;
	
    };



class SignalBenchServer
    {
    public:
	
	SignalBenchServer( AliHLTTimerConditionSem* signal, SignalBenchClient* client, unsigned waitIters, unsigned sleepIters ):
	    fSignal( signal ), fClient( client ), fWaitIters( waitIters ), fSleepIters( sleepIters )
		{
		}

	void RunServer();

	unsigned long GetWaitResults()
		{
		return fWaitResult;
		}

	unsigned long GetSleepResults()
		{
		return fSleepResult;
		}

    protected:

	AliHLTTimerConditionSem* fSignal;
	SignalBenchClient* fClient;
	unsigned fWaitIters;
	unsigned fSleepIters;

	unsigned long fWaitResult;
	unsigned long fSleepResult;
	
    };


void SignalBenchClient::RunClient()
    {
    bool unlocked = false;
    fSignal->Lock();
    while ( !fQuit )
	{
	if ( fSleep )
	    {
	    if ( !unlocked )
		{
		fSignal->Unlock();
		unlocked = true;
		}
	    usleep( 1000000 );
	    }
	else
	    {
	    fSignal->Wait();
	    }
	}
    }

void SignalBenchServer::RunServer()
    {
    unsigned i;
    struct timeval tw1, tw2, ts1, ts2;
    LOG( AliHLTLog::kInformational, "SignalBenchServer::RunServer", "Starting Wait Test" )
	<< "Starting wait test..." << ENDLOG;
    gettimeofday( &tw1, NULL );
    for ( i = 0; i < fWaitIters; i++ )
	fSignal->Signal();
    gettimeofday( &tw2, NULL );
    LOG( AliHLTLog::kInformational, "SignalBenchServer::RunServer", "Wait Test Finished" )
	<< "Wait test finished." << ENDLOG;
    fClient->SetSleep();
    LOG( AliHLTLog::kInformational, "SignalBenchServer::RunServer", "Starting Sleep Test" )
	<< "Starting sleep test..." << ENDLOG;
    gettimeofday( &ts1, NULL );
    for ( i = 0; i < fSleepIters; i++ )
	fSignal->Signal();
    gettimeofday( &ts2, NULL );
    LOG( AliHLTLog::kInformational, "SignalBenchServer::RunServer", "Sleep Test Finished" )
	<< "Sleep test finished." << ENDLOG;
    fClient->SetQuit();
    
    fWaitResult  = (tw2.tv_sec-tw1.tv_sec)*1000000 + (tw2.tv_usec-tw1.tv_usec);
    fSleepResult = (ts2.tv_sec-ts1.tv_sec)*1000000 + (ts2.tv_usec-ts1.tv_usec);
    }


int main( int, char** )
    {
    unsigned waitIters = 10000000;
    unsigned sleepIters = 10000000;
    unsigned long waitResults;
    unsigned long sleepResults;

    AliHLTStdoutLogServer outLog;
    gLog.AddServer( outLog );

    AliHLTTimerConditionSem signal;
    signal.Unlock();
    SignalBenchClient client( &signal );
    AliHLTObjectThread<SignalBenchClient> clientThread( &client, &SignalBenchClient::RunClient );
    SignalBenchServer server( &signal, &client, waitIters, sleepIters );

    LOG( AliHLTLog::kInformational, "SignalBench", "Starting Client Thread" )
	<< "Starting client thread and sleeping for 1s now." << ENDLOG;
    clientThread.Start();
    sleep( 1 );
    LOG( AliHLTLog::kInformational, "SignalBench", "Starting Server" )
	<< "Starting server NOW." << ENDLOG;
    
    server.RunServer();

    LOG( AliHLTLog::kInformational, "SignalBench", "Waiting for Client" )
	<< "Waiting for client thread to finish." << ENDLOG;

    clientThread.Join();

    LOG( AliHLTLog::kInformational, "SignalBench", "Collecting results" )
	<< "Collecting results..." << ENDLOG;


    waitResults = server.GetWaitResults();
    sleepResults = server.GetSleepResults();

    double wrate, wtime, srate, stime;
    wrate = ((double)waitIters)/((double)waitResults);
    wrate *= 1000000;
    wtime = ((double)waitResults) / ((double)waitIters);

    srate = ((double)sleepIters)/((double)sleepResults);
    srate *= 1000000;
    stime = ((double)sleepResults) / ((double)sleepIters);
    
    LOG( AliHLTLog::kBenchmark, "SignalBench", "Wait Results" )
	<< "Wait Results: " << AliHLTLog::kDec << waitIters << " wait iterations done in " 
	<< waitResults << " usec. -> " << wrate << " iter/s <-> " << wtime << " usec./iter."
	<< ENDLOG;

    LOG( AliHLTLog::kBenchmark, "SignalBench", "Sleep Results" )
	<< "Sleep Results: " << AliHLTLog::kDec << sleepIters << " sleep iterations done in " 
	<< sleepResults << " usec. -> " << srate << " iter/s <-> " << stime << " usec./iter."
	<< ENDLOG;
    
    return 0;
    }

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
