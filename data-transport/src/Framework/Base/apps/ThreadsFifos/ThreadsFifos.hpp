/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _THREADSFIFOS_HPP_
#define _THREADSFIFOS_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTThreadFIFO.hpp"
#include "AliHLTThread.hpp"

/**
 * A class for testing the threads and fifos implementation
 *
 * @short A class for testing the threads and fifos implementation
 * @author $Author$ - Initial version by Timm Morten Steinbeck
 * @version $Id$
 */
class AliHLTThreadsFifosThread: public AliHLTThread
	{
	public:

		AliHLTThreadsFifosThread( AliUInt32_t ndx, AliUInt32_t max, AliUInt32_t blkMax, AliUInt32_t bufferSize );

		~AliHLTThreadsFifosThread();

		void SetNext( AliHLTThreadsFifosThread* next );

		AliUInt32_t GetCnt() const
			{
			return fCount;
			}

		AliUInt32_t GetInBlkCnt() const
			{
			return fInBlkCnt;
			}

		AliUInt32_t GetOutBlkCnt() const
			{
			return fOutBlkCnt;
			}

		virtual void Run();


	protected:

		AliUInt32_t fMax;
		AliUInt32_t fBlkMax;
		AliUInt32_t fOutBlkCnt;
		AliUInt32_t fInBlkCnt;
		AliUInt32_t fCount;
		AliUInt32_t fNdx;
		AliUInt8_t* fBlock;
		AliHLTThreadMsg* fMsg;

		AliHLTThreadsFifosThread* fNext;

	};





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _THREADSFIFOS_HPP_
