/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "ThreadsFifos.hpp"
#include "AliHLTThreadMsg.h"
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
   

AliHLTThreadsFifosThread::AliHLTThreadsFifosThread( AliUInt32_t ndx, AliUInt32_t max, AliUInt32_t blkMax, AliUInt32_t bufferSize ):
	AliHLTThread( bufferSize, 1024 )
	{
	fMax = max;
	fBlkMax = blkMax;
	fCount = 0;
	fInBlkCnt = 0;
	fOutBlkCnt = 0;
	fNext = 0;
	fNdx = ndx;
	fMsg = (AliHLTThreadMsg*)new AliUInt8_t[ fBlkMax+kAliHLTThreadMsgBaseSize ];
	if ( fMsg )
		fBlock = fMsg->fData;
	else
		fBlock = NULL;
	}

AliHLTThreadsFifosThread::~AliHLTThreadsFifosThread()
	{
	if ( fMsg )
		delete [] (AliUInt8_t*)fMsg;
	}

void AliHLTThreadsFifosThread::SetNext( AliHLTThreadsFifosThread* next )
	{
	fNext = next;
	}

void AliHLTThreadsFifosThread::Run()
	{
	if ( !fMsg )
		{
		printf( "Thread %d error  - No block buffer\n", fNdx );
		return;
		}

	AliHLTThreadMsg* msg;
	AliHLTThreadFIFO* nextFIFO = &(fNext->fFifo);
#ifndef NO_WRITING
	int fin, fout;
	char filename[500];
	int ret;
#endif
	AliUInt32_t len;
	AliUInt8_t* data;
	int i;
#ifdef RANDOM_BLOCKSIZE
	double tmp;
#endif
	AliHLTThreadFIFO::TError err = AliHLTThreadFIFO::kOk;
#ifdef USE_READMSG
	AliHLTThreadFIFO::TError rerr;
#endif
	bool wroteEnd = false, gotEnd = false;
	int msgType;
	AliUInt32_t outCnt = 0, inCnt = 0;

#ifndef NO_WRITING
	sprintf( filename, "thr%03d-in.dat", fNdx );
	fin = open( filename, O_WRONLY|O_CREAT|O_TRUNC, 00777 );
	if ( fin==-1 )
		{
		printf( "Thread %d error opening infile %s\n", fNdx, filename );
		return;
		}
	sprintf( filename, "thr%03d-out.dat", fNdx );
	fout = open( filename, O_WRONLY|O_CREAT|O_TRUNC, 00777 );
	if ( fout == -1 )
		{
		close( fin );
		printf( "Thread %d error opening outfile %s\n", fNdx, filename );
		return;
		}
#endif

	do
		{
		if ( wroteEnd || err==AliHLTThreadFIFO::kBufferFull )
			{
#ifdef NO_OUTPUT
			printf( "Thread %u waiting for data\n", fNdx );
#endif
			fFifo.WaitForData();
			}
#ifdef USE_READMSG
		rerr = fFifo.Read( msg );
		if ( rerr==AliHLTThreadFIFO::kOk )
#else
		msg = fFifo.GetNextMsg();
		if ( msg != NULL )
#endif
			{
			msgType = msg->fType;
			if ( msgType==1 )
				{
				len = msg->fLength-kAliHLTThreadMsgBaseSize;
				data = msg->fData;
				fInBlkCnt++;
#ifndef NO_WRITING
				ret = write( fin, data, len );
				if ( ret < (int)len )
					{
					printf( "Thread %d error writing to infile - %d of %d\n", fNdx, ret, len );
					if ( ret >= 0 )
						len = ret;
					else
						len = 0;
					}
#endif
#ifdef NO_OUTPUT
				printf( "Thread %u received block %u with %u bytes\n", fNdx, fInBlkCnt, len );
#endif
				inCnt += len;
				}
			if ( msgType==2 )
				{
#ifdef NO_OUTPUT
				printf( "Thread %u got end\n", fNdx );
#endif
				gotEnd = true;
				}
			if ( msgType==3 )
				{
				gotEnd = wroteEnd = true;
#ifdef NO_OUTPUT
				printf( "Thread %u aborted\n", fNdx );
#endif
				}
#ifdef USE_READMSG
			delete [] (AliUInt8_t*)msg;
#else
			fFifo.FreeNextMsg( msg );
#endif
			}
		else
			{
			msgType = 4;
			}
		if ( fCount < fMax )
			{
			if ( err == AliHLTThreadFIFO::kOk )
				{
				do
					{
#ifdef RANDOM_BLOCKSIZE
					tmp = random();
					tmp /= RAND_MAX;
					tmp *=fBlkMax;
					len = (AliUInt32_t)tmp;
#else
					len = fBlkMax;
#endif
					if ( fCount+len > fMax )
						len = fMax-fCount;
					}
				while ( len <= 0 );
				if ( len > fBlkMax )
					len = fBlkMax;
				if ( len>0 )
					{
					for ( i = 0; i < (int)len; i++ )
						fBlock[i] = (AliUInt8_t)random();
#ifndef NO_WRITING
					ret = write( fout, fBlock, len );
					if ( ret < (int)len )
						{
						printf( "Thread %d error writing to outfile - %d of %d\n", fNdx, ret, len );
						if ( ret >=0 )
							len = ret;
						else
							len = 0;
						}
#endif
					outCnt += len;

					fMsg->fLength = kAliHLTThreadMsgBaseSize+len;
					fMsg->fType = 1;
					}
				}
			else
				len = fMsg->fLength-kAliHLTThreadMsgBaseSize;
			if ( len>0 )
				{
				err = nextFIFO->Write( fMsg );
				if ( err==AliHLTThreadFIFO::kOk )
					{
					fOutBlkCnt++;
					fCount += len;
#ifdef NO_OUTPUT
					printf( "Thread %u sent block %u with %u bytes\n", fNdx, fOutBlkCnt, len );
#endif
					}
				else
					usleep( 10 );
				}
			}
		if ( fCount>=fMax && !wroteEnd )
			{
#ifdef NO_OUTPUT
			printf( "Thread %d finished sending, waiting for end\n", fNdx );
#endif
			fMsg->fLength = kAliHLTThreadMsgBaseSize;
			fMsg->fType = 2;
			err = nextFIFO->Write( fMsg );
			if ( err==AliHLTThreadFIFO::kOk )
				wroteEnd = true;
			else
				{
				printf( "Thread %u could not write message (len : %u) - Reason: ", fNdx, fMsg->fLength );
				switch ( err )
					{
					case AliHLTThreadFIFO::kBufferFull: printf( "buffer full\n" ); break;
					case AliHLTThreadFIFO::kBufferTooSmall: printf( "buffer too small\n" ); break;
					case AliHLTThreadFIFO::kInternalError: printf( "internal error\n" ); break;
					default: printf( "unknown error\n" );
					}
				}
			}
		}
	while ( !wroteEnd || !gotEnd );
#ifndef NO_WRITING
	close( fin );
	close( fout );
#endif
#ifdef NO_OUTPUT
	printf( "Thread %u finished - Sent %u bytes - Received %u bytes\n", fNdx, outCnt, inCnt );
#endif
	}



int main( int argc, char* argv[] )
	{
	srandom( time(NULL) );

	if ( argc<5 )
		{
		printf( "Usage: %s threadcount datasize blksize buffersize\n", argv[0] );
		return -1;
		}
	char* err;

	AliUInt32_t thrdCount;
	thrdCount = strtoul( argv[1], &err, 0 );
	if ( *err!=0 )
		{
		printf( "Usage: %s threadcount datasize blksize buffersize\n", argv[0] );
		printf( "Error in threadcount\n" );
		return -1;
		}

	AliUInt32_t dataSize = strtoul( argv[2], &err, 0 );
	if ( *err!=0 )
		{
		printf( "Usage: %s threadcount datasize blksize buffersize\n", argv[0] );
		printf( "Error in datasize\n" );
		return -1;
		}

	AliUInt32_t blkSize = strtoul( argv[3], &err, 0 );
	if ( *err!=0 )
		{
		printf( "Usage: %s threadcount datasize blksize buffersize\n", argv[0] );
		printf( "Error in blksize\n" );
		return -1;
		}

	AliUInt32_t bufferSize = strtoul( argv[4], &err, 0 );
	if ( *err!=0 )
		{
		printf( "Usage: %s threadcount datasize blksize buffersize\n", argv[0] );
		printf( "Error in buffersize\n" );
		return -1;
		}

	AliHLTThreadsFifosThread** threads;
	
	printf( "Using %d threads, data size %d, block size %d, buffersize in fifos %d\n",
		thrdCount, dataSize, blkSize, bufferSize );
#ifdef USE_READMSG
	printf( "Copying messages...\n" );
#else
	printf( "Not copying messages...\n" );
#endif
	threads = new AliHLTThreadsFifosThread*[ thrdCount ];
	if ( !threads )
		{
		printf( "Error allocating thread pointers\n" );
		return -1;
		}

	AliUInt32_t i, n;
	for ( i = 0; i < thrdCount; i++ )
		{
		threads[i] = new AliHLTThreadsFifosThread( i, dataSize, blkSize, bufferSize );
		if ( !threads[i] )
			{
			printf( "Error allocating thread object %d\n", i );
			for ( n = 0; n < i; n++ )
				delete threads[n];
			delete [] threads;
			return -1;
			}
		}

	for ( i = 0; i < thrdCount; i++ )
		threads[i]->SetNext( threads[ (i+1)%thrdCount ] );

	struct timeval start, stop;

	gettimeofday( &start, NULL );


	for ( i = 0; i < thrdCount; i++ )
		{
		
		if ( threads[i]->Start()!= AliHLTThread::kStarted )
			printf( "Error starting thread %d\n", i );
		else
		printf( "Started thread %d\n", i );
		//threads[i]->Run();
		}

	for ( i = 0; i < thrdCount; i++ )
		{
		if ( threads[i]->Join()!=AliHLTThread::kJoined )
			printf( "Error joining with thread %d\n", i );
		else
			printf( "Joined with thread %d\n", i );
		}

	gettimeofday( &stop, NULL );

	AliUInt32_t dt;
	
	if ( stop.tv_usec < start.tv_usec )
		{
		stop.tv_usec += 1000000;
		stop.tv_sec -= 1;
		}

	dt = (stop.tv_sec-start.tv_sec)*1000000+(stop.tv_usec-start.tv_usec);

	printf( "Wrote %d * %d bytes of data in %u musec\n", thrdCount, dataSize, dt );
	return 0;
	}



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
