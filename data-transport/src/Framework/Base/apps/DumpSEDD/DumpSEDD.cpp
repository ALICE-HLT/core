/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: CheckShm.cpp 2069 2007-07-04 09:06:18Z timm $ 
**
***************************************************************************
*/

#include "MLUCString.hpp"
#include "AliHLTSubEventDataDescriptor.h"
#include <fstream>
#include <iostream>
#include <istream>
#include <ios>
#include <iomanip>
#include <vector>
#include <cstring>
#include <cerrno>
#include <cstdlib>
#include <cstdio>


int main( int argc, char** argv )
    {
    int i=1;
    std::vector<char*> inputFilenames;
    const char* errorStr=NULL;
    int errorArg = -1;
    int errorParam = -1;
    bool onlyOpts=false;
    bool showWords=false;
    bool showFields=false;

    while ( i<argc && !errorStr )
	{
	if ( !strcmp( argv[i], "-" ) )
	    {
	    inputFilenames.push_back( argv[i] );
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "--" ) )
	    {
	    onlyOpts = true;
	    i += 1;
	    continue;
	    }
	if ( !onlyOpts && argv[i][0]=='-' )
	    {
	    // Decode option
	    if ( !strcmp( "-f", argv[i] ) || !strcmp( "--fields", argv[i] ) )
		{
		showFields = true;
		i += 1;
		continue;
		}
	    if ( !strcmp( "-w", argv[i] ) || !strcmp( "--words", argv[i] ) )
		{
		showWords = true;
		i += 1;
		continue;
		}
	    errorStr = "Unknown option";
	    errorArg = i;
	    break;
	    }
	inputFilenames.push_back( argv[i] );
	i += 1;
	continue;
	}

    if ( !errorStr )
	{
	if ( inputFilenames.size()<=0 )
	    errorStr = "No input filename given";
	}
    if ( errorStr )
	{
	std::cerr << "Usage: " << argv[0] << " (-w||--words) (-f|--fields) (-|<input-filename>)+" << std::endl;
	std::cerr << "Error: " << errorStr << std::endl;
	if ( errorArg!=-1 )
	    std::cerr << "Offending argument: '" << argv[errorArg] << "'" << std::endl;
	if ( errorParam!=-1 )
	    std::cerr << "Offending parameter: '" << argv[errorParam] << "'" << std::endl;
	return -1;
	}

    if ( !showFields && !showWords )
	showWords=true;

    AliHLTSubEventDataDescriptor sedd;

    using std::ios_base;
    using std::setfill;
    using std::setw;
    using std::cout;

    std::vector<char*>::iterator inputFilenameIter, inputFilenameEnd;
    inputFilenameIter = inputFilenames.begin();
    inputFilenameEnd = inputFilenames.end();
    while ( inputFilenameIter!=inputFilenameEnd )
	{
	std::istream* input = NULL;
	std::ifstream infile;
	if ( !strcmp( *inputFilenameIter, "-" ) )
	    {
	    input = &std::cin;
	    }
	else
	    {
	    infile.open( *inputFilenameIter, ios_base::in|ios_base::binary );
	    if ( !infile.good() )
		{
		std::cerr << "Unable to open file '" << *inputFilenameIter << "': "
			  << strerror(errno) << " (" << errno << ")" << std::endl;
		inputFilenameIter++;
		continue;
		}
	    input = &infile;
	    }
	input->read( (char*)&sedd, sizeof(sedd) );
	if ( !input->good() )
	    {
	    std::cerr << "Error reading sedd from file '" << *inputFilenameIter << "': "
		      << strerror(errno) << " (" << errno << ")" << std::endl;
	    }
	else
	    {
	    // Have to use printf here, ios::width somehow does not work...
	    printf( "\n" );
	    printf( "Header:\n" );
	    printf( "  Length: %lu (0x%08lX)\n", (unsigned long)sedd.fHeader.fLength,(unsigned long)sedd.fHeader.fLength );
	    printf( "  Type: %c%c%c%c (%lu/0x%08lX)\n",
		    (sedd.fHeader.fType.fDescr[3] ? sedd.fHeader.fType.fDescr[3] : ' '),
		    (sedd.fHeader.fType.fDescr[2] ? sedd.fHeader.fType.fDescr[2] : ' '),
		    (sedd.fHeader.fType.fDescr[1] ? sedd.fHeader.fType.fDescr[1] : ' '),
		    (sedd.fHeader.fType.fDescr[0] ? sedd.fHeader.fType.fDescr[0] : ' '),
		    (unsigned long)sedd.fHeader.fType.fID, (unsigned long)sedd.fHeader.fType.fID );
	    printf( "  SubType: %c%c%c (%lu/0x%08lX)\n",
		    (sedd.fHeader.fSubType.fDescr[2] ? sedd.fHeader.fSubType.fDescr[2] : ' '),
		    (sedd.fHeader.fSubType.fDescr[1] ? sedd.fHeader.fSubType.fDescr[1] : ' '),
		    (sedd.fHeader.fSubType.fDescr[0] ? sedd.fHeader.fSubType.fDescr[0] : ' '),
		    (unsigned long)sedd.fHeader.fSubType.fID, (unsigned long)sedd.fHeader.fSubType.fID );
	    printf( "  Version: %u\n", (unsigned)sedd.fHeader.fVersion );
	    printf( "EventID: %s (%u / 0x%X) / 0x%016LX / %Lu\n", 
		    sedd.fEventID.fType<=kAliEventTypeMax ? kAliEventTypeStrings[sedd.fEventID.fType] : "UNKNOWN",
		    (unsigned)sedd.fEventID.fType, (unsigned)sedd.fEventID.fType, 
		    (unsigned long long)sedd.fEventID.fNr, (unsigned long long)sedd.fEventID.fNr );
	    printf( "Event birth: %u.%06u s\n", sedd.fEventBirth_s, sedd.fEventBirth_us );
	    printf( "Oldest Event birth: %u s\n", sedd.fOldestEventBirth_s );
	    printf( "Datatype: %c%c%c%c%c%c%c%c (%Lu / 0x%016LX)\n",
		    (sedd.fDataType.fDescr[7] ? sedd.fDataType.fDescr[7] : ' '),
		    (sedd.fDataType.fDescr[6] ? sedd.fDataType.fDescr[6] : ' '),
		    (sedd.fDataType.fDescr[5] ? sedd.fDataType.fDescr[5] : ' '),
		    (sedd.fDataType.fDescr[4] ? sedd.fDataType.fDescr[4] : ' '),
		    (sedd.fDataType.fDescr[3] ? sedd.fDataType.fDescr[3] : ' '),
		    (sedd.fDataType.fDescr[2] ? sedd.fDataType.fDescr[2] : ' '),
		    (sedd.fDataType.fDescr[1] ? sedd.fDataType.fDescr[1] : ' '),
		    (sedd.fDataType.fDescr[0] ? sedd.fDataType.fDescr[0] : ' '),
		    (unsigned long long)sedd.fDataType.fID,
		    (unsigned long long)sedd.fDataType.fID );
	    MLUCString statusFlagStr;
	    statusFlagStr = "";
	    if ( sedd.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_DATA_CORRUPT )
		statusFlagStr += "CORRUPT ";
	    if ( sedd.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_PASSTHROUGH )
		statusFlagStr += "PASSTHROUGH ";
	    if ( sedd.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_FULLPASSTHROUGH )
		statusFlagStr += "FULLPASSTHROUGH ";
	    if ( sedd.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_MONITOR )
		statusFlagStr += "MONITOR ";
	    if ( sedd.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST )
		statusFlagStr += "BROADCAST ";
	    if ( sedd.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BARRIER )
		statusFlagStr += "BARRIER ";
	    printf( "Status flags: %s( %lu / 0x%08lX)\n", statusFlagStr.c_str(), (unsigned long)sedd.fStatusFlags, (unsigned long)sedd.fStatusFlags );
	    printf( "Data block count: %lu (0x%08lX)\n", (unsigned long)sedd.fDataBlockCount, (unsigned long)sedd.fDataBlockCount );

	    AliHLTSubEventDataBlockDescriptor sedbd;
	    for ( unsigned long nn=0; nn<sedd.fDataBlockCount; nn++ )
		{
		input->read( (char*)&(sedbd), sizeof(sedbd) );
		if ( !input->good() )
		    break;
		printf( "\n" );
		const char* shmIDStr = "UNKNOWN";
		switch ( sedbd.fShmID.fShmType )
		    {
		    case kInvalidShmType: shmIDStr = "invalid"; break;
		    case kBigPhysShmType: shmIDStr = "bigphysarea"; break;
		    case kPhysMemShmType: shmIDStr = "physmem"; break;
		    case kSysVShmType: shmIDStr = "System V"; break;
		    case kSysVShmPrivateType: shmIDStr = "private SystemV"; break;
		    }
		printf( "Data block %8lu Shm ID: %s (%u / 0x%X) / %Lu ( 0x%016LX )\n", nn, 
			shmIDStr, (unsigned)sedbd.fShmID.fShmType, (unsigned)sedbd.fShmID.fShmType,
			(unsigned long long)sedbd.fShmID.fKey.fID, (unsigned long long)sedbd.fShmID.fKey.fID );
		printf( "Data block %8lu size: %u (0x%08X)\n", nn, (unsigned)sedbd.fBlockSize, (unsigned)sedbd.fBlockSize );
		printf( "Data block %8lu offset: %u (0x%08X)\n", nn, (unsigned)sedbd.fBlockOffset, (unsigned)sedbd.fBlockOffset );
		printf( "Data block %8lu producer node: %u.%u.%u.%u (%u / 0x%08X)\n", nn,
			(unsigned)((sedbd.fProducerNode >> 0) & 0xFF), (unsigned)((sedbd.fProducerNode >> 8) & 0xFF), 
			(unsigned)((sedbd.fProducerNode >> 16) & 0xFF), (unsigned)((sedbd.fProducerNode >> 24) & 0xFF), 
			(unsigned)sedbd.fProducerNode, (unsigned)sedbd.fProducerNode );
		printf( "Data block %8lu datatype: %c%c%c%c%c%c%c%c (%Lu / 0x%016LX)\n", nn, 
			(sedbd.fDataType.fDescr[7] ? sedbd.fDataType.fDescr[7] : ' '),
			(sedbd.fDataType.fDescr[6] ? sedbd.fDataType.fDescr[6] : ' '),
			(sedbd.fDataType.fDescr[5] ? sedbd.fDataType.fDescr[5] : ' '),
			(sedbd.fDataType.fDescr[4] ? sedbd.fDataType.fDescr[4] : ' '),
			(sedbd.fDataType.fDescr[3] ? sedbd.fDataType.fDescr[3] : ' '),
			(sedbd.fDataType.fDescr[2] ? sedbd.fDataType.fDescr[2] : ' '),
			(sedbd.fDataType.fDescr[1] ? sedbd.fDataType.fDescr[1] : ' '),
			(sedbd.fDataType.fDescr[0] ? sedbd.fDataType.fDescr[0] : ' '),
			(unsigned long long)sedbd.fDataType.fID,
			(unsigned long long)sedbd.fDataType.fID );
		printf( "Data block %8lu data origin: %c%c%c%c (%u / 0x%016X)\n", nn, 
			(sedbd.fDataOrigin.fDescr[3] ? sedbd.fDataOrigin.fDescr[3] : ' '),
			(sedbd.fDataOrigin.fDescr[2] ? sedbd.fDataOrigin.fDescr[2] : ' '),
			(sedbd.fDataOrigin.fDescr[1] ? sedbd.fDataOrigin.fDescr[1] : ' '),
			(sedbd.fDataOrigin.fDescr[0] ? sedbd.fDataOrigin.fDescr[0] : ' '),
			(unsigned)sedbd.fDataOrigin.fID,
			(unsigned)sedbd.fDataOrigin.fID );
		printf( "Data block %8lu data specification: %u (0x%08X)\n", nn, (unsigned)sedbd.fDataSpecification, (unsigned)sedbd.fDataSpecification );
		statusFlagStr = "";
		if ( sedbd.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_DATA_CORRUPT )
		    statusFlagStr += "CORRUPT ";
		if ( sedbd.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_PASSTHROUGH )
		    statusFlagStr += "PASSTHROUGH ";
		if ( sedbd.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_FULLPASSTHROUGH )
		    statusFlagStr += "FULLPASSTHROUGH ";
		if ( sedbd.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_MONITOR )
		    statusFlagStr += "MONITOR ";
		if ( sedbd.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST )
		    statusFlagStr += "BROADCAST ";
		if ( sedbd.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BARRIER )
		    statusFlagStr += "BARRIER ";
		printf( "Data block %8lu status flags: %s(%u / 0x%08X)\n", nn, statusFlagStr.c_str(), (unsigned)sedbd.fStatusFlags, (unsigned)sedbd.fStatusFlags );
		const char* byteOrderStr = "unknown";
		switch ( (unsigned)sedbd.fAttributes[kAliHLTSEDBDByteOrderAttributeIndex] )
		    {
		    case kAliHLTUnknownByteOrder: byteOrderStr = "unknown"; break;
		    case kAliHLTLittleEndianByteOrder: byteOrderStr = "little endian"; break;
		    case kAliHLTBigEndianByteOrder: byteOrderStr = "big endian (network)"; break;
		    case kAliHLTUnspecifiedByteOrder: byteOrderStr = "unspecified"; break;
		    }
		printf( "Data block %8lu byte order: %s (%u)\n", nn, 
			byteOrderStr, (unsigned)sedbd.fAttributes[kAliHLTSEDBDByteOrderAttributeIndex] );
		printf( "Data block %8lu 64 bit unsigned alignment: %u\n", nn, 
			(unsigned)sedbd.fAttributes[kAliHLTSEDBD64BitAlignmentAttributeIndex] );
		printf( "Data block %8lu 32 bit unsigned alignment: %u\n", nn, 
			(unsigned)sedbd.fAttributes[kAliHLTSEDBD32BitAlignmentAttributeIndex] );
		printf( "Data block %8lu 16 bit unsigned alignment: %u\n", nn, 
			(unsigned)sedbd.fAttributes[kAliHLTSEDBD16BitAlignmentAttributeIndex] );
		printf( "Data block %8lu 8 bit unsigned alignment: %u\n", nn, 
			(unsigned)sedbd.fAttributes[kAliHLTSEDBD8BitAlignmentAttributeIndex] );
		printf( "Data block %8lu float alignment: %u\n", nn, 
			(unsigned)sedbd.fAttributes[kAliHLTSEDBDFloatAlignmentAttributeIndex] );
		printf( "Data block %8lu double alignment: %u\n", nn, 
			(unsigned)sedbd.fAttributes[kAliHLTSEDBDDoubleAlignmentAttributeIndex] );
		
		}
	    
	    }
	
	if ( input == &infile )
	    infile.close();
	input = NULL;
	inputFilenameIter++;
	}
    return 0;
    
    }




/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: CheckShm.cpp 2069 2007-07-04 09:06:18Z timm $ 
**
***************************************************************************
*/
