/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <AliHLTTypes.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main( int argc, char ** argv )
    {
    bool success = true;
    for ( int i = 1; i < argc; i++ )
	{
	int fd;
	fd = open( argv[i], O_RDONLY );
	if ( fd == -1 )
	    {
	    printf( "Cannot open file '%s'.\n", argv[i] );
	    continue;
	    }
	off_t insize;
	insize = lseek( fd, 0, SEEK_END );
	lseek( fd, 0, SEEK_SET );
	AliUInt32_t count;
	int ret = read( fd, &count, 4 );
	if ( ret != 4 )
	    {
	    printf( "Cannot read size specifier from file '%s'.\n", argv[i] );
	    success = false;
	    }
	else
	    {
	    AliUInt32_t size;
	    size = count*sizeof(AliUInt32_t);
	    if ( (unsigned long)insize != (unsigned long)(size+4) )
		{
		printf( "File '%s' is not in old data file format.\n", argv[i] );
		success = false;
		}
	    }
	close( fd );
	}
    if ( success )
	return 0;
    else
	return 1;
    }

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
