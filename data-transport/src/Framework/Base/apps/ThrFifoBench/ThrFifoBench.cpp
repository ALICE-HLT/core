/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTThread.hpp"
#include "AliHLTThreadFIFO.hpp"
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h> 
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>

class BenchThread: public AliHLTThread
	{
	public:

		BenchThread( AliUInt32_t bufferSize ):
			AliHLTThread( bufferSize, 1024 )
			{
			};

		void SetMode( bool latOrBnd, bool master );

		void SetFifo( AliHLTThreadFIFO* fifo );

		void SetSize( AliUInt32_t size )
			{
			fBufferSize = size;
			}

		void SetCount( AliUInt32_t count )
			{
			fCount = count;
			}

		AliUInt32_t GetTime() const
			{
			return fTime;
			}

		AliUInt32_t GetSleepTime() const
			{
			return fSleepTime;
			}

		
		
	protected:

		virtual void Run();

		bool fMode;
		bool fMaster;

		AliHLTThreadFIFO* fNextFifo;

		AliUInt32_t fTime;
		AliUInt32_t fSleepTime;
		AliUInt32_t fCount;

		AliUInt32_t fBufferSize;

	};


void BenchThread::SetMode( bool latOrBnd, bool master )
	{
	fMode = latOrBnd;
	fMaster = master;
	}

void BenchThread::SetFifo( AliHLTThreadFIFO* fifo )
	{
	fNextFifo = fifo;
	}


void BenchThread::Run()
	{
	if ( !fNextFifo )
		return;

	AliHLTThreadMsg* msg;
	AliHLTThreadMsg* rmsg;
	AliUInt8_t* p;
	AliUInt32_t dt;

	p = new AliUInt8_t[ fBufferSize +  kAliHLTThreadMsgBaseSize ];
	if ( !p )
		return;
	msg = (AliHLTThreadMsg*)p;
	fSleepTime = fTime = 0;
	AliHLTThreadFIFO::TError err;


	if ( fMode )
		{
		// latency measurement
		if ( fMaster )
			{
			// Wait for slave to start
#ifndef NO_OUTPUT
			printf( "Master waiting for slave to start...\n" );
#endif
			fFifo.WaitForData();
			err = fFifo.Read( rmsg );
			if ( rmsg )
				delete [] (AliUInt8_t*)rmsg;
			else
				printf( "Master: Error receiving ready message from client %d\n", (int)err );
			msg->fType = 1;
			msg->fLength = fBufferSize+kAliHLTThreadMsgBaseSize;
			AliUInt32_t i;
			struct timeval start, stop;
			gettimeofday( &start, NULL );
			i = 0;

			while ( i < fCount )
				{
#ifndef NO_OUTPUT
				printf( "Master Writing\n" );
#endif
				if ( (err=fNextFifo->Write( msg ))!=AliHLTThreadFIFO::kOk )
					{
					usleep( 5 );
					fSleepTime += 5;
#ifndef NO_OUTPUT
					printf( "Error writing message %u: %d\n", i, (int)err );
#endif
					}
				else
					{
					i++;
#ifndef NO_OUTPUT
					printf( "Master Waiting for data\n" );
#endif
					fFifo.WaitForData();
#ifdef USE_READMSG
					err = fFifo.Read( rmsg );
					if ( rmsg )
						delete [] (AliUInt8_t*)rmsg;
#ifndef NO_OUTPUT
					else
						printf( "Master error reading message: %d\n", (int)err );
#endif
#else
					rmsg = fFifo.GetNextMsg();
#ifndef NO_OUTPUT
					if ( !rmsg )
						printf( "Master error reading message\n" );
#endif
					fFifo.FreeNextMsg( rmsg );
#endif
					}
				}

			gettimeofday( &stop, NULL );
			if ( stop.tv_usec < start.tv_usec )
				{
				stop.tv_usec += 1000000;
				stop.tv_sec -= 1;
				}
			dt = (stop.tv_sec-start.tv_sec)*1000000+(stop.tv_usec-start.tv_usec);
			fTime = dt;
			}
		else
			{
			// slave:
			// Tell master we are ready
			msg->fType = 1;
			msg->fLength = fBufferSize+kAliHLTThreadMsgBaseSize;
			if ( (err=fNextFifo->Write( msg ))!=AliHLTThreadFIFO::kOk )
				printf( "Slave: Error writing ready message to master %d\n", (int)err );
			AliUInt32_t i = 0;
			while ( i < fCount )
				{
#ifndef NO_OUTPUT
				printf( "Slave Waiting for data\n" );
#endif
				fFifo.WaitForData();
#ifdef USE_READMSG
				err = fFifo.Read( rmsg );
#else
				rmsg = fFifo.GetNextMsg();
#endif

				if ( !rmsg )
					printf( "Slave: Error receiving message %u: %d\n", i, (int)err );
				if ( rmsg )
					{
#ifndef NO_OUTPUT
					printf( "Slave writing message\n" );
#endif
					err = fNextFifo->Write( rmsg );
					if ( err!=AliHLTThreadFIFO::kOk )
						printf( "Slave error writing message back to master: %d\n", (int)err );

#ifdef USE_READMSG
					delete [] (AliUInt8_t*)rmsg;
#else
					fFifo.FreeNextMsg( rmsg );
#endif
					i++;
					}
				}
			}
		}
	else
		{
		// bandwidth measurement
		if ( fMaster )
			{
#ifndef NO_OUTPUT
			printf( "Master waiting for slave to start...\n" );
#endif
			fFifo.WaitForData();
			err = fFifo.Read( rmsg );
			if ( rmsg )
				delete [] (AliUInt8_t*)rmsg;
			else
				printf( "Master: Error receiving ready message from client %d\n", (int)err );
			msg->fType = 1;
			msg->fLength = fBufferSize+kAliHLTThreadMsgBaseSize;

			struct timeval start, stop;
			gettimeofday( &start, NULL );
			AliUInt32_t i = 0;

			while ( i < fCount )
				{
#ifndef NO_OUTPUT
				printf( "Master Writing %d\n", i );
#endif
				if ( (err=fNextFifo->Write( msg ))!=AliHLTThreadFIFO::kOk )
					{
					usleep( 5 );
					fSleepTime += 5;
#ifndef NO_OUTPUT
					printf( "Error writing message %u: %d\n", i, (int)err );
#endif
					}
				else
					i++;
				}

			gettimeofday( &stop, NULL );
			if ( stop.tv_usec < start.tv_usec )
				{
				stop.tv_usec += 1000000;
				stop.tv_sec -= 1;
				}
			dt = (stop.tv_sec-start.tv_sec)*1000000+(stop.tv_usec-start.tv_usec);
			fTime = dt;
			do
				{
				usleep( 5 );
				msg->fType = 2;
				msg->fLength = kAliHLTThreadMsgBaseSize;
				err=fNextFifo->Write( msg );
#ifndef NO_OUTPUT
				if ( err==AliHLTThreadFIFO::kOk )
					printf( "Error writing end message to slave: %d\n", err );
#endif
				}
			while ( err!=AliHLTThreadFIFO::kOk );
			}
		else
			{
			int type = 2;
#ifndef NO_OUTPUT
			AliUInt32_t i = 0;
#endif

			msg->fType = 1;
			msg->fLength = fBufferSize+kAliHLTThreadMsgBaseSize;
			if ( (err=fNextFifo->Write( msg ))!=AliHLTThreadFIFO::kOk )
				printf( "Slave: Error writing ready message to master %d\n", (int)err );
			do
				{

				fFifo.WaitForData();
#ifdef USE_READMS
				err = fFifo.Read( rmsg );
#else
				rmsg = fFifo.GetNextMsg();
#endif
				if ( rmsg )
					{
					type = rmsg->fType;
#ifndef NO_OUTPUT
					printf( "Slave read message %d\n", i++ );
#endif
					}

#ifdef USE_READMSG
				delete [] (AliUInt8_t*)rmsg;
#else
				fFifo.FreeNextMsg( rmsg );
#endif
				}
			while ( type!=2 );
			}
		}
	delete [] p;
	}



int main( int argc, char* argv[] )
	{
	if ( argc < 4 )
		{
		printf( "Usage: %s (lat|bnd) number-of-loops messagesize\n", argv[0] );
		return -1;
		}


	char* err;

	err = argv[1];
	while ( *err )
		{
		*err = tolower( *err );
		err++;
		}

	bool mode;
	if ( !strcmp( argv[1], "lat" ) )
		mode = true;
	else
		{
		if ( !strcmp( argv[1], "bnd" ) )
			mode = false;
		else
			{
			printf( "Usage: %s (lat|bnd) number-of-loops messagesize\n", argv[0] );
			return -1;
			}
		}

	printf( "Making %s test...\n", mode ? "latency" : "bandwidth" );

	AliUInt32_t count, size;
	count = strtoul( argv[2], &err, 0 );
	if ( *err!=0 )
		{
		printf( "Error in number-of-loops\n" );
		return -2;
		}
	size = strtoul( argv[3], &err, 0 );
	if ( *err!=0 )
		{
		printf( "Error in messagesize\n" );
 		return -3;
		}

	AliUInt32_t buffer;
	if ( mode )
		buffer = (kAliHLTThreadMsgBaseSize+size/4+1)*2;
	else
		buffer = (kAliHLTThreadMsgBaseSize+size/4+1)*32;

	errno = 0;

	BenchThread master(buffer), slave(buffer);
	master.SetMode( mode, true );
	slave.SetMode( mode, false );
	master.SetSize( size );
	slave.SetSize( size );
	master.SetCount( count );
	slave.SetCount( count );
	master.SetFifo( &slave.fFifo );
	slave.SetFifo( &master.fFifo );
	printf( "Starting...\n" );
#ifndef NO_OUTPUT
	printf( "Starting master\n" );
#endif
	
	master.Start();

#ifndef NO_OUTPUT
	printf( "Starting slave\n" );
#endif
	slave.Start();

#ifndef NO_OUTPUT
	printf( "Joining with master\n" );
#endif
	master.Join();

	printf( "Joined with master\n" );

#ifndef NO_OUTPUT
	printf( "Joining with slave\n" );
#endif
	slave.Join();

	printf( "Joined with slave\n" );

	AliUInt32_t t = master.GetTime();
	AliUInt32_t s = master.GetSleepTime();


	if ( mode )
		{
		printf( "Time needed for %u runs: %u musec (without sleeps: %u musec)\n", count, t, t-s );
		printf( "Average: %f (%f)\n", ((double)t)/count, ((double)(t-s))/count );
		}
	else
		{
		printf( "Time needed for %u*%u==%u bytes: %u musec (without sleeps: %u musec)\n", count, size, count*size, t, t-s );
		printf( "Rate: %f B/s == %f MB/s (%f B/s - %f MB/s)\n", 
			((double)(count*size))*1000000/t, ((double)(count*size))*1000000/t/(1024*1024), 
			((double)(count*size))*1000000/(t-s), ((double)(count*size))*1000000/(t-s)/(1024*1024) );
		printf( "Real Rate (inc. message header): %f B/s == %f MB/s (%f B/s - %f MB/s)\n", 
			((double)(count*(size+kAliHLTThreadMsgBaseSize)))*1000000/t, 
			((double)(count*(size+kAliHLTThreadMsgBaseSize)))*1000000/t/(1024*1024), 
			((double)(count*(size+kAliHLTThreadMsgBaseSize)))*1000000/(t-s), 
			((double)(count*(size+kAliHLTThreadMsgBaseSize)))*1000000/(t-s)/(1024*1024) );

		}
	return 0;
	}



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
