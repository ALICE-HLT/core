/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "Thread.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
   

AliHLTThreadThread::AliHLTThreadThread( AliUInt32_t ndx, AliUInt32_t max, AliUInt32_t bufferSize ):
	AliHLTThread( bufferSize, 1024 )
	{
	fMax = max;
	fNdx = ndx;
	}

void AliHLTThreadThread::Run()
	{
	AliUInt32_t i;
	for ( i = 0; i < fMax; i++ )
		printf( "Thread %02u: Count %u\n", fNdx, i );
	}



int main( int argc, char* argv[] )
	{
	if ( argc<3 )
		{
		printf( "Usage: %s threadcount printcount\n", argv[0] );
		return -1;
		}
	char* err;

	AliUInt32_t thrdCount;
	thrdCount = strtoul( argv[1], &err, 0 );
	if ( *err!=0 )
		{
		printf( "Usage: %s threadcount printcount\n", argv[0] );
		printf( "Error in threadcount\n" );
		return -1;
		}

	AliUInt32_t printcount = strtoul( argv[2], &err, 0 );
	if ( *err!=0 )
		{
		printf( "Usage: %s threadcount printcount\n", argv[0] );
		printf( "Error in printcount\n" );
		return -1;
		}

	/*thrdCount = 1;
	  AliHLTThreadThread thr( 0, printcount, 1000 );
	  
	  thr.Start();
	  printf( "Thread started\n" );
	  thr.Join();
	  printf( "Thread joined\n" );*/

	AliHLTThreadThread** threads;
	
	printf( "Using %d threads, print count %d\n",
		thrdCount, printcount );
	threads = new AliHLTThreadThread*[ thrdCount ];
	if ( !threads )
		{
		printf( "Error allocating thread pointers\n" );
		return -1;
		}
	
	AliUInt32_t i, n;
	for ( i = 0; i < thrdCount; i++ )
		{
		threads[i] = new AliHLTThreadThread( i, printcount, 1000 );
		if ( !threads[i] )
			{
			printf( "Error allocating thread object %d\n", i );
			for ( n = 0; n < i; n++ )
				delete threads[n];
			delete [] threads;
			return -1;
			}
		}
	
	struct timeval start, stop;
	
	gettimeofday( &start, NULL );
	
	
	for ( i = 0; i < thrdCount; i++ )
		{
		if ( threads[i]->Start()!= AliHLTThread::kStarted )
			printf( "Error starting thread %d\n", i );
		else
			printf( "Started thread %d\n", i );
		}
	
	for ( i = 0; i < thrdCount; i++ )
		{
		if ( threads[i]->Join()!=AliHLTThread::kJoined )
			printf( "Error joining with thread %d\n", i );
		else
			printf( "Joined with thread %d\n", i );
		}
	
	gettimeofday( &stop, NULL );
	
	AliUInt32_t dt;
	
	if ( stop.tv_usec < start.tv_usec )
		{
		stop.tv_usec += 1000000;
		stop.tv_sec -= 1;
		}
	
	dt = (stop.tv_sec-start.tv_sec)*1000000+(stop.tv_usec-start.tv_usec);
	
	printf( "Wrote %u * %u iterations in %u musec\n", thrdCount, printcount, dt );
	
	return 0;
	}



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
