/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _THREAD_HPP_
#define _THREAD_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTThreadFIFO.hpp"
#include "AliHLTThread.hpp"

/**
 * A class for testing the threads and fifos implementation
 *
 * @short A class for testing the threads and fifos implementation
 * @author $Author$ - Initial version by Timm Morten Steinbeck
 * @version $Id$
 */
class AliHLTThreadThread: public AliHLTThread
	{
	public:

		AliHLTThreadThread( AliUInt32_t ndx, AliUInt32_t max, AliUInt32_t bufferSize );



	protected:

		virtual void Run();

		AliUInt32_t fNdx;
		AliUInt32_t fMax;

	};





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _THREAD_HPP_
