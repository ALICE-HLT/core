/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#include "AliHLTThreadFIFO.hpp"
#include "AliHLTThreadMsg.h"
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>


int main( int argc, char* argv[] )
    {

    if ( argc<4 )
	{
	printf( "Usage: %s datasize blksize buffersize\n", argv[0] );
	return -1;
	}
    char* cerr;


    AliUInt32_t dataSize = strtoul( argv[1], &cerr, 0 );
    if ( *cerr!=0 )
	{
	printf( "Usage: %s threadcount datasize blksize buffersize\n", argv[0] );
	printf( "Error in datasize\n" );
	return -1;
	}

    AliUInt32_t blkSize = strtoul( argv[2], &cerr, 0 );
    if ( *cerr!=0 )
	{
	printf( "Usage: %s threadcount datasize blksize buffersize\n", argv[0] );
	printf( "Error in blksize\n" );
	return -1;
	}

    AliUInt32_t bufferSize = strtoul( argv[3], &cerr, 0 );
    if ( *cerr!=0 )
	{
	printf( "Usage: %s threadcount datasize blksize buffersize\n", argv[0] );
	printf( "Error in buffersize\n" );
	return -1;
	}


    AliHLTThreadFIFO fifo( bufferSize, 1024 );


    int fin, fout;

    fin = open( "fifo-in.dat", O_WRONLY|O_CREAT|O_TRUNC, 00777 );
    if ( fin==-1 )
	{
	printf( "Error opening infile\n" );
	return -1;
	}
    fout = open( "fifo-out.dat", O_WRONLY|O_CREAT|O_TRUNC, 00777 );
    if ( fout == -1 )
	{
	close( fin );
	printf( "Error opening outfile\n" );
	return -1;
	}



    AliUInt32_t i, count = 0;
    AliHLTThreadMsg* rmsg, *smsg;
    AliUInt8_t* block, *data;

    smsg = (AliHLTThreadMsg*)new AliUInt8_t[ blkSize+kAliHLTThreadMsgBaseSize ];
    if ( smsg ) {
      block = smsg->fData;
    }
    else {
      printf( "Error allocating memory\n" );
      return -1;
    }

    AliUInt32_t len;
    int ret;
    AliUInt32_t blkCnt = 0;
    AliHLTThreadFIFO::TError err = AliHLTThreadFIFO::kOk;
    double tmp;
	
    AliUInt32_t readcount = 0;


    while ( readcount < dataSize )
	{
	printf( "readcount: %lu - dataSize: %lu\n", (unsigned long)readcount, (unsigned long)dataSize );
	rmsg = fifo.GetNextMsg();
	if ( rmsg != NULL )
	    {
	    printf( "rmsg: 0x%08lX\n", (unsigned long)rmsg );
	    if ( rmsg->fType==1 )
		{
		len = rmsg->fLength-kAliHLTThreadMsgBaseSize;
		data = rmsg->fData;
		ret = write( fin, data, len );
		blkCnt++;
		readcount += len;
		if ( ret < (int)len )
		    {
		    printf( "Error writing to infile - %d of %d\n", ret, len );
		    }
		printf( "Received block %u with %u bytes\n", blkCnt, len );
		}
	    else
		printf( "Wrong message type: %lu\n", (unsigned long)rmsg->fType );
	    err = fifo.FreeNextMsg( rmsg );
	    if ( err != AliHLTThreadFIFO::kOk )
		printf( "Error FreeNextMsg: %d\n", err );
	    }
	else
	    printf( "GetNextMsg Error\n" );

	if ( count < dataSize )
	    {
	    if ( err == AliHLTThreadFIFO::kOk )
		{
		tmp = random();
		tmp /= RAND_MAX;
		tmp *=blkSize;
		len = (AliUInt32_t)tmp;
		if ( count+len > dataSize )
		    len = dataSize-count;
		if ( len>0 )
		    {
		    for ( i = 0; i < len; i++ )
			block[i] = (AliUInt8_t)random();
		    ret = write( fout, block, len );
		    if ( ret < (int)len )
			{
			printf( "Error writing to outfile - %d of %d\n", ret, len );
			if ( ret >=0 )
			    len = ret;
			}
		    smsg->fLength = kAliHLTThreadMsgBaseSize+len;
		    smsg->fType = 1;
		    }
		}
	    else
		len = smsg->fLength-kAliHLTThreadMsgBaseSize;
	    if ( len>0 )
		{
		AliHLTThreadMsg* msgP;
		//err = fifo.Write( smsg );
		err = fifo.ReserveSlot( smsg->fLength, msgP );
		if ( err==AliHLTThreadFIFO::kOk )
		    {
		    printf( "msgP: 0x%08lX\n", (unsigned long)msgP );
		    memcpy( msgP, smsg, smsg->fLength );
		    err = fifo.CommitMsg( smsg->fLength );
		    if ( err!=AliHLTThreadFIFO::kOk )
			{
			printf( "Error CommitMsg: %d\n", err );
			}
		    count += len;
		    printf( "Sent %u bytes\n", len );
		    }
		else if ( err==AliHLTThreadFIFO::kBufferTooSmall )
		    {
		    printf( "Error ReserveSlot: Buffer too small for message: length %d\n", len );
		    err = AliHLTThreadFIFO::kOk;
		    }
		else
		    {
		    printf( "Error ReserveSlot: %d\n", err );
		    }
		usleep( 100 );
					}
		}
	    }


	return 0;
	}
