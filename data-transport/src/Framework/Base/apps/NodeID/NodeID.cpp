/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTGetNodeID.hpp"
#include <stdio.h>
#include <string.h>

int main( int, char** )
    {
    AliHLTNodeID_t nodeID;
    unsigned char nodeIDC[5];

    memset( nodeIDC, 0, 5 );
    nodeID = AliHLTGetNodeID();
    memcpy( nodeIDC, &nodeID, 4 );

    printf( "NodeID: 0x%08X - %u\n", nodeID, nodeID );
    printf( "        0x%02X.0x%02X.0x%02X.0x%02X - %3u.%3u.%3u.%3u\n",
	    (unsigned int)nodeIDC[0], (unsigned int)nodeIDC[1], (unsigned int)nodeIDC[2], (unsigned int)nodeIDC[3],
	    (unsigned int)nodeIDC[0], (unsigned int)nodeIDC[1], (unsigned int)nodeIDC[2], (unsigned int)nodeIDC[3] );
    return 0;
    }






/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
