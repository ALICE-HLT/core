/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSharedMemory.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include <vector>
#include <stdlib.h>
#include <stdio.h>


struct Shm
    {
	AliHLTShmID fShmKey;
	AliUInt32_t fBufferSize;
    };
	

int main( int argc, char** argv )
    {
    std::vector<Shm> shms;
    char* cpErr;
    int i = 1;
    const char* errorStr = NULL;
    int errorArg = -1;
    while ( i < argc )
	{
	if ( !strcmp( argv[i], "-shm" ) )
	    {
	    Shm shm;
	    if ( argc <= i+3 )
		{
		errorStr = "Missing shmkey or shmsize parameter.";
		break;
		}
	    if ( !strcmp( argv[i+1], "bigphys" ) )
		shm.fShmKey.fShmType = kBigPhysShmType;
	    else if ( !strcmp( argv[i+1], "physmem" ) )
		shm.fShmKey.fShmType = kPhysMemShmType;
	    else if ( !strcmp( argv[i+1], "sysv" ) )
		shm.fShmKey.fShmType = kSysVShmType;
	    else
		{
		errorArg = i+1;
		errorStr = "Invalid shm type specifier.";
		break;
		}
	    shm.fShmKey.fKey.fID = strtoul( argv[i+2], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorArg = i+2;
		errorStr = "Error converting shmkey specifier.";
		break;
		}
	    shm.fBufferSize = strtoul( argv[i+3], &cpErr, 0 );
	    if ( *cpErr == 'k')
		shm.fBufferSize *= 1024;
	    else if ( *cpErr == 'M')
		shm.fBufferSize *= 1024*1024;
	    else if ( *cpErr == 'G')
		shm.fBufferSize *= 1024*1024*1024;
	    else if ( *cpErr )
		{
		errorArg = i+3;
		errorStr = "Error converting shmsize specifier.";
		break;
		}
	    if ( shm.fBufferSize <= 0 )
		{
		errorArg = i+3;
		errorStr = "shmsize must be greater than zero.";
		break;
		}
	    shms.push_back( shm );
	    i += 4;
	    continue;
	    }
	errorStr = "Unknown argument.";
	break;
	}
    if ( !errorStr )
	{
	if ( shms.size()<=0 )
	    errorStr = "Must specify at least one shared memory segment to check using the -shm argument.";
	}
    if ( errorStr )
	{
	fprintf( stderr, "Usage: %s [ -shm <shmtype> <shmkey> <shmsize> ]+\n", argv[0] );
	fprintf( stderr, "Error: %s\n", errorStr );
	if ( errorArg != -1 && errorArg<argc )
	    fprintf( stderr, "Offending argument: %s\n", argv[errorArg] );
	return -1;
	}

    std::vector<Shm>::iterator shmIter, shmEnd;
    shmIter = shms.begin();
    shmEnd = shms.end();
    bool errorOccured = false;
    AliHLTSharedMemory shmProvider;

    while ( shmIter != shmEnd )
	{
	unsigned long size=shmIter->fBufferSize;
	AliUInt32_t outputShm;
	bool exists = false;
	outputShm = shmProvider.GetShm( shmIter->fShmKey, size, true );
	if ( outputShm == (AliUInt32_t)-1 && !size )
	    {
	    size = 0;
	    outputShm = shmProvider.GetShm( shmIter->fShmKey, size );
	    exists = true;
	    }
	if ( outputShm == (AliUInt32_t)-1 )
	    {
	    const char* shmType = NULL;
	    switch ( shmIter->fShmKey.fShmType )
		{
		case kInvalidShmType:
		    shmType = "invalid";
		    break;
		case kBigPhysShmType:
		    shmType = "bigphys";
		    break;
		case kPhysMemShmType:
		    shmType = "physmem";
		    break;
		case kSysVShmType:
		    shmType = "sysv";
		    break;
		default:
		    shmType = "unknown";
		    break;
		}
	    fprintf( stderr, "Unable to create %s shared memory segment with ID 0x%08LX/%Lu\n", 
		     shmType, (unsigned long long)shmIter->fShmKey.fKey.fID, (unsigned long long)shmIter->fShmKey.fKey.fID );
	    errorOccured = true;
	    shmIter++;
	    continue;
	    }
	if ( exists && size < shmIter->fBufferSize )
	    {
	    const char* shmType = NULL;
	    switch ( shmIter->fShmKey.fShmType )
		{
		case kInvalidShmType:
		    shmType = "invalid";
		    break;
		case kBigPhysShmType:
		    shmType = "bigphys";
		    break;
		case kPhysMemShmType:
		    shmType = "physmem";
		    break;
		case kSysVShmType:
		    shmType = "sysv";
		    break;
		default:
		    shmType = "unknown";
		    break;
		}
	    fprintf( stderr, "Size of existing %s shared memory segment with ID 0x%08LX/%Lu is too small (%lu allocated instead of %lu required)\n", 
		     shmType, (unsigned long long)shmIter->fShmKey.fKey.fID, (unsigned long long)shmIter->fShmKey.fKey.fID, (unsigned long)size, (unsigned long)shmIter->fBufferSize );
	    errorOccured = true;
	    shmIter++;
	    continue;
	    }
	void* ptr;
	ptr = shmProvider.LockShm( outputShm );
	if ( !ptr )
	    {
	    const char* shmType = NULL;
	    switch ( shmIter->fShmKey.fShmType )
		{
		case kInvalidShmType:
		    shmType = "invalid";
		    break;
		case kBigPhysShmType:
		    shmType = "bigphys";
		    break;
		case kPhysMemShmType:
		    shmType = "physmem";
		    break;
		case kSysVShmType:
		    shmType = "sysv";
		    break;
		default:
		    shmType = "unknown";
		    break;
		}
	    fprintf( stderr, "Unable to lock %s shared memory segment with ID 0x%08LX/%Lu\n", 
		     shmType, (unsigned long long)shmIter->fShmKey.fKey.fID, (unsigned long long)shmIter->fShmKey.fKey.fID );
	    errorOccured = true;
	    shmProvider.ReleaseShm( outputShm );
	    shmIter++;
	    continue;
	    }
	shmProvider.UnlockShm( outputShm );
	shmProvider.ReleaseShm( outputShm );
	shmIter++;
	}
    if ( errorOccured )
	return 1;
    else
	return  0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
