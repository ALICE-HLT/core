/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include <fstream>

int main( int, char** )
	{
	AliHLTStdoutLogServer sOut;
	gLogLevel = AliHLTLog::kAll; //  & ~AliHLTLog::kInformational;
	gLog.AddServer( sOut );
	LOG( AliHLTLog::kDebug, "Logtest", "Test" ) << "Test 1" << AliHLTLog::kEnd;
	LOG( AliHLTLog::kDebug, "Logtest", "Test" ) << AliHLTLog::kDec << AliHLTLog::kPrec << 3 << 200 << AliHLTLog::kPrec << 0 << AliHLTLog::kEnd;
	LOG( AliHLTLog::kDebug, "Logtest", "Test" ) << AliHLTLog::kDec << 200 << AliHLTLog::kEnd;
	LOG( AliHLTLog::kDebug, "Logtest", "Test" ) << "0x" << AliHLTLog::kHex << 200 << AliHLTLog::kEnd;
	LOG( AliHLTLog::kDebug, "Logtest", "Test" ) << "0x" << AliHLTLog::kHex << 200U << AliHLTLog::kEnd;
	LOG( AliHLTLog::kDebug, "Logtest", "Test" ) << "0x" << AliHLTLog::kHex << 0x81D1272F << "/" << AliHLTLog::kDec << 0x81D1272F << AliHLTLog::kEnd;
	LOG( AliHLTLog::kDebug, "Logtest", "Test" ) << AliHLTLog::kDec << AliHLTLog::kPrec << 3 << 1000 << AliHLTLog::kEnd;
	LOG( AliHLTLog::kDebug, "Logtest", "Test" ) << 1.11 << AliHLTLog::kEnd;


	LOG( AliHLTLog::kDebug, "Logtest", "Test" ) << "Test 2" << ENDLOG;
	LOG( AliHLTLog::kInformational, "Logtest", "Test" ) << "Test 3" << ENDLOG;
	LOG( AliHLTLog::kWarning, "Logtest", "Test" ) << "Test 4" << ENDLOG;

	return 0;
	}
