/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTBlockHeader.hpp"
#include "AliHLTLog.hpp"

void DumpBlockHeader( AliHLTLog::TLogLevel logLevel, const char* source, const char* severity, const AliHLTBlockHeader* header )
    {
    if ( header )
	{
	LOG( logLevel, source, severity ) << "Block Header: Length: " << AliHLTLog::kDec << header->fLength << " (0x" << AliHLTLog::kHex
					  << header->fLength << ") - Type: 0x" << header->fType.fID << " (" << AliHLTLog::kDec
					  << header->fType.fID << ") '" << (char)header->fType.fDescr[0] << (char)header->fType.fDescr[1] 
					  << (char)header->fType.fDescr[2] << (char)header->fType.fDescr[3] << "' ('" << (char)header->fType.fDescr[3] 
					  << (char)header->fType.fDescr[2] << (char)header->fType.fDescr[1] << (char)header->fType.fDescr[0] 
					  << "') - Subtype: 0x" << header->fSubType.fID << AliHLTLog::kDec << " (" << header->fSubType.fID << ") '" 
					  << (char)header->fSubType.fDescr[0] << (char)header->fSubType.fDescr[1] 
					  << (char)header->fSubType.fDescr[2] << "' ('" << (char)header->fSubType.fDescr[2] << (char)header->fSubType.fDescr[1] 
					  << (char)header->fSubType.fDescr[0] << "') - Version: " << header->fVersion << " (0x" << AliHLTLog::kHex
					  << header->fVersion << ")." << ENDLOG;
	}
    else
	{
	LOG( logLevel, source, severity ) << "Illegal Block Header (NULL pointer)." << ENDLOG;
	}
    }

void DumpBlockHeader( AliHLTLog::TLogLevel logLevel, const char* source, const char* severity, const AliHLTBlockHeader& header )
    {
    DumpBlockHeader( logLevel, source, severity, &header );
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
