/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

// #include "AliHLTLogServer.hpp"
// #include <stdio.h>
// #include <iomanip.h>


// void AliHLTStdoutLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, const char* cdate,
// 				      const char* ctime, AliHLTLog::TLogLevel logLvl, const char* hostname,
// 				      AliUInt32_t ldate, AliUInt32_t ltime_s, AliUInt32_t ltime_us, AliUInt32_t msgNr, const char* line )
// 	{
// 	const char* clvl;
// 	switch ( logLvl )
// 		{
// 		case AliHLTLog::kDebug: clvl = "Debug"; break;
// 		case AliHLTLog::kInformational: clvl = "Information"; break;
// 		case AliHLTLog::kWarning: clvl = "Warning"; break;
// 		case AliHLTLog::kError: clvl = "Error"; break;
// 		case AliHLTLog::kFatal: clvl = "Fatal Error"; break;
// 		default: clvl = "Unknown"; break;
// 		}
// 	printf( "%s %08d.%06d.%06d %10u [%s/%d (%s/%s)] (%s/%s) %s: %s\n", hostname, ldate, ltime_s, ltime_us,
// 		msgNr, file, linenr, cdate, ctime, origin.c_str(), 
// 		keyword.c_str(), clvl, line.c_str() );
// 	}

// void AliHLTStdoutLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, const char* cdate,
// 				      const char* ctime, AliHLTLog::TLogLevel logLvl, const char* hostname,
// 				      AliUInt32_t ldate, AliUInt32_t ltime_s, AliUInt32_t ltime_us, AliUInt32_t msgNr, AliUInt32_t subMsgNr, const char* line )
// 	{
// 	const char* clvl;
// 	switch ( logLvl )
// 		{
// 		case AliHLTLog::kDebug: clvl = "Debug"; break;
// 		case AliHLTLog::kInformational: clvl = "Information"; break;
// 		case AliHLTLog::kWarning: clvl = "Warning"; break;
// 		case AliHLTLog::kError: clvl = "Error"; break;
// 		case AliHLTLog::kFatal: clvl = "Fatal Error"; break;
// 		default: clvl = "Unknown"; break;
// 		}
// 	printf( "%s %08d.%06d.%06d %10u/%10u [%s/%d (%s/%s)] (%s/%s)%s: %s\n", hostname, ldate, ltime_s, ltime_us,
// 		msgNr, subMsgNr, file, linenr, cdate, ctime, origin.c_str(), 
// 		keyword.c_str(), clvl, line.c_str() );
// 	}

// void AliHLTStderrLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, const char* cdate,
// 		      const char* ctime, AliHLTLog::TLogLevel logLvl, const char* hostname,
// 		      AliUInt32_t ldate, AliUInt32_t ltime_s, AliUInt32_t ltime_us, AliUInt32_t msgNr, const char* line )
// 	{
// 	const char* clvl;
// 	switch ( logLvl )
// 		{
// 		case AliHLTLog::kDebug: clvl = "Debug"; break;
// 		case AliHLTLog::kInformational: clvl = "Information"; break;
// 		case AliHLTLog::kWarning: clvl = "Warning"; break;
// 		case AliHLTLog::kError: clvl = "Error"; break;
// 		case AliHLTLog::kFatal: clvl = "Fatal Error"; break;
// 		default: clvl = "Unknown"; break;
// 		}
// 	fprintf( stderr, "%s %08d.%06d.%06d %10u [%s/%d (%s/%s)] (%s/%s)%s: %s\n", hostname, ldate, ltime_s, ltime_us,
// 		msgNr, file, linenr, cdate, ctime, origin.c_str(), 
// 		keyword.c_str(), clvl, line.c_str() );
// 	}

// void AliHLTStderrLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, const char* cdate,
// 		      const char* ctime, AliHLTLog::TLogLevel logLvl, const char* hostname,
// 		      AliUInt32_t ldate, AliUInt32_t ltime_s, AliUInt32_t ltime_us, AliUInt32_t msgNr, AliUInt32_t subMsgNr, const char* line )
// 	{
// 	const char* clvl;
// 	switch ( logLvl )
// 		{
// 		case AliHLTLog::kDebug: clvl = "Debug"; break;
// 		case AliHLTLog::kInformational: clvl = "Information"; break;
// 		case AliHLTLog::kWarning: clvl = "Warning"; break;
// 		case AliHLTLog::kError: clvl = "Error"; break;
// 		case AliHLTLog::kFatal: clvl = "Fatal Error"; break;
// 		default: clvl = "Unknown"; break;
// 		}
// 	fprintf( stderr, "%s %08d.%06d.%06d %10u/%10u [%s/%d (%s/%s)] (%s/%s)%s: %s\n", hostname, ldate, ltime_s, ltime_us,
// 		msgNr, subMsgNr, file, linenr, cdate, ctime, origin.c_str(), 
// 		keyword.c_str(), clvl, line.c_str() );
// 	}




// AliHLTStreamLogServer::AliHLTStreamLogServer( ostream& str ):
// 	fStream( str )
// 	{
// 	}


// void AliHLTStreamLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, const char* cdate,
// 		      const char* ctime, AliHLTLog::TLogLevel logLvl, const char* hostname,
// 		      AliUInt32_t ldate, AliUInt32_t ltime_s, AliUInt32_t ltime_us, AliUInt32_t msgNr, const char* line )
// 	{
// 	const char* clvl;
// 	switch ( logLvl )
// 		{
// 		case AliHLTLog::kDebug: clvl = "Debug"; break;
// 		case AliHLTLog::kInformational: clvl = "Information"; break;
// 		case AliHLTLog::kWarning: clvl = "Warning"; break;
// 		case AliHLTLog::kError: clvl = "Error"; break;
// 		case AliHLTLog::kFatal: clvl = "Fatal Error"; break;
// 		default: clvl = "Unknown"; break;
// 		}
// 	fStream << setfill( '0' ) << hostname << " " << setw( 8 ) << ldate
// 		<< "." << setw( 6 ) << ltime_s << setw( 1 ) << "." << setw( 6 ) << ltime_us << setw( 10 ) << setfill( ' ' ) << msgNr
// 		<< " [" << file << "/" << setw( 0 )
// 		<< linenr << " (" << cdate << "/" << ctime << ")] (" << origin << "/"
// 		<< keyword << ") " << clvl << ": " << line << endl;
// 	}

// void AliHLTStreamLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, const char* cdate,
// 		      const char* ctime, AliHLTLog::TLogLevel logLvl, const char* hostname,
// 		      AliUInt32_t ldate, AliUInt32_t ltime_s, AliUInt32_t ltime_us, AliUInt32_t msgNr, AliUInt32_t subMsgNr, const char* line )
// 	{
// 	const char* clvl;
// 	switch ( logLvl )
// 		{
// 		case AliHLTLog::kDebug: clvl = "Debug"; break;
// 		case AliHLTLog::kInformational: clvl = "Information"; break;
// 		case AliHLTLog::kWarning: clvl = "Warning"; break;
// 		case AliHLTLog::kError: clvl = "Error"; break;
// 		case AliHLTLog::kFatal: clvl = "Fatal Error"; break;
// 		default: clvl = "Unknown"; break;
// 		}
// 	long fl = (fStream.flags() & ios::left) ? 0 : ios::left;
// 	fStream << setfill( '0' ) << hostname << " " << setw( 8 ) << ldate
// 		<< "." << setw( 6 ) << ltime_s << setw( 1 ) << "." <<  setw( 6 ) << ltime_us << setw( 10 ) << setfill( ' ' ) << msgNr
// 		<< "/" << setw( 10 ) << setiosflags( ios::left ) << subMsgNr 
// 		<< resetiosflags( fl ) << " [" << file << "/" << setw( 0 )
// 		<< linenr << " (" << cdate << "/" << ctime << ")] (" << origin << "/"
// 		<< keyword << ") " << clvl << ": " << line << endl;
// 	}





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
