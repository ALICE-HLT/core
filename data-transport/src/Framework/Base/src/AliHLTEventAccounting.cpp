/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTEventAccounting.hpp"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTSubEventDescriptor.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTShmManager.hpp"
#include <netdb.h>
#include <unistd.h>
#include <sys/utsname.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <fstream>
#include <vector>

extern int h_errno;

#define TRACE { LOG( AliHLTLog::kDebug, "AliHLTEventAccounting", "TRACE" ) << "TRACE: " << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG; }


AliHLTEventAccounting::AliHLTEventAccounting( bool printImmediately ):
    fDirectory("."),
    fRun(0),
    fShmMan(NULL),
    fPrintImmediately(printImmediately),
    fFH(-1)
    {
    char myname[ SYS_NMLN+1 ];
    int ret;
    if ( gethostname( myname, SYS_NMLN )==-1 )
	{
	ret = errno;
	LOG( MLUCLog::kError, "AliHLTEventAccounting::AliHLTEventAccounting", "Cannot determine node name" )
	    << "Unable to determine node name: " << strerror(ret) << AliHLTLog::kDec
	    << " (" << ret << ")." << ENDLOG;
	fNode = "UNKNOWN";
	}
    else
	fNode = myname;
    fLastFoundEventNew = fLastFoundEventProcessed = fLastFoundEventFinished = fEvents.begin();
    fEventCount = 0;
    fTrackEventCount = true;
    }

AliHLTEventAccounting::~AliHLTEventAccounting()
    {
    if ( !fPrintImmediately )
	DumpEvents();
    if ( fFH!=-1 )
	close( fFH );
    }

void AliHLTEventAccounting::DumpEvents()
    {
    MLUCMutex::TLocker mutexLocker( fMutex );
#define BUFFER_SIZE 16384
    char buffer[BUFFER_SIZE];

    int fh = OpenFile();

    std::list<TEventData>::iterator iter, end;
    std::list<TEventEvent>::iterator evtIter, evtEnd;
    iter = fEvents.begin();
    end = fEvents.end();
    while ( iter != end )
	{
	snprintf( buffer, BUFFER_SIZE, "Event 0x%016LX/%s:", (unsigned long long)iter->fEventID.fNr, kAliEventTypeStrings[iter->fEventID.fType] );
	unsigned long len = strlen(buffer);
	write( fh, buffer, len );
	WriteTime( fh, " - Event Birth Time: ", iter->fEventBirthTime );
	evtIter = iter->fEvents.begin();
	evtEnd = iter->fEvents.end();
	const char* text=NULL;
	while ( evtIter != evtEnd )
	    {

	    const char* state=NULL;
	    switch ( evtIter->fActionType )
		{
		case kAnnounced:
		    state = "Announced";
		    break;
		case kReceived:
		    state = "Received";
		    break;
		case kProcessed:
		    state = "Processed";
		    break;
		case kForwarded:
		    state = "Forwarded";
		    break;
		case kFinished:
		    state = "Finished";
		    break;
		case kEventDoneData:
		    state = "EventDoneData";
		    break;
		default:
		    state = "UNKNOWN";
		    break;
		}
	    snprintf( buffer, BUFFER_SIZE, " ( %s ", state );
	    write( fh, buffer, strlen(buffer) );
	    switch ( evtIter->fActionType )
		{
		case kAnnounced: // Fallthrough
		case kReceived: // Fallthrough
		case kFinished:
		    snprintf( buffer, BUFFER_SIZE, ", %s ", evtIter->fName.c_str() );
		    write( fh, buffer, strlen(buffer) );
		    break;
		default:
		    break;
		}
	    WriteTime( fh, ", ", evtIter->fTimestamp );
	    switch ( evtIter->fActionType )
		{
		case kAnnounced: // Fallthrough
		case kReceived: // Fallthrough
		case kProcessed:
		    snprintf( buffer, BUFFER_SIZE, ", %u blocks, %lu bytes ", evtIter->fBlockCnt, evtIter->fTotalSize );
		    write( fh, buffer, strlen(buffer) );
		    break;
		case kEventDoneData:
		    snprintf( buffer, BUFFER_SIZE, ", %u words ", evtIter->fBlockCnt );
		    write( fh, buffer, strlen(buffer) );
		    break;
		default:
		    break;
		}
	    text = ")";
	    write( fh, text, strlen(text) );
	    evtIter++;
	    }
	text = "\n";
	write( fh, text, strlen(text) );
	iter++;
	}
    close( fh );

    }

void AliHLTEventAccounting::EventEntered( char const* name, AliEventID_t eventID )
    {
    if ( !fTrackEventCount )
	return;
    MLUCMutex::TLocker eventCountLock( fEventCountMutex );
    ++fEventCount;
    DumpLine( eventID, kEntered, name, 0, fEventCount, 0, 0 );
    }

void AliHLTEventAccounting::EventLeft( char const* name, AliEventID_t eventID )
    {
    if ( !fTrackEventCount )
	return;
    MLUCMutex::TLocker eventCountLock( fEventCountMutex );
    --fEventCount;
    DumpLine( eventID, kLeft, name, 0, fEventCount, 0, 0 );
    }


void AliHLTEventAccounting::NewEvent( char const* name, AliHLTSubEventDataDescriptor const * const sedd )
    {
    TRACE;
    NewEvent( name, sedd, kReceived );
    }

void AliHLTEventAccounting::AnnouncedEvent( char const* name, AliHLTSubEventDataDescriptor const * const sedd )
    {
    TRACE;
    NewEvent( name, sedd, kAnnounced );
    }

	
void AliHLTEventAccounting::NewEvent( char const* name, AliHLTSubEventDataDescriptor const * const sedd, TEventAction state )
    {
    if ( !sedd )
	return;
    TRACE;
    if ( fPrintImmediately )
	{
	unsigned blockCnt = 0;
	unsigned long totalSize = 0;
	if ( fShmMan )
	    {
	    AliHLTSubEventDescriptor descr( sedd );
	    std::vector<AliHLTSubEventDescriptor::BlockData> dataBlocks;
	    descr.Dereference( fShmMan, dataBlocks );
	    std::vector<AliHLTSubEventDescriptor::BlockData>::iterator blkIter, blkEnd;
	    blkIter = dataBlocks.begin();
	    blkEnd = dataBlocks.end();
	    while ( blkIter!=blkEnd )
		{
		++blockCnt;
		totalSize += blkIter->fSize;
		blkIter++;
		}
	    }
	DumpLine( sedd->fEventID, state, name, blockCnt, totalSize );
	return;
	}
    MLUCMutex::TLocker mutexLocker( fMutex );
    std::list<TEventData>::iterator iter;
    //iter = FindEvent( sedd->fEventID, fLastFoundEventNew );
    iter = FindEvent( sedd->fEventID, true );
    if ( iter->fEventBirthTime.tv_sec==0 && iter->fEventBirthTime.tv_usec==0 )
	{
	iter->fEventBirthTime.tv_sec = sedd->fEventBirth_s;
	iter->fEventBirthTime.tv_usec = sedd->fEventBirth_us;
	}

    TEventEvent ee;
    gettimeofday( &ee.fTimestamp, NULL );	 
    ee.fName = name;
    ee.fActionType = state;
    ee.fBlockCnt = 0;
    ee.fTotalSize = 0;
    if ( fShmMan )
	{
	AliHLTSubEventDescriptor descr( sedd );
	std::vector<AliHLTSubEventDescriptor::BlockData> dataBlocks;
	descr.Dereference( fShmMan, dataBlocks );
	std::vector<AliHLTSubEventDescriptor::BlockData>::iterator blkIter, blkEnd;
	blkIter = dataBlocks.begin();
	blkEnd = dataBlocks.end();
	while ( blkIter!=blkEnd )
	    {
	    ++ee.fBlockCnt;
	    ee.fTotalSize += blkIter->fSize;
	    blkIter++;
	    }
	}
    
    iter->fEvents.push_back( ee );
    }

void AliHLTEventAccounting::EventProcessed( AliEventID_t eventID, unsigned outputBlockCnt, unsigned long totalOutputSize )
    {
    TRACE;
    if ( fPrintImmediately )
	{
	DumpLine( eventID, kProcessed, NULL, outputBlockCnt, totalOutputSize );
	return;
	}
    MLUCMutex::TLocker mutexLocker( fMutex );
    std::list<TEventData>::iterator iter;
    //iter = FindEvent( eventID, fLastFoundEventProcessed );
    iter = FindEvent( eventID, true );
    if ( iter != fEvents.end() )
	{
	TEventEvent ee;
	gettimeofday( &ee.fTimestamp, NULL );	 
	ee.fName = "";
	ee.fActionType = kProcessed;
	ee.fBlockCnt = outputBlockCnt;
	ee.fTotalSize = totalOutputSize;
	iter->fEvents.push_back( ee );
	}
    }

void AliHLTEventAccounting::EventForwarded( AliEventID_t eventID )
    {
    TRACE;
    if ( fPrintImmediately )
	{
	DumpLine( eventID, kForwarded, NULL, 0, 0 );
	return;
	}
    MLUCMutex::TLocker mutexLocker( fMutex );
    std::list<TEventData>::iterator iter;
    //iter = FindEvent( eventID, fLastFoundEventProcessed );
    iter = FindEvent( eventID, true );
    if ( iter != fEvents.end() )
	{
	TEventEvent ee;
	gettimeofday( &ee.fTimestamp, NULL );	 
	ee.fName = "";
	ee.fActionType = kForwarded;
	ee.fBlockCnt = 0;
	ee.fTotalSize = 0;
	iter->fEvents.push_back( ee );
	}
    }

void AliHLTEventAccounting::EventFinished( char const* name, AliEventID_t eventID, unsigned forceForward, unsigned forwarded )
    {
    TRACE;
    if ( fPrintImmediately )
	{
	DumpLine( eventID, kFinished, name, 0, 0, forceForward, forwarded );
	return;
	}
    MLUCMutex::TLocker mutexLocker( fMutex );
    std::list<TEventData>::iterator iter;
    //iter = FindEvent( eventID, fLastFoundEventFinished );
    iter = FindEvent( eventID );
    if ( iter != fEvents.end() )
	{
	TEventEvent ee;
	gettimeofday( &ee.fTimestamp, NULL );	 
	ee.fName = name;
	ee.fActionType = kFinished;
	ee.fBlockCnt = 0;
	ee.fTotalSize = 0;
	iter->fEvents.push_back( ee );
	}
    }

void AliHLTEventAccounting::EventDoneData( char const* name, AliEventID_t eventID, unsigned wordCount )
    {
    TRACE;
    if ( fPrintImmediately )
	{
	DumpLine( eventID, kEventDoneData, name, wordCount, 0 );
	return;
	}
    MLUCMutex::TLocker mutexLocker( fMutex );
    std::list<TEventData>::iterator iter;
    //iter = FindEvent( eventID, fLastFoundEventProcessed );
    iter = FindEvent( eventID, true );
    if ( iter != fEvents.end() )
	{
	TEventEvent ee;
	gettimeofday( &ee.fTimestamp, NULL );	 
	ee.fName = name;
	ee.fActionType = kEventDoneData;
	ee.fBlockCnt = wordCount;
	ee.fTotalSize = 0;
	iter->fEvents.push_back( ee );
	}
    }

int AliHLTEventAccounting::OpenFile()
    {
#define BUFFER_SIZE 16384
    char buffer[BUFFER_SIZE];
    char runNr[129];
    snprintf( runNr, 128, "%lu", fRun );
    int ret;
    MLUCString path = fDirectory;
    path += "/Run-";
    path += runNr;
    
    ret = mkdir( path.c_str(), 0777 );
    if ( ret ==-1 && errno != EEXIST )
	{
	ret = errno;
	LOG( MLUCLog::kError, "AliHLTEventAccounting::OpenFile", "Cannot create run directory" )
	    << "Unable to create run " << MLUCLog::kDec << fRun << " directory '"
	    << path.c_str() << "': " << strerror(ret) << AliHLTLog::kDec
	    << " (" << ret << ")." << ENDLOG;
	return -1;
	}
    path += "/";
    path += fNode;
    ret = mkdir( path.c_str(), 0777 );
    if ( ret ==-1 && errno != EEXIST )
	{
	ret = errno;
	LOG( MLUCLog::kError, "AliHLTEventAccounting::OpenFile", "Cannot create node directory" )
	    << "Unable to create node directory '"
	    << path.c_str() << "': " << strerror(ret) << AliHLTLog::kDec
	    << " (" << ret << ")." << ENDLOG;
	return -1;
	}

    struct timeval now;
    gettimeofday( &now, NULL );
    struct tm* t = localtime( &(now.tv_sec) );
    t->tm_mon++;
    t->tm_year += 1900;
    unsigned date = t->tm_year*10000+t->tm_mon*100+t->tm_mday;
    unsigned time_s = t->tm_hour*10000+t->tm_min*100+t->tm_sec;
    unsigned time_us = now.tv_usec;
    snprintf( buffer, BUFFER_SIZE, "%08u.%06u.%06u", date, time_s, time_us );

    path += "/";
    path += fName;
    path += "-";
    path += buffer;
    path += "-EventAccounting.list";
    
    int fh = open( path.c_str(), O_CREAT|O_TRUNC|O_WRONLY, 0666);
    if ( fh==-1 )
	{
	ret = errno;
	LOG( MLUCLog::kError, "AliHLTEventAccounting::OpenFile", "Cannot open output file" )
	    << "Unable to open output file '"
	    << path.c_str() << "': " << strerror(ret) << AliHLTLog::kDec
	    << " (" << ret << ")." << ENDLOG;
	return -1;
	}
    return fh;
    }



void AliHLTEventAccounting::DumpLine( AliEventID_t eventID, TEventAction actionType, const char* name, unsigned blockCnt, unsigned long totalSize, unsigned forceForward, unsigned forwarded )
    {
    struct timeval timestamp;
    gettimeofday( &timestamp, NULL );	 
    MLUCMutex::TLocker mutexLocker( fMutex );
#define BUFFER_SIZE 16384
    char buffer[BUFFER_SIZE];
    unsigned long len=0;

    if ( fFH==-1 )
	{
	fFH = OpenFile();
	if ( fFH==-1 )
	    return;
	}
    
    snprintf( buffer, BUFFER_SIZE, "Event 0x%016LX/%s: ( ", (unsigned long long)eventID.fNr, kAliEventTypeStrings[eventID.fType] );
    len = strlen(buffer);
    write( fFH, buffer, len );
    WriteTime( fFH, "", timestamp );
    const char* state=NULL;
    switch ( actionType )
	{
	case kAnnounced:
	    state = "Announced";
	    break;
	case kReceived:
	    state = "Received";
	    break;
	case kProcessed:
	    state = "Processed";
	    break;
	case kForwarded:
	    state = "Forwarded";
	    break;
	case kFinished:
	    state = "Finished";
	    break;
	case kEventDoneData:
	    state = "EventDoneData";
	    break;
	case kEntered:
	    state = "Entered";
	    break;
	case kLeft:
	    state = "Left";
	    break;
	default:
	    state = "UNKNOWN";
	    break;
	}
    snprintf( buffer, BUFFER_SIZE, " , %s ", state );
    write( fFH, buffer, strlen(buffer) );
    if ( name )
	{
	snprintf( buffer, BUFFER_SIZE, ", %s ", name );
	write( fFH, buffer, strlen(buffer) );
	}
    switch ( actionType )
	{
	case kAnnounced: // Fallthrough
	case kReceived: // Fallthrough
	case kProcessed:
	    snprintf( buffer, BUFFER_SIZE, ", %u blocks, %lu bytes ", blockCnt, totalSize );
	    write( fFH, buffer, strlen(buffer) );
	    break;
	case kFinished:
	    if ( forceForward==1 )
		snprintf( buffer, BUFFER_SIZE, ", forward not forced " );
	    else if ( forceForward==2 )
		snprintf( buffer, BUFFER_SIZE, ", forward forced " );
	    else
		strcpy( buffer, "" ); // snprintf( buffer, BUFFER_SIZE, "" );
	    write( fFH, buffer, strlen(buffer) );
	    if ( forwarded==1 )
		snprintf( buffer, BUFFER_SIZE, ", not forwarded " );
	    else if ( forwarded==2 )
		snprintf( buffer, BUFFER_SIZE, ", forwarded " );
	    else
		strcpy( buffer, "" ); // snprintf( buffer, BUFFER_SIZE, "" );
	    write( fFH, buffer, strlen(buffer) );
	    break;
	case kEventDoneData:
	    snprintf( buffer, BUFFER_SIZE, ", %u words ", blockCnt );
	    write( fFH, buffer, strlen(buffer) );
	    break;
	case kEntered: // Fallthrough
	case kLeft:
	    snprintf( buffer, BUFFER_SIZE, ", %lu events contained ", totalSize );
	    write( fFH, buffer, strlen(buffer) );
	    break;
	default:
	    break;
	}
    const char* text = ")\n";
    write( fFH, text, strlen(text) );
#undef BUFFER_SIZE
    }

void AliHLTEventAccounting::WriteTime( int fh, char const* text, struct timeval const& tv )
    {
    struct tm* t = localtime( &(tv.tv_sec) );
    t->tm_mon++;
    t->tm_year += 1900;
    unsigned date = t->tm_year*10000+t->tm_mon*100+t->tm_mday;
    unsigned time_s = t->tm_hour*10000+t->tm_min*100+t->tm_sec;
    unsigned time_us = tv.tv_usec;
    char buffer[128];
    snprintf( buffer, 128, "%08u.%06u.%06u", date, time_s, time_us );
    unsigned long len;
    len = strlen(text);
    write( fh, text, len );
    len = strlen(buffer);
    write( fh, buffer, len );
    }

#if 0
std::list<AliHLTEventAccounting::TEventData>::iterator AliHLTEventAccounting::FindEvent( AliEventID_t eventID, std::list<TEventData>::iterator& lastFoundEvent )
    {
    std::list<TEventData>::iterator iter, end;
    iter = lastFoundEvent;
    end = fEvents.end();
    while ( iter != end )
	{
	if ( iter->fEventID == eventID )
	    {
	    lastFoundEvent = iter;
	    return lastFoundEvent;
	    }
	iter++;
	}
    iter = fEvents.begin();
    end = lastFoundEvent;
    while ( iter != end )
	{
	if ( iter->fEventID == eventID )
	    {
	    lastFoundEvent = iter;
	    return lastFoundEvent;
	    }
	iter++;
	}
    return fEvents.end();
    }
#endif


std::list<AliHLTEventAccounting::TEventData>::iterator AliHLTEventAccounting::FindEvent( AliEventID_t eventID, bool create )
    {
    std::list<TEventData>::iterator iter, end;
    iter = fEvents.begin();
    end = fEvents.end();
    while ( iter != end )
	{
	if ( iter->fEventID == eventID )
	    {
	    return iter;
	    }
	iter++;
	}
    if ( create )
	{
	TEventData ed;
	ed.fEventID = eventID;
	ed.fEventBirthTime.tv_sec = ed.fEventBirthTime.tv_usec = 0;
	return fEvents.insert( fEvents.end(), ed );
	}
    return fEvents.end();
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
