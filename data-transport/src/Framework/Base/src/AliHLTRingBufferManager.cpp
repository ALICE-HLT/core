/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTRingBufferManager.hpp"
#include "AliHLTLog.hpp"

AliHLTRingBufferManager::AliHLTRingBufferManager( unsigned long blockSize )
    {
    if ( blockSize % gMemoryAlignment )
	blockSize += gMemoryAlignment -(blockSize % gMemoryAlignment);
    fBlockSize = blockSize;
    fBlockCnt = 0;
    fBlockEnd = NULL;
    fNextBlock = NULL;
    pthread_mutex_init( &fBlockMutex, NULL );
    }

AliHLTRingBufferManager::AliHLTRingBufferManager( AliHLTShmID shmKey, unsigned long bufferSize, unsigned long blockSize ):
    AliHLTBufferManager( shmKey, bufferSize )
    {
    if ( bufferSize % gMemoryAlignment )
	bufferSize -= (bufferSize % gMemoryAlignment);
    if ( blockSize % gMemoryAlignment )
	blockSize += gMemoryAlignment -(blockSize % gMemoryAlignment);
    fBlockSize = blockSize;
    fBlockCnt = 0;
    AliUInt32_t bufNdx;
//     fBlockStart = NULL;
    fBlockEnd = NULL;
    fNextBlock = NULL;
    unsigned long n, i;
    bufNdx = GetBufferIndex( shmKey );
    if ( bufNdx == ~(AliUInt32_t)0 )
	{
	LOG( AliHLTLog::kError, "AliHLTRingBufferManager::AliHLTRingBufferManager", "Could not find buffer" )
	    << "Unable to find buffer with shmKey " << AliHLTLog::kDec << shmKey.fShmType
	    << ":" << shmKey.fKey.fID << "/0x" << AliHLTLog::kHex << shmKey.fKey.fID 
	    << "." << ENDLOG;
	return;
	}
    n = bufferSize/fBlockSize;
    for ( i = 0; i < n; i++ )
	{
	TBlockData block;
	block.fBufferNdx = bufNdx;
	block.fOffset = i*fBlockSize;
	block.fUsed = false;
	fBlocks.insert( fBlocks.end(), block );
	fBlockCnt++;
	}
//     fBlockStart = fBlocks.begin();
#if __GNUC__>=3
    fBlockEnd = fBlocks.end().base();
    fNextBlock = fBlocks.begin().base();
#else
    fBlockEnd = fBlocks.end();
    fNextBlock = fBlocks.begin();
#endif
    //     pthread_mutex_init( &fBufferMutex, NULL );
    pthread_mutex_init( &fBlockMutex, NULL );
    }

AliHLTRingBufferManager::~AliHLTRingBufferManager()
    {
    //     pthread_mutex_destroy( &fBufferMutex );
    pthread_mutex_destroy( &fBlockMutex );
    }

bool AliHLTRingBufferManager::AddBuffer( AliHLTShmID shmKey, unsigned long bufferSize )
    {
    pthread_mutex_lock( &fBlockMutex );
    if ( bufferSize % gMemoryAlignment )
	bufferSize -= (bufferSize % gMemoryAlignment);
    InsertBuffer( shmKey, bufferSize );
    AliUInt32_t bufNdx;
    unsigned long i, n;
    bufNdx = GetBufferIndex( shmKey );
    if ( bufNdx == ~(AliUInt32_t)0 )
	{
	pthread_mutex_unlock( &fBlockMutex );
	return false;
	}
//     fBlockStart = NULL;
    fBlockEnd = NULL;
//     fNextBlock = NULL;
    n = bufferSize/fBlockSize;
    for ( i = 0; i < n; i++ )
	{
	TBlockData block;
	block.fBufferNdx = bufNdx;
	block.fOffset = i*fBlockSize;
	block.fUsed = false;
	fBlocks.insert( fBlocks.end(), block );
	fBlockCnt++;
	}
//     fBlockStart = fBlocks.begin();
#if __GNUC__>=3
    fBlockEnd = fBlocks.end().base();
    if ( !fNextBlock )
	fNextBlock = fBlocks.begin().base();
#else
    fBlockEnd = fBlocks.end();
    if ( !fNextBlock )
	fNextBlock = fBlocks.begin();
#endif
    pthread_mutex_unlock( &fBlockMutex );
    return true;
    }

bool AliHLTRingBufferManager::DeleteBuffer( AliHLTShmID shmKey, bool force )
    {
    pthread_mutex_lock( &fBlockMutex );
    AliUInt32_t bufferNdx = GetBufferIndex( shmKey );
    if ( bufferNdx == ~(AliUInt32_t)0 )
	{
	pthread_mutex_unlock( &fBlockMutex );
	return false;
	}

    vector<TBlockData>::iterator iter, end;
    bool found = false;
    bool foundUnused = false;
    do
	{
	found = false;
	iter = fBlocks.begin();
	end = fBlocks.end();
	while ( iter != end )
	    {
	    if ( iter->fBufferNdx == bufferNdx )
		{
		if ( force )
		    {
		    found = true;
		    fBlocks.erase( iter );
		    break;
		    }
		if ( iter->fUsed )
		    {
		    found = true;
		    break;
		    }
		else
		    foundUnused = true;
		}
	    iter++;
	    }
	}
    while ( found && force );
    if ( found )
	{
	LOG( AliHLTLog::kWarning, "AliHLTRingBlockBufferManager::DeleteBuffer", "Cannot delete buffer" )
	    << "Unable to delete buffer. At least one block still uses it and deleting was not enforced." 
	    << ENDLOG;
	}
    else
	{
	if ( !force && foundUnused )
	    {
	    do
		{
		found = false;
		iter = fBlocks.begin();
		end = fBlocks.end();
		while ( iter != end )
		    {
		    if ( iter->fBufferNdx == bufferNdx )
			{
			found = true;
			fBlocks.erase( iter );
			break;
			}
		    iter++;
		    }
		}
	    while ( found );
	    }
	RemoveBuffer( shmKey );
	}
    pthread_mutex_unlock( &fBlockMutex );
    return true;
    }

bool AliHLTRingBufferManager::CanGetBlock()
    {
    if ( fBlockCnt <= 0 )
	return false;
    return true;
    }

bool AliHLTRingBufferManager::GetBlock( AliUInt32_t& bufferNdx, unsigned long& offset,
				       unsigned long& size )
    {
    bool success = false;
    TBlockData* start;
    pthread_mutex_lock( &fBlockMutex );
    if ( !fNextBlock )
	{
	pthread_mutex_unlock( &fBlockMutex );
	return false;
	}
    start = fNextBlock;
    do
	{
	if ( !fNextBlock->fUsed )
	    {
	    fNextBlock->fUsed = true;
	    bufferNdx = fNextBlock->fBufferNdx;
	    offset = fNextBlock->fOffset;
	    success = true;
	    size = fBlockSize;
	    DecreaseFree( fBlockSize );
	    }
	fNextBlock++;
#if __GNUC__>=3
	if ( fNextBlock == fBlockEnd )
	    fNextBlock = fBlocks.begin().base();
#else
	if ( fNextBlock == fBlockEnd )
	    fNextBlock = fBlocks.begin();
#endif
	}
    while ( !success && fNextBlock != start );
    pthread_mutex_unlock( &fBlockMutex );
    if ( !success )
 	{
 	LOG( AliHLTLog::kDebug, "AliHLTRingBufferManager::GetBlock", "Block Not Found" )
 	    << "No block found with size " << AliHLTLog::kDec 
	    << size << " - Block size: " << fBlockSize << "." << ENDLOG;
 	}
    else
	{
	unsigned blcnt = 0;
	for ( unsigned i = 0; i < bufferNdx; i++ )
	    blcnt += fBuffers[i].fBufferSize/fBlockSize;
	blcnt += offset/fBlockSize;
	LOG( AliHLTLog::kDebug, "AliHLTRingBufferManager::GetBlock", "Block count" )
	    << "Block count: " << AliHLTLog::kDec << blcnt << "." << ENDLOG;
	}
    return success;
    }

bool AliHLTRingBufferManager::AdaptBlock( AliUInt32_t, unsigned long,
					 unsigned long )
    {
    return false;
    }

bool AliHLTRingBufferManager::AdaptBlock( AliUInt32_t, unsigned long,
					 unsigned long, unsigned long )
    {
    return false;
    }

bool AliHLTRingBufferManager::ReleaseBlock( AliUInt32_t bufferNr, unsigned long offset, bool )
    {
    unsigned i;
    unsigned bfcnt, blcnt=0;
    pthread_mutex_lock( &fBlockMutex );
    AliUInt32_t bufferNdx = BufferNr2Ndx( bufferNr );
    if ( bufferNdx == ~(AliUInt32_t)0 )
	{
	pthread_mutex_unlock( &fBlockMutex );
	return false;
	}
    bfcnt = fBuffers.size();
    if ( bufferNdx >= bfcnt )
	{
	pthread_mutex_unlock( &fBlockMutex );
	return false;
	}
    LOG( AliHLTLog::kDebug, "AliHLTRingBufferManager::ReleaseBlock", "Real buffer index" )
	<< "Real buffer index: " << AliHLTLog::kDec << bufferNdx << " - passed index: "
	<< bufferNr << "." << ENDLOG;
    for ( i = 0; i < bufferNdx; i++ )
	blcnt += fBuffers[i].fBufferSize/fBlockSize;
    blcnt += offset/fBlockSize;
    LOG( AliHLTLog::kDebug, "AliHLTRingBufferManager::ReleaseBlock", "Block count" )
	<< "Block count: " << AliHLTLog::kDec << blcnt << "." << ENDLOG;
    if ( (fBlocks.begin()+blcnt)->fBufferNdx != bufferNr || (fBlocks.begin()+blcnt)->fOffset != offset )
	{
	LOG( AliHLTLog::kFatal, "AliHLTRingBufferManager::ReleaseBlock", "Internal Error" )
	    << "Internal Error trying to release block." << ENDLOG;
	pthread_mutex_unlock( &fBlockMutex );
	return false;
	}
    (fBlocks.begin()+blcnt)->fUsed = false;
    IncreaseFree( fBlockSize );
    pthread_mutex_unlock( &fBlockMutex );
    return true;
    }

bool AliHLTRingBufferManager::IsBlockFeasible( unsigned long size )
    {
    if ( !AliHLTBufferManager::IsBlockFeasible(size) )
	return false;
    if ( size > fBlockSize )
	return false;
    return true;
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
