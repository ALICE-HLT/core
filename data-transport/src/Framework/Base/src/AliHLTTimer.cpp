/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#define __USE_UNIX98 1

#include "AliHLTLog.hpp"
#include "AliHLTTimer.hpp"
#include <pthread.h>
#include <time.h>
#include <errno.h>


#define USE_STL_ITERATOR_SEARCH

//#define DEBUG_PARANOIA


AliHLTTimer::AliHLTTimer( int timerSlotAllocE2 ):
    fThread( *this )
    {
    //pthread_mutexattr_t ma;
    //pthread_mutexattr_init( &ma );
    //pthread_mutexattr_settype( &ma, PTHREAD_MUTEX_RECURSIVE );
    //pthread_mutex_init( &fAccessSem, &ma );
    //pthread_mutex_init( &fConditionSem, &ma );
    pthread_mutex_init( &fAccessSem, NULL );
    pthread_mutex_init( &fConditionSem, NULL );
    pthread_cond_init( &fCondition, NULL );
    fNextID = 0;
    fQuit = false;
    fQuitted = true;
    fInCallback = false;
    //pthread_mutexattr_destroy( &ma );
    fTimeoutCnt = 0;
    fInvalidsCnt = 0;
    fFirstUsed = fNextFree = 0;
    if ( timerSlotAllocE2 == -1 )
	{
	fTimeoutSize = 0;
	fOriginalTimeoutSize = 0;
	fTimeoutMask = ~(unsigned long)0;
	fTimeoutStart = fTimeouts.begin(); // NULL ?
	}
    else
	{
	fOriginalTimeoutSize = fTimeoutSize = 1 << timerSlotAllocE2;
	fTimeoutMask = fTimeoutSize-1;
	TimeoutData data;
	data.fID = 0;
	//data.fTimeout = timeout_ms;
	data.fTime.tv_sec = 0;
	data.fTime.tv_usec = 0;
	data.fCallback = NULL;
	data.fNotData = 0;
	data.fPriority = 0;
	data.fValid = false;
	fTimeouts.resize( fTimeoutSize, data );
	fTimeoutStart = fTimeouts.begin();
// 	unsigned long n;
// 	for ( n = 0; n < fTimeoutSize; n++ )
// 	    *(fTimeoutStart+n) = data;
	}
    }

AliHLTTimer::~AliHLTTimer()
    {
    pthread_mutex_destroy( &fAccessSem );
    pthread_mutex_destroy( &fConditionSem );
    pthread_cond_destroy( &fCondition );
    }

void AliHLTTimer::Start()
    {
//     gettimeofday( &fTimerStarted, NULL );
    struct timeval tv;
    gettimeofday( &tv, NULL );
    fThread.Start();
//     LOG( AliHLTLog::kDebug, "AliHLTTimer", "Timer started" )
// 	<< "AliHLTTimer started at " << AliHLTLog::kDec << fTimerStarted.tv_sec
// 	<< "." << fTimerStarted.tv_usec << "." << ENDLOG;
    LOG( AliHLTLog::kDebug, "AliHLTTimer", "Timer started" )
	<< "AliHLTTimer started at " << AliHLTLog::kDec << tv.tv_sec
	<< "." << tv.tv_usec << "." << ENDLOG;
    }

void AliHLTTimer::Stop()
    {
    fQuit = true;
    pthread_mutex_lock( &fConditionSem );
    //pthread_cond_signal( &fCondition );
    pthread_cond_broadcast( &fCondition );
    pthread_mutex_unlock( &fConditionSem );
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 10000000;
    gettimeofday( &start, NULL );
    while ( fInCallback || (!fQuitted && deltaT<timeLimit) )
	{
	usleep( 20000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( !fQuitted )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTimer", "Timer not stopped" )
	    << "AliHLTTimer could not be stopped. Aborting thread now..." << ENDLOG;
	fThread.Abort();
	}
    else
	{
	LOG( AliHLTLog::kDebug, "AliHLTTimer", "Timer stopped" )
	    << "AliHLTTimer stopped." << ENDLOG;
	fThread.Join();
	}
    }

AliUInt32_t AliHLTTimer::AddTimeout( AliUInt32_t timeout_ms, AliHLTTimerCallback* callback, AliUInt64_t notData, unsigned int priority )
    {
    bool doSignal;
    TimeoutData data;
    //data.fTimeout = timeout_ms;
    gettimeofday( &data.fTime, NULL );
    data.fTime.tv_sec += (timeout_ms)/1000;
    data.fTime.tv_usec += (timeout_ms%1000)*1000;
    while ( data.fTime.tv_usec >= 1000000 )
	{
	data.fTime.tv_sec++;
	data.fTime.tv_usec -= 1000000;
	}
    data.fCallback = callback;
    data.fNotData = notData;
    data.fPriority = priority;
    data.fValid = true;
    LOG( AliHLTLog::kDebug, "AliHLTTimer", "Timout added" )
	<< "Timeout " << AliHLTLog::kDec << data.fID << "/" << data.fTime.tv_sec << "/" << data.fTime.tv_usec << "/(" << timeout_ms << ")/0x"  
	<< AliHLTLog::kHex << (unsigned long)data.fCallback << "/" << data.fNotData << " added for " << AliHLTLog::kDec << timeout_ms << " ms. ID: " << data.fID 
	<< ENDLOG;
#ifdef DEBUG_PARANOIA
    if ( (int)fInvalidsCnt<0 )
	{
	LOG( AliHLTLog::kFatal, "AliHLTTimer::AddTimeout", "(int)fInvalidsCnt<0" )
	    << "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
	    << fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
	    << fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
	    << " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
// 	fTimeoutCnt += (int)fInvalidsCnt;
// 	fInvalidsCnt = 0;
	}
#endif
    pthread_mutex_lock( &fAccessSem );
    data.fID = fNextID++;
#ifdef DEBUG_PARANOIA
	    if ( fFirstUsed<fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else if ( fFirstUsed==fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != 0 && fTimeoutCnt+fInvalidsCnt != fTimeoutSize )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree+fTimeoutSize-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
#endif
    doSignal = InsertTimeout( data );
#ifdef DEBUG_PARANOIA
    if ( fTimeoutSize )
	{
	    if ( fFirstUsed<fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else if ( fFirstUsed==fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != 0 && fTimeoutCnt+fInvalidsCnt != fTimeoutSize )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree+fTimeoutSize-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	}
#endif
    pthread_mutex_unlock( &fAccessSem );
    if ( doSignal )
	{
	pthread_mutex_lock( &fConditionSem );
	//pthread_cond_signal( &fCondition );
	pthread_cond_broadcast( &fCondition );
	pthread_mutex_unlock( &fConditionSem );
	}
    return data.fID;
    }

unsigned long AliHLTTimer::GetTimeoutCnt()
    {
    unsigned long cnt;
    pthread_mutex_lock( &fAccessSem );
    cnt = fTimeoutCnt;
    pthread_mutex_unlock( &fAccessSem );
    return cnt;
    }

bool AliHLTTimer::CancelTimeout( AliUInt32_t id )
    {
    bool first = false;
    bool found = false;
    vector<TimeoutData>::iterator begin, iter, end;
    pthread_mutex_lock( &fAccessSem );
#ifdef DEBUG_PARANOIA
	    if ( fFirstUsed<fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else if ( fFirstUsed==fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != 0 && fTimeoutCnt+fInvalidsCnt != fTimeoutSize )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree+fTimeoutSize-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
#endif
    // The following is basically implicit in the two later loops...
//     if ( fTimeoutCnt<=0 )
// 	{
// 	pthread_mutex_unlock( &fAccessSem );
// 	return;
// 	}
    if ( !fTimeoutSize )
	{
	begin = iter = fTimeouts.begin();
	end = fTimeouts.end();
	while ( iter != end )
	    {
	    if ( iter->fID == id )
		{
		found = true;
		LOG( AliHLTLog::kDebug, "AliHLTTimer", "Timeout canceled" )
		    << "Timeout " << AliHLTLog::kDec << iter->fID << "/" << iter->fTime.tv_sec << "/" << iter->fTime.tv_usec << "/0x" 
		    << AliHLTLog::kHex << (unsigned long)iter->fCallback << "/" << iter->fNotData << " canceled." << ENDLOG;
		fTimeouts.erase( iter );
		fTimeoutCnt--;
		if ( iter == begin )
		    first = true; // We need to signal to the timer loop, only when the first event
		                  // was changed, which is the one, the timer waits for...
		break;
		}
	    iter++;
	    }
	}
    else
	{
#ifdef TIMER_BINARY_SEARCH
#else
	unsigned long n = fFirstUsed;
	unsigned long i=0;
#ifdef DEBUG_PARANOIA
	if ( (int)fInvalidsCnt<0 )
	    {
	    LOG( AliHLTLog::kFatal, "AliHLTTimer::CancelTimeout", "(int)fInvalidsCnt<0" )
		<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
		<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
		<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
		<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
// 	    fTimeoutCnt += (int)fInvalidsCnt;
// 	    fInvalidsCnt = 0;
	    }
#endif
	while ( i++<(fTimeoutCnt+fInvalidsCnt) && ((fTimeoutStart+n)->fID!=id || !(fTimeoutStart+n)->fValid) )
	    n = (n+1) & fTimeoutMask;
	if ( (fTimeoutStart+n)->fID==id && (fTimeoutStart+n)->fValid )
	    {
	    (fTimeoutStart+n)->fValid = false;
	    found = true;
	    LOG( AliHLTLog::kDebug, "AliHLTTimer", "Timeout canceled" )
		<< "Timeout " << AliHLTLog::kDec << (fTimeoutStart+n)->fID << "/" << (fTimeoutStart+n)->fTime.tv_sec << "/" << (fTimeoutStart+n)->fTime.tv_usec << "/0x" 
		<< AliHLTLog::kHex << (unsigned long)(fTimeoutStart+n)->fCallback << "/" << (fTimeoutStart+n)->fNotData << " canceled." << ENDLOG;
	    if ( n == fFirstUsed )
		{
		fFirstUsed = (fFirstUsed+1) & fTimeoutMask;
		// Skip any invalid timeouts following this one...
		while ( !(fTimeoutStart+fFirstUsed)->fValid && fFirstUsed!=fNextFree )
		    {
		    fFirstUsed = (fFirstUsed+1) & fTimeoutMask;
		    fInvalidsCnt--;
		    }
#ifdef DEBUG_PARANOIA
		if ( (int)fInvalidsCnt<0 )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::CancelTimeout", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
// 		    fTimeoutCnt += (int)fInvalidsCnt;
// 		    fInvalidsCnt = 0;
		    }
#endif
		first = true; // We need to signal to the timer loop, only when the first event
   	                      // was changed, which is the one, the timer waits for...
		}
	    else if ( ((n+1) & fTimeoutMask) == fNextFree )
		{
		fNextFree = n;
		}
	    else
		{
		fInvalidsCnt++;
		}
	    fTimeoutCnt--;
	    if ( (fTimeoutCnt < (fTimeoutSize>>2)) && (fTimeoutSize>fOriginalTimeoutSize) )
		{
		Compact( NULL );
		Resize( false ); // We shrink
		}
	    }
#endif
	}

#ifdef DEBUG_PARANOIA
	    if ( fFirstUsed<fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else if ( fFirstUsed==fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != 0 && fTimeoutCnt+fInvalidsCnt != fTimeoutSize )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree+fTimeoutSize-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
    if ( (int)fInvalidsCnt<0 )
	{
	LOG( AliHLTLog::kFatal, "AliHLTTimer::CancelTimeout", "(int)fInvalidsCnt<0" )
	    << "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
	    << fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
	    << fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
	    << " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
// 	fTimeoutCnt += (int)fInvalidsCnt;
// 	fInvalidsCnt = 0;
	}
#endif

    pthread_mutex_unlock( &fAccessSem );
    if ( first )
	{
	pthread_mutex_lock( &fConditionSem );
	//pthread_cond_signal( &fCondition );
	pthread_cond_broadcast( &fCondition );
	pthread_mutex_unlock( &fConditionSem );
	}
    return found;
    }

bool AliHLTTimer::NewTimeout( AliUInt32_t id, AliUInt32_t new_timeout_ms )
    {
    bool found = false, signal = false;
    TimeoutData newTimeout;
#if __GNUC__>=3
    vector<TimeoutData>::iterator begin, iter, end;
#else
    vector<TimeoutData>::iterator begin = NULL, iter = NULL, end = NULL;
#endif
    pthread_mutex_lock( &fAccessSem );
#ifdef DEBUG_PARANOIA
	    if ( fFirstUsed<fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else if ( fFirstUsed==fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != 0 && fTimeoutCnt+fInvalidsCnt != fTimeoutSize )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree+fTimeoutSize-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
#endif
    if ( !fTimeoutSize )
	{
	begin = iter = fTimeouts.begin();
	end = fTimeouts.end();
	while ( iter != end )
	    {
	    if ( iter->fID == id )
		{
		found = true;
		newTimeout = *iter;
		//newTimeout.fTimeout = new_timeout_ms;
		gettimeofday( &newTimeout.fTime, NULL );
		newTimeout.fTime.tv_sec += (new_timeout_ms)/1000;
		newTimeout.fTime.tv_usec += (new_timeout_ms%1000)*1000;
		LOG( AliHLTLog::kDebug, "AliHLTTimer", "Timout changed" )
		    << "Timeout " << AliHLTLog::kDec << iter->fID << "/" << iter->fTime.tv_sec << "/" << iter->fTime.tv_usec << "/0x" 
		    << AliHLTLog::kHex << (unsigned long)iter->fCallback << "/" << iter->fNotData << " changed to ." 
		    << AliHLTLog::kDec << newTimeout.fID << "/" << newTimeout.fTime.tv_sec << "/" << newTimeout.fTime.tv_usec << "/0x" 
		    << AliHLTLog::kHex << (unsigned long)newTimeout.fCallback << "/" << newTimeout.fNotData
		    << ENDLOG;
		fTimeouts.erase( iter );
		fTimeoutCnt--;
		if ( iter == begin )
		    signal = true;
		break;
		}
	    iter++;
	    }
	}
    else
	{
#ifdef TIMER_BINARY_SEARCH
#else
	unsigned long n = fFirstUsed;
	unsigned long i=0;
	while (  i++<(fTimeoutCnt+fInvalidsCnt) && ((fTimeoutStart+n)->fID!=id || !(fTimeoutStart+n)->fValid) )
	    n = (n+1) & fTimeoutMask;
	if ( (fTimeoutStart+n)->fID==id && (fTimeoutStart+n)->fValid )
	    {
	    found = true;
	    newTimeout = *(fTimeoutStart+n);
	    iter = fTimeoutStart+n;
	    iter->fValid = false;
	    gettimeofday( &newTimeout.fTime, NULL );
	    newTimeout.fTime.tv_sec += (new_timeout_ms)/1000;
	    newTimeout.fTime.tv_usec += (new_timeout_ms%1000)*1000;
	    LOG( AliHLTLog::kDebug, "AliHLTTimer", "Timout changed" )
		<< "Timeout " << AliHLTLog::kDec << iter->fID << "/" << iter->fTime.tv_sec << "/" << iter->fTime.tv_usec << "/0x" 
		<< AliHLTLog::kHex << (unsigned long)iter->fCallback << "/" << iter->fNotData << " changed to ." 
		<< AliHLTLog::kDec << newTimeout.fID << "/" << newTimeout.fTime.tv_sec << "/" << newTimeout.fTime.tv_usec << "/0x" 
		<< AliHLTLog::kHex << (unsigned long)newTimeout.fCallback << "/" << newTimeout.fNotData
		<< ENDLOG;
	    if ( n == fFirstUsed )
		{
		fFirstUsed = (fFirstUsed+1) & fTimeoutMask;
		// Skip any invalid timeouts following this one...
		while ( !(fTimeoutStart+fFirstUsed)->fValid && fFirstUsed!=fNextFree )
		    {
		    fFirstUsed = (fFirstUsed+1) & fTimeoutMask;
		    fInvalidsCnt--;
		    }
#ifdef DEBUG_PARANOIA
		if ( (int)fInvalidsCnt<0 )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::NewTimeout", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
// 		    fTimeoutCnt += (int)fInvalidsCnt;
// 		    fInvalidsCnt = 0;
		    }
#endif
		signal = true;
		}
	    else if ( ((n+1) & fTimeoutMask) == fNextFree )
		{
		fNextFree = n;
		}
	    else
		{
		fInvalidsCnt++;
		}
	    fTimeoutCnt--;
	    }
#endif
	}

#ifdef DEBUG_PARANOIA
	    if ( fFirstUsed<fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else if ( fFirstUsed==fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != 0 && fTimeoutCnt+fInvalidsCnt != fTimeoutSize )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree+fTimeoutSize-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
    if ( (int)fInvalidsCnt<0 )
	{
	LOG( AliHLTLog::kFatal, "AliHLTTimer::NewTimeout", "(int)fInvalidsCnt<0" )
	    << "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
	    << fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
	    << fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
	    << " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
// 	fTimeoutCnt += (int)fInvalidsCnt;
// 	fInvalidsCnt = 0;
	}
#endif
    if ( found )
	{
	if ( InsertTimeout( newTimeout ) )
	    signal = true;
	}
    pthread_mutex_unlock( &fAccessSem );
    if ( signal )
	{
	pthread_mutex_lock( &fConditionSem );
	//pthread_cond_signal( &fCondition );
	pthread_cond_broadcast( &fCondition );
	pthread_mutex_unlock( &fConditionSem );
	}
    return found;
    }

void AliHLTTimer::CancelAllTimeouts( vector<AliUInt64_t>& notificationData )
    {
    CancelTimeoutsFor( NULL, notificationData );
    }

void AliHLTTimer::CancelTimeouts( AliHLTTimerCallback* callback,vector<AliUInt64_t>& notificationData )
    {
    if ( !callback )
	return;
    CancelTimeoutsFor( callback, notificationData );
    }

void AliHLTTimer::CancelTimeoutsFor( AliHLTTimerCallback* callback, vector<AliUInt64_t>& notificationData ) // callback==NULL means all timeouts
    {
    bool found = false;
    bool removed;
    vector<TimeoutData>::iterator begin, iter, end;
    pthread_mutex_lock( &fAccessSem );
    // The following is basically implicit in the two later loops...
//     if ( fTimeoutCnt<=0 )
// 	{
// 	pthread_mutex_unlock( &fAccessSem );
// 	return;
// 	}
    if ( !fTimeoutSize )
	{
	do
	    {
	    removed = false;
	    begin = iter = fTimeouts.begin();
	    end = fTimeouts.end();
	    while ( iter != end )
		{
		if ( !callback || iter->fCallback == callback )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTTimer", "Timeout canceled" )
			<< "Timeout " << AliHLTLog::kDec << iter->fID << "/" << iter->fTime.tv_sec << "/" << iter->fTime.tv_usec << "/0x" 
			<< AliHLTLog::kHex << (unsigned long)iter->fCallback << "/" << iter->fNotData << " canceled." << ENDLOG;
		    notificationData.insert( notificationData.end(), iter->fNotData );
		    fTimeouts.erase( iter );
		    removed = true;
		    fTimeoutCnt--;
		    if ( iter == begin )
			found = true; // We need to signal to the timer loop, only when the first event
		                      // was changed, which is the one, the timer waits for...
		    break;
		    }
		iter++;
		}
	    }
	while ( removed );
	}
    else
	{
#ifdef TIMER_BINARY_SEARCH
#else
	unsigned long n = fFirstUsed;
	unsigned long i=0;
	unsigned long loopCount = fTimeoutCnt+fInvalidsCnt;
	while ( i++<(loopCount) )
	    {
	    if ( (!callback || (fTimeoutStart+n)->fCallback==callback) && (fTimeoutStart+n)->fValid )
		{
		(fTimeoutStart+n)->fValid = false;
		LOG( AliHLTLog::kDebug, "AliHLTTimer", "Timeout canceled" )
		    << "Timeout " << AliHLTLog::kDec << (fTimeoutStart+n)->fID << "/" << (fTimeoutStart+n)->fTime.tv_sec << "/" << (fTimeoutStart+n)->fTime.tv_usec << "/0x" 
		    << AliHLTLog::kHex << (unsigned long)(fTimeoutStart+n)->fCallback << "/" << (fTimeoutStart+n)->fNotData << " canceled." << ENDLOG;
		notificationData.insert( notificationData.end(), (fTimeoutStart+n)->fNotData );
		if ( n == fFirstUsed )
		    {
		    fFirstUsed = (fFirstUsed+1) & fTimeoutMask;
		    // Skip any invalid timeouts following this one...
		    while ( !(fTimeoutStart+fFirstUsed)->fValid && fFirstUsed!=fNextFree )
			{
			fFirstUsed = (fFirstUsed+1) & fTimeoutMask;
			fInvalidsCnt--;
			}
		    found = true; // We need to signal to the timer loop, only when the first event
		    // was changed, which is the one, the timer waits for...
		    }
		else if ( ((n+1) & fTimeoutMask) == fNextFree )
		    {
		    fNextFree = n;
		    }
		else
		    {
		    fInvalidsCnt++;
		    }
		fTimeoutCnt--;
		}
		n = (n+1) & fTimeoutMask;
	    }
	if ( (fTimeoutCnt < (fTimeoutSize>>2)) && (fTimeoutSize>fOriginalTimeoutSize) )
	    {
	    Compact( NULL );
	    Resize( false ); // We shrink
	    }
#endif
	}
    pthread_mutex_unlock( &fAccessSem );
    if ( found )
	{
	pthread_mutex_lock( &fConditionSem );
	//pthread_cond_signal( &fCondition );
	pthread_cond_broadcast( &fCondition );
	pthread_mutex_unlock( &fConditionSem );
	}
    }


AliHLTTimer::AliHLTTimerThread::AliHLTTimerThread( AliHLTTimer& timer ):
    fTimer( timer )
    {
    }

void AliHLTTimer::AliHLTTimerThread::Run()
    {
    fTimer.TimerLoop();
    }

void AliHLTTimer::TimerLoop()
    {
    LOG( AliHLTLog::kDebug, "AliHLTTimer", "Timerloop" )
	<< "Entering timer loop..." << ENDLOG;
    pthread_mutex_lock( &fConditionSem );
    fQuit = false;
    fQuitted = false;
    int ret;
    struct timespec tm;
//     struct timeval tv;
    while ( !fQuit )
	{
	pthread_mutex_lock( &fAccessSem );
	ret = 0;
	if ( fTimeoutCnt>0 )
	    {
// 	    tm.tv_sec = fTimeouts[0].fTimeout/1000;
// 	    tm.tv_nsec = (fTimeouts[0].fTimeout%1000)*1000000;
// 	    gettimeofday( &tv, NULL );
// 	    tm.tv_sec += tv.tv_sec;
// 	    tm.tv_nsec += (tv.tv_usec*1000);
// 	    if ( tm.tv_nsec >= 1000000000 )
// 		{
// 		tm.tv_sec++;
// 		tm.tv_nsec -= 1000000000;
// 		}
	    if ( !fTimeoutSize )
		{
		tm.tv_sec = fTimeouts.begin()->fTime.tv_sec;
		tm.tv_nsec = fTimeouts.begin()->fTime.tv_usec*1000;
		}
	    else
		{
		tm.tv_sec = (fTimeoutStart+fFirstUsed)->fTime.tv_sec;
		tm.tv_nsec = (fTimeoutStart+fFirstUsed)->fTime.tv_usec*1000;
		}
	    pthread_mutex_unlock( &fAccessSem );
	    if ( DOLOG( AliHLTLog::kDebug ) )
		{
		AliUInt64_t tdiff;
		struct timeval tv;
		gettimeofday( &tv, NULL );
		tdiff = (tm.tv_sec-tv.tv_sec)*1000+(tm.tv_nsec/1000000-tv.tv_usec/1000);
		LOG( AliHLTLog::kDebug, "AliHLTTimer", "Timerloop" )
		    << "Next time: " << AliHLTLog::kDec 
		    << tm.tv_sec << " s " << tm.tv_nsec << " ns (" << tdiff << " ms)." << ENDLOG;
		}

	    //pthread_mutex_lock( &fConditionSem );
	    ret = pthread_cond_timedwait( &fCondition, &fConditionSem, &tm );
	    }
	else
	    {
	    pthread_mutex_unlock( &fAccessSem );
	    //pthread_mutex_lock( &fConditionSem );
	    ret = pthread_cond_wait( &fCondition, &fConditionSem );
	    }
	if ( !fQuit && ret==ETIMEDOUT )
	    TimerExpired();
	}
    pthread_mutex_unlock( &fConditionSem );
    fQuitted = true;
    }

bool AliHLTTimer::InsertTimeout( TimeoutData& data )
    {
//     AliUInt64_t diff;
//     struct timeval tv;
//     vector<TimeoutData>::iterator iter, end;
//     gettimeofday( &tv, NULL );
//     if ( tv.tv_usec < fTimerStarted.tv_usec )
// 	{
// 	tv.tv_usec += 1000000;
// 	tv.tv_sec -= 1;
// 	}
//     diff = 1000*(tv.tv_sec-fTimerStarted.tv_sec)+(tv.tv_usec-fTimerStarted.tv_usec)/1000;
//     data.fTimeout += diff;
//     iter = fTimeouts.begin();
//     end = fTimeouts.end();
//     while ( iter != end && iter->fTimeout < data.fTimeout )
// 	iter++;
//     fTimeouts.insert( iter, data );
    vector<TimeoutData>::iterator iter, end;
    if ( !fTimeoutSize )
	{
	iter = fTimeouts.begin();
	end = fTimeouts.end();
	if ( iter == end )
	    {
	    fTimeouts.insert( iter, data );
	    fTimeoutCnt++;
	    return true;
	    }
	else
	    {
	    if ( Compare( data.fTime, (end-1)->fTime )>=0 )
		{
		fTimeouts.insert( end, data );
		fTimeoutCnt++;
		return false;
		}
	    if ( Compare( data.fTime, iter->fTime )<0 )
		{
		fTimeouts.insert( iter, data );
		fTimeoutCnt++;
		return true;
		}
#ifdef USE_STL_ITERATOR_SEARCH
	    // Use a search variant that uses only STL iterators and 
	    // should be easy to change from one container to another.
	    // The drawback is that is uses a simple linear search.
	    iter++;
	    while ( iter != end )
		{
		if ( Compare( data.fTime, iter->fTime )<=0 )
		    {
		    fTimeouts.insert( iter, data );
		    fTimeoutCnt++;
		    return false;
		    }
		iter++;
		}
#else
#warning This binary search code seems to be buggy.
	    // Use a kind of binary search.
	    // Drawback: Works only with vectors.
	    // bsearch( &data, iter, sizeof(TimeoutData), end-iter, &(AliHLTTimer::TimeoutDataComparison) );
	    vector<TimeoutData>::iterator left, right, middle, target;
	    left = iter;
	    right = end-1;
	    target = NULL;
	    int res;
	    while ( right >= left && !target )
		{
		middle = (vector<TimeoutData>::iterator)( ((int)right+(int)left)/2 );
		res = Compare( data.fTime, middle->fTime );
		if ( res == 0 )
		    {
		    target = middle;
		    break;
		    }
		if ( res<0 )
		    {
		    if ( middle-1 >= iter && Compare( data.fTime, (middle-1)->fTime )>0 )
			{
			target = middle;
			break;
			}
		    right = middle-1;
		    }
		if ( res>0 )
		    {
		    if ( middle+1 < end && Compare( data.fTime, (middle+1)->fTime )<0 )
			{
			target = middle+1;
			break;
			}
		    left = middle+1;
		    }
		}
	    if ( target )
		{
		fTimeouts.insert( target, data );
		fTimeoutCnt++;
		}
#endif
	    }
	}
    else
	{
#ifdef DEBUG_PARANOIA
	    if ( fFirstUsed<fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else if ( fFirstUsed==fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != 0 && fTimeoutCnt+fInvalidsCnt != fTimeoutSize )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree+fTimeoutSize-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	if ( (int)fInvalidsCnt<0 )
	    {
	    LOG( AliHLTLog::kFatal, "AliHLTTimer::InsertTimeout", "(int)fInvalidsCnt<0" )
		<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
		<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
		<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
		<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
// 	    fTimeoutCnt += (int)fInvalidsCnt;
// 	    fInvalidsCnt = 0;
	    }
#endif
	// The case of a fixed size array, that still might have to be enlarged...
	if ( fInvalidsCnt>0 && fTimeoutCnt+fInvalidsCnt==fTimeoutSize )
	    {
	    // We have generally enough space, but some of it is occupied by invalid slots.
	    // We try to compact this stuff by moving it together, directly inserting 
	    // the new data.
	    return Compact( &data );
	    }
	else
	    {
	    // We are not limited by invalid slots...
	    if ( fTimeoutCnt==fTimeoutSize )
		Resize( true ); // ... but still we have to increase the size.
#ifdef DEBUG_PARANOIA
	    if ( (int)fInvalidsCnt<0 )
		{
		LOG( AliHLTLog::kFatal, "AliHLTTimer::InsertTimeout", "(int)fInvalidsCnt<0" )
		    << "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
		    << fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
		    << fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
		    << " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
// 		fTimeoutCnt += (int)fInvalidsCnt;
// 		fInvalidsCnt = 0;
		}
#endif
	    unsigned long inv = 0, n = (fFirstUsed+1) & fTimeoutMask;
	    unsigned long i = 0;
	    unsigned long dest=~0UL, src=~0UL;
	    bool foundInvalid = false;
	    if ( fTimeoutCnt<=0 || Compare( data.fTime, (fTimeoutStart+fFirstUsed)->fTime )<=0 )
		{
		// We can insert before the first item, or we insert the first item
		fFirstUsed = (fFirstUsed-1) & fTimeoutMask;
		*(fTimeoutStart+fFirstUsed) = data;
		fTimeoutCnt++;
		return true;
		}
	    else if ( Compare( data.fTime, (fTimeoutStart+((fNextFree-1) & fTimeoutMask))->fTime ) > 0 )
		{
		// We can insert after the last item.
		*(fTimeoutStart+fNextFree) = data;
		fNextFree = (fNextFree+1) & fTimeoutMask;
		}
	    else
		{
		// We have to insert somewhere in the middle.
		// let's find out where.
		while ( (i++<(fTimeoutCnt+fInvalidsCnt-1)) && ( (!(fTimeoutStart+n)->fValid) || Compare( data.fTime, (fTimeoutStart+n)->fTime )>0 ) )
		    {
		    if ( !(fTimeoutStart+n)->fValid )
			{
			inv = n;
			foundInvalid = true;
			}
		    n = (n+1) & fTimeoutMask;
		    }
		// We either have found a slot by now,
		// or something went wrong somewhere before...
		if ( ((fTimeoutStart+n)->fValid) && Compare( data.fTime, (fTimeoutStart+n)->fTime )<=0 )
		    {
		    // Slots has been found
		    if ( foundInvalid )
			{
			// We found an invalid slot in between, that we can use to shift,
			// so we do not need to shift the whole array.
			for ( dest = inv, src = ((inv+1) & fTimeoutMask); src!=n; dest=((dest+1) & fTimeoutMask), src=((src+1) & fTimeoutMask) )
			    *(fTimeoutStart+dest) = *(fTimeoutStart+src);
			*(fTimeoutStart+dest) = data;
			fInvalidsCnt--;
#ifdef DEBUG_PARANOIA
			if ( (int)fInvalidsCnt<0 )
			    {
			    LOG( AliHLTLog::kFatal, "AliHLTTimer::InsertTimeout", "(int)fInvalidsCnt<0" )
				<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
				<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
				<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
				<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
// 			    fTimeoutCnt += (int)fInvalidsCnt;
// 			    fInvalidsCnt = 0;
			    }
#endif
			}
		    else
			{
			unsigned long cnt1, cnt2;
			// Determine what is better, shift the stuff before us
			// or after us to make space??
			// cnt1 is space before us, cnt2 after us.
			if ( fFirstUsed<fNextFree )
			    {
			    // Buffer has not wrapped around and is in one piece.
			    cnt1 = n-fFirstUsed;
			    cnt2 = fNextFree-n;
			    }
			else
			    {
			    // Buffer has wrapped around, with two pieces, at the beginning and at the end.
			    if ( n < fFirstUsed )
				{
				// The slot where we have to insert is in the piece at the beginning
				cnt1 = n+fTimeoutSize-fFirstUsed;
				cnt2 = fNextFree-n;
				}
			    else
				{
				// The slot where we have to insert is in the piece at the end
				cnt1 = n - fFirstUsed;
				cnt2 = fNextFree+fTimeoutSize-n;
				}
			    }
			if ( cnt1<cnt2 )
			    {
			    // There are fewer items before us, than behind us.
			    for ( dest = ((fFirstUsed-1) & fTimeoutMask), src = fFirstUsed; src!=n; dest=((dest+1) & fTimeoutMask), src=((src+1) & fTimeoutMask) )
				{
				*(fTimeoutStart+dest) = *(fTimeoutStart+src);
				}
			    *(fTimeoutStart+dest) = data;
			    fFirstUsed = (fFirstUsed-1) & fTimeoutMask;
			    }
			else
			    {
			    // There are fewer items behind us, than before us.
			    // This movement loop is organised in this way in order to 
			    // be able to take advantage of invalid slots, that follow the one
			    // where we want to insert.
			    TimeoutData prev, next;
			    next = data;
			    for ( i = n; i != fNextFree; i=((i+1) & fTimeoutMask) )
				{
				prev = *(fTimeoutStart+i);
				*(fTimeoutStart+i) = next;
				if ( !prev.fValid )
				    {
				    fInvalidsCnt--;
#ifdef DEBUG_PARANOIA
				    if ( (int)fInvalidsCnt<0 )
					{
					LOG( AliHLTLog::kFatal, "AliHLTTimer::InsertTimeout", "(int)fInvalidsCnt<0" )
					    << "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
					    << fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
					    << fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
					    << " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
// 					fTimeoutCnt += (int)fInvalidsCnt;
// 					fInvalidsCnt = 0;
					}
#endif
				    break; // we need only go so far, until we have found an invalid slot.
				    }
				next = prev;
				}
			    if ( prev.fValid )
				{
				// we did not break of because we found an invalid slot,
				// so we still have to write the last element.
				*(fTimeoutStart+i) = next;
				fNextFree = (fNextFree+1) & fTimeoutMask;
				}
			    }
			}
		    }
		else
		    {
		    // Should not happen...
		    LOG( AliHLTLog::kFatal, "AliHLTTimer", "Internal Error" )
			<< "Internal error while trying to find free slots." << ENDLOG;
		    return false;
		    }
		}
	    fTimeoutCnt++;
	    }
	}
#ifdef DEBUG_PARANOIA
	    if ( fFirstUsed<fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else if ( fFirstUsed==fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != 0 && fTimeoutCnt+fInvalidsCnt != fTimeoutSize )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree+fTimeoutSize-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
    if ( (int)fInvalidsCnt<0 )
	{
	LOG( AliHLTLog::kFatal, "AliHLTTimer::InsertTimeout", "(int)fInvalidsCnt<0" )
	    << "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
	    << fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
	    << fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
	    << " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
// 	fTimeoutCnt += (int)fInvalidsCnt;
// 	fInvalidsCnt = 0;
	}
#endif
    return false;
    }

void AliHLTTimer::Resize( bool grow )
    {
    unsigned long newSize;
    // Do we grow or shrink?
    if ( grow )
	{
	// We grow...
	newSize = fTimeoutSize<<1;
	TimeoutData data;
	data.fID = 0;
	//data.fTimeout = timeout_ms;
	data.fTime.tv_sec = 0;
	data.fTime.tv_usec = 0;
	data.fCallback = NULL;
	data.fNotData = 0;
	data.fValid = false;
	fTimeouts.resize( newSize, data );
	fTimeoutStart = fTimeouts.begin();
	// Note: We double the size...
	if ( fNextFree<fFirstUsed || (fNextFree==fFirstUsed && (fTimeoutCnt+fInvalidsCnt>0)) )
	    {
	    // The used area is split into two parts, one at the beginning
	    // and one at the end of the (old) array.
	    // In other words, the buffer has partly wrapped around.
	    // We copy the block at the beginning of the buffer to the end of the block at 
	    // the end (of the old size buffer, now in the middle)
#if __GNUC__>=3
	    memcpy( fTimeoutStart.base()+fTimeoutSize, fTimeoutStart.base(), fNextFree*sizeof(TimeoutData) );
#else
	    memcpy( fTimeoutStart+fTimeoutSize, fTimeoutStart, fNextFree*sizeof(TimeoutData) );
#endif
	    unsigned long i;
	    for ( i = 0; i < fNextFree; i++ )
		*(fTimeoutStart+i) = data;
	    fNextFree += fTimeoutSize;
	    }
	fTimeoutSize = newSize;
	fTimeoutMask = fTimeoutSize-1;
	}
    else
	{
	// We shrink...
	newSize = fTimeoutSize>>1;
	// Note: fNextFree==fFirstUsed means it is totally empty, 
	// as we use at most 1/4 of the buffer.
	if ( fNextFree < fFirstUsed )
	    {
	    // We have the used area split into two parts, one at the beginning
	    // and one at the end of the (old) array.
	    // In other words, the buffer has partly wrapped around.
	    // Note: We half the size...
	    // This means that newSize is both the new size and the amount by which we shrink.
	    // Also, newSize is then the amount by which we have to move the used slots 
	    // at the (old) end of the buffer toward its beginning (its new end). And since we half the size, we can be
	    // sure not to have any overlap between the src and dest. (We are at most a quarter the old size)
#if __GNUC__>=3
	    memcpy( fTimeoutStart.base()+fFirstUsed-newSize, fTimeoutStart.base()+fFirstUsed, (fTimeoutSize-fFirstUsed)*sizeof(TimeoutData) );
#else
	    memcpy( fTimeoutStart+fFirstUsed-newSize, fTimeoutStart+fFirstUsed, (fTimeoutSize-fFirstUsed)*sizeof(TimeoutData) );
#endif
	    fFirstUsed -= newSize;
	    }
	else
	    {
	    // The used slots are in one block in the middle of the buffer,
	    // not wrapped around.
	    // We move the block to the beginning of the buffer to be sure we do not
	    // cut of any valid data.
	    unsigned long i, n, newEnd = fNextFree-fFirstUsed; // The amount of entries, also the 
	                                                       // new end point, once the block is moved to the beginning of the buffer.
	    TimeoutData data;
	    data.fID = 0;
	    //data.fTimeout = timeout_ms;
	    data.fTime.tv_sec = 0;
	    data.fTime.tv_usec = 0;
	    data.fCallback = NULL;
	    data.fNotData = 0;
	    data.fPriority = 0;
	    data.fValid = false;
	    if ( fFirstUsed>0 )
		{
		// The used block is not yet starting at the beginning.
		if ( newEnd > fFirstUsed )
		    {
		    // The new end points is greater than the old starting point
		    // => the old and new block locations overlap.
		    i = 0;
		    while ( newEnd > i )
			{
			if ( newEnd-i>=fFirstUsed )
			    n = fFirstUsed;
			else
			    n = newEnd-i;
#if __GNUC__>=3
			memcpy( fTimeoutStart.base()+i, fTimeoutStart.base()+i+fFirstUsed, n*sizeof(TimeoutData) );
#else
			memcpy( fTimeoutStart+i, fTimeoutStart+i+fFirstUsed, n*sizeof(TimeoutData) );
#endif
			i += n;
			}
		    // 		if ( newEnd < i )
		    // 		    memcpy( fTimeoutStart+i, fTimeoutStart+i+fFirstUsed, (fNextFree-fFirstUsed-i)*sizeof(TimeoutData) );
		    
		    // 		memcpy( fTimeoutStart, fTimeoutStart+fFirstUsed, fFirstUsed*sizeof(TimeoutData) );
		    // 		memcpy( fTimeoutStart+fFirstUsed, fTimeoutStart+2*fFirstUsed, (newEnd-fFirstUsed)*sizeof(TimeoutData) );
		    i = newEnd;
		    for ( n = 0; n < fNextFree-newEnd; n++ )
			{
			*(fTimeoutStart+i) = data;
			i = (i+1) & fTimeoutMask;
			}
		    }
		else
		    {
		    // The old and new block locations are completely separate.
#if __GNUC__>=3
		    memcpy( fTimeoutStart.base(), fTimeoutStart.base()+fFirstUsed, newEnd*sizeof(TimeoutData) );
#else
		    memcpy( fTimeoutStart, fTimeoutStart+fFirstUsed, newEnd*sizeof(TimeoutData) );
#endif
		    i = fFirstUsed;
		    for ( n = 0; n < newEnd; n++ )
			{
			*(fTimeoutStart+i) = data;
			i = (i+1) & fTimeoutMask;
			}
		    }
		fFirstUsed = 0;
		fNextFree = newEnd;
		}
	    }
	fTimeouts.resize( newSize );
	fTimeoutStart = fTimeouts.begin();
	fTimeoutSize = newSize;
	fTimeoutMask = fTimeoutSize-1;
	}
    }

bool AliHLTTimer::Compact( TimeoutData* data )
    {
#ifdef DEBUG_PARANOIA
	    if ( fFirstUsed<fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else if ( fFirstUsed==fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != 0 && fTimeoutCnt+fInvalidsCnt != fTimeoutSize )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree+fTimeoutSize-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
#endif
    bool insertBeforeFirst = false;
    bool insertBefore = false;
    unsigned long insertPos=0;
    unsigned long n = fFirstUsed;
    unsigned long dest, src;
    bool inserted = false;
    if ( data && Compare( data->fTime, (fTimeoutStart+fFirstUsed)->fTime )<=0 )
	{
	insertBeforeFirst = true;
	insertPos = fFirstUsed;
	}
    unsigned long i;
    for ( i = 0, dest = src = fFirstUsed; i<(fTimeoutCnt+fInvalidsCnt); src = ((src+1) & fTimeoutMask), i++ )
	{
	if ( data && (fTimeoutStart+src)->fValid && Compare( data->fTime, (fTimeoutStart+src)->fTime )<=0 && !insertBeforeFirst && !insertBefore && !inserted )
	    {
	    // We need to insert before this position
	    insertPos = src;
	    if ( dest!=src )
		{
		// Some invalids were already found before this,
		// so we can just insert it here and increase the dest
		// index.
		*(fTimeoutStart+dest) = *data;
		dest = ((dest+1) & fTimeoutMask);
		inserted = true;
		}
	    else
		{
		// We don't have space for this yet.
		insertBefore = true;
		}
	    }
	if ( (fTimeoutStart+src)->fValid )
	    {
	    // The src is valid.
	    if ( dest != src )
		{
		// We only need to copy if dest and src index
		// are different...
		*(fTimeoutStart+dest) = *(fTimeoutStart+src);
		}
	    dest = ((dest+1) & fTimeoutMask);
	    }
	else
	    {
	    if ( insertBefore )
		{
		for ( n = src; n!=insertPos; n = ((n-1) & fTimeoutMask) )
		    *(fTimeoutStart+n) = *(fTimeoutStart+ ((n-1) & fTimeoutMask));
		*(fTimeoutStart+insertPos) = *data;
		insertBefore = false;
		inserted = true;
		dest = ((dest+1) & fTimeoutMask);
		}
	    }
	}
    unsigned long oldNextFree = fNextFree;
    fNextFree = dest;
    TimeoutData emptyData;
    emptyData.fID = 0;
    //emptyData.fTimeout = timeout_ms;
    emptyData.fTime.tv_sec = 0;
    emptyData.fTime.tv_usec = 0;
    emptyData.fCallback = NULL;
    emptyData.fNotData = 0;
    emptyData.fPriority = 0;
    emptyData.fValid = false;
    for ( dest = fNextFree; dest != oldNextFree ; dest = (dest+1) & fTimeoutMask )
	*(fTimeoutStart+dest) = emptyData;

    if ( insertBeforeFirst )
	{
	n = ((fFirstUsed-1) & fTimeoutMask);
	*(fTimeoutStart+n) = *data;
	fFirstUsed = n;
	inserted = true;
	}
    if ( data && !inserted )
	{
	*(fTimeoutStart+fNextFree) = *data;
	fNextFree = ((fNextFree+1) & fTimeoutMask);
	}
    if ( data )
	fTimeoutCnt++;
    fInvalidsCnt = 0;
#ifdef DEBUG_PARANOIA
	    if ( fFirstUsed<fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else if ( fFirstUsed==fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != 0 && fTimeoutCnt+fInvalidsCnt != fTimeoutSize )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree+fTimeoutSize-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
#endif
    if ( insertBeforeFirst )
	return true;
    return false;
    }

void AliHLTTimer::TimerExpired()
    {
    LOG( AliHLTLog::kDebug, "AliHLTTimer", "Timer expired" )
	<< "Timer expired..." << ENDLOG;
//     AliUInt64_t diff;
    unsigned cnt = 0;
    struct timeval tv;
    bool found;
    fInCallback = true;
    do
	{
	pthread_mutex_lock( &fAccessSem );
#ifdef DEBUG_PARANOIA
	if ( fTimeoutSize )
	    {
	    if ( fFirstUsed<fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else if ( fFirstUsed==fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != 0 && fTimeoutCnt+fInvalidsCnt != fTimeoutSize )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree+fTimeoutSize-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    }
#endif
	found = false;
	gettimeofday( &tv, NULL );
	//     if ( tv.tv_usec < fTimerStarted.tv_usec )
	// 	{
	// 	tv.tv_usec += 1000000;
	// 	tv.tv_sec -= 1;
	// 	}
	//     diff = 1000*(tv.tv_sec-fTimerStarted.tv_sec)+(tv.tv_usec-fTimerStarted.tv_usec)/1000;
	//     fTimerStarted = tv;
	vector<TimeoutData>::iterator iter, end;
	vector<TimeoutData> expiredTimeouts;
	expiredTimeouts.reserve( fTimeouts.size() );
	if ( !fTimeoutSize )
	    {
	    iter = fTimeouts.begin();
	    end = fTimeouts.end();
	    while ( iter != end )
		{
		if ( Compare( iter->fTime, tv )<=0 )
		    {
		    found = true;
		    cnt++;
		    expiredTimeouts.insert( expiredTimeouts.end(), *iter );
		    //iter->fCallback->TimerExpired( iter->fNotData );
		    }
		else
		    break;
		// 	if ( iter->fTimeout < diff )
		// 	    iter->fTimeout = 0;
		// 	else
		// 	    iter->fTimeout -= diff;
		// 	if ( iter->fTimeout <= 0 )
		// 	    iter->fCallback->TimerExpired( iter->fNotData );
		iter++;
		}
	    //     while ( fTimeouts.size()>0 && fTimeouts.begin()->fTimeout <= 0 )
	    // 	fTimeouts.erase( fTimeouts.begin() );
	    while ( cnt > 0 && fTimeouts.size()>0 )
		{
		fTimeouts.erase( fTimeouts.begin() );
		cnt--;
		fTimeoutCnt--;
		}
	    }
	else
	    {
	    unsigned long i = 0, nr = fTimeoutCnt+fInvalidsCnt;
#ifdef DEBUG_PARANOIA
	    if ( fFirstUsed<fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else if ( fFirstUsed==fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != 0 && fTimeoutCnt+fInvalidsCnt != fTimeoutSize )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree+fTimeoutSize-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
#endif
	    //while ( fFirstUsed != fNextFree && (Compare( (fTimeoutStart+fFirstUsed)->fTime, tv )<=0 || !(fTimeoutStart+fFirstUsed)->fValid) )
	    while ( i++<nr && ( (!(fTimeoutStart+fFirstUsed)->fValid) || Compare( (fTimeoutStart+fFirstUsed)->fTime, tv )<=0 )  )
		{
		if ( (fTimeoutStart+fFirstUsed)->fValid )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTTimer::TimerExpired", "Timer Expired" )
			<< "Timer expired for id 0x " << AliHLTLog::kHex << (fTimeoutStart+fFirstUsed)->fID 
			<< " (" << AliHLTLog::kDec << (fTimeoutStart+fFirstUsed)->fID << ") - Priority: " 
			<< AliHLTLog::kDec << (fTimeoutStart+fFirstUsed)->fPriority << AliHLTLog::kHex
			<< " - Notificication Data 0x"
			<< AliHLTLog::kHex << (fTimeoutStart+fFirstUsed)->fNotData << " (" << AliHLTLog::kDec
			<< (fTimeoutStart+fFirstUsed)->fNotData << ")." << ENDLOG;
		    expiredTimeouts.insert( expiredTimeouts.end(), *(fTimeoutStart+fFirstUsed) );
		    (fTimeoutStart+fFirstUsed)->fValid = false;
		    fTimeoutCnt--;
		    found = true;
		    }
		else
		    fInvalidsCnt--;
#ifdef DEBUG_PARANOIA
		if ( (int)fInvalidsCnt<0 )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << " - i: " << i << " - nr: "
			<< nr << ". Attempting recovery." << ENDLOG;
// 		    fTimeoutCnt += (int)fInvalidsCnt;
// 		    fInvalidsCnt = 0;
		    }
#endif
		fFirstUsed = ((fFirstUsed+1) & fTimeoutMask );
		}
#ifdef DEBUG_PARANOIA
	    if ( fFirstUsed<fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else if ( fFirstUsed==fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != 0 && fTimeoutCnt+fInvalidsCnt != fTimeoutSize )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree+fTimeoutSize-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
#endif
	    if ( (fTimeoutCnt < (fTimeoutSize>>2)) && (fTimeoutSize>fOriginalTimeoutSize) )
		{
		Compact( NULL );
		Resize( false ); // We shrink
		}
#ifdef DEBUG_PARANOIA
	    if ( fFirstUsed<fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else if ( fFirstUsed==fNextFree )
		{
		if ( fTimeoutCnt+fInvalidsCnt != 0 && fTimeoutCnt+fInvalidsCnt != fTimeoutSize )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
	    else
		{
		if ( fTimeoutCnt+fInvalidsCnt != fNextFree+fTimeoutSize-fFirstUsed )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTTimer::TimerExpired", "(int)fInvalidsCnt<0" )
			<< "Internal Error: (int)fInvalidsCnt<0. fFirstUsed: " << AliHLTLog::kDec
			<< fFirstUsed << " - fNextFree: " << fNextFree << " - (fNextFree+fTimeoutSize: "
			<< fNextFree+fTimeoutSize << ") - fTimeoutCnt: " 
			<< fTimeoutCnt << " - (int)fInvalidsCnt: " << (int)fInvalidsCnt
			<< " - fTimeoutSize: " << fTimeoutSize << ". Attempting recovery." << ENDLOG;
		    }
		}
#endif
	    }
	pthread_mutex_unlock( &fAccessSem );
	if ( found )
	    {
	    pthread_mutex_unlock( &fConditionSem );
	    iter = expiredTimeouts.begin();
	    end = expiredTimeouts.end();
	    while ( iter != end )
		{
		iter->fCallback->TimerExpired( iter->fNotData, iter->fPriority );
		iter++;
		}
	    pthread_mutex_lock( &fConditionSem );
	    }
	}
    while ( found );
    fInCallback = false;
    }


int AliHLTTimer::Compare( const struct timeval& t1, const struct timeval& t2 )
    {
    if ( t1.tv_sec == t2.tv_sec )
	{
	if ( t1.tv_usec == t2.tv_usec )
	    return 0;
	if ( t1.tv_usec < t2.tv_usec )
	    return -1;
	return 1;
	}
    else
	{
	if ( t1.tv_sec < t2.tv_sec )
	    return -1;
	return 1;
	}
    }



// AliHLTTimerConditionSem::AliHLTTimerConditionSem()
//     {
//     //pthread_mutexattr_t ma;
//     //pthread_mutexattr_init( &ma );
//     //pthread_mutexattr_settype( &ma, PTHREAD_MUTEX_RECURSIVE );
//     //pthread_mutex_init( &fConditionSem, &ma );
//     pthread_mutex_init( &fConditionSem, NULL );
//     pthread_cond_init( &fCondition, NULL );
//     //pthread_mutexattr_destroy( &ma );
//     pthread_mutex_init( &fAccessSem, NULL );
//     fNotificationDataCnt = 0;
//     fNotificationDataSize = 0;
//     fNotificationDataMask = ~(unsigned long)0;
//     fLastFreed = fLastUsed = 0;
//     fNotificationStart = NULL;
//     fAllowGrowth = true;
//     }

AliHLTTimerConditionSem::AliHLTTimerConditionSem( int notDataSlotCountExp2, bool allowGrowth )
    {
    //pthread_mutexattr_t ma;
    //pthread_mutexattr_init( &ma );
    //pthread_mutexattr_settype( &ma, PTHREAD_MUTEX_RECURSIVE );
    //pthread_mutex_init( &fConditionSem, &ma );
    pthread_mutex_init( &fAccessSem, NULL );
    pthread_mutex_init( &fConditionSem, NULL );
    pthread_cond_init( &fCondition, NULL );
    //pthread_mutexattr_destroy( &ma );
    if ( notDataSlotCountExp2 < 0 )
	{
	fNotificationValidDataCnt = fNotificationDataCnt = 0;
	fNotificationDataOrigSize = fNotificationDataSize = 0;
	fNotificationDataMask = ~(unsigned long)0;
	fFirstUsed = 0; 
	fNextToUse = 0;
	fNotificationStart = fNotificationData.begin(); // NULL ?
	}
    else
	{
	fNotificationValidDataCnt = fNotificationDataCnt = 0;
	fNotificationDataOrigSize = fNotificationDataSize = 1 << notDataSlotCountExp2;
	fNotificationDataMask = fNotificationDataSize-1;
	NotificationDataType nd;
	nd.fValid = false;
	nd.fData = ~(AliUInt64_t)0;;
	fNotificationData.resize( fNotificationDataSize, nd );
	fFirstUsed = 0; 
	fNextToUse = 0;
	fNotificationStart = fNotificationData.begin();
	}
    fAllowGrowth = allowGrowth;
    //fHasGrown = false;
#ifdef OWNER_DEBUG
    fOwned = false;
    fOwnerThread = (pthread_t)0;
    fOwnerPID = 0;
#endif
    }

AliHLTTimerConditionSem::~AliHLTTimerConditionSem()
    {
    pthread_mutex_destroy( &fConditionSem );
    pthread_cond_destroy( &fCondition );
    //pthread_mutexattr_destroy( &ma );
    pthread_mutex_destroy( &fAccessSem );
    }

void AliHLTTimerConditionSem::Wait()
    {
    //pthread_mutex_lock( &fConditionSem );
    pthread_cond_wait( &fCondition, &fConditionSem );
    //pthread_mutex_unlock( &fConditionSem );
    }

// true when condition was signalled, false, when timeout expired
bool AliHLTTimerConditionSem::WaitTime( AliUInt32_t timeout_ms )
    {
    int ret;
    struct timespec tm;
    struct timeval tv;
    gettimeofday( &tv, NULL );
    tm.tv_sec = tv.tv_sec + timeout_ms/1000;
    tm.tv_nsec = (tv.tv_usec*1000) + (timeout_ms%1000)*1000000;
    if ( tm.tv_nsec >= 1000000000 )
	{
	tm.tv_sec++;
	tm.tv_nsec -= 1000000000;
	}
    //pthread_mutex_lock( &fConditionSem );
    ret = pthread_cond_timedwait( &fCondition, &fConditionSem, &tm );
    //pthread_mutex_unlock( &fConditionSem );
    if ( ret==ETIMEDOUT )
	return false;
    return true;
    }

void AliHLTTimerConditionSem::Signal()
    {
    pthread_mutex_lock( &fConditionSem );
#ifdef OWNER_DEBUG
    fOwned = true;
    fOwnerThread = pthread_self();
    fOwnerPID = getpid();
#endif
    //pthread_cond_signal( &fCondition );
    pthread_cond_broadcast( &fCondition );
#ifdef OWNER_DEBUG
    fOwned = false;
    fOwnerThread = (pthread_t)0;
    fOwnerPID = 0;
#endif
    pthread_mutex_unlock( &fConditionSem );
    }

void AliHLTTimerConditionSem::TimerExpired( AliUInt64_t notData, unsigned long priority )
    {
    if ( !priority )
	AddNotificationData( notData );
    else
	SneakNotificationData( notData );
    Signal();
    }

void AliHLTTimerConditionSem::Lock()
    {
    pthread_mutex_lock( &fConditionSem );
#ifdef OWNER_DEBUG
    fOwned = true;
    fOwnerThread = pthread_self();
    fOwnerPID = getpid();
#endif
    }

void AliHLTTimerConditionSem::Unlock()
    {
#ifdef OWNER_DEBUG
    fOwned = false;
    fOwnerThread = (pthread_t)0;
    fOwnerPID = 0;
#endif
    pthread_mutex_unlock( &fConditionSem );
    }


bool AliHLTTimerConditionSem::AddNotificationData( AliUInt64_t data )
    {
    pthread_mutex_lock( &fAccessSem );
    if ( !fNotificationDataSize )
	{
	NotificationDataType nd;
	nd.fValid = true;
	nd.fData = data;
	fNotificationData.insert( fNotificationData.end(), nd );
	}
    else
	{
	if ( fNotificationDataSize <= fNotificationDataCnt )
	    {
	    if ( !fAllowGrowth )
		{
		pthread_mutex_unlock( &fAccessSem );
		return false;
		}
	    Resize( true );
	    }
	(fNotificationStart+fNextToUse)->fValid = true;
	(fNotificationStart+fNextToUse)->fData = data;
	fNextToUse = (fNextToUse+1) & fNotificationDataMask;
	}
    fNotificationDataCnt++;
    fNotificationValidDataCnt++;
    pthread_mutex_unlock( &fAccessSem );
    return true;
    }

bool AliHLTTimerConditionSem::SneakNotificationData( AliUInt64_t data )
    {
    pthread_mutex_lock( &fAccessSem );
    if ( !fNotificationDataSize )
	{
	NotificationDataType nd;
	nd.fValid = true;
	nd.fData = data;
	fNotificationData.insert( fNotificationData.begin(), nd );
	}
    else
	{
	if ( fNotificationDataSize <= fNotificationDataCnt )
	    {
	    if ( !fAllowGrowth )
		{
		pthread_mutex_unlock( &fAccessSem );
		return false;
		}
	    Resize( true );
	    }
	unsigned long ndx = (fFirstUsed-1) & fNotificationDataMask;
	(fNotificationStart+ndx)->fValid = true;
	(fNotificationStart+ndx)->fData = data;
	fFirstUsed = ndx;
	}
    fNotificationDataCnt++;
    fNotificationValidDataCnt++;
    pthread_mutex_unlock( &fAccessSem );
    return true;
    }
bool AliHLTTimerConditionSem::HaveNotificationData()
    {
    bool ret;
    pthread_mutex_lock( &fAccessSem );
    ret = (fNotificationValidDataCnt>0);
    pthread_mutex_unlock( &fAccessSem );
    return ret;
    }

unsigned long AliHLTTimerConditionSem::GetNotificationDataCnt()
    {
    unsigned long ret;
    pthread_mutex_lock( &fAccessSem );
    ret = fNotificationValidDataCnt;
    pthread_mutex_unlock( &fAccessSem );
    return ret;
    }

AliUInt64_t AliHLTTimerConditionSem::PeekNotificationData()
    {
    AliUInt64_t ret;
    pthread_mutex_lock( &fAccessSem );
    if ( fNotificationValidDataCnt > 0 )
	{
	if ( !fNotificationDataSize )
	    ret = fNotificationData.begin()->fData;
	else
	    {
	    unsigned long n = fFirstUsed;
	    unsigned long i = 0;
	    while ( i++<fNotificationDataCnt && !(fNotificationStart+n)->fValid )
		n = (n+1) & fNotificationDataMask;
	    if ( (fNotificationStart+n)->fValid )
		ret = (fNotificationStart+n)->fData;
	    else
		{
		LOG( AliHLTLog::kFatal, "AliLtTimerConditionSem::PeekNotificationData", "Unexpectedly no data" )
		    << "Not data found with fNotificationValidDataCnt==" << AliHLTLog::kDec << fNotificationValidDataCnt << "..." << ENDLOG;
		ret = ~(AliUInt64_t)0;
		}
	    }
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliLtTimerConditionSem::PeekNotificationData", "No data" )
	    << "PeekNotificationData called without any data to peek at..." << ENDLOG;
	ret = ~(AliUInt64_t)0;
	}
    pthread_mutex_unlock( &fAccessSem );
    return ret;
    }

AliUInt64_t AliHLTTimerConditionSem::PopNotificationData()
    {
    AliUInt64_t ret;
    pthread_mutex_lock( &fAccessSem );
    if ( fNotificationValidDataCnt > 0 )
	{
	if ( !fNotificationDataSize )
	    {
	    ret = fNotificationData.begin()->fData;
	    fNotificationData.erase( fNotificationData.begin() );
	    fNotificationDataCnt--;
	    fNotificationValidDataCnt--;
	    }
	else
	    {
	    unsigned long n = fFirstUsed;
	    unsigned long i = 0, cnt = fNotificationDataCnt;
	    while ( i++<cnt && !(fNotificationStart+n)->fValid )
		{
		fNotificationDataCnt--;
		n = (n+1) & fNotificationDataMask;
		}
	    if ( (fNotificationStart+n)->fValid )
		{
		ret = (fNotificationStart+n)->fData;
		(fNotificationStart+n)->fValid = false;
		fFirstUsed = (n+1) & fNotificationDataMask;
		fNotificationDataCnt--;
		fNotificationValidDataCnt--;
		}
	    else
		{
		LOG( AliHLTLog::kFatal, "AliLtTimerConditionSem::PopNotificationData", "Unexpectedly no data" )
		    << "Not data found with fNotificationValidDataCnt==" << AliHLTLog::kDec << fNotificationValidDataCnt << "..." << ENDLOG;
		ret = ~(AliUInt64_t)0;
		}
	    }
	// If the size is greater than the original size (it has grown) and 
	// the amount of data stored is at most one quarter of the total size
	// then we shrink by one half.
	if ( fAllowGrowth && (fNotificationDataCnt <= (fNotificationDataSize >> 2)) && (fNotificationDataSize>fNotificationDataOrigSize) ) //  && fHasGrown
	    {
	    Resize( false );
	    }
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTTimerConditionSem::PopNotificationData", "No data" )
	    << "PopNotificationData called without any data to pop..." << ENDLOG;
	ret = ~(AliUInt64_t)0;
	}
    pthread_mutex_unlock( &fAccessSem );
    return ret;
    }


void AliHLTTimerConditionSem::Resize( bool grow )
    {
    unsigned long newSize;
    // Do we grow or shrink?
    if ( grow )
	{
	// We grow...
	NotificationDataType nd;
	nd.fValid = false;
	nd.fData = ~(AliUInt64_t)0;
	newSize = fNotificationDataSize<<1;
	fNotificationData.resize( newSize, nd );
	fNotificationStart = fNotificationData.begin();
	// Note: We double the size...
	if ( fNextToUse<fFirstUsed || (fNextToUse==fFirstUsed && fNotificationDataCnt>0) )
	    {
	    // The used area is split into two parts, one at the beginning
	    // and one at the end of the (old) array.
	    // In other words, the buffer has partly wrapped around.
#if __GNUC__>=3
	    memcpy( fNotificationStart.base()+fNotificationDataSize, fNotificationStart.base(), 
		    fNextToUse*sizeof(NotificationDataType) );
#else
	    memcpy( fNotificationStart+fNotificationDataSize, fNotificationStart, 
		    fNextToUse*sizeof(NotificationDataType) );
#endif
	    unsigned long i;
	    for ( i = 0; i < fNextToUse; i++ )
		*(fNotificationStart+i) = nd;
	    fNextToUse += fNotificationDataSize;
	    }
	fNotificationDataSize = newSize;
	fNotificationDataMask = fNotificationDataSize-1;
	}
    else
	{
	// We shrink...
	newSize = fNotificationDataSize>>1;
	// Note: fNextFree==fFirstUsed means it is totally empty, 
	// as we use at most 1/4 of the buffer.
	if ( fNextToUse<fFirstUsed )
	    {
	    // We have the used area split into two parts, one at the beginning
	    // and one at the end of the (old) array.
	    // In other words, the buffer has partly wrapped around.
	    // Note: We half the size...
	    // This means that newSize is both the new size and the amount by which we shrink.
	    // Also, newSize is then the amount by which we have to move the used slots 
	    // at the end of the buffer to its beginning. And since we half the size, we can be
	    // sure not to have any overlap between the src and dest.
#if __GNUC__>=3
	    memcpy( fNotificationStart.base()+fFirstUsed-newSize, fNotificationStart.base()+fFirstUsed,
		    (fNotificationDataSize-fFirstUsed)*sizeof(NotificationDataType) );
#else
	    memcpy( fNotificationStart+fFirstUsed-newSize, fNotificationStart+fFirstUsed,
		    (fNotificationDataSize-fFirstUsed)*sizeof(NotificationDataType) );
#endif
	    fFirstUsed -= newSize;
	    }
	else
	    {
	    // The used slots are in one block in the middle of the buffer,
	    // not wrapped around.
	    // This single block is copied to the beginning of the buffer.
	    unsigned long i, n, newEnd = fNextToUse-fFirstUsed;
	    NotificationDataType nd;
	    nd.fValid = false;
	    nd.fData = ~(AliUInt64_t)0;
	    if ( fFirstUsed>0 )
		{
		// The used block is not yet at the beginning of the buffer.
		if ( newEnd > fFirstUsed )
		    {
		    // The old and new block locations overlap.
		    i = 0;
		    while ( newEnd > i )
			{
			if ( newEnd-i>fFirstUsed )
			    n = fFirstUsed;
			else
			    n = newEnd-i;
#if __GNUC__>=3
			memcpy( fNotificationStart.base()+i, fNotificationStart.base()+i+fFirstUsed, n*sizeof(NotificationDataType) );
#else
			memcpy( fNotificationStart+i, fNotificationStart+i+fFirstUsed, n*sizeof(NotificationDataType) );
#endif
			i += n;
			}
		    if ( newEnd < i )
			{
#if __GNUC__>=3
			memcpy( fNotificationStart.base()+i, fNotificationStart.base()+i+fFirstUsed, (fNextToUse-fFirstUsed-i)*sizeof(NotificationDataType) );
#else
			memcpy( fNotificationStart+i, fNotificationStart+i+fFirstUsed, (fNextToUse-fFirstUsed-i)*sizeof(NotificationDataType) );
#endif
		    // 		memcpy( fNotificationStart, fNotificationStart+fFirstUsed, 
		    // 			fFirstUsed*sizeof(NotificationDataType) );
		    // 		memcpy( fNotificationStart+fFirstUsed, fNotificationStart+2*fFirstUsed,
		    // 			(newEnd-fFirstUsed)*sizeof(NotificationDataType) );
			}
		    i = newEnd;
		    for ( n = 0; n < fNextToUse-newEnd; n++ )
			{
			*(fNotificationStart+i) = nd;
			i = (i+1) & fNotificationDataMask;
			}
		    }
		else
		    {
		    // The old and new block locations are completely separate.
#if __GNUC__>=3
		    memcpy( fNotificationStart.base(), fNotificationStart.base()+fFirstUsed, 
			    newEnd*sizeof(NotificationDataType) );
#else
		    memcpy( fNotificationStart, fNotificationStart+fFirstUsed, 
			    newEnd*sizeof(NotificationDataType) );
#endif
		    i = fFirstUsed;
		    for ( n = 0; n < newEnd; n++ )
			{
			*(fNotificationStart+i) = nd;
			i = (i+1) & fNotificationDataMask;
			}
		    }
		fFirstUsed = 0;
		fNextToUse = newEnd;
		}
	    }
	fNotificationData.resize( newSize );
	fNotificationStart = fNotificationData.begin();
	fNotificationDataSize = newSize;
	fNotificationDataMask = fNotificationDataSize-1;
	}
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
