/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSharedMemory.hpp"
#include "AliHLTLog.hpp"
#include <error.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <errno.h>

AliHLTSharedMemory::AliHLTSharedMemory()
    {
    pthread_mutex_init( &fMutex, NULL );
    fErrorName = "Name Error";
    }
	
AliHLTSharedMemory::~AliHLTSharedMemory()
    {
    vector<ShmData>::iterator iter;
    pthread_mutex_lock( &fMutex );
    while ( fShms.size()>0 )
	{
	iter = fShms.begin();
	if ( iter )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTSharedMemory", "Destructor" )
		<< "Memory region " << AliHLTLog::kDec << iter->fKey << " with id " << iter->fID 
		<< " not explicitly unlocked and released. Done NOW by destructor."
		<< ENDLOG;
	    if ( iter->fPtr )
		shmdt( iter->fPtr );
	    shmctl(iter->fID, IPC_RMID, NULL );
	    }
	fShms.erase( iter );
	}
    pthread_mutex_unlock( &fMutex );
    pthread_mutex_destroy( &fMutex );
    }

// Allocate a shared memory segment of the given
// size and return its ID. name is an identifier
// for the block. The ID will be -1 in case of an error.
AliUInt32_t AliHLTSharedMemory::GetShm( AliHLTShmID shmKey, AliUInt32_t size )
    {
    ShmData data;
    data.fID = shmget( shmKey, size, IPC_CREAT|0660 );
    if ( data.fID == (AliUInt32_t)-1 )
	{
	LOG( AliHLTLog::kInformational, "AliHLTSharedMemory", "Get shared memory" )
	    << "Error requesting memory region " << AliHLTLog::kDec << shmKey << " in PSI bigphysarea."
	    << ENDLOG;
	return (AliUInt32_t)-1;
	}
    data.fSize = size;
    data.fKey = shmKey;
    data.fPtr = NULL;
    pthread_mutex_lock( &fMutex );
    fShms.insert( fShms.end(), data );
    pthread_mutex_unlock ( &fMutex );
    return data.fID;
    }

// Lock a shared memory segment and get a pointer
// on it.
void* AliHLTSharedMemory::LockShm( AliUInt32_t shmID )
    {
    void * ptr=NULL;
    bool found = false;
    vector<ShmData>::iterator iter, end;
    pthread_mutex_lock( &fMutex );
    iter = fShms.begin();
    end = fShms.end();
    while ( iter != end )
	{
	if ( iter->fID == shmID )
	    {
	    found = true;
	    break;
	    }
	iter++;
	}
    if ( found )
	{
	ptr = shmat( iter->fID, NULL, 0 );	
	if ( ptr == (void*)-1 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Shm mapping" )
		<< "Shared memory area " << AliHLTLog::kDec << iter->fKey << " could not be mapped. Reason: " 
		<< strerror( errno ) << " (" << AliHLTLog::kDec << errno << ")." << ENDLOG;
	    pthread_mutex_unlock( &fMutex );
	    return NULL;
	    }
	iter->fPtr = ptr;
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSharedMemory", "Shm mapping" )
		<< "Shared memory area with id " << AliHLTLog::kDec << shmID << " could not be found." << ENDLOG;
	ptr = NULL;
	}
    pthread_mutex_unlock( &fMutex );
    return ptr;
    }
 
// Unlock a previously locked memory segment.
bool AliHLTSharedMemory::UnlockShm( AliUInt32_t shmID )
    {
    bool found = false;
    vector<ShmData>::iterator iter, end;
    pthread_mutex_lock( &fMutex );
    iter = fShms.begin();
    end = fShms.end();
    while ( iter != end )
	{
	if ( iter->fID == shmID )
	    {
	    found = true;
	    break;
	    }
	iter++;
	}
    if ( found )
	{
	shmdt( iter->fPtr );
	iter->fPtr = NULL;
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSharedMemory", "Shm unmapping" )
		<< "Shared memory area with id " << AliHLTLog::kDec << shmID << " could not be found." << ENDLOG;
	}
    pthread_mutex_unlock( &fMutex );
    return found;
    }

// Release a shared memory segment.
bool AliHLTSharedMemory::ReleaseShm( AliUInt32_t shmID )
    {
    bool found = false;
    vector<ShmData>::iterator iter, end;
    pthread_mutex_lock( &fMutex );
    iter = fShms.begin();
    end = fShms.end();
    while ( iter != end )
	{
	if ( iter->fID == shmID )
	    {
	    found = true;
	    break;
	    }
	iter++;
	}
    if ( found )
	{
	if ( iter->fPtr )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTSharedMemory", "Shm release" )
		<< "Shared memory segment " << AliHLTLog::kDec << iter->fKey << " to be released is still locked. Unlocking now" 
		<< ENDLOG;
	    shmdt( iter->fPtr );
	    iter->fPtr = NULL;
	    }
	shmctl(iter->fID, IPC_RMID, NULL );
	fShms.erase( iter );
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSharedMemory", "Shm release" )
		<< "Shared memory area with id " << AliHLTLog::kDec << shmID << " could not be found." << ENDLOG;
	}
    pthread_mutex_unlock( &fMutex );
    return found;
    }


// bool AliHLTSharedMemory::GetShmStatus( AliUInt32_t shmID, ShmStatus* stat )
//     {
//     bool found = false;
//     PSI_Status status;
//     tRegionStatus regStat;
//     vector<ShmData>::iterator iter, end;
//     pthread_mutex_lock( &fMutex );
//     iter = fShms.begin();
//     end = fShms.end();
//     while ( iter != end )
// 	{
// 	if ( iter->fID == shmID )
// 	    {
// 	    found = true;
// 	    break;
// 	    }
// 	iter++;
// 	}
//     if ( found )
// 	{
// 	status = PSI_RegionStat( iter->fRegion, &regStat );
// 	if ( status != PSI_OK )
// 	    {
// 	    LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Shm get status" )
// 		<< "Error getting status for region " << iter->fName << ". Reason: "
// 		<< PSI_strerror(status) << " (" << AliHLTLog::kDec << status << ")." << ENDLOG;
// 	    found = false;
// 	    }
// 	stat->fRefCount = regStat.users;
// 	stat->fPhysAddr = regStat.address;
// 	stat->fSize = regStat.size;
// 	}
//     else
// 	{
// 	LOG( AliHLTLog::kWarning, "AliHLTSharedMemory", "Shm get status" )
// 		<< "Shared memory area with id " << shmID << " could not be found." << ENDLOG;
// 	}
//     pthread_mutex_unlock( &fMutex );
//     return found;
//     }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
