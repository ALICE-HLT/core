/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSharedMemory.hpp"
#include "AliHLTLog.hpp"
#include <error.h>

AliHLTSharedMemory::AliHLTSharedMemory()
    {
    pthread_mutex_init( &fMutex, NULL );
    fErrorName = "Name Error";
    }
	
AliHLTSharedMemory::~AliHLTSharedMemory()
    {
    vector<ShmData>::iterator iter;
    pthread_mutex_lock( &fMutex );
    while ( fShms.size()>0 )
	{
	iter = fShms.begin();
	if ( iter )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTSharedMemory", "Destructor" )
		<< "Memory region " << iter->fName << " with id " << iter->fID 
		<< " not explicitly unlocked and released. Done NOW by destructor."
		<< ENDLOG;
	    if ( iter->fPtr )
		PSI_unmapRegion( iter->fRegion, iter->fPtr );
	    PSI_closeRegion( iter->fRegion );
	    delete [] iter->fName;
	    }
	fShms.erase( iter );
	}
    pthread_mutex_unlock( &fMutex );
    pthread_mutex_destroy( &fMutex );
    }

// Allocate a shared memory segment of the given
// size and return its ID. name is an identifier
// for the block. The ID will be -1 in case of an error.
AliUInt32_t AliHLTSharedMemory::GetShm( const char* name, AliUInt32_t size )
    {
    ShmData data;
    PSI_Status status;
    unsigned long sz;
    String fullName;
    fullName = "/dev/psi/bigphys/";
    fullName += name;
    status = PSI_openRegion( &(data.fRegion), (const char*)fullName );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kInformational, "AliHLTSharedMemory", "Get shared memory" )
	    << "Error requesting memory region " << name << " in PSI bigphysarea."
	    << ENDLOG;
	return (AliUInt32_t)-1;
	}
    if ( size > 0 )
	{
	status = PSI_sizeRegion( data.fRegion, &sz );
	if ( status != PSI_OK )
	    {
	    bool ok = false;
	    if ( status == PSI_SIZE_SET && sz >= size )
		{
		ok = true;
		size = sz;
		LOG( AliHLTLog::kInformational, "AliHLTSharedMemory", "Get shared memory" )
		    << "Requested memory region " << name << " already allocated in PSI bigphysarea with size "
		    << AliHLTLog::kDec << sz << ENDLOG;
		}
	    if ( !ok )
		{
		LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Get shared memory" )
		    << "Error getting requested memory region " << name << " of size " 
		    << AliHLTLog::kDec << size << " in PSI bigphysarea. Reason: "
		    << PSI_strerror( status ) << " (" << AliHLTLog::kDec << status << ")." << ENDLOG;
		return (AliUInt32_t)-1;
		}
	    }
	}
    else
	{
	tRegionStatus regStat;
	status = PSI_RegionStat( data.fRegion, &regStat );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Shm get status" )
		<< "Error getting status for region " << name << ". Reason: "
		<< PSI_strerror(status) << " (" << AliHLTLog::kDec << status << ")." << ENDLOG;
	    size = 0;
	    }
	else
	    size = regStat.size;
	}
    data.fID = (AliUInt32_t)data.fRegion;
    data.fSize = size;
    data.fName = new char[ strlen( name )+1 ];
    if ( !data.fName )
	{
	data.fName = fErrorName;
	}
    else
	strcpy( data.fName, name );
    data.fPtr = NULL;
    pthread_mutex_lock( &fMutex );
    fShms.insert( fShms.end(), data );
    pthread_mutex_unlock ( &fMutex );
    return data.fID;
    }

// Lock a shared memory segment and get a pointer
// on it.
void* AliHLTSharedMemory::LockShm( AliUInt32_t shmID )
    {
    void * ptr=NULL;
    bool found = false;
    PSI_Status status;
    vector<ShmData>::iterator iter, end;
    pthread_mutex_lock( &fMutex );
    iter = fShms.begin();
    end = fShms.end();
    while ( iter != end )
	{
	if ( iter->fID == shmID )
	    {
	    found = true;
	    break;
	    }
	iter++;
	}
    if ( found )
	{
	status = PSI_mapRegion( iter->fRegion, &ptr );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Shm mapping" )
		<< "Shared memory area " << iter->fName << " could not be mapped. Reason: " 
		<< PSI_strerror( status ) << " (" << AliHLTLog::kDec << status << ")." << ENDLOG;
	    pthread_mutex_unlock( &fMutex );
	    return NULL;
	    }
	iter->fPtr = ptr;
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSharedMemory", "Shm mapping" )
		<< "Shared memory area with id " << shmID << " could not be found." << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return NULL;
	}
    pthread_mutex_unlock( &fMutex );
    return ptr;
    }
 
// Unlock a previously locked memory segment.
bool AliHLTSharedMemory::UnlockShm( AliUInt32_t shmID )
    {
    bool found = false;
    PSI_Status status;
    vector<ShmData>::iterator iter, end;
    pthread_mutex_lock( &fMutex );
    iter = fShms.begin();
    end = fShms.end();
    while ( iter != end )
	{
	if ( iter->fID == shmID )
	    {
	    found = true;
	    break;
	    }
	iter++;
	}
    if ( found )
	{
	status = PSI_unmapRegion( iter->fRegion, iter->fPtr );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Shm unmapping" )
		<< "Error unmapping region " << iter->fName << ". Reason: "
		<< PSI_strerror(status) << " (" << AliHLTLog::kDec << status << ")." << ENDLOG;
	    found = false;
	    }
	iter->fPtr = NULL;
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSharedMemory", "Shm unmapping" )
		<< "Shared memory area with id " << shmID << " could not be found." << ENDLOG;
	}
    pthread_mutex_unlock( &fMutex );
    return found;
    }

// Release a shared memory segment.
bool AliHLTSharedMemory::ReleaseShm( AliUInt32_t shmID )
    {
    bool found = false;
    PSI_Status status;
    vector<ShmData>::iterator iter, end;
    pthread_mutex_lock( &fMutex );
    iter = fShms.begin();
    end = fShms.end();
    while ( iter != end )
	{
	if ( iter->fID == shmID )
	    {
	    found = true;
	    break;
	    }
	iter++;
	}
    if ( found )
	{
	if ( iter->fPtr )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTSharedMemory", "Shm release" )
		<< "Shared memory segment " << iter->fName << " to be released is still locked. Unlocking now" 
		<< ENDLOG;
	    status = PSI_unmapRegion( iter->fRegion, iter->fPtr );
	    if ( status != PSI_OK )
		{
		LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Shm release" )
		    << "Error unmapping region " << iter->fName << ". Reason: "
		    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status << ")." << ENDLOG;
		}
	    iter->fPtr = NULL;
	    }
	status = PSI_closeRegion( iter->fRegion );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Shm release" )
		<< "Error releasing region " << iter->fName << ". Reason: "
		<< PSI_strerror(status) << " (" << AliHLTLog::kDec << status << ")." << ENDLOG;
	    found = false;
	    }
	delete [] iter->fName;
	fShms.erase( iter );
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSharedMemory", "Shm release" )
		<< "Shared memory area with id " << shmID << " could not be found." << ENDLOG;
	}
    pthread_mutex_unlock( &fMutex );
    return found;
    }


bool AliHLTSharedMemory::GetShmStatus( AliUInt32_t shmID, ShmStatus* stat )
    {
    bool found = false;
    PSI_Status status;
    tRegionStatus regStat;
    vector<ShmData>::iterator iter, end;
    pthread_mutex_lock( &fMutex );
    iter = fShms.begin();
    end = fShms.end();
    while ( iter != end )
	{
	if ( iter->fID == shmID )
	    {
	    found = true;
	    break;
	    }
	iter++;
	}
    if ( found )
	{
	status = PSI_RegionStat( iter->fRegion, &regStat );
	if ( status != PSI_OK )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Shm get status" )
		<< "Error getting status for region " << iter->fName << ". Reason: "
		<< PSI_strerror(status) << " (" << AliHLTLog::kDec << status << ")." << ENDLOG;
	    found = false;
	    }
	stat->fRefCount = regStat.users;
	stat->fPhysAddr = regStat.address;
	stat->fSize = regStat.size;
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSharedMemory", "Shm get status" )
		<< "Shared memory area with id " << shmID << " could not be found." << ENDLOG;
	}
    pthread_mutex_unlock( &fMutex );
    return found;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
