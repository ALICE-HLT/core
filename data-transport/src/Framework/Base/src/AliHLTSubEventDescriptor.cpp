/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSubEventDescriptor.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTShmManager.hpp"
#include "AliHLTAlignment.h"


AliHLTSubEventDescriptor::AliHLTSubEventDescriptor( const AliHLTSubEventDataDescriptor& data )
    {
    fData = NULL;
    SetDescriptorData( data );
    }

AliHLTSubEventDescriptor::AliHLTSubEventDescriptor( const AliHLTSubEventDataDescriptor* data )
    {
    fData = NULL;
    SetDescriptorData( data );
    }

AliHLTSubEventDescriptor::AliHLTSubEventDescriptor()
    {
    fData = NULL;
    }

AliHLTSubEventDescriptor::~AliHLTSubEventDescriptor()
    {
//     if ( fData )
// 	delete [] fData;
    fData = NULL;
    }


void AliHLTSubEventDescriptor::SetDescriptorData( const AliHLTSubEventDataDescriptor& data )
    {
    SetDescriptorData( &data );
//     if ( fData )
// 	delete [] fData;
//     AliUInt32_t sz, cnt;
//     if ( data.fHeader.fLength==0 )
// 	{
// 	if ( data.fDataBlockCount!=0 )
// 	    {
// 	    LOG( AliHLTLog::kWarning, "AliHLTSubEventDescriptor", "Set subevent-data" )
// 		<< "Passed subevent descriptor has 0 length but block count unequal 0" << ENDLOG;
// 	    }
// 	LOG( AliHLTLog::kWarning, "AliHLTSubEventDescriptor", "Set subevent-data" )
// 	    << "Passed subevent descriptor has 0 length and block count" << ENDLOG;
// 	sz = sizeof(AliHLTSubEventDataDescriptor);
// 	cnt = 0;
// 	}
//     else
// 	{
// 	sz = data.fHeader.fLength;
// 	cnt = CalcDataDescriptorSize(data.fDataBlockCount);
// 	if ( cnt < sz )
// 	    {
// 	    LOG( AliHLTLog::kWarning, "AliHLTSubEventDescriptor", "Set subevent-data" )
// 		<< "Subevent descriptor size calculated from block count smaller "
// 		<< "than given size." << ENDLOG;
// 	    }
// 	if ( cnt > sz )
// 	    {
// 	    LOG( AliHLTLog::kWarning, "AliHLTSubEventDescriptor", "Set subevent-data" )
// 		<< "Subevent descriptor size calculated from block count bigger "
// 		<< "than size of structure. Size enlarged!" << ENDLOG;
// 	    sz = cnt;
// 	    }
// 	cnt = data.fDataBlockCount;
// 	}
//     fData = (AliHLTSubEventDataDescriptor*)new AliUInt8_t[ sz ];
//     if ( fData )
// 	{
// 	memcpy( fData, &data, data.fHeader.fLength );
// 	fData->fHeader.fLength = sz;
// 	fData->fDataBlockCount = cnt;
// 	}
//     else
// 	{
// 	LOG( AliHLTLog::kError, "AliHLTSubEventDescriptor", "Set subevent-data" )
// 	    << "Out of memory allocating subevent data descriptor." << ENDLOG;
// 	return;
// 	}
    }

void AliHLTSubEventDescriptor::SetDescriptorData( const AliHLTSubEventDataDescriptor* data )
    {
    //if ( data )
    fData = data;
    }

AliEventID_t AliHLTSubEventDescriptor::GetEventID() const
    {
    if ( !fData )
	return AliEventID_t();
    return fData->fEventID;
    }

AliUInt32_t AliHLTSubEventDescriptor::GetBlockCnt() const
    {
    if ( !fData )
	return 0;
    return fData->fDataBlockCount;
    }

AliUInt64_t AliHLTSubEventDescriptor::GetEventStatusFlag() const
    {
    if ( !fData )
	return 0;
    return fData->fStatusFlags;
    }


AliHLTEventDataType AliHLTSubEventDescriptor::GetDataType() const
    {
    if ( !fData )
	{
	AliHLTEventDataType tmp = { UNKNOWN_DATAID };
	return tmp;
	}
    return fData->fDataType;
    }

bool AliHLTSubEventDescriptor::GetBlock( AliUInt32_t blockNdx, AliUInt64_t& offset, 
					 AliUInt64_t& size, AliHLTNodeID_t& producerNode, 
					 AliHLTEventDataType& dataType, AliHLTEventDataOrigin& dataOrigin,
					 AliUInt32_t& dataSpecification, AliUInt32_t& statusFlags, 
					 AliUInt8_t* attributes )
    {
    if ( !fData || fData->fDataBlockCount<=blockNdx )
	return false;
    offset = fData->fDataBlocks[ blockNdx ].fBlockOffset;
    size = fData->fDataBlocks[ blockNdx ].fBlockSize;
    producerNode = fData->fDataBlocks[ blockNdx ].fProducerNode;
    dataType = fData->fDataBlocks[ blockNdx ].fDataType;
    dataOrigin = fData->fDataBlocks[ blockNdx ].fDataOrigin;
    dataSpecification = fData->fDataBlocks[ blockNdx ].fDataSpecification;
    statusFlags = fData->fDataBlocks[ blockNdx ].fStatusFlags;
    for ( int i = 0; i < kAliHLTSEDBDAttributeCount; i++ )
	attributes[i] = fData->fDataBlocks[ blockNdx ].fAttributes[i];
    //byteOrder = fData->fDataBlocks[ blockNdx ].fByteOrder;
    return true;
    }
 
 
AliUInt32_t AliHLTSubEventDescriptor::CalcDataDescriptorSize( AliUInt32_t blockCount )
	{
	AliUInt32_t sz;
	sz = sizeof(AliHLTSubEventDataDescriptor)+blockCount*sizeof(AliHLTSubEventDataBlockDescriptor);
	if ( sz % 8 )
	    sz = (sz/8+1)*8;
	return sz;
	}


void AliHLTSubEventDescriptor::Dereference( AliHLTShmManager* shmMan, vector<AliHLTSubEventDescriptor::BlockData>& dataBlocks ) const
    {
    Dereference( shmMan, fData, dataBlocks );
    }

const char* AliHLTSubEventDescriptor::GetTypeForAlignmentIndex( int index )
    {
    switch ( index )
	{
	case kAliHLTSEDBD64BitAlignmentAttributeIndex: 
	    return "64 bit integer";
	case kAliHLTSEDBD32BitAlignmentAttributeIndex: 
	    return "32 bit integer";
	case kAliHLTSEDBD16BitAlignmentAttributeIndex: 
	    return "16 bit integer";
	case kAliHLTSEDBD8BitAlignmentAttributeIndex: 
	    return "8 bit integer";
	case kAliHLTSEDBDFloatAlignmentAttributeIndex: 
	    return "float";
	case kAliHLTSEDBDDoubleAlignmentAttributeIndex:
	    return "double";
	default:
	    return "unknown";
	}
    }

void AliHLTSubEventDescriptor::FillBlockAttributes( AliUInt8_t attributes[8] )
    {
    for ( int j = 0; j < kAliHLTSEDBDAttributeCount; j++ )
	attributes[j] = kAliHLTUnspecifiedAttribute;
    attributes[kAliHLTSEDBDByteOrderAttributeIndex] = kAliHLTNativeByteOrder;
    attributes[kAliHLTSEDBD64BitAlignmentAttributeIndex] = AliHLTDetermineUInt64Alignment();
    attributes[kAliHLTSEDBD32BitAlignmentAttributeIndex] = AliHLTDetermineUInt32Alignment();
    attributes[kAliHLTSEDBD16BitAlignmentAttributeIndex] = AliHLTDetermineUInt16Alignment();
    attributes[kAliHLTSEDBD8BitAlignmentAttributeIndex] = AliHLTDetermineUInt8Alignment();
    attributes[kAliHLTSEDBDFloatAlignmentAttributeIndex] = AliHLTDetermineFloatAlignment();
    attributes[kAliHLTSEDBDDoubleAlignmentAttributeIndex] = AliHLTDetermineDoubleAlignment();
    }

void AliHLTSubEventDescriptor::FillUnspecifiedBlockAttributes( AliUInt8_t attributes[8] )
    {
    if ( attributes[kAliHLTSEDBDByteOrderAttributeIndex]==kAliHLTUnspecifiedAttribute )
	attributes[kAliHLTSEDBDByteOrderAttributeIndex] = kAliHLTNativeByteOrder;
    if ( attributes[kAliHLTSEDBD64BitAlignmentAttributeIndex]==kAliHLTUnspecifiedAttribute )
	attributes[kAliHLTSEDBD64BitAlignmentAttributeIndex] = AliHLTDetermineUInt64Alignment();
    if ( attributes[kAliHLTSEDBD32BitAlignmentAttributeIndex]==kAliHLTUnspecifiedAttribute )
	attributes[kAliHLTSEDBD32BitAlignmentAttributeIndex] = AliHLTDetermineUInt32Alignment();
    if ( attributes[kAliHLTSEDBD16BitAlignmentAttributeIndex]==kAliHLTUnspecifiedAttribute )
	attributes[kAliHLTSEDBD16BitAlignmentAttributeIndex] = AliHLTDetermineUInt16Alignment();
    if ( attributes[kAliHLTSEDBD8BitAlignmentAttributeIndex]==kAliHLTUnspecifiedAttribute )
	attributes[kAliHLTSEDBD8BitAlignmentAttributeIndex] = AliHLTDetermineUInt8Alignment();
    if ( attributes[kAliHLTSEDBDFloatAlignmentAttributeIndex]==kAliHLTUnspecifiedAttribute )
	attributes[kAliHLTSEDBDFloatAlignmentAttributeIndex] = AliHLTDetermineFloatAlignment();
    if ( attributes[kAliHLTSEDBDDoubleAlignmentAttributeIndex]==kAliHLTUnspecifiedAttribute )
	attributes[kAliHLTSEDBDDoubleAlignmentAttributeIndex] = AliHLTDetermineDoubleAlignment();
    }

AliUInt8_t AliHLTSubEventDescriptor::GetBlockAttribute( unsigned attributeIndex )
    {
    switch ( attributeIndex )
	{
	case kAliHLTSEDBDByteOrderAttributeIndex:
	    return kAliHLTNativeByteOrder;
	case kAliHLTSEDBD64BitAlignmentAttributeIndex:
	    return AliHLTDetermineUInt64Alignment();
	case kAliHLTSEDBD32BitAlignmentAttributeIndex:
	    return AliHLTDetermineUInt32Alignment();
	case kAliHLTSEDBD16BitAlignmentAttributeIndex:
	    return AliHLTDetermineUInt16Alignment();
	case kAliHLTSEDBD8BitAlignmentAttributeIndex:
	    return AliHLTDetermineUInt8Alignment();
	case kAliHLTSEDBDFloatAlignmentAttributeIndex:
	    return AliHLTDetermineFloatAlignment();
	case kAliHLTSEDBDDoubleAlignmentAttributeIndex:
	    return AliHLTDetermineDoubleAlignment();
	default:
	    return kAliHLTUnspecifiedAttribute;
	}
    }



void AliHLTSubEventDescriptor::Dereference( AliHLTShmManager* shmMan, const AliHLTSubEventDataDescriptor *sedd, vector<AliHLTSubEventDescriptor::BlockData>& dataBlocks ) const
    {
    if ( !sedd )
	{
	return;
	}
    if ( !shmMan )
	{
	LOG( AliHLTLog::kError, "AliHLTSubEventDescriptor::Dereference", "No shm manmager" )
	    << "No shm manager object to obtain shm buffers." << ENDLOG;
	return;
	}
    if ( sedd->fHeader.fType.fID != ALIL3SUBEVENTDATADESCRIPTOR_TYPE )
	{
	LOG( AliHLTLog::kError, "AliHLTSubEventDescriptor::Dereference", "Wrong Sub-Event Data Descriptor Data ID" )
	    << "Wrong sub-event data descriptor ID: 0x" << AliHLTLog::kHex << sedd->fDataType.fID
	    << " (" << AliHLTLog::kDec << sedd->fDataType.fID << ") - " 
	    << sedd->fHeader.fType.fDescr[0] << sedd->fHeader.fType.fDescr[1] 
	    << sedd->fHeader.fType.fDescr[2] << sedd->fHeader.fType.fDescr[3] 
	    << "("
	    << sedd->fHeader.fType.fDescr[3] << sedd->fHeader.fType.fDescr[2] 
	    << sedd->fHeader.fType.fDescr[1] << sedd->fHeader.fType.fDescr[0] 
	    << "). Expected 0x" << AliHLTLog::kHex << ALIL3SUBEVENTDATADESCRIPTOR_TYPE
	    << " (" << AliHLTLog::kDec << ALIL3SUBEVENTDATADESCRIPTOR_TYPE << ")."
	    << ENDLOG;
	return;
	}
    AliUInt8_t* ptr=NULL;
    AliUInt32_t i, n;
    n = sedd->fDataBlockCount;
    dataBlocks.reserve( dataBlocks.size()+n );
    for ( i = 0; i < n; i++ )
	{
	BlockData bd;
	ptr = shmMan->GetShm( sedd->fDataBlocks[i].fShmID );
	if ( !ptr )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubEventDescriptor::Dereference", "Shm error" )
		<< "Unable to get pointer to shared memory area with id 0x" << AliHLTLog::kHex
		<< sedd->fDataBlocks[i].fShmID.fKey.fID << " )" << AliHLTLog::kDec << sedd->fDataBlocks[i].fShmID.fKey.fID
		<< ")." << ENDLOG;
	    continue;
	    }
	ptr += sedd->fDataBlocks[i].fBlockOffset;
	if ( sedd->fDataBlocks[i].fDataType.fID == COMPOSITE_DATAID )
	    {
	    Dereference( shmMan, (const AliHLTSubEventDataDescriptor*)ptr, dataBlocks );
	    continue;
	    }
	
	bd.fShmKey.fShmType = sedd->fDataBlocks[i].fShmID.fShmType;
	bd.fShmKey.fKey.fID = sedd->fDataBlocks[i].fShmID.fKey.fID;
	bd.fOffset = sedd->fDataBlocks[i].fBlockOffset;
	bd.fData = ptr;
	bd.fSize = sedd->fDataBlocks[i].fBlockSize;
	bd.fProducerNode = sedd->fDataBlocks[i].fProducerNode;
	bd.fDataType = sedd->fDataBlocks[i].fDataType;
	bd.fDataOrigin = sedd->fDataBlocks[i].fDataOrigin;
	bd.fDataSpecification = sedd->fDataBlocks[i].fDataSpecification;
	bd.fStatusFlags = sedd->fDataBlocks[i].fStatusFlags;
	for ( int n = 0; n < kAliHLTSEDBDAttributeCount; n++ )
	    bd.fAttributes[n] = sedd->fDataBlocks[i].fAttributes[n];
	//bd.fByteOrder = sedd->fDataBlocks[i].fByteOrder;
	dataBlocks.insert( dataBlocks.end(), bd );
	}
    }


// class AliHLTSubEventDescriptor
//     {
//     public:

	
// 	AliHLTSubEventDescriptor( const AliHLTSubEventDataDescriptor& data );
//         AliHLTSubEventDescriptor();

//	void SetDescriptorData( const AliHLTSubEventDataDescriptor& data );

//         virtual AliEventID_t GetEventID() const;
//         virtual AliUInt32_t GetBlockCnt() const;
//         virtual bool GetBlock( AliUInt32_t blockNdx, AliUInt32_t& offset, 
//                                AliUInt32_t& size, AliHLTNodeID_t& producerNode, 
//                                AliHLTEventDataType& dataType );

//     protected:

// 	AliHLTSubEventDataDescriptor *fData;

//     private:
//     };










/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
