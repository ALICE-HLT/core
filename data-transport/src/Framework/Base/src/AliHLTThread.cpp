/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#if 0

#include "AliHLTThread.hpp"
#include <stdio.h>


AliHLTThread::AliHLTThread():
	fFifo( 1024, 256 )
	{
	pthread_attr_init( &fThreadAttrs );
	}

AliHLTThread::AliHLTThread( AliUInt32_t bufferSize, AliUInt32_t emergencySize ):
	fFifo( bufferSize, emergencySize )
	{
	pthread_attr_init( &fThreadAttrs );
	}

AliHLTThread::~AliHLTThread()
	{
	pthread_attr_destroy( &fThreadAttrs );
	}


AliHLTThread::TState AliHLTThread::Start()
	{
	int ret;
	//ret = pthread_create( &fThread, &fThreadAttrs, ThreadStart, this );
	ret = pthread_create( &fThread, NULL, ThreadStart, this );
	return !ret ? kStarted : kFailed;
	}


AliHLTThread::TState AliHLTThread::Abort()
	{
	int ret;
	ret = pthread_cancel( fThread );
	return !ret ? kAborted : kFailed;
	}


AliHLTThread::TState AliHLTThread::QuickRestart()
	{
	return kFailed;
	}

AliHLTThread::TState AliHLTThread::Join()
	{
	if ( pthread_join( fThread, NULL ) )
		return kFailed;
	return kJoined;
	}


void* AliHLTThread::ThreadStart( void* obj )
	{
	if ( !obj )
		return NULL;
	AliHLTThread* thr = (AliHLTThread*)obj;
	thr->Run();
	return NULL;
	}

#endif


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
