/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

// #include "AliHLTLog.hpp"
// #include "AliHLTLogServer.hpp"



// //AliHLTLog::TLogLevel AliHLTLog::gLogLevel = AliHLTLog::kDebug;
// //AliHLTLog::TLogLevel& gLogLevel = AliHLTLog::gLogLevel;
// int AliHLTLog::gLogLevel = AliHLTLog::kDebug;
// int& gLogLevel = AliHLTLog::gLogLevel;

// AliHLTLog AliHLTLog::gLog;
// AliHLTLog& gLog = AliHLTLog::gLog;



// AliHLTLog::AliHLTLog()
// 	{
// 	fState = kIdle;
// 	fFormat = kHex;
// 	fPrec = 0;
// 	fLine = "";
// 	fSequence=0;
// 	pthread_mutex_init( &fMutex, NULL );
// 	}


// AliHLTLog::~AliHLTLog()
// 	{
// 	if ( kInLog )
// 		operator<<( kEnd );
// 	pthread_mutex_destroy( &fMutex );
// 	}


// void AliHLTLog::AddServer( AliHLTLogServer* server )
// 	{
// 	fServers.insert( fServers.end(), server );
// 	}

// void AliHLTLog::DelServer( AliHLTLogServer* server )
// 	{
// 	TLogServerVector::iterator iter = fServers.begin();
// 	while ( iter && iter!=fServers.end() )
// 		{
// 		if ( *iter==server )
// 			{
// 			fServers.erase( iter );
// 			return;
// 			}
// 		iter++;
// 		}
// 	}


// void AliHLTLog::FlushServers()
// 	{
// 	AliUInt32_t i, n = fServers.size();
// 	if ( !fIsMod )
// 		{
// 		for ( i = 0; i < n; i++ )
// 			fServers[i]->LogLine( fOrigin, fKeyword, fFile, fLineNr, fCDate, fCTime,
// 					      fLogLevel, fHostname, fLDate, fLTime_s, fLTime_us,
// 					      fSequence, fLine );
// 		}
// 	else
// 		{
// 		for ( i = 0; i < n; i++ )
// 			fServers[i]->LogLine( fOrigin, fKeyword, fFile, fLineNr, fCDate, fCTime,
// 					      fLogLevel, fHostname, fLDate, fLTime_s, fLTime_us, 
// 					      fSequence, fModSequence, fLine );
// 		}
// 	}





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
