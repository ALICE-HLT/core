/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTEventTriggerStruct.h"


bool CompareEventTriggerTypes( void* /*param*/, const AliHLTEventTriggerStruct* ett1, const AliHLTEventTriggerStruct* ett2 )
    {
    AliUInt32_t i;
    if ( ett1->fDataWordCount != ett2->fDataWordCount )
	return false;
    for ( i = 0; i < ett1->fDataWordCount; i++ )
	{
	if ( ett1->fDataWords[i] != ett2->fDataWords[i] )
	    return false;
	}
    return true;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
