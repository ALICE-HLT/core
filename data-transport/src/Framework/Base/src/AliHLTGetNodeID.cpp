/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTGetNodeID.hpp"
#include <netdb.h>
#include <unistd.h>
#include <sys/utsname.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>

extern int h_errno;


#define LOCALHOST htonl( 0x7F000001 )


AliHLTNodeID_t AliHLTGetNodeID()
    {
    char myname[ SYS_NMLN ];
    struct hostent *he;
    AliHLTNodeID_t nodeID, nodeIDF = 0xFFFFFFFF;
    int i = 0;

    if ( gethostname( myname, SYS_NMLN )==-1 )
	return (AliHLTNodeID_t)-1;
    
    he = gethostbyname( myname );
    if ( !he )
	{
	errno = h_errno;
	return (AliHLTNodeID_t)-1;
	}
    
    while ( he->h_addr_list[i] )
	{
	nodeID = *(AliUInt32_t*)(he->h_addr_list[i]);
	if ( nodeID != LOCALHOST && nodeID < nodeIDF )
	    nodeIDF = nodeID;
	i++;
	}
    if ( nodeIDF == 0xFFFFFFFF )
	return (AliHLTNodeID_t)-1;
    return (AliUInt32_t)nodeIDF;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
