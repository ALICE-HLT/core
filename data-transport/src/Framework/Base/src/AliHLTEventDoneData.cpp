/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTEventDoneData.h"
#include "AliHLTEventDoneData.hpp"
#include "AliHLTLog.hpp"


AliHLTEventDoneData* AliHLTMergeEventDoneData( AliEventID_t eventID, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    unsigned long tLength = 0;
    unsigned long offset = 0, tWords = 0;

    vector<AliHLTEventDoneData*>::iterator eddIter, eddEnd;
    eddIter = eventDoneData.begin();
    eddEnd = eventDoneData.end();
    while ( eddIter != eddEnd )
	{
	tLength += (*eddIter)->fHeader.fLength;
	tWords += (*eddIter)->fDataWordCount;
	eddIter++;
	}
    AliHLTEventDoneData* edd;
    if ( tLength<=0 || tWords<=0 )
	{
	return NULL;
	}
    else
	{
	edd = (AliHLTEventDoneData*)new AliUInt8_t[ tLength ];
	if ( !edd )
	    {
	    LOG( AliHLTLog::kError, "MergeEventDoneData", "Out of memory" )
		<< "Out of memory trying to allocate event done data for event 0x" << AliHLTLog::kHex
		<< eventID << " (" << AliHLTLog::kDec << eventID << ") of " << tLength 
		<< " bytes." << ENDLOG;
	    return NULL;
	    }
	else
	    {
	    AliHLTMakeEventDoneData( edd, eventID );
	    edd->fHeader.fLength = tLength;
	    edd->fDataWordCount = tWords;
	    eddIter = eventDoneData.begin();
	    eddEnd = eventDoneData.end();
	    while ( eddIter != eddEnd )
		{
		memcpy( edd->fDataWords+offset, (*eddIter)->fDataWords, (*eddIter)->fDataWordCount*sizeof((*eddIter)->fDataWords[0]) );
		offset += (*eddIter)->fDataWordCount;
		eddIter++;
		}
	    }
	}
    return edd;
    }

unsigned long AliHLTGetMergedEventDoneDataSize( vector<AliHLTEventDoneData*>& eventDoneData )
    {
    unsigned long tLength = 0;
    unsigned long tWords = 0;

    vector<AliHLTEventDoneData*>::iterator eddIter, eddEnd;
    eddIter = eventDoneData.begin();
    eddEnd = eventDoneData.end();
    while ( eddIter != eddEnd )
	{
	tLength += (*eddIter)->fHeader.fLength-sizeof(AliHLTEventDoneData);
	tWords += (*eddIter)->fDataWordCount;
	eddIter++;
	}
    tLength += sizeof(AliHLTEventDoneData);
    if ( tLength<=0 || tWords<=0 )
	{
	return 0;
	}
    return tLength;
    }

void AliHLTMergeEventDoneData( AliEventID_t eventID, AliHLTEventDoneData* eddTarget, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    unsigned long offset = 0;
    AliHLTMakeEventDoneData( eddTarget, eventID );
//     eddTarget->fHeader.fLength = tLength;
//     eddTarget->fDataWordCount = tWords;
    vector<AliHLTEventDoneData*>::iterator eddIter, eddEnd;
    eddIter = eventDoneData.begin();
    eddEnd = eventDoneData.end();
    while ( eddIter != eddEnd )
	{
	eddTarget->fStatusFlags |= (*eddIter)->fStatusFlags;
	memcpy( eddTarget->fDataWords+offset, (*eddIter)->fDataWords, (*eddIter)->fDataWordCount*sizeof((*eddIter)->fDataWords[0]) );
	offset += (*eddIter)->fDataWordCount;
	eddTarget->fDataWordCount += (*eddIter)->fDataWordCount;
	eddTarget->fHeader.fLength += (*eddIter)->fDataWordCount*sizeof((*eddIter)->fDataWords[0]);
	eddIter++;
	}
    }


void AliHLTFreeMergedEventDoneData( AliHLTEventDoneData* ed )
    {
    delete [] (AliUInt8_t*)ed;
    }


AliHLTEventDoneData* AliHLTMakeEventDoneData( AliUInt32_t dataWordCount, AliUInt64_t statusFlags )
    {
    AliHLTEventDoneData* edd;
    unsigned long len = sizeof(AliHLTEventDoneData)+dataWordCount*sizeof(AliUInt32_t);

    edd = (AliHLTEventDoneData*)new AliUInt8_t[ len ];
    
    if ( !edd )
	{
	LOG( AliHLTLog::kError, "MergeEventDoneData", "Out of memory" )
	    << "Out of memory trying to allocate event done data of " 
	    << AliHLTLog::kDec << len << " bytes." << ENDLOG;
	return NULL;
	}

    edd->fHeader.fLength = len;
    edd->fHeader.fType.fID = ALIL3EVENTDONEDATA_TYPE;
    edd->fHeader.fSubType.fID = 0;
    edd->fHeader.fVersion = 1;
    edd->fEventID = AliEventID_t( kAliEventTypeUnknown, ~0ULL );
    edd->fStatusFlags = statusFlags;
    edd->fDataWordCount = dataWordCount;
    return edd;
    }

void AliHLTFreeEventDoneData( AliHLTEventDoneData* ed )
    {
    if ( ed )
	delete [] (AliUInt8_t*)ed;
    }



AliHLTEventDoneDataProcessor::TStatus AliHLTEventDoneDataProcessor::NextCommand( AliUInt32_t const *& cmd, unsigned& wordCount )
    {
    cmd = NULL;
    if ( fPos >= fData->fDataWordCount )
	return kDone;
    unsigned long oldPos = fPos;
    switch ( (AliHLTEventDoneDataCommands)(fData->fDataWords[fPos]) )
	{
	case kAliHLTEventDoneNOPCmd:
	    wordCount = 1;
	    break;
	case kAliHLTEventDoneMonitorReplayEventIDCmd: // Fallthrough
	case kAliHLTEventDoneMonitorFirstOrbitEventCmd:
	    wordCount = 3;
	    break;
	case kAliHLTEventDoneReadoutListCmd: // Fallthrough
	case kAliHLTEventDoneMonitorListCmd:
	    if ( fPos+1>=fData->fDataWordCount )
		return kDataSizeError;
	    wordCount = 2+fData->fDataWords[fPos+1]*4; // Command, count specifier, count specifier blocks of 4 words
	    break;
	case kAliHLTEventDoneFlagMonitorEventCmd:
	    wordCount = 1;
	    break;
	case kAliHLTEventDoneBackPressureHighWaterMark: // Fallthrough
	case kAliHLTEventDoneBackPressureLowWaterMark:
	    wordCount = 3;
	    break;
	default:
	    return kUnknownCommand;
	case kAliHLTEventDoneDebugCmd:
	    if ( fPos+1>=fData->fDataWordCount )
		return kDataSizeError;
	    wordCount = 2+fData->fDataWords[fPos+1]; // Command, count specifier, count specifier number of words
	    break;
	}
    fPos += wordCount;
    if ( fPos>fData->fDataWordCount )
	return kDataSizeError;
    cmd = fData->fDataWords+oldPos;
    return kOk;
    }

bool AliHLTEventDoneDataProcessor::Process( AliHLTEventDoneData *data, EventDoneDataCommandProcessFunc processFunc )
    {
    StartProcess( data );
    AliUInt32_t const * cmd;
    unsigned wordCount;
    TStatus status = NextCommand( cmd, wordCount );
    while ( status == kOk )
	{
	processFunc( cmd, wordCount );
	status = NextCommand( cmd, wordCount );
	}
    return status==kDone;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
