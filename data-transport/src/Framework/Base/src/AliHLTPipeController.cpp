/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTPipeController.hpp"
#include "AliHLTLog.hpp"
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/time.h>



AliHLTPipeController::AliHLTPipeController( const char* pipename )
    {
    fTimeout = 0;
    fPipeName = "/tmp/";
    fPipeName += pipename;
    fPipeName += "-ControlPipe";
    fPipeHandle = -1;
    fQuitValue = &fLocalQuitValue;
    fLocalQuitValue = 0;
    fQuitted = true;
    pthread_mutex_init( &fValueMutex, NULL );
    }

AliHLTPipeController::~AliHLTPipeController()
    {
    pthread_mutex_destroy( &fValueMutex );
    if ( fPipeHandle != -1 )
	close( fPipeHandle );
    struct stat dat;
    int ret;
    ret = stat( fPipeName.c_str(), &dat );
    if ( ret != -1 )
	unlink( fPipeName.c_str() );
    }

void AliHLTPipeController::AddValue( AliUInt32_t& val )
    {
    int ret;
    ret = pthread_mutex_lock( &fValueMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPipeController::AddValue", "Mutex lock error" )
	    << "Error locking value mutex: " << strerror(ret) << AliHLTLog::kDec
	    << " (" << ret << "). Continuing..." << ENDLOG;
	}

    fValues.insert( fValues.end(), &val );

    ret = pthread_mutex_unlock( &fValueMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPipeController::AddValue", "Mutex unlock error" )
	    << "Error unlocking value mutex: " << strerror(ret) << AliHLTLog::kDec
	    << " (" << ret << "). Continuing..." << ENDLOG;
	}
    }

void AliHLTPipeController::Run()
    {
    struct stat dat;
    int ret;
    fQuitted = false;
    if ( fQuitValue )
	*fQuitValue = 0;
    ret = stat( fPipeName.c_str(), &dat );
    if ( ret==-1 && errno==ENOENT )
	{
	// named pipe not found, attempting to create:
	// Warning named pipe is created with rights rw-rw----
	// (umask will be applied to this afterwards...)
	//printf( "mkfifo'ing pipe %s\n", (const char*)name );
	ret = mkfifo( fPipeName.c_str(), 0660 );
	if ( ret==-1 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTPipeController", "Create pipe" )
		<< "Could not create FIFO (mkfifo)" << fPipeName.c_str() << ". Reason: " << strerror(errno) 
		<< " (" << errno << ")." << ENDLOG; 
	    return;
	    }
	}
    if ( ret==-1 && errno!=ENOENT )
	{
	LOG( AliHLTLog::kError, "AliHLTPipeController", "Create pipes" )
	    << "Could not stat FIFO (stat)" << fPipeName.c_str() << ". Reason: " << strerror(errno)
	    << " (" << errno << ")." << ENDLOG;
	return;
	}
    if ( !ret )
	{
	//ret = open( fPipeName.c_str(), O_RDONLY ); // |O_NONBLOCK
	ret = open( fPipeName.c_str(), O_RDWR|O_NONBLOCK ); // 
	if ( ret==-1 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTPipeController", "Open pipes" )
		<< "Unable to open FIFO (open" << fPipeName.c_str() << ". Reason: " << strerror(errno)
		<< " (" << errno << ")" << ENDLOG;
	    return;
	    }
	else 
	    fPipeHandle = ret;
	}


    struct timeval ts, *tsp;
    AliUInt32_t index, value;
    unsigned int readData = 0;
    AliUInt8_t* p = (AliUInt8_t*)&index;

    if ( fPipeHandle == -1 )
	{
	LOG( AliHLTLog::kError, "AliHLTPipeController::Run", "No pipe open" )
	    << "Could not start pipe listening loop. No pipe open..." << ENDLOG;
	return;
	}
    while ( fQuitValue && !*fQuitValue )
	{
	fd_set fds;
	do
	    {
	    FD_ZERO( &fds );
	    FD_SET( fPipeHandle, &fds );
	    errno = 0;
	    ts.tv_sec = 0;
	    ts.tv_usec = 10000;
	    ret = select( fPipeHandle+1, &fds, NULL, NULL, &ts );
	    if ( !fQuitValue || *fQuitValue )
		break;
	    if ( ret < 0 )
		{
		LOG( AliHLTLog::kWarning, "AliHLTPipeController::Run", "select error" )
		    << "Error on select: " << strerror(errno) << AliHLTLog::kDec << " ("
		    << errno << ")." << ENDLOG;
		}
	    }
	while ( ret <= 0 || !FD_ISSET( fPipeHandle, &fds ) );
	if ( *fQuitValue )
	    break;
	readData = 0;
	p = (AliUInt8_t*)&index;
	do
	    {
	    errno = 0;
	    ret = read( fPipeHandle, p+readData, 4-readData );
	    if ( !ret )
		break;
	    if ( ret >= 0 )
		readData += ret;
	    if ( ret < 4 )
		{
		LOG( AliHLTLog::kError, "AliHLTPipeController::Run", "Pipe read error" )
		    << "Error reading index from pipe: " << strerror(errno) << AliHLTLog::kDec
		    << " (" << errno << ")." << ENDLOG;
		}
	    }
	while ( readData<4 && errno == EINTR );
	if ( !readData )
	    continue;
	
	do
	    {
	    FD_ZERO( &fds );
	    FD_SET( fPipeHandle, &fds );
	    if ( fTimeout > 0 )
		{
		ts.tv_sec = fTimeout/1000000;
		ts.tv_usec = fTimeout%1000000;
		tsp = &ts;
		}
	    else
		tsp = NULL;
	    errno = 0;
	    ret = select( fPipeHandle+1, &fds, NULL, NULL, tsp );
	    if ( ret < 0 )
		{
		LOG( AliHLTLog::kWarning, "AliHLTPipeController::Run", "select error" )
		    << "Error on select: " << strerror(errno) << AliHLTLog::kDec << " ("
		    << errno << ")." << ENDLOG;
		}
	    }
	while ( ret<=0 && errno==EINTR );
	if ( ret > 0 )
	    {
	    p= (AliUInt8_t*)&value;
	    readData = 0;
	    do
		{
		errno = 0;
		ret = read( fPipeHandle, p+readData, 4-readData );
		if ( !ret )
		    break;
		if ( ret >= 0 )
		    readData += ret;
		if ( ret < 4 )
		    {
		    LOG( AliHLTLog::kError, "AliHLTPipeController::Run", "Pipe read error" )
			<< "Error reading value from pipe: " << strerror(errno) << AliHLTLog::kDec
			<< " (" << errno << ")." << ENDLOG;
		    }
		}
	    while ( readData<4 && errno == EINTR );
	    if ( !readData )
		continue;

	    ret = pthread_mutex_lock( &fValueMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTPipeController::Run", "Mutex lock error" )
		    << "Error locking value mutex: " << strerror(ret) << AliHLTLog::kDec
		    << " (" << ret << "). Continuing..." << ENDLOG;
		}

	    LOG( AliHLTLog::kDebug, "AliHLTPipeController::Run", "Data read" )
		<< "Read Index " << AliHLTLog::kDec << index << " and value 0x" 
		<< AliHLTLog::kHex << value << "/" << AliHLTLog::kDec << value
		<< "." << ENDLOG;
	    
	    if ( index < fValues.size() )
		{
		*(fValues[ index ]) = value;
		}

	    ret = pthread_mutex_unlock( &fValueMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTPipeController::Run", "Mutex unlock error" )
		    << "Error unlocking value mutex: " << strerror(ret) << AliHLTLog::kDec
		    << " (" << ret << "). Continuing..." << ENDLOG;
		}

	    }
	
	}
    fQuitted = true;
    }
	
// 	MLUCString fPipeName;
// 	int fPipeHandle;

// 	vector<AliUInt32_t&> fValues;
// 	pthread_mutex_t fValueMutex;


void AliHLTPipeController::Quit()
    {
    if ( fQuitted )
	return;
    if ( fQuitValue )
	*fQuitValue = 1;
    int fd;
    fd = open ( fPipeName.c_str(), O_WRONLY ); // |O_NONBLOCK
    if ( fd == -1 )
	{
	LOG( AliHLTLog::kError, "AliHLTPipeController::Quit", "Pipe open" )
	    << "Error opening pipe " << fPipeName.c_str() << ": " 
	    << strerror( errno ) << AliHLTLog::kDec << " (" << errno
	    << ")." << ENDLOG;
	return;
	}
    AliUInt32_t index = ~(AliUInt32_t)0, value=0;
    write( fd, &index, 4 );
    write( fd, &value, 4 );
    close( fd );
    usleep( 10000 );
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
