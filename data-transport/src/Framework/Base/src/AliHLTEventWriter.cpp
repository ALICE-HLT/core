/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://wiki.kip.uni-heidelberg.de/ti/HLT/index.php/Main_Page
** or the corresponding page of the Heidelberg HLT group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author: artur $ - Initial Version by Artur Szostak
**
** $Id: $ 
**
***************************************************************************
*/

#include "AliHLTEventWriter.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTEventTriggerStruct.hpp"
#include "MLUCString.hpp"
#include <vector>
#include <fcntl.h>
#include <cerrno>
#include <glob.h>
#include <cstdlib>

#if __GNUC__>=3
using namespace std;
#endif


bool AliHLTEventWriter::WriteEvent(
		const AliEventID_t& eventID,
		const vector<AliHLTSubEventDescriptor::BlockData>& blocks,
		const AliHLTSubEventDataDescriptor* sedd,
		const AliHLTEventTriggerStruct* ets,
		unsigned long runNumber,
		const char* filePrefix
	)
    {
#if __GNUC__>=3
    return WriteEvent(eventID, blocks.size(), blocks.begin().base(), sedd, ets, runNumber, filePrefix);
#else
    return WriteEvent(eventID, blocks.size(), blocks.begin(), sedd, ets, runNumber, filePrefix);
#endif
    }


bool AliHLTEventWriter::WriteEvent(
		const AliEventID_t& eventID,
		AliUInt32_t blockCnt,
		const AliHLTSubEventDescriptor::BlockData* blocks,
		const AliHLTSubEventDataDescriptor* sedd,
		const AliHLTEventTriggerStruct* ets,
		unsigned long runNumber,
		const char* filePrefix
	)
    {
    unsigned long i;
    if ( blockCnt<=0 )
	return true;

    unsigned long highestCount=0, count;
    glob_t globDat;
    MLUCString prefix;

    char tmp[ 257 ];
    int ret;

    MLUCString filename = filePrefix;
    
    // We want to append a counting number to the file names to make sure that
    // they are unique. Here we first try find what the highest unused number is
    // before writing the files.

    if ( runNumber )
	{
	filename += "-";
	sprintf( tmp, "0x%08lX", (unsigned long)runNumber );
	filename += tmp;
	}

    filename += "-";
    sprintf( tmp, "0x%08lX", (unsigned long)eventID.fType );
    filename += tmp;
    filename += "-";
    sprintf( tmp, "0x%016LX", (unsigned long long)eventID.fNr );
    filename += tmp;
    filename += "-";
    prefix = filename;
    filename += "0x????????";
    filename += ".sedd";
    globDat.gl_offs = 0;
    globDat.gl_pathc = 0;
    globDat.gl_pathv = NULL;
    ret = glob( filename.c_str(), GLOB_MARK, NULL, &globDat );
    if ( !ret && globDat.gl_pathc>0 )
	{
	for ( unsigned n=0; n<globDat.gl_pathc; n++ )
	    {
	    char tmpNr[11];
	    char* cpErr;
	    strncpy( tmpNr, globDat.gl_pathv[n]+strlen(prefix.c_str()), 10 );
	    tmpNr[10] = '\0';
	    count = strtoul( tmpNr, &cpErr, 0 );
	    if ( *cpErr != '\0' )
		count=0;
	    else
		count++;
	    if ( count > highestCount )
		highestCount = count;
	    }
	}
    globfree( &globDat );
    
    // Again try find the highest counting number not used in the .ets file names.

    filename = filePrefix;
    
    if ( runNumber )
	{
	filename += "-";
	sprintf( tmp, "0x%08lX", (unsigned long)runNumber );
	filename += tmp;
	}
    filename += "-";
    sprintf( tmp, "0x%08lX", (unsigned long)eventID.fType );
    filename += tmp;
    filename += "-";
    sprintf( tmp, "0x%016LX", (unsigned long long)eventID.fNr );
    filename += tmp;
    filename += "-";
    prefix = filename;
    filename += "0x????????";
    filename += ".ets";
    globDat.gl_offs = 0;
    globDat.gl_pathc = 0;
    globDat.gl_pathv = NULL;
    ret = glob( filename.c_str(), GLOB_MARK, NULL, &globDat );
    if ( !ret && globDat.gl_pathc>0 )
	{
	for ( unsigned n=0; n<globDat.gl_pathc; n++ )
	    {
	    char tmpNr[11];
	    char* cpErr;
	    strncpy( tmpNr, globDat.gl_pathv[n]+strlen(prefix.c_str()), 10 );
	    tmpNr[10] = '\0';
	    count = strtoul( tmpNr, &cpErr, 0 );
	    if ( *cpErr != '\0' )
		count=0;
	    else
		count++;
	    if ( count > highestCount )
		highestCount = count;
	    }
	}
    globfree( &globDat );

    // Lastly we try find the highest counting number not used in the .bin file names.
    
    for ( i = 0; i < blockCnt; i++ )
	{
	filename = filePrefix;
	
	if ( runNumber )
	    {
	    filename += "-";
	    sprintf( tmp, "0x%08lX", (unsigned long)runNumber );
	    filename += tmp;
	    }

	filename += "-";
	sprintf( tmp, "0x%08lX", (unsigned long)eventID.fType );
	filename += tmp;
	filename += "-";
	sprintf( tmp, "0x%016LX", (unsigned long long)eventID.fNr );
	filename += tmp;
	filename += "-";
	prefix = filename;
	filename += "0x????????";
	
	filename += "-";
	sprintf( tmp, "0x%08lX", (unsigned long)i );
	filename += tmp;
	
	filename += ".bin";
	globDat.gl_offs = 0;
	globDat.gl_pathc = 0;
	globDat.gl_pathv = NULL;
	ret = glob( filename.c_str(), GLOB_MARK, NULL, &globDat );
	if ( !ret && globDat.gl_pathc>0 )
	    {
	    for ( unsigned n=0; n<globDat.gl_pathc; n++ )
		{
		char tmpNr[11];
		char* cpErr;
		strncpy( tmpNr, globDat.gl_pathv[n]+strlen(prefix.c_str()), 10 );
		tmpNr[10] = '\0';
		count = strtoul( tmpNr, &cpErr, 0 );
		if ( *cpErr != '\0' )
		    count=0;
		else
		    count++;
		if ( count > highestCount )
		    highestCount = count;
		}
	    }
	globfree( &globDat );
	}

    // Now we know how to build our filenames to be unique so we can write the
    // data blocks and descriptors to disk.

    filename = filePrefix;
    if ( runNumber )
	{
	filename += "-";
	sprintf( tmp, "0x%08lX", (unsigned long)runNumber );
	filename += tmp;
	}
    
    filename += "-";
    sprintf( tmp, "0x%08lX", (unsigned long)eventID.fType );
    filename += tmp;
    filename += "-";
    sprintf( tmp, "0x%016LX", (unsigned long long)eventID.fNr );
    filename += tmp;
    filename += "-";
    sprintf( tmp, "0x%08lX", highestCount );
    filename += tmp;
    
    filename += ".sedd";
    
    WriteToFile(filename.c_str(), sedd, sedd->fHeader.fLength);

    filename = filePrefix;
    if ( runNumber )
	{
	filename += "-";
	sprintf( tmp, "0x%08lX", (unsigned long)runNumber );
	filename += tmp;
	}
    
    filename += "-";
    sprintf( tmp, "0x%08lX", (unsigned long)eventID.fType );
    filename += tmp;
    filename += "-";
    sprintf( tmp, "0x%016LX", (unsigned long long)eventID.fNr );
    filename += tmp;
    filename += "-";
    sprintf( tmp, "0x%08lX", highestCount );
    filename += tmp;
    
    filename += ".ets";

    WriteToFile(filename.c_str(), ets, ets->fHeader.fLength);
    
    uint8* blockData;
    uint32 blockSize;

    for ( i = 0; i < blockCnt; i++ )
	{
	blockData = blocks[i].fData;
	blockSize = blocks[i].fSize;
	
	filename = filePrefix;
	if ( runNumber )
	    {
	    filename += "-";
	    sprintf( tmp, "0x%08lX", (unsigned long)runNumber );
	    filename += tmp;
	    }
	
	filename += "-";
	sprintf( tmp, "0x%08lX", (unsigned long)eventID.fType );
	filename += tmp;
	filename += "-";
	sprintf( tmp, "0x%016LX", (unsigned long long)eventID.fNr );
	filename += tmp;
	filename += "-";
	sprintf( tmp, "0x%08lX", highestCount );
	filename += tmp;
	
	filename += "-";
	sprintf( tmp, "0x%08lX", (unsigned long)i );
	filename += tmp;
	
	filename += ".bin";
	
	WriteToFile(filename.c_str(), blockData, blockSize);
	}
    return true;
    }


bool AliHLTEventWriter::WriteToFile(const char* filename, const void* data, unsigned long size)
    {
    int file = open(filename, O_WRONLY|O_CREAT|O_TRUNC, 0666);
    if (file == -1)
	{
	LOG(AliHLTLog::kError, "AliHLTEventWriter::WriteToFile", "Cannot Open File")
	    << "Unable to open file '" << filename << "': "
	    << strerror( errno ) << " (" << AliHLTLog::kDec << errno
	    << ")." << ENDLOG;
	return false;
	}
    
    unsigned long bytesWritten = 0;
    while (bytesWritten < size)
	{
	int ret = write(file, ((AliUInt8_t*)data)+bytesWritten, size-bytesWritten);
	if (ret < 0)
	    {
	    LOG( AliHLTLog::kError, "AliHLTEventWriter::WriteToFile", "Error Writing to File" )
	        << "Error writing to file '" << filename << "': "
	        << strerror( errno ) << " (" << AliHLTLog::kDec << errno
	        << ")." << ENDLOG;
	    // Close the file handle but to not check the result since there
	    // is already a problem with the file handle.
	    close(file);
	    return false;
	    }
	bytesWritten += ret;
	}
    
    bool closed = false;
    int attemptCount = 100;
    while (! closed)
        {
        // Only make a limited number of attempts to close the file handle.
        // This is important to prevent potential infinite loops, however unlikely.
        if (--attemptCount < 0) break;
        
        int ret = close(file);
        if (ret == 0)
            {
            closed = true;
            }
        else
            {
            // Try again if we were interrupted while in the close() syscall.
            if (errno == EINTR) continue;
            
            // If we got here then there is a more serious problem with
            // the file handle so just log an error and exit.
	    LOG( AliHLTLog::kError, "AliHLTEventWriter::WriteToFile", "Cannot Close File" )
	        << "Unable to close file '" << filename << "': "
	        << strerror( errno ) << " (" << AliHLTLog::kDec << errno
	        << ")." << ENDLOG;
	    return false;
	    }
        }
    return closed;
    }


/*
***************************************************************************
**
** $Author: artur $ - Initial Version by Artur Szostak
**
** $Id:  $ 
**
***************************************************************************
*/
