/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTLog.hpp"
#include <time.h>


AliHLTDescriptorHandler::AliHLTDescriptorHandler( unsigned maxPreSubEventsExp2, bool grow ):
    fSEDDs( 10, maxPreSubEventsExp2, true ),
    fSEDDCache( maxPreSubEventsExp2, grow, false )
    {
    }

AliHLTDescriptorHandler::~AliHLTDescriptorHandler()
    {
    }


AliHLTDescriptorHandler::operator bool()
    {
    return true;
    }

AliHLTSubEventDataDescriptor* AliHLTDescriptorHandler::GetFreeEventDescriptor( AliEventID_t eventID, AliUInt32_t blockCount )
    {
    AliHLTSubEventDataDescriptor* sedd = NULL;
    if ( eventID==AliEventID_t( kAliEventTypeUnknown, ~0ULL ) )
	{
	LOG( AliHLTLog::kError, "AliHLTDescriptorHandler::GetFreeEventDescriptor", "Invalid Event ID" )
	    << "Invalid event ID passed." << ENDLOG;
	}
#if 0
#warning DEBUG TEST CODE
    sedd = (AliHLTSubEventDataDescriptor*)new AliUInt8_t[ sizeof(AliHLTSubEventDataDescriptor)+
							  blockCount*sizeof(AliHLTSubEventDataBlockDescriptor) ];
#else
    sedd = (AliHLTSubEventDataDescriptor*)fSEDDCache.Get( sizeof(AliHLTSubEventDataDescriptor)+
							  blockCount*sizeof(AliHLTSubEventDataBlockDescriptor) );
#endif
    if ( sedd )
	{
	sedd->fHeader.fLength = sizeof( AliHLTSubEventDataDescriptor )+
				sizeof(AliHLTSubEventDataBlockDescriptor)*blockCount;
	sedd->fDataBlockCount = blockCount;
	sedd->fHeader.fType.fID = ALIL3SUBEVENTDATADESCRIPTOR_TYPE;
	sedd->fHeader.fSubType.fID = 0;
	sedd->fHeader.fVersion = 5;
	sedd->fEventBirth_s = 0;
	sedd->fEventBirth_us = 0;
	sedd->fOldestEventBirth_s = 0;
	sedd->fEventID = eventID;
	sedd->fStatusFlags = 0;
	sedd->fDataType.fID = UNKNOWN_DATAID;
	for ( AliUInt32_t n = 0; n < blockCount; n++ )
	    {
	    sedd->fDataBlocks[n].fShmID.fShmType = kInvalidShmType;
	    sedd->fDataBlocks[n].fShmID.fKey.fID = (AliUInt64_t)-1;
	    sedd->fDataBlocks[n].fBlockSize = 0;
	    sedd->fDataBlocks[n].fBlockOffset = 0;
	    sedd->fDataBlocks[n].fProducerNode = ~0;
	    sedd->fDataBlocks[n].fDataType.fID = UNKNOWN_DATAID;
	    sedd->fDataBlocks[n].fDataType.fID = UNKNOWN_DATAID;
	    sedd->fDataBlocks[n].fDataOrigin.fID = UNKNOWN_DATAORIGIN;
	    sedd->fDataBlocks[n].fDataSpecification = ~(AliUInt32_t)0;
	    for ( int j = 0; j < kAliHLTSEDBDAttributeCount; j++ )
		sedd->fDataBlocks[n].fAttributes[j] = kAliHLTUnspecifiedAttribute;
	    }
	SEDDData sd;
	sd.fEventID = eventID;
	sd.fSEDDPtr = sedd;
	fSEDDs.Add( sd, eventID );
	}
    return sedd;
    }

AliHLTSubEventDataDescriptor* AliHLTDescriptorHandler::CloneEventDescriptor( AliHLTSubEventDataDescriptor const* oldSEDD )
    {
    AliHLTSubEventDataDescriptor* sedd = NULL;
    if ( oldSEDD->fEventID==AliEventID_t( kAliEventTypeUnknown, ~0ULL ) )
	{
	LOG( AliHLTLog::kError, "AliHLTDescriptorHandler::GetFreeEventDescriptor", "Invalid Event ID" )
	    << "Invalid event ID passed." << ENDLOG;
	}
#if 0
#warning DEBUG TEST CODE
    sedd = (AliHLTSubEventDataDescriptor*)new AliUInt8_t[ oldSEDD->fHeader.fLength ];
#else
    sedd = (AliHLTSubEventDataDescriptor*)fSEDDCache.Get( oldSEDD->fHeader.fLength );
#endif
    if ( sedd )
	{
	memcpy( sedd, oldSEDD, oldSEDD->fHeader.fLength );
	SEDDData sd;
	sd.fEventID = oldSEDD->fEventID;
	sd.fSEDDPtr = sedd;
	fSEDDs.Add( sd, sd.fEventID );
	}
    return sedd;
    }


// bool AliHLTDescriptorHandler::FindSEDD( AliEventID_t eventID, bool& isPre, unsigned long& ndx )
//     {
//     AliUInt32_t count;
//     return FindSEDD( eventID, isPre, ndx, count );
//     }

bool AliHLTDescriptorHandler::FindSEDD( AliEventID_t eventID, unsigned long& ndx )
    {
    if ( eventID == AliEventID_t( kAliEventTypeUnknown, ~0ULL ) )
	{
	LOG( AliHLTLog::kError, "AliHLTDescriptorHandler::FindSEDD", "Invalid Event ID" )
	    << "Invalid event ID passed." << ENDLOG;
	return false;
	}
    //if ( MLUCVectorSearcher<SEDDData,AliEventID_t>::FindElement( fSEDDs, &SearchSEDD, eventID, ndx ) )
    if ( fSEDDs.FindElement( eventID, ndx ) )
	{
#if 0
	SEDDData* sd;
	sd = fSEDDs.GetPtr( ndx );
#endif
	return true;
	}
    return false;
    }

AliHLTSubEventDataDescriptor* AliHLTDescriptorHandler::GetEventDescriptor( unsigned long ndx )
    {
    SEDDData* sd;
    sd = fSEDDs.GetPtr( ndx );
    return sd ? sd->fSEDDPtr : NULL;
    }


void AliHLTDescriptorHandler::ReleaseEventDescriptor( AliEventID_t eventID )
    {
    unsigned long ndx;
    SEDDData* sd;
    //if ( MLUCVectorSearcher<SEDDData,AliEventID_t>::FindElement( fSEDDs, &SearchSEDD, eventID, ndx ) )
    if ( fSEDDs.FindElement( eventID, ndx ) )
	{
	sd = fSEDDs.GetPtr( ndx );
	if ( sd && sd->fSEDDPtr )
	    {
#if 0
#warning DEBUG TEST CODE
	    delete [] (AliUInt8_t*)sd->fSEDDPtr;
#else
	    fSEDDCache.Release( (uint8*)sd->fSEDDPtr );
#endif
	    }
	fSEDDs.Remove( ndx );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTDescriptorHandler::ReleaseEventDescriptor", "Unknown descriptor" )
	    << "Event descriptor to release (Event 0x" << AliHLTLog::kHex << eventID << "/" 
	    << AliHLTLog::kDec << eventID << ") could not be found..." << ENDLOG;
	}
    }






/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
