/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSimpleFileEventStorage.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTGetNodeID.hpp"
#include "BCLNetworkData.hpp"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <dirent.h>

#define CURRENTSIMPLEFILEVENTSTORAGEVERSION 2

const unsigned AliHLTSimpleFileEventStorage::kPadSize = 1<<3; // kPadSize has to be a power of two



AliHLTSimpleFileEventStorage::AliHLTSimpleFileEventStorage():
    fFEDCache( 3, false, false )
    {
    fVersion = 0;

    fCurrentNdxFileReadNr = 0;
    fCurrentNdxFileReadOffset = 0;
    
    fCurrentDataFileReadNr = 0;
    fCurrentDataFileReadOffset = 0;
    
    fCurrentTriggerFileReadNr = 0;
    fCurrentTriggerFileReadOffset = 0;

    fNdxReadFH = -1;
    fDataReadFH = -1;
    fTriggerReadFH = -1;
    
    fCurrentNdxFileWriteNr = 0;
    fCurrentNdxFileWriteOffset = 0;
    
    fCurrentDataFileWriteNr = 0;
    fCurrentDataFileWriteOffset = 0;
    
    fCurrentTriggerFileWriteNr = 0;
    fCurrentTriggerFileWriteOffset = 0;

    fNdxWriteFH = -1;
    fDataWriteFH = -1;
    fTriggerWriteFH = -1;

    fMaxNdxFileSize = 1<<30;
    fMaxDataFileSize = 1<<30;
    fMaxTriggerFileSize = 1<<30;
    }

AliHLTSimpleFileEventStorage::~AliHLTSimpleFileEventStorage()
    {
    CloseStorage();
    }

int AliHLTSimpleFileEventStorage::OpenStorage( const char* storageName, bool doWrite )
    {
    if ( fNdxWriteFH!=-1 || fDataWriteFH!=-1 || fNdxReadFH!=-1 || fDataReadFH!=-1 )
	return EBUSY;

    int ret;
    DIR *dir;
    dir = opendir( storageName );
    uint32 version;
    if ( !dir )
	{
	ret = mkdir( storageName, 0777 );
	if ( ret == -1 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::OpenStorage", "Unable to create storage directory" )
		<< "Unable to create storage directory '" << storageName << "': " 
		<< strerror(errno) << AliHLTLog::kDec << " (" << errno << ")." << ENDLOG;
	    return errno;
	    }

	int versionFH;
	MLUCString filename;
	filename = storageName;
	filename += "/version";
	version = CURRENTSIMPLEFILEVENTSTORAGEVERSION;
	BCLNetworkData::Transform( version, kNativeByteOrder, kNetworkByteOrder );
	versionFH = open( filename.c_str(), O_WRONLY|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH );
	if ( versionFH==-1 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::OpenStorage", "Unable to create storage version file" )
		<< "Unable to create storage version file '" << filename.c_str() << "': " 
		<< strerror(errno) << AliHLTLog::kDec << " (" << errno << ")." << ENDLOG;
	    return errno;
	    }
	ret = Write( versionFH, &version, sizeof(version) );
	if ( ret != sizeof(version) )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::OpenStorage", "Error writing storage version" )
		<< "Error writing storage version to file '" << filename.c_str() << "': " 
		<< strerror(errno) << AliHLTLog::kDec << " (" << errno << ")." << ENDLOG;
	    return errno;
	    }
	close( versionFH );
	}
    else
	{
	closedir( dir );
	int versionFH;
	MLUCString filename;
	filename = storageName;
	filename += "/version";
	versionFH = open( filename.c_str(), O_RDONLY );
	if ( versionFH==-1 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::OpenStorage", "Unable to open storage version file" )
		<< "Unable to open storage version file '" << filename.c_str() << "': " 
		<< strerror(errno) << AliHLTLog::kDec << " (" << errno << ")." << ENDLOG;
	    return errno;
	    }
	ret = Read( versionFH, &version, sizeof(version) );
	if ( ret != sizeof(version) )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::OpenStorage", "Error reading storage version" )
		<< "Error reading storage version to file '" << filename.c_str() << "': " 
		<< strerror(errno) << AliHLTLog::kDec << " (" << errno << ")." << ENDLOG;
	    return errno;
	    }
	close( versionFH );
	}
    BCLNetworkData::Transform( version, kNetworkByteOrder, kNativeByteOrder );
    fVersion = version;

    if ( fVersion > CURRENTSIMPLEFILEVENTSTORAGEVERSION )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::OpenStorage", "Unknown storage version" )
	    << "Unknown storage version " << AliHLTLog::kDec
	    << fVersion << ". Supported versions for reading: 1-" << CURRENTSIMPLEFILEVENTSTORAGEVERSION
	    << " - Supported versions for writing: " << CURRENTSIMPLEFILEVENTSTORAGEVERSION
	    << "." << ENDLOG;
	return ENOTSUP;
	}

    fNodeID = AliHLTGetNodeID();


    fStorageName = storageName;
    fWriteable = doWrite;

    ret = OpenRead();
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::OpenStorage", "Error opening storage for read access" )
	    << "Error opening storage '" << fStorageName.c_str() << "' for read access: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << ")." << ENDLOG;
	return ret;
	}

    if ( fWriteable )
	{
	if ( fVersion == CURRENTSIMPLEFILEVENTSTORAGEVERSION )
	    ret = OpenWrite();
	else
	    {
	    LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::OpenStorage", "Cannot write to old version storage" )
		<< "Unable to write to storage version older than current version. Current version: "
		<< AliHLTLog::kDec << CURRENTSIMPLEFILEVENTSTORAGEVERSION << " - Present version: "
		<< fVersion << "." << ENDLOG;
	    ret = ENOTSUP;
	    }
	}

    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::OpenStorage", "Error opening storage for write access" )
	    << "Error opening storage '" << fStorageName.c_str() << "' for write access: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << ")." << ENDLOG;
	}

    return ret;
    }

int AliHLTSimpleFileEventStorage::CloseStorage()
    {
    CloseWrite();
    CloseRead();
    fStorageName = "";
    return 0;
    }

void AliHLTSimpleFileEventStorage::DumpEventLocation( AliHLTLog::TLogLevel lvl, const char* keyword, const AliHLTSimpleFileEventStorage::EventLocationDescriptor* eld )
    {
    if ( !eld )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::DumpEventLocation", "Invalid event location descriptor" )
	    << "Invalid event location descriptor specified (NULL pointer)." << ENDLOG;
	return;
	}
    if ( strcmp( GetStorageTypeName(), eld->GetStorageTypeName() ) )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::DumpEventLocation", "Wrong event location descriptor type" )
	    << "Wrong type of event location descriptor specified. '" << eld->GetStorageTypeName()
	    << "' found; '" << GetStorageTypeName()
	    << "' expected." << ENDLOG;
	return;
	}
    LOG( lvl, "AliHLTSimpleFileEventStorage::DumpEventLocation", keyword )
	<< "Event Location Dump - Index File Nr: 0x" << AliHLTLog::kHex 
	<< reinterpret_cast<const SimpleFileEventLocationDescriptor*>( eld )->GetNdxFileNr() << " (" << AliHLTLog::kDec << reinterpret_cast<const SimpleFileEventLocationDescriptor*>( eld )->GetNdxFileNr()
	<< ") - File Offset: 0x" << AliHLTLog::kHex << reinterpret_cast<const SimpleFileEventLocationDescriptor*>( eld )->GetFileOffset()
	<< " (" << AliHLTLog::kDec << reinterpret_cast<const SimpleFileEventLocationDescriptor*>( eld )->GetFileOffset() << ")." << ENDLOG;
    }


int AliHLTSimpleFileEventStorage::ResetEventLocation()
    {
    CloseRead();
    return OpenRead();
    }

int AliHLTSimpleFileEventStorage::GetNextEventLocation( AliHLTSimpleFileEventStorage::EventLocationDescriptor* eld, AliUInt64_t eventSkipCount )
    {
    if ( !eld )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::GetNextEventLocation", "Invalid event location descriptor" )
	    << "Invalid event location descriptor specified (NULL pointer)." << ENDLOG;
	return EFAULT;
	}
    if ( strcmp( GetStorageTypeName(), eld->GetStorageTypeName() ) )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::GetNextEventLocation", "Wrong event location descriptor type" )
	    << "Wrong type of event location descriptor specified. '" << eld->GetStorageTypeName()
	    << "' found; '" << GetStorageTypeName()
	    << "' expected." << ENDLOG;
	return EINVAL;
	}
    AliUInt64_t n;
    int ret;
    FileEventData* fed = NULL;
//     LOG( AliHLTLog::kDebug, "AliHLTSimpleFileEventStorage::GetNextEventLocation", "Event skip count" )
// 	<< "Event skip count: " << AliHLTLog::kDec << eventSkipCount << ENDLOG;
    for ( n=0; n<eventSkipCount; n++ )
	{
	ret = ReadFileEventData( fed, NULL );
	if ( fed )
	    {
	    fFEDCache.Release( reinterpret_cast<uint8*>( fed ) );
	    fed = NULL;
	    }
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::GetNextEventLocation", "Error skipping events" )
		<< "Error skipping " << AliHLTLog::kDec << eventSkipCount << " events: "
		<< strerror(ret) << " (" << ret << ")." << ENDLOG;
	    return ret;
	    }
// 	LOG( AliHLTLog::kDebug, "AliHLTSimpleFileEventStorage::GetNextEventLocation", "Event storage position" )
// 	    << "Event storage position " << AliHLTLog::kDec << n << ": file index nr: 0x" << AliHLTLog::kHex << fCurrentNdxFileReadNr << " ("
// 	    << AliHLTLog::kDec << fCurrentNdxFileReadNr << ") - file offset: 0x" << AliHLTLog::kHex
// 	    << fCurrentNdxFileReadOffset << " (" << AliHLTLog::kDec << fCurrentNdxFileReadOffset
// 	    << ")." << ENDLOG;
	}
    return FillEventLocationDescriptor( reinterpret_cast<SimpleFileEventLocationDescriptor*>( eld ) );
    }

int AliHLTSimpleFileEventStorage::FindEventLocation( AliEventID_t eventID, AliHLTSimpleFileEventStorage::EventLocationDescriptor* eld )
    {
    if ( !eld )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::FindEventLocation", "Invalid event location descriptor" )
	    << "Invalid event location descriptor specified (NULL pointer)." << ENDLOG;
	return EFAULT;
	}
    if ( strcmp( GetStorageTypeName(), eld->GetStorageTypeName() ) )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::FindEventLocation", "Wrong event location descriptor type" )
	    << "Wrong type of event location descriptor specified. '" << eld->GetStorageTypeName()
	    << "' found; '" << GetStorageTypeName()
	    << "' expected." << ENDLOG;
	return EINVAL;
	}
    int ret=0;
    FileEventData* fed = NULL;
    do
	{
	if ( fed )
	    {
	    fFEDCache.Release( reinterpret_cast<uint8*>( fed ) );
	    fed = NULL;
	    }
	ret = ReadFileEventData( fed, reinterpret_cast<SimpleFileEventLocationDescriptor*>( eld ) );
	}
    while ( !ret && fed && fed->fEvent.fEventID!=eventID );
    if ( !ret && fed && fed->fEvent.fEventID==eventID )
	return 0;
    if ( fed )
	fFEDCache.Release( reinterpret_cast<uint8*>( fed ) );
    if ( ret )
	return ret;
    if ( !ret && !fed )
	return ENOMEM;
    return EIO;
    }

AliHLTSimpleFileEventStorage::EventLocationDescriptor* AliHLTSimpleFileEventStorage::AllocEventLocation()
    {
    return new SimpleFileEventLocationDescriptor();
    }

void AliHLTSimpleFileEventStorage::FreeEventLocation( AliHLTSimpleFileEventStorage::EventLocationDescriptor* eld )
    {
    if ( eld )
	delete eld;
    }

// int AliHLTSimpleFileEventStorage::GetEventDescriptor( const AliHLTEventStorageInterface::EventLocationDescriptor* eld, AliHLTSubEventDataDescriptor*& sedd )
//     {
//     return 0;
//     }

// int AliHLTSimpleFileEventStorage::GetEventData( const AliHLTEventStorageInterface::EventLocationDescriptor* eld, AliHLTSubEventDataDescriptor* sedd, std::vector<AliUInt8_t*> dataBlockPtrs )
//     {
//     return 0;
//     }

int AliHLTSimpleFileEventStorage::GetEventStorageMetaData( const EventLocationDescriptor* eld, EventStorageMetaData* esmd )
    {
    if ( !esmd )
	return EFAULT;
    int ret;
    FileEventData* fed = NULL;
    ret = ReadFileEventData( eld, fed );
    if ( ret )
	return ret;


    esmd->fEventWritten_s = fed->fEvent.fEventWritten_s;
    esmd->fEventWritten_us = fed->fEvent.fEventWritten_us;
    esmd->fEventWriteNode = fed->fEvent.fEventWriteNode;
    esmd->fEventWriteNodeByteOrder = fed->fEvent.fEventWriteNodeByteOrder;

    if ( fed )
	fFEDCache.Release( reinterpret_cast<uint8*>( fed ) );
    return 0;
    }

int AliHLTSimpleFileEventStorage::GetSubEventDataDescriptor( const EventLocationDescriptor* eld, AliHLTSubEventDataDescriptor* sedd )
    {
    if ( !sedd )
	return EFAULT;
    int ret;
    FileEventData* fed = NULL;
    ret = ReadFileEventData( eld, fed );
    if ( ret )
	return ret;

    sedd->fHeader.fLength = fed->fEvent.fHeader.fLength;
    sedd->fHeader.fType.fID = fed->fEvent.fHeader.fType.fID;
    sedd->fHeader.fSubType.fID = fed->fEvent.fHeader.fSubType.fID;
    sedd->fHeader.fVersion = fed->fEvent.fHeader.fVersion;
    sedd->fEventID = fed->fEvent.fEventID;
    sedd->fEventBirth_s = fed->fEvent.fEventBirth_s;
    sedd->fEventBirth_us = fed->fEvent.fEventBirth_us;
    sedd->fDataType.fID = fed->fEvent.fDataType.fID;
    sedd->fDataBlockCount = fed->fEvent.fDataBlockCount;
    

    if ( fed )
	fFEDCache.Release( reinterpret_cast<uint8*>( fed ) );
    return 0;
    }

int AliHLTSimpleFileEventStorage::GetEventBlockData( const EventLocationDescriptor* eld, std::vector<AliHLTSubEventDescriptor::BlockData>& dataBlocks )
    {
    int ret;
    FileEventData* fed = NULL;
    ret = ReadFileEventData( eld, fed );
    if ( ret )
	return ret;

    AliUInt32_t n;
    AliHLTSubEventDescriptor::BlockData block;

    for ( n = 0; n < fed->fEvent.fDataBlockCount; n++ )
	{
	block.fData = NULL;
	//block.fShmKey = fed->fBlocks[n].;
	block.fOffset = 0;
	block.fSize = fed->fBlocks[n].fBlockSize;
// 	printf( "%s:%d: block %d size: %d (0x%08lX) [%d (0x%08lX)]\n", __FILE__, __LINE__, n, block.fSize, block.fSize, fed->fBlocks[n].fBlockSize, fed->fBlocks[n].fBlockSize );
	block.fProducerNode = fed->fBlocks[n].fProducerNode;
	block.fDataType = fed->fBlocks[n].fDataType;
	block.fDataOrigin = fed->fBlocks[n].fDataOrigin;
	block.fDataSpecification = fed->fBlocks[n].fDataSpecification;
	for ( int j = 0; j < kAliHLTSEDBDAttributeCount; j++ )
	    block.fAttributes[j] = fed->fBlocks[n].fAttributes[j];
	dataBlocks.insert( dataBlocks.end(), block );
	}

    if ( fed )
	fFEDCache.Release( reinterpret_cast<uint8*>( fed ) );
    return 0;
    }

int AliHLTSimpleFileEventStorage::GetEventData( const EventLocationDescriptor* eld, const std::vector<AliHLTSubEventDescriptor::BlockData>& dataBlocks )
    {
    int ret;
    FileEventData* fed = NULL;
    ret = ReadFileEventData( eld, fed );
    if ( ret )
	return ret;

    AliUInt32_t n;
    AliUInt32_t cnt;
    if ( dataBlocks.size()<fed->fEvent.fDataBlockCount )
	cnt = dataBlocks.size();
    else
	cnt = fed->fEvent.fDataBlockCount;
    for ( n = 0; n < cnt; n++ )
	{
	if ( dataBlocks[n].fSize!=fed->fBlocks[n].fBlockSize || !dataBlocks[n].fData )
	    {
	    if ( dataBlocks[n].fSize!=fed->fBlocks[n].fBlockSize )
		ret = EINVAL;
	    else
		ret = EFAULT;
	    break;
	    }
	if ( fed->fBlocks[n].fFileNr != fCurrentDataFileReadNr )
	    {
	    CloseDataFileRead();
	    fCurrentDataFileReadNr = fed->fBlocks[n].fFileNr;
	    ret = OpenDataFileRead();
	    if ( ret )
		break;
	    }
	if ( (off_t)fed->fBlocks[n].fBlockOffset != fCurrentDataFileReadOffset )
	    {
	    fCurrentDataFileReadOffset = fed->fBlocks[n].fBlockOffset;
	    ret = SeekDataFileRead();
	    if ( ret )
		break;
	    }
	ret = Read( fDataReadFH, dataBlocks[n].fData, dataBlocks[n].fSize );
	if ( ret != (int)dataBlocks[n].fSize )
	    {
	    ret = errno;
	    break;
	    }
	else
	    ret = 0;
	fCurrentDataFileReadOffset = lseek( fDataReadFH, 0, SEEK_CUR );
	}
    if ( fed )
	fFEDCache.Release( reinterpret_cast<uint8*>( fed ) );
    if ( ret )
	{
	// XXX ...
	return ret;
	}
    return 0;
    }

int AliHLTSimpleFileEventStorage::GetEventTriggerSize( const EventLocationDescriptor* eld, unsigned long& size )
    {
    int ret;
    FileEventData* fed = NULL;
    ret = ReadFileEventData( eld, fed );
    if ( ret )
	return ret;

    size = fed->fTrigger.fBlockSize;
    if ( fed )
	fFEDCache.Release( reinterpret_cast<uint8*>( fed ) );
    return 0;
    }

int AliHLTSimpleFileEventStorage::GetEventTriggerData( const EventLocationDescriptor* eld, AliHLTEventTriggerStruct* ets )
    {
    int ret;
    FileEventData* fed = NULL;
    ret = ReadFileEventData( eld, fed );
    if ( ret )
	return ret;

    if ( fed->fTrigger.fFileNr != fCurrentTriggerFileReadNr )
	{
	CloseTriggerFileRead();
	fCurrentTriggerFileReadNr = fed->fTrigger.fFileNr;
	ret = OpenTriggerFileRead();
	if ( ret )
	    {
	    if ( fed )
		fFEDCache.Release( reinterpret_cast<uint8*>( fed ) );
	    return ret;
	    }
	}
    if ( !ret && (off_t)fed->fTrigger.fBlockOffset != fCurrentTriggerFileReadOffset )
	{
	fCurrentTriggerFileReadOffset = fed->fTrigger.fBlockOffset;
	ret = SeekTriggerFileRead();
	if ( ret )
	    {
	    if ( fed )
		fFEDCache.Release( reinterpret_cast<uint8*>( fed ) );
	    return ret;
	    }
	}
    ret = Read( fTriggerReadFH, ets, fed->fTrigger.fBlockSize );
    if ( ret != (int)fed->fTrigger.fBlockSize )
	{
	ret = errno;
	}
    else
	ret = 0;
    fCurrentTriggerFileReadOffset = lseek( fTriggerReadFH, 0, SEEK_CUR );
    
    if ( fed )
	fFEDCache.Release( reinterpret_cast<uint8*>( fed ) );
    return ret;
    }


int AliHLTSimpleFileEventStorage::WriteEvent( const AliHLTSubEventDataDescriptor* sedd, 
					      unsigned long blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, 
					      const AliHLTEventTriggerStruct* ets, 
					      AliHLTEventStorageInterface::EventLocationDescriptor* eld )
    {
    if ( !fWriteable )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::WriteEvent", "Write attempt for read-only storage" )
	    << "Attempt to write event into storage '" << fStorageName.c_str() 
	    << "' opened in read-only mode." << ENDLOG;
	return EACCES;
	}

    if ( eld && strcmp( GetStorageTypeName(), eld->GetStorageTypeName() ) )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::WriteEvent", "Wrong event location descriptor type" )
	    << "Wrong type of event location descriptor specified. '" << eld->GetStorageTypeName()
	    << "' found; '" << GetStorageTypeName()
	    << "' expected." << ENDLOG;
	return EINVAL;
	}

    unsigned long len = sizeof(FileEventData)+sizeof(FileEventDataBlockDescriptor)*blockCnt;
    FileEventData* fed;
    fed = reinterpret_cast<FileEventData*>( fFEDCache.Get( len ) );
    if ( !fed )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::WriteEvent", "Out of memory" )
	    << "Out of memory trying to allocate file event data structure of " << AliHLTLog::kDec
	    << len
	    << " bytes for " << blockCnt << " data blocks." << ENDLOG;
	return ENOMEM;
	}

    int ret;
    unsigned long i;
    fed->fEvent.fHeader.fLength = len;
    LOG( AliHLTLog::kDebug, "AliHLTSimpleFileEventStorage::WriteEvent", "FED Length" )
	<< "FED Length: " << AliHLTLog::kDec << len << " (0x" << AliHLTLog::kHex
	<< len << ")." << ENDLOG;
    BCLNetworkData::Transform( fed->fEvent.fHeader.fLength, kNativeByteOrder, kNetworkByteOrder );
    LOG( AliHLTLog::kDebug, "AliHLTSimpleFileEventStorage::WriteEvent", "FED Header Length" )
	<< "FED header length: " << AliHLTLog::kDec << fed->fEvent.fHeader.fLength << " (0x" << AliHLTLog::kHex
	<< fed->fEvent.fHeader.fLength << ")." << ENDLOG;
    fed->fEvent.fHeader.fType.fID = ALIHLTFILEEVENTDATA_TYPE;
    BCLNetworkData::Transform( fed->fEvent.fHeader.fType.fID, kNativeByteOrder, kNetworkByteOrder );
    fed->fEvent.fHeader.fSubType.fID = 0;
    fed->fEvent.fHeader.fVersion = 1;

    ret = FillEventDescriptor( sedd, fed->fEvent );
    if ( ret )
	{
	fFEDCache.Release( reinterpret_cast<uint8*>( fed ) );
	return ret;
	}

    ret = WriteTriggerData( ets, fed->fTrigger );
    if ( ret )
	{
	fFEDCache.Release( reinterpret_cast<uint8*>( fed ) );
	return ret;
	}

    AliUInt32_t tmp;
    for ( i = 0; i < blockCnt; i++ )
	{
	LOG( AliHLTLog::kDebug, "AliHLTSimpleFileEventStorage::WriteEvent", "Block size" )
	    << "Event 0x" << AliHLTLog::kHex << sedd->fEventID << " (" << AliHLTLog::kDec << sedd->fEventID
	    << ") block " << i << " size: " << blocks[i].fSize << " (0x" << AliHLTLog::kHex
	    << blocks[i].fSize << ")." << ENDLOG;
	ret = WriteBlock( blocks[i], fed->fBlocks[i], (i==blockCnt-1) );
	BCLNetworkData::Transform( fed->fBlocks[i].fBlockSize, tmp, kNetworkByteOrder, kNativeByteOrder );
	LOG( AliHLTLog::kDebug, "AliHLTSimpleFileEventStorage::WriteEvent", "Block size" )
	    << "Event 0x" << AliHLTLog::kHex << sedd->fEventID << " (" << AliHLTLog::kDec << sedd->fEventID
	    << ") block " << i << " size: " << blocks[i].fSize << " (0x" << AliHLTLog::kHex
	    << blocks[i].fSize << ") - FED block size: " << AliHLTLog::kDec << tmp
	    << " (0x" << AliHLTLog::kHex << tmp << ")." << ENDLOG;
	if ( ret )
	    {
	    fFEDCache.Release( reinterpret_cast<uint8*>( fed ) );
	    return ret;
	    }
	}

    ret = WriteFileEventData( fed, reinterpret_cast<SimpleFileEventLocationDescriptor*>( eld ) );
    fFEDCache.Release( reinterpret_cast<uint8*>( fed) );
    return ret;
    }

void AliHLTSimpleFileEventStorage::CloseRead()
    {
    if ( fNdxReadFH != -1 )
	{
	close( fNdxReadFH );
	fNdxReadFH = -1;
	}
    if ( fDataReadFH != -1 )
	{
	close( fDataReadFH );
	fDataReadFH = -1;
	}
    if ( fTriggerReadFH != -1 )
	{
	close( fTriggerReadFH );
	fTriggerReadFH = -1;
	}

    fCurrentNdxFileReadNr = 0;
    fCurrentNdxFileReadOffset = 0;
    
    fCurrentDataFileReadNr = 0;
    fCurrentDataFileReadOffset = 0;

    fCurrentTriggerFileReadNr = 0;
    fCurrentTriggerFileReadOffset = 0;

    fVersion = 0;
    }

void AliHLTSimpleFileEventStorage::CloseWrite()
    {
    if ( fNdxWriteFH != -1 )
	{
	close( fNdxWriteFH );
	fNdxWriteFH = -1;
	}
    if ( fDataWriteFH != -1 )
	{
	close( fDataWriteFH );
	fDataWriteFH = -1;
	}
    if ( fTriggerWriteFH != -1 )
	{
	close( fTriggerWriteFH );
	fTriggerWriteFH = -1;
	}

    fCurrentNdxFileWriteNr = 0;
    fCurrentNdxFileWriteOffset = 0;
    
    fCurrentDataFileWriteNr = 0;
    fCurrentDataFileWriteOffset = 0;

    fCurrentTriggerFileWriteNr = 0;
    fCurrentTriggerFileWriteOffset = 0;
    }

int AliHLTSimpleFileEventStorage::OpenRead()
    {
    MLUCString filename;
    int flags;
    int acc;
    char buf[ 128 ];
    int versionFH;
    int ret;

    filename = fStorageName;
    filename += "/version";
    uint32 version;
    versionFH = open( filename.c_str(), O_RDONLY );
    if ( versionFH==-1 )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::OpenStorage", "Unable to open storage version file" )
	    << "Unable to open storage version file '" << filename.c_str() << "': " 
	    << strerror(errno) << AliHLTLog::kDec << " (" << errno << ")." << ENDLOG;
	return errno;
	}
    ret = Read( versionFH, &version, sizeof(version) );
    if ( ret != sizeof(version) )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::OpenStorage", "Error reading storage version" )
	    << "Error reading storage version to file '" << filename.c_str() << "': " 
	    << strerror(errno) << AliHLTLog::kDec << " (" << errno << ")." << ENDLOG;
	return errno;
	}
    close( versionFH );
    BCLNetworkData::Transform( version, kNetworkByteOrder, kNativeByteOrder );

    filename = fStorageName;
    sprintf( buf, "/ndx-0x%016lX", fCurrentNdxFileReadNr );
    filename += buf;
    if ( fWriteable )
	{
	acc = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH;
	flags = O_RDWR;
	}
    else
	{
	acc = S_IRUSR|S_IRGRP|S_IROTH;
	flags = O_RDONLY;
	}
    fNdxReadFH = open( filename.c_str(), flags|O_CREAT, acc );
    if ( fNdxReadFH == -1 )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::OpenStorage", "Unable to open index file" )
	    << "Unable to open index file '" << filename.c_str() << "' for reading: " 
	    << strerror(errno) << AliHLTLog::kDec << " (" << errno << ")." << ENDLOG;
	return errno;
	}

    filename = fStorageName;
    sprintf( buf, "/data-0x%016LX", (unsigned long long)fCurrentDataFileReadNr );
    filename += buf;
    if ( fWriteable )
	{
	acc = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH;
	flags = O_RDWR;
	}
    else
	{
	acc = S_IRUSR|S_IRGRP|S_IROTH;
	flags = O_RDONLY;
	}
    fDataReadFH = open( filename.c_str(), flags|O_CREAT, acc );
    if ( fDataReadFH == -1 )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::OpenStorage", "Unable to open initial data file" )
	    << "Unable to open initial data file '" << filename.c_str() << "' for reading: " 
	    << strerror(errno) << AliHLTLog::kDec << " (" << errno << ")." << ENDLOG;
	close( fNdxReadFH );
	return errno;
	}

    filename = fStorageName;
    sprintf( buf, "/trigger-0x%016LX", (unsigned long long)fCurrentTriggerFileReadNr );
    filename += buf;
    if ( fWriteable )
	{
	acc = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH;
	flags = O_RDWR;
	}
    else
	{
	acc = S_IRUSR|S_IRGRP|S_IROTH;
	flags = O_RDONLY;
	}
    fTriggerReadFH = open( filename.c_str(), flags|O_CREAT, acc );
    if ( fTriggerReadFH == -1 )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::OpenStorage", "Unable to open initial trigger file" )
	    << "Unable to open initial trigger file '" << filename.c_str() << "' for reading: " 
	    << strerror(errno) << AliHLTLog::kDec << " (" << errno << ")." << ENDLOG;
	close( fNdxReadFH );
	close( fDataReadFH );
	return errno;
	}
    return 0;
    }

int AliHLTSimpleFileEventStorage::OpenWrite()
    {
    MLUCString filename;
    int flags;
    char buf[ 128 ];
    
    int oldFH = -1;
    int newFH = -1;
    unsigned long oldNdxNr = 0;
    unsigned long newNdxNr = 0;
    AliUInt64_t oldDataNr = 0;
    AliUInt64_t newDataNr = 0;
    AliUInt64_t oldTriggerNr = 0;
    AliUInt64_t newTriggerNr = 0;
    do
	{
	if ( newFH!=-1 )
	    {
	    if ( oldFH!=-1 )
		close( oldFH );
	    oldFH = newFH;
	    newFH = -1;
	    oldNdxNr = newNdxNr;
	    newNdxNr++; 
	    }
	filename = fStorageName;
	sprintf( buf, "/ndx-0x%016lX", newNdxNr );
	filename += buf;
	flags = O_RDWR;
	newFH = open( filename.c_str(), flags, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH );
	}
    while ( newFH!=-1 );
    if ( oldFH == -1 )
	{
	LOG( AliHLTLog::kDebug, "AliHLTSimpleFileEventStorage::OpenWrite", "Debug 1" )
	    << "Debug 1 - " << filename.c_str() << ENDLOG;
	return errno;
	}
    fCurrentNdxFileWriteNr = oldNdxNr;
    fNdxWriteFH = oldFH;
    fCurrentNdxFileWriteOffset = lseek( fNdxWriteFH, 0, SEEK_END );
    
    oldFH = -1;
    newFH = -1;
    do
	{
	if ( newFH!=-1 )
	    {
	    if ( oldFH!=-1 )
		close( oldFH );
	    oldFH = newFH;
	    newFH = -1;
	    oldDataNr = newDataNr;
	    newDataNr++; 
	    }
	filename = fStorageName;
	sprintf( buf, "/data-0x%016LX", (unsigned long long)newDataNr );
	filename += buf;
	flags = O_RDWR;
	newFH = open( filename.c_str(), flags, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH );
	}
    while ( newFH!=-1 );
    if ( oldFH == -1 )
	{
	LOG( AliHLTLog::kDebug, "AliHLTSimpleFileEventStorage::OpenWrite", "Debug 2" )
	    << "Debug 2 - " << filename.c_str() << ENDLOG;
	close( fNdxWriteFH );
	return errno;
	}
    fCurrentDataFileWriteNr = oldDataNr;
    fDataWriteFH = oldFH;
    fCurrentDataFileWriteOffset = lseek( fDataWriteFH, 0, SEEK_END );

    oldFH = -1;
    newFH = -1;
    do
	{
	if ( newFH!=-1 )
	    {
	    if ( oldFH!=-1 )
		close( oldFH );
	    oldFH = newFH;
	    newFH = -1;
	    oldTriggerNr = newTriggerNr;
	    newTriggerNr++; 
	    }
	filename = fStorageName;
	sprintf( buf, "/trigger-0x%016LX", (unsigned long long)newTriggerNr );
	filename += buf;
	flags = O_RDWR;
	newFH = open( filename.c_str(), flags, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH );
	}
    while ( newFH!=-1 );
    if ( oldFH == -1 )
	{
	LOG( AliHLTLog::kDebug, "AliHLTSimpleFileEventStorage::OpenWrite", "Debug 3" )
	    << "Debug 3 - " << filename.c_str() << ENDLOG;
	close( fNdxWriteFH );
	close( fDataWriteFH );
	return errno;
	}
    fCurrentTriggerFileWriteNr = oldTriggerNr;
    fTriggerWriteFH = oldFH;
    fCurrentTriggerFileWriteOffset = lseek( fTriggerWriteFH, 0, SEEK_END );
    
    return 0;
    }

int AliHLTSimpleFileEventStorage::OpenNdxFileWrite()
    {
    MLUCString filename;
    int flags;
    char buf[ 128 ];

    filename = fStorageName;
    sprintf( buf, "/ndx-0x%016lX", fCurrentNdxFileWriteNr );
    filename += buf;
    flags = O_RDWR|O_CREAT;
    fNdxWriteFH = open( filename.c_str(), flags, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH );
    if ( fNdxWriteFH == -1 )
	return errno;
    fCurrentNdxFileWriteOffset = lseek( fNdxWriteFH, 0, SEEK_END );
    return 0;
    }

int AliHLTSimpleFileEventStorage::CloseNdxFileWrite()
    {
    close( fNdxWriteFH );
    fNdxWriteFH = -1;
    fCurrentNdxFileWriteOffset = 0;
    return 0;
    }

int AliHLTSimpleFileEventStorage::OpenNdxFileRead()
    {
    MLUCString filename;
    int flags;
    char buf[ 128 ];

    filename = fStorageName;
    sprintf( buf, "/ndx-0x%016lX", fCurrentNdxFileReadNr );
    filename += buf;
    //flags = O_RDWR|O_CREAT;
    flags = O_RDONLY;
    fNdxReadFH = open( filename.c_str(), flags, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH );
    if ( fNdxReadFH == -1 )
	return errno;
    fCurrentNdxFileReadOffset = lseek( fNdxReadFH, 0, SEEK_SET );
    LOG( AliHLTLog::kDebug, "AliHLTSimpleFileEventStorage::OpenNdxFileRead", "Opened Read Index File" )
	<< "Opened read Index file " << AliHLTLog::kDec << fCurrentNdxFileReadNr
	<< "." << ENDLOG;
    return 0;
    }
 
int AliHLTSimpleFileEventStorage::SeekNdxFileRead()
    {
    if ( lseek( fNdxReadFH, fCurrentNdxFileReadOffset, SEEK_SET )==(off_t)-1 )
	return errno;
    return 0;
    }

int AliHLTSimpleFileEventStorage::CloseNdxFileRead()
    {
    LOG( AliHLTLog::kDebug, "AliHLTSimpleFileEventStorage::CloseNdxFileRead", "Closing Read Index File" )
	<< "Closing read Index file " << AliHLTLog::kDec << fCurrentNdxFileReadNr
	<< "." << ENDLOG;
    close( fNdxReadFH );
    fNdxReadFH = -1;
    fCurrentNdxFileReadOffset = 0;
    return 0;
    }

int AliHLTSimpleFileEventStorage::OpenDataFileWrite()
    {
    MLUCString filename;
    int flags;
    char buf[ 128 ];

    filename = fStorageName;
    sprintf( buf, "/data-0x%016LX", (unsigned long long)fCurrentDataFileWriteNr );
    filename += buf;
    flags = O_RDWR|O_CREAT;
    fDataWriteFH = open( filename.c_str(), flags, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH );
    if ( fDataWriteFH == -1 )
	return errno;
    fCurrentDataFileWriteOffset = lseek( fDataWriteFH, 0, SEEK_END );
    return 0;
    }

int AliHLTSimpleFileEventStorage::CloseDataFileWrite()
    {
    close( fDataWriteFH );
    fDataWriteFH = -1;
    fCurrentDataFileWriteOffset = 0;
    return 0;
    }

int AliHLTSimpleFileEventStorage::OpenDataFileRead()
    {
    MLUCString filename;
    int flags;
    char buf[ 128 ];

    filename = fStorageName;
    sprintf( buf, "/data-0x%016LX", (unsigned long long)fCurrentDataFileReadNr );
    filename += buf;
    flags = O_RDWR|O_CREAT;
    fDataReadFH = open( filename.c_str(), flags, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH );
    if ( fDataReadFH == -1 )
	return errno;
    fCurrentDataFileReadOffset = lseek( fDataReadFH, 0, SEEK_SET );
    return 0;
    }
 
int AliHLTSimpleFileEventStorage::SeekDataFileRead()
    {
    if ( lseek( fDataReadFH, fCurrentDataFileReadOffset, SEEK_SET )==(off_t)-1 )
	return errno;
    return 0;
    }

int AliHLTSimpleFileEventStorage::CloseDataFileRead()
    {
    close( fDataReadFH );
    fDataReadFH = -1;
    fCurrentDataFileReadOffset = 0;
    return 0;
    }

int AliHLTSimpleFileEventStorage::OpenTriggerFileWrite()
    {
    MLUCString filename;
    int flags;
    char buf[ 128 ];

    filename = fStorageName;
    sprintf( buf, "/trigger-0x%016LX", (unsigned long long)fCurrentTriggerFileWriteNr );
    filename += buf;
    flags = O_RDWR|O_CREAT;
    fTriggerWriteFH = open( filename.c_str(), flags, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH );
    if ( fTriggerWriteFH == -1 )
	return errno;
    fCurrentTriggerFileWriteOffset = lseek( fTriggerWriteFH, 0, SEEK_END );
    return 0;
    }

int AliHLTSimpleFileEventStorage::CloseTriggerFileWrite()
    {
    close( fTriggerWriteFH );
    fTriggerWriteFH = -1;
    fCurrentTriggerFileWriteOffset = 0;
    return 0;
    }

int AliHLTSimpleFileEventStorage::OpenTriggerFileRead()
    {
    MLUCString filename;
    int flags;
    char buf[ 128 ];

    filename = fStorageName;
    sprintf( buf, "/trigger-0x%016LX", (unsigned long long)fCurrentTriggerFileReadNr );
    filename += buf;
    flags = O_RDWR|O_CREAT;
    fTriggerReadFH = open( filename.c_str(), flags, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH );
    if ( fTriggerReadFH == -1 )
	return errno;
    fCurrentTriggerFileReadOffset = lseek( fTriggerReadFH, 0, SEEK_SET );
    return 0;
    }
 
int AliHLTSimpleFileEventStorage::SeekTriggerFileRead()
    {
    if ( lseek( fTriggerReadFH, fCurrentTriggerFileReadOffset, SEEK_SET )==(off_t)-1 )
	return errno;
    return 0;
    }

int AliHLTSimpleFileEventStorage::CloseTriggerFileRead()
    {
    close( fTriggerReadFH );
    fTriggerReadFH = -1;
    fCurrentTriggerFileReadOffset = 0;
    return 0;
    }


int AliHLTSimpleFileEventStorage::FillEventDescriptor( const AliHLTSubEventDataDescriptor* sedd, FileEventDescriptor& fed )
    {
    struct timeval tv;
    gettimeofday( &tv, NULL );
    BCLNetworkData::Transform( sedd->fEventID.fType, fed.fEventID.fType, kNativeByteOrder, kNetworkByteOrder );
    BCLNetworkData::Transform( sedd->fEventID.fNr, fed.fEventID.fNr, kNativeByteOrder, kNetworkByteOrder );
    BCLNetworkData::Transform( sedd->fEventBirth_s, fed.fEventBirth_s, kNativeByteOrder, kNetworkByteOrder );
    BCLNetworkData::Transform( sedd->fEventBirth_us, fed.fEventBirth_us, kNativeByteOrder, kNetworkByteOrder );
    BCLNetworkData::Transform( tv.tv_sec, fed.fEventWritten_s, kNativeByteOrder, kNetworkByteOrder );
    BCLNetworkData::Transform( tv.tv_usec, fed.fEventWritten_us, kNativeByteOrder, kNetworkByteOrder );
    BCLNetworkData::Transform( fNodeID, fed.fEventWriteNode, kNativeByteOrder, kNetworkByteOrder );
    fed.fEventWriteNodeByteOrder = kNativeByteOrder;
    BCLNetworkData::Transform( fed.fEventWriteNodeByteOrder, kNativeByteOrder, kNetworkByteOrder );
    BCLNetworkData::Transform( sedd->fDataType.fID, fed.fDataType.fID, kNativeByteOrder, kNetworkByteOrder );
    BCLNetworkData::Transform( sedd->fDataBlockCount, fed.fDataBlockCount, kNativeByteOrder, kNetworkByteOrder );
    return 0;
    }

int AliHLTSimpleFileEventStorage::WriteTriggerData( const AliHLTEventTriggerStruct* ets, FileEventTriggerDescriptor& fetd )
    {
    int ret;

    if ( (unsigned long long)fCurrentTriggerFileWriteOffset+(unsigned long long)ets->fHeader.fLength > (unsigned long long)fMaxTriggerFileSize )
	{
	CloseTriggerFileWrite();
	fCurrentTriggerFileWriteNr++;
	ret = OpenTriggerFileWrite();
	if ( ret )
	    return ret;
	}
    
    fetd.fFileNr = fCurrentTriggerFileWriteNr;
    BCLNetworkData::Transform( fetd.fFileNr , kNativeByteOrder, kNetworkByteOrder );
    fetd.fBlockOffset = fCurrentTriggerFileWriteOffset;
    BCLNetworkData::Transform( fetd.fBlockOffset, kNativeByteOrder, kNetworkByteOrder );
    fetd.fBlockSize = ets->fHeader.fLength;
    BCLNetworkData::Transform( fetd.fBlockSize, kNativeByteOrder, kNetworkByteOrder );
    
    ret = Write( fTriggerWriteFH, ets, ets->fHeader.fLength );
    if ( ret != (int)ets->fHeader.fLength )
	ret = errno;
    else
	ret = 0;
    fCurrentTriggerFileWriteOffset = lseek( fTriggerWriteFH, 0, SEEK_CUR );
    return ret;
    }

int AliHLTSimpleFileEventStorage::WriteBlock( const AliHLTSubEventDescriptor::BlockData& block, FileEventDataBlockDescriptor& fedbd, bool final )
    {
    int ret;

    unsigned paddingSize = 2*kPadSize-(block.fSize & (kPadSize-1));
    if ( final )
	paddingSize += kPadSize;
    uint8 padding[paddingSize];
    memset( padding, 0xFF, paddingSize );

    if ( (unsigned long long)fCurrentDataFileWriteOffset+(unsigned long long)block.fSize+(unsigned long long)paddingSize > (unsigned long long)fMaxDataFileSize )
	{
	CloseDataFileWrite();
	fCurrentDataFileWriteNr++;
	ret = OpenDataFileWrite();
	if ( ret )
	    return ret;
	}

    fedbd.fFileNr = fCurrentDataFileWriteNr;
    BCLNetworkData::Transform( fedbd.fFileNr, kNativeByteOrder, kNetworkByteOrder );
    fedbd.fBlockOffset = fCurrentDataFileWriteOffset;
    BCLNetworkData::Transform( fedbd.fBlockOffset, kNativeByteOrder, kNetworkByteOrder );
    fedbd.fBlockSize = block.fSize;
    BCLNetworkData::Transform( fedbd.fBlockSize, kNativeByteOrder, kNetworkByteOrder );
    fedbd.fProducerNode = block.fProducerNode;
    BCLNetworkData::Transform( fedbd.fProducerNode, kNativeByteOrder, kNetworkByteOrder );
    fedbd.fDataType = block.fDataType;
    BCLNetworkData::Transform( fedbd.fDataType.fID, kNativeByteOrder, kNetworkByteOrder );
    fedbd.fDataOrigin = block.fDataOrigin;
    BCLNetworkData::Transform( fedbd.fDataOrigin.fID, kNativeByteOrder, kNetworkByteOrder );
    fedbd.fDataSpecification = block.fDataSpecification;
    BCLNetworkData::Transform( fedbd.fDataSpecification, kNativeByteOrder, kNetworkByteOrder );
//     fedbd.fByteOrder = block.fByteOrder;
//     BCLNetworkData::Transform( fedbd.fByteOrder, kNativeByteOrder, kNetworkByteOrder );
    
    ret = Write( fDataWriteFH, block.fData, block.fSize );
    if ( ret != (int)block.fSize )
	ret = errno;
    else
	ret = 0;
    if ( !ret )
	{
	ret = Write( fDataWriteFH, padding, paddingSize );
	if ( ret != (int)paddingSize )
	    ret = errno;
	else
	    ret = 0;
	}
    fCurrentDataFileWriteOffset = lseek( fDataWriteFH, 0, SEEK_CUR );
    return ret;
    }

int AliHLTSimpleFileEventStorage::WriteFileEventData( const FileEventData* fed, SimpleFileEventLocationDescriptor* eld )
    {
    uint32 len;
    int ret;

    LOG( AliHLTLog::kDebug, "AliHLTSimpleFileEventStorage::WriteFileEventData", "FED Header Length" )
	<< "FED header length: " << AliHLTLog::kDec << fed->fEvent.fHeader.fLength << " (0x" << AliHLTLog::kHex
	<< fed->fEvent.fHeader.fLength << ")." << ENDLOG;

    BCLNetworkData::Transform( fed->fEvent.fHeader.fLength, len, kNetworkByteOrder, kNativeByteOrder );

    LOG( AliHLTLog::kDebug, "AliHLTSimpleFileEventStorage::WriteFileEventData", "FED Length" )
	<< "FED Length: " << AliHLTLog::kDec << len << " (0x" << AliHLTLog::kHex
	<< len << ")." << ENDLOG;

    if ( fed->fEvent.fDataBlockCount>0 )
	{
	AliUInt32_t tmp=0;
	BCLNetworkData::Transform( fed->fBlocks[0].fBlockSize, tmp, kNetworkByteOrder, kNativeByteOrder );
	LOG( AliHLTLog::kDebug, "AliHLTSimpleFileEventStorage::WriteFileEventData", "Block size" )
	    << "Event block 0 size: " << AliHLTLog::kDec << tmp << " (0x"
	    << AliHLTLog::kHex << tmp << ")." << ENDLOG;
	}
    
    if ( (unsigned long long)fCurrentNdxFileWriteOffset+(unsigned long long)len > (unsigned long long)fMaxNdxFileSize )
	{
	CloseNdxFileWrite();
	fCurrentNdxFileWriteNr++;
	ret = OpenNdxFileWrite();
	if ( ret )
	    return ret;
	}

    off_t startOffset = fCurrentNdxFileWriteOffset;

    ret = Write( fNdxWriteFH, fed, len );
    if ( ret != (int)len )
	ret = errno;
    else
	ret = 0;
    fCurrentNdxFileWriteOffset = lseek( fNdxWriteFH, 0, SEEK_CUR );
    if ( eld )
	{
	eld->SetNdxFileNr( fCurrentNdxFileWriteNr );
	eld->SetFileOffset( startOffset );
	}
    return ret;
    }


int AliHLTSimpleFileEventStorage::ReadFileEventData( FileEventData*& fed, SimpleFileEventLocationDescriptor* eld )
    {
    AliHLTBlockHeader header;
    int ret;
    
    off_t startOffset;
    do
	{
	startOffset = fCurrentNdxFileReadOffset;
	ret = Read( fNdxReadFH, &header, sizeof(header) );
	if ( ret==0 )
	    {
	    CloseNdxFileRead();
	    fCurrentNdxFileReadNr++;
	    ret = OpenNdxFileRead();
	    if ( ret )
		return ENOENT;
	    }
	else if ( ret != (int)sizeof(header) )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::ReadFileEventData", "Error reading file event descriptor header" )
		<< "Error reading event descriptor header from index file: " << strerror(ret)
		<< " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	    return errno;
	    }
	}
    while ( ret<(int)sizeof(header) );
	   
    AliUInt32_t len;
    BCLNetworkData::Transform( header.fLength, len, kNetworkByteOrder, kNativeByteOrder );
    fed = reinterpret_cast<FileEventData*>( fFEDCache.Get( len ) );
    if ( !fed )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::ReadFileEventData", "Out of memory" )
	    << "Out of memory trying to allocate file event descriptor of " << AliHLTLog::kDec
	    << len << " bytes." << ENDLOG;
	return ENOMEM;
	}
    memcpy( &(fed->fEvent.fHeader), &header, sizeof(header) );
    ret = Read( fNdxReadFH, ((AliUInt8_t*)fed)+sizeof(header), len-sizeof(header) );
    if ( ret != (int)( len-sizeof(header) ) )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::ReadFileEventData", "Error reading file event descriptor" )
	    << "Error reading event descriptor from index file: " << strerror(ret)
	    << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	fFEDCache.Release( reinterpret_cast<uint8*>( fed ) );
	fed = NULL;
	return errno;
	}
    if ( fVersion==1 )
	{
	FileEventDataV1* fed1 = reinterpret_cast<FileEventDataV1*>( fed );
	fed=NULL;
	ConvertFileEventDataV1ToCurrent( fed1, fed );
	if ( !fed )
	    {
	    return ENOMEM;
	    }
	fFEDCache.Release( reinterpret_cast<uint8*>( fed1 ) );
	}
    fCurrentNdxFileReadOffset = lseek( fNdxReadFH, 0, SEEK_CUR );

//     printf( "%s:%d: block %d size: %d (0x%08lX)\n", __FILE__, __LINE__, 0, fed->fBlocks[0].fBlockSize, fed->fBlocks[0].fBlockSize );
    AliUInt32_t n;
    TransformEventDescriptor( fed->fEvent );
    TransformTriggerData( fed->fTrigger );
    for ( n = 0; n < fed->fEvent.fDataBlockCount; n++ )
	TransformBlock( fed->fBlocks[n] );
//     printf( "%s:%d: block %d size: %d (0x%08lX)\n", __FILE__, __LINE__, 0, fed->fBlocks[0].fBlockSize, fed->fBlocks[0].fBlockSize );
    if ( eld )
	{
	eld->SetNdxFileNr( fCurrentNdxFileReadNr );
	eld->SetFileOffset( startOffset );
	}
    return 0;
    }

int AliHLTSimpleFileEventStorage::ReadFileEventData( const EventLocationDescriptor* eld, FileEventData*& fed )
    {
    int ret;
    if ( !eld )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::GetEventBlockData", "Invalid event location descriptor" )
	    << "Invalid event location descriptor specified (NULL pointer)." << ENDLOG;
	return EFAULT;
	}
    if ( strcmp( GetStorageTypeName(), eld->GetStorageTypeName() ) )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::GetEventBlockData", "Wrong event location descriptor type" )
	    << "Wrong type of event location descriptor specified. '" << eld->GetStorageTypeName()
	    << "' found; '" << GetStorageTypeName()
	    << "' expected." << ENDLOG;
	return EINVAL;
	}
    if ( fCurrentNdxFileReadNr != reinterpret_cast<const SimpleFileEventLocationDescriptor*>( eld )->GetNdxFileNr() )
	{
	CloseNdxFileRead();
	fCurrentNdxFileReadNr = reinterpret_cast<const SimpleFileEventLocationDescriptor*>( eld )->GetNdxFileNr();
	ret = OpenNdxFileRead();
	if ( ret )
	    return ret;
	}
    if ( fCurrentNdxFileReadOffset!=reinterpret_cast<const SimpleFileEventLocationDescriptor*>( eld )->GetFileOffset() )
	{
	fCurrentNdxFileReadOffset = reinterpret_cast<const SimpleFileEventLocationDescriptor*>( eld )->GetFileOffset();
	ret = SeekNdxFileRead();
	if ( ret )
	    return ret;
	}

    ret = ReadFileEventData( fed, NULL );
//     printf( "%s:%d: block %d size: %d (0x%08lX)\n", __FILE__, __LINE__, 0, fed->fBlocks[0].fBlockSize, fed->fBlocks[0].fBlockSize );
    if ( ret )
	{
	if ( fed )
	    {
	    fFEDCache.Release( reinterpret_cast<uint8*>( fed ) );
	    fed = NULL;
	    }
	return ret;
	}
    if ( !fed )
	return ENOMEM;
    return 0;
    }

int AliHLTSimpleFileEventStorage::FillEventLocationDescriptor( SimpleFileEventLocationDescriptor* eld )
    {
    eld->SetNdxFileNr( fCurrentNdxFileReadNr );
    eld->SetFileOffset( fCurrentNdxFileReadOffset );
    return 0;
    }

void AliHLTSimpleFileEventStorage::TransformEventDescriptor( FileEventDescriptor& fed )
    {
    BCLNetworkData::Transform( fed.fHeader.fLength, kNetworkByteOrder, kNativeByteOrder );
    BCLNetworkData::Transform( fed.fHeader.fType.fID, kNetworkByteOrder, kNativeByteOrder );
    BCLNetworkData::Transform( fed.fEventID.fType, kNetworkByteOrder, kNativeByteOrder );
    BCLNetworkData::Transform( fed.fEventID.fNr, kNetworkByteOrder, kNativeByteOrder );
    BCLNetworkData::Transform( fed.fEventBirth_s, kNetworkByteOrder, kNativeByteOrder );
    BCLNetworkData::Transform( fed.fEventBirth_us, kNetworkByteOrder, kNativeByteOrder );
    BCLNetworkData::Transform( fed.fEventWritten_s, kNetworkByteOrder, kNativeByteOrder );
    BCLNetworkData::Transform( fed.fEventWritten_us, kNetworkByteOrder, kNativeByteOrder );
    BCLNetworkData::Transform( fed.fEventWriteNode, kNetworkByteOrder, kNativeByteOrder );
    BCLNetworkData::Transform( fed.fEventWriteNodeByteOrder, kNetworkByteOrder, kNativeByteOrder );
    BCLNetworkData::Transform( fed.fDataType.fID, kNetworkByteOrder, kNativeByteOrder );
    BCLNetworkData::Transform( fed.fDataBlockCount, kNetworkByteOrder, kNativeByteOrder );
    }

void AliHLTSimpleFileEventStorage::TransformTriggerData( FileEventTriggerDescriptor& fetd )
    {
    BCLNetworkData::Transform( fetd.fFileNr, kNetworkByteOrder, kNativeByteOrder );
    BCLNetworkData::Transform( fetd.fBlockOffset, kNetworkByteOrder, kNativeByteOrder );
    BCLNetworkData::Transform( fetd.fBlockSize, kNetworkByteOrder, kNativeByteOrder );
    }
	
void AliHLTSimpleFileEventStorage::TransformBlock( FileEventDataBlockDescriptor& fedbd )
    {
    BCLNetworkData::Transform( fedbd.fFileNr, kNetworkByteOrder, kNativeByteOrder );
    BCLNetworkData::Transform( fedbd.fBlockOffset, kNetworkByteOrder, kNativeByteOrder );
    BCLNetworkData::Transform( fedbd.fBlockSize, kNetworkByteOrder, kNativeByteOrder );
    BCLNetworkData::Transform( fedbd.fProducerNode, kNetworkByteOrder, kNativeByteOrder );
    BCLNetworkData::Transform( fedbd.fDataType.fID, kNetworkByteOrder, kNativeByteOrder );
    BCLNetworkData::Transform( fedbd.fDataOrigin.fID, kNetworkByteOrder, kNativeByteOrder );
    BCLNetworkData::Transform( fedbd.fDataSpecification, kNetworkByteOrder, kNativeByteOrder );
    //BCLNetworkData::Transform( fedbd.fByteOrder, kNetworkByteOrder, kNativeByteOrder );
    }

void AliHLTSimpleFileEventStorage::ConvertFileEventDataV1ToCurrent( FileEventDataV1* fed1, FileEventData*& fed )
    {
    unsigned long newSize = sizeof(FileEventData)+sizeof(FileEventDataBlockDescriptor)*fed1->fEvent.fDataBlockCount;
    fed = reinterpret_cast<FileEventData*>( fFEDCache.Get( newSize ) );
    if ( !fed )
	{
	LOG( AliHLTLog::kError, "AliHLTSimpleFileEventStorage::ConvertFileEventDataV1ToCurrent", "Out of memory" )
	    << "Out of memory trying to allocate new file event descriptor of " << AliHLTLog::kDec
	    << newSize << " bytes." << ENDLOG;
	return;
	}
    fed->fEvent = fed1->fEvent;
    fed->fTrigger = fed1->fTrigger;
    for ( unsigned long n = 0; n < fed1->fEvent.fDataBlockCount; n++ )
	{
	fed->fBlocks[n].fFileNr = fed1->fBlocks[n].fFileNr;
	fed->fBlocks[n].fBlockSize = fed1->fBlocks[n].fBlockSize;
	fed->fBlocks[n].fBlockOffset = fed1->fBlocks[n].fBlockOffset;
	fed->fBlocks[n].fProducerNode = fed1->fBlocks[n].fProducerNode;
	fed->fBlocks[n].fDataType = fed1->fBlocks[n].fDataType;
	fed->fBlocks[n].fDataOrigin = fed1->fBlocks[n].fDataOrigin;
	fed->fBlocks[n].fDataSpecification = fed1->fBlocks[n].fDataSpecification;
	for ( int j = 0; j < kAliHLTSEDBDAttributeCount; j++ )
	    fed->fBlocks[n].fAttributes[n] = kAliHLTUnknownAttribute;
	fed->fBlocks[n].fAttributes[kAliHLTSEDBDByteOrderAttributeIndex] = (AliUInt8_t)fed1->fBlocks[n].fByteOrder;
	}
    }

int AliHLTSimpleFileEventStorage::Write( const int& fh, const void* buffer, int len )
    {
    const char* fhName;
    if ( &fh == &fNdxWriteFH )
	fhName = "Index";
    else if ( &fh == &fDataWriteFH )
	fhName = "Data";
    else if ( &fh == &fTriggerWriteFH )
	fhName = "Trigger";
    else
	fhName = "Unknown";
    LOG( AliHLTLog::kDebug, "AliHLTSimpleFileEventStorage::Write", "Writing" )
	<< "Writing " << AliHLTLog::kDec << len << " (0x" << AliHLTLog::kHex
	<< len << ") bytes to " << fhName << " file." << ENDLOG;
    return write( fh, buffer, len );
    }

int AliHLTSimpleFileEventStorage::Read( const int& fh, void* buffer, int len )
    {
    const char* fhName;
    if ( &fh == &fNdxReadFH )
	fhName = "Index";
    else if ( &fh == &fDataReadFH )
	fhName = "Data";
    else if ( &fh == &fTriggerReadFH )
	fhName = "Trigger";
    else
	fhName = "Unknown";
    off_t offset = lseek( fh, 0, SEEK_CUR );
    LOG( AliHLTLog::kDebug, "AliHLTSimpleFileEventStorage::Read", "Reading" )
	<< "Reading " << AliHLTLog::kDec << len << " (0x" << AliHLTLog::kHex
	<< len << ") bytes from " << fhName << " file at offset "
	<< AliHLTLog::kDec << offset << " (0x" << AliHLTLog::kHex
	<< offset << ")." << ENDLOG;
    return read( fh, buffer, len );
    }

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
