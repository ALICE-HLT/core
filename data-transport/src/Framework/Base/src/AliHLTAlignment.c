/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTAlignment.h"

    struct AliHLTAlignment64TestStructure
    {
	AliUInt64_t f64Fill;
	AliUInt64_t f64Test64;
	AliUInt32_t f32Fill;
	AliUInt64_t f64Test32;
	AliUInt16_t f16Fill;
	AliUInt64_t f64Test16;
	AliUInt8_t  f8Fill;
	AliUInt64_t f64Test8;
    };
    struct AliHLTAlignment32TestStructure
    {
	AliUInt64_t f64Fill;
	AliUInt32_t f32Test64;
	AliUInt32_t f32Fill;
	AliUInt32_t f32Test32;
	AliUInt16_t f16Fill;
	AliUInt32_t f32Test16;
	AliUInt8_t  f8Fill;
	AliUInt32_t f32Test8;
    };
    struct AliHLTAlignment16TestStructure
    {
	AliUInt64_t f64Fill;
	AliUInt16_t f16Test64;
	AliUInt32_t f32Fill;
	AliUInt16_t f16Test32;
	AliUInt16_t f16Fill;
	AliUInt16_t f16Test16;
	AliUInt8_t  f8Fill;
	AliUInt16_t f16Test8;
    };
    struct AliHLTAlignment8TestStructure
    {
	AliUInt64_t f64Fill;
	AliUInt8_t f8Test64;
	AliUInt32_t f32Fill;
	AliUInt8_t f8Test32;
	AliUInt16_t f16Fill;
	AliUInt8_t f8Test16;
	AliUInt8_t  f8Fill;
	AliUInt8_t f8Test8;
    };
    struct AliHLTAlignmentDoubleTestStructure
    {
	AliUInt64_t f64Fill;
	double fDoubleTest64;
	AliUInt32_t f32Fill;
	double fDoubleTest32;
	AliUInt16_t f16Fill;
	double fDoubleTest16;
	AliUInt8_t  f8Fill;
	double fDoubleTest8;
    };
    struct AliHLTAlignmentFloatTestStructure
    {
	AliUInt64_t f64Fill;
	float fFloatTest64;
	AliUInt32_t f32Fill;
	float fFloatTest32;
	AliUInt16_t f16Fill;
	float fFloatTest16;
	AliUInt8_t  f8Fill;
	float fFloatTest8;
    };

AliUInt8_t AliHLTDetermineUInt64Alignment()
    {
    AliHLTAlignment64TestStructure test;
    if ( (unsigned long)(&test.f64Test64) != ((unsigned long)(&test.f64Fill))+sizeof(test.f64Fill) )
	{
	// Alignment is beyond 64 bit, this is, to the best of my knowledge, currently unheard of.
	return ~(AliUInt8_t)0;
	}
    if ( (unsigned long)(&test.f64Test32) != ((unsigned long)(&test.f32Fill))+sizeof(test.f32Fill) )
	{
	// The 64 bit element does not immedately follow the 32 bit element, 
	// therefore the alignment has to be greater than 4.
	return (AliUInt8_t)8;
	}
    if ( (unsigned long)(&test.f64Test16) != ((unsigned long)(&test.f16Fill))+sizeof(test.f16Fill) )
	{
	// The 64 bit element does not immedately follow the 16 bit element, 
	// therefore the alignment has to be greater than 2.
	return (AliUInt8_t)4;
	}
    if ( (unsigned long)(&test.f64Test8) != ((unsigned long)(&test.f8Fill))+sizeof(test.f8Fill) )
	{
	// The 64 bit element does not immedately follow the 8 bit element, 
	// therefore the alignment has to be greater than 1.
	return (AliUInt8_t)2;
	}
    return 1;
    }

AliUInt8_t AliHLTDetermineUInt32Alignment()
    {
    AliHLTAlignment32TestStructure test;
    if ( (unsigned long)(&test.f32Test64) != ((unsigned long)(&test.f64Fill))+sizeof(test.f64Fill) )
	{
	// Alignment is beyond 64 bit, this is, to the best of my knowledge, currently unheard of.
	return ~(AliUInt8_t)0;
	}
    if ( (unsigned long)(&test.f32Test32) != ((unsigned long)(&test.f32Fill))+sizeof(test.f32Fill) )
	{
	// The 32 bit element does not immedately follow the 32 bit element, 
	// therefore the alignment has to be greater than 4.
	return (AliUInt8_t)8;
	}
    if ( (unsigned long)(&test.f32Test16) != ((unsigned long)(&test.f16Fill))+sizeof(test.f16Fill) )
	{
	// The 32 bit element does not immedately follow the 16 bit element, 
	// therefore the alignment has to be greater than 2.
	return (AliUInt8_t)4;
	}
    if ( (unsigned long)(&test.f32Test8) != ((unsigned long)(&test.f8Fill))+sizeof(test.f8Fill) )
	{
	// The 32 bit element does not immedately follow the 8 bit element, 
	// therefore the alignment has to be greater than 1.
	return (AliUInt8_t)2;
	}
    return 1;
    }

AliUInt8_t AliHLTDetermineUInt16Alignment()
    {
    AliHLTAlignment16TestStructure test;
    if ( (unsigned long)(&test.f16Test64) != ((unsigned long)(&test.f64Fill))+sizeof(test.f64Fill) )
	{
	// Alignment is beyond 64 bit, this is, to the best of my knowledge, currently unheard of.
	return ~(AliUInt8_t)0;
	}
    if ( (unsigned long)(&test.f16Test32) != ((unsigned long)(&test.f32Fill))+sizeof(test.f32Fill) )
	{
	// The 16 bit element does not immedately follow the 32 bit element, 
	// therefore the alignment has to be greater than 4.
	return (AliUInt8_t)8;
	}
    if ( (unsigned long)(&test.f16Test16) != ((unsigned long)(&test.f16Fill))+sizeof(test.f16Fill) )
	{
	// The 16 bit element does not immedately follow the 16 bit element, 
	// therefore the alignment has to be greater than 2.
	return (AliUInt8_t)4;
	}
    if ( (unsigned long)(&test.f16Test8) != ((unsigned long)(&test.f8Fill))+sizeof(test.f8Fill) )
	{
	// The 16 bit element does not immedately follow the 8 bit element, 
	// therefore the alignment has to be greater than 1.
	return (AliUInt8_t)2;
	}
    return 1;
    }

AliUInt8_t AliHLTDetermineUInt8Alignment()
    {
    AliHLTAlignment8TestStructure test;
    if ( (unsigned long)(&test.f8Test64) != ((unsigned long)(&test.f64Fill))+sizeof(test.f64Fill) )
	{
	// Alignment is beyond 64 bit, this is, to the best of my knowledge, currently unheard of.
	return ~(AliUInt8_t)0;
	}
    if ( (unsigned long)(&test.f8Test32) != ((unsigned long)(&test.f32Fill))+sizeof(test.f32Fill) )
	{
	// The 8 bit element does not immedately follow the 32 bit element, 
	// therefore the alignment has to be greater than 4.
	return (AliUInt8_t)8;
	}
    if ( (unsigned long)(&test.f8Test16) != ((unsigned long)(&test.f16Fill))+sizeof(test.f16Fill) )
	{
	// The 8 bit element does not immedately follow the 16 bit element, 
	// therefore the alignment has to be greater than 2.
	return (AliUInt8_t)4;
	}
    if ( (unsigned long)(&test.f8Test8) != ((unsigned long)(&test.f8Fill))+sizeof(test.f8Fill) )
	{
	// The 8 bit element does not immedately follow the 8 bit element, 
	// therefore the alignment has to be greater than 1.
	return (AliUInt8_t)2;
	}
    return 1;
    }

AliUInt8_t AliHLTDetermineDoubleAlignment()
    {
    AliHLTAlignmentDoubleTestStructure test;
    if ( (unsigned long)(&test.fDoubleTest64) != ((unsigned long)(&test.f64Fill))+sizeof(test.f64Fill) )
	{
	// Alignment is beyond 64 bit, this is, to the best of my knowledge, currently unheard of.
	return ~(AliUInt8_t)0;
	}
    if ( (unsigned long)(&test.fDoubleTest32) != ((unsigned long)(&test.f32Fill))+sizeof(test.f32Fill) )
	{
	// The double element does not immedately follow the 32 bit element, 
	// therefore the alignment has to be greater than 4.
	return (AliUInt8_t)8;
	}
    if ( (unsigned long)(&test.fDoubleTest16) != ((unsigned long)(&test.f16Fill))+sizeof(test.f16Fill) )
	{
	// The double element does not immedately follow the 16 bit element, 
	// therefore the alignment has to be greater than 2.
	return (AliUInt8_t)4;
	}
    if ( (unsigned long)(&test.fDoubleTest8) != ((unsigned long)(&test.f8Fill))+sizeof(test.f8Fill) )
	{
	// The double element does not immedately follow the 8 bit element, 
	// therefore the alignment has to be greater than 1.
	return (AliUInt8_t)2;
	}
    return 1;
    }

AliUInt8_t AliHLTDetermineFloatAlignment()
    {
    AliHLTAlignmentFloatTestStructure test;
    if ( (unsigned long)(&test.fFloatTest64) != ((unsigned long)(&test.f64Fill))+sizeof(test.f64Fill) )
	{
	// Alignment is beyond 64 bit, this is, to the best of my knowledge, currently unheard of.
	return ~(AliUInt8_t)0;
	}
    if ( (unsigned long)(&test.fFloatTest32) != ((unsigned long)(&test.f32Fill))+sizeof(test.f32Fill) )
	{
	// The float element does not immedately follow the 32 bit element, 
	// therefore the alignment has to be greater than 4.
	return (AliUInt8_t)8;
	}
    if ( (unsigned long)(&test.fFloatTest16) != ((unsigned long)(&test.f16Fill))+sizeof(test.f16Fill) )
	{
	// The float element does not immedately follow the 16 bit element, 
	// therefore the alignment has to be greater than 2.
	return (AliUInt8_t)4;
	}
    if ( (unsigned long)(&test.fFloatTest8) != ((unsigned long)(&test.f8Fill))+sizeof(test.f8Fill) )
	{
	// The float element does not immedately follow the 8 bit element, 
	// therefore the alignment has to be greater than 1.
	return (AliUInt8_t)2;
	}
    return 1;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
