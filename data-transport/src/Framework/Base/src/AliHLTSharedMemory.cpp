/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSharedMemory.hpp"
#include "AliHLTLog.hpp"
#include <psi_error.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>

#define LOG_DEC_HEX(x) AliHLTLog::kDec << (x) << "(0x" << AliHLTLog::kHex << (x) << ")" << AliHLTLog::kDec

#ifndef NOLIBRORC
#include <librorc.h>
static const char* gkLibrorcName = "Librorc";
#endif

static const char* gkSysVName = "SystemV";


AliHLTSharedMemory::AliHLTSharedMemory()
    {
    pthread_mutex_init( &fMutex, NULL );
    fErrorName = "Name Error";
    fNextKey = 0;
    }
	
AliHLTSharedMemory::~AliHLTSharedMemory()
    {
    vector<ShmData>::iterator iter;
    pthread_mutex_lock( &fMutex );
    while ( fShms.size()>0 )
	{
	iter = fShms.begin();
#if __GNUC__>=3
	if ( iter.base() )
#else
	if ( iter )
#endif
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTSharedMemory", "Destructor" )
		<< "Memory region " << iter->fName << " with type "
		<< AliHLTLog::kDec << iter->fShmType << " and id " << iter->fID 
		<< " not explicitly unlocked and released. Done NOW by destructor."
		<< ENDLOG;
            ReleaseShm(iter->fKey);
	    }
	}
    pthread_mutex_unlock( &fMutex );
    pthread_mutex_destroy( &fMutex );
    }

// Allocate a shared memory segment of the given
// size and return its ID. name is an identifier
// for the block. The ID will be -1 in case of an error.
AliUInt32_t AliHLTSharedMemory::GetShm( AliHLTShmID& shmKey, unsigned long& size, bool failIfExisting )
    {
    ShmData data;
    PSI_Status status;
    unsigned long sz;
    MLUCString fullName;
    switch ( shmKey.fShmType )
	{
	case kPhysMemShmType: // Fallthrough
	case kBigPhysShmType:
	    {
	    if ( shmKey.fShmType==kPhysMemShmType )
		{
		fullName = "/dev/psi/physmem/";
		if ( sizeof(void*)==4 )
		    {
		    AliUInt32_t p;
		    p = (AliUInt32_t)(shmKey.fKey.fID & 0xFFFFFFFF);
		    char tmp[32];
		    sprintf( tmp, "0x%08X", p );
		    fullName += tmp;
		    }
		else
		    {
		    AliUInt64_t p;
		    p = (AliUInt64_t)shmKey.fKey.fID;
		    char tmp[32];
		    sprintf( tmp, "0x%016LX", (unsigned long long)p );
		    fullName += tmp;
		    }
		}
	    else
		{
		fullName = "/dev/psi/bigphys/AliHLTShm-";
		char tmp[32];
		sprintf( tmp, "%016LX", (unsigned long long)shmKey.fKey.fID );
		fullName += tmp;
		}
	    status = PSI_openRegion( &(data.fRegion), fullName.c_str() );
	    if ( status != PSI_OK )
		{
		LOG( AliHLTLog::kInformational, "AliHLTSharedMemory", "Get shared memory" )
		    << "Error requesting memory region " << fullName.c_str() << " in PSI bigphysarea."
		    << ENDLOG;
		return (AliUInt32_t)-1;
		}
	    if ( failIfExisting )
		{
		tRegionStatus regStat;
		status = PSI_getRegionStatus( data.fRegion, &regStat );
		if ( status != PSI_OK )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Shm get status" )
			<< "Error getting status for region " << fullName.c_str() << ". Reason: "
			<< PSI_strerror(status) << " (" << AliHLTLog::kDec << status << ")." << ENDLOG;
		    PSI_closeRegion( data.fRegion );
		    return (AliUInt32_t)-1;
		    }
		if ( regStat.size > 0 )
		    {
		    LOG( AliHLTLog::kInformational, "AliHLTSharedMemory", "Region exists" )
			<< "Region " << fullName.c_str() << " already exists. Aborting as requested." << ENDLOG;
		    PSI_closeRegion( data.fRegion );
		    size = 0;
		    return (AliUInt32_t)-1;
		    }
		}
	    if ( size > 0 )
		{
		sz = size;
		status = PSI_sizeRegion( data.fRegion, &sz );
		if ( status != PSI_OK )
		    {
		    bool ok = false;
		    if ( status == PSI_SIZE_SET && sz >= size )
			{
			ok = true;
			size = sz;
			LOG( AliHLTLog::kInformational, "AliHLTSharedMemory", "Get shared memory" )
			    << "Requested memory region " << fullName.c_str() << " already allocated in PSI bigphysarea with size "
			    << AliHLTLog::kDec << sz << ENDLOG;
			}
		    if ( !ok )
			{
			LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Get shared memory" )
			    << "Error getting requested memory region " << fullName.c_str() << " of size " 
			    << AliHLTLog::kDec << size << " in PSI bigphysarea. Reason: "
			    << PSI_strerror( status ) << " (" << AliHLTLog::kDec << status << ")." << ENDLOG;
			PSI_closeRegion( data.fRegion );
			return (AliUInt32_t)-1;
			}
		    }
		}
	    else
		{
		tRegionStatus regStat;
		status = PSI_getRegionStatus( data.fRegion, &regStat );
		if ( status != PSI_OK )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Shm get status" )
			<< "Error getting status for region " << fullName.c_str() << ". Reason: "
			<< PSI_strerror(status) << " (" << AliHLTLog::kDec << status << ")." << ENDLOG;
		    PSI_closeRegion( data.fRegion );
		    return (AliUInt32_t)-1;
		    //size = 0;
		    }
		else
		    size = regStat.size;
		}
	    data.fID = (AliUInt32_t)data.fRegion;
	    data.fSize = size;
	    char* tmp = new char[ strlen( fullName.c_str() )+1 ];
	    if ( !tmp )
		{
		data.fName = fErrorName;
		}
	    else
		{
		strcpy( tmp, fullName.c_str() );
		data.fName = tmp;
		}
	    data.fPtr = NULL;
	    break;
	    }
	case kSysVShmType: // Fallthrough
	case kSysVShmPrivateType:
	    {
	    int ret, flags = 0660|IPC_CREAT;
	    if ( failIfExisting )
		flags |= IPC_EXCL;
	    if ( shmKey.fShmType==kSysVShmType )
		data.fID = shmget( shmKey.fKey.fID, size, flags );
	    else if ( shmKey.fKey.fID==~(AliUInt64_t)0 )
		data.fID = shmget( IPC_PRIVATE, size, flags );
	    else
		data.fID = shmKey.fKey.fID;
	    if ( data.fID == (AliUInt32_t)-1 )
		{
		LOG( AliHLTLog::kInformational, "AliHLTSharedMemory", "Get shared memory" )
		    << "Error requesting System V memory region " << AliHLTLog::kDec << shmKey.fKey.fID << ": "
		    << strerror(errno) << " (" << errno << ")." << ENDLOG;
		return ~(AliUInt32_t)0;
		}
	    if ( shmKey.fShmType==kSysVShmPrivateType && shmKey.fKey.fID==~(AliUInt64_t)0 )
		shmKey.fKey.fID = data.fID;
	    struct shmid_ds shmBufInfo;
	    ret = shmctl( data.fID, IPC_STAT, &shmBufInfo);
	    if ( ret==-1 )
		{
		LOG( AliHLTLog::kInformational, "AliHLTSharedMemory", "Get shared memory" )
		    << "Error obtaining information about System V memory region " << AliHLTLog::kDec << shmKey.fKey.fID << ": "
		    << strerror(errno) << " (" << errno << ")." << ENDLOG;
		return ~(AliUInt32_t)0;
		}
	    if ( shmBufInfo.shm_segsz == 0 )
		shmBufInfo.shm_segsz = size;
#ifdef __arm__ 
#define RESTRICT_MAX_SIZE
#endif
#ifdef RESTRICT_MAX_SIZE
	    // Work around for non-working shmctl/IPC_STAT function on at least one ARM Linux system.
	    //if ( (AliUInt32_t)shmBufInfo.shm_segsz>size )
	    if ( size != 0 )
		shmBufInfo.shm_segsz = size;
#endif
	    size = data.fSize = (size_t) shmBufInfo.shm_segsz;
	    LOG( AliHLTLog::kDebug, "AliHLTSharedMemory", "Get shared memory" )
		<< "Size: " << AliHLTLog::kDec << size << "/0x" << AliHLTLog::kHex
		<< size << ENDLOG;
	    data.fPtr = NULL;
	    data.fName = gkSysVName;
	    break;
	    }

        case kLibrorcShmType:
            {
#ifndef NOLIBRORC
                librorc::buffer *buf = NULL;
                librorc::device *dev = NULL;

                if( failIfExisting )
                {
                    if( GetExistingLibrorcBuffer(shmKey, &buf) )
                    {
                        LOG( AliHLTLog::kError, "AliHLTSharedMemory", "GetShm")
                            << "Buffer already exists" << ENDLOG;
                        if( buf )
                        { delete buf; }
                        if( dev )
                        { delete dev; }
                        return ~(AliUInt32_t)0;
                    }
                }


                // ending here means the buffer does not exist or existance has not been checked.
                // In any case *buf is still NULL. A possibly existing buffer of a different size
                // will be freed and allocated again with the requested size in the next step.
                if( size > 0 )
                {
                    if( !AllocateLibrorcBuffer(shmKey, size, &dev, &buf) )
                    {
                        LOG( AliHLTLog::kError, "AliHLTSharedMemory", "GetShm")
                            << "librorc buffer allocation failed" << ENDLOG;
                        if( dev )
                        { delete dev; }
                        return ~(AliUInt32_t)0;
                    }
                }
                else
                {
                    if( !GetExistingLibrorcBuffer(shmKey, &buf) )
                    {
                        LOG( AliHLTLog::kError, "AliHLTSharedMemory", "GetShm")
                            << "librorc buffer attach request failed" << ENDLOG;
                        if( dev )
                        { delete dev; }
                        return ~(AliUInt32_t)0;
                    }
                    size = buf->size();
                }

                data.fSize = (AliUInt32_t)buf->getMappingSize();
                data.fID = shmKey.fKey.fID;
                data.fLibrorcBuffer = buf;
                data.fLibrorcDevice = dev;
                data.fPtr = NULL;
                data.fName = gkLibrorcName;
#else
                LOG( AliHLTLog::kError, "AliHLTSharedMemory", "GetShm")
                    << "librorc support not enabled" << ENDLOG;
                return ~(AliUInt32_t)0;
#endif


            }
            break;

	default:
	    return ~(AliUInt32_t)0;
	}
    data.fShmType = shmKey.fShmType;
    data.fKeyID = shmKey.fKey.fID;
    pthread_mutex_lock( &fMutex );
    data.fKey = fNextKey++;
    fShms.insert( fShms.end(), data );
    pthread_mutex_unlock ( &fMutex );
    return data.fKey;
    }

// Lock a shared memory segment and get a pointer
// on it.
void* AliHLTSharedMemory::LockShm( AliUInt32_t shmID )
    {
    void * ptr=NULL;
    bool found = false;
    PSI_Status status;
    vector<ShmData>::iterator iter, end;
    pthread_mutex_lock( &fMutex );
    iter = fShms.begin();
    end = fShms.end();
    while ( iter != end )
	{
	if ( iter->fKey == shmID )
	    {
	    found = true;
	    break;
	    }
	iter++;
	}
    if ( found )
	{
	if ( iter->fShmType==kBigPhysShmType || iter->fShmType==kPhysMemShmType )
	    {
	    status = PSI_mapRegion( iter->fRegion, &ptr );
	    if ( status != PSI_OK )
		{
		LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Shm mapping" )
		    << "Shared memory area " << iter->fName << " could not be mapped. Reason: " 
		    << PSI_strerror( status ) << " (" << AliHLTLog::kDec << status << ")." << ENDLOG;
		pthread_mutex_unlock( &fMutex );
		return NULL;
		}
	    iter->fPtr = ptr;
	    }
	else if ( iter->fShmType==kSysVShmType || iter->fShmType==kSysVShmPrivateType )
	    {
	    ptr = shmat( iter->fID, NULL, 0 );
	    if ( ptr==(void*)-1 )
		{
		LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Shm mapping" )
		    << "System V shared memory area " << iter->fID << " could not be mapped. Reason: " 
		    << strerror(errno) << " (" << AliHLTLog::kDec << errno << ")." << ENDLOG;
		pthread_mutex_unlock( &fMutex );
		return NULL;
		}
	    iter->fPtr = ptr;
	    }
        else if ( iter->fShmType==kLibrorcShmType )
            {
#ifndef NOLIBRORC
            if( iter->fLibrorcBuffer == NULL )
            {
                LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Shm mapping" )
                    << "librorc buffer instance not initialized!" << ENDLOG;
		pthread_mutex_unlock( &fMutex );
                return NULL;
            }

            LOG( AliHLTLog::kInformational, "AliHLTSharedMemory", "LockShm")
                << "Locking buffer ID=" << LOG_DEC_HEX(iter->fLibrorcBuffer->getID())
                << " size=" << LOG_DEC_HEX(iter->fLibrorcBuffer->size()) << ENDLOG;

            ptr = iter->fLibrorcBuffer->getMem();
            iter->fPtr = ptr;
#endif
            }
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSharedMemory", "Shm mapping" )
	    << "Shared memory area with id " << shmID << " could not be found." << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return NULL;
	}
    pthread_mutex_unlock( &fMutex );
    return ptr;
    }
 
// Unlock a previously locked memory segment.
bool AliHLTSharedMemory::UnlockShm( AliUInt32_t shmID )
    {
    bool found = false;
    PSI_Status status;
    vector<ShmData>::iterator iter, end;
    pthread_mutex_lock( &fMutex );
    iter = fShms.begin();
    end = fShms.end();
    while ( iter != end )
	{
	if ( iter->fKey == shmID && iter->fPtr )
	    {
	    found = true;
	    break;
	    }
	iter++;
	}
    if ( found )
	{
	if ( iter->fShmType==kBigPhysShmType || iter->fShmType==kPhysMemShmType )
	    {
	    status = PSI_unmapRegion( iter->fRegion, iter->fPtr );
	    if ( status != PSI_OK )
		{
		LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Shm unmapping" )
		    << "Error unmapping region " << iter->fName << ". Reason: "
		    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status << ")." << ENDLOG;
		found = false;
		}
	    iter->fPtr = NULL;
	    }
	else if ( iter->fShmType==kSysVShmType || iter->fShmType==kSysVShmPrivateType )
	    {
	    int ret;
	    ret = shmdt( iter->fPtr );
	    if ( ret == -1 )
		{
		LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Shm unmapping" )
		    << "Error unmapping System V region " << iter->fID << ". Reason: "
		    << strerror(errno) << " (" << AliHLTLog::kDec << errno << ")." << ENDLOG;
		found = false;
		}
	    iter->fPtr = NULL;
	    }
        else if( iter->fShmType==kLibrorcShmType )
        {
            iter->fPtr = NULL;
        }

	iter = fShms.begin();
	end = fShms.end();
	while ( iter != end )
	    {
	    if ( iter->fKey == shmID )
		iter->fPtr = NULL;
	    iter++;
	    }
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSharedMemory", "Shm unmapping" )
		<< "Shared memory area with id " << shmID << " could not be found." << ENDLOG;
	}
    pthread_mutex_unlock( &fMutex );
    return found;
    }

// Release a shared memory segment.
bool AliHLTSharedMemory::ReleaseShm( AliUInt32_t shmID )
    {
    bool found = false;
    vector<ShmData>::iterator iter, end;
    pthread_mutex_lock( &fMutex );
    iter = fShms.begin();
    end = fShms.end();
    while ( iter != end )
	{
	if ( iter->fKey == shmID )
	    {
	    found = true;
	    break;
	    }
	iter++;
	}

    if ( found )
    {
        switch(iter->fShmType)
        {
            case kBigPhysShmType:
            case kPhysMemShmType:
                { CleanupRegion( iter->fRegion ); }
                break;
            case kSysVShmType:
            case kSysVShmPrivateType:
                { CleanupSysV( iter->fID ); }
                break;
            case kLibrorcShmType:
                {
#ifndef NOLIBRORC
                    if( iter->fLibrorcBuffer )
                    { delete iter->fLibrorcBuffer; }
                    if( iter->fLibrorcDevice)
                    { delete iter->fLibrorcDevice; }
#endif
                    fShms.erase( iter );
                }
                break;
            default:
                {
                    LOG( AliHLTLog::kWarning, "AliHLTSharedMemory", "ReleaseShm" )
                        << "Shared memory area with id " << shmID << " could not be found." << ENDLOG;
                }
                break;
        }
    }
    pthread_mutex_unlock( &fMutex );
    return found;
    }


bool AliHLTSharedMemory::GetShmStatus( AliUInt32_t shmID, ShmStatus* stat )
    {
    bool found = false;
    PSI_Status status;
    tRegionStatus regStat;
    vector<ShmData>::iterator iter, end;
    pthread_mutex_lock( &fMutex );
    iter = fShms.begin();
    end = fShms.end();
    while ( iter != end )
	{
	if ( iter->fKey == shmID )
	    {
	    found = true;
	    break;
	    }
	iter++;
	}
    if ( found )
	{
	if ( iter->fShmType==kBigPhysShmType || iter->fShmType==kPhysMemShmType )
	    {
	    status = PSI_getRegionStatus( iter->fRegion, &regStat );
	    if ( status != PSI_OK )
		{
		LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Shm get status" )
		    << "Error getting status for region " << iter->fName << ". Reason: "
		    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status << ")." << ENDLOG;
		found = false;
		}
	    stat->fRefCount = regStat.users;
	    stat->fPhysAddr = regStat.address;
	    stat->fSize = regStat.size;
	    }
	else if ( iter->fShmType==kSysVShmType )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Get shared memory info" )
		    << "Information for System V memory regions not supported." << ENDLOG;
	    pthread_mutex_unlock( &fMutex );
	    return false;
#if 0
	    int ret;
	    struct shmid_ds shmBufInfo;
	    ret = shmctl( data.fID, IPC_STAT, &shmBufInfo);
	    if ( ret==-1 )
		{
		LOG( AliHLTLog::kInformational, "AliHLTSharedMemory", "Get shared memory" )
		    << "Error obtaining information about System V memory region " << AliHLTLog::kDec << shmKey.fID << ": "
		    << strerror(errno) << " (" << errno << ")." << ENDLOG;
		return ~(AliUInt32_t)0;
		}
#endif
	    }
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSharedMemory", "Shm get status" )
		<< "Shared memory area with id " << shmID << " could not be found." << ENDLOG;
	}
    pthread_mutex_unlock( &fMutex );
    return found;
    }

bool AliHLTSharedMemory::GetPhysicalAddress( const void* virtAddr, void** physAddr, void** busAddr )
    {
    PSI_Status status;
    status = PSI_getPhysAddress( virtAddr, physAddr, busAddr );
    if ( status != PSI_OK )
	{
	LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Get phys address" )
	    << "Error getting physical address for virtual address 0x" << AliHLTLog::kHex 
	    << (unsigned long)virtAddr << ". Reason: "
	    << PSI_strerror(status) << " (" << AliHLTLog::kDec << status << ")." << ENDLOG;
	return false;
	}
    return true;
    }

bool AliHLTSharedMemory::GetPSIRegion( AliHLTShmID shmKey, tRegion& region )
    {
    vector<ShmData>::iterator iter, end;
    switch ( shmKey.fShmType )
	{
	case kPhysMemShmType: // Fallthrough
	case kBigPhysShmType:
	    {
	    pthread_mutex_lock( &fMutex );
	    iter = fShms.begin();
	    end = fShms.end();
	    while ( iter != end )
		{
		if ( iter->fShmType == shmKey.fShmType &&
		     iter->fKeyID == shmKey.fKey.fID )
		    {
		    region = iter->fRegion;
		    pthread_mutex_unlock( &fMutex );
		    return true;
		    }
		iter++;
		}
	    pthread_mutex_unlock( &fMutex );
	    return false; // Not found
	    }
	default:
	    return false; // Not supported
	}
    }

void AliHLTSharedMemory::CleanupRegion( tRegion region )
    {
    void* ptr = NULL;
    vector<ShmData>::iterator iter, end;
    bool found=true;
    while ( found )
	{
	found=false;
	iter = fShms.begin();
	end = fShms.end();
	while ( iter != end )
	    {
	    if ( (iter->fShmType == kPhysMemShmType ||
		  iter->fShmType == kBigPhysShmType) &&
		 iter->fRegion == region )
		{
		if ( iter->fPtr && !ptr )
		    ptr = iter->fPtr;
		if ( iter->fName != fErrorName && iter->fName!=gkSysVName )
		    delete [] iter->fName;
		iter->fName = NULL;
		found=true;
		fShms.erase( iter );
		break;
		}
	    iter++;
	    }
	}
    if ( found )
	{
	if ( ptr )
	    PSI_unmapRegion( region, iter->fPtr );
	PSI_closeRegion( region );
	}
    }



void AliHLTSharedMemory::CleanupSysV( AliUInt32_t id )
{
    vector<ShmData>::iterator iter, end;
    iter = fShms.begin();
    end = fShms.end();
    while ( iter != end ) {
        if ( iter->fID == id && (iter->fShmType==kSysVShmType ||
                    iter->fShmType==kSysVShmPrivateType)) {
            if (iter->fPtr) {
                shmdt(iter->fPtr);
            }
            shmctl(id, IPC_RMID, NULL);
            fShms.erase(iter);
            break;
        }
        ++iter;
    }
}

#ifndef NOLIBRORC
    bool AliHLTSharedMemory::AllocateLibrorcBuffer(AliHLTShmID shmKey,
                                                   size_t size,
                                                   librorc::device **device,
                                                   librorc::buffer **buffer) {
    uint32_t deviceId = AliHLTShmIDGetLibrorcDeviceId( shmKey );
    uint32_t bufferId = AliHLTShmIDGetLibrorcBufferId( shmKey );
    uint32_t overmap = AliHLTShmIDGetLibrorcOvermapFlag( shmKey );

    librorc::device *dev = NULL;
    librorc::buffer *buf = NULL;
    try
    { dev = new librorc::device(deviceId); }
    catch (int e)
    {
        LOG( AliHLTLog::kError, "AliHLTSharedMemory", "Get shared memory")
            << "Failed to initialize librorc device " << LOG_DEC_HEX(deviceId)
            << ": " << librorc::errMsg(e) << ENDLOG;
        *device = NULL;
        return false;
    }

    try
    { buf = new librorc::buffer(dev, size, bufferId, overmap); }
    catch (int e)
    {
        LOG( AliHLTLog::kError, "AliHLTSharedMemory", "AllocateLibrorcBuffer")
            << "Failed to allocate librorc buffer" << LOG_DEC_HEX(bufferId)
            << ": " << librorc::errMsg(e) << ENDLOG;
        *buffer = NULL;
        return false;
    }

    *buffer = buf;
    *device = dev;

    LOG( AliHLTLog::kInformational, "AliHLTSharedMemory", "AllocateLibrorcBuffer")
            << "DMA buffer ShmId:" << shmKey.fKey.fID
            << " librorc-ID:" << LOG_DEC_HEX(buf->getID())
            << ", physical size:" << LOG_DEC_HEX((buf->size()>>20))
            << "MB, mapping size:" << LOG_DEC_HEX((buf->getMappingSize()>>20))
            << "MB, scatter-gather-entries:" << LOG_DEC_HEX(buf->getnSGEntries())
            << ENDLOG;

    return true;
}

bool AliHLTSharedMemory::GetExistingLibrorcBuffer(AliHLTShmID shmKey,
                                                  librorc::buffer **buffer) {
    uint32_t deviceId = AliHLTShmIDGetLibrorcDeviceId( shmKey );
    uint32_t bufferId = AliHLTShmIDGetLibrorcBufferId( shmKey );
    uint32_t overmap = AliHLTShmIDGetLibrorcOvermapFlag( shmKey );

    if (overmap==0) {
        LOG( AliHLTLog::kWarning, "AliHLTSharedMemory", "GetExistingLibrorcBuffer")
            << "Requested librorc buffer without overmapping - "
            << "are you sure that's what you want?" << ENDLOG;
    }

    librorc::buffer *buf = NULL;

    try
    { buf = new librorc::buffer(deviceId, bufferId, overmap); }
    catch (int e)
    {
      LOG(AliHLTLog::kError, "AliHLTSharedMemory", "GetExistingLibrorcBuffer")
          << "Failed to attach to existing librorc buffer "
          << LOG_DEC_HEX(bufferId) << ": " << librorc::errMsg(e) << ENDLOG;
        *buffer = NULL;
        return false;
    }

    *buffer = buf;
    return true;
}

#endif

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

