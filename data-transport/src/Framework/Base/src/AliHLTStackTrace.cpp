/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTStackTrace.hpp"
#include "MLUCString.hpp"
#include "MLUCThread.hpp"
#include <signal.h>
#include <cstring>
#include <cstdlib>

#ifndef NO_STACK_TRACE
#include <execinfo.h>
#endif

void AliHLTStackTrace( const char* text, AliHLTLog::TLogLevel logLevel, AliUInt32_t stackDepth )
    {
#ifndef NO_STACK_TRACE
    if ( stackDepth <= 0 )
	{
	// ...
	return;
	}
    void** buffer = (void**)malloc( sizeof(void*)*stackDepth );
    if ( !buffer )
	{
	// ...
	return;
	}
    int count, i;
    count = backtrace( buffer, stackDepth );
    if ( !count )
	{
	// ...
	return;
	}
    char** syms;
    syms = backtrace_symbols( buffer, count );
    if ( !syms )
	{
	// ...
	return;
	}
    for ( i = 0; i < count; i++ )
	{
	LOG( logLevel, "Stacktrace", text )
	    << text << " stacktrace " << AliHLTLog::kDec << i << "/" << count << ": "
	    << syms[i] << "." << ENDLOG;
	}
    free( syms );
    free( buffer );
#else
    LOG( logLevel, "Stacktrace", text )
      << "Stacktraces not supported on this platform." << ENDLOG;
#endif
    }


// We hide the signal handlers which implement the stack trace in an anonymous
// namespace since they should only ever be called by the the signal handling
// mechanism.
namespace
    {
	
	sighandler_t gOldUSR1Handler = NULL;
	sighandler_t gOldUSR2Handler = NULL;
	
	void USR1SigHandlerStackTraceDistribute(int sig )
	    {
	    pthread_t me = pthread_self();
	    pid_t pid = getpid();
	    printf( "Thread %lu (PID %d) received SIGUSR1\n", (unsigned long)me, (int)pid );
	    if ( sig == SIGUSR1 )
		MLUCThread::Kill( SIGUSR2, false );
	    if ( gOldUSR1Handler )
		gOldUSR1Handler( sig );
	    
	    }
	
	void USR2SigHandlerStackTrace(int sig )
	    {
	    pthread_t me = pthread_self();
	    pid_t pid = getpid();
	    printf( "Thread %lu (PID %d) received SIGUSR2\n", (unsigned long)me, (int)pid );
	    char tmp[17];
	    snprintf( tmp, 17, "%d", (int)pthread_self() );
	    MLUCString text( "Signal triggered stack backtrace thread " );
	    text += tmp;
	    AliHLTStackTrace( text.c_str(), AliHLTLog::kError, 30 );
	    if ( gOldUSR2Handler )
		gOldUSR2Handler( sig );
	    }
	
}; // end of namespace


void AliHLTRegisterStackTraceHandlers()
    {
    gOldUSR1Handler = signal( SIGUSR1, USR1SigHandlerStackTraceDistribute );
    gOldUSR2Handler = signal( SIGUSR2, USR2SigHandlerStackTrace );
    }

void AliHLTUnregisterStackTraceHandlers()
    {
    // Restore old handlers, whatever they were.
    signal( SIGUSR1, gOldUSR1Handler );
    signal( SIGUSR2, gOldUSR2Handler );
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
