/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTReplySignal.hpp"

AliHLTReplySignal::AliHLTReplySignal()
    {
    fNextReplyNr = 0;
    pthread_mutex_init( &fReplyNrMutex, NULL );
    pthread_mutex_init( &fRepliesMutex, NULL );
    }

AliHLTReplySignal::~AliHLTReplySignal()
    {
    pthread_mutex_destroy( &fReplyNrMutex );
    pthread_mutex_destroy( &fRepliesMutex );
    }

AliUInt64_t AliHLTReplySignal::GetReplyNr()
    {
    AliUInt64_t reply;
    pthread_mutex_lock( &fReplyNrMutex );
    reply = ++fNextReplyNr;
    if ( !reply )
	reply = ++fNextReplyNr;
    pthread_mutex_unlock( &fReplyNrMutex );
    return reply;
    }

AliUInt64_t AliHLTReplySignal::WaitForReply( AliUInt64_t replyNr )
    {
    unsigned long ndx;
    AliUInt64_t retval = ~(AliUInt64_t)0;
    bool found = false;
    do
	{
	pthread_mutex_lock( &fRepliesMutex );
	if ( MLUCVectorSearcher<ReplyData,AliUInt64_t>::FindElement( fReplies, &ReplySearchFunc, replyNr, ndx ) )
	    {
	    retval = fReplies.GetPtr( ndx )->fRetval;
	    fReplies.Remove( ndx );
	    found = true;
	    }
	pthread_mutex_unlock( &fRepliesMutex );
	if ( !found )
	    Wait();
	}
    while ( !found );
    return retval;
    }

void AliHLTReplySignal::SignalReply( AliUInt64_t replyNr, AliUInt64_t retval )
    {
    ReplyData reply;
    reply.fReplyNr = replyNr;
    reply.fRetval = retval;
    pthread_mutex_lock( &fRepliesMutex );
    fReplies.Add( reply );
    pthread_mutex_unlock( &fRepliesMutex );
    Signal();
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
