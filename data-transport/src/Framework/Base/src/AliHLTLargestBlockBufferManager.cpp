/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTLargestBlockBufferManager.hpp"
#include "AliHLTLog.hpp"


AliHLTLargestBlockBufferManager::AliHLTLargestBlockBufferManager( unsigned bufferSizeExp2 ):
    fUsedBlocks( 8, bufferSizeExp2, true ),
    fFreeBlocks( bufferSizeExp2, true ),
    fAdaptBlockFreeUpdate(false)
    {
    fCurrentBlock = NULL;
    fCurrentBuffer = fBuffers.begin(); // NULL ?
    fCurrentBlockSet = false;
    fLargestSize = 0;
    fUsedBlocks.SetPreDivisor( gMemoryAlignment << 1 );
    pthread_mutex_init( &fBlockMutex, NULL );
    }

AliHLTLargestBlockBufferManager::AliHLTLargestBlockBufferManager( AliHLTShmID shmKey, unsigned long bufferSize, unsigned bufferSizeExp2 ):
    AliHLTBufferManager( shmKey, bufferSize ),
    fUsedBlocks( 8, bufferSizeExp2, true ),
    fFreeBlocks( bufferSizeExp2, true ),
    fAdaptBlockFreeUpdate(false)
    {
    if ( bufferSize % gMemoryAlignment )
	bufferSize -= (bufferSize % gMemoryAlignment);
    fCurrentBuffer = fBuffers.begin();
    fCurrentBlock = NULL;
    fCurrentBlockSet = false;
    TBlockData block;
    block.fBufferNdx = 0;
    block.fOffset = 0;
    block.fSize = bufferSize;
    fLargestSize = 0;
    InsertFree( block, false );
    //DumpBlocks();
    pthread_mutex_init( &fBlockMutex, NULL );
//     LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::AliHLTLargestBlockBufferManager", "Free Buffer Size" )
// 	<< "Free buffer size: " << AliHLTLog::kDec << fFreeBufferSize << " (0x"
// 	<< AliHLTLog::kHex << fFreeBufferSize << ")." << ENDLOG;
    }

AliHLTLargestBlockBufferManager::~AliHLTLargestBlockBufferManager()
    {
    while ( fUsedBlocks.GetCnt()>0 )
	{
	InsertFree( *fUsedBlocks.GetFirstPtr(), false );
	fUsedBlocks.RemoveFirst();
	}
    pthread_mutex_destroy( &fBlockMutex );
    }

bool AliHLTLargestBlockBufferManager::AddBuffer( AliHLTShmID shmKey, unsigned long bufferSize )
    {
    int ret;
    InsertBuffer( shmKey, bufferSize );
    TBlockData block;
    block.fBufferNdx = 0;
    block.fOffset = 0;
    block.fSize = bufferSize;
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::AddBuffer", "Mutex lock error" )
	    << "Unable to lock block mutex: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << "). Continuing..." << ENDLOG;
	}
    InsertFree( block, false );
//     DumpBlocks();
//     LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::AddBuffer", "Free Buffer Size" )
// 	<< "Free buffer size: " << AliHLTLog::kDec << fFreeBufferSize << " (0x"
// 	<< AliHLTLog::kHex << fFreeBufferSize << ")." << ENDLOG;
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::AddBuffer", "Mutex unlock error" )
	    << "Unable to unlock block mutex: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << "). Continuing..." << ENDLOG;
	}
    return true;
    }

bool AliHLTLargestBlockBufferManager::DeleteBuffer( AliHLTShmID shmKey, bool force )
    {
    bool foundUsed = false;
    int ret;
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::DeleteBuffer", "Mutex lock error" )
	    << "Unable to lock block mutex: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << "). Continuing..." << ENDLOG;
	}
    AliUInt32_t bufferNdx = GetBufferIndex( shmKey );
    unsigned long index;
    while ( MLUCIndexedVectorSearcher<TBlockData,TBlockIndexType,AliUInt32_t>::FindElement( fUsedBlocks, BufferBlockSearchFunc, bufferNdx, index ) )
	{
	if ( force )
	    {
	    fUsedBlocks.Remove( index );
	    fCurrentBlockSet = false;
	    }
	else
	    foundUsed = true;
	}
    if ( foundUsed )
	{
	LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::DeleteBuffer", "Cannot delete buffer" )
	    << "Unable to delete buffer. At least one block still uses it and deleting was not enforced." 
	    << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kInformational, "AliHLTLargestBlockBufferManager::DeleteBuffer", "Deleting buffer" )
	    << "Now removing buffer with shm id 0x" << AliHLTLog::kHex << shmKey.fKey.fID << " (" << AliHLTLog::kDec
	    << shmKey.fKey.fID << ")." << ENDLOG;
	do
	    {
	    foundUsed = false;
	    MLUCList<TBlockData>::TIterator freeIter, freeEnd;
	    freeIter = fFreeBlocks.Begin();
	    freeEnd = fFreeBlocks.End();
	    while ( freeIter != freeEnd )
		{
		if ( freeIter->fBufferNdx == bufferNdx )
		    {
		    fFreeBlocks.Erase( freeIter );
		    foundUsed = true;
		    break;
		    
		    }
		++freeIter;
		}
	    }
	while ( foundUsed ); 
	RemoveBuffer( shmKey );
	}

//     DumpBlocks();
//     LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::DeleteBuffer", "Free Buffer Size" )
// 	<< "Free buffer size: " << AliHLTLog::kDec << fFreeBufferSize << " (0x"
// 	<< AliHLTLog::kHex << fFreeBufferSize << ")." << ENDLOG;
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::DeleteBuffer", "Mutex unlock error" )
	    << "Unable to unlock block mutex: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << "). Continuing..." << ENDLOG;
	}
    return true;
    }

bool AliHLTLargestBlockBufferManager::CanGetBlock()
    {
    bool empty;
    int ret;
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::CanGetBlock", "Mutex lock error" )
	    << "Unable to lock block mutex: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << "). Continuing..." << ENDLOG;
	}
    empty = fFreeBlocks.IsEmpty();
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::CanGetBlock", "Mutex unlock error" )
	    << "Unable to unlock block mutex: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << "). Continuing..." << ENDLOG;
	}
    return !empty;
    }

bool AliHLTLargestBlockBufferManager::GetBlock( AliUInt32_t& bufferNdx, unsigned long& offset,
					       unsigned long& size )
    {
    if ( size % gMemoryAlignment )
	size += gMemoryAlignment -(size % gMemoryAlignment);
    int ret;
    //vector<TBlockData>::iterator freeIter, freeEnd, freeLargest;
    MLUCList<TBlockData>::TIterator freeIter, freeEnd, freeLargest;
    bool freeLargestSet = false;
#if __GNUC__<3
    freeLargest=NULL;
#endif
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::GetBlock", "Mutex lock error" )
	    << "Unable to lock block mutex: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << "). Continuing..." << ENDLOG;
	}

    if ( fLargestSize>0 && !fFreeBlocks.IsEmpty() )
	{
	freeLargest = fLargestFreeBlock;
	freeLargestSet = true;
	fLargestSize = 0;
	}
    else
	{
	freeIter = fFreeBlocks.Begin();
	freeEnd = fFreeBlocks.End();
	while ( freeIter != freeEnd )
	    {
	    if ( !freeLargestSet || freeIter->fSize > freeLargest->fSize )
		{
		freeLargestSet = true;
		freeLargest = freeIter;
		}
	    ++freeIter;
	    }
	}
    if ( freeLargestSet && freeLargest->fSize>=size )
	{
	bufferNdx = freeLargest->fBufferNdx;
	offset = freeLargest->fOffset;
	if ( !size ) //  || freeLargest->fSize==size )
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::GetBlock", "Block found" )
		<< "GetBlock using whole block: " << AliHLTLog::kDec
		<< "Offset: " << freeLargest->fOffset << " - Size: " << freeLargest->fSize
		<< " - Buffer: " << freeLargest->fBufferNdx << "." << ENDLOG;
	    size = freeLargest->fSize;
	    unsigned long currentBlockIndex;
	    fUsedBlocks.Add( *freeLargest, 
			     TBlockIndexType(freeLargest->fBufferNdx, freeLargest->fOffset ), 
			     currentBlockIndex );
	    fCurrentBlock = fUsedBlocks.GetPtr( currentBlockIndex );
	    fCurrentBlockSet = true;
	    if ( !fAdaptBlockFreeUpdate )
		{
		unsigned long oldFree = GetFreeBufferSize();
		DecreaseFree( freeLargest->fSize );
		unsigned long newFree = GetFreeBufferSize();
		LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::GetBlock", "DecreaseFree" )
		    << "DecreaseFree: " << AliHLTLog::kDec << oldFree << " - " << freeLargest->fSize
		    << " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
		}
	    fFreeBlocks.Erase( freeLargest );
	    }
	else
	    {
	    TBlockData freeB, usedB;
	    freeB = usedB = *freeLargest;
	    freeB.fOffset += size;
	    freeB.fSize -= size;
	    usedB.fSize = size;
	    LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::GetBlock", "Block found" )
		<< "GetBlock using partial block: " << AliHLTLog::kDec
		<< "Offset: " << usedB.fOffset << " - Size: " << usedB.fSize
		<< " - Buffer: " << usedB.fBufferNdx << "." << ENDLOG;
	    LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::GetBlock", "Block found" )
		<< "GetBlock remaining block: " << AliHLTLog::kDec
		<< "Offset: " << freeB.fOffset << " - Size: " << freeB.fSize
		<< " - Buffer: " << freeB.fBufferNdx << "." << ENDLOG;
	    if ( !fAdaptBlockFreeUpdate )
		{
		unsigned long oldFree = GetFreeBufferSize();
		DecreaseFree( freeLargest->fSize );
		unsigned long newFree = GetFreeBufferSize();
		LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::GetBlock", "DecreaseFree" )
		    << "DecreaseFree: " << AliHLTLog::kDec << oldFree << " - " << freeLargest->fSize
		    << " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
		}
	    else
		{
		unsigned long oldFree = GetFreeBufferSize();
		DecreaseFree( size );
		unsigned long newFree = GetFreeBufferSize();
		LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::GetBlock", "DecreaseFree" )
		    << "DecreaseFree: " << AliHLTLog::kDec << oldFree << " - " << size
		    << " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
		}
	    fFreeBlocks.Erase( freeLargest );
	    if ( !fAdaptBlockFreeUpdate )
		{
		unsigned long oldFree = GetFreeBufferSize();
		InsertFree( freeB );
		unsigned long newFree = GetFreeBufferSize();
		LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::GetBlock", "InsertFree" )
		    << "InsertFree(true): " << AliHLTLog::kDec << oldFree << " - " << freeB.fSize
		    << " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
		}
	    else
		{
		unsigned long oldFree = GetFreeBufferSize();
		InsertFree( freeB, false );
		unsigned long newFree = GetFreeBufferSize();
		LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::GetBlock", "InsertFree" )
		    << "InsertFree(false): " << AliHLTLog::kDec << oldFree << " - " << freeB.fSize
		    << " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
		}
	    unsigned long currentBlockIndex;
	    fUsedBlocks.Add( usedB, 
			     TBlockIndexType(usedB.fBufferNdx, usedB.fOffset ), 
			     currentBlockIndex );
	    fCurrentBlock = fUsedBlocks.GetPtr( currentBlockIndex );
	    fCurrentBlockSet = true;
	    }
	}
	else
	    {
	    LOG( AliHLTLog::kInformational, "AliHLTLargestBlockBufferManager::GetBlock", "No block found" )
		<< "GetBlock could not find block of size " << AliHLTLog::kDec << size << "." << ENDLOG;
	    freeLargestSet = false;
	    }

//     DumpBlocks();
//     LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::GetBlock", "Free Buffer Size" )
// 	<< "Free buffer size: " << AliHLTLog::kDec << fFreeBufferSize << " (0x"
// 	<< AliHLTLog::kHex << fFreeBufferSize << ")." << ENDLOG;
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::GetBlock", "Mutex unlock error" )
	    << "Unable to unlock block mutex: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << "). Continuing..." << ENDLOG;
	}

    return freeLargestSet;
    }

bool AliHLTLargestBlockBufferManager::AdaptBlock( AliUInt32_t bufferNdx, unsigned long offset,
						 unsigned long newSize )
    {
    if ( newSize % gMemoryAlignment )
	newSize += gMemoryAlignment -(newSize % gMemoryAlignment);
    bool ok = false;
    int ret;
    vector<TBlockData>::iterator iter, end;
    TBlockData* adaptBlock;
    bool adaptBlockSet = false;
#if __GNUC__<3
    adaptBlock=NULL;
#endif
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::AdaptBlock", "Mutex lock error" )
	    << "Unable to lock block mutex: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << "). Continuing..." << ENDLOG;
	}
    if ( fCurrentBlockSet && fCurrentBlock->fBufferNdx == bufferNdx &&
	 fCurrentBlock->fOffset == offset )
	{
	adaptBlock = fCurrentBlock;
	adaptBlockSet = true;
	}
    else
	{
	unsigned long index;
	if ( fUsedBlocks.FindElement( TBlockIndexType( bufferNdx, offset ), index ) )
	    {
	    adaptBlock = fUsedBlocks.GetPtr( index );
	    adaptBlockSet = true;
	    }
	}
    if ( adaptBlockSet )
	{
	LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::AdaptBlock", "Resizing block" )
	    << "Resizing block " << AliHLTLog::kDec << adaptBlock->fBufferNdx << "/" << adaptBlock->fOffset
	    << "/" << adaptBlock->fSize << " to size " << newSize << "." << ENDLOG;
	if ( newSize == adaptBlock->fSize )
	    {
	    if ( fAdaptBlockFreeUpdate )
		{
		unsigned long oldFree = GetFreeBufferSize();
		DecreaseFree( newSize );
		unsigned long newFree = GetFreeBufferSize();
		LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::AdaptBlock", "DecreaseFree" )
		    << "DecreaseFree: " << AliHLTLog::kDec << oldFree << " - " << newSize
		    << " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
		}
	    ok = true;
	    }
	if ( newSize < adaptBlock->fSize )
	    {
	    TBlockData block;
	    block.fBufferNdx = bufferNdx;
	    block.fOffset = offset+newSize;
	    block.fSize = adaptBlock->fSize-newSize;
	    if ( !fAdaptBlockFreeUpdate )
		{
		unsigned long oldFree = GetFreeBufferSize();
		InsertFree( block );
		unsigned long newFree = GetFreeBufferSize();
		LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::AdaptBlock", "InsertFree" )
		    << "InsertFree(true): " << AliHLTLog::kDec << oldFree << " - " << block.fSize
		    << " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
		}
	    else
		{
		unsigned long oldFree = GetFreeBufferSize();
		InsertFree( block, false );
		unsigned long newFree = GetFreeBufferSize();
		LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::AdaptBlock", "InsertFree" )
		    << "InsertFree(false): " << AliHLTLog::kDec << oldFree << " - " << block.fSize
		    << " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
		oldFree = GetFreeBufferSize();
		DecreaseFree( newSize );
		newFree = GetFreeBufferSize();
		LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::AdaptBlock", "DecreaseFree" )
		    << "DecreaseFree: " << AliHLTLog::kDec << oldFree << " - " << newSize
		    << " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
		}
	    adaptBlock->fSize = newSize;
	    ok = true;
	    }
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::AdaptBlock", "No block to resize" )
	    << "Could not find block " << AliHLTLog::kDec << bufferNdx << "/" << offset
	    << " to resize." << ENDLOG;
	}
    

//     DumpBlocks();
//     LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::AdaptBlock", "Free Buffer Size" )
// 	<< "Free buffer size: " << AliHLTLog::kDec << fFreeBufferSize << " (0x"
// 	<< AliHLTLog::kHex << fFreeBufferSize << ")." << ENDLOG;
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::AdaptBlock", "Mutex unlock error" )
	    << "Unable to unlock block mutex: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << "). Continuing..." << ENDLOG;
	}
    return ok;
    }

bool AliHLTLargestBlockBufferManager::AdaptBlock( AliUInt32_t bufferNdx, unsigned long oldOffset,
						 unsigned long newOffset, unsigned long newSize )
    {
    if ( newSize % gMemoryAlignment )
	newSize += gMemoryAlignment -(newSize % gMemoryAlignment);
    if ( newOffset % gMemoryAlignment )
	newOffset -= newOffset % gMemoryAlignment;
    bool ok = false;
    int ret;
    vector<TBlockData>::iterator iter, end;
    TBlockData* adaptBlock;
    bool adaptBlockSet = false;
#if __GNUC__<3
    adaptBlock=NULL;
#endif
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::AdaptBlock", "Mutex lock error" )
	    << "Unable to lock block mutex: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << "). Continuing..." << ENDLOG;
	}
    if ( fCurrentBlockSet && fCurrentBlock->fBufferNdx == bufferNdx &&
	 fCurrentBlock->fOffset == oldOffset )
	{
	adaptBlock = fCurrentBlock;
	adaptBlockSet = true;
	}
    else
	{
	unsigned long index;
	if ( fUsedBlocks.FindElement( TBlockIndexType( bufferNdx, oldOffset ), index ) )
	    {
	    adaptBlock = fUsedBlocks.GetPtr( index );
	    adaptBlockSet = true;
	    }
	}
    if ( adaptBlockSet )
	{
	LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::AdaptBlock", "Resizing block" )
	    << "Resizing block " << AliHLTLog::kDec << adaptBlock->fBufferNdx << "/" << adaptBlock->fOffset
	    << "/" << adaptBlock->fSize << " to new offset and size " << newOffset << "/" << newSize << "." << ENDLOG;
	if ( newSize == adaptBlock->fSize && newOffset == adaptBlock->fOffset )
	    ok = true;
	if ( newOffset+newSize > adaptBlock->fOffset+adaptBlock->fSize )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::AdaptBlock", "Block too big" )
		<< "Block exceeds end offset limit of " << AliHLTLog::kDec << adaptBlock->fOffset+adaptBlock->fSize
		<< " (attempt: " << newOffset+newSize << ")." << ENDLOG;
	    }
	if ( newOffset<adaptBlock->fOffset )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::AdaptBlock", "Block starts too early" )
		<< "Block exceeds start offset limit of " << AliHLTLog::kDec << adaptBlock->fOffset
		<< " (attempt: " << newOffset << ")." << ENDLOG;
	    }
	if ( fAdaptBlockFreeUpdate )
	    {
	    unsigned long oldFree = GetFreeBufferSize();
	    DecreaseFree( newSize );
	    unsigned long newFree = GetFreeBufferSize();
		LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::AdaptBlock", "DecreaseFree" )
		    << "DecreaseFree: " << AliHLTLog::kDec << oldFree << " - " << newSize
		    << " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
	    }
	if ( newSize < adaptBlock->fSize && newOffset == adaptBlock->fOffset )
	    {
	    // Only slack at end
	    TBlockData block;
	    block.fBufferNdx = bufferNdx;
	    block.fOffset = newOffset+newSize;
	    block.fSize = adaptBlock->fSize-newSize;
	    unsigned long oldFree = GetFreeBufferSize();
	    InsertFree( block, !fAdaptBlockFreeUpdate );
	    unsigned long newFree = GetFreeBufferSize();
	    LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::AdaptBlock", "InsertFree" )
		<< "InsertFree(" << (!fAdaptBlockFreeUpdate ? "true" : "false") << "): " << AliHLTLog::kDec << oldFree << " - " << block.fSize
		<< " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
	    adaptBlock->fSize = newSize;
	    ok = true;
	    }
	else if ( newOffset+newSize == adaptBlock->fOffset+adaptBlock->fSize && newOffset > adaptBlock->fOffset )
	    {
	    // Only slack at beginning
	    TBlockData block;
	    block.fBufferNdx = bufferNdx;
	    block.fOffset = adaptBlock->fOffset;
	    block.fSize = newOffset-adaptBlock->fOffset;
	    unsigned long oldFree = GetFreeBufferSize();
	    InsertFree( block, !fAdaptBlockFreeUpdate );
	    unsigned long newFree = GetFreeBufferSize();
	    LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::AdaptBlock", "InsertFree" )
		<< "InsertFree(" << (!fAdaptBlockFreeUpdate ? "true" : "false") << "): " << AliHLTLog::kDec << oldFree << " - " << block.fSize
		<< " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
	    adaptBlock->fOffset = newOffset;
	    adaptBlock->fSize = newSize;
	    ok = true;
	    }
	else if ( newOffset+newSize < adaptBlock->fOffset+adaptBlock->fSize && newOffset > adaptBlock->fOffset )
	    {
	    // Slack at beginning and end
	    TBlockData block;
	    block.fBufferNdx = bufferNdx;
	    block.fOffset = adaptBlock->fOffset;
	    block.fSize = newOffset-adaptBlock->fOffset;
	    unsigned long oldFree = GetFreeBufferSize();
	    InsertFree( block, !fAdaptBlockFreeUpdate );
	    unsigned long newFree = GetFreeBufferSize();
	    LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::AdaptBlock", "InsertFree" )
		<< "InsertFree(" << (!fAdaptBlockFreeUpdate ? "true" : "false") << "): " << AliHLTLog::kDec << oldFree << " - " << block.fSize
		<< " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
	    block.fOffset = newOffset+newSize;
	    block.fSize = adaptBlock->fOffset+adaptBlock->fSize-(newOffset+newSize);
	    oldFree = GetFreeBufferSize();
	    InsertFree( block, !fAdaptBlockFreeUpdate );
	    newFree = GetFreeBufferSize();
	    LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::AdaptBlock", "InsertFree" )
		<< "InsertFree(" << (!fAdaptBlockFreeUpdate ? "true" : "false") << "): " << AliHLTLog::kDec << oldFree << " - " << block.fSize
		<< " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
	    adaptBlock->fOffset = newOffset;
	    adaptBlock->fSize = newSize;
	    ok = true;
	    }
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::AdaptBlock", "No block to resize" )
	    << "Could not find block " << AliHLTLog::kDec << bufferNdx << "/" << oldOffset
	    << " to resize." << ENDLOG;
	}
    

//     DumpBlocks();
//     LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::AdaptBlock", "Free Buffer Size" )
// 	<< "Free buffer size: " << AliHLTLog::kDec << fFreeBufferSize << " (0x"
// 	<< AliHLTLog::kHex << fFreeBufferSize << ")." << ENDLOG;
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::AdaptBlock", "Mutex unlock error" )
	    << "Unable to unlock block mutex: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << "). Continuing..." << ENDLOG;
	}
    return ok;
    }

bool AliHLTLargestBlockBufferManager::ReleaseBlock( AliUInt32_t bufferNdx, unsigned long offset, bool noAdaptBlock )
    {
    int ret;
    bool ok = false;
    vector<TBlockData>::iterator iter, end;
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::ReleaseBlock", "Mutex lock error" )
	    << "Unable to lock block mutex: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << "). Continuing..." << ENDLOG;
	}

    unsigned long index;
#if 0
    MLUCString tmpUsedBefore, tmpUsedAfter;
    fUsedBlocks.AsString( tmpUsedBefore );
    if ( !fUsedBlocks.IsConsistent() )
	{
	LOG( AliHLTLog::kImportant, "AliHLTLargestBlockBufferManager::ReleaseBlock", "Inconsistent used blocks before release" )
	    << "Inconsistent used blocks before release: " << tmpUsedBefore.c_str() << ENDLOG;
	}
#endif
    if ( fUsedBlocks.FindElement( TBlockIndexType( bufferNdx, offset ), index ) )
	{
	unsigned long oldFree = GetFreeBufferSize();
	InsertFree( *fUsedBlocks.GetPtr(index), !noAdaptBlock );
	unsigned long newFree = GetFreeBufferSize();
	LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::ReleaseBlock", "InsertFree" )
	    << "InsertFree(" << (!noAdaptBlock ? "true" : "false") << "): " << AliHLTLog::kDec << oldFree << " - " << (*fUsedBlocks.GetPtr(index)).fSize
	    << " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
	fUsedBlocks.Remove( index );
#if 0
	if ( !fUsedBlocks.IsConsistent() )
	    {
	    fUsedBlocks.AsString( tmpUsedAfter );
	    LOG( AliHLTLog::kImportant, "AliHLTLargestBlockBufferManager::ReleaseBlock", "Inconsistent used blocks after release" )
		<< "Inconsistent used blocks after release - Used blocks before: " << tmpUsedBefore.c_str() << ENDLOG;
	    LOG( AliHLTLog::kImportant, "AliHLTLargestBlockBufferManager::ReleaseBlock", "Inconsistent used blocks after release" )
		<< "Inconsistent used blocks after release - Used blocks after: " << tmpUsedAfter.c_str() << ENDLOG;
	    }
#endif
	fCurrentBlockSet = false;
	ok = true;
	}

    if ( !ok )
	{
	LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::ReleaseBlock", "No block to release" )
	    << "Could not find block " << AliHLTLog::kDec << bufferNdx << "/" << offset
	    << " to release." << ENDLOG;
	}
    
//     DumpBlocks();
//     LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::ReleaseBlock", "Free Buffer Size" )
// 	<< "Free buffer size: " << AliHLTLog::kDec << fFreeBufferSize << " (0x"
// 	<< AliHLTLog::kHex << fFreeBufferSize << ")." << ENDLOG;
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTLargestBlockBufferManager::ReleaseBlock", "Mutex unlock error" )
	    << "Unable to unlock block mutex: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << "). Continuing..." << ENDLOG;
	}
    return ok;
    }


void AliHLTLargestBlockBufferManager::InsertFree( TBlockData& block, bool updateFree )
    {
//     LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::InsertFree", "Block Insert" )
// 	<< "Block to insert " << AliHLTLog::kDec << block.fBufferNdx << "/" << block.fOffset << "/" 
// 	<< block.fSize << "." << ENDLOG;
    // WARNING: The algorithm will only work with containers that remain valid over insertion/deletions.
    // This is not true for MLUCVector/MLUCIndexedVector/std::vector
    if ( block.fSize<=0 )
	return;
    MLUCList<TBlockData>::TIterator iter, end, current;
    bool done = false;
    bool largestValid = false;
    if ( !fFreeBlocks.IsEmpty() && fLargestSize>0 )
	largestValid = true;
    iter = fFreeBlocks.Begin();
    end = fFreeBlocks.End();

    while ( iter != end )
	{
	if ( !largestValid && iter->fSize>fLargestSize )
	    {
	    fLargestSize = iter->fSize;
	    fLargestFreeBlock = iter;
	    }
	if ( !done )
	    {
	    if ( iter->fBufferNdx > block.fBufferNdx )
		{
		LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::InsertFree", "Inserting block" )
		    << "Inserting block " << AliHLTLog::kDec << block.fBufferNdx << "/" << block.fOffset << "/" 
		    << block.fSize << " before block " << iter->fBufferNdx << "/" << iter->fOffset << "/"
		    << iter->fSize << "." << ENDLOG;
		done = true;
		if ( updateFree )
		    {
		    // 		LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::InsertFree", "IncreaseFree" )
		    // 		    << "IncreaseFree: " << AliHLTLog::kDec << block.fSize << "." << ENDLOG;
		    unsigned long oldFree = GetFreeBufferSize();
		    IncreaseFree( block.fSize );
		    unsigned long newFree = GetFreeBufferSize();
		    LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::InsertFree", "IncreaseFree" )
			<< "IncreaseFree: " << AliHLTLog::kDec << oldFree << " - " << block.fSize
			<< " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
		    }
		current = fFreeBlocks.Insert( iter, block );
		if ( !largestValid || current->fSize > fLargestSize )
		    {
		    fLargestFreeBlock = current;
		    fLargestSize = current->fSize;
		    }
		if ( largestValid )
		    break;
		else
		    {
		    iter = current;
		    ++iter;
		    continue;
		    }
		}
	    if ( iter->fBufferNdx == block.fBufferNdx )
		{
		if ( iter->fOffset == block.fOffset+block.fSize )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::InsertFree", "Merging block" )
			<< "Merging block " << AliHLTLog::kDec << block.fBufferNdx << "/" << block.fOffset << "/" 
			<< block.fSize << " before block " << iter->fBufferNdx << "/" << iter->fOffset << "/"
			<< iter->fSize << "." << ENDLOG;
		    done = true;
		    iter->fOffset = block.fOffset;
		    iter->fSize += block.fSize;
		    if ( updateFree )
			{
			// 		    LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::InsertFree", "IncreaseFree" )
			// 			<< "IncreaseFree: " << AliHLTLog::kDec << block.fSize << "." << ENDLOG;
			unsigned long oldFree = GetFreeBufferSize();
			IncreaseFree( block.fSize );
			unsigned long newFree = GetFreeBufferSize();
			LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::InsertFree", "IncreaseFree" )
			    << "IncreaseFree: " << AliHLTLog::kDec << oldFree << " - " << block.fSize
			    << " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
			}
		    if ( !largestValid || iter->fSize > fLargestSize )
			{
			fLargestFreeBlock = iter;
			fLargestSize = iter->fSize;
			}
		    if ( largestValid )
			break;
		    }
		if ( iter->fOffset+iter->fSize == block.fOffset )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::InsertFree", "Merging block" )
			<< "Merging block " << AliHLTLog::kDec << block.fBufferNdx << "/" << block.fOffset << "/" 
			<< block.fSize << " after block " << iter->fBufferNdx << "/" << iter->fOffset << "/"
			<< iter->fSize << "." << ENDLOG;
		    done = true;
		    iter->fSize += block.fSize;
		    if ( updateFree )
			{
			// 		    LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::InsertFree", "IncreaseFree" )
			// 			<< "IncreaseFree: " << AliHLTLog::kDec << block.fSize << "." << ENDLOG;
			unsigned long oldFree = GetFreeBufferSize();
			IncreaseFree( block.fSize );
			unsigned long newFree = GetFreeBufferSize();
			LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::InsertFree", "IncreaseFree" )
			    << "IncreaseFree: " << AliHLTLog::kDec << oldFree << " - " << block.fSize
			    << " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
			}
		    if ( (iter+1) != end && (iter+1)->fOffset == iter->fOffset+iter->fSize )
			{
			LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::InsertFree", "Merging block" )
			    << "Merging new merged block " << AliHLTLog::kDec << iter->fBufferNdx << "/" << iter->fOffset << "/" 
			    << iter->fSize << " with following block " << (iter+1)->fBufferNdx << "/" << (iter+1)->fOffset << "/"
			    << (iter+1)->fSize << "." << ENDLOG;
			iter->fSize += (iter+1)->fSize;
			fFreeBlocks.Erase( (iter+1) );
			}
		    // XX Attention: iter is only valid if the area pointed to by it is not changed by the 
		    // preceeding erase action. This is not true for vector/MLUCVector, but for MLUCList.
		    if ( !largestValid || iter->fSize > fLargestSize )
			{
			fLargestFreeBlock = iter;
			fLargestSize = iter->fSize;
			}
		    if ( largestValid )
			break;
		    }
		if ( iter->fOffset > block.fOffset )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::InsertFree", "Inserting block" )
			<< "Inserting block " << AliHLTLog::kDec << block.fBufferNdx << "/" << block.fOffset << "/" 
			<< block.fSize << " before block " << iter->fBufferNdx << "/" << iter->fOffset << "/"
			<< iter->fSize << "." << ENDLOG;
		    done = true;
		    current = fFreeBlocks.Insert( iter, block );
		    if ( updateFree )
			{
			// 		    LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::InsertFree", "IncreaseFree" )
			// 			<< "IncreaseFree: " << AliHLTLog::kDec << block.fSize << "." << ENDLOG;
			unsigned long oldFree = GetFreeBufferSize();
			IncreaseFree( block.fSize );
			unsigned long newFree = GetFreeBufferSize();
			LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::InsertFree", "IncreaseFree" )
			    << "IncreaseFree: " << AliHLTLog::kDec << oldFree << " - " << block.fSize
			    << " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
			}
		    if ( !largestValid || current->fSize > fLargestSize )
			{
			fLargestFreeBlock = current;
			fLargestSize = current->fSize;
			}
		    if ( largestValid )
			break;
		    else
			{
			iter = current;
			++iter;
			continue;
			}
		    }
		}
	    }
	++iter;
	}
    if ( !done )
	{
	LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::InsertFree", "Appending block" )
	    << "Appending block " << AliHLTLog::kDec << block.fBufferNdx << "/" << block.fOffset << "/" 
	    << block.fSize << " at end of free block list." << ENDLOG;
	current = fFreeBlocks.Insert( fFreeBlocks.End(), block );
	if ( updateFree )
	    {
// 	    LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::InsertFree", "IncreaseFree" )
// 		<< "IncreaseFree: " << AliHLTLog::kDec << block.fSize << "." << ENDLOG;
	    unsigned long oldFree = GetFreeBufferSize();
	    IncreaseFree( block.fSize );
	    unsigned long newFree = GetFreeBufferSize();
	    LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::InsertFree", "IncreaseFree" )
		<< "IncreaseFree: " << AliHLTLog::kDec << oldFree << " - " << block.fSize
		<< " - " << newFree << " - " << GetTotalBufferSize() << ENDLOG;
	    }
	if ( !largestValid || current->fSize > fLargestSize )
	    {
	    fLargestFreeBlock = current;
	    fLargestSize = current->fSize;
	    }
	}
//     LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::InsertFree", "Free Buffer Size" )
// 	<< "Free buffer size: " << AliHLTLog::kDec << fFreeBufferSize << " (0x"
// 	<< AliHLTLog::kHex << fFreeBufferSize << ")." << ENDLOG;
    }


void AliHLTLargestBlockBufferManager::DumpBlocks()
    {
    {
    MLUCList<TBlockData>::TIterator iter, end;
    iter = fFreeBlocks.Begin();
    end = fFreeBlocks.End();
    while ( iter != end )
	{
	LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::DumpBlocks", "Free Block" )
	    << "Free block " << AliHLTLog::kDec << iter->fBufferNdx << "/" << iter->fOffset << "/"
	    << iter->fSize << "." << ENDLOG;
	++iter;
	}
    }
	{
	MLUCIndexedVector<TBlockData,TBlockIndexType>::TIterator iter;
	iter = fUsedBlocks.Begin();
	while ( iter )
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTLargestBlockBufferManager::DumpBlocks", "Used Block" )
		<< "Used block " << AliHLTLog::kDec << iter->fBufferNdx << "/" << iter->fOffset << "/"
		<< iter->fSize << "." << ENDLOG;
	    ++iter;
	    }
	}
    }

bool AliHLTLargestBlockBufferManager::BufferBlockSearchFunc( const TBlockData& el, const AliUInt32_t& searchBufferNdx )
    {
    return el.fBufferNdx == searchBufferNdx;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
