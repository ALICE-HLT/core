/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTGetComAddress.hpp"
#include "BCLSCIComID.hpp"
#include "AliHLTLog.hpp"
#include "BCLSCIAddress.hpp"

#define IPADDR_2_SCINODEID_FILENAME "/etc/SCINodes"

int AliHLTGetComAddress( AliHLTNodeID_t nodeID, BCLAbstrAddressStruct* addressInput )
    {
    BCLAbstrAddress addressClass( addressInput );
    BCLAbstrAddressStruct* address = addressClass.GetData();
    bool found = false;
    switch ( address->fComID )
	{
	case kSCIComID:
	    {
	    BCLSCIAddress sciAddressClass( (BCLSCIAddressStruct*)addressInput );
	    BCLSCIAddressStruct* sciAddress = sciAddressClass.GetData();
	    FILE* fp;
	    int ret;
	    unsigned ip1, ip2, ip3, ip4, sci;
	    unsigned rip1, rip2, rip3, rip4;
	    rip1 = (nodeID & 0xFF000000) >> 24;
	    rip2 = (nodeID & 0x00FF0000) >> 16;
	    rip3 = (nodeID & 0x0000FF00) >>  8;
	    rip4 = (nodeID & 0x000000FF);
	    fp = fopen( IPADDR_2_SCINODEID_FILENAME, "r" );
	    if ( fp == NULL )
		{
		LOG( AliHLTLog::kError, "AliHLTGetComAddress", "No SCI node ID file" )
		    << "Unable to open SCI node ID file " << IPADDR_2_SCINODEID_FILENAME
		    << ". Reason: " << strerror(errno) << " (" << AliHLTLog:.kDec << errno
		    << ")." << ENDLOG;
		return EFAULT;
		}
	    // FIXME: Fill this...


	    MLUCString line;
	    char buffer[257];
	    int len;
	    while ( !feof( fp ) )
		{
		line = "";
		do
		    {
		    fgets( buffer, 256, fp );
		    len = strlen(buffer);
		    if ( len==0 || buffer[len-1]=='\n' )
			{
			buffer[len-1] = 0;
			line += buffer;
			break;
			}
		    line += buffer;
		    }
		while ( !feof(fp) );
		ret = sscanf( line.c_str(), " %u.%u.%u.%u %u",
			      &ip1, &ip2, &ip3, &ip4, &sci );
		if ( ret == 5 )
		    {
		    if ( ip1==rip4 && ip2==rip3 && ip3==rip2 && ip4==rip1 )
			{
			address->fNodeID = (uint16)sci;
			*(BCLSCIAddressStruct*)adressInput = *sciAddressStruct;
			found = true;
			break;
			}
		    }
		}
	    fclose( fp );
	    break;
	    }
	default:
	    {
	    return EINVAL;
	    break;
	    }
	}
    if ( found )
	return 0;
    else
	return EINVAL;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
