/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://wiki.kip.uni-heidelberg.de/ti/HLT/index.php/Main_Page
** or the corresponding page of the Heidelberg HLT group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author: artur $ - Initial Version by Timm Morten Steinbeck
**
** $Id: $ 
**
***************************************************************************
*/

#include "AliHLTEventMetaDataOutput.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTEventTriggerStruct.hpp"
#include "AliHLTSubEventDataDescriptor.h"
#include "MLUCString.hpp"
#include <vector>
#include <fcntl.h>
#include <cerrno>
#include <glob.h>
#include <cstdlib>

#if __GNUC__>=3
using namespace std;
#endif

AliHLTEventMetaDataOutput::AliHLTEventMetaDataOutput( const char* filenamePrefix, bool aliceHLT, unsigned long runNumber ):
    fFilenamePrefix( filenamePrefix ),
    fAliceHLT( aliceHLT ),
    fRunNumber( runNumber ),
    fFileHandle( -1 ),
    fEventTriggerFormatFunc( NULL )
    {
    }

AliHLTEventMetaDataOutput::~AliHLTEventMetaDataOutput()
    {
    Close();
    }

int AliHLTEventMetaDataOutput::Open()
    {
    MLUCString filename ( fFilenamePrefix );
    if ( fRunNumber )
	{
	char tmp[256];
	snprintf( tmp, 256, "%0lu", fRunNumber );
	filename += "-";
	filename += tmp;
	}
    struct timeval tv;
    gettimeofday( &tv, NULL );
    MLUCString timestamp = MLUCString::TimeStamp2String( tv );
    filename += "-";
    filename += timestamp;
    filename += ".eventdump";
    fFileHandle = open( filename.c_str(), O_CREAT|O_TRUNC|O_WRONLY, 0666 );
    if ( fFileHandle==-1 )
	return errno;
    return 0;
    }

int AliHLTEventMetaDataOutput::Close()
    {
    if ( fFileHandle!=-1 )
	close( fFileHandle );
    return 0;
    }

bool AliHLTEventMetaDataOutput::WriteEvent( const AliEventID_t& eventID,
					    const AliHLTSubEventDataDescriptor* sedd,
					    const AliHLTEventTriggerStruct* ets,
					    const char* classifier )
    {
    struct timeval tv;
    tv.tv_sec = tv.tv_usec = 0;
    return WriteEvent( tv, eventID, sedd, ets, classifier );
    }

bool AliHLTEventMetaDataOutput::WriteEvent( struct timeval timestamp,
					    const AliEventID_t& eventID,
					    const AliHLTSubEventDataDescriptor* sedd,
					    const AliHLTEventTriggerStruct* ets, 
					    const char* classifier )
    {
    MLUCString time_str = MLUCString::TimeStamp2String( timestamp );
    MLUCString eventStr;
    eventStr = time_str;

    if ( classifier )
	{
	eventStr += " ";
	eventStr += classifier;
	}

    eventStr += " ";
    char tmp[256];
    snprintf( tmp, 256, "0x%016LX", (unsigned long long)eventID.fNr );
    eventStr += kAliEventTypeStrings[eventID.fType];
    eventStr += "/";
    eventStr += tmp;

    unsigned ndx=0;
    if ( sedd )
	{
	eventStr += " Event Status Flags: ";
	snprintf( tmp, 256, "0x%016LX", (unsigned long long)sedd->fStatusFlags );
	eventStr += tmp;
	eventStr += " Event Birth: ";
	time_str = MLUCString::TimeStamp2String( sedd->fEventBirth_s, sedd->fEventBirth_us );
	eventStr += time_str;
	eventStr += " Oldest Event Birth: ";
	time_str = MLUCString::TimeStamp2String( sedd->fOldestEventBirth_s );
	eventStr += time_str;
	eventStr += " Event Data Type: ";
	tmp[0] = '\'';
	for (  ndx=0; ndx<8; ++ndx )
	    {
	    if ( sedd->fDataType.fDescr[7-ndx]=='\0' )
		break;
	    tmp[ndx] = sedd->fDataType.fDescr[7-ndx];
	    }
	tmp[ndx++] = '\'';
	while ( ndx < 10 )
	    tmp[ndx++] = ' ';
	tmp[ndx++] = '\0';
	eventStr += tmp;
	}

#if 1
    if ( fEventTriggerFormatFunc && ets )
	{
	eventStr += " Trigger data: ";
	MLUCString triggerStr = fEventTriggerFormatFunc( ets );
	eventStr += triggerStr;
	}
#else
    if ( fAliceHLT && ets && ets->fHeader.fLength==sizeof(AliHLTHLTEventTriggerData) )
	{
	eventStr += " Trigger data: Status flag: ";
	snprintf( tmp, 256, "0x%016LX", ((AliHLTHLTEventTriggerData*)ets)->fStatusFlags );
	eventStr += tmp;
	eventStr += " CDH: Version: ";
	snprintf( tmp, 256, "% 3u", (unsigned)AliHLTGetDDLHeaderVersion( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	eventStr += tmp;
	eventStr += " Size: ";
	snprintf( tmp, 256, "% 10u", (unsigned)AliHLTGetDDLHeaderBlockLength( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	eventStr += tmp;
	eventStr += " L1 Trigger Type: ";
	snprintf( tmp, 256, "0x%02lX", (unsigned)AliHLTGetDDLHeaderL1TriggerType( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	eventStr += tmp;
	eventStr += " Event ID1: ";
	snprintf( tmp, 256, "0x%03lX", (unsigned)AliHLTGetDDLHeaderEventID1( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	eventStr += tmp;
	eventStr += " Event ID2: ";
	snprintf( tmp, 256, "0x%06lX", (unsigned)AliHLTGetDDLHeaderEventID2( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	eventStr += tmp;
	eventStr += " Participating Subdetectors: ";
	snprintf( tmp, 256, "0x%06lX", (unsigned)AliHLTGetDDLHeaderPartSubDet( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	eventStr += tmp;
	eventStr += " Block Attributes: ";
	snprintf( tmp, 256, "0x%02lX", (unsigned)AliHLTGetDDLHeaderBlockAttr( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	eventStr += tmp;
	eventStr += " Mini Event ID: ";
	snprintf( tmp, 256, "0x%03lX", (unsigned)AliHLTGetDDLHeaderMiniEventID( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	eventStr += tmp;
	eventStr += " Status & Error: ";
	snprintf( tmp, 256, "0x%04lX", (unsigned)AliHLTGetDDLHeaderStatusError( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	eventStr += tmp;
	eventStr += " Trigger Classes: ";
	snprintf( tmp, 256, "0x%013LX", (unsigned long long)AliHLTGetDDLHeaderTriggerClassesHigh50( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	eventStr += tmp;
	snprintf( tmp, 256, "0x%013LX", (unsigned long long)AliHLTGetDDLHeaderTriggerClassesLow50( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	eventStr += tmp;
	eventStr += " Region of Interest: ";
	snprintf( tmp, 256, "0x%09LX", (unsigned long long)AliHLTGetDDLHeaderROI( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	eventStr += tmp;
	}
#endif


    if ( sedd )
	{
	eventStr += " ";
	snprintf( tmp, 256, "%lu", (unsigned long)(sedd->fDataBlockCount) );
	eventStr += tmp;
	eventStr += " blocks";
	for ( unsigned blk=0; blk<sedd->fDataBlockCount; ++blk )
	    {
	    eventStr += " block ";
	    snprintf( tmp, 256, "%u", blk );
	    eventStr += tmp;
	    eventStr += ": Status Flags: ";
	    snprintf( tmp, 256, "0x%016LX", (unsigned long long)sedd->fDataBlocks[blk].fStatusFlags );
	    eventStr += tmp;
	    eventStr += " Data Type: ";
	    ndx=0;
	    tmp[0] = '\'';
	    for (  ndx=0; ndx<8; ++ndx )
		{
		if ( sedd->fDataBlocks[blk].fDataType.fDescr[7-ndx]=='\0' )
		    break;
		tmp[ndx] = sedd->fDataBlocks[blk].fDataType.fDescr[7-ndx];
		}
	    tmp[ndx++] = '\'';
	    while ( ndx < 10 )
		tmp[ndx++] = ' ';
	    tmp[ndx++] = '\0';
	    eventStr += tmp;
	    eventStr += " Data Origin: ";
	    ndx=0;
	    tmp[0] = '\'';
	    for (  ndx=0; ndx<4; ++ndx )
		{
		if ( sedd->fDataBlocks[blk].fDataOrigin.fDescr[3-ndx]=='\0' )
		    break;
		tmp[ndx] = sedd->fDataBlocks[blk].fDataOrigin.fDescr[3-ndx];
		}
	    tmp[ndx++] = '\'';
	    while ( ndx < 6 )
		tmp[ndx++] = ' ';
	    tmp[ndx++] = '\0';
	    eventStr += tmp;
	    eventStr += " Data Spec.: ";
	    snprintf( tmp, 256, "0x%08X", sedd->fDataBlocks[blk].fDataSpecification );
	    eventStr += tmp;
	    eventStr += "/";
	    snprintf( tmp, 256, "%u", sedd->fDataBlocks[blk].fDataSpecification );
	    eventStr += tmp;
	    }
	}
    eventStr += "\n";
    int written = 0;
    const char* writeData = eventStr.c_str();
    int toWrite = strlen(writeData);
    while ( written<toWrite )
	{
	int ret = write( fFileHandle, writeData+written, toWrite-written );
	if ( ret<0 )
	    return false;
	written += ret;
	}
    return true;
    }
	


/*
***************************************************************************
**
** $Author: artur $ - Initial Version by Timm Morten Steinbeck
**
** $Id:  $ 
**
***************************************************************************
*/
