/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTShmManager.hpp"
#include "AliHLTLog.hpp"

AliHLTShmManager::AliHLTShmManager( AliUInt32_t maxNoUseCount )
    {
    fMaxShmNoUseCount = maxNoUseCount;
    pthread_mutex_init( &fShmsMutex, NULL );
    }

AliHLTShmManager::~AliHLTShmManager()
    {
    while ( fShms.size()>0 )
	{
	UnlockShm( fShms.begin()->fShmID );
	fShms.erase( fShms.begin() );
	}
    pthread_mutex_destroy( &fShmsMutex );
    }

AliUInt8_t* AliHLTShmManager::GetShm( AliHLTShmID const& shmKey )
    {
    if ( shmKey.fShmType==kSysVShmPrivateType &&
	 shmKey.fKey.fID==~(AliUInt64_t)0 )
	return NULL;
    AliHLTShmID tmpShmKey = shmKey;
    return GetShm( tmpShmKey );
    }


AliUInt8_t* AliHLTShmManager::GetShm( AliHLTShmID& shmKey )
    {
    int ret;
    void* p = NULL;
    bool found = false;
    vector<TShmData>::iterator iter, end;
    ret = pthread_mutex_lock( &fShmsMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTShmManager::GetShm", "Mutex lock error" )
	    << "Unable to lock shm mutex: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << "). Continuing..." << ENDLOG;
	}

    iter = fShms.begin();
    end = fShms.end();
    
    while ( iter != end )
	{
	if ( iter->fShmKey.fShmType == shmKey.fShmType &&
	     iter->fShmKey.fKey.fID == shmKey.fKey.fID )
	    {
	    found = true;
	    iter->fNoUseCount = 0;
	    iter->fRefCount++;
	    p = iter->fPtr;
	    }
	else
	    iter->fNoUseCount++;
	iter++;
	}

    if ( !found )
	{
	TShmData shm;
	if ( LockShm( shmKey, shm.fPtr, shm.fShmID ) )
	    {
	    shm.fShmKey.fShmType = shmKey.fShmType;
	    shm.fShmKey.fKey.fID = shmKey.fKey.fID;
	    shm.fRefCount = 1;
	    shm.fNoUseCount = 0;
	    fShms.insert( fShms.end(), shm );
	    p = shm.fPtr;
	    }
	else
	    {
	    LOG( AliHLTLog::kError, "AliHLTShmManager::GeShm", "Unable to get shm buffer" )
		<< "Unable to obtain shm buffer with shm key 0x" << AliHLTLog::kHex 
		<< shmKey.fKey.fID << " (" << AliHLTLog::kDec << shmKey.fKey.fID << ")." << ENDLOG;
	    }
	}

    if ( fMaxShmNoUseCount > 0 )
	{
	do
	    {
	    found = false;
	    while ( iter != end )
		{
		if ( iter->fRefCount <=0 && iter->fNoUseCount >= fMaxShmNoUseCount )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTShmManager::GeShm", "Releasin buffer" )
			<< "Releasing buffer with shm key 0x" << AliHLTLog::kHex 
			<< shmKey.fKey.fID << " (" << AliHLTLog::kDec << shmKey.fKey.fID << ") after " 
			<< iter->fNoUseCount << " unused calls." << ENDLOG;
		    UnlockShm( iter->fShmID );
		    fShms.erase( iter );
		    found = true;
		    break;
		    }
		iter++;
		}
	    }
	while ( found );
	}

    ret = pthread_mutex_unlock( &fShmsMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTShmManager::GetShm", "Mutex unlock error" )
	    << "Unable to unlock shm mutex: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << "). Continuing..." << ENDLOG;
	}
    return (AliUInt8_t*)p;
    }

void AliHLTShmManager::ReleaseShm( AliHLTShmID shmKey )
    {
    int ret;
    vector<TShmData>::iterator iter, end;
    ret = pthread_mutex_lock( &fShmsMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTShmManager::ReleaseShm", "Mutex lock error" )
	    << "Unable to lock shm mutex: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << "). Continuing..." << ENDLOG;
	}

    iter = fShms.begin();
    end = fShms.end();
    
    while ( iter != end )
	{
	if ( iter->fShmKey.fShmType == shmKey.fShmType &&
	     iter->fShmKey.fKey.fID == shmKey.fKey.fID )
	    {
	    iter->fRefCount--;
	    if ( fMaxShmNoUseCount>0 && iter->fRefCount <=0 && iter->fNoUseCount >= fMaxShmNoUseCount )
		{
		UnlockShm( iter->fShmID );
		fShms.erase( iter );
		}
	    break;
	    }
	iter++;
	}

    if ( iter==end )
	{
	LOG( AliHLTLog::kWarning, "AliHLTShmManager::ReleaseShm", "No shm area to release" )
	    << "Shm buffer with key 0x" << AliHLTLog::kHex << shmKey.fKey.fID << " (" << AliHLTLog::kDec 
	    << shmKey.fKey.fID << ") to be released could not be found." << ENDLOG;
	}

    ret = pthread_mutex_unlock( &fShmsMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTShmManager::ReleaseShm", "Mutex unlock error" )
	    << "Unable to unlock shm mutex: " << strerror(ret) << " (" << AliHLTLog::kDec << ret
	    << "). Continuing..." << ENDLOG;
	}
    }

bool AliHLTShmManager::LockShm( AliHLTShmID& shmKey, void*& ptr, AliUInt32_t& shmID )
    {
    unsigned long size=0;
    shmID = fShm.GetShm( shmKey, size );
    if ( shmID == (AliUInt32_t)-1 )
	{
	LOG( AliHLTLog::kError, "AliHLTShmManager::LockShm", "Getting new shared memory" )
	    << "Error getting shared memory region " << shmKey.fKey.fID << "." << ENDLOG;
	return false;
	}
    ptr = fShm.LockShm( shmID );
    if ( !ptr )
	{
	LOG( AliHLTLog::kError, "AliHLTShmManager::LockShm", "Locking shared memory" )
	    << "Error locking shared memory region " << shmKey.fKey.fID << " (with ID "
	    << shmID << ")." << ENDLOG;
	fShm.ReleaseShm( shmID );
	return false;
	}
    return true;
    }

void AliHLTShmManager::UnlockShm( AliUInt32_t shmID )
    {
    fShm.UnlockShm( shmID );
    fShm.ReleaseShm( shmID );
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
