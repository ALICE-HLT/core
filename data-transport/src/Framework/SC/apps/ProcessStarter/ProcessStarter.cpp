/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCInterface.hpp"
#include "AliHLTSCCommandProcessor.hpp"
#include "AliHLTSCCommands.hpp"
#include "AliHLTSCStates.hpp"
#include "AliHLTSCTypes.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTSCStatusHeader.hpp"
#include "AliHLTSCProcessStatus.hpp"
#include "BCLURLAddressDecoder.hpp"
#include "BCLAddressLogger.hpp"
#include "BCLErrorLogCallback.hpp"
#include "MLUCString.hpp"
#include <stdlib.h>
#include <errno.h>


struct AliHLTProcessStarterStatus
    {
	AliHLTSCStatusHeaderStruct                  fHeader;
	AliHLTSCProcessStatusStruct                 fProcessStatus;

	AliHLTProcessStarterStatus()
		{
		AliHLTSCStatusHeader header;
		AliHLTSCProcessStatus process;
		fHeader = *(header.GetData());
		fHeader.fStatusType = ALIHLTPROCESSSTARTERSTATUSTYPE;
		fProcessStatus = *(process.GetData());
		fHeader.fLength += fProcessStatus.fLength;
		fHeader.fLength = sizeof(AliHLTProcessStarterStatus);
		fHeader.fStatusStructCnt = 1;
		fHeader.fFirstStructOffset = (AliUInt32_t)(&fProcessStatus) - (AliUInt32_t)(&fHeader);
		}

    };


class ProcessStarterCommandHandler: public AliHLTSCCommandProcessor
    {
    public:

	ProcessStarterCommandHandler( bool* quitValue )
		{
		fQuitValue = quitValue;
		fStatus = NULL;
		}

	virtual ~ProcessStarterCommandHandler()
		{
		}

	virtual void ProcessCmd( AliHLTSCCommandStruct* cmd );

	bool ProcessLine( const char* id, const char* line );

	void SetStatus( AliHLTProcessStarterStatus* status )
		{
		fStatus = status;
		}

    protected:

	bool ReadLine( istream& istr, MLUCString& line );

	bool* fQuitValue;

	MLUCString fLastFileName;
	MLUCString fLastID;

	AliHLTProcessStarterStatus* fStatus;

    private:
    };


void ProcessStarterCommandHandler::ProcessCmd( AliHLTSCCommandStruct* cmd )
    {
    AliHLTSCCommand cmdC( cmd );
    if ( cmdC.GetData()->fCmd == kAliHLTSCQuitCmd )
	{
	if ( fQuitValue )
	    *fQuitValue = true;
	return;
	}

    if ( cmdC.GetData()->fCmd != kAliHLTSCExecuteCommandCmd )
	{
	LOG( AliHLTLog::kError, "ProcessStarterCommandHandler::ProcessCmd", "Unknown command" )
	    << "Unknown command 0x" << AliHLTLog::kHex << cmdC.GetData()->fCmd << " (" << AliHLTLog::kDec
	    << cmdC.GetData()->fCmd << ")." << ENDLOG;
	return;
	}
    
    struct timeval now;
    if ( fStatus )
	{
	fStatus->fProcessStatus.fState = kAliHLTSCProcessStarterBusyState;
	gettimeofday( &now, NULL );
	fStatus->fProcessStatus.fLastUpdateTime_s = now.tv_sec;
	fStatus->fProcessStatus.fLastUpdateTime_us = now.tv_usec;
	}
    char* filename;
    char* id;
    filename = (char*)cmd->fData;
    id = filename+strlen(filename)+1;

    ifstream file;
    
    file.open( filename );
    if ( !file.good() )
	{
	LOG( AliHLTLog::kError, "ProcessStarterCommandHandler::ProcessCmd", "Error Opening File" )
	    << "Error opening file " << filename << " received from command." << ENDLOG;
	file.close();
	if ( fStatus )
	    {
	    fStatus->fProcessStatus.fState = kAliHLTSCProcessStarterIdleState;
	    gettimeofday( &now, NULL );
	    fStatus->fProcessStatus.fLastUpdateTime_s = now.tv_sec;
	    fStatus->fProcessStatus.fLastUpdateTime_us = now.tv_usec;
	    }
	return;
	}

    if ( fLastFileName == filename && fLastID == id )
	{
	LOG( AliHLTLog::kInformational, "ProcessStarterCommandHandler::ProcessCmd", "File/ID already active" )
	    << "File '" << filename << "' and id '" << id << "' already activated on last command." << ENDLOG;
	file.close();
	if ( fStatus )
	    {
	    fStatus->fProcessStatus.fState = kAliHLTSCProcessStarterIdleState;
	    gettimeofday( &now, NULL );
	    fStatus->fProcessStatus.fLastUpdateTime_s = now.tv_sec;
	    fStatus->fProcessStatus.fLastUpdateTime_us = now.tv_usec;
	    }
	return;
	}

    fLastFileName = filename;
    fLastID = id;
    MLUCString line;
    bool executed = false;

    while ( ReadLine( file, line ) )
	{
	if ( ProcessLine( id, line.c_str() ) )
	    {
	    executed = true;
	    LOG( AliHLTLog::kInformational, "ProcessStarterCommandHandler::ProcessCmd", "Command Executed" )
		<< "Executed command '" << line.c_str() << "' from file '" << filename << "'." << ENDLOG;
	    }
	printf( "%s:%d\n", __FILE__, __LINE__ );
	}
    if ( !executed )
	{
	LOG( AliHLTLog::kDebug, "ProcessStarterCommandHandler::ProcessCmd", "No Command Executed" )
	    << "No command executed from file '" << filename << "'." << ENDLOG;
	}
    if ( fStatus )
	{
	fStatus->fProcessStatus.fState = kAliHLTSCProcessStarterIdleState;
	gettimeofday( &now, NULL );
	fStatus->fProcessStatus.fLastUpdateTime_s = now.tv_sec;
	fStatus->fProcessStatus.fLastUpdateTime_us = now.tv_usec;
	}
    }

bool ProcessStarterCommandHandler::ProcessLine( const char* id, const char* line )
    {
    MLUCString cmpid( id );
    cmpid += ":";
    bool found = false;
    int pre_len = 0;
    int ret;

    if ( !strncmp( line, cmpid.c_str(), strlen(cmpid.c_str()) ) )
	{
	// Line is for our id.
	found = true;
	pre_len = strlen(cmpid.c_str());
	}
    else if ( !strncmp( line, "*:", 2 ) )
	{
	// Line is for all ids.
	found = true;
	pre_len = 2;
	}

    if ( found )
	{
	// Line is either for our id ("id") or is for all ids ("*")
	MLUCString cmd;
	cmd = line+pre_len;
	cmd += " &";
	LOG( AliHLTLog::kDebug, "ProcessStarterCommandHandler::ProcessLine", "System Call" )
	    << "Executing system call: '" << cmd.c_str() << "' for id '" << id << "'." << ENDLOG;
	ret = system( cmd.c_str() );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "ProcessStarterCommandHandler::ProcessLine", "Error during system call" )
		<< "Error calling system: " << strerror(ret) << "/" << strerror(errno) << AliHLTLog::kDec
		<< " (" << ret << "/" << errno << ")." << ENDLOG;
	    }
	}
    return found;
    }


bool ProcessStarterCommandHandler::ReadLine( istream& istr, MLUCString& line )
    {
    char tmp[ 256 ];
    char peeked;
    line = "";
    if ( !istr.good() )
	return false;
    do
	{
	istr.get( tmp, 256, '\n' );
	line += (char*)tmp;
	if ( !istr.good() )
	    return true;
	if ( istr.eof() )
	    {
	    LOG( AliHLTLog::kDebug, "BridgeConfig::ReadLine", "Line Read" )
		<< "Line '" << line.c_str() << "' read." << ENDLOG;
	    return true;
	    }
	peeked = istr.peek();
	if ( peeked == '\n' )
	    {
	    peeked = istr.get();
	    LOG( AliHLTLog::kDebug, "BridgeConfig::ReadLine", "Line Read" )
		<< "Line '" << line.c_str() << "' read." << ENDLOG;
	    return true;
	    }
	}
    while ( true );
    }



int main( int argc, char** argv )
    {
    bool quitValue = 0;
    int ret, i=0;
    gLogLevel = AliHLTLog::kAll;
    BCLAddressLogger::Init();
    AliHLTFileLogServer fileLog( "ProcessStarter-", ".log", 500000, 100 );
    AliHLTFilteredStdoutLogServer stdOut;
    gLog.AddServer( fileLog );
    gLog.AddServer( stdOut );

    char* portAddress = NULL;
    uint16 portNr;
    char* errorStr = NULL;
    char* cpErr;

    char *usage1 = "Usage: ", *usage2 = " -port <msg-communication-TCP-port> (-V <verbosity>)";

    i = 1;
    while ( i<argc )
	{
	if ( !strcmp( argv[i], "-port" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing msg communication address port.";
		break;
		}
	    portAddress = argv[i+1];
	    portNr = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting msg communication address portspecifier:";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing verbosity specifier.";
		break;
		}
	    gLogLevel = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting verbosity specifier:";
		break;
		}
	    i += 2;
	    continue;
	    }
	errorStr = "Unknown option";
	break;
	}
    if ( !errorStr && !portAddress )
	errorStr = "Must specify -port option with msg communication address port.";
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "ProcessStarter", "Usage" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	LOG( AliHLTLog::kError, "ProcessStarter", "Usage" )
	    << "Error: " << errorStr << ENDLOG;
	if ( i<argc )
	    {
	    LOG( AliHLTLog::kError, "ProcessStarter", "Usage" )
		<< "Offending parameter: " << argv[i] << ENDLOG;
	    }
	return 0;
	}
    
    MLUCString addressURL;
    addressURL = "tcpmsg://localhost:";
    addressURL += portAddress;
    AliHLTSCInterface scInterface;
    AliHLTProcessStarterStatus status;
    BCLErrorLogCallback errorCallback;

    ProcessStarterCommandHandler commandProcessor( &quitValue );
    scInterface.AddCommandProcessor( &commandProcessor );
    scInterface.SetStatusData( &(status.fHeader) );
    commandProcessor.SetStatus( &status );
    status.fProcessStatus.fState = kAliHLTSCProcessStarterIdleState;
    ret = scInterface.Bind( addressURL.c_str(), &errorCallback );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "ProcessStarter", "Bind Error" )
	    << "Bind error on address URL '" << addressURL.c_str()
	    << "': " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    else
	{
	i = 0;
	ret = scInterface.Start();
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "ProcessStarter", "Start Error" )
		<< "Error starting interface object: " 
		<< strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    }
	}
    if ( !ret )
	{

	LOG( AliHLTLog::kDebug, "ProcessStarter", "Started" )
	    << "Started" << ENDLOG;

	while ( !quitValue )
	    {
	    usleep( 1000000 );
	    }

	LOG( AliHLTLog::kInformational, "ProcessStarter", "Quitting" )
	    << "Quitting now..:" << ENDLOG;

	}

    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
