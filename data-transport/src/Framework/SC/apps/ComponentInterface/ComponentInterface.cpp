/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCComponentInterface.hpp"
#include "AliHLTSCController.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "BCLAbstrAddress.hpp"
#include "BCLURLAddressDecoder.hpp"
#include "BCLErrorLogCallback.hpp"
#include "AliHLTStatus.hpp"
#include <cstdlib>


int main( int argc, char** argv )
    {
    gLogLevel = AliHLTLog::kAll;
    //AliHLTFilteredStdoutLogServer stdOut;
    AliHLTStderrLogServer stdOut;
    gLog.AddServer( stdOut );

    int ret;
    char* cpErr;

    int readStatus = 0;
    int readStatusData = 0;
    int sendCommand = 0;
    int listCommands = 0;
    char* ownURL = NULL;
    char* clientURL = NULL;
    char* command = NULL;
    char* commandParameter = NULL;
    BCLAbstrAddressStruct* tmpAddress = NULL;
    BCLAbstrAddressStruct* clientAddress = NULL;
    AliUInt32_t cmd = kAliHLTSCSFirstProcessControlCmd;
    MLUCString tmpStr;

    int i = 1;
    int errorArg = -1;
    const char* errorStr = NULL;

    const char *usage1 = "Usage: ", *usage2 = " -address <msg-communication-address-URL> (-status <client-address-URL>) (-statusdata <client-address-URL>) (-command <client-address-URL> <command>) (-paramcommand <client-address-URL> <command> <command-parameters>) (-listcommands)";
    
    while ( i < argc )
	{
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing verbosity level specifier.";
		errorArg = i;
		break;
		}
	    gLogLevel = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting verbosity level specifier..";
		errorArg = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-address" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing local address argument.";
		errorArg = i;
		break;
		}
	    BCLCommunication* tmpCom;
	    bool isBlob;
	    ret = BCLDecodeLocalURL( argv[i+1], tmpCom, tmpAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid local address URL.";
		errorArg = i+1;
		break;
		}
	    ownURL = argv[i+1];
	    BCLFreeAddress( tmpCom, tmpAddress );
	    if ( isBlob )
		{
		errorStr = "Invalid local address URL - Only message communication objects allowed.";
		errorArg = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-status" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing client address argument.";
		errorArg = i;
		break;
		}
	    readStatus = 1;
	    ret = BCLDecodeRemoteURL( argv[i+1], clientAddress );
	    if ( ret )
		{
		errorStr = "Invalid client address URL.";
		errorArg = i+1;
		break;
		}
	    clientURL = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-statusdata" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing client address argument.";
		errorArg = i;
		break;
		}
	    readStatusData = 1;
	    ret = BCLDecodeRemoteURL( argv[i+1], clientAddress );
	    if ( ret )
		{
		errorStr = "Invalid client address URL.";
		errorArg = i+1;
		break;
		}
	    clientURL = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-command" ) || !strcmp( argv[i], "-paramcommand" ) )
	    {
	    bool param=false;
	    int argCount=2;
	    if ( !strcmp( argv[i], "-paramcommand" ) )
		{
		param = true;
		++argCount;
		}
	    if ( argc <= i+argCount )
		{
		if ( param )
		    errorStr = "Missing client address, command, or command parameter argument.";
		else
		    errorStr = "Missing client address or command argument.";
		errorArg = i;
		break;
		}
	    sendCommand = 1;
	    ret = BCLDecodeRemoteURL( argv[i+1], clientAddress );
	    if ( ret )
		{
		errorStr = "Invalid client address URL.";
		errorArg = i+1;
		break;
		}
	    for ( cmd = kAliHLTSCSFirstProcessControlCmd; cmd < kAliHLTSCSLastProcessControlCmd; cmd++ )
		{
		AliHLTSCComponentInterface::GetCommandName( cmd, tmpStr );
		if ( tmpStr == argv[i+2] )
		    break;
		}
	    if ( cmd == kAliHLTSCSLastProcessControlCmd )
		{
		errorStr = "Unknown command.";
		errorArg = i+2;
		break;
		}
	    clientURL = argv[i+1];
	    command = argv[i+2];
	    if ( param )
		commandParameter = argv[i+3];
	    i += argCount+1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-listcommands" ) )
	    {
	    listCommands = 1;
	    i++;
	    continue;
	    }
	errorStr = "Unknown option";
	errorArg = i;
	break;
	}
    if ( !errorStr )
	{
	if ( readStatus+readStatusData+sendCommand+listCommands!=1 )
	    errorStr = "Must specify one of the -status, -statusdata, -command, or -listcommands options.";
	else if ( !ownURL && !listCommands )
	    errorStr = "Must specify the -address option for -status or -command.";
	}
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "ComponentInterface", "Usage" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	LOG( AliHLTLog::kError, "ComponentInterface", "Usage" )
	    << "Error: " << errorStr << ENDLOG;
	if ( errorArg != -1 )
	    {
	    LOG( AliHLTLog::kError, "ComponentInterface", "Usage" )
		<< "Offending parameter: " << argv[i] << ENDLOG;
	    }
	return -1;
	}

//     if ( !listCommands )
// 	gLog.DelServer( stdOut );

    AliHLTSCController controller;

    //BCLErrorLogCallback errorCallback;

    controller.SetReplyTimeout( 2000 ); // Reply timeout of 2s.

    if ( ownURL )
	{
	ret = controller.Bind( ownURL );
	//ret = controller.Bind( ownURL, &errorCallback );
	if ( ret )
	    {
	    BCLFreeAddress( clientAddress );
	    return ret;
	    }
	
	//controller.GetMsgCom()->AddCallback( &errorCallback );
	
	controller.Start();
	}

    if ( readStatus || readStatusData )
	{
	MLUCString typeName, statusName;
	AliHLTSCStatusHeaderStruct* statusHeader;
	ret = controller.GetStatus( clientAddress, statusHeader );
	if ( ret )
	    {
	    controller.Stop();
	    controller.Unbind();
	    BCLFreeAddress( clientAddress );
	    return ret;
	    }
	AliHLTSCComponentInterface::GetTypeName( statusHeader, typeName );
	AliHLTSCComponentInterface::GetStatusName( statusHeader, statusName );
	printf( "%s:%s\n", typeName.c_str(), statusName.c_str() );
	if ( readStatusData )
	    {
	    struct timeval tv;
	    AliHLTSCComponentInterface::GetUpdateTime( statusHeader, tv );
	    struct tm* t;
	    t = localtime( &tv.tv_sec );
	    t->tm_mon++;
	    t->tm_year += 1900;
	    printf( "Last update: %08u.%06u.%06u\n", 
		    (unsigned)(t->tm_year*10000+t->tm_mon*100+t->tm_mday), 
		    (unsigned)(t->tm_hour*10000+t->tm_min*100+t->tm_sec),
		    (unsigned)tv.tv_usec );
	    AliHLTSCProcessStatusStruct procStat;
	    AliHLTSCComponentInterface::GetProcessStatus( statusHeader, procStat );
	    AliHLTSCHLTStatusStruct hltStat;
	    AliHLTSCComponentInterface::GetHLTStatus( statusHeader, hltStat );
	    printf( "Total Received Event Count: %Lu\n", (unsigned long long)hltStat.fReceivedEventCount );
	    printf( "Current Received Event Count: %Lu\n", (unsigned long long)hltStat.fCurrentReceivedEventCount );
	    printf( "Total Processed Event Count: %Lu\n", (unsigned long long)hltStat.fProcessedEventCount );
	    printf( "Current Processed Event Count: %Lu\n", (unsigned long long)hltStat.fCurrentProcessedEventCount );
	    printf( "Total Announced Event Count: %Lu\n", (unsigned long long)hltStat.fAnnouncedEventCount );
	    printf( "Current Announced Event Count: %Lu\n", (unsigned long long)hltStat.fCurrentAnnouncedEventCount );
	    printf( "Current Receive Rate: %f\n", hltStat.fCurrentReceiveRate );
	    printf( "Overall Average Receive Rate: %f\n", hltStat.fAvgReceiveRate );
	    printf( "Average Receive Rate since last Resume: %f\n", hltStat.fResumedAvgReceiveRate );
	    printf( "Current Process Rate: %f\n", hltStat.fCurrentProcessRate );
	    printf( "Overall Average Process Rate: %f\n", hltStat.fAvgProcessRate );
	    printf( "Average Process Rate since last Resume: %f\n", hltStat.fResumedAvgProcessRate );
	    printf( "Current Announce Rate: %f\n", hltStat.fCurrentAnnounceRate );
	    printf( "Overall Average Announce Rate: %f\n", hltStat.fAvgAnnounceRate );
	    printf( "Average Announce Rate since last Resume: %f\n", hltStat.fResumedAvgAnnounceRate );
	    printf( "Pending Input Event Count: %Lu\n", (unsigned long long)hltStat.fPendingInputEventCount );
	    printf( "Pending Output Event Count: %Lu\n", (unsigned long long)hltStat.fPendingOutputEventCount );
	    printf( "Free Output Buffer Size: %Lu\n", (unsigned long long)hltStat.fFreeOutputBuffer );
	    printf( "Total Output Buffer Size: %Lu\n", (unsigned long long)hltStat.fTotalOutputBuffer );
	    printf( "Current Data Event ID: 0x%016LX (%Lu)\n", (unsigned long long)hltStat.fCurrentDataEvent, 
		    (unsigned long long)hltStat.fCurrentDataEvent );
	    printf( "Total Processed Input Data Size: %Lu\n", (unsigned long long)hltStat.fTotalProcessedInputDataSize );
	    printf( "Total Processed Output Data Size: %Lu\n", (unsigned long long)hltStat.fTotalProcessedOutputDataSize );
	    printf( "Current Input Data Process Rate: %f\n", hltStat.fCurrentProcessedInputDataRate );
	    printf( "Overall Average Input Data Process Rate: %f\n", hltStat.fAvgProcessedInputDataRate );
	    printf( "Average Input Data Process Rate: %f\n", hltStat.fResumedProcessedInputDataRate );
	    printf( "Current Output Data Process Rate: %f\n", hltStat.fCurrentProcessedOutputDataRate );
	    printf( "Overall Average Output Data Process Rate: %f\n", hltStat.fAvgProcessedOutputDataRate );
	    printf( "Average Output Data Process Rate: %f\n", hltStat.fResumedProcessedOutputDataRate );
	    printf( "Total Input Data Size / Output Data Size Ratio: %f\n", hltStat.fTotalInput2OutputDataSizeRatio );
	    printf( "Current Input Data Size / Output Data Size Ratio: %f\n", hltStat.fCurrentInput2OutputDataSizeRatio );

	    time_t tmpTime = hltStat.fCurrentEventStart/1000000;
	    t = localtime( &tmpTime );
	    t->tm_mon++;
	    t->tm_year += 1900;
	    printf( "Current Event Start: %08u.%06u.%06u\n", 
		    (unsigned)(t->tm_year*10000+t->tm_mon*100+t->tm_mday), 
		    (unsigned)(t->tm_hour*10000+t->tm_min*100+t->tm_sec),
		    (unsigned)(hltStat.fCurrentEventStart%1000000) );
	    printf( "Last Data Event ID: 0x%016LX (%Lu)\n", (unsigned long long)hltStat.fLastDataEvent, (unsigned long long)hltStat.fLastDataEvent );
	    tmpTime = hltStat.fLastEventStart/1000000;
	    t = localtime( &tmpTime );
	    t->tm_mon++;
	    t->tm_year += 1900;
	    printf( "Last Event Start: %08u.%06u.%06u\n", 
		    (unsigned)(t->tm_year*10000+t->tm_mon*100+t->tm_mday), 
		    (unsigned)(t->tm_hour*10000+t->tm_min*100+t->tm_sec),
		    (unsigned)(hltStat.fLastEventStart%1000000) );
	    tmpTime = hltStat.fLastEventEnd/1000000;
	    t = localtime( &tmpTime );
	    t->tm_mon++;
	    t->tm_year += 1900;
	    printf( "Last Event End: %08u.%06u.%06u\n", 
		    (unsigned)(t->tm_year*10000+t->tm_mon*100+t->tm_mday), 
		    (unsigned)(t->tm_hour*10000+t->tm_min*100+t->tm_sec),
		    (unsigned)(hltStat.fLastEventEnd%1000000) );
	    AliUInt64_t tdiff = hltStat.fLastEventEnd-hltStat.fLastEventStart;
	    printf( "Last event processing time: %lu.%06lu s\n",
		    (unsigned long)(tdiff / 1000000), (unsigned long)(tdiff % 1000000) );
	    }
	controller.FreeStatus( statusHeader );
	}


    if ( sendCommand )
	{
	AliHLTSCCommand cmdData;
	cmdData.GetData()->fCmd = cmd;
	cmdData.GetData()->fParam0 = 0;
	cmdData.GetData()->fParam1 = 0;
	cmdData.GetData()->fDataCnt = 0;
	AliHLTSCCommandStruct *cmdDataStructPtr = cmdData.GetData();
	if ( commandParameter )
	    {
	    unsigned long strLen = strlen(commandParameter);
	    unsigned long paramLen = strLen+1;
	    if ( paramLen % sizeof(cmdData.GetData()->fData[0]) )
		paramLen += sizeof(cmdData.GetData()->fData[0]) - ( paramLen % sizeof(cmdData.GetData()->fData[0]) );
	    cmdDataStructPtr = (AliHLTSCCommandStruct *)new uint8[ cmdData.GetData()->fLength+paramLen ];
	    if ( !cmdDataStructPtr )
		{
		controller.Stop();
		controller.Unbind();
		BCLFreeAddress( clientAddress );
		return ENOMEM;
		}
	    memcpy( cmdDataStructPtr, cmdData.GetData(), cmdData.GetData()->fLength );
	    strcpy( (char*)(cmdDataStructPtr->fData), commandParameter );
	    for ( unsigned long nn=strLen; nn<paramLen; nn++ )
		((char*)(cmdDataStructPtr->fData))[nn]=0;
	    cmdDataStructPtr->fLength += strlen(commandParameter)+1;
	    cmdDataStructPtr->fDataCnt = 3;
	    printf( "%s\n", (char*)(cmdDataStructPtr->fData) );
	    }
	ret = controller.SendCommand( clientAddress, cmdDataStructPtr );
	if ( ret )
	    {
	    controller.Stop();
	    controller.Unbind();
	    BCLFreeAddress( clientAddress );
	    return ret;
	    }
	}

    if ( listCommands )
	{
	LOG( AliHLTLog::kInformational, "ComponentInterface", "Usage" )
	    << "Commands: " << ENDLOG;
	for ( cmd = kAliHLTSCSFirstProcessControlCmd; cmd < kAliHLTSCSLastProcessControlCmd; cmd++ )
	    {
	    AliHLTSCComponentInterface::GetCommandName( cmd, tmpStr );
	    LOG( AliHLTLog::kInformational, "ComponentInterface", "Usage" )
		<< "          " << tmpStr.c_str() << ENDLOG;
	    }
	}

    if ( ownURL )
	{
	controller.Stop();
	controller.Unbind();
	BCLFreeAddress( clientAddress );
	}

    return 0;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
