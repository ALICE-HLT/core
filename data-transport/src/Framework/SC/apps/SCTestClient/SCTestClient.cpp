/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCCommandProcessor.hpp"
#include "AliHLTSCInterface.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTStatus.hpp"
#include "Commands.h"
#include "BCLCommunication.hpp"
#include "BCLAbstrAddress.hpp"
#include "BCLURLAddressDecoder.hpp"
#include "BCLAddressLogger.hpp"
#include "BCLErrorLogCallback.hpp"


class TestCommandProcessor: public AliHLTSCCommandProcessor
    {
    public:

	TestCommandProcessor( AliUInt32_t* quitValue, AliHLTSCInterface* interface )
		{
		fQuitValue = quitValue;
		fIgnoreQuit = false;
		fInterface = interface;
		}

	virtual void ProcessCmd( AliHLTSCCommandStruct* cmd );

    protected:
	
	AliUInt32_t* fQuitValue;
	bool fIgnoreQuit;
	AliHLTSCInterface* fInterface;

    };


void TestCommandProcessor::ProcessCmd( AliHLTSCCommandStruct* cmd )
    {
    AliHLTSCCommand cmdClass( cmd );
    switch ( cmdClass.GetData()->fCmd )
	{
	case kQuitCommand:
	    LOG( AliHLTLog::kInformational, "TestCommandProcessor::ProcessCmd", "Quit" )
		<< "Quit command received" << ENDLOG;
	    if ( !fIgnoreQuit && fQuitValue )
		*fQuitValue = 1;
	    break;
	case kLAMCommand:
	    LOG( AliHLTLog::kInformational, "TestCommandProcessor::ProcessCmd", "LAM" )
		<< "LootAtMe command received" << ENDLOG;
	    if ( fInterface )
		fInterface->LookAtMe();
	    break;
	case kPrintCommand:
	    LOG( AliHLTLog::kInformational, "TestCommandProcessor::ProcessCmd", "Print" )
		<< "Print command received. Printing: 0x" << AliHLTLog::kHex
		<< cmdClass.GetData()->fParam0 << AliHLTLog::kDec << " (" 
		<< cmdClass.GetData()->fParam0 << ")." << ENDLOG;
	    break;
	case kIgnoreQuitCommand:
	    fIgnoreQuit = true;
	    break;
	default:
	    LOG( AliHLTLog::kWarning, "TestCommandProcessor::ProcessCmd", "Unknown Command" )
		<< "Unknown command received: 0x" << AliHLTLog::kHex << cmdClass.GetData()->fCmd
		<< " (" << AliHLTLog::kDec << cmdClass.GetData()->fCmd << "." << ENDLOG;
	    break;
	}
    }

int main( int argc, char** argv )
    {
    AliUInt32_t quitValue = 0;
    int ret, i=0;
    struct timeval now;
    gLogLevel = AliHLTLog::kAll;
    BCLAddressLogger::Init();
    AliHLTFileLogServer fileLog( "SCTestClient-", ".log", 500000, 100 );
    AliHLTFilteredStdoutLogServer stdOut;
    gLog.AddServer( fileLog );
    gLog.AddServer( stdOut );

    char* addressURL = NULL;
    char* errorStr = NULL;

    char *usage1 = "Usage: ", *usage2 = " -address <msg-communication-address-URL>";

    i = 1;
    while ( i<argc )
	{
	if ( !strcmp( argv[i], "-address" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing msg communication address URL.";
		break;
		}
	    addressURL = argv[i+1];
	    i += 2;
	    continue;
	    }
	errorStr = "Unknown option";
	break;
	}
    if ( !errorStr && !addressURL )
	errorStr = "Must specify -address option with msg communication address URL.";
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "SCTestClient", "Usage" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	LOG( AliHLTLog::kError, "SCTestClient", "Usage" )
	    << "Error: " << errorStr << ENDLOG;
	if ( i<argc )
	    {
	    LOG( AliHLTLog::kError, "SCTestClient", "Usage" )
		<< "Offending parameter: " << argv[i] << ENDLOG;
	    }
	return 0;
	}
    
    
    AliHLTSCInterface scInterface;
    AliHLTStatus status;
    scInterface.SetStatusData( &(status.fHeader) );
    BCLErrorLogCallback errorCallback;

    TestCommandProcessor commandProcessor( &quitValue, &scInterface );
    scInterface.AddCommandProcessor( &commandProcessor );
    ret = scInterface.Bind( addressURL, &errorCallback );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "SCTestClient", "Bind Error" )
	    << "Bind error on address URL '" << addressURL 
	    << "': " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    else
	{
	i = 0;
	ret = scInterface.Start();
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "SCTestClient", "Start Error" )
		<< "Error starting interface object: " 
		<< strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    }
	}
    if ( !ret )
	{

	while ( !quitValue )
	    {
	    LOG( AliHLTLog::kDebug, "SCTestClient", "Test Message" )
		<< AliHLTLog::kDec << "Test message " << i++ << ENDLOG;
	    gettimeofday( &now, NULL );
	    status.fHLTStatus.fProcessedEventCount = i;
	    status.fHLTStatus.fCurrentEvent = i;
	    status.fHLTStatus.fCurrentEventStart_s = now.tv_sec;
	    status.fHLTStatus.fCurrentEventStart_us = now.tv_usec;
	    usleep( 1000000 );
	    LOG( AliHLTLog::kInformational, "SCTestClient", "Test Message" )
		<< AliHLTLog::kDec << "Test message " << i++ << ENDLOG;
	    gettimeofday( &now, NULL );
	    status.fHLTStatus.fProcessedEventCount = i;
	    status.fHLTStatus.fCurrentEvent = i;
	    status.fHLTStatus.fCurrentEventStart_s = now.tv_sec;
	    status.fHLTStatus.fCurrentEventStart_us = now.tv_usec;
	    usleep( 1000000 );
	    LOG( AliHLTLog::kWarning, "SCTestClient", "Test Message" )
		<< AliHLTLog::kDec << "Test message " << i++ << ENDLOG;
	    gettimeofday( &now, NULL );
	    status.fHLTStatus.fProcessedEventCount = i;
	    status.fHLTStatus.fCurrentEvent = i;
	    status.fHLTStatus.fCurrentEventStart_s = now.tv_sec;
	    status.fHLTStatus.fCurrentEventStart_us = now.tv_usec;
	    usleep( 1000000 );
	    LOG( AliHLTLog::kError, "SCTestClient", "Test Message" )
		<< AliHLTLog::kDec << "Test message " << i++ << ENDLOG;
	    gettimeofday( &now, NULL );
	    status.fHLTStatus.fProcessedEventCount = i;
	    status.fHLTStatus.fCurrentEvent = i;
	    status.fHLTStatus.fCurrentEventStart_s = now.tv_sec;
	    status.fHLTStatus.fCurrentEventStart_us = now.tv_usec;
	    usleep( 1000000 );
	    LOG( AliHLTLog::kFatal, "SCTestClient", "Test Message" )
		<< AliHLTLog::kDec << "Test message " << i++ << ENDLOG;
	    gettimeofday( &now, NULL );
	    status.fHLTStatus.fProcessedEventCount = i;
	    status.fHLTStatus.fCurrentEvent = i;
	    status.fHLTStatus.fCurrentEventStart_s = now.tv_sec;
	    status.fHLTStatus.fCurrentEventStart_us = now.tv_usec;
	    usleep( 1000000 );

	    }
	LOG( AliHLTLog::kInformational, "SCTestClient", "Quitting" )
	    << "Quitting now..:" << ENDLOG;

	}

    return 0;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
