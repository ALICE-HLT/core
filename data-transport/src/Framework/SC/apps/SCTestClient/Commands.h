#ifndef _COMMANDS_H_
#define _COMMANDS_H_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCSysCmds.hpp"

const AliUInt32_t kQuitCommand = kAliHLTSCSFirstUserCmd + 1;
const AliUInt32_t kLAMCommand = kAliHLTSCSFirstUserCmd + 2;
const AliUInt32_t kPrintCommand = kAliHLTSCSFirstUserCmd + 3;
const AliUInt32_t kIgnoreQuitCommand = kAliHLTSCSFirstUserCmd + 4;



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif /* _COMMANDS_H_ */
