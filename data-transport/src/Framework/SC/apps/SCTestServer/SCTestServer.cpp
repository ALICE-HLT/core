/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCController.hpp"
#include "AliHLTSCLAMHandler.hpp"
#include "AliHLTStatus.hpp"
#include "AliHLTSCStatusHeader.hpp"
#include "AliHLTSCStatusBase.hpp"
#include "AliHLTSCProcessStatus.hpp"
#include "AliHLTSCHLTStatus.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "BCLCommunication.hpp"
#include "BCLAbstrAddress.hpp"
#include "BCLURLAddressDecoder.hpp"
#include "BCLAddressLogger.hpp"
#include "BCLErrorLogCallback.hpp"
#include "../SCTestClient/Commands.h"


class TestLAMHandler: public AliHLTSCLAMHandler
    {
    public:

	TestLAMHandler( bool *lamReceived )
		{
		fLAMReceived = lamReceived;
		}

	virtual void LookAtMe( BCLAbstrAddressStruct* me );

    protected:
	
	bool *fLAMReceived;

    };

void TestLAMHandler::LookAtMe( BCLAbstrAddressStruct* me )
    {
    LOG( AliHLTLog::kWarning, "TestLAMHandler::LookAtMe", "LAM received" )
	<< "Received LookAtMe request from address `";
	BCLAddressLogger::LogAddress( gLog, *me )
	    << "`." << ENDLOG;
    if ( fLAMReceived )
	*fLAMReceived = true;
    }


int main( int argc, char** argv )
    {
    gLogLevel = AliHLTLog::kAll;
    BCLAddressLogger::Init();
    AliHLTFileLogServer fileLog( "SCTestServer-", ".log", 500000, 100 );
    AliHLTFilteredStdoutLogServer stdOut;
    gLog.AddServer( fileLog );
    gLog.AddServer( stdOut );

    bool lamReceived = false;
    int ret;

    int i = 1;
    char* errorStr = NULL;
    char *cerr = NULL;
    bool sendAbortCommand = false;
    bool sendQuitCommand = false;
    bool sendIgnoreQuitCommand = false;
    bool sendPrintCommand = false;
    bool simLAM = false;
    AliUInt32_t printCommandParam = 0;
    bool setVerbosity = false;
    AliUInt32_t verbosity = 0x3F;
    bool readStatus = false;
    bool connect = false;
    char* addressURL = NULL;
    char* clientAddressURL = NULL;
    BCLAbstrAddressStruct* clientAddress = NULL;
    
    char *usage1 = "Usage: ", *usage2 = " -address <msg-communication-address-URL> <client-address-URL> (-quitcommand) (-abortcmd) (-printcommand <numerical-print-param>) (-setverbosity <verbosity>) (-readstatus) (-connect) (-ignorequitcommand) (-simLAM)";

    while ( i<argc )
	{
	if ( !strcmp( argv[i], "-address" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing address URLs.";
		break;
		}
	    if ( argc <= i+2 )
		{
		errorStr = "Missing client address URL.";
		break;
		}
	    addressURL = argv[i+1];
	    clientAddressURL = argv[i+2];
	    ret = BCLDecodeRemoteURL( clientAddressURL, clientAddress );
	    if ( ret )
		{
		errorStr = "Invalid client address URL.";
		break;
		}
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-printcommand" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing print command parameter specifier.";
		break;
		}
	    printCommandParam = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting print command parameter specifier:";
		break;
		}
	    sendPrintCommand = true;
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-quitcommand" ) )
	    {
	    sendQuitCommand = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-ignorequitcommand" ) )
	    {
	    sendIgnoreQuitCommand = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-abortcommand" ) )
	    {
	    sendAbortCommand = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-simLAM" ) )
	    {
	    simLAM = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-setverbosity" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing verbosity specifier.";
		break;
		}
	    verbosity = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting verbosity specifier:";
		break;
		}
	    setVerbosity = true;
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-readstatus" ) )
	    {
	    readStatus = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-connect" ) )
	    {
	    connect = true;
	    i += 1;
	    continue;
	    }
	errorStr = "Unknown option";
	break;
	}
    if ( !errorStr && (!addressURL || !clientAddressURL) )
	errorStr = "Must specify -address option with address URLs.";
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "SCTestServer", "Usage" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	LOG( AliHLTLog::kError, "SCTestServer", "Usage" )
	    << "Error: " << errorStr << ENDLOG;
	if ( i < argc )
	    {
	    LOG( AliHLTLog::kError, "SCTestServer", "Usage" )
		<< "Offending parameter: " << argv[i] << ENDLOG;
	    }
	return 0;
	}

    AliHLTSCController controller;
    controller.SetReplyTimeout( 2000 ); // Reply timeout of 2s.
    TestLAMHandler lamHandler( &lamReceived );
    BCLErrorLogCallback errorCallback;
    controller.AddLAMHandler( &lamHandler );

    ret = controller.Bind( addressURL, &errorCallback );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "SCTestServer", "Bind Error" )
	    << "Bind error on address URL '" << addressURL 
	    << "': " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return ret;
	}
    
    controller.Start();

    if ( connect )
	{
	ret = controller.Connect( clientAddress );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "SCTestServer", "Connect Error" )
		<< "Connect error on client address URL '" << clientAddressURL 
		<< "': " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    connect = false;
	    }
	}
    
    if ( sendPrintCommand )
	{
	AliHLTSCCommand cmd;
	cmd.GetData()->fCmd = kPrintCommand;
	cmd.GetData()->fParam0 = printCommandParam;
	ret = controller.SendCommand( clientAddress, cmd.GetData() );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "SCTestServer", "SendCommand Error" )
		<< "Error sending print command with parameter 0x" 
		<< AliHLTLog::kHex << printCommandParam << " (" 
		<< AliHLTLog::kDec << printCommandParam << ") to "
		<< (connect ? "connected" : "unconnected") << " client address URL '" << clientAddressURL 
		<< "': " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    }
	}

    if ( sendQuitCommand )
	{
	AliHLTSCCommand cmd;
	cmd.GetData()->fCmd = kQuitCommand;
	ret = controller.SendCommand( clientAddress, cmd.GetData() );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "SCTestServer", "SendCommand Error" )
		<< "Error sending quit command to "
		<< (connect ? "connected" : "unconnected") << " client address URL '" << clientAddressURL 
		<< "': " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    }
	}

    if ( sendIgnoreQuitCommand )
	{
	AliHLTSCCommand cmd;
	cmd.GetData()->fCmd = kIgnoreQuitCommand;
	ret = controller.SendCommand( clientAddress, cmd.GetData() );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "SCTestServer", "SendCommand Error" )
		<< "Error sending ignore-quit command to "
		<< (connect ? "connected" : "unconnected") << " client address URL '" << clientAddressURL 
		<< "': " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    }
	}

    if ( sendAbortCommand )
	{
	AliHLTSCCommand cmd;
	cmd.GetData()->fCmd = kAliHLTSCPanicAbortSysCmd;
	ret = controller.SendCommand( clientAddress, cmd.GetData() );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "SCTestServer", "SendCommand Error" )
		<< "Error sending abort command to "
		<< (connect ? "connected" : "unconnected") << " client address URL '" << clientAddressURL 
		<< "': " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    }
	}


    if ( setVerbosity )
	{
	ret = controller.SetVerbosity( clientAddress, verbosity );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "SCTestServer", "SetVerbosity Error" )
		<< "Error setting verbosity to 0x" << AliHLTLog::kHex << verbosity << " on "
		<< (connect ? "connected" : "unconnected") << " client address URL '" << clientAddressURL 
		<< "': " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    }
	}

    if ( readStatus )
	{
	AliHLTSCStatusHeaderStruct* status = NULL;
	ret = controller.GetStatus( clientAddress, status );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "SCTestServer", "GetStatus Error" )
		<< "Error getting status from "
		<< (connect ? "connected" : "unconnected") << " client address URL '" << clientAddressURL 
		<< "': " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    }

	if ( status )
	    {
	    AliHLTStatus* hltStatus;
	    hltStatus = (AliHLTStatus*)status;
	    AliHLTSCHLTStatus stat( &(hltStatus->fHLTStatus) );
	    AliHLTSCStatusHeader header( &(hltStatus->fHeader) );
	    struct tm* t;
	    unsigned int date, time_s, time_us;
	    time_t tmpTime = (time_t)stat.GetData()->fCurrentEventStart_s;
	    t = localtime( &tmpTime );
	    t->tm_mon++;
	    t->tm_year += 1900;
	    date = t->tm_year*10000+t->tm_mon*100+t->tm_mday;
	    time_s = t->tm_hour*10000+t->tm_min*100+t->tm_sec;
	    time_us = stat.GetData()->fCurrentEventStart_us;


	    LOG( AliHLTLog::kInformational, "SCTestServer", "Status Header" )
		<< "Status Header length: " << AliHLTLog::kDec << header.GetData()->fLength
		<< " - status struct count: " << header.GetData()->fStatusStructCnt
		<< "." << ENDLOG;
	    LOG( AliHLTLog::kInformational, "SCTestServer", "Status" )
		<< "Status read: Processed event count: " << AliHLTLog::kDec
		<< stat.GetData()->fProcessedEventCount << " - Current event: "
		<< stat.GetData()->fCurrentEvent << " - Current event started: "
		<< AliHLTLog::kPrec << 8 << date << "." << AliHLTLog::kPrec << 6
		<< time_s << "." << time_us << "." << ENDLOG;
	    }

	if ( status )
	    controller.FreeStatus( status );
	}

    if ( simLAM )
	{
	AliHLTSCCommand cmd;
	cmd.GetData()->fCmd = kLAMCommand;
	ret = controller.SendCommand( clientAddress, cmd.GetData() );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "SCTestServer", "SendCommand Error" )
		<< "Error sending LAM command to "
		<< (connect ? "connected" : "unconnected") << " client address URL '" << clientAddressURL 
		<< "': " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    }
	struct timeval start, now;
	unsigned long long deltaT = 0;
	const unsigned long long timeLimit = 100000000;
	gettimeofday( &start, NULL );
	while ( !lamReceived && deltaT<timeLimit )
	    {
	    usleep( 100000 );
	    gettimeofday( &now, NULL );
	    deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	    }
	if ( !lamReceived )
	    {
	    LOG( AliHLTLog::kError, "SCTestServer", "LAM Error" )
		<< "No  LAM received from "
		<< (connect ? "connected" : "unconnected") << " client address URL '" << clientAddressURL 
		<< "' after " << AliHLTLog::kDec << i << " tries." << ENDLOG;
	    }
	else
	    {
	    LOG( AliHLTLog::kInformational, "SCTestServer", "LAM Received" )
		<< "LAM received from "
		<< (connect ? "connected" : "unconnected") << " client address URL '" << clientAddressURL 
		<< "'." << ENDLOG;
	    }
	}

    controller.Stop();
    return 0;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
