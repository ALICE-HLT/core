/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCController.hpp"
#include "AliHLTSCCommands.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "BCLCommunication.hpp"
#include "BCLAbstrAddress.hpp"
#include "BCLURLAddressDecoder.hpp"
#include "BCLAddressLogger.hpp"
#include "BCLErrorLogCallback.hpp"


int main( int argc, char** argv )
    {
    gLogLevel = AliHLTLog::kAll;
    BCLAddressLogger::Init();
    AliHLTFilteredStdoutLogServer stdOut;
    gLog.AddServer( stdOut );

    int ret;

    int i = 1;
    char* errorStr = NULL;
    char *cerr = NULL;
    bool sendAbortCommand = false;
    //bool sendQuitCommand = false;
    bool sendPauseCommand = false;
    bool sendStartCommand = false;
    bool setVerbosity = false;
    AliUInt32_t verbosity = 0x3F;
    char* addressURL = NULL;
    vector<char*> clientAddressURLs;
    vector<char*>::iterator URLIter, URLEnd;
    vector<BCLAbstrAddressStruct*> clientAddresses;
    vector<BCLAbstrAddressStruct*>::iterator clientIter, clientEnd;
    BCLAbstrAddressStruct* tmpAddress = NULL;
    bool sendConnectCmd = false;
    bool sendDisconnectCmd = false;
    bool sendReconnectCmd = false;
    bool sendNewAddressCmd = false;
    bool sendNewConnectionCmd = false;
    bool sendBridgeNodeStateCmd = false;
    bool sendPURGEALLEVENTSCmd = false;
    char* newMsgAddress = NULL;
    char* newBlobMsgAddress = NULL;
    bool sendCmd = false;
    unsigned long bridgeNodeNr = ~(unsigned long)0;
    bool bridgeNodeState = false;
    bool doProcessStart = false;
    char* processStartFileName = NULL;
    char* processStartID = NULL;
    
    char *usage1 = "Usage: ", *usage2 = " -address <msg-communication-address-URL> -control <client-address-URL> (-control  <client-address-URL>) (-start) (-pause) (-abort) (-disconnect) (-connect) (-reconnect) (-newaddress <new-msg-address-URL> <new-blobmsg-address-url>) (-newconnection <new-msg-address-URL> <new-blobmsg-address-url>) (-bridgenode <node-nr> [up|down]) (-setverbosity <verbosity>) (-PURGEALLEVENTS) (-processstart <filename> <id>)";

    while ( i<argc )
	{
	if ( !strcmp( argv[i], "-address" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing address URL.";
		break;
		}
	    addressURL = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-control" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing address URL.";
		break;
		}
	    ret = BCLDecodeRemoteURL( argv[i+1], tmpAddress );
	    if ( ret )
		{
		errorStr = "Invalid client address URL.";
		break;
		}
	    clientAddressURLs.insert( clientAddressURLs.end(), argv[i+1] );
	    clientAddresses.insert( clientAddresses.end(), tmpAddress );
	    tmpAddress = NULL;
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-start" ) )
	    {
	    sendStartCommand = true;
	    sendCmd = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-pause" ) )
	    {
	    sendPauseCommand = true;
	    sendCmd = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-abort" ) )
	    {
	    sendAbortCommand = true;
	    sendCmd = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-connect" ) )
	    {
	    sendConnectCmd = true;
	    sendCmd = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-disconnect" ) )
	    {
	    sendDisconnectCmd = true;
	    sendCmd = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-reconnect" ) )
	    {
	    sendReconnectCmd = true;
	    sendCmd = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-newaddress" ) )
	    {
	    if ( argc <= i+2 )
		{
		errorStr = "Missing new msg address URL or new blob msg address URL.";
		break;
		}
	    sendNewAddressCmd = true;
	    newMsgAddress = argv[i+1];
	    newBlobMsgAddress = argv[i+2];
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-newconnection" ) )
	    {
	    if ( argc <= i+2 )
		{
		errorStr = "Missing new msg address URL or new blob msg address URL.";
		break;
		}
	    sendNewConnectionCmd = true;
	    newMsgAddress = argv[i+1];
	    newBlobMsgAddress = argv[i+2];
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-bridgenode" ) )
	    {
	    if ( argc <= i+2 )
		{
		errorStr = "Missing bridge node nr or bridge node state specifier.";
		break;
		}
	    bridgeNodeNr = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting bridge node nr specifier:";
		break;
		}
	    if ( !strcmp( argv[i+2], "up" ) )
		bridgeNodeState = true;
	    else if ( !strcmp( argv[i+2], "down" ) )
		bridgeNodeState = false;
	    else
		{
		errorStr = "Wrong bridge node state specifier (up or down).";
		break;
		}
	    sendBridgeNodeStateCmd = true;
	    sendCmd = true;
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-PURGEALLEVENTS" ) )
	    {
	    sendPURGEALLEVENTSCmd = true;
	    sendCmd = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-setverbosity" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing verbosity specifier.";
		break;
		}
	    verbosity = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting verbosity specifier:";
		break;
		}
	    setVerbosity = true;
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-processstart" ) )
	    {
	    if ( argc <= i+2 )
		{
		errorStr = "Missing process start filename or id specifier.";
		break;
		}
	    doProcessStart = true;
	    processStartFileName = argv[i+1];
	    processStartID = argv[i+2];
	    i += 3;
	    continue;
	    }
	errorStr = "Unknown option";
	break;
	}
    if ( !errorStr )
	{
	if ( !addressURL )
	    errorStr = "Must specify -address option with address URLs.";
	else if ( clientAddressURLs.size()<=0 )
	    errorStr = "Must specify at least one -control option.";
	}
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "SCCommander", "Usage" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	LOG( AliHLTLog::kError, "SCCommander", "Usage" )
	    << "Error: " << errorStr << ENDLOG;
	if ( i < argc )
	    {
	    LOG( AliHLTLog::kError, "SCCommander", "Usage" )
		<< "Offending parameter: " << argv[i] << ENDLOG;
	    }
	return 0;
	}

    AliHLTSCController controller;
    controller.SetReplyTimeout( 2000 ); // Reply timeout of 2s.
    BCLErrorLogCallback errorCallback;

    ret = controller.Bind( addressURL, &errorCallback );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "SCCommander", "Bind Error" )
	    << "Bind error on address URL '" << addressURL 
	    << "': " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return ret;
	}
    
    controller.Start();
    AliHLTSCCommand cmd;
    char* commandName = "unknown";

    
    if ( sendStartCommand )
	{
	cmd.GetData()->fCmd = kAliHLTSCStartCmd;
	commandName = "start";
	}

    if ( sendPauseCommand )
	{
	cmd.GetData()->fCmd = kAliHLTSCPauseCmd;
	commandName = "pause";
	}

    if ( sendAbortCommand )
	{
	cmd.GetData()->fCmd = kAliHLTSCPanicAbortSysCmd;
	commandName = "abort";
	}

    if ( sendConnectCmd )
	{
	cmd.GetData()->fCmd = kAliHLTSCConnectCmd;
	commandName = "connect";
	}

    if ( sendDisconnectCmd )
	{
	cmd.GetData()->fCmd = kAliHLTSCDisconnectCmd;
	commandName = "disconnect";
	}

    if ( sendReconnectCmd )
	{
	cmd.GetData()->fCmd = kAliHLTSCReconnectCmd;
	commandName = "reconnect";
	}
    
    if ( sendBridgeNodeStateCmd )
	{
	cmd.GetData()->fCmd = kAliHLTSCSetBridgeNodeStateCmd;
	cmd.GetData()->fParam0 = bridgeNodeNr;
	cmd.GetData()->fParam1 = bridgeNodeState;
	commandName = "set bridge node state";
	}

    if ( sendPURGEALLEVENTSCmd )
	{
	cmd.GetData()->fCmd = kAliHLTSCPURGEALLEVENTSCmd;
	commandName = "PURGE";
	}

    if ( setVerbosity )
	{
	clientIter = clientAddresses.begin();
	clientEnd = clientAddresses.end();
	URLIter = clientAddressURLs.begin();
	URLEnd = clientAddressURLs.end();
	while ( clientIter != clientEnd )
	    {
	    ret = controller.SetVerbosity( *clientIter, verbosity );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "SCCommander", "SetVerbosity Error" )
		    << "Error setting verbosity to 0x" << AliHLTLog::kHex << verbosity 
		    << " on client address URL '" << *URLIter 
		    << "': " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << ")." << ENDLOG;
		break;
		}
	    clientIter++;
	    if ( URLIter != URLEnd )
		URLIter++;
	    }
	}
    if ( sendNewAddressCmd || sendNewConnectionCmd )
	{
	unsigned long msgLen = cmd.GetData()->fLength + strlen( newMsgAddress ) + strlen( newBlobMsgAddress ) + 2;
	AliUInt8_t *msgPtr;
	AliHLTSCCommandStruct* msg;
	msgPtr = new AliUInt8_t[ msgLen ];
	if ( !msgPtr )
	    {
	    LOG( AliHLTLog::kError, "SCCommander", "Out of Memory" )
		<< "Out of memory trying to allocate new address command message of "
		<< AliHLTLog::kDec << msgLen << " bytes." << ENDLOG;
	    }
	else
	    {
	    msg = ((AliHLTSCCommandStruct*)msgPtr);
	    memcpy( msgPtr, cmd.GetData(), cmd.GetData()->fLength );
	    msg->fLength = msgLen;
	    memcpy( msg->fData, newMsgAddress, strlen(newMsgAddress)+1 );
	    memcpy( ((AliUInt8_t*)msg->fData)+strlen(newMsgAddress)+1, newBlobMsgAddress, strlen(newBlobMsgAddress)+1 );
	    msg->fParam0 = 3; // Bits 0 1 and set to signify that both addresses are present.
	    if ( sendNewConnectionCmd )
		msg->fCmd = kAliHLTSCNewConnectionCmd;
	    else
		msg->fCmd = kAliHLTSCNewRemoteAddressesCmd;
	    clientIter = clientAddresses.begin();
	    clientEnd = clientAddresses.end();
	    URLIter = clientAddressURLs.begin();
	    URLEnd = clientAddressURLs.end();
	    while ( clientIter != clientEnd )
		{
		ret = controller.SendCommand( *clientIter, msg );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "SCCommander", "SendCommand Error" )
			<< "Error sending new " << (char*)(sendNewConnectionCmd ? "connection" : "address") << " command to client address URL '" << *URLIter
			<< "': " << strerror(ret) << " (" << AliHLTLog::kDec
			<< ret << ")." << ENDLOG;
		    break;
		    }
		clientIter++;
		if ( URLIter != URLEnd )
		    URLIter++;
		}
	    }
	}

    if ( doProcessStart )
	{
	AliHLTSCCommandStruct* cmdTmp;
	unsigned long sz = cmd.GetData()->fLength+strlen(processStartFileName)+strlen(processStartID)+2;
	cmdTmp = (AliHLTSCCommandStruct*)new AliUInt8_t[ sz ];
	if ( !cmdTmp )
	    {
	    LOG( AliHLTLog::kError, "SCCommander", "Out Of Memory" )
		<< "Out of memory trying to allocate command of " << AliHLTLog::kDec 
		<< sz << " bytes to send process start command." << ENDLOG;
	    }
	else
	    {
	    memcpy( cmdTmp, cmd.GetData(), cmd.GetData()->fLength );
	    cmdTmp->fCmd = kAliHLTSCExecuteCommandCmd;
	    strcpy( (char*)cmdTmp->fData, processStartFileName );
	    strcpy( (char*)(cmdTmp->fData)+strlen(processStartFileName)+1, processStartID );
	    cmdTmp->fLength = sz;
	    clientIter = clientAddresses.begin();
	    clientEnd = clientAddresses.end();
	    URLIter = clientAddressURLs.begin();
	    URLEnd = clientAddressURLs.end();
	    while ( clientIter != clientEnd )
		{
		ret = controller.SendCommand( *clientIter, cmdTmp );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "SCCommander", "SendCommand Error" )
			<< "Error sending " << commandName 
			<< " processstart to client address URL '" << *URLIter
			<< "': " << strerror(ret) << " (" << AliHLTLog::kDec
			<< ret << ")." << ENDLOG;
		    break;
		    }
		clientIter++;
		if ( URLIter != URLEnd )
		    URLIter++;
		}
	    delete [] (AliUInt8_t*)cmdTmp;
	    }
	}

    if ( sendCmd )
	{
	clientIter = clientAddresses.begin();
	clientEnd = clientAddresses.end();
	URLIter = clientAddressURLs.begin();
	URLEnd = clientAddressURLs.end();
	while ( clientIter != clientEnd )
	    {
	    ret = controller.SendCommand( *clientIter, cmd.GetData() );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "SCCommander", "SendCommand Error" )
		    << "Error sending " << commandName 
		    << " command to client address URL '" << *URLIter
		    << "': " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << ")." << ENDLOG;
		break;
		}
	    clientIter++;
	    if ( URLIter != URLEnd )
		URLIter++;
	    }
	}


    controller.Stop();
    return 0;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
