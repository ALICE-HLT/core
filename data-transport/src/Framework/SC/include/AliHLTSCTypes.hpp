#ifndef _AliHLTSCTYPES_HPP_
#define _AliHLTSCTYPES_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"


const AliUInt32_t kAliHLTSCUndefinedComponentType                         = 0x00000000;
const AliUInt32_t kAliHLTSCControlledDataSourceComponentType              = 0x00000001;
const AliUInt32_t kAliHLTSCControlledDataSinkComponentType                = 0x00000002;
const AliUInt32_t kAliHLTSCControlledDataProcessorComponentType           = 0x00000003;
const AliUInt32_t kAliHLTSCControlledBridgeComponentType                  = 0x00000004;
const AliUInt32_t kAliHLTSCEventScattererComponentType                    = 0x00000005;
const AliUInt32_t kAliHLTSCEventGathererComponentType                     = 0x00000006;
const AliUInt32_t kAliHLTSCEventMergerComponentType                       = 0x00000007;


#define ALIHLTPROCESSSTARTERSTATUSTYPE (((AliUInt64_t)'PROC')<<32 | 'STRT')





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _AliHLTSCTYPES_HPP_
