#ifndef _ALIL3SCINTERFACE_HPP_
#define _ALIL3SCINTERFACE_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCCommandProcessor.hpp"
#include "AliHLTSCStatusHeader.hpp"
#include "BCLAbstrAddress.hpp"
#include "BCLMessage.hpp"
#include "BCLMsgCommunication.hpp"
#include "BCLErrorCallback.hpp"
#include "AliHLTThread.hpp"
#include "MLUCConditionSem.hpp"
#include <vector>
#include <pthread.h>
// XXXX TODO LOCKING XXXX
#include "MLUCMutex.hpp"

struct AliHLTSCMessageStruct;


class AliHLTSCInterface
    {
    public:

	AliHLTSCInterface();
	virtual ~AliHLTSCInterface();

	int Start();
	int Stop();
	
	void SetStatusData( AliHLTSCStatusHeaderStruct volatile* statusData );

// XXXX TODO LOCKING XXXX
	void SetStatusDataMutex( MLUCMutex* statusDataMutex );

	void AddCommandProcessor( AliHLTSCCommandProcessor* cmdProc );
	void DelCommandProcessor( AliHLTSCCommandProcessor* cmdProc );

	int Bind( const char* addressURL, BCLErrorCallback* errorCallback = NULL );
	int Unbind();
	BCLMsgCommunication* GetMsgCom() const
		{
		return fMsgCom;
		}
	BCLAbstrAddressStruct* GetAddress() const
		{
		return fAddress;
		}

	int LookAtMe(); // Send status data asnychronously if controller object is connected.
	int LookAtMe( BCLAbstrAddressStruct* address ); // Send status data asnychronously to listening party.

    protected:

	virtual void ProcessSystemCmd( AliHLTSCCommandStruct* cmd );

	void ListenHandler();
	bool fQuitListenHandler;
	bool fListenHandlerQuitted;

	void CmdHandler();
	bool fQuitCmdHandler;
	bool fCmdHandlerQuitted;

	pthread_mutex_t fSendMutex;
	BCLAbstrAddressStruct* fAddress;
	BCLMsgCommunication* fMsgCom;
	bool fIsBound;

	unsigned long fSendTimeout;

	pthread_mutex_t fConnectedAddressMutex;
	vector<BCLAbstrAddressStruct*> fConnectedAddresses;

// XXXX TODO LOCKING XXXX
	MLUCMutex* fStatusDataMutex;
	AliHLTSCStatusHeaderStruct volatile* fStatusData;

	pthread_mutex_t fCmdProcessorMutex;
	vector<AliHLTSCCommandProcessor*> fCmdProcessors;

	MLUCConditionSem<AliHLTSCMessageStruct*> fCmdSignal;

	AliHLTObjectThread<AliHLTSCInterface> fListenThread;
	AliHLTObjectThread<AliHLTSCInterface> fCmdHandlerThread;


    };


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3SCINTERFACE_HPP_
