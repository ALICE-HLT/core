#ifndef _ALIL3SCCONTROLLER_HPP_
#define _ALIL3SCCONTROLLER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCCommandProcessor.hpp"
#include "AliHLTSCStatusHeader.hpp"
#include "AliHLTSCMessage.hpp"
#include "AliHLTSCLAMHandler.hpp"
#include "AliHLTTimer.hpp"
#include "BCLErrorCallback.hpp"
#include "BCLMsgCommunication.hpp"
#include "BCLAbstrAddress.hpp"
#include "BCLMessage.hpp"
#include "AliHLTThread.hpp"
#include <vector>
#include <pthread.h>


class AliHLTSCController
    {
    public:

	AliHLTSCController();
	~AliHLTSCController();

	int Start();
	int Stop();

	void AddLAMHandler( AliHLTSCLAMHandler* handler );
	void DelLAMHandler( AliHLTSCLAMHandler* handler );

	void SetReplyTimeout( AliUInt32_t replyTimeout_ms )
		{
		fReplyTimeout = replyTimeout_ms;
		}

	int Bind( const char* addressURL, BCLErrorCallback* errorCallback = NULL );
	int Unbind();
	BCLMsgCommunication* GetMsgCom() const
		{
		return fMsgCom;
		}
	BCLAbstrAddressStruct* GetAddress() const
		{
		return fAddress;
		}

	int Connect( BCLAbstrAddressStruct* address, bool openOnDemand = false ); // Optional
	int Disconnect( BCLAbstrAddressStruct* address ); 

	int SetVerbosity( BCLAbstrAddressStruct* address, AliUInt32_t verbosity );

	int SendCommand( BCLAbstrAddressStruct* address, const AliHLTSCCommandStruct* cmd );

	int GetStatus( BCLAbstrAddressStruct* address, AliHLTSCStatusHeaderStruct*& status );
	int FreeStatus( AliHLTSCStatusHeaderStruct* status );

	void SuppressComErrorLogs( BCLAbstrAddressStruct* address, unsigned long timeout_us );

    protected:

	void MsgHandler(); // To handle receiving of messages.

	int WaitForMessage( AliHLTSCMessageStruct*& message, AliUInt32_t refNdx, AliUInt32_t timeout_ms );
	
	AliUInt32_t GetNextRefNdx()
		{
		if ( ++fMsgRefNdx == 0 )
		    ++fMsgRefNdx;
		return fMsgRefNdx;
		}

	bool SuppressComErrorLogs( BCLAbstrAddressStruct* address );

	vector<AliHLTSCMessageStruct*> fReplyMessages;
	AliHLTTimerConditionSem fReplySignal;

	AliHLTObjectThread<AliHLTSCController> fMsgThread;
	bool fQuitMsgThread;
	bool fMsgThreadQuitted;

	pthread_mutex_t fComMutex;
	BCLMsgCommunication* fMsgCom;
	BCLAbstrAddressStruct* fAddress;
	bool fIsBound;

	pthread_mutex_t fLAMHandlerMutex;
	vector<AliHLTSCLAMHandler*> fLAMHandlers;

	AliUInt32_t fMsgRefNdx;

	AliUInt32_t fReplyTimeout;

	unsigned long fSendTimeout;

	struct TComErrorLogSuppressionData
	    {
		BCLAbstrAddressStruct* fAddress;
		struct timeval fUntil;
	    };
	vector<TComErrorLogSuppressionData> fComErrorLogSuppressions;
	pthread_mutex_t fComErrorLogSuppressionsMutex;

    };


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3SCCONTROLLER_HPP_
