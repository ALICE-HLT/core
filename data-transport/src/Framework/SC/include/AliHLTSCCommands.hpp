/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#ifndef _ALIL3SCCOMMANDS_HPP_
#define _ALIL3SCCOMMANDS_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCSysCmds.hpp"

// Note, these commands will have to be merged with the process control
// commands in L3SI/include/AliHLTProcessControlCommands.hpp

// For the tolerant classes/components (scatterer,collector,bridgeheads)
const AliUInt32_t kAliHLTSCSetPublisherStatus               = kAliHLTSCSFirstUserCmd + 0x00000000;
const AliUInt32_t kAliHLTSCStartCmd                         = kAliHLTSCSFirstUserCmd + 0x00000001;
const AliUInt32_t kAliHLTSCPauseCmd                         = kAliHLTSCSFirstUserCmd + 0x00000002;
const AliUInt32_t kAliHLTSCQuitCmd                          = kAliHLTSCSFirstUserCmd + 0x00000003;

const AliUInt32_t kAliHLTSCDisconnectCmd                    = kAliHLTSCSFirstUserCmd + 0x00000004;
const AliUInt32_t kAliHLTSCConnectCmd                       = kAliHLTSCSFirstUserCmd + 0x00000005;
const AliUInt32_t kAliHLTSCReconnectCmd                     = kAliHLTSCSFirstUserCmd + 0x00000006;
const AliUInt32_t kAliHLTSCNewConnectionCmd                 = kAliHLTSCSFirstUserCmd + 0x00000007;
const AliUInt32_t kAliHLTSCNewRemoteAddressesCmd            = kAliHLTSCSFirstUserCmd + 0x00000008;

// Dangerous command. Can cause event loss in the system if used improperly.
const AliUInt32_t kAliHLTSCPURGEALLEVENTSCmd                = kAliHLTSCSFirstUserCmd + 0x00000009;

// Commands destined for a BridgeToleranceManager:
const AliUInt32_t kAliHLTSCSetBridgeNodeStateCmd            = kAliHLTSCSFirstUserCmd + 0x0000000A;

// Commands for the ProcessStarter
const AliUInt32_t kAliHLTSCExecuteCommandCmd                = kAliHLTSCSFirstUserCmd + 0x0000000B;

const AliUInt32_t kAliHLTSCSFirstProcessControlCmd          = kAliHLTSCSFirstUserCmd + 0x00000100;


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3SCCOMMANDS_HPP_
