#ifndef _ALIL3SCCOMMANDPROCESSOR_HPP_
#define _ALIL3SCCOMMANDPROCESSOR_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCCommand.hpp"



// Commands below 255 are reserved for system commands.
// Only commands above 255 are forwarded to SCCommandProcessor objects.

class AliHLTSCCommandProcessor
    {
    public:

	virtual ~AliHLTSCCommandProcessor() {};

	virtual void ProcessCmd( AliHLTSCCommandStruct* cmd ) = 0;

    };


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif /* _ALIL3SCCOMMANDPROCESSOR_HPP_ */
