/************************************************************************
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#ifndef _ALIL3STATUS_HPP_
#define _ALIL3STATUS_HPP_

#include "AliHLTSCProcessStatus.hpp"
#include "AliHLTSCHLTStatus.hpp"
#include "AliHLTSCStatusHeader.hpp"
#include "MLUCMutex.hpp"
#include <sys/time.h>




struct AliHLTStatus
    {


	volatile AliHLTSCStatusHeaderStruct    fHeader;
	volatile AliHLTSCProcessStatusStruct   fProcessStatus;
	volatile AliHLTSCHLTStatusStruct       fHLTStatus;

	MLUCMutex fStateMutex;
	MLUCMutex fTimeMutex;
// XXXX TODO LOCKING XXXX
	MLUCMutex fStatusDataMutex;

	AliHLTStatus()
		{
// XXXX TODO LOCKING XXXX
		MLUCMutex::TLocker statusDataLock( fStatusDataMutex );
		MLUCMutex::TLocker stateLock( fStateMutex );
		AliHLTSCStatusHeader header;
		AliHLTSCProcessStatus process;
		AliHLTSCHLTStatus hlt;
#if 0
 		fHeader = *(header.GetData());
 		fProcessStatus = *(process.GetData());
 		fHLTStatus = *(hlt.GetData());
#endif
		memcpy( (void*)&fHeader, header.GetData(), header.GetData()->fLength );
		memcpy( (void*)&fProcessStatus, process.GetData(), process.GetData()->fLength );
		memcpy( (void*)&fHLTStatus, hlt.GetData(), hlt.GetData()->fLength );
		fHeader.fLength += fProcessStatus.fLength + fHLTStatus.fLength;
		fHeader.fStatusType = HLT_STATUS_TYPE;
		fHeader.fStatusStructCnt = 2;
		fHeader.fFirstStructOffset = (AliUInt64_t)( (unsigned long)&fProcessStatus - (unsigned long)&fHeader );
		fProcessStatus.fNextStructOffset = (AliUInt64_t)( (unsigned long)&fHLTStatus - (unsigned long)&fHeader );
		fProcessStatus.fTypeID = HLT_STATUS_PROCESS_TYPE;
		fHLTStatus.fNextStructOffset = 0;
		fHLTStatus.fTypeID = HLT_STATUS_HLT_TYPE;
		}

	void Touch()
		{
/*		struct timeval tv;
		MLUCMutex::TLocker lock( fTimeMutex );
		gettimeofday( &tv, NULL );
		fProcessStatus.fLastUpdateTime_s = tv.tv_sec;
		fProcessStatus.fLastUpdateTime_us = tv.tv_usec;*/
		}


    };


#endif // _ALIL3STATUS_HPP_


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

