/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#ifndef _ALIL3SCMSGS_HPP_
#define _ALIL3SCMSGS_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"


const AliUInt32_t kAliHLTSCNOPMsg              = 0;
const AliUInt32_t kAliHLTSCLookAtMeMsg         = 1;
const AliUInt32_t kAliHLTSCStatusQueryMsg      = 2;
const AliUInt32_t kAliHLTSCStatusReplyMsg      = 3;
const AliUInt32_t kAliHLTSCConnectedMsg        = 4;
const AliUInt32_t kAliHLTSCDisconnectedMsg     = 5;
const AliUInt32_t kAliHLTSCCommandMsg          = 6;



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3SCMSGS_HPP_
