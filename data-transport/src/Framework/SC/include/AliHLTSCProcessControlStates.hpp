#ifndef _ALIL3PROCESSCONTROLSTATES_HPP_
#define _ALIL3PROCESSCONTROLSTATES_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliHLTSCStates.hpp"

// const AliUInt32_t kAliHLTPCStartedState                   = 0x000000001;
// const AliUInt32_t kAliHLTPCConfiguringState               = 0x000000002;
// const AliUInt32_t kAliHLTPCConfiguredState                = 0x000000003;
// const AliUInt32_t kAliHLTPCWaitingState                   = 0x000000004;
// const AliUInt32_t kAliHLTPCBusyState                      = 0x000000005;



// const AliUInt32_t kAliHLTPCErrorFlag                      = 0x800000000;
// const AliUInt32_t kAliHLTPCEndingFlag                     = 0x400000000;

// const AliUInt32_t kAliHLTPCSubscribedFlag                 = 0x000000001;
// const AliUInt32_t kAliHLTPCAcceptingSubscriptionsFlag     = 0x000000002;
// const AliUInt32_t kAliHLTPCBridgeBoundFlag                = 0x000000004;
// const AliUInt32_t kAliHLTPCBridgeConnectedFlag            = 0x000000008;



// const AliUInt32_t kAliHLTPCState                   = 0x0000000;

// Common states (for bridges, and data components)
const AliUInt32_t kAliHLTPCStartedState                          = kAliHLTSCFirstProcessControlState + 0x000000001;
const AliUInt32_t kAliHLTPCConfiguredState                       = kAliHLTSCFirstProcessControlState + 0x000000002;
const AliUInt32_t kAliHLTPCConfigureErrorState                   = kAliHLTSCFirstProcessControlState + 0x000000003;
const AliUInt32_t kAliHLTPCAcceptingSubscriptionsState           = kAliHLTSCFirstProcessControlState + 0x000000004;
const AliUInt32_t kAliHLTPCStartAcceptingSubscriptionsState      = kAliHLTSCFirstProcessControlState + 0x000000005;
const AliUInt32_t kAliHLTPCSubscribedState                       = kAliHLTSCFirstProcessControlState + 0x000000006;
const AliUInt32_t kAliHLTPCUnsubscribingState                    = kAliHLTSCFirstProcessControlState + 0x000000007;
const AliUInt32_t kAliHLTPCAcceptingUnsubscribingState           = kAliHLTSCFirstProcessControlState + 0x000000008;
const AliUInt32_t kAliHLTPCUnsubscribingWithSubscribersState     = kAliHLTSCFirstProcessControlState + 0x000000009;
const AliUInt32_t kAliHLTPCSusbcribedStartAsState                = kAliHLTSCFirstProcessControlState + 0x00000000A;
const AliUInt32_t kAliHLTPCReadyState                            = kAliHLTSCFirstProcessControlState + 0x00000000A;
const AliUInt32_t kAliHLTPCWaitingState                          = kAliHLTSCFirstProcessControlState + 0x00000000B;
const AliUInt32_t kAliHLTPCBusyState                             = kAliHLTSCFirstProcessControlState + 0x00000000C;
const AliUInt32_t kAliHLTPCPausedState                           = kAliHLTSCFirstProcessControlState + 0x00000000D;
const AliUInt32_t kAliHLTPCSWithSubscribersStartASState          = kAliHLTSCFirstProcessControlState + 0x00000000E;
const AliUInt32_t kAliHLTPCWithSubscribersState                  = kAliHLTSCFirstProcessControlState + 0x00000000E;
const AliUInt32_t kAliHLTPCSubscribedWithSubscribersStateASState = kAliHLTSCFirstProcessControlState + 0x00000000F;
const AliUInt32_t kAliHLTPCSubscribedWithSubscribersState        = kAliHLTSCFirstProcessControlState + 0x00000000F;
const AliUInt32_t kAliHLTPCPublisherErrorState                   = kAliHLTSCFirstProcessControlState + 0x000000010;
const AliUInt32_t kAliHLTPCPublisherErrorAcceptingState          = kAliHLTSCFirstProcessControlState + 0x000000011;
const AliUInt32_t kAliHLTPCPublisherErrorProcessingState         = kAliHLTSCFirstProcessControlState + 0x000000012;
const AliUInt32_t kAliHLTPCPublisherErrorWSState                 = kAliHLTSCFirstProcessControlState + 0x000000013;

// Common bridge states
const AliUInt32_t kAliHLTPCBoundState                            = kAliHLTSCFirstProcessControlState + 0x000000014;
const AliUInt32_t kAliHLTPCConnectedState                        = kAliHLTSCFirstProcessControlState + 0x000000015;
const AliUInt32_t kAliHLTPCBindErrorState                        = kAliHLTSCFirstProcessControlState + 0x000000016;
const AliUInt32_t kAliHLTPCConnectionErrorState                  = kAliHLTSCFirstProcessControlState + 0x000000017;

// Publisher bridge states
const AliUInt32_t kAliHLTPCBoundAndAcceptingState                = kAliHLTSCFirstProcessControlState + 0x000000017;
const AliUInt32_t kAliHLTPCBoundWithSubscribersState             = kAliHLTSCFirstProcessControlState + 0x000000018;
const AliUInt32_t kAliHLTPCConnectedWithSubscribersState         = kAliHLTSCFirstProcessControlState + 0x000000019;
const AliUInt32_t kAliHLTPCBindErrorAcceptingState               = kAliHLTSCFirstProcessControlState + 0x00000001A;
const AliUInt32_t kAliHLTPCConnectionErrorAcceptingState         = kAliHLTSCFirstProcessControlState + 0x00000001B;

// Subscriber bridge states
const AliUInt32_t kAliHLTPCSubscribedAndBoundState               = kAliHLTSCFirstProcessControlState + 0x00000001C;
const AliUInt32_t kAliHLTPCPublisherErrorBoundState              = kAliHLTSCFirstProcessControlState + 0x00000001D;
const AliUInt32_t kAliHLTPCBindErrorSubscribedState              = kAliHLTSCFirstProcessControlState + 0x00000001E;
const AliUInt32_t kAliHLTPCPublisherErrorConnectedState          = kAliHLTSCFirstProcessControlState + 0x00000001F;
const AliUInt32_t kAliHLTPCConnectionErrorSubscribedState        = kAliHLTSCFirstProcessControlState + 0x000000020;


const AliUInt32_t kAliHLTPCStartedStateFlag                      = 0x00000001;
const AliUInt32_t kAliHLTPCConfiguredStateFlag                   = 0x00000002;
const AliUInt32_t kAliHLTPCSubscribedStateFlag                   = 0x00000004;
const AliUInt32_t kAliHLTPCAcceptingSubscriptionsStateFlag       = 0x00000008;
const AliUInt32_t kAliHLTPCWithSubscribersStateFlag              = 0x00000010;
const AliUInt32_t kAliHLTPCRunningStateFlag                      = 0x00000020;
const AliUInt32_t kAliHLTPCBusyStateFlag                         = 0x00000040;
const AliUInt32_t kAliHLTPCPausedStateFlag                       = 0x00000080;
const AliUInt32_t kAliHLTPCBoundStateFlag                        = 0x00000100;
const AliUInt32_t kAliHLTPCConnectedStateFlag                    = 0x00000200;
const AliUInt32_t kAliHLTPCChangingStateFlag                     = 0x10000000;
const AliUInt32_t kAliHLTPCNetworkErrorStateFlag                 = 0x20000000;
const AliUInt32_t kAliHLTPCProducerErrorStateFlag                = 0x40000000;
const AliUInt32_t kAliHLTPCConsumerErrorStateFlag                = 0x80000000;
const AliUInt32_t kAliHLTPCErrorStateFlag                        = 0xE0000000;


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3PROCESSCONTROLSTATES_HPP_
