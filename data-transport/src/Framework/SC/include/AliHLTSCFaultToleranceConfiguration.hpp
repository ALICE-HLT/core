#ifndef _ALIHLTSCFAULTTOLERANCECONFIGURATION_HPP_
#define _ALIHLTSCFAULTTOLERANCECONFIGURATION_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "BCLAbstrAddress.hpp"
#include "MLUCString.hpp"
#include <vector>
#include <istream>



/* Configuration file format: (quasi-BNF notation)

line: data-source-line | data-sink-line | target-line | start-nodes-line | start-ids-line | additional-nodes-line | process-file-line

data-source-line: 'source' number subnumber (<subscriber-control-address-URL> <publisher-control-address-URL> <publisher-msg-address-URL> <publisher-blobmsg-address-URL>)

data-sink-line: 'sink' number <publisher-control-address-URL> <subscriber-control-address-URL> <subscriber-msg-address-URL> <subscriber-blobmsg-address-URL>

target-line: 'target' <target-control-address-URL>

start-nodes-line: 'startnodes' number <node>+

start-ids-line: 'startids' number <id>+

additional-nodes-line: 'addnodes' number <node>+

process-file-line: 'processfile' <process-configuration-file-name>

*/

class AliHLTSCFaultToleranceConfiguration
    {
    public:

	struct DataSource
	    {

		DataSource()
			{
			fSubscriberControlAddress = NULL;
			//fPublisherControlAddress = NULL;
			fPublisherMsgAddress = NULL;
			fPublisherBlobMsgAddress = NULL;
			}

		unsigned long fSubNr;

		BCLAbstrAddressStruct* fSubscriberControlAddress;

		//BCLAbstrAddressStruct* fPublisherControlAddress;
		MLUCString fPublisherMsgAddressURL;
		BCLAbstrAddressStruct* fPublisherMsgAddress;
		MLUCString fPublisherBlobMsgAddressURL;
		BCLAbstrAddressStruct* fPublisherBlobMsgAddress;
	    };

	struct DataSink
	    {

		DataSink()
			{
			fPublisherControlAddress = NULL;
			//fSubscriberControlAddress = NULL;
			fSubscriberMsgAddress = NULL;
			fSubscriberBlobMsgAddress = NULL;
			}

		BCLAbstrAddressStruct* fPublisherControlAddress;

		//BCLAbstrAddressStruct* fSubscriberControlAddress;
		MLUCString fSubscriberMsgAddressURL;
		BCLAbstrAddressStruct* fSubscriberMsgAddress;
		MLUCString fSubscriberBlobMsgAddressURL;
		BCLAbstrAddressStruct* fSubscriberBlobMsgAddress;
	    };


	AliHLTSCFaultToleranceConfiguration();
	~AliHLTSCFaultToleranceConfiguration();

	bool ReadConfig( const char* filename );
	void SetDataPathNr( unsigned long dataPathNr );

	void GetSinkData( DataSink& sink );
	void GetSourceData( vector<DataSource>& sources );
	void GetStartNodes( vector<MLUCString>& nodes );
	void GetStartIDs( vector<MLUCString>& ids );
	void GetAddNodes( vector<MLUCString>& nodes );
	void GetTargets( vector<BCLAbstrAddressStruct*>& targets );

	void GetProcessFile( MLUCString& file );
	
	int SetStartNodes( vector<MLUCString>& nodes );
	int SetAddNodes( vector<MLUCString>& nodes );

    protected:
	
	bool ReadLine( istream& istr, MLUCString& line );
	bool GetToken( const char* text, MLUCString& token, const char*& pos );

	bool ReplaceName( MLUCString& url, BCLAbstrAddressStruct*& addr, const char* oldhostname, const char* newhostname );

	unsigned long fNr;

	vector<DataSource> fSources;
	DataSink fSink;
	vector<BCLAbstrAddressStruct*> fTargets;

	vector<MLUCString> fStartNodes;
	vector<MLUCString> fStartIDs;
	vector<MLUCString> fAddNodes;

	MLUCString fProcessFile;
    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTSCFAULTTOLERANCECONFIGURATION_HPP_
