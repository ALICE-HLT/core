#ifndef _ALIL3PROCESSCONTROLCMDS_HPP_
#define _ALIL3PROCESSCONTROLCMDS_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCCommands.hpp"

// Common commands
const AliUInt32_t kAliHLTPCNOPCmd                                = kAliHLTSCSFirstProcessControlCmd + 0x00000000;
const AliUInt32_t kAliHLTPCConfigureCmd                          = kAliHLTSCSFirstProcessControlCmd + 0x00000001;
const AliUInt32_t kAliHLTPCResetConfigurationCmd                 = kAliHLTSCSFirstProcessControlCmd + 0x00000002;
const AliUInt32_t kAliHLTPCUnconfigureCmd                        = kAliHLTSCSFirstProcessControlCmd + 0x00000003;
const AliUInt32_t kAliHLTPCAcceptSubscriptionsCmd                = kAliHLTSCSFirstProcessControlCmd + 0x00000004;
const AliUInt32_t kAliHLTPCStopSubscriptionsCmd                  = kAliHLTSCSFirstProcessControlCmd + 0x00000005;
const AliUInt32_t kAliHLTPCCancelSubscriptionsCmd                = kAliHLTSCSFirstProcessControlCmd + 0x00000006;
const AliUInt32_t kAliHLTPCSubscribeCmd                          = kAliHLTSCSFirstProcessControlCmd + 0x00000007;
const AliUInt32_t kAliHLTPCUnsubscribeCmd                        = kAliHLTSCSFirstProcessControlCmd + 0x00000008;
const AliUInt32_t kAliHLTPCAbortSubscriptionCmd                  = kAliHLTSCSFirstProcessControlCmd + 0x00000009;
const AliUInt32_t kAliHLTPCStartProcessingCmd                    = kAliHLTSCSFirstProcessControlCmd + 0x0000000A;
const AliUInt32_t kAliHLTPCPauseProcessingCmd                    = kAliHLTSCSFirstProcessControlCmd + 0x0000000B;
const AliUInt32_t kAliHLTPCResumeProcessingCmd                   = kAliHLTSCSFirstProcessControlCmd + 0x0000000C;
const AliUInt32_t kAliHLTPCStopProcessingCmd                     = kAliHLTSCSFirstProcessControlCmd + 0x0000000D;
const AliUInt32_t kAliHLTPCEndProgramCmd                         = kAliHLTSCSFirstProcessControlCmd + 0x0000000E;

// Bridge commands
const AliUInt32_t kAliHLTPCBindCmd                               = kAliHLTSCSFirstProcessControlCmd + 0x0000000F;
const AliUInt32_t kAliHLTPCUnbindCmd                             = kAliHLTSCSFirstProcessControlCmd + 0x00000010;
//const AliUInt32_t kAliHLTPCAbortBindCmd                          = kAliHLTSCSFirstProcessControlCmd + 0x00000011;  // == Unbind??
const AliUInt32_t kAliHLTPCConnectCmd                            = kAliHLTSCSFirstProcessControlCmd + 0x00000012;
const AliUInt32_t kAliHLTPCDisconnectCmd                         = kAliHLTSCSFirstProcessControlCmd + 0x00000013;
//const AliUInt32_t kAliHLTPCAbortConnectCmd                       = kAliHLTSCSFirstProcessControlCmd + 0x00000014;  // == Disconnect??
const AliUInt32_t kAliHLTPCReconnectCmd                          = kAliHLTSCSFirstProcessControlCmd + 0x00000015;
const AliUInt32_t kAliHLTPCNewConnectionCmd                      = kAliHLTSCSFirstProcessControlCmd + 0x00000016;
const AliUInt32_t kAliHLTPCNewRemoteAddressesCmd                 = kAliHLTSCSFirstProcessControlCmd + 0x00000017;
const AliUInt32_t kAliHLTPCPURGEALLEVENTSCmd                     = kAliHLTSCSFirstProcessControlCmd + 0x00000018;

const AliUInt32_t kAliHLTPCSetPathStateCmd                       = kAliHLTSCSFirstProcessControlCmd + 0x00000019;

const AliUInt32_t kAliHLTPCPublisherPinEventsCmd                 = kAliHLTSCSFirstProcessControlCmd + 0x0000001A;
const AliUInt32_t kAliHLTPCPublisherUnpinEventsCmd               = kAliHLTSCSFirstProcessControlCmd + 0x0000001B;

const AliUInt32_t kAliHLTPCRemoveSubscriberCmd                   = kAliHLTSCSFirstProcessControlCmd + 0x0000001C;
const AliUInt32_t kAliHLTPCCancelSubscriberCmd                   = kAliHLTSCSFirstProcessControlCmd + 0x0000001D;

const AliUInt32_t kAliHLTPCSimulateErrorCmd                      = kAliHLTSCSFirstProcessControlCmd + 0x0000001E;

const AliUInt32_t kAliHLTPCAddSubscriberCmd                      = kAliHLTSCSFirstProcessControlCmd + 0x0000001F;
const AliUInt32_t kAliHLTPCDelSubscriberCmd                      = kAliHLTSCSFirstProcessControlCmd + 0x00000020;

const AliUInt32_t kAliHLTPCAddPublisherCmd                       = kAliHLTSCSFirstProcessControlCmd + 0x00000021;
const AliUInt32_t kAliHLTPCDelPublisherCmd                       = kAliHLTSCSFirstProcessControlCmd + 0x00000022;

const AliUInt32_t kAliHLTPCEnableSubscriberCmd                   = kAliHLTSCSFirstProcessControlCmd + 0x00000023;
const AliUInt32_t kAliHLTPCDisableSubscriberCmd                  = kAliHLTSCSFirstProcessControlCmd + 0x00000024;

const AliUInt32_t kAliHLTPCEnablePublisherCmd                    = kAliHLTSCSFirstProcessControlCmd + 0x00000025;
const AliUInt32_t kAliHLTPCDisablePublisherCmd                   = kAliHLTSCSFirstProcessControlCmd + 0x00000026;

const AliUInt32_t kAliHLTPCInitiateReconfigureEventCmd           = kAliHLTSCSFirstProcessControlCmd + 0x00000027;

const AliUInt32_t kAliHLTPCDumpEventsCmd                         = kAliHLTSCSFirstProcessControlCmd + 0x00000028;

const AliUInt32_t kAliHLTPCInitiateUpdateDCSEventCmd             = kAliHLTSCSFirstProcessControlCmd + 0x00000029;

const AliUInt32_t kAliHLTPCDumpEventAccountingCmd                = kAliHLTSCSFirstProcessControlCmd + 0x0000002A;

const AliUInt32_t kAliHLTPCSetSubsystemVerbosityCmd              = kAliHLTSCSFirstProcessControlCmd + 0x0000002B;

const AliUInt32_t kAliHLTPCDumpEventMetaDataCmd                  = kAliHLTSCSFirstProcessControlCmd + 0x0000002C;

const AliUInt32_t kAliHLTPCInterruptConnectionCmd                = kAliHLTSCSFirstProcessControlCmd + 0x0000002D;

const AliUInt32_t kAliHLTSCSLastProcessControlCmd                = kAliHLTSCSFirstProcessControlCmd + 0x0000002E;

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3PROCESSCONTROLCMDS_HPP_
