#ifndef _ALIL3BRIDGESTATUSDATA_HPP_
#define _ALIL3BRIDGESTATUSDATA_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#include "AliHLTBridgeStatus.hpp"
#include "AliHLTSCStatusHeader.hpp"
#include "AliHLTSCProcessStatus.hpp"
#include "AliHLTSCHLTStatus.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "MLUCMutex.hpp"
#include <sys/time.h>

#define ALIL3BRIDGESTATUSTYPE (((AliUInt64_t)'L3BR')<<32 | 'DGST')

struct AliHLTBridgeStatusData
    {
	volatile AliHLTSCStatusHeaderStruct    fHeader;
	volatile AliHLTSCProcessStatusStruct   fProcessStatus;
	volatile AliHLTBridgeStatusStruct      fBridgeStatus;
	volatile AliHLTSCHLTStatusStruct       fHLTStatus;

	MLUCMutex fStateMutex;
	MLUCMutex fTimeMutex;
// XXXX TODO LOCKING XXXX
	MLUCMutex fStatusDataMutex;

	AliHLTBridgeStatusData()
		{
// XXXX TODO LOCKING XXXX
		MLUCMutex::TLocker statusDataLock( fStatusDataMutex );
		MLUCMutex::TLocker stateLock( fStateMutex );
		AliHLTSCStatusHeader header;
		AliHLTSCProcessStatus process;
		AliHLTBridgeStatus brdg;
		AliHLTSCHLTStatus hlt;
// 		fHeader = *(header.GetData());
// 		fProcessStatus = *(process.GetData());
// 		fBridgeStatus = *(brdg.GetData());
		memcpy( (void*)&fHeader, header.GetData(),header.GetData()->fLength );
		memcpy( (void*)&fProcessStatus, process.GetData(), process.GetData()->fLength );
		memcpy( (void*)&fBridgeStatus, brdg.GetData(), brdg.GetData()->fLength );
		memcpy( (void*)&fHLTStatus, hlt.GetData(), hlt.GetData()->fLength );
		//fHeader.fLength += fProcessStatus.fLength + fBridgeStatus.fLength;
		fHeader.fLength += fProcessStatus.fLength + fBridgeStatus.fLength + fHLTStatus.fLength;
	        fHeader.fStatusType = ALIL3BRIDGESTATUSTYPE;
		fHeader.fStatusStructCnt = 3;
		fHeader.fFirstStructOffset = (AliUInt64_t)( (unsigned long)(&fProcessStatus) - (unsigned long)(&fHeader) );
		fProcessStatus.fNextStructOffset = (AliUInt64_t)( (unsigned long)(&fBridgeStatus) - (unsigned long)(&fHeader) );
		fProcessStatus.fTypeID = HLT_STATUS_PROCESS_TYPE;
		fBridgeStatus.fNextStructOffset = (AliUInt64_t)( (unsigned long)(&fHLTStatus) - (unsigned long)(&fHeader) );
		fBridgeStatus.fTypeID = HLT_STATUS_BRIDGE_TYPE;
		fHLTStatus.fNextStructOffset = 0;
		fHLTStatus.fTypeID = HLT_STATUS_HLT_TYPE;
		}

	void SetConnected( bool msgOrBlob, bool connected )
		{
		if ( msgOrBlob )
		    fBridgeStatus.fMsgConnected = (connected ? 1 : 0);
		else
		    fBridgeStatus.fBlobConnected = (connected ? 1 : 0);
		{
		MLUCMutex::TLocker lock( fStateMutex );
		if ( fBridgeStatus.fMsgConnected && fBridgeStatus.fBlobConnected )
		    fProcessStatus.fState |= kAliHLTPCConnectedStateFlag;
		else
		    fProcessStatus.fState &= ~kAliHLTPCConnectedStateFlag;
		}
		Touch();
		}

	void Touch()
		{
/*		struct timeval tv;
		gettimeofday( &tv, NULL );
		MLUCMutex::TLocker lock( fTimeMutex );
		fProcessStatus.fLastUpdateTime_s = tv.tv_sec;
		fProcessStatus.fLastUpdateTime_us = tv.tv_usec;*/
		}

    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3BRIDGESTATUSDATA_HPP_
