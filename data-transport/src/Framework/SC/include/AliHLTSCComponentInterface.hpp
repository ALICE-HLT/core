#ifndef _ALIHLTSCCOMPONENTINTERFACE_HPP_
#define _ALIHLTSCCOMPONENTINTERFACE_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCCommands.hpp"
#include "AliHLTSCStates.hpp"
#include "AliHLTSCTypes.hpp"
#include "AliHLTSCProcessControlCmds.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "MLUCString.hpp"
#include "AliHLTSCStatusHeader.hpp"

class AliHLTSCProcessStatusStruct;
class AliHLTSCHLTStatusStruct;

class AliHLTSCComponentInterface
    {
    public:

	static bool GetCommandName( AliUInt32_t command, MLUCString& name );

	static bool GetStatusName( const AliHLTSCStatusHeaderStruct* statusHeader, MLUCString& name );

	static bool GetStatusName( AliUInt64_t status, MLUCString& name );

	static bool GetTypeName( const AliHLTSCStatusHeaderStruct* statusHeader, MLUCString& name );

	static bool GetUpdateTime( const AliHLTSCStatusHeaderStruct* statusHeader, struct timeval& tv );

	static bool GetProcessStatus( const AliHLTSCStatusHeaderStruct* statusHeader, AliHLTSCProcessStatusStruct& status );

	static bool GetHLTStatus( const AliHLTSCStatusHeaderStruct* statusHeader, AliHLTSCHLTStatusStruct& status );

    protected:

	static bool CheckForHLTStatus( const AliHLTSCStatusHeaderStruct* statusHeader );
	static bool CheckForBridgeStatus( const AliHLTSCStatusHeaderStruct* statusHeader );

    private:
    };

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTSCCOMPONENTINTERFACE_HPP_
