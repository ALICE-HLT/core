/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCController.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTSCMessage.hpp"
#include "AliHLTSCMsgs.hpp"
#include "AliHLTSCSysCmds.hpp"
#include "BCLAddressLogger.hpp"
#include "BCLURLAddressDecoder.hpp"
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>


AliHLTSCController::AliHLTSCController():
    fMsgThread( this, &AliHLTSCController::MsgHandler )
    {
    pthread_mutex_init( &fComMutex, NULL );
    fMsgCom = NULL;
    fAddress = NULL;
    fIsBound = false;
    pthread_mutex_init( &fLAMHandlerMutex, NULL );
    pthread_mutex_init( &fComErrorLogSuppressionsMutex, NULL );
    fQuitMsgThread = false;
    fMsgThreadQuitted = true;
    fMsgRefNdx = 0;
#ifdef INITIAL_CONDITIONSEM_UNLOCK
    // Define this when condition semaphore objects should always be unlocked initially.
    fReplySignal.Unlock();
#endif
    fReplyTimeout = ~(AliUInt32_t)0;
    fSendTimeout = 1000; // millisecs
    }

AliHLTSCController::~AliHLTSCController()
    {
    pthread_mutex_destroy( &fComMutex );
    pthread_mutex_destroy( &fLAMHandlerMutex );
    }

int AliHLTSCController::Start()
    {
    fMsgThread.Start();
    return 0;
    }

int AliHLTSCController::Stop()
    {
    fQuitMsgThread = true;
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    gettimeofday( &start, NULL );
    while ( !fMsgThreadQuitted && deltaT<timeLimit )
	{
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( fMsgThreadQuitted )
	return 0;
    else
	return EBUSY;
    }

void AliHLTSCController::AddLAMHandler( AliHLTSCLAMHandler* handler )
    {
    if ( !handler )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::AddLAMHandler", "NULL LAM Handler" )
	    << "NULL pointer LAM handler passed." << ENDLOG;
	return;
	}
    int ret;
    ret = pthread_mutex_lock( &fLAMHandlerMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::AddLAMHandler", "Mutex lock error" )
	    << "Error locking LAM handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}

    fLAMHandlers.insert( fLAMHandlers.end(), handler );
    
    ret = pthread_mutex_unlock( &fLAMHandlerMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::AddLAMHandler", "Mutex lock error" )
	    << "Error locking LAM handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    }

void AliHLTSCController::DelLAMHandler( AliHLTSCLAMHandler* handler )
    {
    if ( !handler )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::DelLAMHandler", "NULL LAM Handler" )
	    << "NULL pointer LAM handler passed." << ENDLOG;
	return;
	}
    int ret;
    vector<AliHLTSCLAMHandler*>::iterator iter, end;
    ret = pthread_mutex_lock( &fLAMHandlerMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::DelLAMHandler", "Mutex lock error" )
	    << "Error locking LAM handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}

    // First find the handler...
    iter = fLAMHandlers.begin();
    end = fLAMHandlers.end();
    while ( iter != end )
	{
	if ( (*iter) == handler )
	    {
	    // ... and then remove it
	    fLAMHandlers.erase( iter );
	    break;
	    }
	iter++;
	}
    
    ret = pthread_mutex_unlock( &fLAMHandlerMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::DelLAMHandler", "Mutex unlock error" )
	    << "Error unlocking LAM handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    }

// int AliHLTSCController::SetComObject( BCLMessage* msgCom )
//     {
//     int ret, retval=0;
//     ret = pthread_mutex_lock( &fComMutex );
//     if ( ret )
// 	{
// 	LOG( AliHLTLog::kError, "AliHLTSCController::SetComObject", "Mutex lock error" )
// 	    << "Error locking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
// 	    << ret << ")." << ENDLOG;
// 	}
//     if ( fMsgCom && !msgCom )
// 	fMsgCom = NULL;
//     if ( msgCom )
// 	{
// 	if ( fMsgCom )
// 	    retval = EBUSY;
// 	else
// 	    fMsgCom = msgCom;
// 	}
//     ret = pthread_mutex_unlock( &fComMutex );
//     if ( ret )
// 	{
// 	LOG( AliHLTLog::kError, "AliHLTSCController::SetComObject", "Mutex unlock error" )
// 	    << "Error unlocking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
// 	    << ret << ")." << ENDLOG;
// 	}
//     return retval;
//     }


int AliHLTSCController::Bind( const char* addressURL, BCLErrorCallback* errorCallback )
    {
    int ret, retval;
    if ( fIsBound )
	{
	return EBUSY;
	}
    ret = pthread_mutex_lock( &fComMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::Bind", "Mutex lock error" )
	    << "Error locking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    bool isBlob = false;
    BCLCommunication* com;
    retval = BCLDecodeLocalURL( addressURL, com, fAddress, isBlob );
    if ( retval )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::Bind", "Address decoding error" )
	    << "Error decoding address URL '" << addressURL << "': " << strerror(retval)
	    << " (" << AliHLTLog::kDec << retval << ")." << ENDLOG;
	fAddress = NULL;
	ret = pthread_mutex_unlock( &fComMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSCController::Bind", "Mutex unlock error" )
		<< "Error unlocking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    }
	return retval;
	}
    if ( isBlob )
	{
	BCLFreeAddress( com, fAddress );
	fAddress = NULL;
	LOG( AliHLTLog::kError, "AliHLTSCController::Bind", "Blob address URL specified" )
	    << "Blob address URL '" << addressURL << "' specified. Need MSG address URL!"
	    << ENDLOG;
	ret = pthread_mutex_unlock( &fComMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSCController::Bind", "Mutex unlock error" )
		<< "Error unlocking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    }
	return EINVAL;
	}
    fMsgCom = (BCLMsgCommunication*)com;

    if ( errorCallback )
	fMsgCom->AddCallback( errorCallback );

    retval = fMsgCom->Bind( fAddress );
    if ( retval )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::Bind", "Bind Error" )
	    << "BCL msg com bind error: " << strerror(retval) << AliHLTLog::kDec
	    << " (" << retval << ")." << ENDLOG;
	BCLFreeAddress( com, fAddress );
	fAddress = NULL;
	fMsgCom = NULL;
	}
    else
	{
	LOG( AliHLTLog::kInformational, "AliHLTSCController::Bind", "Bind successfull" )
	    << "BCL msg com bind successfull." << ENDLOG;
	}
    fIsBound = true;
    ret = pthread_mutex_unlock( &fComMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::Bind", "Mutex unlock error" )
	    << "Error unlocking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    return retval;
    }

int AliHLTSCController::Unbind()
    {
    if ( !fIsBound )
	return 0;
    if ( !fMsgCom || !fAddress )
	return EFAULT;
    
    int ret, retval;
    ret = pthread_mutex_lock( &fComMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::Bind", "Mutex lock error" )
	    << "Error locking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    retval = fMsgCom->Unbind();
    if ( retval )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::Unbind", "Unbind Error" )
	    << "BCL msg com unbind error: " << strerror(retval) << AliHLTLog::kDec
	    << " (" << retval << ")." << ENDLOG;
	}
    BCLFreeAddress( fMsgCom, fAddress );
    fMsgCom = NULL;
    fAddress = NULL;
    fIsBound = false;
    ret = pthread_mutex_unlock( &fComMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::Bind", "Mutex unlock error" )
	    << "Error unlocking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    return retval;
    }


int AliHLTSCController::Connect( BCLAbstrAddressStruct* address, bool openOnDemand )
    {
    int ret,retval=0;
    bool logErrors = !SuppressComErrorLogs( address );
    ret = pthread_mutex_lock( &fComMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::Connect", "Mutex lock error" )
	    << "Error locking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    // First have the com object make a connection.
    if ( !fMsgCom )
	retval = ENODEV;
    else
	retval = fMsgCom->Connect( address, fSendTimeout, openOnDemand );
    if ( !retval )
	{
	// If this worked, we send a connect message to the other side.
	struct timeval now;
	gettimeofday( &now, NULL );
	AliHLTSCMessage msg;
	AliHLTSCMessageStruct* pmsg = msg.GetData();
	pmsg->fMsgType = kAliHLTSCConnectedMsg;
	pmsg->fRefNdx = GetNextRefNdx();
	if ( fMsgRefNdx == 0 )
	    pmsg->fRefNdx = ++fMsgRefNdx;
	pmsg->fTimestamp_s = now.tv_sec;
	pmsg->fTimestamp_us = now.tv_usec;
	retval = fMsgCom->Send( address, pmsg, fSendTimeout );
	if ( retval && logErrors )
	    {
	    LOGG( AliHLTLog::kError, "AliHLTSCController::Connect", "Send Error", tmpLog )
		<< "Error sending Connect message to address '";
		BCLAddressLogger::LogAddress( tmpLog, *address )
		    << "': " << strerror(retval) << " (" << AliHLTLog::kDec
		    << retval << ")." << ENDLOG;
	    }
	}
    else if ( logErrors )
	{
	LOGG( AliHLTLog::kError, "AliHLTSCController::Connect", "Connection Error", tmpLog )
	    << "Error connecting to address '";
	    BCLAddressLogger::LogAddress( tmpLog, *address )
		<< "': " << strerror(retval) << " (" << AliHLTLog::kDec
		<< retval << ")." << ENDLOG;
	}
    
    ret = pthread_mutex_unlock( &fComMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::Connect", "Mutex unlock error" )
	    << "Error unlocking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    return retval;
    }
	
int AliHLTSCController::Disconnect( BCLAbstrAddressStruct* address )
    {
    int ret,retval=0, retval2;
    bool logErrors = !SuppressComErrorLogs( address );
    ret = pthread_mutex_lock( &fComMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::Disconnect", "Mutex lock error" )
	    << "Error locking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    if ( !fMsgCom )
	retval = ENXIO;
    else
	{
	// Send a disconnect message to the other side...
	struct timeval now;
	gettimeofday( &now, NULL );
	AliHLTSCMessage msg;
	AliHLTSCMessageStruct* pmsg = msg.GetData();
	pmsg->fMsgType = kAliHLTSCDisconnectedMsg;
	pmsg->fRefNdx = GetNextRefNdx();
	pmsg->fTimestamp_s = now.tv_sec;
	pmsg->fTimestamp_us = now.tv_usec;
	retval = fMsgCom->Send( address, pmsg, fSendTimeout );
	if ( retval && logErrors )
	    {
	    LOGG( AliHLTLog::kError, "AliHLTSCController::Disconnect", "Send Error", tmpLog )
		<< "Error sending Disconnect message to address '";
		BCLAddressLogger::LogAddress( tmpLog, *address )
		    << "': " << strerror(retval) << " (" << AliHLTLog::kDec
		    << retval << ")." << ENDLOG;
	    }
	// ... and disconnect
	retval2 = fMsgCom->Disconnect( address, fSendTimeout );
	if ( retval2 && logErrors )
	    {
	    LOGG( AliHLTLog::kError, "AliHLTSCController::Disconnect", "Disconnect Error", tmpLog )
		<< "Error disconnecting from address '";
		BCLAddressLogger::LogAddress( tmpLog, *address )
		    << "': " << strerror(retval) << " (" << AliHLTLog::kDec
		    << retval << ")." << ENDLOG;
	    retval = retval2;
	    }
	}
    ret = pthread_mutex_unlock( &fComMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::Disonnect", "Mutex unlock error" )
	    << "Error unlocking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    return retval;
    }

int AliHLTSCController::SetVerbosity( BCLAbstrAddressStruct* address, AliUInt32_t verbosity )
    {
    AliHLTSCCommand cmd;
    AliHLTSCCommandStruct* cmdS = cmd.GetData();
    cmdS->fCmd = kAliHLTSCSetLogLevelSysCmd;
    cmdS->fLength += sizeof(AliUInt32_t);
    cmdS->fParam0 = verbosity;
    cmdS->fDataCnt = 0;
    int ret;
    ret = SendCommand( address, cmdS );
    return ret;
    }
	
int AliHLTSCController::SendCommand( BCLAbstrAddressStruct* address, const AliHLTSCCommandStruct* cmd )
    {
    int ret, retval;
    bool logErrors = !SuppressComErrorLogs( address );
    ret = pthread_mutex_lock( &fComMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::SendCommand(BCLAbstrAddressStruct*,const AliHLTSCCommandStruct*)", "Mutex lock error" )
	    << "Error locking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}

    if ( !fMsgCom)
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::SendCommand(BCLAbstrAddressStruct*,const AliHLTSCCommandStruct*)", "No communication object" )
	    << "no communication object available." << ENDLOG;
	ret = pthread_mutex_unlock( &fComMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSCController::SendCommand(BCLAbstrAddressStruct*,const AliHLTSCCommandStruct*)", "Mutex unlock error" )
		<< "Error unlocking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    }
	return ENODEV;
	}
    
    AliHLTSCMessage scMsg;
    AliHLTSCCommand contr;
    AliHLTSCCommandStruct* pContr;
    AliUInt8_t* dat;
    AliUInt32_t cmdLen;
    // Build the message to send, by copying the message proper and
    // the command contents into one place.
    BCLNetworkData::Transform( cmd->fLength, cmdLen, cmd->fDataFormats[ kCurrentByteOrderIndex ], kNativeByteOrder );
    dat = new AliUInt8_t[ scMsg.GetData()->fLength + cmdLen ];
    if ( !dat )
	{
	if ( logErrors )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSCController::SendCommand(BCLAbstrAddressStruct*,const AliHLTSCCommandStruct*)", "Out of memory" )
		<< "Out of memory allocating command message of " << AliHLTLog::kDec
		<< scMsg.GetData()->fLength + cmdLen << " (0x" << scMsg.GetData()->fLength + cmdLen
		<< ") bytes." << ENDLOG;
	    }
	ret = pthread_mutex_unlock( &fComMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSCController::SendCommand(BCLAbstrAddressStruct*,const AliHLTSCCommandStruct*)", "Mutex unlock error" )
		<< "Error unlocking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    }
	return ENOMEM;
	}
    
    memcpy( dat, scMsg.GetData(), scMsg.GetData()->fLength );
    ((AliHLTSCMessageStruct*)dat)->fLength += cmdLen;
    ((AliHLTSCMessageStruct*)dat)->fMsgType = kAliHLTSCCommandMsg;
    pContr = (AliHLTSCCommandStruct*)( dat + scMsg.GetData()->fLength );
    memcpy( pContr, cmd, cmdLen );
    contr.Adopt( pContr );
    retval = fMsgCom->Send( address, (BCLMessageStruct*)dat, fSendTimeout );
    if ( retval && logErrors )
	{
	LOGG( AliHLTLog::kError, "AliHLTSCController::SendCommand(BCLAbstrAddressStruct*,const AliHLTSCCommandStruct*)", "Send Error", tmpLog )
	    << "Error sending command to address' ";
	    BCLAddressLogger::LogAddress( tmpLog, *address )
		<< "': " << strerror(retval) << " (" << AliHLTLog::kDec
		<< retval << ")." << ENDLOG;
	}
    ret = pthread_mutex_unlock( &fComMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::SendCommand(BCLAbstrAddressStruct*,const AliHLTSCCommandStruct*)", "Mutex unlock error" )
	    << "Error unlocking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    return retval;
    }
	
int AliHLTSCController::GetStatus( BCLAbstrAddressStruct* address, AliHLTSCStatusHeaderStruct*& status )
    {
    int ret, retval;
    bool abort = false;
    status = NULL;
    bool logErrors = !SuppressComErrorLogs( address );
    if ( !fMsgCom )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::GetStatus(BCLAbstrAddressStruct*,AliHLTSCStatusHeaderStruct*&)", "No com object" )
	    << "No message communication object specified (NULL pointer)." << ENDLOG;
	return ENODEV;
	}
    ret = pthread_mutex_lock( &fComMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::GetStatus(BCLAbstrAddressStruct*,AliHLTSCStatusHeaderStruct*&)", "Mutex lock error" )
	    << "Error locking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    
    // First send out a status query.
    AliHLTSCMessage msg;
    msg.GetData()->fMsgType = kAliHLTSCStatusQueryMsg;
    msg.GetData()->fRefNdx = GetNextRefNdx();
    retval = fMsgCom->Send( address, msg.GetData(), fSendTimeout );
    if ( retval )
	{
	if ( logErrors )
	    {
	    LOGG( AliHLTLog::kError, "AliHLTSCController::GetStatus(BCLAbstrAddressStruct*,AliHLTSCStatusHeaderStruct*&)", "Send Error", tmpLog )
		<< "Error sending status request to address' ";
	    BCLAddressLogger::LogAddress( tmpLog, *address )
		<< "': " << strerror(retval) << " (" << AliHLTLog::kDec
		<< retval << ")." << ENDLOG;
	    }
	abort = true;
	}
    ret = pthread_mutex_unlock( &fComMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::GetStatus(BCLAbstrAddressStruct*,AliHLTSCStatusHeaderStruct*&)", "Mutex unlock error" )
	    << "Error unlocking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    AliHLTSCMessageStruct* msgS = NULL;
    if ( !abort )
	{
	// Now wait for the proper reply for our message
	retval = WaitForMessage( msgS, msg.GetData()->fRefNdx, fReplyTimeout );
	if ( retval )
	    {
	    static int errors = 0;
	    if ( errors++ == 3 && logErrors )
		{
		LOG( AliHLTLog::kError, "AliHLTSCController::GetStatus(BCLAbstrAddressStruct*,AliHLTSCStatusHeaderStruct*&)", "Message wait error" )
		    << "Error waiting for reply message with reference index " << AliHLTLog::kDec << msg.GetData()->fRefNdx << ": " 
		    << strerror(retval) << " (" << retval << ")." << ENDLOG;
		}
	    abort = true;
	    }
	}

    AliHLTSCMessage tmpMsg;
    if ( !abort )
	{
	tmpMsg.SetTo( msgS );
	// First copy the message stuff we need to know about and be sure that it has our byte order...
	// Is it really a reply??
	if ( tmpMsg.GetData()->fMsgType != kAliHLTSCStatusReplyMsg )
	    {
	    if ( logErrors )
		{
		LOG( AliHLTLog::kError, "AliHLTSCController::GetStatus(BCLAbstrAddressStruct*,AliHLTSCStatusHeaderStruct*&)", "Wrong reply message" )
		    << "Reply message for reference index " << AliHLTLog::kDec << msg.GetData()->fRefNdx << " has wrong message type " 
		    << AliHLTLog::kDec << tmpMsg.GetData()->fMsgType << " (0x" << AliHLTLog::kHex << tmpMsg.GetData()->fMsgType << ") expected kAliHLTSCStatusReplyMsg "
		    << AliHLTLog::kDec << kAliHLTSCStatusReplyMsg << " (0x" << AliHLTLog::kHex << kAliHLTSCStatusReplyMsg << ")." << ENDLOG;
		}
	    abort = true;
	    retval = EBADMSG;
	    }
	}
    AliHLTSCStatusHeader statusHeader;
    if ( !abort )
	{
	// This is really a reply, now, check the size.
	if ( tmpMsg.GetData()->fLength < msg.GetData()->fLength + statusHeader.GetData()->fLength )
	    {
	    if ( logErrors )
		{
		LOG( AliHLTLog::kError, "AliHLTSCController::GetStatus(BCLAbstrAddressStruct*,AliHLTSCStatusHeaderStruct*&)", "Wrong reply message size" )
		    << "Reply message for reference index " << AliHLTLog::kDec << msg.GetData()->fRefNdx << " has wrong size of "
		    << tmpMsg.GetData()->fLength << " bytes. Expected at least " << msg.GetData()->fLength + statusHeader.GetData()->fLength
		    << " bytes." << ENDLOG;
		}
	    abort = true;
	    retval = EMSGSIZE;
	    }
	}
    AliHLTSCStatusHeaderStruct *shs = NULL, *shsnew = NULL;
    if ( !abort )
	{
	// Now determine the size needed and try to allocate space to copy it into.
	//shs = (AliHLTSCStatusHeaderStruct*)(((AliUInt8_t*)msgS)+tmpMsg.GetData()->fLength);
	shs = (AliHLTSCStatusHeaderStruct*)(msgS->fData);
	statusHeader.SetTo( shs );
	shsnew = (AliHLTSCStatusHeaderStruct*)new AliUInt8_t[ statusHeader.GetData()->fLength ];
	if ( !shsnew )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSCController::GetStatus(BCLAbstrAddressStruct*,AliHLTSCStatusHeaderStruct*&)", "Out of memory" )
		<< "Out of memory trying to allocate status data of " << AliHLTLog::kDec << statusHeader.GetData()->fLength << " bytes."
		<< ENDLOG;
	    abort = true;
	    retval = ENOMEM;
	    }
	}
    if ( !abort )
	{
	// Space is allocated, now copy it.
	memcpy( shsnew, shs, statusHeader.GetData()->fLength );
	status = shsnew;
	LOG( AliHLTLog::kDebug, "AliHLTSCController::GetStatus(BCLAbstrAddressStruct*,AliHLTSCStatusHeaderStruct*&)", "Status reply received" )
	    << "Received status reply." << ENDLOG;
	}

    // And release everything that has been allocated and is not needed any more.
    if ( msgS && fMsgCom )
	{
	ret = pthread_mutex_lock( &fComMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSCController::GetStatus(BCLAbstrAddressStruct*,AliHLTSCStatusHeaderStruct*&)", "Mutex lock error" )
		<< "Error locking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    }
	fMsgCom->ReleaseMsg( msgS );
	ret = pthread_mutex_unlock( &fComMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSCController::GetStatus(BCLAbstrAddressStruct*,AliHLTSCStatusHeaderStruct*&)", "Mutex unlock error" )
		<< "Error unlocking com object mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    }
	}

    // The end
    return retval;
    }
	
int AliHLTSCController::FreeStatus( AliHLTSCStatusHeaderStruct* status )
    {
    if ( status )
 	{
 	delete [] (AliUInt8_t*)status;
 	return 0;
 	}
    return EFAULT;
    }

void AliHLTSCController::SuppressComErrorLogs( BCLAbstrAddressStruct* address, unsigned long timeout_us )
    {
    if ( !fMsgCom )
	return;
    struct timeval now;
    bool remove = false;
    if ( timeout_us == 0 )
	{
	now.tv_sec = now.tv_usec = 0;
	}
    else if ( timeout_us == 0xFFFFFFFF )
	{
	now.tv_sec = now.tv_usec = 0;
	remove = true;
	}
    else
	{
	gettimeofday( &now, NULL );
	now.tv_usec += timeout_us;
	now.tv_sec += (now.tv_usec / 1000000 );
	now.tv_usec %= 1000000;
	}
    pthread_mutex_lock( &fComErrorLogSuppressionsMutex );
    vector<TComErrorLogSuppressionData>::iterator iter, end;
    iter = fComErrorLogSuppressions.begin();
    end = fComErrorLogSuppressions.end();
    while ( iter != end )
	{
	if ( fMsgCom->AddressEquality( iter->fAddress, address ) )
	    {
	    if ( remove )
		{
		fMsgCom->DeleteAddress( iter->fAddress );
		fComErrorLogSuppressions.erase( iter );
		break;
		}
	    else if ( ( iter->fUntil.tv_sec==0 && iter->fUntil.tv_usec==0 ) ||
		 iter->fUntil.tv_sec<now.tv_sec || 
		 (iter->fUntil.tv_sec==now.tv_sec && iter->fUntil.tv_usec<now.tv_usec ) )
		{
		iter->fUntil.tv_sec = now.tv_sec;
		iter->fUntil.tv_usec = now.tv_usec;
		break;
		}
	    }
	iter++;
	}
    if ( iter == end && !remove )
	{
	TComErrorLogSuppressionData celsd;
	//celsd.fAddressURL = addressURL;
	celsd.fAddress = fMsgCom->NewAddress();
	if ( celsd.fAddress )
	    {
	    memcpy( celsd.fAddress, address, fMsgCom->GetAddressLength() );
	    celsd.fUntil = now;
	    fComErrorLogSuppressions.push_back( celsd );
	    }
	}
    pthread_mutex_unlock( &fComErrorLogSuppressionsMutex );
    }



void AliHLTSCController::MsgHandler()
    {
    if ( !fMsgCom )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::MsgHandler", "No com object" )
	    << "No com object specified (NULL pointer)." << ENDLOG;
	return;
	}
    BCLAbstrAddressStruct* sender;
    sender = fMsgCom->NewAddress();
    if ( !sender )
	{
	LOG( AliHLTLog::kError, "AliHLTSCController::MsgHandler", "Out of memory" )
	    << "Out of memory trying to allocate new com address." << ENDLOG;
	return;
	}
    int ret;
    fMsgThreadQuitted = false;
    BCLMessageStruct* message = NULL;
    LOG( AliHLTLog::kDebug, "AliHLTSCController::MsgHandler", "Loop starting" )
	<< "Starting message loop..." << ENDLOG;
    while ( !fQuitMsgThread )
	{
	ret = fMsgCom->Receive( sender, message, 10 ); // Start with a 10ms timeout.
	if ( ret == ETIMEDOUT )
	    {
	    continue;
	    }
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSCController::MsgHandler", "Receive error" )
		<< "Error trying to receive message: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    if ( ret == ENXIO )
		{
		LOG( AliHLTLog::kError, "AliHLTSCController::MsgHandler", "Receive error" )
		    << "Aborting..." << ENDLOG;
		return;
		}
	    continue;
	    }
	if ( !message )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSCController::MsgHandler", "message error" )
		<< "invalid message (NULL pointer) found..:" << ENDLOG;
	    continue;
	    }
	LOG( AliHLTLog::kDebug, "AliHLTSCController::MsgHandler", "Message received" )
	    << "Message '" << *message << "' received." << ENDLOG;
	switch ( message->fMsgType )
	    {
	    case kAliHLTSCStatusReplyMsg:
		LOG( AliHLTLog::kDebug, "AliHLTSCController::MsgHandler", "Replay Message received" )
		    << "Reply Message with reference index " << AliHLTLog::kDec << ((AliHLTSCMessageStruct*)message)->fRefNdx
		    << " received." << ENDLOG;
		fReplySignal.Lock();
		fReplyMessages.insert( fReplyMessages.end(), (AliHLTSCMessageStruct*)message );
		fReplySignal.Unlock();
		fReplySignal.Signal();
		break;
	    case kAliHLTSCLookAtMeMsg:
		{
		vector<AliHLTSCLAMHandler*>::iterator iter, end;
		ret = pthread_mutex_lock( &fLAMHandlerMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSCController::MsgHandler", "Mutex lock error" )
			<< "Error locking LAM handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
			<< ret << ")." << ENDLOG;
		    }
		
		iter = fLAMHandlers.begin();
		end = fLAMHandlers.end();
		while ( iter != end )
		    {
		    if ( *iter )
			(*iter)->LookAtMe( sender );
		    iter++;
		    }

		ret = pthread_mutex_unlock( &fLAMHandlerMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSCController::MsgHandler", "Mutex unlock error" )
			<< "Error unlocking LAM handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
			<< ret << ")." << ENDLOG;
		    }
		break;
		}
	    default:
		LOG( AliHLTLog::kWarning, "AliHLTSCController::MsgHandler", "Unknown Message received" )
		    << "Received message with unknown type " << AliHLTLog::kDec << message->fMsgType
		    << "." << ENDLOG;
		break;
	    }
	}
    LOG( AliHLTLog::kDebug, "AliHLTSCController::MsgHandler", "Loop ending" )
	<< "Leaving message loop..." << ENDLOG;
    }

int AliHLTSCController::WaitForMessage( AliHLTSCMessageStruct*& message, AliUInt32_t refNdx, AliUInt32_t timeout_ms )
    {
    bool dotimeout = timeout_ms != ~(AliUInt32_t)0;
    struct timeval start, stop;
    if ( dotimeout )
	gettimeofday( &start, NULL );
    AliUInt32_t dt=0;
    bool found = true;
    while ( !dotimeout || dt<timeout_ms )
	{
	LOG( AliHLTLog::kDebug, "AliHLTSCController::WaitForMessage", "Waiting for message" )
	    << "Waiting for message." << ENDLOG;
	fReplySignal.Lock();
	if ( !found )
	    {
	    if ( dotimeout )
		fReplySignal.WaitTime( timeout_ms );
	    else
		fReplySignal.Wait();
	    }
	LOG( AliHLTLog::kDebug, "AliHLTSCController::WaitForMessage", "Checking for message" )
	    << "Checking for message with reference index " << AliHLTLog::kDec << refNdx << "." << ENDLOG;
	found = false;
	vector<AliHLTSCMessageStruct*>::iterator iter, end;
	iter = fReplyMessages.begin();
	end = fReplyMessages.end();
	while ( iter != end )
	    {
	    if ( (*iter)->fRefNdx == refNdx )
		{
		found = true;
		message = *iter;
		fReplyMessages.erase( iter );
		break;
		}
	    iter++;
	    }
	fReplySignal.Unlock();
	if ( found )
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTSCController::WaitForMessage", "Message found" )
		<< "Message with reference index " << AliHLTLog::kDec << refNdx << " found." << ENDLOG;
	    return 0;
	    }
	if ( dotimeout )
	    {
	    gettimeofday( &stop, NULL );
	    dt = (stop.tv_sec-start.tv_sec)*1000 + (stop.tv_usec-start.tv_usec)/1000;
	    }
	}
    return ETIMEDOUT;
    }


bool AliHLTSCController::SuppressComErrorLogs( BCLAbstrAddressStruct* address )
    {
    if ( !fMsgCom )
	return false;
    struct timeval now;
    gettimeofday( &now, NULL );
    pthread_mutex_lock( &fComErrorLogSuppressionsMutex );
    vector<TComErrorLogSuppressionData>::iterator iter, end;
    iter = fComErrorLogSuppressions.begin();
    end = fComErrorLogSuppressions.end();
    while ( iter != end )
	{
	if ( fMsgCom->AddressEquality( iter->fAddress, address ) )
	    {
	    if ( ( iter->fUntil.tv_sec==0 && iter->fUntil.tv_usec==0 ) ||
		 iter->fUntil.tv_sec>now.tv_sec || 
		 (iter->fUntil.tv_sec==now.tv_sec && iter->fUntil.tv_usec>now.tv_usec ) )
		{
		pthread_mutex_unlock( &fComErrorLogSuppressionsMutex );
		return true;
		}
	    else if ( iter->fUntil.tv_sec<now.tv_sec || 
		 (iter->fUntil.tv_sec==now.tv_sec && iter->fUntil.tv_usec<now.tv_usec ) )
		{
		fMsgCom->DeleteAddress( iter->fAddress );
		fComErrorLogSuppressions.erase( iter );
		pthread_mutex_unlock( &fComErrorLogSuppressionsMutex );
		return false;
		}
	    }
	iter++;
	}
    pthread_mutex_unlock( &fComErrorLogSuppressionsMutex );
    return false;
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
