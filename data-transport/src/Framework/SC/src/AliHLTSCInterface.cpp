/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCInterface.hpp"
#include "AliHLTSCMsgs.hpp"
#include "AliHLTSCMessage.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTStackTrace.hpp"
#include "AliHLTSCSysCmds.hpp"
#include "AliHLTSCComponentInterface.hpp"
#include "BCLURLAddressDecoder.hpp"
#include "BCLAddressLogger.hpp"
#include <cstring>
#include <cerrno>
#include <iostream>
#include <cstdlib>


AliHLTSCInterface::AliHLTSCInterface():
    fListenThread( this, &AliHLTSCInterface::ListenHandler ),
    fCmdHandlerThread( this, &AliHLTSCInterface::CmdHandler )
    {
    fAddress = NULL;
    fMsgCom = NULL;
    fStatusData = NULL;
// XXXX TODO LOCKING XXXX
    fStatusDataMutex = NULL;
    fIsBound = false;
    fQuitCmdHandler = fQuitListenHandler = false;
    fCmdHandlerQuitted = fListenHandlerQuitted = true;
    pthread_mutex_init( &fCmdProcessorMutex, NULL );
    pthread_mutex_init( &fConnectedAddressMutex, NULL );
    pthread_mutex_init( &fSendMutex, NULL );
    fSendTimeout = 1000;
    }

AliHLTSCInterface::~AliHLTSCInterface()
    {
    Unbind();
    pthread_mutex_destroy( &fCmdProcessorMutex );
    pthread_mutex_destroy( &fConnectedAddressMutex );
    pthread_mutex_destroy( &fSendMutex );
    }
	
int AliHLTSCInterface::Start()
    {
    if ( !fIsBound )
	return ENXIO;
    fQuitListenHandler = false;
    fListenThread.Start();
    fQuitCmdHandler = false;
    fCmdHandlerThread.Start();
    return 0;
    }

int AliHLTSCInterface::Stop()
    {
    fQuitListenHandler = true;
    fQuitCmdHandler = true;
    fCmdSignal.Signal();
    // XXX
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 2000000;
    gettimeofday( &start, NULL );
    while ( (!fListenHandlerQuitted || !fCmdHandlerQuitted) && deltaT<timeLimit )
	{
	usleep( 100000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( !fListenHandlerQuitted || !fCmdHandlerQuitted )
	return EBUSY;
    return 0;
    }

void AliHLTSCInterface::SetStatusData( AliHLTSCStatusHeaderStruct volatile* statusData )
    {
    fStatusData = statusData;
    }

// XXXX TODO LOCKING XXXX
void AliHLTSCInterface::SetStatusDataMutex( MLUCMutex* statusDataMutex )
    {
    fStatusDataMutex = statusDataMutex;
    }
    


void AliHLTSCInterface::AddCommandProcessor( AliHLTSCCommandProcessor* cmdProc )
    {
    int ret;
    ret = pthread_mutex_lock( &fCmdProcessorMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCInterface::AddCommandProcessor", "Mutex lock error" )
	    << "Error locking command processor mutex: " << strerror(ret) << AliHLTLog::kDec
	    << " (" << ret << "). Continuing..." << ENDLOG;
	}

    fCmdProcessors.insert( fCmdProcessors.end(), cmdProc );

    ret = pthread_mutex_unlock( &fCmdProcessorMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCInterface::AddCommandProcessor", "Mutex unlock error" )
	    << "Error unlocking command processor mutex: " << strerror(ret) << AliHLTLog::kDec
	    << " (" << ret << "). Continuing..." << ENDLOG;
	}
    }

void AliHLTSCInterface::DelCommandProcessor( AliHLTSCCommandProcessor* cmdProc )
    {
    int ret;
    vector<AliHLTSCCommandProcessor*>::iterator cmdIter, cmdEnd;
    bool found = false;
    ret = pthread_mutex_lock( &fCmdProcessorMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCInterface::DelCommandProcessor", "Mutex lock error" )
	    << "Error locking command processor mutex: " << strerror(ret) << AliHLTLog::kDec
	    << " (" << ret << "). Continuing..." << ENDLOG;
	}

    cmdIter = fCmdProcessors.begin();
    cmdEnd = fCmdProcessors.end();
    while ( cmdIter != cmdEnd )
	{
	if ( *cmdIter == cmdProc )
	    {
	    fCmdProcessors.erase( cmdIter );
	    found = true;
	    break;
	    }
	cmdIter++;
	}
    ret = pthread_mutex_unlock( &fCmdProcessorMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCInterface::DelCommandProcessor", "Mutex unlock error" )
	    << "Error unlocking command processor mutex: " << strerror(ret) << AliHLTLog::kDec
	    << " (" << ret << "). Continuing..." << ENDLOG;
	}
    if ( !found )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSCInterface::DelCommandProcessor", "Command processor not found" )
	    << "Command processor to be deleted could not be found in list." << ENDLOG;
	}
    }


int AliHLTSCInterface::Bind( const char* addressURL, BCLErrorCallback* errorCallback )
    {
    if ( fIsBound )
	{
	Unbind();
	}
    int ret;
    bool isBlob = false;
    BCLCommunication* com;
    ret = BCLDecodeLocalURL( addressURL, com, fAddress, isBlob );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCInterface::Bind", "Address decoding error" )
	    << "Error decoding address URL '" << addressURL << "': " << strerror(ret)
	    << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	fAddress = NULL;
	return ret;
	}
    if ( isBlob )
	{
	BCLFreeAddress( com, fAddress );
	fAddress = NULL;
	LOG( AliHLTLog::kError, "AliHLTSCInterface::Bind", "Blob address URL specified" )
	    << "Blob address URL '" << addressURL << "' specified. Need MSG address URL!"
	    << ENDLOG;
	return EINVAL;
	}
    fMsgCom = (BCLMsgCommunication*)com;

    if ( errorCallback )
	fMsgCom->AddCallback( errorCallback );

    ret = fMsgCom->Bind( fAddress );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCInterface::Bind", "Bind Error" )
	    << "BCL msg com bind error: " << strerror(ret) << AliHLTLog::kDec
	    << " (" << ret << ")." << ENDLOG;
	}
    LOG( AliHLTLog::kInformational, "AliHLTSCInterface::Bind", "Bind successfull" )
	<< "BCL msg com bind successfull." << ENDLOG;
    fIsBound = true;
    return ret;
    }

int AliHLTSCInterface::Unbind()
    {
    if ( !fIsBound )
	return 0;
    if ( !fMsgCom || !fAddress )
	return EFAULT;
    
    int ret;
    ret = fMsgCom->Unbind();
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCInterface::Unbind", "Unbind Error" )
	    << "BCL msg com unbind error: " << strerror(ret) << AliHLTLog::kDec
	    << " (" << ret << ")." << ENDLOG;
	}
    BCLFreeAddress( fMsgCom, fAddress );
    fMsgCom = NULL;
    fAddress = NULL;
    fIsBound = false;
    return ret;
    }

int AliHLTSCInterface::LookAtMe() // Send status data asnychronously if controller object is connected.
    {
    if ( !fIsBound )
	return EIO;
    int ret;
    int finalRet = 0;
    vector<BCLAbstrAddressStruct*>::iterator addrIter, addrEnd;
    AliHLTSCMessage msg;
    msg.GetData()->fMsgType = kAliHLTSCLookAtMeMsg;
    msg.GetData()->fRefNdx = 0;

    ret = pthread_mutex_lock( &fConnectedAddressMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCInterface::LookAtMe()", "Mutex lock error" )
	    << "Error locking connected addresses mutex: " << strerror(ret) << AliHLTLog::kDec
	    << " (" << ret << "). Continuing..." << ENDLOG;
	}
  
    addrIter = fConnectedAddresses.begin();
    addrEnd = fConnectedAddresses.end();
    while ( addrIter != addrEnd )
	{
	if ( *addrIter )
	    {
	    ret = pthread_mutex_lock( &fSendMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTSCInterface::LookAtMe", "Mutex lock error" )
		    << "Error locking send mutex: " << strerror(ret) << AliHLTLog::kDec
		    << " (" << ret << "). Continuing..." << ENDLOG;
		}
	    ret = fMsgCom->Send( (*addrIter), msg.GetData(), fSendTimeout );
	    if ( ret )
		{
		finalRet = ret;
		LOGG( AliHLTLog::kError, "AliHLTSCInterface::LookAtMe()", "Message send error", tmpLog )
		    << "Error sending LookAtMe message to connected address '";
		    BCLAddressLogger::LogAddress( tmpLog, **addrIter ) << "': "
								    << strerror(ret) << AliHLTLog::kDec
								    << " (" << ret << ")." << ENDLOG;
		}
	    ret = pthread_mutex_unlock( &fSendMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTSCInterface::LookAtMe", "Mutex unlock error" )
		    << "Error unlocking send mutex: " << strerror(ret) << AliHLTLog::kDec
		    << " (" << ret << "). Continuing..." << ENDLOG;
		}
	    }
	addrIter++;
	}
    
    ret = pthread_mutex_unlock( &fConnectedAddressMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCInterface::LookAtMe", "Mutex unlock error" )
	    << "Error unlocking connected addresses mutex: " << strerror(ret) << AliHLTLog::kDec
	    << " (" << ret << "). Continuing..." << ENDLOG;
	}
    return finalRet;
    }

int AliHLTSCInterface::LookAtMe( BCLAbstrAddressStruct* address ) // Send status data asnychronously to listening party.
    {
    if ( !fIsBound )
	return EIO;
    int ret;
    AliHLTSCMessage msg;
    msg.GetData()->fMsgType = kAliHLTSCLookAtMeMsg;

  
    ret = pthread_mutex_lock( &fSendMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCInterface::LookAtMe(BCLAbstrAddressStruct*)", "Mutex lock error" )
	    << "Error locking send mutex: " << strerror(ret) << AliHLTLog::kDec
	    << " (" << ret << "). Continuing..." << ENDLOG;
	}
    ret = fMsgCom->Send( address, msg.GetData(), fSendTimeout );
    if ( ret )
	{
	LOGG( AliHLTLog::kError, "AliHLTSCInterface::LookAtMe(BCLAbstrAddressStruct*)", "Message send error", tmpLog )
	    << "Error sending LookAtMe message to specified address '";
	    BCLAddressLogger::LogAddress( tmpLog, *address ) << "': "
							  << strerror(ret) << AliHLTLog::kDec
							  << " (" << ret << ")." << ENDLOG;
	}
    ret = pthread_mutex_unlock( &fSendMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSCInterface::LookAtMe(BCLAbstrAddressStruct*)", "Mutex unlock error" )
	    << "Error unlocking send mutex: " << strerror(ret) << AliHLTLog::kDec
	    << " (" << ret << "). Continuing..." << ENDLOG;
	}

    return ret;
    }

void AliHLTSCInterface::ProcessSystemCmd( AliHLTSCCommandStruct* cmdS )
    {
    if ( !cmdS )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSCInterface::ProcessSystemCmd", "Internal NULL pointer error" )
	    << "Internal error: NULL pointer passed to AliHLTSCInterface::ProcessSystemCmd. Dumping stack trace..."
	    << ENDLOG;
	AliHLTStackTrace( "AliHLTSCInterface::ProcessSystemCmd Internal Error", AliHLTLog::kFatal, 20 );
	return;
	}
    AliHLTSCCommand cmd( cmdS );
    LOG( AliHLTLog::kDebug, "AliHLTSCInterface::ProcessSystemCmd", "Command received" )
	<< "Received sys command '" << *(cmd.GetData()) << "'." << ENDLOG;
    switch ( cmd.GetData()->fCmd )
	{
	case kAliHLTSCNOPSysCmd:
	    break;
	case kAliHLTSCSetLogLevelSysCmd:
	    gLogLevel = cmd.GetData()->fParam0;
	    break;
	case kAliHLTSCPanicAbortSysCmd:
	    LOG( AliHLTLog::kFatal, "AliHLTSCInterface::ProcessSystemCmd", "Panic Abort" )
		<< "AliHLTSCInterface::ProcessSystemCmd received PANIC ABORT. PANICKING. Where's my tea. Returning 42..." << ENDLOG;
	    exit( 42 );
	    break;
	default:
	    LOG( AliHLTLog::kError, "AliHLTSCInterface::ProcessSystemCmd", "Unknown Command" )
		<< "Unknown command '0x" << AliHLTLog::kHex << cmd.GetData()->fCmd << " (" << AliHLTLog::kDec
		<< cmd.GetData()->fCmd << ")' sent to AliHLTSCInterface::ProcessSystemCmd." << ENDLOG;
	    break;
	}
    }
 

void AliHLTSCInterface::ListenHandler()
    {
    if ( !fIsBound )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSCInterface::ListenHandler", "Not bound" )
	    << "Object not bound. Aborting..." << ENDLOG;
	return;
	}
    if ( !fMsgCom )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSCInterface::ListenHandler", "No communication object" )
	    << "No communication object (NULL pointer). Aborting..." << ENDLOG;
	return;
	}
    
    int ret;
    BCLMessageStruct* msg;
    BCLAbstrAddressStruct* address;
    BCLMessage msgClass;
    AliHLTSCMessage scMsgClass;
    AliHLTSCMessage scRefMsg;

    address = fMsgCom->NewAddress();
    if ( !address )
	{
	LOG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Out of memory" )
	    << "Out of memory allocating new address from communication object. Aborting..." << ENDLOG;
	return;
	}

    fListenHandlerQuitted = false;
    while ( !fQuitListenHandler )
	{
	ret = fMsgCom->Receive( address, msg, 500 );
	if ( ret == ETIMEDOUT )
	    continue;
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Receive error" )
		<< "Error on Receive call: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << "). usleep( 100000 )'ing and continuing..." << ENDLOG;
	    usleep( 100000 );
	    continue;
	    }
	if ( !msg )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "NULL pointer msg" )
		<< "Received NULL pointer msg from Receive. usleep( 100000 )'ing and continuing..." << ENDLOG;
	    usleep( 100000 );
	    continue;
	    }

	msgClass.SetTo( msg );
	if ( msgClass.GetData()->fLength < scRefMsg.GetData()->fLength )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Received message size error" )
		<< "Received message has wrong size: " << AliHLTLog::kDec << msgClass.GetData()->fLength
		<< " bytes received - at least " << scRefMsg.GetData()->fLength << " bytes expected."
		<< ENDLOG;
	    continue;
	    }
	//scMsgClass.Adopt( (AliHLTSCMessageStruct*)msg );

	switch ( msgClass.GetData()->fMsgType )
	    {
	    case kAliHLTSCNOPMsg:
		break;

	    case kAliHLTSCStatusQueryMsg:
		{
		LOG( AliHLTLog::kDebug, "AliHLTSCInterface::ListenHandler", "Received Status Query Message" )
		    << "Received Status Query Message with reference index " << AliHLTLog::kDec
		    << ((AliHLTSCMessageStruct*)msg)->fRefNdx << "." << ENDLOG;
		AliUInt8_t* msgDat=NULL;
		AliHLTSCMessage lmsg;
		AliUInt32_t statLen, totLen;
		if ( fStatusData )
		    {
// XXXX TODO LOCKING XXXX
		    if ( fStatusDataMutex )
			{
			MLUCMutex::TLocker statusDataLock( *fStatusDataMutex );
			BCLNetworkData::Transform( fStatusData->fLength, statLen, fStatusData->fDataFormats[ kCurrentByteOrderIndex ], kNativeByteOrder ); 
			}
		    else
			BCLNetworkData::Transform( fStatusData->fLength, statLen, fStatusData->fDataFormats[ kCurrentByteOrderIndex ], kNativeByteOrder ); 
		    totLen = statLen + lmsg.GetData()->fLength;
		    msgDat = new AliUInt8_t[ totLen ];
		    if ( !msgDat )
			{
			LOG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Out of memory" )
			    << "Out of memory allocating new status reply message of size " << AliHLTLog::kDec
			    << totLen << " 0x" << AliHLTLog::kHex << totLen
			    << "." << ENDLOG;
			break;
			}
		    memcpy( msgDat, lmsg.GetData(), lmsg.GetData()->fLength );
		    //memcpy( msgDat+lmsg.GetData()->fLength, fStatusData, statLen );
		    if ( fStatusDataMutex )
			{
			MLUCMutex::TLocker statusDataLock( *fStatusDataMutex );
			memcpy( ((AliHLTSCMessageStruct*)msgDat)->fData, (void const*)fStatusData, statLen );
			}
		    else
			memcpy( ((AliHLTSCMessageStruct*)msgDat)->fData, (void const*)fStatusData, statLen );
		    ((AliHLTSCMessageStruct*)msgDat)->fLength = totLen;
		    ((AliHLTSCMessageStruct*)msgDat)->fMsgType = kAliHLTSCStatusReplyMsg;
		    ((AliHLTSCMessageStruct*)msgDat)->fRefNdx = ((AliHLTSCMessageStruct*)msg)->fRefNdx;
		    }
		else
		    {
		    statLen = lmsg.GetData()->fLength;
		    msgDat = new AliUInt8_t[ statLen ];
		    if ( !msgDat )
			{
			LOG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Out of memory" )
			    << "Out of memory allocating new empty status reply message of size " << AliHLTLog::kDec
			    << statLen << " 0x" << AliHLTLog::kHex << statLen
			    << "." << ENDLOG;
			break;
			}
		    memcpy( msgDat, lmsg.GetData(), statLen );
		    ((AliHLTSCMessageStruct*)msgDat)->fLength = statLen;
		    ((AliHLTSCMessageStruct*)msgDat)->fMsgType = kAliHLTSCStatusReplyMsg;
		    ((AliHLTSCMessageStruct*)msgDat)->fRefNdx = ((AliHLTSCMessageStruct*)msg)->fRefNdx;
		    LOG( AliHLTLog::kDebug, "AliHLTSCInterface::ListenHandler", "Sending Status Reply Message" )
			<< "Sending Status Reply Message with reference index " << AliHLTLog::kDec
			<< ((AliHLTSCMessageStruct*)msgDat)->fRefNdx << "." << ENDLOG;
		    }
		ret = pthread_mutex_lock( &fSendMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Mutex lock error" )
			<< "Error locking send mutex: " << strerror(ret) << AliHLTLog::kDec
			<< " (" << ret << "). Continuing..." << ENDLOG;
		    }

		ret = fMsgCom->Send( address, (BCLMessageStruct*)msgDat, fSendTimeout );
		if ( ret )
		    {
		    LOGG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Status Reply Send Error", tmpLog )
			<< "Error sending status reply to requester '";
			BCLAddressLogger::LogAddress( tmpLog, *address )
			<< "': " << strerror(ret) << " (" << AliHLTLog::kDec << ret
			<< ")." << ENDLOG;
		    }

		ret = pthread_mutex_unlock( &fSendMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Mutex unlock error" )
			<< "Error unlocking send mutex: " << strerror(ret) << AliHLTLog::kDec
			<< " (" << ret << "). Continuing..." << ENDLOG;
		    }

		if ( msgDat )
		    delete [] msgDat;
		LOGG( AliHLTLog::kInformational, "AliHLTSCInterface::ListenHandler", "Status Reply Sent", tmpLog )
		    << "Status Reply successfully sent to '";
		    BCLAddressLogger::LogAddress( tmpLog, *address )
			<< "'." << ENDLOG;
		break;
		}
	    case kAliHLTSCConnectedMsg:
		{
		bool found = false;
		ret = pthread_mutex_lock( &fConnectedAddressMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Mutex lock error" )
			<< "Error locking connected addresses mutex: " << strerror(ret) << AliHLTLog::kDec
			<< " (" << ret << "). Continuing..." << ENDLOG;
		    }

		vector<BCLAbstrAddressStruct*>::iterator addrIter, addrEnd;
		addrIter = fConnectedAddresses.begin();
		addrEnd = fConnectedAddresses.end();
		while ( addrIter != addrEnd )
		    {
		    if ( address->fLength == (*addrIter)->fLength )
			{
			if ( !memcmp( address, (*addrIter), address->fLength ) )
			    {
			    found = true;
			    break;
			    }
			}
		    addrIter++;
		    }
		if ( !found )
		    {
		    BCLAbstrAddressStruct* newAddress;

		    newAddress = fMsgCom->NewAddress();
		    if ( !newAddress )
			{
			LOG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Out of memory" )
			    << "Out of memory allocating new address from communication object. Aborting..." << ENDLOG;
			}
		    else
			{
			memcpy( newAddress, address, address->fLength );
			fConnectedAddresses.insert( fConnectedAddresses.end(), newAddress );
			LOGG( AliHLTLog::kInformational, "AliHLTSCInterface::ListenHandler", "New Connected Address", tmpLog )
			    << "New connected address '";
			    BCLAddressLogger::LogAddress( tmpLog, *address )
				<< "' successfully added." << ENDLOG;
			}
		    }

		ret = pthread_mutex_unlock( &fConnectedAddressMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Mutex unlock error" )
			<< "Error unlocking connected addresses mutex: " << strerror(ret) << AliHLTLog::kDec
			<< " (" << ret << "). Continuing..." << ENDLOG;
		    }
		if ( !found )
		    {
		    ret = pthread_mutex_lock( &fSendMutex );
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Mutex lock error" )
			    << "Error locking send mutex: " << strerror(ret) << AliHLTLog::kDec
			    << " (" << ret << "). Continuing..." << ENDLOG;
			}
		    ret = fMsgCom->Connect( address, fSendTimeout, true );
		    if ( ret )
			{
			LOGG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Connect error", tmpLog )
			    << "Error connecting to new address '";
			BCLAddressLogger::LogAddress( tmpLog, *address ) << "': "
								      << strerror(ret) << AliHLTLog::kDec
								      << " (" << ret << ")." << ENDLOG;
			}
		    ret = pthread_mutex_unlock( &fSendMutex );
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Mutex unlock error" )
			    << "Error unlocking send mutex: " << strerror(ret) << AliHLTLog::kDec
			    << " (" << ret << "). Continuing..." << ENDLOG;
			}
		    }
		break;
		}
	    case kAliHLTSCDisconnectedMsg:
		{
		bool found = false;
		ret = pthread_mutex_lock( &fConnectedAddressMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Mutex lock error" )
			<< "Error locking connected addresses mutex: " << strerror(ret) << AliHLTLog::kDec
			<< " (" << ret << "). Continuing..." << ENDLOG;
		    }

		vector<BCLAbstrAddressStruct*>::iterator addrIter, addrEnd;
		addrIter = fConnectedAddresses.begin();
		addrEnd = fConnectedAddresses.end();
		while ( addrIter != addrEnd )
		    {
		    if ( address->fLength == (*addrIter)->fLength )
			{
			if ( !memcmp( address, (*addrIter), address->fLength ) )
			    {
			    fConnectedAddresses.erase( addrIter );
			    found = true;
			    break;
			    }
			}
		    addrIter++;
		    }

		ret = pthread_mutex_unlock( &fConnectedAddressMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Mutex unlock error" )
			<< "Error unlocking connected addresses mutex: " << strerror(ret) << AliHLTLog::kDec
			<< " (" << ret << "). Continuing..." << ENDLOG;
		    }

		if ( found )
		    {
		    ret = pthread_mutex_lock( &fSendMutex );
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Mutex lock error" )
			    << "Error locking send mutex: " << strerror(ret) << AliHLTLog::kDec
			    << " (" << ret << "). Continuing..." << ENDLOG;
			}
		    ret = fMsgCom->Disconnect( address, fSendTimeout );
		    if ( ret )
			{
			LOGG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Disconnect error", tmpLog )
			    << "Error disconnecting to new address '";
			BCLAddressLogger::LogAddress( tmpLog, *address ) << "': "
								      << strerror(ret) << AliHLTLog::kDec
								      << " (" << ret << ")." << ENDLOG;
			}
		    ret = pthread_mutex_unlock( &fSendMutex );
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Mutex unlock error" )
			    << "Error unlocking send mutex: " << strerror(ret) << AliHLTLog::kDec
			    << " (" << ret << "). Continuing..." << ENDLOG;
			}
		    }
		if ( found )
		    {
		    LOGG( AliHLTLog::kDebug, "AliHLTSCInterface::ListenHandler", "Disconnected Address", tmpLog )
			<< "Connected address '";
			BCLAddressLogger::LogAddress( tmpLog, *address )
			    << "' successfully disconnected." << ENDLOG;
		    }
		else
		    {
		    LOGG( AliHLTLog::kDebug, "AliHLTSCInterface::ListenHandler", "Unknown Disconnected Address", tmpLog )
			<< "Address '";
			BCLAddressLogger::LogAddress( tmpLog, *address )
			    << "' to be disconnected is not connected." << ENDLOG;
		    }
		break;
		}
	    case kAliHLTSCCommandMsg:
		{
		if ( msgClass.GetData()->fLength >= sizeof(AliHLTSCMessageStruct)+sizeof(AliHLTSCCommandStruct) )
		    {
		    AliHLTSCCommandStruct* command = (AliHLTSCCommandStruct*)(((AliHLTSCMessageStruct*)msg)->fData);
		    
		    AliHLTSCCommand cmdClass( command );
		    MLUCString cmdStr;
		    AliHLTSCComponentInterface::GetCommandName( cmdClass.GetData()->fCmd, cmdStr );
		    LOG( AliHLTLog::kDebug, "AliHLTSCInterface::ListenHandler", "Command received" )
			<< "Command message received: '" << cmdStr.c_str() << "' 0x" << AliHLTLog::kHex 
			<< cmdClass.GetData()->fCmd << "/" << AliHLTLog::kDec << cmdClass.GetData()->fCmd
			<< "." << ENDLOG;

		    fCmdSignal.AddNotificationData( reinterpret_cast<AliHLTSCMessageStruct*>(msg) );
		    msg = NULL;
		    fCmdSignal.Signal();
		    }
		else
		    {
		    LOGG( AliHLTLog::kWarning, "AliHLTSCInterface::ListenHandler", "Command too small", tmpLog )
			<< "Command message received from '";
			BCLAddressLogger::LogAddress( tmpLog, *address )
			    << "' is too small (" << AliHLTLog::kDec << msgClass.GetData()->fLength
			    << " Bytes. Expected at least " << sizeof(AliHLTSCMessageStruct)+sizeof(AliHLTSCCommandStruct)
			    << " Bytes." << ENDLOG;
		    }
		break;
		}
	    default:
		LOG( AliHLTLog::kError, "AliHLTSCInterface::ListenHandler", "Unknown message" )
		    << "Unknown message '0x" << AliHLTLog::kHex << msg->fMsgType << " (" << AliHLTLog::kDec
		    << msg->fMsgType << ")' received." << ENDLOG;
		break;
	    }

	if ( msg )
	    {
	    fMsgCom->ReleaseMsg( msg );
	    msg = NULL;
	    }
	
	}

    fListenHandlerQuitted = true;
    LOG( AliHLTLog::kInformational, "AliHLTSCInterface::ListenHandler", "Leaving" )
	<< "Leaving listen handler..." << ENDLOG;
    }

void AliHLTSCInterface::CmdHandler()
    {
    AliHLTSCMessageStruct* message;
    AliHLTSCCommandStruct* command;
    int ret;
    vector<AliHLTSCCommandProcessor*>::iterator cmdIter, cmdEnd;
    fCmdHandlerQuitted = false;
    fCmdSignal.Lock();
    while ( !fQuitCmdHandler )
	{
	if ( !fCmdSignal.HaveNotificationData() )
	    fCmdSignal.Wait();
	while ( fCmdSignal.HaveNotificationData() )
	    {
	    message = fCmdSignal.PopNotificationData();
	    if ( !message )
		continue;
	    fCmdSignal.Unlock();

	    command = (AliHLTSCCommandStruct*)message->fData;

	    AliHLTSCCommand cmdClass( command );
	    MLUCString cmdStr;
	    AliHLTSCComponentInterface::GetCommandName( cmdClass.GetData()->fCmd, cmdStr );
	    LOG( AliHLTLog::kDebug, "AliHLTSCInterface::CmdHandler", "Processing Command" )
		<< "Processing command: '" << cmdStr.c_str() << "' 0x" << AliHLTLog::kHex 
		<< cmdClass.GetData()->fCmd << "/" << AliHLTLog::kDec << cmdClass.GetData()->fCmd
#if 0
		<< " - Param 0: " << cmdClass.GetData()->fParam0
		<< " - Param 1: " << cmdClass.GetData()->fParam1
		<< " - fData (char*): " << (char*)(cmdClass.GetData()->fData)
#endif
		<< "." << ENDLOG;
	    if ( cmdClass.GetData()->fCmd < 256 )
		ProcessSystemCmd( command );
	    else
		{
		ret = pthread_mutex_lock( &fCmdProcessorMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSCInterface::CmdHandler", "Mutex lock error" )
			<< "Error locking command processor mutex: " << strerror(ret) << AliHLTLog::kDec
			<< " (" << ret << "). Continuing..." << ENDLOG;
		    }
		cmdIter = fCmdProcessors.begin();
		cmdEnd = fCmdProcessors.end();
		while ( cmdIter != cmdEnd )
		    {
		    (*cmdIter)->ProcessCmd( command );
		    cmdIter++;
		    }
		ret = pthread_mutex_unlock( &fCmdProcessorMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSCInterface::CmdHandler", "Mutex unlock error" )
			<< "Error unlocking command processor mutex: " << strerror(ret) << AliHLTLog::kDec
			<< " (" << ret << "). Continuing..." << ENDLOG;
		    }
		}
	    if ( fMsgCom )
		fMsgCom->ReleaseMsg( message );
	    fCmdSignal.Lock();
	    }
	}
    fCmdSignal.Unlock();
    fCmdHandlerQuitted = true;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
