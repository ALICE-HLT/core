/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCComponentInterface.hpp"
#include "AliHLTSCStatusBase.hpp"
#include "AliHLTStatus.hpp"
#include "AliHLTBridgeStatusData.hpp"
#include <stdio.h>

bool AliHLTSCComponentInterface::GetCommandName( AliUInt32_t command, MLUCString& name )
    {
    char tmp[256];
    switch ( command )
	{
	case kAliHLTPCNOPCmd:
	    name = "NOP";
	    break;
	case kAliHLTPCConfigureCmd:
	    name = "Configure";
	    break;
	case kAliHLTPCResetConfigurationCmd:
	    name = "ResetConfiguration";
	    break;
	case kAliHLTPCUnconfigureCmd:
	    name = "Unconfigure";
	    break;
	case kAliHLTPCAcceptSubscriptionsCmd:
	    name = "AcceptSubscriptions";
	    break;
	case kAliHLTPCStopSubscriptionsCmd:
	    name = "StopSubscriptions";
	    break;
	case kAliHLTPCCancelSubscriptionsCmd:
	    name = "CancelSubscriptions";
	    break;
	case kAliHLTPCSubscribeCmd:
	    name = "Subscribe";
	    break;
	case kAliHLTPCUnsubscribeCmd:
	    name = "Unsubscribe";
	    break;
	case kAliHLTPCAbortSubscriptionCmd:
	    name = "AbortSubscription";
	    break;
	case kAliHLTPCStartProcessingCmd:
	    name = "StartProcessing";
	    break;
	case kAliHLTPCPauseProcessingCmd:
	    name = "PauseProcessing";
	    break;
	case kAliHLTPCResumeProcessingCmd:
	    name = "ResumeProcessing";
	    break;
	case kAliHLTPCStopProcessingCmd:
	    name = "StopProcessing";
	    break;
	case kAliHLTPCEndProgramCmd:
	    name = "EndProgram";
	    break;
	case kAliHLTPCBindCmd:
	    name = "Bind";
	    break;
	case kAliHLTPCUnbindCmd:
	    name = "Unbind";
	    break;
// 	case kAliHLTPCAbortBindCmd:
// 	    name = "AbortBind";
// 	    break;
	case kAliHLTPCConnectCmd:
	    name = "Connect";
	    break;
	case kAliHLTPCDisconnectCmd:
	    name = "Disconnect";
	    break;
// 	case kAliHLTPCAbortConnectCmd:
// 	    name = "AbortConnect";
// 	    break;
	case kAliHLTPCReconnectCmd:
	    name = "Reconnect";
	    break;
	case kAliHLTPCNewConnectionCmd:
	    name = "NewConnection";
	    break;
	case kAliHLTPCNewRemoteAddressesCmd:
	    name = "NewRemoteAddresses";
	    break;
	case kAliHLTPCPURGEALLEVENTSCmd:
	    name = "PURGEALLEVENTS";
	    break;
	case kAliHLTPCSetPathStateCmd:
	    name = "SetPathState";
	    break;
	case kAliHLTPCPublisherPinEventsCmd:
	    name = "PublisherPinEvents";
	    break;
	case kAliHLTPCPublisherUnpinEventsCmd:
	    name = "PublisherUnpinEvents";
	    break;
	case kAliHLTPCRemoveSubscriberCmd:
	    name = "RemoveSubscriber";
	    break;
	case kAliHLTPCCancelSubscriberCmd:
	    name = "CancelSubscriber";
	    break;
	case kAliHLTPCSimulateErrorCmd:
	    name = "SimulateError";
	    break;
	case kAliHLTPCAddSubscriberCmd:
	    name = "AddSubscriber";
	    break;
	case kAliHLTPCDelSubscriberCmd:
	    name = "DelSubscriber";
	    break;
	case kAliHLTPCAddPublisherCmd:
	    name = "AddPublisher";
	    break;
	case kAliHLTPCDelPublisherCmd:
	    name = "DelPublisher";
	    break;
	case kAliHLTPCEnableSubscriberCmd:
	    name = "EnableSubscriber";
	    break;
	case kAliHLTPCDisableSubscriberCmd:
	    name = "DisableSubscriber";
	    break;
	case kAliHLTPCEnablePublisherCmd:
	    name = "EnablePublisher";
	    break;
	case kAliHLTPCDisablePublisherCmd:
	    name = "DisablePublisher";
	    break;
	case kAliHLTPCInitiateReconfigureEventCmd:
	    name = "InitiateReconfigureEvent";
	    break;
	case kAliHLTPCDumpEventsCmd:
	    name = "DumpEvents";
	    break;
	case kAliHLTPCInitiateUpdateDCSEventCmd:
	    name = "InitiateUpdateDCSEvent";
	    break;
	case kAliHLTPCDumpEventAccountingCmd:
	    name = "DumpEventAccounting";
	    break;
	case kAliHLTPCSetSubsystemVerbosityCmd:
	    name = "SetSubsystemVerbosity";
	    break;
	case kAliHLTPCDumpEventMetaDataCmd:
	    name = "DumpEventMetaData";
	    break;
	case kAliHLTPCInterruptConnectionCmd:
	    name = "InterruptConnection";
	    break;
	default:
	    sprintf( tmp, "Unknown Command (%lu/0x%08lX)", (unsigned long)command, (unsigned long)command );
	    name = tmp;
	    return false;
	}
    return true;
    }

bool AliHLTSCComponentInterface::GetStatusName( const AliHLTSCStatusHeaderStruct* statusHeader, MLUCString& name )
    {
//     char tmp[256];
    if ( !CheckForHLTStatus( statusHeader ) && !CheckForBridgeStatus( statusHeader ) )
	{
	name = "Incorrect Status Data";
	return false;
	}
    AliHLTStatus* status = (AliHLTStatus*)statusHeader;
    return AliHLTSCComponentInterface::GetStatusName( status->fProcessStatus.fState, name );
    }

bool AliHLTSCComponentInterface::GetStatusName( AliUInt64_t status, MLUCString& name )
    {
    name = "";
    bool added = false;
    if ( status & kAliHLTPCConsumerErrorStateFlag )
	{
	if ( added )
	    name += "+";
	name += "Consumer-Error";
	added = true;
	}
    if ( status & kAliHLTPCProducerErrorStateFlag )
	{
	if ( added )
	    name += "+";
	name += "Producer-Error";
	added = true;
	}
    if ( status & kAliHLTPCNetworkErrorStateFlag )
	{
	if ( added )
	    name += "+";
	name += "Network-Error";
	added = true;
	}
    if ( status & kAliHLTPCChangingStateFlag )
	{
	if ( added )
	    name += "+";
	name += "Changing-State";
	added = true;
	}
    if ( status & kAliHLTPCPausedStateFlag )
	{
	if ( added )
	    name += "+";
	name += "Paused";
	added = true;
	}
    if ( status & kAliHLTPCBusyStateFlag )
	{
	if ( added )
	    name += "+";
	name += "Busy";
	added = true;
	}
    if ( status & kAliHLTPCRunningStateFlag )
	{
	if ( added )
	    name += "+";
	name += "Running";
	added = true;
	}
    if ( status & kAliHLTPCConnectedStateFlag )
	{
	if ( added )
	    name += "+";
	name += "Connected";
	added = true;
	}
    if ( status & kAliHLTPCBoundStateFlag )
	{
	if ( added )
	    name += "+";
	name += "Bound";
	added = true;
	}
    if ( status & kAliHLTPCWithSubscribersStateFlag )
	{
	if ( added )
	    name += "+";
	name += "With-Subscribers";
	added = true;
	}
    if ( status & kAliHLTPCAcceptingSubscriptionsStateFlag )
	{
	if ( added )
	    name += "+";
	name += "Accepting-Subscriptions";
	added = true;
	}
    if ( status & kAliHLTPCSubscribedStateFlag )
	{
	if ( added )
	    name += "+";
	name += "Subscribed";
	added = true;
	}
    if ( status & kAliHLTPCConfiguredStateFlag )
	{
	if ( added )
	    name += "+";
	name += "Configured";
	added = true;
	}
    if ( status & kAliHLTPCStartedStateFlag )
	{
	if ( added )
	    name += "+";
	name += "Started";
	added = true;
	}

//     switch ( status )
// 	{
// 	case kAliHLTPCStartedState:
// 	    name = "Started";
// 	    break;
// 	case kAliHLTPCConfiguredState:
// 	    name = "Configured";
// 	    break;
// 	case kAliHLTPCConfigureErrorState:
// 	    name = "ConfigureError";
// 	    break;
// 	case kAliHLTPCAcceptingSubscriptionsState:
// 	    name = "AcceptingSubscriptions";
// 	    break;
// 	case kAliHLTPCSubscribedState:
// 	    name = "Subscribed";
// 	    break;
// 	case kAliHLTPCUnsubscribingState:
// 	    name = "Unsubscribing";
// 	    break;
// 	case kAliHLTPCAcceptingUnsubscribingState:
// 	    name = "AcceptingUnsubscribing";
// 	    break;
// 	case kAliHLTPCUnsubscribingWithSubscribersState:
// 	    name = "UnsubscribingWithSubscribers";
// 	    break;
// 	case kAliHLTPCReadyState:
// 	    name = "Ready";
// 	    break;
// 	case kAliHLTPCWaitingState:
// 	    name = "Waiting";
// 	    break;
// 	case kAliHLTPCBusyState:
// 	    name = "Busy";
// 	    break;
// 	case kAliHLTPCPausedState:
// 	    name = "Paused";
// 	    break;
// 	case kAliHLTPCWithSubscribersState:
// 	    name = "WithSubscribers";
// 	    break;
// 	case kAliHLTPCSubscribedWithSubscribersState:
// 	    name = "SubscribedWithSubscribers";
// 	    break;
// 	case kAliHLTPCPublisherErrorState:
// 	    name = "PublisherError";
// 	    break;
// 	case kAliHLTPCPublisherErrorAcceptingState:
// 	    name = "PublisherErrorAccepting";
// 	    break;
// 	case kAliHLTPCPublisherErrorProcessingState:
// 	    name = "PublisherErrorProcessing";
// 	    break;
// 	case kAliHLTPCPublisherErrorWSState:
// 	    name = "PublisherErrorWS";
// 	    break;
// 	case kAliHLTPCBoundState:
// 	    name = "Bound";
// 	    break;
// 	case kAliHLTPCConnectedState:
// 	    name = "Connected";
// 	    break;
// 	case kAliHLTPCBindErrorState:
// 	    name = "BindError";
// 	    break;
// 	case kAliHLTPCConnectionErrorState:
// 	    name = "CConnectionError";
// 	    break;
// 	case kAliHLTPCBoundAndAcceptingState:
// 	    name = "BoundAndAccepting";
// 	    break;
// 	case kAliHLTPCBoundWithSubscribersState:
// 	    name = "BoundWithSubscribers";
// 	    break;
// 	case kAliHLTPCConnectedWithSubscribersState:
// 	    name = "ConnectedWithSubscribers";
// 	    break;
// 	case kAliHLTPCBindErrorAcceptingState:
// 	    name = "BindErrorAccepting";
// 	    break;
// 	case kAliHLTPCConnectionErrorAcceptingState:
// 	    name = "ConnectionErrorAccepting";
// 	    break;
// 	case kAliHLTPCSubscribedAndBoundState:
// 	    name = "SubscribedAndBoundS";
// 	    break;
// 	case kAliHLTPCPublisherErrorBoundState:
// 	    name = "PublisherErrorBound";
// 	    break;
// 	case kAliHLTPCBindErrorSubscribedState:
// 	    name = "BindErrorSubscribed";
// 	    break;
// 	case kAliHLTPCPublisherErrorConnectedState:
// 	    name = "PublisherErrorConnected";
// 	    break;
// 	case kAliHLTPCConnectionErrorSubscribedState:
// 	    name = "ConnectionErrorSubscribed";
// 	    break;
// 	default:
// 	    sprintf( tmp, "Unknown Status (%Lu/0x%016LX)", (unsigned long long)status, (unsigned long long)status );
// 	    name = tmp;
// 	    return false;
// 	}
    return true;
    }

bool AliHLTSCComponentInterface::GetTypeName( const AliHLTSCStatusHeaderStruct* statusHeader, MLUCString& name )
    {
    char tmp[256];
    if ( !CheckForHLTStatus( statusHeader ) && !CheckForBridgeStatus( statusHeader ) )
	{
	name = "Incorrect Status Data";
	return false;
	}
    AliHLTStatus* status = (AliHLTStatus*)statusHeader;
    switch ( status->fProcessStatus.fProcessType )
	{
	case kAliHLTSCControlledDataSourceComponentType:
	    name = "ControlledDataSource";
	    break;
	case kAliHLTSCControlledDataSinkComponentType:
	    name = "ControlledDataSink";
	    break;
	case kAliHLTSCControlledDataProcessorComponentType:
	    name = "ControlledDataProcessor";
	    break;
	case kAliHLTSCControlledBridgeComponentType:
	    name = "ControlledBridge";
	    break;
	case kAliHLTSCEventScattererComponentType:
	    name = "EventScatterer";
	    break;
	case kAliHLTSCEventGathererComponentType:
	    name = "EventGatherer";
	    break;
	case kAliHLTSCEventMergerComponentType:
	    name = "EventMerger";
	    break;
	default:
	    sprintf( tmp, "Unknown Type (%lu/0x%08lX)", (unsigned long)status->fProcessStatus.fProcessType, (unsigned long)status->fProcessStatus.fProcessType );
	    name = tmp;
	    return false;
	}
    return true;
    }

bool AliHLTSCComponentInterface::CheckForHLTStatus( const AliHLTSCStatusHeaderStruct* statusHeader )
    {
    AliHLTStatus cmpStatus;
//     if ( cmpStatus.fHeader.fStatusStructCnt != statusHeader->fStatusStructCnt ||
// 	 cmpStatus.fHeader.fFirstStructOffset != statusHeader->fFirstStructOffset ||
// 	 cmpStatus.fHeader.fLength != statusHeader->fLength )
// 	return false;
    if ( cmpStatus.fHeader.fStatusStructCnt != statusHeader->fStatusStructCnt ||
	 cmpStatus.fHeader.fStatusType != statusHeader->fStatusType )
	return false;
    AliUInt32_t cnt;
    AliUInt32_t offset;
    AliHLTSCStatusBaseStruct* statusStruct;
    AliHLTSCStatusBaseStruct* cmpStatusStruct;
    offset = statusHeader->fFirstStructOffset;
    statusStruct = (AliHLTSCStatusBaseStruct*)(((AliUInt8_t*)statusHeader)+offset);
    cmpStatusStruct = (AliHLTSCStatusBaseStruct*)(((AliUInt8_t*)&cmpStatus)+offset);

    for ( cnt = 0; cnt < statusHeader->fStatusStructCnt; cnt++ )
	{
// 	if ( cmpStatusStruct->fLength != statusStruct->fLength ||
// 	     cmpStatusStruct->fNextStructOffset != statusStruct->fNextStructOffset )
// 	    return false;
	if ( cmpStatusStruct->fTypeID != statusStruct->fTypeID )
	    return false;
	offset = statusStruct->fNextStructOffset;
	statusStruct = (AliHLTSCStatusBaseStruct*)(((AliUInt8_t*)statusHeader)+offset);
	cmpStatusStruct = (AliHLTSCStatusBaseStruct*)(((AliUInt8_t*)&cmpStatus)+offset);
	}
    return true;
    }


bool AliHLTSCComponentInterface::CheckForBridgeStatus( const AliHLTSCStatusHeaderStruct* statusHeader )
    {
    AliHLTBridgeStatusData cmpStatus;
    if ( cmpStatus.fHeader.fStatusStructCnt != statusHeader->fStatusStructCnt ||
	 cmpStatus.fHeader.fFirstStructOffset != statusHeader->fFirstStructOffset ||
	 cmpStatus.fHeader.fLength != statusHeader->fLength )
	return false;
    AliUInt32_t cnt;
    AliUInt32_t offset;
    AliHLTSCStatusBaseStruct* statusStruct;
    AliHLTSCStatusBaseStruct* cmpStatusStruct;
    offset = statusHeader->fFirstStructOffset;
    statusStruct = (AliHLTSCStatusBaseStruct*)(((AliUInt8_t*)statusHeader)+offset);
    cmpStatusStruct = (AliHLTSCStatusBaseStruct*)(((AliUInt8_t*)&cmpStatus)+offset);

    for ( cnt = 0; cnt < statusHeader->fStatusStructCnt; cnt++ )
	{
	if ( cmpStatusStruct->fLength != statusStruct->fLength ||
	     cmpStatusStruct->fNextStructOffset != statusStruct->fNextStructOffset )
	    return false;
	offset = statusStruct->fNextStructOffset;
	statusStruct = (AliHLTSCStatusBaseStruct*)(((AliUInt8_t*)statusHeader)+offset);
	cmpStatusStruct = (AliHLTSCStatusBaseStruct*)(((AliUInt8_t*)&cmpStatus)+offset);
	}
    return true;
    }


bool AliHLTSCComponentInterface::GetUpdateTime( const AliHLTSCStatusHeaderStruct* statusHeader, struct timeval& tv )
    {
    if ( CheckForHLTStatus( statusHeader ) )
	{
	tv.tv_sec = ((AliHLTStatus*)statusHeader)->fProcessStatus.fLastUpdateTime_s;
	tv.tv_usec = ((AliHLTStatus*)statusHeader)->fProcessStatus.fLastUpdateTime_us;
	return true;
	}
    else if ( CheckForBridgeStatus( statusHeader ) )
	{
	tv.tv_sec = ((AliHLTBridgeStatusData*)statusHeader)->fProcessStatus.fLastUpdateTime_s;
	tv.tv_usec = ((AliHLTBridgeStatusData*)statusHeader)->fProcessStatus.fLastUpdateTime_us;
	return true;
	}
    else
	{
	tv.tv_sec = tv.tv_usec = 0;
	return false;
	}
    }

bool AliHLTSCComponentInterface::GetProcessStatus( const AliHLTSCStatusHeaderStruct* statusHeader, AliHLTSCProcessStatusStruct& status )
    {
    if ( CheckForHLTStatus( statusHeader ) )
	{
	AliHLTStatus* stat = (AliHLTStatus*)statusHeader;
	AliHLTSCProcessStatus tmpStat( (AliHLTSCProcessStatusStruct*)&(stat->fProcessStatus) );
	memcpy( &status, tmpStat.GetData(), sizeof(AliHLTSCProcessStatusStruct) );
	return true;
	}
    else if ( CheckForBridgeStatus( statusHeader ) )
	{
	AliHLTBridgeStatusData* stat = (AliHLTBridgeStatusData*)statusHeader;
	AliHLTSCProcessStatus tmpStat( (AliHLTSCProcessStatusStruct*)&(stat->fProcessStatus) );
	memcpy( &status, tmpStat.GetData(), sizeof(AliHLTSCProcessStatusStruct) );
	return true;
	}
    memset( &status, 0, sizeof(AliHLTSCProcessStatusStruct) );
    return false;
    }

bool AliHLTSCComponentInterface::GetHLTStatus( const AliHLTSCStatusHeaderStruct* statusHeader, AliHLTSCHLTStatusStruct& status )
    {
    if ( CheckForHLTStatus( statusHeader ) )
	{
	AliHLTStatus* stat = (AliHLTStatus*)statusHeader;
	AliHLTSCHLTStatus tmpStat( (AliHLTSCHLTStatusStruct*)&(stat->fHLTStatus) );
	memcpy( &status, tmpStat.GetData(), sizeof(AliHLTSCHLTStatusStruct) );
	return true;
	}
    else if ( CheckForBridgeStatus( statusHeader ) )
	{
	AliHLTBridgeStatusData* stat = (AliHLTBridgeStatusData*)statusHeader;
	AliHLTSCHLTStatus tmpStat( (AliHLTSCHLTStatusStruct*)&(stat->fHLTStatus) );
	memcpy( &status, tmpStat.GetData(), sizeof(AliHLTSCHLTStatusStruct) );
	return true;
	}
    memset( &status, 0, sizeof(AliHLTSCHLTStatusStruct) );
    return false;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
