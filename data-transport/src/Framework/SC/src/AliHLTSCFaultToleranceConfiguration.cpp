/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCFaultToleranceConfiguration.hpp"
#include "AliHLTLog.hpp"
#include "BCLURLAddressDecoder.hpp"
#include <fstream>
#include <cerrno>
#include <cstdlib>

AliHLTSCFaultToleranceConfiguration::AliHLTSCFaultToleranceConfiguration()
    {
    fNr = ~(unsigned long)0;
    }

AliHLTSCFaultToleranceConfiguration::~AliHLTSCFaultToleranceConfiguration()
    {
    }

bool AliHLTSCFaultToleranceConfiguration::ReadConfig( const char* filename )
    {
    ifstream file;
    int ret;
    char* cpErr = NULL;
    unsigned long nr;
    
    file.open( filename );
    if ( !file.good() )
	{
	LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Opening File" )
	    << "Error opening file " << filename << "." << ENDLOG;
	return false;
	}

    /*
      line: data-source-line | data-sink-line | target-line | nodes-line | ids-line | process-file-line
      
      data-source-line: 'source' number subnumber (<subscriber-control-address-URL> <publisher-control-address-URL> <publisher-msg-address-URL> <publisher-blobmsg-address-URL>)
      
      data-sink-line: 'sink' number <publisher-control-address-URL> <subscriber-control-address-URL> <subscriber-msg-address-URL> <subscriber-blobmsg-address-URL>
      
      target-line: 'target' <target-control-address-URL>
      
      nodes-line: 'nodes' number <node>+
      
      ids-line: 'ids' number <id>+
      
      process-file-line: 'processfile' <process-configuration-file-name>
    */

    MLUCString line, token;
    const char* pos;

    while ( ReadLine( file, line ) )
	{
	if ( !GetToken( line.c_str(), token, pos ) )
	    {	
	    LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		<< "Error parsing file " << filename << ": '" << pos << "'." << ENDLOG;
	    return false;
	    }
	if ( !pos )
	    continue; // Empty line.

	if ( token == "source" )
	    {
	    // data-source-line: 'source' number subnumber <subscriber-control-address-URL> <publisher-control-address-URL> <publisher-msg-address-URL> <publisher-blobmsg-address-URL>
	    DataSource source;
	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected a number." << ENDLOG;
		return false;
		}
	    nr = strtoul( token.c_str(), &cpErr, 0 );
	    if ( cpErr && *cpErr!=0 )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": Found '" << token.c_str() 
		    << "' where a number was expected." << ENDLOG;
		return false;
		}
	    if ( nr != fNr )
		continue;

	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected a number." << ENDLOG;
		return false;
		}
	    source.fSubNr = strtoul( token.c_str(), &cpErr, 0 );
	    if ( cpErr && *cpErr!=0 )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": Found '" << token.c_str() 
		    << "' where a number was expected." << ENDLOG;
		return false;
		}
 
	    if ( GetToken( pos, token, pos ) )
		{
		ret = BCLDecodeRemoteURL( token.c_str(), source.fSubscriberControlAddress );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
			<< "Error parsing subscriber control address '" << token.c_str() 
			<< "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		    return false;
		    }
		
// 		if ( !GetToken( pos, token, pos ) )
// 		    {
// 		    LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
// 			<< "Error parsing file " << filename << ": '" << pos << "', expected publisher-control-address." << ENDLOG;
// 		    return false;
// 		    }
// 		ret = BCLDecodeRemoteURL( token.c_str(), source.fPublisherControlAddress );
// 		if ( ret )
// 		    {
// 		    LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
// 			<< "Error parsing publisher control address '" << token.c_str() 
// 			<< "' in file " << filename << ": '" << pos << "'." << ENDLOG;
// 		    return false;
// 		    }
		
		if ( !GetToken( pos, token, pos ) )
		    {
		    LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
			<< "Error parsing file " << filename << ": '" << pos << "', expected publisher-msg-address." << ENDLOG;
		    return false;
		    }
		ret = BCLDecodeRemoteURL( token.c_str(), source.fPublisherMsgAddress );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
			<< "Error parsing publisher msg address '" << token.c_str() 
			<< "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		    return false;
		    }
		source.fPublisherMsgAddressURL = token;
		
		if ( !GetToken( pos, token, pos ) )
		    {
		    LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
			<< "Error parsing file " << filename << ": '" << pos << "', expected publisher-blobmsg-address." << ENDLOG;
		    return false;
		    }
		ret = BCLDecodeRemoteURL( token.c_str(), source.fPublisherBlobMsgAddress );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
			<< "Error parsing publisher blob msg address '" << token.c_str() 
			<< "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		    return false;
		    }
		source.fPublisherBlobMsgAddressURL = token;

		fSources.insert( fSources.end(), source );
		}
	    }
	else if ( token == "sink" )
	    {
	    // data-sink-line: 'sink' number <publisher-control-address-URL> <subscriber-control-address-URL> <subscriber-msg-address-URL> <subscriber-blobmsg-address-URL>
	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected a number." << ENDLOG;
		return false;
		}
	    nr = strtoul( token.c_str(), &cpErr, 0 );
	    if ( cpErr && *cpErr!=0 )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": Found '" << token.c_str() 
		    << "' where a number was expected." << ENDLOG;
		return false;
		}
	    if ( nr != fNr )
		continue;
	    
	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected publisher-control-address." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), fSink.fPublisherControlAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing publisher control address '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}
	    
// 	    if ( !GetToken( pos, token, pos ) )
// 		{
// 		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
// 		    << "Error parsing file " << filename << ": '" << pos << "', expected subscriber-control-address." << ENDLOG;
// 		return false;
// 		}
// 	    ret = BCLDecodeRemoteURL( token.c_str(), fSink.fSubscriberControlAddress );
// 	    if ( ret )
// 		{
// 		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
// 		    << "Error parsing subscriber control address '" << token.c_str() 
// 		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
// 		return false;
// 		}
	    
	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected subscriber-msg-address." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), fSink.fSubscriberMsgAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing subscriber msg address '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}
	    fSink.fSubscriberMsgAddressURL = token;
	    
	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected subscriber-blobmsg-address." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), fSink.fSubscriberBlobMsgAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing subscriber blob msg address '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}
	    fSink.fSubscriberBlobMsgAddressURL = token;
	    }
	else if ( token == "target" )
	    {
	    BCLAbstrAddressStruct* targetAddr;
	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected target-control-address-URL." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), targetAddr );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing target-control-address-URL '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}
	    
	    fTargets.insert( fTargets.end(), targetAddr );
	    }
	else if ( token == "startnodes" )
	    {
	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected a number." << ENDLOG;
		return false;
		}
	    nr = strtoul( token.c_str(), &cpErr, 0 );
	    if ( cpErr && *cpErr!=0 )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": Found '" << token.c_str() 
		    << "' where a number was expected." << ENDLOG;
		return false;
		}
	    if ( nr != fNr )
		continue;

	    while ( GetToken( pos, token, pos ) )
		{
		fStartNodes.insert( fStartNodes.end(), token );
		}
	    }
	else if ( token == "startids" )
	    {
	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected a number." << ENDLOG;
		return false;
		}
	    nr = strtoul( token.c_str(), &cpErr, 0 );
	    if ( cpErr && *cpErr!=0 )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": Found '" << token.c_str() 
		    << "' where a number was expected." << ENDLOG;
		return false;
		}
	    if ( nr != fNr )
		continue;

	    while ( GetToken( pos, token, pos ) )
		{
		fStartIDs.insert( fStartIDs.end(), token );
		}
	    }
	else if ( token == "addnodes" )
	    {
	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected a number." << ENDLOG;
		return false;
		}
	    nr = strtoul( token.c_str(), &cpErr, 0 );
	    if ( cpErr && *cpErr!=0 )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": Found '" << token.c_str() 
		    << "' where a number was expected." << ENDLOG;
		return false;
		}
	    if ( nr != fNr )
		continue;

	    while ( GetToken( pos, token, pos ) )
		{
		fAddNodes.insert( fAddNodes.end(), token );
		}
	    }
	else if ( token == "processfile" )
	    {
	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected a filename." << ENDLOG;
		return false;
		}
	    fProcessFile = token;
	    }
	else
	    {
	    LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		<< "Error parsing file " << filename << ": '" << pos 
		<< "', expected 'source', 'sink', or 'target', 'nodes', 'ids', 'processfile'." << ENDLOG;
	    return false;
	    }
	}
    return true;
    }

void AliHLTSCFaultToleranceConfiguration::SetDataPathNr( unsigned long dataPathNr )
    {
    fNr = dataPathNr;
    }

void AliHLTSCFaultToleranceConfiguration::GetSinkData( DataSink& sink )
    {
    sink = fSink;
    }

void AliHLTSCFaultToleranceConfiguration::GetSourceData( vector<DataSource>& sources )
    {
    vector<DataSource>::iterator sourceIter, sourceEnd;
    sources.clear();
    sourceIter = fSources.begin();
    sourceEnd = fSources.end();
    while ( sourceIter != sourceEnd )
	{
	sources.insert( sources.end(), *sourceIter );
	sourceIter++;
	}
    }

void AliHLTSCFaultToleranceConfiguration::GetStartNodes( vector<MLUCString>& nodes )
    {
    vector<MLUCString>::iterator iter, end;
    nodes.clear();
    iter = fStartNodes.begin();
    end = fStartNodes.end();
    while ( iter != end )
	{
	nodes.insert( nodes.end(), *iter );
	iter++;
	}
    }

void AliHLTSCFaultToleranceConfiguration::GetStartIDs( vector<MLUCString>& ids )
    {
    vector<MLUCString>::iterator iter, end;
    ids.clear();
    iter = fStartIDs.begin();
    end = fStartIDs.end();
    while ( iter != end )
	{
	ids.insert( ids.end(), *iter );
	iter++;
	}
    }

void AliHLTSCFaultToleranceConfiguration::GetAddNodes( vector<MLUCString>& nodes )
    {
    vector<MLUCString>::iterator iter, end;
    nodes.clear();
    iter = fAddNodes.begin();
    end = fAddNodes.end();
    while ( iter != end )
	{
	nodes.insert( nodes.end(), *iter );
	iter++;
	}
    }

void AliHLTSCFaultToleranceConfiguration::GetTargets( vector<BCLAbstrAddressStruct*>& targets )
    {
    vector<BCLAbstrAddressStruct*>::iterator iter, end;
    targets.clear();
    iter = fTargets.begin();
    end = fTargets.end();
    while ( iter != end )
	{
	targets.insert( targets.end(), *iter );
	iter++;
	}
    }

void AliHLTSCFaultToleranceConfiguration::GetProcessFile( MLUCString& file )
    {
    file = fProcessFile;
    }

int AliHLTSCFaultToleranceConfiguration::SetStartNodes( vector<MLUCString>& nodes )
    {
    vector<MLUCString> oldnodes;
    vector<MLUCString>::iterator iter = nodes.begin(), end = nodes.end();
    vector<MLUCString>::iterator oldIter, oldEnd;
    oldnodes = fStartNodes;
    if ( nodes.size() != fStartNodes.size() )
	return EINVAL;
    fStartNodes.clear();
    while ( iter != end )
	{
	fStartNodes.insert( fStartNodes.end(), *iter );
	iter++;
	}
    
    iter = fStartNodes.begin();
    end = fStartNodes.end();
    oldIter = oldnodes.begin();
    oldEnd = oldnodes.end();
    vector<DataSource>::iterator sourceIter, sourceEnd;
    
    while ( iter != end )
	{
	ReplaceName( fSink.fSubscriberMsgAddressURL, fSink.fSubscriberMsgAddress, oldIter->c_str(), iter->c_str() );
	ReplaceName( fSink.fSubscriberBlobMsgAddressURL, fSink.fSubscriberBlobMsgAddress, oldIter->c_str(), iter->c_str() );
	sourceIter = fSources.begin();
	sourceEnd = fSources.end();
	while ( sourceIter != sourceEnd )
	    {
	    ReplaceName( sourceIter->fPublisherMsgAddressURL, sourceIter->fPublisherMsgAddress, oldIter->c_str(), iter->c_str() );
	    ReplaceName( sourceIter->fPublisherBlobMsgAddressURL, sourceIter->fPublisherBlobMsgAddress, oldIter->c_str(), iter->c_str() );
	    sourceIter++;
	    }
	iter++;
	oldIter++;
	};
    return 0;
    }

int AliHLTSCFaultToleranceConfiguration::SetAddNodes( vector<MLUCString>& nodes )
    {
    vector<MLUCString> oldnodes;
    vector<MLUCString>::iterator iter = nodes.begin(), end = nodes.end();
    vector<MLUCString>::iterator oldIter, oldEnd;
    oldnodes = fAddNodes;
    if ( nodes.size() != fAddNodes.size() )
	return EINVAL;
    fAddNodes.clear();
    while ( iter != end )
	{
	fAddNodes.insert( fAddNodes.end(), *iter );
	iter++;
	}
    
    iter = fAddNodes.begin();
    end = fAddNodes.end();
    oldIter = oldnodes.begin();
    oldEnd = oldnodes.end();
    vector<DataSource>::iterator sourceIter, sourceEnd;
    
    while ( iter != end )
	{
	ReplaceName( fSink.fSubscriberMsgAddressURL, fSink.fSubscriberMsgAddress, oldIter->c_str(), iter->c_str() );
	ReplaceName( fSink.fSubscriberBlobMsgAddressURL, fSink.fSubscriberBlobMsgAddress, oldIter->c_str(), iter->c_str() );
	sourceIter = fSources.begin();
	sourceEnd = fSources.end();
	while ( sourceIter != sourceEnd )
	    {
	    ReplaceName( sourceIter->fPublisherMsgAddressURL, sourceIter->fPublisherMsgAddress, oldIter->c_str(), iter->c_str() );
	    ReplaceName( sourceIter->fPublisherBlobMsgAddressURL, sourceIter->fPublisherBlobMsgAddress, oldIter->c_str(), iter->c_str() );
	    sourceIter++;
	    }
	iter++;
	oldIter++;
	};
    return 0;
    }

bool AliHLTSCFaultToleranceConfiguration::ReadLine( istream& istr, MLUCString& line )
    {
    char tmp[ 256 ];
    char peeked;
    line = "";
    do
	{
	if ( !istr.good() )
	    return false;
	istr.get( tmp, 256, '\n' );
	line += (char*)tmp;
	if ( istr.eof() )
	    {
	    LOG( AliHLTLog::kDebug, "BridgeConfig::ReadLine", "Line Read" )
		<< "Line '" << line.c_str() << "' read." << ENDLOG;
	    return true;
	    }
	peeked = istr.peek();
	if ( peeked == '\n' )
	    {
	    peeked = istr.get();
	    LOG( AliHLTLog::kDebug, "BridgeConfig::ReadLine", "Line Read" )
		<< "Line '" << line.c_str() << "' read." << ENDLOG;
	    return true;
	    }
	}
    while ( true );
    }

bool AliHLTSCFaultToleranceConfiguration::GetToken( const char* text, MLUCString& token, const char*& pos )
    {
    if ( !text )
	return false;
    token = "";
    const char* iter = text;
    while ( *iter && (*iter==' ' || *iter=='\t' || *iter=='\n') )
	iter++;
    if ( !*iter || *iter=='#' )
	{
	pos = NULL;
	return true;
	}
    pos = iter;
    while ( *iter && *iter!=' ' && *iter!='\t' && *iter!='\n' && *iter!='#' )
	{
	token += *iter;
	iter++;
	}
    pos = iter;
//     LOG( AliHLTLog::kDebug, "BridgeConfig::GetToken", "Token Found" )
// 	<< "Token '" << token.c_str() << "' found." << ENDLOG;
    return true;
    }


bool AliHLTSCFaultToleranceConfiguration::ReplaceName( MLUCString& url, BCLAbstrAddressStruct*& addr, const char* oldhostname, const char* newhostname )
    {
    MLUCString tmpStr;
    BCLAbstrAddressStruct* tmpAddr;
    char* cp;
    const char* tmpCP;
    int ret;
    cp = strstr( const_cast<char*>( url.c_str() ), oldhostname );
    if ( cp )
	{
	tmpCP = url.c_str();
	tmpStr = "";
	while ( tmpCP != cp )
	    {
	    tmpStr += *tmpCP;
	    tmpCP++;
	    }
	tmpStr += newhostname;
	tmpCP = cp+strlen(oldhostname);
	tmpStr += tmpCP;
	ret = BCLDecodeRemoteURL( tmpStr.c_str(), tmpAddr );
	if ( ret )
	    return ENXIO;
	BCLFreeAddress( addr );
	url = tmpStr;
	addr = tmpAddr;
	return true;
	}
    return false;
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
