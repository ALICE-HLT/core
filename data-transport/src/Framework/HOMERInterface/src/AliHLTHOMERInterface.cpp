/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTHOMERInterface.hpp"
#include "AliHLTGetNodeID.hpp"
#include "AliHLTHLTEventTriggerData.hpp"
#include <HOMERData.h>
#include <vector>

AliHLTHOMERInterface::AliHLTHOMERInterface()
    {
    fShmManager = NULL;
    fNodeID = AliHLTGetNodeID();
    Clear();
    }

AliHLTHOMERInterface::~AliHLTHOMERInterface()
    {
    }

void AliHLTHOMERInterface::Clear()
    {
    fEventID = ~(AliUInt64_t)0;
    fNodeID = ~(AliUInt32_t)0;
    fETS = NULL;
    fHOMERWriter.Clear();
    fSED.SetDescriptorData( NULL );
    }

bool AliHLTHOMERInterface::SetEvent( AliEventID_t eventID, const AliHLTEventTriggerStruct* ets,
				     const AliHLTSubEventDataDescriptor* sedd )
    {
    if ( !fShmManager )
	return false;
    Clear();
    fEventID = eventID;
    fETS = ets;
    
    fSED.SetDescriptorData( sedd );
    std::vector<AliHLTSubEventDescriptor::BlockData> blocks;
    fSED.Dereference( fShmManager, blocks );
    
    unsigned long i, n = blocks.size();
    homer_uint64 descriptor[kCount_64b_Words];
    HOMERBlockDescriptor homerBlock( descriptor );
    homerBlock.Initialize();
    if ( fETS && fETS->fHeader.fLength==sizeof(AliHLTHLTEventTriggerData) )
	{
	const AliHLTHLTEventTriggerData* hetd = reinterpret_cast<const AliHLTHLTEventTriggerData*>( fETS );
	homerBlock.SetByteOrder( hetd->fAttributes[kAliHLTSEDBDByteOrderAttributeIndex] );
	homerBlock.SetUInt64Alignment( hetd->fAttributes[kAliHLTSEDBD64BitAlignmentAttributeIndex] );
	homerBlock.SetUInt32Alignment( hetd->fAttributes[kAliHLTSEDBD32BitAlignmentAttributeIndex] );
	homerBlock.SetUInt16Alignment( hetd->fAttributes[kAliHLTSEDBD16BitAlignmentAttributeIndex] );
	homerBlock.SetUInt8Alignment( hetd->fAttributes[kAliHLTSEDBD8BitAlignmentAttributeIndex] );
	homerBlock.SetDoubleAlignment( hetd->fAttributes[kAliHLTSEDBDDoubleAlignmentAttributeIndex] );
	homerBlock.SetFloatAlignment( hetd->fAttributes[kAliHLTSEDBDFloatAlignmentAttributeIndex] );
	homerBlock.SetType( HLTEVENTRIGGERDATA_DATAID );
	homerBlock.SetSubType1( HLT_DATAORIGIN );
	homerBlock.SetSubType2( (uint64)0 );
	homerBlock.SetBirth_s( sedd->fEventBirth_s );
	homerBlock.SetBirth_us( sedd->fEventBirth_us );
	homerBlock.SetProducerNode( (uint64)fNodeID );
	homerBlock.SetBlockOffset( 0 );
	homerBlock.SetBlockSize( fETS->fHeader.fLength );
	homerBlock.SetStatusFlags( sedd->fStatusFlags );
	fHOMERWriter.AddBlock( descriptor, fETS );
	}

    for ( i = 0; i < n; i++ )
	{
	homerBlock.Initialize();
	homerBlock.SetByteOrder( blocks[i].fAttributes[kAliHLTSEDBDByteOrderAttributeIndex] );
	homerBlock.SetUInt64Alignment( blocks[i].fAttributes[kAliHLTSEDBD64BitAlignmentAttributeIndex] );
	homerBlock.SetUInt32Alignment( blocks[i].fAttributes[kAliHLTSEDBD32BitAlignmentAttributeIndex] );
	homerBlock.SetUInt16Alignment( blocks[i].fAttributes[kAliHLTSEDBD16BitAlignmentAttributeIndex] );
	homerBlock.SetUInt8Alignment( blocks[i].fAttributes[kAliHLTSEDBD8BitAlignmentAttributeIndex] );
	homerBlock.SetDoubleAlignment( blocks[i].fAttributes[kAliHLTSEDBDDoubleAlignmentAttributeIndex] );
	homerBlock.SetFloatAlignment( blocks[i].fAttributes[kAliHLTSEDBDFloatAlignmentAttributeIndex] );
	homerBlock.SetType( blocks[i].fDataType.fID );
	homerBlock.SetSubType1( (uint64)blocks[i].fDataOrigin.fID );
	homerBlock.SetSubType2( (uint64)blocks[i].fDataSpecification );
	homerBlock.SetBirth_s( sedd->fEventBirth_s );
	homerBlock.SetBirth_us( sedd->fEventBirth_us );
	homerBlock.SetProducerNode( (uint64)blocks[i].fProducerNode );
	homerBlock.SetBlockOffset( 0 );
	homerBlock.SetBlockSize( blocks[i].fSize );
	homerBlock.SetStatusFlags( blocks[i].fStatusFlags );
	fHOMERWriter.AddBlock( descriptor, blocks[i].fData );
	}
    return true;
    }

unsigned long long AliHLTHOMERInterface::GetSize( bool includeData )
    {
    return fHOMERWriter.GetTotalMemorySize( includeData );
    }

void AliHLTHOMERInterface::Copy( void* destination, bool includeData )
    {
    fHOMERWriter.Copy( destination, fEventID.fType, fEventID.fNr, fSED.GetEventStatusFlag(), (homer_uint64)fNodeID, includeData );
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
