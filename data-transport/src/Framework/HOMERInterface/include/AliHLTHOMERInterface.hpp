#ifndef _ALIHLTHOMERINTERFACE_HPP_
#define _ALIHLTHOMERINTERFACE_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "HOMERData.h"
#include "HOMERReader.h"
#include "HOMERWriter.h"
#include "AliEventID.h"
#include "AliHLTEventTriggerStruct.h"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTSubEventDescriptor.hpp"
#include "AliHLTTypes.h"

class AliHLTShmManager;

class AliHLTHOMERInterface
    {
    public:

	AliHLTHOMERInterface();
	virtual ~AliHLTHOMERInterface();

	void SetShmManager( AliHLTShmManager* shmMan )
		{
		fShmManager = shmMan;
		}

	void Clear();
	bool SetEvent( AliEventID_t eventID, const AliHLTEventTriggerStruct* ets,
		       const AliHLTSubEventDataDescriptor* sedd );

	unsigned long long GetSize( bool includeData = true );
	void Copy( void* destination, bool includeData = true );

    protected:

	AliEventID_t fEventID;
	AliHLTNodeID_t fNodeID;
	const AliHLTEventTriggerStruct* fETS;

	AliHLTShmManager* fShmManager;
	
	HOMERWriter fHOMERWriter;

	AliHLTSubEventDescriptor fSED;
	
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTHOMERINTERFACE_HPP_
