/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#include "AliHLTShmCom.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTBlockHeader.hpp"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#ifdef LIBC_MEMDEBUG
#include <mcheck.h>
#endif


#define SHMKEY 0x4711
//#define SHMSIZE 8192
#define SHMSIZE (512*1024)

#define RETRYCOUNT 10000

#define PERFORMANCE_TEST


int main( int argc, char** argv )
    {
#ifdef LIBC_MEMDEBUG
    mtrace();
#endif
    AliHLTFilteredStdoutLogServer sOut;
    gLogLevel = AliHLTLog::kAll;
    gLog.AddServer( sOut );
#ifndef PERFORMANCE_TEST
    int fd;
#endif
    LOG( AliHLTLog::kInformational, "ShmTest", "Test" ) << "Test Start" << AliHLTLog::kDec << ENDLOG;
    if ( argc<5 )
	{
	printf( "Usage: %s <server> <use-blocks-headers> <datasize> <count>\n", argv[0] );
	return -1;
	}
    char* cerr;

    int server = strtol( argv[1], &cerr, 0 );
    if ( *cerr!=0 )
	{
	printf( "Usage: %s  <server> <use-blocks-headers> <datasize> <count>\n", argv[0] );
	printf( "Error in server\n" );
	return -1;
	}

    int useBlockHeaders = strtol( argv[2], &cerr, 0 );
    if ( *cerr!=0 )
	{
	printf( "Usage: %s  <server> <use-blocks-headers> <datasize> <count>\n", argv[0] );
	printf( "Error in use-block-headers\n" );
	return -1;
	}

    int size = strtol( argv[3], &cerr, 0 );
    if ( *cerr!=0 )
	{
	printf( "Usage: %s  <server> <use-blocks-headers> <datasize> <count>\n", argv[0] );
	printf( "Error in datasize\n" );
	return -1;
	}

    AliUInt32_t count = strtoul( argv[4], &cerr, 0 );
    if ( *cerr!=0 )
	{
	printf( "Usage: %s  <server> <use-blocks-headers> <datasize> <count>\n", argv[0] );
	printf( "Error in count\n" );
	return -1;
	}


    //pid_t pid;
    //bool dostart = true;	
    bool dostart;
    dostart = (bool)server;

    AliHLTBlockHeader* pbh = (AliHLTBlockHeader*)new AliUInt8_t[ sizeof(AliHLTBlockHeader)+size ];
    AliUInt8_t* data = (AliUInt8_t*)( pbh+1 );
    if ( !pbh )
	{
	printf( "Error creating data array -  out of memory\n" );
	return -1;
	}
    pbh->fLength = sizeof(AliHLTBlockHeader)+size;
    pbh->fType.fID = (AliUInt32_t)('DATA');
    pbh->fSubType.fID = (AliUInt32_t)('TST');
    pbh->fVersion = 1;

    srandom( time(NULL) );

//     pid = fork();
//     if ( pid>0 )
// 	dostart = false;
//     if ( pid<0 )
// 	{
// 	printf( "Error forking: %d - %s\n", errno, strerror(errno) );
// 	return -1;
// 	}
//     printf( "Forked %d\n", (int)pid );

    key_t shmKey = SHMKEY;
    AliUInt32_t shmSize = SHMSIZE;
    
    AliHLTShmCom* com1;
    if ( dostart )
	{
#ifndef PERFORMANCE_TEST
	fd = open( "Server-In.dat", O_WRONLY|O_CREAT|O_TRUNC, 0644 );
#endif
	com1 = new AliHLTShmCom( shmKey, shmSize, true );
	if ( !(*com1) )
	    {
	    LOG( AliHLTLog::kError, "ShmTest", "Shm Create" )
		<< "Error creating shm segment with key 0x" << AliHLTLog::kHex
		<< shmKey << " (" << AliHLTLog::kDec << shmKey << ") of size "
		<< shmSize << " (0x" << AliHLTLog::kHex << shmSize << ") bytes."
		<< ENDLOG;
	    return 0;
	    }
	}
    else
	{
#ifndef PERFORMANCE_TEST
	fd = open( "Client-Out.dat", O_WRONLY|O_CREAT|O_TRUNC, 0644 );
#endif
	com1 = new AliHLTShmCom( shmKey, shmSize, false );
	}

    com1->SetRetryTimeout( RETRYCOUNT );

    LOG( AliHLTLog::kInformational, "ShmTest", "Test" ) 
	<< "Shm created/opened." << ENDLOG;

	
    AliUInt32_t i, n=0;
    int ret;
    struct timeval start, stop;
    gettimeofday( &start, NULL );
    unsigned long oldsleeps = 0, newsleeps;
    AliUInt8_t *p1, *p2;
    AliUInt32_t size1;
	
    for ( i = 0; i < count; i++ )
	{
	if ( dostart )
	    {
	    //ret = com1->Read( size, data, true );
	    if ( !useBlockHeaders )
		{
		ret=com1->CanRead( false, size, p1, p2, size1, true );
		if ( ret!=0 )
		    {
		    printf( "Server Error reading from shm; number %d (ret==%d (%s); errno==%d (%s)\n", i, ret, strerror(ret), errno, strerror(errno) );
		    return -1;
		    }
		if ( p2 )
		    {
		    memcpy( data, p1, size1 );
		    memcpy( data+size1, p2, size-size1 );
#ifndef PERFORMANCE_TEST
		    write ( fd, data, size );
#endif
		    }
		else
		    {
#ifndef PERFORMANCE_TEST
		    write ( fd, p1, size );
#endif
		    }
		com1->HaveRead( size, p1, p2, size1 );
		}
	    else
		{
		AliHLTBlockHeader *newbh;
		AliUInt8_t *pbhP1, *pbhP2;
		AliUInt32_t pbhSize1;
		ret = com1->Read( newbh, pbhP1, pbhP2, pbhSize1, true );
		if ( ret!=0 )
		    {
		    printf( "Server Error reading block header from shm; number %d (ret==%d (%s); errno==%d (%s)\n", i, ret, strerror(ret), errno, strerror(errno) );
		    return -1;
		    }
#ifndef PERFORMANCE_TEST
		write ( fd, newbh+1, newbh->fLength-sizeof(AliHLTBlockHeader) );
#endif
		com1->FreeRead( newbh, pbhP1, pbhP2, pbhSize1 );
		}
	    }
	else
	    {
	    int j;
	    for ( j = 0; j < size; j++ )
		{
		data[j] = (AliUInt8_t)random();
		}
	    n = 0;
	    do
		{
		if ( !useBlockHeaders )
		    ret = com1->Write( size, data, true );
		else
		    ret = com1->Write( pbh->fLength, (AliUInt8_t*)pbh, true );
		n++;
		}
	    while ( n < 100 && ret!=0 );
#ifndef PERFORMANCE_TEST
	    write( fd, data, size );
	    if ( ret!=0 )
		{
		printf( "Client Error writing %sto shm; number %d (ret==%d (%s); errno==%d (%s)\n", (useBlockHeaders ? "block " : "" ), i, ret, strerror(ret), errno, strerror(errno) );
		return -1;
		}
#endif
	    }
	newsleeps = com1->GetSleepCount();
	if ( newsleeps != oldsleeps )
	    {
	    printf( "%d: New sleep count: %lu\n", i, newsleeps );
	    oldsleeps = newsleeps;
	    }
	}
    gettimeofday( &stop, NULL );
#ifndef PERFORMANCE_TEST
    close( fd );
#endif
    if ( stop.tv_usec < start.tv_usec )
	{
	stop.tv_usec += 1000000;
	stop.tv_sec -= 1;
	}
    AliUInt32_t dt;
    dt = (stop.tv_sec-start.tv_sec)*1000000+(stop.tv_usec-start.tv_usec);

    double ds;
    ds = ((double)dt)/1000000.0;
    wait( &ret );
//     if ( dostart )
// 	{
	printf( "Time taken to send/receive %u time %u bytes: %f s = %u mus\n", 
		count, size, ds, dt );
	printf( "Time per message: %f s = %f mus\n", 
		ds/((double)count), ((double)dt)/((double)count) );
	printf( "Message frequency: %f MHz = %f Hz - transfer rate: %f B/s = %f MB/S\n", 
		((double)count)/((double)dt), ((double)count)/ds, (double)(size*count)/ds, (double)(size*count)/(ds*1024*1024) );
	printf( "Sleep count: %lu\n", com1->GetSleepCount() );
// 	}

    LOG( AliHLTLog::kInformational, "ShmTest", "Test" ) << "Test End" << ENDLOG;
    delete com1;
    }







/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

