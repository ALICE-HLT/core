/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSampleSubscriber.hpp"
#include "AliHLTPublisherPipeProxy.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTEventDoneData.hpp"
#include "MLUCHistogram.hpp"
#include <sys/time.h>



// All time units in microsec. (us.)

double CalcTimeDiff_us( struct timeval* t1, struct timeval* t2 )
    {
    double tmp;
    tmp = t2->tv_sec-t1->tv_sec;
    tmp *= 1000000;
    tmp += t2->tv_usec-t1->tv_usec;
    return tmp;
    }


class BenchmarkSubscriber: public AliHLTSampleSubscriber
    {
    public:

	BenchmarkSubscriber( const char* name );

	virtual ~BenchmarkSubscriber();

	virtual int NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
			      const AliHLTSubEventDataDescriptor& sbevent,
			      const AliHLTEventTriggerStruct& ets );
	
	void WriteHistograms( const char* filenameprefix );
	
	double GetAnnounceTimeDiff_us()
		{
		return CalcTimeDiff_us( &fFirstAnnounceTime, &fLastAnnounceTime );
		}

	unsigned long GetCount()
		{
		return fCount;
		}

    protected:

	MLUCHistogram fAnnounceTime1;
	MLUCHistogram fEventDoneTime1;

	unsigned long fCount;
	
	bool fAnnounced;
	struct timeval fFirstAnnounceTime;
	struct timeval fLastAnnounceTime;

    private:
    };


BenchmarkSubscriber::BenchmarkSubscriber( const char* name ):
    AliHLTSampleSubscriber( name ),
    fAnnounceTime1(  0, 10000000, 10 ),
    fEventDoneTime1( 0,  1000000, 10 )
    {
    fAnnounced = false;
    fCount = 0;
    }

BenchmarkSubscriber::~BenchmarkSubscriber()
    {
    }

int BenchmarkSubscriber::NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
				   const AliHLTSubEventDataDescriptor&,
				   const AliHLTEventTriggerStruct&)
    {
    struct timeval now, start, end;
    double tdiff;
    double at_tdiff;
    gettimeofday( &now, NULL );
    
    if ( fAnnounced )
	{
	at_tdiff = CalcTimeDiff_us( &fLastAnnounceTime, &now );
	fAnnounceTime1.Add( at_tdiff );
	}
    else
	{
	fFirstAnnounceTime = now;
	fAnnounced = true;
	}
    fLastAnnounceTime = now;
    gettimeofday( &start, NULL );
    AliHLTEventDoneData edd;
    AliHLTMakeEventDoneData( &edd, eventID );
    publisher.EventDone( *this, edd );
    gettimeofday( &end, NULL );
    tdiff = CalcTimeDiff_us( &start, &end );
    fEventDoneTime1.Add( tdiff );
    fCount++;
    return 0;
    }
	
void BenchmarkSubscriber::WriteHistograms( const char* filenameprefix )
    {
    MLUCString name;

    name = filenameprefix;
    name += "-AnnounceTime1.histo";
    fAnnounceTime1.Write( name.c_str() );

    name = filenameprefix;
    name += "-EventDoneTime1.histo";
    fEventDoneTime1.Write( name.c_str() );

    }
	


int main( int argc, char** argv )
    {
    AliHLTFilteredStdoutLogServer sOut;
    gLogLevel = AliHLTLog::kAll;
    gLog.AddServer( sOut );
    int i;
    char* cpErr = NULL;
    const char* errorStr = NULL;

    const char* publisherName = NULL;
    const char* subscriberID = NULL;

    const char* usage1 = "Usage: ";
    const char* usage2 = " (-V <verbosity>) -subscribe <publishername> <subscriberID>";

    i = 1;
    while ( (i < argc) && !errorStr )
	{
	if ( !strcmp( argv[i], "-subscribe" ) )
	    {
	    if ( argc <= i+2 )
		{
		errorStr = "Missing publishername or subscriberid parameter";
		break;
		}
	    publisherName = argv[i+1];
	    subscriberID = argv[i+2];
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc <= i +1 )
		{
		errorStr = "Missing verbosity level specifier.";
		break;
		}
	    AliUInt32_t tmpll = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting verbosity level specifier..";
		break;
		}
	    gLogLevel = tmpll;
	    i += 2;
	    continue;
	    }
	errorStr = "Unknown parameter";
	}

    if ( !errorStr )
	{
	if ( !publisherName || !subscriberID )
	    errorStr = "Must specify -subscribe <publishername> <subscriberID> argument";
	}
    
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "BenchmarkSubscriber", "Command line arguments" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	LOG( AliHLTLog::kError, "BenchmarkSubscriber", "Command line arguments" )
	    << errorStr << ENDLOG;
	return -1;
	}

    MLUCString name;
    name = publisherName;
    name += "-";
    name += subscriberID;

    AliHLTFileLogServer fileOut( name.c_str(), ".log", 3000000 );
    gLog.AddServer( fileOut );
    
    BenchmarkSubscriber subscriber( subscriberID );
    AliHLTPublisherPipeProxy publisher( publisherName );

    publisher.Subscribe( subscriber );

    publisher.StartPublishing( subscriber );

    
    subscriber.WriteHistograms( name.c_str() );

    double r, t = subscriber.GetAnnounceTimeDiff_us();
    unsigned long eventCount = subscriber.GetCount();

    r = ((double)eventCount)/t;

    LOG( AliHLTLog::kBenchmark, "BenchmarkSubscriber", "Benchmark" )
	<< "Time needed for receiving " << AliHLTLog::kDec << eventCount << " (0x" 
	<< AliHLTLog::kHex << eventCount << " events: " << t << " microsec. == " 
	<< t/1000000 << " s <-> " << r*1000000 << " events/s == " << r 
	<< " events/microsec." << ENDLOG;

    return 0;
    }

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
