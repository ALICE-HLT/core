ADD_EXECUTABLE(BenchmarkPublisher BenchmarkPublisher/BenchmarkPublisher.cpp)
ADD_EXECUTABLE(BenchmarkSubscriber BenchmarkSubscriber/BenchmarkSubscriber.cpp)

TARGET_LINK_LIBRARIES(BenchmarkPublisher PubSub Base MLUC)
TARGET_LINK_LIBRARIES(BenchmarkSubscriber PubSub Base MLUC)

INSTALL(TARGETS 
    BenchmarkPublisher
    BenchmarkSubscriber
    RUNTIME DESTINATION bin)
