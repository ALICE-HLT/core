/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSampleSubscriber.hpp"
#include "AliHLTPublisherPipeProxy.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTEventDoneData.h"
#include "AliHLTEventDoneData.hpp"
#include "MLUCHistogram.hpp"
#include <sys/time.h>


class EventDoneDataProducer: public AliHLTSampleSubscriber
    {
    public:

	EventDoneDataProducer( const char* name );

	virtual ~EventDoneDataProducer();

	virtual int NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
			      const AliHLTSubEventDataDescriptor& sbevent,
			      const AliHLTEventTriggerStruct& ets );
	
    protected:

	AliHLTEventDoneData* fEDD;

    private:
    };


EventDoneDataProducer::EventDoneDataProducer( const char* name ):
    AliHLTSampleSubscriber( name )
    {
    fEDD = (AliHLTEventDoneData*)new AliUInt8_t[ sizeof(AliHLTEventDoneData)+sizeof(AliEventID_t) ];
    if ( fEDD )
	{
	AliHLTMakeEventDoneData( fEDD, AliEventID_t() );
	fEDD->fDataWordCount = 2;
	fEDD->fHeader.fLength += 2*sizeof(fEDD->fDataWords[0]);
	}
    }

EventDoneDataProducer::~EventDoneDataProducer()
    {
    }

int EventDoneDataProducer::NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
				   const AliHLTSubEventDataDescriptor&,
				   const AliHLTEventTriggerStruct&)
    {
    fEDD->fEventID = eventID;
    *(AliEventID_t*)(fEDD->fDataWords) = eventID;
    publisher.EventDone( *this, *fEDD );
    return 0;
    }
	

int main( int argc, char** argv )
    {
    AliHLTFilteredStdoutLogServer sOut;
    gLogLevel = AliHLTLog::kAll;
    gLog.AddServer( sOut );
    int i;
    char* cpErr = NULL;
    char* errorStr = NULL;

    char* publisherName = NULL;
    char* subscriberID = NULL;

    const char* usage1 = "Usage: ";
    const char* usage2 = " (-V <verbosity>) -subscribe <publishername> <subscriberID>";

    i = 1;
    while ( (i < argc) && !errorStr )
	{
	if ( !strcmp( argv[i], "-subscribe" ) )
	    {
	    if ( argc <= i+2 )
		{
		errorStr = "Missing publishername or subscriberid parameter";
		break;
		}
	    publisherName = argv[i+1];
	    subscriberID = argv[i+2];
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc <= i +1 )
		{
		errorStr = "Missing verbosity level specifier.";
		break;
		}
	    AliUInt32_t tmpll = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting verbosity level specifier..";
		break;
		}
	    gLogLevel = tmpll;
	    i += 2;
	    continue;
	    }
	errorStr = "Unknown parameter";
	}

    if ( !errorStr )
	{
	if ( !publisherName || !subscriberID )
	    errorStr = "Must specify -subscribe <publishername> <subscriberID> argument";
	}
    
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "EventDoneDataProducer", "Command line arguments" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	LOG( AliHLTLog::kError, "EventDoneDataProducer", "Command line arguments" )
	    << errorStr << ENDLOG;
	return -1;
	}

    MLUCString name;
    name = publisherName;
    name += "-";
    name += subscriberID;

    AliHLTFileLogServer fileOut( name.c_str(), ".log", 3000000 );
    gLog.AddServer( fileOut );
    
    EventDoneDataProducer subscriber( subscriberID );
    AliHLTPublisherPipeProxy publisher( publisherName );

    publisher.Subscribe( subscriber );

    publisher.StartPublishing( subscriber );

    return 0;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
