/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#include "AliHLTPipeCom.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#ifdef LIBC_MEMDEBUG
#include <mcheck.h>
#endif



int main( int argc, char** argv )
    {
#ifdef LIBC_MEMDEBUG
    mtrace();
#endif
    AliHLTFilteredStdoutLogServer sOut;
    gLogLevel = AliHLTLog::kAll;
    gLog.AddServer( sOut );
    int fIn, fOut;
    LOG( AliHLTLog::kInformational, "PipeTest", "Test" ) << "Test Start" << AliHLTLog::kDec << ENDLOG;
	if ( argc<3 )
		{
		printf( "Usage: %s datasize count\n", argv[0] );
		return -1;
		}
	char* cerr;


	int size = strtol( argv[1], &cerr, 0 );
	if ( *cerr!=0 )
		{
		printf( "Usage: %s datasize count\n", argv[0] );
		printf( "Error in datasize\n" );
		return -1;
		}

	AliUInt32_t count = strtoul( argv[2], &cerr, 0 );
	if ( *cerr!=0 )
		{
		printf( "Usage: %s datasize count\n", argv[0] );
		printf( "Error in count\n" );
		return -1;
		}


	pid_t pid;
	bool dostart = true;	

	AliUInt8_t* data = new AliUInt8_t[ size ];
	if ( !data )
	    {
	    printf( "Error creating data array -  out of memory\n" );
	    return -1;
	    }

	pid = fork();
	if ( pid>0 )
	    dostart = false;
	if ( pid<0 )
	    {
	    printf( "Error forking: %d - %s\n", errno, strerror(errno) );
	    return -1;
	    }
	printf( "Forked %d\n", (int)pid );

	AliHLTPipeCom pipe1;
	if ( dostart )
	    {
	    fIn = open( "Server-In.dat", O_WRONLY|O_CREAT|O_TRUNC );
	    fOut = open( "Server-Out.dat", O_WRONLY|O_CREAT|O_TRUNC );
	    //sOut.SetPrefix( "Server" );
	    if ( pipe1.Open( "/tmp/PipeTest-Pipe1", "/tmp/PipeTest-Pipe2", true )!=0 )
		{
		printf( "Server Error opening client pipes\n" );
		return -1;
		}
	    }
	else
	    {
	    fIn = open( "Client-In.dat", O_WRONLY|O_CREAT|O_TRUNC );
	    fOut = open( "Client-Out.dat", O_WRONLY|O_CREAT|O_TRUNC );
	    //sOut.SetPrefix( "Client" );
	    AliUInt32_t c=0;
	    int res=-1;
	    while ( c++<10 && res!=0 )
		res = pipe1.Open( "/tmp/PipeTest-Pipe2", "/tmp/PipeTest-Pipe1", false );
	    if ( res!=0 )
		    {
		    printf( "Client Error opening client pipes\n" );
		    return -1;
		    }
	    }
	LOG( AliHLTLog::kInformational, "PipeTest", "Test" ) 
	    << "Pipe object state: " << pipe1 << ENDLOG;

	
	AliUInt32_t i, n=0;
	int ret;
	struct timeval start, stop;
	gettimeofday( &start, NULL );
	
	for ( i = 0; i < count; i++ )
	    {
	    if ( dostart )
		{
		do
		    {
		    ret = pipe1.Write( size, data );
		    n++;
		    }
		while ( n < 100 && ret!=0 );
		write( fOut, data, size );
		if ( ret!=0 )
		    {
		    printf( "Server Error writing to pipe; number %d (ret==%d; errno==%d)\n", i, ret, errno );
		    return -1;
		    }
		if ( pipe1.Read( size, data )!=0 )
		    {
		    printf( "Server Error reading from pipe; number %d\n", i );
		    return -1;
		    }
		write ( fIn, data, size );
		}
	    else
		{
		if ( pipe1.Read( size, data )!=0 )
		    {
		    printf( "Client Error reading from pipe; number %d\n", i );
		    return -1;
		    }
		write ( fIn, data, size );
		if ( pipe1.Write( size, data )!=0 )
		    {
		    printf( "Client Error writing to pipe; number %d\n", i );
		    return -1;
		    }
		write( fOut, data, size );
		}
	    }
	gettimeofday( &stop, NULL );
	close( fIn );
	close ( fOut );
	if ( stop.tv_usec < start.tv_usec )
	    {
	    stop.tv_usec += 1000000;
	    stop.tv_sec -= 1;
	    }
	AliUInt32_t dt;
	dt = (stop.tv_sec-start.tv_sec)*1000000+(stop.tv_usec-start.tv_usec);

	double ds;
	ds = ((double)dt)/1000000.0;
	wait( &ret );
	if ( dostart )
	    {
	    printf( "Time taken to sent+receive (roundtrip) %u time %u bytes: %f s = %u mus\n", 
		    count, size, ds, dt );
	    printf( "Time per (roundtrip) message: %f s = %f mus\n", 
		    ds/((double)count), ((double)dt)/((double)count) );
	    printf( "Roundtrip frequency: %f MHz = %f Hz - transfer rate: %f B/s = %f MB/S\n", 
		    ((double)count)/((double)dt), ((double)count)/ds, (double)(size*count)/ds, (double)(size*count)/(ds*1024*1024) );
	    }

	LOG( AliHLTLog::kInformational, "PipeTest", "Test" ) << "Test End" << ENDLOG;
    }










/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/







/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
