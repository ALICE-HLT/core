/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSamplePublisher.hpp"
#include "AliHLTSubscriberPipeProxy.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTPipeController.hpp"
#include "MLUCHistogram.hpp"
#include <sys/time.h>


// All time units in microsec. (us.)

#ifndef PROFILING
double CalcTimeDiff_us( struct timeval* t1, struct timeval* t2 )
    {
    double tmp;
    tmp = t2->tv_sec-t1->tv_sec;
    tmp *= 1000000;
    tmp += t2->tv_usec-t1->tv_usec;
    return tmp;
    }
#endif

class BenchmarkPublisher: public AliHLTSamplePublisher
    {
    public:
	BenchmarkPublisher( const char* name, int eventSlotsPowerOfTwo = -1 );

	virtual ~BenchmarkPublisher();

	void AnnounceNewEvent();

	void WriteHistograms( const char* filenameprefix );

    protected:

	virtual void CanceledEvent( AliEventID_t event );

	AliHLTSubEventDataDescriptor fSEDD;

	AliHLTEventTriggerStruct fETS;

// 	MLUCHistogram fAnnounceTime1;
// 	MLUCHistogram fCancelTime1;
// 	MLUCHistogram fRoundTripTime1;
// 	MLUCHistogram fAnnounceRate1;
// 	MLUCHistogram fThreadAnnounceTime1;
// 	MLUCHistogram fThreadAnnounceCallTime1;

// 	MLUCHistogram fAnnounceTime2;
// 	MLUCHistogram fCancelTime2;
// 	MLUCHistogram fRoundTripTime2;
// 	MLUCHistogram fAnnounceRate2;
// 	MLUCHistogram fThreadAnnounceTime2;
// 	MLUCHistogram fThreadAnnounceCallTime2;

#ifdef PROFILING
	MLUCHistogram fAnnounceTime;
	MLUCHistogram fCancelTime;
	MLUCHistogram fRoundTripTime;
	MLUCHistogram fAnnounceRate;

	MLUCHistogram fThreadAnnounceTimeNoLocks;
	MLUCHistogram fThreadAnnounceTime;
	MLUCHistogram fThreadAnnounceCallTime;

	MLUCHistogram fAnnouceEventTime;
	MLUCHistogram fAnnouceEventTimeNoLocks;

	MLUCHistogram fEventDoneTime;
	MLUCHistogram fEventDoneTimeNoLocks;

	MLUCHistogram fSignalLockTime;
	MLUCHistogram fSignalUnlockTime;
	MLUCHistogram fMutexLockTime;
	MLUCHistogram fMutexUnlockTime;
#endif

	
	bool fCanceled;
	struct timeval fLastCancelTime;

	bool fAnnounced;
	struct timeval fLastAnnounceTime;

    };




class SubscriptionListenerThread: public AliHLTThread
    {
    public:

	SubscriptionListenerThread( AliHLTSamplePublisher* pub  ):
	    fPub( pub )
		{
		}

    protected:

	virtual void Run()
		{
		if ( fPub )
		    fPub->AcceptSubscriptions();
		}

	AliHLTSamplePublisher* fPub;

    };


#ifdef PROFILING
#define STDHISTO(histo) histo(0,10000,1)
#define ADDHISTO(histos,name) histos.insert( histos.end(), &name )
#else
#define STDHISTO(histo)
#define ADDHISTO(histos,name)
#endif


BenchmarkPublisher::BenchmarkPublisher( const char* name, int eventSlotsPowerOfTwo ):
    AliHLTSamplePublisher( name, eventSlotsPowerOfTwo )
#ifdef PROFILING
,
//     fAnnounceTime1          ( 0,  1000000, 10 ),
//     fCancelTime1            ( 0,  1000000, 10 ),
//     fRoundTripTime1         ( 0, 10000000, 10 ),
//     fAnnounceRate1          ( 0, 10000000, 10 ),
//     fThreadAnnounceTime1    ( 0,  1000000, 10 ),
//     fThreadAnnounceCallTime1( 0,  1000000, 10 ),
//     fAnnounceTime2          ( 0,    10000,  1 ),
//     fCancelTime2            ( 0,    10000,  1 ),
//     fRoundTripTime2         ( 0,    10000,  1 ),
//     fAnnounceRate2          ( 0,    10000,  1 ),
//     fThreadAnnounceTime2    ( 0,    10000,  1 ),
//     fThreadAnnounceCallTime2( 0,    10000,  1 )
    STDHISTO(fAnnounceTime),
    STDHISTO(fCancelTime),
    STDHISTO(fRoundTripTime),
    STDHISTO(fAnnounceRate),
    
    STDHISTO(fThreadAnnounceTimeNoLocks),
    STDHISTO(fThreadAnnounceTime),
    STDHISTO(fThreadAnnounceCallTime),
    
    STDHISTO(fAnnouceEventTime),
    STDHISTO(fAnnouceEventTimeNoLocks),
    
    STDHISTO(fEventDoneTime),
    STDHISTO(fEventDoneTimeNoLocks),
    
    STDHISTO(fSignalLockTime),
    STDHISTO(fSignalUnlockTime),
    STDHISTO(fMutexLockTime),
    STDHISTO(fMutexUnlockTime)
#endif
    {
    fSEDD.fHeader.fLength = sizeof(fSEDD);
    fSEDD.fHeader.fType.fID = ALIL3SUBEVENTDATADESCRIPTOR_TYPE;
    fSEDD.fHeader.fSubType.fID = 0;
    fSEDD.fHeader.fVersion = 5;
    fSEDD.fOldestEventBirth_s = time(NULL);
    fSEDD.fDataType.fID = UNKNOWN_DATAID;
    fSEDD.fDataBlockCount = 0;

    fETS.fHeader.fLength = sizeof(fETS);
    fETS.fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
    fETS.fHeader.fSubType.fID = 0;
    fETS.fHeader.fVersion = 1;
    fETS.fDataWordCount = 0;

    fCanceled = false;
    fAnnounced = false;

    
// #ifdef PROFILING
//     fSubscriberSendThreadNewEventHistos.insert( fSubscriberSendThreadNewEventHistos.end(), &fThreadAnnounceTime1 );
//     fSubscriberSendThreadNewEventHistos.insert( fSubscriberSendThreadNewEventHistos.end(), &fThreadAnnounceTime2 );
//     fSubscriberSendThreadNewEventCallHistos.insert( fSubscriberSendThreadNewEventCallHistos.end(), &fThreadAnnounceCallTime1 );
//     fSubscriberSendThreadNewEventCallHistos.insert( fSubscriberSendThreadNewEventCallHistos.end(), &fThreadAnnounceCallTime2 );
// #endif


    ADDHISTO(fSubscriberSendThreadNewEventNoLocksHistos,fThreadAnnounceTimeNoLocks);
    ADDHISTO(fSubscriberSendThreadNewEventHistos,fThreadAnnounceTime);
    ADDHISTO(fSubscriberSendThreadNewEventCallHistos,fThreadAnnounceCallTime);
    
    ADDHISTO(fAnnounceEventTimeHistos,fAnnouceEventTime);
    ADDHISTO(fAnnounceEventTimeNoLocksHistos,fAnnouceEventTimeNoLocks);
    
    ADDHISTO(fEventDoneTimeHistos,fEventDoneTime);
    ADDHISTO(fEventDoneTimeNoLocksHistos,fEventDoneTimeNoLocks);
    
    ADDHISTO(fSignalLockTimeHistos,fSignalLockTime);
    ADDHISTO(fSignalUnlockTimeHistos,fSignalUnlockTime);
    ADDHISTO(fMutexUnlockTimeHistos,fMutexLockTime);
    ADDHISTO(fSignalLockTimeHistos,fMutexUnlockTime);

    }


BenchmarkPublisher::~BenchmarkPublisher()
    {
    }


void BenchmarkPublisher::AnnounceNewEvent()
    {
    struct timeval start;
#ifdef PROFILING
    struct timeval end;
    double tdiff;
#endif

    gettimeofday( &start, NULL );
    fSEDD.fEventID = start.tv_sec;
    fSEDD.fEventID <<= 32;
    fSEDD.fEventID |= start.tv_usec;
    fSEDD.fEventBirth_s = start.tv_sec;
    fSEDD.fEventBirth_us = start.tv_usec;
#ifdef PROFILING
    gettimeofday( &start, NULL );
#endif
    AnnounceEvent( fSEDD.fEventID, fSEDD, fETS );
#ifdef PROFILING
    gettimeofday( &end, NULL );
    tdiff = CalcTimeDiff_us( &start, &end );
//     fAnnounceTime1.Add( tdiff );
//     fAnnounceTime2.Add( tdiff );
    fAnnounceTime.Add( tdiff );
    if ( fAnnounced )
	{
	double ar_tdiff;
	ar_tdiff = CalcTimeDiff_us( &fLastAnnounceTime, &start );
// 	fAnnounceRate1.Add( ar_tdiff );
// 	fAnnounceRate2.Add( ar_tdiff );
	fAnnounceRate.Add( ar_tdiff );
	}
    else
	fAnnounced = true;
    fLastAnnounceTime = start;
#endif
    }


#ifdef PROFILING
#define WRITEHISTO(histoname) name=filenameprefix;name+="-"#histoname".histo";f##histoname.Write(name.c_str());
#else
#define WRITEHISTO(histoname)
#endif

void BenchmarkPublisher::WriteHistograms( const char* /*filenameprefix*/ )
    {
    MLUCString name;

    WRITEHISTO(AnnounceTime);
    WRITEHISTO(CancelTime);
    WRITEHISTO(RoundTripTime);
    WRITEHISTO(AnnounceRate);
    
    WRITEHISTO(ThreadAnnounceTimeNoLocks);
    WRITEHISTO(ThreadAnnounceTime);
    WRITEHISTO(ThreadAnnounceCallTime);
    
    WRITEHISTO(AnnouceEventTime);
    WRITEHISTO(AnnouceEventTimeNoLocks);
    
    WRITEHISTO(EventDoneTime);
    WRITEHISTO(EventDoneTimeNoLocks);
    
    WRITEHISTO(SignalLockTime);
    WRITEHISTO(SignalUnlockTime);
    WRITEHISTO(MutexLockTime);
    WRITEHISTO(MutexUnlockTime);


//     name = filenameprefix;
//     name += "-AnnounceTimeOverhead1.histo";
//     fAnnounceTime1.Write( name.c_str() );

//     name = filenameprefix;
//     name += "-AnnounceTimeOverhead2.histo";
//     fAnnounceTime2.Write( name.c_str() );
    
//     name = filenameprefix;
//     name += "-CancelTime1.histo";
//     fCancelTime1.Write( name.c_str() );

//     name = filenameprefix;
//     name += "-CancelTime2.histo";
//     fCancelTime2.Write( name.c_str() );
    
//     name = filenameprefix;
//     name += "-RoundTripTime1.histo";
//     fRoundTripTime1.Write( name.c_str() );

//     name = filenameprefix;
//     name += "-RoundTripTime2.histo";
//     fRoundTripTime2.Write( name.c_str() );

//     name = filenameprefix;
//     name += "-AnnounceRate1.histo";
//     fAnnounceRate1.Write( name.c_str() );

//     name = filenameprefix;
//     name += "-AnnounceRate2.histo";
//     fAnnounceRate2.Write( name.c_str() );

//     name = filenameprefix;
//     name += "-ThreadAnnounceTime1.histo";
//     fThreadAnnounceTime1.Write( name.c_str() );

//     name = filenameprefix;
//     name += "-ThreadAnnounceTime2.histo";
//     fThreadAnnounceTime2.Write( name.c_str() );

//     name = filenameprefix;
//     name += "-ThreadAnnounceCallTime1.histo";
//     fThreadAnnounceCallTime1.Write( name.c_str() );

//     name = filenameprefix;
//     name += "-ThreadAnnounceCallTime2.histo";
//     fThreadAnnounceCallTime2.Write( name.c_str() );
    }


#ifdef PROFILING
void BenchmarkPublisher::CanceledEvent( AliEventID_t event )
#else
void BenchmarkPublisher::CanceledEvent( AliEventID_t )
#endif
    {
#ifdef PROFILING
    struct timeval st, now;
    gettimeofday( &now, NULL );
    st.tv_sec = (unsigned)(event >> 32);
    st.tv_usec = (unsigned)(event & 0xFFFFFFFFLL);
    double rt_tdiff = CalcTimeDiff_us( &st, &now );
//     fRoundTripTime1.Add( rt_tdiff );
//     fRoundTripTime2.Add( rt_tdiff );
    fRoundTripTime.Add( rt_tdiff );
    if ( fCanceled )
	{
	double tdiff;
	tdiff = CalcTimeDiff_us( &fLastCancelTime, &now );
// 	fCancelTime1.Add( tdiff );
// 	fCancelTime2.Add( tdiff );
	fCancelTime.Add( tdiff );
	}
    else
	fCanceled = true;
    fLastCancelTime = now;
#endif
    }


int main( int argc, char** argv )
    {
    int i;
    AliHLTFilteredStdoutLogServer sOut;
    gLogLevel = AliHLTLog::kAll;
    gLog.AddServer( sOut );
    const char* publisherName = NULL;
    const char* errorStr = NULL;
    char* cpErr;

    unsigned long eventTime = 0;
    AliUInt32_t preEvtCnt=0;
    int preEvtPower2 = -1;
    unsigned long maxEvents = 0x400;
    unsigned long eventCount = 0x100000;
    
    
    const char* usage1 = "Usage: ";
    const char* usage2 = " (-V <verbosity>) -publisher <publishername> (-eventtime <interval_usec>) (-eventcount <max-event-nr>) (-maxevents <max-event-count-in-system>)";
    
    i = 1;
    while ( (i < argc) && !errorStr )
	{
	if ( !strcmp( argv[i], "-publisher" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing publishername parameter";
		break;
		}
	    publisherName = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc <= i +1 )
		{
		errorStr = "Missing verbosity level specifier.";
		break;
		}
	    AliUInt32_t tmpll = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting verbosity level specifier..";
		break;
		}
	    gLogLevel = tmpll;
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-eventcount" ) )
	    {
	    if ( argc <= i +1 )
		{
		errorStr = "Missing event count specifier.";
		break;
		}
	    eventCount = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting event count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-eventtime" ) )
	    {
	    if ( argc <= i +1 )
		{
		errorStr = "Missing event time interval specifier.";
		break;
		}
	    eventTime = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting event time interval specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-maxevents" ) )
	    {
	    if ( argc <= i +1 )
		{
		errorStr = "Missing max in system event count specifier.";
		break;
		}
	    maxEvents = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting max in system event count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	errorStr = "Unknown parameter";
	}

    if ( !errorStr )
	{
	if ( !publisherName )
	    errorStr = "Must specify -publisher publishername argument";
	}
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "BenchmarkPublisher", "Command line arguments" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	LOG( AliHLTLog::kError, "BenchmarkPublisher", "Command line arguments" )
	    << errorStr << ENDLOG;
	return -1;
	}
    
    AliHLTFileLogServer fileOut( publisherName, ".log", 3000000 );
    gLog.AddServer( fileOut );

    AliHLTPipeController pipeControl( publisherName );
    pipeControl.AddValue( *(AliUInt32_t*)&gLogLevel );
    pipeControl.Start();

    if ( !preEvtCnt )
	{
	preEvtCnt = maxEvents;
	AliUInt32_t mask = 1;
	preEvtPower2 = sizeof(mask)*8 - 1;
	mask <<= preEvtPower2;
	while ( ! (mask & preEvtCnt) )
		{
		mask >>= 1;
		preEvtPower2--;
		}
	if ( preEvtPower2<(int)(sizeof(mask)*8-1) )
	    preEvtPower2++;
	}

    
    BenchmarkPublisher publisher( publisherName, preEvtPower2 );

    publisher.SetSubscriptionLoop( &PublisherPipeSubscriptionInputLoop );
    SubscriptionListenerThread publisherThread( &publisher );
    

    AliUInt32_t publish = 0;
    AliUInt32_t quitPublisher = 0;

    pipeControl.AddValue( publish );
    pipeControl.AddValue( quitPublisher );
    pipeControl.SetQuitValue( quitPublisher );

    publisherThread.Start();

    while ( !publish )
	usleep( 500000 );


    unsigned long events = 0;
    struct timeval start, end1, end2, sleepstart, sleepend;
    double sleeptime1 = 0, sleeptime2 = 0;
    gettimeofday( &start, NULL );
    while ( !quitPublisher && events < eventCount )
	{
	if ( publisher.GetPendingEventCount() < maxEvents )
	    {
	    publisher.AnnounceNewEvent();
	    events++;
	    }
	else
	    {
	    gettimeofday( &sleepstart, NULL );
	    usleep( 10000 );
	    gettimeofday( &sleepend, NULL );
	    sleeptime1 += CalcTimeDiff_us( &sleepstart, &sleepend );
	    }
	}

    gettimeofday( &end1, NULL );

    while ( quitPublisher<=1 && publisher.GetPendingEventCount()>0 )
	{
	gettimeofday( &sleepstart, NULL );
	usleep( 500000 );
	gettimeofday( &sleepend, NULL );
	sleeptime2 += CalcTimeDiff_us( &sleepstart, &sleepend );
	}
    
    gettimeofday( &end2, NULL );

    double t1, t2, r1, r2, ts1, ts2, rs1, rs2;
    t1 = CalcTimeDiff_us( &start, &end1 );
    t2 = CalcTimeDiff_us( &start, &end2 );
    ts1 = t1-sleeptime1;
    ts2 = t2-sleeptime1-sleeptime2;

    r1 = ((double)eventCount)/t1;
    r2 = ((double)eventCount)/t2;

    rs1 = ((double)eventCount)/ts1;
    rs2 = ((double)eventCount)/ts2;

    LOG( AliHLTLog::kBenchmark, "BenchmarkPublisher", "Benchmark" )
	<< "Time needed for announcing " << AliHLTLog::kDec << eventCount << " (0x" 
	<< AliHLTLog::kHex << eventCount << " events (max. events in transit: "
	<< AliHLTLog::kDec << maxEvents << " (0x" << AliHLTLog::kHex << maxEvents
	<<  ")): " << t1 << " microsec. == " << t1/1000000 << " s <-> " 
	<< r1*1000000 << " events/s == " << r1 << " events/microsec." << ENDLOG;


    LOG( AliHLTLog::kBenchmark, "BenchmarkPublisher", "Benchmark" )
	<< "Time needed for announcing " << AliHLTLog::kDec << eventCount << " (0x" 
	<< AliHLTLog::kHex << eventCount << " events (max. events in transit without sleep times: "
	<< AliHLTLog::kDec << maxEvents << " (0x" << AliHLTLog::kHex << maxEvents
	<<  ")): " << ts1 << " microsec. == " << ts1/1000000 << " s <-> " 
	<< rs1*1000000 << " events/s == " << rs1 << " events/microsec." << ENDLOG;


    LOG( AliHLTLog::kBenchmark, "BenchmarkPublisher", "Benchmark" )
	<< "Time needed for announcing and receiving back " << AliHLTLog::kDec << eventCount << " (0x" 
	<< AliHLTLog::kHex << eventCount << " events (max. events in transit: "
	<< AliHLTLog::kDec << maxEvents << " (0x" << AliHLTLog::kHex << maxEvents
	<<  ")): " << t2 << " microsec. == " << t2/1000000 << " s <-> " 
	<< r2*1000000 << " events/s == " << r2 << " events/microsec." << ENDLOG;


    LOG( AliHLTLog::kBenchmark, "BenchmarkPublisher", "Benchmark" )
	<< "Time needed for announcing and receiving back " << AliHLTLog::kDec << eventCount << " (0x" 
	<< AliHLTLog::kHex << eventCount << " events (max. events in transit without sleep times: "
	<< AliHLTLog::kDec << maxEvents << " (0x" << AliHLTLog::kHex << maxEvents
	<<  ")): " << ts2 << " microsec. == " << ts2/1000000 << " s <-> " 
	<< rs2*1000000 << " events/s == " << rs2 << " events/microsec." << ENDLOG;

    //sleep( 1 );
    //sleep( 1 );
    QuitPipeSubscriptionLoop( publisher );

  publisher.CancelAllSubscriptions( true );
  

    publisher.WriteHistograms( publisherName );

    quitPublisher = 1;
    struct timeval now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 10000000;
    gettimeofday( &start, NULL );
    while ( !pipeControl.HasQuitted() && deltaT<timeLimit )
	{
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( pipeControl.HasQuitted() )
	pipeControl.Join();
    else
	{
	LOG( AliHLTLog::kWarning, "MultFilePublisher", "Aborting PipeController" )
	    << "PipeController not ended. Aborting now..." << ENDLOG;
	pipeControl.Abort();
	}


    return 0;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
