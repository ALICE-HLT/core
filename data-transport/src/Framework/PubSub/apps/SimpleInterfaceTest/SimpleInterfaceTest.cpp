/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#include "AliHLTPipeCom.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTEventTriggerStruct.h"
#include "AliHLTSamplePublisher.hpp"
#include "AliHLTPublisherPipeProxy.hpp"
#include "AliHLTSampleSubscriber.hpp"
#include "AliHLTSubscriberPipeProxy.hpp"
#include "AliHLTThread.hpp"
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "MLUCString.hpp"
#include <iostream>
#include <fstream>
#ifdef LIBC_MEMDEBUG
#include <mcheck.h>
#endif

// class TestFilteredLogServer: public AliHLTLogServer
//     {
// 	public:


// 	virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, const char* cdate,
// 			      const char* ctime, AliHLTLog::TLogLevel logLvl, const char* hostname,
// 			      AliUInt32_t ldate, AliUInt32_t ltime_s, AliUInt32_t ltime_us, AliUInt32_t msgNr, const char* line );

// 	virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, const char* cdate,
// 			      const char* ctime, AliHLTLog::TLogLevel, const char* hostname,
// 			      AliUInt32_t ldate, AliUInt32_t ltime_s, AliUInt32_t ltime_us, AliUInt32_t msgNr, AliUInt32_t subMsgNr, const char* line );

// 	virtual ~TestFilteredLogServer() {};

// 	protected:

// 	private:
//     };



// void TestFilteredLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, const char* cdate,
// 				     const char* ctime, AliHLTLog::TLogLevel logLvl, const char* hostname,
// 				     AliUInt32_t ldate, AliUInt32_t ltime_s, AliUInt32_t ltime_us, AliUInt32_t msgNr, const char* line )
//     {
//     const char* clvl;
//     switch ( logLvl )
// 	{
// 	case AliHLTLog::kDebug: clvl = "Debug"; break;
// 	case AliHLTLog::kInformational: clvl = "Information"; break;
// 	case AliHLTLog::kWarning: clvl = "Warning"; break;
// 	case AliHLTLog::kError: clvl = "Error"; break;
// 	case AliHLTLog::kFatal: clvl = "Fatal Error"; break;
// 	default: clvl = "Unknown"; break;
// 	}
// //     printf( "%s %08d.%06d %10u (%s/%s) %s: %s\n", hostname, ldate, ltime,
// // 	    msgNr, (const char*)origin, 
// // 	    (const char*)keyword, clvl, (const char*)line );
//     printf( "%10u (%s/%s) %s: %s\n", msgNr, origin.c_str(), 
// 	    keyword.c_str(), clvl, line.c_str() );
//     }

// void TestFilteredLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, const char* cdate,
// 				     const char* ctime, AliHLTLog::TLogLevel logLvl, const char* hostname,
// 				     AliUInt32_t ldate, AliUInt32_t ltime_s, AliUInt32_t ltime_us, AliUInt32_t msgNr, AliUInt32_t subMsgNr, const char* line )
//     {
//     const char* clvl;
//     switch ( logLvl )
// 	{
// 	case AliHLTLog::kDebug: clvl = "Debug"; break;
// 	case AliHLTLog::kInformational: clvl = "Information"; break;
// 	case AliHLTLog::kWarning: clvl = "Warning"; break;
// 	case AliHLTLog::kError: clvl = "Error"; break;
// 	case AliHLTLog::kFatal: clvl = "Fatal Error"; break;
// 	default: clvl = "Unknown"; break;
// 	}
// //     printf( "%s %08d.%06d %10u/%10u (%s/%s)%s: %s\n", hostname, ldate, ltime,
// // 	    msgNr, subMsgNr, (const char*)origin, 
// // 	    (const char*)keyword, clvl, (const char*)line );
//     printf( "%10u/%10u (%s/%s)%s: %s\n", msgNr, subMsgNr, (const char*)origin.c_str(), 
// 	    (const char*)keyword.c_str(), clvl, (const char*)line.c_str() );
//     }


AliUInt32_t gCount=0;


class TestSubscriptionThread: 
    public AliHLTThread
    {
    public:

	TestSubscriptionThread( AliHLTSamplePublisher& pub ):
	    fPub( pub )
		{
		}
	    

    protected:

	virtual void Run();

	AliHLTSamplePublisher& fPub;

    };

void TestSubscriptionThread::Run()
    {
    fPub.AcceptSubscriptions();
    }


class SimpleInterfaceTestSubscriber: public AliHLTSampleSubscriber
    {
    public:

	SimpleInterfaceTestSubscriber( const char* name, int fh, bool pers ):
	    AliHLTSampleSubscriber( name )
		{
		fFH = fh;
		fPers = pers;
		}

	virtual int NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
			      const AliHLTSubEventDataDescriptor& sbevent,
			      const AliHLTEventTriggerStruct& ets );

	virtual int EventCanceled( AliHLTPublisherInterface& publisher, AliEventID_t eventID );

    protected:

	int fFH;
	bool fPers;

    };

int SimpleInterfaceTestSubscriber::NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
					     const AliHLTSubEventDataDescriptor& sbevent,
					     const AliHLTEventTriggerStruct& ets )
    {
    gCount++;
    write( fFH, &sbevent, sbevent.fHeader.fLength );
    if ( fPers )
	return AliHLTSampleSubscriber::NewEvent( publisher, eventID, sbevent, ets );
    else
	{
	LOG( AliHLTLog::kInformational, "SimpleInterfaceTestSubscriber", "New event" )
	    << "Transient subscriber received new event notification for event " 
	    << eventID << "." << ENDLOG;
	return 0;
	}
    }

int SimpleInterfaceTestSubscriber::EventCanceled( AliHLTPublisherInterface& publisher, AliEventID_t eventID )
    {
    LOG( AliHLTLog::kInformational, "SimpleInterfaceTestSubscriber", "Event canceled" )
	    << "Transient subscriber event canceled" << ENDLOG;
    return AliHLTSampleSubscriber::EventCanceled( publisher, eventID );
    }


int main( int argc, char** argv )
    {
#ifdef LIBC_MEMDEBUG
    mtrace();
#endif
    struct timeval start1, start2, end2, end1;
    AliUInt32_t t1, t2;
    gettimeofday( &start1, NULL );
    AliHLTFilteredStdoutLogServer sOut;
    //AliHLTStdoutLogServer sOut;
    gLogLevel = AliHLTLog::kAll;
    gLog.AddServer( sOut );
    if ( argc<4 || argc>5 )
	{
	LOG( AliHLTLog::kError, "SimpleInterfaceTest", "Parameters" ) 
	    << "Wrong command line arguments: \n" 
	    << "Usage: " << argv[0] << " (-v|-q) (-client clientID (-persistent|-transient)|-server eventcount)" << ENDLOG;
	return -1;
	}
    

    char* cerr;
    int outFile;
    bool verbose = true;
    MLUCString clientName;
    bool persistent;
    MLUCString outName;
    cerr = argv[1];
    while ( *cerr )
	{
	*cerr = tolower( *cerr );
	cerr++;
	}
    cerr = argv[2];
    while ( *cerr )
	{
	*cerr = tolower( *cerr );
	cerr++;
	}

    if ( strcmp( argv[1], "-v" ) && strcmp( argv[1], "-q" ) )
	{
	LOG( AliHLTLog::kError, "SimpleInterfaceTest", "Parameters" ) 
	    << "Wrong command line arguments: \n" 
	    << "Usage: " << argv[0] << " (-v|-q) (-client clientID (-persistent|-transient)|-server eventcount)" << ENDLOG;
	return -1;
	}
    if ( !strcmp( argv[1], "-q" ) )
	verbose = false;
    LOG( AliHLTLog::kInformational, "SimpleInterfaceTest", "Verbosity" )
	<< "Running " << (verbose ? "verbose" : "quit") << ENDLOG;

    if ( strcmp( argv[2], "-client" ) && strcmp( argv[2], "-server" ) )
	{
	LOG( AliHLTLog::kError, "SimpleInterfaceTest", "Parameters" ) 
	    << "Wrong command line arguments: \n" 
	    << "Usage: " << argv[0] << " (-v|-q) (-client clientID (-persistent|-transient)|-server eventcount)" << ENDLOG;
	return -1;
	}

    bool server;
    if ( !strcmp( argv[2], "-client" ) )
	server = false;
    else
	server = true;

    if ( (!server && argc!=5) || (server && argc!=4) )
	{
	LOG( AliHLTLog::kError, "SimpleInterfaceTest", "Parameters" ) 
	    << "Wrong command line arguments: \n" 
	    << "Usage: " << argv[0] << " (-v|-q) (-client clientID (-persistent|-transient)|-server eventcount)" << ENDLOG;
	return -1;
	}
    if ( !server )
	clientName = argv[3];
      
    MLUCString strName;
    strName = "SimpleInterfaceTest";
    strName += argv[2];
    if ( !server )
	{
	strName += "-";
	strName += clientName;
	}
    strName += ".log";
    ofstream of;
    of.open( strName.c_str() );
    AliHLTStreamLogServer streamOut( of );
    gLog.AddServer( streamOut );

    LOG( AliHLTLog::kInformational, "SimpleInterfaceTest", "Start" ) << "Test Start" << AliHLTLog::kDec << ENDLOG;


    outName = "SimpleInterfaceTest";
    outName += argv[2];
    if ( !server )
	{
	outName += "-";
	outName += clientName;
	}
    outName += ".data";
    outFile = open( (const char*)outName.c_str(), O_WRONLY|O_CREAT|O_TRUNC, 0644 );
    if ( outFile==-1 )
	{
	LOG( AliHLTLog::kError, "SimpleInterfaceTest", "Outfile open" )
	    << "Error opening outfile " << outName.c_str() << ". Reason: "
	    << strerror(errno) << " (" << errno << ")." << ENDLOG;
	return 0;
	}

    if ( server )
	{
	// -------------------------------------------------------------------------
	// Server Code
	// -------------------------------------------------------------------------
	gCount = strtol( argv[3], &cerr, 0 );
	if ( *cerr!=0 )
	    {
	    LOG( AliHLTLog::kError, "SimpleInterfaceTest", "Parameters" ) 
		<< "Wrong command line arguments: \n" 
		<< "Usage: " << argv[0] << " (-v|-q) (-client clientID (-persistent|-transient)|-server eventcount)\n "
		<< "Error in numeric eventcount argument" << ENDLOG;
	    return -1;
	    }

	
	bool started = true;
	AliHLTSamplePublisher publisher( "SamplePublisher" );
	publisher.SetComTimeout( 1000000 );
	publisher.SetPingTimeout( 1000 );
	publisher.SetMaxPingCount( 10 );
	publisher.SetSubscriptionLoop( &PublisherPipeSubscriptionInputLoop );
	TestSubscriptionThread pubThread( publisher );
	if ( pubThread.Start() == AliHLTThread::kFailed )
	    {
	    LOG( AliHLTLog::kError, "SimpleInterfaceTest", "Subscription thread" ) 
		<< "Error starting subscription thread." << ENDLOG;
	    started = false;
	    }
	    
	
	AliHLTSubEventDataDescriptor sedd;
	sedd.fHeader.fLength = sizeof( AliHLTSubEventDataDescriptor );
	sedd.fHeader.fType.fID = ALIL3SUBEVENTDATADESCRIPTOR_TYPE;
	sedd.fHeader.fSubType.fID = 0;
	sedd.fHeader.fVersion = 5;
	sedd.fDataType.fID = UNKNOWN_DATAID;
	sedd.fDataBlockCount = 0;
	AliHLTEventTriggerStruct ets;
	ets.fHeader.fLength = sizeof( AliHLTEventTriggerStruct );
	ets.fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
	ets.fHeader.fSubType.fID = 0;
	ets.fHeader.fVersion = 1;
	ets.fDataWordCount = 0;

	AliUInt32_t events = 0;

	LOG( AliHLTLog::kInformational, "SimpleInterfaceTest", "Waiting" ) 
	    << "Waiting for enter before starting publishing" << ENDLOG;
	int tmp;
	scanf( "%d", &tmp );
	//sleep( 10 );
	LOG( AliHLTLog::kInformational, "SimpleInterfaceTest", "Publishing" ) 
	    << "Now starting publishing" << ENDLOG;
	if ( !verbose )
	    gLogLevel = AliHLTLog::kAll & ~(AliHLTLog::kInformational|AliHLTLog::kDebug);//AliHLTLog::kError|AliHLTLog::kFatal;
	else
	    gLogLevel = AliHLTLog::kAll;
	gettimeofday( &start2, NULL );
	while ( events < gCount )
	    {
	    sedd.fEventID = events;
	    write( outFile, &sedd, sedd.fHeader.fLength );
	    publisher.AnnounceEvent( events, sedd, ets );
	    events++;
	    }
	gettimeofday( &end2, NULL );

	LOG( AliHLTLog::kInformational, "SimpleInterfaceTest", "Publishing" ) 
	    << "Finished publishing" << ENDLOG;
	
	while ( publisher.GetPendingEventCount()>0 )
	    usleep( 100000 );
// 	scanf( "%d", &tmp );
// 	if ( gCount < 1000 )
// 	    sleep( 1 );
// 	else
// 	    sleep( gCount/1000 );
	gLogLevel = AliHLTLog::kAll;

	//pubThread.Abort();

	QuitPipeSubscriptionLoop( publisher );
	}
    else
	{
	// -------------------------------------------------------------------------
	// Client Code
	// -------------------------------------------------------------------------

	cerr = argv[4];
	while ( *cerr )
	    {
	    *cerr = tolower( *cerr );
	    cerr++;
	    }
	if ( strcmp( argv[4], "-persistent" ) && strcmp( argv[4], "-transient" ) )
	    {
	    LOG( AliHLTLog::kError, "SimpleInterfaceTest", "Parameters" ) 
		<< "Wrong command line arguments: \n" 
		<< "Usage: " << argv[0] << " (-v|-q) (-client clientID (-persistent|-transient)|-server eventcount)" << ENDLOG;
	    return -1;
	    }
	if ( !strcmp( argv[4], "-persistent" ) )
	    persistent = true;
	else
	    persistent = false;
	LOG( AliHLTLog::kInformational, "SimpleInterfaceTest", "Persistence" )
	    << "Running " << (persistent ? "persistent" : "transient") << ENDLOG;

	MLUCString subName = "SimpleInterfaceTestSubscriber-";
	subName += clientName;
	SimpleInterfaceTestSubscriber subscriber( subName.c_str(), outFile, persistent );
	AliHLTPublisherPipeProxy publisher( "SamplePublisher" );
	publisher.SetTimeout( 1000000 );

	if ( !publisher )
	    {
	    LOG( AliHLTLog::kError, "SimpleInterfaceTest", "Publisher proxy" )
		<< "Error on creation of publisher proxy object for publisher "
		<< publisher.GetName() << "." << ENDLOG;
	    }
	else
	    {
	    LOG( AliHLTLog::kInformational, "SimpleInterfaceTest", "Subscribing" ) 
		<< "Now subscribing" << ENDLOG;
	    if ( !verbose )
		gLogLevel = AliHLTLog::kAll & ~(AliHLTLog::kInformational|AliHLTLog::kDebug);//AliHLTLog::kError|AliHLTLog::kFatal;
	    else
		gLogLevel = AliHLTLog::kAll;
	    publisher.Subscribe( subscriber );
	    if ( !persistent )
		{
		publisher.SetPersistent( subscriber, persistent );
		publisher.SetTransientTimeout( subscriber, 200 );
		}
	    gettimeofday( &start2, NULL );
	    publisher.StartPublishing( subscriber );
	    gLogLevel = AliHLTLog::kAll;
	    gettimeofday( &end2, NULL );
	    }
	}

    close( outFile );
    gettimeofday( &end1, NULL );

    if ( end1.tv_usec < start1.tv_usec )
	{
	end1.tv_usec += 1000000;
	end1.tv_sec -= 1;
	}
    t1 = (end1.tv_sec-start1.tv_sec)*1000000+(end1.tv_usec-start1.tv_usec);
    
    if ( end2.tv_usec < start2.tv_usec )
	{
	end2.tv_usec += 1000000;
	end2.tv_sec -= 1;
	}
    t2 = (end2.tv_sec-start2.tv_sec)*1000000+(end2.tv_usec-start2.tv_usec);

    LOG( AliHLTLog::kInformational, "SimpleInterfaceTest", "Results" ) 
	<< "Time needed for " << gCount << " messages: " << t2 
	<< " usec. " << ((double)t2)/((double)gCount) << " usec/message." << ENDLOG;
    LOG( AliHLTLog::kInformational, "SimpleInterfaceTest", "Results" ) 
	<< "Total time needed for " << gCount << " messages: " << t1 
	<< " usec. " << ((double)t1)/((double)gCount) << " usec/message." << ENDLOG;
    LOG( AliHLTLog::kInformational, "SimpleInterfaceTest", "Results" ) 
	<< "Overhead: " << t1-t2 << " usec." << ENDLOG;

    LOG( AliHLTLog::kInformational, "SimpleInterfaceTest", "End" ) << "Test End" << ENDLOG;
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

