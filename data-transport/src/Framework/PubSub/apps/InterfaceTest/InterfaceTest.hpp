/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _INTERFACETEST_HPP_
#define _INTERFACETEST_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSamplePublisher.hpp"
#include "AliHLTSampleSubscriber.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTThread.hpp"
#include "AliHLTTimer.hpp"
#include <pthread.h>
#include <vector>

typedef AliHLTFilteredStdoutLogServer TestFilteredLogServer;


class TestSubscriptionThread: 
    public AliHLTThread
    {
    public:

	TestSubscriptionThread( AliHLTSamplePublisher& pub ):
	    fPub( pub )
		{
		}
	    

    protected:

	virtual void Run();

	AliHLTSamplePublisher& fPub;

    };


class TestProcessingThread: public AliHLTThread
    {
    public:
	
	TestProcessingThread( AliHLTSubscriberInterface& fSub, 
			      AliHLTPublisherInterface& pub, bool pers, bool lazy, 
			      AliUInt32_t procT, AliUInt32_t procTRand, int fd );

	~TestProcessingThread();

	void NewEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent,
		       const AliHLTEventTriggerStruct& trigger );
	void EventCanceled( AliEventID_t );
	void ReleaseEventsRequest();

	void Quit();

	AliUInt32_t GetCount()
		{
		return fCount;
		}

    protected:

	void Run();

	AliHLTSubscriberInterface& fSub;
	AliHLTPublisherInterface& fPub;
	AliHLTTimerConditionSem fSem;
	AliUInt32_t fProcT;
	AliUInt32_t fProcTRand;
	AliUInt32_t fCount;
	int fFD;

	bool fLazy;
	bool fPers;


	bool fQuit;
	bool fQuitted;

	typedef struct Data
	    {
		AliEventID_t fEventID;
		AliHLTSubEventDataDescriptor* fSEDD;
		AliHLTEventTriggerStruct* fETS;
		AliUInt32_t fTime;
	    } Data;

	pthread_mutex_t fEventsMutex;
	vector<AliEventID_t> fEvents;
	vector<AliHLTEventDoneData*> fFinishedEvents;

    };

class InterfaceTestSubscriber: public AliHLTSampleSubscriber
    {
    public:

	InterfaceTestSubscriber( const char* name ):
	    AliHLTSampleSubscriber( name )
		{
		fProcThread = NULL;
		}

	void SetProcThread( TestProcessingThread* thread )
		{
		fProcThread = thread;
		}

	virtual int NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
			      const AliHLTSubEventDataDescriptor& sbevent,
			      const AliHLTEventTriggerStruct& trigger );

	virtual int EventCanceled( AliHLTPublisherInterface& publisher, AliEventID_t eventID );

	virtual int ReleaseEventsRequest( AliHLTPublisherInterface& publisher );

	virtual int SubscriptionCanceled( AliHLTPublisherInterface& publisher );

    protected:

	TestProcessingThread* fProcThread;

    };

class InterfaceTestPublisher: public AliHLTSamplePublisher
    {
    public:

	InterfaceTestPublisher( const char* name, AliUInt32_t slotcount );
	~InterfaceTestPublisher();

	AliUInt32_t GetSlotCnt();
	AliUInt32_t GetFreeSlotCnt();

	void NewEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDataDescriptor*& sedd );
	void FreeEvent( const AliHLTSubEventDataDescriptor* sedd );


    protected:

	virtual void CanceledEvent( AliEventID_t event );
	virtual void AnnouncedEvent( AliEventID_t eventID, 
				     const AliHLTSubEventDataDescriptor& sbevent, 
				     const AliHLTEventTriggerStruct& trigger, AliUInt32_t refCount );

	const AliHLTSubEventDataDescriptor** fSEDDSlots;
	AliUInt32_t fSlotCnt;
	AliUInt32_t fFreeSlots;
	bool fSlotSetLast;

	AliUInt32_t fLastNewSlot;
	AliUInt32_t fLastDoneSlot;

	pthread_mutex_t fSlotMutex;


    private:
    };


AliUInt32_t MakeNumber( AliUInt32_t stat, AliUInt32_t randMod );

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _INTERFACETEST_HPP_
