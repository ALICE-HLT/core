/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "InterfaceTest.hpp"
#include "AliHLTPublisherPipeProxy.hpp"
#include "AliHLTPublisherShmProxy.hpp"
#include "AliHLTSubscriberPipeProxy.hpp"
#include "AliHLTEventDoneData.hpp"
#include "AliHLTLog.hpp"
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ipc.h>
#include <errno.h>
#ifdef LIBC_MEMDEBUG
#include <mcheck.h>
#endif



void TestSubscriptionThread::Run()
    {
    fPub.AcceptSubscriptions();
    }


TestProcessingThread::TestProcessingThread( AliHLTSubscriberInterface& sub,
					    AliHLTPublisherInterface& pub, bool pers, bool lazy, AliUInt32_t procT,
					    AliUInt32_t procTRand, int fd ): fSub( sub ), fPub( pub )
    {
    fProcT = procT;
    fProcTRand = procTRand;
    fQuit = fQuitted = false;
    fFD = fd;
    fCount = 0;
    fPers = pers;
    fLazy = lazy;
    pthread_mutex_init( &fEventsMutex, NULL );
    }

TestProcessingThread::~TestProcessingThread()
    {
    pthread_mutex_destroy( &fEventsMutex );
    }

void TestProcessingThread::NewEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent,
				     const AliHLTEventTriggerStruct& trigger )
    {
    LOG( AliHLTLog::kInformational, "TestProcessingThread::NewEvent", "Received New Event" )
	<< "New event " << AliHLTLog::kDec << eventID << " received." << ENDLOG;
    Data* data = new Data;
    if ( data )
	{
	fCount++;
	data->fEventID = eventID;
	data->fSEDD = (AliHLTSubEventDataDescriptor*)new AliUInt8_t[ sbevent.fHeader.fLength ];
	if ( data->fSEDD )
	    memcpy( data->fSEDD, &sbevent, sbevent.fHeader.fLength );
	data->fETS = (AliHLTEventTriggerStruct*)new AliUInt8_t[ trigger.fHeader.fLength ];
	if ( data->fETS )
	    memcpy( data->fETS, &trigger, trigger.fHeader.fLength );
	data->fTime = MakeNumber( fProcT, fProcTRand );
	pthread_mutex_lock( &fEventsMutex );
	fEvents.insert( fEvents.end(), eventID );
	pthread_mutex_unlock( &fEventsMutex );
	}
    fSem.AddNotificationData( reinterpret_cast<AliUInt64_t>(data) );
    fSem.Signal();
    }

void TestProcessingThread::EventCanceled( AliEventID_t eventID )
    {
    pthread_mutex_lock( &fEventsMutex );
    vector<AliEventID_t>::iterator iter, end;
	    iter = fEvents.begin();
	    end = fEvents.end();
	    while ( iter != end )
		{
		if ( *iter == eventID )
		    {
		    fEvents.erase( iter );
		    break;
		    }
		iter++;
		}
    pthread_mutex_unlock( &fEventsMutex );
    }

void TestProcessingThread::ReleaseEventsRequest()
    {
    LOG( AliHLTLog::kDebug, "TestProcessingThread::ReleaseEventsRequest", "Request received" )
	<< "Subscriber " << fSub.GetName() << " received event release request." << ENDLOG;
    pthread_mutex_lock( &fEventsMutex );
    if ( !fPers )
	{
	LOG( AliHLTLog::kDebug, "TestProcessingThread::ReleaseEventsRequest", "Transient release" )
	    << "Transient subscriber " << fSub.GetName() << " releasing " << fEvents.size() 
	    << " pending events." << ENDLOG;
	vector<AliHLTEventDoneData*> eventDoneData;
	AliHLTEventDoneData* edd;
	while ( fEvents.size()>0 )
	    {
	    edd = new AliHLTEventDoneData;
	    if ( edd )
		{
		AliHLTMakeEventDoneData( edd, *fEvents.begin() );
		eventDoneData.insert( eventDoneData.end(), edd );
		}
	    fEvents.erase( fEvents.begin() );
	    }
	
	fPub.EventDone( fSub, eventDoneData );
	while ( eventDoneData.size()>0 )
	    {
	    delete *eventDoneData.begin();
	    eventDoneData.erase( eventDoneData.begin() );
	    }
	}
    if ( fLazy )
	{
	LOG( AliHLTLog::kDebug, "TestProcessingThread::ReleaseEventsRequest", "Lazy release" )
	    << "Lazy subscriber " << fSub.GetName() << " releasing " <<fFinishedEvents.size() 
	    << " finished events." << ENDLOG;
	fPub.EventDone( fSub, fFinishedEvents );
	while ( fFinishedEvents.size()>0 )
	    {
	    delete *fFinishedEvents.begin();
	    fFinishedEvents.erase( fFinishedEvents.begin() );
	    }
	}
    pthread_mutex_unlock( &fEventsMutex );
    }


void TestProcessingThread::Quit()
    {
    fQuit = true;
    AliUInt32_t count = 0;
    struct timespec ts;
    while ( !fQuitted && count++ < 100 )
	{
	ts.tv_sec = 0;
	ts.tv_nsec = 1000000;
	nanosleep( &ts, NULL );
	}
    }


void TestProcessingThread::Run()
    {
    Data* data;
    int ret;
    bool available;
    vector<AliEventID_t>::iterator iter, end;
    AliHLTEventDoneData* eddP;
    AliHLTEventDoneData edd;
    fSem.Lock();
    while ( !fQuit )
	{
	if ( !fSem.HaveNotificationData() )
	    fSem.Wait();
	while ( fSem.HaveNotificationData() )
	    {
	    fSem.Unlock();
	    data = reinterpret_cast<Data*>( fSem.PopNotificationData() );
	    if ( !data )
		{
		fSem.Lock();
		continue;
		}
	    LOG( AliHLTLog::kInformational, "TestProcessingThread::Run", "Signal" )
		     << "Received signal for event " << data->fEventID 
		     << " sleeping " << data->fTime << " usec" << ENDLOG;
	    if ( data->fTime )
		{
		struct timespec ts, rem;
		rem.tv_sec = data->fTime / 1000000;
		rem.tv_nsec = (data->fTime%1000000)*1000;
		do
		    {
		    ts = rem;
		    ret = nanosleep( &ts, &rem );
		    }
		while ( ret==-1 && errno==EINTR );
		//usleep( data->fTime );
		}
	    available = false;
	    pthread_mutex_lock( &fEventsMutex );
	    iter = fEvents.begin();
	    end = fEvents.end();
	    while ( iter != end )
		{
		if ( *iter == data->fEventID )
		    {
		    available = true;
		    break;
		    }
		iter++;
		}
	    if ( available )
		fEvents.erase( iter );
	    if ( fLazy )
		{
		eddP = new AliHLTEventDoneData;
		if ( eddP )
		    {
		    AliHLTMakeEventDoneData( eddP, data->fEventID );
		    fFinishedEvents.insert( fFinishedEvents.end(), eddP );
		    }
		}
	    pthread_mutex_unlock( &fEventsMutex );
	    if ( fFD!=-1 && available && data->fSEDD )
		write( fFD, data->fSEDD, data->fSEDD->fHeader.fLength );
	    if ( available && !fLazy )
		{
		AliHLTMakeEventDoneData( &edd, data->fEventID );
		fPub.EventDone( fSub, edd );
		}
	    if ( data->fSEDD )
		delete [] (AliUInt8_t*)data->fSEDD;
	    if ( data->fETS )
		delete [] (AliUInt8_t*)data->fETS;
	    delete data;
	    fSem.Lock();
	    }
	}
    fSem.Unlock();
    fQuitted = true;
    }



int InterfaceTestSubscriber::NewEvent( AliHLTPublisherInterface&, AliEventID_t eventID, 
				       const AliHLTSubEventDataDescriptor& sbevent,
				       const AliHLTEventTriggerStruct& trigger )
    {
    LOG( AliHLTLog::kInformational, "InterfaceTestSubscriber", "New event" )
	<< "Subscriber " << GetName() << " received new event notification for event " 
	<< eventID << "." << ENDLOG;
    if ( fProcThread )
	{
	LOG( AliHLTLog::kInformational, "InterfaceTestSubscriber", "New event" )
	    << "Signalling event" << ENDLOG;
	fProcThread->NewEvent( eventID, sbevent, trigger );
	}
    LOG( AliHLTLog::kInformational, "InterfaceTestSubscriber", "New event" )
	<< "New event processing finished" << ENDLOG;
    return 0;
    }

int InterfaceTestSubscriber::EventCanceled( AliHLTPublisherInterface&, AliEventID_t eventID )
    {
    LOG( AliHLTLog::kInformational, "InterfaceTestSubscriber", "Event canceled" )
	<< "Subscriber " << GetName() << " event " << eventID << " canceled" << ENDLOG;
    if ( fProcThread )
	fProcThread->EventCanceled( eventID );
    return 0;
    }

int InterfaceTestSubscriber::SubscriptionCanceled( AliHLTPublisherInterface& publisher )
    {
    LOG( AliHLTLog::kInformational, "InterfaceTestSubscriber", "Subscription canceled" )
	    << "Subscriber " << GetName() << " subscription canceled" << ENDLOG;
    if ( fProcThread )
	fProcThread->Quit();
    return AliHLTSampleSubscriber::SubscriptionCanceled( publisher );
    }

int InterfaceTestSubscriber::ReleaseEventsRequest( AliHLTPublisherInterface& )
    {
    LOG( AliHLTLog::kInformational, "InterfaceTestSubscriber", "Release events request" )
	    << "Subscriber event release requested." << ENDLOG;
    if ( fProcThread )
	fProcThread->ReleaseEventsRequest();
    return 0;
    }


InterfaceTestPublisher::InterfaceTestPublisher( const char* name, AliUInt32_t slotcount ):
    AliHLTSamplePublisher( name )
    {
    fLastNewSlot = fLastDoneSlot = 0;
    fSEDDSlots = new const AliHLTSubEventDataDescriptor*[ slotcount ];
    if ( fSEDDSlots )
	{
	fSlotCnt = slotcount;
	for ( AliUInt32_t i = 0; i<slotcount; i++ )
	    fSEDDSlots[i]=NULL;
	}
    else
	{
	LOG( AliHLTLog::kError, "InterfaceTest constructor", "Slot allocation" )
	    << "InterfaceTest publisher " << GetName() << " out of memory allocating "
	    << slotcount << " event slots." << ENDLOG;
	fSlotCnt = 0;
	}
    fFreeSlots = fSlotCnt;
    pthread_mutex_init( &fSlotMutex, NULL );
    fSlotSetLast = false;
    }

InterfaceTestPublisher::~InterfaceTestPublisher()
    {
    if ( fSEDDSlots )
	delete [] fSEDDSlots;
    pthread_mutex_destroy( &fSlotMutex );
    }

AliUInt32_t InterfaceTestPublisher::GetSlotCnt()
    {
    return fSlotCnt;
    }

AliUInt32_t InterfaceTestPublisher::GetFreeSlotCnt()
    {
    AliUInt32_t n = (AliUInt32_t)-1;
    pthread_mutex_lock( &fSlotMutex );
    n = fFreeSlots;
    pthread_mutex_unlock( &fSlotMutex );
    return n;
    }

void InterfaceTestPublisher::NewEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDataDescriptor*& sedd )
    {
    sedd = (AliHLTSubEventDataDescriptor*)new AliUInt8_t[ sizeof(AliHLTSubEventDataDescriptor)+
							  blockCnt*sizeof(AliHLTSubEventDataBlockDescriptor) ];
    if ( !sedd )
	return;
    sedd->fHeader.fType.fID = ALIL3SUBEVENTDATADESCRIPTOR_TYPE;
    sedd->fHeader.fSubType.fID = 0;
    sedd->fHeader.fLength = sizeof(AliHLTSubEventDataDescriptor)+
			   blockCnt*sizeof(AliHLTSubEventDataBlockDescriptor);
    sedd->fHeader.fVersion = 5;
    sedd->fEventID = eventID;
    sedd->fDataType.fID = UNKNOWN_DATAID;
    sedd->fDataBlockCount = blockCnt;
    AliUInt32_t i;
    for ( i = 0; i < blockCnt; i++ )
	{
	sedd->fDataBlocks[i].fShmID.fShmType = kInvalidShmType;
	sedd->fDataBlocks[i].fShmID.fKey.fID = 0;
	sedd->fDataBlocks[i].fBlockSize = 0;
	sedd->fDataBlocks[i].fBlockOffset = 0;
	sedd->fDataBlocks[i].fProducerNode = 0;
	sedd->fDataBlocks[i].fDataType.fID = UNKNOWN_DATAID;
	sedd->fDataBlocks[i].fDataOrigin.fID = UNKNOWN_DATAORIGIN;
	sedd->fDataBlocks[i].fDataSpecification = ~(AliUInt32_t)0;
	//sedd->fDataBlocks[i].fByteOrder = kAliHLTNativeByteOrder;
	for ( int n = 0; n < kAliHLTSEDBDAttributeCount; n++ )
	    sedd->fDataBlocks[i].fAttributes[n] = kAliHLTUnknownAttribute;
	sedd->fDataBlocks[i].fAttributes[kAliHLTSEDBDByteOrderAttributeIndex] = kAliHLTNativeByteOrder;
	}
    //cout << "Allocated event " << eventID << endl;
    }

void InterfaceTestPublisher::FreeEvent( const AliHLTSubEventDataDescriptor* sedd )
    {
    //if ( sedd )
    //cout << "Deallocated event " << sedd->fEventID << endl;
    //else 
    //cout << "Deallocated event" << endl;
    delete [] (AliUInt8_t*)sedd;
    }


void InterfaceTestPublisher::CanceledEvent( AliEventID_t event )
    {
    //cout << "Canceled Event " << event << endl;
    AliUInt32_t i;
    const AliHLTSubEventDataDescriptor* sedd=NULL;
    pthread_mutex_lock( &fSlotMutex );
    for ( i = 0; i < fSlotCnt; i++ )
	{
	////cout << "Slot " << i << ": " << fSEDDSlots[i] << endl;
	if ( fSEDDSlots[(i+fLastDoneSlot)%fSlotCnt] && fSEDDSlots[(i+fLastDoneSlot)%fSlotCnt]->fEventID==event )
	    {
	    ////cout << "Found event " << i << endl;
	    sedd = fSEDDSlots[(i+fLastDoneSlot)%fSlotCnt];
	    fSEDDSlots[(i+fLastDoneSlot)%fSlotCnt]=NULL;
	    fFreeSlots++;
	    fLastDoneSlot += i;
	    while ( fLastDoneSlot >= fSlotCnt )
		fLastDoneSlot -= fSlotCnt;
	    break;
	    }
	}
    pthread_mutex_unlock( &fSlotMutex );
    if ( sedd )
	{
	//cout << "Calling free event" << endl;
	FreeEvent( sedd );
	//cout << "Called free event" << endl;
	}
    }

void InterfaceTestPublisher::AnnouncedEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, 
					     const AliHLTEventTriggerStruct&, AliUInt32_t )
    {
    //cout << "Announed event " << eventID << endl;
//     if ( refCount==0 )
// 	return;
    bool found=false;
    AliUInt32_t i;
    pthread_mutex_lock( &fSlotMutex );
    for ( i = 0; i < fSlotCnt; i++ )
	{
	if ( !fSEDDSlots[(i+fLastNewSlot)%fSlotCnt] )
	    {
	    fSEDDSlots[(i+fLastNewSlot)%fSlotCnt]=&sbevent;
	    fFreeSlots--;
	    found = true;
	    fLastNewSlot += i;
	    while ( fLastNewSlot >= fSlotCnt )
		fLastNewSlot -= fSlotCnt;
	    break;
	    }
	}
    pthread_mutex_unlock( &fSlotMutex );
    if ( !found )
	{
	LOG( AliHLTLog::kWarning, "InterfaceTestPublisher::AnnouncedEvent", "Event overflow" )
	    << "No space found in internal buffers to store event " << AliHLTLog::kDec
	    << eventID << "." << ENDLOG;
	}
    }


AliUInt32_t MakeNumber( AliUInt32_t stat, AliUInt32_t randMod )
    {
    AliUInt32_t base;
    base = stat;
    double rnd;
    do
	{
	rnd = random();
	rnd /= RAND_MAX;
	rnd *= 2*randMod+0.5;
	if ( rnd > randMod )
	    rnd -= 2*randMod;
	rnd += base;
	} 
    while ( rnd < 0 );
    base = (AliUInt32_t)rnd;
    return base;
    }


int main( int argc, char** argv )
    {
#ifdef LIBC_MEMDEBUG
    mtrace();
#endif
    struct timeval start1, start2, end1, end2, end3;
    AliUInt32_t waitCount=0;
    bool lazy = false;
    bool server=false, client=false;
    bool noStdOut = false;
    bool noWrite = false;
    MLUCString clientName;
    AliUInt32_t evtCount = (AliUInt32_t)-1;
    AliUInt32_t evtSize=2, evtSizeRand=0;
    AliUInt32_t slots = (AliUInt32_t)-1;
    // Following times in us.
    AliUInt32_t interval=0, intervalRand=0;
    // Following times in secs
    AliUInt32_t runTime=0;
    bool verbose=false, quiet=false;
    int argNdx=0;
    char *cp, *cperr;
    char* errorDescr = NULL;
    bool transient = false;
    bool usePipeProxy = false;
    bool useShmProxy = false;
    AliUInt32_t shmSize = 512*1024;
    key_t pub2SubKey=4742, sub2PubKey=4743;
    bool bufferedLogging = false;

    while ( argNdx+1 < argc )
	{
	argNdx++;
	cp = argv[argNdx];
	while ( *cp )
	    {
	    *cp = tolower( *cp );
	    cp++;
	    }
	cp = argv[argNdx];
	if ( !strcmp( cp, "-server" ) )
	    {
	    server = true;
	    continue;
	    }
	if ( !strcmp( cp, "-transient" ) )
	    {
	    transient = true;
	    continue;
	    }
	if ( !strcmp( cp, "-lazy" ) )
	    {
	    lazy = true;
	    continue;
	    }
	if ( !strcmp( cp, "-nostdout" ) )
	    {
	    noStdOut = true;
	    continue;
	    }
	if ( !strcmp( cp, "-nowrite" ) )
	    {
	    noWrite = true;
	    continue;
	    }
	if ( !strcmp( cp, "-client" ) )
	    {
	    client = true;
	    if ( argNdx+1 >= argc )
		{
		errorDescr = "Missing '-count' argument";
		break;
		}
	    clientName = argv[ ++argNdx ];
	    continue;
	    }
	if ( !strcmp( cp, "-count" ) )
	    {
	    if ( argNdx+1 >= argc )
		{
		errorDescr = "Missing '-count' argument";
		break;
		}
	    evtCount = strtoul( argv[++argNdx], &cperr, 0 );
	    if ( *cperr )
		{
		errorDescr = "Non numeric '-count' argument";
		break;
		}
	    continue;
	    }
	if ( !strcmp( cp, "-slots" ) )
	    {
	    if ( argNdx+1 >= argc )
		{
		errorDescr = "Missing '-slots' argument";
		break;
		}
	    slots = strtoul( argv[++argNdx], &cperr, 0 );
	    if ( *cperr )
		{
		errorDescr = "Non numeric '-slots' argument";
		break;
		}
	    continue;
	    }
	if ( !strcmp( cp, "-blockcount" ) )
	    {
	    if ( argNdx+1 >= argc )
		{
		errorDescr = "Missing '-blockcount' argument";
		break;
		}
	    evtSize = strtoul( argv[++argNdx], &cperr, 0 );
	    if ( *cperr )
		{
		errorDescr = "Non numeric '-blockcount' argument";
		break;
		}
	    continue;
	    }
	if ( !strcmp( cp, "-blockcountrand" ) )
	    {
	    if ( argNdx+1 >= argc )
		{
		errorDescr = "Missing '-blockcountrand' argument";
		break;
		}
	    evtSizeRand = strtoul( argv[++argNdx], &cperr, 0 );
	    if ( *cperr )
		{
		errorDescr = "Non numeric '-blockcountrand' argument";
		break;
		}
	    continue;
	    }
	if ( !strcmp( cp, "-interval" ) )
	    {
	    if ( argNdx+1 >= argc )
		{
		errorDescr = "Missing '-interval' argument";
		break;
		}
	    interval = strtoul( argv[++argNdx], &cperr, 0 );
	    if ( *cperr )
		{
		errorDescr = "Non numeric '-interval' argument";
		break;
		}
	    continue;
	    }
	if ( !strcmp( cp, "-intervalrand" ) )
	    {
	    if ( argNdx+1 >= argc )
		{
		errorDescr = "Missing '-intervalrand' argument";
		break;
		}
	    intervalRand = strtoul( argv[++argNdx], &cperr, 0 );
	    if ( *cperr )
		{
		errorDescr = "Non numeric '-intervalrand' argument";
		break;
		}
	    continue;
	    }
	if ( !strcmp( cp, "-time" ) )
	    {
	    if ( argNdx+1 >= argc )
		{
		errorDescr = "Missing '-time' argument";
		break;
		}
	    runTime = strtoul( argv[++argNdx], &cperr, 0 );
	    if ( *cperr )
		{
		errorDescr = "Non numeric '-time' argument";
		break;
		}
	    continue;
	    }
	if ( !strcmp( cp, "-v" ) )
	    {
	    verbose = true;
	    continue;
	    }
	if ( !strcmp( cp, "-q" ) )
	    {
	    quiet = true;
	    continue;
	    }
	if ( !strcmp( cp, "-pipeproxy" ) )
	    {
	    usePipeProxy = true;
	    continue;
	    }
	if ( !strcmp( cp, "-shmproxy" ) )
	    {
	    useShmProxy = true;
	    continue;
	    }
	if ( !strcmp( cp, "-bufferedlogging" ) )
	    {
	    bufferedLogging = true;
	    continue;
	    }
	errorDescr = "Unknown argument";
	}
    if ( !errorDescr && ( server && ((evtCount==(AliUInt32_t)-1 && runTime==0)
		    || (evtCount!=(AliUInt32_t)-1 && runTime!=0) || (slots==(AliUInt32_t)-1) ) ) )
	errorDescr = "Must specify '-slots slotcount' and either '-count eventcount' or '-time seconds_runtime' argument";
    if ( !errorDescr && ( (server && client) || (!server && !client) ) )
	errorDescr = "Must specify either '-server' or '-client'";
    if ( !errorDescr && client && (usePipeProxy==useShmProxy) )
	errorDescr = "Must use one of '-pipeproxy' or '-shmproxy'";
    if ( errorDescr )
	{
	cerr << errorDescr << endl;
	cerr << "Usage: " << argv[0] << " [-client clientID (-transient) (-lazy) (-pipeproxy) (-shmproxy)| -server -slots slotcount [-count eventcount|-time seconds_runtime] " 
	     << "(-blockcount eventblocks) (-blockcountrand eventblocks_random_modifier_limit) ] (-interval time_interval_in_usec) "
	     << "(-intervalrand time_interval_random_modifier_limit) (-nostdout) (-nowrite) (-bufferedlogging)" << endl << endl;
	return -1;
	}
    gLogLevel = AliHLTLog::kAll & ~(AliHLTLog::kInformational|AliHLTLog::kDebug);
    if ( quiet )
	gLogLevel = AliHLTLog::kAll & ~(AliHLTLog::kInformational|AliHLTLog::kDebug|AliHLTLog::kWarning);
    if ( verbose )
	gLogLevel = AliHLTLog::kAll;

    TestFilteredLogServer sOut;
    //AliHLTStdoutLogServer sOut;
    //gLogLevel = AliHLTLog::kAll;
    AliHLTBufferingLogServer bufferLog( 5000, 2000 );
    if ( !noStdOut )
	{
	if ( !bufferedLogging )
	    gLog.AddServer( sOut );
	else
	    bufferLog.AddServer( sOut );
	}

    MLUCString name;
    name = "InterfaceTest";
    if ( server )
	name += "-server";
    else
	name += "-client";
    if ( client )
	{
	name += "-";
	name += clientName;
	}

    gLog.SetID( name.c_str() );

    name += ".log";
    ofstream of;
    of.open( name.c_str() );
    AliHLTStreamLogServer streamOut( of );
    if ( !bufferedLogging )
	gLog.AddServer( streamOut );
    else
	bufferLog.AddServer( streamOut );

    if ( bufferedLogging )
	gLog.AddServer( bufferLog );

    if ( bufferedLogging )
	bufferLog.StartThread();

    LOG( AliHLTLog::kInformational, "InterfaceTest", "Start" ) << "Test Start" << AliHLTLog::kDec << ENDLOG;

    int fh;
    name = "InterfaceTest";
    if ( server )
	name += "-server";
    else
	name += "-client";
    if ( client )
	{
	name += "-";
	name += clientName;
	}
    name += ".data";
    fh = open( (const char*)name.c_str(), O_WRONLY|O_CREAT|O_TRUNC, 0644 );
    if ( fh<=0 )
	{
	LOG( AliHLTLog::kError, "InterfaceTest", "Data file open" )
	    << "Error opening data file " << name.c_str() << "." << ENDLOG;
	}
    AliUInt32_t count=0;
    long long t1, t2, t3, t4;
    if ( server )
	{
	// -------------------------------------------------------------------------
	// Server Code
	// -------------------------------------------------------------------------

	vector<AliEventID_t> events;
	AliHLTEventTriggerStruct ets;
	ets.fHeader.fLength = sizeof( AliHLTEventTriggerStruct );
	ets.fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
	ets.fHeader.fSubType.fID = 0;
	ets.fHeader.fVersion = 1;
	ets.fDataWordCount = 0;
	int tmp;

	InterfaceTestPublisher publisher( "TestInterfacePublisher", slots );
	publisher.SetComTimeout( 1000000 );
	publisher.SetSubscriptionLoop( &PublisherPipeSubscriptionInputLoop );

	TestSubscriptionThread pubThread( publisher );
	if ( pubThread.Start() == AliHLTThread::kFailed )
	    {
	    LOG( AliHLTLog::kError, "InterfaceTest", "Subscription thread" ) 
		<< "Error starting subscription thread." << ENDLOG;
	    return -1;
	    }
	LOG( AliHLTLog::kInformational, "InterfaceTest", "Waiting" ) 
	    << "Waiting for enter before starting publishing" << ENDLOG;
	scanf( "%d", &tmp );
	LOG( AliHLTLog::kInformational, "InterfaceTest", "Publishing" ) 
	    << "Now starting publishing" << ENDLOG;
	gettimeofday( &start2, NULL );

	bool stop = false;
	AliUInt32_t st;
	AliUInt32_t blkCnt;
	AliUInt32_t startTime, diffTime;
	AliHLTSubEventDataDescriptor* sedd;
	struct timespec ts;
	startTime = time( NULL );
	bool low = false;
	while ( !stop )
	    {
	    blkCnt = MakeNumber( evtSize, evtSizeRand );
	    publisher.NewEvent( count, blkCnt, sedd );
	    publisher.AnnounceEvent( count, *sedd, ets );
	    if ( !noWrite )
		write( fh, sedd, sedd->fHeader.fLength );
	    count++;
	    if ( evtCount!=(AliUInt32_t)-1 && count>=evtCount )
		stop = true;
	    if ( runTime>0 && (count%16==0) )
		{
		diffTime = time( NULL )-startTime;
		if ( diffTime>=runTime )
		    stop = true;
		}
	    if ( publisher.GetFreeSlotCnt() <= (publisher.GetSlotCnt()/10) )
		{
		publisher.RequestEventRelease();
		}
	    if ( publisher.GetFreeSlotCnt() <= 0 )
		{
		//struct timespec ts = { 0, 5000 };
		if ( !low )
		    {
		    LOG( AliHLTLog::kWarning, "InterfaceTest Publisher", "Server loop" )
			<< "LOW WATER MARK" << ENDLOG;
		    low = true;
		    }
		while ( publisher.GetFreeSlotCnt() <= 0 )
		    {
		    waitCount++;
		    //nanosleep( &ts, NULL );
		    }
		LOG( AliHLTLog::kInformational, "InterfaceTest Publisher", "Server loop" )
		    << "Low water mark end" << ENDLOG;
		}
	    else
		low = false;
	    if ( !stop )
		st = MakeNumber( interval, intervalRand );
	    else
		st = 0;
	    LOG( AliHLTLog::kInformational, "InterfaceTest", "Waiting" ) 
		<< "Waiting " << st << " usec." << ENDLOG;
	    if ( st > 0 )
		{
		ts.tv_sec = st/1000000000ULL;
		ts.tv_nsec = st%1000000000ULL;
		nanosleep( &ts, NULL );
		}
	    }
	gettimeofday( &end2, NULL );
	if ( evtCount==(AliUInt32_t)-1 )
	    evtCount = count;

	gettimeofday( &end3, NULL );
	t3 = (end3.tv_sec-end2.tv_sec);
	while ( publisher.GetPendingEventCount()>0 && t3 < 120 ) // Wait 2 minutes
	    {
	    LOG( AliHLTLog::kInformational, "InterfaceTest Publisher", "Pending events" )
		<< AliHLTLog::kDec << publisher.GetPendingEventCount() << " pending events..." << ENDLOG;
	    //usleep( 0 );
	    gettimeofday( &end3, NULL );
	    t3 = (end3.tv_sec-end2.tv_sec);
	    }


	gettimeofday( &end3, NULL );

	struct timeval start, now;
	unsigned long long deltaT = 0;
	const unsigned long long timeLimit = 10000000;
	gettimeofday( &start, NULL );
	while ( publisher.GetPendingEventCount()>0 && deltaT<timeLimit )
	    {
	    publisher.GetPendingEvents( events );
	    LOG( AliHLTLog::kWarning, "InterfaceTest Publisher", "Pending events" )
		<< "Pending events: ";
	    while ( events.size()>0 )
		{
		gLog << AliHLTLog::kDec << events[0];
		events.erase( events.begin() );
		if ( events.size()>0 )
		    gLog << ", ";
		}
	    gLog << ENDLOG;
	    publisher.RequestEventRelease();
	    usleep( 1000 );
	    gettimeofday( &now, NULL );
	    deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	    }

	//     if ( end3.tv_usec < start2.tv_usec )
	// 	{
	// 	end3.tv_usec += 1000000;
	// 	end3.tv_sec -= 1;
	// 	}
	
	publisher.CancelAllSubscriptions( true );
	LOG( AliHLTLog::kDebug, "InterfaceTest", "Subscriptions canceled" ) 
	    << "All subscription canceled..." << ENDLOG;

	QuitPipeSubscriptionLoop( publisher );
	LOG( AliHLTLog::kDebug, "InterfaceTest", "Subscription loop quitted" ) 
	    << "Subscription loop quitted..." << ENDLOG;
	}
    else
	{
	// -------------------------------------------------------------------------
	// Client Code
	// -------------------------------------------------------------------------
	name = "InterfaceTestClient-";
	name += clientName;

	InterfaceTestSubscriber subscriber( name.c_str() );
	//AliHLTPublisherPipeProxy publisher( "TestInterfacePublisher" );

	AliHLTPublisherProxyInterface* publisher = NULL;
	AliUInt32_t publisherTimeout = /*1*/000000;
	if ( usePipeProxy )
	    {
	    AliHLTPublisherPipeProxy* publisherT = new AliHLTPublisherPipeProxy( "TestInterfacePublisher" );
	    //publisherT->SetTimeout( publisherTimeout );
	    publisher = publisherT;
	    }
	if ( useShmProxy )
	    {
	    AliHLTPublisherShmProxy* publisherT = new AliHLTPublisherShmProxy( "TestInterfacePublisher", shmSize, pub2SubKey, sub2PubKey );
	    //publisherT->SetTimeout( publisherTimeout );
	    publisher = publisherT;
	    }

	if ( !publisher )
	    {
	    LOG( AliHLTLog::kError, "InterfaceTest", "Publisher proxy" )
		<< "Error on creation of publisher proxy object for publisher."
		<< ENDLOG;
	    return -1;
	    }

	TestProcessingThread procThread( subscriber, *publisher, !transient, lazy,
					 interval, intervalRand, noWrite ? -1 : fh );
	subscriber.SetProcThread( &procThread );
	publisher->SetTimeout( publisherTimeout );
	procThread.Start();

	if ( ! publisher->operator bool() )
	    {
	    LOG( AliHLTLog::kError, "InterfaceTest", "Publisher proxy" )
		<< "Error creating publisher proxy object for publisher "
		<< publisher->GetName() << "." << ENDLOG;
	    return -1;
	    }

	LOG( AliHLTLog::kInformational, "InterfaceTest", "Subscribing" ) 
	    << "Now subscribing" << ENDLOG;

	publisher->Subscribe( subscriber );
	if ( transient )
	    {
	    publisher->SetPersistent( subscriber, true );
	    publisher->SetTransientTimeout( subscriber, 200 );
	    }
	gettimeofday( &start2, NULL );
	publisher->StartPublishing( subscriber );
	printf( "%s:%d\n", __FILE__, __LINE__ );
	gLogLevel = AliHLTLog::kAll;
	gettimeofday( &end2, NULL );
	gettimeofday( &end3, NULL );
	count = evtCount = procThread.GetCount();
	printf( "%s:%d\n", __FILE__, __LINE__ );
	procThread.Quit();
	printf( "%s:%d\n", __FILE__, __LINE__ );
	delete publisher;
	printf( "%s:%d\n", __FILE__, __LINE__ );
	}
    printf( "%s:%d\n", __FILE__, __LINE__ );
    close( fh );
    gettimeofday( &end1, NULL );

    gLogLevel = AliHLTLog::kAll;

//     if ( end1.tv_usec < start1.tv_usec )
// 	{
// 	end1.tv_usec += 1000000;
// 	end1.tv_sec -= 1;
// 	}
    t1 = (end1.tv_sec-start1.tv_sec)*1000000+(end1.tv_usec-start1.tv_usec);
    
//     if ( end2.tv_usec < start2.tv_usec )
// 	{
// 	end2.tv_usec += 1000000;
// 	end2.tv_sec -= 1;
// 	}
    t2 = (end2.tv_sec-start2.tv_sec)*1000000+(end2.tv_usec-start2.tv_usec);

//     if ( end3.tv_usec < start2.tv_usec )
// 	{
// 	end3.tv_usec += 1000000;
// 	end3.tv_sec -= 1;
// 	}
    t3 = (end3.tv_sec-start2.tv_sec)*1000000+(end3.tv_usec-start2.tv_usec);

    t4 = (end3.tv_sec-end2.tv_sec)*1000000+(end3.tv_usec-end2.tv_usec);


    LOG( AliHLTLog::kInformational, "InterfaceTest", "Results" )
	<< AliHLTLog::kDec << "Time needed for " << count << " messages: " << t2 
	<< " usec. " << ((double)t2)/((double)count) << " usec/message." << ENDLOG;
    if ( server && waitCount>0 )
	{
	LOG( AliHLTLog::kInformational, "InterfaceTest", "Results" )
	    << AliHLTLog::kDec << "Time needed without throtteling " << waitCount << " times for " << count << " messages: " << t2-waitCount*50 
	    << " usec. " << ((double)t2-waitCount*50)/((double)count) << " usec/message." << ENDLOG;
	}
    
    LOG( AliHLTLog::kInformational, "InterfaceTest", "Results" ) 
	<< "Total time needed for " << count << " messages: " << t1 
	<< " usec. " << ((double)t1)/((double)count) << " usec/message." << ENDLOG;
    LOG( AliHLTLog::kInformational, "InterfaceTest", "Results" ) 
	<< "Overhead: " << t1-t2 << " usec." << ENDLOG;


    LOG( AliHLTLog::kInformational, "InterfaceTest", "Results" ) 
	<< "Total time needed for round-trip of " << count << " messages: " << t3 
	<< " usec. " << ((double)t3)/((double)count) << " usec/message." << ENDLOG;


    LOG( AliHLTLog::kInformational, "InterfaceTest", "Results" ) 
	<< "Wait time for events: " << t4 
	<< " usec == " << ((double)t4)/1000000.0 <<  " sec." << ENDLOG;


    LOG( AliHLTLog::kInformational, "InterfaceTest", "End" ) << "Test End" << ENDLOG;

    if ( bufferedLogging )
	{
	bufferLog.StopThread();
	bufferLog.FlushMessages();
	}
    }






/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

