/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSampleSubscriber.hpp"
#include "AliHLTPublisherPipeProxy.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTEventDoneData.h"
#include "AliHLTEventDoneData.hpp"
#include "AliHLTThread.hpp"
#include "MLUCHistogram.hpp"
#include <errno.h>


class EventDoneDataConsumer: public AliHLTSampleSubscriber
    {
    public:

	EventDoneDataConsumer( const char* name );

	virtual ~EventDoneDataConsumer();

	int NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
		  const AliHLTSubEventDataDescriptor& sbevent,
		  const AliHLTEventTriggerStruct& ets);

	virtual int EventDoneData( AliHLTPublisherInterface& publisher, 
				   const AliHLTEventDoneData& 
				   eventDoneData );
	
    protected:

	AliHLTEventDoneData fEDD;

    private:
    };


EventDoneDataConsumer::EventDoneDataConsumer( const char* name ):
    AliHLTSampleSubscriber( name )
    {
    AliHLTMakeEventDoneData( &fEDD, AliEventID_t() );
    }

EventDoneDataConsumer::~EventDoneDataConsumer()
    {
    }

int EventDoneDataConsumer::EventDoneData( AliHLTPublisherInterface& publisher, 
					  const AliHLTEventDoneData& eventDoneData )
    {
    LOG( AliHLTLog::kInformational, "EventDoneDataConsumer::EventDoneData", "Event Done Data Received" )
	<< "Event Done Data with " << AliHLTLog::kDec << eventDoneData.fDataWordCount << " (0x" << AliHLTLog::kHex
	<< eventDoneData.fDataWordCount << ") data words and " << AliHLTLog::kDec << eventDoneData.fHeader.fLength
	<< " (0x" << AliHLTLog::kHex << eventDoneData.fHeader.fLength << ") bytes received." << ENDLOG;
    if ( eventDoneData.fDataWordCount==2 )
	{
	if ( *(AliEventID_t*)(eventDoneData.fDataWords) != eventDoneData.fEventID )
	    {
	    LOG( AliHLTLog::kWarning, "EventDoneDataConsumer::EventDoneData", "Unexpected Event Done Data" )
		<< "Event Done Data w. 2 data words unequal event ID. Event ID: 0x" << AliHLTLog::kHex
		<< eventDoneData.fEventID << " (" << AliHLTLog::kDec << eventDoneData.fEventID << ") - (Event ID)(Data Words): 0x"
		<< AliHLTLog::kHex << *(AliEventID_t*)(eventDoneData.fDataWords) << " (" << AliHLTLog::kDec
		<< *(AliEventID_t*)(eventDoneData.fDataWords) << ")." << ENDLOG;
	    }
	}
    fEDD.fEventID = eventDoneData.fEventID;
    publisher.EventDone( *this, fEDD );
    return 0;
    }


int EventDoneDataConsumer::NewEvent( AliHLTPublisherInterface&, AliEventID_t, 
				   const AliHLTSubEventDataDescriptor&,
				   const AliHLTEventTriggerStruct&)
    {
    return 0;
    }
	

int main( int argc, char** argv )
    {
    AliHLTFilteredStdoutLogServer sOut;
    gLogLevel = AliHLTLog::kAll;
    gLog.AddServer( sOut );
    int i;
    char* cpErr = NULL;
    char* errorStr = NULL;

    char* publisherName = NULL;
    char* subscriberID = NULL;

    const char* usage1 = "Usage: ";
    const char* usage2 = " (-V <verbosity>) -subscribe <publishername> <subscriberID>";

    i = 1;
    while ( (i < argc) && !errorStr )
	{
	if ( !strcmp( argv[i], "-subscribe" ) )
	    {
	    if ( argc <= i+2 )
		{
		errorStr = "Missing publishername or subscriberid parameter";
		break;
		}
	    publisherName = argv[i+1];
	    subscriberID = argv[i+2];
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc <= i +1 )
		{
		errorStr = "Missing verbosity level specifier.";
		break;
		}
	    AliUInt32_t tmpll = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting verbosity level specifier..";
		break;
		}
	    gLogLevel = tmpll;
	    i += 2;
	    continue;
	    }
	errorStr = "Unknown parameter";
	}

    if ( !errorStr )
	{
	if ( !publisherName || !subscriberID )
	    errorStr = "Must specify -subscribe <publishername> <subscriberID> argument";
	}
    
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "EventDoneDataConsumer", "Command line arguments" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	LOG( AliHLTLog::kError, "EventDoneDataConsumer", "Command line arguments" )
	    << errorStr << ENDLOG;
	return -1;
	}

    MLUCString name;
    name = publisherName;
    name += "-";
    name += subscriberID;

    AliHLTFileLogServer fileOut( name.c_str(), ".log", 3000000 );
    gLog.AddServer( fileOut );
    
    AliHLTPublisherPipeProxy publisher( publisherName );
    EventDoneDataConsumer subscriber( subscriberID );

    publisher.Subscribe( subscriber );
    publisher.SetEventDoneDataSend( subscriber, true );

    publisher.StartPublishing( subscriber );

    return 0;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
