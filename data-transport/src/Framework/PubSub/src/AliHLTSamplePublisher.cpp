/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSamplePublisher.hpp"
#include "AliHLTSubscriberInterface.hpp"
#include "AliHLTEventTriggerStruct.hpp"
#include "AliHLTPubSubPipeCom.hpp"
#include "AliHLTSubscriberProxyInterface.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTEventAccounting.hpp"
#include <pthread.h>
#include <errno.h>
#include "AliHLTStackTrace.hpp"
#include <stdlib.h>
// #include <iostream.h>
// #include <stdio.h>




// XXX
#define USE_STL_ITERATOR_SEARCH
// XXX

#define EVENT_CHECK_PARANOIA_LIMIT 8192


#define MSGSLOTRESERVEMAXCOUNT 10
#define MSGSLOTRESERVEWAITCOUNT 500


#ifdef PROFILING
#define DECL_PROF(name) struct timeval name##S, name##E; double name##DiffT = 0;
#define DECL_SIMPLE_PROF(name) double name##DiffT = 0;
#define START_PROF(name) gettimeofday( &name##S, NULL );
#define END_PROF(name) { gettimeofday( &name##E, NULL ); name##DiffT = CalcTimeDiff_us( &name##S, &name##E ); }
#define GET_PROF(name) (name##DiffT)
#define SET_PROF(name, value) name##DiffT = (value)
#define HISTO_PROF( histo, name ) { vector<MLUCHistogram*>::iterator histIter, histEnd; \
                                       histIter = histo.begin(); \
		                       histEnd = histo.end(); \
				       while ( histIter != histEnd ) \
					   { \
					   (*histIter)->Add( name##DiffT ); \
					   histIter++; \
					   } \
                                     }
#define HISTO_MUTEXLOCK_PROF( name ) HISTO_PROF( fMutexLockTimeHistos, name )
#define HISTO_MUTEXLOCK_SUB_PROF( name ) HISTO_PROF( fPub.fMutexLockTimeHistos, name )

#define HISTO_MUTEXUNLOCK_PROF( name ) HISTO_PROF( fMutexUnlockTimeHistos, name )
#define HISTO_MUTEXUNLOCK_SUB_PROF( name ) HISTO_PROF( fPub.fMutexUnlockTimeHistos, name )

#else
#define DECL_PROF(name)
#define DECL_SIMPLE_PROF(name)
#define START_PROF(name)
#define END_PROF(name)
#define GET_PROF(name) (0)
#define SET_PROF(name, value) ;
#define HISTO_PROF( histo, name )

#define HISTO_MUTEXLOCK_PROF( name )
#define HISTO_MUTEXLOCK_SUB_PROF( name )

#define HISTO_MUTEXUNLOCK_PROF( name )
#define HISTO_MUTEXUNLOCK_SUB_PROF( name )
#endif



AliHLTSamplePublisher::AliHLTSamplePublisher( const char* name, int eventSlotsPowerOfTwo ):
    AliHLTPublisherInterface( name ), fEventTimeoutDataCache( sizeof(EventTimeoutData), (eventSlotsPowerOfTwo<0) ? 4 : eventSlotsPowerOfTwo ), 
    fTimer( (eventSlotsPowerOfTwo<0) ? eventSlotsPowerOfTwo : (eventSlotsPowerOfTwo + 1) ),
    fEventThread( *this, (eventSlotsPowerOfTwo<0) ? eventSlotsPowerOfTwo : (eventSlotsPowerOfTwo + 2) ), 
    fPingThread( *this ), fSubscriberCleanup( *this ),
    fEDDCache( (eventSlotsPowerOfTwo<3) ? 3 : eventSlotsPowerOfTwo-3, true, false ),
    fEvents( 8, eventSlotsPowerOfTwo, true ),
    fEventAccounting(NULL)
    {
    fSubscriptionInputLoop = NULL;
    fMaxTimeout = ~(AliUInt32_t)0;
    fPingTimeout = 1000;
    fMaxPingCount = 20;
#ifdef USE_PIPE_TIMEOUT
    fComTimeout = 500000;
#else
    fComTimeout = 0;
#endif
    pthread_mutex_init( &fSubscriberMutex, NULL );
    pthread_mutex_init( &fEventMutex, NULL );
    LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher", "Create Publisher" )
	<< "Publisher " << GetName() << " created" << ENDLOG;
    fTimerCurrentCancelEvent = AliEventID_t();
    fETTCompFunc = CompareEventTriggerTypes;
    fETTCompFuncParam = NULL;
    fCachedEDDAllocs = false;
    fEventThread.Start();
    fPingThread.Start();
    fSubscriberCleanup.Start();
    fTimer.Start();
    fOldestEventBirth_s = 0;
    fPinEvents = false;
    pthread_mutex_init( &fSubscriberEventCallbackMutex, NULL );
#ifdef EVENT_CHECK_PARANOIA
    fLastEventDoneID = ~(AliEventID_t)0;
    fLastEventAnnouncedID = ~(AliEventID_t)0;
    fLastEventSent1ID = ~(AliEventID_t)0;
    fLastEventSent2ID = ~(AliEventID_t)0;
    fDoParanoiaChecks = true;
#endif
    fEventModuloEventIDBased = true;
    //fEventModuloPreDivisor = 1;
    fSendThreadMsgBufferSizeExp2 = 16;
    fEventsAborted = false;
    }

AliHLTSamplePublisher::~AliHLTSamplePublisher()
    {
    fTimer.Stop();
    CancelAllSubscriptions( false, true );
    int ret;
    fEventThread.Quit();
    fPingThread.Quit();
    fSubscriberCleanup.Quit();
    ret = pthread_mutex_destroy( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Destroy Publisher" )
	    << GetName() << " publisher error destroying subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}
    ret = pthread_mutex_destroy( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Destroy Publisher" )
	    << GetName() << " publisher error destroying subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}
    pthread_mutex_destroy( &fSubscriberEventCallbackMutex );
    LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher", "Destroy Publisher" )
	<< "Publisher " << GetName() << " destroyed" << ENDLOG;
    }

int AliHLTSamplePublisher::Subscribe( AliHLTSubscriberInterface& interface )
    {
    if ( fEventAccounting )
	interface.SetEventAccounting( fEventAccounting );
    SubscriberData *data = new SubscriberData;
    if ( !data )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Add subscriber" )
	    << "Unable to allocate memory for subscriber data for subscriber " 
	    << interface.GetName() << ENDLOG;
	return ENOMEM;
	}
    ((AliHLTSubscriberProxyInterface&)interface).SetTimeout( fComTimeout );
    if ( !data )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Add subscriber" )
	    << "Unable to create new subscriber data for subscriber "
	    << interface.GetName() << ENDLOG;
	return ENOMEM;
	}
    data->fInterface = &interface;
    data->fThread = new AliHLTSubscriberThread( *this, data );
    if ( !data->fThread )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Add subscriber" )
	    << "Unable to create new subscriber thread for subscriber "
	    << interface.GetName() << ENDLOG;
	delete data;
	return ENOMEM;
	}
    data->fSendThread = new AliHLTSubscriberSendThread( *this, data, fSendThreadMsgBufferSizeExp2 );
    if ( !data->fSendThread )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Add subscriber" )
	    << "Unable to create new subscriber announce thread for subscriber "
	    << interface.GetName() << ENDLOG;
	delete data->fThread;
	delete data;
	return ENOMEM;
	}
    pthread_mutex_init( &(data->fUsedMutex), NULL );
    data->fPersistent = true;
    data->fSendEventDoneData = false;
    data->fModulo = 1;
    data->fCount = 0;
    data->fTimeout = fMaxTimeout;
    data->fPingCount = 0;
    data->fActive = false;
    data->fThreadActive = false;
    data->fSendThreadActive = false;
    data->fTriggerCnt = 0;
    data->fTriggers = NULL;
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Add subscriber" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	pthread_mutex_destroy( &(data->fUsedMutex) );
	delete data->fSendThread;
	delete data->fThread;
	delete data;
	return ENOLCK;
	}
    vector<SubscriberData*>::iterator iter, end;
    bool found = false;
    iter = fSubscribers.begin();
    end = fSubscribers.end();
    while ( iter != end )
	{
	if ( (*iter)->fInterface < data->fInterface )
	    {
	    found = true;
	    fSubscribers.insert( iter, data );
	    break;
	    }
	iter++;
	}
    if ( !found )
	fSubscribers.insert( fSubscribers.end(), data );
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Add subscriber" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher", "Add subscriber" )
	<< "Subscriber " << interface.GetName() << " added to subscription list" << ENDLOG;
    data->fThread->Start();
    data->fSendThread->Start();
    while ( !data->fThread->IsRunning() || !data->fSendThread->IsRunning() )
	usleep( 0 );
    SubscribedCallbacks( interface.GetName(), &interface );
    return 0;
    }

int AliHLTSamplePublisher::Unsubscribe( AliHLTSubscriberInterface& interface )
    {
    // XXX
    // Warning: The deletion code most likely will have to be changed...
    vector<SubscriberData*>::iterator res; // ??=NULL;
    SubscriberData* data=NULL;
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Remove subscriber" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    bool found=false;
    res = FindSubscriber( fSubscribers, &interface );
#if __GNUC__>=3
    if ( res.base() )
#else
    if ( res )
#endif
	{
	found = true;
	data = *res;
	pthread_mutex_lock( &(data->fUsedMutex) );
	}
    if ( found )
	{
	RemoveSubscriber( data, false, true, false );
	LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher", "Remove subscriber" )
	    << "Subscriber " << interface.GetName() << " unsubscribed" << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher", "Remove subscriber" )
	    << "Unsubscribed subscriber " << interface.GetName() << " attempted to unsubscribe" 
	    << ENDLOG;
	return ENOENT;
	}
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Remove subscriber" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    return 0;
    }

 

int AliHLTSamplePublisher::SetPersistent( AliHLTSubscriberInterface& interface, bool set )
    {
    vector<SubscriberData*>::iterator res;
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Persistenize subscriber" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    bool found = false;
    res = FindSubscriber( fSubscribers, &interface );
#if __GNUC__>=3
    if ( res.base() )
#else
    if ( res )
#endif
	{
	found = true;
	(*res)->fPersistent = set;
	}
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Persistenize subscriber" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    if ( found )
	{
	LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher", "Persistenize subscriber" )
	    << "Subscriber " << interface.GetName() << " set to " 
	    << (set ? "persistent" : "transient") << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher", "Persistenize subscriber" )
	    << "Unsubscribed subscriber " << interface.GetName() << " attempted to be " 
	    << (set ? "persistent" : "transient") << ENDLOG;
	return ENOENT;
	}
    return 0;
    }

int AliHLTSamplePublisher::SetEventType( AliHLTSubscriberInterface& interface,  
					AliUInt32_t modulo )
    {
    vector<SubscriberData*>::iterator res;
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event modulo number setting" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    bool found = false;
    res = FindSubscriber( fSubscribers, &interface );
#if __GNUC__>=3
    if ( res.base() )
#else
    if ( res )
#endif
	{
	found = true;
	if ( modulo )
	    (*res)->fModulo = modulo;
	}
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event modulo number setting" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    if ( found )
	{
	LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher", "Event modulo number setting" )
	    << "Subscriber " << interface.GetName() << " set event modulo to " 
	    << AliHLTLog::kDec << modulo << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher", "Event modulo number setting" )
	    << "Unsubscribed subscriber " << interface.GetName() << " attempted to set event modulo to " 
	    << AliHLTLog::kDec << modulo << ENDLOG;
	return ENOENT;
	}
    return 0;
    }

int AliHLTSamplePublisher::SetEventType( AliHLTSubscriberInterface& interface, 
					vector<AliHLTEventTriggerStruct*>& 
					triggerWords, AliUInt32_t modulo )
    {
    vector<SubscriberData*>::iterator res;
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event type setting" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    bool found = false;
    res = FindSubscriber( fSubscribers, &interface );
#if __GNUC__>=3
    if ( res.base() )
#else
    if ( res )
#endif
	{
	found = true;
	if ( (*res)->fTriggers )
	    {
	    for ( unsigned long nn=0; nn<(*res)->fTriggerCnt; nn++ )
		delete [] reinterpret_cast<uint8*>( (*res)->fTriggers );
	    delete [] (*res)->fTriggers;
	    (*res)->fTriggers = NULL;
	    (*res)->fTriggerCnt = 0;
	    }
	(*res)->fTriggers = new AliHLTEventTriggerStruct*[triggerWords.size()];
	if ( !(*res)->fTriggers )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event type setting - Out of memory" )
		<< GetName() << " publisher ouf of memory trying to allocate storage for " << AliHLTLog::kDec
		<< triggerWords.size() << " trigger word structures."
		<< ENDLOG;
	    }
	else
	    {
	    (*res)->fTriggerCnt = triggerWords.size();
	    unsigned long nn=0;
	    vector<AliHLTEventTriggerStruct*>::iterator etIter, etEnd;
	    AliHLTEventTriggerStruct* et;
	    etIter = triggerWords.begin();
	    etEnd = triggerWords.end();
	    while ( etIter != etEnd )
		{
		//et = new AliHLTEventTriggerStruct( **etIter );
		et = reinterpret_cast<AliHLTEventTriggerStruct*>( new AliUInt8_t[ (*etIter)->fHeader.fLength ] );
		if( !et )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event type setting - Out of memory" )
			<< GetName() << " publisher ouf of memory trying to allocate " << AliHLTLog::kDec
			<< (*etIter)->fHeader.fLength << " bytes of trigger words."
			<< ENDLOG;
		    --((*res)->fTriggerCnt);
		    }
		else
		    {
		    memcpy( et, *etIter, (*etIter)->fHeader.fLength );
		    (*res)->fTriggers[nn++] = et;
		    }
		++etIter;
		}
	    }
	if ( modulo )
	    (*res)->fModulo = modulo;
	}
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event type setting" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    if ( found )
	{
	LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher", "Event type setting" )
	    << "Subscriber " << interface.GetName() << " set " << AliHLTLog::kDec << triggerWords.size()
	    << " event types and modulo number to " 
	    << AliHLTLog::kDec << modulo << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher", "Event modulo number setting" )
	    << "Unsubscribed subscriber " << interface.GetName() << " attempted to set " << AliHLTLog::kDec << triggerWords.size()
	    << " event types and modulo number to " 
	    << AliHLTLog::kDec << modulo << ENDLOG;
	return ENOENT;
	}
    return 0;
    }

int AliHLTSamplePublisher::SetTransientTimeout( AliHLTSubscriberInterface& interface, 
					       AliUInt32_t timeout_ms )
    {
    vector<SubscriberData*>::iterator res;
    int ret;
    if ( timeout_ms==0 )
	{
	timeout_ms = 1;
	LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher", "Transient timeout setting" )
	    << GetName() << " publisher increasing passed transient timeout of 0ms for subscriber "
	    << interface.GetName() << " to " << AliHLTLog::kDec << timeout_ms << "ms." << ENDLOG;
	}
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Transient timeout setting" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    bool found = false;
    res = FindSubscriber( fSubscribers, &interface );
#if __GNUC__>=3
    if ( res.base() )
#else
    if ( res )
#endif
	{
	found = true;
	if ( timeout_ms > fMaxTimeout )
	    (*res)->fTimeout = fMaxTimeout;
	else
	    (*res)->fTimeout = timeout_ms;
	}
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Transient timeout setting" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    if ( found )
	{
	LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher", "Transient timeout setting" )
	    << "Subscriber " << interface.GetName() << " set timeout to " 
	    << AliHLTLog::kDec << timeout_ms << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher", "Transient timeout setting" )
	    << "Unsubscribed subscriber " << interface.GetName() << " attempted to set timeout to " 
	    << AliHLTLog::kDec << timeout_ms << ENDLOG;
	return ENOENT;
	}
    return 0;
    }

int AliHLTSamplePublisher::SetEventDoneDataSend( AliHLTSubscriberInterface& interface, bool set )
    {
    vector<SubscriberData*>::iterator res;
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "SetEDDSend subscriber" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    bool found = false;
    res = FindSubscriber( fSubscribers, &interface );
#if __GNUC__>=3
    if ( res.base() )
#else
    if ( res )
#endif
	{
	found = true;
	(*res)->fSendEventDoneData = set;
	}
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "SetEDDSend subscriber" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    if ( found )
	{
	LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher", "SetEDDSend subscriber" )
	    << "Subscriber " << interface.GetName() << " set to " 
	    << (set ? "receive" : "not receive") << "event done data." << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher", "SetEDDSend subscriber" )
	    << "Unsubscribed subscriber " << interface.GetName() << " attempted to be " 
	    << (set ? "receive" : "not receive") << "event done data." << ENDLOG;
	return ENOENT;
	}
    return 0;
    }

int AliHLTSamplePublisher::EventDone( AliHLTSubscriberInterface& interface, 
				      const AliHLTEventDoneData& eventDoneData, bool forceForward, bool forwarded )
    {
    if ( eventDoneData.fDataWordCount<=0 && forceForward )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSamplePublisher", "Empty force forward event done", 100 )
	    << "Subscriber " << interface.GetName() << " sent empty EventDone message with force-forward flag for event 0x" 
	    << AliHLTLog::kHex << eventDoneData.fEventID << " ("
	    << AliHLTLog::kDec << eventDoneData.fEventID << ")." << ENDLOG;
	}
    if ( fEventAccounting )
	fEventAccounting->EventFinished( interface.GetName(), eventDoneData.fEventID, forceForward ? 2 : 1, forwarded ? 2 : 1 );
    double totalWaitT = 0, totalT = 0;
    DECL_PROF( eventDone );
    DECL_PROF( lock );
    DECL_PROF( unlock );
    DECL_SIMPLE_PROF(eventDoneNoLocks);
    START_PROF(eventDone );
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher", "Event done" )
	<< "Subscriber " << interface.GetName() << " sent EventDone message for event 0x" 
	<< AliHLTLog::kHex << eventDoneData.fEventID << " ("
	<< AliHLTLog::kDec << eventDoneData.fEventID << ")." << ENDLOG;
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher", "Event done" )
	<< "EventDoneData of " << AliHLTLog::kDec << eventDoneData.fHeader.fLength
	<< " bytes and " << eventDoneData.fDataWordCount << " data words received for event 0x"
	<< AliHLTLog::kHex << eventDoneData.fEventID << " (" << AliHLTLog::kDec
	<< eventDoneData.fEventID << ")." << ENDLOG;
    vector<SubscriberData*>::iterator res;
    int ret;
    START_PROF(lock);
    ret = pthread_mutex_lock( &fSubscriberMutex );
    END_PROF(lock);
    HISTO_MUTEXLOCK_PROF(lock);
    totalWaitT += GET_PROF(lock);
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event done" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    bool subFound = false;
    bool persistent = false;
    bool sendEDDSubscriber = false;
    res = FindSubscriber( fSubscribers, &interface );
#if __GNUC__>=3
    if ( res.base() )
#else
    if ( res )
#endif
	{
	persistent = (*res)->fPersistent;
	sendEDDSubscriber = (*res)->fSendEventDoneData;
	subFound = true;
	}
    SubscriberData* subscriberData = *(res);
    START_PROF(unlock);
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    END_PROF(unlock);
    HISTO_MUTEXUNLOCK_PROF(unlock);
    totalWaitT += GET_PROF(unlock);
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event done" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    if ( subFound )
	{
	START_PROF(lock);
	EventDoneDataReceived( subscriberData->fInterface->GetName(), eventDoneData.fEventID, &eventDoneData, forceForward, forwarded );

	ret = pthread_mutex_lock( &fEventMutex );
	END_PROF(lock);
	HISTO_MUTEXLOCK_PROF(lock);
	totalWaitT += GET_PROF(lock);
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event done" )
		<< GetName() << " publisher error locking event data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    return ENOLCK;
	    }

	bool sendEDD = false;
	vector<EventSubscriberData> remainingSubscribers;
	if ( !forwarded )
	    {
	    EventDone( eventDoneData.fEventID, &eventDoneData, &interface, 1, (persistent ? 0 : 1), (sendEDDSubscriber ? 1 : 0), true, sendEDD, remainingSubscribers );
	    START_PROF(unlock);
	    ret = pthread_mutex_unlock( &fEventMutex );
	    END_PROF(unlock);
	    HISTO_MUTEXUNLOCK_PROF(unlock);
	    totalWaitT += GET_PROF(unlock);
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event done" )
		    << GetName() << " publisher error unlocking event data mutex."
		    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		    << ENDLOG;
		return ENOLCK;
		}
	    }
	else
	    {
	    sendEDD = true;
	    ret = pthread_mutex_unlock( &fEventMutex );
	    }

	    
	if ( eventDoneData.fDataWordCount>0 || sendEDD )
		{
		ret = pthread_mutex_lock( &fSubscriberMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event done" )
			<< GetName() << " publisher error locking subscriber data mutex."
			<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
			<< ENDLOG;
		    }
		SendEventDoneData( interface, eventDoneData, forwarded, remainingSubscribers );
		ret = pthread_mutex_unlock( &fSubscriberMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event done" )
			<< GetName() << " publisher error unlocking subscriber data mutex."
			<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
			<< ENDLOG;
		    }
		}
	LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher", "Event done" )
	    << "Subscriber " << interface.GetName() << " done with event " 
	    << AliHLTLog::kDec << eventDoneData.fEventID << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher", "Event done" )
	    << "Unsubscribed subscriber " << interface.GetName() << " attempted to be done with event " 
	    << AliHLTLog::kDec << eventDoneData.fEventID << ENDLOG;
	return ENOENT;
	}
    END_PROF(eventDone);
    HISTO_PROF( fEventDoneTimeHistos, eventDone );
    totalT = GET_PROF(eventDone)-totalWaitT;
    SET_PROF(eventDoneNoLocks, (totalT) );
    HISTO_PROF( fEventDoneTimeNoLocksHistos, eventDoneNoLocks );
    return 0;
    }

int AliHLTSamplePublisher::EventDone( AliHLTSubscriberInterface& interface, 
				     vector<AliHLTEventDoneData*>& eventDoneData, bool forceForward, bool forwarded )
    {
    vector<SubscriberData*>::iterator res;
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event done (multiple)" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    bool subFound = false;
    bool persistent = false;
    bool sendEDDSubscriber = false;
    res = FindSubscriber( fSubscribers, &interface );
#if __GNUC__>=3
    if ( res.base() )
#else
    if ( res )
#endif
	{
	persistent = (*res)->fPersistent;
	sendEDDSubscriber = (*res)->fSendEventDoneData;
	subFound = true;
	}
    SubscriberData* subscriberData = *(res);
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event done (multiple)" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    if ( subFound )
	{
	vector<AliHLTEventDoneData*>::iterator evIter, evEnd;
	evIter = eventDoneData.begin();
	evEnd = eventDoneData.end();
	while ( evIter != evEnd )
	    {
	    EventDoneDataReceived( subscriberData->fInterface->GetName(), (*evIter)->fEventID, *evIter, forceForward, forwarded );
	    evIter++;
	    }

	ret = pthread_mutex_lock( &fEventMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event done (multiple)" )
		<< GetName() << " publisher error locking event data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    return ENOLCK;
	    }
	vector<AliEventID_t> sendEDDEvents;
	vector< vector<EventSubscriberData> > remainingSubscribers; 
	sendEDDEvents.reserve( eventDoneData.size() );
	remainingSubscribers.reserve( eventDoneData.size() );
	evIter = eventDoneData.begin();
	evEnd = eventDoneData.end();
	while ( evIter != evEnd )
	    {
	    bool sendEDDTmp = false;
	    vector<EventSubscriberData> remainingSubscribersTmp; 
	    if ( !forwarded )
		{
		EventDone( (*evIter)->fEventID, (*evIter), &interface, 1, (persistent ? 0 : 1), 
			   (sendEDDSubscriber ? 1 : 0), true, sendEDDTmp, remainingSubscribersTmp );
		}
	    remainingSubscribers.push_back( remainingSubscribersTmp );
	    if ( sendEDDTmp )
		{
		sendEDDEvents.push_back( (*evIter)->fEventID );
		}
		
	    evIter++;
	    }
	ret = pthread_mutex_unlock( &fEventMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event done (multiple)" )
		<< GetName() << " publisher error unlocking event data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    return ENOLCK;
	    }


	ret = pthread_mutex_lock( &fSubscriberMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event done" )
		<< GetName() << " publisher error locking subscriber data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    }
	vector<AliEventID_t>::iterator sendEDDEventIter, sendEDDEventEnd;
	vector< vector<EventSubscriberData> >::iterator remainingSubscriberIter; 
	evIter = eventDoneData.begin();
	evEnd = eventDoneData.end();
	remainingSubscriberIter = remainingSubscribers.begin();
	while ( evIter != evEnd )
	    {
	    bool sendEDD = false;
	    if ( (*evIter)->fDataWordCount>0 )
		{
		sendEDD = true;
		}
	    else
		{
		sendEDDEventIter = sendEDDEvents.begin();
		sendEDDEventEnd = sendEDDEvents.end();
		while ( sendEDDEventIter != sendEDDEventEnd )
		    {
		    if ( *sendEDDEventIter==(*evIter)->fEventID )
			{
			sendEDD = true;
			break;
			}
		    ++sendEDDEventIter;
		    }
		}
	    if ( sendEDD )
		SendEventDoneData( interface, **evIter, forwarded, *remainingSubscriberIter );
	    evIter++;
	    remainingSubscriberIter++;
	    }
	ret = pthread_mutex_unlock( &fSubscriberMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event done" )
		<< GetName() << " publisher error unlocking subscriber data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    }
	

	LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher", "Event done (multiple)" )
	    << "Subscriber " << interface.GetName() << " done with " 
	    << AliHLTLog::kDec << eventDoneData.size() << " events."
	    << ENDLOG;

	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher", "Event done (multiple)" )
	    << "Unsubscribed subscriber " << interface.GetName() << " attempted to be done with "
	    << AliHLTLog::kDec << eventDoneData.size() << " events." << ENDLOG;
	return ENOENT;
	}
    return 0;
    }

int AliHLTSamplePublisher::StartPublishing( AliHLTSubscriberInterface& interface, bool sendOldEvents )
    {
    vector<SubscriberData*>::iterator res;
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Start publishing" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    bool found = false;
    res = FindSubscriber( fSubscribers, &interface );
#if __GNUC__>=3
    if ( res.base() )
#else
    if ( res )
#endif
	{
	(*res)->fActive = true;
	found = true;
	}
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Start publishing" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    if ( found )
	{
	LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher", "Start publishing" )
	    << "Publishing started for subscriber " << interface.GetName() 
	    << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher", "Start publishing" )
	    << "Unsubscribed subscriber " << interface.GetName() << " attempted to start publishing"
	    << ENDLOG;
	return ENOENT;
	}
    if ( sendOldEvents )
	SendCurrentEvents( interface );
    return 0;
    }

int AliHLTSamplePublisher::Ping( AliHLTSubscriberInterface& interface )
    {
    vector<SubscriberData*>::iterator res;
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Publisher pinged" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    bool found = false;
    res = FindSubscriber( fSubscribers, &interface );
#if __GNUC__>=3
    if ( res.base() )
#else
    if ( res )
#endif
	{
	found = true;
	ret = interface.PingAck( *this );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Publisher pinged" )
		<< GetName() << " publisher error sending ping ack to subscriber "
		<< interface.GetName() << ". "
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    Ping( *res );
	    }
	}
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Publisher pinged" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    if ( found )
	{
	LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher", "Publisher pinged" )
	    << "Ping received from subscriber " << interface.GetName() 
	    << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher", "Publisher pinged" )
	    << "Unsubscribed subscriber " << interface.GetName() << " attempted to ping publisher "
	    << ENDLOG;
	return ENOENT;
	}
    return 0;
    }

int AliHLTSamplePublisher::PingAck( AliHLTSubscriberInterface& interface )
    {
    vector<SubscriberData*>::iterator res;
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Publisher ping ack'ed" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    bool found = false;
    res = FindSubscriber( fSubscribers, &interface );
#if __GNUC__>=3
    if ( res.base() )
#else
    if ( res )
#endif
	{
	found = true;
	if ( (*res)->fPingCount>0 )
	    (*res)->fPingCount--;
	}
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Publisher ping ack'ed" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    if ( found )
	{
	LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher", "Publisher ping ack'ed" )
	    << "Ping ack received from subscriber " << interface.GetName() 
	    << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher", "Publisher ping ack'ed" )
	    << "Unsubscribed subscriber " << interface.GetName() << " attempted to send ping ack publisher "
	    << ENDLOG;
	return ENOENT;
	}
    return 0;
    }


int AliHLTSamplePublisher::AnnounceEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, 
					  const AliHLTEventTriggerStruct& trigger )
    {
    if ( fEventAccounting )
	fEventAccounting->AnnouncedEvent( fName.c_str(), &sbevent );
    fEventsAborted = false;
    if ( eventID != sbevent.fEventID )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AnnounceEvent", "Incoherent Event ID" )
	    << "Msg event ID 0x" << AliHLTLog::kHex << eventID << " ("
	    << AliHLTLog::kDec << eventID << ") unequal sedd eventID: 0x"
	    << AliHLTLog::kHex << sbevent.fEventID << " ("
	    << AliHLTLog::kDec << sbevent.fEventID << ")." << ENDLOG;
	}
    DECL_PROF( announce );
    DECL_SIMPLE_PROF( announceNoLocks );
    DECL_PROF( lock );
    DECL_PROF( unlock );
    DECL_PROF( setup );
    DECL_PROF( find_subscribers );
    DECL_PROF( store_event );
    DECL_PROF( find_event_slot );
    DECL_PROF( announce_event );
    DECL_PROF( callbacks );
    DECL_PROF( set_timer );
    DECL_PROF( find_timer );
    START_PROF(announce);
    double totalWait = 0;
    if ( sbevent.fOldestEventBirth_s > fOldestEventBirth_s )
	{
	fOldestEventBirth_s = sbevent.fOldestEventBirth_s;
	CleanupOldEvents();
	}
#ifdef EVENT_CHECK_PARANOIA
    if ( fDoParanoiaChecks )
	{
	if ( fLastEventAnnouncedID!=~(AliEventID_t)0 )
	    {
	    if ( fLastEventAnnouncedID+EVENT_CHECK_PARANOIA_LIMIT<eventID || (fLastEventAnnouncedID>EVENT_CHECK_PARANOIA_LIMIT && fLastEventAnnouncedID-EVENT_CHECK_PARANOIA_LIMIT>eventID) )
		{
		LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AnnounceEvent", "Suspicious Event ID" )
		    << "Suspicious Event ID found: 0x" << AliHLTLog::kHex << eventID << " ("
		    << AliHLTLog::kDec << eventID << ") - last announced event ID: 0x"
		    << AliHLTLog::kHex << fLastEventAnnouncedID << " (" << AliHLTLog::kDec 
		    << fLastEventAnnouncedID << ")." << ENDLOG;
		}
	    }
	fLastEventAnnouncedID = eventID;
	}
#endif
    START_PROF( setup );
    vector<SubscriberData*>::iterator iter, end;
    bool found=false;
    AliUInt32_t refCount=0;
    AliUInt32_t transientCount=0;
    AliUInt32_t eddSubscriberCount=0;
    AliUInt32_t maxTimeout = 0;
    AliUInt32_t errCnt = 0, transientErrCnt = 0;
    int ret;
    vector<SubscriberData*> receivers;
    vector<SubscriberData*> subscriberErrs;
    vector<EventSubscriberData> subscribers;

    END_PROF( setup );
    HISTO_PROF( fAnnounceEventTimeSetupHistos, setup );

    START_PROF( find_subscribers );
    START_PROF(lock );
    ret = pthread_mutex_lock( &fSubscriberMutex );
    END_PROF(lock );
    HISTO_MUTEXLOCK_PROF(lock);
    totalWait += GET_PROF(lock);
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event announce" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}

    receivers.reserve( fSubscribers.size() );
    subscribers.reserve( fSubscribers.size() );
    iter = fSubscribers.begin();
    end = fSubscribers.end();
    while ( iter != end )
	{
	if ( !(*iter)->fActive )
	    {
	    iter++;
	    continue;
	    }
	found=false;
	if ( (*iter)->fTriggerCnt<=0 || (sbevent.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST) )
	    found = true;
	else
	    {
	    for ( unsigned long nn=0; nn<(*iter)->fTriggerCnt; nn++ )
		{
		if ( fETTCompFunc( fETTCompFuncParam, (*iter)->fTriggers[nn], &trigger ) )
		    {
		    found = true;
		    break;
		    }
		}
	    }
#if 0
	if ( found && ( (!fEventModuloEventIDBased && !(((*iter)->fCount / fEventModuloPreDivisor) % (*iter)->fModulo))
			|| (fEventModuloEventIDBased && !((eventID.fNr / fEventModuloPreDivisor) % (*iter)->fModulo)) 
			|| (sbevent.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST) ) )
#else
	if ( found && ( fEventModuloCalculation.ModuloMatch( (*iter)->fCount, eventID, (*iter)->fModulo )
			|| (sbevent.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST) ) )
#endif
	    {
	    if ( (*iter)->fSendEventDoneData )
		eddSubscriberCount++;
	    receivers.insert( receivers.end(), *iter );
	    refCount++;
	    if ( !(*iter)->fPersistent )
		{
		if ( (*iter)->fTimeout>maxTimeout )
		    maxTimeout = (*iter)->fTimeout;
		transientCount++;
		}
	    EventSubscriberData esd;
	    esd.fPersistent = (*iter)->fPersistent;
	    esd.fSubscriber = (*iter)->fInterface;
	    subscribers.insert( subscribers.end(), esd );
	    }
	((*iter)->fCount)++;
	iter++;
	}
    START_PROF(unlock );
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    END_PROF(unlock );
    HISTO_MUTEXLOCK_PROF(unlock);
    totalWait += GET_PROF(unlock);
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event announce" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}

    END_PROF( find_subscribers );
    HISTO_PROF( fAnnounceEventTimeFindSubscribersHistos, find_subscribers );

    if ( refCount > 0 )
	{
	START_PROF( store_event );
	START_PROF(lock );
	ret = pthread_mutex_lock( &fEventMutex );
	END_PROF(lock );
	HISTO_MUTEXLOCK_PROF(lock);
	totalWait += GET_PROF(lock);
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event announce" )
		<< GetName() << " publisher error locking event data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    }
	EventData evt;
	evt.fTimerRestartInProgress = false;
	evt.fSubscribers = subscribers;
	evt.fEventDoneData.reserve( subscribers.size() );
	evt.fEventID = eventID;
	evt.fEventBirth_s = sbevent.fEventBirth_s;
	evt.fEventBirth_us = sbevent.fEventBirth_us;
	evt.fRefCount = refCount;
	evt.fTimerState = 1;
	evt.fTransientCount = transientCount;
	evt.fSendEDDSubscriberCount = eddSubscriberCount;
	//evt.fEventTimeout = new EventTimeoutData;
	evt.fEventTimeout = (EventTimeoutData*)fEventTimeoutDataCache.Get();
	evt.fEventTimeout->fEventID = eventID;
#ifdef PERSISTENT_PUBLISHER_TIMEOUT
	evt.fFinalTimeout = false;
#endif
	if ( transientCount <= 0 )
	    {
#ifdef PERSISTENT_PUBLISHER_TIMEOUT
	    evt.fFinalTimeout = true;
#endif
	    //maxTimeout = fMaxTimeout;
	    maxTimeout = 0;
	    }
	else if ( maxTimeout > fMaxTimeout )
	    {
#ifdef PERSISTENT_PUBLISHER_TIMEOUT
	    evt.fFinalTimeout = true;
#endif
	    maxTimeout = fMaxTimeout;
	    }
	evt.fMaxTimeout = maxTimeout;
	evt.fTimerID = ~(AliUInt32_t)0;
	if ( !fEvents.Add( evt, eventID ) )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AnnounceEvent", "Cannot store event" )
		<< "Cannot store event 0x" << AliHLTLog::kHex << eventID
		<< " (" << AliHLTLog::kDec << eventID << ") meta data.." << ENDLOG;
	    return ENOSPC;
	    }

	START_PROF(unlock );
	ret = pthread_mutex_unlock( &fEventMutex );
	END_PROF(unlock );
	HISTO_MUTEXLOCK_PROF(unlock);
	totalWait += GET_PROF(unlock);
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event announce" )
		<< GetName() << " publisher error unlocking event data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    }
	END_PROF( store_event );
	HISTO_PROF( fAnnounceEventTimeStoreEventHistos, store_event );

	START_PROF( announce_event );
	START_PROF(lock );
	ret = pthread_mutex_lock( &fSubscriberMutex );
	END_PROF(lock );
	HISTO_MUTEXLOCK_PROF(lock);
	totalWait += GET_PROF(unlock);
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event announce" )
		<< GetName() << " publisher error locking subscriber data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    }
#if 0
	SubscriberData* lastReceiver = NULL;
	bool thisReceiver = true;
	do
	    {
	    found = false;
	    iter = receivers.begin();
	    end = receivers.end();
	    while ( iter != end )
		{
		if ( thisReceiver )
		    {
		    found = true;
		    thisReceiver = false;
		    lastReceiver = *iter;
		    if ( !lastReceiver )
			{
			LOG( AliHLTLog::kFatal, "AliHLTSamplePublisher::AnnounceEvent", "NULL pointer" )
			    << "lastReceiver is NULL pointer.... (event: 0x" << AliHLTLog::kHex << eventID << " ("
			    << AliHLTLog::kDec << eventID << ")" << ENDLOG;
			break;
			}
		    START_PROF(lock );
		    pthread_mutex_lock( &(lastReceiver->fUsedMutex) );
		    END_PROF(lock );
		    HISTO_MUTEXLOCK_PROF(lock);
		    totalWait += GET_PROF(lock);
		    START_PROF(unlock );
		    ret = pthread_mutex_unlock( &fSubscriberMutex );
		    END_PROF(unlock );
		    HISTO_MUTEXLOCK_PROF(unlock);
		    totalWait += GET_PROF(unlock);
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event announce" )
			    << GetName() << " publisher error locking subscriber data mutex."
			    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
			    << ENDLOG;
			}
		    ret = AnnounceEvent( lastReceiver, eventID, sbevent, trigger );
		    if ( ret )
			{
			if ( !(lastReceiver)->fPersistent )
			    {
			    transientErrCnt++;
			    }
			else
			    {
			    errCnt++;
			    }
			subscriberErrs.insert( subscriberErrs.end(), lastReceiver );
			LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher", "Event announce" )
			    << GetName() << " publisher error announcing event 0x" << AliHLTLog::kHex
			    << eventID << " (" << AliHLTLog::kDec << eventID 
			    << ") to subscriber " << (lastReceiver)->fInterface->GetName()
			    << ". Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
			    << ENDLOG;
			}
		    START_PROF(unlock );
		    pthread_mutex_unlock( &(lastReceiver->fUsedMutex) );
		    END_PROF(unlock );
		    HISTO_MUTEXLOCK_PROF(unlock);
		    totalWait += GET_PROF(unlock);
		    START_PROF(lock );
		    ret = pthread_mutex_lock( &fSubscriberMutex );
		    END_PROF(lock );
		    HISTO_MUTEXLOCK_PROF(lock);
		    totalWait += GET_PROF(lock);
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event announce" )
			    << GetName() << " publisher error locking subscriber data mutex."
			    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
			    << ENDLOG;
			}
		    break;
		    }
		if ( *iter == lastReceiver )
		    thisReceiver = true;
		iter++;
		}
	    }
	while ( found );
#else
	while ( receivers.size()>0 )
	    {
	    found = false;
	    iter = fSubscribers.begin();
	    end = fSubscribers.end();
	    while ( iter != end )
		{
		if ( *iter == *(receivers.begin()) )
		    {
		    found = true;
		    if ( !(*iter) )
			{
			LOG( AliHLTLog::kFatal, "AliHLTSamplePublisher::AnnounceEvent", "NULL pointer" )
			    << "(*iter) is NULL pointer.... (event: 0x" << AliHLTLog::kHex << eventID << " ("
			    << AliHLTLog::kDec << eventID << ")" << ENDLOG;
			break;
			}
		    START_PROF(lock );
		    pthread_mutex_lock( &((*iter)->fUsedMutex) );
		    END_PROF(lock );
		    HISTO_MUTEXLOCK_PROF(lock);
		    totalWait += GET_PROF(lock);
		    START_PROF(unlock );
		    ret = pthread_mutex_unlock( &fSubscriberMutex );
		    END_PROF(unlock );
		    HISTO_MUTEXLOCK_PROF(unlock);
		    totalWait += GET_PROF(unlock);
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event announce" )
			    << GetName() << " publisher error locking subscriber data mutex."
			    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
			    << ENDLOG;
			}
		    ret = AnnounceEvent( (*iter), eventID, sbevent, trigger );
		    if ( ret )
			{
			if ( !((*iter))->fPersistent )
			    {
			    transientErrCnt++;
			    }
			else
			    {
			    errCnt++;
			    }
			subscriberErrs.insert( subscriberErrs.end(), (*iter) );
			LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher", "Event announce" )
			    << GetName() << " publisher error announcing event 0x" << AliHLTLog::kHex
			    << eventID << " (" << AliHLTLog::kDec << eventID 
			    << ") to subscriber " << ((*iter))->fInterface->GetName()
			    << ". Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
			    << ENDLOG;
			}
		    START_PROF(unlock );
		    pthread_mutex_unlock( &((*iter)->fUsedMutex) );
		    END_PROF(unlock );
		    HISTO_MUTEXLOCK_PROF(unlock);
		    totalWait += GET_PROF(unlock);
		    START_PROF(lock );
		    ret = pthread_mutex_lock( &fSubscriberMutex );
		    END_PROF(lock );
		    HISTO_MUTEXLOCK_PROF(lock);
		    totalWait += GET_PROF(lock);
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event announce" )
			    << GetName() << " publisher error locking subscriber data mutex."
			    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
			    << ENDLOG;
			}
		    break;
		    }
		iter++;
		}
	    if ( !found )
		{
		}
	    receivers.erase( receivers.begin() );
	    }
#endif
	START_PROF(unlock );
	ret = pthread_mutex_unlock( &fSubscriberMutex );
	END_PROF(unlock );
	HISTO_MUTEXLOCK_PROF(unlock);
	totalWait += GET_PROF(unlock);
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event announce" )
		<< GetName() << " publisher error unlocking subscriber data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    }
	END_PROF( announce_event );
	HISTO_PROF( fAnnounceEventTimeAnnounceEventHistos, announce_event );
	}

    START_PROF( callbacks );
    AnnouncedEvent( eventID, sbevent, trigger, refCount );
    if ( refCount <= 0 )
	{
	vector<AliHLTEventDoneData*> edd;
	if ( fEventAccounting )
	    fEventAccounting->EventFinished( GetName(), eventID );
	CanceledEvent( eventID, edd );
	}
    END_PROF( callbacks );
    HISTO_PROF( fAnnounceEventTimeCallbacksHistos, callbacks );

    START_PROF( set_timer );
    if ( refCount > 0 && maxTimeout!=0 )
	{
	START_PROF(lock );
	ret = pthread_mutex_lock( &fEventMutex );
	END_PROF(lock );
	HISTO_MUTEXLOCK_PROF(lock);
	totalWait += GET_PROF(lock);
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event announce" )
		<< GetName() << " publisher error locking event data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    }
	//TEventDataListType::TIterator event_iter;
	EventData* event_iter=NULL;
	unsigned long ndx;
	found = false;
	START_PROF( find_timer );
	if ( fEvents.FindElement( eventID, ndx ) )
	    {
	    event_iter = fEvents.GetPtr( ndx );
	    found = true;
	    }
	END_PROF( find_timer );
	HISTO_PROF( fAnnounceEventTimeFindTimerHistos, find_timer );
	if ( found )
	    {
	    if ( event_iter->fEventID == eventID )
		{
		if ( event_iter->fEventTimeout )
		    {
		    event_iter->fTimerState = 3;
		    if ( event_iter->fMaxTimeout )
			event_iter->fTimerID = fTimer.AddTimeout( event_iter->fMaxTimeout, &(fEventThread.fSem), reinterpret_cast<AliUInt64_t>(event_iter->fEventTimeout) );
		    }
		else
		    {
		    event_iter->fTimerState = 2;
		    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AnnounceEvent", "Event Already released" )
			<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kHex << ") already (pre-)released."
			<< " Now releasing fully." << ENDLOG;
		    ReleaseEvent( ndx, event_iter, true );
		    }
		}
	    else
		{
		found = false;
		}
	    }
	if ( !found )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher", "Event Announce Error" )
		<< GetName() << " publisher error finding freshly announced event 0x" << AliHLTLog::kHex
		<< eventID << AliHLTLog::kDec << " (" << eventID 
		<< ") in event list. Could not add timer for this event..." << ENDLOG;
	    }
	START_PROF(unlock );
	ret = pthread_mutex_unlock( &fEventMutex );
	END_PROF(unlock );
	HISTO_MUTEXLOCK_PROF(unlock);
	totalWait += GET_PROF(unlock);
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event announce" )
		<< GetName() << " publisher error unlocking event data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    }
	}
    END_PROF( set_timer );
    HISTO_PROF( fAnnounceEventTimeSetTimerHistos, set_timer );

    if ( transientErrCnt > 0 || errCnt > 0 )
	{
	iter = subscriberErrs.begin();
	end = subscriberErrs.end();
	while ( iter != end )
	    {
	    EventAnnounceError( *iter, eventID );
	    iter++;
	    }
	}

    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher", "Announce events" )
	<< AliHLTLog::kHex << GetName() << " publisher announced event 0x" << eventID
	<< " (" << AliHLTLog::kDec << eventID << ") with " << sbevent.fDataBlockCount 
	<< " data blocks: Ref. count: " 
	<< refCount << " - Transient count: " << transientCount << "." << ENDLOG;
    END_PROF(announce);
    HISTO_PROF(fAnnounceEventTimeHistos,announce);
#ifdef PROFILING
    double tmp;
    tmp = GET_PROF(announce)-totalWait;
#endif
    SET_PROF(announceNoLocks, tmp );
    HISTO_PROF(fAnnounceEventTimeNoLocksHistos,announceNoLocks);
    return 0;
    }


void AliHLTSamplePublisher::AbortEvent( AliEventID_t eventID, bool sendEventCanceled, bool showMessage )
    {
    int ret;
    if ( showMessage )
	{
	LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher::AbortEvent", "Event Aborted" )
	    << GetName() << " event 0x"
	    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << ") aborted forcefully."
	    << ENDLOG;
	}
    ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AbortEvent", "Event Aborted" )
	    << GetName() << " publisher error locking event data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}
    if ( fTimerCurrentCancelEvent != eventID )
	{
	AliUInt32_t rc=0, tc=0;
	unsigned long ndx;
	if ( fEvents.FindElement( eventID, ndx ) )
	    {
	    EventData* iter = fEvents.GetPtr( ndx );
	    rc = iter->fRefCount;
	    tc = iter->fTransientCount;
	    if ( tc < rc && showMessage )
		{
		LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AbortEvent", "Event Aborted" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		    << ") aborted with " << rc-tc << " persistent subscribers." << ENDLOG;
		}
	    bool sendEDDDummy;
	    vector<EventSubscriberData> remainingSubscribersDummy;
	    EventDone( eventID, NULL, NULL, rc, tc, 0, sendEventCanceled, sendEDDDummy, remainingSubscribersDummy );
	    }
	else if ( showMessage )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AbortEvent", "Event Not Found" )
		<< "Aborted event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		<< ") could not be found in event list." << ENDLOG;
	    }

	}
    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AbortEvent", "Event Aborted" )
	    << GetName() << " publisher error unlocking event data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}
    }

void AliHLTSamplePublisher::AbortAllEvents( bool sendEventCanceled, bool showSingleEventMessages )
    {
    fEventsAborted = true;
    AliEventID_t eventID=AliEventID_t();
    int ret;
    while ( fEvents.GetCnt()>0 )
	{
	ret = pthread_mutex_lock( &fEventMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AbortAllEvents", "Event Aborted" )
		<< GetName() << " publisher error locking event data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    }

	eventID = fEvents.GetFirst().fEventID;
	ret = pthread_mutex_unlock( &fEventMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AbortAllEvents", "Event Aborted" )
		<< GetName() << " publisher error unlocking event data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    }
	AbortEvent( eventID, sendEventCanceled, showSingleEventMessages );
	}
    }


int AliHLTSamplePublisher::AcceptSubscriptions( AliHLTAcceptingSubscriptionsCallbackFunc callback, void* callbackData )
    {
    if ( !fSubscriptionInputLoop )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Accept subscriptions" )
	    << "No subscription input loop function specified. Aborting..." 
	    << ENDLOG;
	return ENODEV;
	}
    LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher", "Accept subscriptions" )
	<< GetName() << " publisher entering subscription loop."
	<< ENDLOG;
    int ret;
    //gComTimeout = fComTimeout;
    //ret = PublisherSubscriptionInputLoop( *this );
    ret = fSubscriptionInputLoop( *this, callback, callbackData );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Accept subscriptions" )
	    << GetName() << " publisher error in subscription loop. Reason: "
	    << strerror(ret) << " (" << AliHLTLog::kDec << ret << "). "
	    << ENDLOG;
	return ret;
	}
    LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher", "Accept subscriptions" )
	<< GetName() << " publisher left subscription loop."
	<< ENDLOG;
    return 0;
    }

AliUInt32_t AliHLTSamplePublisher::GetPendingEventCount()
    {
    unsigned long cnt=0;
    int ret;
    ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Get pending event count" )
	    << GetName() << " publisher error locking event data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	errno = ENOLCK;
	return 0;
	}
    cnt = fEvents.GetCnt();

    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Get pending event count" )
	    << GetName() << " publisher error unlocking event data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	errno = ENOLCK;
	return 0;
	}
    return cnt;
    }


void AliHLTSamplePublisher::GetPendingEvents( vector<AliEventID_t>& events )
    {
    int ret;
    ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Get pending event count" )
	    << GetName() << " publisher error locking event data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	errno = ENOLCK;
	return;
	}
    events.reserve( fEvents.GetCnt() );

    fEvents.Iterate( &GetPendingEventsIterationFunc, reinterpret_cast<void*>(&events) );

    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Get pending event count" )
	    << GetName() << " publisher error unlocking event data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	errno = ENOLCK;
	return;
	}
    }


int AliHLTSamplePublisher::RequestEventRelease()
    {
    vector<SubscriberData*>::iterator iter, end;
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Request event release" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    vector<SubscriberData*> subs;
    iter = fSubscribers.begin();
    end = fSubscribers.end();
    while ( iter != end )
	{
	if ( (*iter)->fActive )
	    subs.insert ( subs.end(), *iter );
	iter++;
	}
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Request event release" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}

    iter = subs.begin();
    end = subs.end();
    while ( iter != end )
	{
	pthread_mutex_lock( &((*iter)->fUsedMutex) );
	ret = (*iter)->fSendThread->ReleaseEventsRequest( *this );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Request event release" )
		<< GetName() << " publisher error signalling event release request to subscriber "
		<< (*iter)->fInterface->GetName() << "." << ENDLOG;
	    }
	pthread_mutex_unlock( &((*iter)->fUsedMutex) );
	iter++;
	}
    return 0;
    }
	
int AliHLTSamplePublisher::CancelAllSubscriptions( bool sendUnsubscribeMsgs, bool waitForLoopEnds )
    {
    SubscriberData* data;
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Destructor" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    while ( fSubscribers.size()>0 )
	{
	data = *(fSubscribers.begin());
	pthread_mutex_lock( &(data->fUsedMutex) );
	RemoveSubscriber( data, true, sendUnsubscribeMsgs, true );
	}
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Destructor" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}

    if ( waitForLoopEnds )
	{
	bool found=false;
	/*unsigned cnt=0;*/
	do
	    {
	    found=false;
	    ret = pthread_mutex_lock( &fSubscriberMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Destructor" )
		    << GetName() << " publisher error unlocking subscriber data mutex."
		    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		    << ENDLOG;
		return ENOLCK;
		}
	    
	    vector<SubscriberData*>::iterator subIter, subEnd;
	    subIter = fSubscribers.begin();
	    subEnd = fSubscribers.end();
	    while ( subIter != subEnd )
		{
		if ( (*subIter)->fThreadActive || (*subIter)->fSendThreadActive )
		    {
		    found = true;
		    break;
		    }
		subIter++;
		}
	    
	    ret = pthread_mutex_unlock( &fSubscriberMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Destructor" )
		    << GetName() << " publisher error unlocking subscriber data mutex."
		    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		    << ENDLOG;
		return ENOLCK;
		}
	    if ( !found )
		found = fSubscriberCleanup.HasWork();
	    if ( found )
		usleep( 25000 );
	    }
	while ( found /*++cnt<400*/ );
	}

    return 0;
    }

int AliHLTSamplePublisher::CancelSubscription( const char* subscriberName, bool sendUnsubscribeMsg )
    {
    SubscriberData* data;
    int ret;
    vector<SubscriberData*>::iterator iter, end;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Destructor" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    iter = fSubscribers.begin();
    end = fSubscribers.end();
    while ( iter!=end )
	{
	data = *iter;
	if ( data && data->fInterface && data->fInterface->GetName() && !strcmp( data->fInterface->GetName(), subscriberName ) )
	    {
	    LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher", "Removing subscriber" )
		<< "Removing subscriber '" << data->fInterface->GetName()
		<< "'." << ENDLOG;
	    pthread_mutex_lock( &(data->fUsedMutex) );
	    RemoveSubscriber( data, true, sendUnsubscribeMsg, true );
	    ret = pthread_mutex_unlock( &fSubscriberMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Destructor" )
		    << GetName() << " publisher error unlocking subscriber data mutex."
		    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		    << ENDLOG;
		return ENOLCK;
		}
	    return 0;
	    }
	iter++;
	}
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Destructor" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}


    return ENOENT;
    }

int AliHLTSamplePublisher::PingActive()
    {
    vector<SubscriberData*>::iterator iter, end;
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Ping subscriber" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    iter = fSubscribers.begin();
    end = fSubscribers.end();
    while ( iter != end )
	{
	if ( (*iter)->fActive )
	    Ping( *iter );
	iter++;
	}
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Ping subscriber" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}

    return 0;
    }



void AliHLTSamplePublisher::SetMaxTimeout( AliUInt32_t timeout_ms )
    {
    fMaxTimeout = timeout_ms;
    LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher", "Setting maximum timeout" )
	<< GetName() << " publisher maximum timeout set to "
	<< AliHLTLog::kDec << timeout_ms << " ms."
	<< ENDLOG;
    }

void AliHLTSamplePublisher::SetPingTimeout( AliUInt32_t timeout_ms )
    {
    fPingTimeout = timeout_ms;
    LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher", "Setting ping timeout" )
	<< GetName() << " publisher ping timeout set to "
	<< AliHLTLog::kDec << timeout_ms << " ms."
	<< ENDLOG;
    }

void AliHLTSamplePublisher::SetComTimeout( AliUInt32_t timeout_usec )
    {
    fComTimeout = timeout_usec;
    LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher", "Setting communication timeout" )
	<< GetName() << " publisher communciation timeout set to "
	<< AliHLTLog::kDec << timeout_usec << " musec."
	<< ENDLOG;
    }

void AliHLTSamplePublisher::SetMaxPingCount( AliUInt32_t count )
    {
    fMaxPingCount = count;
    LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher", "Setting maximum ping count" )
	<< GetName() << " publisher maximum ping count set to "
	<< AliHLTLog::kDec << count << "."
	<< ENDLOG;
    }

AliUInt32_t AliHLTSamplePublisher::GetMaxPingCount()
    {
    return fMaxPingCount;
    }

void AliHLTSamplePublisher::PinEvents( bool pin )
    {
    int ret;
    LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher::PinEvents", pin ? "Pinning events" : "Unpinning events" )
	<< "Now " << (pin ? "pinning" : "unpinning") << " events." << ENDLOG;
    if ( pin )
	fPinEvents = true;
    else
	{
	fPinEvents = false;
	ret = pthread_mutex_lock( &fEventMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher::PinEvents", "Mutex Lock" )
		<< GetName() << " publisher error locking event data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    }
	vector<AliEventID_t> unusedEvents;
	fEvents.Iterate( &FindUnusedEventsIterationFunc, &unusedEvents );
	vector<AliEventID_t>::iterator iter, end;
	iter = unusedEvents.begin();
	end = unusedEvents.end();
	while ( iter != end )
	    {
	    unsigned long ndx;
	    if ( fEvents.FindElement( *iter, ndx ) )
		ReleaseEvent( ndx, NULL, true );
	    else
		{
		LOG( AliHLTLog::kFatal, "AliHLTSamplePublisher::PinEvents", "Internal Unpin Error" )
		    << GetName() << " publisher internal error unpinning event 0x" << AliHLTLog::kHex
		    << *iter << " (" << AliHLTLog::kDec << *iter << ")."
		    << ENDLOG;
		}
	    iter++;
	    }
	
	ret = pthread_mutex_unlock( &fEventMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher::PinEvents", "Mutex Unlock" )
		<< GetName() << " publisher error unlocking event data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    }
	}
    }

void AliHLTSamplePublisher::SetEventTriggerTypeCompareFunc( AliHLTEventTriggerTypeCompareFunc compFunc, void* compFuncParam )
    {
    fETTCompFunc = compFunc;
    fETTCompFuncParam = compFuncParam;
    }

void AliHLTSamplePublisher::AddSubscriberEventCallback( SubscriberEventCallback* secb )
    {
    pthread_mutex_lock( &fSubscriberEventCallbackMutex );
    fSubscriberEventCallbacks.insert( fSubscriberEventCallbacks.end(), secb );
    pthread_mutex_unlock( &fSubscriberEventCallbackMutex );
    }

void AliHLTSamplePublisher::DelSubscriberEventCallback( SubscriberEventCallback* secb )
    {
    vector<SubscriberEventCallback*>::iterator iter, end;
    pthread_mutex_lock( &fSubscriberEventCallbackMutex );
    iter = fSubscriberEventCallbacks.begin();
    end = fSubscriberEventCallbacks.end();
    while ( iter!=end )
	{
	if ( *iter == secb )
	    {
	    fSubscriberEventCallbacks.erase( iter );
	    break;
	    }
	iter++;
	}
    pthread_mutex_unlock( &fSubscriberEventCallbackMutex );
    }




AliHLTSamplePublisher::AliHLTSubscriberThread::AliHLTSubscriberThread( AliHLTSamplePublisher& publ, SubscriberData* data ):
    fPub( publ ), fData( data )
    {
    }

AliHLTSamplePublisher::AliHLTSubscriberThread::~AliHLTSubscriberThread()
    {
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTSubscriberThread", "Destructor" )
	<< "Object destroyed." << ENDLOG;
    }

void AliHLTSamplePublisher::AliHLTSubscriberThread::Run()
    {
    if ( !&fPub )
	return;
    if ( fData )
	{
	LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTSubscriberThread", "Starting" )
	    << fPub.GetName() << " publisher starting subscriber thread for subscriber"
	    << (fData->fInterface)->GetName() << "."
	    << ENDLOG;
	fData->fThreadActive = true;
	((AliHLTSubscriberProxyInterface*)fData->fInterface)->MsgLoop( fPub );
	LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTSubscriberThread", "Finishing" )
	    << fPub.GetName() << " publisher finishing subscriber thread for subscriber"
	    << fData->fInterface->GetName() << "."
	    << ENDLOG;
	//fPub.DeleteSubscriber( fData );
	fData->fSendThread->Quit();
	// 	fPub.fSubscriberCleanup.Signal( fData );
	// 	LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher::AliHLTSubscriberSendThread", "Ending" )
	// 	    << fPub.GetName() << " sent cleanup signal for subscriber thread.."
	// 	    << ENDLOG;
	fData->fThreadActive = false;
	}
    }

AliHLTSamplePublisher::AliHLTSubscriberSendThread::AliHLTSubscriberSendThread( AliHLTSamplePublisher& pub, SubscriberData* data, AliUInt32_t msgBufferSizeExp2 ):
    AliHLTSubscriberInterface( data->fInterface->GetName() ), fSendFifo( msgBufferSizeExp2, 14, true ), fPub( pub ), fData( data )
    {
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker quitLock( fQuitMutex );
    fQuit = fQuitted = false;
    quitLock.Unlock();
    fSendFifo.SetMaxShrinkPart( 4 );
    fAnnounceSEDD = NULL;
    fAnnounceSEDDSize = 0;
    fAnnounceEDD = NULL;
    fAnnounceEDDSize = 0;
    fAnnounceETS = NULL;
    fAnnounceETSSize = 0;
    }

AliHLTSamplePublisher::AliHLTSubscriberSendThread::~AliHLTSubscriberSendThread()
    {
    if ( fAnnounceSEDD )
	delete [] (AliUInt8_t*)fAnnounceSEDD;
    if ( fAnnounceEDD )
	delete [] (AliUInt8_t*)fAnnounceEDD;
    if ( fAnnounceETS )
	delete [] (AliUInt8_t*)fAnnounceETS;
    }

void AliHLTSamplePublisher::AliHLTSubscriberSendThread::Quit()
    {
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTSubscriberSendThread", "Quitting" )
	<< fPub.GetName() << " publisher quitting subscriber announce thread."
	<< ENDLOG;
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker quitLock( fQuitMutex );
    fQuit = true;
    quitLock.Unlock();
    fSem.Signal();
    struct timespec ts;
    quitLock.Lock();
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    gettimeofday( &start, NULL );
    while ( !fQuitted && deltaT<timeLimit )
	{
	quitLock.Unlock();
	ts.tv_sec = 0;
	ts.tv_nsec = 1000000;
	nanosleep( &ts, NULL );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	quitLock.Lock();
	}
    }

int AliHLTSamplePublisher::AliHLTSubscriberSendThread::NewEvent( AliHLTPublisherInterface&, AliEventID_t eventID, 
								 const AliHLTSubEventDataDescriptor& sbevent,
								 const AliHLTEventTriggerStruct& ets )
    {
#ifdef EVENT_CHECK_PARANOIA
#if 0
    if ( fDoParanoiaChecks )
	{
	if ( fPub.fLastEventSent1ID==~(AliEventID_t)0 )
	    {
	    if ( eventID != (AliEventID_t)0 )
		{
		LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::NewEvent", "Suspicious First Event ID" )
		    << "First Event ID!=0: 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ")." << ENDLOG;
		}
	    else
		{
		LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::NewEvent", "Suspicious First Event ID" )
		    << "First Event ID is 0." << ENDLOG;
		}
#ifndef NO_MLUCFIFO_PARANOIA
	    fSendFifo.DumpBufferData( AliHLTLog::kError );
#endif
	    }
#endif
	if ( fPub.fLastEventSent1ID!=~(AliEventID_t)0 )
	    {
	    if ( fPub.fLastEventSent1ID+EVENT_CHECK_PARANOIA_LIMIT<eventID || (fPub.fLastEventSent1ID>EVENT_CHECK_PARANOIA_LIMIT && fPub.fLastEventSent1ID-EVENT_CHECK_PARANOIA_LIMIT>eventID) )
		{
		LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::NewEvent", "Suspicious Event ID" )
		    << "Suspicious Event ID found: 0x" << AliHLTLog::kHex << eventID << " ("
		    << AliHLTLog::kDec << eventID << ") - last sent (1) event ID: 0x"
		    << AliHLTLog::kHex << fPub.fLastEventSent1ID << " (" << AliHLTLog::kDec 
		    << fPub.fLastEventSent1ID << ")." << ENDLOG;
		}
	    }
	fPub.fLastEventSent1ID = eventID;
	}
#endif
    MLUCFifoMsg* msg = NULL;
#if 0
    static MLUCFifoMsg* firstmsg;
#endif
    AliUInt32_t msgLen = ets.fHeader.fLength+sbevent.fHeader.fLength+sizeof(MsgData)+sizeof(MLUCFifoMsg);
    MsgData* data = NULL;
    int ret;
    int count = 0;
    do
	{
	ret = fSendFifo.ReserveMsgSlot( msgLen, msg );
	if ( ret==ENOSPC )
	    for ( int myI = 0; myI < MSGSLOTRESERVEWAITCOUNT; myI++ )
			    ;
	}
    while ( ret!=0 && ret==ENOSPC && count++<MSGSLOTRESERVEMAXCOUNT );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::NewEvent", "Reserving message slot" )
	    << "Announce thread for subscriber " << fData->fInterface->GetName() << " unable to allocate new event message slot for message of "
	    << AliHLTLog::kDec << msgLen << " bytes for event 0x"
	    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << "): " << strerror(ret) << " (" << ret
	    << ")." << ENDLOG;
#ifndef NO_MLUCFIFO_PARANOIA
	fSendFifo.DumpBufferData( AliHLTLog::kError );
#endif
	return ret;
	}
    if ( !msg )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::NewEvent", "Reserving message slot" )
	    << "Announce thread for subscriber " << fData->fInterface->GetName() << " unable to allocate new event message slot for event 0x"
	    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
	return ENOMEM;
	}
    msg->fType = AliHLTPubSubPipeMsg::kNewEvent;
    msg->fLength = msgLen;
    data = (MsgData*)(msg->fData);
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::NewEvent", "Signalling new event" )
	<< "Announce thread for subscriber " << fData->fInterface->GetName() << " signalling new event 0x"
	<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
    if ( eventID != sbevent.fEventID )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::NewEvent", "Incoherent Event ID" )
	    << "Msg event ID 0x" << AliHLTLog::kHex << eventID << " ("
	    << AliHLTLog::kDec << eventID << ") unequal sedd eventID: 0x"
	    << AliHLTLog::kHex << sbevent.fEventID << " ("
	    << AliHLTLog::kDec << sbevent.fEventID << ")." << ENDLOG;
	}
    data->fID = eventID;
#if 0
    if ( eventID == (AliEventID_t)0 )
	{
	firstmsg = msg;
	LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::NewEvent", "First " )
	    << "data: 0x" << AliHLTLog::kHex << (unsigned long)data << " msg: 0x" << (unsigned long)msg 
	    << "data->fID: 0x" << data->fID << " (" << AliHLTLog::kDec << data->fID << ")." << ENDLOG;
	}
    else
	{
	if ( firstmsg==msg )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::NewEvent", "Duplicate message use!!" )
		<< "Duplicate msg used: msg: 0x" << AliHLTLog::kHex << (unsigned long)msg << " firstmsg: 0x"
		<< (unsigned long)firstmsg << " event: 0x" << eventID << " (" << AliHLTLog::kDec << eventID
		<< ")." << ENDLOG;
	    }
	}
#endif
    memcpy( data->GetSEDD(), &sbevent, sbevent.fHeader.fLength );
    if ( data->fID != data->GetSEDD()->fEventID )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::NewEvent", "Incoherent Event ID" )
	    << "Stored Fifo msg event ID 0x" << AliHLTLog::kHex << data->fID << " ("
	    << AliHLTLog::kDec << data->fID << ") unequal stored sedd eventID: 0x"
	    << AliHLTLog::kHex << data->GetSEDD()->fEventID << " ("
	    << AliHLTLog::kDec << data->GetSEDD()->fEventID << ")." << ENDLOG;
	}
    memcpy( data->GetETS(), &ets, ets.fHeader.fLength );
    ret = fSendFifo.CommitMsg( msgLen );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::NewEvent", "Committing message to slot" )
	    << "Announce thread for subscriber " << fData->fInterface->GetName() << " unable to commit new event message for event 0x"
	    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << ") to its slot: " << strerror(ret) << " (" << ret
	    << ")." << ENDLOG;
	return ret;
	}
    fSem.Signal();
    return 0;
    }


int AliHLTSamplePublisher::AliHLTSubscriberSendThread::EventDoneData( AliHLTPublisherInterface&, 
								      const AliHLTEventDoneData& eventDoneData )
    {
    MLUCFifoMsg* msg;
#if 0
    static MLUCFifoMsg* firstmsg;
#endif
    AliUInt32_t msgLen = eventDoneData.fHeader.fLength+sizeof(MsgData)+sizeof(MLUCFifoMsg);
    MsgData* data;
    int ret;
    int count = 0;
    do
	{
	ret = fSendFifo.ReserveMsgSlot( msgLen, msg );
	if ( ret==ENOSPC )
	    for ( int myI = 0; myI < MSGSLOTRESERVEWAITCOUNT; myI++ )
			    ;
	}
    while ( ret!=0 && ret==ENOSPC && count++<MSGSLOTRESERVEMAXCOUNT );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::NewEvent", "Reserving message slot" )
	    << "Announce thread for subscriber " << fData->fInterface->GetName() << " unable to allocate event done data message slot for event 0x"
	    << AliHLTLog::kHex << eventDoneData.fEventID << " (" << AliHLTLog::kDec << eventDoneData.fEventID << "): " << strerror(ret) << " (" << ret
	    << ")." << ENDLOG;
#ifndef NO_MLUCFIFO_PARANOIA
	fSendFifo.DumpBufferData( AliHLTLog::kError );
#endif
	return ret;
	}
    if ( !msg )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::NewEvent", "Reserving message slot" )
	    << "Announce thread for subscriber " << fData->fInterface->GetName() << " unable to allocate event done data message slot for event 0x"
	    << AliHLTLog::kHex << eventDoneData.fEventID << " (" << AliHLTLog::kDec << eventDoneData.fEventID << ")." << ENDLOG;
	return ENOMEM;
	}
    msg->fType = AliHLTPubSubPipeMsg::kEventDoneData;
    msg->fLength = msgLen;
    data = (MsgData*)(msg->fData);
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::NewEvent", "Signalling new event" )
	<< "Announce thread for subscriber " << fData->fInterface->GetName() << " signalling event event done data for event 0x"
	<< AliHLTLog::kHex << eventDoneData.fEventID << " (" << AliHLTLog::kDec << eventDoneData.fEventID << ")." << ENDLOG;
    data->fID = eventDoneData.fEventID;
    memcpy( data->GetSEDD(), &eventDoneData, eventDoneData.fHeader.fLength );
    ret = fSendFifo.CommitMsg( msgLen );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::NewEvent", "Committing message to slot" )
	    << "Announce thread for subscriber " << fData->fInterface->GetName() << " unable to commit event done data message for event 0x"
	    << AliHLTLog::kHex << eventDoneData.fEventID << " (" << AliHLTLog::kDec << eventDoneData.fEventID << ") to its slot: " << strerror(ret) << " (" << ret
	    << ")." << ENDLOG;
	return ret;
	}
    fSem.Signal();
    return 0;
    }

int AliHLTSamplePublisher::AliHLTSubscriberSendThread::EventCanceled( AliHLTPublisherInterface&, AliEventID_t eventID )
    {
    MLUCFifoMsg* msg;
    AliUInt32_t msgLen = sizeof(MsgData)+sizeof(MLUCFifoMsg);
    MsgData* data;
    int ret;
    ret = fSendFifo.ReserveMsgSlot( msgLen, msg );
    if ( ret )
	{
	return ret;
	}
    if ( !msg )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::EventCanceled", "Reserving message slot" )
	    << "Announce thread for subscriber " << fData->fInterface->GetName() << " unable to allocate event canceled message slot for event 0x"
	    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
	return ENOMEM;
	}
    msg->fType = AliHLTPubSubPipeMsg::kEventCanceled;
    msg->fLength = msgLen;
    data = (MsgData*)(msg->fData);
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::EventCanceled", "Signalling event canceled" )
	<< "Announce thread for subscriber " << fData->fInterface->GetName() << " signalling event canceled for event 0x"
	<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
    data->fID = eventID;
    ret = fSendFifo.CommitMsg( msgLen );
    if ( ret )
	{
	return ret;
	}
    fSem.Signal();
    return 0;
    }

int AliHLTSamplePublisher::AliHLTSubscriberSendThread::SubscriptionCanceled( AliHLTPublisherInterface& )
    {
    MLUCFifoMsg* msg;
    AliUInt32_t msgLen = sizeof(MLUCFifoMsg);
    int ret;
    ret = fSendFifo.ReserveMsgSlot( msgLen, msg );
    if ( ret )
	{
	return ret;
	}
    if ( !msg )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::SubscriptionCanceled", "Reserving message slot" )
	    << "Announce thread for subscriber " << fData->fInterface->GetName() << " unable to allocate subscription canceled message slot." 
	    << ENDLOG;
	return ENOMEM;
	}
    msg->fType = AliHLTPubSubPipeMsg::kSubscriptionCanceled;
    msg->fLength = msgLen;
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::SubscriptionCanceled", "Signalling susbcription canceled" )
	<< "Announce thread for subscriber " << fData->fInterface->GetName() << " signalling subscription canceled." 
	<< ENDLOG;
    ret = fSendFifo.CommitMsg( msgLen );
    if ( ret )
	{
	return ret;
	}
    fSem.Signal();
    return 0;
    }

int AliHLTSamplePublisher::AliHLTSubscriberSendThread::ReleaseEventsRequest( AliHLTPublisherInterface& )
    {
    MLUCFifoMsg* msg;
    AliUInt32_t msgLen = sizeof(MLUCFifoMsg);
    int ret;
    ret = fSendFifo.ReserveMsgSlot( msgLen, msg );
    if ( ret )
	{
	return ret;
	}
    if ( !msg )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::ReleaseEventsRequest", "Reserving message slot" )
	    << "Announce thread for subscriber " << fData->fInterface->GetName() << " unable to allocate release event request message slot." 
	    << ENDLOG;
	return ENOMEM;
	}
    msg->fType = AliHLTPubSubPipeMsg::kReleaseEventsRequest;
    msg->fLength = msgLen;
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::ReleaseEventsRequest", "Signalling release event request" )
	<< "Announce thread for subscriber " << fData->fInterface->GetName() << " signalling release event request." 
	<< ENDLOG;
    ret = fSendFifo.CommitMsg( msgLen );
    if ( ret )
	{
	return ret;
	}
    fSem.Signal();
    return 0;
    }

int AliHLTSamplePublisher::AliHLTSubscriberSendThread::Ping( AliHLTPublisherInterface& )
    {
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Ping", "Signalling ping" )
	<< "Announce thread for subscriber " << fData->fInterface->GetName() << " signalling ping." 
	<< ENDLOG;
    int ret;
    MLUCFifoMsg msg;
    msg.fType = AliHLTPubSubPipeMsg::kPing;
    msg.fLength = sizeof(MLUCFifoMsg);
    ret = fSendFifo.WriteEmergency( &msg );
    if ( ret )
	{
	return ret;
	}
    fSem.Signal();
    return 0;
    }

int AliHLTSamplePublisher::AliHLTSubscriberSendThread::PingAck( AliHLTPublisherInterface& )
    {
    MLUCFifoMsg* msg;
    AliUInt32_t msgLen = sizeof(MLUCFifoMsg);
    int ret;
    ret = fSendFifo.ReserveMsgSlot( msgLen, msg );
    if ( ret )
	{
	return ret;
	}
    if ( !msg )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::PingAck", "Reserving message slot" )
	    << "Announce thread for subscriber " << fData->fInterface->GetName() << " unable to allocate ping ack message slot." 
	    << ENDLOG;
	return ENOMEM;
	}
    msg->fType = AliHLTPubSubPipeMsg::kPingAck;
    msg->fLength = msgLen;
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::PingAck", "Signalling ping ack" )
	<< "Announce thread for subscriber " << fData->fInterface->GetName() << " signalling ping ack." 
	<< ENDLOG;
    ret = fSendFifo.CommitMsg( msgLen );
    if ( ret )
	{
	return ret;
	}
    fSem.Signal();
    return 0;
    }


void AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run()
    {
    if ( fData )
	fData->fSendThreadActive = true;
    fSem.Lock();
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker quitLock( fQuitMutex );
    fQuit = false;
    fQuitted = false;
    MLUCFifoMsg* msg;
    MsgData* data;
    int ret;
    AliEventID_t eventID;
    while ( !fQuit )
	{
	quitLock.Unlock();
	fSem.Wait();
	msg=NULL;
	while ( !fQuit && (ret = fSendFifo.GetNextMsg( msg ))!=EAGAIN )
	    {
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Error getting message data" )
		    << "Announce thread for subscriber " << fData->fInterface->GetName() << " error getting signaled message data: "
		    << AliHLTLog::kDec << strerror(ret) << " (" << ret << ")." << ENDLOG;
		continue;
		}
	    if ( !msg )
		{
		LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Getting message data" )
		    << "Announce thread for subscriber " << fData->fInterface->GetName() << " error getting signaled message data."
		    << ENDLOG;
		continue;
		}
	    else
		{
		LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Message data" )
		    << "Signalled message data: 0x" << AliHLTLog::kHex << (unsigned long)msg << "." << ENDLOG;
		}
	    if ( msg->fLength < sizeof(MLUCFifoMsg))
		{
		LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Wrong message size" )
		    << "Announce thread for subscriber " << fData->fInterface->GetName() << " found message with wrong size 0x"
		    << AliHLTLog::kHex << msg->fLength << " (" << AliHLTLog::kDec << msg->fLength << ")..." << ENDLOG;
		}
	    data = (MsgData*)msg->fData;
	    if ( !data )
		{
		LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Getting event data" )
		    << "Announce thread for subscriber " << fData->fInterface->GetName() << " error getting signaled event data."
		    << ENDLOG;
		fSendFifo.FreeNextMsg( msg );
		continue;
		}
	    switch ( msg->fType )
		{
		case AliHLTPubSubPipeMsg::kNewEvent:
		    {
		    DECL_PROF(lock);
		    DECL_PROF(unlock);
		    DECL_PROF(newEvent);
		    DECL_SIMPLE_PROF(newEventNoLocks);
		    DECL_PROF(proxy);
		    START_PROF(newEvent);
		    START_PROF(unlock);
		    fSem.Unlock();
		    END_PROF(unlock);
		    if ( fAnnounceSEDD && fAnnounceSEDDSize < data->GetSEDD()->fHeader.fLength )
			{
			delete [] (AliUInt8_t*)fAnnounceSEDD;
			fAnnounceSEDD = NULL;
			}
		    if ( !fAnnounceSEDD )
			{
			fAnnounceSEDDSize = data->GetSEDD()->fHeader.fLength;
			fAnnounceSEDD = (AliHLTSubEventDataDescriptor*)new AliUInt8_t[ fAnnounceSEDDSize ];
			if ( !fAnnounceSEDD )
			    {
			    LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Out of memory" )
				<< "Out of memory trying to allocate new sub-event data descriptor of size "
				<< AliHLTLog::kDec << fAnnounceSEDDSize << "." << ENDLOG;
			    fSendFifo.FreeNextMsg( msg );
			    break;
			    }
			}
		    if ( fAnnounceETS && fAnnounceETSSize < data->GetETS()->fHeader.fLength )
			{
			delete [] (AliUInt8_t*)fAnnounceETS;
			fAnnounceETS = NULL;
			}
		    if ( !fAnnounceETS )
			{
			fAnnounceETSSize = data->GetETS()->fHeader.fLength;
			fAnnounceETS = (AliHLTEventTriggerStruct*)new AliUInt8_t[ fAnnounceETSSize ];
			if ( !fAnnounceETS )
			    {
			    LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Out of memory" )
				<< "Out of memory trying to allocate new sub-event data descriptor of size "
				<< AliHLTLog::kDec << fAnnounceETSSize << "." << ENDLOG;
			    fSendFifo.FreeNextMsg( msg );
			    break;
			    }
			}
		    memcpy( fAnnounceSEDD, data->GetSEDD(), data->GetSEDD()->fHeader.fLength );
		    memcpy( fAnnounceETS, data->GetETS(), data->GetETS()->fHeader.fLength );
		    eventID = data->fID;
#if 0
		    if ( eventID != fAnnounceSEDD->fEventID )
			{
			LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Incoherent Event ID" )
			    << "Fifo msg event ID 0x" << AliHLTLog::kHex << eventID << " ("
			    << AliHLTLog::kDec << eventID << ") unequal copied sedd eventID: 0x"
			    << AliHLTLog::kHex << fAnnounceSEDD->fEventID << " ("
			    << AliHLTLog::kDec << fAnnounceSEDD->fEventID << ")." << ENDLOG;
			}
		    if ( eventID != data->GetSEDD()->fEventID )
			{
			LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Incoherent Event ID" )
			    << "Fifo msg event ID 0x" << AliHLTLog::kHex << eventID << " ("
			    << AliHLTLog::kDec << eventID << ") unequal original sedd eventID: 0x"
			    << AliHLTLog::kHex << data->GetSEDD()->fEventID << " ("
			    << AliHLTLog::kDec << data->GetSEDD()->fEventID << ")." << ENDLOG;
			}
		    if ( fAnnounceSEDD->fEventID != data->GetSEDD()->fEventID )
			{
			LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Incoherent Event ID" )
			    << "Copied sedd eventID: 0x"
			    << AliHLTLog::kHex << fAnnounceSEDD->fEventID << " ("
			    << AliHLTLog::kDec << fAnnounceSEDD->fEventID << ") unequal original sedd eventID: 0x"
			    << AliHLTLog::kHex << data->GetSEDD()->fEventID << " ("
			    << AliHLTLog::kDec << data->GetSEDD()->fEventID << ")." << ENDLOG;
			}
#endif
#ifdef EVENT_CHECK_PARANOIA
#if 0
		    if ( fDoParanoiaChecks )
			{
			if ( fPub.fLastEventSent2ID==~(AliEventID_t)0 )
			    {
			    if ( eventID != (AliEventID_t)0 )
				{
				LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Suspicious First Event ID" )
				    << "First Event ID!=0: 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
				    << eventID << ") -  data: 0x" << AliHLTLog::kHex << (unsigned long)data << " msg: 0x" << (unsigned long)msg << "." << ENDLOG;
				LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Suspicious First Event ID" )
				    << "Fifo grows: " << MLUCLog::kDec << fSendFifo.GetGrowCnt() << " - Fifo shrinks: "
				    << fSendFifo.GetShrinkCnt() << "." << ENDLOG;
#ifndef NO_MLUCFIFO_PARANOIA
				fSendFifo.DumpBufferData( AliHLTLog::kError );
#endif
				}
			    else
				{
				LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Suspicious First Event ID" )
				    << "First Event ID is 0." << ENDLOG;
				}
			    }
#endif
			if ( fPub.fLastEventSent2ID!=~(AliEventID_t)0 )
			    {
			    if ( fPub.fLastEventSent2ID+EVENT_CHECK_PARANOIA_LIMIT<eventID || (fPub.fLastEventSent2ID>EVENT_CHECK_PARANOIA_LIMIT && fPub.fLastEventSent2ID-EVENT_CHECK_PARANOIA_LIMIT>eventID) )
				{
				LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Suspicious Event ID" )
				    << "Suspicious Event ID found: 0x" << AliHLTLog::kHex << eventID << " ("
				    << AliHLTLog::kDec << eventID << ") - last sent (2) event ID: 0x"
				    << AliHLTLog::kHex << fPub.fLastEventSent2ID << " (" << AliHLTLog::kDec 
				    << fPub.fLastEventSent2ID << ")." << ENDLOG;
#ifndef NO_MLUCFIFO_PARANOIA
				LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Suspicious Event ID" )
				    << "Fifo grows: " << MLUCLog::kDec << fSendFifo.GetGrowCnt() << " - Fifo shrinks: "
				    << fSendFifo.GetShrinkCnt() << "." << ENDLOG;
				fSendFifo.DumpBufferData( AliHLTLog::kWarning );
#endif
				}
			    }
			fPub.fLastEventSent2ID = eventID;
			}
#endif
		    fSendFifo.FreeNextMsg( msg );
		    START_PROF(proxy);
		    ret = fData->fInterface->NewEvent( fPub, eventID, *fAnnounceSEDD, *fAnnounceETS );
		    END_PROF(proxy);
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Announce event" )
			    << "Announce thread for subscriber " << fData->fInterface->GetName() << " error sending new event message for event 0x"
			    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << ") "
			    << ". Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."<< ENDLOG;
			fPub.EventAnnounceError( fData, eventID );
			}
		    else
			{
			LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Announcing new event" )
			    << "Announce thread for subscriber " << fData->fInterface->GetName() << " sent new event message for event 0x"
			    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << ") to subscriber." << ENDLOG;
			}

		    START_PROF(lock);
		    fSem.Lock();
		    END_PROF(lock);
		    END_PROF(newEvent);
		    HISTO_PROF(fPub.fSignalLockTimeHistos,lock);
		    HISTO_PROF(fPub.fSubscriberSendThreadNewEventHistos,newEvent);
		    HISTO_PROF(fPub.fSubscriberSendThreadNewEventCallHistos,newEvent);
#ifdef PROFILING
		    double tmp;
		    tmp = GET_PROF(newEvent)-GET_PROF(lock)-GET_PROF(unlock);
#endif
		    SET_PROF(newEventNoLocks,tmp);
		    HISTO_PROF(fPub.fSubscriberSendThreadNewEventNoLocksHistos,newEventNoLocks);
		    break;
		    }
		case AliHLTPubSubPipeMsg::kEventDoneData:
		    {
		    fSem.Unlock();
		    if ( fAnnounceEDD && fAnnounceEDDSize < data->GetEDD()->fHeader.fLength )
			{
			delete [] (AliUInt8_t*)fAnnounceEDD;
			fAnnounceEDD = NULL;
			}
		    if ( !fAnnounceEDD )
			{
			fAnnounceEDDSize = data->GetEDD()->fHeader.fLength;
			fAnnounceEDD = (AliHLTEventDoneData*)new AliUInt8_t[ fAnnounceEDDSize ];
			if ( !fAnnounceEDD )
			    {
			    LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Out of memory" )
				<< "Out of memory trying to allocate new event done data of size "
				<< AliHLTLog::kDec << fAnnounceEDDSize << "." << ENDLOG;
			    fSendFifo.FreeNextMsg( msg );
			    break;
			    }
			}
		    memcpy( fAnnounceEDD, data->GetEDD(), data->GetEDD()->fHeader.fLength );
		    eventID = data->fID;
#if 0
		    if ( eventID != fAnnounceEDD->fEventID )
			{
			LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Incoherent Event ID" )
			    << "Fifo msg event ID 0x" << AliHLTLog::kHex << eventID << " ("
			    << AliHLTLog::kDec << eventID << ") unequal copied edd eventID: 0x"
			    << AliHLTLog::kHex << fAnnounceEDD->fEventID << " ("
			    << AliHLTLog::kDec << fAnnounceEDD->fEventID << ")." << ENDLOG;
			}
		    if ( eventID != data->GetEDD()->fEventID )
			{
			LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Incoherent Event ID" )
			    << "Fifo msg event ID 0x" << AliHLTLog::kHex << eventID << " ("
			    << AliHLTLog::kDec << eventID << ") unequal original edd eventID: 0x"
			    << AliHLTLog::kHex << data->GetEDD()->fEventID << " ("
			    << AliHLTLog::kDec << data->GetEDD()->fEventID << ")." << ENDLOG;
			}
		    if ( fAnnounceEDD->fEventID != data->GetEDD()->fEventID )
			{
			LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Incoherent Event ID" )
			    << "Copied edd eventID: 0x"
			    << AliHLTLog::kHex << fAnnounceEDD->fEventID << " ("
			    << AliHLTLog::kDec << fAnnounceEDD->fEventID << ") unequal original edd eventID: 0x"
			    << AliHLTLog::kHex << data->GetEDD()->fEventID << " ("
			    << AliHLTLog::kDec << data->GetEDD()->fEventID << ")." << ENDLOG;
			}
#endif
		    fSendFifo.FreeNextMsg( msg );
		    ret = fData->fInterface->EventDoneData( fPub, *fAnnounceEDD );
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Send event done data" )
			    << "Announce thread for subscriber " << fData->fInterface->GetName() << " error sending event done data message for event 0x"
			    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << ") "
			    << ". Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."<< ENDLOG;
			}
		    else
			{
			LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Announcing new event" )
			    << "Announce thread for subscriber " << fData->fInterface->GetName() << " sent event done data message for event 0x"
			    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << ") to subscriber." << ENDLOG;
			}
		    fSem.Lock();
		    break;
		    }
		case AliHLTPubSubPipeMsg::kEventCanceled:
		    {
		    fSem.Unlock();
		    eventID = data->fID;
		    fSendFifo.FreeNextMsg( msg );
		    ret = fData->fInterface->EventCanceled( fPub, eventID );
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Cancel event" )
			    << "Announce thread for subscriber " << fData->fInterface->GetName() << " error sending event canceled message"
			    << ". Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."<< ENDLOG;
			fPub.SubscriberCommunicationError( fData );
			}
		    fSem.Lock();
		    break;
		    }
		case AliHLTPubSubPipeMsg::kSubscriptionCanceled:
		    {
		    fSendFifo.FreeNextMsg( msg );
		    fSem.Unlock();
		    ret = fData->fInterface->SubscriptionCanceled( fPub );
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Cancel event" )
			    << "Announce thread for subscriber " << fData->fInterface->GetName() << " error sending subscription canceled message"
			    << ". Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."<< ENDLOG;
			fPub.SubscriberCommunicationError( fData );
			}
		    ((AliHLTSubscriberProxyInterface*)(fData->fInterface))->QuitMsgLoop();
		    fSem.Lock();
		    break;
		    }
		case AliHLTPubSubPipeMsg::kReleaseEventsRequest:
		    {
		    fSendFifo.FreeNextMsg( msg );
		    fSem.Unlock();
		    ret = fData->fInterface->ReleaseEventsRequest( fPub );
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Cancel event" )
			    << "Announce thread for subscriber " << fData->fInterface->GetName() << " error sending release events request message"
			    << ". Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."<< ENDLOG;
			fPub.SubscriberCommunicationError( fData );
			}
		    fSem.Lock();
		    break;
		    }
		case AliHLTPubSubPipeMsg::kPing:
		    {
		    fSendFifo.FreeNextMsg( msg );
		    fSem.Unlock();
		    ret = fData->fInterface->Ping( fPub );
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Ping" )
			    << "Announce thread for subscriber " << fData->fInterface->GetName() << " error sendingping message"
			    << ". Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."<< ENDLOG;
			//fPub.SubscriberCommunicationError( fData );
			}
		    fSem.Lock();
		    break;
		    }
		case AliHLTPubSubPipeMsg::kPingAck:
		    {
		    fSendFifo.FreeNextMsg( msg );
		    fSem.Unlock();
		    ret = fData->fInterface->PingAck( fPub );
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTSubscriberSendThread::Run", "Ping ack" )
			    << "Announce thread for subscriber " << fData->fInterface->GetName() << " error sending ping ack message"
			    << ". Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."<< ENDLOG;
			fPub.SubscriberCommunicationError( fData );
			}
		    fSem.Lock();
		    break;
		    }
		default:
		    fSendFifo.FreeNextMsg( msg );
		    break;
		}
	    }
	quitLock.Lock();
	}
    quitLock.Unlock();
    fSem.Unlock();
    quitLock.Lock();
    fQuitted = true;
    quitLock.Unlock();
    fPub.fSubscriberCleanup.Signal( fData );
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTSubscriberSendThread", "Ending" )
	<< fPub.GetName() << " sent cleanup signal for subscriber thread.."
	<< ENDLOG;
    if ( fData )
	fData->fSendThreadActive = false;
    }






AliHLTSamplePublisher::AliHLTEventTimeoutThread::AliHLTEventTimeoutThread( AliHLTSamplePublisher& publ, int signalSlotCntExp2 ):
    fPub( publ ), fSem( signalSlotCntExp2, true )
    {
    fQuit = false;
    fQuitted = true;
    }

AliHLTSamplePublisher::AliHLTEventTimeoutThread::~AliHLTEventTimeoutThread()
    {
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTEventTimeoutThreadThread", "Destructor" )
	<< "Object destroyed." << ENDLOG;
    }

void AliHLTSamplePublisher::AliHLTEventTimeoutThread::Quit()
    {
    LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher::AliHLTEventTimeoutThread", "Quitting" )
	<< fPub.GetName() << " publisher quitting event timeout thread."
	<< ENDLOG;
    fQuit = true;
    fSem.Signal();
    struct timespec ts;
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    gettimeofday( &start, NULL );
    while ( !fQuitted && deltaT<timeLimit )
	{
	ts.tv_sec = 0;
	ts.tv_nsec = 1000000;
	nanosleep( &ts, NULL );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    }

void AliHLTSamplePublisher::AliHLTEventTimeoutThread::Run()
    {
    int ret;
    fQuit = false;
    fQuitted = false;
    EventTimeoutData *etd;
    LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher::AliHLTEventTimeoutThread", "Starting" )
	<< fPub.GetName() << " publisher started event timeout thread."
	<< ENDLOG;
    fSem.Lock();
    while ( !fQuit )
	{
	LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTEventTimeoutThread", "Signal'ed" )
	    << "Signal received" << ENDLOG;
	if ( fQuit )
	    break;
	while ( fSem.HaveNotificationData() )
	    {
	    etd = reinterpret_cast<EventTimeoutData*>( fSem.PopNotificationData() );
	    if ( !etd )
		continue;
	    fSem.Unlock();
	    LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher::AliHLTEventTimeoutThread", "Timeout expired" )
		<< fPub.GetName() << " publisher timeout for event "
		<< AliHLTLog::kDec << etd->fEventID << " expired."
		<< ENDLOG;
	    ret = pthread_mutex_lock( &(fPub.fEventMutex) );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTEventTimeoutThread", "Timeout expired" )
		    << fPub.GetName() << " publisher error locking event data mutex."
		    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		    << ENDLOG;
		}
	    if ( fPub.fTimerCurrentCancelEvent != etd->fEventID )
		{
		AliEventID_t id = etd->fEventID;
		AliUInt32_t rc=0, tc=0;
		bool doEvent = false;
#ifdef PERSISTENT_PUBLISHER_TIMEOUT
		bool reTimeout = false;
#endif
		unsigned long ndx;
		if ( fPub.fEvents.FindElement( id, ndx ) )
		    {
		    EventData* iter = fPub.fEvents.GetPtr( ndx );
#ifdef PERSISTENT_PUBLISHER_TIMEOUT
		    if ( iter->fFinalTimeout || iter->fRefCount == iter->fTransientCount )
			{
			rc = iter->fRefCount;
			tc = iter->fTransientCount;
			doEvent = true;
			}
		    else
			{
			reTimeout = true;
			}
#else
		    if ( iter->fTimerRestartInProgress )
			doEvent = false;
		    else
			doEvent = true;
		    rc = tc = iter->fTransientCount;
		    iter->fTimerID = ~(AliUInt32_t)0;
#endif
		    }

		if ( doEvent )
		    {
		    if ( tc < rc )
			{
			LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTEventTimeoutThread", "Timeout expired" )
			    << "Timeout for event 0x" << AliHLTLog::kHex << id << " (" << AliHLTLog::kDec << id
			    << ") expired with " << rc-tc << " persistent subscribers." << ENDLOG;
			}
		    bool sendEDDDummy;
		    vector<EventSubscriberData> remainingSubscribersDummy;
		    fPub.EventDone( id, NULL, NULL, rc, tc, 0, true, sendEDDDummy, remainingSubscribersDummy );
		    }
#ifdef PERSISTENT_PUBLISHER_TIMEOUT
		else if ( reTimeout )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTEventTimeoutThread", "Redoing event timeout" )
			<< fPub.GetName() << " publisher redoing timeout for event 0x"
			<< AliHLTLog::kHex << iter->fEventID << " (" << AliHLTLog::kDec
			<< iter->fEventID << ")." << ENDLOG;
		    AliUInt32_t newTimeout;
		    iter->fFinalTimeout = true;
		    iter->fTimerID = ~(AliUInt32_t)0;
		    newTimeout = fPub.fMaxTimeout-iter->fMaxTimeout;
		    ret = pthread_mutex_unlock( &(fPub.fEventMutex) );
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTEventTimeoutThread", "Timeout expired" )
			    << fPub.GetName() << " publisher error unlocking event data mutex."
			    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
			    << ENDLOG;
			}
		    AliUInt32_t timerID = fPub.fTimer.AddTimeout( newTimeout, &fPub.fEventThread.fSem, reinterpret_cast<AliUInt64_t>(etd) );
		    // Check for current cancel event and if this is our event, cancel the started timer.
		    // otherwise set the stored timer ID for this event to the new one just started.
		    ret = pthread_mutex_lock( &(fPub.fEventMutex) );
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTEventTimeoutThread", "Timeout expired" )
			    << fPub.GetName() << " publisher error locking event data mutex."
			    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
			    << ENDLOG;
			}
		    if ( fPub.fTimerCurrentCancelEvent != id )
			{
			if ( fPub.fEvents.FindElement( id, ndx ) )
			    fPub.fEvents.GetPtr( ndx )->fTimerID = timerID;
			}
		    else
			{
			fPub.fTimer.CancelTimeout( timerID );
			}
		    }
#endif
		else
		    {
		    LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher::AliHLTEventTimeoutThread", "Event Not Found" )
			<< "Timed out event 0x" << AliHLTLog::kHex << id << " (" << AliHLTLog::kDec << id 
			<< ") could not be found in event list." << ENDLOG;
		    }

		}
	    ret = pthread_mutex_unlock( &(fPub.fEventMutex) );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTEventTimeoutThread", "Timeout expired" )
		    << fPub.GetName() << " publisher error unlocking event data mutex."
		    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		    << ENDLOG;
		}
	    fSem.Lock();
	    }
	fSem.Wait();
	}
    fSem.Unlock();
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTEventTimeoutThread", "Leaving" )
	<< fPub.GetName() << " publisher leaving event timeout thread."
	<< ENDLOG;
    fQuitted = true;
    }


AliHLTSamplePublisher::AliHLTPingTimeoutThread::AliHLTPingTimeoutThread( AliHLTSamplePublisher& publ ):
    fPub( publ )
    {
    fQuit = false;
    fQuitted = false;
    }

AliHLTSamplePublisher::AliHLTPingTimeoutThread::~AliHLTPingTimeoutThread()
    {
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTPingTimeoutThreadThread", "Destructor" )
	<< "Object destroyed." << ENDLOG;
    }

void AliHLTSamplePublisher::AliHLTPingTimeoutThread::Quit()
    {
    LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher::AliHLTPingTimeoutThread", "Quitting" )
	<< fPub.GetName() << " publisher quitting ping timeout thread."
	<< ENDLOG;
    fQuit = true;
    fSem.Signal();
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    gettimeofday( &start, NULL );
    struct timespec ts;
    while ( !fQuitted && deltaT<timeLimit )
	{
	ts.tv_sec = 0;
	ts.tv_nsec = 1000000;
	nanosleep( &ts, NULL );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    }

void AliHLTSamplePublisher::AliHLTPingTimeoutThread::Run()
    {
    fSem.Lock();
    fQuit = false;
    fQuitted = false;
    LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher::AliHLTPingTimeoutThread", "Starting" )
	<< fPub.GetName() << " publisher started ping timeout thread."
	<< ENDLOG;
    while ( !fQuit )
	{
	fSem.Wait();
	if ( fQuit )
	    break;
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTPingTimeoutThread", "Ping timeout expired" )
	    << "Ping timeout expired..." << ENDLOG;
	while ( fSem.HaveNotificationData() )
	    {
	    SubscriberData* data = reinterpret_cast<SubscriberData*>( fSem.PopNotificationData() );
	    if ( !data )
		{
		LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTPingTimeoutThread", "Ping timeout expired" )
		    << "Ping timeout expired for unknown subscriber..." << ENDLOG;
		continue;
		}
	    //data->fPingCount++;
 	    LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTPingTimeoutThread", "Ping timeout expired" )
		<< fPub.GetName() << " publisher ping timeout for subscriber "
		<< data->fInterface->GetName() << " expired. (" 
		<< AliHLTLog::kDec << data->fPingCount << " expired timeouts of " 
		<< fPub.fMaxPingCount << " maximum)"
		<< ENDLOG;
	    fPub.PingTimeout( data );
	    if ( data->fPingCount >= fPub.fMaxPingCount )
		{
		LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::AliHLTPingTimeoutThread", "Ping timeout expired" )
		    << fPub.GetName() << " publisher maximum number of ping timeouts reached for subscriber "
		    << data->fInterface->GetName() << "." 
		    << ENDLOG;
		fPub.MaxPingTimeout( data );
		}
	    }
	}
    fSem.Unlock();
    fQuitted = true;
    }


AliHLTSamplePublisher::AliHLTSubscriberCleanupThread::AliHLTSubscriberCleanupThread( AliHLTSamplePublisher& pub ):
    fPub( pub )
    {
    fQuit = false;
    fQuitted = false;
    fHasWork = false;
    }

AliHLTSamplePublisher::AliHLTSubscriberCleanupThread::~AliHLTSubscriberCleanupThread()
    {
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::AliHLTSubscriberCleanupThread", "Destructor" )
	<< "Object destroyed." << ENDLOG;
    }


void AliHLTSamplePublisher::AliHLTSubscriberCleanupThread::Quit()
    {
    fQuit = true;
    fSem.Signal();
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    gettimeofday( &start, NULL );
    struct timespec ts;
    while ( !fQuitted && deltaT<timeLimit )
	{
	ts.tv_sec = 0;
	ts.tv_nsec = 1000000;
	nanosleep( &ts, NULL );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    }

void AliHLTSamplePublisher::AliHLTSubscriberCleanupThread::Run()
    {
    fSem.Lock();
    fQuit = false;
    fQuitted = false;
    AliHLTSamplePublisher::SubscriberData* data;
    while ( !fQuit || fLockedSubscribers.size()>0 || fSem.HaveNotificationData() )
	{
#if 0
	struct timespec ts;
	ts.tv_sec = 0;
	ts.tv_nsec = 50000000;
	nanosleep( &ts, NULL );
	fSem.Wait();
#else
	fSem.Wait( 500 );
#endif

	vector<SubscriberData*>::iterator iter, end;
	bool found;
	do
	    {
	    found = false;
	    iter = fLockedSubscribers.begin();
	    end = fLockedSubscribers.end();
	    while ( iter != end )
		{
		if ( pthread_mutex_trylock( &((*iter)->fUsedMutex) )!=EBUSY )
		    {
		    if ( !(*iter)->fThreadActive && !(*iter)->fSendThreadActive ) // || fQuit )
			{
// 		        pthread_mutex_unlock( &((*iter)->fUsedMutex) );
			fPub.DeleteSubscriber( *iter );
			fLockedSubscribers.erase( iter );
			found = true;
			break;
			}
		    else
			pthread_mutex_unlock( &((*iter)->fUsedMutex) );
		    }
		iter++;
		}
	    }
	while ( found );

	while ( fSem.HaveNotificationData() )
	    {
	    data = fSem.PopNotificationData();
	    if ( pthread_mutex_trylock( &(data->fUsedMutex) )==EBUSY )
		{
// 		pthread_mutex_unlock( &(data->fUsedMutex) );
		fLockedSubscribers.insert( fLockedSubscribers.end(), data );
		}
	    else if (  data->fThreadActive || data->fSendThreadActive )
		{
		pthread_mutex_unlock( &(data->fUsedMutex) );
		fLockedSubscribers.insert( fLockedSubscribers.end(), data );
		}
	    else
		fPub.DeleteSubscriber( data );
	    }

	fHasWork = !fLockedSubscribers.empty();
	}
    fSem.Unlock();
    fQuitted = true;
    }

void AliHLTSamplePublisher::AliHLTSubscriberCleanupThread::Signal( SubscriberData* data )
    {
    fSem.AddNotificationData( data );
    fSem.Signal();
    }


unsigned long AliHLTSamplePublisher::FindEventSlot( AliEventID_t eventID )
    {
    unsigned long ndx;
    if ( fEvents.FindElement( eventID, ndx ) )
	return ndx;
    else
	return ~(unsigned long)0;
    }

void AliHLTSamplePublisher::SendEventDoneData( AliHLTSubscriberInterface& interface, const AliHLTEventDoneData& edd, bool forwarded, const vector<EventSubscriberData>& remainingSubscribers )
    {
    vector<SubscriberData*>::iterator iter, end;
    iter = fSubscribers.begin();
    end = fSubscribers.end();
    while ( iter != end )
	{
	if ( !(*iter)->fActive || !(*iter)->fSendEventDoneData ||
	     (*iter)->fInterface == &interface )
	    {
	    iter++;
	    continue;
	    }
	bool found=false;
	vector<EventSubscriberData>::const_iterator esdIter, esdEnd;
	esdIter = remainingSubscribers.begin();
	esdEnd = remainingSubscribers.end();
	while ( esdIter != esdEnd )
	    {
	    if ( esdIter->fSubscriber==(*iter)->fInterface )
		{
		found=true;
		break;
		}
	    esdIter++;
	    }
	if ( !found && !forwarded )
	    {
	    iter++;
	    continue;
	    }
	(*iter)->fSendThread->EventDoneData( *this, edd );
	iter++;
	}
    }

int AliHLTSamplePublisher::SendCurrentEvents( AliHLTSubscriberInterface& interface )
    {
    // XXX TIMER
    vector<SubscriberData*>::iterator res;
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "SendCurrentEvents" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return ENOLCK;
	}
    bool subFound = false;
    //bool persistent = false;
    AliUInt32_t timeout = 0;
    TSendCurrentEventsData sced;
    sced.fESD.fSubscriber = &interface;
    sced.fESD.fPersistent = false;
    sced.fTimer = &fTimer;
    res = FindSubscriber( fSubscribers, &interface );
    if ( res.base() )
	{
	sced.fESD.fPersistent = (*res)->fPersistent;
	subFound = true;
	timeout = (*res)->fTimeout;
	pthread_mutex_lock( &((*res)->fUsedMutex) );
	}
    SubscriberData* subscriberData = *(res);
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( !subFound )
	return EFAULT;
    if ( timeout>fMaxTimeout )
	timeout = fMaxTimeout;

    ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::SendCurrentEvents", "Mutex Lock" )
	    << GetName() << " publisher error locking event data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}
    
    sced.fEvents.reserve( fEvents.GetCnt() );
    fEvents.Iterate( SendCurrentEventsIterationFunc, &sced );
    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::SendCurrentEvents", "Mutex Unlock" )
	    << GetName() << " publisher error unlocking event data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}


    AliHLTEventTriggerStruct* ets;
    AliHLTSubEventDataDescriptor* sedd;
    AliEventID_t eventID;
    vector<AliEventID_t>::iterator iter, end;
    iter = sced.fEvents.begin();
    end = sced.fEvents.end();
    vector<AliEventID_t> errorEvents;
    while ( iter!=end )
	{
	eventID = *iter;
	if ( GetAnnouncedEventData( eventID, sedd, ets ) )
	    {
	    ret = AnnounceEvent( subscriberData, eventID, *sedd, *ets );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher", "SendCurrentEvents" )
		    << GetName() << " publisher error announcing event 0x" << AliHLTLog::kHex
		    << eventID << " (" << AliHLTLog::kDec << eventID 
		    << ") to subscriber " << subscriberData->fInterface->GetName()
		    << ". Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		    << ENDLOG;
		errorEvents.insert( errorEvents.end(), eventID );
		}
	    }
	else
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher", "SendCurrentEvents" )
		<< GetName() << " publisher unable to obtain data to announce old event 0x" << AliHLTLog::kHex
		<< eventID << " (" << AliHLTLog::kDec << eventID 
		<< ") to subscriber " << subscriberData->fInterface->GetName() << "."
		<< ENDLOG;
	    errorEvents.insert( errorEvents.end(), eventID );
	    }
	iter++;
	}

    iter = errorEvents.begin();
    end = errorEvents.end();
    vector<AliEventID_t>::iterator tmpIter, tmpEnd;
    while ( iter != end )
	{
	eventID = *iter;
	EventAnnounceError( subscriberData, eventID );
	if ( !sced.fESD.fPersistent )
	    {
	    tmpIter = sced.fEvents.begin();
	    tmpEnd = sced.fEvents.end();
	    while ( tmpIter!=tmpEnd )
		{
		if ( *tmpIter == eventID )
		    {
		    sced.fEvents.erase( tmpIter );
		    break;
		    }
		tmpIter++;
		}
	    }
	iter++;
	}


    if ( !sced.fESD.fPersistent )
	{
	ret = pthread_mutex_lock( &fEventMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher::SendCurrentEvents", "Mutex Lock" )
		<< GetName() << " publisher error locking event data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    }
	
	iter = sced.fEvents.begin();
	end = sced.fEvents.end();
	//TEventDataListType::iterator evIter, evEnd;
	while ( iter!=end )
	    {
	    unsigned long ndx;
	    if ( fEvents.FindElement( *iter, ndx ) )
		{
		EventData* evtIter = fEvents.GetPtr( ndx );
		if ( timeout > evtIter->fMaxTimeout )
		    evtIter->fMaxTimeout = timeout;
		if ( timeout < evtIter->fMaxTimeout )
		    timeout = evtIter->fMaxTimeout;
		evtIter->fTimerID = fTimer.AddTimeout( timeout, &(fEventThread.fSem), reinterpret_cast<AliUInt64_t>(evtIter->fEventTimeout) );
		}
	    else
		{
		// Should never happen.
		LOG( AliHLTLog::kFatal, "AliHLTSamplePublisher", "SendCurrentEvents" )
		    << "This will never happen..." << ENDLOG;
		}
	    iter++;
	    }
	ret = pthread_mutex_unlock( &fEventMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher::SendCurrentEvents", "Mutex Unlock" )
		<< GetName() << " publisher error unlocking event data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    }
	}

    pthread_mutex_unlock( &(subscriberData->fUsedMutex) );
    return 0;
    }



void AliHLTSamplePublisher::ReleaseEvent( unsigned long evtNdx, EventData* evtIter, bool sendEventCanceled )
    {
    if ( !evtIter )
	evtIter = fEvents.GetPtr( evtNdx );
    AliUInt32_t evtIterfTimerState = evtIter->fTimerState;
    AliUInt32_t evtIterfMaxTimeout = evtIter->fTimerState;
    int ret;
    AliUInt32_t timerID = evtIter->fTimerID;
    bool cleared = false;

    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::ReleaseEvent", "Releasing Event" )
	<< GetName() << " publisher about to release event 0x" << AliHLTLog::kHex
	<< evtIter->fEventID << " (" << AliHLTLog::kDec << evtIter->fEventID
	<< ") - evtIter->fTimerState: " << MLUCLog::kDec << (unsigned long)evtIter->fTimerState << " - evtIter->fEventTimeout: 0x" << AliHLTLog::kHex << (unsigned long)evtIter->fEventTimeout
	<< " - timerID: 0x" << AliHLTLog::kHex << (unsigned long)timerID << " (" << AliHLTLog::kDec
	<< (unsigned long)timerID << " - evtIter->fMaxTimeout 0x" << AliHLTLog::kHex << (unsigned long)evtIter->fMaxTimeout
	<< " (" << AliHLTLog::kDec << (unsigned long)evtIter->fMaxTimeout << ")." << ENDLOG;

    if ( fPinEvents )
	{
	LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::ReleaseEvent", "Pin Events Active" )
	    << GetName() << " publisher release event 0x" << AliHLTLog::kHex
	    << evtIter->fEventID << " (" << AliHLTLog::kDec << evtIter->fEventID
	    << "): pin events is activated." << ENDLOG;
	return;
	}
    
    //if ( !evtIter->fEventTimeout && timerID == ~(AliUInt32_t)0 )
    if ( evtIter->fTimerState==2 )
	{
	LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::ReleaseEvent", "Clearing Event" )
	    << GetName() << " publisher clearing event 0x" << AliHLTLog::kHex
	    << evtIter->fEventID << " (" << AliHLTLog::kDec << evtIter->fEventID
	    << ")." << ENDLOG;
	fEvents.Remove( evtNdx );
	cleared = true;
	return;
	}
    //vector<EventSubscriberData>::iterator subIter, subEnd;
    vector<EventSubscriberData>& subs = evtIter->fSubscribers;
    vector<EventSubscriberData> trans( subs );
    vector<AliHLTEventDoneData*> edd( evtIter->fEventDoneData );
    AliEventID_t id;
//     subIter = subs.begin();
//     subEnd = subs.end();
//     while ( subIter != subEnd )
// 	{
// 	//(*subIter)->EventCanceled( fEvents[ndx].fEventID );
// 	trans.insert( trans.end(), (*subIter) );
// 	subIter++;
// 	}
    
    id = evtIter->fEventID;
    fTimerCurrentCancelEvent = evtIter->fEventID;
    if ( evtIter->fEventTimeout )
	{
	//delete evtIter->fEventTimeout;
	fEventTimeoutDataCache.Release( (uint8*)(evtIter->fEventTimeout) );
	evtIter->fEventTimeout = NULL;
	}
    //if ( timerID != ~(AliUInt32_t)0 || !evtIter->fMaxTimeout )
    if ( evtIter->fTimerState==3 || (evtIter->fTimerState==1 && !evtIter->fMaxTimeout) )
	{
	LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::ReleaseEvent", "Clearing Event" )
	    << GetName() << " publisher clearing event 0x" << AliHLTLog::kHex
	    << evtIter->fEventID << " (" << AliHLTLog::kDec << evtIter->fEventID
	    << ")." << ENDLOG;
	fEvents.Remove( evtNdx );
	cleared = true;
	}
    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Release event" )
	    << GetName() << " publisher error unlocking event data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}
    if ( timerID != ~(AliUInt32_t)0 )
	fTimer.CancelTimeout( timerID );
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Release event" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}
    CancelEvent( id, edd, trans, sendEventCanceled );
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Release event" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}
    ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Release event" )
	    << GetName() << " publisher error locking event data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}
    fTimerCurrentCancelEvent = AliEventID_t();
    if ( !cleared && evtIterfTimerState!=1 )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSamplePublisher::ReleaseEvent", "Event not cleared" )
	    << GetName() << " publisher event 0x" << AliHLTLog::kHex
	    << id << " (" << AliHLTLog::kDec << id
	    << ") not cleared (evtIter->fTimerState: " 
	    << evtIterfTimerState << " - evtIter->fMaxTimeout: " << evtIterfMaxTimeout 
	    << ")." << ENDLOG;
	AliHLTStackTrace( "AliHLTSamplePublisher::ReleaseEvent", AliHLTLog::kFatal, 16 );
	}
    }

void AliHLTSamplePublisher::EventDone( AliEventID_t eventID, const AliHLTEventDoneData* edd, AliHLTSubscriberInterface* sub,
				       AliUInt32_t refCount, AliUInt32_t transientCount, AliUInt32_t eddSubscriberCount, 
				       bool sendEventCanceled, bool& sendEDD, vector<EventSubscriberData>& remainingSubscribers )
    {
    remainingSubscribers.clear();
    sendEDD = false;
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher", "Event Done" )
	<< "EventID: 0x" << AliHLTLog::kHex << eventID << " - Sub: 0x" << (unsigned long)sub
	<< " - Ref. count: " << AliHLTLog::kDec << refCount << " - Transient count: " << transientCount << "." << ENDLOG;
#ifdef EVENT_CHECK_PARANOIA
    if ( fDoParanoiaChecks )
	{
	if ( fLastEventDoneID!=~(AliEventID_t)0 )
	    {
	    if ( fLastEventDoneID+EVENT_CHECK_PARANOIA_LIMIT<eventID || (fLastEventDoneID>EVENT_CHECK_PARANOIA_LIMIT && fLastEventDoneID-EVENT_CHECK_PARANOIA_LIMIT>eventID) )
		{
		LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::EventDone", "Suspicious Event ID" )
		    << "Suspicious Event ID found: 0x" << AliHLTLog::kHex << eventID << " ("
		    << AliHLTLog::kDec << eventID << ") - last done event ID: 0x"
		    << AliHLTLog::kHex << fLastEventDoneID << " (" << AliHLTLog::kDec 
		    << fLastEventDoneID << ")." << ENDLOG;
		}
	    }
	fLastEventDoneID = eventID;
	}
#endif
    //AliUInt32_t ndx = GetEventIndex( eventID );
    unsigned long evtNdx;
    EventData* evtIter = NULL;
    if ( fEvents.FindElement( eventID, evtNdx ) )
	evtIter = fEvents.GetPtr( evtNdx );
    else 
	{
	if ( !fEventsAborted )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher", "Event Done" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") supposed to be done has not even started yet..." << ENDLOG;
	    }
	//AliHLTStackTrace( "AliHLTSamplePublisher::EventDone(AliEventID_t,AliHLTSubscriberInterface*,AliUInt32_t)", AliHLTLog::kError, 16 );
	return;
	}
    if ( edd && edd->fDataWordCount>0 )
	{
	AliHLTEventDoneData* eddTmp;
	if ( fCachedEDDAllocs )
	    eddTmp = (AliHLTEventDoneData*)fEDDCache.Get( edd->fHeader.fLength );
	else
	    eddTmp = (AliHLTEventDoneData*)new AliUInt8_t[ edd->fHeader.fLength ];
	if ( !eddTmp )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event Done" )
		<< "Out of memory trying to allocate event done data for event 0x"
		<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ")." << ENDLOG;
	    }
	else
	    {
	    memcpy( eddTmp, edd, edd->fHeader.fLength );
	    evtIter->fEventDoneData.insert( evtIter->fEventDoneData.end(), eddTmp );
	    }
	}
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher", "Event Done" )
	<< "Old Event reference counts: Total: " << AliHLTLog::kDec << 
	evtIter->fRefCount << " - Transient: " << evtIter->fTransientCount
	<< "." << ENDLOG;

    if ( evtIter->fRefCount < refCount )
	evtIter->fRefCount = 0;
    else
	evtIter->fRefCount -= refCount;

    if ( evtIter->fTransientCount < transientCount )
	evtIter->fTransientCount = 0;
    else
	evtIter->fTransientCount -= transientCount;
    if ( evtIter->fSendEDDSubscriberCount < eddSubscriberCount )
	evtIter->fSendEDDSubscriberCount = 0;
    else
	evtIter->fSendEDDSubscriberCount -= eddSubscriberCount;
    if ( evtIter->fSendEDDSubscriberCount>=evtIter->fRefCount && evtIter->fEventDoneData.empty() )
	sendEDD = true;

    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher", "Event Done" )
	<< "New Event reference counts: Total: " << AliHLTLog::kDec << 
	evtIter->fRefCount << " - Transient: " << evtIter->fTransientCount
	<< "." << ENDLOG;
    
    if ( sub )
	{
	LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher", "Event Done" )
	    << "Finishing for subscriber..." << ENDLOG;
	vector<EventSubscriberData>::iterator subIter, subEnd;
	vector<EventSubscriberData>& subs = evtIter->fSubscribers;
	subIter = subs.begin();
	subEnd = subs.end();
	while ( subIter != subEnd )
	    {
	    if ( (*subIter).fSubscriber == sub )
		{
		subs.erase( subIter );
		break;
		}
	    subIter++;
	    }
	remainingSubscribers = evtIter->fSubscribers;
	}
    if ( evtIter->fRefCount <= 0 )
	ReleaseEvent( evtNdx, evtIter, sendEventCanceled );
#if 0 // Does not work. Maybe obsolte with better publisher...
    else if ( transientCount>0 && evtIter->fTransientCount==0 )
	{
	// Remove intermediate timeout and set new final one.
	//LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher::EventDone", "Removing transient timeout" )
	LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher::EventDone", "Removing transient timeout" )
	    << "Removing transient timeout for event 0x" << AliHLTLog::kHex
	    << eventID << " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
	AliUInt32_t newTimeout;
	int ret;
	newTimeout = fMaxTimeout-evtIter->fMaxTimeout;
	AliUInt32_t timerID = evtIter->fTimerID;
	ret = pthread_mutex_unlock( &fEventMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher::EventDone", "Timeout expired" )
		<< GetName() << " publisher error unlocking event data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    }
	fTimer.NewTimeout( timerID, newTimeout );
	// Chek for current cancel event and if this is our event, cancel the started timer.
	// otherwise set the stored timer ID for this event to the new one just started.
	ret = pthread_mutex_lock( &fEventMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher::AliHLTEventDone", "Timeout expired" )
		<< GetName() << " publisher error locking event data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    }
	if ( fEvents.FindElement( eventID, evtNdx ) )
	    fEvents.GetPtr( evtNdx )->fFinalTimeout = true;
	}
	
#endif
    }

AliHLTSamplePublisher::EventData* AliHLTSamplePublisher::GetEventIter( AliEventID_t eventID )
    {
    unsigned long ndx;
    if ( fEvents.FindElement( eventID, ndx ) )
	return fEvents.GetPtr( ndx );
    else
	return NULL;
    }

// AliUInt32_t AliHLTSamplePublisher::GetEventIndex( AliEventID_t eventID )
//     {
//     AliUInt32_t ndx = 0;
//     TEventDataListType::iterator evIter, evEnd;
//     evIter = fEvents.begin();
//     evEnd = fEvents.end();
//     while ( evIter != evEnd )
// 	{
// 	if ( evIter->fEventID == eventID )
// 	    {
// 	    return ndx;
// 	    }
// 	evIter++;
// 	ndx++;
// 	}
//     return (AliUInt32_t)-1;
//     }


void AliHLTSamplePublisher::CancelEvent( AliEventID_t id, vector<AliHLTEventDoneData*>& eventDoneData,
					 vector<EventSubscriberData>& transients, bool sendEventCanceled )
    {
    vector<SubscriberData*>::iterator res;
    int ret;
    while ( transients.size()>0 )
	{
	res = FindSubscriber( fSubscribers, transients.begin()->fSubscriber );
#if __GNUC__>=3
	if ( res.base() && sendEventCanceled )
#else
	if ( res && sendEventCanceled )
#endif
	    {
	    ret = (*res)->fSendThread->EventCanceled( *this, id );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Cancel event" )
		    << GetName() << " publisher error sending EventCanceled message for event "
		    << AliHLTLog::kDec << id << " to subscriber " << (*res)->fInterface->GetName()
		    << ". Reason: " << strerror( ret ) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
		Ping( *res );
		}
	    }
	transients.erase( transients.begin() );
	}
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher", "Cancel event" )
	<< GetName() << " publisher canceled event " << AliHLTLog::kDec << id << "." << ENDLOG;
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Cancel event" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}
    if ( fEventAccounting )
	fEventAccounting->EventFinished( GetName(), id );
    CanceledEvent( id, eventDoneData );
    vector<AliHLTEventDoneData*>::iterator iter, end;
    iter = eventDoneData.begin();
    end = eventDoneData.end();
    while ( iter != end )
	{
	if ( (*iter) )
	    {
	    if ( fCachedEDDAllocs )
		fEDDCache.Release( (uint8*)(*iter) );
	    else
		delete [] (uint8*)(*iter);
	    }
	iter++;
	}
    eventDoneData.clear();
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Cancel event" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}
    }


void AliHLTSamplePublisher::RemoveSubscriber( SubscriberData* data, bool forced, bool sendUnsubscribeMsg, bool waitForSubscriberMsgLoopEnd )
    {
    //     LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Remove subscriber" )
    // 	<< "Started. data: 0x" << AliHLTLog::kHex << (unsigned long)data << "." << ENDLOG;
    LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher", "Remove subscriber" )
	<< "Removing subscriber " << data->fInterface->GetName() << (forced ? "forced" : "normally") << "."
	<< ENDLOG;
    vector<SubscriberData*>::iterator iter, end;
    int ret;
    bool persistent=false;
    bool sendEDDSubscriber=false;
    iter = fSubscribers.begin();
    end = fSubscribers.end();
    bool found = false;
    while ( iter != end )
	{
	if ( *iter == data )
	    {
	    found = true;
	    fSubscribers.erase( iter );
	    break;
	    }
	iter++;
	}
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Remove subscriber" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}
    if ( found )
	{
	persistent = data->fPersistent;
	sendEDDSubscriber = data->fSendEventDoneData;
	if ( sendUnsubscribeMsg )
	    {
	    ret = data->fSendThread->SubscriptionCanceled( *this );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Remove subscriber" )
		    << GetName() << " publisher error sending Subscription canceled message to subscriber "
		    << data->fInterface->GetName() << ". " 
		    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		    << ENDLOG;
		}
	    }
	ret = pthread_mutex_lock( &fEventMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Remove subscriber" )
		<< GetName() << " publisher error locking event data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    }
 	LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher", "Remove subscriber" )
 	    << "Before Events Done." << ENDLOG;
	TSendCurrentEventsData sced;
	sced.fEvents.reserve( fEvents.GetCnt() );
	sced.fESD.fSubscriber = data->fInterface;
	fEvents.Iterate( &RemoveSubscriberEventsIterationFunc, &sced );
	vector<AliEventID_t>::iterator evtIter, evtEnd;
	evtIter = sced.fEvents.begin();
	evtEnd = sced.fEvents.end();
	while ( evtIter != evtEnd )
	    {
	    bool sendEDDDummy;
	    vector<EventSubscriberData> remainingSubscribersDummy;
	    EventDone( *evtIter, NULL, data->fInterface, 1, (persistent ? 0 : 1), (sendEDDSubscriber ? 1 : 0), false, sendEDDDummy, remainingSubscribersDummy );
	    evtIter++;
	    }
	// 	LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher", "Remove subscriber" )
	// 	    << "Events done." << ENDLOG;
	ret = pthread_mutex_unlock( &fEventMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Remove subscriber" )
		<< GetName() << " publisher error unlocking event data mutex."
		<< "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		<< ENDLOG;
	    }

	// 	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Remove subscriber" )
	// 	    << "Before proxy message loop finish. data: 0x" << AliHLTLog::kHex << (unsigned long)data << "." << ENDLOG;
	((AliHLTSubscriberProxyInterface*)data->fInterface)->QuitMsgLoop( waitForSubscriberMsgLoopEnd );
	LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher", "Remove subscriber" )
	    << "Proxy message loop finished. data: 0x" << AliHLTLog::kHex << (unsigned long)data
	    << "." << ENDLOG;
	UnsubscribedCallbacks( data->fInterface->GetName(), data->fInterface, forced );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Remove subscriber" )
	    << GetName() << " publisher internal error: Subscriber " 
	    << data->fInterface->GetName() << " could not be found in subscriber list."
	    << ENDLOG;
	}
    pthread_mutex_unlock( &(data->fUsedMutex) );
    //     LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Remove subscriber" )
    // 	<< "Susbcriber removed." << ENDLOG;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Remove subscriber" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}
    }


void AliHLTSamplePublisher::DeleteSubscriber( SubscriberData* data )
    {
    // XXX Might have to be changed;
    if ( !data )
	return;
    MLUCString name = data->fInterface->GetName();
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher", "Subscriber delete" )
	<< GetName() << " publisher deleting subscriber data " 
	<< name.c_str() << "." << ENDLOG;
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher", "Subscriber threads" )
	<< "Subscriber thread: " << (data->fThreadActive ? "active" : "inactive")
	<< " < Subscriber send thread: " << (data->fSendThreadActive ? "active" : "inactive")
	<< "." << ENDLOG;
    AliHLTSubscriberThread* thr = data->fThread;
    AliHLTSubscriberSendThread* annThr = data->fSendThread;
    delete data->fInterface;
    if ( data->fTriggers )
	{
	for ( unsigned long nn=0; nn<data->fTriggerCnt; nn++ )
	    {
	    delete [] reinterpret_cast<uint8*>( data->fTriggers[nn] );
	    }
	delete [] data->fTriggers;
	}
    pthread_mutex_unlock( &(data->fUsedMutex) );
    pthread_mutex_destroy( &(data->fUsedMutex) );
    delete data;
    thr->Abort();
    //thr->Join();
    delete thr;
    annThr->Abort();
    //annThr->Join();
    delete annThr;
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher", "Subscriber delete" )
	<< GetName() << " publisher deleted subscriber data " 
	<< name.c_str() << "." << ENDLOG;
    }


int AliHLTSamplePublisher::AnnounceEvent( SubscriberData* data, AliEventID_t eventID, 
					  const AliHLTSubEventDataDescriptor& sbevent,
					  const AliHLTEventTriggerStruct& ets )
    {
    if ( !data )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSamplePublisher::AnnounceEvent", "NULL pointer" )
	    << "data is NULL pointer.... (event: 0x" << AliHLTLog::kHex << eventID << " ("
	    << AliHLTLog::kDec << eventID << ")" << ENDLOG;
	}
    return data->fSendThread->NewEvent( *this, eventID, sbevent, ets );
    //return 0;
    }


void AliHLTSamplePublisher::Ping( AliHLTSamplePublisher::SubscriberData* data )
    {
    if ( !data )
	return;
//     if ( data->fPingCount >= fMaxPingCount )
// 	{
// 	LOG( AliHLTLog::kInformational, "AliHLTSamplePublisher", "Maximum Ping Count" )
// 	<< GetName() << " publisher not sending Ping message to subscriber "
// 	<< data->fInterface->GetName() << ", maximum ping timeout count of " 
// 	<< AliHLTLog::kDec << fMaxPingCount << " reached." << ENDLOG;
// 	return;
// 	}
    int ret;
    data->fPingCount++;
    ret = data->fSendThread->Ping( *this );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Ping to subscriber" )
	    << GetName() << " publisher error sending Ping message to subscriber "
	    << data->fInterface->GetName() << ". Reason: " << strerror(ret)
	    << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	}
    fTimer.AddTimeout( fPingTimeout, &fPingThread.fSem, reinterpret_cast<AliUInt64_t>(data) );    
    LOG( AliHLTLog::kDebug, "AliHLTSamplePublisher", "Ping to subscriber" )
	<< GetName() << " publisher sent Ping message to subscriber "
	<< data->fInterface->GetName() << "." << ENDLOG;
    }


void AliHLTSamplePublisher::CanceledEvent( AliEventID_t, vector<AliHLTEventDoneData*>& )
    {
    } 

void AliHLTSamplePublisher::AnnouncedEvent( AliEventID_t, const AliHLTSubEventDataDescriptor&, 
					   const AliHLTEventTriggerStruct&, AliUInt32_t )
    {
    }

void AliHLTSamplePublisher::EventDoneDataReceived( const char*, AliEventID_t,
						   const AliHLTEventDoneData*, bool, bool )
    {
    }

bool AliHLTSamplePublisher::GetAnnouncedEventData( AliEventID_t, AliHLTSubEventDataDescriptor*& sedd,
					     AliHLTEventTriggerStruct*& trigger )
    {
    sedd = NULL;
    trigger =NULL;
    return false;
    }


void AliHLTSamplePublisher::EventAnnounceError( SubscriberData* subscriberData, AliEventID_t eventID )
    {
    bool persistent;
    bool sendEDDSubscriber;
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::EventAnnounceError", "Announce Event" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}
    persistent = subscriberData->fPersistent;
    sendEDDSubscriber = subscriberData->fSendEventDoneData;
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::EventAnnounceError", "Announce Event" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}
    ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::EventAnnounceError", "Announce Event" )
	    << GetName() << " publisher error locking event data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}
    bool sendEDDDummy;
    vector<EventSubscriberData> remainingSubscribersDummy;
    EventDone( eventID, NULL, subscriberData->fInterface, 1, (persistent ? 0 : 1), (sendEDDSubscriber ? 1 : 0), false, sendEDDDummy, remainingSubscribersDummy );
    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::EventAnnounceError", "Announce Event" )
	    << GetName() << " publisher error unlocking event data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}
    Ping( subscriberData );
    }

void AliHLTSamplePublisher::SubscriberCommunicationError( SubscriberData* subscriberData )
    {
    Ping( subscriberData );
    }

void AliHLTSamplePublisher::PingTimeout( SubscriberData* )
    {
    }

void AliHLTSamplePublisher::MaxPingTimeout( SubscriberData* subscriberData )
    {
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::MaxPingTimeout", "Remove subscriber" )
	    << GetName() << " publisher error locking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}

    RemoveSubscriber( subscriberData, true, false, true );

    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::MaxPingTimeout", "Remove subscriber" )
	    << GetName() << " publisher error unlocking subscriber data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	}
    }


void AliHLTSamplePublisher::CleanupOldEvents()
    {
    if ( !fOldestEventBirth_s )
	return;
    int ret;
    ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::CleanupOldEvents", "Event Mutex Error" )
	    << GetName() << " publisher error locking event data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return;
	}

    TCleanupOldEventsData coed;
    coed.fEvents.reserve( fEvents.GetCnt() );
    coed.fOldestEventBirth_s = fOldestEventBirth_s;
    fEvents.Iterate( &CleanupOldEventsIterationFunc, &coed );

    unsigned long ndx;
    struct timeval now;
    gettimeofday( &now, NULL );
    EventData* evtIter = NULL;
    //vector<AliEventID_t>::iterator iter, end;
    vector<TCleanupOldEventsData::TCleanupEventData>::iterator iter, end;
    iter = coed.fEvents.begin();
    end = coed.fEvents.end();
    while ( iter != end )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSamplePublisher::CleanupOldEvents", "Cleaning up event" )
	    << "Cleaning up too old event 0x" << AliHLTLog::kHex << iter->fEventID
	    << " (" << AliHLTLog::kDec << iter->fEventID << ") (Birth: "
	    << iter->fEventBirth_s << " s " << iter->fEventBirth_us << " usec - oldest event birth: "
	    << fOldestEventBirth_s << " s - now: " << now.tv_sec << " s " << now.tv_usec << " usec)!" << ENDLOG;
	if ( fEvents.FindElement( iter->fEventID, ndx ) )
	    {
	    evtIter = fEvents.GetPtr(ndx);
	    bool sendEDDDummy;
	    vector<EventSubscriberData> remainingSubscribersDummy;
	    EventDone( evtIter->fEventID, NULL, NULL, evtIter->fRefCount, evtIter->fTransientCount, 0, true, sendEDDDummy, remainingSubscribersDummy );
	    }
	iter++;
	}

    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher::CleanupOldEvents", "Event Mutex Error" )
	    << GetName() << " publisher error locking event data mutex."
	    << "Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
	    << ENDLOG;
	return;
	}
    }

void AliHLTSamplePublisher::CleanupOldEvents( AliUInt32_t oldestEvent_s )
    {
    if ( (AliUInt32_t)oldestEvent_s > fOldestEventBirth_s )
	{
	fOldestEventBirth_s = (AliUInt32_t)oldestEvent_s;
	CleanupOldEvents();
	}
    }




int AliHLTSamplePublisher::CompareSubscriberData( const void* key, const void* arrayMember )
    {
    if ( key < (*(SubscriberData**)arrayMember)->fInterface )
	return -1;
    if ( key > (*(SubscriberData**)arrayMember)->fInterface )
	return 1;
    return 0;
    }

vector<AliHLTSamplePublisher::SubscriberData*>::const_iterator AliHLTSamplePublisher::FindSubscriber( vector<AliHLTSamplePublisher::SubscriberData*> const& subscribers, AliHLTSubscriberInterface* interface )
    {
    vector<SubscriberData*>::const_iterator iter, end;
    iter = subscribers.begin();
    end = subscribers.end();
#ifndef USE_STL_ITERATOR_SEARCH
    vector<SubscriberData*>::iterator res=NULL;
    // Use a binary search that works only for STL vectors
    // but should be faster.
    if ( end>iter+1 )
	{
	res = (vector<AliHLTSamplePublisher::SubscriberData*>::const_iterator)bsearch( &interface, iter, sizeof(SubscriberData*),
										end-iter, AliHLTSamplePublisher::CompareSubscriberData );
	return res;
	}
    if ( iter+1==end )
	{
	if ( (*iter)->fInterface == interface )
	    return iter;
	}
#else
    // Use a linear search using STL iterators that should
    // work with every STL container class.
    while ( iter != end )
	{
	if ( (*iter)->fInterface == interface )
	    {
	    return iter;
	    }
	iter++;
	}
#endif
    return vector<SubscriberData*>::const_iterator(); // NULL??
    }

vector<AliHLTSamplePublisher::SubscriberData*>::iterator AliHLTSamplePublisher::FindSubscriber( vector<AliHLTSamplePublisher::SubscriberData*>& subscribers, AliHLTSubscriberInterface* interface )
    {
    vector<SubscriberData*>::iterator iter, end;
    iter = subscribers.begin();
    end = subscribers.end();
#ifndef USE_STL_ITERATOR_SEARCH
    vector<SubscriberData*>::iterator res=NULL;
    // Use a binary search that works only for STL vectors
    // but should be faster.
    if ( end>iter+1 )
	{
	res = (vector<AliHLTSamplePublisher::SubscriberData*>::iterator)bsearch( &interface, iter, sizeof(SubscriberData*),
										end-iter, AliHLTSamplePublisher::CompareSubscriberData );
	return res;
	}
    if ( iter+1==end )
	{
	if ( (*iter)->fInterface == interface )
	    return iter;
	}
#else
    // Use a linear search using STL iterators that should
    // work with every STL container class.
    while ( iter != end )
	{
	if ( (*iter)->fInterface == interface )
	    {
	    return iter;
	    }
	iter++;
	}
#endif
    return vector<SubscriberData*>::iterator(); // NULL??
    }



int AliHLTSamplePublisher::CompareEventSubscriberData( const void* key, const void* arrayMember )
    {
    if ( key < ((EventSubscriberData*)arrayMember)->fSubscriber )
	return -1;
    if ( key > ((EventSubscriberData*)arrayMember)->fSubscriber )
	return 1;
    return 0;
    }


vector<AliHLTSamplePublisher::EventSubscriberData>::const_iterator AliHLTSamplePublisher::FindSubscriber( vector<AliHLTSamplePublisher::EventSubscriberData> const& subscribers, AliHLTSubscriberInterface* interface )
    {
    vector<EventSubscriberData>::const_iterator iter, end;
    iter = subscribers.begin();
    end = subscribers.end();
#ifndef USE_STL_ITERATOR_SEARCH
    vector<EventSubscriberData>::const_iterator res=NULL;
    // Use a binary search that works only for STL vectors
    // but should be faster.
    if ( end>iter+1 )
	{
	res = (vector<AliHLTSamplePublisher::EventSubscriberData>::const_iterator)bsearch( &interface, iter, sizeof(EventSubscriberData),
										    end-iter, AliHLTSamplePublisher::CompareEventSubscriberData );
	return res;
	}
    if ( iter==end+1 )
	{
	if ( (iter)->fSubscriber == interface )
	    return iter;
	}
#else
    // Use a linear search using STL iterators that should
    // work with every STL container class.
    while ( iter != end )
	{
	if ( (iter)->fSubscriber == interface )
	    {
	    return iter;
	    }
	iter++;
	}
#endif
    return vector<EventSubscriberData>::const_iterator(); // NULL?
    }

vector<AliHLTSamplePublisher::EventSubscriberData>::iterator AliHLTSamplePublisher::FindSubscriber( vector<AliHLTSamplePublisher::EventSubscriberData>& subscribers, AliHLTSubscriberInterface* interface )
    {
    vector<EventSubscriberData>::iterator iter, end;
    iter = subscribers.begin();
    end = subscribers.end();
#ifndef USE_STL_ITERATOR_SEARCH
    vector<EventSubscriberData>::iterator res=NULL;
    // Use a binary search that works only for STL vectors
    // but should be faster.
    if ( end>iter+1 )
	{
	res = (vector<AliHLTSamplePublisher::EventSubscriberData>::iterator)bsearch( &interface, iter, sizeof(EventSubscriberData),
										    end-iter, AliHLTSamplePublisher::CompareEventSubscriberData );
	return res;
	}
    if ( iter==end+1 )
	{
	if ( (iter)->fSubscriber == interface )
	    return iter;
	}
#else
    // Use a linear search using STL iterators that should
    // work with every STL container class.
    while ( iter != end )
	{
	if ( (iter)->fSubscriber == interface )
	    {
	    return iter;
	    }
	iter++;
	}
#endif
    return vector<EventSubscriberData>::iterator(); // NULL?
    }


void AliHLTSamplePublisher::SubscribedCallbacks( const char* name, AliHLTSubscriberInterface* sub )
    {
    vector<SubscriberEventCallback*>::iterator iter, end;
    vector<SubscriberEventCallback*> cbs;
    pthread_mutex_lock( &fSubscriberEventCallbackMutex );
    cbs = fSubscriberEventCallbacks;
    pthread_mutex_unlock( &fSubscriberEventCallbackMutex );
    iter = cbs.begin();
    end = cbs.end();
    while ( iter!=end )
	{
	if ( *iter )
	    (*iter)->Subscribed( this, name, sub );
	iter++;
	}
    }

void AliHLTSamplePublisher::UnsubscribedCallbacks( const char* name, AliHLTSubscriberInterface* sub, bool forced )
    {
    vector<SubscriberEventCallback*>::iterator iter, end;
    vector<SubscriberEventCallback*> cbs;
    pthread_mutex_lock( &fSubscriberEventCallbackMutex );
    cbs = fSubscriberEventCallbacks;
    pthread_mutex_unlock( &fSubscriberEventCallbackMutex );
    iter = cbs.begin();
    end = cbs.end();
    while ( iter!=end )
	{
	if ( *iter )
	    (*iter)->Unsubscribed( this, name, sub, forced );
	iter++;
	}
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
