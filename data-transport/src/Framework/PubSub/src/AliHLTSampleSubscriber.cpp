/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSampleSubscriber.hpp"
#include "AliHLTPublisherInterface.hpp"
#include "AliHLTEventDoneData.hpp"
#include "AliHLTLog.hpp"
#include <string.h>
#include <errno.h>

AliHLTSampleSubscriber::AliHLTSampleSubscriber( const char* name ):
    AliHLTSubscriberInterface( name )
    {
    fPingCount = 0;
    }

AliHLTSampleSubscriber::~AliHLTSampleSubscriber()
    {
    }

int AliHLTSampleSubscriber::NewEvent(  AliHLTPublisherInterface& publisher, 
				       AliEventID_t eventID, 
				       const AliHLTSubEventDataDescriptor&,
				       const AliHLTEventTriggerStruct& )
    {
    LOG( AliHLTLog::kInformational, "AliHLTSampleSubscriber", "New event" )
	<< "Subscriber " << GetName() << " received notification for event "
	<< AliHLTLog::kDec << eventID << "." << ENDLOG;
    int ret;
    AliHLTEventDoneData edd;
    AliHLTMakeEventDoneData( &edd, eventID );
    ret = publisher.EventDone( *this, edd );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSampleSubscriber", "New event" )
	    << "Subscriber " << GetName() << " error sending EventDone message to publisher "
	    << publisher.GetName() << ".Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	return EIO;
	}
    return 0;
    }

int AliHLTSampleSubscriber::EventCanceled(  AliHLTPublisherInterface&,
					   AliEventID_t eventID )
    {
    LOG( AliHLTLog::kInformational, "AliHLTSampleSubscriber", "Event canceled" )
	<< "Subscriber " << GetName() << " received cancellation for event "
	<< AliHLTLog::kDec << eventID << "." << ENDLOG;
    return 0;
    }

int AliHLTSampleSubscriber::EventDoneData( AliHLTPublisherInterface&, 
					   const AliHLTEventDoneData& eventDoneData )
    {
    LOG( AliHLTLog::kInformational, "AliHLTSampleSubscriber", "EventDoneData" )
	<< "Subscriber " << GetName() << " received event done data for event "
	<< AliHLTLog::kDec << eventDoneData.fEventID << "." << ENDLOG;
    return 0;
    }

int AliHLTSampleSubscriber::SubscriptionCanceled( AliHLTPublisherInterface& )
    {
     LOG( AliHLTLog::kInformational, "AliHLTSampleSubscriber", "Subscription canceled" )
	<< "Subscriber " << GetName() << " received cancellation for subscription."
	<< ENDLOG;
     return 0;
    }

int AliHLTSampleSubscriber::ReleaseEventsRequest( AliHLTPublisherInterface& )
    {
     LOG( AliHLTLog::kInformational, "AliHLTSampleSubscriber", "Release events request" )
	<< "Subscriber " << GetName() << " received events release request."
	<< ENDLOG;
     return 0;
    }

int AliHLTSampleSubscriber::Ping( AliHLTPublisherInterface& publisher )
    {
    LOG( AliHLTLog::kInformational, "AliHLTSampleSubscriber", "Subscriber pinged" )
	<< "Subscriber " << GetName() << " received ping message"
	<< ENDLOG;
    int ret;
    ret = publisher.PingAck( *this );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSampleSubscriber", "Subscriber pinged" )
	    << "Subscriber " << GetName() << " error sending ping ack message to publisher "
	    << publisher.GetName() << ". Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	return EIO;
	}
    return 0;
    }

int AliHLTSampleSubscriber::PingAck( AliHLTPublisherInterface& )
    {
    fPingCount--;
    LOG( AliHLTLog::kInformational, "AliHLTSampleSubscriber", "Subscriber ping ack'ed" )
	<< "Subscriber " << GetName() << " received ping ack message. Ping count: "
	<< AliHLTLog::kDec << fPingCount << "." << ENDLOG;
    return 0;
    }

void AliHLTSampleSubscriber::PingPublisher( AliHLTPublisherInterface& pub )
    {
    fPingCount++;
    int ret;
    ret = pub.Ping( *this );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Pinging publisher" )
	    << "Subscriber " << GetName() << " error sending ping message to publisher "
	    << pub.GetName() << ". Ping count: " << AliHLTLog::kDec << fPingCount << "." << ENDLOG;
	}
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
