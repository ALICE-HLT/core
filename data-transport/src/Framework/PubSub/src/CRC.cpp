#include "CRC.hpp" 
/* b is the crc and a is the value to be "added" to it */
#define CRC32DEF(a,b)  { (b) = Crc32Table[(((b) >> 24) ^ (a)) & 255] ^ ((b) << 8); (b) = (b) & 0xFFFFFFFF ;}

void InitCrc32( long* crc32Table ) 
{
  int i, j;  long crc;
  for(i=0; i <256; i++) {
    crc = (i << 24); /* Put i into MSB */
    for(j=0; j < 8; j++) /* 8 reductions */
      crc = (crc << 1) ^ ((crc & 0x80000000L) ? 0x04c11db7L : 0);
    crc32Table[i] = crc;
  }
  //CrcInitFlag = 1 ;
};

long Crc32( int ch, long crc, long* crc32Table ) 
    {
    crc = crc32Table[((crc >> 24) ^ ch) & 255] ^ (crc << 8);
    return crc & 0xFFFFFFFF;
    };

long MakeCRC( long crc, AliHLTBlockHeader* header, long* crc32Table )
    {
    AliUInt8_t* bp = (AliUInt8_t*)header;
    AliUInt32_t i, size = header->fLength;
    for ( i = 0; i < size; i++, bp++ )
	crc = Crc32( *bp, crc, crc32Table );
    return crc;
    }
