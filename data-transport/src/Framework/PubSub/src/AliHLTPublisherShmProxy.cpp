/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTPublisherShmProxy.hpp"
#include "AliHLTSubscriberInterface.hpp"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTEventTriggerStruct.h"
#include "AliHLTLog.hpp"
#include "AliHLTPipeCom.hpp"
#include <errno.h>



AliHLTPublisherShmProxy::AliHLTPublisherShmProxy( const char* name, AliUInt32_t shmSize, key_t pub2SubKey, key_t sub2PubKey ):
    AliHLTPublisherProxyInterface( name ),
    fPubToSub( pub2SubKey, shmSize, true ), fSubToPub( sub2PubKey, shmSize, true ), 
    fPubToSubKey( pub2SubKey ), fSubToPubKey( sub2PubKey ), fShmSize( shmSize )
    {
    fQuitLoop = false;
    fLoopQuitted = true;
    pthread_mutex_init( &fMutex, NULL );
    }


AliHLTPublisherShmProxy::~AliHLTPublisherShmProxy()
    {
    pthread_mutex_destroy( &fMutex );
    }


int AliHLTPublisherShmProxy::Subscribe( AliHLTSubscriberInterface& interface )
    {
    int ret;
    pthread_mutex_lock( &fMutex );
    MLUCString pname;
    AliHLTPipeCom publCom;
    pname = ALIL3PIPEBASEDIR;
    pname += GetName();
    pname += "SubsService";
    ret = publCom.OpenWrite( pname.c_str() );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy::Subscribe", "Publisher subscription pipe open")
	    << "Unable to open pipe " << pname.c_str()
	    << " - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return EPIPE;
	}

    MLUCString fullname;
    fullname = "shm:";
    fullname += interface.GetName();
    AliUInt8_t msgData[ sizeof(AliHLTPubSubShmMsg)+strlen(fullname.c_str())+1+2*sizeof(key_t)+sizeof(AliUInt32_t) ];

    AliHLTPubSubShmMsg* msg = (AliHLTPubSubShmMsg*)msgData;
    MakeMsg( msg, fullname.c_str(), AliHLTPubSubShmMsg::kSubscribe, 0, fPubToSubKey, fSubToPubKey, fShmSize );

    ret = publCom.Write( &(msg->fHeader) );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy::Subscribe", "Subscribe message send")
	    << "Error writing subscriber message of size " << msg->fHeader.fLength
	    << " to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    publCom.Close();
    LOG( AliHLTLog::kInformational, "AliHLTPublisherShmProxy", "Subscribe message")
	<< "Subscribe successfuly sent to publisher " << GetName() << ENDLOG;
    pthread_mutex_unlock( &fMutex );
    return 0;
    }


int AliHLTPublisherShmProxy::Unsubscribe( AliHLTSubscriberInterface& interface )
    {
    if ( !fPubToSub )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "Unsubscribe")
	    << "Unsubscription attempt without open pub-to-sub shared memory." << ENDLOG;
	return EIO;
	}
    pthread_mutex_lock( &fMutex );
    AliHLTPubSubShmMsg* msg;
    

    AliUInt32_t size, size1;
    AliUInt8_t *p1, *p2;
    int ret;

    size = sizeof(AliHLTPubSubShmMsg)+strlen(interface.GetName())+1;
    ret = fPubToSub.CanWrite( size, p1, p2, size1 );
    if ( ret == 0 )
	{
	if ( !p2 )
	    {
	    msg = (AliHLTPubSubShmMsg*)p1;
	    MakeMsg( msg, interface.GetName(), AliHLTPubSubShmMsg::kUnsubscribe, 0, fPubToSubKey, fSubToPubKey, fShmSize );
	    }
	else
	    {
	    AliUInt8_t msgData[ size ];
	    msg = (AliHLTPubSubShmMsg*)msgData;
	    MakeMsg( msg, interface.GetName(), AliHLTPubSubShmMsg::kUnsubscribe, 0, fPubToSubKey, fSubToPubKey, fShmSize );
	    memcpy( p1, msgData, size1 );
	    memcpy( p2, msgData+size1, size-size1 );
	    }
	fPubToSub.HaveWritten( size, p1, p2, size1 );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "Unsubscribe message send")
	    << "Unable to write unsubscribe message ("
	     << AliHLTLog::kDec << size
	    << " Bytes) to subscriber (Pub-To-Sub shm key 0x" << AliHLTLog::kHex << (int)fPubToSubKey
	    << " (" << AliHLTLog::kDec << (int)fPubToSubKey << ")): " 
	    << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    pthread_mutex_unlock( &fMutex );
    return ret;
    }

int AliHLTPublisherShmProxy::SetPersistent( AliHLTSubscriberInterface&, bool pers )
    {
    if ( !fPubToSub )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "SetPersistent")
	    << "SetPersistent attempt without open pub-to-sub shared memory." << ENDLOG;
	return EIO;
	}
    pthread_mutex_lock( &fMutex );
    AliHLTPubSubShmMsg* msg;
    AliHLTPubSubShmMsgParam* param;

    AliUInt32_t size, size1;
    AliUInt8_t *p1, *p2;
    int ret;

    size = sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam);
    ret = fPubToSub.CanWrite( size, p1, p2, size1 );
    if ( ret == 0 )
	{
	if ( !p2 )
	    {
	    msg = (AliHLTPubSubShmMsg*)p1;
	    param = (AliHLTPubSubShmMsgParam*)( p1+sizeof(AliHLTPubSubShmMsg)+1 );
	    MakeMsg( msg, AliHLTPubSubShmMsg::kSetPersistent, 1 );
	    MakeParam( param, pers );
	    }
	else
	    {
	    AliUInt8_t msgData[ size ];
	    msg = (AliHLTPubSubShmMsg*)msgData;
	    param = (AliHLTPubSubShmMsgParam*)( msgData+sizeof(AliHLTPubSubShmMsg)+1 );
	    MakeMsg( msg, AliHLTPubSubShmMsg::kSetPersistent, 1 );
	    MakeParam( param, pers );
	    memcpy( p1, msgData, size1 );
	    memcpy( p2, msgData+size1, size-size1 );
	    }
	fPubToSub.HaveWritten( size, p1, p2, size1 );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "SetPersistent message send")
	    << "Unable to write setpersistent message ("
	     << AliHLTLog::kDec << size
	    << " Bytes) to subscriber (Pub-To-Sub shm key 0x" << AliHLTLog::kHex << (int)fPubToSubKey
	    << " (" << AliHLTLog::kDec << (int)fPubToSubKey << ")): " 
	    << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    pthread_mutex_unlock( &fMutex );
    return ret;
    }


int AliHLTPublisherShmProxy::SetEventType( AliHLTSubscriberInterface&,  
					  AliUInt32_t modulo )
    {
    if ( !fPubToSub )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "SetEventTypeMod")
	    << "SetEventTypeMod attempt without open pub-to-sub shared memory." << ENDLOG;
	return EIO;
	}
    pthread_mutex_lock( &fMutex );
    AliHLTPubSubShmMsg* msg;
    AliHLTPubSubShmMsgParam* param;

    AliUInt32_t size, size1;
    AliUInt8_t *p1, *p2;
    int ret;

    size = sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam);

    ret = fPubToSub.CanWrite( size, p1, p2, size1 );
    if ( ret == 0 )
	{
	if ( !p2 )
	    {
	    msg = (AliHLTPubSubShmMsg*)p1;
	    param = (AliHLTPubSubShmMsgParam*)( p1+sizeof(AliHLTPubSubShmMsg)+1 );
	    MakeMsg( msg, AliHLTPubSubShmMsg::kSetEventTypeMod, 1 );
	    MakeParam( param, modulo );
	    }
	else
	    {
	    AliUInt8_t msgData[ size ];
	    msg = (AliHLTPubSubShmMsg*)msgData;
	    param = (AliHLTPubSubShmMsgParam*)( msgData+sizeof(AliHLTPubSubShmMsg)+1 );

	    MakeMsg( msg, AliHLTPubSubShmMsg::kSetEventTypeMod, 1 );
	    MakeParam( param, modulo );

	    memcpy( p1, msgData, size1 );
	    memcpy( p2, msgData+size1, size-size1 );
	    }
	fPubToSub.HaveWritten( size, p1, p2, size1 );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "SetEventTypeMod message send")
	    << "Unable to write SetEventTypeMod message ("
	     << AliHLTLog::kDec << size
	    << " Bytes) to subscriber (Pub-To-Sub shm key 0x" << AliHLTLog::kHex << (int)fPubToSubKey
	    << " (" << AliHLTLog::kDec << (int)fPubToSubKey << ")): " 
	    << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    pthread_mutex_unlock( &fMutex );
    return ret;
    }


int AliHLTPublisherShmProxy::SetEventType( AliHLTSubscriberInterface&, 
					  vector<AliHLTEventTriggerStruct*>& triggerWords,  
					  AliUInt32_t modulo )
    {
    if ( !fPubToSub )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "SetEventTypeMod")
	    << "SetEventTypeMod attempt without open pub-to-sub shared memory." << ENDLOG;
	return EIO;
	}
    pthread_mutex_lock( &fMutex );
    AliHLTPubSubShmMsg* msg;
    AliHLTPubSubShmMsgParam *param1, *param2;
    AliHLTEventTriggerStruct* ettP;
    AliUInt8_t* ettPStart;

    AliUInt32_t size, size1, trSize;
    AliUInt8_t *p1, *p2;
    int ret;

    trSize = 0;
    int i, n = triggerWords.size();
    for ( i = 0; i < n; i++ )
	trSize += triggerWords[i]->fHeader.fLength;

    size = sizeof(AliHLTPubSubShmMsg)+1+2*sizeof(AliHLTPubSubShmMsgParam)+trSize;

    ret = fPubToSub.CanWrite( size, p1, p2, size1 );
    if ( ret == 0 )
	{
	if ( !p2 )
	    {
	    msg = (AliHLTPubSubShmMsg*)p1;
	    param1 = (AliHLTPubSubShmMsgParam*)( p1+sizeof(AliHLTPubSubShmMsg)+1 );
	    ettPStart = p1+sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam);
	    param2 = (AliHLTPubSubShmMsgParam*)( p1+sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam)+trSize );
	    MakeMsg( msg, AliHLTPubSubShmMsg::kSetEventTypeETT, 2 );
	    MakeParam( param1, triggerWords );
	    for ( i = 0; i < n; i++ )
		{
		ettP = (AliHLTEventTriggerStruct*)ettPStart;
		memcpy( ettP, &(triggerWords[i]->fHeader), triggerWords[i]->fHeader.fLength );
		ettPStart += triggerWords[i]->fHeader.fLength;
		}
	    MakeParam( param2, modulo );
	    }
	else
	    {
	    AliUInt8_t msgData[ size ];
	    msg = (AliHLTPubSubShmMsg*)msgData;
	    param1 = (AliHLTPubSubShmMsgParam*)( msgData+sizeof(AliHLTPubSubShmMsg)+1 );
	    ettPStart = ( msgData+sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam) );
	    param2 = (AliHLTPubSubShmMsgParam*)( msgData+sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam)+trSize );

	    MakeMsg( msg, AliHLTPubSubShmMsg::kSetEventTypeETT, 2 );
	    MakeParam( param1, triggerWords );
	    for ( i = 0; i < n; i++ )
		{
		ettP = (AliHLTEventTriggerStruct*)ettPStart;
		memcpy( ettP, &(triggerWords[i]->fHeader), triggerWords[i]->fHeader.fLength );
		ettPStart += triggerWords[i]->fHeader.fLength;
		}
	    MakeParam( param2, modulo );
	    
	    memcpy( p1, msgData, size1 );
	    memcpy( p2, msgData+size1, size-size1 );
	    }
	fPubToSub.HaveWritten( size, p1, p2, size1 );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "SetEventTypeETT message send")
	    << "Unable to write SetEventTypeETT message ("
	     << AliHLTLog::kDec << size
	    << " Bytes) to subscriber (Pub-To-Sub shm key 0x" << AliHLTLog::kHex << (int)fPubToSubKey
	    << " (" << AliHLTLog::kDec << (int)fPubToSubKey << ")): " 
	    << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}

    pthread_mutex_unlock( &fMutex );
    return ret;
    }

int AliHLTPublisherShmProxy::SetTransientTimeout( AliHLTSubscriberInterface&, 
						 AliUInt32_t timeout_ms )
    {
    if ( !fPubToSub )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "SetTransientTimeout")
	    << "SetTransientTimeout attempt without open pub-to-sub shared memory." << ENDLOG;
	return EIO;
	}
    pthread_mutex_lock( &fMutex );
    AliHLTPubSubShmMsg* msg;
    AliHLTPubSubShmMsgParam* param;

    AliUInt32_t size, size1;
    AliUInt8_t *p1, *p2;
    int ret;

    size = sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam);

    ret = fPubToSub.CanWrite( size, p1, p2, size1 );
    if ( ret == 0 )
	{
	if ( !p2 )
	    {
	    msg = (AliHLTPubSubShmMsg*)p1;
	    param = (AliHLTPubSubShmMsgParam*)( p1+sizeof(AliHLTPubSubShmMsg)+1 );
	    MakeMsg( msg, AliHLTPubSubShmMsg::kSetTransientTimeout, 1 );
	    MakeParam( param, timeout_ms );
	    }
	else
	    {
	    AliUInt8_t msgData[ size ];
	    msg = (AliHLTPubSubShmMsg*)msgData;
	    param = (AliHLTPubSubShmMsgParam*)( msgData+sizeof(AliHLTPubSubShmMsg)+1 );

	    MakeMsg( msg, AliHLTPubSubShmMsg::kSetTransientTimeout, 1 );
	    MakeParam( param, timeout_ms );
		
	    memcpy( p1, msgData, size1 );
	    memcpy( p2, msgData+size1, size-size1 );
	    }
	fPubToSub.HaveWritten( size, p1, p2, size1 );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "SetTransientTimeout message send")
	    << "Unable to write SetTransientTimeout message ("
	     << AliHLTLog::kDec << size
	    << " Bytes) to subscriber (Pub-To-Sub shm key 0x" << AliHLTLog::kHex << (int)fPubToSubKey
	    << " (" << AliHLTLog::kDec << (int)fPubToSubKey << ")): " 
	    << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    pthread_mutex_unlock( &fMutex );
    return ret;
    }

int AliHLTPublisherShmProxy::SetEventDoneDataSend( AliHLTSubscriberInterface&, bool pers )
    {
    if ( !fPubToSub )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "SetEventDoneDataSend")
	    << "SetEventDoneDataSend attempt without open pub-to-sub shared memory." << ENDLOG;
	return EIO;
	}
    pthread_mutex_lock( &fMutex );
    AliHLTPubSubShmMsg* msg;
    AliHLTPubSubShmMsgParam* param;

    AliUInt32_t size, size1;
    AliUInt8_t *p1, *p2;
    int ret;

    size = sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam);
    ret = fPubToSub.CanWrite( size, p1, p2, size1 );
    if ( ret == 0 )
	{
	if ( !p2 )
	    {
	    msg = (AliHLTPubSubShmMsg*)p1;
	    param = (AliHLTPubSubShmMsgParam*)( p1+sizeof(AliHLTPubSubShmMsg)+1 );
	    MakeMsg( msg, AliHLTPubSubShmMsg::kSetEventDoneDataSend, 1 );
	    MakeParam( param, pers );
	    }
	else
	    {
	    AliUInt8_t msgData[ size ];
	    msg = (AliHLTPubSubShmMsg*)msgData;
	    param = (AliHLTPubSubShmMsgParam*)( msgData+sizeof(AliHLTPubSubShmMsg)+1 );
	    MakeMsg( msg, AliHLTPubSubShmMsg::kSetEventDoneDataSend, 1 );
	    MakeParam( param, pers );
	    memcpy( p1, msgData, size1 );
	    memcpy( p2, msgData+size1, size-size1 );
	    }
	fPubToSub.HaveWritten( size, p1, p2, size1 );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "SetEventDoneDataSend message send")
	    << "Unable to write seteventdonedatasend message ("
	     << AliHLTLog::kDec << size
	    << " Bytes) to subscriber (Pub-To-Sub shm key 0x" << AliHLTLog::kHex << (int)fPubToSubKey
	    << " (" << AliHLTLog::kDec << (int)fPubToSubKey << ")): " 
	    << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    pthread_mutex_unlock( &fMutex );
    return ret;
    }

int AliHLTPublisherShmProxy::EventDone( AliHLTSubscriberInterface&, 
					const AliHLTEventDoneData& eventDoneData, bool forceForward, bool forwarded )
    {
    if ( !fPubToSub )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "EventDoneSing")
	    << "EventDoneSing attempt without open pub-to-sub shared memory." << ENDLOG;
	return EIO;
	}
    pthread_mutex_lock( &fMutex );
    AliHLTPubSubShmMsg* msg;
    AliHLTPubSubShmMsgParam* param;
    AliHLTPubSubShmMsgParam* paramForceFlag;
    AliHLTPubSubShmMsgParam* paramForwardFlag;
    AliHLTEventDoneData* edd;

    AliUInt32_t size, size1;
    AliUInt8_t *p1, *p2;
    int ret;

    size = sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam)*3+eventDoneData.fHeader.fLength;

    ret = fPubToSub.CanWrite( size, p1, p2, size1 );
    if ( ret == 0 )
	{
	if ( !p2 )
	    {
	    msg = (AliHLTPubSubShmMsg*)p1;
	    param = (AliHLTPubSubShmMsgParam*)( p1+sizeof(AliHLTPubSubShmMsg)+1 );
	    paramForceFlag = (AliHLTPubSubShmMsgParam*)( p1+sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam) );
	    paramForwardFlag = (AliHLTPubSubShmMsgParam*)( p1+sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam)*2 );
	    edd = (AliHLTEventDoneData*)(p1+sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam)*3);
	    MakeMsg( msg, AliHLTPubSubShmMsg::kEventDoneSing, 3 );
	    MakeParam( param, eventDoneData );
	    MakeParam( paramForceFlag, forceForward );
	    MakeParam( paramForwardFlag, forwarded );
	    memcpy( edd, &eventDoneData, eventDoneData.fHeader.fLength );
	    }
	else
	    {
	    AliUInt8_t msgData[ size ];
	    msg = (AliHLTPubSubShmMsg*)msgData;
	    param = (AliHLTPubSubShmMsgParam*)( msgData+sizeof(AliHLTPubSubShmMsg)+1 );
	    paramForceFlag = (AliHLTPubSubShmMsgParam*)( msgData+sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam) );
	    paramForwardFlag = (AliHLTPubSubShmMsgParam*)( msgData+sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam)*2 );
	    edd = (AliHLTEventDoneData*)( msgData+sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam)*3 );

	    MakeMsg( msg, AliHLTPubSubShmMsg::kEventDoneSing, 3 );
	    MakeParam( param, eventDoneData );
	    MakeParam( paramForceFlag, forceForward );
	    MakeParam( paramForwardFlag, forwarded );
	    memcpy( edd, &eventDoneData, eventDoneData.fHeader.fLength );
		
	    memcpy( p1, msgData, size1 );
	    memcpy( p2, msgData+size1, size-size1 );
	    }
	fPubToSub.HaveWritten( size, p1, p2, size1 );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "EventDoneSing message send")
	    << "Unable to write EventDoneSing message ("
	     << AliHLTLog::kDec << size
	    << " Bytes) to subscriber (Pub-To-Sub shm key 0x" << AliHLTLog::kHex << (int)fPubToSubKey
	    << " (" << AliHLTLog::kDec << (int)fPubToSubKey << ")): " 
	    << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    pthread_mutex_unlock( &fMutex );
    return ret;
    }

int AliHLTPublisherShmProxy::EventDone( AliHLTSubscriberInterface&, 
				       vector<AliHLTEventDoneData*>& eventDoneData, bool forceForward, bool forwarded )
    {
    if ( !fPubToSub )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "EventDoneMult")
	    << "EventDoneMult attempt without open pub-to-sub shared memory." << ENDLOG;
	return EIO;
	}
    pthread_mutex_lock( &fMutex );
    AliHLTPubSubShmMsg* msg;
    AliHLTPubSubShmMsgParam* param;
    AliHLTPubSubShmMsgParam* paramForceFlag;
    AliHLTPubSubShmMsgParam* paramForwardFlag;
    AliHLTEventDoneData* edd;

    AliUInt32_t size, size1;
    AliUInt8_t *p1, *p2;
    int ret;
    unsigned i, n = eventDoneData.size();

    size = sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam)*3;
    for ( i = 0; i < n; i++ )
	size += eventDoneData[i]->fHeader.fLength;
    
    ret = fPubToSub.CanWrite( size, p1, p2, size1 );
    if ( ret == 0 )
	{
	if ( !p2 )
	    {
	    msg = (AliHLTPubSubShmMsg*)p1;
	    param = (AliHLTPubSubShmMsgParam*)( p1+sizeof(AliHLTPubSubShmMsg)+1 );
	    paramForceFlag = (AliHLTPubSubShmMsgParam*)( p1+sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam) );
	    paramForwardFlag = (AliHLTPubSubShmMsgParam*)( p1+sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam)*2 );
	    edd = (AliHLTEventDoneData*)( p1+sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam)*3 );

	    MakeMsg( msg, AliHLTPubSubShmMsg::kEventDoneMult, 3 );
	    MakeParam( param, eventDoneData );
	    MakeParam( paramForceFlag, forceForward );
	    MakeParam( paramForwardFlag, forwarded );
	    for ( i = 0; i < n; i++ )
		{
		memcpy( edd, eventDoneData[i], eventDoneData[i]->fHeader.fLength );
		edd = (AliHLTEventDoneData*)(((AliUInt8_t*)edd)+eventDoneData[i]->fHeader.fLength);
		}
	    }
	else
	    {
	    AliUInt8_t msgData[ size ];
	    msg = (AliHLTPubSubShmMsg*)msgData;
	    param = (AliHLTPubSubShmMsgParam*)( msgData+sizeof(AliHLTPubSubShmMsg)+1 );
	    paramForceFlag = (AliHLTPubSubShmMsgParam*)( msgData+sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam) );
	    paramForwardFlag = (AliHLTPubSubShmMsgParam*)( msgData+sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam)*2 );
	    edd = (AliHLTEventDoneData*)( msgData+sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam)*3 );

	    MakeMsg( msg, AliHLTPubSubShmMsg::kEventDoneMult, 3 );
	    MakeParam( param, eventDoneData );
	    MakeParam( paramForceFlag, forceForward );
	    MakeParam( paramForwardFlag, forwarded );
	    for ( i = 0; i < n; i++ )
		{
		memcpy( edd, eventDoneData[i], eventDoneData[i]->fHeader.fLength );
		edd = (AliHLTEventDoneData*)(((AliUInt8_t*)edd)+eventDoneData[i]->fHeader.fLength);
		}

	    memcpy( p1, msgData, size1 );
	    memcpy( p2, msgData+size1, size-size1 );
	    }
	fPubToSub.HaveWritten( size, p1, p2, size1 );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "EventDoneMult message send")
	    << "Unable to write EventDoneMult message ("
	    << AliHLTLog::kDec << size
	    << " Bytes) to subscriber (Pub-To-Sub shm key 0x" << AliHLTLog::kHex << (int)fPubToSubKey
	    << " (" << AliHLTLog::kDec << (int)fPubToSubKey << ")): " 
	    << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    pthread_mutex_unlock( &fMutex );
    return ret;
    }

int AliHLTPublisherShmProxy::StartPublishing( AliHLTSubscriberInterface& interface, bool sendOldEvents )
    {
    if ( !fPubToSub )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "StartPublishing")
	    << "StartPublishing attempt without open pub-to-sub shared memory." << ENDLOG;
	return EIO;
	}
    pthread_mutex_lock( &fMutex );
    AliHLTPubSubShmMsg* msg;
    AliHLTPubSubShmMsgParam* param;

    AliUInt32_t size, size1;
    AliUInt8_t *p1, *p2;
    int ret;

    size = sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam);
    ret = fPubToSub.CanWrite( size, p1, p2, size1 );
    if ( ret == 0 )
	{
	if ( !p2 )
	    {
	    msg = (AliHLTPubSubShmMsg*)p1;
	    param = (AliHLTPubSubShmMsgParam*)( p1+sizeof(AliHLTPubSubShmMsg)+1 );
	    MakeMsg( msg, AliHLTPubSubShmMsg::kStartPublishing, 0 );
	    MakeParam( param, sendOldEvents );
	    }
	else
	    {
	    AliUInt8_t msgData[ size ];
	    msg = (AliHLTPubSubShmMsg*)msgData;
	    param = (AliHLTPubSubShmMsgParam*)( msgData+sizeof(AliHLTPubSubShmMsg)+1 );
	    MakeMsg( msg, AliHLTPubSubShmMsg::kStartPublishing, 0 );
	    MakeParam( param, sendOldEvents );
	    memcpy( p1, msgData, size1 );
	    memcpy( p2, msgData+size1, size-size1 );
	    }
	fPubToSub.HaveWritten( size, p1, p2, size1 );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "StartPublishing message send")
	    << "Unable to write StartPublishing message ("
	     << AliHLTLog::kDec << size
	    << " Bytes) to subscriber (Pub-To-Sub shm key 0x" << AliHLTLog::kHex << (int)fPubToSubKey
	    << " (" << AliHLTLog::kDec << (int)fPubToSubKey << ")): " 
	    << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    pthread_mutex_unlock( &fMutex );
    fQuitLoop = false;
    return MsgLoop( interface );
    }

int AliHLTPublisherShmProxy::Ping( AliHLTSubscriberInterface& )
    {
    if ( !fPubToSub )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "Ping")
	    << "Ping attempt without open pub-to-sub shared memory." << ENDLOG;
	return EIO;
	}
    pthread_mutex_lock( &fMutex );
    AliHLTPubSubShmMsg* msg;

    AliUInt32_t size, size1;
    AliUInt8_t *p1, *p2;
    int ret;

    size = sizeof(AliHLTPubSubShmMsg)+1;
    ret = fPubToSub.CanWrite( size, p1, p2, size1 );
    if ( ret == 0 )
	{
	if ( !p2 )
	    {
	    msg = (AliHLTPubSubShmMsg*)p1;
	    MakeMsg( msg, AliHLTPubSubShmMsg::kPing, 0 );
	    }
	else
	    {
	    AliUInt8_t msgData[ size ];
	    msg = (AliHLTPubSubShmMsg*)msgData;
	    MakeMsg( msg, AliHLTPubSubShmMsg::kPing, 0 );

	    memcpy( p1, msgData, size1 );
	    memcpy( p2, msgData+size1, size-size1 );
	    }
	fPubToSub.HaveWritten( size, p1, p2, size1 );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "Ping message send")
	    << "Unable to write Ping message ("
	     << AliHLTLog::kDec << size
	    << " Bytes) to subscriber (Pub-To-Sub shm key 0x" << AliHLTLog::kHex << (int)fPubToSubKey
	    << " (" << AliHLTLog::kDec << (int)fPubToSubKey << ")): " 
	    << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    pthread_mutex_unlock( &fMutex );
    return ret;
    }

int AliHLTPublisherShmProxy::PingAck( AliHLTSubscriberInterface& )
    {
    if ( !fPubToSub )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "PingAck")
	    << "PingAck attempt without open pub-to-sub shared memory." << ENDLOG;
	return EIO;
	}
    pthread_mutex_lock( &fMutex );
    AliHLTPubSubShmMsg* msg;

    AliUInt32_t size, size1;
    AliUInt8_t *p1, *p2;
    int ret;

    size = sizeof(AliHLTPubSubShmMsg)+1;
    ret = fPubToSub.CanWrite( size, p1, p2, size1 );
    if ( ret == 0 )
	{
	if ( !p2 )
	    {
	    msg = (AliHLTPubSubShmMsg*)p1;
	    MakeMsg( msg, AliHLTPubSubShmMsg::kPingAck, 0 );
	    }
	else
	    {
	    AliUInt8_t msgData[ size ];
	    msg = (AliHLTPubSubShmMsg*)msgData;
	    MakeMsg( msg, AliHLTPubSubShmMsg::kPingAck, 0 );

	    memcpy( p1, msgData, size1 );
	    memcpy( p2, msgData+size1, size-size1 );
	    }
	fPubToSub.HaveWritten( size, p1, p2, size1 );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "PingAck message send")
	    << "Unable to write PingAck message ("
	     << AliHLTLog::kDec << size
	    << " Bytes) to subscriber (Pub-To-Sub shm key 0x" << AliHLTLog::kHex << (int)fPubToSubKey
	    << " (" << AliHLTLog::kDec << (int)fPubToSubKey << ")): " 
	    << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    pthread_mutex_unlock( &fMutex );
    return ret;
    }


void AliHLTPublisherShmProxy::SetTimeout( AliUInt32_t timeout_usec )
    {
    fPubToSub.SetTimeout( timeout_usec );
    fSubToPub.SetTimeout( timeout_usec );
    }

void AliHLTPublisherShmProxy::AbortPublishing()
    {
    fQuitLoop = true;
    fSubToPub.Abort();
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    gettimeofday( &start, NULL );
    while ( !fLoopQuitted && deltaT<timeLimit )
	{
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    }

void AliHLTPublisherShmProxy::MakeMsg( AliHLTPubSubShmMsg* msg, const char* subscrName, 
				      AliHLTPubSubShmMsg::MsgType type,
				      AliUInt32_t paramCnt, key_t pub2SubKey, key_t sub2PubKey,
				      AliUInt32_t shmSize )
    {
    int sz = sizeof(AliHLTPubSubShmMsg)+1;
    if ( subscrName )
	sz += ( strlen(subscrName) + 2*sizeof(key_t) + sizeof(AliUInt32_t) );
    if ( !msg )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "Message NULL pointer")
	    << "Message NULL pointer passed for subscriber " << subscrName << ENDLOG;
	return;
	}
    msg->fHeader.fLength = sz;
    msg->fHeader.fType.fID = ALIL3PUBSUBPIPEMSG_TYPE;
    msg->fHeader.fSubType.fID = 0;
    msg->fHeader.fVersion = 1;
    msg->fType = (AliUInt32_t)type;
    msg->fParamCnt = paramCnt;
    if ( subscrName )
	{
	strcpy( (char*)msg->fSubscriberName, subscrName );
	key_t* pkey = (key_t*)( ((AliUInt8_t*)msg)+sizeof(AliHLTPubSubShmMsg)+1+strlen(subscrName) );
	*pkey = pub2SubKey;
	pkey++;
	*pkey = sub2PubKey;
	pkey++;
	AliUInt32_t* pSz = (AliUInt32_t*)pkey;
	*pSz = shmSize;
	}
    else
	msg->fSubscriberName[0]=0;
    }

void AliHLTPublisherShmProxy::MakeParam( AliHLTPubSubShmMsgParam* param, bool val )
    {
    param->fHeader.fType.fID = ALIL3PUBSUBPIPEMSGPARAM_TYPE;
    param->fHeader.fSubType.fID = 0;
    param->fHeader.fVersion = 1;
    param->fHeader.fLength = sizeof(AliHLTPubSubShmMsgParam);
    param->fType = AliHLTPubSubShmMsgParam::kBool;
    param->fData = (AliUInt64_t)val;
    }

void AliHLTPublisherShmProxy::MakeParam( AliHLTPubSubShmMsgParam* param, AliUInt32_t val )
    {
    param->fHeader.fType.fID = ALIL3PUBSUBPIPEMSGPARAM_TYPE;
    param->fHeader.fSubType.fID = 0;
    param->fHeader.fVersion = 1;
    param->fHeader.fLength = sizeof(AliHLTPubSubShmMsgParam);
    param->fType = AliHLTPubSubShmMsgParam::kUInt32;
    param->fData = val;
    }

void AliHLTPublisherShmProxy::MakeParam( AliHLTPubSubShmMsgParam* param, vector<AliHLTEventTriggerStruct*>& val )
    {
    param->fHeader.fType.fID = ALIL3PUBSUBPIPEMSGPARAM_TYPE;
    param->fHeader.fSubType.fID = 0;
    param->fHeader.fVersion = 1;
    param->fHeader.fLength = sizeof(AliHLTPubSubShmMsgParam);
    param->fType = AliHLTPubSubShmMsgParam::kVectorETT;
    param->fData = val.size();
    }

void AliHLTPublisherShmProxy::MakeParam( AliHLTPubSubShmMsgParam* param, const AliHLTEventDoneData& )
    {
    param->fHeader.fType.fID = ALIL3PUBSUBPIPEMSGPARAM_TYPE;
    param->fHeader.fSubType.fID = 0;
    param->fHeader.fVersion = 1;
    param->fHeader.fLength = sizeof(AliHLTPubSubShmMsgParam);
    param->fType = AliHLTPubSubShmMsgParam::kEvtDoneD;
    param->fData = 0;
    }

void AliHLTPublisherShmProxy::MakeParam( AliHLTPubSubShmMsgParam* param, vector<AliHLTEventDoneData*>& val )
    {
    param->fHeader.fType.fID = ALIL3PUBSUBPIPEMSGPARAM_TYPE;
    param->fHeader.fSubType.fID = 0;
    param->fHeader.fVersion = 1;
    param->fHeader.fLength = sizeof(AliHLTPubSubShmMsgParam);
    param->fType = AliHLTPubSubShmMsgParam::kVectorEvtDoneD;
    param->fData = val.size();
    }


int AliHLTPublisherShmProxy::MsgLoop( AliHLTSubscriberInterface& interface )
    {
    fLoopQuitted = false;
    int ret;
    char name[9], turnedName[9];
    AliUInt8_t *msgP1, *msgP2;
    AliUInt32_t msgSize1;
    AliHLTBlockHeader *pbh, sample = { 0, {ALIL3PUBSUBPIPEMSG_TYPE}, {0}, 1 };
    AliHLTPubSubShmMsg *msg;
    do
	{
	pbh = NULL;
	ret = fSubToPub.Read( pbh, msgP1, msgP2, msgSize1, true );
	msg = (AliHLTPubSubShmMsg*)pbh;
	if ( ret && ret!=ETIMEDOUT )
	    {
	    LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "Msg loop read")
		<< "Error reading publisher (Publisher " << GetName() << ") message from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	    return ret;
	    }
	if ( msg->fHeader.fType.fID != ALIL3PUBSUBPIPEMSG_TYPE )
	    {
	    strncpy( name, (const char*)msg->fHeader.fType.fDescr, 4 );
	    name[4] = 0;
	    turnedName[0] = name[3];
	    turnedName[1] = name[2];
	    turnedName[2] = name[1];
	    turnedName[3] = name[0];
	    turnedName[4] = 0;
	    LOGM( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "Msg loop", 100 )
		<< "Read unexpected datatype " << AliHLTLog::kHex << msg->fHeader.fType.fID
		<< " == " << name << " (" << turnedName << ")"
		<< " from shared memory when expecting message (" << sample.fType.fID << ")" << ENDLOG;
	    fSubToPub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
	    continue;
	    }
	switch ( msg->fType )
	    {
	    case AliHLTPubSubShmMsg::kNewEvent:
		{
		LOG( AliHLTLog::kDebug, "AliHLTPublisherShmProxy", "NewEvent" )
		    << "NewEvent message received from publisher " << GetName() << ENDLOG;
		ret = HandleNewEventMsg( interface, msg, msgP1, msgP2, msgSize1 );
		if ( ret )
		    return ret;
		break;
		}
	    case AliHLTPubSubShmMsg::kEventCanceled:
		{
		LOG( AliHLTLog::kDebug, "AliHLTPublisherShmProxy", "EventCanceled" )
		    << "EventCanceled message received from publisher " << GetName() << ENDLOG;
		ret = HandleEventCanceledMsg( interface, msg, msgP1, msgP2, msgSize1 );
		if ( ret )
		    return ret;
		break;
		}
	    case AliHLTPubSubShmMsg::kEventDoneData:
		{
		LOG( AliHLTLog::kDebug, "AliHLTPublisherShmProxy", "EventDoneData" )
		    << "EventDoneData message received from subscriber " << GetName() << ENDLOG;
		ret = HandleEventDoneDataMsg( interface, msg, msgP1, msgP2, msgSize1 );
		if ( ret )
		    return ret;
		break;
		}
	    case AliHLTPubSubShmMsg::kSubscriptionCanceled:
		{
		LOG( AliHLTLog::kDebug, "AliHLTPublisherShmProxy", "SubscriptionCanceled" )
		    << "SubscriptionCanceled message received from publisher " << GetName() << ENDLOG;
		interface.SubscriptionCanceled( *this );
		fSubToPub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
		fQuitLoop = true;
		break;
		}
	    case AliHLTPubSubShmMsg::kReleaseEventsRequest:
		{
		LOG( AliHLTLog::kDebug, "AliHLTPublisherShmProxy", "ReleaseEventsRequest" )
		    << "ReleaseEventsRequest message received from publisher  " << GetName() << ENDLOG;
		interface.ReleaseEventsRequest( *this );
		fSubToPub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
		if ( ret )
		    return ret;
		break;
		}
	    case AliHLTPubSubShmMsg::kPing:
		{
		LOG( AliHLTLog::kDebug, "AliHLTPublisherShmProxy", "Ping" )
		    << "Ping message received from publisher" << ENDLOG;
		interface.Ping( *this );
		fSubToPub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
		break;
		}
	    case AliHLTPubSubShmMsg::kPingAck:
		{
		LOG( AliHLTLog::kDebug, "AliHLTPublisherShmProxy", "PingAck" )
		    << "PingAck message received from publisher" << ENDLOG;
		interface.PingAck( *this );
		fSubToPub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
		break;
		}
	    default:
		LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy::MsgLoop", "Unknown msg received" )
		    << "Received unknown message!" << ENDLOG;
		fSubToPub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
		break;
	    }
	}
    while ( !fQuitLoop );
    fLoopQuitted = true;
    return 0;
    }


int AliHLTPublisherShmProxy::HandleNewEventMsg( AliHLTSubscriberInterface& interface, AliHLTPubSubShmMsg* msg, 
				       AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 )
    {
    int ret;
    AliUInt8_t *parEvtIDP1, *parEvtIDP2, *parSEDDP1, *parSEDDP2, *seddP1, *seddP2,
	*parETTP1, *parETTP2, *ettP1, *ettP2;;
    AliUInt32_t parEvtIDSize1, parSEDDSize1, seddSize1, parETTSize1, ettSize1;
    char name[9], turnedName[9];
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    // Check message parameter count
    if ( msg->fParamCnt != 3 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "NewEvent msg", 100 )
	    << "Unexpected parameter count for NewEvent message: " << msg->fParamCnt
	    << " instead of 3" << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
	return 0;
	}
    fSubToPub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
    msg = NULL;
    AliHLTPubSubShmMsgParam *parEvtID, *parSEDD, *parETT;
    AliHLTSubEventDataDescriptor* sedd;
    AliHLTEventTriggerStruct* ett;
    AliHLTBlockHeader *pbh;
    // Read first message parameter descriptor (event ID) (parameter included)
    pbh = NULL;
    ret = fSubToPub.Read( pbh, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
    parEvtID = (AliHLTPubSubShmMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "NewEvent msg parameter")
	    << "Error reading NewEvent message event ID parameter from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
    if ( parEvtID->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)parEvtID->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "NewEvent msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << parEvtID->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
	return 0;
	}
    if ( parEvtID->fType != AliHLTPubSubShmMsgParam::kEvtID )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "NewEvent msg parameter type", 100 )
	    << "Unexpected message parameter type for NewEvent message: " << AliHLTLog::kDec 
	    << (int)parEvtID->fType << " when expecting " << (int)AliHLTPubSubShmMsgParam::kEvtID << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
	return 0;
	}
    // Read second message parameter descriptor (SubEventDataDescriptor)
    pbh = NULL;
    ret = fSubToPub.Read( pbh, parSEDDP1, parSEDDP2, parSEDDSize1 );
    parSEDD = (AliHLTPubSubShmMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "NewEvent msg parameter")
	    << "Error reading NewEvent message subevent data descriptor parameter from shared memory. " 
	    << "Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
	return ret;
	}
    if ( parSEDD->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)parSEDD->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "NewEvent msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << parSEDD->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 1 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parSEDD, parSEDDP1, parSEDDP2, parSEDDSize1 );
	return 0;
	}
    if ( parSEDD->fType != AliHLTPubSubShmMsgParam::kSEDD )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "NewEvent msg parameter type", 100 )
	    << "Unexpected message parameter type for NewEvent message: " << AliHLTLog::kDec 
	    << (int)parSEDD->fType << " when expecting " << (int)AliHLTPubSubShmMsgParam::kSEDD << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parSEDD, parSEDDP1, parSEDDP2, parSEDDSize1 );
	return 0;
	}
    // Read second message parameter data 
    pbh = NULL;
    ret = fSubToPub.Read( pbh, seddP1, seddP2, seddSize1 );
    sedd = (AliHLTSubEventDataDescriptor*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "NewEvent msg parameter")
	    << "Error reading NewEvent message subevent data descriptor from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parSEDD, parSEDDP1, parSEDDP2, parSEDDSize1 );
	return ret;
	}
    if ( sedd->fHeader.fType.fID != ALIL3SUBEVENTDATADESCRIPTOR_TYPE )
	{
	sample.fType.fID = ALIL3SUBEVENTDATADESCRIPTOR_TYPE;
	strncpy( name, (char*)sedd->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "NewEvent msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << sedd->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting SubEventDataDescriptor (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parSEDD, parSEDDP1, parSEDDP2, parSEDDSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)sedd, seddP1, seddP2, seddSize1 );
	return 0;
	}

    // Read third message parameter descriptor (EventTriggerStruct)
    pbh = NULL;
    ret = fSubToPub.Read( pbh, parETTP1, parETTP2, parETTSize1 );
    parETT = (AliHLTPubSubShmMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "NewEvent msg parameter")
	    << "Error reading NewEvent message event trigger struct parameter from shared memory. " 
	    << "Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parSEDD, parSEDDP1, parSEDDP2, parSEDDSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)sedd, seddP1, seddP2, seddSize1 );
	return ret;
	}
    if ( parETT->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	sample.fType.fID = ALIL3PUBSUBPIPEMSGPARAM_TYPE;
	strncpy( name, (char*)parETT->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "NewEvent msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << parETT->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 2 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parSEDD, parSEDDP1, parSEDDP2, parSEDDSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)sedd, seddP1, seddP2, seddSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parETT, parETTP1, parETTP2, parETTSize1 );
	return 0;
	}
    if ( parETT->fType != AliHLTPubSubShmMsgParam::kETT )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "NewEvent msg parameter type", 100 )
	    << "Unexpected message parameter type for NewEvent message: " << AliHLTLog::kDec 
	    << (int)parETT->fType << " when expecting " << (int)AliHLTPubSubShmMsgParam::kETT << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parSEDD, parSEDDP1, parSEDDP2, parSEDDSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)sedd, seddP1, seddP2, seddSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parETT, parETTP1, parETTP2, parETTSize1 );
	return 0;
	}

    // Read third message parameter data 
    pbh = NULL;
    ret = fSubToPub.Read( pbh, ettP1, ettP2, ettSize1 );
    ett = (AliHLTEventTriggerStruct*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "NewEvent msg parameter")
	    << "Error reading NewEvent message subevent data descriptor from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parSEDD, parSEDDP1, parSEDDP2, parSEDDSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)sedd, seddP1, seddP2, seddSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parETT, parETTP1, parETTP2, parETTSize1 );
	return ret;
	}
    if ( ett->fHeader.fType.fID != ALIL3EVENTTRIGGERSTRUCT_TYPE )
	{
	sample.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
	strncpy( name, (char*)ett->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "NewEvent msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << ett->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting EventTriggerStruct (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parSEDD, parSEDDP1, parSEDDP2, parSEDDSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)sedd, seddP1, seddP2, seddSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parETT, parETTP1, parETTP2, parETTSize1 );
	fSubToPub.FreeRead( (AliHLTBlockHeader*)ett, ettP1, ettP2, ettSize1 );
	return 0;
	}

    interface.NewEvent( *this, AliEventID_t( parEvtID->fDataType, parEvtID->fData ), *sedd, *ett );

    fSubToPub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
    fSubToPub.FreeRead( (AliHLTBlockHeader*)parSEDD, parSEDDP1, parSEDDP2, parSEDDSize1 );
    fSubToPub.FreeRead( (AliHLTBlockHeader*)sedd, seddP1, seddP2, seddSize1 );
    fSubToPub.FreeRead( (AliHLTBlockHeader*)parETT, parETTP1, parETTP2, parETTSize1 );
    fSubToPub.FreeRead( (AliHLTBlockHeader*)ett, ettP1, ettP2, ettSize1 );
    return 0;
    }


int AliHLTPublisherShmProxy::HandleEventCanceledMsg( AliHLTSubscriberInterface& interface, AliHLTPubSubShmMsg* msg, 
				       AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 )
    {
    int ret;
    char name[9], turnedName[9];
    AliUInt8_t *parEvtIDP1, *parEvtIDP2;
    AliUInt32_t parEvtIDSize1;
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    // Check message parameter count
    if ( msg->fParamCnt != 1 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "EventCanceled msg", 100 )
	    << "Unexpected parameter count for EventCanceled message: " << msg->fParamCnt
	    << " instead of 1" << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
	return 0;
	}
    fSubToPub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
    msg = NULL;
    AliHLTPubSubShmMsgParam *parEvtID;
    AliHLTBlockHeader* pbh;
    pbh = NULL;
    ret = fSubToPub.Read( pbh, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
    parEvtID = (AliHLTPubSubShmMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "EventCanceled msg parameter")
	    << "Error reading EventCanceled message event ID parameter from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
    if ( parEvtID->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)parEvtID->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "EventCanceled msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << parEvtID->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
	return 0;
	}
    if ( parEvtID->fType != AliHLTPubSubShmMsgParam::kEvtID )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "EventCanceled msg parameter type", 100 )
	    << "Unexpected message parameter type for EventCanceled message: " << AliHLTLog::kDec 
	    << (int)parEvtID->fType << " when expecting " << (int)AliHLTPubSubShmMsgParam::kEvtID << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
	return 0;
	}
    interface.EventCanceled( *this, AliEventID_t( parEvtID->fDataType, parEvtID->fData ) );
    fSubToPub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
    return 0;
    }


int AliHLTPublisherShmProxy::HandleEventDoneDataMsg( AliHLTSubscriberInterface& interface, AliHLTPubSubShmMsg* msg, AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 )
    {
    int ret;
    char name[9], turnedName[9];
    AliUInt8_t *parEvtIDP1, *parEvtIDP2;
    AliUInt8_t *parEvtDDP1, *parEvtDDP2;
    AliUInt32_t parEvtIDSize1;
    AliUInt32_t parEvtDDSize1;
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    if ( msg->fParamCnt != 1 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "EventDoneData msg", 100 )
	    << "Unexpected parameter count for EventDoneData message: " << msg->fParamCnt
	    << " instead of 1" << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
	return 0;
	}
    fSubToPub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
    msg = NULL;
    AliHLTPubSubShmMsgParam * parEvtID;
    AliHLTBlockHeader *pbh;
    AliHLTEventDoneData* edd;
    pbh = NULL;
    ret = fSubToPub.Read( pbh, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
    parEvtID = (AliHLTPubSubShmMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "EventDoneData msg parameter")
	    << "Error reading EventDoneData message bool parameter from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
    if ( parEvtID->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)parEvtID->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "EventDoneData msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << parEvtID->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
	return 0;
	}
    if ( parEvtID->fType != AliHLTPubSubShmMsgParam::kEvtDoneD )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "EventDoneData msg parameter type", 100 )
	    << "Unexpected message parameter type for EventDoneData message: " << AliHLTLog::kDec 
	    << (int)parEvtID->fType << " when expecting " << (int)AliHLTPubSubShmMsgParam::kEvtDoneD << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
	return 0;
	}
    fSubToPub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );

    ret = fSubToPub.Read( pbh, parEvtDDP1, parEvtDDP2, parEvtDDSize1 );
    edd = (AliHLTEventDoneData*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherShmProxy", "EventDoneData msg parameter")
	    << "Error reading EventDoneData message event done data from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
    if ( edd->fHeader.fType.fID != ALIL3EVENTDONEDATA_TYPE )
	{
	sample.fType.fID = ALIL3EVENTDONEDATA_TYPE;
	strncpy( name, (char*)edd->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherShmProxy", "EventDoneData msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << edd->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fSubToPub.FreeRead( (AliHLTBlockHeader*)edd, parEvtDDP1, parEvtDDP2, parEvtDDSize1 );
	return 0;
	}

    LOG( AliHLTLog::kDebug, "AliHLTPublisherShmProxy", "EventDoneData msg" )
	<< "EventDoneData event: 0x" << AliHLTLog::kHex << edd->fEventID << " (" 
	<< AliHLTLog::kDec << edd->fEventID << ")." << ENDLOG;
    interface.EventDoneData( *this, *edd );
    fSubToPub.FreeRead( (AliHLTBlockHeader*)edd, parEvtDDP1, parEvtDDP2, parEvtDDSize1 );
    return 0;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
