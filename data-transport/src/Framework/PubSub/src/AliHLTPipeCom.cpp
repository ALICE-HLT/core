/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTPipeCom.hpp"
#include "AliHLTLog.hpp"
#include "MLUCLimitTimer.hpp"
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/uio.h>
#include <fstream>
#include <sys/mman.h>
#include <unistd.h>


AliHLTPipeCom::AliHLTPipeCom()
    {
    fReadPipeName = "";
//    fReadHandle = -1;
    fWritePipeName = "";
//    fWriteHandle = -1;
//    fPipeSize=0;
    fCache=NULL;
    fOpenRead = false;
    fOpenWrite = false;
    fTimeout.tv_sec = 0;
    fTimeout.tv_usec = 0;
    fCacheStart = 0;
    fCacheSize = 0;
    fSharedFdReadHandle = -1;
    fSharedFdWriteHandle = -1;
    fSharedReadMemory = NULL;
    fSharedWriteMemory = NULL;
    fSharedReadControl = NULL;
    fSharedWriteControl = NULL;
    fSharedReadBuffer = NULL;
    fSharedWriteBuffer = NULL;
#ifdef PIPE_PARANOIA
    fWriteCount = 0;
    fReadCount = 0;
#endif
    }

AliHLTPipeCom::AliHLTPipeCom( const char* readPipe, const char* writePipe, bool create )
    {
    fReadPipeName = "";
//    fReadHandle = -1;
    fWritePipeName = "";
//    fWriteHandle = -1;
//    fPipeSize=0;
    fCache=NULL;
    fOpenRead = true;
    fOpenWrite = true;
    fTimeout.tv_sec = 0;
    fTimeout.tv_usec = 0;
    fCacheStart = 0;
    fCacheSize = 0;
    fSharedFdReadHandle = -1;
    fSharedFdWriteHandle = -1;
    fSharedReadMemory = NULL;
    fSharedWriteMemory = NULL;
    fSharedReadControl = NULL;
    fSharedWriteControl = NULL;
    fSharedReadBuffer = NULL;
    fSharedWriteBuffer = NULL;
    Open( readPipe, writePipe, create );
#ifdef PIPE_PARANOIA
    fWriteCount = 0;
    fReadCount = 0;
#endif
    }

AliHLTPipeCom::AliHLTPipeCom( const char* readPipe, bool create )
    {
    fReadPipeName = "";
//    fReadHandle = -1;
    fWritePipeName = "";
//    fWriteHandle = -1;
//    fPipeSize=0;
    fCache=NULL;
    fOpenWrite = false;
    fOpenRead = true;
    fTimeout.tv_sec = 0;
    fTimeout.tv_usec = 0;
    fCacheStart = 0;
    fCacheSize = 0;
    fSharedFdReadHandle = -1;
    fSharedFdWriteHandle = -1;
    fSharedReadMemory = NULL;
    fSharedWriteMemory = NULL;
    fSharedReadControl = NULL;
    fSharedWriteControl = NULL;
    fSharedReadBuffer = NULL;
    fSharedWriteBuffer = NULL;
    OpenRead( readPipe, create );
#ifdef PIPE_PARANOIA
    fWriteCount = 0;
    fReadCount = 0;
#endif
    }

AliHLTPipeCom::AliHLTPipeCom( const char* writePipe )
    {
    fReadPipeName = "";
//    fReadHandle = -1;
    fWritePipeName = "";
//    fWriteHandle = -1;
//    fPipeSize=0;
    fCache=NULL;
    fOpenWrite = true;
    fOpenRead = false;
    fTimeout.tv_sec = 0;
    fTimeout.tv_usec = 0;
    fCacheStart = 0;
    fCacheSize = 0;
    fSharedFdReadHandle = -1;
    fSharedFdWriteHandle = -1;
    fSharedReadMemory = NULL;
    fSharedWriteMemory = NULL;
    fSharedReadControl = NULL;
    fSharedWriteControl = NULL;
    fSharedReadBuffer = NULL;
    fSharedWriteBuffer = NULL;
    OpenWrite( writePipe );
#ifdef PIPE_PARANOIA
    fWriteCount = 0;
    fReadCount = 0;
#endif
    }

int AliHLTPipeCom::Open( const char* readPipe, const char* writePipe, bool create )
    {
    fOpenRead = true;
    fOpenWrite = true;
    fReadPipeName = readPipe;
    fWritePipeName = writePipe;
//    fReadHandle = -1;
//    fWriteHandle = -1;

/*    if ( create )
	{
	if ( CreatePipe( readPipe )==-1 )
	    return errno;
	if ( CreatePipe( writePipe )==-1 )
	    return errno;
	}

    int ret=-1;
    fReadHandle = OpenPipe( readPipe, true );
    if ( fReadHandle==-1 )
	return errno;

    char buf[ fPipeSize ];
    if ( create )
	{
	do
	    {
	    ret = read( fReadHandle, buf, fPipeSize );
	    if ( ret>0 )
		{
		LOG( AliHLTLog::kDebug, "AliHLTPipeCom", "Read Pipe Emptying" )
		    << "Read " << AliHLTLog::kDec << ret << " bytes from freshly opened read pipe."
		    << ENDLOG;
		}
	    }
	while ( ret>0 );
	fWriteHandle = OpenPipe( writePipe, true );
	if ( fWriteHandle==-1 )
	    {
	    ClosePipe( fReadHandle );
	    return errno;
	    }
	do
	    {
	    ret = read( fWriteHandle, buf, fPipeSize );
	    if ( ret>0 )
		{
		LOG( AliHLTLog::kDebug, "AliHLTPipeCom", "Write Pipe Emptying" )
		    << "Read " << AliHLTLog::kDec << ret << " bytes from freshly opened write pipe."
		    << ENDLOG;
		}
	    }
	while ( ret>0 );
	ClosePipe( fWriteHandle );
	}
    fWriteHandle = OpenPipe( writePipe, false );
    if ( fWriteHandle==-1 )
	{
	ClosePipe( fReadHandle );
	return errno;
	}*/

    int ret = OpenSharedSegment(readPipe, true, fSharedFdReadHandle, fSharedReadMemory, fSharedReadBuffer, fSharedReadControl, true);
    if (ret) return(ret);
    ret = OpenSharedSegment(writePipe, true, fSharedFdWriteHandle, fSharedWriteMemory, fSharedWriteBuffer, fSharedWriteControl, false);
    if (ret)
    {
	CloseSharedSegment(readPipe, true, fSharedFdReadHandle, fSharedReadMemory, true);
	return(ret);
    }

    LOG( AliHLTLog::kInformational, "AliHLTPipeCom", "Open pipes" )
	<< "Pipes " << readPipe << " and " << writePipe
	<< " successfully opened" << ENDLOG;

    return 0;
    }

int AliHLTPipeCom::OpenRead( const char* readPipe, bool create )
    {
    fOpenRead = true;
    fOpenWrite = false;
    fReadPipeName = readPipe;
    fWritePipeName = "";
//    fReadHandle = -1;
//    fWriteHandle = -1;

/*    if ( create )
	{
	if ( CreatePipe( readPipe )==-1 )
	    return errno;
	}

    fReadHandle = OpenPipe( readPipe, true );
    if ( fReadHandle==-1 )
	return errno;*/
    int ret = OpenSharedSegment(readPipe, true, fSharedFdReadHandle, fSharedReadMemory, fSharedReadBuffer, fSharedReadControl, true);
    if (ret) return(ret);

    LOG( AliHLTLog::kInformational, "AliHLTPipeCom", "Open pipes" )
	<< "Read pipe " << readPipe 
	<< " successfully opened" << ENDLOG;

    return 0;
    }

int AliHLTPipeCom::OpenWrite( const char* writePipe )
    {
    fOpenRead = false;
    fOpenWrite = true;
    fWritePipeName = writePipe;
    fReadPipeName = "";
//    fReadHandle = -1;
//    fWriteHandle = -1;

/*    fWriteHandle = OpenPipe( writePipe, false );
    if ( fWriteHandle==-1 )
	return errno;*/

    int ret = OpenSharedSegment(writePipe, true, fSharedFdWriteHandle, fSharedWriteMemory, fSharedWriteBuffer, fSharedWriteControl, false);
    if (ret) return(ret);


    LOG( AliHLTLog::kInformational, "AliHLTPipeCom", "Open pipes" )
	<< "Write pipe " << writePipe 
	<< " successfully opened" << ENDLOG;

    return 0;
    }

int AliHLTPipeCom::Close()
    {
    int ret1=0, ret2=0;
    bool cl1=false, cl2=false;
/*    if ( fReadHandle!=-1 )
	{
	cl1 = true;
	ret1 = ClosePipe( fReadHandle );
	}*/
    if (fSharedFdReadHandle != -1)
    {
	cl1 = true;
	ret1 = CloseSharedSegment(fReadPipeName.c_str(), true, fSharedFdReadHandle, fSharedReadMemory, true); //The reader does the cleanup
    }
    if ( ret1 )
	LOG( AliHLTLog::kWarning, "AliHLTPipeCom", "Close pipes" )
	    << "Error closing read pipe " << fReadPipeName.c_str() << ENDLOG;
/*    if ( fWriteHandle!=-1 )
	{
	cl2 = true;
	ret2 = ClosePipe( fWriteHandle );
	}*/
    if (fSharedFdWriteHandle != -1)
    {
	cl2 = true;
	ret2 = CloseSharedSegment(fWritePipeName.c_str(), false, fSharedFdWriteHandle, fSharedWriteMemory, false);
    }
    if ( ret2 )
	LOG( AliHLTLog::kWarning, "AliHLTPipeCom", "Close pipes" )
	    << "Error closing write pipe " << fWritePipeName.c_str() << ENDLOG;

    if ( ret1 )
	return ret1;
    if ( ret2 )
	return ret2;

    if ( cl1 && cl2 )
	{
	LOG( AliHLTLog::kInformational, "AliHLTPipeCom", "Close pipes" )
	    << "Pipes " << fReadPipeName.c_str() << " and " << fWritePipeName.c_str()
	    << " closed" << ENDLOG;
	}
    else
	{
	if ( cl1 )
	    {
	    LOG( AliHLTLog::kInformational, "AliHLTPipeCom", "Close pipes" )
		<< "Pipe " << fReadPipeName.c_str()
		<< " closed" << ENDLOG;
	    }
	if ( cl2 )
	    {
	    LOG( AliHLTLog::kInformational, "AliHLTPipeCom", "Close pipes" )
		<< "Pipe " << fWritePipeName.c_str()
		<< " closed" << ENDLOG;
	    }
	}
    fReadPipeName = "";
    fWritePipeName = "";
//    fPipeSize=0;
    if(fCache) delete [] fCache;
    fCache=NULL;
    fSharedFdReadHandle = -1;
    fSharedFdWriteHandle = -1;
    return 0;
    }

AliHLTPipeCom::~AliHLTPipeCom()
    {
    Close();
    }

int AliHLTPipeCom::Read( AliUInt32_t size, AliUInt8_t* buffer, bool block, struct timeval* timeout )
    {
    MLUCLIMITTIMER( MLUCLIMITTIMER_DEFAULT_TIMER_LIMIT_US );
    int ret;
    AliUInt32_t count = 0, toRead, szRead = 0;
    fd_set rd;
    bool retry;
//    if ( fReadHandle==-1 )
    if ( fSharedFdReadHandle==-1 )
	{
	LOG( AliHLTLog::kError, "Alil3PipeCom", "Pipe read" )
	    << "Handle for read pipe " << fReadPipeName.c_str() 
	    << " is invalid." << ENDLOG;
	return EBADF;
	}
    struct timeval* ptv, tv;
    //     struct timeval s, e;
    while ( szRead < size )
	{
	if ( fCacheSize > 0 )
	    {
	    if ( size-szRead < fCacheSize )
		toRead = size-szRead;
	    else
		toRead = fCacheSize;
	    memcpy( buffer+szRead, fCache+fCacheStart, toRead );
#ifdef PIPE_PARANOIA
	    fReadCount += toRead;
#endif
	    fCacheStart += toRead;
	    fCacheSize -= toRead;
	    szRead += toRead;
	    }
	if ( szRead < size )
	    {
	    retry = true;
	    if ( fCacheSize != 0 )
		{
		LOG( AliHLTLog::kFatal, "AliHLTPipeCom::Read", "Internal Error" )
		    << "Internal error while trying to read " << AliHLTLog::kDec
		    << size << " bytes from pipe " << fReadPipeName.c_str()
		    << "." << ENDLOG;
		}
	    fCacheStart = 0;
	    do
		{
		// 		if ( size-szRead < MAXPIPEBLOCKSIZE )
		// 		  toRead = size-szRead;
		// 		else
		// 		  toRead = MAXPIPEBLOCKSIZE;
//		toRead = fPipeSize;
		toRead = sharedSize;
		MLUCLIMITTIMER_NAME( readTimer, MLUCLIMITTIMER_DEFAULT_TIMER_LIMIT_US );
//		ret = read( fReadHandle, fCache, toRead );
		ret = readShared( fCache, toRead );
		MLUCLIMITTIMER_NAME_STOP( readTimer );
		if ( ret >=0 )
		    {
		    retry = false;
		    fCacheSize = ret;
		    }
		if ( ret < 0 && errno!=EAGAIN )
		    {
		    LOG( AliHLTLog::kError, "Alil3PipeCom", "Pipe read" )
			<< "Error on read call on pipe " << fReadPipeName.c_str() 
			<< ". Reason: " << strerror(errno) << " (" << errno << ")."
			<< ENDLOG;
		    return errno;
		    }
		if ( ret < 0 && errno==EAGAIN )
		    retry = false;
		}
	    while ( retry );
	    if ( fCacheSize == 0 )
		{
//		FD_ZERO( &rd );
//		FD_SET( fReadHandle, &rd );
		if ( (!fTimeout.tv_sec && !fTimeout.tv_usec && !timeout) || block )
		    {
		    ptv = NULL;
		    }
		else
		    {
		    tv = fTimeout;
		    if ( timeout )
		        tv = *timeout;
		    ptv = &tv;
		    }
		do
		    {
		    retry = false;
		    // 	    gettimeofday( &s, NULL );
		    errno = 0;
		    MLUCLIMITTIMER_NAME( selectTimer, MLUCLIMITTIMER_DEFAULT_TIMER_LIMIT_US );
//		    ret = select( fReadHandle+1, &rd, NULL, NULL, ptv );
		    ret = waitForWrite(ptv);
		    if (ret == 0 && fSharedReadControl->readPtr != fSharedReadControl->writePtr) ret = 1;
		    MLUCLIMITTIMER_NAME_STOP( selectTimer );
		    // 	    gettimeofday( &e, NULL );
		    if ( ret < 0 && errno == EINTR && count++ < 1000 )
			{
			LOG( AliHLTLog::kWarning, "Alil3PipeCom", "Pipe read" )
			    << "Failure on select call on pipe " << fReadPipeName.c_str() 
			    << ". Reason: " << strerror(errno) << " (" << errno << "). Retrying" << ENDLOG;
			retry = true;
			}
		    if ( retry && ptv )
			{
			tv = fTimeout;
			// 		if ( e.tv_usec < s.tv_usec )
			// 		    {
			// 		    e.tv_usec += 1000000;
			// 		    e.tv_sec -= 1;
			// 		    }
			// 		e.tv_usec -= s.tv_usec;
			// 		e.tv_sec -= s.tv_sec;
			// 		tv = fTimeout;
			// 		if ( tv.tv_usec < e.tv_usec )
			// 		    {
			// 		    if ( tv.tv_sec>0 )
			// 			{
			// 			tv.tv_usec += 1000000;
			// 			tv.tv_sec -= 1;
			// 			}
			// 		    else
			// 			tv.tv_usec = tv.tv_sec = 0;
			// 		    }
			// 		if ( tv.tv_sec > e.tv_sec && tv.tv_usec > e.tv_usec )
			// 		    {
			// 		    tv.tv_usec -= e.tv_usec;
			// 		    tv.tv_sec -= e.tv_sec;
			// 		    }
			// 		else
			// 		    tv.tv_usec = tv.tv_sec = 0;
			}
		    }
		while ( retry );
		if ( ret < 0 )
		    {
		    LOG( AliHLTLog::kError, "Alil3PipeCom", "Pipe read" )
			<< "Error on select call on pipe " << fReadPipeName.c_str() 
			<< ". Reason: " << strerror(errno) << " (" << errno << ")."
			<< ENDLOG;
		    return errno;
		    }
		if ( ret == 0 )
		    {
//		    LOG( AliHLTLog::kInformational, "Alil3PipeCom", "Pipe read" )
//			<< "Timeout on select call on pipe " << fReadPipeName.c_str() 
//			<< "." << ENDLOG;
		    return ETIMEDOUT;
		    }
		}
	    }
	}
    return 0;
    }

int AliHLTPipeCom::Read( AliHLTBlockHeader*& header, bool block, struct timeval* timeout )
    {
    MLUCLIMITTIMER( MLUCLIMITTIMER_DEFAULT_TIMER_LIMIT_US );
    int ret;
    AliUInt32_t headerSize=0;
    AliHLTBlockHeader head;
    AliHLTBlockHeader* phead;
    if ( header )
	{
	headerSize = header->fLength;
	phead = header;
	}
    else
	phead = &head;
    ret = this->Read( sizeof(AliHLTBlockHeader), (AliUInt8_t*)phead, block, timeout );
    if ( ret )
	return ret;
    if ( phead->fLength<sizeof(AliHLTBlockHeader) )
	{
	LOG( AliHLTLog::kError, "AliHLTPipeCom::Read", "Wrong message header length" )
	    << "Wrong message header size " << AliHLTLog::kDec << phead->fLength << " expected at least "
	    << sizeof(AliHLTBlockHeader) << "." << ENDLOG;
	return EMSGSIZE;
	}
    // XXX
#ifdef DEBUGPARANOIA
    if ( head.fLength >= 1024 )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPipeCom::Read", "Possibly too large message" )
	    << "Possibly too large message read from pipe " << fReadPipeName
	    << " - Size: " << AliHLTLog::kDec << head.fLength << " - Type: "
	    << (char)head.fType.fDescr[0] << (char)head.fType.fDescr[1] << (char)head.fType.fDescr[2]
	    << (char)head.fType.fDescr[3] << " - Subtype: " 
	    << (char)head.fSubType.fDescr[0] << (char)head.fSubType.fDescr[1] << (char)head.fSubType.fDescr[2]
	    << " - Version. " << head.fVersion << "." << ENDLOG;
	}
#endif
    // XXX
    if ( !header || phead->fLength>headerSize )
	{
	header = (AliHLTBlockHeader*)new AliUInt8_t[ phead->fLength ];
	if ( !header )
	    return ENOMEM;
	*header = *phead;
	}
    //ret = this->Read( header->fLength-sizeof(AliHLTBlockHeader), ((AliUInt8_t*)header)+sizeof(AliHLTBlockHeader), block, timeout );
    ret = this->Read( header->fLength-sizeof(AliHLTBlockHeader), ((AliUInt8_t*)header)+sizeof(AliHLTBlockHeader), true, NULL );
    if ( ret )
	return ret;
    return 0;
    }

int AliHLTPipeCom::Write( AliUInt32_t size, const AliUInt8_t* buffer, bool block, bool quitting )
    {
    if (size == 0) return(0);
    int ret;
    if (fSharedWriteControl->writers > 1)
    {
	//There are multiple writers, we must do an atomic write
	if (size >= sharedSize)
	{
	    LOG( AliHLTLog::kError, "Alil3PipeCom", "Pipe write" ) << "Message too big, cannot pipe through shared buffer" << ENDLOG;
	    return(-1);
	}
	do
	{
	    ret = writeShared( buffer, size, true, false, quitting );
	} while (ret == 0);
	return(0);
    }

    MLUCLIMITTIMER( MLUCLIMITTIMER_DEFAULT_TIMER_LIMIT_US );
    AliUInt32_t count = 0, toWrite, szWritten = 0;
    fd_set wd;
    bool retry;
    int error_try=0, max_error_try=10;
//    if ( fWriteHandle==-1 )
    if ( fSharedFdWriteHandle==-1 )
	{
	LOG( AliHLTLog::kError, "Alil3PipeCom", "Pipe write" )
	    << "Handle for write pipe " << fWritePipeName.c_str() 
	    << " is invalid." << ENDLOG;
	return EBADF;
	}
    //struct timeval *ptv, tv, s, e;
    struct timeval *ptv, tv;
    while ( szWritten < size )
	{
	retry = true;
	do
	    {
//	    if ( size-szWritten < fPipeSize )
	    if ( size-szWritten < sharedSize )
		toWrite = size-szWritten;
	    else
		toWrite = sharedSize;
//	    ret = write( fWriteHandle, buffer+szWritten, toWrite );
	    ret = writeShared( buffer + szWritten, toWrite, false, quitting);
	    if ( ret >= 0 )
		{
#ifdef PIPE_PARANOIA
		fWriteCount += ret;
#endif
		szWritten += ret;
		}
	    if ( ret < 0 && errno!=EAGAIN )
		{
		  if(++error_try <= max_error_try) {
		    LOG( AliHLTLog::kWarning, "Alil3PipeCom", "Pipe write" )
		      << "Try # " << error_try << ": Error on write call on pipe "
		      << fWritePipeName.c_str()
                      << ". Reason: " << strerror(errno) << " (" << errno << ")."
                      << ENDLOG;
		  } else {
		    LOG( AliHLTLog::kError, "Alil3PipeCom", "Pipe write" )
		      << "Tried " << error_try << " times, aborting: Error on write call on pipe "
		      << fWritePipeName.c_str() 
		      << ". Reason: " << strerror(errno) << " (" << errno << ")."
		      << ENDLOG;
		    return errno;
		  }
		}
	    if ( ret < 0 && errno==EAGAIN )
		retry = false;
	    }
	while ( retry && szWritten < size );
	/*if ( szWritten < size )
	    {
	    FD_ZERO( &wd );
	    FD_SET( fWriteHandle, &wd );
	    if ( (!fTimeout.tv_sec && !fTimeout.tv_usec) || block )
		ptv = NULL;
	    else
		{
		tv = fTimeout;
		ptv = &tv;
		}
	    do
		{
		retry = false;
		//gettimeofday( &s, NULL );
		errno = 0;
		ret = select( fWriteHandle+1, NULL, &wd, NULL, ptv );
		//gettimeofday( &e, NULL );
		if ( ret < 0 && errno == EINTR && count++ < 1000  )
		    {
		    LOG( AliHLTLog::kWarning, "Alil3PipeCom", "Pipe write" )
			<< "Failure on select call on pipe " << fWritePipeName.c_str() 
			<< ". Reason: " << strerror(errno) << " (" << errno << "). Retrying" << ENDLOG;
		    retry = true;
		    }
		if ( retry && ptv )
		    {
		    tv = fTimeout;
		    // 		if ( e.tv_usec < s.tv_usec )
		    // 		    {
		    // 		    e.tv_usec += 1000000;
		    // 		    e.tv_sec -= 1;
		    // 		    }
		    // 		e.tv_usec -= s.tv_usec;
		    // 		e.tv_sec -= s.tv_sec;
		    // 		tv = fTimeout;
		    // 		if ( tv.tv_usec < e.tv_usec )
		    // 		    {
		    // 		    if ( tv.tv_sec>0 )
		    // 			{
		    // 			tv.tv_usec += 1000000;
		    // 			tv.tv_sec -= 1;
		    // 			}
		    // 		    else
		    // 			tv.tv_usec = tv.tv_sec = 0;
		    // 		    }
		    // 		if ( tv.tv_sec > e.tv_sec && tv.tv_usec > e.tv_usec )
		    // 		    {
		    // 		    tv.tv_usec -= e.tv_usec;
		    // 		    tv.tv_sec -= e.tv_sec;
		    // 		    }
		    // 		else
		    // 		    tv.tv_usec = tv.tv_sec = 0;
		    }
		}
	    while ( retry );
	    if ( ret < 0 )
		{
		LOG( AliHLTLog::kError, "Alil3PipeCom", "Pipe write" )
		    << "Error on select call on pipe " << fWritePipeName.c_str() 
		    << "." << ENDLOG;
		return errno;
		}
	    if ( ret == 0 )
		{
		LOG( AliHLTLog::kError, "Alil3PipeCom", "Pipe write" )
		    << "Timeout on select call on pipe " << fWritePipeName.c_str() 
		    << ". Reason: " << strerror(errno) << " (" << errno << ")."
		    << ENDLOG;
		return ETIMEDOUT;
		}
	    }*/
	}
    return 0;
    }

int AliHLTPipeCom::Write( unsigned count, AliUInt32_t* sizes, AliUInt8_t** buffers, bool block )
    {
    writeSharedMulti(count, (const unsigned char**) buffers, sizes);
    return(0);
/*    if ( !count )
	return 0;
    int ret;
    
    bool dolock = fSharedWriteControl->writers > 1;

    if (dolock)
    {
	//If there are multiple writers, we must combine everything into an atomic write
	sem_wait(&fSharedWriteControl->writeSem);
	for (int i = 0;i < count;i++)
	{
	    if (sizes[i])
	    {
		if (sizes[i] >= sharedSize)
		{
		    LOG( AliHLTLog::kError, "Alil3PipeCom", "Pipe write" ) << "Message too big, cannot pipe through shared buffer: " << sizes[i] << " > " << sharedSize << ENDLOG;
		    if (dolock) sem_post(&fSharedWriteControl->writeSem);
		    return(-1);
		}
		do
		{
		    ret = writeShared( buffers[i], sizes[i], true, true );
		} while (ret == 0);
	    }
	}
	sem_post(&fSharedWriteControl->writeSem);
	return(0);
    }
    
    for (int i = 0;i < count;i++)
    {
	ret = Write(sizes[i], buffers[i], block);
	if (ret) return(ret);
    }
    
    return(0);*/
    
/*    MLUCLIMITTIMER( MLUCLIMITTIMER_DEFAULT_TIMER_LIMIT_US );
    if ( fWriteHandle==-1 )
	{
	LOG( AliHLTLog::kError, "Alil3PipeCom", "Pipe write" )
	    << "Handle for write pipe " << fWritePipeName.c_str() 
	    << " is invalid." << ENDLOG;
	return EBADF;
	}
    unsigned nextBuffer = 0;
    AliUInt32_t iterCount = 0, szWritten = 0;
    AliUInt32_t toWrite[ count ];
    toWrite[0] = sizes[0];
    for ( unsigned ic=1; ic<count; ic++ )
	{
	toWrite[ic] = toWrite[ic-1]+sizes[ic];
	}
    AliUInt32_t totalToWrite = toWrite[count-1];
    fd_set wd;
    bool retry;
    //struct timeval *ptv, tv, s, e;
    struct timeval *ptv, tv;
    unsigned long tempSize;
    unsigned tempCount, bi, tempOffset;
    struct iovec ioVecs[count];
    int error_try=0, max_error_try=10;
    while ( szWritten < totalToWrite )
	{
	retry = true;
	do
	    {
	    tempSize = 0;
	    tempCount=0;
	    bi = nextBuffer;
	    if ( bi>0 )
		tempOffset = szWritten-toWrite[bi-1];
	    else
		tempOffset = szWritten;
// 	    LOG( AliHLTLog::kDebug, "AliHLTPipeCom::Write", "Preparing vector write" )
// 		<< "Preparing vector write: nextBuffer==" << AliHLTLog::kDec << nextBuffer
// 		<< " - tempOffset==0x" << AliHLTLog::kHex << tempOffset
// 		<< " (" << AliHLTLog::kDec << tempOffset << ")." << ENDLOG;
	    while ( tempSize<fPipeSize && bi<count)
		{
		if ( tempSize+sizes[bi]-tempOffset<=fPipeSize )
		    {
		    ioVecs[tempCount].iov_base = buffers[bi]+tempOffset;
		    ioVecs[tempCount].iov_len = sizes[bi]-tempOffset;
		    tempSize += sizes[bi]-tempOffset;
		    tempCount++;
		    bi++;
		    }
		else
		    {
		    ioVecs[tempCount].iov_base = buffers[bi]+tempOffset;
		    ioVecs[tempCount].iov_len = fPipeSize-tempSize;
		    tempSize = fPipeSize;
		    tempCount++;
		    }
		tempOffset = 0;
		}
// 	    LOG( AliHLTLog::kDebug, "AliHLTPipeCom::Write", "Writing vector" )
// 		<< "Writing vector with " << AliHLTLog::kDec << tempCount 
// 		<< " entries to pipe." << ENDLOG;
// 	    for ( unsigned ic=0; ic<tempCount; ic++ )
// 		{
// 		LOG( AliHLTLog::kDebug, "AliHLTPipeCom::Write", "Writing vector" )
// 		    << "Vector " << AliHLTLog::kDec << ic << ": ox"
// 		    << AliHLTLog::kHex << (unsigned long)ioVecs[ic].iov_base << " / " 
// 		    << ioVecs[ic].iov_len << " (" << AliHLTLog::kDec << ioVecs[ic].iov_len
// 		    << ") bytes." << ENDLOG;
// 		}
	    do {
	      ret = writev( fWriteHandle, ioVecs, tempCount );
	      if(ret < 0 && errno == EFAULT && ++error_try <= max_error_try) {
		LOG( AliHLTLog::kWarning, "Alil3PipeCom", "Pipe write" )
		  << "Try # " << error_try << ": Error on write call on pipe "
		  << fWritePipeName.c_str()
		  << ". Reason: " << strerror(errno) << " (" << errno << ")."
		  << ENDLOG;
	      } else {
		break;
	      }
	    } while (true);
	    if ( ret > 0 )
		{
#ifdef PIPE_PARANOIA
		fWriteCount += ret;
#endif
		szWritten += ret;
		if ( (unsigned)ret==tempSize )
		    nextBuffer = bi;
		else
		    {
		    while ( nextBuffer<count && toWrite[nextBuffer]<=szWritten )
			nextBuffer++;
		    }
// 		LOG( AliHLTLog::kDebug, "AliHLTPipeCom::Write", "Vector written" )
// 		    << "Vector written: " << AliHLTLog::kDec << ret << " bytes written - szWritten: "
// 		    << szWritten << " - nextBuffer: " << nextBuffer << "." << ENDLOG;
		}
	    if ( ret < 0 && errno!=EAGAIN )
		{
		  LOG( AliHLTLog::kError, "Alil3PipeCom", "Pipe write" )
		    << "Tried " << error_try << " times, aborting: Error on write call on pipe "
		    << fWritePipeName.c_str() 
		    << ". Reason: " << strerror(errno) << " (" << errno << ")."
		    << ENDLOG;
		  return errno;
		}
	    if ( ret==0 || (ret < 0 && errno==EAGAIN) )
		retry = false;
	    }
	while ( retry && szWritten < totalToWrite );
	if ( szWritten < totalToWrite )
	    {
	    FD_ZERO( &wd );
	    FD_SET( fWriteHandle, &wd );
	    if ( (!fTimeout.tv_sec && !fTimeout.tv_usec) || block )
		ptv = NULL;
	    else
		{
		tv = fTimeout;
		ptv = &tv;
		}
	    iterCount = 0;
	    do
		{
		retry = false;
		//gettimeofday( &s, NULL );
		errno = 0;
		ret = select( fWriteHandle+1, NULL, &wd, NULL, ptv );
		//gettimeofday( &e, NULL );
		if ( ret < 0 && errno == EINTR && iterCount++ < 1000  )
		    {
		    LOG( AliHLTLog::kWarning, "Alil3PipeCom", "Pipe write" )
			<< "Failure on select call on pipe " << fWritePipeName.c_str() 
			<< ". Reason: " << strerror(errno) << " (" << errno << "). Retrying" << ENDLOG;
		    retry = true;
		    }
		if ( retry && ptv )
		    {
		    tv = fTimeout;
		    }
		}
	    while ( retry );
	    if ( ret < 0 )
		{
		LOG( AliHLTLog::kError, "Alil3PipeCom", "Pipe write" )
		    << "Error on select call on pipe " << fWritePipeName.c_str() 
		    << "." << ENDLOG;
		return errno;
		}
	    if ( ret == 0 )
		{
		LOG( AliHLTLog::kError, "Alil3PipeCom", "Pipe write" )
		    << "Timeout on select call on pipe " << fWritePipeName.c_str() 
		    << ". Reason: " << strerror(errno) << " (" << errno << ")."
		    << ENDLOG;
		return ETIMEDOUT;
		}
	    }
	}*/
    return 0;
    }


int AliHLTPipeCom::Write( const AliHLTBlockHeader* header, bool block, bool quitting )
    {
    MLUCLIMITTIMER( MLUCLIMITTIMER_DEFAULT_TIMER_LIMIT_US );
    if ( !header )
	return EFAULT;
    // XXX
#ifdef DEBUGPARANOIA
    if ( header->fLength >= 1024 )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPipeCom::Write", "Possibly too large message" )
	    << "Possibly too large message write from pipe " << fReadPipeName
	    << " - Size: " << AliHLTLog::kDec << header->fLength << " - Type: "
	    << (char)header->fType.fDescr[0] << (char)header->fType.fDescr[1] << (char)header->fType.fDescr[2]
	    << (char)header->fType.fDescr[3] << " - Subtype: " 
	    << (char)header->fSubType.fDescr[0] << (char)header->fSubType.fDescr[1] << (char)header->fSubType.fDescr[2]
	    << " - Version. " << header->fVersion << "." << ENDLOG;
	}
#endif
    // XXX
    return this->Write( header->fLength, (AliUInt8_t*)header, block, quitting );
    }

int AliHLTPipeCom::Write( unsigned count, AliHLTBlockHeader const** headers, bool block )
    {
    if ( !headers )
	return EFAULT;
    if ( !count )
	return 0;
//     if ( count==1 )
// 	{
// 	if ( !headers[0] )
// 	    return EFAULT;
// 	return this->Write( headers[0]->fLength, (AliUInt8_t*)headers[0], block );
// 	}
    AliUInt32_t sizes[count];
    AliUInt8_t* buffers[count];
    unsigned si, di;
    for ( di=si=0; si<count; si++ )
	{
	if ( headers[si] )
	    {
	    sizes[di] = headers[si]->fLength;
	    buffers[di] = (AliUInt8_t*)(headers[si]);
	    di++;
	    }
	}
    return Write( count, sizes, (AliUInt8_t**)buffers, block );
    }


AliHLTPipeCom::operator bool()
    {
//    if ( (fOpenRead && fReadHandle==-1) || ( fOpenWrite && fWriteHandle==-1 ) )
    if ( (fOpenRead && fSharedFdReadHandle==-1) || ( fOpenWrite && fSharedFdWriteHandle==-1 ) )
	return false;
    return true;
    }

void AliHLTPipeCom::SetTimeout( AliUInt32_t timeout_usec )
    {
//	LOG( AliHLTLog::kError, "AliHLTPipeCom::Write", "Possibly too large message" ) << "Set timeout: " << timeout_usec << ENDLOG;
    fTimeout.tv_sec = timeout_usec/1000000;
    fTimeout.tv_usec = timeout_usec%1000000;
    }

// AliHLTPipeCom::operator string()
//     {
//     string tmp;
//     bool opened = false;
//     if ( fRWOpened )
// 	{
// 	tmp = "AliHLTPipeCom: Pipes " + fReadPipeName + " and " + fWritePipeName;
// 	if ( fReadHandle!=-1 && fWriteHandle!=-1 )
// 	    opened = true;
// 	}
//     else
// 	{
// 	tmp = "AliHLTPipeCom: Pipe " + fReadPipeName;
// 	if ( fReadHandle!=-1 )
// 	    opened = true;
// 	}
//     if ( opened )
// 	tmp += " open";
//     else
// 	tmp += " not open";
//     return tmp;
//     }


/*int AliHLTPipeCom::CreatePipe( const char* name )
    {
    struct stat dat;
    int ret;
    //printf( "stat'ing pipe %s\n", (const char*)name );
    ret = stat( name, &dat );
    if ( ret==-1 && errno==ENOENT )
	{
	// named pipe not found, attempting to create:
	// Warning named pipe is created with rights rw-rw----
	// (umask will be applied to this afterwards...)
	//printf( "mkfifo'ing pipe %s\n", (const char*)name );
	ret = mkfifo( name, 0660 );
	if ( ret==-1 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTPipeCom", "Access pipes" )
		<< "Could not create FIFO (mkfifo)" << name << ". Reason: " << strerror(errno) 
		<< " (" << errno << ")." << ENDLOG; 
	    return -1;
	    }
	}
    if ( ret==-1 && errno!=ENOENT )
	{
	LOG( AliHLTLog::kError, "AliHLTPipeCom", "Create pipes" )
	    << "Could not stat FIFO (stat)" << name << ". Reason: " << strerror(errno)
	    << " (" << errno << ")." << ENDLOG;
	return -1;
	}
    return 0;
    }

int AliHLTPipeCom::OpenPipe( const char* name, bool read )
    {
    int ret;
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 10000000;
    gettimeofday( &start, NULL );
    do
	{
	//printf( "open'ing pipe %s\n", (const char*)name );
	if ( read )
	    ret = open( name, O_RDWR|O_NONBLOCK );//ret = open( name, O_RDONLY|O_NONBLOCK );
	else
	    ret = open( name, O_RDWR|O_NONBLOCK );
	if ( ret==-1 )
	    usleep( 0 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    while ( ret==-1 && deltaT<timeLimit );
    // Warning: The above uses implementation specific behaviour on Linux.
    // Posix does not specify, what happens for a pipe opened read-write,
    // either non-blocking or blocking, when there is no reader already
    // available.
    if ( ret==-1 )
	{
	LOG( AliHLTLog::kError, "AliHLTPipeCom", "Open pipes" )
	    << "Unable to open FIFO (open " << name << "). Reason: " << strerror(errno)
	    << " (" << errno << ")" << ENDLOG;
	return -1;
	}
    //printf( "pipe %s open'ed\n", (const char*)name );
    int max_size=DEFAULTPIPEBLOCKSIZE;*/
    /* This could be used to set the pipe size to the maximum allowed value */
    /*
    std::fstream max_pipe_file;
    max_pipe_file.open("/proc/sys/fs/pipe-max-size", std::fstream::in);
    if(max_pipe_file.is_open()) {
      max_pipe_file >> max_size;
      max_pipe_file.close();
    } else {
      LOG( AliHLTLog::kError, "AliHLTPipeCom", "Open pipes" )
	<< "Cannot determine max pipe size via /proc/sys/fs/pipe-max-size, "
	<< "using default pipe size " << AliHLTLog::kDec << DEFAULTPIPEBLOCKSIZE
	<< "." << ENDLOG;
    }
    */
    /*int pSize=fcntl(ret, F_SETPIPE_SZ, max_size);
    if(pSize>=0){
      LOG( AliHLTLog::kDebug, "AliHLTPipeCom", "Open pipes" )
	<< "Set size of " << AliHLTLog::kDec << pSize << " to pipe " 
	<< name << "." << ENDLOG;
    } else {
      LOG( AliHLTLog::kError, "AliHLTPipeCom", "Open pipes" )
	<< "Error setting size " << AliHLTLog::kDec << max_size << " to pipe "
	<< name << ": " << strerror(pSize) << " (" << errno << ")" << ENDLOG;
      pSize=fcntl(ret, F_GETPIPE_SZ);
    }
    if(pSize != fPipeSize){
      fPipeSize=pSize;
      if(fCache) delete [] fCache;
      fCache=new AliUInt8_t[fPipeSize];
    }
    return ret;
    }

int AliHLTPipeCom::ClosePipe( int& handle )
    {
    int ret=0;
    if ( handle!=-1 )
	ret = close( handle );
    handle = -1;
    if ( ret==-1 )
	return errno;
    return 0;
    }*/


AliHLTLog& operator<<( AliHLTLog& log, const AliHLTPipeCom& pipe )
    {
    bool opened = false;
    if ( pipe.fOpenRead && pipe.fOpenWrite )
	{
	log << "AliHLTPipeCom: Pipes " << pipe.fReadPipeName.c_str() << " and " << pipe.fWritePipeName.c_str();
//	if ( pipe.fReadHandle!=-1 && pipe.fWriteHandle!=-1 )
	if ( pipe.fSharedFdReadHandle!=-1 && pipe.fSharedFdWriteHandle!=-1 )
	    opened = true;
	}
    else
	{
	log <<  "AliHLTPipeCom: Pipe " << (pipe.fOpenRead ? pipe.fReadPipeName.c_str() : pipe.fWritePipeName.c_str());
//	if ( (pipe.fOpenRead ? pipe.fReadHandle : pipe.fWriteHandle) != -1 )
	if ( (pipe.fOpenRead ? pipe.fSharedFdReadHandle : pipe.fSharedFdWriteHandle) != -1 )
	    opened = true;
	}
    if ( opened )
	log << " open";
    else
	log << " not open";
    return log;
    }

//Shared memory stuff
int AliHLTPipeCom::OpenSharedSegment(const char* segmentname, bool createControl, int& fd, void*& memory, unsigned char*& buffer, sharedMemControl*& control, bool read)
{
    if (strncmp(segmentname, "/tmp/", 5) == 0) segmentname += 5;
    char sharedName[256] = "/alice-hlt-shared-pipe-";
    strncat(sharedName, segmentname, 256 - 23);
    sharedName[255] = 0;

//    LOG( AliHLTLog::kImportant, "AliHLTPipeCom", "OpenSharedSegment" ) << "Opening Pipe " << sharedName << " (Read " << read << ", Create " << createControl << ")" << ENDLOG;
    
    int fdHandle;
    if (createControl)
    {
	fdHandle = shm_open(sharedName, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
	if (fdHandle == -1)
	{
	    createControl = false;
	}
    }
    if (!createControl)
    {
	fdHandle = shm_open(sharedName, O_RDWR, S_IRUSR | S_IWUSR);
    }

    if (fdHandle == -1)
    {
        LOG( AliHLTLog::kError, "AliHLTPipeCom", "OpenSharedSegment" ) << "Error creating / opening shared memory segment: " << sharedName << ENDLOG;
        return(-1);
    }

    void* sharedPtr = mmap(NULL, sharedTotalSize, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
    if (sharedPtr == MAP_FAILED)
    {
        LOG( AliHLTLog::kError, "AliHLTPipeCom", "OpenSharedSegment" ) << "Error allocating memory" << ENDLOG;
        return(1);
    }

    if (ftruncate(fdHandle, sharedSize + sharedControlSize) == -1)
    {
        LOG( AliHLTLog::kError, "AliHLTPipeCom", "OpenSharedSegment" ) << "Error setting size of shared memory segment" << ENDLOG;
        return(1);
    }

    sharedPtr = mmap(sharedPtr, sharedSize, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED, fdHandle, 0);
    if (sharedPtr == MAP_FAILED)
    {
        LOG( AliHLTLog::kError, "AliHLTPipeCom", "OpenSharedSegment" ) << "Error mapping shared memory" << ENDLOG;
        return(1);
    }
    void* tmpPtr = mmap((char*) sharedPtr + sharedSize, sharedSize + sharedControlSize, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED, fdHandle, 0);
    if (tmpPtr == MAP_FAILED)
    {
        LOG( AliHLTLog::kError, "AliHLTPipeCom", "OpenSharedSegment" ) << "Error mapping shared memory 2nd time" << ENDLOG;
        return(1);
    }

    fd = fdHandle;
    memory = sharedPtr;
    buffer = (unsigned char*) sharedPtr;
    control = (sharedMemControl*) (buffer + 2 * sharedSize);
    
    if (createControl)
    {
	if (control->initialized)
	{
    	    LOG( AliHLTLog::kError, "AliHLTPipeCom", "OpenSharedSegment" ) << "Shared segment already initialized" << ENDLOG;
    	    return(1);
	}
	memset(control, 0, sharedControlSize);

	if (sem_init(&control->readSem, 1, 0) || sem_init(&control->writeSem, 1, 1)/* || sem_init(&control->waitSem, 1, 1)*/)
	{
            LOG( AliHLTLog::kError, "AliHLTPipeCom", "OpenSharedSegment" ) << "Error initializing semaphore" << ENDLOG;
	    return(1);
	}
	if (pthread_spin_init(&control->waitSpin, 1))
	{
            LOG( AliHLTLog::kError, "AliHLTPipeCom", "OpenSharedSegment" ) << "Error initializing spinlock" << ENDLOG;
	    return(1);
	}
	control->readerSleeping = false;
	control->initialized = true;
    }
    else
    {
	while (control->initialized == false) usleep(10000);
    }
    
    if (!read)
    {
	sem_wait(&control->writeSem);
	control->writers++;
	sem_post(&control->writeSem);
    }

    if(fCache) delete[] fCache;
    fCache = new AliUInt8_t[sharedSize];

    return(0);
}

int AliHLTPipeCom::CloseSharedSegment(const char* segmentname, bool deleteControl, int fd, void* memory, bool read)
{
    if (strncmp(segmentname, "/tmp/", 5) == 0) segmentname += 5;
    char sharedName[256] = "/alice-hlt-shared-pipe-";
    strncat(sharedName, segmentname, 256 - 23);
    sharedName[255] = 0;

    /*if (read && deleteControl)
    {
        sharedMemControl* control = (sharedMemControl*) (((char*) memory) + 2 * sharedSize);
	sem_destroy(&control->readSem);
	sem_destroy(&control->writeSem);
	//sem_destroy(&control->waitSem);
	pthread_spin_destroy(&control->waitSpin);
    }*/
    

    if (munmap(memory, sharedTotalSize) != 0)
    {
        LOG( AliHLTLog::kError, "AliHLTPipeCom", "CloseSharedSegment" ) << "Error during unmap" << ENDLOG;
        return(1);
    }

    /*if (read && deleteControl && shm_unlink(sharedName) != 0)
    {
        LOG( AliHLTLog::kError, "AliHLTPipeCom", "CloseSharedSegment" ) << "Error during unlinking shared memory segment" << ENDLOG;
        return(1);
    }*/
    return(0);
}

int AliHLTPipeCom::writeShared(const unsigned char* buffer, unsigned int toWrite, bool atomic, bool nolock, bool quitting)
{
    if (toWrite == 0) return(0);
    
    if (fSharedWriteControl->writers <= 1) nolock = true;
    if (!nolock) sem_wait(&fSharedWriteControl->writeSem);
    unsigned int maxWrite = (fSharedWriteControl->readPtr - fSharedWriteControl->writePtr - 1) & sharedSizeBitMask;
    if (atomic ? (maxWrite < toWrite) : (!maxWrite))
    {
	if (!nolock) sem_post(&fSharedWriteControl->writeSem);
	return(0);
    }
    unsigned char* startPos = fSharedWriteBuffer + (fSharedWriteControl->writePtr & sharedSizeBitMask);
    unsigned int doWrite = toWrite > maxWrite ? maxWrite : toWrite;
    memcpy(startPos, buffer, doWrite);
    //sem_wait(&fSharedWriteControl->waitSem);
    if (!quitting) pthread_spin_lock(&fSharedWriteControl->waitSpin);
    fSharedWriteControl->writePtr += doWrite;
    if (fSharedWriteControl->readerSleeping)
    {
	fSharedWriteControl->readerSleeping = false;
	sem_post(&fSharedWriteControl->readSem);
    }
    //sem_post(&fSharedWriteControl->waitSem);
    if (!quitting) pthread_spin_unlock(&fSharedWriteControl->waitSpin);
    if (!nolock) sem_post(&fSharedWriteControl->writeSem);
    return(doWrite);
}

void AliHLTPipeCom::writeSharedMulti(unsigned int count, const unsigned char** buffers, unsigned int* sizes)
{
    if (count == 0) return;
    
    bool dolock = fSharedWriteControl->writers > 1;
    if (dolock) sem_wait(&fSharedWriteControl->writeSem);

    unsigned int writePtrTmp = fSharedWriteControl->writePtr;
    for (int i = 0;i < count;i++)
    {
	if (sizes[i] == 0) continue;

	int toWrite = sizes[i];
	int written = 0;
	do
	{
	    unsigned int maxWrite = (fSharedWriteControl->readPtr - writePtrTmp - 1) & sharedSizeBitMask;
	    if (maxWrite == 0)
	    {
		if (writePtrTmp != fSharedWriteControl->writePtr)
		{
		    pthread_spin_lock(&fSharedWriteControl->waitSpin);
		    fSharedWriteControl->writePtr = writePtrTmp;
		    if (fSharedWriteControl->readerSleeping)
		    {
			fSharedWriteControl->readerSleeping = false;
			sem_post(&fSharedWriteControl->readSem);
		    }
		    pthread_spin_unlock(&fSharedWriteControl->waitSpin);
		}
		continue;
	    }

	    unsigned char* startPos = fSharedWriteBuffer + (writePtrTmp & sharedSizeBitMask);
	    unsigned int doWrite = toWrite > maxWrite ? maxWrite : toWrite;
	    memcpy(startPos, buffers[i] + written, doWrite);
	
	    written += doWrite;
	    writePtrTmp += doWrite;
	    toWrite -= doWrite;
	}
	while (toWrite);
    }
    if (writePtrTmp != fSharedWriteControl->writePtr)
    {
        pthread_spin_lock(&fSharedWriteControl->waitSpin);
        fSharedWriteControl->writePtr = writePtrTmp;
        if (fSharedWriteControl->readerSleeping)
        {
	    fSharedWriteControl->readerSleeping = false;
	    sem_post(&fSharedWriteControl->readSem);
	}
        pthread_spin_unlock(&fSharedWriteControl->waitSpin);
    }

    if (dolock) sem_post(&fSharedWriteControl->writeSem);
    return;
}

int AliHLTPipeCom::readShared(unsigned char* buffer, unsigned int toRead)
{
    if (toRead == 0) return(0);
    unsigned int maxRead = (fSharedReadControl->writePtr - fSharedReadControl->readPtr) & sharedSizeBitMask;
    if (maxRead == 0) return(0);
    unsigned char* startPos = fSharedReadBuffer + (fSharedReadControl->readPtr & sharedSizeBitMask);
    unsigned int doRead = toRead > maxRead ? maxRead : toRead;
    memcpy(buffer, startPos, doRead);
    fSharedReadControl->readPtr += doRead;
    return(doRead);
}

int AliHLTPipeCom::waitForWrite(timeval* timeout)
{
    if (fSharedReadControl->writePtr != fSharedReadControl->readPtr) return(0);
    //sem_wait(&fSharedReadControl->waitSem);
    pthread_spin_lock(&fSharedReadControl->waitSpin);
    if (fSharedReadControl->writePtr != fSharedReadControl->readPtr)
    {
	//sem_post(&fSharedReadControl->waitSem);
	pthread_spin_unlock(&fSharedReadControl->waitSpin);
	return(0);
    }
    fSharedReadControl->readerSleeping = true;
    //sem_post(&fSharedReadControl->waitSem);
    pthread_spin_unlock(&fSharedReadControl->waitSpin);
    int ret;
    if (timeout)
    {
        timespec t;
	clock_gettime(CLOCK_REALTIME, &t);
        LOG( AliHLTLog::kError, "AliHLTPipeCom", "OpenSharedSegment" ) << "Using timeout" << timeout->tv_sec << "." << timeout->tv_usec << ENDLOG;
	t.tv_sec += timeout->tv_sec;
	t.tv_nsec += timeout->tv_usec * 1000;
	if (t.tv_nsec >= 1000000000)
	{
    	    t.tv_sec++;
    	    t.tv_nsec -= 1000000000;
	}
	ret = sem_timedwait(&fSharedReadControl->readSem, &t);
	//sem_wait(&fSharedReadControl->waitSem);
	pthread_spin_lock(&fSharedReadControl->waitSpin);
	if (fSharedReadControl->readerSleeping == false)
	{
	    sem_trywait(&fSharedReadControl->readSem);
	}
	fSharedReadControl->readerSleeping = false;
	//sem_post(&fSharedReadControl->waitSem);
	pthread_spin_unlock(&fSharedReadControl->waitSpin);
    }
    else
    {
	ret = sem_wait(&fSharedReadControl->readSem);
    }
    
    return(ret);
}

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
