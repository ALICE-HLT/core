/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTShmCom.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTBlockHeader.hpp"
#include <sys/types.h>
#include <sys/time.h>
#include <sys/unistd.h>
#include <cerrno>
#include <cstdlib>


#define SLEEPTIME_US 10000


AliHLTShmCom::AliHLTShmCom( key_t shmKey, AliUInt32_t size, bool create )
    {
    fSleepCount = 0;
    fShmKey = shmKey;
    fShmSize = size;
    fShmPtr = NULL;
    fShmID = -1;
    fTimeout = ~(AliUInt32_t)0;
    fRetryCount = 100;
    fShmID = shmget( fShmKey, fShmSize, (create ? IPC_CREAT : 0)|0664 );
    if ( fShmID == -1 )
	{
	LOG( AliHLTLog::kError, "AliHLTShmCom::AliHLTShmCom", "Error getting shm segment" )
	    << "Error getting shared memory segment of " << AliHLTLog::kDec << fShmSize
	    << " bytes with key 0x" << AliHLTLog::kHex << (int)fShmKey
	    << " (" << AliHLTLog::kDec << (int)fShmKey << ": " << strerror(errno) << " ("
	    << errno << ")." << ENDLOG;
	return;
	}
    fShmPtr = (AliUInt8_t*)shmat( fShmID, NULL, 0 );
    if ( !fShmPtr )
	{
	LOG( AliHLTLog::kError, "AliHLTShmCom::AliHLTShmCom", "Error attaching to shm segment" )
	    << "Error attaching to shared memory segment with key 0x" << AliHLTLog::kHex << (int)fShmKey
	    << " (" << AliHLTLog::kDec << (int)fShmKey << ": " << strerror(errno) << " ("
	    << errno << ")." << ENDLOG;
	struct shmid_ds id;
	shmctl( fShmID, IPC_RMID, &id );
	fShmID = -1;
	return;
	}
    fHeader = (TShmHeader*)fShmPtr;
    fData = fShmPtr+sizeof(TShmHeader);
    if ( create )
	{
	fHeader->fWriteNdx = fHeader->fReadNdx = fHeader->fFreeNdx = 0;
	fHeader->fSize = fShmSize-sizeof(TShmHeader);
	}
    fDbgLogLevel = (AliHLTLog::TLogLevel)0;
    //fDbgLogLevel = AliHLTLog::kDebug;
#ifdef SHMCOM_DEBUG
    fStateLimit = 200;
#endif
    fQuit = false;
    }

AliHLTShmCom::~AliHLTShmCom()
    {
    if ( fShmID != -1 )
	{
	if ( fShmPtr )
	    shmdt( (void*)fShmPtr );
	struct shmid_ds id;
	shmctl( fShmID, IPC_RMID, &id );
	}
    }

void AliHLTShmCom::SetTimeout( AliUInt32_t ms )
    {
    fTimeout = ms;
    }

void AliHLTShmCom::SetRetryTimeout( AliUInt32_t cnt )
    {
    fRetryCount = cnt;  
    }

int AliHLTShmCom::Write( AliUInt32_t size, AliUInt8_t* data, bool block )
    {
    AliUInt32_t tmp, size1;
    AliUInt8_t *p1, *p2;
    tmp = CanWrite( size, p1, p2, size1, block );
    if ( tmp )
	return tmp;
    if ( !p2 )
	{
	memcpy( p1, data, size );
	}
    else
	{
	memcpy( p1, data, size1 );
	memcpy( p2, data + size1, 
		size - size1 );
	}
    HaveWritten( size, p1, p2, size1 );
    return 0;
    }

int AliHLTShmCom::CanWrite( AliUInt32_t size, AliUInt8_t*& p1, AliUInt8_t*& p2, 
			   AliUInt32_t& size1, bool block )
    {
    if ( size > fHeader->fSize )
	return ENOSPC;
    if ( !size )
	{
	p1 = p2 = NULL;
	size1 = 0;
	return 0;
	}
    struct timeval t1, t2;
    bool doneLog = false;
    AliUInt32_t freeMem = 0;
    AliUInt32_t tdiff=0;
    AliUInt32_t cnt = 0;
    if ( !fShmPtr )
	return EFAULT;
    if ( fHeader->fFreeNdx <= fHeader->fWriteNdx )
	freeMem = fHeader->fSize - (fHeader->fWriteNdx - fHeader->fFreeNdx) - 1;
    else
	freeMem = fHeader->fFreeNdx - fHeader->fWriteNdx -1;
    if ( freeMem < size )
	{
	if ( !block )
	    gettimeofday( &t1, NULL );
	do
	    {
	    if ( fQuit )
		return ECANCELED;
	    if ( fHeader->fFreeNdx <= fHeader->fWriteNdx )
		freeMem = fHeader->fSize - (fHeader->fWriteNdx - fHeader->fFreeNdx) - 1;
	    else
		freeMem = fHeader->fFreeNdx - fHeader->fWriteNdx -1;
	    if ( freeMem >= size )
		break;
	    if ( cnt >= fRetryCount )
		{
		if ( !doneLog )
		    {
#ifdef SHMCOM_DEBUG
		    if ( fStates.size()>fStateLimit )
			fStates.erase( fStates.begin() );
		    fStates.insert( fStates.end(), *fHeader );
#endif
		    if ( !block )
			{
			gettimeofday( &t2, NULL );
			tdiff = t2.tv_sec*1000;
			tdiff -= t1.tv_sec*1000;
			tdiff += t2.tv_usec/1000;
			tdiff -= t1.tv_usec/1000;
			if ( tdiff >= fTimeout )
			    return ETIMEDOUT;
			}
		    LOG( fDbgLogLevel, "AliHLTShmCom::CanWrite", "Shm Status" )
			<< "Data write attempt: size: " << AliHLTLog::kDec << size 
			<< " - WriteNdx: " << fHeader->fWriteNdx << " - ReadNdx: " << fHeader->fReadNdx
			<< " - FreeNdx: " << fHeader->fFreeNdx << " - Size: " << fHeader->fSize
			<< "." << ENDLOG;
		    doneLog = true;
		    }
		fSleepCount++;
		usleep( SLEEPTIME_US );
		cnt = 0;
		}
	    else
		cnt++;
	    }
	while ( freeMem < size );
	}
    if ( fHeader->fWriteNdx + size <= fHeader->fSize )
	{
	p1 = fData + fHeader->fWriteNdx;
	size1 = size;
	p2 = NULL;
	}
    else
	{
	p1 = fData + fHeader->fWriteNdx;
	size1 = fHeader->fSize - fHeader->fWriteNdx;
	p2 = fData;
	}
    return 0;
    }

int AliHLTShmCom::HaveWritten( AliUInt32_t size, AliUInt8_t*, AliUInt8_t* p2, AliUInt32_t size1 )
    {
    AliUInt32_t  tmp;
    if ( !p2 )
	{
	tmp = fHeader->fWriteNdx + size;
	if ( tmp > fHeader->fSize )
	    {
	    LOG( AliHLTLog::kFatal, "AliHLTShmCom::HaveWritten", "Internal Error" )
		<< "Internal error, wrong size: " << AliHLTLog::kDec << size 
		<< " (0x" << AliHLTLog::kHex << size << ")." << ENDLOG;
	    }
	if ( tmp == fHeader->fSize )
	    tmp = 0;
	fHeader->fWriteNdx = tmp;
	}
    else
	{
	tmp = size-size1;
	fHeader->fWriteNdx = tmp;
	}
#ifdef SHMCOM_DEBUG
    if ( fStates.size()>fStateLimit )
	fStates.erase( fStates.begin() );
    fStates.insert( fStates.end(), *fHeader );
#endif
    LOG( fDbgLogLevel, "AliHLTShmCom::HaveWritten", "Shm Status" )
	<< "Data written: size: " << AliHLTLog::kDec << size << " - size1: " << size1
	<< " - WriteNdx: " << fHeader->fWriteNdx << " - ReadNdx: " << fHeader->fReadNdx
	<< " - FreeNdx: " << fHeader->fFreeNdx << " - Size: " << fHeader->fSize
	<< "." << ENDLOG;
    return 0;
    }

int AliHLTShmCom::Read( AliUInt32_t size, AliUInt8_t* data, bool block )
    {
    AliUInt32_t tmp, size1;
    AliUInt8_t *p1, *p2;
    tmp = CanRead( false, size, p1, p2, size1, block );
    if ( tmp )
	return tmp;
    if (  !p2 )
	{
	memcpy( data, p1, size );
	}
    else
	{
	memcpy( data, p1, size1 );
	memcpy( data + size1, p2, 
		size - size1 );
	}
    HaveRead( size, p1, p2, size1 );
    return 0;
    }

int AliHLTShmCom::Read( AliHLTBlockHeader*& pbh, AliUInt8_t*& p1, AliUInt8_t*& p2, AliUInt32_t& size1, bool block )
    {
    AliUInt32_t tmp;
    tmp = CanRead( true, sizeof(AliHLTBlockHeader), p1, p2, size1, block );
    if ( tmp )
	return tmp;
    LOG( fDbgLogLevel, "AliHLTShmCom::Read", "Block header peek" )
	<< "Peeked block header." << ENDLOG;
    if ( p2 )
	{
	AliHLTBlockHeader bh;
	memcpy( &bh, p1, size1 );
	memcpy( ((AliUInt8_t*)&bh)+size1, p2, sizeof(AliHLTBlockHeader)-size1 );
	LOG( fDbgLogLevel, "AliHLTShmCom::Read", "Block header read" )
	    << "Block size: " << AliHLTLog::kDec << bh.fLength << "." << ENDLOG;
	tmp = CanRead( false, bh.fLength, p1, p2, size1, block );
	if ( tmp )
	    {
	    pbh = NULL;
	    return tmp;
	    }
	pbh = (AliHLTBlockHeader*)malloc( bh.fLength );
	if ( !pbh )
	    {
	    LOG( AliHLTLog::kError, "AliHLTShmCom::Read", "Out of memory" )
		<< "Out of memory allocating memory for data of size " << bh.fLength
		<< " bytes." << ENDLOG;
	    HaveRead( bh.fLength, p1, p2, size1 );
	    return ENOMEM;
	    }
	AliUInt8_t* tmpP = (AliUInt8_t*)pbh;
	if ( p2 )
	    {
	    memcpy( tmpP, p1, size1 );
	    memcpy( tmpP+size1, p2, bh.fLength-size1 );
	    }
	else
	    {
	    LOG( AliHLTLog::kFatal, "AliHLTShmCom::Read", "Wrap around disappeared" )
		<< "Internal Error: Wrap around disappeared. Should not happen!" << ENDLOG;
	    memcpy( tmpP, p1, bh.fLength );
	    }
	LOG( fDbgLogLevel, "AliHLTShmCom::Read", "Block read (1)" )
	    << "Block of " << AliHLTLog::kDec << pbh->fLength << " bytes read (1)." << ENDLOG;
	DumpBlockHeader( fDbgLogLevel, "AliHLTShmCom::Read", "Block read (1)", pbh );
	}
    else
	{
	pbh = (AliHLTBlockHeader*)p1;
	LOG( fDbgLogLevel, "AliHLTShmCom::Read", "Block header accessed" )
	    << "Block size: " << AliHLTLog::kDec << pbh->fLength << "." << ENDLOG;
	AliUInt32_t len = pbh->fLength;
	tmp = CanRead( false, len, p1, p2, size1, block );
	if ( tmp )
	    return tmp;

	if ( p2 )
	    {
	    pbh = (AliHLTBlockHeader*)malloc( len );
	    if ( !pbh )
		{
		LOG( AliHLTLog::kError, "AliHLTShmCom::Read", "Out of memory" )
		    << "Out of memory allocating memory for data of size " << len
		    << " bytes." << ENDLOG;
		HaveRead( len, p1, p2, size1 );
		return ENOMEM;
		}
	    AliUInt8_t* tmpP = (AliUInt8_t*)pbh;
	    memcpy( tmpP, p1, size1 );
	    memcpy( tmpP+size1, p2, len-size1 );
	    LOG( fDbgLogLevel, "AliHLTShmCom::Read", "Block read (2)" )
		<< "Block of " << AliHLTLog::kDec << pbh->fLength << " bytes read (2)." << ENDLOG;
	    //DumpBlockHeader( fDbgLogLevel, "AliHLTShmCom::Read", "Block read (2)", pbh );
	    }
	else
	    {
	    pbh = (AliHLTBlockHeader*)p1;
	    LOG( fDbgLogLevel, "AliHLTShmCom::Read", "Block accessed" )
		<< "Block of " << AliHLTLog::kDec << pbh->fLength << " bytes accessed." << ENDLOG;
	    //DumpBlockHeader( fDbgLogLevel, "AliHLTShmCom::Read", "Block accessed", pbh );
	    }
	}
    return 0;
    }

int AliHLTShmCom::FreeRead( AliHLTBlockHeader* pbh, AliUInt8_t* p1, AliUInt8_t* p2, AliUInt32_t size1 )
    {
    if ( !pbh )
	return EFAULT;
    HaveRead( pbh->fLength, p1, p2, size1 );
    if ( p2 )
	{
	free( (AliUInt8_t*)pbh );
	}
    return 0;
    }

int AliHLTShmCom::CanRead( bool peek, AliUInt32_t size, AliUInt8_t*& p1, AliUInt8_t*& p2, 
			  AliUInt32_t& size1, bool block )
    {
    if ( size > fHeader->fSize )
	return ENOSPC;
    if ( !size )
	{
	p1 = p2 = NULL;
	size1 = 0;
	return 0;
	}
    bool doneLog = false;
    struct timeval t1, t2;
    AliUInt32_t avail;
    AliUInt32_t tdiff=0;
    AliUInt32_t cnt = 0;
    if ( !fShmPtr )
	return EFAULT;
    if ( fHeader->fWriteNdx >= fHeader->fReadNdx )
	avail = fHeader->fWriteNdx - fHeader->fReadNdx;
    else
	avail = fHeader->fSize - ( fHeader->fReadNdx - fHeader->fWriteNdx );
    if ( avail < size )
	{
	if ( !block )
	    gettimeofday( &t1, NULL );
	do
	    {
	    if ( fQuit )
		return ECANCELED;
	    if ( fHeader->fWriteNdx >= fHeader->fReadNdx )
		avail = fHeader->fWriteNdx - fHeader->fReadNdx;
	    else
		avail = fHeader->fSize - ( fHeader->fReadNdx - fHeader->fWriteNdx );
	    if ( avail >= size )
		break;
	    if ( cnt >= fRetryCount )
		{
		if ( !block )
		    {
		    gettimeofday( &t2, NULL );
		    tdiff = t2.tv_sec*1000;
		    tdiff -= t1.tv_sec*1000;
		    tdiff += t2.tv_usec/1000;
		    tdiff -= t1.tv_usec/1000;
		    if ( tdiff >= fTimeout )
			return ETIMEDOUT;
		    }
		if ( !doneLog )
		    {
#ifdef SHMCOM_DEBUG
		    if ( fStates.size()>fStateLimit )
			fStates.erase( fStates.begin() );
		    fStates.insert( fStates.end(), *fHeader );
#endif
		    LOG( fDbgLogLevel, "AliHLTShmCom::CanRead", "Shm Status" )
			<< "Data read attempt: size: " << AliHLTLog::kDec << size 
			<< " - WriteNdx: " << fHeader->fWriteNdx << " - ReadNdx: " << fHeader->fReadNdx
			<< " - FreeNdx: " << fHeader->fFreeNdx << " - Size: " << fHeader->fSize
			<< "." << ENDLOG;
		    doneLog = true;
		    }
		cnt = 0;
		usleep( SLEEPTIME_US );
		fSleepCount++;
		}
	    else
		cnt++;
	    }
	while ( avail < size );
	}
    if (  fHeader->fWriteNdx > fHeader->fReadNdx || fHeader->fReadNdx+size<=fHeader->fSize )
	{
	p1 = fData + fHeader->fReadNdx;
	size1 = size;
	p2 = NULL;
	if ( !peek )
	    {
	    AliUInt32_t tmp = fHeader->fReadNdx + size;
	    if ( tmp>fHeader->fSize )
		{
		LOG( AliHLTLog::kFatal, "AliHLTShmCom::CanRead", "Internal Error" )
		    << "Internal error, wrong size: " << AliHLTLog::kDec << size 
		    << " (0x" << AliHLTLog::kHex << size << ")." << ENDLOG;
		}
	    if ( tmp == fHeader->fSize )
		tmp = 0;
	    fHeader->fReadNdx = tmp;
	    }
	}
    else
	{
	p1 = fData + fHeader->fReadNdx;
	size1 = fHeader->fSize - fHeader->fReadNdx;
	p2 = fData;
	if ( !peek )
	    fHeader->fReadNdx = size-size1;
	}
#ifdef SHMCOM_DEBUG
    if ( fStates.size()>fStateLimit )
	fStates.erase( fStates.begin() );
    fStates.insert( fStates.end(), *fHeader );
#endif
    LOG( fDbgLogLevel, "AliHLTShmCom::CanRead", "Shm Status" )
	<< "Data " << (peek ? "peeked" : "read") << ": size: " << AliHLTLog::kDec << size << " - size1: " << size1
	<< " - WriteNdx: " << fHeader->fWriteNdx << " - ReadNdx: " << fHeader->fReadNdx
	<< " - FreeNdx: " << fHeader->fFreeNdx << " - Size: " << fHeader->fSize
	<< "." << ENDLOG;
    return 0;
    }

int AliHLTShmCom::HaveRead( AliUInt32_t size, AliUInt8_t*, AliUInt8_t* p2, AliUInt32_t size1 )
    {
    AliUInt32_t tmp;
    if ( !p2 )
	{
	tmp = fHeader->fFreeNdx + size;
	if ( tmp>fHeader->fSize )
	    {
	    LOG( AliHLTLog::kFatal, "AliHLTShmCom::HaveRead", "Internal Error" )
		<< "Internal error, wrong size: " << AliHLTLog::kDec << size 
		<< " (0x" << AliHLTLog::kHex << size << ")." << ENDLOG;
	    }
	if ( tmp == fHeader->fSize )
	    tmp = 0;
	fHeader->fFreeNdx = tmp;
	}
    else
	{
	tmp = size - size1;
	fHeader->fFreeNdx = tmp;
	}
#ifdef SHMCOM_DEBUG
    if ( fStates.size()>fStateLimit )
	fStates.erase( fStates.begin() );
    fStates.insert( fStates.end(), *fHeader );
#endif
    LOG( fDbgLogLevel, "AliHLTShmCom::HaveRead", "Shm Status" )
	<< "Data read: size: " << AliHLTLog::kDec << size << " - size1: " << size1
	<< " - WriteNdx: " << fHeader->fWriteNdx << " - ReadNdx: " << fHeader->fReadNdx
	<< " - FreeNdx: " << fHeader->fFreeNdx << " - Size: " << fHeader->fSize
	<< "." << ENDLOG;
    return 0;
    }


AliHLTShmCom::operator bool()
    {
    return (bool)fShmPtr;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
