/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSubscriberPipeProxy.hpp"
#include "AliHLTPublisherInterface.hpp"
#include "AliHLTEventTriggerStruct.h"
#include "AliHLTEventAccounting.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTStackTrace.hpp"
#ifdef PIPECOM_DOCRC
#include "CRC.hpp"
#endif
#include "MLUCLimitTimer.hpp"
#include <errno.h>


#define EVENT_CHECK_PARANOIA_LIMIT 8192


AliHLTSubscriberPipeProxy::AliHLTSubscriberPipeProxy( const char* name ):
    AliHLTSubscriberProxyInterface( name )
    {
    int ret;
    MLUCString p2sname, s2pname;
    s2pname = ALIL3PIPEBASEDIR;
    s2pname += GetName();
    char tmpUID[64];
    sprintf( tmpUID, "%d", (int)getuid() );
    s2pname += "-";
    s2pname += tmpUID;
    s2pname += "-";
    s2pname += "SubsToPubl";
    p2sname = ALIL3PIPEBASEDIR;
    p2sname += GetName();
    sprintf( tmpUID, "%d", (int)getuid() );
    p2sname += "-";
    p2sname += tmpUID;
    p2sname += "-";
    p2sname += "PublToSubs";
    ret = fPubSubCom.Open( s2pname.c_str(), p2sname.c_str() , false );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "Subscriner pipes open")
	    << "Unable to open pipes " 
	    << s2pname.c_str() << " and "
	    << p2sname.c_str() << " - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	}
    pthread_mutex_init( &fMutex, NULL );
    fQuitted = false;
    fQuit = false;
    fParBool = NULL;
    fParBoolSize = 0;
    fParCount = NULL;
    fParCountSize = 0;
    fParEvtID = NULL;
    fParEvtIDSize = 0;
    fHSETMETT = NULL;
    fHSETMETTSize = 0;
    fHSETMMod = NULL;
    fHSETMModSize = 0;
    fHEDSMEDD = NULL;
    fHEDSMEDDSize = 0;
    fParVect = NULL;
    fParVectSize = 0;
#ifdef PIPECOM_DOCRC
    InitCrc32( fCRCTable );
#endif
#ifdef EVENT_CHECK_PARANOIA
    fLastEventDoneID = ~(AliEventID_t)0;
    fLastEventAnnouncedID = ~(AliEventID_t)0;
#endif
#ifdef PIPE_PARANOIA
    fWriteCount = 0;
    fReadCount = 0;
#endif
    fEventAccounting = NULL;
    fProxyName = name;
    fProxyName += "-SubscriberProxy";
    }


AliHLTSubscriberPipeProxy::~AliHLTSubscriberPipeProxy()
    {
    pthread_mutex_destroy( &fMutex );
    if ( fParBool )
	delete [] (AliUInt8_t*)fParBool;
    if ( fParCount )
	delete [] (AliUInt8_t*)fParCount;
    if ( fParEvtID )
	delete [] (AliUInt8_t*)fParEvtID;
    if ( fHSETMETT )
	delete [] (AliUInt8_t*)fHSETMETT;
    if ( fHSETMMod )
	delete [] (AliUInt8_t*)fHSETMMod;
    if ( fHEDSMEDD )
	delete [] (AliUInt8_t*)fHEDSMEDD;
    if ( fParVect )
	delete [] (AliUInt8_t*)fParVect;
    while ( fHSETMETTs.size()>0 )
	{
	if ( fHSETMETTs[0].fETT )
	    delete [] (AliUInt8_t*)fHSETMETTs[0].fETT;
	fHSETMETTs.erase( fHSETMETTs.begin() );
	}
    while ( fHEDMMEDDs.size()>0 )
	{
	if ( fHEDMMEDDs[0].fEDD )
	    delete [] (AliUInt8_t*)fHEDMMEDDs[0].fEDD;
	fHEDMMEDDs.erase( fHEDMMEDDs.begin() );
	}
    }


int AliHLTSubscriberPipeProxy::NewEvent(  AliHLTPublisherInterface&, AliEventID_t eventID, 
					  const AliHLTSubEventDataDescriptor& sbevent,
					  const AliHLTEventTriggerStruct& ets )
    {
    MLUCLIMITTIMER( MLUCLIMITTIMER_DEFAULT_TIMER_LIMIT_US );
    if ( fEventAccounting )
	fEventAccounting->NewEvent( fProxyName.c_str(), &sbevent );
    LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy::NewEvent", "NewEvent" )
	<< AliHLTLog::kHex << GetName() << " susbcriber pipe proxy received event 0x" << eventID
	<< " (" << AliHLTLog::kDec << eventID << ") with " << sbevent.fDataBlockCount 
	<< " data blocks." << ENDLOG;
    if ( eventID != sbevent.fEventID )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy::NewEvent", "Incoherent Event ID" )
	    << "Msg event ID 0x" << AliHLTLog::kHex << eventID << " ("
	    << AliHLTLog::kDec << eventID << ") unequal sedd eventID: 0x"
	    << AliHLTLog::kHex << sbevent.fEventID << " ("
	    << AliHLTLog::kDec << sbevent.fEventID << ")." << ENDLOG;
	}
#ifdef EVENT_CHECK_PARANOIA
    if ( fLastEventAnnouncedID!=~(AliEventID_t)0 )
	{
	if ( fLastEventAnnouncedID+EVENT_CHECK_PARANOIA_LIMIT<eventID || (fLastEventAnnouncedID>EVENT_CHECK_PARANOIA_LIMIT && fLastEventAnnouncedID-EVENT_CHECK_PARANOIA_LIMIT>eventID) )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy::NewEvent", "Suspicious Event ID" )
		<< "Suspicious Event ID found: 0x" << AliHLTLog::kHex << eventID << " ("
		<< AliHLTLog::kDec << eventID << ") - last announced event ID: 0x"
		<< AliHLTLog::kHex << fLastEventAnnouncedID << " (" << AliHLTLog::kDec 
		<< fLastEventAnnouncedID << ")." << ENDLOG;
	    }
	}
    fLastEventAnnouncedID = eventID;
#endif

    AliUInt8_t msgData[ sizeof(AliHLTPubSubPipeMsg)+1 ];
#ifdef PIPECOM_DOCRC
    long crc = 0;
#endif
    AliUInt32_t cnt;
    pthread_mutex_lock( &fMutex );
    AliHLTPubSubPipeMsg* msg = (AliHLTPubSubPipeMsg*)msgData;
    MakeMsg( msg, AliHLTPubSubPipeMsg::kNewEvent, 3 );
    cnt = msg->fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif

    int ret;
    AliHLTPubSubPipeMsgParam param1, param2, param3;
    MakeParam( param1, eventID );
    cnt += param1.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&param1, fCRCTable );
#endif

    MakeParam( param2, sbevent );
    cnt += param2.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&param2, fCRCTable );
#endif
    cnt += sbevent.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&(sbevent.fHeader), fCRCTable );
#endif

    MakeParam( param3, ets );
    cnt += param3.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&param3, fCRCTable );
#endif
    cnt += ets.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&(ets.fHeader), fCRCTable );
#endif

#ifdef PIPECOM_DOCRC
    cnt += sizeof(long);
#endif

    AliHLTBlockHeader const* pbhs[6];
    pbhs[0] = &(msg->fHeader);
    pbhs[1] = &(param1.fHeader);
    pbhs[2] = &(param2.fHeader);
    pbhs[3] = &(sbevent.fHeader);
    pbhs[4] = &(param3.fHeader);
    pbhs[5] = &(ets.fHeader);

    ret = fPubSubCom.Write( 6, pbhs );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "NewEvent message send")
	    << "Error writing publisher message and parameters of size " << msg->fHeader.fLength
	    << "to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
#ifdef PIPECOM_DOCRC
    fPubSubCom.Write( sizeof(long), (AliUInt8_t*)&crc );
    //LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "NewEvent message CRC written")
    //<< "Wrote NewEvent CRC: " << AliHLTLog::kHex << crc << "." << ENDLOG;
#endif
#ifdef PIPE_PARANOIA
    fWriteCount += msg->fHeader.fLength+param1.fHeader.fLength+param2.fHeader.fLength+sbevent.fHeader.fLength+param3.fHeader.fLength+ets.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    fWriteCount += sizeof(long);
#endif
    if ( fWriteCount != fPubSubCom.GetWriteCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Write Count Inconsistency" )
	    << "Pipe communication object thinks it has written " << AliHLTLog::kDec 
	    << fPubSubCom.GetWriteCount() << " bytes, while I think it should have written "
	    << fWriteCount << " bytes." << ENDLOG;
	}
#endif
    LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "NewEvent message")
	<< "NewEvent successfully sent to subscriber " << GetName() << ENDLOG;

    pthread_mutex_unlock( &fMutex );
    return 0;
    }



int AliHLTSubscriberPipeProxy::EventCanceled(  AliHLTPublisherInterface&, AliEventID_t eventID )
    {
    AliUInt8_t msgData[ sizeof(AliHLTPubSubPipeMsg)+1 ];
#ifdef PIPECOM_DOCRC
    long crc = 0;
#endif
    AliUInt32_t cnt;
    pthread_mutex_lock( &fMutex );
    AliHLTPubSubPipeMsg* msg = (AliHLTPubSubPipeMsg*)msgData;
    MakeMsg( msg, AliHLTPubSubPipeMsg::kEventCanceled, 1 );
    cnt = msg->fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    int ret;
    AliHLTPubSubPipeMsgParam param;
    MakeParam( param, eventID );
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&param, fCRCTable );
#endif
    cnt += param.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    cnt += sizeof(long);
#endif

    AliHLTBlockHeader const* pbhs[2];
    pbhs[0] = &(msg->fHeader);
    pbhs[1] = &(param.fHeader);

    ret = fPubSubCom.Write( 2, pbhs );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "EventCanceled message send")
	    << "Error writing publisher message and parameters of size " << msg->fHeader.fLength
	    << "to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
#ifdef PIPECOM_DOCRC
    fPubSubCom.Write( sizeof(long), (AliUInt8_t*)&crc );
#endif
#ifdef PIPE_PARANOIA
    fWriteCount += msg->fHeader.fLength+param.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    fWriteCount += sizeof(long);
#endif
    if ( fWriteCount != fPubSubCom.GetWriteCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Write Count Inconsistency" )
	    << "Pipe communication object thinks it has written " << AliHLTLog::kDec 
	    << fPubSubCom.GetWriteCount() << " bytes, while I think it should have written "
	    << fWriteCount << " bytes." << ENDLOG;
	}
#endif
    LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "EventCanceled message")
	<< "EventCanceled successfully sent to subscriber " << GetName() << ENDLOG;
    pthread_mutex_unlock( &fMutex );
    return 0;
    }


int AliHLTSubscriberPipeProxy::EventDoneData( AliHLTPublisherInterface&, const AliHLTEventDoneData& eventDoneData )
    {
    MLUCLIMITTIMER( MLUCLIMITTIMER_DEFAULT_TIMER_LIMIT_US );
    if ( fEventAccounting )
	fEventAccounting->EventDoneData( fProxyName.c_str(), eventDoneData.fEventID, eventDoneData.fDataWordCount );
    AliUInt32_t cnt;
#ifdef PIPECOM_DOCRC
    long crc = 0;
#endif
    pthread_mutex_lock( &fMutex );
    LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "EventDoneData")
	<< "EventDoneData of " << AliHLTLog::kDec << eventDoneData.fHeader.fLength
	<< " bytes and " << eventDoneData.fDataWordCount << " data words received for event 0x"
	<< AliHLTLog::kHex << eventDoneData.fEventID << " (" << AliHLTLog::kDec
	<< eventDoneData.fEventID << ")." << ENDLOG;
    AliUInt8_t msgData[ sizeof(AliHLTPubSubPipeMsg)+1 ];
    AliHLTPubSubPipeMsg* msg = (AliHLTPubSubPipeMsg*)msgData;
    MakeMsg( msg, AliHLTPubSubPipeMsg::kEventDoneData, 1 );
    cnt = msg->fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif

    AliHLTPubSubPipeMsgParam param;
    MakeParam( param, eventDoneData );
    cnt += param.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&param, fCRCTable );
    cnt += sizeof(long);
#endif

    cnt += eventDoneData.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&eventDoneData, fCRCTable );
    cnt += sizeof(long);
#endif
    int ret;

    AliHLTBlockHeader const* pbhs[3];
    pbhs[0] = &(msg->fHeader);
    pbhs[1] = &(param.fHeader);
    pbhs[2] = &(eventDoneData.fHeader);
    ret = fPubSubCom.Write( 3, pbhs );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "EventDoneData message send")
	    << "Unable to write eventdonedata message and parameters to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}

#ifdef PIPECOM_DOCRC
    fPubSubCom.Write( sizeof(long), (AliUInt8_t*)&crc );
#endif
#ifdef PIPE_PARANOIA
    fWriteCount += msg->fHeader.fLength+param.fHeader.fLength+eventDoneData.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    fWriteCount += sizeof(long);
#endif
    if ( fWriteCount != fPubSubCom.GetWriteCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Write Count Inconsistency" )
	    << "Pipe communication object thinks it has written " << AliHLTLog::kDec 
	    << fPubSubCom.GetWriteCount() << " bytes, while I think it should have written "
	    << fWriteCount << " bytes." << ENDLOG;
	}
#endif
    LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "EventDoneData" )
	<< "EventDoneData message for event 0x" << AliHLTLog::kHex << eventDoneData.fEventID << " (" 
	<< AliHLTLog::kDec << eventDoneData.fEventID << ") successfuly sent to publisher " << GetName() << ENDLOG;
    pthread_mutex_unlock( &fMutex );
    return ret;
    }


int AliHLTSubscriberPipeProxy::SubscriptionCanceled( AliHLTPublisherInterface& )
    {
    AliUInt8_t msgData[ sizeof(AliHLTPubSubPipeMsg)+1 ];
#ifdef PIPECOM_DOCRC
    long crc = 0;
#endif
    //AliHLTStackTrace( "SusbcriptionCanceled", AliHLTLog::kError, 16 );
    pthread_mutex_lock( &fMutex );
    AliHLTPubSubPipeMsg* msg = (AliHLTPubSubPipeMsg*)msgData;
    MakeMsg( msg, AliHLTPubSubPipeMsg::kSubscriptionCanceled, 0 );
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    int ret;
    ret = fPubSubCom.Write( &(msg->fHeader) );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "SubscriptionCanceled message send")
	    << "Error writing publisher message of size " << msg->fHeader.fLength
	    << "to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	//fQuit = true;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    LOG( AliHLTLog::kInformational, "AliHLTSubscriberPipeProxy", "SubscriptionCanceled message")
	<< "SubscriptionCanceled successfully sent to subscriber " << GetName() << ENDLOG;
    //fQuit = true;
#ifdef PIPECOM_DOCRC
    fPubSubCom.Write( sizeof(long), (AliUInt8_t*)&crc );
#endif
#ifdef PIPE_PARANOIA
    fWriteCount += msg->fHeader.fLength;
#ifdef PIPECOM_DOCRC
    fWriteCount += sizeof(long);
#endif
    if ( fWriteCount != fPubSubCom.GetWriteCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Write Count Inconsistency" )
	    << "Pipe communication object thinks it has written " << AliHLTLog::kDec 
	    << fPubSubCom.GetWriteCount() << " bytes, while I think it should have written "
	    << fWriteCount << " bytes." << ENDLOG;
	}
#endif
    pthread_mutex_unlock( &fMutex );
    return 0;
    }

int AliHLTSubscriberPipeProxy::ReleaseEventsRequest( AliHLTPublisherInterface& )
    {
    AliUInt8_t msgData[ sizeof(AliHLTPubSubPipeMsg)+1 ];
#ifdef PIPECOM_DOCRC
    long crc = 0;
#endif
    pthread_mutex_lock( &fMutex );
    AliHLTPubSubPipeMsg* msg = (AliHLTPubSubPipeMsg*)msgData;
    MakeMsg( msg, AliHLTPubSubPipeMsg::kReleaseEventsRequest, 0 );
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    int ret;
    ret = fPubSubCom.Write( &(msg->fHeader) );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "ReleaseEventsRequest message send")
	    << "Error writing publisher message of size " << msg->fHeader.fLength
	    << "to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "ReleaseEventsRequest message")
	<< "ReleaseEventsRequest successfully sent to subscriber " << GetName() << ENDLOG;
#ifdef PIPECOM_DOCRC
    fPubSubCom.Write( sizeof(long), (AliUInt8_t*)&crc );
#endif
#ifdef PIPE_PARANOIA
    fWriteCount += msg->fHeader.fLength;
#ifdef PIPECOM_DOCRC
    fWriteCount += sizeof(long);
#endif
    if ( fWriteCount != fPubSubCom.GetWriteCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Write Count Inconsistency" )
	    << "Pipe communication object thinks it has written " << AliHLTLog::kDec 
	    << fPubSubCom.GetWriteCount() << " bytes, while I think it should have written "
	    << fWriteCount << " bytes." << ENDLOG;
	}
#endif
    pthread_mutex_unlock( &fMutex );
    return 0;
    }

int AliHLTSubscriberPipeProxy::Ping( AliHLTPublisherInterface& )
    {
    AliUInt8_t msgData[ sizeof(AliHLTPubSubPipeMsg)+1 ];
#ifdef PIPECOM_DOCRC
    long crc = 0;
#endif
    pthread_mutex_lock( &fMutex );
    AliHLTPubSubPipeMsg* msg = (AliHLTPubSubPipeMsg*)msgData;
    MakeMsg( msg, AliHLTPubSubPipeMsg::kPing, 0 );
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    int ret;
    ret = fPubSubCom.Write( &(msg->fHeader) );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "Ping message send")
	    << "Error writing publisher message of size " << msg->fHeader.fLength
	    << "to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "Ping message")
	<< "Ping successfully sent to subscriber " << GetName() << ENDLOG;
#ifdef PIPECOM_DOCRC
    fPubSubCom.Write( sizeof(long), (AliUInt8_t*)&crc );
#endif
#ifdef PIPE_PARANOIA
    fWriteCount += msg->fHeader.fLength;
#ifdef PIPECOM_DOCRC
    fWriteCount += sizeof(long);
#endif
    if ( fWriteCount != fPubSubCom.GetWriteCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Write Count Inconsistency" )
	    << "Pipe communication object thinks it has written " << AliHLTLog::kDec 
	    << fPubSubCom.GetWriteCount() << " bytes, while I think it should have written "
	    << fWriteCount << " bytes." << ENDLOG;
	}
#endif
    pthread_mutex_unlock( &fMutex );
    return 0;
    }

int AliHLTSubscriberPipeProxy::PingAck( AliHLTPublisherInterface& )
    {
    AliUInt8_t msgData[ sizeof(AliHLTPubSubPipeMsg)+1 ];
#ifdef PIPECOM_DOCRC
    long crc = 0;
#endif
    pthread_mutex_lock( &fMutex );
    AliHLTPubSubPipeMsg* msg = (AliHLTPubSubPipeMsg*)msgData;
    MakeMsg( msg, AliHLTPubSubPipeMsg::kPingAck, 0 );
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    int ret;
    ret = fPubSubCom.Write( &(msg->fHeader) );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "PingAck message send")
	    << "Error writing publisher message of size " << msg->fHeader.fLength
	    << "to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "PingAck message")
	<< "PingAck successfully sent to subscriber " << GetName() << ENDLOG;
#ifdef PIPECOM_DOCRC
    fPubSubCom.Write( sizeof(long), (AliUInt8_t*)&crc );
#endif
#ifdef PIPE_PARANOIA
    fWriteCount += msg->fHeader.fLength;
#ifdef PIPECOM_DOCRC
    fWriteCount += sizeof(long);
#endif
    if ( fWriteCount != fPubSubCom.GetWriteCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Write Count Inconsistency" )
	    << "Pipe communication object thinks it has written " << AliHLTLog::kDec 
	    << fPubSubCom.GetWriteCount() << " bytes, while I think it should have written "
	    << fWriteCount << " bytes." << ENDLOG;
	}
#endif
    pthread_mutex_unlock( &fMutex );
    return 0;
    }

void AliHLTSubscriberPipeProxy::SetTimeout( AliUInt32_t timeout_usec )
    {
    fPubSubCom.SetTimeout( timeout_usec );
    }


void AliHLTSubscriberPipeProxy::MakeMsg( AliHLTPubSubPipeMsg* msg, AliHLTPubSubPipeMsg::MsgType type,
					 AliUInt32_t paramCnt )
    {
    int sz = sizeof(AliHLTPubSubPipeMsg)+1;
    if ( !msg )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "Message NULL pointer")
	    << "Null pointer message passed for subscriber " << GetName() << ENDLOG;
	return;
	}
    // #define VALGRIND_PARANOIA
#ifdef VALGRIND_PARANOIA
    memset( msg, 0, sz );
#endif
    msg->fHeader.fLength = sz;
    msg->fHeader.fType.fID = ALIL3PUBSUBPIPEMSG_TYPE;
    msg->fHeader.fSubType.fID = 0;
    msg->fHeader.fVersion = 1;
    msg->fType = (AliUInt32_t)type;
    msg->fParamCnt = paramCnt;
    msg->fSubscriberName[0]=0;
    }

void AliHLTSubscriberPipeProxy::MakeParam( AliHLTPubSubPipeMsgParam& param, const AliHLTSubEventDataDescriptor& )
    {
    // #define VALGRIND_PARANOIA
#ifdef VALGRIND_PARANOIA
    memset( &param, 0, sizeof(AliHLTPubSubPipeMsgParam) );
#endif
    param.fHeader.fType.fID = ALIL3PUBSUBPIPEMSGPARAM_TYPE;
    param.fHeader.fSubType.fID = 0;
    param.fHeader.fVersion = 1;
    param.fHeader.fLength = sizeof(AliHLTPubSubPipeMsgParam);
    param.fType = AliHLTPubSubPipeMsgParam::kSEDD;
    param.fData = 0;
    }

void AliHLTSubscriberPipeProxy::MakeParam( AliHLTPubSubPipeMsgParam& param, const AliHLTEventTriggerStruct& )
    {
    // #define VALGRIND_PARANOIA
#ifdef VALGRIND_PARANOIA
    memset( &param, 0, sizeof(AliHLTPubSubPipeMsgParam) );
#endif
    param.fHeader.fType.fID = ALIL3PUBSUBPIPEMSGPARAM_TYPE;
    param.fHeader.fSubType.fID = 0;
    param.fHeader.fVersion = 1;
    param.fHeader.fLength = sizeof(AliHLTPubSubPipeMsgParam);
    param.fType = AliHLTPubSubPipeMsgParam::kETT;
    param.fData = 0;
    }

void AliHLTSubscriberPipeProxy::MakeParam( AliHLTPubSubPipeMsgParam& param, AliEventID_t val )
    {
    // #define VALGRIND_PARANOIA
#ifdef VALGRIND_PARANOIA
    memset( &param, 0, sizeof(AliHLTPubSubPipeMsgParam) );
#endif
    param.fHeader.fType.fID = ALIL3PUBSUBPIPEMSGPARAM_TYPE;
    param.fHeader.fSubType.fID = 0;
    param.fHeader.fVersion = 1;
    param.fHeader.fLength = sizeof(AliHLTPubSubPipeMsgParam);
    param.fType = AliHLTPubSubPipeMsgParam::kEvtID;
    param.fData = val.fNr;
    param.fDataType = val.fType;
    }

void AliHLTSubscriberPipeProxy::MakeParam( AliHLTPubSubPipeMsgParam& param, const AliHLTEventDoneData& )
    {
    // #define VALGRIND_PARANOIA
#ifdef VALGRIND_PARANOIA
    memset( &param, 0, sizeof(AliHLTPubSubPipeMsgParam) );
#endif
    param.fHeader.fType.fID = ALIL3PUBSUBPIPEMSGPARAM_TYPE;
    param.fHeader.fSubType.fID = 0;
    param.fHeader.fVersion = 1;
    param.fHeader.fLength = sizeof(AliHLTPubSubPipeMsgParam);
    param.fType = AliHLTPubSubPipeMsgParam::kEvtDoneD;
    param.fData = 0;
    }

int AliHLTSubscriberPipeProxy::MsgLoop( AliHLTPublisherInterface& interface )
    {
#ifdef PIPECOM_DOCRC
    long crcc, crcr;
#endif
    int ret;
    char name[9], turnedName[9];
    AliHLTBlockHeader *pbh, *pbhSave, sample = { 0, {ALIL3PUBSUBPIPEMSG_TYPE}, {0}, 1 };
    AliUInt32_t pbhSize;
    AliHLTPubSubPipeMsg *msg;
    fQuit = false;
    fQuitted = false;
    //#define USE_PIPE_TIMEOUT
#ifdef USE_PIPE_TIMEOUT
    struct timeval timeout;
#endif
    pbh = NULL;
    pbhSize = 0;
    do
	{
	pbhSave = pbh;
	// 	LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "Msg loop read")
	// 	    << "Entering read" << ENDLOG;
	// USE_PIPE_TIMEOUT is defined or not a few lines above
#ifdef USE_PIPE_TIMEOUT
	timeout.tv_sec = 0;
	timeout.tv_usec = 250000;
	ret = fPubSubCom.Read( pbh, false, &timeout );
#else
	ret = fPubSubCom.Read( pbh, true );
#endif
	msg = (AliHLTPubSubPipeMsg*)pbh;
	if ( ret && ret!=ETIMEDOUT )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "Msg loop read")
		<< "Error reading publisher (Subscriber " << GetName() << ") message from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	    return ret;
	    }
	// 	if ( ret==ETIMEDOUT )
	// 	    {
	// 	    LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "Msg loop read")
	// 		<< "Read returned with ETIMEDOUT" << ENDLOG;
	// 	    }
#ifdef PIPE_PARANOIA
	if ( !ret )
	    {
	    fReadCount += pbh->fLength;
	    if ( fReadCount != fPubSubCom.GetReadCount() )
		{
		LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
		    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
		    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
		    << fReadCount << " bytes." << ENDLOG;
		}
	    }
#endif
 	if ( pbhSave!=pbh )
	    {
	    pbhSize = pbh->fLength;
	    delete [] (AliUInt8_t*)pbhSave;
	    pbhSave = NULL;
	    }
 	if ( ret==ETIMEDOUT && fQuit )
 	    break;
	if ( ret==ETIMEDOUT && !fQuit )
 	    continue;
	if ( msg->fHeader.fType.fID != ALIL3PUBSUBPIPEMSG_TYPE )
	    {
	    strncpy( name, (const char*)msg->fHeader.fType.fDescr, 4 );
	    name[4] = 0;
	    turnedName[0] = name[3];
	    turnedName[1] = name[2];
	    turnedName[2] = name[1];
	    turnedName[3] = name[0];
	    turnedName[4] = 0;
	    LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "Msg loop", 100 )
		<< "Read unexpected datatype " << AliHLTLog::kHex << msg->fHeader.fType.fID
		<< " == " << name << " (" << turnedName << ")"
		<< " from pipe when expecting pipe message (" << sample.fType.fID << ")" << ENDLOG;
	    continue;
	    }
#ifdef PIPECOM_DOCRC
	crcc = 0;
#endif
	switch ( msg->fType )
	    {
	    case AliHLTPubSubPipeMsg::kUnsubscribe:
		{
		LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "Unsubscribe" )
		    << "Unsubscribe message received from subscriber" << ENDLOG;
		if ( strcmp( GetName(), (const char*)msg->fSubscriberName ) )
		    {
		    msg->fSubscriberName[ msg->fHeader.fLength-sizeof(AliHLTPubSubPipeMsg)-1 ] = 0;
		    LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "Unsubscribe subscriber name", 100 )
			<< "Subscriber name '" << (const char*)msg->fSubscriberName 
			<< "'passed to Unsubscribe message differs from subscriber proxy name '"
			<< GetName() << "'" << ENDLOG;
		    break;
		    }
#ifdef PIPECOM_DOCRC
		crcc = MakeCRC( crcc, (AliHLTBlockHeader*)msg, fCRCTable );
		fPubSubCom.Read( sizeof(long), (AliUInt8_t*)&crcr );
#ifdef PIPE_PARANOIA
		fReadCount += sizeof(long);
		if ( fReadCount != fPubSubCom.GetReadCount() )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
			<< "Pipe communication object thinks it has read " << AliHLTLog::kDec 
			<< fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
			<< fReadCount << " bytes." << ENDLOG;
		    }
#endif
		if ( crcr != crcc )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Unsubscribe CRC" )
			<< "CRC error when trying to read Unsubscribe message. Read CRC: "
			<< AliHLTLog::kHex << crcr << " - Calculated CRC: " << crcc << "." 
			<< ENDLOG;
		    }
#endif
		//printf( "%s:%d\n", __FILE__, __LINE__ );
		interface.Unsubscribe( *this );
		//printf( "%s:%d\n", __FILE__, __LINE__ );
		break;
		}
	    case AliHLTPubSubPipeMsg::kSetPersistent:
		{
		LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "SetPersistent" )
		    << "SetPersistent message received from subscriber " << GetName() << ENDLOG;
		ret = HandleSetPersistentMsg( interface, msg );
		if ( ret )
		    return ret;
		break;
		}
	    case AliHLTPubSubPipeMsg::kSetEventDoneDataSend:
		{
		LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "SetEventDoneDataSend" )
		    << "SetEventDoneDataSend message received from subscriber " << GetName() << ENDLOG;
		ret = HandleSetEventDoneDataSendMsg( interface, msg );
		if ( ret )
		    return ret;
		break;
		}
	    case AliHLTPubSubPipeMsg::kSetEventTypeMod:
		{
		LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "SetEventTypeMod" )
		    << "SetEventTypeMod message received from subscriber " << GetName() << ENDLOG;
		ret = HandleSetEventTypeModMsg( interface, msg );
		if ( ret )
		    return ret;
		break;
		}
	    case AliHLTPubSubPipeMsg::kSetEventTypeETT:
		{
		LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "SetEventTypeMod" )
		    << "SetEventTypeMod message received from subscriber " << GetName() << ENDLOG;
		ret = HandleSetEventTypeETTMsg( interface, msg );
		if ( ret )
		    return ret;
		break;
		}
	    case AliHLTPubSubPipeMsg::kSetTransientTimeout:
		{
		LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "SetTransientTimeout" )
		    << "SetTransientTimeout message received from subscriber " << GetName() << ENDLOG;
		ret = HandleSetTransientTimeoutMsg( interface, msg );
		if ( ret )
		    return ret;
		break;
		}
	    case AliHLTPubSubPipeMsg::kEventDoneSing:
		{
		LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "EventDoneSing" )
		    << "EventDoneSing message received from subscriber " << GetName() << ENDLOG;
		ret = HandleEventDoneSingMsg( interface, msg );
		if ( ret )
		    return ret;
		break;
		}
	    case AliHLTPubSubPipeMsg::kEventDoneMult:
		{
		LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "EventDoneMult" )
		    << "EventDoneMult message received from subscriber " << GetName() << ENDLOG;
		ret = HandleEventDoneMultMsg( interface, msg );
		if ( ret )
		    return ret;
		break;
		}
	    case AliHLTPubSubPipeMsg::kStartPublishing:
		{
		LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "StartPublishing" )
		    << "StartPublishing message received from subscriber " << GetName() << ENDLOG;
		bool sendOldEvents = false;
		ret = HandleStartPublishingMsg( interface, msg, sendOldEvents );
		interface.StartPublishing( *this, sendOldEvents );
		if ( ret )
		    return ret;
		break;
		}
	    case AliHLTPubSubPipeMsg::kPing:
		{
		LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "Ping" )
		    << "Ping message received from subscriber " << GetName() << ENDLOG;
#ifdef PIPECOM_DOCRC
		crcc = MakeCRC( crcc, (AliHLTBlockHeader*)msg, fCRCTable );
		fPubSubCom.Read( sizeof(long), (AliUInt8_t*)&crcr );
#ifdef PIPE_PARANOIA
		fReadCount += sizeof(long);
		if ( fReadCount != fPubSubCom.GetReadCount() )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
			<< "Pipe communication object thinks it has read " << AliHLTLog::kDec 
			<< fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
			<< fReadCount << " bytes." << ENDLOG;
		    }
#endif
		if ( crcr != crcc )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Ping CRC" )
			<< "CRC error when trying to read Ping message. Read CRC: "
			<< AliHLTLog::kHex << crcr << " - Calculated CRC: " << crcc << "." 
			<< ENDLOG;
		    }
#endif
		interface.Ping( *this );
		break;
		}
	    case AliHLTPubSubPipeMsg::kPingAck:
		{
		LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "PingAck" )
		    << "PingAck message received from subscriber " << GetName() << ENDLOG;
#ifdef PIPECOM_DOCRC
		crcc = MakeCRC( crcc, (AliHLTBlockHeader*)msg, fCRCTable );
		fPubSubCom.Read( sizeof(long), (AliUInt8_t*)&crcr );
#ifdef PIPE_PARANOIA
		fReadCount += sizeof(long);
		if ( fReadCount != fPubSubCom.GetReadCount() )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
			<< "Pipe communication object thinks it has read " << AliHLTLog::kDec 
			<< fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
			<< fReadCount << " bytes." << ENDLOG;
		    }
#endif
		if ( crcr != crcc )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "PingAck CRC" )
			<< "CRC error when trying to read PingAck message. Read CRC: "
			<< AliHLTLog::kHex << crcr << " - Calculated CRC: " << crcc << "." 
			<< ENDLOG;
		    }
#endif
		interface.PingAck( *this );
		break;
		}
	    case AliHLTPubSubPipeMsg::kQuitLoop:
		LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy::MsgLoop", "QuitLoop msg received" )
		    << "Received QuitLoop message." << ENDLOG;
		fQuit = true;
		break;
	    default:
		LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy::MsgLoop", "Unknown msg received" )
		    << "Received unknown message!" << ENDLOG;
		break;
	    }
	pbh->fLength = pbhSize;
	}
    while ( !fQuit );
    if ( pbh )
	delete [] (AliUInt8_t*)pbh;
    LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy::MsgLoop", "Loop ended" )
	<< "Msg loop ended." << ENDLOG;
    fQuitted = true;
    return 0;
    }


void AliHLTSubscriberPipeProxy::QuitMsgLoop( bool wait )
    {
    // This is called in direct succession from the msg loop
    //AliHLTStackTrace( "QuitMsgLoop", AliHLTLog::kError, 16 );
    fQuit = true;


    AliHLTPipeCom com;
    int ret;
    MLUCString s2pname;
    s2pname = fPubSubCom.GetReadPipeName();
    ret = com.OpenWrite( s2pname.c_str() );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy::QuitMsgLoop", "Quit pipe open" )
	    << "Error opening pipe " << s2pname.c_str()
	    << " for writing quit message to publisher proxy. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return;
	}
    int count = 0;
    bool doWait = true;
    while ( !fQuitted && count < 256 && doWait )
	{
	count++;
	AliHLTPubSubPipeMsg msg;
	msg.fHeader.fType.fID = ALIL3PUBSUBPIPEMSG_TYPE;
	msg.fHeader.fSubType.fID = 0;
	msg.fHeader.fVersion = 1;
	msg.fHeader.fLength = sizeof( AliHLTPubSubPipeMsg );
	msg.fType = AliHLTPubSubPipeMsg::kQuitLoop;
	msg.fParamCnt = 0;
	LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy::QuitMsgLoop", "Writing quit command" )
	    << "Writing quit command now." << ENDLOG;
	ret = com.Write( &(msg.fHeader), false, true );
	// 	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy::QuitMsgLoop", "Quit command written" )
	// 	    << "Quit command written." << ENDLOG;
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy::QuitMsgLoop", "Subscriber proxy quit message write" )
		<< "Unable to write publisher proxy quit message to pipe " 
		<< s2pname.c_str() 
		<< " Reason: " << strerror( ret ) << " (" << ret << ")"<< ENDLOG;
	    return;
	    }
	if ( !fQuitted && wait )
	    usleep( 50000 );
	// 	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy::QuitMsgLoop", "After usleep" )
	// 	    << "After usleep." << ENDLOG;
	doWait = wait;
	}
    com.Close();
    count=0;
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 2000000;
    gettimeofday( &start, NULL );
    while ( !fQuitted && deltaT<timeLimit && wait )
	{
	LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy::QuitMsgLoop", "Waiting for loop finish" ) 
	    << "Waiting for publisher proxy loop to finish... (Attempt " << count << ")." << ENDLOG;
	count++;
	usleep( 1000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}


    //     AliHLTPipeCom com;
    //     int ret;
    //     MLUCString s2pname;
    //     s2pname = ALIL3PIPEBASEDIR;
    //     s2pname += GetName();
    //     s2pname += "SubsToPubl";
    //     LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "opening pipe" )
    // 	<< "opening pipe " << s2pname.c_str()
    // 	<< " for writing quit message to subscriber proxy." << ENDLOG;
    //     ret = com.Open( s2pname.c_str() );
    //     if ( ret )
    // 	{
    // 	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "Quit pipe open" )
    // 	    << "Error opening pipe " << s2pname.c_str()
    // 	    << " for writing quit message to subscriber proxy. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
    // 	return;
    // 	}
    //     int count = 0;
    //     while ( !fQuitted && count < 256 )
    // 	{
    // 	count++;
    // 	AliHLTPubSubPipeMsg msg;
    // 	msg.fHeader.fType.fID = ALIL3PUBSUBPIPEMSG_TYPE;
    // 	msg.fHeader.fSubType.fID = 0;
    // 	msg.fHeader.fVersion = 1;
    // 	msg.fHeader.fLength = sizeof( AliHLTPubSubPipeMsg );
    // 	msg.fType = AliHLTPubSubPipeMsg::kQuitLoop;
    // 	msg.fParamCnt = 0;
    // 	LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy::QuitMsgLoop", "Writing quit command" )
    // 	    << "Writing quit command now." << ENDLOG;
    // 	ret = com.Write( &(msg.fHeader), false );
    // 	// 	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy::QuitMsgLoop", "Quit command written" )
    // 	// 	    << "Quit command written." << ENDLOG;
    // 	if ( ret )
    // 	    {
    // 	    LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "Subscriber proxy quit message write" )
    // 		<< "Unable to write subscriber proxy quit message to pipe " 
    // 		<< s2pname.c_str() 
    // 		<< " Reason: " << strerror( ret ) << " (" << ret << ")"<< ENDLOG;
    // 	    return;
    // 	    }
    // 	usleep( 50000 );
    // 	// 	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy::QuitMsgLoop", "After usleep" )
    // 	// 	    << "After usleep." << ENDLOG;
    // 	}
    //     com.Close();
    //     count = 0;
    //     while ( !fQuitted && count<256 )
    // 	{
    // 	LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "Waiting for loop finish" ) 
    // 	    << "Waiting for subscriber proxy loop to finish... (Attempt " << count << ")." << ENDLOG;
    // 	count++;
    // 	usleep( 1000 );
    // 	}



    //     LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy::QuitMsgLoop", "Done" )
    // 	<< "Finished making msg loop quit.." << ENDLOG;
    }


int AliHLTSubscriberPipeProxy::HandleSetPersistentMsg( AliHLTPublisherInterface& interface, AliHLTPubSubPipeMsg* msg )
    {
#ifdef PIPECOM_DOCRC
    long crcc = 0, crcr;
#endif
    int ret;
    char name[9], turnedName[9];
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    if ( msg->fParamCnt != 1 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "SetPersistent msg", 100 )
	    << "Unexpected parameter count for SetPersistent message: " << msg->fParamCnt
	    << " instead of 1" << ENDLOG;
	return 0;
	}
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    AliHLTBlockHeader *pbh;
    pbh = (AliHLTBlockHeader*)fParBool;
    ret = fPubSubCom.Read( pbh );
    //     parBool = (AliHLTPubSubPipeMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "SetPersistent msg parameter")
	    << "Error reading SetPersistent message bool parameter from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fParBool )
	{
	if ( fParBool )
	    delete [] (AliUInt8_t*)fParBool;
	fParBool = (AliHLTPubSubPipeMsgParam*)pbh;
	fParBoolSize = pbh->fLength;
	}
    else
	pbh->fLength = fParBoolSize;
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fParBool->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)fParBool->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "SetPersistent msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fParBool->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting pipe message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}
    if ( fParBool->fType != AliHLTPubSubPipeMsgParam::kBool )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "SetPersistent msg parameter type", 100 )
	    << "Unexpected message parameter type for SetPersistent message: " << AliHLTLog::kDec 
	    << (int)fParBool->fType << " when expecting " << (int)AliHLTPubSubPipeMsgParam::kBool << ENDLOG;
	return 0;
	}
#ifdef PIPECOM_DOCRC
    fPubSubCom.Read( sizeof(long), (AliUInt8_t*)&crcr );
#ifdef PIPE_PARANOIA
    fReadCount += sizeof(long);
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( crcr != crcc )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "SetPersistent msg CRC" )
	    << "CRC error when trying to read SetPersistent message. Read CRC: "
	    << AliHLTLog::kHex << crcr << " - Calculated CRC: " << crcc << "." 
	    << ENDLOG;
	}
#endif

    interface.SetPersistent( *this, (bool)fParBool->fData );
    return 0;
    }


int AliHLTSubscriberPipeProxy::HandleSetEventDoneDataSendMsg( AliHLTPublisherInterface& interface, AliHLTPubSubPipeMsg* msg )
    {
#ifdef PIPECOM_DOCRC
    long crcc = 0, crcr;
#endif
    int ret;
    char name[9], turnedName[9];
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    if ( msg->fParamCnt != 1 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "SetEventDoneDataSend msg", 100 )
	    << "Unexpected parameter count for SetEventDoneDataSend message: " << msg->fParamCnt
	    << " instead of 1" << ENDLOG;
	return 0;
	}
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    AliHLTBlockHeader *pbh;
    pbh = (AliHLTBlockHeader*)fParBool;
    ret = fPubSubCom.Read( pbh );
    //     parBool = (AliHLTPubSubPipeMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "SetEventDoneDataSend msg parameter")
	    << "Error reading SetEventDoneDataSend message bool parameter from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fParBool )
	{
	if ( fParBool )
	    delete [] (AliUInt8_t*)fParBool;
	fParBool = (AliHLTPubSubPipeMsgParam*)pbh;
	fParBoolSize = pbh->fLength;
	}
    else
	pbh->fLength = fParBoolSize;
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fParBool->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)fParBool->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "SetEventDoneDataSend msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fParBool->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting pipe message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}
    if ( fParBool->fType != AliHLTPubSubPipeMsgParam::kBool )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "SetEventDoneDataSend msg parameter type", 100 )
	    << "Unexpected message parameter type for SetEventDoneDataSend message: " << AliHLTLog::kDec 
	    << (int)fParBool->fType << " when expecting " << (int)AliHLTPubSubPipeMsgParam::kBool << ENDLOG;
	return 0;
	}
#ifdef PIPECOM_DOCRC
    fPubSubCom.Read( sizeof(long), (AliUInt8_t*)&crcr );
#ifdef PIPE_PARANOIA
    fReadCount += sizeof(long);
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( crcr != crcc )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "SetEventDoneDataSend msg CRC" )
	    << "CRC error when trying to read SetEventDoneDataSend message. Read CRC: "
	    << AliHLTLog::kHex << crcr << " - Calculated CRC: " << crcc << "." 
	    << ENDLOG;
	}
#endif

    interface.SetEventDoneDataSend( *this, (bool)fParBool->fData );
    return 0;
    }


int AliHLTSubscriberPipeProxy::HandleSetEventTypeModMsg( AliHLTPublisherInterface& interface, AliHLTPubSubPipeMsg* msg )
    {
#ifdef PIPECOM_DOCRC
    long crcc = 0, crcr;
#endif
    int ret;
    char name[9], turnedName[9];
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    if ( msg->fParamCnt != 1 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "SetEventTypeMod msg", 100 )
	    << "Unexpected parameter count for SetEventTypeMod message: " << msg->fParamCnt
	    << " instead of 1" << ENDLOG;
	return 0;
	}
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    AliHLTBlockHeader *pbh;
    pbh = (AliHLTBlockHeader*)fParCount;
    ret = fPubSubCom.Read( pbh );
    //     parCount = (AliHLTPubSubPipeMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "SetEventTypeMod msg parameter")
	    << "Error reading SetEventTypeMod message count parameter from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fParCount )
	{
	if ( fParCount )
	    delete [] (AliUInt8_t*)fParCount;
	fParCount = (AliHLTPubSubPipeMsgParam*)pbh;
	fParCountSize = pbh->fLength;
	}
    else
	pbh->fLength = fParCountSize;
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fParCount->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)fParCount->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "SetEventTypeMod msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fParCount->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting pipe message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}
    if ( fParCount->fType != AliHLTPubSubPipeMsgParam::kUInt32 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "SetEventTypeMod msg parameter type", 100 )
	    << "Unexpected message parameter type for SetEventTypeMod message: " << AliHLTLog::kDec 
	    << (int)fParCount->fType << " when expecting " << (int)AliHLTPubSubPipeMsgParam::kUInt32 << ENDLOG;
	return 0;
	}
#ifdef PIPECOM_DOCRC
    fPubSubCom.Read( sizeof(long), (AliUInt8_t*)&crcr );
#ifdef PIPE_PARANOIA
    fReadCount += sizeof(long);
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( crcr != crcc )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "SetEventTypeMod msg CRC" )
	    << "CRC error when trying to read SetEventTypeMod message. Read CRC: "
	    << AliHLTLog::kHex << crcr << " - Calculated CRC: " << crcc << "." 
	    << ENDLOG;
	}
#endif
    
    interface.SetEventType( *this, (AliUInt32_t)fParCount->fData );
    return 0;
    }

int AliHLTSubscriberPipeProxy::HandleSetEventTypeETTMsg( AliHLTPublisherInterface& interface, AliHLTPubSubPipeMsg* msg )
    {
#ifdef PIPECOM_DOCRC
    long crcc = 0, crcr;
#endif
    int ret;
    char name[9], turnedName[9];
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    if ( msg->fParamCnt != 2 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "SetEventTypeETT msg", 100 )
	    << "Unexpected parameter count for SetEventTypeETT message: " << msg->fParamCnt
	    << " instead of 2" << ENDLOG;
	return 0;
	}
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    AliHLTBlockHeader *pbh;
    pbh = (AliHLTBlockHeader*)fHSETMETT;
    ret = fPubSubCom.Read( pbh );
    //     parETT = (AliHLTPubSubPipeMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "SetEventTypeETT msg parameter")
	    << "Error reading SetEventTypeETT message vector parameter from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fHSETMETT )
	{
	if ( fHSETMETT )
	    delete [] (AliUInt8_t*)fHSETMETT;
	fHSETMETT = (AliHLTPubSubPipeMsgParam*)pbh;
	fHSETMETTSize = pbh->fLength;
	}
    else
	pbh->fLength = fHSETMETTSize;
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fHSETMETT->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)fHSETMETT->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "SetEventTypeETT msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fHSETMETT->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting pipe message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}
    if ( fHSETMETT->fType != AliHLTPubSubPipeMsgParam::kVectorETT )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "SetEventTypeETT msg parameter type", 100 )
	    << "Unexpected message parameter type for SetEventTypeETT message: " << AliHLTLog::kDec 
	    << (int)fHSETMETT->fType << " when expecting " << (int)AliHLTPubSubPipeMsgParam::kVectorETT << ENDLOG;
	return 0;
	}
    AliUInt32_t i, n = (AliUInt32_t)fHSETMETT->fData;
    THSETMETTData ettData;
    ettData.fETT = NULL;
    ettData.fSize = 0;
    fHSETMETTs.reserve( n );
    while ( fHSETMETTs.size()<n )
	fHSETMETTs.insert( fHSETMETTs.end(), ettData );
    vector<AliHLTEventTriggerStruct*> triggerWords;
    triggerWords.reserve( n );
    for ( i = 0; i < n; i++ )
	{
	pbh = (AliHLTBlockHeader*)fHSETMETTs[i].fETT;
	if ( pbh )
	    pbh->fLength = fHSETMETTs[i].fSize;
	ret = fPubSubCom.Read( pbh );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "SetEventTypeETT msg parameter data")
		<< "Error reading SetEventTypeETT msg parameter data " << i << " from pipe Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	    return ret;
	    }
#ifdef PIPE_PARANOIA
	fReadCount += pbh->fLength;
	if ( fReadCount != fPubSubCom.GetReadCount() )
	    {
	    LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
		<< "Pipe communication object thinks it has read " << AliHLTLog::kDec 
		<< fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
		<< fReadCount << " bytes." << ENDLOG;
	    }
#endif
	if ( pbh != (AliHLTBlockHeader*)fHSETMETTs[i].fETT )
	    {
	    if ( fHSETMETTs[i].fETT )
		delete [] (AliUInt8_t*)fHSETMETTs[i].fETT;
	    fHSETMETTs[i].fETT = (AliHLTEventTriggerStruct*)pbh;
	    fHSETMETTs[i].fSize = pbh->fLength;
	    }

#ifdef PIPECOM_DOCRC
	crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
	// 	fHSETMETTs[i].fETT = (AliHLTEventTriggerStruct*)pbh;
	if ( fHSETMETTs[i].fETT->fHeader.fType.fID != ALIL3EVENTTRIGGERSTRUCT_TYPE )
	    {
	    strncpy( name, (char*)fHSETMETT->fHeader.fType.fDescr, 4 );
	    sample.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
	    name[4] = 0;
	    turnedName[0] = name[3];
	    turnedName[1] = name[2];
	    turnedName[2] = name[1];
	    turnedName[3] = name[0];
	    turnedName[4] = 0;
	
	    LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "SetEventTypeETT msg parameter", 100 )
		<< "Read unexpected datatype " << AliHLTLog::kHex << fHSETMETT->fHeader.fType.fID
		<< " == " << name << " (" << turnedName << ")"
		<< " from pipe when expecting pipe message parameter 0 (" 
		<< sample.fType.fID << ")" << ENDLOG;
	    return 0;
	    }
	triggerWords.insert( triggerWords.end(), fHSETMETTs[i].fETT );
	}
    pbh = (AliHLTBlockHeader*)fHSETMMod;
    ret = fPubSubCom.Read( pbh );
    //     parMod = (AliHLTPubSubPipeMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "SetEventTypeETT msg parameter")
	    << "Error reading SetEventTypeETT message modulo parameter from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fHSETMMod )
	{
	if ( fHSETMMod )
	    delete [] (AliUInt8_t*)fHSETMMod;
	fHSETMMod = (AliHLTPubSubPipeMsgParam*)pbh;
	fHSETMModSize = pbh->fLength;
	}
    else
	pbh->fLength = fHSETMModSize;
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fHSETMMod->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)fHSETMMod->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "SetEventTypeETT msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fHSETMMod->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting pipe message parameter 1 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}
    //if ( fHSETMMod->fType != AliHLTPubSubPipeMsgParam::kVectorETT )
    if ( fHSETMMod->fType != AliHLTPubSubPipeMsgParam::kUInt32 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "SetEventTypeETT msg parameter type", 100 )
	    << "Unexpected message parameter type for SetEventTypeETT message: " << AliHLTLog::kDec 
	    << (int)fHSETMMod->fType << " when expecting " << (int)AliHLTPubSubPipeMsgParam::kUInt32 << ENDLOG;
	return 0;
	}
#ifdef PIPECOM_DOCRC
    fPubSubCom.Read( sizeof(long), (AliUInt8_t*)&crcr );
#ifdef PIPE_PARANOIA
    fReadCount += sizeof(long);
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( crcr != crcc )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "SetEventTypeETT msg CRC" )
	    << "CRC error when trying to read SetEventTypeETT message. Read CRC: "
	    << AliHLTLog::kHex << crcr << " - Calculated CRC: " << crcc << "." 
	    << ENDLOG;
	}
#endif
    interface.SetEventType( *this, triggerWords,(AliUInt32_t)fHSETMMod->fData );
    return 0;
    }


int AliHLTSubscriberPipeProxy::HandleSetTransientTimeoutMsg( AliHLTPublisherInterface& interface, AliHLTPubSubPipeMsg* msg )
    {
#ifdef PIPECOM_DOCRC
    long crcc = 0, crcr;
#endif
    int ret;
    char name[9], turnedName[9];
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    if ( msg->fParamCnt != 1 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "SetTransientTimeout msg", 100 )
	    << "Unexpected parameter count for SetTransientTimeout message: " << msg->fParamCnt
	    << " instead of 1" << ENDLOG;
	return 0;
	}
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    AliHLTBlockHeader *pbh;
    pbh = (AliHLTBlockHeader*)fParCount;
    ret = fPubSubCom.Read( pbh );
    //     parCount = (AliHLTPubSubPipeMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "SetTransientTimeout msg parameter")
	    << "Error reading SetTransientTimeout message count parameter from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fParCount )
	{
	if ( fParCount )
	    delete [] (AliUInt8_t*)fParCount;
	fParCount = (AliHLTPubSubPipeMsgParam*)pbh;
	fParCountSize = pbh->fLength;
	}
    else
	pbh->fLength = fParCountSize;
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fParCount->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)fParCount->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "SetTransientTimeout msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fParCount->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting pipe message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}
    if ( fParCount->fType != AliHLTPubSubPipeMsgParam::kUInt32 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "SetTransientTimeout msg parameter type", 100 )
	    << "Unexpected message parameter type for SetTransientTimeout message: " << AliHLTLog::kDec 
	    << (int)fParCount->fType << " when expecting " << (int)AliHLTPubSubPipeMsgParam::kUInt32 << ENDLOG;
	return 0;
	}
#ifdef PIPECOM_DOCRC
    fPubSubCom.Read( sizeof(long), (AliUInt8_t*)&crcr );
#ifdef PIPE_PARANOIA
    fReadCount += sizeof(long);
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( crcr != crcc )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "SetTransientTimeout msg CRC" )
	    << "CRC error when trying to read SetTransientTimeout message. Read CRC: "
	    << AliHLTLog::kHex << crcr << " - Calculated CRC: " << crcc << "." 
	    << ENDLOG;
	}
#endif
    
    interface.SetTransientTimeout( *this, (AliUInt32_t)fParCount->fData );
    return 0;
    }



int AliHLTSubscriberPipeProxy::HandleEventDoneSingMsg( AliHLTPublisherInterface& interface, AliHLTPubSubPipeMsg* msg )
    {
    MLUCLIMITTIMER_NAME( timer0, MLUCLIMITTIMER_DEFAULT_TIMER_LIMIT_US );
    MLUCLIMITTIMER_NAME( timer1, MLUCLIMITTIMER_DEFAULT_TIMER_LIMIT_US );
#ifdef PIPECOM_DOCRC
    long crcc = 0, crcr;
#endif
    int ret;
    char name[9], turnedName[9];
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    if ( msg->fParamCnt != 3 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "EventDoneSing msg", 100 )
	    << "Unexpected parameter count for EventDoneSing message: " << msg->fParamCnt
	    << " instead of 3" << ENDLOG;
	return 0;
	}
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    AliHLTBlockHeader *pbh;
    pbh = (AliHLTBlockHeader*)fParEvtID;
    ret = fPubSubCom.Read( pbh );
    //     parEvtID = (AliHLTPubSubPipeMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "EventDoneSing msg parameter")
	    << "Error reading EventDoneSing message primary parameter from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fParEvtID )
	{
	if ( fParEvtID )
	    delete [] (AliUInt8_t*)fParEvtID;
	fParEvtID = (AliHLTPubSubPipeMsgParam*)pbh;
	fParEvtIDSize = pbh->fLength;
	}
    else
	pbh->fLength = fParEvtIDSize;
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fParEvtID->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)fParEvtID->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "EventDoneSing msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fParEvtID->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting pipe message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}
    if ( fParEvtID->fType != AliHLTPubSubPipeMsgParam::kEvtDoneD )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "EventDoneSing msg parameter type", 100 )
	    << "Unexpected message parameter type for EventDoneSing message: " << AliHLTLog::kDec 
	    << (int)fParEvtID->fType << " when expecting " << (int)AliHLTPubSubPipeMsgParam::kEvtDoneD << ENDLOG;
	return 0;
	}


    pbh = (AliHLTBlockHeader*)fParBool;
    ret = fPubSubCom.Read( pbh );
    //     parEvtID = (AliHLTPubSubPipeMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "EventDoneSing msg parameter")
	    << "Error reading EventDoneSing message bool parameter 0 from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fParBool )
	{
	if ( fParBool )
	    delete [] (AliUInt8_t*)fParBool;
	fParBool = (AliHLTPubSubPipeMsgParam*)pbh;
	fParBoolSize = pbh->fLength;
	}
    else
	pbh->fLength = fParBoolSize;
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fParBool->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)fParBool->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "EventDoneSing msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fParBool->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting pipe message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}
    if ( fParBool->fType != AliHLTPubSubPipeMsgParam::kBool )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "EventDoneSing msg parameter type", 100 )
	    << "Unexpected message parameter type for EventDoneSing message: " << AliHLTLog::kDec 
	    << (int)fParBool->fType << " when expecting " << (int)AliHLTPubSubPipeMsgParam::kBool << ENDLOG;
	return 0;
	}
    bool forceForward = (bool)(fParBool->fData);


    pbh = (AliHLTBlockHeader*)fParBool;
    ret = fPubSubCom.Read( pbh );
    //     parEvtID = (AliHLTPubSubPipeMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "EventDoneSing msg parameter")
	    << "Error reading EventDoneSing message bool parameter 1 from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fParBool )
	{
	if ( fParBool )
	    delete [] (AliUInt8_t*)fParBool;
	fParBool = (AliHLTPubSubPipeMsgParam*)pbh;
	fParBoolSize = pbh->fLength;
	}
    else
	pbh->fLength = fParBoolSize;
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fParBool->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)fParBool->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "EventDoneSing msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fParBool->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting pipe message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}
    if ( fParBool->fType != AliHLTPubSubPipeMsgParam::kBool )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "EventDoneSing msg parameter type", 100 )
	    << "Unexpected message parameter type for EventDoneSing message: " << AliHLTLog::kDec 
	    << (int)fParBool->fType << " when expecting " << (int)AliHLTPubSubPipeMsgParam::kBool << ENDLOG;
	return 0;
	}
    bool forwarded = (bool)(fParBool->fData);


    pbh = (AliHLTBlockHeader*)fHEDSMEDD;
    if ( pbh )
	pbh->fLength = fHEDSMEDDSize;
    ret = fPubSubCom.Read( pbh );
    //     edd = (AliHLTEventDoneData*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "EventDoneSing msg parameter")
	    << "Error reading EventDoneSing message data from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif

    if ( pbh != (AliHLTBlockHeader*)fHEDSMEDD )
	{
	if ( fHEDSMEDD )
	    delete [] (AliUInt8_t*)fHEDSMEDD;
	fHEDSMEDD = (AliHLTEventDoneData*)pbh;
	fHEDSMEDDSize = pbh->fLength;
	}
	
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif

    if ( fHEDSMEDD->fHeader.fType.fID != ALIL3EVENTDONEDATA_TYPE )
	{
	sample.fType.fID = ALIL3EVENTDONEDATA_TYPE;
	strncpy( name, (char*)fHEDSMEDD->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "EventDoneSing msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fHEDSMEDD->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting event done data (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}

#ifdef PIPECOM_DOCRC
    fPubSubCom.Read( sizeof(long), (AliUInt8_t*)&crcr );
#ifdef PIPE_PARANOIA
    fReadCount += sizeof(long);
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( crcr != crcc )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "EventDoneSing msg CRC" )
	    << "CRC error when trying to read EventDoneSing message. Read CRC: "
	    << AliHLTLog::kHex << crcr << " - Calculated CRC: " << crcc << "." 
	    << ENDLOG;
	}
#endif

    LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy", "EventDoneSing msg" )
	<< "EventDoneSing event: 0x" << AliHLTLog::kHex << (AliEventID_t)fHEDSMEDD->fEventID << " (" 
	<< AliHLTLog::kDec << (AliEventID_t)fHEDSMEDD->fEventID << ") (forceForward: " << (forceForward ? "yes" : "no") << " - forwarded: "
	<< (forwarded ? "yes" : "no") << ")." << ENDLOG;

#ifdef EVENT_CHECK_PARANOIA
    if ( fLastEventDoneID!=~(AliEventID_t)0 )
	{
	if ( fLastEventDoneID+EVENT_CHECK_PARANOIA_LIMIT<(AliEventID_t)fHEDSMEDD->fEventID || (fLastEventDoneID>EVENT_CHECK_PARANOIA_LIMIT && fLastEventDoneID-EVENT_CHECK_PARANOIA_LIMIT>(AliEventID_t)fHEDSMEDD->fEventID) )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy::HandleEventDoneSingMsg", "Suspicious Event ID" )
		<< "Suspicious Event ID found: 0x" << AliHLTLog::kHex << (AliEventID_t)fHEDSMEDD->fEventID << " ("
		<< AliHLTLog::kDec << (AliEventID_t)fHEDSMEDD->fEventID << ") - last done event ID: 0x"
		<< AliHLTLog::kHex << fLastEventDoneID << " (" << AliHLTLog::kDec 
		<< fLastEventDoneID << ")." << ENDLOG;
	    }
	}
    fLastEventDoneID = (AliEventID_t)fHEDSMEDD->fEventID;
#endif

    MLUCLIMITTIMER_NAME_STOP( timer1 );
    
    if ( fEventAccounting )
	fEventAccounting->EventFinished( fProxyName.c_str(), fHEDSMEDD->fEventID, forceForward ? 2 : 1, forwarded ? 2 : 1 );

    interface.EventDone( *this, *fHEDSMEDD, forceForward, forwarded );
    return 0;
    }


int AliHLTSubscriberPipeProxy::HandleEventDoneMultMsg( AliHLTPublisherInterface& interface, AliHLTPubSubPipeMsg* msg )
    {
#ifdef PIPECOM_DOCRC
    long crcc = 0, crcr;
#endif
    int ret;
    char name[9], turnedName[9];
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    if ( msg->fParamCnt != 3 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "EventDoneMult msg", 100 )
	    << "Unexpected parameter count for EventDoneMult message: " << msg->fParamCnt
	    << " instead of 3" << ENDLOG;
	return 0;
	}
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    AliHLTBlockHeader *pbh;


    pbh = (AliHLTBlockHeader*)fParBool;
    ret = fPubSubCom.Read( pbh );
    //     parEvtID = (AliHLTPubSubPipeMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "EventDoneMult msg parameter")
	    << "Error reading EventDoneMult message bool parameter 0 from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fParBool )
	{
	if ( fParBool )
	    delete [] (AliUInt8_t*)fParBool;
	fParBool = (AliHLTPubSubPipeMsgParam*)pbh;
	fParBoolSize = pbh->fLength;
	}
    else
	pbh->fLength = fParBoolSize;
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fParBool->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)fParBool->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "EventDoneMult msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fParBool->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting pipe message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}
    if ( fParBool->fType != AliHLTPubSubPipeMsgParam::kBool )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "EventDoneMult msg parameter type", 100 )
	    << "Unexpected message parameter type for EventDoneMult message: " << AliHLTLog::kDec 
	    << (int)fParBool->fType << " when expecting " << (int)AliHLTPubSubPipeMsgParam::kBool << ENDLOG;
	return 0;
	}
    bool forceForward = (bool)(fParBool->fData);


    pbh = (AliHLTBlockHeader*)fParBool;
    ret = fPubSubCom.Read( pbh );
    //     parEvtID = (AliHLTPubSubPipeMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "EventDoneMult msg parameter")
	    << "Error reading EventDoneMult message bool parameter 1 from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fParBool )
	{
	if ( fParBool )
	    delete [] (AliUInt8_t*)fParBool;
	fParBool = (AliHLTPubSubPipeMsgParam*)pbh;
	fParBoolSize = pbh->fLength;
	}
    else
	pbh->fLength = fParBoolSize;
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fParBool->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)fParBool->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "EventDoneMult msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fParBool->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting pipe message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}
    if ( fParBool->fType != AliHLTPubSubPipeMsgParam::kBool )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "EventDoneMult msg parameter type", 100 )
	    << "Unexpected message parameter type for EventDoneMult message: " << AliHLTLog::kDec 
	    << (int)fParBool->fType << " when expecting " << (int)AliHLTPubSubPipeMsgParam::kBool << ENDLOG;
	return 0;
	}
    bool forwarded = (bool)(fParBool->fData);


    pbh = (AliHLTBlockHeader*)fParVect;
    ret = fPubSubCom.Read( pbh );
    //     parVect = (AliHLTPubSubPipeMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "EventDoneMult msg parameter")
	    << "Error reading EventDoneMult message vector parameter from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fParVect )
	{
	if ( fParVect )
	    delete [] (AliUInt8_t*)fParVect;
	fParVect = (AliHLTPubSubPipeMsgParam*)pbh;
	fParVectSize = pbh->fLength;
	}
    else
	pbh->fLength = fParVectSize;
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fParVect->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)fParVect->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "EventDoneMult msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fParVect->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting pipe message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}
    if ( fParVect->fType != AliHLTPubSubPipeMsgParam::kVectorEvtDoneD )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "EventDoneMult msg parameter type", 100 )
	    << "Unexpected message parameter type for EventDoneMult message: " << AliHLTLog::kDec 
	    << (int)fParVect->fType << " when expecting " << (int)AliHLTPubSubPipeMsgParam::kVectorEvtDoneD << ENDLOG;
	return 0;
	}
    AliUInt32_t i, n = fParVect->fData;
    THEDMMEDDData eddData;
    eddData.fEDD = NULL;
    eddData.fSize = 0;
    fHEDMMEDDs.reserve( n );
    while ( fHEDMMEDDs.size()<n )
	fHEDMMEDDs.insert( fHEDMMEDDs.end(), eddData );
    AliHLTBlockHeader* blh;
    vector<AliHLTEventDoneData*> eventDoneData( n );
    sample.fType.fID = ALIL3EVENTDONEDATA_TYPE;
    for ( i = 0; i < n; i++ )
	{
	blh = (AliHLTBlockHeader*)fHEDMMEDDs[i].fEDD;
	if ( blh )
	    blh->fLength = fHEDMMEDDs[i].fSize;
	ret = fPubSubCom.Read( blh );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "EventDoneMult msg parameter data")
		<< "Error reading EventDoneMult message vector parameter element " << AliHLTLog::kDec
		<< i << " of " << n << " data from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	    return ret;
	    }
#ifdef PIPE_PARANOIA
	fReadCount += blh->fLength;
	if ( fReadCount != fPubSubCom.GetReadCount() )
	    {
	    LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
		<< "Pipe communication object thinks it has read " << AliHLTLog::kDec 
		<< fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
		<< fReadCount << " bytes." << ENDLOG;
	    }
#endif
	if ( blh != (AliHLTBlockHeader*)fHEDMMEDDs[i].fEDD )
	    {
	    if ( fHEDMMEDDs[i].fEDD )
		delete [] (AliUInt8_t*)fHEDMMEDDs[i].fEDD;
	    fHEDMMEDDs[i].fEDD = (AliHLTEventDoneData*)blh;
	    fHEDMMEDDs[i].fSize = blh->fLength;
	    }
	
#ifdef PIPECOM_DOCRC
	crcc = MakeCRC( crcc, (AliHLTBlockHeader*)blh, fCRCTable );
#endif
	if ( blh->fType.fID != ALIL3EVENTDONEDATA_TYPE )
	    {
	    strncpy( name, (char*)blh->fType.fDescr, 4 );
	    name[4] = 0;
	    turnedName[0] = name[3];
	    turnedName[1] = name[2];
	    turnedName[2] = name[1];
	    turnedName[3] = name[0];
	    turnedName[4] = 0;
	    
	    LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "EventDoneMult msg parameter data", 100 )
		<< "Read unexpected datatype " << AliHLTLog::kHex << blh->fType.fID
		<< " == " << name << " (" << turnedName << ")"
		<< " from pipe when expecting pipe message vector element parameter "
		<< AliHLTLog::kDec << i << " of " << n << " data (" 
		<< sample.fType.fID << ")" << ENDLOG;
	    return EIO;
	    }
	eventDoneData[i] = (AliHLTEventDoneData*)blh;
	}


#ifdef PIPECOM_DOCRC
    fPubSubCom.Read( sizeof(long), (AliUInt8_t*)&crcr );
#ifdef PIPE_PARANOIA
    fReadCount += sizeof(long);
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( crcr != crcc )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "EventDoneMult msg CRC" )
	    << "CRC error when trying to read EventDoneMult message. Read CRC: "
	    << AliHLTLog::kHex << crcr << " - Calculated CRC: " << crcc << "." 
	    << ENDLOG;
	}
#endif
    interface.EventDone( *this, eventDoneData, forceForward, forwarded );
    return 0;
    }


int AliHLTSubscriberPipeProxy::HandleStartPublishingMsg( AliHLTPublisherInterface&, AliHLTPubSubPipeMsg* msg, 
							 bool& sendOldEvents )
    {
#ifdef PIPECOM_DOCRC
    long crcc = 0, crcr;
#endif
    int ret;
    char name[9], turnedName[9];
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    if ( msg->fParamCnt != 1 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "StartPublishing msg", 100 )
	    << "Unexpected parameter count for StartPublishing message: " << msg->fParamCnt
	    << " instead of 1" << ENDLOG;
	return 0;
	}
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    AliHLTBlockHeader *pbh;
    pbh = (AliHLTBlockHeader*)fParBool;
    ret = fPubSubCom.Read( pbh );
    //     parBool = (AliHLTPubSubPipeMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberPipeProxy", "StartPublishing msg parameter")
	    << "Error reading StartPublishing message bool parameter from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fParBool )
	{
	if ( fParBool )
	    delete [] (AliUInt8_t*)fParBool;
	fParBool = (AliHLTPubSubPipeMsgParam*)pbh;
	fParBoolSize = pbh->fLength;
	}
    else
	pbh->fLength = fParBoolSize;
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fParBool->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)fParBool->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "StartPublishing msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fParBool->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting pipe message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}
    if ( fParBool->fType != AliHLTPubSubPipeMsgParam::kBool )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberPipeProxy", "StartPublishing msg parameter type", 100 )
	    << "Unexpected message parameter type for StartPublishing message: " << AliHLTLog::kDec 
	    << (int)fParBool->fType << " when expecting " << (int)AliHLTPubSubPipeMsgParam::kBool << ENDLOG;
	return 0;
	}
#ifdef PIPECOM_DOCRC
    fPubSubCom.Read( sizeof(long), (AliUInt8_t*)&crcr );
#ifdef PIPE_PARANOIA
    fReadCount += sizeof(long);
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( crcr != crcc )
	{
	LOG( AliHLTLog::kFatal, "AliHLTSubscriberPipeProxy", "StartPublishing msg CRC" )
	    << "CRC error when trying to read StartPublishing message. Read CRC: "
	    << AliHLTLog::kHex << crcr << " - Calculated CRC: " << crcc << "." 
	    << ENDLOG;
	}
#endif

    sendOldEvents = (bool)fParBool->fData;
    return 0;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
