/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTPublisherPipeProxy.hpp"
#include "AliHLTSubscriberInterface.hpp"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTEventAccounting.hpp"
#include "AliHLTLog.hpp"
#ifdef PIPECOM_DOCRC
#include "CRC.hpp"
#endif
#include "MLUCLimitTimer.hpp"
#include <errno.h>


#define EVENT_CHECK_PARANOIA_LIMIT 8192
//#define TRACE printf( "%s:%d\n", __FILE__, __LINE__ )
#define TRACE 

#if 0
#define TRACELOG( lvl, orig, keys ) LOG(lvl,orig,keys)
#else
#define TRACELOG( lvl, orig, keys ) LOG(AliHLTLog::kNone,orig,keys)
#endif



AliHLTPublisherPipeProxy::AliHLTPublisherPipeProxy( const char* name ):
    AliHLTPublisherProxyInterface( name )
    {
    //     MLUCString rn, wn;
    fPubSubComOpened = false;
    pthread_mutex_init( &fMutex, NULL );
#ifdef OWNER_DEBUG
    fOwned = false;
    fOwnerThread = (pthread_t)0;
    fOwnerPID = 0;
#endif
    fQuitLoop = false;
    fLoopQuitted = true;

    fParEvtID = NULL;
    fParEvtIDSize = 0;
    fHNEMParSEDD = NULL;
    fHNEMParSEDDSize = 0;
    fHNEMParETS = NULL;
    fHNEMParETSSize = 0;
    fHNEMSEDD = NULL;
    fHNEMSEDDSize = 0;
    fHNEMETS = NULL;
    fHNEMETSSize = 0;
    fHEDMEDD = NULL;
    fHEDMEDDSize = 0;

#ifdef PIPECOM_DOCRC
    InitCrc32( fCRCTable );
#endif
#ifdef EVENT_CHECK_PARANOIA
    fLastEventDoneID = ~(AliEventID_t)0;
    fLastEventAnnouncedID = ~(AliEventID_t)0;
#endif
#ifdef PIPE_PARANOIA
    fWriteCount = 0;
    fReadCount = 0;
#endif
    fEventAccounting = NULL;
    fProxyName = name;
    fProxyName += "-PublisherProxy";
    }


AliHLTPublisherPipeProxy::~AliHLTPublisherPipeProxy()
    {
    pthread_mutex_destroy( &fMutex );
    if ( fParEvtID )
	delete [] (AliUInt8_t*)fParEvtID;
    if ( fHNEMParSEDD )
	delete [] (AliUInt8_t*)fHNEMParSEDD;
    if ( fHNEMParETS )
	delete [] (AliUInt8_t*)fHNEMParETS;
    if ( fHNEMSEDD )
	delete [] (AliUInt8_t*)fHNEMSEDD;
    if ( fHNEMETS )
	delete [] (AliUInt8_t*)fHNEMETS;
    if ( fHEDMEDD )
	delete [] (AliUInt8_t*)fHEDMEDD;
    }


int AliHLTPublisherPipeProxy::Subscribe( AliHLTSubscriberInterface& interface )
    {
    TRACE;
    int ret;
    MLUCString pname;
    pname = ALIL3PIPEBASEDIR;
    pname += GetName();
    char tmpUID[64];
    sprintf( tmpUID, "%d", (int)getuid() );
    pname += "-";
    pname += tmpUID;
    pname += "-";
    pname += "SubsService";
    ret = fPublCom.OpenWrite( pname.c_str() );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Publisher subscription pipe open")
	    << "Unable to open pipe " << pname.c_str()
	    << " - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	}
    MLUCString fullname;
    fullname = "pipe:";
    fullname += interface.GetName();
    TRACE;
    ret = pthread_mutex_lock( &fMutex );
#ifdef OWNER_DEBUG
    fOwned = true;
    fOwnerThread = pthread_self();
    fOwnerPID = getpid();
#endif
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Publisher subscription pipe open")
	    << "Error locking mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    if ( !fPublCom )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "Subscribe")
	    << "Subscription attempt without open publisher subscription pipe." << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return EPIPE;
	}
    TRACE;
    AliUInt8_t msgData[ sizeof(AliHLTPubSubPipeMsg)+strlen(fullname.c_str())+1 ];
    AliHLTPubSubPipeMsg* msg = (AliHLTPubSubPipeMsg*)msgData;
    MakeMsg( msg, fullname.c_str(), AliHLTPubSubPipeMsg::kSubscribe, 0 );
    TRACE;
    MLUCString p2sname, s2pname;
    p2sname = ALIL3PIPEBASEDIR;
    p2sname += interface.GetName();
    sprintf( tmpUID, "%d", (int)getuid() );
    p2sname += "-";
    p2sname += tmpUID;
    p2sname += "-";
    p2sname += "PublToSubs";
    s2pname = ALIL3PIPEBASEDIR;
    s2pname += interface.GetName();
    sprintf( tmpUID, "%d", (int)getuid() );
    s2pname += "-";
    s2pname += tmpUID;
    s2pname += "-";
    s2pname += "SubsToPubl";
    TRACE;
    ret = fPubSubCom.Open( p2sname.c_str(), s2pname.c_str(), true );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Subscriber pipes open")
	    << "Unable to open subscriber pipes " << p2sname.c_str() 
	    << " and " << s2pname.c_str() << " - Reason: " 
	    << strerror( ret ) << " (" << ret << ")" << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    TRACE;
    fPubSubComOpened = true;
    ret = fPublCom.Write( &(msg->fHeader) );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Subscribe message send")
	    << "Error writing subscriber message of size " << msg->fHeader.fLength
	    << " to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return ret;
	}

    TRACE;
    LOG( AliHLTLog::kInformational, "AliHLTPublisherPipeProxy", "Subscribe message")
	<< "Subscribe successfuly sent to publisher " << GetName() << ENDLOG;
    TRACE;
#ifdef OWNER_DEBUG
    fOwned = false;
    fOwnerThread = (pthread_t)0;
    fOwnerPID = 0;
#endif
    pthread_mutex_unlock( &fMutex );
    TRACE;
    return 0;
    }


int AliHLTPublisherPipeProxy::Unsubscribe( AliHLTSubscriberInterface& interface )
    {
    int ret;
#ifdef PIPECOM_DOCRC
    long crc = 0;
#endif
    ret = pthread_mutex_lock( &fMutex );
#ifdef OWNER_DEBUG
    fOwned = true;
    fOwnerThread = pthread_self();
    fOwnerPID = getpid();
#endif
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Publisher subscription pipe open")
	    << "Error locking mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    if ( !fPubSubComOpened )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "Unsubscribe")
	    << "Unsubscription attempt without open subscriber pipes." << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return EPIPE;
	}
    AliUInt8_t msgData[ sizeof(AliHLTPubSubPipeMsg)+strlen(interface.GetName())+1 ];
    AliHLTPubSubPipeMsg* msg = (AliHLTPubSubPipeMsg*)msgData;
    MakeMsg( msg, interface.GetName(), AliHLTPubSubPipeMsg::kUnsubscribe, 0 );
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    ret = fPubSubCom.Write( &(msg->fHeader) );
#ifdef PIPECOM_DOCRC
    fPubSubCom.Write( sizeof(long), (AliUInt8_t*)&crc );
#endif
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Unsubscribe message send")
	    << "Unable to write unsubscribe message to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
#ifdef PIPE_PARANOIA
    fWriteCount += msg->fHeader.fLength;
#ifdef PIPECOM_DOCRC
    fWriteCount += sizeof(long);
#endif
    if ( fWriteCount != fPubSubCom.GetWriteCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Write Count Inconsistency" )
	    << "Pipe communication object thinks it has written " << AliHLTLog::kDec 
	    << fPubSubCom.GetWriteCount() << " bytes, while I think it should have written "
	    << fWriteCount << " bytes." << ENDLOG;
	}
#endif
    //     ret = fPubSubCom.Close();
    //     fPubSubComOpened = false;
    //     if ( ret )
    // 	{
    // 	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Subscriber pipes close")
    // 	    << "Error closing subscriber pipes. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
    // 	}
    //     else
	{
	LOG( AliHLTLog::kInformational, "AliHLTPublisherPipeProxy", "Unsubscribe")
	    << "Unsubscription from publisher " << GetName() << "successful" << ENDLOG;
	}
#ifdef OWNER_DEBUG
    fOwned = false;
    fOwnerThread = (pthread_t)0;
    fOwnerPID = 0;
#endif
    pthread_mutex_unlock( &fMutex );
    return ret;
    }

int AliHLTPublisherPipeProxy::SetPersistent( AliHLTSubscriberInterface&, bool pers )
    {
    int ret;
    AliUInt32_t cnt;
#ifdef PIPECOM_DOCRC
    long crc = 0;
#endif
    ret = pthread_mutex_lock( &fMutex );
#ifdef OWNER_DEBUG
    fOwned = true;
    fOwnerThread = pthread_self();
    fOwnerPID = getpid();
#endif
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Publisher subscription pipe open")
	    << "Error locking mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    if ( !fPubSubComOpened )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "SetPersistent")
	    << "SetPersistent attempt without open subscriber pipes." << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return EPIPE;
	}
    AliUInt8_t msgData[ sizeof(AliHLTPubSubPipeMsg)+1 ];
    AliHLTPubSubPipeMsg* msg = (AliHLTPubSubPipeMsg*)msgData;
    MakeMsg( msg, NULL, AliHLTPubSubPipeMsg::kSetPersistent, 1 );
    cnt = msg->fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    AliHLTPubSubPipeMsgParam param;
    MakeParam( param, pers );
    cnt += param.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&param, fCRCTable );
#endif
#ifdef PIPECOM_DOCRC
    cnt += sizeof(long);
#endif

    AliHLTBlockHeader const* pbhs[2];
    pbhs[0] = &(msg->fHeader);
    pbhs[1] = &(param.fHeader);
    ret = fPubSubCom.Write( 2U, pbhs );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "SetPersistent message send")
	    << "Unable to write setpersistent message and parameter to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
#ifdef PIPECOM_DOCRC
    fPubSubCom.Write( sizeof(long), (AliUInt8_t*)&crc );
#endif
#ifdef PIPE_PARANOIA
    fWriteCount += msg->fHeader.fLength+param.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    fWriteCount += sizeof(long);
#endif
    if ( fWriteCount != fPubSubCom.GetWriteCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Write Count Inconsistency" )
	    << "Pipe communication object thinks it has written " << AliHLTLog::kDec 
	    << fPubSubCom.GetWriteCount() << " bytes, while I think it should have written "
	    << fWriteCount << " bytes." << ENDLOG;
	}
#endif
    LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "SetPersistent" )
	<< "SetPersistent message successfuly sent to publisher " << GetName() << ENDLOG;
#ifdef OWNER_DEBUG
    fOwned = false;
    fOwnerThread = (pthread_t)0;
    fOwnerPID = 0;
#endif
    pthread_mutex_unlock( &fMutex );
    return ret;
    }


int AliHLTPublisherPipeProxy::SetEventType( AliHLTSubscriberInterface&,  
					    AliUInt32_t modulo )
    {
    AliUInt32_t cnt;
    int ret;
#ifdef PIPECOM_DOCRC
    long crc = 0;
#endif
    ret = pthread_mutex_lock( &fMutex );
#ifdef OWNER_DEBUG
    fOwned = true;
    fOwnerThread = pthread_self();
    fOwnerPID = getpid();
#endif
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Publisher subscription pipe open")
	    << "Error locking mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    if ( !fPubSubComOpened )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "SetEventTypeMod")
	    << "SetEventTypeMod attempt without open subscriber pipes." << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return EPIPE;
	}
    AliUInt8_t msgData[ sizeof(AliHLTPubSubPipeMsg)+1 ];
    AliHLTPubSubPipeMsg* msg = (AliHLTPubSubPipeMsg*)msgData;
    MakeMsg( msg, NULL, AliHLTPubSubPipeMsg::kSetEventTypeMod, 1 );
    cnt = msg->fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    AliHLTPubSubPipeMsgParam param;
    MakeParam( param, modulo );
    cnt += param.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&param, fCRCTable );
#endif
#ifdef PIPECOM_DOCRC
    cnt += sizeof(long);
#endif

    AliHLTBlockHeader const* pbhs[2];
    pbhs[0] = &(msg->fHeader);
    pbhs[1] = &(param.fHeader);
    ret = fPubSubCom.Write( 2, pbhs );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "SetEventTypeMod message send")
	    << "Unable to write seteventtypemod message and parameter to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
#ifdef PIPECOM_DOCRC
    fPubSubCom.Write( sizeof(long), (AliUInt8_t*)&crc );
#endif
#ifdef PIPE_PARANOIA
    fWriteCount += msg->fHeader.fLength+param.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    fWriteCount += sizeof(long);
#endif
    if ( fWriteCount != fPubSubCom.GetWriteCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Write Count Inconsistency" )
	    << "Pipe communication object thinks it has written " << AliHLTLog::kDec 
	    << fPubSubCom.GetWriteCount() << " bytes, while I think it should have written "
	    << fWriteCount << " bytes." << ENDLOG;
	}
#endif
    LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "SetEventTypeMod" )
	<< "SetEventTypeMod message successfuly sent to publisher " << GetName() << ENDLOG;
#ifdef OWNER_DEBUG
    fOwned = false;
    fOwnerThread = (pthread_t)0;
    fOwnerPID = 0;
#endif
    pthread_mutex_unlock( &fMutex );
    return ret;
    }


int AliHLTPublisherPipeProxy::SetEventType( AliHLTSubscriberInterface&, 
					    vector<AliHLTEventTriggerStruct*>& triggerWords,  
					    AliUInt32_t modulo )
    {
    AliUInt32_t cnt;
    int ret;
#ifdef PIPECOM_DOCRC
    long crc = 0;
#endif
    ret = pthread_mutex_lock( &fMutex );
#ifdef OWNER_DEBUG
    fOwned = true;
    fOwnerThread = pthread_self();
    fOwnerPID = getpid();
#endif
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Publisher subscription pipe open")
	    << "Error locking mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    if ( !fPubSubComOpened )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "SetEventTypeETT")
	    << "SetEventTypeETT attempt without open subscriber pipes." << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return EPIPE;
	}
    AliUInt8_t msgData[ sizeof(AliHLTPubSubPipeMsg)+1 ];
    AliHLTPubSubPipeMsg* msg = (AliHLTPubSubPipeMsg*)msgData;
    MakeMsg( msg, NULL, AliHLTPubSubPipeMsg::kSetEventTypeETT, 2 );
    cnt = msg->fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    AliHLTPubSubPipeMsgParam param1, param2;
    MakeParam( param1, triggerWords );
    cnt += param1.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&param1, fCRCTable );
#endif
    int i, n = triggerWords.size();
    for ( i = 0; i < n; i++ )
	{
	cnt += triggerWords[i]->fHeader.fLength;
#ifdef PIPECOM_DOCRC
	crc = MakeCRC( crc, (AliHLTBlockHeader*)&(triggerWords[i]->fHeader), fCRCTable );
#endif
	}
    MakeParam( param2, modulo );
    cnt += param2.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&param2, fCRCTable );
    cnt += sizeof(long);
#endif

    AliHLTBlockHeader const* pbhs[3+n];
    pbhs[0] = &(msg->fHeader);
    pbhs[1] = &(param1.fHeader);
    //     AliUInt32_t i, n = triggerWords.size();
    n = triggerWords.size();
    for ( i = 0; i < n; i++ )
	pbhs[2+i] = &(triggerWords[i]->fHeader);
    pbhs[2+n] = &(param2.fHeader);
    ret = fPubSubCom.Write( 3+n, pbhs );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "SetTransientTimeout message send")
	    << "Unable to write SetEventTypeETT message and parameters to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
#ifdef PIPECOM_DOCRC
    fPubSubCom.Write( sizeof(long), (AliUInt8_t*)&crc );
#endif
#ifdef PIPE_PARANOIA
    fWriteCount += msg->fHeader.fLength+param1.fHeader.fLength+param2.fHeader.fLength;
    for ( i = 0; i < n; i++ )
	fWriteCount += triggerWords[i]->fHeader.fLength;
#ifdef PIPECOM_DOCRC
    fWriteCount += sizeof(long);
#endif
    if ( fWriteCount != fPubSubCom.GetWriteCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Write Count Inconsistency" )
	    << "Pipe communication object thinks it has written " << AliHLTLog::kDec 
	    << fPubSubCom.GetWriteCount() << " bytes, while I think it should have written "
	    << fWriteCount << " bytes." << ENDLOG;
	}
#endif
    LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "SetEventTypeETT" )
	<< "SetEventTypeETT message successfuly sent to publisher " << GetName() << ENDLOG;
#ifdef OWNER_DEBUG
    fOwned = false;
    fOwnerThread = (pthread_t)0;
    fOwnerPID = 0;
#endif
    pthread_mutex_unlock( &fMutex );
    return ret;
    }

int AliHLTPublisherPipeProxy::SetTransientTimeout( AliHLTSubscriberInterface&, 
						   AliUInt32_t timeout_ms )
    {
    AliUInt32_t cnt;
    int ret;
#ifdef PIPECOM_DOCRC
    long crc = 0;
#endif
    ret = pthread_mutex_lock( &fMutex );
#ifdef OWNER_DEBUG
    fOwned = true;
    fOwnerThread = pthread_self();
    fOwnerPID = getpid();
#endif
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Publisher subscription pipe open")
	    << "Error locking mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    if ( !fPubSubComOpened )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "SetTransientTimeout")
	    << "SetTransientTimeout attempt without open subscriber pipes." << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return EPIPE;
	}
    AliUInt8_t msgData[ sizeof(AliHLTPubSubPipeMsg)+1 ];
    AliHLTPubSubPipeMsg* msg = (AliHLTPubSubPipeMsg*)msgData;
    MakeMsg( msg, NULL, AliHLTPubSubPipeMsg::kSetTransientTimeout, 1 );
    cnt = msg->fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    AliHLTPubSubPipeMsgParam param;
    MakeParam( param, timeout_ms );
    cnt += param.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&param, fCRCTable );
    cnt += sizeof(long);
#endif

    AliHLTBlockHeader const* pbhs[2];
    pbhs[0] = &(msg->fHeader);
    pbhs[1] = &(param.fHeader);
    ret = fPubSubCom.Write( 2, pbhs );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "SetTransientTimeout message send")
	    << "Unable to write settransienttimeout message to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
#ifdef PIPECOM_DOCRC
    fPubSubCom.Write( sizeof(long), (AliUInt8_t*)&crc );
#endif
#ifdef PIPE_PARANOIA
    fWriteCount += msg->fHeader.fLength+param.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    fWriteCount += sizeof(long);
#endif
    if ( fWriteCount != fPubSubCom.GetWriteCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Write Count Inconsistency" )
	    << "Pipe communication object thinks it has written " << AliHLTLog::kDec 
	    << fPubSubCom.GetWriteCount() << " bytes, while I think it should have written "
	    << fWriteCount << " bytes." << ENDLOG;
	}
#endif
    LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "SetTransientTimeout" )
	<< "SetTransientTimeout message for " << AliHLTLog::kDec << timeout_ms << "ms successfuly sent to publisher " << GetName() << ENDLOG;
#ifdef OWNER_DEBUG
    fOwned = false;
    fOwnerThread = (pthread_t)0;
    fOwnerPID = 0;
#endif
    pthread_mutex_unlock( &fMutex );
    return ret;
    }


int AliHLTPublisherPipeProxy::SetEventDoneDataSend( AliHLTSubscriberInterface&, bool sendEDD )
    {
    AliUInt32_t cnt;
    int ret;
#ifdef PIPECOM_DOCRC
    long crc = 0;
#endif
    ret = pthread_mutex_lock( &fMutex );
#ifdef OWNER_DEBUG
    fOwned = true;
    fOwnerThread = pthread_self();
    fOwnerPID = getpid();
#endif
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Publisher subscription pipe open")
	    << "Error locking mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    if ( !fPubSubComOpened )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "SetEventDoneDataSend")
	    << "SetEventDoneDataSend attempt without open subscriber pipes." << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return EPIPE;
	}
    AliUInt8_t msgData[ sizeof(AliHLTPubSubPipeMsg)+1 ];
    AliHLTPubSubPipeMsg* msg = (AliHLTPubSubPipeMsg*)msgData;
    MakeMsg( msg, NULL, AliHLTPubSubPipeMsg::kSetEventDoneDataSend, 1 );
    cnt = msg->fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    AliHLTPubSubPipeMsgParam param;
    MakeParam( param, sendEDD );
    cnt += param.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&param, fCRCTable );
#endif
#ifdef PIPECOM_DOCRC
    cnt += sizeof(long);
#endif

    AliHLTBlockHeader const* pbhs[2];
    pbhs[0] = &(msg->fHeader);
    pbhs[1] = &(param.fHeader);
    ret = fPubSubCom.Write( 2, pbhs );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "SetEventDoneDataSend message send")
	    << "Unable to write seteventdonedatasend message to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
#ifdef PIPECOM_DOCRC
    fPubSubCom.Write( sizeof(long), (AliUInt8_t*)&crc );
#endif
#ifdef PIPE_PARANOIA
    fWriteCount += msg->fHeader.fLength+param.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    fWriteCount += sizeof(long);
#endif
    if ( fWriteCount != fPubSubCom.GetWriteCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Write Count Inconsistency" )
	    << "Pipe communication object thinks it has written " << AliHLTLog::kDec 
	    << fPubSubCom.GetWriteCount() << " bytes, while I think it should have written "
	    << fWriteCount << " bytes." << ENDLOG;
	}
#endif
    LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "SetEventDoneDataSend" )
	<< "SetEventDoneDataSend message successfuly sent to publisher " << GetName() << ENDLOG;
#ifdef OWNER_DEBUG
    fOwned = false;
    fOwnerThread = (pthread_t)0;
    fOwnerPID = 0;
#endif
    pthread_mutex_unlock( &fMutex );
    return ret;
    }


int AliHLTPublisherPipeProxy::EventDone( AliHLTSubscriberInterface&, 
					 const AliHLTEventDoneData& eventDoneData, bool forceForward, bool forwarded )
    {
    MLUCLIMITTIMER( MLUCLIMITTIMER_DEFAULT_TIMER_LIMIT_US );
    if ( fEventAccounting )
	fEventAccounting->EventLeft( fProxyName.c_str(), eventDoneData.fEventID );
    if ( fEventAccounting )
	fEventAccounting->EventFinished( fProxyName.c_str(), eventDoneData.fEventID, forceForward ? 2 : 1, forwarded ? 2 : 1 );
#ifdef EVENT_CHECK_PARANOIA
    if ( fLastEventDoneID!=~(AliEventID_t)0 )
	{
	if ( fLastEventDoneID+EVENT_CHECK_PARANOIA_LIMIT<eventID || (fLastEventDoneID>EVENT_CHECK_PARANOIA_LIMIT && fLastEventDoneID-EVENT_CHECK_PARANOIA_LIMIT>eventID) )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy::EventDone", "Suspicious Event ID" )
		<< "Suspicious Event ID found: 0x" << AliHLTLog::kHex << eventID << " ("
		<< AliHLTLog::kDec << eventID << ") - last done event ID: 0x"
		<< AliHLTLog::kHex << fLastEventDoneID << " (" << AliHLTLog::kDec 
		<< fLastEventDoneID << ")." << ENDLOG;
	    }
	}
    fLastEventDoneID = eventID;
#endif

    AliUInt32_t cnt;
    int ret;
#ifdef PIPECOM_DOCRC
    long crc = 0;
#endif
    ret = pthread_mutex_lock( &fMutex );
#ifdef OWNER_DEBUG
    fOwned = true;
    fOwnerThread = pthread_self();
    fOwnerPID = getpid();
#endif
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Publisher subscription pipe open")
	    << "Error locking mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    if ( !fPubSubComOpened )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "EventDoneSing")
	    << "EventDoneSing attempt without open subscriber pipes." << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return EPIPE;
	}
    LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "EventDoneSing")
	<< "EventDoneData of " << AliHLTLog::kDec << eventDoneData.fHeader.fLength
	<< " bytes and " << eventDoneData.fDataWordCount << " data words received for event 0x"
	<< AliHLTLog::kHex << eventDoneData.fEventID << " (" << AliHLTLog::kDec
	<< eventDoneData.fEventID << ") (forceForward: " << (forceForward ? "yes" : "no") << " - forwarded: "
	<< (forwarded ? "yes" : "no") << ")." << ENDLOG;
    AliUInt8_t msgData[ sizeof(AliHLTPubSubPipeMsg)+1 ];
    AliHLTPubSubPipeMsg* msg = (AliHLTPubSubPipeMsg*)msgData;
    MakeMsg( msg, NULL, AliHLTPubSubPipeMsg::kEventDoneSing, 3 );
    cnt = msg->fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif

    AliHLTPubSubPipeMsgParam param;
    MakeParam( param, eventDoneData );
    cnt += param.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&param, fCRCTable );
#endif
    AliHLTPubSubPipeMsgParam paramForceFlag;
    MakeParam( paramForceFlag, forceForward );
    cnt += paramForceFlag.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&paramForceFlag, fCRCTable );
#endif
    AliHLTPubSubPipeMsgParam paramForwardFlag;
    MakeParam( paramForwardFlag, forwarded );
    cnt += paramForwardFlag.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&paramForwardFlag, fCRCTable );
#endif


    cnt += eventDoneData.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&eventDoneData, fCRCTable );
#endif
#ifdef PIPECOM_DOCRC
    cnt += sizeof(long);
#endif

    AliHLTBlockHeader const* pbhs[5];
    pbhs[0] = &(msg->fHeader);
    pbhs[1] = &(param.fHeader);
    pbhs[2] = &(paramForceFlag.fHeader);
    pbhs[3] = &(paramForwardFlag.fHeader);
    pbhs[4] = &(eventDoneData.fHeader);
    ret = fPubSubCom.Write( 5U, pbhs );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "EventDoneSing message send")
	    << "Unable to write eventdonesing message to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
#ifdef PIPECOM_DOCRC
    fPubSubCom.Write( sizeof(long), (AliUInt8_t*)&crc );
#endif
#ifdef PIPE_PARANOIA
    fWriteCount += msg->fHeader.fLength+param.fHeader.fLength+paramForceFlag.fHeader.fLength+paramForwardFlag.fHeader.fLength+eventDoneData.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    fWriteCount += sizeof(long);
#endif
    if ( fWriteCount != fPubSubCom.GetWriteCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Write Count Inconsistency" )
	    << "Pipe communication object thinks it has written " << AliHLTLog::kDec 
	    << fPubSubCom.GetWriteCount() << " bytes, while I think it should have written "
	    << fWriteCount << " bytes." << ENDLOG;
	}
#endif
    LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "EventDoneSing" )
	<< "EventDoneSing message for event 0x" << AliHLTLog::kHex << eventDoneData.fEventID << " (" 
	<< AliHLTLog::kDec << eventDoneData.fEventID << ") successfuly sent to publisher " << GetName() << ENDLOG;
#ifdef OWNER_DEBUG
    fOwned = false;
    fOwnerThread = (pthread_t)0;
    fOwnerPID = 0;
#endif
    pthread_mutex_unlock( &fMutex );
    return ret;
    }

int AliHLTPublisherPipeProxy::EventDone( AliHLTSubscriberInterface&, 
					 vector<AliHLTEventDoneData*>& eventDoneData, bool forceForward, bool forwarded )
    {
    AliUInt32_t cnt;
    int ret;
#ifdef PIPECOM_DOCRC
    long crc = 0;
#endif
    ret = pthread_mutex_lock( &fMutex );
#ifdef OWNER_DEBUG
    fOwned = true;
    fOwnerThread = pthread_self();
    fOwnerPID = getpid();
#endif
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Publisher subscription pipe open")
	    << "Error locking mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    if ( !fPubSubComOpened )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "EventDoneMult")
	    << "EventDoneMult attempt without open subscriber pipes." << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return EPIPE;
	}
    AliUInt8_t msgData[ sizeof(AliHLTPubSubPipeMsg)+1 ];
    AliHLTPubSubPipeMsg* msg = (AliHLTPubSubPipeMsg*)msgData;
    AliHLTPubSubPipeMsgParam param;
    MakeMsg( msg, NULL, AliHLTPubSubPipeMsg::kEventDoneMult, 3 );
    cnt = msg->fHeader.fLength;
    AliHLTPubSubPipeMsgParam paramForceFlag;
    MakeParam( paramForceFlag, forceForward );
    cnt += paramForceFlag.fHeader.fLength;
    AliHLTPubSubPipeMsgParam paramForwardFlag;
    MakeParam( paramForwardFlag, forwarded );
    cnt += paramForwardFlag.fHeader.fLength;
    MakeParam( param, eventDoneData );
    cnt += param.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)msg, fCRCTable );
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&paramForceFlag, fCRCTable );
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&paramForwardFlag, fCRCTable );
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&param, fCRCTable );
#endif
    int i, n = param.fData;
    for ( i = 0; i < n; i++ )
	{
	cnt += eventDoneData[i]->fHeader.fLength;
#ifdef PIPECOM_DOCRC
	crc = MakeCRC( crc, (AliHLTBlockHeader*)eventDoneData[i], fCRCTable );
#endif
	}
#ifdef PIPECOM_DOCRC
    cnt += sizeof(long);
#endif

    AliHLTBlockHeader const* pbhs[4+n];
    pbhs[0] = &(msg->fHeader);
    pbhs[1] = &(paramForceFlag.fHeader);
    pbhs[2] = &(paramForwardFlag.fHeader);
    pbhs[3] = &(param.fHeader);
    for ( i = 0; i < n; i++ )
	pbhs[4+i] = (AliHLTBlockHeader*)eventDoneData[i];
    
    ret = fPubSubCom.Write( 4+n, pbhs );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "EventDoneMult message send")
	    << "Unable to write eventdonemult message and parameters to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
#ifdef PIPECOM_DOCRC
    fPubSubCom.Write( sizeof(long), (AliUInt8_t*)&crc );
#endif
#ifdef PIPE_PARANOIA
    fWriteCount += msg->fHeader.fLength+paramForceFlag.fHeader.fLength+paramForwardFlag.fHeader.fLength+param.fHeader.fLength;
    for ( i = 0; i < n; i++ )
	fWriteCount += ((AliHLTBlockHeader*)eventDoneData[i])->fLength;
#ifdef PIPECOM_DOCRC
    fWriteCount += sizeof(long);
#endif
    if ( fWriteCount != fPubSubCom.GetWriteCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Write Count Inconsistency" )
	    << "Pipe communication object thinks it has written " << AliHLTLog::kDec 
	    << fPubSubCom.GetWriteCount() << " bytes, while I think it should have written "
	    << fWriteCount << " bytes." << ENDLOG;
	}
#endif
    LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "EventDoneMult" )
	<< "EventDoneMult message successfuly sent to publisher " << GetName() << ENDLOG;
#ifdef OWNER_DEBUG
    fOwned = false;
    fOwnerThread = (pthread_t)0;
    fOwnerPID = 0;
#endif
    pthread_mutex_unlock( &fMutex );
    return ret;
    }

int AliHLTPublisherPipeProxy::StartPublishing( AliHLTSubscriberInterface& interface, bool sendOldEvents )
    {
    int ret;
#ifdef PIPECOM_DOCRC
    long crc = 0;
#endif
    ret = pthread_mutex_lock( &fMutex );
#ifdef OWNER_DEBUG
    fOwned = true;
    fOwnerThread = pthread_self();
    fOwnerPID = getpid();
#endif
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Publisher subscription pipe open")
	    << "Error locking mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    if ( !fPubSubComOpened )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "StartPublishing")
	    << "StartPublishing attempt without open subscriber pipes." << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return EPIPE;
	}
    AliUInt8_t msgData[ sizeof(AliHLTPubSubPipeMsg)+1 ];
    AliHLTPubSubPipeMsg* msg = (AliHLTPubSubPipeMsg*)msgData;
    MakeMsg( msg, NULL, AliHLTPubSubPipeMsg::kStartPublishing, 1 );
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    AliHLTPubSubPipeMsgParam param;
    MakeParam( param, sendOldEvents );
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)&param, fCRCTable );
#endif

    ret = fPubSubCom.Write( &(msg->fHeader) );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "StartPublishing message send")
	    << "Unable to write startpublishing message to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    ret = fPubSubCom.Write( &(param.fHeader) );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "StartPublishing message parameter send")
	    << "Unable to write startpublishing message parameter to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    LOG( AliHLTLog::kInformational, "AliHLTPublisherPipeProxy", "StartPublishing")
	<< "StartPublishing call to publisher " << GetName() << " successful" << ENDLOG;
#ifdef PIPECOM_DOCRC
    fPubSubCom.Write( sizeof(long), (AliUInt8_t*)&crc );
#endif
#ifdef PIPE_PARANOIA
    fWriteCount += msg->fHeader.fLength+param.fHeader.fLength;
#ifdef PIPECOM_DOCRC
    fWriteCount += sizeof(long);
#endif
    if ( fWriteCount != fPubSubCom.GetWriteCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Write Count Inconsistency" )
	    << "Pipe communication object thinks it has written " << AliHLTLog::kDec 
	    << fPubSubCom.GetWriteCount() << " bytes, while I think it should have written "
	    << fWriteCount << " bytes." << ENDLOG;
	}
#endif
#ifdef OWNER_DEBUG
    fOwned = false;
    fOwnerThread = (pthread_t)0;
    fOwnerPID = 0;
#endif
    pthread_mutex_unlock( &fMutex );
    fQuitLoop = false;
    ret = MsgLoop( interface );
    return ret;
    }

int AliHLTPublisherPipeProxy::Ping( AliHLTSubscriberInterface& )
    {
    int ret;
#ifdef PIPECOM_DOCRC
    long crc = 0;
#endif
    ret = pthread_mutex_lock( &fMutex );
#ifdef OWNER_DEBUG
    fOwned = true;
    fOwnerThread = pthread_self();
    fOwnerPID = getpid();
#endif
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Publisher subscription pipe open")
	    << "Error locking mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    if ( !fPubSubComOpened )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "Ping")
	    << "Ping attempt without open subscriber pipes." << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return EPIPE;
	}
    AliUInt8_t msgData[ sizeof(AliHLTPubSubPipeMsg)+1 ];
    AliHLTPubSubPipeMsg* msg = (AliHLTPubSubPipeMsg*)msgData;
    MakeMsg( msg, NULL, AliHLTPubSubPipeMsg::kPing, 0 );
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    ret = fPubSubCom.Write( &(msg->fHeader) );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Ping message send")
	    << "Unable to write ping message to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    //     ret = fPubSubCom.Close();
    //     fPubSubComOpened = false;
    //     if ( ret )
    // 	{
    // 	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Subscriber pipes close")
    // 	    << "Error closing subscriber pipes. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
    // 	}
    //     else
	{
	LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "Ping")
	    << "Ping to publisher " << GetName() << "successful" << ENDLOG;
	}
#ifdef PIPECOM_DOCRC
    fPubSubCom.Write( sizeof(long), (AliUInt8_t*)&crc );
#endif
#ifdef PIPE_PARANOIA
    fWriteCount += msg->fHeader.fLength;
#ifdef PIPECOM_DOCRC
    fWriteCount += sizeof(long);
#endif
    if ( fWriteCount != fPubSubCom.GetWriteCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Write Count Inconsistency" )
	    << "Pipe communication object thinks it has written " << AliHLTLog::kDec 
	    << fPubSubCom.GetWriteCount() << " bytes, while I think it should have written "
	    << fWriteCount << " bytes." << ENDLOG;
	}
#endif
#ifdef OWNER_DEBUG
    fOwned = false;
    fOwnerThread = (pthread_t)0;
    fOwnerPID = 0;
#endif
    pthread_mutex_unlock( &fMutex );
    return ret;
    }

int AliHLTPublisherPipeProxy::PingAck( AliHLTSubscriberInterface& )
    {
    int ret;
#ifdef PIPECOM_DOCRC
    long crc = 0;
#endif
    ret = pthread_mutex_lock( &fMutex );
#ifdef OWNER_DEBUG
    fOwned = true;
    fOwnerThread = pthread_self();
    fOwnerPID = getpid();
#endif
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Publisher subscription pipe open")
	    << "Error locking mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	}
    if ( !fPubSubComOpened )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "PingAck")
	    << "PingAck attempt without open subscriber pipes." << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return EPIPE;
	}
    AliUInt8_t msgData[ sizeof(AliHLTPubSubPipeMsg)+1 ];
    AliHLTPubSubPipeMsg* msg = (AliHLTPubSubPipeMsg*)msgData;
    MakeMsg( msg, NULL, AliHLTPubSubPipeMsg::kPingAck, 0 );
#ifdef PIPECOM_DOCRC
    crc = MakeCRC( crc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    ret = fPubSubCom.Write( &(msg->fHeader) );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "PingAck message send")
	    << "Unable to write pingack message to pipe - Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
#ifdef OWNER_DEBUG
	fOwned = false;
	fOwnerThread = (pthread_t)0;
	fOwnerPID = 0;
#endif
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    //     ret = fPubSubCom.Close();
    //     fPubSubComOpened = false;
    //     if ( ret )
    // 	{
    // 	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Subscriber pipes close")
    // 	    << "Error closing subscriber pipes. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
    // 	}
    //     else
	{
	LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "PingAck")
	    << "PingAck to publisher " << GetName() << "successful" << ENDLOG;
	}
#ifdef PIPECOM_DOCRC
    fPubSubCom.Write( sizeof(long), (AliUInt8_t*)&crc );
#endif
#ifdef PIPE_PARANOIA
    fWriteCount += msg->fHeader.fLength;
#ifdef PIPECOM_DOCRC
    fWriteCount += sizeof(long);
#endif
    if ( fWriteCount != fPubSubCom.GetWriteCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Write Count Inconsistency" )
	    << "Pipe communication object thinks it has written " << AliHLTLog::kDec 
	    << fPubSubCom.GetWriteCount() << " bytes, while I think it should have written "
	    << fWriteCount << " bytes." << ENDLOG;
	}
#endif
#ifdef OWNER_DEBUG
    fOwned = false;
    fOwnerThread = (pthread_t)0;
    fOwnerPID = 0;
#endif
    pthread_mutex_unlock( &fMutex );
    return ret;
    }


void AliHLTPublisherPipeProxy::SetTimeout( AliUInt32_t timeout_usec )
    {
    fPublCom.SetTimeout( timeout_usec );
    fPubSubCom.SetTimeout( timeout_usec );
    }


void AliHLTPublisherPipeProxy::AbortPublishing()
    {
    AliHLTPipeCom com;
    int ret;
    MLUCString s2pname;
    fQuitLoop = true;
    s2pname = fPubSubCom.GetReadPipeName();
    ret = com.OpenWrite( s2pname.c_str() );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy::AbortPublishing", "Quit pipe open" )
	    << "Error opening pipe " << s2pname.c_str()
	    << " for writing quit message to publisher proxy. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return;
	}
    int count = 0;
    while ( !fLoopQuitted && count < 256 )
	{
	count++;
	AliHLTPubSubPipeMsg msg;
	msg.fHeader.fType.fID = ALIL3PUBSUBPIPEMSG_TYPE;
	msg.fHeader.fSubType.fID = 0;
	msg.fHeader.fVersion = 1;
	msg.fHeader.fLength = sizeof( AliHLTPubSubPipeMsg );
	msg.fType = AliHLTPubSubPipeMsg::kSubscriptionCanceled;
	msg.fParamCnt = 0;
	LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy::AbortPublishing", "Writing quit command" )
	    << "Writing quit command now." << ENDLOG;
	ret = com.Write( &(msg.fHeader), false );
	// 	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy::AbortPublishing", "Quit command written" )
	// 	    << "Quit command written." << ENDLOG;
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy::AbortPublishing", "Subscriber proxy quit message write" )
		<< "Unable to write publisher proxy quit message to pipe " 
		<< s2pname.c_str() 
		<< " Reason: " << strerror( ret ) << " (" << ret << ")"<< ENDLOG;
	    return;
	    }
	usleep( 50000 );
	// 	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy::AbortPublishing", "After usleep" )
	// 	    << "After usleep." << ENDLOG;
	}
    com.Close();
    count=0;
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 200000;
    gettimeofday( &start, NULL );
    while ( !fLoopQuitted && deltaT<timeLimit )
	{
	LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy::AbortPublishing", "Waiting for loop finish" ) 
	    << "Waiting for publisher proxy loop to finish... (Attempt " << count << ")." << ENDLOG;
	count++;
	usleep( 1000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    }


void AliHLTPublisherPipeProxy::MakeMsg( AliHLTPubSubPipeMsg* msg, const char* subscrName, 
					AliHLTPubSubPipeMsg::MsgType type,
					AliUInt32_t paramCnt )
    {
    int sz = sizeof(AliHLTPubSubPipeMsg)+1;
    if ( subscrName )
	sz += strlen(subscrName);
    if ( !msg )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Message NULL pointer")
	    << "Message NULL pointer passed for subscriber " << subscrName << ENDLOG;
	return;
	}
    // #define VALGRIND_PARANOIA
#ifdef VALGRIND_PARANOIA
    memset( msg, 0, sz );
#endif
    msg->fHeader.fLength = sz;
    msg->fHeader.fType.fID = ALIL3PUBSUBPIPEMSG_TYPE;
    msg->fHeader.fSubType.fID = 0;
    msg->fHeader.fVersion = 1;
    msg->fType = (AliUInt32_t)type;
    msg->fParamCnt = paramCnt;
    if ( subscrName )
	strcpy( (char*)msg->fSubscriberName, subscrName );
    else
	msg->fSubscriberName[0]=0;
    }

void AliHLTPublisherPipeProxy::MakeParam( AliHLTPubSubPipeMsgParam& param, bool val )
    {
    // #define VALGRIND_PARANOIA
#ifdef VALGRIND_PARANOIA
    memset( &param, 0, sizeof(AliHLTPubSubPipeMsgParam) );
#endif
    param.fHeader.fType.fID = ALIL3PUBSUBPIPEMSGPARAM_TYPE;
    param.fHeader.fSubType.fID = 0;
    param.fHeader.fVersion = 1;
    param.fHeader.fLength = sizeof(AliHLTPubSubPipeMsgParam);
    param.fType = AliHLTPubSubPipeMsgParam::kBool;
    param.fData = (AliUInt64_t)val;
    }

void AliHLTPublisherPipeProxy::MakeParam( AliHLTPubSubPipeMsgParam& param, AliUInt32_t val )
    {
    // #define VALGRIND_PARANOIA
#ifdef VALGRIND_PARANOIA
    memset( &param, 0, sizeof(AliHLTPubSubPipeMsgParam) );
#endif
    param.fHeader.fType.fID = ALIL3PUBSUBPIPEMSGPARAM_TYPE;
    param.fHeader.fSubType.fID = 0;
    param.fHeader.fVersion = 1;
    param.fHeader.fLength = sizeof(AliHLTPubSubPipeMsgParam);
    param.fType = AliHLTPubSubPipeMsgParam::kUInt32;
    param.fData = val;
    }

void AliHLTPublisherPipeProxy::MakeParam( AliHLTPubSubPipeMsgParam& param, vector<AliHLTEventTriggerStruct*>& val )
    {
    // #define VALGRIND_PARANOIA
#ifdef VALGRIND_PARANOIA
    memset( &param, 0, sizeof(AliHLTPubSubPipeMsgParam) );
#endif
    param.fHeader.fType.fID = ALIL3PUBSUBPIPEMSGPARAM_TYPE;
    param.fHeader.fSubType.fID = 0;
    param.fHeader.fVersion = 1;
    param.fHeader.fLength = sizeof(AliHLTPubSubPipeMsgParam);
    param.fType = AliHLTPubSubPipeMsgParam::kVectorETT;
    param.fData = val.size();
    }

void AliHLTPublisherPipeProxy::MakeParam( AliHLTPubSubPipeMsgParam& param, const AliHLTEventDoneData& )
    {
    // #define VALGRIND_PARANOIA
#ifdef VALGRIND_PARANOIA
    memset( &param, 0, sizeof(AliHLTPubSubPipeMsgParam) );
#endif
    param.fHeader.fType.fID = ALIL3PUBSUBPIPEMSGPARAM_TYPE;
    param.fHeader.fSubType.fID = 0;
    param.fHeader.fVersion = 1;
    param.fHeader.fLength = sizeof(AliHLTPubSubPipeMsgParam);
    param.fType = AliHLTPubSubPipeMsgParam::kEvtDoneD;
    param.fData = 0;
    }

void AliHLTPublisherPipeProxy::MakeParam( AliHLTPubSubPipeMsgParam& param, vector<AliHLTEventDoneData*>& val )
    {
    // #define VALGRIND_PARANOIA
#ifdef VALGRIND_PARANOIA
    memset( &param, 0, sizeof(AliHLTPubSubPipeMsgParam) );
#endif
    param.fHeader.fType.fID = ALIL3PUBSUBPIPEMSGPARAM_TYPE;
    param.fHeader.fSubType.fID = 0;
    param.fHeader.fVersion = 1;
    param.fHeader.fLength = sizeof(AliHLTPubSubPipeMsgParam);
    param.fType = AliHLTPubSubPipeMsgParam::kVectorEvtDoneD;
    param.fData = val.size();
    }


int AliHLTPublisherPipeProxy::MsgLoop( AliHLTSubscriberInterface& interface )
    {
    int ret;
    char name[9], turnedName[9];
    AliHLTBlockHeader *pbh, *pbhSave, sample = { 0, {ALIL3PUBSUBPIPEMSG_TYPE}, {0}, 1 };
    AliUInt32_t pbhSize;
    AliHLTPubSubPipeMsg *msg;
#ifdef PIPECOM_DOCRC
    long crcr, crcc;
#endif
    fLoopQuitted = false;
    pbh = NULL;
    pbhSize = 0;
    do
	{
	pbhSave = pbh;
	ret = fPubSubCom.Read( pbh, true );
	msg = (AliHLTPubSubPipeMsg*)pbh;
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "Msg loop read")
		<< "Error reading publisher (Publisher " << GetName() << ") message from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	    return ret;
	    }
#ifdef PIPE_PARANOIA
	fReadCount += pbh->fLength;
	if ( fReadCount != fPubSubCom.GetReadCount() )
	    {
	    LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Read Count Inconsistency" )
		<< "Pipe communication object thinks it has read " << AliHLTLog::kDec 
		<< fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
		<< fReadCount << " bytes." << ENDLOG;
	    }
#endif
	if ( pbhSave!=pbh )
	    {
	    pbhSize = pbh->fLength;
	    if ( pbhSave )
		delete [] (AliUInt8_t*)pbhSave;
	    pbhSave = NULL;
	    }
	if ( msg->fHeader.fType.fID != ALIL3PUBSUBPIPEMSG_TYPE )
	    {
	    strncpy( name, (const char*)msg->fHeader.fType.fDescr, 4 );
	    name[4] = 0;
	    turnedName[0] = name[3];
	    turnedName[1] = name[2];
	    turnedName[2] = name[1];
	    turnedName[3] = name[0];
	    turnedName[4] = 0;
	    LOGM( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "Msg loop", 100 )
		<< "Read unexpected datatype " << AliHLTLog::kHex << msg->fHeader.fType.fID
		<< " == " << name << " (" << turnedName << ")"
		<< " from pipe when expecting pipe message (" << sample.fType.fID << ")" << ENDLOG;
	    continue;
	    }
#ifdef PIPECOM_DOCRC
	crcc = 0;
#endif
	switch ( msg->fType )
	    {
	    case AliHLTPubSubPipeMsg::kNewEvent:
		{
		LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "NewEvent" )
		    << "NewEvent message received from publisher " << GetName() << ENDLOG;
		ret = HandleNewEventMsg( interface, msg );
		if ( ret )
		    return ret;
		break;
		}
	    case AliHLTPubSubPipeMsg::kEventCanceled:
		{
		LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "EventCanceled" )
		    << "EventCanceled message received from publisher " << GetName() << ENDLOG;
		ret = HandleEventCanceledMsg( interface, msg );
		if ( ret )
		    return ret;
		break;
		}
	    case AliHLTPubSubPipeMsg::kEventDoneData:
		{
		LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "EventDoneData" )
		    << "EventDoneData message received from subscriber " << GetName() << ENDLOG;
		ret = HandleEventDoneDataMsg( interface, msg );
		if ( ret )
		    return ret;
		break;
		}
	    case AliHLTPubSubPipeMsg::kSubscriptionCanceled:
		{
		LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "SubscriptionCanceled" )
		    << "SubscriptionCanceled message received from publisher " << GetName() << ENDLOG;
#ifdef PIPECOM_DOCRC
		crcc = MakeCRC( crcc, (AliHLTBlockHeader*)msg, fCRCTable );
		fPubSubCom.Read( sizeof(long), (AliUInt8_t*)&crcr );
#ifdef PIPE_PARANOIA
		fReadCount += sizeof(long);
		if ( fReadCount != fPubSubCom.GetReadCount() )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Read Count Inconsistency" )
			<< "Pipe communication object thinks it has read " << AliHLTLog::kDec 
			<< fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
			<< fReadCount << " bytes." << ENDLOG;
		    }
#endif
		if ( crcr != crcc )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "SubscriptionCanceled CRC" )
			<< "CRC error when trying to read SubscriptionCanceled message. Read CRC: "
			<< AliHLTLog::kHex << crcr << " - Calculated CRC: " << crcc << "." 
			<< ENDLOG;
		    }
#endif
    TRACELOG(AliHLTLog::kDebug, "AliHLTProcessingSubscriber::SubscriptionCanceled", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
		interface.SubscriptionCanceled( *this );
    TRACELOG(AliHLTLog::kDebug, "AliHLTProcessingSubscriber::SubscriptionCanceled", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
		fQuitLoop = true;
		break;
		}
	    case AliHLTPubSubPipeMsg::kReleaseEventsRequest:
		{
		LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "ReleaseEventsRequest" )
		    << "ReleaseEventsRequest message received from publisher  " << GetName() << ENDLOG;
#ifdef PIPECOM_DOCRC
		crcc = MakeCRC( crcc, (AliHLTBlockHeader*)msg, fCRCTable );
		fPubSubCom.Read( sizeof(long), (AliUInt8_t*)&crcr );
#ifdef PIPE_PARANOIA
		fReadCount += sizeof(long);
		if ( fReadCount != fPubSubCom.GetReadCount() )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Read Count Inconsistency" )
			<< "Pipe communication object thinks it has read " << AliHLTLog::kDec 
			<< fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
			<< fReadCount << " bytes." << ENDLOG;
		    }
#endif
		if ( crcr != crcc )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "ReleaseEventsRequest CRC" )
			<< "CRC error when trying to read ReleaseEventsRequest message. Read CRC: "
			<< AliHLTLog::kHex << crcr << " - Calculated CRC: " << crcc << "." 
			<< ENDLOG;
		    }
#endif
		interface.ReleaseEventsRequest( *this );
		if ( ret )
		    return ret;
		break;
		}
	    case AliHLTPubSubPipeMsg::kPing:
		{
		LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "Ping" )
		    << "Ping message received from publisher" << ENDLOG;
#ifdef PIPECOM_DOCRC
		crcc = MakeCRC( crcc, (AliHLTBlockHeader*)msg, fCRCTable );
		fPubSubCom.Read( sizeof(long), (AliUInt8_t*)&crcr );
#ifdef PIPE_PARANOIA
		fReadCount += sizeof(long);
		if ( fReadCount != fPubSubCom.GetReadCount() )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Read Count Inconsistency" )
			<< "Pipe communication object thinks it has read " << AliHLTLog::kDec 
			<< fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
			<< fReadCount << " bytes." << ENDLOG;
		    }
#endif
		if ( crcr != crcc )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Ping CRC" )
			<< "CRC error when trying to read Ping message. Read CRC: "
			<< AliHLTLog::kHex << crcr << " - Calculated CRC: " << crcc << "." 
			<< ENDLOG;
		    }
#endif
		interface.Ping( *this );
		break;
		}
	    case AliHLTPubSubPipeMsg::kPingAck:
		{
		LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "PingAck" )
		    << "PingAck message received from publisher" << ENDLOG;
#ifdef PIPECOM_DOCRC
		crcc = MakeCRC( crcc, (AliHLTBlockHeader*)msg, fCRCTable );
		fPubSubCom.Read( sizeof(long), (AliUInt8_t*)&crcr );
#ifdef PIPE_PARANOIA
		fReadCount += sizeof(long);
		if ( fReadCount != fPubSubCom.GetReadCount() )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Read Count Inconsistency" )
			<< "Pipe communication object thinks it has read " << AliHLTLog::kDec 
			<< fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
			<< fReadCount << " bytes." << ENDLOG;
		    }
#endif
		if ( crcr != crcc )
		    {
		    LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "PingAck CRC" )
			<< "CRC error when trying to read PingAck message. Read CRC: "
			<< AliHLTLog::kHex << crcr << " - Calculated CRC: " << crcc << "." 
			<< ENDLOG;
		    }
#endif
		interface.PingAck( *this );
		break;
		}
	    default:
		break;
	    }
	pbh->fLength = pbhSize;
	}
    while ( !fQuitLoop );
    if ( pbh )
	delete [] (AliUInt8_t*)pbh;
    fPubSubCom.Close();
    fPubSubComOpened = false;
    fLoopQuitted = true;
    return 0;
    }


int AliHLTPublisherPipeProxy::HandleNewEventMsg( AliHLTSubscriberInterface& interface, AliHLTPubSubPipeMsg* msg )
    {
    MLUCLIMITTIMER_NAME( timer0, MLUCLIMITTIMER_DEFAULT_TIMER_LIMIT_US );
    MLUCLIMITTIMER_NAME( timer1, MLUCLIMITTIMER_DEFAULT_TIMER_LIMIT_US );
    int ret;
    char name[9], turnedName[9];
#ifdef PIPECOM_DOCRC
    long crcc = 0, crcr;
#endif
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    // Check message parameter count
    if ( msg->fParamCnt != 3 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "NewEvent msg", 100 )
	    << "Unexpected parameter count for NewEvent message: " << msg->fParamCnt
	    << " instead of 3" << ENDLOG;
	return 0;
	}
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    AliHLTBlockHeader *pbh;
    // Read first message parameter descriptor (event ID) (parameter included)
    pbh = (AliHLTBlockHeader*)fParEvtID;
    ret = fPubSubCom.Read( pbh );
    //parEvtID = (AliHLTPubSubPipeMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "NewEvent msg parameter")
	    << "Error reading NewEvent message event ID parameter from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fParEvtID )
	{
	// 	LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "NewEvent msg")
	// 	    << "New event message allocated by pipe com object." << ENDLOG;
	if ( fParEvtID )
	    delete [] (AliUInt8_t*)fParEvtID;
	fParEvtID = (AliHLTPubSubPipeMsgParam*)pbh;
	fParEvtIDSize = pbh->fLength;
	}
    else
	pbh->fLength = fParEvtIDSize;
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fParEvtID->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)fParEvtID->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "NewEvent msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fParEvtID->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting pipe message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}
    if ( fParEvtID->fType != AliHLTPubSubPipeMsgParam::kEvtID )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "NewEvent msg parameter type", 100 )
	    << "Unexpected message parameter type for NewEvent message: " << AliHLTLog::kDec 
	    << (int)fParEvtID->fType << " when expecting " << (int)AliHLTPubSubPipeMsgParam::kEvtID << ENDLOG;
	return 0;
	}
    // Read second message parameter descriptor (SubEventDataDescriptor)
    pbh = (AliHLTBlockHeader*)fHNEMParSEDD;
    ret = fPubSubCom.Read( pbh );
    //parSEDD = (AliHLTPubSubPipeMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "NewEvent msg parameter")
	    << "Error reading NewEvent message subevent data descriptor parameter from pipe. " 
	    << "Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( (AliHLTBlockHeader*)fHNEMParSEDD != pbh )
	{
	if ( fHNEMParSEDD )
	    delete [] (AliUInt8_t*)fHNEMParSEDD;
	fHNEMParSEDD = (AliHLTPubSubPipeMsgParam*)pbh;
	fHNEMParSEDDSize = pbh->fLength;
	}
    else
	pbh->fLength = fHNEMParSEDDSize;
	
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fHNEMParSEDD->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)fHNEMParSEDD->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "NewEvent msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fHNEMParSEDD->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting pipe message parameter 1 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}
    if ( fHNEMParSEDD->fType != AliHLTPubSubPipeMsgParam::kSEDD )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "NewEvent msg parameter type", 100 )
	    << "Unexpected message parameter type for NewEvent message: " << AliHLTLog::kDec 
	    << (int)fHNEMParSEDD->fType << " when expecting " << (int)AliHLTPubSubPipeMsgParam::kSEDD << ENDLOG;
	return 0;
	}
    // Read second message parameter 
    pbh = (AliHLTBlockHeader*)fHNEMSEDD;
    if ( pbh )
	pbh->fLength = fHNEMSEDDSize;
    ret = fPubSubCom.Read( pbh );
    //sedd = (AliHLTSubEventDataDescriptor*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "NewEvent msg parameter")
	    << "Error reading NewEvent message subevent data descriptor from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fHNEMSEDD )
	{
	if ( fHNEMSEDD )
	    delete [] (AliUInt8_t*)fHNEMSEDD;
	fHNEMSEDD = (AliHLTSubEventDataDescriptor*)pbh;
	fHNEMSEDDSize = pbh->fLength;
	}
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fHNEMSEDD->fHeader.fType.fID != ALIL3SUBEVENTDATADESCRIPTOR_TYPE )
	{
	sample.fType.fID = ALIL3SUBEVENTDATADESCRIPTOR_TYPE;
	strncpy( name, (char*)fHNEMSEDD->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "NewEvent msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fHNEMSEDD->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting SubEventDataDescriptor (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}


    // Read third message parameter descriptor (Event Trigger (Type) Structure)
    pbh = (AliHLTBlockHeader*)fHNEMParETS;
    ret = fPubSubCom.Read( pbh );
    //parETS = (AliHLTPubSubPipeMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "NewEvent msg parameter")
	    << "Error reading NewEvent message event trigger type parameter from pipe. " 
	    << "Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fHNEMParETS )
	{
	if ( fHNEMParETS )
	    delete [] (AliUInt8_t*)fHNEMParETS;
	fHNEMParETS = (AliHLTPubSubPipeMsgParam*)pbh;
	fHNEMParETSSize = pbh->fLength;
	}
    else
	pbh->fLength = fHNEMParETSSize;

#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fHNEMParETS->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)fHNEMParETS->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "NewEvent msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fHNEMParETS->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting pipe message parameter 2 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}
    if ( fHNEMParETS->fType != AliHLTPubSubPipeMsgParam::kETT )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "NewEvent msg parameter type", 100 )
	    << "Unexpected message parameter type for NewEvent message: " << AliHLTLog::kDec 
	    << (int)fHNEMParETS->fType << " when expecting " << (int)AliHLTPubSubPipeMsgParam::kETT << ENDLOG;
	return 0;
	}
    // Read third message parameter 
    pbh = (AliHLTBlockHeader*)fHNEMETS;
    if ( pbh )
	pbh->fLength = fHNEMETSSize;
    ret = fPubSubCom.Read( pbh );
    //ets = (AliHLTEventTriggerStruct*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "NewEvent msg parameter")
	    << "Error reading NewEvent message subevent data descriptor from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fHNEMETS )
	{
	if ( fHNEMETS )
	    delete [] (AliUInt8_t*)fHNEMETS;
	fHNEMETS = (AliHLTEventTriggerStruct*)pbh;
	fHNEMETSSize = pbh->fLength;
	}
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fHNEMETS->fHeader.fType.fID != ALIL3EVENTTRIGGERSTRUCT_TYPE )
	{
	sample.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
	strncpy( name, (char*)fHNEMETS->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "NewEvent msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fHNEMETS->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting SubEventDataDescriptor (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}


#ifdef PIPECOM_DOCRC
    fPubSubCom.Read( sizeof(long), (AliUInt8_t*)&crcr );
    if ( crcr != crcc )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "NewEvent msg CRC" )
	    << "CRC error when trying to read newEvent message. Read CRC: "
	    << AliHLTLog::kHex << crcr << " - Calculated CRC: " << crcc << "." 
	    << ENDLOG;
	}
#ifdef PIPE_PARANOIA
    fReadCount += sizeof(long);
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
#endif

#ifdef EVENT_CHECK_PARANOIA
    if ( fLastEventAnnouncedID!=~(AliEventID_t)0 )
	{
	if ( fLastEventAnnouncedID+EVENT_CHECK_PARANOIA_LIMIT<(AliEventID_t)fParEvtID->fData || (fLastEventAnnouncedID>EVENT_CHECK_PARANOIA_LIMIT && fLastEventAnnouncedID-EVENT_CHECK_PARANOIA_LIMIT>(AliEventID_t)fParEvtID->fData) )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy::HandleNewEventMsg", "Suspicious Event ID" )
		<< "Suspicious Event ID found: 0x" << AliHLTLog::kHex << (AliEventID_t)fParEvtID->fData << " ("
		<< AliHLTLog::kDec << (AliEventID_t)fParEvtID->fData << ") - last announced event ID: 0x"
		<< AliHLTLog::kHex << fLastEventAnnouncedID << " (" << AliHLTLog::kDec 
		<< fLastEventAnnouncedID << ")." << ENDLOG;
	    }
	}
    fLastEventAnnouncedID = (AliEventID_t)fParEvtID->fData;
#endif

    LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "NewEvent" )
	<< GetName() << " NewEvent 0x" << AliHLTLog::kHex << AliEventID_t( fParEvtID->fDataType, fParEvtID->fData )
	<< " (" << AliHLTLog::kDec << AliEventID_t( fParEvtID->fDataType, fParEvtID->fData ) << ")." 
	<< ENDLOG;
    if ( AliEventID_t( fParEvtID->fDataType, fParEvtID->fData ) != fHNEMSEDD->fEventID )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy::HandleNewEventMsg::NewEvent", "Incoherent Event ID" )
	    << "Msg event ID 0x" << AliHLTLog::kHex << AliEventID_t( fParEvtID->fDataType, fParEvtID->fData ) << " ("
	    << AliHLTLog::kDec << AliEventID_t( fParEvtID->fDataType, fParEvtID->fData ) << ") unequal sedd eventID: 0x"
	    << AliHLTLog::kHex << fHNEMSEDD->fEventID << " ("
	    << AliHLTLog::kDec << fHNEMSEDD->fEventID << ")." << ENDLOG;
	}
    LOG( AliHLTLog::kDebug, "AliHLTSubscriberPipeProxy::NewEvent", "NewEvent" )
	<< AliHLTLog::kHex << GetName() << " susbcriber pipe proxy received event 0x" << AliEventID_t( fParEvtID->fDataType, fParEvtID->fData )
	<< " (" << AliHLTLog::kDec << AliEventID_t( fParEvtID->fDataType, fParEvtID->fData ) << ") with " << fHNEMSEDD->fDataBlockCount 
	<< " data blocks." << ENDLOG;

    MLUCLIMITTIMER_NAME_STOP( timer1 );

    if ( fEventAccounting )
	fEventAccounting->EventEntered( fProxyName.c_str(), fHNEMSEDD->fEventID );

    if ( fEventAccounting )
	fEventAccounting->NewEvent( fProxyName.c_str(), fHNEMSEDD );

    interface.NewEvent( *this, AliEventID_t( fParEvtID->fDataType, fParEvtID->fData ), *fHNEMSEDD, *fHNEMETS );
    return 0;
    }


int AliHLTPublisherPipeProxy::HandleEventCanceledMsg( AliHLTSubscriberInterface& interface, AliHLTPubSubPipeMsg* msg )
    {
#ifdef PIPECOM_DOCRC
    long crcc = 0, crcr;
#endif
    int ret;
    char name[9], turnedName[9];
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    // Check message parameter count
    if ( msg->fParamCnt != 1 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "EventCanceled msg", 100 )
	    << "Unexpected parameter count for EventCanceled message: " << msg->fParamCnt
	    << " instead of 1" << ENDLOG;
	return 0;
	}
    AliHLTBlockHeader* pbh;
    pbh = (AliHLTBlockHeader*)fParEvtID;
    ret = fPubSubCom.Read( pbh );
    //parEvtID = (AliHLTPubSubPipeMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "EventCanceled msg parameter")
	    << "Error reading EventCanceled message event ID parameter from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fParEvtID )
	{
	if ( fParEvtID )
	    delete [] (AliUInt8_t*)fParEvtID;
	fParEvtID = (AliHLTPubSubPipeMsgParam*)pbh;
	fParEvtIDSize = pbh->fLength;
	}
    else
	pbh->fLength = fParEvtIDSize;
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fParEvtID->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)fParEvtID->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "EventCanceled msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fParEvtID->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting pipe message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}
    if ( fParEvtID->fType != AliHLTPubSubPipeMsgParam::kEvtID )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "EventCanceled msg parameter type", 100 )
	    << "Unexpected message parameter type for EventCanceled message: " << AliHLTLog::kDec 
	    << (int)fParEvtID->fType << " when expecting " << (int)AliHLTPubSubPipeMsgParam::kEvtID << ENDLOG;
	return 0;
	}
#ifdef PIPECOM_DOCRC
    fPubSubCom.Read( sizeof(long), (AliUInt8_t*)&crcr );
    if ( crcr != crcc )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "EventCanceled msg CRC" )
	    << "CRC error when trying to read EventCanceled message. Read CRC: "
	    << AliHLTLog::kHex << crcr << " - Calculated CRC: " << crcc << "." 
	    << ENDLOG;
	}
#ifdef PIPE_PARANOIA
    fReadCount += sizeof(long);
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
#endif
    interface.EventCanceled( *this, AliEventID_t( fParEvtID->fDataType, fParEvtID->fData ) );
    return 0;
    }

int AliHLTPublisherPipeProxy::HandleEventDoneDataMsg( AliHLTSubscriberInterface& interface, AliHLTPubSubPipeMsg* msg )
    {
    MLUCLIMITTIMER_NAME( timer0, MLUCLIMITTIMER_DEFAULT_TIMER_LIMIT_US );
    MLUCLIMITTIMER_NAME( timer1, MLUCLIMITTIMER_DEFAULT_TIMER_LIMIT_US );
#ifdef PIPECOM_DOCRC
    long crcc = 0, crcr;
#endif
    int ret;
    char name[9], turnedName[9];
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    if ( msg->fParamCnt != 1 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "EventDoneData msg", 100 )
	    << "Unexpected parameter count for EventDoneData message: " << msg->fParamCnt
	    << " instead of 1" << ENDLOG;
	return 0;
	}
#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)msg, fCRCTable );
#endif
    AliHLTBlockHeader *pbh;
    pbh = (AliHLTBlockHeader*)fParEvtID;
    ret = fPubSubCom.Read( pbh );
    //parEvtID = (AliHLTPubSubPipeMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "EventDoneData msg parameter")
	    << "Error reading EventDoneData message bool parameter from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fParEvtID )
	{
	if ( fParEvtID )
	    delete [] (AliUInt8_t*)fParEvtID;
	fParEvtID = (AliHLTPubSubPipeMsgParam*)pbh;
	fParEvtIDSize = pbh->fLength;
	}
    else
	pbh->fLength = fParEvtIDSize;

#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif
    if ( fParEvtID->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)fParEvtID->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "EventDoneData msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fParEvtID->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting pipe message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}
    if ( fParEvtID->fType != AliHLTPubSubPipeMsgParam::kEvtDoneD )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "EventDoneData msg parameter type", 100 )
	    << "Unexpected message parameter type for EventDoneData message: " << AliHLTLog::kDec 
	    << (int)fParEvtID->fType << " when expecting " << (int)AliHLTPubSubPipeMsgParam::kEvtID << ENDLOG;
	return 0;
	}
    pbh = (AliHLTBlockHeader*)fHEDMEDD;
    if ( fHEDMEDD )
	pbh->fLength = fHEDMEDDSize;
    ret = fPubSubCom.Read( pbh );
    //edd = (AliHLTEventDoneData*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherPipeProxy", "EventDoneData msg parameter")
	    << "Error reading EventDoneData message data from pipe. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
#ifdef PIPE_PARANOIA
    fReadCount += pbh->fLength;
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
    if ( pbh != (AliHLTBlockHeader*)fHEDMEDD )
	{
	if ( fHEDMEDD )
	    delete [] (AliUInt8_t*)fHEDMEDD;
	fHEDMEDD = (AliHLTEventDoneData*)pbh;
	fHEDMEDDSize = pbh->fLength;
	}

#ifdef PIPECOM_DOCRC
    crcc = MakeCRC( crcc, (AliHLTBlockHeader*)pbh, fCRCTable );
#endif

    if ( fHEDMEDD->fHeader.fType.fID != ALIL3EVENTDONEDATA_TYPE )
	{
	sample.fType.fID = ALIL3EVENTDONEDATA_TYPE;
	strncpy( name, (char*)fHEDMEDD->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTPublisherPipeProxy", "EventDoneData msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << fHEDMEDD->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from pipe when expecting event done data (" 
	    << sample.fType.fID << ")" << ENDLOG;
	return 0;
	}

#ifdef PIPECOM_DOCRC
    fPubSubCom.Read( sizeof(long), (AliUInt8_t*)&crcr );
    if ( crcr != crcc )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "EventDoneData msg CRC" )
	    << "CRC error when trying to read EventDoneData message. Read CRC: "
	    << AliHLTLog::kHex << crcr << " - Calculated CRC: " << crcc << "." 
	    << ENDLOG;
	}
#ifdef PIPE_PARANOIA
    fReadCount += sizeof(long);
    if ( fReadCount != fPubSubCom.GetReadCount() )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherPipeProxy", "Read Count Inconsistency" )
	    << "Pipe communication object thinks it has read " << AliHLTLog::kDec 
	    << fPubSubCom.GetReadCount() << " bytes, while I think it should have read "
	    << fReadCount << " bytes." << ENDLOG;
	}
#endif
#endif

    LOG( AliHLTLog::kDebug, "AliHLTPublisherPipeProxy", "EventDoneData msg" )
	<< "EventDoneData event: 0x" << AliHLTLog::kHex << (AliEventID_t)fHEDMEDD->fEventID << " (" 
	<< AliHLTLog::kDec << (AliEventID_t)fHEDMEDD->fEventID << ")." << ENDLOG;

    MLUCLIMITTIMER_NAME_STOP( timer1 );

    if ( fEventAccounting )
	fEventAccounting->EventDoneData( fProxyName.c_str(), fHEDMEDD->fEventID, fHEDMEDD->fDataWordCount );

    interface.EventDoneData( *this, *fHEDMEDD );
    return 0;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
