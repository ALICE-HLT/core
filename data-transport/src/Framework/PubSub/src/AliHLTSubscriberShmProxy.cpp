/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSubscriberShmProxy.hpp"
#include "AliHLTPublisherInterface.hpp"
#include "AliHLTEventTriggerStruct.h"
#include "AliHLTLog.hpp"
#include "AliHLTStackTrace.hpp"
#include <errno.h>




AliHLTSubscriberShmProxy::AliHLTSubscriberShmProxy( const char* name, AliUInt32_t shmSize, 
						  key_t pub2SubKey, key_t sub2PubKey ):
    AliHLTSubscriberProxyInterface( name ), 
    fPubToSub( pub2SubKey, shmSize, false ), fSubToPub( sub2PubKey, shmSize, false ), 
    fPubToSubKey( pub2SubKey ), fSubToPubKey( sub2PubKey )
    {
    pthread_mutex_init( &fMutex, NULL );
    fQuitted = false;
    fQuit = false;
    // XXX
    // Timeout initialization
    fPubToSubTimeout = fSubToPub.GetTimeout();
    fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );
    }


AliHLTSubscriberShmProxy::~AliHLTSubscriberShmProxy()
    {
    pthread_mutex_destroy( &fMutex );
    }


int AliHLTSubscriberShmProxy::NewEvent(  AliHLTPublisherInterface&, AliEventID_t eventID, 
					 const AliHLTSubEventDataDescriptor& sbevent,
					 const AliHLTEventTriggerStruct& etstruct )
    {
    int ret;
    AliUInt32_t size, size1;
    AliUInt8_t *p1, *p2;
    AliHLTPubSubShmMsg* msg;
    AliHLTPubSubShmMsgParam *par1, *par2, *par3;
    AliHLTSubEventDataDescriptor* sedd;
    AliHLTEventTriggerStruct* ett;
    pthread_mutex_lock( &fMutex );
    size = sizeof(AliHLTPubSubShmMsg)+1+3*sizeof(AliHLTPubSubShmMsgParam)+sbevent.fHeader.fLength+etstruct.fHeader.fLength;
    ret=fSubToPub.CanWrite( size, p1, p2, size1 );
    if ( ret==0 )
	{
	if ( !p2 )
	    {
	    msg = (AliHLTPubSubShmMsg*)p1;
	    par1 = (AliHLTPubSubShmMsgParam*)( p1 + sizeof(AliHLTPubSubShmMsg) + 1 );
	    par2 = (AliHLTPubSubShmMsgParam*)( p1 + sizeof(AliHLTPubSubShmMsg) + 1 + 
					      sizeof(AliHLTPubSubShmMsgParam) );
	    sedd = (AliHLTSubEventDataDescriptor*)( p1 + sizeof(AliHLTPubSubShmMsg) + 1 + 
						    2*sizeof(AliHLTPubSubShmMsgParam) );
	    par3 = (AliHLTPubSubShmMsgParam*)( p1 + sizeof(AliHLTPubSubShmMsg) + 1 + 
					       2*sizeof(AliHLTPubSubShmMsgParam) +
					       sbevent.fHeader.fLength );
	    ett = (AliHLTEventTriggerStruct*)( p1 + sizeof(AliHLTPubSubShmMsg) + 1 + 
					       3*sizeof(AliHLTPubSubShmMsgParam) +
					       sbevent.fHeader.fLength );
	  
	    MakeMsg( msg, AliHLTPubSubShmMsg::kNewEvent, 3 );
	    MakeParam( par1, eventID );
	    MakeParam( par2, sbevent );
	    memcpy( sedd, &sbevent, sbevent.fHeader.fLength );
	    MakeParam( par3, etstruct );
	    memcpy( ett, &etstruct, etstruct.fHeader.fLength );
	    }
	else
	    {
	    //AliUInt32_t size2 = size-sbevent.fHeader.fLength;
	    //AliUInt8_t msgData[ size2 ];
	    AliUInt8_t msgData[ size ];
	    msg = (AliHLTPubSubShmMsg*)msgData;
	    par1 = (AliHLTPubSubShmMsgParam*)( msgData + sizeof(AliHLTPubSubShmMsg) + 1 );
	    par2 = (AliHLTPubSubShmMsgParam*)( msgData + sizeof(AliHLTPubSubShmMsg) + 1 + 
					      sizeof(AliHLTPubSubShmMsgParam) );
	    sedd = (AliHLTSubEventDataDescriptor*)( msgData + sizeof(AliHLTPubSubShmMsg) + 1 + 
						    2*sizeof(AliHLTPubSubShmMsgParam) );
	    par3 = (AliHLTPubSubShmMsgParam*)( msgData + sizeof(AliHLTPubSubShmMsg) + 1 + 
					       2*sizeof(AliHLTPubSubShmMsgParam) +
					       sbevent.fHeader.fLength );
	    ett = (AliHLTEventTriggerStruct*)( msgData + sizeof(AliHLTPubSubShmMsg) + 1 + 
					       3*sizeof(AliHLTPubSubShmMsgParam) +
					       sbevent.fHeader.fLength );
	  
	  
	    MakeMsg( msg, AliHLTPubSubShmMsg::kNewEvent, 3 );
	    MakeParam( par1, eventID );
	    MakeParam( par2, sbevent );
	    memcpy( sedd, &sbevent, sbevent.fHeader.fLength );
	    MakeParam( par3, etstruct );
	    memcpy( ett, &etstruct, etstruct.fHeader.fLength );

	    memcpy( p1, msgData, size1 );
	    memcpy( p2, msgData+size1, size-size1 );
	  
// 	    if ( size2 <= size1 )
// 		{
// 		memcpy( p1, msgData, size2 );
// 		memcpy( p1+size2, &sbevent, size1-size2 );
// 		memcpy( p2, ((AliUInt8_t*)(&sbevent))+size1-size2, sbevent.fHeader.fLength-(size1-size2) );
// 		}
// 	    else
// 		{
// 		memcpy( p1, msgData, size1 );
// 		memcpy( p2, msgData+size1, size2-size1 );
// 		memcpy( p2+size2-size1, ((AliUInt8_t*)(&sbevent)), sbevent.fHeader.fLength );
// 		}
	    }
	fSubToPub.HaveWritten( size, p1, p2, size1 );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy::NewEvent", "Unable to write NewEvent message" )
	    << "Unable to write NewEvent message (" << AliHLTLog::kDec << size
	    << " Bytes) to subscriber (Sub-To-Pub shm key 0x" << AliHLTLog::kHex << (int)fSubToPubKey
	    << " (" << AliHLTLog::kDec << (int)fSubToPubKey << ")): " << strerror(ret) << " (" 
	    << ret << ")." << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    pthread_mutex_unlock( &fMutex );
    return 0;
    }



int AliHLTSubscriberShmProxy::EventCanceled(  AliHLTPublisherInterface&, AliEventID_t eventID )
    {
    int ret;
    AliUInt32_t size, size1;
    AliUInt8_t *p1, *p2;
    AliHLTPubSubShmMsg* msg;
    AliHLTPubSubShmMsgParam *par1;
    pthread_mutex_lock( &fMutex );
    size = sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam);
    ret=fSubToPub.CanWrite( size, p1, p2, size1 );
    if ( ret==0 )
	{
	if ( !p2 )
	    {
	    msg = (AliHLTPubSubShmMsg*)p1;
	    par1 = (AliHLTPubSubShmMsgParam*)( p1 + sizeof(AliHLTPubSubShmMsg) + 1 );
	  
	    MakeMsg( msg, AliHLTPubSubShmMsg::kEventCanceled, 1 );
	    MakeParam( par1, eventID );
	    }
	else
	    {
	    AliUInt8_t msgData[ size ];
	    msg = (AliHLTPubSubShmMsg*)msgData;
	    par1 = (AliHLTPubSubShmMsgParam*)( msgData + sizeof(AliHLTPubSubShmMsg) + 1 );
	  
	    MakeMsg( msg, AliHLTPubSubShmMsg::kEventCanceled, 1 );
	    MakeParam( par1, eventID );
	  
	    memcpy( p1, msgData, size1 );
	    memcpy( p2, msgData+size1, size-size1 );
	    }
	fSubToPub.HaveWritten( size, p1, p2, size1 );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy::EventCanceled", "Unable to write EventCanceled message" )
	    << "Unable to write EventCanceled message (" << AliHLTLog::kDec << size
	    << " Bytes) to subscriber (Sub-To-Pub shm key 0x" << AliHLTLog::kHex << (int)fSubToPubKey
	    << " (" << AliHLTLog::kDec << (int)fSubToPubKey << ")): " << strerror(ret) << " (" 
	    << ret << ")." << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    pthread_mutex_unlock( &fMutex );
    return 0;
    }

int AliHLTSubscriberShmProxy::EventDoneData( AliHLTPublisherInterface&, 
					     const AliHLTEventDoneData& eventDoneData )
    {
//     if ( !fSubToPub )
// 	{
// 	LOG( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "EventDoneSing")
// 	    << "EventDoneSing attempt without open pub-to-sub shared memory." << ENDLOG;
// 	return EIO;
// 	}
    pthread_mutex_lock( &fMutex );
    AliHLTPubSubShmMsg* msg;
    AliHLTPubSubShmMsgParam* param;
    AliHLTEventDoneData* edd;

    AliUInt32_t size, size1;
    AliUInt8_t *p1, *p2;
    int ret;

    size = sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam)+eventDoneData.fHeader.fLength;

    ret = fSubToPub.CanWrite( size, p1, p2, size1 );
    if ( ret == 0 )
	{
	if ( !p2 )
	    {
	    msg = (AliHLTPubSubShmMsg*)p1;
	    param = (AliHLTPubSubShmMsgParam*)( p1+sizeof(AliHLTPubSubShmMsg)+1 );
	    edd = (AliHLTEventDoneData*)(p1+sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam));
	    MakeMsg( msg, AliHLTPubSubShmMsg::kEventDoneData, 1 );
	    MakeParam( param, eventDoneData );
	    memcpy( edd, &eventDoneData, eventDoneData.fHeader.fLength );
	    }
	else
	    {
	    AliUInt8_t msgData[ size ];
	    msg = (AliHLTPubSubShmMsg*)msgData;
	    param = (AliHLTPubSubShmMsgParam*)( msgData+sizeof(AliHLTPubSubShmMsg)+1 );
	    edd = (AliHLTEventDoneData*)( msgData+sizeof(AliHLTPubSubShmMsg)+1+sizeof(AliHLTPubSubShmMsgParam) );

	    MakeMsg( msg, AliHLTPubSubShmMsg::kEventDoneData, 1 );
	    MakeParam( param, eventDoneData );
	    memcpy( edd, &eventDoneData, eventDoneData.fHeader.fLength );
		
	    memcpy( p1, msgData, size1 );
	    memcpy( p2, msgData+size1, size-size1 );
	    }
	fSubToPub.HaveWritten( size, p1, p2, size1 );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy", "EventDoneData message send")
	    << "Unable to write EventDoneData message ("
	     << AliHLTLog::kDec << size
	    << " Bytes) to subscriber (Pub-To-Sub shm key 0x" << AliHLTLog::kHex << (int)fSubToPubKey
	    << " (" << AliHLTLog::kDec << (int)fSubToPubKey << ")): " 
	    << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    pthread_mutex_unlock( &fMutex );
    return ret;
    }

int AliHLTSubscriberShmProxy::SubscriptionCanceled( AliHLTPublisherInterface& )
    {
    int ret;
    AliUInt32_t size, size1;
    AliUInt8_t *p1, *p2;
    AliHLTPubSubShmMsg* msg;
    pthread_mutex_lock( &fMutex );
    size = sizeof(AliHLTPubSubShmMsg)+1;
    ret=fSubToPub.CanWrite( size, p1, p2, size1 );
    if ( ret==0 )
	{
	if ( !p2 )
	    {
	    msg = (AliHLTPubSubShmMsg*)p1;
	  
	    MakeMsg( msg, AliHLTPubSubShmMsg::kSubscriptionCanceled, 0 );
	    }
	else
	    {
	    AliUInt8_t msgData[ size ];
	    msg = (AliHLTPubSubShmMsg*)msgData;
	  
	    MakeMsg( msg, AliHLTPubSubShmMsg::kSubscriptionCanceled, 0 );
	  
	    memcpy( p1, msgData, size1 );
	    memcpy( p2, msgData+size1, size-size1 );
	    }
	fSubToPub.HaveWritten( size, p1, p2, size1 );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy::SubscriptionCanceled", "Unable to write SubscriptionCanceled message" )
	    << "Unable to write SubscriptionCanceled message (" << AliHLTLog::kDec << size
	    << " Bytes) to subscriber (Sub-To-Pub shm key 0x" << AliHLTLog::kHex << (int)fSubToPubKey
	    << " (" << AliHLTLog::kDec << (int)fSubToPubKey << ")): " << strerror(ret) << " (" 
	    << ret << ")." << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    pthread_mutex_unlock( &fMutex );
    return 0;
    }

int AliHLTSubscriberShmProxy::ReleaseEventsRequest( AliHLTPublisherInterface& )
    {
    int ret;
    AliUInt32_t size, size1;
    AliUInt8_t *p1, *p2;
    AliHLTPubSubShmMsg* msg;
    pthread_mutex_lock( &fMutex );
    size = sizeof(AliHLTPubSubShmMsg)+1;
    ret=fSubToPub.CanWrite( size, p1, p2, size1 );
    if ( ret==0 )
	{
	if ( !p2 )
	    {
	    msg = (AliHLTPubSubShmMsg*)p1;
	  
	    MakeMsg( msg, AliHLTPubSubShmMsg::kReleaseEventsRequest, 0 );
	    }
	else
	    {
	    AliUInt8_t msgData[ size ];
	    msg = (AliHLTPubSubShmMsg*)msgData;
	  
	    MakeMsg( msg, AliHLTPubSubShmMsg::kReleaseEventsRequest, 0 );
	  
	    memcpy( p1, msgData, size1 );
	    memcpy( p2, msgData+size1, size-size1 );
	    }
	fSubToPub.HaveWritten( size, p1, p2, size1 );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy::ReleaseEventsRequest", "Unable to write ReleaseEventsRequest message" )
	    << "Unable to write ReleaseEventsRequest message (" << AliHLTLog::kDec << size
	    << " Bytes) to subscriber (Sub-To-Pub shm key 0x" << AliHLTLog::kHex << (int)fSubToPubKey
	    << " (" << AliHLTLog::kDec << (int)fSubToPubKey << ")): " << strerror(ret) << " (" 
	    << ret << ")." << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    pthread_mutex_unlock( &fMutex );
    return 0;
    }

int AliHLTSubscriberShmProxy::Ping( AliHLTPublisherInterface& )
    {
    int ret;
    AliUInt32_t size, size1;
    AliUInt8_t *p1, *p2;
    AliHLTPubSubShmMsg* msg;
    pthread_mutex_lock( &fMutex );
    size = sizeof(AliHLTPubSubShmMsg)+1;
    ret=fSubToPub.CanWrite( size, p1, p2, size1 );
    if ( ret==0 )
	{
	if ( !p2 )
	    {
	    msg = (AliHLTPubSubShmMsg*)p1;
	  
	    MakeMsg( msg, AliHLTPubSubShmMsg::kPing, 0 );
	    }
	else
	    {
	    AliUInt8_t msgData[ size ];
	    msg = (AliHLTPubSubShmMsg*)msgData;
	  
	    MakeMsg( msg, AliHLTPubSubShmMsg::kPing, 0 );
	  
	    memcpy( p1, msgData, size1 );
	    memcpy( p2, msgData+size1, size-size1 );
	    }
	fSubToPub.HaveWritten( size, p1, p2, size1 );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy::Ping", "Unable to write Ping message" )
	    << "Unable to write Ping message (" << AliHLTLog::kDec << size
	    << " Bytes) to subscriber (Sub-To-Pub shm key 0x" << AliHLTLog::kHex << (int)fSubToPubKey
	    << " (" << AliHLTLog::kDec << (int)fSubToPubKey << ")): " << strerror(ret) << " (" 
	    << ret << ")." << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    pthread_mutex_unlock( &fMutex );
    return 0;
    }

int AliHLTSubscriberShmProxy::PingAck( AliHLTPublisherInterface& )
    {
    int ret;
    AliUInt32_t size, size1;
    AliUInt8_t *p1, *p2;
    AliHLTPubSubShmMsg* msg;
    pthread_mutex_lock( &fMutex );
    size = sizeof(AliHLTPubSubShmMsg)+1;
    ret=fSubToPub.CanWrite( size, p1, p2, size1 );
    if ( ret==0 )
	{
	if ( !p2 )
	    {
	    msg = (AliHLTPubSubShmMsg*)p1;
	  
	    MakeMsg( msg, AliHLTPubSubShmMsg::kPingAck, 0 );
	    }
	else
	    {
	    AliUInt8_t msgData[ size ];
	    msg = (AliHLTPubSubShmMsg*)msgData;
	  
	    MakeMsg( msg, AliHLTPubSubShmMsg::kPingAck, 0 );
	  
	    memcpy( p1, msgData, size1 );
	    memcpy( p2, msgData+size1, size-size1 );
	    }
	fSubToPub.HaveWritten( size, p1, p2, size1 );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy::PingAck", "Unable to write PingAck message" )
	    << "Unable to write PingAck message (" << AliHLTLog::kDec << size
	    << " Bytes) to subscriber (Sub-To-Pub shm key 0x" << AliHLTLog::kHex << (int)fSubToPubKey
	    << " (" << AliHLTLog::kDec << (int)fSubToPubKey << ")): " << strerror(ret) << " (" 
	    << ret << ")." << ENDLOG;
	pthread_mutex_unlock( &fMutex );
	return ret;
	}
    pthread_mutex_unlock( &fMutex );
    return 0;
    }

void AliHLTSubscriberShmProxy::SetTimeout( AliUInt32_t timeout_usec )
    {
    fSubToPub.SetTimeout( timeout_usec );
    fPubToSubTimeout = timeout_usec;
    }


void AliHLTSubscriberShmProxy::MakeMsg( AliHLTPubSubShmMsg* msg, AliHLTPubSubShmMsg::MsgType type,
				       AliUInt32_t paramCnt )
    {
    int sz = sizeof(AliHLTPubSubShmMsg)+1;
    if ( !msg )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy", "Message NULL pointer")
	    << "Null pointer message passed for subscriber " << GetName() << ENDLOG;
	return;
	}
    msg->fHeader.fLength = sz;
    msg->fHeader.fType.fID = ALIL3PUBSUBPIPEMSG_TYPE;
    msg->fHeader.fSubType.fID = 0;
    msg->fHeader.fVersion = 1;
    msg->fType = (AliUInt32_t)type;
    msg->fParamCnt = paramCnt;
    msg->fSubscriberName[0]=0;
    }

void AliHLTSubscriberShmProxy::MakeParam( AliHLTPubSubShmMsgParam* param, const AliHLTSubEventDataDescriptor& )
    {
    param->fHeader.fType.fID = ALIL3PUBSUBPIPEMSGPARAM_TYPE;
    param->fHeader.fSubType.fID = 0;
    param->fHeader.fVersion = 1;
    param->fHeader.fLength = sizeof(AliHLTPubSubShmMsgParam);
    param->fType = AliHLTPubSubShmMsgParam::kSEDD;
    param->fData = 0;
    }

void AliHLTSubscriberShmProxy::MakeParam( AliHLTPubSubShmMsgParam* param, const AliHLTEventTriggerStruct& )
    {
    param->fHeader.fType.fID = ALIL3PUBSUBPIPEMSGPARAM_TYPE;
    param->fHeader.fSubType.fID = 0;
    param->fHeader.fVersion = 1;
    param->fHeader.fLength = sizeof(AliHLTPubSubShmMsgParam);
    param->fType = AliHLTPubSubShmMsgParam::kETT;
    param->fData = 0;
    }

void AliHLTSubscriberShmProxy::MakeParam( AliHLTPubSubShmMsgParam* param, AliEventID_t val )
    {
    param->fHeader.fType.fID = ALIL3PUBSUBPIPEMSGPARAM_TYPE;
    param->fHeader.fSubType.fID = 0;
    param->fHeader.fVersion = 1;
    param->fHeader.fLength = sizeof(AliHLTPubSubShmMsgParam);
    param->fType = AliHLTPubSubShmMsgParam::kEvtID;
    param->fDataType = val.fType;
    param->fData = val.fNr;
    }

void AliHLTSubscriberShmProxy::MakeParam( AliHLTPubSubShmMsgParam* param, const AliHLTEventDoneData& )
    {
    param->fHeader.fType.fID = ALIL3PUBSUBPIPEMSGPARAM_TYPE;
    param->fHeader.fSubType.fID = 0;
    param->fHeader.fVersion = 1;
    param->fHeader.fLength = sizeof(AliHLTPubSubShmMsgParam);
    param->fType = AliHLTPubSubShmMsgParam::kEvtDoneD;
    param->fData = 0;
    }


int AliHLTSubscriberShmProxy::MsgLoop( AliHLTPublisherInterface& interface )
    {
    int ret;
    char name[9], turnedName[9];
    AliHLTBlockHeader *pbh, sample = { 0, {ALIL3PUBSUBPIPEMSG_TYPE}, {0}, 1 };
    AliHLTPubSubShmMsg *msg;
    fQuit = false;
    fQuitted = false;
    AliUInt8_t *msgP1, *msgP2;
    AliUInt32_t msgSize1;
    do
	{
	pbh = NULL;
	ret = fPubToSub.Read( pbh, msgP1, msgP2, msgSize1, true );
	msg = (AliHLTPubSubShmMsg*)pbh;
	if ( ret && ret!=ETIMEDOUT )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy::MsgLoop", "Msg loop read")
		<< "Error reading publisher (Subscriber " << GetName() << ") message from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	    return ret;
	    }
	if ( msg->fHeader.fType.fID != ALIL3PUBSUBPIPEMSG_TYPE )
	    {
	    strncpy( name, (const char*)msg->fHeader.fType.fDescr, 4 );
	    name[4] = 0;
	    turnedName[0] = name[3];
	    turnedName[1] = name[2];
	    turnedName[2] = name[1];
	    turnedName[3] = name[0];
	    turnedName[4] = 0;
	    LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "Msg loop", 100 )
		<< "Read unexpected datatype " << AliHLTLog::kHex << msg->fHeader.fType.fID
		<< " == " << name << " (" << turnedName << ")"
		<< " from shared memory when expecting message (" << sample.fType.fID << ")" << ENDLOG;
	    fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
	    continue;
	    }
	if ( ret != ETIMEDOUT )
	    {
	    switch ( msg->fType )
		{
		case AliHLTPubSubShmMsg::kUnsubscribe:
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberShmProxy", "Unsubscribe" )
			<< "Unsubscribe message received from subscriber" << ENDLOG;
		    if ( strcmp( GetName(), (const char*)msg->fSubscriberName ) )
			{
			msg->fSubscriberName[ msg->fHeader.fLength-sizeof(AliHLTPubSubShmMsg)-1 ] = 0;
			LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "Unsubscribe subscriber name", 100 )
			    << "Subscriber name '" << (const char*)msg->fSubscriberName 
			    << "'passed to Unsubscribe message differs from subscriber proxy name '"
			    << GetName() << "'" << ENDLOG;
			fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
			break;
			}
		    interface.Unsubscribe( *this );
		    fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
		    break;
		    }
		case AliHLTPubSubShmMsg::kSetPersistent:
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberShmProxy", "SetPersistent" )
			<< "SetPersistent message received from subscriber " << GetName() << ENDLOG;
		    ret = HandleSetPersistentMsg( interface, msg, msgP1, msgP2, msgSize1 );
		    if ( ret )
			return ret;
		    break;
		    }
		case AliHLTPubSubShmMsg::kSetEventDoneDataSend:
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberShmProxy", "SetEventDoneDataSend" )
			<< "SetEventDoneDataSend message received from subscriber " << GetName() << ENDLOG;
		    ret = HandleSetEventDoneDataSendMsg( interface, msg, msgP1, msgP2, msgSize1 );
		    if ( ret )
			return ret;
		    break;
		    }
		case AliHLTPubSubShmMsg::kSetEventTypeMod:
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberShmProxy", "SetEventTypeMod" )
			<< "SetEventTypeMod message received from subscriber " << GetName() << ENDLOG;
		    ret = HandleSetEventTypeModMsg( interface, msg, msgP1, msgP2, msgSize1 );
		    if ( ret )
			return ret;
		    break;
		    }
		case AliHLTPubSubShmMsg::kSetEventTypeETT:
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberShmProxy", "SetEventTypeMod" )
			<< "SetEventTypeMod message received from subscriber " << GetName() << ENDLOG;
		    ret = HandleSetEventTypeETTMsg( interface, msg, msgP1, msgP2, msgSize1 );
		    if ( ret )
			return ret;
		    break;
		    }
		case AliHLTPubSubShmMsg::kSetTransientTimeout:
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberShmProxy", "SetTransientTimeout" )
			<< "SetTransientTimeout message received from subscriber " << GetName() << ENDLOG;
		    ret = HandleSetTransientTimeoutMsg( interface, msg, msgP1, msgP2, msgSize1 );
		    if ( ret )
			return ret;
		    break;
		    }
		case AliHLTPubSubShmMsg::kEventDoneSing:
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberShmProxy", "EventDoneSing" )
			<< "EventDoneSing message received from subscriber " << GetName() << ENDLOG;
		    ret = HandleEventDoneSingMsg( interface, msg, msgP1, msgP2, msgSize1 );
		    if ( ret )
			return ret;
		    break;
		    }
		case AliHLTPubSubShmMsg::kEventDoneMult:
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberShmProxy", "EventDoneMult" )
			<< "EventDoneMult message received from subscriber " << GetName() << ENDLOG;
		    ret = HandleEventDoneMultMsg( interface, msg, msgP1, msgP2, msgSize1 );
		    if ( ret )
			return ret;
		    break;
		    }
		case AliHLTPubSubShmMsg::kStartPublishing:
		    {
		    bool sendOldEvents = false;
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberShmProxy", "StartPublishing" )
			<< "StartPublishing message received from subscriber " << GetName() << ENDLOG;
		    //fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
		    ret = HandleStartPublishingMsg( interface, msg, msgP1, msgP2, msgSize1, sendOldEvents );
		    if ( ret )
			return ret;
		    interface.StartPublishing( *this, sendOldEvents );
		    break;
		    }
		case AliHLTPubSubShmMsg::kPing:
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberShmProxy", "Ping" )
			<< "Ping message received from subscriber " << GetName() << ENDLOG;
		    interface.Ping( *this );
		    fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
		    break;
		    }
		case AliHLTPubSubShmMsg::kPingAck:
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberShmProxy", "PingAck" )
			<< "PingAck message received from subscriber " << GetName() << ENDLOG;
		    interface.PingAck( *this );
		    fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
		    break;
		    }
		case AliHLTPubSubShmMsg::kQuitLoop:
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberShmProxy::MsgLoop", "QuitLoop msg received" )
			<< "Received QuitLoop message." << ENDLOG;
		    fQuit = true;
		    fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
		    break;
		default:
		    LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy::MsgLoop", "Unknown msg received" )
			<< "Received unknown message!" << ENDLOG;
		    fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
		    break;
		}
	    }
	}
    while ( !fQuit );
    LOG( AliHLTLog::kDebug, "AliHLTSubscriberShmProxy::MsgLoop", "Loop ended" )
	<< "Msg loop ended." << ENDLOG;
    fQuitted = true;
    return 0;
    }


void AliHLTSubscriberShmProxy::QuitMsgLoop(bool wait)
    {
    fQuit = true;
    if ( wait )
	{
	struct timeval start, now;
	unsigned long long deltaT = 0;
	const unsigned long long timeLimit = 10000000;
	gettimeofday( &start, NULL );
	while ( !fQuitted && deltaT<timeLimit )
	    {
	    usleep( 50000 );
	    gettimeofday( &now, NULL );
	    deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	    // 	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy::QuitMsgLoop", "After usleep" )
	    // 	    << "After usleep." << ENDLOG;
	    }
	//     LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy::QuitMsgLoop", "Done" )
	// 	<< "Finished making msg loop quit.." << ENDLOG;
	}
    }


int AliHLTSubscriberShmProxy::HandleSetPersistentMsg( AliHLTPublisherInterface& interface, AliHLTPubSubShmMsg* msg, AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 )
    {
    int ret;
    char name[9], turnedName[9];
    AliUInt8_t *parBoolP1, *parBoolP2;
    AliUInt32_t parBoolSize1;
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    if ( msg->fParamCnt != 1 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "SetPersistent msg", 100 )
	    << "Unexpected parameter count for SetPersistent message: " << msg->fParamCnt
	    << " instead of 1" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
	return 0;
	}
    fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
    msg = NULL;
    AliHLTPubSubShmMsgParam * parBool;
    AliHLTBlockHeader *pbh;
    pbh = NULL;
    fPubToSub.SetTimeout( fPubToSubTimeout );
    ret = fPubToSub.Read( pbh, parBoolP1, parBoolP2, parBoolSize1 );
    fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );

    parBool = (AliHLTPubSubShmMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy", "SetPersistent msg parameter")
	    << "Error reading SetPersistent message bool parameter from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
    if ( parBool->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)parBool->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "SetPersistent msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << parBool->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parBool, parBoolP1, parBoolP2, parBoolSize1 );
	return 0;
	}
    if ( parBool->fType != AliHLTPubSubShmMsgParam::kBool )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "SetPersistent msg parameter type", 100 )
	    << "Unexpected message parameter type for SetPersistent message: " << AliHLTLog::kDec 
	    << (int)parBool->fType << " when expecting " << (int)AliHLTPubSubShmMsgParam::kBool << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parBool, parBoolP1, parBoolP2, parBoolSize1 );
	return 0;
	}

    interface.SetPersistent( *this, (bool)parBool->fData );
    fPubToSub.FreeRead( (AliHLTBlockHeader*)parBool, parBoolP1, parBoolP2, parBoolSize1 );
    return 0;
    }


int AliHLTSubscriberShmProxy::HandleSetEventDoneDataSendMsg( AliHLTPublisherInterface& interface, AliHLTPubSubShmMsg* msg, AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 )
    {
    int ret;
    char name[9], turnedName[9];
    AliUInt8_t *parBoolP1, *parBoolP2;
    AliUInt32_t parBoolSize1;
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    if ( msg->fParamCnt != 1 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "SetEventDoneDataSend msg", 100 )
	    << "Unexpected parameter count for SetEventDoneDataSend message: " << msg->fParamCnt
	    << " instead of 1" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
	return 0;
	}
    fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
    msg = NULL;
    AliHLTPubSubShmMsgParam * parBool;
    AliHLTBlockHeader *pbh;
    pbh = NULL;
    fPubToSub.SetTimeout( fPubToSubTimeout );
    ret = fPubToSub.Read( pbh, parBoolP1, parBoolP2, parBoolSize1 );
    fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );

    parBool = (AliHLTPubSubShmMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy", "SetEventDoneDataSend msg parameter")
	    << "Error reading SetEventDoneDataSend message bool parameter from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
    if ( parBool->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)parBool->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "SetEventDoneDataSend msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << parBool->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parBool, parBoolP1, parBoolP2, parBoolSize1 );
	return 0;
	}
    if ( parBool->fType != AliHLTPubSubShmMsgParam::kBool )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "SetEventDoneDataSend msg parameter type", 100 )
	    << "Unexpected message parameter type for SetEventDoneDataSend message: " << AliHLTLog::kDec 
	    << (int)parBool->fType << " when expecting " << (int)AliHLTPubSubShmMsgParam::kBool << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parBool, parBoolP1, parBoolP2, parBoolSize1 );
	return 0;
	}

    interface.SetEventDoneDataSend( *this, (bool)parBool->fData );
    fPubToSub.FreeRead( (AliHLTBlockHeader*)parBool, parBoolP1, parBoolP2, parBoolSize1 );
    return 0;
    }


int AliHLTSubscriberShmProxy::HandleSetEventTypeModMsg( AliHLTPublisherInterface& interface, AliHLTPubSubShmMsg* msg, AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 )
    {
    int ret;
    char name[9], turnedName[9];
    AliUInt8_t *parCountP1, *parCountP2;
    AliUInt32_t parCountSize1;
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    if ( msg->fParamCnt != 1 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "SetEventTypeMod msg", 100 )
	    << "Unexpected parameter count for SetEventTypeMod message: " << msg->fParamCnt
	    << " instead of 1" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
	return 0;
	}
    fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
    msg = NULL;
    AliHLTPubSubShmMsgParam * parCount;
    AliHLTBlockHeader *pbh;
    pbh = NULL;
    fPubToSub.SetTimeout( fPubToSubTimeout );
    ret = fPubToSub.Read( pbh, parCountP1, parCountP2, parCountSize1 );
    fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );
    parCount = (AliHLTPubSubShmMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy", "SetEventTypeMod msg parameter")
	    << "Error reading SetEventTypeMod message count parameter from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
    if ( parCount->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)parCount->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "SetEventTypeMod msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << parCount->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parCount, parCountP1, parCountP2, parCountSize1 );
	return 0;
	}
    if ( parCount->fType != AliHLTPubSubShmMsgParam::kUInt32 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "SetEventTypeMod msg parameter type", 100 )
	    << "Unexpected message parameter type for SetEventTypeMod message: " << AliHLTLog::kDec 
	    << (int)parCount->fType << " when expecting " << (int)AliHLTPubSubShmMsgParam::kUInt32 << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parCount, parCountP1, parCountP2, parCountSize1 );
	return 0;
	}
    
    interface.SetEventType( *this, (AliUInt32_t)parCount->fData );
    fPubToSub.FreeRead( (AliHLTBlockHeader*)parCount, parCountP1, parCountP2, parCountSize1 );
    return 0;
    }

int AliHLTSubscriberShmProxy::HandleSetEventTypeETTMsg( AliHLTPublisherInterface& interface, AliHLTPubSubShmMsg* msg, AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 )
    {
    int ret;
    char name[9], turnedName[9];
    AliUInt8_t *parETTP1, *parETTP2, *ettP1, *ettP2, *parModP1, *parModP2;
    AliUInt32_t parETTSize1, ettSize1, parModSize1;
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    if ( msg->fParamCnt != 2 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "SetEventTypeETT msg", 100 )
	    << "Unexpected parameter count for SetEventTypeETT message: " << msg->fParamCnt
	    << " instead of 2" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
	return 0;
	}
    fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
    msg = NULL;
    AliHLTPubSubShmMsgParam *parETT, *parMod;
    AliHLTBlockHeader *pbh;
    pbh = NULL;
    fPubToSub.SetTimeout( fPubToSubTimeout );
    ret = fPubToSub.Read( pbh, parETTP1, parETTP2, parETTSize1 );
    fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );
    parETT = (AliHLTPubSubShmMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy", "SetEventTypeETT msg parameter")
	    << "Error reading SetEventTypeETT message vector parameter from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
    if ( parETT->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)parETT->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "SetEventTypeETT msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << parETT->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parETT, parETTP1, parETTP2, parETTSize1 );
	return 0;
	}
    if ( parETT->fType != AliHLTPubSubShmMsgParam::kVectorETT )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "SetEventTypeETT msg parameter type", 100 )
	    << "Unexpected message parameter type for SetEventTypeETT message: " << AliHLTLog::kDec 
	    << (int)parETT->fType << " when expecting " << (int)AliHLTPubSubShmMsgParam::kVectorETT << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parETT, parETTP1, parETTP2, parETTSize1 );
	return 0;
	}
    AliUInt32_t i, n = (AliUInt32_t)parETT->fData;
    vector<AliHLTEventTriggerStruct*> triggerWords;
    vector<AliUInt8_t*> ettP1s;
    vector<AliUInt8_t*> ettP2s;
    vector<AliUInt32_t> ettSize1s;
    AliHLTEventTriggerStruct *etts;
    for ( i = 0; i < n; i++ )
	{
	pbh = NULL;
	fPubToSub.SetTimeout( fPubToSubTimeout );
	ret = fPubToSub.Read( pbh, ettP1, ettP2, ettSize1 );
	fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy", "SetEventTypeETT msg parameter data")
		<< "Error reading SetEventTypeETT msg parameter data " << i << " from shared memory Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	    fPubToSub.FreeRead( (AliHLTBlockHeader*)parETT, parETTP1, parETTP2, parETTSize1 );
	    while ( triggerWords.size() > 0 )
		{
		etts = triggerWords[0];
		triggerWords.erase( triggerWords.begin() );
		fPubToSub.FreeRead( (AliHLTBlockHeader*)etts, *ettP1s.begin(), *ettP2s.begin(), *ettSize1s.begin() );
		ettP1s.erase( ettP1s.begin() );
		ettP2s.erase( ettP2s.begin() );
		ettSize1s.erase( ettSize1s.begin() );
		}
	    return ret;
	    }
	etts = (AliHLTEventTriggerStruct*)pbh;
	if ( etts->fHeader.fType.fID != ALIL3EVENTTRIGGERSTRUCT_TYPE )
	    {
	    strncpy( name, (char*)parETT->fHeader.fType.fDescr, 4 );
	    sample.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
	    name[4] = 0;
	    turnedName[0] = name[3];
	    turnedName[1] = name[2];
	    turnedName[2] = name[1];
	    turnedName[3] = name[0];
	    turnedName[4] = 0;
	
	    LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "SetEventTypeETT msg parameter", 100 )
		<< "Read unexpected datatype " << AliHLTLog::kHex << parETT->fHeader.fType.fID
		<< " == " << name << " (" << turnedName << ")"
		<< " from shared memory when expecting message parameter 0 (" 
		<< sample.fType.fID << ")" << ENDLOG;
	    fPubToSub.FreeRead( (AliHLTBlockHeader*)parETT, parETTP1, parETTP2, parETTSize1 );
	    while ( triggerWords.size() > 0 )
		{
		etts = triggerWords[0];
		triggerWords.erase( triggerWords.begin() );
		fPubToSub.FreeRead( (AliHLTBlockHeader*)etts, *ettP1s.begin(), *ettP2s.begin(), *ettSize1s.begin() );
		ettP1s.erase( ettP1s.begin() );
		ettP2s.erase( ettP2s.begin() );
		ettSize1s.erase( ettSize1s.begin() );
		}
	    return 0;
	    }
	triggerWords.insert( triggerWords.end(), etts );
	ettP1s.insert( ettP1s.end(), ettP1 );
	ettP2s.insert( ettP2s.end(), ettP2 );
	ettSize1s.insert( ettSize1s.end(), ettSize1 );
	}
    fPubToSub.FreeRead( (AliHLTBlockHeader*)parETT, parETTP1, parETTP2, parETTSize1 );
    pbh = NULL;
    fPubToSub.SetTimeout( fPubToSubTimeout );
    ret = fPubToSub.Read( pbh, parModP1, parModP2, parModSize1 );
    fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );
    parMod = (AliHLTPubSubShmMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy", "SetEventTypeETT msg parameter")
	    << "Error reading SetEventTypeETT message modulo parameter from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	while ( triggerWords.size() > 0 )
	    {
	    etts = triggerWords[0];
	    triggerWords.erase( triggerWords.begin() );
	    fPubToSub.FreeRead( (AliHLTBlockHeader*)etts, *ettP1s.begin(), *ettP2s.begin(), *ettSize1s.begin() );
	    ettP1s.erase( ettP1s.begin() );
	    ettP2s.erase( ettP2s.begin() );
	    ettSize1s.erase( ettSize1s.begin() );
	    }
	return ret;
	}
    if ( parMod->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)parMod->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "SetEventTypeETT msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << parMod->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 1 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	while ( triggerWords.size() > 0 )
	    {
	    etts = triggerWords[0];
	    triggerWords.erase( triggerWords.begin() );
	    fPubToSub.FreeRead( (AliHLTBlockHeader*)etts, *ettP1s.begin(), *ettP2s.begin(), *ettSize1s.begin() );
	    ettP1s.erase( ettP1s.begin() );
	    ettP2s.erase( ettP2s.begin() );
	    ettSize1s.erase( ettSize1s.begin() );
	    }
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parMod, parModP1, parModP2, parModSize1 );
	return 0;
	}
    if ( parMod->fType != AliHLTPubSubShmMsgParam::kVectorETT )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "SetEventTypeETT msg parameter type", 100 )
	    << "Unexpected message parameter type for SetEventTypeETT message: " << AliHLTLog::kDec 
	    << (int)parMod->fType << " when expecting " << (int)AliHLTPubSubShmMsgParam::kUInt32 << ENDLOG;
	while ( triggerWords.size() > 0 )
	    {
	    etts = triggerWords[0];
	    triggerWords.erase( triggerWords.begin() );
	    fPubToSub.FreeRead( (AliHLTBlockHeader*)etts, *ettP1s.begin(), *ettP2s.begin(), *ettSize1s.begin() );
	    ettP1s.erase( ettP1s.begin() );
	    ettP2s.erase( ettP2s.begin() );
	    ettSize1s.erase( ettSize1s.begin() );
	    }
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parMod, parModP1, parModP2, parModSize1 );
	return 0;
	}
    interface.SetEventType( *this, triggerWords,(AliUInt32_t)parMod->fData );
    while ( triggerWords.size() > 0 )
	{
	etts = triggerWords[0];
	triggerWords.erase( triggerWords.begin() );
	fPubToSub.FreeRead( (AliHLTBlockHeader*)etts, *ettP1s.begin(), *ettP2s.begin(), *ettSize1s.begin() );
	ettP1s.erase( ettP1s.begin() );
	ettP2s.erase( ettP2s.begin() );
	ettSize1s.erase( ettSize1s.begin() );
	}
    fPubToSub.FreeRead( (AliHLTBlockHeader*)parMod, parModP1, parModP2, parModSize1 );
    return 0;
    }


int AliHLTSubscriberShmProxy::HandleSetTransientTimeoutMsg( AliHLTPublisherInterface& interface, AliHLTPubSubShmMsg* msg, AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 )
    {
    int ret;
    char name[9], turnedName[9];
    AliUInt8_t *parCountP1, *parCountP2;
    AliUInt32_t parCountSize1;
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    if ( msg->fParamCnt != 1 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "SetTransientTimeout msg", 100 )
	    << "Unexpected parameter count for SetTransientTimeout message: " << msg->fParamCnt
	    << " instead of 1" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
	return 0;
	}
    fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
    msg = NULL;
    AliHLTPubSubShmMsgParam * parCount;
    AliHLTBlockHeader *pbh;
    pbh = NULL;
    fPubToSub.SetTimeout( fPubToSubTimeout );
    ret = fPubToSub.Read( pbh, parCountP1, parCountP2, parCountSize1 );
    fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );
    parCount = (AliHLTPubSubShmMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy", "SetTransientTimeout msg parameter")
	    << "Error reading SetTransientTimeout message count parameter fromshared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
    if ( parCount->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)parCount->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "SetTransientTimeout msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << parCount->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parCount, parCountP1, parCountP2, parCountSize1 );
	return 0;
	}
    if ( parCount->fType != AliHLTPubSubShmMsgParam::kUInt32 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "SetTransientTimeout msg parameter type", 100 )
	    << "Unexpected message parameter type for SetTransientTimeout message: " << AliHLTLog::kDec 
	    << (int)parCount->fType << " when expecting " << (int)AliHLTPubSubShmMsgParam::kUInt32 << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parCount, parCountP1, parCountP2, parCountSize1 );
	return 0;
	}
    
    interface.SetTransientTimeout( *this, (AliUInt32_t)parCount->fData );
    fPubToSub.FreeRead( (AliHLTBlockHeader*)parCount, parCountP1, parCountP2, parCountSize1 );
    return 0;
    }



int AliHLTSubscriberShmProxy::HandleEventDoneSingMsg( AliHLTPublisherInterface& interface, AliHLTPubSubShmMsg* msg, AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 )
    {
    int ret;
    char name[9], turnedName[9];
    AliUInt8_t *parEvtIDP1, *parEvtIDP2;
    AliUInt8_t *parForceFlagP1, *parForceFlagP2;
    AliUInt8_t *parForwardedFlagP1, *parForwardedFlagP2;
    AliUInt8_t *parEvtDDP1, *parEvtDDP2;
    AliUInt32_t parEvtIDSize1;
    AliUInt32_t parEvtDDSize1;
    AliUInt32_t parForceFlagSize1;
    AliUInt32_t parForwardedFlagSize1;
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    if ( msg->fParamCnt != 3 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "EventDoneSing msg", 100 )
	    << "Unexpected parameter count for EventDoneSing message: " << msg->fParamCnt
	    << " instead of 3" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
	return 0;
	}
    fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
    msg = NULL;
    AliHLTPubSubShmMsgParam * parEvtID;
    AliHLTBlockHeader *pbh;
    AliHLTEventDoneData* edd;
    pbh = NULL;
    fPubToSub.SetTimeout( fPubToSubTimeout );
    ret = fPubToSub.Read( pbh, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
    fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );
    parEvtID = (AliHLTPubSubShmMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy", "EventDoneSing msg parameter")
	    << "Error reading EventDoneSing message bool parameter from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
    if ( parEvtID->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)parEvtID->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "EventDoneSing msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << parEvtID->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
	return 0;
	}
    if ( parEvtID->fType != AliHLTPubSubShmMsgParam::kEvtDoneD )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "EventDoneSing msg parameter type", 100 )
	    << "Unexpected message parameter type for EventDoneSing message: " << AliHLTLog::kDec 
	    << (int)parEvtID->fType << " when expecting " << (int)AliHLTPubSubShmMsgParam::kEvtDoneD << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );
	return 0;
	}
    fPubToSub.FreeRead( (AliHLTBlockHeader*)parEvtID, parEvtIDP1, parEvtIDP2, parEvtIDSize1 );


    AliHLTPubSubShmMsgParam * parForceFlag;
    pbh = NULL;
    fPubToSub.SetTimeout( fPubToSubTimeout );
    ret = fPubToSub.Read( pbh, parForceFlagP1, parForceFlagP2, parForceFlagSize1 );
    fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );
    parForceFlag = (AliHLTPubSubShmMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy", "EventDoneSing msg parameter")
	    << "Error reading EventDoneSing message bool force forward parameter from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
    if ( parForceFlag->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)parForceFlag->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "EventDoneSing msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << parForceFlag->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parForceFlag, parForceFlagP1, parForceFlagP2, parForceFlagSize1 );
	return 0;
	}
    if ( parForceFlag->fType != AliHLTPubSubShmMsgParam::kBool )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "EventDoneSing msg parameter type", 100 )
	    << "Unexpected message parameter type for EventDoneSing message: " << AliHLTLog::kDec 
	    << (int)parForceFlag->fType << " when expecting " << (int)AliHLTPubSubShmMsgParam::kBool << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parForceFlag, parForceFlagP1, parForceFlagP2, parForceFlagSize1 );
	return 0;
	}
    bool forceForward = parForceFlag->fData;
    fPubToSub.FreeRead( (AliHLTBlockHeader*)parForceFlag, parForceFlagP1, parForceFlagP2, parForceFlagSize1 );


    AliHLTPubSubShmMsgParam * parForwardedFlag;
    pbh = NULL;
    fPubToSub.SetTimeout( fPubToSubTimeout );
    ret = fPubToSub.Read( pbh, parForwardedFlagP1, parForwardedFlagP2, parForwardedFlagSize1 );
    fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );
    parForwardedFlag = (AliHLTPubSubShmMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy", "EventDoneSing msg parameter")
	    << "Error reading EventDoneSing message bool force forward parameter from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
    if ( parForwardedFlag->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)parForwardedFlag->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "EventDoneSing msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << parForwardedFlag->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parForwardedFlag, parForwardedFlagP1, parForwardedFlagP2, parForwardedFlagSize1 );
	return 0;
	}
    if ( parForwardedFlag->fType != AliHLTPubSubShmMsgParam::kBool )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "EventDoneSing msg parameter type", 100 )
	    << "Unexpected message parameter type for EventDoneSing message: " << AliHLTLog::kDec 
	    << (int)parForwardedFlag->fType << " when expecting " << (int)AliHLTPubSubShmMsgParam::kBool << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parForwardedFlag, parForwardedFlagP1, parForwardedFlagP2, parForwardedFlagSize1 );
	return 0;
	}
    bool forwarded = parForwardedFlag->fData;
    fPubToSub.FreeRead( (AliHLTBlockHeader*)parForwardedFlag, parForwardedFlagP1, parForwardedFlagP2, parForwardedFlagSize1 );


    fPubToSub.SetTimeout( fPubToSubTimeout );
    ret = fPubToSub.Read( pbh, parEvtDDP1, parEvtDDP2, parEvtDDSize1 );
    fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );
    edd = (AliHLTEventDoneData*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy", "EventDoneSing msg parameter")
	    << "Error reading EventDoneSing message event done data from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
    if ( edd->fHeader.fType.fID != ALIL3EVENTDONEDATA_TYPE )
	{
	sample.fType.fID = ALIL3EVENTDONEDATA_TYPE;
	strncpy( name, (char*)edd->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "EventDoneSing msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << edd->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)edd, parEvtDDP1, parEvtDDP2, parEvtDDSize1 );
	return 0;
	}

    LOG( AliHLTLog::kDebug, "AliHLTSubscriberShmProxy", "EventDoneSing msg" )
	<< "EventDoneSing event: 0x" << AliHLTLog::kHex << edd->fEventID << " (" 
	<< AliHLTLog::kDec << edd->fEventID << ")." << ENDLOG;
    interface.EventDone( *this, *edd, forceForward, forwarded );
    fPubToSub.FreeRead( (AliHLTBlockHeader*)edd, parEvtDDP1, parEvtDDP2, parEvtDDSize1 );
    return 0;
    }


int AliHLTSubscriberShmProxy::HandleEventDoneMultMsg( AliHLTPublisherInterface& interface, AliHLTPubSubShmMsg* msg, AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 )
    {
    int ret;
    char name[9], turnedName[9];
    AliUInt8_t *parVectP1, *parVectP2, *eddP1, *eddP2;
    AliUInt8_t *parForceFlagP1, *parForceFlagP2;
    AliUInt8_t *parForwardedFlagP1, *parForwardedFlagP2;
    AliUInt32_t parVectSize1, eddSize1;
    AliUInt32_t parForceFlagSize1;
    AliUInt32_t parForwardedFlagSize1;
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    if ( msg->fParamCnt != 1 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "EventDoneMult msg", 100 )
	    << "Unexpected parameter count for EventDoneMult message: " << msg->fParamCnt
	    << " instead of 1" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
	return 0;
	}
    fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
    msg = NULL;
    AliHLTPubSubShmMsgParam * parVect;
    AliHLTEventDoneData* edd;
    AliHLTBlockHeader *pbh;
    pbh = NULL;
    fPubToSub.SetTimeout( fPubToSubTimeout );
    ret = fPubToSub.Read( pbh, parVectP1, parVectP2, parVectSize1 );
    parVect = (AliHLTPubSubShmMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy", "EventDoneMult msg parameter")
	    << "Error reading EventDoneMult message vector parameter from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );
	return ret;
	}
    if ( parVect->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)parVect->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "EventDoneMult msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << parVect->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parVect, parVectP1, parVectP2, parVectSize1 );
	fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );
	return 0;
	}
    if ( parVect->fType != AliHLTPubSubShmMsgParam::kVectorEvtDoneD )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "EventDoneMult msg parameter type", 100 )
	    << "Unexpected message parameter type for EventDoneMult message: " << AliHLTLog::kDec 
	    << (int)parVect->fType << " when expecting " << (int)AliHLTPubSubShmMsgParam::kVectorEvtDoneD << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parVect, parVectP1, parVectP2, parVectSize1 );
	fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );
	return 0;
	}

    AliUInt32_t i, n = parVect->fData;
    fPubToSub.FreeRead( (AliHLTBlockHeader*)parVect, parVectP1, parVectP2, parVectSize1 );


    AliHLTPubSubShmMsgParam * parForceFlag;
    pbh = NULL;
    fPubToSub.SetTimeout( fPubToSubTimeout );
    ret = fPubToSub.Read( pbh, parForceFlagP1, parForceFlagP2, parForceFlagSize1 );
    fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );
    parForceFlag = (AliHLTPubSubShmMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy", "EventDoneSing msg parameter")
	    << "Error reading EventDoneSing message bool force forward parameter from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
    if ( parForceFlag->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)parForceFlag->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "EventDoneSing msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << parForceFlag->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parForceFlag, parForceFlagP1, parForceFlagP2, parForceFlagSize1 );
	return 0;
	}
    if ( parForceFlag->fType != AliHLTPubSubShmMsgParam::kBool )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "EventDoneSing msg parameter type", 100 )
	    << "Unexpected message parameter type for EventDoneSing message: " << AliHLTLog::kDec 
	    << (int)parForceFlag->fType << " when expecting " << (int)AliHLTPubSubShmMsgParam::kBool << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parForceFlag, parForceFlagP1, parForceFlagP2, parForceFlagSize1 );
	return 0;
	}
    bool forceForward = parForceFlag->fData;
    fPubToSub.FreeRead( (AliHLTBlockHeader*)parForceFlag, parForceFlagP1, parForceFlagP2, parForceFlagSize1 );


    AliHLTPubSubShmMsgParam * parForwardedFlag;
    pbh = NULL;
    fPubToSub.SetTimeout( fPubToSubTimeout );
    ret = fPubToSub.Read( pbh, parForwardedFlagP1, parForwardedFlagP2, parForwardedFlagSize1 );
    fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );
    parForwardedFlag = (AliHLTPubSubShmMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy", "EventDoneSing msg parameter")
	    << "Error reading EventDoneSing message bool force forward parameter from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
    if ( parForwardedFlag->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)parForwardedFlag->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "EventDoneSing msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << parForwardedFlag->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parForwardedFlag, parForwardedFlagP1, parForwardedFlagP2, parForwardedFlagSize1 );
	return 0;
	}
    if ( parForwardedFlag->fType != AliHLTPubSubShmMsgParam::kBool )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "EventDoneSing msg parameter type", 100 )
	    << "Unexpected message parameter type for EventDoneSing message: " << AliHLTLog::kDec 
	    << (int)parForwardedFlag->fType << " when expecting " << (int)AliHLTPubSubShmMsgParam::kBool << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parForwardedFlag, parForwardedFlagP1, parForwardedFlagP2, parForwardedFlagSize1 );
	return 0;
	}
    bool forwarded = parForwardedFlag->fData;
    fPubToSub.FreeRead( (AliHLTBlockHeader*)parForwardedFlag, parForwardedFlagP1, parForwardedFlagP2, parForwardedFlagSize1 );


    sample.fType.fID = ALIL3EVENTDONEDATA_TYPE;
    AliHLTEventDoneData* eddTmp;
    vector<AliHLTEventDoneData*> edds;
	
    fPubToSub.SetTimeout( fPubToSubTimeout );
    for ( i = 0; i < n; i++ )
	{
	pbh = NULL;
	ret = fPubToSub.Read( pbh, eddP1, eddP2, eddSize1 );
	edd = (AliHLTEventDoneData*)pbh;
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy", "EventDoneMult msg parameter")
		<< "Error reading EventDoneMult message vector element " << AliHLTLog::kDec
		<< i << " of " << n << " from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	    fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );
	    return ret;
	    }
	if ( edd->fHeader.fType.fID != ALIL3EVENTDONEDATA_TYPE )
	    {
	    strncpy( name, (char*)edd->fHeader.fType.fDescr, 4 );
	    name[4] = 0;
	    turnedName[0] = name[3];
	    turnedName[1] = name[2];
	    turnedName[2] = name[1];
	    turnedName[3] = name[0];
	    turnedName[4] = 0;
	    
	    LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "EventDoneMult msg parameter", 100 )
		<< "Read unexpected datatype " << AliHLTLog::kHex << edd->fHeader.fType.fID
		<< " == " << name << " (" << turnedName << ")"
		<< " from shared memory when expecting message parameter 0 (" 
		<< sample.fType.fID << ")" << ENDLOG;
	    fPubToSub.FreeRead( (AliHLTBlockHeader*)edd, eddP1, eddP2, eddSize1 );
	    fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );
	    return 0;
	    }
	eddTmp = (AliHLTEventDoneData*)new AliUInt8_t[ edd->fHeader.fLength ];
	if ( !eddTmp )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy", "Out Of Memory" )
		<< "Out of memory trying to allocate memory for event done data parameter "
		<< AliHLTLog::kDec << i << " of " << n << "." << ENDLOG;
	    fPubToSub.FreeRead( (AliHLTBlockHeader*)edd, eddP1, eddP2, eddSize1 );
	    fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );
	    return 0;
	    }
	memcpy( eddTmp, edd, edd->fHeader.fLength );
	edds.insert( edds.end(), eddTmp );
	fPubToSub.FreeRead( (AliHLTBlockHeader*)edd, eddP1, eddP2, eddSize1 );
	}
    fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );
    interface.EventDone( *this, edds, forceForward, forwarded );
    n = edds.size();
    for ( i = 0; i < n; i++ )
	delete [] (AliUInt8_t*)edds[i];
    return 0;
    }


int AliHLTSubscriberShmProxy::HandleStartPublishingMsg( AliHLTPublisherInterface&, AliHLTPubSubShmMsg* msg, AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1, bool& sendOldEvents )
    {
    int ret;
    char name[9], turnedName[9];
    AliUInt8_t *parBoolP1, *parBoolP2;
    AliUInt32_t parBoolSize1;
    AliHLTBlockHeader sample = { 0, {ALIL3PUBSUBPIPEMSGPARAM_TYPE}, {0}, 1 };
    if ( msg->fParamCnt != 1 )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "StartPublishing msg", 100 )
	    << "Unexpected parameter count for StartPublishing message: " << msg->fParamCnt
	    << " instead of 1" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
	return 0;
	}
    fPubToSub.FreeRead( (AliHLTBlockHeader*)msg, msgP1, msgP2, msgSize1 );
    msg = NULL;
    AliHLTPubSubShmMsgParam * parBool;
    AliHLTBlockHeader *pbh;
    pbh = NULL;
    fPubToSub.SetTimeout( fPubToSubTimeout );
    ret = fPubToSub.Read( pbh, parBoolP1, parBoolP2, parBoolSize1 );
    fPubToSub.SetTimeout( PUBSUB_SHM_TIMEOUT_MSEC );

    parBool = (AliHLTPubSubShmMsgParam*)pbh;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberShmProxy", "StartPublishing msg parameter")
	    << "Error reading StartPublishing message bool parameter from shared memory. Reason: " << strerror( ret ) << " (" << ret << ")" << ENDLOG;
	return ret;
	}
    if ( parBool->fHeader.fType.fID != ALIL3PUBSUBPIPEMSGPARAM_TYPE )
	{
	strncpy( name, (char*)parBool->fHeader.fType.fDescr, 4 );
	name[4] = 0;
	turnedName[0] = name[3];
	turnedName[1] = name[2];
	turnedName[2] = name[1];
	turnedName[3] = name[0];
	turnedName[4] = 0;
	
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "StartPublishing msg parameter", 100 )
	    << "Read unexpected datatype " << AliHLTLog::kHex << parBool->fHeader.fType.fID
	    << " == " << name << " (" << turnedName << ")"
	    << " from shared memory when expecting message parameter 0 (" 
	    << sample.fType.fID << ")" << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parBool, parBoolP1, parBoolP2, parBoolSize1 );
	return 0;
	}
    if ( parBool->fType != AliHLTPubSubShmMsgParam::kBool )
	{
	LOGM( AliHLTLog::kWarning, "AliHLTSubscriberShmProxy", "StartPublishing msg parameter type", 100 )
	    << "Unexpected message parameter type for StartPublishing message: " << AliHLTLog::kDec 
	    << (int)parBool->fType << " when expecting " << (int)AliHLTPubSubShmMsgParam::kBool << ENDLOG;
	fPubToSub.FreeRead( (AliHLTBlockHeader*)parBool, parBoolP1, parBoolP2, parBoolSize1 );
	return 0;
	}

    sendOldEvents = (bool)parBool->fData;
    fPubToSub.FreeRead( (AliHLTBlockHeader*)parBool, parBoolP1, parBoolP2, parBoolSize1 );
    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
