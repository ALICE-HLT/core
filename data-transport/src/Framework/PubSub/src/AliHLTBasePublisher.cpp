/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTBasePublisher.hpp"
#include "AliHLTBasePublisherMsg.hpp"
#include "AliHLTLog.hpp"
#include <errno.h>

AliHLTBasePublisher::AliHLTBasePublisher( const char* name, int eventSlotsPowerOfTwo ):
    AliHLTPublisherInterface( name ), fWorkerThread( *this, eventSlotsPowerOfTwo ),
    fCallbackThread( *this, eventSlotsPowerOfTwo )
    //, fSubscriptionThread( *this )
    {
    fTimer = NULL;
    fWorkerThread.Start();
    //fCallbackThread.Start();
    //fSubscriptionThread.Start();
    }

AliHLTBasePublisher::~AliHLTBasePublisher()
    {
    printf( "%s:%d\n", __FILE__, __LINE__ );
    fWorkerThread.Quit();
    printf( "%s:%d\n", __FILE__, __LINE__ );
    //fCallbackThread.Quit();
    //fSubscriptionThread.Quit();
    }


int AliHLTBasePublisher::Subscribe( AliHLTSubscriberInterface& 
				   interface )
    {
    AliHLTBasePublisherSubscriberMsg* msg;
    MLUCFifoMsg *baseMsg;
    int ret;
    AliUInt64_t replyNr = fReplySignal.GetReplyNr();
    ret = fWorkerThread.fMsgFifo.ReserveMsgSlot( sizeof(AliHLTBasePublisherSubscriberMsg), baseMsg );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTBasePublisher::Subscribe", "Unable to get fifo msg slot" )
	    << "Unable to get fifo msg slot for Susbcribe message to WorkerThread: " 
	    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	return ret;
	}
    msg = (AliHLTBasePublisherSubscriberMsg*)baseMsg;
    msg->fType = kAliHLTBasePublisherSubscribeMsg;
    msg->fReplyNr = replyNr;
    msg->fSubscriber = &interface;
    msg->fLength = sizeof(AliHLTBasePublisherSubscriberMsg);
    ret = fWorkerThread.fMsgFifo.CommitMsg( msg->fLength );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTBasePublisher::Subscribe", "Error committing msg" )
	    << "Error committing  msg to fifo slot for Susbcribe message to WorkerThread: " 
	    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	return ret;
	}
    ret = (int)fReplySignal.WaitForReply( replyNr );
    return ret;
    }

int AliHLTBasePublisher::Unsubscribe( AliHLTSubscriberInterface& 
				     interface )
    {
    return 0;
    }

int AliHLTBasePublisher::SetPersistent( AliHLTSubscriberInterface& 
				       interface, bool )
    {
    return 0;
    }

int AliHLTBasePublisher::SetEventType( AliHLTSubscriberInterface& 
				      interface, AliUInt32_t modulo )
    {
    return 0;
    }

int AliHLTBasePublisher::SetEventType( AliHLTSubscriberInterface& 
				      interface, vector<AliHLTEventTriggerStruct*>& 
				      triggerWords, AliUInt32_t modulo = 1 )
    {
    return 0;
    }

int AliHLTBasePublisher::SetTransientTimeout( AliHLTSubscriberInterface& interface, 
					     AliUInt32_t timeout_ms )
    {
    return 0;
    }

int AliHLTBasePublisher::EventDone( AliHLTSubscriberInterface& 
				   whichInterface, AliEventID_t 
				   eventID )
    {
    return 0;
    }

int AliHLTBasePublisher::EventDone( AliHLTSubscriberInterface& 
				   whichInterface, 
				   vector<AliEventID_t>& 
				   eventIDs )
    {
    return 0;
    }

int AliHLTBasePublisher::StartPublishing( AliHLTSubscriberInterface& 
					 interface )
    {
    return 0;
    }

int AliHLTBasePublisher::Ping( AliHLTSubscriberInterface& interface )
    {
    return 0;
    }

int AliHLTBasePublisher::PingAck( AliHLTSubscriberInterface& 
				 interface )
    {
    return 0;
    }

int AliHLTBasePublisher::AnnounceEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, AliHLTEventTriggerStruct& trigger )
    {
    return 0;
    }
	
void AliHLTBasePublisher::AbortEvent( AliEventID_t eventID )
    {
    }

void AliHLTBasePublisher::AcceptSubscriptions()
    {
    if ( !fSubscriptionInputLoop )
	{
	LOG( AliHLTLog::kError, "AliHLTBasePublisher", "Accept subscriptions" )
	    << "No subscription input loop function specified. Aborting..." 
	    << ENDLOG;
	return;
	}
    LOG( AliHLTLog::kInformational, "AliHLTBasePublisher", "Accept subscriptions" )
	<< GetName() << " publisher entering subscription loop."
	<< ENDLOG;
    int ret;
    ret = fSubscriptionInputLoop( *this );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTBasePublisher", "Accept subscriptions" )
	    << GetName() << " publisher error in subscription loop. Reason: "
	    << strerror(ret) << " (" << AliHLTLog::kDec << ret << "). "
	    << ENDLOG;
	}
    LOG( AliHLTLog::kInformational, "AliHLTBasePublisher", "Accept subscriptions" )
	<< GetName() << " publisher left subscription loop."
	<< ENDLOG;
    }

AliUInt32_t AliHLTBasePublisher::GetPendingEventCount()
    {
    return 0;
    }

void AliHLTBasePublisher::GetPendingEvents( vector<AliEventID_t>& events )
    {
    }

int AliHLTBasePublisher::RequestEventRelease()
    {
    return 0;
    }
	
int AliHLTBasePublisher::CancelAllSubscriptions()
    {
    return 0;
    }

int AliHLTBasePublisher::PingSubscribers()
    {
    return 0;
    }

void AliHLTBasePublisher::SetMaxTransientTimeout( AliUInt32_t timeout_ms )
    {
    }

void AliHLTBasePublisher::SetPersistentTimeout( bool enable, AliUInt32_t timeout_ms=0 )
    {
    }

void AliHLTBasePublisher::SetPingTimeout( AliUInt32_t timeout_ms )
    {
    }

void AliHLTBasePublisher::SetComTimeout( AliUInt32_t timeout_usec )
    {
    }

void AliHLTBasePublisher::SetMaxPingCount( AliUInt32_t count )
    {
    }

AliUInt32_t AliHLTBasePublisher::GetMaxPingCount()
    {
    return 0;
    }

void AliHLTBasePublisher::SetEventTriggerTypeCompareFunc( AliHLTEventTriggerTypeCompareFunc compFunc )
    {
    }

void AliHLTBasePublisher::CanceledEvent( AliEventID_t event )
    {
    }
	
void AliHLTBasePublisher::AnnouncedEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, 
					 AliHLTEventTriggerStruct& trigger, AliUInt32_t refCount )
    {
    }

AliHLTBasePublisher::WorkerThread::WorkerThread( AliHLTBasePublisher& publ, int slotsPowerOfTwo ):
    fMsgFifo( ((slotsPowerOfTwo<0) ? 20 : (slotsPowerOfTwo+5)), 16 ),
    fEvents( slotsPowerOfTwo ),
    fSubscribers( 4 ),
    fTimeoutDataCache( sizeof(TimeoutData), ((slotsPowerOfTwo<0) ? 10 : (slotsPowerOfTwo+1) ) ),
    fPublisher( publ )
    {
    fQuit = false;
    fQuitted = true;
    }
		       
int AliHLTBasePublisher::WorkerThread::AddSubscriber( AliHLTSubscriberInterface* subscriber )
    {
    SubscriberData* sd;
    sd = new SubscriberData;
    if ( !sd )
	{
	LOG( AliHLTLog::kError, "AliHLTBasePublisher::WorkerThread::AddSubscriber", "Out Of Memory" )
	    << "Out of memory trying to allocate SubscriberData structure." << ENDLOG;
	return ENOMEM;
	}
    sd->fActive = false;
    sd->fPersistent = true;
    sd->fModulo = 1;
    sd->fCount = 0;
    sd->fInterface = subscriber;
    sd->fRecvThread = new AliHLTBasePublisher::SubscriberRecvThread( fPublisher, sd );
    sd->fSendThread = new AliHLTBasePublisher::SubscriberSendThread( fPublisher.GetName(), fPublisher, sd );
    sd->fTimeout = 0;
    sd->fPingCount = 0;
    fSubscribers.Add( sd );
    LOG( AliHLTLog::kInformational, "AliHLTBasePublisher::WorkerThread::AddSubscriber", "Subscriber Added" )
	<< "Subscriber '" << subscriber->GetName() << "' added." << ENDLOG;
    //sd->fRecvThread->Start();
    //sd->fSendThread->Start();
    LOG( AliHLTLog::kDebug, "AliHLTBasePublisher::WorkerThread::AddSubscriber", "Subscriber Threads Started" )
	<< "Threads for subscriber '" << subscriber->GetName() << "' started." << ENDLOG;
    return 0;
    }

void AliHLTBasePublisher::WorkerThread::Run()
    {
    int ret;
    AliUInt64_t replyNr;
    MLUCFifoMsg* baseMsg;
    fQuitted = false;
    while ( !fQuit )
	{
	ret = fMsgFifo.WaitForData();
	if ( fQuit )
	    break;
	if ( ret )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTBasePublisher::WorkerThread::Run", "Error waiting for data" )
		<< "Received error from fifo WaitForData method: "
		 << AliHLTLog::kDec << strerror(ret) << " (" << ret << ")." << ENDLOG;
	    continue;
	    }
	ret = fMsgFifo.GetNextMsg( baseMsg );
	if ( ret )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTBasePublisher::WorkerThread::Run", "Error getting msg" )
		<< "Received error from fifo GetNextMsg method: "
		 << AliHLTLog::kDec << strerror(ret) << " (" << ret << ")." << ENDLOG;
	    continue;
	    }
	if ( !baseMsg )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTBasePublisher::WorkerThread::Run", "Error getting msg" )
		<< "Received NULL pointer from  fifo GetNextMsg method." << ENDLOG;
	    continue;
	    }
	LOG( AliHLTLog::kDebug, "AliHLTBasePublisher::WorkerThread::Run", "Msg received" )
	    << "Received msg 0x" << AliHLTLog::kHex << baseMsg->fType << " (" << AliHLTLog::kDec
	    << baseMsg->fType << ") with length " << baseMsg->fLength << " (0x" << AliHLTLog::kHex
	    << baseMsg->fLength << ")." << ENDLOG;
	switch ( baseMsg->fType )
	    {
	    case kAliHLTBasePublisherQuitThreadMsg:
		{
		printf( "%s:%d\n", __FILE__, __LINE__ );
		LOG( AliHLTLog::kDebug, "AliHLTBasePublisher::WorkerThread::Run", "Msg received" )
		    << "Received quit message." << ENDLOG;
    printf( "%s:%d\n", __FILE__, __LINE__ );
		fQuit = true;
    printf( "%s:%d\n", __FILE__, __LINE__ );
		fMsgFifo.FreeMsg( baseMsg );
    printf( "%s:%d\n", __FILE__, __LINE__ );
		break;
		}
	    case kAliHLTBasePublisherSubscribeMsg:
		{
		int retval;
		LOG( AliHLTLog::kDebug, "AliHLTBasePublisher::WorkerThread::Run", "Msg received" )
		    << "Received subscribe message." << ENDLOG;
		AliHLTBasePublisherSubscriberMsg* msg;
		msg = (AliHLTBasePublisherSubscriberMsg*)baseMsg;

		AliHLTSubscriberInterface* subs = msg->fSubscriber;
		replyNr = msg->fReplyNr;

		fMsgFifo.FreeMsg( baseMsg );

		retval = AddSubscriber( subs );
		fPublisher.fReplySignal.SignalReply( replyNr, (AliUInt64_t)retval );
		break;
		}
	    default:
		LOG( AliHLTLog::kDebug, "AliHLTBasePublisher::WorkerThread::Run", "Msg received" )
		    << "Received unknown message." << ENDLOG;
		fMsgFifo.FreeMsg( baseMsg );
		break;
	    }
	}
    printf( "%s:%d\n", __FILE__, __LINE__ );
    fQuitted = true;
    printf( "%s:%d\n", __FILE__, __LINE__ );
    }

void AliHLTBasePublisher::WorkerThread::Quit()
    {
    printf( "%s:%d\n", __FILE__, __LINE__ );
    AliHLTBasePublisherMsg msg;
    msg.fLength = sizeof(AliHLTBasePublisherMsg);
    msg.fType = kAliHLTBasePublisherQuitThreadMsg;
    msg.fReplyNr = 0;
    fQuit = true;
    printf( "%s:%d\n", __FILE__, __LINE__ );
    fMsgFifo.Write( &msg );
    printf( "%s:%d\n", __FILE__, __LINE__ );
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 500000;
    gettimeofday( &start, NULL );
    printf( "%s:%d\n", __FILE__, __LINE__ );
    while ( deltaT<timeLimit && !fQuitted )
	{
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    printf( "%s:%d\n", __FILE__, __LINE__ );
    if ( fQuitted )
	Join();
    else
	Abort();
    printf( "%s:%d\n", __FILE__, __LINE__ );
    }

void AliHLTBasePublisher::WorkerThread::TimerExpired( AliUInt64_t notData )
    {
    }

AliHLTBasePublisher::CallbackThread::CallbackThread( AliHLTBasePublisher& publ, int slotsExp2 ):
    fMsgFifo( ((slotsExp2<0) ? 18 : (slotsExp2+3)), 10 ),
    fPublisher( publ )
    {
    }

void AliHLTBasePublisher::CallbackThread::Run()
    {
    }

void AliHLTBasePublisher::CallbackThread::Quit()
    {
    }

// AliHLTBasePublisher::SubscriptionThread::SubscriptionThread( AliHLTBasePublisher& publ ):
//     fPublisher( publ )
//     {
//     }

// void AliHLTBasePublisher::SubscriptionThread::Run()
//     {
//     }

// void AliHLTBasePublisher::SubscriptionThread::Quit()
//     {
//     }


AliHLTBasePublisher::SubscriberRecvThread::SubscriberRecvThread( AliHLTBasePublisher& publ, WorkerThread::SubscriberData* data ):
    fPublisher( publ ), fData( data )
    {
    }

AliHLTBasePublisher::SubscriberRecvThread::~SubscriberRecvThread()
    {
    }

void AliHLTBasePublisher::SubscriberRecvThread::Run()
    {
    }



AliHLTBasePublisher::SubscriberSendThread::SubscriberSendThread( const char* name, AliHLTBasePublisher& pub, WorkerThread::SubscriberData* data ):
    AliHLTSubscriberInterface( name ), fPublisher( pub ), fData( data )
    {
    }
	
AliHLTBasePublisher::SubscriberSendThread::~SubscriberSendThread()
    {
    }

void AliHLTBasePublisher::SubscriberSendThread::Quit()
    {
    }

int AliHLTBasePublisher::SubscriberSendThread::NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
								const AliHLTSubEventDataDescriptor& sbevent )
    {
    return 0;
    }

int AliHLTBasePublisher::SubscriberSendThread::EventCanceled( AliHLTPublisherInterface& publisher, AliEventID_t eventID )
    {
    return 0;
    }

int AliHLTBasePublisher::SubscriberSendThread::SubscriptionCanceled( AliHLTPublisherInterface& publisher )
    {
    return 0;
    }

int AliHLTBasePublisher::SubscriberSendThread::ReleaseEventsRequest( AliHLTPublisherInterface& publisher )
    {
    return 0;
    }

int AliHLTBasePublisher::SubscriberSendThread::Ping( AliHLTPublisherInterface& publisher )
    {
    return 0;
    }

int AliHLTBasePublisher::SubscriberSendThread::PingAck( AliHLTPublisherInterface& publisher )
    {
    return 0;
    }

void AliHLTBasePublisher::SubscriberSendThread::Run()
    {
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
