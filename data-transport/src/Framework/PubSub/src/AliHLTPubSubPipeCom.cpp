/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTPubSubPipeCom.hpp"
#include "AliHLTPublisherInterface.hpp"
#include "AliHLTPipeCom.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTSubscriberPipeProxy.hpp"
#include "AliHLTSubscriberShmProxy.hpp"
#include <errno.h>

bool gQuitPipeSubscriptionInputLoop = false;
bool gQuittedPipeSubscriptionInputLoop = false;

AliUInt32_t gComTimeout = 0;


int PublisherPipeSubscriptionInputLoop( AliHLTPublisherInterface& interface, AliHLTAcceptingSubscriptionsCallbackFunc callback, void* callbackData )
    {
    AliHLTPipeCom com;
    int ret;

    gQuitPipeSubscriptionInputLoop = false;
    gQuittedPipeSubscriptionInputLoop = false;
    MLUCString subsname;
    subsname = ALIL3PIPEBASEDIR;
    subsname += interface.GetName();
    char tmpUID[64];
    sprintf( tmpUID, "%d", (int)getuid() );
    subsname += "-";
    subsname += tmpUID;
    subsname += "-";
    subsname += "SubsService";
    ret = com.OpenRead( subsname.c_str(), true );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "PublisherPipeSubscriptionInputLoop", "Subscriber read pipe open" )
	    << "Unable to open subscriber read pipe " 
	    << subsname.c_str() << ENDLOG;
	return ret;
	}

    AliHLTBlockHeader* pbh=NULL, sample = { 0, {ALIL3PUBSUBPIPEMSG_TYPE}, {0}, 1 };
    AliHLTPubSubPipeMsg* msg;
    char name[9], turnedName[9];
    com.SetTimeout( gComTimeout );
    if ( callback )
	callback( callbackData );
    while ( !gQuitPipeSubscriptionInputLoop )
	{
	if ( pbh )
	    {
	    delete [] pbh;
	    pbh = NULL;
	    }
	ret = com.Read( pbh, true );
	LOG( AliHLTLog::kDebug, "PublisherPipeSubscriptionInputLoop", "msg loop read" )
	    << "Message read." << ENDLOG;
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "PublisherPipeSubscriptionInputLoop", "msg loop read" )
		<< "Error reading pipe message: " << strerror(ret) << AliHLTLog::kDec << " (" << ret << ")." << ENDLOG;
	    return ret;
	    }
	if ( !pbh )
	    {
	    LOG( AliHLTLog::kError, "PublisherPipeSubscriptionInputLoop", "msg loop read" )
		<< "NULL pointer returned by pipe read." << ENDLOG;
	    continue;
	    }
	LOG( AliHLTLog::kDebug, "PublisherPipeSubscriptionInputLoop", "msg loop read" )
	    << "Message!=NULL." << ENDLOG;
	msg = (AliHLTPubSubPipeMsg*)pbh;
	if ( msg->fHeader.fType.fID != ALIL3PUBSUBPIPEMSG_TYPE )
	    {
	    strncpy( name, (const char*)msg->fHeader.fType.fDescr, 4 );
	    name[4] = 0;
	    turnedName[0] = name[3];
	    turnedName[1] = name[2];
	    turnedName[2] = name[1];
	    turnedName[3] = name[0];
	    turnedName[4] = 0;
	    LOGM( AliHLTLog::kWarning, "PublisherPipeSubscriptionInputLoop", "Msg loop", 100 )
		<< "Read unexpected datatype " << AliHLTLog::kHex << msg->fHeader.fType.fID
		<< " == " << name << " (" << turnedName << ")"
		<< " from pipe when expecting pipe message (" << sample.fType.fID << ")" << ENDLOG;
	    continue;
	    }
	LOG( AliHLTLog::kDebug, "PublisherPipeSubscriptionInputLoop", "msg loop read" )
	    << "Correct message type." << ENDLOG;
	if ( msg->fType == AliHLTPubSubPipeMsg::kSubscribe )
	    {
	    AliHLTSubscriberProxyInterface* proxy;
	    LOG( AliHLTLog::kDebug, "PublisherPipeSubscriptionInputLoop", "msg loop read" )
		<< "Subscribe message." << ENDLOG;
	    char* colonP;
	    colonP = strchr( (char*)msg->fSubscriberName, ':' );
	    if ( !colonP )
		{
		proxy = new AliHLTSubscriberPipeProxy( (const char*)msg->fSubscriberName );
		LOG( AliHLTLog::kInformational, "PublisherPipeSubscriptionInputLoop", "SubscriberProxy created" )
		    << "Created default pipe subscriber proxy '" << (const char*)msg->fSubscriberName << "'." << ENDLOG;
		}
	    else
		{
		// subscriber name has a prefix of the form XXX: where XXX says what type of proxy to use.
		unsigned len = colonP-(const char*)msg->fSubscriberName; // Length of prefix before name
		if ( !strncmp( (const char*)msg->fSubscriberName, "shm", len ) )
		    {
		    // We have an shm based proxy, the name starts after the colon
		    // After the name we find the two shared memory keys and the shared memory size
		    // These we retrieve first.
		    key_t pub2SubKey, sub2PubKey, *pkey;
		    AliUInt32_t shmSize, *pSz;
		    pkey = (key_t*)( colonP+strlen(colonP)+1 );
		    pub2SubKey = *pkey;
		    pkey++;
		    sub2PubKey = *pkey;
		    pkey++;
		    pSz = (AliUInt32_t*)pkey;
		    shmSize = *pSz;
		    proxy = new AliHLTSubscriberShmProxy( (const char*)(colonP+1), shmSize, pub2SubKey, sub2PubKey );
		    LOG( AliHLTLog::kInformational, "PublisherPipeSubscriptionInputLoop", "SubscriberProxy created" )
			<< "Created shared memory subscriber proxy '" << (colonP+1) << "' - PubToSubKey: 0x" 
			<< AliHLTLog::kHex << (int)pub2SubKey << " (" << AliHLTLog::kDec << (int)pub2SubKey 
			<< ") - SubToPubKey: 0x" << AliHLTLog::kHex << (int)sub2PubKey << " (" << AliHLTLog::kDec 
			<< (int)sub2PubKey << ") - shm size : " << shmSize << " (0x" << AliHLTLog::kHex
			<< shmSize << ")." << ENDLOG;
		    }
		else if ( !strncmp( (const char*)msg->fSubscriberName, "pipe", len ) )
		    {
		    // We have a pipe based proxy, the name starts after the colon
		    proxy = new AliHLTSubscriberPipeProxy( (const char*)(colonP+1) );
		    LOG( AliHLTLog::kInformational, "PublisherPipeSubscriptionInputLoop", "SubscriberProxy created" )
			<< "Created pipe subscriber proxy '" << (colonP+1) << "'." << ENDLOG;
		    }
		else
		    {
		    // We have an unknown proxy.
		    *colonP = '\0';
		    LOG( AliHLTLog::kError, "PublisherPipeSubscriptionInputLoop", "Unknown SubscriberProxy Type" )
			<< "Unknown subscriber proxy type '" << (const char*)msg->fSubscriberName
			<< "'. Unable to create subscriber proxy '" << (colonP+1) <<'.' << ENDLOG;
		    *colonP = ':';
		    proxy = NULL;
		    continue;
		    }
		}
	    if ( !proxy )
		{
		msg->fSubscriberName[ msg->fHeader.fLength-sizeof(AliHLTPubSubPipeMsg)-1 ] = 0;
		LOG( AliHLTLog::kError, "PublisherPipeSubscriptionInputLoop", "Subscriber proxy create" )
		    << "Out of memory creating subscriber proxy named "
		    << (const char*)msg->fSubscriberName << ENDLOG;
		continue;
		}
	    LOG( AliHLTLog::kDebug, "PublisherPipeSubscriptionInputLoop", "msg loop read" )
		<< "Proxy created." << ENDLOG;
	    interface.Subscribe( *proxy );
	    LOG( AliHLTLog::kDebug, "PublisherPipeSubscriptionInputLoop", "msg loop read" )
		<< "Proxy subscribed." << ENDLOG;
	    }
	else
	    {
	    if (  msg->fType != AliHLTPubSubPipeMsg::kQuitLoop )
		{
		LOGM( AliHLTLog::kWarning, "PublisherPipeSubscriptionInputLoop", "Message type", 100 )
		    << "Unexpected pipe message type read: " << (int)msg->fType
		    << " - Only allowed types here are kSubscribe (" << (int)AliHLTPubSubPipeMsg::kSubscribe
		    << ") or kQuitLoop (" << (int)AliHLTPubSubPipeMsg::kQuitLoop << ")" << ENDLOG;
		continue;
		}
	    LOG( AliHLTLog::kDebug, "PublisherPipeSubscriptionInputLoop", "msg loop read" )
		<< "Quit loop message." << ENDLOG;
	    }
	}
    if ( pbh )
	{
	delete [] pbh;
	pbh = NULL;
	}
    LOG( AliHLTLog::kDebug, "PublisherPipeSubscriptionInputLoop", "msg loop read" )
	<< "Loop done." << ENDLOG;
    gQuittedPipeSubscriptionInputLoop = true;
    return 0;
    }


void PublisherPipeSubscriptionCanceled( AliHLTSubscriberInterface& interface )
    {
    delete (AliHLTSubscriberPipeProxy*)&interface;
    }


void QuitPipeSubscriptionLoop( AliHLTPublisherInterface& interface )
    {
    gQuitPipeSubscriptionInputLoop = true;
    AliHLTPipeCom com;
    MLUCString subsname;
    subsname = ALIL3PIPEBASEDIR;
    subsname += interface.GetName();
    char tmpUID[64];
    sprintf( tmpUID, "%d", (int)getuid() );
    subsname += "-";
    subsname += tmpUID;
    subsname += "-";
    subsname += "SubsService";
    int ret = com.OpenWrite( subsname.c_str() );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "QuitPipeSubscriptionLoop", "Publisher read pipe open" )
	    << "Unable to open subscriber read pipe " 
	    << subsname.c_str() << " for writing quit message" << ENDLOG;
	return;
	}
    AliHLTPubSubPipeMsg msg;
    msg.fHeader.fType.fID = ALIL3PUBSUBPIPEMSG_TYPE;
    msg.fHeader.fSubType.fID = 0;
    msg.fHeader.fVersion = 1;
    msg.fHeader.fLength = sizeof( AliHLTPubSubPipeMsg );
    msg.fType = AliHLTPubSubPipeMsg::kQuitLoop;
    msg.fParamCnt = 0;
    com.SetTimeout( 1000000 );
    ret = com.Write( &(msg.fHeader) );
    if ( ret )
	{
	if ( ret != ETIMEDOUT )
	    {
	    LOG( AliHLTLog::kError, "QuitPipeSubscriptionLoop", "Publisher quit message write" )
		<< "Unable to write publisher quit message to pipe " 
		<< subsname.c_str() << ENDLOG;
	    return;
	    }
	else
	    {
	    LOG( AliHLTLog::kWarning, "QuitPipeSubscriptionLoop", "Publisher quit message write" )
		<< "Timeout writing publisher quit message to pipe " 
		<< subsname.c_str() << ENDLOG;
	    }
	}
    com.Close();
    unsigned count=0;
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 50000000;
    gettimeofday( &start, NULL );
    while ( !gQuittedPipeSubscriptionInputLoop && deltaT<timeLimit )
	{
	LOG( AliHLTLog::kInformational, "QuitPipeSubscriptionLoop", "Waiting for finish" ) 
	    << "Waiting for subscription loop to finish... (Attempt " << count << ")." << ENDLOG;
	count++;
	usleep( 500000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
