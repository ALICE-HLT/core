/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3SUBSCRIBERINTERFACE_HPP_
#define _ALIL3SUBSCRIBERINTERFACE_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliEventID.h"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTEventTriggerStruct.h"
#include "AliHLTEventDoneData.h"
#include "MLUCString.hpp"



class AliHLTPublisherInterface;
class AliHLTEventAccounting;

class AliHLTSubscriberInterface
    {
    public:
	    
	// Constructor. 
	// The name argument uniquely identifies 
	// AliHLTSubscriberInterface to publishing services.
	AliHLTSubscriberInterface( const char* name );

	// Destructor
	virtual ~AliHLTSubscriberInterface();

	// This method is called whenever a new wanted 
	// event is available.
	virtual int NewEvent( AliHLTPublisherInterface& publisher, 
			      AliEventID_t eventID, 
			      const AliHLTSubEventDataDescriptor& sbevent,
			      const AliHLTEventTriggerStruct& ets ) = 0;

	// This method is called for a transient subscriber, 
	// when an event has been removed from the list. 
	virtual int EventCanceled( AliHLTPublisherInterface& publisher, 
				   AliEventID_t eventID ) = 0;

	// This method is called for a subscriber when new event done data is 
	// received by a publisher (sent from another subscriber) for an event. 
	virtual int EventDoneData( AliHLTPublisherInterface& publisher, 
				   const AliHLTEventDoneData& 
				   eventDoneData ) = 0;

	// This method is called when the subscription ends.
	virtual int SubscriptionCanceled( AliHLTPublisherInterface& publisher ) = 0;

	// This method is called when the publishing 
	// service runs out of buffer space. The subscriber 
	// has to free as many events as possible and send an 
	// EventDone message with their IDs to the publisher
	virtual int ReleaseEventsRequest( AliHLTPublisherInterface& publisher ) = 0; 

	// Method used to check alive status of subscriber. 
	// This message has to be answered with 
	// PingAck message within less  than a second.
	virtual int Ping( AliHLTPublisherInterface& publisher ) = 0;

	// Method used by a subscriber in response to a Ping 
	// received message. The PingAck message has to be 
	// called within less than one second.
	virtual int PingAck( AliHLTPublisherInterface& publisher ) = 0;

	// Get the subscriber's name
	const char* GetName();

	virtual void SetEventAccounting( AliHLTEventAccounting* /*eventAccounting*/ )
		{
		}

    protected:

	MLUCString fName;

    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3SUBSCRIBERINTERFACE_HPP_
