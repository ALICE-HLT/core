#ifndef _ALIL3BASEPUBLISHER_HPP_
#define _ALIL3BASEPUBLISHER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTPublisherInterface.hpp"
#include "AliHLTSubscriberInterface.hpp"
#include "AliHLTThread.hpp"
#include "AliHLTReplySignal.hpp"
#include "MLUCFifo.hpp"
#include "MLUCVector.hpp"
#include "MLUCNewCacheObject.hpp"
#include <pthread.h>


class AliHLTTimer;

class AliHLTBasePublisher: public AliHLTPublisherInterface
    {
    public:

	// Constructor. The name argument serves as the
	// unique identifier for this subscription service
	// The eventSlotsPowerOfTwo argument specifies the number 
	// of event slots that are to be made available in the publisher.
	// If this is less than zero, then this number will be kept variable with 
	// possible overhead if the number of events in the publisher is large.
	// If greater or equal to zero, the number of slots will be 
	// 2^eventSlotsPowerOfTwo.
	AliHLTBasePublisher( const char* name, int eventSlotsPowerOfTwo = -1 );

	// Destructor
	virtual ~AliHLTBasePublisher();

	// Subscribe the given AliHLTSubscriberInterface to this 
	// subscription service.
	virtual int Subscribe( AliHLTSubscriberInterface& 
			       interface );

	// Unsubscribe a previously subscribed Subscriber-
	// Interface from this subscription service.
	virtual int Unsubscribe( AliHLTSubscriberInterface& 
				 interface );

 

	// Set the persistency status of the specified 
	// AliHLTSubscriberInterface:
	// Persistent (bool==true) for DAQ or L3; 
	// Transient (aka non-persistent) (bool==false)
	// for monitors. For monitors more event selection 
	// parameters can be specified (see below).
	virtual int SetPersistent( AliHLTSubscriberInterface& 
				   interface, bool );  

	// Specify that the AliHLTSubscriberInterface, is 
	// only notified about every modulo'th event.
	virtual int SetEventType( AliHLTSubscriberInterface& 
				  interface,  
				  AliUInt32_t modulo );

	// Specify that the AliHLTSubscriberInterface only 
	// wants to be notified about events whose trigger 
	// words appear in the passed list. The additional 
	// modulo parameter has the meaning as in the 
	// previous method and defaults to 1.
	virtual int SetEventType( AliHLTSubscriberInterface& 
				  interface, 
				  vector
				  <AliHLTEventTriggerStruct*>& 
				  triggerWords,  
				  AliUInt32_t modulo = 1 );

	// This method is used to specify to the publisher the 
	// minimum amount of time that an event can be kept by
	// a transient subscriber. The timeout is given in 
	// milliseconds
	virtual int SetTransientTimeout( AliHLTSubscriberInterface& interface, 
					 AliUInt32_t timeout_ms );

	// Method used by the subscriber to signal that it
	// finished working on the event with the specified 
	// event ID and that it can now be removed.
	virtual int EventDone( AliHLTSubscriberInterface& 
			       whichInterface, AliEventID_t 
			       eventID );

	// Method used by the subscriber to signal that it
	// finished working on the events in the list 
	// of event IDs.
	virtual int EventDone( AliHLTSubscriberInterface& 
			       whichInterface, 
			       vector<AliEventID_t>& 
			       eventIDs );

	// This method tells the publishing service to start 
	// the notification about events and starts an 
	// event loop from which the calling (and given) 
	// AliHLTSubscriberInterface thread will return after 
	// a SubscriptionCanceled message has been received by it.
	virtual int StartPublishing( AliHLTSubscriberInterface& 
				     interface );

	// Method used to check the alive status of the 
	// publishing service. This message must be answered 
	// with a PingAck message to the calling (and given) 
	// AliHLTSubscriberInterface within less than a second.
	virtual int Ping( AliHLTSubscriberInterface& interface );

	// Method used by the AliHLTSubscriberInterface in 
	// response to a Ping message received from this 
	// publishing service. The PingAck message must be 
	// called  in at most one second after the Ping 
	// message has been received.
	virtual int PingAck( AliHLTSubscriberInterface& 
			     interface );


	virtual int AnnounceEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, AliHLTEventTriggerStruct& trigger );
	virtual void AbortEvent( AliEventID_t eventID );

	virtual void AcceptSubscriptions();

	virtual AliUInt32_t GetPendingEventCount();

	virtual void GetPendingEvents( vector<AliEventID_t>& events );

	virtual int RequestEventRelease();
	
	virtual int CancelAllSubscriptions();

	virtual int PingSubscribers();

	//virtual void CancelSubscription( AliHLTSubscriberInterface& forInterface );

	void SetMaxTransientTimeout( AliUInt32_t timeout_ms );
	void SetPersistentTimeout( bool enable, AliUInt32_t timeout_ms=0 );

	void SetPingTimeout( AliUInt32_t timeout_ms );

	void SetComTimeout( AliUInt32_t timeout_usec );

	void SetMaxPingCount( AliUInt32_t count );

	AliUInt32_t GetMaxPingCount();

	void SetSubscriptionLoop( int (*loopFunc)( AliHLTPublisherInterface& ) )
		{
		fSubscriptionInputLoop = loopFunc;
		}

	void SetEventTriggerTypeCompareFunc( AliHLTEventTriggerTypeCompareFunc compFunc );
	void SetTimer( AliHLTTimer* timer )
		{
		fTimer = timer;
		}
	
    protected:

	class SubscriberSendThread;
	class SubscriberRecvThread;

	class WorkerThread;
	class CallbackThread;

	    friend class WorkerThread;

	virtual void CanceledEvent( AliEventID_t event );
	// empty callback. Will be called when an
	//event has already been internally canceled. Will be 
	// called with no mutexes locked!
	virtual void AnnouncedEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, 
				     AliHLTEventTriggerStruct& trigger, AliUInt32_t refCount );
	// empty callback. Will be called when an
	//event has been announced. Will be 
	// called with no mutexes locked!


	AliHLTTimer* fTimer;

	AliHLTReplySignal fReplySignal;

	class WorkerThread: public AliHLTThread
	    {
	    public:

		typedef struct SubscriberData
		    {
			bool fActive;
			bool fPersistent;
			AliUInt32_t fCount;
			AliUInt32_t fModulo;
			AliHLTSubscriberInterface* fInterface;
			SubscriberRecvThread* fRecvThread;
			SubscriberSendThread* fSendThread;
			AliUInt32_t fTimeout;
			vector<AliHLTEventTriggerStruct*> fTriggers;
			//vector<AliEventID_t> fEvents;
			AliUInt32_t fPingCount;
		    }
		SubscriberData;


		WorkerThread( AliHLTBasePublisher& publ, int slotsPowerOfTwo );
		
		void Quit();
		
		MLUCFifo fMsgFifo;

	    protected:

		class SubscriberRecvThread;
		class SubscriberSendThread;

		int AddSubscriber( AliHLTSubscriberInterface* subscriber );
		
		virtual void Run();

		virtual void TimerExpired( AliUInt64_t notData );


		typedef struct TimeoutData
		    {
			enum TimeoutType { kEventTimeout, kPingTimeout };
			TimeoutType fType;
			union
			    {
				AliEventID_t fEventID;
				SubscriberData* fSubscriber;
			    };
		    }
		TimeoutData;
		
		typedef struct EventSubscriberData
		    {
			AliHLTSubscriberInterface* fSubscriber;
			bool fPersistent;
		    }
		EventSubscriberData;
		
		typedef struct EventData
		    {
			AliEventID_t fEventID;
			AliUInt32_t  fRefCount;
			AliUInt32_t fTransientCount;
			TimeoutData* fEventTimeout;
			bool fFinalTimeout;
			bool fUsed;
			AliUInt32_t fMaxTimeout;
			AliUInt32_t fTimerID;
			vector<EventSubscriberData> fSubscribers;
		    }
		EventData;

		MLUCVector<EventData> fEvents;

		typedef SubscriberData* PSubscriberData;
		MLUCVector<PSubscriberData> fSubscribers;

		bool FindSubscriberByInterface( const PSubscriberData& elem, uint64 ref );

		MLUCNewCacheObject fTimeoutDataCache;

		bool fQuit;
		bool fQuitted;

		AliHLTBasePublisher& fPublisher;
		
	    private:
	    };

	class CallbackThread: public AliHLTThread
	    {
	    public:
		
		CallbackThread( AliHLTBasePublisher& publ, int slotsExp2 );
 
		void Quit();

		MLUCFifo fMsgFifo;

	    protected:
		
		virtual void Run();

		AliHLTBasePublisher& fPublisher;

	    private:
	    };

// 	class SubscriptionThread: public AliHLTThread
// 	    {
// 	    public:
		
// 		SubscriptionThread( AliHLTBasePublisher& publ );
 
// 		void Quit();

// 	    protected:
		
// 		virtual void Run();
// 		AliHLTBasePublisher& fPublisher;

// 	    private:
// 	    };

	class SubscriberRecvThread: public AliHLTThread
	    {
	    public:

		SubscriberRecvThread( AliHLTBasePublisher& publ, WorkerThread::SubscriberData* data );
		~SubscriberRecvThread();

	    protected:

		virtual void Run();

		AliHLTBasePublisher& fPublisher;
		WorkerThread::SubscriberData *fData;

	    private:
	    };

	class SubscriberSendThread: public AliHLTThread, public AliHLTSubscriberInterface
	    {
	    public:

		SubscriberSendThread( const char* name, AliHLTBasePublisher& pub, WorkerThread::SubscriberData* data );
		~SubscriberSendThread();

		void Quit();

		virtual int NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
				      const AliHLTSubEventDataDescriptor& sbevent );

		virtual int EventCanceled( AliHLTPublisherInterface& publisher, AliEventID_t eventID );

		virtual int SubscriptionCanceled( AliHLTPublisherInterface& publisher );

		virtual int ReleaseEventsRequest( AliHLTPublisherInterface& publisher ); 

		virtual int Ping( AliHLTPublisherInterface& publisher );

		virtual int PingAck( AliHLTPublisherInterface& publisher );

	    protected:

		virtual void Run();

		AliHLTBasePublisher& fPublisher;
		WorkerThread::SubscriberData* fData;
		AliHLTTimerConditionSem fSem;
		bool fQuit;
		bool fQuitted;

		    friend class AliHLTSamplePublisher;

		struct MsgData
		    {
			//AliHLTPubSubPipeMsg::MsgType fMsg;
			AliEventID_t fID;
			AliHLTSubEventDataDescriptor fSEDD;
		    };
		
	    private:
		
	    };

	int (*fSubscriptionInputLoop)( AliHLTPublisherInterface& );

	WorkerThread fWorkerThread;
	CallbackThread fCallbackThread;
	//SubscriptionThread fSubscriptionThread;

    private:
    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3BASEPUBLISHER_HPP_
