#ifndef CRC_HPP
#define CRC_HPP
/* code stolen from DD April 97 */
/* static long CrcInitFlag = 0 ; */
/* static long Crc32Table[256] ; */

#include "AliHLTBlockHeader.h"

void InitCrc32( long* crc32Table ) ;
long Crc32( int ch, long crc, long* crc32Table );

long MakeCRC( long crc, AliHLTBlockHeader* header, long* crc32Table );


#endif /* end of CRC_HH */
