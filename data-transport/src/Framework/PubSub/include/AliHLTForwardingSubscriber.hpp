#ifndef _ALIHLTFORWARDINGSUBSCRIBER_HPP_
#define _ALIHLTFORWARDINGSUBSCRIBER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSubscriberInterface.hpp"
#include "AliHLTLog.hpp"


template<class ParentClass, class ParentIndexType>
class AliHLTForwardingSubscriber: public AliHLTSubscriberInterface
    {
    public:

	AliHLTForwardingSubscriber( ParentClass* parent, ParentIndexType ndx, const char* name ):
	    AliHLTSubscriberInterface( name ), fParent( parent ), fParentNdx( ndx )
		{
		};

	virtual ~AliHLTForwardingSubscriber() {};

	// This method is called whenever a new wanted 
	// event is available.
	virtual int NewEvent( AliHLTPublisherInterface& publisher, 
			      AliEventID_t eventID, 
			      const AliHLTSubEventDataDescriptor& sbevent,
			      const AliHLTEventTriggerStruct& ets )
		{
		if ( eventID != sbevent.fEventID )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTForwardingSubscriber::NewEvent", "Incoherent Event ID" )
			<< "Msg event ID 0x" << AliHLTLog::kHex << eventID << " ("
			<< AliHLTLog::kDec << eventID << ") unequal sedd eventID: 0x"
			<< AliHLTLog::kHex << sbevent.fEventID << " ("
			<< AliHLTLog::kDec << sbevent.fEventID << ")." << ENDLOG;
		    }
		return fParent->NewEvent( this, fParentNdx, publisher, eventID, 
				  sbevent, ets );
		}

	// This method is called for a transient subscriber, 
	// when an event has been removed from the list. 
	virtual int EventCanceled( AliHLTPublisherInterface& publisher, 
				   AliEventID_t eventID )
		{
		return fParent->EventCanceled( this, fParentNdx, publisher, eventID );
		}

	// This method is called for a subscriber when new event done data is 
	// received by a publisher (sent from another subscriber) for an event. 
	virtual int EventDoneData( AliHLTPublisherInterface& publisher, 
				   const AliHLTEventDoneData& 
				   eventDoneData )
		{
		return fParent->EventDoneData( this, fParentNdx, publisher, eventDoneData );
		}

	// This method is called when the subscription ends.
	virtual int SubscriptionCanceled( AliHLTPublisherInterface& publisher )
		{
		return fParent->SubscriptionCanceled( this, fParentNdx, publisher );
		}

	// This method is called when the publishing 
	// service runs out of buffer space. The subscriber 
	// has to free as many events as possible and send an 
	// EventDone message with their IDs to the publisher
	virtual int ReleaseEventsRequest( AliHLTPublisherInterface& publisher )
		{
		return fParent->ReleaseEventsRequest( this, fParentNdx, publisher );
		}

	// Method used to check alive status of subscriber. 
	// This message has to be answered with 
	// PingAck message within less  than a second.
	virtual int Ping( AliHLTPublisherInterface& publisher )
		{
		return fParent->Ping( this, fParentNdx, publisher );
		}

	// Method used by a subscriber in response to a Ping 
	// received message. The PingAck message has to be 
	// called within less than one second.
	virtual int PingAck( AliHLTPublisherInterface& publisher )
		{
		return fParent->PingAck( this, fParentNdx, publisher );
		}


    protected:

	
	ParentClass* fParent;
	ParentIndexType fParentNdx;

    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTFORWARDINGSUBSCRIBER_HPP_
