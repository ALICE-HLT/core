/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3PUBSUBPIPECOM_HPP_
#define _ALIL3PUBSUBPIPECOM_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliHLTBlockHeader.h"


typedef struct AliHLTPubSubPipeMsg
    {

	enum MsgType { kNone=0, kSubscribe, kUnsubscribe, kSetPersistent,
		       kSetEventTypeMod, kSetEventTypeETT, kSetTransientTimeout,
		       kEventDoneSing, kEventDoneMult, kStartPublishing,
		       kPing, kPingAck, kNewEvent, kEventCanceled, 
		       kSubscriptionCanceled, kReleaseEventsRequest, 
		       kSetEventDoneDataSend, kEventDoneData, kQuitLoop };

	AliHLTBlockHeader  fHeader;
	AliUInt32_t       fType;
	AliUInt32_t       fParamCnt;
	AliUInt8_t        fSubscriberName[0];

    } AliHLTPubSubPipeMsg;

#define ALIL3PUBSUBPIPEMSG_TYPE       ((AliUInt32_t)'PUPM')

typedef struct AliHLTPubSubPipeMsgParam
    {
	enum ParamType { kBool=1, kUInt32, kEvtID, kETT, kVectorETT, kEvtDoneD, kVectorEvtDoneD,
	kSEDD };
	AliHLTBlockHeader fHeader;
	AliUInt32_t fType;
	AliUInt32_t fDataType;
	AliUInt64_t fData;
	// For the vector params the data is the element count,
	// the elements are just appended.
    } AliHLTPubSubPipeMsgParam;

#define ALIL3PUBSUBPIPEMSGPARAM_TYPE  ((AliUInt32_t)'PPMP')



class AliHLTPublisherInterface;
class AliHLTSubscriberInterface;

typedef void (*AliHLTAcceptingSubscriptionsCallbackFunc)( void* data );

int PublisherPipeSubscriptionInputLoop( AliHLTPublisherInterface& interface, AliHLTAcceptingSubscriptionsCallbackFunc callback=0, void* callbackData=0 );

void PublisherPipeSubscriptionCanceled( AliHLTPublisherInterface& interface );

void QuitPipeSubscriptionLoop( AliHLTPublisherInterface& interface );


#define ALIL3PIPEBASEDIR "/tmp/"

extern bool gQuitPipeSubscriptionInputLoop;
extern bool gQuittedPipeSubscriptionInputLoop;

extern AliUInt32_t gPipeComTimeout;




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3PUBSUBPIPECOM_HPP_ 
