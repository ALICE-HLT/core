/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3PUBLISHERINTERFACE_HPP_
#define _ALIL3PUBLISHERINTERFACE_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliEventID.h"
#include "AliHLTEventDoneData.h"
#include "AliHLTEventTriggerStruct.h"
#include "MLUCString.hpp"
#include <vector>


class AliHLTSubscriberInterface;
class AliHLTEventAccounting;

#if __GNUC__>=3
using namespace std;
#endif


class AliHLTPublisherInterface
    {
    public:


	// Constructor. The name argument serves as the
	// unique identifier for this subscription service
	AliHLTPublisherInterface( const char* name );

	// Destructor
	virtual ~AliHLTPublisherInterface();

	// Subscribe the given AliHLTSubscriberInterface to this 
	// subscription service.
	virtual int Subscribe( AliHLTSubscriberInterface& 
			       interface ) = 0;

	// Unsubscribe a previously subscribed Subscriber-
	// Interface from this subscription service.
	virtual int Unsubscribe( AliHLTSubscriberInterface& 
				 interface ) = 0;

 

	// Set the persistency status of the specified 
	// AliHLTSubscriberInterface:
	// Persistent (bool==true) for DAQ or L3; 
	// Transient (aka non-persistent) (bool==false)
	// for monitors. For monitors more event selection 
	// parameters can be specified (see below).
	virtual int SetPersistent( AliHLTSubscriberInterface& 
				   interface, bool ) = 0;  

	// Specify that the AliHLTSubscriberInterface, is 
	// only notified about every modulo'th event.
	virtual int SetEventType( AliHLTSubscriberInterface& 
				  interface,  
				  AliUInt32_t modulo ) = 0;

	// Specify that the AliHLTSubscriberInterface only 
	// wants to be notified about events whose trigger 
	// words appear in the passed list. The additional 
	// modulo parameter has the meaning as in the 
	// previous method and defaults to 1.
	virtual int SetEventType( AliHLTSubscriberInterface& 
				  interface, 
				  vector
				  <AliHLTEventTriggerStruct*>& 
				  triggerWords,  
				  AliUInt32_t modulo = 1 ) = 0;

	// This method is used to specify to the publisher the 
	// minimum amount of time that an event can be kept by
	// a transient subscriber. The timeout is given in 
	// milliseconds
	virtual int SetTransientTimeout( AliHLTSubscriberInterface& interface, 
					 AliUInt32_t timeout_ms ) = 0;

	// This method is used to specify to the publisher
	// that a subscriber wants to receive all the non-trivial
	// (fDataWordCount>0) event done data
	// structures sent for events by other subscribers. 
	virtual int SetEventDoneDataSend( AliHLTSubscriberInterface& 
					  interface, bool ) = 0;  

	// Method used by the subscriber to signal that it
	// finished working on the event with the event ID 
	// in the specified structure  and that it can now be
	// removed.
	// The payload specified in the structure can be 
	// interpreted according to an application's needs. 
	// forceForward signals that the data should be sent 
	// down the chain before the event is fully done yet
	virtual int EventDone( AliHLTSubscriberInterface& 
			       whichInterface, 
			       const AliHLTEventDoneData& 
			       eventDoneData, bool forceForward=false, bool forwarded=false ) = 0;


	// Method used by the subscriber to signal that it
	// finished working on the events whose event IDs are
	// contained in the list of structures.
	// For each event a separate payload can be specified
	// in its corresponding data structure.
	// forceForward signals that the data should be sent 
	// down the chain before the event is fully done yet
	virtual int EventDone( AliHLTSubscriberInterface& 
			       whichInterface, 
			       vector<AliHLTEventDoneData*>& 
			       eventDoneData, bool forceForward=false, bool forwarded=false ) = 0;

	// This method tells the publishing service to start 
	// the notification about events and starts an 
	// event loop from which the calling (and given) 
	// AliHLTSubscriberInterface thread will return after 
	// a SubscriptionCanceled message has been received by it.
	virtual int StartPublishing( AliHLTSubscriberInterface& 
				     interface, bool sendOldEvents = false ) = 0;

	// Method used to check the alive status of the 
	// publishing service. This message must be answered 
	// with a PingAck message to the calling (and given) 
	// AliHLTSubscriberInterface within less than a second.
	virtual int Ping( AliHLTSubscriberInterface& interface ) = 0;

	// Method used by the AliHLTSubscriberInterface in 
	// response to a Ping message received from this 
	// publishing service. The PingAck message must be 
	// called  in at most one second after the Ping 
	// message has been received.
	virtual int PingAck( AliHLTSubscriberInterface& 
			     interface ) = 0;

	// Get the publisher's name
	const char* GetName();

	virtual void SetEventAccounting( AliHLTEventAccounting* /*eventAccounting*/ )
		{
		}

    protected:


	MLUCString fName;

    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3PUBLISHERINTERFACE_HPP_
