/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3SAMPLESUBSCRIBER_HPP_
#define _ALIL3SAMPLESUBSCRIBER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSubscriberInterface.hpp"

class AliHLTPublisherInterface;


class AliHLTSampleSubscriber: public AliHLTSubscriberInterface
    {
    public:
	    
	// Constructor. 
	// The name argument uniquely identifies 
	// AliHLTSampleSubscriber to publishing services.
	AliHLTSampleSubscriber( const char* name );

	// Destructor
	virtual ~AliHLTSampleSubscriber();

	// This method is called whenever a new wanted 
	// event is available.
	virtual int NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
			      const AliHLTSubEventDataDescriptor& sbevent,
			      const AliHLTEventTriggerStruct& ets );

	// This method is called for a transient subscriber, 
	// when an event has been removed from the list. 
	virtual int EventCanceled( AliHLTPublisherInterface& publisher, AliEventID_t eventID );

	// This method is called for a subscriber when new event done data is 
	// received by a publisher (sent from another subscriber) for an event. 
	virtual int EventDoneData( AliHLTPublisherInterface& publisher, 
				   const AliHLTEventDoneData& 
				   eventDoneData );

	// This method is called when the subscription ends.
	virtual int SubscriptionCanceled( AliHLTPublisherInterface& publisher );

	// This method is called when the publishing 
	// service runs out of buffer space. The subscriber 
	// has to free as many events as possible and send an 
	// EventDone message with their IDs to the publisher
	virtual int ReleaseEventsRequest( AliHLTPublisherInterface& publisher ); 

	// Method used to check alive status of subscriber. 
	// This message has to be answered with 
	// PingAck message within less  than a second.
	virtual int Ping( AliHLTPublisherInterface& publisher );

	// Method used by a subscriber in response to a Ping 
	// received message. The PingAck message has to be 
	// called within less than one second.
	virtual int PingAck( AliHLTPublisherInterface& publisher );

	AliUInt32_t GetPingCount()
		{
		return fPingCount;
		}

	void PingPublisher( AliHLTPublisherInterface& pub );


    protected:

	AliUInt32_t fPingCount;

    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3SAMPLESUBSCRIBER_HPP_
