#ifndef _ALIL3BASEPUBLISHERMSGS_HPP_
#define _ALIL3BASEPUBLISHERMSGS_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSubscriberInterface.hpp"
#include "MLUCFifo.hpp"

const unsigned kAliHLTBasePublisherQuitThreadMsg         = 0x00000000;
const unsigned kAliHLTBasePublisherSubscribeMsg          = 0x00000001;
const unsigned kAliHLTBasePublisherUnsubscribeMsg        = 0x00000002;
const unsigned kAliHLTBasePublisherSetPersistentMsg      = 0x00000003;
const unsigned kAliHLTBasePublisherSetEventTypeModMsg    = 0x00000004;
const unsigned kAliHLTBasePublisherSetEventTypeETTMsg    = 0x00000005;


struct AliHLTBasePublisherMsg: public MLUCFifoMsg
    {
	AliUInt64_t fReplyNr;
    };


struct AliHLTBasePublisherSubscriberMsg: public AliHLTBasePublisherMsg
    {
	AliHLTSubscriberInterface* fSubscriber;
    };


struct AliHLTBasePublisherSubscriberParam1Msg: public AliHLTBasePublisherSubscriberMsg
    {
	AliUInt64_t fParam;
    };

struct AliHLTBasePublisherSubscriberParam2Msg: public AliHLTBasePublisherSubscriberMsg
    {
	AliUInt64_t fParams[2];
    };

struct AliHLTBasePublisherSubscriberParam3Msg: public AliHLTBasePublisherSubscriberMsg
    {
	AliUInt64_t fParams[3];
    };






/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3BASEPUBLISHERMSGS_HPP_
