#ifndef _ALIHLTFORWARDINGPUBLISHER_HPP_
#define _ALIHLTFORWARDINGPUBLISHER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSamplePublisher.hpp"

template<class ParentClass, class ParentIndexType>
class AliHLTForwardingPublisher: public AliHLTSamplePublisher
    {
    public:

	AliHLTForwardingPublisher( ParentClass* parent, ParentIndexType ndx, const char* name, int eventSlotsPowerOfTwo = -1 ):
	    AliHLTSamplePublisher( name, eventSlotsPowerOfTwo ), fParent( parent ), fParentNdx( ndx )
		{
		CacheEventDoneDataAllocs();
		}
	virtual ~AliHLTForwardingPublisher() {};

    protected:
	
	virtual void CanceledEvent( AliEventID_t event, vector<AliHLTEventDoneData*>& eventDoneData )
		{
		fParent->CanceledEvent( this, fParentNdx, event, eventDoneData );
		}
	// empty callback. Will be called when an
	//event has already been internally canceled. Will be 
	// called with no mutexes locked!

	virtual void AnnouncedEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, 
				     const AliHLTEventTriggerStruct& trigger, AliUInt32_t refCount )
		{
		fParent->AnnouncedEvent( this, fParentNdx, eventID, sbevent, trigger, refCount );
		}
	// empty callback. Will be called when an
	//event has been announced. Will be 
	// called with no mutexes locked!

	virtual void EventDoneDataReceived( const char* subscriberName, AliEventID_t eventID,
					    const AliHLTEventDoneData* edd, bool forceForward, bool forwarded )
		{
		fParent->EventDoneDataReceived( this, fParentNdx, subscriberName, eventID, edd, forceForward, forwarded );
		}
	// empty callback. Will be called when an event done message with non-trivial event done data
	// (fDataWordCount>0) has been received for that event. 
	// This is called prior to the sending of the EventDoneData to interested other subscribers
	// (SendEventDoneData above)
	// called with no mutexes locked!

	virtual bool GetAnnouncedEventData( AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
				      AliHLTEventTriggerStruct*& trigger )
		{
		return fParent->GetAnnouncedEventData( this, fParentNdx, eventID, sedd, trigger );
		}
	// Empty callback, called when an already announced event has to be announced again to a new
	// subscriber that wants to receive old events. This method needs to return the pointers
	// to the sub-event data descriptor and the event trigger structure. If the function returns
	// false this is a signal that the desired data could not be found anymore. 
	// This default implementation just returns false


	ParentClass* fParent;
	ParentIndexType fParentNdx;
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTFORWARDINGPUBLISHER_HPP_
