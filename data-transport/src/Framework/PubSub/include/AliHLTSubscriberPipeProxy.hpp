/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3SUBSCRIBERPIPEPROXY_HPP_
#define _ALIL3SUBSCRIBERPIPEPROXY_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSubscriberProxyInterface.hpp"
#include "AliHLTPipeCom.hpp"
#include "AliHLTPubSubPipeCom.hpp"
#include <pthread.h>


//#define EVENT_CHECK_PARANOIA
// EVENT_CHECK_PARANOIA_LIMIT defined in source file.


class AliHLTPublisherInterface;
class AliHLTEventAccounting;


class AliHLTSubscriberPipeProxy: public AliHLTSubscriberProxyInterface
    {
    public:
	    
	// Constructor. 
	// The name argument uniquely identifies 
	// AliHLTSubscriberInterface to publishing services.
	AliHLTSubscriberPipeProxy( const char* name );

	// Destructor
	virtual ~AliHLTSubscriberPipeProxy();

	// This method is called whenever a new wanted 
	// event is available.
	virtual int NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
			      const AliHLTSubEventDataDescriptor& sbevent,
			      const AliHLTEventTriggerStruct& ets );

	// This method is called for a transient subscriber, 
	// when an event has been removed from the list. 
	virtual int EventCanceled( AliHLTPublisherInterface& publisher, AliEventID_t eventID );

	// This method is called for a subscriber when new event done data is 
	// received by a publisher (sent from another subscriber) for an event. 
	virtual int EventDoneData( AliHLTPublisherInterface& publisher, 
				   const AliHLTEventDoneData& 
				   eventDoneData );

	// This method is called when the subscription ends.
	virtual int SubscriptionCanceled( AliHLTPublisherInterface& publisher );

	// This method is called when the publishing 
	// service runs out of buffer space. The subscriber 
	// has to free as many events as possible and send an 
	// EventDone message with their IDs to the publisher
	virtual int ReleaseEventsRequest( AliHLTPublisherInterface& publisher ); 

	// Method used to check alive status of subscriber. 
	// This message has to be answered with 
	// PingAck message within less  than a second.
	virtual int Ping( AliHLTPublisherInterface& publisher );

	// Method used by a subscriber in response to a Ping 
	// received message. The PingAck message has to be 
	// called within less than one second.
	virtual int PingAck( AliHLTPublisherInterface& publisher );

	virtual int MsgLoop( AliHLTPublisherInterface& interface );

	void QuitMsgLoop( bool wait = true );

	void SetTimeout( AliUInt32_t timeout_usec );

	virtual void SetEventAccounting( AliHLTEventAccounting* eventAccounting )
		{
		fEventAccounting = eventAccounting;
		}

    protected:

	void MakeMsg( AliHLTPubSubPipeMsg* msg, AliHLTPubSubPipeMsg::MsgType type,
				     AliUInt32_t paramCnt );

	void MakeParam( AliHLTPubSubPipeMsgParam& param, const AliHLTSubEventDataDescriptor& );
	void MakeParam( AliHLTPubSubPipeMsgParam& param, const AliHLTEventTriggerStruct& );
	void MakeParam( AliHLTPubSubPipeMsgParam& param, AliEventID_t val );
	void MakeParam( AliHLTPubSubPipeMsgParam& param, 
			const AliHLTEventDoneData& eventDoneData );

	int HandleSetPersistentMsg( AliHLTPublisherInterface& interface, AliHLTPubSubPipeMsg* msg );
	int HandleSetEventTypeModMsg( AliHLTPublisherInterface& interface, AliHLTPubSubPipeMsg* msg );
	int HandleSetEventTypeETTMsg( AliHLTPublisherInterface& interface, AliHLTPubSubPipeMsg* msg );
	int HandleSetTransientTimeoutMsg( AliHLTPublisherInterface& interface, AliHLTPubSubPipeMsg* msg );
	int HandleSetEventDoneDataSendMsg( AliHLTPublisherInterface& interface, AliHLTPubSubPipeMsg* msg );
	int HandleEventDoneSingMsg( AliHLTPublisherInterface& interface, AliHLTPubSubPipeMsg* msg );
	int HandleEventDoneMultMsg( AliHLTPublisherInterface& interface, AliHLTPubSubPipeMsg* msg );
	int HandleStartPublishingMsg( AliHLTPublisherInterface& interface, AliHLTPubSubPipeMsg* msg, 
				      bool& sendOldEvents );


	AliHLTPipeCom fPubSubCom;
	
	bool fQuit;
	bool fQuitted;

	pthread_mutex_t fMutex;

	AliHLTPubSubPipeMsgParam * fParBool;
	AliUInt32_t fParBoolSize;
	AliHLTPubSubPipeMsgParam * fParCount;
	AliUInt32_t fParCountSize;
	AliHLTPubSubPipeMsgParam * fParEvtID;
	AliUInt32_t fParEvtIDSize;
	AliHLTPubSubPipeMsgParam * fHSETMETT;
	AliUInt32_t fHSETMETTSize;
	struct THSETMETTData
	    {
		AliHLTEventTriggerStruct* fETT;
		AliUInt32_t fSize;
	    };
	vector<THSETMETTData> fHSETMETTs;
	AliHLTPubSubPipeMsgParam * fHSETMMod;
	AliUInt32_t fHSETMModSize;
	AliHLTEventDoneData* fHEDSMEDD;
	AliUInt32_t fHEDSMEDDSize;
	AliHLTPubSubPipeMsgParam * fParVect;
	AliUInt32_t fParVectSize;
	struct THEDMMEDDData
	    {
		AliHLTEventDoneData* fEDD;
		AliUInt32_t fSize;
	    };
	vector<THEDMMEDDData> fHEDMMEDDs;
	


#ifdef PIPECOM_DOCRC
	// CRC stuff
#endif
	
	long fCRCTable[ 256 ];

#ifdef EVENT_CHECK_PARANOIA
	AliEventID_t fLastEventDoneID;
	AliEventID_t fLastEventAnnouncedID;
#endif

#ifdef PIPE_PARANOIA
	AliUInt64_t fWriteCount;
	AliUInt64_t fReadCount;
#endif

	AliHLTEventAccounting* fEventAccounting;

	MLUCString fProxyName;


    private:
    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3SUBSCRIBERPIPEPROXY_HPP_
