/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3PIPECOM_HPP_
#define _ALIL3PIPECOM_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliHLTLog.hpp"
#include "AliHLTBlockHeader.h"
#include "MLUCString.hpp"
#include <sys/time.h>
#include <semaphore.h>

//#define PIPE_PARANOIA

//class AliHLTLog;

#define DEFAULTPIPEBLOCKSIZE 65536


/**
 * A class for communication between two processes
 * via named pipes.
 * The object sets up two pipes for each process, one
 * for reading, one for writing. Optionally a process may
 * choose to open just the reading pipe.
 *
 * @short A class for named pipe communication between two processes
 * @author $Author$ - Initial version by Timm Morten Steinbeck
 * @version  $Id$
 */
class AliHLTPipeCom
    {
    public:


	/**
	 * Constructor.
	 * Does not open the pipes.
	 */
	AliHLTPipeCom();

	/**
	 * Constructor.
	 * Will attempt to open the specified pipes.
	 *
	 * @param readPipe The name of the pipe from which to read data.
	 * @param writePipe The name of the pipe to which to write data.
	 * @param create Specifies if the pipe file entries are to be created
	 * (via mkfifo (2)) if they do not exist already.
	 * @see AliHLTLog
	 */
	AliHLTPipeCom( const char* readPipe, const char* writePipe, bool create );

	/**
	 * Constructor.
	 * Will attempt to open the specified read pipe.
	 *
	 * @param readPipe The name of the pipe from which to read data.
	 * @param create Specifies if the pipe file entries are to be created
	 * (via mkfifo (2)) if they do not exist already.
	 * @see AliHLTLog
	 */
	AliHLTPipeCom( const char* readPipe, bool create );

	/**
	 * Constructor.
	 * Will attempt to open the specified write pipe.
	 *
	 * @param writePipe The name of the pipe to which to write data.
	 * @see AliHLTLog
	 */
	AliHLTPipeCom( const char* writePipe );

	/**
	 * Desctructor.
	 * Will close the pipes.
	 */
	virtual ~AliHLTPipeCom();

	/**
	 * Open the specified pipes.
	 * Failures on opening will be logged to the system
	 * logging mechanism
	 *
	 * @return 0 on success, an error indicator (errno conform) otherwise
	 * @param readPipe The name of the pipe from which to read data.
	 * @param writePipe The name of the pipe to which to write data.
	 * @param create Specifies if the pipe file entries are to be created
	 * (via mkfifo (2)) if they do not exist already.
	 * @see AliHLTLog
	 */
	int Open( const char* readPipe, const char* writePipe, bool create );

	/**
	 * Open the specified read pipe.
	 *
	 * @return 0 on success, an error indicator (errno conform) otherwise
	 * @param readPipe The name of the pipe from which to read data.
	 * @param create Specifies if the pipe file entries are to be created
	 * (via mkfifo (2)) if they do not exist already.
	 * @see AliHLTLog
	 */
	int OpenRead( const char* readPipe, bool create );

	/**
	 * Open the specified write pipe.
	 *
	 * @return 0 on success, an error indicator (errno conform) otherwise
	 * @param writePipe The name of the pipe to which to write data.
	 * @see AliHLTLog
	 */
	int OpenWrite( const char* writePipe );

	/**
	 * Close all open pipes.
	 *
	 * @return 0 on success, an error indicator (errno conform) otherwise
	 */
	int Close();

	/**
	 * Read the specified amount of data (from the read pipe)
	 * into the specified buffer.
	 *
	 * @return 0 on success, an error indicator (errno conform) otherwise
	 * @param size The number of bytes to read from the pipe.
	 * @param buffer The address of the memory area into which to
	 * store the read data.
	 */
	int Read( AliUInt32_t size, AliUInt8_t* buffer, bool block=false, struct timeval* timeout=NULL );

	/**
	 * Read a structure from the pipe that is described by a block 
	 * header structure. This assumes, that the header structure is
	 * the first element of the structure. The memory for the structure
	 * will be allocated in the method and has to be deleted by a call to
	 * 'delete [] header;'.
	 * 
	 * @return 0 on success, an error indicator (errno conform) otherwise
	 * @param header Reference to a pointer, where the location of the
	 * allocated memory with the read structure will be stored.
	 */
	int Read( AliHLTBlockHeader*& header, bool block=false, struct timeval* timeout=NULL );
		
	/**
	 * Write the specified amount of data from the specified 
	 * buffer (into the write pipe).
	 *
	 * @return 0 on success, an error indicator (errno conform) otherwise
	 * @param size The number of bytes to write to the pipe.
	 * @param buffer The address of the memory area where the data
	 * to be written is stored.
	 */
	int Write( AliUInt32_t size, const AliUInt8_t* buffer, bool block=false, bool quitting = false );

	int Write( unsigned cnt, AliUInt32_t* sizes, AliUInt8_t** buffers, bool block=false );

	/**
	 * Write the structure defined by the specified block header
	 * into the write pipe. This assumes, that the header is the 
	 * first element of the structure to write.
	 *
	 * @return 0 on success, an error indicator (errno conform) otherwise
	 * @param header Pointer to the block header of the structure to write.
	 */
 	int Write( const AliHLTBlockHeader* header, bool block=false, bool quitting = false );

	int Write( unsigned cnt, AliHLTBlockHeader const** headers, bool block=false );

	/**
	 * bool conversion operator.
	 * Returns true, if pipe opening was successful.
	 *
	 * @return true if the pipes were opened successfully, false otherwise.
	 */
	operator bool();

	/**
	 * string conversion operator.
	 * Returns a description of the current state of the
	 * object as a string.
	 *
	 * @return A string with the current state of the object.
	 */
	//operator string();

	void SetTimeout( AliUInt32_t timeout_usec );

	const char* GetReadPipeName()
		{
		return fReadPipeName.c_str();
		}

	const char* GetWritePipeName()
		{
		return fWritePipeName.c_str();
		}

#ifdef PIPE_PARANOIA
	AliUInt64_t GetWriteCount() const
		{
		return fWriteCount;
		}
	AliUInt64_t GetReadCount() const
		{
		return fReadCount;
		}
#endif

    protected:

      AliUInt8_t* fCache;
      AliUInt32_t fCacheSize;
      AliUInt32_t fCacheStart;

	/**
	 * The name of the read pipe.
	 */
	MLUCString fReadPipeName;
		
	/**
	 * The name of the write pipe.
	 */
	MLUCString fWritePipeName;

	/**
	 * A flag saying wether the read pipe has to 
	 * be opened (true) or not (false)
	 */
	bool fOpenRead;

	/**
	 * A flag saying wether the write pipe has to 
	 * be opened (true) or not (false)
	 */
	bool fOpenWrite;

	/**
	 * The file handle (actually descriptor) of the read pipe.
	 */
//	int fReadHandle;
		
	/**
	 * The file handle (actually descriptor) of the write pipe.
	 */
//	int fWriteHandle;

//        int fPipeSize;

	struct timeval fTimeout;

	/**
	 * Create a pipe with the given name.
	 *
	 * @return 0 on success, an errno conforming error indicator otherwise
	 * @param name The name of the filesystem entry under which the pipe will appear.
	 */
	int CreatePipe( const char* name );
	
	/**
	 * Open a pipe with the given name.
	 *
	 * @return 0 on success, -1 otherwise.
	 * @param name The name of the filesystem entry which is the pipe to open
	 */
	//int OpenPipe( const char* name, bool read );


	/**
	 * Close the pipe with the given filehandle (actually file descriptor).
	 *
	 * @return 0 on success, an errno conforming error indicator otherwise
	 * @param handle The file descriptor of the pipe to close.
	 */
	//int ClosePipe( int& handle );

#ifdef PIPE_PARANOIA
	AliUInt64_t fWriteCount;
	AliUInt64_t fReadCount;
#endif

	//Shared memory stuff:
	struct sharedMemControl
	{
	    volatile unsigned int readPtr;
	    volatile unsigned int writePtr;
	    sem_t readSem;
	    sem_t writeSem;
	    //sem_t waitSem;
	    pthread_spinlock_t waitSpin;
	    volatile bool readerSleeping;
	    volatile bool initialized;
	    volatile int writers;
	};
	
	static const unsigned int sharedSizeBits = 16; //Must be at least 12 for a page size of 4 kb
	static const unsigned int sharedSize = 1 << sharedSizeBits;
	static const unsigned int sharedSizeBitMask = sharedSize - 1;
	static const unsigned int sharedControlSize = sizeof(sharedMemControl) + (4096 - sizeof(sharedMemControl) % 4096) % 4096;
	static const unsigned int sharedTotalSize = 2 * sharedSize + sharedControlSize;
	
	int fSharedFdReadHandle;
	int fSharedFdWriteHandle;
	void* fSharedReadMemory;
	void* fSharedWriteMemory;
	sharedMemControl* fSharedReadControl;
	sharedMemControl* fSharedWriteControl;
	unsigned char* fSharedReadBuffer;
	unsigned char* fSharedWriteBuffer;
	
	int OpenSharedSegment(const char* segmentname, bool createControl, int& fd, void*& memory, unsigned char*& buffer, sharedMemControl*& control, bool read);
	int CloseSharedSegment(const char* segmentname, bool deleteControl, int fd, void* memory, bool read);
	int writeShared(const unsigned char* buffer, unsigned int toWrite, bool atomic = false, bool nolock = false, bool quitting = false);
	void writeSharedMulti(unsigned int count, const unsigned char** buffer, unsigned int* sizes);
	int readShared(unsigned char* buffer, unsigned int toRead);
	int waitForWrite(timeval* timeout);

    private:

	    friend AliHLTLog& operator<<( AliHLTLog& log, const AliHLTPipeCom& );

    };


AliHLTLog& operator<<( AliHLTLog& log, const AliHLTPipeCom& );


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#endif // _ALIL3PIPECOM_HPP_

