/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef ALIl3PUBLISHERPIPEPROXY_HPP_
#define ALIl3PUBLISHERPIPEPROXY_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTPublisherInterface.hpp"
#include "AliHLTPublisherProxyInterface.hpp"
#include "AliHLTPipeCom.hpp"
#include "AliHLTPubSubPipeCom.hpp"
#include <pthread.h>

//#define EVENT_CHECK_PARANOIA
// EVENT_CHECK_PARANOIA_LIMIT defined in source file.

class AliHLTSubEventDataDescriptor;
class AliHLTEventAccounting;

// Store debug information about a condition sem's owner.
#define OWNER_DEBUG

class AliHLTPublisherPipeProxy: public AliHLTPublisherProxyInterface
    {
    public:

	AliHLTPublisherPipeProxy( const char* name );
	
	virtual ~AliHLTPublisherPipeProxy();


	// Subscribe the given AliHLTSubscriberInterface to this 
	// subscription service.
	virtual int Subscribe( AliHLTSubscriberInterface& 
			       interface );

	// Unsubscribe a previously subscribed Subscriber-
	// Interface from this subscription service.
	virtual int Unsubscribe( AliHLTSubscriberInterface& 
				 interface );

	// Set the persistency status of the specified 
	// AliHLTSubscriberInterface:
	// Persistent (bool==true) for DAQ or L3; 
	// Transient (aka non-persistent) (bool==false)
	// for monitors. For monitors more event selection 
	// parameters can be specified (see below).
	virtual int SetPersistent( AliHLTSubscriberInterface& 
				   interface, bool );  

	// Specify that the AliHLTSubscriberInterface, is 
	// only notified about every modulo'th event.
	virtual int SetEventType( AliHLTSubscriberInterface& 
				  interface,  
				  AliUInt32_t modulo );

	// Specify that the AliHLTSubscriberInterface only 
	// wants to be notified about events whose trigger 
	// words appear in the passed list. The additional 
	// modulo parameter has the meaning as in the 
	// previous method and defaults to 1.
	virtual int SetEventType( AliHLTSubscriberInterface& 
				  interface, 
				  vector
				  <AliHLTEventTriggerStruct*>& 
				  triggerWords,  
				  AliUInt32_t modulo = 1 );

	// This method is used to specify to the publisher the 
	// minimum amount of time that an event can be kept by
	// a transient subscriber. The timeout is given in 
	// milliseconds
	virtual int SetTransientTimeout( AliHLTSubscriberInterface& interface, 
					 AliUInt32_t timeout_ms );

	// This method is used to specify to the publisher
	// that a subscriber wants to receive all the event done data
	// structures sent for events by other subscribers. 
	virtual int SetEventDoneDataSend( AliHLTSubscriberInterface& 
					  interface, bool );  

	// Method used by the subscriber to signal that it
	// finished working on the event with the event ID 
	// in the specified structure  and that it can now be
	// removed.
	// The payload specified in the structure can be 
	// interpreted according to an application's needs. 
	virtual int EventDone( AliHLTSubscriberInterface& 
			       whichInterface, 
			       const AliHLTEventDoneData& 
			       eventDoneData, bool forceForward=false, bool forwarded=false );


	// Method used by the subscriber to signal that it
	// finished working on the events whose event IDs are
	// contained in the list of structures.
	// For each event a separate payload can be specified
	// in its corresponding data structure.
	virtual int EventDone( AliHLTSubscriberInterface& 
			       whichInterface, 
			       vector<AliHLTEventDoneData*>& 
			       eventDoneData, bool forceForward=false, bool forwarded=false );

	// This method tells the publishing service to start 
	// the notification about events and starts an 
	// event loop from which the calling (and given) 
	// AliHLTSubscriberInterface thread will return after 
	// an EventCanceled message has been received by it.
	virtual int StartPublishing( AliHLTSubscriberInterface& 
				     interface, bool sendOldEvents = false );

	// Method used to check the alive status of the 
	// publishing service. This message must be answered 
	// with a PingAck message to the calling (and given) 
	// AliHLTSubscriberInterface within less than a second.
	virtual int Ping( AliHLTSubscriberInterface& interface );

	// Method used by the AliHLTSubscriberInterface in 
	// response to a Ping message received from this 
	// publishing service. The PingAck message must be 
	// called  in at most one second after the Ping 
	// message has been received.
	virtual int PingAck( AliHLTSubscriberInterface& 
			     interface );



	operator bool()
		{
		if ( fPubSubComOpened )
		    return (bool)fPublCom && (bool)fPubSubCom;
		else
		    return (bool)fPublCom;
		};

	virtual void AbortPublishing();

	void SetTimeout( AliUInt32_t timeout_usec );

	void SetEventAccounting( AliHLTEventAccounting* eventAccounting )
		{
		fEventAccounting = eventAccounting;
		}

    protected:


	void MakeMsg( AliHLTPubSubPipeMsg* msg, const char* subscrName, 
		      AliHLTPubSubPipeMsg::MsgType type,
		      AliUInt32_t paramCnt );

	void MakeParam( AliHLTPubSubPipeMsgParam& param, bool val );
	void MakeParam( AliHLTPubSubPipeMsgParam& param, AliUInt32_t val );
	void MakeParam( AliHLTPubSubPipeMsgParam& param, vector<AliHLTEventTriggerStruct*>& val );
	void MakeParam( AliHLTPubSubPipeMsgParam& param, 
			const AliHLTEventDoneData& eventDoneData );
	void MakeParam( AliHLTPubSubPipeMsgParam& param, vector<AliHLTEventDoneData*>& val );

	virtual int MsgLoop( AliHLTSubscriberInterface& interface );

	virtual int HandleNewEventMsg( AliHLTSubscriberInterface& interface, AliHLTPubSubPipeMsg* msg );
	virtual int HandleEventCanceledMsg( AliHLTSubscriberInterface& interface, AliHLTPubSubPipeMsg* msg );
	virtual int HandleEventDoneDataMsg( AliHLTSubscriberInterface& interface, AliHLTPubSubPipeMsg* msg );

	AliHLTPipeCom fPublCom;
	AliHLTPipeCom fPubSubCom;

	bool fPubSubComOpened;

	pthread_mutex_t fMutex;
#ifdef OWNER_DEBUG
	bool fOwned;
	pthread_t fOwnerThread;
	pid_t fOwnerPID;
#endif

	bool fQuitLoop;
	bool fLoopQuitted;

	AliHLTPubSubPipeMsgParam* fParEvtID;
	AliUInt32_t fParEvtIDSize;
	AliHLTPubSubPipeMsgParam* fHNEMParSEDD;
	AliUInt32_t fHNEMParSEDDSize;
	AliHLTPubSubPipeMsgParam* fHNEMParETS;
	AliUInt32_t fHNEMParETSSize;
	AliHLTSubEventDataDescriptor* fHNEMSEDD;
	AliUInt32_t fHNEMSEDDSize;
	AliHLTEventTriggerStruct* fHNEMETS;
	AliUInt32_t fHNEMETSSize;
	AliHLTEventDoneData* fHEDMEDD;
	AliUInt32_t fHEDMEDDSize;


#ifdef PIPECOM_DOCRC
	// CRC stuff
	
	long fCRCTable[ 256 ];
#endif

#ifdef EVENT_CHECK_PARANOIA
	AliEventID_t fLastEventDoneID;
	AliEventID_t fLastEventAnnouncedID;
#endif

#ifdef PIPE_PARANOIA
	AliUInt64_t fWriteCount;
	AliUInt64_t fReadCount;
#endif


	AliHLTEventAccounting* fEventAccounting;

	MLUCString fProxyName;

    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // ALIl3PUBLISHERPIPEPROXY_HPP_
