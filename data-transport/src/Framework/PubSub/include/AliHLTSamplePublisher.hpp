/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3SAMPLEPUBLISHER_HPP_
#define _ALIL3SAMPLEPUBLISHER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

//#define EVENT_CHECK_PARANOIA
// EVENT_CHECK_PARANOIA_LIMIT defined in source file.

// Warning, the code enable by the following define could be buggy.
// .. but without it is not thoroughly tested either.
//#define PERSISTENT_PUBLISHER_TIMEOUT


#include "AliHLTTypes.h"
#include "AliHLTPublisherInterface.hpp"
#include "AliHLTSubscriberInterface.hpp"
#include "AliHLTThread.hpp"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTTimer.hpp"
#include "AliHLTPubSubPipeCom.hpp"
#include "AliHLTEventModuloCalculation.hpp"
#include "MLUCFifo.hpp"
#include "MLUCAllocCache.hpp"
#include "MLUCDynamicAllocCache.hpp"
#include "MLUCConditionSem.hpp"
#include "MLUCIndexedVector.hpp"
#include <vector>
#include <list>
#include <pthread.h>

//#define PROFILING
#ifdef PROFILING
#include "MLUCHistogram.hpp"

inline double CalcTimeDiff_us( struct timeval* t1, struct timeval* t2 )
    {
    double tmp;
    tmp = t2->tv_sec-t1->tv_sec;
    tmp *= 1000000;
    tmp += t2->tv_usec-t1->tv_usec;
    return tmp;
    }


#endif


class AliHLTEventAccounting;


class AliHLTSamplePublisher: public AliHLTPublisherInterface
    {
    public:


	// Constructor. The name argument serves as the
	// unique identifier for this subscription service
	// The eventSlotsPowerOfTwo argument specifies the number 
	// of event slots that are to be made available in the publisher.
	// If this is less than zero, then this number will be kept variable with 
	// possible overhead if the number of events in the publisher is large.
	// If greater or equal to zero, the number of slots will be 
	// 2^eventSlotsPowerOfTwo.
	AliHLTSamplePublisher( const char* name, int eventSlotsPowerOfTwo = -1 );

	// Destructor
	virtual ~AliHLTSamplePublisher();

	// Subscribe the given AliHLTSubscriberInterface to this 
	// subscription service.
	virtual int Subscribe( AliHLTSubscriberInterface& 
			       interface );

	// Unsubscribe a previously subscribed Subscriber-
	// Interface from this subscription service.
	virtual int Unsubscribe( AliHLTSubscriberInterface& 
				 interface );

 

	// Set the persistency status of the specified 
	// AliHLTSubscriberInterface:
	// Persistent (bool==true) for DAQ or L3; 
	// Transient (aka non-persistent) (bool==false)
	// for monitors. For monitors more event selection 
	// parameters can be specified (see below).
	virtual int SetPersistent( AliHLTSubscriberInterface& 
				   interface, bool );  

	// Specify that the AliHLTSubscriberInterface, is 
	// only notified about every modulo'th event.
	virtual int SetEventType( AliHLTSubscriberInterface& 
				  interface,  
				  AliUInt32_t modulo );

	// Specify that the AliHLTSubscriberInterface only 
	// wants to be notified about events whose trigger 
	// words appear in the passed list. The additional 
	// modulo parameter has the meaning as in the 
	// previous method and defaults to 1.
	virtual int SetEventType( AliHLTSubscriberInterface& 
				  interface, 
				  vector
				  <AliHLTEventTriggerStruct*>& 
				  triggerWords,  
				  AliUInt32_t modulo = 1 );

	// This method is used to specify to the publisher the 
	// minimum amount of time that an event can be kept by
	// a transient subscriber. The timeout is given in 
	// milliseconds
	virtual int SetTransientTimeout( AliHLTSubscriberInterface& interface, 
					 AliUInt32_t timeout_ms );

	// This method is used to specify to the publisher
	// that a subscriber wants to receive all the non-trivial
	// (fDataWordCount>0) event done data
	// structures sent for events by other subscribers. 
	virtual int SetEventDoneDataSend( AliHLTSubscriberInterface& 
					  interface, bool );  

	// Method used by the subscriber to signal that it
	// finished working on the event with the specified 
	// event ID and that it can now be removed.
	virtual int EventDone( AliHLTSubscriberInterface& 
			       whichInterface, 
			       const AliHLTEventDoneData& 
			       eventDoneData, 
			       bool forceForward=false, 
			       bool forwarded=false );

	// Method used by the subscriber to signal that it
	// finished working on the events in the list 
	// of event IDs.
	virtual int EventDone( AliHLTSubscriberInterface& 
			       whichInterface, 
			       vector<AliHLTEventDoneData*>& 
			       eventDoneData, 
			       bool forceForward=false, 
			       bool forwarded=false );

	// This method tells the publishing service to start 
	// the notification about events and starts an 
	// event loop from which the calling (and given) 
	// AliHLTSubscriberInterface thread will return after 
	// a SubscriptionCanceled message has been received by it.
	virtual int StartPublishing( AliHLTSubscriberInterface& 
				     interface, bool sendOldEvents = false );

	// Method used to check the alive status of the 
	// publishing service. This message must be answered 
	// with a PingAck message to the calling (and given) 
	// AliHLTSubscriberInterface within less than a second.
	virtual int Ping( AliHLTSubscriberInterface& interface );

	// Method used by the AliHLTSubscriberInterface in 
	// response to a Ping message received from this 
	// publishing service. The PingAck message must be 
	// called  in at most one second after the Ping 
	// message has been received.
	virtual int PingAck( AliHLTSubscriberInterface& 
			     interface );


	virtual int AnnounceEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, const AliHLTEventTriggerStruct& trigger );

	virtual void AbortEvent( AliEventID_t eventID, bool sendEventCanceled = true, bool showMessage = true );

	virtual void AbortAllEvents( bool sendEventCanceled = true, bool showSingleEventMessages = true );

	virtual int AcceptSubscriptions( AliHLTAcceptingSubscriptionsCallbackFunc callback=NULL, void* callbackData=NULL );
	virtual void AcceptSubscriptionsVoid()
		{
		AcceptSubscriptions( NULL, NULL );
		}

	virtual AliUInt32_t GetPendingEventCount();

	virtual void GetPendingEvents( vector<AliEventID_t>& events );

	virtual int RequestEventRelease();
	
	virtual int CancelAllSubscriptions( bool sendUnsubscribeMsgs, bool waitForLoopEnds = false );

	virtual int CancelSubscription( const char* subscriberName, bool sendUnsubscribeMsg );

	virtual int PingActive();

	//virtual void CancelSubscription( AliHLTSubscriberInterface& forInterface );

	void SetMaxTimeout( AliUInt32_t timeout_ms );

	void SetPingTimeout( AliUInt32_t timeout_ms );

	void SetComTimeout( AliUInt32_t timeout_usec );

	void SetMaxPingCount( AliUInt32_t count );

	void SetEventModuloPreDivisor( unsigned long preDiv )
		{
		if ( preDiv )
		    fEventModuloCalculation.SetEventModuloPreDivisor(preDiv);
		    //fEventModuloPreDivisor = preDiv;
		}

	AliUInt32_t GetMaxPingCount();

	virtual void PinEvents( bool pin );

	bool GetEventData( AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
			   AliHLTEventTriggerStruct*& trigger )
		{
		return GetAnnouncedEventData( eventID, sedd, trigger );
		}


	void SetSubscriptionLoop( int (*loopFunc)( AliHLTPublisherInterface&, AliHLTAcceptingSubscriptionsCallbackFunc, void* ) )
		{
		fSubscriptionInputLoop = loopFunc;
		}

	void SetEventTriggerTypeCompareFunc( AliHLTEventTriggerTypeCompareFunc compFunc, void* compFuncParam );

#ifdef EVENT_CHECK_PARANOIA
	void DoParanoiaChecks( bool doChecks )
		{
		fDoParanoiaChecks = doChecks;
		}
#endif


	class SubscriberEventCallback
	    {
	    public:
		virtual ~SubscriberEventCallback() {};
		virtual void Subscribed( AliHLTSamplePublisher* pub, const char* name, AliHLTSubscriberInterface* sub ) = 0;
		virtual void Unsubscribed( AliHLTSamplePublisher* pub, const char* name, AliHLTSubscriberInterface* sub, bool forced ) = 0;
	    };

	void AddSubscriberEventCallback( SubscriberEventCallback* secb );
	void DelSubscriberEventCallback( SubscriberEventCallback* secb );

	// Important, this method must only be called when there are no events in the system.
	// Otherwise it will fail and return false
	bool CacheEventDoneDataAllocs()
		{
		if ( fEvents.GetCnt()>0 )
		    return false;
		fCachedEDDAllocs = true;
		return true;
		}

	void SetEventAccounting( AliHLTEventAccounting* eventAccounting )
		{
		fEventAccounting = eventAccounting;
		}

    protected:

	class AliHLTSubscriberThread;
	class AliHLTSubscriberSendThread;
	class AliHLTPingTimeoutThread;
	class AliHLTEventTimeoutThread;
	class AliHLTSubscriberCleanupThread;
	    friend class AliHLTSubscriberThread;
	    friend class AliHLTSubscriberSendThread;
	    friend class AliHLTEventTimeoutThread;
	    friend class AliHLTPingTimeoutThread;
	    friend class AliHLTSubscriberCleanupThread;
	

	typedef struct SubscriberData
	    {
		bool fActive;
		bool fPersistent;
		bool fSendEventDoneData;
		AliUInt32_t fCount;
		AliUInt32_t fModulo;
		AliHLTSubscriberInterface* fInterface;
		AliHLTSubscriberThread* fThread;
		bool fThreadActive;
		AliHLTSubscriberSendThread* fSendThread;
		bool fSendThreadActive;
		AliUInt32_t fTimeout;
#if 0
		vector<AliHLTEventTriggerStruct*> fTriggers;
#else
		unsigned long fTriggerCnt;
		AliHLTEventTriggerStruct** fTriggers;
#endif
		//vector<AliEventID_t> fEvents;
		AliUInt32_t fPingCount;
		pthread_mutex_t fUsedMutex;
	    }
	SubscriberData;

	typedef struct EventTimeoutData
	    {
		AliEventID_t fEventID;
	    }
	EventTimeoutData;

	MLUCAllocCache fEventTimeoutDataCache;

	typedef struct EventSubscriberData
	    {
		AliHLTSubscriberInterface* fSubscriber;
		bool fPersistent;
		EventSubscriberData() { fSubscriber=NULL;fPersistent=false; };
	    }
	EventSubscriberData;

	typedef struct EventData
	    {
		AliEventID_t fEventID;
		AliUInt32_t fEventBirth_s;
		AliUInt32_t fEventBirth_us;
		AliUInt32_t  fRefCount;
		AliUInt32_t fTransientCount;
		AliUInt32_t fSendEDDSubscriberCount;
		EventTimeoutData* fEventTimeout;
#ifdef PERSISTENT_PUBLISHER_TIMEOUT
		bool fFinalTimeout;
#endif
		bool fTimerRestartInProgress;
		char fTimerState;
		AliUInt32_t fMaxTimeout;
		AliUInt32_t fTimerID;
		vector<EventSubscriberData> fSubscribers;
		// Is this the right way to store event done data??
		// See the EventDone methods for more detail.
		vector<AliHLTEventDoneData*> fEventDoneData;
	    }
	EventData;



	class AliHLTSubscriberThread: public AliHLTThread
	    {
	    public:

		AliHLTSubscriberThread( AliHLTSamplePublisher& publ, SubscriberData* data );
		~AliHLTSubscriberThread();

	    protected:

		virtual void Run();

		AliHLTSamplePublisher& fPub;
		SubscriberData *fData;

	    private:
	    };

	class AliHLTSubscriberSendThread: public AliHLTThread, public AliHLTSubscriberInterface
	    {
	    public:

		AliHLTSubscriberSendThread( AliHLTSamplePublisher& pub, SubscriberData* data, AliUInt32_t msgBufferSize );
		~AliHLTSubscriberSendThread();

		void Quit();

		virtual int NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
				      const AliHLTSubEventDataDescriptor& sbevent,
				      const AliHLTEventTriggerStruct& ets );

		virtual int EventCanceled( AliHLTPublisherInterface& publisher, AliEventID_t eventID );

		virtual int EventDoneData( AliHLTPublisherInterface& publisher, 
					   const AliHLTEventDoneData& eventDoneData );

		virtual int SubscriptionCanceled( AliHLTPublisherInterface& publisher );

		virtual int ReleaseEventsRequest( AliHLTPublisherInterface& publisher ); 

		virtual int Ping( AliHLTPublisherInterface& publisher );

		virtual int PingAck( AliHLTPublisherInterface& publisher );

		MLUCFifo fSendFifo;

	    protected:

		virtual void Run();

		AliHLTSamplePublisher& fPub;
		SubscriberData* fData;
		AliHLTTimerConditionSem fSem;
		bool fQuit;
		bool fQuitted;
// XXXX TODO LOCKING XXXX
		MLUCMutex fQuitMutex;

		    friend class AliHLTSamplePublisher;

		struct MsgData
		    {
			//AliHLTPubSubPipeMsg::MsgType fMsg;
			AliEventID_t fID;
#if 0
			union 
			    {
				AliHLTSubEventDataDescriptor fSEDD;
				AliHLTEventDoneData fEDD;
			    } fPayload;
#else
			AliUInt8_t fPayload[0];
#endif
			AliHLTEventTriggerStruct* GetETS() const
				{
#if 0
				return (AliHLTEventTriggerStruct*)( ((AliUInt8_t*)&(fPayload.fSEDD))+fPayload.fSEDD.fHeader.fLength);
#else
				return (AliHLTEventTriggerStruct*)( ((AliUInt8_t*)fPayload)+((AliHLTSubEventDataDescriptor*)fPayload)->fHeader.fLength );
				}
			AliHLTSubEventDataDescriptor* GetSEDD() const
				{
				return (AliHLTSubEventDataDescriptor*)fPayload;
				}

			AliHLTEventDoneData* GetEDD() const
				{
				return (AliHLTEventDoneData*)fPayload;
#endif
				}
		    };

		AliHLTSubEventDataDescriptor* fAnnounceSEDD;
		AliUInt32_t fAnnounceSEDDSize;
		AliHLTEventDoneData* fAnnounceEDD;
		AliUInt32_t fAnnounceEDDSize;
		AliHLTEventTriggerStruct* fAnnounceETS;
		AliUInt32_t fAnnounceETSSize;
		
	    private:
		
	    };

	class AliHLTEventTimeoutThread: public AliHLTThread
	    {
	    public:

		AliHLTEventTimeoutThread( AliHLTSamplePublisher& publ, int signalSlotCntExp2 );
		~AliHLTEventTimeoutThread();

		void Quit();

	    protected:
		
		virtual void Run();

		AliHLTSamplePublisher& fPub;
		AliHLTTimerConditionSem fSem;
		bool fQuit;
		bool fQuitted;

		    friend class AliHLTSamplePublisher;
		
	    private:
	    };

	class AliHLTPingTimeoutThread: public AliHLTThread
	    {
	    public:
		
		AliHLTPingTimeoutThread( AliHLTSamplePublisher& publ );
		~AliHLTPingTimeoutThread();

		void Quit();

	    protected:

		virtual void Run();

		bool fQuit;
		bool fQuitted;
		AliHLTSamplePublisher& fPub;
		AliHLTTimerConditionSem fSem;

		    friend class AliHLTSamplePublisher;

	    private:
	    };

	class AliHLTSubscriberCleanupThread: public AliHLTThread
	    {
	    public:

		AliHLTSubscriberCleanupThread( AliHLTSamplePublisher& pub );
		~AliHLTSubscriberCleanupThread();
		void Quit();
		virtual void Run();
		void Signal( SubscriberData* data );
		bool HasWork() const
			{
			return fHasWork;
			}

	    protected:

		AliHLTSamplePublisher& fPub;
		bool fQuit;
		bool fQuitted;
		MLUCConditionSem<SubscriberData*> fSem;
		vector<SubscriberData*> fLockedSubscribers;

		bool fHasWork;

	    private:
	    };

	int (*fSubscriptionInputLoop)( AliHLTPublisherInterface&, AliHLTAcceptingSubscriptionsCallbackFunc, void* );

	AliHLTEventTriggerTypeCompareFunc fETTCompFunc;
	void* fETTCompFuncParam;
	
	AliUInt32_t fMaxTimeout;
	AliUInt32_t fPingTimeout;
	AliUInt32_t fMaxPingCount;
	AliUInt32_t fComTimeout;

	AliHLTTimer fTimer;
	AliHLTEventTimeoutThread fEventThread;
	AliHLTPingTimeoutThread fPingThread;
	AliHLTSubscriberCleanupThread fSubscriberCleanup;
	AliEventID_t fTimerCurrentCancelEvent;

	unsigned fSendThreadMsgBufferSizeExp2;

	bool fCachedEDDAllocs;
	MLUCDynamicAllocCache fEDDCache;


	// XXXXXXXXXXX
	// Please note, that if you absolutely need to lock
	// both the fSubscriberMutex AND the fEventMutex at the same time
	// and cannot make a workaround with locking one after the other,
	// then ALWAYS lock the fSubscriberMutex BEFORE the fEventMUTEX!!!
	pthread_mutex_t fSubscriberMutex;
	// This array (vector) is sorted by the fInterface member of the structure
	// pointed to.
	vector<SubscriberData*> fSubscribers;

	//typedef vector<EventData> TEventDataListType;
	typedef MLUCIndexedVector<EventData,AliEventID_t> TEventDataListType;
	//typedef list<EventData> TEventDataListType;
	// XXXXXXXXXXX
	// Please note, that if you absolutely need to lock
	// both the fSubscriberMutex AND the fEventMutex at the same time
	// and cannot make a workaround with locking one after the other,
	// then ALWAYS lock the fSubscriberMutex BEFORE the fEventMUTEX!!!
	pthread_mutex_t fEventMutex;
	//list<EventData> fEvents;
	TEventDataListType fEvents;
	// Number of events for which slots are to be kept available.
	// A value of 0 means that the vector is to grow dynamically.
	// For non-zero the number is fixed!
	// Also note, that for non-zero this number is always a power of two.
	// This eases the wrap around logic. See below.

	static void GetPendingEventsIterationFunc( const EventData& el, void* arg )
		{
		reinterpret_cast< vector<AliEventID_t>*>( arg )->push_back( el.fEventID );
		}
	static void FindUnusedEventsIterationFunc( const EventData& el, void* arg )
		{
		if ( el.fRefCount <= 0 )
		    reinterpret_cast< vector<AliEventID_t>*>( arg )->push_back( el.fEventID );
		}
	struct TSendCurrentEventsData
	    {
		vector<AliEventID_t> fEvents;
		//bool fTransientFound;
		EventSubscriberData fESD;
		AliHLTTimer* fTimer;
	    };
	static void SendCurrentEventsIterationFunc( EventData& el, void* arg )
		{
		TSendCurrentEventsData* sced = reinterpret_cast<TSendCurrentEventsData*>( arg );
		sced->fEvents.push_back( el.fEventID );
		if ( sced->fESD.fPersistent )
		    {
		    if ( el.fTimerID != ~(AliUInt32_t)0 )
			{
			sced->fTimer->CancelTimeout( el.fTimerID );
			el.fTimerID = ~(AliUInt32_t)0;
			}
		    el.fTransientCount++;
		    el.fTimerRestartInProgress = true;
		    }
		el.fSubscribers.push_back( sced->fESD );
		}
	static void RemoveSubscriberEventsIterationFunc( const EventData& el, void* arg )
		{
		TSendCurrentEventsData* sced = reinterpret_cast<TSendCurrentEventsData*>( arg );
		if ( FindSubscriber( el.fSubscribers, sced->fESD.fSubscriber ) != vector<EventSubscriberData>::iterator() )
		    sced->fEvents.push_back( el.fEventID );
		}
	struct TCleanupOldEventsData
	    {
		struct TCleanupEventData
		    {
			AliEventID_t fEventID;
			AliUInt32_t fEventBirth_s;
			AliUInt32_t fEventBirth_us;
		    };
		vector<TCleanupEventData> fEvents;
		AliUInt32_t fOldestEventBirth_s;
	    };
	static void CleanupOldEventsIterationFunc( const EventData& el, void* arg )
		{
		TCleanupOldEventsData* coed = reinterpret_cast<TCleanupOldEventsData*>( arg );
		if ( el.fEventBirth_s < coed->fOldestEventBirth_s )
		    {
		    TCleanupOldEventsData::TCleanupEventData ced;
		    ced.fEventID = el.fEventID;
		    ced.fEventBirth_s = el.fEventBirth_s;
		    ced.fEventBirth_us = el.fEventBirth_us;
		    coed->fEvents.push_back( ced );
		    //coed->fEvents.push_back( el.fEventID );
		    }
		}


	AliUInt32_t fOldestEventBirth_s;


	// Note: These methods must only be called in the fixed size
	// event slots mode, aka when fEventSlots!= 0!!
	unsigned long FindEventSlot( AliEventID_t eventID );

	pthread_mutex_t fSubscriberToDeleteMutex;
	vector<AliHLTSubscriberThread*> fSubscribersToDelete;
	
	void SendEventDoneData( AliHLTSubscriberInterface& interface, const AliHLTEventDoneData& edd, bool forwarded, const vector<EventSubscriberData>& remainingSubscribers ); // Must be called with fSubscriberMutex already locked!!!

	// This method tells the publisher to send all events that 
	// are currently already active to the calling (and passed)
	// subscriber. Called with no mutexes locked.
	virtual int SendCurrentEvents( AliHLTSubscriberInterface& interface );



	void ReleaseEvent( unsigned long evtNdx, EventData* evtIter, bool sendEventCanceled ); // Must be called with fEventMutex already locked!!!
	void EventDone( AliEventID_t eventID, const AliHLTEventDoneData* edd, AliHLTSubscriberInterface* sub, 
			AliUInt32_t refCount, AliUInt32_t transientCount, AliUInt32_t eddSubscriberCountCount, bool sendEventCanceled, bool& sendEDD, vector<EventSubscriberData>& remainingSubscribers ); // Must be called with fEventMutex already locked!!!
	// AliUInt32_t GetEventIndex( AliEventID_t eventID ); // Must be called with fEventMutex already locked!!!
	EventData* GetEventIter( AliEventID_t eventID ); // Must be called with fEventMutex already locked!!!
	void RemoveSubscriber( SubscriberData* data, bool forced, bool sendUnsubscribeMsg, bool waitForSubscriberMsgLoopEnd ); // Must be called with fSubscriberMutex already locked!!! Must not rely on consistency of fSubscriberMutex locked data; will unlocked/relocked in this method.
	void DeleteSubscriber( SubscriberData* data );
	int AnnounceEvent( SubscriberData* data, AliEventID_t eventID, 
			   const AliHLTSubEventDataDescriptor& sbevent,
			   const AliHLTEventTriggerStruct& ets ); // Must be called with fSubscriberMutex already locked!!!
	// Returns 0 on success, errno on failure...

	void CancelEvent( AliEventID_t evts, vector<AliHLTEventDoneData*>& eventDoneData, 
			  vector<EventSubscriberData>& transients, bool sendEventCanceled ); // Must be called with fSubscriberMutex already locked!!!

	void Ping( SubscriberData* data );


	virtual void CanceledEvent( AliEventID_t event, vector<AliHLTEventDoneData*>& eventDoneData );
	// empty callback. Will be called when an
	//event has already been internally canceled. Will be 
	// called with no mutexes locked!

	virtual void AnnouncedEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, 
				     const AliHLTEventTriggerStruct& trigger, AliUInt32_t refCount );
	// empty callback. Will be called when an
	//event has been announced. Will be 
	// called with no mutexes locked!

	virtual void EventDoneDataReceived( const char* subscriberName, AliEventID_t eventID,
					    const AliHLTEventDoneData*, bool forceForward, bool forwarded );
	// empty callback. Will be called when an event done message with non-trivial event done data
	// (fDataWordCount>0) has been received for that event. 
	// This is called prior to the sending of the EventDoneData to interested other subscribers
	// (SendEventDoneData above)
	// called with no mutexes locked!

	virtual bool GetAnnouncedEventData( AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
				      AliHLTEventTriggerStruct*& trigger );
	// Empty callback, called when an already announced event has to be announced again to a new
	// subscriber that wants to receive old events. This method needs to return the pointers
	// to the sub-event data descriptor and the event trigger structure. If the function returns
	// false this is a signal that the desired data could not be found anymore. 
	// This default implementation just returns false


	virtual void EventAnnounceError( SubscriberData* subscriberData, AliEventID_t eventID );
	// Default callback, can be overriden. Called with no mutexes locked.
	virtual void SubscriberCommunicationError( SubscriberData* subscriberData );
	// Default callback, can be overriden. Called with no mutexes locked.
	virtual void PingTimeout( SubscriberData* subscriberData );
	// Default callback, can be overriden. Called with no mutexes locked.
	virtual void MaxPingTimeout( SubscriberData* subscriberData );
	// Default callback, can be overriden. Called with no mutexes locked.

	void CleanupOldEvents();
	void CleanupOldEvents( AliUInt32_t oldestEvent_s );



	// Must be called with any corresponding mutexes locked.
	static int CompareSubscriberData( const void* key, const void* arrayMember );

	// Must be called with any corresponding mutexes locked.
	static vector<SubscriberData*>::const_iterator FindSubscriber( vector<SubscriberData*> const& subscribers, AliHLTSubscriberInterface* interface );
	static vector<SubscriberData*>::iterator FindSubscriber( vector<SubscriberData*>& subscribers, AliHLTSubscriberInterface* interface );

	// Must be called with any corresponding mutexes locked.
	static int CompareEventSubscriberData( const void* key, const void* arrayMember );

	// Must be called with any corresponding mutexes locked.
	static vector<EventSubscriberData>::const_iterator FindSubscriber( vector<EventSubscriberData> const& subscribers, AliHLTSubscriberInterface* interface );
	static vector<EventSubscriberData>::iterator FindSubscriber( vector<EventSubscriberData>& subscribers, AliHLTSubscriberInterface* interface );

#ifdef EVENT_CHECK_PARANOIA
	AliEventID_t fLastEventDoneID;
	AliEventID_t fLastEventAnnouncedID;
	AliEventID_t fLastEventSent1ID;
	AliEventID_t fLastEventSent2ID;
	bool fDoParanoiaChecks;
#endif

#ifdef PROFILING
	vector<MLUCHistogram*> fSubscriberSendThreadNewEventNoLocksHistos;
	vector<MLUCHistogram*> fSubscriberSendThreadNewEventHistos;
	vector<MLUCHistogram*> fSubscriberSendThreadNewEventCallHistos;
	vector<MLUCHistogram*> fMutexLockTimeHistos;
	vector<MLUCHistogram*> fMutexUnlockTimeHistos;
	vector<MLUCHistogram*> fSignalLockTimeHistos;
	vector<MLUCHistogram*> fSignalUnlockTimeHistos;
	vector<MLUCHistogram*> fEventDoneTimeHistos;
	vector<MLUCHistogram*> fEventDoneTimeNoLocksHistos;
	vector<MLUCHistogram*> fAnnounceEventTimeHistos;
	vector<MLUCHistogram*> fAnnounceEventTimeNoLocksHistos;
	vector<MLUCHistogram*> fAnnounceEventTimeSetupHistos;
	vector<MLUCHistogram*> fAnnounceEventTimeFindSubscribersHistos;
	vector<MLUCHistogram*> fAnnounceEventTimeStoreEventHistos;
	vector<MLUCHistogram*> fAnnounceEventTimeFindEventSlotHistos;
	vector<MLUCHistogram*> fAnnounceEventTimeAnnounceEventHistos;
	vector<MLUCHistogram*> fAnnounceEventTimeCallbacksHistos;
	vector<MLUCHistogram*> fAnnounceEventTimeSetTimerHistos;
	vector<MLUCHistogram*> fAnnounceEventTimeFindTimerHistos;
	
#endif

	bool fPinEvents;

	vector<SubscriberEventCallback*> fSubscriberEventCallbacks;
	pthread_mutex_t fSubscriberEventCallbackMutex;

	bool fEventModuloEventIDBased;
	//unsigned long fEventModuloPreDivisor;

	void SubscribedCallbacks( const char* name, AliHLTSubscriberInterface* sub );
	void UnsubscribedCallbacks( const char* name, AliHLTSubscriberInterface* sub, bool forced );

	AliHLTEventAccounting* fEventAccounting;

	bool fEventsAborted;

	AliHLTEventModuloCalculation fEventModuloCalculation;

    private:
    };






/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3SAMPLEPUBLISHER_HPP_
