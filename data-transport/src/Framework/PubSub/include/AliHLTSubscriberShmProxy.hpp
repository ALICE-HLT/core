/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3SUBSCRIBERSHMPROXY_HPP_
#define _ALIL3SUBSCRIBERSHMPROXY_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSubscriberProxyInterface.hpp"
#include "AliHLTShmCom.hpp"
#include "AliHLTPubSubShmCom.hpp"
#include <pthread.h>


class AliHLTPublisherInterface;

class AliHLTSubscriberShmProxy: public AliHLTSubscriberProxyInterface
    {
    public:
	    
	// Constructor. 
	// The name argument uniquely identifies 
	// AliHLTSubscriberInterface to publishing services.
	AliHLTSubscriberShmProxy( const char* name, AliUInt32_t shmSize, key_t pub2SubKey, key_t sub2PubKey );

	// Destructor
	virtual ~AliHLTSubscriberShmProxy();

	// This method is called whenever a new wanted 
	// event is available.
	virtual int NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
			      const AliHLTSubEventDataDescriptor& sbevent,
			      const AliHLTEventTriggerStruct& ets );

	// This method is called for a transient subscriber, 
	// when an event has been removed from the list. 
	virtual int EventCanceled( AliHLTPublisherInterface& publisher, AliEventID_t eventID );

	// This method is called for a subscriber when new event done data is 
	// received by a publisher (sent from another subscriber) for an event. 
	virtual int EventDoneData( AliHLTPublisherInterface& publisher, 
				   const AliHLTEventDoneData& 
				   eventDoneData );

	// This method is called when the subscription ends.
	virtual int SubscriptionCanceled( AliHLTPublisherInterface& publisher );

	// This method is called when the publishing 
	// service runs out of buffer space. The subscriber 
	// has to free as many events as possible and send an 
	// EventDone message with their IDs to the publisher
	virtual int ReleaseEventsRequest( AliHLTPublisherInterface& publisher ); 

	// Method used to check alive status of subscriber. 
	// This message has to be answered with 
	// PingAck message within less  than a second.
	virtual int Ping( AliHLTPublisherInterface& publisher );

	// Method used by a subscriber in response to a Ping 
	// received message. The PingAck message has to be 
	// called within less than one second.
	virtual int PingAck( AliHLTPublisherInterface& publisher );

	virtual int MsgLoop( AliHLTPublisherInterface& interface );

	void QuitMsgLoop(bool wait=true);

	void SetTimeout( AliUInt32_t timeout_usec );


    protected:

	void MakeMsg( AliHLTPubSubShmMsg* msg, AliHLTPubSubShmMsg::MsgType type,
		      AliUInt32_t paramCnt );

	void MakeParam( AliHLTPubSubShmMsgParam* param, const AliHLTSubEventDataDescriptor& );
	void MakeParam( AliHLTPubSubShmMsgParam* param, const AliHLTEventTriggerStruct& );
	void MakeParam( AliHLTPubSubShmMsgParam* param, AliEventID_t val );
	void MakeParam( AliHLTPubSubShmMsgParam* param, const AliHLTEventDoneData& );

	int HandleSetPersistentMsg( AliHLTPublisherInterface& interface, AliHLTPubSubShmMsg* msg, AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 );
	int HandleSetEventDoneDataSendMsg( AliHLTPublisherInterface& interface, AliHLTPubSubShmMsg* msg, AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 );
	int HandleSetEventTypeModMsg( AliHLTPublisherInterface& interface, AliHLTPubSubShmMsg* msg, AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 );
	int HandleSetEventTypeETTMsg( AliHLTPublisherInterface& interface, AliHLTPubSubShmMsg* msg, AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 );
	int HandleSetTransientTimeoutMsg( AliHLTPublisherInterface& interface, AliHLTPubSubShmMsg* msg, AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 );
	int HandleEventDoneSingMsg( AliHLTPublisherInterface& interface, AliHLTPubSubShmMsg* msg, AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 );
	int HandleEventDoneMultMsg( AliHLTPublisherInterface& interface, AliHLTPubSubShmMsg* msg, AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 );
	int HandleStartPublishingMsg( AliHLTPublisherInterface& interface, AliHLTPubSubShmMsg* msg, AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1, bool& sendOldEvents );


	// Communication direction is as seen from the proxies.
	// e.g. pub-to-sub means publisher-proxy to subscriber-proxy.
	AliHLTShmCom fPubToSub;
	AliHLTShmCom fSubToPub;
        
        key_t fPubToSubKey;
        key_t fSubToPubKey;

        AliUInt32_t fPubToSubTimeout;
	
	bool fQuit;
	bool fQuitted;

	pthread_mutex_t fMutex;

    private:
    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3SUBSCRIBERSHMPROXY_HPP_
