/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef ALIL3PUBLISHERSHMPROXY_HPP_
#define ALIL3PUBLISHERSHMPROXY_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTPublisherInterface.hpp"
#include "AliHLTPublisherProxyInterface.hpp"
#include "AliHLTShmCom.hpp"
#include "AliHLTPubSubShmCom.hpp"
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>

class AliHLTPublisherShmProxy: public AliHLTPublisherProxyInterface
    {
    public:

	AliHLTPublisherShmProxy( const char* name, AliUInt32_t shmSize, key_t pub2SubKey, key_t sub2PubKey );
	
	virtual ~AliHLTPublisherShmProxy();


	// Subscribe the given AliHLTSubscriberInterface to this 
	// subscription service.
	virtual int Subscribe( AliHLTSubscriberInterface& 
			       interface );

	// Unsubscribe a previously subscribed Subscriber-
	// Interface from this subscription service.
	virtual int Unsubscribe( AliHLTSubscriberInterface& 
				 interface );

	// Set the persistency status of the specified 
	// AliHLTSubscriberInterface:
	// Persistent (bool==true) for DAQ or L3; 
	// Transient (aka non-persistent) (bool==false)
	// for monitors. For monitors more event selection 
	// parameters can be specified (see below).
	virtual int SetPersistent( AliHLTSubscriberInterface& 
				   interface, bool );  

	// Specify that the AliHLTSubscriberInterface, is 
	// only notified about every modulo'th event.
	virtual int SetEventType( AliHLTSubscriberInterface& 
				  interface,  
				  AliUInt32_t modulo );

	// Specify that the AliHLTSubscriberInterface only 
	// wants to be notified about events whose trigger 
	// words appear in the passed list. The additional 
	// modulo parameter has the meaning as in the 
	// previous method and defaults to 1.
	virtual int SetEventType( AliHLTSubscriberInterface& 
				  interface, 
				  vector
				  <AliHLTEventTriggerStruct*>& 
				  triggerWords,  
				  AliUInt32_t modulo = 1 );

	// This method is used to specify to the publisher the 
	// minimum amount of time that an event can be kept by
	// a transient subscriber. The timeout is given in 
	// milliseconds
	virtual int SetTransientTimeout( AliHLTSubscriberInterface& interface, 
					 AliUInt32_t timeout_ms );

	// This method is used to specify to the publisher
	// that a subscriber wants to receive all the event done data
	// structures sent for events by other subscribers. 
	virtual int SetEventDoneDataSend( AliHLTSubscriberInterface& 
					  interface, bool );  

	// Method used by the subscriber to signal that it
	// finished working on the event with the specified 
	// event ID and that it can now be removed.
	virtual int EventDone( AliHLTSubscriberInterface& 
			       whichInterface, 
			       const AliHLTEventDoneData& 
			       eventDoneData, bool forceForward=false, bool forwarded=false );

	// Method used by the subscriber to signal that it
	// finished working on the events in the list 
	// of event IDs.
	virtual int EventDone( AliHLTSubscriberInterface& 
			       whichInterface, 
			       vector<AliHLTEventDoneData*>& 
			       eventDoneData, bool forceForward=false, bool forwarded=false );

	// This method tells the publishing service to start 
	// the notification about events and starts an 
	// event loop from which the calling (and given) 
	// AliHLTSubscriberInterface thread will return after 
	// an EventCanceled message has been received by it.
	virtual int StartPublishing( AliHLTSubscriberInterface& 
				     interface, bool sendOldEvents = false );

	// Method used to check the alive status of the 
	// publishing service. This message must be answered 
	// with a PingAck message to the calling (and given) 
	// AliHLTSubscriberInterface within less than a second.
	virtual int Ping( AliHLTSubscriberInterface& interface );

	// Method used by the AliHLTSubscriberInterface in 
	// response to a Ping message received from this 
	// publishing service. The PingAck message must be 
	// called  in at most one second after the Ping 
	// message has been received.
	virtual int PingAck( AliHLTSubscriberInterface& 
			     interface );

	void SetTimeout( AliUInt32_t timeout_usec );

	virtual void AbortPublishing();

	operator bool()
		{
		return ( (bool)fPubToSub && (bool)fSubToPub );
		}

    protected:


	void MakeMsg( AliHLTPubSubShmMsg* msg, AliHLTPubSubShmMsg::MsgType type,
		      AliUInt32_t paramCnt )
		{
		MakeMsg( msg, NULL, type, paramCnt, 0, 0, 0 );
		}

	void MakeMsg( AliHLTPubSubShmMsg* msg, const char* subscrName, 
		      AliHLTPubSubShmMsg::MsgType type,
		      AliUInt32_t paramCnt, key_t pub2SubKey, key_t sub2PubKey, AliUInt32_t shmSize );

	void MakeParam( AliHLTPubSubShmMsgParam* param, bool val );
	void MakeParam( AliHLTPubSubShmMsgParam* param, AliUInt32_t val );
	void MakeParam( AliHLTPubSubShmMsgParam* param, vector<AliHLTEventTriggerStruct*>& val );
	void MakeParam( AliHLTPubSubShmMsgParam* param, const AliHLTEventDoneData& );
	void MakeParam( AliHLTPubSubShmMsgParam* param, vector<AliHLTEventDoneData*>& val );

	virtual int MsgLoop( AliHLTSubscriberInterface& interface );

	virtual int HandleNewEventMsg( AliHLTSubscriberInterface& interface, AliHLTPubSubShmMsg* msg, 
				       AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 );
	virtual int HandleEventCanceledMsg( AliHLTSubscriberInterface& interface, AliHLTPubSubShmMsg* msg, 
					    AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 );
	virtual int HandleEventDoneDataMsg( AliHLTSubscriberInterface& interface, AliHLTPubSubShmMsg* msg, 
				       AliUInt8_t* msgP1, AliUInt8_t* msgP2, AliUInt32_t msgSize1 );

	// Communication direction is as seen from the procies.
	// e.g. pub-to-sub means publisher-proxy to subscriber-proxy.
	AliHLTShmCom fPubToSub;
	AliHLTShmCom fSubToPub;

        key_t fPubToSubKey;
        key_t fSubToPubKey;
	AliUInt32_t fShmSize;

	pthread_mutex_t fMutex;

	bool fQuitLoop;
	bool fLoopQuitted;

    private:
    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // ALIL3PUBLISHERSHMPROXY_HPP_
