/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef ALIL3PUBLISHERSHMCOM_HPP_
#define ALIL3PUBLISHERSHMCOM_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliHLTLog.hpp"
#include "AliHLTBlockHeader.h"
#include <sys/ipc.h>
#include <sys/shm.h>

#ifdef SHMCOM_DEBUG
#undef SHMCOM_DEBUG
#endif
//#define SHMCOM_DEBUG


#define CACHELINE_ALIGNED
// #ifdef CACHELINE_ALIGNED
// #undef CACHELINE_ALIGNED
// #endif
// #if ALIPLATFORM == LINUX
// #include <linux/cache.h>
// #define CACHELINE_ALIGNED ____cacheline_aligned
// #else
// #define CACHELINE_ALIGNED
// #endif

class AliHLTShmCom
    {
    public:
  
	AliHLTShmCom( key_t shmKey, AliUInt32_t size, bool create = true );
	~AliHLTShmCom();

	void SetTimeout( AliUInt32_t ms );
	AliUInt32_t GetTimeout()
		{
		return fTimeout;
		}

	void SetRetryTimeout( AliUInt32_t cnt );

	unsigned long GetSleepCount()
		{
		return fSleepCount;
		}

	void SetDebugLogLevel( AliHLTLog::TLogLevel loglevel )
		{
		fDbgLogLevel = loglevel;
		}

	int Write( AliUInt32_t size, AliUInt8_t* data, bool block=false );
	int CanWrite( AliUInt32_t size, AliUInt8_t*& p1, AliUInt8_t*& p2, AliUInt32_t& size1, bool block=false );
	int HaveWritten( AliUInt32_t size, AliUInt8_t* p1, AliUInt8_t* p2, AliUInt32_t size1 );

	int Read( AliUInt32_t size, AliUInt8_t* data, bool block=false );
	int Read( AliHLTBlockHeader*& pbh, AliUInt8_t*& p1, AliUInt8_t*& p2, AliUInt32_t& size1, bool block=false );
	int FreeRead( AliHLTBlockHeader* pbh, AliUInt8_t* p1, AliUInt8_t* p2, AliUInt32_t size1 );
	int CanRead( AliUInt32_t size, AliUInt8_t*& p1, AliUInt8_t*& p2, AliUInt32_t& size1, bool block=false )
		{
		return CanRead( false, size, p1, p2, size1, block );
		}
	int CanRead( bool peek, AliUInt32_t size, AliUInt8_t*& p1, AliUInt8_t*& p2, AliUInt32_t& size1, bool block=false );
	int HaveRead( AliUInt32_t size, AliUInt8_t* p1, AliUInt8_t* p2, AliUInt32_t size1 );

	operator bool();

	void Abort()
		{
		fQuit = true;
		}

    protected:

	AliHLTLog::TLogLevel fDbgLogLevel;

	key_t fShmKey;
	AliUInt32_t fShmSize;
	int fShmID;
	AliUInt8_t* fShmPtr;
	AliUInt32_t fTimeout;
	AliUInt32_t fRetryCount;
  
	struct TShmHeader
	    {
		CACHELINE_ALIGNED volatile AliUInt32_t fWriteNdx;
		CACHELINE_ALIGNED volatile AliUInt32_t fReadNdx;
		CACHELINE_ALIGNED volatile AliUInt32_t fFreeNdx;
		CACHELINE_ALIGNED volatile AliUInt32_t fSize;
	    };

	TShmHeader* fHeader;
	AliUInt8_t* fData;

	unsigned long fSleepCount;

#ifdef SHMCOM_DEBUG
	unsigned int fStateLimit;
	vector<TShmHeader> fStates;
#endif

	bool fQuit;

    };


#endif // ALIL3PUBLISHERSHMCOM_HPP_
