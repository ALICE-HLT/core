
This is the code for the Alice High Level Trigger (HLT) Framework.

The code is split up in several Modules, each of which has its own directory
under the top-level directory. The current modules are:

Base: This modules provides basic and utility classes, such as some special
      types, timer and thread classes, etc.

SC: (SystemControl) This modules provides the basic facilities to remotely 
    control and monitor processes.

PubSub: This modules provides the basic classes for the publisher-subscriber
        interface.

WorkerComp: This module provides some components doing some useful work (more
            or less) in an analysis chain. These components all interact via
            the publisher-subscriber interface.

DataFlowComp: This module provides components to shape and control the 
              data-flow in an analysis chain. (Scatterers, Gatherers,
              Mergers, Bridges). These components all interact via
              the publisher-subscriber interface.

IntegrTest-3/4: These modules contain data processing components used in
                two integration test with the Bergen group producing the
                analysis components.

Samples: Three sample programs for a data source, a data processor/analysis
         program, and a data sink.


Each modules will produce one library (shared or static or both) and a number
of binaries. The libraries can be found in each module's directory under
lib/platform, where platform is a short tag of the platform you are using,
your operating system and processor architecture (values provided by uname -m
and uname -s respectively). Binaries can be found in each module's directory 
under bin/platform.
