/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#if 1


#include "AliHLTTypes.h"
#include "AliHLTProcessingSubscriber.hpp"
#include "AliHLTControlledComponent.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTEventDoneData.h"
#include "AliHLTTriggerEventFilter.hpp"
#include "AliHLTEventAccounting.hpp"
#include <MLUCMutex.hpp>
#include <vector>
#include <list>



class TriggeredFilterSubscriber: public AliHLTProcessingSubscriber
    {
    public:

	TriggeredFilterSubscriber( const char* name, AliHLTEventDoneDataCommands eddCmd, bool sendEventDone, AliUInt32_t minBlockSize,
			     AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, int eventCountAlloc = -1 ):
	    AliHLTProcessingSubscriber( name, sendEventDone, minBlockSize,
					perEventFixedSize, perBlockFixedSize, sizeFactor, eventCountAlloc ),
	    fEDDCmd(eddCmd),
	    fEDDData( 8, eventCountAlloc ),
	    fEDDCache( eventCountAlloc, true, false ),
	    fEventBacklog( 10, 16, false )
		{
		fHighWaterMarkLatestExpiry = 0;
		}

	void SetTriggerEventFilter( AliHLTTriggerEventFilter* tef )
		{
		fFilter = tef;
		}



	virtual int ProcessEventDoneData( const AliHLTEventDoneData& 
					  eventDoneData, bool storeEDDIfEventNotFound )
		{
		vector<AliHLTSubEventDescriptor::BlockData> forwardBlocks;
		bool matchingCommandFound = false;
		pthread_mutex_lock( &fEventDataMutex );
		    {
		    // Check for expiry of high water marks
		    // Only remove ANY high water mark if the latest expiry time has passed
		    // This is done as it is only checked for the existence of at least one high water mark anyway
		    // and this way we do not have to iterate through the list to check for expired ones every time
		    struct timeval now;
		    gettimeofday( &now, NULL );
		    MLUCMutex::TLocker highWaterMarkLock( fHighWaterMarkMutex );
		    if ( !fHighWaterMarks.empty() 
			 && now.tv_sec>fHighWaterMarkLatestExpiry )
			fHighWaterMarks.clear();
		    }
		unsigned long ndx=~0UL;
		if ( MLUCVectorSearcher<EventData*,AliEventID_t>::FindElement( fEventData, &EventDataSearchFunc, eventDoneData.fEventID, ndx ) )
		    {
		    EventData* ed = fEventData.Get( ndx );
		    if ( !ed->fAnnounced )
			{
			fFilter->FilterEventDescriptorMatches( fEDDCmd, ed->fSEDD, &eventDoneData, forwardBlocks );
			AliHLTEventDoneDataProcessor eddp( &eventDoneData );
			AliHLTEventDoneDataProcessor::TStatus status;
			AliUInt32_t const * nextCmd;
			unsigned wordCount;
			status = eddp.NextCommand( nextCmd, wordCount );
			while ( status == AliHLTEventDoneDataProcessor::kOk )
			    {
			    if ( nextCmd[0] == kAliHLTEventDoneFlagMonitorEventCmd )
				{
				ed->fSEDD->fStatusFlags |= ALIHLTSUBEVENTDATADESCRIPTOR_MONITOR;
				//break; // Remove this, if this loop can process more commands!!!
				LOG( AliHLTLog::kDebug, "TriggeredFilterSubscriber::EventDoneData", "Setting monitor flag" )
				    << "Setting monitor flag for event 0x"
				    << AliHLTLog::kHex << eventDoneData.fEventID << " (" << AliHLTLog::kDec
				    << eventDoneData.fEventID << ")." << ENDLOG;
				}
			    else if ( nextCmd[0] == (AliUInt32_t)fEDDCmd )
				{
				matchingCommandFound=true;
				}
			    status = eddp.NextCommand( nextCmd, wordCount );
			    }
#if 0
			if ( fEDDCmd==kAliHLTEventDoneMonitorListCmd && !(ed->fSEDD->fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_MONITOR) )
			    {
			    forwardBlocks.clear();
			    }
#endif
			if ( fEDDCmd==kAliHLTEventDoneMonitorListCmd )
			    {
			    MLUCMutex::TLocker highWaterMarkLock( fHighWaterMarkMutex );
			    if ( !fHighWaterMarks.empty() )
				{
				// At least one high water mark currently active
				forwardBlocks.clear();
				ed->fSEDD->fStatusFlags &= ~ALIHLTSUBEVENTDATADESCRIPTOR_MONITOR;
				}
			    }
#if 0
			LOG( AliHLTLog::kImportant, "TriggeredFilterSubscriber::EventDoneData", "Output block count" )
			    << "Sending out event with " << AliHLTLog::kDec << forwardBlocks.size() << " blocks."
			    << ENDLOG;
#endif
			
			}
		    }
		else
		    {
		    LOG( AliHLTLog::kDebug, "TriggeredFilterSubscriber::EventDoneData", "Cannot find event" )
			<< "Unable to find event 0x"
			<< AliHLTLog::kHex << eventDoneData.fEventID << " (" << AliHLTLog::kDec
			<< eventDoneData.fEventID << ")." << ENDLOG;
		    if ( storeEDDIfEventNotFound )
			{
			bool found=false;
			    {
			    MLUCMutex::TLocker eventBacklogLock( fEventBacklogMutex );
			    unsigned long tmpNdx;
			    if ( fEventBacklog.FindElement( eventDoneData.fEventID, tmpNdx ) )
				 found=true;
			    }
			if ( !found )
			    {
			    MLUCMutex::TLocker fEDDLocker( fEDDMutex );
			    TEDDData eddd;
			    eddd.fEventID = eventDoneData.fEventID;
			    eddd.fEDD = reinterpret_cast<AliHLTEventDoneData*>( fEDDCache.Get( eventDoneData.fHeader.fLength ) );
			    if ( !eddd.fEDD )
				{
				LOG( AliHLTLog::kWarning, "TriggeredFilterSubscriber::EventDoneData", "Out of memory" )
				    << "Out of memory trying to allocate memory for storing event done data of " << AliHLTLog::kDec
				    << eventDoneData.fHeader.fLength << " bytes." << ENDLOG;
				}
			    else
				{
				memcpy( eddd.fEDD, &eventDoneData, eventDoneData.fHeader.fLength );
				fEDDData.Add( eddd, eventDoneData.fEventID );
				}
			    }
			}
		    }
		pthread_mutex_unlock( &fEventDataMutex );
		if ( matchingCommandFound )
		    AnnounceEvent( eventDoneData.fEventID, forwardBlocks );

		    {
		    MLUCMutex::TLocker eventBacklogLock( fEventBacklogMutex );
		    while ( fEventBacklog.GetCnt()>=fEventBacklog.GetSize() )
			fEventBacklog.RemoveFirst();
		    fEventBacklog.Add( eventDoneData.fEventID, eventDoneData.fEventID );
		    }
		return 0;
		}



	virtual bool ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, 
				   const AliHLTSubEventDataDescriptor* /*sedd*/, const AliHLTEventTriggerStruct*,
				   AliUInt8_t*, AliUInt32_t&, vector<AliHLTSubEventDescriptor::BlockData>&, 
				   AliHLTEventDoneData*& )
		{
		if ( !fFilter )
		    {
		    LOG( AliHLTLog::kWarning, "TriggeredFilterSubscriber::ProcessEvent", "No trigger event filter" )
			<< "No trigger event filter object configured. Forwarding event 0x"
			<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ") totally empty." << ENDLOG;
		    return true;
		    }

		if ( eventID.fType == kAliEventTypeData )
		    {
		    MLUCMutex::TLocker fEDDLocker( fEDDMutex );
		    unsigned long ndx;
		    if ( fEDDData.GetSize()>0 && fEDDData.FindElement( eventID, ndx ) )
			{
			LOG( AliHLTLog::kDebug, "TriggeredFilterSubscriber::ProcessEvent", "Using stored EventDoneData" )
			    << "Using already received and stored event done data for event 0x"
			    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			    << eventID << ")" << ENDLOG;
			TEDDData* edddp;
			edddp = fEDDData.GetPtr( ndx );
			AliHLTEventDoneData* edd = edddp->fEDD;
			fEDDData.Remove( ndx );
			fEDDLocker.Unlock();
			if ( DOLOG(AliHLTLog::kDebug) )
			    {
			    MLUCString output;
			    char tmp[1024];
			    for ( unsigned long long ii=0; ii<edd->fDataWordCount; ii++ )
				{
				if ( output!="" )
				    output += " ";
				snprintf( tmp, 1024, "0x%08X", edd->fDataWords[ii] );
				output += tmp;
				}
			    LOG( AliHLTLog::kDebug, "TriggeredFilterSubscriber::ProcessEvent", "Stored EventDoneData" )
				<< "Stored EventDoneData: " << output.c_str() << ENDLOG;
			    }
			ProcessEventDoneData( *edd, false );
			fEDDLocker.Lock();
			fEDDCache.Release( reinterpret_cast<uint8*>( edd ) );
			}
		    }
		else
		    {
		    vector<AliHLTSubEventDescriptor::BlockData> forwardBlocks;
		    forwardBlocks.reserve( blockCnt );
		    for ( unsigned long ii=0; ii<blockCnt; ++ii )
			{
			forwardBlocks.push_back( blocks[ii] );
			}
		    AnnounceEvent( eventID, forwardBlocks );
		    }
		return true;
		}


	virtual int EventDoneData( AliHLTPublisherInterface& pubInt, 
				   const AliHLTEventDoneData& 
				   eventDoneData )
		{
		if ( fEventAccounting )
		    fEventAccounting->EventDoneData( pubInt.GetName(), eventDoneData.fEventID, eventDoneData.fDataWordCount );
		if ( DOLOG(AliHLTLog::kDebug) )
		    {
		    MLUCString output;
		    char tmp[1024];
		    for ( unsigned long long ii=0; ii<eventDoneData.fDataWordCount; ii++ )
			{
			if ( output!="" )
			    output += " ";
			snprintf( tmp, 1024, "0x%08X", eventDoneData.fDataWords[ii] );
			output += tmp;
			}
		    LOG( AliHLTLog::kDebug, "TriggeredFilterSubscriber::EventDoneData", "EventDoneData received" )
			<< "EventDoneData received: " << output.c_str() << ENDLOG;
		    }
		if ( eventDoneData.fEventID.fType == kAliEventTypeData )
		    {
		    if ( !fFilter )
			{
			LOG( AliHLTLog::kWarning, "TriggeredFilterSubscriber::EventDoneData", "No trigger event filter" )
			    << "No trigger event filter object configured. Forwarding event 0x"
			    << AliHLTLog::kHex << eventDoneData.fEventID << " (" << AliHLTLog::kDec
			    << eventDoneData.fEventID << ") totally empty." << ENDLOG;
			vector<AliHLTSubEventDescriptor::BlockData> dummyBlocks;
			AnnounceEvent( eventDoneData.fEventID, dummyBlocks );
			return 0;
			}
		    return ProcessEventDoneData( eventDoneData, true );
		    }
		return 0;
		}

	
	virtual void EventDoneDataReceived( const char* subscriberName, AliEventID_t eventID,
					    const AliHLTEventDoneData* edd, bool forceForward, bool forwarded )
		{
		if ( DOLOG(AliHLTLog::kDebug) && edd->fDataWordCount>0 )
		    {
		    MLUCString output;
		    char tmp[1024];
		    for ( unsigned long long ii=0; ii<edd->fDataWordCount; ii++ )
			{
			if ( output!="" )
			    output += " ";
			snprintf( tmp, 1024, "0x%08X", edd->fDataWords[ii] );
			output += tmp;
			}
		    LOG( AliHLTLog::kDebug, "TriggeredFilter::EventDoneDataReceived", "EventDoneData received" )
			<< "EventDoneData received for event 0x" << AliHLTLog::kHex << edd->fEventID << " ("
			<< AliHLTLog::kDec << edd->fEventID << "): " << output.c_str() << ENDLOG;
		    }
		AliHLTProcessingSubscriber::EventDoneDataReceived( subscriberName, eventID, edd, forceForward, forwarded );
		if ( fEDDCmd!=kAliHLTEventDoneMonitorListCmd )
		    return;

		    {
		    // Check for expiry of high water marks
		    // Only remove ANY high water mark if the latest expiry time has passed
		    // This is done as it is only checked for the existence of at least one high water mark anyway
		    // and this way we do not have to iterate through the list to check for expired ones every time
		    struct timeval now;
		    gettimeofday( &now, NULL );
		    MLUCMutex::TLocker highWaterMarkLock( fHighWaterMarkMutex );
		    if ( !fHighWaterMarks.empty() 
			 && now.tv_sec>fHighWaterMarkLatestExpiry )
			fHighWaterMarks.clear();
		    }

		AliHLTEventDoneDataProcessor eddp( edd );
		AliHLTEventDoneDataProcessor::TStatus status;
		AliUInt32_t const * nextCmd;
		unsigned wordCount;
		status = eddp.NextCommand( nextCmd, wordCount );
		while ( status == AliHLTEventDoneDataProcessor::kOk )
		    {
		    if ( (nextCmd[0] == kAliHLTEventDoneBackPressureHighWaterMark || nextCmd[0] == kAliHLTEventDoneBackPressureLowWaterMark) )
			{
			// Take back pressure into account
			AliUInt64_t id = (((AliUInt64_t)nextCmd[1]) << 32) | ((AliUInt64_t)nextCmd[2]);
			if ( nextCmd[0] == kAliHLTEventDoneBackPressureHighWaterMark )
			    {
			    LOG( AliHLTLog::kDebug, "TriggeredFilterSubscriber::EventDoneDataReceived", "High Water Mark" )
				<< "Received back pressure high water mark ID 0x" 
				<< AliHLTLog::kHex << id
				<< "." << ENDLOG;
			    struct timeval now;
			    gettimeofday( &now, NULL );
			    MLUCMutex::TLocker highWaterMarkLock( fHighWaterMarkMutex );
			    THighWaterMarkData hwmd;
			    hwmd.fHighWaterMarkID = id;
			    hwmd.fExpiry = now.tv_sec + gkAliHLTEventDoneBackPressureHighWaterMarkExpiryTime_s;
			    fHighWaterMarks.push_back( hwmd );
			    fHighWaterMarkLatestExpiry = hwmd.fExpiry;
			    }
			if ( nextCmd[0] == kAliHLTEventDoneBackPressureLowWaterMark )
			    {
			    LOG( AliHLTLog::kDebug, "TriggeredFilterSubscriber::EventDoneDataReceived", "Low Water Mark" )
				<< "Received back pressure low water mark ID 0x" 
				<< AliHLTLog::kHex << id
				<< "." << ENDLOG;
			    bool renewLatestExpiry = false;
			    std::list<THighWaterMarkData>::iterator iter, end;
			    MLUCMutex::TLocker highWaterMarkLock( fHighWaterMarkMutex );
			    iter = fHighWaterMarks.begin();
			    end = fHighWaterMarks.end();
			    while ( iter != end )
				{
				if ( iter->fHighWaterMarkID == id )
				    {
				    if ( iter->fExpiry==fHighWaterMarkLatestExpiry )
					renewLatestExpiry = true;
				    fHighWaterMarks.erase( iter );
				    break;
				    }
				++iter;
				}
			    if ( renewLatestExpiry )
				{
				fHighWaterMarkLatestExpiry = 0;
				iter = fHighWaterMarks.begin();
				end = fHighWaterMarks.end();
				while ( iter != end )
				    {
				    if ( iter->fExpiry>fHighWaterMarkLatestExpiry )
					fHighWaterMarkLatestExpiry = iter->fExpiry;
				    ++iter;
				    }
				}
			    }
			}
		    status = eddp.NextCommand( nextCmd, wordCount );
		    }
		}


	void PURGEALLEVENTS()
	    {
	    AliHLTProcessingSubscriber::PURGEALLEVENTS();
	    MLUCMutex::TLocker fEDDLocker( fEDDMutex );
	    while ( fEDDData.GetCnt()>0 )
		{
		fEDDCache.Release( reinterpret_cast<uint8*>( fEDDData.GetFirstPtr()->fEDD ) );
		fEDDData.RemoveFirst();
		}
	    }

	
    protected:

	struct TEDDData
	    {
		AliEventID_t fEventID;
		AliHLTEventDoneData* fEDD;
	    };

	AliHLTEventDoneDataCommands fEDDCmd;

	AliHLTTriggerEventFilter* fFilter;

	MLUCMutex fEDDMutex;
	
	MLUCIndexedVector<TEDDData,AliEventID_t> fEDDData;
	MLUCDynamicAllocCache fEDDCache;

	struct THighWaterMarkData
	    {
		AliUInt64_t fHighWaterMarkID;
		time_t fExpiry;
	    };

	MLUCMutex fHighWaterMarkMutex; // If locked together with fEventDataMutex, fEventDataMutex MUST be locked FIRST
	std::list<THighWaterMarkData> fHighWaterMarks;
	time_t fHighWaterMarkLatestExpiry;

	MLUCMutex fEventBacklogMutex;
	MLUCIndexedVector<AliEventID_t,AliEventID_t> fEventBacklog;
	

    private:
    };


class TriggeredFilterComponent: public AliHLTControlledAsyncProcessorComponent
    {
    public:

	TriggeredFilterComponent( const char* name, int argc, char** argv );
	virtual ~TriggeredFilterComponent() {};


    protected:

	//virtual AliHLTProcessingSubscriber* CreateSubscriber();
	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );

	virtual AliHLTProcessingSubscriber* CreateSubscriber();
	virtual bool CreateParts();
	virtual void DestroyParts();

	virtual bool SetupComponents();
	virtual void ResetConfiguration();


	bool fReadoutOrMonitoring;
	AliHLTTriggerEventFilter* fFilter;

	typedef AliHLTControlledAsyncProcessorComponent TParent;

    private:
    };


TriggeredFilterComponent::TriggeredFilterComponent( const char* name, int argc, char** argv ):
    TParent( name, argc, argv ),
    fReadoutOrMonitoring(true),
    fFilter(NULL)
    {
    fRequestPublisherEventDoneData = true;
    }

void TriggeredFilterComponent::ResetConfiguration()
    {
    fReadoutOrMonitoring = true;
    fFilter = NULL;
    TParent::ResetConfiguration();
    fRequestPublisherEventDoneData = true; // Overwrite parent setting...
    }


const char* TriggeredFilterComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    if ( !strcmp( argv[i], "-monitorfilter" ) )
	{
	fReadoutOrMonitoring = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-readoutfilter" ) )
	{
	fReadoutOrMonitoring = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventsexp2" ) )
	{
	char* cpErr=NULL;
	if ( argc <= i+1 )
	    {
	    return "Missing event count (power of 2) specifier.";
	    }
	fMaxPreEventCountExp2 = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting event count (power of 2) specifier..";
	    }
	fMaxPreEventCount = 1 << fMaxPreEventCountExp2;
	i += 2;
	return NULL;
	}
    return TParent::ParseCmdLine( argc, argv, i, errorArg );
    }

void TriggeredFilterComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    TParent::PrintUsage( errorStr, errorArg, errorParam );
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-monitorfilter: Determine that filtering is done for monitoring events. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	<< "-readoutfilter: Determine that filtering is done for readout of events. (Optional, default)" << ENDLOG;
    }


AliHLTProcessingSubscriber* TriggeredFilterComponent::CreateSubscriber()
    {
    return new TriggeredFilterSubscriber( fSubscriberID.c_str(), 
					  fReadoutOrMonitoring ? kAliHLTEventDoneReadoutListCmd : kAliHLTEventDoneMonitorListCmd,
					  fSendEventDone, fMinBlockSize, 
					  fPerEventFixedSize, fPerBlockFixedSize, fSizeFactor, 
					  fMaxPreEventCountExp2 );
    }

bool TriggeredFilterComponent::CreateParts()
    {
    if ( !TParent::CreateParts() )
	return false;
    fFilter = new AliHLTTriggerEventFilter;
    if ( !fFilter )
	{
	LOG( AliHLTLog::kError, "TriggeredFilterComponent::CreateParts", "Cannot create trigger event filter object" )
	    << "Unable to create trigger event filter object." << ENDLOG;
	return false;
	}
    return true;
    }

void TriggeredFilterComponent::DestroyParts()
    {
    if ( fFilter )
	delete fFilter;
    fFilter = NULL;
    TParent::DestroyParts();
    }

bool TriggeredFilterComponent::SetupComponents()
    {
    if ( !TParent::SetupComponents() )
	return false;
    if ( fAliceHLT )
	fFilter->SetDataSpecificationMatchFunction( AliHLTTriggerEventFilter::AliceDataSpecificationMatcher );
    else
	fFilter->SetDataSpecificationMatchFunction( AliHLTTriggerEventFilter::DataSpecificationComparer );
    reinterpret_cast<TriggeredFilterSubscriber*>( fSubscriber )->SetTriggerEventFilter( fFilter );
    return true;
    }


int main( int argc, char** argv )
    {
    TriggeredFilterComponent procComp( "TriggeredFilter", argc, argv );
    procComp.Run();
    return 0;
    }


#else

Old code, not remotely controllable by TaskManager et al.

#include "AliHLTSubscriberInterface.hpp"
#include "AliHLTSamplePublisher.hpp"
#include "AliHLTTriggerDecision.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTEventDoneData.hpp"
#include "AliHLTEventTriggerStruct.h"
#include "AliHLTTriggerEventFilter.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTThread.hpp"
#include "AliHLTPublisherPipeProxy.hpp"
#include "MLUCVector.hpp"
#include "MLUCDynamicAllocCache.hpp"
#include <pthread.h>
#include <syslog.h>

class TriggeredFilterSubscriber;

class TriggeredFilterPublisher: public AliHLTSamplePublisher
    {
    public:

	TriggeredFilterPublisher( const char* name, TriggeredFilterSubscriber* sub, int eventSlotsPowerOfTwo = -1 ):
	    AliHLTSamplePublisher( name, eventSlotsPowerOfTwo ),
	    fSubscriber( sub ),
	    fAnnounceThread( this, &AliHLTSamplePublisher::AcceptSubscriptionsVoid )
		{
		SetSubscriptionLoop( &PublisherPipeSubscriptionInputLoop );
		fAnnouncing = false;
		}
	virtual ~TriggeredFilterPublisher()
		{
		}

	void StartAnnouncing()
		{
		fAnnouncing = true;
		fAnnounceThread.Start();
		fAnnouncing = false;
		}
	void StopAnnouncing()
		{
		QuitPipeSubscriptionLoop( *this );
		struct timeval start, now;
		unsigned long long deltaT = 0;
		const unsigned long long timeLimit = 5000000;
		gettimeofday( &start, NULL );
		while ( fAnnouncing && deltaT<timeLimit )
		    {
		    usleep( 10000 );
		    gettimeofday( &now, NULL );
		    deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
		    }
		if ( fAnnouncing )
		    fAnnounceThread.Abort();
		else
		    fAnnounceThread.Join();
		}

    protected:

	virtual void CanceledEvent( AliEventID_t event, vector<AliHLTEventDoneData*>& eventDoneData );

	virtual bool GetAnnouncedEventData( AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
				      AliHLTEventTriggerStruct*& trigger );

	TriggeredFilterSubscriber* fSubscriber;

	AliHLTObjectThread<TriggeredFilterPublisher> fAnnounceThread;
	bool fAnnouncing;

    private:
    };

class TriggeredFilterSubscriber: public AliHLTSubscriberInterface
    {
    public:

	TriggeredFilterSubscriber( const char* name, int eventSlotsPowerOfTwo = -1 );
	virtual ~TriggeredFilterSubscriber();

	void SetPublisher( TriggeredFilterPublisher* pub )
		{
		fPublisher = pub;
		fDescriptors = NULL;
		}

	void SetDescriptorHandler( AliHLTDescriptorHandler* descr )
		{
		fDescriptors = descr;
		}

	void AnnounceUntriggeredEventsAsEmpty( bool ann )
		{
		fAnnounceUntriggeredEventsAsEmpty = ann;
		}

	void SetAliceMode( bool mode )
		{
		if ( mode )
		    fTriggerEventFilter.SetDataSpecificationMatchFunction( &(AliHLTTriggerEventFilter::AliceDataSpecificationMatcher) );
		else
		    fTriggerEventFilter.SetDataSpecificationMatchFunction( &(AliHLTTriggerEventFilter::DataSpecificationComparer) );
		}
	
	virtual int NewEvent( AliHLTPublisherInterface& publisher, 
			      AliEventID_t eventID, 
			      const AliHLTSubEventDataDescriptor& sbevent,
			      const AliHLTEventTriggerStruct& trigger );

	virtual int EventCanceled( AliHLTPublisherInterface& publisher, 
				   AliEventID_t eventID );

	virtual int EventDoneData( AliHLTPublisherInterface& publisher, 
				   const AliHLTEventDoneData& 
				   eventDoneData );

	virtual int SubscriptionCanceled( AliHLTPublisherInterface& publisher );

	virtual int ReleaseEventsRequest( AliHLTPublisherInterface& publisher ); 

	virtual int Ping( AliHLTPublisherInterface& publisher );

	virtual int PingAck( AliHLTPublisherInterface& publisher );

	void SetDataType( AliHLTEventDataType dataType )
		{
		fSimSEDD->fDataBlocks[0].fDataType = dataType;
		fDataTypeSet = true;
		}

	void SetDataOrigin( AliHLTEventDataOrigin dataOrigin )
		{
		fSimSEDD->fDataBlocks[0].fDataOrigin = dataOrigin;
		fDataOriginSet = true;
		}

	void SetDataSpecification( AliUInt32_t dataSpec )
		{
		fSimSEDD->fDataBlocks[0].fDataSpecification = dataSpec;
		fDataSpecificationSet = true;
		}



	void CanceledEvent( AliEventID_t event, vector<AliHLTEventDoneData*>& eventDoneData );

	virtual bool GetAnnouncedEventData( AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
				      AliHLTEventTriggerStruct*& trigger );

    protected:

	TriggeredFilterPublisher* fPublisher;

	AliHLTTriggerEventFilter fTriggerEventFilter;

	struct EventData
	    {
		EventData()
			{
			fSEDD = NULL;
			fETS = NULL;
			fPublisher = NULL;
			}

		AliHLTSubEventDataDescriptor* fSEDD;
		AliHLTEventTriggerStruct* fETS;
		AliHLTPublisherInterface* fPublisher;
	    };

	pthread_mutex_t fEventMutex;
	MLUCVector<EventData> fReceivedEvents;
	MLUCVector<EventData> fAnnouncedEvents;

	pthread_mutex_t fDescriptorMutex;
	AliHLTDescriptorHandler* fDescriptors;

	MLUCDynamicAllocCache fETSCache;

	bool fAnnounceUntriggeredEventsAsEmpty;
	AliHLTSubEventDataDescriptor fEmptySEDD;
	AliHLTEventTriggerStruct fEmptyETS;

	AliHLTEventDoneData fEDD;


	static bool EventSearchFunc( const EventData& ed, const AliEventID_t& searchData )
		{
		return ed.fSEDD && ed.fSEDD->fEventID == searchData;
		}

	bool fDataTypeSet;
	bool fDataOriginSet;
	bool fDataSpecificationSet;

	AliUInt8_t fSimSEDDData[ sizeof(AliHLTSubEventDataDescriptor)+
				 sizeof(AliHLTSubEventDataBlockDescriptor) ];
	AliHLTSubEventDataDescriptor* fSimSEDD;

    private:
    };


void TriggeredFilterPublisher::CanceledEvent( AliEventID_t event, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    if ( fSubscriber )
	fSubscriber->CanceledEvent( event, eventDoneData );
    }

bool TriggeredFilterPublisher::GetAnnouncedEventData( AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
						      AliHLTEventTriggerStruct*& trigger )
    {
    if ( fSubscriber )
	return fSubscriber->GetAnnouncedEventData( eventID, sedd, trigger );
    else
	return false;
    }


TriggeredFilterSubscriber::TriggeredFilterSubscriber( const char* name, int eventSlotsPowerOfTwo ):
    AliHLTSubscriberInterface( name ),
    fReceivedEvents( eventSlotsPowerOfTwo ),
    fAnnouncedEvents( eventSlotsPowerOfTwo ),
    fETSCache( (eventSlotsPowerOfTwo<=0) ? 1 : eventSlotsPowerOfTwo, true, false ),
    fSimSEDD( (AliHLTSubEventDataDescriptor*)fSimSEDDData )
    {
    pthread_mutex_init( &fEventMutex, NULL );
    pthread_mutex_init( &fDescriptorMutex, NULL );

    fAnnounceUntriggeredEventsAsEmpty = false;

    fTriggerEventFilter.SetDataSpecificationMatchFunction( &(AliHLTTriggerEventFilter::DataSpecificationComparer) );

    AliHLTMakeEventDoneData( &fEDD, AliEventID_t() );

    fEmptySEDD.fHeader.fLength = sizeof( AliHLTSubEventDataDescriptor );
    fEmptySEDD.fDataBlockCount = 0;
    fEmptySEDD.fHeader.fType.fID = ALIL3SUBEVENTDATADESCRIPTOR_TYPE;
    fEmptySEDD.fHeader.fSubType.fID = 0;
    fEmptySEDD.fHeader.fVersion = 5;
    fEmptySEDD.fEventBirth_s = 0;
    fEmptySEDD.fEventBirth_us = 0;
    fEmptySEDD.fOldestEventBirth_s = 0;
    fEmptySEDD.fEventID = AliEventID_t();
    fEmptySEDD.fDataType.fID = UNKNOWN_DATAID;

    fEmptyETS.fHeader.fLength = sizeof(AliHLTEventTriggerStruct);
    fEmptyETS.fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
    fEmptyETS.fHeader.fSubType.fID = 0;
    fEmptyETS.fHeader.fVersion = 1;
    fEmptyETS.fDataWordCount = 0;

    fDataTypeSet = false;
    fDataOriginSet = false;
    fDataSpecificationSet = false;

    fSimSEDD->fHeader.fLength = sizeof( AliHLTSubEventDataDescriptor )+
			       sizeof(AliHLTSubEventDataBlockDescriptor);
    fSimSEDD->fDataBlockCount = 1;
    fSimSEDD->fHeader.fType.fID = ALIL3SUBEVENTDATADESCRIPTOR_TYPE;
    fSimSEDD->fHeader.fSubType.fID = 0;
    fSimSEDD->fHeader.fVersion = 4;
    fSimSEDD->fEventBirth_s = 0;
    fSimSEDD->fEventBirth_us = 0;
    fSimSEDD->fOldestEventBirth_s = 0;
    fSimSEDD->fEventID = AliEventID_t();
    fSimSEDD->fDataType.fID = COMPOSITE_DATAID;
    fSimSEDD->fDataBlocks[0].fDataType.fID = ALL_DATAID;
    fSimSEDD->fDataBlocks[0].fDataOrigin.fID = ALL_DATAORIGIN;
    fSimSEDD->fDataBlocks[0].fDataSpecification = ~(AliUInt32_t)0;
    for ( int i = 0; i < kAliHLTSEDBDAttributeCount; i++ )
	fSimSEDD->fDataBlocks[0].fAttributes[i] = kAliHLTUnknownAttribute;
    }

TriggeredFilterSubscriber::~TriggeredFilterSubscriber()
    {
    pthread_mutex_destroy( &fEventMutex );
    pthread_mutex_destroy( &fDescriptorMutex );
    }

int TriggeredFilterSubscriber::NewEvent( AliHLTPublisherInterface& publisher, 
			      AliEventID_t eventID, 
			      const AliHLTSubEventDataDescriptor& sbevent,
			      const AliHLTEventTriggerStruct& trigger )
    {
    if ( !fPublisher )
	{
	LOG( AliHLTLog::kError, "TriggeredFilterSubscriber::NewEvent", "No Publisher" )
	    << "No publisher specified. Unable to forward event." << ENDLOG;
	fEDD.fEventID = eventID;
	publisher.EventDone( *this, fEDD );
	return 0;
	}
    int ret;
    EventData ed;
    ed.fPublisher = &publisher;

    ret = pthread_mutex_lock( &fDescriptorMutex );
    if ( ret )
	{
	}
    if ( !fDescriptors )
	{
	LOG( AliHLTLog::kError, "TriggeredFilterSubscriber::NewEvent", "No Descriptor Handler" )
	    << "No descriptor handler specified. Unable to store and forward event." << ENDLOG;
	fEDD.fEventID = eventID;
	publisher.EventDone( *this, fEDD );
	return 0;
	}
    ed.fSEDD = fDescriptors->CloneEventDescriptor( sbevent );
    if ( !ed.fSEDD )
	{
	LOG( AliHLTLog::kError, "TriggeredFilterSubscriber::NewEvent", "No Descriptor" )
	    << "Unable to obtain event descriptor for event 0x" << AliHLTLog::kHex << eventID
	    << " (" << AliHLTLog::kDec << eventID << ") with " << sbevent.fDataBlockCount
	    << " data blocks from descriptor. Unable to store and forward event." << ENDLOG;
	fEDD.fEventID = eventID;
	publisher.EventDone( *this, fEDD );
	return 0;
	}
    ret = pthread_mutex_unlock( &fDescriptorMutex );
    if ( ret )
	{
	}

    if ( trigger.fDataWordCount>0 )
	{
	//ed.fETS = (AliHLTEventTriggerStruct*)new AliUInt8_t[ trigger.fHeader.fLength ];
	ed.fETS = (AliHLTEventTriggerStruct*)fETSCache.Get( trigger.fHeader.fLength );
	if ( !ed.fETS )
	    {
	    LOG( AliHLTLog::kError, "TriggeredFilterSubscriber::NewEvent", "Out Of Memory" )
		<< "Out of memory trying to allocate event trigger struct for event 0x" 
		<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID 
		<< ") of " << trigger.fHeader.fLength << " bytes." << ENDLOG;
	    }
	else
	    memcpy( ed.fETS, &trigger, trigger.fHeader.fLength );
	}
    else
	ed.fETS = NULL;

    ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	}
    
    fReceivedEvents.Add( ed );

    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	}
    
    return 0;
    }

int TriggeredFilterSubscriber::EventCanceled( AliHLTPublisherInterface&, 
				   AliEventID_t eventID )
    {
    int ret;
    unsigned long ndx;
    bool found = false;
    AliHLTEventTriggerStruct* ets=NULL;
    ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	}

    LOG( AliHLTLog::kInformational, "TriggeredFilterSubscriber::EventCanceled", "Event Canceled" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << ") canceled." << ENDLOG;

    if ( MLUCVectorSearcher<EventData,AliEventID_t>::FindElement( fReceivedEvents, &EventSearchFunc, eventID, ndx ) )
	{
	// Just remove event from list and free data.
	ets = fReceivedEvents.GetPtr( ndx )->fETS;
	fReceivedEvents.Remove( ndx );
	found = true;
	}
    else if ( MLUCVectorSearcher<EventData,AliEventID_t>::FindElement( fAnnouncedEvents, &EventSearchFunc, eventID, ndx ) )
	{
	// Remove event from list and abort already published/announced event.
	ets = fAnnouncedEvents.GetPtr( ndx )->fETS;
	fAnnouncedEvents.Remove( ndx );
	found = true;
	if ( fPublisher )
	    fPublisher->AbortEvent( eventID, true );
	}
    else
	{
	LOG( AliHLTLog::kWarning, "TriggeredFilterSubscriber::EventCanceled", "Canceled Event Not Found" )
	    << "Canceled event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") could not be found in lists." << ENDLOG;
	}
    

    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	}
   
    if ( found )
	{
	ret = pthread_mutex_lock( &fDescriptorMutex );
	if ( ret )
	    {
	    }
	if ( fDescriptors )
	    fDescriptors->ReleaseEventDescriptor( eventID );
	ret = pthread_mutex_unlock( &fDescriptorMutex );
	if ( ret )
	    {
	    }
	if ( ets )
	    fETSCache.Release( (uint8*)ets );   //delete [] (AliUInt8_t*)ets;
	}
    return 0;
    }

int TriggeredFilterSubscriber::EventDoneData( AliHLTPublisherInterface& publisher, 
				   const AliHLTEventDoneData& 
				   eventDoneData )
    {
    unsigned long ndx, n;
    int ret;
    AliHLTSubEventDataDescriptor* sedd = NULL;
    AliHLTEventTriggerStruct* ets = NULL;
    const AliHLTTriggerDecisionBlock* tdb;
    EventData ed;
    bool found = false;

    if ( !AliHLTHasValidTriggerDecisionBlocks( &eventDoneData ) )
	{
	LOG( AliHLTLog::kWarning, "TriggeredFilterSubscriber::EventDoneData", "Invalid EventDoneData" )
	    << "Event done data for event 0x" << AliHLTLog::kHex << eventDoneData.fEventID << " (" << AliHLTLog::kDec
	    << eventDoneData.fEventID << ") does not contain valid trigger decision data." << ENDLOG;
	return 0;
	}
    n = AliHLTGetTriggerDecisionBlockCount( &eventDoneData );
    LOG( AliHLTLog::kInformational, "TriggeredFilterSubscriber::EventDoneData", "Trigger Decision" )
	<< AliHLTLog::kDec << n << " Trigger Decision Blocks Received." << ENDLOG;
    for ( ndx = 0; ndx < n; ndx++ )
	{
	tdb = AliHLTGetTriggerDecisionBlock( &eventDoneData, ndx );
	if ( tdb )
	    AliHLTLogTriggerDecisionBlock( eventDoneData.fEventID, tdb, AliHLTLog::kInformational, "TriggeredFilterSubscriber::EventDoneData", "Trigger Decision" );
	else
	    LOG( AliHLTLog::kInformational, "TriggeredFilterSubscriber::EventDoneData", "Trigger Decision" )
		<< "Trigger Decision Block " << AliHLTLog::kDec << ndx << " not available." << ENDLOG;
	}
	    
    ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	}

    if ( fPublisher && MLUCVectorSearcher<EventData,AliEventID_t>::FindElement( fReceivedEvents, &EventSearchFunc, eventDoneData.fEventID, ndx ) )
	{
	// Just remove event from list
	//fReceivedEvents.Remove( ndx );
	ed = fReceivedEvents.Get( ndx );
	sedd = ed.fSEDD;
	ets = ed.fETS;
	bool sim = false;
	if ( fDataTypeSet || fDataOriginSet || fDataSpecificationSet )
	    {
	    sedd = fSimSEDD;
	    fSimSEDD->fEventID = sedd->fEventID;
	    sim = true;
	    }
	if ( sedd )
	    {
	    found = fTriggerEventFilter.FilterEventDescriptorMatches( sedd, &eventDoneData );
	    }
	if ( sim )
	    {
	    fSimSEDD->fHeader.fLength = sizeof( AliHLTSubEventDataDescriptor )+
				       sizeof(AliHLTSubEventDataBlockDescriptor);
	    fSimSEDD->fDataBlockCount = 1;
	    fSimSEDD->fEventID = AliEventID_t();
	    sedd = ed.fSEDD;
	    }
	if ( found )
	    {
	    if ( !ets )
		ets = &fEmptyETS;
	    fPublisher->AnnounceEvent( sedd->fEventID, *sedd, *ets );
	    }
	else if ( fAnnounceUntriggeredEventsAsEmpty )
	    {
	    fEmptySEDD.fEventID = sedd->fEventID;
	    fEmptySEDD.fEventBirth_s = sedd->fEventBirth_s;
	    fEmptySEDD.fEventBirth_us = sedd->fEventBirth_us;
	    fEmptySEDD.fOldestEventBirth_s = sedd->fOldestEventBirth_s;
	    fPublisher->AnnounceEvent( sedd->fEventID, fEmptySEDD, fEmptyETS );
	    found = true;
	    }
	else
	    fReceivedEvents.Remove( ndx );
	}
    if ( found )
	{
	fAnnouncedEvents.Add( ed );
	fReceivedEvents.Remove( ndx );
	}
    else
	{
	fEDD.fEventID = eventDoneData.fEventID;
	publisher.EventDone( *this, fEDD );
	}
    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	}

    if ( !found )
	{
	if ( sedd )
	    {
	    ret = pthread_mutex_lock( &fDescriptorMutex );
	    if ( ret )
		{
		}
	    if ( fDescriptors )
		fDescriptors->ReleaseEventDescriptor( eventDoneData.fEventID );
	    ret = pthread_mutex_unlock( &fDescriptorMutex );
	    if ( ret )
		{
		}
	    }
	if ( ets )
	    fETSCache.Release( (uint8*)ets );    //delete [] (AliUInt8_t*)ets;
	}
    return 0;
    }

int TriggeredFilterSubscriber::SubscriptionCanceled( AliHLTPublisherInterface& )
    {
    return 0;
    }

int TriggeredFilterSubscriber::ReleaseEventsRequest( AliHLTPublisherInterface& )
    {
    if ( fPublisher )
	fPublisher->RequestEventRelease();
    return 0;
    }

int TriggeredFilterSubscriber::Ping( AliHLTPublisherInterface& publisher )
    {
    publisher.PingAck( *this );
    return 0;
    }

int TriggeredFilterSubscriber::PingAck( AliHLTPublisherInterface& )
    {
    return 0;
    }


void TriggeredFilterSubscriber::CanceledEvent( AliEventID_t eventID, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    int ret;
    EventData ed;
    bool found = false;
    unsigned long ndx;

    ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	}
    if ( MLUCVectorSearcher<EventData,AliEventID_t>::FindElement( fAnnouncedEvents, &EventSearchFunc, eventID, ndx ) )
	{
	found = true;
	ed = fAnnouncedEvents.Get( ndx );
	fAnnouncedEvents.Remove( ndx );
	}
    else if ( MLUCVectorSearcher<EventData,AliEventID_t>::FindElement( fReceivedEvents, &EventSearchFunc, eventID, ndx ) )
	{
	found = true;
	ed = fReceivedEvents.Get( ndx );
	fReceivedEvents.Remove( ndx );
	LOG( AliHLTLog::kWarning, "TriggeredFilterSubscriber::CanceledEvent", "Event not announced" )
	    << " Canceled event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") could not be found in list of announced events." << ENDLOG;
	}
    else
	{
	LOG( AliHLTLog::kWarning, "TriggeredFilterSubscriber::CanceledEvent", "Event not found" )
	    << " Canceled event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") could not be found in lists of announced or received events." << ENDLOG;
	}
    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	}

    if ( found )
	{
	AliHLTEventDoneData* edd;
	edd = AliHLTMergeEventDoneData( eventID, eventDoneData );
	if ( !edd )
	    {
	    AliHLTEventDoneData tmpEDD;
	    AliHLTMakeEventDoneData( &tmpEDD, eventID );
	    bool noneNULL = false;
	    vector<AliHLTEventDoneData*>::iterator eddIter, eddEnd;
	    eddIter = eventDoneData.begin();
	    eddEnd = eventDoneData.end();
	    while ( eddIter != eddEnd )
		{
		if ( *eddIter && (*eddIter)->fDataWordCount )
		    noneNULL = true;
		eddIter++;
		}
	    if ( noneNULL )
		{
		LOG( AliHLTLog::kError, "TriggeredFilterSubscriber::CanceledEvent", "Out of memory" )
		    << "Out of memory merging event done data for event 0x" << AliHLTLog::kHex
		    << eventID << " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
		}
	    ed.fPublisher->EventDone( *this, tmpEDD );
	    }
	else
	    {
	    ed.fPublisher->EventDone( *this, *edd );
	    AliHLTFreeMergedEventDoneData( edd );
	    edd = NULL;
	    }
	if ( ed.fSEDD )
	    {
	    ret = pthread_mutex_lock( &fDescriptorMutex );
	    if ( ret )
		{
		}
	    if ( fDescriptors )
		fDescriptors->ReleaseEventDescriptor( eventID );
	    ret = pthread_mutex_unlock( &fDescriptorMutex );
	    if ( ret )
		{
		}
	    }
	if ( ed.fETS )
	    fETSCache.Release( (uint8*)ed.fETS );   //delete [] (AliUInt8_t*)ed.fETS;
	}
    }

bool TriggeredFilterSubscriber::GetAnnouncedEventData( AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
						       AliHLTEventTriggerStruct*& trigger )
    {
    int ret;
    EventData ed;
    unsigned long ndx;
    sedd = NULL;
    trigger = NULL;

    ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	}
    if ( MLUCVectorSearcher<EventData,AliEventID_t>::FindElement( fAnnouncedEvents, &EventSearchFunc, eventID, ndx ) )
	{
	ed = fAnnouncedEvents.Get( ndx );
	sedd = ed.fSEDD;
	trigger = ed.fETS;
	if ( !trigger )
	    trigger = &fEmptyETS;
	//fAnnouncedEvents.Remove( ndx );
	}
    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	}
    if ( sedd && trigger )
	return true;
    return false;
    }


int main( int argc, char** argv )
    {
    AliHLTFilteredStdoutLogServer sOut;
    gLogLevel = AliHLTLog::kAll;
    gLog.AddServer( sOut );

    char* rePublisherName = NULL;
    char* publisherName = NULL;
    char* subscriberID = NULL;
    char* errorStr = NULL;
    char* cpErr;
    bool doSysLog = false;
    int sysLogFacility = -1;
    bool nostdout = false;
    int slotCount = -1;
    bool untriggeredEventsEmpty = false;
    unsigned long minSEDDBlockCount = 0;
    unsigned long maxSEDDBlockCount = 2;
    bool aliceMode = false;
    AliHLTEventDataType eventDataType;
    eventDataType.fID = UNKNOWN_DATAID;
    AliHLTEventDataOrigin eventDataOrigin;
    eventDataOrigin.fID = UNKNOWN_DATAORIGIN;
    AliUInt32_t eventDataSpecification = ~(AliUInt32_t)0;
    bool eventDataTypeSet = false;
    bool eventDataOriginSet = false;
    bool eventDataSpecificationSet = false;

    const char* usage1 = "Usage: ";
    const char* usage2 = " (-V <verbosity>) -publisher <publishername> -subscribe <publisher> <susbcriberID> (-untriggeredempty) (-eventslots <event-slot-count>) (-nostdout) (-syslog <facility>) (-minblockcount <minimum-event-block-count) (-maxblockcount <maximum-event-block-count) (-alicemode) (-datatype <eventdatatype-id-max.8.chars>) (-dataorigin <eventdataorigin-id-max.4.chars>) (-dataspec <eventdataspecification>)";


    int i = 1;
    while ( (i < argc) && !errorStr )
	{
	if ( !strcmp( argv[i], "-nostdout" ) )
	    {
	    nostdout = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-syslog" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing syslog facility parameter";
		break;
		}
	    if ( !strcmp( argv[i+1], "DAEMON" ) )
		{
		sysLogFacility = LOG_DAEMON;
		}
	    else if ( !strcmp( argv[i+1], "LOCAL0" ) )
		{
		sysLogFacility = LOG_LOCAL0;
		}
	    else if ( !strcmp( argv[i+1], "LOCAL1" ) )
		{
		sysLogFacility = LOG_LOCAL1;
		}
	    else if ( !strcmp( argv[i+1], "LOCAL2" ) )
		{
		sysLogFacility = LOG_LOCAL2;
		}
	    else if ( !strcmp( argv[i+1], "LOCAL3" ) )
		{
		sysLogFacility = LOG_LOCAL3;
		}
	    else if ( !strcmp( argv[i+1], "LOCAL4" ) )
		{
		sysLogFacility = LOG_LOCAL4;
		}
	    else if ( !strcmp( argv[i+1], "LOCAL5" ) )
		{
		sysLogFacility = LOG_LOCAL5;
		}
	    else if ( !strcmp( argv[i+1], "LOCAL6" ) )
		{
		sysLogFacility = LOG_LOCAL6;
		}
	    else if ( !strcmp( argv[i+1], "LOCAL7" ) )
		{
		sysLogFacility = LOG_LOCAL7;
		}
	    else if ( !strcmp( argv[i+1], "USER" ) )
		{
		sysLogFacility = LOG_USER;
		}
	    else
		{
		errorStr = "Unknown syslog facility parameter.";
		break;
		}
	    doSysLog = true;
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-publisher" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing re-publisher name parameter";
		break;
		}
	    rePublisherName = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-subscribe" ) )
	    {
	    if ( argc <= i+2 )
		{
		errorStr = "Missing publishername or subscriber ID parameter";
		break;
		}
	    publisherName = argv[i+1];
	    subscriberID = argv[i+2];
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc <= i +1 )
		{
		errorStr = "Missing verbosity level specifier.";
		break;
		}
	    AliUInt32_t tmpll = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting verbosity level specifier..";
		break;
		}
	    gLogLevel = tmpll;
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-eventslots" ) )
	    {
	    if ( argc <= i +1 )
		{
		errorStr = "Missing event slot count specifier.";
		break;
		}
	    long tmpll = strtol( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting event slot count specifier..";
		break;
		}
	    slotCount = tmpll;
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-minblockcount" ) )
	    {
	    if ( argc <= i +1 )
		{
		errorStr = "Missing minimum event block count specifier.";
		break;
		}
	    minSEDDBlockCount = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting minimum event block count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-maxblockcount" ) )
	    {
	    if ( argc <= i +1 )
		{
		errorStr = "Missing maximum event block count specifier.";
		break;
		}
	    maxSEDDBlockCount = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting maximum event block count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-untriggeredempty" ) )
	    {
	    untriggeredEventsEmpty = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-alicemode" ) )
	    {
	    aliceMode = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-datatype" ) )
	    {
	    if ( argc <= i +1 )
		{
		errorStr = "Missing datatype specifier.";
		break;
		}
	    if ( strlen( argv[i+1])>8 )
		{
		errorStr = "Maximum allowed length for datatype specifier is 8 characters.";
		break;
		}
	    AliUInt64_t tmpDT = 0;
	    unsigned tmpN;
	    for( tmpN = 0; tmpN < strlen( argv[i+1]); tmpN++ )
		{
		tmpDT = (tmpDT << 8) | (AliUInt64_t)(argv[i+1][tmpN]);
		}
	    for ( ; tmpN < 8; tmpN++ )
		{
		tmpDT = (tmpDT << 8) | (AliUInt64_t)(' ');
		}
	    eventDataType.fID = tmpDT;
	    eventDataTypeSet = true;
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-dataorigin" ) )
	    {
	    if ( argc <= i +1 )
		{
		errorStr = "Missing dataorigin specifier.";
		break;
		}
	    if ( strlen( argv[i+1])>8 )
		{
		errorStr = "Maximum allowed length for dataorigin specifier is 4 characters.";
		break;
		}
	    AliUInt32_t tmpDO = 0;
	    unsigned tmpN;
	    for( tmpN = 0; tmpN < strlen( argv[i+1]); tmpN++ )
		{
		tmpDO = (tmpDO << 8) | (AliUInt32_t)(argv[i+1][tmpN]);
		}
	    for ( ; tmpN < 4; tmpN++ )
		{
		tmpDO = (tmpDO << 8) | (AliUInt32_t)(' ');
		}
	    eventDataOrigin.fID = tmpDO;
	    eventDataOriginSet = true;
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-dataspec" ) )
	    {
	    if ( argc <= i +1 )
		{
		errorStr = "Missing event data specification specifier.";
		break;
		}
	    AliUInt32_t tmpll = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting event data specification specifier..";
		break;
		}
	    eventDataSpecification = tmpll;
	    eventDataSpecificationSet = true;
	    i += 2;
	    continue;
	    }
	errorStr = "Unknown parameter";
	}

    if ( !errorStr )
	{
	if ( !rePublisherName )
	    errorStr = "Must specify the -publisher parameter.";
	else if ( !publisherName || !subscriberID )
	    errorStr = "Must specify the -subscribe parameter.";
	}
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "TriggeredFilter", "Command line arguments" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	LOG( AliHLTLog::kError, "TriggeredFilter", "Command line arguments" )
	    << errorStr << ENDLOG;
	return -1;
	}

    if ( nostdout )
	{
	gLog.DelServer( sOut );
	}

    MLUCString name;
    name = rePublisherName;
    name += "-";
    AliHLTFileLogServer fileOut( name.c_str(), ".log", 3000000 );
    gLog.AddServer( fileOut );

    AliHLTSysLogServer* sysLog = NULL;
    if ( doSysLog )
	{
	sysLog = new AliHLTSysLogServer( publisherName, sysLogFacility );
	if ( sysLog )
	    gLog.AddServer( sysLog );
	}
    
    TriggeredFilterSubscriber subscriber( subscriberID, slotCount );
    TriggeredFilterPublisher rePublisher( rePublisherName, &subscriber, slotCount );
    AliHLTPublisherPipeProxy publisher( publisherName );

    AliHLTDescriptorHandler descriptors( (slotCount<0 ? 4 : slotCount) );

    subscriber.SetPublisher( &rePublisher );
    subscriber.SetDescriptorHandler( &descriptors );
    subscriber.AnnounceUntriggeredEventsAsEmpty( untriggeredEventsEmpty );
    subscriber.SetAliceMode( aliceMode );

    if ( eventDataTypeSet )
	subscriber.SetDataType( eventDataType );
    if ( eventDataOriginSet )
	subscriber.SetDataOrigin( eventDataOrigin );
    if ( eventDataSpecificationSet )
	subscriber.SetDataSpecification( eventDataSpecification );

    rePublisher.StartAnnouncing();

    publisher.Subscribe( subscriber );
    publisher.SetEventDoneDataSend( subscriber, true );
    publisher.StartPublishing( subscriber );

    rePublisher.StopAnnouncing();
    rePublisher.CancelAllSubscriptions( true );

    return 0;
    }

#endif


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
