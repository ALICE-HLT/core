/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3REPUBLISHER_HPP_
#define _ALIL3REPUBLISHER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTDetectorPublisher.hpp"
#include <pthread.h>
// #include "TFile.h"
// #include "AliTPCParam.h"
// #include "TROOT.h"

class AliHLTDetectorSubscriber;

class AliHLTDetectorRePublisher: public AliHLTDetectorPublisher
    {
    public:

	AliHLTDetectorRePublisher( const char* name, bool sendEventDone, int slotCntExp2 = -1 );
	virtual ~AliHLTDetectorRePublisher();

	void SetPrePublisher( AliHLTPublisherInterface& pub )
		{
		fPub = &pub;
		}
	void SetPreSubscriber( AliHLTSubscriberInterface& sub )
		{
		fSub = &sub;
		}


    protected:

	virtual int WaitForEvent( AliEventID_t& eventID, AliHLTSubEventDataDescriptor*& sbevent,
				  AliHLTEventTriggerStruct*& trg ); // Called with no locked mutexes
	virtual void EventFinished( AliEventID_t, AliHLTSubEventDataDescriptor& sbevent, vector<AliHLTEventDoneData*>& eventDoneData ); // Called with no locked mutexes
	virtual void EventFinished( AliEventID_t, vector<AliHLTEventDoneData*>& eventDoneData ); // Called with no locked mutexes
	virtual void QuitEventLoop(); // Called with no locked mutexes
	virtual void StartEventLoop();
	virtual void EndEventLoop();

	virtual void AnnouncedEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, 
				     const AliHLTEventTriggerStruct& trigger, AliUInt32_t refCount );


	AliHLTPublisherInterface* fPub;
	AliHLTSubscriberInterface* fSub;

	bool fSendEventDone;

	AliHLTEventDoneData fDefaultEDD;

	    friend class AliHLTDetectorSubscriber;

    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3REPUBLISHER_HPP_
