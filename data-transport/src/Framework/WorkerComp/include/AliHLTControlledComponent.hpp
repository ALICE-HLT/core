#ifndef _ALIHLTCONTROLLEDCOMPONENT_HPP_
#define _ALIHLTCONTROLLEDCOMPONENT_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTProcessingSubscriber.hpp"
#include "AliHLTProcessingRePublisher.hpp"
#include "AliHLTSamplePublisher.hpp"
#include "AliHLTPublisherProxyInterface.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTSCCommandProcessor.hpp"
#include "AliHLTPersistentState.hpp"
#include "AliHLTTriggerStackMachineAssembler.hpp"
#include "MLUCString.hpp"
#include "MLUCConditionSem.hpp"
#include "MLUCSysMESLogServer.hpp"
#include "MLUCDynamicLibraryLogServer.hpp"
#include "MLUCMutex.hpp"
#include "AliHLTReadoutList.hpp"
#include <iostream>
#include <map>
#include <signal.h>

class AliHLTStatus;
class AliHLTSCInterface;
struct AliHLTSCCommandStruct;
class AliHLTBufferManager;
class AliHLTDetectorPublisher;
struct BCLAbstrAddressStruct;
class AliHLTEventAccounting;
class AliHLTTriggerStackMachine;

class AliHLTControlledComponent
    {
    public:

	virtual ~AliHLTControlledComponent();

	virtual void Run();

	void SetMinPreBlockCount( AliUInt32_t minPreBlockCount )
		{
		fMinPreBlockCount = minPreBlockCount;
		}

	void SetMaxPreBlockCount( AliUInt32_t maxPreBlockCount )
		{
		fMaxPreBlockCount = maxPreBlockCount;
		}

	void SetMaxNoUseCount( AliUInt32_t maxNoUseCount )
		{
		fMaxNoUseCount = maxNoUseCount;
		}

	void AliceHLT()
		{
		fAliceHLT = true;
		}

    protected:

	class SubscriptionListenerThread;
	class SubscriptionThread;
	class CommandProcessor;
	class SubscriberEventCallback;
	    friend class CommandProcessor;
	    friend class SubscriberEventCallback;
	class SubscriberEventCallback: public AliHLTSamplePublisher::SubscriberEventCallback
	    {
	    public:
		SubscriberEventCallback( AliHLTControlledComponent* comp ):
		    fComp( comp )
			{
			}
		virtual ~SubscriberEventCallback() {}
		virtual void Subscribed( AliHLTSamplePublisher*, const char*, AliHLTSubscriberInterface* )
			{
			fComp->Subscription();
			}
		virtual void Unsubscribed( AliHLTSamplePublisher*, const char*, AliHLTSubscriberInterface*, bool )
			{
			fComp->Unsubscription();
			}
	    protected:
		AliHLTControlledComponent* fComp;
	    };
	class CommandProcessor: public AliHLTSCCommandProcessor
	    {
	    public:
		CommandProcessor( AliHLTControlledComponent* comp ):
		    fComp( comp )
			{
			}
		virtual void ProcessCmd( AliHLTSCCommandStruct* cmd )
			{
			fComp->ProcessSCCmd( cmd );
			}
	    protected:
		AliHLTControlledComponent* fComp;
	    };



	enum AliHLTControlledComponentType { kUnknown = 0, kDataSource = 1, kDataSink = 2, kDataProcessor = 3, kAsyncProcessor = 4 };

	AliHLTControlledComponent( AliHLTControlledComponentType componentType, const char* compName, int argc, char** argv );

	virtual void ResetConfiguration();

	virtual bool PreConfigure();
	virtual bool PostUnconfigure();

	virtual bool CheckConfiguration();
	virtual void ShowConfiguration();

	virtual void Configure();
	virtual void Unconfigure();

	virtual bool CreateParts();
	virtual void DestroyParts();

	virtual bool SetupStdLogging();
 	virtual bool SetupFileLogging();
 	virtual bool SetupSysLogging();
 	virtual bool SetupSysMESLogging();
	virtual bool SetupDynLibLogging();
	virtual void RemoveLogging();

	virtual bool ParseCmdLine();
	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );

	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );
	
	virtual void IllegalCmd( AliUInt32_t state, AliUInt32_t cmd );


	virtual bool SetupSC();
	virtual AliHLTStatus* CreateStatusData();
	virtual bool SetupStatusData();
	virtual AliHLTSCInterface* CreateSCInterface();
	virtual bool SetupSCInterface();
	virtual void RemoveSC();
	virtual void LookAtMe();

	virtual void ProcessSCCmd( AliHLTSCCommandStruct* cmd );
	virtual void ExtractConfig( AliHLTSCCommandStruct* cmd );


	virtual bool SetupShmBufferMemory();
	virtual bool SetupSharedMemory();
	virtual bool RemoveShmBufferMemory();
#ifdef USE_DDL_LIST_SHM
	virtual bool SetupDDLListSharedMemory();
#endif
	virtual AliHLTSharedMemory* CreateSharedMemory();
	virtual AliHLTShmManager* CreateShmManager();
	virtual AliHLTBufferManager* CreateBufferManager();

 	virtual AliHLTDescriptorHandler* CreateInDescriptorHandler();
	virtual AliHLTDescriptorHandler* CreateOutDescriptorHandler();

	virtual AliHLTDetectorPublisher* CreatePublisher() = 0;
	virtual AliHLTProcessingRePublisher* CreateRePublisher() = 0;
	virtual AliHLTPublisherProxyInterface* CreatePublisherProxy() = 0;
	virtual SubscriptionListenerThread* CreateSubscriptionThread();

	virtual AliHLTProcessingSubscriber* CreateSubscriber() = 0;

	virtual bool SetupComponents();
	virtual bool Subscribe();
	virtual void Unsubscribe();
	virtual void AbortSubscription();

	void Subscription();
	void Unsubscription();


	virtual void PURGEALLEVENTS();

	bool RegisterSubsystemVerbosity( MLUCString const& subsysID, int& verbosity );
	bool UnregisterSubsystemVerbosity( MLUCString const& subsysID );
	bool SetSubsystemVerbosity( MLUCString const& subsysID, int verbosity );

        void PreMapRorcBuffers();
        void UnMapRorcBuffers();

	AliHLTControlledComponentType fComponentType;
	const char* fComponentTypeName;
	
	const char* fComponentName;

	int fArgc;
	char** fArgv;

	MLUCConditionSem<AliUInt32_t> fCmdSignal;
	//AliUInt32_t fCmd;

	bool fStdoutLogging;
	MLUCString fLogName;
	AliHLTFilteredStdoutLogServer *fLogOut;
	
	bool fFileLogging;
	AliHLTFileLogServer* fFileLog;

	bool fSysLogging;
	AliHLTSysLogServer* fSysLog;
	int fSysLogFacility;

	bool fSysMESLogging;
	MLUCSysMESLogServer* fSysMESLog;

	bool fDynLibLogging;
        MLUCDynamicLibraryLogServer* fDynLibLog;
        MLUCString fDynLibPath;

	bool fBenchmark;

	struct LAMAddressData
	    {
		MLUCString fStr;
		BCLAbstrAddressStruct* fAddress;
	    };

	AliHLTStatus* fStatus;
	MLUCString fSCInterfaceAddress;
	AliHLTSCInterface* fSCInterface;
	vector<LAMAddressData> fLAMAddresses;
	pthread_mutex_t fLAMAddressMutex;

	CommandProcessor fCmdProcessor;

	int fPersistentStateShmID;
	AliHLTPersistentStateBase* fPersistentState;

	bool fPrivateSharedMemory;
	AliHLTSharedMemory* fSharedMemory;
	AliHLTShmManager* fShmManager;
	AliHLTBufferManager* fBufferManager;
	enum TBufferManagerType { kLargestBlockBufferManager, kRingBufferManager };
	TBufferManagerType fBufferManagerType;

	AliUInt32_t fOutputShm;
	AliHLTShmID fShmKey;
	unsigned long fBufferSize;
	AliUInt32_t fMaxPreEventCount;
	unsigned fMaxPreEventCountExp2;
	AliUInt32_t fMinPreBlockCount;
	AliUInt32_t fMaxPreBlockCount;
	AliUInt32_t fMaxNoUseCount;

	unsigned long fMinBlockSize;
	AliUInt32_t fPerEventFixedSize;
	AliUInt32_t fPerBlockFixedSize;
	double fSizeFactor;


 	AliHLTDescriptorHandler* fInDescriptorHandler;
	AliHLTDescriptorHandler* fOutDescriptorHandler;


	bool fSendEventDone;

	
	unsigned long fRunNumber;
	unsigned long fBeamType;
	unsigned long fHLTMode;
	MLUCString fRunType;
	//unsigned long fRunType;


	MLUCString fOwnPublisherName;
	AliUInt32_t fPublisherTimeout;
	AliHLTSamplePublisher* fPublisher;
	AliUInt32_t fSubscriberCount;
	SubscriberEventCallback fSubscriberEventCallback;

	MLUCString fAttachedPublisherName;
	key_t fPub2SubProxyShmKey;
	key_t fSub2PubProxyShmKey;
	unsigned long fProxyShmSize;
	AliHLTPublisherProxyInterface* fPublisherProxy;

	pthread_mutex_t fSubscriberRemovalListMutex;
	vector<MLUCString> fSubscriberRemovalList;

	SubscriptionListenerThread* fSubscriptionListenerThread;

	bool fSubscriptionRequestOldEvents;
	
	MLUCString fSubscriberID;
	AliHLTProcessingSubscriber* fSubscriber;
	SubscriptionThread* fSubscriptionThread;
	AliUInt32_t fSubscriberRetryCount;
	AliUInt32_t fSubscriberRetryTime;

	AliUInt32_t fSubscriptionEventModulo;
	bool fUseSubscriptionEventModulo;

	bool fTransientSubscriber;

	unsigned long fMaxEventAge;
#ifdef ALLOW_ARTIFICIAL_EVENT_LOSS
	unsigned long fEventLossModulo;
#endif
	
	class SubscriptionListenerThread;
	    friend class SubscriptionListenerThread;
	class SubscriptionListenerThread: public AliHLTThread
	    {
	    public:
		SubscriptionListenerThread( AliHLTControlledComponent* comp, AliHLTSamplePublisher* pub  ):
		    fPub( pub ), fComp( comp )
			{
			fQuitted = true;
			}
		bool HasQuitted()
			{
			return fQuitted;
			}
		void Quit()
			{
			if ( fPub )
			    QuitPipeSubscriptionLoop( *fPub );
			}
	    protected:
		virtual void Run();
		
		AliHLTSamplePublisher* fPub;
		AliHLTControlledComponent* fComp;
		bool fQuitted;
	    };


	class SubscriptionThread;
	    friend class SubscriptionThread;
	class SubscriptionThread: public AliHLTThread
	    {
	    public:
		SubscriptionThread( AliHLTControlledComponent* comp, AliHLTPublisherProxyInterface* proxy, AliHLTProcessingSubscriber* sub ):
		    fProxy( proxy ), fSubscriber( sub ), fComp( comp )
			{
			fQuitted = true;
			}
		bool HasQuitted()
			{
			return fQuitted;
			}
		void Quit()
			{
			if ( fProxy )
			    fProxy->Unsubscribe( *fSubscriber );
			}
		void AbortPublishing()
			{
			if ( fProxy )
			    fProxy->AbortPublishing();
			}
	    protected:
		virtual void Run();

		AliHLTPublisherProxyInterface* fProxy;
		AliHLTProcessingSubscriber* fSubscriber;
		AliHLTControlledComponent* fComp;
		bool fQuitted;
	    };

	bool fAliceHLT;

	AliHLTEventDataType fEventDataType;
	AliHLTEventDataOrigin fEventDataOrigin;
	AliUInt32_t fEventDataSpecification;
	bool fAllowDataTypeOriginSpecSet;

	AliUInt32_t fDDLID;
	hltreadout_uint32_t* fDDLListData;
#ifdef USE_DDL_LIST_SHM
	AliHLTShmID fDDLListShmKey;
	AliUInt32_t fDDLListOutputShm;
	AliHLTSubEventDataBlockDescriptor fDDLListBlock;

	bool fNoDDLListAutoGeneration;
#endif

	unsigned fTickInterval_us;

	unsigned long fMaxPendingEventCount;

	AliHLTEventAccounting* fEventAccounting;

	bool fRequestPublisherEventDoneData;

	bool fSecondary;

	unsigned long fEventModuloPreDivisor;

	struct TSubsystemVerbosityData
	    {
		TSubsystemVerbosityData( MLUCString id, int& verbosity ):
		    fID( id ),
		    fVerbosity( &verbosity )
			{
			}

		MLUCString fID;
		int* fVerbosity;
	    };
	std::vector<TSubsystemVerbosityData> fSubsystemVerbosities;
	MLUCMutex fSubsystemVerbosityMutex;
	//std::map<MLUCString,int&> fSubsystemVerbosities;

	AliHLTTriggerStackMachine* fTriggerStackMachine;

	std::vector<MLUCString> fEventTypeStackMachineCodesText;

	std::vector<AliHLTEventTriggerStruct*> fEventTypeStackMachineCodes;

	AliHLTTriggerStackMachineAssembler fStackMachineAssembler;

	std::map<MLUCString,unsigned> fTriggerClassBitIndices;

	std::map<MLUCString,MLUCString> fTriggerClassGroups;

	bool fHardwareCoProcessor;
	AliHLTEventDataType fHardwareCoProcessorOutputEventDataType;
	bool fHardwareCoProcessorOutputEventDataTypeSet;

	MLUCString fECSParameters;

	bool fForceEventDoneDataForward;

	bool fAdaptBlockFreeUpdate;

	struct sigaction fOldSigAction;

	bool fAllowIgnoreEventTriggerCommands;
	bool fIgnoreEventTriggerCommands;

        vector<unsigned int> fPreMapRorcBuffers;

    private:
    };


class AliHLTControlledSourceComponent: public AliHLTControlledComponent
    {
    public:
	AliHLTControlledSourceComponent( const char* compName, int argc, char** argv ):
	    AliHLTControlledComponent( AliHLTControlledComponent::kDataSource, compName, argc, argv )
		{
		}
	~AliHLTControlledSourceComponent();

    protected:

	virtual AliHLTProcessingRePublisher* CreateRePublisher()
		{
		return NULL;
		}
	virtual AliHLTPublisherProxyInterface* CreatePublisherProxy()
		{
		return NULL;
		}
	virtual AliHLTProcessingSubscriber* CreateSubscriber()
		{
		return NULL;
		}

    };

class AliHLTControlledAsyncProcessorComponent: public AliHLTControlledComponent
    {
    public:
	AliHLTControlledAsyncProcessorComponent( const char* compName, int argc, char** argv ):
	    AliHLTControlledComponent( AliHLTControlledComponent::kAsyncProcessor, compName, argc, argv )
		{
		}
	~AliHLTControlledAsyncProcessorComponent();

    protected:

	virtual AliHLTDetectorPublisher* CreatePublisher()
		{
		return NULL;
		}
	virtual AliHLTProcessingRePublisher* CreateRePublisher();
	virtual AliHLTPublisherProxyInterface* CreatePublisherProxy();
    };

class AliHLTControlledProcessorComponent: public AliHLTControlledComponent
    {
    public:
	AliHLTControlledProcessorComponent( const char* compName, int argc, char** argv ):
	    AliHLTControlledComponent( AliHLTControlledComponent::kDataProcessor, compName, argc, argv )
		{
		}
	~AliHLTControlledProcessorComponent();

    protected:

	virtual AliHLTDetectorPublisher* CreatePublisher()
		{
		return NULL;
		}
	virtual AliHLTProcessingRePublisher* CreateRePublisher();
	virtual AliHLTPublisherProxyInterface* CreatePublisherProxy();
    };

template<class Processor>
class AliHLTControlledDefaultProcessorComponent: public AliHLTControlledProcessorComponent
    {
    public:
	AliHLTControlledDefaultProcessorComponent( const char* compName, int argc, char** argv ):
	    AliHLTControlledProcessorComponent( compName, argc, argv )
		{
		}
	~AliHLTControlledDefaultProcessorComponent()
		{
		}

    protected:
	virtual AliHLTProcessingSubscriber* CreateSubscriber()
		{
		return new Processor( fSubscriberID.c_str(), fSendEventDone, fMinBlockSize, fPerEventFixedSize, fPerBlockFixedSize, fSizeFactor, fMaxPreEventCountExp2 );
		}

    };

class AliHLTControlledSinkComponent: public AliHLTControlledComponent
    {
    public:
	AliHLTControlledSinkComponent( const char* compName, int argc, char** argv ):
	    AliHLTControlledComponent( AliHLTControlledComponent::kDataSink, compName, argc, argv )
		{
		}
	~AliHLTControlledSinkComponent();

    protected:
	virtual AliHLTDetectorPublisher* CreatePublisher()
		{
		return NULL;
		}
	virtual AliHLTProcessingRePublisher* CreateRePublisher();
	virtual AliHLTPublisherProxyInterface* CreatePublisherProxy();
    };


template<class Sink>
class AliHLTControlledDefaultSinkComponent: public AliHLTControlledSinkComponent
    {
    public:
	AliHLTControlledDefaultSinkComponent( const char* compName, int argc, char** argv ):
	    AliHLTControlledSinkComponent( compName, argc, argv )
		{
		}
	~AliHLTControlledDefaultSinkComponent()
		{
		}

    protected:
	virtual AliHLTProcessingSubscriber* CreateSubscriber()
		{
		return new Sink( fSubscriberID.c_str(), fSendEventDone, fMinBlockSize, fPerEventFixedSize, fPerBlockFixedSize, fSizeFactor, fMaxPreEventCountExp2 );
		}

    };


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTCONTROLLEDCOMPONENT_HPP_
