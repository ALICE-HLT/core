/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3PROCESSINGREPUBLISHER_HPP_
#define _ALIL3PROCESSINGREPUBLISHER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSamplePublisher.hpp"
#include <pthread.h>
#include <vector>

class AliHLTProcessingSubscriber;

class AliHLTProcessingRePublisher: public AliHLTSamplePublisher
    {
    public:

	AliHLTProcessingRePublisher( const char* name, int eventSlotsPowerOfTwo );
	virtual ~AliHLTProcessingRePublisher();

	void SetSubscriber( AliHLTProcessingSubscriber* sub )
		{
		fSubscriber = sub;
		}

    protected:

	virtual void CanceledEvent( AliEventID_t event, vector<AliHLTEventDoneData*>& eventDoneData );

	virtual void EventDoneDataReceived( const char* subscriberName, AliEventID_t eventID,
					    const AliHLTEventDoneData*, bool forceForward, bool forwarded );

	virtual bool GetAnnouncedEventData( AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
					    AliHLTEventTriggerStruct*& trigger );

	AliHLTProcessingSubscriber* fSubscriber;

    private:
    };






/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3PROCESSINGREPUBLISHER_HPP_
