/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3DETECTORPUBLISHER_HPP_
#define _ALIL3DETECTORPUBLISHER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSamplePublisher.hpp"
#include "AliHLTSharedMemory.hpp"
#include "AliHLTPersistentState.hpp"
#include "AliHLTShmID.h"
#include "AliHLTThread.hpp"
#include "AliHLTStatus.hpp"
#include "MLUCDynamicAllocCache.hpp"
#include "AliHLTReadoutList.hpp"
//#include <psi.h>
#include <pthread.h>
#include <vector>

class AliHLTBufferManager;
class AliHLTShmManager;
class AliHLTDescriptorHandler;
class AliHLTHLTEventTriggerData;

#define ALIHLTDETECTORPUBLISHERSTATE_TYPE ((AliUInt32_t)'DEPS')

class AliHLTDetectorPublisher: public AliHLTSamplePublisher
    {
    public:

	struct State
	    {
		State()
			{
			fHeader.fLength = sizeof(State);
			fHeader.fType.fID = ALIHLTDETECTORPUBLISHERSTATE_TYPE;
			fHeader.fSubType.fID = 0;
			fHeader.fVersion = 1;
			fLastEventID = AliEventID_t();
			fState = 0;
			Touch();
			}
		void Touch()
			{
			struct timeval tv;
			gettimeofday( &tv, NULL );
			fUpdateTime_sec = tv.tv_sec;
			fUpdateTime_usec = tv.tv_usec;
			}
		AliHLTBlockHeader fHeader;
		AliEventID_t fLastEventID;
		AliUInt32_t fUpdateTime_sec;
		AliUInt32_t fUpdateTime_usec;
		AliUInt32_t fState;
	    };

	AliHLTDetectorPublisher( const char* name, int eventSlotsPowerOfTwo = -1 );
	virtual ~AliHLTDetectorPublisher();

	void SetBufferManager( AliHLTBufferManager* bufferMan )
		{
		fBufferManager = bufferMan;
		}

	void SetShmManager( AliHLTShmManager* shmMan )
		{
		fShmManager = shmMan;
		}
	void SetShm( AliHLTSharedMemory* shm )
		{
		fShm = shm;
		}

	void SetDescriptorHandler( AliHLTDescriptorHandler* descrHand )
		{
		fDescriptors = descrHand;
		}

	void SetPersistentState( State* persState )
		{
		fPersistentState = persState;
		}

	void SetMaxEventAge( unsigned long age )
		{
		fMaxEventAge = age;
		}

	void SetRunNumber( unsigned long runNr )
		{
		fRunNumber = runNr;
		}

	void SetRunType( unsigned long beamtype, unsigned long hltmode, MLUCString const& runType )
		{
		fBeamType = beamtype;
		fHLTMode = hltmode;
		fRunType = runType;
		}

	void SetMaxPendingEventCount( unsigned long maxCnt )
		{
		fMaxPendingEventCount = maxCnt;
		}

	void UpdateEventAge();

	virtual void StartAnnouncing();

	virtual void StopAnnouncing();

	operator bool();

	void DoBenchmark()
		{
		fBenchmark = true;
		}

	void ForwardEvents()
		{
		fForwardEvents = true;
		}

	void SetStatusData( AliHLTStatus* status )
		{
		fStatus = status;
		}

	virtual void PauseProcessing();
	virtual void ResumeProcessing();

	void AliceHLT()
		{
		fAliceHLT = true;
		}

#ifdef USE_DDL_LIST_SHM
	void SetDDLListBlock( AliHLTSubEventDataBlockDescriptor const& dlbd )
		{
		fDDLListBlock = dlbd;
		fDDLListBlockSet = true;
		}
#endif
	void SetDDLListData( AliUInt32_t* ddlListData )
		{
		fDDLListData = ddlListData;
		}

	void SetTickInterval( unsigned tickInterval_us )
		{
		fTickInterval_us = tickInterval_us;
		}

	void SetECSParameters( const MLUCString& ecsParam )
		{
		fECSParameters = ecsParam;
		}

	void AddConfigureEvent( AliUInt32_t eventType, AliUInt64_t eventNr, char const* data );


// 	AliHLTDescriptorHandler& GetDescriptors()
// 		{
// 		return fDescriptors;
// 		}

// 	AliUInt64_t GetShmKey( AliUInt32_t bufferIndex ) const
// 		{
// 		return ((bufferIndex < fShmData.size()) ? fShmData[bufferIndex].fShmKey.fID : (AliUInt64_t)~0);
// 		}

// 	AliUInt8_t* GetShmPtr( AliUInt32_t bufferIndex ) const
// 		{
// 		return (AliUInt8_t*)((bufferIndex < fShmData.size()) ? fShmData[bufferIndex].fShmPtr : NULL);
// 		}
// 	AliUInt32_t GetBufferIndex( AliUInt64_t shmKey )
// 		{
// 		if ( shmKey-fShmKeyStart >= fShmData.size() )
// 		    return (AliUInt32_t)-1;
// 		return shmKey-fShmKeyStart;
// 		}


// 	AliUInt32_t GetFreeBlock( AliUInt32_t size,  AliUInt32_t& bufferIndex  ); // Returns offset;
// 	void ReleaseBlock(  AliUInt32_t bufferIndex, AliUInt32_t offset, AliUInt32_t size );

	virtual void DumpEvents();

	virtual void DumpEventMetaData();

	virtual void PURGEALLEVENTS();

	virtual void SetStartTimestamp( unsigned long startTimestamp )
		{
		fStartTimestamp = startTimestamp;
		}

    protected:

	// Return 0 to go on, !=0 when finished.
	virtual int WaitForEvent( AliEventID_t& eventID, AliHLTSubEventDataDescriptor*& sbevent,
				  AliHLTEventTriggerStruct*& trg ) = 0; // Called with no locked mutexes
	virtual void EventFinished( AliEventID_t, AliHLTSubEventDataDescriptor& sbevent, vector<AliHLTEventDoneData*>& eventDoneData ) = 0; // Called with no locked mutexes
	virtual void EventFinished( AliEventID_t, vector<AliHLTEventDoneData*>& eventDoneData ) = 0; // Called with no locked mutexes
	virtual void QuitEventLoop() = 0; // Called with no locked mutexes
	virtual void StartEventLoop() = 0;
	virtual void EndEventLoop() = 0;

	virtual bool MustContinue()
		{
		return false;
		}

	virtual void CanceledEvent( AliEventID_t event, vector<AliHLTEventDoneData*>& eventDoneData );
	virtual void AnnouncedEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, 
				     const AliHLTEventTriggerStruct& trigger, AliUInt32_t refCount );
	virtual void EventDoneDataReceived( const char* subscriberName, AliEventID_t eventID,
					    const AliHLTEventDoneData*, bool forceForward, bool forwarded );
	virtual bool GetAnnouncedEventData( AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
				      AliHLTEventTriggerStruct*& trigger );

	virtual unsigned GetAdditionalBlockCount();
	virtual bool GetAdditionalBlock( unsigned ndx, AliHLTSubEventDataBlockDescriptor& blockDescriptor );
	virtual bool ReleaseAdditionalBlock( AliHLTSubEventDataBlockDescriptor& blockDescriptor );

	virtual int CreateSORBlocks( vector<AliHLTSubEventDataBlockDescriptor>& blockDescriptors );
	virtual int ReleaseSORBlocks();
	virtual int CreateEORBlocks( vector<AliHLTSubEventDataBlockDescriptor>& blockDescriptor );
	virtual int ReleaseEORBlocks();

	void Tick( bool sendTickEvent = true );

	bool FillHLTEventTriggerDataDDLList( AliHLTHLTEventTriggerData* hetd );
	

	AliHLTNodeID_t fNodeID;

	bool fBenchmark;

	bool fForwardEvents;

	MLUCDynamicAllocCache fETSCache;
	AliHLTDescriptorHandler* fDescriptors;
	AliHLTBufferManager* fBufferManager;
	AliHLTShmManager* fShmManager;
	AliHLTSharedMemory* fShm;

	State* fPersistentState;

	struct ETSData
	    {
		AliEventID_t fEventID;
		AliHLTEventTriggerStruct* fETS;
	    };

	MLUCVector<ETSData> fETSs;
	pthread_mutex_t fETSMutex;

	
	class AliHLTDetectorEventAnnouncer;
	friend class AliHLTDetectorEventAnnouncer;

	class AliHLTDetectorEventAnnouncer: public AliHLTThread
	    {
	    public:

		AliHLTDetectorEventAnnouncer( AliHLTDetectorPublisher& pub );

		void Quit();
		bool HasQuitted();
		bool ShallQuit() const
			{
			return fQuit;
			}

	    protected:

		virtual void Run();

		AliHLTDetectorPublisher& fPub;

// XXXX TODO LOCKING XXXX
		MLUCMutex fQuitMutex;
		bool fQuit;
		bool fQuitted;

	    private:
	    };

	AliHLTDetectorEventAnnouncer fEventAnnouncer;

	pthread_mutex_t fSEDDMutex;

	static bool EventSearchFunc( const ETSData& ed, const AliEventID_t& searchData )
		{
		return ed.fEventID == searchData;
		}

	//unsigned long fAnnounceBackoffTime; // backoff time after a failed announce before the retry for the same event in microsec. 
	unsigned long fAnnounceBackoffSleepTime; // backoff time after a failed announce before the retry for the same event in microsec. 
	unsigned long fAnnouncePollSleepModulo;
	unsigned long fAnnounceBusyPollTime;

	AliHLTStatus* fStatus;
	bool fPaused;
	MLUCConditionSem<uint8> fPauseSignal;

	unsigned long fStatusRateUpdateTime_ms;
	struct timeval fStartTime;
	struct timeval fResumeTime;
	struct timeval fLastMeasurementTime;
	bool fStarted;
	bool fMeasurementModeExactRate;
	AliUInt64_t fLastMeasurementAnnounceEventCount;
	AliUInt64_t fLastMeasurementTotalProcessedOutputDataSize;
	AliUInt64_t fStartMeasurementAnnounceEventCount;
	AliUInt64_t fStartMeasurementResumeAnnounceEventCount;
	AliUInt64_t fStartMeasurementTotalProcessedOutputDataSize;
	AliUInt64_t fStartMeasurementResumeTotalProcessedOutputDataSize;
	
	virtual void EventRateTimerExpired();
	class EventRateTimerCallback;
	    friend class EventRateTimerCallback;
	class EventRateTimerCallback: public AliHLTTimerCallback
	    {
	    public:
		EventRateTimerCallback( AliHLTDetectorPublisher* parent ):
		    fParent( parent )
			{
			}
		virtual ~EventRateTimerCallback() {};

		virtual void TimerExpired( AliUInt64_t, unsigned long )
			{
			fParent->EventRateTimerExpired();
			}

	    protected:
		AliHLTDetectorPublisher* fParent;
	    };

	EventRateTimerCallback fEventRateTimerCallback;

	pthread_mutex_t fStatusMutex;

	unsigned long fMaxEventAge;

	bool fAliceHLT;
#ifdef USE_DDL_LIST_SHM
	AliHLTSubEventDataBlockDescriptor fDDLListBlock;
	bool fDDLListBlockSet;
#endif
	AliUInt32_t* fDDLListData;

	std::vector<AliUInt64_t> fMonitorFirstEventOrbitNrs;
	pthread_mutex_t fMonitorFirstEventOrbitNrMutex;

	AliUInt64_t fLastTickEvent;
	unsigned fTickInterval_us;

	unsigned long fRunNumber;
	unsigned long fBeamType;
	unsigned long fHLTMode;
	MLUCString fRunType;
	//unsigned long fRunType;

// XXXX TODO LOCKING XXXX
	MLUCMutex fShmIDMutex;
	AliUInt32_t fSORShmID;
	AliUInt32_t fEORShmID;

	unsigned long fMaxPendingEventCount;

	pthread_mutex_t fConfigureEventIDMutex;
	struct ConfigureEventData
	    {
		AliUInt32_t fEventType;
		AliUInt64_t fEventNr;
		MLUCString fData;
		AliUInt32_t fShmID;
	    };
	vector<ConfigureEventData> fConfigureEventIDs;
	vector<ConfigureEventData> fSentConfigureEventIDs;

	bool fBufferWarnings;
	bool fBuffer20PercentWarning;
	bool fBuffer10PercentWarning;

	MLUCString fECSParameters;

	unsigned long fStartTimestamp;

    private:
    };






/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3DETECTORPUBLISHER_HPP_
