/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3DETECTORSUBSCRIBER_HPP_
#define _ALIL3DETECTORSUBSCRIBER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSampleSubscriber.hpp"
#include "AliHLTPublisherInterface.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTSharedMemory.hpp"
#include "AliHLTThread.hpp"
#include "AliHLTTimer.hpp"
#include "MLUCAllocCache.hpp"
#include <vector>
#include <pthread.h>

class AliHLTDetectorSubscriber: public AliHLTSampleSubscriber
    {
    public:

	struct BlockData
	    {
		AliHLTShmID fShmID;
		AliUInt32_t fOffset;
		AliUInt8_t* fData;
		AliUInt32_t fSize;
		AliHLTNodeID_t fProducerNode;
		AliHLTEventDataType fDataType;
		AliHLTEventDataOrigin fDataOrigin;
		AliUInt32_t fDataSpecification;
		AliUInt8_t fAttributes[kAliHLTSEDBDAttributeCount]; // Byte order and alignment
	    };


	AliHLTDetectorSubscriber( const char* name, AliUInt32_t maxShmNoUseCount, int eventCountAlloc = -1 );
	virtual ~AliHLTDetectorSubscriber();

	void SetPublisher( AliHLTPublisherInterface& publ )
		{
		fPub = &publ;
		}

	operator bool();

	virtual int NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
			      const AliHLTSubEventDataDescriptor& sbevent,
			      const AliHLTEventTriggerStruct& ets );
	
	// This method is called for a transient subscriber, 
	// when an event has been removed from the list. 
	virtual int EventCanceled( AliHLTPublisherInterface& publisher, AliEventID_t eventID );
	
	// This method is called when the subscription ends.
	virtual int SubscriptionCanceled( AliHLTPublisherInterface& publisher );

	// This method is called when the publishing 
	// service runs out of buffer space. The subscriber 
	// has to free as many events as possible and send an 
	// EventDone message with their IDs to the publisher
	virtual int ReleaseEventsRequest( AliHLTPublisherInterface& publisher ); 

	void StartProcessing()
		{
		fProcessingThread.Start();
		}

	void StopProcessing()
		{
		QuitProcessing();
		}

	bool EventStarted( AliEventID_t eventID );
	//int EventDone( AliEventID_t eventID, void* dummyRemoveWhenDone );
	int EventDone( AliHLTEventDoneData* edd );

	virtual void ProcessSEDD( const AliHLTSubEventDataDescriptor* sedd, vector<BlockData>& dataBlocks );


    protected:

	class AliHLTProcessingThread;
	friend class AliHLTProcessingThread;

	AliHLTPublisherInterface* fPub;


	struct ShmData
	    {
		AliHLTShmID fShmKey;
		AliUInt32_t fShmID;
		void* fShmPtr;
		AliUInt32_t fNonRefCount;
		AliUInt32_t fRefCount;
	    };

	typedef struct ShmData ShmData;

	struct EventData
	    {
		AliHLTSubEventDataDescriptor* fSEDD;
		AliHLTEventTriggerStruct* fETS;
	    };


	AliHLTDescriptorHandler fDescriptors;
	AliHLTSharedMemory fShm;
	vector<ShmData> fShmData;
	pthread_mutex_t fShmMutex;
	AliUInt32_t fMaxShmNoUseCount;

	AliHLTEventTriggerStruct fDefaultETS;

	void* GetShmPtr( AliHLTShmID shmKey );

	AliEventID_t fCurrentEvent;

	class AliHLTProcessingThread: public AliHLTThread
	    {
	    public:

		AliHLTProcessingThread( AliHLTDetectorSubscriber& sub );
		~AliHLTProcessingThread();

	    protected:

		virtual void Run();

		AliHLTDetectorSubscriber& fSub;

	    private:
	    };

	AliHLTProcessingThread fProcessingThread;

	virtual void AbortProcessing();
	virtual void DoProcessing(); // Called by fProcessingThread.Run()
	void QuitProcessing();
	bool ProcessingQuitted()
		{
		return fProcessingQuitted;
		}

	virtual void ProcessEvent( const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* ets ) = 0;

 	class AliHLTEventSemaphore: public AliHLTTimerConditionSem
 	    {
 	    public:
		
		AliHLTEventSemaphore( int slotsCountE2 = -1, bool grow = false ):
		    AliHLTTimerConditionSem( slotsCountE2, grow )
			{
			}

 		void SignalEvent( EventData* event );
		
 		EventData* RemoveEvent( AliEventID_t eventID );
		
 	    private:
 	    private:
 	    };
	
 	AliHLTEventSemaphore fEventSignal;

	pthread_mutex_t fSEDDMutex;

	MLUCAllocCache fEventDataCache;

	bool fQuitProcessing;
	bool fProcessingQuitted;
	bool fProcessingOk;

    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3DETECTORSUBSCRIBER_HPP_
