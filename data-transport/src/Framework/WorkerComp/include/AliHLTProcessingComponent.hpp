/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3PROCESSINGCOMPONENT_HPP_
#define _ALIL3PROCESSINGCOMPONENT_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTProcessingSubscriber.hpp"
#include "AliHLTProcessingRePublisher.hpp"
#include "AliHLTSamplePublisher.hpp"
#include "AliHLTPublisherProxyInterface.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTShmID.h"
#include "MLUCString.hpp"
#include <iostream>
#include <fstream>
#include <sys/ipc.h>
#include <sys/shm.h>

class AliHLTBufferManager;
class AliHLTShmManager;
class AliHLTSharedMemory;
class AliHLTDescriptorHandler;
class AliHLTPipeController;


class AliHLTProcessingComponent
    {
    public:

	AliHLTProcessingComponent( char* name, int argc, char** argv, unsigned long bufferSize, unsigned long minBlockSize );
	virtual ~AliHLTProcessingComponent();

	virtual void Run();

	void NoRePublisher()
		{
		fNoRePublisher = true;
		}

    protected:

	class SubscriptionListenerThread;

	virtual AliHLTProcessingSubscriber* CreateSubscriber() = 0;
	virtual AliHLTProcessingRePublisher* CreateRePublisher();
	virtual AliHLTBufferManager* CreateBufferManager();
	virtual AliHLTShmManager* CreateShmManager();
	virtual AliHLTSharedMemory* CreateSharedMemory();
 	virtual AliHLTDescriptorHandler* CreateInDescriptorHandler();
	virtual AliHLTDescriptorHandler* CreateOutDescriptorHandler();
	virtual AliHLTPublisherProxyInterface* CreatePublisher();
	virtual AliHLTPipeController* CreatePipeController();
	virtual SubscriptionListenerThread* CreateSubscriptionThread();
	virtual const char* ProcessCmdLine( int argc, char** argv, int& ndx );
	virtual bool ProcessCmdLine();
	virtual void SetupLogging();
	virtual void SetupFileLogging();
	virtual void SetupSysLogging();
	virtual void SetupSharedMemory();
	virtual void StartSubscriptionLoop();
	virtual void StopPublishing();
	virtual void SetupComponents();
	virtual void StartProcessing();
	virtual void StopProcessing();

	virtual void PrintUsage( const char* errorStr, int invCmdArgNdx );

	key_t fPub2SubProxyShmKey;
	key_t fSub2PubProxyShmKey;
	unsigned long fProxyShmSize;
	AliHLTProcessingSubscriber* fSubscriber;
	AliHLTProcessingRePublisher* fRePublisher;
	AliHLTPublisherProxyInterface* fPublisher;

	AliHLTBufferManager* fBufferManager;
	AliHLTShmManager* fShmManager;
	AliHLTSharedMemory* fSharedMemory;
 	AliHLTDescriptorHandler* fInDescriptorHandler;
	AliHLTDescriptorHandler* fOutDescriptorHandler;
	AliHLTPipeController* fPipeController;
	SubscriptionListenerThread* fSubscriptionThread;

	AliUInt32_t fOutputShm;
	AliHLTShmID fShmKey;
	unsigned long fBufferSize;
	AliUInt32_t fMaxNoUseCount;
	AliUInt32_t fMinPreBlockCount;
	AliUInt32_t fMaxPreBlockCount;
	AliUInt32_t fMaxPreEventCount;
	int fMaxPreEventCountExp2;
	bool fSendEventDone;
	unsigned long fMinBlockSize;
	AliUInt32_t fPublisherTimeout;
	AliUInt32_t fSubscriberRetryCount;
	AliUInt32_t fSubscriberRetryTime;

	bool fNoRePublisher;

	AliUInt32_t fPipeControllerQuit;

	bool fBenchmark;

	
	MLUCString fPublisherName;
	MLUCString fRePublisherName;
	MLUCString fSubscriberID;
	MLUCString fSubscriberName;

	MLUCString fName;
	int fArgc;
	char** fArgv;

	class SubscriptionListenerThread: public AliHLTThread
	    {
	    public:
		
		SubscriptionListenerThread( AliHLTSamplePublisher* pub  ):
		    fPub( pub )
			{
			fQuitted = true;
			}

		bool HasQuitted()
			{
			return fQuitted;
			}
		
	    protected:
		
		virtual void Run()
			{
			fQuitted = false;
			if ( fPub )
			    fPub->AcceptSubscriptions();
			fQuitted = true;
			}
		
		AliHLTSamplePublisher* fPub;
		bool fQuitted;

	    };

	
	AliHLTFilteredStdoutLogServer *fLogOut;
	AliHLTFileLogServer *fFileLog;
	AliHLTSysLogServer* fSysLog;
	bool fNoStdout;
	bool fNoFileLog;
	bool fDoSysLog;
	int fSysLogFacility;


    };




template<class TSub>
class AliHLTDefaultProcessingComponent: public AliHLTProcessingComponent
    {
    public:

	AliHLTDefaultProcessingComponent( char* name, int argc, char** argv, AliUInt32_t bufferSize, AliUInt32_t minBlockSize ):
	    AliHLTProcessingComponent( name, argc, argv, bufferSize, minBlockSize )
		{
		}

	virtual ~AliHLTDefaultProcessingComponent()
		{
		}

    protected:

	virtual AliHLTProcessingSubscriber* CreateSubscriber()
		{
		return new TSub( fSubscriberName.c_str(), fSendEventDone, fMinBlockSize, fMaxPreEventCountExp2 );
		}
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3PROCESSINGCOMPONENT_HPP_
