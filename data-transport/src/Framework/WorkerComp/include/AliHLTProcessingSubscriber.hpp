/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3PROCESSINGSUBSCRIBER_HPP_
#define _ALIL3PROCESSINGSUBSCRIBER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

//#define EVENT_CHECK_PARANOIA
//#define EVENT_CHECK_PARANOIA_LIMIT 512
// Also: 1


#include "AliHLTSubscriberInterface.hpp"
#include "AliHLTEventTriggerStruct.h"
#include "AliHLTBufferManager.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTShmManager.hpp"
#include "AliHLTThread.hpp"
#include "AliHLTTimer.hpp"
#include "AliHLTStatus.hpp"
#include "AliHLTEventDoneData.h"
#include "AliHLTSubEventDescriptor.hpp"
#include "MLUCAllocCache.hpp"
#include "MLUCObjectCache.hpp"
#include "MLUCDynamicAllocCache.hpp"
#include "MLUCConditionSem.hpp"
#include <vector>
#include <pthread.h>


class AliHLTPublisherInterface;
class AliHLTProcessingRePublisher;
class AliHLTDetectorPublisher;
class AliHLTEventDoneDataWriter;
class AliHLTEventAccounting;

#if 0
#define ALLOW_ARTIFICIAL_EVENT_LOSS
#warning Artificial event loss support enabled. Dangerous for production setups.
#endif

class AliHLTProcessingSubscriber: public AliHLTSubscriberInterface
    {
    public:
	    
// 	struct BlockData
// 	    {
// 		AliHLTShmID fShmKey;
// 		AliUInt32_t fOffset;
// 		AliUInt8_t* fData;
// 		AliUInt32_t fSize;
// 		AliHLTNodeID_t fProducerNode;
// 		AliHLTEventDataType fDataType;
// 		AliHLTEventDataOrigin fDataOrigin;
// 		AliUInt32_t           fDataSpecification;
// 		AliUInt32_t           fByteOrder;
// 	    };

	// Constructor. 
	// The name argument uniquely identifies 
	// AliHLTProcessingSubscriber to publishing services.
	AliHLTProcessingSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize,
				   AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, int eventCountAlloc = -1 );

	// Destructor
	virtual ~AliHLTProcessingSubscriber();

	// This method is called whenever a new wanted 
	// event is available.
	virtual int NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
			      const AliHLTSubEventDataDescriptor& sbevent,
			      const AliHLTEventTriggerStruct& ets );

	// This method is called for a transient subscriber, 
	// when an event has been removed from the list. 
	virtual int EventCanceled( AliHLTPublisherInterface& publisher, AliEventID_t eventID );

	// This method is called for a subscriber when new event done data is 
	// received by a publisher (sent from another subscriber) for an event. 
	virtual int EventDoneData( AliHLTPublisherInterface& publisher, 
				   const AliHLTEventDoneData& 
				   eventDoneData );

	// This method is called when the subscription ends.
	virtual int SubscriptionCanceled( AliHLTPublisherInterface& publisher );

	// This method is called when the publishing 
	// service runs out of buffer space. The subscriber 
	// has to free as many events as possible and send an 
	// EventDone message with their IDs to the publisher
	virtual int ReleaseEventsRequest( AliHLTPublisherInterface& publisher ); 

	// Method used to check alive status of subscriber. 
	// This message has to be answered with 
	// PingAck message within less  than a second.
	virtual int Ping( AliHLTPublisherInterface& publisher );

	// Method used by a subscriber in response to a Ping 
	// received message. The PingAck message has to be 
	// called within less than one second.
	virtual int PingAck( AliHLTPublisherInterface& publisher );

	AliUInt32_t GetPingCount()
		{
		return fPingCount;
		}

	void SetRetryCount( AliUInt32_t maxCount ) // 0 means infinitely
		{
		fMaxRetries = maxCount;
		}
	
	AliUInt32_t GetRetryCount()
		{
		return fMaxRetries;
		}

	void SetForwardMaxRetryEvents( bool retry )
		{
		fForwardMaxRetryEvents = retry;
		}

	void SetRetryTime( AliUInt32_t retryTime_ms ) // milliseconds
		{
		fRetryTime = retryTime_ms;
		}
	
	AliUInt32_t GetRetryTime()
		{
		return fRetryTime;
		}

	void Secondary()
		{
		fSecondary = true;
		}

	void PingPublisher( AliHLTPublisherInterface& pub );

	void SetPublisher( AliHLTPublisherInterface* pub )
		{
		fPublisher = pub;
		}

	void SetRePublisher( AliHLTProcessingRePublisher* republisher )
		{
		fRePublisher = republisher;
		}

	void SetBufferManager( AliHLTBufferManager* bufMan )
		{
		fBufferManager = bufMan;
		}

 	void SetInDescriptorHandler( AliHLTDescriptorHandler* descHand )
 		{
 		fInDescriptorHandler = descHand;
 		}
 
	void SetOutDescriptorHandler( AliHLTDescriptorHandler* descHand )
		{
		fOutDescriptorHandler = descHand;
		}

	void SetOutputSizeParameters( AliUInt32_t minBlockSize, AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor )
		{
		fMinBlockSize = minBlockSize;
		fPerEventFixedSize = perEventFixedSize;
		fPerBlockFixedSize = perBlockFixedSize;
		fSizeFactor = sizeFactor;
		}


	void SetShmManager( AliHLTShmManager* shmMan )
		{
		fShmManager = shmMan;
		}

	void SetAsync()
		{
		fAsync = true;
		}

	void SetEventAccounting( AliHLTEventAccounting* eventAccounting )
		{
		fEventAccounting = eventAccounting;
		}

	virtual void StartProcessing()
		{
		fTimer.Start();
		fTimer.AddTimeout( fStatusRateUpdateTime_ms, &fEventRateTimerCallback, 0 );
		if ( !fMeasurementModeExactRate )
		    {
		    gettimeofday( &fStartTime, NULL );
		    fLastMeasurementTime = fStartTime;
		    fResumeTime = fStartTime;
		    }
		fQuitCleanup = false;
		fCleanupThread.Start();
		fQuitProcessing = false;
		fProcessingThread.Start();
		}

	virtual void StopProcessing()
		{
		fTimer.Stop();
		QuitCleanup();
		QuitProcessing();
		}

	void PauseProcessing();
	void ResumeProcessing();

	void NoRePublishing()
		{
		fNoRePublishing = true;
		}
	
	virtual void ProcessSEDD( const AliHLTSubEventDataDescriptor* sedd, vector<AliHLTSubEventDescriptor::BlockData>& dataBlocks );

	void DoBenchmark()
		{
		fBenchmark = true;
		}
	
	void SetStatusData( AliHLTStatus* status )
		{
		fStatus = status;
		}

	void SetRunNumber( unsigned long runNr )
		{
		fRunNumber = runNr;
		}
	
#ifdef ALLOW_ARTIFICIAL_EVENT_LOSS
	void SetEventLossModulo( unsigned long mod )
		{
		fEventLossModulo = mod;
		}
#endif

	void AliceHLT()
		{
		fAliceHLT = true;
		}

	void RequestTickEvents()
		{
		fProcessTickEvents = true;
		}

	void ForceEventDoneDataForward()
		{
		fForceEventDoneDataForward = true;
		}


	// Called only for non-trivial (fDataWordCount>0) EventDoneData
	virtual void EventDoneDataReceived( const char* subscriberName, AliEventID_t eventID,
					    const AliHLTEventDoneData*, bool forceForward, bool forwarded );

	virtual bool GetAnnouncedEventData( AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
					    AliHLTEventTriggerStruct*& trigger );

	virtual void PublisherEventDone( AliEventID_t eventID, vector<AliHLTEventDoneData*>& eventDoneData );

	virtual void PURGEALLEVENTS();

	virtual void DumpEventMetaData();


    protected:

	struct InBlockData
	    {
		AliEventID_t fEventID;
		AliHLTShmID fShmKey;
	    };

	struct OutBlockData
	    {
		AliEventID_t fEventID;
		AliUInt32_t fBufferNdx;
		AliUInt32_t fOffset;
		AliUInt32_t fSize;
		AliHLTShmID fShmKey;
	    };

	struct EventData
	    {
		EventData():
		    fInBlocks( 2, true ),
		    fOutBlocks( 1, true )
			{
			fSEDD=NULL;
			fETS=NULL;
			fPostponedEDD=NULL;
			}
		
		void ResetCachedObject()
			{
			fInBlocks.Clear();
			fOutBlocks.Clear();
			fSEDD=NULL;
			fETS=NULL;
			fPostponedEDD=NULL;
			}

		AliEventID_t fEventID;
		AliHLTSubEventDataDescriptor* fSEDD;
		AliHLTEventTriggerStruct* fETS;
		MLUCVector<InBlockData> fInBlocks;
		MLUCVector<OutBlockData> fOutBlocks;
		vector<AliHLTSubEventDescriptor::BlockData> fOutputBlocks;
		bool fCanceled;
		bool fActive;
		bool fDone;
		bool fAnnounced;
		bool fForceEmptyForward;
		AliHLTEventDoneData* fPostponedEDD;
	    };
	typedef EventData* PEventData;


	    friend class AliHLTProcessingRePublisher;

	virtual bool ProcessEvent( AliEventID_t eventID, AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, 
				   const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* etsp,
				   AliUInt8_t* outputPtr, AliUInt32_t& size, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, AliHLTEventDoneData*& edd ) = 0;

	virtual void SignalCleanupInEvent( AliEventID_t eventID, vector<AliHLTEventDoneData*>& eventDoneData );
	virtual void SignalCleanupInEvent( AliEventID_t eventID, AliHLTEventDoneData* existingEDD=NULL );
	virtual bool CleanupInEvent( AliHLTEventDoneData* eventDoneData ); // Return if event was cleaned up properly and passed eventDoneData can be released
	//virtual void CleanupOutEvent( AliEventID_t eventID );

	AliHLTEventDoneData* GetEventDoneData( AliEventID_t eventID, AliUInt32_t dataWordCount );
	void FreeEventDoneData( AliHLTEventDoneData* edd );

	AliHLTEventDoneData* GetEventDoneData( AliEventID_t eventID );
	void PutEventDoneData( AliHLTEventDoneData* edd );

	int AnnounceEvent( EventData* ed, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks );
	int AnnounceEvent( AliEventID_t eventID, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks );

	bool fNoRePublishing;

	AliHLTProcessingRePublisher* fRePublisher;
	AliHLTPublisherInterface* fPublisher;
	AliHLTDetectorPublisher* fNextPublisher;

	AliHLTBufferManager* fBufferManager;

 	AliHLTDescriptorHandler* fInDescriptorHandler;
 	pthread_mutex_t fInSEDDMutex;

	AliHLTDescriptorHandler* fOutDescriptorHandler;
	pthread_mutex_t fOutSEDDMutex;

	MLUCDynamicAllocCache fETSCache;

	MLUCDynamicAllocCache fEDDCache;

	AliHLTShmManager* fShmManager;

	AliHLTEventTriggerStruct fDefaultETS;

	bool fSendEventDone; 
	// Actually this variable should be named something like
	// fSendEventDoneWhenFinishedProcessingIncomingEventAndDontWaitForEventDoneFromFollowingSubscriber
	// When this is true, incoming events are freed when they are processed, otherwise they are only
	// freed when the outgoing event has been freed (EventDone received) by the following processing level.

	bool fForwardMaxRetryEvents;

	AliUInt32_t fMinBlockSize;
	AliUInt32_t fPerEventFixedSize;
	AliUInt32_t fPerBlockFixedSize;
	double fSizeFactor;

	AliUInt32_t fPingCount;

	AliEventID_t fCurrentEvent;

	bool fPublisherCurrentEventDone;
	AliEventID_t fPublisherCurrentEvent;
	pthread_mutex_t fPublisherCurrentEventDoneMutex;

	class AliHLTProcessingThread: public AliHLTThread
	    {
	    public:

		AliHLTProcessingThread( AliHLTProcessingSubscriber& sub );
		~AliHLTProcessingThread();

	    protected:

		virtual void Run();

		AliHLTProcessingSubscriber& fSub;

	    private:
	    };

	    friend class AliHLTProcessingThread;

	AliHLTProcessingThread fProcessingThread;

	bool fQuitProcessing;
	bool fProcessingQuitted;
	bool fProcessingOk;

	virtual void AbortProcessing();
	virtual void DoProcessing(); // Called by fProcessingThread.Run()
 	void QuitProcessing();
	bool ProcessingQuitted()
		{
		return fProcessingQuitted;
		}
 	class AliHLTEventSemaphore: public MLUCConditionSem<PEventData>
 	    {
 	    public:
		
		AliHLTEventSemaphore( int slotsCountE2 = -1 ):
		    MLUCConditionSem<PEventData>( slotsCountE2 )
			{
			}
		
 		void SignalEvent( PEventData ed );
		
 		PEventData RemoveEvent( AliEventID_t eventID );

 	    protected:

		static bool FindEventByID( const PEventData& ed, const AliEventID_t& searchData )
			{
			return ed && ed->fEventID == searchData;
			}
 	    private:
 	    };

	//MLUCAllocCache fEventDataCache;
	MLUCObjectCache<EventData> fEventDataCache;

	//MLUCAllocCache fEventDoneCache;
	AliHLTEventDoneData fDefaultEDD;

	MLUCVector<EventData*> fEventData;
	pthread_mutex_t fEventDataMutex;
	
 	AliHLTEventSemaphore fEventSignal;
	MLUCVector<EventData*> fRetryData;
 	//pthread_mutex_t fSignalMutex;
 
	//vector<OutBlockData> fOutBlocks;
//  	MLUCVector<OutBlockData> fOutBlocks;
//  	pthread_mutex_t fOutBlockMutex;

	//vector<InBlockData> fInBlocks;
// 	MLUCVector<InBlockData> fInBlocks;
// 	pthread_mutex_t fInBlockMutex;

	bool fBenchmark;

	class AliHLTCleanupThread: public AliHLTThread
	    {
	    public:

		AliHLTCleanupThread( AliHLTProcessingSubscriber& sub );
		~AliHLTCleanupThread();

	    protected:

		virtual void Run();

		AliHLTProcessingSubscriber& fSub;

	    private:
	    };

	    friend class AliHLTCleanupThread;

	AliHLTCleanupThread fCleanupThread;
	MLUCConditionSem<AliHLTEventDoneData*> fCleanupSignal;

	virtual void DoCleanupLoop();
	virtual void QuitCleanup();

	bool fQuitCleanup;
	bool fCleanupQuitted;

	AliHLTNodeID_t fNodeID;

	struct EventRetryData
	    {
		AliUInt32_t fRetryCount;
		EventData* fEventData;
		AliUInt32_t fTimerID;
		bool fActive;
	    };

	vector<EventRetryData> fEventRetries;
	pthread_mutex_t fEventRetryMutex;
	AliUInt32_t fMaxRetries;
	AliUInt32_t fRetryTime; // in milliseconds
	AliHLTTimer fTimer;

	MLUCVector<AliHLTEventDoneData*> fDoneData;
	pthread_mutex_t fDoneDataMutex;

	class AliHLTRetryCallback: public AliHLTTimerCallback
	    {
	    public:

		AliHLTRetryCallback( AliHLTProcessingSubscriber& sub ):
		    fSub( sub )
			{
			}
		virtual ~AliHLTRetryCallback() {};
		
		virtual void TimerExpired( AliUInt64_t notData, unsigned long priority );

	    protected:

		AliHLTProcessingSubscriber& fSub;

	    };

	    friend class AliHLTRetryCallback;

	AliHLTRetryCallback fRetryCallback;


	AliHLTStatus* fStatus;
	bool fPaused;
	unsigned long fStatusRateUpdateTime_ms;
	struct timeval fStartTime;
	struct timeval fResumeTime;
	struct timeval fLastMeasurementTime;
	AliUInt64_t fLastMeasurementReceiveEventCount;
	AliUInt64_t fLastMeasurementProcessedEventCount;
	AliUInt64_t fLastMeasurementAnnounceEventCount;
	AliUInt64_t fLastMeasurementTotalProcessedInputDataSize;
	AliUInt64_t fLastMeasurementTotalProcessedOutputDataSize;
	AliUInt64_t fStartMeasurementReceiveEventCount;
	AliUInt64_t fStartMeasurementProcessedEventCount;
	AliUInt64_t fStartMeasurementAnnounceEventCount;
	AliUInt64_t fStartMeasurementResumeReceiveEventCount;
	AliUInt64_t fStartMeasurementResumeProcessedEventCount;
	AliUInt64_t fStartMeasurementResumeAnnounceEventCount;
	AliUInt64_t fStartMeasurementTotalProcessedInputDataSize;
	AliUInt64_t fStartMeasurementTotalProcessedOutputDataSize;
	AliUInt64_t fStartMeasurementResumeTotalProcessedInputDataSize;
	AliUInt64_t fStartMeasurementResumeTotalProcessedOutputDataSize;

	bool fMeasurementModeExactRate; // Other option is exact count...
	
	virtual void EventRateTimerExpired();
	class EventRateTimerCallback;
	    friend class EventRateTimerCallback;
	class EventRateTimerCallback: public AliHLTTimerCallback
	    {
	    public:
		EventRateTimerCallback( AliHLTProcessingSubscriber* parent ):
		    fParent( parent )
			{
			}
		virtual ~EventRateTimerCallback() {};

		virtual void TimerExpired( AliUInt64_t, unsigned long )
			{
			fParent->EventRateTimerExpired();
			}

	    protected:
		AliHLTProcessingSubscriber* fParent;

	    };

	EventRateTimerCallback fEventRateTimerCallback;


// 	static bool OutBlockSearchFunc( const OutBlockData& el, uint64 searchData )
// 		{
// 		return el.fEventID == (AliEventID_t)searchData;
// 		}

// 	static bool InBlockSearchFunc( const InBlockData& el, uint64 searchData )
// 		{
// 		return el.fEventID == (AliEventID_t)searchData;
// 		}

	static bool EventDataSearchFunc( const PEventData& ed, const AliEventID_t& searchData )
		{
		return ed && ed->fEventID == searchData;
		}

	typedef AliHLTEventDoneData* PAliHLTEventDoneData;
	static bool DoneDataSearchFunc( const PAliHLTEventDoneData& el, const AliEventID_t& searchData )
		{
		return el && el->fEventID == searchData;
		}


	AliUInt32_t fOldestEventBirth_s;

	void CleanupOldEvents();
	static bool OldEventSearchFunc( const PEventData& ed, const AliUInt32_t& searchData )
		{
		return ed && ed->fSEDD->fEventBirth_s < searchData;
		}

#ifdef EVENT_CHECK_PARANOIA
	AliEventID_t fLastNewEvent;
#endif

	//#ifdef ALLOW_ARTIFICIAL_EVENT_LOSS
	// Activated always, to keep the size of objects constant, independent of this feature...
	unsigned long fEventLossModulo;
	unsigned long long fEventCounter;
	//#endif

	bool fAsync;
	pthread_mutex_t fAsyncAnnounceMutex;

	bool fAliceHLT;

	unsigned long fRunNumber;

	bool fProcessTickEvents;

	bool fBufferWarnings;
	bool fBuffer20PercentWarning;
	bool fBuffer10PercentWarning;

	AliHLTEventAccounting* fEventAccounting;

	bool fParanoidOutputBlockChecks;


	bool fSecondary;

	bool fEventsPurged;

	bool fForceEventDoneDataForward;

	bool fHighWaterMarkActive;
	AliUInt64_t fHighWaterMarkID;
	unsigned long fHighWaterMark;
	unsigned long fLowWaterMark;
	struct timeval fHighWaterMarkSent;

	// Return true if their is watermark data to be sent.
	// structure pointed to by waterMarkEDD must be able to accept/hold 6 more words.
	bool CreateWaterMarkEDD( AliHLTEventDoneData* const waterMarkEDD, AliEventID_t eventID=AliEventID_t(), bool initializeEDD=false ); 

    private:

	void Init( const char* name, bool sendEventDone, int eventCountAlloc = -1 );
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3PROCESSINGSUBSCRIBER_HPP_
