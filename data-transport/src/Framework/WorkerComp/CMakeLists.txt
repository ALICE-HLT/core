INCLUDE_DIRECTORIES(include
    ../Base/include
    ../PubSub/include
    ../SC/include
    ../Trigger/include
    ${CMAKE_CURRENT_BINARY_DIR}/src
    ${CMAKE_CURRENT_BINARY_DIR}/../SC/src
)

ADD_SUBDIRECTORY(src)
ADD_SUBDIRECTORY(apps)

FILE(GLOB INC_FILES include/*.h*)

INSTALL(FILES ${INC_FILES} DESTINATION include/framework)
