/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTProcessingRePublisher.hpp"
#include "AliHLTProcessingSubscriber.hpp"
#include "AliHLTLog.hpp"


AliHLTProcessingRePublisher::AliHLTProcessingRePublisher( const char* name, int eventSlotsPowerOfTwo ):
    AliHLTSamplePublisher( name, eventSlotsPowerOfTwo )
    {
    fSubscriber = NULL;
    CacheEventDoneDataAllocs();
    }

AliHLTProcessingRePublisher::~AliHLTProcessingRePublisher()
    {
    }

void AliHLTProcessingRePublisher::CanceledEvent( AliEventID_t event, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    if ( fSubscriber )
	fSubscriber->PublisherEventDone( event, eventDoneData );
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTProcessingRePublisher::CanceledEvent", "No associated subscriber" )
	    << "No associated subscriber. Unable to pass canceled event 0x" << AliHLTLog::kHex
	    << event << " (" << AliHLTLog::kDec << event << ")." << ENDLOG;
	}
    }


void AliHLTProcessingRePublisher::EventDoneDataReceived( const char* subscriberName, AliEventID_t eventID,
			    const AliHLTEventDoneData* edd, bool forceForward, bool forwarded )
    {
    if ( !edd )
	{
	LOG( AliHLTLog::kInformational, "AliHLTProcessingRePublisher::EventDoneDataReceived", "NULL Event Done Data Received" )
	    << "Received NULL Event Done Data" << ENDLOG;
	return;
	}
    LOG( AliHLTLog::kDebug, "AliHLTProcessingRePublisher::EventDoneDataReceived", "Event Done Data" )
	<< "Received Event Done Data for event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << ") of " << edd->fHeader.fLength << " bytes for " << edd->fDataWordCount 
	<< " data words." << ENDLOG;
    if ( fSubscriber )
	fSubscriber->EventDoneDataReceived( subscriberName, eventID, edd, forceForward, forwarded );
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTProcessingRePublisher::EventDoneDataReceived", "No associated subscriber" )
	    << "No associated subscriber. Unable to pass event done data for event 0x" << AliHLTLog::kHex
	    << eventID << " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
	}
    }


bool AliHLTProcessingRePublisher::GetAnnouncedEventData( AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
							 AliHLTEventTriggerStruct*& trigger )
    {
    if ( fSubscriber )
	return fSubscriber->GetAnnouncedEventData( eventID, sedd, trigger );
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTProcessingRePublisher::GetAnnouncedEventData", "No associated subscriber" )
	    << "No associated subscriber. Unable to get data for already announced event 0x" << AliHLTLog::kHex
	    << eventID << " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
	return false;
	}
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
