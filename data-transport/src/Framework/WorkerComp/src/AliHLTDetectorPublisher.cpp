/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTDetectorPublisher.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "AliHLTBufferManager.hpp"
#include "AliHLTGetNodeID.hpp"
#include "AliHLTSubEventDescriptor.hpp"
#include "AliHLTHLTEventTriggerData.hpp"
#include "AliHLTEventWriter.hpp"
#include "AliHLTEventAccounting.hpp"
#include "AliHLTEventMetaDataOutput.hpp"
#include "MLUCCmdlineParser.hpp"
#include <time.h>


AliHLTDetectorPublisher::AliHLTDetectorPublisher( const char* name, int eventSlotsPowerOfTwo ):
    AliHLTSamplePublisher( name, eventSlotsPowerOfTwo ),
    fETSCache( (eventSlotsPowerOfTwo<=0) ? 1 : eventSlotsPowerOfTwo+1, true, false ),
    fETSs( eventSlotsPowerOfTwo+1 ),
    fEventAnnouncer( *this ),
    fEventRateTimerCallback( this ),
    fMaxEventAge( 0 ),
    fMaxPendingEventCount( 0 ),
    fBufferWarnings(true),
    fBuffer20PercentWarning(false),
    fBuffer10PercentWarning(false)
    {
    pthread_mutex_init( &fSEDDMutex, NULL );
    pthread_mutex_init( &fStatusMutex, NULL );
    pthread_mutex_init( &fETSMutex, NULL );
    pthread_mutex_init( &fMonitorFirstEventOrbitNrMutex, NULL );
    pthread_mutex_init( &fConfigureEventIDMutex, NULL );
    fShmManager = NULL;
    fBufferManager = NULL;
    fDescriptors = NULL;
    fPersistentState = NULL;
    fBenchmark = false;
    fForwardEvents = false;
    //fAnnounceBackoffTime = 10000; // 10 ms;
    fAnnounceBackoffSleepTime = 0; // give up rest of time slice
    fAnnouncePollSleepModulo = 1500;
    fAnnounceBusyPollTime = 5;
    fNodeID = AliHLTGetNodeID();
    fStatus = NULL;
    fPaused = false;
    fStatusRateUpdateTime_ms = 1000; // 1 second
    fStartTime.tv_sec = fStartTime.tv_usec = fResumeTime.tv_sec = fResumeTime.tv_usec = fLastMeasurementTime.tv_sec = fLastMeasurementTime.tv_usec = 0;
    fMeasurementModeExactRate = true;
    fLastMeasurementAnnounceEventCount = 0;
    fLastMeasurementAnnounceEventCount = 0;
    fLastMeasurementTotalProcessedOutputDataSize = 0;
    fStartMeasurementAnnounceEventCount = 0;
    fStartMeasurementResumeAnnounceEventCount = 0;
    fStartMeasurementTotalProcessedOutputDataSize = 0;
    fStartMeasurementResumeTotalProcessedOutputDataSize = 0;
    fStarted = false;

#ifdef INITIAL_CONDITIONSEM_UNLOCK
    // Define this when condition semaphore objects should always be unlocked initially.
    fPauseSignal.Unlock();
#endif
    CacheEventDoneDataAllocs();

    fAliceHLT = false;

    fLastTickEvent = 0;
    fTickInterval_us = 0;

    fRunNumber = 0;
    fBeamType = 0;
    fHLTMode = 0;
    fRunType = "";

// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker shmIDLock( fShmIDMutex );
    fSORShmID=(AliUInt32_t)-1;
    fEORShmID=(AliUInt32_t)-1;
    shmIDLock.Unlock();

    fDDLListData = NULL;
#ifdef USE_DDL_LIST_SHM
    fDDLListBlockSet = false;
#endif

    fStartTimestamp = 0;
    }

AliHLTDetectorPublisher::~AliHLTDetectorPublisher()
    {
    vector<AliUInt64_t> dummyNotificationData;
    fTimer.CancelTimeouts( &fEventRateTimerCallback, dummyNotificationData );
    pthread_mutex_destroy( &fSEDDMutex );
    pthread_mutex_destroy( &fStatusMutex );
    pthread_mutex_destroy( &fETSMutex );
    pthread_mutex_destroy( &fMonitorFirstEventOrbitNrMutex );
    pthread_mutex_destroy( &fConfigureEventIDMutex );
    }

void AliHLTDetectorPublisher::UpdateEventAge()
    {
    unsigned long oldestEvent_s;
    if ( fMaxEventAge )
	{
	struct timeval now;
	gettimeofday( &now, NULL );
	if ( (unsigned long)now.tv_sec > fMaxEventAge )
	    oldestEvent_s = now.tv_sec - fMaxEventAge;
	else
	    oldestEvent_s = 0;
	CleanupOldEvents( oldestEvent_s );
	}
    }


void AliHLTDetectorPublisher::StartAnnouncing()
    {
    if ( !fShmManager )
	{
	LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::StartAnnouncing", "No Shm Manager" )
	    << "No shared memory manager specified. Aborting..." << ENDLOG;
	return;
	}
    if ( !fBufferManager )
	{
	LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::StartAnnouncing", "No Buffer Manager" )
	    << "No buffer manager specified. Aborting..." << ENDLOG;
	return;
	}
    if ( !fDescriptors && !fForwardEvents )
	{
	LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::StartAnnouncing", "No Descriptor Handler" )
	    << "No descriptor handler specified. Aborting..." << ENDLOG;
	return;
	}
    fEventAnnouncer.Start();
    fTimer.AddTimeout( fStatusRateUpdateTime_ms, &fEventRateTimerCallback, 0 );
    gettimeofday( &fStartTime, NULL );
    fLastMeasurementTime = fStartTime;
    fResumeTime = fStartTime;
    }

void AliHLTDetectorPublisher::StopAnnouncing()
    {
    fEventAnnouncer.Quit();
    }


AliHLTDetectorPublisher::operator bool()
    {
    return ( fForwardEvents || (bool)(fDescriptors) );//  && (fShmPtr!=NULL);
    }

void AliHLTDetectorPublisher::PauseProcessing()
    {
    fPaused = true;
    }

void AliHLTDetectorPublisher::ResumeProcessing()
    {
    fPaused = false;
    gettimeofday( &fResumeTime, NULL );
    fPauseSignal.Signal();
    }

void AliHLTDetectorPublisher::AddConfigureEvent( AliUInt32_t eventType, AliUInt64_t eventNr, char const* data )
    {
    ConfigureEventData ced;
    ced.fEventType = eventType;
    ced.fEventNr = eventNr;
    ced.fData = data;
    ced.fShmID = ~(AliUInt32_t)0;
    pthread_mutex_lock( &fConfigureEventIDMutex );
    fConfigureEventIDs.push_back( ced );
    pthread_mutex_unlock( &fConfigureEventIDMutex );
    }

void AliHLTDetectorPublisher::DumpEvents()
    {
    LOG( AliHLTLog::kDebug, "AliHLTDetectorPublisher::DumpEvent", "Event dump" )
	<< "Dumping all current events to file." << ENDLOG;
    
    // We need to get the list of all current pending events, fetch their sub event descriptors,
    // dereference the shared memory pointers to all the data blocks and then write the
    // blocks to disk.
    
    vector<AliEventID_t> events;
    GetPendingEvents(events);
    
    vector<AliEventID_t>::const_iterator event;
    for (event = events.begin(); event != events.end(); event++)
        {
        AliHLTSubEventDataDescriptor* sedd;
        AliHLTEventTriggerStruct* trigger;
        if ( ! GetAnnouncedEventData(*event, sedd, trigger) )
            {
            LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::DumpEvent", "No event")
                << "Could not find the event: 0x" << AliHLTLog::kHex << event->fNr << "/" << event->fType
		<< " (" << AliHLTLog::kDec << event->fNr << "/" << event->fType << ")." << ENDLOG;
            continue;
            }
        vector<AliHLTSubEventDescriptor::BlockData> dataBlocks;
        AliHLTSubEventDescriptor desc(sedd);
        desc.Dereference(fShmManager, dataBlocks);
        
        AliHLTEventWriter::WriteEvent(*event, dataBlocks, sedd, trigger, fRunNumber, GetName());
        }
    
    if (events.size() == 0)
        {
        LOG( AliHLTLog::kDebug, "AliHLTDetectorPublisher::DumpEvent", "Nothing to dump" )
	    << "There were no events found to be dumped to disk" << ENDLOG;
	}
    }

void AliHLTDetectorPublisher::DumpEventMetaData()
    {
    LOG( AliHLTLog::kDebug, "AliHLTDetectorPublisher::DumpEventMetaData", "Event meta data dump" )
	<< "Dumping all current events to file." << ENDLOG;
    
    // We need to get the list of all current pending events, fetch their sub event descriptors,
    // dereference the shared memory pointers to all the data blocks and then write the
    // blocks to disk.

    AliHLTEventMetaDataOutput metaDataOutput( GetName(), fAliceHLT, fRunNumber );

    if ( fAliceHLT )
	metaDataOutput.SetEventTriggerDataFormatFunction(  AliHLTHLEventTriggerData2String );

    int ret = metaDataOutput.Open();
    if ( ret!=0 )
	{
	LOG( AliHLTLog::kWarning, "AliHLTDetectorPublisher::DumpEventMetaData", "Error opening event meta data output file" )
	    << "Error opening event meta data output file: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return;
	}
    
    vector<AliEventID_t> events;
    GetPendingEvents(events);
    
    vector<AliEventID_t>::const_iterator event;
    for (event = events.begin(); event != events.end(); event++)
        {
        AliHLTSubEventDataDescriptor* sedd;
        AliHLTEventTriggerStruct* trigger;
        if ( ! GetAnnouncedEventData(*event, sedd, trigger) )
            {
            LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::DumpEventMetaData", "No event")
                << "Could not find the event: 0x" << AliHLTLog::kHex << event->fNr << "/" << event->fType
		<< " (" << AliHLTLog::kDec << event->fNr << "/" << event->fType << ")." << ENDLOG;
            continue;
            }

	metaDataOutput.WriteEvent( *event, sedd, trigger, "announced" );
        }

    metaDataOutput.Close();
    
    if (events.size() == 0)
        {
        LOG( AliHLTLog::kDebug, "AliHLTDetectorPublisher::DumpEventMetaData", "Nothing to dump" )
	    << "There were no events found to be dumped to disk" << ENDLOG;
	}
    }


void AliHLTDetectorPublisher::PURGEALLEVENTS()
    {
    }


void AliHLTDetectorPublisher::EventRateTimerExpired()
    {
    if ( !fStatus )
	return;
    pthread_mutex_lock( &fStatusMutex );
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker stateLock( fStatus->fStatusDataMutex );
    unsigned long long curAnnCount = fStatus->fHLTStatus.fAnnouncedEventCount;
    unsigned long long curOutputData = fStatus->fHLTStatus.fTotalProcessedOutputDataSize;
    struct timeval now;
    double tdiff, avgtdiff, resumetdiff;

    if ( fMeasurementModeExactRate )
	{
	if ( curAnnCount>0 && !fStarted )
	    {
	    fStarted = true;
	    fLastMeasurementTime = fStartTime;
	    fStartMeasurementResumeAnnounceEventCount = fStartMeasurementAnnounceEventCount = curAnnCount;
	    fStartMeasurementResumeTotalProcessedOutputDataSize = fStartMeasurementTotalProcessedOutputDataSize = curOutputData;
	    LOG( AliHLTLog::kBenchmark, "AliHLTDetectorPublisher::EventRateTimerExpired", "Event Rate Results" )
		<< "Discarding from rate measurements: "
		<< curAnnCount << " announced events - "
		<< curOutputData << " announced bytes."
		<< ENDLOG;
	    //return;
	    }
	curAnnCount -= fStartMeasurementAnnounceEventCount;
	curOutputData -= fStartMeasurementTotalProcessedOutputDataSize;
	}

    
    gettimeofday( &now, NULL );
    tdiff = now.tv_sec;
    tdiff -= fLastMeasurementTime.tv_sec;
    tdiff *=1000000;
    tdiff += now.tv_usec;
    tdiff -= fLastMeasurementTime.tv_usec;
    avgtdiff = now.tv_sec;
    avgtdiff -= fStartTime.tv_sec;
    avgtdiff *=1000000;
    avgtdiff += now.tv_usec;
    avgtdiff -= fStartTime.tv_usec;
    resumetdiff = now.tv_sec;
    resumetdiff -= fResumeTime.tv_sec;
    resumetdiff *=1000000;
    resumetdiff += now.tv_usec;
    resumetdiff -= fResumeTime.tv_usec;

    fStatus->fHLTStatus.fCurrentAnnounceRate = (((double)(curAnnCount-fLastMeasurementAnnounceEventCount))/tdiff)*1000000.0;
    fStatus->fHLTStatus.fAvgAnnounceRate = (((double)curAnnCount)/avgtdiff)*1000000.0;
    fStatus->fHLTStatus.fResumedAvgAnnounceRate = (((double)(curAnnCount))/resumetdiff)*1000000.0;
    fStatus->fHLTStatus.fCurrentAnnouncedEventCount = curAnnCount-fLastMeasurementAnnounceEventCount;

    fStatus->fHLTStatus.fCurrentProcessedOutputDataRate = (((double)(curOutputData-fLastMeasurementTotalProcessedOutputDataSize))/tdiff)*1000000.0;
    fStatus->fHLTStatus.fAvgProcessedOutputDataRate = (((double)curOutputData)/avgtdiff)*1000000.0;
    fStatus->fHLTStatus.fResumedProcessedOutputDataRate = (((double)(curOutputData-fStartMeasurementResumeTotalProcessedOutputDataSize))/resumetdiff)*1000000.0;
    fStatus->fHLTStatus.fCurrentProcessedOutputDataSize = curOutputData-fLastMeasurementTotalProcessedOutputDataSize;

    LOG( AliHLTLog::kBenchmark, "AliHLTDetectorPublisher::EventRateTimerExpired", "Event Rate Results" )
	<< "Current Event Announce Rate during last " << AliHLTLog::kDec << tdiff << " milliseconds ("
	<< curAnnCount-fLastMeasurementAnnounceEventCount << " events): "
	<< fStatus->fHLTStatus.fCurrentAnnounceRate << " Hz == " 
	<< (fStatus->fHLTStatus.fCurrentAnnounceRate/1000.0) << " kHz == " 
	<< (fStatus->fHLTStatus.fCurrentAnnounceRate/1000000.0) << " MHz."
	<< ENDLOG;
    LOG( AliHLTLog::kBenchmark, "AliHLTDetectorPublisher::EventRateTimerExpired", "Event Rate Results" )
	<< "Total Global Event Announce Rate after " << AliHLTLog::kDec << (curAnnCount+fStartMeasurementAnnounceEventCount) << " events: "
	<< fStatus->fHLTStatus.fAvgAnnounceRate << " Hz == " 
	<< (fStatus->fHLTStatus.fAvgAnnounceRate/1000.0) << " kHz == " 
	<< (fStatus->fHLTStatus.fAvgAnnounceRate/1000000.0) << " MHz."
	<< ENDLOG;
    LOG( AliHLTLog::kBenchmark, "AliHLTDetectorPublisher::EventRateTimerExpired", "Event Rate Results" )
	<< "Global Event Announce Rate since last resume after " << AliHLTLog::kDec << (curAnnCount+fStartMeasurementAnnounceEventCount) << " events: "
	<< fStatus->fHLTStatus.fResumedAvgAnnounceRate << " Hz == " 
	<< (fStatus->fHLTStatus.fResumedAvgAnnounceRate/1000.0) << " kHz == " 
	<< (fStatus->fHLTStatus.fResumedAvgAnnounceRate/1000000.0) << " MHz."
	<< ENDLOG;

    fLastMeasurementTime = now;
    fLastMeasurementAnnounceEventCount = curAnnCount;
    fLastMeasurementTotalProcessedOutputDataSize = curOutputData;

    pthread_mutex_unlock( &fStatusMutex );
    fTimer.AddTimeout( fStatusRateUpdateTime_ms, &fEventRateTimerCallback, 0 );
    }




void AliHLTDetectorPublisher::CanceledEvent( AliEventID_t event, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    LOG( AliHLTLog::kDebug, "AliHLTDetectorPublisher::CanceledEvent", "Event finished" )
	<< "Finished proccesing event 0x" << AliHLTLog::kHex << event << " (" << AliHLTLog::kDec
	<< event << ")." << ENDLOG;
    if ( fStatus && GetPendingEventCount()<=0 )
	{
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
	}
    if ( event.fType != kAliEventTypeTick && event.fType != kAliEventTypeReconfigure && event.fType != kAliEventTypeDCSUpdate )
	{
	unsigned long ndx;
	if ( fDescriptors )
	    {
	    pthread_mutex_lock( &fSEDDMutex );
	    if ( fDescriptors->FindSEDD( event, ndx ) )
		{
		AliHLTSubEventDataDescriptor* sedd;
		sedd = fDescriptors->GetEventDescriptor( ndx );
		pthread_mutex_unlock( &fSEDDMutex );
		EventFinished( event, *sedd, eventDoneData );
		pthread_mutex_lock( &fSEDDMutex );
		fDescriptors->ReleaseEventDescriptor( event );
		}
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::CanceledEvent", "Event finished" )
		    << "Attempting to cancel non-belonging event 0x" << AliHLTLog::kHex << event 
		    << " (" << AliHLTLog::kDec << event << ")..:" << ENDLOG;
		pthread_mutex_unlock( &fSEDDMutex );
		EventFinished( event, eventDoneData );
		pthread_mutex_lock( &fSEDDMutex );	
		}
	    pthread_mutex_unlock( &fSEDDMutex );
	    }
	else
	    {
	    if ( !fForwardEvents )
		{
		LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::CanceledEvent", "No Descriptor Handler" )
		    << "No descriptor handler has been specified." << ENDLOG;
		}
	    EventFinished( event, eventDoneData );
	    }
	AliHLTEventTriggerStruct* ets = NULL;
	pthread_mutex_lock( &fETSMutex );
	if ( MLUCVectorSearcher<ETSData,AliEventID_t>::FindElement( fETSs, EventSearchFunc, event, ndx ) )
	    {
	    ets = fETSs.GetPtr( ndx )->fETS;
	    fETSs.Remove( ndx );
	    }
	pthread_mutex_unlock( &fETSMutex );
	//     LOG( AliHLTLog::kDebug, "AliHLTDetectorPublisher::CanceledEvent", "ETS to release" )
	// 	<< "Releasing EventTriggerStruct for event 0x" << AliHLTLog::kHex << event 
	// 	<< " (" << AliHLTLog::kDec << event << "): x0" << MLUCLog::kHex
	// 	<< (unsigned long)ets << "." << ENDLOG;
	if ( ets )
	    fETSCache.Release( (uint8*)ets );
	else
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTDetectorPublisher::CanceledEvent", "Event finished" )
		<< "Unable to find trigger data for event 0x" << AliHLTLog::kHex << event 
		<< " (" << AliHLTLog::kDec << event << ")..:" << ENDLOG;
	    }
	if ( fStatus )
	    {
	    pthread_mutex_lock( &fStatusMutex );
// XXXX TODO LOCKING XXXX
	    MLUCMutex::TLocker stateLock( fStatus->fStatusDataMutex );
	    fStatus->fHLTStatus.fPendingOutputEventCount--;
	    if ( fBufferManager )
		fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
	    pthread_mutex_unlock( &fStatusMutex );
	    }
	}
    else if (  event.fType == kAliEventTypeReconfigure || event.fType == kAliEventTypeDCSUpdate )
	{
	pthread_mutex_lock( &fConfigureEventIDMutex );
	vector<ConfigureEventData>::iterator iter = fSentConfigureEventIDs.begin(), end = fSentConfigureEventIDs.end();
	while ( iter != end )
	    {
	    if ( iter->fEventNr == event.fNr )
		{
		if ( iter->fShmID!=(AliUInt32_t)-1 )
		    {
		    fShm->ReleaseShm( iter->fShmID );
		    }
		fSentConfigureEventIDs.erase( iter );
		break;
		}
	    iter++;
	    }
	pthread_mutex_unlock( &fConfigureEventIDMutex );
	}
    AliHLTSamplePublisher::CanceledEvent( event, eventDoneData );
    }

void AliHLTDetectorPublisher::AnnouncedEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, 
					      const AliHLTEventTriggerStruct& trigger, AliUInt32_t refCount )
    {
    if ( fStatus )
	{
	struct timeval t1;
	AliUInt64_t tm;
	gettimeofday( &t1, NULL );
	tm = t1.tv_sec;
	tm *= 1000000;
	tm += t1.tv_usec;
	pthread_mutex_lock( &fStatusMutex );
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker stateLock( fStatus->fStatusDataMutex );
	if ( eventID.fType != kAliEventTypeTick && eventID.fType != kAliEventTypeReconfigure && eventID.fType != kAliEventTypeDCSUpdate )
	    {
	    fStatus->fHLTStatus.fAnnouncedEventCount++;
	    fStatus->fHLTStatus.fPendingOutputEventCount++;
	    }
	fStatus->fHLTStatus.fLastDataEvent = AliEventID_t().fNr;
	fStatus->fHLTStatus.fLastEventStart = fStatus->fHLTStatus.fCurrentEventStart;
	fStatus->fHLTStatus.fLastEventEnd = tm;
	fStatus->fHLTStatus.fLastDataEvent = eventID.fNr;
	if ( fBufferManager )
	    {
	    fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
	    }
	pthread_mutex_unlock( &fStatusMutex );
	}
    AliHLTSamplePublisher::AnnouncedEvent( eventID, sbevent, trigger, refCount );
    }

void AliHLTDetectorPublisher::EventDoneDataReceived( const char* /*subscriberName*/, AliEventID_t eventID,
						     const AliHLTEventDoneData* edd, bool forceForward, bool forwarded )
    {
    if ( fEventAccounting )
	fEventAccounting->EventFinished( fName.c_str(), eventID, forceForward ? 2 : 1, forwarded ? 2 : 1 );
    unsigned long ii = 0;
    while ( ii < edd->fDataWordCount )
	{
	switch ( (AliHLTEventDoneDataCommands)(edd->fDataWords[ii]) )
	    {
	    case kAliHLTEventDoneNOPCmd:
		ii++;
		break;
	    case kAliHLTEventDoneMonitorReplayEventIDCmd:
		{
		if ( ii+2>=edd->fDataWordCount )
		    {
		    LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::EventDoneDataReceived", "Inconsistent EventDoneData Format" )
			<< "Inconsistencies detetected in EventDoneData: " << AliHLTLog::kDec
			<< "Current Index: " << ii << " - Data Word Count: " << edd->fDataWordCount
			<< " - Command: Replay Event ID." << ENDLOG;
		    break;
		    }
		AliEventID_t eventID;
		AliHLTSubEventDataDescriptor* seddOrig=NULL;
		AliHLTSubEventDataDescriptor* sedd=NULL;
		AliHLTEventTriggerStruct* trigger=NULL;
		eventID.fNr = edd->fDataWords[ii+1] | ( ((AliUInt64_t)edd->fDataWords[ii+2]) << 32 );
		eventID.fType = kAliEventTypeData;
		if ( GetEventData( eventID, seddOrig, trigger ) )
		    {
		    eventID.fType = kAliEventTypeDataReplay;
		    sedd = (AliHLTSubEventDataDescriptor*) new uint8[ seddOrig->fHeader.fLength ];
		    if ( !sedd )
			{
			LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::EventDoneDataReceived", "Out Of Memory" )
			    << "Out of memory trying to allocate SEDD for replay event " << AliHLTLog::kDec
			    << eventID << " (0x" << AliHLTLog::kHex << eventID
			    << ") of " << AliHLTLog::kDec << seddOrig->fHeader.fLength
			    << " bytes." << ENDLOG;
			    
			}
		    else
			{
			
			memcpy( sedd, seddOrig, seddOrig->fHeader.fLength );
 			sedd->fEventID = eventID;
			sedd->fStatusFlags |= ALIHLTSUBEVENTDATADESCRIPTOR_FULLPASSTHROUGH|ALIHLTSUBEVENTDATADESCRIPTOR_MONITOR;
			LOG( AliHLTLog::kDebug, "AliHLTDetectorPublisher::EventDoneDataReceived", "Replay Event" )
			    << "Replay event 0x" << AliHLTLog::kHex << eventID
			    << " (" << AliHLTLog::kDec
			    << eventID << ") requested." << ENDLOG;
			AnnounceEvent( eventID, *sedd, *trigger );
			delete [] sedd;
			sedd = NULL;
			}
		    }
		else
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTDetectorPublisher::EventDoneDataReceived", "Cannot Find Requested Replay Event" )
			<< "Event " << AliHLTLog::kDec << eventID << " (0x" << AliHLTLog::kHex << eventID
			<< ") requested for replay cannot be found." << ENDLOG;
		    }
		ii += 3;
		break;
		}
	    case kAliHLTEventDoneMonitorFirstOrbitEventCmd:
		{
		if ( ii+2>=edd->fDataWordCount )
		    {
		    LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::EventDoneDataReceived", "Inconsistent EventDoneData Format" )
			<< "Inconsistencies detetected in EventDoneData: " << AliHLTLog::kDec
			<< "Current Index: " << ii << " - Data Word Count: " << edd->fDataWordCount
			<< " - Command: Monitor First Orbit Event." << ENDLOG;
		    break;
		    }
		AliUInt64_t orbitID = edd->fDataWords[ii+1] | ( ((AliUInt64_t)edd->fDataWords[ii+2]) << 32 );
#if 0
		if ( fAliceHLT )
		    orbitID &= 0xFFFFFFFFFFFFF000ULL;
#endif
		pthread_mutex_lock( &fMonitorFirstEventOrbitNrMutex );
		fMonitorFirstEventOrbitNrs.push_back( orbitID );
		pthread_mutex_unlock( &fMonitorFirstEventOrbitNrMutex );
		LOG( AliHLTLog::kDebug, "AliHLTDetectorPublisher::EventDoneDataReceived", "Monitor First Orbit Event" )
		    << "Monitor first orbit event 0x" << AliHLTLog::kHex << orbitID
		    << " (" << AliHLTLog::kDec
		    << orbitID << ") requested." << ENDLOG;
		ii += 3;
		break;
		}
	    case kAliHLTEventDoneReadoutListCmd: // Fallthrough
	    case kAliHLTEventDoneMonitorListCmd:
		if ( ii+edd->fDataWords[ii+1]*4+1>=edd->fDataWordCount )
		    {
		    LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::EventDoneDataReceived", "Inconsistent EventDoneData Format" )
			<< "Inconsistencies detetected in EventDoneData: " << AliHLTLog::kDec
			<< "Current Index: " << ii << " - Data Word Count: " << edd->fDataWordCount
			<< " - Command: Readout List." << ENDLOG;
		    break;
		    }
		// Nothing to do in source components for this command, currently.
#if 0
		LOG( AliHLTLog::kWarning, "AliHLTDetectorPublisher::EventDoneDataReceived", "EventDoneReadoutListCmd Not Yet Implemented" )
		    << "EventDone Readout List Command not yet implemented..." << ENDLOG;
		LOG( AliHLTLog::kDebug, "AliHLTDetectorPublisher::EventDoneDataReceived", "EventDoneReadoutListCmd Not Yet Implemented" )
		    << "EventDone Readout List Command not yet implemented..." 
		    << " (" << AliHLTLog::kDec << edd->fDataWords[ii+1] << " count specifier -> " << edd->fDataWords[ii+1]*4+2
		    << " total data words." << ENDLOG;
#endif
		ii += edd->fDataWords[ii+1]*4+2;
		break;
	    case kAliHLTEventDoneFlagMonitorEventCmd:
		ii += 1;
		break;
	    case kAliHLTEventDoneDebugCmd:
		if ( ii+edd->fDataWords[ii+1]+1>=edd->fDataWordCount )
		    {
		    LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::EventDoneDataReceived", "Inconsistent EventDoneData Format" )
			<< "Inconsistencies detetected in EventDoneData: " << AliHLTLog::kDec
			<< "Current Index: " << ii << " - Data Word Count: " << edd->fDataWordCount
			<< " - Command: Readout List." << ENDLOG;
		    break;
		    }
		for ( unsigned nn=0; nn<edd->fDataWords[ii+1]; nn++ )
		    LOG( AliHLTLog::kDebug, "AliHLTDetectorPublisher::EventDoneDataReceived", "Debug event done data" )
			<< AliHLTLog::kDec << "Debug event done data word " << nn << ": " << edd->fDataWords[ii+1+nn]
			<< " - 0x" << AliHLTLog::kHex << edd->fDataWords[ii+1+nn] << ENDLOG;
		ii += edd->fDataWords[ii+1]+2;
		break;
	    case kAliHLTEventDoneBackPressureHighWaterMark:
		ii += 3;
		break;
	    case kAliHLTEventDoneBackPressureLowWaterMark:
		ii += 3;
		break;
	    default:
		LOG( AliHLTLog::kWarning, "AliHLTDetectorPublisher::EventDoneDataReceived", "Unknown EventDoneData Command Word" )
		    << "Received unknown EventDoneData command word at index " << AliHLTLog::kDec << ii << ": "
		    << edd->fDataWords[ii] << " (0x" << AliHLTLog::kHex
		    << edd->fDataWords[ii] << ")." << ENDLOG;
		ii++;
		break;
	    }
	}
    }


bool AliHLTDetectorPublisher::GetAnnouncedEventData( AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
				      AliHLTEventTriggerStruct*& trigger )
    {
    unsigned long ndx;
    trigger = NULL;
    sedd = NULL;
    if ( fDescriptors )
	{
	pthread_mutex_lock( &fSEDDMutex );
	if ( fDescriptors->FindSEDD( eventID, ndx ) )
	    sedd = fDescriptors->GetEventDescriptor( ndx );
	pthread_mutex_unlock( &fSEDDMutex );
	}
    pthread_mutex_lock( &fETSMutex );
    if ( MLUCVectorSearcher<ETSData,AliEventID_t>::FindElement( fETSs, EventSearchFunc, eventID, ndx ) )
	trigger = fETSs.GetPtr( ndx )->fETS;
    pthread_mutex_unlock( &fETSMutex );
    if ( sedd && trigger )
	return true;
    sedd = NULL;
    trigger = NULL;
    return false;
    }


AliHLTDetectorPublisher::AliHLTDetectorEventAnnouncer::AliHLTDetectorEventAnnouncer( AliHLTDetectorPublisher& pub ):
    fPub( pub )
    {
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker quitLock( fQuitMutex );
    fQuit = false;
    fQuitted = false;
    }

void AliHLTDetectorPublisher::AliHLTDetectorEventAnnouncer::Quit()
    {
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker quitLock( fQuitMutex );
    fQuit = true;
    quitLock.Unlock();
    fPub.QuitEventLoop();
    fPub.fPauseSignal.Signal();
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 5000000;
    gettimeofday( &start, NULL );
    struct timespec ts;
    quitLock.Lock();
    while ( !fQuitted && deltaT<timeLimit )
	{
	quitLock.Unlock();
	ts.tv_sec = 0;
	ts.tv_nsec = 50000;
	nanosleep( &ts, NULL );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	quitLock.Lock();
	}
    }

bool AliHLTDetectorPublisher::AliHLTDetectorEventAnnouncer::HasQuitted()
    {
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker quitLock( fQuitMutex );
    bool quit = fQuitted;
    return quit;
    }

void AliHLTDetectorPublisher::AliHLTDetectorEventAnnouncer::Run()
    {
    int ret;
    unsigned long tmpCount;
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker quitLock( fQuitMutex );
    fQuit = fQuitted = false;
    quitLock.Unlock();
    AliEventID_t eventID;
    AliHLTSubEventDataDescriptor* sbevent;
    AliHLTEventTriggerStruct* trg;
    fPub.StartEventLoop();

    //#define DO_BENCHMARK 
#ifdef DO_BENCHMARK
    struct timeval t1, t2, t3, t4;
    AliUInt32_t dt;
    AliUInt32_t oldLog;
#endif

    if ( fPub.fStatus )
	{
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fPub.fStatus->fStatusDataMutex );
	MLUCMutex::TLocker stateLock( fPub.fStatus->fStateMutex );
	fPub.fStatus->fProcessStatus.fState |= kAliHLTPCRunningStateFlag;
	fPub.fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	}

#ifdef DO_BENCHMARK
    if ( fPub.fBenchmark )
	gettimeofday( &t1, NULL );
#endif
    //quitLock.Lock();
    //while ( (fPub.fPaused || ( fPub.fMaxPendingEventCount>0 && fPub.GetPendingEventCount()>=fPub.fMaxPendingEventCount ) || fPub.WaitForEvent( eventID, sbevent, trg ) ) && (!fQuit || fPub.MustContinue()) )
    while ( (fPub.fPaused || ( fPub.fMaxPendingEventCount>0 && fPub.GetPendingEventCount()>=fPub.fMaxPendingEventCount && !fPub.MustContinue() ) || fPub.WaitForEvent( eventID, sbevent, trg ) ) )
	{
#if 0
	if ( fQuit )
	    {
#define LOGFLAG(flag) << " " << #flag << ": " << ( (flag) ? "true" : "false" )
	LOG( AliHLTLog::kWarning, "AliHLTDetectorPublisher::AliHLTDetectorEventAnnouncer::Run", "Flag debugging" )
	    << "Flag debugging: "
	    LOGFLAG(fQuit)
	    LOGFLAG(fPub.fPaused)
	    LOGFLAG((fPub.fMaxPendingEventCount>0))
	    LOGFLAG((fPub.GetPendingEventCount()>=fPub.fMaxPendingEventCount))
	    LOGFLAG((fPub.MustContinue()))
	    LOGFLAG((bool)sbevent)
	    LOGFLAG((bool)trg)
	    << ENDLOG;
	    }
#endif
	quitLock.Lock();
	if ( fQuit )
	    {
	    quitLock.Unlock();
	    if ( !fPub.MustContinue() )
		break;
	    }
	else
	    quitLock.Unlock();
	if ( fPub.fStatus && sbevent && trg )
	    {
// XXXX TODO LOCKING XXXX
	    MLUCMutex::TLocker statusDataLock( fPub.fStatus->fStatusDataMutex );
	    MLUCMutex::TLocker stateLock( fPub.fStatus->fStateMutex );
	    fPub.fStatus->fProcessStatus.fState |= kAliHLTPCBusyStateFlag;
	    }
	if ( sbevent && trg && fPub.fMaxEventAge )
	    {
	    struct timeval now;
	    gettimeofday( &now, NULL );
	    if ( fPub.fMaxEventAge && (unsigned long)now.tv_sec > fPub.fMaxEventAge )
		sbevent->fOldestEventBirth_s = now.tv_sec - fPub.fMaxEventAge;
	    else
		sbevent->fOldestEventBirth_s = 0;
	    }
	quitLock.Lock();
	while ( (fPub.fPaused || ( fPub.fMaxPendingEventCount>0 && fPub.GetPendingEventCount()>=fPub.fMaxPendingEventCount && !fPub.MustContinue() ) ) && (!fQuit /* || fPub.MustContinue()*/) && !sbevent && !trg )
	    {
	    quitLock.Unlock();
	    if ( fPub.fStatus && fPub.fPaused )
		{
// XXXX TODO LOCKING XXXX
		MLUCMutex::TLocker statusDataLock( fPub.fStatus->fStatusDataMutex );
		MLUCMutex::TLocker stateLock( fPub.fStatus->fStateMutex );
		fPub.fStatus->fProcessStatus.fState |= kAliHLTPCPausedStateFlag;
		}
	    fPub.fPauseSignal.Lock();
	    fPub.fPauseSignal.Wait( 500 );
	    fPub.fPauseSignal.Unlock();
	    if ( fPub.fStatus && !fPub.fPaused )
		{
// XXXX TODO LOCKING XXXX
		MLUCMutex::TLocker statusDataLock( fPub.fStatus->fStatusDataMutex );
		MLUCMutex::TLocker stateLock( fPub.fStatus->fStateMutex );
		fPub.fStatus->fProcessStatus.fState &= ~kAliHLTPCPausedStateFlag;
		}
	    quitLock.Lock();
	    }
	quitLock.Unlock();


#ifdef DO_BENCHMARK
	if ( fPub.fBenchmark )
	    {
	    gettimeofday( &t2, NULL );
	    dt = (t2.tv_sec-t1.tv_sec)*1000000+(t2.tv_usec-t1.tv_usec);
	    oldLog = gLogLevel;
	    LOG( AliHLTLog::kBenchmark, "AliHLTDetectorPublisher::AliHLTDetectorEventAnnouncer::Run", "Benchmark 1" )
		<< "Time needed for WaitForEvent: " << AliHLTLog::kDec << dt << " musec" << ENDLOG;
	    gLogLevel = oldLog;
	    }
#endif

	if ( sbevent && trg )
	    {
 	    LOG( AliHLTLog::kDebug, "AliHLTDetectorPublisher::AliHLTDetectorEventAnnouncer::Run", "Received event" )
		<< "Received event 0x" << AliHLTLog::kHex << eventID 
 		<< " (" << AliHLTLog::kDec << eventID << ") with "
		<< sbevent->fDataBlockCount << " data blocks." << ENDLOG;

	    pthread_mutex_lock( &fPub.fMonitorFirstEventOrbitNrMutex );
	    if ( !fPub.fMonitorFirstEventOrbitNrs.empty() )
		{
		std::vector<AliUInt64_t>::iterator iter, end;
		bool found=true;
		while ( found && !fPub.fMonitorFirstEventOrbitNrs.empty() )
		    {
		    found=false;
		    iter = fPub.fMonitorFirstEventOrbitNrs.begin();
		    end = fPub.fMonitorFirstEventOrbitNrs.end();
		    while ( iter != end )
			{
			if ( eventID.fType == kAliEventTypeData && eventID.fNr >= *iter )
			    {
			    LOG( AliHLTLog::kDebug, "AliHLTDetectorPublisher::AliHLTDetectorEventAnnouncer::Run", "Marking monitor event" )
				<< "Marking event 0x" << AliHLTLog::kHex << eventID 
				<< " (" << AliHLTLog::kDec << eventID << ") for monitor full passthrough." << ENDLOG;
			    found=true;
			    sbevent->fStatusFlags |= ALIHLTSUBEVENTDATADESCRIPTOR_FULLPASSTHROUGH|ALIHLTSUBEVENTDATADESCRIPTOR_MONITOR;
			    fPub.fMonitorFirstEventOrbitNrs.erase( iter );
			    break;
			    }
			iter++;
			}
		    }
		}
	    pthread_mutex_unlock( &fPub.fMonitorFirstEventOrbitNrMutex );


	    // XXX
	    AliHLTEventTriggerStruct* tmpTrg;
	    tmpTrg = (AliHLTEventTriggerStruct*)fPub.fETSCache.Get( trg->fHeader.fLength );
// 	    LOG( AliHLTLog::kDebug, "AliHLTDetectorPublisher::AliHLTDetectorEventAnnouncer::Run", "Obtained ETS" )
// 		<< "Ontained EventTriggerStruct for event 0x" << AliHLTLog::kHex << eventID 
// 		<< " (" << AliHLTLog::kDec << eventID << "): x0" << MLUCLog::kHex
// 		<< (unsigned long)tmpTrg << "." << ENDLOG;
	    if ( tmpTrg )
		{
		ETSData ed;
		ed.fEventID = eventID;
		ed.fETS = tmpTrg;
		memcpy( tmpTrg, trg, trg->fHeader.fLength );
		pthread_mutex_lock( &fPub.fETSMutex );
		fPub.fETSs.Add( ed );
		pthread_mutex_unlock( &fPub.fETSMutex );
		}
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::AliHLTDetectorEventAnnouncer::Run", "Out of memory" )
		    << "Unable to allocate memory for event trigger structure of " << AliHLTLog::kDec
		    << trg->fHeader.fLength << " bytes." << ENDLOG;
		}

#ifdef DO_BENCHMARK
	    if ( fPub.fBenchmark )
		gettimeofday( &t3, NULL );
#endif

	    tmpCount = 0;
	    struct timespec ts;
	    do
		{
#if 0
		quitLock.Lock();
		while ( fPub.fPaused && (!fQuit || fPub.MustContinue()) )
		    {
		    quitLock.Unlock();
		    if ( fPub.fStatus )
			{
// XXXX TODO LOCKING XXXX
			MLUCMutex::TLocker statusDataLock( fPub.fStatus->fStatusDataMutex );
			MLUCMutex::TLocker stateLock( fPub.fStatus->fStateMutex );
			fPub.fStatus->fProcessStatus.fState |= kAliHLTPCPausedStateFlag;
			}
		    fPub.fPauseSignal.Lock();
		    fPub.fPauseSignal.Wait( 500 );
		    fPub.fPauseSignal.Unlock();
		    if ( fPub.fStatus && !fPub.fPaused )
			{
// XXXX TODO LOCKING XXXX
			MLUCMutex::TLocker statusDataLock( fPub.fStatus->fStatusDataMutex );
			MLUCMutex::TLocker stateLock( fPub.fStatus->fStateMutex );
			fPub.fStatus->fProcessStatus.fState &= ~kAliHLTPCPausedStateFlag;
			}
		    quitLock.Lock();
		    }
		quitLock.Unlock();
#endif
		ret = fPub.AnnounceEvent( eventID, *sbevent, *trg );
		if ( ret )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTDetectorPublisher::AliHLTDetectorEventAnnouncer::Run", "Announce Event Failed" )
			<< "Announce event for event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
			<< ") failed." << ENDLOG;
		    LOG( AliHLTLog::kWarning, "AliHLTDetectorPublisher::AliHLTDetectorEventAnnouncer::Run", "Counts" )
			<< "GetPendingEventCount(): " << AliHLTLog::kDec << fPub.GetPendingEventCount()
			<< "." << ENDLOG;
		    tmpCount++;
		    if ( !(tmpCount % fPub.fAnnouncePollSleepModulo) )
			{
			//ts.tv_sec = ts.tv_nsec = 0;
			//ts.tv_nsec = 1000000;
			ts.tv_sec = fPub.fAnnounceBackoffSleepTime/1000000;
			ts.tv_nsec = (fPub.fAnnounceBackoffSleepTime%1000000)*1000;
			nanosleep( &ts, NULL );
			}
		    else
			{
			struct timeval tts, tte;
			AliUInt32_t dtt;
			gettimeofday( &tts, NULL );
			do
			    {
			    gettimeofday( &tte, NULL );
			    dtt = (tte.tv_sec-tts.tv_sec)*1000000+(tte.tv_usec-tts.tv_usec);
			    }
			while ( dtt < fPub.fAnnounceBusyPollTime );
			}
		    //usleep( fPub.fAnnounceBackoffTime );
		    }
		else if ( fPub.fPersistentState )
		    {
		    fPub.fPersistentState->fLastEventID = eventID;
		    fPub.fPersistentState->Touch();
		    }
		quitLock.Lock();
		}
	    while ( ret && (!fQuit || fPub.MustContinue()) );
	    quitLock.Unlock();

#ifdef DO_BENCHMARK
	    if ( fPub.fBenchmark )
		{
		gettimeofday( &t4, NULL );
		oldLog = gLogLevel;
		dt = (t4.tv_sec-t3.tv_sec)*1000000+(t4.tv_usec-t3.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTDetectorPublisher::AliHLTDetectorEventAnnouncer::Run", "Benchmark 1" )
		    << "Time needed for AnnounceEvent: " << AliHLTLog::kDec << dt << " musec" << ENDLOG;
		gLogLevel = oldLog;
		}
#endif
	    
	    //if ( fPub.fStatus )
	    //fPub.fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;

#if 0
	    quitLock.Lock();
	    while ( fPub.fPaused && (!fQuit || fPub.MustContinue()) )
		{
		quitLock.Lock();
		if ( fPub.fStatus )
		    {
// XXXX TODO LOCKING XXXX
		    MLUCMutex::TLocker statusDataLock( fPub.fStatus->fStatusDataMutex );
		    MLUCMutex::TLocker stateLock( fPub.fStatus->fStateMutex );
		    fPub.fStatus->fProcessStatus.fState |= kAliHLTPCPausedStateFlag;
		    }
		fPub.fPauseSignal.Lock();
		fPub.fPauseSignal.Wait( 500 );
		fPub.fPauseSignal.Unlock();
		if ( fPub.fStatus && !fPub.fPaused )
		    {
// XXXX TODO LOCKING XXXX
		    MLUCMutex::TLocker statusDataLock( fPub.fStatus->fStatusDataMutex );
		    MLUCMutex::TLocker stateLock( fPub.fStatus->fStateMutex );
		    fPub.fStatus->fProcessStatus.fState &= ~kAliHLTPCPausedStateFlag;
		    }
		quitLock.Lock();
		}
	    quitLock.Unlock();
#endif

	    //fPub.PingActive();
	    }
	sbevent = NULL;
	trg = NULL;

#ifdef DO_BENCHMARK
	if ( fPub.fBenchmark )
	    gettimeofday( &t1, NULL );
#endif

	//quitLock.Lock();
	}
    //quitLock.Unlock();
    fPub.EndEventLoop();
    if ( fPub.fStatus )
	{
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fPub.fStatus->fStatusDataMutex );
	MLUCMutex::TLocker stateLock( fPub.fStatus->fStateMutex );
	if ( !(fPub.fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTDetectorPublisher::AliHLTDetectorEventAnnouncer::Run", "Inappropriate state change" )
		<< "State change occured without set state change flag." << ENDLOG;
	    //fPub.fStatus->fProcessStatus.fState |= kAliHLTPCProducerErrorStateFlag;
	    }
	//fPub.fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
	fPub.fStatus->fProcessStatus.fState &= ~kAliHLTPCRunningStateFlag;
	fPub.fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	fPub.fStatus->fProcessStatus.fState &= ~kAliHLTPCPausedStateFlag;
	stateLock.Unlock();
	statusDataLock.Unlock();
	}
#if 0
#define LOGFLAG(flag) << " " << #flag << ": " << ( (flag) ? "true" : "false" )
    LOG( AliHLTLog::kWarning, "AliHLTDetectorPublisher::AliHLTDetectorEventAnnouncer::Run", "Flag debugging (end)" )
	<< "Flag debugging (end): "
	LOGFLAG(fQuit)
	LOGFLAG(fPub.fPaused)
	LOGFLAG((fPub.fMaxPendingEventCount>0))
	LOGFLAG((fPub.GetPendingEventCount()>=fPub.fMaxPendingEventCount))
	LOGFLAG((fPub.MustContinue()))
	LOGFLAG((bool)sbevent)
	LOGFLAG((bool)trg)
	<< ENDLOG;
#endif
    quitLock.Lock();
    fQuitted = true;
    fPub.fPaused = false;
    }


unsigned AliHLTDetectorPublisher::GetAdditionalBlockCount()
    {
#ifdef USE_DDL_LIST_SHM
    if ( fAliceHLT && fDDLListBlockSet )
	return 1;
#endif
    return 0;
    }

bool AliHLTDetectorPublisher::GetAdditionalBlock( unsigned /*ndx*/, AliHLTSubEventDataBlockDescriptor& /*blockDescriptor*/ )
    {
#ifdef USE_DDL_LIST_SHM
    if ( fAliceHLT && fDDLListBlockSet && ndx==0 )
	{
	blockDescriptor = fDDLListBlock;
	return true;
	}
#endif
    return false;
    }

bool AliHLTDetectorPublisher::ReleaseAdditionalBlock( AliHLTSubEventDataBlockDescriptor& /*blockDescriptor*/ )
    {
#ifdef USE_DDL_LIST_SHM
    if ( fAliceHLT && fDDLListBlockSet )
	{
	if ( blockDescriptor.fShmID.fShmType == fDDLListBlock.fShmID.fShmType && blockDescriptor.fShmID.fKey.fID == fDDLListBlock.fShmID.fKey.fID && 
	     blockDescriptor.fBlockOffset == fDDLListBlock.fBlockOffset && blockDescriptor.fBlockSize == fDDLListBlock.fBlockSize )
	    return true;
	}
#endif
    return false;
    }

int AliHLTDetectorPublisher::CreateSORBlocks( vector<AliHLTSubEventDataBlockDescriptor>& blockDescriptors )
    {
    blockDescriptors.clear();
    if ( !fShm )
	return ENODEV;
    AliHLTShmID shmKey;
    shmKey.fShmType = kSysVShmPrivateType;
    shmKey.fKey.fID = ~(AliUInt64_t)0;

    const unsigned xORStructSize = sizeof(AliUInt32_t)*3;
    
    unsigned long runTypeLen = strlen(fRunType.c_str())+1;
    unsigned long ecsParamLen = fECSParameters.Length()+1;
    unsigned long size = xORStructSize+runTypeLen+ecsParamLen;
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker shmIDLock( fShmIDMutex );
    fSORShmID = fShm->GetShm( shmKey, size );
    if ( fSORShmID==(AliUInt32_t)-1 )
	{
	shmIDLock.Unlock();
	LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::CreateSORBlock", "Cannot create shared memory" )
	    << "Cannot create private System V shared memory." << ENDLOG;
	return ENOMEM;
	}
    if ( size < xORStructSize+runTypeLen )
	{
	fShm->ReleaseShm( fSORShmID );
	shmIDLock.Unlock();
	LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::CreateSORBlock", "Shared memory too small" )
	    << "Created shared memory is too small (" << AliHLTLog::kDec
	    << size << " bytes instead of " << xORStructSize << " bytes)." << ENDLOG;
	return ENOMEM;
	}
    AliUInt32_t* ptr = (AliUInt32_t*)fShm->LockShm( fSORShmID );
    if ( !ptr )
	{
	fShm->ReleaseShm( fSORShmID );
	shmIDLock.Unlock();
	LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::CreateSORBlock", "Cannot lock shared memory" )
	    << "Cannot lock shared memory." << ENDLOG;
	return ENOMEM;
	}
    shmIDLock.Unlock();

    ptr[0] = xORStructSize;

    AliUInt32_t beamTypeHLTMode = fBeamType | (fHLTMode << 3);

    ptr[1] = (AliUInt32_t)fRunNumber;
    ptr[2] = (AliUInt32_t)beamTypeHLTMode;
    AliHLTSubEventDataBlockDescriptor sebd;

    sebd.fShmID = shmKey;
    sebd.fBlockSize = xORStructSize;
    sebd.fBlockOffset = 0;
    sebd.fProducerNode = fNodeID;
    sebd.fDataType.fID = STARTOFRUN_DATAID;
    sebd.fDataOrigin.fID = PRIV_DATAORIGIN;
    sebd.fDataSpecification = 0;
    sebd.fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_PASSTHROUGH;
    AliHLTSubEventDescriptor::FillBlockAttributes( sebd.fAttributes );

    blockDescriptors.push_back( sebd );


    if ( runTypeLen>1 )
	{
	strcpy( ((char*)ptr)+xORStructSize, fRunType.c_str() );
	sebd.fShmID = shmKey;
	sebd.fBlockSize = runTypeLen;
	sebd.fBlockOffset = xORStructSize;
	sebd.fProducerNode = fNodeID;
	sebd.fDataType.fID = RUNTYPE_DATAID;
	sebd.fDataOrigin.fID = PRIV_DATAORIGIN;
	sebd.fDataSpecification = 0;
	sebd.fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_PASSTHROUGH;
	AliHLTSubEventDescriptor::FillBlockAttributes( sebd.fAttributes );
	
	blockDescriptors.push_back( sebd );
	}
    if ( !fECSParameters.IsEmpty() && !fECSParameters.IsWhiteSpace() )
	{
	strcpy( ((char*)ptr)+xORStructSize+runTypeLen, fECSParameters.c_str() );
	sebd.fShmID = shmKey;
	sebd.fBlockSize = ecsParamLen;
	sebd.fBlockOffset = xORStructSize+runTypeLen;
	sebd.fProducerNode = fNodeID;
	sebd.fDataType.fID = ECSPARAM_DATAID;
	sebd.fDataOrigin.fID = PRIV_DATAORIGIN;
	sebd.fDataSpecification = 0;
	sebd.fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_PASSTHROUGH;
	AliHLTSubEventDescriptor::FillBlockAttributes( sebd.fAttributes );
	
	blockDescriptors.push_back( sebd );
	}
    shmIDLock.Lock();
    fShm->UnlockShm( fSORShmID );
    return 0;
    }

int AliHLTDetectorPublisher::ReleaseSORBlocks()
    {
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker shmIDLock( fShmIDMutex );
    if ( fSORShmID!=(AliUInt32_t)-1 )
	{
	fShm->ReleaseShm( fSORShmID );
	fSORShmID=(AliUInt32_t)-1;
	}
    return 0;
    }

int AliHLTDetectorPublisher::CreateEORBlocks( vector<AliHLTSubEventDataBlockDescriptor>& blockDescriptors )
    {
    blockDescriptors.clear();
    if ( !fShm )
	return ENODEV;
    AliHLTShmID shmKey;
    shmKey.fShmType = kSysVShmPrivateType;
    shmKey.fKey.fID = ~(AliUInt64_t)0;

    const unsigned xORStructSize = sizeof(AliUInt32_t)*3;
    
    unsigned long size = xORStructSize;
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker shmIDLock( fShmIDMutex );
    fEORShmID = fShm->GetShm( shmKey, size );
    if ( fEORShmID==(AliUInt32_t)-1 )
	{
	shmIDLock.Unlock();
	LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::CreateEORBlock", "Cannot create shared memory" )
	    << "Cannot create private System V shared memory." << ENDLOG;
	return ENOMEM;
	}
    if ( size < xORStructSize )
	{
	fShm->ReleaseShm( fEORShmID );
	shmIDLock.Unlock();
	LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::CreateEORBlock", "Shared memory too small" )
	    << "Created shared memory is too small (" << AliHLTLog::kDec
	    << size << " bytes instead of " << xORStructSize << " bytes)." << ENDLOG;
	return ENOMEM;
	}
    AliUInt32_t* ptr = (AliUInt32_t*)fShm->LockShm( fEORShmID );
    if ( !ptr )
	{
	fShm->ReleaseShm( fEORShmID );
	shmIDLock.Unlock();
	LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::CreateEORBlock", "Cannot lock shared memory" )
	    << "Cannot lock shared memory." << ENDLOG;
	return ENOMEM;
	}

    shmIDLock.Unlock();

    AliUInt32_t beamTypeHLTMode = fBeamType | (fHLTMode << 3);

    ptr[0] = xORStructSize;
    ptr[1] = (AliUInt32_t)fRunNumber;
    ptr[2] = (AliUInt32_t)beamTypeHLTMode;
    AliHLTSubEventDataBlockDescriptor sebd;

    sebd.fShmID = shmKey;
    sebd.fBlockSize = xORStructSize;
    sebd.fBlockOffset = 0;
    sebd.fProducerNode = fNodeID;
    sebd.fDataType.fID = ENDOFRUN_DATAID;
    sebd.fDataOrigin.fID = PRIV_DATAORIGIN;
    sebd.fDataSpecification = 0;
    sebd.fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_PASSTHROUGH;
    AliHLTSubEventDescriptor::FillBlockAttributes( sebd.fAttributes );
    
    blockDescriptors.push_back( sebd );
    shmIDLock.Lock();
    fShm->UnlockShm( fEORShmID );
    return 0;
    }

int AliHLTDetectorPublisher::ReleaseEORBlocks()
    {
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker shmIDLock( fShmIDMutex );
    if ( fEORShmID!=(AliUInt32_t)-1 )
	{
	fShm->ReleaseShm( fEORShmID );
	fEORShmID=(AliUInt32_t)-1;
	}
    return 0;
    }


void AliHLTDetectorPublisher::Tick( bool sendTickEvent )
    {
    struct timeval now;
    gettimeofday( &now, NULL );
    if ( fTickInterval_us && sendTickEvent )
	{
	unsigned long long tnow = ((unsigned long long)now.tv_usec) + ((unsigned long long)now.tv_sec)*1000000;
	if ( !fLastTickEvent )
	    fLastTickEvent = ((tnow / fTickInterval_us)-1) * fTickInterval_us;
	while ( tnow-fLastTickEvent >= fTickInterval_us )
	    {
	    fLastTickEvent += fTickInterval_us;

	    AliEventID_t eventID;
	    eventID.fType = kAliEventTypeTick;
	    eventID.fNr = fLastTickEvent;
	    AliHLTSubEventDataDescriptor sbevent;
	    
	    sbevent.fHeader.fLength = sizeof( AliHLTSubEventDataDescriptor );
	    sbevent.fHeader.fType.fID = ALIL3SUBEVENTDATADESCRIPTOR_TYPE;
	    sbevent.fHeader.fSubType.fID = 0;
	    sbevent.fHeader.fVersion = 5;
	    sbevent.fEventID = eventID;
	    sbevent.fEventBirth_s = now.tv_sec;
	    sbevent.fEventBirth_us = now.tv_usec;
	    sbevent.fOldestEventBirth_s = 0;//fOldestEventBirth;
	    sbevent.fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST;
	    sbevent.fDataBlockCount = 0;
	    sbevent.fDataType.fID = UNKNOWN_DATAID;
	    
	    AliHLTEventTriggerStruct ets;
	    ets.fHeader.fLength = sizeof(AliHLTEventTriggerStruct);
	    ets.fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
	    ets.fHeader.fSubType.fID = 0;
	    ets.fHeader.fVersion = 1;
	    ets.fDataWordCount = 0;
	    
	    AnnounceEvent( eventID, sbevent, ets );
	    }
	}
    vector<ConfigureEventData> configEventIDs;
    pthread_mutex_lock( &fConfigureEventIDMutex );
    configEventIDs = fConfigureEventIDs;
    fConfigureEventIDs.clear();
    pthread_mutex_unlock( &fConfigureEventIDMutex );
    vector<ConfigureEventData>::iterator iter = configEventIDs.begin(), end = configEventIDs.end();
    while ( iter != end )
	{
	AliEventID_t eventID;
	eventID.fType = iter->fEventType;
	eventID.fNr = iter->fEventNr;
	unsigned long dataBlockCnt=0;
	if ( eventID.fType == kAliEventTypeReconfigure )
	    dataBlockCnt = 3;
	else
	    dataBlockCnt = 1;
	const unsigned long sbDataSize = sizeof(AliHLTSubEventDataDescriptor)+dataBlockCnt*sizeof(AliHLTSubEventDataBlockDescriptor);
	AliUInt8_t sbData[sbDataSize];
	memset( sbData, 0, sbDataSize );
	AliHLTSubEventDataDescriptor& sbevent = *(AliHLTSubEventDataDescriptor*)sbData;
	    
	sbevent.fHeader.fLength = sizeof( AliHLTSubEventDataDescriptor );
	sbevent.fHeader.fType.fID = ALIL3SUBEVENTDATADESCRIPTOR_TYPE;
	sbevent.fHeader.fSubType.fID = 0;
	sbevent.fHeader.fVersion = 5;
	sbevent.fEventID = eventID;
	sbevent.fEventBirth_s = now.tv_sec;
	sbevent.fEventBirth_us = now.tv_usec;
	sbevent.fOldestEventBirth_s = 0;//fOldestEventBirth;
	sbevent.fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST;
	sbevent.fDataBlockCount = 0;
	sbevent.fDataType.fID = UNKNOWN_DATAID;
	    
	AliHLTEventTriggerStruct ets;
	ets.fHeader.fLength = sizeof(AliHLTEventTriggerStruct);
	ets.fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
	ets.fHeader.fSubType.fID = 0;
	ets.fHeader.fVersion = 1;
	ets.fDataWordCount = 0;

	if ( eventID.fType == kAliEventTypeReconfigure )
	    {
	    MLUCString ids, comps, paths;
	    std::vector<MLUCString> tokens;
	    iter->fData.Split( tokens, ' ' );
	    std::vector<MLUCString>::iterator tok_iter, tok_end;
	    tok_iter = tokens.begin();
	    tok_end = tokens.end();
	    while ( tok_iter != tok_end )
		{
		std::vector<MLUCString> parts;
		tok_iter->Split( parts, ':' );
		if ( parts.size()==2 )
		    {
		    if ( parts[0]=="id" )
			{
			if ( ids.Length()>0 )
			    ids += " ";
			ids += parts[1];
			}
		    else if ( parts[0]=="comp" )
			{
			if ( comps.Length()>0 )
			    comps += " ";
			comps += parts[1];
			}
		    else if ( parts[0]=="path" )
			{
			if ( paths.Length()>0 )
			    paths += " ";
			paths += parts[1];
			}
		    }
		tok_iter++;
		}
	    unsigned long dataLen = ids.Length()+comps.Length()+paths.Length()+3;
	    if ( dataLen>3 && fShm )
		{
		AliHLTShmID shmKey;
		shmKey.fShmType = kSysVShmPrivateType;
		shmKey.fKey.fID = ~(AliUInt64_t)0;
		
		unsigned long size = dataLen;
		iter->fShmID = fShm->GetShm( shmKey, size );
		if ( iter->fShmID==(AliUInt32_t)-1 )
		    {
		    LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::Tick", "Cannot create shared memory" )
			<< "Cannot create private System V shared memory for reconfigure event 0x"
			<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ")." << ENDLOG;
		    }
		else if ( size < dataLen )
		    {
		    fShm->ReleaseShm( iter->fShmID );
		    LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::Tick", "Shared memory too small" )
			<< "Created shared memory is too small (" << AliHLTLog::kDec
			<< size << " bytes instead of " << dataLen << " bytes) for reconfigure event 0x"
			<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ")." << ENDLOG;
		    }
		else
		    {
		    char* ptr = (char*)fShm->LockShm( iter->fShmID );
		    if ( !ptr )
			{
			fShm->ReleaseShm( iter->fShmID );
			LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::Tick", "Cannot lock shared memory" )
			    << "Cannot lock shared memory for reconfigure event 0x"
			    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			    << eventID << ")." << ENDLOG;
			}
		    else
			{
			unsigned long blockNdx = 0;
			unsigned long offset = 0;
			if ( ids.Length()>0 )
			    {
			    strcpy( ptr+offset, ids.c_str() );
			    sbevent.fDataBlocks[blockNdx].fShmID = shmKey;
			    sbevent.fDataBlocks[blockNdx].fBlockSize = strlen(ptr+offset)+1;
			    sbevent.fDataBlocks[blockNdx].fBlockOffset = offset;
			    sbevent.fDataBlocks[blockNdx].fProducerNode = fNodeID;
			    sbevent.fDataBlocks[blockNdx].fDataType.fID = COMPONENTCONFIGIDS_DATAID;
			    sbevent.fDataBlocks[blockNdx].fDataOrigin.fID = PRIV_DATAORIGIN;
			    sbevent.fDataBlocks[blockNdx].fDataSpecification = 0;
			    
			    sbevent.fDataBlocks[blockNdx].fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_PASSTHROUGH;
			    AliHLTSubEventDescriptor::FillBlockAttributes( sbevent.fDataBlocks[0].fAttributes );
			    
			    sbevent.fHeader.fLength += sizeof(AliHLTSubEventDataBlockDescriptor);
			    sbevent.fDataBlockCount++;
			    offset += sbevent.fDataBlocks[blockNdx].fBlockSize;
			    blockNdx++;
			    }
			if ( comps.Length()>0 )
			    {
			    strcpy( ptr+offset, comps.c_str() );
			    sbevent.fDataBlocks[blockNdx].fShmID = shmKey;
			    sbevent.fDataBlocks[blockNdx].fBlockSize = strlen(ptr+offset)+1;
			    sbevent.fDataBlocks[blockNdx].fBlockOffset = offset;
			    sbevent.fDataBlocks[blockNdx].fProducerNode = fNodeID;
			    sbevent.fDataBlocks[blockNdx].fDataType.fID = COMPONENTCONFIGCOMPS_DATAID;
			    sbevent.fDataBlocks[blockNdx].fDataOrigin.fID = PRIV_DATAORIGIN;
			    sbevent.fDataBlocks[blockNdx].fDataSpecification = 0;
			    
			    sbevent.fDataBlocks[blockNdx].fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_PASSTHROUGH;
			    AliHLTSubEventDescriptor::FillBlockAttributes( sbevent.fDataBlocks[0].fAttributes );
			    
			    sbevent.fHeader.fLength += sizeof(AliHLTSubEventDataBlockDescriptor);
			    sbevent.fDataBlockCount++;
			    offset += sbevent.fDataBlocks[blockNdx].fBlockSize;
			    blockNdx++;
			    }
			if ( paths.Length()>0 )
			    {
			    strcpy( ptr+offset, paths.c_str() );
			    sbevent.fDataBlocks[blockNdx].fShmID = shmKey;
			    sbevent.fDataBlocks[blockNdx].fBlockSize = strlen(ptr+offset)+1;
			    sbevent.fDataBlocks[blockNdx].fBlockOffset = offset;
			    sbevent.fDataBlocks[blockNdx].fProducerNode = fNodeID;
			    sbevent.fDataBlocks[blockNdx].fDataType.fID = COMPONENTCONFIGPATHS_DATAID;
			    sbevent.fDataBlocks[blockNdx].fDataOrigin.fID = PRIV_DATAORIGIN;
			    sbevent.fDataBlocks[blockNdx].fDataSpecification = 0;
			    
			    sbevent.fDataBlocks[blockNdx].fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_PASSTHROUGH;
			    AliHLTSubEventDescriptor::FillBlockAttributes( sbevent.fDataBlocks[0].fAttributes );
			    
			    sbevent.fHeader.fLength += sizeof(AliHLTSubEventDataBlockDescriptor);
			    sbevent.fDataBlockCount++;
			    offset += sbevent.fDataBlocks[blockNdx].fBlockSize;
			    blockNdx++;
			    }
			ptr = NULL;
			fShm->UnlockShm( iter->fShmID );
			
			pthread_mutex_lock( &fConfigureEventIDMutex );
			fSentConfigureEventIDs.push_back( *iter );
			pthread_mutex_unlock( &fConfigureEventIDMutex );
			    
			}
		    }
		}
	    }
	else
	    {
	    unsigned long dataLen = iter->fData.Length()+1;
	    if ( dataLen>1 && fShm )
		{
		AliHLTShmID shmKey;
		shmKey.fShmType = kSysVShmPrivateType;
		shmKey.fKey.fID = ~(AliUInt64_t)0;
		
		unsigned long size = dataLen;
		iter->fShmID = fShm->GetShm( shmKey, size );
		if ( iter->fShmID==(AliUInt32_t)-1 )
		    {
		    LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::Tick", "Cannot create shared memory" )
			<< "Cannot create private System V shared memory for DCS update event 0x"
			<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ")." << ENDLOG;
		    }
		else if ( size < dataLen )
		    {
		    fShm->ReleaseShm( iter->fShmID );
		    LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::Tick", "Shared memory too small" )
			<< "Created shared memory is too small (" << AliHLTLog::kDec
			<< size << " bytes instead of " << dataLen << " bytes) for DCS update event 0x"
			<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ")." << ENDLOG;
		    }
		else
		    {
		    char* ptr = (char*)fShm->LockShm( iter->fShmID );
		    if ( !ptr )
			{
			fShm->ReleaseShm( iter->fShmID );
			LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::Tick", "Cannot lock shared memory" )
			    << "Cannot lock shared memory for DCS update event 0x"
			    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			    << eventID << ")." << ENDLOG;
			}
		    else
			{
			strcpy( ptr, iter->fData.c_str() );
			sbevent.fDataBlocks[0].fShmID = shmKey;
			sbevent.fDataBlocks[0].fBlockSize = strlen(ptr)+1;
			sbevent.fDataBlocks[0].fBlockOffset = 0;
			sbevent.fDataBlocks[0].fProducerNode = fNodeID;
			sbevent.fDataBlocks[0].fDataType.fID = DCSUPDATE_DATAID;
			sbevent.fDataBlocks[0].fDataOrigin.fID = PRIV_DATAORIGIN;
			sbevent.fDataBlocks[0].fDataSpecification = 0;
			    
			sbevent.fDataBlocks[0].fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_PASSTHROUGH;
			AliHLTSubEventDescriptor::FillBlockAttributes( sbevent.fDataBlocks[0].fAttributes );
			
			sbevent.fHeader.fLength += sizeof(AliHLTSubEventDataBlockDescriptor);
			sbevent.fDataBlockCount++;

			ptr = NULL;
			fShm->UnlockShm( iter->fShmID );
			}
		    
		    pthread_mutex_lock( &fConfigureEventIDMutex );
		    fSentConfigureEventIDs.push_back( *iter );
		    pthread_mutex_unlock( &fConfigureEventIDMutex );
		    
		    }
		}
	    else if ( !fShm )
		{
		LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::Tick", "Cannot create shared memory" )
		    << "Cannot create shared memory for DCS update event 0x"
		    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ") as no shared memory object is available." << ENDLOG;
		}
	    }

	AnnounceEvent( eventID, sbevent, ets );
	
	iter++;
	}
    }

bool AliHLTDetectorPublisher::FillHLTEventTriggerDataDDLList( AliHLTHLTEventTriggerData* hetd )
    {
    if ( !fDDLListData )
	return false;
    memcpy( hetd->fReadoutList, fDDLListData, AliHLTReadoutList::GetReadoutListSize( gReadoutListVersion ) );
    return true;
    }




// AliUInt32_t AliHLTDetectorPublisher::GetFreeBlock( AliUInt32_t size,  AliUInt32_t& bufferIndex )
//     {
//     vector<FreeListVector*>::iterator liter, lend;
//     pthread_mutex_lock( &fFreeListMutex );
//     liter = fFreeLists.begin();
//     lend = fFreeLists.end();
//     AliUInt32_t offset = ~0, j;
//     j=0;
//     FreeListVector::iterator smallest, iter, end;
//     smallest = NULL;
//     while ( liter != lend )
// 	{
// 	FreeListVector& flv = **liter;
// 	iter = flv.begin();
// 	end = flv.end();
// 	while ( iter != end )
// 	    {
// 	    if ( iter->fSize >= size )
// 		{
// 		if ( !smallest || smallest->fSize>iter->fSize )
// 		    smallest = iter;
// 		}
// 	    iter++;
// 	    }
// 	if ( smallest )
// 	    {
// 	    offset = smallest->fOffset;
// 	    smallest->fOffset += size;
// 	    smallest->fSize -= size;
// 	    if ( smallest->fSize == 0 )
// 		flv.erase( smallest );
// 	    bufferIndex = j;
// 	    break;
// 	    }
// 	liter++;
// 	j++;
// 	}
//     pthread_mutex_unlock( &fFreeListMutex );
//     return offset;
//     }

// void AliHLTDetectorPublisher::ReleaseBlock( AliUInt32_t bufferIndex, AliUInt32_t offset, AliUInt32_t size )
//     {
//     bool inserted = false;
//     FreeListData fld;
//     vector<FreeListData>::iterator begin, iter, end;
//     if ( bufferIndex >= fFreeLists.size() )
// 	return;
//     pthread_mutex_lock( &fFreeListMutex );
//     FreeListVector& flv = *fFreeLists[bufferIndex];
//     begin = iter = flv.begin();
//     end = flv.end();
//     while ( iter != end && iter->fOffset )
// 	{
// 	if ( offset+size == iter->fOffset )
// 	    {
// 	    iter->fOffset = offset;
// 	    iter->fSize += size;
// 	    inserted = true;
// 	    //if ( iter != begin && (iter-1)->fOffset+(iter-1)->fSize == offset )
// 	    break;
// 	    }
// 	if ( offset == iter->fOffset+iter->fSize )
// 	    {
// 	    iter->fSize += size;
// 	    if ( iter != end )
// 		{
// 		if ( iter->fOffset+iter->fSize == (iter+1)->fOffset )
// 		    {
// 		    iter->fSize += (iter+1)->fSize;
// 		    flv.erase( iter+1 );
// 		    }
// 		}
// 	    inserted = true;
// 	    break;
// 	    }
// 	if ( offset > iter->fOffset )
// 	    {
// 	    fld.fOffset = offset;
// 	    fld.fSize = size;
// 	    flv.insert( iter, fld );
// 	    inserted = true;
// 	    break;
// 	    }
// 	iter++;
// 	}

//     if ( !inserted )
// 	{
// 	fld.fOffset = offset;
// 	fld.fSize = size;
// 	flv.insert( end, fld );
// 	}
//     pthread_mutex_unlock( &fFreeListMutex );
//     }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
