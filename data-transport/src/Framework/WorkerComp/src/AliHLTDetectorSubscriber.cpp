/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTDetectorSubscriber.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTEventDoneData.hpp"
#include <errno.h>


AliHLTDetectorSubscriber::AliHLTDetectorSubscriber( const char* name, AliUInt32_t maxShmNoUseCount, int eventCountAlloc ):
    AliHLTSampleSubscriber( name ), fDescriptors( (eventCountAlloc<0) ? 4 : eventCountAlloc ), 
    fProcessingThread( *this ),
    fEventSignal( eventCountAlloc, true ),
    fEventDataCache( sizeof(EventData), (eventCountAlloc<0) ? 4 : eventCountAlloc )
    {
    fPub = NULL;
    pthread_mutex_init( &fSEDDMutex, NULL );
    pthread_mutex_init( &fShmMutex, NULL );
    fCurrentEvent = AliEventID_t();
    fQuitProcessing = fProcessingQuitted = false;
    fMaxShmNoUseCount = maxShmNoUseCount;
    fDefaultETS.fHeader.fLength = sizeof(fDefaultETS);
    fDefaultETS.fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
    fDefaultETS.fHeader.fSubType.fID = 0;
    fDefaultETS.fHeader.fVersion = 1;
    fDefaultETS.fDataWordCount = 0;
    }

AliHLTDetectorSubscriber::~AliHLTDetectorSubscriber()
    {
    vector<ShmData>::iterator iter;
    while ( fShmData.size()>0 )
	{
	iter = fShmData.begin();
#if __GNUC__>=3
	if ( iter.base() )
#else
	if ( iter )
#endif
	    {
	    if ( iter->fShmID!= (AliUInt32_t)-1 )
		{
		if ( iter->fShmPtr )
		    fShm.UnlockShm( iter->fShmID );
		iter->fShmPtr = NULL;
		fShm.ReleaseShm( iter->fShmID );
		iter->fShmID = (AliUInt32_t)-1;
		}
	    }
	fShmData.erase( iter );
	}

    pthread_mutex_destroy( &fSEDDMutex );
    pthread_mutex_destroy( &fShmMutex );
    }

AliHLTDetectorSubscriber::operator bool()
    {
    return (bool)fDescriptors;
    }

int AliHLTDetectorSubscriber::NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
					const AliHLTSubEventDataDescriptor& sbevent,
					const AliHLTEventTriggerStruct& ets )
    {
    AliHLTSubEventDataDescriptor* sedd;
    pthread_mutex_lock( &fSEDDMutex );
    sedd = fDescriptors.GetFreeEventDescriptor( eventID, sbevent.fDataBlockCount );
    pthread_mutex_unlock( &fSEDDMutex );
    LOG( AliHLTLog::kDebug, "AliHLTDetectorSubscriber", "New event received" )
	<< AliHLTLog::kDec << "Received notification for event " << eventID << " with " 
	<< sbevent.fDataBlockCount << " data blocks. (Length: " 
	<< sbevent.fHeader.fLength << ") - sedd 0x" << AliHLTLog::kHex << (unsigned long)sedd << "." << ENDLOG;
    if ( !sedd )
	{
	LOG( AliHLTLog::kError, "AliHLTDetectorSusbcriber", "Get new event descriptor" )
	    << "Error, unable to get new event descriptor." << ENDLOG;
	AliHLTEventDoneData edd;
	AliHLTMakeEventDoneData( &edd, eventID );
	publisher.EventDone( *this, edd );
	return ENOMEM;
	}
    memcpy( sedd, &sbevent, sbevent.fHeader.fLength );
    AliUInt32_t i;
    vector<ShmData>::iterator iter, end;
    bool found;
    for ( i = 0; i < sedd->fDataBlockCount; i++ )
	{
	pthread_mutex_lock( &fShmMutex );
	iter = fShmData.begin();
	end = fShmData.end();
	found = false;
	while ( iter != end )
	    {
	    if ( iter->fShmKey.fShmType ==  sedd->fDataBlocks[i].fShmID.fShmType &&
		 iter->fShmKey.fKey.fID == sedd->fDataBlocks[i].fShmID.fKey.fID )
		{
		iter->fNonRefCount = 0;
		iter->fRefCount++;
		found = true;
		}
	    else
		{
		iter->fNonRefCount++;
		if ( fMaxShmNoUseCount && iter->fNonRefCount > fMaxShmNoUseCount )
		    {
		    }
		}
	    iter++;
	    }
	pthread_mutex_unlock( &fShmMutex );
	if ( !found )
	    {
	    ShmData data;
	    unsigned long tmpSize = 0;
	    data.fShmKey = sedd->fDataBlocks[i].fShmID;
	    data.fShmID = fShm.GetShm( data.fShmKey, tmpSize );
	    if ( data.fShmID == (AliUInt32_t)-1 )
		{
		LOG( AliHLTLog::kError, "AliHLTDetectorSubscriber", "Getting new event shared memory" )
		    << "Error getting shared memory region " << data.fShmKey.fKey.fID << " for event " 
		    << AliHLTLog::kDec << eventID << "." << ENDLOG;
		return ENOMEM;
		}
	    data.fShmPtr = fShm.LockShm( data.fShmID );
	    if ( !data.fShmPtr )
		{
		LOG( AliHLTLog::kError, "AliHLTDetectorSubscriber", "Getting new event shared memory" )
		    << "Error locking shared memory region " << data.fShmKey.fKey.fID << " (with ID "
		    << data.fShmID << ") for event " << AliHLTLog::kDec << eventID << "." << ENDLOG;
		return ENOMEM;
		}
	    data.fNonRefCount = 0;
	    data.fRefCount = 1;
	    pthread_mutex_lock( &fShmMutex );
	    fShmData.insert( fShmData.end(), data );
	    pthread_mutex_unlock( &fShmMutex );
	    }
	}
    EventData *ed;
    ed = (EventData*)fEventDataCache.Get();
    if ( !ed )
	{
	LOG( AliHLTLog::kError, "AliHLTDetectorSubscriber", "Out of memory" )
	    << "Out of memory trying to allocate EventData of " << AliHLTLog::kDec
	    << sizeof(EventData) << " bytes for event 0x" << AliHLTLog::kHex
	    << eventID << " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
	return ENOMEM;
	}
    ed->fSEDD = sedd;
    if ( ets.fDataWordCount > 0 )
	{
	ed->fETS = (AliHLTEventTriggerStruct*)new AliUInt8_t[ ets.fHeader.fLength ];
	if ( ed->fETS )
	    {
	    memcpy( ed->fETS, &ets, ets.fHeader.fLength );
	    }
	else
	    {
	    LOG( AliHLTLog::kError, "AliHLTDetectorSubscriber", "Out of memory" )
		<< "Out of memory trying to allocate EventTrigger Data of " << AliHLTLog::kDec
		<< ets.fHeader.fLength << " bytes for event 0x" << AliHLTLog::kHex
		<< eventID << " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
	    }
	}
    else
	ed->fETS = NULL;
    fEventSignal.SignalEvent( ed );
    return 0;
    }

int AliHLTDetectorSubscriber::EventCanceled( AliHLTPublisherInterface& publisher, AliEventID_t eventID )
    {
    pthread_mutex_lock( &fSEDDMutex );
    EventData* edTmp = NULL;
    if ( fCurrentEvent == eventID )
	{
	fCurrentEvent = AliEventID_t();
	pthread_mutex_unlock( &fSEDDMutex );
	AbortProcessing();
	pthread_mutex_lock( &fSEDDMutex );
	}
    else
	{
	edTmp = fEventSignal.RemoveEvent( eventID );
	}
    unsigned long ndx;
    if ( fDescriptors.FindSEDD( eventID, ndx ) )
	{
	//AliHLTSubEventDataDescriptor* sedd;
	//sedd = fDescriptors.GetEventDescriptor( isPre, ndx );
	fDescriptors.ReleaseEventDescriptor( eventID );
	}
    pthread_mutex_unlock( &fSEDDMutex );
    if ( edTmp )
	{
	if ( edTmp->fETS )
	    delete [] (AliUInt8_t*)edTmp->fETS;
	fEventDataCache.Release( (uint8*)edTmp );
	}
    return AliHLTSampleSubscriber::EventCanceled( publisher, eventID );
    }

int AliHLTDetectorSubscriber::SubscriptionCanceled( AliHLTPublisherInterface& publisher )
    {
    if ( fCurrentEvent != AliEventID_t() )
	{
	AbortProcessing();
	QuitProcessing();
	}
    pthread_mutex_lock( &fSEDDMutex );
    if ( fCurrentEvent != AliEventID_t() )
	{
	fCurrentEvent = AliEventID_t();
	}
    pthread_mutex_unlock( &fSEDDMutex );
    //AliHLTSubEventDataDescriptor* sedd;
    EventData* ed;
    unsigned long ndx;
    while ( fEventSignal.HaveNotificationData() )
	{
	ed = reinterpret_cast<EventData*>( fEventSignal.PopNotificationData() );
	if ( ed )
	    {
	    pthread_mutex_lock( &fSEDDMutex );
	    if ( fDescriptors.FindSEDD( ed->fSEDD->fEventID, ndx ) )
		{
		//sedd = fDescriptors.GetEventDescriptor( isPre, ndx );
		fDescriptors.ReleaseEventDescriptor( ed->fSEDD->fEventID );
		}
	    pthread_mutex_unlock( &fSEDDMutex );
	    if ( ed->fETS )
		delete [] (AliUInt8_t*)ed->fETS;
	    fEventDataCache.Release( (uint8*)ed );
	    }
	}
    return AliHLTSampleSubscriber::SubscriptionCanceled( publisher );
    }

int AliHLTDetectorSubscriber::ReleaseEventsRequest( AliHLTPublisherInterface& publisher )
    {
    return AliHLTSampleSubscriber::ReleaseEventsRequest( publisher );
    }

bool AliHLTDetectorSubscriber::EventStarted( AliEventID_t eventID )
    {
    pthread_mutex_lock( &fSEDDMutex );
    fCurrentEvent = eventID;
    bool ret;
    unsigned long ndx;
    ret = fDescriptors.FindSEDD( eventID, ndx );
    pthread_mutex_unlock( &fSEDDMutex );
    return ret;
    }

int AliHLTDetectorSubscriber::EventDone( AliHLTEventDoneData* edd )
    {
    if ( !edd )
	return EFAULT;
    pthread_mutex_lock( &fSEDDMutex );
    fCurrentEvent = AliEventID_t();
    fDescriptors.ReleaseEventDescriptor( edd->fEventID );
    pthread_mutex_unlock( &fSEDDMutex );
    if ( fPub )
	{
	return fPub->EventDone( *this, *edd );
	}
    return ENODEV;
    }

void* AliHLTDetectorSubscriber::GetShmPtr( AliHLTShmID shmKey )
    {
    vector<ShmData>::iterator iter, end;
    void* p = NULL;
    int ret;
    bool found = false;
    ret = pthread_mutex_lock( &fShmMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTDetectorSubscriber::GetShmPtr", "Shm Mutex Error" )
	    << "Error locking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    iter = fShmData.begin();
    end = fShmData.end();
    while ( iter != end )
	{
	if ( iter->fShmKey.fShmType == shmKey.fShmType &&  
	     iter->fShmKey.fKey.fID == shmKey.fKey.fID )
	    {
	    iter->fNonRefCount = 0;
	    iter->fRefCount++;
	    found = true;
	    p = iter->fShmPtr;
	    //break;
	    }
	else
	    {
	    iter->fNonRefCount++;
	    if ( fMaxShmNoUseCount && iter->fNonRefCount > fMaxShmNoUseCount )
		{
		}
	    }
	iter++;
	}
    if ( !found )
	{
	ShmData data;
	unsigned long tmpSize = 0;
	data.fShmKey.fShmType = shmKey.fShmType;
	data.fShmKey.fKey.fID = shmKey.fKey.fID;
	data.fShmID = fShm.GetShm( data.fShmKey, tmpSize );
	if ( data.fShmID == (AliUInt32_t)-1 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTDetectorSubscriber::GetShmPtr", "Getting new event shared memory" )
		<< "Error getting shared memory region " << data.fShmKey.fKey.fID << "." << ENDLOG;
	    ret = pthread_mutex_unlock( &fShmMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTDetectorSubscriber::GetShmPtr", "Shm Mutex Error" )
		    << "Error unlocking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << "). Continuing..." << ENDLOG;
		}
	    return NULL;
	    }
	data.fShmPtr = fShm.LockShm( data.fShmID );
	if ( !data.fShmPtr )
	    {
	    LOG( AliHLTLog::kError, "AliHLTDetectorSubscriber::GetShmPtr", "Getting new event shared memory" )
		<< "Error locking shared memory region " << data.fShmKey.fKey.fID << " (with ID "
		<< data.fShmID << ")." << ENDLOG;
	    fShm.ReleaseShm( data.fShmID );
	    ret = pthread_mutex_unlock( &fShmMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTDetectorSubscriber::GetShmPtr", "Shm Mutex Error" )
		    << "Error unlocking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << "). Continuing..." << ENDLOG;
		}
	    return NULL;
	    }
	p = data.fShmPtr;
	data.fNonRefCount = 0;
	data.fRefCount = 1;
	fShmData.insert( fShmData.end(), data );
	}
    ret = pthread_mutex_unlock( &fShmMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTDetectorSubscriber::GetShmPtr", "Shm Mutex Error" )
	    << "Error unlocking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    return p;
    }

AliHLTDetectorSubscriber::AliHLTProcessingThread::AliHLTProcessingThread( AliHLTDetectorSubscriber& sub ):
    fSub( sub )
    {
    }

AliHLTDetectorSubscriber::AliHLTProcessingThread::~AliHLTProcessingThread()
    {
    }

void AliHLTDetectorSubscriber::AliHLTProcessingThread::Run()
    {
    fSub.DoProcessing();
    }

void AliHLTDetectorSubscriber::AliHLTEventSemaphore::SignalEvent( AliHLTDetectorSubscriber::EventData* ed )
    {
    if ( ed )
	{
	LOG( AliHLTLog::kDebug, "AliHLTDetectorSubscriber::AliHLTEventSemaphore::SignalEvent", "Event Received" )
	    << "Event 0x" << AliHLTLog::kHex << ed->fSEDD->fEventID << " (" << AliHLTLog::kDec
	    << ed->fSEDD->fEventID << ") - ed->fSEDD 0x" << AliHLTLog::kHex << (unsigned long)ed->fSEDD << " received." << ENDLOG;
	}
    else
	{
 	LOG( AliHLTLog::kWarning, "AliHLTDetectorSubscriber::AliHLTEventSemaphore::SignalEvent", "NULL pointer EventData" )
	    << "Received NULL pointer EventData." << ENDLOG;
	}
    bool ret;
    ret = AddNotificationData( reinterpret_cast<AliUInt64_t>(ed) );
    if ( !ret )
	{
 	LOG( AliHLTLog::kError, "AliHLTDetectorSubscriber::AliHLTEventSemaphore::SignalEvent", "Cannot add event" )
	    << "Unable to add event data 0x" << AliHLTLog::kHex << (unsigned long)ed << " for event 0x"
	    << AliHLTLog::kHex << ed->fSEDD->fEventID << " (" << AliHLTLog::kDec << ed->fSEDD->fEventID 
	    << ")." << ENDLOG;
	}
    Signal();
    }

AliHLTDetectorSubscriber::EventData* AliHLTDetectorSubscriber::AliHLTEventSemaphore::RemoveEvent( AliEventID_t eventID )
    {
    LOG( AliHLTLog::kDebug, "AliHLTDetectorSubscriber::AliHLTEventSemaphore::RemoveEvent", "Event Removal" )
	<< "About to remove event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << ")." << ENDLOG;
//     AliUInt64_t data = (AliUInt64_t)eventID;
//     vector<AliUInt64_t>::iterator iter, end;
//     pthread_mutex_lock( &fAccessSem );
//     iter = fNotificationData.begin();
//     end = fNotificationData.end();
//     while ( iter != end )
// 	{
// 	if ( *iter == data )
// 	    {
// 	    fNotificationData.erase( iter );
// 	    break;
// 	    }
// 	iter++;
// 	}
//     pthread_mutex_unlock( &fAccessSem );

    // XXX Check eventID vvs. sedd stored/compared.
    EventData* edTmp = NULL;
    AliUInt64_t data = eventID.fNr;
    vector<NotificationDataType>::iterator iter, end;
    pthread_mutex_lock( &fAccessSem );
    if ( fNotificationValidDataCnt > 0 )
	{
	if ( !fNotificationDataSize )
	    {
	    iter = fNotificationData.begin();
	    end = fNotificationData.end();
	    while ( iter != end )
		{
		if ( iter->fData && (reinterpret_cast<EventData*>(iter->fData))->fSEDD && 
		     (reinterpret_cast<EventData*>(iter->fData))->fSEDD->fEventID == eventID )
		    {
		    edTmp = reinterpret_cast<EventData*>(iter->fData);
		    fNotificationData.erase( iter );
		    break;
		    }
		iter++;
		}
	    fNotificationValidDataCnt--;
	    fNotificationDataCnt--;
	    }
	else
	    {
	    unsigned long n = fFirstUsed;
	    unsigned long i = 0;
	    while ( i++<fNotificationDataCnt && ((fNotificationStart+n)->fData!=data || !(fNotificationStart+n)->fValid) )
		n = (n+1) & fNotificationDataMask;
	    if ( (fNotificationStart+n)->fValid && (fNotificationStart+n)->fData && (reinterpret_cast<EventData*>((fNotificationStart+n)->fData))->fSEDD && 
		 (reinterpret_cast<EventData*>((fNotificationStart+n)->fData))->fSEDD->fEventID==eventID )
		{
		edTmp = reinterpret_cast<EventData*>((fNotificationStart+n)->fData);
		(fNotificationStart+n)->fValid = false;
		fNotificationValidDataCnt--;
		}
	    }
	}
    pthread_mutex_unlock( &fAccessSem );
    return edTmp;
}


void AliHLTDetectorSubscriber::DoProcessing()
    {
    LOG( AliHLTLog::kDebug, "AliHLTDetectorSubscriber", "Processing loop" )
	<< "Processing loop started." << ENDLOG;
    fQuitProcessing = false;
    fProcessingQuitted = false;
    //AliHLTSubEventDataDescriptor* sedd;
    EventData* ed;
    AliHLTEventTriggerStruct* ets;
    fEventSignal.Lock();
    while ( !fQuitProcessing )
	{
	if ( !fEventSignal.HaveNotificationData() )
	    fEventSignal.Wait();
	while ( fEventSignal.HaveNotificationData() )
	    {
	    ed = reinterpret_cast<EventData*>( fEventSignal.PopNotificationData() );
	    if ( !ed )
		continue;
	    if ( !ed->fSEDD )
		{
		if ( ed->fETS )
		    delete [] (AliUInt8_t*)ed->fETS;
		fEventDataCache.Release(( uint8*)ed );
		continue;
		}
	    LOG( AliHLTLog::kDebug, "AliHLTDetectorSubscriber::DoProcessing", "Event received" )
		<< "Event 0x" << AliHLTLog::kHex << ed->fSEDD->fEventID << " ("
		<< AliHLTLog::kDec << ed->fSEDD->fEventID << ") - ed 0x" << AliHLTLog::kHex << (unsigned long)ed << " received." << ENDLOG;
	    fProcessingOk = true;
	    fEventSignal.Unlock();
	    if ( ed->fETS )
		ets = ed->fETS;
	    else
		ets = &fDefaultETS;
	    ProcessEvent( ed->fSEDD, ets );
	    if ( ed->fETS )
		delete [] (AliUInt8_t*)ed->fETS;
	    fEventDataCache.Release( (uint8*)ed );
	    fEventSignal.Lock();
	    }
	}
    fProcessingQuitted = true;
    }

void AliHLTDetectorSubscriber::AbortProcessing()
    {
    fProcessingOk = false;
    }

void AliHLTDetectorSubscriber::QuitProcessing()
    {
    fQuitProcessing = true;
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 5000000;
    gettimeofday( &start, NULL );
    struct timespec ts;
    while ( !fProcessingQuitted && deltaT<timeLimit )
	{
	ts.tv_sec = 0;
	ts.tv_nsec = 500000;
	nanosleep( &ts, NULL );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    }


void AliHLTDetectorSubscriber::ProcessSEDD( const AliHLTSubEventDataDescriptor* sedd, vector<BlockData>& dataBlocks )
    {
    if ( !sedd )
	{
	LOG( AliHLTLog::kError, "AliHLTDetectorSubscriber::ProcessSEDD", "Sub-Event Data Descriptor NULL pointer" )
	    << "Sub-event data descriptor NULL pointer passed," << ENDLOG;
	return;
	}
    if ( sedd->fHeader.fType.fID != ALIL3SUBEVENTDATADESCRIPTOR_TYPE )
	{
	LOG( AliHLTLog::kError, "AliHLTDetectorSubscriber::ProcessSEDD", "Wrong Sub-Event Data Descriptor Data ID" )
	    << "Wrong sub-event data descriptor ID: 0x" << AliHLTLog::kHex << sedd->fDataType.fID
	    << " (" << AliHLTLog::kDec << sedd->fDataType.fID << ") - " 
	    << sedd->fHeader.fType.fDescr[0] << sedd->fHeader.fType.fDescr[1] 
	    << sedd->fHeader.fType.fDescr[2] << sedd->fHeader.fType.fDescr[3] 
	    << "("
	    << sedd->fHeader.fType.fDescr[3] << sedd->fHeader.fType.fDescr[2] 
	    << sedd->fHeader.fType.fDescr[1] << sedd->fHeader.fType.fDescr[0] 
	    << "). Expected 0x" << AliHLTLog::kHex << ALIL3SUBEVENTDATADESCRIPTOR_TYPE
	    << " (" << AliHLTLog::kDec << ALIL3SUBEVENTDATADESCRIPTOR_TYPE << ")."
	    << ENDLOG;
	return;
	}
    AliUInt8_t* ptr;
    AliUInt32_t i, n;
    n = sedd->fDataBlockCount;
    for ( i = 0; i < n; i++ )
	{
	BlockData bd;
	ptr = (AliUInt8_t*)GetShmPtr( sedd->fDataBlocks[i].fShmID );
	if ( !ptr )
	    {
	    LOG( AliHLTLog::kError, "AliHLTDetectorSubscriber::ProcessSEDD", "Shm error" )
		<< "Unable to get pointer to shared memory area with id 0x" << AliHLTLog::kHex
		<< sedd->fDataBlocks[i].fShmID.fKey.fID << " )" << AliHLTLog::kDec << sedd->fDataBlocks[i].fShmID.fKey.fID
		<< ")." << ENDLOG;
	    continue;
	    }
	ptr += sedd->fDataBlocks[i].fBlockOffset;
	if ( sedd->fDataBlocks[i].fDataType.fID == COMPOSITE_DATAID )
	    {
	    ProcessSEDD( (AliHLTSubEventDataDescriptor*)ptr, dataBlocks );
	    continue;
	    }

	bd.fShmID.fShmType = sedd->fDataBlocks[i].fShmID.fShmType;
	bd.fShmID.fKey.fID = sedd->fDataBlocks[i].fShmID.fKey.fID;
	bd.fOffset = sedd->fDataBlocks[i].fBlockOffset;
	bd.fData = ptr;
	bd.fSize = sedd->fDataBlocks[i].fBlockSize;
	bd.fDataType = sedd->fDataBlocks[i].fDataType;
	bd.fDataOrigin = sedd->fDataBlocks[i].fDataOrigin;
	bd.fDataSpecification = sedd->fDataBlocks[i].fDataSpecification;
	for ( int j = 0; j < kAliHLTSEDBDAttributeCount; j++ )
	    bd.fAttributes[j] = sedd->fDataBlocks[i].fAttributes[j];
	bd.fProducerNode = sedd->fDataBlocks[i].fProducerNode;
	dataBlocks.insert( dataBlocks.end(), bd );
	}
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

