/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTProcessingSubscriber.hpp"
#include "AliHLTProcessingRePublisher.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTGetNodeID.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "AliHLTEventDoneData.hpp"
#include "AliHLTHLTEventTriggerData.hpp"
#include "AliHLTEventAccounting.hpp"
#include "AliHLTEventMetaDataOutput.hpp"
#include "MLUCVector.hpp"
#include <errno.h>
#include <iostream>

#define EVENT_CHECK_PARANOIA_LIMIT 8192


#if 0
#define TRACELOG( lvl, orig, keys ) LOG(lvl,orig,keys)
#else
#define TRACELOG( lvl, orig, keys ) LOG(AliHLTLog::kNone,orig,keys)
#endif


AliHLTProcessingSubscriber::AliHLTProcessingSubscriber( const char* name, bool sendEventDone, AliUInt32_t minBlockSize,
						      AliUInt32_t perEventFixedSize, AliUInt32_t perBlockFixedSize, double sizeFactor, int eventCountAlloc ):
    AliHLTSubscriberInterface( name ), fETSCache( (eventCountAlloc<=0) ? 1 : eventCountAlloc, true, false ), 
    fEDDCache( (eventCountAlloc<=0) ? 1 : eventCountAlloc+1, true, false ), fProcessingThread( *this ), 
    //fEventDataCache( sizeof(EventData), (eventCountAlloc<0) ? 4 : eventCountAlloc ),
    fEventDataCache( (eventCountAlloc<0) ? 4 : eventCountAlloc ),
    //fEventDoneCache( (eventCountAlloc<0) ? 4 : eventCountAlloc ),
    fEventData( (eventCountAlloc<0) ? 4 : eventCountAlloc ),
    fEventSignal( eventCountAlloc ), 
    fRetryData( eventCountAlloc, true ), 
    // fOutBlocks( eventCountAlloc, true ), //fInBlocks( eventCountAlloc, true ), 
    fCleanupThread( *this ), fCleanupSignal( (eventCountAlloc<0) ? -1 : (eventCountAlloc-2) ), 
    fTimer( (eventCountAlloc<0) ? -1 : (eventCountAlloc-3) ), fRetryCallback( *this ),
    fEventRateTimerCallback( this )
//     AliHLTSubscriberInterface( name ), fProcessingThread( *this ), fEventSignal(), 
//     fCleanupThread( *this ), fCleanupSignal(), 
//     fRetryCallback( *this )
    //#ifdef ALLOW_ARTIFICIAL_EVENT_LOSS
    , fEventLossModulo ( 0 ), fEventCounter( 0 ),
    fBufferWarnings(true),
    fBuffer20PercentWarning(false),
    fBuffer10PercentWarning(false),
    fEventAccounting(NULL),
    fParanoidOutputBlockChecks(false)
    //#endif
    {
    fMinBlockSize = minBlockSize;
    fPerEventFixedSize = perEventFixedSize;
    fPerBlockFixedSize = perBlockFixedSize;
    fSizeFactor = sizeFactor;
    fMeasurementModeExactRate = true;
    fForwardMaxRetryEvents = true;
    fSecondary = false;
    fEventsPurged = false;
    fForceEventDoneDataForward = false;
    Init( name, sendEventDone, eventCountAlloc );
    }

AliHLTProcessingSubscriber::~AliHLTProcessingSubscriber()
    {
    pthread_mutex_destroy( &fInSEDDMutex );
    pthread_mutex_destroy( &fOutSEDDMutex );
//     pthread_mutex_destroy( &fOutBlockMutex );
//     pthread_mutex_destroy( &fInBlockMutex );
    //pthread_mutex_destroy( &fSignalMutex );
    pthread_mutex_destroy( &fEventRetryMutex );
    pthread_mutex_destroy( &fEventDataMutex );
    pthread_mutex_destroy( &fDoneDataMutex );
    pthread_mutex_destroy( &fAsyncAnnounceMutex );
    pthread_mutex_destroy( &fPublisherCurrentEventDoneMutex );
    }

int AliHLTProcessingSubscriber::NewEvent(  AliHLTPublisherInterface& pubInt, 
					   AliEventID_t eventID, 
					   const AliHLTSubEventDataDescriptor& sbevent,
					   const AliHLTEventTriggerStruct& ets )
    {
    fEventsPurged = false;
    if ( sbevent.fOldestEventBirth_s > fOldestEventBirth_s )
	{
	fOldestEventBirth_s = sbevent.fOldestEventBirth_s;
	CleanupOldEvents();
	}
#ifdef EVENT_CHECK_PARANOIA
    if ( fLastNewEvent!=AliEventID_t() )
	{
	if ( fLastNewEvent+EVENT_CHECK_PARANOIA_LIMIT<eventID || (fLastNewEvent>EVENT_CHECK_PARANOIA_LIMIT && fLastNewEvent-EVENT_CHECK_PARANOIA_LIMIT>eventID) )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTProcessingSubscriber::NewEvent", "Suspicious Event ID" )
		<< "Suspicious Event ID found: 0x" << AliHLTLog::kHex << eventID << " ("
		<< AliHLTLog::kDec << eventID << ") - last received event ID: 0x"
		<< AliHLTLog::kHex << fLastNewEvent << " (" << AliHLTLog::kDec 
		<< fLastNewEvent << ")." << ENDLOG;
	    }
	}
    fLastNewEvent = eventID;
#endif
    if ( fEventAccounting )
	fEventAccounting->NewEvent( pubInt.GetName(), &sbevent );
    int ret;
    //AliHLTSubEventDataDescriptor* sedd;
    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber", "New event" )
	<< "Subscriber " << GetName() << " received notification for event 0x"
	<< AliHLTLog::kHex << eventID << " ("
	<< AliHLTLog::kDec << eventID << ") with "
	<< sbevent.fDataBlockCount << " data blocks." << ENDLOG;
    AliHLTEventDoneData edd;
    if ( !fInDescriptorHandler )
	{
	LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::NewEvent", "No input sedd handler" )
	    << "No input sedd handler to get SEDD. Aborting event." << ENDLOG;
	if ( fPublisher )
	    {
	    AliHLTMakeEventDoneData( &edd, eventID, sbevent.fStatusFlags );
	    fPublisher->EventDone( *this, edd );
	    }
	return 0;
	}

    EventData* ed;
    //ed = (EventData*)fEventDataCache.Get();
    ed = fEventDataCache.Get();
    if ( !ed )
	{
	LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::NewEvent", "Out of memory" )
	    << "Out of memory allocating EventData structure of " << AliHLTLog::kDec
	    << sizeof(EventData) << " bytes for event 0x" << AliHLTLog::kHex << eventID
	    << " (" << AliHLTLog::kDec << eventID << ") from allocation cache." << ENDLOG;
	if ( fPublisher )
	    {
	    AliHLTMakeEventDoneData( &edd, eventID, sbevent.fStatusFlags );
	    fPublisher->EventDone( *this, edd );
	    }
	return 0; // ENOMEM; ??? XXX
	}

    ed->fEventID = eventID;
    ed->fCanceled = false;
    ed->fActive = false;
    ed->fDone = false;
    ed->fAnnounced = false;
    ed->fForceEmptyForward = false;

    ret = pthread_mutex_lock( &fInSEDDMutex );
    if ( ret )
	{
	}
    ed->fSEDD = fInDescriptorHandler->CloneEventDescriptor( sbevent );
    
    ret = pthread_mutex_unlock( &fInSEDDMutex );
    if ( ret )
	{
	}
	
    if ( !ed->fSEDD )
	{
	LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::NewEvent", "No input sedd" )
	    << "No input sedd received from input sedd handler. Aborting event." << ENDLOG;
	//fEventDataCache.Release( (uint8*)ed );
	fEventDataCache.Release( ed );
	if ( fPublisher )
	    {
	    AliHLTMakeEventDoneData( &edd, eventID, sbevent.fStatusFlags );
	    fPublisher->EventDone( *this, edd );
	    }
	return 0; // ENOMEN; ??? XXX
	}
    if ( ets.fDataWordCount>0 )
	{
	//ed->fETS = (AliHLTEventTriggerStruct*) new AliUInt8_t[ ets.fHeader.fLength ];
	ed->fETS = (AliHLTEventTriggerStruct*)fETSCache.Get( ets.fHeader.fLength );
	if ( !ed->fETS )
	    {
	    LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::NewEvent", "Out of memory" )
		<< "Out of memory allocating EventTrigger Data of " << AliHLTLog::kDec
		<< ets.fHeader.fLength << " bytes for event 0x" << AliHLTLog::kHex << eventID
		<< " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
	    }
	else
	    memcpy( ed->fETS, &ets, ets.fHeader.fLength );
	}
    else
	ed->fETS = NULL;

    ret = pthread_mutex_lock( &fEventDataMutex );
    if ( ret )
	{
	}
    fEventData.Add( ed );
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCBusyStateFlag;
	}
    ret = pthread_mutex_unlock( &fEventDataMutex );
    if ( ret )
	{
	}

//     ret = pthread_mutex_lock( &fSignalMutex );
//     if ( ret )
// 	{
// 	}
    if ( fStatus && eventID.fType!=kAliEventTypeTick && eventID.fType!=kAliEventTypeReconfigure && eventID.fType!=kAliEventTypeDCSUpdate )
	{
	fStatus->fHLTStatus.fReceivedEventCount++;
	fStatus->fHLTStatus.fPendingInputEventCount = fRetryData.GetCnt()+fEventSignal.GetNotificationDataCnt()+1;
	fStatus->Touch();
	}
    fEventSignal.SignalEvent( ed );
//     ret = pthread_mutex_unlock( &fSignalMutex );
//     if ( ret )
// 	{
// 	}

    return 0;
    }

int AliHLTProcessingSubscriber::EventCanceled(  AliHLTPublisherInterface&,
					       AliEventID_t eventID )
    {
    int ret;
    unsigned long ndx;
    vector<EventRetryData>::iterator iter, end;
    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::EventCanceled", "Event canceled" )
	<< "Subscriber " << GetName() << " received cancellation for event 0x"
	<< AliHLTLog::kHex << eventID << " ("
	<< AliHLTLog::kDec << eventID << ")." << ENDLOG;

    pthread_mutex_lock( &fEventDataMutex );
	
    if ( MLUCVectorSearcher<EventData*,AliEventID_t>::FindElement( fEventData, &EventDataSearchFunc, eventID, ndx ) )
	{
	fEventData.Get( ndx )->fCanceled = true;
	}
    pthread_mutex_unlock( &fEventDataMutex );

    fEventSignal.Lock();
    
    if ( MLUCVectorSearcher<EventData*,AliEventID_t>::FindElement( fRetryData, &EventDataSearchFunc, eventID, ndx ) )
	{
	fRetryData.Remove( ndx );
	}

    fEventSignal.Unlock();

    if ( fCurrentEvent != eventID )
	{
	LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::EventCanceled", "Removing event" )
	    << "Removing event 0x" << AliHLTLog::kHex << eventID << " ("
	    << AliHLTLog::kDec << eventID << ") from pending event list..." << ENDLOG;
	bool found = false;
	ret = pthread_mutex_lock( &fEventRetryMutex );
	if ( ret )
	    {
	    // XXX
	    }

	iter = fEventRetries.begin();
	end = fEventRetries.end();
	while ( iter != end )
	    {
#if __GNUC__>=3
	    if ( iter.base() && iter->fEventData && iter->fEventData->fEventID==eventID )
#else
	    if ( iter && iter->fEventData && iter->fEventData->fEventID==eventID )
#endif
		{
		if (  !iter->fActive )
		    {
		    LOG( AliHLTLog::kInformational, "AliHLTProcessingSubscriber::EventCanceled", "Removing retry timeout" )
			<< "Removing retry timeout for event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ")." << ENDLOG;
		    fTimer.CancelTimeout( iter->fTimerID );
		    }
		LOG( AliHLTLog::kInformational, "AliHLTProcessingSubscriber::EventCanceled", "Removing retry event" )
		    << "Removing event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ") from retry list." << ENDLOG;
		fEventRetries.erase( iter );
		found = true;
		break;
		}
	    iter++;
	    }

	ret = pthread_mutex_unlock( &fEventRetryMutex );
	if ( ret )
	    {
	    // XXX
	    }

	if ( !found )
	    {
// 	    ret = pthread_mutex_lock( &fSignalMutex );
// 	    if ( ret )
// 		{
// 		}
	    fEventSignal.Lock();
	    
	    fEventSignal.RemoveEvent( eventID );
	    
	    fEventSignal.Unlock();

// 	    ret = pthread_mutex_unlock( &fSignalMutex );
// 	    if ( ret )
// 		{
// 		}
	    }
	SignalCleanupInEvent( eventID );
	}
    else
	{
	LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::EventCanceled", "Removing event" )
	    << "Removing current event 0x" << AliHLTLog::kHex << eventID << " ("
	    << AliHLTLog::kDec << eventID << ")..." << ENDLOG;
	AbortProcessing();
	fCurrentEvent = AliEventID_t();
	}
    return 0;
    }

int AliHLTProcessingSubscriber::EventDoneData( AliHLTPublisherInterface&, 
					       const AliHLTEventDoneData& )
    {
    return 0;
    }

bool AliHLTProcessingSubscriber::GetAnnouncedEventData( AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
							AliHLTEventTriggerStruct*& trigger )
    {
    sedd = NULL;
    trigger = NULL;
    unsigned long ndx;
    EventData* ed = NULL;
    int ret;

    if ( fOutDescriptorHandler )
	{
	ret = pthread_mutex_lock( &fOutSEDDMutex );
	if ( ret )
	    {
	    }
	if ( fOutDescriptorHandler->FindSEDD( eventID, ndx ) )
	    sedd = fOutDescriptorHandler->GetEventDescriptor( ndx );
	ret = pthread_mutex_unlock( &fOutSEDDMutex );
	if ( ret )
	    {
	    }
	}
    else
	{
	}
    if ( !sedd )
	return false;
    ret = pthread_mutex_lock( &fEventDataMutex );
    if ( ret )
	{
	}
    if ( MLUCVectorSearcher<EventData*,AliEventID_t>::FindElement( fEventData, &EventDataSearchFunc, eventID, ndx ) )
	{
	ed = fEventData.Get( ndx );
	//fEventData.Remove( ndx );
	}
    ret = pthread_mutex_unlock( &fEventDataMutex );
    if ( ret )
	{
	}
    if ( ed && ed->fETS )
	{
	trigger = ed->fETS;
	return true;
	}
    sedd = NULL;
	
    return false;
    }


int AliHLTProcessingSubscriber::SubscriptionCanceled( AliHLTPublisherInterface& )
    {
    LOG( AliHLTLog::kInformational, "AliHLTProcessingSubscriber", "Subscription canceled" )
	<< "Subscriber " << GetName() << " received cancellation for subscription."
	<< ENDLOG;

    TRACELOG(AliHLTLog::kDebug, "AliHLTProcessingSubscriber::SubscriptionCanceled", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    if ( fCurrentEvent == AliEventID_t() )
	{
    TRACELOG(AliHLTLog::kDebug, "AliHLTProcessingSubscriber::SubscriptionCanceled", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	AbortProcessing();
    TRACELOG(AliHLTLog::kDebug, "AliHLTProcessingSubscriber::SubscriptionCanceled", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	}
    TRACELOG(AliHLTLog::kDebug, "AliHLTProcessingSubscriber::SubscriptionCanceled", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    QuitProcessing();
    TRACELOG(AliHLTLog::kDebug, "AliHLTProcessingSubscriber::SubscriptionCanceled", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    return 0;
    }

int AliHLTProcessingSubscriber::ReleaseEventsRequest( AliHLTPublisherInterface& )
    {
    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber", "Release events request" )
	<< "Subscriber " << GetName() << " received events release request."
	<< ENDLOG;
    if ( fRePublisher )
	fRePublisher->RequestEventRelease();
    fEventSignal.Signal();
    return 0;
    }

int AliHLTProcessingSubscriber::Ping( AliHLTPublisherInterface& publisher )
    {
    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber", "Subscriber pinged" )
	<< "Subscriber " << GetName() << " received ping message"
	<< ENDLOG;
    int ret;
    ret = publisher.PingAck( *this );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber", "Subscriber pinged" )
	    << "Subscriber " << GetName() << " error sending ping ack message to publisher "
	    << publisher.GetName() << ". Reason: " << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	return EIO;
	}
    return 0;
    }

int AliHLTProcessingSubscriber::PingAck( AliHLTPublisherInterface& )
    {
    fPingCount--;
    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber", "Subscriber ping ack'ed" )
	<< "Subscriber " << GetName() << " received ping ack message. Ping count: "
	<< AliHLTLog::kDec << fPingCount << "." << ENDLOG;
    return 0;
    }

void AliHLTProcessingSubscriber::PingPublisher( AliHLTPublisherInterface& pub )
    {
    fPingCount++;
    int ret;
    ret = pub.Ping( *this );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTProcessingPublisher", "Pinging publisher" )
	    << "Subscriber " << GetName() << " error sending ping message to publisher "
	    << pub.GetName() << ". Ping count: " << AliHLTLog::kDec << fPingCount << "." << ENDLOG;
	}
    }

void AliHLTProcessingSubscriber::EventRateTimerExpired()
    {
    if ( !fStatus )
	return;
    unsigned long long curRecvCount = fStatus->fHLTStatus.fReceivedEventCount;
    unsigned long long curProcCount = fStatus->fHLTStatus.fProcessedEventCount;
    unsigned long long curAnnCount = fStatus->fHLTStatus.fAnnouncedEventCount;

    unsigned long long curInputData = fStatus->fHLTStatus.fTotalProcessedInputDataSize;
    unsigned long long curOutputData = fStatus->fHLTStatus.fTotalProcessedOutputDataSize;
    struct timeval now;
    double tdiff, avgtdiff, resumetdiff;

    if ( fMeasurementModeExactRate )
	{
#if 0
	LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::EventRateTimerExpired", "TRACE" ) 
	    << AliHLTLog::kDec
	    << fStartTime.tv_sec << " s + " << fStartTime.tv_usec << " us: "
	    << " - curRecvCount: " << curRecvCount
	    << " - curProcCount: " << curProcCount
	    << " - curAnnCount: " << curAnnCount
	    << " - curInputData: " << curInputData
	    << " - curOutputData: " << curOutputData
	    << ENDLOG;
#endif
	if ( !fStartTime.tv_sec && !fStartTime.tv_usec && curRecvCount>0 )
	    {
	    gettimeofday( &fStartTime, NULL );
	    fLastMeasurementTime = fStartTime;
	    fResumeTime = fStartTime;
	    fStartMeasurementResumeReceiveEventCount = fStartMeasurementReceiveEventCount = curRecvCount;
	    fStartMeasurementResumeProcessedEventCount = fStartMeasurementProcessedEventCount = curProcCount;
	    fStartMeasurementResumeAnnounceEventCount = fStartMeasurementAnnounceEventCount = curAnnCount;
	    fStartMeasurementResumeTotalProcessedInputDataSize = fStartMeasurementTotalProcessedInputDataSize = curInputData;
	    fStartMeasurementResumeTotalProcessedOutputDataSize = fStartMeasurementTotalProcessedOutputDataSize = curOutputData;
	    LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::EventRateTimerExpired", "Event Rate Results" )
		<< "Discarding from rate measurements: "
		<< curRecvCount << " received events - "
		<< curProcCount << " processed events - "
		<< curAnnCount << " announced events - "
		<< curInputData << " received bytes - "
		<< curOutputData << " announced bytes."
		<< ENDLOG;
	    //return;
	    }
	curRecvCount -= fStartMeasurementReceiveEventCount;
	curProcCount -= fStartMeasurementProcessedEventCount;
	curAnnCount -= fStartMeasurementAnnounceEventCount;
	curInputData -= fStartMeasurementTotalProcessedInputDataSize;
	curOutputData -= fStartMeasurementTotalProcessedOutputDataSize;
	}

#if 0
    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::EventRateTimerExpired", "TRACE" ) 
	<< AliHLTLog::kDec
	<< fStartTime.tv_sec << " s + " << fStartTime.tv_usec << " us: "
	<< " - curRecvCount: " << curRecvCount
	<< " - curProcCount: " << curProcCount
	<< " - curAnnCount: " << curAnnCount
	<< " - curInputData: " << curInputData
	<< " - curOutputData: " << curOutputData
	<< ENDLOG;
#endif

    do
	{
	gettimeofday( &now, NULL );
	}
    while ( (now.tv_sec==fLastMeasurementTime.tv_sec && now.tv_usec==fLastMeasurementTime.tv_usec) ||
	    (now.tv_sec==fStartTime.tv_sec && now.tv_usec==fStartTime.tv_usec) ||
	    (now.tv_sec==fResumeTime.tv_sec && now.tv_usec==fResumeTime.tv_usec) );

    tdiff = now.tv_sec;
    tdiff -= fLastMeasurementTime.tv_sec;
    tdiff *=1000000;
    tdiff += now.tv_usec;
    tdiff -= fLastMeasurementTime.tv_usec;
    avgtdiff = now.tv_sec;
    avgtdiff -= fStartTime.tv_sec;
    avgtdiff *=1000000;
    avgtdiff += now.tv_usec;
    avgtdiff -= fStartTime.tv_usec;
    resumetdiff = now.tv_sec;
    resumetdiff -= fResumeTime.tv_sec;
    resumetdiff *=1000000;
    resumetdiff += now.tv_usec;
    resumetdiff -= fResumeTime.tv_usec;

    fStatus->fHLTStatus.fCurrentReceiveRate = (((double)(curRecvCount-fLastMeasurementReceiveEventCount))/tdiff)*1000000.0;
    fStatus->fHLTStatus.fAvgReceiveRate = (((double)curRecvCount)/avgtdiff)*1000000.0;
    fStatus->fHLTStatus.fResumedAvgReceiveRate = (((double)(curRecvCount-fStartMeasurementResumeReceiveEventCount))/resumetdiff)*1000000.0;
    fStatus->fHLTStatus.fCurrentReceivedEventCount = curRecvCount-fLastMeasurementReceiveEventCount;

    fStatus->fHLTStatus.fCurrentProcessRate = (((double)(curProcCount-fLastMeasurementProcessedEventCount))/tdiff)*1000000.0;
    fStatus->fHLTStatus.fAvgProcessRate = (((double)curProcCount)/avgtdiff)*1000000.0;
    fStatus->fHLTStatus.fResumedAvgProcessRate = (((double)(curProcCount-fStartMeasurementResumeProcessedEventCount))/resumetdiff)*1000000.0;
    fStatus->fHLTStatus.fCurrentProcessedEventCount = curProcCount-fLastMeasurementProcessedEventCount;

    fStatus->fHLTStatus.fCurrentAnnounceRate = (((double)(curAnnCount-fLastMeasurementAnnounceEventCount))/tdiff)*1000000.0;
    fStatus->fHLTStatus.fAvgAnnounceRate = (((double)curAnnCount)/avgtdiff)*1000000.0;
    fStatus->fHLTStatus.fResumedAvgAnnounceRate = (((double)(curAnnCount-fStartMeasurementResumeAnnounceEventCount))/resumetdiff)*1000000.0;
    fStatus->fHLTStatus.fCurrentAnnouncedEventCount = curAnnCount-fLastMeasurementAnnounceEventCount;

    fStatus->fHLTStatus.fCurrentProcessedInputDataRate = (((double)(curInputData-fLastMeasurementTotalProcessedInputDataSize))/tdiff)*1000000.0;
    fStatus->fHLTStatus.fAvgProcessedInputDataRate = (((double)curInputData)/avgtdiff)*1000000.0;
    fStatus->fHLTStatus.fResumedProcessedInputDataRate = (((double)(curInputData-fStartMeasurementResumeTotalProcessedInputDataSize))/resumetdiff)*1000000.0;
    fStatus->fHLTStatus.fCurrentProcessedInputDataSize = curInputData-fLastMeasurementTotalProcessedInputDataSize;

    fStatus->fHLTStatus.fCurrentProcessedOutputDataRate = (((double)(curOutputData-fLastMeasurementTotalProcessedOutputDataSize))/tdiff)*1000000.0;
    fStatus->fHLTStatus.fAvgProcessedOutputDataRate = (((double)curOutputData)/avgtdiff)*1000000.0;
    fStatus->fHLTStatus.fResumedProcessedOutputDataRate = (((double)(curOutputData-fStartMeasurementResumeTotalProcessedOutputDataSize))/resumetdiff)*1000000.0;
    fStatus->fHLTStatus.fCurrentProcessedOutputDataSize = curOutputData-fLastMeasurementTotalProcessedOutputDataSize;

    fStatus->fHLTStatus.fTotalInput2OutputDataSizeRatio = ((double)curInputData) / ((double)curOutputData);
    fStatus->fHLTStatus.fCurrentInput2OutputDataSizeRatio = ((double)fStatus->fHLTStatus.fCurrentProcessedInputDataSize) / ((double)fStatus->fHLTStatus.fCurrentProcessedOutputDataSize);


    LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::EventRateTimerExpired", "Event Rate Results" )
	<< "Current Event Receive Rate during last " << AliHLTLog::kDec << tdiff << " milliseconds ("
	<< curRecvCount-fLastMeasurementReceiveEventCount << " events): "
	<< fStatus->fHLTStatus.fCurrentReceiveRate << " Hz == " 
	<< (fStatus->fHLTStatus.fCurrentReceiveRate/1000.0) << " kHz == " 
	<< (fStatus->fHLTStatus.fCurrentReceiveRate/1000000.0) << " MHz."
	<< ENDLOG;
    LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::EventRateTimerExpired", "Event Rate Results" )
	<< "Total Global Event Receive Rate after " << AliHLTLog::kDec << (curRecvCount+fStartMeasurementReceiveEventCount) << " events: "
	<< fStatus->fHLTStatus.fAvgReceiveRate << " Hz == " 
	<< (fStatus->fHLTStatus.fAvgReceiveRate/1000.0) << " kHz == " 
	<< (fStatus->fHLTStatus.fAvgReceiveRate/1000000.0) << " MHz."
	<< ENDLOG;
    LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::EventRateTimerExpired", "Event Rate Results" )
	<< "Global Event Receive Rate since last resume after " << AliHLTLog::kDec << (curRecvCount-fStartMeasurementResumeReceiveEventCount+fStartMeasurementReceiveEventCount) << " events: "
	<< fStatus->fHLTStatus.fResumedAvgReceiveRate << " Hz == " 
	<< (fStatus->fHLTStatus.fResumedAvgReceiveRate/1000.0) << " kHz == " 
	<< (fStatus->fHLTStatus.fResumedAvgReceiveRate/1000000.0) << " MHz."
	<< ENDLOG;

    LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::EventRateTimerExpired", "Event Rate Results" )
	<< "Current Event Process Rate during last " << AliHLTLog::kDec << tdiff << " milliseconds ("
	<< curProcCount-fLastMeasurementProcessedEventCount << " events): "
	<< fStatus->fHLTStatus.fCurrentProcessRate << " Hz == " 
	<< (fStatus->fHLTStatus.fCurrentProcessRate/1000.0) << " kHz == " 
	<< (fStatus->fHLTStatus.fCurrentProcessRate/1000000.0) << " MHz."
	<< ENDLOG;
    LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::EventRateTimerExpired", "Event Rate Results" )
	<< "Total Global Event Process Rate after " << AliHLTLog::kDec << (curProcCount+fStartMeasurementProcessedEventCount) << " events: "
	<< fStatus->fHLTStatus.fAvgProcessRate << " Hz == " 
	<< (fStatus->fHLTStatus.fAvgProcessRate/1000.0) << " kHz == " 
	<< (fStatus->fHLTStatus.fAvgProcessRate/1000000.0) << " MHz."
	<< ENDLOG;
    LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::EventRateTimerExpired", "Event Rate Results" )
	<< "Global Event Process Rate since last resume after " << AliHLTLog::kDec << (curProcCount-fStartMeasurementResumeProcessedEventCount+fStartMeasurementProcessedEventCount) << " events: "
	<< fStatus->fHLTStatus.fResumedAvgProcessRate << " Hz == " 
	<< (fStatus->fHLTStatus.fResumedAvgProcessRate/1000.0) << " kHz == " 
	<< (fStatus->fHLTStatus.fResumedAvgProcessRate/1000000.0) << " MHz."
	<< ENDLOG;

    if ( fRePublisher )
	{
	LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::EventRateTimerExpired", "Event Rate Results" )
	    << "Current Event Announce Rate during last " << AliHLTLog::kDec << tdiff << " milliseconds ("
	    << curAnnCount-fLastMeasurementAnnounceEventCount << " events): "
	    << fStatus->fHLTStatus.fCurrentAnnounceRate << " Hz == " 
	    << (fStatus->fHLTStatus.fCurrentAnnounceRate/1000.0) << " kHz == " 
	    << (fStatus->fHLTStatus.fCurrentAnnounceRate/1000000.0) << " MHz."
	    << ENDLOG;
	LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::EventRateTimerExpired", "Event Rate Results" )
	    << "Total Global Event Announce Rate after " << AliHLTLog::kDec << (curAnnCount+fStartMeasurementAnnounceEventCount) << " events: "
	    << fStatus->fHLTStatus.fAvgAnnounceRate << " Hz == " 
	    << (fStatus->fHLTStatus.fAvgAnnounceRate/1000.0) << " kHz == " 
	    << (fStatus->fHLTStatus.fAvgAnnounceRate/1000000.0) << " MHz."
	    << ENDLOG;
	LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::EventRateTimerExpired", "Event Rate Results" )
	    << "Global Event Announce Rate since last resume after " << AliHLTLog::kDec << (curAnnCount-fStartMeasurementResumeAnnounceEventCount+fStartMeasurementAnnounceEventCount) << " events: "
	    << fStatus->fHLTStatus.fResumedAvgAnnounceRate << " Hz == " 
	    << (fStatus->fHLTStatus.fResumedAvgAnnounceRate/1000.0) << " kHz == " 
	    << (fStatus->fHLTStatus.fResumedAvgAnnounceRate/1000000.0) << " MHz."
	    << ENDLOG;
	}

    fStatus->Touch();

    fLastMeasurementTime = now;

    fLastMeasurementReceiveEventCount = curRecvCount;
    fLastMeasurementProcessedEventCount = curProcCount;
    fLastMeasurementAnnounceEventCount = curAnnCount;

    fLastMeasurementTotalProcessedInputDataSize = curInputData;
    fLastMeasurementTotalProcessedOutputDataSize = curOutputData;

    fTimer.AddTimeout( fStatusRateUpdateTime_ms, &fEventRateTimerCallback, 0 );
    }


void AliHLTProcessingSubscriber::PauseProcessing()
    {
    fPaused = true;
    fEventSignal.Signal();
    }
void AliHLTProcessingSubscriber::ResumeProcessing()
    {
    fPaused = false;
    gettimeofday( &fResumeTime, NULL );
    fStartMeasurementResumeReceiveEventCount = fStatus->fHLTStatus.fReceivedEventCount;
    fStartMeasurementResumeProcessedEventCount = fStatus->fHLTStatus.fProcessedEventCount;
    fStartMeasurementResumeAnnounceEventCount = fStatus->fHLTStatus.fAnnouncedEventCount;
    fStartMeasurementResumeTotalProcessedInputDataSize = fStatus->fHLTStatus.fTotalProcessedInputDataSize;
    fStartMeasurementResumeTotalProcessedOutputDataSize = fStatus->fHLTStatus.fTotalProcessedOutputDataSize;
    fStatus->Touch();
    fEventSignal.Signal();
    }


void AliHLTProcessingSubscriber::ProcessSEDD( const AliHLTSubEventDataDescriptor* sedd, vector<AliHLTSubEventDescriptor::BlockData>& dataBlocks )
    {
#if 1
    AliHLTSubEventDescriptor desc( sedd );
    desc.Dereference( fShmManager, dataBlocks );
#else
    if ( !sedd )
	{
	LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::ProcessSEDD", "Sub-Event Data Descriptor NULL pointer" )
	    << "Sub-event data descriptor NULL pointer passed," << ENDLOG;
	return;
	}
    if ( !fShmManager )
	{
	LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::ProcessSEDD", "No shm manmager" )
	    << "No shm manager object to obtain shm buffers." << ENDLOG;
	return;
	}
    if ( sedd->fHeader.fType.fID != ALIL3SUBEVENTDATADESCRIPTOR_TYPE )
	{
	LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::ProcessSEDD", "Wrong Sub-Event Data Descriptor Data ID" )
	    << "Wrong sub-event data descriptor ID: 0x" << AliHLTLog::kHex << sedd->fDataType.fID
	    << " (" << AliHLTLog::kDec << sedd->fDataType.fID << ") - " 
	    << sedd->fHeader.fType.fDescr[0] << sedd->fHeader.fType.fDescr[1] 
	    << sedd->fHeader.fType.fDescr[2] << sedd->fHeader.fType.fDescr[3] 
	    << "("
	    << sedd->fHeader.fType.fDescr[3] << sedd->fHeader.fType.fDescr[2] 
	    << sedd->fHeader.fType.fDescr[1] << sedd->fHeader.fType.fDescr[0] 
	    << "). Expected 0x" << AliHLTLog::kHex << ALIL3SUBEVENTDATADESCRIPTOR_TYPE
	    << " (" << AliHLTLog::kDec << ALIL3SUBEVENTDATADESCRIPTOR_TYPE << ")."
	    << ENDLOG;
	return;
	}
    AliUInt8_t* ptr=NULL;
    AliUInt32_t i, n;
    n = sedd->fDataBlockCount;
    for ( i = 0; i < n; i++ )
	{
	AliHLTSubEventDescriptor::BlockData bd;
	ptr = fShmManager->GetShm( sedd->fDataBlocks[i].fShmID );
	if ( !ptr )
	    {
	    LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::ProcessSEDD", "Shm error" )
		<< "Unable to get pointer to shared memory area with id 0x" << AliHLTLog::kHex
		<< sedd->fDataBlocks[i].fShmID.fKey.fID << " )" << AliHLTLog::kDec << sedd->fDataBlocks[i].fShmID.fKey.fID
		<< ")." << ENDLOG;
	    continue;
	    }
	ptr += sedd->fDataBlocks[i].fBlockOffset;
	if ( sedd->fDataBlocks[i].fDataType.fID == COMPOSITE_DATAID )
	    {
	    ProcessSEDD( (AliHLTSubEventDataDescriptor*)ptr, dataBlocks );
	    continue;
	    }

	bd.fShmKey.fShmType = sedd->fDataBlocks[i].fShmID.fShmType;
	bd.fShmKey.fKey.fID = sedd->fDataBlocks[i].fShmID.fKey.fID;
	bd.fOffset = sedd->fDataBlocks[i].fBlockOffset;
	bd.fData = ptr;
	bd.fSize = sedd->fDataBlocks[i].fBlockSize;
	bd.fDataType = sedd->fDataBlocks[i].fDataType;
	bd.fProducerNode = sedd->fDataBlocks[i].fProducerNode;
	bd.fDataOrigin.fID = sedd->fDataBlocks[i].fDataOrigin.fID;
	// Data origin is just a 32 bit number.
	bd.fDataSpecification = sedd->fDataBlocks[i].fDataSpecification;
	bd.fStatusFlags = sedd->fDataBlocks[i].fStatusFlags;
	//bd.fByteOrder = sedd->fDataBlocks[i].fByteOrder;
	for ( int j = 0; j < kAliHLTSEDBDAttributeCount; j++ )
	    bd.fAttributes[j] = sedd->fDataBlocks[i].fAttributes[j];
	dataBlocks.insert( dataBlocks.end(), bd );
	}
#endif
    }


void AliHLTProcessingSubscriber::EventDoneDataReceived( const char* /*subscriberName*/, AliEventID_t eventID,
							const AliHLTEventDoneData* edd, bool forceForward, bool forwarded )
    {
    if ( fEventAccounting )
	fEventAccounting->EventFinished( fName.c_str(), eventID, forceForward ? 2 : 1, forwarded ? 2 : 1 );
    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::EventDoneDataReceived", "EventDoneData Received" )
	    << "EventDoneData Received for event 0x" << AliHLTLog::kHex << edd->fEventID
	    << " (" << AliHLTLog::kDec << edd->fEventID << ")" << ENDLOG;
    if ( forceForward && fPublisher )
	{
	LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::EventDoneDataReceived", "Forwarding EventDoneData" )
	    << "Force-forwarding EventDoneData for event 0x" << AliHLTLog::kHex << edd->fEventID
	    << " (" << AliHLTLog::kDec << edd->fEventID << ")" << ENDLOG;
	if ( fEventAccounting )
	    fEventAccounting->EventFinished( fPublisher->GetName(), eventID, forceForward ? 2 : 1, 2 );
	fPublisher->EventDone( *this, *edd, forceForward, true );
	}
    }


void AliHLTProcessingSubscriber::PublisherEventDone( AliEventID_t eventID, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    if ( !fSendEventDone && !fEventsPurged )
	SignalCleanupInEvent( eventID, eventDoneData );

    //CleanupOutEvent( eventID );
    pthread_mutex_lock( &fPublisherCurrentEventDoneMutex );
    if ( fPublisherCurrentEvent == eventID )
	fPublisherCurrentEventDone = true;
    pthread_mutex_unlock( &fPublisherCurrentEventDoneMutex );
    }

void AliHLTProcessingSubscriber::PURGEALLEVENTS()
    {
    // fEventDataCache, fInDescriptorHandler, fETSCache, fEventData, fRetryData, fEventSignal, fOutDescriptorHandler (fRePublisher), event done data (fEDDCache), fCleanupSignal, fShmManager, ed->fInBlocks, ed->fOutBlocks, fBufferManager, 
    fEventsPurged = true;
    fEventSignal.Lock();
    while ( fEventSignal.HaveNotificationData() )
	fEventSignal.PopNotificationData();
    fRetryData.Clear();
    fEventSignal.Unlock();

    fCleanupSignal.Lock();
    while ( fCleanupSignal.HaveNotificationData() )
	{
	AliHLTEventDoneData* edd = fCleanupSignal.PopNotificationData();
	fEDDCache.Release( (uint8*)edd );
	}
    fCleanupSignal.Unlock();

    std::vector<PEventData> events;
    int ret;
    ret = pthread_mutex_lock( &fEventDataMutex );
    if ( ret )
	{
	}
    events.reserve( fEventData.GetCnt() );
    while ( fEventData.GetCnt()>0 )
	{
	EventData* ed = fEventData.GetFirst();
	fEventData.RemoveFirst();
	if ( ed )
	    events.push_back( ed );
	}
    ret = pthread_mutex_unlock( &fEventDataMutex );
    if ( ret )
	{
	}

    std::vector<PEventData>::iterator evtIter, evtEnd;
    evtIter = events.begin();
    evtEnd = events.end();
    while ( evtIter != evtEnd )
	{
	EventData* ed = *evtIter;
	if ( ed->fAnnounced )
	    {
	    ret = pthread_mutex_lock( &fOutSEDDMutex );
	    if ( ret )
		{
		}
	    fOutDescriptorHandler->ReleaseEventDescriptor( ed->fEventID );
	    ret = pthread_mutex_unlock( &fOutSEDDMutex );
	    if ( ret )
		{
		}
	    }
	ret = pthread_mutex_lock( &fInSEDDMutex );
	if ( ret )
	    {
	    }
	fInDescriptorHandler->ReleaseEventDescriptor( ed->fEventID );
	ret = pthread_mutex_unlock( &fInSEDDMutex );
	if ( ret )
	    {
	    }
	InBlockData* inIter;
	while ( ed->fInBlocks.GetCnt() )
	    {
	    inIter = ed->fInBlocks.GetFirstPtr();
	    if ( fShmManager )
		fShmManager->ReleaseShm( inIter->fShmKey );
	    ed->fInBlocks.RemoveFirst();
	    }
	OutBlockData* outIter;
	if ( fRePublisher )
	    {
	    while ( ed->fOutBlocks.GetCnt() )
		{
		outIter = ed->fOutBlocks.GetFirstPtr();
		if ( fShmManager && outIter->fShmKey.fShmType != kInvalidShmType )
		    fShmManager->ReleaseShm( outIter->fShmKey );
		if ( fBufferManager && outIter->fBufferNdx!=~(AliUInt32_t)0 && outIter->fOffset!=~(AliUInt32_t)0 )
		    fBufferManager->ReleaseBlock( outIter->fBufferNdx, outIter->fOffset );
		ed->fOutBlocks.RemoveFirst();
		}
	    }
	if ( ed->fETS )
	    {
	    fETSCache.Release( (uint8*)ed->fETS );  //delete [] (AliUInt8_t*)ed->fETS;
	    ed->fETS = NULL;
	    }
	fEventDataCache.Release( ed );
	evtIter++;
	}
    if ( fBufferManager )
	{
	if ( fStatus )
	    {
	    fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
	    fStatus->Touch();
	    }
	if ( fBufferWarnings )
	    {
	    double freeRatio = (((double)fBufferManager->GetFreeBufferSize())/((double)fBufferManager->GetTotalBufferSize()));
	    if ( freeRatio>0.3 )
		{
		fBuffer20PercentWarning = false;
		fBuffer10PercentWarning = false;
		}
	    }
	}
    if ( fStatus )
	{
	fStatus->fHLTStatus.fPendingInputEventCount = fStatus->fHLTStatus.fPendingOutputEventCount = 0;
	fStatus->Touch();
	}
    }

void AliHLTProcessingSubscriber::DumpEventMetaData()
    {
    int ret;
    ret = pthread_mutex_lock( &fEventDataMutex );
    if ( ret )
	{
	}
    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::DumpEventMetaData", "Event meta data dump" )
	<< "Dumping all current events to file." << ENDLOG;
    
    // We need to get the list of all current pending events, fetch their sub event descriptors,
    // dereference the shared memory pointers to all the data blocks and then write the
    // blocks to disk.
    MLUCString prefix;
    prefix = GetName();
    prefix += "-Pending";
    AliHLTEventMetaDataOutput metaDataOutputPending( prefix.c_str(), fAliceHLT, fRunNumber );

    if ( fAliceHLT )
	metaDataOutputPending.SetEventTriggerDataFormatFunction(  AliHLTHLEventTriggerData2String );

    ret = metaDataOutputPending.Open();
    if ( ret!=0 )
	{
	LOG( AliHLTLog::kWarning, "AliHLTProcessingSubscriber::DumpEventMetaData", "Error opening event meta data output file" )
	    << "Error opening event meta data output file '" << prefix.c_str() << "': " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return;
	}

    MLUCVector<PEventData>::TIterator eventIter;
    eventIter = fEventData.Begin();
    while ( eventIter )
	{
	PEventData ed = *eventIter;
	if ( !ed->fDone )
	    metaDataOutputPending.WriteEvent( ed->fEventID, ed->fSEDD, ed->fETS, "pending" );
	++eventIter;
	}

    metaDataOutputPending.Close();

    prefix = GetName();
    prefix += "-Processed";
    AliHLTEventMetaDataOutput metaDataOutputProcessed( prefix.c_str(), fAliceHLT, fRunNumber );

    if ( fAliceHLT )
	metaDataOutputProcessed.SetEventTriggerDataFormatFunction(  AliHLTHLEventTriggerData2String );

    ret = metaDataOutputProcessed.Open();
    if ( ret!=0 )
	{
	LOG( AliHLTLog::kWarning, "AliHLTProcessingSubscriber::DumpEventMetaData", "Error opening event meta data output file" )
	    << "Error opening event meta data output file '" << prefix.c_str() << "': " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return;
	}

    eventIter = fEventData.Begin();
    while ( eventIter )
	{
	PEventData ed = *eventIter;
	if ( ed->fDone )
	    metaDataOutputProcessed.WriteEvent( ed->fEventID, ed->fSEDD, ed->fETS, "processed" );
	++eventIter;
	}

    metaDataOutputProcessed.Close();

    
    if (fEventData.GetSize() == 0)
        {
        LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::DumpEventMetaData", "Nothing to dump" )
	    << "There were no events found to be dumped to disk" << ENDLOG;
	}


    ret = pthread_mutex_unlock( &fEventDataMutex );
    if ( ret )
	{
	}
    }



void AliHLTProcessingSubscriber::SignalCleanupInEvent( AliEventID_t eventID, vector<AliHLTEventDoneData*>& origEventDoneData )
    {
    vector<AliHLTEventDoneData*> eventDoneData( origEventDoneData );
    unsigned long tLength = 0;
//     unsigned long offset = 0, tWords = 0;
    //unsigned long tWords = 0;
    //vector<AliHLTEventDoneData*>::iterator eddIter, eddEnd;
    AliHLTEventDoneData* eddOwn;
    eddOwn = GetEventDoneData( eventID );
    if ( eddOwn )
	eventDoneData.insert( eventDoneData.end(), eddOwn );
//     eddIter = eventDoneData.begin();
//     eddEnd = eventDoneData.end();
//     while ( eddIter != eddEnd )
// 	{
// 	tWords += (*eddIter)->fDataWordCount;
// 	eddIter++;
// 	}
//     tLength = fDefaultEDD.fHeader.fLength + tWords*sizeof((*eddIter)->fDataWords[0]);

    uint8 waterMarkEDDBuffer[sizeof(AliHLTEventDoneData)+sizeof(eddOwn->fDataWords[0])*6];
    bool storedWaterMark = CreateWaterMarkEDD( reinterpret_cast<AliHLTEventDoneData*>(waterMarkEDDBuffer), eventID, true );
    if ( storedWaterMark )
	eventDoneData.insert( eventDoneData.end(), reinterpret_cast<AliHLTEventDoneData*>(waterMarkEDDBuffer) );
#if 1
    if ( storedWaterMark )
	{
	LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::SignalCleanupInEvent", "Sending water mark message" )
	    << "Created water mark message for event 0x" << AliHLTLog::kHex
	    << eventID << " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
	}
#endif

    tLength = AliHLTGetMergedEventDoneDataSize( eventDoneData );
    AliHLTEventDoneData* edd;
    if ( tLength<=sizeof(AliHLTEventDoneData) )
	{
	edd = (AliHLTEventDoneData*)fEDDCache.Get( sizeof(AliHLTEventDoneData) );
	if ( !edd )
	    {
	    LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::SignalCleanupInEvent", "Out of memory" )
		<< "Out of memory trying to allocate default event done data for event 0x" << AliHLTLog::kHex
		<< eventID << " (" << AliHLTLog::kDec << eventID << ") of " << sizeof(AliHLTEventDoneData) 
		<< " bytes from EventDoneData cache." << ENDLOG;
	    return;
	    }
	else
	    memcpy( edd, &fDefaultEDD, sizeof(AliHLTEventDoneData) );
	}
    else
	{
	edd = (AliHLTEventDoneData*)fEDDCache.Get( tLength );
	if ( !edd )
	    {
	    LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::SignalCleanupInEvent", "Out of memory" )
		<< "Out of memory trying to allocate event done data for event 0x" << AliHLTLog::kHex
		<< eventID << " (" << AliHLTLog::kDec << eventID << ") of " << tLength 
		<< " bytes. Continuing without actual EventDoneData..." << ENDLOG;
	    edd = (AliHLTEventDoneData*)fEDDCache.Get( sizeof(AliHLTEventDoneData) );
	    if ( !edd )
		{
		LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::SignalCleanupInEvent", "Out of memory" )
		    << "Out of memory trying to allocate default event done data for event 0x" << AliHLTLog::kHex
		    << eventID << " (" << AliHLTLog::kDec << eventID << ") of " << sizeof(AliHLTEventDoneData) 
		    << " bytes from EventDoneData cache. Aborting Event!!!" << ENDLOG;
		return;
		}
	    else
		memcpy( edd, &fDefaultEDD, sizeof(AliHLTEventDoneData) );
	    }
	else
	    {
	    AliHLTMergeEventDoneData( eventID, edd, eventDoneData );
// 	    memcpy( edd, &fDefaultEDD, sizeof(AliHLTEventDoneData) );
// 	    edd->fHeader.fLength = tLength;
// 	    edd->fDataWordCount = tWords;
// 	    eddIter = eventDoneData.begin();
// 	    eddEnd = eventDoneData.end();
// 	    offset = 0;
// 	    while ( eddIter != eddEnd )
// 		{
// 		memcpy( edd->fDataWords+offset, (*eddIter)->fDataWords, (*eddIter)->fDataWordCount*sizeof((*eddIter)->fDataWords[0]) );
// 		offset += (*eddIter)->fDataWordCount;
// 		eddIter++;
// 		}
	    }
	}
#if 1
    eventDoneData.clear();
    if ( eddOwn )
	FreeEventDoneData( eddOwn );
#else
    if ( storedWaterMark )
	eventDoneData.erase( eventDoneData.rbegin().base() );
    if ( eddOwn )
	{
	eventDoneData.erase( eventDoneData.rbegin().base() );
	FreeEventDoneData( eddOwn );
	}
#endif
    edd->fEventID = eventID;

    fCleanupSignal.AddNotificationData( edd );
    
    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::SignalCleanupInEvent(AliEventID_t,vector<AliHLTEventDoneData*>&)", "Signalling cleanup" )
	<< "Signalling cleanup of event 0x" << AliHLTLog::kHex
	<< eventID << " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;

    fCleanupSignal.Signal();
    }

void AliHLTProcessingSubscriber::SignalCleanupInEvent( AliEventID_t eventID, AliHLTEventDoneData* existingEDD )
    {
    AliHLTEventDoneData* edd;
    if ( existingEDD )
	edd = existingEDD;
    else
	edd = GetEventDoneData( eventID );
    if ( edd && !existingEDD ) // If an existing EDD is provided, this will already contain any water mark commands if necessary...
	{
	uint8 waterMarkEDDBuffer[sizeof(AliHLTEventDoneData)+sizeof(edd->fDataWords[0])*6];
	bool storedWaterMark = CreateWaterMarkEDD( reinterpret_cast<AliHLTEventDoneData*>(waterMarkEDDBuffer), eventID, true );
#if 1
	if ( storedWaterMark )
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::SignalCleanupInEvent", "Sending water mark message" )
		<< "Created water mark message for event  event 0x" << AliHLTLog::kHex
		<< eventID << " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
	    }
#endif
	if ( storedWaterMark )
	    {
	    vector<AliHLTEventDoneData*> eventDoneData;
	    eventDoneData.push_back( edd );
	    eventDoneData.push_back( reinterpret_cast<AliHLTEventDoneData*>(waterMarkEDDBuffer) );
	    unsigned long tLength = AliHLTGetMergedEventDoneDataSize( eventDoneData );
	    AliHLTEventDoneData* eddTmp = (AliHLTEventDoneData*)fEDDCache.Get( tLength );
	    if ( eddTmp )
		{
		AliHLTMergeEventDoneData( eventID, eddTmp, eventDoneData );
		edd = eddTmp;
		}
	    }
	}
    if ( !edd )
	{
	edd = (AliHLTEventDoneData*)fEDDCache.Get( sizeof(AliHLTEventDoneData)+sizeof(edd->fDataWords[0])*6 );
	if ( !edd )
	    {
	    LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::SignalCleanupInEvent", "Out of memory" )
		<< "Out of memory trying to allocate default event done data for event 0x" << AliHLTLog::kHex
		<< eventID << " (" << AliHLTLog::kDec << eventID << ") of " << (sizeof(AliHLTEventDoneData)+sizeof(edd->fDataWords[0])*6)
		<< " bytes from EventDoneData cache." << ENDLOG;
	    return;
	    }
	memcpy( edd, &fDefaultEDD, sizeof(AliHLTEventDoneData) );
	edd->fEventID = eventID;

	bool storedWaterMark = CreateWaterMarkEDD( edd );
#if 1
	if ( storedWaterMark )
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::SignalCleanupInEvent", "Sending water mark message" )
		<< "Created water mark message for event  event 0x" << AliHLTLog::kHex
		<< eventID << " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
	    }
#endif

	}

    fCleanupSignal.AddNotificationData( edd );

    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::SignalCleanupInEvent(AliEventID_t)", "Signalling cleanup" )
	<< "Signalling cleanup of event 0x" << AliHLTLog::kHex
	<< eventID << " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
    
    fCleanupSignal.Signal();
    }

bool AliHLTProcessingSubscriber::CleanupInEvent( AliHLTEventDoneData* eventDoneData )
    {
    AliEventID_t eventID = eventDoneData->fEventID;
    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::CleanupInEvent", "Cleanup" )
	<< "Cleaning up event 0x" << AliHLTLog::kHex
	<< eventID << " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
    int ret;
    unsigned long ndx;
    bool announced = false;
    ret = pthread_mutex_lock( &fEventDataMutex );
    if ( ret )
	{
	}
    if ( MLUCVectorSearcher<EventData*,AliEventID_t>::FindElement( fEventData, &EventDataSearchFunc, eventID, ndx ) )
	{
	announced = fEventData.Get(ndx)->fAnnounced;
	fEventData.Get(ndx)->fDone = true;
	if ( fEventData.Get( ndx )->fActive )
	    {
	    fEventData.Get(ndx)->fPostponedEDD = eventDoneData;
	    ret = pthread_mutex_unlock( &fEventDataMutex );
	    if ( ret )
		{
		
		}
	    return false;
	    }
	}
    ret = pthread_mutex_unlock( &fEventDataMutex );
    if ( ret )
	{
	}

    if ( fRePublisher )
	{
	if ( fOutDescriptorHandler && announced )
	    {
	    ret = pthread_mutex_lock( &fOutSEDDMutex );
	    if ( ret )
		{
		}
	    fOutDescriptorHandler->ReleaseEventDescriptor( eventID );
	    ret = pthread_mutex_unlock( &fOutSEDDMutex );
	    if ( ret )
		{
		}
	    }
	else
	    {
	    }
	}

#ifdef DO_BENCHMARK
#undef DO_BENCHMARK
#endif
    //#define DO_BENCHMARK
#ifdef DO_BENCHMARK
    struct timeval t1, t2, t3, t4, t5;
    AliUInt32_t dt;
#endif
#ifdef DO_BENCHMARK
    if ( fBenchmark )
	gettimeofday( &t1, NULL );
#endif
    if ( fInDescriptorHandler )
	{
	ret = pthread_mutex_lock( &fInSEDDMutex );
	if ( ret )
	    {
	    }
	fInDescriptorHandler->ReleaseEventDescriptor( eventID );
	ret = pthread_mutex_unlock( &fInSEDDMutex );
	if ( ret )
	    {
	    }
	}
    else
	{
	}
#ifdef DO_BENCHMARK
    if ( fBenchmark )
	{
	gettimeofday( &t2, NULL );
	dt = (t2.tv_sec-t1.tv_sec)*1000000+(t2.tv_usec-t1.tv_usec);
	LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::CleanupInEvent", "Benchmark 1" )
	    << "Time from 1 to 2: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
	gettimeofday( &t2, NULL );
	}
#endif

#if 0
    if ( !fRePublisher && fEventAccounting )
#else
    if ( fEventAccounting )
#endif
	fEventAccounting->EventFinished( fName.c_str(), eventID, fForceEventDoneDataForward ? 2 : 1, 1 );

    if ( fPublisher )
	{
	fPublisher->EventDone( *this, *eventDoneData, false, false ); // Use fForceEventDoneDataForward for 2nd parameter?
	}
    else
	{
	}




    InBlockData* inIter;
    OutBlockData* outIter;
    EventData* ed = NULL;
    ret = pthread_mutex_lock( &fEventDataMutex );
    if ( ret )
	{
	}
    if ( MLUCVectorSearcher<EventData*,AliEventID_t>::FindElement( fEventData, &EventDataSearchFunc, eventID, ndx ) )
	{
	ed = fEventData.Get( ndx );
	fEventData.Remove( ndx );
	}
    if ( fStatus && fRePublisher )
	{
	fStatus->fHLTStatus.fPendingOutputEventCount = fRePublisher->GetPendingEventCount();
	fStatus->Touch();
	}
    if ( fStatus && fEventData.GetCnt()<=0 )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
	}

    ret = pthread_mutex_unlock( &fEventDataMutex );
    if ( ret )
	{
	}
    if ( ed )
	{
	while ( ed->fInBlocks.GetCnt() )
	    {
	    inIter = ed->fInBlocks.GetFirstPtr();
	    if ( fShmManager )
		fShmManager->ReleaseShm( inIter->fShmKey );
	    ed->fInBlocks.RemoveFirst();
	    }
	if ( fRePublisher )
	    {
	    while ( ed->fOutBlocks.GetCnt() )
		{
		outIter = ed->fOutBlocks.GetFirstPtr();
		if ( fShmManager && outIter->fShmKey.fShmType != kInvalidShmType )
		    fShmManager->ReleaseShm( outIter->fShmKey );
		if ( fBufferManager && outIter->fBufferNdx!=~(AliUInt32_t)0 && outIter->fOffset!=~(AliUInt32_t)0 )
		    fBufferManager->ReleaseBlock( outIter->fBufferNdx, outIter->fOffset );
		ed->fOutBlocks.RemoveFirst();
		}
	    if ( fBufferManager )
		{
		if ( fStatus )
		    {
		    fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
		    fStatus->Touch();
		    }
		if ( fBufferWarnings )
		    {
		    double freeRatio = (((double)fBufferManager->GetFreeBufferSize())/((double)fBufferManager->GetTotalBufferSize()));
		    if ( freeRatio>0.3 )
			{
			fBuffer20PercentWarning = false;
			fBuffer10PercentWarning = false;
			}
		    }
		}
	    }
	if ( ed->fETS )
	    {
	    fETSCache.Release( (uint8*)ed->fETS );  //delete [] (AliUInt8_t*)ed->fETS;
	    ed->fETS = NULL;
	    }
	//fEventDataCache.Release( (uint8*)ed );
	fEventDataCache.Release( ed );
	fEventSignal.Signal();
	ed = NULL;
	}

#ifdef DO_BENCHMARK
    if ( fBenchmark )
	{
	gettimeofday( &t3, NULL );
	dt = (t3.tv_sec-t2.tv_sec)*1000000+(t3.tv_usec-t2.tv_usec);
	LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::CleanupInEvent", "Benchmark 2" )
	    << "Time from 2 to 3: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
	gettimeofday( &t3, NULL );
	}
#endif

//     InBlockData* iter;
//     bool found = false;
//     AliUInt32_t count=0;
//     ret = pthread_mutex_lock( &fInBlockMutex );
//     if ( ret )
// 	{
// 	}
//     if ( fShmManager )
// 	{
// 	while ( fInBlocks.FindElement( &InBlockSearchFunc, (uint64)eventID, ndx ) )
// 	    {
// 	    iter = fInBlocks.GetPtr( ndx );
// 	    found = true;
// 	    count++;
// 	    fShmManager->ReleaseShm( iter->fShmKey );
// 	    fInBlocks.Remove( ndx );
// 	    }
// 	}
//     else
// 	{
// 	}
    
#ifdef DO_BENCHMARK
    if ( fBenchmark )
	{
	gettimeofday( &t4, NULL );
	dt = (t4.tv_sec-t3.tv_sec)*1000000+(t4.tv_usec-t3.tv_usec);
	LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::CleanupInEvent", "Benchmark 3" )
	    << "Time from 3 to 4: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
	gettimeofday( &t4, NULL );
	}
#endif


//     ret = pthread_mutex_unlock( &fInBlockMutex );
//     if ( ret )
// 	{
// 	}

#ifdef DO_BENCHMARK
    if ( fBenchmark )
	{
	gettimeofday( &t5, NULL );
	dt = (t5.tv_sec-t4.tv_sec)*1000000+(t5.tv_usec-t4.tv_usec);
	LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::CleanupInEvent", "Benchmark 4" )
	    << "Time from 4 to 5: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
	dt = (t5.tv_sec-t1.tv_sec)*1000000+(t5.tv_usec-t1.tv_usec);
	LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::CleanupInEvent", "Benchmark 5" )
	    << "Time from 1 to 5: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
	gettimeofday( &t5, NULL );
	}
#endif
    return true;
    }

// void AliHLTProcessingSubscriber::CleanupOutEvent( AliEventID_t eventID )
//     {
//     int ret;
//     bool found = true;
//     unsigned long ndx;
//     ret = pthread_mutex_lock( &fOutBlockMutex );
//     if ( ret )
// 	{
// 	}
//     while ( found )
// 	{
// 	found = false;
// 	if ( fOutBlocks.FindElement( &OutBlockSearchFunc, (uint64)eventID, ndx ) )
// 	    {
// 	    found = true;
// 	    }
// 	}
    
//     ret = pthread_mutex_unlock( &fOutBlockMutex );
//     if ( ret )
// 	{
// 	}
//     }

AliHLTEventDoneData* AliHLTProcessingSubscriber::GetEventDoneData( AliEventID_t eventID, AliUInt32_t dataWordCount )
    {
    AliHLTEventDoneData* tmp = NULL;
    static AliHLTEventDoneData tmp2;
#if 0
    tmp = (AliHLTEventDoneData*)fEDDCache.Get( sizeof(AliHLTEventDoneData)+dataWordCount*sizeof( AliHLTEventDoneData::fDataWords[0] ) );
#else // Work around around presumed gcc bug.
    tmp = (AliHLTEventDoneData*)fEDDCache.Get( sizeof(tmp2)+dataWordCount*sizeof( tmp2.fDataWords[0] ) );
#endif
    if ( !tmp )
	return NULL;
    AliHLTMakeEventDoneData( tmp, eventID );
    //tmp->fHeader.fLength = sizeof(AliHLTEventDoneData)+dataWordCount*sizeof( ((AliHLTEventDoneData*)NULL)->fDataWords[0] );
#if 0
    tmp->fHeader.fLength = sizeof(AliHLTEventDoneData)+dataWordCount*sizeof( AliHLTEventDoneData::fDataWords[0] );
#else // Work around around presumed gcc bug.
    tmp->fHeader.fLength = sizeof(tmp2)+dataWordCount*sizeof( tmp2.fDataWords[0] );
#endif
    tmp->fDataWordCount = dataWordCount;
    return tmp;
    }

void AliHLTProcessingSubscriber::FreeEventDoneData( AliHLTEventDoneData* edd )
    {
    fEDDCache.Release( (uint8*)edd );
    }

AliHLTEventDoneData* AliHLTProcessingSubscriber::GetEventDoneData( AliEventID_t eventID )
    {
    unsigned long ndx;
    AliHLTEventDoneData* edd = NULL;
    pthread_mutex_lock( &fDoneDataMutex );
    if ( MLUCVectorSearcher<AliHLTEventDoneData*,AliEventID_t>::FindElement( fDoneData, DoneDataSearchFunc, eventID, ndx ) )
	{
	edd = fDoneData.Get( ndx );
	fDoneData.Remove( ndx );
	}
    pthread_mutex_unlock( &fDoneDataMutex );
    return edd;
    }

void AliHLTProcessingSubscriber::PutEventDoneData( AliHLTEventDoneData* edd )
    {
    pthread_mutex_lock( &fDoneDataMutex );
    fDoneData.Add( edd );
    pthread_mutex_unlock( &fDoneDataMutex );
    }

int AliHLTProcessingSubscriber::AnnounceEvent( AliHLTProcessingSubscriber::EventData* ed, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks )
    {
    if ( fAsync )
	pthread_mutex_lock( &fAsyncAnnounceMutex );
    int ret = 0;
    unsigned long n0 = ed->fOutputBlocks.size(), n1 = outputBlocks.size(), n=n0+n1;
    ret = pthread_mutex_lock( &fOutSEDDMutex );
    if ( ret )
	{
	}
    AliHLTSubEventDataDescriptor *newSEDD = fOutDescriptorHandler->GetFreeEventDescriptor( ed->fEventID, n );
    ret = pthread_mutex_unlock( &fOutSEDDMutex );
    if ( ret )
	{
	}
    
    
    if ( newSEDD )
	{ // Got SEDD
	newSEDD->fDataType.fID = COMPOSITE_DATAID;
	newSEDD->fEventID = ed->fEventID;
	if ( ed && ed->fSEDD )
	    {
	    newSEDD->fOldestEventBirth_s = ed->fSEDD->fOldestEventBirth_s;
	    newSEDD->fEventBirth_s = ed->fSEDD->fEventBirth_s;
	    newSEDD->fEventBirth_us = ed->fSEDD->fEventBirth_us;
	    newSEDD->fStatusFlags = ed->fSEDD->fStatusFlags;
	    }
	else
	    {
	    newSEDD->fOldestEventBirth_s = 0;
	    newSEDD->fEventBirth_s = 0;
	    newSEDD->fEventBirth_us = 0;
	    newSEDD->fStatusFlags = 0;
	    }
	if ( ed && ed->fSEDD && ed->fSEDD->fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_FULLPASSTHROUGH )
	    newSEDD->fStatusFlags |= ALIHLTSUBEVENTDATADESCRIPTOR_FULLPASSTHROUGH;
	for ( unsigned long i = 0; i < n0; i++ )
	    {
	    char tmp[9];
	    strncpy( tmp, (char*)ed->fOutputBlocks[i].fDataType.fDescr, 8 );
	    tmp[8] = 0;
	    LOG( AliHLTLog::kDebug, "ALiL3ProcessingSubscriber::AnnounceEvent", "Event processed" )
		<< AliHLTLog::kDec << "Datablock " << i << ": Offset: " << ed->fOutputBlocks[i].fOffset
		<< " Size: " << ed->fOutputBlocks[i].fSize << " Datatype: " << tmp << " (" 
		<< ed->fOutputBlocks[i].fDataType.fID << "/0x" << AliHLTLog::kHex << ed->fOutputBlocks[i].fDataType.fID
		<< ")." << ENDLOG;
	    newSEDD->fDataBlocks[i].fShmID.fShmType = ed->fOutputBlocks[i].fShmKey.fShmType;
	    newSEDD->fDataBlocks[i].fShmID.fKey.fID = ed->fOutputBlocks[i].fShmKey.fKey.fID;
	    newSEDD->fDataBlocks[i].fBlockOffset = ed->fOutputBlocks[i].fOffset;
	    newSEDD->fDataBlocks[i].fProducerNode = ed->fOutputBlocks[i].fProducerNode;
	    newSEDD->fDataBlocks[i].fBlockSize = ed->fOutputBlocks[i].fSize;
	    newSEDD->fDataBlocks[i].fDataType.fID = ed->fOutputBlocks[i].fDataType.fID;
	    newSEDD->fDataBlocks[i].fDataOrigin.fID = ed->fOutputBlocks[i].fDataOrigin.fID;
	    newSEDD->fDataBlocks[i].fDataSpecification = ed->fOutputBlocks[i].fDataSpecification;
	    newSEDD->fDataBlocks[i].fStatusFlags = ed->fOutputBlocks[i].fStatusFlags;
	    for ( int j = 0; j < kAliHLTSEDBDAttributeCount; j++ )
		{
		if ( ed->fOutputBlocks[i].fAttributes[j]!=kAliHLTUnspecifiedAttribute )
		    newSEDD->fDataBlocks[i].fAttributes[j] = ed->fOutputBlocks[i].fAttributes[j];
		else
		    newSEDD->fDataBlocks[i].fAttributes[j] = AliHLTSubEventDescriptor::GetBlockAttribute( j );
		}
	    //newSEDD->fDataBlocks[i].fByteOrder = outBlocks[i].fByteOrder;
	    }
	for ( unsigned long i = 0; i < n1; i++ )
	    {
	    char tmp[9];
	    strncpy( tmp, (char*)outputBlocks[i].fDataType.fDescr, 8 );
	    tmp[8] = 0;
	    LOG( AliHLTLog::kDebug, "ALiL3ProcessingSubscriber::AnnounceEvent", "Event processed" )
		<< AliHLTLog::kDec << "Datablock " << i << ": Offset: " << outputBlocks[i].fOffset
		<< " Size: " << outputBlocks[i].fSize << " Datatype: " << tmp << " (" 
		<< outputBlocks[i].fDataType.fID << "/0x" << AliHLTLog::kHex << outputBlocks[i].fDataType.fID
		<< ")." << ENDLOG;
	    newSEDD->fDataBlocks[n0+i].fShmID.fShmType = outputBlocks[i].fShmKey.fShmType;
	    newSEDD->fDataBlocks[n0+i].fShmID.fKey.fID = outputBlocks[i].fShmKey.fKey.fID;
	    newSEDD->fDataBlocks[n0+i].fBlockOffset = outputBlocks[i].fOffset;
	    newSEDD->fDataBlocks[n0+i].fProducerNode = outputBlocks[i].fProducerNode;
	    newSEDD->fDataBlocks[n0+i].fBlockSize = outputBlocks[i].fSize;
	    newSEDD->fDataBlocks[n0+i].fDataType.fID = outputBlocks[i].fDataType.fID;
	    newSEDD->fDataBlocks[n0+i].fDataOrigin.fID = outputBlocks[i].fDataOrigin.fID;
	    newSEDD->fDataBlocks[n0+i].fDataSpecification = outputBlocks[i].fDataSpecification;
	    newSEDD->fDataBlocks[n0+i].fStatusFlags = outputBlocks[i].fStatusFlags;
	    for ( int j = 0; j < kAliHLTSEDBDAttributeCount; j++ )
		{
		if ( outputBlocks[i].fAttributes[j]!=kAliHLTUnspecifiedAttribute )
		    newSEDD->fDataBlocks[n0+i].fAttributes[j] = outputBlocks[i].fAttributes[j];
		else
		    newSEDD->fDataBlocks[n0+i].fAttributes[j] = AliHLTSubEventDescriptor::GetBlockAttribute( j );
		}
	    //newSEDD->fDataBlocks[n0+i].fByteOrder = outputBlocks[i].fByteOrder;
	    }
	AliHLTEventTriggerStruct* ets;
	if ( ed->fETS )
	    ets = ed->fETS;
	else
	    ets = &fDefaultETS;
#ifdef ALLOW_ARTIFICIAL_EVENT_LOSS
	if ( !fEventLossModulo || (++fEventCounter % fEventLossModulo) )
	    {
#endif
	    
	    ret = fRePublisher->AnnounceEvent( ed->fEventID, *newSEDD, *ets );
	    
#ifdef ALLOW_ARTIFICIAL_EVENT_LOSS
	    }
	else
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTProcessingSubscriber::AnnounceEvent", "Losing event" )
					    << "Inducing artificial event loss for event 0x" << AliHLTLog::kHex << ed->fEventID
					    << " (" << AliHLTLog::kDec << ed->fEventID << ")." << ENDLOG;
	    ret = 0;
	    }
#endif
	
	if ( ret )
	    {
	    int tmpRet;
	    tmpRet = pthread_mutex_lock( &fOutSEDDMutex );
	    if ( tmpRet )
		{
		}
	    fOutDescriptorHandler->ReleaseEventDescriptor( ed->fEventID );
	    tmpRet = pthread_mutex_unlock( &fOutSEDDMutex );
	    if ( tmpRet )
		{
		}
	    LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::AnnounceEvent", "Error announcing event" )
		<< "Error announcing event 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec
		<< ed->fEventID << ")." << ENDLOG;
	    }
	else 
	    {
	    ed->fAnnounced = true;
	    if ( fStatus )
		{
		if ( ed->fEventID.fType != kAliEventTypeTick && ed->fEventID.fType!=kAliEventTypeReconfigure && ed->fEventID.fType!=kAliEventTypeDCSUpdate )
		    {
		    fStatus->fHLTStatus.fAnnouncedEventCount++;
		    fStatus->Touch();
		    }
		ret = pthread_mutex_lock( &fEventDataMutex );
		if ( ret )
		    {
		    }
		fStatus->fHLTStatus.fPendingOutputEventCount = fRePublisher->GetPendingEventCount();
		fStatus->Touch();
		ret = pthread_mutex_unlock( &fEventDataMutex );
		if ( ret )
		    {
		    }
		}
	    }
	
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::AnnounceEvent", "Error getting new subevent descriptor" )
	    << "Error getting new subevent descriptor for event 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec
	    << ed->fEventID << ")." << ENDLOG;
	ret = ENOMEM;
	}
    if ( fAsync )
	pthread_mutex_unlock( &fAsyncAnnounceMutex );
    return ret;
    }

int AliHLTProcessingSubscriber::AnnounceEvent( AliEventID_t eventID, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks )
    {
    int ret, annRet=EINVAL;
    ret = pthread_mutex_lock( &fEventDataMutex );
    if ( ret )
	{
	}
    unsigned long ndx;
    if ( MLUCVectorSearcher<EventData*,AliEventID_t>::FindElement( fEventData, &EventDataSearchFunc, eventID, ndx ) )
	{
	EventData* ed = fEventData.Get(ndx);
	ed->fActive = true;
	ret = pthread_mutex_unlock( &fEventDataMutex );
	if ( ret )
	    {
	    }
	bool sendDone = fSendEventDone;
	pthread_mutex_lock( &fPublisherCurrentEventDoneMutex );
	fPublisherCurrentEvent = ed->fEventID;
	fPublisherCurrentEventDone = false;
	pthread_mutex_unlock( &fPublisherCurrentEventDoneMutex );
	annRet = AnnounceEvent( ed, outputBlocks );
	pthread_mutex_lock( &fPublisherCurrentEventDoneMutex );
	if ( fPublisherCurrentEventDone )
	    sendDone = false;
	pthread_mutex_unlock( &fPublisherCurrentEventDoneMutex );
	ret = pthread_mutex_lock( &fEventDataMutex );
	ed->fActive = false;
	if ( ed->fDone )
	    sendDone = true;
	ret = pthread_mutex_unlock( &fEventDataMutex );
	if ( sendDone )
	    SignalCleanupInEvent( ed->fEventID, ed->fPostponedEDD );
	}
    else
	ret = pthread_mutex_unlock( &fEventDataMutex );
    return annRet;
    }



AliHLTProcessingSubscriber::AliHLTProcessingThread::AliHLTProcessingThread( AliHLTProcessingSubscriber& sub ):
    fSub( sub )
    {
    }

AliHLTProcessingSubscriber::AliHLTProcessingThread::~AliHLTProcessingThread()
    {
    }

void AliHLTProcessingSubscriber::AliHLTProcessingThread::Run()
    {
    if ( fSub.fStatus )
	{
	MLUCMutex::TLocker stateLock( fSub.fStatus->fStateMutex );
	fSub.fStatus->fProcessStatus.fState |= kAliHLTPCRunningStateFlag;
	fSub.fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	}
    fSub.DoProcessing();
    if ( fSub.fStatus )
	{
	MLUCMutex::TLocker stateLock( fSub.fStatus->fStateMutex );
	fSub.fStatus->fProcessStatus.fState &= ~kAliHLTPCRunningStateFlag;
	if ( !(fSub.fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) )
	    fSub.fStatus->fProcessStatus.fState |= kAliHLTPCConsumerErrorStateFlag;
	else
	    fSub.fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	}
    }

void AliHLTProcessingSubscriber::AliHLTEventSemaphore::SignalEvent( AliHLTProcessingSubscriber::PEventData ed )
    {
    AddNotificationData( ed );
    Signal();
    }

AliHLTProcessingSubscriber::PEventData AliHLTProcessingSubscriber::AliHLTEventSemaphore::RemoveEvent( AliEventID_t eventID )
    {
    unsigned long ndx;
    AliHLTProcessingSubscriber::PEventData edTmp = NULL;
    pthread_mutex_lock( &fAccessSem );
    if ( MLUCVectorSearcher<PEventData,AliEventID_t>::FindElement( fNotificationData, &FindEventByID, eventID, ndx ) )
	{
	edTmp = fNotificationData.Get( ndx );
	fNotificationData.Remove( ndx );
	}
    pthread_mutex_unlock( &fAccessSem );
    return edTmp;
    }

void AliHLTProcessingSubscriber::AbortProcessing()
    {
    }

void AliHLTProcessingSubscriber::DoProcessing()
    {
    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber", "Processing loop" )
	<< "Processing loop started." << ENDLOG;

    if ( !fBufferManager )
	{
	// ...
	fProcessingQuitted = true;
	return;
	}
    if ( !fShmManager )
	{
	// ...
	fProcessingQuitted = true;
	return;
	}
    if ( !fOutDescriptorHandler )
	{
	// ...
	fProcessingQuitted = true;
	return;
	}
    int ret;
    fQuitProcessing = false;
    fProcessingQuitted = false;
    //AliHLTSubEventDataDescriptor* sedd, *newSEDD;
    AliHLTEventDoneData* edd = NULL;
    EventData* ed;
    vector<AliHLTSubEventDescriptor::BlockData> dataBlocks;
    AliHLTEventTriggerStruct* ets;
    bool ok = true;
    AliUInt32_t buffer;
    unsigned long offset, size = (AliUInt32_t)-1;
    AliUInt32_t count = 0;
    AliHLTShmID shmID;
    AliUInt8_t* outData=NULL;
    AliUInt32_t outSize=0;
    AliEventID_t eventID;
    bool sendDone;
    bool retry = false;
    bool isRetryEvent;
    bool forceForward = false;
    bool success = true;
#ifdef DO_BENCHMARK
#undef DO_BENCHMARK
#endif
    //#define DO_BENCHMARK
#ifdef DO_BENCHMARK
    struct timeval t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16, t17, t18, tfinal;
    AliUInt32_t dt;
#endif

    fEventSignal.Lock();
    while ( !fQuitProcessing )
	{
	if ( (fRetryData.GetCnt()<=0 && !fEventSignal.HaveNotificationData()) || fPaused || !success )
	    fEventSignal.Wait();
	if ( fPaused )
	    {
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCPausedStateFlag;
		}
// 	    if ( fStatus && fStatus->fProcessStatus.fState==kAliHLTPCWaitingState )
// 		fStatus->fProcessStatus.fState = kAliHLTPCPausedState;
	    continue;
	    }
	else
	    {
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState &= ~kAliHLTPCPausedStateFlag;
		}
// 	    if ( fStatus && fStatus->fProcessStatus.fState==kAliHLTPCPausedState )
// 		fStatus->fProcessStatus.fState = kAliHLTPCWaitingState;
	    }
	success = true;
	while ( !fQuitProcessing && (fRetryData.GetCnt() || fEventSignal.HaveNotificationData()) && success )
	    {
#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		gettimeofday( &t1, NULL );
#endif
	    AliUInt64_t inCnt = fRetryData.GetCnt()+fEventSignal.GetNotificationDataCnt()-1;
	    if ( fRetryData.GetCnt() )
		{
		ed = fRetryData.GetFirst();
		//fRetryData.RemoveFirst();
		isRetryEvent = true;
		if ( ed )
		    {
		    LOG( AliHLTLog::kInformational, "AliHLTProcessingSubscriber::DoProcessing", "Event Retry" )
			<< "Retrying event 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID
			<< ")." << ENDLOG;
		    }
		}
	    else
		{
		ed = reinterpret_cast<EventData*>( fEventSignal.PopNotificationData() );
		isRetryEvent = false;
		}
	    if ( !ed )
		{
		if ( isRetryEvent )
		    fRetryData.RemoveFirst();
		continue;
		}
	    forceForward = false;
	    if ( ed->fForceEmptyForward )
		forceForward = true;
	    if ( forceForward )
		{
		LOG( AliHLTLog::kWarning, "AliHLTProcessingSubscriber::DoProcessing", "Forcing Empty Event Forward" )
		    << "Forcing empty forward for event 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID
		    << ")." << ENDLOG;
		}
	    fProcessingOk = true;
	    fEventSignal.Unlock();
	    pthread_mutex_lock( &fEventDataMutex );
	    if ( ed->fDone )
		{
		pthread_mutex_unlock( &fEventDataMutex );
		fEventSignal.Lock();
		continue;
		}
	    if ( !fAsync )
		ed->fActive = true;
	    pthread_mutex_unlock( &fEventDataMutex );
	    if ( fStatus )
		{
		//fStatus->fProcessStatus.fState |= kAliHLTPCBusyStateFlag;
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fHLTStatus.fPendingInputEventCount = inCnt;
		fStatus->Touch();
		}
// 	    if ( fStatus && fStatus->fProcessStatus.fState==kAliHLTPCWaitingState )
// 		fStatus->fProcessStatus.fState = kAliHLTPCBusyState;
	    eventID = ed->fEventID;

	    //#undef DO_BENCHMARK	    //
#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		{
		gettimeofday( &t2, NULL );
		dt = (t2.tv_sec-t1.tv_sec)*1000000+(t2.tv_usec-t1.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::DoProcessing", "Benchmark 1" )
		    << "Time from 1 to 2: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		gettimeofday( &t2, NULL );
		}
#endif

	    if ( ed->fETS )
		ets = ed->fETS;
	    else
		ets = &fDefaultETS;
	    sendDone = fSendEventDone;
	    retry = false;
	    ProcessSEDD( ed->fSEDD, dataBlocks );
	    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::DoProcessing", "Processing event" )
		<< "Processing event 0x" << AliHLTLog::kHex << eventID << " ("
		<< AliHLTLog::kDec << eventID << ") with " << dataBlocks.size() 
		<< " datablocks." << ENDLOG;
#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		{
		gettimeofday( &t3, NULL );
		dt = (t3.tv_sec-t2.tv_sec)*1000000+(t3.tv_usec-t2.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::DoProcessing", "Benchmark 2" )
		    << "Time from 2 to 3: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		gettimeofday( &t3, NULL );
		}
#endif
		
	    vector<AliHLTSubEventDescriptor::BlockData>::iterator iter, end;
	    InBlockData ibd;
	    ibd.fEventID = ed->fEventID;
#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		{
		gettimeofday( &t4, NULL );
		dt = (t4.tv_sec-t3.tv_sec)*1000000+(t4.tv_usec-t3.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::DoProcessing", "Benchmark 3" )
		    << "Time from 3 to 4: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		gettimeofday( &t4, NULL );
		}
#endif
// 	    ret = pthread_mutex_lock( &fInBlockMutex );
// 	    if ( ret )
// 		{
// 		}
	    unsigned long dataBlocksTotalSize=0;
	    unsigned long dataBlockCnt = 0;
	    if ( DOLOG(AliHLTLog::kDebug) )
		{
		char datatype[9];
		for ( unsigned ii=0; ii<8; ii++ )
		    datatype[ii] = ed->fSEDD->fDataType.fDescr[7-ii];
		datatype[8] = 0;
		LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::DoProcessing", "Input data" )
		    << "Event 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec
		    << ed->fEventID << ") status: 0x" << AliHLTLog::kHex << ed->fSEDD->fStatusFlags << " / "
		    << AliHLTLog::kDec << ed->fSEDD->fStatusFlags << " - " << ed->fSEDD->fDataBlockCount 
		    << " blocks - birth: " << AliHLTLog::kDec << ed->fSEDD->fEventBirth_s << " s "
		    << ed->fSEDD->fEventBirth_us << " us (oldest event birth: " << ed->fSEDD->fOldestEventBirth_s
		    << " s) - datatype: '" << datatype << "'." << ENDLOG;
		}
	    iter = dataBlocks.begin();
	    end = dataBlocks.end();
	    while ( iter != end )
		{
		if ( DOLOG(AliHLTLog::kDebug) )
		    {
		    char datatype[9], dataorigin[5];
		    for ( unsigned ii=0; ii<8; ii++ )
			datatype[ii] = iter->fDataType.fDescr[7-ii];
		    datatype[8] = 0;
		    for ( unsigned ii=0; ii<4; ii++ )
			dataorigin[ii] = iter->fDataOrigin.fDescr[3-ii];
		    dataorigin[4] = 0;
		    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::DoProcessing", "Input data block" )
			<< "Event 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec
			<< ed->fEventID << ") input data block " << (iter-dataBlocks.begin()) << ": Shm ID: 0x" << AliHLTLog::kHex
			<< iter->fShmKey.fKey.fID << " - Offset: 0x" << iter->fOffset << " (" << AliHLTLog::kDec
			<< iter->fOffset << ") - Size: 0x" << AliHLTLog::kHex << iter->fSize << " (" << AliHLTLog::kDec
			<< iter->fSize << ") - '" << datatype << "' : '" << dataorigin << "' : 0x" << AliHLTLog::kHex
			<< iter->fDataSpecification << " / " << AliHLTLog::kDec << iter->fDataSpecification << "." << ENDLOG;
		    }
		ibd.fShmKey.fShmType = iter->fShmKey.fShmType;
		ibd.fShmKey.fKey.fID = iter->fShmKey.fKey.fID;
		dataBlocksTotalSize += iter->fSize;
		++dataBlockCnt;
		ed->fInBlocks.Add( ibd );
		iter++;
		}
// 	    ret = pthread_mutex_unlock( &fInBlockMutex );
// 	    if ( ret )
// 		{
// 		}
#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		{
		gettimeofday( &t5, NULL );
		dt = (t5.tv_sec-t4.tv_sec)*1000000+(t5.tv_usec-t4.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::DoProcessing", "Benchmark 4" )
		    << "Time from 4 to 5: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		gettimeofday( &t5, NULL );
		}
#endif
	    

	    if ( fBufferManager->CanGetBlock() )
		{
		unsigned long minBlockSize;
		if ( fPerEventFixedSize!=0 || fPerBlockFixedSize!=0 || fSizeFactor!=0.0 )
		    minBlockSize = fPerEventFixedSize + dataBlockCnt*fPerBlockFixedSize + (unsigned long)((double)dataBlocksTotalSize*fSizeFactor);
		else
		    minBlockSize = fMinBlockSize;
		LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::DoProcessing", "Block sizes" )
		    << "Min. block size: " << AliHLTLog::kDec << minBlockSize << " - fMinBlockSize: " << fMinBlockSize
		    << " - fPerEventFixedSize: " << fPerEventFixedSize << " - fPerBlockFixedSize: " << fPerBlockFixedSize << " - fSizeFactor: " << fSizeFactor
		    << " - block count: " << dataBlockCnt << " - total input data size: " << dataBlocksTotalSize
		    << ENDLOG;
		ok = true;
		count = 0;
#ifdef DO_BENCHMARK
		if ( fBenchmark )
		    {
		    gettimeofday( &t6, NULL );
		    dt = (t6.tv_sec-t5.tv_sec)*1000000+(t6.tv_usec-t5.tv_usec);
		    LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::DoProcessing", "Benchmark 5" )
			<< "Time from 5 to 6: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		    gettimeofday( &t6, NULL );
		    }
#endif
		// check for reasonable minBlockSize;
		// if too large compared to total (free) buffer
		// estimation might be off. Try with smaller block size.
		// Processing might fail, though.
		unsigned long maxSupportedBlockSize = fBufferManager->GetFreeBufferSize() / 10UL;
		if(minBlockSize > maxSupportedBlockSize)
		  minBlockSize=maxSupportedBlockSize;

		struct timeval start, now;
		unsigned long long deltaT = 0;
		const unsigned long long timeLimit = 1000000;
		do
		    {
		    if ( !fBufferManager->IsBlockFeasible( minBlockSize ) || forceForward )
			{
			ok = false;
			size = 0;
			offset = ~(unsigned long)0;
			buffer = ~(AliUInt32_t)0;
			break;
			}
		    size = 0;
		    ok = fBufferManager->GetBlock( buffer, offset, size );
		    if ( ok && size < minBlockSize )
			fBufferManager->ReleaseBlock( buffer, offset, true );
		    if ( !ok )
			size = 0;
		    if ( !ok || size < minBlockSize )
			{
			if ( deltaT==0 )
			    gettimeofday( &start, NULL );
			usleep( 0 );
			gettimeofday( &now, NULL );
			deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
			}
		    else
			break;
		    }
		while ( size < minBlockSize && deltaT<timeLimit && !fQuitProcessing );
		//while ( ok && size < fMinBlockSize && count++ < 1000 );
		if ( fStatus )
		    {
		    fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
		    fStatus->Touch();
		    }


#ifdef DO_BENCHMARK
		if ( fBenchmark )
		    {
		    gettimeofday( &t7, NULL );
		    dt = (t7.tv_sec-t6.tv_sec)*1000000+(t7.tv_usec-t6.tv_usec);
		    LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::DoProcessing", "Benchmark 6" )
			<< "Time from 6 to 7: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		    gettimeofday( &t7, NULL );
		    }
#endif
		if ( size < minBlockSize && !forceForward )
		    {
		    LOG( AliHLTLog::kInformational, "AliHLTProcessingSubscriber::DoProcessing", "GetBuffer " )
			<< "Unable to get buffer of size " << AliHLTLog::kDec << minBlockSize 
			<< " from publisher for event 0x" << AliHLTLog::kHex << eventID << " ("
			<< AliHLTLog::kDec << eventID << "). Maximal received size: " << size << ENDLOG;
		    ok = false;
		    }
		if ( ok || forceForward )
		    { // Got output block
		    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::DoProcessing", "Buffer data" )
			<< "Buffer received from buffer manager: " << AliHLTLog::kDec << buffer
			<< "/" << offset << "/" << size << "." << ENDLOG;
#ifdef DO_BENCHMARK
		    if ( fBenchmark )
			{
			gettimeofday( &t8, NULL );
			dt = (t8.tv_sec-t7.tv_sec)*1000000+(t8.tv_usec-t7.tv_usec);
			LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::DoProcessing", "Benchmark 7" )
			    << "Time from 7 to 8: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
			gettimeofday( &t8, NULL );
			}
#endif
		    if ( ok )
			{
			shmID = fBufferManager->GetShmKey( buffer );
			outData = fShmManager->GetShm( shmID );
#ifdef DO_BENCHMARK
			if ( fBenchmark )
			    {
			    gettimeofday( &t9, NULL );
			    dt = (t9.tv_sec-t8.tv_sec)*1000000+(t9.tv_usec-t8.tv_usec);
			    LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::DoProcessing", "Benchmark 8" )
				<< "Time from 8 to 9: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
			    gettimeofday( &t9, NULL );
			    }
#endif
			}
		    if ( outData || forceForward )
			{ // Got output shm ptr
			if (size >= 0x100000000) //This might be cased to integers later on, so make sure that our size (for one event) fits into a signed int
			    outSize = 0x7FFFFFFF;
			else
			    outSize = size;
			if ( outData )
			    outData += offset;
			else
			    outData = NULL;
			vector<AliHLTSubEventDescriptor::BlockData> outBlocks;
#ifdef DO_BENCHMARK
			if ( fBenchmark )
			    {
			    gettimeofday( &t10, NULL );
			    dt = (t10.tv_sec-t9.tv_sec)*1000000+(t10.tv_usec-t9.tv_usec);
			    LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::DoProcessing", "Benchmark 9" )
				<< "Time from 9 to 10: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
			    gettimeofday( &t10, NULL );
			    }
#endif
			edd = NULL;
			if ( fStatus )
			    {
			    struct timeval t1;
			    AliUInt64_t tm;
			    gettimeofday( &t1, NULL );
			    tm = t1.tv_sec;
			    tm *= 1000000;
			    tm += t1.tv_usec;
// 			    fStatus->fHLTStatus.fCurrentEventStart_s = t1.tv_sec;
// 			    fStatus->fHLTStatus.fCurrentEventStart_us = t1.tv_usec;
			    fStatus->fHLTStatus.fCurrentEventStart = tm;
			    fStatus->fHLTStatus.fCurrentDataEvent = eventID.fNr;
			    fStatus->Touch();
			    }
			bool canceled = ed->fCanceled;
// 			if ( fStatus )
// 			    fStatus->fProcessStatus.fState |= kAliHLTPCBusyStateFlag;
#if __GNUC__>=3
			if ( canceled || forceForward || (eventID.fType==kAliEventTypeTick && !fProcessTickEvents) || ProcessEvent( eventID, dataBlocks.size(), dataBlocks.begin().base(), ed->fSEDD, ets, outData, outSize, outBlocks, edd ) )
#else
			if ( canceled || forceForward || (eventID.fType==kAliEventTypeTick && !fProcessTickEvents) || ProcessEvent( eventID, dataBlocks.size(), dataBlocks.begin(), ed->fSEDD, ets, outData, outSize, outBlocks, edd ) )
#endif
			    {
			    if ( edd )
				{
				if ( ed && ed->fSEDD )
				    edd->fStatusFlags |= ed->fSEDD->fStatusFlags;
				else
				    edd->fStatusFlags = 0;
				}
			    if ( fPublisher && fForceEventDoneDataForward )
				{
				if ( edd )
				    fPublisher->EventDone( *this, *edd, fForceEventDoneDataForward, true );
				else
				    {
				    AliHLTEventDoneData eddTmp;
				    if ( ed && ed->fSEDD )
					AliHLTMakeEventDoneData( &eddTmp, eventID, ed->fSEDD->fStatusFlags );
				    else
					AliHLTMakeEventDoneData( &eddTmp, eventID, 0 );
				    fPublisher->EventDone( *this, eddTmp, fForceEventDoneDataForward, true );
				    }
				}
			    if ( fEventAccounting )
				fEventAccounting->EventProcessed( eventID, outBlocks.size(), outSize );

			    if ( canceled || forceForward || (eventID.fType==kAliEventTypeTick && !fProcessTickEvents) || (ed && ed->fSEDD && (ed->fSEDD->fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST) && fSecondary) )
				{
				outSize = 0;
				outBlocks.clear();
				sendDone = false;
				retry = false;
				}
#define NEWBUFFERSIZE_BLOCK_LOGIC
#ifdef NEWBUFFERSIZE_BLOCK_LOGIC
			    vector<AliHLTSubEventDescriptor::BlockData>::iterator outBlockIter=outBlocks.begin(), outBlockEnd=outBlocks.end();
			    unsigned long highestOffset=0, lowestOffset=0;
			    AliHLTShmID invalidShmID;
			    invalidShmID.fShmType = kInvalidShmType;
			    invalidShmID.fKey.fID = ~(AliUInt64_t)0;
			    while ( outBlockIter!=outBlockEnd )
				{
				if ( (outBlockIter->fShmKey==shmID || outBlockIter->fShmKey==invalidShmID) )
				    {
				    highestOffset = outBlockIter->fOffset+outBlockIter->fSize;
				    lowestOffset = outBlockIter->fOffset;
				    break;
				    }
				outBlockIter++;
				}
			    while ( outBlockIter!=outBlockEnd )
				{
				if ( (outBlockIter->fShmKey==shmID || outBlockIter->fShmKey==invalidShmID) && highestOffset < outBlockIter->fOffset+outBlockIter->fSize )
				    highestOffset = outBlockIter->fOffset+outBlockIter->fSize;
				if ( (outBlockIter->fShmKey==shmID || outBlockIter->fShmKey==invalidShmID) && lowestOffset > outBlockIter->fOffset )
				    lowestOffset = outBlockIter->fOffset;
				outBlockIter++;
				}
			    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::DoProcessing", "Output data" )
				<< "Output data sizes for event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
				<< eventID << "): " 
				<< AliHLTLog::kDec << lowestOffset << " (0x" << AliHLTLog::kHex << lowestOffset << ") - "
				<< AliHLTLog::kDec << highestOffset << " (0x" << AliHLTLog::kHex << highestOffset
				<< ")." << ENDLOG;
			    if ( highestOffset > size )
				{
				LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::DoProcessing", "Output data too big" )
				    << "Output data for event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
				    << eventID << ") is too big: " << highestOffset << " (0x" << AliHLTLog::kHex << highestOffset
				    << ") with only " << AliHLTLog::kDec << size << " (0x" << AliHLTLog::kHex << size
				    << ") allowed." << ENDLOG;
				}
#else
			    if ( outSize!=0 && outBlocks.size()==0 )
				{
				LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::DoProcessing", "Output data mismatch" )
				    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
				    << eventID << ": Output data size is unequal zero but no output data blocks are produced."
				    << ENDLOG;
				}
			    if ( fParanoidOutputBlockChecks )
				{
				vector<AliHLTSubEventDescriptor::BlockData>::iterator outBlockIter=outBlocks.begin(), outBlockEnd=outBlocks.end();
				unsigned long highestOffsetEnd = 0;
				while ( outBlockIter!=outBlockEnd )
				    {
				    if ( (outBlockIter->fShmKey==shmID || outBlockIter->fShmKey==invalidShmID) && highestOffsetEnd < outBlockIter->fOffset+outBlockIter->fSize )
					highestOffsetEnd = outBlockIter->fOffset+outBlockIter->fSize;
				    outBlockIter++;
				    }
				if ( highestOffsetEnd!=outSize )
				    {
				    LOG( AliHLTLog::kWarning, "AliHLTProcessingSubscriber::DoProcessing", "Possible output data mismatch" )
					<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
					<< eventID << ": Output data size is " << AliHLTLog::kDec << outSize
					<< " but last output block ends at offset " << highestOffsetEnd << "."
					<< ENDLOG;
				    }
				}
			    if ( outSize > size )
				{
				LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::DoProcessing", "Output data too big" )
				    << "Output data for event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
				    << eventID << ") is too big: " << outSize << " (0x" << AliHLTLog::kHex << outSize
				    << ") with only " << AliHLTLog::kDec << size << " (0x" << AliHLTLog::kHex << size
				    << ") allowed." << ENDLOG;
				}
#endif
			    if ( fStatus )
				{
				struct timeval t1;
				AliUInt64_t tm;
				gettimeofday( &t1, NULL );
				tm = t1.tv_sec;
				tm *= 1000000;
				tm += t1.tv_usec;
				fStatus->fHLTStatus.fLastDataEvent = AliEventID_t().fNr;
// 				fStatus->fHLTStatus.fLastEventStart_s = fStatus->fHLTStatus.fCurrentEventStart_s;
// 				fStatus->fHLTStatus.fLastEventStart_us = fStatus->fHLTStatus.fCurrentEventStart_us;
// 				fStatus->fHLTStatus.fLastEventEnd_s = t1.tv_sec;
// 				fStatus->fHLTStatus.fLastEventEnd_us = t1.tv_usec;
				fStatus->fHLTStatus.fLastEventStart = fStatus->fHLTStatus.fCurrentEventStart;
				fStatus->fHLTStatus.fLastEventEnd = tm;
				fStatus->fHLTStatus.fLastDataEvent = eventID.fNr;
				fStatus->fHLTStatus.fCurrentDataEvent = AliEventID_t().fNr;
				if ( eventID.fType != kAliEventTypeTick && eventID.fType!=kAliEventTypeReconfigure && eventID.fType!=kAliEventTypeDCSUpdate )
				    fStatus->fHLTStatus.fProcessedEventCount++;
				fStatus->fHLTStatus.fTotalProcessedOutputDataSize += outSize;
				fStatus->fHLTStatus.fTotalProcessedInputDataSize += dataBlocksTotalSize;
				fStatus->Touch();
				}
			    // event processed successfully
			    if ( edd )
				PutEventDoneData( edd );
#ifdef DO_BENCHMARK
			    if ( fBenchmark )
				{
				gettimeofday( &t11, NULL );
				dt = (t11.tv_sec-t10.tv_sec)*1000000+(t11.tv_usec-t10.tv_usec);
				LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::DoProcessing", "Benchmark 10" )
				    << "Time from 10 to 11: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
				gettimeofday( &t11, NULL );
				}
#endif
#ifdef NEWBUFFERSIZE_BLOCK_LOGIC
			    LOG( AliHLTLog::kDebug, "ALiL3ProcessingSubscriber::DoProcessing", "Event processed" )
				<< "Event 0x" << AliHLTLog::kHex << ed->fEventID << AliHLTLog::kDec << " (" 
				<< ed->fEventID << ") processed. " << outBlocks.size() << " datablock"
				<< ( outBlocks.size()!=1 ? "s" : "" ) << " with " 
				<< highestOffset-lowestOffset << " bytes produced (canceled: " << (canceled ? "true" : "false") << ")." << ENDLOG;
#else
			    LOG( AliHLTLog::kDebug, "ALiL3ProcessingSubscriber::DoProcessing", "Event processed" )
				<< "Event 0x" << AliHLTLog::kHex << ed->fEventID << AliHLTLog::kDec << " (" 
				<< ed->fEventID << ") processed. " << outBlocks.size() << " datablock"
				<< ( outBlocks.size()!=1 ? "s" : "" ) << " with " 
				<< outSize << " bytes produced (canceled: " << (canceled ? "true" : "false") << ")." << ENDLOG;
#endif

			    if ( !canceled && (fRePublisher || fAsync) )
				{
				// RePublisher available
				AliUInt32_t i, n = outBlocks.size();
#ifdef NEWBUFFERSIZE_BLOCK_LOGIC
#if 0 
				// Not allowed with new buffer logic ; 20100216
				if ( lowestOffset<size && highestOffset>0 )
#else
				if ( lowestOffset || highestOffset )
#endif
				    {
				    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::DoProcessing", "Output data" )
					<< "Adapting" << ENDLOG;
				    fBufferManager->AdaptBlock( buffer, offset, offset+lowestOffset, highestOffset-lowestOffset );
				    }
#else
				if ( outSize > 0 )
				    {
				    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::DoProcessing", "Output data" )
					<< "Adapting" << ENDLOG;
				    fBufferManager->AdaptBlock( buffer, offset, outSize );
				    }
#endif
				else if ( !forceForward && ok )
				    {
				    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::DoProcessing", "Output data" )
					<< "Releasing" << ENDLOG;
				    fBufferManager->ReleaseBlock( buffer, offset, true );
				    }
				if ( fStatus )
				    {
				    fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
				    fStatus->Touch();
				    }
				if ( fBufferWarnings )
				    {
				    double freeRatio = (((double)fBufferManager->GetFreeBufferSize())/((double)fBufferManager->GetTotalBufferSize()));
				    if ( !fBuffer20PercentWarning && freeRatio<0.2 && freeRatio>=0.1 )
					{
					LOG( AliHLTLog::kWarning, "AliHLTProcessingSubscriber::DoProcessing", 
					     "Less than 20% free output buffer" )
					  << "Less than 20 percent free output buffer available (" 
					  << freeRatio << " percent -- " << AliHLTLog::kDec
					  << fBufferManager->GetFreeBufferSize()/(1024*1024) << " MB of "
					  << fBufferManager->GetTotalBufferSize()/(1024*1024) << " MB available)." 
					  << ENDLOG;
					fBuffer20PercentWarning = true;
					}
				    if ( !fBuffer10PercentWarning && freeRatio<0.1 )
					{
					LOG( AliHLTLog::kWarning, "AliHLTProcessingSubscriber::DoProcessing",
					     "Less than 10% free output buffer" )
					  << "Less than 10 percent free output buffer available (" 
					  << freeRatio << " percent -- " << AliHLTLog::kDec
					  << fBufferManager->GetFreeBufferSize()/(1024*1024) << " MB of "
					  << fBufferManager->GetTotalBufferSize()/(1024*1024) << " MB available)."
					  << ENDLOG;
					fBuffer20PercentWarning = true;
					fBuffer10PercentWarning = true;
					}
				    }

#ifdef DO_BENCHMARK
				if ( fBenchmark )
				    {
				    gettimeofday( &t12, NULL );
				    dt = (t12.tv_sec-t11.tv_sec)*1000000+(t12.tv_usec-t11.tv_usec);
				    LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::DoProcessing", "Benchmark 11" )
					<< "Time from 11 to 12: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
				    gettimeofday( &t12, NULL );
				    }
#endif


				OutBlockData obd;
				obd.fEventID = ed->fEventID;
#ifdef NEWBUFFERSIZE_BLOCK_LOGIC
#if 0 
				// Not allowed with new buffer logic ; 20100216 / 20100517
				if ( lowestOffset<size && highestOffset>0 )
#else
				if ( lowestOffset || highestOffset )
#endif
				    {
				    obd.fBufferNdx = buffer;
				    obd.fOffset = offset+lowestOffset;
				    obd.fSize = highestOffset-lowestOffset;
				    obd.fShmKey.fShmType = shmID.fShmType;
				    obd.fShmKey.fKey.fID = shmID.fKey.fID;
				    }
#else
				if ( outSize > 0 )
				    {
				    obd.fBufferNdx = buffer;
				    obd.fOffset = offset;
				    obd.fSize = outSize;
				    obd.fShmKey.fShmType = shmID.fShmType;
				    obd.fShmKey.fKey.fID = shmID.fKey.fID;
				    }
#endif
				else
				    {
				    obd.fBufferNdx = ~(AliUInt32_t)0;
				    obd.fOffset = ~(AliUInt32_t)0;
				    obd.fSize = 0;
				    obd.fShmKey.fShmType = kInvalidShmType;
				    obd.fShmKey.fKey.fID = ~(AliUInt64_t)0;
				    }
// 				ret = pthread_mutex_lock( &fOutBlockMutex );
// 				if ( ret )
// 				    {
// 				    }
				ed->fOutBlocks.Add( obd );
// 				ret = pthread_mutex_unlock( &fOutBlockMutex );
// 				if ( ret )
// 				    {
// 				    }
				
#ifdef DO_BENCHMARK
				if ( fBenchmark )
				    {
				    gettimeofday( &t13, NULL );
				    dt = (t13.tv_sec-t12.tv_sec)*1000000+(t13.tv_usec-t12.tv_usec);
				    LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::DoProcessing", "Benchmark 12" )
					<< "Time from 13 to 12: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
				    gettimeofday( &t13, NULL );
				    }
#endif

				
				if ( !fAsync )
				    {
				    for ( i = 0; i < n; i++ )
					{
					if ( outBlocks[i].fShmKey.fShmType == kInvalidShmType && outBlocks[i].fShmKey.fKey.fID == ~(AliUInt64_t)0 )
					    {
					    outBlocks[i].fShmKey.fShmType = fBufferManager->GetShmKey( buffer ).fShmType;
					    outBlocks[i].fShmKey.fKey.fID = fBufferManager->GetShmKey( buffer ).fKey.fID;
					    outBlocks[i].fOffset += offset;
					    outBlocks[i].fProducerNode = fNodeID;
					    AliHLTSubEventDescriptor::FillBlockAttributes( outBlocks[i].fAttributes );
					    }
					}
				    unsigned long NN = dataBlocks.size();
				    vector<AliHLTSubEventDescriptor::BlockData>::iterator insert = outBlocks.begin();
				    for ( unsigned mm = 0; mm<NN; mm++ )
					{
					if ( dataBlocks[mm].fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_PASSTHROUGH ||
					     ed->fSEDD->fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_FULLPASSTHROUGH )
					    {
					    insert = outBlocks.insert( insert, dataBlocks[mm] );
					    insert++;
					    }
					}
				    pthread_mutex_lock( &fPublisherCurrentEventDoneMutex );
				    fPublisherCurrentEventDone = false;
				    fPublisherCurrentEvent = ed->fEventID;
				    pthread_mutex_unlock( &fPublisherCurrentEventDoneMutex );
	
				    ret = AnnounceEvent( ed, outBlocks );
				    pthread_mutex_lock( &fPublisherCurrentEventDoneMutex );
				    if ( fPublisherCurrentEventDone )
					sendDone = false;
				    pthread_mutex_unlock( &fPublisherCurrentEventDoneMutex );
				    if ( ret )
					{
					// ...
					if ( ok )
					    fBufferManager->ReleaseBlock( buffer, offset );
					if ( fStatus )
					    {
					    fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
					    fStatus->Touch();
					    }
					retry = true;
					LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::DoProcessing", "Error announcing event" )
					    << "Error announcing event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
					    << eventID << ")." << ENDLOG;
					}
				    }
				else
				    {
				    ed->fOutputBlocks = outBlocks;
				    // XXX ???
				    // ...
				    // XXX ??? ed->fActive = false; // Actually done further down
				    }

				}
			    else
				{
				if ( !fNoRePublishing )
				    {
				    LOG( AliHLTLog::kInformational, "AliHLTProcessingSubscriber::Run", "No republisher" )
					<< "Event 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec
					<< ed->fEventID << ") will not be published." << ENDLOG;
				    // ...
				    }
#ifdef DO_BENCHMARK
				if ( fBenchmark )
				    gettimeofday( &t16, NULL );
#endif
				if ( ok && !forceForward )
				    fBufferManager->ReleaseBlock( buffer, offset, true );
				if ( fStatus )
				    {
				    fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
				    fStatus->Touch();
				    }
				sendDone = true;
				}
			    }
			else
			    {
			    if ( fStatus )
				{
				fStatus->fHLTStatus.fCurrentDataEvent = AliEventID_t().fNr;
				fStatus->Touch();
				}
			    if ( ok && !forceForward )
				fBufferManager->ReleaseBlock( buffer, offset, true );
			    if ( fStatus )
				{
				fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
				fStatus->Touch();
				}
			    retry = true;
			    LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::DoProcessing", "Error processing event" )
				<< "Error processing event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
				<< eventID << ")." << ENDLOG;
			    // ...
			    }
// 			if ( fStatus )
// 			    fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
			outBlocks.clear();
			}
		    else
			{
			if ( ok && !forceForward )
			    fBufferManager->ReleaseBlock( buffer, offset, true );
			if ( fStatus )
			    {
			    fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
			    fStatus->Touch();
			    }
			retry = true;
			LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::DoProcessing", "Error getting output pointer" )
			    << "Error getting output shm pointer for event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			    << eventID << ")." << ENDLOG;
			// ...
			}
		    
		    }
		else
		    {
		    if ( !fBufferManager->IsBlockFeasible( minBlockSize ) )
			{
			LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::DoProcessing", "GetBuffer " )
			    << "Output shm block of requested size " << AliHLTLog::kDec << minBlockSize 
			    << " (0x" << AliHLTLog::kHex << minBlockSize
			    << ") bytes from publisher for event 0x" << AliHLTLog::kHex << eventID << " ("
			    << AliHLTLog::kDec << eventID << ") can never be obtained from buffer of size "
			    << AliHLTLog::kDec << fBufferManager->GetTotalBufferSize()
			    << " (0x" << AliHLTLog::kHex << fBufferManager->GetTotalBufferSize() << "). Event has be aborted... (Debug: fPerEventFixedSize: " 
			    << AliHLTLog::kDec << fPerEventFixedSize << " - fPerBlockFixedSize: " << fPerBlockFixedSize << " - fSizeFactor: " << fSizeFactor
			    << " - block count: " << dataBlockCnt << " - total input data size: " << dataBlocksTotalSize << ")" << ENDLOG;
			if ( fForwardMaxRetryEvents )
			    {
			    retry = true;
			    ed->fForceEmptyForward = true;
			    }
			}
		    else 
			retry = true;
		    LOG( AliHLTLog::kInformational, "AliHLTProcessingSubscriber::DoProcessing", "Error getting output block" )
			<< "Error getting output shm block for event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ")." << ENDLOG;
		    }
		}
	    else
		{
		retry = true;
		LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::DoProcessing", "Unable to get output block" )
		    << "Unable to get output shm block from buffer manager for event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ")." << ENDLOG;
		}
#if 0
	    vector<AliHLTSubEventDescriptor::BlockData>::iterator dbIter, dbEnd;
	    dbIter = dataBlocks.begin();
	    dbEnd = dataBlocks.end();
	    while ( dbIter != dbEnd )
		{
		fShmManager->ReleaseShm( dbIter->fShmKey );
		dbIter++;
		}
	    dataBlocks.clear();
#else
	    while ( !dataBlocks.empty() )
		{
		fShmManager->ReleaseShm( dataBlocks.begin()->fShmKey );
		dataBlocks.erase( dataBlocks.begin() );
		}
#endif
#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		{
		gettimeofday( &t17, NULL );
		dt = (t17.tv_sec-t16.tv_sec)*1000000+(t17.tv_usec-t16.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::DoProcessing", "Benchmark 16" )
		    << "Time from 16 to 17: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		gettimeofday( &t17, NULL );
		}
#endif

	    ret = pthread_mutex_lock( &fEventRetryMutex );
	    if ( ret )
		{
		// XXX
		}
	    if ( retry )
		{
		sendDone = false;
		success = false;
		vector<EventRetryData>::iterator retryIter, retryEnd;
		retryIter = fEventRetries.begin();
		retryEnd =  fEventRetries.end();
		while ( retryIter != retryEnd )
		    {
		    if ( retryIter->fEventData == ed )
			break;
		    retryIter++;
		    }
		if ( retryIter == retryEnd )
		    {
		    LOG( AliHLTLog::kInformational, "AliHLTProcessingSubscriber::DoProcessing", "Event Retry" )
			<< "Event 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID
			<< ") retry." << ENDLOG;
		    LOG( AliHLTLog::kInformational, "AliHLTProcessingSubscriber::DoProcessing", "Event Retry" )
			<< "Scheduling event 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID
			<< ") for retry." << ENDLOG;
// 		    LOG( AliHLTLog::kWarning, "AliHLTProcessingSubscriber::DoProcessing", "Event Retry" )
// 			<< "Event 0x" << AliHLTLog::kHex << sedd->fEventID << " (" << AliHLTLog::kDec << sedd->fEventID
// 			<< ") retry." << ENDLOG;
		    EventRetryData retryData;
		    retryData.fRetryCount = 0;
		    retryData.fEventData = ed;
		    retryData.fTimerID = ~(AliUInt32_t)0;
		    retryData.fActive = false;
		    retryIter = fEventRetries.insert( fEventRetries.end(), retryData );
		    }
#if __GNUC__>=3
		if ( retryIter.base() )
#else
		if ( retryIter )
#endif
		    {
		    if ( fMaxRetries !=0 && !fForwardMaxRetryEvents && ++(retryIter->fRetryCount)>fMaxRetries )
			{
			LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::DoProcessing", "Event Aborted" )
			    << "Event 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID
			    << ") aborted after " << retryIter->fRetryCount << " retries." << ENDLOG;
			sendDone = true;
			fEventRetries.erase( retryIter );
			}
		    else
			{
			if ( fMaxRetries !=0 && fForwardMaxRetryEvents && fBufferManager->GetFreeBufferSize()==fBufferManager->GetTotalBufferSize() && ++(retryIter->fRetryCount)>fMaxRetries )
			    ed->fForceEmptyForward = true;
			LOG( AliHLTLog::kInformational, "AliHLTProcessingSubscriber::DoProcessing", "Event Retry" )
			    << "Event 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID
			    << ") scheduled for retry after " << retryIter->fRetryCount << " tries." << ENDLOG;
			retryIter->fTimerID = fTimer.AddTimeout( fRetryTime, &fRetryCallback, reinterpret_cast<AliUInt64_t>(ed) );
			retryIter->fActive = false;
			}
		    }
		if ( !sendDone )
		    {
		    if ( !isRetryEvent )
			{
			fEventSignal.Lock();
			fRetryData.Add( ed );
			fEventSignal.Unlock();
			}
		    }
		else if ( isRetryEvent )
		    {
		    fEventSignal.Lock();
		    fRetryData.RemoveFirst();
		    fEventSignal.Unlock();
		    }
		// XXXX
		}
	    else
		{
		if ( isRetryEvent )
		    {
		    fEventSignal.Lock();
		    fRetryData.RemoveFirst();
		    fEventSignal.Unlock();
		    }
		vector<EventRetryData>::iterator retryIter, retryEnd;
		retryIter = fEventRetries.begin();
		retryEnd =  fEventRetries.end();
		while ( retryIter != retryEnd )
		    {
		    if ( retryIter->fEventData == ed )
			{
			LOG( AliHLTLog::kInformational, "AliHLTProcessingSubscriber::DoProcessing", "Retry Event Done" )
			    << "Event 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID
			    << ") removed from retry list after successful processing after " << retryIter->fRetryCount << " tries." << ENDLOG;
			fEventRetries.erase( retryIter );
			break;
			}
		    retryIter++;
		    }
		}
	    ret = pthread_mutex_unlock( &fEventRetryMutex );
	    if ( ret )
		{
		// XXX
		}

	    pthread_mutex_lock( &fEventDataMutex );
	    if ( !fAsync )
		ed->fActive = false;
	    if ( ed->fDone )
		sendDone = true;
	    pthread_mutex_unlock( &fEventDataMutex );

	    if ( sendDone )
		SignalCleanupInEvent( ed->fEventID, ed->fPostponedEDD );
#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		{
		gettimeofday( &t18, NULL );
		dt = (t18.tv_sec-t17.tv_sec)*1000000+(t18.tv_usec-t17.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::DoProcessing", "Benchmark 17" )
		    << "Time from 17 to 18: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		gettimeofday( &t18, NULL );
		}
#endif


	    //#define DO_BENCHMARK
#ifdef DO_BENCHMARK
	    if ( fBenchmark )
		{
		gettimeofday( &tfinal, NULL );
		dt = (tfinal.tv_sec-t1.tv_sec)*1000000+(tfinal.tv_usec-t1.tv_usec);
		LOG( AliHLTLog::kBenchmark, "AliHLTProcessingSubscriber::DoProcessing", "Benchmark Final" )
		    << "Time from 1 to final: " << AliHLTLog::kDec << dt << " musec." << ENDLOG;
		}
#endif
	    //if ( fStatus )
	    //fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
// 	    if ( fStatus && fStatus->fProcessStatus.fState==kAliHLTPCBusyState )
// 		fStatus->fProcessStatus.fState = kAliHLTPCWaitingState;


	    fEventSignal.Lock();
	    }
	}
    fProcessingQuitted = true;
    fEventSignal.Unlock();

//     if ( fStatus )
// 	{
// 	switch ( fStatus->fProcessStatus.fState )
// 	    {
// 	    case kAliHLTPCWaitingState:
// 		fStatus->fProcessStatus.fState = kAliHLTPCReadyState;
// 		break;
// 	    case kAliHLTPCPublisherErrorProcessingState:
// 		fStatus->fProcessStatus.fState = kAliHLTPCPublisherErrorAcceptingState;
// 		break;
// 	    }
// 	}
    }

void AliHLTProcessingSubscriber::QuitProcessing()
    {
    // ...
    if ( fProcessingQuitted )
	return;
    fQuitProcessing = true;
    fEventSignal.Signal();
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 5000000;
    gettimeofday( &start, NULL );
    while ( !fProcessingQuitted && deltaT<timeLimit )
	{
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( fProcessingQuitted )
	fProcessingThread.Join();
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTProcessingSubscriber::QuitProcessing", "Aborting processing thread" )
	    << "Processing thread cannot be ended. Aborting now..."
	    << ENDLOG;
	fProcessingThread.Abort();
	}
    EventData* ed;
    AliHLTEventDoneData edd;
    AliHLTMakeEventDoneData( &edd, AliEventID_t() );
    while ( fEventSignal.HaveNotificationData() )
	{
	ed = reinterpret_cast<EventData*>( fEventSignal.PopNotificationData() );
	fEventSignal.Unlock();
	if ( ed )
	    {
	    edd.fEventID = ed->fEventID;
	    CleanupInEvent( &edd );
	    // XXX TODO for below line: Add notification data 
	    // SignalCleanupInEvent( ed->fEventID );
	    }
	fEventSignal.Lock();
	}
    fEventSignal.Unlock();
    }


AliHLTProcessingSubscriber::AliHLTCleanupThread::AliHLTCleanupThread( AliHLTProcessingSubscriber& sub ):
    fSub( sub ) 
    { 
    }

AliHLTProcessingSubscriber::AliHLTCleanupThread::~AliHLTCleanupThread()
    {
    }

void AliHLTProcessingSubscriber::AliHLTCleanupThread::Run()
    {
    fSub.DoCleanupLoop();
    }

void AliHLTProcessingSubscriber::DoCleanupLoop()
    {
    fCleanupQuitted = false;
    AliHLTEventDoneData* edd;
    fCleanupSignal.Lock();
    while ( !fQuitCleanup )
	{
	if ( !fCleanupSignal.HaveNotificationData() )
	    fCleanupSignal.Wait();
	while ( fCleanupSignal.HaveNotificationData() && !fQuitCleanup )
	    {
	    edd = fCleanupSignal.PopNotificationData();
	    if ( !edd )
		{
		continue;
		}
#if 0
	    // XXX ALERT CATCH ME DEBUGGING AID
	    if ( edd->fHeader.fLength != 28 )
		*((int*)NULL) = 0;
	    // XXX
#endif
	    fCleanupSignal.Unlock();
	    if ( CleanupInEvent( edd ) )
		fEDDCache.Release( (uint8*)edd );
	    fCleanupSignal.Lock();
	    }
	}
    fCleanupSignal.Unlock();
    fCleanupQuitted = true;
    if ( fStatus )
	fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
    }
	
void AliHLTProcessingSubscriber::QuitCleanup()
    {
    fQuitCleanup = true;
    fCleanupSignal.Signal();
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 5000000;
    gettimeofday( &start, NULL );
    while ( !fCleanupQuitted && deltaT<timeLimit )
	{
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( fCleanupQuitted )
	fCleanupThread.Join();
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTProcessingSubscriber::QuitCleanup", "Aborting cleanup thread" )
	    << "Cleanup thread cannot be ended. Aborting now..."
	    << ENDLOG;
	fCleanupThread.Abort();
	}
    }


void AliHLTProcessingSubscriber::AliHLTRetryCallback::TimerExpired( AliUInt64_t, unsigned long )
    {
//     int ret;
//     bool found = false;
//     EventData* ed = reinterpret_cast<EventData*>(notData);
//     vector<EventRetryData>::iterator iter, end;
//     if ( !ed )
// 	{
// 	LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::AliHLTRetryCallback::TimerExpired", "NULL SEDD" )
// 	    << "NULL pointer received as sub-event data descriptor." << ENDLOG;
// 	return;
// 	}
//     ret = pthread_mutex_lock( &(fSub.fEventRetryMutex) );
//     if ( ret )
// 	{
// 	// XXX
// 	}
    
//     iter = fSub.fEventRetries.begin();
//     end = fSub.fEventRetries.end();
//     while ( iter != end )
// 	{
// 	if ( iter->fEventData==ed )
// 	    {
// 	    iter->fActive = true;
// 	    found = true;
// 	    break;
// 	    }
// 	iter++;
// 	}

//     if ( !found )
// 	{
// 	LOG( AliHLTLog::kWarning, "AliHLTProcessingSubscriber::AliHLTRetryCallback::TimerExpired", "SEDD not found" )
// 	    << "SEDD not found in event retry data list. Maybe deleted elsewehere??" << ENDLOG;
// 	}

//     ret = pthread_mutex_unlock( &(fSub.fEventRetryMutex) );
//     if ( ret )
// 	{
// 	// XXX
// 	}
    
//     if ( found )
// 	{

// 	//fSub.fEventSignal.SignalEvent( ed, true );
// 	fSub.fEventSignal.Signal();

// 	}
    fSub.fEventSignal.Signal();
    }


void AliHLTProcessingSubscriber::CleanupOldEvents()
    {
    if ( !fOldestEventBirth_s )
	return;
    int ret;
    unsigned long ndx;
    vector<EventRetryData>::iterator iter, end;
    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::CleanupOldEvents", "Cleaning up old events" )
	<< "Subscriber " << GetName() << " received cleaning events older than "
	<< AliHLTLog::kDec << fOldestEventBirth_s << "." << ENDLOG;

    pthread_mutex_lock( &fEventDataMutex );
	
    if ( MLUCVectorSearcher<EventData*,AliUInt32_t>::FindElement( fEventData, &OldEventSearchFunc, fOldestEventBirth_s, ndx ) )
	{
	fEventData.Get( ndx )->fCanceled = true;
	}

    pthread_mutex_unlock( &fEventDataMutex );


    fEventSignal.Lock();
    while ( MLUCVectorSearcher<EventData*,AliUInt32_t>::FindElement( fRetryData, &OldEventSearchFunc, fOldestEventBirth_s, ndx ) )
	{
	fRetryData.Remove( ndx );
	}

    fEventSignal.Unlock();

    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::CleanupOldEvents", "Removing event" )
	<< "Removing events older than "
	<< AliHLTLog::kDec << fOldestEventBirth_s << " from pending event list..." << ENDLOG;
    bool found = false;
    ret = pthread_mutex_lock( &fEventRetryMutex );
    if ( ret )
	{
	// XXX
	}
    
    do
	{
	found = false;
	iter = fEventRetries.begin();
	end = fEventRetries.end();
	while ( iter != end )
	    {
#if __GNUC__>=3
	    if ( iter.base() && iter->fEventData && iter->fEventData->fSEDD && iter->fEventData->fSEDD->fEventBirth_s < fOldestEventBirth_s )
#else
		if ( iter && iter->fEventData && iter->fEventData->fSEDD && iter->fEventData->fSEDD->fEventBirth_s < fOldestEventBirth_s )
#endif
		    {
		    AliEventID_t eventID = iter->fEventData->fEventID;
		    if (  !iter->fActive )
			{
			LOG( AliHLTLog::kWarning, "AliHLTProcessingSubscriber::CleanupOldEvents", "Removing retry timeout" )
			    << "Removing retry timeout for event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			    << eventID << ")." << ENDLOG;
			fTimer.CancelTimeout( iter->fTimerID );
			}
		    LOG( AliHLTLog::kWarning, "AliHLTProcessingSubscriber::CleanupOldEvents", "Removing retry event" )
			<< "Removing event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ") from retry list." << ENDLOG;
		    fEventRetries.erase( iter );
		    found = true;
		    break;
		    }
	    iter++;
	    }
	}
    while ( found );
    
    ret = pthread_mutex_unlock( &fEventRetryMutex );
    if ( ret )
	{
	// XXX
	}
    
    pthread_mutex_lock( &fEventDataMutex );
    ndx=fEventData.GetStartNdx();
    //while ( MLUCVectorSearcher<PEventData,AliUInt32_t>::FindElement( fEventData, &OldEventSearchFunc, fOldestEventBirth_s, ndx ) )
    while ( MLUCVectorSearcher<PEventData,AliUInt32_t>::FindElement( fEventData, ndx, true, &OldEventSearchFunc, fOldestEventBirth_s, ndx ) )
	{
	AliEventID_t eventID = fEventData.Get( ndx )->fEventID;
	LOG( AliHLTLog::kWarning, "AliHLTProcessingSubscriber::CleanupOldEvents", "Cleaning up event" )
	    << "Cleaning up event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ")." << ENDLOG;
	pthread_mutex_unlock( &fEventDataMutex );
	fEventSignal.Lock();
	
	fEventSignal.RemoveEvent( eventID );
	
	fEventSignal.Unlock();
	SignalCleanupInEvent( eventID );
	pthread_mutex_lock( &fEventDataMutex );
	ndx++;
	}
    pthread_mutex_unlock( &fEventDataMutex );
    }

// Return true if their is watermark data to be sent.
// structure pointed to by waterMarkEDD must be able to accept/hold 6 more words.
bool AliHLTProcessingSubscriber::CreateWaterMarkEDD( AliHLTEventDoneData* const waterMarkEDD, AliEventID_t eventID, bool initializeEDD )
    {
    struct timeval now;
    gettimeofday( &now, NULL );
    bool renewHighWaterMark = false;
    unsigned long long pendingInput = 0;
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	pendingInput = fStatus->fHLTStatus.fPendingInputEventCount;
	}
    if ( fHighWaterMarkActive 
#if 0
#warning Water mark repeat time limit disabled
#else
	 && (unsigned long)now.tv_sec >= (unsigned long)(fHighWaterMarkSent.tv_sec+gkAliHLTEventDoneBackPressureHighWaterMarkExpiryTime_s-2) 
#endif
	 && pendingInput>=fLowWaterMark )
	{
	// renew high water mark before expiry, if input queue not below low water mark yet
	renewHighWaterMark = true;
	}
    if ( (!fHighWaterMarkActive && pendingInput>fHighWaterMark)
	 || (fHighWaterMarkActive && pendingInput<fLowWaterMark) 
	 || renewHighWaterMark )
	{
	
	if ( initializeEDD )
	    AliHLTMakeEventDoneData( waterMarkEDD, eventID, 0 );

	LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::CreateWaterMarkEDD", "Water Mark" )
	    << "Pending event count: " << AliHLTLog::kDec
	    << pendingInput << "." << ENDLOG;
	unsigned long off=waterMarkEDD->fDataWordCount;
	if ( fHighWaterMarkActive ) // Both to actually stop it and to renew
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::CreateWaterMarkEDD", "Low Water Mark" )
		<< "Sending back pressure low water mark ID 0x" 
		<< AliHLTLog::kHex << fHighWaterMarkID
		<< "." << ENDLOG;
	    waterMarkEDD->fDataWords[off+0] = kAliHLTEventDoneBackPressureLowWaterMark;
	    waterMarkEDD->fDataWords[off+1] = (AliUInt32_t)((fHighWaterMarkID & 0xFFFFFFFF00000000ULL) >> 32);
	    waterMarkEDD->fDataWords[off+2] = (AliUInt32_t)(fHighWaterMarkID & 0xFFFFFFFFULL);
	    waterMarkEDD->fDataWordCount += 3;
	    waterMarkEDD->fHeader.fLength += 3*sizeof(waterMarkEDD->fDataWords[0]);
	    off += 3;
	    fHighWaterMarkActive = false;
	    fHighWaterMarkID = 0;
	    }
	if ( (!fHighWaterMarkActive && pendingInput>fHighWaterMark) || renewHighWaterMark )
	    {
	    fHighWaterMarkSent = now;
	    fHighWaterMarkID = (((AliUInt64_t)now.tv_sec) << 32) | ((AliUInt64_t)now.tv_usec);
	    fHighWaterMarkActive = true;
	    waterMarkEDD->fDataWords[off+0] = kAliHLTEventDoneBackPressureHighWaterMark;
	    waterMarkEDD->fDataWords[off+1] = (AliUInt32_t)((fHighWaterMarkID & 0xFFFFFFFF00000000ULL) >> 32);
	    waterMarkEDD->fDataWords[off+2] = (AliUInt32_t)(fHighWaterMarkID & 0xFFFFFFFFULL);
	    waterMarkEDD->fDataWordCount += 3;
	    waterMarkEDD->fHeader.fLength += 3*sizeof(waterMarkEDD->fDataWords[0]);
	    LOG( AliHLTLog::kDebug, "AliHLTProcessingSubscriber::CreateWaterMarkEDD", "High Water Mark" )
		<< "Sending back pressure high water mark ID 0x" 
				    << AliHLTLog::kHex << fHighWaterMarkID
		<< "." << ENDLOG;
	    }
	
	return true;
	}
    return false;
    }


void AliHLTProcessingSubscriber::Init( const char* /*name*/, bool sendEventDone, int /*eventCountAlloc*/ )
    {
    fPingCount = 0;
    fSendEventDone = sendEventDone;
    fCurrentEvent = AliEventID_t();
    fQuitProcessing = fProcessingQuitted = false;
    fProcessingOk = false;
    fBenchmark = false;

    fNoRePublishing = false;

    fRePublisher = NULL;
    fPublisher = NULL;
    fBufferManager = NULL;
    fInDescriptorHandler = fOutDescriptorHandler = NULL;
    fShmManager = NULL;

    pthread_mutex_init( &fInSEDDMutex, NULL );
    pthread_mutex_init( &fOutSEDDMutex, NULL );
//     pthread_mutex_init( &fOutBlockMutex, NULL );
//     pthread_mutex_init( &fInBlockMutex, NULL );
    //pthread_mutex_init( &fSignalMutex, NULL );
    pthread_mutex_init( &fEventRetryMutex, NULL );
    pthread_mutex_init( &fEventDataMutex, NULL );
    pthread_mutex_init( &fDoneDataMutex, NULL );
    pthread_mutex_init( &fAsyncAnnounceMutex, NULL );
    pthread_mutex_init( &fPublisherCurrentEventDoneMutex, NULL );

    fNodeID = AliHLTGetNodeID();

    fPublisherCurrentEventDone = false;
    fPublisherCurrentEvent = AliEventID_t();
    fCleanupQuitted = true;
    fQuitCleanup = false;
    fStatus = NULL;
    fMaxRetries = 0;
    fRetryTime = 100; // Try ten times a second
    fPaused = false;
    fStatusRateUpdateTime_ms = 1000; // 1 second
    fStartTime.tv_sec = fStartTime.tv_usec = fResumeTime.tv_sec = fResumeTime.tv_usec = fLastMeasurementTime.tv_sec = fLastMeasurementTime.tv_usec = 0;
    fLastMeasurementReceiveEventCount = 0;
    fLastMeasurementProcessedEventCount = 0;
    fLastMeasurementAnnounceEventCount = 0;
    fLastMeasurementTotalProcessedInputDataSize = 0;
    fLastMeasurementTotalProcessedOutputDataSize = 0;

    fDefaultETS.fHeader.fLength = sizeof(fDefaultETS);
    fDefaultETS.fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
    fDefaultETS.fHeader.fSubType.fID = 0;
    fDefaultETS.fHeader.fVersion = 1;
    fDefaultETS.fDataWordCount = 0;

    AliHLTMakeEventDoneData( &fDefaultEDD, AliEventID_t() );

#ifdef EVENT_CHECK_PARANOIA
    fLastNewEvent = AliEventID_t();
#endif
    fOldestEventBirth_s = 0;
    fAsync = false;
    fAliceHLT = false;
    fRunNumber = 0;
    
    fProcessTickEvents = false;

    fHighWaterMarkActive = false;
    fHighWaterMarkID = 0;
#if 1
    fHighWaterMark = 100;
    fLowWaterMark = 10;
#else
#warning Water marks are 0
    fHighWaterMark = 0;
    fLowWaterMark = 0;
#endif
    fHighWaterMarkSent.tv_sec = fHighWaterMarkSent.tv_usec = 0;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
