/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTControlledComponent.hpp"
#include "AliHLTStatus.hpp"
#include "AliHLTSCCommand.hpp"
#include "AliHLTSCProcessControlCmds.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "AliHLTSCTypes.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTSCInterface.hpp"
#include "AliHLTRingBufferManager.hpp"
#include "AliHLTLargestBlockBufferManager.hpp"
#include "AliHLTDetectorPublisher.hpp"
#include "AliHLTPublisherProxyInterface.hpp"
#include "AliHLTPublisherPipeProxy.hpp"
#include "AliHLTPublisherShmProxy.hpp"
#include "AliHLTGetNodeID.hpp"
#include "AliHLTStackTrace.hpp"
#include "AliHLTSCComponentInterface.hpp"
#include "AliHLTEventAccounting.hpp"
#include "AliHLTTriggerStackMachine.hpp"
#include "BCLURLAddressDecoder.hpp"
#include "BCLAbstrAddress.hpp"
#include "BCLAddressLogger.hpp"
#include "MLUCCmdlineParser.hpp"
#include "MLUCRegEx.hpp"
#include "AliHLTReadoutList.hpp"
#include <syslog.h>

#include <stdio.h>
#define TRACE printf( "%s:%d\n", __FILE__, __LINE__ )

#if 0
#define TRACELOG( lvl, orig, keys ) LOG(lvl,orig,keys)
#else
#define TRACELOG( lvl, orig, keys ) LOG(AliHLTLog::kNone,orig,keys)
#endif



static void AcceptingSubscriptionsCallbackFunc( void* status )
    {
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker statusDataLock( ((AliHLTStatus*)status)->fStatusDataMutex );
    MLUCMutex::TLocker stateLock( ((AliHLTStatus*)status)->fStateMutex );
    ((AliHLTStatus*)status)->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
    }


static inline bool operator<( MLUCString const& s1, MLUCString const& s2 )
    {
    return strcmp( s1.c_str(), s2.c_str() )<0;
    }

static void QuitSignalHandler( int sigValue )
    {
    exit( (1 << 31) | sigValue );
    }



AliHLTControlledComponent::AliHLTControlledComponent( AliHLTControlledComponent::AliHLTControlledComponentType componentType, const char* compName, int argc, char** argv ):
    fCmdSignal( 5 ),
    fCmdProcessor( this ), fSubscriberEventCallback( this ),
    fMaxEventAge( 0 ),
#ifdef ALLOW_ARTIFICIAL_EVENT_LOSS
    fEventLossModulo( 0 ),
#endif
    fEventAccounting(NULL),
    fPreMapRorcBuffers()
    {
    fComponentType = componentType;
    switch ( fComponentType )
	{
	case kDataSource:
	    fComponentTypeName = "Data Source"; 
	    break;
	case kDataSink:
	    fComponentTypeName = "Data Sink"; 
	    break;
	case kDataProcessor:
	    fComponentTypeName = "Data Processor";
	    break;
	case kAsyncProcessor:
	    fComponentTypeName = "Asynchronous Processor";
	    break;
	default:
	    fComponentTypeName = "Unknown";
	    break;
	}
    fAllowDataTypeOriginSpecSet = false;
    fComponentName = compName;
    fArgc = argc;
    fArgv = argv;
    fStatus = NULL;
    fLogOut = NULL;
    fFileLog = NULL;
    fSysLog = NULL;
    fSysMESLog = NULL;
    fSysMESLogging = false;
    fDynLibLog = NULL;
    fDynLibPath = "";
    fPrivateSharedMemory = false;
    fSharedMemory = NULL;
    fShmManager = NULL;
    fBufferManager = NULL;
    fInDescriptorHandler = NULL;
    fOutDescriptorHandler = NULL;
    fPublisher = NULL;
    fSubscriberCount = 0;
    fPublisherProxy = NULL;
    fSubscriptionListenerThread = NULL;
    fOwnPublisherName = "";
    fAttachedPublisherName = "";
    fPub2SubProxyShmKey = fSub2PubProxyShmKey = (key_t)-1;
    fProxyShmSize = 0;
    fSubscriberID = "";
    fSubscriber = NULL;
    fSubscriptionThread = NULL;
    fSubscriberRetryCount = 0;
    fSubscriberRetryTime = 100; // milliseconds
    fUseSubscriptionEventModulo = false;
    fSubscriptionEventModulo = 1;
    fTransientSubscriber = false;
#ifdef USE_DDL_LIST_SHM
    fDDLListOutputShm = (AliUInt32_t)-1;
#endif
    fDDLListData = NULL;
    fTickInterval_us = 0;
    pthread_mutex_init( &fLAMAddressMutex, NULL );
    pthread_mutex_init( &fSubscriberRemovalListMutex, NULL );
    ResetConfiguration();
    AliHLTRegisterStackTraceHandlers();
    fMaxPendingEventCount = 0;
    fEventAccounting = NULL;
    fRequestPublisherEventDoneData = false;
    fSecondary = false;
    fEventModuloPreDivisor = 1;
    fTriggerStackMachine = NULL;
    fHardwareCoProcessor = false;
    fHardwareCoProcessorOutputEventDataType.fID = UNKNOWN_DATAID;
    fHardwareCoProcessorOutputEventDataTypeSet = false;
    fForceEventDoneDataForward = false;
    fAdaptBlockFreeUpdate = true;
    struct sigaction newSigAction;
    newSigAction.sa_sigaction = NULL;
    newSigAction.sa_handler = &QuitSignalHandler;
    sigemptyset( &newSigAction.sa_mask );
    newSigAction.sa_flags = 0;
    sigaction( SIGQUIT, &newSigAction, &fOldSigAction );

    fAllowIgnoreEventTriggerCommands = false;
    fIgnoreEventTriggerCommands = false;
    }


AliHLTControlledComponent::~AliHLTControlledComponent()
    {
    //Unconfigure();
    PostUnconfigure();
    pthread_mutex_destroy( &fLAMAddressMutex );
    pthread_mutex_destroy( &fSubscriberRemovalListMutex );
    }


void AliHLTControlledComponent::ResetConfiguration()
    {
    fStdoutLogging = true;
    fFileLogging = true;
    fSysLogging = false;
    fSysMESLogging = false;
    fSysMESLog = NULL;
    fDynLibLogging = false;
    fDynLibPath = "";
    fDynLibLog = NULL;
    if ( fComponentName )
	fLogName = fComponentName;
    else
	fLogName = fArgv[0];
    fSCInterfaceAddress = "tcpmsg://localhost:10000/";
    fBenchmark = false;

    fPrivateSharedMemory = false;
    fShmKey.fShmType = kInvalidShmType;
    fShmKey.fKey.fID = ~(AliUInt64_t)0;
    fBufferSize = 0;
    fMinBlockSize = 0;
    fPerEventFixedSize = 0;
    fPerBlockFixedSize = 0;
    fSizeFactor = 0.0;

    fMaxPreEventCount = 1;
    fMaxPreEventCountExp2 = 0;
    fMinPreBlockCount = 1;
    fMaxPreBlockCount = 1;

    fSendEventDone = false;

    fRunNumber = 0;
    fBeamType = 0;
    fHLTMode = 0;
    fRunType = "";
    //fRunType = 0;

    fMaxNoUseCount = 0;

    fPublisherTimeout = ~(AliUInt32_t)0;
    fPublisher = NULL;
    fSubscriberCount = 0;
    fPublisherProxy = NULL;
    fSubscriptionListenerThread = NULL;
    fOwnPublisherName = "";
    fAttachedPublisherName = "";
    fPub2SubProxyShmKey = fSub2PubProxyShmKey = (key_t)-1;
    fProxyShmSize = 0;

    fSubscriptionRequestOldEvents = true;

    fSubscriberID = "";
    fSubscriber = NULL;
    fSubscriptionThread = NULL;
    fSubscriberRetryCount = 0;
    fSubscriberRetryTime = 100; // milliseconds
    fSCInterface = NULL;

    fUseSubscriptionEventModulo = false;
    fSubscriptionEventModulo = 1;
    fTransientSubscriber = false;

    fPersistentStateShmID = -1;
    fPersistentState = NULL;

    fAliceHLT = false;

    fBufferManagerType = kLargestBlockBufferManager;

    fEventDataType.fID = UNKNOWN_DATAID;
    fEventDataOrigin.fID = UNKNOWN_DATAORIGIN;
    fEventDataSpecification = ~(AliUInt32_t)0;
    fDDLID = ~(AliUInt32_t)0;
#ifdef USE_DDL_LIST_SHM
    fDDLListShmKey.fShmType = kSysVShmPrivateType;
    fDDLListShmKey.fKey.fID = ~(AliUInt64_t)0;
#endif
    fDDLListData = NULL;

    fTickInterval_us = 0;

    fMaxPendingEventCount = 0;
    fEventAccounting = NULL;
    fRequestPublisherEventDoneData = false;
    fSecondary = false;
    fEventModuloPreDivisor = 1;

    while ( fEventTypeStackMachineCodes.size()>0 )
	{
	delete [] reinterpret_cast<uint8*>( fEventTypeStackMachineCodes[0] );
	fEventTypeStackMachineCodes.erase( fEventTypeStackMachineCodes.begin() );
	}

    fHardwareCoProcessor = false;
    fHardwareCoProcessorOutputEventDataType.fID = UNKNOWN_DATAID;
    fHardwareCoProcessorOutputEventDataTypeSet = false;

    fECSParameters = "";

    fForceEventDoneDataForward = false;

    fAllowIgnoreEventTriggerCommands = false;
    fIgnoreEventTriggerCommands = false;
    }


void AliHLTControlledComponent::Run()
    {
    if ( !PreConfigure() )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::Run", "Pre-Configuration Error" )
	    << "Pre-Configuration Error for " << fComponentTypeName << " component "
	    << fComponentName << "." << ENDLOG;
	PostUnconfigure();
	return;
	}
    LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::Run", "Component Started" )
	<< fComponentTypeName << " component " << fComponentName << " started." << ENDLOG;
    AliUInt32_t cmd = kAliHLTPCNOPCmd;
    bool quit = false;
    fCmdSignal.Lock();
    do
	{
	if ( !fCmdSignal.HaveNotificationData() )
	    fCmdSignal.Wait();
	while ( fCmdSignal.HaveNotificationData() )
	    {
	    cmd = fCmdSignal.PopNotificationData();
	    fCmdSignal.Unlock();
// XXXX TODO LOCKING XXXX
	    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    switch ( cmd )
		{
		case kAliHLTPCEndProgramCmd:
		    if ( (fStatus->fProcessStatus.fState & kAliHLTPCStartedStateFlag) &&
			 !(fStatus->fProcessStatus.fState & ~kAliHLTPCStartedStateFlag) )
			{
			quit = true;
			cmd = kAliHLTPCNOPCmd;
			}
		    break;
		case kAliHLTPCSubscribeCmd:
		    if ( fComponentType != kDataSink && fComponentType != kDataProcessor && fComponentType != kAsyncProcessor )
			break;
		    if ( (fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) &&
			 !(fStatus->fProcessStatus.fState & (kAliHLTPCChangingStateFlag|kAliHLTPCSubscribedStateFlag)) &&
			 !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
			{
			fStatus->fProcessStatus.fState |= (kAliHLTPCChangingStateFlag|kAliHLTPCSubscribedStateFlag);
			stateLock.Unlock();
			statusDataLock.Unlock();
			Subscribe();
			PreMapRorcBuffers();
			statusDataLock.Lock();
			stateLock.Lock();
			cmd = kAliHLTPCNOPCmd;
			}
		    if ( fStatus->fProcessStatus.fState & (kAliHLTPCChangingStateFlag|kAliHLTPCSubscribedStateFlag) )
			cmd = kAliHLTPCNOPCmd;
			
		    
		    // 		switch ( fStatus->fProcessStatus.fState )
		    // 		    {
		    // 		    case kAliHLTPCConfiguredState: // Fallthrough
		    // 		    case kAliHLTPCAcceptingSubscriptionsState: // Fallthrough
		    // 		    case kAliHLTPCWithSubscribersState:
		    
		    // 			cmd = kAliHLTPCNOPCmd;
		    // 			break;
		    // 		    }
		    break;
		case kAliHLTPCUnsubscribeCmd:
		    if ( fComponentType != kDataSink && fComponentType != kDataProcessor && fComponentType != kAsyncProcessor )
			break;
		    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) || (fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) )
			cmd = kAliHLTPCNOPCmd;
		    if ( (fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) &&
			 !(fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) && 
			 !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
			{
			fStatus->fProcessStatus.fState |= kAliHLTPCChangingStateFlag;
			stateLock.Unlock();
			statusDataLock.Unlock();
			UnMapRorcBuffers();
			Unsubscribe();
			statusDataLock.Lock();
			stateLock.Lock();
			fStatus->fProcessStatus.fState &= ~(kAliHLTPCChangingStateFlag|kAliHLTPCSubscribedStateFlag|kAliHLTPCConsumerErrorStateFlag);
			cmd = kAliHLTPCNOPCmd;
			}
		    break;
		case kAliHLTPCAbortSubscriptionCmd:
		    if ( fComponentType != kDataSink && fComponentType != kDataProcessor && fComponentType != kAsyncProcessor )
			break;
		    if ( (fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) &&
			 !(fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) && 
			 !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
			{
			fStatus->fProcessStatus.fState |= kAliHLTPCChangingStateFlag;
			stateLock.Unlock();
			statusDataLock.Unlock();
			AbortSubscription();
			statusDataLock.Lock();
			stateLock.Lock();
			fStatus->fProcessStatus.fState &= ~(kAliHLTPCChangingStateFlag|kAliHLTPCSubscribedStateFlag|kAliHLTPCConsumerErrorStateFlag);
			cmd = kAliHLTPCNOPCmd;
			}
		    break;
		case kAliHLTPCAcceptSubscriptionsCmd:
		    if ( fComponentType != kDataSource && fComponentType != kDataProcessor && fComponentType != kAsyncProcessor )
			break;
		    if ( (fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) &&
			 !(fStatus->fProcessStatus.fState & (kAliHLTPCChangingStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag)) &&
			 !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
			{
			fStatus->fProcessStatus.fState |= (kAliHLTPCChangingStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag);
			if ( fSubscriptionListenerThread )
			    fSubscriptionListenerThread->Start();
			else
			    {
			    fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
			    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
			    LOG( AliHLTLog::kError, "AliHLTControlledComponent::Run", "No subscription listener thread" )
				<< "no subscription listener thread available to start." << ENDLOG;
			    }
			cmd = kAliHLTPCNOPCmd;
			}
		    break;
		case kAliHLTPCStopSubscriptionsCmd:
		    if ( fComponentType != kDataSource && fComponentType != kDataProcessor && fComponentType != kAsyncProcessor )
			break;
		    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) 
			 || ((fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) && (fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag)) )
			{
			cmd = kAliHLTPCNOPCmd;
			break;
			}
		    if ( (fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) &&
			 !(fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) && 
			 !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
			{
			fStatus->fProcessStatus.fState |= kAliHLTPCChangingStateFlag;
			stateLock.Unlock();
			statusDataLock.Unlock();
			fSubscriptionListenerThread->Quit();
			statusDataLock.Lock();
			stateLock.Lock();
			//fStatus->fProcessStatus.fState &= ~(kAliHLTPCChangingStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag|kAliHLTPCProducerErrorStateFlag);
			fStatus->fProcessStatus.fState &= ~(kAliHLTPCProducerErrorStateFlag);
			cmd = kAliHLTPCNOPCmd;
			}
		    break;
		case kAliHLTPCCancelSubscriptionsCmd:
		    if ( fComponentType != kDataSource && fComponentType != kDataProcessor && fComponentType != kAsyncProcessor )
			break;
		    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) &&
			 !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
			{
			if ( fPublisher )
			    {
			    stateLock.Unlock();
			    statusDataLock.Unlock();
			    fPublisher->CancelAllSubscriptions( true );
			    statusDataLock.Lock();
			    stateLock.Lock();
			    }
			cmd = kAliHLTPCNOPCmd;
			}
		    break;
		case kAliHLTPCStartProcessingCmd:
		    {
		    bool ok = true;
		    switch ( fComponentType )
			{
			case kDataSource: // Fallthrough
			case kAsyncProcessor:
			    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) ||
				 (fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
				ok = false;
			    break;
			case kDataProcessor:
			    if ( !(fStatus->fProcessStatus.fState & (kAliHLTPCAcceptingSubscriptionsStateFlag|kAliHLTPCSubscribedStateFlag)) ||
				 (fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
				ok = false;
			    break;
			case kDataSink:
			    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) ||
				 (fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
				ok = false;
			    break;
			default:
			    ok = false;
			    break;
			}
		    if ( !ok )
			break;
		    fStatus->fProcessStatus.fState |= (kAliHLTPCChangingStateFlag|kAliHLTPCRunningStateFlag);
		    if ( fComponentType==kDataSource && fPublisher)
			{
			stateLock.Unlock();
			statusDataLock.Unlock();
			((AliHLTDetectorPublisher*)fPublisher)->StartAnnouncing();
			statusDataLock.Lock();
			stateLock.Lock();
			}
		    if ( fComponentType!=kDataSource && fSubscriber )
			{
			stateLock.Unlock();
			statusDataLock.Unlock();
			fSubscriber->StartProcessing();
			statusDataLock.Lock();
			stateLock.Lock();
			}
		    cmd = kAliHLTPCNOPCmd;
		    break;
		    }
		case kAliHLTPCPauseProcessingCmd:
		    // 		if ( fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag )
		    // 		    break;
		    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
			break;
		    if ( fComponentType==kDataSource )
			{
			stateLock.Unlock();
			statusDataLock.Unlock();
			((AliHLTDetectorPublisher*)fPublisher)->PauseProcessing();
			statusDataLock.Lock();
			stateLock.Lock();
			cmd = kAliHLTPCNOPCmd;
			}
		    if ( fComponentType==kDataProcessor || fComponentType==kDataSink || fComponentType==kAsyncProcessor )
			{
			stateLock.Unlock();
			statusDataLock.Unlock();
			fSubscriber->PauseProcessing();
			statusDataLock.Lock();
			stateLock.Lock();
			cmd = kAliHLTPCNOPCmd;
			}
		    break;
		case kAliHLTPCResumeProcessingCmd:
		    // 		if ( fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag )
		    // 		    break;
		    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
			break;
		    if ( fComponentType==kDataSource )
			{
			stateLock.Unlock();
			statusDataLock.Unlock();
			((AliHLTDetectorPublisher*)fPublisher)->ResumeProcessing();
			statusDataLock.Lock();
			stateLock.Lock();
			cmd = kAliHLTPCNOPCmd;
			}
		    if ( fComponentType==kDataProcessor || fComponentType==kDataSink || fComponentType==kAsyncProcessor )
			{
			stateLock.Unlock();
			statusDataLock.Unlock();
			fSubscriber->ResumeProcessing();
			statusDataLock.Lock();
			stateLock.Lock();
			cmd = kAliHLTPCNOPCmd;
			}
		    break;
		case kAliHLTPCStopProcessingCmd:
		    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
			{
			cmd = kAliHLTPCNOPCmd;
			break;
			}
		    fStatus->fProcessStatus.fState |= kAliHLTPCChangingStateFlag;		
		    if ( fComponentType==kDataSource )
			{
			stateLock.Unlock();
			statusDataLock.Unlock();
			((AliHLTDetectorPublisher*)fPublisher)->StopAnnouncing();
			statusDataLock.Lock();
			stateLock.Lock();
			cmd = kAliHLTPCNOPCmd;
			}
		    if ( fComponentType==kDataProcessor || fComponentType==kDataSink || fComponentType==kAsyncProcessor )
			{
			stateLock.Unlock();
			statusDataLock.Unlock();
			fSubscriber->StopProcessing();
			statusDataLock.Lock();
			stateLock.Lock();
			cmd = kAliHLTPCNOPCmd;
			}
		    break;
		case kAliHLTPCPublisherPinEventsCmd:
		    if ( fComponentType != kDataSource && fComponentType != kDataProcessor && fComponentType != kAsyncProcessor )
			break;
		    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
			break;
		    if ( fPublisher )
			{
			stateLock.Unlock();
			statusDataLock.Unlock();
			fPublisher->PinEvents( true );
			statusDataLock.Lock();
			stateLock.Lock();
			}
		    cmd = kAliHLTPCNOPCmd;
		    break;
		case kAliHLTPCPublisherUnpinEventsCmd:
		    if ( fComponentType != kDataSource && fComponentType != kDataProcessor && fComponentType != kAsyncProcessor )
			break;
		    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
			break;
		    if ( fPublisher )
			{
			stateLock.Unlock();
			statusDataLock.Unlock();
			fPublisher->PinEvents( false );
			statusDataLock.Lock();
			stateLock.Lock();
			}
		    cmd = kAliHLTPCNOPCmd;
		    break;
		case kAliHLTPCRemoveSubscriberCmd: // Fallthrough
		case kAliHLTPCCancelSubscriberCmd:
		    {
		    if ( fComponentType != kDataSource && fComponentType != kDataProcessor && fComponentType != kAsyncProcessor )
			break;
		    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) && 
			 !(fStatus->fProcessStatus.fState & kAliHLTPCWithSubscribersStateFlag) )
			break;
		    stateLock.Unlock();
		    statusDataLock.Unlock();
		    bool sendUnsubscribeMsg;
		    if ( cmd==kAliHLTPCCancelSubscriberCmd )
			sendUnsubscribeMsg = true;
		    else
			sendUnsubscribeMsg = false;
		    MLUCString tmp;
		    pthread_mutex_lock( &fSubscriberRemovalListMutex );
		    while ( fSubscriberRemovalList.size()>0 )
			{
			tmp = *(fSubscriberRemovalList.begin());
			fSubscriberRemovalList.erase( fSubscriberRemovalList.begin() );
			pthread_mutex_unlock( &fSubscriberRemovalListMutex );
			if ( fPublisher )
			    fPublisher->CancelSubscription( tmp.c_str(), sendUnsubscribeMsg );
			pthread_mutex_lock( &fSubscriberRemovalListMutex );
			}
		    pthread_mutex_unlock( &fSubscriberRemovalListMutex );
		    statusDataLock.Lock();
		    stateLock.Lock();
		    break;
		    }
		case kAliHLTPCDumpEventsCmd:
		    if ( fComponentType==kDataSource && fPublisher != NULL )
		        {
			stateLock.Unlock();
			statusDataLock.Unlock();
		        ((AliHLTDetectorPublisher*)fPublisher)->DumpEvents();
			statusDataLock.Lock();
			stateLock.Lock();
		        cmd = kAliHLTPCNOPCmd;
		        }
		    break;
		case kAliHLTPCSimulateErrorCmd:
		    if ( !(fStatus->fProcessStatus.fState & ~kAliHLTPCStartedStateFlag) )
			break;
		    LOG( AliHLTLog::kError, "AliHLTControlledComponent::Run", "SIMULATING ERROR" )
			<< "SIMULATING ERROR. Setting error flag." << ENDLOG;
		    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		    break;
		case kAliHLTPCPURGEALLEVENTSCmd:
		    stateLock.Unlock();
		    statusDataLock.Unlock();
		    PURGEALLEVENTS();
		    statusDataLock.Lock();
		    stateLock.Lock();
		    cmd = kAliHLTPCNOPCmd;
		    break;
		}
	    if ( cmd != kAliHLTPCNOPCmd )
		{
		AliUInt32_t state = fStatus->fProcessStatus.fState;
		stateLock.Unlock();
		statusDataLock.Unlock();
		IllegalCmd( state, cmd );
		statusDataLock.Lock();
		stateLock.Lock();
		}
	    fCmdSignal.Lock();
	    }
	}
    while ( !quit );
    fCmdSignal.Unlock();
    LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::Run", "Component Ended" )
	<< fComponentTypeName << " component " << fComponentName << " ended." << ENDLOG;
    PostUnconfigure();
    }


bool AliHLTControlledComponent::PreConfigure()
    {
    gLogLevel = AliHLTLog::kAll;
    if ( !SetupStdLogging() ) // Set this up here, so that we can see error output from command line parsing.
	return false; 
    if ( !ParseCmdLine() )
	return false;
    if ( !SetupStdLogging() ) // set it up again, as the configuration/cmd line might have disabled it.
	return false;
    if ( !SetupFileLogging() )
	return false;
    if ( !SetupSysLogging() )
	return false;
    if ( !SetupSysMESLogging() )
	return false;
    if ( !SetupDynLibLogging() )
        return false;
    if ( !SetupSC() )
	return false;
    return true;
    }

bool AliHLTControlledComponent::PostUnconfigure()
    {
    RemoveSC();
    RemoveLogging();
    return true;
    }

bool AliHLTControlledComponent::CheckConfiguration()
    {
    if ( !fPrivateSharedMemory && (fShmKey.fShmType == kInvalidShmType ||
				   fShmKey.fKey.fID == ~(AliUInt64_t)0 ||
				   fBufferSize == 0) )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::CheckConfiguration", "No shm specified" )
	    << "Must specify a shared memory buffer type, id, and size. (e.g. using the -shm command line option."
	    << ENDLOG;
	return false;
	}
    if ( !fPrivateSharedMemory && fMinBlockSize==0 && fPerEventFixedSize==0 && fPerBlockFixedSize==0 && fSizeFactor==0.0 )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::CheckConfiguration", "No min. block size specified" )
	    << "Must specify a (minimum) shm block size specification. (e.g. using the -minblocksize or -outputsizeestimate command line option."
	    << ENDLOG;
	return false;
	}
    if ( (fComponentType==kDataSource || fComponentType==kDataProcessor || fComponentType==kAsyncProcessor) &&
	 fOwnPublisherName=="" )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::CheckConfiguration", "No publisher name specified" )
	    << "Must specify a publisher name. (e.g. using the -publisher command line option.)"
	    << ENDLOG;
	return false;
	}

    if ( fComponentType==kDataSource && fAliceHLT && fDDLID==~(AliUInt32_t)0 )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::CheckConfiguration", "No DDL ID" )
	    << "In ALICE HLT mode a DDL ID must be specified. (e.g. using the -ddlid command line option.)"
	    << ENDLOG;
	return false;
	}
    if ( fComponentType==kDataSource && !fAliceHLT && fDDLID!=~(AliUInt32_t)0 )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::CheckConfiguration", "DDL ID Only in ALICE HLT mode" )
	    << "A DDL ID can only be specified in ALICE HLT mode a (set using the -alicehlt command line option.)"
	    << ENDLOG;
	return false;
	}
    if ( fComponentType==kDataSource && fAliceHLT && fDDLID!=~(AliUInt32_t)0 &&
	 ( fEventDataType.fID!=UNKNOWN_DATAID || fEventDataOrigin.fID!=UNKNOWN_DATAORIGIN ||
	   fEventDataSpecification!=~(AliUInt32_t)0 ) )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::CheckConfiguration", "No explicit data type/origin/spec" )
	    << "In ALICE HLT mode no explicit specification of data type/origin/specification is allowed."
	    << ENDLOG;
	return false;
	}
    if ( fComponentType==kDataSource && !fAliceHLT && (fHardwareCoProcessor || fHardwareCoProcessorOutputEventDataTypeSet) )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::CheckConfiguration", "Hardware co-processors only for ALICE HLT" )
	    << "Hardware co-processor support is currently only available in special ALICE HLT mode."
	    << ENDLOG;
	return false;
	}

    if ( fComponentType==kDataSource && !fHardwareCoProcessor && fHardwareCoProcessorOutputEventDataTypeSet )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::CheckConfiguration", "Hardware co-processors data type set without active hardware co-processor" )
	    << "Hardware co-processor data type is specified but hardware co-processor support is not activated (-hwcoproc option)."
	    << ENDLOG;
	return false;
	}

#ifdef USE_DDL_LIST_SHM
    if ( (fComponentType!=kDataSource || !fAliceHLT) && 
	 ( fDDLListShmKey.fShmType != kSysVShmPrivateType ||
	 fDDLListShmKey.fKey.fID != ~(AliUInt64_t)0 ) )
	{
	LOG( AliHLTLog::kWarning, "AliHLTControlledComponent::CheckConfiguration", "DDL list shm not used" )
	    << "A shared memory buffer for the auto-generated readout list is only used by data sources in special ALICE HLT mode.."
	    << ENDLOG;
	}
#endif


    if ( (fComponentType==kDataProcessor || fComponentType==kAsyncProcessor || fComponentType==kDataSink) &&
	 fAttachedPublisherName=="" )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::CheckConfiguration", "No publisher name to attach to specified" )
	    << "Must specify a publisher name to attach to. (e.g. using the -subscribe command line option."
	    << ENDLOG;
	return false;
	}

    if ( (fComponentType==kDataProcessor || fComponentType==kAsyncProcessor || fComponentType==kDataSink) &&
	 fSubscriberID=="" )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::CheckConfiguration", "No subscriber ID specified" )
	    << "Must specify a subscriber ID. (e.g. using the -subscribe command line option."
	    << ENDLOG;
	return false;
	}

    if ( !fAliceHLT && fEventTypeStackMachineCodes.size()>0 )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::CheckConfiguration", "Event Type Code only supported in ALICE HLT mode" )
	    << "Event type code is only supported in special ALICE HLT mode."
	    << ENDLOG;
	return false;
	}
    if ( fAliceHLT && !AliHLTReadoutList::IsVersionSupported(gReadoutListVersion) )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::CheckConfiguration", "Readout List Version not supported" )
	    << "Readout list version " << AliHLTLog::kDec << gReadoutListVersion
	    << (gReadoutListVersion==0 ? " (default)" : "")
	    << " is not supported. Please set a different version using the -readoutlistversion parameter."
	    << ENDLOG;
	return false;
	}

    if ( fAliceHLT && fComponentType==kDataSource )
	{
	TDetectorID detID;
	detID = AliHLTReadoutList::GetDetectorID( fDDLID );
	if ( detID >=kENDID )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledComponent::CheckConfiguration", "DDL ID Detector ID part unknown" )
		<< "Detector ID '" << AliHLTLog::kDec << detID << "' derived form DDL ID '" << fDDLID << "/0x" << AliHLTLog::kHex
		<< fDDLID << "' is not known." << ENDLOG;
	    return false;
	    }
	if ( AliHLTReadoutList::GetReadoutListDetectorDDLCount( gReadoutListVersion, detID )<AliHLTReadoutList::GetDetectorDDLNr( fDDLID ) )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledComponent::CheckConfiguration", "Invalid DDL ID" )
		<< "Invalid DDL ID '" << AliHLTLog::kDec << fDDLID << "/0x" << AliHLTLog::kHex << fDDLID
		<< "' not known (maybe too high for specified detector?)." << ENDLOG;
	    return false;
	    }
	}


    vector<LAMAddressData>::iterator lamIter, lamEnd;
    pthread_mutex_lock( &fLAMAddressMutex );
    lamIter = fLAMAddresses.begin();
    lamEnd = fLAMAddresses.end();
    bool isBlob = false;
    bool fail = false;
    while ( lamIter != lamEnd )
	{
	if ( BCLCheckURL( lamIter->fStr.c_str(), false, isBlob ) )
	    {
	    fail = true;
	    break;
	    }
	if ( isBlob )
	    {
	    break;
	    }
	lamIter++;
	}
    if ( fail )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::CheckConfiguration", "Error decoding LAM address" )
	    << "Error decoding LAM address '" << lamIter->fStr.c_str() << "'." << ENDLOG;
	}
    if ( isBlob )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::CheckConfiguration", "LAM address must be msg address" )
	    << "Specified LAM address '" << lamIter->fStr.c_str() << "' is a blob address instead of a message address." << ENDLOG;
	}
    pthread_mutex_unlock( &fLAMAddressMutex );
    if ( fail || isBlob )
	return false;
    return true;
    }

void AliHLTControlledComponent::ShowConfiguration()
    {
    LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "Run number & type" )
	<< "Run number: " << AliHLTLog::kDec << fRunNumber << " (0x" << AliHLTLog::kHex
	<< fRunNumber 
	<< ") - Beam type: 0x" << fBeamType << " - HLT mode: 0x" << fHLTMode
	<< " - Run type: " << fRunType.c_str() << "." << ENDLOG;
	//<< ") - Run type: 0x" << fRunType << "." << ENDLOG;
    LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "SC config" )
	<< "SC interface address: " << fSCInterfaceAddress.c_str() << ENDLOG;
    LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "Log config" )
	<< (fStdoutLogging ? "Stdout logging, " : "") << (fFileLogging ? "File logging, " : "")
	<< (fSysLogging ? "Syslog logging, " : "") << "logname: " << fLogName.c_str()
	<< (fSysMESLogging ? ", SysMES logging" : "")
	<< (fDynLibLogging ? ", Dynamic library logging to " : "")
	<< (fDynLibLogging ? fDynLibPath.c_str() : "")
	<< "." << ENDLOG;
    const char* shmType;
    switch ( fShmKey.fShmType )
	{
	case kBigPhysShmType:
	    shmType = "bigphys";
	    break;
	case kPhysMemShmType:
	    shmType = "physmem";
	    break;
	case kSysVShmType:
	    shmType = "sysv";
	    break;
	case kSysVShmPrivateType:
	    shmType = "sysv-private";
	    break;
	default:
	    shmType = "invalid";
	    break;
	}
    const char* bufferManType;
    switch ( fBufferManagerType )
	{
	case kLargestBlockBufferManager:
	    bufferManType = "LargestBlock";
	    break;
	case kRingBufferManager:
	    bufferManType = "Ring";
	    break;
	default:
	    bufferManType = "Invalid";
	    break;
	}
    if ( !fPrivateSharedMemory )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "Shm config" )
	    << "Shm type: " << shmType << " - shm key: 0x" << AliHLTLog::kHex << fShmKey.fKey.fID
	    << "/" << AliHLTLog::kDec << fShmKey.fKey.fID << " - shm buffer size: 0x" << AliHLTLog::kHex
	    << fBufferSize << "/" << AliHLTLog::kDec << fBufferSize << " - minimum block size: 0x"
	    << AliHLTLog::kHex << fMinBlockSize << "/" << AliHLTLog::kDec << fMinBlockSize 
	    << " - Per event fixed size 0x" << AliHLTLog::kHex << fPerEventFixedSize
	    << " ("  << AliHLTLog::kDec << fPerEventFixedSize
	    << " - Per block fixed size 0x" << AliHLTLog::kHex << fPerBlockFixedSize
	    << " ("  << AliHLTLog::kDec << fPerBlockFixedSize
	    << " - Size factor "  << AliHLTLog::kDec << fSizeFactor
	    << ") - maximum no use count: " << fMaxNoUseCount
	    << " - Buffer manager: " << bufferManType
	    << "." << ENDLOG;
	}

    if ( fComponentType==kDataSource || fComponentType==kDataProcessor || fComponentType==kAsyncProcessor )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "Own Publisher config" )
	    << "Own publisher name: " << fOwnPublisherName.c_str() << "." << ENDLOG;
	}

    if ( fComponentType==kDataProcessor || fComponentType==kAsyncProcessor || fComponentType==kDataSink )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "Attached Publisher(Proxy) & Subscriber config" )
	    << "Attached publisher name: " << fAttachedPublisherName.c_str() << "  - subscriber ID: "
	    << fSubscriberID.c_str() << "." << ENDLOG;
	}


    vector<LAMAddressData>::iterator lamIter, lamEnd;
    pthread_mutex_lock( &fLAMAddressMutex );
    lamIter = fLAMAddresses.begin();
    lamEnd = fLAMAddresses.end();
    while ( lamIter != lamEnd )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "LAM address config" )
	    << "Specified LAM address: '" << lamIter->fStr.c_str() << "'." << ENDLOG;
	lamIter++;
	}
    pthread_mutex_unlock( &fLAMAddressMutex );
    

    if ( fPersistentStateShmID!=-1 )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "Persistent shm state config" )
	    << "Persistent state shm ID: " << AliHLTLog::kDec << fPersistentStateShmID << " (0x"
	    << AliHLTLog::kHex << fPersistentStateShmID << ")." << ENDLOG;
	}
    LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "Publish old events config" )
	<< "Request to publish old events on subscription: " << (fSubscriptionRequestOldEvents ? "Yes" : "No") << ENDLOG;
    LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "Event Modulo" )
	<< "Event modulo: " << (fUseSubscriptionEventModulo ? "Yes" : "No") << " - Counter: "
	<< AliHLTLog::kDec << fSubscriptionEventModulo << ENDLOG;
    LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "Transient/Persistent" )
	<< "Subscriber is " << (fTransientSubscriber ? "Transient" : "Persistent") << "." << ENDLOG;
#ifdef ALLOW_ARTIFICIAL_EVENT_LOSS
    if ( fEventLossModulo )
	{
	LOG( AliHLTLog::kWarning, "AliHLTControlledComponent::ShowConfiguration", "Artificial Event Loss Activated" )
	    << "Artificial event loss modulo activated (" << AliHLTLog::kDec << fEventLossModulo << ")." << ENDLOG;
	}
#endif
    if ( fAliceHLT )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "ALICE HLT" )
	    << "Special ALICE HLT options activated." << ENDLOG;
	if ( fComponentType==kDataSource )
	    {
	    LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "ALICE DDL ID" )
		<< "ALICE DDL ID: " << AliHLTLog::kDec << fDDLID << " (0x" << AliHLTLog::kHex << fDDLID << ")." << ENDLOG;
#ifdef USE_DDL_LIST_SHM
	    switch ( fDDLListShmKey.fShmType )
		{
		case kBigPhysShmType:
		    shmType = "bigphys";
		    break;
		case kPhysMemShmType:
		    shmType = "physmem";
		    break;
		case kSysVShmType:
		    shmType = "sysv";
		    break;
		case kSysVShmPrivateType:
		    shmType = "sysv-private";
		    break;
		default:
		    shmType = "invalid";
		    break;
		}
	    LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "DDL list shm config" )
		<< "DDL list shm type: " << shmType << " - shm key: 0x" << AliHLTLog::kHex << fDDLListShmKey.fKey.fID
		<< "/" << AliHLTLog::kDec << fDDLListShmKey.fKey.fID << "." << ENDLOG;
#endif
	    }
	}

    if ( fMaxPendingEventCount )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "Maximum Pending Event Count" )
	    << "Maximum number of allowed pending events: " << AliHLTLog::kDec << fMaxPendingEventCount
	    << " (0x" << AliHLTLog::kHex << fMaxPendingEventCount << ")." << ENDLOG;
	}

    if ( fEventAccounting )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "Event Accounting is active" )
	    << "Full event accounting is active." << ENDLOG;
	}
    if ( fSecondary )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "Secondary Process" )
	    << "Process is secondary." << ENDLOG;
	}

    if ( fForceEventDoneDataForward )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "EventDoneData Forward Forced" )
	    << "Event Done Data Forwarding is forced." << ENDLOG;
	}
    
    }


void AliHLTControlledComponent::Configure()
    {
    ShowConfiguration();
    if ( !CheckConfiguration() )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::Configure", "Configuration check failed" )
	    << "Check of configuration failed." << ENDLOG;
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	return;
	}

    SetupStdLogging(); // set it up again, as configuration changes might have disabled it.
    SetupFileLogging();
    SetupSysLogging();
    SetupSysMESLogging();
    SetupDynLibLogging();
    
    if ( fMaxPreEventCount == 1 && fMaxPreEventCountExp2 == 0 )
	{
	if ( fMinBlockSize > 0 && fBufferSize/fMinBlockSize>0 )
	    fMaxPreEventCount = fBufferSize/fMinBlockSize+4;
	else
	    fMaxPreEventCount = 128;
	unsigned bit = 1;
	int bitPos = sizeof(fMaxPreEventCount)*8-1;
	bit <<= bitPos;
	while ( ! (bit & fMaxPreEventCount) )
	    {
	    bit >>= 1;
	    bitPos--;
	    }
	if ( bitPos < (int)(sizeof(fMaxPreEventCount)*8-1) )
	    bitPos++;
	fMaxPreEventCountExp2 = bitPos;
	}

    if ( !fPrivateSharedMemory )
	{
	if ( !SetupShmBufferMemory() )
	    {
	    const char* shmType;
	    switch ( fShmKey.fShmType )
		{
		case kBigPhysShmType:
		    shmType = "bigphys";
		    break;
		case kPhysMemShmType:
		    shmType = "physmem";
		    break;
		case kSysVShmType:
		    shmType = "sysv";
		    break;
		default:
		    shmType = "invalid";
		    break;
		}
	    LOG( AliHLTLog::kError, "AliHLTControlledComponent::Configure", "Unable to Setup shm buffer" )
		<< "Unable to setup shared memory buffer of size 0x" << AliHLTLog::kHex << fBufferSize
		<< "/" << AliHLTLog::kDec << fBufferSize << " with " << shmType << " shm key 0x" << AliHLTLog::kHex
		<< fShmKey.fKey.fID << "/" << AliHLTLog::kDec << fShmKey.fKey.fID << "." << ENDLOG;
// XXXX TODO LOCKING XXXX
	    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return;
	    }
	}

    if ( fAliceHLT && fComponentType==kDataSource )
	{
	TDetectorID detID;
	detID = AliHLTReadoutList::GetDetectorID( fDDLID );
	const char* shortName = AliHLTReadoutList::GetDetectorShortName( detID );
	AliUInt32_t tmpDO = 0;
	unsigned tmpN;
	for( tmpN = 0; tmpN < strlen( shortName); tmpN++ )
	    {
	    tmpDO = (tmpDO << 8) | (AliUInt32_t)(shortName[tmpN]);
	    }
	for ( ; tmpN < 4; tmpN++ )
	    {
	    tmpDO = (tmpDO << 8) | (AliUInt32_t)(' ');
	    }
	fEventDataOrigin.fID = tmpDO;
	fEventDataType.fID = DDLRAWDATA_DATAID;
	if ( fHardwareCoProcessor )
	    {
	    if ( fHardwareCoProcessorOutputEventDataTypeSet )
		fEventDataType.fID = fHardwareCoProcessorOutputEventDataType.fID;
	    else
		fEventDataType.fID = DDLPREPROCDATA_DATAID;
	    }
	if ( detID==kTPC )
	    {
	    //fEventDataType.fID = DDLRAWDATAPACKED_DATAID;
	    fEventDataSpecification = 0;
	    unsigned slice, patch;
	    slice = AliHLTReadoutList::GetTPCSlice( fDDLID );
	    patch = AliHLTReadoutList::GetTPCPatch( fDDLID );
	    fEventDataSpecification |= (slice&0xFF)<<24;
	    fEventDataSpecification |= (slice&0xFF)<<16;
	    fEventDataSpecification |= (patch&0xFF)<<8;
	    fEventDataSpecification |= (patch&0xFF);
	    }
	else if ( detID==kMUON_TRK || detID==kMUON_TRG )
	    {
	    const char* shortName = "MUON";
	    tmpDO = 0;
	    for( tmpN = 0; tmpN < strlen( shortName); tmpN++ )
		{
		tmpDO = (tmpDO << 8) | (AliUInt32_t)(shortName[tmpN]);
		}
	    for ( ; tmpN < 4; tmpN++ )
		{
		tmpDO = (tmpDO << 8) | (AliUInt32_t)(' ');
		}
	    fEventDataOrigin.fID = tmpDO;
	    unsigned shift = AliHLTReadoutList::GetDetectorDDLNr( fDDLID );
	    if ( detID==kMUON_TRG )
		shift += AliHLTReadoutList::GetReadoutListDetectorDDLCount( gReadoutListVersion, kMUON_TRK );
	    fEventDataSpecification = 0;
	    fEventDataSpecification |= 1 << shift;
	    }
	else if ( detID==kEMCAL )
	    {
	    //Change EMCAL Specification to DDLNum instead of 1 << DDLNum for Run 2, as EMCAL has more than 32 links now
	    fEventDataSpecification = AliHLTReadoutList::GetDetectorDDLNr( fDDLID );
	    }
	else
	    {
	    //fEventDataType.fID = DDLRAWDATAPACKED_DATAID;
	    fEventDataSpecification = 0;
	    fEventDataSpecification |= 1 << AliHLTReadoutList::GetDetectorDDLNr( fDDLID );
	    }

#ifdef USE_DDL_LIST_SHM
	if ( !SetupDDLListSharedMemory() )
	    {
	    char* shmType;
	    switch ( fDDLListShmKey.fShmType )
		{
		case kBigPhysShmType:
		    shmType = "bigphys";
		    break;
		case kPhysMemShmType:
		    shmType = "physmem";
		    break;
		case kSysVShmType:
		    shmType = "sysv";
		    break;
		case kSysVShmPrivateType:
		    shmType = "sysv-private";
		    break;
		default:
		    shmType = "invalid";
		    break;
		}
	    LOG( AliHLTLog::kError, "AliHLTControlledComponent::Configure", "Unable to Setup DDL list shm buffer" )
		<< "Unable to setup DDL list shared memory buffer of size 0x" << AliHLTLog::kHex << AliHLTReadoutList::GetReadoutListSize()
		<< "/" << AliHLTLog::kDec << AliHLTReadoutList::GetReadoutListSize() << " with " << shmType << " shm key 0x" << AliHLTLog::kHex
		<< fDDLListShmKey.fKey.fID << "/" << AliHLTLog::kDec << fDDLListShmKey.fKey.fID << "." << ENDLOG;
// XXXX TODO LOCKING XXXX
	    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return;
	    }
	fDDLListBlock.fShmID = fDDLListShmKey;
	fDDLListBlock.fBlockSize = AliHLTReadoutList::GetReadoutListSize();
	fDDLListBlock.fBlockOffset = 0;
	fDDLListBlock.fProducerNode = AliHLTGetNodeID();
	fDDLListBlock.fDataType.fID = DDLLIST_DATAID;
	fDDLListBlock.fDataOrigin.fID = HLT_DATAORIGIN;
	fDDLListBlock.fDataSpecification = 0;
	fDDLListBlock.fStatusFlags = ALIHLTSUBEVENTDATADESCRIPTOR_PASSTHROUGH;
	AliHLTSubEventDescriptor::FillBlockAttributes( fDDLListBlock.fAttributes );
#else
	fDDLListData = reinterpret_cast<hltreadout_uint32_t*>( new AliUInt8_t[AliHLTReadoutList::GetReadoutListSize( gReadoutListVersion )] );
	if ( !fDDLListData )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledComponent::Configure", "Out of memory" )
		<< "Unable to allocate DDL list memory buffer of size 0x" << AliHLTLog::kHex << AliHLTReadoutList::GetReadoutListSize( gReadoutListVersion )
		<< "/" << AliHLTLog::kDec << AliHLTReadoutList::GetReadoutListSize( gReadoutListVersion ) << "." << ENDLOG;
// XXXX TODO LOCKING XXXX
	    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return;
	    }
#endif
	AliHLTReadoutList readoutList( gReadoutListVersion, fDDLListData );
	readoutList.Reset();
	readoutList.SetDDL( fDDLID );
	}



// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) && !CreateParts() )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::Configure", "Creation of parts failed" )
	    << "Creation of parts failed." << ENDLOG;
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	return;
	}
    if ( fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag )
	return;
    stateLock.Unlock();
    statusDataLock.Unlock();
    if ( !SetupComponents() )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::Configure", "Setup of components failed" )
	    << "Setup of components failed." << ENDLOG;
	stateLock.Lock();
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	return;
	}
    statusDataLock.Lock();
    stateLock.Lock();
    fStatus->fProcessStatus.fState |= kAliHLTPCConfiguredStateFlag;
    }

void AliHLTControlledComponent::Unconfigure()
    {
    DestroyParts();
    if ( !fPrivateSharedMemory )
	RemoveShmBufferMemory();
#ifndef USE_DDL_LIST_SHM
    if ( fDDLListData )
	{
	delete [] reinterpret_cast<AliUInt8_t*>( fDDLListData );
	fDDLListData = NULL;
	}
#endif
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
    gLog.ResetRunNumber();
#endif
    if ( fStatus )
	{
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
	}
    }

bool AliHLTControlledComponent::CreateParts()
    {
    if ( fComponentType==kDataProcessor || fComponentType==kAsyncProcessor || fComponentType==kDataSink )
	{
	fInDescriptorHandler = CreateInDescriptorHandler();
	if ( !fInDescriptorHandler )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledComponent::Configure", "Unable to Create Incoming Descriptor Handler" )
		<< "Unable to create incoming event descriptor handler." << ENDLOG;
// XXXX TODO LOCKING XXXX
	    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return false;
	    }
	}

    fOutDescriptorHandler = CreateOutDescriptorHandler();
    if ( !fOutDescriptorHandler )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::Configure", "Unable to Create Outgoing Descriptor Handler" )
	    << "Unable to create outgoing event descriptor handler." << ENDLOG;
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	return false;
	}

    switch ( fComponentType )
	{
	case kDataSource:
	    fPublisher = CreatePublisher();
	    if ( !fPublisher )
		{
		LOG( AliHLTLog::kError, "AliHLTControlledComponent::Configure", "Unable to Create Publisher Object" )
		    << "Unable to create publisher object." << ENDLOG;
// XXXX TODO LOCKING XXXX
		MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		return false;
		}
	    break;
	case kDataProcessor: // Fallthrough
	case kAsyncProcessor:
	    fPublisher = CreateRePublisher();
	    if ( !fPublisher )
		{
		LOG( AliHLTLog::kError, "AliHLTControlledComponent::Configure", "Unable to Create RePublisher Object" )
		    << "Unable to create republisher object." << ENDLOG;
// XXXX TODO LOCKING XXXX
		MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		return false;
		}
	    // Fallthrough
	case kDataSink:
	    fPublisherProxy = CreatePublisherProxy();
	    if ( !fPublisherProxy )
		{
		LOG( AliHLTLog::kError, "AliHLTControlledComponent::Configure", "Unable to Create Publisher Proxy Object" )
		    << "Unable to create publisher proxy object." << ENDLOG;
// XXXX TODO LOCKING XXXX
		MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		return false;
		}
	    fSubscriber = CreateSubscriber();
	    if ( !fSubscriber )
		{
		LOG( AliHLTLog::kError, "AliHLTControlledComponent::Configure", "Unable to Create Subscriber Object" )
		    << "Unable to create subscriber object." << ENDLOG;
// XXXX TODO LOCKING XXXX
		MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		return false;
		}
	    break;
	default:
	    fComponentTypeName = "Unknown";
	    break;
	}

    if ( fComponentType==kDataSource || fComponentType==kDataProcessor || fComponentType==kAsyncProcessor )
	{
	fSubscriptionListenerThread = CreateSubscriptionThread();
	if ( !fSubscriptionListenerThread )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledComponent::Configure", "Unable to Create Subscription Thread Object" )
		    << "Unable to create subscription thread object." << ENDLOG;
// XXXX TODO LOCKING XXXX
	    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return false;
	    }
	if ( fAliceHLT )
	    fTriggerStackMachine = new AliHLTTriggerStackMachine;
	}
    if ( fComponentType==kDataProcessor || fComponentType==kAsyncProcessor || fComponentType==kDataSink )
	{
	fSubscriptionThread = new SubscriptionThread( this, fPublisherProxy, fSubscriber );
	if ( !fSubscriptionThread )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledComponent::Configure", "Unable to Create Subscription Thread Object" )
		    << "Unable to create subscription thread object." << ENDLOG;
// XXXX TODO LOCKING XXXX
	    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return false;
	    }
	}

    vector<LAMAddressData>::iterator lamIter, lamEnd;
    int ret;
    pthread_mutex_lock( &fLAMAddressMutex );
    lamIter = fLAMAddresses.begin();
    lamEnd = fLAMAddresses.end();
    while ( lamIter != lamEnd )
	{
	ret = BCLDecodeRemoteURL( lamIter->fStr.c_str(), lamIter->fAddress );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledComponent::CreateParts", "Error decoding LAM address" )
		<< "Error decoding LAM address '" << lamIter->fStr.c_str() << "'." << ENDLOG;
// XXXX TODO LOCKING XXXX
	    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    break;
	    }
	lamIter++;
	}
    pthread_mutex_unlock( &fLAMAddressMutex );

    if ( fPersistentStateShmID!=-1 )
	{
	switch ( fComponentType )
	    {
	    case kDataSource:
		fPersistentState = new AliHLTPersistentState<AliHLTDetectorPublisher::State>( fPersistentStateShmID );
		if ( !fPersistentState || !(*fPersistentState) )
		    {
		    LOG( AliHLTLog::kError, "AliHLTControlledComponent::CreateParts", "Creation of persistent state shm failed" )
			<< "Creation of persistent state shm with ID " << AliHLTLog::kDec << fPersistentStateShmID
			<< " failed." << ENDLOG;
// XXXX TODO LOCKING XXXX
		    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		    return false;
		    }
		break;
	    case kDataProcessor:
		break;
	    case kDataSink:
		break;
	    default:
		break;
	    }
	}
    return true;
    }

void AliHLTControlledComponent::DestroyParts()
    {
    if ( fPublisher )
	fPublisher->CancelAllSubscriptions( true, true );

    vector<LAMAddressData>::iterator lamIter, lamEnd;
    pthread_mutex_lock( &fLAMAddressMutex );
    lamIter = fLAMAddresses.begin();
    lamEnd = fLAMAddresses.end();
    while ( lamIter != lamEnd )
	{
	if ( lamIter->fAddress )
	    BCLFreeAddress( lamIter->fAddress );
	lamIter++;
	}
    pthread_mutex_unlock( &fLAMAddressMutex );


    if ( fSubscriptionThread )
	{
	delete fSubscriptionThread;
	fSubscriptionThread = NULL;
	}
    if ( fSubscriptionListenerThread )
	{
	delete fSubscriptionListenerThread;
	fSubscriptionListenerThread = NULL;
	}

    if ( fPublisher )
	{
	delete fPublisher;
	fPublisher = NULL;
	}
    if ( fPublisherProxy )
	{
	delete fPublisherProxy;
	fPublisherProxy = NULL;
	}

    if ( fSubscriber )
	{
	delete fSubscriber;
	fSubscriber = NULL;
	}
    if ( fOutDescriptorHandler )
	{
	delete fOutDescriptorHandler;
	fOutDescriptorHandler = NULL;
	}

    if ( fInDescriptorHandler )
	{
	delete fInDescriptorHandler;
	fInDescriptorHandler = NULL;
	}


    if ( fPersistentState )
	{
	delete fPersistentState;
	fPersistentState = NULL;
	}

    if ( fEventAccounting )
	{
	delete fEventAccounting;
	fEventAccounting = NULL;
	}

    if ( fTriggerStackMachine )
	{
	delete fTriggerStackMachine;
	fTriggerStackMachine = NULL;
	}


    return;
    }


bool AliHLTControlledComponent::SetupStdLogging()
    {
    gLog.SetID( fComponentName );
    BCLAddressLogger::Init();
    if ( fStdoutLogging )
	{
	if ( !fLogOut )
	    {
	    fLogOut = new AliHLTFilteredStdoutLogServer;
	    if ( !fLogOut )
		return false;
	    gLog.AddServer( fLogOut );
	    }
	}
    else
	{
	if ( fLogOut )
	    {
	    gLog.DelServer( fLogOut );
	    delete fLogOut;
	    fLogOut = NULL;
	    }
	}
    gLog.SetStackTraceComparisonDepth( 1 );
    return true;
    }

bool AliHLTControlledComponent::SetupFileLogging()
    {
    if ( fFileLogging )
	{
	if ( !fFileLog )
	    {
	    fFileLog = new AliHLTFileLogServer( fLogName.c_str(), ".log", 2500000 );
	    if ( !fFileLog )
		return false;
	    gLog.AddServer( fFileLog );
	    }
	}
    else
	{
	if ( fFileLog )
	    {
	    gLog.DelServer( fFileLog );
	    delete fFileLog;
	    fFileLog = NULL;
	    }
	}
    return true;
    }

bool AliHLTControlledComponent::SetupSysLogging()
    {
    if ( fSysLogging )
	{
	if ( !fSysLog )
	    {
	    fSysLog = new AliHLTSysLogServer( fLogName.c_str(), fSysLogFacility );
	    if ( !fSysLog )
		return false;
	    gLog.AddServer( fSysLog );
	    }
	}
    else
	{
	if ( fSysLog )
	    {
	    gLog.DelServer( fSysLog );
	    delete fSysLog;
	    fSysLog = NULL;
	    }
	}
    return true;
    }

bool AliHLTControlledComponent::SetupSysMESLogging()
    {
    if ( fSysMESLogging )
	{
	if ( !fSysMESLog )
	    {
	    fSysMESLog = new MLUCSysMESLogServer;
	    if ( !fSysMESLog )
		return false;
	    gLog.AddServer( fSysMESLog );
	    }
	}
    else
	{
	if ( fSysMESLog )
	    {
	    gLog.DelServer( fSysMESLog );
	    delete fSysMESLog;
	    fSysMESLog = NULL;
	    }
	}
    return true;
    }

bool AliHLTControlledComponent::SetupDynLibLogging()
    {
    if ( fDynLibLogging )
	{
        if ( !fDynLibLog )
	    {
            fDynLibLog = new MLUCDynamicLibraryLogServer(fDynLibPath.c_str(), fComponentName);
            if ( !fDynLibLog )
                return false;
            gLog.AddServer( fDynLibLog );
	    }
	}
    else
	{
        if ( fDynLibLog )
	    {
            gLog.DelServer( fDynLibLog );
            delete fDynLibLog;
            fDynLibLog = NULL;
	    }
	}
    return true;
    }

void AliHLTControlledComponent::RemoveLogging()
    {
    if ( fDynLibLog )
	{
        gLog.DelServer( fDynLibLog );
        delete fDynLibLog;
        fDynLibLog = NULL;
	}
    if ( fSysMESLog )
	{
	gLog.DelServer( fSysMESLog );
	delete fSysMESLog;
	fSysMESLog = NULL;
	}
    if ( fSysLog )
	{
	gLog.DelServer( fSysLog );
	delete fSysLog;
	fSysLog = NULL;
	}
    if ( fFileLog )
	{
	gLog.DelServer( fFileLog );
	delete fFileLog;
	fFileLog = NULL;
	}
    if ( fLogOut )
	{
	gLog.DelServer( fLogOut );
	delete fLogOut;
	fLogOut = NULL;
	}
    }

bool AliHLTControlledComponent::ParseCmdLine()
    {
    int i, errorArg=-1;
    const char* errorStr = NULL;
    
    i = 1;
    while ( (i < fArgc) && !errorStr )
	{
	errorStr = ParseCmdLine( fArgc, fArgv, i, errorArg );
	}
    if ( errorStr )
	{
	PrintUsage( errorStr, i, errorArg );
	return false;
	}
    return true;
    }
	
const char* AliHLTControlledComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cpErr;
    if ( !strcmp( argv[i], "-name" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing component name parameter";
	    }
	fComponentName = argv[i+1];
	fLogName = fComponentName;
	i += 2;
	return  NULL;
	}
    if ( !strcmp( argv[i], "-control" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing Status&Control interface address URL parameter";
	    }
	fSCInterfaceAddress = argv[i+1];
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-shm" ) )
	{
	if ( argc <= i+3 )
	    {
	    return "Missing shmkey or shmsize parameter";
	    }
	if ( !strcmp( argv[i+1], "bigphys" ) )
	    fShmKey.fShmType = kBigPhysShmType;
	else if ( !strcmp( argv[i+1], "physmem" ) )
	    fShmKey.fShmType = kPhysMemShmType;
	else if ( !strcmp( argv[i+1], "sysv" ) )
	    fShmKey.fShmType = kSysVShmType;
	else if ( !strcmp( argv[i+1], "librorc" ) )
	    fShmKey.fShmType = kLibrorcShmType;
	else
	    {
	    errorArg = i+1;
	    return "Invalid shm type specifier...";
	    }
	fShmKey.fKey.fID = strtoul( argv[i+2], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+2;
	    return "Error converting shmkey specifier..";
	    }
	fBufferSize = strtoul( argv[i+3], &cpErr, 0 );
	if ( *cpErr == 'k')
	    fBufferSize *= 1024;
	else if ( *cpErr == 'M')
	    fBufferSize *= 1024*1024;
	else if ( *cpErr == 'G')
	    fBufferSize *= 1024*1024*1024;
	else if ( *cpErr )
	    {
	    errorArg = i+3;
	    return "Error converting shmsize specifier..";
	    }
	i += 4;
	return NULL;
	}
    if ( !strcmp( argv[i], "-minblocksize" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing minimum block size specifier.";
	    }
	fMinBlockSize = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr == 'k')
	    fMinBlockSize *= 1024;
	else if ( *cpErr == 'M')
	    fMinBlockSize *= 1024*1024;
	else if ( *cpErr == 'G')
	    fMinBlockSize *= 1024*1024*1024;
	else if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting minimum block size specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-buffermanager" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing buffer manager type specifier.";
	    }
	if ( !strcmp(argv[i+1], "ring" ) )
	    fBufferManagerType = kRingBufferManager;
	else if ( !strcmp(argv[i+1], "largestblock" ) )
	    fBufferManagerType = kLargestBlockBufferManager;
	else
	    {
	    errorArg = i+1;
	    return "Unknown buffer manager type";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-outputsizeestimate" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing per event fixed size, block fixed size & size factor specifier.";
	    }
	if ( argc <= i+2 )
	    {
	    return "Missing block fixed size & size factor specifier.";
	    }
	if ( argc <= i+3 )
	    {
	    return "Missing size factor specifier.";
	    }
	fPerEventFixedSize = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr == 'k')
	    fPerEventFixedSize *= 1024;
	else if ( *cpErr == 'M')
	    fPerEventFixedSize *= 1024*1024;
	else if ( *cpErr == 'G')
	    fPerEventFixedSize *= 1024*1024*1024;
	else if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting per event fixed size specifier.";
	    }
	fPerBlockFixedSize = strtoul( argv[i+2], &cpErr, 0 );
	if ( *cpErr == 'k')
	    fPerBlockFixedSize *= 1024;
	else if ( *cpErr == 'M')
	    fPerBlockFixedSize *= 1024*1024;
	else if ( *cpErr == 'G')
	    fPerBlockFixedSize *= 1024*1024*1024;
	else if ( *cpErr )
	    {
	    errorArg = i+2;
	    return "Error converting per block fixed size specifier.";
	    }
	fSizeFactor = strtod( argv[i+3], &cpErr );
	if ( *cpErr == 'k')
	    fSizeFactor *= 1024.0;
	else if ( *cpErr == 'M')
	    fSizeFactor *= 1024.0*1024.0;
	else if ( *cpErr == 'G')
	    fSizeFactor *= 1024.0*1024.0*1024.0;
	else if ( *cpErr )
	    {
	    errorArg = i+3;
	    return "Error converting size factor specifier.";
	    }
	
	i += 3;
	return NULL;
	}
    if ( !strcmp( argv[i], "-senddone" ) )
	{
	fSendEventDone = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-subscribeoldevents" ) )
	{
	fSubscriptionRequestOldEvents = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-nosubscribeoldevents" ) )
	{
	fSubscriptionRequestOldEvents = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-V" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing verbosity level specifier.";
	    }
	gLogLevel = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting verbosity level specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-benchmark" ) )
	{
	fBenchmark = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-nostdout" ) )
	{
	fStdoutLogging = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-nofilelog" ) )
	{
	fFileLogging = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-syslog" ) )
	{
	if ( argc <= i+2 )
	    {
	    return "Missing syslog facility parameter";
	    }
	if ( !strcmp( argv[i+1], "DAEMON" ) )
	    {
	    fSysLogFacility = LOG_DAEMON;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL0" ) )
	    {
	    fSysLogFacility = LOG_LOCAL0;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL1" ) )
	    {
	    fSysLogFacility = LOG_LOCAL1;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL2" ) )
	    {
	    fSysLogFacility = LOG_LOCAL2;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL3" ) )
	    {
	    fSysLogFacility = LOG_LOCAL3;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL4" ) )
	    {
	    fSysLogFacility = LOG_LOCAL4;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL5" ) )
	    {
	    fSysLogFacility = LOG_LOCAL5;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL6" ) )
	    {
	    fSysLogFacility = LOG_LOCAL6;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL7" ) )
	    {
	    fSysLogFacility = LOG_LOCAL7;
	    }
	else if ( !strcmp( argv[i+1], "USER" ) )
	    {
	    fSysLogFacility = LOG_USER;
	    }
	else
	    {
	    return "Unknown syslog facility parameter.";
	    }
	fSysLogging = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-sysmeslog" ) )
	{
	fSysMESLogging = true;
	i += 1;
	return NULL;
	}
    if ( !strcmp( argv[i], "-infologger" ) )
	{
        if ( argc <= i+1 )
	    {
            return "Missing path to dynamic log library.";
	    }
        fDynLibPath = argv[i+1];
        fDynLibLogging = true;
        i += 2;
        return NULL;
	}
    if ( !strcmp( argv[i], "-publishertimeout" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing publisher timeout specifier.";
	    }
	fPublisherTimeout = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting publisher timeout specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-publisher" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing publisher name specifier.";
	    }
	fOwnPublisherName = argv[i+1];
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventmodulopredivisor" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing event modulo pre-divisor specifier.";
		}
	fEventModuloPreDivisor = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting event modulo pre-divisor specifier.";
	    }
	if ( !fEventModuloPreDivisor )
	    {
	    errorArg = i+1;
	    return "Event modulo pre-divisor specifier must be non-zero.";
	    }
	i += 2;
	return NULL;
	}

    if ( !strcmp( argv[i], "-subscribe" ) )
	{
	if ( argc <= i+2 )
	    {
	    return "Missing publishername or subscriberid parameter";
	    }
	fAttachedPublisherName = argv[i+1];
	fSubscriberID = argv[i+2];
	i += 3;
	return NULL;
	}
    if ( !strcmp( argv[i], "-shmproxy" ) )
	{
	if ( argc <= i+3 )
	    {
	    return "Missing publisher-to-subscriber or subscriber-to-publisher proxy-shmkey or proxy-shmsize parameter";
	    }
	fProxyShmSize = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting proxy shmsize specifier..";
	    }
	fPub2SubProxyShmKey = (key_t)strtol( argv[i+2], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting publisher-to-subscriber proxy-shmkey specifier..";
	    }
	fSub2PubProxyShmKey = (key_t)strtol( argv[i+3], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting subscriber-to-publisher proxy-shmkey specifier..";
	    }
	i += 4;
	return NULL;
	}
    if ( !strcmp( argv[i], "-subscriberretrycount" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing susbcriber retry count specifier.";
	    }
	fSubscriberRetryCount = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting susbcriber retry count specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-subscriberretrytime" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing susbcriber retry time specifier.";
	    }
	fSubscriberRetryTime = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting susbcriber retry time specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-lamaddress" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing lam address specifier.";
	    }
	LAMAddressData lad;
	lad.fAddress = NULL;
	lad.fStr = argv[i+1];
	fLAMAddresses.insert( fLAMAddresses.end(), lad );
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventmodulo" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing event modulo specifier.";
	    }
	fSubscriptionEventModulo = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting event modulo specifier.";
	    }
	if ( !fSubscriptionEventModulo )
	    {
	    return "Event modulo specifier must not be zero.";
	    }
	fUseSubscriptionEventModulo = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventtypecode" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing event type code.";
	    }
	fEventTypeStackMachineCodesText.push_back( MLUCString(argv[i+1]) );
#if 0
	AliHLTTriggerStackMachineAssembler sma;
	MLUCSimpleStackMachineAssembler::TError smaErr;
	smaErr = sma.Assemble( argv[i+1] );
	//smaErr = ((MLUCSimpleStackMachineAssembler&)sma).Assemble( argv[i+1] );
	if ( !smaErr.Ok() )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledComponent::ParseCmdLine", "Stack Machine Assembler Error" )
		<< "Error returned by trigger stack machine assembler: " << smaErr.Description() << ENDLOG;
	    return "Error translating trigger stack machine assembler code for event type.";
	    }
	unsigned long codeWordCount = sma.GetCodeWordCount();
	unsigned long totalSize = codeWordCount*sizeof(uint64)+sizeof(AliHLTEventTriggerStruct);
	AliHLTEventTriggerStruct* code = reinterpret_cast<AliHLTEventTriggerStruct*>( new uint8[ totalSize ] );
	if ( !code )
	    {
	    return "Out of memory allocating event type code for trigger stack machine.";
	    }
	memcpy( &(code->fDataWords[0]), sma.GetCode(), sizeof(uint64)*codeWordCount );
	code->fHeader.fLength = totalSize;
	code->fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
	code->fHeader.fSubType.fID = 0;
	code->fHeader.fVersion = 1;
	code->fDataWordCount = (codeWordCount*sizeof(uint64))/sizeof(code->fDataWords[0]);

	fEventTypeStackMachineCodes.push_back( code );
#endif
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-ctptriggerclasses" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing CTP trigger class specifier.";
	    }
	MLUCString tcString( argv[i+1] );
	if ( tcString.Length()>0 )
	    {
	    std::vector<MLUCString> triggerClasses, triggerClass, detectors;
	    tcString.Split( triggerClasses, ',' );
	    std::vector<MLUCString>::iterator tcIter, tcEnd;
	    tcIter = triggerClasses.begin();
	    tcEnd = triggerClasses.end();
	    while ( tcIter != tcEnd )
		{
		tcIter->Split( triggerClass, ':' );
		if ( triggerClass.size()!=3 )
		    return "Error parsing trigger class specifier (single trigger class format)";
		unsigned long ndx=0;
		ndx = strtoul( triggerClass[0].c_str(), &cpErr, 10 );
		if ( *cpErr != '\0' )
		    return "Error parsing trigger class specifier (single trigger class bit index specifier)";
		
		fStackMachineAssembler.AddSymbol( triggerClass[1].c_str(), 1 << ndx );
		
		fTriggerClassBitIndices.insert( std::pair<MLUCString,unsigned>( triggerClass[1], ndx ) );
		
		tcIter++;
		}
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-ctptriggerclassgroup" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing CTP trigger class group identifier and match specification";
	    }
	if ( argc <= i+2 )
	    {
	    return "Missing CTP trigger class group match specification";
	    }
	fTriggerClassGroups.insert( std::pair<MLUCString,MLUCString>( MLUCString(argv[i+1]), MLUCString(argv[i+2]) ) );
	i += 3;
	return NULL;
	}
    if ( !strcmp( argv[i], "-transient" ) )
	{
	fTransientSubscriber = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-persistentstateshm" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing persistent state store shared memory id parameter";
	    }
	fPersistentStateShmID = (key_t)strtol( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting persistent state store shared memory id specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-maxeventage" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing maximum event age specifier.";
	    }
	fMaxEventAge = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting maximum event age specifier.";
	    }
	i += 2;
	return NULL;
	}
#ifdef ALLOW_ARTIFICIAL_EVENT_LOSS
    if ( !strcmp( argv[i], "-eventlossmodulo" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing event loss modulo specifier.";
	    }
	fEventLossModulo = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting event loss modulo specifier.";
	    }
	i += 2;
	return NULL;
	}
#endif
    if ( !strcmp( argv[i], "-alicehlt" ) )
	{
	fAliceHLT = true;
	i++;
	return NULL;
	}
    if ( fComponentType==kDataProcessor || fComponentType==kDataSink )
	{
	if ( !strcmp( argv[i], "-secondary" ) )
	    {
	    fSecondary = true;
	    i++;
	    return NULL;
	    }
	}
    if ( fComponentType==kDataSource && fAllowDataTypeOriginSpecSet )
	{
	if ( !strcmp( argv[i], "-datatype" ) )
	    {
	    if ( argc <= i +1 )
		{
		return "Missing datatype specifier.";
		}
	    if ( strlen( argv[i+1])>8 )
		{
		return "Maximum allowed length for datatype specifier is 8 characters.";
		}
	    AliUInt64_t tmpDT = 0;
	    unsigned tmpN;
	    for( tmpN = 0; tmpN < strlen( argv[i+1]); tmpN++ )
		{
		tmpDT = (tmpDT << 8) | (AliUInt64_t)(argv[i+1][tmpN]);
		}
	    for ( ; tmpN < 8; tmpN++ )
		{
		tmpDT = (tmpDT << 8) | (AliUInt64_t)(' ');
		}
	    fEventDataType.fID = tmpDT;
	    i += 2;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-dataorigin" ) )
	    {
	    if ( argc <= i +1 )
		{
		return "Missing dataorigin specifier.";
		}
	    if ( strlen( argv[i+1])>4 )
		{
		return "Maximum allowed length for dataorigin specifier is 4 characters.";
		}
	    AliUInt32_t tmpDO = 0;
	    unsigned tmpN;
	    for( tmpN = 0; tmpN < strlen( argv[i+1]); tmpN++ )
		{
		tmpDO = (tmpDO << 8) | (AliUInt32_t)(argv[i+1][tmpN]);
		}
	    for ( ; tmpN < 4; tmpN++ )
		{
		tmpDO = (tmpDO << 8) | (AliUInt32_t)(' ');
		}
	    fEventDataOrigin.fID = tmpDO;
	    i += 2;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-dataspec" ) )
	    {
	    if ( argc <= i +1 )
		{
		return "Missing event data specification specifier.";
		}
	    AliUInt32_t tmpll = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		return "Error converting event data specification specifier..";
		}
	    fEventDataSpecification = tmpll;
	    i += 2;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-hwcoprocdatatype" ) )
	    {
	    if ( argc <= i +1 )
		{
		return "Missing hardware co-processor datatype specifier.";
		}
	    if ( strlen( argv[i+1])>8 )
		{
		return "Maximum allowed length for hardware co-processor datatype specifier is 8 characters.";
		}
	    AliUInt64_t tmpDT = 0;
	    unsigned tmpN;
	    for( tmpN = 0; tmpN < strlen( argv[i+1]); tmpN++ )
		{
		tmpDT = (tmpDT << 8) | (AliUInt64_t)(argv[i+1][tmpN]);
		}
	    for ( ; tmpN < 8; tmpN++ )
		{
		tmpDT = (tmpDT << 8) | (AliUInt64_t)(' ');
		}
	    fHardwareCoProcessorOutputEventDataType.fID = tmpDT;
	    fHardwareCoProcessorOutputEventDataTypeSet = true;
	    i += 2;
	    return NULL;
	    }
	}
    if ( fComponentType==kDataSource )
	{
	if ( !strcmp( argv[i], "-ecsparameters" ) )
	    {
	    if ( argc <= i +1 )
		{
		return "Missing ECS parameters specifier.";
		}
	    fECSParameters = argv[i+1];
	    i += 2;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-hwcoproc" ) )
	    {
	    fHardwareCoProcessor = true;
	    i += 1;
	    return NULL;
	    }
	}
    if ( fComponentType==kDataSource && fAllowIgnoreEventTriggerCommands )
	{
	if ( !strcmp( argv[i], "-ignoreeventtriggercommands" ) )
	    {
	    fIgnoreEventTriggerCommands = true;
	    i += 1;
	    return NULL;
	    }
	}
    if ( !strcmp( argv[i], "-runnr" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing run number specifier.";
	    }
	fRunNumber = strtoul( argv[i+1], &cpErr, 0 );
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
	gLog.SetRunNumber( fRunNumber );
#endif
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting run number specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-beamtype" ) )
	{
	if ( argc <= i + 1 )
	    {
	    return "Missing beam type specifier.";
	    }
	fBeamType = 0;
	if ( !strcmp(argv[i+1], "pp") )
	    fBeamType = 1;
	else if ( !strcmp(argv[i+1], "AA") )
	    fBeamType = 2;
	else if ( !strcmp(argv[i+1], "pA") )
	    fBeamType = 3;
	else
	    {
	    errorArg = i+1;
	    return "Wrong first parameter for beam type (must be 'pp' or 'AA' or 'pA').";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-hltmode" ) )
	{
	if ( argc <= i + 1 )
	    {
	    return "Missing HLT mode specifier.";
	    }
	fHLTMode = 0;
	if ( !strcmp(argv[i+1],"A") )
	    fHLTMode = 1;
	else if ( !strcmp(argv[i+1],"B") )
	    fHLTMode = 2;
	else if ( !strcmp(argv[i+1],"C") )
	    fHLTMode = 3;
	else if ( !strcmp(argv[i+1],"D") )
	    fHLTMode = 4;
	else if ( !strcmp(argv[i+1],"E") )
	    fHLTMode = 5;
	else
	    {
	    errorArg = i+1;
	    return "Wrong second parameter for HLT mode (must be 'A', 'B', 'C', 'D' or 'E').";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-runtype" ) )
	{
	if ( argc <= i + 1 )
	    {
	    return "Missing run type specifier.";
	    }
	fRunType = argv[i+1];
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-readoutlistversion" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing readout list data format version number specifier.";
	    }
	gReadoutListVersion = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting readout list data format version specifier.";
	    }
	i += 2;
	return NULL;
	}
#if 0
    if ( !strcmp( argv[i], "-runtype" ) )
	{
	if ( argc <= i +2 )
	    {
	    return "Missing run type specifier.";
	    }
	fRunType = 0;
	if ( !strcmp(argv[i+1], "pp") )
	    fRunType |= 1;
	else if ( !strcmp(argv[i+1], "AA") )
	    fRunType |= 2;
	else if ( !strcmp(argv[i+1], "pA") )
	    fRunType |= 3;
	else
	    {
	    errorArg = i+1;
	    return "Wrong first parameter for run type (must be 'pp' or 'AA' or 'pA').";
	    }
	if ( !strcmp(argv[i+2],"A") )
	    fRunType |= 1<<3;
	else if ( !strcmp(argv[i+2],"B") )
	    fRunType |= 2<<3;
	else if ( !strcmp(argv[i+2],"C") )
	    fRunType |= 3<<3;
	else if ( !strcmp(argv[i+2],"D") )
	    fRunType |= 4<<3;
	else if ( !strcmp(argv[i+2],"E") )
	    fRunType |= 5<<3;
	else
	    {
	    errorArg = i+1;
	    return "Wrong second parameter for run type (must be 'A', 'B', 'C', 'D' or 'E').";
	    }
	i += 3;
	return NULL;
	}
#endif
    if ( !strcmp( argv[i], "-eventaccounting" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing event accounting output file directory.";
	    }
	fEventAccounting = new AliHLTEventAccounting;
	fEventAccounting->SetDirectory( argv[i+1] );
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-forceeventdonedataforward" ) )
	{
	fForceEventDoneDataForward = true;
	i += 1;
	return NULL;
	}
    if ( !strcmp( argv[i], "-premaprorcbuffers" ) )
        {
	if ( argc <= i+1 )
	  {
            return "Missing list of rorc buffer ids to pre-map.";
	  }
	MLUCString bufferList=argv[i+1];
	std::vector<MLUCString> tokens;
	bufferList.Split(tokens, ',');
	std::vector<MLUCString>::iterator iter;
	for(iter=tokens.begin(); iter!=tokens.end(); ++iter){
	  if(iter->Size()==0) continue;
	  errno=0;
	  long int bufID=strtoul(iter->c_str(), NULL, 10);
	  if(errno){
	    return "Cannot parse list of RORC buffer IDs.";
	  }
	  fPreMapRorcBuffers.push_back(bufID);
	}
	i += 2;
	return NULL;
	}
    if ( fComponentType==kDataSource )
	{
	if ( !strcmp( argv[i], "-ddlid" ) )
	    {
	    if ( argc <= i+1 )
		{
		return "Missing DDL ID specifier.";
		}
	    fDDLID = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorArg = i+1;
		return "Error converting DDL ID specifier.";
		}
#if 0
	    TDetectorID detID;
	    detID = AliHLTReadoutList::GetDetectorID( fDDLID );
	    if ( detID >=kENDID )
		{
		errorArg = i+1;
		return "Invalid DDL ID (Detector ID unknown)";
		}
	    if ( AliHLTReadoutList::GetReadoutListDetectorDDLCount( gReadoutListVersion, detID )<AliHLTReadoutList::GetDetectorDDLNr( fDDLID ) )
		{
		errorArg = i+1;
		return "Invalid DDL ID (Detector DDL nr too high)";
		}
#endif
	    fEventDataType.fID = UNKNOWN_DATAID;
	    fEventDataOrigin.fID = UNKNOWN_DATAORIGIN;
	    fEventDataSpecification = ~(AliUInt32_t)0;
	    i += 2;
	    return NULL;
	    }
#ifdef USE_DDL_LIST_SHM
	if ( !strcmp( argv[i], "-ddllistshm" ) )
	    {
	    if ( argc <= i+2 )
		{
		return "Missing DDL list shmkey or shmsize parameter";
		}
	    if ( !strcmp( argv[i+1], "bigphys" ) )
		fDDLListShmKey.fShmType = kBigPhysShmType;
	    else if ( !strcmp( argv[i+1], "physmem" ) )
		fDDLListShmKey.fShmType = kPhysMemShmType;
	    else if ( !strcmp( argv[i+1], "sysv" ) )
		fDDLListShmKey.fShmType = kSysVShmType;
	    else if ( !strcmp( argv[i+1], "librorc" ) )
		fDDLListShmKey.fShmType = kLibrorcShmType;
	    else
		{
		errorArg = i+1;
		return "Invalid DDL list shm type specifier...";
		}
	    fDDLListShmKey.fKey.fID = strtoul( argv[i+2], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorArg = i+2;
		return "Error converting DDL list shmkey specifier..";
		}
	    i += 3;
	    return NULL;
	    }
#endif
	if ( !strcmp( argv[i], "-tickeventinterval" ) )
	    {
	    if ( argc <= i +1 )
		{
		return "Missing tick event interval specifier.";
		}
	    fTickInterval_us = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorArg = i+1;
		return "Error converting tick event interval specifier.";
		}
	    i += 2;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-maxpendingevents" ) )
	    {
	    if ( argc <= i +1 )
		{
		return "Missing maximum pending event count specifier.";
		}
	    fMaxPendingEventCount = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorArg = i+1;
		return "Error converting maximum pending event count specifier.";
		}
	    i += 2;
	    return NULL;
	    }
	}
    return "Unknown Parameter";
    }
	

void AliHLTControlledComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Usage" )
	<< "Error parsing command line arguments: " << errorStr << "." << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Usage" )
	<< "Offending argument: " << fArgv[errorArg] << " (arg " << AliHLTLog::kDec << errorArg << ")." << ENDLOG;
    if ( errorParam>=0 && errorParam<fArgc )
	{
	LOG( AliHLTLog::kError, "Controlled Processing Component", "Usage" )
	    << "Offending parameter: " << fArgv[errorParam] << "." << ENDLOG;
	}
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-control <status&control address URL>: Specify the address on which the Status&Control listens for remote supervision." << ENDLOG;
    if ( fComponentType==kDataSource || fComponentType==kDataProcessor || fComponentType==kAsyncProcessor )
	{
	LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	    << "-publisher <publisher-id>: Specifies the name/id to use for the publisher object. (Mandatory)" << ENDLOG;
	}
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-V <verbosity>: Specifies the verbosity to use in the program. Set bits correspond to: 1 - benchmark, 2 - information, 4 - debug, 8 - warning, 16 - error, 32 - fatal error. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-nostdout: Inihibit logging to stdout. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-nofilelog: Inihibit logging to files. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-syslog <facility>: Send log messages to the syslog daemon. 'facility' can be one of the following values: DAEMON,USER,LOCAL0,LOCAL1,LOCAL2,LOCAL3,LOCAL4,LOCAL5,LOCAL6,LOCAL7. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-sysmeslog: Send log messages to the SysMES system. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-infologger <path-to-infologger-logserver-shared-library>: Send log messages to the infoLogger system via the given shared library. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-senddone: Specify that an EventDone is sent as soon as processing is finished. Without this option wait until the next steps sends an EventDone. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-minblocksize <minimum-output-block-size>: Specify the minimum size to use for output blocks. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-buffermanager [ ring | largestblock ]: Specify the type of buffer manager to use. (Optional, default largestblock)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-outputsizeestimate <per-block-fixed-size> <size-factor>: Specify elements for the expected size of the output data. This has two parts, a fixed ize for each input data block and a size factor with which the input data will be multiplied. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-shm [bigphys|physmem|sysv] <shmkey> <shmsize>: Specifies a 32 bit shm key and the size of the corresponding shared memory segment. (Mandatory)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-publishertimeout <publisher-timeout-ms>: Specifies a timeout to use in milliseconds for published events. (Optional)" << ENDLOG;
    if ( fComponentType==kDataSink || fComponentType==kDataProcessor || fComponentType==kAsyncProcessor )
	{
	LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	    << "-subscribe <publishername> <subscriberid>: Specifies the name of the publisher to subscribe to and the subscriber id to use. (Mandatory)" << ENDLOG;
	}
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-shmproxy <shmsize> <publisher-to-subscriber-shmkey> <subscriber-to-publisher-shmkey>: Use shared memory proxies for publisher subscriber communication using the given parameters. (Optional, default is named pipe communication)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-subscriberretrycount <max-retry-count>: Specify the maximum number of retries that new events are tried to be processed. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-subscriberretrytime <retry-time-in-milliseconds>: Specify the amount of time (in milliseconds) between of retries for new events to be processed. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-lamaddress <lam-target-address-URL>: Specify an address to which Look-At-Me (LAM) requests (interrupts) will be sent. (Optional, may be used multiple times)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-subscribeoldevents: Request old events already present in publisher when starting processing. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-nosubscribeoldevents: Do not request old events already present in publisher when starting processing. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-eventmodulo <event-modulo-count>: Request an event modulo count for subscription (receive only every n'th event). (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-eventtypecode <trigger-stack-machine-assembler-code>: Specify some assembler code to be executed in the parent publisher for each event to determine whether this subscriber wants to receive the event. (Optionla, only available in ALICE HLT mode)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-ctptriggerclasses <CTP-Trigger-Class-String>: Specify the ECS string containing the trigger class specification to the component. (Optional, only in special ALICE HLT mode)." << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-ctptriggerclassgroup <CTP-Trigger-Class-Group-Identifier> <CTP-Trigger-Class-Group-RegEx-Match-Specification>: Specify a group of trigger classes/bits, identifieed by a common identifier, that are specified by a regex applied to the trigger class identifier. (Optional, only in special ALICE HLT mode)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-transient: The subscription will be made transient. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-maxeventage <maxmimum-event-age>: Specify the maxmimum age for events still allowed in the system. (Optional)" << ENDLOG;
    if ( fComponentType!=kDataSink )
	{
	LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	    << "-eventmodulopredivisor <event modulo pre-divisor>: Specify a number by which the event ID/number will be divided before the event modulo is calculated ( (event-ID-or-Number / pre-divisor) % event-modulo). (Optional, default 1)" << ENDLOG;
	}
    if ( fComponentType==kDataSource && fAllowDataTypeOriginSpecSet )
	{
	LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	    << "-datatype <type>: Specify the type as which the data is to be specified. (Optional)" << ENDLOG;
	LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	    << "-dataorigin <type>: Specify the origin to be specified for the data. (Optional)" << ENDLOG;
	LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	    << "-dataspec <type>: Specify the specifier that is to be used to characterize the data. (Optional)" << ENDLOG;
	}
#ifdef ALLOW_ARTIFICIAL_EVENT_LOSS
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-eventlossmodulo <event-loss-modulo>: Specify an artificial event loss modulo specifier. (Optional)" << ENDLOG;
#endif
    if ( fComponentType==kDataProcessor )
	{
	LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	    << "-secondary: Specify that this process is a secondary process in a group processing round-robin. Important only for broadcast events. (Optional)" << ENDLOG;
	}
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-forceeventdonedataforward: Specify that this process is sending out event done data with a flag set that enforces immediate forwarding. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-alicehlt: Activate some special features and options useful only for the ALICE HLT. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-runnr <run-number>: Set the run number to be used. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-beamtype [pp|AA]: Set the type of beam, either pp or AA. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-hltmode [A|B|C|D|E]: Set the HLT mode for this run, A, B, C, D, or E. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-runtype <run-type-string>: Set the type of run. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-readoutlistversion <readout-list-data-format-version-number>: Set the version number of the data format for the readout list data. (Optional)" << ENDLOG;
#if 0
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-runtype [pp|AA] [A|B|C|D|E]: Set the type of run. Two components, beam type: pp or AA and HLT mode: A, B, C, D, or E. (Optional)" << ENDLOG;
#endif
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-eventaccounting <output-directory>: Activate full event accounting through the whole component's lifetime, output files will be written into the given directory which must already exist. (Optional)" << ENDLOG;
    if ( fComponentType==kDataSource )
	{
	LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	    << "-ddlid <ALICE-DDL-ID>: Specify the ID of the DDL for which data is published. (Mandatory, only valid in ALICE HLT mode)" << ENDLOG;
#ifdef USE_DDL_LIST_SHM
	LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	    << "-ddllistshm [bigphys|physmem|sysv] <shmkey>: Specifies a 32 bit shm key for a shared memory segment to be used for the auto-generated DDL list. (Mandatory, only valid for ALICE HLT mode)" << ENDLOG;
#endif
	LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	    << "-tickeventinterval <interval-micro-secs.>: Set the interval (in microseconds) that passes between tick events that are produced periodically. (Optional, default 0 for disabled)" << ENDLOG;
	LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	    << "-maxpendingevents <<maximum-pending-event-count>: Set the maximum number of pending output events that is allowed to be in the system. (optional, default 0 for disabled)" << ENDLOG;
	LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	    << "-hwcoproc: Specify that the data produced corresponds not to raw data but to raw data pre-processed already by a hardware co-processor. (Optional, only in special ALICE HLT mode)" << ENDLOG;
	}
    if ( fComponentType==kDataSource && fAllowDataTypeOriginSpecSet )
	{
	LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	    << "-hwcoprocdatatype <type>: Specify the type as which the data from the hardware co-processor is to be specified. (Optional, only in special ALICE HLT mode, default is DDLPRPRC)" << ENDLOG;
	}
    if ( fComponentType==kDataSource && fAllowIgnoreEventTriggerCommands )
	{
	LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	    << "-ignoreeventtriggercommands: Ignore commands that trigger the generation of special events. (Optional)" << ENDLOG;
	}
    }

void AliHLTControlledComponent::IllegalCmd( AliUInt32_t state, AliUInt32_t cmd )
    {
    MLUCString cmdStr, stateStr;
    AliHLTSCComponentInterface::GetCommandName( cmd, cmdStr );
    AliHLTSCComponentInterface::GetStatusName( state, stateStr );
    LOG( AliHLTLog::kWarning, "AliHLTControlledComponent::IllegalCmd", "Illegal Command" )
	<< "Illegal command '" << cmdStr.c_str() << "' 0x" << AliHLTLog::kHex << cmd << "/" << AliHLTLog::kDec
	<< cmd << ") for state '" << stateStr.c_str() << "' (0x" << AliHLTLog::kHex << state << "/" << AliHLTLog::kDec
	<< state << ")." << ENDLOG;
    AliHLTStackTrace( "Illegal Command", AliHLTLog::kWarning, 4 );
    }


bool AliHLTControlledComponent::SetupSC()
    {
    fStatus = CreateStatusData();
    if ( !fStatus )
	return false;
    if ( !SetupStatusData() )
	return false;

    fSCInterface = CreateSCInterface();
    if ( !fSCInterface )
	return false;
    if ( !SetupSCInterface() )
	return false;

    return true;
    }
	
AliHLTStatus* AliHLTControlledComponent::CreateStatusData()
    {
    return new AliHLTStatus;
    }

bool AliHLTControlledComponent::SetupStatusData()
    {
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    fStatus->fProcessStatus.fState = 0;
    fStatus->fProcessStatus.fState |= kAliHLTPCStartedStateFlag;
    stateLock.Unlock();
//     fStatus->fProcessStatus.fState = kAliHLTPCStartedState;
    switch ( fComponentType )
	{
	case kDataSource:
	    fStatus->fProcessStatus.fProcessType = kAliHLTSCControlledDataSourceComponentType;
	    break;
	case kDataSink:
	    fStatus->fProcessStatus.fProcessType = kAliHLTSCControlledDataSinkComponentType;
	    break;
	case kDataProcessor: // Fallthrough
	case kAsyncProcessor:
	    fStatus->fProcessStatus.fProcessType = kAliHLTSCControlledDataProcessorComponentType;
	    break;
	default:
	    fStatus->fProcessStatus.fProcessType = kAliHLTSCUndefinedComponentType;
	    break;
	}
    fStatus->Touch();
    return true;
    }

AliHLTSCInterface* AliHLTControlledComponent::CreateSCInterface()
    {
    return new AliHLTSCInterface();
    }
	
bool AliHLTControlledComponent::SetupSCInterface()
    {
    int ret;
    ret = fSCInterface->Bind( fSCInterfaceAddress.c_str() );
    if ( ret )
	{
	return false;
	}
    fSCInterface->SetStatusDataMutex( &(fStatus->fStatusDataMutex) );
    fSCInterface->SetStatusData( &(fStatus->fHeader) );
    fSCInterface->AddCommandProcessor( &fCmdProcessor );
    ret = fSCInterface->Start();
    if ( ret )
	{
	return false;
	}
    return true;
    }

void AliHLTControlledComponent::RemoveSC()
    {
    if ( fSCInterface )
	{
	fSCInterface->Stop();
	fSCInterface->Unbind();
	delete fSCInterface;
	fSCInterface = NULL;
	}
    if ( fStatus )
	{
	delete fStatus;
	fStatus = NULL;
	}
    }

void AliHLTControlledComponent::LookAtMe()
    {
    vector<LAMAddressData>::iterator lamIter, lamEnd;
    int ret;
    pthread_mutex_lock( &fLAMAddressMutex );
    lamIter = fLAMAddresses.begin();
    lamEnd = fLAMAddresses.end();
    while ( lamIter != lamEnd )
	{
	if ( lamIter->fAddress )
	    {
	    ret = fSCInterface->LookAtMe( lamIter->fAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTControlledComponent<BaseClass,PublisherClass,SubscriberClass>::LookAtMe", "Error sending LAM request" )
		    << "Error sending LAM request to LAM address '" << lamIter->fStr.c_str() << ": "
		    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
		}
	    }
	lamIter++;
	}
    pthread_mutex_unlock( &fLAMAddressMutex );
    }


void AliHLTControlledComponent::ProcessSCCmd( AliHLTSCCommandStruct* cmdS )
    {
    bool ok = false;
    AliHLTSCCommand cmd( cmdS );
    if ( !fStatus )
	{
	// XXX
	return;
	}
// XXXX TODO LOCKING XXXX
    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    switch ( cmd.GetData()->fCmd )
	{
	case kAliHLTPCNOPCmd:
	    ok = true;
	    break;
	case kAliHLTPCEndProgramCmd:
	    if ( !(fStatus->fProcessStatus.fState & ~(kAliHLTPCStartedStateFlag|kAliHLTPCErrorStateFlag)) )
		{
		ok = true;
		fCmdSignal.AddNotificationData( cmd.GetData()->fCmd );
		fCmdSignal.Signal();
		}
	    break;
	case kAliHLTPCConfigureCmd:
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag )
		{
		ok = true;
		break;
		}
	    ok = true;
	    stateLock.Unlock();
	    statusDataLock.Unlock();
	    ExtractConfig( cmdS );
	    Configure();
	    statusDataLock.Lock();
	    stateLock.Lock();
	    break;
	case kAliHLTPCUnconfigureCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) && !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		{
		ok = true;
		break;
		}
	    ok = true;
	    stateLock.Unlock();
	    statusDataLock.Unlock();
	    Unconfigure();
	    statusDataLock.Lock();
	    stateLock.Lock();
	    fStatus->fProcessStatus.fState = fStatus->fProcessStatus.fState & ~(kAliHLTPCConfiguredStateFlag|kAliHLTPCErrorStateFlag);
	    break;
	case kAliHLTPCResetConfigurationCmd:
	    if ( fStatus->fProcessStatus.fState & ~kAliHLTPCStartedStateFlag )
		break;
	    ok = true;
	    stateLock.Unlock();
	    statusDataLock.Unlock();
	    ResetConfiguration();
	    statusDataLock.Lock();
	    stateLock.Lock();
	    break;
	case kAliHLTPCAcceptSubscriptionsCmd:
	    if ( fComponentType!=kDataSource && fComponentType!=kDataProcessor && fComponentType!=kAsyncProcessor )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
		break;
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag )
		{
		ok = true;
		break;
		}
	    ok = true;
	    
	    fCmdSignal.AddNotificationData( cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCStopSubscriptionsCmd:
	    if ( fComponentType!=kDataSource && fComponentType!=kDataProcessor && fComponentType!=kAsyncProcessor )
		break;
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) )
		{
		ok = true;
		break;
		}
	    ok = true;
	    fCmdSignal.AddNotificationData( cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCCancelSubscriptionsCmd:
	    if ( fComponentType!=kDataSource && fComponentType!=kDataProcessor && fComponentType!=kAsyncProcessor )
		break;
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) ||
		 (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCSubscribeCmd:
	    if ( fComponentType!=kDataSink && fComponentType!=kDataProcessor && fComponentType!=kAsyncProcessor )
		break;
// 	    if ( fStatus->fProcessStatus.fState & ~(kAliHLTPCStartedStateFlag|kAliHLTPCConfiguredStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag) )
// 		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) ||
		 (fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		break;
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) ||
		 (fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) )
		{
		ok = true;
		break;
		}
	    ok = true;
	    fCmdSignal.AddNotificationData( cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCUnsubscribeCmd:
	    if ( fComponentType!=kDataSink && fComponentType!=kDataProcessor && fComponentType!=kAsyncProcessor )
		break;
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) )
		{
		ok = true;
		break;
		}
	    ok = true;
	    fCmdSignal.AddNotificationData( cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCAbortSubscriptionCmd:
	    if ( fComponentType!=kDataSink && fComponentType!=kDataProcessor && fComponentType!=kAsyncProcessor )
		break;
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) )
		{
		ok = true;
		break;
		}
	    ok = true;
	    fCmdSignal.AddNotificationData( cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCStartProcessingCmd:
	    {
	    switch ( fComponentType )
		{
		case kDataSource:
		    if ( (fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) &&
			 !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
			if ( cmd.GetData()->fParam0 )
			    ((AliHLTDetectorPublisher*)fPublisher)->SetStartTimestamp( cmd.GetData()->fParam0 );
			ok = true;
		    break;
		case kDataProcessor: // Fallthrough
		case kAsyncProcessor:
		    if ( (fStatus->fProcessStatus.fState & (kAliHLTPCAcceptingSubscriptionsStateFlag|kAliHLTPCSubscribedStateFlag)) &&
			 !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
			ok = true;
		    break;
		case kDataSink:
		    if ( (fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) &&
			 !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
			ok = true;
		    break;
		default:
		    ok = false;
		    break;
		}
	    if ( ok )
		{
		if ( fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag )
		    break;
		fCmdSignal.AddNotificationData( cmd.GetData()->fCmd );
		fCmdSignal.Signal();
		}
	    break;
	    }
	case kAliHLTPCPauseProcessingCmd:
// 	    if ( fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag )
// 		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCResumeProcessingCmd:
// 		if ( fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag )
// 		    break;
	    ok = true;
	    fCmdSignal.AddNotificationData( cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCStopProcessingCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		{
		ok = true;
		break;
		}
	    ok = true;
	    fCmdSignal.AddNotificationData( cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCPublisherPinEventsCmd:
	    if ( fComponentType!=kDataSource && fComponentType!=kDataProcessor && fComponentType!=kAsyncProcessor )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCPublisherUnpinEventsCmd:
	    if ( fComponentType!=kDataSource && fComponentType!=kDataProcessor && fComponentType!=kAsyncProcessor )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCRemoveSubscriberCmd:
	    {
	    if ( fComponentType!=kDataSource && fComponentType!=kDataProcessor && fComponentType!=kAsyncProcessor )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) && 
		 !(fStatus->fProcessStatus.fState & kAliHLTPCWithSubscribersStateFlag) )
		break;
	    ok = true;
	    MLUCString tmp;
	    tmp = (char*)cmdS->fData;
	    stateLock.Unlock();
	    statusDataLock.Unlock();
	    pthread_mutex_lock( &fSubscriberRemovalListMutex );
	    fSubscriberRemovalList.insert( fSubscriberRemovalList.end(), tmp );
	    pthread_mutex_unlock( &fSubscriberRemovalListMutex );
	    fCmdSignal.AddNotificationData( cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    statusDataLock.Lock();
	    stateLock.Lock();
	    break;
	    }
	case kAliHLTPCDumpEventsCmd:
	    // The component must be a source or processor type component and started.
	    if ( fComponentType!=kDataSource && fComponentType!=kDataProcessor && fComponentType!=kAsyncProcessor )
		break;
	    if ( !(fStatus->fProcessStatus.fState & ~kAliHLTPCStartedStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCSimulateErrorCmd:
	    if ( !(fStatus->fProcessStatus.fState & ~kAliHLTPCStartedStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCInitiateReconfigureEventCmd:
	    if ( !(fStatus->fProcessStatus.fState & ~kAliHLTPCConfiguredStateFlag) )
		break;
	    stateLock.Unlock();
	    statusDataLock.Unlock();
	    if ( fComponentType == kDataSource && fPublisher && !fIgnoreEventTriggerCommands )
		{
		AliUInt64_t eventNr = 0;
		eventNr = cmd.GetData()->fParam0;
		eventNr <<= 32;
		eventNr |= cmd.GetData()->fParam1;
		((AliHLTDetectorPublisher*)fPublisher)->AddConfigureEvent( kAliEventTypeReconfigure, eventNr, (char const*)(cmdS->fData) );
		ok = true;
		}
	    statusDataLock.Lock();
	    stateLock.Lock();
	    break;
	case kAliHLTPCInitiateUpdateDCSEventCmd:
	    if ( !(fStatus->fProcessStatus.fState & ~kAliHLTPCConfiguredStateFlag) )
		break;
	    stateLock.Unlock();
	    statusDataLock.Unlock();
	    if ( fComponentType == kDataSource && fPublisher && !fIgnoreEventTriggerCommands )
		{
		AliUInt64_t eventNr = 0;
		eventNr = cmd.GetData()->fParam0;
		eventNr <<= 32;
		eventNr |= cmd.GetData()->fParam1;
		((AliHLTDetectorPublisher*)fPublisher)->AddConfigureEvent( kAliEventTypeDCSUpdate, eventNr, (char const*)(cmdS->fData) );
		ok = true;
		}
	    statusDataLock.Lock();
	    stateLock.Lock();
	    break;
	case kAliHLTPCDumpEventAccountingCmd:
	    if ( fEventAccounting )
		fEventAccounting->DumpEvents();
	    ok = true;
	    break;
	case kAliHLTPCPURGEALLEVENTSCmd:
	    ok = true;
	    fCmdSignal.AddNotificationData( cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCSetSubsystemVerbosityCmd:
	    ok = true;
	    SetSubsystemVerbosity( MLUCString((char const*)(cmdS->fData)), cmd.GetData()->fParam0 );
	    break;
	case kAliHLTPCDumpEventMetaDataCmd:
	    stateLock.Unlock();
	    statusDataLock.Unlock();
	    if ( fComponentType == kDataSource && fPublisher )
		((AliHLTDetectorPublisher*)fPublisher)->DumpEventMetaData();
	    else if ( fSubscriber )
		fSubscriber->DumpEventMetaData();
	    ok = true;
	    statusDataLock.Lock();
	    stateLock.Lock();
	    break;
	}
    if ( !ok )
	{
	AliUInt32_t state = fStatus->fProcessStatus.fState;
	stateLock.Unlock();
	statusDataLock.Unlock();
	IllegalCmd( state, cmd.GetData()->fCmd );
	statusDataLock.Lock();
	stateLock.Lock();
	}
    }


void AliHLTControlledComponent::ExtractConfig( AliHLTSCCommandStruct* cmdS )
    {
    if ( cmdS->fDataCnt<=0 )
	return;
    char* config = (char*)cmdS->fData;
    LOG( AliHLTLog::kDebug, "AliHLTControlledComponent::ExtractConfig", "Configuration string" )
	<< "Received configuration string: '" << config << "'." << ENDLOG;
    if ( strlen(config)<=0 )
	return;
    MLUCCmdlineParser parser;
    vector<MLUCString> tokens;
    if ( !parser.ParseCmd( config, tokens ) )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::ExtractConfig", "Error parsing configuration string" )
	    << "Error parsing configuration string '" << config << "'." << ENDLOG;
	return;
	}
    int argc = tokens.size();
    if ( argc<=0 )
	return;
    char** argv;
    argv = new char*[ argc+1 ];
    if ( !argv )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::ExtractConfig", "Out of memory" )
	    << "Out of memory allocating temporary buffer of " << AliHLTLog::kDec
	    << argc*sizeof(char*) << " bytes." << ENDLOG;
	return;
	}
    int i, errorArg=-1;
    const char* errorStr = NULL;

    for ( i = 0; i < argc; i++ )
	argv[i] = (char*)tokens[i].c_str();
    argv[argc] = NULL;

    i = 0;
    while ( (i < argc) && !errorStr )
	{
	errorStr = ParseCmdLine( argc, argv, i, errorArg );
	}
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::ExtractConfig", "Error extracting configuration from configuration string" )
	    << "Error extracting configuration from configuration string '" << config << "'." << ENDLOG;
	int tmpArgc = fArgc;
	char** tmpArgv = fArgv;
	fArgc = argc;
	fArgv = argv;
	PrintUsage( errorStr, i, errorArg );
	fArgc = tmpArgc;
	fArgv = tmpArgv;
	}
    delete [] argv;
    argv = NULL;
    }



bool AliHLTControlledComponent::SetupShmBufferMemory()
    {
    fSharedMemory = CreateSharedMemory();
    if ( !fSharedMemory )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::SetupShmBufferMemory", "No Shared Memory" )
	    << "Error creating shared memory object." << ENDLOG;
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	return false;
	}
    if ( !SetupSharedMemory() )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::SetupShmBufferMemory", "Error setting up Shared Memory" )
	    << "Error setting up shared memory object." << ENDLOG;
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	return false;
	}

    fShmManager = CreateShmManager();
    if ( !fShmManager )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::SetupShmBufferMemory", "Error creating shared memory manager" )
	    << "Error creating Shared Memory Manager object." << ENDLOG;
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	return false;
	}

    fBufferManager = CreateBufferManager();
    if ( !fBufferManager )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::SetupShmBufferMemory", "Error creating buffer manager" )
	    << "Error creating Buffer Manager object." << ENDLOG;
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	return false;
	}
// XXXX TODO LOCKING XXXX
    if ( fStatus )
	{
	MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	fStatus->fHLTStatus.fFreeOutputBuffer = fStatus->fHLTStatus.fTotalOutputBuffer = fBufferManager->GetTotalBufferSize();
	}
    
    return true;
    }

bool AliHLTControlledComponent::SetupSharedMemory()
    {
    unsigned long oldSize = fBufferSize;
    fOutputShm = fSharedMemory->GetShm( fShmKey, fBufferSize );
    if (fBufferSize < oldSize)
    {
	LOG( AliHLTLog::kWarning, "AliHLTControllerComponent::SetupSharedMemory", "Buffer Size Reduced" ) << "Shared memory initialization reduced the buffer size from 0x" << oldSize << " to 0x" << fBufferSize << ENDLOG;
    }
    if ( fOutputShm == (AliUInt32_t)-1 )
	return false;
    // Lock, even though we do not need the pointer to ensure that
    // the memory can be accessed fully.
    void* dummyPtr = fSharedMemory->LockShm( fOutputShm );
    if ( !dummyPtr )
	return false;
    //fSharedMemory->UnlockShm( fOutputShm );
    return true;
    }

bool AliHLTControlledComponent::RemoveShmBufferMemory()
    {
    if ( fBufferManager )
       {
// XXXX TODO LOCKING XXXX
       MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
       fStatus->fHLTStatus.fTotalOutputBuffer = 0;
       statusDataLock.Unlock();
       delete fBufferManager;
       fBufferManager = NULL;
       }
    if ( fShmManager )
       {
       delete fShmManager;
       fShmManager = NULL;
       }
    if ( fSharedMemory )
       {
#ifdef USE_DDL_LIST_SHM
       if ( fDDLListOutputShm != (AliUInt32_t)-1 )
	   {
	   fSharedMemory->UnlockShm( fDDLListOutputShm );
	   fSharedMemory->ReleaseShm( fDDLListOutputShm );
	   fDDLListOutputShm = (AliUInt32_t)-1;
	   fDDLListData = NULL;
	   }
#endif
       fSharedMemory->UnlockShm( fOutputShm );
       fSharedMemory->ReleaseShm( fOutputShm );
       delete fSharedMemory;
       fSharedMemory = NULL;
       }

    return true;
    }


#ifdef USE_DDL_LIST_SHM
bool AliHLTControlledComponent::SetupDDLListSharedMemory()
    {
    unsigned long size = (AliUInt32_t)AliHLTReadoutList::GetReadoutListSize();
    fDDLListOutputShm = fSharedMemory->GetShm( fDDLListShmKey, size );
    if ( fDDLListOutputShm == (AliUInt32_t)-1 || size<(AliUInt32_t)AliHLTReadoutList::GetReadoutListSize() )
	return false;
    // Lock, even though we do not need the pointer to ensure that
    // the memory can be accessed fully.
    fDDLListData = reinterpret_cast<hltreadout_uint32_t*>( fSharedMemory->LockShm( fDDLListOutputShm ) );
    if ( !fDDLListData )
	return false;
    return true;
    }
#endif

AliHLTSharedMemory* AliHLTControlledComponent::CreateSharedMemory()
    {
    return new AliHLTSharedMemory;
    }

AliHLTShmManager* AliHLTControlledComponent::CreateShmManager()
    {
    return new AliHLTShmManager( fMaxNoUseCount );
    }

AliHLTBufferManager* AliHLTControlledComponent::CreateBufferManager()
    {
    switch ( fBufferManagerType )
	{
	case kRingBufferManager:
	    return new AliHLTRingBufferManager( fShmKey, fBufferSize, fMinBlockSize );
	case kLargestBlockBufferManager:
	    {
	    AliHLTLargestBlockBufferManager* tmpBM = new AliHLTLargestBlockBufferManager( fShmKey, fBufferSize, fMaxPreEventCountExp2 );
	    tmpBM->SetAdaptBlockFreeUpdate( fAdaptBlockFreeUpdate );
	    return tmpBM;
	    }
	};
    return NULL;
    }


AliHLTDescriptorHandler* AliHLTControlledComponent::CreateInDescriptorHandler()
    {
    return new AliHLTDescriptorHandler( fMaxPreEventCountExp2 );
    }

AliHLTDescriptorHandler* AliHLTControlledComponent::CreateOutDescriptorHandler()
    {
    return new AliHLTDescriptorHandler( fMaxPreEventCountExp2 );
    }

AliHLTControlledComponent::SubscriptionListenerThread* AliHLTControlledComponent::CreateSubscriptionThread()
    {
    return new SubscriptionListenerThread( this, fPublisher );
    }

bool AliHLTControlledComponent::SetupComponents()
    {
//     if ( fBenchmark )
// 	fSubscriber->DoBenchmark();

    if ( fEventAccounting )
	{
	if ( fShmManager )
	    fEventAccounting->SetShmManager( fShmManager );
	fEventAccounting->SetName( fComponentName );
	fEventAccounting->SetRun( fRunNumber );
	}
    if ( fComponentType==kDataSink || fComponentType==kDataProcessor || fComponentType==kAsyncProcessor )
	{
#ifdef USE_PIPE_TIMEOUT
	fPublisherProxy->SetTimeout( 500000 );
#endif
	if ( fEventAccounting )
	    {
	    fSubscriber->SetEventAccounting( fEventAccounting );
	    fPublisherProxy->SetEventAccounting( fEventAccounting );
	    }
	fSubscriber->SetPublisher( fPublisherProxy );
	fSubscriber->SetRetryCount( fSubscriberRetryCount );
	fSubscriber->SetRetryTime( fSubscriberRetryTime );
	if ( fPublisher )
	    fSubscriber->SetRePublisher( (AliHLTProcessingRePublisher*)fPublisher );
	fSubscriber->SetBufferManager( fBufferManager );
	fSubscriber->SetInDescriptorHandler( fInDescriptorHandler );
	fSubscriber->SetOutDescriptorHandler( fOutDescriptorHandler );
	fSubscriber->SetShmManager( fShmManager );
	fSubscriber->SetStatusData( fStatus );
	fSubscriber->SetRunNumber( fRunNumber );
#ifdef ALLOW_ARTIFICIAL_EVENT_LOSS
	fSubscriber->SetEventLossModulo( fEventLossModulo );
#endif
	if ( fSecondary )
	    fSubscriber->Secondary();
	if ( fForceEventDoneDataForward )
	    fSubscriber->ForceEventDoneDataForward();
	}
    if ( fComponentType==kAsyncProcessor )
	fSubscriber->SetAsync();
    if ( fComponentType!=kDataSink && fPublisher && fEventAccounting )
	fPublisher->SetEventAccounting( fEventAccounting );
    if ( fComponentType==kDataSink || fComponentType==kAsyncProcessor )
	fSubscriber->NoRePublishing();
    if ( fComponentType==kDataProcessor || fComponentType==kAsyncProcessor )
	((AliHLTProcessingRePublisher*)fPublisher)->SetSubscriber( fSubscriber );

    if ( fAliceHLT && ( fComponentType==kDataProcessor || fComponentType==kDataSink || fComponentType==kAsyncProcessor ) )
	{
	fSubscriber->AliceHLT();

	std::map<MLUCString,MLUCString>::iterator tcgIter = fTriggerClassGroups.begin(), tcgEnd = fTriggerClassGroups.end();
	MLUCRegEx regEx;
	while ( tcgIter != tcgEnd )
	    {
	    regEx.SetPattern( tcgIter->second.c_str(), MLUCRegEx::kPFExtended );
	    AliUInt64_t bits = 0;
	    std::map<MLUCString,unsigned>::iterator tcbiIter = fTriggerClassBitIndices.begin(), tcbiEnd = fTriggerClassBitIndices.end();
	    while ( tcbiIter != tcbiEnd )
		{
		if ( regEx.Search( tcbiIter->first.c_str() ) )
		    bits |= 1 << tcbiIter->second;
		++tcbiIter;
		}
	    fStackMachineAssembler.AddSymbol( tcgIter->first.c_str(), bits );
	    ++tcgIter;
	    }

	std::vector<MLUCString>::iterator codeIter = fEventTypeStackMachineCodesText.begin(), codeEnd = fEventTypeStackMachineCodesText.end();
	MLUCSimpleStackMachineAssembler::TError smaErr;
	while ( codeIter != codeEnd )
	    {
	    smaErr = fStackMachineAssembler.Assemble( *codeIter );
	    //smaErr = ((MLUCSimpleStackMachineAssembler&)fStackMachineAssembler).Assemble( argv[i+1] );
	    if ( !smaErr.Ok() )
		{
		LOG( AliHLTLog::kError, "AliHLTControlledComponent::SetupComponents", "Stack Machine Assembler Error" )
		    << "Error returned by trigger stack machine assembler: " << smaErr.Description() << ENDLOG;
		return false;
		}
	    unsigned long codeWordCount = fStackMachineAssembler.GetCodeWordCount();
	    unsigned long totalSize = codeWordCount*sizeof(uint64)+sizeof(AliHLTEventTriggerStruct);
	    AliHLTEventTriggerStruct* code = reinterpret_cast<AliHLTEventTriggerStruct*>( new uint8[ totalSize ] );
	    if ( !code )
		{
		LOG( AliHLTLog::kError, "AliHLTControlledComponent::SetupComponents", "Out of memory" )
		    << "Out of memory allocating event type code for trigger stack machine." << ENDLOG;
		return false;
		}
	    memcpy( &(code->fDataWords[0]), fStackMachineAssembler.GetCode(), sizeof(uint64)*codeWordCount );
	    code->fHeader.fLength = totalSize;
	    code->fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
	    code->fHeader.fSubType.fID = 0;
	    code->fHeader.fVersion = 1;
	    code->fDataWordCount = (codeWordCount*sizeof(uint64))/sizeof(code->fDataWords[0]);
	    fEventTypeStackMachineCodes.push_back( code );
	    codeIter++;
	    }
	}

    
    if ( fComponentType==kDataSource || fComponentType==kDataProcessor || fComponentType==kAsyncProcessor )
	{
	if ( fPublisher )
	    {
	    fPublisher->SetMaxTimeout( fPublisherTimeout );
	    fPublisher->SetSubscriptionLoop( &PublisherPipeSubscriptionInputLoop );
	    fPublisher->AddSubscriberEventCallback( &fSubscriberEventCallback );
	    fPublisher->SetEventModuloPreDivisor( fEventModuloPreDivisor );
	    if ( fAliceHLT )
		fPublisher->SetEventTriggerTypeCompareFunc( StackMachineCompareEventTrigger, fTriggerStackMachine );
	    }
	}
    if ( fComponentType==kDataSource )
	{
	if ( !fPrivateSharedMemory )
	    {
	    ((AliHLTDetectorPublisher*)fPublisher)->SetBufferManager( fBufferManager );
	    ((AliHLTDetectorPublisher*)fPublisher)->SetShmManager( fShmManager );
	    ((AliHLTDetectorPublisher*)fPublisher)->SetShm( fSharedMemory );
	    }
	if ( !fECSParameters.IsWhiteSpace() && !fECSParameters.IsEmpty() )
	    ((AliHLTDetectorPublisher*)fPublisher)->SetECSParameters( fECSParameters );
	((AliHLTDetectorPublisher*)fPublisher)->SetDescriptorHandler( fOutDescriptorHandler );
	((AliHLTDetectorPublisher*)fPublisher)->SetStatusData( fStatus );
	((AliHLTDetectorPublisher*)fPublisher)->SetMaxEventAge( fMaxEventAge );
	((AliHLTDetectorPublisher*)fPublisher)->SetTickInterval( fTickInterval_us );
	((AliHLTDetectorPublisher*)fPublisher)->SetRunNumber( fRunNumber );
	((AliHLTDetectorPublisher*)fPublisher)->SetRunType( fBeamType, fHLTMode, fRunType );
	((AliHLTDetectorPublisher*)fPublisher)->SetMaxPendingEventCount( fMaxPendingEventCount );
	if ( fComponentType==kDataSource && fPersistentState )
	    ((AliHLTDetectorPublisher*)fPublisher)->SetPersistentState( reinterpret_cast< AliHLTPersistentState<AliHLTDetectorPublisher::State>* >( fPersistentState )->GetPtr() );
	if ( fAliceHLT )
	    {
	    ((AliHLTDetectorPublisher*)fPublisher)->AliceHLT();
#ifdef USE_DDL_LIST_SHM
	    if ( fComponentType==kDataSource )
		((AliHLTDetectorPublisher*)fPublisher)->SetDDLListBlock( fDDLListBlock, fDDLListData );
#else
	    if ( fComponentType==kDataSource )
		((AliHLTDetectorPublisher*)fPublisher)->SetDDLListData( fDDLListData );
#endif
	    }
	}

    
//     fPublisher->Subscribe( *fSubscriber );
    return true;
    }

bool AliHLTControlledComponent::Subscribe()
    {
    if ( fComponentType==kDataSink || fComponentType==kDataProcessor || fComponentType==kAsyncProcessor )
	{
	int ret;
	ret = fPublisherProxy->Subscribe( *fSubscriber );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledComponent::Subscribe", "Error creating subscribing" )
		<< "Error subscribing subscriber '" << fSubscriber->GetName() << "' to publisher '"
		<< fPublisherProxy->GetName() << "': " << strerror(ret) << " ("
		<< MLUCLog::kDec << ret << ")." << ENDLOG;
// XXXX TODO LOCKING XXXX
	    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCConsumerErrorStateFlag;
	    return false;
	    }
	if ( fTransientSubscriber )
	    {
	    fPublisherProxy->SetPersistent( *fSubscriber, false );
	    }
	if ( fUseSubscriptionEventModulo )
	    {
	    fPublisherProxy->SetEventType( *fSubscriber, fSubscriptionEventModulo );
	    }
	if ( fAliceHLT && fEventTypeStackMachineCodes.size()>0 )
	    {
	    AliUInt32_t modulo = 1;
	    if ( fUseSubscriptionEventModulo )
		modulo = fSubscriptionEventModulo;
	    fPublisherProxy->SetEventType( *fSubscriber, fEventTypeStackMachineCodes, modulo );
	    }
	if ( fRequestPublisherEventDoneData )
	    fPublisherProxy->SetEventDoneDataSend( *fSubscriber, true );
		
	fSubscriptionThread->Start();
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	
	return true;
	}
    return false;
    }

void AliHLTControlledComponent::Unsubscribe()
    {
    fSubscriptionThread->Quit();
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    gettimeofday( &start, NULL );
    while ( !fSubscriptionThread->HasQuitted() && deltaT<timeLimit )
	{
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( !fSubscriptionThread->HasQuitted() )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::Unsubscribe", "Aborting Publishing Loop" )
	    << "Publishing loop for subscriber '" << fSubscriber->GetName() << "' and publisher '"
	    << fPublisherProxy->GetName() << "' has to be aborted." << ENDLOG;
	fSubscriptionThread->AbortPublishing();
	}
    deltaT = 0;    
    gettimeofday( &start, NULL );
    while ( !fSubscriptionThread->HasQuitted() && deltaT<timeLimit )
	{
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( !fSubscriptionThread->HasQuitted() )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::Unsubscribe", "Aborting Subscription Thread" )
	    << "Subscription thread loop for subscriber '" << fSubscriber->GetName() << "' and publisher '"
	    << fPublisherProxy->GetName() << "' has to be aborted." << ENDLOG;
	fSubscriptionThread->Abort();
	}
    fSubscriptionThread->Join();
    }

void AliHLTControlledComponent::AbortSubscription()
    {
    LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::AbortSubscription", "Aborting Publishing Loop" )
	<< "Publishing loop for subscriber '" << fSubscriber->GetName() << "' and publisher '"
	<< fPublisherProxy->GetName() << "' is being aborted." << ENDLOG;
    fSubscriptionThread->AbortPublishing();
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    gettimeofday( &start, NULL );
    while ( !fSubscriptionThread->HasQuitted() && deltaT<timeLimit )
	{
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( !fSubscriptionThread->HasQuitted() )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::AbortSubscription", "Aborting Subscription Thread" )
	    << "Subscription thread loop for subscriber '" << fSubscriber->GetName() << "' and publisher '"
	    << fPublisherProxy->GetName() << "' has to be aborted." << ENDLOG;
	fSubscriptionThread->Abort();
	}
    fSubscriptionThread->Join();
    }

void AliHLTControlledComponent::Subscription()
    {
    fSubscriberCount++;
    if ( fStatus )
	{
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCWithSubscribersStateFlag;
	}
    }

void AliHLTControlledComponent::Unsubscription()
    {
    if ( !--fSubscriberCount && fStatus )
	{
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCWithSubscribersStateFlag;
	}
    }

void AliHLTControlledComponent::PURGEALLEVENTS()
    {
    if ( (fComponentType==kDataProcessor || fComponentType==kDataSink || fComponentType==kAsyncProcessor) && fSubscriber )
	fSubscriber->PURGEALLEVENTS();
    if ( (fComponentType==kDataProcessor || fComponentType==kAsyncProcessor || fComponentType==kDataSource) && fPublisher )
	fPublisher->AbortAllEvents( false, false );
    }

bool AliHLTControlledComponent::RegisterSubsystemVerbosity( MLUCString const& subsysID, int& verbosity )
    {
    std::vector<TSubsystemVerbosityData>::iterator iter, end;
    MLUCMutex::TLocker locker( fSubsystemVerbosityMutex );
    iter = fSubsystemVerbosities.begin();
    end = fSubsystemVerbosities.end();
    while ( iter != end )
	{
	if ( iter->fID == subsysID )
	    return false;
	iter++;
	}
    TSubsystemVerbosityData newSVD( subsysID, verbosity );
    fSubsystemVerbosities.push_back( newSVD );
    return true;
    }

bool AliHLTControlledComponent::UnregisterSubsystemVerbosity( MLUCString const& subsysID )
    {
    std::vector<TSubsystemVerbosityData>::iterator iter, end;
    MLUCMutex::TLocker locker( fSubsystemVerbosityMutex );
    iter = fSubsystemVerbosities.begin();
    end = fSubsystemVerbosities.end();
    while ( iter != end )
	{
	if ( iter->fID == subsysID )
	    {
	    fSubsystemVerbosities.erase( iter );
	    return true;
	    }
	iter++;
	}
    return false;
    }

bool AliHLTControlledComponent::SetSubsystemVerbosity( MLUCString const& subsysID, int verbosity )
    {
    std::vector<TSubsystemVerbosityData>::iterator iter, end;
    MLUCMutex::TLocker locker( fSubsystemVerbosityMutex );
    iter = fSubsystemVerbosities.begin();
    end = fSubsystemVerbosities.end();
    while ( iter != end )
	{
	if ( iter->fID == subsysID )
	    {
	    *(iter->fVerbosity) = verbosity;
	    return true;
	    }
	iter++;
	}
    return false;
    }

void AliHLTControlledComponent::PreMapRorcBuffers()
{
  if(fPreMapRorcBuffers.size()==0) return;
  if(! fShmManager){
    LOG( AliHLTLog::kError, "AliHLTControlledComponent::PreMapRorcBuffers()" , "ShmManager not available." )
      << "No ShmManager available, cannot pre-map librorc buffers." << ENDLOG;
  } else {
    for(int i=0; i<fPreMapRorcBuffers.size(); ++i){
      AliHLTShmID id={ kLibrorcShmType, fPreMapRorcBuffers[i]};
      fShmManager->GetShm(id);
    }
  }
}

void AliHLTControlledComponent::UnMapRorcBuffers()
{
  if(fPreMapRorcBuffers.size()==0) return;
  if(! fShmManager){
    LOG( AliHLTLog::kError, "AliHLTControlledComponent::UnMapRorcBuffers()" , "ShmManager not available." )
      << "No ShmManager available, cannot unmap librorc buffers." << ENDLOG;
  } else {
    for(int i=0; i<fPreMapRorcBuffers.size(); ++i){
      AliHLTShmID id={ kLibrorcShmType, fPreMapRorcBuffers[i]};
      fShmManager->ReleaseShm(id);
    }
  }
}


AliHLTControlledSourceComponent::~AliHLTControlledSourceComponent()
    {
    }


AliHLTControlledAsyncProcessorComponent::~AliHLTControlledAsyncProcessorComponent()
    {
    }

AliHLTProcessingRePublisher* AliHLTControlledAsyncProcessorComponent::CreateRePublisher()
    {
    return new AliHLTProcessingRePublisher( fOwnPublisherName.c_str(), fMaxPreEventCountExp2 );
    }

AliHLTPublisherProxyInterface* AliHLTControlledAsyncProcessorComponent::CreatePublisherProxy()
    {
    if ( fProxyShmSize )
	return new AliHLTPublisherShmProxy( fAttachedPublisherName.c_str(), fProxyShmSize, fPub2SubProxyShmKey, fSub2PubProxyShmKey );
    else
	return new AliHLTPublisherPipeProxy( fAttachedPublisherName.c_str() );
    }


AliHLTControlledProcessorComponent::~AliHLTControlledProcessorComponent()
    {
    }

AliHLTProcessingRePublisher* AliHLTControlledProcessorComponent::CreateRePublisher()
    {
    return new AliHLTProcessingRePublisher( fOwnPublisherName.c_str(), fMaxPreEventCountExp2 );
    }

AliHLTPublisherProxyInterface* AliHLTControlledProcessorComponent::CreatePublisherProxy()
    {
    if ( fProxyShmSize )
	return new AliHLTPublisherShmProxy( fAttachedPublisherName.c_str(), fProxyShmSize, fPub2SubProxyShmKey, fSub2PubProxyShmKey );
    else
	return new AliHLTPublisherPipeProxy( fAttachedPublisherName.c_str() );
    }

AliHLTControlledSinkComponent::~AliHLTControlledSinkComponent()
    {
    }

AliHLTProcessingRePublisher* AliHLTControlledSinkComponent::CreateRePublisher()
    {
    return NULL;
    }

AliHLTPublisherProxyInterface* AliHLTControlledSinkComponent::CreatePublisherProxy()
    {
    if ( fProxyShmSize )
	return new AliHLTPublisherShmProxy( fAttachedPublisherName.c_str(), fProxyShmSize, fPub2SubProxyShmKey, fSub2PubProxyShmKey );
    else
	return new AliHLTPublisherPipeProxy( fAttachedPublisherName.c_str() );
    }


void AliHLTControlledComponent::SubscriptionListenerThread::Run()
    {
    fQuitted = false;
    if ( fPub )
	{
// 	AliUInt32_t tmp = fComp->fStatus->fProcessStatus.fState;
// 	tmp &= ~kAliHLTPCChangingStateFlag;
// 	tmp |= kAliHLTPCAcceptingSubscriptionsStateFlag;
// 	fComp->fStatus->fProcessStatus.fState = tmp;
#if 0
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fComp->fStatus->fStatusDataMutex );
	MLUCMutex::TLocker stateLock( fComp->fStatus->fStateMutex );
	fComp->fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	stateLock.Unlock();
	statusDataLock.Unlock();
#else
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fComp->fStatus->fStatusDataMutex, false );
	MLUCMutex::TLocker stateLock( fComp->fStatus->fStateMutex, false );
#endif
#if 0
	fPub->AcceptSubscriptions( &AcceptingSubscriptionsCallbackFunc, (void*)fStatus );
	statusDataLock.Lock();
	stateLock.Lock();
	if ( !(fComp->fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) )
	    fComp->fStatus->fProcessStatus.fState |= kAliHLTPCProducerErrorStateFlag;
#else
	if ( fPub->AcceptSubscriptions( &AcceptingSubscriptionsCallbackFunc, (void*)(fComp->fStatus) ) )
	    {
	    statusDataLock.Lock();
	    stateLock.Lock();
	    fComp->fStatus->fProcessStatus.fState |= kAliHLTPCProducerErrorStateFlag;
	    }
	else
	    {
	    statusDataLock.Lock();
	    stateLock.Lock();
	    }
#endif
	fComp->fStatus->fProcessStatus.fState &= ~(kAliHLTPCChangingStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag);
	}
    fQuitted = true;
    }
		
void AliHLTControlledComponent::SubscriptionThread::Run()
    {
    fQuitted = false;
    if ( fProxy && fSubscriber )
	{
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fComp->fStatus->fStatusDataMutex );
	MLUCMutex::TLocker stateLock( fComp->fStatus->fStateMutex );
	fComp->fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	stateLock.Unlock();
	statusDataLock.Unlock();
	int ret = fProxy->StartPublishing( *fSubscriber, fComp->fSubscriptionRequestOldEvents );
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledComponent::SubscriptionThread::Run", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledComponent::SubscriptionThread::Run", "Subscription Error" )
		<< "Error from proxy's start publishing loop: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledComponent::SubscriptionThread::Run", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    statusDataLock.Lock();
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledComponent::SubscriptionThread::Run", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    stateLock.Lock();
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledComponent::SubscriptionThread::Run", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    fComp->fStatus->fProcessStatus.fState |= kAliHLTPCConsumerErrorStateFlag;
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledComponent::SubscriptionThread::Run", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    }
// 	if ( !(fComp->fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) )
// 	    fComp->fStatus->fProcessStatus.fState |= kAliHLTPCConsumerErrorStateFlag;
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledComponent::SubscriptionThread::Run", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	}
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledComponent::SubscriptionThread::Run", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    fQuitted = true;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
