/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTDetectorRePublisher.hpp"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTEventDataType.h"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTBufferManager.hpp"
#include "AliHLTShmManager.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTEventDoneData.hpp"

// #include "AliTPCcluster.h"
// #include "AliTPCClustersArray.h"
// #include "AliTPCClustersRow.h"
// #include "AliHLTSpacePointData.h"
// #include "AliHLTTransform.h"



AliHLTDetectorRePublisher::AliHLTDetectorRePublisher( const char* name, bool sendEventDone, int slotCntExp2 ):
    AliHLTDetectorPublisher( name, slotCntExp2 )
    //fRoot( "Alil3RootFilePublisher", "A publisher of data read in from root files" )
    {
    fSendEventDone = sendEventDone;

    AliHLTMakeEventDoneData( &fDefaultEDD, AliEventID_t() );
    }

AliHLTDetectorRePublisher::~AliHLTDetectorRePublisher()
    {
    }

int AliHLTDetectorRePublisher::WaitForEvent( AliEventID_t&, AliHLTSubEventDataDescriptor*& sbevent,
					      AliHLTEventTriggerStruct*& trg )
    {
    sbevent = NULL;
    trg = NULL;
    return 0;
    }

void AliHLTDetectorRePublisher::EventFinished( AliEventID_t eventID, AliHLTSubEventDataDescriptor& sbevent, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    AliUInt32_t i, ndx;
    if ( !fBufferManager )
	{
	LOG( AliHLTLog::kError, "AliHLTDetectorRePublisher::EventFinished", "No Buffer Manager" )
	    << "No buffer manager specified." << ENDLOG;
	}
    if ( !fShmManager )
	{
	LOG( AliHLTLog::kError, "AliHLTDetectorRePublisher::EventFinished", "No Shm Manager" )
	    << "No shared memory manager specified." << ENDLOG;
	}
    for ( i = 0; i < sbevent.fDataBlockCount; i++ )
	{
	if ( fBufferManager )
	    {
	    ndx = fBufferManager->GetBufferIndex( sbevent.fDataBlocks[i].fShmID );
	    if ( ndx != ~(AliUInt32_t)0 )
		fBufferManager->ReleaseBlock( ndx, sbevent.fDataBlocks[i].fBlockOffset );
	    }
	if ( fShmManager )
	    fShmManager->ReleaseShm( sbevent.fDataBlocks[i].fShmID );
	}
    EventFinished( eventID, eventDoneData );
    }

void AliHLTDetectorRePublisher::EventFinished( AliEventID_t eventID, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    LOG( AliHLTLog::kDebug, "AliHLTDetectorRePublisher::EventFinished", "Event finished" )
	<< "Finishing event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << "). fPub: " << (fPub ? "Ok" : "NULL") << "fSendEventDone: " << (fSendEventDone ? "true" : "false") << "." << ENDLOG;
    if ( !fPub || !fSendEventDone )
	return;

    unsigned long tLength = 0;
//     unsigned long offset = 0, tWords = 0;
//     vector<AliHLTEventDoneData*>::iterator eddIter, eddEnd;
//     eddIter = eventDoneData.begin();
//     eddEnd = eventDoneData.end();
//     while ( eddIter != eddEnd )
// 	{
// 	tLength += (*eddIter)->fHeader.fLength;
// 	tWords += (*eddIter)->fDataWordCount;
// 	eddIter++;
// 	}
    AliHLTEventDoneData* edd;
    tLength = AliHLTGetMergedEventDoneDataSize( eventDoneData );
//     if ( tLength<=0 || tWords<=0 )
    if ( tLength<=sizeof(AliHLTEventDoneData) )
	{
	edd = &fDefaultEDD;
	}
    else
	{
	//edd = (AliHLTEventDoneData*)new AliUInt8_t[ tLength ];
	edd = (AliHLTEventDoneData*)fEDDCache.Get( tLength );
	if ( !edd )
	    {
	    LOG( AliHLTLog::kError, "AliHLTProcessingSubscriber::SignalCleanupInEvent", "Out of memory" )
		<< "Out of memory trying to allocate event done data for event 0x" << AliHLTLog::kHex
		<< eventID << " (" << AliHLTLog::kDec << eventID << ") of " << tLength 
		<< " bytes. Continuing without actual EventDoneData..." << ENDLOG;
	    edd = &fDefaultEDD;
	    }
	else
	    {
	    AliHLTMergeEventDoneData( eventID, edd, eventDoneData );
// 	    memcpy( edd, &fDefaultEDD, sizeof(AliHLTEventDoneData) );
// 	    edd->fHeader.fLength = tLength;
// 	    edd->fDataWordCount = tWords;
// 	    eddIter = eventDoneData.begin();
// 	    eddEnd = eventDoneData.end();
// 	    while ( eddIter != eddEnd )
// 		{
// 		memcpy( edd->fDataWords+offset, (*eddIter)->fDataWords, 
// 			(*eddIter)->fDataWordCount*sizeof((*eddIter)->fDataWords[0]) );
// 		offset += (*eddIter)->fDataWordCount;
// 		eddIter++;
// 		}
	    }
	}
    edd->fEventID = eventID;

    fPub->EventDone( *fSub, *edd );
//     if ( edd->fDataWordCount>=0 )
// 	delete [] (AliUInt8_t*)edd;
    if ( edd != &fDefaultEDD )
	fEDDCache.Release( (uint8*)edd );
    else
	edd->fEventID = AliEventID_t();
    edd = NULL;
    }

void AliHLTDetectorRePublisher::QuitEventLoop()
    {
    }

void AliHLTDetectorRePublisher::StartEventLoop()
    {
    }

void AliHLTDetectorRePublisher::EndEventLoop()
    {
    }


void AliHLTDetectorRePublisher::AnnouncedEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, 
						const AliHLTEventTriggerStruct& trigger, AliUInt32_t )
    {
    AliHLTSubEventDataDescriptor* sedd = NULL;
    if ( fDescriptors )
	{
	pthread_mutex_lock( &fSEDDMutex );
	sedd = fDescriptors->GetFreeEventDescriptor( eventID, sbevent.fDataBlockCount );
	pthread_mutex_unlock( &fSEDDMutex );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::AliHLTDetectorEventAnnouncer::AnnouncedEvent", "No descriptor handler" )
	    << "No descriptor handler configured for republisher " << GetName() << "." << ENDLOG;
	}
    if ( sedd )
	memcpy( sedd, &sbevent, sbevent.fHeader.fLength );
    else
	{
	LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::AliHLTDetectorEventAnnouncer::AnnouncedEvent", "Out of memory" )
	    << "Unable to allocate subevent data descriptor memory for event 0x" << AliHLTLog::kHex
	    << eventID << " (" << AliHLTLog::kDec << eventID << ") of " << AliHLTLog::kDec
	    << sbevent.fHeader.fLength << " bytes." << ENDLOG;
	return;
	}

    AliHLTEventTriggerStruct* ets;
    ets = (AliHLTEventTriggerStruct*)fETSCache.Get( trigger.fHeader.fLength );
    if ( ets )
	{
	ETSData ed;
	ed.fEventID = eventID;
	ed.fETS = ets;
	memcpy( ets, &trigger, trigger.fHeader.fLength );
	pthread_mutex_lock( &fETSMutex );
	fETSs.Add( ed );
	pthread_mutex_unlock( &fETSMutex );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTDetectorPublisher::AliHLTDetectorEventAnnouncer::AnnouncedEvent", "Out of memory" )
	    << "Unable to allocate memory for event trigger structure of " << AliHLTLog::kDec
	    << trigger.fHeader.fLength << " bytes for event 0x" << AliHLTLog::kHex
	    << eventID << " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
	pthread_mutex_lock( &fSEDDMutex );
	fDescriptors->ReleaseEventDescriptor( eventID );
	pthread_mutex_unlock( &fSEDDMutex );
	}
    
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
