/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

//#include "AliHLTProcessingRePublisher.hpp"
#include "AliHLTProcessingComponent.hpp"
#include "AliHLTProcessingSubscriber.hpp"

#include "AliHLTPublisherPipeProxy.hpp"
#include "AliHLTPublisherShmProxy.hpp"
#include "AliHLTLargestBlockBufferManager.hpp"
#include "AliHLTRingBufferManager.hpp"
#include "AliHLTPipeController.hpp"
#include <syslog.h>

AliHLTProcessingComponent::AliHLTProcessingComponent( char* name, int argc, char** argv, unsigned long bufferSize, unsigned long minBlockSize )
    {
    fName = name;
    fArgc = argc;
    fArgv = argv;
    fBufferSize = bufferSize;
    fMinBlockSize = minBlockSize;

    fNoRePublisher = false;

    fSubscriber = NULL;
    fRePublisher = NULL;
    fPublisher = NULL;
    fBufferManager = NULL;
    fShmManager = NULL;
    fSharedMemory = 0;
    fInDescriptorHandler = NULL;
    fOutDescriptorHandler = NULL;
    fPipeController = NULL;
    fPipeControllerQuit = 0;
    fSubscriptionThread = NULL;
    fOutputShm = ~(AliUInt32_t)0;
    fShmKey.fShmType = kInvalidShmType;
    fShmKey.fKey.fID = ~(AliUInt64_t)0;
    fMaxNoUseCount = 0;
    fMinPreBlockCount = 1;
    fMaxPreBlockCount = 1;
    fMaxPreEventCount = 0;
    fMaxPreEventCountExp2 = -1;
    fPublisherTimeout = ~(AliUInt32_t)0;
    //fPublisherName = NULL;
    //fRePublisherName = NULL;
    //fSubscriberID = NULL;
    fLogOut = NULL;
    fFileLog = NULL;
    fSysLog = NULL;
    fDoSysLog = false;
    fSysLogFacility = -1;
    fNoStdout = false;
    fSubscriberRetryCount = 0;
    fSubscriberRetryTime = 100; // milliseconds
    fNoFileLog = false;
    fSendEventDone = false;
    fBenchmark = false;
    fPub2SubProxyShmKey = fSub2PubProxyShmKey = (key_t)-1;
    fProxyShmSize = 0;
    }

AliHLTProcessingComponent::~AliHLTProcessingComponent()
    {
    if ( fSubscriber )
	delete fSubscriber;
    if ( fRePublisher )
	delete fRePublisher;
    if ( fPublisher )
	delete fPublisher;
    if ( fBufferManager )
	delete fBufferManager;
    if ( fShmManager )
	delete fShmManager;
    if ( fOutputShm != ~(AliUInt32_t)0 && fSharedMemory )
	{
	//fSharedMemory->UnlockShm( fOutputShm );
	fSharedMemory->ReleaseShm( fOutputShm );
	}
    if ( fSharedMemory )
	delete fSharedMemory;
    if ( fInDescriptorHandler )
 	delete fInDescriptorHandler;
    if ( fOutDescriptorHandler )
	delete fOutDescriptorHandler;
    if ( fPipeController )
	{
	if ( fPipeController->HasQuitted() )
	    fPipeController->Join();
	else
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTProcessingComponent::~AliHLTProcessingComponent", "Aborting PipeController Thread" )
		<< "Now forcefully aborting PipeController thread..." << ENDLOG;
	    fPipeController->Abort();
	    }
	delete fPipeController;
	}
    if ( fSubscriptionThread )
	{
	if ( fSubscriptionThread->HasQuitted() )
	    fSubscriptionThread->Join();
	else
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTProcessingComponent::~AliHLTProcessingComponent", "Aborting Subscription Thread" )
		<< "Now forcefully aborting subscription thread..." << ENDLOG;
	    fSubscriptionThread->Abort();
	    }
	delete fSubscriptionThread;
	}
    if ( fSysLog )
	{
	gLog.DelServer( fSysLog );
	delete fSysLog;
	}
    if ( fFileLog )
	{
	gLog.DelServer( fFileLog );
	delete fFileLog;
	}
    if ( fLogOut )
	{
	gLog.DelServer( fLogOut );
	delete fLogOut;
	}
    }

void AliHLTProcessingComponent::Run()
    {
    LOG( AliHLTLog::kDebug, "AliHLTProcessingComponent::Run", "Starting Run method" )
	<< "Starting AliHLTProcessingComponent Run method..." << ENDLOG;
    SetupLogging();
    if ( !ProcessCmdLine() )
	return;
    SetupFileLogging();
    SetupSysLogging();
//     if ( fRePublisherName && !fNoRePublisher )
// 	{
	fPipeController = CreatePipeController();
	if ( !fPipeController )
	    {
	    // ...
	    return;
	    }
	fPipeController->AddValue( *(AliUInt32_t*)&gLogLevel );
	fPipeController->Start();
// 	}


    if ( !fMaxPreEventCount )
	{
	if ( fMinBlockSize > 0 && fBufferSize/fMinBlockSize>0 )
	    fMaxPreEventCount = fBufferSize/fMinBlockSize+4;
	else
	    fMaxPreEventCount = 16;
	AliUInt32_t mask = 1;
	unsigned count = sizeof(mask)*8 - 1;
	mask <<= count;
	while ( ! (mask & fMaxPreEventCount) )
	    {
	    mask >>= 1;
	    count--;
	    }
	if ( count < sizeof(mask)*8 - 1 )
	    count++;
	fMaxPreEventCountExp2 = count;
	}



    fSubscriber = CreateSubscriber();
    if ( !fSubscriber )
	{
	// ...
	return;
	}
    if ( fRePublisherName.Length()>0 && !fNoRePublisher )
	{
	fRePublisher = CreateRePublisher();
	if ( !fRePublisher )
	    {
	    // ...
	    return;
	    }
	}
    else
	{
	if ( !fNoRePublisher )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTProcessingComponent::Run", "No republisher" )
		<< "No republisher specified. Resulting data will not republished." << ENDLOG;
	    }
	}
    fSharedMemory = CreateSharedMemory();
    if ( !fSharedMemory )
	{
	// ...
	return;
	}
    SetupSharedMemory();
    fBufferManager = CreateBufferManager();
    if ( !fBufferManager )
	{
	// ...
	return;
	}
    fShmManager = CreateShmManager();
    if ( !fShmManager )
	{
	// ...
	return;
	}


    fInDescriptorHandler = CreateInDescriptorHandler();
    if ( !fInDescriptorHandler )
 	{
 	// ...
 	return;
 	}
    fOutDescriptorHandler = CreateOutDescriptorHandler();
    if ( !fOutDescriptorHandler )
	{
	// ...
	return;
	}
    fPublisher = CreatePublisher();
    if ( !fPublisher )
	{
	// ...
	return;
	}
    if ( fRePublisher )
	{
	fSubscriptionThread = CreateSubscriptionThread();
	if ( !fSubscriptionThread )
	    {
	    // ...
	    return;
	    }
	}
    SetupComponents();
    StartSubscriptionLoop();

    StartProcessing();
    StopProcessing();
    if ( fPipeController )
	{
	fPipeControllerQuit = 1;
	fPipeController->Quit();
	}
    StopPublishing();
    LOG( AliHLTLog::kDebug, "AliHLTProcessingComponent::Run", "Leaving Run method" )
	<< "Leaving AliHLTProcessingComponent Run method..." << ENDLOG;
    }



AliHLTProcessingRePublisher* AliHLTProcessingComponent::CreateRePublisher()
	{
	if ( !fNoRePublisher )
	    return new AliHLTProcessingRePublisher( fRePublisherName.c_str(), fMaxPreEventCountExp2 );
	else
	    return NULL;
	}

AliHLTBufferManager* AliHLTProcessingComponent::CreateBufferManager()
	{
	//return new AliHLTLargestBlockBufferManager( fShmKey, fBufferSize );
	return new AliHLTRingBufferManager( fShmKey, fBufferSize, fMinBlockSize );
	}

AliHLTShmManager* AliHLTProcessingComponent::CreateShmManager()
	{
	return new AliHLTShmManager( fMaxNoUseCount );
	}

AliHLTSharedMemory* AliHLTProcessingComponent::CreateSharedMemory()
    {
    return new AliHLTSharedMemory;
    }

AliHLTDescriptorHandler* AliHLTProcessingComponent::CreateInDescriptorHandler()
    {
    return new AliHLTDescriptorHandler( fMaxPreEventCountExp2 );
    }

AliHLTDescriptorHandler* AliHLTProcessingComponent::CreateOutDescriptorHandler()
    {
    return new AliHLTDescriptorHandler( fMaxPreEventCountExp2 );
    }

AliHLTPublisherProxyInterface* AliHLTProcessingComponent::CreatePublisher()
	{
	if ( fProxyShmSize )
	    return new AliHLTPublisherShmProxy( fPublisherName.c_str(), fProxyShmSize, fPub2SubProxyShmKey, fSub2PubProxyShmKey );
	else
	    return new AliHLTPublisherPipeProxy( fPublisherName.c_str() );
	}

AliHLTPipeController* AliHLTProcessingComponent::CreatePipeController()
	{
	if ( !fNoRePublisher && fRePublisherName.Length()<=0 )
	    return new AliHLTPipeController( fRePublisherName.c_str() );
	else
	    {
	    MLUCString name;
	    name = fPublisherName;
	    name += "-";
	    name += fSubscriberID;
	    return new AliHLTPipeController( name.c_str() );
	    }
	}

AliHLTProcessingComponent::SubscriptionListenerThread* AliHLTProcessingComponent::CreateSubscriptionThread()
	{
	    return new SubscriptionListenerThread( fRePublisher );
	}

const char* AliHLTProcessingComponent::ProcessCmdLine( int argc, char** argv, int& i )
	{
	char* cerr = NULL;
	if ( !strcmp( argv[i], "-publisher" ) && !fNoRePublisher )
	    {
	    if ( argc <= i+1 )
		{
		return "Missing publishername parameter";
		}
	    fRePublisherName = argv[i+1];
	    i += 2;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-subscribe" ) )
	    {
	    if ( argc <= i+2 )
		{
		return "Missing publishername or subscriberid parameter";
		}
	    fPublisherName = argv[i+1];
	    fSubscriberID = argv[i+2];
	    i += 3;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-shm" ) )
	    {
	    if ( argc <= i+3 )
		{
		return "Missing shmkey or shmsize parameter";
		}
	    if ( !strcmp( argv[i+1], "bigphys" ) )
		fShmKey.fShmType = kBigPhysShmType;
	    else if ( !strcmp( argv[i+1], "physmem" ) )
		fShmKey.fShmType = kPhysMemShmType;
	    else if ( !strcmp( argv[i+1], "sysv" ) )
		fShmKey.fShmType = kSysVShmType;
	    else
		return "Invalid shm type specifier...";
	    fShmKey.fKey.fID = strtoul( argv[i+2], &cerr, 0 );
	    if ( *cerr )
		{
		return "Error converting shmkey specifier..";
		}
	    fBufferSize = strtoul( argv[i+3], &cerr, 0 );
	    if ( *cerr )
		{
		return "Error converting shmsize specifier..";
		}
	    i += 4;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc <= i+1 )
		{
		return "Missing verbosity level specifier.";
		}
	    gLogLevel = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		return "Error converting verbosity level specifier..";
		}
	    i += 2;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-timeout" ) )
	    {
	    if ( argc <= i+1 )
		{
		return "Missing publisher timeout specifier.";
		}
	    fPublisherTimeout = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		return "Error converting publisher timeout specifier..";
		}
	    i += 2;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-subscriberretrycount" ) )
	    {
	    if ( argc <= i+1 )
		{
		return "Missing susbcriber retry count specifier.";
		}
	    fSubscriberRetryCount = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		return "Error converting susbcriber retry count specifier..";
		}
	    i += 2;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-subscriberretrytime" ) )
	    {
	    if ( argc <= i+1 )
		{
		return "Missing susbcriber retry time specifier.";
		}
	    fSubscriberRetryTime = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		return "Error converting susbcriber retry time specifier..";
		}
	    i += 2;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-minblocksize" ) )
	    {
	    if ( argc <= i+1 )
		{
		return "Missing minimum block size specifier.";
		}
	    fMinBlockSize = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		return "Error converting minimum block size specifier..";
		}
	    i += 2;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-senddone" ) )
	    {
	    fSendEventDone = true;
	    i++;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-benchmark" ) )
	    {
	    fBenchmark = true;
	    i++;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-nostdout" ) )
	    {
	    fNoStdout = true;
	    i++;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-nofilelog" ) )
	    {
	    fNoFileLog = true;
	    i++;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-syslog" ) )
	    {
	    if ( argc <= i+2 )
		{
		return "Missing syslog facility parameter";
		}
	    if ( !strcmp( argv[i+1], "DAEMON" ) )
		{
		fSysLogFacility = LOG_DAEMON;
		}
	    else if ( !strcmp( argv[i+1], "LOCAL0" ) )
		{
		fSysLogFacility = LOG_LOCAL0;
		}
	    else if ( !strcmp( argv[i+1], "LOCAL1" ) )
		{
		fSysLogFacility = LOG_LOCAL1;
		}
	    else if ( !strcmp( argv[i+1], "LOCAL2" ) )
		{
		fSysLogFacility = LOG_LOCAL2;
		}
	    else if ( !strcmp( argv[i+1], "LOCAL3" ) )
		{
		fSysLogFacility = LOG_LOCAL3;
		}
	    else if ( !strcmp( argv[i+1], "LOCAL4" ) )
		{
		fSysLogFacility = LOG_LOCAL4;
		}
	    else if ( !strcmp( argv[i+1], "LOCAL5" ) )
		{
		fSysLogFacility = LOG_LOCAL5;
		}
	    else if ( !strcmp( argv[i+1], "LOCAL6" ) )
		{
		fSysLogFacility = LOG_LOCAL6;
		}
	    else if ( !strcmp( argv[i+1], "LOCAL7" ) )
		{
		fSysLogFacility = LOG_LOCAL7;
		}
	    else if ( !strcmp( argv[i+1], "USER" ) )
		{
		fSysLogFacility = LOG_USER;
		}
	    else
		{
		return "Unknown syslog facility parameter.";
		}
	    fDoSysLog = true;
	    i += 2;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-shmproxy" ) )
	    {
	    if ( argc <= i+3 )
		{
		return "Missing publisher-to-subscriber or subscriber-to-publisher proxy-shmkey or proxy-shmsize parameter";
		}
	    fProxyShmSize = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		return "Error converting proxy shmsize specifier..";
		}
	    fPub2SubProxyShmKey = (key_t)strtol( argv[i+2], &cerr, 0 );
	    if ( *cerr )
		{
		return "Error converting publisher-to-subscriber proxy-shmkey specifier..";
		}
	    fSub2PubProxyShmKey = (key_t)strtol( argv[i+3], &cerr, 0 );
	    if ( *cerr )
		{
		return "Error converting subscriber-to-publisher proxy-shmkey specifier..";
		}
	    i += 4;
	    return NULL;
	    
	    }
	return "Unknown parameter";
	}
 
bool AliHLTProcessingComponent::ProcessCmdLine()
	 {
	 int i, preI=0;
	 const char* errorStr = NULL;
	 
	 i = 1;
	 while ( (i < fArgc) && !errorStr )
	     {
	     preI = i;
	     errorStr = ProcessCmdLine( fArgc, fArgv, i );
	     }
	 if ( !errorStr )
	     {
	     if ( (fPublisherName.Length()<=0 || fSubscriberID.Length()<=0) )
		 errorStr = "Must specify -subscribe publishername subscriberid argument";
	     if ( fShmKey.fShmType == kInvalidShmType )
		 errorStr = "Must specify the -shm option with an shmkey!=-1.";
	     }
	 else
	     preI = 0;
	 if ( errorStr )
	     {
	     PrintUsage( errorStr, preI );
	     return false;
	     }
	 if ( fRePublisherName.Size()>0 )
	     fSubscriberName = fRePublisherName;
	 else
	     {
	     fSubscriberName = fPublisherName;
	     fSubscriberName += "Sub";
	     }
	 fSubscriberName += "-";
	 fSubscriberName += fSubscriberID;
	 return true;
	 }
 
void AliHLTProcessingComponent::SetupLogging()
	{
	gLog.SetID( fSubscriberName.c_str() );
	gLogLevel = AliHLTLog::kAll;
	fLogOut = new AliHLTFilteredStdoutLogServer;
	if ( !fLogOut )
	    {
	    // ...
	    }
	else
	    gLog.AddServer( fLogOut );
	}

void AliHLTProcessingComponent::SetupFileLogging()
	{
	if ( fNoFileLog )
	    return;
	MLUCString nameprefix;


	nameprefix = fSubscriberName;
	nameprefix += "-";
	fFileLog = new AliHLTFileLogServer( nameprefix.c_str(), ".log", 3000000 );
	gLog.AddServer( fFileLog );

	if ( fNoStdout && fLogOut )
	    gLog.DelServer( fLogOut );
	}

void AliHLTProcessingComponent::SetupSysLogging()
	{
	if ( !fDoSysLog )
	    return;

	fSysLog = new AliHLTSysLogServer( fSubscriberName.c_str(), fSysLogFacility );
	if ( !fSysLog )
	    {
	    // XXX
	    // ...
	    return;
	    }
	gLog.AddServer( fSysLog );

	if ( fNoStdout && fNoFileLog && fLogOut )
	    gLog.DelServer( fLogOut );
	}

void AliHLTProcessingComponent::SetupSharedMemory()
	{
	fOutputShm = fSharedMemory->GetShm( fShmKey, fBufferSize );
	}

void AliHLTProcessingComponent::StartSubscriptionLoop()
	{
	if ( fSubscriptionThread )
	    fSubscriptionThread->Start();
	}

void AliHLTProcessingComponent::StopPublishing()
    {
    if ( fRePublisher )
	{
	QuitPipeSubscriptionLoop( *fRePublisher );
	fRePublisher->CancelAllSubscriptions( true );
	}
    }

void AliHLTProcessingComponent::SetupComponents()
	{
	if ( fBenchmark )
	    fSubscriber->DoBenchmark();
	fSubscriber->SetPublisher( fPublisher );
	fSubscriber->SetRetryCount( fSubscriberRetryCount );
	fSubscriber->SetRetryTime( fSubscriberRetryTime );
	if ( fRePublisher )
	    fSubscriber->SetRePublisher( fRePublisher );
	fSubscriber->SetBufferManager( fBufferManager );
	fSubscriber->SetInDescriptorHandler( fInDescriptorHandler );
	fSubscriber->SetOutDescriptorHandler( fOutDescriptorHandler );
	fSubscriber->SetShmManager( fShmManager );

	if ( fNoRePublisher )
	    fSubscriber->NoRePublishing();

	if ( fRePublisher )
	    {
	    fRePublisher->SetMaxTimeout( fPublisherTimeout );
	    fRePublisher->SetSubscriptionLoop( &PublisherPipeSubscriptionInputLoop );
	    fRePublisher->SetSubscriber( fSubscriber );
	    }

	if ( fPipeController )
	    {
	    // TODO: Setup PipeController
	    fPipeController->SetQuitValue( fPipeControllerQuit );
	    }

	fPublisher->Subscribe( *fSubscriber );
	    
	}

void AliHLTProcessingComponent::StartProcessing()
	{
	fSubscriber->StartProcessing();

	fPublisher->StartPublishing( *fSubscriber );
	}

void AliHLTProcessingComponent::StopProcessing()
	{
	fSubscriber->StopProcessing();
	}

void AliHLTProcessingComponent::PrintUsage( const char* errorStr, int invCmdArgNdx )
	{
	if ( errorStr )
	    {
	    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
		<< "Error processing command line arguments: " << errorStr << "." << ENDLOG;
	    }
	if ( invCmdArgNdx != 0 )
	    {
	    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
		<< "Offending argument " << AliHLTLog::kDec << invCmdArgNdx << ": " 
		<< fArgv[ invCmdArgNdx ] << "." << ENDLOG;
	    }
	LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	    << "Usage: " << fArgv[0] << " <Options>" << ENDLOG;
	LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	    << "Options: " << ENDLOG;
	if ( !fNoRePublisher )
	    {
	    LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
		<< "-publisher <publishername>: Specifies the name of the outputting publisher. (Mandatory)" << ENDLOG;
	    }
	LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	    << "-subscribe <publishername> <subscriberid>: Specifies the name of the publisher to subscribe to and the subscriber id to use. (Mandatory)" << ENDLOG;
	LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	    << "-shm [bigphys|physmem|sysv] <shmkey> <shmsize>: Specifies a 32 bit shm key and the size of the corresponding shared memory segment. (Mandatory)" << ENDLOG;
	LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	    << "-V <verbosity>: Specifies the verbosity to use in the program. Set bits correspond to: 1 - benchmark, 2 - information, 4 - debug, 8 - warning, 16 - error, 32 - fatal error. (Optional)" << ENDLOG;
	LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	    << "-timeout <publisher-timeout-ms>: Specifies a timeout to use in milliseconds for published events. (Optional)" << ENDLOG;
	LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	    << "-senddone: Specify that an EventDone is sent as soon as processing is finished. Without this option wait until the next steps sends an EventDone. (Optional)" << ENDLOG;
	LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	    << "-nostdout: Inihibit logging to stdout. (Optional)" << ENDLOG;
	LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	    << "-nofilelog: Inihibit logging to files. (Optional)" << ENDLOG;
	LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	    << "-syslog <facility>: Send log messages to the syslog daemon. 'facility' can be one of the following values: DAEMON,USER,LOCAL0,LOCAL1,LOCAL2,LOCAL3,LOCAL4,LOCAL5,LOCAL6,LOCAL7. (Optional)" << ENDLOG;
	LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	    << "-shmproxy <shmsize> <publisher-to-subscriber-shmkey> <subscriber-to-publisher-shmkey>: Use shared memory proxies for publisher subscriber communication using the given parameters." << ENDLOG;
	LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	    << "-minblocksize <minimum-output-block-size>: Specify the minimum size to use for output blocks. (Optional)" << ENDLOG;
	LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	    << "-subscriberretrycount <max-retry-count>: Specify the maximum number of retries that new events are tried to be processed. (Optional)" << ENDLOG;
	LOG( AliHLTLog::kError, "Processing Component", "Command line usage" )
	    << "-subscriberretrytime <retry-time-in-milliseconds>: Specify the amount of time (in milliseconds) between of retries for new events to be processed. (Optional)" << ENDLOG;

	}





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
