#ifndef _ALIHLTDDLHEADERDATA_H_
#define _ALIHLTDDLHEADERDATA_H_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck,
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$
**
***************************************************************************
*/

#ifdef __cplusplus
extern "C" {
#endif

#define DDL_HEADER_BYTE_ORDER_LSB


#ifdef DDL_HEADER_BYTE_ORDER_LSB
#define DDLHEADERVERSIONINDEX 7
#else
#define DDLHEADERVERSIONINDEX 0
#endif
#define DDLHEADERBLOCKLENGTHWORDINDEX 0


#define DDL_HEADER_DEFAULT_VERSION 3
#define DDL_HEADER_MAX_VERSION 3

typedef volatile AliUInt8_t AliVolatileUInt8_t;
typedef volatile AliUInt16_t AliVolatileUInt16_t;
typedef volatile AliUInt32_t AliVolatileUInt32_t;
typedef volatile AliUInt64_t AliVolatileUInt64_t;

#define AliHLTGetDDLHeaderVersion( headerData ) ( ((AliVolatileUInt8_t*)headerData)[DDLHEADERVERSIONINDEX] )
#define AliHLTSetDDLHeaderVersion( headerData, version ) ( ((AliVolatileUInt8_t*)headerData)[DDLHEADERVERSIONINDEX] = (AliVolatileUInt8_t)version )

struct AliHLTDDLHeaderDataV1
    {
	AliVolatileUInt32_t fData[8];
    };

typedef AliHLTDDLHeaderDataV1 AliHLTDDLHeaderDataV2;

struct AliHLTDDLHeaderDataV3
    {
	AliVolatileUInt32_t fData[10];
    };

#define AliHLTDDLHeaderDataV1Size (8*4)
#define AliHLTDDLHeaderDataV2Size (8*4)
#define AliHLTDDLHeaderDataV3Size (10*4)

#define AliHLTGetDDLHeaderVersionSize( headerVersion ) ( (headerVersion==3) ? AliHLTDDLHeaderDataV3Size : ( (headerVersion==2) ? AliHLTDDLHeaderDataV2Size : ( (headerVersion==1) ? AliHLTDDLHeaderDataV1Size : 0xFFFFFFFF) ) )

#define AliHLTGetDDLHeaderSize(headerData)                                     \
  ((AliHLTGetDDLHeaderVersion(headerData) == 3)                                \
       ? AliHLTDDLHeaderDataV3Size                                             \
       : ((AliHLTGetDDLHeaderVersion(headerData) == 2)                         \
              ? AliHLTDDLHeaderDataV2Size                                      \
              : ((AliHLTGetDDLHeaderVersion(headerData) == 1)                  \
                     ? AliHLTDDLHeaderDataV1Size                               \
                     : 0xFFFFFFFF)))

#define AliHLTMinDDLHeaderSize (8*4)

#define AliHLTGetDDLHeaderBlockLength( headerData ) ( ((AliVolatileUInt32_t*)headerData)[ DDLHEADERBLOCKLENGTHWORDINDEX ] )
#define AliHLTSetDDLHeaderBlockLength( headerData, blockLength ) { ((AliVolatileUInt32_t*)headerData)[ DDLHEADERBLOCKLENGTHWORDINDEX ] = (AliVolatileUInt32_t)(blockLength); }

#define AliHLTGetDDLHeaderL1TriggerType( headerData ) ( ( ((AliVolatileUInt32_t*)headerData)[ 1 ] >> 14 ) & 0xFF )
#define AliHLTSetDDLHeaderL1TriggerType( headerData, L1TriggerType ) { ((AliVolatileUInt32_t*)headerData)[ 1 ] = ((((AliVolatileUInt32_t*)headerData)[ 1 ]) & 0xFFC03FFF) | (((AliVolatileUInt32_t)(L1TriggerType) & 0xFF) << 14);}

#define AliHLTGetDDLHeaderEventID1( headerData ) ( ((AliVolatileUInt16_t*)headerData)[ 2 ] & 0x0FFF )
#define AliHLTSetDDLHeaderEventID1( headerData, eventID1 ) { ((AliVolatileUInt16_t*)headerData)[ 2 ] = ( (AliVolatileUInt16_t)( (AliVolatileUInt16_t)(eventID1) & 0x0FFF ) | (((AliVolatileUInt16_t*)headerData)[ 2 ] & 0xF000) ); }

#define AliHLTGetDDLHeaderEventID2( headerData ) ( ((AliVolatileUInt32_t*)headerData)[ 2 ] & 0x00FFFFFF )
#define AliHLTSetDDLHeaderEventID2( headerData, eventID2 ) { ((AliVolatileUInt32_t*)headerData)[ 2 ]  = ( (AliVolatileUInt32_t) ( (AliVolatileUInt32_t)(eventID2) & 0x00FFFFFF ) | (((AliVolatileUInt32_t*)headerData)[ 2 ] & 0xFF000000) ); }

#define AliHLTGetDDLHeaderPartSubDet( headerData ) ( ((AliVolatileUInt32_t*)headerData)[ 3 ] & 0x00FFFFFF )
#define AliHLTSetDDLHeaderPartSubDet( headerData, partSubDet ) { ((AliVolatileUInt32_t*)headerData)[ 3 ] = (AliVolatileUInt32_t) ( ((AliVolatileUInt32_t)(partSubDet) & 0x00FFFFFF) | (((AliVolatileUInt32_t*)headerData)[ 3 ] & 0xFF000000)); }

#define AliHLTGetDDLHeaderBlockAttr( headerData ) ( ((AliVolatileUInt8_t*)headerData)[ 15 ] )
#define AliHLTSetDDLHeaderBlockAttr( headerData, blockAttr ) { ((AliVolatileUInt8_t*)headerData)[ 15 ]  = (AliVolatileUInt8_t)(blockAttr); }

#define AliHLTGetDDLHeaderMiniEventID( headerData ) ( ((AliVolatileUInt16_t*)headerData)[ 8 ] & 0x0FFF )
#define AliHLTSetDDLHeaderMiniEventID( headerData, miniEventID ) { ((AliVolatileUInt16_t*)headerData)[ 8 ]  = (AliVolatileUInt16_t)( (miniEventID) & 0x0FFF ) | (((AliVolatileUInt16_t*)headerData)[ 8 ] & 0xF000); }

#define AliHLTGetDDLHeaderStatusErrorMask( headerData ) ( (AliHLTGetDDLHeaderVersion( headerData )==3) ? 0xFFFFF000 : 0x0FFFF000 )
#define AliHLTGetDDLHeaderStatusError( headerData ) ( (((AliVolatileUInt32_t*)headerData)[ 4 ] & AliHLTGetDDLHeaderStatusErrorMask( headerData )) >> 12 )
#define AliHLTSetDDLHeaderStatusError( headerData, statusError ) { ((AliVolatileUInt32_t*)headerData)[ 4 ] = (AliVolatileUInt32_t)( ((statusError) << 12) & AliHLTGetDDLHeaderStatusErrorMask( headerData )) | (((AliVolatileUInt32_t*)headerData)[ 4 ] & 0x00000FFF); }

#define AliHLTGetDDLHeaderTriggerClassesLow( headerData ) ( ((AliVolatileUInt32_t*)headerData)[ 5 ] )
#define AliHLTSetDDLHeaderTriggerClassesLow( headerData, triggerClassesLow ) { ((AliVolatileUInt32_t*)headerData)[ 5 ] = (triggerClassesLow); }

#define AliHLTGetDDLHeaderTriggerClassesMiddleLow( headerData ) ( ((AliVolatileUInt32_t*)headerData)[ 6 ] & 0x0003FFFF )
#define AliHLTSetDDLHeaderTriggerClassesMiddleLow( headerData, triggerClassesHigh ) { \
  ((AliVolatileUInt32_t*)headerData)[ 6 ] = (((AliVolatileUInt32_t*)headerData)[ 6 ] & 0xFFFC0000) | (AliVolatileUInt32_t)( (triggerClassesHigh) & 0x0003FFFF ); \
}

#define AliHLTGetDDLHeaderTriggerClassesMiddleHigh( headerData ) ( (((AliVolatileUInt32_t*)headerData)[ 6 ] >> 18 ) | ((((AliVolatileUInt32_t*)headerData)[ 7 ] & 0x0003FFFF) << 14) )
#define AliHLTSetDDLHeaderTriggerClassesMiddleHigh( headerData, triggerClassesLow ) { \
  ((AliVolatileUInt32_t*)headerData)[ 6 ] = (((AliVolatileUInt32_t*)headerData)[ 6 ] & 0x0003FFFF) | ((triggerClassesLow & 0x00003FFF) << 18); \
  ((AliVolatileUInt32_t*)headerData)[ 7 ] = (((AliVolatileUInt32_t*)headerData)[ 7 ] & 0xFFFC0000) | ((triggerClassesLow >> 14) & 0x0003FFFF); \
}

#define AliHLTGetDDLHeaderTriggerClassesHigh( headerData ) ( ((((AliVolatileUInt32_t*)headerData)[ 7 ] >> 18) & 0x00003FFF) | ((((AliVolatileUInt32_t*)headerData)[ 8 ] & 0x0000000F) << 14) )
#define AliHLTSetDDLHeaderTriggerClassesHigh( headerData, triggerClassesHigh ) { \
  ((AliVolatileUInt32_t*)headerData)[ 7 ] = (((AliVolatileUInt32_t*)headerData)[ 7 ] & 0x0003FFFF) | ((AliVolatileUInt32_t)( (triggerClassesHigh & 0x00003FFF) << 18)); \
  ((AliVolatileUInt32_t*)headerData)[ 8 ] = (((AliVolatileUInt32_t*)headerData)[ 8 ] & 0xFFFFFFF0) | ((AliVolatileUInt32_t)( (triggerClassesHigh >> 14) & 0x0000000F)); \
}

#define AliHLTGetDDLHeaderTriggerClassesLow50( headerData ) ( (((AliVolatileUInt64_t)AliHLTGetDDLHeaderTriggerClassesMiddleLow(headerData)) << 32) | AliHLTGetDDLHeaderTriggerClassesLow(headerData) )
#define AliHLTSetDDLHeaderTriggerClassesLow50( headerData, triggerClasses ) { \
    AliHLTSetDDLHeaderTriggerClassesMiddleLow( headerData, (AliVolatileUInt32_t)( ((triggerClasses) & 0xFFFFFFFF00000000ULL) >> 32 ) ); \
    AliHLTSetDDLHeaderTriggerClassesLow( headerData, (AliVolatileUInt32_t)( (triggerClasses) & 0x00000000FFFFFFFFULL ) ); \
}

#define AliHLTGetDDLHeaderTriggerClassesHigh50( headerData ) ( (AliHLTGetDDLHeaderVersion( headerData )==3) ? (((AliVolatileUInt64_t)AliHLTGetDDLHeaderTriggerClassesHigh(headerData)) << 32) | AliHLTGetDDLHeaderTriggerClassesMiddleHigh(headerData) : 0ULL )
#define AliHLTSetDDLHeaderTriggerClassesHigh50( headerData, triggerClasses ) { if(AliHLTGetDDLHeaderVersion( headerData )==3) {AliHLTSetDDLHeaderTriggerClassesHigh( headerData, (AliVolatileUInt32_t)( ((triggerClasses) & 0xFFFFFFFF00000000ULL) >> 32 ) ); AliHLTSetDDLHeaderTriggerClassesMiddleHigh( headerData, (AliVolatileUInt32_t)( (triggerClasses) & 0x00000000FFFFFFFFULL ) ); }}

#define AliHLTGetDDLHeaderROILowByte( headerData ) ( (AliHLTGetDDLHeaderVersion( headerData )==3) ? 35 : 27 )
#define AliHLTGetDDLHeaderROILow( headerData ) ( (((AliVolatileUInt8_t*)headerData)[ (AliHLTGetDDLHeaderROILowByte( headerData )) ] & 0xF0) >> 4 )
#define AliHLTSetDDLHeaderROILow( headerData, roiLow ) { ((AliVolatileUInt8_t*)headerData)[ AliHLTGetDDLHeaderROILowByte( headerData ) ] = (AliVolatileUInt8_t)( (AliVolatileUInt8_t)((roiLow) << 4) & 0xF0); }

#define AliHLTGetDDLHeaderROIHighWord( headerData ) ((AliHLTGetDDLHeaderVersion( headerData )==3) ? 9 : 7)
#define AliHLTGetDDLHeaderROIHigh( headerData ) ( ((AliVolatileUInt32_t*)headerData)[ AliHLTGetDDLHeaderROIHighWord( headerData ) ] )
#define AliHLTSetDDLHeaderROIHigh( headerData, roiHigh ) { ((AliVolatileUInt32_t*)headerData)[ AliHLTGetDDLHeaderROIHighWord( headerData ) ] = (AliVolatileUInt32_t)(roiHigh); }

#define AliHLTGetDDLHeaderROI( headerData ) ( (((AliVolatileUInt64_t)AliHLTGetDDLHeaderROIHigh(headerData)) << 4) | AliHLTGetDDLHeaderROILow(headerData) )
#define AliHLTSetDDLHeaderROI( headerData, roi ) { AliHLTSetDDLHeaderROIHigh( headerData, (AliVolatileUInt32_t)( ((roi) & 0x0000000FFFFFFFF0ULL) >> 4 ) ); AliHLTSetDDLHeaderROILow( headerData, (AliVolatileUInt32_t)( (roi) & 0x000000000000000FULL ) ); }


#define AliHLTSetDDLHeaderEventID( headerData, eventID ) { AliHLTSetDDLHeaderEventID1(headerData,(eventID)&0x0FFF);AliHLTSetDDLHeaderMiniEventID(headerData,(eventID)&0x0FFF);AliHLTSetDDLHeaderEventID2(headerData,((eventID) >> 12)&0x00FFFFFF); }

    /** CDHv3 MBZs:
     * - Word1, bits 23-22 + 13-12
     * - Word8, bits 27-4
     **/
#define AliHLTGetDDLHeaderMBZv3(headerData)                                    \
  ((((((AliVolatileUInt32_t *)headerData)[1] >> 12) & 3) << 26) |              \
   (((((AliVolatileUInt32_t *)headerData)[1] >> 22) & 3) << 24) |              \
   ((((AliVolatileUInt32_t *)headerData)[8] >> 4) & 0xFFFFFF))

    /** CDHv2 MBZs:
     * - Word1, bits 23-22 + 13-12
     * - Word2, bits 31-24
     * - Word4, bits 31-28
     * - Word6, bits 27-18
     **/
#define AliHLTGetDDLHeaderMBZv2(headerData)                                    \
  ((((((AliVolatileUInt32_t *)headerData)[1] >> 12) & 3) << 22) |              \
   (((((AliVolatileUInt32_t *)headerData)[1] >> 22) & 3) << 20) |              \
   (((((AliVolatileUInt32_t *)headerData)[2] >> 24) & 0xFF) << 18) |           \
   (((((AliVolatileUInt32_t *)headerData)[4] >> 28) & 0xF) << 10) |            \
   ((((AliVolatileUInt32_t *)headerData)[6] >> 18) & 0x3FF))

#define AliHLTGetDDLHeaderMBZ(headerData)                                      \
  ((AliHLTGetDDLHeaderVersion(headerData) == 3)                                \
       ? AliHLTGetDDLHeaderMBZv3(headerData)                                   \
       : ((AliHLTGetDDLHeaderVersion(headerData) == 2)                         \
              ? AliHLTGetDDLHeaderMBZv2(headerData)                            \
              : ((AliHLTGetDDLHeaderVersion(headerData) == 1) ? 0              \
                                                              : 0xFFFFFFFF)))

#ifdef __cplusplus
}
#endif

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$
**
***************************************************************************
*/

#endif /* _ALIHLTDDLHEADERDATA_H_ */
