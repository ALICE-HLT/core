#ifndef _ALIHLTDDLHEADER_HPP_
#define _ALIHLTDDLHEADER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck,
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliEventID.h"

class AliHLTDDLHeader
    {
    public:

	AliHLTDDLHeader();

	bool Set( AliUInt32_t* headerData );

	AliUInt32_t GetBlockLength();
	bool SetBlockLength( AliUInt32_t length );
	AliUInt8_t GetVersion();
	AliEventID_t GetEventID();
	AliUInt32_t GetHeaderSize();
	AliUInt32_t GetWord( unsigned long n );
	AliUInt32_t GetL1TriggerType();
        AliUInt32_t GetMBZ();

    protected:

	AliUInt32_t* fHeaderData;

    };


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$
**
***************************************************************************
*/

#endif /* _ALIHLTDDLHEADER_HPP_ */
