/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck,
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$
**
***************************************************************************
*/

#include "AliHLTDDLHeader.hpp"
#include "AliHLTDDLHeaderData.h"
#include <stdio.h>

//#define DDL_HEADER_BYTE_ORDER_LSB
#ifdef DDL_HEADER_BYTE_ORDER_LSB
#define ntohl
#define htonl
#else
#include <netinet/in.h>
#endif


AliHLTDDLHeader::AliHLTDDLHeader()
    {
    fHeaderData = NULL;
    }

bool AliHLTDDLHeader::Set( AliUInt32_t* headerData )
    {
    if ( AliHLTGetDDLHeaderVersion( headerData ) > DDL_HEADER_MAX_VERSION )
	return false;

    unsigned long s = AliHLTGetDDLHeaderSize( headerData );
    if ( s == 0xFFFFFFFF )
	return false;
//     fHeaderData = new AliUInt32_t[ s / sizeof(AliUInt32_t) ];
//     if ( !fHeaderData )
// 	return false;
    fHeaderData = headerData;
//     for ( n = 0; n < s/sizeof(AliUInt32_t); n++ )
// 	fHeaderData[n] = headerData[n];
    return true;
    }

AliUInt32_t AliHLTDDLHeader::GetBlockLength()
    {
    if ( !fHeaderData || AliHLTGetDDLHeaderVersion( fHeaderData )>DDL_HEADER_MAX_VERSION )
	return 0xFFFFFFFF;
    return ntohl(fHeaderData[0]);
    }

bool AliHLTDDLHeader::SetBlockLength( AliUInt32_t length )
    {
    if ( !fHeaderData || AliHLTGetDDLHeaderVersion( fHeaderData )>DDL_HEADER_MAX_VERSION )
	return false;
    fHeaderData[0] = htonl( length );
    return true;
    }

AliUInt8_t AliHLTDDLHeader::GetVersion()
    {
    if ( !fHeaderData )
	return 0xFF;
    return AliHLTGetDDLHeaderVersion( fHeaderData );
    }

AliEventID_t AliHLTDDLHeader::GetEventID()
    {
    if ( !fHeaderData || AliHLTGetDDLHeaderVersion( fHeaderData )>DDL_HEADER_MAX_VERSION )
	return AliEventID_t();
    AliEventID_t eventID;
    eventID.fNr = AliHLTGetDDLHeaderEventID2( fHeaderData );
    eventID.fNr <<= 12;
    eventID.fNr |= AliHLTGetDDLHeaderEventID1( fHeaderData );
    return eventID;
    }

AliUInt32_t AliHLTDDLHeader::GetHeaderSize()
    {
    if ( !fHeaderData || AliHLTGetDDLHeaderVersion( fHeaderData )>DDL_HEADER_MAX_VERSION )
	return 0xFFFFFFFF;
    return AliHLTGetDDLHeaderSize( fHeaderData );
    }

AliUInt32_t AliHLTDDLHeader::GetL1TriggerType()
    {
    if ( !fHeaderData || AliHLTGetDDLHeaderVersion( fHeaderData )>DDL_HEADER_MAX_VERSION )
	return 0xFFFFFFFF;
    return AliHLTGetDDLHeaderL1TriggerType( fHeaderData );
    }


AliUInt32_t AliHLTDDLHeader::GetWord( unsigned long n )
    {
    return ntohl( fHeaderData[n] );
    }

AliUInt32_t AliHLTDDLHeader::GetMBZ()
    {
    if (!fHeaderData || AliHLTGetDDLHeaderVersion(fHeaderData) > DDL_HEADER_MAX_VERSION )
	return 0xFFFFFFFF;
    return AliHLTGetDDLHeaderMBZ(fHeaderData);
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$
**
***************************************************************************
*/
