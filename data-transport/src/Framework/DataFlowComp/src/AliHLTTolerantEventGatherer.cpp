/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTolerantEventGatherer.hpp"
#include "AliHLTEventGathererSubscriber.hpp"
#include "AliHLTTolerantRoundRobinEventScattererCmd.hpp"
#include "AliHLTSCCommands.hpp"
#include "AliHLTLog.hpp"
#include <string.h>

AliHLTTolerantEventGatherer::AliHLTTolerantEventGatherer( int slotcnt, int eventDoneBackLogSlotCnt ):
    fEventDataCache( (slotcnt<4) ? 4 : slotcnt ),
    fEventDoneBackLog( (eventDoneBackLogSlotCnt<0) ? -1 : eventDoneBackLogSlotCnt ), 
    fEventData( slotcnt ),
    fCommandProcessor( *this ),
    fSignal( slotcnt, true )
    {
    pthread_mutex_init( &fSubscriberMutex, NULL );
    pthread_mutex_init( &fEventMutex, NULL );
    fDummyTrigger.fHeader.fLength = sizeof(AliHLTEventTriggerStruct);
    fDummyTrigger.fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
    fDummyTrigger.fHeader.fSubType.fID = 0;
    fDummyTrigger.fHeader.fVersion = 1;
    fDummyTrigger.fDataWordCount = 0;
    if ( eventDoneBackLogSlotCnt<0 )
	{
	fEventDoneBackLogSize = -eventDoneBackLogSlotCnt;
	}
    else
	{
	fEventDoneBackLogSize = fEventDoneBackLog.GetSize();
	}
    LOG( AliHLTLog::kDebug, "AliHLTTolerantEventGatherer::AliHLTTolerantEventGatherer", "Backlog" )
	<< AliHLTLog::kDec << "Backlog size: " << fEventDoneBackLogSize
	<< " - cnt: " << fEventDoneBackLog.GetCnt() << " - spec. size: " << eventDoneBackLogSlotCnt << "." << ENDLOG;
    fStatus = NULL;
    fLAMProxy = NULL;
    }

AliHLTTolerantEventGatherer::~AliHLTTolerantEventGatherer()
    {
    pthread_mutex_destroy( &fSubscriberMutex );
    pthread_mutex_destroy( &fEventMutex );
    }


void AliHLTTolerantEventGatherer::AddSubscriber( AliHLTEventGathererSubscriber* subscriber )
    {
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantEventGatherer::AddSubscriber", "Mutex lock error" )
	    << "Error locking subscriber mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    fSubscribers.insert( fSubscribers.end(), subscriber );
    fToleranceHandler.Add();
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantEventGatherer::AddSubscriber", "Mutex unlock error" )
	    << "Error unlocking subscriber mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    }

void AliHLTTolerantEventGatherer::SetupCommandProcessor( AliHLTSCInterface* interface )
    {
    if ( interface )
	interface->AddCommandProcessor( &fCommandProcessor );
    }

bool AliHLTTolerantEventGatherer::WaitForEvent( AliEventID_t& eventID, EventGathererData*& eventData )
    {
    fQuitWaiting = false;
    if ( fSubscribers.size() <=0 )
	{
	eventData = NULL;
	return false;
	}
    fSignal.Lock();
    do
	{
	while ( !fQuitWaiting && fSignal.HaveNotificationData() )
	    {
	    EventGathererData* tmpED = fSignal.PopNotificationData();
	    if ( tmpED )
		{
		fSignal.Unlock();
		eventData = tmpED;
		eventID = tmpED->fEventID;
		return true;
		}
	    }
	if ( !fQuitWaiting )
	    fSignal.Wait();
	}
    while ( !fQuitWaiting );
    eventData = NULL;
    fSignal.Unlock();
    return false;
    }

void AliHLTTolerantEventGatherer::StopWaiting()
    {
    fQuitWaiting = true;
    fSignal.Signal();
    }


void AliHLTTolerantEventGatherer::EventDone( AliHLTEventDoneData* ed )
    {
    if ( !ed )
	{
	LOG( AliHLTLog::kError, "AliHLTtolerantEventGatherer::EventDone", "NULL pointer" )
	    << "NULL pointer EventDone data passed." << ENDLOG;
	return;
	}
    AliEventID_t eventID = ed->fEventID;
    LOG( AliHLTLog::kDebug, "AliHLTTolerantEventGatherer::EventDone", "Event done" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
	<< ") finished..." << ENDLOG;
    int ret;
    //vector<EventData*>::iterator eventIter, eventEnd;
    EventGathererData** eventIter;
    EventGathererData* edp;
    bool found = false;
    ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantEventGatherer::EventDone", "Mutex lock error" )
	    << "Error locking event mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    unsigned long ndx, subNdx = ~(unsigned long)0;
    AliHLTEventGathererSubscriber* sub = NULL;
    if ( fEventData.FindElement( &FindEventDataByID, (uint64)eventID, ndx ) )
	{
	eventIter = fEventData.GetPtr( ndx );
	edp = *eventIter;
	sub = edp->fSubscriber;
	subNdx = edp->fSubscriberNdx;
	if ( !fToleranceHandler.IsOk( subNdx ) )
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTTolerantEventGatherer::EventDone", "Event done postponed" )
		<< "EventDone for 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		<< ") postponed. Publisher " << subNdx << " is down." << ENDLOG;
	    found = false;
	    edp->fDone = true;
	    edp->fEDD = (AliHLTEventDoneData*)new AliUInt8_t[ ed->fHeader.fLength ];
	    if ( !edp->fEDD )
		{
		LOG( AliHLTLog::kError, "AliHLTTolerantEventGatherer::EventDone", "Out of memory" )
		    << "Out of memory trying to allocate event done data of " << AliHLTLog::kDec
		    << ed->fHeader.fLength << " bytes." << ENDLOG;
		}
	    else
		memcpy( edp->fEDD, ed, ed->fHeader.fLength );
	    }
	else
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTTolerantEventGatherer::EventDone", "Event done sent" )
		<< "EventDone for 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		<< ") sent to Publisher " << subNdx << "." << ENDLOG;
	    //delete edp;
	    
	    fEventData.Remove( ndx );
	    found = true;
	    }
	}
    else
	{
	// XXX
	// Warning
	}
    LOG( AliHLTLog::kDebug, "AliHLTTolerantEventGatherer::EventDone", "Backlog 1" )
	<< AliHLTLog::kDec << "Backlog size: " << fEventDoneBackLogSize
	<< " - cnt: " << fEventDoneBackLog.GetCnt() << "." << ENDLOG;
    EventDoneBackLogData edbld;
    if ( fEventDoneBackLog.GetCnt() >= fEventDoneBackLogSize )
	{
	edbld = fEventDoneBackLog.GetFirst();
	fEventDoneBackLog.RemoveFirst();
	if ( edbld.fEDD )
	    delete [] (AliUInt8_t*)edbld.fEDD;
	}
    edbld.fEventID = eventID;
    edbld.fEDD = (AliHLTEventDoneData*)new AliUInt8_t[ ed->fHeader.fLength ];
    if ( !edbld.fEDD )
	{
	LOG( AliHLTLog::kError, "AliHLTTolerantEventGatherer::EventDone", "Out of memory" )
	    << "Out of memory trying to allocate event done data (for backlog) of " 
	    << AliHLTLog::kDec << ed->fHeader.fLength << " bytes." << ENDLOG;
	}
    else
	memcpy( edbld.fEDD, ed, ed->fHeader.fLength );
    fEventDoneBackLog.Add( edbld );
    LOG( AliHLTLog::kDebug, "AliHLTTolerantEventGatherer::EventDone", "Backlog 2" )
	<< AliHLTLog::kDec << "Backlog cnt: " << fEventDoneBackLog.GetCnt() << "." << ENDLOG;

//     eventIter = fEventData.begin();
//     eventEnd = fEventData.end();
//     while ( eventIter != eventEnd )
// 	{
// 	if ( eventIter && edp->fEventID == eventID )
// 	    {
// 	    sub = edp->fSubscriber;
// 	    edp->fDataBlocks.clear();
// 	    //delete edp;
// 	    fEventDataCache.Release( (uint8*)edp );
// 	    fEventData.erase( eventIter );
// 	    found = true;
// 	    break;
// 	    }
// 	eventIter++;
// 	}
    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantEventGatherer::EventDone", "Mutex unlock error" )
	    << "Error unlocking event mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    if ( found )
	{
	if ( sub )
	    {
	    ret = sub->EventDone( ed );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTTolerantEventGatherer::EventDone", "Error sending event done" )
		    << "Error sending event done for event 0x" << AliHLTLog::kHex 
		    << eventID << " (" << AliHLTLog::kDec << eventID << ") to subscriber " << subNdx
		    << ": " << strerror(ret) << " (" << ret << ")." << ENDLOG;
		found = false;
		SubscriberError( sub );
		}
	    }
	else
	    {
	    LOG( AliHLTLog::kError, "AliHLTTolerantEventGatherer::EventDone", "No subscriber for event done" )
		<< "No subscriber available to send event done for event 0x" << AliHLTLog::kHex 
		<< eventID << " (" << AliHLTLog::kDec << eventID << ") to subscriber " << subNdx
		<< "." << ENDLOG;
	    }
	if ( found )
	    {
	    if ( edp->fETS )
		delete [] (AliUInt8_t*)edp->fETS;
	    edp->fDataBlocks.clear();
	    fEventDataCache.Release( edp );
	    }
	else
	    {
	    ret = pthread_mutex_lock( &fEventMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTTolerantEventGatherer::EventDone", "Mutex lock error" )
		    << "Error locking event mutex: " << strerror(ret)
		    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
		}
	    fEventData.Add( edp );
	    ret = pthread_mutex_unlock( &fEventMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTTolerantEventGatherer::EventDone", "Mutex unlock error" )
		    << "Error unlocking event mutex: " << strerror(ret)
		    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
		}
	    }
	}
    }

unsigned long AliHLTTolerantEventGatherer::GetSubscriberNdx( AliHLTEventGathererSubscriber* subscriber )
    {
    vector<AliHLTEventGathererSubscriber*>::iterator iter, end;
    unsigned long ndx = 0;
    iter = fSubscribers.begin();
    end = fSubscribers.end();
    while ( iter != end )
	{
	if ( *iter == subscriber )
	    return ndx;
	ndx++;
	iter++;
	}
    return ~(unsigned long)0;
    }


void AliHLTTolerantEventGatherer::NewEvent( AliHLTEventGathererSubscriber* subscriber, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* ets )
    {
    int ret;
    if ( !subscriber )
	{
	LOG( AliHLTLog::kError, "AliHLTTolerantEventGatherer::NewEvent", "Subscriber NULL pointer" )
	    << "Specified subscriber pointer is NULL pointer..." << ENDLOG;
	return;
	}
    if ( !sedd )
	{
	LOG( AliHLTLog::kError, "AliHLTTolerantEventGatherer::NewEvent", "SEDD NULL pointer" )
	    << "Specified sub-event data descriptor pointer is NULL pointer..." << ENDLOG;
	return;
	}
    ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantEventGatherer::NewEvent", "Mutex lock error" )
	    << "Error locking event mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    unsigned long ndx;
    EventGathererData** eventIter;
    EventGathererData *ed;
    bool alreadyDone = false;
    //AliEventID_t doneEventID = ~(AliEventID_t)0;
    AliHLTEventDoneData* doneData = NULL;
    unsigned long newNdx = GetSubscriberNdx( subscriber );
    if ( fEventData.FindElement( &FindEventDataByID, (uint64)sedd->fEventID, ndx ) )
	{
	    
	ed = fEventData.Get( ndx );

	LOG( AliHLTLog::kInformational, "AliHLTTolerantEventGatherer::NewEvent", "Duplicate Event" )
	    << "Event 0x" << AliHLTLog::kHex << sedd->fEventID << " (" << AliHLTLog::kDec
	    << sedd->fEventID << ") received again. Set Subscriber from " << ed->fSubscriberNdx
	    << " to " << newNdx << "." << ENDLOG;

	ed->fSubscriber = subscriber;
	ed->fSubscriberNdx = newNdx;
	if ( ed->fDone )
	    {
	    alreadyDone = true;
	    //doneEventID = sedd->fEventID;
	    doneData = ed->fEDD;
	    LOG( AliHLTLog::kInformational, "AliHLTTolerantEventGatherer::NewEvent", "Duplicate Event Done" )
		<< "Duplicate Event 0x" << AliHLTLog::kHex << sedd->fEventID << " (" << AliHLTLog::kDec
		<< sedd->fEventID << ") already done, sending EventDone to new publisher." << ENDLOG;
	    }
	}
    else if ( fEventDoneBackLog.FindElement( &FindEventBackLogByID, (uint64)sedd->fEventID, ndx ) )
	{
	LOG( AliHLTLog::kInformational, "AliHLTTolerantEventGatherer::NewEvent", "Duplicate Event (Done)" )
	    << "Event 0x" << AliHLTLog::kHex << sedd->fEventID << " (" << AliHLTLog::kDec
	    << sedd->fEventID << ") received again and already done. Re-Sending event done now to subscriber with index " 
	    << newNdx << "." << ENDLOG;
	EventDoneBackLogData* edbld;
	edbld = fEventDoneBackLog.GetPtr( ndx );
	if ( subscriber )
	    subscriber->EventDone( edbld->fEDD );
	}
    else
	{
	//ed = new EventData;
	ed = fEventDataCache.Get();
	if ( !ed )
	    {
	    LOG( AliHLTLog::kError, "AliHLTTolerantEventGatherer::NewEvent", "Out of memory" )
		<< "Out of memory trying to allocate EventGathererData structure." << ENDLOG;
	    ret = pthread_mutex_unlock( &fEventMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTTolerantEventGatherer::NewEvent", "Mutex unlock error" )
		    << "Error unlocking event mutex: " << strerror(ret)
		    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
		}
	    return;
	    }
	ed->fEventID = sedd->fEventID;
	ed->fSubscriber = subscriber;
	ed->fDone = false;
	ed->fEDD = NULL;
	ed->fSubscriberNdx = newNdx;
	ed->fETS = (AliHLTEventTriggerStruct*)new AliUInt8_t[ ets->fHeader.fLength ];
	if ( !ed->fETS )
	    {
	    LOG( AliHLTLog::kError, "AliHLTEventGatherer::NewEvent", "Out of memory" )
		<< "Out of memory trying to allocate event trigger struct data of "
		<< AliHLTLog::kDec << ets->fHeader.fLength << " bytes." << ENDLOG;
	    }
	else
	    memcpy( ed->fETS, ets, ets->fHeader.fLength );
	LOG( AliHLTLog::kDebug, "AliHLTTolerantEventGatherer::NewEvent", "New Event" )
	    << "Event 0x" << AliHLTLog::kHex << sedd->fEventID << " (" << AliHLTLog::kDec
	    << sedd->fEventID << ") subscriber index: " << ed->fSubscriberNdx << "." << ENDLOG;
	//eventIter = fEventData.insert( fEventData.end(), ed );
	fEventData.Add( ed, ndx );
	eventIter = fEventData.GetPtr( ndx );
	subscriber->ProcessSEDD( sedd, (*eventIter)->fDataBlocks );
	fSignal.AddNotificationData( *eventIter );
	fSignal.Signal();
	}

    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantEventGatherer::NewEvent", "Mutex unlock error" )
	    << "Error unlocking event mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    if ( alreadyDone )
	{
	if ( doneData )
	    {
	    EventDone( doneData );
	    delete [] (AliUInt8_t*)doneData;
	    }
	else
	    LOG( AliHLTLog::kFatal, "AliHLTTolerantEventGatherer::NewEvent", "Internal Error" )
		<< "Done Data on already done event 0x" << AliHLTLog::kHex << sedd->fEventID
		<< " (" << AliHLTLog::kDec << sedd->fEventID << ") not available." << ENDLOG;
	}
    }


void AliHLTTolerantEventGatherer::SubscriptionCanceled( AliHLTEventGathererSubscriber* subscriber )
    {
    int ret;
    vector<AliHLTEventGathererSubscriber*>::iterator subIter, subEnd;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantEventGatherer::SubscriptionCanceled", "Mutex lock error" )
	    << "Error locking subscriber mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    while ( subIter != subEnd )
	{
	if ( (*subIter) == subscriber ) 
	    {
	    fSubscribers.erase( subIter );
	    if ( fSubscribers.size() <= 0 )
		{
		fQuitWaiting = true;
		fSignal.Signal();
		}
	    break;
	    }
	subIter++;
	}
    
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantEventGatherer::SubscriptionCanceled", "Mutex unlock error" )
	    << "Error unlocking subscriber mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    }

void AliHLTTolerantEventGatherer::SubscriberError( AliHLTEventGathererSubscriber* subscriber )
    {
    if ( !subscriber )
	return;
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::SubscriberError", "Mutex lock error" )
	    << "Error locking status mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}

    unsigned long ndx = 0;
    vector<AliHLTEventGathererSubscriber*>::iterator iter, end;
    iter = fSubscribers.begin();
    end = fSubscribers.end();
    while ( iter != end )
	{
	if ( *iter == subscriber )
	    break;
	ndx++;
	iter++;
	}
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::SubscriberError", "Mutex unlock error" )
	    << "Error unlocking status mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    LOG( AliHLTLog::kError, "AliHLTTolerantRoundRobinEventScatterer::SubscriberError", "Subscriber Down" )
	<< "Subscriber with index " << AliHLTLog::kDec << ndx << " is down." << ENDLOG;
    if ( iter != end )
	{
	if ( fStatus )
	    {
	    if ( ndx < fStatus->fChannelStatus.fChannelsCnt )
		{
		fStatus->fChannelStatus.fChannels[ndx] = 0;
		if ( fLAMProxy )
		    fLAMProxy->LookAtMe();
		}
	    }
	}
    return;
    }


void AliHLTTolerantEventGatherer::SetSubscriber( AliHLTEventGathererSubscriber* subscriber, bool up, AliEventID_t )
    {
    if ( !subscriber )
	return;
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantEventGatherer::SetSubscriber", "Mutex unlock error" )
	    << "Error unlocking status mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}

    unsigned long ndx = 0; // i, n, cnt, ndx=0;
    bool doSet = false;
    vector<AliHLTEventGathererSubscriber*>::iterator iter, end;
    iter = fSubscribers.begin();
    end = fSubscribers.end();
    while ( iter != end )
	{
	if ( *iter == subscriber )
	    break;
	ndx++;
	iter++;
	}
    if ( iter != end )
	{
	if ( fToleranceHandler.IsOk( ndx ) == up )
	    {
	     LOG( AliHLTLog::kDebug, "AliHLTTolerantEventGatherer::SetSubscriber", "Setting Publisher" )
	         << "Subscriber '" << subscriber->GetName() << "' (" << AliHLTLog::kDec << ndx << ") already set to " << (up ? "up" : "down") << "." << ENDLOG;
	    }
	else
	    { 
	    LOG( AliHLTLog::kDebug, "AliHLTTolerantEventGatherer::SetSubscriber", "Setting Publisher" )
	        << "Setting subscriber '" << subscriber->GetName() << "' (" << AliHLTLog::kDec << ndx << ") to " << (up ? "up" : "down") << "." << ENDLOG;
	    fToleranceHandler.Set( ndx, up );
	    doSet = true;
	    }
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantEventGatherer::SetSubscriber", "Subscriber not found" )
	    << "Subscriber '" << subscriber->GetName() << "' to set to " << (up ? "up" : "down") << " not found." << ENDLOG;
	}

    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantEventGatherer::SetSubscriber", "Mutex unlock error" )
	    << "Error unlocking status mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    // As the events are resend by the Scatterer anyway, this does not make much sense here.
//     if ( up && doSet )
// 	{
// 	// Look for already done events for this subscriber and send them.
// 	// XXX
// 	ret = pthread_mutex_lock( &fEventMutex );
// 	if ( ret )
// 	    {
// 	    LOG( AliHLTLog::kWarning, "AliHLTTolerantEventGatherer::SetSubscriber", "Mutex lock error" )
// 		<< "Error locking event mutex: " << strerror(ret)
// 		<< AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
// 	    }
// 	unsigned long evtNdx = 0;
// 	//vector<EventGathererData*>::iterator eventIter;
// 	EventGathererData *ed;
// 	vector<AliEventID_t> events;
// 	while ( fEventData.FindElement( evtNdx, true, &FindEventDataBySubscriberNdx, (uint64)ndx, evtNdx ) )
// 	    {
// 	    //unsigned long newNdx = GetSubscriberNdx( subscriber );
// 	    ed = fEventData.Get( evtNdx );
// 	    events.insert( events.end(), ed->fEventID );
// 	    evtNdx++;
// 	    }
// 	ret = pthread_mutex_unlock( &fEventMutex );
// 	if ( ret )
// 	    {
// 	    LOG( AliHLTLog::kWarning, "AliHLTTolerantEventGatherer::SetSubscriber", "Mutex unlock error" )
// 		<< "Error unlocking event mutex: " << strerror(ret)
// 		<< AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
// 	    }
// 	while ( events.size()>0 )
// 	    {
// 	    EventDone( *(events.begin()) );
// 	    events.erase( events.begin() );
// 	    }
// 	}
    }

void AliHLTTolerantEventGatherer::ProcessCmd( AliHLTSCCommandStruct* cmd )
    {
    AliHLTSCCommand cmdC( cmd );
    AliHLTTolerantRoundRobinEventScattererCmd edCmdC;
    if ( cmdC.GetData()->fCmd != kAliHLTSCSetPublisherStatus || cmdC.GetData()->fLength != edCmdC.GetData()->fLength )
	return; // Unknown command.
    edCmdC.SetTo( (AliHLTTolerantRoundRobinEventScattererCmdStruct*)cmd );
    int ret;
    bool up = (bool)cmd->fParam1;
    unsigned ndx = cmd->fParam0;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantEventGatherer::ProcessCmd", "Mutex lock error" )
	    << "Error locking subscriber mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    AliHLTEventGathererSubscriber* subscriber;
    if (  ndx >= fSubscribers.size() )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantEventGatherer::ProcessCmd", "Wrong Subscriber Number" )
	    << "Error number of subscriber to set " << (up ? "up" : "down") << " ("
	    << AliHLTLog::kDec << ndx << ") is larger than or equal to number of subscribers ("
	    << fSubscribers.size() << ")." << ENDLOG;
	ret = pthread_mutex_unlock( &fSubscriberMutex );
	return;
	}

    subscriber = fSubscribers[ ndx ];

    ret = pthread_mutex_unlock( &fSubscriberMutex );

    SetSubscriber( subscriber, up, edCmdC.GetData()->fLastEventID );
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
