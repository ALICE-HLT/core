/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTEventMergerRePublisher.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTEventDoneData.hpp"

AliHLTEventMergerRePublisher::AliHLTEventMergerRePublisher( const char* name, int slotCntExp2 ):
    AliHLTDetectorRePublisher( name, false, slotCntExp2 )
    {
    fMerger = NULL;
    ForwardEvents();
    CacheEventDoneDataAllocs();
    }

AliHLTEventMergerRePublisher::~AliHLTEventMergerRePublisher()
    {
    }


void AliHLTEventMergerRePublisher::CanceledEvent( AliEventID_t eventID, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    LOG( AliHLTLog::kDebug, "AliHLTEventMergerRePublisher::CanceledEvent", "Event Canceled" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
	<< ") canceled..." << ENDLOG;
    if ( fMerger )
	{
	AliHLTEventDoneData* edd, edS;
	edd = AliHLTMergeEventDoneData( eventID, eventDoneData );
	if ( !edd )
	    {
	    edd = &edS;
	    AliHLTMakeEventDoneData( edd, eventID );
	    }
	fMerger->EventDone( edd );
	if ( edd != &edS )
	    AliHLTFreeMergedEventDoneData( edd );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTEventMergerRePublisher::CanceledEvent", "No Merger" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
	<< ") could not be properly freed - No Merger object specified..." << ENDLOG;
	}
    AliHLTDetectorRePublisher::CanceledEvent( eventID, eventDoneData );
    }







/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
