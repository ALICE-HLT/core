/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTEventMergerSubscriber.hpp"
#include "AliHLTEventMerger.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTEventDoneData.hpp"
#include <errno.h>


AliHLTEventMergerSubscriber::AliHLTEventMergerSubscriber( const char* name, AliUInt32_t maxShmNoUseCount, int maxPreSubEventsExp2 ):
    AliHLTDetectorSubscriber( name, maxShmNoUseCount, maxPreSubEventsExp2 )
    {
    fMerger = NULL;
    }

AliHLTEventMergerSubscriber::~AliHLTEventMergerSubscriber()
    {
    }

// void AliHLTEventMergerSubscriber::EventDone( AliHLTEventID_t eventID )
//     {
//     }

void AliHLTEventMergerSubscriber::SetEventMerger( AliHLTEventMerger* merger )
    {
    fMerger = merger;
    }

int AliHLTEventMergerSubscriber::SubscriptionCanceled( AliHLTPublisherInterface& )
    {
    int ret=0;
    if ( fMerger )
	fMerger->SubscriptionCanceled( this );
    else
	ret = ENODEV;
    return ret;
    }


void AliHLTEventMergerSubscriber::ProcessEvent( const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* ets )
    {
    if ( !sedd )
	{
	LOG( AliHLTLog::kError, "AliHLTEventMergerSubscriber::ProcessEvent", "SEDD NULL pointer" )
	    << "Sub-Event Data Descriptor passed is NULL pointer..." << ENDLOG;
	return;
	}
    AliUInt32_t i, n = sedd->fDataBlockCount;
    for ( i = 0; i < n; i++ )
      {
	LOG( AliHLTLog::kDebug, "AliHLTEventMergerSubscriber::ProcessEvent", "Process Descriptor" )
	  << "Event Descriptor " << AliHLTLog::kDec << i << ": 0x" 
	  << AliHLTLog::kHex << sedd->fDataBlocks[i].fShmID.fKey.fID << "/" << AliHLTLog::kDec 
	  << sedd->fDataBlocks[i].fBlockOffset << "/" << sedd->fDataBlocks[i].fBlockSize << "/"
	  << AliHLTLog::kHex << sedd->fDataBlocks[i].fProducerNode << "/0x" << sedd->fDataBlocks[i].fDataType.fID
	  << " (" << AliHLTLog::kDec << sedd->fDataBlocks[i].fDataType.fID << ")." << ENDLOG;
      }
    if ( fMerger )
	fMerger->NewEvent( this, sedd, ets );
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventMergerSubscriber::ProcessEvent", "No merger object specified" )
	    << "No merger object specified yet. EventMergerSubscriber '" << GetName() << "' pretending to be done with event..."
	    << ENDLOG;
	AliHLTEventDoneData edd;
	AliHLTMakeEventDoneData( &edd, sedd->fEventID );
	EventDone( &edd );
	}
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
