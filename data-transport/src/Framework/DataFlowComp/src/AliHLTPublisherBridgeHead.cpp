/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTPublisherBridgeHead.hpp"
#include "AliHLTBridgeMsg.hpp"
#include "AliHLTBridgeBlock.hpp"
#include "AliHLTBridgeMsgs.h"
#include "AliHLTEventAccounting.hpp"
#include "AliHLTHLTEventTriggerData.hpp"
#include "AliHLTEventMetaDataOutput.hpp"
#include "BCLAbstrAddress.hpp"
#include "BCLAddressLogger.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTEventDoneData.hpp"
#include "AliHLTBridgeHelperData.hpp"
#include "AliHLTBridgeData.hpp"
#include "AliHLTBridgeBlock.hpp"
#include <errno.h>
#ifdef OUTPUT_DEBUG
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#endif


#if 0
#define TRACELOG( lvl, orig, keys ) LOG(lvl,orig,keys)
#else
#define TRACELOG( lvl, orig, keys ) LOG(AliHLTLog::kNone,orig,keys)
#endif


AliHLTPublisherBridgeHead::AliHLTPublisherBridgeHead( const char* name, int eventSlotsExp2 ):
    AliHLTSamplePublisher( name, eventSlotsExp2 ), 
    fMsgCache( 4, false, false ),
    fEventDataCache( sizeof(EventData), (eventSlotsExp2<0) ? 4 : eventSlotsExp2 ),
    fEventList( eventSlotsExp2, true ),
    fETSCache( (eventSlotsExp2<=0) ? 1 : eventSlotsExp2, true, false ),
    fTimer( (eventSlotsExp2<4) ? eventSlotsExp2 : (eventSlotsExp2-3) ),
    fRetrySignal( (eventSlotsExp2<4) ? eventSlotsExp2 : (eventSlotsExp2-3), true ), 
    fRetryThread( this, &AliHLTPublisherBridgeHead::RetryLoop ),
    fBlobLoopThread( this, &AliHLTPublisherBridgeHead::BlobLoop ),
    fIntEDDCache( (eventSlotsExp2<4) ? eventSlotsExp2 : (eventSlotsExp2-3) ),
    fEDDCache( (eventSlotsExp2<4) ? eventSlotsExp2 : (eventSlotsExp2-3), true, false ),
    fEventAccounting(NULL)
    {
    fShmCopy = false;
    fCopyDest = NULL;
    fMsgCom = NULL;
    fBlobMsgCom= NULL;
    fBlobCom = NULL;
    fCommunicationTimeout = 0;
    fGlobalTransferID = ~(BCLTransferID)0;
    fDescriptorHandler = NULL;
    fReceiveAddr = NULL;
    fRemoteMsgAddress = fRemoteBlobMsgAddress = NULL;
    pthread_mutex_init( &fEventListMutex, NULL );
    pthread_mutex_init( &fDescriptorMutex, NULL );
    pthread_mutex_init( &fMsgComMutex, NULL );
    pthread_mutex_init( &fComAccessMutex, NULL );
    pthread_mutex_init( &fComRecvMutex, NULL );
    fShmID.fShmType = kInvalidShmType;
    fShmID.fKey.fID = (AliUInt64_t)-1;
    fBlobConnected = fMsgConnected = false;
    fConnectionDesired = false;
    fRetryTime = 500; // given in milliseconds, 1/2 second.
    fMaxRetryCount = 600; // retry basically for 5 minutes (if I calculated correctly...)
    fRetryLoopQuitted = true;
    fQuitRetryLoop = false;
    fLAMProxy = NULL;
    fErrorCallback = &fDefaultErrorCallback;
    fStatus = NULL;
    fPaused = false;
    fAllEventsPurged = false;
    fAliceHLT = false;
    fRunNumber = 0;
    CacheEventDoneDataAllocs();
    fOldBCLLogSet = false;
    fOldBCLLog = 0;
    }

AliHLTPublisherBridgeHead::~AliHLTPublisherBridgeHead()
    {
    pthread_mutex_destroy( &fEventListMutex );
    pthread_mutex_destroy( &fDescriptorMutex );
    pthread_mutex_destroy( &fMsgComMutex );
    pthread_mutex_destroy( &fComAccessMutex );
    pthread_mutex_destroy( &fComRecvMutex );
    if ( fReceiveAddr )
	fMsgCom->DeleteAddress( fReceiveAddr );
    }

void AliHLTPublisherBridgeHead::SetShmCopy( bool copy, AliUInt8_t* dest )
    {
    LOG( AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::SetShmCopy", "Setting shm copy  " )
	<< "Setting shm copy data: Ptr: 0x" << AliHLTLog::kHex << reinterpret_cast<AliUInt64_t>(dest) << AliHLTLog::kDec
	<< " copy: " << (copy ? "true" : "false") << "." << ENDLOG;
    fShmCopy = copy;
    fCopyDest = dest;
    }

int AliHLTPublisherBridgeHead::Connect()
    {
    return Connect( true );
    }

int AliHLTPublisherBridgeHead::Connect( bool doLock )
    {
    if ( !fOldBCLLogSet )
	{
	fOldBCLLog = gBCLLogLevelRef;
	fOldBCLLogSet = true;
	gBCLLogLevelRef = 0x70 & gLogLevel;
	}
    if ( fBlobConnected && fMsgConnected && fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	return 0;
	}
    fConnectionDesired = true;
    if ( doLock )
	pthread_mutex_lock( &fComAccessMutex );
    if ( !fMsgCom || !fBlobCom || !fRemoteMsgAddress || !fRemoteBlobMsgAddress )
	{
	if ( doLock )
	    pthread_mutex_unlock( &fComAccessMutex );
	return ENODEV;
	}
    int ret;
    if ( !fBlobConnected )
	{
	int retries = 0;
	do
	{
	    if ( fCommunicationTimeout )
		ret = fBlobCom->Connect( fRemoteBlobMsgAddress, fCommunicationTimeout );
	    else
		ret = fBlobCom->Connect( fRemoteBlobMsgAddress );
	} while (ret == 110 && ++retries < 5);
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::Connect", "Blob Connection Error" )
		<< "Unable to connect blob communication object: " << strerror(ret) << " ("
		<< AliHLTLog::kDec << ret << ").\n"  << "Number of retries before giving up: " << retries << ENDLOG;
	    if ( doLock )
		pthread_mutex_unlock( &fComAccessMutex );
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCNetworkErrorStateFlag;
		}
	    if ( fLAMProxy )
		fLAMProxy->LookAtMe();
	    return ret;
	    }
	fBlobConnected = true;
	if ( fStatus )
	    fStatus->SetConnected( false, fBlobConnected );
	}
    if ( doLock )
	pthread_mutex_lock( &fMsgComMutex );
    if ( !fMsgConnected )
	{
	if ( fCommunicationTimeout )
	    ret = fMsgCom->Connect( fRemoteMsgAddress, fCommunicationTimeout );
	else
	    ret = fMsgCom->Connect( fRemoteMsgAddress );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::Connect", "Msg Connection Error" )
		<< "Unable to connect message communication object: " << strerror(ret) << " ("
		<< AliHLTLog::kDec << ret << ")." << ENDLOG;
	    if ( doLock )
		{
		pthread_mutex_unlock( &fMsgComMutex );
		pthread_mutex_unlock( &fComAccessMutex );
		Disconnect( true );
		}
	    else
		Disconnect( true, true, 1000, false );
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCNetworkErrorStateFlag;
		}
	    if ( fLAMProxy )
		fLAMProxy->LookAtMe();
	    return ret;
	    }
	fMsgConnected = true;
	if ( fStatus )
	    fStatus->SetConnected( true, fMsgConnected );
	}
    AliHLTBridgeMsg msg;
    AliHLTBridgeMsgStruct* pmsg;
    AliUInt8_t* tmpPtr;
    AliUInt32_t lenM, lenBM;
    unsigned long msgLen = msg.GetData()->fLength;
    BCLNetworkData::Transform( fMsgCom->GetAddress()->fLength, lenM, fMsgCom->GetAddress()->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );
    msgLen += lenM;
    BCLNetworkData::Transform( fBlobMsgCom->GetAddress()->fLength, lenBM, fBlobMsgCom->GetAddress()->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );
    msgLen += lenBM;
    pmsg = (AliHLTBridgeMsgStruct*)new AliUInt8_t[ msgLen ];
    if ( !pmsg )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::Connect", "Out of memory" )
	    << "Out of memory trying to allocate BridgeConnect msg of " << AliHLTLog::kDec
	    << msgLen << " bytes." << ENDLOG;
	pmsg = msg.GetData();
	}
    else
	{
	tmpPtr = (AliUInt8_t*)pmsg;
	memcpy( pmsg, msg.GetData(), msg.GetData()->fLength );
	tmpPtr += msg.GetData()->fLength;
	memcpy( tmpPtr, fMsgCom->GetAddress(), lenM );
	tmpPtr += lenM;
	memcpy( tmpPtr, fBlobMsgCom->GetAddress(), lenBM );
	pmsg->fLength += lenM+lenBM;
	pmsg->fEventID = lenM;
	pmsg->fDataType = lenBM;
	}
    pmsg->fMsgType = kAliHLTBridgeConnectMsg;
    if ( fCommunicationTimeout )
	ret = fMsgCom->Send( fRemoteMsgAddress, pmsg, fCommunicationTimeout );
    else
	ret = fMsgCom->Send( fRemoteMsgAddress, pmsg );
    if ( pmsg != msg.GetData() )
	delete [] (AliUInt8_t*)pmsg;
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::Connect", "Connection Message Error" )
	    << "Cannot send connection message to remote message communication object: " << strerror(ret) << " ("
	    << AliHLTLog::kDec << ret << ")." << ENDLOG;
	if ( doLock )
	    {
	    pthread_mutex_unlock( &fMsgComMutex );
	    pthread_mutex_unlock( &fComAccessMutex );
	    Disconnect( true );
	    }
	else
	    Disconnect( true, true, 1000, false );
	if ( fStatus )
	    {
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCNetworkErrorStateFlag;
	    }
	if ( fLAMProxy )
	    fLAMProxy->LookAtMe();
	return ret;
	}
    if ( doLock )
	{
	pthread_mutex_unlock( &fMsgComMutex );
	pthread_mutex_unlock( &fComAccessMutex );
	}
    if ( fErrorCallback )
	fErrorCallback->ConnectionEstablished( this );
    LOGG( AliHLTLog::kInformational, "AliHLTPublisherBridgeHead::Connect", "Connection established", tmpLog )
	<< "Connection established to ";
	BCLAddressLogger::LogAddress( tmpLog, *fRemoteMsgAddress )
	    << "/";
	BCLAddressLogger::LogAddress( tmpLog, *fRemoteBlobMsgAddress )
	    << "." << ENDLOG;
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCNetworkErrorStateFlag;
	}
    return 0;
    }

int AliHLTPublisherBridgeHead::Disconnect()
    {
    return Disconnect( true );
    }

int AliHLTPublisherBridgeHead::Disconnect( bool initiator, bool do_timeout, AliUInt32_t timeout_ms, bool doLock )
    {
    TRACELOG(AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    fConnectionDesired = false;
    if ( doLock )
	pthread_mutex_lock( &fComAccessMutex );
    TRACELOG(AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    if ( !fMsgCom || !fBlobCom || !fRemoteMsgAddress || !fRemoteBlobMsgAddress )
	{
	if ( doLock )
	    pthread_mutex_unlock( &fComAccessMutex );
	if ( fOldBCLLogSet )
	    {
	    fOldBCLLogSet = false;
	    gBCLLogLevelRef = fOldBCLLog;
	    }
	return ENODEV;
	}
    AliHLTBridgeMsg msg;
    msg.GetData()->fMsgType = kAliHLTBridgeDisconnectMsg;
    int ret;
    TRACELOG(AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    fBlobCom->ReleaseBlob( fGlobalTransferID );
    fGlobalTransferID = ~(BCLTransferID)0;
    if ( doLock )
	pthread_mutex_lock( &fMsgComMutex );
    TRACELOG(AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    if ( fMsgConnected )
	{
	if ( initiator )
	    {
	    TRACELOG(AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    if ( do_timeout )
		ret = fMsgCom->Send( fRemoteMsgAddress, msg.GetData(), timeout_ms );
	    else if ( fCommunicationTimeout )
		ret = fMsgCom->Send( fRemoteMsgAddress, msg.GetData(), fCommunicationTimeout ); // XXX
	    else
		ret = fMsgCom->Send( fRemoteMsgAddress, msg.GetData() ); // XXX
	    TRACELOG(AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    }
    TRACELOG(AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	if ( fCommunicationTimeout )
	    ret = fMsgCom->Disconnect( fRemoteMsgAddress, fCommunicationTimeout );
	else
	    ret = fMsgCom->Disconnect( fRemoteMsgAddress );
    TRACELOG(AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	fMsgConnected = false;
	if ( fStatus )
	    fStatus->SetConnected( true, fMsgConnected );
    TRACELOG(AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	}
    TRACELOG(AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    if ( doLock )
	pthread_mutex_unlock( &fMsgComMutex );
    TRACELOG(AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    if ( fBlobConnected )
	{
    TRACELOG(AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	if ( fCommunicationTimeout )
	    ret = fBlobCom->Disconnect( fRemoteBlobMsgAddress, fCommunicationTimeout );
	else
	    ret = fBlobCom->Disconnect( fRemoteBlobMsgAddress );
    TRACELOG(AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	fBlobConnected = false;
	if ( fStatus )
	    fStatus->SetConnected( false, fBlobConnected );
    TRACELOG(AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	}
    if ( doLock )
	pthread_mutex_unlock( &fComAccessMutex );
    TRACELOG(AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCNetworkErrorStateFlag;
	}
    TRACELOG(AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    if ( fOldBCLLogSet )
	{
	fOldBCLLogSet = false;
	gBCLLogLevelRef = fOldBCLLog;
	}
    return 0;
    }

int AliHLTPublisherBridgeHead::Start()
    {
    fRetryLoopQuitted = true;
    fQuitRetryLoop = false;
    fBlobLoopThread.Start();
    fRetryThread.Start();
    fTimer.Start();
    return 0;
    }

int AliHLTPublisherBridgeHead::Stop()
    {
    fTimer.Stop();
    QuitBlobLoop();
    fQuitRetryLoop = true;
    fRetrySignal.Signal();
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    gettimeofday( &start, NULL );
    while ( !fRetryLoopQuitted && deltaT<timeLimit )
	{
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( fRetryLoopQuitted )
	{
	fRetryThread.Join();
	return 0;
	}
    else
	{
	// XXX
	fRetryThread.Abort();
	return EBUSY;
	}
    }
  
void AliHLTPublisherBridgeHead::MsgLoop()
    {
    if ( !fDescriptorHandler )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherBridgeHead::MsgLoop", "No descriptor handler" )
	    << "No descriptor handler object specified. Aborting..." << ENDLOG;
	return;
	}
//     BCLAbstrAddressStruct* addr;
//     addr = fMsgCom->NewAddress();
    BCLMessageStruct* bMsg;
    AliHLTBridgeMsgStruct *msg;
    AliHLTBridgeMsg msgClass;
    AliUInt32_t msgBaseLen = msgClass.GetData()->fLength;
    fMsgLoopQuitted = false;
    fQuitMsgLoop = false;
    //vector<EventData*>::iterator iter, end;
    int ret;
    AliUInt32_t wrongMsgSizeCnt = 0;
    while ( !fQuitMsgLoop )
	{
	pthread_mutex_lock( &fComRecvMutex );
	ret = fMsgCom->Receive( fReceiveAddr, bMsg, 2000 );
	// Timeout for 2s, so that anybody else can get a 
	// regular chance at getting the fComRecvMutex.
	if ( ret && ret!=ETIMEDOUT )
	    {
	    LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::MsgLoop", "Msg Receive Error" )
		<< "Error receiving message from subscriber bridge head: " << strerror(ret)
		<< " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	    if ( ret == EMSGSIZE )
		{
		if ( wrongMsgSizeCnt++ >= 8 )
		    {
		    fMsgCom->Reset();
		    wrongMsgSizeCnt = 0;
		    }
		}
	    }
	if ( fQuitMsgLoop )
	    {
	    pthread_mutex_unlock( &fComRecvMutex );
	    break;
	    }
	if ( ret )
	    {
	    pthread_mutex_unlock( &fComRecvMutex );
	    continue;
	    }
	if ( !fReceiveAddr || !bMsg )
	    {
	    LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::MsgLoop", "Msg receive error" )
		<< "Null pointer error receiving message from subscriber bridge head: "
		<< strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	    if ( bMsg )
		fMsgCom->ReleaseMsg( bMsg );
	    pthread_mutex_unlock( &fComRecvMutex );
	    continue;
	    }
	msg = (AliHLTBridgeMsgStruct*)bMsg;
	msgClass.Adopt( msg );
	switch ( msg->fMsgType )
	    {
	    case kAliHLTBridgeConnectMsg:
		LOG( AliHLTLog::kInformational, "AliHLTPublisherBridgeHead::MsgLoop", "Connection established" )
		    << "Connection established to subscriber bridge head." << ENDLOG;
#if 0
		LOG( AliHLTLog::kImportant, "AliHLTPublisherBridgeHead::MsgLoop", "Sleeping" )
		    << "Sleeping for 10s" << ENDLOG;
		sleep( 10 );
#endif
		if ( !fMsgConnected || !fBlobConnected )
		    {
		    AliUInt32_t lenM, lenBM, lenMSend, lenBMSend;
		    lenMSend = (AliUInt32_t)msg->fEventID;
		    lenBMSend = (AliUInt32_t)msg->fDataType;
		    BCLNetworkData::Transform( fRemoteMsgAddress->fLength, lenM, fRemoteMsgAddress->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );
		    BCLNetworkData::Transform( fRemoteBlobMsgAddress->fLength, lenBM, fRemoteBlobMsgAddress->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );
		    if ( msg->fLength != msgBaseLen+lenM+lenBM )
			{
			LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::MsgLoop", "Incorrect message size" )
			    << "Connect message has incorrect message size " << AliHLTLog::kDec
			    << msg->fLength << " (" << msgBaseLen+lenM+lenBM
			    << " expected) for contained remote addresses. Using pre-defined addresses instead."
			    << ENDLOG;
			}
		    else if ( lenMSend != lenM )
			{
			LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::MsgLoop", "Incorrect remote address size" )
			    << "Connect message has incorrrect remote msg address length of " << AliHLTLog::kDec
			    << lenMSend << " (" << lenM << " expected). Using pre-defined addresses instead."
			    << ENDLOG;
			}
		    else if ( lenBMSend != lenBM )
			{
			LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::MsgLoop", "Incorrect remote address size" )
			    << "Connect message has incorrrect remote blob msg address length of " << AliHLTLog::kDec
			    << lenBMSend << " (" << lenBM << " expected). Using pre-defined addresses instead."
			    << ENDLOG;
			}
		    else
			{
			BCLAbstrAddressStruct *newMsg, *newBlobMsg;
			newMsg = (BCLAbstrAddressStruct*)(((AliUInt8_t*)msg)+msgBaseLen);
			newBlobMsg = (BCLAbstrAddressStruct*)(((AliUInt8_t*)newMsg)+lenM);
			memcpy( fRemoteMsgAddress, newMsg, lenM );
			memcpy( fRemoteBlobMsgAddress, newBlobMsg, lenBM );
			}

		    ret = Connect();
		    }
		if ( !fMsgConnected )
		    {
		    LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::MsgLoop", "Connection error" )
			<< "Unable to make msg connection to subscriber bridge head: " 
			<< strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
		    Disconnect( true );
		    }
		if ( !fBlobConnected )
		    {
		    LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::MsgLoop", "Connection error" )
			<< "Unable to make blob connection to subscriber bridge head: " 
			<< strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
		    Disconnect( true );
		    }

		break;
	    case kAliHLTBridgeGlobalTransferIDMsg:
		fGlobalTransferID = msg->fTransferID;
		LOG( AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::MsgLoop", "Transfer ID received" )
		    << "Received global transfer ID: 0x" << AliHLTLog::kHex << fGlobalTransferID
		    << " (" << AliHLTLog::kDec << fGlobalTransferID << ")." << ENDLOG;
		LOG( AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::MsgLoop", "Connection State" )
		    << "fStatus: 0x" << AliHLTLog::kHex << (unsigned long)fStatus << " - fMsgConnected: "
		    << (fMsgConnected ? "true" : "false") << " - fBlobConnected: "
		    << (fBlobConnected ? "true" : "false") << "." << ENDLOG;
		if ( fStatus )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::MsgLoop", "Connection State" )
			<< "fStatus->fProcessStatus.fState&kAliHLTPCChangingStateFlag: " << AliHLTLog::kHex
			<< (fStatus->fProcessStatus.fState&kAliHLTPCChangingStateFlag)
			<< "fStatus->fProcessStatus.fState&kAliHLTPCConnectedStateFlag: "
			<< (fStatus->fProcessStatus.fState&kAliHLTPCConnectedStateFlag)
			<< "fStatus->fProcessStatus.fState&kAliHLTPCNetworkErrorStateFlag: "
			<< (fStatus->fProcessStatus.fState&kAliHLTPCNetworkErrorStateFlag)
			<< "." << ENDLOG;
		    }
		if ( fStatus )
		    {
		    if ( fMsgConnected )
			fStatus->SetConnected( true, fMsgConnected );
		    if ( fBlobConnected )
			fStatus->SetConnected( false, fBlobConnected );
			{
			MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			if ( fStatus->fProcessStatus.fState&kAliHLTPCChangingStateFlag )
			    fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
			}
		    if ( fMsgConnected && fBlobConnected )
			{
			MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			if ( !(fStatus->fProcessStatus.fState&kAliHLTPCConnectedStateFlag) )
			    fStatus->fProcessStatus.fState |= kAliHLTPCConnectedStateFlag;
			if ( fStatus->fProcessStatus.fState&kAliHLTPCNetworkErrorStateFlag )
			    fStatus->fProcessStatus.fState &= ~kAliHLTPCConnectedStateFlag;
			}
		    else
			{
			MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			if ( fStatus->fProcessStatus.fState&kAliHLTPCConnectedStateFlag )
			    fStatus->fProcessStatus.fState &= ~kAliHLTPCConnectedStateFlag;
			}
		    }
		break;
	    case kAliHLTBridgeDisconnectMsg:
		LOG( AliHLTLog::kInformational, "AliHLTPublisherBridgeHead::MsgLoop", "Disconnect msg received" )
			<< "Received disconnect message from publisher bridge head." << ENDLOG;
		Disconnect( false );
		break;
	    case kAliHLTBridgeSubscriptionCanceledMsg:
		fQuitMsgLoop = true;
		if ( 1 )
		    {
		    LOG( AliHLTLog::kInformational, "AliHLTPublisherBridgeHead::MsgLoop", "Susbcription canceled" )
			<< "Subscription canceled. Leaving message loop." 
			<< ENDLOG;
		    }
		break;
	    case kAliHLTBridgeEventCanceledMsg:
		{
		LOG( AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::MsgLoop", "Event canceled" )
		    << "Already dispatched event 0x" << AliHLTLog::kHex << msg->fEventID
		    << " (" << AliHLTLog::kDec << msg->fEventID << ") cancelled." 
		    << ENDLOG;
		AbortEvent( AliEventID_t( msg->fEventType, msg->fEventID ), true );
		break;
		}
	    case kAliHLTBridgeReleaseEventsRequestMsg:
		LOG( AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::MsgLoop", "Event release request" )
		    << "Event release request received from subscriber bridge head." << ENDLOG;
		RequestEventRelease();
		break;
	    default:
		LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::MsgLoop", "Unknown message" )
		    << "Unknown message '" << AliHLTLog::kDec << msg->fMsgType << " (0x" 
		    << AliHLTLog::kHex << msg->fMsgType << ")' received from subscriber bridge."
		    << ENDLOG;
		break;
	    }
	fMsgCom->ReleaseMsg( bMsg );
	pthread_mutex_unlock( &fComRecvMutex );
	}
    //fMsgCom->DeleteAddress( addr );
    fMsgLoopQuitted = true;
    }

bool AliHLTPublisherBridgeHead::QuitMsgLoop()
    {
    if ( fMsgLoopQuitted )
	return true;
    fQuitMsgLoop = true;
    AliHLTBridgeMsg msg;
    msg.GetData()->fMsgType = kAliHLTBridgeEmptyMsg;
    int ret;
    BCLAbstrAddressStruct *addr;
    pthread_mutex_lock( &fComAccessMutex );
    addr = fMsgCom->NewAddress();
    if ( addr )
	{
	memcpy( addr, fMsgCom->GetAddress(),fMsgCom->GetAddress()->fLength );
	pthread_mutex_lock( &fMsgComMutex );
	ret = fMsgCom->Send( addr, msg.GetData() );
	pthread_mutex_unlock( &fMsgComMutex );
	fMsgCom->DeleteAddress( addr );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::QuitMsgLoop", "Msg send error" )
		<< "Error sending quit message to self: " << strerror(ret) << AliHLTLog::kDec
		<< " (" << ret << ")." << ENDLOG;
	    }
	}
    pthread_mutex_unlock( &fComAccessMutex );
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    gettimeofday( &start, NULL );
    while ( !fMsgLoopQuitted && deltaT<timeLimit )
	{
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    return fMsgLoopQuitted;
    }

void AliHLTPublisherBridgeHead::BlobLoop()
    {
    if ( !fDescriptorHandler )
	{
	LOG( AliHLTLog::kFatal, "AliHLTPublisherBridgeHead::BlobLoop", "No descriptor handler" )
	    << "No descriptor handler object specified. Aborting..." << ENDLOG;
	return;
	}
    fBlobLoopQuitted = false;
    fQuitBlobLoop = false;
    int ret;
    unsigned long i;
    BCLTransferID transferID;
    vector<uint64> offsets;
    vector<uint64> sizes;
    while ( !fQuitBlobLoop )
	{
	ret = fBlobCom->WaitForReceivedTransfer( transferID, offsets, sizes );
	if ( !ret && !fQuitBlobLoop )
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::BlobLoop", "New event received" )
		<< "New event received from subscriber bridge head with " 
		<< offsets.size() << " data blocks." << ENDLOG;
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCBusyStateFlag;
		}
	    AliHLTBridgeTransferSEDD* transferSEDD = NULL;
	    AliHLTSubEventDataDescriptor* currentSEDD = NULL;
	    AliUInt8_t* p;
	    p = fBlobCom->GetBlobData( transferID );
	    if ( !p )
		{
		if ( fConnectionDesired )
		    {
		    LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::BlobLoop", "Error retrieving data for received event" )
			<< "Cannot retrieve data for received event from blob communication object. Aborting event..." << ENDLOG;
		    }
		if ( fStatus )
		    {
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
		    }
		continue;
		}
	    AliUInt64_t transferOffset, destOffset = 0;
	    transferOffset = fBlobCom->GetBlobOffset( transferID );
	    EventData* ed = (EventData*)fEventDataCache.Get();
	    transferSEDD = (AliHLTBridgeTransferSEDD*)( p+offsets[0] );

	    AliHLTBridgeData bridgeData;
	    AliHLTBridgeBlock bridgeBlock;
	    bridgeData.AdoptFully( &(transferSEDD->fData) );
	    for ( i = 0; i < transferSEDD->fData.fBlockCount; i++ )
		bridgeBlock.AdoptFully( &(transferSEDD->fBlocks[i]) );
	    AliEventID_t eventID( transferSEDD->fData.fEventType, transferSEDD->fData.fEventNr );

	    LOG( AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::BlobLoop", "New event received" )
		<< "New event received is 0x" << AliHLTLog::kHex << transferSEDD->fData.fEventType
		<< "/" << transferSEDD->fData.fEventNr << " (" << AliHLTLog::kDec
		<< transferSEDD->fData.fEventType << "/" << transferSEDD->fData.fEventNr
		<< ") - Event data blocks:" << transferSEDD->fData.fBlockCount << "." 
		<< ENDLOG;
	    LOG( AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::BlobLoop", "Event transfer descriptor" )
		<< "Event 0x" << AliHLTLog::kHex << transferSEDD->fData.fEventNr
		<< " (" << AliHLTLog::kDec << transferSEDD->fData.fEventNr << ") transfer descriptor offset: "
		<< AliHLTLog::kDec << offsets[0] << " (0x" << AliHLTLog::kHex << offsets[0] << ")." 
		<< ENDLOG;
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		if ( eventID.fType!=kAliEventTypeTick && eventID.fType!=kAliEventTypeReconfigure && eventID.fType!=kAliEventTypeDCSUpdate )
		    fStatus->fHLTStatus.fReceivedEventCount++;
		fStatus->Touch();
		}
	    if ( !ed )
		{
		LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::MsgLoop", "Out of memory" )
		    << "Out of memory allocating EventData struct for new event end message."
		    << "Aborting event 0x" << AliHLTLog::kHex << transferSEDD->fData.fEventNr << " ("
		    << AliHLTLog::kDec << transferSEDD->fData.fEventNr << ")."
		    << ENDLOG;
		if ( fStatus )
		    {
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
		    }
		}

	    
	    ret = pthread_mutex_lock( &fDescriptorMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::BlobLoop", "Descriptor Handler Mutex Error" )
		    << "Error locking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << "). Continuing..." << ENDLOG;
		}
	    if ( fDescriptorHandler )
		currentSEDD = fDescriptorHandler->GetFreeEventDescriptor( eventID, transferSEDD->fData.fBlockCount );
	    ret = pthread_mutex_unlock( &fDescriptorMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::BlobLoop", "Descriptor Handler Mutex Error" )
		    << "Error unlocking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << "). Continuing..." << ENDLOG;
		}
	    if ( !currentSEDD )
		{
		LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::BlobLoop", "Out of memory" )
		    << "Error trying to allocate memory for sub-event data descriptor for event 0x" 
		    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		    << ")." << ENDLOG;
		fEventDataCache.Release( (uint8*)ed );
		ed = NULL;
		if ( fStatus )
		    {
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
		    }
		break;
		}
	    ed->fSEDD = currentSEDD;
// 	    currentSEDD->fHeader.fLength = sizeof(AliHLTSubEventDataDescriptor)+
// 					   transferSEDD->fData.fBlockCount*sizeof(AliHLTSubEventDataBlockDescriptor);
// 	    currentSEDD->fHeader.fType.fID = ALIL3SUBEVENTDATADESCRIPTOR_TYPE;
// 	    currentSEDD->fHeader.fSubType.fID = 0;
// 	    currentSEDD->fHeader.fVersion = 1;
// 	    currentSEDD->fEventID = transferSEDD->fData.fEventID;
// 	    currentSEDD->fDataBlockCount = transferSEDD->fData.fBlockCount;
	    currentSEDD->fDataType.fID = transferSEDD->fData.fDataType;
	    currentSEDD->fStatusFlags = transferSEDD->fData.fStatusFlags;
            currentSEDD->fEventBirth_s = transferSEDD->fData.fEventBirth_s;
            currentSEDD->fEventBirth_us = transferSEDD->fData.fEventBirth_us;
	    
	    ed->fETS = (AliHLTEventTriggerStruct*)fETSCache.Get( sizeof(AliHLTEventTriggerStruct)+sizes[1] );
	    if ( ed->fETS )
		{
		ed->fETS->fHeader.fLength = sizeof(AliHLTEventTriggerStruct)+sizes[1];
		ed->fETS->fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
		ed->fETS->fHeader.fSubType.fID = 0;
		ed->fETS->fHeader.fVersion = 1;
		ed->fETS->fDataWordCount = sizes[1] / sizeof(ed->fETS->fDataWords[0]);
		memcpy( ed->fETS->fDataWords, p+offsets[1], sizes[1] );
		}
	    
	    for ( i = 0; i < currentSEDD->fDataBlockCount; i++ )
		{
		currentSEDD->fDataBlocks[i].fShmID = fShmID;
		currentSEDD->fDataBlocks[i].fBlockSize = transferSEDD->fBlocks[i].fBlockSize;
		LOG( AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::BlobLoop", "New event block" )
		    << "New Event 0x" << AliHLTLog::kHex << currentSEDD->fEventID
		    << " (" << AliHLTLog::kDec << currentSEDD->fEventID << ")block " << AliHLTLog::kDec << i << " Offset: "
		    << transferSEDD->fBlocks[i].fBlockOffset << " - Size: " << transferSEDD->fBlocks[i].fBlockSize << "." << ENDLOG;
		if ( fShmCopy && !fCopyDest )
		    {
		    LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::BlobLoop", "Event data block received" )
			<< "Unable to copy event data block " << AliHLTLog::kDec << i 
			<< " to external shared memory..." 
			<< ENDLOG;
		    }
		if ( transferOffset != (AliUInt32_t)-1 )
		    {
		    if ( fShmCopy )
			{
			if ( fCopyDest )
			    {
			    AliUInt8_t* dest, *src;
			    dest = fCopyDest;
			    dest += destOffset;
			    src = p+offsets[i+2];
			    memcpy( dest, src, transferSEDD->fBlocks[i].fBlockSize  );
			    destOffset += transferSEDD->fBlocks[i].fBlockSize;
			    }
			else
			    {
			    currentSEDD->fDataBlocks[i].fBlockOffset = (AliUInt32_t)-1;
			    currentSEDD->fDataBlocks[i].fShmID.fShmType = kInvalidShmType;
			    currentSEDD->fDataBlocks[i].fShmID.fKey.fID = (AliUInt64_t)-1;
			    }
			}
		    else
			currentSEDD->fDataBlocks[i].fBlockOffset = transferSEDD->fBlocks[i].fBlockOffset+transferOffset;
		    }
		else
		    currentSEDD->fDataBlocks[i].fBlockOffset = (AliUInt32_t)-1;
		currentSEDD->fDataBlocks[i].fProducerNode = transferSEDD->fBlocks[i].fProducerNode;
		currentSEDD->fDataBlocks[i].fDataType.fID = transferSEDD->fBlocks[i].fDataType;
		currentSEDD->fDataBlocks[i].fDataOrigin.fID = transferSEDD->fBlocks[i].fDataOrigin;
		currentSEDD->fDataBlocks[i].fDataSpecification = transferSEDD->fBlocks[i].fDataSpecification;
		currentSEDD->fDataBlocks[i].fStatusFlags = transferSEDD->fBlocks[i].fStatusFlags;
		//currentSEDD->fDataBlocks[i].fByteOrder = transferSEDD->fBlocks[i].fDataByteOrder;
		for ( int j = 0; j < kAliHLTSEDBDAttributeCount; j++ )
		    currentSEDD->fDataBlocks[i].fAttributes[j] = transferSEDD->fBlocks[i].fDataAttributes[j];
		
		}
	    if ( fEventAccounting )
		fEventAccounting->NewEvent( GetName(), currentSEDD );


	    ret = pthread_mutex_lock( &fEventListMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::BlobLoop", "SEDD List Mutex Error" )
		    << "Error locking sedd list mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << "). Continuing..." << ENDLOG;
		}
	    fEventList.Add( ed );
	    fAllEventsPurged = false;
	    ret = pthread_mutex_unlock( &fEventListMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::BlobLoop", "SEDD List Mutex Error" )
		    << "Error unlocking sedd list mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << "). Continuing..." << ENDLOG;
		}
	    AnnounceEvent( ed->fSEDD->fEventID, *(ed->fSEDD), *(ed->fETS) );
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
		if ( ed->fSEDD->fEventID.fType!=kAliEventTypeTick && ed->fSEDD->fEventID.fType!=kAliEventTypeReconfigure && ed->fSEDD->fEventID.fType!=kAliEventTypeDCSUpdate )
		    fStatus->fHLTStatus.fAnnouncedEventCount++;
		fStatus->fHLTStatus.fPendingOutputEventCount = GetPendingEventCount();
		fStatus->Touch();
		}
	    }
	}
    fBlobLoopQuitted = true;
    }

bool AliHLTPublisherBridgeHead::QuitBlobLoop()
    {
    if ( fBlobLoopQuitted )
	return true;
    fQuitBlobLoop = true;
    fBlobCom->AbortWaitForReceivedTransfer();
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    gettimeofday( &start, NULL );
    while ( !fBlobLoopQuitted && deltaT<timeLimit )
	{
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    return fBlobLoopQuitted;
    }


void AliHLTPublisherBridgeHead::CanceledEvent( AliEventID_t event, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    if ( fEventAccounting )
	fEventAccounting->EventFinished( GetName(), event );
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fHLTStatus.fPendingOutputEventCount = GetPendingEventCount();
	fStatus->Touch();
	}

    AliHLTEventDoneData* edd;
    unsigned long sz = AliHLTGetMergedEventDoneDataSize( eventDoneData );
    edd = (AliHLTEventDoneData*)fEDDCache.Get( sz );
    if ( edd )
	AliHLTMergeEventDoneData( event, edd, eventDoneData );
    FreeEvent( event, edd, true );
    }

void AliHLTPublisherBridgeHead::AnnouncedEvent( AliEventID_t eventID, 
						const AliHLTSubEventDataDescriptor&, 
						const AliHLTEventTriggerStruct&, 
						AliUInt32_t )
    {
    LOG( AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::AnnouncedEvent", "Event announced" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << ") announced."
	<< ENDLOG;
    }

bool AliHLTPublisherBridgeHead::GetAnnouncedEventData( AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
						       AliHLTEventTriggerStruct*& trigger )
    {
    PEventData* iter = NULL;
    sedd = NULL;
    trigger = NULL;
    int ret;
    unsigned long ndx;

    ret = pthread_mutex_lock( &fEventListMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::GetAnnouncedEventData", "SEDD List Mutex Error" )
	    << "Error locking sedd list mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    if ( MLUCVectorSearcher<PEventData,AliEventID_t>::FindElement( fEventList, &FindEventDataByEvent, eventID, ndx ) )
	{
	iter = fEventList.GetPtr( ndx );
	sedd = (*iter)->fSEDD;
	trigger = (*iter)->fETS;
	}
    ret = pthread_mutex_unlock( &fEventListMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::GetAnnouncedEventData", "SEDD List Mutex Error" )
	    << "Error unlocking sedd list mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    if ( sedd && trigger )
	return true;
    return false;
    }

void AliHLTPublisherBridgeHead::EventDoneDataReceived( const char* /*subscriberName*/, AliEventID_t eventID,
					    const AliHLTEventDoneData* edd, bool forceForward, bool forwarded )
    {
    if ( fEventAccounting )
	fEventAccounting->EventFinished( fName.c_str(), eventID, forceForward ? 2 : 1, forwarded ? 2 : 1 );
    bool releaseEvent = false;
    if ( forceForward && edd )
	{
	int ret = 0;
	PEventData* iter = NULL;
	if ( !fPaused && fConnectionDesired )
	    {
	    if ( !fMsgConnected )
		ret = Connect();
	    if ( !fMsgConnected || ret )
		{
		LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::EventDoneDataReceived", "Connection error" )
		    << "Unable to connect to subscriber bridge head: " 
		    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
		}
	    }
	unsigned long ndx;
	ret = pthread_mutex_lock( &fEventListMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::EventDoneDataReceived", "SEDD List Mutex Error" )
		<< "Error locking sedd list mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << "). Continuing..." << ENDLOG;
	    }
	if ( fAllEventsPurged ) // Everything is gone already...
	    {
	    ret = pthread_mutex_unlock( &fEventListMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::EventDoneDataReceived", "SEDD List Mutex Error" )
		    << "Error unlocking sedd list mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << ")." << ENDLOG;
		}
	    return;
	    }
	if ( MLUCVectorSearcher<PEventData,AliEventID_t>::FindElement( fEventList, &FindEventDataByEvent, eventID, ndx ) )
	    {
	    iter = fEventList.GetPtr( ndx );
	    releaseEvent = SendEventDoneDataForward( edd );
	    }
	else
	    {
	    LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::EventDoneDataReceived", "Event Not Found" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" 
		<< AliHLTLog::kDec << eventID << ") for forward message could not be found." << ENDLOG;
	    }
	if ( !releaseEvent )
	    {
	    //eventData = new EventDoneData;
	    EventDoneData* eventData = fIntEDDCache.Get();
	    if ( !eventData )
		{
		LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::EventDoneDataReceived", "Out of memory" )
		    << "Out of memory allocating EventDoneData structure." << ENDLOG;
		LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::EventDoneDataReceived", "Event Lost" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		    << ") could not be signalled as done..." << ENDLOG;
		releaseEvent = true;
		}
	    else
		{
		LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::EventDoneDataReceived", "Event Forward Retry" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		    << ") done data could not be forwarded on first try. Trying again later..." 
		    << ENDLOG;
		eventData->fEventID = eventID;
		eventData->fRetryCount = 1;
		eventData->fSendDone = false;
		eventData->fForceForward = true;
		unsigned long sz = edd->fHeader.fLength;
		eventData->fEDD = (AliHLTEventDoneData*)fEDDCache.Get( sz );
		if ( !eventData->fEDD )
		    {
		    LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::EventDoneDataReceived", "Event Retry Out of Memory" )
			<< "Out of memory while trying to allocate " << AliHLTLog::kDec << sz
			<< " bytes of event done (forward) data for event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
			<< "). Cannot retry!" 
			<< ENDLOG;
		    fIntEDDCache.Release( eventData );
		    eventData = NULL;
		    }
		else
		    {
		    memcpy( eventData->fEDD, edd, sz );
		    fTimer.AddTimeout( fRetryTime, &fRetrySignal, reinterpret_cast<AliUInt64_t>(eventData) );
		    }
		}
	    }
	ret = pthread_mutex_unlock( &fEventListMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::EventDoneDataReceived", "SEDD List Mutex Error" )
		<< "Error unlocking sedd list mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    }
	}
    }


bool AliHLTPublisherBridgeHead::SendEventDoneDataForward( const AliHLTEventDoneData* edd )
    {
    bool success = false;
    int ret;
    if ( !fPaused && fMsgConnected )
	{
	AliHLTBridgeMsg msgClass;
	AliHLTBridgeMsgStruct* msg;
	AliUInt32_t len = 0;
	if ( edd && edd->fDataWordCount>0 )
	    len = edd->fHeader.fLength;
	if ( len )
	    {
	    //msg = (AliHLTBridgeMsgStruct*)new AliUInt8_t[ msgClass.GetData()->fLength + len ];
	    msg = (AliHLTBridgeMsgStruct*)fMsgCache.Get( msgClass.GetData()->fLength + len );
	    if ( !msg )
		{
		LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::SendEventDoneDataForward", "Out of memory" )
		    << "Out of memory trying to allocate event done forward message of " << AliHLTLog::kDec
		    << msgClass.GetData()->fLength + len << " bytes. Not sending event done data..." << ENDLOG;
		msg = msgClass.GetData();
		}
	    else
		{
		memcpy( msg, msgClass.GetData(), msgClass.GetData()->fLength );
		memcpy( ((AliUInt8_t*)msg)+msgClass.GetData()->fLength, edd, len );
		msg->fLength += len;
		}
	    }
	else
	    msg = msgClass.GetData();
	
	msg->fMsgType = kAliHLTBridgeEventDoneForwardMsg;
	msg->fEventID = edd->fEventID.fNr;
	msg->fEventType = edd->fEventID.fType;
	pthread_mutex_lock( &fMsgComMutex );
	if ( fCommunicationTimeout )
	    ret = fMsgCom->Send( fRemoteMsgAddress, msg, fCommunicationTimeout );
	else
	    ret = fMsgCom->Send( fRemoteMsgAddress, msg );
	pthread_mutex_unlock( &fMsgComMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::SendEventDoneDataForward", "Msg send error" )
		<< "Error sending event done forward message for event 0x" << AliHLTLog::kHex 
		<< edd->fEventID << " (" << AliHLTLog::kDec << edd->fEventID
		<< ") to subscriber bridge head: " << strerror(ret) << " (" 
		<< ret << ")." << ENDLOG;
	    if ( fErrorCallback )
		fErrorCallback->ConnectionError( this );
	    }
	else
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::SendEventDoneDataForward", "Done Msg sent" )
		<< "Event done forward message for event 0x" << AliHLTLog::kHex 
		<< edd->fEventID << " (" << AliHLTLog::kDec << edd->fEventID
		<< ") successfully sent to subscriber bridge head." << ENDLOG;
	    success = true;
	    }
	if ( msg!=msgClass.GetData() )
	    {
	    //delete [] (AliUInt8_t*)msg;
	    fMsgCache.Release( (AliUInt8_t*)msg );
	    msg=NULL;
	    }
	}
    else
	{
	LOG( AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::SendEventDoneDataForward", "Not forwarding done Msg" )
	    << "Not sending event done forward message for event 0x" << AliHLTLog::kHex 
	    << edd->fEventID << " (" << AliHLTLog::kDec << edd->fEventID
	    << ") to subscriber bridge head." << ENDLOG;
	}
    return true;
    }



void AliHLTPublisherBridgeHead::RetryLoop()
    {
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCRunningStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	}
    fRetryLoopQuitted = false;
    std::vector<EventDoneData*> readd_erd;
    bool readded = false;
    while ( !fQuitRetryLoop )
	{
	if ( !fRetrySignal.HaveNotificationData() )
	    {
	    if ( !readded )
		fRetrySignal.Wait();
	    else
		fRetrySignal.WaitTime( 100 );
	    }
	readded = false;
	while ( fRetrySignal.HaveNotificationData() && !fQuitRetryLoop )
	    {
	    EventDoneData* erd;
	    erd = reinterpret_cast<EventDoneData*>( fRetrySignal.PopNotificationData() );
	    if ( !erd )
		continue;
	    fRetrySignal.Unlock();

	    if ( !erd->fForceForward )
		FreeEvent( erd->fEventID, erd->fEDD, erd->fSendDone, erd );
	    else
		{
		bool success = SendEventDoneDataForward( erd->fEDD );
		if ( success )
		    {
		    fEDDCache.Release( (uint8*)erd->fEDD );
		    fIntEDDCache.Release( erd );
		    }
		else
		    readd_erd.push_back( erd );
		}

	    fRetrySignal.Lock();
	    }
	while ( !readd_erd.empty() )
	    {
	    fRetrySignal.AddNotificationData( reinterpret_cast<AliUInt64_t>(*readd_erd.begin()) );
	    readd_erd.erase( readd_erd.begin() );
	    readded = true;
	    }

	}
    fRetrySignal.Unlock();
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCRunningStateFlag;
	}
    }



void AliHLTPublisherBridgeHead::FreeEvent( AliEventID_t eventID, AliHLTEventDoneData* eventDoneData, bool sendDone, EventDoneData* eventData )
    {
    LOG( AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::FreeEvent", "Freeing Event" )
	<< "Freeing event 0x" << AliHLTLog::kHex 
	<< eventID << " (" << AliHLTLog::kDec << eventID
	<< ")." << ENDLOG;
    int ret = 0;
    bool releaseEvent = true;
    PEventData* iter = NULL;
    if ( !fPaused && fConnectionDesired )
	{
	if ( !fMsgConnected )
	    ret = Connect();
	if ( !fMsgConnected || ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::FreeEvent", "Connection error" )
		<< "Unable to connect to subscriber bridge head: " 
		<< strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	    }
	}
    unsigned long ndx;
    ret = pthread_mutex_lock( &fEventListMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::FreeEvent", "SEDD List Mutex Error" )
	    << "Error locking sedd list mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    if ( fAllEventsPurged ) // Everything is gone already...
	{
	ret = pthread_mutex_unlock( &fEventListMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::FreeEvent", "SEDD List Mutex Error" )
		<< "Error unlocking sedd list mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    }
	return;
	}
    if ( MLUCVectorSearcher<PEventData,AliEventID_t>::FindElement( fEventList, &FindEventDataByEvent, eventID, ndx ) )
	{
	iter = fEventList.GetPtr( ndx );
	if ( !fPaused && fMsgConnected && sendDone )
	    {
	    AliHLTBridgeMsg msgClass;
	    AliHLTBridgeMsgStruct* msg;
	    AliUInt32_t len = 0;
	    if ( eventDoneData && eventDoneData->fDataWordCount>0 )
		len = eventDoneData->fHeader.fLength;
	    if ( len )
		{
		//msg = (AliHLTBridgeMsgStruct*)new AliUInt8_t[ msgClass.GetData()->fLength + len ];
		msg = (AliHLTBridgeMsgStruct*)fMsgCache.Get( msgClass.GetData()->fLength + len );
		if ( !msg )
		    {
		    LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::FreeEvent", "Out of memory" )
			<< "Out of memory trying to allocate event done message of " << AliHLTLog::kDec
			<< msgClass.GetData()->fLength + len << " bytes. Not sending event done data..." << ENDLOG;
		    msg = msgClass.GetData();
		    }
		else
		    {
		    memcpy( msg, msgClass.GetData(), msgClass.GetData()->fLength );
		    memcpy( ((AliUInt8_t*)msg)+msgClass.GetData()->fLength, eventDoneData, len );
		    msg->fLength += len;
		    }
		}
	    else
		msg = msgClass.GetData();

	    msg->fMsgType = kAliHLTBridgeEventDoneMsg;
	    msg->fEventID = (*iter)->fSEDD->fEventID.fNr;
	    msg->fEventType = (*iter)->fSEDD->fEventID.fType;
	    pthread_mutex_lock( &fMsgComMutex );
	    if ( fCommunicationTimeout )
		ret = fMsgCom->Send( fRemoteMsgAddress, msg, fCommunicationTimeout );
	    else
		ret = fMsgCom->Send( fRemoteMsgAddress, msg );
	    pthread_mutex_unlock( &fMsgComMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::FreeEvent", "Msg send error" )
		    << "Error sending event done message for event 0x" << AliHLTLog::kHex 
		    << (*iter)->fSEDD->fEventID << " (" << AliHLTLog::kDec << (*iter)->fSEDD->fEventID
		    << ") to subscriber bridge head: " << strerror(ret) << " (" 
		    << ret << ")." << ENDLOG;
		if ( fErrorCallback )
		    fErrorCallback->ConnectionError( this );
		//Disconnect( true );
		releaseEvent = false;
		}
	    else
		{
		LOG( AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::FreeEvent", "Done Msg sent" )
		<< "Event done message for event 0x" << AliHLTLog::kHex 
		<< (*iter)->fSEDD->fEventID << " (" << AliHLTLog::kDec << (*iter)->fSEDD->fEventID
		<< ") successfully sent to subscriber bridge head." << ENDLOG;
		}
	    if ( msg!=msgClass.GetData() )
		{
		//delete [] (AliUInt8_t*)msg;
		fMsgCache.Release( (AliUInt8_t*)msg );
		msg=NULL;
		}
	    }
	else
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::FreeEvent", "Not sending done Msg" )
		<< "Not sending event done message for event 0x" << AliHLTLog::kHex 
		<< (*iter)->fSEDD->fEventID << " (" << AliHLTLog::kDec << (*iter)->fSEDD->fEventID
		<< ") to subscriber bridge head." << ENDLOG;
	    }
	if ( fPaused )
	    releaseEvent = false;
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::FreeEvent", "Event Not Found" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" 
	    << AliHLTLog::kDec << eventID << ") to be freed could not be found." << ENDLOG;
	}
    if ( !releaseEvent )
	{
	if ( !eventData )
	    {
	    //eventData = new EventDoneData;
	    eventData = fIntEDDCache.Get();
	    if ( !eventData )
		{
		LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::FreeEvent", "Out of memory" )
		    << "Out of memory allocating EventDoneData structure." << ENDLOG;
		LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::FreeEvent", "Event Lost" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		    << ") could not be signalled as done..." << ENDLOG;
		releaseEvent = true;
		}
	    else
		{
		LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::FreeEvent", "Event Retry" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		    << ") could not be signalled as done on first try. Trying again later..." 
		    << ENDLOG;
		eventData->fEventID = eventID;
		eventData->fRetryCount = 1;
		eventData->fSendDone = sendDone;
		eventData->fForceForward = false;
		eventData->fEDD = eventDoneData;
		fTimer.AddTimeout( fRetryTime, &fRetrySignal, reinterpret_cast<AliUInt64_t>(eventData) );
		}
	    }
	else
	    {
	    if ( !fPaused && eventData->fRetryCount > fMaxRetryCount )
		{
		LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::FreeEvent", "Event Lost" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		    << ") could not be signalled as done after " << eventData->fRetryCount 
		    << " retries. Giving up..." << ENDLOG;
		releaseEvent = true;
		}
	    else
		{
		LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::FreeEvent", "Event Retry" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		    << ") could not be signalled as done on " << eventData->fRetryCount 
		    << ". retry. Trying again later..." << ENDLOG;
		if ( !fPaused )
		    eventData->fRetryCount++;
		fTimer.AddTimeout( fRetryTime, &fRetrySignal, reinterpret_cast<AliUInt64_t>(eventData) );
		}
	    
	    }
	}
    if ( releaseEvent && iter )
	{
	//delete [] (AliUInt8_t*)(*iter)->fSEDD;
	//delete (*iter);
	if ( (*iter)->fETS )
	    {
	    fETSCache.Release( (uint8*)( (*iter)->fETS ) );
	    (*iter)->fETS=NULL;
	    }
	fEventDataCache.Release( (uint8*)(*iter) );
	(*iter) = NULL;
	iter = NULL;
	fEventList.Remove( ndx );
	}
    ret = pthread_mutex_unlock( &fEventListMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::FreeEvent", "SEDD List Mutex Error" )
	    << "Error unlocking sedd list mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}

    if ( releaseEvent )
	{
	ret = pthread_mutex_lock( &fDescriptorMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::FreeEvent", "Descriptor Handler Mutex Error" )
		<< "Error locking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << "). Continuing..." << ENDLOG;
	    }
	if ( fDescriptorHandler )
	    fDescriptorHandler->ReleaseEventDescriptor( eventID );
	ret = pthread_mutex_unlock( &fDescriptorMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::FreeEvent", "Descriptor Handler Mutex Error" )
		<< "Error unlocking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << "). Continuing..." << ENDLOG;
	    }
	if ( eventDoneData )
	    {
	    fEDDCache.Release( (uint8*)eventDoneData );
	    eventDoneData = NULL;
	    //AliHLTFreeMergedEventDoneData( eventDoneData );
	    }
	if ( eventData )
	    {
	    fIntEDDCache.Release( eventData );
	    eventData = NULL;
	    }
	}
    }

bool AliHLTPublisherBridgeHead::FindEventDataByEvent( const PEventData& elem, const AliEventID_t& ref )
    {
    if ( elem && elem->fSEDD && elem->fSEDD->fEventID == ref )
	return true;
    return false;
    }


void AliHLTPublisherBridgeHead::PURGEALLEVENTS()
    {
    int ret;
    unsigned long timerCnts = 0, eventCnts = 0;
    // fEventDataCache
    // fEventList
    // fDescriptorHandler
    // fRetrySignal
    // fTimer
    EventDoneData* erd;
    vector<AliUInt64_t> timerNotData;
    vector<AliUInt64_t>::iterator notIter, notEnd;
//     vector<PEventData> eventData;
//     vector<PEventData>::iterator evtIter, evtEnd;
    PEventData ed;
    vector<AliEventID_t> eventIDs;
    vector<AliEventID_t>::iterator idIter, idEnd;

    fTimer.CancelTimeouts( &fRetrySignal, timerNotData );
    notIter = timerNotData.begin();
    notEnd = timerNotData.end();
    while ( notIter != notEnd )
	{
	timerCnts++;
	if ( *notIter )
	    fIntEDDCache.Release( reinterpret_cast<EventDoneData*>(*notIter) );
	notIter++;
	}

    fRetrySignal.Lock();
    while ( fRetrySignal.HaveNotificationData() )
	{
	erd = reinterpret_cast<EventDoneData*>( fRetrySignal.PopNotificationData() );
	fIntEDDCache.Release( erd );
	}

    ret = pthread_mutex_lock( &fEventListMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::PURGEALLEVENTS", "SEDD List Mutex Error" )
	    << "Error locking sedd list mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    while ( fEventList.GetCnt()>0 )
	{
	eventCnts++;
	ed = fEventList.GetFirst();
	if ( ed->fSEDD )
	    eventIDs.insert( eventIDs.end(), ed->fSEDD->fEventID );
	if ( ed->fETS )
	    {
	    fETSCache.Release( (uint8*)( ed->fETS ) );
	    ed->fETS = NULL;
	    }
	fEventDataCache.Release( (uint8*)ed );
	ed = NULL;
	fEventList.RemoveFirst();
	}
    fAllEventsPurged = true;

    ret = pthread_mutex_unlock( &fEventListMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::PURGEALLEVENTS", "SEDD List Mutex Error" )
	    << "Error unlocking sedd list mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}

    ret = pthread_mutex_lock( &fDescriptorMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::PURGEALLEVENTS", "Descriptor Handler Mutex Error" )
	    << "Error locking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    idIter = eventIDs.begin();
    idEnd = eventIDs.end();
    while ( idIter != idEnd )
	{
	if ( fDescriptorHandler )
	    fDescriptorHandler->ReleaseEventDescriptor( *idIter );
	idIter++;
	}
    ret = pthread_mutex_unlock( &fDescriptorMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::PURGEALLEVENTS", "Descriptor Handler Mutex Error" )
	    << "Error unlocking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}

    fRetrySignal.Unlock();

    AbortAllEvents( false, false );
    }

void AliHLTPublisherBridgeHead::INTERRUPTBLOBCONNECTION()
    {
    fBlobCom->InterruptBlobConnection( fRemoteBlobMsgAddress );    
    }


void AliHLTPublisherBridgeHead::DumpEventMetaData()
    {
    LOG( AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::DumpEventMetaData", "Event meta data dump" )
	<< "Dumping all current events to file." << ENDLOG;
    
    // We need to get the list of all current pending events, fetch their sub event descriptors,
    // dereference the shared memory pointers to all the data blocks and then write the
    // blocks to disk.

    AliHLTEventMetaDataOutput metaDataOutput( GetName(), fAliceHLT, fRunNumber );

    if ( fAliceHLT )
	metaDataOutput.SetEventTriggerDataFormatFunction(  AliHLTHLEventTriggerData2String );

    int ret = metaDataOutput.Open();
    if ( ret!=0 )
	{
	LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::DumpEventMetaData", "Error opening event meta data output file" )
	    << "Error opening event meta data output file: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return;
	}
    
    vector<AliEventID_t> events;
    GetPendingEvents(events);
    
    vector<AliEventID_t>::const_iterator event;
    for (event = events.begin(); event != events.end(); event++)
        {
        AliHLTSubEventDataDescriptor* sedd;
        AliHLTEventTriggerStruct* trigger;
        if ( ! GetAnnouncedEventData(*event, sedd, trigger) )
            {
            LOG( AliHLTLog::kError, "AliHLTPublisherBridgeHead::DumpEventMetaData", "No event")
                << "Could not find the event: 0x" << AliHLTLog::kHex << event->fNr << "/" << event->fType
		<< " (" << AliHLTLog::kDec << event->fNr << "/" << event->fType << ")." << ENDLOG;
            continue;
            }

	metaDataOutput.WriteEvent( *event, sedd, trigger, "received/announced" );
        }

    metaDataOutput.Close();
    
    if (events.size() == 0)
        {
        LOG( AliHLTLog::kDebug, "AliHLTPublisherBridgeHead::DumpEventMetaData", "Nothing to dump" )
	    << "There were no events found to be dumped to disk" << ENDLOG;
	}
    }



AliHLTPublisherBridgeHead::DefaultErrorCallback::DefaultErrorCallback()
    {
    fCorrectionInProgress = false;
    fConnected = false;
    pthread_mutex_init( &fProgressMutex, NULL );
    }

AliHLTPublisherBridgeHead::DefaultErrorCallback::~DefaultErrorCallback()
    {
    pthread_mutex_destroy( &fProgressMutex );
    }

void AliHLTPublisherBridgeHead::DefaultErrorCallback::ConnectionError( AliHLTPublisherBridgeHead* bridgeHead )
    {
    pthread_mutex_lock( &fProgressMutex );
    if ( fCorrectionInProgress )
	{
	pthread_mutex_unlock( &fProgressMutex );
	return;
	}
    fCorrectionInProgress = true;
    pthread_mutex_unlock( &fProgressMutex );
    
    fConnected = false;
    
    if ( bridgeHead )
	{
	bridgeHead->Pause();
	bridgeHead->LockCom();
	bridgeHead->Disconnect( true, true, 1000, false );
	bridgeHead->Connect( false );
	bridgeHead->UnlockCom();
	if ( fConnected )
	    bridgeHead->Restart();
	else
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTPublisherBridgeHead::DefaultErrorCallback::ConnectionError", "Unable to re-establish connection" )
		<< "Unable to re-establish connection to subscriber bridge head. Remaining in paused state." << ENDLOG;
	    }
	}
    
    pthread_mutex_lock( &fProgressMutex );
    fCorrectionInProgress = false;
    pthread_mutex_unlock( &fProgressMutex );
    }

void AliHLTPublisherBridgeHead::DefaultErrorCallback::ConnectionEstablished( AliHLTPublisherBridgeHead* )
    {
    fConnected = true;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

