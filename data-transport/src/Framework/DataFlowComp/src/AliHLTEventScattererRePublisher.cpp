/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTEventScattererRePublisher.hpp"
#include "AliHLTEventScatterer.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTEventDoneData.hpp"

AliHLTEventScattererRePublisher::AliHLTEventScattererRePublisher( const char* name, int slotCntExp2 ):
    AliHLTDetectorRePublisher( name, false, slotCntExp2 )
    {
    fScatterer = NULL;
    ForwardEvents();
    CacheEventDoneDataAllocs();
    LOG( AliHLTLog::kDebug, "AliHLTEventScattererRePublisher::AliHLTEventScattererRePublisher", "Starting" )
	<< "AliHLTEventScattererRePublisher object " << GetName() << " created..." << ENDLOG;
    }

AliHLTEventScattererRePublisher::~AliHLTEventScattererRePublisher()
    {
    }


void AliHLTEventScattererRePublisher::EventFinished( AliEventID_t eventID, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    if ( fScatterer )
	{
	AliHLTEventDoneData* ed, defED;
	ed = AliHLTMergeEventDoneData( eventID, eventDoneData );
	if ( !ed )
	    {
	    AliHLTMakeEventDoneData( &defED, eventID );
	    ed = &defED;
	    }
	fScatterer->EventDone( ed, this );
	if ( ed != &defED )
	    AliHLTFreeMergedEventDoneData( ed );
	}
    AliHLTDetectorRePublisher::EventFinished( eventID, eventDoneData );
    }


void AliHLTEventScattererRePublisher::MaxPingTimeout( SubscriberData* )
    {
    // Overwrite previous behaviour of removing the subscriber concerned.
    // Mark ourselves as bad.
     if ( fScatterer )
 	fScatterer->PublisherDown( this );
    }

void AliHLTEventScattererRePublisher::EventAnnounceError( SubscriberData*, AliEventID_t eventID )
    {
    // Overwrite previous behaviour of removing the subscriber concerned.
    // Mark ourselves as bad.
     if ( fScatterer )
 	fScatterer->PublisherSendError( this, eventID );
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
