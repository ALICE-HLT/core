/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTEventMergerNew.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTEventDoneData.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTShmManager.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "AliHLTHLTEventTriggerData.hpp"
#include "AliHLTEventAccounting.hpp"
#include "AliHLTEventMetaDataOutput.hpp"
#include "AliHLTDDLHeaderData.h"
#include "AliEventID2MLUCString.hpp"
#include <errno.h>
#include <math.h>

#define TRACE printf( "%s:%d\n", __FILE__, __LINE__ )

unsigned long AliHLTEventMergerNew::gfSubscriberIndexShift = 0;
unsigned long AliHLTEventMergerNew::gfSubscriberIndexMask = 0;


#if 0
#warning Event Done Backlog debugging enabled
#define BACKLOG_DEBUG
#endif

AliHLTEventMergerNew::AliHLTEventMergerNew( char const* name, int slotCount ):
    fName( name ),
#ifdef MERGED_BITMAPS
    fBitmapCachePerEvent( (slotCount>=4 ? slotCount : 4), true, false ),
#else
    fBitmapCachePerEvent( (slotCount>=4 ? slotCount+2 : 6), true, false ),
#endif
    fBitmapCacheTmp( 6, true, false ),
#if 1
    fEvents( 16, (slotCount>=4 ? slotCount : 4) ), 
#ifdef BACKLOG_DEBUG
    fEDDBacklog( 16, 2, true ),
#else
    fEDDBacklog( 16, (slotCount>=4 ? slotCount : 4), true ),
#endif
    fIncompleteEventCount(0ULL),
    fCompleteEventCount(0ULL),
    fQueuedEvents( 8, 4, true ),
#else
    fFinishedEventData( 8, slotCount ), 
    fUnfinishedEventData( 8, slotCount ), 
    fEDDBacklog( 8, (slotCount>=0 ? slotCount+4 : 8), true ),
#endif
    fEventDataCache( (slotCount>=0 ? slotCount+2 : 4) ),
    fIncompleteEventLog( 10, (slotCount>=0 ? slotCount+2 : 4), true ),
    fETSCache( (slotCount>0 ? slotCount+1 : 5), true, false ),
    fEDDCache( (slotCount>0 ? slotCount+4 : 8), true, false ),
    fSignal( (slotCount>=0 ? (slotCount>9 ? slotCount-3 : 6) :2) ), 
    fProcessingThread( this, &AliHLTEventMergerNew::DoProcessing ),
    fTimeoutDataCache( (slotCount>=0 ? slotCount : 4) ),
    fEventTimeoutCallback( this ),
    fTimer( (slotCount>=0 ? slotCount : 4) ),
    fEventAccounting(NULL),
    fTriggerClassContributingDDLCnt(0)
    {
#if 1
#else
    fFinishedEventData.SetPreDivisor( 4789 );
    fUnfinishedEventData.SetPreDivisor( 4789 );
    fEDDBacklog.SetPreDivisor( 4789 );
#endif
    
    fHighestSubscriberNdx = 0;
    fSubscribers = NULL;
    fSubscriberCnt = 0;
    fPublisher = NULL;
    fQuitProcessing = false;
    fProcessingQuitted = true;
    fDescriptorHandler = NULL;
    fSignal.Unlock();
    fPaused = false;
    fStatus = NULL;
    fLastBlockCnt = 0;

    fStaticETS.fHeader.fLength = sizeof(AliHLTEventTriggerStruct);
    fStaticETS.fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
    fStaticETS.fHeader.fSubType.fID = 0;
    fStaticETS.fHeader.fVersion = 1;
    fStaticETS.fDataWordCount = 0;

    AliHLTMakeEventDoneData( &fStaticEDD, AliEventID_t() );
    pthread_mutex_init( &fStaticEDDMutex, NULL );
    pthread_mutex_init( &fDescriptorHandlerMutex, NULL );
    pthread_mutex_init( &fEDDCacheMutex, NULL );

    fExpectedSubscribersBitmap = NULL;
    gfSubscriberIndexShift = (unsigned long)(0.5+log(sizeof(unsigned long)*8.0)/log(2.0));
    gfSubscriberIndexMask = (1 << gfSubscriberIndexShift)-1;
    fExpectedSubscribersBitmapLength = 0;
    fExpectedSubscriberCnt = 0;
    fDynamicTimeoutAdaption = false;
    fEventTimeout_ms = 0;
    fEventTimeoutLastLog_ms = 0;
    fMaxEventTimeout_ms = 0;
    fMinEventTimeout_ms = 0;
    fEventTimeoutShift_ms = 0;
    fNoReceiveEventCntLimit = 50;
#if 0
    fDynamicTimeoutAdaptionAlgorithm = fgkDynamicTimeoutAdaptionAverageAlgorithm;
#else
    fDynamicTimeoutAdaptionAlgorithm = fgkDynamicTimeoutAdaptionMaximumAlgorithm;
#endif
    fLastEventCompletionTimesCnt = 0;
    fLastEventCompletionTimesNdx = 0;
    fLastEventCompletionTimesAvg = 0;
    fLastEventCompletionTimes2TimeoutFactor = 2.0;
    fLastEventCompletionTimesMax = 0;
    fLastEventCompletionTimesMaxIndex = 0;
    pthread_mutex_init( &fBitmapCacheMutex, NULL );

    fETSMergeMode = kAppendETS;

    fAliceHLT = false;

    fSOREventReceived = false;
    fSOREventCompleted = false;

    fStrictSORRequirements = true;

    fDynamicInputXabling = false;

    fEventsPurged = false;

    fEventCount = 0;

    fETTCompFunc = CompareEventTriggerTypes;
    fETTCompFuncParam = NULL;

    fRunNumber = 0;
    }

AliHLTEventMergerNew::~AliHLTEventMergerNew()
    {
#ifdef PROFILE_INDEXED_VECTOR
    LOG( MLUCLog::kImportant, "AliHLTEventMergerNew", "Profile Data" )
	<< MLUCLog::kHex
	<< "fEvents: 0x" << (unsigned long long)&fEvents
	<< " - fEDDBacklog: 0x" << (unsigned long long)&fEDDBacklog
	<< " - fBitmapCachePerEvent: 0x" << (unsigned long long)&fBitmapCachePerEvent
	<< " - fEventDataCache: 0x" << (unsigned long long)&fEventDataCache
	<< ENDLOG;
#endif
    pthread_mutex_destroy( &fDescriptorHandlerMutex );
    pthread_mutex_destroy( &fEDDCacheMutex );
    pthread_mutex_destroy( &fStaticEDDMutex );
    pthread_mutex_destroy( &fBitmapCacheMutex );
    }

void AliHLTEventMergerNew::AddSubscriber( TSubscriber* subscriber, AliHLTPublisherInterface* publisher, unsigned long ndx )
    {
    SubscriberData sd;
    sd.fNdx = ndx;
    sd.fSubscriber = subscriber;
    sd.fPublisher = publisher;
    sd.fEnabled = true;
    sd.fAutoDisabled = false;
    sd.fContributingDDLs = NULL;
    sd.fNoReceiveEventCnt = 0;
    sd.fSubExpSubBitmapMutex = new MLUCMutex;
    //sd.fSubExpSubBitmapMutex = new pthread_mutex_t;
    //pthread_mutex_init( sd.fSubExpSubBitmapMutex, NULL );
    sd.fSubExpSubBitmap = NULL;
    sd.fModuloCnt = 0;
    sd.fModulos = NULL;
    sd.fTriggerCnt = 0;
    sd.fTriggers = NULL;
    MLUCMultiReaderMutex::TWriteLocker subscriberWriteLock( fSubscriberMutex );
    if ( (fExpectedSubscriberCnt<=0 || ndx>fHighestSubscriberNdx) )
	{
	unsigned long newLen = 1+ndx/(sizeof(unsigned long)*8);
	unsigned long* newBitmap = new unsigned long[ newLen ];
	if ( !newBitmap )
	    {
	    LOG( AliHLTLog::kError, "AliHLTEventMergerNew::AddSubscriber", "Out of memory" )
		<< "Cannot allocate bitmap for expected subscriber list when trying to add subscriber "
		<< AliHLTLog::kDec << ndx << "/0x" << AliHLTLog::kHex << ndx << " ("
		<< subscriber->GetName() << ")." << ENDLOG;
	    return;
	    }
	if ( fExpectedSubscribersBitmap )
	    {
	    memcpy( newBitmap, fExpectedSubscribersBitmap, fExpectedSubscribersBitmapLength*sizeof(unsigned long) );
	    delete [] fExpectedSubscribersBitmap;
	    }
	memset( newBitmap+fExpectedSubscribersBitmapLength, 0, (newLen-fExpectedSubscribersBitmapLength)*sizeof(unsigned long) );
	fExpectedSubscribersBitmapLength = newLen;
	fExpectedSubscribersBitmap = newBitmap;
	
	for ( unsigned nn=0; nn<fSubscriberCnt; nn++ )
	    {
	    if ( fSubscribers[nn].fSubExpSubBitmapMutex )
		{
		//pthread_mutex_lock( fSubscribers[nn].fSubExpSubBitmapMutex );
		MLUCMutex::TLocker lock( *fSubscribers[nn].fSubExpSubBitmapMutex );
		if ( fSubscribers[nn].fSubExpSubBitmap )
		    {
		    delete [] fSubscribers[nn].fSubExpSubBitmap;
		    }
		fSubscribers[nn].fSubExpSubBitmap = new unsigned long[ newLen ];
		if ( !fSubscribers[nn].fSubExpSubBitmap )
		    {
		    LOG( AliHLTLog::kError, "AliHLTEventMergerNew::AddSubscriber", "Out of memory" )
			<< "Cannot allocate bitmap for expected subscriber list when trying to add subscriber "
			<< AliHLTLog::kDec << ndx << "/0x" << AliHLTLog::kHex << ndx << " ("
			<< subscriber->GetName() << ")." << ENDLOG;
		    return;
		    }
		memcpy( fSubscribers[nn].fSubExpSubBitmap, fExpectedSubscribersBitmap, fExpectedSubscribersBitmapLength*sizeof(unsigned long) );
		
		//pthread_mutex_unlock( fSubscribers[nn].fSubExpSubBitmapMutex );
		}
	    
	    }
	}
    else
	{
	unsigned long newLen = 1+fHighestSubscriberNdx/(sizeof(unsigned long)*8);
	sd.fSubExpSubBitmap = new unsigned long[ newLen ];
	if ( !sd.fSubExpSubBitmap )
	    {
	    LOG( AliHLTLog::kError, "AliHLTEventMergerNew::AddSubscriber", "Out of memory" )
		<< "Cannot allocate bitmap for expected subscriber list when trying to add subscriber "
		<< AliHLTLog::kDec << ndx << "/0x" << AliHLTLog::kHex << ndx << " ("
		<< subscriber->GetName() << ")." << ENDLOG;
	    return;
	    }
	memcpy( sd.fSubExpSubBitmap, fExpectedSubscribersBitmap, fExpectedSubscribersBitmapLength*sizeof(unsigned long) );
	}
    if ( !(fExpectedSubscribersBitmap[GetBitmapWordIndex(ndx)] & GetBitmapBitInWord(ndx)) )
	{
	if ( fSubscriberCnt<ndx+1 )
	    {
	    SubscriberData* newSubscribers = new SubscriberData[ndx+1];
	    if ( !newSubscribers )
		{
		LOG( AliHLTLog::kError, "AliHLTEventMergerNew::AddSubscriber", "Out of memory" )
		    << "Cannot allocate subscriber list data to add subscriber "
		    << AliHLTLog::kDec << ndx << "/0x" << AliHLTLog::kHex << ndx << " ("
		    << subscriber->GetName() << ")." << ENDLOG;
		return;
		}
	    for ( unsigned long nn=0; nn<fSubscriberCnt; nn++ )
		newSubscribers[nn] = fSubscribers[nn];
	    
	    SubscriberData dummySD;
	      
	    dummySD.fSubscriber = NULL;
	    dummySD.fPublisher = NULL;
	    dummySD.fNdx = ~0UL;
	    dummySD.fEnabled = false;
	    dummySD.fContributingDDLs = NULL;
	    dummySD.fSubExpSubBitmap = NULL;
	    dummySD.fSubExpSubBitmapMutex = NULL;
	    dummySD.fAutoDisabled = false;
	    dummySD.fModuloCnt = 0;
	    dummySD.fModulos = NULL;
	    dummySD.fTriggerCnt = 0;
	    dummySD.fTriggers = NULL;
	    for ( unsigned long nn=fSubscriberCnt; nn<ndx+1; nn++ )
		newSubscribers[nn] = dummySD;
	    if ( fSubscribers )
		delete [] fSubscribers;
	    fSubscribers = newSubscribers;
	    fSubscriberCnt = ndx+1;

	    }
	fSubscribers[ndx] = sd;
	fExpectedSubscribersBitmap[GetBitmapWordIndex(ndx)] |= GetBitmapBitInWord(ndx);
	if ( ndx > fHighestSubscriberNdx )
	    fHighestSubscriberNdx = ndx;
	fExpectedSubscriberCnt++;
	}
#if 0
    LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddSubscriber", "New Bitmap", fExpectedSubscribersBitmap, fExpectedSubscribersBitmapLength );
#endif
    }

void AliHLTEventMergerNew::DelSubscriber( unsigned long ndx )
    {
    bool found = false;
    MLUCMultiReaderMutex::TWriteLocker subscriberWriteLock( fSubscriberMutex );
    if ( fSubscriberCnt>ndx && fSubscribers[ndx].fNdx==ndx )
        {
	SubscriberData dummySD;

	MLUCMutex* mutex = fSubscribers[ndx].fSubExpSubBitmapMutex;
	dummySD.fSubscriber = NULL;
	dummySD.fPublisher = NULL;
	dummySD.fNdx = ~0UL;
	dummySD.fEnabled = false;
	dummySD.fContributingDDLs = NULL;
	dummySD.fAutoDisabled = false;
	dummySD.fSubExpSubBitmap = NULL;
	dummySD.fSubExpSubBitmapMutex = NULL;
	dummySD.fModuloCnt = 0;
	dummySD.fModulos = NULL;
	dummySD.fTriggerCnt = 0;
	dummySD.fTriggers = NULL;
	{
	    MLUCMutex::TLocker lock( *mutex );
	    unsigned long* bitmap = fSubscribers[ndx].fSubExpSubBitmap;
	    fSubscribers[ndx] = dummySD;
	    delete [] bitmap;
	}
	delete mutex;
	found = true;
	if ( fExpectedSubscribersBitmap[GetBitmapWordIndex(ndx)] & GetBitmapBitInWord(ndx) )
	    fExpectedSubscriberCnt--;
	fExpectedSubscribersBitmap[GetBitmapWordIndex(ndx)] &= ~GetBitmapBitInWord(ndx);
	}
    subscriberWriteLock.Unlock();
#if 0
    LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::DelSubscriber", "New Bitmap", fExpectedSubscribersBitmap, fExpectedSubscribersBitmapLength );
#endif
    if ( found )
	{
	MLUCMultiReaderMutex::TWriteLocker eventDataLock( fEventMutex );
	fEvents.Iterate( AliHLTEventMergerNew::SubscriberDisabledEventIterationFunc, &ndx );
	unsigned long endx;
	do
	    {
	    found = false;
	    found = MLUCIndexedVectorSearcher<PEventData,AliEventID_t,uint32>::FindElement( fEvents, FindEmptyEventData, 0, endx );
	    if ( found )
		fEvents.Remove( endx );
	    }
	while ( found );
	vector<PEventData> lst;
	fEvents.Iterate( AliHLTEventMergerNew::AnnounceAnnounceableEventsIterationFunc, (void*)&lst );
	vector<PEventData>::iterator eventIter, eventEnd;
	eventIter = lst.begin();
	eventEnd = lst.end();
	PEventData ed;
	while ( eventIter != eventEnd )
	    {
	    ed = *eventIter;
	    ed->fState=kComplete;
		{
		MLUCMutex::TLocker countLock( fEventCountMutex );
		fIncompleteEventCount--;
		fCompleteEventCount++;
		}
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		if ( (*eventIter)->fEventID.fType!=kAliEventTypeTick && (*eventIter)->fEventID.fType!=kAliEventTypeReconfigure && (*eventIter)->fEventID.fType!=kAliEventTypeDCSUpdate )
		    ++(fStatus->fHLTStatus.fProcessedEventCount);
		fStatus->Touch();
		}
	    eventIter++;
	    }
	if ( fStatus )
	    {
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		{
		MLUCMutex::TLocker countLock( fEventCountMutex );
		fStatus->fHLTStatus.fPendingInputEventCount = fIncompleteEventCount;
		}
	    fStatus->Touch();
	    }
	eventDataLock.Unlock();
	if ( lst.size()>0 )
	    {
	    eventIter = lst.begin();
	    eventEnd = lst.end();
	    while ( eventIter != eventEnd )
		{
		//fSignal.Lock();
		fSignal.AddNotificationData( *eventIter );
		//fSignal.Unlock();
		++eventIter;
		}
	    fSignal.Signal();
	    }
	}
    }

void AliHLTEventMergerNew::EnableSubscriber( unsigned long ndx, bool autoDisable )
    {
    MLUCMultiReaderMutex::TWriteLocker subscriberWriteLock( fSubscriberMutex );
    if ( ndx<=fHighestSubscriberNdx && !(fExpectedSubscribersBitmap[GetBitmapWordIndex(ndx)] & GetBitmapBitInWord(ndx)) )
	{
	if ( fSubscriberCnt>ndx && fSubscribers[ndx].fNdx==ndx )
	    {
	    fSubscribers[ndx].fEnabled = true;
	    fSubscribers[ndx].fAutoDisabled = autoDisable;
	    fExpectedSubscribersBitmap[GetBitmapWordIndex(ndx)] |= GetBitmapBitInWord(ndx);
	    fExpectedSubscriberCnt++;
	    if ( autoDisable )
		{
		LOG( AliHLTLog::kImportant, "AliHLTEventMergerNew::EnableSubscriber", "Auto enabling subscriber" )
		    << "Automatically re-enabling subscriber " << fSubscribers[ndx].fSubscriber->GetName()
		    << " (index " << AliHLTLog::kDec << ndx << ")." << ENDLOG;
		}
	    }
#if 0
// The following error just had to be kept there for future generations to amuse themselves...
 	    false = true;
#endif
	}
#if 0
    LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::EnableSubscriber", "New Bitmap", fExpectedSubscribersBitmap, fExpectedSubscribersBitmapLength );
#endif
    }

void AliHLTEventMergerNew::DisableSubscriber( unsigned long ndx, bool autoDisable )
    {
    bool found = false;
    MLUCMultiReaderMutex::TWriteLocker subscriberWriteLock( fSubscriberMutex );
    if ( ndx<=fHighestSubscriberNdx && (fExpectedSubscribersBitmap[GetBitmapWordIndex(ndx)] & GetBitmapBitInWord(ndx)) )
	{
	if ( fSubscriberCnt>ndx && fSubscribers[ndx].fNdx==ndx )
	    {
	    found = true;
	    fSubscribers[ndx].fEnabled = false;
	    fSubscribers[ndx].fAutoDisabled = autoDisable;
	    if ( fExpectedSubscribersBitmap[GetBitmapWordIndex(ndx)] & GetBitmapBitInWord(ndx) )
		fExpectedSubscriberCnt--;
	    fExpectedSubscribersBitmap[GetBitmapWordIndex(ndx)] &= ~GetBitmapBitInWord(ndx);
	    if ( autoDisable )
		{
		LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::EnableSubscriber", "Auto disabling subscriber" )
		    << "Automatically disabling subscriber " << fSubscribers[ndx].fSubscriber->GetName()
		    << " (index " << AliHLTLog::kDec << ndx << ") because of missing input for " << fNoReceiveEventCntLimit << " events." << ENDLOG;
		}
	    }
	}
    subscriberWriteLock.Unlock();
#if 0
    LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::DisableSubscriber", "New Bitmap", fExpectedSubscribersBitmap, fExpectedSubscribersBitmapLength );
#endif
    if ( found )
	{
	MLUCMultiReaderMutex::TWriteLocker eventDataLock( fEventMutex );
	fEvents.Iterate( AliHLTEventMergerNew::SubscriberDisabledEventIterationFunc, &ndx );
	unsigned long sndx;
	do
	    {
	    found = false;
	    found = MLUCIndexedVectorSearcher<PEventData,AliEventID_t,uint32>::FindElement( fEvents, FindEmptyEventData, 0, sndx );
	    if ( found )
		fEvents.Remove( sndx );
	    }
	while ( found );
	vector<PEventData> lst;
	fEvents.Iterate( AliHLTEventMergerNew::AnnounceAnnounceableEventsIterationFunc, (void*)&lst );
	vector<PEventData>::iterator eventIter, eventEnd;
	eventIter = lst.begin();
	eventEnd = lst.end();
	PEventData ed;
	while ( eventIter != eventEnd )
	    {
	    ed = *eventIter;
	    ed->fState = kComplete;
		{
		MLUCMutex::TLocker countLock( fEventCountMutex );
		fIncompleteEventCount--;
		fCompleteEventCount++;
		}
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		if ( (*eventIter)->fEventID.fType!=kAliEventTypeTick && (*eventIter)->fEventID.fType!=kAliEventTypeReconfigure && (*eventIter)->fEventID.fType!=kAliEventTypeDCSUpdate )
		    ++(fStatus->fHLTStatus.fProcessedEventCount);
		fStatus->Touch();
		}
	    eventIter++;
	    }
	if ( fStatus )
	    {
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		{
		MLUCMutex::TLocker countLock( fEventCountMutex );
		fStatus->fHLTStatus.fPendingInputEventCount = fIncompleteEventCount;
		}
	    fStatus->Touch();
	    }
	eventDataLock.Unlock();
	if ( lst.size()>0 )
	    {
	    eventIter = lst.begin();
	    eventEnd = lst.end();
	    while ( eventIter != eventEnd )
		{
		//fSignal.Lock();
		fSignal.AddNotificationData( *eventIter );
		//fSignal.Unlock();
		++eventIter;
		}
	    fSignal.Signal();
	    }
	}
    }

void AliHLTEventMergerNew::SetSubscriberContributingDDLList( unsigned long ndx, volatile_hltreadout_uint32_t* contrDDLList )
    {
    MLUCMultiReaderMutex::TWriteLocker subscriberWriteLock( fSubscriberMutex );
    if ( fSubscriberCnt>ndx && fSubscribers[ndx].fNdx==ndx )
        {
	fSubscribers[ndx].fContributingDDLs = contrDDLList;
	}
    }

void AliHLTEventMergerNew::SetSubscriberEventModulos( unsigned long ndx, const std::vector<unsigned long>& modulos )
    {
    MLUCMultiReaderMutex::TWriteLocker subscriberWriteLock( fSubscriberMutex );
    if ( fSubscriberCnt>ndx && fSubscribers[ndx].fNdx==ndx )
        {
	if ( fSubscribers[ndx].fModulos )
	    {
	    delete [] fSubscribers[ndx].fModulos;
	    fSubscribers[ndx].fModulos = NULL;
	    }
	fSubscribers[ndx].fModulos = new unsigned long[modulos.size()];
	if ( fSubscribers[ndx].fModulos )
	    {
	    fSubscribers[ndx].fModuloCnt = modulos.size();
	    for ( unsigned long nn=0; nn<fSubscribers[ndx].fModuloCnt; nn++ )
		fSubscribers[ndx].fModulos[nn] = modulos[nn];
	    }
	else
	    {
	    LOG( AliHLTLog::kError, "AliHLTEventMergerNew::SetSubscriberEventModulos", "Out of memory allocating subscriber event modulos" )
		<< "Out of memory allocating memory to store " << AliHLTLog::kDec << modulos.size()
		<< " subscriber event modulos (subscriber " << ndx << " - " << fSubscribers[ndx].fSubscriber->GetName()
		<< ")." << ENDLOG;
	    }
	}
    }

void AliHLTEventMergerNew::SetEventTypeStackMachineCodes( unsigned long ndx, const std::vector<AliHLTEventTriggerStruct*>& eventTypeStackMachineCodes )
    {
    MLUCMultiReaderMutex::TWriteLocker subscriberWriteLock( fSubscriberMutex );
    if ( fSubscriberCnt>ndx && fSubscribers[ndx].fNdx==ndx )
        {
	if ( fSubscribers[ndx].fTriggers )
	    {
	    for ( unsigned long nn=0; nn<fSubscribers[ndx].fTriggerCnt; nn++ )
		delete [] reinterpret_cast<uint8*>( fSubscribers[ndx].fTriggers[nn] );
	    delete [] fSubscribers[ndx].fTriggers;
	    fSubscribers[ndx].fTriggers = NULL;
	    fSubscribers[ndx].fTriggerCnt = 0;
	    }
	if ( !eventTypeStackMachineCodes.empty() )
	    {
	    fSubscribers[ndx].fTriggers = new AliHLTEventTriggerStruct*[eventTypeStackMachineCodes.size()];
	    if ( !fSubscribers[ndx].fTriggers )
		{
		LOG( AliHLTLog::kError, "AliHLTEventMergerNew::SetEventTypeStackMachineCodes", "Event type setting - Out of memory" )
		    << "Out of memory trying to allocate storage for " << AliHLTLog::kDec
		    << eventTypeStackMachineCodes.size() << " trigger word structures."
		    << ENDLOG;
		}
	    else
		{
		fSubscribers[ndx].fTriggerCnt = eventTypeStackMachineCodes.size();
		unsigned long nn=0;
		vector<AliHLTEventTriggerStruct*>::const_iterator etIter, etEnd;
		AliHLTEventTriggerStruct* et;
		etIter = eventTypeStackMachineCodes.begin();
		etEnd = eventTypeStackMachineCodes.end();
		while ( etIter != etEnd )
		    {
		    //et = new AliHLTEventTriggerStruct( **etIter );
		    et = reinterpret_cast<AliHLTEventTriggerStruct*>( new AliUInt8_t[ (*etIter)->fHeader.fLength ] );
		    if( !et )
			{
			LOG( AliHLTLog::kError, "AliHLTSamplePublisher", "Event type setting - Out of memory" )
			    << "Ouf of memory trying to allocate " << AliHLTLog::kDec
			    << (*etIter)->fHeader.fLength << " bytes of trigger words."
			    << ENDLOG;
			--(fSubscribers[ndx].fTriggerCnt);
			}
		    else
			{
			memcpy( et, *etIter, (*etIter)->fHeader.fLength );
			fSubscribers[ndx].fTriggers[nn++] = et;
			}
		    ++etIter;
		    }
		}
	    }
	}
    }

void AliHLTEventMergerNew::SetEventTriggerTypeCompareFunc( AliHLTEventTriggerTypeCompareFunc compFunc, void* compFuncParam )
    {
    fETTCompFunc = compFunc;
    fETTCompFuncParam = compFuncParam;
    }



void AliHLTEventMergerNew::SetPublisher( TPublisher* pub )
    {
    fPublisher = pub;
    }

void AliHLTEventMergerNew::StartProcessing()
    {
    fSOREventReceived = false;
    fSOREventCompleted = false;
    fQuitProcessing = false;
    fTimer.Start();
    fProcessingThread.Start();
    }

void AliHLTEventMergerNew::StopProcessing()
    {
    fQuitProcessing = true;
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    gettimeofday( &start, NULL );
    while ( !fProcessingQuitted && deltaT<timeLimit )
	{
	fSignal.Signal();
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( !fProcessingQuitted )
	fProcessingThread.Abort();
    fProcessingThread.Join();
    fTimer.Stop();
    }

void AliHLTEventMergerNew::PauseProcessing()
    {
    fPaused = true;
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCPausedStateFlag;
	}
    fSignal.Signal();
    }

void AliHLTEventMergerNew::ResumeProcessing()
    {
    fPaused = false;
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCPausedStateFlag;
	}
    fSignal.Signal();
    }


// This method is called whenever a new wanted 
// event is available.
int AliHLTEventMergerNew::NewEvent( TSubscriber* sub, unsigned long subNdx, 
				    AliHLTPublisherInterface& publisher, 
				    AliEventID_t eventID, 
				    const AliHLTSubEventDataDescriptor& sbevent,
				    const AliHLTEventTriggerStruct& origets )
    {
    if ( fEventAccounting )
	fEventAccounting->NewEvent( publisher.GetName(), &sbevent );
    fEventsPurged = false;
    AliHLTEventTriggerStruct* ets=NULL;
    AliHLTEventTriggerStruct* oldETS=NULL;
    unsigned long oldETSWordCount=0;
    AliUInt32_t oldETSLength = sizeof(AliHLTEventTriggerStruct);

#if 0
    unsigned long expSubBitmapLength=0;
    unsigned long* expSubBitmap=NULL;
#endif
	{
	MLUCMultiReaderMutex::TReadLocker subscriberLock( fSubscriberMutex );
	if ( (fDynamicInputXabling || fDynamicTimeoutAdaption) && subNdx<fSubscriberCnt && fSubscribers[subNdx].fNdx==subNdx )
	    fSubscribers[subNdx].fNoReceiveEventCnt = 0;
	if ( fDynamicInputXabling && !(fExpectedSubscribersBitmap[GetBitmapWordIndex(subNdx)] & GetBitmapBitInWord(subNdx)) && fSubscribers[subNdx].fAutoDisabled )
	    {
	    subscriberLock.Unlock();
	    EnableSubscriber( subNdx, true );
	    }
	}
    
    unsigned long evNdx=~0UL;
    PEventData ed;
    MLUCMutex::TLocker eventLock;
    bool found = false;
    bool eventFound=false;
    vector<unsigned long>::iterator iter, end;
    MLUCMultiReaderMutex::TReadLocker eventDataLock( fEventMutex );
    do
	{
    if ( evNdx!=~0UL || fEvents.FindElementReverse( eventID, evNdx ) )
	{
	ed = fEvents.Get( evNdx );
#if 1
	if ( !ed )
	    {
	    MLUCString str;
	    fEvents.AsString( str );
	    LOG( AliHLTLog::kFatal, "AliHLTEventMergerNew::NewEvent", "Internal Error" )
		<< "Internal Error: ed==0 - EventID: 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << "). fEvents dump: \n" << str.c_str() << "\n." << ENDLOG;
	    }
	if ( ed->fEventID != eventID )
	    {
	    MLUCString str;
	    fEvents.AsString( str );
	    LOG( AliHLTLog::kFatal, "AliHLTEventMergerNew::NewEvent", "Internal Error" )
		<< "Internal Error: ed->fEventID != eventID - eventID: 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		<< ") - ed->fEventID: 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID 
		<< "). fEvents dump: \n" << str.c_str() << "\n." << ENDLOG;
	    }
#endif
	eventFound = true;
	eventLock.Set( ed->fMutex );
	if ( ed->fState==kComplete )
	    {
	    // Event has already been received and forwarded (maybe because of timeout...). We add our data
	    LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "Successive Event Part After Announce" )
		<< "Retrieved successive event part for event 0x" << AliHLTLog::kHex
		<< eventID << " (" << AliHLTLog::kDec << eventID << ") with "
		<< sbevent.fDataBlockCount << " data blocks after event was already announced." << ENDLOG;
	    if ( fDynamicTimeoutAdaption )
		AddEventReceiveInterval( ed->fFirstPartReceive );
#ifdef MERGED_BITMAPS
	    if ( subNdx <= ed->fHighestExpectedPartNdx && (ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kExpectedParts+subNdx)] & GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kExpectedParts+subNdx)) )
#else
	    if ( subNdx <= ed->fHighestExpectedPartNdx && (ed->fExpectedPartBitmap[GetBitmapWordIndex(subNdx)] & GetBitmapBitInWord(subNdx)) )
#endif
		found=true;
	    if ( !found )
		{
		// no part of this event is expected for this subscriber
		LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "Unexpected Event Part" )
		    << "Received unexpected event part " << AliHLTLog::kDec << subNdx << " from publisher "
		    << publisher.GetName() << " for event 0x" << AliHLTLog::kHex
		    << eventID << " (" << AliHLTLog::kDec << eventID << ") (HighestExpectedPartNdx: " << AliHLTLog::kDec
		    << ed->fHighestExpectedPartNdx << " - Bitmap word index: " << GetBitmapWordIndex(subNdx) << " - Bitmap position in word: "
		    << GetBitmapBitInWord(subNdx) << ")." << ENDLOG;
#if 0
		LogBitmap( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "Scheduling incomplete event - Expected Part Bitmap", ed->fExpectedPartBitmap, fExpectedSubscribersBitmapLength );
		LogBitmap( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "Scheduling incomplete Event - Received Part Bitmap", ed->fReceivedPartBitmap, fExpectedSubscribersBitmapLength );
		return 0;
#endif
		}
	
	    // Event has already been announced, we should not add any further 
	    // of our data. We just add that we contributed so we get the event done message.
	    if ( subNdx<= ed->fHighestExpectedPartNdx )
		{
#ifdef MERGED_BITMAPS
		if ( !(ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kExpectedParts+subNdx)] & GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kExpectedParts+subNdx)) )
		    {
		    ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kExpectedParts+subNdx)] |= GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kExpectedParts+subNdx);
		    ed->fSubscribersExpected++;
		    }
		if ( ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kReceivedParts+subNdx)] & GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kReceivedParts+subNdx) )
		    {
		    // A part of this event has already been received by this subscriber. Shouldn't happen.
		    LOG( AliHLTLog::kInformational, "AliHLTEventMergerNew::NewEvent", "Event already received" )
			<< "Event 0x" << AliHLTLog::kHex
			<< eventID << " (" << AliHLTLog::kDec << ") already received from subscriber '"
			<< sub->GetName() << "'/" << subNdx << "." << ENDLOG;
		    }
		else
		    {
		    ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kReceivedParts+subNdx)] |= GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kReceivedParts+subNdx);
		    ed->fSubscribersPresent++;
		    }
#else
		if ( !(ed->fExpectedPartBitmap[GetBitmapWordIndex(subNdx)] & GetBitmapBitInWord(subNdx)) )
		    {
		    ed->fExpectedPartBitmap[GetBitmapWordIndex(subNdx)] |= GetBitmapBitInWord(subNdx);
		    ed->fSubscribersExpected++;
		    }
		if ( ed->fReceivedPartBitmap[GetBitmapWordIndex(subNdx)] & GetBitmapBitInWord(subNdx) )
		    {
		    // A part of this event has already been received by this subscriber. Shouldn't happen.
		    LOG( AliHLTLog::kInformational, "AliHLTEventMergerNew::NewEvent", "Event already received" )
			<< "Event 0x" << AliHLTLog::kHex
			<< eventID << " (" << AliHLTLog::kDec << ") already received from subscriber '"
			<< sub->GetName() << "'/" << subNdx << "." << ENDLOG;
		    }
		else
		    {
		    ed->fReceivedPartBitmap[GetBitmapWordIndex(subNdx)] |= GetBitmapBitInWord(subNdx);
		    ed->fSubscribersPresent++;
		    }
#endif
		}
	    return 0;
	    }
	else if ( ed->fState==kIncomplete )
	    {
	    // Event has already been received. We add our data
	    LOG( AliHLTLog::kInformational, "AliHLTEventMergerNew::NewEvent", "Successive Event Part" )
		<< "Retrieved successive event part for event 0x" << AliHLTLog::kHex
		<< eventID << " (" << AliHLTLog::kDec << eventID << ") with "
		<< sbevent.fDataBlockCount << " data blocks." << ENDLOG;
#if 0
	    if ( ed->fExpectedPartBitmap[0] == 0 )
		{
		MLUCString str;
		fUnfinishedEventData.AsString( str );
		LOG( AliHLTLog::kFatal, "AliHLTEventMergerNew::NewEvent", "Internal Error" )
		    << "Internal Error: ed->fExpectedPartBitmap[0] == 0 - eventID: 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		    << "). fUnfinishedEventData dump: \n" << str.c_str() << "\n." << ENDLOG;
		}
	    if ( ed->fExpectedPartBitmap[0] != 3 )
		{
		MLUCString str;
		fUnfinishedEventData.AsString( str );
		LOG( AliHLTLog::kFatal, "AliHLTEventMergerNew::NewEvent", "Internal Error" )
		    << "Internal Error: ed->fExpectedPartBitmap[0] != 3 - eventID: 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		    << "). fUnfinishedEventData dump: \n" << str.c_str() << "\n." << ENDLOG;
		LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Expected part Bitmap", ed->fExpectedPartBitmap, fExpectedSubscribersBitmapLength );
		LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Expected subscribers Bitmap", fExpectedSubscribersBitmap, fExpectedSubscribersBitmapLength );
		}
	    LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Expected Part Bitmap 1", ed->fExpectedPartBitmap, fExpectedSubscribersBitmapLength );
	    LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Received Part Bitmap 1", ed->fReceivedPartBitmap, fExpectedSubscribersBitmapLength );
	    LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "New Part" )
		<< "subNdx: " << AliHLTLog::kDec << subNdx << " - ed->fHighestExpectedPartNdx: " << ed->fHighestExpectedPartNdx << "." << ENDLOG;
#endif
#ifdef MERGED_BITMAPS
	    if ( subNdx <= ed->fHighestExpectedPartNdx && (ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kExpectedParts+subNdx)] & GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kExpectedParts+subNdx)) )
#else
	    if ( subNdx <= ed->fHighestExpectedPartNdx && (ed->fExpectedPartBitmap[GetBitmapWordIndex(subNdx)] & GetBitmapBitInWord(subNdx)) )
#endif
		found=true;
	    if ( !found )
		{
		// no part of this event is expected for this subscriber
		LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "Unexpected Event Part" )
		    << "Received unexpected event part " << AliHLTLog::kDec << subNdx << " from publisher "
		    << publisher.GetName() << " for event 0x" << AliHLTLog::kHex
		    << eventID << " (" << AliHLTLog::kDec << eventID << ") (HighestExpectedPartNdx: " << AliHLTLog::kDec
#ifdef MERGED_BITMAPS
		    << ed->fHighestExpectedPartNdx << " - Bitmap word index: " << GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kExpectedParts+subNdx) << " - Bitmap position in word: "
		    << GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kExpectedParts+subNdx) << ")." << ENDLOG;
#else
		    << ed->fHighestExpectedPartNdx << " - Bitmap word index: " << GetBitmapWordIndex(subNdx) << " - Bitmap position in word: "
		    << GetBitmapBitInWord(subNdx) << ")." << ENDLOG;
#endif
		LogBitmap( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "Unexpected Event Part - Expected Part Bitmap", ed->fExpectedPartBitmap, fExpectedSubscribersBitmapLength );
		LogBitmap( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "Unexpected Event Part - Received Part Bitmap", ed->fReceivedPartBitmap, fExpectedSubscribersBitmapLength );
		if ( subNdx > ed->fHighestExpectedPartNdx )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "Unexpected Event Part" )
			<< "Cannot add unexpected event part " << AliHLTLog::kDec << subNdx << " from publisher "
			<< publisher.GetName() << " for event 0x" << AliHLTLog::kHex
			<< eventID << " (" << AliHLTLog::kDec << eventID << ") - event part will be ignored... " 
			<< ENDLOG;
		    // XXX Send Event Done?
		    return 0;
		    }
#ifdef MERGED_BITMAPS
		ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kExpectedParts+subNdx)] |= GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kExpectedParts+subNdx);
#else
		ed->fExpectedPartBitmap[GetBitmapWordIndex(subNdx)] |= GetBitmapBitInWord(subNdx);
#endif
		ed->fSubscribersExpected++;
		}
	
	    // If event has already been canceled, we do not need  to add any further 
	    // of our data. We just add that we contributed so we get the event done message.
	    if ( ed->fCanceled )
		{
		LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "Event Canceled" )
		    << "Event 0x" << AliHLTLog::kHex
		    << eventID << " (" << AliHLTLog::kDec << eventID << ") already canceled." << ENDLOG;
		if ( subNdx<= ed->fHighestExpectedPartNdx )
		    {
#ifdef MERGED_BITMAPS
		    if ( ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kReceivedParts+subNdx)] & GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kReceivedParts+subNdx) )
#else
		    if ( ed->fReceivedPartBitmap[GetBitmapWordIndex(subNdx)] & GetBitmapBitInWord(subNdx) )
#endif
			{
			// A part of this event has already been received by this subscriber. Shouldn't happen.
			LOG( AliHLTLog::kInformational, "AliHLTEventMergerNew::NewEvent", "Event already received" )
			    << "Event 0x" << AliHLTLog::kHex
			    << eventID << " (" << AliHLTLog::kDec << ") already received from subscriber '"
			    << sub->GetName() << "'/" << subNdx << "." << ENDLOG;
			}
		    else
			{
#if 0
			LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Received Part Bitmap 3a", ed->fReceivedPartBitmap, fExpectedSubscribersBitmapLength );
#endif
#ifdef MERGED_BITMAPS
			ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kReceivedParts+subNdx)] |= GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kReceivedParts+subNdx);
#else
			ed->fReceivedPartBitmap[GetBitmapWordIndex(subNdx)] |= GetBitmapBitInWord(subNdx);
#endif
#if 0
			LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Received Part Bitmap 3b", ed->fReceivedPartBitmap, fExpectedSubscribersBitmapLength );
#endif
			ed->fSubscribersPresent++;
			}
		    }
		return 0;
		}
	    if ( sbevent.fEventBirth_s > ed->fEventBirth_s || 
		 (sbevent.fEventBirth_s==ed->fEventBirth_s && sbevent.fEventBirth_us > ed->fEventBirth_us) )
		{ // When in doubt, always use the later time, making the event younger and giving it a longer grace period
		ed->fEventBirth_s = sbevent.fEventBirth_s;
		ed->fEventBirth_us = sbevent.fEventBirth_us;
		}
	    if ( sbevent.fOldestEventBirth_s < ed->fOldestEventBirth_s ) // When in doubt, always use the earlier time, giving a grace period to older events.
		ed->fOldestEventBirth_s = sbevent.fOldestEventBirth_s;
	    ed->fStatusFlags |= sbevent.fStatusFlags;
	    oldETS = ed->fETS;
	    oldETSWordCount = oldETS->fDataWordCount;
	    oldETSLength = oldETS->fHeader.fLength;
	    }
	}
    else if ( fEDDBacklog.FindElement( eventID, evNdx ) )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "Event already done" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") already done." << ENDLOG;
	    EventBacklogData ebd = fEDDBacklog.Get( evNdx );
	    if ( fDynamicTimeoutAdaption )
		AddEventReceiveInterval( ebd.fFirstPartReceive );
	    if ( ebd.fEDD )
		{
		eventDataLock.Unlock();
		publisher.EventDone( *sub, *(ebd.fEDD) );
		}
	    else
		{
		eventDataLock.Unlock();
		pthread_mutex_lock( &fStaticEDDMutex );
		fStaticEDD.fEventID = eventID;
		publisher.EventDone( *sub, fStaticEDD );
		pthread_mutex_unlock( &fStaticEDDMutex );
		}
	    return 0;
	    }
    else if ( fIncompleteEventLog.FindElement( eventID, evNdx ) )
	{
	eventFound = true;
	LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "Event already done incompletely" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") already done incompletely." << ENDLOG;
	eventDataLock.Unlock();
	
	pthread_mutex_lock( &fStaticEDDMutex );
	fStaticEDD.fEventID = eventID;
	publisher.EventDone( *sub, fStaticEDD );
	pthread_mutex_unlock( &fStaticEDDMutex );
	return 0;
	}
    else
	{
	// Search for it, add if not present
	// After acquiring write lock search again and remove; 
	// if not found, it is clear that somebody else just is in the process of adding it, so we jump to the beginning again;
	// This structure should hopefully never contain more than a handful of events, and can therefore be protected by a normal mutex
#if 0
	eventDataLock.Unlock();
	MLUCMultiReaderMutex::TWriteLocker eventDataWriteLock( fEventMutex );
#endif

	    {
	    MLUCMutex::TLocker queueLock( fQueuedEventMutex );
	    unsigned long tmpNdx;
	    if ( !fQueuedEvents.FindElement( eventID, tmpNdx ) )
		{
		fQueuedEvents.Add( eventID, eventID );
		}
	    }

	MLUCMultiReaderMutex::TWriteLocker eventDataWriteLock( eventDataLock );

	    {
	    MLUCMutex::TLocker queueLock( fQueuedEventMutex );
	    unsigned long tmpNdx;
	    if ( fQueuedEvents.FindElement( eventID, tmpNdx ) )
		fQueuedEvents.Remove( tmpNdx );
	    else
		continue; // Already added by some other thread, try again to find it...
	    }

#if 0
	// Test again whether event has been added in the meantime before we obtained the write lock
	if ( fEvents.FindElementReverse( eventID, evNdx ) )
	    continue; // Try again to find it...
	unsigned long tmpEvNdx=~0UL;
	if ( fIncompleteEventLog.FindElement( eventID, tmpEvNdx ) )
	    continue; // Try again to find it...
	if ( fEDDBacklog.FindElement( eventID, tmpEvNdx ) )
	    continue; // Try again to find it...
#endif
	LOG( AliHLTLog::kInformational, "AliHLTEventMergerNew::NewEvent", "First Event Part" )
	    << "Retrieved first event part for event 0x" << AliHLTLog::kHex
	    << eventID << " (" << AliHLTLog::kDec << eventID << ") with "
	    << sbevent.fDataBlockCount << " data blocks." << ENDLOG;

	unsigned long highestSubscriberNdx=0;
	if ( eventID.fType==kAliEventTypeStartOfRun )
	    fSOREventReceived = true;

	    {
	    MLUCMultiReaderMutex::TReadLocker subscriberLock( fSubscriberMutex );
	    highestSubscriberNdx = fHighestSubscriberNdx;
	    if ( subNdx <= fHighestSubscriberNdx && (fExpectedSubscribersBitmap[GetBitmapWordIndex(subNdx)] & GetBitmapBitInWord(subNdx)) )
		found = true;
	    }

	if ( !found )
	    {
	    // No part of this event from this subscriber is expected.
	    LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "Unexpected First Event Part" )
		<< "Received unexpected first event part " << AliHLTLog::kDec << subNdx << " from publisher "
		<< publisher.GetName() << " for event 0x" << AliHLTLog::kHex
		<< eventID << " (" << AliHLTLog::kDec << eventID << ") (HighestExpectedPartNdx: " << AliHLTLog::kDec
		<< fHighestSubscriberNdx << " - Bitmap word index: " << GetBitmapWordIndex(subNdx) << " - Bitmap position in word: "
		<< GetBitmapBitInWord(subNdx) << ")." << ENDLOG;
		{
		MLUCMultiReaderMutex::TReadLocker subscriberLock( fSubscriberMutex );
		LogBitmap( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "Scheduling incomplete event - Expected Part Bitmap", fExpectedSubscribersBitmap, fExpectedSubscribersBitmapLength );
		}
	    if ( subNdx > fHighestSubscriberNdx )
		{
		LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "Unexpected Event Part" )
		    << "Cannot add unexpected event part " << AliHLTLog::kDec << subNdx << " from publisher "
		    << publisher.GetName() << " for event 0x" << AliHLTLog::kHex
		    << eventID << " (" << AliHLTLog::kDec << eventID << ") - event part will be ignored... " 
		    << ENDLOG;
		return 0;
		}
	    }
	// Event has not yet been received. We create the entry for it
	ed = (PEventData)fEventDataCache.Get();
	if ( !ed )
	    {
	    LOG( AliHLTLog::kError, "AliHLTEventMergerNew::NewEvent", "Out Of Memory" )
		<< "Out of memory allocating event data structure for event 0x"
		<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << ")."
		<< ENDLOG;
	    return ENOMEM;
	    }
	ed->fEventID = eventID;
	unsigned long tmpLen = 0;
	ed->fHighestExpectedPartNdx = highestSubscriberNdx;
	ed->fExpectedPartBitmap = NULL;
	gettimeofday( &ed->fFirstPartReceive, NULL );
	unsigned long expSubBitmapLength=0;
#ifdef MERGED_BITMAPS
	if ( !AllocateBitmap( highestSubscriberNdx*kMaxParts, ed->fExpectedPartBitmap, expSubBitmapLength ) )
#else
	if ( !AllocateBitmap( highestSubscriberNdx, ed->fExpectedPartBitmap, expSubBitmapLength ) )
#endif
	    {
	    LOG( AliHLTLog::kError, "AliHLTEventMergerNew::NewEvent", "Out Of Memory" )
		<< "Out of memory allocating expected part bitmap for event 0x"
		<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << ")."
		<< ENDLOG;
	    fEventDataCache.Release( ed );
	    return ENOMEM;
	    }
	    {
	    MLUCMultiReaderMutex::TReadLocker subscriberLock( fSubscriberMutex );
#ifdef MERGED_BITMAPS
	    unsigned long actualBtmapLength = 1+highestSubscriberNdx/(sizeof(unsigned long)*8);
	    memcpy( ed->fBitmaps, fExpectedSubscribersBitmap, actualBtmapLength );
	    memset( ed->fBitmaps, 0, expSubBitmapLength*sizeof(unsigned long)-actualBtmapLength );
#else
	    memcpy( ed->fExpectedPartBitmap, fExpectedSubscribersBitmap, expSubBitmapLength*sizeof(unsigned long) );
#endif
	    ed->fSubscribersExpected = fExpectedSubscriberCnt;
	    }
#ifdef MERGED_BITMAPS
	if ( subNdx <= fHighestSubscriberNdx && !(ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kExpectedParts+subNdx)] & GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kExpectedParts+subNdx)) )
#else
	if ( subNdx <= fHighestSubscriberNdx && !(ed->fExpectedPartBitmap[GetBitmapWordIndex(subNdx)] & GetBitmapBitInWord(subNdx)) )
#endif
	    {
	    // Part not expected by default, we received it and so have to set the expected bit...
#ifdef MERGED_BITMAPS
	    ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kExpectedParts+subNdx)] |= GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kExpectedParts+subNdx);
#else
	    ed->fExpectedPartBitmap[GetBitmapWordIndex(subNdx)] |= GetBitmapBitInWord(subNdx);
#endif
	    ed->fSubscribersExpected++;
	    }
	if ( fAliceHLT && origets.fHeader.fLength == sizeof(AliHLTHLTEventTriggerData) && eventID.fType==kAliEventTypeData )
	    {
	      AliUInt64_t triggerClasses[2]; 
	      triggerClasses[0] = AliHLTGetDDLHeaderTriggerClassesLow50(reinterpret_cast<AliHLTHLTEventTriggerData const*>(&origets)->fCommonHeader);
	      triggerClasses[1]= AliHLTGetDDLHeaderTriggerClassesHigh50(reinterpret_cast<AliHLTHLTEventTriggerData const*>(&origets)->fCommonHeader);
	    AliUInt64_t detectorPattern = AliHLTGetDDLHeaderPartSubDet(reinterpret_cast<AliHLTHLTEventTriggerData const*>(&origets)->fCommonHeader);
	    AliUInt32_t L1msg = AliHLTGetDDLHeaderL1TriggerType(reinterpret_cast<AliHLTHLTEventTriggerData const*>(&origets)->fCommonHeader);
	    LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "CDH data" )
	      << "CDH data: Trigger classes: 0x" << triggerClasses[1] << " 0x" << triggerClasses[1] << " - detectorPattern: 0x" << detectorPattern
		<< " - L1msg: 0x" << L1msg << ENDLOG;
	    if ( (triggerClasses[0] || triggerClasses[1]) || ((L1msg & 0x1) && detectorPattern) ) // ignore trigger classes, if all zero; do not ignore software events where the detector pattern is non-zero
		{
		AliHLTReadoutListData rdlDataEvt;
		AliHLTReadoutList rdLstEvt(gReadoutListVersion,rdlDataEvt);
		unsigned long tcCnt = 0;
		
		if ( (L1msg & 0x1) && detectorPattern ) // software events; set all bits for each detector in the detector pattern
		    {
		    rdLstEvt.Reset();
		    for ( TDetectorID detID=kFIRSTID; detID<kENDID; detID = (TDetectorID)((int)detID + 1) )
			{
			if ( detectorPattern & (1 << (unsigned)detID) )
			    {
			    rdLstEvt.SetDetectorDDLs( detID );
			    ++tcCnt;
			    }
			}
		    }
		else
		  tcCnt = GetTriggerClassMaskContributingDDLs( triggerClasses, rdlDataEvt ); // physics events, triggerClass field should be correct

		if ( tcCnt>0 )
		    {

		    volatile_hltreadout_uint32_t** ddlLists(NULL);
		    if ( GetSubscriberDDLLists( ddlLists ) )
			{
		    
			AliHLTReadoutListData rdlData;
			AliHLTReadoutList rdLst(gReadoutListVersion,rdlData);
			
			for ( unsigned long ndx=0; ndx<=fHighestSubscriberNdx; ndx++ )
			    {
			    if ( ndx == subNdx )
				continue; // We skip this subscriber, as it has obviously contributed...
#ifdef MERGED_BITMAPS
			    if ( (ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kExpectedParts+ndx)] & GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kExpectedParts+ndx)) &&
#else
			    if ( (ed->fExpectedPartBitmap[GetBitmapWordIndex(ndx)] & GetBitmapBitInWord(ndx)) &&
#endif
				 ddlLists[ndx] )
				{
				AliHLTReadoutList rdLstSub(gReadoutListVersion,ddlLists[ndx]);
				rdLst.Reset();
				rdLst = rdLstSub; // Copy DDL list contributing to this subscriber input
				rdLst &= rdLstEvt; // Perform bitwise (DDL wise) AND with input list for all trigger classes for this event
				if ( !rdLst ) // Check if at least one DDL matches
				    {
				    // If not, deactivate this subscriber input for this event by disabling the bit
#ifdef MERGED_BITMAPS
				    ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kExpectedParts+ndx)] &= ~GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kExpectedParts+ndx);
#else
				    ed->fExpectedPartBitmap[GetBitmapWordIndex(ndx)] &= ~GetBitmapBitInWord(ndx);
#endif
				    ed->fSubscribersExpected--;
				    }
				}
			    }
			FreeSubscriberDDLLists( ddlLists );
			}
		    else
			{
			LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "Cannot allocate contributing DDL list list" )
			    << "Cannot allocate list of contributing DDL lists for event 0x"
			    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << ")."
			    << ENDLOG;
			}
		    }
		else
		    {
		    if ( GetTriggerClassCnt() )
			{
			LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "No matching trigger classes" )
			    << "No matching trigger classes found for event 0x"
			    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << ") (trigger class field: 0x"
			    << AliHLTLog::kHex << triggerClasses[1] << " 0x" << triggerClasses[0] << ")."
			    << ENDLOG;
			}
		    }
		}
	    }
	for ( unsigned long ndx=0; ndx<=fHighestSubscriberNdx; ndx++ )
	    {
	    if ( ndx == subNdx )
		continue; // We skip this subscriber, as it has obviously contributed...
#ifdef MERGED_BITMAPS
	    if ( (ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kExpectedParts+ndx)] & GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kExpectedParts+ndx)) && !(sbevent.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST) )
#else
	    if ( (ed->fExpectedPartBitmap[GetBitmapWordIndex(ndx)] & GetBitmapBitInWord(ndx)) && !(sbevent.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST) )
#endif
		{
		bool found=true;
		if ( fSubscribers[ndx].fModuloCnt )
		    {
		    found = false;
		    for ( unsigned long nn=0; nn<fSubscribers[ndx].fModuloCnt; nn++ )
			{
			if ( fEventModuloCalculation.ModuloMatch( fEventCount, eventID, fSubscribers[ndx].fModulos[nn] ) )
			    {
			    found = true;
			    break;
			    }
			}
		    }
		if ( found && fSubscribers[ndx].fTriggerCnt )
		    {
		    found = false;
		    for ( unsigned long nn=0; nn<fSubscribers[ndx].fTriggerCnt; nn++ )
			{
			if ( fETTCompFunc( fETTCompFuncParam, fSubscribers[ndx].fTriggers[nn], &origets ) )
			    {
			    found = true;
			    break;
			    }
			}
		    }
		if ( !found )
		    {
#ifdef MERGED_BITMAPS
		    ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kExpectedParts+ndx)] &= ~GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kExpectedParts+ndx);
#else
		    ed->fExpectedPartBitmap[GetBitmapWordIndex(ndx)] &= ~GetBitmapBitInWord(ndx);
#endif
		    ed->fSubscribersExpected--;
		    }
		}
	    }
#ifndef MERGED_BITMAPS
	tmpLen = 0;
	ed->fCanceledPartBitmap = NULL;
	if ( !AllocateBitmap( fHighestSubscriberNdx, ed->fCanceledPartBitmap, tmpLen ) )
	    {
	    LOG( AliHLTLog::kError, "AliHLTEventMergerNew::NewEvent", "Out Of Memory" )
		<< "Out of memory allocating canceled part bitmap for event 0x"
		<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << ")."
		<< ENDLOG;
	    ReleaseBitmap( ed->fExpectedPartBitmap );
	    fEventDataCache.Release( ed );
	    return ENOMEM;
	    }
	memset( ed->fCanceledPartBitmap, 0, tmpLen*sizeof(unsigned long) );
	tmpLen = 0;
	ed->fReceivedPartBitmap = NULL;
	if ( !AllocateBitmap( fHighestSubscriberNdx, ed->fReceivedPartBitmap, tmpLen ) )
	    {
	    LOG( AliHLTLog::kError, "AliHLTEventMergerNew::NewEvent", "Out Of Memory" )
		<< "Out of memory allocating received part bitmap for event 0x"
		<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << ")."
		<< ENDLOG;
	    ReleaseBitmap( ed->fExpectedPartBitmap );
	    ReleaseBitmap( ed->fCanceledPartBitmap );
	    fEventDataCache.Release( ed );
	    return ENOMEM;
	    }
	memset( ed->fReceivedPartBitmap, 0, tmpLen*sizeof(unsigned long) );
#endif
	
	if ( fLastBlockCnt<ed->fSubscribersExpected )
	    ed->fDataBlocks.SetPreAlloc( ed->fSubscribersExpected );
	else
	    ed->fDataBlocks.SetPreAlloc( fLastBlockCnt );
	ed->fEventBirth_s = sbevent.fEventBirth_s;
	ed->fEventBirth_us = sbevent.fEventBirth_us;
	ed->fOldestEventBirth_s = sbevent.fOldestEventBirth_s;
	ed->fStatusFlags = sbevent.fStatusFlags;
	ed->fState = kIncomplete;
	fEventCount++;
	    {
	    MLUCMutex::TLocker countLock( fEventCountMutex );
	    fIncompleteEventCount++;
	    }
	eventLock.Set( ed->fMutex );
	fEvents.Add( ed, eventID, evNdx );
	eventFound = true;
	if ( fStatus )
	    {
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    if ( eventID.fType!=kAliEventTypeTick && eventID.fType!=kAliEventTypeReconfigure && eventID.fType!=kAliEventTypeDCSUpdate )
		 fStatus->fHLTStatus.fReceivedEventCount++;
		{
		MLUCMutex::TLocker countLock( fEventCountMutex );
		fStatus->fHLTStatus.fPendingInputEventCount = fIncompleteEventCount;
		}
	    fStatus->Touch();
	    }
	if ( fEventTimeout_ms )
	    {
	    TimeoutData* td = fTimeoutDataCache.Get();
	    if ( td )
		{
		td->fEventID = eventID;
		td->fTimeout = fEventTimeout_ms;
		fTimer.AddTimeout( td->fTimeout, &fEventTimeoutCallback, reinterpret_cast<AliUInt64_t>( td ) );
		}
	    else
		{
		LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "Cannot add event timeout" )
		    << "Cannot add timeout for event 0x" << AliHLTLog::kHex << eventID << " ("
		    << AliHLTLog::kDec << eventID << ") (Timeout data cache out of memory)." << ENDLOG;
		}
	    }
	}
	}
    while (!eventFound );
#if 0
    if ( ed->fExpectedPartBitmap[0] != 3 )
	{
	MLUCString str;
	fUnfinishedEventData.AsString( str );
	LOG( AliHLTLog::kFatal, "AliHLTEventMergerNew::NewEvent", "Internal Error" )
	    << "Internal Error: ed->fExpectedPartBitmap[0] != 3 - eventID: 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
	    << "). fUnfinishedEventData dump: \n" << str.c_str() << "\n." << ENDLOG;
	LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Expected part Bitmap", ed->fExpectedPartBitmap, fExpectedSubscribersBitmapLength );
	LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Expected subscribers Bitmap", fExpectedSubscribersBitmap, fExpectedSubscribersBitmapLength );
	}
#endif
    
    if ( subNdx <= ed->fHighestExpectedPartNdx )
	{
#ifdef MERGED_BITMAPS
	if ( ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kReceivedParts+subNdx)] & GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kReceivedParts+subNdx) )
#else
	if ( ed->fReceivedPartBitmap[GetBitmapWordIndex(subNdx)] & GetBitmapBitInWord(subNdx) )
#endif
	    {
	    // A part of this event has already been received by this subscriber. Shouldn't happen.
	    LOG( AliHLTLog::kInformational, "AliHLTEventMergerNew::NewEvent", "Event already received" )
		<< "Event 0x" << AliHLTLog::kHex
		<< eventID << " (" << AliHLTLog::kDec << ") already received from subscriber '"
		<< sub->GetName() << "'/" << subNdx << "." << ENDLOG;
	    return 0;
	    }
	else
	    {
#if 0
	    LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Received Part Bitmap 4a", ed->fReceivedPartBitmap, fExpectedSubscribersBitmapLength );
#endif
#ifdef MERGED_BITMAPS
	    ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kReceivedParts+subNdx)] |= GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kReceivedParts+subNdx);
#else
	    ed->fReceivedPartBitmap[GetBitmapWordIndex(subNdx)] |= GetBitmapBitInWord(subNdx);
#endif
#if 0
	    LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Received Part Bitmap 4b", ed->fReceivedPartBitmap, fExpectedSubscribersBitmapLength );
#endif
	    ed->fSubscribersPresent++;
	    }
	}

#if 0
    if ( ed->fExpectedPartBitmap[0] != 3 )
	{
	MLUCString str;
	fUnfinishedEventData.AsString( str );
	LOG( AliHLTLog::kFatal, "AliHLTEventMergerNew::NewEvent", "Internal Error" )
	    << "Internal Error: ed->fExpectedPartBitmap[0] != 3 - eventID: 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
	    << "). fUnfinishedEventData dump: \n" << str.c_str() << "\n." << ENDLOG;
	LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Expected part Bitmap", ed->fExpectedPartBitmap, fExpectedSubscribersBitmapLength );
	LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Expected subscribers Bitmap", fExpectedSubscribersBitmap, fExpectedSubscribersBitmapLength );
	}
    LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Expected Part Bitmap 2", ed->fExpectedPartBitmap, fExpectedSubscribersBitmapLength );
    LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Received Part Bitmap 2", ed->fReceivedPartBitmap, fExpectedSubscribersBitmapLength );

    if ( ed->fExpectedPartBitmap[0] == 0 )
	{
	MLUCString str;
	fUnfinishedEventData.AsString( str );
	LOG( AliHLTLog::kFatal, "AliHLTEventMergerNew::NewEvent", "Internal Error" )
	    << "Internal Error: ed->fExpectedPartBitmap[0] == 0 - eventID: 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
	    << "). fUnfinishedEventData dump: \n" << str.c_str() << "\n." << ENDLOG;
	}
    if ( ed->fExpectedPartBitmap[0] != 3 )
	{
	MLUCString str;
	fUnfinishedEventData.AsString( str );
	LOG( AliHLTLog::kFatal, "AliHLTEventMergerNew::NewEvent", "Internal Error" )
	    << "Internal Error: ed->fExpectedPartBitmap[0] != 3 - eventID: 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
	    << "). fUnfinishedEventData dump: \n" << str.c_str() << "\n." << ENDLOG;
	LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Expected part Bitmap", ed->fExpectedPartBitmap, fExpectedSubscribersBitmapLength );
	LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Expected subscribers Bitmap", fExpectedSubscribersBitmap, fExpectedSubscribersBitmapLength );
	}
#endif


    unsigned long i;
    DataBlock db;
    db.fSubscriberNdx = subNdx;
    unsigned long blockNdx=~0UL;
#if 0
    bool SORBlockAdded = false;
    bool EORBlockAdded = false;
    bool runtypeBlockAdded=false;
    for ( i = 0; i < sbevent.fDataBlockCount; i++ )
	{
	if ( !(sbevent.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST) || 
	     ( !MLUCVectorSearcher<DataBlock,AliHLTSubEventDataBlockDescriptor>::FindElement( ed->fDataBlocks, FindDataBlock, sbevent.fDataBlocks[i], blockNdx ) 
	       && (!SORBlockAdded || sbevent.fDataBlocks[i].fDataType.fID != STARTOFRUN_DATAID || sbevent.fDataBlocks[i].fDataOrigin.fID != PRIV_DATAORIGIN )
	       && (!EORBlockAdded || sbevent.fDataBlocks[i].fDataType.fID != ENDOFRUN_DATAID || sbevent.fDataBlocks[i].fDataOrigin.fID != PRIV_DATAORIGIN )
	       && (!runtypeBlockAdded || sbevent.fDataBlocks[i].fDataType.fID != RUNTYPE_DATAID || sbevent.fDataBlocks[i].fDataOrigin.fID != PRIV_DATAORIGIN ) ) )
	    {
	    db.fBlockData = sbevent.fDataBlocks[i];
	    ed->fDataBlocks.Add( db );
	    if ( sbevent.fDataBlocks[i].fDataType.fID == STARTOFRUN_DATAID && sbevent.fDataBlocks[i].fDataOrigin.fID == PRIV_DATAORIGIN )
		SORBlockAdded = true;
	    if ( sbevent.fDataBlocks[i].fDataType.fID == ENDOFRUN_DATAID && sbevent.fDataBlocks[i].fDataOrigin.fID == PRIV_DATAORIGIN )
		EORBlockAdded = true;
	    if ( sbevent.fDataBlocks[i].fDataType.fID == RUNTYPE_DATAID && sbevent.fDataBlocks[i].fDataOrigin.fID == PRIV_DATAORIGIN )
		runtypeBlockAdded = true;
	    }
	}
#else
    // Cover non Start/End-Of-Run/Data specific blocks
    for ( i = 0; i < sbevent.fDataBlockCount; i++ )
	{
	if ( !(sbevent.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST) || 
	     ( !MLUCVectorSearcher<DataBlock,AliHLTSubEventDataBlockDescriptor>::FindElement( ed->fDataBlocks, FindDataBlock, sbevent.fDataBlocks[i], blockNdx ) 
	       && (sbevent.fDataBlocks[i].fDataType.fID != STARTOFRUN_DATAID || sbevent.fDataBlocks[i].fDataOrigin.fID != PRIV_DATAORIGIN )
	       && (sbevent.fDataBlocks[i].fDataType.fID != ENDOFRUN_DATAID || sbevent.fDataBlocks[i].fDataOrigin.fID != PRIV_DATAORIGIN )
	       && (sbevent.fDataBlocks[i].fDataType.fID != RUNTYPE_DATAID || sbevent.fDataBlocks[i].fDataOrigin.fID != PRIV_DATAORIGIN )
	       && (sbevent.fDataBlocks[i].fDataType.fID != ECSPARAM_DATAID || sbevent.fDataBlocks[i].fDataOrigin.fID != PRIV_DATAORIGIN ) 
	       && (sbevent.fDataBlocks[i].fDataType.fID != DCSUPDATE_DATAID || sbevent.fDataBlocks[i].fDataOrigin.fID != PRIV_DATAORIGIN ) 
	       && (sbevent.fDataBlocks[i].fDataType.fID != COMPONENTCONFIGIDS_DATAID || sbevent.fDataBlocks[i].fDataOrigin.fID != PRIV_DATAORIGIN ) 
	       && (sbevent.fDataBlocks[i].fDataType.fID != COMPONENTCONFIGCOMPS_DATAID || sbevent.fDataBlocks[i].fDataOrigin.fID != PRIV_DATAORIGIN ) 
	       && (sbevent.fDataBlocks[i].fDataType.fID != COMPONENTCONFIGPATHS_DATAID || sbevent.fDataBlocks[i].fDataOrigin.fID != PRIV_DATAORIGIN ) ) )
	    {
	    db.fBlockData = sbevent.fDataBlocks[i];
	    ed->fDataBlocks.Add( db );
	    }
	}

    // Start/End-Of-Run/Data specific blocks, only one block from STARTOFRUN, ENDOFRUN, RUNTYPE types, independent of originator, block size, data spec., ...
    if ( eventID.fType==kAliEventTypeStartOfRun || eventID.fType==kAliEventTypeEndOfRun || eventID.fType==kAliEventTypeSync )
	{
	for ( i = 0; i < sbevent.fDataBlockCount; i++ )
	    {
	    if ( (sbevent.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST)
		 && (sbevent.fDataBlocks[i].fDataType.fID == STARTOFRUN_DATAID 
		     || sbevent.fDataBlocks[i].fDataType.fID == ENDOFRUN_DATAID
		     || sbevent.fDataBlocks[i].fDataType.fID == RUNTYPE_DATAID
		     || sbevent.fDataBlocks[i].fDataType.fID == ECSPARAM_DATAID) 
		 && sbevent.fDataBlocks[i].fDataOrigin.fID == PRIV_DATAORIGIN 
		 && !MLUCVectorSearcher<DataBlock,AliHLTSubEventDataBlockDescriptor>::FindElement( ed->fDataBlocks, FindxORDataBlock, sbevent.fDataBlocks[i], blockNdx ) )
		{
		db.fBlockData = sbevent.fDataBlocks[i];
		ed->fDataBlocks.Add( db );
		
		}
	    
	    }
	}
    else if ( eventID.fType==kAliEventTypeDCSUpdate )
	{
	for ( i = 0; i < sbevent.fDataBlockCount; i++ )
	    {
	    if ( (sbevent.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST)
		 && (sbevent.fDataBlocks[i].fDataType.fID == DCSUPDATE_DATAID ) 
		 && sbevent.fDataBlocks[i].fDataOrigin.fID == PRIV_DATAORIGIN 
		 && !MLUCVectorSearcher<DataBlock,AliHLTSubEventDataBlockDescriptor>::FindElement( ed->fDataBlocks, FindxORDataBlock, sbevent.fDataBlocks[i], blockNdx ) )
		{
		db.fBlockData = sbevent.fDataBlocks[i];
		ed->fDataBlocks.Add( db );
		}
	    }
	}
    else if ( eventID.fType==kAliEventTypeReconfigure )
	{
	for ( i = 0; i < sbevent.fDataBlockCount; i++ )
	    {
	    if ( (sbevent.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST)
		 && (sbevent.fDataBlocks[i].fDataType.fID == COMPONENTCONFIGIDS_DATAID 
		     || sbevent.fDataBlocks[i].fDataType.fID == COMPONENTCONFIGCOMPS_DATAID
		     || sbevent.fDataBlocks[i].fDataType.fID == COMPONENTCONFIGPATHS_DATAID) 
		 && sbevent.fDataBlocks[i].fDataOrigin.fID == PRIV_DATAORIGIN 
		 && !MLUCVectorSearcher<DataBlock,AliHLTSubEventDataBlockDescriptor>::FindElement( ed->fDataBlocks, FindxORDataBlock, sbevent.fDataBlocks[i], blockNdx ) )
		{
		db.fBlockData = sbevent.fDataBlocks[i];
		ed->fDataBlocks.Add( db );
		}
	    }
	}

#endif


    TETSMergeMode mergeMode = fETSMergeMode;
    if ( fAliceHLT )
	mergeMode=kHLTETS;
    if ( mergeMode==kHLTETS && origets.fHeader.fLength != sizeof(AliHLTHLTEventTriggerData) )
	{
	mergeMode = kAppendETS;
	if ( eventID.fType!=kAliEventTypeTick && eventID.fType!=kAliEventTypeReconfigure && eventID.fType!=kAliEventTypeDCSUpdate )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "No HLT Trigger Information" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << "): Trigger information has wrong size (expected: " << AliHLTLog::kDec << sizeof(AliHLTHLTEventTriggerData)
		<< " bytes - received: " << origets.fHeader.fLength << " bytes). Using basic append trigger merge mode." << ENDLOG;
	    }
	}
    switch ( fETSMergeMode )
	{
	case kAppendETS:
	    {
	    unsigned long etsLength = oldETSLength+origets.fDataWordCount*sizeof(origets.fDataWords[0]);
	    ets = (AliHLTEventTriggerStruct*)fETSCache.Get( etsLength );
	    if ( !ets )
		{
		// XXX
		LOG( AliHLTLog::kError, "AliHLTEventMergerNew::NewEvent", "Out Of Memory" )
		    << "Out of memory allocating new event trigger structure of "
		    << AliHLTLog::kDec << etsLength << " bytes for event 0x" << AliHLTLog::kHex
		    << eventID << " (" << AliHLTLog::kDec << eventID 
		    << "). Skipping event trigger contribution of subscriber "
		    << subNdx << "." << ENDLOG;
		}
	    else
		{
		if ( oldETS )
		    memcpy( ets, oldETS, oldETSLength );
		else
		    memcpy( ets, &fStaticETS, sizeof(AliHLTEventTriggerStruct) );
		memcpy( ets->fDataWords+ets->fDataWordCount, origets.fDataWords, origets.fDataWordCount*sizeof(origets.fDataWords[0]) );
		ets->fDataWordCount += origets.fDataWordCount;
		ets->fHeader.fLength = etsLength;
		if ( oldETS )
		    fETSCache.Release( (uint8*)oldETS );
		ed->fETS = ets;
		}
	    }
	    break;
	case kHLTETS:
	    {
	    if ( !oldETS )
		{
		unsigned long etsLength = sizeof(AliHLTHLTEventTriggerData);
		ets = (AliHLTEventTriggerStruct*)fETSCache.Get( etsLength );
		if ( !ets )
		    {
		    // XXX
		    LOG( AliHLTLog::kError, "AliHLTEventMergerNew::NewEvent", "Out Of Memory" )
			<< "Out of memory allocating new event trigger structure of "
			<< AliHLTLog::kDec << etsLength << " bytes for event 0x" << AliHLTLog::kHex
			<< eventID << " (" << AliHLTLog::kDec << eventID 
			<< "). Skipping event trigger contribution of subscriber "
			<< subNdx << "." << ENDLOG;
		    }
		else
		    {
		    oldETS = ets;
		    AliHLTHLTEventTriggerData* hetd = reinterpret_cast<AliHLTHLTEventTriggerData*>( ets );
		    memset( hetd, 0, sizeof(*hetd) );
		    hetd->Fill();
		    hetd->ResetCachedObject();
		    memcpy( hetd->fCommonHeader, ((AliHLTHLTEventTriggerData&)origets).fCommonHeader, sizeof(hetd->fCommonHeader[0])*hetd->fCommonHeaderWordCnt );
		    ed->fETS = ets;
		    }
		}
	    AliHLTHLTMergeEventTriggerData( reinterpret_cast<AliHLTHLTEventTriggerData*>( oldETS ), reinterpret_cast<AliHLTHLTEventTriggerData const*>( &origets ) );
	    }
	    break;
	}
    
    bool announce=false;
    LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Subscriber Counts" )
	<< "Event 0x" << AliHLTLog::kHex
	<< eventID << " (" << AliHLTLog::kDec << eventID 
	<< "): subscribers present: " << AliHLTLog::kDec << ed->fSubscribersPresent
	<< " - subscribers expected: " << ed->fSubscribersExpected << "." << ENDLOG;
    PEventData announceED=ed;
    std::vector<PEventData> announceEDs;
    //announceEDs.reserve( 1 );
    if ( fDynamicTimeoutAdaption && ed->fSubscribersPresent >= ed->fSubscribersExpected )
        AddEventReceiveInterval( ed->fFirstPartReceive );
    if ( ed->fSubscribersPresent >= ed->fSubscribersExpected && (fSOREventCompleted || (eventID.fType!=kAliEventTypeData && eventID.fType!=kAliEventTypeEndOfRun && eventID.fType!=kAliEventTypeSync) || !fAliceHLT || (!fStrictSORRequirements && !fSOREventReceived)) )
	{
	if ( eventID.fType==kAliEventTypeStartOfRun )
	    fSOREventCompleted = true;

	ed->fBusy = true;
	fLastBlockCnt = ed->fDataBlocks.GetCnt();
	announce = true;
	ed->fState = kComplete;
	eventLock.Unlock();
	if ( eventID.fType != kAliEventTypeStartOfRun && eventID.fType != kAliEventTypeEndOfRun )
	    {
	    MLUCMutex::TLocker countLock( fEventCountMutex );
	    fIncompleteEventCount--;
	    fCompleteEventCount++;
	    }
	if ( fStatus )
	    {
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    if ( eventID.fType!=kAliEventTypeTick && eventID.fType!=kAliEventTypeReconfigure && eventID.fType!=kAliEventTypeDCSUpdate )
		++(fStatus->fHLTStatus.fProcessedEventCount);
	    fStatus->Touch();
	    }

	if ( eventID.fType == kAliEventTypeEndOfRun )
	    {
	    unsigned long cnt;
		{
		MLUCMutex::TLocker countLock( fEventCountMutex );
		cnt = fIncompleteEventCount;
		}
	    MLUCMultiReaderMutex::TWriteLocker eventDataWriteLock( eventDataLock );
	    announceEDs.reserve( cnt+1 );
	    if ( cnt > 1 )
		{
		LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "End of run flushing" )
		    << "End of run event received completely. Flushing "
		    << AliHLTLog::kDec << cnt << " remaining incompletely received events." << ENDLOG;
		fEvents.Iterate( AliHLTEventMergerNew::GetIncompleteEventsIterationFunc, (void*)&announceEDs );
		vector<PEventData>::iterator eventIter, eventEnd;
		eventIter = announceEDs.begin();
		eventEnd = announceEDs.end();

		while ( eventIter!=eventEnd )
		    {
		    (*eventIter)->fBusy = true;
		    (*eventIter)->fState = kComplete;
			{
			MLUCMutex::TLocker countLock( fEventCountMutex );
			fIncompleteEventCount--;
			fCompleteEventCount++;
			}
		    if ( fStatus )
			{
			MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			if ( eventID.fType!=kAliEventTypeTick && eventID.fType!=kAliEventTypeReconfigure && eventID.fType!=kAliEventTypeDCSUpdate )
			    ++(fStatus->fHLTStatus.fProcessedEventCount);
			fStatus->Touch();
			}
		    ++eventIter;
		    }
		}
	    announceEDs.push_back( announceED ); // Make sure, end of run event is last event
	    announceED = NULL;
	    }


	if ( eventID.fType==kAliEventTypeStartOfRun )
	    {
		{
		MLUCMutex::TLocker countLock( fEventCountMutex );
		announceEDs.reserve( fIncompleteEventCount+1 );
		}
	    announceEDs.push_back( announceED ); // Make sure, start of run is first!!!
	    announceED = NULL;
	    MLUCMultiReaderMutex::TWriteLocker eventDataWriteLock( eventDataLock );
	    fEvents.Iterate( AliHLTEventMergerNew::AnnounceAnnounceableEventsIterationFunc, (void*)&announceEDs );
	    vector<PEventData>::iterator eventIter, eventEnd;
	    eventIter = announceEDs.begin();
	    eventEnd = announceEDs.end();
	    PEventData edTmp;
	    while ( eventIter != eventEnd )
		{
		//if ( fUnfinishedEventData.FindElement( AliHLTEventMergerNew::FindEventDataByID, *eventIter, unfinishedNdx ) )
		edTmp = *eventIter;
		LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Completed event not held back" )
		    << "Stopping holding back of completed event 0x" << AliHLTLog::kHex << edTmp->fEventID << " (" << AliHLTLog::kDec << edTmp->fEventID
		    << ")." << ENDLOG;
		edTmp->fState = kComplete;
		    {
		    MLUCMutex::TLocker countLock( fEventCountMutex );
		    fIncompleteEventCount--;
		    fCompleteEventCount++;
		    }
		if ( fStatus )
		    {
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    if ( eventID.fType!=kAliEventTypeTick && eventID.fType!=kAliEventTypeReconfigure && eventID.fType!=kAliEventTypeDCSUpdate )
			++(fStatus->fHLTStatus.fProcessedEventCount);
		    fStatus->Touch();
		    }
		eventIter++;
		}
	    if ( announceEDs.size()>1 )
		{
		LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::NewEvent", "Start of run flushing" )
		    << "Timeout expired for start of run event. Flushing "
		    << AliHLTLog::kDec << (announceEDs.size()-1) << " events received in the meantime." << ENDLOG;
		}
	    }

	if ( fStatus )
	    {
	    MLUCMutex::TLocker countLock( fEventCountMutex );
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fHLTStatus.fPendingInputEventCount = fIncompleteEventCount;
	    fStatus->Touch();
	    }
	}
    else if ( ed->fSubscribersPresent >= ed->fSubscribersExpected )
	{
	LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Holding back complete event" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
	    << ") completed but held back." << ENDLOG;
	if ( !fSOREventCompleted && fAliceHLT && fStrictSORRequirements && !fSOREventReceived ) //  && eventID.fType!=kAliEventTypeStartOfRun not necessary, only data and EOR events should arrive here...
	    {
	    LOG( AliHLTLog::kError, "AliHLTEventMergerNew::NewEvent", "Received Data Event before Start-Of-Run/Data" )
		<< "Data event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		<< ") received before Start-Of-Run/Data event on input "
		<< subNdx << "." << ENDLOG;
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		}
	    }
	}
#if 0
    if ( ed->fExpectedPartBitmap[0] != 3 )
	{
	MLUCString str;
	fUnfinishedEventData.AsString( str );
	LOG( AliHLTLog::kFatal, "AliHLTEventMergerNew::NewEvent", "Internal Error" )
	    << "Internal Error: ed->fExpectedPartBitmap[0] != 3 - eventID: 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
	    << "). fUnfinishedEventData dump: \n" << str.c_str() << "\n." << ENDLOG;
	LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Expected part Bitmap", ed->fExpectedPartBitmap, fExpectedSubscribersBitmapLength );
	LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Expected subscribers Bitmap", fExpectedSubscribersBitmap, fExpectedSubscribersBitmapLength );
	}
#endif
    eventDataLock.Unlock();
    eventLock.Unlock();
    if ( announce )
	{
	LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::NewEvent", "Scheduling event" )
	    << "Scheduling event 0x" << AliHLTLog::kHex
	    << eventID << " (" << AliHLTLog::kDec << eventID 
	    << ") for announcement." << ENDLOG;
	//fSignal.Lock();
	if ( announceED )
	    {
	    fSignal.AddNotificationData( announceED );
	    }
	else
	    {
	    unsigned long i, cnt = announceEDs.size();
	    for ( i=0; i<cnt; i++ )
		fSignal.AddNotificationData( announceEDs[i] );
	    }
	//fSignal.Unlock();
	fSignal.Signal();
	}
    return 0;
    }

// This method is called for a transient subscriber, 
// when an event has been removed from the list. 
int AliHLTEventMergerNew::EventCanceled( TSubscriber*, unsigned long ndx, 
					 AliHLTPublisherInterface&, 
					 AliEventID_t eventID )
    {
    LOG( AliHLTLog::kInformational, "AliHLTEventMergerNew::EventCanceled", "Event canceled" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << ") canceled." << ENDLOG;
    unsigned long evNdx;
    PEventData ed;
    MLUCMultiReaderMutex::TReadLocker eventDataLock( fEventMutex );
    if ( fEvents.FindElement( eventID, evNdx ) )
	{
	ed = fEvents.Get( evNdx );
#if 0
	if ( !ed )
	    {
	    MLUCString str;
	    fUnfinishedEventData.AsString( str );
	    LOG( AliHLTLog::kFatal, "AliHLTEventMergerNew::EventCanceled", "Internal Error" )
		<< "Internal Error: ed==0 - EventID: 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << "). fUnfinishedEventData duimp: \n" << str.c_str() << "\n." << ENDLOG;
	    }
 	if ( ed->fEventID != eventID )
	    {
	    MLUCString str;
	    fUnfinishedEventData.AsString( str );
	    LOG( AliHLTLog::kFatal, "AliHLTEventMergerNew::EventCanceled", "Internal Error" )
		<< "Internal Error: ed->fEventID != eventID - eventID: 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		<< ") - ed->fEventID: 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID 
		<< "). fUnfinishedEventData dump: \n" << str.c_str() << "\n." << ENDLOG;
	    }
#endif
	MLUCMutex::TLocker eventLock( ed->fMutex );
	if ( ed->fState==kIncomplete )
	    {
#ifdef MERGED_BITMAPS
	    if ( ndx<=ed->fHighestExpectedPartNdx && ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kExpectedParts+ndx)] & GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kExpectedParts+ndx) )
#else
	    if ( ndx<=ed->fHighestExpectedPartNdx && ed->fExpectedPartBitmap[GetBitmapWordIndex(ndx)] & GetBitmapBitInWord(ndx) )
#endif
		{
		ed->fCanceled = true;
#ifdef MERGED_BITMAPS
		ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kCanceledParts+ndx)] |= GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kCanceledParts+ndx);
#else
		ed->fCanceledPartBitmap[GetBitmapWordIndex(ndx)] |= GetBitmapBitInWord(ndx);
#endif
		}
	    }
	else if ( ed->fState==kComplete )
	    {
#ifdef MERGED_BITMAPS
	    if ( ndx<=ed->fHighestExpectedPartNdx && ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kExpectedParts+ndx)] & GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kExpectedParts+ndx) )
#else
	    if ( ndx<=ed->fHighestExpectedPartNdx && ed->fExpectedPartBitmap[GetBitmapWordIndex(ndx)] & GetBitmapBitInWord(ndx) )
#endif
		{
		ed->fCanceled = true;
#ifdef MERGED_BITMAPS
		ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kCanceledParts+ndx)] |= GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kCanceledParts+ndx);
#else
		ed->fCanceledPartBitmap[GetBitmapWordIndex(ndx)] |= GetBitmapBitInWord(ndx);
#endif
		}
	    }
	}
    // XXX
    return 0;
    }
	
// This method is called for a subscriber when new event done data is 
// received by a publisher (sent from another subscriber) for an event. 
int AliHLTEventMergerNew::EventDoneData( TSubscriber*, unsigned long, 
					 AliHLTPublisherInterface&, 
					 const AliHLTEventDoneData& )
    {
    return 0; // We will not receive anything anyway
    }

// This method is called when the subscription ends.
int AliHLTEventMergerNew::SubscriptionCanceled( TSubscriber*, unsigned long ndx, 
						AliHLTPublisherInterface& )
    {
    DisableSubscriber( ndx );
    return 0;
    }
	
// This method is called when the publishing 
// service runs out of buffer space. The subscriber 
// has to free as many events as possible and send an 
// EventDone message with their IDs to the publisher
int AliHLTEventMergerNew::ReleaseEventsRequest( TSubscriber*, unsigned long, 
						AliHLTPublisherInterface& )
    {
    fPublisher->RequestEventRelease();
    return 0;
    } 

// Method used to check alive status of subscriber. 
// This message has to be answered with 
// PingAck message within less  than a second.
int AliHLTEventMergerNew::Ping( TSubscriber* sub, unsigned long, 
				AliHLTPublisherInterface& publisher )
    {
    publisher.PingAck( *sub );
    return 0;
    }
	
// Method used by a subscriber in response to a Ping 
// received message. The PingAck message has to be 
// called within less than one second.
int AliHLTEventMergerNew::PingAck( TSubscriber*, unsigned long, 
				   AliHLTPublisherInterface& )
    {
    return 0;
    }


// Will be called when an
//event has already been internally canceled. 
void AliHLTEventMergerNew::CanceledEvent( TPublisher*, unsigned long,
					  AliEventID_t eventID, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    if ( fEventAccounting )
	fEventAccounting->EventFinished( fName.c_str(), eventID );
    unsigned long len;
    AliHLTEventDoneData* edd=NULL;
    len = AliHLTGetMergedEventDoneDataSize( eventDoneData );
    if ( len>sizeof(AliHLTEventDoneData) )
	{
	pthread_mutex_lock( &fEDDCacheMutex );
	edd = (AliHLTEventDoneData*)fEDDCache.Get( len );
	pthread_mutex_unlock( &fEDDCacheMutex );
	if ( !edd )
	    {
	    LOG( AliHLTLog::kError, "AliHLTEventMergerNew::CanceledEvent", "Out of memory" )
		<< "Out of memory trying to allocate event done data of"
		<< AliHLTLog::kDec << len << " bytes." << ENDLOG;
	    // XXX Retry??
	    }
	else
	    AliHLTMergeEventDoneData( eventID, edd, eventDoneData );
	}
//     if ( !edd )
// 	{
// 	fStaticEDD.fEventID = eventID;
// 	edd = &fStaticEDD;
// 	}
    unsigned long evNdx;
    PEventData ed;
    MLUCMultiReaderMutex::TWriteLocker eventWriteDataLock( fEventMutex );
    if ( fEvents.FindElement( eventID, evNdx ) )
	{
	ed = fEvents.Get( evNdx );
#if 0
	if ( !ed )
	    {
	    MLUCString str;
	    fFinishedEventData.AsString( str );
	    LOG( AliHLTLog::kFatal, "AliHLTEventMergerNew::CanceledEvent", "Internal Error" )
		<< "Internal Error: ed==0 - EventID: 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << "). fUnfinishedEventData duimp: \n" << str.c_str() << "\n." << ENDLOG;
	    }
 	if ( ed->fEventID != eventID )
	    {
	    MLUCString str;
	    fFinishedEventData.AsString( str );
	    LOG( AliHLTLog::kFatal, "AliHLTEventMergerNew::CanceledEvent", "Internal Error" )
		<< "Internal Error: ed->fEventID != eventID - eventID: 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		<< ") - ed->fEventID: 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID 
		<< "). fUnfinishedEventData dump: \n" << str.c_str() << "\n." << ENDLOG;
	    }
#endif
	MLUCMutex::TLocker eventLock( ed->fMutex );
	if ( ed->fState==kIncomplete )
	    {
	    LOG( AliHLTLog::kError, "AliHLTEventMergerNew::CanceledEvent", "Done event not complete" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		<< ") reported as done is not even complete yet..." << ENDLOG;
	    }
	if ( ed->fState==kDone )
	    {
	    LOG( AliHLTLog::kError, "AliHLTEventMergerNew::CanceledEvent", "Event done multiple times" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		<< ") reported as done is already set as done..." << ENDLOG;
	    }
	ed->fDone = true;
	ed->fState = kDone;
	ed->fEDD = edd;
	if ( !ed->fBusy )
	    ReleaseEvent( evNdx, eventWriteDataLock, eventLock );
	}
    else if ( !fEventsPurged )
	{
	// XXX
	LOG( AliHLTLog::kError, "AliHLTEventMergerNew::CanceledEvent", "Lost Event" )
	    << "Finished event 0x" << AliHLTLog::kHex << eventID << " ("
	    << AliHLTLog::kDec << eventID << ") lost." << ENDLOG;
	}
    if ( fStatus && fPublisher )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fHLTStatus.fPendingOutputEventCount = fPublisher->GetPendingEventCount();
	fStatus->Touch();
	}
    }

// empty callback. Will be called when an
//event has been announced. 
void AliHLTEventMergerNew::AnnouncedEvent( TPublisher*, unsigned long,
					   AliEventID_t, const AliHLTSubEventDataDescriptor&, 
					   const AliHLTEventTriggerStruct&, AliUInt32_t )
    {
    MLUCMultiReaderMutex::TReadLocker eventDataLock( fEventMutex );
    if ( fStatus && fPublisher )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fHLTStatus.fPendingOutputEventCount = fPublisher->GetPendingEventCount();
	fStatus->Touch();
	}
    }

// empty callback. Will be called when an event done message with non-trivial event done data
// (fDataWordCount>0) has been received for that event. 
// This is called prior to the sending of the EventDoneData to interested other subscribers
void AliHLTEventMergerNew::EventDoneDataReceived( TPublisher*, unsigned long,
						  const char*, AliEventID_t eventID,
						  const AliHLTEventDoneData* edd, bool forceForward, bool forwarded )
    {
    if ( fEventAccounting )
	fEventAccounting->EventFinished( fName.c_str(), eventID, forceForward ? 2 : 1, forwarded ? 2 : 1 );
    if ( forceForward && edd )
	{
	unsigned long evNdx;
	PEventData ed;
	MLUCMultiReaderMutex::TReadLocker eventDataLock( fEventMutex );
	if ( fEvents.FindElement( eventID, evNdx ) )
	    {
	    ed = fEvents.Get( evNdx );
	    MLUCMutex::TLocker eventLock( ed->fMutex );
#ifdef MERGED_BITMAPS
	    unsigned long *bitmaps = ed->fBitmaps;
#else
	    unsigned long *canceledBitmap = ed->fCanceledPartBitmap;
	    unsigned long *receivedBitmap = ed->fReceivedPartBitmap;
#endif
	    unsigned long highestPartNdx = ed->fHighestExpectedPartNdx;
	    eventDataLock.Unlock();
	    
	    register SubscriberData* iter;
	    unsigned long wordIndex, bitInWord;
	    MLUCMultiReaderMutex::TReadLocker subscriberLock( fSubscriberMutex );
	    iter = fSubscribers;
	    for ( unsigned long nn=0; nn<fSubscriberCnt; nn++, iter++ )
		{
		if ( iter->fNdx<=highestPartNdx )
		    {
#ifdef MERGED_BITMAPS
		    if ( (bitmaps[GetBitmapWordIndex(highestPartNdx*kReceivedParts+iter->fNdx)] & GetBitmapBitInWord(highestPartNdx*kReceivedParts+iter->fNdx)) && !(bitmaps[GetBitmapWordIndex(highestPartNdx*kCanceledParts+iter->fNdx)] & GetBitmapBitInWord(highestPartNdx*kCanceledParts+iter->fNdx)) && iter->fPublisher )
#else
		    wordIndex = GetBitmapWordIndex(iter->fNdx);
		    bitInWord = GetBitmapBitInWord(iter->fNdx);
		    if ( (receivedBitmap[wordIndex] & bitInWord) && !(canceledBitmap[wordIndex] & bitInWord) && iter->fPublisher )
#endif
			{
			if ( fEventAccounting )
			    fEventAccounting->EventFinished( iter->fPublisher->GetName(), eventID, forceForward ? 2 : 1, 2 );
			iter->fPublisher->EventDone( *(iter->fSubscriber), *edd, true, true );
			}
		    }
		}
	    }
	}
    }

// Empty callback, called when an already announced event has to be announced again to a new
// subscriber that wants to receive old events. This method needs to return the pointers
// to the sub-event data descriptor and the event trigger structure. If the function returns
// false this is a signal that the desired data could not be found anymore. 
bool AliHLTEventMergerNew::GetAnnouncedEventData( TPublisher*, unsigned long,
						  AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
						  AliHLTEventTriggerStruct*& trigger )
    {
    unsigned long evNdx;
    PEventData ed;
    bool found = false;
    MLUCMultiReaderMutex::TReadLocker eventDataLock( fEventMutex );
    if ( fEvents.FindElement( eventID, evNdx ) )
	{
	found = true;
	ed = fEvents.Get( evNdx );
#if 0
	if ( !ed )
	    {
	    MLUCString str;
	    fFinishedEventData.AsString( str );
	    LOG( AliHLTLog::kFatal, "AliHLTEventMergerNew::GetAnnouncedEventData", "Internal Error" )
		<< "Internal Error: ed==0 - EventID: 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << "). fUnfinishedEventData duimp: \n" << str.c_str() << "\n." << ENDLOG;
	    }
 	if ( ed->fEventID != eventID )
	    {
	    MLUCString str;
	    fFinishedEventData.AsString( str );
	    LOG( AliHLTLog::kFatal, "AliHLTEventMergerNew::GetAnnouncedEventData", "Internal Error" )
		<< "Internal Error: ed->fEventID != eventID - eventID: 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		<< ") - ed->fEventID: 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID 
		<< "). fUnfinishedEventData dump: \n" << str.c_str() << "\n." << ENDLOG;
	    }
#endif
	MLUCMutex::TLocker eventLock( ed->fMutex );
	sedd = ed->fSEDD;
	trigger = ed->fETS;
	}
    return found;
    }

void AliHLTEventMergerNew::PURGEALLEVENTS()
    {
    // fEDDBacklog AllocateBitmap^-1 (ReleaseBitmap) fFinishedEventData fUnfinishedEventData (ed->fReceivedPartBitmap / ed->fExpectedPartBitmap / ed->fCanceledPartBitmap) fEventDataCache fTimeoutDataCache (fTimer) fETSCache fEDDCache fSignal fDescriptorHandler (ed->fSEDD)
    fEventsPurged = true;
    vector<AliUInt64_t> notificationData;
    fTimer.CancelAllTimeouts( notificationData );

    fSignal.Lock();
    while ( fSignal.HaveNotificationData() )
	fSignal.PopNotificationData();
    fSignal.Unlock();

    vector<PEventData> events;
    vector<AliHLTEventDoneData*> edds;
	{
	MLUCMultiReaderMutex::TWriteLocker eventWriteDataLock( fEventMutex );
	
	events.reserve( fEvents.GetCnt() );
	while ( fEvents.GetCnt()>0 )
	    {
	    events.push_back( fEvents.GetFirst() );
	    fEvents.RemoveFirst();
	    }
	
	while ( fIncompleteEventLog.GetCnt()>0 )
	    fIncompleteEventLog.RemoveFirst();
	}
    
    vector<PEventData>::iterator evtIter, evtEnd;
    evtIter = events.begin();
    evtEnd = events.end();
    while ( evtIter != evtEnd )
	{
	PEventData ed=*evtIter;
	if ( ed )
	    {
	    if ( ed->fETS )
		{
		fETSCache.Release( (uint8*)ed->fETS );
		}
	    
	    if ( ed->fSEDD )
		{
		pthread_mutex_lock( &fDescriptorHandlerMutex );
		fDescriptorHandler->ReleaseEventDescriptor( ed->fEventID );
		pthread_mutex_unlock( &fDescriptorHandlerMutex );
		}
	    if ( ed->fEDD )
		{
		pthread_mutex_lock( &fEDDCacheMutex );
		fEDDCache.Release( (uint8*)ed->fEDD );
		pthread_mutex_unlock( &fEDDCacheMutex );
		}
#ifdef MERGED_BITMAPS
	    if ( ed->fBitmaps )
		ReleaseBitmap( ed->fBitmaps );
#else
	    if ( ed->fExpectedPartBitmap )
		ReleaseBitmap( ed->fExpectedPartBitmap );
	    if ( ed->fCanceledPartBitmap )
		ReleaseBitmap( ed->fCanceledPartBitmap );
	    if ( ed->fReceivedPartBitmap )
		ReleaseBitmap( ed->fReceivedPartBitmap );
#endif
	    fEventDataCache.Release( ed );
	    }
	evtIter++;
	}


    vector<AliUInt64_t>::iterator timerIter, timerEnd;
    timerIter = notificationData.begin();
    timerEnd = notificationData.end();
    while ( timerIter != timerEnd )
	{
	TimeoutData* td = reinterpret_cast<TimeoutData*>( *timerIter );
	fTimeoutDataCache.Release( td );
	timerIter++;
	}
    
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fHLTStatus.fPendingOutputEventCount = fStatus->fHLTStatus.fPendingInputEventCount = 0;
	fStatus->Touch();
	}
    }


void AliHLTEventMergerNew::DumpEventMetaData()
    {
    MLUCMultiReaderMutex::TReadLocker eventDataLock( fEventMutex );
    LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::DumpEventMetaData", "Event meta data dump" )
	<< "Dumping all current events to file." << ENDLOG;
    
    // We need to get the list of all current pending events, fetch their sub event descriptors,
    // dereference the shared memory pointers to all the data blocks and then write the
    // blocks to disk.

    MLUCString prefix;
    prefix = fName;
    prefix += "-Pending";
    AliHLTEventMetaDataOutput metaDataOutputPending( prefix.c_str(), fAliceHLT, fRunNumber );

    if ( fAliceHLT )
	metaDataOutputPending.SetEventTriggerDataFormatFunction(  AliHLTHLEventTriggerData2String );

    int ret = metaDataOutputPending.Open();
    if ( ret!=0 )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::DumpEventMetaData", "Error opening event meta data output file" )
	    << "Error opening event meta data output file '" << prefix.c_str() << ": " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return;
	}

    bool found=false;
    MLUCIndexedVector<PEventData,AliEventID_t>::TIterator eventIter;
    eventIter = fEvents.Begin();
    while ( eventIter )
	{
	PEventData ed=*eventIter;
	if ( ed )
	    {
	    MLUCMutex::TLocker eventLock( ed->fMutex );
	    if ( ed->fState==kIncomplete )
		{
		metaDataOutputPending.WriteEvent( ed->fEventID, ed->fSEDD, ed->fETS, "pending" );
		found=true;
		}
	    }
	++eventIter;
	}

    metaDataOutputPending.Close();


    prefix = fName;
    prefix += "-Processed";
    AliHLTEventMetaDataOutput metaDataOutputProcessed( prefix.c_str(), fAliceHLT, fRunNumber );

    if ( fAliceHLT )
	metaDataOutputProcessed.SetEventTriggerDataFormatFunction(  AliHLTHLEventTriggerData2String );

    ret = metaDataOutputProcessed.Open();
    if ( ret!=0 )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::DumpEventMetaData", "Error opening event meta data output file" )
	    << "Error opening event meta data output file '" << prefix.c_str() << ": " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return;
	}

    eventIter = fEvents.Begin();
    while ( eventIter )
	{
	PEventData ed=*eventIter;
	if ( ed )
	    {
	    MLUCMutex::TLocker eventLock( ed->fMutex );
	    if ( ed->fState==kComplete )
		{
		metaDataOutputProcessed.WriteEvent( ed->fEventID, ed->fSEDD, ed->fETS, "processed" );
		found=true;
		}
	    }
	++eventIter;
	}

    metaDataOutputProcessed.Close();
    
    if ( !found )
        {
        LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::DumpEventMetaData", "Nothing to dump" )
	    << "There were no events found to be dumped to disk" << ENDLOG;
	}


    }


void AliHLTEventMergerNew::SetTriggerClassBitContributingDDLs( unsigned triggerClassBit, hltreadout_uint32_t const* ddlList )
    {
    MLUCMutex::TLocker locker( fTriggerClassContributingDDLsMutex );
    if ( fTriggerClassContributingDDLs.size()<triggerClassBit+1 )
	fTriggerClassContributingDDLs.resize( triggerClassBit+1, NULL );
    if ( !fTriggerClassContributingDDLs[triggerClassBit] && ddlList )
	fTriggerClassContributingDDLCnt++;
    else if ( fTriggerClassContributingDDLs[triggerClassBit] && !ddlList )
	fTriggerClassContributingDDLCnt--;
    fTriggerClassContributingDDLs[triggerClassBit] = ddlList;
    }

void AliHLTEventMergerNew::SetTriggerClassMaskContributingDDLs( AliUInt64_t* triggerClassMask, hltreadout_uint32_t const* ddlList )
    {
      for(int i=0; i<2; ++i){
	unsigned offset = 50 * i;
	unsigned bit = 50;
	// We start at highest bit, since then we will have to resize only once,
	// otherwise we would have to resize potentially for every set bit.
	while ( bit-- )	{
	  if ( triggerClassMask[i] & (((AliUInt64_t)1)<<bit) )
	    SetTriggerClassBitContributingDDLs( bit+offset, ddlList );
	}
      }
    }

bool AliHLTEventMergerNew::GetTriggerClassBitContributingDDLs( unsigned triggerClassBit, volatile_hltreadout_uint32_t* ddlList, bool reset )
    {
    AliHLTReadoutList rdLstDst(gReadoutListVersion,ddlList);
    if ( reset )
	rdLstDst.Reset();
    MLUCMutex::TLocker locker( fTriggerClassContributingDDLsMutex );
    if ( fTriggerClassContributingDDLs.size()<triggerClassBit+1 )
	return false;
    if ( !fTriggerClassContributingDDLs[triggerClassBit] )
	return false;
    AliHLTReadoutList rdLstSrc(gReadoutListVersion,(volatile_hltreadout_uint32_t*)(fTriggerClassContributingDDLs[triggerClassBit]));
    locker.Unlock();
    rdLstDst |= rdLstSrc;
    return true;
    }

unsigned long AliHLTEventMergerNew::GetTriggerClassMaskContributingDDLs( AliUInt64_t* triggerClassMask, volatile_hltreadout_uint32_t* ddlList )
    {
    unsigned long cnt=0;
    AliHLTReadoutList rdLst(gReadoutListVersion,ddlList);
    rdLst.Reset();
    for(int i=0; i<2; ++i){
      unsigned offset = 50 * i;
      unsigned bit = 50;
      while ( bit-- ) {
	if ( triggerClassMask[i] & (((AliUInt64_t)1)<<bit) )
	  if ( GetTriggerClassBitContributingDDLs( bit+offset, ddlList, false ) )
	    cnt++; 
      }
    }
    return cnt;
    }

unsigned long AliHLTEventMergerNew::GetTriggerClassCnt()
    {
    MLUCMutex::TLocker locker( fTriggerClassContributingDDLsMutex );
    return fTriggerClassContributingDDLCnt;
#if 0
    unsigned long cnt=0;
    std::vector<hltreadout_uint32_t const*>::iterator, iter, end;
    iter = fTriggerClassContributingDDLs.begin();
    end = fTriggerClassContributingDDLs.end();
    while ( iter != end )
	{
	if ( *iter )
	    cnt++;
	++iter;
	}
    return cnt;
#endif
    }



void AliHLTEventMergerNew::GetSubscriberDDLList( unsigned long subNdx, volatile_hltreadout_uint32_t*& contributingDDList )
    {
    contributingDDList = NULL;
    MLUCMultiReaderMutex::TReadLocker subscriberLock( fSubscriberMutex );
    if ( fSubscriberCnt>subNdx && fSubscribers[subNdx].fNdx==subNdx )
        {
	contributingDDList = fSubscribers[subNdx].fContributingDDLs;
	}
    }

bool AliHLTEventMergerNew::GetSubscriberDDLLists( volatile_hltreadout_uint32_t**& contributingDDLLists )
    {
    MLUCMultiReaderMutex::TReadLocker subscriberLock( fSubscriberMutex );
    contributingDDLLists = new volatile_hltreadout_uint32_t*[fHighestSubscriberNdx+1];
    if ( !contributingDDLLists )
	{
	return false;
	}
    register SubscriberData* iter = fSubscribers;
    for ( unsigned long nn=0; nn<=fHighestSubscriberNdx; nn++ )
	contributingDDLLists[nn] = NULL;
    for ( unsigned long nn=0; nn<fSubscriberCnt; nn++, iter++ )
	{
	if ( iter->fEnabled )
	      contributingDDLLists[iter->fNdx] = iter->fContributingDDLs;
	}
    return true;
    }


void AliHLTEventMergerNew::DoProcessing()
    {
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCRunningStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	}

    fProcessingQuitted = false;
    AliEventID_t eventID;
    unsigned long ndx;
    unsigned long blkCnt;
    PEventData ed=NULL;
    int ret;

    fSignal.Lock();
    while ( !fQuitProcessing )
	{
	if ( !fSignal.HaveNotificationData() || fPaused )
	    fSignal.Wait();
	if ( fQuitProcessing )
	    {
	    fSignal.Unlock();
	    break;
	    }
	if ( fPaused )
	    {
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCPausedStateFlag;
		}
	    continue;
	    }
	else
	    {
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState &= ~kAliHLTPCPausedStateFlag;
		}
	    }
	while ( fSignal.HaveNotificationData() )
	    {
	    ed = fSignal.PopNotificationData();
 	    if ( !ed )
 		continue;
	    fSignal.Unlock();
	    //#define DO_PROCESSING_REDUCE_READLOCK
#ifdef DO_PROCESSING_REDUCE_READLOCK
#else
#warning How much of this eventDataLock is really needed here? Could we skip it altogether after having obtained the event lock itself?
	    MLUCMultiReaderMutex::TReadLocker eventDataLock( fEventMutex );
#endif
	    MLUCMutex::TLocker eventLock( ed->fMutex );
	    eventID = ed->fEventID;
	    LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::DoProcessing", "Next event" )
		<< "Next: Event 0x" << AliHLTLog::kHex << eventID << " ("
		<< AliHLTLog::kDec << eventID << ")." << ENDLOG;
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCBusyStateFlag;
		}
	    if ( ed->fCanceled )
		{
		LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::DoProcessing", "Event already canceled (1)" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " ("
		    << AliHLTLog::kDec << eventID << ") is already canceled (1)."
		    << ENDLOG;
#ifdef DO_PROCESSING_REDUCE_READLOCK
		MLUCMultiReaderMutex::TWriteLocker eventWriteDataLock( fEventMutex );
#else
		MLUCMultiReaderMutex::TWriteLocker eventWriteDataLock( eventDataLock );
#endif
		ed->fBusy = false;
		if ( fEvents.FindElement( eventID, ndx ) )
		    ReleaseEvent( ndx, eventWriteDataLock, eventLock );
		if ( fStatus )
		    {
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
		    }
		fSignal.Lock();
		continue;
		}
	    blkCnt = ed->fDataBlocks.GetCnt();
	    pthread_mutex_lock( &fDescriptorHandlerMutex );
	    ed->fSEDD = fDescriptorHandler->GetFreeEventDescriptor( eventID, blkCnt );
	    pthread_mutex_unlock( &fDescriptorHandlerMutex );
	    if ( !ed->fSEDD )
		{
		LOG( AliHLTLog::kError, "AliHLTEventMergerNew::DoProcessing", "Out Of Memory" )
		    << "Out of memory allocating sub-event data descriptor for "
		    << AliHLTLog::kDec << blkCnt << " blocks for event 0x" << AliHLTLog::kHex << eventID << " ("
		    << AliHLTLog::kDec << eventID << ")." << ENDLOG;
		// XXX Start retry timer.
		if ( fStatus )
		    {
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
		    }
		fSignal.Lock();
		continue;
		}
	    ed->fSEDD->fEventBirth_s = ed->fEventBirth_s;
	    ed->fSEDD->fEventBirth_us = ed->fEventBirth_us;
	    ed->fSEDD->fOldestEventBirth_s = ed->fOldestEventBirth_s;
	    ed->fSEDD->fStatusFlags = ed->fStatusFlags;
	    // XXX This chould actually be checked...
	    ed->fSEDD->fDataType.fID = COMPOSITE_DATAID;
// 		unsigned long i;
// 		for ( i = 0; i < blkCnt; i++ )
// 		    ed->fSEDD->fDataBlocks[i] = ed->fDataBlocks.GetPtr( i )->fBlockData;
	    ed->fSEDD->fDataBlockCount = 0;
	    ed->fDataBlocks.Iterate( AliHLTEventMergerNew::AddDataBlocksEventIterationFunc, (void*)(ed->fSEDD) );
	    if ( ed->fSEDD->fDataBlockCount!=blkCnt )
		{
		LOG( AliHLTLog::kFatal, "AliHLTEventMergerNew::DoProcessing", "Internal Error" )
		    << "Internal error for event 0x" << AliHLTLog::kHex << eventID << " ("
		    << AliHLTLog::kDec << eventID << "): Wrong number of data blocks added to event descriptor ("
		    << ed->fSEDD->fDataBlockCount << " instead of " << blkCnt << ")."
		    << ENDLOG;
#ifdef DO_PROCESSING_REDUCE_READLOCK
		MLUCMultiReaderMutex::TWriteLocker eventWriteDataLock( fEventMutex );
#else
		MLUCMultiReaderMutex::TWriteLocker eventWriteDataLock( eventDataLock );
#endif
		ed->fBusy = false;
		if ( fEvents.FindElement( eventID, ndx ) )
		    ReleaseEvent( ndx, eventWriteDataLock, eventLock );
		if ( fStatus )
		    {
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
		    }
		fSignal.Lock();
		continue;
		}
	    
	    if ( ed->fCanceled )
		{
		LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::DoProcessing", "Event already canceled (2)" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " ("
		    << AliHLTLog::kDec << eventID << ") is already canceled (2)."
		    << ENDLOG;
#ifdef DO_PROCESSING_REDUCE_READLOCK
		MLUCMultiReaderMutex::TWriteLocker eventWriteDataLock( fEventMutex );
#else
		MLUCMultiReaderMutex::TWriteLocker eventWriteDataLock( eventDataLock );
#endif
		ed->fBusy = false;
		if ( fEvents.FindElement( eventID, ndx ) )
		    ReleaseEvent( ndx, eventWriteDataLock, eventLock );
		if ( fStatus )
		    {
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
		    }
		fSignal.Lock();
		continue;
		}
#ifndef DO_PROCESSING_REDUCE_READLOCK
	    eventDataLock.Unlock();
#endif
	    eventLock.Unlock();
	    ret = fPublisher->AnnounceEvent( eventID, *ed->fSEDD, *ed->fETS );
	    if ( fEventAccounting )
		fEventAccounting->EventForwarded( eventID );
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		if ( eventID.fType!=kAliEventTypeTick && eventID.fType!=kAliEventTypeReconfigure && eventID.fType!=kAliEventTypeDCSUpdate )
		    ++(fStatus->fHLTStatus.fAnnouncedEventCount);
		fStatus->Touch();
		}
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTEventMergerNew::DoProcessing", "Error Announcing Event" )
		    << "Error announcing event 0x" << AliHLTLog::kHex << eventID << " ("
		    << AliHLTLog::kDec << eventID << ") via publisher '" << fPublisher->GetName()
		    << "': " << strerror(ret) << " (" << ret << ")." << ENDLOG;
		// Start retry timer...
		if ( fStatus )
		    {
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
		    }
		fSignal.Lock();
		continue;
		}
#ifdef DO_PROCESSING_REDUCE_READLOCK
	    MLUCMultiReaderMutex::TReadLocker eventDataLock( fEventMutex );
#else
	    eventDataLock.Lock();
#endif
	    eventLock.Lock();
	    ed->fBusy = false;
	    if ( ed->fCanceled || ed->fDone )
		{
		MLUCMultiReaderMutex::TWriteLocker eventWriteDataLock( eventDataLock );
		if ( fEvents.FindElement( eventID, ndx ) )
		    ReleaseEvent( ndx, eventWriteDataLock, eventLock );
		}
	    eventDataLock.Unlock();
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
		}
	    fSignal.Lock();
	    }
	}
    fSignal.Unlock();
    fProcessingQuitted = true;

    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCRunningStateFlag;
	if ( !(fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) )
	    fStatus->fProcessStatus.fState |= kAliHLTPCConsumerErrorStateFlag;
	}
    }

void AliHLTEventMergerNew::ReleaseEvent( unsigned long currNdx, MLUCMultiReaderMutex::TWriteLocker& eventDataWriteLock, MLUCMutex::TLocker& eventLock )
    {
    PEventData ed;
    ed = fEvents.Get( currNdx );
#ifdef MERGED_BITMAPS
    unsigned long *bitmaps = ed->fBitmaps;
#else
    unsigned long *canceledBitmap = ed->fCanceledPartBitmap;
    unsigned long *expectedBitmap = ed->fExpectedPartBitmap;
    unsigned long *receivedBitmap = ed->fReceivedPartBitmap;
#endif
    unsigned long highestPartNdx = ed->fHighestExpectedPartNdx;
    struct timeval firstPartReceived = ed->fFirstPartReceive;

    AliHLTEventTriggerStruct* ets = ed->fETS;
    AliHLTEventDoneData* edd = ed->fEDD;
    AliEventID_t eventID = ed->fEventID;

    LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::ReleaseEvent", "Releasing Event" )
	<< "Releasing event 0x" << AliHLTLog::kHex << eventID << AliHLTLog::kDec
	<< " (" << eventID << ")." << ENDLOG;

    fEvents.Remove( currNdx );
    eventLock.Unlock();
    fEventDataCache.Release( ed );
	{
	MLUCMutex::TLocker countLock( fEventCountMutex );
	fCompleteEventCount--;
	}

    fETSCache.Release( (uint8*)ets );

    AliHLTEventDoneData* oldEDD=NULL;
    if ( 1 ) // if ( edd )
	{
	EventBacklogData ebd;
	ebd.fEDD = edd;
	ebd.fEventID = eventID;
	ebd.fFirstPartReceive = firstPartReceived;
	unsigned long long totalEventCount;
	    {
	    MLUCMutex::TLocker countLock( fEventCountMutex );
	    totalEventCount = fCompleteEventCount+fIncompleteEventCount;
	    }
#ifdef BACKLOG_DEBUG
	if ( fEDDBacklog.GetCnt()>=fEDDBacklog.GetSize() && fEDDBacklog.GetCnt()>0 )
#else
	if ( fEDDBacklog.GetCnt()>=fEDDBacklog.GetSize() && fEDDBacklog.GetCnt()>0 && fEDDBacklog.GetSize()>=(totalEventCount << 1) )
#endif
	    {
	    oldEDD = fEDDBacklog.GetFirstPtr()->fEDD;
	    fEDDBacklog.RemoveFirst();
	    }
	if ( !fEDDBacklog.Add( ebd, eventID ) )
	  {
	    LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::ReleaseEvent", "Cannot add event to done list" )
	      << "Unable to add event 0x" << AliHLTLog::kHex << eventID << AliHLTLog::kDec
	      << " (" << eventID << ") to done event backlog list (back log size: " 
	      << fEDDBacklog.GetSize() << " - elements in backlog: "
	      << fEDDBacklog.GetCnt() << ")." << ENDLOG;
	    pthread_mutex_lock( &fEDDCacheMutex );
	    if ( ebd.fEDD )
	        fEDDCache.Release( (uint8*)ebd.fEDD );
	    pthread_mutex_unlock( &fEDDCacheMutex );
	  }
// 	edd = NULL;
	}

    eventDataWriteLock.Unlock();


    pthread_mutex_lock( &fDescriptorHandlerMutex );
    fDescriptorHandler->ReleaseEventDescriptor( eventID );
    pthread_mutex_unlock( &fDescriptorHandlerMutex );
    

	{
	register SubscriberData* iter;
	unsigned long wordIndex, bitInWord;
	MLUCMultiReaderMutex::TReadLocker subscriberLock( fSubscriberMutex );
	iter = fSubscribers;
	for ( unsigned long nn=0; nn<fSubscriberCnt; nn++, iter++ )
	    {
	    if ( iter->fNdx<=highestPartNdx )
		{
#ifdef MERGED_BITMAPS
		if ( iter->fNdx<=highestPartNdx && (bitmaps[GetBitmapWordIndex(highestPartNdx*kReceivedParts+iter->fNdx)] & GetBitmapBitInWord(highestPartNdx*kReceivedParts+iter->fNdx)) && !(bitmaps[GetBitmapWordIndex(highestPartNdx*kCanceledParts+iter->fNdx)] & GetBitmapBitInWord(highestPartNdx*kCanceledParts+iter->fNdx)) && iter->fPublisher )
#else
		wordIndex = GetBitmapWordIndex(iter->fNdx);
		bitInWord = GetBitmapBitInWord(iter->fNdx);
		if ( (receivedBitmap[wordIndex] & bitInWord) && !(canceledBitmap[wordIndex] & bitInWord) && iter->fPublisher )
#endif
		    {
		    if ( edd )
			iter->fPublisher->EventDone( *(iter->fSubscriber), *edd );
		    else
			{
			pthread_mutex_lock( &fStaticEDDMutex );
			fStaticEDD.fEventID = eventID;
			iter->fPublisher->EventDone( *(iter->fSubscriber), fStaticEDD );
			pthread_mutex_unlock( &fStaticEDDMutex );
			}
		    }
		}
	    }
	}

#ifdef MERGED_BITMAPS
    ReleaseBitmap( bitmaps );
#else
    ReleaseBitmap( expectedBitmap );
    ReleaseBitmap( canceledBitmap );
    ReleaseBitmap( receivedBitmap );
#endif


//     if ( edd )
// 	{
// 	pthread_mutex_lock( &fEDDCacheMutex );
// 	fEDDCache.Release( (uint8*)edd );
// 	pthread_mutex_unlock( &fEDDCacheMutex );
// 	}

    if ( oldEDD )
	{
	pthread_mutex_lock( &fEDDCacheMutex );
	fEDDCache.Release( (uint8*)oldEDD );
	pthread_mutex_unlock( &fEDDCacheMutex );
	}

    eventDataWriteLock.Lock();
    }



void AliHLTEventMergerNew::SubscriberDisabledEventIterationFunc( const PEventData& el, void* arg )
    {
    unsigned long ndx = *(unsigned long*)arg;
#ifndef MERGED_BITMAPS
    unsigned long wordIndex = GetBitmapWordIndex(ndx);
    unsigned long bitInWord = GetBitmapBitInWord(ndx);
#endif
    MLUCMutex::TLocker lock( el->fMutex );
#ifdef MERGED_BITMAPS
    if ( ndx > el->fHighestExpectedPartNdx || !(el->fBitmaps[GetBitmapWordIndex(el->fHighestExpectedPartNdx*kExpectedParts+ndx)] & GetBitmapBitInWord(el->fHighestExpectedPartNdx*kExpectedParts+ndx)) )
#else
    if ( ndx > el->fHighestExpectedPartNdx || !(el->fExpectedPartBitmap[wordIndex] & bitInWord) )
#endif
	return;
#ifdef MERGED_BITMAPS
    if ( el->fBitmaps[GetBitmapWordIndex(el->fHighestExpectedPartNdx*kExpectedParts+ndx)] & GetBitmapBitInWord(el->fHighestExpectedPartNdx*kExpectedParts+ndx) && !(el->fBitmaps[GetBitmapWordIndex(el->fHighestExpectedPartNdx*kReceivedParts+ndx)] & GetBitmapBitInWord(el->fHighestExpectedPartNdx*kReceivedParts+ndx)) )
#else
    if ( (el->fExpectedPartBitmap[wordIndex] & bitInWord) && !(el->fReceivedPartBitmap[wordIndex] & bitInWord) )
#endif
	{
	el->fSubscribersExpected--;
#ifdef MERGED_BITMAPS
	el->fBitmaps[GetBitmapWordIndex(el->fHighestExpectedPartNdx*kExpectedParts+ndx)] &= ~GetBitmapBitInWord(el->fHighestExpectedPartNdx*kExpectedParts+ndx);
#else
	el->fExpectedPartBitmap[wordIndex] &= ~bitInWord;
#endif
	}
    }


void AliHLTEventMergerNew::PurgeSubscriberEventIterationFunc( const PEventData& el, void* arg )
    {
    bool found = false;
    unsigned long ndx = *(unsigned long*)arg;
#ifndef MERGED_BITMAPS
    unsigned long wordIndex = GetBitmapWordIndex(ndx);
    unsigned long bitInWord = GetBitmapBitInWord(ndx);
#endif
    MLUCMutex::TLocker lock( el->fMutex );
#ifdef MERGED_BITMAPS
    if ( ndx > el->fHighestExpectedPartNdx || !(el->fBitmaps[GetBitmapWordIndex(el->fHighestExpectedPartNdx*kExpectedParts+ndx)] & GetBitmapBitInWord(el->fHighestExpectedPartNdx*kExpectedParts+ndx)) )
#else
    if ( ndx > el->fHighestExpectedPartNdx || !(el->fExpectedPartBitmap[wordIndex] & bitInWord) )
#endif
	return;
#ifdef MERGED_BITMAPS
    if ( el->fBitmaps[GetBitmapWordIndex(el->fHighestExpectedPartNdx*kExpectedParts+ndx)] & GetBitmapBitInWord(el->fHighestExpectedPartNdx*kExpectedParts+ndx) )
	{
	el->fSubscribersExpected--;
	el->fBitmaps[GetBitmapWordIndex(el->fHighestExpectedPartNdx*kExpectedParts+ndx)] &= ~GetBitmapBitInWord(el->fHighestExpectedPartNdx*kExpectedParts+ndx);
	}
#else
    if ( el->fExpectedPartBitmap[wordIndex] & bitInWord )
	{
	el->fSubscribersExpected--;
	el->fExpectedPartBitmap[wordIndex] &= ~bitInWord;
	}
#endif
    found = false;
#ifdef MERGED_BITMAPS
    if ( (el->fBitmaps[GetBitmapWordIndex(el->fHighestExpectedPartNdx*kReceivedParts+ndx)] & GetBitmapBitInWord(el->fHighestExpectedPartNdx*kReceivedParts+ndx)) )
	{
	found = true;
#if 0
	LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::EventIterationFunc", "Received Part Bitmap 5a", el->fReceivedPartBitmap, el->fHighestExpectedPartNdx );
#endif
	el->fBitmaps[GetBitmapWordIndex(el->fHighestExpectedPartNdx*kReceivedParts+ndx)] &= ~GetBitmapBitInWord(el->fHighestExpectedPartNdx*kReceivedParts+ndx);
#if 0
	LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::EventIterationFunc", "Received Part Bitmap 5b", el->fReceivedPartBitmap, el->fHighestExpectedPartNdx );
#endif
	}
#else
    if ( (el->fReceivedPartBitmap[wordIndex] & bitInWord) )
	{
	found = true;
#if 0
	LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::EventIterationFunc", "Received Part Bitmap 5a", el->fReceivedPartBitmap, el->fHighestExpectedPartNdx );
#endif
	el->fReceivedPartBitmap[wordIndex] &= ~bitInWord;
#if 0
	LogBitmap( AliHLTLog::kDebug, "AliHLTEventMergerNew::EventIterationFunc", "Received Part Bitmap 5b", el->fReceivedPartBitmap, el->fHighestExpectedPartNdx );
#endif
	}
#endif
    if ( !found )
	return;
    el->fSubscribersPresent--;

    unsigned long evNdx;
    while ( MLUCVectorSearcher<DataBlock,unsigned long>::FindElement( el->fDataBlocks, FindSubscriberDataBlocks, ndx, evNdx ) )
	el->fDataBlocks.Remove( evNdx );

    // XXX ; Currently no way to identify which ets words came from which subscriber...
    // FIXME!!
//     vector<TriggerData>::iterator etsIter, etsEnd;
//     do
// 	{
// 	found = false;
// 	etsIter = el->fReceivedETSs.begin();
// 	etsEnd = el->fReceivedETSs.end();
// 	while ( etsIter != etsEnd )
// 	    {
// 	    if ( etsIter->fSubscriberNdx==ndx )
// 		{
// 		found = true;
// 		el->fReceivedETSs.erase( etsIter );
// 		break;
// 		}		
// 	    etsIter++;
// 	    }
// 	}
//     while ( found );
    }

// Must be protected by write lock!
void AliHLTEventMergerNew::AnnounceAnnounceableEventsIterationFunc( const PEventData& el, void* arg )
    {
    vector<PEventData>* lst = (vector<PEventData>*)arg;
    if ( (el->fSubscribersExpected == el->fSubscribersPresent || el->fScheduled) && el->fState==kIncomplete && !el->fBusy )
	lst->push_back( el );
    }

// Must be protected by write lock!
void AliHLTEventMergerNew::GetIncompleteEventsIterationFunc( const PEventData& el, void* arg )
    {
    vector<PEventData>* lst = (vector<PEventData>*)arg;
    if ( el->fState==kIncomplete && !el->fBusy )
	lst->push_back( el );
    }

void AliHLTEventMergerNew::AddDataBlocksEventIterationFunc( const DataBlock& el, void* arg )
    {
    ((AliHLTSubEventDataDescriptor*)arg)->fDataBlocks[ ((AliHLTSubEventDataDescriptor*)arg)->fDataBlockCount++ ] = el.fBlockData;
    }

// must be called with fSubscriberMutex already locked
AliHLTPublisherInterface* AliHLTEventMergerNew::GetPublisher( unsigned long subscriberNdx )
    {
    if ( fSubscriberCnt>subscriberNdx && fSubscribers[subscriberNdx].fNdx==subscriberNdx )
        return fSubscribers[subscriberNdx].fPublisher;
    return NULL;
    }

AliHLTSubscriberInterface* AliHLTEventMergerNew::GetSubscriber( unsigned long subscriberNdx )
    {
    if ( fSubscriberCnt>subscriberNdx && fSubscribers[subscriberNdx].fNdx==subscriberNdx )
        return fSubscribers[subscriberNdx].fSubscriber;
    return NULL;
    }

bool AliHLTEventMergerNew::AllocateBitmap( unsigned long ndx, unsigned long*& bitmap, unsigned long& len, bool perEvent )
    {
    MLUCDynamicAllocCache& bitmapCache = ( perEvent ? fBitmapCachePerEvent : fBitmapCacheTmp );
    if ( !bitmap || ndx>=len*sizeof(unsigned long)*8 )
	{
	unsigned long newLen = 1+ndx/(sizeof(unsigned long)*8);
	pthread_mutex_lock( &fBitmapCacheMutex );
	unsigned long* newBitmap = reinterpret_cast<unsigned long*>( bitmapCache.Get( newLen*sizeof(unsigned long) ) );
	if ( bitmap && newBitmap )
	    {
	    memcpy( newBitmap, bitmap, len*sizeof(unsigned long) );
	    bitmapCache.Release( reinterpret_cast<uint8*>( bitmap ) );
	    }
	if ( newBitmap )
	    memset( newBitmap+len, 0, (newLen-len)*sizeof(unsigned long) );
	pthread_mutex_unlock( &fBitmapCacheMutex );
	if ( !newBitmap )
	    return false;
	bitmap = newBitmap;
	len = newLen;
	return true;
	}
    return true;
    }

void AliHLTEventMergerNew::ReleaseBitmap( unsigned long* bitmap, bool perEvent )
    {
    if ( !bitmap )
	return;
    MLUCDynamicAllocCache& bitmapCache = ( perEvent ? fBitmapCachePerEvent : fBitmapCacheTmp );
    pthread_mutex_lock( &fBitmapCacheMutex );
    bitmapCache.Release( reinterpret_cast<uint8*>( bitmap ) );
    pthread_mutex_unlock( &fBitmapCacheMutex );
    }

void AliHLTEventMergerNew::LogBitmap( AliHLTLog::TLogLevel level, const char* origin, const char* keyword, unsigned long* bitmap, unsigned long len )
    {
    char tmp[512];
    MLUCString bits;
    for ( unsigned long i = 0; i < len; i++ )
	{
	sprintf( tmp, " 0x%08lX", bitmap[i] );
	bits += tmp;
	}
    LOG( level, origin, keyword )
	<< "Bitmap: " << bits.c_str() << "." << ENDLOG;
    }


void AliHLTEventMergerNew::EventTimeoutExpired( AliUInt64_t notData, unsigned long /*priority*/ )
    {
    AliHLTEventMergerNew::TimeoutData* td = reinterpret_cast<AliHLTEventMergerNew::TimeoutData*>( notData );
    if ( !td )
	return;
#if 1
    MLUCMultiReaderMutex::TReadLocker eventDataLock( fEventMutex );
    unsigned long eventTimeout = fEventTimeout_ms;
    eventDataLock.Unlock();
    if ( td->fTimeout<eventTimeout )
	{
	unsigned long newTimeout = eventTimeout - td->fTimeout;
	td->fTimeout = eventTimeout;
	LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::EventTimeoutExpired", "Rescheduling event timeout" )
	    << "Rescheduling event 0x" << AliHLTLog::kHex << td->fEventID <<  " (" << AliHLTLog::kDec << td->fEventID
	    << ") by " << newTimeout << " ms (total " << eventTimeout << " ms)." << ENDLOG;
	fTimer.AddTimeout( newTimeout, &fEventTimeoutCallback, reinterpret_cast<AliUInt64_t>( td ) );
	return;
	}
#endif
    unsigned long unfinishedNdx;
    eventDataLock.Lock();
    if ( fEvents.FindElement( td->fEventID, unfinishedNdx ) )
	{
	PEventData ed;
	ed = fEvents.Get( unfinishedNdx );
	MLUCMutex::TLocker eventLock( ed->fMutex );
	if ( ed && ed->fState==kIncomplete )
	    {
	    struct timeval now;
	    gettimeofday( &now, NULL );
	    unsigned long tdiff;
	    tdiff = ((unsigned long)( now.tv_sec-ed->fFirstPartReceive.tv_sec))*1000000UL + now.tv_usec-ed->fFirstPartReceive.tv_usec;
	    bool holdBack = false;
	    vector<PEventData> announceEDs;
	    if ( fSOREventCompleted || (ed->fEventID.fType!=kAliEventTypeData && ed->fEventID.fType!=kAliEventTypeEndOfRun && ed->fEventID.fType!=kAliEventTypeSync ) || !fAliceHLT || (!fStrictSORRequirements && !fSOREventReceived) )
		{
		LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::EventTimeoutExpired", "Scheduling incomplete event" )
		    << "Scheduling incomplete event ( " << AliHLTLog::kDec << ed->fSubscribersPresent
		    << " of " << ed->fSubscribersExpected <<  " subscribers) 0x" << AliHLTLog::kHex
		    << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID 
		    << ") for announcement after " << tdiff << " microsecs. ." << ENDLOG;
		if ( ed->fEventID.fType==kAliEventTypeStartOfRun )
		    fSOREventCompleted = true;
		
		ed->fBusy = true;
		ed->fState=kComplete;
		fLastBlockCnt = ed->fDataBlocks.GetCnt();
		eventLock.Unlock();
		    {
		    MLUCMutex::TLocker countLock( fEventCountMutex );
		    fIncompleteEventCount--;
		    fCompleteEventCount++;
		    }
		if ( fStatus )
		    {
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			{
			MLUCMutex::TLocker countLock( fEventCountMutex );
			fStatus->fHLTStatus.fPendingInputEventCount = fIncompleteEventCount;
			}
		    if ( ed->fEventID.fType!=kAliEventTypeTick && ed->fEventID.fType!=kAliEventTypeReconfigure && ed->fEventID.fType!=kAliEventTypeDCSUpdate )
			++(fStatus->fHLTStatus.fProcessedEventCount);
		    fStatus->Touch();
		    }
		fIncompleteEventLog.Add( ed->fEventID, ed->fEventID );
		announceEDs.reserve( 1 );
		if ( ed->fEventID.fType == kAliEventTypeEndOfRun )
		    {
		    unsigned long cnt;
			{
			MLUCMutex::TLocker countLock( fEventCountMutex );
			cnt = fIncompleteEventCount;
			}
		    MLUCMultiReaderMutex::TWriteLocker eventDataWriteLock( eventDataLock );
		    announceEDs.reserve( cnt+1 );
		    if ( cnt > 0 )
			{
			LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::EventTimeoutExpired", "End of run flushing" )
			    << "Timeout expired for end of run event. Flushing "
			    << AliHLTLog::kDec << cnt << " remaining incompletely received events." << ENDLOG;
			fEvents.Iterate( AliHLTEventMergerNew::GetIncompleteEventsIterationFunc, (void*)&announceEDs );
			vector<PEventData>::iterator eventIter, eventEnd;
			eventIter = announceEDs.begin();
			eventEnd = announceEDs.end();
			while ( eventIter!=eventEnd )
			    {
			    (*eventIter)->fBusy = true;
			    (*eventIter)->fState = kComplete;
				{
				MLUCMutex::TLocker countLock( fEventCountMutex );
				fIncompleteEventCount--;
				fCompleteEventCount++;
				}
			    if ( fStatus )
				{
				MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
				if ( ed->fEventID.fType!=kAliEventTypeTick && ed->fEventID.fType!=kAliEventTypeReconfigure && ed->fEventID.fType!=kAliEventTypeDCSUpdate )
				    ++(fStatus->fHLTStatus.fProcessedEventCount);
				fStatus->Touch();
				}
			    ++eventIter;
			    }
			}
		    }
		announceEDs.push_back( ed ); // Make sure, end of run event is last event!!!
		
		if ( ed->fEventID.fType==kAliEventTypeStartOfRun )
		    {
		    unsigned long long tmpCnt;
			{
			MLUCMutex::TLocker countLock( fEventCountMutex );
			announceEDs.reserve( fIncompleteEventCount+1 );
			tmpCnt = fIncompleteEventCount;
			}
		    MLUCMultiReaderMutex::TWriteLocker eventDataWriteLock( eventDataLock );
		    fEvents.Iterate( AliHLTEventMergerNew::AnnounceAnnounceableEventsIterationFunc, (void*)&announceEDs );
		    vector<PEventData>::iterator eventIter, eventEnd;
		    eventIter = announceEDs.begin();
		    eventEnd = announceEDs.end();
		    PEventData edTmp;
		    LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::EventTimeoutExpired", "Start of run flushing" )
			<< "Timeout expired for start of run event. Flushing "
			<< AliHLTLog::kDec << tmpCnt << " events received in the meantime." << ENDLOG;
		    while ( eventIter != eventEnd )
			{
			//if ( fUnfinishedEventData.FindElement( AliHLTEventMergerNew::FindEventDataByID, *eventIter, unfinishedNdx ) )
			edTmp = *eventIter;
			LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::EventTimeoutExpired", "Event not held back" )
			    << "Stopping holding back of " << (edTmp->fScheduled ? "flushed" : "completed") << " event 0x" << AliHLTLog::kHex << edTmp->fEventID << " (" << AliHLTLog::kDec << edTmp->fEventID
			    << ")." << ENDLOG;
			edTmp->fState = kComplete;
			    {
			    MLUCMutex::TLocker countLock( fEventCountMutex );
			    fIncompleteEventCount--;
			    fCompleteEventCount++;
			    }
			if ( fStatus )
			    {
			    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			    if ( (*eventIter)->fEventID.fType!=kAliEventTypeTick && (*eventIter)->fEventID.fType!=kAliEventTypeReconfigure && (*eventIter)->fEventID.fType!=kAliEventTypeDCSUpdate )
				++(fStatus->fHLTStatus.fProcessedEventCount);
			    fStatus->Touch();
			    }
			eventIter++;
			}
		    }
		}
	    else
		{
		ed->fScheduled = true;
		holdBack = true;
		if ( !fSOREventCompleted && fAliceHLT && fStrictSORRequirements && !fSOREventReceived ) //  && ed->fEventID.fType!=kAliEventTypeStartOfRun not necessary, only Data and EOR events arrive here...
		    {
		    LOG( AliHLTLog::kError, "AliHLTEventMergerNew::NewEvent", "Data Event Completed before Start-Of-Run/Data" )
			<< "Data event 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID
			<< ") completed before Start-Of-Run/Data event." << ENDLOG;
		    if ( fStatus )
			{
			MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
			}
		    }
		}
	    
	    if ( fDynamicTimeoutAdaption )
		AddEventReceiveInterval( (unsigned long)(fEventTimeout_ms*1.189207115002721) );  // 1.189207115002721 == 2^(1/4)
	    
	    eventDataLock.Unlock();
	    char const* title = NULL;
	    if ( fDynamicInputXabling /* || fDynamicTimeoutAdaption*/ )
		{
		vector<unsigned long> disableNdxs;
		disableNdxs.reserve( fSubscriberCnt );
		    {
		    MLUCMultiReaderMutex::TReadLocker subscriberLock( fSubscriberMutex );
		    for ( unsigned long ndx=0; ndx <= ed->fHighestExpectedPartNdx; ndx++ )
			{
			if ( fDynamicInputXabling &&
#ifdef MERGED_BITMAPS
			     (ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kExpectedParts+ndx)] & GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kExpectedParts+ndx)) &&
			     !(ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kReceivedParts+ndx)] & GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kReceivedParts+ndx))
#else
			    (ed->fExpectedPartBitmap[GetBitmapWordIndex(ndx)] & GetBitmapBitInWord(ndx)) &&
				!(ed->fReceivedPartBitmap[GetBitmapWordIndex(ndx)] & GetBitmapBitInWord(ndx))
#endif
			   )
			    {
			    fSubscribers[ndx].fNoReceiveEventCnt++;
			    if ( fSubscribers[ndx].fNoReceiveEventCnt>=fNoReceiveEventCntLimit )
				disableNdxs.push_back( ndx );
			    }
			}
		    }
		if ( fDynamicInputXabling )
		    {
		    vector<unsigned long>::iterator disIter, disEnd;
		    disIter = disableNdxs.begin();
		    disEnd = disableNdxs.end();
		    while ( disIter != disEnd )
			{
			DisableSubscriber( *disIter, true );
			disIter++;
			}
		    }
		}
	    
#if 0
	    if ( !holdBack )
		title = "Flushing incomplete event - Expected Part Bitmap";
	    else
		title = "Holding back flushed incomplete event - Expected Part Bitmap";
	    LogBitmap( AliHLTLog::kWarning, "AliHLTEventMergerNew::EventTimeoutExpired", title, ed->fExpectedPartBitmap, fExpectedSubscribersBitmapLength );
	    if ( !holdBack )
		title = "Flushing incomplete event - Received Part Bitmap";
	    else
		title = "Holding back flushed incomplete event - Received Part Bitmap";
	    LogBitmap( AliHLTLog::kWarning, "AliHLTEventMergerNew::EventTimeoutExpired", title, ed->fReceivedPartBitmap, fExpectedSubscribersBitmapLength );
#endif
	    if ( DOLOG(AliHLTLog::kWarning) )
		{
		if ( !holdBack )
		    title = "Flushing incomplete event - Missing Subscribers";
		else
		    title = "Holding back flushed incomplete event - Missing Subscribers";
		MLUCString miss;
		char tmp[256];
		bool first = true;
		    {
		    MLUCMultiReaderMutex::TReadLocker subscriberLock( fSubscriberMutex );
		    for ( unsigned long ndx=0; ndx <= ed->fHighestExpectedPartNdx; ndx++ )
			{
#if 0
			LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::EventTimeoutExpired", "Subscriber Miss/Present" )
			    << "Checking subscriber " << AliHLTLog::kDec << ndx << " '" << fSubscribers[ndx].fSubscriber->GetName()
			    << "' - Expected: " << ( (ed->fExpectedPartBitmap[GetBitmapWordIndex(ndx)] & GetBitmapBitInWord(ndx)) ? "yes" : "no" )
			    << " - Received: " << ( (ed->fReceivedPartBitmap[GetBitmapWordIndex(ndx)] & GetBitmapBitInWord(ndx)) ? "yes" : "no" )
			    << ENDLOG;
#endif
#ifdef MERGED_BITMAPS
			if ( (ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kExpectedParts+ndx)] & GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kExpectedParts+ndx)) &&
			     !(ed->fBitmaps[GetBitmapWordIndex(ed->fHighestExpectedPartNdx*kReceivedParts+ndx)] & GetBitmapBitInWord(ed->fHighestExpectedPartNdx*kReceivedParts+ndx)) )
#else
			if ( (ed->fExpectedPartBitmap[GetBitmapWordIndex(ndx)] & GetBitmapBitInWord(ndx)) &&
			     !(ed->fReceivedPartBitmap[GetBitmapWordIndex(ndx)] & GetBitmapBitInWord(ndx)) )
#endif
			    {
			    snprintf( tmp, 256, " (index %lu)", ndx );
			    if ( !first )
				miss += " - ";
			    first = false;
			    miss += fSubscribers[ndx].fSubscriber->GetName();
			    miss += tmp;
			    }
			}
		    }
		LOG( AliHLTLog::kWarning, "AliHLTEventMergerNew::EventTimeoutExpired", title )
		    << "Event 0x" << AliHLTLog::kHex
		    << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID 
		    << ") missing subscribers: " << miss.c_str() << ENDLOG;
		}
	    //memcpy( ed->fReceivedPartBitmap, ed->fExpectedPartBitmap, fExpectedSubscribersBitmapLength*sizeof(unsigned long) );
	    //fSignal.Lock();
	    unsigned long i, cnt = announceEDs.size();
	    for ( i=0; i<cnt; i++ )
		fSignal.AddNotificationData( announceEDs[i] );
	    //fSignal.Unlock();
	    fSignal.Signal();
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    {
		    MLUCMutex::TLocker countLock( fEventCountMutex );
		    fStatus->fHLTStatus.fPendingInputEventCount = fIncompleteEventCount;
		    }
		fStatus->Touch();
		}
	    }
	else if ( !ed )
	    {
	    fEvents.Remove( unfinishedNdx );
	    eventDataLock.Unlock();
	    LOG( AliHLTLog::kError, "AliHLTEventMergerNew::EventTimeoutExpired", "Cannot retrieve incomplete event" )
		<< "cannot retrieve incomplete event 0x" << AliHLTLog::kHex
		<< td->fEventID << " (" << AliHLTLog::kDec << td->fEventID 
		<< ") for announcement." << ENDLOG;
	    }
	}
    else
	eventDataLock.Unlock();
    fTimeoutDataCache.Release( td );
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
