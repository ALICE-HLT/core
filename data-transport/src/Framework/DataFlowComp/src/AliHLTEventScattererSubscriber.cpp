/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTEventScattererSubscriber.hpp"
#include "AliHLTEventScatterer.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTEventDoneData.hpp"
#include <errno.h>


AliHLTEventScattererSubscriber::AliHLTEventScattererSubscriber( const char* name, AliUInt32_t maxShmNoUseCount, int maxPreSubEventsExp2 ):
    AliHLTDetectorSubscriber( name, maxShmNoUseCount, maxPreSubEventsExp2 )
    {
    fScatterer = NULL;
    AliHLTMakeEventDoneData( &fDoneData, 0 );
    }

AliHLTEventScattererSubscriber::~AliHLTEventScattererSubscriber()
    {
    }

// void AliHLTEventScattererSubscriber::EventDone( AliHLTEventID_t eventID )
//     {
//     }

void AliHLTEventScattererSubscriber::SetScatterer( AliHLTEventScatterer* dispatcher )
    {
    fScatterer = dispatcher;
    }

int AliHLTEventScattererSubscriber::SubscriptionCanceled( AliHLTPublisherInterface& )
    {
    int ret=0;
    if ( fScatterer )
	fScatterer->CancelAllSubscriptions();
    else
	ret = ENODEV;
    return ret;
    }

int AliHLTEventScattererSubscriber::ReleaseEventsRequest( AliHLTPublisherInterface& )
    {
    int ret=0;
    if ( fScatterer )
	fScatterer->RequestEventRelease();
    else
	ret = ENODEV;
    return ret;
    }

int AliHLTEventScattererSubscriber::EventCanceled( AliHLTPublisherInterface& publisher, AliEventID_t eventID )
    {
    if ( fScatterer )
	fScatterer->EventCanceled( eventID );
    return AliHLTDetectorSubscriber::EventCanceled( publisher, eventID );
    }

void AliHLTEventScattererSubscriber::ProcessEvent( const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* ets )
    {
    if ( !sedd )
	{
	LOG( AliHLTLog::kError, "AliHLTEventScattererSubscriber::ProcessEvent", "SEDD NULL pointer" )
	    << "Sub-Event Data Descriptor passed is NULL pointer..." << ENDLOG;
	return;
	}
    LOG( AliHLTLog::kDebug, "AliHLTEventScattererSubscriber::ProcessEvent", "Event Received" )
	<< "Event 0x" << AliHLTLog::kHex << sedd->fEventID << " (" << AliHLTLog::kDec
	<< sedd->fEventID << ") - sedd 0x" << AliHLTLog::kHex << (unsigned long)sedd << " received." << ENDLOG;
    if ( fScatterer )
	{
	
	vector<AliHLTDetectorSubscriber::BlockData> dataBlocks;
	ProcessSEDD( sedd, dataBlocks );
	fScatterer->NewEvent( sedd->fEventID, *sedd, *ets );
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventScattererSubscriber::ProcessEvent", "No merger object specified" )
	    << "No merger object specified yet. EventScattererSubscriber '" << GetName() << "' pretending to be done with event..."
	    << ENDLOG;
	fDoneData.fEventID = sedd->fEventID;
	EventDone( &fDoneData );
	}
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
