/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTControlledBridgeComponent.hpp"
#include "BCLURLAddressDecoder.hpp"
#include "BCLCommunication.hpp"
#include "BCLMsgCommunication.hpp"
#include "BCLBlobCommunication.hpp"
#include "AliHLTSCProcessControlCmds.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "AliHLTBridgeStatusData.hpp"
#include "AliHLTSCCommand.hpp"
#include "AliHLTSCTypes.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTSCInterface.hpp"
#include "AliHLTPublisherProxyInterface.hpp"
#include "AliHLTPublisherPipeProxy.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTShmManager.hpp"
#include "AliHLTRingBufferManager.hpp"
#include "AliHLTLargestBlockBufferManager.hpp"
#include "AliHLTStackTrace.hpp"
#include "AliHLTSCComponentInterface.hpp"
#include "AliHLTEventAccounting.hpp"
#include "AliHLTTriggerStackMachine.hpp"
#include "AliHLTTriggerStackMachineAssembler.hpp"
#include "AliHLTReadoutList.hpp"
#include "BCLSCIComID.hpp"
#include "MLUCCmdlineParser.hpp"
#include "MLUCSysMESLogServer.hpp"
#include "MLUCRegEx.hpp"
#include <syslog.h>
#include <stdio.h>

#if 0
#define TRACELOG( lvl, orig, keys ) LOG(lvl,orig,keys)
#else
#define TRACELOG( lvl, orig, keys ) LOG(AliHLTLog::kNone,orig,keys)
#endif


static void AcceptingSubscriptionsCallbackFunc( void* status )
    {
    MLUCMutex::TLocker stateLock( ((AliHLTBridgeStatusData*)status)->fStateMutex );
    ((AliHLTBridgeStatusData*)status)->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
    }


static inline bool operator<( MLUCString const& s1, MLUCString const& s2 )
    {
    return strcmp( s1.c_str(), s2.c_str() )<0;
    }

static void QuitSignalHandler( int sigValue )
    {
    exit( (1 << 31) | sigValue );
    }



template<class BridgeHeadClass>
inline AliHLTControlledBridgeComponent<BridgeHeadClass>::AliHLTControlledBridgeComponent( AliHLTControlledBridgeComponentType compType, 
											  const char* compName, int argc, char** argv ):
    fCmdSignal( 5 ),
    fCmdProcessor( this ),
    fLAM( this ),
    fTraceCallback( 7 ),
    fErrorCallback( this ),
    fEventAccounting(NULL)
    {
    fComponentType = compType;
    switch ( fComponentType )
	{
	case kPublisherBridgeHead:
	    fComponentTypeName = "Publisher Bridge Head"; 
	    break;
	case kSubscriberBridgeHead:
	    fComponentTypeName = "Subscriber Bridge Head"; 
	    break;
	default:
	    fComponentTypeName = "Unknown";
	    break;
	}
    fComponentName = compName;
    fArgc = argc;
    fArgv = argv;

    fCmdSignal.Unlock();

    fLogOut = NULL;
    fFileLog = NULL;
    fSysLog = NULL;
    fSysMESLogging = false;
    fSysMESLog = NULL;
    fDynLibLog = NULL;
    fDynLibPath = "";
    fDynLibLogging = false;
    fDescriptorHandler = NULL;
    fShmManager = NULL;
    fBridgeHead = NULL;
    fSCInterface = NULL;
    fStatus = NULL;
    fBlobMsgCom = NULL;
    fMsgCom = NULL;
    fBlobCom = NULL;
    fOwnBlobMsgAddress = NULL;
    fOwnBlobAddress = NULL;
    fOwnMsgAddress = NULL;
    fRemoteBlobMsgAddress = NULL;
    fRemoteMsgAddress = NULL;
    fEventModuloPreDivisor = 1;
    fUseSubscriptionEventModulo = false;
    fSubscriptionEventModulo = 1;
    fTriggerStackMachine = NULL;
    fRunNumber = 0;
    fBeamType = 0;
    fHLTMode = 0;
    fRunType = "";

    pthread_mutex_init( &fLAMAddressMutex, NULL );
    fEventAccounting = NULL;
    struct sigaction newSigAction;
    newSigAction.sa_sigaction = NULL;
    newSigAction.sa_handler = &QuitSignalHandler;
    sigemptyset( &newSigAction.sa_mask );
    newSigAction.sa_flags = 0;
    sigaction( SIGQUIT, &newSigAction, &fOldSigAction );
    ResetConfiguration();
    AliHLTRegisterStackTraceHandlers();
    }

template<class BridgeHeadClass>
inline AliHLTControlledBridgeComponent<BridgeHeadClass>::~AliHLTControlledBridgeComponent()
    {
    Unconfigure();
    pthread_mutex_destroy( &fLAMAddressMutex );
    }

template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::Run()
    {
    if ( !PreConfigure() )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::Run", "Pre-Configuration Error" )
	    << "Pre-Configuration Error for " << fComponentTypeName << " component "
	    << fComponentName << "." << ENDLOG;
	PostUnconfigure();
	return;
	}
    LOG( AliHLTLog::kInformational, "AliHLTControlledBridgeComponent::Run", "Component Started" )
	<< fComponentTypeName << " component " << fComponentName << " started." << ENDLOG;
    AliUInt32_t cmd = kAliHLTPCNOPCmd;
    bool quit = false;
    fCmdSignal.Lock();
    do
	{
	if ( !fCmdSignal.HaveNotificationData() )
	    fCmdSignal.Wait();
	while ( fCmdSignal.HaveNotificationData() )
	    {
	    cmd = (AliUInt32_t)fCmdSignal.PopNotificationData();
	    fCmdSignal.Unlock();
	    if ( !ExecCmd( cmd, quit ) )
		IllegalCmd( fStatus->fProcessStatus.fState, cmd );
	    fCmdSignal.Lock();
	    }
	}
    while ( !quit );
    fCmdSignal.Unlock();
    LOG( AliHLTLog::kInformational, "AliHLTControlledBridgeComponent::Run", "Component Ended" )
	<< fComponentTypeName << " component " << fComponentName << " ended." << ENDLOG;
    PostUnconfigure();
    }

template<class BridgeHeadClass>
inline bool AliHLTControlledBridgeComponent<BridgeHeadClass>::ExecCmd( AliUInt32_t cmd, bool& quit )
    {
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    switch ( cmd )
	{
	case kAliHLTPCEndProgramCmd:
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCStartedStateFlag) &&
		 !(fStatus->fProcessStatus.fState & ~kAliHLTPCStartedStateFlag) )
		{
		quit = true;
		return true;
		}
	    break;
	case kAliHLTPCStartProcessingCmd:
	    {
	    bool ok = true;
	    if ( fComponentType==kPublisherBridgeHead )
		{
		if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) ||
// 		     !(fStatus->fProcessStatus.fState & kAliHLTPCConnectedStateFlag) ||
		     (fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		    ok = false;
		}
	    else if ( fComponentType==kSubscriberBridgeHead )
		{
		if ( !(fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) ||
// 		     !(fStatus->fProcessStatus.fState & kAliHLTPCConnectedStateFlag) ||
		     (fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		    ok = false;
		}
	    else
		ok = false;
	    if ( !ok )
		break;
	    fStatus->fProcessStatus.fState |= (kAliHLTPCChangingStateFlag|kAliHLTPCRunningStateFlag);
	    stateLock.Unlock();
	    StartProcessing();
	    return true;
	    }
	case kAliHLTPCPauseProcessingCmd:
	    // 		if ( fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag )
	    // 		    break;
	    stateLock.Unlock();
	    PauseProcessing();
	    return true;
	case kAliHLTPCResumeProcessingCmd:
	    // 		if ( fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag )
	    // 		    break;
	    stateLock.Unlock();
	    ResumeProcessing();
	    return true;
	case kAliHLTPCStopProcessingCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		break;
	    fStatus->fProcessStatus.fState |= kAliHLTPCChangingStateFlag;		
	    stateLock.Unlock();
	    StopProcessing();
	    stateLock.Lock();
	    fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;		
	    return true;
	case kAliHLTPCBindCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
		break;
	    if ( fStatus->fProcessStatus.fState & ~(kAliHLTPCStartedStateFlag|kAliHLTPCConfiguredStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag|kAliHLTPCWithSubscribersStateFlag|kAliHLTPCSubscribedStateFlag) )
 		break;
	    stateLock.Unlock();
	    Bind();
	    return true;
	case kAliHLTPCUnbindCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCBoundStateFlag) )
		break;
	    stateLock.Unlock();
	    Unbind();
	    return true;
	case kAliHLTPCConnectCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCBoundStateFlag) )
		break;
// 	    if ( fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag && !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) )
// 		break;
// 	    if ( fStatus->fProcessStatus.fState & ~(kAliHLTPCStartedStateFlag|kAliHLTPCConfiguredStateFlag|kAliHLTPCBoundStateFlag) )
// 		break;
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCBusyStateFlag) )
		break;
	    stateLock.Unlock();
	    Connect();
	    return true;
	case kAliHLTPCDisconnectCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConnectedStateFlag) )
		break;
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag && !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) && 
		 (fStatus->fProcessStatus.fState & kAliHLTPCBusyStateFlag) )
		break;
	    stateLock.Unlock();
	    Disconnect();
	    return true;
	case kAliHLTPCReconnectCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConnectedStateFlag) )
		break;
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag && !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) )
		break;
	    stateLock.Unlock();
	    Reconnect();
	    return true;
	case kAliHLTPCNewConnectionCmd:
	    {
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConnectedStateFlag) )
		break;
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag && !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) )
		break;
	    stateLock.Unlock();
	    MLUCString* newMsgAddr=NULL;
	    MLUCString* newBlobMsgAddr=NULL;
	    fCmdSignal.Lock();
 	    if ( fCmdSignal.HaveNotificationData() )
		newMsgAddr = reinterpret_cast<MLUCString*>(fCmdSignal.PopNotificationData());
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent<BridgeHeadClass>::ExecCmd", "Unable to get msg address" )
		    << "Internal Error: Unable to get new message address from cmd signal for new connection command. Continuing without valid address."
		    << ENDLOG;
		}
 	    if ( fCmdSignal.HaveNotificationData() )
		newBlobMsgAddr = reinterpret_cast<MLUCString*>(fCmdSignal.PopNotificationData());
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent<BridgeHeadClass>::ExecCmd", "Unable to get blob msg address" )
		    << "Internal Error: Unable to get new blob message address from cmd signal for new connection command. Continuing without valid address."
		    << ENDLOG;
		}
	    fCmdSignal.Unlock();
	    Reconnect( (newMsgAddr ? newMsgAddr->c_str() : NULL), (newBlobMsgAddr ? newBlobMsgAddr->c_str() : NULL) );
	    if ( newBlobMsgAddr )
		delete newBlobMsgAddr;
	    if ( newMsgAddr )
		delete newMsgAddr;
	    return true;
	    }
	case kAliHLTPCNewRemoteAddressesCmd:
	    {
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
		break;
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag && !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) )
		break;
	    stateLock.Unlock();
	    MLUCString* newMsgAddr=NULL;
	    MLUCString* newBlobMsgAddr=NULL;
	    fCmdSignal.Lock();
 	    if ( fCmdSignal.HaveNotificationData() )
		newMsgAddr = reinterpret_cast<MLUCString*>(fCmdSignal.PopNotificationData());
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent<BridgeHeadClass>::ExecCmd", "Unable to get msg address" )
		    << "Internal Error: Unable to get new message address from cmd signal for new remote address command. Continuing without valid address."
		    << ENDLOG;
		}
 	    if ( fCmdSignal.HaveNotificationData() )
		newBlobMsgAddr = reinterpret_cast<MLUCString*>(fCmdSignal.PopNotificationData());
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent<BridgeHeadClass>::ExecCmd", "Unable to get blob msg address" )
		    << "Internal Error: Unable to get new blob message address from cmd signal for new remote address command. Continuing without valid address."
		    << ENDLOG;
		}
	    fCmdSignal.Unlock();
	    Disconnect( newMsgAddr->c_str(), newBlobMsgAddr->c_str() );
	    delete newBlobMsgAddr;
	    delete newMsgAddr;
	    return true;
	    }
	case kAliHLTPCPURGEALLEVENTSCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
		break;
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag && !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) )
		break;
	    stateLock.Unlock();
	    if ( fBridgeHead )
		fBridgeHead->PURGEALLEVENTS();
	    return true;
	case kAliHLTPCSimulateErrorCmd:
	    if ( !(fStatus->fProcessStatus.fState & ~kAliHLTPCStartedStateFlag) )
		break;
	    stateLock.Unlock();
	    LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent<BridgeHeadClass>::ExecCmd", "SIMULATING ERROR" )
		<< "SIMULATING ERROR. Setting error flag." << ENDLOG;
	    stateLock.Lock();
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return true;
	}
    return false;
    }

template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::ResetConfiguration()
    {
    fStdoutLogging = true;
    fFileLogging = true;
    fSysLogging = false;
    fSysLogFacility = 0;
    fSysMESLogging = false;
    fSysMESLog = NULL;
    fDynLibLog = NULL;
    fDynLibPath = "";
    fDynLibLogging = false;
    if ( fComponentName )
	fLogName = fComponentName;
    else
	fLogName = fArgv[0];
    fSCInterfaceAddress = "tcpmsg://localhost:10000/";

    //     fMaxPreEventCount = 1;
    //     fMaxPreEventCountExp2 = 0;
    fMaxPreEventCount = 0;
    fMaxPreEventCountExp2 = -1;
    fMinPreBlockCount = 1;
    fMaxPreBlockCount = 1;
    fMaxNoUseCount = 0;

    fBlobBufferSize = 0;
    fCommunicationTimeout = 0;
    fAliceHLT = false;
    fEventAccounting = NULL;
    fEventModuloPreDivisor = 1;
    fUseSubscriptionEventModulo = false;
    fSubscriptionEventModulo = 1;

    fRunNumber = 0;
    fBeamType = 0;
    fHLTMode = 0;
    fRunType = "";

    while ( fEventTypeStackMachineCodes.size()>0 )
	{
	delete [] reinterpret_cast<uint8*>( fEventTypeStackMachineCodes[0] );
	fEventTypeStackMachineCodes.erase( fEventTypeStackMachineCodes.begin() );
	}
    }

template<class BridgeHeadClass>
inline bool AliHLTControlledBridgeComponent<BridgeHeadClass>::PreConfigure()
    {
    gLogLevel = AliHLTLog::kAll;
    if ( !SetupStdLogging() ) // Set this up here, so that we can see error output from command line parsing.
	return false; 
    if ( !ParseCmdLine() )
	return false;
    if ( !SetupStdLogging() ) // set it up again, as the configuration/cmd line might have disabled it.
	return false;
    if ( !SetupFileLogging() )
	return false;
    if ( !SetupSysLogging() )
	return false;
    if ( !SetupSysMESLogging() )
	return false;
    if ( !SetupDynLibLogging() )
        return false;
    if ( !SetupSC() )
	return false;
    return true;
    }

template<class BridgeHeadClass>
inline bool AliHLTControlledBridgeComponent<BridgeHeadClass>::PostUnconfigure()
    {
    RemoveSC();
    RemoveLogging();
    return true;
    }

template<class BridgeHeadClass>
inline bool AliHLTControlledBridgeComponent<BridgeHeadClass>::CheckConfiguration()
    {
    bool isBlob;
    int ret;


    if ( !fAliceHLT && fEventTypeStackMachineCodes.size()>0 )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::CheckConfiguration", "Event Type Code only supported in ALICE HLT mode" )
	    << "Event type code is only supported in special ALICE HLT mode."
	    << ENDLOG;
	return false;
	}

    if ( fAliceHLT && !AliHLTReadoutList::IsVersionSupported(gReadoutListVersion) )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::CheckConfiguration", "Readout List Version not supported" )
	    << "Readout list version " << AliHLTLog::kDec << gReadoutListVersion
	    << (gReadoutListVersion==0 ? " (default)" : "")
	    << " is not supported. Please set a different version using the -readoutlistversion parameter."
	    << ENDLOG;
	return false;
	}

    if ( fOwnMsgAddressStr!="" )
	{
	ret = BCLCheckURL( fOwnMsgAddressStr.c_str(), true, isBlob );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::CheckConfiguration", "Invalid local msg address" )
		<< "Invalid local msg address specified." << ENDLOG;
	    return false;
	    }
	if ( isBlob )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::CheckConfiguration", "Specified local msg address is a blob address" )
		<< "Specified local msg address is a blob address instead of a msg address." << ENDLOG;
	    return false;
	    }
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::CheckConfiguration", "No local msg address" )
	    << "No local msg address specified (e.g. via the -msg command line parameter)." << ENDLOG;
	return false;
	}
    if ( fRemoteMsgAddressStr!="" )
	{
	ret = BCLCheckURL( fRemoteMsgAddressStr.c_str(), false, isBlob );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::CheckConfiguration", "Invalid remote msg address" )
		<< "Invalid remote msg address specified." << ENDLOG;
	    return false;
	    }
	if ( isBlob )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::CheckConfiguration", "Specified remote msg address is a blob address" )
		<< "Specified remote msg address is a blob address instead of a msg address." << ENDLOG;
	    return false;
	    }
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::CheckConfiguration", "No remote msg address" )
	    << "No remote msg address specified (e.g. via the -msg command line parameter)." << ENDLOG;
	return false;
	}

    if ( fOwnBlobMsgAddressStr!="" )
	{
	ret = BCLCheckURL( fOwnBlobMsgAddressStr.c_str(), true, isBlob );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::CheckConfiguration", "Invalid local blob msg address" )
		<< "Invalid local blob msg address specified." << ENDLOG;
	    return false;
	    }
	if ( isBlob )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::CheckConfiguration", "Specified local blob msg address is a blob address" )
		<< "Specified local blob msg address is a blob address instead of a blob msg address." << ENDLOG;
	    return false;
	    }
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::CheckConfiguration", "No local blob msg address" )
	    << "No local blob msg address specified (e.g. via the -blobmsg command line parameter)." << ENDLOG;
	return false;
	}
    if ( fRemoteBlobMsgAddressStr!="" )
	{
	ret = BCLCheckURL( fRemoteBlobMsgAddressStr.c_str(), false, isBlob );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::CheckConfiguration", "Invalid remote blob msg address" )
		<< "Invalid remote blob msg address specified." << ENDLOG;
	    return false;
	    }
	if ( isBlob )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::CheckConfiguration", "Specified remote blob msg address is a blob address" )
		<< "Specified remote blob msg address is a blob address instead of a blob msg address." << ENDLOG;
	    return false;
	    }
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::CheckConfiguration", "No remote blob msg address" )
	    << "No remote blob msg address specified (e.g. via the -blobmsg command line parameter)." << ENDLOG;
	return false;
	}


    if ( fOwnBlobAddressStr!="" )
	{
	ret = BCLCheckURL( fOwnBlobAddressStr.c_str(), true, isBlob );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::CheckConfiguration", "Invalid local blob address" )
		<< "Invalid local blob address specified." << ENDLOG;
	    return false;
	    }
	if ( !isBlob )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::CheckConfiguration", "Specified local blob address is a msg address" )
		<< "Specified local blob address is a msg address instead of a blob address." << ENDLOG;
	    return false;
	    }
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::CheckConfiguration", "No local blob address" )
	    << "No local blob address specified (e.g. via the -blob command line parameter)." << ENDLOG;
	return false;
	}


    typename vector<LAMAddressData>::iterator lamIter, lamEnd;
    pthread_mutex_lock( &fLAMAddressMutex );
    lamIter = fLAMAddresses.begin();
    lamEnd = fLAMAddresses.end();
    isBlob = false;
    bool fail = false;
    while ( lamIter != lamEnd )
	{
	if ( BCLCheckURL( lamIter->fStr.c_str(), false, isBlob ) )
	    {
	    fail = true;
	    break;
	    }
	if ( isBlob )
	    {
	    break;
	    }
	lamIter++;
	}
    if ( fail )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::CheckConfiguration", "Error decoding LAM address" )
	    << "Error decoding LAM address '" << lamIter->fStr.c_str() << "'." << ENDLOG;
	}
    if ( isBlob )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::CheckConfiguration", "LAM address must be msg address" )
	    << "Specified LAM address '" << lamIter->fStr.c_str() << "' is a blob address instead of a message address." << ENDLOG;
	}
    pthread_mutex_unlock( &fLAMAddressMutex );
    if ( fail || isBlob )
	return false;
    return true;
    }

template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::ShowConfiguration()
    {
    LOG( AliHLTLog::kInformational, "AliHLTControlledBridgeComponent<BridgeHeadClass>::ShowConfiguration", "Run number & type" )
	<< "Run number: " << AliHLTLog::kDec << fRunNumber << " (0x" << AliHLTLog::kHex
	<< fRunNumber 
	<< ") - Beam type: 0x" << fBeamType << " - HLT mode: 0x" << fHLTMode
	<< " - Run type: " << fRunType.c_str() << "." << ENDLOG;
	//<< ") - Run type: 0x" << fRunType << "." << ENDLOG;
    LOG( AliHLTLog::kInformational, "AliHLTControlledBridgeComponent<BridgeHeadClass>::ShowConfiguration", "Communication Configuration" )
	<< "Local msg address: " << fOwnMsgAddressStr.c_str() << " - Remote msg address: " << fRemoteMsgAddressStr.c_str()
	<< " - Local blob msg address: " << fOwnBlobMsgAddressStr.c_str() << " - Remote blob msg address: " << fRemoteBlobMsgAddressStr.c_str()
	<< " - Local blob address: " << fOwnBlobMsgAddressStr.c_str() << "." << ENDLOG;
    LOG( AliHLTLog::kInformational, "AliHLTControlledBridgeComponent::ShowConfiguration", "SC config" )
	<< "SC interface address: " << fSCInterfaceAddress.c_str() << ENDLOG;
    LOG( AliHLTLog::kInformational, "AliHLTControlledBridgeComponent::ShowConfiguration", "Log config" )
	<< (fStdoutLogging ? "Stdout logging, " : "") << (fFileLogging ? "File logging, " : "")
	<< (fSysLogging ? "Syslog logging, " : "") << "logname: " << fLogName.c_str()
	<< (fSysMESLogging ? ", SysMES logging" : "")
	<< (fDynLibLogging ? ", Dynamic library logging to " : "")
	<< (fDynLibLogging ? fDynLibPath.c_str() : "")
	<< "." << ENDLOG;
    LOG( AliHLTLog::kInformational, "AliHLTControlledBridgeComponent::ShowConfiguration", "Shm config" )
	<< "Event slot pre-allocation count: " << AliHLTLog::kDec
	<< fMaxPreEventCount << " - maximum no use count: " << fMaxNoUseCount
	<< "." << ENDLOG;

    typename vector<LAMAddressData>::iterator lamIter, lamEnd;
    pthread_mutex_lock( &fLAMAddressMutex );
    lamIter = fLAMAddresses.begin();
    lamEnd = fLAMAddresses.end();
    while ( lamIter != lamEnd )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledBridgeComponent::ShowConfiguration", "LAM address config" )
	    << "Specified LAM address: '" << lamIter->fStr.c_str() << "'." << ENDLOG;
	lamIter++;
	}
    pthread_mutex_unlock( &fLAMAddressMutex );
    if ( fAliceHLT )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledBridgeComponent::ShowConfiguration", "ALICE HLT" )
	    << "Special ALICE HLT options activated." << ENDLOG;
	}
    if ( fEventAccounting )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "Event Accounting is active" )
	    << "Full event accounting is active." << ENDLOG;
	}
    }


template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::Configure()
    {
    fDescriptorHandler = CreateDescriptorHandler();
    if ( !fDescriptorHandler )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::Configure", "Unable to Create Descriptor Handler" )
	    << "Unable to create event descriptor handler." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	return;
	}

    fShmManager = CreateShmManager();
    if ( !fShmManager )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::Configure", "Error creating shared memory manager" )
	    << "Error creating Shared Memory Manager object." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	return;
	}


    int ret;
    bool isBlob;
    BCLCommunication* tmpCom;
    ret = BCLDecodeLocalURL( fOwnMsgAddressStr.c_str(), tmpCom, fOwnMsgAddress, isBlob );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::Configure", "Error in local msg configuration" )
	    << "Error in local msg configuration '" << fOwnMsgAddressStr.c_str() << "': "
	    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	return;
	}
    if ( isBlob )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::Configure", "Error in local msg configuration" )
	    << "Error in local msg configuration '" << fOwnMsgAddressStr.c_str() << "': Communication object is a blob object."
	    << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	return;
	}
    fMsgCom = (BCLMsgCommunication*)tmpCom;
    ret = BCLDecodeRemoteURL( fRemoteMsgAddressStr.c_str(), fRemoteMsgAddress );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::Configure", "Error in remote msg configuration" )
	    << "Error in remote msg configuration '" << fRemoteMsgAddressStr.c_str() << "': "
	    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	return;
	}

    ret = BCLDecodeLocalURL( fOwnBlobMsgAddressStr.c_str(), tmpCom, fOwnBlobMsgAddress, isBlob );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::Configure", "Error in local blob msg configuration" )
	    << "Error in local blob msg configuration '" << fOwnBlobMsgAddressStr.c_str() << "': "
	    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	return;
	}
    if ( isBlob )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::Configure", "Error in local blob msg configuration" )
	    << "Error in local blob msg configuration '" << fOwnBlobMsgAddressStr.c_str() << "': Communication object is a blob object."
	    << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	return;
	}
    fBlobMsgCom = (BCLMsgCommunication*)tmpCom;
    ret = BCLDecodeRemoteURL( fRemoteBlobMsgAddressStr.c_str(), fRemoteBlobMsgAddress );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::Configure", "Error in remote blob msg configuration" )
	    << "Error in remote blob msg configuration '" << fRemoteBlobMsgAddressStr.c_str() << "': "
	    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	return;
	}

    ret = BCLDecodeLocalURL( fOwnBlobAddressStr.c_str(), tmpCom, fOwnBlobAddress, isBlob );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::Configure", "Error in local blob configuration" )
	    << "Error in local blob configuration '" << fOwnBlobAddressStr.c_str() << "': "
	    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	return;
	}
    if ( !isBlob )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::Configure", "Error in local blob configuration" )
	    << "Error in local blob configuration '" << fOwnBlobAddressStr.c_str() << "': Communication object is not blob object."
	    << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	return;
	}
    fBlobCom = (BCLBlobCommunication*)tmpCom;

    typename vector<LAMAddressData>::iterator lamIter, lamEnd;
    pthread_mutex_lock( &fLAMAddressMutex );
    lamIter = fLAMAddresses.begin();
    lamEnd = fLAMAddresses.end();
    while ( lamIter != lamEnd )
	{
	ret = BCLDecodeRemoteURL( lamIter->fStr.c_str(), lamIter->fAddress );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::Configure", "Error decoding LAM address" )
		<< "Error decoding LAM address '" << lamIter->fStr.c_str() << "'." << ENDLOG;
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	    break;
	    }
	lamIter++;
	}
    pthread_mutex_unlock( &fLAMAddressMutex );
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
    if ( fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag )
	return;
    }

template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::Unconfigure()
    {
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Unconfigure", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    typename vector<LAMAddressData>::iterator lamIter, lamEnd;
    pthread_mutex_lock( &fLAMAddressMutex );
    lamIter = fLAMAddresses.begin();
    lamEnd = fLAMAddresses.end();
    while ( lamIter != lamEnd )
	{
	if ( lamIter->fAddress )
	    BCLFreeAddress( lamIter->fAddress );
	lamIter++;
	}
    pthread_mutex_unlock( &fLAMAddressMutex );
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Unconfigure", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;

    if ( fBlobCom || fOwnBlobAddress )
	BCLFreeAddress( fBlobCom, fOwnBlobAddress );
    fBlobCom = NULL;
    fOwnBlobAddress = NULL;
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Unconfigure", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    if ( fRemoteBlobMsgAddress )
	BCLFreeAddress( fRemoteBlobMsgAddress );
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Unconfigure", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    fRemoteBlobMsgAddress = NULL;
    if ( fBlobMsgCom || fOwnBlobMsgAddress )
	BCLFreeAddress( fBlobMsgCom, fOwnBlobMsgAddress );
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Unconfigure", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    fBlobMsgCom = NULL;
    fOwnBlobMsgAddress = NULL;
    if ( fRemoteMsgAddress )
	BCLFreeAddress( fRemoteMsgAddress );
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Unconfigure", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    fRemoteMsgAddress = NULL;
    if ( fMsgCom || fOwnMsgAddress )
	BCLFreeAddress( fMsgCom, fOwnMsgAddress );
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Unconfigure", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    fMsgCom = NULL;
    fOwnMsgAddress = NULL;

    if ( fShmManager )
	{
	delete fShmManager;
	fShmManager = NULL;
	}
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Unconfigure", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;

    if ( fDescriptorHandler )
	{
	delete fDescriptorHandler;
	fDescriptorHandler = NULL;
	}
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Unconfigure", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    if ( fEventAccounting )
	{
	delete fEventAccounting;
	fEventAccounting = NULL;
	}
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Unconfigure", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    }


template<class BridgeHeadClass>
inline bool AliHLTControlledBridgeComponent<BridgeHeadClass>::SetupStdLogging()
    {
    BCLAddressLogger::Init();
    if ( fStdoutLogging )
	{
	if ( !fLogOut )
	    {
	    fLogOut = new AliHLTFilteredStdoutLogServer;
	    if ( !fLogOut )
		return false;
	    gLog.AddServer( fLogOut );
	    }
	}
    else
	{
	if ( fLogOut )
	    {
	    gLog.DelServer( fLogOut );
	    delete fLogOut;
	    fLogOut = NULL;
	    }
	}
    gLog.SetStackTraceComparisonDepth( 1 );
    return true;
    }

template<class BridgeHeadClass>
inline bool AliHLTControlledBridgeComponent<BridgeHeadClass>::SetupFileLogging()
    {
    if ( fFileLogging )
	{
	if ( !fFileLog )
	    {
	    fFileLog = new AliHLTFileLogServer( fLogName.c_str(), ".log", 2500000 );
	    if ( !fFileLog )
		return false;
	    gLog.AddServer( fFileLog );
	    }
	}
    else
	{
	if ( fFileLog )
	    {
	    gLog.DelServer( fFileLog );
	    delete fFileLog;
	    fFileLog = NULL;
	    }
	}
    return true;
    }

template<class BridgeHeadClass>
inline bool AliHLTControlledBridgeComponent<BridgeHeadClass>::SetupSysLogging()
    {
    if ( fSysLogging )
	{
	if ( !fSysLog )
	    {
	    fSysLog = new AliHLTSysLogServer( fLogName.c_str(), fSysLogFacility );
	    if ( !fSysLog )
		return false;
	    gLog.AddServer( fSysLog );
	    }
	}
    else
	{
	if ( fSysLog )
	    {
	    gLog.DelServer( fSysLog );
	    delete fSysLog;
	    fSysLog = NULL;
	    }
	}
    return true;
    }

template<class BridgeHeadClass>
inline bool AliHLTControlledBridgeComponent<BridgeHeadClass>::SetupSysMESLogging()
    {
    if ( fSysMESLogging )
	{
	if ( !fSysMESLog )
	    {
	    fSysMESLog = new MLUCSysMESLogServer;
	    if ( !fSysMESLog )
		return false;
	    gLog.AddServer( fSysMESLog );
	    }
	}
    else
	{
	if ( fSysMESLog )
	    {
	    gLog.DelServer( fSysMESLog );
	    delete fSysMESLog;
	    fSysMESLog = NULL;
	    }
	}
    return true;
    }

template<class BridgeHeadClass>
inline bool AliHLTControlledBridgeComponent<BridgeHeadClass>::SetupDynLibLogging()
    {
    if ( fDynLibLogging )
	{
        if ( !fDynLibLog )
	    {
            fDynLibLog = new MLUCDynamicLibraryLogServer(fDynLibPath.c_str(), fComponentName );
            if ( !fDynLibLog )
                return false;
            gLog.AddServer( fDynLibLog );
	    }
	}
    else
	{
        if ( fDynLibLog )
	    {
            gLog.DelServer( fDynLibLog );
            delete fDynLibLog;
            fDynLibLog = NULL;
	    }
	}
    return true;
    }

template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::RemoveLogging()
    {
    if ( fDynLibLog )
	{
        gLog.DelServer( fDynLibLog );
        delete fDynLibLog;
        fDynLibLog = NULL;
	}
    if ( fSysMESLog )
	{
	gLog.DelServer( fSysMESLog );
	delete fSysMESLog;
	fSysMESLog = NULL;
	}
    if ( fSysLog )
	{
	gLog.DelServer( fSysLog );
	delete fSysLog;
	fSysLog = NULL;
	}
    if ( fFileLog )
	{
	gLog.DelServer( fFileLog );
	delete fFileLog;
	fFileLog = NULL;
	}
    if ( fLogOut )
	{
	gLog.DelServer( fLogOut );
	delete fLogOut;
	fLogOut = NULL;
	}
    }


template<class BridgeHeadClass>
inline bool AliHLTControlledBridgeComponent<BridgeHeadClass>::ParseCmdLine()
    {
    int i, errorArg=-1;
    const char* errorStr = NULL;
    
    i = 1;
    while ( (i < fArgc) && !errorStr )
	{
	errorStr = ParseCmdLine( fArgc, fArgv, i, errorArg );
	}
    if ( errorStr )
	{
	PrintUsage( errorStr, i, errorArg );
	return false;
	}
    return true;
    }

template<class BridgeHeadClass>
inline const char* AliHLTControlledBridgeComponent<BridgeHeadClass>::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cpErr;
    if ( !strcmp( argv[i], "-name" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing component name parameter";
	    }
	fComponentName = argv[i+1];
	fLogName = fComponentName;
	i += 2;
	return  NULL;
	}
    if ( !strcmp( argv[i], "-control" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing Status&Control interface address URL parameter";
	    }
	fSCInterfaceAddress = argv[i+1];
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-V" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing verbosity level specifier.";
	    }
	gLogLevel = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting verbosity level specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-nostdout" ) )
	{
	fStdoutLogging = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-nofilelog" ) )
	{
	fFileLogging = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-syslog" ) )
	{
	if ( argc <= i+2 )
	    {
	    return "Missing syslog facility parameter";
	    }
	if ( !strcmp( argv[i+1], "DAEMON" ) )
	    {
	    fSysLogFacility = LOG_DAEMON;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL0" ) )
	    {
	    fSysLogFacility = LOG_LOCAL0;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL1" ) )
	    {
	    fSysLogFacility = LOG_LOCAL1;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL2" ) )
	    {
	    fSysLogFacility = LOG_LOCAL2;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL3" ) )
	    {
	    fSysLogFacility = LOG_LOCAL3;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL4" ) )
	    {
	    fSysLogFacility = LOG_LOCAL4;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL5" ) )
	    {
	    fSysLogFacility = LOG_LOCAL5;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL6" ) )
	    {
	    fSysLogFacility = LOG_LOCAL6;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL7" ) )
	    {
	    fSysLogFacility = LOG_LOCAL7;
	    }
	else if ( !strcmp( argv[i+1], "USER" ) )
	    {
	    fSysLogFacility = LOG_USER;
	    }
	else
	    {
	    return "Unknown syslog facility parameter.";
	    }
	fSysLogging = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-sysmeslog" ) )
	{
	fSysMESLogging = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-infologger" ) )
	{
        if ( argc <= i+1 )
	    {
            return "Missing path to dynamic log library.";
	    }
        fDynLibPath = argv[i+1];
        fDynLibLogging = true;
        i += 2;
        return NULL;
	}
    if ( !strcmp( argv[i], "-eventsexp2" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing event count (power of 2) specifier.";
	    }
	fMaxPreEventCountExp2 = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting event count (power of 2) specifier..";
	    }
	fMaxPreEventCount = 1 << fMaxPreEventCountExp2;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-blob" ) )
	{
	if ( argc <= i +2 )
	    {
	    return "Missing blob address options.";
	    }
	if ( fBlobCom )
	    {
	    return "Duplicate -blob option.";
	    }
	fBlobBufferSize = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr == 'k')
	    fBlobBufferSize *= 1024;
	else if ( *cpErr == 'M')
	    fBlobBufferSize *= 1024*1024;
	else if ( *cpErr == 'G')
	    fBlobBufferSize *= 1024*1024*1024;
	else if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting blob buffersize argument.";
	    }
	fOwnBlobAddressStr = argv[i+2];
	i += 3;
	return NULL;
	}
    if ( !strcmp( argv[i], "-blobmsg" ) )
	{
	if ( argc <= i +2 )
	    {
	    return "Missing blob msg address options.";
	    }
	if ( fBlobMsgCom )
	    {
	    return "Duplicate -blobmsg option.";
	    }
	fOwnBlobMsgAddressStr = argv[i+1];
	fRemoteBlobMsgAddressStr = argv[i+2];
	i += 3;
	return NULL;
	}
    if ( !strcmp( argv[i], "-msg" ) )
	{
	if ( argc <= i +2 )
	    {
	    return "Missing msg address options.";
	    }
	if ( fMsgCom )
	    {
	    return "Duplicate -msg option.";
	    }
	fOwnMsgAddressStr = argv[i+1];
	fRemoteMsgAddressStr = argv[i+2];
	i += 3;
	return NULL;
	}
    if ( !strcmp( argv[i], "-lamaddress" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing lam address specifier.";
	    }
	LAMAddressData lad;
	lad.fAddress = NULL;
	lad.fStr = argv[i+1];
	fLAMAddresses.insert( fLAMAddresses.end(), lad );
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-alicehlt" ) )
	{
	fAliceHLT = true;
	i += 1;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventaccounting" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing event accounting output file directory.";
	    }
	fEventAccounting = new AliHLTEventAccounting;
	fEventAccounting->SetDirectory( argv[i+1] );
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-runnr" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing run number specifier.";
	    }
	fRunNumber = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting run number specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-beamtype" ) )
	{
	if ( argc <= i + 1 )
	    {
	    return "Missing beam type specifier.";
	    }
	fBeamType = 0;
	if ( !strcmp(argv[i+1], "pp") )
	    fBeamType = 1;
	else if ( !strcmp(argv[i+1], "AA") )
	    fBeamType = 2;
	else if ( !strcmp(argv[i+1], "pA") )
	    fBeamType = 3;
	else
	    {
	    errorArg = i+1;
	    return "Wrong first parameter for beam type (must be 'pp' or 'AA' or 'pA').";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-hltmode" ) )
	{
	if ( argc <= i + 1 )
	    {
	    return "Missing HLT mode specifier.";
	    }
	fHLTMode = 0;
	if ( !strcmp(argv[i+1],"A") )
	    fHLTMode = 1;
	else if ( !strcmp(argv[i+1],"B") )
	    fHLTMode = 2;
	else if ( !strcmp(argv[i+1],"C") )
	    fHLTMode = 3;
	else if ( !strcmp(argv[i+1],"D") )
	    fHLTMode = 4;
	else if ( !strcmp(argv[i+1],"E") )
	    fHLTMode = 5;
	else
	    {
	    errorArg = i+1;
	    return "Wrong second parameter for HLT mode (must be 'A', 'B', 'C', 'D' or 'E').";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-runtype" ) )
	{
	if ( argc <= i + 1 )
	    {
	    return "Missing run type specifier.";
	    }
	fRunType = argv[i+1];
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-readoutlistversion" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing readout list data format version number specifier.";
	    }
	gReadoutListVersion = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting readout list data format version specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-ctptriggerclasses" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing CTP trigger class specifier.";
	    }
	MLUCString tcString( argv[i+1] );
	if ( tcString.Length()>0 )
	    {
	    std::vector<MLUCString> triggerClasses, triggerClass, detectors;
	    tcString.Split( triggerClasses, ',' );
	    std::vector<MLUCString>::iterator tcIter, tcEnd;
	    tcIter = triggerClasses.begin();
	    tcEnd = triggerClasses.end();
	    while ( tcIter != tcEnd )
		{
		tcIter->Split( triggerClass, ':' );
		if ( triggerClass.size()!=3 )
		    return "Error parsing trigger class specifier (single trigger class format)";
		unsigned long ndx=0;
		ndx = strtoul( triggerClass[0].c_str(), &cpErr, 10 );
		if ( *cpErr != '\0' )
		    return "Error parsing trigger class specifier (single trigger class bit index specifier)";
		
		fStackMachineAssembler.AddSymbol( triggerClass[1].c_str(), 1 << ndx );
		
		fTriggerClassBitIndices.insert( std::pair<MLUCString,unsigned>( triggerClass[1], ndx ) );
		
		tcIter++;
		}
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-ctptriggerclassgroup" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing CTP trigger class group identifier and match specification";
	    }
	if ( argc <= i+2 )
	    {
	    return "Missing CTP trigger class group match specification";
	    }
	fTriggerClassGroups.insert( std::pair<MLUCString,MLUCString>( MLUCString(argv[i+1]), MLUCString(argv[i+2]) ) );
	i += 3;
	return NULL;
	}
    return "Unknown Parameter";
    }

template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::PrintUsage( const char* errorStr, int errorArg, int errorParam, int argc, char** argv )
    {
    if ( argc==-1 )
	argc = fArgc;
    if ( argv==NULL )
	argv = fArgv;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Usage" )
	<< "Error parsing command line arguments: " << errorStr << "." << ENDLOG;
    if ( errorArg>=0 && errorArg<argc )
	{
	LOG( AliHLTLog::kError, "Controlled Bridge Component", "Usage" )
	    << "Offending argument: " << argv[errorArg] << "." << ENDLOG;
	}
    if ( errorParam>=0 && errorParam<argc )
	{
	LOG( AliHLTLog::kError, "Controlled Bridge Component", "Usage" )
	    << "Offending parameter: " << argv[errorParam] << "." << ENDLOG;
	}
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-control <status&control address URL>: Specify the address on which the Status&Control listens for remote supervision." << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-V <verbosity>: Specifies the verbosity to use in the program. Set bits correspond to: 1 - benchmark, 2 - information, 4 - debug, 8 - warning, 16 - error, 32 - fatal error. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-nostdout: Inihibit logging to stdout. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-nofilelog: Inihibit logging to files. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-syslog <facility>: Send log messages to the syslog daemon. 'facility' can be one of the following values: DAEMON,USER,LOCAL0,LOCAL1,LOCAL2,LOCAL3,LOCAL4,LOCAL5,LOCAL6,LOCAL7. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-sysmeslog: Send log messages to the SysMES system. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-infologger <path-to-infologger-logserver-shared-library>: Send log messages to the infoLogger system via the given shared library. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-eventsexp2 <event-count-power-of-2>: Specify the power of two for the number of event slots to pre-allocate. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-blobmsg <blobmsg-own-address-URL> <blobmsg-remote-address-URL>: Specify the local and remote blob message addresses. (Mandatory)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-msg <msg-own-address-URL> <msg-remote-address-URL>: Specify the local and remote message addresses. (Mandatory)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-blob <blobsize> <blob-own-address-URL>: Specify the local blob buffer size and bob address. (Mandatory)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-lamaddress <lam-target-address-URL>: Specify an address to which Look-At-Me (LAM) requests (interrupts) will be sent. (Optional, may be used multiple times)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-alicehlt: Activate some special features and options useful only for the ALICE HLT. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-runnr <run-number>: Set the run number to be used. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-beamtype [pp|AA]: Set the type of beam, either pp or AA. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-hltmode [A|B|C|D|E]: Set the HLT mode for this run, A, B, C, D, or E. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-runtype <run-type-string>: Set the type of run. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-readoutlistversion <readout-list-data-format-version-number>: Set the version number of the data format for the readout list data. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-ctptriggerclasses <CTP-Trigger-Class-String>: Specify the ECS string containing the trigger class specification to the component. (Optional, only in special ALICE HLT mode)." << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-ctptriggerclassgroup <CTP-Trigger-Class-Group-Identifier> <CTP-Trigger-Class-Group-RegEx-Match-Specification>: Specify a group of trigger classes/bits, identifieed by a common identifier, that are specified by a regex applied to the trigger class identifier. (Optional, only in special ALICE HLT mode)" << ENDLOG;
    }

template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::IllegalCmd( AliUInt32_t state, AliUInt32_t cmd )
    {
    MLUCString cmdStr, stateStr;
    AliHLTSCComponentInterface::GetCommandName( cmd, cmdStr );
    AliHLTSCComponentInterface::GetStatusName( state, stateStr );
    LOG( AliHLTLog::kWarning, "AliHLTControlledBridgeComponent::IllegalCmd", "Illegal Command" )
	<< "Illegal command '" << cmdStr.c_str() << "' 0x" << AliHLTLog::kHex << cmd << "/" << AliHLTLog::kDec
	<< cmd << ") for state '" << stateStr.c_str() << "' (0x" << AliHLTLog::kHex << state << "/" << AliHLTLog::kDec
	<< state << ")." << ENDLOG;
    }


template<class BridgeHeadClass>
inline bool AliHLTControlledBridgeComponent<BridgeHeadClass>::SetupSC()
    {
    fStatus = CreateStatusData();
    if ( !fStatus )
	return false;
    if ( !SetupStatusData() )
	return false;

    fSCInterface = CreateSCInterface();
    if ( !fSCInterface )
	return false;
    if ( !SetupSCInterface() )
	return false;

    return true;
    }

template<class BridgeHeadClass>
inline AliHLTBridgeStatusData* AliHLTControlledBridgeComponent<BridgeHeadClass>::CreateStatusData()
    {
    return new AliHLTBridgeStatusData;
    }

template<class BridgeHeadClass>
inline bool AliHLTControlledBridgeComponent<BridgeHeadClass>::SetupStatusData()
    {
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    fStatus->fProcessStatus.fState = 0;
    fStatus->fProcessStatus.fState |= kAliHLTPCStartedStateFlag;
    stateLock.Unlock();
    //     fStatus->fProcessStatus.fState = kAliHLTPCStartedState;
    switch ( fComponentType )
	{
	case kPublisherBridgeHead:
	    fStatus->fProcessStatus.fProcessType = kAliHLTSCControlledBridgeComponentType;
	    break;
	case kSubscriberBridgeHead:
	    fStatus->fProcessStatus.fProcessType = kAliHLTSCControlledBridgeComponentType;
	    break;
	default:
	    fStatus->fProcessStatus.fProcessType = kAliHLTSCUndefinedComponentType;
	    break;
	}
    fStatus->fBridgeStatus.fMsgConnected = 0;
    fStatus->fBridgeStatus.fBlobConnected = 0;
    fStatus->Touch();
    return true;
    }

template<class BridgeHeadClass>
inline AliHLTSCInterface* AliHLTControlledBridgeComponent<BridgeHeadClass>::CreateSCInterface()
    {
    return new AliHLTSCInterface();
    }

template<class BridgeHeadClass>
inline bool AliHLTControlledBridgeComponent<BridgeHeadClass>::SetupSCInterface()
    {
    int ret;
    ret = fSCInterface->Bind( fSCInterfaceAddress.c_str() );
    if ( ret )
	{
	return false;
	}
    ret = fSCInterface->Start();
    if ( ret )
	{
	return false;
	}
    fSCInterface->SetStatusData( &(fStatus->fHeader) );
    fSCInterface->AddCommandProcessor( &fCmdProcessor );
    return true;
    }

template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::RemoveSC()
    {
    if ( fSCInterface )
	{
	fSCInterface->Stop();
	fSCInterface->Unbind();
	delete fSCInterface;
	fSCInterface = NULL;
	}
    if ( fStatus )
	{
	delete fStatus;
	fStatus = NULL;
	}
    }

template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::LookAtMe()
    {
    typename vector<LAMAddressData>::iterator lamIter, lamEnd;
    int ret;
    pthread_mutex_lock( &fLAMAddressMutex );
    lamIter = fLAMAddresses.begin();
    lamEnd = fLAMAddresses.end();
    while ( lamIter != lamEnd )
	{
	if ( lamIter->fAddress )
	    {
	    ret = fSCInterface->LookAtMe( lamIter->fAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent<BaseClass,PublisherClass,SubscriberClass>::LookAtMe", "Error sending LAM request" )
		    << "Error sending LAM request to LAM address '" << lamIter->fStr.c_str() << ": "
		    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
		}
	    }
	lamIter++;
	}
    pthread_mutex_unlock( &fLAMAddressMutex );
    }

template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::ProcessSCCmd( AliHLTSCCommandStruct* cmdS )
    {
    bool ok = false;
    char* newMsgComURL = NULL;
    char* newBlobMsgComURL = NULL;
    MLUCString* newMsgComStr = NULL;
    MLUCString* newBlobMsgComStr = NULL;
    unsigned long offset = 0;
    unsigned long dataOffset;
    AliHLTSCCommand cmd( cmdS );
    dataOffset = ((unsigned long)cmdS->fData) - ((unsigned long)cmdS);
    if ( !fStatus )
	{
	// XXX
	return;
	}
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    switch ( cmd.GetData()->fCmd )
	{
	case kAliHLTPCNOPCmd:
	    ok = true;
	    break;
	case kAliHLTPCEndProgramCmd:
	    if ( !(fStatus->fProcessStatus.fState & ~(kAliHLTPCStartedStateFlag|kAliHLTPCErrorStateFlag)) )
		{
		ok = true;
		fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
		fCmdSignal.Signal();
		}
	    break;
	case kAliHLTPCConfigureCmd:
	    if ( fStatus->fProcessStatus.fState & ~kAliHLTPCStartedStateFlag )
		break;
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag )
		{
		ok = true;
		break;
		}
	    ok = true;
	    fStatus->fProcessStatus.fState |= kAliHLTPCConfiguredStateFlag|kAliHLTPCChangingStateFlag;
	    stateLock.Unlock();
	    ExtractConfig( cmdS );
	    ShowConfiguration();
	    if ( !CheckConfiguration() )
		{
		stateLock.Lock();
		fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
		break;
		}

	    Configure();
	    stateLock.Lock();
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		{
		stateLock.Unlock();
		SetupComponents();
		}
	    break;
	case kAliHLTPCUnconfigureCmd:
	    if ( fStatus->fProcessStatus.fState & ~(kAliHLTPCConfiguredStateFlag|kAliHLTPCStartedStateFlag|kAliHLTPCErrorStateFlag) )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
		{
		ok = true;
		break;
		}
	    ok = true;
	    stateLock.Unlock();
	    Unconfigure();
	    stateLock.Lock();
	    fStatus->fProcessStatus.fState = fStatus->fProcessStatus.fState & ~(kAliHLTPCConfiguredStateFlag|kAliHLTPCErrorStateFlag);
	    break;
	case kAliHLTPCResetConfigurationCmd:
	    if ( fStatus->fProcessStatus.fState & ~kAliHLTPCStartedStateFlag )
		break;
	    ok = true;
	    stateLock.Unlock();
	    ResetConfiguration();
	    break;
	    // 	case kAliHLTPCAcceptSubscriptionsCmd:
	    // 	    if ( GetMinPublisherCount()<=0 )
	    // 		break;
	    // 	    if ( fStatus->fProcessStatus.fState & ~(kAliHLTPCStartedStateFlag|kAliHLTPCConfiguredStateFlag|kAliHLTPCSubscribedStateFlag) )
	    // 		break;
	    // 	    ok = true;
	    // 	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    // 	    fCmdSignal.Signal();
	    // 	    break;
	    // 	case kAliHLTPCStopSubscriptionsCmd:
	    // 	    if ( GetMinPublisherCount()<=0 )
	    // 		break;
	    // 	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) ||
	    // 		 (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
	    // 		break;
	    // 	    ok = true;
	    // 	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    // 	    fCmdSignal.Signal();
	    // 	    break;
	    // 	case kAliHLTPCCancelSubscriptionsCmd:
	    // 	    if ( GetMinPublisherCount()<=0 )
	    // 		break;
	    // 	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) ||
	    // 		 (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
	    // 		break;
	    // 	    ok = true;
	    // 	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    // 	    fCmdSignal.Signal();
	    // 	    break;
	    // 	case kAliHLTPCSubscribeCmd:
	    // 	    if ( GetMinSubscriberCount()<=0 )
	    // 		break;
	    // 	    if ( fStatus->fProcessStatus.fState & ~(kAliHLTPCStartedStateFlag|kAliHLTPCConfiguredStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag) )
	    // 		break;
	    // 	    ok = true;
	    // 	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    // 	    fCmdSignal.Signal();
	    // 	    break;
	    // 	case kAliHLTPCUnsubscribeCmd:
	    // 	    if ( GetMinSubscriberCount()<=0 )
	    // 		break;
	    // 	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) ||
	    // 		 (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
	    // 		break;
	    // 	    ok = true;
	    // 	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    // 	    fCmdSignal.Signal();
	    // 	    break;
	case kAliHLTPCStartProcessingCmd:
	    {
	    // 	    if ( GetMinPublisherCount()>0 )
	    // 		{
	    // 		if ( (fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) &&
	    // 		     !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
	    // 		    ok = true;
	    // 		}
	    // 	    if ( GetMinSubscriberCount()>0 )
	    // 		{
	    // 		if ( (fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) &&
	    // 		     !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
	    // 		    ok = true;
	    // 		}
	    ok = true;
	    if ( fComponentType==kPublisherBridgeHead )
		{
		if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) ||
// 		     !(fStatus->fProcessStatus.fState & kAliHLTPCConnectedStateFlag) ||
		     (fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		    ok = false;
		}
	    else if ( fComponentType==kSubscriberBridgeHead )
		{
		if ( !(fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) ||
// 		     !(fStatus->fProcessStatus.fState & kAliHLTPCConnectedStateFlag) ||
		     (fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		    ok = false;
		}
	    else
		ok = false;
	    if ( ok )
		{
		if ( fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag )
		    break;
		fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
		fCmdSignal.Signal();
		}
	    break;
	    }
	case kAliHLTPCBindCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
		break;
	    if ( fStatus->fProcessStatus.fState & ~(kAliHLTPCStartedStateFlag|kAliHLTPCConfiguredStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag|kAliHLTPCWithSubscribersStateFlag|kAliHLTPCSubscribedStateFlag|kAliHLTPCBoundStateFlag) )
 		break;
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCBoundStateFlag )
		{
		ok = true;
 		break;
		}
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCUnbindCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCBoundStateFlag) )
		{
		ok = true;
		break;
		}
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCConnectCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCBoundStateFlag) )
		break;
// 	    if ( fStatus->fProcessStatus.fState & ~(kAliHLTPCStartedStateFlag|kAliHLTPCConfiguredStateFlag|kAliHLTPCBoundStateFlag|kAliHLTPCSubscribedStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag) )
//  		break;
// 	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag && !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag)) ||
// 		 (fStatus->fProcessStatus.fState & kAliHLTPCBusyStateFlag) )
// 		break;
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCBusyStateFlag) )
		break;
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCConnectedStateFlag )
		{
		ok = true;
		break;
		}
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCDisconnectCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConnectedStateFlag) )
		{
		ok = true;
		break;
		}
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag && !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) && 
		 (fStatus->fProcessStatus.fState & kAliHLTPCBusyStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCReconnectCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConnectedStateFlag) )
		break;
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag && !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCNewConnectionCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConnectedStateFlag) )
		break;
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag )
		break;
	    stateLock.Unlock();
	    ok = true;
	    if ( cmd.GetData()->fParam0 & 1 )
		{
		newMsgComURL = (char*)cmdS->fData;
		if ( (strlen(newMsgComURL)+dataOffset) > cmd.GetData()->fLength )
		    {
		    LOG( AliHLTLog::kWarning, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "Length Error" )
			<< "Error (1) in message length: " << AliHLTLog::kDec << (strlen(newMsgComURL)+dataOffset)
			<< " greater than " << cmd.GetData()->fLength << ENDLOG;
		    }
		else
		    {
		    LOG( AliHLTLog::kDebug, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "New msg address" )
			<< "New Msg Address: " << newMsgComURL << "." << ENDLOG;
		    }
		newMsgComStr = new MLUCString( newMsgComURL );
		}	    
	    if ( cmd.GetData()->fParam0 & 2 )
		{
		newBlobMsgComURL = ((char*)(cmdS->fData))+offset;
		if ( (strlen(newBlobMsgComURL)+dataOffset+offset) > cmd.GetData()->fLength )
		    {
		    LOG( AliHLTLog::kWarning, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "Length Error" )
			<< "Error (2) in message length: " << AliHLTLog::kDec << (strlen(newBlobMsgComURL)+dataOffset+offset)
			<< " greater than " << cmd.GetData()->fLength << ENDLOG;
		    }
		else
		    {
		    LOG( AliHLTLog::kDebug, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "New blob msg address" )
			<< "New Blob Msg Address: " << newBlobMsgComURL << "." << ENDLOG;
		    }
		newBlobMsgComStr = new MLUCString( newBlobMsgComURL );
		}
	    fCmdSignal.Lock();
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.AddNotificationData( reinterpret_cast<AliUInt64_t>(newMsgComStr) );
	    fCmdSignal.AddNotificationData( reinterpret_cast<AliUInt64_t>(newBlobMsgComStr) );
	    fCmdSignal.Unlock();
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCNewRemoteAddressesCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
		break;
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag && !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) )
		break;
	    stateLock.Unlock();
	    ok = true;
	    if ( cmd.GetData()->fParam0 & 1 )
		{
		newMsgComURL = (char*)cmdS->fData;
		if ( (strlen(newMsgComURL)+dataOffset) > cmd.GetData()->fLength )
		    {
		    LOG( AliHLTLog::kWarning, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "Length Error" )
			<< "Error (1) in message length: " << AliHLTLog::kDec << (strlen(newMsgComURL)+dataOffset)
			<< " greater than " << cmd.GetData()->fLength << ENDLOG;
		    }
		else
		    {
		    LOG( AliHLTLog::kDebug, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "New msg address" )
			<< "New Msg Address: " << newMsgComURL << "." << ENDLOG;
		    }
		newMsgComStr = new MLUCString( newMsgComURL );
		}	    
	    if ( cmd.GetData()->fParam0 & 2 )
		{
		newBlobMsgComURL = ((char*)(cmdS->fData))+offset;
		if ( (strlen(newBlobMsgComURL)+dataOffset+offset) > cmd.GetData()->fLength )
		    {
		    LOG( AliHLTLog::kWarning, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "Length Error" )
			<< "Error (2) in message length: " << AliHLTLog::kDec << (strlen(newBlobMsgComURL)+dataOffset+offset)
			<< " greater than " << cmd.GetData()->fLength << ENDLOG;
		    }
		else
		    {
		    LOG( AliHLTLog::kDebug, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "New blob msg address" )
			<< "New Blob Msg Address: " << newBlobMsgComURL << "." << ENDLOG;
		    }
		newBlobMsgComStr = new MLUCString( newBlobMsgComURL );
		}
	    fCmdSignal.Lock();
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.AddNotificationData( reinterpret_cast<AliUInt64_t>(newMsgComStr) );
	    fCmdSignal.AddNotificationData( reinterpret_cast<AliUInt64_t>(newBlobMsgComStr) );
	    fCmdSignal.Unlock();
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCPURGEALLEVENTSCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
		break;
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag && !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCPauseProcessingCmd:
	    // 	    if ( fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag )
	    // 		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCResumeProcessingCmd:
	    // 		if ( fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag )
	    // 		    break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCStopProcessingCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		{
		ok = true;
		break;
		}
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCSimulateErrorCmd:
	    if ( !(fStatus->fProcessStatus.fState & ~kAliHLTPCStartedStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCDumpEventAccountingCmd:
	    stateLock.Unlock();
	    if ( fEventAccounting )
		fEventAccounting->DumpEvents();
	    ok = true;
	    break;
	case kAliHLTPCSetSubsystemVerbosityCmd:
	    ok = true;
	    // Not supported/applicable here. Ignore
	    break;
	case kAliHLTPCDumpEventMetaDataCmd:
	    stateLock.Unlock();
	    fBridgeHead->DumpEventMetaData();
	    stateLock.Lock();
	    ok = true;
	    break;
	case kAliHLTPCInterruptConnectionCmd:
	    LOG( AliHLTLog::kWarning, "AliHLTControlledBridgeComponent<BridgeHeadClass>::ProcessSCCmd", "Interrupt Connection Command Received" )
		<< "Received command to forcefully interrupt data connection." << ENDLOG;
	    fBridgeHead->INTERRUPTBLOBCONNECTION();
	    return;
	}
    if ( !ok )
	IllegalCmd( fStatus->fProcessStatus.fState, cmd.GetData()->fCmd );
    }

template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::ExtractConfig( AliHLTSCCommandStruct* cmdS )
    {
    char* config = (char*)cmdS->fData;
    if ( strlen(config)<=0 )
	return;
    MLUCCmdlineParser parser;
    vector<MLUCString> tokens;
    if ( !parser.ParseCmd( config, tokens ) )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::ExtractConfig", "Error parsing configuration string" )
	    << "Error parsing configuration string '" << config << "'." << ENDLOG;
	return;
	}
    int argc = tokens.size();
    if ( argc<=0 )
	return;
    char** argv;
    argv = new char*[ argc+1 ];
    if ( !argv )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::ExtractConfig", "Out of memory" )
	    << "Out of memory allocating temporary buffer of " << AliHLTLog::kDec
	    << argc*sizeof(char*) << " bytes." << ENDLOG;
	return;
	}
    int i, errorArg=-1;
    const char* errorStr = NULL;

    for ( i = 0; i < argc; i++ )
	argv[i] = (char*)tokens[i].c_str();
    argv[argc] = NULL;
    
    i = 0;
    while ( (i < argc) && !errorStr )
	{
	errorStr = ParseCmdLine( argc, argv, i, errorArg );
	}
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::ExtractConfig", "Error extracting configuration from configuration string" )
	    << "Error extracting configuration from configuration string '" << config << "'." << ENDLOG;
	PrintUsage( errorStr, i, errorArg, argc, argv );
	return;
	}
    }

template<class BridgeHeadClass>
inline AliHLTDescriptorHandler* AliHLTControlledBridgeComponent<BridgeHeadClass>::CreateDescriptorHandler()
    {
    return new AliHLTDescriptorHandler( (fMaxPreEventCountExp2>=0 ? fMaxPreEventCountExp2 : 4) );
    }

template<class BridgeHeadClass>
inline AliHLTShmManager* AliHLTControlledBridgeComponent<BridgeHeadClass>::CreateShmManager()
    {
    return new AliHLTShmManager( fMaxNoUseCount );
    }

template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::SetupComponents()
    {
    fBlobCom->SetMsgCommunication( fBlobMsgCom );
    fMsgCom->AddCallback( &fLogCallback );
    fBlobMsgCom->AddCallback( &fLogCallback );
    fBlobCom->AddCallback( &fLogCallback );
    fMsgCom->AddCallback( &fTraceCallback );
    fBlobMsgCom->AddCallback( &fTraceCallback );
    fBlobCom->AddCallback( &fTraceCallback );
    fBlobCom->SetBlobBuffer( fBlobBufferSize );
    fMsgCom->SetReceiveTimeout( fCommunicationTimeout );
    fBlobMsgCom->SetReceiveTimeout( fCommunicationTimeout );
    fBlobCom->SetReceiveTimeout( fCommunicationTimeout );

    fBridgeHead->SetMsgCom( fMsgCom, fRemoteMsgAddress );
    fBridgeHead->SetBlobCom( fBlobCom, fBlobMsgCom, fRemoteBlobMsgAddress );
    fBridgeHead->SetRunNumber( fRunNumber );
    if ( fEventAccounting )
	{
	if ( fShmManager )
	    fEventAccounting->SetShmManager( fShmManager );
	fEventAccounting->SetName( fComponentName );
	fEventAccounting->SetRun( fRunNumber );
	if ( fBridgeHead )
	    fBridgeHead->SetEventAccounting( fEventAccounting );
	}
    }

template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::Bind()
    {
    {
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    fStatus->fProcessStatus.fState |= (kAliHLTPCBoundStateFlag|kAliHLTPCChangingStateFlag);
    }
    int ret;
    ret = fMsgCom->Bind( fOwnMsgAddress );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Bind", "Error Binding Msg Object" )
	    << "Error binding msg object to address '" << fOwnMsgAddressStr.c_str()
	    << "':" << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCNetworkErrorStateFlag;
	fStatus->fProcessStatus.fState&= ~kAliHLTPCChangingStateFlag;
	return;
	}
    ret = fBlobMsgCom->Bind( fOwnBlobMsgAddress );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Bind", "Error Binding BlobMsg Object" )
	    << "Error binding blob msg object to address '" << fOwnMsgAddressStr.c_str()
	    << "':" << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCNetworkErrorStateFlag;
	fStatus->fProcessStatus.fState&= ~kAliHLTPCChangingStateFlag;
	return;
	}
    ret = fBlobCom->Bind( fOwnBlobAddress );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Bind", "Error Binding Blob Object" )
	    << "Error binding blob object to address '" << fOwnMsgAddressStr.c_str()
	    << "':" << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCNetworkErrorStateFlag;
	fStatus->fProcessStatus.fState&= ~kAliHLTPCChangingStateFlag;
	return;
	}
    StartMsgLoop();
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
    }

template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::Unbind()
    {
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Unbind", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    fStatus->fProcessStatus.fState |= kAliHLTPCChangingStateFlag;
    stateLock.Unlock();
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Unbind", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    StopMsgLoop();
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Unbind", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    fMsgCom->Unbind();
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Unbind", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    fBlobCom->Unbind();
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Unbind", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    fBlobMsgCom->Unbind();
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Unbind", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    stateLock.Lock();
    fStatus->fProcessStatus.fState &= ~(kAliHLTPCBoundStateFlag|kAliHLTPCChangingStateFlag|kAliHLTPCNetworkErrorStateFlag);
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Unbind", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    }

template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::Connect()
    {
    {
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    fStatus->fProcessStatus.fState |= (kAliHLTPCChangingStateFlag|kAliHLTPCConnectedStateFlag);
    }
    fBridgeHead->LockCom();
    fBridgeHead->Connect( false );
    fBridgeHead->UnlockCom();
    LOG( AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Connect", "Connection State" )
	<< "fStatus->fProcessStatus.fState&kAliHLTPCChangingStateFlag: " << AliHLTLog::kHex
	<< (fStatus->fProcessStatus.fState&kAliHLTPCChangingStateFlag)
	<< " fStatus->fProcessStatus.fState&kAliHLTPCConnectedStateFlag: "
	<< (fStatus->fProcessStatus.fState&kAliHLTPCConnectedStateFlag)
	<< " fStatus->fProcessStatus.fState&kAliHLTPCNetworkErrorStateFlag: "
	<< (fStatus->fProcessStatus.fState&kAliHLTPCNetworkErrorStateFlag)
	<< "." << ENDLOG;
    //fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
    }

template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::Disconnect( const char* newMsgAddressStr, const char* newBlobMsgAddressStr )
    {
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    int ret=0;
    BCLAbstrAddressStruct *newMsgAddr=NULL, *newBlobMsgAddr=NULL;
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= (kAliHLTPCChangingStateFlag);
	}
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    fBridgeHead->LockCom();
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    fBridgeHead->Disconnect( true, true, 1000, false );
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    if ( newMsgAddressStr )
	{
	ret = BCLDecodeRemoteURL( newMsgAddressStr, newMsgAddr );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Disconnect", "Error in remote msg configuration" )
		<< "Error in remote msg configuration '" << fRemoteMsgAddressStr.c_str() << "': "
		<< strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCNetworkErrorStateFlag;
	    }
	}
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    if ( newBlobMsgAddressStr && !ret )
	{
	ret = BCLDecodeRemoteURL( newBlobMsgAddressStr, newBlobMsgAddr );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Disconnect", "Error in remote blob msg configuration" )
		<< "Error in remote blob msg configuration '" << fRemoteBlobMsgAddressStr.c_str() << "': "
		<< strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCNetworkErrorStateFlag;
		}
	    if ( newMsgAddr )
		{
		BCLFreeAddress( newMsgAddr );
		newMsgAddr = NULL;
		}
	    }
	}
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    if ( !ret )
	{
	if ( newMsgAddr )
	    {
	    BCLAbstrAddressStruct* oldaddr = fRemoteMsgAddress;
	    fBridgeHead->SetRemoteMsgAddress( newMsgAddr );
	    fRemoteMsgAddressStr = newMsgAddressStr;
	    fRemoteMsgAddress = newMsgAddr;
	    BCLFreeAddress( oldaddr );
	    }
	if ( newBlobMsgAddr )
	    {
	    BCLAbstrAddressStruct* oldaddr = fRemoteBlobMsgAddress;
	    fBridgeHead->SetRemoteBlobMsgAddress( newBlobMsgAddr );
	    fRemoteBlobMsgAddressStr = newBlobMsgAddressStr;
	    fRemoteBlobMsgAddress = newBlobMsgAddr;
	    BCLFreeAddress( oldaddr );
	    }
	}
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    fBridgeHead->UnlockCom();
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    fStatus->fProcessStatus.fState &= ~(kAliHLTPCChangingStateFlag|kAliHLTPCConnectedStateFlag);
    if ( !ret )
	fStatus->fProcessStatus.fState &= ~kAliHLTPCNetworkErrorStateFlag;
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Disconnect", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    }

template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::Reconnect( const char* newMsgAddressStr, const char* newBlobMsgAddressStr )
    {
    int ret=0;
    BCLAbstrAddressStruct *newMsgAddr=NULL, *newBlobMsgAddr=NULL;
    fBridgeHead->LockCom();
    fBridgeHead->Disconnect( true, true, 1000, false );
    if ( newMsgAddressStr )
	{
	ret = BCLDecodeRemoteURL( newMsgAddressStr, newMsgAddr );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Disconnect", "Error in remote msg configuration" )
		<< "Error in remote msg configuration '" << fRemoteMsgAddressStr.c_str() << "': "
		<< strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCNetworkErrorStateFlag;
	    }
	}
    if ( newBlobMsgAddressStr && !ret )
	{
	ret = BCLDecodeRemoteURL( newBlobMsgAddressStr, newBlobMsgAddr );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent<BridgeHeadClass>::Disconnect", "Error in remote blob msg configuration" )
		<< "Error in remote blob msg configuration '" << fRemoteBlobMsgAddressStr.c_str() << "': "
		<< strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCNetworkErrorStateFlag;
		}
	    if ( newMsgAddr )
		{
		BCLFreeAddress( newMsgAddr );
		newMsgAddr = NULL;
		}
	    }
	}
    if ( !ret )
	{
	if ( newMsgAddr )
	    {
	    BCLAbstrAddressStruct* oldaddr = fRemoteMsgAddress;
	    fBridgeHead->SetRemoteMsgAddress( newMsgAddr );
	    fRemoteMsgAddressStr = newMsgAddressStr;
	    fRemoteMsgAddress = newMsgAddr;
	    BCLFreeAddress( oldaddr );
	    }
	if ( newBlobMsgAddr )
	    {
	    BCLAbstrAddressStruct* oldaddr = fRemoteBlobMsgAddress;
	    fBridgeHead->SetRemoteBlobMsgAddress( newBlobMsgAddr );
	    fRemoteBlobMsgAddressStr = newBlobMsgAddressStr;
	    fRemoteBlobMsgAddress = newBlobMsgAddr;
	    BCLFreeAddress( oldaddr );
	    }
	}
    fBridgeHead->Connect( false );
    fBridgeHead->UnlockCom();
    }


template<class BridgeHeadClass>
inline AliHLTControlledBridgeComponent<BridgeHeadClass>::BridgeHeadErrorCallback::BridgeHeadErrorCallback( TSelf* comp ):
    fComp( comp )
    {
    }

template<class BridgeHeadClass>
inline AliHLTControlledBridgeComponent<BridgeHeadClass>::BridgeHeadErrorCallback::~BridgeHeadErrorCallback()
    {
    }

template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::BridgeHeadErrorCallback::ConnectionError( BridgeHeadClass* bridgeHead )
    {
    //printf( "----BridgeHeadErrorCallback::ConnectionError----\n" );
    LOG( AliHLTLog::kWarning, "AliHLTControlledBridgeComponent<BridgeHeadClass>::BridgeHeadErrorCallback::ConnectionError", "Connection Error" )
	<< "Connection Error in BridgeHead. Pausing processing." << ENDLOG;
    if ( fComp->fStatus )
	{
	MLUCMutex::TLocker stateLock( fComp->fStatus->fStateMutex );
	fComp->fStatus->fProcessStatus.fState |= kAliHLTPCNetworkErrorStateFlag;
	}
    if ( bridgeHead )
	{
	bridgeHead->Pause();
#if 0
	bridgeHead->LockCom();
	bridgeHead->Disconnect( true, true, 1000, false );
	bridgeHead->Connect( false );
	bridgeHead->UnlockCom();
#endif
	}
    }

template<class BridgeHeadClass>
inline void AliHLTControlledBridgeComponent<BridgeHeadClass>::BridgeHeadErrorCallback::ConnectionEstablished( BridgeHeadClass* bridgeHead )
    {
    //printf( "----BridgeHeadErrorCallback::ConnectionEstablished----\n" );
    if ( fComp->fStatus )
	{
	MLUCMutex::TLocker stateLock( fComp->fStatus->fStateMutex );
	fComp->fStatus->fProcessStatus.fState &= ~kAliHLTPCNetworkErrorStateFlag;
	fComp->fStatus->fProcessStatus.fState |= kAliHLTPCConnectedStateFlag;
	}
    if ( bridgeHead )
	{
	bridgeHead->Restart();
	}
    }




AliHLTControlledPublisherBridgeHeadComponent::AliHLTControlledPublisherBridgeHeadComponent( const char* compName, int argc, char** argv ):
    AliHLTControlledBridgeComponent<AliHLTPublisherBridgeHead>( AliHLTControlledBridgeComponent<AliHLTPublisherBridgeHead>::kPublisherBridgeHead, compName, argc, argv ),
					  fSubscriberEventCallback( this ),
					  fMsgLoopThread( this, &AliHLTControlledPublisherBridgeHeadComponent::PublisherMsgLoop )
    {
    fSubscriptionListenerThread = NULL;
    fShm = NULL;
    fBlobPtr = NULL;
    fSubscriptionListenerLoopRunning = false;
    fPublisherMsgLoopRunning = false;
    pthread_mutex_init( &fSubscriberRemovalListMutex, NULL );
    ResetConfiguration();
    }

AliHLTControlledPublisherBridgeHeadComponent::~AliHLTControlledPublisherBridgeHeadComponent()
    {
    Unconfigure();
    pthread_mutex_destroy( &fSubscriberRemovalListMutex );
    }

bool AliHLTControlledPublisherBridgeHeadComponent::ExecCmd( AliUInt32_t cmd, bool& quit )
    {
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    switch ( cmd )
	{
	case kAliHLTPCAcceptSubscriptionsCmd:
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) &&
		 !(fStatus->fProcessStatus.fState & (kAliHLTPCChangingStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag)) &&
		 !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		{
		fStatus->fProcessStatus.fState |= (kAliHLTPCChangingStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag);
		stateLock.Unlock();
		AcceptSubscriptions();
		return true;
		}
	    break;
	case kAliHLTPCStopSubscriptionsCmd:
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) &&
		 !(fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) && 
		 !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		{
		fStatus->fProcessStatus.fState |= kAliHLTPCChangingStateFlag;
		stateLock.Unlock();
		StopSubscriptions();
		stateLock.Lock();
		fStatus->fProcessStatus.fState &= ~(kAliHLTPCChangingStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag|kAliHLTPCProducerErrorStateFlag);
		return true;
		}
	    break;
	case kAliHLTPCCancelSubscriptionsCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) &&
		 !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		{
		stateLock.Unlock();
		CancelSubscriptions();
		return true;
		}
	    break;
	case kAliHLTPCPublisherPinEventsCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
		break;
	    if ( fBridgeHead )
		{
		stateLock.Unlock();
		fBridgeHead->PinEvents( true );
		return true;
		}
	    break;
	case kAliHLTPCPublisherUnpinEventsCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
		break;
	    if ( fBridgeHead )
		{
		stateLock.Unlock();
		fBridgeHead->PinEvents( false );
		return true;
		}
	    break;
	case kAliHLTPCRemoveSubscriberCmd: // Fallthrough
	case kAliHLTPCCancelSubscriberCmd:
	    {
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) && 
		 !(fStatus->fProcessStatus.fState & kAliHLTPCWithSubscribersStateFlag) )
		break;
	    stateLock.Unlock();
	    bool sendUnsubscribeMsg;
	    if ( cmd==kAliHLTPCCancelSubscriberCmd )
		sendUnsubscribeMsg = true;
	    else
		sendUnsubscribeMsg = false;
	    MLUCString tmp;
	    pthread_mutex_lock( &fSubscriberRemovalListMutex );
	    while ( fSubscriberRemovalList.size()>0 )
		{
		tmp = *(fSubscriberRemovalList.begin());
		fSubscriberRemovalList.erase( fSubscriberRemovalList.begin() );
		pthread_mutex_unlock( &fSubscriberRemovalListMutex );
		if ( fBridgeHead )
		    fBridgeHead->CancelSubscription( tmp.c_str(), sendUnsubscribeMsg );
		pthread_mutex_lock( &fSubscriberRemovalListMutex );
		}
	    pthread_mutex_unlock( &fSubscriberRemovalListMutex );
	    return true;
	    }
	}
    stateLock.Unlock();
    return TParent::ExecCmd( cmd, quit );
    }

void AliHLTControlledPublisherBridgeHeadComponent::ProcessSCCmd( AliHLTSCCommandStruct* cmdS )
    {
    AliHLTSCCommand cmd( cmdS );
    if ( !fStatus )
	{
	// XXX
	return;
	}
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    switch ( cmd.GetData()->fCmd )
	{
	case kAliHLTPCAcceptSubscriptionsCmd:
	    if ( fStatus->fProcessStatus.fState & ~(kAliHLTPCStartedStateFlag|kAliHLTPCConfiguredStateFlag|kAliHLTPCSubscribedStateFlag|kAliHLTPCBoundStateFlag|kAliHLTPCConnectedStateFlag) )
		break;
	    stateLock.Unlock();
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    return;
	case kAliHLTPCStopSubscriptionsCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) ||
		 (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		break;
	    stateLock.Unlock();
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    return;
	case kAliHLTPCCancelSubscriptionsCmd:
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) ||
		 (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		break;
	    stateLock.Unlock();
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    return;
	case kAliHLTPCPublisherPinEventsCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
		break;
	    stateLock.Unlock();
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    return;
	case kAliHLTPCPublisherUnpinEventsCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
		break;
	    stateLock.Unlock();
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    return;
	case kAliHLTPCRemoveSubscriberCmd:
	    {
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) && 
		 !(fStatus->fProcessStatus.fState & kAliHLTPCWithSubscribersStateFlag) )
		break;
	    stateLock.Unlock();
	    MLUCString tmp;
	    tmp = (char*)cmdS->fData;
	    pthread_mutex_lock( &fSubscriberRemovalListMutex );
	    fSubscriberRemovalList.insert( fSubscriberRemovalList.end(), tmp );
	    pthread_mutex_unlock( &fSubscriberRemovalListMutex );
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    return;
	    }
	}
    stateLock.Unlock();
    TParent::ProcessSCCmd( cmdS );
    }

void AliHLTControlledPublisherBridgeHeadComponent::ExtractConfig( AliHLTSCCommandStruct* cmd )
    {
    TParent::ExtractConfig( cmd );
    }

void AliHLTControlledPublisherBridgeHeadComponent::StartMsgLoop()
    {
    fMsgLoopThread.Start();    
    }

void AliHLTControlledPublisherBridgeHeadComponent::StopMsgLoop()
    {
    StopPublisherMsgLoop();
    }

void AliHLTControlledPublisherBridgeHeadComponent::StartProcessing()
    {
    fBridgeHead->Start();
    }

void AliHLTControlledPublisherBridgeHeadComponent::PauseProcessing()
    {
    fBridgeHead->Pause();
    }

void AliHLTControlledPublisherBridgeHeadComponent::ResumeProcessing()
    {
    fBridgeHead->Restart();
    }

void AliHLTControlledPublisherBridgeHeadComponent::StopProcessing()
    {
    fBridgeHead->Stop();
    }



bool AliHLTControlledPublisherBridgeHeadComponent::CheckConfiguration()
    {
    if ( fOwnPublisherName=="" )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledPublisherBridgeHeadComponent::CheckConfiguration", "No publisher specified" )
	    << "No publishers specified (e.g. using the -publisher command line option)." << ENDLOG;
	return false;
	}
    return TParent::CheckConfiguration();
    }

void AliHLTControlledPublisherBridgeHeadComponent::ShowConfiguration()
    {
    TParent::ShowConfiguration();
    LOG( AliHLTLog::kInformational, "AliHLTControlledPublisherBridgeHeadComponent::ShowConfiguration", "Own Publisher config" )
	<< "Own publisher name: '" << fOwnPublisherName.c_str() << "'." << ENDLOG;
    }

void AliHLTControlledPublisherBridgeHeadComponent::ResetConfiguration()
    {
    TParent::ResetConfiguration();
    fSubscriberCount=0;
    fPublisherTimeout = 0;
    fOwnPublisherName = "";
    fShmKey.fShmType = kInvalidShmType;
    fShmKey.fKey.fID = ~(AliUInt64_t)0;
    fShmID = (AliUInt32_t)-1;
    fShmAddCopy = false;
    }

void AliHLTControlledPublisherBridgeHeadComponent::Configure()
    {
    TParent::Configure();

    if ( fAliceHLT )
	fTriggerStackMachine = new AliHLTTriggerStackMachine;

    fBridgeHead = CreatePublisher();
    if ( !fBridgeHead )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::Configure", "Unable to Create Publisher Object" )
	    << "Unable to create publisher object '"
	    << fOwnPublisherName.c_str() << "'." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	return;
	}
    fSubscriptionListenerThread = CreateSubscriptionListenerThread();
    if ( !fSubscriptionListenerThread )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::Configure", "Unable to Create Subscription Listener Thread" )
	    << "Unable to create subscription listener thread." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	return;
	}
    fShm = CreateShm();
    if ( !fShm )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::Configure", "Unable to Create Shared Memory Object" )
	    << "Unable to create shared memory object." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	return;
	}
    }

void AliHLTControlledPublisherBridgeHeadComponent::Unconfigure()
    {
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledPublisherBridgeHeadComponent::Unconfigure", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    if ( fBridgeHead )
	fBridgeHead->CancelAllSubscriptions( true, true );
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledPublisherBridgeHeadComponent::Unconfigure", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;

    if ( fShm )
	{
	if ( fShmID != (AliUInt32_t)-1 )
	    {
	    fShm->UnlockShm( fShmID );
	    fShm->ReleaseShm( fShmID );
	    }
	delete fShm;
	fShm = NULL;
	}
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledPublisherBridgeHeadComponent::Unconfigure", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    if ( fSubscriptionListenerThread )
	{
	delete fSubscriptionListenerThread;
	fSubscriptionListenerThread = NULL;
	}
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledPublisherBridgeHeadComponent::Unconfigure", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    if ( fBridgeHead )
	{
	delete fBridgeHead;
	fBridgeHead = NULL;
	}

    if ( fTriggerStackMachine )
	{
	delete fTriggerStackMachine;
	fTriggerStackMachine = NULL;
	}
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledPublisherBridgeHeadComponent::Unconfigure", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;

    TParent::Unconfigure();
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledPublisherBridgeHeadComponent::Unconfigure", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    }

const char* AliHLTControlledPublisherBridgeHeadComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cpErr;

    if ( !strcmp( argv[i], "-publishertimeout" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing publisher timeout specifier.";
	    }
	fPublisherTimeout = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting publisher timeout specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventmodulopredivisor" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing event modulo pre-divisor specifier.";
	    }
	fEventModuloPreDivisor = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting event modulo pre-divisor specifier.";
	    }
	if ( !fEventModuloPreDivisor )
	    {
	    errorArg = i+1;
	    return "Event modulo pre-divisor specifier must be non-zero.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-shm" ) )
	{
	if ( argc <= i+2 )
	    {
	    return "Missing shm key specifier.";
	    }
	if ( !strcmp( argv[i+1], "bigphys" ) )
	    fShmKey.fShmType = kBigPhysShmType;
	else if ( !strcmp( argv[i+1], "physmem" ) )
	    fShmKey.fShmType = kPhysMemShmType;
	else if ( !strcmp( argv[i+1], "sysv" ) )
	    fShmKey.fShmType = kSysVShmType;
	else
	    {
	    errorArg = i+1;
	    return "Invalid shm type specifier...";
	    }
	fShmKey.fKey.fID = strtoul( argv[i+2], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+2;
	    return "Error converting shm key specifier..";
	    }
	i += 3;
	return NULL;
	}
    if ( !strcmp( argv[i], "-publisher" ) )
	{
	if ( argc < i +1 )
	    {
	    return "Missing publishername specifier.";
	    }
	fOwnPublisherName = argv[i+1];
	i += 2;
	return NULL;
	}
    return TParent::ParseCmdLine( argc, argv, i, errorArg );
    }

void AliHLTControlledPublisherBridgeHeadComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam, int argc, char** argv )
    {
    TParent::PrintUsage( errorStr, errorArg, errorParam, argc, argv );
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-publisher <publisher-id>: Specifies the name/id to use for the publisher object. (Mandatory)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-publishertimeout <timeout-ms>: Specify the timeout (in milliesconds) that the publisher uses. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-shm [bigphys|physmem|sysv] <shmkey>: Specifies a 32 bit shm key for the used shared memory segment. (Mandatory)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-eventmodulopredivisor <event modulo pre-divisor>: Specify a number by which the event ID/number will be divided before the event modulo is calculated ( (event-ID-or-Number / pre-divisor) % event-modulo). (Optional, default 1)" << ENDLOG;
    }

void AliHLTControlledPublisherBridgeHeadComponent::AcceptSubscriptions()
    {
    if ( fSubscriptionListenerThread )
	fSubscriptionListenerThread->Start();
    else
	{
	LOG( AliHLTLog::kError, "AliHLTControlledPublisherBridgeHeadComponent::AcceptSubscriptions", "No subscription listen thread" )
	    << "No subscription listen thread found." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	}
    }

void AliHLTControlledPublisherBridgeHeadComponent::StopSubscriptions()
    {
    if ( fSubscriptionListenerThread )
	fSubscriptionListenerThread->Quit();
    }

void AliHLTControlledPublisherBridgeHeadComponent::CancelSubscriptions()
    {
    if ( fBridgeHead )
	fBridgeHead->CancelAllSubscriptions( true );
    }

// void AliHLTControlledPublisherBridgeHeadComponent::SubscriptionLoop();
// bool AliHLTControlledPublisherBridgeHeadComponent::QuitSubscriptionLoop();

void AliHLTControlledPublisherBridgeHeadComponent::SetupComponents()
    {
    TParent::SetupComponents();
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	if ( (fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
	    return;
	}
    if ( fShmKey.fShmType!=kInvalidShmType && fBlobCom->GetComID() == kSCIComID )
	fShmAddCopy = true;

    bool success = false;
    if ( fBlobCom->GetComID() == kSCIComID && !fShmAddCopy )
	{
	fBlobCom->SetBlobBuffer( fBlobBufferSize );
	}
    else
	{
	if ( fShmKey.fShmType != kInvalidShmType )
	    {
	    unsigned long size = fBlobBufferSize;
	    fShmID = fShm->GetShm( fShmKey, size );
	    if ( fShmID != (AliUInt32_t)-1 )
		success = true;
	    }
	else
	    {
	      fShmKey.fShmType = kBigPhysShmType;
	      fShmKey.fKey.fID = 1;
	      unsigned long size = fBlobBufferSize;
	      while ( !success )
		  {
		  fShmID = fShm->GetShm( fShmKey, size, true );
		  if ( fShmID == (AliUInt32_t)-1 )
		    {
		      if ( size==0 )
			size = fBlobBufferSize;
		      else
			break;
		    }
		  else
		    success = true;
		  if ( fShmKey.fKey.fID >= (~(AliUInt64_t)0) - 1 )
		      break;
		  ++fShmKey.fKey.fID;
		}
	    }
	if ( success )
	    {
	    //shm.ReleaseShm( shmID );
	    //shmID = shm.GetShm( shmKey, size );
	    fBlobPtr = (AliUInt8_t*)fShm->LockShm( fShmID );
	    if ( !fBlobPtr )
		{
		LOG( AliHLTLog::kError, "AliHLTControlledPublisherBridgeHeadComponent::SetupComponents", "Error locking blob memory" )
		    << "Error locking blob memory with ID 0x" << AliHLTLog::kHex << fShmKey.fShmType << ":0x" << fShmKey.fKey.fID
		    << "." << ENDLOG;
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		return;
		}
	    if ( fBlobCom->GetComID() != kSCIComID )
		fBlobCom->SetBlobBuffer( fBlobBufferSize, fBlobPtr );
	    else
		fBlobCom->SetBlobBuffer( fBlobBufferSize );
	    }
	else
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledPublisherBridgeHeadComponent::SetupComponents", "Error getting free blob memory" )
		<< "Error getting free blob memory segment. (ID 0x" << AliHLTLog::kHex << fShmKey.fShmType << ":0x" << fShmKey.fKey.fID
		<< ")." << ENDLOG;
	    fShmKey.fShmType = kInvalidShmType;
	    fShmKey.fKey.fID = ~(AliUInt64_t)0;
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return;
	    }
	}
    
    fBridgeHead->SetMaxTimeout( fPublisherTimeout );
    fBridgeHead->SetSubscriptionLoop( &PublisherPipeSubscriptionInputLoop );
    fBridgeHead->AddSubscriberEventCallback( &fSubscriberEventCallback );
    fBridgeHead->SetDescriptorHandler( fDescriptorHandler );
    fBridgeHead->SetShmID( fShmKey );
    fBridgeHead->SetStatusData( fStatus );
    fBridgeHead->SetErrorCallback( &fErrorCallback );
    fBridgeHead->SetLAMProxy( &fLAM );
    fBridgeHead->SetCommunicationTimeout( fCommunicationTimeout );
    fBridgeHead->SetEventModuloPreDivisor( fEventModuloPreDivisor );
    if ( fAliceHLT )
	fBridgeHead->SetEventTriggerTypeCompareFunc( StackMachineCompareEventTrigger, fTriggerStackMachine );
    }


void AliHLTControlledPublisherBridgeHeadComponent::Bind()
    {
    TParent::Bind();

    if ( fBlobCom->GetComID() == kSCIComID && !fShmAddCopy )
	{
	AliUInt8_t* pv;
	void* pp;
	pv = fBlobCom->GetBlobBuffer();
	if ( AliHLTSharedMemory::GetPhysicalAddress( (void*)pv, &pp, NULL ) )
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTControlledPublisherBridgeHeadComponent::Bind", "Memories" )
		<< "Virtual address: 0x" << AliHLTLog::kHex << (unsigned long)pv << " - Physical address: 0x"
		<< (unsigned long)pp << ENDLOG;
	    unsigned long size = fBlobBufferSize;
	    fShmKey.fKey.fID = reinterpret_cast<AliUInt64_t>(pp);
	    fShmKey.fShmType = kPhysMemShmType;
	    fShmID = fShm->GetShm( fShmKey, size );
	    }
	else
	    {
	    fShmKey.fShmType = kInvalidShmType;
	    fShmKey.fKey.fID = ~(AliUInt64_t)0;
	    }
	}

    if ( fShmAddCopy && fBlobCom->GetComID() == kSCIComID )
	{
	fBridgeHead->SetShmCopy( true, fBlobPtr );
	}
    }

AliHLTPublisherBridgeHead* AliHLTControlledPublisherBridgeHeadComponent::CreatePublisher()
    {
    return new AliHLTPublisherBridgeHead( fOwnPublisherName.c_str(), fMaxPreEventCountExp2 );
    }

AliHLTControlledPublisherBridgeHeadComponent::SubscriptionListenerThread* AliHLTControlledPublisherBridgeHeadComponent::CreateSubscriptionListenerThread()
    {
    return new SubscriptionListenerThread( this, fBridgeHead );
    }

AliHLTSharedMemory* AliHLTControlledPublisherBridgeHeadComponent::CreateShm()
    {
    return new AliHLTSharedMemory;
    }

void AliHLTControlledPublisherBridgeHeadComponent::PublisherMsgLoop()
    {
    fPublisherMsgLoopRunning = true;
    fBridgeHead->MsgLoop();
    fPublisherMsgLoopRunning = false;
    }

bool AliHLTControlledPublisherBridgeHeadComponent::StopPublisherMsgLoop()
    {
    fBridgeHead->QuitMsgLoop();
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 2000000;
    gettimeofday( &start, NULL );
    while ( deltaT<timeLimit && fPublisherMsgLoopRunning )
	{
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( fPublisherMsgLoopRunning )
	fMsgLoopThread.Abort();
    fMsgLoopThread.Join();
    return fPublisherMsgLoopRunning;
    }

void AliHLTControlledPublisherBridgeHeadComponent::Subscription( AliHLTSamplePublisher* )
    {
    fSubscriberCount++;
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCWithSubscribersStateFlag;    
	}
    }

void AliHLTControlledPublisherBridgeHeadComponent::Unsubscription( AliHLTSamplePublisher* )
    {
    fSubscriberCount--;
    if ( !fSubscriberCount && fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCWithSubscribersStateFlag;
	}
    }

void AliHLTControlledPublisherBridgeHeadComponent::SubscriptionListenerThread::Run()
    {
    fQuitted = false;
    if ( fPub )
	{
	fComp->fSubscriptionListenerLoopRunning = true;
#if 0
	    {
	    MLUCMutex::TLocker stateLock( fComp->fStatus->fStateMutex );
	    fComp->fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	    }
#endif
#if 0
	    fPub->AcceptSubscriptions( &AcceptingSubscriptionsCallbackFunc, (void*)fStatus );
	
	// XXX Error detection has to be more elaborate...
	    {
	    MLUCMutex::TLocker stateLock( fComp->fStatus->fStateMutex );
	    if ( !(fComp->fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) )
		fComp->fStatus->fProcessStatus.fState |= kAliHLTPCProducerErrorStateFlag;
	    }
#else
	    if ( fPub->AcceptSubscriptions( &AcceptingSubscriptionsCallbackFunc, (void*)(fComp->fStatus) ) )
	    {
	    MLUCMutex::TLocker stateLock( fComp->fStatus->fStateMutex );
	    fComp->fStatus->fProcessStatus.fState |= kAliHLTPCProducerErrorStateFlag;
	    }
#endif
	}
    fQuitted = true;
    }
		




AliHLTControlledSubscriberBridgeHeadComponent::AliHLTControlledSubscriberBridgeHeadComponent( const char* compName, int argc, char** argv ):
    AliHLTControlledBridgeComponent<AliHLTSubscriberBridgeHead>( AliHLTControlledBridgeComponent<AliHLTSubscriberBridgeHead>::kSubscriberBridgeHead, compName, argc, argv ),
				    fMsgLoopThread( this, &AliHLTControlledSubscriberBridgeHeadComponent::SubscriberMsgLoop )
    {
    fPublisherProxy = NULL;
    fSubscriptionThread = NULL;
    fBufferManager = NULL;
    fSubscriptionLoopRunning = false;
    fSubscriberMsgLoopRunning = false;
    ResetConfiguration();
    }

AliHLTControlledSubscriberBridgeHeadComponent::~AliHLTControlledSubscriberBridgeHeadComponent()
    {
    Unconfigure();
    }

bool AliHLTControlledSubscriberBridgeHeadComponent::ExecCmd( AliUInt32_t cmd, bool& quit )
    {
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    switch ( cmd )
	{
	case kAliHLTPCSubscribeCmd:
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) &&
		 !(fStatus->fProcessStatus.fState & (kAliHLTPCChangingStateFlag|kAliHLTPCSubscribedStateFlag)) &&
		 !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		{
		fStatus->fProcessStatus.fState |= (kAliHLTPCChangingStateFlag|kAliHLTPCSubscribedStateFlag);
		stateLock.Unlock();
		Subscribe();
		return true;
		}
	    if ( fStatus->fProcessStatus.fState & (kAliHLTPCChangingStateFlag|kAliHLTPCSubscribedStateFlag) )
		return true;
	    break;
	case kAliHLTPCUnsubscribeCmd:
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) &&
		 !(fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) && 
		 !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		{
		fStatus->fProcessStatus.fState |= kAliHLTPCChangingStateFlag;
		stateLock.Unlock();
		Unsubscribe();
		stateLock.Lock();
		fStatus->fProcessStatus.fState &= ~(kAliHLTPCChangingStateFlag|kAliHLTPCSubscribedStateFlag|kAliHLTPCConsumerErrorStateFlag);
		return true;
		}
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) )
		return true;
	    break;
	}
    stateLock.Unlock();
    return TParent::ExecCmd( cmd, quit );
    }

void AliHLTControlledSubscriberBridgeHeadComponent::ProcessSCCmd( AliHLTSCCommandStruct* cmdS )
    {
    AliHLTSCCommand cmd( cmdS );
    if ( !fStatus )
	{
	// XXX
	return;
	}
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    switch ( cmd.GetData()->fCmd )
	{
	case kAliHLTPCSubscribeCmd:
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag )
		return;
	    if ( fStatus->fProcessStatus.fState & ~(kAliHLTPCStartedStateFlag|kAliHLTPCConfiguredStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag|kAliHLTPCBoundStateFlag|kAliHLTPCConnectedStateFlag) )
		break;
	    stateLock.Unlock();
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    return;
	case kAliHLTPCUnsubscribeCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) )
		return;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) ||
		 (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		break;
	    stateLock.Unlock();
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    return;
	}
    stateLock.Unlock();
    TParent::ProcessSCCmd( cmdS );
    }

void AliHLTControlledSubscriberBridgeHeadComponent::ExtractConfig( AliHLTSCCommandStruct* cmd )
    {
    return TParent::ExtractConfig( cmd );
    }


void AliHLTControlledSubscriberBridgeHeadComponent::StartMsgLoop()
    {
    fMsgLoopThread.Start();    
    }

void AliHLTControlledSubscriberBridgeHeadComponent::StopMsgLoop()
    {
    StopSubscriberMsgLoop();
    }

void AliHLTControlledSubscriberBridgeHeadComponent::StartProcessing()
    {
    fBridgeHead->Start();
//     fMsgLoopThread.Start();    
    }

void AliHLTControlledSubscriberBridgeHeadComponent::PauseProcessing()
    {
    fBridgeHead->Pause();
    }

void AliHLTControlledSubscriberBridgeHeadComponent::ResumeProcessing()
    {
    fBridgeHead->Restart();
    }

void AliHLTControlledSubscriberBridgeHeadComponent::StopProcessing()
    {
//     StopSubscriberMsgLoop();
    fBridgeHead->Stop();
    }

bool AliHLTControlledSubscriberBridgeHeadComponent::CheckConfiguration()
    {
    if ( fMinBlockSize == 0 )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledSubscriberBridgeHeadComponent::CheckConfiguration", "No min. block size specified" )
	    << "Must specify a minimum shm block size. (e.g. using the -minblocksize command line option."
	    << ENDLOG;
	return false;
	}
    if ( fAttachedPublisherName=="" )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledSubscriberBridgeHeadComponent::CheckConfiguration", "No publisher name to attach to specified" )
	    << "Must specify a publisher name to attach to. (e.g. using the -subscribe command line option."
	    << ENDLOG;
	return false;
	}

    if ( fSubscriberID=="" )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledSubscriberBridgeHeadComponent::CheckConfiguration", "No subscriber ID specified" )
	    << "Must specify a subscriber ID. (e.g. using the -subscribe command line option."
	    << ENDLOG;
	return false;
	}
    return true;
    }

void AliHLTControlledSubscriberBridgeHeadComponent::ShowConfiguration()
    {
    LOG( AliHLTLog::kInformational, "AliHLTControlledSubscriberBridgeHeadComponent::ShowConfiguration", "Attached Publisher(Proxy) & Subscriber config" )
	<< "Attached publisher name: " << fAttachedPublisherName.c_str() << "  - subscriber ID: "
	<< fSubscriberID.c_str() << "." << ENDLOG;
    const char* bufferManType;
    switch ( fBufferManagerType )
	{
	case kLargestBlockBufferManager:
	    bufferManType = "LargestBlock";
	    break;
	case kRingBufferManager:
	    bufferManType = "Ring";
	    break;
	default:
	    bufferManType = "Invalid";
	    break;
	}
    LOG( AliHLTLog::kInformational, "AliHLTControlledSubscriberBridgeHeadComponent::ShowConfiguration", "Buffer Manager" )
	<< "Buffer manager: " << bufferManType << ENDLOG;
    LOG( AliHLTLog::kInformational, "AliHLTControlledBridgeComponent::ShowConfiguration", "Event Modulo" )
	<< "Event modulo: " << (fUseSubscriptionEventModulo ? "Yes" : "No") << " - Counter: "
	<< AliHLTLog::kDec << fSubscriptionEventModulo << ENDLOG;
    }

void AliHLTControlledSubscriberBridgeHeadComponent::ResetConfiguration()
    {
    TParent::ResetConfiguration();
    fMinBlockSize = 0;
    fAttachedPublisherName = "";
    fSubscriberID = "";
    
    fSubscriptionRequestOldEvents = true;

    fSubscriberRetryCount = 0;
    fSubscriberRetryTime = 0;

    fRetryTime = 0;
    fBufferManagerType = kLargestBlockBufferManager;
    }

void AliHLTControlledSubscriberBridgeHeadComponent::Configure()
    {
    TParent::Configure();
    fPublisherProxy = CreatePublisherProxy();
    if ( !fPublisherProxy )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::Configure", "Unable to Create Publisher Proxy Object" )
	    << "Unable to create publisher proxy object." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	return;
	}
    fBridgeHead = CreateSubscriber();
    if ( !fBridgeHead )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::Configure", "Unable to Create Subscriber Object" )
	    << "Unable to create subscriber object." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	return;
	}
    fSubscriptionThread = new SubscriptionThread( this, fPublisherProxy, fBridgeHead );
    if ( !fSubscriptionThread )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledComponent::Configure", "Unable to Create Subscription Thread Object" )
	    << "Unable to create subscription thread object." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	return;
	}
    fBufferManager = CreateBufferManager();
    if ( !fBufferManager )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::Configure", "Error creating buffer manager" )
	    << "Error creating Buffer Manager object." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	return;
	}
    if ( fStatus )
	{
	MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	fStatus->fHLTStatus.fFreeOutputBuffer = fStatus->fHLTStatus.fTotalOutputBuffer = fBufferManager->GetTotalBufferSize();
	}

    }

void AliHLTControlledSubscriberBridgeHeadComponent::Unconfigure()
    {
    if ( fBufferManager )
	{
	delete fBufferManager;
	fBufferManager = NULL;
	}
    if ( fSubscriptionThread )
	{
	delete fSubscriptionThread;
	fSubscriptionThread = NULL;
	}
    if ( fBridgeHead )
	{
	delete fBridgeHead;
	fBridgeHead = NULL;
	}
    if ( fPublisherProxy )
	{
	delete fPublisherProxy;
	fPublisherProxy = NULL;
	}
    TParent::Unconfigure();
    }

void AliHLTControlledSubscriberBridgeHeadComponent::SetupComponents()
    {
    TParent::SetupComponents();
    fBridgeHead->SetDescriptorHandler( fDescriptorHandler);
    fBridgeHead->SetBufferManager( fBufferManager );
//     fBridgeHead->SetMsgCom( fMsgCom, fRemoteMsgAddress );
//     fBridgeHead->SetBlobCom( fBlobCom, fBlobMsgCom, fRemoteBlobMsgAddress );
    fBridgeHead->SetLAMProxy( &fLAM );
    fBridgeHead->SetStatusData( fStatus );
    fBridgeHead->SetCommunicationTimeout( fCommunicationTimeout );
    fBridgeHead->SetErrorCallback( &fErrorCallback );
#ifdef USE_PIPE_TIMEOUT
    fPublisherProxy->SetTimeout( 500000 );
#endif
    if ( fEventAccounting )
	fPublisherProxy->SetEventAccounting( fEventAccounting );
    if ( fAliceHLT )
	{
	std::map<MLUCString,MLUCString>::iterator tcgIter = fTriggerClassGroups.begin(), tcgEnd = fTriggerClassGroups.end();
	MLUCRegEx regEx;
	while ( tcgIter != tcgEnd )
	    {
	    regEx.SetPattern( tcgIter->second.c_str(), MLUCRegEx::kPFExtended );
	    AliUInt64_t bits = 0;
	    std::map<MLUCString,unsigned>::iterator tcbiIter = fTriggerClassBitIndices.begin(), tcbiEnd = fTriggerClassBitIndices.end();
	    while ( tcbiIter != tcbiEnd )
		{
		if ( regEx.Search( tcbiIter->first.c_str() ) )
		    bits |= 1 << tcbiIter->second;
		++tcbiIter;
		}
	    fStackMachineAssembler.AddSymbol( tcgIter->first.c_str(), bits );
	    ++tcgIter;
	    }

	std::vector<MLUCString>::iterator codeIter = fEventTypeStackMachineCodesText.begin(), codeEnd = fEventTypeStackMachineCodesText.end();
	MLUCSimpleStackMachineAssembler::TError smaErr;
	while ( codeIter != codeEnd )
	    {
	    smaErr = fStackMachineAssembler.Assemble( *codeIter );
	    //smaErr = ((MLUCSimpleStackMachineAssembler&)fStackMachineAssembler).Assemble( argv[i+1] );
	    if ( !smaErr.Ok() )
		{
		LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::SetupComponents", "Stack Machine Assembler Error" )
		    << "Error returned by trigger stack machine assembler: " << smaErr.Description() << ENDLOG;
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		return;
		}
	    unsigned long codeWordCount = fStackMachineAssembler.GetCodeWordCount();
	    unsigned long totalSize = codeWordCount*sizeof(uint64)+sizeof(AliHLTEventTriggerStruct);
	    AliHLTEventTriggerStruct* code = reinterpret_cast<AliHLTEventTriggerStruct*>( new uint8[ totalSize ] );
	    if ( !code )
		{
		LOG( AliHLTLog::kError, "AliHLTControlledBridgeComponent::SetupComponents", "Out of memory" )
		    << "Out of memory allocating event type code for trigger stack machine." << ENDLOG;
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		return;
		}
	    memcpy( &(code->fDataWords[0]), fStackMachineAssembler.GetCode(), sizeof(uint64)*codeWordCount );
	    code->fHeader.fLength = totalSize;
	    code->fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
	    code->fHeader.fSubType.fID = 0;
	    code->fHeader.fVersion = 1;
	    code->fDataWordCount = (codeWordCount*sizeof(uint64))/sizeof(code->fDataWords[0]);
	    fEventTypeStackMachineCodes.push_back( code );
	    codeIter++;
	    }
	}
    }

AliHLTPublisherProxyInterface* AliHLTControlledSubscriberBridgeHeadComponent::CreatePublisherProxy()
    {
    return new AliHLTPublisherPipeProxy( fAttachedPublisherName.c_str() );
    }

AliHLTSubscriberBridgeHead* AliHLTControlledSubscriberBridgeHeadComponent::CreateSubscriber()
    {
    return new AliHLTSubscriberBridgeHead( fSubscriberID.c_str(), 10, fMaxPreEventCountExp2 );
    }

AliHLTBufferManager* AliHLTControlledSubscriberBridgeHeadComponent::CreateBufferManager()
    {
    switch ( fBufferManagerType )
	{
	case kRingBufferManager:
	    return new AliHLTRingBufferManager( fMinBlockSize );
	case kLargestBlockBufferManager:
	    {
	    AliHLTLargestBlockBufferManager* tmpBM = new AliHLTLargestBlockBufferManager( fMaxPreEventCountExp2 );
	    //tmpBM->SetAdaptBlockFreeUpdate( true );
	    return tmpBM;
	    }
	};
    return NULL;
    }


// void AliHLTControlledSubscriberBridgeHeadComponent::SubscriptionLoop()
//     {
//     }

// bool AliHLTControlledSubscriberBridgeHeadComponent::QuitSubscriptionLoop();

void AliHLTControlledSubscriberBridgeHeadComponent::Subscribe()
    {
	int ret;
	ret = fPublisherProxy->Subscribe( *fBridgeHead );
	if ( ret )
	    {
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCConsumerErrorStateFlag;
		}
	    LOG( AliHLTLog::kError, "AliHLTControlledComponent::Subscribe", "Error creating subscribing" )
		<< "Error subscribing subscriber '" << fBridgeHead->GetName() << "' to publisher '"
		<< fPublisherProxy->GetName() << "': " << strerror(ret) << " ("
		<< MLUCLog::kDec << ret << ")." << ENDLOG;
	    return;
	    }
	fSubscriptionThread->Start();
	if ( fUseSubscriptionEventModulo )
	    {
	    fPublisherProxy->SetEventType( *fBridgeHead, fSubscriptionEventModulo );
	    }
	if ( fAliceHLT && fEventTypeStackMachineCodes.size()>0 )
	    {
	    AliUInt32_t modulo = 1;
	    if ( fUseSubscriptionEventModulo )
		modulo = fSubscriptionEventModulo;
	    fPublisherProxy->SetEventType( *fBridgeHead, fEventTypeStackMachineCodes, modulo );
	    }
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
    }

void AliHLTControlledSubscriberBridgeHeadComponent::Unsubscribe()
    {
    fSubscriptionThread->Quit();
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    gettimeofday( &start, NULL );
    while ( !fSubscriptionThread->HasQuitted() && deltaT<timeLimit )
	{
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( !fSubscriptionThread->HasQuitted() )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::Unsubscribe", "Aborting Publishing Loop" )
	    << "Publishing loop for subscriber '" << fBridgeHead->GetName() << "' and publisher '"
	    << fPublisherProxy->GetName() << "' has to be aborted." << ENDLOG;
	fSubscriptionThread->AbortPublishing();
	}
    gettimeofday( &start, NULL );
    deltaT = 0;
    while ( !fSubscriptionThread->HasQuitted() && deltaT<timeLimit )
	{
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( !fSubscriptionThread->HasQuitted() )
	{
	LOG( AliHLTLog::kWarning, "AliHLTControlledSubscriberBridgeHeadComponent::Unsubscribe", "Aborting Subscription Thread" )
	    << "Subscription thread loop for subscriber '" << fBridgeHead->GetName() << "' and publisher '"
	    << fPublisherProxy->GetName() << "' has to be aborted." << ENDLOG;
	fSubscriptionThread->Abort();
	}
    fSubscriptionThread->Join();
    }

void AliHLTControlledSubscriberBridgeHeadComponent::SubscriberMsgLoop()
    {
    fSubscriberMsgLoopRunning = true;
    fBridgeHead->MsgLoop();
    fSubscriberMsgLoopRunning = false;
    }

bool AliHLTControlledSubscriberBridgeHeadComponent::StopSubscriberMsgLoop()
    {
    fBridgeHead->QuitMsgLoop();
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 2000000;
    gettimeofday( &start, NULL );
    while ( deltaT<timeLimit && fSubscriberMsgLoopRunning )
	{
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( fSubscriberMsgLoopRunning )
	{
	LOG( AliHLTLog::kWarning, "AliHLTControlledSubscriberBridgeHeadComponent::StopSubscriberMsgLoop", "Aborting Subscription Thread" )
	    << "SubscriberMsgLoop has to be aborted." << ENDLOG;
	fMsgLoopThread.Abort();
	}
    fMsgLoopThread.Join();
    return fSubscriberMsgLoopRunning;
    }


const char* AliHLTControlledSubscriberBridgeHeadComponent::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cpErr;
    if ( !strcmp( argv[i], "-subscribe" ) )
	{
	if ( argc <= i+2 )
	    {
	    return "Missing publishername or subscriberid parameter";
	    }
	fAttachedPublisherName = argv[i+1];
	fSubscriberID = argv[i+2];
	i += 3;
	return NULL;
	}
    if ( !strcmp( argv[i], "-subscribeoldevents" ) )
	{
	fSubscriptionRequestOldEvents = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-nosubscribeoldevents" ) )
	{
	fSubscriptionRequestOldEvents = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventmodulo" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing event modulo specifier.";
	    }
	fSubscriptionEventModulo = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting event modulo specifier.";
	    }
	if ( !fSubscriptionEventModulo )
	    {
	    return "Event modulo specifier must not be zero.";
	    }
	fUseSubscriptionEventModulo = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventtypecode" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing event type code.";
	    }
	fEventTypeStackMachineCodesText.push_back( MLUCString(argv[i+1]) );
#if 0
	AliHLTTriggerStackMachineAssembler sma;
	MLUCSimpleStackMachineAssembler::TError smaErr;
	smaErr = sma.Assemble( argv[i+1] );
	//smaErr = ((MLUCSimpleStackMachineAssembler&)sma).Assemble( argv[i+1] );
	if ( !smaErr.Ok() )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledComponent::ParseCmdLine", "Stack Machine Assembler Error" )
		<< "Error returned by trigger stack machine assembler: " << smaErr.Description() << ENDLOG;
	    return "Error translating trigger stack machine assembler code for event type.";
	    }
	unsigned long codeWordCount = sma.GetCodeWordCount();
	unsigned long totalSize = codeWordCount*sizeof(uint64)+sizeof(AliHLTEventTriggerStruct);
	AliHLTEventTriggerStruct* code = reinterpret_cast<AliHLTEventTriggerStruct*>( new uint8[ totalSize ] );
	if ( !code )
	    {
	    return "Out of memory allocating event type code for trigger stack machine.";
	    }
	memcpy( &(code->fDataWords[0]), sma.GetCode(), sizeof(uint64)*codeWordCount );
	code->fHeader.fLength = totalSize;
	code->fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
	code->fHeader.fSubType.fID = 0;
	code->fHeader.fVersion = 1;
	code->fDataWordCount = (codeWordCount*sizeof(uint64))/sizeof(code->fDataWords[0]);

	fEventTypeStackMachineCodes.push_back( code );
#endif
	i += 2;
	return NULL;
	}
//     if ( !strcmp( argv[i], "-shmproxy" ) )
// 	{
// 	if ( argc <= i+3 )
// 	    {
// 	    return "Missing publisher-to-subscriber or subscriber-to-publisher proxy-shmkey or proxy-shmsize parameter";
// 	    }
// 	fProxyShmSize = strtoul( argv[i+1], &cpErr, 0 );
// 	if ( *cpErr )
// 	    {
// 	    return "Error converting proxy shmsize specifier..";
// 	    }
// 	fPub2SubProxyShmKey = (key_t)strtol( argv[i+2], &cpErr, 0 );
// 	if ( *cpErr )
// 	    {
// 	    return "Error converting publisher-to-subscriber proxy-shmkey specifier..";
// 	    }
// 	fSub2PubProxyShmKey = (key_t)strtol( argv[i+3], &cpErr, 0 );
// 	if ( *cpErr )
// 	    {
// 	    return "Error converting subscriber-to-publisher proxy-shmkey specifier..";
// 	    }
// 	i += 4;
// 	return NULL;
    // 	}
    if ( !strcmp( argv[i], "-retrytime" ) )
	{
	if ( argc < i +1 )
	    {
	    return "Missing retry time specifier.";
	    }
	fRetryTime = strtoul( argv[i+1], &cpErr, 0 );
// 	retryTimeSet = true;
	if ( *cpErr )
	    {
	    return "Error converting retry time specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-minblocksize" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing minimum block size specifier.";
	    }
	fMinBlockSize = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr == 'k')
	    fMinBlockSize *= 1024;
	else if ( *cpErr == 'M')
	    fMinBlockSize *= 1024*1024;
	else if ( *cpErr == 'G')
	    fMinBlockSize *= 1024*1024*1024;
	else if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting minimum block size specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-buffermanager" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing buffer manager type specifier.";
	    }
	if ( !strcmp(argv[i+1], "ring" ) )
	    fBufferManagerType = kRingBufferManager;
	else if ( !strcmp(argv[i+1], "largestblock" ) )
	    fBufferManagerType = kLargestBlockBufferManager;
	else
	    {
	    errorArg = i+1;
	    return "Unknown buffer manager type";
	    }
	i += 2;
	return NULL;
	}
    return TParent::ParseCmdLine( argc, argv, i, errorArg );
    }

void AliHLTControlledSubscriberBridgeHeadComponent::PrintUsage( const char* errorStr, int errorArg, int errorParam, int argc, char** argv )
    {
    TParent::PrintUsage( errorStr, errorArg, errorParam, argc, argv );
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-subscribe <publishername> <subscriberid>: Specifies the name of the publisher to subscribe to and the subscriber id to use. (Mandatory)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-retrytime <time-in-milliseconds>: Specifies the time between retries to send an event. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-minblocksize <minimum-output-block-size>: Specify the minimum size to use for output blocks. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-buffermanager [ ring | largestblock ]: Specify the type of buffer manager to use. (Optional, default largestblock)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-subscribeoldevents: Request old events already present in publisher when starting processing. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-nosubscribeoldevents: Do not request old events already present in publisher when starting processing. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-eventmodulo <event-modulo-count>: Request an event modulo count for subscription (receive only every n'th event). (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Bridge Component", "Command line usage" )
	<< "-eventtypecode <trigger-stack-machine-assembler-code>: Specify some assembler code to be executed in the parent publisher for each event to determine whether this subscriber wants to receive the event. (Optionla, only available in ALICE HLT mode)" << ENDLOG;
    }

	
void AliHLTControlledSubscriberBridgeHeadComponent::SubscriptionThread::Run()
    {
    fQuitted = false;
    if ( fProxy && fSubscriber )
	{
	fComp->fSubscriptionLoopRunning = true;
	MLUCMutex::TLocker stateLock( fComp->fStatus->fStateMutex );
	fComp->fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	}
    fProxy->StartPublishing( *fSubscriber, fComp->fSubscriptionRequestOldEvents );
    
    // XXX Error detection has to be more elaborate...
    MLUCMutex::TLocker stateLock( fComp->fStatus->fStateMutex );
    if ( !(fComp->fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) )
	fComp->fStatus->fProcessStatus.fState |= kAliHLTPCConsumerErrorStateFlag;
    fQuitted = true;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
