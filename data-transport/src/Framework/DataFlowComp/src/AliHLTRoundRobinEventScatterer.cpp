/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTRoundRobinEventScatterer.hpp"
#include "AliHLTLog.hpp"
#include <errno.h>


AliHLTRoundRobinEventScatterer::AliHLTRoundRobinEventScatterer()
    {
    fEventCount = 0;
    }

AliHLTRoundRobinEventScatterer::~AliHLTRoundRobinEventScatterer()
    {
    }

int AliHLTRoundRobinEventScatterer::AnnounceEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, const AliHLTEventTriggerStruct& trigger )
    {
    AliHLTEventScattererRePublisher* publisher;
    if ( fPublisherCount <= 0 )
	return ENODEV;

    if ( fEventCount >= fPublisherCount )
	fEventCount = 0;
    //publisher = fPublishers[ fEventCount ];
    publisher = fPublishers[ eventID % fPublisherCount ];
    fEventCount++;

    return publisher->AnnounceEvent( eventID, sbevent, trigger );
    }

int AliHLTRoundRobinEventScatterer::EventCanceled( AliEventID_t eventID )
    {
    int ret, retval;
    vector<AliHLTEventScattererRePublisher*>::iterator iter, end;
    ret = pthread_mutex_lock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTRoundRobinEventScatterer::EventCanceled", "Mutex lock error" )
	    << "Error locking publisher mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}

    iter = fPublishers.begin();
    end = fPublishers.end();
    while ( iter != end )
	{
	if ( (*iter) )
	    {
	    (*iter)->AbortEvent( eventID );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTRoundRobinEventScatterer::EventCanceled", "Publisher send error" )
		    << "Error sending EventCanceled to publisher " << (*iter)->GetName() << ": "
		    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		    << ENDLOG;
		retval = ret;
		}
	    }
	iter++;
	}

    ret = pthread_mutex_unlock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTRoundRobinEventScatterer::EventCanceled", "Mutex unlock error" )
	    << "Error unlocking publisher mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    return 0;
    }


int AliHLTRoundRobinEventScatterer::RequestEventRelease()
    {
    int ret, retval;
    vector<AliHLTEventScattererRePublisher*>::iterator iter, end;
    ret = pthread_mutex_lock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTRoundRobinEventScatterer::RequestEventRelease", "Mutex lock error" )
	    << "Error locking publisher mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}

    iter = fPublishers.begin();
    end = fPublishers.end();
    while ( iter != end )
	{
	if ( (*iter) )
	    {
	    ret = (*iter)->RequestEventRelease();
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTRoundRobinEventScatterer::RequestEventRelease", "Publisher send error" )
		    << "Error sending RequestEventRelease to publisher " << (*iter)->GetName() << ": "
		    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		    << ENDLOG;
		retval = ret;
		}
	    }
	iter++;
	}

    ret = pthread_mutex_unlock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTRoundRobinEventScatterer::RequestEventRelease", "Mutex unlock error" )
	    << "Error unlocking publisher mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    return 0;
    }

int AliHLTRoundRobinEventScatterer::CancelAllSubscriptions()
    {
    int ret, retval;
    vector<AliHLTEventScattererRePublisher*>::iterator iter, end;
    ret = pthread_mutex_lock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTRoundRobinEventScatterer::CancelAllSubscriptions", "Mutex lock error" )
	    << "Error locking publisher mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}

    iter = fPublishers.begin();
    end = fPublishers.end();
    while ( iter != end )
	{
	if ( (*iter) )
	    {
	    ret = (*iter)->CancelAllSubscriptions( true );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTRoundRobinEventScatterer::CancelAllSubscriptions", "Publisher send error" )
		    << "Error sending CancelAllSubscriptions to publisher " << (*iter)->GetName() << ": "
		    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		    << ENDLOG;
		retval = ret;
		}
	    }
	iter++;
	}

    ret = pthread_mutex_unlock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTRoundRobinEventScatterer::CancelAllSubscriptions", "Mutex unlock error" )
	    << "Error unlocking publisher mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    return 0;
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
