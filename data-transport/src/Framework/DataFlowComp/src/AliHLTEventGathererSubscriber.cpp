/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTEventGathererSubscriber.hpp"
#include "AliHLTBaseEventGatherer.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTEventDoneData.hpp"
#include <errno.h>


AliHLTEventGathererSubscriber::AliHLTEventGathererSubscriber( const char* name, AliUInt32_t maxShmNoUseCount, int maxPreSubEventsExp2 ):
    AliHLTDetectorSubscriber( name, maxShmNoUseCount, maxPreSubEventsExp2 )
    {
    fGatherer = NULL;
    }

AliHLTEventGathererSubscriber::~AliHLTEventGathererSubscriber()
    {
    }

// void AliHLTEventGathererSubscriber::EventDone( AliHLTEventID_t eventID )
//     {
//     }

void AliHLTEventGathererSubscriber::SetEventGatherer( AliHLTBaseEventGatherer* merger )
    {
    fGatherer = merger;
    }

int AliHLTEventGathererSubscriber::SubscriptionCanceled( AliHLTPublisherInterface& )
    {
    int ret=0;
    if ( fGatherer )
	fGatherer->SubscriptionCanceled( this );
    else
	ret = ENODEV;
    return ret;
    }


void AliHLTEventGathererSubscriber::ProcessEvent( const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* ets )
    {
    if ( !sedd )
	{
	LOG( AliHLTLog::kError, "AliHLTEventGathererSubscriber::ProcessEvent", "SEDD NULL pointer" )
	    << "Sub-Event Data Descriptor passed is NULL pointer..." << ENDLOG;
	return;
	}
    if ( fGatherer )
	fGatherer->NewEvent( this, sedd, ets );
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventGathererSubscriber::ProcessEvent", "No merger object specified" )
	    << "No merger object specified yet. EventGathererSubscriber '" << GetName() << "' pretending to be done with event..."
	    << ENDLOG;
	AliHLTEventDoneData edd;
	AliHLTMakeEventDoneData( &edd, sedd->fEventID );
	EventDone( &edd );
	}
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
