/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTolerantRoundRobinEventScattererNew.hpp"
#include "AliHLTLog.hpp"


AliHLTTolerantRoundRobinEventScattererNew::AliHLTTolerantRoundRobinEventScattererNew( char const* name, int slotCount ):
    AliHLTEventScattererNew( name, slotCount )
    {
    pthread_mutex_init( &fMutex, NULL );
    }
	
AliHLTTolerantRoundRobinEventScattererNew::~AliHLTTolerantRoundRobinEventScattererNew()
    {
    pthread_mutex_destroy( &fMutex );
    }
	

void AliHLTTolerantRoundRobinEventScattererNew::PublisherAdded( PublisherData* pub )
    {
    pthread_mutex_lock( &fMutex );
    fToleranceHandler.Add();
    fPublisherNdxMap.insert( fPublisherNdxMap.end(), pub->fNdx );
    pthread_mutex_unlock( &fMutex );
    }

void AliHLTTolerantRoundRobinEventScattererNew::PublisherDeleted( PublisherData* pub, bool& )
    {
    unsigned long ndx=0;
    pthread_mutex_lock( &fMutex );
    vector<unsigned long>::iterator iter, end;
    iter = fPublisherNdxMap.begin();
    end = fPublisherNdxMap.end();
    while ( iter != end )
	{
	if ( *iter == pub->fNdx )
	    {
	    fToleranceHandler.Del( ndx );
	    fPublisherNdxMap.erase( iter );
	    break;
	    }
	ndx++;
	iter++;
	}
    pthread_mutex_unlock( &fMutex );
    }

void AliHLTTolerantRoundRobinEventScattererNew::PublisherEnabled( PublisherData* pub )
    {
    unsigned long ndx=0;
    pthread_mutex_lock( &fMutex );
    vector<unsigned long>::iterator iter, end;
    iter = fPublisherNdxMap.begin();
    end = fPublisherNdxMap.end();
    while ( iter != end )
	{
	if ( *iter == pub->fNdx )
	    {
	    fToleranceHandler.Set( ndx, true );
	    break;
	    }
	ndx++;
	iter++;
	}
    pthread_mutex_unlock( &fMutex );
    }

void AliHLTTolerantRoundRobinEventScattererNew::PublisherDisabled( PublisherData* pub, bool& )
    {
    unsigned long ndx=0;
    pthread_mutex_lock( &fMutex );
    vector<unsigned long>::iterator iter, end;
    iter = fPublisherNdxMap.begin();
    end = fPublisherNdxMap.end();
    while ( iter != end )
	{
	if ( *iter == pub->fNdx )
	    {
	    fToleranceHandler.Set( ndx, false );
	    break;
	    }
	ndx++;
	iter++;
	}
    pthread_mutex_unlock( &fMutex );
    }

bool AliHLTTolerantRoundRobinEventScattererNew::GetEventPublisher( EventData* ed, unsigned long& pubNdx )
    {
    unsigned long oNdx, bNdx;
    bool found;
    pubNdx = ~0UL;
    pthread_mutex_lock( &fMutex );
    if ( fEventIDBased )
	found = fToleranceHandler.Get( ed->fEventID.fNr, oNdx, bNdx );
    else
	found = fToleranceHandler.Get( ed->fSeqNr, oNdx, bNdx );
    pthread_mutex_unlock( &fMutex );
    if ( found )
	{
	if ( oNdx!=bNdx )
	    {
	    LOG( AliHLTLog::kInformational, "AliHLTTolerantRoundRobinEventScattererNew::GetEventPublisher", "Dispatching event to alternate target" )
		<< "Dispatching event 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID 
		<< ") to alternate publisher " << bNdx
		<< " instead of default publisher " << oNdx
		<< "." << ENDLOG;
	    }
	else
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTTolerantRoundRobinEventScattererNew::GetEventPublisher", "Dispatching event to default target" )
		<< "Dispatching event 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID 
		<< ") to default publisher " << oNdx
		<< "." << ENDLOG;
	    }
	pubNdx = bNdx;
	}
    return found;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
