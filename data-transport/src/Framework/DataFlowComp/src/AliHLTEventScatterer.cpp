/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTEventScatterer.hpp"
#include "AliHLTEventScattererSubscriber.hpp"
#include "AliHLTLog.hpp"
#include <errno.h>


AliHLTEventScatterer::AliHLTEventScatterer()
    {
    pthread_mutex_init( &fPublisherMutex, NULL );
    fPublisherCount = 0;
    }

AliHLTEventScatterer::~AliHLTEventScatterer()
    {
    pthread_mutex_destroy( &fPublisherMutex );
    }

void AliHLTEventScatterer::AddPublisher( AliHLTEventScattererRePublisher* publisher )
    {
    if ( !publisher )
	return;
    int ret;
    ret = pthread_mutex_lock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventScatterer::AddPublisher", "Mutex lock error" )
	    << "Error locking publisher mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}

    fPublishers.insert( fPublishers.end(), publisher );
    fPublisherCount++;

    PublisherAdded( publisher );

    ret = pthread_mutex_unlock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventScatterer::AddPublisher", "Mutex unlock error" )
	    << "Error unlocking publisher mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    }


int AliHLTEventScatterer::NewEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, const AliHLTEventTriggerStruct& trigger )
    {
    int ret;
    ret = pthread_mutex_lock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventScatterer::NewEvent", "Mutex lock error" )
	    << "Error locking publisher mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}

    LOG( AliHLTLog::kDebug, "AliHLTEventScatterer::NewEvent", "Event Received" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << ") - sedd 0x" << AliHLTLog::kHex << (unsigned long)&sbevent << " received." << ENDLOG;
    

    AnnounceEvent( eventID, sbevent, trigger );
    

    ret = pthread_mutex_unlock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventScatterer::NewEvent", "Mutex unlock error" )
	    << "Error unlocking publisher mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    return 0;
    }



int AliHLTEventScatterer::EventDone( AliHLTEventDoneData* eventDoneData, AliHLTEventScattererRePublisher* publisher )
    {
    if ( !eventDoneData )
	{
	LOG( AliHLTLog::kError, "AliHLTEventScatterer::EventDone", "NULL pointer" )
	    << "Received NULL pointer event done data." << ENDLOG;
	return EFAULT;
	}
    int ret;
    LOG( AliHLTLog::kDebug, "AliHLTEventScatterer::EventDone", "Event done" )
	<< "Event 0x" << AliHLTLog::kHex << eventDoneData->fEventID << " done. " 
	<< "fSubscriber: " << (fSubscriber ? "Ok" : "NULL" )
	<< ENDLOG;
    ret = pthread_mutex_lock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventScatterer::EventDone", "Mutex lock error" )
	    << "Error locking publisher mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    ReleasingEvent( eventDoneData->fEventID, publisher );
    ret = pthread_mutex_unlock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventScatterer::EventDone", "Mutex unlock error" )
	    << "Error unlocking publisher mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    if ( fSubscriber )
	{
	fSubscriber->EventDone( eventDoneData );
	return 0;
	}
    return ENODEV;
    }


void AliHLTEventScatterer::PublisherAdded( AliHLTEventScattererRePublisher* )
    {
    }

void AliHLTEventScatterer::ReleasingEvent( AliEventID_t, AliHLTEventScattererRePublisher* )
    {
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
