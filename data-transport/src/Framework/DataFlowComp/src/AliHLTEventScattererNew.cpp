/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTEventScattererNew.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTEventDoneData.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTShmManager.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "AliHLTEventAccounting.hpp"
#include "AliHLTHLTEventTriggerData.hpp"
#include "AliHLTEventMetaDataOutput.hpp"
#include "AliEventID2MLUCString.hpp"
#include <errno.h>


AliHLTEventScattererNew::AliHLTEventScattererNew( char const* name, int slotCount ):
    fName( name ),
#ifndef USE_MLUCINDEXEDVECTOR
    fEventData( slotCount ), fEventDataCache( (slotCount>=0 ? slotCount : 4) ),
#else
    fEventData( 10, slotCount ), fEventDataCache( (slotCount>=0 ? slotCount : 4) ),
#endif
    fETSCache( (slotCount>0 ? slotCount+3 : 7), true, false ),
    fEDDCache( (slotCount>0 ? slotCount : 4), true, false ),
    fSignal( (slotCount>=0 ? (slotCount>9 ? slotCount-3 : 6) :2) ),
    fProcessingThread( this, &AliHLTEventScattererNew::DoProcessing ),
    fEventAccounting(NULL),
    fEventsPurged(false),
    fSeqNr(0),
    fEventIDBased(true)
    {
    pthread_mutex_init( &fPublisherMutex, NULL );
    fActivePublisherCount = 0;
    fSubscriber = NULL;
    pthread_mutex_init( &fEventMutex, NULL );
    fPaused = false;
    fSignal.Unlock();
    fDescriptorHandler = NULL;
    pthread_mutex_init( &fDescriptorHandlerMutex, NULL );
    fShmManager = NULL;
    
    pthread_mutex_init( &fETSCacheMutex, NULL );

    pthread_mutex_init( &fEDDCacheMutex, NULL );
    AliHLTMakeEventDoneData( &fStaticEDD, AliEventID_t() );
    pthread_mutex_init( &fStaticEDDMutex, NULL );

    fStatus = NULL;
    fQuitProcessing = false;
    fProcessingQuitted = false;
    fRunNumber = 0;
    fAliceHLT = false;
    fStrictSORRequirements = true;
    }
	
AliHLTEventScattererNew::~AliHLTEventScattererNew()
    {
    pthread_mutex_destroy( &fPublisherMutex );
    pthread_mutex_destroy( &fEventMutex );
    pthread_mutex_destroy( &fDescriptorHandlerMutex );
    pthread_mutex_destroy( &fETSCacheMutex );
    pthread_mutex_destroy( &fEDDCacheMutex );
    pthread_mutex_destroy( &fStaticEDDMutex );
    }


void AliHLTEventScattererNew::AddPublisher( TPublisher* publisher, unsigned long ndx )
    {
    PublisherData pd;
    pd.fNdx = ndx;
    pd.fPublisher = publisher;
    pd.fEnabled = true;
    vector<PublisherData>::iterator iter;
    pthread_mutex_lock( &fPublisherMutex );
    iter = fPublishers.insert( fPublishers.end(), pd );
    fActivePublisherCount++;
    PublisherAdded( &(*iter) );
#ifdef USE_PUBLISHERMAP
    if ( ndx>= fPublisherMap.size() )
	{
	PublisherData pde;
	pde.fNdx = (unsigned long)-1;
	pde.fPublisher = NULL;
	pde.fEnabled = false;
	unsigned long oldSz = fPublisherMap.size();
	fPublisherMap.resize( ndx+1 );
	for ( unsigned long n = oldSz; n<ndx+1; n++ )
	    fPublisherMap[n] = pde;
	}
    fPublisherMap[ndx] = pd;
#endif
    pthread_mutex_unlock( &fPublisherMutex );
    if ( fActivePublisherCount==1 )
	fSignal.Signal();
    }

void AliHLTEventScattererNew::DelPublisher( unsigned long pNdx )
    {
    vector<PublisherData>::iterator iter, end;
    bool found = false;
    bool resend = false;
    PublisherData pd;
    pthread_mutex_lock( &fPublisherMutex );
    iter = fPublishers.begin();
    end = fPublishers.end();
    while ( iter != end )
	{
	if ( iter->fNdx == pNdx )
	    {
	    found = true;
	    pd = *iter;
	    if ( iter->fEnabled )
		fActivePublisherCount--;
	    fPublishers.erase( iter );
#ifdef USE_PUBLISHERMAP
	    fPublisherMap[pNdx].fNdx = (unsigned long)-1;
	    fPublisherMap[pNdx].fPublisher = NULL;
	    fPublisherMap[pNdx].fEnabled = false;
#endif
	    pd.fEnabled = false;
	    break;
	    }
	iter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    if ( found )
	{
	PublisherDeleted( &pd, resend );
	if ( resend )
	    {
	    CancelAllEvents( pNdx );
	    ResendEvents( pNdx );
	    }
	}
    }

void AliHLTEventScattererNew::EnablePublisher( unsigned long ndx )
    {
    vector<PublisherData>::iterator iter, end;
    bool found = false;
    pthread_mutex_lock( &fPublisherMutex );
    iter = fPublishers.begin();
    end = fPublishers.end();
    while ( iter != end )
	{
	if ( iter->fNdx == ndx )
	    {
	    if ( !iter->fEnabled )
		{
		iter->fEnabled = true;
		found = true;
		fActivePublisherCount++;
		}
	    break;
	    }
	iter++;
	}
    PublisherEnabled( &(*iter) );
    pthread_mutex_unlock( &fPublisherMutex );
    if ( fActivePublisherCount==1 )
	fSignal.Signal();
    }

void AliHLTEventScattererNew::DisablePublisher( unsigned long pNdx )
    {
    vector<PublisherData>::iterator iter, end;
    bool found = false;
    bool resend = false;
    pthread_mutex_lock( &fPublisherMutex );
    iter = fPublishers.begin();
    end = fPublishers.end();
    while ( iter != end )
	{
	if ( iter->fNdx == pNdx )
	    {
 	    if ( iter->fEnabled )
		{
		found = true;
		fActivePublisherCount--;
		iter->fEnabled = false;
		}
	    break;
	    }
	iter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    if ( found )
	{
	PublisherDisabled( &(*iter), resend );
	if ( resend )
	    {
	    CancelAllEvents( pNdx );
	    ResendEvents( pNdx );
	    }
	}
    }

void AliHLTEventScattererNew::SetSubscriber( TSubscriber* sub )
    {
    fSubscriber = sub;
    }

void AliHLTEventScattererNew::SetOrigPublisher( AliHLTPublisherInterface* pub )
    {
    fOrigPublisher = pub;
    }

void AliHLTEventScattererNew::StartProcessing()
    {
    fQuitProcessing = false;
    fProcessingThread.Start();
    }

void AliHLTEventScattererNew::StopProcessing()
    {
    fQuitProcessing = true;
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    gettimeofday( &start, NULL );
    while ( !fProcessingQuitted && deltaT<timeLimit )
	{
	fSignal.Signal();
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( !fProcessingQuitted )
	fProcessingThread.Abort();
    fProcessingThread.Join();
    }

void AliHLTEventScattererNew::PauseProcessing()
    {
    fPaused = true;
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCPausedStateFlag;
	}
    fSignal.Signal();
    }

void AliHLTEventScattererNew::ResumeProcessing()
    {
    fPaused = false;
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCPausedStateFlag;
	}
    fSignal.Signal();
    }


// This method is called whenever a new wanted 
// event is available.
int AliHLTEventScattererNew::NewEvent( TSubscriber*, unsigned long, 
				       AliHLTPublisherInterface& pubInt, 
				       AliEventID_t eventID, 
				       const AliHLTSubEventDataDescriptor& sbevent,
				       const AliHLTEventTriggerStruct& origets )
    {
    if ( fEventAccounting )
	fEventAccounting->NewEvent( pubInt.GetName(), &sbevent );
    fEventsPurged = false;
    if ( fStatus && eventID.fType!=kAliEventTypeTick && eventID.fType!=kAliEventTypeReconfigure && eventID.fType!=kAliEventTypeDCSUpdate )
	{
	fStatus->fHLTStatus.fReceivedEventCount++;
	fStatus->Touch();
	}
    LOG( AliHLTLog::kDebug, "AliHLTEventScattererNew::NewEvent", "NewEvent" )
	<< AliHLTLog::kHex << "Event scatterer received event 0x" << eventID
	<< " (" << AliHLTLog::kDec << eventID << ") with " << sbevent.fDataBlockCount 
	<< " data blocks." << ENDLOG;
    if ( eventID != sbevent.fEventID )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventScattererNew::NewEvent", "Incoherent Event ID" )
	    << "Msg event ID 0x" << AliHLTLog::kHex << eventID << " ("
	    << AliHLTLog::kDec << eventID << ") unequal sedd eventID: 0x"
	    << AliHLTLog::kHex << sbevent.fEventID << " ("
	    << AliHLTLog::kDec << sbevent.fEventID << ")." << ENDLOG;
	}
    AliHLTEventTriggerStruct* ets;
    pthread_mutex_lock( &fETSCacheMutex );
    ets = (AliHLTEventTriggerStruct*)fETSCache.Get( origets.fHeader.fLength );
    pthread_mutex_unlock( &fETSCacheMutex );
    if ( !ets )
	{
	// XXX
	LOG( AliHLTLog::kError, "AliHLTEventScattererNew::NewEvent", "Out Of Memory" )
	    << "Out of memory allocating event trigger structure of "
	    << AliHLTLog::kDec << origets.fHeader.fLength << " bytes." << ENDLOG;
	return ENOMEM;
	}
    memcpy( ets, &origets, origets.fHeader.fLength );

    AliHLTSubEventDataDescriptor* sedd;
    pthread_mutex_lock( &fDescriptorHandlerMutex );
    sedd = fDescriptorHandler->GetFreeEventDescriptor( eventID, sbevent.fDataBlockCount );
    pthread_mutex_unlock( &fDescriptorHandlerMutex );
    if ( !sedd )
	{
	// XXX
	LOG( AliHLTLog::kError, "AliHLTEventScattererNew::NewEvent", "Out Of Memory" )
	    << "Out of memory allocating sub-event data descriptor of "
	    << AliHLTLog::kDec << sbevent.fHeader.fLength << " bytes ("
	    << sbevent.fDataBlockCount << " blocks)." << ENDLOG;
	pthread_mutex_lock( &fETSCacheMutex );
	fETSCache.Release( (uint8*)ets );
	pthread_mutex_unlock( &fETSCacheMutex );
	return ENOMEM;
	}
    unsigned long length = sbevent.fHeader.fLength;
    if ( length > sedd->fHeader.fLength )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventScattererNew::NewEvent", "Event Descriptor Error" )
	    << "Sub-Event Descriptor mismatch in length and block count." << ENDLOG;
	length = sedd->fHeader.fLength;
	}
    memcpy( sedd, &sbevent, length );

    PEventData ed;
    pthread_mutex_lock( &fEventMutex );
    ed = (PEventData)fEventDataCache.Get();
    if ( !ed )
	{
	LOG( AliHLTLog::kError, "AliHLTEventScattererNew::NewEvent", "Out Of Memory" )
	    << "Out of memory allocating event data structure for event 0x"
	    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << ")."
	    << ENDLOG;
	pthread_mutex_unlock( &fEventMutex );
	pthread_mutex_lock( &fETSCacheMutex );
	fETSCache.Release( (uint8*)ets );
	pthread_mutex_unlock( &fETSCacheMutex );
	pthread_mutex_lock( &fDescriptorHandlerMutex );
	fDescriptorHandler->ReleaseEventDescriptor( eventID );
	pthread_mutex_unlock( &fDescriptorHandlerMutex );
	return ENOMEM;
	}
    ed->fEventID = eventID;
    ed->fBusy = true;
    ed->fETS = ets;
    ed->fSEDD = sedd;
    ed->fEDD = NULL;
    ed->fBroadcastEventDoneForwardSent = false;
#if 0
    if ( (sbevent.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST) )
	ed->fSeqNr = fSeqNr++;
    else
	ed->fSeqNr = ~0ULL;
#else
    ed->fSeqNr = fSeqNr++;
#endif
#ifndef USE_MLUCINDEXEDVECTOR
    fEventData.Add( ed );
#else
    fEventData.Add( ed, eventID );
#endif
#ifdef MLUCINDEXVECTOR_CROSSCHECK
    fEventIDs.push_back( eventID );
    DoCrossCheck( "AliHLTEventScattererNew::NewEvent", __FILE__, __LINE__ );
#endif
    pthread_mutex_unlock( &fEventMutex );
    fSignal.AddNotificationData( ed );
    fSignal.Signal();
    LOG( AliHLTLog::kDebug, "AliHLTEventScattererNew::NewEvent", "Event Signalled" )
	<< "Event 0x"
	<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec 
	<< eventID << ") signalled."
	<< ENDLOG;
    return 0;
    }
	
// This method is called for a transient subscriber, 
// when an event has been removed from the list. 
int AliHLTEventScattererNew::EventCanceled( TSubscriber*, unsigned long, 
					    AliHLTPublisherInterface&, 
					    AliEventID_t eventID )
    {
    unsigned long evNdx;
    unsigned long pNdx=~0UL;
    bool doCancel = false;
    PEventData ed;
    pthread_mutex_lock( &fEventMutex );
#ifndef USE_MLUCINDEXEDVECTOR
    if ( MLUCVectorSearcher<PEventData,AliEventID_t>::FindElement( fEventData, FindEventDataByID, eventID, evNdx ) )
#else
    if ( fEventData.FindElement( eventID, evNdx ) )
#endif
	{
	ed = fEventData.Get( evNdx );
	ed->fCanceled = true;
	if ( !ed->fBusy )
	    {
	    pNdx = ed->fPublisherNdx;
	    doCancel = true;
	    ReleaseEvent( evNdx );
	    }
	}
    pthread_mutex_unlock( &fEventMutex );
    if ( doCancel )
	CancelEvent( pNdx, eventID );
    return 0;
    }
	
// This method is called for a subscriber when new event done data is 
// received by a publisher (sent from another subscriber) for an event. 
int AliHLTEventScattererNew::EventDoneData( TSubscriber*, unsigned long, 
					    AliHLTPublisherInterface&, 
					    const AliHLTEventDoneData& )
    {
    return 0;
    }

// This method is called when the subscription ends.
int AliHLTEventScattererNew::SubscriptionCanceled( TSubscriber*, unsigned long, 
						   AliHLTPublisherInterface& )
    {
    return 0;
    }
	
// This method is called when the publishing 
// service runs out of buffer space. The subscriber 
// has to free as many events as possible and send an 
// EventDone message with their IDs to the publisher
int AliHLTEventScattererNew::ReleaseEventsRequest( TSubscriber*, unsigned long, 
						   AliHLTPublisherInterface& )
    {
    vector<PublisherData>::iterator iter, end;
    pthread_mutex_lock( &fPublisherMutex );
    iter = fPublishers.begin();
    end = fPublishers.end();
    while ( iter != end )
	{
	if ( iter->fEnabled )
	    iter->fPublisher->RequestEventRelease();
	iter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    return 0;
    }

// Method used to check alive status of subscriber. 
// This message has to be answered with 
// PingAck message within less  than a second.
int AliHLTEventScattererNew::Ping( TSubscriber* sub, unsigned long, 
				   AliHLTPublisherInterface& publisher )
    {
    publisher.PingAck( *sub );
    return 0;
    }
	
// Method used by a subscriber in response to a Ping 
// received message. The PingAck message has to be 
// called within less than one second.
int AliHLTEventScattererNew::PingAck( TSubscriber*, unsigned long, 
				      AliHLTPublisherInterface& )
    {
    return 0;
    }


// empty callback. Will be called when an
//event has already been internally canceled. 
void AliHLTEventScattererNew::CanceledEvent( TPublisher* pubInt, unsigned long pubNdx,
					     AliEventID_t eventID, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    if ( fEventAccounting )
	fEventAccounting->EventFinished( pubInt->GetName(), eventID );
    unsigned long len;
    AliHLTEventDoneData* edd=NULL;
    len = AliHLTGetMergedEventDoneDataSize( eventDoneData );
    if ( len>sizeof(AliHLTEventDoneData) )
	{
	pthread_mutex_lock( &fEDDCacheMutex );
	edd = (AliHLTEventDoneData*)fEDDCache.Get( len );
	pthread_mutex_unlock( &fEDDCacheMutex );
	if ( !edd )
	    {
	    LOG( AliHLTLog::kError, "AliHLTEventScattererNew::CanceledEvent", "Out of memory" )
		<< "Out of memory trying to allocate event done data of"
		<< AliHLTLog::kDec << len << " bytes." << ENDLOG;
	    // XXX Retry??
	    }
	else
	    AliHLTMergeEventDoneData( eventID, edd, eventDoneData );
	}
//     if ( !edd )
// 	{
// 	fStaticEDD.fEventID = eventID;
// 	edd = &fStaticEDD;
// 	}
    unsigned long evNdx;
    PEventData ed;
    bool found = false;
    bool done = false;
    pthread_mutex_lock( &fEventMutex );
#ifndef USE_MLUCINDEXEDVECTOR
    if ( MLUCVectorSearcher<PEventData,AliEventID_t>::FindElement( fEventData, FindEventDataByID, eventID, evNdx ) )
#else
    if ( fEventData.FindElement( eventID, evNdx ) )
#endif
	{
	found = true;
	ed = fEventData.Get( evNdx );
	if ( !ed->fEDD )
	    ed->fEDD = edd;
#define ONLY_ONE_EDD
#ifdef ONLY_ONE_EDD
	else
	    {
	    fEDDCache.Release( (uint8*)edd );
	    edd = ed->fEDD;
	    }
#else
	else if ( ed->fEDD && edd )
	    {
	    vector<AliHLTEventDoneData*> tmpEDDV;
	    tmpEDDV.push_back( ed->fEDD );
	    tmpEDDV.push_back( edd );
	    len = AliHLTGetMergedEventDoneDataSize( tmpEDDV );
	    if ( len>sizeof(AliHLTEventDoneData) )
		{
		pthread_mutex_lock( &fEDDCacheMutex );
		AliHLTEventDoneData* tmpEDD = (AliHLTEventDoneData*)fEDDCache.Get( len );
		pthread_mutex_unlock( &fEDDCacheMutex );
		if ( !tmpEDD )
		    {
		    LOG( AliHLTLog::kError, "AliHLTEventScattererNew::CanceledEvent", "Out of memory" )
			<< "Out of memory trying to allocate event done data of"
			<< AliHLTLog::kDec << len << " bytes." << ENDLOG;
		    pthread_mutex_lock( &fEDDCacheMutex );
		    fEDDCache.Release( (uint8*)edd );
		    pthread_mutex_unlock( &fEDDCacheMutex );
		    edd = ed->fEDD;
		    // XXX Retry??
		    }
		else
		    {
		    AliHLTMergeEventDoneData( eventID, tmpEDD, tmpEDDV );
		    pthread_mutex_lock( &fEDDCacheMutex );
		    fEDDCache.Release( (uint8*)ed->fEDD );
		    fEDDCache.Release( (uint8*)edd );
		    pthread_mutex_unlock( &fEDDCacheMutex );
		    edd = ed->fEDD = tmpEDD;
		    }
		}
	    else
		{
		pthread_mutex_lock( &fEDDCacheMutex );
		fEDDCache.Release( (uint8*)edd );
		pthread_mutex_unlock( &fEDDCacheMutex );
		edd = ed->fEDD;
		}
	    }
#endif
	if ( !ed )
	    {
	    MLUCString str;
#ifdef USE_MLUCINDEXEDVECTOR
	    fEventData.AsString( str );
#endif
	    LOG( AliHLTLog::kFatal, "AliHLTEventScattererNew::CanceledEvent", "Internal Error" )
		<< "Internal Error: ed==0x" << AliHLTLog::kHex << (unsigned long)ed << ". fEventData dump: \n" << str.c_str() << "\n." << ENDLOG;
	    }
	if ( ed && ed->fEventID != eventID )
	    {
	    MLUCString str;
#ifdef USE_MLUCINDEXEDVECTOR
	    fEventData.AsString( str );
#endif
	    LOG( AliHLTLog::kFatal, "AliHLTEventScattererNew::CanceledEvent", "Internal Error" )
		<< "Internal Error: ed==0x" << AliHLTLog::kHex << (unsigned long)ed << " - eventID: 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") ed->fEventID: 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID
		<< "). fEventData dump: \n" << str.c_str() << "\n." << ENDLOG;
	    }
	if ( ed->fSEDD && (ed->fSEDD->fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST) )
	    {
	    bool foundPub=false;
	    vector<unsigned long>::iterator iter, end;
	    iter = ed->fBroadcastReceivedPublishers.begin();
	    end = ed->fBroadcastReceivedPublishers.end();
	    while ( iter != end )
		{
		if ( *iter == pubNdx )
		    {
		    foundPub=true;
		    break;
		    }
		iter++;
		}
	    if ( !foundPub )
		{
		if ( ed->fBroadcastUsedPublishers.size()>0 )
		    {
		    iter = ed->fBroadcastUsedPublishers.begin();
		    end = ed->fBroadcastUsedPublishers.end();
		    while ( iter != end )
			{
			if ( *iter == pubNdx )
			    {
			    foundPub=true;
			    break;
			    }
			iter++;
			}
		    }
		if ( !foundPub )
		    ed->fBroadcastUsedPublishers.push_back( pubNdx );
		ed->fBroadcastReceivedPublishers.push_back( pubNdx );
		}
	    if ( ed->fBroadcastUsedPublishers.size() == ed->fBroadcastReceivedPublishers.size() )
		ed->fDone = true;
	    }
	else
	    ed->fDone = true;
	done = ed->fDone;
	if ( ed->fDone && !ed->fBusy )
	    {
	    if ( fEventAccounting )
		fEventAccounting->EventFinished( fName.c_str(), eventID );
	    ReleaseEvent( evNdx );
	    }
	}
    else if ( !fEventsPurged )
	{
	// XXX
	LOG( AliHLTLog::kError, "AliHLTEventScattererNew::CanceledEvent", "Lost Event" )
	    << "Finished event 0x" << AliHLTLog::kHex << eventID << " ("
	    << AliHLTLog::kDec << eventID << ") lost." << ENDLOG;
	}
    if ( fStatus )
	{
	fStatus->fHLTStatus.fPendingOutputEventCount = fEventData.GetCnt();
	fStatus->Touch();
	}
    pthread_mutex_unlock( &fEventMutex );

    }

// empty callback. Will be called when an
//event has been announced. 
void AliHLTEventScattererNew::AnnouncedEvent( TPublisher*, unsigned long,
					      AliEventID_t, const AliHLTSubEventDataDescriptor&, 
					      const AliHLTEventTriggerStruct&, AliUInt32_t )
    {
    if ( fStatus )
	{
	fStatus->fHLTStatus.fPendingOutputEventCount = fEventData.GetCnt();
	fStatus->Touch();
	}
    }

// empty callback. Will be called when an event done message with non-trivial event done data
// (fDataWordCount>0) has been received for that event. 
// This is called prior to the sending of the EventDoneData to interested other subscribers
// (SendEventDoneData above)
void AliHLTEventScattererNew::EventDoneDataReceived( TPublisher* /*pub*/, unsigned long /*ndx*/,
						     const char* /*subscriberName*/, AliEventID_t eventID,
						     const AliHLTEventDoneData* edd, bool forceForward, bool forwarded )
    {
    if ( fEventAccounting )
	fEventAccounting->EventFinished( fName.c_str(), eventID, forceForward ? 2 : 1, forwarded ? 2 : 1 );
    if ( fOrigPublisher && fSubscriber && forceForward )
	{
	if ( fEventAccounting )
	    fEventAccounting->EventFinished( fOrigPublisher->GetName(), eventID, forceForward ? 2 : 1, 2 );
	bool doSend=false;
	if ( edd && (edd->fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST) )
	    {
	    pthread_mutex_lock( &fEventMutex );
	    unsigned long evNdx;
	    if ( fEventData.FindElement( eventID, evNdx ) )
		{
		PEventData ed;
		ed = fEventData.Get( evNdx );
		if ( ed && !ed->fBroadcastEventDoneForwardSent )
		    doSend = true;
		if ( ed )
		    ed->fBroadcastEventDoneForwardSent = true;
		}
	    pthread_mutex_unlock( &fEventMutex );
	    }
	else
	    doSend = true;
	if ( doSend )
	    fOrigPublisher->EventDone( *fSubscriber, *edd, forceForward, true );
	}
    }

// Empty callback, called when an already announced event has to be announced again to a new
// subscriber that wants to receive old events. This method needs to return the pointers
// to the sub-event data descriptor and the event trigger structure. If the function returns
// false this is a signal that the desired data could not be found anymore. 
bool AliHLTEventScattererNew::GetAnnouncedEventData( TPublisher*, unsigned long,
						     AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
						     AliHLTEventTriggerStruct*& trigger )
    {
    unsigned long evNdx;
    PEventData ed;
    bool found = false;
    pthread_mutex_lock( &fEventMutex );
#ifndef USE_MLUCINDEXEDVECTOR
    if ( MLUCVectorSearcher<PEventData,AliEventID_t>::FindElement( fEventData, FindEventDataByID, eventID, evNdx ) )
#else
    if ( fEventData.FindElement( eventID, evNdx ) )
#endif
	{
	found = true;
	ed = fEventData.Get( evNdx );
	sedd = ed->fSEDD;
	trigger = ed->fETS;
	}
    pthread_mutex_unlock( &fEventMutex );
    return found;
    }

void AliHLTEventScattererNew::PURGEALLEVENTS()
    {
    // fETSCache (ed->fETS) fDescriptorHandler (ed->fSEDD) fEventDataCache fEventData fSignal fEDDCache (ed->fEDD)
    fEventsPurged = true;
    fSignal.Lock();
    while ( fSignal.HaveNotificationData() )
	fSignal.PopNotificationData();
    fSignal.Unlock();

    vector<PEventData> events;
    pthread_mutex_lock( &fEventMutex );
    events.reserve( fEventData.GetCnt() );
    while ( fEventData.GetCnt()>0 )
	{
	events.push_back( fEventData.GetFirst() );
	fEventData.RemoveFirst();
	}
    pthread_mutex_unlock( &fEventMutex );

    
    vector<PEventData>::iterator evtIter, evtEnd;
    evtIter = events.begin();
    evtEnd = events.end();
    while ( evtIter != evtEnd )
	{
	PEventData ed=*evtIter;
	if ( ed )
	    {

	    if ( ed->fETS )
		{
		pthread_mutex_lock( &fETSCacheMutex );
		fETSCache.Release( (uint8*)ed->fETS );
		pthread_mutex_unlock( &fETSCacheMutex );
		}
	    
	    if ( ed->fSEDD )
		{
		pthread_mutex_lock( &fDescriptorHandlerMutex );
		fDescriptorHandler->ReleaseEventDescriptor( ed->fEventID );
		pthread_mutex_unlock( &fDescriptorHandlerMutex );
		}

	    if ( ed->fEDD )
		{
		pthread_mutex_lock( &fEDDCacheMutex );
		fEDDCache.Release( (uint8*)ed->fEDD );
		pthread_mutex_unlock( &fEDDCacheMutex );
		}
	
	    fEventDataCache.Release( ed );
	    }
	evtIter++;
	}


    if ( fStatus )
	{
	pthread_mutex_lock( &fEventMutex );
	fStatus->fHLTStatus.fPendingInputEventCount = fStatus->fHLTStatus.fPendingOutputEventCount = fEventData.GetCnt();
	pthread_mutex_unlock( &fEventMutex );
	fStatus->Touch();
	}

    }


void AliHLTEventScattererNew::DumpEventMetaData()
    {
    int ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	}
    LOG( AliHLTLog::kDebug, "AliHLTEventScattererNew::DumpEventMetaData", "Event meta data dump" )
	<< "Dumping all current events to file." << ENDLOG;
    
    // We need to get the list of all current pending events, fetch their sub event descriptors,
    // dereference the shared memory pointers to all the data blocks and then write the
    // blocks to disk.

    MLUCString prefix;
    prefix = fName;
    prefix += "-Pending";
    AliHLTEventMetaDataOutput metaDataOutputPending( prefix.c_str(), fAliceHLT, fRunNumber );

    if ( fAliceHLT )
	metaDataOutputPending.SetEventTriggerDataFormatFunction(  AliHLTHLEventTriggerData2String );

    ret = metaDataOutputPending.Open();
    if ( ret!=0 )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventScattererNew::DumpEventMetaData", "Error opening event meta data output file" )
	    << "Error opening event meta data output file '" << prefix.c_str() << ": " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return;
	}

    MLUCIndexedVector<PEventData,AliEventID_t>::TIterator eventIter;
    eventIter = fEventData.Begin();
    while ( eventIter )
	{
	PEventData ed=*eventIter;
	if ( ed )
	    metaDataOutputPending.WriteEvent( ed->fEventID, ed->fSEDD, ed->fETS, "pending" );
	++eventIter;
	}

    metaDataOutputPending.Close();


    if (fEventData.GetCnt() == 0)
        {
        LOG( AliHLTLog::kDebug, "AliHLTEventScattererNew::DumpEventMetaData", "Nothing to dump" )
	    << "There were no events found to be dumped to disk" << ENDLOG;
	}


    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	}
    }

void AliHLTEventScattererNew::DoProcessing()
    {
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCRunningStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	}
    fProcessingQuitted = false;
    AliEventID_t eventID;
    unsigned long ndx;
    unsigned long pubNdx;
    PEventData ed=NULL;

    fSignal.Lock();
    while ( !fQuitProcessing )
	{
	LOG( AliHLTLog::kDebug, "AliHLTEventScattererNew::DoProcessing", "Waiting" )
	    << "Waiting for event signal." << ENDLOG;
	if ( !fSignal.HaveNotificationData() || !fActivePublisherCount || fPaused )
	    fSignal.Wait();
	LOG( AliHLTLog::kDebug, "AliHLTEventScattererNew::DoProcessing", "Woken Up" )
	    << "Woken up from event signal." << ENDLOG;
	if ( fQuitProcessing )
	    {
	    fSignal.Unlock();
	    break;
	    }
	if ( fPaused )
	    {
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCPausedStateFlag;
		}
	    continue;
	    }
	else
	    {
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState &= ~kAliHLTPCPausedStateFlag;
		}
	    }
	while ( fSignal.HaveNotificationData() )
	    {
	    ed = fSignal.PeekNotificationData();
 	    if ( !ed )
 		continue;
	    fSignal.Unlock();
	    pthread_mutex_lock( &fEventMutex );
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCBusyStateFlag;
		}
	    eventID = ed->fEventID;
	    if ( ed->fCanceled )
		{
// 		ed->fBusy = false;
		LOG( AliHLTLog::kInformational, "AliHLTEventScattererNew::DoProcessing", "Event canceled" )
		    << "Event 0x" << AliHLTLog::kHex
		    << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID
		    << ") canceled before announcement." << ENDLOG;
#ifndef USE_MLUCINDEXEDVECTOR
		if ( MLUCVectorSearcher<PEventData,AliEventID_t>::FindElement( fEventData, FindEventDataByID, eventID, ndx ) )
#else
		if ( fEventData.FindElement( eventID, ndx ) )
#endif
		    ReleaseEvent( ndx );
		if ( fStatus )
		    {
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
		    }
		pthread_mutex_unlock( &fEventMutex );
		fSignal.Lock();
		fSignal.PopNotificationData();
		continue;
		}
	    if ( ed->fSEDD && (ed->fSEDD->fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST) )
		{
		pthread_mutex_unlock( &fEventMutex );
		LOG( AliHLTLog::kDebug, "AliHLTEventScattererNew::DoProcessing", "Event broadcast" )
		    << AliHLTLog::kHex << "Event scatterer broadcasts event 0x" << eventID
		    << " (" << AliHLTLog::kDec << eventID << ") with " << ed->fSEDD->fDataBlockCount 
		    << " data blocks." << ENDLOG;
		vector<unsigned long> publishers;
		AnnounceEvent( eventID, ed->fSEDD, ed->fETS, publishers );
		if ( fEventAccounting )
		    fEventAccounting->EventForwarded( eventID );
		pthread_mutex_lock( &fEventMutex );
		vector<unsigned long>::iterator pubIter, pubEnd, usedIter, usedEnd;
		pubIter = publishers.begin();
		pubEnd = publishers.end();
		while ( pubIter != pubEnd )
		    {
		    usedIter = ed->fBroadcastUsedPublishers.begin();
		    usedEnd = ed->fBroadcastUsedPublishers.end();
		    bool found=false;
		    while ( usedIter != usedEnd )
			{
			if ( *pubIter == *usedIter )
			    {
			    found=true;
			    break;
			    }
			usedIter++;
			}
		    if ( !found )
			ed->fBroadcastUsedPublishers.push_back( *pubIter );
		    ++pubIter;
		    }
		if ( ed->fBroadcastReceivedPublishers.size()>0 )
		    {
		    vector<unsigned long>::iterator recvIter, recvEnd;
		    recvIter = ed->fBroadcastReceivedPublishers.begin();
		    recvEnd = ed->fBroadcastReceivedPublishers.end();
		    while ( recvIter != recvEnd )
			{
			usedIter = ed->fBroadcastUsedPublishers.begin();
			usedEnd = ed->fBroadcastUsedPublishers.end();
			bool found=false;
			while ( usedIter != usedEnd )
			    {
			    if ( *recvIter == *usedIter )
				{
				found=true;
				break;
				}
			    usedIter++;
			    }
			if ( !found )
			    ed->fBroadcastUsedPublishers.push_back( *recvIter );
			recvIter++;
			}
		    if ( ed->fBroadcastReceivedPublishers.size() == ed->fBroadcastUsedPublishers.size() )
			ed->fDone = true;
		    }
		}
	    else
		{
		if ( GetEventPublisher( ed, pubNdx ) )
		    {
		    ed->fPublisherNdx = pubNdx;
		    }
		else
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTEventScattererNew::DoProcessing", "No publisher available" )
			<< "No publisher available for event 0x" << AliHLTLog::kHex
			<< ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID
			<< "). Deferring for retry." << ENDLOG;
		    if ( fStatus )
			{
			MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
			}
		    pthread_mutex_unlock( &fEventMutex );
		    fSignal.Lock();
		    break;
		    }
		pthread_mutex_unlock( &fEventMutex );
		LOG( AliHLTLog::kDebug, "AliHLTEventScattererNew::DoProcessing", "Event announce" )
		    << AliHLTLog::kHex << "Event scatterer announces event 0x" << eventID
		    << " (" << AliHLTLog::kDec << eventID << ") with " << ed->fSEDD->fDataBlockCount 
		    << " data blocks." << ENDLOG;
		AnnounceEvent( pubNdx, eventID, ed->fSEDD, ed->fETS );
		pthread_mutex_lock( &fEventMutex );
		}
	    bool canceled=false;
	    ed->fBusy = false;
	    canceled = ed->fCanceled;
	    if ( ed->fCanceled || ed->fDone )
		{
		if ( ed->fCanceled )
		    {
		    LOG( AliHLTLog::kInformational, "AliHLTEventScattererNew::DoProcessing", "Event canceled" )
			<< "Event 0x" << AliHLTLog::kHex
			<< ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID
			<< ") canceled after announcement." << ENDLOG;
		    }
		else
		    {
		    LOG( AliHLTLog::kInformational, "AliHLTEventScattererNew::DoProcessing", "Event done" )
			<< "Event 0x" << AliHLTLog::kHex
			<< ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID
			<< ") done directly after announcement." << ENDLOG;
		    }
#ifndef USE_MLUCINDEXEDVECTOR
		if ( MLUCVectorSearcher<PEventData,AliEventID_t>::FindElement( fEventData, FindEventDataByID, eventID, ndx ) )
#else
		if ( fEventData.FindElement( eventID, ndx ) )
#endif
		    ReleaseEvent( ndx );
		}
	    else
		{
		LOG( AliHLTLog::kInformational, "AliHLTEventScattererNew::DoProcessing", "Event announced" )
		    << "Event 0x" << AliHLTLog::kHex
		    << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID
		    << ") announced to publisher " << pubNdx << "." << ENDLOG;
		}
	    pthread_mutex_unlock( &fEventMutex );
	    if ( canceled )
		CancelEvent( pubNdx, eventID );
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
		}
	    fSignal.Lock();
	    fSignal.PopNotificationData();
	    }
	}
		
    fSignal.Unlock();
    fProcessingQuitted = true;

    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCRunningStateFlag;
	if ( !(fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) )
	    fStatus->fProcessStatus.fState |= kAliHLTPCConsumerErrorStateFlag;
	}
    }

void AliHLTEventScattererNew::ResendEvents( unsigned long pNdx )
    {
    unsigned long eNdx;
    PEventData ed;
    MLUCVector<PEventData> events;
    pthread_mutex_lock( &fEventMutex );
#ifndef USE_MLUCINDEXEDVECTOR
    while ( MLUCVectorSearcher<PEventData,unsigned long>::FindElement( fEventData, FindEventDataForPublisher, pNdx, eNdx ) )
#else
    while ( MLUCIndexedVectorSearcher<PEventData,AliEventID_t,unsigned long>::FindElement( fEventData, FindEventDataForPublisher, pNdx, eNdx ) )
#endif
	{
	ed = fEventData.Get( eNdx );
	ed->fBusy = true;
	ed->fPublisherNdx = ~(unsigned long)0;
	events.Add( ed );
	}
    pthread_mutex_unlock( &fEventMutex );

    while ( events.GetSize()>0 )
	{
	ed = events.GetFirst();
	fSignal.AddNotificationData( ed );
	events.RemoveFirst();
	}
    }

// bool AliHLTEventScattererNew::GetEventPublisher( EventData* ed, unsigned long& pubNdx ) = 0; 
	
AliHLTEventScattererNew::TPublisher* AliHLTEventScattererNew::GetPublisher( unsigned long pubNdx )
    {
#ifndef USE_PUBLISHERMAP
    vector<PublisherData>::iterator iter, end;
    iter = fPublishers.begin();
    end = fPublishers.end();
    while ( iter != end )
	{
	if ( iter->fNdx == pubNdx )
	    return iter->fPublisher;
	iter++;
	}
    return NULL;
#else
    if ( pubNdx < fPublisherMap.size() )
	return fPublisherMap[pubNdx].fPublisher;
    else
	return NULL;
#endif
    }

void AliHLTEventScattererNew::AnnounceEvent( AliEventID_t eventID, AliHLTSubEventDataDescriptor* sedd, AliHLTEventTriggerStruct* ets, vector<unsigned long>& publishers )
    {
    int ret;
    LOG( AliHLTLog::kDebug, "AliHLTEventScattererNew::AnnounceEvent", "Event broadcast" )
	<< AliHLTLog::kHex << "Event scatterer broadcasts event 0x" << eventID
	<< " (" << AliHLTLog::kDec << eventID << ") with " << sedd->fDataBlockCount 
	<< " data blocks to all publishers." << ENDLOG;
    vector<PublisherData>::iterator iter, end;
    pthread_mutex_lock( &fPublisherMutex );
    iter = fPublishers.begin();
    end = fPublishers.end();
    unsigned long n=0;
    while ( iter != end )
	{
	if ( iter->fEnabled )
	    {
	    TPublisher* pub = iter->fPublisher;
	    if ( pub )
		{
		ret = pub->AnnounceEvent( eventID, *sedd, *ets );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "AliHLTEventScattererNew::AnnounceEvent", "Error Announcing Event" )
			<< "Error announcing event 0x" << AliHLTLog::kHex
			<< eventID << " (" << AliHLTLog::kDec
			<< eventID << ") to publisher number " << pub->GetName() << " ("
			<< n << "): " << strerror(ret) << " ("
			<< ret << ")." << ENDLOG;
		    }
		else
		    publishers.push_back( iter->fNdx );
		}
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTEventScattererNew::AnnounceEvent", "Error Finding Publisher" )
		    << "Error finding publisher number " << AliHLTLog::kDec
		    << n << "  for event 0x" << AliHLTLog::kHex
		    << eventID << " (" << AliHLTLog::kDec
		    << eventID << ")." << ENDLOG;
		}
	    }
	n++;
	iter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    }

void AliHLTEventScattererNew::AnnounceEvent( unsigned long pubNdx, AliEventID_t eventID, AliHLTSubEventDataDescriptor* sedd, AliHLTEventTriggerStruct* ets )
    {
    int ret;
    LOG( AliHLTLog::kDebug, "AliHLTEventScattererNew::AnnounceEvent", "Event announce" )
	<< AliHLTLog::kHex << "Event scatterer announces event 0x" << eventID
	<< " (" << AliHLTLog::kDec << eventID << ") with " << sedd->fDataBlockCount 
	<< " data blocks to publisher " << AliHLTLog::kDec << pubNdx << "." << ENDLOG;
    if ( eventID != sedd->fEventID )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventScattererNew::NewEvent", "Incoherent Event ID" )
	    << "Msg event ID 0x" << AliHLTLog::kHex << eventID << " ("
	    << AliHLTLog::kDec << eventID << ") unequal sedd eventID: 0x"
	    << AliHLTLog::kHex << sedd->fEventID << " ("
	    << AliHLTLog::kDec << sedd->fEventID << ")." << ENDLOG;
	}
    pthread_mutex_lock( &fPublisherMutex );
    TPublisher* pub;
    pub = GetPublisher( pubNdx );
    if ( pub )
	{
	ret = pub->AnnounceEvent( eventID, *sedd, *ets );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTEventScattererNew::AnnounceEvent", "Error Announcing Event" )
		<< "Error announcing event 0x" << AliHLTLog::kHex
		<< eventID << " (" << AliHLTLog::kDec
		<< eventID << ") to publisher " << pub->GetName() << " ("
		<< pubNdx << "): " << strerror(ret) << " ("
		<< ret << ")." << ENDLOG;
	    }
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTEventScattererNew::AnnounceEvent", "Error Finding Publisher" )
	    << "Error finding publisher with index (" << AliHLTLog::kDec
	    << pubNdx << " for event 0x" << AliHLTLog::kHex
	    << eventID << " (" << AliHLTLog::kDec
	    << eventID << ")." << ENDLOG;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    }

void AliHLTEventScattererNew::CancelEvent( unsigned long pubNdx, AliEventID_t eventID )
    {
    pthread_mutex_lock( &fPublisherMutex );
    TPublisher* pub;
    pub = GetPublisher( pubNdx );
    if ( pub )
	{
	pub->AbortEvent( eventID, true );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTEventScattererNew::CancelEvent", "Error Finding Publisher" )
	    << "Error finding publisher with index (" << AliHLTLog::kDec
	    << pubNdx << " to cancel event 0x" << AliHLTLog::kHex
	    << eventID << " (" << AliHLTLog::kDec
	    << eventID << ")." << ENDLOG;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    }

void AliHLTEventScattererNew::CancelAllEvents( unsigned long pubNdx )
    {
    pthread_mutex_lock( &fPublisherMutex );
    TPublisher* pub;
    pub = GetPublisher( pubNdx );
    if ( pub )
	{
	pub->AbortAllEvents( true );
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTEventScattererNew::CancelAllEvents", "Error Finding Publisher" )
	    << "Error finding publisher with index (" << AliHLTLog::kDec
	    << pubNdx << " to cancel events." << ENDLOG;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    }


void AliHLTEventScattererNew::ReleaseEvent( unsigned long ndx )
    {
    PEventData ed;
    ed = fEventData.Get( ndx );
#ifdef MLUCINDEXVECTOR_CROSSCHECK
	{
	std::vector<AliEventID_t>::iterator evIdIter, evIdEnd;
	evIdIter = fEventIDs.begin();
	evIdEnd = fEventIDs.end();
	while ( evIdIter != evIdEnd )
	    {
	    if ( *evIdIter == ed->fEventID )
		{
		fEventIDs.erase( evIdIter );
		break;
		}
	    evIdIter++;
	    }
	}
#endif

#if 0
    if ( ed->fSEDD && (ed->fSEDD->fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST) )
	{
	MLUCString publStr;
	MLUCString separator;
	std::vector<unsigned long>::iterator pubIter, pubEnd;
	publStr = "Used publishers: ";
	pubIter = ed->fBroadcastUsedPublishers.begin();
	pubEnd = ed->fBroadcastUsedPublishers.end();
	separator = "";
	while ( pubIter!=pubEnd )
	    {
	    char tmpStr[512];
	    snprintf( tmpStr, 512, "%lu", *pubIter );
	    publStr += separator;
	    publStr += tmpStr;
	    separator = ", ";
	    ++pubIter;
	    }
	publStr += "- Received publishers: ";
	pubIter = ed->fBroadcastReceivedPublishers.begin();
	pubEnd = ed->fBroadcastReceivedPublishers.end();
	separator = "";
	while ( pubIter!=pubEnd )
	    {
	    char tmpStr[512];
	    snprintf( tmpStr, 512, "%lu", *pubIter );
	    publStr += separator;
	    publStr += tmpStr;
	    separator = ", ";
	    ++pubIter;
	    }
	LOG( AliHLTLog::kImportant, "AliHLTEventScattererNew::ReleaseEvent", "Release event data" )
	    << "Release of event 0x" << AliHLTLog::kHex << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID << "): "
	    << publStr.c_str() 
	    << " - done: " << ( ed->fDone ? "yes" : "no" ) 
	    << " - canceled: " << ( ed->fCanceled ? "yes" : "no" )
	    << ENDLOG;
	}
#endif

    AliHLTEventTriggerStruct* ets = ed->fETS;
    AliEventID_t eventID = ed->fEventID;
    bool canceled = ed->fCanceled;

    fEventData.Remove( ndx );
#ifdef MLUCINDEXVECTOR_CROSSCHECK
	DoCrossCheck( "AliHLTEventScattererNew::NewEvent", __FILE__, __LINE__ );
#endif
    AliHLTEventDoneData* edd = ed->fEDD;
#if 0
    // Should be doine in ResetCachedObejct of EventDataCache...
    ed->fETS = NULL;
    ed->fEventID = AliEventID_t();
    ed->fSEDD = NULL;
    ed->fEDD = NULL;
#endif
    fEventDataCache.Release( ed );

    pthread_mutex_unlock( &fEventMutex );

    pthread_mutex_lock( &fDescriptorHandlerMutex );
    fDescriptorHandler->ReleaseEventDescriptor( eventID );
    pthread_mutex_unlock( &fDescriptorHandlerMutex );
    
    pthread_mutex_lock( &fETSCacheMutex );
    fETSCache.Release( (uint8*)ets );
    pthread_mutex_unlock( &fETSCacheMutex );

    if ( !canceled )
	{
	if ( edd )
	    fOrigPublisher->EventDone( *fSubscriber, *edd );
	else
	    {
	    pthread_mutex_lock( &fStaticEDDMutex );
	    fStaticEDD.fEventID = eventID;
	    fOrigPublisher->EventDone( *fSubscriber, fStaticEDD );
	    pthread_mutex_unlock( &fStaticEDDMutex );
	    }
	}
    if ( edd )
	{
	pthread_mutex_lock( &fEDDCacheMutex );
	fEDDCache.Release( (uint8*)edd );
	pthread_mutex_unlock( &fEDDCacheMutex );
	}


    pthread_mutex_lock( &fEventMutex );
    }

#ifdef MLUCINDEXVECTOR_CROSSCHECK
void AliHLTEventScattererNew::DoCrossCheck( const char* functionName, const char* file, unsigned long line )
    {
    std::vector<AliEventID_t>::iterator evIdIter, evIdEnd;
    PEventData ed;
    unsigned long evNdx;
    evIdIter = fEventIDs.begin();
    evIdEnd = fEventIDs.end();
    while ( evIdIter != evIdEnd )
	{
	if ( fEventData.FindElement( *evIdIter, evNdx ) )
	    {
	    ed = fEventData.Get( evNdx );
	    if ( !ed )
		{
		MLUCString str;
#ifdef USE_MLUCINDEXEDVECTOR
		fEventData.AsString( str );
#endif
		LOG( AliHLTLog::kFatal, "AliHLTEventScattererNew::DoCrossCheck", "Internal Error" )
		    << "Function " << functionName << " (" << file << "/" << MLUCLog::kDec
		    << line << "): Internal Error: ed==0x" << AliHLTLog::kHex << (unsigned long)ed << ". fEventData dump: \n" << str.c_str() << "\n." << ENDLOG;
		}
	    else if ( ed->fEventID != *evIdIter )
		{
		MLUCString str;
#ifdef USE_MLUCINDEXEDVECTOR
		fEventData.AsString( str );
#endif
		LOG( AliHLTLog::kFatal, "AliHLTEventScattererNew::DoCrossCheck", "Internal Error" )
		    << "Function " << functionName << " (" << file << "/" << MLUCLog::kDec
		    << line << "): Internal Error: ed->fEventID!=0x" << AliHLTLog::kHex << (unsigned long long)(*evIdIter) << ". fEventData dump: \n" << str.c_str() << "\n." << ENDLOG;
		}
	    }
	else
	    {
	    MLUCString str;
#ifdef USE_MLUCINDEXEDVECTOR
	    fEventData.AsString( str );
#endif
	    LOG( AliHLTLog::kFatal, "AliHLTEventScattererNew::DoCrossCheck", "Internal Error" )
		<< "Function " << functionName << " (" << file << "/" << MLUCLog::kDec
		<< line << "): Internal Error: Cannot find event 0x" << AliHLTLog::kHex << (unsigned long long)(*evIdIter) << ". fEventData dump: \n" << str.c_str() << "\n." << ENDLOG;
	    }
	evIdIter++;
	}
    }
#endif


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
