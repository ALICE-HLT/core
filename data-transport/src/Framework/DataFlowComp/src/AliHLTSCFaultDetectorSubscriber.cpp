/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCFaultDetectorSubscriber.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTPublisherInterface.hpp"
#include "AliHLTEventDoneData.hpp"

AliHLTSCFaultDetectorSubscriber::AliHLTSCFaultDetectorSubscriber( const char* name ):
    AliHLTSubscriberInterface( name )
    {
    fLastEventID = AliEventID_t();
    fTimer = NULL;
    fTimeoutID = ~(AliUInt32_t)0;
    fTimerStarted = false;
    fPaused = false;
    fTimeout_us = 0;
    fPublisher = NULL;
    fPublisherNdx = ~(unsigned long)0;
    fStatus = NULL;
    AliHLTMakeEventDoneData( &fDoneData, AliEventID_t() );
    pthread_mutex_init( &fMutex, NULL );
    }

AliHLTSCFaultDetectorSubscriber::~AliHLTSCFaultDetectorSubscriber()
    {
    pthread_mutex_destroy( &fMutex );
    }

void AliHLTSCFaultDetectorSubscriber::SetTimer( AliHLTTimer* timer )
    {
    fTimer = timer;
    }
	
void AliHLTSCFaultDetectorSubscriber::SetEventTimeout( unsigned long timeout_us )
    {
    fTimeout_us = timeout_us;
    }

void AliHLTSCFaultDetectorSubscriber::SetPublisher( AliHLTPublisherInterface* publisher )
    {
    fPublisher = publisher;
    }

void AliHLTSCFaultDetectorSubscriber::SetStatus( AliHLTTDStatus* status )
    {
    fStatus = status;
    }

void AliHLTSCFaultDetectorSubscriber::SetPublisherNdx( unsigned long ndx )
    {
    fPublisherNdx = ndx;
    }

void AliHLTSCFaultDetectorSubscriber::Pause()
    {
    LOG( AliHLTLog::kInformational, "AliHLTSCFaultDetectorSubscriber::Pause", "Pausing" )
	<< "Received Pause Command." << ENDLOG;
    pthread_mutex_lock( &fMutex );
    fPaused = true;
    if ( fTimerStarted )
	{
	fTimer->CancelTimeout( fTimeoutID );
	fTimerStarted = false;
	}
    pthread_mutex_unlock( &fMutex );
    }

void AliHLTSCFaultDetectorSubscriber::Start()
    {
    LOG( AliHLTLog::kInformational, "AliHLTSCFaultDetectorSubscriber::Start", "Starting" )
	<< "Received Start Command." << ENDLOG;
    pthread_mutex_lock( &fMutex );
    if ( fPaused )
	fPaused = false;
    if ( !fTimerStarted )
	{
	fTimeoutID = fTimer->AddTimeout( fTimeout_us/1000, this, 0 );
	fTimerStarted = true;
	}
    pthread_mutex_unlock( &fMutex );
    }

// This method is called whenever a new wanted 
// event is available.
int AliHLTSCFaultDetectorSubscriber::NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
					       const AliHLTSubEventDataDescriptor&,
					       const AliHLTEventTriggerStruct& )
    {
    LOG( AliHLTLog::kInformational, "AliHLTSCFaultDetectorSubscriber::NewEvent", "New Event" )
	<< "New event received from publisher " << publisher.GetName() 
	<< ": 0x" << AliHLTLog::kHex << eventID 
	<< " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
    if ( !fTimer )
	return ENODEV;
    pthread_mutex_lock( &fMutex );
    if ( fStatus )
	{
	fStatus->fToleranceStatus.fState = 1;
	fStatus->fToleranceStatus.fLastEventIDType = eventID.fType;
	fStatus->fToleranceStatus.fLastEventIDNr = eventID.fNr;
	fLastEventID = eventID;
	struct timeval now;
	gettimeofday( &now, NULL );
	fStatus->fProcessStatus.fLastUpdateTime_s = now.tv_sec;
	fStatus->fProcessStatus.fLastUpdateTime_us = now.tv_usec;
	}

    if ( !fPaused )
	{
	if ( fTimerStarted )
	    {
	    fTimer->NewTimeout( fTimeoutID, fTimeout_us/1000 );
	    }
	else
	    {
	    fTimeoutID = fTimer->AddTimeout( fTimeout_us/1000, this, 0 );
	    fTimerStarted = true;
	    }
	}

    pthread_mutex_unlock( &fMutex );
    
    EventReceived( eventID );

    fDoneData.fEventID = eventID;
    publisher.EventDone( *this, fDoneData );
    LOG( AliHLTLog::kInformational, "AliHLTSCFaultDetectorSubscriber::NewEvent", "Event Done" )
	<< "Event 0x" << AliHLTLog::kHex << eventID 
	<< " (" << AliHLTLog::kDec << eventID << ") from publisher " << publisher.GetName() 
	<< " done." << ENDLOG;
    return 0;
    }

// This method is called for a transient subscriber, 
// when an event has been removed from the list. 
int AliHLTSCFaultDetectorSubscriber::EventCanceled( AliHLTPublisherInterface&, AliEventID_t )
    {
    return 0;
    }

// This method is called when the subscription ends.
int AliHLTSCFaultDetectorSubscriber::SubscriptionCanceled( AliHLTPublisherInterface& )
    {
    return 0;
    }

// This method is called when the publishing 
// service runs out of buffer space. The subscriber 
// has to free as many events as possible and send an 
// EventDone message with their IDs to the publisher
int AliHLTSCFaultDetectorSubscriber::ReleaseEventsRequest( AliHLTPublisherInterface& )
    {
    return 0;
    }

// Method used to check alive status of subscriber. 
// This message has to be answered with 
// PingAck message within less  than a second.
int AliHLTSCFaultDetectorSubscriber::Ping( AliHLTPublisherInterface& publisher )
    {
    publisher.PingAck( *this );
    return 0;
    }

// Method used by a subscriber in response to a Ping 
// received message. The PingAck message has to be 
// called within less than one second.
int AliHLTSCFaultDetectorSubscriber::PingAck( AliHLTPublisherInterface& )
    {
    return 0;
    }


void AliHLTSCFaultDetectorSubscriber::TimerExpired( AliUInt64_t, unsigned long )
    {
    const char* eventType = "Unknown";
    switch ( fLastEventID.fType )
	{
	case kAliEventTypeStartOfRun: 
	    eventType = "Start-Of-Run";
	    break;
	case kAliEventTypeData:
	    eventType = "Data";
	    break;
	case kAliEventTypeEndOfRun:
	    eventType = "End-Of-Run";
	    break;
	}
    LOG( AliHLTLog::kInformational, "ToleranceDetectionSubscriber::TimerExpired", "Timer Expired" )
	<< "Timer expired for publisher " << AliHLTLog::kDec << fPublisherNdx 
	<< " with last event 0x" << AliHLTLog::kHex << fLastEventID.fNr 
	<< " (" << AliHLTLog::kDec << fLastEventID.fNr << ") (event type: " 
	<< eventType << ")." << ENDLOG;
    pthread_mutex_lock( &fMutex );
    fTimerStarted = false;
    if ( fStatus )
	{
	fStatus->fToleranceStatus.fState = 0;
	struct timeval now;
	gettimeofday( &now, NULL );
	fStatus->fProcessStatus.fLastUpdateTime_s = now.tv_sec;
	fStatus->fProcessStatus.fLastUpdateTime_us = now.tv_usec;
	pthread_mutex_unlock( &fMutex );
	}

    Timeout();
    }
	


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
