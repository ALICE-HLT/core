/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTEventMerger.hpp"
#include "AliHLTEventMergerSubscriber.hpp"
#include "AliHLTLog.hpp"
#include <string.h>

AliHLTEventMerger::AliHLTEventMerger( int slotCount ):
    fEventDataCache( (slotCount<0) ? 4 : slotCount ),
    fEventData( slotCount ),
    fSignal( slotCount, true ),
    fTimer( slotCount ),
    fTimeoutSignal( slotCount, true )
    {
    pthread_mutex_init( &fSubscriberMutex, NULL );
    pthread_mutex_init( &fEventMutex, NULL );
    fSubscriberCount = 0;
    fSignal.Unlock();
    fDummyTrigger.fHeader.fLength = sizeof(AliHLTEventTriggerStruct);
    fDummyTrigger.fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
    fDummyTrigger.fHeader.fSubType.fID = 0;
    fDummyTrigger.fHeader.fVersion = 1;
    fDummyTrigger.fDataWordCount = 0;
    fTimer.Start();
    fTimeoutSignal.Unlock();
    fQuitTimeout = false;
    fTimeoutQuitted = true;
    fEventTimeout = 0;
    }

AliHLTEventMerger::~AliHLTEventMerger()
    {
    pthread_mutex_destroy( &fSubscriberMutex );
    pthread_mutex_destroy( &fEventMutex );
    fTimer.Stop();
    }


void AliHLTEventMerger::AddSubscriber( AliHLTEventMergerSubscriber* subscriber )
    {
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventMerger::AddSubscriber", "Mutex lock error" )
	    << "Error locking subscriber mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    fSubscribers.insert( fSubscribers.end(), subscriber );
    fSubscriberCount++;
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventMerger::AddSubscriber", "Mutex unlock error" )
	    << "Error unlocking subscriber mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    }

bool AliHLTEventMerger::WaitForEvent( AliEventID_t& eventID, EventData*& eventData )
    {
    fQuitWaiting = false;
    if ( fSubscriberCount <=0 )
	{
	eventData = NULL;
	return false;
	}
    fSignal.Lock();
    do
	{
	while ( !fQuitWaiting && fSignal.HaveNotificationData() )
	    {
	    EventData* tmpED = reinterpret_cast<EventData*>( fSignal.PopNotificationData() );
	    if ( tmpED )
		{
		fSignal.Unlock();
		LOG( AliHLTLog::kDebug, "AliHLTEventMerger::WaitForEvent", "Event available" )
		    << "Event 0x" << AliHLTLog::kHex << tmpED->fEventID << " (" << AliHLTLog::kDec 
		    << tmpED->fEventID << ") available with " << tmpED->fSubscribersPresent 
		    << " of " << tmpED->fSubscribersExpected << " subscribers." << ENDLOG;
		eventData = tmpED;
		eventID = tmpED->fEventID;
		return true;
		}
	    }
	if ( !fQuitWaiting )
	    fSignal.Wait();
	}
    while ( !fQuitWaiting );
    eventData = NULL;
    fSignal.Unlock();
    return false;
    }

void AliHLTEventMerger::StopWaiting()
    {
    fQuitWaiting = true;
    fSignal.Signal();
    }


void AliHLTEventMerger::EventDone( AliHLTEventDoneData* ed )
    {
    if ( !ed )
	{
	LOG( AliHLTLog::kError, "AliHLTEventMerger::EventDone", "NULL pointer" )
	    << "NULL pointer EventDone data passed." << ENDLOG;
	return;
	}
    AliEventID_t eventID = ed->fEventID;
    LOG( AliHLTLog::kDebug, "AliHLTEventMerger::EventDone", "Event done" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
	<< ") finished..." << ENDLOG;
    int ret;
    //vector<EventData*>::iterator eventIter, eventEnd;
    EventData** eventIter;
    bool found = false;
    unsigned long ndx;
    ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventMerger::EventDone", "Mutex lock error" )
	    << "Error locking event mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    if ( fEventData.FindElement( &FindEventDataByID, (uint64)eventID, ndx ) )
	{
	found = true;
	eventIter = fEventData.GetPtr( ndx );
	if ( (*eventIter)->fETS )
	    delete [] (AliUInt8_t*)(*eventIter)->fETS;
	fEventDataCache.Release( *eventIter );
	fEventData.Remove( ndx );
	}
//     eventIter = fEventData.begin();
//     eventEnd = fEventData.end();
//     while ( eventIter != eventEnd )
// 	{
// 	if ( eventIter && (*eventIter)->fEventID == eventID )
// 	    {
// 	    found = true;
// 	    delete (*eventIter);
// 	    fEventData.erase( eventIter );
// 	    break;
// 	    }
// 	eventIter++;
// 	}
    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventMerger::EventDone", "Mutex unlock error" )
	    << "Error unlocking event mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    if ( found )
	{
	vector<AliHLTEventMergerSubscriber*>::iterator subIter, subEnd;
	ret = pthread_mutex_lock( &fSubscriberMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTEventMerger::EventDone", "Mutex lock error" )
		<< "Error locking subscriber mutex: " << strerror(ret)
		<< AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	    }
	subIter = fSubscribers.begin();
	subEnd = fSubscribers.end();
	AliUInt32_t count = 0;
	while ( subIter != subEnd )
	    {
	    if ( (*subIter) )
		{
		(*subIter)->EventDone( ed );
		}
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTEventMerger::EventDone", "No subscriber" )
		    << "Could not send event done for event 0x" << AliHLTLog::kHex << eventID
		    << AliHLTLog::kDec << " (" << eventID << ") to subscriber " << count << ". No pointer..." << ENDLOG;
		}
	    subIter++;
	    count++;
	    }
	ret = pthread_mutex_unlock( &fSubscriberMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTEventMerger::EventDone", "Mutex unlock error" )
		<< "Error unlocking subscriber mutex: " << strerror(ret)
		<< AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	    }
	}
    else
	{
	LOG( AliHLTLog::kError, "AliHLTEventMerger::EventDone", "Event not found" )
	    << "Could not find finshed event 0x" << AliHLTLog::kHex << eventID << " ("
	    << AliHLTLog::kDec << eventID << ") in event list..." << ENDLOG;
	}
    }

void AliHLTEventMerger::NewEvent( AliHLTEventMergerSubscriber* subscriber, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* ets )
    {
    int ret;
    if ( !subscriber )
	{
	LOG( AliHLTLog::kError, "AliHLTEventMerger::NewEvent", "Subscriber NULL pointer" )
	    << "Specified subscriber pointer is NULL pointer..." << ENDLOG;
	return;
	}
    if ( !sedd )
	{
	LOG( AliHLTLog::kError, "AliHLTEventMerger::NewEvent", "SEDD NULL pointer" )
	    << "Specified sub-event data descriptor pointer is NULL pointer..." << ENDLOG;
	return;
	}
    LOG( AliHLTLog::kDebug, "AliHLTEventMerger::NewEvent", "New event received" )
	<< "Event 0x" << AliHLTLog::kHex << sedd->fEventID << " (" << AliHLTLog::kDec
	<< sedd->fEventID << ") received." << ENDLOG;
    //vector<EventData*>::iterator eventIter, eventEnd;
    EventData** eventIter = NULL;
    bool found = false;
    unsigned long ndx;
    ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventMerger::NewEvent", "Mutex lock error" )
	    << "Error locking event mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    if ( fEventData.FindElementReverse( &FindEventDataByID, (uint64)sedd->fEventID, ndx ) )
	{
	eventIter = fEventData.GetPtr( ndx );
	found = true;
	}
//     eventIter = fEventData.begin();
//     eventEnd = fEventData.end();
//     while ( eventIter != eventEnd )
// 	{
// 	if ( eventIter && (*eventIter)->fEventID == sedd->fEventID )
// 	    {
// 	    found = true;
// 	    break;
// 	    }
// 	eventIter++;
// 	}
    if ( found )
	{
	subscriber->ProcessSEDD( sedd, (*eventIter)->fDataBlocks );
	(*eventIter)->fSubscribersPresent++;
	if ( (*eventIter)->fSubscribersPresent >= (*eventIter)->fSubscribersExpected )
	    {
	    LOG( AliHLTLog::kDebug, "AlIL3EventMerger::NewEvent", "Present event ready" )
		<< "Present event 0x" << AliHLTLog::kHex << (*eventIter)->fEventID 
		<< " (" << AliHLTLog::kDec << (*eventIter)->fEventID << " is ready for announcement."
		<< ENDLOG;
 	    fSignal.AddNotificationData( reinterpret_cast<AliUInt64_t>(*eventIter) );
 	    fSignal.Signal();
	    if ( (*eventIter)->fTimerID != ~(AliUInt32_t)0 )
		fTimer.CancelTimeout( (*eventIter)->fTimerID );
	    }
	}
    else
	{
	EventData *ed;
	//ed = new EventData;
	ed = fEventDataCache.Get();
	if ( !ed )
	    {
	    LOG( AliHLTLog::kError, "AliHLTEventMerger::NewEvent", "Out of memory" )
		<< "Out of memory trying to allocate EventData structure." << ENDLOG;
	    ret = pthread_mutex_unlock( &fEventMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTEventMerger::NewEvent", "Mutex unlock error" )
		    << "Error unlocking event mutex: " << strerror(ret)
		    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
		}
	    return;
	    }
	ed->fEventID = sedd->fEventID;
	ed->fSubscribersExpected = fSubscriberCount;
	ed->fSubscribersPresent = 1;
	if ( fEventTimeout > 0 && ed->fSubscribersPresent < ed->fSubscribersExpected)
	    {
	    ret = pthread_mutex_unlock( &fEventMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTEventMerger::NewEvent", "Mutex unlock error" )
		    << "Error unlocking event mutex: " << strerror(ret)
		    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
		}
#warning The following usage of the event ID as a timer callback parameter has to be changed, as the AliEventID_t type is now larger than a 64 bit unsigned.
	    ed->fTimerID = fTimer.AddTimeout( fEventTimeout, &fTimeoutSignal, ed->fEventID );
	    ret = pthread_mutex_lock( &fEventMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTEventMerger::NewEvent", "Mutex lock error" )
		    << "Error locking event mutex: " << strerror(ret)
		    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
		}
	    }
	else
	    ed->fTimerID = ~(AliUInt32_t)0;

	ed->fETS = (AliHLTEventTriggerStruct*)new AliUInt8_t[ ets->fHeader.fLength ];
	if ( !ed->fETS )
	    {
	    LOG( AliHLTLog::kError, "AliHLTEventMerger::NewEvent", "Out of memory" )
		<< "Out of memory trying to allocate event trigger struct data of "
		<< AliHLTLog::kDec << ets->fHeader.fLength << " bytes." << ENDLOG;
	    }
	else
	    memcpy( ed->fETS, ets, ets->fHeader.fLength );
	
	//eventIter = fEventData.insert( eventEnd, ed );
	fEventData.Add( ed, ndx );
	eventIter = fEventData.GetPtr( ndx );
	subscriber->ProcessSEDD( sedd, (*eventIter)->fDataBlocks );
	if ( (*eventIter)->fSubscribersPresent >= (*eventIter)->fSubscribersExpected )
	    {
	    LOG( AliHLTLog::kDebug, "AlIL3EventMerger::NewEvent", "New event ready" )
		<< "New event 0x" << AliHLTLog::kHex << (*eventIter)->fEventID 
		<< " (" << AliHLTLog::kDec << (*eventIter)->fEventID << " is ready for announcement."
		<< ENDLOG;
 	    fSignal.AddNotificationData( reinterpret_cast<AliUInt64_t>(*eventIter) );
 	    fSignal.Signal();
	    }
	}
    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventMerger::NewEvent", "Mutex unlock error" )
	    << "Error unlocking event mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    }


void AliHLTEventMerger::SubscriptionCanceled( AliHLTEventMergerSubscriber* subscriber )
    {
    int ret;
    vector<AliHLTEventMergerSubscriber*>::iterator subIter, subEnd;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventMerger::SubscriptionCanceled", "Mutex lock error" )
	    << "Error locking subscriber mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    while ( subIter != subEnd )
	{
	if ( (*subIter) == subscriber ) 
	    {
	    // returns iterator to following element after erase operation
	    subIter=fSubscribers.erase( subIter );
	    fSubscriberCount--;
	    if ( fSubscriberCount <= 0 )
		{
		fQuitWaiting = true;
		fSignal.Signal();
		}
	    }
	else
	  subIter++;
	}
    
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventMerger::SubscriptionCanceled", "Mutex unlock error" )
	    << "Error unlocking subscriber mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    }


void AliHLTEventMerger::DoTimeout()
    {
    int ret;
    AliEventID_t eventID;
    bool found = false;
    //vector<EventData*>::iterator eventIter, eventEnd;
    EventData** eventIter = NULL;
    unsigned long ndx;
    fTimeoutSignal.Lock();
    fTimeoutQuitted = false;
    while ( !fQuitTimeout )
	{
	while ( fTimeoutSignal.HaveNotificationData() )
	    {
	    eventID = (AliEventID_t)fTimeoutSignal.PopNotificationData();
	    if ( eventID == ~(AliEventID_t)0 )
		continue;
	    fTimeoutSignal.Unlock();
	    
	    ret = pthread_mutex_lock( &fEventMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTEventMerger::DoTimeout", "Mutex lock error" )
		    << "Error locking event mutex: " << strerror(ret)
		    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
		}

	    if ( fEventData.FindElement( &FindEventDataByID, (uint64)eventID, ndx ) )
		{
		found = true;
		eventIter = fEventData.GetPtr( ndx );
		}
// 	    eventIter = fEventData.begin();
// 	    eventEnd = fEventData.end();
// 	    found = false;
// 	    while ( eventIter != eventEnd )
// 		{
// 		if ( eventIter && (*eventIter)->fEventID == eventID )
// 		    {
// 		    found = true;
// 		    break;
// 		    }
// 		eventIter++;
// 		}
	    if ( found )
		{
		LOG( AliHLTLog::kDebug, "AliHLTEventMerger::DoTimeout", "Timeout event ready" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << " ready after timeout." << ENDLOG;
 		fSignal.AddNotificationData( reinterpret_cast<AliUInt64_t>(*eventIter) );
 		fSignal.Signal();
		}
	    else
		{
		LOG( AliHLTLog::kWarning, "AliHLTEventMerger::DoTimeout", "Timeout eventnot found" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << " not found after timeout." << ENDLOG;
		}

	    ret = pthread_mutex_unlock( &fEventMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTEventMerger::DoTimeout", "Mutex unlock error" )
		    << "Error unlocking event mutex: " << strerror(ret)
		    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
		}
	    
	    
	    fTimeoutSignal.Lock();
	    }
	fTimeoutSignal.Wait();
	}
    fTimeoutSignal.Unlock();
    fTimeoutQuitted = true;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
