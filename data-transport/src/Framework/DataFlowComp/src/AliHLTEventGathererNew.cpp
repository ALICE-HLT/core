/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTEventGathererNew.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTEventDoneData.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTShmManager.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "AliHLTEventAccounting.hpp"
#include "AliHLTHLTEventTriggerData.hpp"
#include "AliHLTEventMetaDataOutput.hpp"
#include <errno.h>

AliHLTEventGathererNew::AliHLTEventGathererNew( char const* name, int slotCountExp2 ):
    fName( name ),
    fEventData( 10, slotCountExp2 ),
    fEDDBacklog( 10, (slotCountExp2>0 ? slotCountExp2 : 4), true ),
    fDisabledSubscriberEventData( 10, (slotCountExp2>0 ? slotCountExp2 : 4) ),
    fDisabledSubscriberEDDs( 10, (slotCountExp2>0 ? slotCountExp2 : 4) ),
#if 0
#warning Growing of caches disabled for debugging reasons
    fEventDataCache( (slotCountExp2>0 ? slotCountExp2+1 : 4), false ),
    fETSCache( (slotCountExp2>0 ? slotCountExp2+1 : 4), false, false ),
    fEDDCache( (slotCountExp2>0 ? slotCountExp2+1 : 4), false, false ),
#else
    fEventDataCache( (slotCountExp2>0 ? slotCountExp2 : 4) ),
    fETSCache( (slotCountExp2>0 ? slotCountExp2 : 4), true, false ),
    fEDDCache( (slotCountExp2>0 ? slotCountExp2 : 4), true, false ),
#endif
    fSignal( (slotCountExp2>0 ? (slotCountExp2>9 ? slotCountExp2-3 : 6) :2) ),
    fProcessingThread( this, &AliHLTEventGathererNew::DoProcessing ),
    fEventAccounting(NULL),
    fSORSentDeferBuffer( 3, true ),
    fTimeoutDataCache( 2, true ),
    fTimerCallback( this )
    {
    LOG( AliHLTLog::kDebug, "AliHLTEventGathererNew::AliHLTEventGathererNew", "slotCountExp2" )
	<< "slotCountExp2: " << AliHLTLog::kDec << slotCountExp2 << ENDLOG;
    pthread_mutex_init( &fSubscriberMutex, NULL );
    fPublisher = NULL;
    pthread_mutex_init( &fEventMutex, NULL );
    fPaused = false;
    fSignal.Unlock();
    fDescriptorHandler = NULL;
    pthread_mutex_init( &fDescriptorHandlerMutex, NULL );
    
    pthread_mutex_init( &fETSCacheMutex, NULL );

    pthread_mutex_init( &fEDDCacheMutex, NULL );
    AliHLTMakeEventDoneData( &fStaticEDD, AliEventID_t() );
    pthread_mutex_init( &fStaticEDDMutex, NULL );

    fStatus = NULL;
    fQuitProcessing = false;
    fProcessingQuitted = false;
    fSORSent = false;
    fAliceHLT = false;
    
    fBroadcastTimeout_ms = 0;
    fSORTimeout_ms = 0;

    fEventsPurged = false;

    fRunNumber = 0;

    fStrictSORRequirements = true;
    }

AliHLTEventGathererNew::~AliHLTEventGathererNew()
    {
    pthread_mutex_destroy( &fSubscriberMutex );
    pthread_mutex_destroy( &fEventMutex );
    pthread_mutex_destroy( &fDescriptorHandlerMutex );
    pthread_mutex_destroy( &fETSCacheMutex );
    pthread_mutex_destroy( &fEDDCacheMutex );
    pthread_mutex_destroy( &fStaticEDDMutex );
    }


void AliHLTEventGathererNew::AddSubscriber( TSubscriber* subscriber, AliHLTPublisherInterface* proxy, unsigned long ndx )
    {
    SubscriberData sd;
    sd.fNdx = ndx;
    sd.fSubscriber = subscriber;
    sd.fProxy = proxy;
    sd.fEnabled = true;
    sd.fReceivedSOR = false;
    vector<SubscriberData>::iterator iter;
    pthread_mutex_lock( &fSubscriberMutex );
    iter = fSubscribers.insert( fSubscribers.end(), sd );
    pthread_mutex_unlock( &fSubscriberMutex );
    }

void AliHLTEventGathererNew::DelSubscriber( unsigned long sNdx )
    {
    vector<SubscriberData>::iterator iter, end;
    bool found = false;
    SubscriberData sd;
    pthread_mutex_lock( &fSubscriberMutex );
    iter = fSubscribers.begin();
    end = fSubscribers.end();
    while ( iter != end )
	{
	if ( iter->fNdx == sNdx )
	    {
	    found = true;
	    sd = *iter;
	    fSubscribers.erase( iter );
	    break;
	    }
	iter++;
	}
    pthread_mutex_unlock( &fSubscriberMutex );
    if ( found )
	{
	vector<AliEventID_t> events;
	vector<PEventData> announceEvents;
	unsigned long evNdx;
	PEventData ed;
	pthread_mutex_lock( &fEventMutex );
	while ( MLUCIndexedVectorSearcher<PEventData,AliEventID_t,unsigned long>::FindElement( fEventData, FindActiveEventDataBySubscriber, sNdx, evNdx ) )
	    {
	    ed = fEventData.Get( evNdx );
	    if ( ed->fSEDD->fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST )
		{
		vector<unsigned long>::iterator iter, end;
		iter = ed->fBroadcastExpectedSubscribers.begin();
		end = ed->fBroadcastExpectedSubscribers.end();
		while ( iter != end )
		    {
		    if ( *iter == sNdx )
			{
			ed->fBroadcastExpectedSubscribers.erase( iter );
			break;
			}
		    iter++;
		    }
		iter = ed->fBroadcastReceivedSubscribers.begin();
		end = ed->fBroadcastReceivedSubscribers.end();
		while ( iter != end )
		    {
		    if ( *iter == sNdx )
			{
			ed->fBroadcastReceivedSubscribers.erase( iter );
			break;
			}
		    iter++;
		    }
		if ( ed->fBroadcastReceivedSubscribers.size() == ed->fBroadcastExpectedSubscribers.size() )
		    {
		    int ret = MakeBroadcastEventSEDD( ed );
		    if ( !ret )
			announceEvents.push_back( ed );
		    }
		}
	    else
		{
		ed->fCanceled = true;
		if ( !ed->fBusy )
		    {
		    events.insert( events.end(), ed->fEventID );
		    ReleaseEvent( fEventData, evNdx );
		    }
		}
	    }
	pthread_mutex_unlock( &fEventMutex );
	while ( events.size()>0 )
	    {
	    fPublisher->AbortEvent( *(events.begin()), true );
	    events.erase( events.begin() );
	    }
	bool doSignal=false;
	while ( announceEvents.size()>0 )
	    {
	    fSignal.AddNotificationData( announceEvents[0] );
	    announceEvents.erase( announceEvents.begin() );
	    doSignal = true;
	    }
	if ( doSignal )
	    fSignal.Signal();
	}
    }

void AliHLTEventGathererNew::EnableSubscriber( unsigned long ndx )
    {
    vector<SubscriberData>::iterator iter, end;
    pthread_mutex_lock( &fSubscriberMutex );
    iter = fSubscribers.begin();
    end = fSubscribers.end();
    while ( iter != end )
	{
	if ( iter->fNdx == ndx )
	    {
	    if ( !iter->fEnabled )
		{
		iter->fEnabled = true;
		}
	    break;
	    }
	iter++;
	}
    pthread_mutex_unlock( &fSubscriberMutex );
    }

void AliHLTEventGathererNew::DisableSubscriber( unsigned long sNdx )
    {
    vector<SubscriberData>::iterator iter, end;
    bool found = false;
    pthread_mutex_lock( &fSubscriberMutex );
    iter = fSubscribers.begin();
    end = fSubscribers.end();
    while ( iter != end )
	{
	if ( iter->fNdx == sNdx )
	    {
	    if ( iter->fEnabled )
		{
		iter->fEnabled = false;
		found = true;
		}
	    break;
	    }
	iter++;
	}
    pthread_mutex_unlock( &fSubscriberMutex );
    if ( found )
	{
	unsigned long evNdx;
	PEventData ed;
	EventBacklogData ebd;
	vector<PEventData> announceEvents;
	pthread_mutex_lock( &fEventMutex );
	while ( MLUCIndexedVectorSearcher<PEventData,AliEventID_t,unsigned long>::FindElement( fEventData, FindEventDataBySubscriber, sNdx, evNdx ) )
	    {
	    ed = fEventData.Get( evNdx );
	    if ( ed->fSEDD->fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST )
		{
		vector<unsigned long>::iterator iter, end;
		iter = ed->fBroadcastExpectedSubscribers.begin();
		end = ed->fBroadcastExpectedSubscribers.end();
		while ( iter != end )
		    {
		    if ( *iter == sNdx )
			{
			ed->fBroadcastExpectedSubscribers.erase( iter );
			break;
			}
		    iter++;
		    }
		iter = ed->fBroadcastReceivedSubscribers.begin();
		end = ed->fBroadcastReceivedSubscribers.end();
		while ( iter != end )
		    {
		    if ( *iter == sNdx )
			{
			ed->fBroadcastReceivedSubscribers.erase( iter );
			break;
			}
		    iter++;
		    }
		if ( ed->fBroadcastReceivedSubscribers.size() == ed->fBroadcastExpectedSubscribers.size() )
		    {
		    int ret = MakeBroadcastEventSEDD( ed );
		    if ( !ret )
			announceEvents.push_back( ed );
		    }
		}
	    else
		{
		ed->fSubscriberDisabled = true;
		fDisabledSubscriberEventData.Add( ed, ed->fEventID );
		fEventData.Remove( evNdx );
		}
	    }
	while ( MLUCIndexedVectorSearcher<EventBacklogData,AliEventID_t,unsigned long>::FindElement( fEDDBacklog, FindEDDBacklogBySubscriber, sNdx, evNdx ) )
	    {
	    ebd = fEDDBacklog.Get( evNdx );
	    ebd.fSubscriberDisabled = true;
	    fDisabledSubscriberEDDs.Add( ebd, ebd.fEventID );
	    fEDDBacklog.Remove( evNdx );
	    }
	pthread_mutex_unlock( &fEventMutex );
	bool doSignal=false;
	while ( announceEvents.size()>0 )
	    {
	    fSignal.AddNotificationData( announceEvents[0] );
	    announceEvents.erase( announceEvents.begin() );
	    doSignal = true;
	    }
	if ( doSignal )
	    fSignal.Signal();
	}
    }


void AliHLTEventGathererNew::SetPublisher( TPublisher* pub )
    {
    fPublisher = pub;
    }



void AliHLTEventGathererNew::StartProcessing()
    {
    fQuitProcessing = false;
    fTimer.Start();
    fProcessingThread.Start();
    }

void AliHLTEventGathererNew::StopProcessing()
    {
    fQuitProcessing = true;
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    gettimeofday( &start, NULL );
    while ( !fProcessingQuitted && deltaT<timeLimit )
	{
	fSignal.Signal();
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( !fProcessingQuitted )
	fProcessingThread.Abort();
    fProcessingThread.Join();
    fTimer.Stop();
    }

void AliHLTEventGathererNew::PauseProcessing()
    {
    fPaused = true;
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCPausedStateFlag;
	}
    fSignal.Signal();
    }

void AliHLTEventGathererNew::ResumeProcessing()
    {
    fPaused = false;
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCPausedStateFlag;
	}
    fSignal.Signal();
    }


// This method is called whenever a new wanted 
// event is available.
int AliHLTEventGathererNew::NewEvent( TSubscriber* sub, unsigned long ndx, 
				      AliHLTPublisherInterface& publisher, 
				      AliEventID_t eventID, 
				      const AliHLTSubEventDataDescriptor& sbevent,
				      const AliHLTEventTriggerStruct& origets )
    {
    if ( fEventAccounting )
	fEventAccounting->NewEvent( publisher.GetName(), &sbevent );
    fEventsPurged = false;
    
    LOG( AliHLTLog::kDebug, "AliHLTEventGathererNew::NewEvent", "Event received" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << ") received from subscriber " << ndx
	<< "." << ENDLOG;
    AliHLTEventTriggerStruct* ets=NULL;
#if 0
    pthread_mutex_lock( &fETSCacheMutex );
    ets = (AliHLTEventTriggerStruct*)fETSCache.Get( origets.fHeader.fLength );
    pthread_mutex_unlock( &fETSCacheMutex );
    if ( !ets )
	{
	// XXX
	LOG( AliHLTLog::kError, "AliHLTEventGathererNew::NewEvent", "Out Of Memory" )
	    << "Out of memory allocating event trigger structure of "
	    << AliHLTLog::kDec << origets.fHeader.fLength << " bytes." << ENDLOG;
	return ENOMEM;
	}
#endif

    AliHLTSubEventDataDescriptor* sedd=NULL;
#if 0
    pthread_mutex_lock( &fDescriptorHandlerMutex );
    sedd = fDescriptorHandler->GetFreeEventDescriptor( eventID, sbevent.fDataBlockCount );
    pthread_mutex_unlock( &fDescriptorHandlerMutex );
    if ( !sedd )
	{
	// XXX
	LOG( AliHLTLog::kError, "AliHLTEventGathererNew::NewEvent", "Out Of Memory" )
	    << "Out of memory allocating sub-event data descriptor of "
	    << AliHLTLog::kDec << sbevent.fHeader.fLength << " bytes ("
	    << sbevent.fDataBlockCount << " blocks)." << ENDLOG;
	pthread_mutex_lock( &fETSCacheMutex );
	fETSCache.Release( (uint8*)ets );
	ets = NULL;
	pthread_mutex_unlock( &fETSCacheMutex );
	return ENOMEM;
	}
#endif
    pthread_mutex_lock( &fSubscriberMutex );

    bool flushNoSOR = false;
    vector<unsigned long> expectedParts;
    if (  fAliceHLT && !fSORSent )
	{
	vector<SubscriberData>::iterator iter, end;
	iter = fSubscribers.begin();
	end = fSubscribers.end();
	while ( iter != end )
	    {
	    if ( iter->fNdx==ndx )
		{
		if ( !iter->fReceivedSOR && (eventID.fType==kAliEventTypeData || eventID.fType==kAliEventTypeEndOfRun || eventID.fType==kAliEventTypeSync) )
		    {
		    LOG( (fStrictSORRequirements ? AliHLTLog::kError : AliHLTLog::kWarning), "AliHLTEventGathererNew::NewEvent", "Received Data Event before Start-Of-Run/Data" )
			<< "Data event 0x" << AliHLTLog::kHex << eventID
			<< AliHLTLog::kDec << " (" << eventID << ") received before Start-Of-Run/Data event on input "
			<< ndx << "." << ENDLOG;
		    if ( fStrictSORRequirements && fStatus )
			{
			MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
			}
		    flushNoSOR = !fStrictSORRequirements;
		    }
		if ( eventID.fType==kAliEventTypeStartOfRun )
		    iter->fReceivedSOR = true;
		break;
		}
	    iter++;
	    }
	}
    if (  fAliceHLT && eventID.fType==kAliEventTypeEndOfRun )
	{
	vector<SubscriberData>::iterator iter, end;
	iter = fSubscribers.begin();
	end = fSubscribers.end();
	while ( iter != end )
	    {
	    iter->fReceivedSOR = false;
	    iter++;
	    }
	}
    if ( sbevent.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST )
	{
	vector<SubscriberData>::iterator iter, end;
	iter = fSubscribers.begin();
	end = fSubscribers.end();
	while ( iter != end )
	    {
	    if ( iter->fEnabled || iter->fNdx==ndx ) // We received something from subscriber ndx (which we are currently processing), so we have to ensure it is included, whether enabled or not.
		expectedParts.push_back( iter->fNdx );
	    iter++;
	    }
	}

    pthread_mutex_unlock( &fSubscriberMutex );

    unsigned long evNdx;
    EventBacklogData ebd = { AliEventID_t(), ~0UL, NULL, false };
    PEventData ed;
    bool found=false;
    bool foundEDD=false;
    AliHLTEventDoneData* oldEDD=NULL;
    pthread_mutex_lock( &fEventMutex );
    if ( sbevent.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST )
	{
	// This is a broadcast event, check if parts of this event have already
	// been received from other subscribers
	// (We have to perform merger functionality in this case, e.g. wait for
	// parts of the event from all subscribers)
	if ( fEventData.FindElement( eventID, evNdx ) )
	    {
	    // Yes, we already received some parts
	    ed = fEventData.Get( evNdx );
	    ed->fBroadcastStatusFlags |= sbevent.fStatusFlags;
	    vector<unsigned long>::iterator iter, end;
	    bool expected=false;
	    // Check if this part was expected for this event, add to expected parts if not
	    iter = ed->fBroadcastExpectedSubscribers.begin();
	    end = ed->fBroadcastExpectedSubscribers.end();
	    while ( iter != end )
		{
		if ( *iter == ndx )
		    {
		    expected = true;
		    break;
		    }
		iter++;
		}
	    if ( !expected )
		{
		LOG( AliHLTLog::kWarning, "AliHLTEventGathererNew::NewEvent", "Unexpected Broadcast participant" )
		    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		    << eventID << ") unexpected from subscriber " << ndx
		    << "." << ENDLOG;
		ed->fBroadcastExpectedSubscribers.push_back( ndx );
		}
	    // Add blocks and mark our contribution to this event, if not already done so (this last part should not happen normally)
	    bool foundInList = false;
	    iter = ed->fBroadcastReceivedSubscribers.begin();
	    end = ed->fBroadcastReceivedSubscribers.end();
	    while ( iter != end )
		{
		if ( *iter == ndx )
		    {
		    foundInList = true;
		    break;
		    }
		iter++;
		}
	    bool SORBlockAdded = false;
	    bool EORBlockAdded = false;
	    bool runtypeBlockAdded=false;
	    bool ECSParamBlockAdded=false;
	    bool DCSUpdateBlockAdded=false;
	    bool componentConfigIDsBlockAdded=false;
	    bool componentConfigCompsBlockAdded=false;
	    bool componentConfigPathsBlockAdded=false;
	    ed->fBroadcastDataBlocks.reserve( ed->fBroadcastDataBlocks.size() + sbevent.fDataBlockCount );
	    for ( unsigned long nn=0; nn<sbevent.fDataBlockCount; nn++ )
		{
		vector<AliHLTSubEventDataBlockDescriptor>::iterator blIter, blEnd;
		blIter=ed->fBroadcastDataBlocks.begin();
		blEnd=ed->fBroadcastDataBlocks.end();
		if ( (!SORBlockAdded || sbevent.fDataBlocks[nn].fDataType.fID != STARTOFRUN_DATAID || sbevent.fDataBlocks[nn].fDataOrigin.fID != PRIV_DATAORIGIN ) &&
		     (!EORBlockAdded || sbevent.fDataBlocks[nn].fDataType.fID != ENDOFRUN_DATAID || sbevent.fDataBlocks[nn].fDataOrigin.fID != PRIV_DATAORIGIN ) &&
		     (!runtypeBlockAdded || sbevent.fDataBlocks[nn].fDataType.fID != RUNTYPE_DATAID || sbevent.fDataBlocks[nn].fDataOrigin.fID != PRIV_DATAORIGIN ) &&
		     (!ECSParamBlockAdded || sbevent.fDataBlocks[nn].fDataType.fID != ECSPARAM_DATAID || sbevent.fDataBlocks[nn].fDataOrigin.fID != PRIV_DATAORIGIN ) &&
		     (!DCSUpdateBlockAdded || sbevent.fDataBlocks[nn].fDataType.fID != DCSUPDATE_DATAID || sbevent.fDataBlocks[nn].fDataOrigin.fID != PRIV_DATAORIGIN ) &&
		     (!componentConfigIDsBlockAdded || sbevent.fDataBlocks[nn].fDataType.fID != COMPONENTCONFIGIDS_DATAID || sbevent.fDataBlocks[nn].fDataOrigin.fID != PRIV_DATAORIGIN ) &&
		     (!componentConfigCompsBlockAdded || sbevent.fDataBlocks[nn].fDataType.fID != COMPONENTCONFIGCOMPS_DATAID || sbevent.fDataBlocks[nn].fDataOrigin.fID != PRIV_DATAORIGIN ) &&
		     (!componentConfigPathsBlockAdded || sbevent.fDataBlocks[nn].fDataType.fID != COMPONENTCONFIGPATHS_DATAID || sbevent.fDataBlocks[nn].fDataOrigin.fID != PRIV_DATAORIGIN ) )
		    {
		    while ( blIter != blEnd )
			{
			if ( blIter->fShmID.fShmType == sbevent.fDataBlocks[nn].fShmID.fShmType &&
			     blIter->fShmID.fKey.fID == sbevent.fDataBlocks[nn].fShmID.fKey.fID &&
			     blIter->fBlockSize == sbevent.fDataBlocks[nn].fBlockSize &&
			     blIter->fBlockOffset == sbevent.fDataBlocks[nn].fBlockOffset &&
			     blIter->fProducerNode == sbevent.fDataBlocks[nn].fProducerNode &&
			     blIter->fDataType.fID == sbevent.fDataBlocks[nn].fDataType.fID &&
			     blIter->fDataOrigin.fID == sbevent.fDataBlocks[nn].fDataOrigin.fID &&
			     blIter->fDataSpecification == sbevent.fDataBlocks[nn].fDataSpecification &&
			     blIter->fStatusFlags == sbevent.fDataBlocks[nn].fStatusFlags )
			    {
			    bool mismatch=false;
			    for ( int bai=0; bai<kAliHLTSEDBDAttributeCount; bai++ )
				{
				if ( blIter->fAttributes[bai] != sbevent.fDataBlocks[nn].fAttributes[bai] )
				    {
				    mismatch = true;
				    break;
				    }
				}
			    if ( !mismatch )
				break;
			    }
			blIter++;
			}
		    if ( blIter==blEnd )
			{
			ed->fBroadcastDataBlocks.push_back( sbevent.fDataBlocks[nn] );
			if ( sbevent.fDataBlocks[nn].fDataType.fID == STARTOFRUN_DATAID && sbevent.fDataBlocks[nn].fDataOrigin.fID == PRIV_DATAORIGIN )
			    SORBlockAdded = true;
			if ( sbevent.fDataBlocks[nn].fDataType.fID == ENDOFRUN_DATAID && sbevent.fDataBlocks[nn].fDataOrigin.fID == PRIV_DATAORIGIN )
			    EORBlockAdded = true;
			if ( sbevent.fDataBlocks[nn].fDataType.fID == RUNTYPE_DATAID && sbevent.fDataBlocks[nn].fDataOrigin.fID == PRIV_DATAORIGIN )
			    runtypeBlockAdded = true;
			if ( sbevent.fDataBlocks[nn].fDataType.fID == ECSPARAM_DATAID && sbevent.fDataBlocks[nn].fDataOrigin.fID == PRIV_DATAORIGIN )
			    ECSParamBlockAdded = true;
			if ( sbevent.fDataBlocks[nn].fDataType.fID == DCSUPDATE_DATAID && sbevent.fDataBlocks[nn].fDataOrigin.fID == PRIV_DATAORIGIN )
			    DCSUpdateBlockAdded = true;
			if ( sbevent.fDataBlocks[nn].fDataType.fID == COMPONENTCONFIGIDS_DATAID && sbevent.fDataBlocks[nn].fDataOrigin.fID == PRIV_DATAORIGIN )
			    componentConfigIDsBlockAdded = true;
			if ( sbevent.fDataBlocks[nn].fDataType.fID == COMPONENTCONFIGCOMPS_DATAID && sbevent.fDataBlocks[nn].fDataOrigin.fID == PRIV_DATAORIGIN )
			    componentConfigCompsBlockAdded = true;
			if ( sbevent.fDataBlocks[nn].fDataType.fID == COMPONENTCONFIGPATHS_DATAID && sbevent.fDataBlocks[nn].fDataOrigin.fID == PRIV_DATAORIGIN )
			    componentConfigPathsBlockAdded = true;
			}
		    }
		}
	    if ( !foundInList )
		ed->fBroadcastReceivedSubscribers.push_back( ndx );
	    if ( ed->fBroadcastReceivedSubscribers.size() >= ed->fBroadcastExpectedSubscribers.size() ) // All contributing subscribers received.
		{
		// If all expected subscribers have contributed, prepare the event for announcement later on below
		found = true;
		int ret = MakeBroadcastEventSEDD( ed );
		if ( ret )
		    {
		    pthread_mutex_unlock( &fEventMutex );
#if 0
		    pthread_mutex_lock( &fETSCacheMutex );
		    fETSCache.Release( (uint8*)ets );
		    ets = NULL;
		    pthread_mutex_unlock( &fETSCacheMutex );
		    pthread_mutex_lock( &fDescriptorHandlerMutex );
		    fDescriptorHandler->ReleaseEventDescriptor( eventID );
		    sedd = NULL;
		    pthread_mutex_unlock( &fDescriptorHandlerMutex );
#endif
		    return ret;
		    }
		}
	    else
		{
		pthread_mutex_unlock( &fEventMutex );
#if 0
		pthread_mutex_lock( &fETSCacheMutex );
		fETSCache.Release( (uint8*)ets );
		ets = NULL;
		pthread_mutex_unlock( &fETSCacheMutex );
		pthread_mutex_lock( &fDescriptorHandlerMutex );
		fDescriptorHandler->ReleaseEventDescriptor( eventID );
		sedd = NULL;
		pthread_mutex_unlock( &fDescriptorHandlerMutex );
#endif
		return 0;
		}
	    }
	}
    // Three ways to get here: non-broadcast event, first contributing part of broadcast event, broadcast event with all parts (found==true)
    if ( !found )
	{
	// Check to see whether event was already received and maybe even finished already.
	if ( fDisabledSubscriberEventData.FindElement( eventID, evNdx ) && !(sbevent.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST) )
	    {
	    ed = fDisabledSubscriberEventData.Get( evNdx );
	    LOG( AliHLTLog::kInformational, "AliHLTEventGathererNew::NewEvent", "Event already received" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") has already been received from subscriber " << ed->fSubscriberNdx
		<< "." << ENDLOG;
	    ed->fSubscriberNdx = ndx;
	    ed->fSubscriberDisabled = false;
	    fEventData.Add( ed, eventID );
	    fDisabledSubscriberEventData.Remove( evNdx );
	    found=true;
	    }
	else if ( fDisabledSubscriberEDDs.FindElement( eventID, evNdx ) && !(sbevent.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST) )
	    {
	    ebd = fDisabledSubscriberEDDs.Get( evNdx );
	    LOG( AliHLTLog::kInformational, "AliHLTEventGathererNew::NewEvent", "Event already done" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") already done by disabled subscriber " << ebd.fSubscriberNdx
		<< "." << ENDLOG;
	    ebd.fSubscriberNdx = ndx;
	    if ( fEDDBacklog.GetSize()==fEDDBacklog.GetCnt() && fEDDBacklog.GetSize()>=fEventData.GetSize() )
		{
		oldEDD = fEDDBacklog.GetFirst().fEDD;
		fEDDBacklog.RemoveFirst();
		}
	    fEDDBacklog.Add( ebd, eventID );
	    fDisabledSubscriberEDDs.Remove( evNdx );
	    foundEDD = true;
	    }
	else if ( fEDDBacklog.FindElement( eventID, evNdx ) )
	    {
	    ebd = fEDDBacklog.Get( evNdx );
	    LOG( AliHLTLog::kWarning, "AliHLTEventGathererNew::NewEvent", "Event already done" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") already done by subscriber " << ebd.fSubscriberNdx
		<< "." << ENDLOG;
	    foundEDD = true;
	    }
	}
    if ( found )
	{
	bool doSignal=false;
	if ( !fAliceHLT || fSORSent || (ed->fEventID.fType!=kAliEventTypeData && ed->fEventID.fType!=kAliEventTypeEndOfRun && ed->fEventID.fType!=kAliEventTypeSync) )
	    {
	    fSignal.AddNotificationData( ed );
	    doSignal = true;
	    }
	else
	    fSORSentDeferBuffer.Add( ed );
	if ( fAliceHLT && !fSORSent && (ed->fEventID.fType==kAliEventTypeStartOfRun || flushNoSOR ) )
	    {
	    if ( ed->fEventID.fType!=kAliEventTypeStartOfRun )
		{
		LOG( AliHLTLog::kWarning, "AliHLTEventGathererNew::NewEvent", "SOR not received on all inputs" )
		    << "SOD event not received on all inputs." << ENDLOG;
		}
	    fSORSent = true;
	    while ( fSORSentDeferBuffer.GetCnt()>0 )
		{
		fSignal.AddNotificationData( fSORSentDeferBuffer.GetFirst() );
		fSORSentDeferBuffer.RemoveFirst();
		}
	    doSignal = true;
	    }
	pthread_mutex_unlock( &fEventMutex );
	if ( doSignal )
	    {
	    fSignal.Signal();
	    LOG( AliHLTLog::kDebug, "AliHLTEventGathererNew::NewEvent", "Event Signalled" )
		<< "Event 0x"
		<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec 
		<< eventID << ") signalled."
		<< ENDLOG;
	    }
#if 0
	pthread_mutex_lock( &fETSCacheMutex );
	fETSCache.Release( (uint8*)ets );
	ets = NULL;
	pthread_mutex_unlock( &fETSCacheMutex );
	pthread_mutex_lock( &fDescriptorHandlerMutex );
	fDescriptorHandler->ReleaseEventDescriptor( eventID );
	sedd = NULL;
	pthread_mutex_unlock( &fDescriptorHandlerMutex );
#endif
	return 0;
	}

    if ( foundEDD )
	{
	pthread_mutex_unlock( &fEventMutex );
	if ( oldEDD )
	    {
	    pthread_mutex_lock( &fEDDCacheMutex );
	    fEDDCache.Release( (uint8*)oldEDD );
	    pthread_mutex_unlock( &fEDDCacheMutex );
	    }
	if ( ebd.fEDD )
	    {
	    publisher.EventDone( *sub, *(ebd.fEDD) );
	    }
	else
	    {
	    pthread_mutex_lock( &fStaticEDDMutex );
	    fStaticEDD.fEventID = eventID;
	    publisher.EventDone( *sub, fStaticEDD );
	    pthread_mutex_unlock( &fStaticEDDMutex );
	    }
#if 0
	pthread_mutex_lock( &fETSCacheMutex );
	fETSCache.Release( (uint8*)ets );
	ets = NULL;
	pthread_mutex_unlock( &fETSCacheMutex );
	pthread_mutex_lock( &fDescriptorHandlerMutex );
	fDescriptorHandler->ReleaseEventDescriptor( eventID );
	sedd = NULL;
	pthread_mutex_unlock( &fDescriptorHandlerMutex );
#endif
	return 0;
	}

    pthread_mutex_lock( &fETSCacheMutex );
    ets = (AliHLTEventTriggerStruct*)fETSCache.Get( origets.fHeader.fLength );
    pthread_mutex_unlock( &fETSCacheMutex );
    if ( !ets )
	{
	// XXX
	LOG( AliHLTLog::kError, "AliHLTEventGathererNew::NewEvent", "Out Of Memory" )
	    << "Out of memory allocating event trigger structure of "
	    << AliHLTLog::kDec << origets.fHeader.fLength << " bytes." << ENDLOG;
	return ENOMEM;
	}
    
    pthread_mutex_lock( &fDescriptorHandlerMutex );
    sedd = fDescriptorHandler->GetFreeEventDescriptor( eventID, sbevent.fDataBlockCount );
    pthread_mutex_unlock( &fDescriptorHandlerMutex );
    if ( !sedd )
	{
	// XXX
	LOG( AliHLTLog::kError, "AliHLTEventGathererNew::NewEvent", "Out Of Memory" )
	    << "Out of memory allocating sub-event data descriptor of "
	    << AliHLTLog::kDec << sbevent.fHeader.fLength << " bytes ("
	    << sbevent.fDataBlockCount << " blocks)." << ENDLOG;
	pthread_mutex_lock( &fETSCacheMutex );
	fETSCache.Release( (uint8*)ets );
	ets = NULL;
	pthread_mutex_unlock( &fETSCacheMutex );
	return ENOMEM;
	}
    
    memcpy( ets, &origets, origets.fHeader.fLength );


    unsigned long length = sbevent.fHeader.fLength;
    if ( length > sedd->fHeader.fLength )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventGathererNew::NewEvent", "Event Descriptor Error" )
	    << "Sub-Event Descriptor mismatch in length and block count." << ENDLOG;
	length = sedd->fHeader.fLength;
	}
    memcpy( sedd, &sbevent, length );

    ed = (PEventData)fEventDataCache.Get();
    if ( !ed )
	{
	LOG( AliHLTLog::kError, "AliHLTEventGathererNew::NewEvent", "Out Of Memory" )
	    << "Out of memory allocating event data structure for event 0x"
	    << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << ")."
	    << ENDLOG;
	pthread_mutex_unlock( &fEventMutex );
	pthread_mutex_lock( &fETSCacheMutex );
	fETSCache.Release( (uint8*)ets );
	ets = NULL;
	pthread_mutex_unlock( &fETSCacheMutex );
	pthread_mutex_lock( &fDescriptorHandlerMutex );
	fDescriptorHandler->ReleaseEventDescriptor( eventID );
	sedd = NULL;
	pthread_mutex_unlock( &fDescriptorHandlerMutex );
	return ENOMEM;
	}
    ed->fEventID = eventID;
    ed->fBusy = true;
    ed->fETS = ets;
    ed->fSEDD = sedd;
    ed->fSubscriberNdx = ndx;
    ed->fSubscriberDisabled = false;
    ed->fBroadcastExpectedSubscribers = expectedParts;
    ed->fBroadcastReceivedSubscribers.push_back( ndx );
    bool announce = false;
    if ( sbevent.fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST )
	{
	ed->fBroadcastStatusFlags = sbevent.fStatusFlags;
	for ( unsigned long nn=0; nn<sbevent.fDataBlockCount; nn++ )
	    {
	    ed->fBroadcastDataBlocks.push_back( sbevent.fDataBlocks[nn] );
	    }
	if ( ed->fBroadcastReceivedSubscribers.size() >= ed->fBroadcastExpectedSubscribers.size() ) // All contributing subscribers received.
	    {
	    // If all expected subscribers have contributed, prepare the event for announcement later on below
	    announce = true;
	    int ret = MakeBroadcastEventSEDD( ed );
	    if ( ret )
		{
		pthread_mutex_unlock( &fEventMutex );
		pthread_mutex_lock( &fETSCacheMutex );
		fETSCache.Release( (uint8*)ets );
		ets = NULL;
		pthread_mutex_unlock( &fETSCacheMutex );
		pthread_mutex_lock( &fDescriptorHandlerMutex );
		fDescriptorHandler->ReleaseEventDescriptor( eventID );
		sedd = NULL;
		pthread_mutex_unlock( &fDescriptorHandlerMutex );
		return ret;
		}
	    }
	else if ( fBroadcastTimeout_ms )
	    {
	    TimeoutData* td = fTimeoutDataCache.Get();
	    if ( !td )
		{
		// XXX
		}
	    else
		{
		td->fEventID = ed->fEventID;
		fTimer.AddTimeout( fBroadcastTimeout_ms, &fTimerCallback, reinterpret_cast<AliUInt64_t>(td) );
		}
	    }
	}
    else
	announce = true; // No broadcast, only this subscriber contributes
    fEventData.Add( ed, eventID );
    if ( announce )
	{
	bool doSignal=false;
	if ( !fAliceHLT || fSORSent || (ed->fEventID.fType!=kAliEventTypeData && ed->fEventID.fType!=kAliEventTypeEndOfRun && ed->fEventID.fType!=kAliEventTypeSync) )
	    {
	    fSignal.AddNotificationData( ed );
	    doSignal = true;
	    }
	else
	    {
	    if ( fSORSentDeferBuffer.GetCnt()<=0 && fSORTimeout_ms )
		fTimer.AddTimeout( fSORTimeout_ms, &fTimerCallback, 0UL );
	    fSORSentDeferBuffer.Add( ed );
	    }
	if ( fAliceHLT && !fSORSent && (ed->fEventID.fType==kAliEventTypeStartOfRun || flushNoSOR ) )
	    {
	    if ( ed->fEventID.fType!=kAliEventTypeStartOfRun )
		{
		LOG( AliHLTLog::kWarning, "AliHLTEventGathererNew::NewEvent", "SOR not received on all inputs" )
		    << "SOD event not received on all inputs." << ENDLOG;
		}
	    fSORSent = true;
	    while ( fSORSentDeferBuffer.GetCnt()>0 )
		{
		fSignal.AddNotificationData( fSORSentDeferBuffer.GetFirst() );
		fSORSentDeferBuffer.RemoveFirst();
		}
	    doSignal = true;
	    }
	pthread_mutex_unlock( &fEventMutex );
	if ( doSignal )
	    {
	    fSignal.Signal();
	    LOG( AliHLTLog::kDebug, "AliHLTEventGathererNew::NewEvent", "Event Signalled" )
		<< "Event 0x"
		<< AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec 
		<< eventID << ") signalled."
		<< ENDLOG;
	    }
	}
    else
	pthread_mutex_unlock( &fEventMutex );
    return 0;
    }

	
// This method is called for a transient subscriber, 
// when an event has been removed from the list. 
int AliHLTEventGathererNew::EventCanceled( TSubscriber*, unsigned long, 
					   AliHLTPublisherInterface&, 
					   AliEventID_t eventID )
    {
    unsigned long evNdx;
    bool doCancel = false;
    PEventData ed;
    pthread_mutex_lock( &fEventMutex );
    if ( fEventData.FindElement( eventID, evNdx ) )
	{
	ed = fEventData.Get( evNdx );
	ed->fCanceled = true;
	if ( !ed->fBusy )
	    {
	    doCancel = true;
	    ReleaseEvent( fEventData, evNdx );
	    }
	}
    pthread_mutex_unlock( &fEventMutex );
    if ( doCancel )
	fPublisher->AbortEvent( eventID );
    return 0;
    }

	
// This method is called for a subscriber when new event done data is 
// received by a publisher (sent from another subscriber) for an event. 
int AliHLTEventGathererNew::EventDoneData( TSubscriber*, unsigned long, 
					   AliHLTPublisherInterface&, 
					   const AliHLTEventDoneData& )
    {
    return 0;
    }


// This method is called when the subscription ends.
int AliHLTEventGathererNew::SubscriptionCanceled( TSubscriber*, unsigned long, 
						  AliHLTPublisherInterface& )
    {
    return 0;
    }

	
// This method is called when the publishing 
// service runs out of buffer space. The subscriber 
// has to free as many events as possible and send an 
// EventDone message with their IDs to the publisher
int AliHLTEventGathererNew::ReleaseEventsRequest( TSubscriber*, unsigned long, 
						  AliHLTPublisherInterface& )
    {
    fPublisher->RequestEventRelease();
    return 0;
    }
 

// Method used to check alive status of subscriber. 
// This message has to be answered with 
// PingAck message within less  than a second.
int AliHLTEventGathererNew::Ping( TSubscriber* sub, unsigned long, 
				  AliHLTPublisherInterface& publisher )
    {
    publisher.PingAck( *sub );
    return 0;
    }

	
// Method used by a subscriber in response to a Ping 
// received message. The PingAck message has to be 
// called within less than one second.
int AliHLTEventGathererNew::PingAck( TSubscriber*, unsigned long, 
				     AliHLTPublisherInterface& )
    {
    return 0;
    }



// empty callback. Will be called when an
//event has already been internally canceled. 
void AliHLTEventGathererNew::CanceledEvent( TPublisher*, unsigned long,
					    AliEventID_t eventID, vector<AliHLTEventDoneData*>& eventDoneData )
    {
    if ( fEventAccounting )
	fEventAccounting->EventFinished( fName.c_str(), eventID );
    unsigned long len;
    AliHLTEventDoneData* edd=NULL;
    len = AliHLTGetMergedEventDoneDataSize( eventDoneData );
    if ( len>sizeof(AliHLTEventDoneData) )
	{
	pthread_mutex_lock( &fEDDCacheMutex );
	edd = (AliHLTEventDoneData*)fEDDCache.Get( len );
	pthread_mutex_unlock( &fEDDCacheMutex );
	if ( !edd )
	    {
	    LOG( AliHLTLog::kError, "AliHLTEventGathererNew::CanceledEvent", "Out of memory" )
		<< "Out of memory trying to allocate event done data of"
		<< AliHLTLog::kDec << len << " bytes." << ENDLOG;
	    // XXX Retry??
	    }
	else
	    AliHLTMergeEventDoneData( eventID, edd, eventDoneData );
	}

    unsigned long evNdx, sNdx=~0UL;
    PEventData ed;
    bool found = false;
    AliHLTEventDoneData* oldEDD=NULL;
    vector<unsigned long> subscriberIndices;
    pthread_mutex_lock( &fEventMutex );
    if ( fDisabledSubscriberEventData.FindElement( eventID, evNdx ) )
	{
	ed = fDisabledSubscriberEventData.Get( evNdx );
	EventBacklogData ebd;
	ebd.fSubscriberNdx = ed->fSubscriberNdx;
	ebd.fEDD = edd;
	ebd.fEventID = eventID;
	fDisabledSubscriberEDDs.Add( ebd, ebd.fEventID );
	ReleaseEvent( fDisabledSubscriberEventData, evNdx );
	}
    else if ( fEventData.FindElement( eventID, evNdx ) )
	{
	found = true;
	ed = fEventData.Get( evNdx );
	ed->fDone = true;
	EventBacklogData ebd;
	sNdx = ebd.fSubscriberNdx = ed->fSubscriberNdx;
	ebd.fEDD = edd;
	ebd.fEventID = eventID;
	if ( fEDDBacklog.GetSize()==fEDDBacklog.GetCnt() && fEDDBacklog.GetSize()>=fEventData.GetSize() )
	    {
	    oldEDD = fEDDBacklog.GetFirst().fEDD;
	    fEDDBacklog.RemoveFirst();
	    }
	if ( ed->fSEDD->fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST )
	    subscriberIndices = ed->fBroadcastReceivedSubscribers;
	else
	    subscriberIndices.push_back( ed->fSubscriberNdx );
	fEDDBacklog.Add( ebd, eventID );
	if ( !ed->fBusy )
	    ReleaseEvent( fEventData, evNdx );
	}
    else if ( !fEventsPurged )
	{
	// XXX
	LOG( AliHLTLog::kError, "AliHLTEventGathererNew::CanceledEvent", "Lost Event" )
	    << "Finished event 0x" << AliHLTLog::kHex << eventID << " ("
	    << AliHLTLog::kDec << eventID << ") lost." << ENDLOG;
	}
    pthread_mutex_unlock( &fEventMutex );
    if ( found )
	{
	vector<unsigned long>::iterator subIter, subEnd;
	vector<SubscriberData>::iterator iter, end;
	pthread_mutex_lock( &fSubscriberMutex );
	subIter = subscriberIndices.begin();
	subEnd = subscriberIndices.end();
	while ( subIter != subEnd )
	    {
	    iter = fSubscribers.begin();
	    end = fSubscribers.end();
	    while ( iter != end )
		{
		if ( iter->fNdx == *subIter )
		    {
		    if ( iter->fEnabled )
			{
			if ( edd )
			    iter->fProxy->EventDone( *(iter->fSubscriber), *edd );
			else
			    {
			    pthread_mutex_lock( &fStaticEDDMutex );
			    fStaticEDD.fEventID = eventID;
			    iter->fProxy->EventDone( *(iter->fSubscriber), fStaticEDD );
			    pthread_mutex_unlock( &fStaticEDDMutex );
			    }
			}
		    break;
		    }
		iter++;
		}
	    subIter++;
	    }
	pthread_mutex_unlock( &fSubscriberMutex );
	}

    if ( oldEDD )
	{
	pthread_mutex_lock( &fEDDCacheMutex );
	fEDDCache.Release( (uint8*)oldEDD );
	pthread_mutex_unlock( &fEDDCacheMutex );
	}

    pthread_mutex_lock( &fEventMutex );
    if ( fStatus && fPublisher )
	{
	fStatus->fHLTStatus.fPendingOutputEventCount = fPublisher->GetPendingEventCount();
	fStatus->fHLTStatus.fPendingInputEventCount = fEventData.GetCnt()-fPublisher->GetPendingEventCount();
	fStatus->Touch();
	}
    pthread_mutex_unlock( &fEventMutex );

    }


// empty callback. Will be called when an
//event has been announced. 
void AliHLTEventGathererNew::AnnouncedEvent( TPublisher*, unsigned long,
					     AliEventID_t eventID, const AliHLTSubEventDataDescriptor&, 
					     const AliHLTEventTriggerStruct&, AliUInt32_t )
    {
    pthread_mutex_lock( &fEventMutex );
    if ( fStatus && fPublisher )
	{
	if ( eventID.fType!=kAliEventTypeTick && eventID.fType!=kAliEventTypeReconfigure && eventID.fType!=kAliEventTypeDCSUpdate )
	    fStatus->fHLTStatus.fReceivedEventCount++;
	fStatus->fHLTStatus.fPendingOutputEventCount = fPublisher->GetPendingEventCount();
	fStatus->fHLTStatus.fPendingInputEventCount = fEventData.GetCnt()-fPublisher->GetPendingEventCount();
	fStatus->Touch();
	}
    pthread_mutex_unlock( &fEventMutex );
    }


// empty callback. Will be called when an event done message with non-trivial event done data
// (fDataWordCount>0) has been received for that event. 
// This is called prior to the sending of the EventDoneData to interested other subscribers
// (SendEventDoneData above)
void AliHLTEventGathererNew::EventDoneDataReceived( TPublisher* /*pub*/, unsigned long /*pubIndex*/,
						    const char* /*subscriberName*/, AliEventID_t eventID,
						    const AliHLTEventDoneData* edd, bool forceForward, bool forwarded )
    {
    if ( fEventAccounting )
	fEventAccounting->EventFinished( fName.c_str(), eventID, forceForward ? 2 : 1, forwarded ? 2 : 1 );
    if ( forceForward && edd )
	{
	unsigned long evNdx;
	PEventData ed;
	pthread_mutex_lock( &fEventMutex );
	if ( fEventData.FindElement( eventID, evNdx ) )
	    {
	    vector<unsigned long> subscriberIndices;
	    ed = fEventData.Get( evNdx );
	    if ( ed->fSEDD && (ed->fSEDD->fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST) )
		subscriberIndices = ed->fBroadcastReceivedSubscribers;
	    else
		subscriberIndices.push_back( ed->fSubscriberNdx );
	    
	    pthread_mutex_unlock( &fEventMutex );
	    vector<unsigned long>::iterator subIter, subEnd;
	    vector<SubscriberData>::iterator iter, end;
	    pthread_mutex_lock( &fSubscriberMutex );
	    subIter = subscriberIndices.begin();
	    subEnd = subscriberIndices.end();
	    while ( subIter != subEnd )
		{
		iter = fSubscribers.begin();
		end = fSubscribers.end();
		while ( iter != end )
		    {
		    if ( iter->fNdx == *subIter )
			{
			if ( iter->fEnabled )
			    {
			    if ( fEventAccounting )
				fEventAccounting->EventFinished( iter->fProxy->GetName(), eventID, forceForward ? 2 : 1, 2 );
			    iter->fProxy->EventDone( *(iter->fSubscriber), *edd, true, true );
			    }
			break;
			}
		    iter++;
		    }
		subIter++;
		}
	    pthread_mutex_unlock( &fSubscriberMutex );
	    }
	else
	    pthread_mutex_unlock( &fEventMutex );
	}
    
    }


// Empty callback, called when an already announced event has to be announced again to a new
// subscriber that wants to receive old events. This method needs to return the pointers
// to the sub-event data descriptor and the event trigger structure. If the function returns
// false this is a signal that the desired data could not be found anymore. 
bool AliHLTEventGathererNew::GetAnnouncedEventData( TPublisher*, unsigned long,
						    AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
						    AliHLTEventTriggerStruct*& trigger )
    {
    unsigned long evNdx;
    PEventData ed;
    bool found = false;
    pthread_mutex_lock( &fEventMutex );
    if ( fEventData.FindElement( eventID, evNdx ) )
	{
	found = true;
	ed = fEventData.Get( evNdx );
	sedd = ed->fSEDD;
	trigger = ed->fETS;
	}
    else if ( fDisabledSubscriberEventData.FindElement( eventID, evNdx ) )
	{
	found = true;
	ed = fDisabledSubscriberEventData.Get( evNdx );
	sedd = ed->fSEDD;
	trigger = ed->fETS;
	}
    pthread_mutex_unlock( &fEventMutex );
    return found;
    }

void AliHLTEventGathererNew::PURGEALLEVENTS()
    {
    // fETSCache fDescriptorHandler fEventData fDisabledSubscriberEventData fDisabledSubscriberEDDs fEDDBacklog fSignal fSORSentDeferBuffer fEDDCache (ebd.fEDD from fEDDBacklog / fDisabledSubscriberEDDs) fEventDataCache fTimer
    fEventsPurged = true;
    vector<AliUInt64_t> dummyNotificationData;
    fTimer.CancelAllTimeouts( dummyNotificationData );

    fSignal.Lock();
    while ( fSignal.HaveNotificationData() )
	fSignal.PopNotificationData();
    fSignal.Unlock();

    vector<PEventData> events;
    vector<AliHLTEventDoneData*> edds;
    pthread_mutex_lock( &fEventMutex );

    events.reserve( fEventData.GetCnt()+fDisabledSubscriberEventData.GetCnt() );
    while ( fEventData.GetCnt()>0 )
	{
	events.push_back( fEventData.GetFirst() );
	fEventData.RemoveFirst();
	}
    while ( fDisabledSubscriberEventData.GetCnt()>0 )
	{
	events.push_back( fDisabledSubscriberEventData.GetFirst() );
	fDisabledSubscriberEventData.RemoveFirst();
	}

    edds.reserve( fEDDBacklog.GetCnt() + fDisabledSubscriberEDDs.GetCnt() );
    while ( fEDDBacklog.GetCnt()>0 )
	{
	edds.push_back( fEDDBacklog.GetFirst().fEDD );
	fEDDBacklog.RemoveFirst();
	}
    while ( fDisabledSubscriberEDDs.GetCnt()>0 )
	{
	edds.push_back( fDisabledSubscriberEDDs.GetFirst().fEDD );
	fDisabledSubscriberEDDs.RemoveFirst();
	}
    fSORSentDeferBuffer.Clear();

    pthread_mutex_unlock( &fEventMutex );

    
    vector<PEventData>::iterator evtIter, evtEnd;
    evtIter = events.begin();
    evtEnd = events.end();
    while ( evtIter != evtEnd )
	{
	PEventData ed=*evtIter;
	if ( ed )
	    {
	    if ( ed->fETS )
		{
		pthread_mutex_lock( &fETSCacheMutex );
		fETSCache.Release( (uint8*)ed->fETS );
		pthread_mutex_unlock( &fETSCacheMutex );
		}
	    
	    if ( ed->fSEDD )
		{
		pthread_mutex_lock( &fDescriptorHandlerMutex );
		fDescriptorHandler->ReleaseEventDescriptor( ed->fEventID );
		pthread_mutex_unlock( &fDescriptorHandlerMutex );
		}
	
	    fEventDataCache.Release( ed );
	    }
	evtIter++;
	}

    vector<AliHLTEventDoneData*>::iterator eddIter, eddEnd;
    eddIter = edds.begin();
    eddEnd = edds.end();
    while ( eddIter!=eddEnd )
	{
	if ( *eddIter )
	    {
	    pthread_mutex_lock( &fEDDCacheMutex );
	    fEDDCache.Release( (uint8*)*eddIter );
	    pthread_mutex_unlock( &fEDDCacheMutex );
	    }
	eddIter++;
	}

    pthread_mutex_lock( &fEventMutex );
    if ( fStatus )
	{
	fStatus->fHLTStatus.fPendingOutputEventCount = fStatus->fHLTStatus.fPendingInputEventCount = 0;
	fStatus->Touch();
	}
    pthread_mutex_unlock( &fEventMutex );
    }

void AliHLTEventGathererNew::DumpEventMetaData()
    {
    int ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	}
    LOG( AliHLTLog::kDebug, "AliHLTEventGathererNew::DumpEventMetaData", "Event meta data dump" )
	<< "Dumping all current events to file." << ENDLOG;
    
    // We need to get the list of all current pending events, fetch their sub event descriptors,
    // dereference the shared memory pointers to all the data blocks and then write the
    // blocks to disk.

    MLUCString prefix;
    prefix = fName;
    prefix += "-Pending";
    AliHLTEventMetaDataOutput metaDataOutputPending( prefix.c_str(), fAliceHLT, fRunNumber );

    if ( fAliceHLT )
	metaDataOutputPending.SetEventTriggerDataFormatFunction(  AliHLTHLEventTriggerData2String );

    ret = metaDataOutputPending.Open();
    if ( ret!=0 )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventGathererNew::DumpEventMetaData", "Error opening event meta data output file" )
	    << "Error opening event meta data output file '" << prefix.c_str() << ": " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return;
	}

    MLUCIndexedVector<PEventData,AliEventID_t>::TIterator eventIter;
    eventIter = fEventData.Begin();
    while ( eventIter )
	{
	PEventData ed=*eventIter;
	if ( ed )
	    metaDataOutputPending.WriteEvent( ed->fEventID, ed->fSEDD, ed->fETS, "pending" );
	++eventIter;
	}

    metaDataOutputPending.Close();


    if (fEventData.GetCnt() == 0)
        {
        LOG( AliHLTLog::kDebug, "AliHLTEventGathererNew::DumpEventMetaData", "Nothing to dump" )
	    << "There were no events found to be dumped to disk" << ENDLOG;
	}


    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	}
    }


void AliHLTEventGathererNew::DoProcessing()
    {
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCRunningStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	}
    fProcessingQuitted = false;
    AliEventID_t eventID;
    unsigned long ndx;
    PEventData ed=NULL;

    fSignal.Lock();
    while ( !fQuitProcessing )
	{
	LOG( AliHLTLog::kDebug, "AliHLTEventGathererNew::DoProcessing", "Waiting" )
	    << "Waiting for event signal." << ENDLOG;
	if ( !fSignal.HaveNotificationData() || fPaused )
	    fSignal.Wait();
	LOG( AliHLTLog::kDebug, "AliHLTEventGathererNew::DoProcessing", "Woken Up" )
	    << "Woken up from event signal." << ENDLOG;
	if ( fQuitProcessing )
	    {
	    fSignal.Unlock();
	    break;
	    }
	if ( fPaused )
	    {
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCPausedStateFlag;
		}
	    continue;
	    }
	else
	    {
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState &= ~kAliHLTPCPausedStateFlag;
		}
	    }
	while ( fSignal.HaveNotificationData() )
	    {
	    ed = fSignal.PopNotificationData();
 	    if ( !ed )
 		continue;
	    eventID = ed->fEventID;
	    fSignal.Unlock();
	    pthread_mutex_lock( &fEventMutex );
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCBusyStateFlag;
		}
	    MLUCIndexedVector<PEventData,AliEventID_t>* edv=NULL;
	    if ( !ed->fSubscriberDisabled )
		edv = &fEventData;
	    else
		edv = &fDisabledSubscriberEventData;
	    if ( ed->fCanceled )
		{
// 		ed->fBusy = false;
		LOG( AliHLTLog::kInformational, "AliHLTEventGathererNew::DoProcessing", "Event canceled" )
		    << "Event 0x" << AliHLTLog::kHex
		    << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID
		    << ") canceled before announcement." << ENDLOG;
		if ( edv->FindElement( eventID, ndx ) )
		    ReleaseEvent( *edv, ndx );
		if ( fStatus )
		    {
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
		    }
		pthread_mutex_unlock( &fEventMutex );
		fSignal.Lock();
		//fSignal.PopNotificationData();
		continue;
		}
	    pthread_mutex_unlock( &fEventMutex );
	    fPublisher->AnnounceEvent( eventID, *(ed->fSEDD), *(ed->fETS) );
	    if ( fEventAccounting )
		fEventAccounting->EventForwarded( eventID );
	    pthread_mutex_lock( &fEventMutex );
	    bool canceled=false;
	    ed->fBusy = false;
	    canceled = ed->fCanceled;
	    if ( ed->fCanceled || ed->fDone )
		{
		if ( ed->fCanceled )
		    {
		    LOG( AliHLTLog::kInformational, "AliHLTEventGathererNew::DoProcessing", "Event canceled" )
			<< "Event 0x" << AliHLTLog::kHex
			<< ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID
			<< ") canceled after announcement." << ENDLOG;
		    }
		else
		    {
		    LOG( AliHLTLog::kInformational, "AliHLTEventGathererNew::DoProcessing", "Event done" )
			<< "Event 0x" << AliHLTLog::kHex
			<< ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID
			<< ") canceled done directly after announcement." << ENDLOG;
		    }
		if ( edv->FindElement( eventID, ndx ) )
		    ReleaseEvent( *edv, ndx );
		}
	    else
		{
		LOG( AliHLTLog::kInformational, "AliHLTEventGathererNew::DoProcessing", "Event announced" )
		    << "Event 0x" << AliHLTLog::kHex
		    << ed->fEventID << " (" << AliHLTLog::kDec << ed->fEventID
		    << ") announced." << ENDLOG;
		}
	    pthread_mutex_unlock( &fEventMutex );
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
		}
	    fSignal.Lock();
	    }
	}
		
    fSignal.Unlock();
    fProcessingQuitted = true;

    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCRunningStateFlag;
	if ( !(fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) )
	    fStatus->fProcessStatus.fState |= kAliHLTPCConsumerErrorStateFlag;
	}
    }


void AliHLTEventGathererNew::ReleaseEvent( MLUCIndexedVector<PEventData,AliEventID_t>& edv, unsigned long ndx )
    {
    PEventData ed;
    ed = edv.Get( ndx );

    AliHLTEventTriggerStruct* ets = ed->fETS;
    AliEventID_t eventID = ed->fEventID;

    fEventDataCache.Release( ed );
    edv.Remove( ndx );

    pthread_mutex_unlock( &fEventMutex );

    pthread_mutex_lock( &fDescriptorHandlerMutex );
    fDescriptorHandler->ReleaseEventDescriptor( eventID );
    pthread_mutex_unlock( &fDescriptorHandlerMutex );
    
    pthread_mutex_lock( &fETSCacheMutex );
    fETSCache.Release( (uint8*)ets );
    pthread_mutex_unlock( &fETSCacheMutex );

    pthread_mutex_lock( &fEventMutex );
    }

int AliHLTEventGathererNew::MakeBroadcastEventSEDD( EventData* ed )
    {
    AliHLTSubEventDataDescriptor* sedd;
    AliHLTSubEventDataDescriptor sbevent;
    memcpy( &sbevent, ed->fSEDD, sizeof(sbevent) );
    sbevent.fHeader.fLength = sizeof(sbevent);
    sbevent.fDataBlockCount = 0;
    pthread_mutex_lock( &fDescriptorHandlerMutex );
    fDescriptorHandler->ReleaseEventDescriptor( ed->fEventID );
    pthread_mutex_unlock( &fDescriptorHandlerMutex );
    ed->fSEDD = NULL;

    pthread_mutex_lock( &fDescriptorHandlerMutex );
    sedd = fDescriptorHandler->GetFreeEventDescriptor( ed->fEventID, ed->fBroadcastDataBlocks.size() );
    pthread_mutex_unlock( &fDescriptorHandlerMutex );
    if ( !sedd )
	{
	// XXX
	LOG( AliHLTLog::kError, "AliHLTEventGathererNew::NewEvent", "Out Of Memory" )
	    << "Out of memory allocating sub-event data descriptor for "
	    << AliHLTLog::kDec
	    << ed->fBroadcastDataBlocks.size() << " blocks." << ENDLOG;
	return ENOMEM;
	}
    ed->fSEDD = sedd;

    unsigned long tmpLen = ed->fSEDD->fHeader.fLength;
    memcpy( sedd, &sbevent, sbevent.fHeader.fLength );
    ed->fSEDD->fHeader.fLength = tmpLen;

    sedd->fStatusFlags = ed->fBroadcastStatusFlags;
    sedd->fDataBlockCount = ed->fBroadcastDataBlocks.size();
    vector<AliHLTSubEventDataBlockDescriptor>::iterator blkIter, blkEnd;
    blkIter = ed->fBroadcastDataBlocks.begin();
    blkEnd = ed->fBroadcastDataBlocks.end();
    unsigned long ndx=0;
    while ( blkIter != blkEnd )
	{
	sedd->fDataBlocks[ndx] = *blkIter;
	ndx++;
	blkIter++;
	}
    return 0;
    }



void AliHLTEventGathererNew::TimerExpired( TimeoutData* notData )
    {
    bool doSignal = false;
    pthread_mutex_lock( &fEventMutex );
    bool flushSORSentDeferBuffer=false;
    vector<unsigned long> missingSubscribers;
    unsigned long ndx;
    if ( notData && fEventData.FindElement( notData->fEventID, ndx ) )
	{
	PEventData ed = fEventData.Get( ndx );
	if ( ed )
	    {
	    vector<unsigned long>::iterator expSubIter, expSubEnd, recSubIter, recSubEnd;
	    expSubIter = ed->fBroadcastExpectedSubscribers.begin();
	    expSubEnd = ed->fBroadcastExpectedSubscribers.end();
	    missingSubscribers.reserve( ed->fBroadcastExpectedSubscribers.size() );
	    while ( expSubIter != expSubEnd )
		{
		recSubIter = ed->fBroadcastReceivedSubscribers.begin();
		recSubEnd = ed->fBroadcastReceivedSubscribers.end();
		while ( recSubIter != recSubEnd )
		    {
		    if ( *expSubIter==*recSubIter )
			break;
		    recSubIter++;
		    }
		if ( recSubIter == recSubEnd )
		    missingSubscribers.push_back( *expSubIter );
		expSubIter++;
		}
	    LOG( AliHLTLog::kWarning, "AliHLTEventGathererNew::TimerExpired", "Broadcast event timeout expired" )
		<< "Timeout for broadcast event 0x" << AliHLTLog::kHex << ed->fEventID << " (" 
		<< AliHLTLog::kDec << ed->fEventID << ") expired after " << fBroadcastTimeout_ms << " ms."
		<< ENDLOG;
	    int ret = MakeBroadcastEventSEDD( ed );
	    if ( ret )
		{
		pthread_mutex_unlock( &fEventMutex );
		return;
		}
	    fSignal.AddNotificationData( ed );
	    doSignal = true;
	    }
	if ( fAliceHLT && !fSORSent && ed->fEventID.fType==kAliEventTypeStartOfRun )
	    flushSORSentDeferBuffer = true;
	}
    if ( !notData && fAliceHLT && !fSORSent )
	{
	flushSORSentDeferBuffer = true;
	LOG( AliHLTLog::kWarning, "AliHLTEventGathererNew::TimerExpired", "Timeout expired for SOR" )
	    << "No SOR event received after first data event after " << AliHLTLog::kDec << fSORTimeout_ms
	    << " ms. Forwarding received events nonetheless." << ENDLOG;
	}
    if ( flushSORSentDeferBuffer )
	{
	fSORSent = true;
	while ( fSORSentDeferBuffer.GetCnt()>0 )
	    {
	    fSignal.AddNotificationData( fSORSentDeferBuffer.GetFirst() );
	    fSORSentDeferBuffer.RemoveFirst();
	    }
	doSignal = true;
	}
    pthread_mutex_unlock( &fEventMutex );
    if ( doSignal )
	{
	fSignal.Signal();
	LOG( AliHLTLog::kDebug, "AliHLTEventGathererNew::NewEvent", "Event Signalled" )
	    << "Signalling event after to announce after timeout."
	    << ENDLOG;
	}
    MLUCString miss;
    char tmp[256];
    bool first = true;
    if ( !missingSubscribers.empty() )
	{
	vector<unsigned long>::iterator subIter, subEnd;
	subIter = missingSubscribers.begin();
	subEnd = missingSubscribers.end();
	while ( subIter != subEnd )
	    {
	    snprintf( tmp, 256, " (index %lu)", *subIter );
	    if ( !first )
		miss += " - ";
	    first = false;
	    vector<SubscriberData>::iterator iter, end;
	    pthread_mutex_lock( &fSubscriberMutex );
	    iter = fSubscribers.begin();
	    end = fSubscribers.end();
	    while ( iter != end )
		{
		if ( iter->fNdx==*subIter ) // We received something from subscriber ndx (which we are currently processing), so we have to ensure it is included, whether enabled or not.
		    {
		    miss += fSubscribers[*subIter].fSubscriber->GetName();
		    break;
		    }
		iter++;
		}
	    pthread_mutex_unlock( &fSubscriberMutex );
	    miss += tmp;
	    subIter++;
	    }
	}
    if ( notData )
	fTimeoutDataCache.Release( notData );
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
