/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTEventGatherer.hpp"
#include "AliHLTEventGathererSubscriber.hpp"
#include "AliHLTLog.hpp"
#include <string.h>

AliHLTEventGatherer::AliHLTEventGatherer( int slotcnt ):
    fEventDataCache( (slotcnt<0) ? 4 : slotcnt ),
    fEventData( slotcnt ),
    fSignal( slotcnt, true )
    {
    pthread_mutex_init( &fSubscriberMutex, NULL );
    pthread_mutex_init( &fEventMutex, NULL );
    fDummyTrigger.fHeader.fLength = sizeof(AliHLTEventTriggerStruct);
    fDummyTrigger.fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
    fDummyTrigger.fHeader.fSubType.fID = 0;
    fDummyTrigger.fHeader.fVersion = 1;
    fDummyTrigger.fDataWordCount = 0;
    }

AliHLTEventGatherer::~AliHLTEventGatherer()
    {
    pthread_mutex_destroy( &fSubscriberMutex );
    pthread_mutex_destroy( &fEventMutex );
    }


void AliHLTEventGatherer::AddSubscriber( AliHLTEventGathererSubscriber* subscriber )
    {
    int ret;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventGatherer::AddSubscriber", "Mutex lock error" )
	    << "Error locking subscriber mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    fSubscribers.insert( fSubscribers.end(), subscriber );
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventGatherer::AddSubscriber", "Mutex unlock error" )
	    << "Error unlocking subscriber mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    }

bool AliHLTEventGatherer::WaitForEvent( AliEventID_t& eventID, EventGathererData*& eventData )
    {
    fQuitWaiting = false;
    if ( fSubscribers.size() <=0 )
	{
	eventData = NULL;
	return false;
	}
    fSignal.Lock();
    do
	{
	while ( !fQuitWaiting && fSignal.HaveNotificationData() )
	    {
	    EventGathererData* tmpED = reinterpret_cast<EventGathererData*>( fSignal.PopNotificationData() );
	    if ( tmpED )
		{
		fSignal.Unlock();
		eventData = tmpED;
		eventID = tmpED->fEventID;
		return true;
		}
	    }
	if ( !fQuitWaiting )
	    fSignal.Wait();
	}
    while ( !fQuitWaiting );
    eventData = NULL;
    fSignal.Unlock();
    return false;
    }

void AliHLTEventGatherer::StopWaiting()
    {
    fQuitWaiting = true;
    fSignal.Signal();
    }


void AliHLTEventGatherer::EventDone( AliHLTEventDoneData* ed )
    {
    if ( !ed )
	{
	LOG( AliHLTLog::kError, "AliHLTEventGatherer::EventDone", "NULL pointer" )
	    << "NULL pointer EventDone data passed." << ENDLOG;
	return;
	}
    AliEventID_t eventID = ed->fEventID;
    LOG( AliHLTLog::kDebug, "AliHLTEventGatherer::EventDone", "Event done" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
	<< ") finished..." << ENDLOG;
    int ret;
    //vector<EventGathererData*>::iterator eventIter, eventEnd;
    EventGathererData** eventIter;
    bool found = false;
    ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventGatherer::EventDone", "Mutex lock error" )
	    << "Error locking event mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    unsigned long ndx;
    AliHLTEventGathererSubscriber* sub = NULL;
    if ( fEventData.FindElement( &FindEventDataByID, (uint64)eventID, ndx ) )
	{
	eventIter = fEventData.GetPtr( ndx );
	sub = (*eventIter)->fSubscriber;
	(*eventIter)->fDataBlocks.clear();
	//delete (*eventIter);
	if ( (*eventIter)->fETS )
	    delete [] reinterpret_cast<AliUInt8_t*>( (*eventIter)->fETS );
	fEventDataCache.Release( *eventIter );
	fEventData.Remove( ndx );
	found = true;
	}
//     eventIter = fEventData.begin();
//     eventEnd = fEventData.end();
//     while ( eventIter != eventEnd )
// 	{
// 	if ( eventIter && (*eventIter)->fEventID == eventID )
// 	    {
// 	    sub = (*eventIter)->fSubscriber;
// 	    (*eventIter)->fDataBlocks.clear();
// 	    //delete (*eventIter);
// 	    fEventDataCache.Release( (uint8*)(*eventIter) );
// 	    fEventData.erase( eventIter );
// 	    found = true;
// 	    break;
// 	    }
// 	eventIter++;
// 	}
    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventGatherer::EventDone", "Mutex unlock error" )
	    << "Error unlocking event mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    if ( found )
	{
	if ( sub )
	    sub->EventDone( ed );
	}
    }

void AliHLTEventGatherer::NewEvent( AliHLTEventGathererSubscriber* subscriber, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* ets )
    {
    int ret;
    if ( !subscriber )
	{
	LOG( AliHLTLog::kError, "AliHLTEventGatherer::NewEvent", "Subscriber NULL pointer" )
	    << "Specified subscriber pointer is NULL pointer..." << ENDLOG;
	return;
	}
    if ( !sedd )
	{
	LOG( AliHLTLog::kError, "AliHLTEventGatherer::NewEvent", "SEDD NULL pointer" )
	    << "Specified sub-event data descriptor pointer is NULL pointer..." << ENDLOG;
	return;
	}
    ret = pthread_mutex_lock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventGatherer::NewEvent", "Mutex lock error" )
	    << "Error locking event mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    EventGathererData *ed;
    //ed = new EventData;
    ed = fEventDataCache.Get();
    if ( !ed )
	{
	LOG( AliHLTLog::kError, "AliHLTEventGatherer::NewEvent", "Out of memory" )
	    << "Out of memory trying to allocate EventData structure." << ENDLOG;
	ret = pthread_mutex_unlock( &fEventMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTEventGatherer::NewEvent", "Mutex unlock error" )
		<< "Error unlocking event mutex: " << strerror(ret)
		<< AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	    }
	return;
	}
    EventGathererData** eventIter;
    ed->fEventID = sedd->fEventID;
    ed->fSubscriber = subscriber;
    ed->fETS = (AliHLTEventTriggerStruct*)new AliUInt8_t[ ets->fHeader.fLength ];
    if ( !ed->fETS )
	{
	LOG( AliHLTLog::kError, "AliHLTEventGatherer::NewEvent", "Out of memory" )
	    << "Out of memory trying to allocate event trigger struct data of "
	    << AliHLTLog::kDec << ets->fHeader.fLength << " bytes." << ENDLOG;
	}
    else
	memcpy( ed->fETS, ets, ets->fHeader.fLength );
    unsigned long ndx;
    //eventIter = fEventData.insert( fEventData.end(), ed );
    fEventData.Add( ed, ndx );
    eventIter = fEventData.GetPtr( ndx );
    subscriber->ProcessSEDD( sedd, (*eventIter)->fDataBlocks );
    fSignal.AddNotificationData( reinterpret_cast<AliUInt64_t>(*eventIter) );
    fSignal.Signal();
    ret = pthread_mutex_unlock( &fEventMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventGatherer::NewEvent", "Mutex unlock error" )
	    << "Error unlocking event mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    }


void AliHLTEventGatherer::SubscriptionCanceled( AliHLTEventGathererSubscriber* subscriber )
    {
    int ret;
    vector<AliHLTEventGathererSubscriber*>::iterator subIter, subEnd;
    ret = pthread_mutex_lock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventGatherer::SubscriptionCanceled", "Mutex lock error" )
	    << "Error locking subscriber mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    while ( subIter != subEnd )
	{
	if ( (*subIter) == subscriber ) 
	    {
	    fSubscribers.erase( subIter );
	    if ( fSubscribers.size() <= 0 )
		{
		fQuitWaiting = true;
		fSignal.Signal();
		}
	    break;
	    }
	subIter++;
	}
    
    ret = pthread_mutex_unlock( &fSubscriberMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTEventGatherer::SubscriptionCanceled", "Mutex unlock error" )
	    << "Error unlocking subscriber mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    }






/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
