/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTControlledDataFlowComponent.hpp"
#include "AliHLTStatus.hpp"
#include "AliHLTSCCommand.hpp"
#include "AliHLTSCProcessControlCmds.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "AliHLTSCTypes.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTSCInterface.hpp"
#include "AliHLTPublisherProxyInterface.hpp"
#include "AliHLTPublisherPipeProxy.hpp"
#include "AliHLTForwardingPublisher.hpp"
#include "AliHLTForwardingSubscriber.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTShmManager.hpp"
#include "AliHLTStackTrace.hpp"
#include "AliHLTSCComponentInterface.hpp"
#include "AliHLTEventAccounting.hpp"
#include "AliHLTTriggerStackMachine.hpp"
#include "AliHLTTriggerStackMachineAssembler.hpp"
#include "AliHLTConfig.hpp"
#include "BCLURLAddressDecoder.hpp"
#include "BCLAddressLogger.hpp"
#include "MLUCCmdlineParser.hpp"
#include "MLUCSysMESLogServer.hpp"
#include "MLUCRegEx.hpp"
#include <syslog.h>
#include <stdio.h>
#include <limits.h>

#if 0
#define TRACELOG( lvl, orig, keys ) LOG(lvl,orig,keys)
#else
#define TRACELOG( lvl, orig, keys ) LOG(AliHLTLog::kNone,orig,keys)
#endif


template<class BaseClass, class PublisherClass, class SubscriberClass>
void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::AcceptingSubscriptionsCallbackFunc( void* status )
    {
    MLUCMutex::TLocker statusDataLock( ((SubscriptionStatusData*)status)->fMutex );
    ((SubscriptionStatusData*)status)->fPresentSubscriptions++;
    if ( ((SubscriptionStatusData*)status)->fPresentSubscriptions >= ((SubscriptionStatusData*)status)->fRequiredSubscriptions )
	{
	MLUCMutex::TLocker stateLock( ((SubscriptionStatusData*)status)->fStatus->fStateMutex );
	((SubscriptionStatusData*)status)->fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	}
    }


static inline bool operator<( MLUCString const& s1, MLUCString const& s2 )
    {
    return strcmp( s1.c_str(), s2.c_str() )<0;
    }


static void QuitSignalHandler( int sigValue )
    {
    exit( (1 << 31) | sigValue );
    }




#ifndef DO_INLINE
#define INLINE inline
// #include "AliHLTControlledDataFlowComponent.inl"
#endif


/***************************************************************************/

#define TRACE printf( "%s:%d\n", __FILE__, __LINE__ )

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::AliHLTControlledDataFlowComponent( AliHLTControlledDataFlowComponent::AliHLTControlledDataFlowComponentType componentType, 
								      const char* compName, int argc, char** argv ):
    fCmdSignal( 5 ),
    fCmdProcessor( this ), fSubscriberEventCallback( this ),
    fEventAccounting(NULL),
    fEventIDBased(true)
    {
    fComponentType = componentType;
    switch ( fComponentType )
	{
	case kMerger:
	    fComponentTypeName = "Merger"; 
	    break;
	case kScatterer:
	    fComponentTypeName = "Scatterer"; 
	    break;
	case kGatherer:
	    fComponentTypeName = "Gatherer";
	    break;
	case kPublisherBridgeHead:
	    fComponentTypeName = "Publisher Bridge Head"; 
	    break;
	case kSubscriberBridgeHead:
	    fComponentTypeName = "Subscriber Bridge Head"; 
	    break;
	default:
	    fComponentTypeName = "Unknown";
	    break;
	}
    fComponentName = compName;
    fArgc = argc;
    fArgv = argv;
    fSCInterface = NULL;
    fStatus = NULL;
    fLogOut = NULL;
    fFileLog = NULL;
    fSysLog = NULL;
    fSysMESLog = NULL;
    fSysMESLogging = false;
    fDynLibLog = NULL;
    fDynLibPath = "";
    fDynLibLogging = false;
    fDescriptorHandler = NULL;
    fShmManager = NULL;
    fSubscriberRetryCount = 0;
    fSubscriberRetryTime = 100; // milliseconds
    fBaseObject = NULL;
    fMaxPreEventCount = 0;
    fMaxPreEventCountExp2 = -1;
    pthread_mutex_init( &fPublisherMutex, NULL );
    pthread_mutex_init( &fSubscriberMutex, NULL );
    pthread_mutex_init( &fLAMAddressMutex, NULL );
    pthread_mutex_init( &fSubscriberRemovalListMutex, NULL );
    fMaxSubscriberIndex = 0;
    fMaxPublisherIndex = 0;
    fEventAccounting = NULL;
    fScattererParam = 0;
    fScattererParamSet = false;
    fGathererBroadcastTimeout_ms = 0;
    fGathererSORTimeout_ms = 0;
    fEventModuloPreDivisor = 1;
    fTriggerStackMachine = NULL;
    fRunNumber = 0;
    fBeamType = 0;
    fHLTMode = 0;
    fRunType = "";
    fStrictSORRequirements = true;
    struct sigaction newSigAction;
    newSigAction.sa_sigaction = NULL;
    newSigAction.sa_handler = &QuitSignalHandler;
    sigemptyset( &newSigAction.sa_mask );
    newSigAction.sa_flags = 0;
    sigaction( SIGQUIT, &newSigAction, &fOldSigAction );
    ResetConfiguration();
    AliHLTRegisterStackTraceHandlers();
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::~AliHLTControlledDataFlowComponent()
    {
    Unconfigure();
    pthread_mutex_destroy( &fPublisherMutex );
    pthread_mutex_destroy( &fSubscriberMutex );
    pthread_mutex_destroy( &fLAMAddressMutex );
    pthread_mutex_destroy( &fSubscriberRemovalListMutex );
    }


template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Run()
    {
    if ( !PreConfigure() )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Run", "Pre-Configuration Error" )
	    << "Pre-Configuration Error for " << fComponentTypeName << " component "
	    << fComponentName << "." << ENDLOG;
	PostUnconfigure();
	return;
	}
    LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::Run", "Component Started" )
	<< fComponentTypeName << " component " << fComponentName << " started." << ENDLOG;
    AliUInt32_t cmd = kAliHLTPCNOPCmd;
    bool quit = false;
    fCmdSignal.Lock();
    do
	{
	if ( !fCmdSignal.HaveNotificationData() )
	    fCmdSignal.Wait();
	while ( fCmdSignal.HaveNotificationData() )
	    {
	    cmd = (AliUInt32_t)fCmdSignal.PopNotificationData();
	    fCmdSignal.Unlock();
	    if ( !ExecCmd( cmd, quit ) )
		IllegalCmd( fStatus->fProcessStatus.fState, cmd );
	    fCmdSignal.Lock();
	    }
	}
    while ( !quit );
    fCmdSignal.Unlock();
    LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::Run", "Component Ended" )
	<< fComponentTypeName << " component " << fComponentName << " ended." << ENDLOG;
    PostUnconfigure();
    }


template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::ExecCmd( AliUInt32_t cmd, bool& quit )
    {
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    switch ( cmd )
	{
	case kAliHLTPCEndProgramCmd:
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCStartedStateFlag) &&
		 !(fStatus->fProcessStatus.fState & ~kAliHLTPCStartedStateFlag) )
		{
		quit = true;
		return true;
		}
	    break;
	case kAliHLTPCSubscribeCmd:
	    if ( GetMinSubscriberCount()<=0 )
		break;
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) &&
		 !(fStatus->fProcessStatus.fState & (kAliHLTPCChangingStateFlag|kAliHLTPCSubscribedStateFlag)) &&
		 !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		{
		fStatus->fProcessStatus.fState |= (kAliHLTPCChangingStateFlag|kAliHLTPCSubscribedStateFlag);
		stateLock.Unlock();
		Subscribe();
		return true;
		}
	    if ( fStatus->fProcessStatus.fState & (kAliHLTPCChangingStateFlag|kAliHLTPCSubscribedStateFlag) )
		return true;
	    
	    // 		switch ( fStatus->fProcessStatus.fState )
	    // 		    {
	    // 		    case kAliHLTPCConfiguredState: // Fallthrough
	    // 		    case kAliHLTPCAcceptingSubscriptionsState: // Fallthrough
	    // 		    case kAliHLTPCWithSubscribersState:
	    
	    // 			return true;
	    // 			break;
	    // 		    }
	    break;
	case kAliHLTPCUnsubscribeCmd:
	    if ( GetMinSubscriberCount()<=0 )
		break;
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) &&
		 !(fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) && 
		 !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		{
		fStatus->fProcessStatus.fState |= kAliHLTPCChangingStateFlag;
		stateLock.Unlock();
		Unsubscribe();
		stateLock.Lock();
		fStatus->fProcessStatus.fState &= ~(kAliHLTPCChangingStateFlag|kAliHLTPCSubscribedStateFlag|kAliHLTPCConsumerErrorStateFlag);
		return true;
		}
	    break;
	case kAliHLTPCAcceptSubscriptionsCmd:
	    if ( GetMinPublisherCount()<=0 )
		break;
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) &&
		 !(fStatus->fProcessStatus.fState & (kAliHLTPCChangingStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag)) &&
		 !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		{
		fStatus->fProcessStatus.fState |= (kAliHLTPCChangingStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag);
		stateLock.Unlock();
		AcceptSubscriptions();
		return true;
		}
	    break;
	case kAliHLTPCStopSubscriptionsCmd:
	    if ( GetMinPublisherCount()<=0 )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) 
		 || ((fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) && (fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag)) )
		{
		return true;
		}
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) &&
		 !(fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) && 
		 !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		{
		fStatus->fProcessStatus.fState |= kAliHLTPCChangingStateFlag;
		stateLock.Unlock();
		StopSubscriptions();
		stateLock.Lock();
		fStatus->fProcessStatus.fState &= ~(kAliHLTPCChangingStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag|kAliHLTPCProducerErrorStateFlag);
		return true;
		}
	    break;
	case kAliHLTPCCancelSubscriptionsCmd:
	    if ( GetMinPublisherCount()<=0 )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) &&
		 !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		{
		stateLock.Unlock();
		CancelSubscriptions();
		return true;
		}
	    break;
	case kAliHLTPCStartProcessingCmd:
	    {
	    bool ok = true;
	    if ( GetMinPublisherCount()>0 )
		{
		if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) ||
		     (fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		    ok = false;
		}
	    if ( GetMinSubscriberCount()>0 )
		{
		if ( !(fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) ||
		     (fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		    ok = false;
		}
	    if ( !ok )
		break;
	    fStatus->fProcessStatus.fState |= (kAliHLTPCChangingStateFlag|kAliHLTPCRunningStateFlag);
	    stateLock.Unlock();
	    StartProcessing();
	    return true;
	    }
	case kAliHLTPCPauseProcessingCmd:
	    // 		if ( fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag )
	    // 		    break;
	    stateLock.Unlock();
	    PauseProcessing();
	    return true;
	case kAliHLTPCResumeProcessingCmd:
	    // 		if ( fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag )
	    // 		    break;
	    stateLock.Unlock();
	    ResumeProcessing();
	    return true;
	case kAliHLTPCStopProcessingCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		break;
	    fStatus->fProcessStatus.fState |= kAliHLTPCChangingStateFlag;		
	    stateLock.Unlock();
	    StopProcessing();
	    stateLock.Lock();
	    fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;		
	    return true;
	case kAliHLTPCPublisherPinEventsCmd:
	    {
	    if ( GetMinPublisherCount()<=0 )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
		break;
	    stateLock.Unlock();
	    typename vector<PublisherData>::iterator pubIter, pubEnd;
	    pthread_mutex_lock( &fPublisherMutex );
	    pubIter = fPublishers.begin();
	    pubEnd = fPublishers.end();
	    while ( pubIter != pubEnd )
		{
		if ( pubIter->fPublisher )
		    pubIter->fPublisher->PinEvents( true );
		pubIter++;
		}
	    pthread_mutex_unlock( &fPublisherMutex );
	    return true;
	    }
	case kAliHLTPCPublisherUnpinEventsCmd:
	    {
	    if ( GetMinPublisherCount()<=0 )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
		break;
	    stateLock.Unlock();
	    typename vector<PublisherData>::iterator pubIter, pubEnd;
	    pthread_mutex_lock( &fPublisherMutex );
	    pubIter = fPublishers.begin();
	    pubEnd = fPublishers.end();
	    while ( pubIter != pubEnd )
		{
		if ( pubIter->fPublisher )
		    pubIter->fPublisher->PinEvents( false );
		pubIter++;
		}
	    pthread_mutex_unlock( &fPublisherMutex );
	    return true;
	    }
	case kAliHLTPCRemoveSubscriberCmd: // Fallthrough
	case kAliHLTPCCancelSubscriberCmd:
	    {
	    if ( GetMinPublisherCount()<=0 )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) && 
		 !(fStatus->fProcessStatus.fState & kAliHLTPCWithSubscribersStateFlag) )
		break;
	    stateLock.Unlock();
	    bool sendUnsubscribeMsg;
	    if ( cmd==kAliHLTPCCancelSubscriberCmd )
		sendUnsubscribeMsg = true;
	    else
		sendUnsubscribeMsg = false;
	    SubscriberRemovalData tmp;
	    pthread_mutex_lock( &fSubscriberRemovalListMutex );
	    while ( fSubscriberRemovalList.size()>0 )
		{
		tmp = *(fSubscriberRemovalList.begin());
		fSubscriberRemovalList.erase( fSubscriberRemovalList.begin() );
		pthread_mutex_unlock( &fSubscriberRemovalListMutex );
		pthread_mutex_lock( &fPublisherMutex );
		if ( fPublishers.size()<tmp.fPublisherIndex && fPublishers[tmp.fPublisherIndex].fPublisher )
		    fPublishers[tmp.fPublisherIndex].fPublisher->CancelSubscription( tmp.fSubscriberName.c_str(), sendUnsubscribeMsg );
		pthread_mutex_unlock( &fPublisherMutex );
		pthread_mutex_lock( &fSubscriberRemovalListMutex );
		}
	    pthread_mutex_unlock( &fSubscriberRemovalListMutex );
	    return true;
	    }
	case kAliHLTPCSimulateErrorCmd:
	    if ( !(fStatus->fProcessStatus.fState & ~kAliHLTPCStartedStateFlag) )
		break;
	    stateLock.Unlock();
	    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::ExecCmd", "SIMULATING ERROR" )
		<< "SIMULATING ERROR. Setting error flag." << ENDLOG;
	    stateLock.Lock();
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return true;
	case kAliHLTPCPURGEALLEVENTSCmd:
	    stateLock.Unlock();
	    PURGEALLEVENTS();
	    stateLock.Lock();
	    return true;
	}
    return false;
    }
												  


template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::ResetConfiguration()
    {
    fStdoutLogging = true;
    fFileLogging = true;
    fSysLogging = false;
    fSysMESLogging = false;
    fSysMESLog = NULL;
    fDynLibLog = NULL;
    fDynLibPath = "";
    fDynLibLogging = false;
    fSCInterface = NULL;
    if ( fComponentName )
	fLogName = fComponentName;
    else
	fLogName = fArgv[0];
    fSCInterfaceAddress = "tcpmsg://localhost:10000/";

//     fMaxPreEventCount = 1;
//     fMaxPreEventCountExp2 = 0;
    fMinPreBlockCount = 1;
    fMaxPreBlockCount = 1;
    fMaxNoUseCount = 0;

    fPublisherTimeout = ~(AliUInt32_t)0;

    fSubscriberRetryCount = 0;
    fSubscriberRetryTime = 100; // milliseconds

    fSubscriptionRequestOldEvents = true;

    fEventTimeout_ms = 0;
    fDynamicTimeoutAdaptionMinTimeout_ms = 0;
    fDynamicTimeoutAdaptionMaxTimeout_ms = 0;
    fEventTimeoutShift_ms = 0;
    fDynamicInputXablingNoEventCntLimit = 0;

    typename vector<SubscriberData>::iterator subIter, subEnd;
    pthread_mutex_lock( &fSubscriberMutex );
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    while ( subIter != subEnd )
	{
	subIter->fSubscriptionLoopRunning = false;
	subIter++;
	}
    pthread_mutex_unlock( &fSubscriberMutex );

    typename vector<PublisherData>::iterator PubIter, PubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    PubIter = fPublishers.begin();
    PubEnd = fPublishers.end();
    while ( PubIter != PubEnd )
	{
	PubIter->fSubscriptionListenerLoopRunning = false;
	PubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    fETSMergeMode = AliHLTEventMergerNew::kAppendETS;
    fAliceHLT = false;
    fMaxSubscriberIndex = 0;
    fMaxPublisherIndex = 0;
    fEventAccounting = NULL;
    fScattererParam = 0;
    fScattererParamSet = false;
    fGathererBroadcastTimeout_ms = 0;
    fGathererSORTimeout_ms = 0;
    fEventModuloPreDivisor = 1;

    fRunNumber = 0;
    fBeamType = 0;
    fHLTMode = 0;
    fRunType = "";

    fStrictSORRequirements = true;

    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::PreConfigure()
    {
    gLogLevel = AliHLTLog::kAll;
    if ( !SetupStdLogging() ) // Set this up here, so that we can see error output from command line parsing.
	return false; 
    if ( !ParseCmdLine() )
	return false;
    if ( !SetupStdLogging() ) // set it up again, as the configuration/cmd line might have disabled it.
	return false;
    if ( !SetupFileLogging() )
	return false;
    if ( !SetupSysLogging() )
	return false;
    if ( !SetupSysMESLogging() )
	return false;
    if ( !SetupDynLibLogging() )
        return false;
    if ( !SetupSC() )
	return false;
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::PostUnconfigure()
    {
    RemoveSC();
    RemoveLogging();
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CheckConfiguration()
    {
    if ( GetMinPublisherCount()>fPublishers.size() )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::CheckConfiguration", "Not enough publishers specified" )
	    << "Not enough publishers specified (e.g. using -publisher command line options). Need between "
	    << MLUCLog::kDec << GetMinPublisherCount() << " and " << GetMaxPublisherCount()
	    << " publishers." << ENDLOG;
	return false;
	}
    if ( GetMaxPublisherCount()<fPublishers.size() )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::CheckConfiguration", "Too many publishers specified" )
	    << "Too many publishers specified (e.g. using -publisher command line options). Need between "
	    << MLUCLog::kDec << GetMinPublisherCount() << " and " << GetMaxPublisherCount()
	    << " publishers." << ENDLOG;
	return false;
	}
    if ( GetMinSubscriberCount()>fSubscribers.size() )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::CheckConfiguration", "Not enough subscribers specified" )
	    << "Not enough subscribers specified (e.g. using -subscribe command line options). Need between "
	    << MLUCLog::kDec << GetMinSubscriberCount() << " and " << GetMaxSubscriberCount()
	    << " subscribers." << ENDLOG;
	return false;
	}
    if ( GetMaxSubscriberCount()<fSubscribers.size() )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::CheckConfiguration", "Too many subscribers specified" )
	    << "Too many subscribers specified (e.g. using -subscribe command line options). Need between "
	    << MLUCLog::kDec << GetMinSubscriberCount() << " and " << GetMaxSubscriberCount()
	    << " subscribers." << ENDLOG;
	return false;
	}

    typename vector<SubscriberData>::iterator subIter, subEnd;
    pthread_mutex_lock( &fSubscriberMutex );
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    while ( subIter != subEnd )
	{
	if ( !fAliceHLT && subIter->fEventTypeStackMachineCodes.size()>0 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::CheckConfiguration", "Event Type Code only supported in ALICE HLT mode" )
		<< "Event type code is only supported in special ALICE HLT mode."
		<< ENDLOG;
	    pthread_mutex_unlock( &fSubscriberMutex );
	    return false;
	    }
	subIter++;
	}
    pthread_mutex_unlock( &fSubscriberMutex );

    if ( fAliceHLT && !AliHLTReadoutList::IsVersionSupported(gReadoutListVersion) )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::CheckConfiguration", "Readout List Version not supported" )
	    << "Readout list version " << AliHLTLog::kDec << gReadoutListVersion
	    << (gReadoutListVersion==0 ? " (default)" : "")
	    << " is not supported. Please set a different version using the -readoutlistversion parameter."
	    << ENDLOG;
	return false;
	}

    typename vector<LAMAddressData>::iterator lamIter, lamEnd;
    pthread_mutex_lock( &fLAMAddressMutex );
    lamIter = fLAMAddresses.begin();
    lamEnd = fLAMAddresses.end();
    bool isBlob = false;
    bool fail = false;
    while ( lamIter != lamEnd )
	{
	if ( BCLCheckURL( lamIter->fStr.c_str(), false, isBlob ) )
	    {
	    fail = true;
	    break;
	    }
	if ( isBlob )
	    {
	    break;
	    }
	lamIter++;
	}
    if ( fail )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::CheckConfiguration", "Error decoding LAM address" )
	    << "Error decoding LAM address '" << lamIter->fStr.c_str() << "'." << ENDLOG;
	}
    if ( isBlob )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::CheckConfiguration", "LAM address must be msg address" )
	    << "Specified LAM address '" << lamIter->fStr.c_str() << "' is a blob address instead of a message address." << ENDLOG;
	}
    pthread_mutex_unlock( &fLAMAddressMutex );
    if ( fail || isBlob )
	return false;
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::ShowConfiguration()
    {
    LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::ShowConfiguration", "Run number & type" )
	<< "Run number: " << AliHLTLog::kDec << fRunNumber << " (0x" << AliHLTLog::kHex
	<< fRunNumber 
	<< ") - Beam type: 0x" << fBeamType << " - HLT mode: 0x" << fHLTMode
	<< " - Run type: " << fRunType.c_str() << "." << ENDLOG;
	//<< ") - Run type: 0x" << fRunType << "." << ENDLOG;
    LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::ShowConfiguration", "SC config" )
	<< "SC interface address: " << fSCInterfaceAddress.c_str() << ENDLOG;
    LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::ShowConfiguration", "Log config" )
	<< (fStdoutLogging ? "Stdout logging, " : "") << (fFileLogging ? "File logging, " : "")
 	<< (fSysLogging ? "Syslog logging, " : "") << "logname: " << fLogName.c_str()
 	<< (fSysMESLogging ? ", SysMES logging" : "")
	<< (fDynLibLogging ? ", Dynamic library logging to " : "")
	<< (fDynLibLogging ? fDynLibPath.c_str() : "")
	<< "." << ENDLOG;

    LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::ShowConfiguration", "Shm config" )
	<< "Event slot pre-allocation count: " << AliHLTLog::kDec
	<< fMaxPreEventCount << " - maximum no use count: " << fMaxNoUseCount
	<< "." << ENDLOG;
#if 0
    Implement looping over different subscribers
    LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::ShowConfiguration", "Event Modulo" )
	<< "Event modulo: " << (fUseSubscriptionEventModulo ? "Yes" : "No") << " - Counter: "
	<< AliHLTLog::kDec << fSubscriptionEventModulo << ENDLOG;
#endif

    typename vector<PublisherData>::iterator pubIter, pubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    while ( pubIter != pubEnd )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::ShowConfiguration", "Own Publisher config" )
	    << "Own publisher name " << AliHLTLog::kDec << pubIter->fNdx << ": " << pubIter->fOwnPublisherName.c_str() << "." << ENDLOG;
	pubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );

    typename vector<SubscriberData>::iterator subIter, subEnd;
    pthread_mutex_lock( &fSubscriberMutex );
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    while ( subIter != subEnd )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::ShowConfiguration", "Own Subscriber config" )
	    << "Attached publisher name " << AliHLTLog::kDec << subIter->fNdx << ": " << subIter->fAttachedPublisherName.c_str()
	    << " - subscriber ID:" << subIter->fSubscriberID.c_str() << "." << ENDLOG;
	subIter++;
	}
    pthread_mutex_unlock( &fSubscriberMutex );

    typename vector<LAMAddressData>::iterator lamIter, lamEnd;
    pthread_mutex_lock( &fLAMAddressMutex );
    lamIter = fLAMAddresses.begin();
    lamEnd = fLAMAddresses.end();
    while ( lamIter != lamEnd )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::ShowConfiguration", "LAM address config" )
	    << "Specified LAM address: '" << lamIter->fStr.c_str() << "'." << ENDLOG;
	lamIter++;
	}
    pthread_mutex_unlock( &fLAMAddressMutex );
    if ( fComponentType==kMerger && fEventTimeout_ms )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::ShowConfiguration", "Event timeout config" )
	    << "Event timeout: '" << AliHLTLog::kDec << fEventTimeout_ms << " ms." << ENDLOG;
	}
    if ( fComponentType==kMerger && fDynamicTimeoutAdaptionMaxTimeout_ms )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::ShowConfiguration", "Dynamic event timeout config" )
	    << "Dynamic event timeouts activated - Min. timeout: '" << AliHLTLog::kDec << fDynamicTimeoutAdaptionMinTimeout_ms << " ms - Max. timeout: '" << AliHLTLog::kDec << fDynamicTimeoutAdaptionMaxTimeout_ms << " ms." << ENDLOG;
	}
    if ( fComponentType==kMerger && fDynamicInputXablingNoEventCntLimit )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::ShowConfiguration", "Dynamic input Xabling config" )
	    << "Dynamic disabling/enabling of inputs activated - Max. tolerated count of not received events: '" << AliHLTLog::kDec << fDynamicInputXablingNoEventCntLimit << "." << ENDLOG;
	}
    if ( fAliceHLT )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::ShowConfiguration", "ALICE HLT" )
	    << "Special ALICE HLT options activated." << ENDLOG;
	}
    if ( fEventAccounting )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "Event Accounting is active" )
	    << "Full event accounting is active." << ENDLOG;
	}
    if ( fComponentType==kScatterer && fScattererParamSet )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "Scatterer Parameter" )
	    << "Scatterer parameter is " << AliHLTLog::kDec << fScattererParam << "." << ENDLOG;
	}
    if ( fComponentType==kGatherer )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "Gatherer broadcast timeout" )
	    << "Gatherer timeout for broadcast events: " << AliHLTLog::kDec << fGathererBroadcastTimeout_ms << ENDLOG;
	LOG( AliHLTLog::kInformational, "AliHLTControlledComponent::ShowConfiguration", "Gatherer SOR event timeout" )
	    << "Gatherer timeout for SOR events: " << AliHLTLog::kDec << fGathererSORTimeout_ms << ENDLOG;
	}
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Configure()
    {
    ShowConfiguration();
    if ( !CheckConfiguration() )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	return;
	}

    unsigned long ndx=0;
    char* cpErr=NULL;
    if ( fCTPTriggerClasses.Length()>0 )
	{
	std::vector<MLUCString> triggerClasses, triggerClass, detectors;
	fCTPTriggerClasses.Split( triggerClasses, ',' );
	std::vector<MLUCString>::iterator tcIter, tcEnd;
	tcIter = triggerClasses.begin();
	tcEnd = triggerClasses.end();
	while ( tcIter != tcEnd )
	    {
	    tcIter->Split( triggerClass, ':' );
	    if ( triggerClass.size()!=3 )
		{
		LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Error Parsing Trigger Class Specifier" )
		    << "Error parsing trigger class specifier '" << fCTPTriggerClasses.c_str()
		    << "' (single trigger class format)." << ENDLOG;
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		return;
		}
	    ndx = strtoul( triggerClass[0].c_str(), &cpErr, 10 );
	    if ( *cpErr != '\0' )
		{
		LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Error Parsing Trigger Class Specifier" )
		    << "Error parsing trigger class specifier '" << fCTPTriggerClasses.c_str()
		    << "' (single trigger class bit index specifier '" << triggerClass[0].c_str() << "')." << ENDLOG;
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		return;
		}
	
	    fStackMachineAssembler.AddSymbol( triggerClass[1].c_str(), 1 << ndx );
	    
	    fTriggerClassBitIndices.insert( std::pair<MLUCString,unsigned>( triggerClass[1], ndx ) );
	    
	    if ( fComponentType==kMerger )
		{
		MLUCMutex::TLocker ddlListLocker( fTriggerClassContributingDDLsMutex );
		if ( fTriggerClassContributingDDLs.size()<ndx+1 )
		    fTriggerClassContributingDDLs.resize(ndx+1, NULL );
		if ( !fTriggerClassContributingDDLs[ndx] )
		    fTriggerClassContributingDDLs[ndx] = new hltreadout_uint32_t[READOUTLIST_MAX_WORDCOUNT+1];
		if ( !fTriggerClassContributingDDLs[ndx] )
		    {
		    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Out of memopry allocating trigger class DDL list" )
			<< "Out of memopry allocating trigger class DDL list of " << AliHLTLog::kDec
			<< sizeof(hltreadout_uint32_t)*(READOUTLIST_MAX_WORDCOUNT+1) << " bytes." << ENDLOG;
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		    return;
		    }
	    
		AliHLTReadoutList rdLst(gReadoutListVersion,(volatile_hltreadout_uint32_t*)(fTriggerClassContributingDDLs[ndx]));
		rdLst.Reset();
	    
	    
		triggerClass[2].Split( detectors, '-' );
		std::vector<MLUCString>::iterator detIter, detEnd;
		detIter = detectors.begin();
		detEnd = detectors.end();
		while ( detIter != detEnd )
		    {
		    unsigned long detID;
		    detID = strtoul( detIter->c_str(), &cpErr, 10 );
		    if ( *cpErr != '\0' )
			{
			LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Error Parsing Trigger Class Specifier" )
			    << "Error parsing trigger class specifier '" << fCTPTriggerClasses.c_str()
			    << "' (single trigger class detector ID '" << detIter->c_str() << "')." << ENDLOG;
			MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
			return;
			}
		    if ( !rdLst.SetDetectorDDLs( (TDetectorID)detID ) )
			{
			LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Error Parsing Trigger Class Specifier" )
			    << "Error parsing trigger class specifier '" << fCTPTriggerClasses.c_str()
			    << "' (single trigger class setting detector DDLs)." << ENDLOG;
			MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
			return;
			}
		    detIter++;
		    }
		}
	    tcIter++;
	    }
	}


    std::map<MLUCString,MLUCString>::iterator inputDDLIter = fSubscriberInputDDLSpecifications.begin(), inputDDLEnd = fSubscriberInputDDLSpecifications.end();
    while ( inputDDLIter != inputDDLEnd )
	{
	MLUCString subscriberID = inputDDLIter->first;
	MLUCString ddlList = inputDDLIter->second;
	typename vector<SubscriberData>::iterator iter, end;
	iter = fSubscribers.begin();
	end = fSubscribers.end();
	while ( iter != end )
	    {
	    if ( iter->fSubscriberID==subscriberID )
		{
		if ( !iter->fContributingDDLs )
		    iter->fContributingDDLs = new hltreadout_uint32_t[READOUTLIST_MAX_WORDCOUNT+1];
		if ( !iter->fContributingDDLs )
		    {
		    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Out of memory allocating subscriber DDL list" )
			<< "Out of memory allocating subscriber DDL list of " << AliHLTLog::kDec
			<< sizeof(hltreadout_uint32_t)*(READOUTLIST_MAX_WORDCOUNT+1) << " for subscriber '"
			<< subscriberID.c_str() << "' input DDL list." << ENDLOG;
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		    return;
		    }
		AliHLTReadoutList rdLst(gReadoutListVersion,iter->fContributingDDLs);
		rdLst.Reset();
		if ( !rdLst.ParseDDLList( ddlList.c_str() ) )
		    {
		    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Error parsing subscriber DDL list" )
			<< "Error parsing subscriber DDL list for subscriber '"
			<< subscriberID.c_str() << "' (must be comma separated list of valid DDL IDs)." << ENDLOG;
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		    return;
		    }
		break;
		}
	    iter++;
	    }
	if ( iter==end )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Subscriber not found for subscriber DDL list" )
		<< "Subscriber '" << subscriberID.c_str() << "' not found for subscriber DDL list." << ENDLOG;
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return;
	    }
	++inputDDLIter;
	}


    fDescriptorHandler = CreateDescriptorHandler();
    if ( !fDescriptorHandler )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Unable to Create Descriptor Handler" )
	    << "Unable to create event descriptor handler." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	return;
	}

    fShmManager = CreateShmManager();
    if ( !fShmManager )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Error creating shared memory manager" )
	    << "Error creating Shared Memory Manager object." << ENDLOG;
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	return;
	}

    if ( HasBaseObject() )
	{
	fBaseObject = CreateBaseObject();
	if ( !fBaseObject )
	    {
	    MLUCString title;
	    title = "Unable to Create ";
	    title += fComponentTypeName;
	    title += " Base Object";
	    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", title.c_str() )
		<< "Unable to create " << fComponentTypeName << " component base object." << ENDLOG;
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return;
	    }
	}

    typename vector<PublisherData>::iterator pubIter, pubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    while ( pubIter != pubEnd )
	{
	pubIter->fPublisher = CreatePublisher( pubIter->fNdx, &(*pubIter) );
	if ( !pubIter->fPublisher )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Unable to Create Publisher Object" )
		<< "Unable to create publisher object " << AliHLTLog::kDec << pubIter->fNdx << " - '"
		<< pubIter->fOwnPublisherName.c_str() << "'." << ENDLOG;
	    pthread_mutex_unlock( &fPublisherMutex );
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return;
	    }
	pubIter->fSubscriptionListenerThread = CreateSubscriptionListenerThread( pubIter->fNdx, &(*pubIter) );
	if ( !pubIter->fSubscriptionListenerThread )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Unable to Create Subscription Listener Thread Object" )
		<< "Unable to create subscription listener thread object for publisher " << AliHLTLog::kDec << pubIter->fNdx << " ('"
		<< pubIter->fOwnPublisherName.c_str() << "')." << ENDLOG;
	    pthread_mutex_unlock( &fPublisherMutex );
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return;
	    }
	pubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );

    typename vector<SubscriberData>::iterator subIter, subEnd;
    pthread_mutex_lock( &fSubscriberMutex );
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    while ( subIter != subEnd )
	{
	subIter->fSubscriber = CreateSubscriber( subIter->fNdx, &(*subIter) );
	if ( !subIter->fSubscriber )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Unable to Create Subscriber Object" )
		<< "Unable to create subscriber object " << AliHLTLog::kDec << subIter->fNdx << " - '"
		<< subIter->fAttachedPublisherName.c_str() << "'/'" << subIter->fSubscriberID.c_str()
		<< "'." << ENDLOG;
	    pthread_mutex_unlock( &fSubscriberMutex );
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return;
	    }
	subIter->fPublisherProxy = CreatePublisherProxy( subIter->fNdx, &(*subIter) );
	if ( !subIter->fPublisherProxy )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Unable to Create Publisher Proxy Object" )
		<< "Unable to create publisher proxy object " << AliHLTLog::kDec << subIter->fNdx << " - '"
		<< subIter->fAttachedPublisherName.c_str()
		<< "'." << ENDLOG;
	    pthread_mutex_unlock( &fSubscriberMutex );
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return;
	    }
	subIter->fSubscriptionThread = CreateSubscriptionThread( subIter->fNdx, &(*subIter) );
	if ( !subIter->fSubscriptionThread )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Unable to Create Subscription Thread Object" )
		<< "Unable to create subscription thread object for subscriber " << AliHLTLog::kDec << subIter->fNdx << " - '"
		<< subIter->fAttachedPublisherName.c_str() << "'/'" << subIter->fSubscriberID.c_str()
		<< "'." << ENDLOG;
	    pthread_mutex_unlock( &fSubscriberMutex );
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return;
	    }
	subIter++;
	}
    pthread_mutex_unlock( &fSubscriberMutex );

    if ( fAliceHLT )
	fTriggerStackMachine = new AliHLTTriggerStackMachine;

    typename vector<LAMAddressData>::iterator lamIter, lamEnd;
    int ret;
    pthread_mutex_lock( &fLAMAddressMutex );
    lamIter = fLAMAddresses.begin();
    lamEnd = fLAMAddresses.end();
    while ( lamIter != lamEnd )
	{
	ret = BCLDecodeRemoteURL( lamIter->fStr.c_str(), lamIter->fAddress );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Error decoding LAM address" )
		<< "Error decoding LAM address '" << lamIter->fStr.c_str() << "'." << ENDLOG;
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    break;
	    }
	lamIter++;
	}
    pthread_mutex_unlock( &fLAMAddressMutex );
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    if ( fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag )
	return;
    stateLock.Unlock();
    SetupComponents();
    stateLock.Lock();
    fStatus->fProcessStatus.fState |= kAliHLTPCConfiguredStateFlag;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Unconfigure()
    {
    typename vector<PublisherData>::iterator pubIter, pubEnd;

    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    while ( pubIter != pubEnd )
	{
	if ( pubIter->fPublisher )
	    pubIter->fPublisher->CancelAllSubscriptions( true, true );
	pubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );

    typename vector<LAMAddressData>::iterator lamIter, lamEnd;
    pthread_mutex_lock( &fLAMAddressMutex );
    lamIter = fLAMAddresses.begin();
    lamEnd = fLAMAddresses.end();
    while ( lamIter != lamEnd )
	{
	if ( lamIter->fAddress )
	    BCLFreeAddress( lamIter->fAddress );
	lamIter++;
	}
    pthread_mutex_unlock( &fLAMAddressMutex );

    typename vector<SubscriberData>::iterator subIter, subEnd;
    pthread_mutex_lock( &fSubscriberMutex );
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    while ( subIter != subEnd )
	{
	if ( subIter->fSubscriptionThread )
	    {
	    delete subIter->fSubscriptionThread;
	    subIter->fSubscriptionThread = NULL;
	    }
	if ( subIter->fPublisherProxy )
	    {
	    delete subIter->fPublisherProxy;
	    subIter->fPublisherProxy = NULL;
	    }
	if ( subIter->fSubscriber )
	    {
	    delete subIter->fSubscriber;
	    subIter->fSubscriber = NULL;
	    }
	if ( subIter->fContributingDDLs )
	    {
	    delete [] subIter->fContributingDDLs;
	    subIter->fContributingDDLs = NULL;
	    }
	while ( !subIter->fSubscriberInputEventTypeStackMachineCodes.empty() )
	    {
	    delete [] reinterpret_cast<uint8*>( *subIter->fSubscriberInputEventTypeStackMachineCodes.begin() );
	    subIter->fSubscriberInputEventTypeStackMachineCodes.erase( subIter->fSubscriberInputEventTypeStackMachineCodes.begin() );
	    }
	while ( subIter->fEventTypeStackMachineCodes.size()>0 )
	    {
	    delete [] reinterpret_cast<uint8*>( subIter->fEventTypeStackMachineCodes[0] );
	    subIter->fEventTypeStackMachineCodes.erase( subIter->fEventTypeStackMachineCodes.begin() );
	    }
	subIter++;
	}
    pthread_mutex_unlock( &fSubscriberMutex );
    
    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    while ( pubIter != pubEnd )
	{
	if ( pubIter->fSubscriptionListenerThread )
	    {
	    delete pubIter->fSubscriptionListenerThread;
	    pubIter->fSubscriptionListenerThread = NULL;
	    }
	if ( pubIter->fPublisher )
	    {
	    delete pubIter->fPublisher;
	    pubIter->fPublisher = NULL;
	    }
	pubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );

    if ( fShmManager )
	{
	delete fShmManager;
	fShmManager = NULL;
	}

    if ( fDescriptorHandler )
	{
	delete fDescriptorHandler;
	fDescriptorHandler = NULL;
	}
    if ( fEventAccounting )
	{
	delete fEventAccounting;
	fEventAccounting = NULL;
	}

    if ( fTriggerStackMachine )
	{
	delete fTriggerStackMachine;
	fTriggerStackMachine = NULL;
	}

    if ( fBaseObject )
	{
	delete fBaseObject;
	fBaseObject = NULL;
	}

    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SetupStdLogging()
    {
    BCLAddressLogger::Init();
    if ( fStdoutLogging )
	{
	if ( !fLogOut )
	    {
	    fLogOut = new AliHLTFilteredStdoutLogServer;
	    if ( !fLogOut )
		return false;
	    gLog.AddServer( fLogOut );
	    }
	}
    else
	{
	if ( fLogOut )
	    {
	    gLog.DelServer( fLogOut );
	    delete fLogOut;
	    fLogOut = NULL;
	    }
	}
    gLog.SetStackTraceComparisonDepth( 1 );
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SetupFileLogging()
    {
    if ( fFileLogging )
	{
	if ( !fFileLog )
	    {
	    fFileLog = new AliHLTFileLogServer( fLogName.c_str(), ".log", 2500000 );
	    if ( !fFileLog )
		return false;
	    gLog.AddServer( fFileLog );
	    }
	}
    else
	{
	if ( fFileLog )
	    {
	    gLog.DelServer( fFileLog );
	    delete fFileLog;
	    fFileLog = NULL;
	    }
	}
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SetupSysLogging()
    {
    if ( fSysLogging )
	{
	if ( !fSysLog )
	    {
	    fSysLog = new AliHLTSysLogServer( fLogName.c_str(), fSysLogFacility );
	    if ( !fSysLog )
		return false;
	    gLog.AddServer( fSysLog );
	    }
	}
    else
	{
	if ( fSysLog )
	    {
	    gLog.DelServer( fSysLog );
	    delete fSysLog;
	    fSysLog = NULL;
	    }
	}
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SetupSysMESLogging()
    {
    if ( fSysMESLogging )
	{
	if ( !fSysMESLog )
	    {
	    fSysMESLog = new MLUCSysMESLogServer;
	    if ( !fSysMESLog )
		return false;
	    gLog.AddServer( fSysMESLog );
	    }
	}
    else
	{
	if ( fSysMESLog )
	    {
	    gLog.DelServer( fSysMESLog );
	    delete fSysMESLog;
	    fSysMESLog = NULL;
	    }
	}
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SetupDynLibLogging()
    {
    if ( fDynLibLogging )
	{
        if ( !fDynLibLog )
	    {
            fDynLibLog = new MLUCDynamicLibraryLogServer(fDynLibPath.c_str(), fComponentName );
            if ( !fDynLibLog )
                return false;
            gLog.AddServer( fDynLibLog );
	    }
	}
    else
	{
        if ( fDynLibLog )
	    {
            gLog.DelServer( fDynLibLog );
            delete fDynLibLog;
            fDynLibLog = NULL;
	    }
	}
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::RemoveLogging()
    {
    if ( fDynLibLog )
	{
        gLog.DelServer( fDynLibLog );
        delete fDynLibLog;
        fDynLibLog = NULL;
	}
    if ( fSysMESLog )
	{
	gLog.DelServer( fSysMESLog );
	delete fSysMESLog;
	fSysMESLog = NULL;
	}
    if ( fSysLog )
	{
	gLog.DelServer( fSysLog );
	delete fSysLog;
	fSysLog = NULL;
	}
    if ( fFileLog )
	{
	gLog.DelServer( fFileLog );
	delete fFileLog;
	fFileLog = NULL;
	}
    if ( fLogOut )
	{
	gLog.DelServer( fLogOut );
	delete fLogOut;
	fLogOut = NULL;
	}
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::ParseCmdLine()
    {
    int i, errorArg=-1;
    const char* errorStr = NULL;
    
    i = 1;
    while ( (i < fArgc) && !errorStr )
	{
	errorStr = ParseCmdLine( fArgc, fArgv, i, errorArg );
	}
    if ( errorStr )
	{
	PrintUsage( errorStr, i, errorArg );
	return false;
	}
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE const char* AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cpErr;
    if ( !strcmp( argv[i], "-name" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing component name parameter";
	    }
	fComponentName = argv[i+1];
	fLogName = fComponentName;
	i += 2;
	return  NULL;
	}
    if ( !strcmp( argv[i], "-control" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing Status&Control interface address URL parameter";
	    }
	fSCInterfaceAddress = argv[i+1];
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-V" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing verbosity level specifier.";
	    }
	gLogLevel = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting verbosity level specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-nostdout" ) )
	{
	fStdoutLogging = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-nofilelog" ) )
	{
	fFileLogging = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-syslog" ) )
	{
	if ( argc <= i+2 )
	    {
	    return "Missing syslog facility parameter";
	    }
	if ( !strcmp( argv[i+1], "DAEMON" ) )
	    {
	    fSysLogFacility = LOG_DAEMON;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL0" ) )
	    {
	    fSysLogFacility = LOG_LOCAL0;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL1" ) )
	    {
	    fSysLogFacility = LOG_LOCAL1;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL2" ) )
	    {
	    fSysLogFacility = LOG_LOCAL2;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL3" ) )
	    {
	    fSysLogFacility = LOG_LOCAL3;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL4" ) )
	    {
	    fSysLogFacility = LOG_LOCAL4;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL5" ) )
	    {
	    fSysLogFacility = LOG_LOCAL5;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL6" ) )
	    {
	    fSysLogFacility = LOG_LOCAL6;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL7" ) )
	    {
	    fSysLogFacility = LOG_LOCAL7;
	    }
	else if ( !strcmp( argv[i+1], "USER" ) )
	    {
	    fSysLogFacility = LOG_USER;
	    }
	else
	    {
	    return "Unknown syslog facility parameter.";
	    }
	fSysLogging = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-sysmeslog" ) )
	{
	fSysMESLogging = true;
	i += 1;
	return NULL;
	}
    if ( !strcmp( argv[i], "-infologger" ) )
	{
        if ( argc <= i+1 )
	    {
            return "Missing path to dynamic log library.";
	    }
        fDynLibPath = argv[i+1];
        fDynLibLogging = true;
        i += 2;
        return NULL;
	}
    if ( !strcmp( argv[i], "-publishertimeout" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing publisher timeout specifier.";
	    }
	fPublisherTimeout = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting publisher timeout specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-publisher" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing publisher name specifier.";
	    }
	PublisherData pd;
	pd.fOwnPublisherName = argv[i+1];
	pd.fNdx = fMaxPublisherIndex++;
	pthread_mutex_lock( &fPublisherMutex );
	fPublishers.insert( fPublishers.end(), pd );
	pthread_mutex_unlock( &fPublisherMutex );
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventmodulopredivisor" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing event modulo pre-divisor specifier.";
	    }
	fEventModuloPreDivisor = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting event modulo pre-divisor specifier.";
	    }
	if ( !fEventModuloPreDivisor )
	    {
	    errorArg = i+1;
	    return "Event modulo pre-divisor specifier must be non-zero.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-subscribereventmodulo" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing subscriber ID and event modulo specifier.";
	    }
	if ( argc <= i+2 )
	    {
	    return "Missing event modulo specifier.";
	    }
	AliUInt32_t modulo = (AliUInt32_t)strtoul( argv[i+2], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting event modulo specifier.";
	    }
	if ( !modulo )
	    {
	    return "Event modulo specifier must not be zero.";
	    }
	typename vector<SubscriberData>::iterator iter, end;
	iter = fSubscribers.begin();
	end = fSubscribers.end();
	while ( iter != end )
	    {
	    if ( iter->fSubscriberID==argv[i+1] )
		{
		iter->fEventModulo = modulo;
		break;
		}
	    iter++;
	    }
	if ( iter==end )
	    {
	    errorArg = i+1;
	    return "Subscriber not found.";
	    }
	i += 3;
	return NULL;
	}
    if ( !strcmp( argv[i], "-subscribereventtypecode" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing subscriber ID and event type code specifier.";
	    }
	if ( argc <= i+2 )
	    {
	    return "Missing event type code specifier.";
	    }
		typename vector<SubscriberData>::iterator iter, end;
	iter = fSubscribers.begin();
	end = fSubscribers.end();
	while ( iter != end )
	    {
	    if ( iter->fSubscriberID==argv[i+1] )
		{
		iter->fEventTypeStackMachineCodesText.push_back( MLUCString(argv[i+2]) );
		break;
		}
	    iter++;
	    }
	if ( iter==end )
	    {
	    errorArg = i+1;
	    return "Subscriber not found.";
	    }
	i += 3;
	return NULL;
	}
    if ( !strcmp( argv[i], "-subscribe" ) )
	{
	if ( argc <= i+2 )
	    {
	    return "Missing publishername or subscriberid parameter";
	    }
	SubscriberData sd;
	sd.fAttachedPublisherName = argv[i+1];
	sd.fSubscriberID = argv[i+2];
	sd.fNdx = fMaxSubscriberIndex++;
	sd.fEventModulo = 1;
	pthread_mutex_lock( &fSubscriberMutex );
	fSubscribers.insert( fSubscribers.end(), sd );
	pthread_mutex_unlock( &fSubscriberMutex );
	i += 3;
	return NULL;
	}
    if ( !strcmp( argv[i], "-subscriberretrycount" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing susbcriber retry count specifier.";
	    }
	fSubscriberRetryCount = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting susbcriber retry count specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-subscriberretrytime" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing susbcriber retry time specifier.";
	    }
	fSubscriberRetryTime = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting susbcriber retry time specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventsexp2" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing event count (power of 2) specifier.";
	    }
	fMaxPreEventCountExp2 = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting event count (power of 2) specifier..";
	    }
	fMaxPreEventCount = 1 << fMaxPreEventCountExp2;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-lamaddress" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing lam address specifier.";
	    }
	LAMAddressData lad;
	lad.fAddress = NULL;
	lad.fStr = argv[i+1];
	fLAMAddresses.insert( fLAMAddresses.end(), lad );
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-subscribeoldevents" ) )
	{
	fSubscriptionRequestOldEvents = true;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-nosubscribeoldevents" ) )
	{
	fSubscriptionRequestOldEvents = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-runnr" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing run number specifier.";
	    }
	fRunNumber = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting run number specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-beamtype" ) )
	{
	if ( argc <= i + 1 )
	    {
	    return "Missing beam type specifier.";
	    }
	fBeamType = 0;
	if ( !strcmp(argv[i+1], "pp") )
	    fBeamType = 1;
	else if ( !strcmp(argv[i+1], "AA") )
	    fBeamType = 2;
	else if ( !strcmp(argv[i+1], "pA") )
	    fBeamType = 3;
	else
	    {
	    errorArg = i+1;
	    return "Wrong first parameter for beam type (must be 'pp' or 'AA' or 'pA').";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-hltmode" ) )
	{
	if ( argc <= i + 1 )
	    {
	    return "Missing HLT mode specifier.";
	    }
	fHLTMode = 0;
	if ( !strcmp(argv[i+1],"A") )
	    fHLTMode = 1;
	else if ( !strcmp(argv[i+1],"B") )
	    fHLTMode = 2;
	else if ( !strcmp(argv[i+1],"C") )
	    fHLTMode = 3;
	else if ( !strcmp(argv[i+1],"D") )
	    fHLTMode = 4;
	else if ( !strcmp(argv[i+1],"E") )
	    fHLTMode = 5;
	else
	    {
	    errorArg = i+1;
	    return "Wrong second parameter for HLT mode (must be 'A', 'B', 'C', 'D' or 'E').";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-runtype" ) )
	{
	if ( argc <= i + 1 )
	    {
	    return "Missing run type specifier.";
	    }
	fRunType = argv[i+1];
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-readoutlistversion" ) )
	{
	if ( argc <= i +1 )
	    {
	    return "Missing readout list data format version number specifier.";
	    }
	gReadoutListVersion = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting readout list data format version specifier.";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-ctptriggerclasses" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing CTP trigger class specifier.";
	    }
	fCTPTriggerClasses = argv[i+1];
#if 0
	MLUCString tcString( argv[i+1] );
	std::vector<MLUCString> triggerClasses, triggerClass, detectors;
	tcString.Split( triggerClasses, ',' );
	std::vector<MLUCString>::iterator tcIter, tcEnd;
	tcIter = triggerClasses.begin();
	tcEnd = triggerClasses.end();
	while ( tcIter != tcEnd )
	    {
	    tcIter->Split( triggerClass, ':' );
	    if ( triggerClass.size()!=3 )
		return "Error parsing trigger class specifier (single trigger class format)";
	    unsigned long ndx=0;
	    ndx = strtoul( triggerClass[0].c_str(), &cpErr, 10 );
	    if ( *cpErr != '\0' )
		return "Error parsing trigger class specifier (single trigger class bit index specifier)";

	    fStackMachineAssembler.AddSymbol( triggerClass[1].c_str(), 1 << ndx );
	    
	    fTriggerClassBitIndices.insert( std::pair<MLUCString,unsigned>( triggerClass[1], ndx ) );

	    if ( fComponentType==kMerger )
		{
		MLUCMutex::TLocker ddlListLocker( fTriggerClassContributingDDLsMutex );
		if ( fTriggerClassContributingDDLs.size()<ndx+1 )
		    fTriggerClassContributingDDLs.resize(ndx+1, NULL );
		if ( !fTriggerClassContributingDDLs[ndx] )
		    fTriggerClassContributingDDLs[ndx] = new hltreadout_uint32_t[READOUTLIST_MAX_WORDCOUNT+1];
		if ( !fTriggerClassContributingDDLs[ndx] )
		    return "Out of memory allocating trigger class DDL list";

		AliHLTReadoutList rdLst(gReadoutListVersion,(volatile_hltreadout_uint32_t*)(fTriggerClassContributingDDLs[ndx]));
		rdLst.Reset();


		triggerClass[2].Split( detectors, '-' );
		std::vector<MLUCString>::iterator detIter, detEnd;
		detIter = detectors.begin();
		detEnd = detectors.end();
		while ( detIter != detEnd )
		    {
		    unsigned long detID;
		    detID = strtoul( detIter->c_str(), &cpErr, 10 );
		    if ( *cpErr != '\0' )
			return "Error parsing trigger class specifier (single trigger class detector ID)";
		    if ( !rdLst.SetDetectorDDLs( (TDetectorID)detID ) )
			return "Error processing trigger class specifier (single trigger class setting detector DDLs)";
		    detIter++;
		    }
		}
	    tcIter++;
	    }
#endif
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-ctptriggerclassgroup" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing CTP trigger class group identifier and match specification";
	    }
	if ( argc <= i+2 )
	    {
	    return "Missing CTP trigger class group match specification";
	    }
	fTriggerClassGroups.insert( std::pair<MLUCString,MLUCString>( MLUCString(argv[i+1]), MLUCString(argv[i+2]) ) );
	i += 3;
	return NULL;
	}
    if ( fComponentType==kMerger )
	{
	if ( !strcmp( argv[i], "-eventtimeout" ) )
	    {
	    if ( argc <= i+1 )
		{
		return "Missing event timeout specifier.";
		}
	    fEventTimeout_ms = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		return "Error converting event timeout specifier..";
		}
	    i += 2;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-eventtimeoutshift" ) )
	    {
	    if ( argc <= i+1 )
		{
		return "Missing event timeout shift specifier.";
		}
	    fEventTimeoutShift_ms = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		return "Error converting event timeout specifier..";
		}
	    i += 2;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-dynamictimeouts" ) )
	    {
	    if ( argc <= i+1 )
		{
		return "Missing min. and max. dynamic timeout specifiers.";
		}
	    if ( argc <= i+2 )
		{
		return "Missing max. dynamic timeout specifier.";
		}
	    fDynamicTimeoutAdaptionMinTimeout_ms = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		return "Error converting min. dynamic timeout specifier..";
		}
	    fDynamicTimeoutAdaptionMaxTimeout_ms = strtoul( argv[i+2], &cpErr, 0 );
	    if ( *cpErr )
		{
		return "Error converting max. dynamic timeout specifier..";
		}
	    if ( fDynamicTimeoutAdaptionMaxTimeout_ms<=0 )
		{
		return "Max. dynamic timeout specifier must be bigger than zero (0 disables)..";
		}
	    if ( fDynamicTimeoutAdaptionMaxTimeout_ms<fDynamicTimeoutAdaptionMinTimeout_ms )
		{
		return "Max. dynamic timeout specifier must be bigger than min.timeout specifier..";
		}
	    i += 3;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-dynamicinputXable" ) )
	    {
	    if ( argc <= i+1 )
		{
		return "Missing max. tolerated count for missing number of input events specifier.";
		}
	    fDynamicInputXablingNoEventCntLimit = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		return "Error converting max. tolerated count for missing number of input events specifier..";
		}
	    i += 2;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-etsmergemode" ) )
	    {
	    if ( argc <= i+1 )
		{
		return "Missing event ETS merge mode specifier.";
		}
	    if ( !strcmp( argv[i+1], "append" ) )
		fETSMergeMode = AliHLTEventMergerNew::kAppendETS;
	    else if ( !strcmp( argv[i+1], "hlt" ) )
		fETSMergeMode = AliHLTEventMergerNew::kHLTETS;
	    else
		return "Unknown event ETS merge mode specifier.";
	    i += 2;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-subscriberinputddllist" ) )
	    {
	    if ( argc <= i+1 )
		{
		return "Missing subscriber ID and subscriber DDL list specifier.";
		}
	    if ( argc <= i+2 )
		{
		return "Missing subscriber DDL list specifier.";
		}
	    fSubscriberInputDDLSpecifications.insert( std::pair<MLUCString,MLUCString>( MLUCString(argv[i+1]), MLUCString(argv[i+2]) ) );
#if 0
	    typename vector<SubscriberData>::iterator iter, end;
	    iter = fSubscribers.begin();
	    end = fSubscribers.end();
	    while ( iter != end )
		{
		if ( iter->fSubscriberID==argv[i+1] )
		    {
		    if ( !iter->fContributingDDLs )
			iter->fContributingDDLs = new hltreadout_uint32_t[READOUTLIST_MAX_WORDCOUNT+1];
		    if ( !iter->fContributingDDLs )
			return "Out of memory allocating subscriber DDL list";
		    AliHLTReadoutList rdLst(gReadoutListVersion,iter->fContributingDDLs);
		    rdLst.Reset();
		    if ( !rdLst.ParseDDLList( argv[i+2] ) )
			{
			errorArg = i+1;
			return "Error parsing subscriber DDL list (must be comma separated list of valid DDL IDs";
			}
		    break;
		    }
		iter++;
		}
	    if ( iter==end )
		{
		errorArg = i+1;
		return "Subscriber not found.";
		}
#endif
	    i += 3;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-subscriberinputmodulo" ) )
	    {
	    if ( argc <= i+1 )
		{
		return "Missing subscriber ID and subscriber event modulo specifier.";
		}
	    if ( argc <= i+2 )
		{
		return "Missing subscriber event modulo specifier.";
		}
	    typename vector<SubscriberData>::iterator iter, end;
	    iter = fSubscribers.begin();
	    end = fSubscribers.end();
	    while ( iter != end )
		{
		if ( iter->fSubscriberID==argv[i+1] )
		    {
		    unsigned long modulo = strtoul( argv[i+2], &cpErr, 0 );
		    if ( *cpErr!='\0' )
			{
			errorArg = i+2;
			return "Error parsing subscriber event modulo specifier";
			}
		    iter->fSubscriberInputModulos.push_back( modulo );
		    break;
		    }
		iter++;
		}
	    if ( iter==end )
		{
		errorArg = i+1;
		return "Subscriber not found.";
		}
	    i += 3;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-subscriberinputeventtypecode" ) )
	    {
	    if ( argc <= i+1 )
		{
		return "Missing subscriber ID and subscriber event type code specifier.";
		}
	    if ( argc <= i+2 )
		{
		return "Missing subscriber event type code specifier.";
		}
	    typename vector<SubscriberData>::iterator iter, end;
	    iter = fSubscribers.begin();
	    end = fSubscribers.end();
	    while ( iter != end )
		{
		if ( iter->fSubscriberID==argv[i+1] )
		    {
		    iter->fSubscriberInputEventTypeStackMachineCodesText.push_back( argv[i+2] );
		    break;
		    }
		iter++;
		}
	    if ( iter==end )
		{
		errorArg = i+1;
		return "Subscriber not found.";
		}
	    i += 3;
	    return NULL;
	    }
#if 0
	if ( !strcmp( argv[i], "-triggerclassddllist" ) )
	    {
	    if ( argc <= i+1 )
		{
		return "Missing trigger class and DDL list specifier.";
		}
	    if ( argc <= i+2 )
		{
		return "Missing trigger class DDL list specifier.";
		}
	    char* cpErr=NULL;
	    unsigned long long triggerClass = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr!='\0' )
		{
		errorArg = i+1;
		return "Error parsing trigger class specifier";
		}
	    unsigned long ndx=0, lastNdx=0;
	    unsigned cnt=0;
	    while ( ndx<sizeof(triggerClass)*8 )
		{
		if ( triggerClass & (1ULL<<ndx) )
		    {
		    ++cnt;
		    lastNdx = ndx;
		    }
		++ndx;
		}
	    if ( cnt<=0 )
		{
		errorArg = i+1;
		return "No bit set in trigger class specifier";
		}
	    if ( cnt>1 )
		{
		errorArg = i+1;
		return "More than one bit set in trigger class specifier";
		}
	    ndx = lastNdx;
	    
	    MLUCMutex::TLocker ddlListLocker( fTriggerClassContributingDDLsMutex );
	    if ( fTriggerClassContributingDDLs.size()<ndx+1 )
		fTriggerClassContributingDDLs.resize(ndx+1, NULL );
	    if ( !fTriggerClassContributingDDLs[ndx] )
		fTriggerClassContributingDDLs[ndx] = new hltreadout_uint32_t[READOUTLIST_MAX_WORDCOUNT+1];
	    if ( !fTriggerClassContributingDDLs[ndx] )
		return "Out of memory allocating trigger class DDL list";

	    AliHLTReadoutList rdLst(gReadoutListVersion,(volatile_hltreadout_uint32_t*)(fTriggerClassContributingDDLs[ndx]));
	    rdLst.Reset();
	    if ( !rdLst.ParseDDLList( argv[i+2] ) )
		{
		errorArg = i+1;
		return "Error parsing trigger class DDL list (must be comma separated list of valid DDL IDs";
		}
	    
	    i += 3;
	    return NULL;
	    }
#endif
	}
    if ( fComponentType==kScatterer )
	{
	if ( !strcmp( argv[i], "-scattererparam" ) )
	    {
	    if ( argc <= i+1 )
		{
		return "Missing scatterer parameter specifier.";
		}
	    fScattererParam = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		return "Error converting scatterer parameter specifier..";
		}
	    fScattererParamSet = true;
	    i += 2;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-seqnrdistribution" ) )
	    {
	    fEventIDBased = false;
	    i += 1;
	    return NULL;
	    }
	}
    if ( fComponentType==kGatherer )
	{
	if ( !strcmp( argv[i], "-broadcasteventtimeout" ) )
	    {
	    if ( argc <= i+1 )
		{
		return "Missing broadcast event timeout specifier.";
		}
	    fGathererBroadcastTimeout_ms = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		return "Error converting broadcast event timeout specifier.";
		}
	    i += 2;
	    return NULL;
	    }
	if ( !strcmp( argv[i], "-soreventtimeout" ) )
	    {
	    if ( argc <= i+1 )
		{
		return "Missing SOR event timeout specifier.";
		}
	    fGathererSORTimeout_ms = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		return "Error converting SOR event timeout specifier.";
		}
	    i += 2;
	    return NULL;
	    }
	}
    if ( !strcmp( argv[i], "-alicehlt" ) )
	{
	fAliceHLT = true;
	fETSMergeMode = AliHLTEventMergerNew::kHLTETS;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventaccounting" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing event accounting output file directory.";
	    }
	fEventAccounting = new AliHLTEventAccounting;
	fEventAccounting->SetDirectory( argv[i+1] );
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-nostrictsor" ) )
	{
	fStrictSORRequirements = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-strictsor" ) )
	{
	fStrictSORRequirements = true;
	i++;
	return NULL;
	}

    return "Unknown Parameter";
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::PrintUsage( const char* errorStr, int errorArg, int errorParam, int argc, char** argv )
    {
    if ( argc==-1 )
	argc = fArgc;
    if ( argv==NULL )
	argv = fArgv;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Usage" )
	<< "Error parsing command line arguments: " << errorStr << "." << ENDLOG;
    if ( errorArg>=0 && errorArg<argc )
	{
	LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Usage" )
	    << "Offending argument: " << argv[errorArg] << "." << ENDLOG;
	}
    if ( errorParam>=0 && errorParam<argc )
	{
	LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Usage" )
	    << "Offending parameter: " << argv[errorParam] << "." << ENDLOG;
	}
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-control <status&control address URL>: Specify the address on which the Status&Control listens for remote supervision." << ENDLOG;
    if ( GetMaxPublisherCount()>0 )
	{
	const char *multi1, *multi2;
	if ( GetMinPublisherCount()==0 )
	    multi1 = "Optional";
	else
	    multi1 = "Mandatory";
	if ( GetMaxPublisherCount()>1 )
	    multi2 = ", may be used multiple times";
	else
	    multi2 = "";
	LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	    << "-publisher <publisher-id>: Specifies the name/id to use for the publisher object. ("
	    << multi1 << multi2 << ")" << ENDLOG;
	}
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-V <verbosity>: Specifies the verbosity to use in the program. Set bits correspond to: 1 - benchmark, 2 - information, 4 - debug, 8 - warning, 16 - error, 32 - fatal error. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-nostdout: Inihibit logging to stdout. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-nofilelog: Inihibit logging to files. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-syslog <facility>: Send log messages to the syslog daemon. 'facility' can be one of the following values: DAEMON,USER,LOCAL0,LOCAL1,LOCAL2,LOCAL3,LOCAL4,LOCAL5,LOCAL6,LOCAL7. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-sysmeslog: Send log messages to the SysMES system. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-infologger <path-to-infologger-logserver-shared-library>: Send log messages to the infoLogger system via the given shared library. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-publishertimeout <publisher-timeout-ms>: Specifies a timeout to use in milliseconds for published events. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-eventmodulopredivisor <event modulo pre-divisor>: Specify a number by which the event ID/number will be divided before the event modulo is calculated ( (event-ID-or-Number / pre-divisor) % event-modulo). (Optional, default 1)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-eventmodulo <event-modulo-count>: Request an event modulo count for subscription (receive only every n'th event). (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-eventtypecode <trigger-stack-machine-assembler-code>: Specify some assembler code to be executed in the parent publisher for each event to determine whether this subscriber wants to receive the event. (Optionla, only available in ALICE HLT mode)" << ENDLOG;
    if ( GetMaxSubscriberCount()>0 )
	{
	const char *multi1, *multi2;
	if ( GetMinSubscriberCount()==0 )
	    multi1 = "Optional";
	else
	    multi1 = "Mandatory";
	if ( GetMaxSubscriberCount()>1 )
	    multi2 = ", may be used multiple times";
	else
	    multi2 = "";
	LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	    << "-subscribe <publishername> <subscriberid>: Specifies the name of the publisher to subscribe to and the subscriber id to use. ("
	    << multi1 << multi2 << ")" << ENDLOG;
	}
//     LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
// 	<< "-shmproxy <shmsize> <publisher-to-subscriber-shmkey> <subscriber-to-publisher-shmkey>: Use shared memory proxies for publisher subscriber communication using the given parameters. (Optional, default is named pipe communication)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-subscriberretrycount <max-retry-count>: Specify the maximum number of retries that new events are tried to be processed. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-subscriberretrytime <retry-time-in-milliseconds>: Specify the amount of time (in milliseconds) between of retries for new events to be processed. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-eventsexp2 <event-count-power-of-2>: Specify the power of two for the number of event slots to pre-allocate. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-lamaddress <lam-target-address-URL>: Specify an address to which Look-At-Me (LAM) requests (interrupts) will be sent. (Optional, may be used multiple times)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-subscribeoldevents: Request old events already present in publisher when starting processing. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-nosubscribeoldevents: Do not request old events already present in publisher when starting processing. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-runnr <run-number>: Set the run number to be used. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-beamtype [pp|AA]: Set the type of beam, either pp or AA. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-hltmode [A|B|C|D|E]: Set the HLT mode for this run, A, B, C, D, or E. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-runtype <run-type-string>: Set the type of run. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-readoutlistversion <readout-list-data-format-version-number>: Set the version number of the data format for the readout list data. (Optional)" << ENDLOG;
    if ( fComponentType==kMerger )
	{
	LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	    << "-eventtimeout <event-timeout-ms>: Specify event timeout (in milliseconds) when partially assembled events will be transmitted anyway. (Optional)" << ENDLOG;
	LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	    << "-eventtimeoutshift <event-timeout-shift-ms>: Specify event timeout shift (in milliseconds) when partially assembled events will be transmitted anyway. (Optional)" << ENDLOG;
	LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	    << "-dynamictimeouts <min.-event-timeout-ms> <max.-event-timeout-ms>: Enable dynamic event timeout calculation and specify the absolute minimum and maximum allowed timeout (in milliseconds) when partially assembled events will be transmitted anyway. (Optional)" << ENDLOG;
	LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	    << "-dynamicinputXable <max.-missing-event-count>: Enable dynamic dis-/enabling of subscriber inputs if too many events did not arrive on a specific input channel, give the maximum number of events which are allowed to be missing in a row on a given input until it is disabled. (Optional)" << ENDLOG;
	LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	    << "-etsmergemode [append | hlt ]: Specify the mode how event trigger structures will be merged. append appends data from all contributing sub events, hlt performs an or on some HLT specific fields. (Optional, default append)" << ENDLOG;
	LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	    << "-subscriberinputmodulo <subscriber ID> <event-modulo-for-subscriber>: Specify the modulos of events for which the given subscriber input can contribute. (Optional, can be specified multiple times)." << ENDLOG;
	LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	    << "-subscriberinputeventtypecode <subscriber ID> <event-type-code-for-subscriber>: Specify event type code which determines for which events the given subscriber input can contribute. (Optional, can be specified multiple times, only in special ALICE HLT mode)." << ENDLOG;
	LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	    << "-subscriberinputddllist <subscriber ID> <ddl-list-string>: Specify the IDs of DDLs which contribute to the given subscriber input. The DDL list must be a comma separated list of valid DDL IDs (ranges (e.g. 0-3) are also allowed) (Optional, only in special ALICE HLT mode)." << ENDLOG;
#if 0
	LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	    << "-triggerclassddllist <trigger class> <ddl-list-string>: Specify the IDs of DDLs which contribute to events of the given trigger class. The DDL list must be a comma separated list of valid DDL IDs (ranges (e.g. 0-3) are also allowed) (Optional, only in special ALICE HLT mode)." << ENDLOG;
#endif
	}
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-ctptriggerclasses <CTP-Trigger-Class-String>: Specify the ECS string containing the trigger class specification to the component. (Optional, only in special ALICE HLT mode)." << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-ctptriggerclassgroup <CTP-Trigger-Class-Group-Identifier> <CTP-Trigger-Class-Group-RegEx-Match-Specification>: Specify a group of trigger classes/bits, identifieed by a common identifier, that are specified by a regex applied to the trigger class identifier. (Optional, only in special ALICE HLT mode)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-alicehlt: Activate some special features and options useful only for the ALICE HLT. (Optional)" << ENDLOG;
    if ( fComponentType==kScatterer )
	{
	LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	    << "-scattererparam <scatterer parameter>: Set a scatterer parameter (number) that influences how the scatterer distributes events. Exact influence depends on scatterer type. (Optional)" << ENDLOG;
	LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	    << "-seqnrdistribution: Distribute the events based on the event sequence number and not based on the event ID. (Optional)" << ENDLOG;
	}
    if ( fComponentType==kGatherer )
	{
	LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	    << "-broadcasteventtimeout <broadcast event timeout ms>: Set the timeout for broadcast events in milliseconds (Optional, 0 disables (default))" << ENDLOG;
	LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	    << "-soreventtimeout <SOR event timeout ms>: Set the timeout for Start-Of-Run events in milliseconds (Optional, 0 disables (default))" << ENDLOG;
	}
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-nostrictsor: Do not be absolutely strict about requiring Start-of-Run/Data event before any data event. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Data Flow Component", "Command line usage" )
	<< "-strictsor: Be strict about requiring Start-of-Run/Data event before any data event. (Optional, default)" << ENDLOG;
    }
	
template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::IllegalCmd( AliUInt32_t state, AliUInt32_t cmd )
    {
    MLUCString cmdStr, stateStr;
    AliHLTSCComponentInterface::GetCommandName( cmd, cmdStr );
    AliHLTSCComponentInterface::GetStatusName( state, stateStr );
    LOG( AliHLTLog::kWarning, "AliHLTControlledDataFlowComponent::IllegalCmd", "Illegal Command" )
	<< "Illegal command '" << cmdStr.c_str() << "' 0x" << AliHLTLog::kHex << cmd << "/" << AliHLTLog::kDec
	<< cmd << ") for state '" << stateStr.c_str() << "' (0x" << AliHLTLog::kHex << state << "/" << AliHLTLog::kDec
	<< state << ")." << ENDLOG;
    AliHLTStackTrace( "Illegal Command", AliHLTLog::kWarning, 4 );
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SetupSC()
    {
    fStatus = CreateStatusData();
    if ( !fStatus )
	return false;
    if ( !SetupStatusData() )
	return false;

    fSCInterface = CreateSCInterface();
    if ( !fSCInterface )
	return false;
    if ( !SetupSCInterface() )
	return false;

    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE AliHLTStatus* AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CreateStatusData()
    {
    return new AliHLTStatus;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SetupStatusData()
    {
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    fStatus->fProcessStatus.fState = 0;
    fStatus->fProcessStatus.fState |= kAliHLTPCStartedStateFlag;
//     fStatus->fProcessStatus.fState = kAliHLTPCStartedState;
    switch ( fComponentType )
	{
	case kMerger:
	    fStatus->fProcessStatus.fProcessType = kAliHLTSCEventMergerComponentType;
	    break;
	case kScatterer:
	    fStatus->fProcessStatus.fProcessType = kAliHLTSCEventScattererComponentType;
	    break;
	case kGatherer:
	    fStatus->fProcessStatus.fProcessType = kAliHLTSCEventGathererComponentType;
	    break;
	case kPublisherBridgeHead:
	    fStatus->fProcessStatus.fProcessType = kAliHLTSCControlledBridgeComponentType;
	    break;
	case kSubscriberBridgeHead:
	    fStatus->fProcessStatus.fProcessType = kAliHLTSCControlledBridgeComponentType;
	    break;
	default:
	    fStatus->fProcessStatus.fProcessType = kAliHLTSCUndefinedComponentType;
	    break;
	}
    stateLock.Unlock();
    fStatus->Touch();
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE AliHLTSCInterface* AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CreateSCInterface()
    {
    return new AliHLTSCInterface();
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SetupSCInterface()
    {
    int ret;
    ret = fSCInterface->Bind( fSCInterfaceAddress.c_str() );
    if ( ret )
	{
	return false;
	}
    ret = fSCInterface->Start();
    if ( ret )
	{
	return false;
	}
    fSCInterface->SetStatusData( &(fStatus->fHeader) );
    fSCInterface->AddCommandProcessor( &fCmdProcessor );
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::RemoveSC()
    {
    if ( fSCInterface )
	{
	fSCInterface->Stop();
	fSCInterface->Unbind();
	delete fSCInterface;
	fSCInterface = NULL;
	}
    if ( fStatus )
	{
	delete fStatus;
	fStatus = NULL;
	}
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::LookAtMe()
    {
    typename vector<LAMAddressData>::iterator lamIter, lamEnd;
    int ret;
    pthread_mutex_lock( &fLAMAddressMutex );
    lamIter = fLAMAddresses.begin();
    lamEnd = fLAMAddresses.end();
    while ( lamIter != lamEnd )
	{
	if ( lamIter->fAddress )
	    {
	    ret = fSCInterface->LookAtMe( lamIter->fAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::LookAtMe", "Error sending LAM request" )
		    << "Error sending LAM request to LAM address '" << lamIter->fStr.c_str() << ": "
		    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
		}
	    }
	lamIter++;
	}
    pthread_mutex_unlock( &fLAMAddressMutex );
    }


template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::ProcessSCCmd( AliHLTSCCommandStruct* cmdS )
    {
    bool ok = false;
    AliHLTSCCommand cmd( cmdS );
    if ( !fStatus )
	{
	// XXX
	return;
	}
    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
    switch ( cmd.GetData()->fCmd )
	{
	case kAliHLTPCNOPCmd:
	    ok = true;
	    break;
	case kAliHLTPCEndProgramCmd:
	    if ( !(fStatus->fProcessStatus.fState & ~(kAliHLTPCStartedStateFlag|kAliHLTPCErrorStateFlag)) )
		{
		ok = true;
		fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
		fCmdSignal.Signal();
		}
	    break;
	case kAliHLTPCConfigureCmd:
	    if ( fStatus->fProcessStatus.fState & ~kAliHLTPCStartedStateFlag )
		break;
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag )
		{
		ok = true;
		break;
		}
	    ok = true;
	    stateLock.Unlock();
	    ExtractConfig( cmdS );
	    Configure();
	    stateLock.Lock();
	    break;
	case kAliHLTPCUnconfigureCmd:
	    if ( fStatus->fProcessStatus.fState & ~(kAliHLTPCStartedStateFlag|kAliHLTPCConfiguredStateFlag|kAliHLTPCErrorStateFlag) )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
		{
		ok = true;
		break;
		}
	    ok = true;
	    stateLock.Unlock();
	    Unconfigure();
	    stateLock.Lock();
	    fStatus->fProcessStatus.fState = fStatus->fProcessStatus.fState & ~(kAliHLTPCConfiguredStateFlag|kAliHLTPCErrorStateFlag);
	    break;
	case kAliHLTPCResetConfigurationCmd:
	    if ( fStatus->fProcessStatus.fState & ~kAliHLTPCStartedStateFlag )
		break;
	    ok = true;
	    stateLock.Unlock();
	    ResetConfiguration();
	    stateLock.Lock();
	    break;
	case kAliHLTPCAcceptSubscriptionsCmd:
	    if ( GetMinPublisherCount()<=0 )
		break;
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag )
		{
		ok = true;
		break;
		}
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) ||
		 !(fStatus->fProcessStatus.fState & kAliHLTPCStartedStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCStopSubscriptionsCmd:
	    if ( GetMinPublisherCount()<=0 )
		break;
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) )
		{
		ok = true;
		break;
		}
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCCancelSubscriptionsCmd:
	    if ( GetMinPublisherCount()<=0 )
		break;
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) ||
		 (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCSubscribeCmd:
	    if ( GetMinSubscriberCount()<=0 )
		break;
// 	    if ( fStatus->fProcessStatus.fState & ~(kAliHLTPCStartedStateFlag|kAliHLTPCConfiguredStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag|kAliHLTPCWithSubscribersState) )
// 		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) ||
		 (fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		break;
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) ||
		 (fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) )
		{
		ok = true;
		break;
		}
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCUnsubscribeCmd:
	    if ( GetMinSubscriberCount()<=0 )
		break;
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) )
		{
		ok = true;
		break;
		}
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCStartProcessingCmd:
	    {
	    if ( GetMinPublisherCount()>0 )
		{
		if ( (fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) &&
		     !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		    ok = true;
		}
	    if ( GetMinSubscriberCount()>0 )
		{
		if ( (fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) &&
		     !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		    ok = true;
		}
	    if ( ok )
		{
		if ( fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag )
		    break;
		fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
		fCmdSignal.Signal();
		}
	    break;
	    }
	case kAliHLTPCPauseProcessingCmd:
// 	    if ( fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag )
// 		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCResumeProcessingCmd:
// 		if ( fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag )
// 		    break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCStopProcessingCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		{
		ok = true;
		break;
		}
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCPublisherPinEventsCmd:
	    if ( GetMinPublisherCount()<=0 )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCPublisherUnpinEventsCmd:
	    if ( GetMinPublisherCount()<=0 )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCRemoveSubscriberCmd:
	    {
	    if ( GetMinPublisherCount()<=0 )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) && 
		 !(fStatus->fProcessStatus.fState & kAliHLTPCWithSubscribersStateFlag) )
		break;
	    stateLock.Unlock();
	    ok = true;
	    SubscriberRemovalData tmp( (char*)cmdS->fData, cmdS->fParam0 );
	    pthread_mutex_lock( &fSubscriberRemovalListMutex );
	    fSubscriberRemovalList.insert( fSubscriberRemovalList.end(), tmp );
	    pthread_mutex_unlock( &fSubscriberRemovalListMutex );
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	    }
	case kAliHLTPCSimulateErrorCmd:
	    if ( !(fStatus->fProcessStatus.fState & ~kAliHLTPCStartedStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCDumpEventAccountingCmd:
	    stateLock.Unlock();
	    if ( fEventAccounting )
		fEventAccounting->DumpEvents();
	    ok = true;
	    break;
	case kAliHLTPCPURGEALLEVENTSCmd:
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCSetSubsystemVerbosityCmd:
	    ok = true;
	    // Not supported/applicable here. Ignore
	    break;
	case kAliHLTPCDumpEventMetaDataCmd:
	    stateLock.Unlock();
	    fBaseObject->DumpEventMetaData();
	    stateLock.Lock();
	    ok = true;
	    break;
	}
    if ( !ok )
	IllegalCmd( fStatus->fProcessStatus.fState, cmd.GetData()->fCmd );
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::ExtractConfig( AliHLTSCCommandStruct* cmdS )
    {
    char* config = (char*)cmdS->fData;
    if ( strlen(config)<=0 )
	return;
    MLUCCmdlineParser parser;
    vector<MLUCString> tokens;
    if ( !parser.ParseCmd( config, tokens ) )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::ExtractConfig", "Error parsing configuration string" )
	    << "Error parsing configuration string '" << config << "'." << ENDLOG;
	return;
	}
    int argc = tokens.size();
    if ( argc<=0 )
	return;
    char** argv;
    argv = new char*[ argc+1 ];
    if ( !argv )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::ExtractConfig", "Out of memory" )
	    << "Out of memory allocating temporary buffer of " << AliHLTLog::kDec
	    << argc*sizeof(char*) << " bytes." << ENDLOG;
	return;
	}
    int i, errorArg=-1;
    const char* errorStr = NULL;

    for ( i = 0; i < argc; i++ )
	argv[i] = (char*)tokens[i].c_str();
    argv[argc] = NULL;
    
    i = 0;
    while ( (i < argc) && !errorStr )
	{
	errorStr = ParseCmdLine( argc, argv, i, errorArg );
	}
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::ExtractConfig", "Error extracting configuration from configuration string" )
	    << "Error extracting configuration from configuration string '" << config << "'." << ENDLOG;
	PrintUsage( errorStr, i, errorArg, argc, argv );
	return;
	}
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE AliHLTDescriptorHandler* AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CreateDescriptorHandler()
    {
//     return new AliHLTDescriptorHandler( fMinPreBlockCount, fMaxPreBlockCount, fMaxPreEventCountExp2 );
#if 0
#warning Growing of descriptor caches disabled for debugging reasons
    return new AliHLTDescriptorHandler( (fMaxPreEventCountExp2>=0 ? fMaxPreEventCountExp2 : 4), false );
#else
    return new AliHLTDescriptorHandler( (fMaxPreEventCountExp2>=0 ? fMaxPreEventCountExp2 : 4) );
#endif
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE AliHLTShmManager* AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CreateShmManager()
    {
    return new AliHLTShmManager( fMaxNoUseCount );
    }


template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE typename AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SubscriptionListenerThread* AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CreateSubscriptionListenerThread( unsigned long, TSelf::PublisherData* pd )
    {
    return new SubscriptionListenerThread( this, pd->fPublisher );
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE AliHLTPublisherProxyInterface* AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CreatePublisherProxy( unsigned long, TSelf::SubscriberData* sd )
    {
//     if ( fProxyShmSize )
// 	return new AliHLTPublisherShmProxy( fAttachedPublisherName.c_str(), fProxyShmSize, fPub2SubProxyShmKey, fSub2PubProxyShmKey );
//     else
    return new AliHLTPublisherPipeProxy( sd->fAttachedPublisherName.c_str() );
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE typename AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SubscriptionThread* AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CreateSubscriptionThread( unsigned long, TSelf::SubscriberData* sd )
    {
    return new SubscriptionThread( this, sd->fPublisherProxy, sd->fSubscriber );
    }
	
template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SetupComponents()
    {
    if ( fRunNumber )
	fBaseObject->SetRunNumber( fRunNumber );
    if ( fAliceHLT )
	fBaseObject->AliceHLT();
    if ( fStrictSORRequirements )
	fBaseObject->StrictSORRequirements();
    else
	fBaseObject->NoStrictSORRequirements();
    fBaseObject->SetEventModuloPreDivisor( fEventModuloPreDivisor );
    typename vector<PublisherData>::iterator pubIter, pubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    while ( pubIter != pubEnd )
	{
	pubIter->fPublisher->SetMaxTimeout( fPublisherTimeout );
	pubIter->fPublisher->SetSubscriptionLoop( &PublisherPipeSubscriptionInputLoop );
	pubIter->fPublisher->AddSubscriberEventCallback( &fSubscriberEventCallback );
	pubIter->fPublisher->SetEventModuloPreDivisor( fEventModuloPreDivisor );
	if ( fAliceHLT )
	    pubIter->fPublisher->SetEventTriggerTypeCompareFunc( StackMachineCompareEventTrigger, fTriggerStackMachine );
	if ( fEventAccounting )
	    pubIter->fPublisher->SetEventAccounting( fEventAccounting );
	pubIter++;
	}
    fSubscriptionStatusData.fRequiredSubscriptions = fPublishers.size();
    pthread_mutex_unlock( &fPublisherMutex );

    typename vector<SubscriberData>::iterator subIter, subEnd;
    pthread_mutex_lock( &fSubscriberMutex );
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    while ( subIter != subEnd )
	{
	if ( fEventAccounting )
	    subIter->fPublisherProxy->SetEventAccounting( fEventAccounting );
#ifdef USE_PIPE_TIMEOUT
	subIter->fPublisherProxy->SetTimeout( 500000 );    
#endif
	subIter++;
	}
    pthread_mutex_unlock( &fSubscriberMutex );
    if ( fEventAccounting )
	{
	if ( fShmManager )
	    fEventAccounting->SetShmManager( fShmManager );
	fEventAccounting->SetName( fComponentName );
	fEventAccounting->SetRun( fRunNumber );
	if ( fBaseObject )
	    fBaseObject->SetEventAccounting( fEventAccounting );
	}

    if ( fAliceHLT )
	{
	std::map<MLUCString,MLUCString>::iterator tcgIter = fTriggerClassGroups.begin(), tcgEnd = fTriggerClassGroups.end();
	MLUCRegEx regEx;
	while ( tcgIter != tcgEnd )
	    {
	    regEx.SetPattern( tcgIter->second.c_str(), MLUCRegEx::kPFExtended );
	    AliUInt64_t bits = 0;
	    std::map<MLUCString,unsigned>::iterator tcbiIter = fTriggerClassBitIndices.begin(), tcbiEnd = fTriggerClassBitIndices.end();
	    while ( tcbiIter != tcbiEnd )
		{
		if ( regEx.Search( tcbiIter->first.c_str() ) )
		    bits |= 1 << tcbiIter->second;
		++tcbiIter;
		}
	    fStackMachineAssembler.AddSymbol( tcgIter->first.c_str(), bits );
	    ++tcgIter;
	    }

	typename vector<SubscriberData>::iterator subIter, subEnd;
	pthread_mutex_lock( &fSubscriberMutex );
	subIter = fSubscribers.begin();
	subEnd = fSubscribers.end();
	while ( subIter != subEnd )
	    {
	    std::vector<MLUCString>::iterator codeIter = subIter->fEventTypeStackMachineCodesText.begin(), codeEnd = subIter->fEventTypeStackMachineCodesText.end();
	    MLUCSimpleStackMachineAssembler::TError smaErr;
	    while ( codeIter != codeEnd )
		{
		smaErr = fStackMachineAssembler.Assemble( *codeIter );
		//smaErr = ((MLUCSimpleStackMachineAssembler&)fStackMachineAssembler).Assemble( argv[i+1] );
		if ( !smaErr.Ok() )
		    {
		    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::SetupComponents", "Stack Machine Assembler Error" )
			<< "Error returned by trigger stack machine assembler: " << smaErr.Description() << ENDLOG;
		    pthread_mutex_unlock( &fSubscriberMutex );
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		    return;
		    }
		unsigned long codeWordCount = fStackMachineAssembler.GetCodeWordCount();
		unsigned long totalSize = codeWordCount*sizeof(uint64)+sizeof(AliHLTEventTriggerStruct);
		AliHLTEventTriggerStruct* code = reinterpret_cast<AliHLTEventTriggerStruct*>( new uint8[ totalSize ] );
		if ( !code )
		    {
		    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::SetupComponents", "Out of memory" )
			<< "Out of memory allocating event type code for trigger stack machine." << ENDLOG;
		    pthread_mutex_unlock( &fSubscriberMutex );
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		    return;
		    }
		memcpy( &(code->fDataWords[0]), fStackMachineAssembler.GetCode(), sizeof(uint64)*codeWordCount );
		code->fHeader.fLength = totalSize;
		code->fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
		code->fHeader.fSubType.fID = 0;
		code->fHeader.fVersion = 1;
		code->fDataWordCount = (codeWordCount*sizeof(uint64))/sizeof(code->fDataWords[0]);
		subIter->fEventTypeStackMachineCodes.push_back( code );
		codeIter++;
		}
	    subIter++;
	    }
	pthread_mutex_unlock( &fSubscriberMutex );
	}


    fSubscriptionStatusData.fStatus = fStatus;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Subscribe()
    {
    typename vector<SubscriberData>::iterator subIter, subEnd;
    pthread_mutex_lock( &fSubscriberMutex );
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    while ( subIter != subEnd )
	{
	int ret;
	ret = subIter->fPublisherProxy->Subscribe( *(subIter->fSubscriber) );
	if ( ret )
	    {
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCConsumerErrorStateFlag;
	    stateLock.Unlock();
	    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Subscribe", "Error subscribing" )
		<< "Error subscribing subscriber '" << subIter->fSubscriber->GetName() << "' to publisher '"
		<< subIter->fPublisherProxy->GetName() << "': " << strerror(ret) << " ("
		<< MLUCLog::kDec << ret << ")." << ENDLOG;
	    pthread_mutex_unlock( &fSubscriberMutex );
	    stateLock.Lock();
	    fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	    return;
	    }
	subIter->fSubscriptionThread->Start();
	subIter->fPublisherProxy->SetEventType( *(subIter->fSubscriber), subIter->fEventModulo );
	if ( fAliceHLT && subIter->fEventTypeStackMachineCodes.size()>0 )
	    {
	    subIter->fPublisherProxy->SetEventType( *(subIter->fSubscriber), subIter->fEventTypeStackMachineCodes, subIter->fEventModulo );
	    }
// 	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	subIter++;
	}
    pthread_mutex_unlock( &fSubscriberMutex );
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Unsubscribe()
    {
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Unsubscribe", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    typename vector<SubscriberData>::iterator subIter, subEnd;
    pthread_mutex_lock( &fSubscriberMutex );
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    while ( subIter != subEnd )
	{
	if ( subIter->fSubscriptionThread )
	    subIter->fSubscriptionThread->Quit();
	subIter++;
	}
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Unsubscribe", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    
    
    bool notStopped = true;
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    gettimeofday( &start, NULL );
    while ( notStopped && deltaT<timeLimit )
	{
	notStopped = false;
	subIter = fSubscribers.begin();
	subEnd = fSubscribers.end();
	while ( subIter != subEnd )
	    {
	    if ( subIter->fSubscriptionThread && !subIter->fSubscriptionThread->HasQuitted() )
		{
		notStopped = true;
		break;
		}
	    subIter++;
	    }
	if ( notStopped )
	    {
	    usleep( 10000 );
	    gettimeofday( &now, NULL );
	    deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	    }
	}
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Unsubscribe", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;

    if ( notStopped )
	{
	subIter = fSubscribers.begin();
	subEnd = fSubscribers.end();
	while ( subIter != subEnd )
	    {
	    if ( subIter->fSubscriptionThread && !subIter->fSubscriptionThread->HasQuitted() )
		{
		LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::Unsubscribe", "Aborting Publishing Loop" )
		    << "Publishing loop for subscriber '" << subIter->fSubscriber->GetName() << "' and publisher '"
		    << subIter->fPublisherProxy->GetName() << "' has to be aborted." << ENDLOG;
		subIter->fSubscriptionThread->AbortPublishing();
		}
	    subIter++;
	    }
	}
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Unsubscribe", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    deltaT = 0;
    gettimeofday( &start, NULL );
    while ( notStopped && deltaT<timeLimit )
	{
	notStopped = false;
	subIter = fSubscribers.begin();
	subEnd = fSubscribers.end();
	while ( subIter != subEnd )
	    {
	    if ( subIter->fSubscriptionThread && !subIter->fSubscriptionThread->HasQuitted() )
		{
		notStopped = true;
		break;
		}
	    subIter++;
	    }
	if ( notStopped )
	    {
	    usleep( 10000 );
	    gettimeofday( &now, NULL );
	    deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	    }
	}
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Unsubscribe", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    if ( notStopped )
	{
	subIter = fSubscribers.begin();
	subEnd = fSubscribers.end();
	while ( subIter != subEnd )
	    {
	    if ( subIter->fSubscriptionThread && !subIter->fSubscriptionThread->HasQuitted() )
		{
		LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::Unsubscribe", "Aborting Subscription Thread" )
		    << "Subscription thread loop for subscriber '" << subIter->fSubscriber->GetName() << "' and publisher '"
		    << subIter->fPublisherProxy->GetName() << "' has to be aborted." << ENDLOG;
		subIter->fSubscriptionThread->Abort();
		}
	    subIter->fSubscriptionThread->Join();
	    subIter++;
	    }
	}
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Unsubscribe", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;


    pthread_mutex_unlock( &fSubscriberMutex );
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::AcceptSubscriptions()
    {
    typename vector<PublisherData>::iterator pubIter, pubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    while ( pubIter != pubEnd )
	{
	if ( pubIter->fSubscriptionListenerThread )
	    pubIter->fSubscriptionListenerThread->Start();
	else
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::AcceptSubscriptions", "No subscription thread" )
		<< "Publisher '" << pubIter->fOwnPublisherName.c_str() << "' has no subscription thread."
		<< ENDLOG;
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    break;
	    }
	pubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::StopSubscriptions()
    {
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::StopSubscriptions", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    typename vector<PublisherData>::iterator pubIter, pubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    while ( pubIter != pubEnd )
	{
	if ( pubIter->fSubscriptionListenerThread )
	    {
	    pubIter->fSubscriptionListenerThread->Quit();
	    }
	pubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::StopSubscriptions", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CancelSubscriptions()
    {
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CancelSubscriptions", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    typename vector<PublisherData>::iterator pubIter, pubEnd;
    PublisherClass* nextPublisher = NULL;
    pthread_mutex_lock( &fPublisherMutex );
    bool pubFound=false;
    do
	{
	// Complex loop construct because
	// a) the muted has to be released before the call to CancelAllSubscriptions
	// b) We must make sure, all publishers are reached
	pubIter = fPublishers.begin();
	nextPublisher = pubIter->fPublisher;
	pubEnd = fPublishers.end();
	while ( pubIter != pubEnd )
	    {
	    pubFound=false;
	    if ( pubIter->fPublisher==nextPublisher && nextPublisher )
		{
		nextPublisher=NULL;
		unsigned long n=1;
		while ( pubIter+n!=pubEnd && (pubIter+n)->fPublisher==NULL )
		    n++;
		if ( pubIter+n!=pubEnd )
		    nextPublisher = (pubIter+n)->fPublisher;
		if ( pubIter->fPublisher )
		    {
		    pthread_mutex_unlock( &fPublisherMutex );
		    pubFound=true;
		    pubIter->fPublisher->CancelAllSubscriptions( true );
		    pthread_mutex_lock( &fPublisherMutex );
		    }
		pubIter = fPublishers.begin();
		}
	    else
		pubIter++;
	    }
	}
    while ( !pubFound && nextPublisher ); // Can happen if publisher == pubNext is removed during the call to CancelAllSubscriptions...
    pthread_mutex_unlock( &fPublisherMutex );
    TRACELOG(AliHLTLog::kDebug, "AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CancelSubscriptions", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
    }


template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Subscription( AliHLTSamplePublisher* pub )
    {
    typename vector<PublisherData>::iterator pubIter, pubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    while ( pubIter != pubEnd )
	{
	if ( pubIter->fPublisher == pub )
	    {
	    pubIter->fSubscriberCount++;
	    break;
	    }
	pubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCWithSubscribersStateFlag;    
	}
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Unsubscription( AliHLTSamplePublisher* pub )
    {
    bool found=false;
    typename vector<PublisherData>::iterator pubIter, pubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    while ( pubIter != pubEnd )
	{
	if ( pubIter->fPublisher == pub )
	    pubIter->fSubscriberCount--;
	if ( pubIter->fSubscriberCount )
	    found = true;
	pubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    if ( !found && fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCWithSubscribersStateFlag;    
	}
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::PURGEALLEVENTS()
    {
    if ( fBaseObject )
	fBaseObject->PURGEALLEVENTS();
    typename vector<PublisherData>::iterator pubIter, pubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    while ( pubIter != pubEnd )
	{
	if ( pubIter->fPublisher )
	    pubIter->fPublisher->AbortAllEvents( false, false );
	pubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    }


template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SubscriptionListenerThread::Run()
    {
    fQuitted = false;
    bool found=false;
    if ( fPub )
	{
	typename vector<PublisherData>::iterator pubIter, pubEnd;
	pthread_mutex_lock( &fComp->fPublisherMutex );
	pubIter = fComp->fPublishers.begin();
	pubEnd = fComp->fPublishers.end();
	while ( pubIter != pubEnd )
	    {
	    if ( pubIter->fPublisher == fPub )
		pubIter->fSubscriptionListenerLoopRunning = true;
	    if ( !pubIter->fSubscriptionListenerLoopRunning )
		found = true;
	    pubIter++;
	    }
	pthread_mutex_unlock( &fComp->fPublisherMutex );

#if 0
	if ( !found )
	    {
	    MLUCMutex::TLocker stateLock( fComp->fStatus->fStateMutex );
	    fComp->fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	    }
#endif
	if ( fPub->AcceptSubscriptions( &AcceptingSubscriptionsCallbackFunc, (void*)&(fComp->fSubscriptionStatusData) ) )
	    {
	    MLUCMutex::TLocker stateLock( fComp->fStatus->fStateMutex );
	    fComp->fStatus->fProcessStatus.fState |= kAliHLTPCProducerErrorStateFlag;
	    }
	}
    fQuitted = true;
    }
		
template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SubscriptionThread::Run()
    {
    fQuitted = false;
    bool found=false;
    if ( fProxy && fSubscriber )
	{
	typename vector<SubscriberData>::iterator subIter, subEnd;
	pthread_mutex_lock( &fComp->fSubscriberMutex );
	subIter = fComp->fSubscribers.begin();
	subEnd = fComp->fSubscribers.end();
	while ( subIter != subEnd )
	    {
	    if ( subIter->fSubscriber == fSubscriber )
		{
		subIter->fSubscriptionLoopRunning = true;
		}
	    if ( !subIter->fSubscriptionLoopRunning )
		{
		found = true;
		}
	    subIter++;
	    }
	pthread_mutex_unlock( &fComp->fSubscriberMutex );


	if ( !found )
	    {
	    MLUCMutex::TLocker stateLock( fComp->fStatus->fStateMutex );
	    fComp->fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	    }
	int ret = fProxy->StartPublishing( *fSubscriber, fComp->fSubscriptionRequestOldEvents );

	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::SubscriptionThread::Run", "Subscription Error" )
		<< "Error from proxy's start publishing loop: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    MLUCMutex::TLocker stateLock( fComp->fStatus->fStateMutex );
	    fComp->fStatus->fProcessStatus.fState |= kAliHLTPCConsumerErrorStateFlag;
	    }

// 	// XXX Error detection has to be more elaborate...
	
// 	if ( !(fComp->fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) )
// 	    fComp->fStatus->fProcessStatus.fState |= kAliHLTPCConsumerErrorStateFlag;
	}
    fQuitted = true;
    }




/***************************************************************************/







AliHLTControlledMerger::AliHLTControlledMerger( const char* compName, int argc, char** argv):
    TParent( TParent::kMerger, compName, argc, argv )
    {
    }

AliHLTControlledMerger::~AliHLTControlledMerger()
    {
    }

bool AliHLTControlledMerger::HasBaseObject()
    {
    return true;
    }

unsigned long AliHLTControlledMerger::GetMinSubscriberCount()
    {
    return 1;
    }

unsigned long AliHLTControlledMerger::GetMaxSubscriberCount()
    {
    return ULONG_MAX;
    }

unsigned long AliHLTControlledMerger::GetMinPublisherCount()
    {
    return 1;
    }

unsigned long AliHLTControlledMerger::GetMaxPublisherCount()
    {
    return 1;
    }

bool AliHLTControlledMerger::ExecCmd( AliUInt32_t cmd, bool& quit )
    {
    switch ( cmd )
	{
	case kAliHLTPCSetPathStateCmd:
	    {
	    bool up=true;
	    unsigned ndx=~0U;
	    fCmdSignal.Lock();
	    if ( fCmdSignal.HaveNotificationData() )
		up = (bool)fCmdSignal.PopNotificationData();
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTControlledMerger::ExecCmd", "Unable to get path state" )
		    << "Internal Error: Unable to get path state from cmd signal for set path state command. Assuming state is good."
		    << ENDLOG;
		}
	    if ( fCmdSignal.HaveNotificationData() )
		ndx = (unsigned)fCmdSignal.PopNotificationData();
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTControlledMerger::ExecCmd", "Unable to get path specifier index" )
		    << "Internal Error: Unable to get path specifier index from cmd signal for set path state command. Assuming index (unsigned)-1."
		    << ENDLOG;
		}
	    fCmdSignal.Unlock();
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) &&
		 !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) )
		return false;
	    stateLock.Unlock();
	    if ( up )
		fBaseObject->EnableSubscriber( ndx );
	    else
		fBaseObject->DisableSubscriber( ndx );
	    return true;
	    }
	}
    return TParent::ExecCmd( cmd, quit );
    }

void AliHLTControlledMerger::ProcessSCCmd( AliHLTSCCommandStruct* cmdS )
    {
    AliHLTSCCommand cmd( cmdS );
    if ( !fStatus )
	{
	// XXX
	return;
	}
    switch ( cmd.GetData()->fCmd )
	{
	case kAliHLTPCSetPathStateCmd:
	    {
	    bool up = (bool)cmd.GetData()->fParam1;
	    unsigned ndx = cmd.GetData()->fParam0;
	    fCmdSignal.Lock();
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.AddNotificationData( (AliUInt64_t)ndx );
	    fCmdSignal.AddNotificationData( (AliUInt64_t)up );
	    fCmdSignal.Unlock();
	    fCmdSignal.Signal();
	    return;
	    }
	}
    TParent::ProcessSCCmd( cmdS );
    }


AliHLTEventMergerNew* AliHLTControlledMerger::CreateBaseObject()
    {
    return new AliHLTEventMergerNew( fComponentName, fMaxPreEventCountExp2 );
    }

void AliHLTControlledMerger::StartProcessing()
    {
    fBaseObject->StartProcessing();
    }

void AliHLTControlledMerger::PauseProcessing()
    {
    fBaseObject->PauseProcessing();
    }

void AliHLTControlledMerger::ResumeProcessing()
    {
    fBaseObject->ResumeProcessing();
    }

void AliHLTControlledMerger::StopProcessing()
    {
    fBaseObject->StopProcessing();
    }


AliHLTEventMergerNew::TPublisher* AliHLTControlledMerger::CreatePublisher( unsigned long pubNdx, PublisherData* pd )
    {
    return new AliHLTEventMergerNew::TPublisher( fBaseObject, pubNdx, pd->fOwnPublisherName.c_str(), fMaxPreEventCountExp2 );
    }


AliHLTEventMergerNew::TSubscriber* AliHLTControlledMerger::CreateSubscriber( unsigned long subNdx, SubscriberData* sd )
    {
    return new AliHLTEventMergerNew::TSubscriber( fBaseObject, subNdx, sd->fSubscriberID.c_str() );
    }


void AliHLTControlledMerger::Unconfigure()
    {
    std::vector<AliHLTEventTriggerStruct*> etcEmpty;
    vector<SubscriberData>::iterator subIter, subEnd;
    pthread_mutex_lock( &fSubscriberMutex );
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    while ( subIter != subEnd )
	{
	fBaseObject->SetEventTypeStackMachineCodes( subIter->fNdx, etcEmpty );
	subIter++;
	}
    pthread_mutex_unlock( &fSubscriberMutex );
    TParent::Unconfigure();
    }

void AliHLTControlledMerger::SetupComponents()
    {
    TParent::SetupComponents();
    fBaseObject->SetDescriptorHandler( fDescriptorHandler );
//     fBaseObject->SetShmManager( fShmManager );
    pthread_mutex_lock( &fPublisherMutex );
    fBaseObject->SetPublisher( fPublishers.begin()->fPublisher );
    pthread_mutex_unlock( &fPublisherMutex );
    fBaseObject->SetStatusData( fStatus );
    fBaseObject->SetEventTimeout( fEventTimeout_ms );
    if ( fEventTimeoutShift_ms )
	fBaseObject->SetEventTimeoutShift( fEventTimeoutShift_ms );
    if ( fDynamicTimeoutAdaptionMaxTimeout_ms )
	fBaseObject->EnableDynamicTimeoutAdaption( fDynamicTimeoutAdaptionMinTimeout_ms, fDynamicTimeoutAdaptionMaxTimeout_ms );
    if ( fDynamicInputXablingNoEventCntLimit )
	fBaseObject->EnableDynamicInputXabling( fDynamicInputXablingNoEventCntLimit );
    if ( fAliceHLT )
	{
	fBaseObject->SetEventTriggerTypeCompareFunc( StackMachineCompareEventTrigger, fTriggerStackMachine );
	}
    
    //unsigned long n = 0;
    vector<TParent::SubscriberData>::iterator subIter, subEnd;
    pthread_mutex_lock( &fSubscriberMutex );
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    while ( subIter != subEnd )
	{
	if ( subIter->fSubscriber )
	    {
	    fBaseObject->AddSubscriber( subIter->fSubscriber, subIter->fPublisherProxy, subIter->fNdx );
	    fBaseObject->SetSubscriberContributingDDLList( subIter->fNdx, subIter->fContributingDDLs );
	    fBaseObject->SetSubscriberEventModulos( subIter->fNdx, subIter->fSubscriberInputModulos );
	    if ( fAliceHLT )
		{
		MLUCSimpleStackMachineAssembler::TError smaErr;
		vector<MLUCString>::iterator etcIter, etcEnd;
		etcIter = subIter->fSubscriberInputEventTypeStackMachineCodesText.begin();
		etcEnd = subIter->fSubscriberInputEventTypeStackMachineCodesText.end();
		while ( etcIter != etcEnd )
		    {
		    smaErr = fStackMachineAssembler.Assemble( *etcIter );
		    if ( !smaErr.Ok() )
			{
			LOG( AliHLTLog::kError, "AliHLTControlledMerger::SetupComponents", "Stack Machine Assembler Error" )
			    << "Error returned by trigger stack machine assembler while assembling code for input subscriber " << subIter->fSubscriber->GetName()
			    << ": " << smaErr.Description() 
			    << ENDLOG;
			pthread_mutex_unlock( &fSubscriberMutex );
			MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
			return;
			}
		    unsigned long codeWordCount = fStackMachineAssembler.GetCodeWordCount();
		    unsigned long totalSize = codeWordCount*sizeof(uint64)+sizeof(AliHLTEventTriggerStruct);
		    AliHLTEventTriggerStruct* code = reinterpret_cast<AliHLTEventTriggerStruct*>( new uint8[ totalSize ] );
		    if ( !code )
			{
			LOG( AliHLTLog::kError, "AliHLTControlledComponent::SetupComponents", "Out of memory" )
			    << "Out of memory allocating event type code for trigger stack machine for input subscriber " << subIter->fSubscriber->GetName()
			    << "." << ENDLOG;
			pthread_mutex_unlock( &fSubscriberMutex );
			MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
			return;
			}
		    memcpy( &(code->fDataWords[0]), fStackMachineAssembler.GetCode(), sizeof(uint64)*codeWordCount );
		    code->fHeader.fLength = totalSize;
		    code->fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
		    code->fHeader.fSubType.fID = 0;
		    code->fHeader.fVersion = 1;
		    code->fDataWordCount = (codeWordCount*sizeof(uint64))/sizeof(code->fDataWords[0]);
		    subIter->fSubscriberInputEventTypeStackMachineCodes.push_back( code );
		    
		    etcIter++;
		    }
		fBaseObject->SetEventTypeStackMachineCodes( subIter->fNdx, subIter->fSubscriberInputEventTypeStackMachineCodes );

		}
	    }
	//n++;
	subIter++;
	}
    pthread_mutex_unlock( &fSubscriberMutex );
    fBaseObject->SetETSMergeMode( fETSMergeMode );

    MLUCMutex::TLocker ddlListLocker( fTriggerClassContributingDDLsMutex );
    unsigned ndx=0, maxNdx=fTriggerClassContributingDDLs.size();
    while ( ndx<maxNdx )
	{
	if ( fTriggerClassContributingDDLs[ndx] )
	    fBaseObject->SetTriggerClassBitContributingDDLs( ndx, fTriggerClassContributingDDLs[ndx] );
	++ndx;
	}
    }





AliHLTControlledScattererBase::AliHLTControlledScattererBase( const char* compName, int argc, char** argv ):
    TParent( TParent::kScatterer, compName, argc, argv )
    {
    }

AliHLTControlledScattererBase::~AliHLTControlledScattererBase()
    {
    }


bool AliHLTControlledScattererBase::HasBaseObject()
    {
    return true;
    }

unsigned long AliHLTControlledScattererBase::GetMinSubscriberCount()
    {
    return 1;
    }

unsigned long AliHLTControlledScattererBase::GetMaxSubscriberCount()
    {
    return 1;
    }

unsigned long AliHLTControlledScattererBase::GetMinPublisherCount()
    {
    return 1;
    }

unsigned long AliHLTControlledScattererBase::GetMaxPublisherCount()
    {
    return ULONG_MAX;
    }

bool AliHLTControlledScattererBase::ExecCmd( AliUInt32_t cmd, bool& quit )
    {
    switch ( cmd )
	{
	case kAliHLTPCSetPathStateCmd:
	    {
	    bool up=true;
	    unsigned ndx=~0U;
	    fCmdSignal.Lock();
	    if ( fCmdSignal.HaveNotificationData() )
		up = (bool)fCmdSignal.PopNotificationData();
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTControlledScattererBase::ExecCmd", "Unable to get path state" )
		    << "Internal Error: Unable to get path state from cmd signal for set path state command. Assuming state is good."
		    << ENDLOG;
		}
	    if ( fCmdSignal.HaveNotificationData() )
		ndx = (unsigned)fCmdSignal.PopNotificationData();
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTControlledScattererBase::ExecCmd", "Unable to get path specifier index" )
		    << "Internal Error: Unable to get path specifier index from cmd signal for set path state command. Assuming index (unsigned)-1."
		    << ENDLOG;
		}
	    fCmdSignal.Unlock();
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) &&
		 !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) )
		return false;
	    stateLock.Unlock();
	    if ( up )
		fBaseObject->EnablePublisher( ndx );
	    else
		fBaseObject->DisablePublisher( ndx );
	    return true;
	    }
	case kAliHLTPCAddPublisherCmd:
	    {
	    char* ID=NULL;
	    fCmdSignal.Lock();
	    if ( fCmdSignal.HaveNotificationData() )
		ID = (char*)(unsigned long)fCmdSignal.PopNotificationData();
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTControlledScattererBase::ExecCmd", "Unable to get publisher ID" )
		    << "Internal Error: Unable to get newpublisher ID from cmd signal for add publisher command. Aborting..."
		    << ENDLOG;
		return false;
		}
	    fCmdSignal.Unlock();
	    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ExecCmd", "AddPublisher Command" )
		<< "Add publisher command received for publisher ID '" << ID << "'." << ENDLOG;
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) &&
		 !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) )
		{
		delete[] ID;
		return false;
		}
	    stateLock.Unlock();
	    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    std::vector<PublisherData>::iterator pubIter, pubEnd;
	    pthread_mutex_lock( &fPublisherMutex );
	    pubIter = fPublishers.begin();
	    pubEnd = fPublishers.end();
	    while ( pubIter != pubEnd )
		{
		if ( pubIter->fOwnPublisherName == ID )
		    {
		    LOG( AliHLTLog::kInformational, "AliHLTControlledScattererBase::ExecCmd / AddPublisherCmd", "Publisher Already Present" )
			<< "Publisher object " << AliHLTLog::kDec << pubIter->fNdx << " - '"
			<< pubIter->fOwnPublisherName.c_str() << "' is already present." << ENDLOG;
		    break;
		    }
		pubIter++;
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    if ( pubIter==pubEnd )
		{
		PublisherData pd;
		pd.fOwnPublisherName = ID;
		pd.fNdx = fMaxPublisherIndex++;
		pubIter = fPublishers.insert( fPublishers.end(), pd );
		fSubscriptionStatusData.fRequiredSubscriptions++;
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    stateLock.Lock();
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag )
		{
		stateLock.Unlock();
		LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
		if ( !pubIter->fPublisher )
		    {
		    pubIter->fPublisher = CreatePublisher( pubIter->fNdx, &(*pubIter) );
		    if ( !pubIter->fPublisher )
			{
			LOG( AliHLTLog::kError, "AliHLTControlledScattererBase::ExecCmd / AddPublisherCmd", "Unable to Create Publisher Object" )
			    << "Unable to create publisher object " << AliHLTLog::kDec << pubIter->fNdx << " - '"
			    << pubIter->fOwnPublisherName.c_str() << "'." << ENDLOG;
			pthread_mutex_unlock( &fPublisherMutex );
			delete[] ID;
			stateLock.Lock();
			fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
			return false;
			}
		    pubIter->fPublisher->SetMaxTimeout( fPublisherTimeout );
		    pubIter->fPublisher->SetSubscriptionLoop( &PublisherPipeSubscriptionInputLoop );
		    pubIter->fPublisher->AddSubscriberEventCallback( &fSubscriberEventCallback );
		    
		    fBaseObject->AddPublisher( pubIter->fPublisher, pubIter->fNdx );
		    }
		LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
		if ( !pubIter->fSubscriptionListenerThread )
		    {
		    pubIter->fSubscriptionListenerThread = CreateSubscriptionListenerThread( pubIter->fNdx, &(*pubIter) );
		    if ( !pubIter->fSubscriptionListenerThread )
			{
			LOG( AliHLTLog::kError, "AliHLTControlledScattererBase::ExecCmd / AddPublisherCmd", "Unable to Create Subscription Listener Thread Object" )
			    << "Unable to create subscription listener thread object for publisher " << AliHLTLog::kDec << pubIter->fNdx << " ('"
			    << pubIter->fOwnPublisherName.c_str() << "')." << ENDLOG;
			pthread_mutex_unlock( &fPublisherMutex );
			delete[] ID;
			stateLock.Lock();
			fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
			return false;
			}
		    }
		
		LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
		stateLock.Lock();
		}
	    if ( fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag )
		{
		stateLock.Unlock();
		LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
		if ( pubIter->fSubscriptionListenerThread )
		    {
		LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
		    if ( pubIter->fSubscriptionListenerThread->HasQuitted() )
			{
		LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
			pubIter->fSubscriptionListenerThread->Start();
			}
		    }
		else
		    {
		    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::AcceptSubscriptions", "No subscription thread" )
			<< "Publisher '" << pubIter->fOwnPublisherName.c_str() << "' has no subscription thread."
			<< ENDLOG;
		    pthread_mutex_unlock( &fPublisherMutex );
		    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
		    stateLock.Lock();
		    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
		    delete[] ID;
		    return false;
		    }
		}
	    else
		stateLock.Unlock();
	    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    pthread_mutex_unlock( &fPublisherMutex );
	    delete[] ID;
	    return true;
	    }
	case kAliHLTPCDelPublisherCmd:
	    {
	    char* ID=NULL;
	    fCmdSignal.Lock();
	    if ( fCmdSignal.HaveNotificationData() )
		ID = (char*)(unsigned long)fCmdSignal.PopNotificationData();
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTControlledScattererBase::ExecCmd", "Unable to get publisher ID" )
		    << "Internal Error: Unable to get newpublisher ID from cmd signal for add publisher command. Aborting..."
		    << ENDLOG;
		return false;
		}
	    fCmdSignal.Unlock();
	    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ExecCmd", "DelPublisher Command" )
		<< "Del publisher command received for publisher ID '" << ID << "'." << ENDLOG;
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		if ( (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) &&
		     !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) )
		    {
		    delete[] ID;
		    return false;
		    }
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    vector<PublisherData>::iterator pubIter, pubEnd;
	    pthread_mutex_lock( &fPublisherMutex );
	    pubIter = fPublishers.begin();
	    pubEnd = fPublishers.end();
	    while ( pubIter != pubEnd )
		{
		if ( pubIter->fOwnPublisherName == ID )
		    {
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    if ( (fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) && pubIter->fSubscriptionListenerThread )
			{
			stateLock.Unlock();
			pubIter->fPublisher->CancelAllSubscriptions( true );
			pubIter->fSubscriptionListenerThread->Quit();
			stateLock.Lock();
			}
		    if ( fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag )
			{
			stateLock.Unlock();
			fBaseObject->DelPublisher( pubIter->fNdx );
			if ( pubIter->fSubscriptionListenerThread )
			    {
			    delete pubIter->fSubscriptionListenerThread;
			    pubIter->fSubscriptionListenerThread = NULL;
			    }
			if ( pubIter->fPublisher )
			    {
			    delete pubIter->fPublisher;
			    pubIter->fPublisher = NULL;
			    }
			}
		    break;
		    }
		pubIter++;
		}
	    pthread_mutex_unlock( &fPublisherMutex );
	    

	    delete[] ID;
	    return true;
	    }
	case kAliHLTPCEnablePublisherCmd:
	    {
	    char* ID=NULL;
	    fCmdSignal.Lock();
	    if ( fCmdSignal.HaveNotificationData() )
		ID = (char*)(unsigned long)fCmdSignal.PopNotificationData();
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTControlledScattererBase::ExecCmd", "Unable to get publisher ID" )
		    << "Internal Error: Unable to get newpublisher ID from cmd signal for add publisher command. Aborting..."
		    << ENDLOG;
		return false;
		}
	    fCmdSignal.Unlock();
	    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ExecCmd", "DelPublisher Command" )
		<< "Del publisher command received for publisher ID '" << ID << "'." << ENDLOG;
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		if ( (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) &&
		     !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) )
		    {
		    delete[] ID;
		    return false;
		    }
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    vector<PublisherData>::iterator pubIter, pubEnd;
	    pthread_mutex_lock( &fPublisherMutex );
	    pubIter = fPublishers.begin();
	    pubEnd = fPublishers.end();
	    while ( pubIter != pubEnd )
		{
		if ( pubIter->fOwnPublisherName == ID )
		    {
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    if ( fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag )
			{
			stateLock.Unlock();
			fBaseObject->EnablePublisher( pubIter->fNdx );
			}
		    break;
		    }
		pubIter++;
		}
	    pthread_mutex_unlock( &fPublisherMutex );

	    delete[] ID;
	    return true;
	    }
	case kAliHLTPCDisablePublisherCmd:
	    {
	    char* ID=NULL;
	    fCmdSignal.Lock();
	    if ( fCmdSignal.HaveNotificationData() )
		ID = (char*)(unsigned long)fCmdSignal.PopNotificationData();
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTControlledScattererBase::ExecCmd", "Unable to get publisher ID" )
		    << "Internal Error: Unable to get newpublisher ID from cmd signal for add publisher command. Aborting..."
		    << ENDLOG;
		return false;
		}
	    fCmdSignal.Unlock();
	    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ExecCmd", "DelPublisher Command" )
		<< "Del publisher command received for publisher ID '" << ID << "'." << ENDLOG;
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		if ( (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) &&
		     !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) )
		    {
		    delete[] ID;
		    return false;
		    }
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    vector<PublisherData>::iterator pubIter, pubEnd;
	    pthread_mutex_lock( &fPublisherMutex );
	    pubIter = fPublishers.begin();
	    pubEnd = fPublishers.end();
	    while ( pubIter != pubEnd )
		{
		if ( pubIter->fOwnPublisherName == ID )
		    {
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    if ( fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag )
			{
			stateLock.Unlock();
			fBaseObject->DisablePublisher( pubIter->fNdx );
			}
		    break;
		    }
		pubIter++;
		}
	    pthread_mutex_unlock( &fPublisherMutex );

	    delete[] ID;
	    return true;
	    }
	}
    return TParent::ExecCmd( cmd, quit );
    }

void AliHLTControlledScattererBase::ProcessSCCmd( AliHLTSCCommandStruct* cmdS )
    {
    AliHLTSCCommand cmd( cmdS );
    if ( !fStatus )
	{
	// XXX
	return;
	}
    switch ( cmd.GetData()->fCmd )
	{
	case kAliHLTPCSetPathStateCmd:
	    {
	    bool up = (bool)cmd.GetData()->fParam1;
	    unsigned ndx = cmd.GetData()->fParam0;
	    fCmdSignal.Lock();
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.AddNotificationData( (AliUInt64_t)ndx );
	    fCmdSignal.AddNotificationData( (AliUInt64_t)up );
	    fCmdSignal.Unlock();
	    fCmdSignal.Signal();
	    return;
	    }
	case kAliHLTPCAddPublisherCmd: // fallthrough
	case kAliHLTPCDelPublisherCmd: // fallthrough
	case kAliHLTPCEnablePublisherCmd: // fallthrough
	case kAliHLTPCDisablePublisherCmd:
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ProcessSCCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    if ( GetMinPublisherCount()<=0 )
		break;
	    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ProcessSCCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    pthread_mutex_lock( &fPublisherMutex );
	    unsigned long pubCnt = fPublishers.size();
	    pthread_mutex_unlock( &fPublisherMutex );
	    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ProcessSCCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    if ( cmd.GetData()->fCmd==kAliHLTPCAddPublisherCmd && GetMaxPublisherCount()<=pubCnt )
		{
		LOG( AliHLTLog::kWarning, "AliHLTControlledScattererBase::ProcessSCCmd", "AddPublisher Command" )
		    << "Too many publishers already specified (" << AliHLTLog::kDec << pubCnt << " of "
		    << GetMaxPublisherCount() << " allowed)." << ENDLOG;
		break;
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ProcessSCCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    if ( cmd.GetData()->fCmd==kAliHLTPCDelPublisherCmd && GetMinPublisherCount()>=pubCnt )
		{
		LOG( AliHLTLog::kWarning, "AliHLTControlledScattererBase::ProcessSCCmd", "DelPublisher Command" )
		    << "Not enough publishers left (" << AliHLTLog::kDec << pubCnt << " of "
		    << GetMinPublisherCount() << " required)." << ENDLOG;
		break;
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ProcessSCCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    char* ID=NULL;
	    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ProcessSCCmd", "Add/Del-Publisher Command" )
		<< "Add/Del publisher command received for publisher ID '" << (char*)(cmdS->fData) << "'." << ENDLOG;
	    ID = new char[ strlen((char*)(cmdS->fData))+1 ];
	    if ( !ID )
		{
		LOG( AliHLTLog::kError, "AliHLTControlledScattererBase::ProcessSCCmd", "Out of memory" )
		    << "Out of memory trying to allocate memory for Add/Del-Publisher command parameter of "
		    << AliHLTLog::kDec << strlen((char*)(cmd.GetData()->fData))+1 << " bytes." << ENDLOG;
		break;
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ProcessSCCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    strcpy( ID, (char*)( cmdS->fData ) );
	    fCmdSignal.Lock();
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.AddNotificationData( (AliUInt64_t)(unsigned long)ID );
	    fCmdSignal.Unlock();
	    fCmdSignal.Signal();
	    LOG( AliHLTLog::kDebug, "AliHLTControlledScattererBase::ProcessSCCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    return;
	    }
	}
    TParent::ProcessSCCmd( cmdS );
    }

void AliHLTControlledScattererBase::StartProcessing()
    {
    fBaseObject->StartProcessing();
    }

void AliHLTControlledScattererBase::PauseProcessing()
    {
    fBaseObject->PauseProcessing();
    }

void AliHLTControlledScattererBase::ResumeProcessing()
    {
    fBaseObject->ResumeProcessing();
    }

void AliHLTControlledScattererBase::StopProcessing()
    {
    fBaseObject->StopProcessing();
    }


AliHLTEventScattererNew::TPublisher* AliHLTControlledScattererBase::CreatePublisher( unsigned long pubNdx, PublisherData* pd )
    {
    return new AliHLTEventScattererNew::TPublisher( fBaseObject, pubNdx, pd->fOwnPublisherName.c_str(), fMaxPreEventCountExp2 );
    }


AliHLTEventScattererNew::TSubscriber* AliHLTControlledScattererBase::CreateSubscriber( unsigned long subNdx, SubscriberData* sd )
    {
    return new AliHLTEventScattererNew::TSubscriber( fBaseObject, subNdx, sd->fSubscriberID.c_str() );
    }


void AliHLTControlledScattererBase::SetupComponents()
    {
    TParent::SetupComponents();
    fBaseObject->SetDescriptorHandler( fDescriptorHandler );
    pthread_mutex_lock( &fSubscriberMutex );
    fBaseObject->SetSubscriber( fSubscribers.begin()->fSubscriber );
    fBaseObject->SetOrigPublisher( fSubscribers.begin()->fPublisherProxy );
    pthread_mutex_unlock( &fSubscriberMutex );
    fBaseObject->SetStatusData( fStatus );
    if ( fScattererParamSet )
	fBaseObject->SetScattererParameter( fScattererParam  );

    if ( fEventIDBased )
	fBaseObject->DistributionEventIDBased();
    else
	fBaseObject->DistributionSeqNrBased();
    
    vector<TParent::PublisherData>::iterator pubIter, pubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    while ( pubIter != pubEnd )
	{
	if ( pubIter->fPublisher )
	    {
	    fBaseObject->AddPublisher( pubIter->fPublisher, pubIter->fNdx );
	    }
	pubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    
    }




AliHLTControlledGatherer::AliHLTControlledGatherer( const char* compName, int argc, char** argv):
    TParent( TParent::kGatherer, compName, argc, argv )
    {
    }

AliHLTControlledGatherer::~AliHLTControlledGatherer()
    {
    }


bool AliHLTControlledGatherer::HasBaseObject()
    {
    return true;
    }

unsigned long AliHLTControlledGatherer::GetMinSubscriberCount()
    {
    return 1;
    }

unsigned long AliHLTControlledGatherer::GetMaxSubscriberCount()
    {
    return ULONG_MAX;
    }

unsigned long AliHLTControlledGatherer::GetMinPublisherCount()
    {
    return 1;
    }

unsigned long AliHLTControlledGatherer::GetMaxPublisherCount()
    {
    return 1;
    }

bool AliHLTControlledGatherer::ExecCmd( AliUInt32_t cmd, bool& quit )
    {
    switch ( cmd )
	{
	case kAliHLTPCSetPathStateCmd:
	    {
	    bool up=true;
	    unsigned ndx=~0U;
	    fCmdSignal.Lock();
	    if ( fCmdSignal.HaveNotificationData() )
		up = (bool)fCmdSignal.PopNotificationData();
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTControlledGatherer::ExecCmd", "Unable to get path state" )
		    << "Internal Error: Unable to get path state from cmd signal for set path state command. Assuming state is good."
		    << ENDLOG;
		}
	    if ( fCmdSignal.HaveNotificationData() )
		ndx = (unsigned)fCmdSignal.PopNotificationData();
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTControlledGatherer::ExecCmd", "Unable to get path specifier index" )
		    << "Internal Error: Unable to get path specifier index from cmd signal for set path state command. Assuming index (unsigned)-1."
		    << ENDLOG;
		}
	    fCmdSignal.Unlock();
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		if ( (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) &&
		     !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) )
		    return false;
		}
	    if ( up )
		fBaseObject->EnableSubscriber( ndx );
	    else
		fBaseObject->DisableSubscriber( ndx );
	    return true;
	    }
	case kAliHLTPCAddSubscriberCmd:
	    {
	    char* tmpIDs=NULL;
	    fCmdSignal.Lock();
	    if ( fCmdSignal.HaveNotificationData() )
		tmpIDs = (char*)(unsigned long)fCmdSignal.PopNotificationData();
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTControlledGatherer::ExecCmd", "Unable to get subscriber ID" )
		    << "Internal Error: Unable to get newsubscriber ID from cmd signal for add subscriber command. Aborting..."
		    << ENDLOG;
		return false;
		}
	    fCmdSignal.Unlock();
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ExecCmd", "AddSubscriber Command" )
		<< "Add subscriber command received for subscriber ID '" << tmpIDs << "'." << ENDLOG;
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		if ( (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) &&
		     !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) )
		    {
		    delete[] tmpIDs;
		    return false;
		    }
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    std::vector<MLUCString> IDs;
	    if ( !MLUCCmdlineParser::ParseCmd( tmpIDs, IDs ) || IDs.size()<2)
		{
		LOG( AliHLTLog::kError, "AliHLTControlledGatherer::ExecCmd", "Unable to get subscriber ID" )
		    << "Unable to parse publisher & subscriber IDs from add subscriber command. Aborting..."
		    << ENDLOG;
		delete[] tmpIDs;
		return false;
		}
	    std::vector<SubscriberData>::iterator subIter, subEnd;
	    pthread_mutex_lock( &fSubscriberMutex );
	    subIter = fSubscribers.begin();
	    subEnd = fSubscribers.end();
	    while ( subIter != subEnd )
		{
		if ( subIter->fSubscriberID == IDs[1] )
		    {
		    LOG( AliHLTLog::kInformational, "AliHLTControlledGatherer::ExecCmd / AddSubscriberCmd", "Subscriber Already Present" )
			<< "Subscriber object " << AliHLTLog::kDec << subIter->fNdx << " - '"
			<< subIter->fSubscriberID.c_str() << "' is already present." << ENDLOG;
		    break;
		    }
		subIter++;
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    if ( subIter==subEnd )
		{
		SubscriberData sd;
		sd.fAttachedPublisherName = IDs[0];
		sd.fSubscriberID = IDs[1];
		sd.fNdx = fMaxSubscriberIndex++;
		sd.fEventModulo = 1;
		subIter = fSubscribers.insert( fSubscribers.end(), sd );
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;

		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		if ( fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag )
		    {
		    stateLock.Unlock();
		    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
		    if ( !subIter->fSubscriber )
			{
			subIter->fSubscriber = CreateSubscriber( subIter->fNdx, &(*subIter) );
			if ( !subIter->fSubscriber )
			    {
			    LOG( AliHLTLog::kError, "AliHLTControlledGatherer::ExecCmd", "Unable to Create Subscriber Object" )
				<< "Unable to create subscriber object " << AliHLTLog::kDec << subIter->fNdx << " - '"
				<< subIter->fAttachedPublisherName.c_str() << "'/'" << subIter->fSubscriberID.c_str()
				<< "'." << ENDLOG;
			    pthread_mutex_unlock( &fSubscriberMutex );
			    delete[] tmpIDs;
			    stateLock.Lock();
			    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
			    return false;
			    }
			}
		    }
		LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
		if ( !subIter->fPublisherProxy )
		    {
		    subIter->fPublisherProxy = CreatePublisherProxy( subIter->fNdx, &(*subIter) );
		    if ( !subIter->fPublisherProxy )
			{
			LOG( AliHLTLog::kError, "AliHLTControlledGatherer::ExecCmd", "Unable to Create Publisher Proxy Object" )
			    << "Unable to create publisher proxy object " << AliHLTLog::kDec << subIter->fNdx << " - '"
			    << subIter->fAttachedPublisherName.c_str()
			    << "'." << ENDLOG;
			pthread_mutex_unlock( &fSubscriberMutex );
			delete[] tmpIDs;
			MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
			return false;
			}
		    }
		LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
		if ( !subIter->fSubscriptionThread )
		    {
		    subIter->fSubscriptionThread = CreateSubscriptionThread( subIter->fNdx, &(*subIter) );
		    if ( !subIter->fSubscriptionThread )
			{
			LOG( AliHLTLog::kError, "AliHLTControlledGatherer::ExecCmd", "Unable to Create Subscription Thread Object" )
			    << "Unable to create subscription thread object for subscriber " << AliHLTLog::kDec << subIter->fNdx << " - '"
			    << subIter->fAttachedPublisherName.c_str() << "'/'" << subIter->fSubscriberID.c_str()
			    << "'." << ENDLOG;
			pthread_mutex_unlock( &fSubscriberMutex );
			delete[] tmpIDs;
			MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
			return false;
			}
		    }
#ifdef USE_PIPE_TIMEOUT
		subIter->fPublisherProxy->SetTimeout( 500000 );    
#endif
		LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
		fBaseObject->AddSubscriber( subIter->fSubscriber, subIter->fPublisherProxy, subIter->fNdx );
		}
		    {
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    if ( fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag )
			{
			stateLock.Unlock();
			int ret;
			ret = subIter->fPublisherProxy->Subscribe( *(subIter->fSubscriber) );
			if ( ret )
			    {
			    stateLock.Lock();
			    fStatus->fProcessStatus.fState |= kAliHLTPCConsumerErrorStateFlag;
			    stateLock.Unlock();
			    LOG( AliHLTLog::kError, "AliHLTControlledGatherer::ExecCmd", "Error subscribing" )
				<< "Error subscribing subscriber '" << subIter->fSubscriber->GetName() << "' to publisher '"
				<< subIter->fPublisherProxy->GetName() << "': " << strerror(ret) << " ("
				<< MLUCLog::kDec << ret << ")." << ENDLOG;
			    delete[] tmpIDs;
			    pthread_mutex_unlock( &fSubscriberMutex );
			    stateLock.Lock();
			    fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
			    return false;
			    }
			subIter->fSubscriptionThread->Start();
			}
		    }
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    pthread_mutex_unlock( &fSubscriberMutex );
	    delete[] tmpIDs;
	    return true;
	    }
	case kAliHLTPCDelSubscriberCmd:
	    {
	    char* tmpIDs=NULL;
	    fCmdSignal.Lock();
	    if ( fCmdSignal.HaveNotificationData() )
		tmpIDs = (char*)(unsigned long)fCmdSignal.PopNotificationData();
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTControlledGatherer::ExecCmd", "Unable to get subscriber ID" )
		    << "Internal Error: Unable to get newsubscriber ID from cmd signal for add subscriber command. Aborting..."
		    << ENDLOG;
		return false;
		}
	    fCmdSignal.Unlock();
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ExecCmd", "AddSubscriber Command" )
		<< "Add subscriber command received for subscriber ID '" << tmpIDs << "'." << ENDLOG;
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		if ( (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) &&
		     !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) )
		    {
		    delete[] tmpIDs;
		    return false;
		    }
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    std::vector<MLUCString> IDs;
	    if ( !MLUCCmdlineParser::ParseCmd( tmpIDs, IDs ) || IDs.size()<2)
		{
		LOG( AliHLTLog::kError, "AliHLTControlledGatherer::ExecCmd", "Unable to get subscriber ID" )
		    << "Unable to parse publisher & subscriber IDs from add subscriber command. Aborting..."
		    << ENDLOG;
		delete[] tmpIDs;
		return false;
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    vector<SubscriberData>::iterator subIter, subEnd;
	    pthread_mutex_lock( &fSubscriberMutex );
	    subIter = fSubscribers.begin();
	    subEnd = fSubscribers.end();
	    while ( subIter != subEnd )
		{
		if ( subIter->fSubscriberID == IDs[1] )
		    {
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    if ( (fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) && subIter->fSubscriptionThread )
			{
			stateLock.Unlock();
			subIter->fSubscriptionThread->Quit();
			struct timeval start, now;
			unsigned long long deltaT = 0;
			const unsigned long long timeLimit = 1000000;
			gettimeofday( &start, NULL );
			while ( !subIter->fSubscriptionThread->HasQuitted() && deltaT<timeLimit )
			    {
			    usleep( 10000 );
			    gettimeofday( &now, NULL );
			    deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
			    }
			if ( !subIter->fSubscriptionThread->HasQuitted() )
			    {
			    LOG( AliHLTLog::kInformational, "AliHLTControlledGatherer::ExecCmd", "Aborting Publishing Loop" )
				<< "Publishing loop for subscriber '" << subIter->fSubscriber->GetName() << "' and publisher '"
				<< subIter->fPublisherProxy->GetName() << "' has to be aborted." << ENDLOG;
			    subIter->fSubscriptionThread->AbortPublishing();
			    }
			deltaT = 0;
			gettimeofday( &start, NULL );
			while ( !subIter->fSubscriptionThread->HasQuitted() && deltaT<timeLimit )
			    {
			    usleep( 10000 );
			    gettimeofday( &now, NULL );
			    deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
			    }
			if ( !subIter->fSubscriptionThread->HasQuitted() )
			    {
			    LOG( AliHLTLog::kInformational, "AliHLTControlledGatherer::ExecCmd", "Aborting Subscription Thread" )
				<< "Subscription thread loop for subscriber '" << subIter->fSubscriber->GetName() << "' and publisher '"
				<< subIter->fPublisherProxy->GetName() << "' has to be aborted." << ENDLOG;
			    subIter->fSubscriptionThread->Abort();
			    }
			subIter->fSubscriptionThread->Join();
			stateLock.Lock();
			}
		    if ( fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag )
			{
			stateLock.Unlock();
			fBaseObject->DelSubscriber( subIter->fNdx );
			if ( subIter->fSubscriptionThread )
			    {
			    delete subIter->fSubscriptionThread;
			    subIter->fSubscriptionThread = NULL;
			    }
			if ( subIter->fPublisherProxy )
			    {
			    delete subIter->fPublisherProxy;
			    subIter->fPublisherProxy = NULL;
			    }
			if ( subIter->fSubscriber )
			    {
			    delete subIter->fSubscriber;
			    subIter->fSubscriber = NULL;
			    }
			if ( subIter->fContributingDDLs )
			    {
			    delete [] subIter->fContributingDDLs;
			    subIter->fContributingDDLs = NULL;
			    }
			}
		    break;
		    }
		subIter++;
		}
	    pthread_mutex_unlock( &fSubscriberMutex );
	    

	    delete[] tmpIDs;
	    return true;
	    }
	case kAliHLTPCEnableSubscriberCmd:
	    {
	    char* tmpIDs=NULL;
	    fCmdSignal.Lock();
	    if ( fCmdSignal.HaveNotificationData() )
		tmpIDs = (char*)(unsigned long)fCmdSignal.PopNotificationData();
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTControlledGatherer::ExecCmd", "Unable to get subscriber ID" )
		    << "Internal Error: Unable to get newsubscriber ID from cmd signal for add subscriber command. Aborting..."
		    << ENDLOG;
		return false;
		}
	    fCmdSignal.Unlock();
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ExecCmd", "AddSubscriber Command" )
		<< "Add subscriber command received for subscriber ID '" << tmpIDs << "'." << ENDLOG;
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		if ( (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) &&
		     !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) )
		    {
		    delete[] tmpIDs;
		    return false;
		    }
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    std::vector<MLUCString> IDs;
	    if ( !MLUCCmdlineParser::ParseCmd( tmpIDs, IDs ) || IDs.size()<2)
		{
		LOG( AliHLTLog::kError, "AliHLTControlledGatherer::ExecCmd", "Unable to get subscriber ID" )
		    << "Unable to parse publisher & subscriber IDs from add subscriber command. Aborting..."
		    << ENDLOG;
		delete[] tmpIDs;
		return false;
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    vector<SubscriberData>::iterator subIter, subEnd;
	    pthread_mutex_lock( &fSubscriberMutex );
	    subIter = fSubscribers.begin();
	    subEnd = fSubscribers.end();
	    while ( subIter != subEnd )
		{
		if ( subIter->fSubscriberID == IDs[1] )
		    {
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    if ( fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag )
			{
			stateLock.Unlock();
			fBaseObject->EnableSubscriber( subIter->fNdx );
			}
		    break;
		    }
		subIter++;
		}
	    pthread_mutex_unlock( &fSubscriberMutex );
	    

	    delete[] tmpIDs;
	    return true;
	    }
	case kAliHLTPCDisableSubscriberCmd:
	    {
	    char* tmpIDs=NULL;
	    fCmdSignal.Lock();
	    if ( fCmdSignal.HaveNotificationData() )
		tmpIDs = (char*)(unsigned long)fCmdSignal.PopNotificationData();
	    else
		{
		LOG( AliHLTLog::kError, "AliHLTControlledGatherer::ExecCmd", "Unable to get subscriber ID" )
		    << "Internal Error: Unable to get newsubscriber ID from cmd signal for add subscriber command. Aborting..."
		    << ENDLOG;
		return false;
		}
	    fCmdSignal.Unlock();
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ExecCmd", "AddSubscriber Command" )
		<< "Add subscriber command received for subscriber ID '" << tmpIDs << "'." << ENDLOG;
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		if ( (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) &&
		     !(fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag) )
		    {
		    delete[] tmpIDs;
		    return false;
		    }
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    std::vector<MLUCString> IDs;
	    if ( !MLUCCmdlineParser::ParseCmd( tmpIDs, IDs ) || IDs.size()<2)
		{
		LOG( AliHLTLog::kError, "AliHLTControlledGatherer::ExecCmd", "Unable to get subscriber ID" )
		    << "Unable to parse publisher & subscriber IDs from add subscriber command. Aborting..."
		    << ENDLOG;
		delete[] tmpIDs;
		return false;
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ExecCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    vector<SubscriberData>::iterator subIter, subEnd;
	    pthread_mutex_lock( &fSubscriberMutex );
	    subIter = fSubscribers.begin();
	    subEnd = fSubscribers.end();
	    while ( subIter != subEnd )
		{
		if ( subIter->fSubscriberID == IDs[1] )
		    {
		    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		    if ( fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag )
			{
			stateLock.Unlock();
			fBaseObject->DisableSubscriber( subIter->fNdx );
			}
		    break;
		    }
		subIter++;
		}
	    pthread_mutex_unlock( &fSubscriberMutex );
	    

	    delete[] tmpIDs;
	    return true;
	    }
	}
    return TParent::ExecCmd( cmd, quit );
    }

void AliHLTControlledGatherer::ProcessSCCmd( AliHLTSCCommandStruct* cmdS )
    {
    AliHLTSCCommand cmd( cmdS );
    if ( !fStatus )
	{
	// XXX
	return;
	}
    switch ( cmd.GetData()->fCmd )
	{
	case kAliHLTPCSetPathStateCmd:
	    {
	    bool up = (bool)cmd.GetData()->fParam1;
	    unsigned ndx = cmd.GetData()->fParam0;
	    fCmdSignal.Lock();
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.AddNotificationData( (AliUInt64_t)ndx );
	    fCmdSignal.AddNotificationData( (AliUInt64_t)up );
	    fCmdSignal.Unlock();
	    fCmdSignal.Signal();
	    return;
	    }
	case kAliHLTPCAddSubscriberCmd: // fallthrough
	case kAliHLTPCDelSubscriberCmd: // fallthrough
	case kAliHLTPCEnableSubscriberCmd: // fallthrough
	case kAliHLTPCDisableSubscriberCmd:
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ProcessSCCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    if ( GetMinSubscriberCount()<=0 )
		break;
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ProcessSCCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    pthread_mutex_lock( &fSubscriberMutex );
	    unsigned long subCnt = fSubscribers.size();
	    pthread_mutex_unlock( &fSubscriberMutex );
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ProcessSCCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    if ( cmd.GetData()->fCmd==kAliHLTPCAddSubscriberCmd && GetMaxSubscriberCount()<=subCnt )
		{
		LOG( AliHLTLog::kWarning, "AliHLTControlledGatherer::ProcessSCCmd", "AddSubscriber Command" )
		    << "Too many publishers already specified (" << AliHLTLog::kDec << subCnt << " of "
		    << GetMaxSubscriberCount() << " allowed)." << ENDLOG;
		break;
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ProcessSCCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    if ( cmd.GetData()->fCmd==kAliHLTPCDelSubscriberCmd && GetMinSubscriberCount()>=subCnt )
		{
		LOG( AliHLTLog::kWarning, "AliHLTControlledGatherer::ProcessSCCmd", "DelSubscriber Command" )
		    << "Not enough publishers left (" << AliHLTLog::kDec << subCnt << " of "
		    << GetMinSubscriberCount() << " required)." << ENDLOG;
		break;
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ProcessSCCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    char* ID=NULL;
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ProcessSCCmd", "Add/Del-Subscriber Command" )
		<< "Add/Del publisher command received for publisher ID '" << (char*)(cmdS->fData) << "'." << ENDLOG;
	    ID = new char[ strlen((char*)(cmdS->fData))+1 ];
	    if ( !ID )
		{
		LOG( AliHLTLog::kError, "AliHLTControlledGatherer::ProcessSCCmd", "Out of memory" )
		    << "Out of memory trying to allocate memory for Add/Del-Subscriber command parameter of "
		    << AliHLTLog::kDec << strlen((char*)(cmd.GetData()->fData))+1 << " bytes." << ENDLOG;
		break;
		}
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ProcessSCCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    strcpy( ID, (char*)( cmdS->fData ) );
	    fCmdSignal.Lock();
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.AddNotificationData( (AliUInt64_t)(unsigned long)ID );
	    fCmdSignal.Unlock();
	    fCmdSignal.Signal();
	    LOG( AliHLTLog::kDebug, "AliHLTControlledGatherer::ProcessSCCmd", "TRACE" ) << __FILE__ << ":" << AliHLTLog::kDec << __LINE__ << ENDLOG;
	    return;
	    }
	}
    TParent::ProcessSCCmd( cmdS );
    }



AliHLTEventGathererNew* AliHLTControlledGatherer::CreateBaseObject()
    {
    return new AliHLTEventGathererNew( fComponentName, fMaxPreEventCountExp2 );
    }

void AliHLTControlledGatherer::StartProcessing()
    {
    fBaseObject->StartProcessing();
    }

void AliHLTControlledGatherer::PauseProcessing()
    {
    fBaseObject->PauseProcessing();
    }

void AliHLTControlledGatherer::ResumeProcessing()
    {
    fBaseObject->ResumeProcessing();
    }

void AliHLTControlledGatherer::StopProcessing()
    {
    fBaseObject->StopProcessing();
    }


AliHLTEventGathererNew::TPublisher* AliHLTControlledGatherer::CreatePublisher( unsigned long pubNdx, PublisherData* pd )
    {
    return new AliHLTEventGathererNew::TPublisher( fBaseObject, pubNdx, pd->fOwnPublisherName.c_str(), fMaxPreEventCountExp2 );
    }


AliHLTEventGathererNew::TSubscriber* AliHLTControlledGatherer::CreateSubscriber( unsigned long subNdx, SubscriberData* sd )
    {
    return new AliHLTEventGathererNew::TSubscriber( fBaseObject, subNdx, sd->fSubscriberID.c_str() );
    }


void AliHLTControlledGatherer::SetupComponents()
    {
    TParent::SetupComponents();
    fBaseObject->SetDescriptorHandler( fDescriptorHandler );
    pthread_mutex_lock( &fPublisherMutex );
    fBaseObject->SetPublisher( fPublishers.begin()->fPublisher );
    pthread_mutex_unlock( &fPublisherMutex );
    fBaseObject->SetStatusData( fStatus );
    if ( fGathererBroadcastTimeout_ms )
	fBaseObject->SetBroadcastTimeout( fGathererBroadcastTimeout_ms );
    if ( fGathererSORTimeout_ms )
	fBaseObject->SetSORTimeout( fGathererSORTimeout_ms );
    
    vector<TParent::SubscriberData>::iterator subIter, subEnd;
    pthread_mutex_lock( &fSubscriberMutex );
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    while ( subIter != subEnd )
	{
	if ( subIter->fSubscriber )
	    {
	    fBaseObject->AddSubscriber( subIter->fSubscriber, subIter->fPublisherProxy, subIter->fNdx );
	    }
	subIter++;
	}
    pthread_mutex_unlock( &fSubscriberMutex );
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
