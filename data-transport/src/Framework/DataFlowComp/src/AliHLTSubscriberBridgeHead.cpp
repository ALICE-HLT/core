/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSubscriberBridgeHead.hpp"
#include "AliHLTBridgeMsg.hpp"
#include "AliHLTBridgeBlock.hpp"
#include "AliHLTBridgeMsgs.h"
#include "AliHLTLog.hpp"
#include "AliHLTPublisherInterface.hpp"
#include "AliHLTEventDoneData.hpp"
#include "AliHLTEventAccounting.hpp"
#include "AliHLTHLTEventTriggerData.hpp"
#include "AliHLTEventMetaDataOutput.hpp"
#include "BCLAddressLogger.hpp"
#include <errno.h>
#ifdef OUTPUT_DEBUG
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#endif


AliHLTSubscriberBridgeHead::AliHLTSubscriberBridgeHead( const char* name, AliUInt32_t maxShmNoUseCount, int slotCountExp2 ):
    AliHLTSubscriberInterface( name ), 
    fMsgCache( 4, false, false ),
    fETSCache( slotCountExp2, true, false ),
    fTimer( (slotCountExp2<4) ? slotCountExp2 : (slotCountExp2-3) ),
    fTransferSignal( (slotCountExp2<3) ? slotCountExp2 : (slotCountExp2-2) ),
    fTransferThread( this, &AliHLTSubscriberBridgeHead::TransferLoop ),
    fEventDataCache( sizeof(EventData), (slotCountExp2<0) ? 4 : slotCountExp2 ),
    fEventSEDDs( slotCountExp2, true ),
    fRetryCallback( this ),
    fEventAccounting(NULL)
    {
    pthread_mutex_init( &fShmMutex, NULL );
    pthread_mutex_init( &fTransferMutex, NULL );
    pthread_mutex_init( &fEventSEDDMutex, NULL );
    pthread_mutex_init( &fMsgComMutex, NULL );
    pthread_mutex_init( &fComAccessMutex, NULL );
    pthread_mutex_init( &fComRecvMutex, NULL );
    pthread_mutex_init( &fDescriptorMutex, NULL );
    fMaxShmNoUseCount = maxShmNoUseCount;
    fMsgCom = NULL;
    fBlobMsgCom = NULL;
    fBlobCom = NULL;
    fRemoteMsgAddress = fRemoteBlobMsgAddress = NULL;
    fReceiveAddr = NULL;
    fCommunicationTimeout = 0;
    fGlobalTransferID = ~(BCLTransferID)0;
    fRemoteBlobSize = 0;
    fBufferManager = NULL;
    fDescriptorHandler = NULL;
    fTransferLoopQuitted = true;
    fQuitTransferLoop = false;
    fMsgLoopQuitted = true;
    fQuitMsgLoop = false;
    fBlobConnected = fMsgConnected = false;
    fConnectionDesired = false;
    fRetryTime = 1000; // 1 second.
    fMaxRetryCount = 0;
    fTimer.Start();
    fLAMProxy = NULL;
    fErrorCallback = &fDefaultErrorCallback;
    fStatus = NULL;
    fPaused = false;
    fOldestEventBirth_s = 0;
    fTransferSEDD = NULL;
    fTransferSEDDLength = 0;
    fTransferSignal.Unlock();
    fForwardMaxRetryEvents = true;
    fAllEventsPurged = false;
    fHighWaterMarkActive = false;
    fHighWaterMarkID = 0;
    fHighWaterMark = 100;
    fLowWaterMark = 10;
    fHighWaterMarkSent.tv_sec = fHighWaterMarkSent.tv_usec = 0;
    fAliceHLT = false;
    fRunNumber = 0;
    fOldBCLLogSet = false;
    fOldBCLLog = 0;
    }


AliHLTSubscriberBridgeHead::~AliHLTSubscriberBridgeHead()
    {
    vector<ShmData>::iterator iter;
    while ( fShmData.size()>0 )
	{
	iter = fShmData.begin();
	if ( iter->fShmID!= (AliUInt32_t)-1 )
	    {
	    if ( iter->fShmPtr )
		fShm.UnlockShm( iter->fShmID );
	    iter->fShmPtr = NULL;
	    fShm.ReleaseShm( iter->fShmID );
	    iter->fShmID = (AliUInt32_t)-1;
	    }
	fShmData.erase( iter );
	}
    pthread_mutex_destroy( &fShmMutex );
    pthread_mutex_destroy( &fTransferMutex );
    pthread_mutex_destroy( &fEventSEDDMutex );
    pthread_mutex_destroy( &fMsgComMutex );
    pthread_mutex_destroy( &fComAccessMutex );
    pthread_mutex_destroy( &fComRecvMutex );
    pthread_mutex_destroy( &fDescriptorMutex );
    if ( fReceiveAddr )
	fMsgCom->DeleteAddress( fReceiveAddr );
    if ( fTransferSEDD )
	delete [] (AliUInt8_t*)fTransferSEDD;
    }


int AliHLTSubscriberBridgeHead::Connect( bool doLock )
    {
    if ( !fOldBCLLogSet )
	{
	fOldBCLLog = gBCLLogLevelRef;
	fOldBCLLogSet = true;
	gBCLLogLevelRef = 0x70 & gLogLevel;
	}
    if ( fBlobConnected && fMsgConnected && fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	return 0;
	}
    fConnectionDesired = true;
    if ( doLock )
	pthread_mutex_lock( &fComAccessMutex );
    if ( !fMsgCom || !fBlobCom || !fRemoteMsgAddress || !fRemoteBlobMsgAddress )
	{
	if ( doLock )
	    pthread_mutex_unlock( &fComAccessMutex );
	return ENODEV;
	}
    int ret;
    if ( !fBlobConnected )
	{
#if 0
	LOG( AliHLTLog::kImportant, "AliHLTPublisherBridgeHead::MsgLoop", "Sleeping" )
	    << "Sleeping for 10s" << ENDLOG;
	sleep( 10 );
#endif
	if ( fCommunicationTimeout )
	    ret = fBlobCom->Connect( fRemoteBlobMsgAddress, fCommunicationTimeout );
	else
	    ret = fBlobCom->Connect( fRemoteBlobMsgAddress );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::Connect", "Blob Connection Error" )
		<< "Error connecting blob object. " << strerror(ret) << " (" 
		<< AliHLTLog::kDec << ret << ")." << ENDLOG;
	    if ( doLock )
		pthread_mutex_unlock( &fComAccessMutex );
	    if ( fStatus )
		{
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCNetworkErrorStateFlag;
		}
		fStatus->SetConnected( false, fBlobConnected );
		}
	    if ( fLAMProxy )
		fLAMProxy->LookAtMe();
	    return ret;
	    }
	fBlobConnected = true;
	LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::Connect", "Blob Object Connect" )
	    << "Blob object connected." << ENDLOG;
	if ( fStatus )
	    fStatus->SetConnected( false, fBlobConnected );
	}
    if ( fBlobMsgCom->CanLockConnection() )
	{
	if ( fCommunicationTimeout )
	    ret = fBlobMsgCom->LockConnection( fRemoteBlobMsgAddress, fCommunicationTimeout );
	else
	    ret = fBlobMsgCom->LockConnection( fRemoteBlobMsgAddress );
	}
    if ( fBlobCom->CanLockConnection() )
	{
	if ( fCommunicationTimeout )
	    ret = fBlobCom->LockConnection( fRemoteBlobMsgAddress, fCommunicationTimeout );
	else
	    ret = fBlobCom->LockConnection( fRemoteBlobMsgAddress );
	}
    if ( doLock )
	pthread_mutex_lock( &fMsgComMutex );
    if ( !fMsgConnected )
	{
	if ( fCommunicationTimeout )
	    ret = fMsgCom->Connect( fRemoteMsgAddress, fCommunicationTimeout );
	else
	    ret = fMsgCom->Connect( fRemoteMsgAddress );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::Connect", "Msg Connection Error" )
		<< "Error connecting msg object: " << strerror(ret) << " (" 
		<< AliHLTLog::kDec << ret << ")." << ENDLOG;
	    if ( doLock )
		{
		pthread_mutex_unlock( &fMsgComMutex );
		pthread_mutex_unlock( &fComAccessMutex );
		Disconnect();
		}
	    else
		Disconnect( true, true, 1000, false );
	    if ( fStatus )
		{
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCNetworkErrorStateFlag;
		}    
		fStatus->SetConnected( true, fMsgConnected );
		}
	    if ( fLAMProxy )
		fLAMProxy->LookAtMe();
	    return ret;
	    }
	fMsgConnected = true;
	LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::Connect", "Msg Object Connect" )
	    << "Message object connected." << ENDLOG;
	if ( fStatus )
	    fStatus->SetConnected( true, fMsgConnected );
	}
    if ( fMsgCom->CanLockConnection() )
	{
	if ( fCommunicationTimeout )
	    ret = fMsgCom->LockConnection( fRemoteMsgAddress, fCommunicationTimeout );
	else
	    ret = fMsgCom->LockConnection( fRemoteMsgAddress );
	}
    AliHLTBridgeMsg msg;
    AliHLTBridgeMsgStruct* pmsg;
    AliUInt8_t* tmpPtr;
    AliUInt32_t lenM, lenBM;
    unsigned long msgLen = msg.GetData()->fLength;
    BCLNetworkData::Transform( fMsgCom->GetAddress()->fLength, lenM, fMsgCom->GetAddress()->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );
    msgLen += lenM;
    BCLNetworkData::Transform( fBlobMsgCom->GetAddress()->fLength, lenBM, fBlobMsgCom->GetAddress()->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );
    msgLen += lenBM;
    //pmsg = (AliHLTBridgeMsgStruct*)new AliUInt8_t[ msgLen ];
    pmsg = (AliHLTBridgeMsgStruct*)fMsgCache.Get( msgLen );
    if ( !pmsg )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::Connect", "Out of memory" )
	    << "Out of memory trying to allocate BridgeConnect msg of " << AliHLTLog::kDec
	    << msgLen << " bytes." << ENDLOG;
	pmsg = msg.GetData();
	}
    else
	{
	tmpPtr = (AliUInt8_t*)pmsg;
	memcpy( tmpPtr, msg.GetData(), msg.GetData()->fLength );
	tmpPtr += msg.GetData()->fLength;
	memcpy( tmpPtr, fMsgCom->GetAddress(), lenM );
	tmpPtr += lenM;
	memcpy( tmpPtr, fBlobMsgCom->GetAddress(), lenBM );
	pmsg->fLength += lenM+lenBM;
	pmsg->fEventID = lenM;
	pmsg->fDataType = lenBM;
	}
    pmsg->fMsgType = kAliHLTBridgeConnectMsg;
    if ( fCommunicationTimeout )
	ret = fMsgCom->Send( fRemoteMsgAddress, pmsg, fCommunicationTimeout );
    else
	ret = fMsgCom->Send( fRemoteMsgAddress, pmsg );
    if ( pmsg != msg.GetData() )
	{
	fMsgCache.Release( (uint8*)pmsg );
	//delete [] (AliUInt8_t*)pmsg;
	pmsg = NULL;
	}
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::Connect", "Connect Msg Send Error" )
		<< "Error sending connect msg: " << strerror(ret) << " (" 
		<< AliHLTLog::kDec << ret << ")." << ENDLOG;
	if ( doLock )
	    {
	    pthread_mutex_unlock( &fMsgComMutex );
	    pthread_mutex_unlock( &fComAccessMutex );
	    Disconnect( true );
	    }
	else
	    Disconnect( true, true, 1000, false );
	if ( fStatus )
	    {
	    MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCNetworkErrorStateFlag;
	    }
	if ( fLAMProxy )
	    fLAMProxy->LookAtMe();
	return ret;
	}
    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::Connect", "Connect Msg Sent" )
	<< "Connect message sent." << ENDLOG;
    if ( doLock )
	{
	pthread_mutex_unlock( &fMsgComMutex );
	pthread_mutex_unlock( &fComAccessMutex );
	}
    if ( fErrorCallback )
	fErrorCallback->ConnectionEstablished( this );
    LOGG( AliHLTLog::kInformational, "AliHLTSubscriberBridgeHead::Connect", "Connection established", tmpLog )
	<< "Connection established to ";
	BCLAddressLogger::LogAddress( tmpLog, *fRemoteMsgAddress )
	    << "/";
	BCLAddressLogger::LogAddress( tmpLog, *fRemoteBlobMsgAddress )
	    << "." << ENDLOG;
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCNetworkErrorStateFlag;
	}
    return 0;
    }

int AliHLTSubscriberBridgeHead::Disconnect( bool initiator, bool do_timeout, AliUInt32_t timeout_ms, bool doLock )
    {
    fConnectionDesired = false;
    if ( doLock )
	pthread_mutex_lock( &fComAccessMutex );
    if ( !fMsgCom || !fBlobCom || !fRemoteMsgAddress || !fRemoteBlobMsgAddress )
	{
	if ( doLock )
	    pthread_mutex_unlock( &fComAccessMutex );
	if ( fOldBCLLogSet )
	    {
	    fOldBCLLogSet = false;
	    gBCLLogLevelRef = fOldBCLLog;
	    }
	return ENODEV;
	}
    AliHLTBridgeMsg msg;
    msg.GetData()->fMsgType = kAliHLTBridgeDisconnectMsg;
    int ret;
    if ( doLock )
	pthread_mutex_lock( &fMsgComMutex );
    if ( fMsgConnected )
	{
	if ( initiator )
	    {
	    if ( do_timeout )
		ret = fMsgCom->Send( fRemoteMsgAddress, msg.GetData(), timeout_ms );
	    else if ( fCommunicationTimeout )
		ret = fMsgCom->Send( fRemoteMsgAddress, msg.GetData(), fCommunicationTimeout );
	    else
		ret = fMsgCom->Send( fRemoteMsgAddress, msg.GetData() );
	    }
	LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::Disconnect", "Disconnect Msg Sent" )
	    << "Disconnect message sent." << ENDLOG;
	if ( fCommunicationTimeout )
	    ret = fMsgCom->Disconnect( fRemoteMsgAddress, fCommunicationTimeout );
	else
	    ret = fMsgCom->Disconnect( fRemoteMsgAddress );
	fMsgConnected = false;
	LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::Disconnect", "Msg Object Disconnect" )
	    << "Message object disconnected." << ENDLOG;
	if ( fStatus )
	    fStatus->SetConnected( true, fMsgConnected );
	}
    if ( doLock )
	pthread_mutex_unlock( &fMsgComMutex );
    if ( fBlobConnected )
	{
	if ( fCommunicationTimeout )
	    ret = fBlobCom->Disconnect( fRemoteBlobMsgAddress, fCommunicationTimeout );
	else
	    ret = fBlobCom->Disconnect( fRemoteBlobMsgAddress );
	fBlobConnected = false;
	LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::Disconnect", "Blob Object Disconnect" )
	    << "Blob object disconnected." << ENDLOG;
	if ( fStatus )
	    fStatus->SetConnected( false, fBlobConnected );
	}
    if ( fBufferManager && fGlobalTransferID!=~(BCLTransferID)0 )
	{
	AliHLTShmID id;
	id.fShmType = kInvalidShmType;
	id.fKey.fID = fGlobalTransferID;
	if ( !fBufferManager->DeleteBuffer( id, true ) )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::Disconnect", "Buffer Deletion failed" )
		<< "Deletion of buffer with shared memory id 0x" << AliHLTLog::kHex << id.fKey.fID 
		<< " (" << AliHLTLog::kDec << id.fKey.fID << ") failed." << ENDLOG;
	    }
	if ( fStatus )
	    {
	    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	    fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
	    fStatus->fHLTStatus.fTotalOutputBuffer = fBufferManager->GetTotalBufferSize();
	    }
	fGlobalTransferID = ~(BCLTransferID)0;
	fRemoteBlobSize = 0;
	if ( fStatus )
	    {
// XXXX TODO LOCKING XXXX
	    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	    fStatus->fHLTStatus.fTotalOutputBuffer = fBufferManager->GetTotalBufferSize();
	    }
	}
    if ( doLock )
	pthread_mutex_unlock( &fComAccessMutex );
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~kAliHLTPCNetworkErrorStateFlag;
	}
    if ( fOldBCLLogSet )
	{
	fOldBCLLogSet = false;
	gBCLLogLevelRef = fOldBCLLog;
	}
    return 0;
    }

void AliHLTSubscriberBridgeHead::Start()
    {
    fTransferThread.Start();
    }

void AliHLTSubscriberBridgeHead::Stop()
    {
    QuitTransferLoop();
    }


int AliHLTSubscriberBridgeHead::NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
					  const AliHLTSubEventDataDescriptor& sbevent,
					  const AliHLTEventTriggerStruct& trigger )
    {
    if ( fEventAccounting )
	fEventAccounting->NewEvent( publisher.GetName(), &sbevent );
    if ( fStatus )
	{
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	if ( eventID.fType!=kAliEventTypeTick && eventID.fType!=kAliEventTypeReconfigure && eventID.fType!=kAliEventTypeDCSUpdate )
	    fStatus->fHLTStatus.fReceivedEventCount++;
	fStatus->Touch();
	}
    if ( sbevent.fOldestEventBirth_s > fOldestEventBirth_s )
	{
	fOldestEventBirth_s = sbevent.fOldestEventBirth_s;
	CleanupOldEvents();
	}
    int ret;
    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::NewEvent", "New Event" )
	<< "New event 0x" << AliHLTLog::kHex << eventID << " ("
	<< AliHLTLog::kDec << eventID << ") with "
	<< sbevent.fDataBlockCount << " data blocks received."
	<< ENDLOG;
    if ( !fDescriptorHandler )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::NewEvent", "No descriptor handler" )
	    << "No descriptor handler specified." << ENDLOG;
	return ENXIO;
	}
    AliHLTSubEventDataDescriptor* sedd;
    //sedd = (AliHLTSubEventDataDescriptor*)new AliUInt8_t[ sbevent.fHeader.fLength ];
    ret = pthread_mutex_lock( &fDescriptorMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::NewEvent", "Descriptor Handler Mutex Error" )
	    << "Error locking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    sedd = fDescriptorHandler->GetFreeEventDescriptor( sbevent.fEventID, sbevent.fDataBlockCount );
    ret = pthread_mutex_unlock( &fDescriptorMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::NewEvent", "Descriptor Handler Mutex Error" )
	    << "Error unlocking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    if ( !sedd )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::NewEvent", "Out of memory" )
	    << "Out of memory allocating new Sub-Event Descriptor." << ENDLOG;
	return ENOMEM;
	}
    unsigned long length = sbevent.fHeader.fLength;
    if ( length > sedd->fHeader.fLength )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::NewEvent", "Event Descriptor Error" )
	    << "Sub-Event Descriptor mismatch in length and block count." << ENDLOG;
	length = sedd->fHeader.fLength;
	}
    memcpy( sedd, &sbevent, length );
    //EventData* ed = new EventData;
    AliHLTEventTriggerStruct* ets;
    //ets = (AliHLTEventTriggerStruct*) new AliUInt8_t[ trigger.fHeader.fLength ];
    ets = (AliHLTEventTriggerStruct*)fETSCache.Get( trigger.fHeader.fLength );
    if ( !ets )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::NewEvent", "Out of memory" )
	    << "Out of memory allocating new event trigger data of " << AliHLTLog::kDec
	    << trigger.fHeader.fLength << " bytes." << ENDLOG;
	ret = pthread_mutex_lock( &fDescriptorMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::NewEvent", "Descriptor Handler Mutex Error" )
		<< "Error locking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << "). Continuing..." << ENDLOG;
	    }
	fDescriptorHandler->ReleaseEventDescriptor( sbevent.fEventID );
	ret = pthread_mutex_unlock( &fDescriptorMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::NewEvent", "Descriptor Handler Mutex Error" )
		<< "Error unlocking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << "). Continuing..." << ENDLOG;
	    }
	return ENOMEM;
	}
    memcpy( ets, &trigger, trigger.fHeader.fLength );

    EventData* ed = (EventData*)fEventDataCache.Get();
    if ( !ed )
	{
	ret = pthread_mutex_lock( &fDescriptorMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::NewEvent", "Descriptor Handler Mutex Error" )
		<< "Error locking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << "). Continuing..." << ENDLOG;
	    }
	fDescriptorHandler->ReleaseEventDescriptor( sbevent.fEventID );
	ret = pthread_mutex_unlock( &fDescriptorMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::NewEvent", "Descriptor Handler Mutex Error" )
		<< "Error unlocking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << "). Continuing..." << ENDLOG;
	    }
	if ( ets )
	    {
	    fETSCache.Release( (uint8*)ets );
	    ets = NULL;
	    //delete [] (AliUInt8_t*)ets;
	    }
	LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::NewEvent", "Out of memory" )
	    << "Out of memory allocating new event data structure." << ENDLOG;
	return ENOMEM;
	}
    ed->fSEDD = sedd;
    ed->fETS = ets;
    ed->fPub = &publisher;
    ed->fRetryCount = 0;
    ed->fBufferNdx = ~(AliUInt32_t)0;
    ed->fBufferOffset = 0;
    ed->fTransferAborted = false;
    ed->fTransferInProgress = true;
    ed->fEventDone = false;
    ed->fForceEmptyForward = false;
    ret = pthread_mutex_lock( &fEventSEDDMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::NewEvent", "Event SEDD Mutex Error" )
	    << "Error locking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    fEventSEDDs.Add( ed );
    fAllEventsPurged = false;
    unsigned long long eventSEDDCnt = fEventSEDDs.GetCnt();
    ret = pthread_mutex_unlock( &fEventSEDDMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::NewEvent", "Event SEDD Mutex Error" )
	    << "Error unlocking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    fTransferSignal.AddNotificationData( ed );
    if ( fStatus )
	{
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	fStatus->fHLTStatus.fPendingInputEventCount = fTransferSignal.GetNotificationDataCnt();
	fStatus->fHLTStatus.fPendingOutputEventCount = eventSEDDCnt-fTransferSignal.GetNotificationDataCnt();
	}
    fTransferSignal.Signal();
    return 0;
    }

int AliHLTSubscriberBridgeHead::EventCanceled( AliHLTPublisherInterface&, AliEventID_t eventID )
    {
    int ret; 
    bool found = false;
    unsigned long ndx;
    ret = pthread_mutex_lock( &fEventSEDDMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::EventCanceled", "Event SEDD Mutex Error" )
	    << "Error locking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    if ( MLUCVectorSearcher<PEventData,AliEventID_t>::FindElement( fEventSEDDs, &FindEventDataByEvent, eventID, ndx ) )
	{
	PEventData* iter = fEventSEDDs.GetPtr( ndx );
	(*iter)->fTransferAborted = true;
	found = (*iter)->fTransferInProgress;
	if ( !found )
	    {
	    ret = pthread_mutex_lock( &fDescriptorMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::EventCanceled", "Descriptor Handler Mutex Error" )
		    << "Error locking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << "). Continuing..." << ENDLOG;
		}
	    if ( fDescriptorHandler )
		fDescriptorHandler->ReleaseEventDescriptor( eventID );
	    ret = pthread_mutex_unlock( &fDescriptorMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::EventCanceled", "Descriptor Handler Mutex Error" )
		    << "Error unlocking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << "). Continuing..." << ENDLOG;
		}
	    if ( (*iter)->fETS )
		{
		fETSCache.Release( (uint8*)( (*iter)->fETS ) );
		//delete [] (AliUInt8_t*)(*iter)->fETS;
		}
	    if ( (*iter)->fBufferNdx != ~(AliUInt32_t)0 && fBufferManager )
		{
		if ( !fBufferManager->ReleaseBlock( (*iter)->fBufferNdx, (*iter)->fBufferOffset ) )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::EventCanceled", "Unable to release block" )
			<< "Unable to release block with buffer index " << AliHLTLog::kDec
			<< (*iter)->fBufferNdx << "." << ENDLOG;
		    }
		if ( fStatus )
		    {
		    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
		    fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
		    }
		(*iter)->fBufferNdx = ~(AliUInt32_t)0;
		(*iter)->fBufferOffset = 0;
		if ( fStatus )
		    {
// XXXX TODO LOCKING XXXX
		    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
		    fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
		    }
		}
	    
	    //delete *iter;
	    (*iter)->fSEDD = NULL;
	    (*iter)->fETS = NULL;
	    (*iter)->fPub = NULL;
	    fEventDataCache.Release( (uint8*)(*iter) );
	    
	    }
	fEventSEDDs.Remove( ndx );
	}
    ret = pthread_mutex_unlock( &fEventSEDDMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::EventCanceled", "Event SEDD Mutex Error" )
	    << "Error unlocking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    if ( !found && fConnectionDesired )
	{
	LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::EventCanceled", "Sending Cancel Event" )
	    << "Sending event canceled message to publisher bridge head: " 
	    << ENDLOG;
	AliHLTBridgeMsg msg;
	msg.GetData()->fMsgType = kAliHLTBridgeEventCanceledMsg;
	msg.GetData()->fEventType = eventID.fType;
	msg.GetData()->fEventID = eventID.fNr;
	msg.GetData()->fBlockCount = 0;
	msg.GetData()->fFlags = kWaitDelivery;
	if ( !fMsgConnected )
	    ret = Connect();
	if ( !fMsgConnected )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::EventCanceled", "Connection error" )
		<< "Unable to connect to publisher bridge head: " 
		<< strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	    return ret;
	    }
	pthread_mutex_lock( &fMsgComMutex );
	if ( fCommunicationTimeout )
	    ret = fMsgCom->Send( fRemoteMsgAddress, msg.GetData(), fCommunicationTimeout );
	else
	    ret = fMsgCom->Send( fRemoteMsgAddress, msg.GetData() );
	pthread_mutex_unlock( &fMsgComMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::EventCanceled", "Msg Send Error" )
		<< "Error sending event canceled message to publisher bridge head: " 
		<< strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	    if ( fErrorCallback )
		fErrorCallback->ConnectionError( this );
	    //Disconnect();
	    return ret;
	    }
	}
    return 0;
    }


int AliHLTSubscriberBridgeHead::SubscriptionCanceled( AliHLTPublisherInterface& )
    {
    if ( !fConnectionDesired )
	return 0;
    int ret;
    AliHLTBridgeMsg msg;
    msg.GetData()->fMsgType = kAliHLTBridgeSubscriptionCanceledMsg;
    msg.GetData()->fFlags = kWaitDelivery;
    if ( !fMsgConnected )
	ret = Connect();
    if ( !fMsgConnected )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::SubscriptionCanceled", "Connection error" )
	    << "Unable to connect to publisher bridge head: " 
	    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	return ret;
	}
    pthread_mutex_lock( &fMsgComMutex );
    if ( fCommunicationTimeout )
	ret = fMsgCom->Send( fRemoteMsgAddress, msg.GetData(), fCommunicationTimeout );
    else
	ret = fMsgCom->Send( fRemoteMsgAddress, msg.GetData() );
    pthread_mutex_unlock( &fMsgComMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::SubscriptionCanceled", "Msg Send Error" )
	    << "Error sending subscription canceled message to publisher bridge head: " 
	    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	if ( fErrorCallback )
	    fErrorCallback->ConnectionError( this );
	//Disconnect();
	return ret;
	}
    return 0;
    }


int AliHLTSubscriberBridgeHead::ReleaseEventsRequest( AliHLTPublisherInterface& )
    {
    if ( !fConnectionDesired )
	return 0;
    int ret;
    AliHLTBridgeMsg msg;
    msg.GetData()->fMsgType = kAliHLTBridgeReleaseEventsRequestMsg;
    msg.GetData()->fFlags = kWaitDelivery;
    bool failed = false;
    if ( !fMsgConnected )
	ret = Connect();
    if ( !fMsgConnected )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::ReleaseEventsRequest", "Connection error" )
	    << "Unable to connect to publisher bridge head: " 
	    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	failed = true;
	}
    if ( !failed )
	{
	pthread_mutex_lock( &fMsgComMutex );
	if ( fCommunicationTimeout )
	    ret = fMsgCom->Send( fRemoteMsgAddress, msg.GetData(), fCommunicationTimeout );
	else
	    ret = fMsgCom->Send( fRemoteMsgAddress, msg.GetData() );
	pthread_mutex_unlock( &fMsgComMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::ReleaseEventsRequest", "Msg Send Error" )
		<< "Error sending release event request message to publisher bridge head: " 
		<< strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	    if ( fErrorCallback )
		fErrorCallback->ConnectionError( this );
	    //Disconnect();
	    failed = true;
	    }
	}
    if ( failed )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::ReleaseEventsRequest", "Releasing Events" )
	    << "Unable to send release event request message to publisher bridge head. " 
	    << ENDLOG;
	}
    // Make this dependant on some settable flag, or a number of retries???
    // XXX
//     if ( failed )
// 	{
// 	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::ReleaseEventsRequest", "Releasing Events" )
// 	    << "Unable to send release event request message to publisher bridge head. " 
// 	    << "Cancelling events locally..." << ENDLOG;
	
// 	ret = pthread_mutex_lock( &fEventSEDDMutex );
// 	if ( ret )
// 	    {
// 	    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::ReleaseEventsRequest", "Event SEDD Mutex Error" )
// 		<< "Error locking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
// 		<< ret << "). Continuing..." << ENDLOG;
// 	    }
// 	unsigned long ndx;
// 	PEventData* iter;
// 	AliEventID_t evtID;
// 	AliHLTPublisherInterface* evtPub;
// 	while ( fEventSEDDs.FindElement( &FindEventDataByPublisher, (uint64)&publisher, ndx ) )
// 	    {
// 	    iter = fEventSEDDs.GetPtr( ndx );
// 	    if ( (*iter)->fSEDD )
// 		{
// 		evtID = (*iter)->fSEDD->fEventID;
// 		//delete [] (AliUInt8_t*)((*iter)->fSEDD);
// 		ret = pthread_mutex_lock( &fDescriptorMutex );
// 		if ( ret )
// 		    {
// 		    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::ReleaseEventsRequest", "Descriptor Handler Mutex Error" )
// 			<< "Error locking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
// 			<< ret << "). Continuing..." << ENDLOG;
// 		    }
// 		if ( fDescriptorHandler )
// 		    fDescriptorHandler->ReleaseEventDescriptor( evtID );
// 		ret = pthread_mutex_unlock( &fDescriptorMutex );
// 		if ( ret )
// 		    {
// 		    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::ReleaseEventsRequest", "Descriptor Handler Mutex Error" )
// 			<< "Error unlocking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
// 			<< ret << "). Continuing..." << ENDLOG;
// 		    }
// 		}
// 	    if ( (*iter)->fBufferNdx != ~(AliUInt32_t)0 && fBufferManager )
// 		fBufferManager->ReleaseBlock( (*iter)->fBufferNdx, (*iter)->fBufferOffset );
	    
// 	    //delete (*iter);
// 	    fEventDataCache.Release( (uint8*)(*iter) );
// 	    ret = evtPub->EventDone( *this, evtID );
// 	    if ( ret )
// 		{
// 		LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::ReleaseEventsRequest", "Event Done Error" )
// 		    << "Error sending event done to publisher '" << evtPub->GetName() 
// 		    << "': " << strerror(ret) << AliHLTLog::kDec << " (" << ret << "). Continuing..."
// 		    << ENDLOG;
// 		}
// 	    fEventSEDDs.Remove( ndx );
// 	    }
// 	ret = pthread_mutex_unlock( &fEventSEDDMutex );
// 	if ( ret )
// 	    {
// 	    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::ReleaseEventsRequest", "Event SEDD Mutex Error" )
// 		<< "Error unlocking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
// 		<< ret << "). Continuing..." << ENDLOG;
// 	    }
// 	return ret;
// 	}
    return 0;
    }


int AliHLTSubscriberBridgeHead::Ping( AliHLTPublisherInterface& publisher )
    {
    publisher.PingAck( *this );
    return 0;
    }


int AliHLTSubscriberBridgeHead::PingAck( AliHLTPublisherInterface& )
    {
    return 0;
    }

void AliHLTSubscriberBridgeHead::MsgLoop()
    {
    //addr = fMsgCom->NewAddress();
    BCLMessageStruct* bMsg;
    AliHLTBridgeMsgStruct* msg;
    AliHLTBridgeMsg msgClass;
    AliUInt32_t msgBaseLen = msgClass.GetData()->fLength;
    fMsgLoopQuitted = false;
    fQuitMsgLoop = false;
    //vector<EventData*>::iterator iter, end;
    PEventData* iter;
    int ret;
    AliUInt32_t wrongMsgSizeCnt = 0;
    while ( !fQuitMsgLoop )
	{
	pthread_mutex_lock( &fComRecvMutex );
	ret = fMsgCom->Receive( fReceiveAddr, bMsg, 2000 ); 
	// Timeout for 2s, so that anybody else can get a 
	// regular chance at getting the fComRecvMutex.
	if ( ret && ret!=ETIMEDOUT )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::MsgLoop", "Msg receive error" )
		<< "Error receiving message from publisher bridge head: "
		<< strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	    if ( ret == EMSGSIZE )
		{
		if ( wrongMsgSizeCnt++ >= 8 )
		    {
		    fMsgCom->Reset();
		    wrongMsgSizeCnt = 0;
		    }
		}
	    }
	if ( fQuitMsgLoop )
	    {
	    pthread_mutex_unlock( &fComRecvMutex );
	    break;
	    }
	if ( ret )
	    {
	    pthread_mutex_unlock( &fComRecvMutex );
	    continue;
	    }
	if ( !fReceiveAddr || !bMsg )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::MsgLoop", "Msg receive error" )
		<< "Null pointer error receiving message from publisher bridge head: "
		<< strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
	    if ( bMsg )
		fMsgCom->ReleaseMsg( bMsg );
	    pthread_mutex_unlock( &fComRecvMutex );
	    continue;
	    }
	msg = (AliHLTBridgeMsgStruct*)bMsg;
	msgClass.Adopt( msg );
	switch ( msg->fMsgType )
	    {
	    case kAliHLTBridgeConnectMsg:
		{
		if ( !fMsgConnected || !fBlobConnected )
		    {
		    AliUInt32_t lenM, lenBM, lenMSend, lenBMSend;
		    lenMSend = (AliUInt32_t)msg->fEventID;
		    lenBMSend = (AliUInt32_t)msg->fDataType;
		    BCLNetworkData::Transform( fRemoteMsgAddress->fLength, lenM, fRemoteMsgAddress->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );
		    BCLNetworkData::Transform( fRemoteBlobMsgAddress->fLength, lenBM, fRemoteBlobMsgAddress->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );
		    if ( msg->fLength != msgBaseLen+lenM+lenBM )
			{
			LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::MsgLoop", "Incorrect message size" )
			    << "Connect message has incorrect message size " << AliHLTLog::kDec
			    << msg->fLength << " (" << msgBaseLen+lenM+lenBM
			    << " expected) for contained remote addresses. Using pre-defined addresses instead."
			    << ENDLOG;
			}
		    else if ( lenMSend != lenM )
			{
			LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::MsgLoop", "Incorrect remote address size" )
			    << "Connect message has incorrrect remote msg address length of " << AliHLTLog::kDec
			    << lenMSend << " (" << lenM << " expected). Using pre-defined addresses instead."
			    << ENDLOG;
			}
		    else if ( lenBMSend != lenBM )
			{
			LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::MsgLoop", "Incorrect remote address size" )
			    << "Connect message has incorrrect remote blob msg address length of " << AliHLTLog::kDec
			    << lenBMSend << " (" << lenBM << " expected). Using pre-defined addresses instead."
			    << ENDLOG;
			}
		    else
			{
			BCLAbstrAddressStruct *newMsg, *newBlobMsg;
			newMsg = (BCLAbstrAddressStruct*)(((AliUInt8_t*)msg)+msgBaseLen);
			newBlobMsg = (BCLAbstrAddressStruct*)(((AliUInt8_t*)newMsg)+lenM);
			memcpy( fRemoteMsgAddress, newMsg, lenM );
			memcpy( fRemoteBlobMsgAddress, newBlobMsg, lenBM );
			}
		    
		    ret = Connect();
		    }
		if ( !fMsgConnected || !fBlobConnected )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::MsgLoop", "Connection error" )
			<< "Unable to connect to publisher bridge head: " 
			<< strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
		    if ( fErrorCallback )
			fErrorCallback->ConnectionError( this );
		    //Disconnect();
		    break;
		    }
		
		pthread_mutex_lock( &fComAccessMutex );
		if ( fBlobCom )
		    fRemoteBlobSize = fBlobCom->GetRemoteBlobSize( fRemoteBlobMsgAddress, fCommunicationTimeout );
		if ( !fRemoteBlobSize )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::MsgLoop", "Blob error" )
			<< "Unable to determine remote blob size. Disconnecting..." << ENDLOG;
		    pthread_mutex_unlock( &fComAccessMutex );
		    if ( fErrorCallback )
			fErrorCallback->ConnectionError( this );
		    //Disconnect();
		    break;
		    }
		if ( fBlobCom )
		    fGlobalTransferID = fBlobCom->PrepareBlobTransfer( fRemoteBlobMsgAddress, fRemoteBlobSize, fCommunicationTimeout );
		pthread_mutex_unlock( &fComAccessMutex );
		if ( fGlobalTransferID == ~(BCLTransferID)0 )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::MsgLoop", "Blob error" )
			<< "Unable to determine remote blob size. Disconnecting..." << ENDLOG;
		    if ( fErrorCallback )
			fErrorCallback->ConnectionError( this );
		    //Disconnect();
		    break;
		    }
		AliHLTShmID shmID;
		shmID.fShmType = kInvalidShmType;
		shmID.fKey.fID = fGlobalTransferID;
		if ( !fBufferManager )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::MsgLoop", "No buffer manager" )
			<< "No buffer manager. Don't know what to do..." << ENDLOG;
		    }
		if ( fBufferManager && !fBufferManager->AddBuffer( shmID, fRemoteBlobSize ) )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::MsgLoop", "Buffer manager error" )
			<< "Unable to add buffer to buffer manager. Don't know what to do..." << ENDLOG;
		    }
		else if ( fBufferManager )
		    {
		    if ( fStatus )
			{
// XXXX TODO LOCKING XXXX
			MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
			fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
			fStatus->fHLTStatus.fTotalOutputBuffer = fBufferManager->GetTotalBufferSize();
			}
		    AliHLTBridgeMsg msg1;
		    msg1.GetData()->fMsgType = kAliHLTBridgeGlobalTransferIDMsg;
		    msg1.GetData()->fFlags = kWaitDelivery;
		    msg1.GetData()->fTransferID = fGlobalTransferID;
		    pthread_mutex_lock( &fMsgComMutex );
		    if ( fCommunicationTimeout )
			ret = fMsgCom->Send( fRemoteMsgAddress, msg1.GetData(), fCommunicationTimeout );
		    else
			ret = fMsgCom->Send( fRemoteMsgAddress, msg1.GetData() );
		    pthread_mutex_unlock( &fMsgComMutex );
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::MsgLoop", "Msg Send Error" )
			    << "Error sending global transfer ID message to publisher bridge head: " 
			    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
			if ( fErrorCallback )
			    fErrorCallback->ConnectionError( this );
			//Disconnect();
			}
		    }

		LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::MsgLoop", "Connection State" )
		    << "fStatus: 0x" << AliHLTLog::kHex << (unsigned long)fStatus << " - fMsgConnected: "
		    << (fMsgConnected ? "true" : "false") << " - fBlobConnected: "
		    << (fBlobConnected ? "true" : "false") << "." << ENDLOG;
		if ( fStatus )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::MsgLoop", "Connection State" )
			<< "fStatus->fProcessStatus.fState&kAliHLTPCChangingStateFlag: " << AliHLTLog::kHex
			<< (fStatus->fProcessStatus.fState&kAliHLTPCChangingStateFlag)
			<< " fStatus->fProcessStatus.fState&kAliHLTPCConnectedStateFlag: "
			<< (fStatus->fProcessStatus.fState&kAliHLTPCConnectedStateFlag)
			<< " fStatus->fProcessStatus.fState&kAliHLTPCNetworkErrorStateFlag: "
			<< (fStatus->fProcessStatus.fState&kAliHLTPCNetworkErrorStateFlag)
			<< "." << ENDLOG;
		    }
		if ( fStatus )
		    {
		    if ( fMsgConnected )
			fStatus->SetConnected( true, fMsgConnected );
		    if ( fBlobConnected )
			fStatus->SetConnected( false, fBlobConnected );
			{
			MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			if ( fStatus->fProcessStatus.fState&kAliHLTPCChangingStateFlag )
			    fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
			}
		    if ( fMsgConnected && fBlobConnected )
			{
			MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			if ( !(fStatus->fProcessStatus.fState&kAliHLTPCConnectedStateFlag) )
			    fStatus->fProcessStatus.fState |= kAliHLTPCConnectedStateFlag;
			if ( fStatus->fProcessStatus.fState&kAliHLTPCNetworkErrorStateFlag )
			    fStatus->fProcessStatus.fState &= ~kAliHLTPCConnectedStateFlag;
			}
		    else
			{
			MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			if ( fStatus->fProcessStatus.fState&kAliHLTPCConnectedStateFlag )
			    fStatus->fProcessStatus.fState &= ~kAliHLTPCConnectedStateFlag;
			}
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::MsgLoop", "Connection State" )
			<< "fStatus->fProcessStatus.fState&kAliHLTPCChangingStateFlag: " << AliHLTLog::kHex
			<< (fStatus->fProcessStatus.fState&kAliHLTPCChangingStateFlag)
			<< " fStatus->fProcessStatus.fState&kAliHLTPCConnectedStateFlag: "
			<< (fStatus->fProcessStatus.fState&kAliHLTPCConnectedStateFlag)
			<< " fStatus->fProcessStatus.fState&kAliHLTPCNetworkErrorStateFlag: "
			<< (fStatus->fProcessStatus.fState&kAliHLTPCNetworkErrorStateFlag)
			<< "." << ENDLOG;

		    }

		ret = pthread_mutex_lock( &fEventSEDDMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::MsgLoop", "Event SEDD Mutex Error" )
			<< "Error locking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
			<< ret << "). Continuing..." << ENDLOG;
		    }
		fEventSEDDs.Iterate( &IterateEventDataAddTransferSignal, reinterpret_cast<void*>(&fTransferSignal) );
		ret = pthread_mutex_unlock( &fEventSEDDMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::MsgLoop", "Event SEDD Mutex Error" )
			<< "Error unlocking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
			<< ret << "). Continuing..." << ENDLOG;
		    }
		fTransferSignal.Signal();
		break;
		}
	    case kAliHLTBridgeDisconnectMsg:
		LOG( AliHLTLog::kInformational, "AliHLTSubscriberBridgeHead::MsgLoop", "Disconnect msg received" )
		    << "Received disconnect message from publisher bridge head." << ENDLOG;
		Disconnect( false );
		break;
	    case kAliHLTBridgeEventDoneMsg:
		{
		if ( fEventAccounting )
		    {
		    MLUCString tmp;
		    tmp = GetName();
		    tmp += " remote partner";
		    fEventAccounting->EventFinished( tmp.c_str(), AliEventID_t( msg->fEventType, msg->fEventID ) );
		    }
		LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::MsgLoop", "Event Done" )
		    << "Event done bridge message received for event 0x" << AliHLTLog::kHex
		    << msg->fEventID << " (" << AliHLTLog::kDec << msg->fEventID
		    << ")." << ENDLOG;
		ret = pthread_mutex_lock( &fEventSEDDMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::MsgLoop", "Event SEDD Mutex Error" )
			<< "Error locking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
			<< ret << "). Continuing..." << ENDLOG;
		    }
		bool found = false;
		unsigned long ndx;
		AliHLTPublisherInterface* evtPub = NULL;
		AliHLTBridgeMsg msgClass1;
		AliHLTEventDoneData* edd, dummyEDD;
		if ( msg->fLength > msgClass1.GetData()->fLength )
		    edd = (AliHLTEventDoneData*)( ((AliUInt8_t*)msg) + msgClass1.GetData()->fLength );
		else
		    {
		    edd = &dummyEDD;
		    AliHLTMakeEventDoneData( edd, AliEventID_t( msg->fEventType, msg->fEventID ) );
		    }
		if ( MLUCVectorSearcher<PEventData,AliEventID_t>::FindElement( fEventSEDDs, &FindEventDataByEvent, AliEventID_t( msg->fEventType, msg->fEventID ), ndx ) )
		    {
		    iter = fEventSEDDs.GetPtr( ndx );
		    found = true;
		    evtPub = (*iter)->fPub;
		    if ( !(*iter)->fTransferInProgress )
			{
			//delete [] (AliUInt8_t*)((*iter)->fSEDD);
			ret = pthread_mutex_lock( &fDescriptorMutex );
			if ( ret )
			    {
			    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::MsgLoop", "Descriptor Handler Mutex Error" )
				<< "Error locking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
				<< ret << "). Continuing..." << ENDLOG;
			    }
			if ( fDescriptorHandler )
			    fDescriptorHandler->ReleaseEventDescriptor( AliEventID_t( msg->fEventType, msg->fEventID ) );
			ret = pthread_mutex_unlock( &fDescriptorMutex );
			if ( ret )
			    {
			    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::MsgLoop", "Descriptor Handler Mutex Error" )
				<< "Error unlocking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
				<< ret << "). Continuing..." << ENDLOG;
			    }
			if ( (*iter)->fETS )
			    {
			    fETSCache.Release( (uint8*)( (*iter)->fETS ) );
			    //delete [] (AliUInt8_t*)(*iter)->fETS;
			    }
			if ( (*iter)->fBufferNdx != ~(AliUInt32_t)0 && fBufferManager )
			    {
			    if ( !fBufferManager->ReleaseBlock( (*iter)->fBufferNdx, (*iter)->fBufferOffset ) )
				{
				LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::MsgLoop", "Unable to release block" )
				    << "Unable to release block with buffer index " << AliHLTLog::kDec
				    << (*iter)->fBufferNdx << " (event 0x" << AliHLTLog::kHex << msg->fEventID << " (" << AliHLTLog::kDec
				    << msg->fEventID << "))." << ENDLOG;
				}
			    if ( fStatus )
				{
				MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
				fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
				}
			    (*iter)->fBufferNdx = ~(AliUInt32_t)0;
			    (*iter)->fBufferOffset = 0;
			    if ( fStatus )
				{
// XXXX TODO LOCKING XXXX
				MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
				fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
				}
			    }
			
			//delete *iter;
			(*iter)->fSEDD = NULL;
			(*iter)->fETS = NULL;
			(*iter)->fPub = NULL;
			fEventDataCache.Release( (uint8*)(*iter) );
			}
		    else
			(*iter)->fEventDone = true;
		    fEventSEDDs.Remove( ndx );
		    }
		if ( !found )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::MsgLoop", "Event not found" )
			<< "Done event 0x" << AliHLTLog::kHex << msg->fEventID << " (" << AliHLTLog::kDec
			<< msg->fEventID << ") could not be found in event descriptor list." << ENDLOG;
		    }
		
		if ( evtPub )
		    {
		    AliHLTEventDoneData* useEdd = edd;
		    struct timeval now;
		    gettimeofday( &now, NULL );
		    bool renewHighWaterMark = false;
		    if ( fHighWaterMarkActive 
			 && (unsigned long)now.tv_sec >= (unsigned long)(fHighWaterMarkSent.tv_sec+gkAliHLTEventDoneBackPressureHighWaterMarkExpiryTime_s-2) 
			 && fTransferSignal.GetNotificationDataCnt()>=fLowWaterMark )
			{
			// renew high water mark before expiry, if input queue not below low water mark yet
			renewHighWaterMark = true;
			}
		    if ( (!fHighWaterMarkActive && fTransferSignal.GetNotificationDataCnt()>fHighWaterMark)
			 || (fHighWaterMarkActive && fTransferSignal.GetNotificationDataCnt()<fLowWaterMark) 
			 || renewHighWaterMark )
		        {
			LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::MsgLoop", "Water Mark" )
			    << "Pending event count: " << AliHLTLog::kDec
			    << fTransferSignal.GetNotificationDataCnt() << "." << ENDLOG;
			unsigned long wordCount = edd->fDataWordCount+3;
			if ( renewHighWaterMark )
			    wordCount += 3;
			useEdd = AliHLTMakeEventDoneData( wordCount, edd->fStatusFlags );
			if ( !useEdd )
			     {
			     LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::MsgLoop", "Out Of Memory" )
			       << "out of memory allocating event done data of " << AliHLTLog::kDec << wordCount 
			       << " words for water mark for event 0x" << AliHLTLog::kHex << edd->fEventID << " (" << AliHLTLog::kDec
			       << edd->fEventID << ")." << ENDLOG;
			     useEdd = edd;
			     }
			else
			    {
			    vector<AliHLTEventDoneData*> eddListTmp;
			    eddListTmp.push_back( edd );
			    AliHLTMergeEventDoneData( edd->fEventID, useEdd, eddListTmp );
			    unsigned long off = edd->fDataWordCount;
			    if ( renewHighWaterMark )
				{
				LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::MsgLoop", "Low Water Mark" )
				    << "Sending back pressure low water mark ID 0x" 
				    << AliHLTLog::kHex << fHighWaterMarkID
				    << "." << ENDLOG;
				useEdd->fDataWords[off] = kAliHLTEventDoneBackPressureLowWaterMark;
				useEdd->fDataWords[off+1] = (AliUInt32_t)((fHighWaterMarkID & 0xFFFFFFFF00000000ULL) >> 32);
				useEdd->fDataWords[off+2] = (AliUInt32_t)(fHighWaterMarkID & 0xFFFFFFFFULL);
				useEdd->fDataWordCount += 3;
				useEdd->fHeader.fLength += 3*sizeof(useEdd->fDataWords[0]);
				off += 3;
				fHighWaterMarkActive = false;
				fHighWaterMarkID = 0;
				}
			    if ( !fHighWaterMarkActive )
				{
				fHighWaterMarkSent = now;
				fHighWaterMarkID = (((AliUInt64_t)now.tv_sec) << 32) | ((AliUInt64_t)now.tv_usec);
				fHighWaterMarkActive = true;
				useEdd->fDataWords[off] = kAliHLTEventDoneBackPressureHighWaterMark;
				useEdd->fDataWords[off+1] = (AliUInt32_t)((fHighWaterMarkID & 0xFFFFFFFF00000000ULL) >> 32);
				useEdd->fDataWords[off+2] = (AliUInt32_t)(fHighWaterMarkID & 0xFFFFFFFFULL);
				LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::MsgLoop", "High Water Mark" )
				    << "Sending back pressure high water mark ID 0x" 
				    << AliHLTLog::kHex << fHighWaterMarkID
				    << "." << ENDLOG;
				}
			    else
				{
				LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::MsgLoop", "Low Water Mark" )
				    << "Sending back pressure low water mark ID 0x" 
				    << AliHLTLog::kHex << fHighWaterMarkID
				    << "." << ENDLOG;
				fHighWaterMarkActive = false;
				useEdd->fDataWords[off] = kAliHLTEventDoneBackPressureLowWaterMark;
				useEdd->fDataWords[off+1] = (AliUInt32_t)((fHighWaterMarkID & 0xFFFFFFFF00000000ULL) >> 32);
				useEdd->fDataWords[off+2] = (AliUInt32_t)(fHighWaterMarkID & 0xFFFFFFFFULL);
				fHighWaterMarkID = 0;
				}
			    useEdd->fDataWordCount += 3;
			    useEdd->fHeader.fLength += 3*sizeof(useEdd->fDataWords[0]);
			    }
			}
		    ret = evtPub->EventDone( *this, *useEdd );
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::MsgLoop", "Event Done Error" )
			    << "Error sending event done to publisher '" << evtPub->GetName() 
			    << "': " << strerror(ret) << AliHLTLog::kDec << " (" << ret << ")."
			    << ENDLOG;
			}
		    if ( useEdd != edd )
		        {
			AliHLTFreeEventDoneData( useEdd );
			useEdd = NULL;
		        }
		    }
		else
		    {
		    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::MsgLoop", "Event not found" )
			<< "Done event 0x" << AliHLTLog::kHex << msg->fEventID << " (" << AliHLTLog::kDec
			<< msg->fEventID << ") could not be found in event descriptor list." << ENDLOG;
		    }
		if ( fStatus )
		    {
// XXXX TODO LOCKING XXXX
		    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
		    fStatus->fHLTStatus.fPendingOutputEventCount = fEventSEDDs.GetCnt()-fTransferSignal.GetNotificationDataCnt();
		    fStatus->Touch();
		    }
		ret = pthread_mutex_unlock( &fEventSEDDMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::MsgLoop", "Event SEDD Mutex Error" )
			<< "Error unlocking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
			<< ret << "). Continuing..." << ENDLOG;
		    }
		}
		break;
	    case kAliHLTBridgeEventDoneForwardMsg:
		{
		if ( fEventAccounting )
		    {
		    MLUCString tmp;
		    tmp = GetName();
		    tmp += " remote partner";
		    fEventAccounting->EventFinished( tmp.c_str(), AliEventID_t( msg->fEventType, msg->fEventID ), 2, 2 );
		    }
		LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::MsgLoop", "Event Done Forward" )
		    << "Event done fordward bridge message received for event 0x" << AliHLTLog::kHex
		    << msg->fEventID << " (" << AliHLTLog::kDec << msg->fEventID
		    << ")." << ENDLOG;
		ret = pthread_mutex_lock( &fEventSEDDMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::MsgLoop", "Event SEDD Mutex Error" )
			<< "Error locking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
			<< ret << "). Continuing..." << ENDLOG;
		    }
		bool found = false;
		unsigned long ndx;
		AliHLTPublisherInterface* evtPub = NULL;
		AliHLTBridgeMsg msgClass1;
		AliHLTEventDoneData* edd=NULL;
		AliHLTEventDoneData eddDummy;
		if ( msg->fLength > msgClass1.GetData()->fLength )
		    edd = (AliHLTEventDoneData*)( ((AliUInt8_t*)msg) + msgClass1.GetData()->fLength );
		else
		    {
		    ret = pthread_mutex_unlock( &fEventSEDDMutex );
		    if ( ret )
			{
			LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::MsgLoop", "Event SEDD Mutex Error" )
			    << "Error unlocking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
			    << ret << "). Continuing..." << ENDLOG;
			}
		    edd = &eddDummy;
		    AliHLTMakeEventDoneData( edd, AliEventID_t( msg->fEventType, msg->fEventID ) );
		    // break;
		    }
		if ( MLUCVectorSearcher<PEventData,AliEventID_t>::FindElement( fEventSEDDs, &FindEventDataByEvent, AliEventID_t( msg->fEventType, msg->fEventID ), ndx ) )
		    {
		    iter = fEventSEDDs.GetPtr( ndx );
		    found = true;
		    evtPub = (*iter)->fPub;
		    if ( evtPub )
			{
			ret = evtPub->EventDone( *this, *edd, true, true );
			if ( ret )
			    {
			    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::MsgLoop", "Event Done Error" )
				<< "Error sending event done forward to publisher '" << evtPub->GetName() 
				<< "': " << strerror(ret) << AliHLTLog::kDec << " (" << ret << ")."
				<< ENDLOG;
			    }
			}
		    else
			{
			LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::MsgLoop", "Event not found" )
			    << "Event 0x" << AliHLTLog::kHex << msg->fEventID << " (" << AliHLTLog::kDec
			    << msg->fEventID << ") could not be found in event descriptor list for event done forward." << ENDLOG;
			}
		    }
		else
		    {
		    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::MsgLoop", "Event not found" )
			<< "Event 0x" << AliHLTLog::kHex << msg->fEventID << " (" << AliHLTLog::kDec
			<< msg->fEventID << ") could not be found in event descriptor list for event done forward." << ENDLOG;
		    }
		    
		ret = pthread_mutex_unlock( &fEventSEDDMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::MsgLoop", "Event SEDD Mutex Error" )
			<< "Error unlocking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
			<< ret << "). Continuing..." << ENDLOG;
		    }
		}
		break;
	    default:
		LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::MsgLoop", "Unknown message" )
		    << "Unknown message '" << AliHLTLog::kDec << msg->fMsgType << " (0x" 
		    << AliHLTLog::kHex << msg->fMsgType << ")' received from publisher bridge."
		    << ENDLOG;
		break;
	    }
	fMsgCom->ReleaseMsg( bMsg );
	pthread_mutex_unlock( &fComRecvMutex );
	}
    fMsgLoopQuitted = true;
    }

bool AliHLTSubscriberBridgeHead::QuitMsgLoop()
    {
    if ( fMsgLoopQuitted )
	return true;
    fQuitMsgLoop = true;
    AliHLTBridgeMsg msg;
    msg.GetData()->fMsgType = kAliHLTBridgeEmptyMsg;
    int ret;
    pthread_mutex_lock( &fComAccessMutex );
    BCLAbstrAddressStruct* self = fMsgCom->NewAddress();
    if ( !self )
	{
	pthread_mutex_unlock( &fComAccessMutex );
	return false;
	}
    memcpy( self, fMsgCom->GetAddress(), self->fLength );
    pthread_mutex_lock( &fMsgComMutex );
    ret = fMsgCom->Send( self, msg.GetData() );
    pthread_mutex_unlock( &fMsgComMutex );
    fMsgCom->DeleteAddress( self );
    pthread_mutex_unlock( &fComAccessMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::QuitMsgLoop", "Msg send error" )
	    << "Error sending quit message to self: " << strerror(ret) << AliHLTLog::kDec
	    << " (" << ret << ")." << ENDLOG;
	}
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 10000000;
    gettimeofday( &start, NULL );
    while ( !fMsgLoopQuitted && deltaT<timeLimit )
	{
	usleep( 0 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    return fMsgLoopQuitted;
    }

void AliHLTSubscriberBridgeHead::ProcessDescriptor( AliHLTSubEventDataDescriptor* sedd, 
						   vector<AliHLTSubEventDataBlockDescriptor>& descriptors, 
						   unsigned long long& totalLength, vector<uint8*>& datas, vector<unsigned long>& offsets, 
						   vector<unsigned long>& sizes )
    {
    if ( !sedd )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::ProcessDescriptor", "Sub-Event Data Descriptor NULL pointer" )
	    << "Sub-event data descriptor NULL pointer passed," << ENDLOG;
	return;
	}
    if ( sedd->fHeader.fType.fID != ALIL3SUBEVENTDATADESCRIPTOR_TYPE )
	{
	LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::ProcessDescriptor", "Wrong Sub-Event Data Descriptor Data ID" )
	    << "Wrong sub-event data descriptor ID: 0x" << AliHLTLog::kHex << sedd->fDataType.fID
	    << " (" << AliHLTLog::kDec << sedd->fDataType.fID << ") - " 
	    << sedd->fHeader.fType.fDescr[0] << sedd->fHeader.fType.fDescr[1] 
	    << sedd->fHeader.fType.fDescr[2] << sedd->fHeader.fType.fDescr[3] 
	    << "("
	    << sedd->fHeader.fType.fDescr[3] << sedd->fHeader.fType.fDescr[2] 
	    << sedd->fHeader.fType.fDescr[1] << sedd->fHeader.fType.fDescr[0] 
	    << "). Expected 0x" << AliHLTLog::kHex << ALIL3SUBEVENTDATADESCRIPTOR_TYPE
	    << " (" << AliHLTLog::kDec << ALIL3SUBEVENTDATADESCRIPTOR_TYPE << ")."
	    << ENDLOG;
	return;
	}
    AliUInt8_t* ptr;
    AliUInt32_t i, n, tmpSize;
    n = sedd->fDataBlockCount;
    for ( i = 0; i < n; i++ )
	{
	ptr = (AliUInt8_t*)GetShmPtr( sedd->fDataBlocks[i].fShmID );
	if ( !ptr )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::ProcessDescriptor", "Shm error" )
		<< "Unable to get pointer to shared memory area with id 0x" << AliHLTLog::kHex
		<< sedd->fDataBlocks[i].fShmID.fKey.fID << " )" << AliHLTLog::kDec << sedd->fDataBlocks[i].fShmID.fKey.fID
		<< ")." << ENDLOG;
	    continue;
	    }
	ptr += sedd->fDataBlocks[i].fBlockOffset;
	if ( sedd->fDataBlocks[i].fDataType.fID == COMPOSITE_DATAID )
	    {
	    ProcessDescriptor( (AliHLTSubEventDataDescriptor*)ptr, descriptors, totalLength, datas, offsets, sizes );
	    continue;
	    }
	descriptors.insert( descriptors.end(), sedd->fDataBlocks[i] );
	datas.insert( datas.end(), ptr );
	tmpSize = sedd->fDataBlocks[i].fBlockSize;
	// XXX
	sizes.insert( sizes.end(), tmpSize );
	if ( tmpSize % gMemoryAlignment )
	    tmpSize += gMemoryAlignment - (tmpSize % gMemoryAlignment);
	offsets.insert( offsets.end(), totalLength );
	//totalLength += sedd->fDataBlocks[i].fBlockSize;
	totalLength += tmpSize;
	}
    }



void AliHLTSubscriberBridgeHead::TransferLoop()
    { // TransferLoop
    int ret;
    fQuitTransferLoop = false;
    fTransferLoopQuitted = false;
    bool isRetryEvent = false;
    bool forceForward = false;
    bool success = true;

    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState |= kAliHLTPCRunningStateFlag;
	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	}
    fTransferSignal.Lock();
    while ( !fQuitTransferLoop )
	{
	// Wait for a new signal to arrive.
	// This signals new work for us.
	if ( (!fTransferSignal.HaveNotificationData() || !success) && !fQuitTransferLoop )
	    {
	    if ( !success && fTransferSignal.HaveNotificationData() && fTimer.GetTimeoutCnt()<=0 )
		fTimer.AddTimeout( fRetryTime, &fRetryCallback, ~(AliUInt64_t)0ULL );
	    fTransferSignal.Wait();
	    }
	if ( fQuitTransferLoop )
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::TransferLoop", "Leaving Transfer Loop" )
		<< "Now leaving transfer loop." << ENDLOG;
	    break;
	    }
	if ( !fBufferManager )
	    {
	    LOG( AliHLTLog::kFatal, "AliHLTSubscriberBridgeHead::TransferLoop", "No buffer manager set" )
		<< "No buffer manager specified." << ENDLOG;
	    continue;
	    }
	if ( !fDescriptorHandler )
	    {
	    LOG( AliHLTLog::kFatal, "AliHLTSubscriberBridgeHead::TransferLoop", "No descriptor handler set" )
		<< "No descriptor handler object specified." << ENDLOG;
	    continue;
	    }
	// The while loop to handle more than one new event at a time:
 	success = true;
	pthread_mutex_lock( &fComAccessMutex );
 	while ( fTransferSignal.HaveNotificationData() && success && !fQuitTransferLoop )
	    {
	    if ( !fConnectionDesired || fQuitTransferLoop )
		break; // XXX continue with included usleep??
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState |= kAliHLTPCBusyStateFlag;
		}
	    AliHLTSubEventDataDescriptor* sedd;
	    AliHLTEventTriggerStruct* ets;
	    EventData* ed;
	    // Try to get the data:
	    ed = fTransferSignal.PeekNotificationData();
	    // Free the lock on the signal to enable new events to be added
	    // while we process this one.
	    fTransferSignal.Unlock();
	    if ( !ed )
		{
		LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::TransferLoop", "No EventData Available" )
		    << "No event data available from transfer signal." << ENDLOG;
		fTransferSignal.Lock();
		fTransferSignal.PopNotificationData(); // Remove current first entry, otherwise endless loop
		if ( fStatus )
		    {
			{
			MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
			fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
			}
                        {
// XXXX TODO LOCKING XXXX
			MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
			fStatus->fHLTStatus.fPendingInputEventCount = fTransferSignal.GetNotificationDataCnt();
			}
		    }
		continue;
		}
	    isRetryEvent = (ed->fRetryCount>0);
	    forceForward = ed->fForceEmptyForward;
	    sedd = ed->fSEDD;
	    if ( !(ed->fSEDD) )
		{
		LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::TransferLoop", "No SubEvent Descriptor Available" )
		    << "No sub-event data descriptor available in event data from transfer signal." << ENDLOG;
		fTransferSignal.Lock();
		fTransferSignal.PopNotificationData(); // Remove current first entry, otherwise endless loop
		if ( fStatus )
		    {
// XXXX TODO LOCKING XXXX
		    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
		    fStatus->fHLTStatus.fPendingInputEventCount = fTransferSignal.GetNotificationDataCnt();
		    }
		continue;
		}
	    ets = ed->fETS;
	    if ( !ed->fETS )
		{
		LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::TransferLoop", "No EventTrigger Data Available" )
		    << "No event trigger data available in event data from transfer signal." << ENDLOG;
		fTransferSignal.Lock();
		fTransferSignal.PopNotificationData(); // Remove current first entry, otherwise endless loop
		if ( fStatus )
		    {
// XXXX TODO LOCKING XXXX
		    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
		    fStatus->fHLTStatus.fPendingInputEventCount = fTransferSignal.GetNotificationDataCnt();
		    }
		continue;
		}
	    bool transferAborted = false;
	    int reason = 0;
	    bool eventAborted = false;
	    bool eventDone = false;
	    vector<AliHLTSubEventDataBlockDescriptor> dataBlocks;
	    unsigned long long totalLength = 0;
	    bool paused = fPaused;
	    if ( paused )
		{
		reason = 1;
		transferAborted = true;
		}

	    if ( !transferAborted )
		{
		// Connect if not already done...
		if ( !fBlobConnected )
		    ret = Connect();
		// ... and check if the connect actually worked.
		if ( !fBlobConnected )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::TransferLoop", "Connection error" )
			<< "Unable to connect blob to publisher bridge head: " 
			<< strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
		    reason = 2;
		    transferAborted = true;
		    // XXX Retry??
		    }
		}
		
	    if ( !transferAborted && (!fRemoteBlobSize || fGlobalTransferID == ~(BCLTransferID)0) )
		{
		reason = 3;
		transferAborted = true;
		LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::TransferLoop", "No remote blob data available" )
		    << "No remote blob data available yet. Retrying..." << ENDLOG;
		}
	    pthread_mutex_unlock( &fComAccessMutex );


	    vector<unsigned long> descriptorOffsetList; 
	    vector<uint8*> descriptorDatas;
	    //vector<uint8*>::iterator dataIter, dataEnd;
	    vector<unsigned long> descriptorSizes;
	    //vector< uint32>::iterator sizeIter, sizeEnd;
	    unsigned long bufferOffset, bufferSize;
	    AliUInt32_t bufferNdx = ~(AliUInt32_t)0;
	    vector<uint8*> datas;
	    vector<uint64> sizes;
	    vector<uint64> offsetList; 
	    uint32 seddDestLength = 0;
	    uint32 etsDestLength = 0;
	    if ( !transferAborted )
		{
		// Collect all blocks recursively into list
		LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::TransferLoop", "Block accounting" )
		    << "Event 0x" << AliHLTLog::kHex << sedd->fEventID << " ("
		    << AliHLTLog::kDec << sedd->fEventID << " sedd data blocks: " << AliHLTLog::kDec << sedd->fDataBlockCount << "." << ENDLOG;
		if ( !forceForward )
		    {
		    ProcessDescriptor( sedd, dataBlocks, totalLength, descriptorDatas, descriptorOffsetList, descriptorSizes );
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::TransferLoop", "Block accounting" )
			<< "Event 0x" << AliHLTLog::kHex << sedd->fEventID << " ("
			<< AliHLTLog::kDec << sedd->fEventID << " sedd data blocks: " << AliHLTLog::kDec << sedd->fDataBlockCount 
			<< " - dataBlocks data blocks: " << dataBlocks.size()
			<< "." << ENDLOG;
		    }
		else
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::TransferLoop", "Block accounting" )
			<< "Event 0x" << AliHLTLog::kHex << sedd->fEventID << " ("
			<< AliHLTLog::kDec << sedd->fEventID << ") force forwarded empty." << ENDLOG;
		    }

		seddDestLength = sizeof(AliHLTBridgeTransferSEDD)+sizeof(AliHLTBridgeBlockStruct)*dataBlocks.size();
		etsDestLength = ed->fETS->fDataWordCount*sizeof(ed->fETS->fDataWords[0]);
		if ( seddDestLength % gMemoryAlignment )
		    seddDestLength += gMemoryAlignment - (seddDestLength % gMemoryAlignment);
		if ( etsDestLength % gMemoryAlignment )
		    etsDestLength += gMemoryAlignment - (etsDestLength % gMemoryAlignment);

		totalLength += seddDestLength + etsDestLength;
		


#ifdef OUTPUT_DEBUG
		    {
		    // Write all event data blocks into a file for analysis and comparison
		    // with the blocks received on the other side.
		    AliUInt32_t cnt = 0;
		    int fh;
		    char nam[ 1024 ];
		    dataIter = datas.begin();
		    dataEnd = datas.end();
		    sizeIter = sizes.begin();
		    sizeEnd = sizes.end();
		    while ( dataIter != dataEnd )
			{
			sprintf( nam, "Event-%lu-Block-%lu.data", (unsigned long)sedd->fEventID, (unsigned long)cnt );
			fh = open( nam, O_WRONLY|O_CREAT|O_TRUNC, 0644 );
			if ( fh != -1 )
			    {
			    write( fh, *dataIter, *sizeIter );
			    close( fh );
			    }
			else
			    {
			    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::TransferLoop", "Data file error" )
				<< "Error opening data file " << nam << ": " << strerror(errno) 
				<< " (" << AliHLTLog::kDec << errno << ")." << ENDLOG;
			    }
			dataIter++;
			sizeIter++;
			cnt++;
			}
		    }
#endif
		    
		    bufferSize = totalLength;
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::TransferLoop", "Buffer Block" )
			<< "Requesting block of 0x" << AliHLTLog::kHex << bufferSize << " (" << AliHLTLog::kDec << bufferSize << ") bytes." << ENDLOG;
		    transferAborted = !fBufferManager->GetBlock( bufferNdx, bufferOffset, bufferSize );
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::TransferLoop", "Buffer Block" )
			<< "Block " << (char*)( transferAborted ? "unsuccessfully" : "successfully" ) << " received from buffer manager: Offset: " << AliHLTLog::kDec
			<< bufferOffset << " - Size: " << bufferSize << " - Index: " << bufferNdx << "." << ENDLOG;
		    if ( transferAborted )
			{
			LOG( AliHLTLog::kInformational, "AliHLTSubscriberBridgeHead::TransferLoop", "Buffer Block" )
			    << "Event 0x" << AliHLTLog::kHex << sedd->fEventID << "( " << AliHLTLog::kDec
			    << sedd->fEventID << "): Block unsuccessfully received from buffer manager: Offset: " << AliHLTLog::kDec
			    << bufferOffset << " - Size: " << bufferSize << " - Index: " << bufferNdx << "." << ENDLOG;
			reason = 4;
			}
		    else
			{
			fBufferManager->AdaptBlock( bufferNdx, bufferOffset, totalLength );
			if ( fStatus )
			    {
			    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
			    fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
			    }
			}
		    if ( !transferAborted && bufferSize < totalLength )
			{
			LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::TransferLoop", "Buffer Block" )
			    << "Event 0x" << AliHLTLog::kHex << sedd->fEventID << "( " << AliHLTLog::kDec
			    << sedd->fEventID << "): Block received from buffer manager too small: Received: " << AliHLTLog::kDec
			    << bufferSize << " - Needed: " << totalLength << "." << ENDLOG;
			reason = 5;
			transferAborted = true;
			}
		    if ( transferAborted && (!fBufferManager->IsBlockFeasible( totalLength ) || forceForward) )
			{
			LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::TransferLoop", "Buffer Block" )
			    << "Buffer block of size " << AliHLTLog::kDec << totalLength 
			    << " from publisher for event 0x" << AliHLTLog::kHex << sedd->fEventID << " ("
			    << AliHLTLog::kDec << sedd->fEventID << ") is permantly impossible to obtain. Event will be "
			    << (fForwardMaxRetryEvents ? "forwarded empty" : "aborted") << "..." << ENDLOG;
			if ( fForwardMaxRetryEvents && !forceForward ) // Drastic measures if we cannot even force forward an empty event...
			    ed->fForceEmptyForward = true;
			else
			    eventAborted = true;
			}
		}

	    if ( !transferAborted )
		{
		
		// Take care of event descriptor and trigger structure
		if ( fTransferSEDD && fTransferSEDDLength < sizeof(AliHLTBridgeTransferSEDD)+sizeof(AliHLTBridgeBlockStruct)*dataBlocks.size() )
		    {
		    delete [] (AliUInt8_t*)fTransferSEDD;
		    fTransferSEDD = NULL;
		    fTransferSEDDLength = 0;
		    }
		if ( !fTransferSEDD )
		    {
		    fTransferSEDD = (AliHLTBridgeTransferSEDD*)new AliUInt8_t[sizeof(AliHLTBridgeTransferSEDD)+sizeof(AliHLTBridgeBlockStruct)*dataBlocks.size()];
		    if ( !fTransferSEDD )
			{
			reason = 7;
			transferAborted = true;
			LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::TransferLoop", "Transfer SEDD" )
			    << "Out of memory allocating transfer loop temporary sedd of " 
			    << (unsigned long)(sizeof(AliHLTBridgeTransferSEDD)+sizeof(AliHLTBridgeBlockStruct)*dataBlocks.size())
			    << " bytes." << ENDLOG;
			}
		    else
			{
			fTransferSEDDLength = sizeof(AliHLTBridgeTransferSEDD)+sizeof(AliHLTBridgeBlockStruct)*dataBlocks.size();
			AliHLTBridgeData bridgeData;
			fTransferSEDD->fData = *(bridgeData.GetData());
			AliHLTBridgeBlock bridgeBlock;
			for ( unsigned long i = 0; i < dataBlocks.size(); i++ )
			    fTransferSEDD->fBlocks[i] = *(bridgeBlock.GetData());
			}
		    }
		}

	    if ( !transferAborted )
		{
		fTransferSEDD->fData.fEventType = sedd->fEventID.fType;
		fTransferSEDD->fData.fEventNr = sedd->fEventID.fNr;
		fTransferSEDD->fData.fEventBirth_s = sedd->fEventBirth_s;
		fTransferSEDD->fData.fEventBirth_us = sedd->fEventBirth_us;
		fTransferSEDD->fData.fOldestEventBirth_s = sedd->fOldestEventBirth_s;
		fTransferSEDD->fData.fDataType = sedd->fDataType.fID;
		fTransferSEDD->fData.fBlockCount = dataBlocks.size();
		fTransferSEDD->fData.fStatusFlags = sedd->fStatusFlags;

		for ( unsigned long i = 0; i < dataBlocks.size(); i++ )
		    {
		    fTransferSEDD->fBlocks[i].fBlockSize = dataBlocks[i].fBlockSize;
		    //fTransferSEDD->fBlocks[i].fBlockOffset = dataBlocks[i].fBlockOffset;
		    fTransferSEDD->fBlocks[i].fBlockOffset = ~(uint32)0;
		    fTransferSEDD->fBlocks[i].fProducerNode = dataBlocks[i].fProducerNode;
		    fTransferSEDD->fBlocks[i].fDataType = dataBlocks[i].fDataType.fID;
		    fTransferSEDD->fBlocks[i].fDataOrigin = dataBlocks[i].fDataOrigin.fID;
		    fTransferSEDD->fBlocks[i].fDataSpecification = dataBlocks[i].fDataSpecification;
		    fTransferSEDD->fBlocks[i].fStatusFlags = dataBlocks[i].fStatusFlags;
		    //fTransferSEDD->fBlocks[i].fDataByteOrder = dataBlocks[i].fByteOrder;
		    for ( int j = 0; j < kAliHLTSEDBDAttributeCount; j++ )
			fTransferSEDD->fBlocks[i].fDataAttributes[j] = dataBlocks[i].fAttributes[j];
		    }
		

		vector<unsigned long>::iterator offsetSizeIter, offsetSizeEnd;
		vector<uint8*>::iterator dataIter, dataEnd;
		offsetList.reserve( 2+descriptorOffsetList.size() );
		offsetList.insert( offsetList.end(), (uint64)0 );
		offsetList.insert( offsetList.end(), (uint64)seddDestLength );
		offsetSizeIter = descriptorOffsetList.begin();
		offsetSizeEnd = descriptorOffsetList.end();
		while ( offsetSizeIter != offsetSizeEnd )
		    {
		    offsetList.insert( offsetList.end(), (uint64)( (*offsetSizeIter) + seddDestLength + etsDestLength ) );
		    offsetSizeIter++;
		    }
		sizes.reserve( 2+descriptorSizes.size() );
		sizes.insert( sizes.end(), sizeof(AliHLTBridgeTransferSEDD)+sizeof(AliHLTBridgeBlockStruct)*dataBlocks.size() );
		sizes.insert( sizes.end(), ed->fETS->fDataWordCount*sizeof(ed->fETS->fDataWords[0]) );
		offsetSizeIter = descriptorSizes.begin();
		offsetSizeEnd = descriptorSizes.end();
		while ( offsetSizeIter != offsetSizeEnd )
		    {
		    sizes.insert( sizes.end(), (uint64)*offsetSizeIter );
		    offsetSizeIter++;
		    }
		datas.reserve( 2+descriptorDatas.size() );
		datas.insert( datas.end(), (uint8*)fTransferSEDD );
		datas.insert( datas.end(), (uint8*)(ed->fETS->fDataWords) );
		dataIter = descriptorDatas.begin();
		dataEnd = descriptorDatas.end();
		while ( dataIter != dataEnd )
		    {
		    datas.insert( datas.end(), *dataIter );
		    dataIter++;
		    }
		

		if ( fStatus )
		    {
// XXXX TODO LOCKING XXXX
		    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
		    fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
		    }
		ed->fBufferNdx = bufferNdx;
		ed->fBufferOffset = bufferOffset;
		// 		offsetList.insert( offsetList.end(), (uint32)bufferOffset );
		// We start at offset bufferOffset and all the other blocks follow.
		unsigned long oi, on = offsetList.size();
		for ( oi = 0; oi < on; oi++ )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::TransferLoop", "Block offsets" )
			<< "Block " << AliHLTLog::kDec << oi << " relative offset: " << offsetList[oi]
			<< " (0x" << AliHLTLog::kHex << offsetList[oi] << ")." << ENDLOG;
		    offsetList[oi] = offsetList[oi]+(uint32)bufferOffset;
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::TransferLoop", "Block offsets" )
			<< "Block " << AliHLTLog::kDec << oi << " absolute offset: " << offsetList[oi]
			<< " (0x" << AliHLTLog::kHex << offsetList[oi] << ")." << ENDLOG;
		    }
		// Now set the offset lists for the blocks
		for ( oi = 0; oi < fTransferSEDD->fData.fBlockCount; oi++ )
		    fTransferSEDD->fBlocks[oi].fBlockOffset = offsetList[oi+2];
		pthread_mutex_lock( &fComAccessMutex );

		// Transfer all collected blocks;

		if ( fBlobConnected && fConnectionDesired )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::TransferLoop", "Starting Transfer" )
			<< "Starting transfer of event 0x" << AliHLTLog::kHex
			<< sedd->fEventID << " (" << AliHLTLog::kDec << sedd->fEventID << ")." 
			<< ENDLOG;
		    ret = fBlobCom->PushBlob( fRemoteBlobMsgAddress, fGlobalTransferID, datas, offsetList, sizes, fCommunicationTimeout );
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::TransferLoop", "Ending Transfer" )
			<< "Ending transfer of event 0x" << AliHLTLog::kHex
			<< sedd->fEventID << " (" << AliHLTLog::kDec << sedd->fEventID << ")." 
			<< ENDLOG;
		    pthread_mutex_unlock( &fComAccessMutex );
		    if ( ret )
			{
			reason = 6;
			transferAborted = true;
			LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::TransferLoop", "Blob Transfer Error" )
			    << "Error transferring event data blob for event 0x" << AliHLTLog::kHex
			    << sedd->fEventID << " (" << AliHLTLog::kDec << sedd->fEventID << "): "
			    << strerror(ret) << " (" << ret << ")." << ENDLOG;
			if ( fErrorCallback )
			    fErrorCallback->ConnectionError( this );
			//Disconnect();
			}
		    else
			{
			if ( fEventAccounting )
			    fEventAccounting->EventForwarded( sedd->fEventID );
			if ( fStatus )
			    {
// XXXX TODO LOCKING XXXX
			    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
			    if ( sedd->fEventID.fType!=kAliEventTypeTick && sedd->fEventID.fType!=kAliEventTypeReconfigure && sedd->fEventID.fType!=kAliEventTypeDCSUpdate )
				fStatus->fHLTStatus.fProcessedEventCount++;
			    fStatus->Touch();
			    }
			}
		    }
		else
		    {
		    bool desired = fConnectionDesired;
		    pthread_mutex_unlock( &fComAccessMutex );
		    transferAborted = true;
		    reason = 6;
		    if ( desired )
			{
			LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::TransferLoop", "No Blob Transfer Connection" )
			    << "Error transferring event data blob for event 0x" << AliHLTLog::kHex
			    << sedd->fEventID << " (" << AliHLTLog::kDec << sedd->fEventID << "): - No blob connection established."
			    << ENDLOG;
			}
		    }
		}

	    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::TransferLoop", "TRACE" )
		<< "TRACE" << ENDLOG;

	    AliEventID_t eventID = ed->fSEDD->fEventID;
	    if ( !transferAborted )
		{
		// Check wether event has been aborted in the meantime
		// and if so change message type from new event to event canceled.

		ret = pthread_mutex_lock( &fEventSEDDMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::TransferLoop", "Event SEDD Mutex Error" )
			<< "Error locking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
			<< ret << "). Continuing..." << ENDLOG;
		    }
		if ( ed->fTransferAborted )
		    {
		    LOG( AliHLTLog::kInformational, "AliHLTSubscriberBridgeHead::TransferLoop", "Transfer Aborted" )
			<< "Transfer of event 0x" << AliHLTLog::kHex << sedd->fEventID << " (" << AliHLTLog::kDec
			<< sedd->fEventID << ") aborted." << ENDLOG;
		    reason = 8;
		    transferAborted = true;
		    eventAborted = true;
		    }
		else if ( ed->fEventDone )
		    {
		    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::TransferLoop", "Event Done" )
			<< "Event 0x" << AliHLTLog::kHex << sedd->fEventID << " (" << AliHLTLog::kDec
			<< sedd->fEventID << ") already done." << ENDLOG;
		    reason = 11;
		    transferAborted = true;
		    eventDone = true;
		    }
		else
		    {
		    ed->fTransferInProgress = false;
		    if ( fStatus )
			{
// XXXX TODO LOCKING XXXX
			MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
			fStatus->fHLTStatus.fPendingOutputEventCount = fEventSEDDs.GetCnt()-fTransferSignal.GetNotificationDataCnt();
			fStatus->Touch();
			}
		    }
		unsigned long long eventSEDDCnt = fEventSEDDs.GetCnt();
		ret = pthread_mutex_unlock( &fEventSEDDMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::TransferLoop", "Event SEDD Mutex Error" )
			<< "Error unlocking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
			<< ret << "). Continuing..." << ENDLOG;
		    }
		fTransferSignal.PopNotificationData();
		if ( fStatus )
		    {
// XXXX TODO LOCKING XXXX
		    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
		    fStatus->fHLTStatus.fPendingInputEventCount = fTransferSignal.GetNotificationDataCnt();
		    fStatus->fHLTStatus.fPendingOutputEventCount = eventSEDDCnt-fTransferSignal.GetNotificationDataCnt();
		    }
		}


	    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::TransferLoop", "TRACE" )
		<< "TRACE" << ENDLOG;

	    if ( eventAborted )
		{

	    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::TransferLoop", "TRACE" )
		<< "TRACE" << ENDLOG;

		AliHLTBridgeMsg msgClass;
		msgClass.GetData()->fMsgType = kAliHLTBridgeEventCanceledMsg;
		msgClass.GetData()->fTransferID = fGlobalTransferID;
		
		msgClass.GetData()->fEventID = eventID.fNr;
		msgClass.GetData()->fEventType = eventID.fType;
		if ( !fMsgConnected )
		    ret = Connect();
		if ( !fMsgConnected )
		    {
		    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::TransferLoop", "Connection error" )
			<< "Unable to connect to publisher bridge head: " 
			<< strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
		    reason = 9;
		    transferAborted = true;
		    }
		else
		    {
		    pthread_mutex_lock( &fMsgComMutex );
		    if ( fCommunicationTimeout )
			ret = fMsgCom->Send( fRemoteMsgAddress, msgClass.GetData(), fCommunicationTimeout );
		    else
			ret = fMsgCom->Send( fRemoteMsgAddress, msgClass.GetData() );
		    pthread_mutex_unlock( &fMsgComMutex );
		    if ( ret )
			{
			LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::TransferLoop", "Msg send error" )
			    << "Error sending event start message to publisher bridge head: "
			    << strerror(ret) << AliHLTLog::kDec << " (" << ret << ")." << ENDLOG;
			reason = 10;
			transferAborted = true;
			if ( fErrorCallback )
			    fErrorCallback->ConnectionError( this );
			//Disconnect();
			}
		    }
		}
	    
	    
// 	    }



	    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::TransferLoop", "TRACE" )
		<< "TRACE" << ENDLOG;

	    if ( transferAborted )
		{

	    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::TransferLoop", "TRACE" )
		<< "TRACE" << ENDLOG;

		success = false;
		if ( reason==4 || reason==11 )
		    {
		    LOG( AliHLTLog::kInformational, "AliHLTSubscriberBridgeHead::TransferLoop", "Event aborted" )
			<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ") aborted (reason: " << reason << ")." << ENDLOG;
		    }
		else
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::TransferLoop", "Event aborted" )
			<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ") aborted (reason: " << reason << ")." << ENDLOG;
		    }
		if ( paused )
		    {
		    LOG( AliHLTLog::kInformational, "AliHLTSubscriberBridgeHead::TransferLoop", "Event aborted" )
			<< "Just paused..." << ENDLOG;
		    }

		if ( !eventAborted && !eventDone )
		    {
#ifndef REDUCE_RETRY_OUTPUT
		    LOG( AliHLTLog::kInformational, "AliHLTSubscriberBridgeHead::TransferLoop", "Event retry" )
			<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
			<< eventID << ") scheduled for retry." << ENDLOG;
#endif
		    if ( ed && !paused )
			ed->fRetryCount++;
		    if ( ed && (!fMaxRetryCount || ed->fRetryCount <= fMaxRetryCount || paused || fForwardMaxRetryEvents ) )
			{
			if ( ed->fSEDD )
			    {
#ifndef REDUCE_RETRY_OUTPUT
			    LOG( AliHLTLog::kInformational, "AliHLTSubscriberBridgeHead::TransferLoop", "Scheduling Event for Retry" )
				<< "Scheduling event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
				<< eventID << ") (" << ed->fSEDD->fDataBlockCount << " data blocks) for retry " 
				<< ed->fRetryCount << "." << ENDLOG;
#endif
// 			    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::TransferLoop", "Scheduling Event for Retry" )
// 				<< "Scheduling event 0x" << AliHLTLog::kHex << ed->fSEDD->fEventID << " (" << AliHLTLog::kDec
// 				<< ed->fSEDD->fEventID << ") for retry " << ed->fRetryCount << "." << ENDLOG;
			    }
			if ( fMaxRetryCount && ed->fRetryCount > fMaxRetryCount && fBufferManager->GetFreeBufferSize()==fBufferManager->GetTotalBufferSize() )
			    {
			    ed->fForceEmptyForward = true;
			    LOG( AliHLTLog::kInformational, "AliHLTSubscriberBridgeHead::TransferLoop", "Empty event forwarding" )
				<< "Force forwarding event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
				<< eventID << ") empty after " 
				<< ed->fRetryCount << " retries!" << ENDLOG;
			    }
			fTimer.AddTimeout( fRetryTime, &fRetryCallback, reinterpret_cast<AliUInt64_t>(ed) );
			}
		    else
			{
			if ( ed->fSEDD )
			    {
			    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::TransferLoop", "Event Aborted" )
			    << "Event 0x" << AliHLTLog::kHex << ed->fSEDD->fEventID << " (" << AliHLTLog::kDec
			    << ed->fSEDD->fEventID << ") aborted after " << ed->fRetryCount << " retries." << ENDLOG;
			    }
			if ( ed->fPub && ed->fSEDD )
			    {
			    AliHLTEventDoneData dummyEDD;
			    AliHLTMakeEventDoneData( &dummyEDD, ed->fSEDD->fEventID );
			    ret = ed->fPub->EventDone( *this, dummyEDD );
			    if ( ret )
				{
				LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::TransferLoop", "Event Done Error" )
				    << "Error sending event done to publisher '" << ed->fPub->GetName() 
				    << "': " << strerror(ret) << AliHLTLog::kDec << " (" << ret << ")."
				    << ENDLOG;
				}
			    }
			ret = pthread_mutex_lock( &fEventSEDDMutex );
			if ( ret )
			    {
			    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::TransferLoop", "Event SEDD Mutex Error" )
				<< "Error locking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
				<< ret << "). Continuing..." << ENDLOG;
			    }
			unsigned long ndx;
			if ( MLUCVectorSearcher<PEventData,AliEventID_t>::FindElement( fEventSEDDs, &FindEventDataByEvent, ed->fSEDD->fEventID, ndx ) )
			    fEventSEDDs.Remove( ndx );
			unsigned long long eventSEDDCnt = fEventSEDDs.GetCnt();
			ret = pthread_mutex_unlock( &fEventSEDDMutex );
			if ( ret )
			    {
			    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::TransferLoop", "Event SEDD Mutex Error" )
				<< "Error unlocking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
				<< ret << "). Continuing..." << ENDLOG;
			    }
			ret = pthread_mutex_lock( &fDescriptorMutex );
			if ( ret )
			    {
			    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::TransferLoop", "Descriptor Handler Mutex Error" )
				<< "Error locking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
				<< ret << "). Continuing..." << ENDLOG;
			    }
			if ( ed->fSEDD && fDescriptorHandler )
			    fDescriptorHandler->ReleaseEventDescriptor( ed->fSEDD->fEventID );
			ret = pthread_mutex_unlock( &fDescriptorMutex );
			if ( ret )
			    {
			    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::TransferLoop", "Descriptor Handler Mutex Error" )
				<< "Error unlocking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
				<< ret << "). Continuing..." << ENDLOG;
			    }
			if ( ed->fETS )
			    {
			    fETSCache.Release( (uint8*)ed->fETS );
			    //delete [] (AliUInt8_t*)ed->fETS;
			    }
			ed->fSEDD = NULL;
			ed->fETS = NULL;
			ed->fPub = NULL;
			fEventDataCache.Release( (uint8*)ed );
			ed = NULL;
			fTransferSignal.PopNotificationData();
			if ( fStatus )
			    {
// XXXX TODO LOCKING XXXX
			    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
			    fStatus->fHLTStatus.fPendingInputEventCount = fTransferSignal.GetNotificationDataCnt();
			    fStatus->fHLTStatus.fPendingOutputEventCount = eventSEDDCnt-fTransferSignal.GetNotificationDataCnt();
			    }
			}
		    }
		else
		    {
		    ret = pthread_mutex_lock( &fDescriptorMutex );
		    if ( ret )
			{
			LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::TransferLoop", "Descriptor Handler Mutex Error" )
			    << "Error locking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
			    << ret << "). Continuing..." << ENDLOG;
			}
		    if ( ed->fSEDD && fDescriptorHandler )
			fDescriptorHandler->ReleaseEventDescriptor( ed->fSEDD->fEventID );
		    ret = pthread_mutex_unlock( &fDescriptorMutex );
		    if ( ret )
			{
			LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::TransferLoop", "Descriptor Handler Mutex Error" )
			    << "Error unlocking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
			    << ret << "). Continuing..." << ENDLOG;
			}
		    if ( ed->fETS )
			{
			fETSCache.Release( (uint8*)ed->fETS );
			//delete [] (AliUInt8_t*)ed->fETS;
			}
		    ed->fSEDD = NULL;
		    ed->fETS = NULL;
		    ed->fPub = NULL;
		    fEventDataCache.Release( (uint8*)ed );
		    ed = NULL;
		    }
		if ( bufferNdx != ~(AliUInt32_t)0 )
		    {
		    fBufferManager->ReleaseBlock( bufferNdx, bufferOffset );
		    if ( ed )
			{
			ed->fBufferNdx = ~(AliUInt32_t)0;
			ed->fBufferOffset = 0;
			}
		    if ( fStatus )
			{
// XXXX TODO LOCKING XXXX
			MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
			fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
			}
		    }
		}
	    else 
		{

	    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::TransferLoop", "TRACE" )
		<< "TRACE" << ENDLOG;

		if ( isRetryEvent )
		    {
		    LOG( AliHLTLog::kInformational, "AliHLTSubscriberBridgeHead::TransferLoop", "Retry event sent" )
			<< "Retry event 0x" << AliHLTLog::kHex << eventID << " (" << 
			AliHLTLog::kDec << eventID << ") successfully sent." << ENDLOG;
		    }
		}

	    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::TransferLoop", "TRACE" )
		<< "TRACE" << ENDLOG;

	    // Free sedd pointer
	    //delete [] (AliUInt8_t*)sedd;
	    fTransferSignal.Lock();
	    if ( fStatus )
		{
		MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
		fStatus->fProcessStatus.fState &= ~kAliHLTPCBusyStateFlag;
		}
	    pthread_mutex_lock( &fComAccessMutex );
	    }
	pthread_mutex_unlock( &fComAccessMutex );
	}
    fTransferSignal.Unlock();
    if ( fStatus )
	{
	MLUCMutex::TLocker stateLock( fStatus->fStateMutex );
	fStatus->fProcessStatus.fState &= ~(kAliHLTPCRunningStateFlag|kAliHLTPCBusyStateFlag|kAliHLTPCPausedStateFlag);
	}
    fTransferLoopQuitted = true;
    }
    
void AliHLTSubscriberBridgeHead::QuitTransferLoop()
    {
    fQuitTransferLoop = true;
    fTransferSignal.Signal();
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 3000000;
    gettimeofday( &start, NULL );
    while ( !fTransferLoopQuitted && deltaT<timeLimit )
	{
	usleep( 0 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( fTransferLoopQuitted )
	fTransferThread.Join();
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::QuitTransferLoop", "Aborting transfer loop" )
	    << "Transfer loop not terminated correctly"
	    << " after " << AliHLTLog::kDec << deltaT << " microsec"
	    << ". Aborting now..." << ENDLOG;
	fTransferLoopQuitted = true;
	fTransferThread.Abort();
	}
    }
  
void AliHLTSubscriberBridgeHead::AddRetry( AliUInt64_t, unsigned long )
    {
//     fTransferSignal.Lock();
//     fRetryData.Add( notData );
//     fTransferSignal.Unlock();
    fTransferSignal.Signal();
    }
  


void* AliHLTSubscriberBridgeHead::GetShmPtr( AliHLTShmID shmKey )
    {
    vector<ShmData>::iterator iter, end;
    void* p = NULL;
    int ret;
    bool found = false;
    ret = pthread_mutex_lock( &fShmMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::GetShmPtr", "Shm Mutex Error" )
	    << "Error locking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    iter = fShmData.begin();
    end = fShmData.end();
    while ( iter != end )
	{
	if ( iter->fShmKey.fKey.fID == shmKey.fKey.fID &&
	     iter->fShmKey.fShmType == shmKey.fShmType )
	    {
	    iter->fNonRefCount = 0;
	    iter->fRefCount++;
	    found = true;
	    p = iter->fShmPtr;
	    //break;
	    }
	else
	    {
	    iter->fNonRefCount++;
	    if ( fMaxShmNoUseCount && iter->fNonRefCount > fMaxShmNoUseCount )
		{
		}
	    }
	iter++;
	}
    if ( !found )
	{
	ShmData data;
	unsigned long tmpSize = 0;
	data.fShmKey.fShmType = shmKey.fShmType; //kInvalidShmType;
	data.fShmKey.fKey.fID = shmKey.fKey.fID;
	data.fShmID = fShm.GetShm( data.fShmKey, tmpSize );
	if ( data.fShmID == (AliUInt32_t)-1 )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::GetShmPtr", "Getting new event shared memory" )
		<< "Error getting shared memory region " << data.fShmKey.fKey.fID << "." << ENDLOG;
	    ret = pthread_mutex_unlock( &fShmMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::GetShmPtr", "Shm Mutex Error" )
		    << "Error unlocking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << "). Continuing..." << ENDLOG;
		}
	    return NULL;
	    }
	data.fShmPtr = fShm.LockShm( data.fShmID );
	if ( !data.fShmPtr )
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::GetShmPtr", "Getting new event shared memory" )
		<< "Error locking shared memory region " << data.fShmKey.fKey.fID << " (with ID "
		<< data.fShmID << ")." << ENDLOG;
	    fShm.ReleaseShm( data.fShmID );
	    ret = pthread_mutex_unlock( &fShmMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::GetShmPtr", "Shm Mutex Error" )
		    << "Error unlocking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << "). Continuing..." << ENDLOG;
		}
	    return NULL;
	    }
	p = data.fShmPtr;
	data.fNonRefCount = 0;
	data.fRefCount = 1;
	fShmData.insert( fShmData.end(), data );
	}
    ret = pthread_mutex_unlock( &fShmMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::GetShmPtr", "Shm Mutex Error" )
	    << "Error unlocking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    return p;
    }

void AliHLTSubscriberBridgeHead::CleanupOldEvents()
    {
    int ret; 
    bool found = false;
    unsigned long ndx;
    ret = pthread_mutex_lock( &fEventSEDDMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::CleanupOldEvents", "Event SEDD Mutex Error" )
	    << "Error locking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    while ( MLUCVectorSearcher<PEventData,AliUInt32_t>::FindElement( fEventSEDDs, &FindOldEventData, fOldestEventBirth_s, ndx ) )
	{
	PEventData* iter = fEventSEDDs.GetPtr( ndx );
	if ( !(*iter) || !((*iter)->fSEDD) )
	    continue;
	AliEventID_t eventID = (*iter)->fSEDD->fEventID;
	(*iter)->fTransferAborted = true;
	found = (*iter)->fTransferInProgress;
	if ( !found )
	    {
	    ret = pthread_mutex_lock( &fDescriptorMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::CleanupOldEvents", "Descriptor Handler Mutex Error" )
		    << "Error locking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << "). Continuing..." << ENDLOG;
		}
	    if ( fDescriptorHandler )
		fDescriptorHandler->ReleaseEventDescriptor( eventID );
	    ret = pthread_mutex_unlock( &fDescriptorMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::CleanupOldEvents", "Descriptor Handler Mutex Error" )
		    << "Error unlocking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << "). Continuing..." << ENDLOG;
		}
	    if ( (*iter)->fETS )
		{
		fETSCache.Release( (uint8*)( (*iter)->fETS ) );
		//delete [] (AliUInt8_t*)(*iter)->fETS;
		}
	    if ( (*iter)->fBufferNdx != ~(AliUInt32_t)0 && fBufferManager )
		{
		if ( !fBufferManager->ReleaseBlock( (*iter)->fBufferNdx, (*iter)->fBufferOffset ) )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::CleanupOldEvents", "Unable to release block" )
			<< "Unable to release block with buffer index " << AliHLTLog::kDec
			<< (*iter)->fBufferNdx << "." << ENDLOG;
		    }
		if ( fStatus )
		    {
		    MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
		    fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
		    }
		(*iter)->fBufferNdx = ~(AliUInt32_t)0;
		(*iter)->fBufferOffset = 0;
		}
	    
	    //delete *iter;
	    (*iter)->fSEDD = NULL;
	    (*iter)->fETS = NULL;
	    (*iter)->fPub = NULL;
	    fEventDataCache.Release( (uint8*)(*iter) );
	    
	    }
	fEventSEDDs.Remove( ndx );
# if 0
	ret = pthread_mutex_unlock( &fEventSEDDMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::CleanupOldEvents", "Event SEDD Mutex Error" )
		<< "Error unlocking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << "). Continuing..." << ENDLOG;
	    }
	// Should not be needed, as the oldest event birth marker has to be forwarded and thus the PBH should be able to determine too old events on its own.
	if ( !found && fConnectionDesired )
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::CleanupOldEvents", "Sending Cancel Event" )
		<< "Sending event canceled message to publisher bridge head: " 
		<< ENDLOG;
	    AliHLTBridgeMsg msg;
	    msg.GetData()->fMsgType = kAliHLTBridgeEventCanceledMsg;
	    msg.GetData()->fEventType = eventID.fType;
	    msg.GetData()->fEventID = eventID.fNr;
	    msg.GetData()->fBlockCount = 0;
	    msg.GetData()->fFlags = kWaitDelivery;
	    if ( !fMsgConnected )
		ret = Connect();
	    if ( !fMsgConnected )
		{
		LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::CleanupOldEvents", "Connection error" )
		    << "Unable to connect to publisher bridge head: " 
		    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
		return;
		}
	    pthread_mutex_lock( &fMsgComMutex );
	    if ( fCommunicationTimeout )
		ret = fMsgCom->Send( fRemoteMsgAddress, msg.GetData(), fCommunicationTimeout );
	    else
		ret = fMsgCom->Send( fRemoteMsgAddress, msg.GetData() );
	    pthread_mutex_unlock( &fMsgComMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::CleanupOldEvents", "Msg Send Error" )
		    << "Error sending event canceled message to publisher bridge head: " 
		    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
		if ( fErrorCallback )
		    fErrorCallback->ConnectionError( this );
		//Disconnect();
		return;
		}
	    }
	ret = pthread_mutex_lock( &fEventSEDDMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::CleanupOldEvents", "Event SEDD Mutex Error" )
		<< "Error locking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << "). Continuing..." << ENDLOG;
	    }
#endif
	}
    if ( fStatus )
	{
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
	}
    ret = pthread_mutex_unlock( &fEventSEDDMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::CleanupOldEvents", "Event SEDD Mutex Error" )
	    << "Error unlocking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    return;
    }



bool AliHLTSubscriberBridgeHead::FindEventDataByPublisher( const PEventData& elem, uint64 ref )
    {
    if ( elem && elem->fPub == reinterpret_cast<AliHLTPublisherInterface*>(ref) )
	return true;
    return false;
    }

bool AliHLTSubscriberBridgeHead::FindEventDataByEvent( const PEventData& elem, const AliEventID_t& ref )
    {
    if ( elem && elem->fSEDD && elem->fSEDD->fEventID == ref )
	return true;
    return false;
    }

bool AliHLTSubscriberBridgeHead::FindOldEventData( const PEventData& elem, const AliUInt32_t& oldestEventBirth_s )
    {
    if ( elem && elem->fSEDD && elem->fSEDD->fEventBirth_s < oldestEventBirth_s )
	return true;
    return false;
    }

void AliHLTSubscriberBridgeHead::IterateEventDataAddTransferSignal( const PEventData& elem, void* arg )
    {
    if ( !elem->fTransferAborted )
	{
	elem->fTransferInProgress = true;
	reinterpret_cast<AliHLTTimerConditionSem*>(arg)->AddNotificationData( reinterpret_cast<AliUInt64_t>(elem) );
	}
    }


void AliHLTSubscriberBridgeHead::PURGEALLEVENTS()
    {
#if 0
    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::PURGEALLEVENTS", "PURGEALLEVENTS called" )
	<< "PURGE ALL EVENTS method called.!" << ENDLOG;
#endif
    int ret;
    EventData* ed;
    // fTransferSignal
    // fTransferData
    // fEventSEDDs
    // (fEventDataCache, fBufferManager)
    // fTimer
    vector<PEventData> eventData;
    vector<PEventData>::iterator evtIter, evtEnd;
    vector<AliUInt64_t> timerNotData;
    unsigned long timerCnts = 0, eventCnts = 0, transferCnts = 0;
    ret = pthread_mutex_lock( &fTransferMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::PURGEALLEVENTS", "Transfer Mutex Error" )
	    << "Error locking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    fTimer.CancelTimeouts( &fRetryCallback, timerNotData );

    fTransferSignal.Lock();
    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::PURGEALLEVENTS", "Timer Counts" )
	<< AliHLTLog::kDec << timerCnts << " timer events found." << ENDLOG;

    while ( fTransferSignal.HaveNotificationData() )
	{
	fTransferSignal.PopNotificationData();
	transferCnts++;
	}
	
    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::PURGEALLEVENTS", "Transfer Counts" )
	<< AliHLTLog::kDec << transferCnts << " transfer events found." << ENDLOG;

    pthread_mutex_lock( &fEventSEDDMutex );
    fAllEventsPurged = true;
    while ( fEventSEDDs.GetCnt()>0 )
	{
	ed = fEventSEDDs.GetFirst();
	eventData.insert( eventData.end(), ed );
	fEventSEDDs.RemoveFirst();
	eventCnts++;
	}

    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::PURGEALLEVENTS", "Stored Counts" )
	<< AliHLTLog::kDec << eventCnts << " stored events found." << ENDLOG;

    if ( fStatus )
	{
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	fStatus->fHLTStatus.fPendingInputEventCount = fTransferSignal.GetNotificationDataCnt();
	fStatus->fHLTStatus.fPendingOutputEventCount = fEventSEDDs.GetCnt()-fTransferSignal.GetNotificationDataCnt();
	}


    pthread_mutex_unlock( &fEventSEDDMutex );

    fTransferSignal.Unlock();

    ret = pthread_mutex_unlock( &fTransferMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::PURGEALLEVENTS", "Transfer Mutex Error" )
	    << "Error locking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}

    evtIter = eventData.begin();
    evtEnd = eventData.end();
    while ( evtIter != evtEnd )
	{
	ed = *evtIter;
	if ( ed )
	    {
	    AliHLTPublisherInterface* evtPub = NULL;
	    evtPub = ed->fPub;
	    ret = pthread_mutex_lock( &fDescriptorMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::PURGEALLEVENTS", "Descriptor Handler Mutex Error" )
		    << "Error locking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << "). Continuing..." << ENDLOG;
		}
	    if ( fDescriptorHandler && ed->fSEDD )
		fDescriptorHandler->ReleaseEventDescriptor( ed->fSEDD->fEventID );
	    ret = pthread_mutex_unlock( &fDescriptorMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::PURGEALLEVENTS", "Descriptor Handler Mutex Error" )
		    << "Error unlocking descriptor handler mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << "). Continuing..." << ENDLOG;
		}
	    if ( ed->fETS )
		{
		fETSCache.Release( (uint8*)ed->fETS );
		//delete [] (AliUInt8_t*)ed->fETS;
		}
	    if ( ed->fBufferNdx != ~(AliUInt32_t)0 && fBufferManager && fBufferManager->HasBuffer(ed->fBufferNdx) )
		{
		if ( !fBufferManager->ReleaseBlock( ed->fBufferNdx, ed->fBufferOffset ) )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::PURGEALLEVENTS", "Unable to release block" )
			<< "Unable to release block with buffer index " << AliHLTLog::kDec
			<< ed->fBufferNdx << "." << ENDLOG;
		    }
		ed->fBufferNdx = ~(AliUInt32_t)0;
		ed->fBufferOffset = 0;
		}
	    ed->fSEDD = NULL;
	    ed->fETS = NULL;
	    ed->fPub = NULL;
	    fEventDataCache.Release( (uint8*)ed );
	    }
	evtIter++;
	}
    if ( fStatus )
	{
// XXXX TODO LOCKING XXXX
	MLUCMutex::TLocker statusDataLock( fStatus->fStatusDataMutex );
	fStatus->fHLTStatus.fFreeOutputBuffer = fBufferManager->GetFreeBufferSize();
	}
    }

void AliHLTSubscriberBridgeHead::INTERRUPTBLOBCONNECTION()
    {
    fBlobCom->InterruptBlobConnection( fRemoteBlobMsgAddress );    
    }

void AliHLTSubscriberBridgeHead::DumpEventMetaData()
    {
    int ret = pthread_mutex_unlock( &fEventSEDDMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::NewEvent", "Event SEDD Mutex Error" )
	    << "Error unlocking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::DumpEventMetaData", "Event meta data dump" )
	<< "Dumping all current events to file." << ENDLOG;
    
    // We need to get the list of all current pending events, fetch their sub event descriptors,
    // dereference the shared memory pointers to all the data blocks and then write the
    // blocks to disk.

    MLUCString prefix;
    prefix = fName;
    prefix += "-Pending";
    AliHLTEventMetaDataOutput metaDataOutputPending( prefix.c_str(), fAliceHLT, fRunNumber );

    if ( fAliceHLT )
	metaDataOutputPending.SetEventTriggerDataFormatFunction(  AliHLTHLEventTriggerData2String );

    ret = metaDataOutputPending.Open();
    if ( ret!=0 )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::DumpEventMetaData", "Error opening event meta data output file" )
	    << "Error opening event meta data output file '" << prefix.c_str() << ": " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return;
	}

    MLUCVector<PEventData>::TIterator eventIter;
    eventIter = fEventSEDDs.Begin();
    while ( eventIter )
	{
	PEventData ed=*eventIter;
	if ( ed && ed->fSEDD && ed->fTransferInProgress && !ed->fEventDone )
	    metaDataOutputPending.WriteEvent( ed->fSEDD->fEventID, ed->fSEDD, ed->fETS, "pending" );
	++eventIter;
	}

    metaDataOutputPending.Close();


    prefix = fName;
    prefix += "-Sent";
    AliHLTEventMetaDataOutput metaDataOutputSent( prefix.c_str(), fAliceHLT, fRunNumber );

    if ( fAliceHLT )
	metaDataOutputSent.SetEventTriggerDataFormatFunction(  AliHLTHLEventTriggerData2String );

    ret = metaDataOutputSent.Open();
    if ( ret!=0 )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::DumpEventMetaData", "Error opening event meta data output file" )
	    << "Error opening event meta data output file '" << prefix.c_str() << ": " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return;
	}

    eventIter = fEventSEDDs.Begin();
    while ( eventIter )
	{
	PEventData ed=*eventIter;
	if ( ed && ed->fSEDD && (!ed->fTransferInProgress || ed->fEventDone) )
	    metaDataOutputSent.WriteEvent( ed->fSEDD->fEventID, ed->fSEDD, ed->fETS, "sent" );
	++eventIter;
	}

    metaDataOutputSent.Close();
    
    if (fEventSEDDs.GetCnt() == 0)
        {
        LOG( AliHLTLog::kDebug, "AliHLTSubscriberBridgeHead::DumpEventMetaData", "Nothing to dump" )
	    << "There were no events found to be dumped to disk" << ENDLOG;
	}


    ret = pthread_mutex_unlock( &fEventSEDDMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTSubscriberBridgeHead::NewEvent", "Event SEDD Mutex Error" )
	    << "Error unlocking transfer mutex: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << "). Continuing..." << ENDLOG;
	}
    }


AliHLTSubscriberBridgeHead::DefaultErrorCallback::DefaultErrorCallback()
    {
    fCorrectionInProgress = false;
    fConnected = false;
    pthread_mutex_init( &fProgressMutex, NULL );
    }

AliHLTSubscriberBridgeHead::DefaultErrorCallback::~DefaultErrorCallback()
    {
    pthread_mutex_destroy( &fProgressMutex );
    }

void AliHLTSubscriberBridgeHead::DefaultErrorCallback::ConnectionError( AliHLTSubscriberBridgeHead* bridgeHead )
    {
    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::DefaultErrorCallback::ConnectionError", "Connection Error" )
	<< "Error in connection. Attempting recovery." << ENDLOG;
    pthread_mutex_lock( &fProgressMutex );
    if ( fCorrectionInProgress )
	{
	pthread_mutex_unlock( &fProgressMutex );
	return;
	}
    fCorrectionInProgress = true;
    pthread_mutex_unlock( &fProgressMutex );
    
    fConnected = false;
    
    if ( bridgeHead )
	{
	bridgeHead->Pause();
	bridgeHead->LockCom();
	bridgeHead->Disconnect( true, true, 1000, false );
	bridgeHead->Connect( false );
	bridgeHead->UnlockCom();
	if ( fConnected )
	    bridgeHead->Restart();
	else
	    {
	    LOG( AliHLTLog::kError, "AliHLTSubscriberBridgeHead::DefaultErrorCallback::ConnectionError", "Unable to re-establish connection" )
		<< "Unable to re-establish connection to publisher bridge head. Remaining in paused state." << ENDLOG;
	    }
	}

    pthread_mutex_lock( &fProgressMutex );
    fCorrectionInProgress = false;
    pthread_mutex_unlock( &fProgressMutex );
    }

void AliHLTSubscriberBridgeHead::DefaultErrorCallback::ConnectionEstablished( AliHLTSubscriberBridgeHead* )
    {
    fConnected = true;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
