/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTolerantRoundRobinEventScatterer.hpp"
#include "AliHLTTolerantRoundRobinEventScattererCmd.hpp"
#include "AliHLTSCCommands.hpp"
#include "AliHLTLog.hpp"
#include <errno.h>


AliHLTTolerantRoundRobinEventScatterer::AliHLTTolerantRoundRobinEventScatterer():
    fCommandProcessor( *this )
    {
    //fUpPublisherCnt = 0;
    fTmpSEDD = NULL;
    fTmpETS = NULL;
    fStatus = NULL;
    fLAMProxy = NULL;
    pthread_mutex_init( &fDescriptorMutex, NULL );
    //pthread_mutex_init( &fToleranceMutex, NULL );
    }

AliHLTTolerantRoundRobinEventScatterer::~AliHLTTolerantRoundRobinEventScatterer()
    {
    if ( fTmpSEDD )
	delete [] (AliUInt8_t*)fTmpSEDD;
    if ( fTmpETS )
	delete [] (AliUInt8_t*)fTmpETS;
    pthread_mutex_destroy( &fDescriptorMutex );
    //pthread_mutex_destroy( &fToleranceMutex );
    }

void AliHLTTolerantRoundRobinEventScatterer::PublisherAdded( AliHLTEventScattererRePublisher* publisher )
    {
    if ( !publisher )
	return;
//     vector<bool>::iterator iter, end;

    //pthread_mutex_lock( &fToleranceMutex );
    fToleranceHandler.Add();
    //pthread_mutex_unlock( &fToleranceMutex );
//     fPublishersOk.insert( fPublishersOk.end(), true );
//     iter = fPublishersOk.begin();
//     end = fPublishersOk.end();
//     fPublisherMaps.insert( fPublisherMaps.end(), 0 );
//     *(fPublisherMaps.begin()+fUpPublisherCnt) = fPublisherCount-1;
//     fUpPublisherCnt++;
    PublisherData pd;
    fEvents.insert( fEvents.end(), pd );
    }


int AliHLTTolerantRoundRobinEventScatterer::AnnounceEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, const AliHLTEventTriggerStruct& trigger )
    {
    LOG( AliHLTLog::kDebug, "AliHLTTolerantRoundRobinEventScatterer::AnnounceEvent", "Event Received" )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	<< eventID << ") - sedd 0x" << AliHLTLog::kHex << (unsigned long)&sbevent << " received." << ENDLOG;
    int ret = 0;

    bool retry = false;
    if ( fPublisherCount <= 0 )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::AnnounceEvent", "No Publisher" )
	    << "No publisher specified." << ENDLOG;
	return ENODEV;
	}
    unsigned long oNdx, bNdx;

    EventData ed;
    ed.fEventID = eventID;
    gettimeofday( &ed.fTime, NULL );
    //ed.fETS = &trigger;
    ed.fETS = (AliHLTEventTriggerStruct*) new AliUInt8_t[ trigger.fHeader.fLength ];
    if ( !ed.fETS )
	{
	LOG( AliHLTLog::kError, "AliHLTTolerantRoundRobinEventScatterer::AnnounceEvent", "Out of memory" )
	    << "Out of memory trying to allocate event trigger struct of " << AliHLTLog::kDec
	    << trigger.fHeader.fLength << " bytes. " << ENDLOG;
	}
    else
	memcpy( ed.fETS, &trigger, trigger.fHeader.fLength );
    ed.fSEDD = NULL;
    if ( fDescriptorHandler )
	{
	ret = pthread_mutex_lock( &fDescriptorMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::AnnounceEvent", "Mutex Lock Error" )
		<< "Error locking descriptor handler mutex: " << AliHLTLog::kDec << strerror(ret) << " ("
		<< ret << "). Continuing..." << ENDLOG;
	    }
	ed.fSEDD = fDescriptorHandler->GetFreeEventDescriptor( eventID, sbevent.fDataBlockCount );
	ret = pthread_mutex_unlock( &fDescriptorMutex );
	if ( ret )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::AnnounceEvent", "Mutex Unlock Error" )
		<< "Error unlocking descriptor handler mutex: " << AliHLTLog::kDec << strerror(ret) << " ("
		<< ret << "). Continuing..." << ENDLOG;
	    }
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::AnnounceEvent", "No Descriptor Handler" )
	    << "No Descriptor Handler set." << ENDLOG;
	}
    if ( !ed.fSEDD )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::AnnounceEvent", "No Event Descriptor" )
	    << "Cannot get sub-event descriptor." << ENDLOG;
	}
    else
	{
	memcpy( ed.fSEDD, &sbevent, sbevent.fHeader.fLength );
	}



    //if ( GetPublisherIndex( eventID, oNdx, bNdx ) )
    //pthread_mutex_lock( &fToleranceMutex );
    if ( fToleranceHandler.Get( eventID, oNdx, bNdx ) )
	{
	if ( oNdx!=bNdx )
	    {
	    LOG( AliHLTLog::kInformational, "AliHLTTolerantRoundRobinEventScatterer::AnnounceEvent", "Dispatching event to alternate target" )
		<< "Dispatching event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID 
		<< ") to alternate publisher " << bNdx << " (" << fPublishers[ bNdx ]->GetName() 
		<< ") instead of default publisher " << oNdx << " (" << fPublishers[ oNdx ]->GetName() 
		<< ")." << ENDLOG;
	    }
	else
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTTolerantRoundRobinEventScatterer::AnnounceEvent", "Dispatching event to default target" )
		<< "Dispatching event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID 
		<< ") to default publisher " << oNdx << " (" << fPublishers[ oNdx ]->GetName() 
		<< ")." << ENDLOG;
	    }
	ret = fPublishers[ bNdx ]->AnnounceEvent( eventID, sbevent, trigger );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "AliHLTTolerantRoundRobinEventScatterer::AnnounceEvent", "Error Announcing Event" )
		<< "Error announcing event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID
		<< ") to publisher " << bNdx << " (" << fPublishers[ bNdx ]->GetName() 
		<< "): " << strerror(ret) << " (" << ret << ")." << ENDLOG;
	    }
	fEvents[ bNdx ].fEvents.Add( ed );
	}
    else
	{
	retry = true;
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::AnnounceEvent", "No Publisher" )
	    << "No working publisher available. Retrying later..." << ENDLOG;
	}
    //pthread_mutex_unlock( &fToleranceMutex );
    if ( retry )
	{
	fRetryEvents.Add( ed );
	}
    return ret;
    }

void AliHLTTolerantRoundRobinEventScatterer::ReleasingEvent( AliEventID_t eventID, AliHLTEventScattererRePublisher* publisher )
    {
    int ret;
    unsigned ndx = 0;
    bool found = false;
    vector<AliHLTEventScattererRePublisher*>::iterator pubIter, pubEnd;
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    while ( pubIter != pubEnd )
	{
	if ( *pubIter == publisher )
	    {
	    found = true;
	    break;
	    }
	ndx++;
	pubIter++;
	}

    if ( found && fToleranceHandler.IsOk( ndx ) )
	{
	unsigned long elemNdx;
	if ( fEvents[ ndx ].fEvents.FindElement( &EventSearchFunc, (uint64)eventID, elemNdx ) )
	    {
	    if ( fDescriptorHandler )
		{
		ret = pthread_mutex_lock( &fDescriptorMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::EventDone", "Mutex Lock Error" )
			<< "Error locking descriptor handler mutex: " << AliHLTLog::kDec << strerror(ret) << " ("
			<< ret << "). Continuing..." << ENDLOG;
		    }
		fDescriptorHandler->ReleaseEventDescriptor( eventID );
		ret = pthread_mutex_unlock( &fDescriptorMutex );
		if ( ret )
		    {
		    LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::EventDone", "Mutex Unlock Error" )
			<< "Error unlocking descriptor handler mutex: " << AliHLTLog::kDec << strerror(ret) << " ("
			<< ret << "). Continuing..." << ENDLOG;
		    }
		}
	    EventData* evd;
	    evd = fEvents[ ndx ].fEvents.GetPtr( elemNdx );
	    if ( evd->fETS )
		delete [] (AliUInt8_t*)evd->fETS;
	    fEvents[ ndx ].fEvents.Remove( elemNdx );
	    LOG( AliHLTLog::kDebug, "AliHLTTolerantRoundRobinEventScatterer::EventDone", "Event Released" )
		<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") released from publisher " << ndx << " ('" << fPublishers[ndx]->GetName()
		<< "')." << ENDLOG;
	    }
	else
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::EventDone", "Event Not Found" )
		<< "Error finding event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ") listed for publisher " << ndx << " ('" << fPublishers[ndx]->GetName()
		<< "')." << ENDLOG;
	    }
	}
    else
	{
	if ( !found )
	    {
	    LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::EventDone", "No Publisher Found" )
		<< "Error finding publisher for event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
		<< eventID << ")." << ENDLOG;
	    }
// 	else
// 	    {
// 	    LOG( AliHLTLog::kInformational, "AliHLTTolerantRoundRobinEventScatterer::EventDone", "Publisher Down" )
// 		<< "Publisher " << AliHLTLog::kDec << ndx << " found for event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
// 		<< eventID << ")." << ENDLOG;
// 	    }
	}
    }

int AliHLTTolerantRoundRobinEventScatterer::EventCanceled( AliEventID_t eventID )
    {
    int ret;
    ret = pthread_mutex_lock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::EventCanceled", "Mutex lock error" )
	    << "Error locking publisher mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}

    vector<PublisherData>::iterator iter, end;
    unsigned long elemNdx;
    unsigned ndx = 0;
    unsigned long oNdx = 0, bNdx = 0;
    bool found = false;
    //pthread_mutex_lock( &fToleranceMutex );
    if ( fToleranceHandler.Get( eventID, oNdx, bNdx ) )
	{
	if ( fEvents[ ndx ].fEvents.FindElement( &EventSearchFunc, (uint64)eventID, elemNdx ) )
	    {
	    ndx = oNdx;
	    found = true;
	    }
	else if ( (oNdx != bNdx) && fEvents[ bNdx ].fEvents.FindElement( &EventSearchFunc, (uint64)eventID, elemNdx ) )
	    {
	    ndx = bNdx;
	    found = true;
	    }
	}
    //pthread_mutex_unlock( &fToleranceMutex );
    if ( !found )
	{
	iter = fEvents.begin();
	end = fEvents.end();
	while ( iter != end )
	    {
	    if ( ndx != oNdx && ndx != bNdx )
		{
		if ( iter->fEvents.FindElement( &EventSearchFunc, (uint64)eventID, elemNdx ) )
		    {
		    found = true;
		    break;
		    }
		}
	    ndx++;
	    iter++;
	    }
	}
    if ( found )
	{
	iter = fEvents.begin()+ndx;
	if ( fDescriptorHandler )
	    {
	    ret = pthread_mutex_lock( &fDescriptorMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::EventCanceled", "Mutex Lock Error" )
		    << "Error locking descriptor handler mutex: " << AliHLTLog::kDec << strerror(ret) << " ("
		    << ret << "). Continuing..." << ENDLOG;
		}
	    fDescriptorHandler->ReleaseEventDescriptor( eventID );
	    ret = pthread_mutex_unlock( &fDescriptorMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::EventCanceled", "Mutex Unlock Error" )
		    << "Error unlocking descriptor handler mutex: " << AliHLTLog::kDec << strerror(ret) << " ("
		    << ret << "). Continuing..." << ENDLOG;
		}
	    }
 	EventData* evd;
	evd = iter->fEvents.GetPtr( elemNdx );
	if ( evd->fETS )
	    delete [] (AliUInt8_t*)evd->fETS;
	iter->fEvents.Remove( elemNdx );
	// XXX
	fPublishers[ ndx ]->AbortEvent( eventID );
	LOG( AliHLTLog::kDebug, "AliHLTTolerantRoundRobinEventScatterer::EventCanceled", "Event Released" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") released from publisher " << ndx << " ('" << fPublishers[ndx]->GetName()
	    << "')." << ENDLOG;
	}
    else if ( fRetryEvents.FindElement( &EventSearchFunc, (uint64)eventID, elemNdx ) )
	{
	LOG( AliHLTLog::kInformational, "AliHLTTolerantRoundRobinEventScatterer::EventCanceled", "Event to release not yet announced" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") to be released has not been announced yet. (Placed in retry list)" << ENDLOG;
	
	if ( fDescriptorHandler )
	    {
	    ret = pthread_mutex_lock( &fDescriptorMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::EventCanceled", "Mutex Lock Error" )
		    << "Error locking descriptor handler mutex: " << AliHLTLog::kDec << strerror(ret) << " ("
		    << ret << "). Continuing..." << ENDLOG;
		}
	    fDescriptorHandler->ReleaseEventDescriptor( eventID );
	    ret = pthread_mutex_unlock( &fDescriptorMutex );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::EventCanceled", "Mutex Unlock Error" )
		    << "Error unlocking descriptor handler mutex: " << AliHLTLog::kDec << strerror(ret) << " ("
		    << ret << "). Continuing..." << ENDLOG;
		}
	    }
 	EventData* evd;
	evd = fRetryEvents.GetPtr( elemNdx );
	if ( evd->fETS )
	    delete [] (AliUInt8_t*)evd->fETS;
	fRetryEvents.Remove( elemNdx );
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::EventCanceled", "Event to Release Not Found" )
	    << "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec
	    << eventID << ") to be released not for. Sending to all publishers..." << ENDLOG;
	vector<AliHLTEventScattererRePublisher*>::iterator pIter, pEnd;
	pIter = fPublishers.begin();
	pEnd = fPublishers.end();
	while ( pIter != pEnd )
	    {
	    (*pIter)->AbortEvent( eventID );
	    pIter++;
	    }
	}

    ret = pthread_mutex_unlock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::EventCanceled", "Mutex unlock error" )
	    << "Error unlocking publisher mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    return 0;
    }

int AliHLTTolerantRoundRobinEventScatterer::RequestEventRelease()
    {
    int ret, retval;
    vector<AliHLTEventScattererRePublisher*>::iterator iter, end;
    ret = pthread_mutex_lock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::RequestEventRelease", "Mutex lock error" )
	    << "Error locking publisher mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}

    iter = fPublishers.begin();
    end = fPublishers.end();
    while ( iter != end )
	{
	if ( (*iter) )
	    {
	    ret = (*iter)->RequestEventRelease();
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTTolerantRoundRobinEventScatterer::RequestEventRelease", "Publisher send error" )
		    << "Error sending RequestEventRelease to publisher " << (*iter)->GetName() << ": "
		    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		    << ENDLOG;
		retval = ret;
		}
	    }
	iter++;
	}

    ret = pthread_mutex_unlock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::RequestEventRelease", "Mutex unlock error" )
	    << "Error unlocking publisher mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    return 0;
    }

int AliHLTTolerantRoundRobinEventScatterer::CancelAllSubscriptions()
    {
    int ret, retval;
    vector<AliHLTEventScattererRePublisher*>::iterator iter, end;
    ret = pthread_mutex_lock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::CancelAllSubscriptions", "Mutex lock error" )
	    << "Error locking publisher mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}

    iter = fPublishers.begin();
    end = fPublishers.end();
    while ( iter != end )
	{
	if ( (*iter) )
	    {
	    ret = (*iter)->CancelAllSubscriptions( true );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "AliHLTTolerantRoundRobinEventScatterer::CancelAllSubscriptions", "Publisher send error" )
		    << "Error sending CancelAllSubscriptions to publisher " << (*iter)->GetName() << ": "
		    << strerror(ret) << " (" << AliHLTLog::kDec << ret << ")."
		    << ENDLOG;
		retval = ret;
		}
	    }
	iter++;
	}

    ret = pthread_mutex_unlock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::CancelAllSubscriptions", "Mutex unlock error" )
	    << "Error unlocking publisher mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    return 0;
    }

int AliHLTTolerantRoundRobinEventScatterer::PublisherDown( AliHLTEventScattererRePublisher* publisher )
    {
    if ( !publisher )
	return EFAULT;
    int ret;
    ret = pthread_mutex_lock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::PublisherDown", "Mutex lock error" )
	    << "Error locking status mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}

    unsigned long ndx = 0;
    vector<AliHLTEventScattererRePublisher*>::iterator iter, end;
    iter = fPublishers.begin();
    end = fPublishers.end();
    while ( iter != end )
	{
	if ( *iter == publisher )
	    break;
	ndx++;
	iter++;
	}
    ret = pthread_mutex_unlock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::PublisherDown", "Mutex unlock error" )
	    << "Error unlocking status mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    LOG( AliHLTLog::kError, "AliHLTTolerantRoundRobinEventScatterer::PublisherDown", "Publisher Down" )
	<< "Publisher with index " << AliHLTLog::kDec << ndx << " is down." << ENDLOG;
    if ( iter != end )
	{
	if ( fStatus )
	    {
	    if ( ndx < fStatus->fChannelStatus.fChannelsCnt )
		{
		fStatus->fChannelStatus.fChannels[ndx] = 0;
		if ( fLAMProxy )
		    fLAMProxy->LookAtMe();
		}
	    }
	}
    else
	return ENODEV;
    return 0;
    }

int AliHLTTolerantRoundRobinEventScatterer::PublisherSendError( AliHLTEventScattererRePublisher*, AliEventID_t )
    {
    return 0;
    }


void AliHLTTolerantRoundRobinEventScatterer::SetupCommandProcessor( AliHLTSCInterface* interface )
    {
    if ( interface )
	interface->AddCommandProcessor( &fCommandProcessor );
    }


void AliHLTTolerantRoundRobinEventScatterer::SetPublisher( AliHLTEventScattererRePublisher* publisher, bool up, AliEventID_t )
    {
    if ( !publisher )
	return;
    int ret;
    ret = pthread_mutex_lock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::SetPublisher", "Mutex lock error" )
	    << "Error locking status mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}

    unsigned long ndx = 0; // i, n, cnt, ndx=0;
    bool doSet = false;
    vector<AliHLTEventScattererRePublisher*>::iterator iter, end;
    vector<AliEventID_t> publisherEvents;
    vector<AliEventID_t>::iterator evtIter, evtEnd;
    iter = fPublishers.begin();
    end = fPublishers.end();
    while ( iter != end )
	{
	if ( *iter == publisher )
	    break;
	ndx++;
	iter++;
	}
    if ( iter != end )
	{
	//pthread_mutex_lock( &fToleranceMutex );
	if ( fToleranceHandler.IsOk( ndx ) == up )
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTTolerantRoundRobinEventScatterer::SetPublisher", "Setting Publisher" )
	        << "Publisher '" << publisher->GetName() << "' (" << AliHLTLog::kDec << ndx << ") already set to " << (up ? "up" : "down") << "." << ENDLOG;
	    }
	else
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTTolerantRoundRobinEventScatterer::SetPublisher", "Setting Publisher" )
	        << "Setting publisher '" << publisher->GetName() << "' (" << AliHLTLog::kDec << ndx << ") to " << (up ? "up" : "down") << "." << ENDLOG;
	    fToleranceHandler.Set( ndx, up );
	    doSet = true;
	    }
	//pthread_mutex_unlock( &fToleranceMutex );
// 	fUpPublisherCnt += ( up ? +1 : -1 );
// 	*(fPublishersOk.begin()+ndx) = up;
// 	n = cnt = 0;
// 	for ( i = 0; i < fPublisherCount; i++ )
// 	    {
// 	    if ( fPublishersOk[i] )
// 		{
// 		*(fPublisherMaps.begin()+n) = i; // i+cnt??
// 		n++;
// 		}
// 	    else
// 		cnt++;
// 	    }

	if ( up && fRetryEvents.GetCnt()>0 )
	    {
	    while ( fRetryEvents.GetCnt()>0 )
		{
		// XXX
 		EventData ed;
		ed = fRetryEvents.GetFirst();
		fRetryEvents.RemoveFirst();
		bool ok = true;
		if ( fTmpSEDD && fTmpSEDD->fHeader.fLength<ed.fSEDD->fHeader.fLength )
		    {
		    delete [] (AliUInt8_t*)fTmpSEDD;
		    fTmpSEDD = NULL;
		    }
		if ( !fTmpSEDD )
		    fTmpSEDD = (AliHLTSubEventDataDescriptor*)new AliUInt8_t[ ed.fSEDD->fHeader.fLength ];
		if ( !fTmpSEDD )
		    {
		    LOG( AliHLTLog::kError, "AliHLTTolerantRoundRobinEventScatterer::SetPublisher", "Out of Memory" )
			<< "Out of memory trying to allocate tmp descriptor of " << AliHLTLog::kDec
			<< ed.fSEDD->fHeader.fLength << " Bytes." << ENDLOG;
		    ok = false;
		    // XXX
		    // Retry
		    }
		if ( fTmpETS && ed.fETS && fTmpETS->fHeader.fLength<ed.fETS->fHeader.fLength )
		    {
		    delete [] (AliUInt8_t*)fTmpETS;
		    fTmpETS = NULL;
		    }
		if ( !fTmpETS && ed.fETS )
		    fTmpETS = (AliHLTEventTriggerStruct*)new AliUInt8_t[ ed.fETS->fHeader.fLength ];
		if ( !fTmpETS && ed.fETS )
		    {
		    LOG( AliHLTLog::kError, "AliHLTTolerantRoundRobinEventScatterer::SetPublisher", "Out of Memory" )
			<< "Out of memory trying to allocate tmp event trigger struct of " << AliHLTLog::kDec
			<< ed.fETS->fHeader.fLength << " Bytes." << ENDLOG;
		    ok = false;
		    // XXX
		    // Retry
		    }
		if ( ok )
		    {
		    memcpy( fTmpSEDD, ed.fSEDD, ed.fSEDD->fHeader.fLength );
		    if ( ed.fETS )
			memcpy( fTmpETS, ed.fETS, ed.fETS->fHeader.fLength );
		    if ( fDescriptorHandler )
			{
			ret = pthread_mutex_lock( &fDescriptorMutex );
			if ( ret )
			    {
			    LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::SetPublisher", "Mutex Lock Error" )
				<< "Error locking descriptor handler mutex: " << AliHLTLog::kDec << strerror(ret) << " ("
				<< ret << "). Continuing..." << ENDLOG;
			    }
			fDescriptorHandler->ReleaseEventDescriptor( ed.fEventID );
			ret = pthread_mutex_unlock( &fDescriptorMutex );
			if ( ret )
			    {
			    LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::SetPublisher", "Mutex Unlock Error" )
				<< "Error unlocking descriptor handler mutex: " << AliHLTLog::kDec << strerror(ret) << " ("
				<< ret << "). Continuing..." << ENDLOG;
			    }
			}
		    if ( ed.fETS )
			delete [] (AliUInt8_t*)ed.fETS;
		    ret = AnnounceEvent( ed.fEventID, *fTmpSEDD, *fTmpETS );
		    if ( ret )
			{
			LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::SetPublisher", "Error Re-Announcing Event" )
			    << "Error re-announcing event 0x" << AliHLTLog::kHex << ed.fEventID << " (" << AliHLTLog::kDec
			    << ed.fEventID << ")." << ENDLOG;
			}
		    }
		}
	    }

	if ( !up && doSet )
	    {
	    // Redispatch events...
	    // XXX
	    //MLUCVector<EventData> dispatchedEvents;
// 	    struct timeval pT;
// 	    unsigned long lastEvtNdx;
// 	    unsigned long newerEvtNdx;
// 	    uint64 t64;
// 	    if ( lastEventID != ~(AliEventID_t)0 )
// 		{
// 		if ( fEvents[ndx].fEvents.FindElement( &EventSearchFunc, (uint64)lastEventID, lastEvtNdx ) )
// 		    {
// 		    pT = fEvents[ndx].fEvents.GetPtr( lastEvtNdx )->fTime;
// 		    t64 = pT.tv_sec;
// 		    t64 <<= 32;
// 		    t64 |= pT.tv_usec;
// 		    }
// 		else
// 		    t64 = 0;
// 		// If last event could not be found, assume it has already been done and all events 
// 		// still present came in after it.
// 		}
// 	    else
// 		t64 = 0;
// 	    while ( fEvents[ndx].fEvents.FindElement( &TimeLaterSearchFunc, (uint64)t64, newerEvtNdx ) )
// 		{
// 		EventData ed;
// 		ed = *(fEvents[ndx].fEvents.GetPtr( newerEvtNdx ));
// 		fEvents[ndx].fEvents.Remove( newerEvtNdx );
	    // Now we use all events that we have not received back so far...
	    while ( fEvents[ndx].fEvents.GetCnt()>0 )
		{
 		EventData ed;
		ed = fEvents[ndx].fEvents.GetFirst();
		fEvents[ndx].fEvents.RemoveFirst();
		bool ok = true;
		if ( fTmpSEDD && fTmpSEDD->fHeader.fLength<ed.fSEDD->fHeader.fLength )
		    {
		    delete [] (AliUInt8_t*)fTmpSEDD;
		    fTmpSEDD = NULL;
		    }
		if ( !fTmpSEDD )
		    fTmpSEDD = (AliHLTSubEventDataDescriptor*)new AliUInt8_t[ ed.fSEDD->fHeader.fLength ];
		if ( !fTmpSEDD )
		    {
		    LOG( AliHLTLog::kError, "AliHLTTolerantRoundRobinEventScatterer::SetPublisher", "Out of Memory" )
			<< "Out of memory trying to allocate tmp descriptor of " << AliHLTLog::kDec
			<< ed.fSEDD->fHeader.fLength << " Bytes." << ENDLOG;
		    ok = false;
		    // XXX
		    // Retry
		    }
		if ( fTmpETS && ed.fETS && fTmpETS->fHeader.fLength<ed.fETS->fHeader.fLength )
		    {
		    delete [] (AliUInt8_t*)fTmpETS;
		    fTmpETS = NULL;
		    }
		if ( !fTmpETS && ed.fETS )
		    fTmpETS = (AliHLTEventTriggerStruct*)new AliUInt8_t[ ed.fETS->fHeader.fLength ];
		if ( !fTmpETS && ed.fETS )
		    {
		    LOG( AliHLTLog::kError, "AliHLTTolerantRoundRobinEventScatterer::SetPublisher", "Out of Memory" )
			<< "Out of memory trying to allocate tmp event trigger struct of " << AliHLTLog::kDec
			<< ed.fETS->fHeader.fLength << " Bytes." << ENDLOG;
		    ok = false;
		    // XXX
		    // Retry
		    }
		else
		    {
		    memcpy( fTmpSEDD, ed.fSEDD, ed.fSEDD->fHeader.fLength );
		    if ( ed.fETS )
			memcpy( fTmpETS, ed.fETS, ed.fETS->fHeader.fLength );
		    if ( fDescriptorHandler )
			{
			ret = pthread_mutex_lock( &fDescriptorMutex );
			if ( ret )
			    {
			    LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::SetPublisher", "Mutex Lock Error" )
				<< "Error locking descriptor handler mutex: " << AliHLTLog::kDec << strerror(ret) << " ("
				<< ret << "). Continuing..." << ENDLOG;
			    }
			fDescriptorHandler->ReleaseEventDescriptor( ed.fEventID );
			ret = pthread_mutex_unlock( &fDescriptorMutex );
			if ( ret )
			    {
			    LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::SetPublisher", "Mutex Unlock Error" )
				<< "Error unlocking descriptor handler mutex: " << AliHLTLog::kDec << strerror(ret) << " ("
				<< ret << "). Continuing..." << ENDLOG;
			    }
			}
		    publisherEvents.insert( publisherEvents.end(), ed.fEventID );
		    ret = AnnounceEvent( ed.fEventID, *fTmpSEDD, *fTmpETS );
		    if ( ret )
			{
			LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::SetPublisher", "Error Re-Announcing Event" )
			    << "Error re-announcing event 0x" << AliHLTLog::kHex << ed.fEventID << " (" << AliHLTLog::kDec
			    << ed.fEventID << ")." << ENDLOG;
			}
		    }
		}
	    }
	

// 	if ( (n != fUpPublisherCnt) || (n+cnt != fPublisherCount) )
// 	    {
// 	    LOG( AliHLTLog::kFatal, "AliHLTTolerantRoundRobinEventScatterer::SetPublisher", "Internal Error" )
// 		<< "Internal error setting publisher '" << publisher->GetName() << "` to " << (up ? "up" : "down")
// 		<< ": i==" << AliHLTLog::kDec << i << " - n==" << n << " - cnt==" << cnt << " - fPublisherCount=="
// 		<< fPublisherCount << " - fUpPublisherCnt==" << fUpPublisherCnt << "." << ENDLOG;
// 	    }
	}
    else
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::SetPublisher", "Publisher not found" )
	    << "Publisher '" << publisher->GetName() << "' to set to " << (up ? "up" : "down") << " not found." << ENDLOG;
	}

    ret = pthread_mutex_unlock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::SetPublisher", "Mutex unlock error" )
	    << "Error unlocking status mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}

    evtIter = publisherEvents.begin();
    evtEnd = publisherEvents.end();
    while ( evtIter != evtEnd )
	{
	publisher->AbortEvent( *evtIter, false );
	evtIter++;
	}
    }

// bool AliHLTTolerantRoundRobinEventScatterer::GetPublisherIndex( AliEventID_t eventID, unsigned& baseNdx, unsigned& backupNdx )
//     {

//     if ( fPublisherCount <= 0 )
// 	{
// 	return false;
// 	}

//     unsigned mod = (eventID % fPublisherCount );
//     baseNdx = mod;
//     if ( (mod>=fPublishersOk.size()) || (mod>=fPublishers.size()) )
// 	{
// 	return false;
// 	}
    
//     if ( fPublishersOk[ mod ] )
// 	{
// 	backupNdx = mod;
// 	}
//     else
// 	{
// 	if ( fUpPublisherCnt<= 0 )
// 	    return false;
// 	mod = (eventID % fUpPublisherCnt );
// 	if ( mod >= fPublisherMaps.size() )
// 	    {
// 	    return false;
// 	    }
// 	mod = fPublisherMaps[ mod ];
// 	if ( (mod>=fPublishersOk.size()) || (mod>=fPublishers.size()) )
// 	    {
// 	    return false;
// 	    }
// 	backupNdx = mod;
// 	}

//     return true;
//     }

void AliHLTTolerantRoundRobinEventScatterer::ProcessCmd( AliHLTSCCommandStruct* cmd )
    {
    AliHLTSCCommand cmdC( cmd );
    AliHLTTolerantRoundRobinEventScattererCmd edCmdC;
    if ( cmdC.GetData()->fCmd != kAliHLTSCSetPublisherStatus || cmdC.GetData()->fLength != edCmdC.GetData()->fLength )
	return; // Unknown command.
    edCmdC.SetTo( (AliHLTTolerantRoundRobinEventScattererCmdStruct*)cmd );
    int ret;
    bool up = (bool)cmd->fParam1;
    unsigned ndx = cmd->fParam0;
    ret = pthread_mutex_lock( &fPublisherMutex );
    if ( ret )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::ProcessCmd", "Mutex lock error" )
	    << "Error locking publisher mutex: " << strerror(ret)
	    << AliHLTLog::kDec << " (" << ret << "). Continuing..." << ENDLOG;
	}
    AliHLTEventScattererRePublisher* publisher;
    if (  ndx >= fPublisherCount )
	{
	LOG( AliHLTLog::kWarning, "AliHLTTolerantRoundRobinEventScatterer::ProcessCmd", "Wrong Publisher Number" )
	    << "Error number of publisher to set " << (up ? "up" : "down") << " ("
	    << AliHLTLog::kDec << ndx << ") is larger than or equal to number of publishers ("
	    << fPublisherCount << ")." << ENDLOG;
	ret = pthread_mutex_unlock( &fPublisherMutex );
	return;
	}

    publisher = fPublishers[ ndx ];

    ret = pthread_mutex_unlock( &fPublisherMutex );

    SetPublisher( publisher, up, edCmdC.GetData()->fLastEventID );
    }


bool AliHLTTolerantRoundRobinEventScatterer::EventSearchFunc( const EventData& elem, uint64 reference )
    {
    return elem.fEventID == (AliEventID_t)reference;
    }

bool AliHLTTolerantRoundRobinEventScatterer::TimeLaterSearchFunc( const EventData& elem, uint64 reference )
    {
    long t_sec, t_usec;
    t_sec = (AliUInt32_t)( reference >> 32 );
    t_usec = (AliUInt32_t)( reference & 0xFFFFFFFF );
    
    if ( (elem.fTime.tv_sec > t_sec) || 
	 ( (elem.fTime.tv_sec==t_sec) && (elem.fTime.tv_usec>t_usec) ) )
	return true;
    else
	return false;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
