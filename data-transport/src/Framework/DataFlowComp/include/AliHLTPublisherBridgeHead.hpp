/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3PUBLISHERBRIDGEHEAD_HPP_
#define _ALIL3PUBLISHERBRIDGEHEAD_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSamplePublisher.hpp"
#include "AliHLTSharedMemory.hpp"
#include "AliHLTBridgeHeadHelper.hpp"
#include "BCLMsgCommunication.hpp"
#include "BCLBlobCommunication.hpp"
#include "AliHLTEventTriggerStruct.h"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTThread.hpp"
#include "AliHLTTimer.hpp"
#include "AliHLTBridgeStatusData.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "MLUCVector.hpp"
#include "MLUCAllocCache.hpp"
#include "MLUCObjectCache.hpp"
#include "MLUCDynamicAllocCache.hpp"

class AliHLTEventAccounting;

class AliHLTPublisherBridgeHead: public AliHLTSamplePublisher
    {
    public:
  
	AliHLTPublisherBridgeHead( const char* name, int eventSlotsExp2 = -1 );
	~AliHLTPublisherBridgeHead();

	void SetDescriptorHandler( AliHLTDescriptorHandler* descrHandler )
		{
		fDescriptorHandler = descrHandler;
		}

	void SetMsgCom( BCLMsgCommunication* msgCom, BCLAbstrAddressStruct* remoteMsgAddress )
		{
		if ( fReceiveAddr && fMsgCom )
		    {
		    fMsgCom->DeleteAddress( fReceiveAddr );
		    fReceiveAddr = NULL;
		    }
		fMsgCom = msgCom;
		fRemoteMsgAddress = remoteMsgAddress;
		if ( fMsgCom )
		    fReceiveAddr = fMsgCom->NewAddress();
		}
	void SetRemoteMsgAddress( BCLAbstrAddressStruct* remoteMsgAddress )
		{
		fRemoteMsgAddress = remoteMsgAddress;
		}
	void SetBlobCom( BCLBlobCommunication* blobCom, BCLMsgCommunication* blobMsgCom, BCLAbstrAddressStruct* remoteBlobMsgAddress )
		{
		fBlobCom = blobCom;
		fBlobCom->EnableReceiveNotification( true );
		fBlobMsgCom = blobMsgCom;
		fRemoteBlobMsgAddress = remoteBlobMsgAddress;
		}
	void SetRemoteBlobMsgAddress( BCLAbstrAddressStruct* remoteBlobMsgAddress )
		{
		fRemoteBlobMsgAddress = remoteBlobMsgAddress;
		}

	void SetCommunicationTimeout( unsigned long connectionTimeout )
		{
		fCommunicationTimeout = connectionTimeout;
		}

	void SetEventAccounting( AliHLTEventAccounting* eventAccounting )
		{
		fEventAccounting = eventAccounting;
		}

	void LockCom()
		{
		pthread_mutex_lock( &fComAccessMutex );
		pthread_mutex_lock( &fMsgComMutex );
		}

	void UnlockCom()
		{
		pthread_mutex_unlock( &fMsgComMutex );
		pthread_mutex_unlock( &fComAccessMutex );
		}

	void LockRecvCom()
		{
		pthread_mutex_lock( &fComRecvMutex );
		}

	void UnlockRecvCom()
		{
		pthread_mutex_unlock( &fComRecvMutex );
		}


	bool IsMsgConnected()
		{
		return fMsgConnected;
		}

	bool IsBlobConnected()
		{
		return fBlobConnected;
		}

	void SetShmID( AliHLTShmID shmID )
		{
		fShmID = shmID;
		}

	void SetMaxRetryCount( AliUInt32_t count )
		{
		fMaxRetryCount = count;
		}

	void SetRetryTime( AliUInt32_t retryTime_ms )
		{
		fRetryTime = retryTime_ms;
		}

	void SetLAMProxy( AliHLTBridgeHeadLAMProxy* proxy )
		{
		fLAMProxy = proxy;
		}

	void SetStatusData( AliHLTBridgeStatusData* status )
		{
		fStatus = status;
		}

	void SetErrorCallback( AliHLTBridgeHeadErrorCallback<AliHLTPublisherBridgeHead>* errorCallback )
		{
		fErrorCallback = errorCallback;
		}

	void AliceHLT()
		{
		fAliceHLT = true;
		}

	void SetRunNumber( unsigned long runNumber )
		{
		runNumber = fRunNumber;
		}

	void Pause()
		{
		if ( fStatus )
		    fStatus->fProcessStatus.fState |= kAliHLTPCPausedStateFlag;
		fPaused = true;
		}

	void Restart()
		{
		if ( fStatus )
		    fStatus->fProcessStatus.fState &= ~kAliHLTPCPausedStateFlag;
		fPaused = false;
		}

	void SetShmCopy( bool copy, AliUInt8_t* dest = NULL );

	int Connect();
	int Connect( bool doLock );
	int Disconnect();
	int Disconnect( bool initiator, bool do_timeout = false, AliUInt32_t timeout_ms = 0, bool doLock = true );
	int Start();
	int Stop();
  
	void MsgLoop();
	bool QuitMsgLoop();

	// Warning, extremely dangerous method. This can cause event loss in your system.
	void PURGEALLEVENTS();

	void INTERRUPTBLOBCONNECTION();

	virtual void DumpEventMetaData();

    protected:

	    friend class AliHLTBridgeHeadErrorCallback<AliHLTPublisherBridgeHead>;

	bool fShmCopy;
	AliUInt8_t* fCopyDest;

	bool fQuitMsgLoop;
	bool fMsgLoopQuitted;

	bool fQuitBlobLoop;
	bool fBlobLoopQuitted;
  
	bool fPaused;

	BCLMsgCommunication* fMsgCom;
	BCLMsgCommunication* fBlobMsgCom;
	BCLBlobCommunication* fBlobCom;

	unsigned long fCommunicationTimeout;

	bool fBlobConnected;
	bool fMsgConnected;
	bool fConnectionDesired;

	BCLAbstrAddressStruct* fRemoteMsgAddress;
	BCLAbstrAddressStruct* fRemoteBlobMsgAddress;

	BCLAbstrAddressStruct* fReceiveAddr;

	MLUCDynamicAllocCache fMsgCache;

	pthread_mutex_t fMsgComMutex;
	pthread_mutex_t fComAccessMutex;
	pthread_mutex_t fComRecvMutex;

	AliHLTShmID fShmID;

	virtual void CanceledEvent( AliEventID_t event, vector<AliHLTEventDoneData*>& eventDoneData );
	// empty callback. Will be called when an
	//event has already been internally canceled. Will be 
	// called with no mutexes locked!
	virtual void AnnouncedEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, 
				     const AliHLTEventTriggerStruct& trigger, AliUInt32_t refCount );
	// empty callback. Will be called when an
	//event has been announced. Will be 
	// called with no mutexes locked!
	virtual bool GetAnnouncedEventData( AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
				      AliHLTEventTriggerStruct*& trigger );
	// Empty callback, called when an already announced event has to be announced again to a new
	// subscriber that wants to receive old events. This method needs to return the pointers
	// to the sub-event data descriptor and the event trigger structure. If the function returns
	// false this is a signal that the desired data could not be found anymore. 
	// This default implementation just returns false

	virtual void EventDoneDataReceived( const char* subscriberName, AliEventID_t eventID,
					    const AliHLTEventDoneData*, bool forceForward, bool forwarded );
	// empty callback. Will be called when an event done message with non-trivial event done data
	// (fDataWordCount>0) has been received for that event. 
	// This is called prior to the sending of the EventDoneData to interested other subscribers
	// (SendEventDoneData above)
	// called with no mutexes locked!

	bool SendEventDoneDataForward( const AliHLTEventDoneData* edd );


	struct EventData
	    {
		EventData()
			{
			fSEDD = NULL;
			fETS = NULL;
			}
		AliHLTSubEventDataDescriptor* fSEDD;
		AliHLTEventTriggerStruct* fETS;
	    };

	MLUCAllocCache fEventDataCache;

	typedef EventData* PEventData;
	MLUCVector<PEventData> fEventList;
	pthread_mutex_t fEventListMutex;

	static bool FindEventDataByEvent( const PEventData& elem, const AliEventID_t& ref );

	AliHLTDescriptorHandler* fDescriptorHandler;
	pthread_mutex_t fDescriptorMutex;

	MLUCDynamicAllocCache fETSCache;

	AliHLTTimer fTimer;
	AliHLTTimerConditionSem fRetrySignal;
	AliHLTObjectThread<AliHLTPublisherBridgeHead> fRetryThread;
	bool fQuitRetryLoop;
	bool fRetryLoopQuitted;

	void RetryLoop();

	AliHLTObjectThread<AliHLTPublisherBridgeHead> fBlobLoopThread;
	void BlobLoop();
	bool QuitBlobLoop();

	struct EventDoneData
	    {
		AliEventID_t fEventID;
		AliUInt32_t fRetryCount;
		AliHLTEventDoneData* fEDD;
		bool fSendDone;
		bool fForceForward;
		void ResetCachedObject() {};
	    };

	MLUCObjectCache<EventDoneData> fIntEDDCache; // For internal use of struct EventDoneData

	MLUCDynamicAllocCache fEDDCache; // To store AliHLTEventDoneData received for events.

	void FreeEvent( AliEventID_t eventID, AliHLTEventDoneData* eventDoneData, bool sendDone, EventDoneData* eventData = NULL );

	AliUInt32_t fMaxRetryCount;

	AliUInt32_t fRetryTime; // in milliseconds.

	BCLTransferID fGlobalTransferID;

	AliHLTBridgeHeadLAMProxy* fLAMProxy;
	AliHLTBridgeStatusData* fStatus;

// 	vector<const AliHLTSubEventDataDescriptor*> fSEDDList;
// 	pthread_mutex_t fSEDDListMutex;
// 	AliHLTSubEventDataDescriptor* fCurrentSEDD;
// 	AliHLTEventTriggerStruct* fEventTrigger;
// 	vector<BCLTransferID> fCurrentTransferIDs;

	AliHLTBridgeHeadErrorCallback<AliHLTPublisherBridgeHead>* fErrorCallback;

	class DefaultErrorCallback: public AliHLTBridgeHeadErrorCallback<AliHLTPublisherBridgeHead>
	    {
	    public:

		DefaultErrorCallback();

		virtual ~DefaultErrorCallback();

		virtual void ConnectionError( AliHLTPublisherBridgeHead* bridgeHead );
		
		virtual void ConnectionEstablished( AliHLTPublisherBridgeHead* bridgeHead );

	    protected:
		
		bool fCorrectionInProgress;
		bool fConnected;
		pthread_mutex_t fProgressMutex;

	    };

	DefaultErrorCallback fDefaultErrorCallback;

	AliHLTEventAccounting* fEventAccounting;

	bool fAllEventsPurged;

	bool fAliceHLT;
	unsigned long fRunNumber;

	int fOldBCLLog;
	bool fOldBCLLogSet;
	
    private:
    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3PUBLISHERBRIDGEHEAD_HPP_
