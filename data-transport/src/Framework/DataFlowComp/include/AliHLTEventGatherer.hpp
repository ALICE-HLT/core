/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3EVENTCOLLECTOR_HPP_
#define _ALIL3EVENTCOLLECTOR_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliEventID.h"
#include "AliHLTEventDataType.h"
#include "AliHLTEventTriggerStruct.h"
#include "AliHLTDetectorSubscriber.hpp"
#include "AliHLTBaseEventGatherer.hpp"
#include "MLUCObjectCache.hpp"
#include "MLUCVector.hpp"

class AliHLTEventGathererSubscriber;

class AliHLTEventGatherer: public AliHLTBaseEventGatherer
    {
    public:

	AliHLTEventGatherer( int slotcnt = -1 );

	virtual ~AliHLTEventGatherer();


	void AddSubscriber( AliHLTEventGathererSubscriber* subscriber );

	virtual bool WaitForEvent( AliEventID_t& eventID, EventGathererData*& eventData );
	void StopWaiting();

	virtual void EventDone( AliHLTEventDoneData* ed );


    protected:

	//MLUCAllocCache fEventDataCache;
	MLUCObjectCache<EventGathererData> fEventDataCache;

	pthread_mutex_t fSubscriberMutex;
	vector<AliHLTEventGathererSubscriber*> fSubscribers;

	bool fQuitWaiting;

	pthread_mutex_t fEventMutex;
	//vector<EventGathererData*> fEventData;
	typedef EventGathererData* PEventGathererData;
	MLUCVector<PEventGathererData> fEventData;

	virtual void NewEvent( AliHLTEventGathererSubscriber* subscriber, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* ets );
	virtual void SubscriptionCanceled( AliHLTEventGathererSubscriber* subscriber );
	virtual void SubscriberError( AliHLTEventGathererSubscriber* )
		{
		}

	AliHLTTimerConditionSem fSignal;

	AliHLTEventTriggerStruct fDummyTrigger;

	    friend class AliHLTEventGathererSubscriber;

	static bool FindEventDataByID( const PEventGathererData& elem, uint64 ref )
		{
		return elem->fEventID == (AliEventID_t)ref;
		}

    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3EVENTCOLLECTOR_HPP_
