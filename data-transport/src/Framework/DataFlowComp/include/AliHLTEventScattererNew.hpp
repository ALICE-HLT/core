#ifndef _ALIHLTEVENTSCATTERERNEW_HPP_
#define _ALIHLTEVENTSCATTERERNEW_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
#include "AliHLTTypes.h"
#include "AliEventID.h"
#include "AliHLTEventDataType.h"
#include "AliHLTEventTriggerStruct.h"
#include "AliHLTForwardingSubscriber.hpp"
#include "AliHLTForwardingPublisher.hpp"
#include "AliHLTTimer.hpp"
#include "AliHLTThread.hpp"
#include "AliHLTSubEventDescriptor.hpp"
#include "MLUCObjectCache.hpp"
#include "MLUCDynamicAllocCache.hpp"
#include "MLUCIndexedVector.hpp"
#include "MLUCVector.hpp"
#include "MLUCString.hpp"
#include "MLUCConditionSem.hpp"
#include "AliHLTStatus.hpp"

#define USE_MLUCINDEXEDVECTOR
#define USE_PUBLISHERMAP

//#define MLUCINDEXVECTOR_CROSSCHECK

class AliHLTDescriptorHandler;
class AliHLTStatus;
class AliHLTEventAccounting;

class AliHLTEventScattererNew
    {
    public:

	typedef AliHLTForwardingSubscriber<AliHLTEventScattererNew,unsigned long> TSubscriber;
	typedef AliHLTForwardingPublisher<AliHLTEventScattererNew,unsigned long> TPublisher;

	struct EventData
	    {
		unsigned long long fSeqNr;
		AliEventID_t fEventID;
		unsigned long fPublisherNdx;
		AliHLTSubEventDataDescriptor* fSEDD;
		AliHLTEventTriggerStruct* fETS;
// 		bool fRePublishInProgress;
		bool fBusy;
		bool fCanceled;
		bool fDone;

		vector<unsigned long> fBroadcastUsedPublishers;
		vector<unsigned long> fBroadcastReceivedPublishers;
		bool fBroadcastEventDoneForwardSent;
		AliHLTEventDoneData* fEDD;

		void ResetCachedObject()
			{
			fSeqNr = 0;
			fCanceled = false;
			fDone = false;
			fPublisherNdx = ~(unsigned long)0;
			fSEDD = NULL;
			fETS = NULL;
			fBusy = false;
			fEDD = NULL;
			fBroadcastEventDoneForwardSent = false;
			fEventID = AliEventID_t();
			fBroadcastUsedPublishers.clear();
			fBroadcastReceivedPublishers.clear();
			}
		EventData()
			{
			ResetCachedObject();
			}
	    };
	
	
	AliHLTEventScattererNew( char const* name, int slotCount = -1 );
	
	virtual ~AliHLTEventScattererNew();

 	void SetDescriptorHandler( AliHLTDescriptorHandler* descHand )
 		{
 		fDescriptorHandler = descHand;
 		}

	void SetShmManager( AliHLTShmManager* shmMan )
		{
		fShmManager = shmMan;
		}

	void SetStatusData( AliHLTStatus* status )
		{
		fStatus = status;
		}

	void SetEventAccounting( AliHLTEventAccounting* eventAccounting )
		{
		fEventAccounting = eventAccounting;
		}

	void AliceHLT()
		{
		fAliceHLT = true;
		}

	void SetEventModuloPreDivisor( unsigned long preDivisor )
		{
		fEventData.SetPreDivisor( preDivisor );
		}

	void SetRunNumber( unsigned long runNumber )
		{
		fRunNumber = runNumber;
		}

	virtual void SetScattererParameter( unsigned long scattererParam ) = 0;

 	void AddPublisher( TPublisher* subscriber, unsigned long ndx );
	void DelPublisher( unsigned long ndx );
	void EnablePublisher( unsigned long ndx );
	void DisablePublisher( unsigned long ndx );

	void SetSubscriber( TSubscriber* pub );
	void SetOrigPublisher( AliHLTPublisherInterface* pub );

	void StartProcessing();
	void StopProcessing();
	void PauseProcessing();
	void ResumeProcessing();


	// This method is called whenever a new wanted 
	// event is available.
	virtual int NewEvent( TSubscriber* sub, unsigned long ndx, 
			      AliHLTPublisherInterface& publisher, 
			      AliEventID_t eventID, 
			      const AliHLTSubEventDataDescriptor& sbevent,
			      const AliHLTEventTriggerStruct& ets );
	
	// This method is called for a transient subscriber, 
	// when an event has been removed from the list. 
	virtual int EventCanceled( TSubscriber* sub, unsigned long ndx, 
				   AliHLTPublisherInterface& publisher, 
				   AliEventID_t eventID );
	
	// This method is called for a subscriber when new event done data is 
	// received by a publisher (sent from another subscriber) for an event. 
	virtual int EventDoneData( TSubscriber* sub, unsigned long ndx, 
				   AliHLTPublisherInterface& publisher, 
				   const AliHLTEventDoneData& 
				   eventDoneData );

	// This method is called when the subscription ends.
	virtual int SubscriptionCanceled( TSubscriber* sub, unsigned long ndx, 
					  AliHLTPublisherInterface& publisher );
	
	// This method is called when the publishing 
	// service runs out of buffer space. The subscriber 
	// has to free as many events as possible and send an 
	// EventDone message with their IDs to the publisher
	virtual int ReleaseEventsRequest( TSubscriber* sub, unsigned long ndx, 
					  AliHLTPublisherInterface& publisher ); 

	// Method used to check alive status of subscriber. 
	// This message has to be answered with 
	// PingAck message within less  than a second.
	virtual int Ping( TSubscriber* sub, unsigned long ndx, 
			  AliHLTPublisherInterface& publisher );
	
	// Method used by a subscriber in response to a Ping 
	// received message. The PingAck message has to be 
	// called within less than one second.
	virtual int PingAck( TSubscriber* sub, unsigned long ndx, 
			     AliHLTPublisherInterface& publisher );


	// empty callback. Will be called when an
	//event has already been internally canceled. 
	virtual void CanceledEvent( TPublisher* pub, unsigned long ndx,
				    AliEventID_t event, vector<AliHLTEventDoneData*>& eventDoneData );

	// empty callback. Will be called when an
	//event has been announced. 
	virtual void AnnouncedEvent( TPublisher* pub, unsigned long ndx,
				     AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, 
				     const AliHLTEventTriggerStruct& trigger, AliUInt32_t refCount );

	// empty callback. Will be called when an event done message with non-trivial event done data
	// (fDataWordCount>0) has been received for that event. 
	// This is called prior to the sending of the EventDoneData to interested other subscribers
	// (SendEventDoneData above)
	virtual void EventDoneDataReceived( TPublisher* pub, unsigned long ndx,
					    const char* subscriberName, AliEventID_t eventID,
					    const AliHLTEventDoneData* edd, bool forceForward, bool forwarded );

	// Empty callback, called when an already announced event has to be announced again to a new
	// subscriber that wants to receive old events. This method needs to return the pointers
	// to the sub-event data descriptor and the event trigger structure. If the function returns
	// false this is a signal that the desired data could not be found anymore. 
	virtual bool GetAnnouncedEventData( TPublisher* pub, unsigned long ndx,
					    AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
					    AliHLTEventTriggerStruct*& trigger );

	virtual void PURGEALLEVENTS();

	virtual void DumpEventMetaData();

	void DistributionEventIDBased()
		{
		fEventIDBased = true;
		}
	void DistributionSeqNrBased()
		{
		fEventIDBased = false;
		}

	void NoStrictSORRequirements()
		{
		fStrictSORRequirements = false;
		}
	void StrictSORRequirements()
		{
		fStrictSORRequirements = true;
		}

    protected:

	struct PublisherData
	    {
		TPublisher* fPublisher;
		unsigned long fNdx;
		bool fEnabled;
	    };

	virtual void DoProcessing();

	virtual void PublisherAdded( PublisherData* ) // Empty callback
		{
		}
	virtual void PublisherDeleted( PublisherData*, bool& ) // Empty callback
		{
		}
	virtual void PublisherEnabled( PublisherData* ) // Empty callback
		{
		}
	virtual void PublisherDisabled( PublisherData*, bool& ) // Empty callback
		{
		}

	virtual bool GetEventPublisher( EventData* ed, unsigned long& pubNdx ) = 0; 

	void ResendEvents( unsigned long pubNdx );

	TPublisher* GetPublisher( unsigned long pubNdx );

	void AnnounceEvent( AliEventID_t eventID, AliHLTSubEventDataDescriptor* sedd, AliHLTEventTriggerStruct* ets, vector<unsigned long>& publishers );
	void AnnounceEvent( unsigned long pubNdx, AliEventID_t eventID, AliHLTSubEventDataDescriptor* sedd, AliHLTEventTriggerStruct* ets );
	void CancelEvent( unsigned long pubNdx, AliEventID_t eventID );
	void CancelAllEvents( unsigned long pubNdx );
	
	void ReleaseEvent( unsigned long ndx );

	MLUCString fName;

	pthread_mutex_t fPublisherMutex;
	vector<PublisherData> fPublishers;
	unsigned long fActivePublisherCount;
#ifndef USE_PUBLISHERMAP
#else
        vector<PublisherData> fPublisherMap;
#endif

	TSubscriber* fSubscriber;
	AliHLTPublisherInterface* fOrigPublisher;

	pthread_mutex_t fEventMutex;
	//vector<EventData*> fEventData;
	typedef EventData* PEventData;
#ifndef USE_MLUCINDEXEDVECTOR
	MLUCVector<PEventData> fEventData;
#else
	MLUCIndexedVector<PEventData,AliEventID_t> fEventData;
#endif
#ifdef MLUCINDEXVECTOR_CROSSCHECK
	std::vector<AliEventID_t> fEventIDs;
#endif
	MLUCObjectCache<EventData> fEventDataCache;
	
	bool fQuitProcessing;
	bool fProcessingQuitted;

	bool fPaused;

	AliHLTDescriptorHandler* fDescriptorHandler;
	pthread_mutex_t fDescriptorHandlerMutex;

	AliHLTShmManager* fShmManager;

	MLUCDynamicAllocCache fETSCache;
	pthread_mutex_t fETSCacheMutex;

	MLUCDynamicAllocCache fEDDCache;
	pthread_mutex_t fEDDCacheMutex;

	AliHLTEventDoneData fStaticEDD;
	pthread_mutex_t fStaticEDDMutex;

	MLUCConditionSem<PEventData> fSignal;

	AliHLTObjectThread<AliHLTEventScattererNew> fProcessingThread;

	static bool FindEventDataByID( const PEventData& elem, const AliEventID_t& refID )
		{
		return elem->fEventID == refID;
		}

	static bool FindEventDataForPublisher( const PEventData& elem, const unsigned long& ref )
		{
		return elem->fPublisherNdx == ref;
		}

	AliHLTStatus* fStatus;

#ifdef MLUCINDEXVECTOR_CROSSCHECK
	void DoCrossCheck( const char* functionName, const char* file, unsigned long line );
#endif

	AliHLTEventAccounting* fEventAccounting;

	bool fEventsPurged;

	unsigned long fRunNumber;

	bool fAliceHLT;

	unsigned long long fSeqNr;

	bool fEventIDBased;

	bool fStrictSORRequirements;

    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTEVENTSCATTERERNEW_HPP_
