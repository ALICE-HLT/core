/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3EVENTMERGER_HPP_
#define _ALIL3EVENTMERGER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliEventID.h"
#include "AliHLTEventDataType.h"
#include "AliHLTEventTriggerStruct.h"
#include "AliHLTDetectorSubscriber.hpp"
#include "AliHLTTimer.hpp"
#include "AliHLTThread.hpp"
#include "MLUCObjectCache.hpp"
#include "MLUCVector.hpp"


class AliHLTEventMergerSubscriber;

class AliHLTEventMerger
    {
    public:

	struct EventData
	    {
		AliEventID_t fEventID;
		AliUInt32_t fSubscribersExpected;
		AliUInt32_t fSubscribersPresent;
		AliUInt32_t fTimerID;
		vector<AliHLTDetectorSubscriber::BlockData> fDataBlocks;
		AliHLTEventTriggerStruct* fETS; // Warning: This is currently taken from the first arriving subevent for each event...
		void ResetCachedObject()
			{
			fDataBlocks.clear();
			}
	    };

	AliHLTEventMerger( int slotCount = -1 );

	virtual ~AliHLTEventMerger();


	void AddSubscriber( AliHLTEventMergerSubscriber* subscriber );

	virtual bool WaitForEvent( AliEventID_t& eventID, EventData*& eventData );
	void StopWaiting();

	virtual void EventDone( AliHLTEventDoneData* edd );

	void SetEventTimeout( AliUInt32_t timeout )
		{
		fEventTimeout = timeout;
		}

    protected:

	MLUCObjectCache<EventData> fEventDataCache;

	pthread_mutex_t fSubscriberMutex;
	vector<AliHLTEventMergerSubscriber*> fSubscribers;
	AliUInt32_t fSubscriberCount;

	bool fQuitWaiting;

	pthread_mutex_t fEventMutex;
	//vector<EventData*> fEventData;
	typedef EventData* PEventData;
	MLUCVector<PEventData> fEventData;

	virtual void NewEvent( AliHLTEventMergerSubscriber* subscriber, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* ets );
	virtual void SubscriptionCanceled( AliHLTEventMergerSubscriber* subscriber );

	AliHLTTimerConditionSem fSignal;

	AliHLTEventTriggerStruct fDummyTrigger;

	    friend class AliHLTEventMergerSubscriber;

	AliHLTTimer fTimer;
	AliHLTTimerConditionSem fTimeoutSignal;

	class EventTimeoutThread: public AliHLTThread
	    {
	    public:
		
		EventTimeoutThread( AliHLTEventMerger& pub ):
		    fPub( pub )
			{
			}

	    protected:
		
		virtual void Run()
			{
			fPub.DoTimeout();
			}

		AliHLTEventMerger& fPub;

	    };

	    friend class EventTimeoutThread;

	virtual void DoTimeout();

	bool fQuitTimeout;
	bool fTimeoutQuitted;

	AliUInt32_t fEventTimeout;

	static bool FindEventDataByID( const PEventData& elem, uint64 ref )
		{
		return elem->fEventID == (AliEventID_t)ref;
		}

    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3EVENTMERGER_HPP_
