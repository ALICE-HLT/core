/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3EVENTCOLLECTORSUBSCRIBER_HPP_
#define _ALIL3EVENTCOLLECTORSUBSCRIBER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTDetectorSubscriber.hpp"

class AliHLTBaseEventGatherer;

class AliHLTEventGathererSubscriber: public AliHLTDetectorSubscriber
    {
    public:
	
	AliHLTEventGathererSubscriber( const char* name, AliUInt32_t maxShmNoUseCount, int maxPreSubEventsExp2 = -1 );

	~AliHLTEventGathererSubscriber();

// 	void EventDone( AliHLTEventID_t eventID );

	void SetEventGatherer( AliHLTBaseEventGatherer* merger );

	virtual int SubscriptionCanceled( AliHLTPublisherInterface& publisher );

	
    protected:

	virtual void ProcessEvent( const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* ets );

	AliHLTBaseEventGatherer* fGatherer;

    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3EVENTCOLLECTORSUBSCRIBER_HPP_
