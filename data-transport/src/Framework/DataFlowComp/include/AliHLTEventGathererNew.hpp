#ifndef _AlIHLTEVENTGATHERERNEW_HPP_
#define _AlIHLTEVENTGATHERERNEW_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliEventID.h"
#include "AliHLTEventDataType.h"
#include "AliHLTEventTriggerStruct.h"
#include "AliHLTForwardingSubscriber.hpp"
#include "AliHLTForwardingPublisher.hpp"
#include "AliHLTTimer.hpp"
#include "AliHLTThread.hpp"
#include "AliHLTSubEventDescriptor.hpp"
#include "MLUCObjectCache.hpp"
#include "MLUCDynamicAllocCache.hpp"
#include "MLUCVector.hpp"
#include "MLUCIndexedVector.hpp"
#include "AliHLTStatus.hpp"


class AliHLTDescriptorHandler;
class AliHLTStatus;
class AliHLTEventAccounting;

class AliHLTEventGathererNew
    {
    public:

	typedef AliHLTForwardingSubscriber<AliHLTEventGathererNew,unsigned long> TSubscriber;
	typedef AliHLTForwardingPublisher<AliHLTEventGathererNew,unsigned long> TPublisher;

	struct EventData
	    {
		AliEventID_t fEventID;
		unsigned long fSubscriberNdx;
		AliHLTSubEventDataDescriptor* fSEDD;
		AliHLTEventTriggerStruct* fETS;
// 		bool fRePublishInProgress;
		bool fBusy;
		bool fCanceled;
		bool fDone;
		bool fSubscriberDisabled;
		vector<unsigned long> fBroadcastExpectedSubscribers;
		vector<unsigned long> fBroadcastReceivedSubscribers;
		vector<AliHLTSubEventDataBlockDescriptor> fBroadcastDataBlocks;
		AliUInt64_t fBroadcastStatusFlags;

		void ResetCachedObject()
			{
			fCanceled = false;
			fDone = false;
			fSubscriberDisabled = false;
			fSubscriberNdx = ~(unsigned long)0;
			fBroadcastExpectedSubscribers.clear();
			fBroadcastReceivedSubscribers.clear();
			fBroadcastDataBlocks.clear();
			fBroadcastStatusFlags = 0;
			}
		EventData()
			{
			ResetCachedObject();
			}
	    };
	
	AliHLTEventGathererNew( char const* name, int slotCountExp2 = -1 );
	virtual ~AliHLTEventGathererNew();

 	void SetDescriptorHandler( AliHLTDescriptorHandler* descHand )
 		{
 		fDescriptorHandler = descHand;
 		}

	void SetStatusData( AliHLTStatus* status )
		{
		fStatus = status;
		}

	void SetEventAccounting( AliHLTEventAccounting* eventAccounting )
		{
		fEventAccounting = eventAccounting;
		}

	void SetBroadcastTimeout( unsigned long time_ms )
		{
		fBroadcastTimeout_ms = time_ms;
		}
	void SetSORTimeout( unsigned long time_ms )
		{
		fSORTimeout_ms = time_ms;
		}

	void AliceHLT()
		{
		fAliceHLT = true;
		}

	void SetEventModuloPreDivisor( unsigned long preDivisor )
		{
		fEventData.SetPreDivisor( preDivisor );
		fEDDBacklog.SetPreDivisor( preDivisor );
		fDisabledSubscriberEventData.SetPreDivisor( preDivisor );
		fDisabledSubscriberEDDs.SetPreDivisor( preDivisor );
		}

	void SetRunNumber( unsigned long runNumber )
		{
		fRunNumber = runNumber;
		}

	void NoStrictSORRequirements()
		{
		fStrictSORRequirements = false;
		}
	void StrictSORRequirements()
		{
		fStrictSORRequirements = true;
		}

 	void AddSubscriber( TSubscriber* subscriber, AliHLTPublisherInterface* proxy, unsigned long ndx );
	void DelSubscriber( unsigned long ndx );
	void EnableSubscriber( unsigned long ndx );
	void DisableSubscriber( unsigned long ndx );

	void SetPublisher( TPublisher* pub );


	void StartProcessing();
	void StopProcessing();
	void PauseProcessing();
	void ResumeProcessing();

	// This method is called whenever a new wanted 
	// event is available.
	virtual int NewEvent( TSubscriber* sub, unsigned long ndx, 
			      AliHLTPublisherInterface& publisher, 
			      AliEventID_t eventID, 
			      const AliHLTSubEventDataDescriptor& sbevent,
			      const AliHLTEventTriggerStruct& ets );
	
	// This method is called for a transient subscriber, 
	// when an event has been removed from the list. 
	virtual int EventCanceled( TSubscriber* sub, unsigned long ndx, 
				   AliHLTPublisherInterface& publisher, 
				   AliEventID_t eventID );
	
	// This method is called for a subscriber when new event done data is 
	// received by a publisher (sent from another subscriber) for an event. 
	virtual int EventDoneData( TSubscriber* sub, unsigned long ndx, 
				   AliHLTPublisherInterface& publisher, 
				   const AliHLTEventDoneData& 
				   eventDoneData );

	// This method is called when the subscription ends.
	virtual int SubscriptionCanceled( TSubscriber* sub, unsigned long ndx, 
					  AliHLTPublisherInterface& publisher );
	
	// This method is called when the publishing 
	// service runs out of buffer space. The subscriber 
	// has to free as many events as possible and send an 
	// EventDone message with their IDs to the publisher
	virtual int ReleaseEventsRequest( TSubscriber* sub, unsigned long ndx, 
					  AliHLTPublisherInterface& publisher ); 

	// Method used to check alive status of subscriber. 
	// This message has to be answered with 
	// PingAck message within less  than a second.
	virtual int Ping( TSubscriber* sub, unsigned long ndx, 
			  AliHLTPublisherInterface& publisher );
	
	// Method used by a subscriber in response to a Ping 
	// received message. The PingAck message has to be 
	// called within less than one second.
	virtual int PingAck( TSubscriber* sub, unsigned long ndx, 
			     AliHLTPublisherInterface& publisher );


	// empty callback. Will be called when an
	//event has already been internally canceled. 
	virtual void CanceledEvent( TPublisher* pub, unsigned long ndx,
				    AliEventID_t event, vector<AliHLTEventDoneData*>& eventDoneData );

	// empty callback. Will be called when an
	//event has been announced. 
	virtual void AnnouncedEvent( TPublisher* pub, unsigned long ndx,
				     AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, 
				     const AliHLTEventTriggerStruct& trigger, AliUInt32_t refCount );

	// empty callback. Will be called when an event done message with non-trivial event done data
	// (fDataWordCount>0) has been received for that event. 
	// This is called prior to the sending of the EventDoneData to interested other subscribers
	// (SendEventDoneData above)
	virtual void EventDoneDataReceived( TPublisher* pub, unsigned long ndx,
					    const char* subscriberName, AliEventID_t eventID,
					    const AliHLTEventDoneData* edd, bool forceForward, bool forwarded );

	// Empty callback, called when an already announced event has to be announced again to a new
	// subscriber that wants to receive old events. This method needs to return the pointers
	// to the sub-event data descriptor and the event trigger structure. If the function returns
	// false this is a signal that the desired data could not be found anymore. 
	virtual bool GetAnnouncedEventData( TPublisher* pub, unsigned long ndx,
					    AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
					    AliHLTEventTriggerStruct*& trigger );


	virtual void PURGEALLEVENTS();

	virtual void DumpEventMetaData();

    protected:

	typedef EventData* PEventData;

	struct SubscriberData
	    {
		TSubscriber* fSubscriber;
		AliHLTPublisherInterface* fProxy;
		unsigned long fNdx;
		bool fEnabled;
		bool fReceivedSOR;
	    };

	struct EventBacklogData
	    {
		AliEventID_t fEventID;
		unsigned long fSubscriberNdx;
		AliHLTEventDoneData* fEDD;
		bool fSubscriberDisabled;
	    };

	virtual void DoProcessing();

	void ReleaseEvent( MLUCIndexedVector<PEventData,AliEventID_t>& edv, unsigned long ndx );

	int MakeBroadcastEventSEDD( EventData* ed ); // Must be called with fEventMutex locked!! Not allowed to unlock/re-lock internally

	MLUCString fName;

	pthread_mutex_t fSubscriberMutex;
	vector<SubscriberData> fSubscribers;

	TPublisher* fPublisher;

	pthread_mutex_t fEventMutex; // NOTE: If this mutex is locked simultaneously with fETSCacheMutex, then fEventMutex MUST BE LOCKED FIRST
	//vector<EventData*> fEventData;
	MLUCIndexedVector<PEventData,AliEventID_t> fEventData;
	MLUCIndexedVector<EventBacklogData,AliEventID_t> fEDDBacklog;
	MLUCIndexedVector<PEventData,AliEventID_t> fDisabledSubscriberEventData;
	MLUCIndexedVector<EventBacklogData,AliEventID_t> fDisabledSubscriberEDDs;

	MLUCObjectCache<EventData> fEventDataCache;
	
	bool fQuitProcessing;
	bool fProcessingQuitted;

	bool fPaused;

	AliHLTDescriptorHandler* fDescriptorHandler;
	pthread_mutex_t fDescriptorHandlerMutex;

	MLUCDynamicAllocCache fETSCache;
	pthread_mutex_t fETSCacheMutex; // NOTE: If this mutex is locked simultaneously with fEventMutex, then fEventMutex MUST BE LOCKED FIRST
	
	MLUCDynamicAllocCache fEDDCache;
	pthread_mutex_t fEDDCacheMutex;

	AliHLTEventDoneData fStaticEDD;
	pthread_mutex_t fStaticEDDMutex;

	MLUCConditionSem<PEventData> fSignal;

	AliHLTObjectThread<AliHLTEventGathererNew> fProcessingThread;

	static bool FindEventDataByID( const PEventData& elem, const AliEventID_t& ref )
		{
		return elem->fEventID == ref;
		}

	static bool FindEventDataBySubscriber( const PEventData& elem, const unsigned long& ref )
		{
		if ( elem->fSubscriberNdx == ref )
		    return true;
		vector<unsigned long>::iterator iter, end;
		iter = elem->fBroadcastExpectedSubscribers.begin();
		end = elem->fBroadcastExpectedSubscribers.end();
		while ( iter != end )
		    {
		    if ( *iter == ref )
			return true;
		    iter++;
		    }
		return false;
		}

	static bool FindActiveEventDataBySubscriber( const PEventData& elem, const unsigned long& ref )
		{
		if ( elem->fCanceled )
		    return false;
		if ( elem->fSubscriberNdx == ref )
		    return true;
		if ( !(elem->fSEDD->fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_BROADCAST) )
		    return false;
		vector<unsigned long>::iterator iter, end;
		iter = elem->fBroadcastExpectedSubscribers.begin();
		end = elem->fBroadcastExpectedSubscribers.end();
		while ( iter != end )
		    {
		    if ( *iter == ref )
			return true;
		    iter++;
		    }
		return false;
		}

	static bool FindEDDBacklogByID( const EventBacklogData& elem, const AliEventID_t& ref )
		{
		return elem.fEventID == ref;
		}

	static bool FindEDDBacklogBySubscriber( const EventBacklogData& elem, const unsigned long& ref )
		{
		return elem.fSubscriberNdx == ref;
		}

	AliHLTStatus* fStatus;

	AliHLTEventAccounting* fEventAccounting;

	bool fSORSent;
	MLUCVector <PEventData> fSORSentDeferBuffer;

	bool fAliceHLT;



	AliHLTTimer fTimer;

	struct TimeoutData
	    {
		AliEventID_t fEventID;
		void ResetCachedObject() {}
	    };
	MLUCObjectCache<TimeoutData> fTimeoutDataCache;
	
	class TimerCallback: public AliHLTTimerCallback
	    {
	    public:
		TimerCallback( AliHLTEventGathererNew* parent ):
		    fParent( parent )
			{
			}
		virtual void TimerExpired( AliUInt64_t notData, unsigned long /*priority*/ )
			{
			fParent->TimerExpired( reinterpret_cast<TimeoutData*>( notData ) );
			}
	    protected:
		AliHLTEventGathererNew* fParent;
	    };
	TimerCallback fTimerCallback;

	void TimerExpired( TimeoutData* notData );

	unsigned long fBroadcastTimeout_ms;
	unsigned long fSORTimeout_ms;

	bool fEventsPurged;

	unsigned long fRunNumber;

	bool fStrictSORRequirements;

    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _AlIHLTEVENTGATHERERNEW_HPP_
