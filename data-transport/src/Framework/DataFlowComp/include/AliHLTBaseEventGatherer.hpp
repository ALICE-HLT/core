/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3BASEEVENTCOLLECTOR_HPP_
#define _ALIL3BASEEVENTCOLLECTOR_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliEventID.h"
#include "AliHLTEventDataType.h"
#include "AliHLTEventTriggerStruct.h"
#include "AliHLTDetectorSubscriber.hpp"
#include "MLUCObjectCache.hpp"
#include "MLUCVector.hpp"

class AliHLTEventGathererSubscriber;

class AliHLTBaseEventGatherer
    {
    public:

	struct EventGathererData
	    {
		AliEventID_t fEventID;
		vector<AliHLTDetectorSubscriber::BlockData> fDataBlocks;
		AliHLTEventGathererSubscriber* fSubscriber;
		AliHLTEventTriggerStruct* fETS;
		unsigned long fSubscriberNdx;
		bool fDone;
		AliHLTEventDoneData* fEDD;
		void ResetCachedObject()
			{
			fDataBlocks.clear();
			}
	    };

	virtual bool WaitForEvent( AliEventID_t& eventID, EventGathererData*& eventData ) = 0;

	virtual void EventDone( AliHLTEventDoneData* ed ) = 0;


    protected:

	virtual void NewEvent( AliHLTEventGathererSubscriber* subscriber, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* ets ) = 0;
	virtual void SubscriptionCanceled( AliHLTEventGathererSubscriber* subscriber ) = 0;
	virtual void SubscriberError( AliHLTEventGathererSubscriber* subscriber ) = 0;

	    friend class AliHLTEventGathererSubscriber;

    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3BASEEVENTCOLLECTOR_HPP_
