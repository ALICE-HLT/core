/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3TOLERANTROUNDROBINEVENTDISPATCHER_HPP_
#define _ALIL3TOLERANTROUNDROBINEVENTDISPATCHER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliEventID.h"
#include "AliHLTSamplePublisher.hpp"
#include "AliHLTEventScatterer.hpp"
#include "AliHLTSCInterface.hpp"
#include "AliHLTSCCommandProcessor.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTToleranceStatus.hpp"
#include "MLUCToleranceHandler.hpp"
#include <pthread.h>
#include <vector>

#if __GNUC__>=3
using namespace std;
#endif


class AliHLTTolerantRoundRobinEventScattererLAMProxy
    {
    public:

	virtual void LookAtMe() = 0;
	
    };

class AliHLTTolerantRoundRobinEventScatterer: public AliHLTEventScatterer
    {
    public:

	AliHLTTolerantRoundRobinEventScatterer();
	virtual ~AliHLTTolerantRoundRobinEventScatterer();

	//virtual void AddPublisher( AliHLTEventScattererRePublisher* publisher );

	virtual int AnnounceEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, const AliHLTEventTriggerStruct& trigger );

	//virtual int EventDone( AliEventID_t eventID );
	virtual int EventCanceled( AliEventID_t eventID );
	virtual int RequestEventRelease();
	virtual int CancelAllSubscriptions();
	virtual int PublisherDown( AliHLTEventScattererRePublisher* publisher );
	virtual int PublisherSendError( AliHLTEventScattererRePublisher* publisher, AliEventID_t eventID );

	void SetupCommandProcessor( AliHLTSCInterface* interface );

	void SetDescriptorHandler( AliHLTDescriptorHandler* descrHandler )
		{
		fDescriptorHandler = descrHandler;
		}

	void SetStatus( AliHLTToleranceStatus* status )
		{
		fStatus = status;
		}

	void SetLAMProxy( AliHLTTolerantRoundRobinEventScattererLAMProxy* proxy )
		{
		fLAMProxy = proxy;
		}

    protected:

	virtual void PublisherAdded( AliHLTEventScattererRePublisher* publisher );
	virtual void ReleasingEvent( AliEventID_t eventID, AliHLTEventScattererRePublisher* publisher );

	virtual void SetPublisher( AliHLTEventScattererRePublisher* publisher, bool up, AliEventID_t lastEventID );

// 	virtual bool GetPublisherIndex( AliEventID_t eventID, unsigned& baseNdx, unsigned& backupNdx );

	void ProcessCmd( AliHLTSCCommandStruct* cmd );

	MLUCToleranceHandler fToleranceHandler;
	//pthread_mutex_t fToleranceMutex;
// 	vector<bool> fPublishersOk;
// 	vector<unsigned> fPublisherMaps;

	//unsigned fTotalPublisherCnt;
// 	unsigned fUpPublisherCnt;

	struct EventData
	    {
		AliEventID_t fEventID;
		struct timeval fTime;
		AliHLTSubEventDataDescriptor* fSEDD;
		AliHLTEventTriggerStruct* fETS;
	    };

	struct PublisherData
	    {
		MLUCVector<EventData> fEvents;
	    };

	vector<PublisherData> fEvents;

	class CommandProcessor: public AliHLTSCCommandProcessor
	    {
	    public:

		CommandProcessor( AliHLTTolerantRoundRobinEventScatterer& scheduler ):
		    fScheduler( scheduler )
			{
			}

		virtual void ProcessCmd( AliHLTSCCommandStruct* cmd )
			{
			fScheduler.ProcessCmd( cmd );
			}

	    protected:

		AliHLTTolerantRoundRobinEventScatterer& fScheduler;

	    };

	    friend class CommandProcessor;

	CommandProcessor fCommandProcessor;

	AliHLTToleranceStatus* fStatus;

	AliHLTTolerantRoundRobinEventScattererLAMProxy* fLAMProxy;

	pthread_mutex_t fDescriptorMutex;
	AliHLTDescriptorHandler* fDescriptorHandler;
	AliHLTSubEventDataDescriptor* fTmpSEDD;
	AliHLTEventTriggerStruct* fTmpETS;


	MLUCVector<EventData> fRetryEvents;

	static bool EventSearchFunc( const EventData& elem, uint64 reference );
	static bool TimeLaterSearchFunc( const EventData& elem, uint64 reference );

    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3TOLERANTROUNDROBINEVENTDISPATCHER_HPP_
