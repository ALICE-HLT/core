/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3ROUNDROBINEVENTDISPATCHER_HPP_
#define _ALIL3ROUNDROBINEVENTDISPATCHER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliEventID.h"
#include "AliHLTSamplePublisher.hpp"
#include "AliHLTEventScatterer.hpp"
#include <pthread.h>
#include <vector>

#if __GNUC__>=3
using namespace std;
#endif


class AliHLTRoundRobinEventScatterer: public AliHLTEventScatterer
    {
    public:

	AliHLTRoundRobinEventScatterer();
	virtual ~AliHLTRoundRobinEventScatterer();

	virtual int AnnounceEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, const AliHLTEventTriggerStruct& trigger );
	virtual int EventCanceled( AliEventID_t eventID );
	virtual int RequestEventRelease();
	virtual int CancelAllSubscriptions();

	virtual int PublisherDown( AliHLTEventScattererRePublisher* )
		{
		return 0;
		}

	virtual int PublisherSendError( AliHLTEventScattererRePublisher*, AliEventID_t )
		{
		return 0;
		}

    protected:

	AliUInt32_t fEventCount;

    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3ROUNDROBINEVENTDISPATCHER_HPP_
