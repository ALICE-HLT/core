/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3EVENTMERGERSUBSCRIBER_HPP_
#define _ALIL3EVENTMERGERSUBSCRIBER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTDetectorSubscriber.hpp"

class AliHLTEventMerger;

class AliHLTEventMergerSubscriber: public AliHLTDetectorSubscriber
    {
    public:
	
	AliHLTEventMergerSubscriber( const char* name, AliUInt32_t maxShmNoUseCount, int maxPreSubEventsExp2 = -1 );

	~AliHLTEventMergerSubscriber();

// 	void EventDone( AliHLTEventID_t eventID );

	void SetEventMerger( AliHLTEventMerger* merger );

	virtual int SubscriptionCanceled( AliHLTPublisherInterface& publisher );

	
    protected:

	virtual void ProcessEvent( const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* ets );

	AliHLTEventMerger* fMerger;

    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3EVENTMERGERSUBSCRIBER_HPP_
