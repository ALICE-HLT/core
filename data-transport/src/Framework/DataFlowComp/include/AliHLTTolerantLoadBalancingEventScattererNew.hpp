#ifndef _ALIHLTTOLERANTLOADBALANCINGEVENTSCATTERERNEW_HPP_
#define _ALIHLTTOLERANTLOADBALANCINGEVENTSCATTERERNEW_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTEventScattererNew.hpp"
#include "MLUCToleranceHandler.hpp"

class AliHLTTolerantLoadBalancingEventScattererNew: public AliHLTEventScattererNew
    {
    public:

	AliHLTTolerantLoadBalancingEventScattererNew( char const* name, int slotCount = -1 );
	
	virtual ~AliHLTTolerantLoadBalancingEventScattererNew();

	void SetScattererParameter( unsigned long scattererParam )
		{
		}


	

    protected:

	virtual void PublisherAdded( PublisherData* pub );
	virtual void PublisherDeleted( PublisherData* pub, bool& republishEvents );
	virtual void PublisherEnabled( PublisherData* pub );
	virtual void PublisherDisabled( PublisherData* pub, bool& republishEvents );

	virtual bool GetEventPublisher( EventData* ed, unsigned long& pubNdx ); 


	MLUCToleranceHandler fToleranceHandler;
	vector<unsigned long> fPublisherNdxMap;
	pthread_mutex_t fMutex;

    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTTOLERANTLOADBALANCINGEVENTSCATTERERNEW_HPP_
