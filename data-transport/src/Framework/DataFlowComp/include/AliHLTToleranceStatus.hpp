#ifndef _ALIL3TOLERANCESTATUS_HPP_
#define _ALIL3TOLERANCESTATUS_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCStatusHeader.hpp"
#include "AliHLTSCProcessStatus.hpp"
#include "AliHLTSCToleranceChannelStatus.hpp"

#define ALIL3TOLERANCESTATUSTYPE (((AliUInt64_t)'L3TC')<<32 | 'STAT')

struct AliHLTToleranceStatus
    {
	AliHLTSCStatusHeaderStruct                  fHeader;
	AliHLTSCProcessStatusStruct                 fProcessStatus;
	AliHLTSCToleranceChannelStatusStruct        fChannelStatus;

	AliHLTToleranceStatus()
		{
		AliHLTSCStatusHeader header;
		AliHLTSCProcessStatus process;
		AliHLTSCToleranceChannelStatus tc;
		fHeader = *(header.GetData());
		fHeader.fStatusType = ALIL3TOLERANCESTATUSTYPE;
		fProcessStatus = *(process.GetData());
		fChannelStatus = *(tc.GetData());
		fHeader.fLength += fProcessStatus.fLength + fChannelStatus.fLength;
		fHeader.fStatusStructCnt = 2;
		}

    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3TOLERANCESTATUS_HPP_
