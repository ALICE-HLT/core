/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3EVENTCOLLECTOR_HPP_
#define _ALIL3EVENTCOLLECTOR_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliEventID.h"
#include "AliHLTEventDataType.h"
#include "AliHLTEventTriggerStruct.h"
#include "AliHLTDetectorSubscriber.hpp"
#include "AliHLTSCInterface.hpp"
#include "AliHLTSCCommandProcessor.hpp"
#include "AliHLTBaseEventGatherer.hpp"
#include "AliHLTToleranceStatus.hpp"
#include "MLUCObjectCache.hpp"
#include "MLUCVector.hpp"
#include "MLUCToleranceHandler.hpp"
#include "MLUCConditionSem.hpp"

class AliHLTTolerantEventGathererLAMProxy
    {
    public:

	virtual void LookAtMe() = 0;
	
    };


class AliHLTEventGathererSubscriber;

class AliHLTTolerantEventGatherer: public AliHLTBaseEventGatherer
    {
    public:

	AliHLTTolerantEventGatherer( int slotcnt = -1,int eventDoneBackLogSlotCnt = -1 );

	virtual ~AliHLTTolerantEventGatherer();

	void AddSubscriber( AliHLTEventGathererSubscriber* subscriber );

	virtual bool WaitForEvent( AliEventID_t& eventID, EventGathererData*& eventData );
	void StopWaiting();

	void SetupCommandProcessor( AliHLTSCInterface* interface );

	virtual void EventDone( AliHLTEventDoneData* edd );

	void SetSubscriber( unsigned long ndx, bool up );

	void SetStatus( AliHLTToleranceStatus* status )
		{
		fStatus = status;
		}

	void SetLAMProxy( AliHLTTolerantEventGathererLAMProxy* proxy )
		{
		fLAMProxy = proxy;
		}

    protected:

	unsigned long GetSubscriberNdx( AliHLTEventGathererSubscriber* subscriber );

	//MLUCAllocCache fEventDataCache;
	MLUCObjectCache<EventGathererData> fEventDataCache;

	pthread_mutex_t fSubscriberMutex;
	vector<AliHLTEventGathererSubscriber*> fSubscribers;

	MLUCToleranceHandler fToleranceHandler;

	struct EventDoneBackLogData
	    {
		AliEventID_t fEventID;
		AliHLTEventDoneData* fEDD;
	    };

	MLUCVector<EventDoneBackLogData> fEventDoneBackLog;
	unsigned long fEventDoneBackLogSize;

	bool fQuitWaiting;

	pthread_mutex_t fEventMutex;
	//vector<EventGathererData*> fEventData;
	typedef EventGathererData* PEventGathererData;
	MLUCVector<PEventGathererData> fEventData;

	virtual void NewEvent( AliHLTEventGathererSubscriber* subscriber, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventTriggerStruct* ets );
	virtual void SubscriptionCanceled( AliHLTEventGathererSubscriber* subscriber );
	virtual void SubscriberError( AliHLTEventGathererSubscriber* subscriber );

	virtual void SetSubscriber( AliHLTEventGathererSubscriber* subscriber, bool up, AliEventID_t lastEventID );

	void ProcessCmd( AliHLTSCCommandStruct* cmd );

	class CommandProcessor: public AliHLTSCCommandProcessor
	    {
	    public:

		CommandProcessor( AliHLTTolerantEventGatherer& scheduler ):
		    fScheduler( scheduler )
			{
			}

		virtual void ProcessCmd( AliHLTSCCommandStruct* cmd )
			{
			fScheduler.ProcessCmd( cmd );
			}

	    protected:

		AliHLTTolerantEventGatherer& fScheduler;

	    };

	    friend class CommandProcessor;

	CommandProcessor fCommandProcessor;


	MLUCConditionSem<EventGathererData*> fSignal;

	AliHLTEventTriggerStruct fDummyTrigger;

	    friend class AliHLTEventGathererSubscriber;

	static bool FindEventDataByID( const PEventGathererData& elem, uint64 ref )
		{
		return elem->fEventID == (AliEventID_t)ref;
		}

	static bool FindEventBackLogByID( const EventDoneBackLogData& elem, uint64 ref )
		{
		return elem.fEventID == (AliEventID_t)ref;
		}

	static bool FindEventDataBySubscriberNdx( const PEventGathererData& elem, uint64 ref )
		{
		return elem->fSubscriberNdx == (unsigned long)ref;
		}

	AliHLTToleranceStatus* fStatus;

	AliHLTTolerantEventGathererLAMProxy* fLAMProxy;

    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3EVENTCOLLECTOR_HPP_
