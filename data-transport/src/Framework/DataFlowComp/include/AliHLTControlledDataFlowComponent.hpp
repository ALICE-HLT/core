#ifndef _ALIHLTCONTROLLEDDATAFLOWCOMPONENT_HPP_
#define _ALIHLTCONTROLLEDDATAFLOWCOMPONENT_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSamplePublisher.hpp"
#include "AliHLTPublisherProxyInterface.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTSCCommandProcessor.hpp"
#include "MLUCString.hpp"
#include "MLUCConditionSem.hpp"
#include "AliHLTEventMergerNew.hpp"
#include "AliHLTEventScattererNew.hpp"
#include "AliHLTEventGathererNew.hpp"
#include "AliHLTTriggerStackMachineAssembler.hpp"
#include "MLUCSysMESLogServer.hpp"
#include "MLUCDynamicLibraryLogServer.hpp"
#include "AliHLTReadoutList.hpp"
#include <pthread.h>
#include <signal.h>
#include <vector>

//#define DO_INLINE


class AliHLTStatus;
class AliHLTSCInterface;
struct AliHLTSCCommandStruct;
class AliHLTBufferManager;
class AliHLTDescriptorHandler;
class BCLAbstrAddressStruct;
class AliHLTEventAccounting;
class AliHLTTriggerStackMachine;

template<class BaseClass, class PublisherClass, class SubscriberClass>
class AliHLTControlledDataFlowComponent
    {
    public:

	typedef AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass> TSelf;

	virtual ~AliHLTControlledDataFlowComponent();
	
	void SetMinPreBlockCount( AliUInt32_t minPreBlockCount )
		{
		fMinPreBlockCount = minPreBlockCount;
		}

	void SetMaxPreBlockCount( AliUInt32_t maxPreBlockCount )
		{
		fMaxPreBlockCount = maxPreBlockCount;
		}

	void SetMaxNoUseCount( AliUInt32_t maxNoUseCount )
		{
		fMaxNoUseCount = maxNoUseCount;
		}

	virtual void Run();

    protected:

	struct PublisherData;
	struct SubscriberData;
	class SubscriptionListenerThread;
	class SubscriptionThread;
	class SubscriberEventCallback;
	    friend class SubscriberEventCallback;
	class CommandProcessor;
	    friend class CommandProcessor;
	class SubscriberEventCallback: public AliHLTSamplePublisher::SubscriberEventCallback
	    {
	    public:
		SubscriberEventCallback( AliHLTControlledDataFlowComponent* comp ):
		    fComp( comp )
			{
			}
		virtual ~SubscriberEventCallback() {};
		virtual void Subscribed( AliHLTSamplePublisher* pub, const char*, AliHLTSubscriberInterface* )
			{
			fComp->Subscription( pub );
			}
		virtual void Unsubscribed( AliHLTSamplePublisher* pub, const char*, AliHLTSubscriberInterface*, bool )
			{
			fComp->Unsubscription( pub );
			}
	    protected:
		AliHLTControlledDataFlowComponent* fComp;
	    };
	class CommandProcessor: public AliHLTSCCommandProcessor
	    {
	    public:
		CommandProcessor( AliHLTControlledDataFlowComponent* comp ):
		    fComp( comp )
			{
			}
		virtual ~CommandProcessor() {};
		virtual void ProcessCmd( AliHLTSCCommandStruct* cmd )
			{
			fComp->ProcessSCCmd( cmd );
			}
	    protected:
		AliHLTControlledDataFlowComponent* fComp;
	    };

// 	    friend class CommandProcessor;

	enum AliHLTControlledDataFlowComponentType { kUnknown = 0, kMerger = 1, kScatterer = 2, kGatherer = 3, kPublisherBridgeHead = 4, kSubscriberBridgeHead = 5 };

	AliHLTControlledDataFlowComponent( AliHLTControlledDataFlowComponentType componentType, const char* compName, int argc, char** argv );

	virtual void ResetConfiguration();

	virtual bool ExecCmd( AliUInt32_t cmd, bool& quit );

	virtual bool PreConfigure();
	virtual bool PostUnconfigure();

	virtual bool CheckConfiguration();
	virtual void ShowConfiguration();

	virtual void Configure();
	virtual void Unconfigure();

	virtual bool SetupStdLogging();
 	virtual bool SetupFileLogging();
 	virtual bool SetupSysLogging();
 	virtual bool SetupSysMESLogging();
	virtual bool SetupDynLibLogging();
	virtual void RemoveLogging();

	virtual bool ParseCmdLine();
	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );

	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam, int argc=-1, char** argv=NULL );
	
	virtual void IllegalCmd( AliUInt32_t state, AliUInt32_t cmd );

	virtual bool SetupSC();
	virtual AliHLTStatus* CreateStatusData();
	virtual bool SetupStatusData();
	virtual AliHLTSCInterface* CreateSCInterface();
	virtual bool SetupSCInterface();
	virtual void RemoveSC();
	virtual void LookAtMe();

	virtual void ProcessSCCmd( AliHLTSCCommandStruct* cmd );
	virtual void ExtractConfig( AliHLTSCCommandStruct* cmd );

 	virtual AliHLTDescriptorHandler* CreateDescriptorHandler();
	virtual AliHLTShmManager* CreateShmManager();

	virtual bool HasBaseObject() = 0;
	virtual unsigned long GetMinSubscriberCount() = 0;
	virtual unsigned long GetMaxSubscriberCount() = 0;
	virtual unsigned long GetMinPublisherCount() = 0;
	virtual unsigned long GetMaxPublisherCount() = 0;

	virtual BaseClass* CreateBaseObject() = 0;
	virtual void StartProcessing() = 0;
	virtual void PauseProcessing() = 0;
	virtual void ResumeProcessing() = 0;
	virtual void StopProcessing() = 0;

	virtual PublisherClass* CreatePublisher( unsigned long pubNdx, PublisherData* ) = 0;
	virtual SubscriptionListenerThread* CreateSubscriptionListenerThread( unsigned long pubNdx, PublisherData* );

	virtual AliHLTPublisherProxyInterface* CreatePublisherProxy( unsigned long pubNdx, SubscriberData* );
	virtual SubscriberClass* CreateSubscriber( unsigned long subNdx, SubscriberData* ) = 0;
	virtual SubscriptionThread* CreateSubscriptionThread( unsigned long subNdx, SubscriberData* );
	
	virtual void SetupComponents();
	virtual void Subscribe();
	virtual void Unsubscribe();

	virtual void AcceptSubscriptions();
	virtual void StopSubscriptions();
	virtual void CancelSubscriptions();

	void Subscription( AliHLTSamplePublisher* pub );
	void Unsubscription( AliHLTSamplePublisher* pub );

	virtual void PURGEALLEVENTS();

	AliHLTControlledDataFlowComponentType fComponentType;
	const char* fComponentTypeName;
	
	const char* fComponentName;

	int fArgc;
	char** fArgv;

	MLUCConditionSem<AliUInt64_t> fCmdSignal;

	bool fStdoutLogging;
	MLUCString fLogName;
	AliHLTFilteredStdoutLogServer *fLogOut;
	
	bool fFileLogging;
	AliHLTFileLogServer* fFileLog;

	bool fSysLogging;
	AliHLTSysLogServer* fSysLog;
	int fSysLogFacility;

	bool fSysMESLogging;
	MLUCSysMESLogServer* fSysMESLog;

	bool fDynLibLogging;
        MLUCDynamicLibraryLogServer* fDynLibLog;
        MLUCString fDynLibPath;

	struct LAMAddressData
	    {
		MLUCString fStr;
		BCLAbstrAddressStruct* fAddress;
	    };

	AliHLTStatus* fStatus;
	MLUCString fSCInterfaceAddress;
	AliHLTSCInterface* fSCInterface;
	vector<LAMAddressData> fLAMAddresses;
	pthread_mutex_t fLAMAddressMutex;

	CommandProcessor fCmdProcessor;

	AliUInt32_t fMaxPreEventCount;
	int fMaxPreEventCountExp2;
	AliUInt32_t fMinPreBlockCount;
	AliUInt32_t fMaxPreBlockCount;
	AliUInt32_t fMaxNoUseCount;

 	AliHLTDescriptorHandler* fDescriptorHandler;
	AliHLTShmManager* fShmManager;


	BaseClass* fBaseObject;

	struct PublisherData
	    {
		PublisherClass* fPublisher;
		MLUCString fOwnPublisherName;
		AliUInt32_t fSubscriberCount;
		bool fSubscriptionListenerLoopRunning;
		SubscriptionListenerThread* fSubscriptionListenerThread;
		unsigned fNdx;
		PublisherData()
			{
			fPublisher = NULL;
			fSubscriptionListenerThread = NULL;
			fSubscriberCount = 0;
			fSubscriptionListenerLoopRunning = false;
			fNdx = ~(unsigned)0;
			}
	    };
 	vector<PublisherData> fPublishers;
	pthread_mutex_t fPublisherMutex;
	AliUInt32_t fPublisherTimeout;
	SubscriberEventCallback fSubscriberEventCallback;

	struct SubscriberRemovalData
	    {
		SubscriberRemovalData( const char* subName, unsigned pubNdx )
			{
			fSubscriberName = subName;
			fPublisherIndex = pubNdx;
			}
		SubscriberRemovalData()
			{
			fSubscriberName = "";
			fPublisherIndex = (unsigned)-1;
			}
		MLUCString fSubscriberName;
		unsigned fPublisherIndex;
	    };
		
	pthread_mutex_t fSubscriberRemovalListMutex;
	vector<SubscriberRemovalData> fSubscriberRemovalList;



	struct SubscriberData
	    {
		SubscriberClass* fSubscriber;
		MLUCString fAttachedPublisherName;
		// 		key_t fPub2SubProxyShmKey;
		// 		key_t fSub2PubProxyShmKey;
		// 		unsigned long fProxyShmSize;
		AliHLTPublisherProxyInterface* fPublisherProxy;
		MLUCString fSubscriberID;
		SubscriptionThread* fSubscriptionThread;
		bool fSubscriptionLoopRunning;
		unsigned fNdx;
		AliUInt32_t fEventModulo;
		std::vector<MLUCString> fEventTypeStackMachineCodesText;
		std::vector<AliHLTEventTriggerStruct*> fEventTypeStackMachineCodes;
		volatile_hltreadout_uint32_t* fContributingDDLs;
		std::vector<unsigned long> fSubscriberInputModulos;
		std::vector<MLUCString> fSubscriberInputEventTypeStackMachineCodesText;
		std::vector<AliHLTEventTriggerStruct*> fSubscriberInputEventTypeStackMachineCodes;
		SubscriberData()
			{
			fSubscriber = NULL;
			fPublisherProxy = NULL;
			fSubscriptionThread = NULL;
			fSubscriptionLoopRunning = false;
			fNdx = ~(unsigned)0;
			fContributingDDLs = NULL;
			}
	    };
 	vector<SubscriberData> fSubscribers;
	pthread_mutex_t fSubscriberMutex;
	AliUInt32_t fSubscriberRetryCount;
	AliUInt32_t fSubscriberRetryTime;

	bool fSubscriptionRequestOldEvents;
	
	class SubscriptionListenerThread;
	    friend class SubscriptionListenerThread;
	class SubscriptionListenerThread: public AliHLTThread
	    {
	    public:
		SubscriptionListenerThread( AliHLTControlledDataFlowComponent* comp, AliHLTSamplePublisher* pub  ):
		    fPub( pub ), fComp( comp )
			{
			fQuitted = true;
			}
		virtual ~SubscriptionListenerThread() {};
		bool HasQuitted()
			{
			return fQuitted;
			}
		void Quit()
			{
			if ( fPub )
			    QuitPipeSubscriptionLoop( *fPub );
			}
	    protected:
		virtual void Run();
		
		AliHLTSamplePublisher* fPub;
		AliHLTControlledDataFlowComponent* fComp;
		bool fQuitted;
	    };


	class SubscriptionThread;
	    friend class SubscriptionThread;
	class SubscriptionThread: public AliHLTThread
	    {
	    public:
		SubscriptionThread( AliHLTControlledDataFlowComponent* comp, AliHLTPublisherProxyInterface* proxy, SubscriberClass* sub ):
		    fProxy( proxy ), fSubscriber( sub ), fComp( comp )
			{
			fQuitted = true;
			}
		virtual ~SubscriptionThread() {};
		bool HasQuitted()
			{
			return fQuitted;
			}
		void Quit()
			{
			if ( fProxy )
			    fProxy->Unsubscribe( *fSubscriber );
			}
		void AbortPublishing()
			{
			if ( fProxy )
			    fProxy->AbortPublishing();
			}
	    protected:
		virtual void Run();

		AliHLTPublisherProxyInterface* fProxy;
		SubscriberClass* fSubscriber;
		AliHLTControlledDataFlowComponent* fComp;
		bool fQuitted;
	    };

	unsigned long fMaxSubscriberIndex;
	unsigned long fMaxPublisherIndex;


	unsigned long fEventTimeout_ms;
	unsigned long fDynamicTimeoutAdaptionMinTimeout_ms;
	unsigned long fDynamicTimeoutAdaptionMaxTimeout_ms;
	unsigned long fEventTimeoutShift_ms;
	unsigned long fDynamicInputXablingNoEventCntLimit;
	AliHLTEventMergerNew::TETSMergeMode fETSMergeMode;

	bool fAliceHLT;

	AliHLTEventAccounting* fEventAccounting;

	std::vector<hltreadout_uint32_t const*> fTriggerClassContributingDDLs;
	MLUCMutex fTriggerClassContributingDDLsMutex;

	struct SubscriptionStatusData
	    {
		MLUCMutex fMutex;
		unsigned long fRequiredSubscriptions;
		unsigned long fPresentSubscriptions;
		AliHLTStatus* fStatus;
		SubscriptionStatusData()
			{
			fRequiredSubscriptions = fPresentSubscriptions = 0;
			fStatus = NULL;
			}
	    };
	SubscriptionStatusData fSubscriptionStatusData;

	static void AcceptingSubscriptionsCallbackFunc( void* status );

	unsigned long fScattererParam;
	bool fScattererParamSet;

	unsigned long fGathererBroadcastTimeout_ms;
	unsigned long fGathererSORTimeout_ms;


	unsigned long fEventModuloPreDivisor;

	AliHLTTriggerStackMachine* fTriggerStackMachine;

	std::vector<MLUCString> fEventTypeStackMachineCodesText;

	std::vector<AliHLTEventTriggerStruct*> fEventTypeStackMachineCodes;

	AliHLTTriggerStackMachineAssembler fStackMachineAssembler;

	std::map<MLUCString,unsigned> fTriggerClassBitIndices;

	std::map<MLUCString,MLUCString> fTriggerClassGroups;

	//AliUInt32_t fSubscriptionEventModulo;
	//bool fUseSubscriptionEventModulo;


	unsigned long fRunNumber;
	unsigned long fBeamType;
	unsigned long fHLTMode;
	MLUCString fRunType;

	MLUCString fCTPTriggerClasses;

	std::map<MLUCString,MLUCString> fSubscriberInputDDLSpecifications;

	bool fEventIDBased;

	bool fStrictSORRequirements;

	struct sigaction fOldSigAction;

    };

#ifdef DO_INLINE
#define INLINE inline
#include "AliHLTControlledDataFlowComponent.inl"
#endif



class AliHLTControlledMerger: public AliHLTControlledDataFlowComponent<AliHLTEventMergerNew, AliHLTEventMergerNew::TPublisher, AliHLTEventMergerNew::TSubscriber>
    {
    public:

	typedef AliHLTControlledDataFlowComponent<AliHLTEventMergerNew, AliHLTEventMergerNew::TPublisher, AliHLTEventMergerNew::TSubscriber> TParent;

	AliHLTControlledMerger( const char* compName, int argc, char** argv);
	virtual ~AliHLTControlledMerger();

    protected:

	virtual bool HasBaseObject();
	virtual unsigned long GetMinSubscriberCount();
	virtual unsigned long GetMaxSubscriberCount();
	virtual unsigned long GetMinPublisherCount();
	virtual unsigned long GetMaxPublisherCount();

	virtual bool ExecCmd( AliUInt32_t cmd, bool& quit );
	virtual void ProcessSCCmd( AliHLTSCCommandStruct* cmd );

	virtual AliHLTEventMergerNew* CreateBaseObject();
	virtual void StartProcessing();
	virtual void PauseProcessing();
	virtual void ResumeProcessing();
	virtual void StopProcessing();

	virtual AliHLTEventMergerNew::TPublisher* CreatePublisher( unsigned long pubNdx, PublisherData* );

	virtual AliHLTEventMergerNew::TSubscriber* CreateSubscriber( unsigned long subNdx, SubscriberData* );

	virtual void Unconfigure();

	virtual void SetupComponents();

    };


class AliHLTControlledScattererBase: public AliHLTControlledDataFlowComponent<AliHLTEventScattererNew, AliHLTEventScattererNew::TPublisher, AliHLTEventScattererNew::TSubscriber>
    {
    public:

	typedef AliHLTControlledDataFlowComponent<AliHLTEventScattererNew, AliHLTEventScattererNew::TPublisher, AliHLTEventScattererNew::TSubscriber> TParent;

	AliHLTControlledScattererBase( const char* compName, int argc, char** argv);
	virtual ~AliHLTControlledScattererBase();

    protected:

	virtual bool HasBaseObject();
	virtual unsigned long GetMinSubscriberCount();
	virtual unsigned long GetMaxSubscriberCount();
	virtual unsigned long GetMinPublisherCount();
	virtual unsigned long GetMaxPublisherCount();

	virtual bool ExecCmd( AliUInt32_t cmd, bool& quit );
	virtual void ProcessSCCmd( AliHLTSCCommandStruct* cmd );

	// 	virtual AliHLTEventScattererNew* CreateBaseObject();
	virtual void StartProcessing();
	virtual void PauseProcessing();
	virtual void ResumeProcessing();
	virtual void StopProcessing();

	virtual AliHLTEventScattererNew::TPublisher* CreatePublisher( unsigned long pubNdx, PublisherData* );

	virtual AliHLTEventScattererNew::TSubscriber* CreateSubscriber( unsigned long subNdx, SubscriberData* );

	virtual void SetupComponents();

    };

template<class ScattererType>
class AliHLTControlledScatterer: public AliHLTControlledScattererBase
    {
    public:
	AliHLTControlledScatterer( const char* compName, int argc, char** argv):
	    AliHLTControlledScattererBase( compName, argc, argv)
		{
		}
	virtual ~AliHLTControlledScatterer()
		{
		}

    protected:
 	virtual ScattererType* CreateBaseObject()
		{
		return new ScattererType( fComponentName, fMaxPreEventCountExp2 );
		}

    };



class AliHLTControlledGatherer: public AliHLTControlledDataFlowComponent<AliHLTEventGathererNew, AliHLTEventGathererNew::TPublisher, AliHLTEventGathererNew::TSubscriber>
    {
    public:

	typedef AliHLTControlledDataFlowComponent<AliHLTEventGathererNew, AliHLTEventGathererNew::TPublisher, AliHLTEventGathererNew::TSubscriber> TParent;

	AliHLTControlledGatherer( const char* compName, int argc, char** argv );
	virtual ~AliHLTControlledGatherer();

    protected:

	virtual bool HasBaseObject();
	virtual unsigned long GetMinSubscriberCount();
	virtual unsigned long GetMaxSubscriberCount();
	virtual unsigned long GetMinPublisherCount();
	virtual unsigned long GetMaxPublisherCount();

	virtual bool ExecCmd( AliUInt32_t cmd, bool& quit );
	virtual void ProcessSCCmd( AliHLTSCCommandStruct* cmd );

	virtual AliHLTEventGathererNew* CreateBaseObject();
	virtual void StartProcessing();
	virtual void PauseProcessing();
	virtual void ResumeProcessing();
	virtual void StopProcessing();

	virtual AliHLTEventGathererNew::TPublisher* CreatePublisher( unsigned long pubNdx, PublisherData* );

	virtual AliHLTEventGathererNew::TSubscriber* CreateSubscriber( unsigned long subNdx, SubscriberData* );

	virtual void SetupComponents();

    };

/*
class AliHLTControlledSubscriberBridgeHead: public AliHLTControlledDataFlowComponent<AliHLTSubscriberBridgeHead, AliHLTSamplePublisher, AliHLTSubscriberBridgeHead>
    {
    public:

	typedef AliHLTControlledDataFlowComponent<AliHLTSubscriberBridgeHead, AliHLTSamplePublisher, AliHLTSubscriberBridgeHead> TParent;

	AliHLTControlledSubscriberBridgeHead( const char* compName, int argc, char** argv );
	~AliHLTControlledSubscriberBridgeHead();

    protected:

	virtual bool HasBaseObject();
	virtual unsigned long GetMinSubscriberCount();
	virtual unsigned long GetMaxSubscriberCount();
	virtual unsigned long GetMinPublisherCount();
	virtual unsigned long GetMaxPublisherCount();

	virtual AliHLTEventGathererNew* CreateBaseObject();
	virtual void StartProcessing();
	virtual void PauseProcessing();
	virtual void ResumeProcessing();
	virtual void StopProcessing();

	virtual AliHLTSamplePublisher* CreatePublisher( unsigned long pubNdx, PublisherData* );

	virtual AlHLTSubscriberBridgeHead* CreateSubscriber( unsigned long subNdx, SubscriberData* );

	virtual void SetupComponents();

	virtual void ResetConfiguration();

	virtual bool PreConfigure();
	virtual bool PostUnconfigure();

	virtual bool CheckConfiguration();
	virtual void ShowConfiguration();

	virtual void Configure();
	virtual void Unconfigure();

	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam );
	
	virtual void IllegalCmd( AliUInt32_t state, AliUInt32_t cmd );

	virtual bool SetupSC();
	virtual void ProcessSCCmd( AliHLTSCCommandStruct* cmd );



	class TolerantSubscriberBridgeHeadLAM: public AliHLTBridgeHeadLAMProxy
	    {
	    public:
		TolerantSubscriberBridgeHeadLAM( AliHLTControlledSubscriberBridgeHead* csbh ):
		    fComp( csbh )
			{
			}
		virtual void LookAtMe();
	    protected:
		AliHLTControlledSubscriberBridgeHead* fComp;
	    };

	struct TolerantSubscriberBridgeHeadComData
	    {
		TolerantSubscriberBridgeHeadComData()
			{
			fBlobMsgCom = NULL;
			fMsgCom = NULL;
			fBlobCom = NULL;
			fOwnBlobMsgAddress=NULL;
			fOwnBlobAddress=NULL; 
			fOwnMsgAddress=NULL;
			fRemoteBlobMsgAddress=NULL;
			fRemoteMsgAddress=NULL;
			fBlobBufferSize = 0;
			fCommunicationTimeout = 0;
			}
		BCLMsgCommunication* fBlobMsgCom;
		BCLMsgCommunication* fMsgCom;
		BCLBlobCommunication* fBlobCom;
		BCLAbstrAddressStruct *fOwnBlobMsgAddress, 
		    *fOwnBlobAddress, 
		    *fOwnMsgAddress,
		    *fRemoteBlobMsgAddress,
		    *fRemoteMsgAddress;
	
		uint32 fBlobBufferSize;
		uint32 fCommunicationTimeout;
	    };

	class TolerantSubscriberBridgeHeadErrorCallback: public AliHLTBridgeHeadErrorCallback<AliHLTSubscriberBridgeHead>
	    {
	    public:
		TolerantSubscriberBridgeHeadErrorCallback();
		virtual ~TolerantSubscriberBridgeHeadErrorCallback();
		virtual void ConnectionError( AliHLTSubscriberBridgeHead* bridgeHead );
		virtual void ConnectionEstablished( AliHLTSubscriberBridgeHead* bridgeHead );
	    protected:
	    private:
	    };

	AliHLTBridgeStatusData fBridgeStatus;
	TolerantSubscriberBridgeHeadComData fComData;
	

    };
*/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTCONTROLLEDDATAFLOWCOMPONENT_HPP_
