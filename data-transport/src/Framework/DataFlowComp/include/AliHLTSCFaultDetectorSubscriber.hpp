#ifndef _ALIHLTFAULTDETECTORSUBSCRIBER_HPP_
#define _ALIHLTFAULTDETECTORSUBSCRIBER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#include "AliHLTSCProcessStatus.hpp"
#include "AliHLTSCToleranceDetectorStatus.hpp"
#include "AliHLTTDStatus.hpp"
#include "AliHLTTimer.hpp"
#include "AliHLTEventDoneData.h"
#include "AliHLTSubscriberInterface.hpp"
#include "MLUCString.hpp"
#include <pthread.h>
#include <errno.h>


class AliHLTSCFaultDetectorSubscriber: public AliHLTSubscriberInterface, public AliHLTTimerCallback
    {
    public:

	AliHLTSCFaultDetectorSubscriber( const char* name );

	virtual ~AliHLTSCFaultDetectorSubscriber();

	void SetTimer( AliHLTTimer* timer );
	
	void SetEventTimeout( unsigned long timeout_us );

	void SetPublisher( AliHLTPublisherInterface* publisher );

	void SetStatus( AliHLTTDStatus* status );

	void SetPublisherNdx( unsigned long ndx );

	void Pause();

	void Start();

	virtual void Timeout() = 0;
	virtual void EventReceived( AliEventID_t eventID ) = 0;

	// This method is called whenever a new wanted 
	// event is available.
	virtual int NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
			      const AliHLTSubEventDataDescriptor& sbevent,
			      const AliHLTEventTriggerStruct& ets );

	// This method is called for a transient subscriber, 
	// when an event has been removed from the list. 
	virtual int EventCanceled( AliHLTPublisherInterface& publisher, AliEventID_t eventID );

	virtual int EventDoneData( AliHLTPublisherInterface&, 
				   const AliHLTEventDoneData& )
		{
		return 0;
		}

	// This method is called when the subscription ends.
	virtual int SubscriptionCanceled( AliHLTPublisherInterface& publisher );

	// This method is called when the publishing 
	// service runs out of buffer space. The subscriber 
	// has to free as many events as possible and send an 
	// EventDone message with their IDs to the publisher
	virtual int ReleaseEventsRequest( AliHLTPublisherInterface& publisher ); 

	// Method used to check alive status of subscriber. 
	// This message has to be answered with 
	// PingAck message within less  than a second.
	virtual int Ping( AliHLTPublisherInterface& publisher );

	// Method used by a subscriber in response to a Ping 
	// received message. The PingAck message has to be 
	// called within less than one second.
	virtual int PingAck( AliHLTPublisherInterface& publisher );
	
    protected:

	virtual void TimerExpired( AliUInt64_t notData, unsigned long );

	AliEventID_t fLastEventID;

	AliHLTTimer* fTimer;
	AliUInt32_t fTimeoutID;
	bool fTimerStarted;

	bool fPaused;
	
	unsigned long fTimeout_us;

	AliHLTTDStatus* fStatus;

	AliHLTPublisherInterface* fPublisher;

	unsigned long fPublisherNdx;

	pthread_mutex_t fMutex;

	AliHLTEventDoneData fDoneData;

    private:
    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTFAULTDETECTORSUBSCRIBER_HPP_
