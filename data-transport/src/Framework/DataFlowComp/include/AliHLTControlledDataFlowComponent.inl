/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTControlledDataFlowComponent.hpp"
#include "AliHLTStatus.hpp"
#include "AliHLTSCCommand.hpp"
#include "AliHLTSCProcessControlCmds.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "AliHLTSCTypes.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTSCInterface.hpp"
#include "AliHLTPublisherProxyInterface.hpp"
#include "AliHLTPublisherPipeProxy.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTShmManager.hpp"
#include <syslog.h>
#include <stdio.h>


#define TRACE printf( "%s:%d\n", __FILE__, __LINE__ )

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::AliHLTControlledDataFlowComponent( AliHLTControlledDataFlowComponent::AliHLTControlledDataFlowComponentType componentType, 
								      const char* compName, int argc, char** argv ):
    fCmdProcessor( this ), fSubscriberEventCallback( this )
    {
    fComponentType = componentType;
    switch ( fComponentType )
	{
	case kMerger:
	    fComponentTypeName = "Merger"; 
	    break;
	case kScatterer:
	    fComponentTypeName = "Scatterer"; 
	    break;
	case kGatherer:
	    fComponentTypeName = "Gatherer";
	    break;
	case kPublisherBridgeHead:
	    fComponentTypeName = "Publisher Bridge Head"; 
	    break;
	case kSubscriberBridgeHead:
	    fComponentTypeName = "Subscriber Bridge Head"; 
	    break;
	default:
	    fComponentTypeName = "Unknown";
	    break;
	}
    fComponentName = compName;
    fArgc = argc;
    fArgv = argv;
    fStatus = NULL;
    fLogOut = NULL;
    fFileLog = NULL;
    fSysLog = NULL;
    fDescriptorHandler = NULL;
    fShmManager = NULL;
    fSubscriberRetryCount = 0;
    fSubscriberRetryTime = 100; // milliseconds
    fBaseObject = NULL;
    fMaxPreEventCount = 0;
    fMaxPreEventCountExp2 = -1;
    pthread_mutex_init( &fPublisherMutex, NULL );
    pthread_mutex_init( &fSubscriberMutex, NULL );
    ResetConfiguration();
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::~AliHLTControlledDataFlowComponent()
    {
    Unconfigure();
    pthread_mutex_destroy( &fPublisherMutex );
    pthread_mutex_destroy( &fSubscriberMutex );
    }


template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Run()
    {
    if ( !PreConfigure() )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Run", "Pre-Configuration Error" )
	    << "Pre-Configuration Error for " << fComponentTypeName << " component "
	    << fComponentName << "." << ENDLOG;
	PostUnconfigure();
	return;
	}
    LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::Run", "Component Started" )
	<< fComponentTypeName << " component " << fComponentName << " started." << ENDLOG;
    AliUInt32_t cmd = kAliHLTPCNOPCmd;
    bool quit = false;
    do
	{
	fCmdSignal.Wait();
	cmd = (AliUInt32_t)fCmdSignal.PopNotificationData();
	switch ( cmd )
	    {
	    case kAliHLTPCEndProgramCmd:
		if ( (fStatus->fProcessStatus.fState & kAliHLTPCStartedStateFlag) &&
		     !(fStatus->fProcessStatus.fState & ~kAliHLTPCStartedStateFlag) )
		    {
		    quit = true;
		    cmd = kAliHLTPCNOPCmd;
		    }
		break;
	    case kAliHLTPCSubscribeCmd:
		if ( GetMinSubscriberCount()<=0 )
		    break;
		if ( (fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) &&
		     !(fStatus->fProcessStatus.fState & (kAliHLTPCChangingStateFlag|kAliHLTPCSubscribedStateFlag)) &&
		     !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		    {
		    fStatus->fProcessStatus.fState |= (kAliHLTPCChangingStateFlag|kAliHLTPCSubscribedStateFlag);
		    Subscribe();
		    cmd = kAliHLTPCNOPCmd;
		    }
		    
// 		switch ( fStatus->fProcessStatus.fState )
// 		    {
// 		    case kAliHLTPCConfiguredState: // Fallthrough
// 		    case kAliHLTPCAcceptingSubscriptionsState: // Fallthrough
// 		    case kAliHLTPCWithSubscribersState:
			
// 			cmd = kAliHLTPCNOPCmd;
// 			break;
// 		    }
		break;
	    case kAliHLTPCUnsubscribeCmd:
		if ( GetMinSubscriberCount()<=0 )
		    break;
		if ( (fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) &&
		     !(fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) && 
		     !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		    {
		    fStatus->fProcessStatus.fState |= kAliHLTPCChangingStateFlag;
		    Unsubscribe();
		    fStatus->fProcessStatus.fState &= ~(kAliHLTPCChangingStateFlag|kAliHLTPCSubscribedStateFlag|kAliHLTPCConsumerErrorStateFlag);
		    cmd = kAliHLTPCNOPCmd;
		    }
		break;
	    case kAliHLTPCAcceptSubscriptionsCmd:
		if ( GetMinPublisherCount()<=0 )
		    break;
		if ( (fStatus->fProcessStatus.fState & kAliHLTPCConfiguredStateFlag) &&
		     !(fStatus->fProcessStatus.fState & (kAliHLTPCChangingStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag)) &&
		     !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		    {
		    fStatus->fProcessStatus.fState |= (kAliHLTPCChangingStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag);
		    AcceptSubscriptions();
		    cmd = kAliHLTPCNOPCmd;
		    }
		break;
	    case kAliHLTPCStopSubscriptionsCmd:
		if ( GetMinPublisherCount()<=0 )
		    break;
		if ( (fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) &&
		   !(fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) && 
		   !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		    {
		    fStatus->fProcessStatus.fState |= kAliHLTPCChangingStateFlag;
		    StopSubscriptions();
 		    fStatus->fProcessStatus.fState &= ~(kAliHLTPCChangingStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag|kAliHLTPCProducerErrorStateFlag);
		    cmd = kAliHLTPCNOPCmd;
		    }
		break;
	    case kAliHLTPCCancelSubscriptionsCmd:
		if ( GetMinPublisherCount()<=0 )
		    break;
		if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) &&
		     !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		    {
		    CancelSubscriptions();
		    cmd = kAliHLTPCNOPCmd;
		    }
		break;
	    case kAliHLTPCStartProcessingCmd:
		{
		bool ok = true;
		if ( GetMinPublisherCount()>0 )
		    {
		    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) ||
			 (fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
			ok = false;
		    }
		if ( GetMinSubscriberCount()>0 )
		    {
		    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) ||
			 (fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
			ok = false;
		    }
		if ( !ok )
		    break;
		fStatus->fProcessStatus.fState |= (kAliHLTPCChangingStateFlag|kAliHLTPCRunningStateFlag);
		StartBaseObject();
		cmd = kAliHLTPCNOPCmd;
		break;
		}
	    case kAliHLTPCPauseProcessingCmd:
// 		if ( fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag )
// 		    break;
		BaseObjectPause();
		cmd = kAliHLTPCNOPCmd;
		break;
	    case kAliHLTPCResumeProcessingCmd:
// 		if ( fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag )
// 		    break;
		BaseObjectResume();
		cmd = kAliHLTPCNOPCmd;
		break;
	    case kAliHLTPCStopProcessingCmd:
		if ( !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		    break;
		fStatus->fProcessStatus.fState |= kAliHLTPCChangingStateFlag;		
		StopBaseObject();
		fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;		
		cmd = kAliHLTPCNOPCmd;
		break;
	    }
	if ( cmd != kAliHLTPCNOPCmd )
	    IllegalCmd( fStatus->fProcessStatus.fState, cmd );
	}
    while ( !quit );
    LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::Run", "Component Ended" )
	<< fComponentTypeName << " component " << fComponentName << " ended." << ENDLOG;
    PostUnconfigure();
    }


template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::ResetConfiguration()
    {
    fStdoutLogging = true;
    fFileLogging = true;
    fSysLogging = false;
    if ( fComponentName )
	fLogName = fComponentName;
    else
	fLogName = fArgv[0];
    fSCInterfaceAddress = "tcpmsg://localhost:10000/";

//     fMaxPreEventCount = 1;
//     fMaxPreEventCountExp2 = 0;
    fMinPreBlockCount = 1;
    fMaxPreBlockCount = 1;
    fMaxNoUseCount = 0;

    fPublisherTimeout = ~(AliUInt32_t)0;

    fSubscriberRetryCount = 0;
    fSubscriberRetryTime = 100; // milliseconds

    typename vector<TSelf::SubscriberData>::iterator subIter, subEnd;
    pthread_mutex_lock( &fSubscriberMutex );
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    while ( subIter != subEnd )
	{
	subIter->fSubscriptionLoopRunning = false;
	subIter++;
	}
    pthread_mutex_unlock( &fSubscriberMutex );

    typename vector<TSelf::PublisherData>::iterator PubIter, PubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    PubIter = fPublishers.begin();
    PubEnd = fPublishers.end();
    while ( PubIter != PubEnd )
	{
	PubIter->fSubscriptionListenerLoopRunning = false;
	PubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::PreConfigure()
    {
    gLogLevel = AliHLTLog::kAll;
    if ( !SetupStdLogging() ) // Set this up here, so that we can see error output from command line parsing.
	return false; 
    if ( !ParseCmdLine() )
	return false;
    if ( !SetupStdLogging() ) // set it up again, as the configuration/cmd line might have disabled it.
	return false;
    if ( !SetupFileLogging() )
	return false;
    if ( !SetupSysLogging() )
	return false;
    if ( !SetupSC() )
	return false;
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::PostUnconfigure()
    {
    RemoveSC();
    RemoveLogging();
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CheckConfiguration()
    {
    if ( GetMinPublisherCount()>fPublishers.size() )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::CheckConfiguration", "Not enough publishers specified" )
	    << "Not enough publishers specified (e.g. using -publisher command line options). Need between "
	    << MLUCLog::kDec << GetMinPublisherCount() << " and " << GetMaxPublisherCount()
	    << " publishers." << ENDLOG;
	return false;
	}
    if ( GetMaxPublisherCount()<fPublishers.size() )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::CheckConfiguration", "Too many publishers specified" )
	    << "Too many publishers specified (e.g. using -publisher command line options). Need between "
	    << MLUCLog::kDec << GetMinPublisherCount() << " and " << GetMaxPublisherCount()
	    << " publishers." << ENDLOG;
	return false;
	}
    if ( GetMinSubscriberCount()>fSubscribers.size() )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::CheckConfiguration", "Not enough subscribers specified" )
	    << "Not enough subscribers specified (e.g. using -subscriber command line options). Need between "
	    << MLUCLog::kDec << GetMinSubscriberCount() << " and " << GetMaxSubscriberCount()
	    << " subscribers." << ENDLOG;
	return false;
	}
    if ( GetMaxSubscriberCount()<fSubscribers.size() )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::CheckConfiguration", "Too many subscribers specified" )
	    << "Too many subscribers specified (e.g. using -subscriber command line options). Need between "
	    << MLUCLog::kDec << GetMinSubscriberCount() << " and " << GetMaxSubscriberCount()
	    << " subscribers." << ENDLOG;
	return false;
	}
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::ShowConfiguration()
    {
    LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::ShowConfiguration", "SC config" )
	<< "SC interface address: " << fSCInterfaceAddress.c_str() << ENDLOG;
    LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::ShowConfiguration", "Log config" )
	<< (fStdoutLogging ? "Stdout logging, " : "") << (fFileLogging ? "File logging, " : "")
	<< (fSysLogging ? "Syslog logging, " : "") << "logname: " << fLogName.c_str() << "." 
	<< ENDLOG;
    unsigned long n;

    LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::ShowConfiguration", "Shm config" )
	<< "Event slot pre-allocation count: " << AliHLTLog::kDec
	<< fMaxPreEventCount << " - maximum no use count: " << fMaxNoUseCount
	<< "." << ENDLOG;

    typename vector<TSelf::PublisherData>::iterator pubIter, pubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    n = 0;
    while ( pubIter != pubEnd )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::ShowConfiguration", "Own Publisher config" )
	    << "Own publisher name " << AliHLTLog::kDec << n << ": " << pubIter->fOwnPublisherName.c_str() << "." << ENDLOG;
	n++;
	pubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );

    typename vector<TSelf::SubscriberData>::iterator subIter, subEnd;
    pthread_mutex_lock( &fSubscriberMutex );
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    n = 0;
    while ( subIter != subEnd )
	{
	LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::ShowConfiguration", "Own Subscriber config" )
	    << "Attached publisher name " << AliHLTLog::kDec << n << ": " << subIter->fAttachedPublisherName.c_str()
	    << " - subscriber ID:" << subIter->fSubscriberID.c_str() << "." << ENDLOG;
	n++;
	subIter++;
	}
    pthread_mutex_unlock( &fSubscriberMutex );

    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Configure()
    {
    ShowConfiguration();
    if ( !CheckConfiguration() )
	{
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	return;
	}

    fDescriptorHandler = CreateDescriptorHandler();
    if ( !fDescriptorHandler )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Unable to Create Descriptor Handler" )
	    << "Unable to create event descriptor handler." << ENDLOG;
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	return;
	}

    fShmManager = CreateShmManager();
    if ( !fShmManager )
	{
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Error creating shared memory manager" )
	    << "Error creating Shared Memory Manager object." << ENDLOG;
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	return;
	}

    fBaseObject = CreateBaseObject();
    if ( !fBaseObject )
	{
	MLUCString title;
	title = "Unable to Create ";
	title += fComponentTypeName;
	title += " Base Object";
	LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", title.c_str() )
	    << "Unable to create " << fComponentTypeName << " component base object." << ENDLOG;
	fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	return;
	}

    unsigned long n;

    typename vector<TSelf::PublisherData>::iterator pubIter, pubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    n = 0;
    while ( pubIter != pubEnd )
	{
	pubIter->fPublisher = CreatePublisher( n, &(*pubIter) );
	if ( !pubIter->fPublisher )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Unable to Create Publisher Object" )
		<< "Unable to create publisher object " << AliHLTLog::kDec << n << " - '"
		<< pubIter->fOwnPublisherName.c_str() << "'." << ENDLOG;
	    pthread_mutex_unlock( &fPublisherMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return;
	    }
	pubIter->fSubscriptionListenerThread = CreateSubscriptionListenerThread( n, &(*pubIter) );
	if ( !pubIter->fSubscriptionListenerThread )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Unable to Create Subscription Listener Thread Object" )
		<< "Unable to create subscription listener thread object for publisher " << AliHLTLog::kDec << n << " ('"
		<< pubIter->fOwnPublisherName.c_str() << "')." << ENDLOG;
	    pthread_mutex_unlock( &fPublisherMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return;
	    }
	n++;
	pubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );

    typename vector<TSelf::SubscriberData>::iterator subIter, subEnd;
    pthread_mutex_lock( &fSubscriberMutex );
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    n = 0;
    while ( subIter != subEnd )
	{
	subIter->fSubscriber = CreateSubscriber( n, &(*subIter) );
	if ( !subIter->fSubscriber )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Unable to Create Subscriber Object" )
		<< "Unable to create subscriber object " << AliHLTLog::kDec << n << " - '"
		<< subIter->fAttachedPublisherName.c_str() << "'/'" << subIter->fSubscriberID.c_str()
		<< "'." << ENDLOG;
	    pthread_mutex_unlock( &fSubscriberMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return;
	    }
	subIter->fPublisherProxy = CreatePublisherProxy( n, &(*subIter) );
	if ( !subIter->fPublisherProxy )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Unable to Create Publisher Proxy Object" )
		<< "Unable to create publisher proxy object " << AliHLTLog::kDec << n << " - '"
		<< subIter->fAttachedPublisherName.c_str()
		<< "'." << ENDLOG;
	    pthread_mutex_unlock( &fSubscriberMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return;
	    }
	subIter->fSubscriptionThread = CreateSubscriptionThread( n, &(*subIter) );
	if ( !subIter->fSubscriptionThread )
	    {
	    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Configure", "Unable to Create Subscription Thread Object" )
		<< "Unable to create subscription thread object for subscriber " << AliHLTLog::kDec << n << " - '"
		<< subIter->fAttachedPublisherName.c_str() << "'/'" << subIter->fSubscriberID.c_str()
		<< "'." << ENDLOG;
	    pthread_mutex_unlock( &fSubscriberMutex );
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    return;
	    }
	n++;
	subIter++;
	}
    pthread_mutex_unlock( &fSubscriberMutex );
    
    SetupComponents();

    fStatus->fProcessStatus.fState |= kAliHLTPCConfiguredStateFlag;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Unconfigure()
    {
    typename vector<TSelf::SubscriberData>::iterator subIter, subEnd;
    pthread_mutex_lock( &fSubscriberMutex );
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    while ( subIter != subEnd )
	{
	if ( subIter->fSubscriptionThread )
	    {
	    delete subIter->fSubscriptionThread;
	    subIter->fSubscriptionThread = NULL;
	    }
	if ( subIter->fPublisherProxy )
	    {
	    delete subIter->fPublisherProxy;
	    subIter->fPublisherProxy = NULL;
	    }
	if ( subIter->fSubscriber )
	    {
	    delete subIter->fSubscriber;
	    subIter->fSubscriber = NULL;
	    }
	subIter++;
	}
    pthread_mutex_unlock( &fSubscriberMutex );
    
    typename vector<TSelf::PublisherData>::iterator pubIter, pubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    while ( pubIter != pubEnd )
	{
	if ( pubIter->fSubscriptionListenerThread )
	    {
	    delete pubIter->fSubscriptionListenerThread;
	    pubIter->fSubscriptionListenerThread = NULL;
	    }
	if ( pubIter->fPublisher )
	    {
	    delete pubIter->fPublisher;
	    pubIter->fPublisher = NULL;
	    }
	pubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );

    if ( fShmManager )
	{
	delete fShmManager;
	fShmManager = NULL;
	}

    if ( fDescriptorHandler )
	{
	delete fDescriptorHandler;
	fDescriptorHandler = NULL;
	}
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SetupStdLogging()
    {
    if ( fStdoutLogging )
	{
	if ( !fLogOut )
	    {
	    fLogOut = new AliHLTFilteredStdoutLogServer;
	    if ( !fLogOut )
		return false;
	    gLog.AddServer( fLogOut );
	    }
	}
    else
	{
	if ( fLogOut )
	    {
	    gLog.DelServer( fLogOut );
	    delete fLogOut;
	    fLogOut = NULL;
	    }
	}
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SetupFileLogging()
    {
    if ( fFileLogging )
	{
	if ( !fFileLog )
	    {
	    fFileLog = new AliHLTFileLogServer( fLogName.c_str(), ".log", 2500000 );
	    if ( !fFileLog )
		return false;
	    gLog.AddServer( fFileLog );
	    }
	}
    else
	{
	if ( fFileLog )
	    {
	    gLog.DelServer( fFileLog );
	    delete fFileLog;
	    fFileLog = NULL;
	    }
	}
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SetupSysLogging()
    {
    if ( fSysLogging )
	{
	if ( !fSysLog )
	    {
	    fSysLog = new AliHLTSysLogServer( fLogName.c_str(), fSysLogFacility );
	    if ( !fSysLog )
		return false;
	    gLog.AddServer( fSysLog );
	    }
	}
    else
	{
	if ( fSysLog )
	    {
	    gLog.DelServer( fSysLog );
	    delete fSysLog;
	    fSysLog = NULL;
	    }
	}
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::RemoveLogging()
    {
    if ( fSysLog )
	{
	gLog.DelServer( fSysLog );
	delete fSysLog;
	fSysLog = NULL;
	}
    if ( fFileLog )
	{
	gLog.DelServer( fFileLog );
	delete fFileLog;
	fFileLog = NULL;
	}
    if ( fLogOut )
	{
	gLog.DelServer( fLogOut );
	delete fLogOut;
	fLogOut = NULL;
	}
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::ParseCmdLine()
    {
    int i, errorArg=-1;
    char* errorStr = NULL;
    
    i = 1;
    while ( (i < fArgc) && !errorStr )
	{
	errorStr = ParseCmdLine( fArgc, fArgv, i, errorArg );
	}
    if ( errorStr )
	{
	PrintUsage( errorStr, i, errorArg );
	return false;
	}
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE char* AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::ParseCmdLine( int argc, char** argv, int& i, int& errorArg )
    {
    char* cpErr;
    if ( !strcmp( argv[i], "-control" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing Status&Control interface address URL parameter";
	    }
	fSCInterfaceAddress = argv[i+1];
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-V" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing verbosity level specifier.";
	    }
	gLogLevel = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    errorArg = i+1;
	    return "Error converting verbosity level specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-nostdout" ) )
	{
	fStdoutLogging = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-nofilelog" ) )
	{
	fFileLogging = false;
	i++;
	return NULL;
	}
    if ( !strcmp( argv[i], "-syslog" ) )
	{
	if ( argc <= i+2 )
	    {
	    return "Missing syslog facility parameter";
	    }
	if ( !strcmp( argv[i+1], "DAEMON" ) )
	    {
	    fSysLogFacility = LOG_DAEMON;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL0" ) )
	    {
	    fSysLogFacility = LOG_LOCAL0;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL1" ) )
	    {
	    fSysLogFacility = LOG_LOCAL1;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL2" ) )
	    {
	    fSysLogFacility = LOG_LOCAL2;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL3" ) )
	    {
	    fSysLogFacility = LOG_LOCAL3;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL4" ) )
	    {
	    fSysLogFacility = LOG_LOCAL4;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL5" ) )
	    {
	    fSysLogFacility = LOG_LOCAL5;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL6" ) )
	    {
	    fSysLogFacility = LOG_LOCAL6;
	    }
	else if ( !strcmp( argv[i+1], "LOCAL7" ) )
	    {
	    fSysLogFacility = LOG_LOCAL7;
	    }
	else if ( !strcmp( argv[i+1], "USER" ) )
	    {
	    fSysLogFacility = LOG_USER;
	    }
	else
	    {
	    return "Unknown syslog facility parameter.";
	    }
	fSysLogging = true;
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-publishertimeout" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing publisher timeout specifier.";
	    }
	fPublisherTimeout = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting publisher timeout specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-publisher" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing publisher name specifier.";
	    }
	TSelf::PublisherData pd;
	pd.fOwnPublisherName = argv[i+1];
	pthread_mutex_lock( &fPublisherMutex );
	fPublishers.insert( fPublishers.end(), pd );
	pthread_mutex_unlock( &fPublisherMutex );
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-subscribe" ) )
	{
	if ( argc <= i+2 )
	    {
	    return "Missing publishername or subscriberid parameter";
	    }
	TSelf::SubscriberData sd;
	sd.fAttachedPublisherName = argv[i+1];
	sd.fSubscriberID = argv[i+2];
	pthread_mutex_lock( &fSubscriberMutex );
	fSubscribers.insert( fSubscribers.end(), sd );
	pthread_mutex_unlock( &fSubscriberMutex );
	i += 3;
	return NULL;
	}
    if ( !strcmp( argv[i], "-subscriberretrycount" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing susbcriber retry count specifier.";
	    }
	fSubscriberRetryCount = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting susbcriber retry count specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-subscriberretrytime" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing susbcriber retry time specifier.";
	    }
	fSubscriberRetryTime = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting susbcriber retry time specifier..";
	    }
	i += 2;
	return NULL;
	}
    if ( !strcmp( argv[i], "-eventsexp2" ) )
	{
	if ( argc <= i+1 )
	    {
	    return "Missing event count (power of 2) specifier.";
	    }
	fMaxPreEventCountExp2 = strtoul( argv[i+1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    return "Error converting event count (power of 2) specifier..";
	    }
	fMaxPreEventCount = 1 << fMaxPreEventCountExp2;
	i += 2;
	return NULL;
	}
    return "Unknown Parameter";
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::PrintUsage( const char* errorStr, int errorArg, int errorParam )
    {
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Usage" )
	<< "Error parsing command line arguments: " << errorStr << "." << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Usage" )
	<< "Offending argument: " << fArgv[errorArg] << "." << ENDLOG;
    if ( errorParam>=0 && errorParam<fArgc )
	{
	LOG( AliHLTLog::kError, "Controlled Processing Component", "Usage" )
	    << "Offending parameter: " << fArgv[errorParam] << "." << ENDLOG;
	}
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-control <status&control address URL>: Specify the address on which the Status&Control listens for remote supervision." << ENDLOG;
    if ( GetMaxPublisherCount()>0 )
	{
	char *multi1, *multi2;
	if ( GetMinPublisherCount()==0 )
	    multi1 = "Optional";
	else
	    multi1 = "Mandatory";
	if ( GetMaxPublisherCount()>1 )
	    multi2 = ", may be used multiple times";
	else
	    multi2 = "";
	LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	    << "-publisher <publisher-id>: Specifies the name/id to use for the publisher object. ("
	    << multi1 << multi2 << ")" << ENDLOG;
	}
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-V <verbosity>: Specifies the verbosity to use in the program. Set bits correspond to: 1 - benchmark, 2 - information, 4 - debug, 8 - warning, 16 - error, 32 - fatal error. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-nostdout: Inihibit logging to stdout. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-nofilelog: Inihibit logging to files. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-syslog <facility>: Send log messages to the syslog daemon. 'facility' can be one of the following values: DAEMON,USER,LOCAL0,LOCAL1,LOCAL2,LOCAL3,LOCAL4,LOCAL5,LOCAL6,LOCAL7. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-publishertimeout <publisher-timeout-ms>: Specifies a timeout to use in milliseconds for published events. (Optional)" << ENDLOG;
    if ( GetMaxSubscriberCount()>0 )
	{
	char *multi1, *multi2;
	if ( GetMinSubscriberCount()==0 )
	    multi1 = "Optional";
	else
	    multi1 = "Mandatory";
	if ( GetMaxSubscriberCount()>1 )
	    multi2 = ", may be used multiple times";
	else
	    multi2 = "";
	LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	    << "-subscribe <publishername> <subscriberid>: Specifies the name of the publisher to subscribe to and the subscriber id to use. ("
	    << multi1 << multi2 << ")" << ENDLOG;
	}
//     LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
// 	<< "-shmproxy <shmsize> <publisher-to-subscriber-shmkey> <subscriber-to-publisher-shmkey>: Use shared memory proxies for publisher subscriber communication using the given parameters. (Optional, default is named pipe communication)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-subscriberretrycount <max-retry-count>: Specify the maximum number of retries that new events are tried to be processed. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-subscriberretrytime <retry-time-in-milliseconds>: Specify the amount of time (in milliseconds) between of retries for new events to be processed. (Optional)" << ENDLOG;
    LOG( AliHLTLog::kError, "Controlled Processing Component", "Command line usage" )
	<< "-eventsexp2 <event-count-power-of-2>: Specify the power of two for the number of event slots to pre-allocate. (Optional)" << ENDLOG;
    }
	
template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::IllegalCmd( AliUInt32_t state, AliUInt32_t cmd )
    {
    LOG( AliHLTLog::kWarning, "AliHLTControlledDataFlowComponent::IllegalCmd", "Illegal Command" )
	<< "Illegal command 0x" << AliHLTLog::kHex << cmd << "/" << AliHLTLog::kDec
	<< cmd << " for state 0x" << AliHLTLog::kHex << state << "/" << AliHLTLog::kDec
	<< state << "." << ENDLOG;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SetupSC()
    {
    fStatus = CreateStatusData();
    if ( !fStatus )
	return false;
    if ( !SetupStatusData() )
	return false;

    fSCInterface = CreateSCInterface();
    if ( !fSCInterface )
	return false;
    if ( !SetupSCInterface() )
	return false;

    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE AliHLTStatus* AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CreateStatusData()
    {
    return new AliHLTStatus;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SetupStatusData()
    {
    fStatus->fProcessStatus.fState = 0;
    fStatus->fProcessStatus.fState |= kAliHLTPCStartedStateFlag;
//     fStatus->fProcessStatus.fState = kAliHLTPCStartedState;
    switch ( fComponentType )
	{
	case kMerger:
	    fStatus->fProcessStatus.fProcessType = kAliHLTSCEventMergerComponentType;
	    break;
	case kScatterer:
	    fStatus->fProcessStatus.fProcessType = kAliHLTSCEventScattererComponentType;
	    break;
	case kGatherer:
	    fStatus->fProcessStatus.fProcessType = kAliHLTSCEventGathererComponentType;
	    break;
	case kPublisherBridgeHead:
	    fStatus->fProcessStatus.fProcessType = kAliHLTSCControlledBridgeComponentType;
	    break;
	case kSubscriberBridgeHead:
	    fStatus->fProcessStatus.fProcessType = kAliHLTSCControlledBridgeComponentType;
	    break;
	default:
	    fStatus->fProcessStatus.fProcessType = kAliHLTSCUndefinedComponentType;
	    break;
	}
    fStatus->Touch();
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE AliHLTSCInterface* AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CreateSCInterface()
    {
    return new AliHLTSCInterface();
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE bool AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SetupSCInterface()
    {
    int ret;
    ret = fSCInterface->Bind( fSCInterfaceAddress.c_str() );
    if ( ret )
	{
	return false;
	}
    ret = fSCInterface->Start();
    if ( ret )
	{
	return false;
	}
    fSCInterface->SetStatusData( &(fStatus->fHeader) );
    fSCInterface->AddCommandProcessor( &fCmdProcessor );
    return true;
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::RemoveSC()
    {
    if ( fSCInterface )
	{
	fSCInterface->Stop();
	fSCInterface->Unbind();
	delete fSCInterface;
	fSCInterface = NULL;
	}
    if ( fStatus )
	{
	delete fStatus;
	fStatus = NULL;
	}
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::ProcessSCCmd( AliHLTSCCommandStruct* cmdS )
    {
    bool ok = false;
    AliHLTSCCommand cmd( cmdS );
    if ( !fStatus )
	{
	// XXX
	return;
	}
    switch ( cmd.GetData()->fCmd )
	{
	case kAliHLTPCEndProgramCmd:
	    if ( !(fStatus->fProcessStatus.fState & ~(kAliHLTPCStartedStateFlag|kAliHLTPCErrorStateFlag)) )
		{
		ok = true;
		fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
		fCmdSignal.Signal();
		}
	    break;
	case kAliHLTPCConfigureCmd:
	    if ( fStatus->fProcessStatus.fState & ~kAliHLTPCStartedStateFlag )
		break;
	    ok = true;
	    ExtractConfig( cmdS );
	    Configure();
	    break;
	case kAliHLTPCUnconfigureCmd:
	    if ( fStatus->fProcessStatus.fState & ~(kAliHLTPCStartedStateFlag|kAliHLTPCConfiguredStateFlag|kAliHLTPCErrorStateFlag) )
		break;
	    ok = true;
	    Unconfigure();
	    fStatus->fProcessStatus.fState = fStatus->fProcessStatus.fState & ~(kAliHLTPCConfiguredStateFlag|kAliHLTPCErrorStateFlag);
	    break;
	case kAliHLTPCResetConfigurationCmd:
	    if ( fStatus->fProcessStatus.fState & ~kAliHLTPCStartedStateFlag )
		break;
	    ok = true;
	    ResetConfiguration();
	    break;
	case kAliHLTPCAcceptSubscriptionsCmd:
	    if ( GetMinPublisherCount()<=0 )
		break;
	    if ( fStatus->fProcessStatus.fState & ~(kAliHLTPCStartedStateFlag|kAliHLTPCConfiguredStateFlag|kAliHLTPCSubscribedStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCStopSubscriptionsCmd:
	    if ( GetMinPublisherCount()<=0 )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) ||
		 (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCCancelSubscriptionsCmd:
	    if ( GetMinPublisherCount()<=0 )
		break;
	    if ( (fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) ||
		 (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCSubscribeCmd:
	    if ( GetMinSubscriberCount()<=0 )
		break;
	    if ( fStatus->fProcessStatus.fState & ~(kAliHLTPCStartedStateFlag|kAliHLTPCConfiguredStateFlag|kAliHLTPCAcceptingSubscriptionsStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCUnsubscribeCmd:
	    if ( GetMinSubscriberCount()<=0 )
		break;
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) ||
		 (fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCStartProcessingCmd:
	    {
	    if ( GetMinPublisherCount()>0 )
		{
		if ( (fStatus->fProcessStatus.fState & kAliHLTPCAcceptingSubscriptionsStateFlag) &&
		     !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		    ok = true;
		}
	    if ( GetMinSubscriberCount()>0 )
		{
		if ( (fStatus->fProcessStatus.fState & kAliHLTPCSubscribedStateFlag) &&
		     !(fStatus->fProcessStatus.fState & kAliHLTPCErrorStateFlag) )
		    ok = true;
		}
	    if ( ok )
		{
		fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
		fCmdSignal.Signal();
		}
	    break;
	    }
	case kAliHLTPCPauseProcessingCmd:
// 	    if ( fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag )
// 		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCResumeProcessingCmd:
// 		if ( fStatus->fProcessStatus.fState & kAliHLTPCPausedStateFlag )
// 		    break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	case kAliHLTPCStopProcessingCmd:
	    if ( !(fStatus->fProcessStatus.fState & kAliHLTPCRunningStateFlag) )
		break;
	    ok = true;
	    fCmdSignal.AddNotificationData( (AliUInt64_t)cmd.GetData()->fCmd );
	    fCmdSignal.Signal();
	    break;
	}
    if ( !ok )
	IllegalCmd( fStatus->fProcessStatus.fState, cmd.GetData()->fCmd );
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::ExtractConfig( AliHLTSCCommandStruct* cmd )
    {
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE AliHLTDescriptorHandler* AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CreateDescriptorHandler()
    {
//     return new AliHLTDescriptorHandler( fMinPreBlockCount, fMaxPreBlockCount, fMaxPreEventCountExp2 );
    return new AliHLTDescriptorHandler( fMinPreBlockCount, fMaxPreBlockCount, (fMaxPreEventCountExp2>=0 ? fMaxPreEventCountExp2 : 4) );
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE AliHLTShmManager* AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CreateShmManager()
    {
    return new AliHLTShmManager( fMaxNoUseCount );
    }


template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE typename AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SubscriptionListenerThread* AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CreateSubscriptionListenerThread( unsigned long pubNdx, TSelf::PublisherData* pd )
    {
    return new SubscriptionListenerThread( this, pd->fPublisher );
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE AliHLTPublisherProxyInterface* AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CreatePublisherProxy( unsigned long pubNdx, TSelf::SubscriberData* sd )
    {
//     if ( fProxyShmSize )
// 	return new AliHLTPublisherShmProxy( fAttachedPublisherName.c_str(), fProxyShmSize, fPub2SubProxyShmKey, fSub2PubProxyShmKey );
//     else
    return new AliHLTPublisherPipeProxy( sd->fAttachedPublisherName.c_str() );
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE typename AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SubscriptionThread* AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CreateSubscriptionThread( unsigned long subNdx, TSelf::SubscriberData* sd )
    {
    return new SubscriptionThread( this, sd->fPublisherProxy, sd->fSubscriber );
    }
	
template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SetupComponents()
    {
    typename vector<TSelf::PublisherData>::iterator pubIter, pubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    while ( pubIter != pubEnd )
	{
	pubIter->fPublisher->SetMaxTimeout( fPublisherTimeout );
	pubIter->fPublisher->SetSubscriptionLoop( &PublisherPipeSubscriptionInputLoop );
	pubIter->fPublisher->AddSubscriberEventCallback( &fSubscriberEventCallback );
	pubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Subscribe()
    {
    typename vector<TSelf::SubscriberData>::iterator subIter, subEnd;
    pthread_mutex_lock( &fSubscriberMutex );
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    while ( subIter != subEnd )
	{
	int ret;
	ret = subIter->fPublisherProxy->Subscribe( *(subIter->fSubscriber) );
	if ( ret )
	    {
	    fStatus->fProcessStatus.fState |= kAliHLTPCConsumerErrorStateFlag;
	    LOG( AliHLTLog::kError, "AliHLTControlledDataFlowComponent::Subscribe", "Error subscribing" )
		<< "Error subscribing subscriber '" << subIter->fSubscriber->GetName() << "' to publisher '"
		<< subIter->fPublisherProxy->GetName() << "': " << strerror(ret) << " ("
		<< MLUCLog::kDec << ret << ")." << ENDLOG;
	    pthread_mutex_unlock( &fSubscriberMutex );
	    fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	    return;
	    }
	subIter->fSubscriptionThread->Start();
// 	fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	subIter++;
	}
    pthread_mutex_unlock( &fSubscriberMutex );
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Unsubscribe()
    {
    typename vector<TSelf::SubscriberData>::iterator subIter, subEnd;
    pthread_mutex_lock( &fSubscriberMutex );
    subIter = fSubscribers.begin();
    subEnd = fSubscribers.end();
    unsigned long n=0;
    while ( subIter != subEnd )
	{
	subIter->fSubscriptionThread->Quit();
	n = 0;
	while ( !subIter->fSubscriptionThread->HasQuitted() && n++<100 )
	    usleep( 10000 );
	if ( !subIter->fSubscriptionThread->HasQuitted() )
	    {
	    LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::Unsubscribe", "Aborting Publishing Loop" )
		<< "Publishing loop for subscriber '" << subIter->fSubscriber->GetName() << "' and publisher '"
		<< subIter->fPublisherProxy->GetName() << "' has to be aborted." << ENDLOG;
	    subIter->fSubscriptionThread->AbortPublishing();
	    }
	while ( !subIter->fSubscriptionThread->HasQuitted() && n++<100 )
	    usleep( 10000 );
	if ( !subIter->fSubscriptionThread->HasQuitted() )
	    {
	    LOG( AliHLTLog::kInformational, "AliHLTControlledDataFlowComponent::Unsubscribe", "Aborting Subscription Thread" )
		<< "Subscription thread loop for subscriber '" << subIter->fSubscriber->GetName() << "' and publisher '"
		<< subIter->fPublisherProxy->GetName() << "' has to be aborted." << ENDLOG;
	    subIter->fSubscriptionThread->Abort();
	    }
	subIter->fSubscriptionThread->Join();
	subIter++;
	}
    pthread_mutex_unlock( &fSubscriberMutex );
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::AcceptSubscriptions()
    {
    typename vector<TSelf::PublisherData>::iterator pubIter, pubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    while ( pubIter != pubEnd )
	{
	if ( pubIter->fSubscriptionListenerThread )
	    pubIter->fSubscriptionListenerThread->Start();
	else
	    {
	    fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	    fStatus->fProcessStatus.fState |= kAliHLTPCErrorStateFlag;
	    break;
	    }
	pubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::StopSubscriptions()
    {
    typename vector<TSelf::PublisherData>::iterator pubIter, pubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    while ( pubIter != pubEnd )
	{
	if ( pubIter->fSubscriptionListenerThread )
	    {
	    pubIter->fSubscriptionListenerThread->Quit();
	    }
	pubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::CancelSubscriptions()
    {
    typename vector<TSelf::PublisherData>::iterator pubIter, pubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    while ( pubIter != pubEnd )
	{
	if ( pubIter->fPublisher )
	    {
	    pubIter->fPublisher->CancelAllSubscriptions();
	    }
	pubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    }


template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Subscription( AliHLTSamplePublisher* pub )
    {
    typename vector<TSelf::PublisherData>::iterator pubIter, pubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    while ( pubIter != pubEnd )
	{
	if ( pubIter->fPublisher == pub )
	    {
	    pubIter->fSubscriberCount++;
	    break;
	    }
	pubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    if ( fStatus )
	fStatus->fProcessStatus.fState |= kAliHLTPCWithSubscribersStateFlag;    
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::Unsubscription( AliHLTSamplePublisher* pub )
    {
    bool found=false;
    typename vector<TSelf::PublisherData>::iterator pubIter, pubEnd;
    pthread_mutex_lock( &fPublisherMutex );
    pubIter = fPublishers.begin();
    pubEnd = fPublishers.end();
    while ( pubIter != pubEnd )
	{
	if ( pubIter->fPublisher == pub )
	    pubIter->fSubscriberCount--;
	if ( pubIter->fSubscriberCount )
	    found = true;
	pubIter++;
	}
    pthread_mutex_unlock( &fPublisherMutex );
    if ( !found && fStatus )
	fStatus->fProcessStatus.fState &= ~kAliHLTPCWithSubscribersStateFlag;    
    }

template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SubscriptionListenerThread::Run()
    {
    fQuitted = false;
    bool found=false;
    if ( fPub )
	{
	typename vector<TSelf::PublisherData>::iterator pubIter, pubEnd;
	pthread_mutex_lock( &fComp->fPublisherMutex );
	pubIter = fComp->fPublishers.begin();
	pubEnd = fComp->fPublishers.end();
	while ( pubIter != pubEnd )
	    {
	    if ( pubIter->fPublisher == fPub )
		pubIter->fSubscriptionListenerLoopRunning = true;
	    if ( !pubIter->fSubscriptionListenerLoopRunning )
		found = true;
	    pubIter++;
	    }
	pthread_mutex_unlock( &fComp->fPublisherMutex );


	if ( !found )
	    {
	    fComp->fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	    }
	fPub->AcceptSubscriptions();

	// XXX Error detection has to be more elaborate...
	if ( !(fComp->fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) )
	    fComp->fStatus->fProcessStatus.fState |= kAliHLTPCProducerErrorStateFlag;
	}
    fQuitted = true;
    }
		
template<class BaseClass, class PublisherClass, class SubscriberClass>
INLINE void AliHLTControlledDataFlowComponent<BaseClass,PublisherClass,SubscriberClass>::SubscriptionThread::Run()
    {
    fQuitted = false;
    bool found=false;
    if ( fProxy && fSubscriber )
	{
	typename vector<TSelf::SubscriberData>::iterator subIter, subEnd;
	pthread_mutex_lock( &fComp->fSubscriberMutex );
	subIter = fComp->fSubscribers.begin();
	subEnd = fComp->fSubscribers.end();
	while ( subIter != subEnd )
	    {
	    if ( subIter->fSubscriber == fSubscriber )
		{
		subIter->fSubscriptionLoopRunning = true;
		}
	    if ( !subIter->fSubscriptionLoopRunning )
		{
		found = true;
		}
	    subIter++;
	    }
	pthread_mutex_unlock( &fComp->fSubscriberMutex );


	if ( !found )
	    {
	    fComp->fStatus->fProcessStatus.fState &= ~kAliHLTPCChangingStateFlag;
	    }
	fProxy->StartPublishing( *fSubscriber );

	// XXX Error detection has to be more elaborate...
	if ( !(fComp->fStatus->fProcessStatus.fState & kAliHLTPCChangingStateFlag) )
	    fComp->fStatus->fProcessStatus.fState |= kAliHLTPCConsumerErrorStateFlag;
	}
    fQuitted = true;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
