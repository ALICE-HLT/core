/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3BRIDGEMSGS_H_
#define _ALIL3BRIDGEMSGS_H_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"

const AliUInt32_t kAliHLTBridgeEmptyMsg = 0;
const AliUInt32_t kAliHLTBridgeConnectMsg = 1;
const AliUInt32_t kAliHLTBridgeDisconnectMsg = 2;
const AliUInt32_t kAliHLTBridgeNewEventMsg = 3;
const AliUInt32_t kAliHLTBridgeEventCanceledMsg = 5;
const AliUInt32_t kAliHLTBridgeSubscriptionCanceledMsg = 6;
const AliUInt32_t kAliHLTBridgeReleaseEventsRequestMsg = 7;
const AliUInt32_t kAliHLTBridgeEventDoneMsg = 8;
const AliUInt32_t kAliHLTBridgeEventDoneForwardMsg = 9;
const AliUInt32_t kAliHLTBridgeTransferCanceledMsg = 10;
const AliUInt32_t kAliHLTBridgeGlobalTransferIDMsg = 11;


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif /* _ALIL3BRIDGEMSGS_H_ */
