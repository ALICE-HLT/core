/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3EVENTDISPATCHER_HPP_
#define _ALIL3EVENTDISPATCHER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliEventID.h"
#include "AliHLTEventScattererRePublisher.hpp"
#include "AliHLTEventScatterer.hpp"
#include <pthread.h>
#include <vector>

#if __GNUC__>=3
using namespace std;
#endif


class AliHLTEventScattererSubscriber;

class AliHLTEventScatterer
    {
    public:

	AliHLTEventScatterer();
	virtual ~AliHLTEventScatterer();

	virtual void AddPublisher( AliHLTEventScattererRePublisher* publisher );

	void SetSubscriber( AliHLTEventScattererSubscriber* subscriber )
		{
		fSubscriber = subscriber;
		}

	virtual int NewEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, const AliHLTEventTriggerStruct& trigger );
	virtual int EventDone( AliHLTEventDoneData* eventDoneData, AliHLTEventScattererRePublisher* publisher );
	virtual int EventCanceled( AliEventID_t eventID ) = 0;
	virtual int RequestEventRelease() = 0;
	virtual int CancelAllSubscriptions() = 0;
	virtual int PublisherDown( AliHLTEventScattererRePublisher* publisher ) = 0;
	virtual int PublisherSendError( AliHLTEventScattererRePublisher* publisher, AliEventID_t eventID ) = 0;

    protected:

	AliHLTEventScattererSubscriber *fSubscriber;

	// Called with fPublisherMutex locked!!
	virtual int AnnounceEvent( AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, const AliHLTEventTriggerStruct& trigger ) = 0;

	vector<AliHLTEventScattererRePublisher*> fPublishers;
	AliUInt32_t fPublisherCount;
	pthread_mutex_t fPublisherMutex;

	// Callbacks for derived classes. empty here.
	// Called with fPublisherMutex locked.
	virtual void PublisherAdded( AliHLTEventScattererRePublisher* publisher );
	virtual void ReleasingEvent( AliEventID_t eventID, AliHLTEventScattererRePublisher* publisher );

	

    private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3EVENTDISPATCHER_HPP_
