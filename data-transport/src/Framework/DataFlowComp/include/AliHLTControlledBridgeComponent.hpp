#ifndef _ALIHLTCONTROLLEDBRIDGECOMPONENT_HPP_
#define _ALIHLTCONTROLLEDBRIDGECOMPONENT_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliHLTPublisherProxyInterface.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTSCCommandProcessor.hpp"
#include "AliHLTSubscriberBridgeHead.hpp"
#include "AliHLTPublisherBridgeHead.hpp"
#include "AliHLTTriggerStackMachineAssembler.hpp"
#include "MLUCString.hpp"
#include "MLUCConditionSem.hpp"
#include "BCLErrorLogCallback.hpp"
#include "BCLStackTraceCallback.hpp"
#include "MLUCSysMESLogServer.hpp"
#include "MLUCDynamicLibraryLogServer.hpp"
#include <signal.h>


class AliHLTBridgeStatusData;
class AliHLTSCInterface;
struct AliHLTSCCommandStruct;
class AliHLTBufferManager;
class AliHLTDescriptorHandler;
class AliHLTShmManager;
class AliHLTEventAccounting;
class AliHLTTriggerStackMachine;

template<class BridgeHeadClass>
class AliHLTControlledBridgeComponent
    {
    public:

	typedef AliHLTControlledBridgeComponent<BridgeHeadClass> TSelf;

	virtual ~AliHLTControlledBridgeComponent();

	void SetMinPreBlockCount( AliUInt32_t minPreBlockCount )
		{
		fMinPreBlockCount = minPreBlockCount;
		}

	void SetMaxPreBlockCount( AliUInt32_t maxPreBlockCount )
		{
		fMaxPreBlockCount = maxPreBlockCount;
		}

	void SetMaxNoUseCount( AliUInt32_t maxNoUseCount )
		{
		fMaxNoUseCount = maxNoUseCount;
		}

	virtual void Run();

    protected:

	class CommandProcessor;
	    friend class CommandProcessor;
	class CommandProcessor: public AliHLTSCCommandProcessor
	    {
	    public:
		CommandProcessor( AliHLTControlledBridgeComponent<BridgeHeadClass>* comp ):
		    fComp( comp )
			{
			}
		virtual ~CommandProcessor() {};
		virtual void ProcessCmd( AliHLTSCCommandStruct* cmd )
			{
			fComp->ProcessSCCmd( cmd );
			}
	    protected:
		AliHLTControlledBridgeComponent<BridgeHeadClass>* fComp;
	    };

// 	    friend class CommandProcessor;

	enum AliHLTControlledBridgeComponentType { kUnknown = 0, kPublisherBridgeHead = 1, kSubscriberBridgeHead = 2 };

	AliHLTControlledBridgeComponent( AliHLTControlledBridgeComponentType compType, const char* compName, int argc, char** argv );

	virtual bool ExecCmd( AliUInt32_t cmd, bool& quit );

	virtual void ResetConfiguration();

	virtual bool PreConfigure();
	virtual bool PostUnconfigure();

	virtual bool CheckConfiguration();
	virtual void ShowConfiguration();

	virtual void Configure();
	virtual void Unconfigure();

	virtual bool SetupStdLogging();
 	virtual bool SetupFileLogging();
 	virtual bool SetupSysLogging();
 	virtual bool SetupSysMESLogging();
	virtual bool SetupDynLibLogging();
	virtual void RemoveLogging();

	virtual bool ParseCmdLine();
	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );

	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam, int argc=-1, char** argv=NULL );
	
	virtual void IllegalCmd( AliUInt32_t state, AliUInt32_t cmd );

	virtual bool SetupSC();
	virtual AliHLTBridgeStatusData* CreateStatusData();
	virtual bool SetupStatusData();
	virtual AliHLTSCInterface* CreateSCInterface();
	virtual bool SetupSCInterface();
	virtual void RemoveSC();
	virtual void LookAtMe();

	virtual void ProcessSCCmd( AliHLTSCCommandStruct* cmd );
	virtual void ExtractConfig( AliHLTSCCommandStruct* cmd );

	virtual void StartProcessing() = 0;
	virtual void PauseProcessing() = 0;
	virtual void ResumeProcessing() = 0;
	virtual void StopProcessing() = 0;

 	virtual AliHLTDescriptorHandler* CreateDescriptorHandler();
	virtual AliHLTShmManager* CreateShmManager();

	virtual void StartMsgLoop() = 0;
	virtual void StopMsgLoop() = 0;

	virtual void SetupComponents();

	virtual void Bind();
	virtual void Unbind();
	virtual void Connect();
	virtual void Disconnect( const char* newMsgAddress = NULL, const char* newBlobMsgAddress = NULL );
	virtual void Reconnect( const char* newMsgAddress = NULL, const char* newBlobMsgAddress = NULL );

	AliHLTControlledBridgeComponentType fComponentType;
	const char* fComponentTypeName;
	
	const char* fComponentName;

	int fArgc;
	char** fArgv;

	BridgeHeadClass* fBridgeHead;

	MLUCConditionSem<AliUInt64_t> fCmdSignal;

	bool fStdoutLogging;
	MLUCString fLogName;
	AliHLTFilteredStdoutLogServer *fLogOut;
	
	bool fFileLogging;
	AliHLTFileLogServer* fFileLog;

	bool fSysLogging;
	AliHLTSysLogServer* fSysLog;
	int fSysLogFacility;

	bool fSysMESLogging;
	MLUCSysMESLogServer* fSysMESLog;

	bool fDynLibLogging;
        MLUCDynamicLibraryLogServer* fDynLibLog;
        MLUCString fDynLibPath;

	struct LAMAddressData
	    {
		MLUCString fStr;
		BCLAbstrAddressStruct* fAddress;
	    };

	AliHLTBridgeStatusData* fStatus;
	MLUCString fSCInterfaceAddress;
	AliHLTSCInterface* fSCInterface;
	vector<LAMAddressData> fLAMAddresses;
	pthread_mutex_t fLAMAddressMutex;

	CommandProcessor fCmdProcessor;

	AliUInt32_t fMaxPreEventCount;
	int fMaxPreEventCountExp2;
	AliUInt32_t fMinPreBlockCount;
	AliUInt32_t fMaxPreBlockCount;
	AliUInt32_t fMaxNoUseCount;

 	AliHLTDescriptorHandler* fDescriptorHandler;
	AliHLTShmManager* fShmManager;

	class BridgeHeadLAM;
	    friend class BridgeHeadLAM;
	class BridgeHeadLAM: public AliHLTBridgeHeadLAMProxy
	    {
	    public:
		BridgeHeadLAM( TSelf* comp ):
		    fComp( comp )
			{
			}
		virtual ~BridgeHeadLAM() {};
		virtual void LookAtMe()
			{
			fComp->LookAtMe();
			}
	    protected:
		TSelf* fComp;
	    };

	BridgeHeadLAM fLAM;

	MLUCString fOwnMsgAddressStr;
	MLUCString fRemoteMsgAddressStr;
	MLUCString fOwnBlobMsgAddressStr;
	MLUCString fRemoteBlobMsgAddressStr;
	MLUCString fOwnBlobAddressStr;
	BCLMsgCommunication* fBlobMsgCom;
	BCLMsgCommunication* fMsgCom;
	BCLBlobCommunication* fBlobCom;
	BCLAbstrAddressStruct *fOwnBlobMsgAddress, 
				 *fOwnBlobAddress, 
				  *fOwnMsgAddress,
			   *fRemoteBlobMsgAddress,
			       *fRemoteMsgAddress;

	
	uint32 fBlobBufferSize;
	uint32 fCommunicationTimeout; // Timeout of 10s, given in ms.

	BCLErrorLogCallback fLogCallback;
	BCLStackTraceCallback fTraceCallback;

	class BridgeHeadErrorCallback;
	    friend class BridgeHeadErrorCallback;
	class BridgeHeadErrorCallback: public AliHLTBridgeHeadErrorCallback<BridgeHeadClass>
	    {
	    public:
		BridgeHeadErrorCallback( TSelf* comp );
		virtual ~BridgeHeadErrorCallback();
		virtual void ConnectionError( BridgeHeadClass* bridgeHead );
		virtual void ConnectionEstablished( BridgeHeadClass* bridgeHead );
	    protected:
		TSelf* fComp;
	    private:
	    };
	BridgeHeadErrorCallback fErrorCallback;

	bool fAliceHLT;

	AliHLTEventAccounting* fEventAccounting;

	unsigned long fEventModuloPreDivisor;

	AliHLTTriggerStackMachine* fTriggerStackMachine;

	std::vector<MLUCString> fEventTypeStackMachineCodesText;

	std::vector<AliHLTEventTriggerStruct*> fEventTypeStackMachineCodes;

	AliHLTTriggerStackMachineAssembler fStackMachineAssembler;

	std::map<MLUCString,unsigned> fTriggerClassBitIndices;

	std::map<MLUCString,MLUCString> fTriggerClassGroups;

	AliUInt32_t fSubscriptionEventModulo;
	bool fUseSubscriptionEventModulo;

	unsigned long fRunNumber;
	unsigned long fBeamType;
	unsigned long fHLTMode;
	MLUCString fRunType;

	struct sigaction fOldSigAction;

    };



class AliHLTControlledPublisherBridgeHeadComponent: public AliHLTControlledBridgeComponent<AliHLTPublisherBridgeHead>
    {
    public:

	AliHLTControlledPublisherBridgeHeadComponent( const char* compName, int argc, char** argv );
	virtual ~AliHLTControlledPublisherBridgeHeadComponent();

    protected:

	typedef AliHLTControlledBridgeComponent<AliHLTPublisherBridgeHead> TParent;

	class SubscriptionListenerThread;

	virtual bool ExecCmd( AliUInt32_t cmd, bool& quit );

	class SubscriberEventCallback;
	    friend class SubscriberEventCallback;
	class SubscriberEventCallback: public AliHLTSamplePublisher::SubscriberEventCallback
	    {
	    public:
		SubscriberEventCallback( AliHLTControlledPublisherBridgeHeadComponent* comp ):
		    fComp( comp )
			{
			}
		virtual ~SubscriberEventCallback() {}
		virtual void Subscribed( AliHLTSamplePublisher* pub, const char*, AliHLTSubscriberInterface* )
			{
			fComp->Subscription( pub );
			}
		virtual void Unsubscribed( AliHLTSamplePublisher* pub, const char*, AliHLTSubscriberInterface*, bool )
			{
			fComp->Unsubscription( pub );
			}
	    protected:
		AliHLTControlledPublisherBridgeHeadComponent* fComp;
	    };

	virtual void ProcessSCCmd( AliHLTSCCommandStruct* cmd );
	virtual void ExtractConfig( AliHLTSCCommandStruct* cmd );

	virtual void StartMsgLoop();
	virtual void StopMsgLoop();

	virtual void StartProcessing();
	virtual void PauseProcessing();
	virtual void ResumeProcessing();
	virtual void StopProcessing();

	virtual bool CheckConfiguration();
	virtual void ShowConfiguration();

	virtual void ResetConfiguration();
	virtual void Configure();
	virtual void Unconfigure();

	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam, int argc=-1, char** argv=NULL );

	virtual void AcceptSubscriptions();
	virtual void StopSubscriptions();
	virtual void CancelSubscriptions();
// 	virtual void SubscriptionLoop();
// 	virtual bool QuitSubscriptionLoop();

	virtual void SetupComponents();

	virtual void Bind();

	virtual AliHLTPublisherBridgeHead* CreatePublisher();
	virtual SubscriptionListenerThread* CreateSubscriptionListenerThread();

	AliHLTSharedMemory* CreateShm();

	virtual void PublisherMsgLoop();
	virtual bool StopPublisherMsgLoop();


	void Subscription( AliHLTSamplePublisher* pub );
	void Unsubscription( AliHLTSamplePublisher* pub );

// 	AliHLTPublisherBridgeHead* fPublisher;
	MLUCString fOwnPublisherName;
	AliUInt32_t fSubscriberCount;
	AliUInt32_t fPublisherTimeout;
	SubscriberEventCallback fSubscriberEventCallback;
	bool fSubscriptionListenerLoopRunning;
	SubscriptionListenerThread* fSubscriptionListenerThread;

	pthread_mutex_t fSubscriberRemovalListMutex;
	vector<MLUCString> fSubscriberRemovalList;

	AliHLTObjectThread<AliHLTControlledPublisherBridgeHeadComponent> fMsgLoopThread;
	bool fPublisherMsgLoopRunning;

	AliHLTShmID fShmKey;
	AliUInt32_t fShmID;
	bool fShmAddCopy;
	AliHLTSharedMemory* fShm;
	AliUInt8_t* fBlobPtr;

	class SubscriptionListenerThread;
	    friend class SubscriptionListenerThread;
	class SubscriptionListenerThread: public AliHLTThread
	    {
	    public:
		SubscriptionListenerThread( AliHLTControlledPublisherBridgeHeadComponent* comp, AliHLTSamplePublisher* pub  ):
		    fPub( pub ), fComp( comp )
			{
			fQuitted = true;
			}
		virtual ~SubscriptionListenerThread() {};
		bool HasQuitted()
			{
			return fQuitted;
			}
		void Quit()
			{
			if ( fPub )
			    QuitPipeSubscriptionLoop( *fPub );
			}
	    protected:
		virtual void Run();
		
		AliHLTSamplePublisher* fPub;
		AliHLTControlledPublisherBridgeHeadComponent* fComp;
		bool fQuitted;
	    };



    };

class AliHLTControlledSubscriberBridgeHeadComponent: public AliHLTControlledBridgeComponent<AliHLTSubscriberBridgeHead>
    {
    public:

	AliHLTControlledSubscriberBridgeHeadComponent( const char* compName, int argc, char** argv );
	virtual ~AliHLTControlledSubscriberBridgeHeadComponent();

    protected:

	typedef AliHLTControlledBridgeComponent<AliHLTSubscriberBridgeHead> TParent;

	class SubscriptionThread;

	virtual bool ExecCmd( AliUInt32_t cmd, bool& quit );

	virtual void ProcessSCCmd( AliHLTSCCommandStruct* cmd );
	virtual void ExtractConfig( AliHLTSCCommandStruct* cmd );

	virtual void StartMsgLoop();
	virtual void StopMsgLoop();

	virtual void StartProcessing();
	virtual void PauseProcessing();
	virtual void ResumeProcessing();
	virtual void StopProcessing();

	virtual bool CheckConfiguration();
	virtual void ShowConfiguration();

	virtual void ResetConfiguration();
	virtual void Configure();
	virtual void Unconfigure();

	virtual void SetupComponents();

	virtual AliHLTPublisherProxyInterface* CreatePublisherProxy();
	virtual AliHLTSubscriberBridgeHead* CreateSubscriber();

	virtual AliHLTBufferManager* CreateBufferManager();

	virtual const char* ParseCmdLine( int argc, char** argv, int& i, int& errorArg );
	virtual void PrintUsage( const char* errorStr, int errorArg, int errorParam, int argc=-1, char** argv=NULL );

// 	virtual void SubscriptionLoop();
// 	virtual bool QuitSubscriptionLoop();
	virtual void Subscribe();
	virtual void Unsubscribe();

	virtual void SubscriberMsgLoop();
	virtual bool StopSubscriberMsgLoop();

	
// 	SubscriberClass* fSubscriber;
	MLUCString fAttachedPublisherName;
	AliHLTPublisherProxyInterface* fPublisherProxy;
	MLUCString fSubscriberID;
	SubscriptionThread* fSubscriptionThread;
	bool fSubscriptionLoopRunning;

	bool fSubscriptionRequestOldEvents;

	AliHLTBufferManager* fBufferManager;
	enum TBufferManagerType { kLargestBlockBufferManager, kRingBufferManager };
	TBufferManagerType fBufferManagerType;
	AliUInt32_t fMinBlockSize;
	AliUInt32_t fRetryTime;

	unsigned long fSubscriberRetryCount;
	unsigned long fSubscriberRetryTime;


	AliHLTObjectThread<AliHLTControlledSubscriberBridgeHeadComponent> fMsgLoopThread;
	bool fSubscriberMsgLoopRunning;

	class SubscriptionThread;
	    friend class SubscriptionThread;
	class SubscriptionThread: public AliHLTThread
	    {
	    public:
		SubscriptionThread( AliHLTControlledSubscriberBridgeHeadComponent* comp, AliHLTPublisherProxyInterface* proxy, AliHLTSubscriberBridgeHead* sub ):
		    fProxy( proxy ), fSubscriber( sub ), fComp( comp )
			{
			fQuitted = true;
			}
		virtual ~SubscriptionThread() {};
		bool HasQuitted()
			{
			return fQuitted;
			}
		void Quit()
			{
			if ( fProxy )
			    fProxy->Unsubscribe( *fSubscriber );
			}
		void AbortPublishing()
			{
			if ( fProxy )
			    fProxy->AbortPublishing();
			}
	    protected:
		virtual void Run();

		AliHLTPublisherProxyInterface* fProxy;
		AliHLTSubscriberBridgeHead* fSubscriber;
		AliHLTControlledSubscriberBridgeHeadComponent* fComp;
		bool fQuitted;
	    };

    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTCONTROLLEDBRIDGECOMPONENT_HPP_
