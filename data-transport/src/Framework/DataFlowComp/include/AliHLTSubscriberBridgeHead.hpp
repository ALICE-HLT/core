/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3SUBSCRIBERBRIDGEHEAD_HPP_
#define _ALIL3SUBSCRIBERBRIDGEHEAD_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSubscriberInterface.hpp"
#include "AliHLTSharedMemory.hpp"
#include "AliHLTBridgeHeadHelper.hpp"
#include "BCLMsgCommunication.hpp"
#include "BCLBlobCommunication.hpp"
#include "BCLAbstrAddress.hpp"
#include "AliHLTBufferManager.hpp"
#include "AliHLTDescriptorHandler.hpp"
#include "AliHLTThread.hpp"
#include "AliHLTTimer.hpp"
#include "AliHLTSCProcessControlStates.hpp"
#include "MLUCVector.hpp"
#include "MLUCAllocCache.hpp"
#include "MLUCDynamicAllocCache.hpp"
#include "MLUCConditionSem.hpp"
#include "AliHLTBridgeStatusData.hpp"
#include "AliHLTBridgeData.hpp"
#include "AliHLTBridgeBlock.hpp"
#include "AliHLTBridgeHelperData.hpp"

class AliHLTEventAccounting;

class AliHLTSubscriberBridgeHead: public AliHLTSubscriberInterface
    {
    public:
  
	AliHLTSubscriberBridgeHead( const char* name, AliUInt32_t maxShmNoUseCount, int slotCountExp2 = -1 );
	~AliHLTSubscriberBridgeHead();

	void SetDescriptorHandler( AliHLTDescriptorHandler* descriptHand )
		{
		fDescriptorHandler = descriptHand;
		}

	void SetBufferManager( AliHLTBufferManager* bufferMan )
		{
		fBufferManager = bufferMan;
		}

	void SetMsgCom( BCLMsgCommunication* msgCom, BCLAbstrAddressStruct* remoteMsgAddress )
		{
		if ( fReceiveAddr && fMsgCom )
		    {
		    fMsgCom->DeleteAddress( fReceiveAddr );
		    fReceiveAddr = NULL;
		    }
		fMsgCom = msgCom;
		fRemoteMsgAddress = remoteMsgAddress;
		if ( fMsgCom )
		    fReceiveAddr = fMsgCom->NewAddress();
		}
	void SetRemoteMsgAddress( BCLAbstrAddressStruct* remoteMsgAddress )
		{
		fRemoteMsgAddress = remoteMsgAddress;
		}
	void SetBlobCom( BCLBlobCommunication* blobCom, BCLMsgCommunication* blobMsgCom, BCLAbstrAddressStruct* remoteBlobMsgAddress )
		{
		fBlobCom = blobCom;
		fBlobMsgCom = blobMsgCom;
		fRemoteBlobMsgAddress = remoteBlobMsgAddress;
		}
	void SetRemoteBlobMsgAddress( BCLAbstrAddressStruct* remoteBlobMsgAddress )
		{
		fRemoteBlobMsgAddress = remoteBlobMsgAddress;
		}

	void SetCommunicationTimeout( unsigned long connectionTimeout )
		{
		fCommunicationTimeout = connectionTimeout;
		}

	void SetEventAccounting( AliHLTEventAccounting* eventAccounting )
		{
		fEventAccounting = eventAccounting;
		}

	void LockCom()
		{
		pthread_mutex_lock( &fComAccessMutex );
		pthread_mutex_lock( &fMsgComMutex );
		}

	void UnlockCom()
		{
		pthread_mutex_unlock( &fMsgComMutex );
		pthread_mutex_unlock( &fComAccessMutex );
		}

	void LockRecvCom()
		{
		pthread_mutex_lock( &fComRecvMutex );
		}

	void UnlockRecvCom()
		{
		pthread_mutex_unlock( &fComRecvMutex );
		}

	bool IsMsgConnected()
		{
		return fMsgConnected;
		}

	bool IsBlobConnected()
		{
		return fBlobConnected;
		}

	void SetLAMProxy( AliHLTBridgeHeadLAMProxy* proxy )
		{
		fLAMProxy = proxy;
		}

	void SetStatusData( AliHLTBridgeStatusData* status )
		{
		fStatus = status;
		}

	void SetErrorCallback( AliHLTBridgeHeadErrorCallback<AliHLTSubscriberBridgeHead>* errorCallback )
		{
		fErrorCallback = errorCallback;
		}

	int Connect( bool doLock = true );
	int Disconnect( bool initiator = true, bool do_timeout = false, AliUInt32_t timeout_ms = 0, bool doLock = true );

	AliUInt32_t GetMaxRetryCount()
		{
		return fMaxRetryCount;
		}
	void SetMaxRetryCount( AliUInt32_t maxCount )
		{
		fMaxRetryCount = maxCount;
		}

	AliUInt32_t GetRetryTime()
		{
		return fRetryTime;
		}
	void SetRetryTime( AliUInt32_t retryTime )
		{
		fRetryTime = retryTime;
		}

	void AliceHLT()
		{
		fAliceHLT = true;
		}

	void SetRunNumber( unsigned long runNumber )
		{
		fRunNumber = runNumber;
		}

	void Start();
	void Stop();

	void Pause()
		{
		if ( fStatus )
		    fStatus->fProcessStatus.fState |= kAliHLTPCPausedStateFlag;
		fPaused = true;
		}

	void Restart()
		{
		if ( fStatus )
		    fStatus->fProcessStatus.fState &= ~kAliHLTPCPausedStateFlag;
		fPaused = false;
		}
  
	virtual int NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
			      const AliHLTSubEventDataDescriptor& sbevent,
			      const AliHLTEventTriggerStruct& ets );
	virtual int EventCanceled( AliHLTPublisherInterface& publisher, AliEventID_t eventID );
	virtual int EventDoneData( AliHLTPublisherInterface&, 
				   const AliHLTEventDoneData& )
		{
		return 0;
		}
	virtual int SubscriptionCanceled( AliHLTPublisherInterface& publisher );
	virtual int ReleaseEventsRequest( AliHLTPublisherInterface& publisher ); 
	virtual int Ping( AliHLTPublisherInterface& publisher );
	virtual int PingAck( AliHLTPublisherInterface& publisher );

	void MsgLoop();
	bool QuitMsgLoop();

	// Warning, extremely dangerous method. This can cause event loss in your system.
	void PURGEALLEVENTS();

	void INTERRUPTBLOBCONNECTION();

	virtual void DumpEventMetaData();
  
    protected:

	struct EventData
	    {
		AliHLTSubEventDataDescriptor* fSEDD;
		AliHLTEventTriggerStruct* fETS;
		AliHLTPublisherInterface* fPub;
		AliUInt32_t fRetryCount;
		AliUInt32_t fBufferNdx;
		AliUInt32_t fBufferOffset;
		bool fTransferAborted;
		bool fTransferInProgress;
		bool fEventDone;
	        bool fForceEmptyForward;
	    };

	    friend class AliHLTBridgeHeadErrorCallback<AliHLTSubscriberBridgeHead>;

	bool fQuitMsgLoop;
	bool fMsgLoopQuitted;

	bool fPaused;
  
	BCLMsgCommunication* fMsgCom;
	BCLMsgCommunication* fBlobMsgCom;
	BCLBlobCommunication* fBlobCom;

 	pthread_mutex_t fMsgComMutex;
	pthread_mutex_t fComAccessMutex;
	pthread_mutex_t fComRecvMutex;

	unsigned long fCommunicationTimeout;

	bool fBlobConnected;
	bool fMsgConnected;
	bool fConnectionDesired;

	BCLAbstrAddressStruct* fRemoteMsgAddress;
	BCLAbstrAddressStruct* fRemoteBlobMsgAddress;

	BCLAbstrAddressStruct* fReceiveAddr;

	MLUCDynamicAllocCache fMsgCache;

	BCLTransferID fGlobalTransferID;
	uint32 fRemoteBlobSize;

	AliHLTBufferManager* fBufferManager;

	AliHLTDescriptorHandler* fDescriptorHandler;
	pthread_mutex_t fDescriptorMutex;

	MLUCDynamicAllocCache fETSCache;

	void ProcessDescriptor( AliHLTSubEventDataDescriptor* sedd, vector<AliHLTSubEventDataBlockDescriptor>& descriptors, unsigned long long& totalLength, vector<uint8*>& datas, vector<unsigned long>& offsets, vector<unsigned long>& sizes );
  
	AliHLTSharedMemory fShm;
  
	struct ShmData
	    {
		AliHLTShmID fShmKey;
		AliUInt32_t fShmID;
		void* fShmPtr;
		AliUInt32_t fNonRefCount;
		AliUInt32_t fRefCount;
	    };

	typedef struct ShmData ShmData;

	vector<ShmData> fShmData;
	pthread_mutex_t fShmMutex;
	AliUInt32_t fMaxShmNoUseCount;

	pthread_mutex_t fTransferMutex;

	class AliHLTHelperThread: public AliHLTThread
	    {
	    public:
	
		AliHLTHelperThread( AliHLTSubscriberBridgeHead* com, void (AliHLTSubscriberBridgeHead::*func)(void) )
			{
			fCom = com;
			fFunc = func;
			}
		virtual ~AliHLTHelperThread() {};
	    protected:
	
		AliHLTSubscriberBridgeHead* fCom;
		void (AliHLTSubscriberBridgeHead::*fFunc)(void);
	
		virtual void Run()
			{
			(fCom->*fFunc)();
			}
	    };

	class SBHRetryTimerCallback: public AliHLTTimerCallback
	    {
	    public:

		SBHRetryTimerCallback( AliHLTSubscriberBridgeHead* parent ):
		    fParent( parent )
			{
			}
		virtual ~SBHRetryTimerCallback() {};
		
		virtual void TimerExpired( AliUInt64_t notData, unsigned long priority )
			{
			fParent->AddRetry( notData, priority );
			}

	    protected:

		AliHLTSubscriberBridgeHead* fParent;
		
	    };

	    friend class SBHRetryTimerCallback;

	AliHLTTimer fTimer;

	MLUCConditionSem<EventData*> fTransferSignal;
      
	void TransferLoop();
	AliHLTHelperThread fTransferThread;
	void QuitTransferLoop();

	bool fQuitTransferLoop;
	bool fTransferLoopQuitted;

// 	void DataTransferLoop();
// 	AliHLTHelperThread fDataTransferThread;
// 	void QuitDataTransferLoop();

// 	bool fQuitDataTransferLoop;
// 	bool fDataTransferLoopQuitted;
    
	void AddRetry( AliUInt64_t notData, unsigned long priority );
      
	void* GetShmPtr( AliHLTShmID shmKey );

	AliUInt32_t fOldestEventBirth_s;

	void CleanupOldEvents();

	MLUCAllocCache fEventDataCache;

	AliHLTBridgeTransferSEDD* fTransferSEDD;
	AliUInt32_t fTransferSEDDLength;

	AliUInt32_t fMaxRetryCount;

	AliUInt32_t fRetryTime; // in milliseconds.
  
	typedef EventData* PEventData;
	MLUCVector<PEventData> fEventSEDDs;
	pthread_mutex_t fEventSEDDMutex;
      
	SBHRetryTimerCallback fRetryCallback;

	static bool FindEventDataByPublisher( const PEventData& elem, uint64 ref );
	static bool FindEventDataByEvent( const PEventData& elem, const AliEventID_t& ref );
	static bool FindOldEventData( const PEventData& elem, const AliUInt32_t& oldestEventBirth_s );
	static void IterateEventDataAddTransferSignal( const PEventData& elem, void* arg );


	AliHLTBridgeHeadLAMProxy* fLAMProxy;
	AliHLTBridgeStatusData* fStatus;

	AliHLTBridgeHeadErrorCallback<AliHLTSubscriberBridgeHead>* fErrorCallback;

	class DefaultErrorCallback: public AliHLTBridgeHeadErrorCallback<AliHLTSubscriberBridgeHead>
	    {
	    public:

		DefaultErrorCallback();

		virtual ~DefaultErrorCallback();

		virtual void ConnectionError( AliHLTSubscriberBridgeHead* bridgeHead );
		
		virtual void ConnectionEstablished( AliHLTSubscriberBridgeHead* bridgeHead );

	    protected:
		
		bool fCorrectionInProgress;
		bool fConnected;
		pthread_mutex_t fProgressMutex;

	    };

	DefaultErrorCallback fDefaultErrorCallback;

	bool fForwardMaxRetryEvents;

	AliHLTEventAccounting* fEventAccounting;

	bool fAllEventsPurged;

	bool fHighWaterMarkActive;
	AliUInt64_t fHighWaterMarkID;
	unsigned long fHighWaterMark;
	unsigned long fLowWaterMark;
	struct timeval fHighWaterMarkSent;


	bool fAliceHLT;

	unsigned long fRunNumber;

	int fOldBCLLog;
	bool fOldBCLLogSet;

    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3SUBSCRIBERBRIDGEHEAD_HPP_
