#ifndef _ALIL3TDSTATUS_HPP_
#define _ALIL3TDSTATUS_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCStatusHeader.hpp"
#include "AliHLTSCProcessStatus.hpp"
#include "AliHLTSCToleranceDetectorStatus.hpp"

#define ALIL3TDSTATUSTYPE (((AliUInt64_t)'L3TD')<<32 | 'STAT')

struct AliHLTTDStatus
    {
	AliHLTSCStatusHeaderStruct                  fHeader;
	AliHLTSCProcessStatusStruct                 fProcessStatus;
	AliHLTSCToleranceDetectorStatusStruct       fToleranceStatus;

	AliHLTTDStatus()
		{
		AliHLTSCStatusHeader header;
		AliHLTSCProcessStatus process;
		AliHLTSCToleranceDetectorStatus td;
		fHeader = *(header.GetData());
		fHeader.fStatusType = ALIL3TDSTATUSTYPE;
		fProcessStatus = *(process.GetData());
		fToleranceStatus = *(td.GetData());
		fHeader.fLength += fProcessStatus.fLength + fToleranceStatus.fLength;
		fHeader.fStatusStructCnt = 2;
		}

    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3TDSTATUS_HPP_
