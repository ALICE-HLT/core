/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIL3EVENTMERGERNEW_HPP_
#define _ALIL3EVENTMERGERNEW_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliEventID.h"
#include "AliHLTEventDataType.h"
#include "AliHLTEventTriggerStruct.h"
#include "AliHLTForwardingSubscriber.hpp"
#include "AliHLTForwardingPublisher.hpp"
#include "AliHLTTimer.hpp"
#include "AliHLTThread.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTEventModuloCalculation.hpp"
#include "MLUCObjectCache.hpp"
#include "MLUCDynamicAllocCache.hpp"
#include "MLUCVector.hpp"
#include "MLUCIndexedVector.hpp"
#include "MLUCConditionSem.hpp"
#include "MLUCMutex.hpp"
#include "MLUCMultiReaderMutex.hpp"
#include "AliHLTStatus.hpp"
#include "AliHLTReadoutList.hpp"

class AliHLTDescriptorHandler;
class AliHLTStatus;
class AliHLTEventAccounting;


class AliHLTEventMergerNew
    {
    public:

	typedef AliHLTForwardingSubscriber<AliHLTEventMergerNew,unsigned long> TSubscriber;
	typedef AliHLTForwardingPublisher<AliHLTEventMergerNew,unsigned long> TPublisher;

	enum TEventState { kUndefined, kIncomplete, kComplete, kDone };

#ifdef MERGED_BITMAPS
	enum BitmapOffsets { kExpectedParts=0, kCanceledParts=1, kReceivedParts=2, kMaxParts=3 };
#endif

	struct DataBlock
	    {
		unsigned long fSubscriberNdx;
		AliHLTSubEventDataBlockDescriptor fBlockData;
	    };
	struct TriggerData
	    {
		unsigned long fSubscriberNdx;
		AliHLTEventTriggerStruct* fETS;
	    };
	struct EventData
	    {
		AliEventID_t fEventID;
		AliUInt32_t fSubscribersExpected;
		AliUInt32_t fSubscribersPresent;
		unsigned long fHighestExpectedPartNdx;
#ifdef MERGED_BITMAPS
		unsigned long* fBitmaps;
#else
		unsigned long* fExpectedPartBitmap;
		unsigned long* fCanceledPartBitmap;
		unsigned long* fReceivedPartBitmap;
#endif
		MLUCVector<DataBlock> fDataBlocks;
		AliHLTEventTriggerStruct* fETS;
		AliHLTSubEventDataDescriptor* fSEDD;
		bool fBusy;
		bool fDone;
		bool fCanceled;
		bool fScheduled;
		AliUInt32_t              fEventBirth_s;
		AliUInt32_t              fEventBirth_us;
		AliUInt32_t              fOldestEventBirth_s;
		AliUInt64_t              fStatusFlags;
		AliHLTEventDoneData*     fEDD;
	        struct timeval fFirstPartReceive;
		TEventState fState;

		MLUCMutex fMutex;

		void ResetCachedObject()
			{
			fDataBlocks.Clear();
			Init();
			}
		EventData():
		    fDataBlocks( 4 )
			{
			Init();
			}
		void Init()
			{
			fETS = NULL;
			fSEDD = NULL;
			fEDD = NULL;
			fSubscribersPresent = 0;
			fBusy = false;
			fDone = false;
			fCanceled = false;
			fScheduled = false;
			fFirstPartReceive.tv_sec = fFirstPartReceive.tv_usec = 0;
			fState = kUndefined;
			}
	    };

	enum TETSMergeMode { kAppendETS=0, kHLTETS=1 };

	AliHLTEventMergerNew( char const* name, int slotCount = -1 );

	virtual ~AliHLTEventMergerNew();


 	void SetDescriptorHandler( AliHLTDescriptorHandler* descHand )
 		{
 		fDescriptorHandler = descHand;
 		}

	void SetStatusData( AliHLTStatus* status )
		{
		fStatus = status;
		}

	void SetETSMergeMode( TETSMergeMode mergeMode )
		{
		fETSMergeMode = mergeMode;
		}
	void SetEventAccounting( AliHLTEventAccounting* eventAccounting )
		{
		fEventAccounting = eventAccounting;
		}

	void SetEventModuloPreDivisor( unsigned long preDivisor )
		{
		fEventModuloCalculation.SetEventModuloPreDivisor( preDivisor );
		fEvents.SetPreDivisor( preDivisor );
		fEDDBacklog.SetPreDivisor( preDivisor );
		fQueuedEvents.SetPreDivisor( preDivisor );
		fIncompleteEventLog.SetPreDivisor( preDivisor );
		}

	void SetRunNumber( unsigned long runNumber )
		{
		fRunNumber =runNumber;
		}

	void AliceHLT()
		{
		fAliceHLT = true;
		}

 	void SetEventTimeout( AliUInt32_t timeout )
 		{
 		fEventTimeoutLastLog_ms = fEventTimeout_ms = timeout;
		switch ( fDynamicTimeoutAdaptionAlgorithm )
		    {
		    case fgkDynamicTimeoutAdaptionAverageAlgorithm:
			{
			fLastEventCompletionTimesAvg = (unsigned long)(timeout/fLastEventCompletionTimes2TimeoutFactor);
			for ( unsigned long ii=0; ii<fgkLastEventCompletionTimesAverageAlgorithmSize; ii++ )
			    fLastEventCompletionTimes[ii] = fLastEventCompletionTimesAvg;
			fLastEventCompletionTimesCnt = fgkLastEventCompletionTimesAverageAlgorithmSize;
			}
		    case fgkDynamicTimeoutAdaptionMaximumAlgorithm:
			{
			fLastEventCompletionTimesMax = (unsigned long)( timeout/fLastEventCompletionTimes2TimeoutFactor );
			for ( unsigned long ii=0; ii<fgkLastEventCompletionTimesMaximumAlgorithmSize; ii++ )
			    fLastEventCompletionTimes[ii] =  fLastEventCompletionTimesMax;
			fLastEventCompletionTimesMaxIndex = fgkLastEventCompletionTimesMaximumAlgorithmSize-1;
			fLastEventCompletionTimesCnt = fgkLastEventCompletionTimesMaximumAlgorithmSize;
			}
		    }
 		}

 	void SetEventTimeoutShift( AliUInt32_t timeoutShift )
 		{
 		fEventTimeoutShift_ms = timeoutShift;
		}
	
	void EnableDynamicTimeoutAdaption( unsigned long minTimeout, unsigned long maxTimeout )
		{
		fDynamicTimeoutAdaption = true;
		fMinEventTimeout_ms = minTimeout;
		fMaxEventTimeout_ms = maxTimeout;
		}

	void EnableDynamicInputXabling( unsigned long noReceiveEventCntLimit )
		{
		fDynamicInputXabling = true;
		fNoReceiveEventCntLimit = noReceiveEventCntLimit;
		}

	void NoStrictSORRequirements()
		{
		fStrictSORRequirements = false;
		}
	void StrictSORRequirements()
		{
		fStrictSORRequirements = true;
		}


	void AddSubscriber( TSubscriber* subscriber, AliHLTPublisherInterface* publisher, unsigned long ndx );
	void DelSubscriber( unsigned long ndx );
	void EnableSubscriber( unsigned long ndx, bool autoDisable=false );
	void DisableSubscriber( unsigned long ndx, bool autoDisable=false );

	void SetSubscriberContributingDDLList( unsigned long ndx, volatile_hltreadout_uint32_t* contrDDLList );
	void SetSubscriberEventModulos( unsigned long ndx, const std::vector<unsigned long>& modulos );
	void SetEventTypeStackMachineCodes( unsigned long ndx, const std::vector<AliHLTEventTriggerStruct*>& eventTypeStackMachineCodes );

	void SetTriggerClassBitContributingDDLs( unsigned triggerClassBit, hltreadout_uint32_t const* ddlList );
	void SetTriggerClassMaskContributingDDLs( AliUInt64_t* triggerClassMask, hltreadout_uint32_t const* ddlList );

	void SetEventTriggerTypeCompareFunc( AliHLTEventTriggerTypeCompareFunc compFunc, void* compFuncParam );

	void SetPublisher( TPublisher* pub );
	
	void StartProcessing();
	void StopProcessing();
	void PauseProcessing();
	void ResumeProcessing();
	
	
	// This method is called whenever a new wanted 
	// event is available.
	virtual int NewEvent( TSubscriber* sub, unsigned long ndx, 
			      AliHLTPublisherInterface& publisher, 
			      AliEventID_t eventID, 
			      const AliHLTSubEventDataDescriptor& sbevent,
			      const AliHLTEventTriggerStruct& ets );
	
	// This method is called for a transient subscriber, 
	// when an event has been removed from the list. 
	virtual int EventCanceled( TSubscriber* sub, unsigned long ndx, 
				   AliHLTPublisherInterface& publisher, 
				   AliEventID_t eventID );
	
	// This method is called for a subscriber when new event done data is 
	// received by a publisher (sent from another subscriber) for an event. 
	virtual int EventDoneData( TSubscriber* sub, unsigned long ndx, 
				   AliHLTPublisherInterface& publisher, 
				   const AliHLTEventDoneData& 
				   eventDoneData );

	// This method is called when the subscription ends.
	virtual int SubscriptionCanceled( TSubscriber* sub, unsigned long ndx, 
					  AliHLTPublisherInterface& publisher );
	
	// This method is called when the publishing 
	// service runs out of buffer space. The subscriber 
	// has to free as many events as possible and send an 
	// EventDone message with their IDs to the publisher
	virtual int ReleaseEventsRequest( TSubscriber* sub, unsigned long ndx, 
					  AliHLTPublisherInterface& publisher ); 

	// Method used to check alive status of subscriber. 
	// This message has to be answered with 
	// PingAck message within less  than a second.
	virtual int Ping( TSubscriber* sub, unsigned long ndx, 
			  AliHLTPublisherInterface& publisher );
	
	// Method used by a subscriber in response to a Ping 
	// received message. The PingAck message has to be 
	// called within less than one second.
	virtual int PingAck( TSubscriber* sub, unsigned long ndx, 
			     AliHLTPublisherInterface& publisher );


	// empty callback. Will be called when an
	//event has already been internally canceled. 
	virtual void CanceledEvent( TPublisher* pub, unsigned long ndx,
				    AliEventID_t event, vector<AliHLTEventDoneData*>& eventDoneData );

	// empty callback. Will be called when an
	//event has been announced. 
	virtual void AnnouncedEvent( TPublisher* pub, unsigned long ndx,
				     AliEventID_t eventID, const AliHLTSubEventDataDescriptor& sbevent, 
				     const AliHLTEventTriggerStruct& trigger, AliUInt32_t refCount );

	// empty callback. Will be called when an event done message with non-trivial event done data
	// (fDataWordCount>0) has been received for that event. 
	// This is called prior to the sending of the EventDoneData to interested other subscribers
	// (SendEventDoneData above)
	virtual void EventDoneDataReceived( TPublisher* pub, unsigned long ndx,
					    const char* subscriberName, AliEventID_t eventID,
					    const AliHLTEventDoneData* edd, bool forceForward, bool forwarded );

	// Empty callback, called when an already announced event has to be announced again to a new
	// subscriber that wants to receive old events. This method needs to return the pointers
	// to the sub-event data descriptor and the event trigger structure. If the function returns
	// false this is a signal that the desired data could not be found anymore. 
	virtual bool GetAnnouncedEventData( TPublisher* pub, unsigned long ndx,
					    AliEventID_t eventID, AliHLTSubEventDataDescriptor*& sedd,
					    AliHLTEventTriggerStruct*& trigger );

	
	virtual void PURGEALLEVENTS();

	virtual void DumpEventMetaData();

    protected:

	struct SubscriberData
	    {
		TSubscriber* fSubscriber;
		AliHLTPublisherInterface* fPublisher;
		unsigned long fNdx;
		bool fEnabled;
	        bool fAutoDisabled;
		volatile_hltreadout_uint32_t* fContributingDDLs;
	        unsigned fNoReceiveEventCnt;
		unsigned long* fSubExpSubBitmap;
		//pthread_mutex_t* fSubExpSubBitmapMutex;
		MLUCMutex* fSubExpSubBitmapMutex;
		unsigned long fModuloCnt;
		unsigned long* fModulos;
		unsigned long fTriggerCnt;
		AliHLTEventTriggerStruct** fTriggers;
	    };

	struct EventBacklogData
	    {
		AliEventID_t fEventID;
		AliHLTEventDoneData* fEDD;
	        struct timeval fFirstPartReceive;
	    };

	virtual void DoProcessing();

	void ReleaseEvent( unsigned long ndx, MLUCMultiReaderMutex::TWriteLocker& eventDataWriteLock, MLUCMutex::TLocker& eventLock );

	void GetSubscriberDDLList( unsigned long subNdx, volatile_hltreadout_uint32_t*& contributingDDList );
	bool GetSubscriberDDLLists( volatile_hltreadout_uint32_t**& contributingDDLLists );
	void FreeSubscriberDDLLists( volatile_hltreadout_uint32_t** contributingDDLLists )
		{
		delete [] contributingDDLLists;
		}

	bool GetTriggerClassBitContributingDDLs( unsigned triggerClassBit, volatile_hltreadout_uint32_t* ddlList, bool reset=true );
	unsigned long GetTriggerClassMaskContributingDDLs( AliUInt64_t* triggerClassMask, volatile_hltreadout_uint32_t* ddlList );
	unsigned long GetTriggerClassCnt();

	MLUCString fName;

	MLUCMultiReaderMutex fSubscriberMutex; // When acquiring fSubscriberMutex and (fStaticEDDMutex or fBitmapCacheMutex) together, FIRST acquire fSubscriberMutex ; When acquiring fSubscriberMutex and fEventMutex together first acquire fEventMutex and SECOND only fSubscriberMutex
	//vector<SubscriberData> fSubscribers;
	SubscriberData* fSubscribers;
	unsigned long fSubscriberCnt;
	//vector<unsigned long> fExpectedSubscribers;
	unsigned long fHighestSubscriberNdx;
	unsigned long *fExpectedSubscribersBitmap;
	unsigned long fExpectedSubscribersBitmapLength;
	unsigned long fExpectedSubscriberCnt;
	static unsigned long gfSubscriberIndexShift;
	static unsigned long gfSubscriberIndexMask;
	MLUCDynamicAllocCache fBitmapCachePerEvent;
	MLUCDynamicAllocCache fBitmapCacheTmp;
	pthread_mutex_t fBitmapCacheMutex; // When acquiring fSubscriberMutex and fBitmapCacheMutex together, FIRST acquire fSubscriberMutex

	TPublisher* fPublisher;

	bool fQuitProcessing;
	bool fProcessingQuitted;

#if 0
	pthread_mutex_t fEventMutex;
#endif
	MLUCMultiReaderMutex fEventMutex; // When acquiring fSubscriberMutex and fEventMutex together FIRST acquire fEventMutex and second only fSubscriberMutex
	//vector<EventData*> fEventData;
	typedef EventData* PEventData;
#if 1
	MLUCIndexedVector<PEventData,AliEventID_t> fEvents;
	MLUCIndexedVector<EventBacklogData,AliEventID_t> fEDDBacklog;
	MLUCMutex fEventCountMutex;
	unsigned long long fIncompleteEventCount;
	unsigned long long fCompleteEventCount;
	MLUCIndexedVector<AliEventID_t,AliEventID_t> fQueuedEvents;
	MLUCMutex fQueuedEventMutex;

#else
	MLUCIndexedVector<PEventData,AliEventID_t> fFinishedEventData;
	MLUCIndexedVector<PEventData,AliEventID_t> fUnfinishedEventData;
	MLUCIndexedVector<EventBacklogData,AliEventID_t> fEDDBacklog;
#endif
	MLUCObjectCache<EventData> fEventDataCache;
	MLUCIndexedVector<AliEventID_t, AliEventID_t> fIncompleteEventLog;

	unsigned long fLastBlockCnt;

	bool fPaused;

	AliHLTDescriptorHandler* fDescriptorHandler;
	pthread_mutex_t fDescriptorHandlerMutex;

	MLUCDynamicAllocCache fETSCache; // Protected by fEventMutex
// 	pthread_mutex_t fETSCacheMutex;

	AliHLTEventTriggerStruct fStaticETS;

	MLUCDynamicAllocCache fEDDCache;
	pthread_mutex_t fEDDCacheMutex;

	AliHLTEventDoneData fStaticEDD;
	pthread_mutex_t fStaticEDDMutex; // When acquiring fSubscriberMutex and fStaticEDDMutex together, FIRST acquire fSubscriberMutex

	MLUCConditionSem<PEventData> fSignal;

	AliHLTObjectThread<AliHLTEventMergerNew> fProcessingThread;

// 	static bool FindEventDataByID( const PEventData& elem, uint64 ref )
// 		{
// 		return elem->fEventID == (AliEventID_t)ref;
// 		}

	static bool FindEmptyEventData( const PEventData& elem, const uint32& )
		{
		return !elem->fSubscribersExpected;
		}

	static bool FindSubscriberDataBlocks( const DataBlock& elem, const unsigned long& ref )
		{
		return elem.fSubscriberNdx == ref;
		}

	static bool FindDataBlock( const DataBlock& elem, const AliHLTSubEventDataBlockDescriptor& ref )
		{
		if ( elem.fBlockData.fShmID.fShmType != ref.fShmID.fShmType ||
		     elem.fBlockData.fShmID.fKey.fID != ref.fShmID.fKey.fID ||
		     elem.fBlockData.fBlockSize != ref.fBlockSize ||
		     elem.fBlockData.fBlockOffset != ref.fBlockOffset ||
		     elem.fBlockData.fProducerNode != ref.fProducerNode ||
		     elem.fBlockData.fDataType.fID != ref.fDataType.fID ||
		     elem.fBlockData.fDataOrigin.fID != ref.fDataOrigin.fID ||
		     elem.fBlockData.fDataSpecification != ref.fDataSpecification ||
		     elem.fBlockData.fStatusFlags != ref.fStatusFlags )
		    return false;
		for ( int bai=0; bai<kAliHLTSEDBDAttributeCount; bai++ )
		    {
		    if ( elem.fBlockData.fAttributes[bai] != ref.fAttributes[bai] )
			return false;
		    }
		return true;
		}


	static bool FindxORDataBlock( const DataBlock& elem, const AliHLTSubEventDataBlockDescriptor& ref )
		{
		if ( elem.fBlockData.fDataType.fID != ref.fDataType.fID ||
		     elem.fBlockData.fDataOrigin.fID != ref.fDataOrigin.fID )
		    return false;
		return true;
		}


// 	static bool FindEDDBacklogByID( const EventBacklogData& elem, uint64 ref )
// 		{
// 		return elem.fEventID == (AliEventID_t)ref;
// 		}


// 	static bool FindAnnounceableEventData( const PEventData& elem, uint64 ref )
// 		{
// 		return elem->fSubscribersExpected == elem->fSubscribersPresent;
// 		}

	static void SubscriberDisabledEventIterationFunc( const PEventData& el, void* arg );
	static void PurgeSubscriberEventIterationFunc( const PEventData& el, void* arg );
	static void AddDataBlocksEventIterationFunc( const DataBlock& el, void* arg );

	// Must be protected by write lock!
	static void AnnounceAnnounceableEventsIterationFunc( const PEventData& el, void* arg );
	// Must be protected by write lock!
	static void GetIncompleteEventsIterationFunc( const PEventData& el, void* arg );

	AliHLTPublisherInterface* GetPublisher( unsigned long subscriberNdx ); // must be called with fSubscriberMutex already locked.
	AliHLTSubscriberInterface* GetSubscriber( unsigned long subscriberNdx ); // must be called with fSubscriberMutex already locked.

	static unsigned long GetBitmapWordIndex( unsigned long ndx )
		{
		return ndx >> gfSubscriberIndexShift;
		}
	static unsigned long GetBitmapBitInWord( unsigned long ndx )
		{
		return 1UL << (ndx & gfSubscriberIndexMask);
		}

	bool AllocateBitmap( unsigned long ndx, unsigned long*& bitmap, unsigned long& len, bool perEvent = true );
	void ReleaseBitmap( unsigned long* bitmap, bool perEvent = true );
	static void LogBitmap( AliHLTLog::TLogLevel, const char* origin, const char* keyword, unsigned long* bitmap, unsigned long len );

        bool fDynamicTimeoutAdaption;
	
	unsigned long fEventTimeout_ms;
	unsigned long fEventTimeoutLastLog_ms;
	unsigned long fMaxEventTimeout_ms;
	unsigned long fMinEventTimeout_ms;
	unsigned long fEventTimeoutShift_ms;
	unsigned long fNoReceiveEventCntLimit;

	const static unsigned fgkDynamicTimeoutAdaptionAverageAlgorithm = 0;
	const static unsigned fgkDynamicTimeoutAdaptionMaximumAlgorithm = 1;
        const static unsigned fgkLastEventCompletionTimesAverageAlgorithmSizeExp2 = 5;
        const static unsigned fgkLastEventCompletionTimesAverageAlgorithmSize = 1 << fgkLastEventCompletionTimesAverageAlgorithmSizeExp2;
        const static unsigned fgkLastEventCompletionTimesAverageAlgorithmSizeMask = (1 << fgkLastEventCompletionTimesAverageAlgorithmSizeExp2)-1;
        const static unsigned fgkLastEventCompletionTimesMaximumAlgorithmSizeExp2 = 11;
        const static unsigned fgkLastEventCompletionTimesMaximumAlgorithmSize = 1 << fgkLastEventCompletionTimesMaximumAlgorithmSizeExp2;
        const static unsigned fgkLastEventCompletionTimesMaximumAlgorithmSizeMask = (1 << fgkLastEventCompletionTimesMaximumAlgorithmSizeExp2)-1;
	unsigned fDynamicTimeoutAdaptionAlgorithm;
        unsigned fLastEventCompletionTimesCnt;
        unsigned fLastEventCompletionTimesNdx;
        unsigned long fLastEventCompletionTimes[ (fgkLastEventCompletionTimesAverageAlgorithmSize>fgkLastEventCompletionTimesMaximumAlgorithmSize) ? fgkLastEventCompletionTimesAverageAlgorithmSize : fgkLastEventCompletionTimesMaximumAlgorithmSize];
        unsigned long fLastEventCompletionTimesAvg;
        unsigned long fLastEventCompletionTimesMax;
        unsigned fLastEventCompletionTimesMaxIndex;
        double fLastEventCompletionTimes2TimeoutFactor;
      
        void AddEventReceiveInterval( struct timeval firstPartReceiveTime ) // Protected by fEventMutex
            {
	    struct timeval now;
	    gettimeofday( &now, NULL );
	    unsigned long tdiff_ms = (now.tv_sec-firstPartReceiveTime.tv_sec)*1000+(now.tv_usec-firstPartReceiveTime.tv_usec)/1000;
	    AddEventReceiveInterval( tdiff_ms );
	    }

        void AddEventReceiveInterval( unsigned long tdiff_ms ) // Protected by fEventMutex
		{
		LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Adding new completion time" )
		    << "Adding new event completion time to " << AliHLTLog::kDec << tdiff_ms << " ms (Cnt: "
		    << fLastEventCompletionTimesCnt << ")." << ENDLOG;
		switch ( fDynamicTimeoutAdaptionAlgorithm )
		    {
		    case fgkDynamicTimeoutAdaptionAverageAlgorithm:
			{
			if ( fLastEventCompletionTimesCnt<fgkLastEventCompletionTimesAverageAlgorithmSize )
			    {
			    fLastEventCompletionTimes[fLastEventCompletionTimesNdx] = tdiff_ms;
			    fLastEventCompletionTimesAvg *= fLastEventCompletionTimesCnt;
			    ++fLastEventCompletionTimesCnt;
			    ++fLastEventCompletionTimesNdx;
			    fLastEventCompletionTimesAvg += tdiff_ms;
			    fLastEventCompletionTimesAvg /= fLastEventCompletionTimesCnt;
			    }
			else
			    {
			    LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
				<< AliHLTLog::kDec
				<< "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
				<< " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
				<< " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
				<< " - fLastEventCompletionTimesAvg: " << fLastEventCompletionTimesAvg
				<< ENDLOG;
			    fLastEventCompletionTimesNdx &= fgkLastEventCompletionTimesAverageAlgorithmSizeMask;
			    LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
				<< AliHLTLog::kDec
				<< "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
				<< " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
				<< " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
				<< " - fLastEventCompletionTimesAvg: " << fLastEventCompletionTimesAvg
				<< ENDLOG;
			    fLastEventCompletionTimesAvg *= fLastEventCompletionTimesCnt;
			    LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
				<< AliHLTLog::kDec
				<< "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
				<< " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
				<< " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
				<< " - fLastEventCompletionTimesAvg: " << fLastEventCompletionTimesAvg
				<< ENDLOG;
			    if ( fLastEventCompletionTimesAvg > fLastEventCompletionTimes[fLastEventCompletionTimesNdx] )
				fLastEventCompletionTimesAvg -= fLastEventCompletionTimes[fLastEventCompletionTimesNdx];
			    else
				fLastEventCompletionTimesAvg = 0;
			    LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
				<< AliHLTLog::kDec
				<< "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
				<< " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
				<< " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
				<< " - fLastEventCompletionTimesAvg: " << fLastEventCompletionTimesAvg
				<< ENDLOG;
			    fLastEventCompletionTimes[fLastEventCompletionTimesNdx] = tdiff_ms;
			    LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
				<< AliHLTLog::kDec
				<< "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
				<< " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
				<< " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
				<< " - fLastEventCompletionTimesAvg: " << fLastEventCompletionTimesAvg
				<< ENDLOG;
			    fLastEventCompletionTimesAvg += fLastEventCompletionTimes[fLastEventCompletionTimesNdx];
			    LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
				<< AliHLTLog::kDec
				<< "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
				<< " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
				<< " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
				<< " - fLastEventCompletionTimesAvg: " << fLastEventCompletionTimesAvg
				<< ENDLOG;
			    ++fLastEventCompletionTimesNdx;
			    LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
				<< AliHLTLog::kDec
				<< "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
				<< " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
				<< " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
				<< " - fLastEventCompletionTimesAvg: " << fLastEventCompletionTimesAvg
				<< ENDLOG;
			    fLastEventCompletionTimesAvg /= fLastEventCompletionTimesCnt;
			    LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
				<< AliHLTLog::kDec
				<< "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
				<< " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
				<< " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
				<< " - fLastEventCompletionTimesAvg: " << fLastEventCompletionTimesAvg
				<< ENDLOG;
			    }
			fEventTimeout_ms = (unsigned long)(fLastEventCompletionTimes2TimeoutFactor*fLastEventCompletionTimesAvg);
			}
			
		    case fgkDynamicTimeoutAdaptionMaximumAlgorithm:
			{
			LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
			    << AliHLTLog::kDec
			    << "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
			    << " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
			    << " - fLastEventCompletionTimesMaxIndex: " << fLastEventCompletionTimesMaxIndex
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]: " << fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]
			    << ENDLOG;
			if ( fLastEventCompletionTimesCnt<fgkLastEventCompletionTimesMaximumAlgorithmSize )
			    {
			    fLastEventCompletionTimes[fLastEventCompletionTimesNdx] = tdiff_ms;
			LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
			    << AliHLTLog::kDec
			    << "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
			    << " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
			    << " - fLastEventCompletionTimesMaxIndex: " << fLastEventCompletionTimesMaxIndex
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]: " << fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]
			    << ENDLOG;
			    if ( tdiff_ms >= fLastEventCompletionTimesMax )
				{
				fLastEventCompletionTimesMax = tdiff_ms;
				fLastEventCompletionTimesMaxIndex = fLastEventCompletionTimesNdx;
				}
			LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
			    << AliHLTLog::kDec
			    << "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
			    << " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
			    << " - fLastEventCompletionTimesMaxIndex: " << fLastEventCompletionTimesMaxIndex
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]: " << fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]
			    << ENDLOG;
			    ++fLastEventCompletionTimesCnt;
			    ++fLastEventCompletionTimesNdx;
			LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
			    << AliHLTLog::kDec
			    << "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
			    << " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
			    << " - fLastEventCompletionTimesMaxIndex: " << fLastEventCompletionTimesMaxIndex
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]: " << fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]
			    << ENDLOG;
			    }
			else
			    {
			    fLastEventCompletionTimesNdx &= fgkLastEventCompletionTimesMaximumAlgorithmSizeMask;
			LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
			    << AliHLTLog::kDec
			    << "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
			    << " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
			    << " - fLastEventCompletionTimesMaxIndex: " << fLastEventCompletionTimesMaxIndex
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]: " << fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]
			    << ENDLOG;
			    fLastEventCompletionTimes[fLastEventCompletionTimesNdx] = tdiff_ms;
			LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
			    << AliHLTLog::kDec
			    << "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
			    << " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
			    << " - fLastEventCompletionTimesMaxIndex: " << fLastEventCompletionTimesMaxIndex
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]: " << fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]
			    << ENDLOG;
			    if ( tdiff_ms >= fLastEventCompletionTimesMax )
				{
			LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
			    << AliHLTLog::kDec
			    << "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
			    << " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
			    << " - fLastEventCompletionTimesMaxIndex: " << fLastEventCompletionTimesMaxIndex
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]: " << fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]
			    << ENDLOG;
				fLastEventCompletionTimesMax = tdiff_ms;
				fLastEventCompletionTimesMaxIndex = fLastEventCompletionTimesNdx;
			LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
			    << AliHLTLog::kDec
			    << "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
			    << " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
			    << " - fLastEventCompletionTimesMaxIndex: " << fLastEventCompletionTimesMaxIndex
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]: " << fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]
			    << ENDLOG;
				}
			    else if ( fLastEventCompletionTimesNdx == fLastEventCompletionTimesMaxIndex )
				{
			LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
			    << AliHLTLog::kDec
			    << "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
			    << " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
			    << " - fLastEventCompletionTimesMaxIndex: " << fLastEventCompletionTimesMaxIndex
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]: " << fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]
			    << ENDLOG;
				fLastEventCompletionTimesMaxIndex = fLastEventCompletionTimesNdx;
			LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
			    << AliHLTLog::kDec
			    << "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
			    << " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
			    << " - fLastEventCompletionTimesMaxIndex: " << fLastEventCompletionTimesMaxIndex
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]: " << fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]
			    << ENDLOG;
				fLastEventCompletionTimesMax = fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex];
			LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
			    << AliHLTLog::kDec
			    << "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
			    << " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
			    << " - fLastEventCompletionTimesMaxIndex: " << fLastEventCompletionTimesMaxIndex
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]: " << fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]
			    << ENDLOG;
				for ( unsigned ii = 1, 
					      ndx = (fLastEventCompletionTimesMaxIndex+ii) & fgkLastEventCompletionTimesMaximumAlgorithmSizeMask; 
				      ii<fgkLastEventCompletionTimesMaximumAlgorithmSize; 
				      ii++,
					      ndx = (ndx+1) & fgkLastEventCompletionTimesMaximumAlgorithmSizeMask )
				    {
				    if ( fLastEventCompletionTimes[ii]>=fLastEventCompletionTimesMax )
					{
			LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
			    << AliHLTLog::kDec
			    << "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
			    << " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
			    << " - fLastEventCompletionTimesMaxIndex: " << fLastEventCompletionTimesMaxIndex
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]: " << fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]
			    << ENDLOG;
					fLastEventCompletionTimesMaxIndex = ii;
					fLastEventCompletionTimesMax = fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex];
			LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
			    << AliHLTLog::kDec
			    << "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
			    << " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
			    << " - fLastEventCompletionTimesMaxIndex: " << fLastEventCompletionTimesMaxIndex
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]: " << fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]
			    << ENDLOG;
					}
				    }
				}
			    }
			LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Debug" )
			    << AliHLTLog::kDec
			    << "fLastEventCompletionTimesCnt: " << fLastEventCompletionTimesCnt
			    << " - fLastEventCompletionTimesNdx: " << fLastEventCompletionTimesNdx
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesNdx]: " << fLastEventCompletionTimes[fLastEventCompletionTimesNdx]
			    << " - fLastEventCompletionTimesMaxIndex: " << fLastEventCompletionTimesMaxIndex
			    << " - fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]: " << fLastEventCompletionTimes[fLastEventCompletionTimesMaxIndex]
			    << ENDLOG;
			++fLastEventCompletionTimesNdx;
			fEventTimeout_ms = (unsigned long)( fLastEventCompletionTimes2TimeoutFactor*fLastEventCompletionTimesMax );
			}
		    default:
			break;
		    }
		if ( fEventTimeout_ms>fMaxEventTimeout_ms )
		    fEventTimeout_ms = fMaxEventTimeout_ms;
		if ( fEventTimeout_ms<fMinEventTimeout_ms )
		    fEventTimeout_ms = fMinEventTimeout_ms;
		fEventTimeout_ms += fEventTimeoutShift_ms;
		LOG( AliHLTLog::kDebug, "AliHLTEventMergerNew::AddEventReceiveInterval", "Setting new timeout" )
		    << "Setting new event timeout to " << AliHLTLog::kDec << fEventTimeout_ms << " ms." << ENDLOG;
		unsigned long diff = (fEventTimeoutLastLog_ms>fEventTimeout_ms) ? (fEventTimeoutLastLog_ms-fEventTimeout_ms) : (fEventTimeout_ms-fEventTimeoutLastLog_ms);
		if ( diff > (fEventTimeoutLastLog_ms>>3) )
		    {
		    LOG( AliHLTLog::kInformational, "AliHLTEventMergerNew::AddEventReceiveInterval", "Setting new timeout" )
			<< "Setting new event timeout to " << AliHLTLog::kDec << fEventTimeout_ms << " ms." << ENDLOG;
		    fEventTimeoutLastLog_ms = fEventTimeout_ms;
		    }
		}

	AliHLTStatus* fStatus;

	struct TimeoutData
	    {
		AliEventID_t fEventID;
		unsigned long fTimeout;
		void ResetCachedObject()
			{
			fEventID.fType = kAliEventTypeUnknown;
			fEventID.fNr = ~(AliUInt64_t)0;
			}
	    };

	MLUCObjectCache<TimeoutData> fTimeoutDataCache;
	    friend class AliHLTEventMergerNewTimerCallback;
	
	class AliHLTEventMergerNewTimerCallback: public AliHLTTimerCallback
	    {
	    public:
		
		AliHLTEventMergerNewTimerCallback( AliHLTEventMergerNew* merger ):
		    fMerger( merger )
			{
			}
		
		virtual ~AliHLTEventMergerNewTimerCallback() {};
		
		virtual void TimerExpired( AliUInt64_t notData, unsigned long priority )
			{
			if ( fMerger )
			    fMerger->EventTimeoutExpired( notData, priority );
			}
	    protected:
		AliHLTEventMergerNew* fMerger;
	    };

	AliHLTEventMergerNewTimerCallback fEventTimeoutCallback;
	AliHLTTimer fTimer;

	virtual void EventTimeoutExpired( AliUInt64_t notData, unsigned long priority );

		
	TETSMergeMode fETSMergeMode;

	bool fAliceHLT;

	AliHLTEventAccounting* fEventAccounting;

	std::vector<hltreadout_uint32_t const*> fTriggerClassContributingDDLs;
	unsigned long fTriggerClassContributingDDLCnt;
	MLUCMutex fTriggerClassContributingDDLsMutex;

	bool fSOREventReceived;
	bool fSOREventCompleted;
	bool fStrictSORRequirements;

	bool fDynamicInputXabling;

	bool fEventsPurged;

	AliHLTEventModuloCalculation fEventModuloCalculation;
	unsigned long fEventCount;

	AliHLTEventTriggerTypeCompareFunc fETTCompFunc;
	void* fETTCompFuncParam;

	unsigned long fRunNumber;

    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIL3EVENTMERGERNEW_HPP_
