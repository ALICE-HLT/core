/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIl3EVENTCOLLECTORREPUBLISHER_HPP_
#define _ALIl3EVENTCOLLECTORREPUBLISHER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTDetectorRePublisher.hpp"
#include "AliHLTBaseEventGatherer.hpp"
//#include <string>


class AliHLTEventGathererRePublisher: public AliHLTDetectorRePublisher
    {
    public:

	AliHLTEventGathererRePublisher( const char* name, int slotCntExp2 = -1 );

	virtual ~AliHLTEventGathererRePublisher();

	void SetGatherer( AliHLTBaseEventGatherer* collector )
		{
		fGatherer = collector;
		}

    protected:

	virtual void CanceledEvent( AliEventID_t event, vector<AliHLTEventDoneData*>& eventDoneData );


	AliHLTBaseEventGatherer* fGatherer;

    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIl3EVENTCOLLECTORREPUBLISHER_HPP_
