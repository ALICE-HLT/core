/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _ALIl3EVENTMERGERREPUBLISHER_HPP_
#define _ALIl3EVENTMERGERREPUBLISHER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTDetectorRePublisher.hpp"
#include "AliHLTEventMerger.hpp"
//#include <string>


class AliHLTEventMergerRePublisher: public AliHLTDetectorRePublisher
    {
    public:

	AliHLTEventMergerRePublisher( const char* name, int slotCntExp2 = -1 );

	virtual ~AliHLTEventMergerRePublisher();

	void SetMerger( AliHLTEventMerger* merger )
		{
		fMerger = merger;
		}

    protected:

	virtual void CanceledEvent( AliEventID_t event, vector<AliHLTEventDoneData*>& eventDoneData );


	AliHLTEventMerger* fMerger;

    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIl3EVENTMERGERREPUBLISHER_HPP_
