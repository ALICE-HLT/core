/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTEventDataType.h"
#include "AliEventID.h"
#include "AliHLTThread.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTPubSubPipeCom.hpp"
#include "AliHLTPublisherPipeProxy.hpp"
#include "AliHLTEventScatterer.hpp"
#include "AliHLTEventScattererSubscriber.hpp"
#include "AliHLTEventScattererRePublisher.hpp"
#include "AliHLTRoundRobinEventScatterer.hpp"
#include "AliHLTPipeController.hpp"
#include <iostream>
#include <fstream>
#ifdef LIBC_MEMDEBUG
#include <mcheck.h>
#endif


class SubscriptionListenerThread: public AliHLTThread
    {
    public:

	SubscriptionListenerThread( AliHLTEventScattererRePublisher* pub  ):
	    fPub( pub )
		{
		}

    protected:

	virtual void Run()
		{
		if ( fPub )
		    fPub->AcceptSubscriptions();
		}

	AliHLTEventScattererRePublisher* fPub;

    };

struct PublishData
    {
	char* fPublisherName;
	AliHLTEventScattererRePublisher* fPublisher;
	SubscriptionListenerThread* fThread;
    };



int main( int argc, char** argv )
    {
#ifdef LIBC_MEMDEBUG
    mtrace();
#endif
    int i;
    AliHLTFilteredStdoutLogServer sOut;
    gLogLevel = AliHLTLog::kAll;
    gLog.AddServer( sOut );

    char* publisherName = NULL;
    char* subscriberID = NULL;
    char* errorStr = NULL;
    char* cerr;
    vector<PublishData> publishers;
    const char* usage1 = "Usage: ";
    const char* usage2 = " (-V verbosity) -subscribe publishername subscriberid) (-nostdout) (-slotslog2 <slotcount>) (-publisher publishername1) (-publisher publishername2)  ...";
    bool nostdout = false;
    int slotCount = -1;
    
    i = 1;
    while ( (i < argc) && !errorStr )
	{
	if ( !strcmp( argv[i], "-nostdout" ) )
	    {
	    nostdout = true;
	    i++;
	    continue;
	    }
	if ( !strcmp( argv[i], "-publisher" ) )
	    {
	    if ( argc < i+1 )
		{
		errorStr = "Missing publishername parameter";
		break;
		}
	    PublishData pd;
	    pd.fPublisherName = argv[i+1];
	    pd.fPublisher = NULL;
	    pd.fThread = NULL;
	    publishers.insert( publishers.end(), pd );
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-subscribe" ) )
	    {
	    if ( argc < i+2 )
		{
		errorStr = "Missing publishername or subscriberid parameter";
		break;
		}
	    publisherName = argv[i+1];
	    subscriberID = argv[i+2];
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc < i +1 )
		{
		errorStr = "Missing verbosity level specifier.";
		break;
		}
	    gLogLevel = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting verbosity level specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-slotslog2" ) )
	    {
	    if ( argc < i +1 )
		{
		errorStr = "Missing slot-count specifier.";
		break;
		}
	    slotCount = strtol( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting slot-count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	errorStr = "Unknown parameter";
	}
    if ( !errorStr )
	{
	if ( publishers.size()<=0 )
	    {
	    errorStr = "Must specify at least one -publisher publishername argument";
	    }
	if ( !subscriberID || !publisherName )
	    errorStr = "Must specify the -subscribe publishername subscriberid argument";
	}
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "EventScatterer", "Command line parameters" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	LOG( AliHLTLog::kError, "EventScatterer", "Command line parameters" )
	    << errorStr << ENDLOG;
	return -1;
	}

    if ( nostdout )
	{
	gLog.DelServer( sOut );
	}

    MLUCString name = "EventScatterer-";
    name += publisherName;
    name += "-";
    name += subscriberID;
    name += "-";
    AliHLTFileLogServer fileOut( name.c_str(), ".log", 3000000 );
    gLog.AddServer( fileOut );

    name = "EventScatterer-";
    name += publisherName;
    name += "-";
    name += subscriberID;
//     name = "EventScatterer-";
//     name += subscriberID;
    AliHLTPipeController pipeControl( name.c_str() );
    pipeControl.AddValue( *(AliUInt32_t*)&gLogLevel );
    pipeControl.Start();

    AliHLTRoundRobinEventScatterer dispatcher;
    name = "EventScatterer-";
    name += subscriberID;
    AliHLTEventScattererSubscriber subscriber( name.c_str(), 10, slotCount );
    AliHLTPublisherPipeProxy publisher( publisherName );
    dispatcher.SetSubscriber( &subscriber );
    subscriber.SetScatterer( &dispatcher );
    subscriber.SetPublisher( publisher );

    vector<PublishData>::iterator iter, end;
    iter = publishers.begin();
    end = publishers.end();
    while ( iter != end )
	{
	iter->fPublisher = new AliHLTEventScattererRePublisher( iter->fPublisherName, slotCount );
	iter->fPublisher->SetSubscriptionLoop( &PublisherPipeSubscriptionInputLoop );
	iter->fThread = new SubscriptionListenerThread( iter->fPublisher );
	iter->fPublisher->SetScatterer( &dispatcher );
	iter->fPublisher->SetPrePublisher( publisher );
	iter->fPublisher->SetPreSubscriber( subscriber );
	iter->fThread->Start();
	dispatcher.AddPublisher( iter->fPublisher );
	iter++;
	}

    subscriber.StartProcessing();
    publisher.Subscribe( subscriber );
    
    publisher.StartPublishing( subscriber );
    subscriber.StopProcessing();

    iter = publishers.begin();
    end = publishers.end();
    while ( iter != end )
	{
	QuitPipeSubscriptionLoop( *(iter->fPublisher) );
	delete iter->fPublisher;
	delete iter->fThread;
	iter++;
	}

    return 0;
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
