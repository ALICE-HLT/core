/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLMsgCommunication.hpp"
#include "BCLBlobCommunication.hpp"
#include "BCLURLAddressDecoder.hpp"
#include "AliHLTSubscriberBridgeHead.hpp"
#include "AliHLTPublisherPipeProxy.hpp"
#include "AliHLTPublisherShmProxy.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTThread.hpp"
#include "AliHLTPipeController.hpp"
#include "AliHLTRingBufferManager.hpp"
#include "BCLAddressLogger.hpp"
#include "BCLErrorLogCallback.hpp"
#include "BCLStackTraceCallback.hpp"
// XXX
#include "BCLTCPComID.hpp"
#include "BCLTCPBlobCommunication.hpp"
// XXX
#include <fstream>
#include <iostream>
#include <sys/ipc.h>
#include <sys/shm.h>
#ifdef LIBC_MEMDEBUG
#include <mcheck.h>
#endif

template<class T>
class SubscriberBridgeHeadThread: public AliHLTThread
    {
    public:

	SubscriberBridgeHeadThread( T* obj, void (T::*func)(void) )
		{
		fObj = obj;
		fFunc = func;
		}

    protected:

	T* fObj;
	void (T::*fFunc)(void);

	virtual void Run()
		{
		(fObj->*fFunc)();
		}
    };

class SubscriberBridgeHeadFailCallback: public BCLErrorCallback
    {
    public:
	
	SubscriberBridgeHeadFailCallback( bool& failVar ):
	    fFailVar( failVar )
		{
		fFailVar = false;
		}
	
	virtual BCLErrorAction ConnectionTemporarilyLost( int, BCLCommunication*,
							  BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction ConnectionError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction BindError( int, BCLCommunication*,
					       BCLAbstrAddressStruct*, 
					     BCLMessageStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction MsgSendError( int, BCLCommunication*,
					       BCLAbstrAddressStruct*, 
					     BCLMessageStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction MsgReceiveError( int, BCLCommunication*,
					       BCLAbstrAddressStruct*, 
						BCLMessageStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction BlobPrepareError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction BlobSendError( int, BCLCommunication*,
					      BCLAbstrAddressStruct*, 
					      BCLTransferID ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction BlobReceiveError( int, BCLCommunication*,
						 BCLTransferID ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction AddressError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction GeneralError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

    protected:
	
	bool& fFailVar;

    };


int main( int argc, char** argv )
    {
#ifdef LIBC_MEMDEBUG
    mtrace();
#endif
    uint32 i;
    char* errorStr = NULL;
    const char* usage1 = "Usage: ";
    const char* usage2 = " -blobmsg <blobmsg-own-address-URL> <blobmsg-remote-address-URL> -blob <blobsize> <blob-own-address-URL> -msg <msg-own-address-URL> <msg-remote-address-URL> (-V <verbosity_levels>) -subscribe <publishername> <subscriberid> (-timeout <connectiontimeout_ms>) (-nostdout) (-blocksize <blocksize>) (-mindescriptorblocks <minimum descriptor block count>) (-maxdescriptorblocks <maximum descriptor block count>) (-slotcount <(event) slot count>) (-noconnect) (-shmproxy <shmsize> <publisher-to-subscriber-shmkey> <subscriber-to-publisher-shmkey>)";
    char* cerr;
    char* publisherName = NULL;
    char* subscriberID = NULL;
    BCLAddressLogger::Init();
    AliHLTFilteredStdoutLogServer sOut;
    gLogLevel = AliHLTLog::kAll;
    gLog.AddServer( sOut );
    BCLErrorLogCallback logCallback;
    BCLStackTraceCallback traceCallback( 7 );
    bool comFailure = false;
    SubscriberBridgeHeadFailCallback failureCallback( comFailure );
    gLogLevel = AliHLTLog::kWarning|AliHLTLog::kError|AliHLTLog::kFatal;

    BCLMsgCommunication* blobMsgCom = NULL;
    BCLMsgCommunication* msgCom = NULL;
    BCLBlobCommunication* blobCom = NULL;
    BCLAbstrAddressStruct *ownBlobMsgAddress=NULL, 
			     *ownBlobAddress=NULL, 
			      *ownMsgAddress=NULL,
		       *remoteBlobMsgAddress=NULL,
			   *remoteMsgAddress=NULL;

    uint32 blobBufferSize = 0;
    uint32 connectionTimeout = 10000; // Timeout of 10s, given in ms.
    bool nostdout = false;
    bool useShmProxy = false;
    key_t pub2SubProxyShmKey = 0;
    key_t sub2PubProxyShmKey = 0;
    unsigned long proxyShmSize = 0;
    AliUInt32_t blockSize = 1048576;
    AliUInt32_t minSEDDBlockCnt = 1;
    AliUInt32_t maxSEDDBlockCnt = 3;
    AliUInt32_t slotCnt = 256;
    unsigned int slotCntExp2;
    bool noConnect = false;

    i = 1;
    while ( (i < (uint32)argc) && !errorStr )
	{
	if ( !strcmp( argv[i], "-nostdout" ) )
	    {
	    nostdout = true;
	    i++;
	    continue;
	    }
	if ( !strcmp( argv[i], "-noconnect" ) )
	    {
	    noConnect = true;
	    i++;
	    continue;
	    }
	if ( !strcmp( argv[i], "-blob" ) )
	    {
	    if ( (uint32)argc <= i +2 )
		{
		errorStr = "Missing blob address options.";
		break;
		}
	    if ( blobCom )
		{
		errorStr = "Duplicate -blob option.";
		break;
		}
	    blobBufferSize = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting blob buffersize argument.";
		break;
		}
	    bool isBlob;
	    int ret;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+2], com_tmp, ownBlobAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid blob address URL";
		break;
		}
	    if ( !isBlob )
		{
		errorStr = "No msg addresses allowed in client blob URL";
		break;
		}
	    blobCom = (BCLBlobCommunication*)com_tmp;
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-blobmsg" ) )
	    {
	    if ( (uint32)argc <= i +2 )
		{
		errorStr = "Missing blob msg address options.";
		break;
		}
	    if ( blobMsgCom )
		{
		errorStr = "Duplicate -blobmsg option.";
		break;
		}
	    bool isBlob;
	    int ret;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+1], com_tmp, ownBlobMsgAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid blob-msg address URL";
		break;
		}
	    if ( isBlob )
		{
		errorStr = "No blob addresses allowed in client blob-msg URL";
		break;
		}
	    ret = BCLDecodeRemoteURL( argv[i+2], remoteBlobMsgAddress );
	    if ( ret )
		{
		errorStr = "Invalid blob-msg address URL";
		break;
		}
	    blobMsgCom = (BCLMsgCommunication*)com_tmp;
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-msg" ) )
	    {
	    if ( (uint32)argc <= i +2 )
		{
		errorStr = "Missing msg address options.";
		break;
		}
	    if ( msgCom )
		{
		errorStr = "Duplicate -msg option.";
		break;
		}
	    bool isBlob;
	    int ret;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+1], com_tmp, ownMsgAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid msg address URL";
		break;
		}
	    if ( isBlob )
		{
		errorStr = "No blob addresses allowed in msg URL";
		break;
		}
	    ret = BCLDecodeRemoteURL( argv[i+2], remoteMsgAddress );
	    if ( ret )
		{
		errorStr = "Invalid msg address URL";
		break;
		}
	    msgCom = (BCLMsgCommunication*)com_tmp;
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing verbosity level specifier.";
		break;
		}
	    gLogLevel = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting verbosity level specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-subscribe" ) )
	    {
	    if ( (uint32)argc < i +2 )
		{
		errorStr = "Missing publishername or subscriberid specifier.";
		break;
		}
	    publisherName = argv[i+1];
	    subscriberID = argv[i+2];
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-timeout" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing connection timeout specifier.";
		break;
		}
	    connectionTimeout = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting connection timeout specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-blocksize" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing block size specifier.";
		break;
		}
	    blockSize = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting block size specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-mindescriptorblocks" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing minimum event descriptor block count specifier.";
		break;
		}
	    minSEDDBlockCnt = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting minimum event descriptor block count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-maxdescriptorblocks" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing maximum event descriptor block count specifier.";
		break;
		}
	    maxSEDDBlockCnt = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting maximum event descriptor block count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-slotcount" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing (event) slot count specifier.";
		break;
		}
	    slotCnt = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting (event) slot count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-shmproxy" ) )
	    {
	    useShmProxy = true;
	    if ( (uint32)argc <= i+3 )
		{
		errorStr = "Missing publisher-to-subscriber or subscriber-to-publisher proxy-shmkey or proxy-shmsize parameter";
		break;
		}
	    proxyShmSize = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting proxy shmsize specifier..";
		break;
		}
	    pub2SubProxyShmKey = (key_t)strtol( argv[i+2], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting publisher-to-subscriber proxy-shmkey specifier..";
		break;
		}
	    sub2PubProxyShmKey = (key_t)strtol( argv[i+3], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting subscriber-to-publisher proxy-shmkey specifier..";
		break;
		}
	    i += 4;
	    continue;
	    }
	errorStr = "Unknown option";
	}
    if ( (!publisherName || !subscriberID) && !errorStr )
	errorStr = "Must specify -subscribe publishername subscriberid option.";
    if ( (!blobCom || !msgCom || !blobMsgCom) && !errorStr )
	errorStr = "Must specify one each of the blob, blobmsg and msg parameters";

    if ( errorStr )
	{
	LOG( MLUCLog::kError, "SubscriberBridgeHead", "Command line options" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	MLUCString urls;
	vector<MLUCString> msgurls, bloburls;
	BCLGetAllowedURLs( msgurls, bloburls );
	vector<MLUCString>::iterator iter, end;
	iter = msgurls.begin();
	end = msgurls.end();
	while ( iter != end )
	    {
	    urls += *iter;
	    if ( iter+1 != end )
		urls += ", ";
	    iter++;
	    }

	iter = bloburls.begin();
	end = bloburls.end();
	if ( iter != end )
	    urls += ", ";
	while ( iter != end )
	    {
	    urls += *iter;
	    if ( iter+1 != end )
		urls += ", ";
	    iter++;
	    }
	LOG( MLUCLog::kError, "SubscriberBridgeHead", "Command line options" )
	    << "Allowed address URLS: " << urls.c_str() << "." << ENDLOG;
	LOG( MLUCLog::kError, "SubscriberBridgeHead", "Command line options" )
	    << errorStr << ENDLOG;
	return -1;
	}

    if ( nostdout )
	gLog.DelServer( sOut );

    blobCom->SetMsgCommunication( blobMsgCom );

    ofstream of;
    MLUCString name;
    name = "SubscriberBridgeHead-";
    name += publisherName;
    name += "-";
    name += subscriberID;
    name += "-";
    AliHLTFileLogServer fileOut( name.c_str(), ".log", 3000000 );
    gLog.AddServer( fileOut );

//     name = "SubscriberBridgeHead-";
//     name += publisherName;
//     name += "-";
//     name += subscriberID;
    name = publisherName;
    name += "-";
    name += subscriberID;
    name += "Bridge";
    AliHLTPipeController pipeControl( name.c_str() );
    pipeControl.AddValue( *(AliUInt32_t*)&gLogLevel );
    pipeControl.Start();


    msgCom->AddCallback( &logCallback );
    blobMsgCom->AddCallback( &logCallback );
    blobCom->AddCallback( &logCallback );
    msgCom->AddCallback( &traceCallback );
    blobMsgCom->AddCallback( &traceCallback );
    blobCom->AddCallback( &traceCallback );
    blobCom->SetBlobBuffer( blobBufferSize );
    msgCom->AddCallback( &failureCallback );
    blobMsgCom->AddCallback( &failureCallback );
    blobCom->AddCallback( &failureCallback );

    blobMsgCom->Bind( ownBlobMsgAddress );
    blobCom->Bind( ownBlobAddress );
    msgCom->Bind( ownMsgAddress );

    if ( comFailure )
	{
	LOG( AliHLTLog::kFatal, "SubscriberBridgeHead", "Communication error" )
	    << "A communication error occured. Aborting..." << ENDLOG;
	blobCom->Disconnect( remoteBlobMsgAddress );
	msgCom->Disconnect( remoteMsgAddress );
	blobCom->Unbind();
	blobMsgCom->Unbind();
	msgCom->Unbind();
	BCLFreeAddress( remoteMsgAddress );
	BCLFreeAddress( remoteBlobMsgAddress );
	BCLFreeAddress( msgCom, ownMsgAddress );
	BCLFreeAddress( blobCom, ownBlobAddress );
	BCLFreeAddress( blobMsgCom, ownBlobMsgAddress );
	return -1;
	}

    MLUCString subscriberName;
    subscriberName = "SubscriberBridgeHead-";
    subscriberName += subscriberID;
    AliHLTPublisherProxyInterface *publisher;
    AliHLTPublisherPipeProxy *pipePublisher;
    AliHLTPublisherShmProxy *shmPublisher;
    if ( useShmProxy )
	publisher = shmPublisher = new AliHLTPublisherShmProxy( publisherName, proxyShmSize, pub2SubProxyShmKey, sub2PubProxyShmKey );
    else
	publisher = pipePublisher = new AliHLTPublisherPipeProxy( publisherName );

    publisher->SetTimeout( /*1*/000000 );
    if ( !(*publisher) )
	{
	LOG( AliHLTLog::kError, "SubscriberBridgeHead", "Publisher proxy" )
	    << "Error on creation of publisher proxy object for publisher "
	    << publisher->GetName() << "." << ENDLOG;
	return -1;
	}

    AliUInt32_t mask = 0x80000000;
    slotCntExp2 = sizeof(mask)*8-1;
    while ( ! (mask & slotCnt) )
	{
	mask >>= 1;
	slotCntExp2--;
	}
    mask = 1 << slotCntExp2;
    if ( slotCntExp2 < sizeof(mask)*8-1 && slotCnt>mask )
	slotCntExp2++;
    

    AliHLTDescriptorHandler descriptorHandler( slotCntExp2 );
    AliHLTRingBufferManager bufferManager( blockSize );
    AliHLTSubscriberBridgeHead subscriber( subscriberName.c_str(), 10, slotCntExp2 );
    subscriber.SetDescriptorHandler( &descriptorHandler );
    subscriber.SetBufferManager( &bufferManager );
    subscriber.SetMsgCom( msgCom, remoteMsgAddress );
    subscriber.SetBlobCom( blobCom, blobMsgCom, remoteBlobMsgAddress );
    // Thread start msg loop MsgLoop
    SubscriberBridgeHeadThread<AliHLTSubscriberBridgeHead> 
	subscriberMsgThread( &subscriber,
			     &AliHLTSubscriberBridgeHead::MsgLoop );
    subscriberMsgThread.Start();
    if ( !noConnect )
	subscriber.Connect();


    publisher->Subscribe( subscriber );
    publisher->StartPublishing( subscriber );

    if ( subscriber.QuitMsgLoop() )
	subscriberMsgThread.Join();
    else
	subscriberMsgThread.Abort();


    blobCom->Disconnect( remoteBlobMsgAddress );
    msgCom->Disconnect( remoteMsgAddress );
    blobCom->Unbind();
    blobMsgCom->Unbind();
    msgCom->Unbind();
    BCLFreeAddress( remoteMsgAddress );
    BCLFreeAddress( remoteBlobMsgAddress );
    BCLFreeAddress( msgCom, ownMsgAddress );
    BCLFreeAddress( blobCom, ownBlobAddress );
    BCLFreeAddress( blobMsgCom, ownBlobMsgAddress );
    delete publisher;
    return 0;
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
