/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLMsgCommunication.hpp"
#include "BCLBlobCommunication.hpp"
#include "BCLURLAddressDecoder.hpp"
#include "AliHLTPublisherBridgeHead.hpp"
#include "BCLSCIComID.hpp"
#include "AliHLTPubSubPipeCom.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTThread.hpp"
#include "AliHLTPipeController.hpp"
#include "BCLAddressLogger.hpp"
#include "BCLErrorLogCallback.hpp"
#include "BCLStackTraceCallback.hpp"
// XXX
#include "BCLTCPComID.hpp"
#include "BCLTCPBlobCommunication.hpp"
// XXX
#include <fstream>
#include <iostream>
#include <sys/time.h>
#include <unistd.h>
#ifdef LIBC_MEMDEBUG
#include <mcheck.h>
#endif

template<class T>
class PublisherBridgeHeadThread: public AliHLTThread
    {
    public:

	PublisherBridgeHeadThread( T* obj, void (T::*func)(void) )
		{
		fObj = obj;
		fFunc = func;
		}

    protected:

	T* fObj;
	void (T::*fFunc)(void);

	virtual void Run()
		{
		(fObj->*fFunc)();
		}
    };


class PublisherBridgeHeadFailCallback: public BCLErrorCallback
    {
    public:
	
	PublisherBridgeHeadFailCallback( bool& failVar ):
	    fFailVar( failVar )
		{
		fFailVar = false;
		}

	virtual BCLErrorAction ConnectionTemporarilyLost( int, BCLCommunication*,
							  BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction ConnectionError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction BindError( int, BCLCommunication*,
					       BCLAbstrAddressStruct*, 
					     BCLMessageStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction MsgSendError( int, BCLCommunication*,
					       BCLAbstrAddressStruct*, 
					     BCLMessageStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction MsgReceiveError( int, BCLCommunication*,
					       BCLAbstrAddressStruct*, 
						BCLMessageStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction BlobPrepareError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction BlobSendError( int, BCLCommunication*,
					      BCLAbstrAddressStruct*, 
					      BCLTransferID ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction BlobReceiveError( int, BCLCommunication*,
						 BCLTransferID ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction AddressError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction GeneralError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

    protected:
	
	bool& fFailVar;

    };



int main( int argc, char** argv )
    {
#ifdef LIBC_MEMDEBUG
    mtrace();
#endif
    uint32 i;
    char* errorStr = NULL;
    const char* usage1 = "Usage: ";
    const char* usage2 = " [-blobmsg blobmsg-own-address-URL blobmsg-remote-address-URL ] [ -blob blobsize blob-own-address-URL ] [ -msg msg-own-address-URL msg-remote-address-URL ] (-V verbosity_levels) -publisher publishername (-timeout connectiontimeout_ms) (-eventtimeout eventtimeout_ms) (-shm [bigphys|physmem|sysv] <shmkey>) (-nostdout) (-noconnect) (-retrycount <count>) (-retrytime <time-in-milliseconds>) (-publishertimeout <timeout_ms>) (-mindescriptorblocks <minimum descriptor block count>) (-maxdescriptorblocks <maximum descriptor block count>) (-slotcount <(event) slot count>)";
    char* cerr;
    char* publisherName = NULL;
    BCLAddressLogger::Init();
    AliHLTFilteredStdoutLogServer sOut;
    gLogLevel = AliHLTLog::kAll;
    gLog.AddServer( sOut );
    BCLErrorLogCallback logCallback;
    BCLStackTraceCallback traceCallback( 7 );
    bool comFailure = false;
    PublisherBridgeHeadFailCallback failureCallback( comFailure );
    gLogLevel = AliHLTLog::kWarning|AliHLTLog::kError|AliHLTLog::kFatal;

    bool shmAddCopy = false;

    BCLMsgCommunication* blobMsgCom = NULL;
    BCLMsgCommunication* msgCom = NULL;
    BCLBlobCommunication* blobCom = NULL;
    BCLAbstrAddressStruct *ownBlobMsgAddress=NULL, 
			     *ownBlobAddress=NULL, 
			      *ownMsgAddress=NULL,
		       *remoteBlobMsgAddress=NULL,
			   *remoteMsgAddress=NULL;

    uint32 blobBufferSize = 0;
    uint32 connectionTimeout = 10000; // Timeout of 10s, given in ms.
    uint32 eventTimeout = 10000; // Timeout of 10s, given in ms.
    AliUInt32_t shmID = (AliUInt32_t)-1;;
    AliHLTShmID shmKey;
    shmKey.fShmType = kInvalidShmType;
    shmKey.fKey.fID = ~(AliUInt64_t)0;
    bool nostdout = false;
    AliUInt32_t retryCount = 0;
    bool retryCountSet = false;
    AliUInt32_t retryTime = 0;
    bool retryTimeSet = false;
    AliUInt32_t publisherTimeout = ~(AliUInt32_t)0;
    AliUInt32_t minSEDDBlockCnt = 1;
    AliUInt32_t maxSEDDBlockCnt = 3;
    AliUInt32_t slotCnt = 256;
    unsigned int slotCntExp2;
    bool noConnect = false;

    i = 1;
    while ( (i < (uint32)argc) && !errorStr )
	{
	if ( !strcmp( argv[i], "-nostdout" ) )
	    {
	    nostdout = true;
	    i++;
	    continue;
	    }
 	if ( !strcmp( argv[i], "-noconnect" ) )
 	    {
 	    noConnect = true;
 	    i++;
 	    continue;
 	    }
	if ( !strcmp( argv[i], "-blob" ) )
	    {
	    if ( (uint32)argc <= i +2 )
		{
		errorStr = "Missing blob address options.";
		break;
		}
	    if ( blobCom )
		{
		errorStr = "Duplicate -blob option.";
		break;
		}
	    blobBufferSize = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting blob buffersize argument.";
		break;
		}
	    bool isBlob;
	    int ret;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+2], com_tmp, ownBlobAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid blob address URL";
		break;
		}
	    if ( !isBlob )
		{
		errorStr = "No msg addresses allowed in client blob URL";
		break;
		}
	    blobCom = (BCLBlobCommunication*)com_tmp;
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-blobmsg" ) )
	    {
	    if ( (uint32)argc <= i +2 )
		{
		errorStr = "Missing blob msg address options.";
		break;
		}
	    if ( blobMsgCom )
		{
		errorStr = "Duplicate -blobmsg option.";
		break;
		}
	    bool isBlob;
	    int ret;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+1], com_tmp, ownBlobMsgAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid blob-msg address URL";
		break;
		}
	    if ( isBlob )
		{
		errorStr = "No blob addresses allowed in client blob-msg URL";
		break;
		}
	    ret = BCLDecodeRemoteURL( argv[i+2], remoteBlobMsgAddress );
	    if ( ret )
		{
		errorStr = "Invalid blob-msg address URL";
		break;
		}
	    blobMsgCom = (BCLMsgCommunication*)com_tmp;
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-msg" ) )
	    {
	    if ( (uint32)argc <= i +2 )
		{
		errorStr = "Missing msg address options.";
		break;
		}
	    if ( msgCom )
		{
		errorStr = "Duplicate -msg option.";
		break;
		}
	    bool isBlob;
	    int ret;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+1], com_tmp, ownMsgAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid msg address URL";
		break;
		}
	    if ( isBlob )
		{
		errorStr = "No blob addresses allowed in msg URL";
		break;
		}
	    ret = BCLDecodeRemoteURL( argv[i+2], remoteMsgAddress );
	    if ( ret )
		{
		errorStr = "Invalid msg address URL";
		break;
		}
	    msgCom = (BCLMsgCommunication*)com_tmp;
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing verbosity level specifier.";
		break;
		}
	    gLogLevel = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting verbosity level specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-retrytime" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing retry time specifier.";
		break;
		}
	    retryTime = strtoul( argv[i+1], &cerr, 0 );
	    retryTimeSet = true;
	    if ( *cerr )
		{
		errorStr = "Error converting retry time specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-retrycount" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing retry count specifier.";
		break;
		}
	    retryCount = strtoul( argv[i+1], &cerr, 0 );
	    retryCountSet = true;
	    if ( *cerr )
		{
		errorStr = "Error converting retry count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-publishertimeout" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing publisher timeout specifier.";
		break;
		}
	    publisherTimeout = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting publisher timeout specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-shm" ) )
	    {
	    if ( (uint32)argc < i +2 )
		{
		errorStr = "Missing shm key specifier.";
		break;
		}
	    if ( !strcmp( argv[i+1], "bigphys" ) )
		shmKey.fShmType = kBigPhysShmType;
	    else if ( !strcmp( argv[i+1], "physmem" ) )
		shmKey.fShmType = kPhysMemShmType;
	    else if ( !strcmp( argv[i+1], "sysv" ) )
		shmKey.fShmType = kSysVShmType;
	    else
		{
		errorStr = "Invalid shm type specifier...";
		break;
		}
	    shmKey.fKey.fID = strtoul( argv[i+2], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting shm key specifier..";
		break;
		}
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-publisher" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing publishername specifier.";
		break;
		}
	    publisherName = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-timeout" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing connection timeout specifier.";
		break;
		}
	    connectionTimeout = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting connection timeout specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-eventtimeout" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing event timeout specifier.";
		break;
		}
	    eventTimeout = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting event timeout specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-mindescriptorblocks" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing minimum event descriptor block count specifier.";
		break;
		}
	    minSEDDBlockCnt = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting minimum event descriptor block count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-maxdescriptorblocks" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing maximum event descriptor block count specifier.";
		break;
		}
	    maxSEDDBlockCnt = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting maximum event descriptor block count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-slotcount" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing (event) slot count specifier.";
		break;
		}
	    slotCnt = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting (event) slot count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	errorStr = "Unknown option";
	}
    if ( !publisherName && !errorStr)
	errorStr = "Must specify -publisher publishername option.";
    if ( (!blobCom || !msgCom || !blobMsgCom) && !errorStr )
	errorStr = "Must specify one each of the blob, blobmsg and msg parameters";

    if ( errorStr )
	{
	LOG( MLUCLog::kError, "PublisherBridgeHead", "Command line options" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	MLUCString urls;
	vector<MLUCString> msgurls, bloburls;
	BCLGetAllowedURLs( msgurls, bloburls );
	vector<MLUCString>::iterator iter, end;
	iter = msgurls.begin();
	end = msgurls.end();
	while ( iter != end )
	    {
	    urls += *iter;
	    if ( iter+1 != end )
		urls += ", ";
	    iter++;
	    }

	iter = bloburls.begin();
	end = bloburls.end();
	if ( iter != end )
	    urls += ", ";
	while ( iter != end )
	    {
	    urls += *iter;
	    if ( iter+1 != end )
		urls += ", ";
	    iter++;
	    }
	LOG( MLUCLog::kError, "PublisherBridgeHead", "Command line options" )
	    << "Allowed address URLS: " << urls.c_str() << "." << ENDLOG;
	LOG( MLUCLog::kError, "PublisherBridgeHead", "Command line options" )
	    << errorStr << ENDLOG;
	return -1;
	}

    if ( nostdout )
	gLog.DelServer( sOut );

    if ( shmKey.fShmType!=kInvalidShmType && blobCom->GetComID()==kSCIComID )
      shmAddCopy = true;

    blobCom->SetMsgCommunication( blobMsgCom );

    ofstream of;
    MLUCString name;
    name = "PublisherBridgeHead-";
    name += publisherName;
    name += "-";
    AliHLTFileLogServer fileOut( name.c_str(), ".log", 3000000 );
    gLog.AddServer( fileOut );

    //name = "PublisherBridgeHead-";
    //name += publisherName;
    name = publisherName;
    name += "Bridge";
    AliHLTPipeController pipeControl( name.c_str() );
    pipeControl.AddValue( *(AliUInt32_t*)&gLogLevel );
    pipeControl.Start();

    msgCom->AddCallback( &logCallback );
    blobMsgCom->AddCallback( &logCallback );
    blobCom->AddCallback( &logCallback );
    msgCom->AddCallback( &traceCallback );
    blobMsgCom->AddCallback( &traceCallback );
    blobCom->AddCallback( &traceCallback );
    msgCom->AddCallback( &failureCallback );
    blobMsgCom->AddCallback( &failureCallback );
    blobCom->AddCallback( &failureCallback );

    AliHLTSharedMemory shm;
    AliUInt8_t* blobPtr = NULL;
    bool success = false;

    if ( blobCom->GetComID() == kSCIComID && !shmAddCopy )
	{
	blobCom->SetBlobBuffer( blobBufferSize );
	}
    else
	{
	  if ( shmKey.fShmType != kInvalidShmType )
	    {
	      AliUInt32_t size = blobBufferSize;
	      shmID = shm.GetShm( shmKey, size );
	      if ( shmID != (AliUInt32_t)-1 )
		success = true;
	    }
	  else
	      {
	      shmKey.fShmType = kBigPhysShmType;
	      shmKey.fKey.fID = 1;
	      AliUInt32_t size = blobBufferSize;
	      while ( !success )
		  {
		  shmID = shm.GetShm( shmKey, size, true );
		  if ( shmID == (AliUInt32_t)-1 )
		      {
		      if ( size==0 )
			  size = blobBufferSize;
		      else
			  break;
		      }
		  else
		      success = true;
		  if ( shmKey.fKey.fID >= (~(AliUInt64_t)0) - 1 )
		      break;
		  ++shmKey.fKey.fID;
		}
	    }
	if ( success )
	    {
	      //shm.ReleaseShm( shmID );
	      //shmID = shm.GetShm( shmKey, size );
	      blobPtr = (AliUInt8_t*)shm.LockShm( shmID );
	      if ( blobCom->GetComID() != kSCIComID )
		blobCom->SetBlobBuffer( blobBufferSize, blobPtr );
	      else
		blobCom->SetBlobBuffer( blobBufferSize );
	    }
	else
	    {
	    shmKey.fShmType = kInvalidShmType;
	    shmKey.fKey.fID = ~(AliUInt64_t)0;
	    }
	}

    blobMsgCom->Bind( ownBlobMsgAddress );
    blobCom->Bind( ownBlobAddress );
    msgCom->Bind( ownMsgAddress );

    if ( blobCom->GetComID() == kSCIComID && !shmAddCopy )
	{
	AliUInt8_t* pv;
	void* pp;
	pv = blobCom->GetBlobBuffer();
	if ( AliHLTSharedMemory::GetPhysicalAddress( (void*)pv, &pp, NULL ) )
	    {
	    LOG( AliHLTLog::kInformational, "PublisherBridgeHead", "Memories" )
		<< "Virtual address: 0x" << AliHLTLog::kHex << (AliUInt32_t)pv << " - Physical address: 0x"
		<< (AliUInt32_t)pp << ENDLOG;
	    AliUInt32_t size = blobBufferSize;
	    shmKey.fShmType = kPhysMemShmType;
	    shmKey.fKey.fID = reinterpret_cast<AliUInt64_t>(pp);
	    shmID = shm.GetShm( shmKey, size );
	    }
	else
	    {
	    shmKey.fShmType = kInvalidShmType;
	    shmKey.fKey.fID = ~(AliUInt64_t)0;
	    }
	}

    

    if ( comFailure )
	{
	LOG( AliHLTLog::kFatal, "PublisherBridgeHead", "Communication error" )
	    << "A communication error occured. Aborting..." << ENDLOG;
	//blobCom->Disconnect( remoteBlobMsgAddress );
	//msgCom->Disconnect( remoteMsgAddress );
	blobCom->Unbind();
	blobMsgCom->Unbind();
	msgCom->Unbind();
	BCLFreeAddress( remoteMsgAddress );
	BCLFreeAddress( remoteBlobMsgAddress );
	BCLFreeAddress( msgCom, ownMsgAddress );
	BCLFreeAddress( blobCom, ownBlobAddress );
	BCLFreeAddress( blobMsgCom, ownBlobMsgAddress );
	shm.ReleaseShm( shmID );
	return -1;
	}

    AliUInt32_t mask = 0x80000000;
    slotCntExp2 = sizeof(mask)*8-1;
    while ( ! (mask & slotCnt) )
	{
	mask >>= 1;
	slotCntExp2--;
	}
    mask = 1 << slotCntExp2;
    if ( slotCntExp2 < sizeof(mask)*8-1 && slotCnt>mask )
	slotCntExp2++;

    AliHLTDescriptorHandler descriptorHandler( slotCntExp2 );

    name = publisherName;
    AliHLTPublisherBridgeHead publisher( name.c_str(), slotCntExp2 );
    publisher.SetDescriptorHandler( &descriptorHandler );
    if ( shmAddCopy && blobCom->GetComID() == kSCIComID )
      {
	publisher.SetShmCopy( true, blobPtr );
      }
    publisher.SetMsgCom( msgCom, remoteMsgAddress );
    publisher.SetBlobCom( blobCom, blobMsgCom, remoteBlobMsgAddress );
    publisher.SetShmID( shmKey );
    publisher.SetSubscriptionLoop( &PublisherPipeSubscriptionInputLoop );

    PublisherBridgeHeadThread<AliHLTPublisherBridgeHead> 
	publisherMsgThread( &publisher,
			     &AliHLTPublisherBridgeHead::AcceptSubscriptions );
    publisherMsgThread.Start();
    if ( retryTimeSet )
	publisher.SetRetryTime( retryTime );
    if ( retryCountSet )
	publisher.SetMaxRetryCount( retryCount );
    publisher.SetMaxTimeout( publisherTimeout );
    publisher.Start();
    if ( !noConnect )
	publisher.Connect();
    
    publisher.MsgLoop();
    publisher.Stop();

    AliUInt32_t td = 0;
    struct timeval t1, t2;
    gettimeofday( &t1, NULL );
    while ( publisher.GetPendingEventCount()>0 && (eventTimeout<=0 || td<eventTimeout) )
	{
	usleep( 10 );
	gettimeofday( &t2, NULL );
	td = (t2.tv_usec-t1.tv_usec)/1000 + (t2.tv_sec-t1.tv_sec)*1000;
	}

    QuitPipeSubscriptionLoop( publisher );
    LOG( AliHLTLog::kDebug, "PublisherBridgeHead", "Quitted Subscription loop" )
	<< "PipeSubscriptionLoop quitted." << ENDLOG;
    publisher.CancelAllSubscriptions( true );
    LOG( AliHLTLog::kDebug, "PublisherBridgeHead", "Subscriptions Cancelled" )
	<< "All subscriptions cancelled." << ENDLOG;

    //blobCom->Disconnect( remoteBlobMsgAddress );
    //msgCom->Disconnect( remoteMsgAddress );
    blobCom->Unbind();
    blobMsgCom->Unbind();
    msgCom->Unbind();
    BCLFreeAddress( remoteMsgAddress );
    BCLFreeAddress( remoteBlobMsgAddress );
    BCLFreeAddress( msgCom, ownMsgAddress );
    BCLFreeAddress( blobCom, ownBlobAddress );
    BCLFreeAddress( blobMsgCom, ownBlobMsgAddress );
    shm.ReleaseShm( shmID );
    return 0;
    }
    
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
