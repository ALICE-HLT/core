/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLMsgCommunication.hpp"
#include "BCLBlobCommunication.hpp"
#include "BCLURLAddressDecoder.hpp"
#include "AliHLTSubscriberBridgeHead.hpp"
#include "AliHLTPublisherPipeProxy.hpp"
#include "AliHLTPublisherShmProxy.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTThread.hpp"
#include "AliHLTPipeController.hpp"
#include "AliHLTSCInterface.hpp"
#include "AliHLTSCCommands.hpp"
#include "AliHLTRingBufferManager.hpp"
#include "BCLErrorLogCallback.hpp"
#include "BCLStackTraceCallback.hpp"
// XXX
#include "BCLTCPComID.hpp"
#include "BCLTCPBlobCommunication.hpp"
// XXX
#include <fstream>
#include <iostream>
#include <sys/ipc.h>
#include <sys/shm.h>
#ifdef LIBC_MEMDEBUG
#include <mcheck.h>
#endif

template<class T>
class SubscriberBridgeHeadThread: public AliHLTThread
    {
    public:

	SubscriberBridgeHeadThread( T* obj, void (T::*func)(void) )
		{
		fObj = obj;
		fFunc = func;
		}

    protected:

	T* fObj;
	void (T::*fFunc)(void);

	virtual void Run()
		{
		(fObj->*fFunc)();
		}
    };

class SubscriberBridgeHeadFailCallback: public BCLErrorCallback
    {
    public:
	
	SubscriberBridgeHeadFailCallback( bool& failVar ):
	    fFailVar( failVar )
		{
		fFailVar = false;
		}

	virtual BCLErrorAction ConnectionTemporarilyLost( int, BCLCommunication*,
							  BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction ConnectionError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction BindError( int, BCLCommunication*,
					       BCLAbstrAddressStruct*, 
					     BCLMessageStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction MsgSendError( int, BCLCommunication*,
					       BCLAbstrAddressStruct*, 
					     BCLMessageStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction MsgReceiveError( int, BCLCommunication*,
					       BCLAbstrAddressStruct*, 
						BCLMessageStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction BlobPrepareError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction BlobSendError( int, BCLCommunication*,
					      BCLAbstrAddressStruct*, 
					      BCLTransferID ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction BlobReceiveError( int, BCLCommunication*,
						 BCLTransferID ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction AddressError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction GeneralError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

    protected:
	
	bool& fFailVar;

    };


class TolerantSubscriberBridgeHeadLAM: public AliHLTBridgeHeadLAMProxy
    {
    public:

	TolerantSubscriberBridgeHeadLAM( AliHLTSCInterface* interface,
					 vector<BCLAbstrAddressStruct*>& lamAddresses ):
	    fInterface( interface ),
	    fLAMAddresses( lamAddresses )
		{
		}

	virtual void LookAtMe();

    protected:

	AliHLTSCInterface* fInterface;
	vector<BCLAbstrAddressStruct*>& fLAMAddresses;

    };

struct TolerantSubscriberBridgeHeadComData
    {
	TolerantSubscriberBridgeHeadComData()
		{
		fBlobMsgCom = NULL;
		fMsgCom = NULL;
		fBlobCom = NULL;
		fOwnBlobMsgAddress=NULL;
		fOwnBlobAddress=NULL; 
		fOwnMsgAddress=NULL;
		fRemoteBlobMsgAddress=NULL;
		fRemoteMsgAddress=NULL;
		
		fBlobBufferSize = 0;
		//fCommunicationTimeout = 10000; // Timeout of 10s, given in ms.
		fCommunicationTimeout = 0;
		}

	BCLMsgCommunication* fBlobMsgCom;
	BCLMsgCommunication* fMsgCom;
	BCLBlobCommunication* fBlobCom;
	BCLAbstrAddressStruct *fOwnBlobMsgAddress, 
				 *fOwnBlobAddress, 
				  *fOwnMsgAddress,
			   *fRemoteBlobMsgAddress,
			       *fRemoteMsgAddress;
	
	uint32 fBlobBufferSize;
	uint32 fCommunicationTimeout; // Timeout of 10s, given in ms.
    };

class TolerantSubscriberBridgeHeadCommandHandler: public AliHLTSCCommandProcessor
    {
    public:
	
	TolerantSubscriberBridgeHeadCommandHandler( AliHLTSubscriberBridgeHead* bridgeHead,
						    TolerantSubscriberBridgeHeadComData* comData );
	
	virtual void ProcessCmd( AliHLTSCCommandStruct* cmd );
	
    protected:
	
	AliHLTSubscriberBridgeHead* fBridgeHead;
	TolerantSubscriberBridgeHeadComData* fComData;

    };

class TolerantSubscriberBridgeHeadErrorCallback: public AliHLTBridgeHeadErrorCallback<AliHLTSubscriberBridgeHead>
    {
    public:

		TolerantSubscriberBridgeHeadErrorCallback();

		virtual ~TolerantSubscriberBridgeHeadErrorCallback();

		virtual void ConnectionError( AliHLTSubscriberBridgeHead* bridgeHead );
		
		virtual void ConnectionEstablished( AliHLTSubscriberBridgeHead* bridgeHead );
	
    protected:
    private:
    };


void TolerantSubscriberBridgeHeadLAM::LookAtMe()
    {
    vector<BCLAbstrAddressStruct*>::iterator addrIter, addrEnd;
    int ret;

    addrIter = fLAMAddresses.begin();
    addrEnd = fLAMAddresses.end();
    
    while ( addrIter != addrEnd )
	{
	ret = fInterface->LookAtMe( *addrIter );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "TolerantSubscriberBridgeHeadLAM::LookAtMe", "LAM Send Error" )
		<< "Error sending LAM to address ";
		BCLAddressLogger::LogAddress( gLog, **addrIter )
		    << ": " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << ")." << ENDLOG;
	    }
	addrIter++;
	}
    }

TolerantSubscriberBridgeHeadCommandHandler::TolerantSubscriberBridgeHeadCommandHandler( AliHLTSubscriberBridgeHead* bridgeHead,
											TolerantSubscriberBridgeHeadComData* comData ):
    fBridgeHead( bridgeHead ), fComData( comData )
    {
    }
	
void TolerantSubscriberBridgeHeadCommandHandler::ProcessCmd( AliHLTSCCommandStruct* cmd )
    {
    if ( !cmd )
	return;
    AliHLTSCCommand cmdS( cmd );
    
    LOG( AliHLTLog::kDebug, "TolerantSubscriberBridgeHeadCommandHandler::ProcessCmd", "Command received" )
	<< "Command 0x" << AliHLTLog::kHex << cmdS.GetData()->fCmd << " (" << AliHLTLog::kDec
	<< cmdS.GetData()->fCmd << ") received." << ENDLOG;
    char* newMsgComURL = NULL;
    char* newBlobMsgComURL = NULL;
    unsigned long offset = 0;
    unsigned long dataOffset;
    int ret;
    BCLAbstrAddressStruct *newMsgAddr = NULL, 
			  *oldMsgAddr = fComData->fRemoteMsgAddress,
			 *newBlobMsgAddr = NULL,
			 *oldBlobMsgAddr = fComData->fRemoteBlobMsgAddress;

    dataOffset = ((unsigned long)cmd->fData) - ((unsigned long)cmd);
    switch ( cmdS.GetData()->fCmd )
	{
	case kAliHLTSCDisconnectCmd:
	    LOG( AliHLTLog::kDebug, "TolerantSubscriberBridgeHeadCommandHandler::ProcessCmd", "Disconnect command received" )
		<< "Disconnect command received." << ENDLOG;
	    fBridgeHead->Disconnect( true, true, 1000 );
	    break;
	case kAliHLTSCConnectCmd:
	    LOG( AliHLTLog::kDebug, "TolerantSubscriberBridgeHeadCommandHandler::ProcessCmd", "Connect command received" )
		<< "Connect command received." << ENDLOG;
	    fBridgeHead->Connect();
	    break;
	case kAliHLTSCReconnectCmd:
	    LOG( AliHLTLog::kDebug, "TolerantSubscriberBridgeHeadCommandHandler::ProcessCmd", "Reconnect command received" )
		<< "Reconnect command received." << ENDLOG;
	    if ( fBridgeHead )
		{
		fBridgeHead->Pause();
 		fBridgeHead->LockCom();
		fBridgeHead->Disconnect( true, true, 1000, false );
		fBridgeHead->Connect( false );
 		fBridgeHead->UnlockCom();
		fBridgeHead->Restart();
		}
	    break;
	case kAliHLTSCNewConnectionCmd:
	    if ( true )
		{
		LOG( AliHLTLog::kDebug, "TolerantSubscriberBridgeHeadCommandHandler::ProcessCmd", "New connection command received" )
		    << "New connection command received." << ENDLOG;
		}
	    else  // Fallthrough
	case kAliHLTSCNewRemoteAddressesCmd:
                {
		LOG( AliHLTLog::kDebug, "TolerantSubscriberBridgeHeadCommandHandler::ProcessCmd", "New address command received" )
		    << "New address command received." << ENDLOG;
		}
	    if ( fBridgeHead )
		{
		if ( cmdS.GetData()->fParam0 & 1 )
		    {
		    newMsgComURL = (char*)cmd->fData;
		    if ( (strlen(newMsgComURL)+dataOffset) > cmdS.GetData()->fLength )
			{
			LOG( AliHLTLog::kWarning, "TolerantSubscriberBridgeHeadCommandHandler::ProcessCmd", "Length Error" )
			    << "Error (1) in message length: " << AliHLTLog::kDec << (strlen(newMsgComURL)+dataOffset)
			    << " greater than " << cmdS.GetData()->fLength << ENDLOG;
			}
		    else
			{
			LOG( AliHLTLog::kDebug, "TolerantSubscriberBridgeHeadCommandHandler::ProcessCmd", "New msg address" )
			    << "New Msg Address: " << newMsgComURL << "." << ENDLOG;
			}
		    ret = BCLDecodeRemoteURL( newMsgComURL, newMsgAddr );
		    if ( ret || !newMsgAddr )
			{
			LOG( AliHLTLog::kError, "TolerantSubscriberBridgeHeadCommandHandler::ProcessCmd", "Error in msg com URL" )
			    << "Error decoding new remote msg com address URL: " << strerror(ret) 
			    << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
			if ( newMsgAddr )
			    {
			    BCLFreeAddress( newMsgAddr );
			    newMsgAddr = NULL;
			    }
			}
		    else if ( newMsgAddr && fComData->fMsgCom && newMsgAddr->fComID != fComData->fMsgCom->GetComID() )
			{
			LOG( AliHLTLog::kError, "TolerantSubscriberBridgeHeadCommandHandler::ProcessCmd", "Wrong Com ID" )
			    << "Com ID " << AliHLTLog::kDec << newMsgAddr->fComID
			    << " in new remote msg address is unequal to com ID " << fComData->fMsgCom->GetComID()
			    << " in msg com object." << ENDLOG;
			BCLFreeAddress( newMsgAddr );
			newMsgAddr = NULL;
			}
		    offset += strlen( newMsgComURL )+1;
		    }
		if ( cmdS.GetData()->fParam0 & 2 )
		    {
		    newBlobMsgComURL = ((char*)(cmd->fData))+offset;
		    if ( (strlen(newBlobMsgComURL)+dataOffset+offset) > cmdS.GetData()->fLength )
			{
			LOG( AliHLTLog::kWarning, "TolerantSubscriberBridgeHeadCommandHandler::ProcessCmd", "Length Error" )
			    << "Error (2) in message length: " << AliHLTLog::kDec << (strlen(newBlobMsgComURL)+dataOffset+offset)
			    << " greater than " << cmdS.GetData()->fLength << ENDLOG;
			}
		    else
			{
			LOG( AliHLTLog::kDebug, "TolerantSubscriberBridgeHeadCommandHandler::ProcessCmd", "New blob msg address" )
			    << "New Blob Msg Address: " << newBlobMsgComURL << "." << ENDLOG;
			}
		    ret = BCLDecodeRemoteURL( newBlobMsgComURL, newBlobMsgAddr );
		    if ( ret || !newBlobMsgAddr )
			{
			LOG( AliHLTLog::kError, "TolerantSubscriberBridgeHeadCommandHandler::ProcessCmd", "Error in blob msg com URL" )
			    << "Error decoding new remote blob msg com address URL: " << strerror(ret) 
			    << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
			if ( newBlobMsgAddr )
			    {
			    BCLFreeAddress( newBlobMsgAddr );
			    newMsgAddr = NULL;
			    newBlobMsgAddr = NULL;
			    }
			}
		    else if ( newBlobMsgAddr && fComData->fBlobMsgCom && newBlobMsgAddr->fComID != fComData->fBlobMsgCom->GetComID() )
			{
			LOG( AliHLTLog::kError, "TolerantSubscriberBridgeHeadCommandHandler::ProcessCmd", "Wrong Com ID" )
			    << "Com ID " << AliHLTLog::kDec << newBlobMsgAddr->fComID
			    << " in new remote blob msg address is unequal to com ID " << fComData->fBlobMsgCom->GetComID()
			    << " in msg com object." << ENDLOG;
			BCLFreeAddress( newBlobMsgAddr );
			newMsgAddr = NULL;
			newBlobMsgAddr = NULL;
			}
		    }

		fBridgeHead->Pause();
 		fBridgeHead->LockCom();
		fBridgeHead->Disconnect( true, true, 1000, false );
		if ( newMsgAddr )
		    {
		    fBridgeHead->SetRemoteMsgAddress( newMsgAddr );
		    fComData->fRemoteMsgAddress = newMsgAddr;
		    }
		if ( newBlobMsgAddr )
		    {
		    fBridgeHead->SetRemoteBlobMsgAddress( newBlobMsgAddr );
		    fComData->fRemoteBlobMsgAddress = newBlobMsgAddr;
		    }
		if ( cmdS.GetData()->fCmd == kAliHLTSCNewConnectionCmd )
		    fBridgeHead->Connect( false ); // No connect in case of new address command.
		fBridgeHead->UnlockCom();
		fBridgeHead->Restart();
		if ( newMsgAddr )
		    {
		    BCLFreeAddress( oldMsgAddr );
		    }
		if ( newBlobMsgAddr )
		    {
		    BCLFreeAddress( oldBlobMsgAddr );
		    }
		}
	    break;
	case kAliHLTSCPURGEALLEVENTSCmd:
	    LOG( AliHLTLog::kInformational, "TolerantSubscriberBridgeHeadCommandHandler::ProcessCmd", "PURGE command received" )
		<< "PURGE ALL EVENTS command received." << ENDLOG;
	    if ( fBridgeHead )
		fBridgeHead->PURGEALLEVENTS();
	    LOG( AliHLTLog::kInformational, "TolerantSubscriberBridgeHeadCommandHandler::ProcessCmd", "ALL EVENTS PURGED" )
		<< "ALL EVENTS PURGED!!" << ENDLOG;
	    break;
	}
    }


TolerantSubscriberBridgeHeadErrorCallback::TolerantSubscriberBridgeHeadErrorCallback()
    {
    }

TolerantSubscriberBridgeHeadErrorCallback::~TolerantSubscriberBridgeHeadErrorCallback()
    {
    }

void TolerantSubscriberBridgeHeadErrorCallback::ConnectionError( AliHLTSubscriberBridgeHead* bridgeHead )
    {
    if ( bridgeHead )
	{
	LOG( AliHLTLog::kWarning, "TolerantSubscriberBridgeHeadErrorCallback::ConnectionError", "Connection Error" )
	    << "Connection Error in SubscriberBridgeHead. Pausing processing." << ENDLOG;
	bridgeHead->Pause();
	bridgeHead->LockCom();
	bridgeHead->Disconnect( true, true, 1000, false );
	bridgeHead->UnlockCom();
	}
    }
		
void TolerantSubscriberBridgeHeadErrorCallback::ConnectionEstablished( AliHLTSubscriberBridgeHead* bridgeHead )
    {
    if ( bridgeHead )
	{
	bridgeHead->Restart();
	}
    }



int main( int argc, char** argv )
    {
#ifdef LIBC_MEMDEBUG
    mtrace();
#endif
    uint32 i;
    char* errorStr = NULL;
    const char* usage1 = "Usage: ";
    const char* usage2 = " -blobmsg <blobmsg-own-address-URL> <blobmsg-remote-address-URL> -blob <blobsize> <blob-own-address-URL> -msg <msg-own-address-URL> <msg-remote-address-URL> (-V <verbosity_levels>) -subscribe <publishername> <subscriberid> (-timeout <connectiontimeout_ms>) (-nostdout) (-blocksize <blocksize>) (-mindescriptorblocks <minimum descriptor block count>) (-maxdescriptorblocks <maximum descriptor block count>) (-slotcount <(event) slot count>) (-noconnect) (-shmproxy <shmsize> <publisher-to-subscriber-shmkey> <subscriber-to-publisher-shmkey>) (-retrytime <time-in-milliseconds>) -controlurl <System-Control-URL> (-lamurl <LAM-URL>)*";
    char* cerr;
    char* publisherName = NULL;
    char* subscriberID = NULL;
    AliHLTFilteredStdoutLogServer sOut;
    gLogLevel = AliHLTLog::kAll;
    gLog.AddServer( sOut );
    BCLErrorLogCallback logCallback;
    BCLStackTraceCallback traceCallback( 7 );
    bool comFailure = false;
    SubscriberBridgeHeadFailCallback failureCallback( comFailure );
    gLogLevel = AliHLTLog::kWarning|AliHLTLog::kError|AliHLTLog::kFatal;

    BCLAddressLogger::Init();

    TolerantSubscriberBridgeHeadComData comData;
    bool nostdout = false;
    bool useShmProxy = false;
    key_t pub2SubProxyShmKey = 0;
    key_t sub2PubProxyShmKey = 0;
    unsigned long proxyShmSize = 0;
    AliUInt32_t blockSize = 1048576;
    AliUInt32_t minSEDDBlockCnt = 1;
    AliUInt32_t maxSEDDBlockCnt = 3;
    AliUInt32_t slotCnt = 256;
    unsigned int slotCntExp2;
    vector<BCLAbstrAddressStruct*> lamAddresses;
    char* controlAddressURL = NULL;
    bool noConnect = false;
    int ret;
    AliUInt32_t retryTime = 0;
    bool retryTimeSet = false;

    i = 1;
    while ( (i < (uint32)argc) && !errorStr )
	{
	if ( !strcmp( argv[i], "-retrytime" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing retry time specifier.";
		break;
		}
	    retryTime = strtoul( argv[i+1], &cerr, 0 );
	    retryTimeSet = true;
	    if ( *cerr )
		{
		errorStr = "Error converting retry time specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-nostdout" ) )
	    {
	    nostdout = true;
	    i++;
	    continue;
	    }
	if ( !strcmp( argv[i], "-noconnect" ) )
	    {
	    noConnect = true;
	    i++;
	    continue;
	    }
	if ( !strcmp( argv[i], "-controlurl" ) )
	    {
	    if (  (uint32)argc <= i+1 )
		{
		errorStr = "Missing system control URL parameter";
		break;
		}
	    controlAddressURL = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-lamurl" ) )
	    {
	    BCLAbstrAddressStruct* tmpLAM = NULL;
	    if (  (uint32)argc <= i+1 )
		{
		errorStr = "Missing LAM address URL parameter";
		break;
		}
	    //LAMURL = argv[i+1];
            ret = BCLDecodeRemoteURL( argv[i+1], tmpLAM );
	    if ( ret || !tmpLAM )
	      {
	      errorStr = "Error decoding LAM address URL parameter.";
	      break;
	      }
	    lamAddresses.insert( lamAddresses.end(), tmpLAM );
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-blob" ) )
	    {
	    if ( (uint32)argc <= i +2 )
		{
		errorStr = "Missing blob address options.";
		break;
		}
	    if ( comData.fBlobCom )
		{
		errorStr = "Duplicate -blob option.";
		break;
		}
	    comData.fBlobBufferSize = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting blob buffersize argument.";
		break;
		}
	    bool isBlob;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+2], com_tmp, comData.fOwnBlobAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid blob address URL";
		break;
		}
	    if ( !isBlob )
		{
		errorStr = "No msg addresses allowed in client blob URL";
		break;
		}
	    comData.fBlobCom = (BCLBlobCommunication*)com_tmp;
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-blobmsg" ) )
	    {
	    if ( (uint32)argc <= i +2 )
		{
		errorStr = "Missing blob msg address options.";
		break;
		}
	    if ( comData.fBlobMsgCom )
		{
		errorStr = "Duplicate -blobmsg option.";
		break;
		}
	    bool isBlob;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+1], com_tmp, comData.fOwnBlobMsgAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid blob-msg address URL";
		break;
		}
	    if ( isBlob )
		{
		errorStr = "No blob addresses allowed in client blob-msg URL";
		break;
		}
	    ret = BCLDecodeRemoteURL( argv[i+2], comData.fRemoteBlobMsgAddress );
	    if ( ret )
		{
		errorStr = "Invalid blob-msg address URL";
		break;
		}
	    comData.fBlobMsgCom = (BCLMsgCommunication*)com_tmp;
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-msg" ) )
	    {
	    if ( (uint32)argc <= i +2 )
		{
		errorStr = "Missing msg address options.";
		break;
		}
	    if ( comData.fMsgCom )
		{
		errorStr = "Duplicate -msg option.";
		break;
		}
	    bool isBlob;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+1], com_tmp, comData.fOwnMsgAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid msg address URL";
		break;
		}
	    if ( isBlob )
		{
		errorStr = "No blob addresses allowed in msg URL";
		break;
		}
	    ret = BCLDecodeRemoteURL( argv[i+2], comData.fRemoteMsgAddress );
	    if ( ret )
		{
		errorStr = "Invalid msg address URL";
		break;
		}
	    comData.fMsgCom = (BCLMsgCommunication*)com_tmp;
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing verbosity level specifier.";
		break;
		}
	    gLogLevel = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting verbosity level specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-subscribe" ) )
	    {
	    if ( (uint32)argc < i +2 )
		{
		errorStr = "Missing publishername or subscriberid specifier.";
		break;
		}
	    publisherName = argv[i+1];
	    subscriberID = argv[i+2];
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-timeout" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing connection timeout specifier.";
		break;
		}
	    comData.fCommunicationTimeout = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting connection timeout specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-blocksize" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing block size specifier.";
		break;
		}
	    blockSize = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting block size specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-mindescriptorblocks" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing minimum event descriptor block count specifier.";
		break;
		}
	    minSEDDBlockCnt = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting minimum event descriptor block count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-maxdescriptorblocks" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing maximum event descriptor block count specifier.";
		break;
		}
	    maxSEDDBlockCnt = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting maximum event descriptor block count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-slotcount" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing (event) slot count specifier.";
		break;
		}
	    slotCnt = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting (event) slot count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-shmproxy" ) )
	    {
	    useShmProxy = true;
	    if ( (uint32)argc <= i+3 )
		{
		errorStr = "Missing publisher-to-subscriber or subscriber-to-publisher proxy-shmkey or proxy-shmsize parameter";
		break;
		}
	    proxyShmSize = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting proxy shmsize specifier..";
		break;
		}
	    pub2SubProxyShmKey = (key_t)strtol( argv[i+2], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting publisher-to-subscriber proxy-shmkey specifier..";
		break;
		}
	    sub2PubProxyShmKey = (key_t)strtol( argv[i+3], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting subscriber-to-publisher proxy-shmkey specifier..";
		break;
		}
	    i += 4;
	    continue;
	    }
	errorStr = "Unknown option";
	}
    if ( !errorStr )
	{
	if ( !publisherName || !subscriberID )
	    errorStr = "Must specify -subscribe publishername subscriberid option.";
	if ( !comData.fBlobCom || !comData.fMsgCom || !comData.fBlobMsgCom )
	    errorStr = "Must specify one each of the blob, blobmsg and msg parameters";
	if ( !controlAddressURL )
	    errorStr = "Must specify the -controlurl option.";
	}

    if ( errorStr )
	{
	LOG( MLUCLog::kError, "SubscriberBridgeHead", "Command line options" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	MLUCString urls;
	vector<MLUCString> msgurls, bloburls;
	BCLGetAllowedURLs( msgurls, bloburls );
	vector<MLUCString>::iterator iter, end;
	iter = msgurls.begin();
	end = msgurls.end();
	while ( iter != end )
	    {
	    urls += *iter;
	    if ( iter+1 != end )
		urls += ", ";
	    iter++;
	    }

	iter = bloburls.begin();
	end = bloburls.end();
	if ( iter != end )
	    urls += ", ";
	while ( iter != end )
	    {
	    urls += *iter;
	    if ( iter+1 != end )
		urls += ", ";
	    iter++;
	    }
	LOG( MLUCLog::kError, "SubscriberBridgeHead", "Command line options" )
	    << "Allowed address URLS: " << urls.c_str() << "." << ENDLOG;
	LOG( MLUCLog::kError, "SubscriberBridgeHead", "Command line options" )
	    << errorStr << ENDLOG;
	return -1;
	}

    if ( nostdout )
	gLog.DelServer( sOut );

    comData.fBlobCom->SetMsgCommunication( comData.fBlobMsgCom );


    ofstream of;
    MLUCString name;
    name = "SubscriberBridgeHead-";
    name += publisherName;
    name += "-";
    name += subscriberID;
    name += "-";
    AliHLTFileLogServer fileOut( name.c_str(), ".log", 3000000 );
    gLog.AddServer( fileOut );

//     name = "SubscriberBridgeHead-";
//     name += publisherName;
//     name += "-";
//     name += subscriberID;
    name = publisherName;
    name += "-";
    name += subscriberID;
    name += "Bridge";
    AliHLTPipeController pipeControl( name.c_str() );
    pipeControl.AddValue( *(AliUInt32_t*)&gLogLevel );
    pipeControl.Start();


    comData.fMsgCom->AddCallback( &logCallback );
    comData.fBlobMsgCom->AddCallback( &logCallback );
    comData.fBlobCom->AddCallback( &logCallback );
    comData.fMsgCom->AddCallback( &traceCallback );
    comData.fBlobMsgCom->AddCallback( &traceCallback );
    comData.fBlobCom->AddCallback( &traceCallback );
    comData.fBlobCom->SetBlobBuffer( comData.fBlobBufferSize );
    comData.fMsgCom->AddCallback( &failureCallback );
    comData.fBlobMsgCom->AddCallback( &failureCallback );
    comData.fBlobCom->AddCallback( &failureCallback );
    comData.fMsgCom->SetReceiveTimeout( comData.fCommunicationTimeout );
    comData.fBlobMsgCom->SetReceiveTimeout( comData.fCommunicationTimeout );
    comData.fBlobCom->SetReceiveTimeout( comData.fCommunicationTimeout );

    LOG( AliHLTLog::kDebug, "SubscriberBridgeHead", "Communication Timeout" )
	<< "Communication timeout: " << AliHLTLog::kDec << comData.fCommunicationTimeout
	<< "." << ENDLOG;

    comData.fBlobMsgCom->Bind( comData.fOwnBlobMsgAddress );
    comData.fBlobCom->Bind( comData.fOwnBlobAddress );
    comData.fMsgCom->Bind( comData.fOwnMsgAddress );

    if ( comFailure )
	{
	LOG( AliHLTLog::kFatal, "SubscriberBridgeHead", "Communication error" )
	    << "A communication error occured. Aborting..." << ENDLOG;
	comData.fBlobCom->Disconnect( comData.fRemoteBlobMsgAddress );
	comData.fMsgCom->Disconnect( comData.fRemoteMsgAddress );
	comData.fBlobCom->Unbind();
	comData.fBlobMsgCom->Unbind();
	comData.fMsgCom->Unbind();
	BCLFreeAddress( comData.fRemoteMsgAddress );
	BCLFreeAddress( comData.fRemoteBlobMsgAddress );
	BCLFreeAddress( comData.fMsgCom, comData.fOwnMsgAddress );
	BCLFreeAddress( comData.fBlobCom, comData.fOwnBlobAddress );
	BCLFreeAddress( comData.fBlobMsgCom, comData.fOwnBlobMsgAddress );
	return -1;
	}

    MLUCString subscriberName;
    subscriberName = "SubscriberBridgeHead-";
    subscriberName += subscriberID;
    AliHLTPublisherProxyInterface *publisher;
//     AliHLTPublisherPipeProxy pipePublisher( publisherName );
//     AliHLTPublisherShmProxy shmPublisher( publisherName, proxyShmSize, pub2SubProxyShmKey, sub2PubProxyShmKey );
    if ( useShmProxy )
	publisher = new AliHLTPublisherShmProxy( publisherName, proxyShmSize, pub2SubProxyShmKey, sub2PubProxyShmKey );
    else
	publisher = new AliHLTPublisherPipeProxy( publisherName );

    if ( !publisher )
	{
	LOG( AliHLTLog::kError, "SubscriberBridgeHead", "Publisher proxy creation failed" )
	    << "Error on creation of publisher proxy object for publisher. Out of memory." 
	    << ENDLOG;
	return -1;
	}

    publisher->SetTimeout( /*1*/000000 );
    if ( !(*publisher) )
	{
	LOG( AliHLTLog::kError, "SubscriberBridgeHead", "Publisher proxy" )
	    << "Error on creation of publisher proxy object for publisher "
	    << publisher->GetName() << "." << ENDLOG;
	return -1;
	}

    AliUInt32_t mask = 0x80000000;
    slotCntExp2 = sizeof(mask)*8-1;
    while ( ! (mask & slotCnt) )
	{
	mask >>= 1;
	slotCntExp2--;
	}
    mask = 1 << slotCntExp2;
    if ( slotCntExp2 < sizeof(mask)*8-1 && slotCnt>mask )
	slotCntExp2++;
    

    AliHLTBridgeStatusData bridgeStatus;
    AliHLTSCInterface scInterface;
    scInterface.SetStatusData( &(bridgeStatus.fHeader) );
    TolerantSubscriberBridgeHeadLAM lamHandler( &scInterface, lamAddresses );
    AliHLTDescriptorHandler descriptorHandler( slotCntExp2 );
    AliHLTRingBufferManager bufferManager( blockSize );
    TolerantSubscriberBridgeHeadErrorCallback errorCallback;
    AliHLTSubscriberBridgeHead subscriber( subscriberName.c_str(), 10, slotCntExp2 );
    subscriber.SetDescriptorHandler( &descriptorHandler );
    subscriber.SetBufferManager( &bufferManager );
    subscriber.SetMsgCom( comData.fMsgCom, comData.fRemoteMsgAddress );
    subscriber.SetBlobCom( comData.fBlobCom, comData.fBlobMsgCom, comData.fRemoteBlobMsgAddress );
    subscriber.SetLAMProxy( &lamHandler );
    subscriber.SetStatusData( &bridgeStatus );
    subscriber.SetCommunicationTimeout( comData.fCommunicationTimeout );
    subscriber.SetErrorCallback( &errorCallback );
    if ( retryTimeSet )
	subscriber.SetRetryTime( retryTime );

    TolerantSubscriberBridgeHeadCommandHandler cmdHandler( &subscriber, &comData );
    scInterface.AddCommandProcessor( &cmdHandler );

    // Thread start msg loop MsgLoop
    SubscriberBridgeHeadThread<AliHLTSubscriberBridgeHead> 
	subscriberMsgThread( &subscriber,
			     &AliHLTSubscriberBridgeHead::MsgLoop );

    scInterface.Bind( controlAddressURL );
    scInterface.Start();
    subscriberMsgThread.Start();
    if ( !noConnect )
	subscriber.Connect();


    publisher->Subscribe( subscriber );
    publisher->StartPublishing( subscriber );

    if ( subscriber.QuitMsgLoop() )
	subscriberMsgThread.Join();
    else
	subscriberMsgThread.Abort();

    scInterface.Stop();


    comData.fBlobCom->Disconnect( comData.fRemoteBlobMsgAddress );
    comData.fMsgCom->Disconnect( comData.fRemoteMsgAddress );
    comData.fBlobCom->Unbind();
    comData.fBlobMsgCom->Unbind();
    comData.fMsgCom->Unbind();
    BCLFreeAddress( comData.fRemoteMsgAddress );
    BCLFreeAddress( comData.fRemoteBlobMsgAddress );
    BCLFreeAddress( comData.fMsgCom, comData.fOwnMsgAddress );
    BCLFreeAddress( comData.fBlobCom, comData.fOwnBlobAddress );
    BCLFreeAddress( comData.fBlobMsgCom, comData.fOwnBlobMsgAddress );
    return 0;
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
