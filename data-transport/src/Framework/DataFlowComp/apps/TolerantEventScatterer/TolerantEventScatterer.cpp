/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTEventDataType.h"
#include "AliEventID.h"
#include "AliHLTThread.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTPubSubPipeCom.hpp"
#include "AliHLTPublisherPipeProxy.hpp"
#include "AliHLTEventScatterer.hpp"
#include "AliHLTEventScattererSubscriber.hpp"
#include "AliHLTEventScattererRePublisher.hpp"
#include "AliHLTTolerantRoundRobinEventScatterer.hpp"
#include "AliHLTPipeController.hpp"
#include "BCLAbstrAddress.hpp"
#include "BCLURLAddressDecoder.hpp"
#include <iostream>
#include <fstream>
#ifdef LIBC_MEMDEBUG
#include <mcheck.h>
#endif


class SubscriptionListenerThread: public AliHLTThread
    {
    public:

	SubscriptionListenerThread( AliHLTEventScattererRePublisher* pub  ):
	    fPub( pub )
		{
		}

    protected:

	virtual void Run()
		{
		if ( fPub )
		    fPub->AcceptSubscriptions();
		}

	AliHLTEventScattererRePublisher* fPub;

    };

struct PublishData
    {
	char* fPublisherName;
	AliHLTEventScattererRePublisher* fPublisher;
	SubscriptionListenerThread* fThread;
    };

class LAMProxy: public AliHLTTolerantRoundRobinEventScattererLAMProxy
    {
    public:

	LAMProxy( AliHLTSCInterface* interface, BCLAbstrAddressStruct* lamAddress )
		{
		fInterface = interface;
		fLAMAddress = lamAddress;
		}
	virtual ~LAMProxy()
		{
		}

	virtual void LookAtMe()
		{
		fInterface->LookAtMe( fLAMAddress );
		}

    protected:
	
	AliHLTSCInterface* fInterface;
	
	BCLAbstrAddressStruct* fLAMAddress;

    };



int main( int argc, char** argv )
    {
#ifdef LIBC_MEMDEBUG
    mtrace();
#endif
    int i;
    AliHLTFilteredStdoutLogServer sOut;
    gLogLevel = AliHLTLog::kAll;
    gLog.AddServer( sOut );

    int ret;
    char* publisherName = NULL;
    char* subscriberID = NULL;
    char* errorStr = NULL;
    char* cerr;
    unsigned long eventCountPreAlloc = 1024;
    unsigned eventSizeMinPreAlloc = 1;
    unsigned eventSizeMaxPreAlloc = 4;
    char* controlAddressURL = NULL;
    char* LAMURL = NULL;
    BCLAbstrAddressStruct* lamAddress = NULL;
    vector<PublishData> publishers;
    const char* usage1 = "Usage: ";
    const char* usage2 = " (-V <verbosity>) -subscribe <publishername> <subscriberid> -controlurl <System-Control-URL> (-eventcount <pre-alloc-events-min-blockcount> <pre-alloc-events-max-blockcount> <event-count-to-pre-alloc>) (-comtimeout <pub-sub-communication-timeout_usec>) (-lamurl <LAM-URL>) (-nostdout) (-slotslog2 <eventslotcount>) -publisher <publishername1> (-publisher <publishername2>)  ...";
    int slotsLog2 = -1;
#if 0
    int slotsLog2Pub;
#endif
    bool nostdout = false;
    bool comTimeoutSet = false;
    unsigned long comTimeout = 0;
    
    i = 1;
    while ( (i < argc) && !errorStr )
	{
	if ( !strcmp( argv[i], "-nostdout" ) )
	    {
	    nostdout = true;
	    i++;
	    continue;
	    }
	if ( !strcmp( argv[i], "-controlurl" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing system control URL parameter";
		break;
		}
	    controlAddressURL = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-eventcount" ) )
	    {
	    if ( argc <= i+3 )
		{
		errorStr = "Missing a pre-allocation event parameter";
		break;
		}
	    eventSizeMinPreAlloc = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting pre-allocation minimum event block count specifier..";
		break;
		}
	    eventSizeMaxPreAlloc = strtoul( argv[i+2], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting pre-allocation maximum event block count specifier..";
		break;
		}
	    eventCountPreAlloc = strtoul( argv[i+3], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting pre-allocation event count specifier..";
		break;
		}
	    i += 4;
	    continue;
	    }
	if ( !strcmp( argv[i], "-lamurl" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing LAM address URL parameter";
		break;
		}
	    LAMURL = argv[i+1];
	    ret = BCLDecodeRemoteURL( LAMURL, lamAddress );
	    if ( ret )
		{
		errorStr = "Error decoding LAM address URL parameter";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-comtimeout" ) )
	    {
	    if ( argc < i +1 )
		{
		errorStr = "Missing publisher subscriber communication timeout specifier.";
		break;
		}
	    comTimeoutSet = true;
	    comTimeout = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting communication timeout specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-publisher" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing publishername parameter";
		break;
		}
	    PublishData pd;
	    pd.fPublisherName = argv[i+1];
	    pd.fPublisher = NULL;
	    pd.fThread = NULL;
	    publishers.insert( publishers.end(), pd );
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-subscribe" ) )
	    {
	    if ( argc <= i+2 )
		{
		errorStr = "Missing publishername or subscriberid parameter";
		break;
		}
	    publisherName = argv[i+1];
	    subscriberID = argv[i+2];
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-slotslog2" ) )
	    {
	    if ( argc <= i +1 )
		{
		errorStr = "Missing slot count (log2) specifier specifier.";
		break;
		}
	    slotsLog2 = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting slot count (log2) specifier specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc <= i +1 )
		{
		errorStr = "Missing verbosity level specifier.";
		break;
		}
	    gLogLevel = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting verbosity level specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	errorStr = "Unknown parameter";
	}
    if ( !errorStr )
	{
	if ( publishers.size()<=0 )
	    {
	    errorStr = "Must specify at least one -publisher publishername argument";
	    }
	if ( !subscriberID || !publisherName )
	    errorStr = "Must specify the -subscribe publishername subscriberid argument";
	}
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "EventScatterer", "Command line parameters" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	LOG( AliHLTLog::kError, "EventScatterer", "Command line parameters" )
	    << errorStr << ENDLOG;
	return -1;
	}

    if ( nostdout )
	{
	gLog.DelServer( sOut );
	}

#if 0
    // This was to restrict the amount of event slots for each subscriber
    // to a fraction of the global one, but in the events of faults, this might
    // actually be not enough.
    int cntExp2;
    unsigned long mask;
    cntExp2 = sizeof(mask)*8-1;
    mask = 0x80000000;
    while ( mask>0 && !(mask & publishers.size()) )
	{
	mask >>= 1;
	cntExp2--;
	}
    slotsLog2Pub = slotsLog2-cntExp2;
#endif

    MLUCString name = "EventScatterer-";
    name += publisherName;
    name += "-";
    name += subscriberID;
    name += "-";
    AliHLTFileLogServer fileOut( name.c_str(), ".log", 3000000 );
    gLog.AddServer( fileOut );

    AliHLTDescriptorHandler descriptors( eventCountPreAlloc );
    AliHLTSCInterface scInterface;

    LAMProxy lamProxy( &scInterface, lamAddress );

    AliHLTToleranceStatus tmpStatus;
    AliHLTToleranceStatus* status = (AliHLTToleranceStatus*)new AliUInt8_t[ tmpStatus.fHeader.fLength+sizeof(AliUInt32_t)*publishers.size() ];
    if ( !status )
	{
	LOG( AliHLTLog::kError, "EventScatterer", "Out Of Memory" )
	    << "Out of memory trying to allocate status structure of "
	    << AliHLTLog::kDec << tmpStatus.fHeader.fLength+sizeof(AliUInt32_t)*publishers.size()
	    << " bytes." << ENDLOG;
	}
    else
	{
	memcpy( status, &tmpStatus, tmpStatus.fHeader.fLength );
	status->fChannelStatus.fChannelsCnt = publishers.size();
	status->fHeader.fLength += sizeof(AliUInt32_t)*publishers.size();
	status->fChannelStatus.fLength += sizeof(AliUInt32_t)*publishers.size();
	for ( unsigned long k = 0; k < status->fChannelStatus.fChannelsCnt; k++ )
	    status->fChannelStatus.fChannels[k] = 1;
	}

    name = "TolerantScatterer-";
    //name += publisherName;
    //name += "-";
    name += subscriberID;
//     name = "EventScatterer-";
//     name += subscriberID;
    AliHLTPipeController pipeControl( name.c_str() );
    pipeControl.AddValue( *(AliUInt32_t*)&gLogLevel );
    pipeControl.Start();

    scInterface.Bind( controlAddressURL );
    scInterface.Start();
    scInterface.SetStatusData( &(status->fHeader) );

    AliHLTTolerantRoundRobinEventScatterer dispatcher;
    name = "TolerantScatterer-";
    name += subscriberID;
    AliHLTEventScattererSubscriber subscriber( name.c_str(), 10, 4 );
    AliHLTPublisherPipeProxy publisher( publisherName );

    dispatcher.SetSubscriber( &subscriber );
    dispatcher.SetupCommandProcessor( &scInterface );
    dispatcher.SetDescriptorHandler( &descriptors );
    dispatcher.SetLAMProxy( &lamProxy );
    if ( status )
	dispatcher.SetStatus( status );

    subscriber.SetScatterer( &dispatcher );
    subscriber.SetPublisher( publisher );
    
    vector<PublishData>::iterator iter, end;
    iter = publishers.begin();
    end = publishers.end();
    while ( iter != end )
	{
	iter->fPublisher = new AliHLTEventScattererRePublisher( iter->fPublisherName, slotsLog2 );
	if ( comTimeoutSet && iter->fPublisher )
	    iter->fPublisher->SetComTimeout( comTimeout );
	iter->fPublisher->SetSubscriptionLoop( &PublisherPipeSubscriptionInputLoop );
	iter->fThread = new SubscriptionListenerThread( iter->fPublisher );
	iter->fPublisher->SetScatterer( &dispatcher );
	iter->fPublisher->SetPrePublisher( publisher );
	iter->fPublisher->SetPreSubscriber( subscriber );
	iter->fThread->Start();
	dispatcher.AddPublisher( iter->fPublisher );
	iter++;
	}

    subscriber.StartProcessing();
    publisher.Subscribe( subscriber );
    
    publisher.StartPublishing( subscriber );
    subscriber.StopProcessing();

    iter = publishers.begin();
    end = publishers.end();
    while ( iter != end )
	{
	QuitPipeSubscriptionLoop( *(iter->fPublisher) );
	delete iter->fPublisher;
	delete iter->fThread;
	iter++;
	}

    scInterface.Stop();
    scInterface.Unbind();

    return 0;
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
