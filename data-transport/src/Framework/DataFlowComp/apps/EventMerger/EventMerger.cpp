/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTEventDataType.h"
#include "AliHLTEventMerger.hpp"
#include "AliHLTEventMergerSubscriber.hpp"
#include "AliHLTEventMergerRePublisher.hpp"
#include "AliEventID.h"
#include "AliHLTThread.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTPubSubPipeCom.hpp"
#include "AliHLTPublisherPipeProxy.hpp"
#include "AliHLTPipeController.hpp"
#include <iostream>
#include <fstream>
#ifdef LIBC_MEMDEBUG
#include <mcheck.h>
#endif

AliHLTSubEventDataDescriptor* MakeSEDD( AliEventID_t eventID, vector<AliHLTDetectorSubscriber::BlockData>& dataBlocks );
void FreeSEDD( AliHLTSubEventDataDescriptor* sedd );

class SubscriptionThread: public AliHLTThread
    {
    public:

	SubscriptionThread( AliHLTPublisherInterface* pub, AliHLTSubscriberInterface* sub  ):
	    fPub( pub ), fSub( sub )
		{
		}

    protected:

	virtual void Run()
		{
		if ( fPub && fSub )
		    fPub->StartPublishing( *fSub );
		}

	AliHLTPublisherInterface* fPub;
	AliHLTSubscriberInterface* fSub;

    };

class SubscriptionListenerThread: public AliHLTThread
    {
    public:

	SubscriptionListenerThread( AliHLTSamplePublisher* pub  ):
	    fPub( pub )
		{
		}

    protected:

	virtual void Run()
		{
		if ( fPub )
		    fPub->AcceptSubscriptions();
		}

	AliHLTSamplePublisher* fPub;

    };


struct SubscribeData
    {
	char* fPublisherName;
	char* fSubscriberID;
	AliHLTPublisherInterface* fPublisher;
	AliHLTEventMergerSubscriber* fSubscriber;
	SubscriptionThread* fThread;
    };



int main( int argc, char** argv )
    {
#ifdef LIBC_MEMDEBUG
    mtrace();
#endif
    int i;
    AliHLTFilteredStdoutLogServer sOut;
    gLogLevel = AliHLTLog::kAll;
    gLog.AddServer( sOut );


    char* publisherName = NULL;
    char* errorStr = NULL;
    char* cerr;
    vector<SubscribeData> subscriptions;
    const char* usage1 = "Usage: ";
    const char* usage2 = " (-V verbosity) -publisher publishername (-nostdout) (-slotslog2 <slotcount>) (-subscribe publishername1 subscriberid1) (-subscribe publishername2 subscriberid2) ...";
    bool nostdout = false;
    int slotCount = -1;
    
    i = 1;
    while ( (i < argc) && !errorStr )
	{
	if ( !strcmp( argv[i], "-nostdout" ) )
	    {
	    nostdout = true;
	    i++;
	    continue;
	    }
	if ( !strcmp( argv[i], "-publisher" ) )
	    {
	    if ( argc < i+1 )
		{
		errorStr = "Missing publishername parameter";
		break;
		}
	    publisherName = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-subscribe" ) )
	    {
	    if ( argc < i+2 )
		{
		errorStr = "Missing publishername or subscriberid parameter";
		break;
		}
	    SubscribeData sd;
	    sd.fPublisherName = argv[i+1];
	    sd.fSubscriberID = argv[i+2];
	    sd.fPublisher = NULL;
	    sd.fSubscriber = NULL;
	    sd.fThread = NULL;
	    subscriptions.insert( subscriptions.end(), sd );
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc < i +1 )
		{
		errorStr = "Missing verbosity level specifier.";
		break;
		}
	    gLogLevel = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting verbosity level specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-slotslog2" ) )
	    {
	    if ( argc < i +1 )
		{
		errorStr = "Missing slot-count specifier.";
		break;
		}
	    slotCount = strtol( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting slot-count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	errorStr = "Unknown parameter";
	}
    if ( !publisherName )
	{
	errorStr = "Must specify -publisher publishername argument";
	}
    if ( subscriptions.size() <= 0 )
	errorStr = "Must specify at least one -subscribe publishername subscriberid argument";
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "EventMerger", "Command line parameters" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	LOG( AliHLTLog::kError, "EventMerger", "Command line parameters" )
	    << errorStr << ENDLOG;
	return -1;
	}

    if ( nostdout )
	{
	gLog.DelServer( sOut );
	}

    MLUCString name = "EventMerger-";
    name += publisherName;
    name += "-";
    AliHLTFileLogServer fileOut( name.c_str(), ".log", 3000000 );
    gLog.AddServer( fileOut );

    //name = "EventMerger-";
    //name += publisherName;
    name = publisherName;
    AliHLTPipeController pipeControl( name.c_str() );
    pipeControl.AddValue( *(AliUInt32_t*)&gLogLevel );
    pipeControl.Start();

	

    AliHLTEventMerger merger( slotCount );
    AliHLTEventMergerRePublisher rePublisher( publisherName, slotCount );
    rePublisher.SetSubscriptionLoop( &PublisherPipeSubscriptionInputLoop );
    rePublisher.SetMerger( &merger );
    SubscriptionListenerThread rePublisherThread( &rePublisher );
    rePublisherThread.Start();

    vector<SubscribeData>::iterator subIter, subEnd;

    subIter = subscriptions.begin();
    subEnd = subscriptions.end();

    AliUInt32_t maxShmNoUseCount = 10;
    //AliUInt32_t maxPreSubEvents = 5;

    while ( subIter != subEnd )
	{
	subIter->fPublisher = new AliHLTPublisherPipeProxy( subIter->fPublisherName );
	name = "EventMerger-";
	name += subIter->fSubscriberID;
	subIter->fSubscriber = new AliHLTEventMergerSubscriber( name.c_str(), maxShmNoUseCount, slotCount );
	subIter->fSubscriber->SetEventMerger( &merger );
	subIter->fSubscriber->SetPublisher( *(subIter->fPublisher) );
	merger.AddSubscriber( subIter->fSubscriber );
	subIter->fThread = new SubscriptionThread( subIter->fPublisher, subIter->fSubscriber );
	subIter->fPublisher->Subscribe( *(subIter->fSubscriber) );
	subIter->fThread->Start();
	subIter->fSubscriber->StartProcessing();
	subIter++;
	}

    AliHLTEventMerger::EventData *eventData;
    AliEventID_t eventID;
    AliHLTSubEventDataDescriptor* sedd;
    AliHLTEventTriggerStruct *etsp;
    while ( merger.WaitForEvent( eventID, eventData ) )
	{
	sedd = MakeSEDD( eventID, eventData->fDataBlocks );
	etsp = eventData->fETS;
	if ( sedd )
	    {
	      AliUInt32_t i, n = sedd->fDataBlockCount;
	      for ( i = 0; i < n; i++ )
		{
		  LOG( AliHLTLog::kDebug, "AliHLTEventMergerSubscriber::ProcessEvent", "Process Descriptors" )
		    << "Event Descriptor " << AliHLTLog::kDec << i << ": 0x" 
		    << AliHLTLog::kHex << sedd->fDataBlocks[i].fShmID.fKey.fID << "/" << AliHLTLog::kDec 
		    << sedd->fDataBlocks[i].fBlockOffset << "/" << sedd->fDataBlocks[i].fBlockSize << "/"
		    << AliHLTLog::kHex << sedd->fDataBlocks[i].fProducerNode << "/0x" << sedd->fDataBlocks[i].fDataType.fID
		    << " (" << AliHLTLog::kDec << sedd->fDataBlocks[i].fDataType.fID << ")." << ENDLOG;
		}
	      
	    rePublisher.AnnounceEvent( eventID, *sedd, *etsp );
	    FreeSEDD( sedd );
	    }
	}


    QuitPipeSubscriptionLoop( rePublisher );
	
    return 0;
    }


AliHLTSubEventDataDescriptor* MakeSEDD( AliEventID_t eventID, vector<AliHLTDetectorSubscriber::BlockData>& dataBlocks )
    {
    LOG( AliHLTLog::kDebug, "EventMerger/MakeSEDD", "Making SEDD" )
	<< "Making sedd for event 0x" << AliHLTLog::kHex << eventID << " ("
	<< AliHLTLog::kDec << eventID << ")." << ENDLOG;
    AliUInt32_t ndx = 0, count = dataBlocks.size();
    vector<AliHLTDetectorSubscriber::BlockData>::iterator iter, end;
    AliHLTSubEventDataDescriptor* sedd;
    sedd = (AliHLTSubEventDataDescriptor*)new AliUInt8_t[ sizeof(AliHLTSubEventDataDescriptor)+count*sizeof(AliHLTSubEventDataBlockDescriptor) ];
    if ( !sedd )
	{
	LOG( AliHLTLog::kError, "EventMerger/MakeSEDD", "Out of memory" )
	    << "Out of memory trying to allocate sub-event data descriptor for " << AliHLTLog::kDec
	    << count << " data blocks." << ENDLOG;
	return NULL;
	}
    sedd->fHeader.fLength = sizeof(AliHLTSubEventDataDescriptor)+count*sizeof(AliHLTSubEventDataBlockDescriptor);
    sedd->fHeader.fType.fID = ALIL3SUBEVENTDATADESCRIPTOR_TYPE;
    sedd->fHeader.fSubType.fID = 0;
    sedd->fHeader.fVersion = 5;
    sedd->fDataType.fID = COMPOSITE_DATAID;
    sedd->fEventID = eventID;
    sedd->fDataBlockCount = count;
    iter = dataBlocks.begin();
    end = dataBlocks.end();
    ndx = 0;
    while ( iter != end )
	{
	sedd->fDataBlocks[ndx].fShmID = iter->fShmID;
	sedd->fDataBlocks[ndx].fBlockOffset = iter->fOffset;
	sedd->fDataBlocks[ndx].fBlockSize = iter->fSize;
	sedd->fDataBlocks[ndx].fProducerNode = iter->fProducerNode;
	sedd->fDataBlocks[ndx].fDataType.fID = iter->fDataType.fID;
	sedd->fDataBlocks[ndx].fDataOrigin.fID = iter->fDataOrigin.fID;
	sedd->fDataBlocks[ndx].fDataSpecification = iter->fDataSpecification;
	//sedd->fDataBlocks[ndx].fByteOrder = iter->fByteOrder;
	for ( int n = 0; n<kAliHLTSEDBDAttributeCount; n++ )
	    sedd->fDataBlocks[ndx].fAttributes[n] = iter->fAttributes[n];
	iter++;
	ndx++;
	}
    return sedd;
    }

void FreeSEDD( AliHLTSubEventDataDescriptor* sedd )
    {
    if ( sedd )
	delete [] (AliUInt8_t*)sedd;
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
