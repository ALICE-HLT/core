/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTEventDataType.h"
#include "AliHLTTolerantEventGatherer.hpp"
#include "AliHLTEventGathererSubscriber.hpp"
#include "AliHLTEventGathererRePublisher.hpp"
#include "AliEventID.h"
#include "AliHLTThread.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTPubSubPipeCom.hpp"
#include "AliHLTPublisherPipeProxy.hpp"
#include "AliHLTPipeController.hpp"
#include "BCLAbstrAddress.hpp"
#include "BCLURLAddressDecoder.hpp"
#include <iostream>
#include <fstream>
#ifdef LIBC_MEMDEBUG
#include <mcheck.h>
#endif

AliHLTSubEventDataDescriptor* MakeSEDD( AliEventID_t eventID, vector<AliHLTDetectorSubscriber::BlockData>& dataBlocks );
void FreeSEDD( AliHLTSubEventDataDescriptor* sedd );

class SubscriptionThread: public AliHLTThread
    {
    public:

	SubscriptionThread( AliHLTPublisherInterface* pub, AliHLTSubscriberInterface* sub  ):
	    fPub( pub ), fSub( sub )
		{
		}

    protected:

	virtual void Run()
		{
		if ( fPub && fSub )
		    fPub->StartPublishing( *fSub );
		}

	AliHLTPublisherInterface* fPub;
	AliHLTSubscriberInterface* fSub;

    };

class SubscriptionListenerThread: public AliHLTThread
    {
    public:

	SubscriptionListenerThread( AliHLTSamplePublisher* pub  ):
	    fPub( pub )
		{
		}

    protected:

	virtual void Run()
		{
		if ( fPub )
		    fPub->AcceptSubscriptions();
		}

	AliHLTSamplePublisher* fPub;

    };


struct SubscribeData
    {
	char* fPublisherName;
	char* fSubscriberID;
	AliHLTPublisherProxyInterface* fPublisher;
	AliHLTEventGathererSubscriber* fSubscriber;
	SubscriptionThread* fThread;
    };


class LAMProxy: public AliHLTTolerantEventGathererLAMProxy
    {
    public:

	LAMProxy( AliHLTSCInterface* interface, BCLAbstrAddressStruct* lamAddress )
		{
		fInterface = interface;
		fLAMAddress = lamAddress;
		}
	virtual ~LAMProxy()
		{
		}

	virtual void LookAtMe()
		{
		fInterface->LookAtMe( fLAMAddress );
		}

    protected:
	
	AliHLTSCInterface* fInterface;
	
	BCLAbstrAddressStruct* fLAMAddress;

    };


int main( int argc, char** argv )
    {
#ifdef LIBC_MEMDEBUG
    mtrace();
#endif
    int i;
    AliHLTFilteredStdoutLogServer sOut;
    gLogLevel = AliHLTLog::kAll;
    gLog.AddServer( sOut );

    char* publisherName = NULL;
    char* errorStr = NULL;
    char* cerr;
    int ret;
    vector<SubscribeData> subscriptions;
    const char* usage1 = "Usage: ";
    const char* usage2 = " (-V verbosity) -controlurl <System-Control-URL> (-lamurl <LAM-URL>) -publisher publishername (-nostdout) (-slotslog2 <slotcount>) (-comtimeout <pub-sub-communication-timeout_usec>) (-backlogslotslog2 <slocount>) (-subscribe publishername1 subscriberid1) (-subscribe publishername2 subscriberid2) ...";
    bool nostdout = false;
    int slotsLog2 = -1;
#if 0
    int slotsLog2Sub;
#endif
    int backLogSlotCount = -1;
    bool backLogSet = false;
    char* controlAddressURL = NULL;
    char* LAMURL = NULL;
    BCLAbstrAddressStruct* lamAddress = NULL;
    bool comTimeoutSet = false;
    unsigned long comTimeout = 0;
    
    i = 1;
    while ( (i < argc) && !errorStr )
	{
	if ( !strcmp( argv[i], "-nostdout" ) )
	    {
	    nostdout = true;
	    i++;
	    continue;
	    }
	if ( !strcmp( argv[i], "-publisher" ) )
	    {
	    if ( argc < i+1 )
		{
		errorStr = "Missing publishername parameter";
		break;
		}
	    publisherName = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-subscribe" ) )
	    {
	    if ( argc < i+2 )
		{
		errorStr = "Missing publishername or subscriberid parameter";
		break;
		}
	    SubscribeData sd;
	    sd.fPublisherName = argv[i+1];
	    sd.fSubscriberID = argv[i+2];
	    sd.fPublisher = NULL;
	    sd.fSubscriber = NULL;
	    subscriptions.insert( subscriptions.end(), sd );
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc < i +1 )
		{
		errorStr = "Missing verbosity level specifier.";
		break;
		}
	    gLogLevel = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting verbosity level specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-comtimeout" ) )
	    {
	    if ( argc < i +1 )
		{
		errorStr = "Missing publisher subscriber communication timeout specifier.";
		break;
		}
	    comTimeoutSet = true;
	    comTimeout = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting communication timeout specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-slotslog2" ) )
	    {
	    if ( argc < i +1 )
		{
		errorStr = "Missing slot-count specifier.";
		break;
		}
	    slotsLog2 = strtol( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting slot-count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-backlogslotslog2" ) )
	    {
	    if ( argc < i +1 )
		{
		errorStr = "Missing backlog slot-count specifier.";
		break;
		}
	    backLogSlotCount = strtol( argv[i+1], &cerr, 0 );
	    backLogSet = true;
	    if ( *cerr )
		{
		errorStr = "Error converting backlog slot-count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-controlurl" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing system control URL parameter";
		break;
		}
	    controlAddressURL = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-lamurl" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing LAM address URL parameter";
		break;
		}
	    LAMURL = argv[i+1];
	    ret = BCLDecodeRemoteURL( LAMURL, lamAddress );
	    if ( ret )
		{
		errorStr = "Error decoding LAM address URL parameter";
		break;
		}
	    i += 2;
	    continue;
	    }
	errorStr = "Unknown parameter";
	}
    if ( !errorStr )
	{
	if ( !publisherName )
	    {
	    errorStr = "Must specify -publisher publishername argument";
	    }
	if ( subscriptions.size() <= 0 )
	    errorStr = "Must specify at least one -subscribe publishername subscriberid argument";
	}
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "EventGatherer", "Command line parameters" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	LOG( AliHLTLog::kError, "EventGatherer", "Command line parameters" )
	    << errorStr << ENDLOG;
	return -1;
	}

    if ( !backLogSet )
	backLogSlotCount = slotsLog2;

    if ( nostdout )
	{
	gLog.DelServer( sOut );
	}

    MLUCString name = "EventGatherer-";
    name += publisherName;
    name += "-";
    AliHLTFileLogServer fileOut( name.c_str(), ".log", 3000000 );
    gLog.AddServer( fileOut );

#if 0
    // This was to restrict the amount of event slots for each subscriber
    // to a fraction of the global one, but in the events of faults, this might
    // actually be not enough.
    int cntExp2;
    unsigned long mask;
    cntExp2 = sizeof(mask)*8-1;
    mask = 0x80000000;
    while ( mask>0 && !(mask & subscriptions.size()) )
	{
	mask >>= 1;
	cntExp2--;
	}
    slotsLog2Sub = slotsLog2-cntExp2;
#endif

    //name = "EventGatherer-";
    //name += publisherName;
    name = publisherName;
    AliHLTPipeController pipeControl( name.c_str() );
    pipeControl.AddValue( *(AliUInt32_t*)&gLogLevel );
    pipeControl.Start();

    AliHLTSCInterface scInterface;

    LAMProxy lamProxy( &scInterface, lamAddress );

    AliHLTToleranceStatus tmpStatus;
    AliHLTToleranceStatus* status = (AliHLTToleranceStatus*)new AliUInt8_t[ tmpStatus.fHeader.fLength+sizeof(AliUInt32_t)*subscriptions.size() ];
    if ( !status )
	{
	LOG( AliHLTLog::kError, "EventScatterer", "Out Of Memory" )
	    << "Out of memory trying to allocate status structure of "
	    << AliHLTLog::kDec << tmpStatus.fHeader.fLength+sizeof(AliUInt32_t)*subscriptions.size()
	    << " bytes." << ENDLOG;
	}
    else
	{
	memcpy( status, &tmpStatus, tmpStatus.fHeader.fLength );
	status->fChannelStatus.fChannelsCnt = subscriptions.size();
	status->fHeader.fLength += sizeof(AliUInt32_t)*subscriptions.size();
	status->fChannelStatus.fLength += sizeof(AliUInt32_t)*subscriptions.size();
	for ( unsigned long k = 0; k < status->fChannelStatus.fChannelsCnt; k++ )
	    status->fChannelStatus.fChannels[k] = 1;
	}

    scInterface.Bind( controlAddressURL );
    scInterface.Start();
    scInterface.SetStatusData( &(status->fHeader) );
	

    AliHLTTolerantEventGatherer merger( slotsLog2, backLogSlotCount );
    AliHLTEventGathererRePublisher rePublisher( publisherName, slotsLog2 );
    rePublisher.SetSubscriptionLoop( &PublisherPipeSubscriptionInputLoop );
    rePublisher.SetGatherer( &merger );
    SubscriptionListenerThread rePublisherThread( &rePublisher );
    rePublisherThread.Start();

    merger.SetupCommandProcessor( &scInterface );
    merger.SetLAMProxy( &lamProxy );
    if ( status )
	merger.SetStatus( status );
    
    vector<SubscribeData>::iterator subIter, subEnd;

    subIter = subscriptions.begin();
    subEnd = subscriptions.end();

    AliUInt32_t maxShmNoUseCount = 10;
    //AliUInt32_t maxPreSubEvents = 5;

    while ( subIter != subEnd )
	{
	subIter->fPublisher = new AliHLTPublisherPipeProxy( subIter->fPublisherName );
	if ( comTimeoutSet && subIter->fPublisher )
	    subIter->fPublisher->SetTimeout( comTimeout );
	name = "EventGatherer-";
	name += subIter->fSubscriberID;
	subIter->fSubscriber = new AliHLTEventGathererSubscriber( name.c_str(), maxShmNoUseCount, slotsLog2 );
	subIter->fSubscriber->SetEventGatherer( &merger );
	subIter->fSubscriber->SetPublisher( *(subIter->fPublisher) );
	merger.AddSubscriber( subIter->fSubscriber );
	subIter->fThread = new SubscriptionThread( subIter->fPublisher, subIter->fSubscriber );
	subIter->fPublisher->Subscribe( *(subIter->fSubscriber) );
	subIter->fThread->Start();
	subIter->fSubscriber->StartProcessing();
	subIter++;
	}

    AliHLTBaseEventGatherer::EventGathererData *eventData;
    AliEventID_t eventID;
    AliHLTSubEventDataDescriptor* sedd;
    AliHLTEventTriggerStruct *etsp;
    while ( merger.WaitForEvent( eventID, eventData ) )
	{
	sedd = MakeSEDD( eventID, eventData->fDataBlocks );
	etsp = eventData->fETS;
	if ( sedd )
	    {
	    rePublisher.AnnounceEvent( eventID, *sedd, *etsp );
	    FreeSEDD( sedd );
	    }
	}


    QuitPipeSubscriptionLoop( rePublisher );

    scInterface.Stop();
    scInterface.Unbind();
	
    return 0;
    }


AliHLTSubEventDataDescriptor* MakeSEDD( AliEventID_t eventID, vector<AliHLTDetectorSubscriber::BlockData>& dataBlocks )
    {
    LOG( AliHLTLog::kDebug, "EventGatherer/MakeSEDD", "Making SEDD" )
	<< "Making sedd for event 0x" << AliHLTLog::kHex << eventID << " ("
	<< AliHLTLog::kDec << eventID << ")." << ENDLOG;
    AliUInt32_t ndx = 0, count = dataBlocks.size();
    vector<AliHLTDetectorSubscriber::BlockData>::iterator iter, end;
    AliHLTSubEventDataDescriptor* sedd;
    sedd = (AliHLTSubEventDataDescriptor*)new AliUInt8_t[ sizeof(AliHLTSubEventDataDescriptor)+count*sizeof(AliHLTSubEventDataBlockDescriptor) ];
    if ( !sedd )
	{
	LOG( AliHLTLog::kError, "EventGatherer/MakeSEDD", "Out of memory" )
	    << "Out of memory trying to allocate sub-event data descriptor for " << AliHLTLog::kDec
	    << count << " data blocks." << ENDLOG;
	return NULL;
	}
    sedd->fHeader.fLength = sizeof(AliHLTSubEventDataDescriptor)+count*sizeof(AliHLTSubEventDataBlockDescriptor);
    sedd->fHeader.fType.fID = ALIL3SUBEVENTDATADESCRIPTOR_TYPE;
    sedd->fHeader.fSubType.fID = 0;
    sedd->fHeader.fVersion = 5;
    sedd->fDataType.fID = COMPOSITE_DATAID;
    sedd->fEventID = eventID;
    sedd->fDataBlockCount = count;
    iter = dataBlocks.begin();
    end = dataBlocks.end();
    ndx = 0;
    while ( iter != end )
	{
	sedd->fDataBlocks[ndx].fShmID = iter->fShmID;
	sedd->fDataBlocks[ndx].fBlockOffset = iter->fOffset;
	sedd->fDataBlocks[ndx].fBlockSize = iter->fSize;
	sedd->fDataBlocks[ndx].fProducerNode = iter->fProducerNode;
	sedd->fDataBlocks[ndx].fDataType.fID = iter->fDataType.fID;
	sedd->fDataBlocks[ndx].fDataOrigin.fID = iter->fDataOrigin.fID;
	sedd->fDataBlocks[ndx].fDataSpecification = iter->fDataSpecification;
	//sedd->fDataBlocks[ndx].fByteOrder = iter->fByteOrder;
	for ( int n = 0; n<kAliHLTSEDBDAttributeCount; n++ )
	    sedd->fDataBlocks[ndx].fAttributes[n] = iter->fAttributes[n];
	iter++;
	ndx++;
	}
    return sedd;
    }

void FreeSEDD( AliHLTSubEventDataDescriptor* sedd )
    {
    if ( sedd )
	delete [] (AliUInt8_t*)sedd;
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
