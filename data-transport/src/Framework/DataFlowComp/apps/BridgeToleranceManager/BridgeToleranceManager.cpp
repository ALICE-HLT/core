/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCInterface.hpp"
#include "AliHLTSCCommandProcessor.hpp"
#include "AliHLTSCController.hpp"
#include "AliHLTSCCommands.hpp"
#include "AliHLTBridgeStatusData.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTTolerantRoundRobinEventScattererCmd.hpp"
#include "BCLURLAddressDecoder.hpp"
#include "MLUCString.hpp"
#include <stdio.h>
#include <signal.h>


bool gQuit = false;

void QuitSignalHandler( int sig );

struct DataSource
    {
	unsigned long fSubNr;

	BCLAbstrAddressStruct* fSubscriberControlAddress;

	BCLAbstrAddressStruct* fPublisherControlAddress;
	MLUCString fPublisherMsgAddressURL;
	BCLAbstrAddressStruct* fPublisherMsgAddress;
	MLUCString fPublisherBlobMsgAddressURL;
	BCLAbstrAddressStruct* fPublisherBlobMsgAddress;
    };

struct DataSink
    {

	BCLAbstrAddressStruct* fPublisherControlAddress;

	BCLAbstrAddressStruct* fSubscriberControlAddress;
	MLUCString fSubscriberMsgAddressURL;
	BCLAbstrAddressStruct* fSubscriberMsgAddress;
	MLUCString fSubscriberBlobMsgAddressURL;
	BCLAbstrAddressStruct* fSubscriberBlobMsgAddress;
    };

struct DataPath
    {
	enum Type { kActive, kSpare, kDown };

	unsigned long fNr;
	unsigned long fActiveNr;

	Type fType;

	vector<DataSource> fSources;
	DataSink fSink;
    };

struct Detector
    {
	unsigned long fActiveNr;
	BCLAbstrAddressStruct* fControlAddress;
    };

struct ActivesNeededSourceData
    {
	unsigned long fSubNr;
	BCLAbstrAddressStruct* fSubscriberControlAddress;
    };

struct ActivesNeededData
    {
	unsigned long fActiveNr;
	BCLAbstrAddressStruct* fSinkPublisherControlAddress;
	vector<ActivesNeededSourceData> fSourceData;
    };


/* Configuration file format: (quasi-BNF notation)

   line: data-source-line | data-sink-line | spare-source-line | spare-sink-line | target-line | detector-line

   data-source-line: 'source' number subnumber <subscriber-control-address-URL> <publisher-control-address-URL> <publisher-msg-address-URL> <publisher-blobmsg-address-URL>

   data-sink-line: 'sink' number <publisher-control-address-URL> <subscriber-control-address-URL> <subscriber-msg-address-URL> <subscriber-blobmsg-address-URL>

   spare-source-line: 'sparesource' number subnumber <publisher-control-address-URL> <publisher-msg-address-URL> <publisher-blobmsg-address-URL>

   spare-sink-line: 'sparesink' number <subscriber-control-address-URL> <subscriber-msg-address-URL> <subscriber-blobmsg-address-URL>

   target-line: 'target' <target-control-address-URL>

   detector-line: 'detector' <nr> <tolerance-detector-control-address-URL>

*/

class BridgeConfig
    {
    public:

	BridgeConfig();
	~BridgeConfig();

	bool ReadConfig( const char* filename );

	bool SetPathStatus( unsigned long nr, bool up );
	bool GetActivePath( unsigned long activeNr, DataPath& activePath );
	bool SetSpareActive( unsigned long newActiveNr );
	bool GetPath( unsigned long nr, DataPath& path );
	void GetTargets( vector<BCLAbstrAddressStruct*>& targets );
	bool GetToleranceDetector( unsigned long activeNr, Detector& detector );

    protected:
	
	bool ReadLine( istream& istr, MLUCString& line );
	bool GetToken( const char* text, MLUCString& token, const char*& pos );

	vector<DataPath> fDataPaths;
	vector<ActivesNeededData> fActivesNeeded;
	vector<BCLAbstrAddressStruct*> fTargets;
	vector<Detector> fDetectors;

    };

class CommandHandler: public AliHLTSCCommandProcessor
    {
    public:
       
	CommandHandler( BridgeConfig* bridgeConfig, AliHLTSCController* controller, unsigned long sendBridgeCmdDelay_ms )
		{
		fBridgeConfig = bridgeConfig;
		fController = controller;
		fSendBridgeCmdDelay = sendBridgeCmdDelay_ms;
		pthread_mutex_init( &fBridgeConfigMutex, NULL );
		pthread_mutex_init( &fReconnectEndPointMutex, NULL );
		pthread_mutex_init( &fConnectsMutex, NULL );
		}


	virtual ~CommandHandler()
		{
		pthread_mutex_destroy( &fBridgeConfigMutex );
		pthread_mutex_destroy( &fReconnectEndPointMutex );
		pthread_mutex_destroy( &fConnectsMutex );
		}
	
	virtual void ProcessCmd( AliHLTSCCommandStruct* cmd );

 	void GetConnectsNeeded( vector<DataPath>& activePaths );
		
 	void GetReconnectPaths( vector<DataPath>& paths );

 	bool MakeConnection( DataPath activePath );

	bool SetTargets( unsigned long activeNr );

    protected:

	pthread_mutex_t fBridgeConfigMutex;
	BridgeConfig* fBridgeConfig;
	AliHLTSCController* fController;
	unsigned long fSendBridgeCmdDelay; // In millisec.

	vector<unsigned long> fActivesNeeded;

 	pthread_mutex_t fReconnectEndPointMutex;
 	vector<DataPath> fReconnectPaths;

 	pthread_mutex_t fConnectsMutex;
	vector<DataPath> fConnectPaths;
// 	vector<BridgeEndPoint> fConnectEndPoints;
// 	vector<BridgeNode> fConnectActiveNodes;
	
    };



BridgeConfig::BridgeConfig()
    {
    }

BridgeConfig::~BridgeConfig()
    {
    vector<DataPath>::iterator pathIter, pathEnd;
    vector<DataSource>::iterator sourceIter, sourceEnd;
    vector<BCLAbstrAddressStruct*>::iterator targetIter, targetEnd;
    pathIter = fDataPaths.begin();
    pathEnd = fDataPaths.begin();
    while ( pathIter != pathEnd )
	{
	sourceIter = pathIter->fSources.begin();
	sourceEnd = pathIter->fSources.end();
	while ( sourceIter != sourceEnd )
	    {
	    if ( sourceIter->fSubscriberControlAddress )
		BCLFreeAddress( sourceIter->fSubscriberControlAddress );
	    
	    if ( sourceIter->fPublisherControlAddress )
		BCLFreeAddress( sourceIter->fPublisherControlAddress );
	    if ( sourceIter->fPublisherMsgAddress )
		BCLFreeAddress( sourceIter->fPublisherMsgAddress );
	    if ( sourceIter->fPublisherBlobMsgAddress )
		BCLFreeAddress( sourceIter->fPublisherBlobMsgAddress );
	    sourceIter++;
	    }
	
	if ( pathIter->fSink.fPublisherControlAddress )
	    BCLFreeAddress( pathIter->fSink.fPublisherControlAddress );

	if ( pathIter->fSink.fSubscriberControlAddress )
	    BCLFreeAddress( pathIter->fSink.fSubscriberControlAddress );
	if ( pathIter->fSink.fSubscriberMsgAddress )
	    BCLFreeAddress( pathIter->fSink.fSubscriberMsgAddress );
	if ( pathIter->fSink.fSubscriberBlobMsgAddress )
	    BCLFreeAddress( pathIter->fSink.fSubscriberBlobMsgAddress );
	pathIter++;
	}

    targetIter = fTargets.begin();
    targetEnd = fTargets.end();
    while ( targetIter != targetEnd )
	{
	if ( *targetIter )
	    BCLFreeAddress( *targetIter );
	targetIter++;
	}
    }

bool BridgeConfig::ReadConfig( const char* filename )
    {
    ifstream file;
    int ret;
    char* cpErr = NULL;
    vector<DataPath>::iterator pathIter, pathEnd;
    unsigned long nr;
    
    file.open( filename );
    if ( !file.good() )
	{
	LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Opening File" )
	    << "Error opening file " << filename << "." << ENDLOG;
	return false;
	}

    /*
      data-source-line: 'source' number subnumber <subscriber-control-address-URL> <publisher-control-address-URL> <publisher-msg-address-URL> <publisher-blobmsg-address-URL>
      
      data-sink-line: 'sink' number <publisher-control-address-URL> <subscriber-control-address-URL> <subscriber-msg-address-URL> <subscriber-blobmsg-address-URL>
      
      spare-source-line: 'sparesource' number subnumber <publisher-control-address-URL> <publisher-msg-address-URL> <publisher-blobmsg-address-URL>
      
      spare-sink-line: 'sparesink' number <subscriber-control-address-URL> <subscriber-msg-address-URL> <subscriber-blobmsg-address-URL>
    */


    MLUCString line, token;
    const char* pos;
    while ( ReadLine( file, line ) )
	{
	if ( !GetToken( line.c_str(), token, pos ) )
	    {	
	    LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		<< "Error parsing file " << filename << ": '" << pos << "'." << ENDLOG;
	    return false;
	    }
	if ( !pos )
	    continue; // Empty line.

	if ( token == "source" )
	    {
	    // data-source-line: 'source' number subnumber <subscriber-control-address-URL> <publisher-control-address-URL> <publisher-msg-address-URL> <publisher-blobmsg-address-URL>
	    DataSource source;
	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected a number." << ENDLOG;
		return false;
		}
	    nr = strtoul( token.c_str(), &cpErr, 0 );
	    if ( cpErr && *cpErr!=0 )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": Found '" << token.c_str() 
		    << "' where a number was expected." << ENDLOG;
		return false;
		}
	    pathIter = fDataPaths.begin();
	    pathEnd = fDataPaths.end();
	    while ( pathIter != pathEnd )
		{
		if ( pathIter->fNr == nr )
		    break;
		pathIter++;
		}
	    if ( pathIter == pathEnd )
		{
		DataPath path;
		path.fNr = nr;
		path.fActiveNr = nr;
		path.fType = DataPath::kActive;
		pathIter = fDataPaths.insert( fDataPaths.end(), path );
		}

	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected a number." << ENDLOG;
		return false;
		}
	    source.fSubNr = strtoul( token.c_str(), &cpErr, 0 );
	    if ( cpErr && *cpErr!=0 )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": Found '" << token.c_str() 
		    << "' where a number was expected." << ENDLOG;
		return false;
		}

	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected subscriber-control-address." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), source.fSubscriberControlAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing subscriber control address '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}

	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected publisher-control-address." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), source.fPublisherControlAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing publisher control address '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}

	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected publisher-msg-address." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), source.fPublisherMsgAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing publisher msg address '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}
	    source.fPublisherMsgAddressURL = token;

	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected publisher-blobmsg-address." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), source.fPublisherBlobMsgAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing publisher blob msg address '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}
	    source.fPublisherBlobMsgAddressURL = token;

	    pathIter->fSources.insert( pathIter->fSources.end(), source );
	    }
	else if ( token == "sink" )
	    {
	    // data-sink-line: 'sink' number <publisher-control-address-URL> <subscriber-control-address-URL> <subscriber-msg-address-URL> <subscriber-blobmsg-address-URL>
	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected a number." << ENDLOG;
		return false;
		}
	    nr = strtoul( token.c_str(), &cpErr, 0 );
	    if ( cpErr && *cpErr!=0 )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": Found '" << token.c_str() 
		    << "' where a number was expected." << ENDLOG;
		return false;
		}

	    pathIter = fDataPaths.begin();
	    pathEnd = fDataPaths.end();
	    while ( pathIter != pathEnd )
		{
		if ( pathIter->fNr == nr )
		    break;
		pathIter++;
		}
	    if ( pathIter == pathEnd )
		{
		DataPath path;
		path.fNr = nr;
		path.fActiveNr = nr;
		path.fType = DataPath::kActive;
		pathIter = fDataPaths.insert( fDataPaths.end(), path );
		}


	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected publisher-control-address." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), pathIter->fSink.fPublisherControlAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing publisher control address '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}

	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected subscriber-control-address." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), pathIter->fSink.fSubscriberControlAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing subscriber control address '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}

	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected subscriber-msg-address." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), pathIter->fSink.fSubscriberMsgAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing subscriber msg address '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}
	    pathIter->fSink.fSubscriberMsgAddressURL = token;

	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected subscriber-blobmsg-address." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), pathIter->fSink.fSubscriberBlobMsgAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing subscriber blob msg address '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}
	    pathIter->fSink.fSubscriberBlobMsgAddressURL = token;

	    }
	else if ( token == "sparesource" )
	    {
	    // spare-source-line: 'sparesource' number subnumber <publisher-control-address-URL> <publisher-msg-address-URL> <publisher-blobmsg-address-URL>
	    DataSource source;
	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected a number." << ENDLOG;
		return false;
		}
	    nr = strtoul( token.c_str(), &cpErr, 0 );
	    if ( cpErr && *cpErr!=0 )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": Found '" << token.c_str() 
		    << "' where a number was expected." << ENDLOG;
		return false;
		}
	    pathIter = fDataPaths.begin();
	    pathEnd = fDataPaths.end();
	    while ( pathIter != pathEnd )
		{
		if ( pathIter->fNr == nr )
		    break;
		pathIter++;
		}
	    if ( pathIter == pathEnd )
		{
		DataPath path;
		path.fNr = nr;
		path.fActiveNr = ~(unsigned long)0;
		path.fType = DataPath::kSpare;
		pathIter = fDataPaths.insert( fDataPaths.end(), path );
		}

	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected a number." << ENDLOG;
		return false;
		}
	    source.fSubNr = strtoul( token.c_str(), &cpErr, 0 );
	    if ( cpErr && *cpErr!=0 )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": Found '" << token.c_str() 
		    << "' where a number was expected." << ENDLOG;
		return false;
		}

	    source.fSubscriberControlAddress = NULL;

	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected publisher-control-address." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), source.fPublisherControlAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing publisher control address '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}

	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected publisher-msg-address." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), source.fPublisherMsgAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing publisher msg address '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}
	    source.fPublisherMsgAddressURL = token;

	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected publisher-blobmsg-address." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), source.fPublisherBlobMsgAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing publisher blob msg address '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}
	    source.fPublisherBlobMsgAddressURL = token;

	    pathIter->fSources.insert( pathIter->fSources.end(), source );
	    }
	else if ( token == "sparesink" )
	    {
	    // spare-sink-line: 'sparesink' number <subscriber-control-address-URL> <subscriber-msg-address-URL> <subscriber-blobmsg-address-URL>
	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected a number." << ENDLOG;
		return false;
		}
	    nr = strtoul( token.c_str(), &cpErr, 0 );
	    if ( cpErr && *cpErr!=0 )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": Found '" << token.c_str() 
		    << "' where a number was expected." << ENDLOG;
		return false;
		}
	    pathIter = fDataPaths.begin();
	    pathEnd = fDataPaths.end();
	    while ( pathIter != pathEnd )
		{
		if ( pathIter->fNr == nr )
		    break;
		pathIter++;
		}
	    if ( pathIter == pathEnd )
		{
		DataPath path;
		path.fNr = nr;
		path.fActiveNr = ~(unsigned long)0;
		path.fType = DataPath::kSpare;
		pathIter = fDataPaths.insert( fDataPaths.end(), path );
		}

	    pathIter->fSink.fPublisherControlAddress = NULL;

	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected subscriber-control-address." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), pathIter->fSink.fSubscriberControlAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing subscriber control address '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}

	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected subscriber-msg-address." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), pathIter->fSink.fSubscriberMsgAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing subscriber msg address '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}
	    pathIter->fSink.fSubscriberMsgAddressURL = token;

	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected subscriber-blobmsg-address." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), pathIter->fSink.fSubscriberBlobMsgAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing subscriber blob msg address '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}
	    pathIter->fSink.fSubscriberBlobMsgAddressURL = token;

	    }
	else if ( token == "target" )
	    {
	    BCLAbstrAddressStruct* targetAddr;
	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected target-control-address-URL." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), targetAddr );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing target-control-address-URL '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}

	    fTargets.insert( fTargets.end(), targetAddr );
	    }
	else if ( token == "detector" )
	    {
	    Detector detector;
	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected a number." << ENDLOG;
		return false;
		}
	    detector.fActiveNr = strtoul( token.c_str(), &cpErr, 0 );
	    if ( cpErr && *cpErr!=0 )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": Found '" << token.c_str() 
		    << "' where a number was expected." << ENDLOG;
		return false;
		}
	    if ( !GetToken( pos, token, pos ) )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing file " << filename << ": '" << pos << "', expected tolerance-detector-control-address-URL." << ENDLOG;
		return false;
		}
	    ret = BCLDecodeRemoteURL( token.c_str(), detector.fControlAddress );
	    if ( ret )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		    << "Error parsing tolerance-detector-control-address-URL '" << token.c_str() 
		    << "' in file " << filename << ": '" << pos << "'." << ENDLOG;
		return false;
		}

	    fDetectors.insert( fDetectors.end(), detector );
	    }
	else
	    {
	    LOG( AliHLTLog::kError, "BridgeConfig::ReadConfig", "Error Parsing File" )
		<< "Error parsing file " << filename << ": '" << pos 
		<< "', expected 'active', 'spare', or 'endpoint'." << ENDLOG;
	    return false;
	    }
	}
    return true;
    }

bool BridgeConfig::SetPathStatus( unsigned long nr, bool up )
    {
    vector<DataPath>::iterator pathIter, pathEnd;
    unsigned long activeNr;

    pathIter = fDataPaths.begin();
    pathEnd = fDataPaths.end();
    while ( pathIter != pathEnd )
	{
	if ( pathIter->fNr == nr )
	    break;
	pathIter++;
	}

    if ( pathIter == pathEnd )
	{
	LOG( AliHLTLog::kError, "BridgeConfig::SetPathStatus", "Data Path Not Found" )
	    << "No path with number " << AliHLTLog::kDec << nr << " found." << ENDLOG;
	return false;
	}

    if ( !up )
	{
	// path to be set to down.
	if ( pathIter->fType == DataPath::kActive )
	    {
	    pathIter->fType = DataPath::kDown;
	    activeNr = pathIter->fActiveNr;
	    pathIter->fActiveNr = ~(unsigned long)0;
	    vector<DataPath>::iterator spareIter, spareEnd;
	    spareIter = fDataPaths.begin();
	    spareEnd = fDataPaths.end();
	    while ( spareIter != spareEnd )
		{
		if ( spareIter->fType == DataPath::kSpare &&
		     spareIter->fSources.size()==pathIter->fSources.size() )
		    break;
		spareIter++;
		}
	    if ( spareIter == spareEnd )
		{
		LOG( AliHLTLog::kError, "BridgeConfig::SetPathStatus", "No Spare path" )
		    << "No suitable Spare Path available for path " << AliHLTLog::kDec << nr << "." << ENDLOG;
		ActivesNeededSourceData sourceData;
		ActivesNeededData data;
		data.fActiveNr = activeNr;
		data.fSinkPublisherControlAddress = pathIter->fSink.fPublisherControlAddress;
		pathIter->fSink.fPublisherControlAddress = NULL;
		vector<DataSource>::iterator sourceIter, sourceEnd;
		sourceIter = pathIter->fSources.begin();
		sourceEnd = pathIter->fSources.end();
		while ( sourceIter != sourceEnd )
		    {
		    sourceData.fSubNr = sourceIter->fSubNr;
		    sourceData.fSubscriberControlAddress = sourceIter->fSubscriberControlAddress;
		    data.fSourceData.insert( data.fSourceData.end(), sourceData );
		    sourceIter++;
		    }
		fActivesNeeded.insert( fActivesNeeded.end(), data );
		return false;
		}
	    else
		{
		spareIter->fActiveNr = activeNr;
		spareIter->fType = DataPath::kActive;
		spareIter->fSink.fPublisherControlAddress = pathIter->fSink.fPublisherControlAddress;
		pathIter->fSink.fPublisherControlAddress = NULL;
		vector<DataSource>::iterator sourceIter, sourceEnd, spareSourceIter, spareSourceEnd;
		sourceIter = pathIter->fSources.begin();
		sourceEnd = pathIter->fSources.end();
		while ( sourceIter != sourceEnd )
		    {
		    spareSourceIter = spareIter->fSources.begin();
		    spareSourceEnd = spareIter->fSources.end();
		    while ( spareSourceIter != spareSourceEnd )
			{
			if ( spareSourceIter->fSubNr == sourceIter->fSubNr )
			    {
			    spareSourceIter->fSubscriberControlAddress = sourceIter->fSubscriberControlAddress;
			    sourceIter->fSubscriberControlAddress = NULL;
			    break;
			    }
			spareSourceIter++;
			}
		    sourceIter++;
		    }
		}
	    }
	else if ( pathIter->fType == DataPath::kSpare )
	    {
	    // Just mark the spare as down (and thus unavailable as spare...)
	    pathIter->fType = DataPath::kDown;
	    }
	else
	    {
	    // Invalid state of path. Is already down.
	    char* stateName;
	    if ( pathIter->fType == DataPath::kDown )
		stateName = "down";
	    else
		stateName = "unknown";
	    LOG( AliHLTLog::kError, "BridgeConfig::SetPathStatus", "Invalid State of Path" )
		<< "Path to set down " << AliHLTLog::kDec << nr << "/" << pathIter->fNr 
		<< " is in invalid state " << stateName << "." << ENDLOG;
	    return false;
	    }
	}
    else
	{
	if ( pathIter->fType == DataPath::kDown )
	    {
	    pathIter->fType = DataPath::kSpare;
	    }
	else
	    {
	    // Invalid state to set up...
	    char* stateName;
	    if ( pathIter->fType == DataPath::kActive )
		stateName = "active";
	    else if ( pathIter->fType == DataPath::kSpare )
		stateName = "spare";
	    else
		stateName = "unknown";
	    LOG( AliHLTLog::kError, "BridgeConfig::SetPathStatus", "Invalid State of Bridge Path" )
		<< "Bridge Path to set up " << AliHLTLog::kDec << nr << "/" << pathIter->fNr 
		<< " is in invalid state " << stateName << "." << ENDLOG;
	    return false;
	    }
	}
    return true;
    }

bool BridgeConfig::GetActivePath( unsigned long activeNr, DataPath& activePath )
    {
    vector<DataPath>::iterator pathIter, pathEnd;
    vector<DataSource>::iterator sourceIter, sourceEnd;

    pathIter = fDataPaths.begin();
    pathEnd = fDataPaths.end();
    while ( pathIter != pathEnd )
	{
	if ( pathIter->fActiveNr == activeNr )
	    break;
	pathIter++;
	}

    if ( pathIter == pathEnd )
	{
	LOG( AliHLTLog::kError, "BridgeConfig::GetActivePath", "Path Not Found" )
	    << "No Path with active number " << AliHLTLog::kDec << activeNr << " found." << ENDLOG;
	return false;
	}
    
    activePath.fNr = pathIter->fNr;
    activePath.fActiveNr = pathIter->fActiveNr;
    activePath.fType = pathIter->fType;
    activePath.fSink = pathIter->fSink;

    activePath.fSources.clear();
    sourceIter = pathIter->fSources.begin();
    sourceEnd = pathIter->fSources.end();
    while ( sourceIter != sourceEnd )
	{
	activePath.fSources.insert( activePath.fSources.end(), *sourceIter );
	sourceIter++;
	}

    return true;
    }

bool BridgeConfig::SetSpareActive( unsigned long activeNr )
    {
    vector<ActivesNeededData>::iterator andIter, andEnd;
    vector<ActivesNeededSourceData>::iterator ansdIter, ansdEnd;
    vector<DataPath>::iterator pathIter, pathEnd;
    vector<DataSource>::iterator sourceIter, sourceEnd;

    andIter = fActivesNeeded.begin();
    andEnd = fActivesNeeded.end();
    while ( andIter != andEnd )
	{
	if ( andIter->fActiveNr == activeNr )
	    break;
	andIter++;
	}

    if ( andIter == andEnd )
	{
	LOG( AliHLTLog::kError, "BridgeConfig::SetSpareActive", "Necessary Active Not Found" )
	    << "Path " << AliHLTLog::kDec << activeNr
	    << " does not need a spare to be set to active." << ENDLOG;
	return false;
	}

    pathIter = fDataPaths.begin();
    pathEnd = fDataPaths.end();
    while ( pathIter != pathEnd )
	{
	if ( pathIter->fType == DataPath::kSpare &&
	     pathIter->fSources.size() == andIter->fSourceData.size() )
	    break;
	pathIter++;
	}

    if ( pathIter == pathEnd )
	{
	LOG( AliHLTLog::kError, "BridgeConfig::SetSpareActive", "No Spare Path Found" )
	    << "No Spare Path found." << ENDLOG;
	return false;
	}
    
    pathIter->fType = DataPath::kActive;
    pathIter->fActiveNr = andIter->fActiveNr;
    pathIter->fSink.fPublisherControlAddress = andIter->fSinkPublisherControlAddress;
    ansdIter = andIter->fSourceData.begin();
    ansdEnd = andIter->fSourceData.end();
    while ( ansdIter != ansdEnd )
	{
	sourceIter = pathIter->fSources.begin();
	sourceEnd = pathIter->fSources.end();
	while ( sourceIter != sourceEnd )
	    {
	    if ( sourceIter->fSubNr == ansdIter->fSubNr )
		{
		sourceIter->fSubscriberControlAddress = ansdIter->fSubscriberControlAddress;
		break;
		}
	    sourceIter++;
	    }
	ansdIter++;
	}
    fActivesNeeded.erase( andIter );
    return true;
    }

bool BridgeConfig::GetPath( unsigned long nr, DataPath& path )
    {
    vector<DataPath>::iterator pathIter, pathEnd;
    vector<DataSource>::iterator sourceIter, sourceEnd;

    pathIter = fDataPaths.begin();
    pathEnd = fDataPaths.end();
    while ( pathIter != pathEnd )
	{
	if ( pathIter->fNr == nr )
	    break;
	pathIter++;
	}

    if ( pathIter == pathEnd )
	{
	LOG( AliHLTLog::kError, "BridgeConfig::GetPath", "Path Not Found" )
	    << "No Path with number " << AliHLTLog::kDec << nr << " found." << ENDLOG;
	return false;
	}
    
    path.fNr = pathIter->fNr;
    path.fActiveNr = pathIter->fActiveNr;
    path.fType = pathIter->fType;
    path.fSink = pathIter->fSink;

    path.fSources.clear();
    sourceIter = pathIter->fSources.begin();
    sourceEnd = pathIter->fSources.end();
    while ( sourceIter != sourceEnd )
	{
	path.fSources.insert( path.fSources.end(), *sourceIter );
	sourceIter++;
	}

    return true;
    }

void BridgeConfig::GetTargets( vector<BCLAbstrAddressStruct*>& targets )
    {
    vector<BCLAbstrAddressStruct*>::iterator iter, end;
    targets.clear();
    iter = fTargets.begin();
    end = fTargets.end();
    while ( iter != end )
	{
	targets.insert( targets.end(), *iter );
	iter++;
	}
    }

bool BridgeConfig::GetToleranceDetector( unsigned long activeNr, Detector& detector )
    {
    vector<Detector>::iterator iter, end;
    iter = fDetectors.begin();
    end = fDetectors.end();
    while ( iter != end )
	{
	if ( iter->fActiveNr == activeNr )
	    {
	    detector = *iter;
	    return true;
	    }
	iter++;
	}
    return false;
    }

bool BridgeConfig::ReadLine( istream& istr, MLUCString& line )
    {
    char tmp[ 256 ];
    char peeked;
    line = "";
    do
	{
	if ( !istr.good() )
	    return false;
	istr.get( tmp, 256, '\n' );
	line += (char*)tmp;
	if ( istr.eof() )
	    {
	    LOG( AliHLTLog::kDebug, "BridgeConfig::ReadLine", "Line Read" )
		<< "Line '" << line.c_str() << "' read." << ENDLOG;
	    return true;
	    }
	peeked = istr.peek();
	if ( peeked == '\n' )
	    {
	    peeked = istr.get();
	    LOG( AliHLTLog::kDebug, "BridgeConfig::ReadLine", "Line Read" )
		<< "Line '" << line.c_str() << "' read." << ENDLOG;
	    return true;
	    }
	}
    while ( true );
    }

bool BridgeConfig::GetToken( const char* text, MLUCString& token, const char*& pos )
    {
    if ( !text )
	return false;
    token = "";
    const char* iter = text;
    while ( *iter && (*iter==' ' || *iter=='\t' || *iter=='\n') )
	iter++;
    if ( !*iter || *iter=='#' )
	{
	pos = NULL;
	return true;
	}
    pos = iter;
    while ( *iter && *iter!=' ' && *iter!='\t' && *iter!='\n' && *iter!='#' )
	{
	token += *iter;
	iter++;
	}
    pos = iter;
//     LOG( AliHLTLog::kDebug, "BridgeConfig::GetToken", "Token Found" )
// 	<< "Token '" << token.c_str() << "' found." << ENDLOG;
    return true;
    }


void CommandHandler::ProcessCmd( AliHLTSCCommandStruct* cmd )
    {
    AliHLTSCCommand cmdC( cmd ), bridgeCmd;
    DataPath activePath, path;
    vector<DataPath> reconnectPaths;
    vector<DataPath> connectPaths;
    vector<DataSource>::iterator sourceIter, sourceEnd;
    unsigned long activeNr, nr;
    unsigned long connectCnt = 0;
    int ret;
    
    pthread_mutex_lock( &fBridgeConfigMutex );
    LOG( AliHLTLog::kDebug, "CommandHandler::ProcessCmd", "Command received" )
	<< "Command 0x" << AliHLTLog::kHex << cmdC.GetData()->fCmd << " (" << AliHLTLog::kDec
	<< cmdC.GetData()->fCmd << ") received." << ENDLOG;
    switch ( cmdC.GetData()->fCmd )
	{
	case kAliHLTSCSetBridgeNodeStateCmd:
	    LOG( AliHLTLog::kDebug, "CommandHandler::ProcessCmd", "Set Bridge Node State command received" )
		<< "Set bridge node state command received." << ENDLOG;
	    if ( cmdC.GetData()->fParam1 )
		{
		// Set path up
		// Param0 is the real path number.
		nr = (unsigned long)cmdC.GetData()->fParam0;
		if ( !fBridgeConfig->GetPath( nr, path ) )
		    {
		    LOG( AliHLTLog::kWarning, "CommandHandler::ProcessCmd", "No Path Found" )
			<< "No Path '" << AliHLTLog::kDec << nr << "' found." << ENDLOG;
		    break;
		    }
		LOG( AliHLTLog::kInformational, "CommandHandler::ProcessCmd", "Setting Path Up" )
		    << "Setting path " << AliHLTLog::kDec << path.fNr << " to up." << ENDLOG;
		if ( !fBridgeConfig->SetPathStatus( path.fNr, true ) )
		    {
		    // Can only fail due to wrong current status of path...
		    break;
		    }
		if ( fActivesNeeded.size()>0 )
		    {
		    activeNr = *fActivesNeeded.begin();
		    if ( !fBridgeConfig->SetSpareActive( activeNr ) )
			{
			LOG( AliHLTLog::kWarning, "CommandHandler::ProcessCmd", "Error Activating Spare" )
			    << "Error setting spare new path " << AliHLTLog::kDec << path.fNr
			    << " to active path " << activeNr << "." << ENDLOG;
			break;
			}
		    if ( !fBridgeConfig->GetActivePath( activeNr, activePath ) )
			{
			LOG( AliHLTLog::kWarning, "CommandHandler::ProcessCmd", "Error Getting Activated Spare" )
			    << "Internal Error: Unable to get activated (" << AliHLTLog::kDec
			    << activeNr << " spare path " << path.fNr << "." << ENDLOG;
			break;
			}
		    // XXX

		    connectPaths.insert( connectPaths.end(), path );
		    connectCnt++;
// 		    usleep( fSendBridgeCmdDelay*1000 );
// 		    if ( !MakeConnection( endPoint, activeNode ) )
// 			{
// 			LOG( AliHLTLog::kWarning, "CommandHandler::ProcessCmd", "Error Establishing Connection" )
// 			    << "Error establishing connection between endpoint " << AliHLTLog::kDec << endPoint.fNr
// 			    << " and node " << activeNode.fNr << "." << ENDLOG;
// 			break;
// 			}
// 		    else
// 			{
// 			reconnectEndPoints.insert( reconnectEndPoints.end(), endPoint );
// 			// XXX
// 			}
		    LOG( AliHLTLog::kInformational, "CommandHandler::ProcessCmd", "Reactivated Path Up" )
			<< "Activated spare path " << AliHLTLog::kDec << activePath.fNr << " as "
			<< activePath.fActiveNr << "." << ENDLOG;
		    fActivesNeeded.erase( fActivesNeeded.begin() );
		    }
		
		}
	    else
		{
		// Set path down
		// Param0 is the active path number.
		activeNr = (unsigned long)cmdC.GetData()->fParam0;
		if ( activeNr == ~(unsigned long)0 )
		    {
		    LOG( AliHLTLog::kWarning, "CommandHandler::ProcessCmd", "Invalid Active Nr" )
			<< "Active Number " << AliHLTLog::kDec << activeNr << " is invalid." 
			<< ENDLOG;
		    break;
		    }
		if ( !fBridgeConfig->GetActivePath( activeNr, activePath ) )
		    {
		    LOG( AliHLTLog::kWarning, "CommandHandler::ProcessCmd", "No Active Path Found" )
			<< "No Active Path '" << AliHLTLog::kDec << activeNr
			<< "' found." << ENDLOG;
		    break;
		    }
		LOG( AliHLTLog::kInformational, "CommandHandler::ProcessCmd", "Setting Path Down" )
		    << "Setting active path " << AliHLTLog::kDec << activePath.fActiveNr << "/" << activePath.fNr
		    << " to down." << ENDLOG;

		bridgeCmd.GetData()->fCmd = kAliHLTSCPURGEALLEVENTSCmd;

		sourceIter = activePath.fSources.begin();
		sourceEnd = activePath.fSources.end();
		while ( sourceIter != sourceEnd )
		    {
		    if ( sourceIter->fSubscriberControlAddress )
			{
			ret = fController->SendCommand( sourceIter->fSubscriberControlAddress, bridgeCmd.GetData() );
			if ( ret )
			    {
			    LOG( AliHLTLog::kWarning, "CommandHandler::ProcessCmd", "Error Sending Subscriber EndPoint Command" )
				<< "Error sending PURGE command to subscriber endpoint '" << AliHLTLog::kDec 
				<< activeNr << "': " << strerror(ret) << " (" << ret << ")." << ENDLOG;
			    }
			}
		    sourceIter++;
		    }
		if ( activePath.fSink.fPublisherControlAddress )
		    {
		    ret = fController->SendCommand( activePath.fSink.fPublisherControlAddress, bridgeCmd.GetData() );
		    if ( ret )
			{
			LOG( AliHLTLog::kWarning, "CommandHandler::ProcessCmd", "Error Sending Publisher EndPoint Command" )
			    << "Error sending PURGE command to publisher endpoint '" << AliHLTLog::kDec 
			    << activeNr << "': " << strerror(ret) << " (" << ret << ")." << ENDLOG;
			}
		    }

		bridgeCmd.GetData()->fCmd = kAliHLTSCDisconnectCmd;
		sourceIter = activePath.fSources.begin();
		sourceEnd = activePath.fSources.end();
		while ( sourceIter != sourceEnd )
		    {
		    if ( sourceIter->fSubscriberControlAddress )
			{
			ret = fController->SendCommand( sourceIter->fSubscriberControlAddress, bridgeCmd.GetData() );
			if ( ret )
			    {
			    LOG( AliHLTLog::kWarning, "CommandHandler::ProcessCmd", "Error Sending Subscriber EndPoint Command" )
				<< "Error sending disconnect command to subscriber endpoint '" << AliHLTLog::kDec 
				<< activeNr << "': " << strerror(ret) << " (" << ret << ")." << ENDLOG;
			    }
			}
		    sourceIter++;
		    }
		if ( activePath.fSink.fPublisherControlAddress )
		    {
		    ret = fController->SendCommand( activePath.fSink.fPublisherControlAddress, bridgeCmd.GetData() );
		    if ( ret )
			{
			LOG( AliHLTLog::kWarning, "CommandHandler::ProcessCmd", "Error Sending Publisher EndPoint Command" )
			    << "Error sending disconnect command to publisher endpoint '" << AliHLTLog::kDec 
			    << activeNr << "': " << strerror(ret) << " (" << ret << ")." << ENDLOG;
			}
		    }

		// Maybe only one of these is really necessary.
		bridgeCmd.GetData()->fCmd = kAliHLTSCPURGEALLEVENTSCmd;

		sourceIter = activePath.fSources.begin();
		sourceEnd = activePath.fSources.end();
		while ( sourceIter != sourceEnd )
		    {
		    if ( sourceIter->fSubscriberControlAddress )
			{
			ret = fController->SendCommand( sourceIter->fSubscriberControlAddress, bridgeCmd.GetData() );
			if ( ret )
			    {
			    LOG( AliHLTLog::kWarning, "CommandHandler::ProcessCmd", "Error Sending Subscriber EndPoint Command" )
				<< "Error sending PURGE command to subscriber endpoint '" << AliHLTLog::kDec 
				<< activeNr << "': " << strerror(ret) << " (" << ret << ")." << ENDLOG;
			    }
			}
		    sourceIter++;
		    }
		if ( activePath.fSink.fPublisherControlAddress )
		    {
		    ret = fController->SendCommand( activePath.fSink.fPublisherControlAddress, bridgeCmd.GetData() );
		    if ( ret )
			{
			LOG( AliHLTLog::kWarning, "CommandHandler::ProcessCmd", "Error Sending Publisher EndPoint Command" )
			    << "Error sending PURGE command to publisher endpoint '" << AliHLTLog::kDec 
			    << activeNr << "': " << strerror(ret) << " (" << ret << ")." << ENDLOG;
			}
		    }
		
		if ( !fBridgeConfig->SetPathStatus( activePath.fNr, false ) )
		    {
		    // Error in setting path status to down.
		    // After the above checks, this can only be because no spare path is available...
		    // How to handle this??
		    // Store the path number in a list of missing paths and when a new path becomes available
		    // use that.
		    fActivesNeeded.insert( fActivesNeeded.end(), activeNr );
		    break;
		    }
		// This should return the new active path.
		fBridgeConfig->GetActivePath( activeNr, activePath );
		connectPaths.insert( connectPaths.end(), activePath );
		connectCnt++;
		LOG( AliHLTLog::kDebug, "CommandHandler::ProcessCmd", "Activated Spare Path" )
		    << "Activated spare path " << AliHLTLog::kDec << activePath.fNr << " as active path "
		    << activePath.fActiveNr << "." << ENDLOG;
		}
	    break;
	default:
	    LOG( AliHLTLog::kWarning, "CommandHandler::ProcessCmd", "Unknown command received" )
		<< "Unknow command 0x" << AliHLTLog::kHex << cmdC.GetData()->fCmd << " (" << AliHLTLog::kDec
		<< cmdC.GetData()->fCmd << ") received." << ENDLOG;
	    break;
	}
    pthread_mutex_unlock( &fBridgeConfigMutex );
    if ( connectCnt>0 )
	{
	pthread_mutex_lock( &fConnectsMutex );
	vector<DataPath>::iterator pathIter, pathEnd;
	vector<DataPath>::iterator pathSIter, pathSEnd;
	pathIter = connectPaths.begin();
	pathEnd = connectPaths.end();
	while ( pathIter != pathEnd )
	    {
	    pathSIter = fConnectPaths.begin();
	    pathSEnd = fConnectPaths.end();
	    while ( pathSIter != pathSEnd )
		{
		if ( pathSIter->fNr == pathIter->fNr )
		    {
		    fConnectPaths.erase( pathSIter );
		    break;
		    }
		pathSIter++;
		}
	    fConnectPaths.insert( fConnectPaths.end(), *pathIter );
	    pathIter++;
	    }
	pthread_mutex_unlock( &fConnectsMutex );
	}
    }


void CommandHandler::GetConnectsNeeded( vector<DataPath>& activePaths )
    {
    activePaths.clear();
    pthread_mutex_lock( &fConnectsMutex );
    vector<DataPath>::iterator pathIter, pathEnd;
    
    pathIter = fConnectPaths.begin();
    pathEnd = fConnectPaths.end();

    while ( pathIter != pathEnd )
	{
	activePaths.insert( activePaths.end(), *pathIter );
	pathIter++;
	}
    pthread_mutex_unlock( &fConnectsMutex );
    }

void CommandHandler::GetReconnectPaths( vector<DataPath>& paths )
    {
    paths.clear();
    pthread_mutex_lock( &fReconnectEndPointMutex );
    vector<DataPath>::iterator pathIter, pathEnd;
    pathIter = fReconnectPaths.begin();
    pathEnd = fReconnectPaths.end();
    while ( pathIter != pathEnd )
	{
	paths.insert( paths.end(), *pathIter );
	pathIter++;
	}
    pthread_mutex_unlock( &fReconnectEndPointMutex );
    }


bool CommandHandler::MakeConnection( DataPath activePath )
    {
    AliHLTSCCommand bridgeCmd;
    int ret;
    unsigned long msgLen;
    AliUInt8_t *msgPtr;
    AliHLTSCCommandStruct* msg;
    unsigned long activeNr = activePath.fActiveNr;
    vector<DataSource>::iterator sourceIter, sourceEnd;
    sourceIter = activePath.fSources.begin();
    sourceEnd = activePath.fSources.end();
    while ( sourceIter != sourceEnd )
	{
	msgLen = bridgeCmd.GetData()->fLength + strlen( sourceIter->fPublisherMsgAddressURL.c_str() ) + strlen( sourceIter->fPublisherBlobMsgAddressURL.c_str() ) + 2;
	msgPtr = new AliUInt8_t[ msgLen ];
	if ( !msgPtr )
	    {
	    LOG( AliHLTLog::kWarning, "CommandHandler::MakeConnection", "Out of Memory" )
		<< "Out of memory trying to allocate new address command message of "
		<< AliHLTLog::kDec << msgLen << " bytes." << ENDLOG;
	    return false;
	    }
	else
	    {
	    msg = ((AliHLTSCCommandStruct*)msgPtr);
	    memcpy( msgPtr, bridgeCmd.GetData(), bridgeCmd.GetData()->fLength );
	    msg->fLength = msgLen;
	    memcpy( msg->fData, sourceIter->fPublisherMsgAddressURL.c_str(), strlen(sourceIter->fPublisherMsgAddressURL.c_str())+1 );
	    memcpy( ((AliUInt8_t*)msg->fData)+strlen(sourceIter->fPublisherMsgAddressURL.c_str())+1, sourceIter->fPublisherBlobMsgAddressURL.c_str(), 
		    strlen(sourceIter->fPublisherBlobMsgAddressURL.c_str())+1 );
	    msg->fParam0 = 3; // Bits 0 1 and set to signify that both addresses are present.
	    msg->fCmd = kAliHLTSCNewConnectionCmd;
	    if ( sourceIter->fSubscriberControlAddress )
		{
		bridgeCmd.GetData()->fCmd = kAliHLTSCPURGEALLEVENTSCmd;
		ret = fController->SendCommand( sourceIter->fSubscriberControlAddress, bridgeCmd.GetData() );
		if ( ret )
		    {
		    LOG( AliHLTLog::kWarning, "CommandHandler::MakeConnection", "Error Sending Subscriber EndPoint Command" )
			<< "Error sending PURGEALLEVENTS command to subscriber endpoint '" << AliHLTLog::kDec 
			<< activeNr << "': " << strerror(ret) << " (" << ret << ")." << ENDLOG;
		    }
		ret = fController->SendCommand( sourceIter->fSubscriberControlAddress, msg );
		delete [] msgPtr;
		if ( ret )
		    {
		    LOG( AliHLTLog::kWarning, "CommandHandler::MakeConnection", "Error Sending Subscriber EndPoint Command" )
			<< "Error sending new connection command to subscriber endpoint '" << AliHLTLog::kDec 
			<< activeNr << "': " << strerror(ret) << " (" << ret << ")." << ENDLOG;
		    return false;
		    }
		}
	    }
	sourceIter++;
	}
    
    msgLen = bridgeCmd.GetData()->fLength + strlen( activePath.fSink.fSubscriberMsgAddressURL.c_str() ) + strlen( activePath.fSink.fSubscriberBlobMsgAddressURL.c_str() ) + 2;
    msgPtr = new AliUInt8_t[ msgLen ];
    if ( !msgPtr )
	{
	LOG( AliHLTLog::kWarning, "CommandHandler::MakeConnection", "Out of Memory" )
	    << "Out of memory trying to allocate new address command message of "
	    << AliHLTLog::kDec << msgLen << " bytes." << ENDLOG;
	return false;
	}
    else
	{
	msg = ((AliHLTSCCommandStruct*)msgPtr);
	memcpy( msgPtr, bridgeCmd.GetData(), bridgeCmd.GetData()->fLength );
	msg->fLength = msgLen;
	memcpy( msg->fData, activePath.fSink.fSubscriberMsgAddressURL.c_str(), strlen(activePath.fSink.fSubscriberMsgAddressURL.c_str())+1 );
	memcpy( ((AliUInt8_t*)msg->fData)+strlen(activePath.fSink.fSubscriberMsgAddressURL.c_str())+1, activePath.fSink.fSubscriberBlobMsgAddressURL.c_str(), 
		strlen(activePath.fSink.fSubscriberBlobMsgAddressURL.c_str())+1 );
	msg->fParam0 = 3; // Bits 0 1 and set to signify that both addresses are present.
	msg->fCmd = kAliHLTSCNewConnectionCmd;
	if ( activePath.fSink.fPublisherControlAddress )
	    {
	    bridgeCmd.GetData()->fCmd = kAliHLTSCPURGEALLEVENTSCmd;
	    ret = fController->SendCommand( activePath.fSink.fPublisherControlAddress, bridgeCmd.GetData() );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "CommandHandler::MakeConnection", "Error Sending Publisher EndPoint Command" )
		    << "Error sending PURGEALLEVENTS command to publisher endpoint '" << AliHLTLog::kDec 
		    << activeNr << "': " << strerror(ret) << " (" << ret << ")." << ENDLOG;
		}
	    ret = fController->SendCommand( activePath.fSink.fPublisherControlAddress, msg );
	    delete [] msgPtr;
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "CommandHandler::MakeConnection", "Error Sending Publisher EndPoint Command" )
		    << "Error sending new connection command to publisher endpoint '" << AliHLTLog::kDec 
		    << activeNr << "': " << strerror(ret) << " (" << ret << ")." << ENDLOG;
		return false;
		}
	    }
	}
    LOG( AliHLTLog::kDebug, "CommandHandler::MakeConnection", "Connection Cmds Sent" )
	<< "New connection command sent to endpoint " << AliHLTLog::kDec 
	<< activeNr << "." << ENDLOG;

    vector<DataPath>::iterator pathIter, pathEnd;
    pthread_mutex_lock( &fConnectsMutex );
    
    pathIter = fConnectPaths.begin();
    pathEnd = fConnectPaths.end();

    while ( pathIter != pathEnd )
	{
	if ( pathIter->fActiveNr == activeNr )
	    {
	    fConnectPaths.erase( pathIter );
	    break;
	    }
	pathIter++;
	}
    pthread_mutex_unlock( &fConnectsMutex );
    
    pthread_mutex_lock( &fReconnectEndPointMutex );
    fReconnectPaths.insert( fReconnectPaths.end(), activePath );
    pthread_mutex_unlock( &fReconnectEndPointMutex );
    return true;
    }

bool CommandHandler::SetTargets( unsigned long activeNr )
    {
    vector<BCLAbstrAddressStruct*> targets;
    Detector detector;
    detector.fActiveNr = ~(unsigned long)0;
    detector.fControlAddress = NULL;
    bool haveDetector = false;
    pthread_mutex_lock( &fBridgeConfigMutex );
    if ( fBridgeConfig )
	{
	fBridgeConfig->GetTargets( targets );
	haveDetector = fBridgeConfig->GetToleranceDetector( activeNr, detector );
	}
    pthread_mutex_unlock( &fBridgeConfigMutex );

    bool ret = true, tmp;
    AliHLTTolerantRoundRobinEventScattererCmd targetCmd;
    
    targetCmd.GetData()->fCmd = kAliHLTSCSetPublisherStatus;
    targetCmd.GetData()->fParam0 = activeNr;
    targetCmd.GetData()->fParam1 = 1;
    targetCmd.GetData()->fLastEventID = ~(AliUInt64_t)0;

    vector<BCLAbstrAddressStruct*>::iterator addrIter, addrEnd;
    addrIter = targets.begin();
    addrEnd = targets.end();
    while ( addrIter != addrEnd )
	{
	tmp = fController->SendCommand( *addrIter, targetCmd.GetData() );
	if ( tmp )
	    {
	    LOG( AliHLTLog::kWarning, "CommandHandler::SetTargets", "Error sending commands" )
		<< "Error sending commands to target " << AliHLTLog::kDec
		<< (unsigned long)(addrIter-targets.begin()) << ": " 
		<< strerror(tmp) << " (" << tmp << ")." << ENDLOG;
	    ret = false;
	    }
	addrIter++;
	}

    if ( haveDetector )
	{
	AliHLTSCCommand detectorCmd;
	
	detectorCmd.GetData()->fCmd = kAliHLTSCStartCmd;
	
	if ( detector.fControlAddress )
	    {
	    tmp = fController->SendCommand( detector.fControlAddress, detectorCmd.GetData() );
	    if ( tmp )
		{
		LOG( AliHLTLog::kWarning, "CommandHandler::SetTargets", "Error sending commands" )
		    << "Error sending commands to detector " << AliHLTLog::kDec
		    << detector.fActiveNr << ": " << strerror(tmp) << " (" << tmp << ")." << ENDLOG;
		ret = false;
		}
	    }
	}
    LOG( ret ? AliHLTLog::kInformational : AliHLTLog::kWarning, "CommandHandler::SetTargets", "Commands Sent" )
	<< (char*)( ret ? "Successfully" : "Unsuccessfully") << " sent commands to " << AliHLTLog::kDec << targets.size()
	<< " targets " << " and " << (haveDetector ? "detector " : "no detector ") << detector.fActiveNr << " to set publisher with index " 
	<< activeNr << " to up." << ENDLOG;

    if ( ret )
	{
	pthread_mutex_lock( &fReconnectEndPointMutex );
	vector<DataPath>::iterator pathIter, pathEnd;
	pathIter = fReconnectPaths.begin();
	pathEnd = fReconnectPaths.end();
	while ( pathIter != pathEnd )
	    {
	    if ( pathIter->fActiveNr == activeNr )
		{
		fReconnectPaths.erase( pathIter );
		break;
		}
	    pathIter++;
	    }
	pthread_mutex_unlock( &fReconnectEndPointMutex );
	}
    return ret;
    }



int main( int argc, char** argv )
    {
    AliHLTStdoutLogServer logOut;
    gLog.AddServer( logOut );

    char* usage1 = "Usage: ";
    char* usage2 = " -name <name> -controladdress <local-control-address-URL> -superviseaddress <local-supervise-address-URL> -bridgeconfig <config-file-name> (-nostdout) (-V <verbosity) (-bridgedelay <bridge-command-send-delay-ms>) (-checkinterval <bridge-status-check-interval>)";
    char* errorStr = NULL;
    int errorOptNdx = 0;
    char* cErrP;

    char* controlAddress = NULL;
    char* superviseAddress = NULL;
    char* name = NULL;
    char* configFile = NULL;
    bool nostdout = false;
    unsigned long bridgeCmdSendDelay_ms = 0;
    unsigned long statusCheckInterval_ms = 0;
    int i = 1;
    int ret;

    while ( i<argc && !errorStr )
	{
	if ( !strcmp( argv[i], "-nostdout" ) )
	    {
	    nostdout = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing verbosity level specifier.";
		errorOptNdx = i;
		break;
		}
	    gLogLevel = strtoul( argv[i+1], &cErrP, 0 );
	    if ( *cErrP )
		{
		errorStr = "Error converting verbosity level specifier.";
		errorOptNdx = i;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-bridgedelay" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing bridge command send delay specifier.";
		errorOptNdx = i;
		break;
		}
	    bridgeCmdSendDelay_ms = strtoul( argv[i+1], &cErrP, 0 );
	    if ( *cErrP )
		{
		errorStr = "Error converting bridge command send delay specifier.";
		errorOptNdx = i;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-checkinterval" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing bridge status check interval specifier.";
		errorOptNdx = i;
		break;
		}
	    statusCheckInterval_ms = strtoul( argv[i+1], &cErrP, 0 );
	    if ( *cErrP )
		{
		errorStr = "Error converting bridge status check interval specifier.";
		errorOptNdx = i;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-name" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing name specifier.";
		errorOptNdx = i;
		break;
		}
	    name = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-controladdress" ) )
	    {
	    if ( argc<=i+1 )
		{
		errorStr = "Missing control address URL option.";
		errorOptNdx = i;
		break;
		}
	    controlAddress = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-superviseaddress" ) )
	    {
	    if ( argc<=i+1 )
		{
		errorStr = "Missing supervise address URL option.";
		errorOptNdx = i;
		break;
		}
	    superviseAddress = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-bridgeconfig" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing bridge configuration file name.";
		errorOptNdx = i;
		break;
		}
	    configFile = argv[i+1];
	    i += 2;
	    continue;
	    }
	errorStr = "Unknown Option";
	errorOptNdx = i;
	}

    if ( !errorStr )
	{
	if ( !controlAddress )
	    errorStr = "Must specify the -controladdress option.";
	if ( !superviseAddress )
	    errorStr = "Must specify the -superviseAddress option.";
	if ( !name )
	    errorStr = "Must spefify the -name option.";
	if ( !configFile )
	    errorStr = "Must specify the -bridgeconfig option:";
	}

    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "BridgeToleranceManager", "Usage" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	LOG( AliHLTLog::kError, "BridgeToleranceManager", "Usage" )
	    << "Error: " << errorStr << ENDLOG;
	if ( errorOptNdx )
	    {
	    LOG( AliHLTLog::kError, "BridgeToleranceManager", "Usage" )
		<< "Offending command: " << argv[ errorOptNdx ] << "." << ENDLOG;
	    }
	return -1;
	}

    gLog.SetID( name );
    AliHLTFileLogServer fileLog( name, ".log", 3000000 );
    gLog.AddServer( fileLog );
    if ( nostdout )
	{
	gLog.DelServer( logOut );
	}

    BridgeConfig bridgeConfig;
    if ( !bridgeConfig.ReadConfig( configFile ) )
	{
	LOG( AliHLTLog::kError, "BridgeToleranceManager", "Configuration File Error" )
	    << "Error reading configuration file '" << configFile << "'." << ENDLOG;
	return -1;
	}

    AliHLTSCController controller;
    AliHLTSCInterface interface;

    CommandHandler cmdHandler( &bridgeConfig, &controller, bridgeCmdSendDelay_ms );
    interface.AddCommandProcessor( &cmdHandler );

    interface.Bind( controlAddress );
    controller.Bind( superviseAddress );

    signal( SIGQUIT, QuitSignalHandler );

    interface.Start();
    controller.Start();

    vector<DataPath> paths;
    vector<DataPath>::iterator pathIter, pathEnd;
    vector<DataSource>::iterator sourceIter, sourceEnd;
    bool status;
    bool subEPStatus, pubEPStatus;
    AliHLTSCStatusHeaderStruct* statusHeader = NULL;

    while ( !gQuit )
	{
	// First check all the already reconnected end points wether
	// they are already up again and if so inform the configured
	// targets that they can re-use that route.
	cmdHandler.GetReconnectPaths( paths );
	pathIter = paths.begin();
	pathEnd = paths.end();
	while ( pathIter != pathEnd )
	    {
	    status = true;
	    // Check status:
	    // Subscriber end point bridge head status:
	    // Read status
	    sourceIter = pathIter->fSources.begin();
	    sourceEnd = pathIter->fSources.end();
	    while ( sourceIter != sourceEnd )
		{
		ret = controller.GetStatus( sourceIter->fSubscriberControlAddress, statusHeader );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "BridgeToleranceManager", "GetStatus Error" )
			<< "Error getting status from path " << AliHLTLog::kDec
			<< pathIter->fNr << "/" << sourceIter->fSubNr 
			<< " subscriber bridge head." << ENDLOG;
		    
		    status = false;
		    }
		if ( status && statusHeader )
		    {
		    // Check status type
		    AliHLTSCStatusHeader header( statusHeader );
		    if ( header.GetData()->fStatusType == ALIL3BRIDGESTATUSTYPE )
			{
			// Check actual status itself.
			AliHLTBridgeStatusData* statusData = NULL;
			statusData = (AliHLTBridgeStatusData*)statusHeader;
			AliHLTBridgeStatus stat( &(statusData->fBridgeStatus) );
			if ( !stat.GetData()->fMsgConnected || !stat.GetData()->fBlobConnected )
			    status = false;
			LOG( AliHLTLog::kDebug, "BridgeToleranceManager", "Reconnect Status" )
			    << "Reconnect subscriber status for " << AliHLTLog::kDec << pathIter->fNr
			    << "/" << sourceIter->fSubNr
			    << ": Msg: " << stat.GetData()->fMsgConnected << " - Blob: "
			    << stat.GetData()->fBlobConnected 
			    << " - Status: " << (status ? "true" : "false") << "." << ENDLOG;
			}
		    else
			status = false;
		    
		    }
		sourceIter++;
		}

	    if ( status )
		{
		// Publisher end point bridge head status:
		// Read status
		ret = controller.GetStatus( pathIter->fSink.fPublisherControlAddress, statusHeader );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "BridgeToleranceManager", "GetStatus Error" )
			<< "Error getting status from end point " << AliHLTLog::kDec
			<< pathIter->fNr << " publisher bridge head." << ENDLOG;
		    
		    status = false;
		    }
		}
	    if ( status && statusHeader )
		{
		// Check status type
		AliHLTSCStatusHeader header( statusHeader );
		if ( header.GetData()->fStatusType == ALIL3BRIDGESTATUSTYPE )
		    {
		    // Check actual status itself.
		    AliHLTBridgeStatusData* statusP = NULL;
		    statusP = (AliHLTBridgeStatusData*)statusHeader;
		    AliHLTBridgeStatus stat( &(statusP->fBridgeStatus) );
// 		    AliHLTBridgeStatusData* status = NULL;
// 		    status = (AliHLTBridgeStatusData*)statusHeader;
// 		    AliHLTBridgeStatus stat( &(status->fBridgeStatus) );
		    if ( !stat.GetData()->fMsgConnected || !stat.GetData()->fBlobConnected )
			status = false;
		    LOG( AliHLTLog::kDebug, "BridgeToleranceManager", "Reconnect Status" )
			<< "Reconnect publisher status for " << AliHLTLog::kDec << pathIter->fNr
			<< ": Msg: " << stat.GetData()->fMsgConnected << " - Blob: "
			<< stat.GetData()->fBlobConnected 
			<< " - Status: " << (status ? "true" : "false") << "." << ENDLOG;
		    }
		else
		    status = false;
		
		}
	    
	    if ( status )
		{
		LOG( AliHLTLog::kDebug, "BridgeToleranceManager", "Setting Targets" )
		    << "Setting targets for " << AliHLTLog::kDec << pathIter->fActiveNr
		    << "." << ENDLOG;
		cmdHandler.SetTargets( pathIter->fActiveNr );
		}
	    else
		statusHeader = NULL;

	    pathIter++;
	    }


	// Check if any connections has been torn down and have rerouted via an
	// alternative path. If so, try to establish the connection to the spare path.
	cmdHandler.GetConnectsNeeded( paths );
	pathIter = paths.begin();
	pathEnd = paths.end();
	while ( pathIter != pathEnd )
	    {
	    // Check to see if both msg and blob connect status
	    // is down for both endpoints. Only then try to attempt
	    // a new connection. This is to ensure, that the 
	    // PURGEALLEVENTS cmd sent in MakeConnection really gets 
	    // all events.
	    subEPStatus=false;
	    pubEPStatus=false;
	    status = true;
	    // Check status:
	    // Subscriber end point bridge head status:
	    // Read status
	    sourceIter = pathIter->fSources.begin();
	    sourceEnd = pathIter->fSources.end();
	    while ( sourceIter != sourceEnd )
		{
		ret = controller.GetStatus( sourceIter->fSubscriberControlAddress, statusHeader );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "BridgeToleranceManager", "GetStatus Error" )
			<< "Error getting status from end point " << AliHLTLog::kDec
			<< pathIter->fNr << "/" << sourceIter->fSubNr
			<< " subscriber bridge head." << ENDLOG;
		
		    status = false;
		    }
		if ( status && statusHeader )
		    {
		    // Check status type
		    AliHLTSCStatusHeader header( statusHeader );
		    if ( header.GetData()->fStatusType == ALIL3BRIDGESTATUSTYPE )
			{
			// Check actual status itself.
			AliHLTBridgeStatusData* statusData = NULL;
			statusData = (AliHLTBridgeStatusData*)statusHeader;
			AliHLTBridgeStatus stat( &(statusData->fBridgeStatus) );
			if ( stat.GetData()->fMsgConnected || stat.GetData()->fBlobConnected )
			    subEPStatus = true;
			LOG( AliHLTLog::kDebug, "BridgeToleranceManager", "NeededConnection Status" )
			    << "Needed connection subscriber status for " << AliHLTLog::kDec << pathIter->fNr
			    << "/" << sourceIter->fSubNr
			    << ": Msg: " << stat.GetData()->fMsgConnected << " - Blob: "
			    << stat.GetData()->fBlobConnected 
			    << " - Status (Sub EP): " << (subEPStatus ? "true" : "false") << "." << ENDLOG;
			}
		    else
			status = false;
		    
		    }
		sourceIter++;
		}

	    if ( status )
		{
		// Publisher end point bridge head status:
		// Read status
		ret = controller.GetStatus( pathIter->fSink.fPublisherControlAddress, statusHeader );
		if ( ret )
		    {
		    LOG( AliHLTLog::kError, "BridgeToleranceManager", "GetStatus Error" )
			<< "Error getting status from end point " << AliHLTLog::kDec
			<< pathIter->fNr << " publisher bridge head." << ENDLOG;
		    
		    status = false;
		    }
		}
	    if ( status && statusHeader )
		{
		// Check status type
		AliHLTSCStatusHeader header( statusHeader );
		if ( header.GetData()->fStatusType == ALIL3BRIDGESTATUSTYPE )
		    {
		    // Check actual status itself.
		    AliHLTBridgeStatusData* statusData = NULL;
		    statusData = (AliHLTBridgeStatusData*)statusHeader;
		    AliHLTBridgeStatus stat( &(statusData->fBridgeStatus) );
		    if ( stat.GetData()->fMsgConnected || stat.GetData()->fBlobConnected )
			pubEPStatus = true;
		    LOG( AliHLTLog::kDebug, "BridgeToleranceManager", "NeededConnection Status" )
			<< "Needed connection publisher status for " << AliHLTLog::kDec << pathIter->fNr
			<< ": Msg: " << stat.GetData()->fMsgConnected << " - Blob: "
			<< stat.GetData()->fBlobConnected
			<< " - Status (Pub EP): " << (pubEPStatus ? "true" : "false") << "." << ENDLOG;
		    }
		else
		    status = false;

		}

	    if ( status && !pubEPStatus && !subEPStatus )
		{
		LOG( AliHLTLog::kDebug, "BridgeToleranceManager", "NeededConnection" )
		    << "Establishing needed connection for " << AliHLTLog::kDec << pathIter->fNr
		    << "." << ENDLOG;
		cmdHandler.MakeConnection( *pathIter );
		}
	    else
		statusHeader = NULL;
	    pathIter++;
	    }

	usleep( statusCheckInterval_ms );
	}

    controller.Stop();
    interface.Stop();
    return 0;
    }

void QuitSignalHandler( int )
    {
    gQuit = true;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
