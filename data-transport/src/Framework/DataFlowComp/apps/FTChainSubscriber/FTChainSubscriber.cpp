/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


//#define USE_REAL_FT


#ifndef USE_REAL_FT
#warning Warning: this program is currently very specific to the Heidelberg Test Setup
#endif

#include "AliHLTSCFaultDetectorSubscriber.hpp"
#include "AliHLTSCFaultToleranceConfiguration.hpp"
#include "AliHLTSCController.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTTolerantRoundRobinEventScattererCmd.hpp"
#include "AliHLTSCCommands.hpp"
#include "AliHLTSCStates.hpp"
#include "AliHLTSCTypes.hpp"
#include "AliHLTBridgeStatus.hpp"
#include "AliHLTBridgeStatusData.hpp"
#include "AliHLTPublisherPipeProxy.hpp"
#include "BCLAbstrAddress.hpp"
#include "BCLURLAddressDecoder.hpp"
#include "MLUCString.hpp"

class FTChainSubscriber: public AliHLTSCFaultDetectorSubscriber
    {
    public:

	FTChainSubscriber( const char* name, uint16 processStarterPort );

	virtual ~FTChainSubscriber();

	void SetFTConfig( AliHLTSCFaultToleranceConfiguration* ftConfig );

	void SetController( AliHLTSCController* controller );

	virtual void Timeout();
	virtual void EventReceived( AliEventID_t eventID );

	void CheckLoop();

	void QuitCheckLoop();

	bool HasQuittedCheckLoop()
		{
		return fQuitted;
		}

    protected:

	int SetPath( bool up );
	int DisconnectSink();
	int DisconectSources();
	//int CheckAndRepairStartNodes( vector<MLUCString>& nodes );
	//int GetReplacementStartNodes( vector<MLUCString>& nodes );
	int RepairStartNodes( bool recoveryInProgress, vector<MLUCString>& nodes, vector<MLUCString>& addnodes, bool& newNodesObtained );
	int GetReplacementAddNodes( vector<MLUCString>& nodes );
	int StartReplacementProcesses( vector<MLUCString>& nodes, vector<MLUCString>& ids );
	int ConnectSources();
	int ConnectSink();

	bool AreProcessesStarted();
	bool AreProcessesConnected();

	void BuildStarterURL( const char* hostname, uint16 starterPort, MLUCString& url, BCLAbstrAddressStruct*& addressPtr );

	pthread_mutex_t fFTMutex;
	AliHLTSCFaultToleranceConfiguration* fFTConfig;

	AliHLTSCController* fController;

	bool fRecoveryInProgress;
	struct timeval fRecoveryStarted;

	uint16 fProcessStarterPort;

	unsigned fUpdatetime_us;

	bool fQuit;
	bool fQuitted;

#ifndef USE_REAL_FT
	struct ReplacementEntry
	    {
		MLUCString fOriginalID;
		MLUCString fReplacementID;
	    };

	vector<ReplacementEntry> fReplacements;

	vector<MLUCString> fSpares;

	unsigned fIDLen;

#endif

	
    private:
    };


FTChainSubscriber::FTChainSubscriber( const char* name, uint16 processStarterPort ):
    AliHLTSCFaultDetectorSubscriber( name )
    {
    fProcessStarterPort = processStarterPort;
    fFTConfig = NULL;
    fController = NULL;
    fRecoveryInProgress = false;
    pthread_mutex_init( &fFTMutex, NULL );
    fUpdatetime_us = 500000;
    fQuit = false;
    fQuitted = true;

#ifndef USE_REAL_FT
    MLUCString tmps;
//     tmps = "304";
//     fSpares.insert( fSpares.end(), tmps );
//     tmps = "305";
//     fSpares.insert( fSpares.end(), tmps );
//     tmps = "306";
//     fSpares.insert( fSpares.end(), tmps );
    tmps = "307";
    fSpares.insert( fSpares.end(), tmps );
    fIDLen = 3;
#endif
    }

FTChainSubscriber::~FTChainSubscriber()
    {
    pthread_mutex_destroy( &fFTMutex );
    }

void FTChainSubscriber::SetFTConfig( AliHLTSCFaultToleranceConfiguration* ftConfig )
    {
    fFTConfig = ftConfig;
    }

void FTChainSubscriber::SetController( AliHLTSCController* controller )
    {
    fController = controller;
    }

void FTChainSubscriber::Timeout()
    {
    int ret;
    bool newNodes = false;
    vector<MLUCString> startnodes;
    vector<MLUCString> ids;
    vector<MLUCString> addnodes;
    pthread_mutex_lock( &fFTMutex );
    if ( fFTConfig )
	{
	fFTConfig->GetStartNodes( startnodes );
	fFTConfig->GetStartIDs( ids );
	}
    else
	{
	LOG( AliHLTLog::kFatal, "FTChainSubscriber::TimerExpired", "No FT Config" )
	    << "No FT configuration object configured. Cannot handle timeout..." << ENDLOG;
	pthread_mutex_unlock( &fFTMutex );
	return;
	}
    gettimeofday( &fRecoveryStarted, NULL );
    SetPath( false );
    DisconnectSink();
    DisconectSources();
    ret = RepairStartNodes( fRecoveryInProgress, startnodes, addnodes, newNodes );
    if ( newNodes )
	{
	fFTConfig->SetStartNodes( startnodes );
	fFTConfig->SetAddNodes( addnodes );
	StartReplacementProcesses( startnodes, ids );
	}
    fRecoveryInProgress = true;
    pthread_mutex_unlock( &fFTMutex );
    }
	
void FTChainSubscriber::EventReceived( AliEventID_t )
    {
    pthread_mutex_lock( &fFTMutex );
    fRecoveryInProgress = false;
    pthread_mutex_unlock( &fFTMutex );
    }


void FTChainSubscriber::CheckLoop()
    {
    fQuitted = false;
    fQuit = false;
    bool connectCmdSent = false;
    bool started, connected;
    while ( !fQuit )
	{
	pthread_mutex_lock( &fFTMutex );
	if ( fRecoveryInProgress )
	    {
	    started = AreProcessesStarted();
	    connected = AreProcessesConnected();
	    if ( started && !connected && !connectCmdSent )
		{
		ConnectSink();
		ConnectSources();
		connectCmdSent = true;
		}
	    if ( started && connected )
		{
		SetPath( true );
		if ( !fPaused )
		    Start();
		}
	    }
 	pthread_mutex_unlock( &fFTMutex );
	usleep( fUpdatetime_us );
	}
    fQuitted = true;
    }

void FTChainSubscriber::QuitCheckLoop()
    {
    fQuit = true;
    }

int FTChainSubscriber::SetPath( bool up )
    {
    vector<BCLAbstrAddressStruct*> targets;
    vector<BCLAbstrAddressStruct*>::iterator targetIter, targetEnd;
    fFTConfig->GetTargets( targets );

    AliHLTTolerantRoundRobinEventScattererCmd cmd;
    
    cmd.GetData()->fCmd = kAliHLTSCSetPublisherStatus;
    cmd.GetData()->fParam0 = fPublisherNdx;
    cmd.GetData()->fParam1 = up;
    cmd.GetData()->fLastEventID = fLastEventID;
    
    targetIter = targets.begin();
    targetEnd = targets.end();
    while ( targetIter != targetEnd )
	{
	fController->SendCommand( *targetIter, cmd.GetData() );
	targetIter++;
	}
    return 0;
    }

int FTChainSubscriber::DisconnectSink()
    {
    AliHLTSCCommand bridgeCmd;
    int ret;
    uint32 cmds[] = { kAliHLTSCPURGEALLEVENTSCmd, kAliHLTSCDisconnectCmd, kAliHLTSCPURGEALLEVENTSCmd };
    unsigned i, cmdCount = 3;

    AliHLTSCFaultToleranceConfiguration::DataSink sink;

    fFTConfig->GetSinkData( sink );

    if ( sink.fPublisherControlAddress )
	{
	for ( i = 0; i < cmdCount; i++ )
	    {
	    bridgeCmd.GetData()->fCmd = cmds[i];
	    ret = fController->SendCommand( sink.fPublisherControlAddress, bridgeCmd.GetData() );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "CommandHandler::ProcessCmd", "Error Sending Publisher EndPoint Command" )
		    << "Error sending command 0x" << MLUCLog::kHex << bridgeCmd.GetData()->fCmd << " to publisher endpoint '" << AliHLTLog::kDec 
		    << fPublisherNdx << "': " << strerror(ret) << " (" << ret << ")." << ENDLOG;
		}
	    }
	}
    return 0;
    }

int FTChainSubscriber::DisconectSources()
    {
    AliHLTSCCommand bridgeCmd;
    int ret;
    uint32 cmds[] = { kAliHLTSCPURGEALLEVENTSCmd, kAliHLTSCDisconnectCmd, kAliHLTSCPURGEALLEVENTSCmd };
    unsigned i, cmdCount = 3;

    vector<AliHLTSCFaultToleranceConfiguration::DataSource> sources;
    vector<AliHLTSCFaultToleranceConfiguration::DataSource>::iterator sourceIter, sourceEnd;

    fFTConfig->GetSourceData( sources );

    sourceIter = sources.begin();
    sourceEnd = sources.end();
    while ( sourceIter != sourceEnd )
	{
	if ( sourceIter->fSubscriberControlAddress )
	    {
	    for ( i = 0; i < cmdCount; i++ )
		{
		bridgeCmd.GetData()->fCmd = cmds[i];
		ret = fController->SendCommand( sourceIter->fSubscriberControlAddress, bridgeCmd.GetData() );
		if ( ret )
		    {
		    LOG( AliHLTLog::kWarning, "CommandHandler::ProcessCmd", "Error Sending Subscriber EndPoint Command" )
			<< "Error sending command 0x" << MLUCLog::kHex << bridgeCmd.GetData()->fCmd << " to subscriber endpoint '" << AliHLTLog::kDec 
			<< fPublisherNdx << "': " << strerror(ret) << " (" << ret << ")." << ENDLOG;
		    return ret;
		    }
		}
	    }
	sourceIter++;
	}
    return 0;
    }

int FTChainSubscriber::RepairStartNodes( bool, vector<MLUCString>& nodes, vector<MLUCString>& addnodes, bool& )
    {
#ifdef USE_REAL_FT
    newNodesObtained = false;
    ftHost_t ftHost;
    ftRetVal_t ftRetval;
    ftHandle_t ftHandle;
    ftHost.local = 1;
    strcpy( ftHost.host.local, stdFTSocket() );
    ftRetval = openFT( ftHost, &ftHandle );


    resourceError( ... );
    
    ftRetval = closeFT( ftHandle );
#else

    vector<ReplacementEntry>::iterator replIter, replEnd;
    vector<MLUCString>::iterator nodeIter, nodeEnd;
    MLUCString tmpID, newNode;
    const char* tmpCP;
    bool found;
    char tmpCA[1025];

    nodeIter = nodes.begin();
    nodeEnd = nodes.end();

    if ( nodeIter==nodeEnd )
	{
	nodeIter = addnodes.begin();
	nodeEnd = addnodes.end();
	}

    while ( nodeIter != nodeEnd )
	{
	found = false;

	tmpCP = nodeIter->c_str();
	if ( strlen(tmpCP)>=3 )
	    {
	    tmpID = tmpCP+strlen(tmpCP)-3;

	    replIter = fReplacements.begin();
	    replEnd = fReplacements.end();
	    
	    while ( replIter != replEnd )
		{
		if ( replIter->fOriginalID == tmpID )
		    {
		    found = true;

 		    strncpy( tmpCA, tmpCP, strlen(tmpCP)-3 );
		    tmpCA[ strlen(tmpCP)-3 ] = (char)0;
		    strcat( tmpCA, replIter->fReplacementID.c_str() );

		    LOG( AliHLTLog::kInformational, "FTChainSubscriber::RepairStartNodes", "Node re-assigned (Simulated)" )
			<< "Node assignment (already used): " << tmpCP << " -> " << tmpCA << " (IDs: "
			<< replIter->fOriginalID.c_str() << " -> " << replIter->fReplacementID.c_str() << ")."
			<< ENDLOG;

		    nodeIter->Set( tmpCA );

		    break;
		    }
		replIter++;
		}


	    if ( !found )
		{
		if ( fSpares.size()>0 )
		    {
		    ReplacementEntry repl;
		    repl.fOriginalID = tmpID;
		    repl.fReplacementID = *fSpares.begin();
		    fSpares.erase( fSpares.begin() );
		    replIter = fReplacements.insert( fReplacements.end(), repl );

 		    strncpy( tmpCA, tmpCP, strlen(tmpCP)-3 );
		    tmpCA[ strlen(tmpCP)-3 ] = (char)0;
		    strcat( tmpCA, replIter->fReplacementID.c_str() );

		    LOG( AliHLTLog::kInformational, "FTChainSubscriber::RepairStartNodes", "Node new assigned (Simulated)" )
			<< "Node assignment (new): " << tmpCP << " -> " << tmpCA << " (IDs: "
			<< replIter->fOriginalID.c_str() << " -> " << replIter->fReplacementID.c_str() << ")."
			<< ENDLOG;

		    nodeIter->Set( tmpCA );
		    }
		else
		    {
		    LOG( AliHLTLog::kError, "FTChainSubscriber::RepairStartNodes", "No replacement node found (Simulated)" )
			<< "No replacement node found for node " << tmpCP << " (ID " << tmpID.c_str() << ")." << ENDLOG;
		    }
		}
	    
	    }

	nodeIter++;
	if ( nodeIter==nodeEnd && nodeEnd!=addnodes.end() )
	    {
	    nodeIter = addnodes.begin();
	    nodeEnd = addnodes.end();
	    }
	}

#endif
    return 0;
    }

int FTChainSubscriber::StartReplacementProcesses( vector<MLUCString>& nodes, vector<MLUCString>& ids )
    {
    MLUCString url, file;
    BCLAbstrAddressStruct* addr = NULL;
    int ret;

    if ( nodes.size() != ids.size() )
	{
	LOG( AliHLTLog::kError, "FTChainSubscriber::StartReplacementProcesses", "Unequal number of nodes and ids" )
	    << "Unequal number of nodes (" << AliHLTLog::kDec << nodes.size() << ") and ids ("
	    << ids.size() << ")." << ENDLOG;
	return EINVAL;
	}
    AliHLTSCCommand cmdC;
    AliHLTSCCommandStruct* cmd;

    unsigned long size;
    vector<MLUCString>::iterator nodeIter, nodeEnd, idIter, idEnd;

    fFTConfig->GetProcessFile( file );
    nodeIter = nodes.begin();
    nodeEnd = nodes.end();
    idIter = ids.begin();
    idEnd = ids.end();
    while ( nodeIter != nodeEnd )
	{
	BuildStarterURL( nodeIter->c_str(), fProcessStarterPort, url, addr );
	if ( !addr )
	    {
	    LOG( AliHLTLog::kWarning, "FTChainSubscriber::StartReplacementProcesses", "Unable to build URL" )
		<< "Unable to build process starter URL. " << ENDLOG;
	    nodeIter++;
	    idIter++;
	    continue;
	    }
	size = cmdC.GetData()->fLength + strlen(idIter->c_str()) + strlen(file.c_str()) + 2;
	cmd = (AliHLTSCCommandStruct*) new AliUInt8_t[ size ];
	if ( !cmd )
	    {
 	    LOG( AliHLTLog::kError, "FTChainSubscriber::StartReplacementProcesses", "Out of memory" )
		<< "Out of memory trying to allocate cmd of " << AliHLTLog::kDec << size
		<< " bytes." << ENDLOG;
	    nodeIter++;
	    idIter++;
	    continue;
	    }
	memcpy( cmd, cmdC.GetData(), cmdC.GetData()->fLength );
	char *filename, *id;
	filename = (char*)cmd->fData;
	id = filename+strlen(file.c_str())+1;
	strcpy( filename, file.c_str() );
	strcpy( id, idIter->c_str() );
	cmd->fCmd = kAliHLTSCExecuteCommandCmd;

	ret = fController->SendCommand( addr, cmd );
	if ( ret )
	    {
 	    LOG( AliHLTLog::kError, "FTChainSubscriber::StartReplacementProcesses", "Error sending command" )
		<< "Error sending process start command (" << file.c_str() << "/" << idIter->c_str()
		<< ") to node " << nodeIter->c_str() << "." << ENDLOG;
	    }
	BCLFreeAddress( addr );
	addr = NULL;
	delete [] (AliUInt8_t*)cmd;
	cmd = NULL;
	nodeIter++;
	idIter++;
	}
    return 0;
    }

int FTChainSubscriber::ConnectSources()
    {
    AliHLTSCCommand bridgeCmd;
    int ret;
    unsigned long msgLen;
    AliUInt8_t *msgPtr;
    AliHLTSCCommandStruct* msg;

    vector<AliHLTSCFaultToleranceConfiguration::DataSource> sources;
    vector<AliHLTSCFaultToleranceConfiguration::DataSource>::iterator sourceIter, sourceEnd;

    fFTConfig->GetSourceData( sources );

    sourceIter = sources.begin();
    sourceEnd = sources.end();
    
    while ( sourceIter != sourceEnd )
	{
	msgLen = bridgeCmd.GetData()->fLength + strlen( sourceIter->fPublisherMsgAddressURL.c_str() ) + strlen( sourceIter->fPublisherBlobMsgAddressURL.c_str() ) + 2;
	msgPtr = new AliUInt8_t[ msgLen ];
	if ( !msgPtr )
	    {
	    LOG( AliHLTLog::kWarning, "CommandHandler::MakeConnection", "Out of Memory" )
		<< "Out of memory trying to allocate new address command message of "
		<< AliHLTLog::kDec << msgLen << " bytes." << ENDLOG;
	    return false;
	    }
	else
	    {
	    msg = ((AliHLTSCCommandStruct*)msgPtr);
	    memcpy( msgPtr, bridgeCmd.GetData(), bridgeCmd.GetData()->fLength );
	    msg->fLength = msgLen;
	    memcpy( msg->fData, sourceIter->fPublisherMsgAddressURL.c_str(), strlen(sourceIter->fPublisherMsgAddressURL.c_str())+1 );
	    memcpy( ((AliUInt8_t*)msg->fData)+strlen(sourceIter->fPublisherMsgAddressURL.c_str())+1, sourceIter->fPublisherBlobMsgAddressURL.c_str(), 
		    strlen(sourceIter->fPublisherBlobMsgAddressURL.c_str())+1 );
	    msg->fParam0 = 3; // Bits 0 1 and set to signify that both addresses are present.
	    msg->fCmd = kAliHLTSCNewConnectionCmd;
	    if ( sourceIter->fSubscriberControlAddress )
		{
		bridgeCmd.GetData()->fCmd = kAliHLTSCPURGEALLEVENTSCmd;
		ret = fController->SendCommand( sourceIter->fSubscriberControlAddress, bridgeCmd.GetData() );
		if ( ret )
		    {
		    LOG( AliHLTLog::kWarning, "CommandHandler::MakeConnection", "Error Sending Subscriber EndPoint Command" )
			<< "Error sending PURGEALLEVENTS command to subscriber endpoint: " 
			<< strerror(ret) << " (" << ret << ")." << ENDLOG;
		    }
		ret = fController->SendCommand( sourceIter->fSubscriberControlAddress, msg );
		delete [] msgPtr;
		if ( ret )
		    {
		    LOG( AliHLTLog::kWarning, "CommandHandler::MakeConnection", "Error Sending Subscriber EndPoint Command" )
			<< "Error sending new connection command to subscriber endpoint: "
			<< strerror(ret) << " (" << ret << ")." << ENDLOG;
		    return false;
		    }
		}
	    }
	sourceIter++;
	}
    return 0;
    }

int FTChainSubscriber::ConnectSink()
    {
    AliHLTSCCommand bridgeCmd;
    int ret;
    unsigned long msgLen;
    AliUInt8_t *msgPtr;
    AliHLTSCCommandStruct* msg;

    AliHLTSCFaultToleranceConfiguration::DataSink sink;
    fFTConfig->GetSinkData( sink );

    msgLen = bridgeCmd.GetData()->fLength + strlen( sink.fSubscriberMsgAddressURL.c_str() ) + strlen( sink.fSubscriberBlobMsgAddressURL.c_str() ) + 2;
    msgPtr = new AliUInt8_t[ msgLen ];
    if ( !msgPtr )
	{
	LOG( AliHLTLog::kWarning, "CommandHandler::MakeConnection", "Out of Memory" )
	    << "Out of memory trying to allocate new address command message of "
	    << AliHLTLog::kDec << msgLen << " bytes." << ENDLOG;
	return ENOMEM;
	}
    else
	{
	msg = ((AliHLTSCCommandStruct*)msgPtr);
	memcpy( msgPtr, bridgeCmd.GetData(), bridgeCmd.GetData()->fLength );
	msg->fLength = msgLen;
	memcpy( msg->fData, sink.fSubscriberMsgAddressURL.c_str(), strlen(sink.fSubscriberMsgAddressURL.c_str())+1 );
	memcpy( ((AliUInt8_t*)msg->fData)+strlen(sink.fSubscriberMsgAddressURL.c_str())+1, sink.fSubscriberBlobMsgAddressURL.c_str(), 
		strlen(sink.fSubscriberBlobMsgAddressURL.c_str())+1 );
	msg->fParam0 = 3; // Bits 0 1 and set to signify that both addresses are present.
	msg->fCmd = kAliHLTSCNewConnectionCmd;
	if ( sink.fPublisherControlAddress )
	    {
	    bridgeCmd.GetData()->fCmd = kAliHLTSCPURGEALLEVENTSCmd;
	    ret = fController->SendCommand( sink.fPublisherControlAddress, bridgeCmd.GetData() );
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "CommandHandler::MakeConnection", "Error Sending Publisher EndPoint Command" )
		    << "Error sending PURGEALLEVENTS command to publisher endpoint: " 
		    << strerror(ret) << " (" << ret << ")." << ENDLOG;
		}
	    ret = fController->SendCommand( sink.fPublisherControlAddress, msg );
	    delete [] msgPtr;
	    if ( ret )
		{
		LOG( AliHLTLog::kWarning, "CommandHandler::MakeConnection", "Error Sending Publisher EndPoint Command" )
		    << "Error sending new connection command to publisher endpoint: " 
		    << strerror(ret) << " (" << ret << ")." << ENDLOG;
		return ret;
		}
	    }
	}
    return 0;
    }


bool FTChainSubscriber::AreProcessesStarted()
    {
    MLUCString url;
    BCLAbstrAddressStruct* addr = NULL;
    
    int ret;

    AliHLTSCStatusHeaderStruct* statusHeader;
    AliHLTSCStatusHeader header;
    AliHLTSCProcessStatus processStatus;
    vector<MLUCString> nodes;
    vector<MLUCString>::iterator nodeIter, nodeEnd;

    fFTConfig->GetStartNodes( nodes );
    nodeIter = nodes.begin();
    nodeEnd = nodes.end();
    while ( nodeIter != nodeEnd )
	{
	BuildStarterURL( nodeIter->c_str(), fProcessStarterPort, url, addr );
	if ( !addr )
	    {
	    LOG( AliHLTLog::kWarning, "FTChainSubscriber::AreProcessesStarted", "Unable to build URL" )
		<< "Unable to build process starter URL. " << ENDLOG;
	    return false;
	    }

	ret = fController->GetStatus( addr, statusHeader );
	if ( ret )
	    {
	    BCLFreeAddress( addr );
	    LOG( AliHLTLog::kWarning, "FTChainSubscriber::AreProcessesStarted", "Error reading status" )
		<< "Error trying to read status from url '" << url.c_str()
		<< "': " << strerror(ret) << " (" << AliHLTLog::kDec << ret
		<< ")." << ENDLOG;
	    return false;
	    }
	
	BCLFreeAddress( addr );

	header.SetTo( statusHeader );
	if ( header.GetData()->fStatusType != ALIHLTPROCESSSTARTERSTATUSTYPE ||
	     header.GetData()->fStatusStructCnt!= 1 )
	    {
	    LOG( AliHLTLog::kWarning, "FTChainSubscriber::AreProcessesStarted", "Wrong status data" )
		<< "Read incorrect status data from url '" << url.c_str()
		<< "': Expected type " << AliHLTLog::kHex << ALIHLTPROCESSSTARTERSTATUSTYPE
		<< " instead of " << header.GetData()->fStatusType << " and 1 status structure instead of "
		<< AliHLTLog::kDec << header.GetData()->fStatusStructCnt << "." << ENDLOG;
	    fController->FreeStatus( statusHeader );
	    return false;
	    }
	processStatus.SetTo( (AliHLTSCProcessStatusStruct*)( ((AliUInt8_t*)statusHeader)+header.GetData()->fFirstStructOffset ) );
	fController->FreeStatus( statusHeader );
	if ( processStatus.GetData()->fState != kAliHLTSCProcessStarterIdleState )
	    {
	    LOG( AliHLTLog::kWarning, "FTChainSubscriber::AreProcessesStarted", "Starter not finished" )
		<< "Process starter at URL " << url.c_str() << " not finished yet. " << ENDLOG;
	    return false;
	    }
	
	nodeIter++;
	}
    return true;
    }
	
bool FTChainSubscriber::AreProcessesConnected()
    {
    AliHLTSCFaultToleranceConfiguration::DataSink sink;
    fFTConfig->GetSinkData( sink );
    int ret;
    AliHLTSCStatusHeaderStruct* statusHeader = NULL;
    AliHLTBridgeStatus stat;
    AliHLTSCStatusHeader header;
    AliHLTBridgeStatusData* status = NULL;
 
    if ( !sink.fPublisherControlAddress )
	return false;

    ret = fController->GetStatus( sink.fPublisherControlAddress, statusHeader );
    if ( ret || !statusHeader )
	{
	LOG( AliHLTLog::kError, "FTChainSubscriber::AreProcessesConnected", "Unable to read sink status" )
	    << "Unable to read sink publisher bridge head status: " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return false;
	}
    header.SetTo( statusHeader );
    if ( header.GetData()->fStatusType != ALIL3BRIDGESTATUSTYPE ||
	 header.GetData()->fStatusStructCnt!=2 )
	{
	LOG( AliHLTLog::kError, "FTChainSubscriber::AreProcessesConnected", "Wrong status data" )
	    << "Read incorrect status data from sink publisher bridge head. Expected status type "
	    << AliHLTLog::kHex << ALIL3BRIDGESTATUSTYPE << " instead of " << header.GetData()->fStatusType
	    << " and 2 structures instead of " << header.GetData()->fStatusStructCnt 
	    << "." << ENDLOG;
	return false;
	}
    status = (AliHLTBridgeStatusData*)statusHeader;
    stat.SetTo( &(status->fBridgeStatus) );
    fController->FreeStatus( statusHeader );
    if ( !stat.GetData()->fMsgConnected || !stat.GetData()->fBlobConnected )
	return false;


    vector<AliHLTSCFaultToleranceConfiguration::DataSource> sources;
    vector<AliHLTSCFaultToleranceConfiguration::DataSource>::iterator sourceIter, sourceEnd;

    fFTConfig->GetSourceData( sources );

    sourceIter = sources.begin();
    sourceEnd = sources.end();
    
    while ( sourceIter != sourceEnd )
	{
	if ( !sourceIter->fSubscriberControlAddress )
	    return false;

	ret = fController->GetStatus( sourceIter->fSubscriberControlAddress, statusHeader );
	if ( ret || !statusHeader )
	    {
	    LOG( AliHLTLog::kError, "FTChainSubscriber::AreProcessesConnected", "Unable to read source status" )
		<< "Unable to read source subscriber bridge head status: " << strerror(ret) << " (" << AliHLTLog::kDec
		<< ret << ")." << ENDLOG;
	    return false;
	    }
	header.SetTo( statusHeader );
	if ( header.GetData()->fStatusType != ALIL3BRIDGESTATUSTYPE ||
	     header.GetData()->fStatusStructCnt!=2 )
	    {
	    LOG( AliHLTLog::kError, "FTChainSubscriber::AreProcessesConnected", "Wrong status data" )
		<< "Read incorrect status data from source subscriber bridge head. Expected status type "
		<< AliHLTLog::kHex << ALIL3BRIDGESTATUSTYPE << " instead of " << header.GetData()->fStatusType
		<< " and 2 structures instead of " << header.GetData()->fStatusStructCnt 
		<< "." << ENDLOG;
	    return false;
	    }
	status = (AliHLTBridgeStatusData*)statusHeader;
	stat.SetTo( &(status->fBridgeStatus) );
	fController->FreeStatus( statusHeader );
	if ( !stat.GetData()->fMsgConnected || !stat.GetData()->fBlobConnected )
	    return false;
	
	sourceIter++;
	}
    return true;
    }


void FTChainSubscriber::BuildStarterURL( const char* hostname, uint16 starterPort, MLUCString& url, BCLAbstrAddressStruct*& addressPtr )
    {
    char tmp[32];
    int ret;
    sprintf( tmp, "%d", starterPort );

    url = "tcpmsg://";
    url += hostname;
    url += ":";
    url += tmp;
    url += "/";
    ret = BCLDecodeRemoteURL( url.c_str(), addressPtr );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "FTChainSubscriber::BuildStarterURL", "Internal error" )
	    << "Internal error, unable to decode constructed url: '" << url.c_str() 
	    << "': " << strerror(ret) << AliHLTLog::kDec << " (" << ret << ")."
	    << ENDLOG;
	}
    }


int main( int argc, char** argv )
    {
    AliHLTStdoutLogServer logOut;
    gLog.AddServer( logOut );

    char* usage1 = "Usage: ";
    char* usage2 = " -subscribe <publishername> <subscriberid> -controladdress <local-control-address-URL> -configfile <config-file-name> -starterport <process-starter-port> -datapath <data-path-nr> (-nostdout) (-V <verbosity>) (-eventtimeout <event_timeout_us>) (-transient <timeout_ms>)";
    char* errorStr = NULL;
    int errorOptNdx = 0;
    char* cErrP;

    char* controlAddressURL = NULL;
    char* subscriberID = NULL;
    char* publishername = NULL;
    char* configFile = NULL;
    uint16 starterPort = 0;
    bool starterPortSet = false;
    uint16 dataPath = 0;
    bool dataPathSet = false;
    bool nostdout = false;
    unsigned long event_timeout_us = 5000000;
    bool transient = false;
    unsigned long transientTimeout = 1000;

    AliHLTSCFaultToleranceConfiguration configuration;

    int i = 1;
    int ret;

    while ( i<argc && !errorStr )
	{
	if ( !strcmp( argv[i], "-nostdout" ) )
	    {
	    nostdout = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing verbosity level specifier.";
		errorOptNdx = i;
		break;
		}
	    gLogLevel = strtoul( argv[i+1], &cErrP, 0 );
	    if ( *cErrP )
		{
		errorStr = "Error converting verbosity level specifier.";
		errorOptNdx = i;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-subscribe" ) )
	    {
	    if ( argc <= i+2 )
		{
		errorStr = "Missing publishername or subscriber id specifier.";
		errorOptNdx = i;
		break;
		}
	    publishername = argv[i+1];
	    subscriberID = argv[i+2];
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-controladdress" ) )
	    {
	    bool isBlob;
	    BCLCommunication* com;
	    BCLAbstrAddressStruct* addr;
	    if ( argc <= i+1 )
		{
		errorStr = "Missing control address URL.";
		errorOptNdx = i;
		break;
		}
	    controlAddressURL = argv[i+1];
	    ret = BCLDecodeLocalURL( argv[i+1], com, addr, isBlob );
	    if ( ret )
		{
		errorStr = "Error decoding local control address URL.";
		errorOptNdx = i+1;
		break;
		}
	    BCLFreeAddress( com, addr );
	    if ( isBlob )
		{
		errorStr = "Blob addresses not allowed for control address URLs.";
		errorOptNdx = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-configfile" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing configuration file name.";
		errorOptNdx = i;
		break;
		}
	    configFile = argv[i+1];
	    if ( !configuration.ReadConfig( configFile ) )
		{
		errorStr = "Error reading specified configuration file.";
		errorOptNdx = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-starterport" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing process starter port number.";
		errorOptNdx = i;
		break;
		}
	    starterPort = (uint16)strtoul( argv[i+1], &cErrP, 0 );
	    starterPortSet = true;
	    if ( *cErrP )
		{
		errorStr = "Error converting process starter port number.";
		errorOptNdx = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-datapath" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing data path number.";
		errorOptNdx = i;
		break;
		}
	    dataPath = (uint16)strtoul( argv[i+1], &cErrP, 0 );
	    dataPathSet = true;
	    if ( *cErrP )
		{
		errorStr = "Error converting data path number.";
		errorOptNdx = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-eventtimeout" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing event timeout (microsec.) specifier.";
		errorOptNdx = i;
		break;
		}
	    event_timeout_us = strtoul( argv[i+1], &cErrP, 0 );
	    if ( *cErrP )
		{
		errorStr = "Error converting event timeout (microsec.) specifier.";
		errorOptNdx = i;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-transient" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing transient subscriber timeout specifier.";
		errorOptNdx = i;
		break;
		}
	    transient = true;
	    transientTimeout = strtoul( argv[i+1], &cErrP, 0 );
	    if ( *cErrP )
		{
		errorStr = "Error converting transient subscriber timeout specifier.";
		errorOptNdx = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	errorStr = "Unknown parameter";
	errorOptNdx = i;
	break;
	}

    if ( !errorStr )
	{
	if ( !publishername || !subscriberID )
	    errorStr = "Must specify -subscribe option.";
	else if ( !controlAddressURL )
	    errorStr = "Must specify the -controladdress option.";
	else if ( !configFile )
	    errorStr = "Must specify the -configfile option.";
	else if ( !starterPortSet )
	    errorStr = "Must specify the -starterport option.";
	else if ( !dataPathSet )
	    errorStr = "Must specify the -datapath option.";
	}
	
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "FTChainSubscriber", "Usage" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	LOG( AliHLTLog::kError, "FTChainSubscriber", "Usage" )
	    << "Error: " << errorStr << ENDLOG;
	if ( errorOptNdx )
	    {
	    LOG( AliHLTLog::kError, "FTChainSubscriber", "Usage" )
		<< "Offending command: " << argv[ errorOptNdx ] << "." << ENDLOG;
	    }
	return -1;
	}

    gLog.SetID( publishername );
    AliHLTFileLogServer fileLog( publishername, ".log", 3000000 );
    gLog.AddServer( fileLog );
    if ( nostdout )
	{
	gLog.DelServer( logOut );
	}

    configuration.SetDataPathNr( dataPath );

    FTChainSubscriber chainSubscriber( subscriberID, starterPort );
    chainSubscriber.SetFTConfig( &configuration );

    AliHLTSCController controller;
    controller.Bind( controlAddressURL );
    chainSubscriber.SetController( &controller );

    AliHLTPublisherPipeProxy publisher( publishername );

    AliHLTObjectThread<FTChainSubscriber> checkLoopThread( &chainSubscriber, 
							   &FTChainSubscriber::CheckLoop );

    AliHLTTimer timer;
    chainSubscriber.SetTimer( &timer );
    chainSubscriber.SetEventTimeout( event_timeout_us );
    chainSubscriber.SetPublisher( &publisher );
    chainSubscriber.SetPublisherNdx( dataPath );
    //chainSubscriber.SetStatus( );

    timer.Start();
    controller.Start();
    checkLoopThread.Start();

    publisher.Subscribe( chainSubscriber );

    if ( transient )
	{
	publisher.SetPersistent( chainSubscriber, false );
	publisher.SetTransientTimeout( chainSubscriber, transientTimeout );
	}

    publisher.StartPublishing( chainSubscriber );

    controller.Stop();
    controller.Unbind();
    timer.Stop();
    chainSubscriber.QuitCheckLoop();
    i = 0;
    while ( !chainSubscriber.HasQuittedCheckLoop() && i++ < 10 )
	usleep( 500000 );
    if ( chainSubscriber.HasQuittedCheckLoop() )
	checkLoopThread.Join();
    else
	checkLoopThread.Abort();

    return 0;
    }

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
