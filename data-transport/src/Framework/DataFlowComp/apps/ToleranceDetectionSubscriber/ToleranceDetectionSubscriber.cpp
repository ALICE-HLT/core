/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSCInterface.hpp"
#include "AliHLTSCProcessStatus.hpp"
#include "AliHLTSCToleranceDetectorStatus.hpp"
#include "AliHLTSCCommandProcessor.hpp"
#include "AliHLTSCCommands.hpp"
#include "AliHLTTDStatus.hpp"
#include "AliHLTTimer.hpp"
#include "AliHLTSubscriberInterface.hpp"
#include "AliHLTPublisherPipeProxy.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTEventDoneData.hpp"
#include "BCLURLAddressDecoder.hpp"
#include "MLUCString.hpp"
#include <pthread.h>
#include <errno.h>



class ToleranceDetectionSubscriber: public AliHLTSubscriberInterface, public AliHLTTimerCallback
    {
    public:

	ToleranceDetectionSubscriber( const char* name );

	virtual ~ToleranceDetectionSubscriber();

	void SetTimer( AliHLTTimer* timer );
	
	void SetEventTimeout( unsigned long timeout_us );

	void SetStatus( AliHLTTDStatus* status );

	void SetPublisher( AliHLTPublisherInterface* publisher );

	void SetSCInterface( AliHLTSCInterface* interface );

	void AddLAMAddress( BCLAbstrAddressStruct* address );

	void SetPublisherNdx( unsigned long ndx );

	virtual void TimerExpired( AliUInt64_t notData, unsigned long );

	// This method is called whenever a new wanted 
	// event is available.
	virtual int NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
			      const AliHLTSubEventDataDescriptor& sbevent,
			      const AliHLTEventTriggerStruct& ets );

	// This method is called for a transient subscriber, 
	// when an event has been removed from the list. 
	virtual int EventCanceled( AliHLTPublisherInterface& publisher, AliEventID_t eventID );

	virtual int EventDoneData( AliHLTPublisherInterface&, 
				   const AliHLTEventDoneData& )
		{
		return 0;
		}

	// This method is called when the subscription ends.
	virtual int SubscriptionCanceled( AliHLTPublisherInterface& publisher );

	// This method is called when the publishing 
	// service runs out of buffer space. The subscriber 
	// has to free as many events as possible and send an 
	// EventDone message with their IDs to the publisher
	virtual int ReleaseEventsRequest( AliHLTPublisherInterface& publisher ); 

	// Method used to check alive status of subscriber. 
	// This message has to be answered with 
	// PingAck message within less  than a second.
	virtual int Ping( AliHLTPublisherInterface& publisher );

	// Method used by a subscriber in response to a Ping 
	// received message. The PingAck message has to be 
	// called within less than one second.
	virtual int PingAck( AliHLTPublisherInterface& publisher );
	
    protected:

	void ProcessCmd( AliHLTSCCommandStruct* cmd );


	AliEventID_t fLastEventID;

	AliHLTTimer* fTimer;
	AliUInt32_t fTimeoutID;
	bool fTimerStarted;

	bool fPaused;
	
	unsigned long fTimeout_us;

	AliHLTPublisherInterface* fPublisher;

	unsigned long fPublisherNdx;

	AliHLTTDStatus* fStatus;

	AliHLTEventDoneData fDoneData;

	AliHLTSCInterface* fInterface;
	vector<BCLAbstrAddressStruct*> fLAMAddresses;
	
	pthread_mutex_t fMutex;

	class CommandHandler: public AliHLTSCCommandProcessor
	    {
	    public:

		CommandHandler( ToleranceDetectionSubscriber& subscriber ):
		    fSubscriber( subscriber )
			{
			}
		
		virtual void ProcessCmd( AliHLTSCCommandStruct* cmd )
			{
			fSubscriber.ProcessCmd( cmd );
			}

	    protected:

		ToleranceDetectionSubscriber& fSubscriber;

	    };

	CommandHandler fCommandHandler;

	    friend class CommandHandler;
    private:
    };


ToleranceDetectionSubscriber::ToleranceDetectionSubscriber( const char* name ):
    AliHLTSubscriberInterface( name ), fCommandHandler( *this )
    {
    fLastEventID = ~(AliEventID_t)0;
    fTimer = NULL;
    fTimeoutID = ~(AliUInt32_t)0;
    fTimerStarted = false;
    fPaused = false;
    fTimeout_us = 0;
    fPublisher = NULL;
    fInterface = NULL;
    fPublisherNdx = ~(unsigned long)0;
    AliHLTMakeEventDoneData( &fDoneData, 0 );
    pthread_mutex_init( &fMutex, NULL );
    }

ToleranceDetectionSubscriber::~ToleranceDetectionSubscriber()
    {
    pthread_mutex_destroy( &fMutex );
    }

void ToleranceDetectionSubscriber::SetTimer( AliHLTTimer* timer )
    {
    fTimer = timer;
    }

void ToleranceDetectionSubscriber::SetEventTimeout( unsigned long timeout_us )
    {
    fTimeout_us = timeout_us;
    }

void ToleranceDetectionSubscriber::SetStatus( AliHLTTDStatus* status )
    {
    fStatus = status;
    }

void ToleranceDetectionSubscriber::SetPublisher( AliHLTPublisherInterface* publisher )
    {
    fPublisher = publisher;
    }

void ToleranceDetectionSubscriber::SetSCInterface( AliHLTSCInterface* interface )
    {
    fInterface = interface;
    if ( fInterface )
	fInterface->AddCommandProcessor( &fCommandHandler );
    }

void ToleranceDetectionSubscriber::AddLAMAddress( BCLAbstrAddressStruct* lamAddress )
    {
    fLAMAddresses.insert( fLAMAddresses.end(), lamAddress );
    }

void ToleranceDetectionSubscriber::SetPublisherNdx( unsigned long ndx )
    {
    fPublisherNdx = ndx;
    }

void ToleranceDetectionSubscriber::TimerExpired( AliUInt64_t, unsigned long )
    {
    if ( !fInterface )
	{
	LOG( AliHLTLog::kError, "ToleranceDetectionSubscriber::TimerExpired", "No SC Interface" )
	    << "No SC interface object specified." << ENDLOG;
	return;
	}
    LOG( AliHLTLog::kInformational, "ToleranceDetectionSubscriber::TimerExpired", "Timer Expired" )
	<< "Timer expired for publisher " << AliHLTLog::kDec << fPublisherNdx 
	<< " with last event 0x" << AliHLTLog::kHex << fLastEventID 
	<< " (" << AliHLTLog::kDec << fLastEventID << ")." << ENDLOG;
    pthread_mutex_lock( &fMutex );
    fTimerStarted = false;
    fStatus->fToleranceStatus.fState = 0;
    struct timeval now;
    gettimeofday( &now, NULL );
    fStatus->fProcessStatus.fLastUpdateTime_s = now.tv_sec;
    fStatus->fProcessStatus.fLastUpdateTime_us = now.tv_usec;
    pthread_mutex_unlock( &fMutex );

    vector<BCLAbstrAddressStruct*>::iterator iter, end;
    iter = fLAMAddresses.begin();
    end = fLAMAddresses.end();
    while ( iter != end )
	{
	if ( *iter )
	    fInterface->LookAtMe( *iter );
	iter++;
	}
    }

int ToleranceDetectionSubscriber::NewEvent( AliHLTPublisherInterface& publisher, AliEventID_t eventID, 
					    const AliHLTSubEventDataDescriptor&,
					    const AliHLTEventTriggerStruct& )
    {
    LOG( AliHLTLog::kInformational, "ToleranceDetectionSubscriber::NewEvent", "New Event" )
	<< "New event received from publisher " << publisher.GetName() 
	<< ": 0x" << AliHLTLog::kHex << eventID 
	<< " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
    if ( !fTimer )
	return ENODEV;
    pthread_mutex_lock( &fMutex );
    fStatus->fToleranceStatus.fState = 1;
    fStatus->fToleranceStatus.fLastEventID = eventID;
    fLastEventID = eventID;
    struct timeval now;
    gettimeofday( &now, NULL );
    fStatus->fProcessStatus.fLastUpdateTime_s = now.tv_sec;
    fStatus->fProcessStatus.fLastUpdateTime_us = now.tv_usec;

    if ( !fPaused )
	{
	if ( fTimerStarted )
	    {
	    fTimer->NewTimeout( fTimeoutID, fTimeout_us/1000 );
	    }
	else
	    {
	    fTimeoutID = fTimer->AddTimeout( fTimeout_us/1000, this, 0 );
	    fTimerStarted = true;
	    }
	}

    pthread_mutex_unlock( &fMutex );
    fDoneData.fEventID = eventID;
    publisher.EventDone( *this, fDoneData );
    LOG( AliHLTLog::kInformational, "ToleranceDetectionSubscriber::NewEvent", "Event Done" )
	<< "Event 0x" << AliHLTLog::kHex << eventID 
	<< " (" << AliHLTLog::kDec << eventID << ") from publisher " << publisher.GetName() 
	<< " done." << ENDLOG;
    return 0;
    }

int ToleranceDetectionSubscriber::EventCanceled( AliHLTPublisherInterface&, AliEventID_t )
    {
    return 0;
    }

int ToleranceDetectionSubscriber::SubscriptionCanceled( AliHLTPublisherInterface& )
    {
    return 0;
    }

int ToleranceDetectionSubscriber::ReleaseEventsRequest( AliHLTPublisherInterface& )
    {
    return 0;
    }

int ToleranceDetectionSubscriber::Ping( AliHLTPublisherInterface& publisher )
    {
    publisher.PingAck( *this );
    return 0;
    }

int ToleranceDetectionSubscriber::PingAck( AliHLTPublisherInterface& )
    {
    return 0;
    }

void ToleranceDetectionSubscriber::ProcessCmd( AliHLTSCCommandStruct* cmd )
    {
    AliHLTSCCommand cmdS( cmd );
    switch ( cmdS.GetData()->fCmd )
	{
	case kAliHLTSCPauseCmd:
	    {
	    LOG( AliHLTLog::kInformational, "ToleranceDetectionSubscriber::ProcessCmd", "Pause Cmd" )
		<< "Received Pause Command." << ENDLOG;
	    pthread_mutex_lock( &fMutex );
	    fPaused = true;
	    if ( fTimerStarted )
		{
		fTimer->CancelTimeout( fTimeoutID );
		fTimerStarted = false;
		}
	    pthread_mutex_unlock( &fMutex );
	    break;
	    }
	case kAliHLTSCStartCmd:
	    {
	    LOG( AliHLTLog::kInformational, "ToleranceDetectionSubscriber::ProcessCmd", "Start Cmd" )
		<< "Received Start Command." << ENDLOG;
	    pthread_mutex_lock( &fMutex );
	    if ( fPaused )
		fPaused = false;
	    if ( !fTimerStarted )
		{
		fTimeoutID = fTimer->AddTimeout( fTimeout_us/1000, this, 0 );
		fTimerStarted = true;
		}
	    pthread_mutex_unlock( &fMutex );
	    break;
	    }
	}
    }


int main( int argc, char** argv )
    {
    AliHLTStdoutLogServer logOut;
    gLog.AddServer( logOut );

    int i = 1;
    char* usage1 = "Usage: ", *usage2 = " -subscribe <publishername> <subscriber-ID> -eventtimeout <event-timeout-usec> -publisherndx <publisher-ndx> -controlurl <local-SC-addres-URL> -lamurl <remote-SC-LAM-address-URL> (-nostdout) (-V <verbosity>) (-transient <timeout_ms>)";
    char* errorStr = NULL;
    int errorOptNdx = 0;
    char* cErrP;
    char* publisherName = NULL;
    char* subscriberID = NULL;
    unsigned publisherNdx = ~(unsigned)0;
    bool nostdout = false;
    unsigned long eventTimeout = 0;
    char* localSCAddress = NULL;
    vector<char*> remoteSCLAMAddresses;
    bool transient = false;
    unsigned long transientTimeout = 1000;

    while ( i<argc && !errorStr )
	{
	if ( !strcmp( argv[i], "-subscribe" ) )
	    {
	    if ( argc<=i+2 )
		{
		errorStr = "Missing publisher-name or subscriber-ID option.";
		errorOptNdx = i;
		break;
		}
	    publisherName = argv[ i+1 ];
	    subscriberID = argv[ i+2 ];
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-nostdout" ) )
	    {
	    nostdout = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing verbosity level specifier.";
		errorOptNdx = i;
		break;
		}
	    gLogLevel = strtoul( argv[i+1], &cErrP, 0 );
	    if ( *cErrP )
		{
		errorStr = "Error converting verbosity level specifier.";
		errorOptNdx = i;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-transient" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing transient subscriber timeout specifier.";
		errorOptNdx = i;
		break;
		}
	    transient = true;
	    transientTimeout = strtoul( argv[i+1], &cErrP, 0 );
	    if ( *cErrP )
		{
		errorStr = "Error converting transient subscriber timeout specifier.";
		errorOptNdx = i;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-publisherndx" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing supervised publisher ndx specifier.";
		errorOptNdx = i;
		break;
		}
	    publisherNdx = strtoul( argv[i+1], &cErrP, 0 );
	    if ( *cErrP )
		{
		errorStr = "Error converting supervised publisher ndx specifier.";
		errorOptNdx = i;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-eventtimeout" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing event timeout specifier.";
		errorOptNdx = i;
		break;
		}
	    eventTimeout = strtoul( argv[i+1], &cErrP, 0 );
	    if ( *cErrP )
		{
		errorStr = "Error converting event timeout specifier.";
		errorOptNdx = i;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-controlurl" ) )
	    {
	    if ( argc<=i+1 )
		{
		errorStr = "Missing local-SC-address option.";
		errorOptNdx = i;
		break;
		}
	    localSCAddress = argv[ i+1 ];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-lamurl" ) )
	    {
	    if ( argc<=i+1 )
		{
		errorStr = "Missing remote-SC-LAM-address option.";
		errorOptNdx = i;
		break;
		}
	    remoteSCLAMAddresses.insert( remoteSCLAMAddresses.end(), argv[i+1] );
	    i += 2;
	    continue;
	    }
	errorStr = "Unknown option";
	break;
	}

    if ( !errorStr )
	{
	if ( !publisherName )
	    errorStr = "Must specify -subscribe option.";
	else if ( !eventTimeout )
	    errorStr = "Must specify -eventtimeout option.";
	else if ( !localSCAddress )
	    errorStr = "Must specify -systemcontrol option.";
	else if ( remoteSCLAMAddresses.size()<=0 )
	    errorStr = "Must specify at least one -lam option.";
	else if ( publisherNdx == ~(unsigned)0 )
	    errorStr = "Must specify -publisherndx option";
	}
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "ToleranceDetectionSubscriber", "Usage" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	LOG( AliHLTLog::kError, "ToleranceDetectionSubscriber", "Usage" )
	    << "Error: " << errorStr << ENDLOG;
	if ( errorOptNdx )
	    {
	    LOG( AliHLTLog::kError, "ToleranceDetectionSubscriber", "Usage" )
		<< "Offending command: " << argv[ errorOptNdx ] << "." << ENDLOG;
	    }
	return -1;
	}


    MLUCString name;
    name = "ToleranceDetectionSubscriber-";
    name += subscriberID;

    AliHLTTimer timer;
    AliHLTSCInterface interface;
    AliHLTTDStatus status;
    status.fToleranceStatus.fPublisherNdx = publisherNdx;
    status.fToleranceStatus.fState = 1;
    status.fToleranceStatus.fLastEventID = ~(AliEventID_t)0;
    interface.SetStatusData( &(status.fHeader) );
    BCLAbstrAddressStruct* remoteBCLAddress;
    vector<char*>::iterator lamIter, lamEnd;
    vector<BCLAbstrAddressStruct*> lamAddresses;
    vector<BCLAbstrAddressStruct*>::iterator lamAddrIter, lamAddrEnd;
    lamIter = remoteSCLAMAddresses.begin();
    lamEnd = remoteSCLAMAddresses.end();
    while ( lamIter != lamEnd )
	{
	BCLDecodeRemoteURL( *lamIter, remoteBCLAddress );
	lamAddresses.insert( lamAddresses.end(), remoteBCLAddress );
	lamIter++;
	}

    AliHLTFileLogServer fileLog( name.c_str(), ".log", 3000000 );
    gLog.AddServer( fileLog );
    if ( nostdout )
	{
	gLog.DelServer( logOut );
	}
    
    AliHLTPublisherPipeProxy publisher( publisherName );
    ToleranceDetectionSubscriber subscriber( subscriberID );

    subscriber.SetPublisher( &publisher );
    subscriber.SetTimer( &timer );
    subscriber.SetSCInterface( &interface );
    subscriber.SetStatus( &status );
    subscriber.SetPublisherNdx( publisherNdx );
    subscriber.SetEventTimeout( eventTimeout );
    lamAddrIter = lamAddresses.begin();
    lamAddrEnd = lamAddresses.end();
    while ( lamAddrIter != lamAddrEnd )
	{
	if ( *lamAddrIter )
	    subscriber.AddLAMAddress( *lamAddrIter );
	lamAddrIter++;
	}

    interface.Bind( localSCAddress );

    timer.Start();
    interface.Start();

    publisher.Subscribe( subscriber );
    if ( transient )
	{
	publisher.SetPersistent( subscriber, false );
	publisher.SetTransientTimeout( subscriber, transientTimeout );
	}

    publisher.StartPublishing( subscriber );

    interface.Stop();
    interface.Unbind();
    timer.Stop();
    
    return 0;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
