/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLMsgCommunication.hpp"
#include "BCLBlobCommunication.hpp"
#include "BCLURLAddressDecoder.hpp"
#include "AliHLTPublisherBridgeHead.hpp"
#include "BCLSCIComID.hpp"
#include "AliHLTPubSubPipeCom.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "AliHLTThread.hpp"
#include "AliHLTPipeController.hpp"
#include "AliHLTSCInterface.hpp"
#include "AliHLTSCCommands.hpp"
#include "BCLErrorLogCallback.hpp"
#include "BCLStackTraceCallback.hpp"
// XXX
#include "BCLTCPComID.hpp"
#include "BCLTCPBlobCommunication.hpp"
// XXX
#include <fstream>
#include <iostream>
#include <sys/time.h>
#include <unistd.h>
#ifdef LIBC_MEMDEBUG
#include <mcheck.h>
#endif

template<class T>
class PublisherBridgeHeadThread: public AliHLTThread
    {
    public:

	PublisherBridgeHeadThread( T* obj, void (T::*func)(void) )
		{
		fObj = obj;
		fFunc = func;
		}

    protected:

	T* fObj;
	void (T::*fFunc)(void);

	virtual void Run()
		{
		(fObj->*fFunc)();
		}
    };


class PublisherBridgeHeadFailCallback: public BCLErrorCallback
    {
    public:
	
	PublisherBridgeHeadFailCallback( bool& failVar ):
	    fFailVar( failVar )
		{
		fFailVar = false;
		}

	virtual BCLErrorAction ConnectionTemporarilyLost( int, BCLCommunication*,
							  BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction ConnectionError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction BindError( int, BCLCommunication*,
					       BCLAbstrAddressStruct*, 
					     BCLMessageStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction MsgSendError( int, BCLCommunication*,
					       BCLAbstrAddressStruct*, 
					     BCLMessageStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction MsgReceiveError( int, BCLCommunication*,
					       BCLAbstrAddressStruct*, 
						BCLMessageStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction BlobPrepareError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction BlobSendError( int, BCLCommunication*,
					      BCLAbstrAddressStruct*, 
					      BCLTransferID ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction BlobReceiveError( int, BCLCommunication*,
						 BCLTransferID ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction AddressError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

	virtual BCLErrorAction GeneralError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { fFailVar = true; return kAbort; };

    protected:
	
	bool& fFailVar;

    };


class TolerantPublisherBridgeHeadLAM: public AliHLTBridgeHeadLAMProxy
    {
    public:
 
 	TolerantPublisherBridgeHeadLAM( AliHLTSCInterface* interface,
					vector<BCLAbstrAddressStruct*>& lamAddresses ):
 	    fInterface( interface ),
 	    fLAMAddresses( lamAddresses )
 		{
 		}
 
 	virtual void LookAtMe();
 
    protected:
 
 	AliHLTSCInterface* fInterface;
 	vector<BCLAbstrAddressStruct*>& fLAMAddresses;
 
    };
 
struct TolerantPublisherBridgeHeadComData
    {
 	TolerantPublisherBridgeHeadComData()
 		{
 		fBlobMsgCom = NULL;
 		fMsgCom = NULL;
 		fBlobCom = NULL;
 		fOwnBlobMsgAddress=NULL;
 		fOwnBlobAddress=NULL; 
 		fOwnMsgAddress=NULL;
 		fRemoteBlobMsgAddress=NULL;
 		fRemoteMsgAddress=NULL;
 		
 		fBlobBufferSize = 0;
 		//fCommunicationTimeout = 10000; // Timeout of 10s, given in ms.
		fCommunicationTimeout = 0;
 		}
 
 	BCLMsgCommunication* fBlobMsgCom;
 	BCLMsgCommunication* fMsgCom;
 	BCLBlobCommunication* fBlobCom;
 	BCLAbstrAddressStruct *fOwnBlobMsgAddress, 
	    *fOwnBlobAddress, 
	    *fOwnMsgAddress,
	    *fRemoteBlobMsgAddress,
	    *fRemoteMsgAddress;
 	
 	uint32 fBlobBufferSize;
 	uint32 fCommunicationTimeout; // Timeout of 10s, given in ms.
    };
 
class TolerantPublisherBridgeHeadCommandHandler: public AliHLTSCCommandProcessor
    {
    public:
 	
 	TolerantPublisherBridgeHeadCommandHandler( AliHLTPublisherBridgeHead* bridgeHead,
						   TolerantPublisherBridgeHeadComData* comData );
 	
 	virtual void ProcessCmd( AliHLTSCCommandStruct* cmd );
 	
    protected:
 	
 	AliHLTPublisherBridgeHead* fBridgeHead;
 	TolerantPublisherBridgeHeadComData* fComData;
 
    };
 
class TolerantPublisherBridgeHeadErrorCallback: public AliHLTBridgeHeadErrorCallback<AliHLTPublisherBridgeHead>
    {
    public:

		TolerantPublisherBridgeHeadErrorCallback();

		virtual ~TolerantPublisherBridgeHeadErrorCallback();

		virtual void ConnectionError( AliHLTPublisherBridgeHead* bridgeHead );
		
		virtual void ConnectionEstablished( AliHLTPublisherBridgeHead* bridgeHead );
	
    protected:
    private:
    };

 
void TolerantPublisherBridgeHeadLAM::LookAtMe()
    {
    vector<BCLAbstrAddressStruct*>::iterator addrIter, addrEnd;
    int ret;
 
    addrIter = fLAMAddresses.begin();
    addrEnd = fLAMAddresses.end();
     
    while ( addrIter != addrEnd )
 	{
 	ret = fInterface->LookAtMe( *addrIter );
 	if ( ret )
 	    {
 	    LOG( AliHLTLog::kError, "TolerantPublisherBridgeHeadLAM::LookAtMe", "LAM Send Error" )
 		<< "Error sending LAM to address ";
 		BCLAddressLogger::LogAddress( gLog, **addrIter )
 		    << ": " << strerror(ret) << " (" << AliHLTLog::kDec
 		    << ret << ")." << ENDLOG;
 	    }
 	addrIter++;
 	}
    }
 
TolerantPublisherBridgeHeadCommandHandler::TolerantPublisherBridgeHeadCommandHandler( AliHLTPublisherBridgeHead* bridgeHead,
										      TolerantPublisherBridgeHeadComData* comData ):
    fBridgeHead( bridgeHead ), fComData( comData )
    {
    }
 	
void TolerantPublisherBridgeHeadCommandHandler::ProcessCmd( AliHLTSCCommandStruct* cmd )
    {
    if ( !cmd )
 	return;
    AliHLTSCCommand cmdS( cmd );
     
    LOG( AliHLTLog::kDebug, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "Command received" )
 	<< "Command 0x" << AliHLTLog::kHex << cmdS.GetData()->fCmd << " (" << AliHLTLog::kDec
 	<< cmdS.GetData()->fCmd << ") received." << ENDLOG;
    char* newMsgComURL = NULL;
    char* newBlobMsgComURL = NULL;
    unsigned long offset = 0;
    unsigned long dataOffset;
    int ret;
    BCLAbstrAddressStruct *newMsgAddr = NULL, 
 			  *oldMsgAddr = fComData->fRemoteMsgAddress,
		      *newBlobMsgAddr = NULL,
		      *oldBlobMsgAddr = fComData->fRemoteBlobMsgAddress;
 
    dataOffset = ((unsigned long)cmd->fData) - ((unsigned long)cmd);
    switch ( cmdS.GetData()->fCmd )
 	{
 	case kAliHLTSCDisconnectCmd:
 	    LOG( AliHLTLog::kDebug, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "Disconnect command received" )
 		<< "Disconnect command received." << ENDLOG;
 	    fBridgeHead->Disconnect( true, true, 1000 );
 	    break;
 	case kAliHLTSCConnectCmd:
 	    LOG( AliHLTLog::kDebug, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "Connect command received" )
 		<< "Connect command received." << ENDLOG;
 	    fBridgeHead->Connect();
 	    break;
 	case kAliHLTSCReconnectCmd:
 	    LOG( AliHLTLog::kDebug, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "Reconnect command received" )
 		<< "Reconnect command received." << ENDLOG;
 	    if ( fBridgeHead )
 		{
 		fBridgeHead->Pause();
  		fBridgeHead->LockCom();
 		fBridgeHead->Disconnect( true, true, 1000, false );
 		fBridgeHead->Connect( false );
  		fBridgeHead->UnlockCom();
 		fBridgeHead->Restart();
 		}
 	    break;
 	case kAliHLTSCNewConnectionCmd: // Fallthrough
 	    if ( true )
 		{
 		LOG( AliHLTLog::kDebug, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "New connection command received" )
 		    << "New connection command received." << ENDLOG;
 		}
 	    else
		case kAliHLTSCNewRemoteAddressesCmd:
               {
	       LOG( AliHLTLog::kDebug, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "New address command received" )
		   << "New address command received." << ENDLOG;
	       }
 	    if ( fBridgeHead )
 		{
 		if ( cmdS.GetData()->fParam0 & 1 )
 		    {
 		    newMsgComURL = (char*)cmd->fData;
 		    if ( (strlen(newMsgComURL)+dataOffset) > cmdS.GetData()->fLength )
 			{
 			LOG( AliHLTLog::kWarning, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "Length Error" )
 			    << "Error (1) in message length: " << AliHLTLog::kDec << (strlen(newMsgComURL)+dataOffset)
 			    << " greater than " << cmdS.GetData()->fLength << ENDLOG;
 			}
 		    else
 			{
 			LOG( AliHLTLog::kDebug, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "New msg address" )
 			    << "New Msg Address: " << newMsgComURL << "." << ENDLOG;
 			}
 		    ret = BCLDecodeRemoteURL( newMsgComURL, newMsgAddr );
 		    if ( ret || !newMsgAddr )
 			{
 			LOG( AliHLTLog::kError, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "Error in msg com URL" )
 			    << "Error decoding new remote msg com address URL: " << strerror(ret) 
 			    << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
 			if ( newMsgAddr )
 			    {
 			    BCLFreeAddress( newMsgAddr );
 			    newMsgAddr = NULL;
 			    }
 			}
 		    else if ( newMsgAddr && fComData->fMsgCom && newMsgAddr->fComID != fComData->fMsgCom->GetComID() )
 			{
 			LOG( AliHLTLog::kError, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "Wrong Com ID" )
 			    << "Com ID " << AliHLTLog::kDec << newMsgAddr->fComID
 			    << " in new remote msg address is unequal to com ID " << fComData->fMsgCom->GetComID()
 			    << " in msg com object." << ENDLOG;
 			BCLFreeAddress( newMsgAddr );
 			newMsgAddr = NULL;
 			}
 		    offset += strlen( newMsgComURL )+1;
 		    }
 		if ( cmdS.GetData()->fParam0 & 2 )
 		    {
 		    newBlobMsgComURL = ((char*)(cmd->fData))+offset;
 		    if ( (strlen(newBlobMsgComURL)+dataOffset+offset) > cmdS.GetData()->fLength )
 			{
 			LOG( AliHLTLog::kWarning, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "Length Error" )
 			    << "Error (2) in message length: " << AliHLTLog::kDec << (strlen(newBlobMsgComURL)+dataOffset+offset)
 			    << " greater than " << cmdS.GetData()->fLength << ENDLOG;
 			}
 		    else
 			{
 			LOG( AliHLTLog::kDebug, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "New blob msg address" )
 			    << "New Blob Msg Address: " << newBlobMsgComURL << "." << ENDLOG;
 			}
 		    ret = BCLDecodeRemoteURL( newBlobMsgComURL, newBlobMsgAddr );
 		    if ( ret || !newBlobMsgAddr )
 			{
 			LOG( AliHLTLog::kError, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "Error in blob msg com URL" )
 			    << "Error decoding new remote blob msg com address URL: " << strerror(ret) 
 			    << " (" << AliHLTLog::kDec << ret << ")." << ENDLOG;
 			if ( newBlobMsgAddr )
 			    {
 			    BCLFreeAddress( newBlobMsgAddr );
 			    newMsgAddr = NULL;
 			    newBlobMsgAddr = NULL;
 			    }
 			}
 		    else if ( newBlobMsgAddr && fComData->fBlobMsgCom && newBlobMsgAddr->fComID != fComData->fBlobMsgCom->GetComID() )
 			{
 			LOG( AliHLTLog::kError, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "Wrong Com ID" )
 			    << "Com ID " << AliHLTLog::kDec << newBlobMsgAddr->fComID
 			    << " in new remote blob msg address is unequal to com ID " << fComData->fBlobMsgCom->GetComID()
 			    << " in msg com object." << ENDLOG;
 			BCLFreeAddress( newBlobMsgAddr );
 			newMsgAddr = NULL;
 			newBlobMsgAddr = NULL;
 			}
 		    }
 
 		fBridgeHead->Pause();
  		fBridgeHead->LockCom();
 		fBridgeHead->Disconnect( true, true, 1000, false );
 		if ( newMsgAddr )
 		    {
 		    fBridgeHead->SetRemoteMsgAddress( newMsgAddr );
 		    fComData->fRemoteMsgAddress = newMsgAddr;
 		    }
 		if ( newBlobMsgAddr )
 		    {
 		    fBridgeHead->SetRemoteBlobMsgAddress( newBlobMsgAddr );
 		    fComData->fRemoteBlobMsgAddress = newBlobMsgAddr;
 		    }
 		if ( cmdS.GetData()->fCmd == kAliHLTSCNewConnectionCmd )
 		    fBridgeHead->Connect( false ); // No connect in case of new address command.
 		fBridgeHead->UnlockCom();
 		fBridgeHead->Restart();
 		if ( newMsgAddr )
 		    {
 		    BCLFreeAddress( oldMsgAddr );
 		    }
 		if ( newBlobMsgAddr )
 		    {
 		    BCLFreeAddress( oldBlobMsgAddr );
 		    }
 		}
 	    break;
	case kAliHLTSCPURGEALLEVENTSCmd:
	    LOG( AliHLTLog::kInformational, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "PURGE command received" )
		<< "PURGE ALL EVENTS command received." << ENDLOG;
	    if ( fBridgeHead )
		fBridgeHead->PURGEALLEVENTS();
	    LOG( AliHLTLog::kInformational, "TolerantPublisherBridgeHeadCommandHandler::ProcessCmd", "ALL EVENTS PURGED" )
		<< "ALL EVENTS PURGED!!" << ENDLOG;
	    break;
 	}
    }
 
 

TolerantPublisherBridgeHeadErrorCallback::TolerantPublisherBridgeHeadErrorCallback()
    {
    }

TolerantPublisherBridgeHeadErrorCallback::~TolerantPublisherBridgeHeadErrorCallback()
    {
    }

void TolerantPublisherBridgeHeadErrorCallback::ConnectionError( AliHLTPublisherBridgeHead* bridgeHead )
    {
    if ( bridgeHead )
	{
	LOG( AliHLTLog::kWarning, "TolerantPublisherBridgeHeadErrorCallback::ConnectionError", "Connection Error" )
	    << "Connection Error in PublisherBridgeHead. Pausing processing." << ENDLOG;
	bridgeHead->Pause();
	bridgeHead->LockCom();
	bridgeHead->Disconnect( true, true, 1000, false );
	bridgeHead->UnlockCom();
	}
    }
		
void TolerantPublisherBridgeHeadErrorCallback::ConnectionEstablished( AliHLTPublisherBridgeHead* bridgeHead )
    {
    if ( bridgeHead )
	{
	bridgeHead->Restart();
	}
    }



int main( int argc, char** argv )
    {
#ifdef LIBC_MEMDEBUG
    mtrace();
#endif
    uint32 i;
    char* errorStr = NULL;
    const char* usage1 = "Usage: ";
    const char* usage2 = " [-blobmsg blobmsg-own-address-URL blobmsg-remote-address-URL ] [ -blob blobsize blob-own-address-URL ] [ -msg msg-own-address-URL msg-remote-address-URL ] (-V verbosity_levels) -publisher publishername (-timeout connectiontimeout_ms) (-eventtimeout eventtimeout_ms) (-shm [bigphys|physmem|sysv] <shmkey>) (-nostdout) (-noconnect) (-retrycount <count>) (-retrytime <time-in-milliseconds>) (-publishertimeout <timeout_ms>) (-mindescriptorblocks <minimum descriptor block count>) (-maxdescriptorblocks <maximum descriptor block count>) (-slotcount <(event) slot count>) -controlurl <System-Control-URL> (-lamurl <LAM-URL>)*";
    char* cerr;
    char* publisherName = NULL;
    AliHLTFilteredStdoutLogServer sOut;
    BCLAddressLogger::Init();
    gLogLevel = AliHLTLog::kAll;
    gLog.AddServer( sOut );
    BCLErrorLogCallback logCallback;
    BCLStackTraceCallback traceCallback( 7 );
    bool comFailure = false;
    PublisherBridgeHeadFailCallback failureCallback( comFailure );
    gLogLevel = AliHLTLog::kWarning|AliHLTLog::kError|AliHLTLog::kFatal;

    bool shmAddCopy = false;

    TolerantPublisherBridgeHeadComData comData;
//     BCLMsgCommunication* blobMsgCom = NULL;
//     BCLMsgCommunication* msgCom = NULL;
//     BCLBlobCommunication* blobCom = NULL;
//     BCLAbstrAddressStruct *ownBlobMsgAddress=NULL, 
// 			     *ownBlobAddress=NULL, 
// 			      *ownMsgAddress=NULL,
// 		       *remoteBlobMsgAddress=NULL,
// 			   *remoteMsgAddress=NULL;

    //uint32 blobBufferSize = 0;
    //uint32 connectionTimeout = 10000; // Timeout of 10s, given in ms.
    uint32 eventTimeout = 10000; // Timeout of 10s, given in ms.
    AliUInt32_t shmID = (AliUInt32_t)-1;;
    AliHLTShmID shmKey;
    shmKey.fShmType = kInvalidShmType;
    shmKey.fKey.fID = ~(AliUInt64_t)0;
    bool nostdout = false;
    AliUInt32_t retryCount = 0;
    bool retryCountSet = false;
    AliUInt32_t retryTime = 0;
    bool retryTimeSet = false;
    AliUInt32_t publisherTimeout = ~(AliUInt32_t)0;
    AliUInt32_t minSEDDBlockCnt = 1;
    AliUInt32_t maxSEDDBlockCnt = 3;
    AliUInt32_t slotCnt = 256;
    unsigned int slotCntExp2;
    vector<BCLAbstrAddressStruct*> lamAddresses;
    char* controlAddressURL = NULL;
    bool noConnect = false;
    int ret;

    i = 1;
    while ( (i < (uint32)argc) && !errorStr )
	{
	if ( !strcmp( argv[i], "-nostdout" ) )
	    {
	    nostdout = true;
	    i++;
	    continue;
	    }
	if ( !strcmp( argv[i], "-blob" ) )
	    {
	    if ( (uint32)argc <= i +2 )
		{
		errorStr = "Missing blob address options.";
		break;
		}
	    if ( comData.fBlobCom )
		{
		errorStr = "Duplicate -blob option.";
		break;
		}
	    comData.fBlobBufferSize = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting blob buffersize argument.";
		break;
		}
	    bool isBlob;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+2], com_tmp, comData.fOwnBlobAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid blob address URL";
		break;
		}
	    if ( !isBlob )
		{
		errorStr = "No msg addresses allowed in client blob URL";
		break;
		}
	    comData.fBlobCom = (BCLBlobCommunication*)com_tmp;
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-blobmsg" ) )
	    {
	    if ( (uint32)argc <= i +2 )
		{
		errorStr = "Missing blob msg address options.";
		break;
		}
	    if ( comData.fBlobMsgCom )
		{
		errorStr = "Duplicate -blobmsg option.";
		break;
		}
	    bool isBlob;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+1], com_tmp, comData.fOwnBlobMsgAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid blob-msg address URL";
		break;
		}
	    if ( isBlob )
		{
		errorStr = "No blob addresses allowed in client blob-msg URL";
		break;
		}
	    ret = BCLDecodeRemoteURL( argv[i+2], comData.fRemoteBlobMsgAddress );
	    if ( ret )
		{
		errorStr = "Invalid blob-msg address URL";
		break;
		}
	    comData.fBlobMsgCom = (BCLMsgCommunication*)com_tmp;
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-msg" ) )
	    {
	    if ( (uint32)argc <= i +2 )
		{
		errorStr = "Missing msg address options.";
		break;
		}
	    if ( comData.fMsgCom )
		{
		errorStr = "Duplicate -msg option.";
		break;
		}
	    bool isBlob;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+1], com_tmp, comData.fOwnMsgAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid msg address URL";
		break;
		}
	    if ( isBlob )
		{
		errorStr = "No blob addresses allowed in msg URL";
		break;
		}
	    ret = BCLDecodeRemoteURL( argv[i+2], comData.fRemoteMsgAddress );
	    if ( ret )
		{
		errorStr = "Invalid msg address URL";
		break;
		}
	    comData.fMsgCom = (BCLMsgCommunication*)com_tmp;
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing verbosity level specifier.";
		break;
		}
	    gLogLevel = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting verbosity level specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-retrytime" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing retry time specifier.";
		break;
		}
	    retryTime = strtoul( argv[i+1], &cerr, 0 );
	    retryTimeSet = true;
	    if ( *cerr )
		{
		errorStr = "Error converting retry time specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-retrycount" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing retry count specifier.";
		break;
		}
	    retryCount = strtoul( argv[i+1], &cerr, 0 );
	    retryCountSet = true;
	    if ( *cerr )
		{
		errorStr = "Error converting retry count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-publishertimeout" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing publisher timeout specifier.";
		break;
		}
	    publisherTimeout = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting publisher timeout specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-shm" ) )
	    {
	    if ( (uint32)argc < i +3 )
		{
		errorStr = "Missing shm key specifier.";
		break;
		}
	    if ( !strcmp( argv[i+1], "bigphys" ) )
		shmKey.fShmType = kBigPhysShmType;
	    else if ( !strcmp( argv[i+1], "physmem" ) )
		shmKey.fShmType = kPhysMemShmType;
	    else if ( !strcmp( argv[i+1], "sysv" ) )
		shmKey.fShmType = kSysVShmType;
	    else
		{
		errorStr = "Invalid shm type specifier...";
		break;
		}
	    shmKey.fKey.fID = strtoul( argv[i+2], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting shm key specifier..";
		break;
		}
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-publisher" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing publishername specifier.";
		break;
		}
	    publisherName = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-timeout" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing connection timeout specifier.";
		break;
		}
	    comData.fCommunicationTimeout = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting connection timeout specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-eventtimeout" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing event timeout specifier.";
		break;
		}
	    eventTimeout = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting event timeout specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-mindescriptorblocks" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing minimum event descriptor block count specifier.";
		break;
		}
	    minSEDDBlockCnt = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting minimum event descriptor block count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-maxdescriptorblocks" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing maximum event descriptor block count specifier.";
		break;
		}
	    maxSEDDBlockCnt = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting maximum event descriptor block count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-slotcount" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing (event) slot count specifier.";
		break;
		}
	    slotCnt = strtoul( argv[i+1], &cerr, 0 );
	    if ( *cerr )
		{
		errorStr = "Error converting (event) slot count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
 	if ( !strcmp( argv[i], "-noconnect" ) )
 	    {
 	    noConnect = true;
 	    i++;
 	    continue;
 	    }
 	if ( !strcmp( argv[i], "-controlurl" ) )
 	    {
 	    if (  (uint32)argc <= i+1 )
 		{
 		errorStr = "Missing system control URL parameter";
 		break;
 		}
 	    controlAddressURL = argv[i+1];
 	    i += 2;
 	    continue;
 	    }
 	if ( !strcmp( argv[i], "-lamurl" ) )
 	    {
 	    BCLAbstrAddressStruct* tmpLAM = NULL;
 	    if (  (uint32)argc <= i+1 )
 		{
 		errorStr = "Missing LAM address URL parameter";
 		break;
 		}
	    ret = BCLDecodeRemoteURL( argv[i+1], tmpLAM );
 	    if ( ret || !tmpLAM )
 	      {
 	      errorStr = "Error decoding LAM address URL parameter.";
 	      break;
 	      }
 	    lamAddresses.insert( lamAddresses.end(), tmpLAM );
 	    i += 2;
 	    continue;
 	    }
	errorStr = "Unknown option";
	}
    if ( !errorStr )
	{
	if ( !publisherName)
	    errorStr = "Must specify -publisher publishername option.";
	if ( !comData.fBlobCom || !comData.fMsgCom || !comData.fBlobMsgCom )
	    errorStr = "Must specify one each of the blob, blobmsg and msg parameters";
 	if ( !controlAddressURL )
 	    errorStr = "Must specify the -controlurl option.";
	}
    if ( errorStr )
	{
	LOG( MLUCLog::kError, "PublisherBridgeHead", "Command line options" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	MLUCString urls;
	vector<MLUCString> msgurls, bloburls;
	BCLGetAllowedURLs( msgurls, bloburls );
	vector<MLUCString>::iterator iter, end;
	iter = msgurls.begin();
	end = msgurls.end();
	while ( iter != end )
	    {
	    urls += *iter;
	    if ( iter+1 != end )
		urls += ", ";
	    iter++;
	    }

	iter = bloburls.begin();
	end = bloburls.end();
	if ( iter != end )
	    urls += ", ";
	while ( iter != end )
	    {
	    urls += *iter;
	    if ( iter+1 != end )
		urls += ", ";
	    iter++;
	    }
	LOG( MLUCLog::kError, "PublisherBridgeHead", "Command line options" )
	    << "Allowed address URLS: " << urls.c_str() << "." << ENDLOG;
	LOG( MLUCLog::kError, "PublisherBridgeHead", "Command line options" )
	    << errorStr << ENDLOG;
	return -1;
	}

    if ( nostdout )
	gLog.DelServer( sOut );

    if ( shmKey.fShmType!=kInvalidShmType && comData.fBlobCom->GetComID() == kSCIComID )
      shmAddCopy = true;

    comData.fBlobCom->SetMsgCommunication( comData.fBlobMsgCom );

    ofstream of;
    MLUCString name;
    name = "PublisherBridgeHead-";
    name += publisherName;
    name += "-";
    AliHLTFileLogServer fileOut( name.c_str(), ".log", 3000000 );
    gLog.AddServer( fileOut );

    //name = "PublisherBridgeHead-";
    //name += publisherName;
    name = publisherName;
    name += "Bridge";
    AliHLTPipeController pipeControl( name.c_str() );
    pipeControl.AddValue( *(AliUInt32_t*)&gLogLevel );
    pipeControl.Start();

    comData.fMsgCom->AddCallback( &logCallback );
    comData.fBlobMsgCom->AddCallback( &logCallback );
    comData.fBlobCom->AddCallback( &logCallback );
    comData.fMsgCom->AddCallback( &traceCallback );
    comData.fBlobMsgCom->AddCallback( &traceCallback );
    comData.fBlobCom->AddCallback( &traceCallback );
    comData.fMsgCom->AddCallback( &failureCallback );
    comData.fBlobMsgCom->AddCallback( &failureCallback );
    comData.fBlobCom->AddCallback( &failureCallback );
    comData.fMsgCom->SetReceiveTimeout( comData.fCommunicationTimeout );
    comData.fBlobMsgCom->SetReceiveTimeout( comData.fCommunicationTimeout );
    comData.fBlobCom->SetReceiveTimeout( comData.fCommunicationTimeout );

    LOG( AliHLTLog::kDebug, "PublisherBridgeHead", "Communication Timeout" )
	<< "Communication timeout: " << AliHLTLog::kDec << comData.fCommunicationTimeout
	<< "." << ENDLOG;

    AliHLTSharedMemory shm;
    AliUInt8_t* blobPtr = NULL;
    bool success = false;

    if ( comData.fBlobCom->GetComID() == kSCIComID && !shmAddCopy )
	{
	comData.fBlobCom->SetBlobBuffer( comData.fBlobBufferSize );
	}
    else
	{
	  if ( shmKey.fShmType != kInvalidShmType )
	    {
	      AliUInt32_t size = comData.fBlobBufferSize;
	      shmID = shm.GetShm( shmKey, size );
	      if ( shmID != (AliUInt32_t)-1 )
		success = true;
	    }
	  else
	    {
	      shmKey.fShmType = kBigPhysShmType;
	      shmKey.fKey.fID = 1;
	      AliUInt32_t size = comData.fBlobBufferSize;
	      while ( !success )
		{
		  shmID = shm.GetShm( shmKey, size, true );
		  if ( shmID == (AliUInt32_t)-1 )
		    {
		      if ( size==0 )
			size = comData.fBlobBufferSize;
		      else
			break;
		    }
		  else
		    success = true;
		  if ( shmKey.fKey.fID >= (~(AliUInt64_t)0) - 1 )
		      break;
		  ++shmKey.fKey.fID;
		}
	    }
	if ( success )
	    {
	      //shm.ReleaseShm( shmID );
	      //shmID = shm.GetShm( shmKey, size );
	      blobPtr = (AliUInt8_t*)shm.LockShm( shmID );
	      if ( comData.fBlobCom->GetComID() != kSCIComID )
		comData.fBlobCom->SetBlobBuffer( comData.fBlobBufferSize, blobPtr );
	      else
		comData.fBlobCom->SetBlobBuffer( comData.fBlobBufferSize );
	    }
	else
	    {
	    shmKey.fShmType = kInvalidShmType;
	    shmKey.fKey.fID = ~(AliUInt64_t)0;
	    }
	}

    comData.fBlobMsgCom->Bind( comData.fOwnBlobMsgAddress );
    comData.fBlobCom->Bind( comData.fOwnBlobAddress );
    comData.fMsgCom->Bind( comData.fOwnMsgAddress );

    if ( comData.fBlobCom->GetComID() == kSCIComID && !shmAddCopy )
	{
	AliUInt8_t* pv;
	void* pp;
	pv = comData.fBlobCom->GetBlobBuffer();
	if ( AliHLTSharedMemory::GetPhysicalAddress( (void*)pv, &pp, NULL ) )
	    {
	    LOG( AliHLTLog::kInformational, "PublisherBridgeHead", "Memories" )
		<< "Virtual address: 0x" << AliHLTLog::kHex << (AliUInt32_t)pv << " - Physical address: 0x"
		<< (AliUInt32_t)pp << ENDLOG;
	    AliUInt32_t size = comData.fBlobBufferSize;
	    shmKey.fKey.fID = reinterpret_cast<AliUInt64_t>(pp);
	    shmKey.fShmType = kPhysMemShmType;
	    shmID = shm.GetShm( shmKey, size );
	    }
	else
	    {
	    shmKey.fShmType = kInvalidShmType;
	    shmKey.fKey.fID = ~(AliUInt64_t)0;
	    }
	}

    

    if ( comFailure )
	{
	LOG( AliHLTLog::kFatal, "PublisherBridgeHead", "Communication error" )
	    << "A communication error occured. Aborting..." << ENDLOG;
	//comData.fBlobCom->Disconnect( comData.fRemoteBlobMsgAddress );
	//comData.fMsgCom->Disconnect( comData.fRemoteMsgAddress );
	comData.fBlobCom->Unbind();
	comData.fBlobMsgCom->Unbind();
	comData.fMsgCom->Unbind();
	BCLFreeAddress( comData.fRemoteMsgAddress );
	BCLFreeAddress( comData.fRemoteBlobMsgAddress );
	BCLFreeAddress( comData.fMsgCom, comData.fOwnMsgAddress );
	BCLFreeAddress( comData.fBlobCom, comData.fOwnBlobAddress );
	BCLFreeAddress( comData.fBlobMsgCom, comData.fOwnBlobMsgAddress );
	shm.ReleaseShm( shmID );
	return -1;
	}

    AliUInt32_t mask = 0x80000000;
    slotCntExp2 = sizeof(mask)*8-1;
    while ( ! (mask & slotCnt) )
	{
	mask >>= 1;
	slotCntExp2--;
	}
    mask = 1 << slotCntExp2;
    if ( slotCntExp2 < sizeof(mask)*8-1 && slotCnt>mask )
	slotCntExp2++;

    AliHLTBridgeStatusData bridgeStatus;
    AliHLTSCInterface scInterface;
    scInterface.SetStatusData( &(bridgeStatus.fHeader) );
    TolerantPublisherBridgeHeadLAM lamHandler( &scInterface, lamAddresses );

    AliHLTDescriptorHandler descriptorHandler( slotCntExp2 );

    name = publisherName;
    TolerantPublisherBridgeHeadErrorCallback errorCallback;
    AliHLTPublisherBridgeHead publisher( name.c_str(), slotCntExp2 );
    if ( shmAddCopy && comData.fBlobCom->GetComID() == kSCIComID )
      {
	publisher.SetShmCopy( true, blobPtr );
      }
    publisher.SetDescriptorHandler( &descriptorHandler );
    publisher.SetMsgCom( comData.fMsgCom, comData.fRemoteMsgAddress );
    publisher.SetBlobCom( comData.fBlobCom, comData.fBlobMsgCom, comData.fRemoteBlobMsgAddress );
    publisher.SetShmID( shmKey );
    publisher.SetSubscriptionLoop( &PublisherPipeSubscriptionInputLoop );
    publisher.SetCommunicationTimeout( comData.fCommunicationTimeout );
    publisher.SetErrorCallback( &errorCallback );

    publisher.SetLAMProxy( &lamHandler );
    publisher.SetStatusData( &bridgeStatus );

    TolerantPublisherBridgeHeadCommandHandler cmdHandler( &publisher, &comData );
    scInterface.AddCommandProcessor( &cmdHandler );

    PublisherBridgeHeadThread<AliHLTPublisherBridgeHead> 
	publisherMsgThread( &publisher,
			     &AliHLTPublisherBridgeHead::AcceptSubscriptions );
    publisherMsgThread.Start();
    if ( retryTimeSet )
	publisher.SetRetryTime( retryTime );
    if ( retryCountSet )
	publisher.SetMaxRetryCount( retryCount );


    scInterface.Bind( controlAddressURL );
    scInterface.Start();

    publisher.SetMaxTimeout( publisherTimeout );
    publisher.Start();
    if ( !noConnect )
	publisher.Connect();
    
    publisher.MsgLoop();
    publisher.Stop();

    AliUInt32_t td = 0;
    struct timeval t1, t2;
    gettimeofday( &t1, NULL );
    while ( publisher.GetPendingEventCount()>0 && (eventTimeout<=0 || td<eventTimeout) )
	{
	usleep( 10 );
	gettimeofday( &t2, NULL );
	td = (t2.tv_usec-t1.tv_usec)/1000 + (t2.tv_sec-t1.tv_sec)*1000;
	}

    QuitPipeSubscriptionLoop( publisher );
    LOG( AliHLTLog::kDebug, "PublisherBridgeHead", "Quitted Subscription loop" )
	<< "PipeSubscriptionLoop quitted." << ENDLOG;
    publisher.CancelAllSubscriptions( true );
    LOG( AliHLTLog::kDebug, "PublisherBridgeHead", "Subscriptions Cancelled" )
	<< "All subscriptions cancelled." << ENDLOG;

    scInterface.Stop();


    //comData.fBlobCom->Disconnect( comData.fRemoteBlobMsgAddress );
    //comData.fMsgCom->Disconnect( comData.fRemoteMsgAddress );
    comData.fBlobCom->Unbind();
    comData.fBlobMsgCom->Unbind();
    comData.fMsgCom->Unbind();
    BCLFreeAddress( comData.fRemoteMsgAddress );
    BCLFreeAddress( comData.fRemoteBlobMsgAddress );
    BCLFreeAddress( comData.fMsgCom, comData.fOwnMsgAddress );
    BCLFreeAddress( comData.fBlobCom, comData.fOwnBlobAddress );
    BCLFreeAddress( comData.fBlobMsgCom, comData.fOwnBlobMsgAddress );
    shm.ReleaseShm( shmID );
    return 0;
    }
    
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
