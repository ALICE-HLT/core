/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTDStatus.hpp"
#include "AliHLTToleranceStatus.hpp"
#include "AliHLTSCController.hpp"
#include "AliHLTSCLAMHandler.hpp"
#include "AliHLTSCStatusHeader.hpp"
#include "AliHLTSCStatusBase.hpp"
#include "AliHLTSCProcessStatus.hpp"
#include "AliHLTSCToleranceDetectorStatus.hpp"
#include "AliHLTLog.hpp"
#include "AliHLTLogServer.hpp"
#include "BCLCommunication.hpp"
#include "BCLAbstrAddress.hpp"
#include "BCLURLAddressDecoder.hpp"
#include "BCLAddressLogger.hpp"
#include "BCLErrorLogCallback.hpp"
#include "AliHLTTolerantRoundRobinEventScattererCmd.hpp"
#include "AliHLTSCCommands.hpp"
#include <signal.h>


void QuitSignalHandler( int sig );


class ToleranceLAMHandler: public AliHLTSCLAMHandler
    {
    public:

	ToleranceLAMHandler( AliHLTTimerConditionSem* signal )
		{
		fSignal = signal;
		}

	virtual void LookAtMe( BCLAbstrAddressStruct* me );

    protected:
	
	AliHLTTimerConditionSem* fSignal;

    };

class ToleranceSupervisor
    {
    public:

	ToleranceSupervisor( vector<BCLAbstrAddressStruct*>* supervisedAddresses, 
			     vector<BCLAbstrAddressStruct*>* targetAddresses,
			     unsigned long interval_ms,
			     AliHLTSCController* controller,
			     bool pauseClients )
		{
		fQuit = false;
		fQuitted = true;
		fSupervisedAddresses = supervisedAddresses;
		fTargetAddresses = targetAddresses;
		fInterval = interval_ms;
		fController = controller;
		fPauseClients = pauseClients;
		}
	~ToleranceSupervisor()
		{
		}

	void Quit()
		{
		fQuit = true;
		}
	bool HasQuitted()
		{
		return fQuitted;
		}

	void SetBridgeAddresses( vector<BCLAbstrAddressStruct*> bridgeAddresses )
		{
		fBridgeAddresses.clear();
		vector<BCLAbstrAddressStruct*>::iterator iter, end;
		iter = bridgeAddresses.begin();
		end = bridgeAddresses.end();
		while ( iter != end )
		    {
		    fBridgeAddresses.insert( fBridgeAddresses.end(), *iter );
		    iter++;
		    }
		}

	void Supervise();

	AliHLTTimerConditionSem fSignal;

    protected:

	void Check();
	void Set( unsigned long ndx, bool up, AliEventID_t lastEventID );
	void PauseClients();

	vector<BCLAbstrAddressStruct*>* fSupervisedAddresses;
	vector<BCLAbstrAddressStruct*>* fTargetAddresses;
	vector<AliUInt8_t> fOk;
	vector<AliUInt8_t> fOldOk;
	vector<AliEventID_t> fLastEventIDs;

	vector<BCLAbstrAddressStruct*> fBridgeAddresses;

	AliHLTSCController* fController;

	unsigned long fInterval; // in millisecs.

	bool fPauseClients;

	bool fQuit;
	bool fQuitted;

    private:
    };

ToleranceSupervisor *gSupervisor = NULL;


void ToleranceLAMHandler::LookAtMe( BCLAbstrAddressStruct* me )
    {
    LOG( AliHLTLog::kWarning, "ToleranceLAMHandler::LookAtMe", "LAM received" )
	<< "Received LookAtMe request from address `";
	BCLAddressLogger::LogAddress( gLog, *me )
	    << "`." << ENDLOG;
    if ( fSignal )
	fSignal->Signal();

    }


void ToleranceSupervisor::Supervise()
    {
    fQuitted = false;
    while ( !fQuit )
	{
	if ( fInterval )
	    fSignal.WaitTime( fInterval );
	Check();

	if ( fOk.size()!=fOldOk.size() || fOk.size()!=fLastEventIDs.size() || fOldOk.size()!=fLastEventIDs.size() )
	    {
	    LOG( AliHLTLog::kFatal, "ToleranceSupervisor::Supervise", "Internal Error" )
		<< "Ok, OldOk, fLastEventIDs arrays have unequal size." << ENDLOG;
	    }

	vector<AliUInt8_t>::iterator newIter, newEnd, oldIter;
	vector<AliEventID_t>::iterator eventIDIter;
	newIter = fOk.begin();
	oldIter = fOldOk.begin();
	eventIDIter = fLastEventIDs.begin();
	newEnd = fOk.end();
	unsigned long ndx = 0;
	bool foundChange = false;

	while ( newIter != newEnd )
	    {
	    if ( *oldIter != *newIter )
		{
		foundChange = true;
		break;
		}
	    newIter++;
	    oldIter++;
	    }
	if ( foundChange )
	    {
	    PauseClients();
	    newIter = fOk.begin();
	    oldIter = fOldOk.begin();
	    eventIDIter = fLastEventIDs.begin();
	    newEnd = fOk.end();
	    while ( newIter != newEnd )
		{
		if ( *oldIter != *newIter )
		    {
		    LOG( AliHLTLog::kInformational, "ToleranceSupervisor::Supervise", "Status Change" )
			<< "Status change for supervised process with index " << AliHLTLog::kDec
			<< ndx << " changed from " << *oldIter << " to " << *newIter 
			<< " - Last event: 0x" << AliHLTLog::kHex << *eventIDIter
			<< " (" << AliHLTLog::kDec << *eventIDIter << ")." << ENDLOG;
		    Set( ndx, (bool)*newIter, *eventIDIter );
		    }
		*oldIter = *newIter;
		ndx++;
		newIter++;
		oldIter++;
		eventIDIter++;
		}
	    }
	
	}
    fQuitted = true;
    }

void ToleranceSupervisor::Check()
    {
    int ret;
    unsigned long ndx;
    AliUInt8_t state;
    AliEventID_t eventID;
    AliHLTSCStatusHeaderStruct* statusHeader = NULL;
    vector<BCLAbstrAddressStruct*>::iterator addrIter, addrEnd;

    addrIter = fSupervisedAddresses->begin();
    addrEnd = fSupervisedAddresses->end();
    while ( addrIter != addrEnd )
	{
	ret = fController->GetStatus( *addrIter, statusHeader );
	if ( ret )
	    {
	    LOG( AliHLTLog::kError, "ToleraceSupervisorThread::Check", "GetStatus Error" )
		<< "Error getting status from client address URL '";
		BCLAddressLogger::LogAddress( gLog, **addrIter )
		    << "': " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << ")." << ENDLOG;
	    addrIter++;
	    continue;
	    }
	
	if ( statusHeader )
	    {
	    AliHLTSCStatusHeader header( statusHeader );
	    if ( header.GetData()->fStatusType == ALIL3TDSTATUSTYPE )
		{
		AliHLTTDStatus* status = NULL;
		status = (AliHLTTDStatus*)statusHeader;
		AliHLTSCToleranceDetectorStatus stat( &(status->fToleranceStatus) );
		
		ndx = stat.GetData()->fPublisherNdx;
		state = stat.GetData()->fState;
		eventID = stat.GetData()->fLastEventID;
		
		LOG( AliHLTLog::kDebug, "ToleranceSupervisor::Check", "Status Read" )
		    << "Read status for supervised process with index " << AliHLTLog::kDec
		    << ndx << ": " << state << "  - Last event: 0x" << AliHLTLog::kHex << eventID
		    << " (" << AliHLTLog::kDec << eventID << ")." << ENDLOG;
		
		while ( ndx >= fOk.size() )
		    {
		    fOk.insert( fOk.end(), (AliUInt8_t)1 );
		    fOldOk.insert( fOldOk.end(), (AliUInt8_t)1 );
		    fLastEventIDs.insert( fLastEventIDs.end(), ~(AliEventID_t)0 );
		    }
		*(fOk.begin()+ndx) = state;
		*(fLastEventIDs.begin()+ndx) = eventID;
		}
	    else if ( header.GetData()->fStatusType == ALIL3TOLERANCESTATUSTYPE )
		{
		AliHLTToleranceStatus* status = (AliHLTToleranceStatus*)statusHeader;
		AliHLTSCToleranceChannelStatus stat;
		stat.AdoptFully( &(status->fChannelStatus) );
		for ( ndx = 0; ndx < stat.GetData()->fChannelsCnt; ndx++ )
		    {
		    LOG( AliHLTLog::kDebug, "ToleranceSupervisor::Check", "Status Read" )
			<< "Read status for supervised process with index " << AliHLTLog::kDec
			<< ndx << ": " << stat.GetData()->fChannels[ndx] << "." << ENDLOG;
		    while ( ndx >= fOk.size() )
			{
			fOk.insert( fOk.end(), (AliUInt8_t)1 );
			fOldOk.insert( fOldOk.end(), (AliUInt8_t)1 );
			fLastEventIDs.insert( fLastEventIDs.end(), ~(AliEventID_t)0 );
			}
		    if ( !stat.GetData()->fChannels[ndx] && *(fOk.begin()+ndx) )
			*(fOk.begin()+ndx) = 0;
		    }
		}
	    else
		{
		LOG( AliHLTLog::kWarning, "ToleranceSupervisor::Check", "Unknown Status" )
		    << "Unknown Status: 0x" << AliHLTLog::kHex << header.GetData()->fStatusType
		    << " (" << AliHLTLog::kDec << header.GetData()->fStatusType << ")."
		    << ENDLOG;
		}
	    }
	else
	    {
	    LOG( AliHLTLog::kError, "ToleraceSupervisorThread::Check", "GetStatus Error" )
		<< "Error getting status (NULL Pointer) from client address URL '";
		BCLAddressLogger::LogAddress( gLog, **addrIter )
		    << "': " << strerror(ret) << " (" << AliHLTLog::kDec
		    << ret << ")." << ENDLOG;
	    }
	
	addrIter++;
	}
    }


void ToleranceSupervisor::Set( unsigned long ndx, bool up, AliEventID_t lastEventID )
    {
    AliHLTTolerantRoundRobinEventScattererCmd cmd;
    
    cmd.GetData()->fCmd = kAliHLTSCSetPublisherStatus;
    cmd.GetData()->fParam0 = ndx;
    cmd.GetData()->fParam1 = up;
    cmd.GetData()->fLastEventID = lastEventID;

    vector<BCLAbstrAddressStruct*>::iterator addrIter, addrEnd;
    addrIter = fTargetAddresses->begin();
    addrEnd = fTargetAddresses->end();
    while ( addrIter != addrEnd )
	{
	fController->SendCommand( *addrIter, cmd.GetData() );
	addrIter++;
	}
    LOG( AliHLTLog::kDebug, "ToleranceSupervisor::Set", "Commands Sent" )
	<< "Sent commands to " << AliHLTLog::kDec << fTargetAddresses->size()
	<< " to set publisher with index " << ndx << " to " << (up ? "up" : "down")
	<< "." << ENDLOG;
	
    unsigned long n = 0;
    if ( !up )
	{
	addrIter = fBridgeAddresses.begin();
	addrEnd = fBridgeAddresses.end();
	while ( addrIter != addrEnd )
	    {
	    cmd.GetData()->fCmd = kAliHLTSCSetBridgeNodeStateCmd;
	    cmd.GetData()->fParam0 = ndx;
	    cmd.GetData()->fParam1 = 0;
	    fController->SendCommand( *addrIter, cmd.GetData() );

	    LOG( AliHLTLog::kDebug, "ToleranceSupervisor::Set", "Bridge Commands Sent" )
		<< "Sent command to bridge manager " << AliHLTLog::kDec << n
		<< " to set bridge node '" << AliHLTLog::kDec << ndx
		<< "' status to down." << ENDLOG;
	    addrIter++;
	    n++;
	    }
	}
    }

void ToleranceSupervisor::PauseClients()
    {
    AliHLTTolerantRoundRobinEventScattererCmd cmd;
    
    cmd.GetData()->fCmd = kAliHLTSCPauseCmd;

    vector<BCLAbstrAddressStruct*>::iterator addrIter, addrEnd;

    addrIter = fSupervisedAddresses->begin();
    addrEnd = fSupervisedAddresses->end();
    while ( addrIter != addrEnd )
	{
	fController->SendCommand( *addrIter, cmd.GetData() );
	addrIter++;
	}
    LOG( AliHLTLog::kDebug, "ToleranceSupervisor::Set", "Commands Sent" )
	<< "Sent Pause commands to " << AliHLTLog::kDec << fSupervisedAddresses->size()
	<< " supervised clients." << ENDLOG;
    }

int main( int argc, char** argv )
    {
    AliHLTStdoutLogServer logOut;
    gLog.AddServer( logOut );
    int ret;

    char* usage1 = "Usage: ";
    char* usage2 = " -name <name> -address <local-address-URL> -interval <query-interval-millisec.> -supervise <supervised-address-URL> -target <target-address-URL> (-nostdout) (-V <verbosity) (-pauseclients) (-bridgemanager <bridge-manager-address-URL>)";
    char* errorStr = NULL;
    int errorOptNdx = 0;
    char* cErrP;
    vector<BCLAbstrAddressStruct*> supervisedURLs;
    vector<BCLAbstrAddressStruct*> targetURLs;
    vector<BCLAbstrAddressStruct*> bridgeURLs;
    BCLAbstrAddressStruct* addressStruct;
    char* listenAddress = NULL;
    bool nostdout = false;
    MLUCString name;
    bool nameSet = false;
    bool pauseClients = false;
    vector<BCLAbstrAddressStruct*> bridgeManagers;
    unsigned long interval = 0;
    int i = 1;

    while ( i<argc && !errorStr )
	{
	if ( !strcmp( argv[i], "-nostdout" ) )
	    {
	    nostdout = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing verbosity level specifier.";
		errorOptNdx = i;
		break;
		}
	    gLogLevel = strtoul( argv[i+1], &cErrP, 0 );
	    if ( *cErrP )
		{
		errorStr = "Error converting verbosity level specifier.";
		errorOptNdx = i;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-interval" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing query interval specifier.";
		errorOptNdx = i;
		break;
		}
	    interval = strtoul( argv[i+1], &cErrP, 0 );
	    if ( *cErrP )
		{
		errorStr = "Error converting query interval specifier.";
		errorOptNdx = i;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-name" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing name specifier.";
		errorOptNdx = i;
		break;
		}
	    name = argv[i+1];
	    nameSet = true;
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-bridgemanager" ) )
	    {
	    BCLAbstrAddressStruct* tmpAddr;
	    if ( argc <= i+1 )
		{
		errorStr = "Missing bridge manager address URL specifier.";
		errorOptNdx = i;
		break;
		}
	    ret = BCLDecodeRemoteURL( argv[i+1], tmpAddr );
	    if ( ret )
		{
		errorStr = "Error decoding bridge manager address URL.";
		errorOptNdx = i+1;
		break;
		}
	    bridgeManagers.insert( bridgeManagers.end(), tmpAddr );
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-address" ) )
	    {
	    if ( argc<=i+1 )
		{
		errorStr = "Missing local address URL option.";
		errorOptNdx = i;
		break;
		}
	    listenAddress = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-supervise" ) )
	    {
	    if ( argc<=i+1 )
		{
		errorStr = "Missing address URL to supervise option.";
		errorOptNdx = i;
		break;
		}
	    ret = BCLDecodeRemoteURL( argv[i+1], addressStruct );
	    if ( ret )
		{
		errorStr = "Error decoding address URL to supervise.";
		errorOptNdx = i;
		break;
		}
	    supervisedURLs.insert( supervisedURLs.end(), addressStruct );
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-target" ) )
	    {
	    if ( argc<=i+1 )
		{
		errorStr = "Missing target address URL option.";
		errorOptNdx = i;
		break;
		}
	    ret = BCLDecodeRemoteURL( argv[i+1], addressStruct );
	    if ( ret )
		{
		errorStr = "Error decoding target address URL.";
		errorOptNdx = i;
		break;
		}
	    targetURLs.insert( targetURLs.end(), addressStruct );
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-pauseclients" ) )
	    {
	    pauseClients = true;
	    i += 1;
	    continue;
	    }
	errorStr = "Unknown option";
	break;
	}

    if ( !errorStr )
	{
	if ( !nameSet )
	    errorStr = "Must specify the -name option.";
	else if ( supervisedURLs.size()<=0 )
	    errorStr = "Must specify at least one -supervise option.";
	else if ( targetURLs.size()<=0 )
	    errorStr = "Must specify at least one -target option.";
	}
    if ( errorStr )
	{
	LOG( AliHLTLog::kError, "ToleranceSupervisor", "Usage" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	LOG( AliHLTLog::kError, "ToleranceSupervisor", "Usage" )
	    << "Error: " << errorStr << ENDLOG;
	if ( errorOptNdx )
	    {
	    LOG( AliHLTLog::kError, "ToleranceSupervisor", "Usage" )
		<< "Offending command: " << argv[ errorOptNdx ] << "." << ENDLOG;
	    }
	return -1;
	}

    gLog.SetID( name.c_str() );
    AliHLTFileLogServer fileLog( name.c_str(), ".log", 3000000 );
    gLog.AddServer( fileLog );
    if ( nostdout )
	{
	gLog.DelServer( logOut );
	}

    AliHLTSCController controller;
    ToleranceSupervisor supervisor( &supervisedURLs, &targetURLs, interval, &controller, pauseClients ); // Check every 2s.
    gSupervisor = &supervisor;
    controller.SetReplyTimeout( 30000 ); // Reply timeout of 30s.
    ToleranceLAMHandler lamHandler( &(supervisor.fSignal) );
    BCLErrorLogCallback errorCallback;
    controller.AddLAMHandler( &lamHandler );
    supervisor.SetBridgeAddresses( bridgeManagers );

    ret = controller.Bind( listenAddress, &errorCallback );
    if ( ret )
	{
	LOG( AliHLTLog::kError, "SCTestServer", "Bind Error" )
	    << "Bind error on address URL '" << listenAddress
	    << "': " << strerror(ret) << " (" << AliHLTLog::kDec
	    << ret << ")." << ENDLOG;
	return ret;
	}
    
    signal( SIGQUIT, QuitSignalHandler );

    controller.Start();

    supervisor.Supervise();

    controller.Stop();
    return 0;
    }

void QuitSignalHandler( int )
    {
    if ( gSupervisor )
	gSupervisor->Quit();
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
