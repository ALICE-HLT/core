#!/usr/bin/env python

import sys, re, string, os;

if len(sys.argv) <2:
    print "Usage:",sys.argv[0]," <release version number>"
    print
    print "       Where <release vesion number> has three parts separated"
    print "       by dots, e.g. 0.7.0."
    sys.exit()

version = sys.argv[1]

# Extract major, minor and revision from the version number.
match = re.compile( "([0-9]+)\.([0-9]+)\.([0-9]+)([a-z]?)" ).search( version )

if ( match == None ):
    print "Usage:",sys.argv[0]," <release version number>"
    print
    print "       Where <release vesion number> has three parts separated"
    print "       by dots, e.g. 0.7.0."
    sys.exit()

major = match.group( 1 )
minor = match.group( 2 )
revision = match.group( 3 )
patchlevel = match.group( 4 )


# Build the cvs release tag.
#cvsversion = "release_"+major+"_"+minor+"_"+revision+patchlevel
#print "CVS Release: "+cvsversion


# Find the modules configured here.
f=open( "conf/Makefile.modules.include", "r" )
file = f.read()
match = re.compile( "MODULES=(.*)\n" ).search( file )

if match == None:
    print "Error: No modules description file \"conf/Makefile.modules.include\" found."
    sys.exit()

print "Setting version information to version "+version+" / library version "+major+"."+minor+"."+revision #+" / cvs version "+cvsversion
    
modulestr = match.group( 1 )
#print "Modules string: "+modulestr

modules = string.split( modulestr )
#print "Modules:",modules
modules.append( "ModuleTemplate" )

modulelist_string = ""

for m in modules:
    #print m
    outfile=open( m+"/conf/Makefile.version.conf", "w" )
    modulelist_string = modulelist_string+" "+m+"/conf/Makefile.version.conf"
    # Old version format.
    #outfile.write( "MAJORVERSION="+major+"\n" )
    #outfile.write( "VERSION=$(MAJORVERSION)."+minor+"."+revision+"\n" )
    # New version format
    outfile.write( "MAJORVERSION="+major+"\n" )
    outfile.write( "MINORVERSION="+minor+"\n" )
    outfile.write( "REVISION="+revision+"\n" )
    outfile.write( "PATCHLEVEL="+patchlevel+"\n" )
    outfile.close()

#print "cvs commit -m \"Updated makefile version identification to version "+version+".\""
#os.system(  "cvs commit -m \"Updated makefile version identification to version "+version+".\"" )
#print "cvs tag "+cvsversion
#os.system( "cvs tag "+cvsversion )

#print "svn commit -m \"Updated makefile version identifications to version "+version+".\""+modulelist_string
os.system( "svn commit -m \"Updated makefile version identifications to version "+version+".\""+modulelist_string )
#print "../utils/MakeSVNTag.sh "+version
os.system( "../utils/MakeSVNTag.sh "+version )
