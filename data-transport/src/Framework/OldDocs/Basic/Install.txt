
Alice Level 3 Framework Code - Prototype 4

Installation

This framework depends on three other libraries, the MLUC, BCL, and psi
libraries, that you have to install first, MLUC has to be installed before BCL. 
For these libraries please see their corresponding documentation files.

For configuration of the package you have to enter a number of settings to the
conf/Makefile.conf configuration file. Note that you should only have to change the
settings in the top-level conf/Makefile.conf, not in the other conf sub-directories, 
e.g. L3SI/conf/Makefile.conf.

First of all you have to decide wether you want to build static and/or shared versions
of the framework libraries. Depending on this you have to define the "BUILD_STATIC_LIBS"
and "BUILD_SHARED_LIBS" Makefile variables in this file. Note that this is independent to
what you define the variables, if you define them, the corresponding version of the 
libraries will be built.

Next you will have to set the "TARGET" Makefile variable to the platform you want to
compile for. This is a combination of OS and CPU type and determines where object and 
library files will be placed and also where the MLUC and BCL libraries will be searched.
This is always in a "lib/$(TARGET)" subdirectory of the top-level directory of this 
framework and the two libraries.

Next you have to set the absolute paths to the top-level directories of the MLUC
and BCL libraries. This is done in the variables "MLUCTOPDIR" and "BCLTOPDIR".
You do not need to set or use the "KIPDIR" variable as in the sample configuration,
this is just for convenience. You can however use the "TOPDIR" variable which 
references this framework's top-level directory and specify a path relative to this
directory.

Lastly you have to set the path to the top-level directory of the psi package.
This is the directory under which the "include" and "src" subdirectories are located.


Once you think you have correctly set the above variables you can start make in the 
top-level directory of the framework. This should run for some time and produce a 
number of component programs and libraries.

Some sample programs showing how to build programs according to your own needs can
be found in the subdirectories located under samples/test. Short descriptions on 
these programs are in the doc/SamplePrograms.txt file.

Some short documentation on the component programs can be found in the 
doc/Components.txt file.

Reference to additional documentation can be found in the file 
doc/AdditionalDocumentation.txt.

