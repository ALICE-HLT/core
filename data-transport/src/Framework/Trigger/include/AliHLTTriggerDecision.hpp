#ifndef _ALIHLTTRIGGERDECISION_HPP_
#define _ALIHLTTRIGGERDECISION_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTriggerDecision.h"
#include "AliHLTEventDoneData.h"
#include "AliHLTEventDoneData.hpp"
#include "AliHLTLog.hpp"
#include <vector>



inline void AliHLTLogTriggerDecisionArray( AliEventID_t eventID, const AliHLTTriggerDecisionArray tdb, AliHLTLog::TLogLevel logLevel, const char* origin, const char* keywords )
    {
    unsigned i;
    char tmp1[5], tmp2[5], tmp3[9], tmp4[9];
    char c;
    AliHLTEventDataType   dataType;
    AliHLTEventDataOrigin dataOrigin;
    AliUInt32_t dataSpec;
    dataType.fID = ((AliUInt64_t)tdb[0]) | ( ((AliUInt64_t)tdb[1]) << 32 );
    dataOrigin.fID = tdb[2];
    dataSpec = tdb[3];

    for ( i = 0; i < 4; i++ )
	{
	if ( dataOrigin.fDescr[i] >= 32 )
	    c = dataOrigin.fDescr[i];
	else
	    c = '?';
	tmp1[i] = c;
	tmp2[3-i] = c;
	}
    tmp1[4] = 0;
    tmp2[4] = 0;
    for ( i = 0; i < 8; i++ )
	{
	if ( dataType.fDescr[i] >= 32 )
	    c = dataType.fDescr[i];
	else
	    c = '?';
	tmp3[i] = c;
	tmp4[7-i] = c;
	}
    tmp3[8] = 0;
    tmp4[8] = 0;
    LOG( logLevel, origin, keywords )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << ") Trigger Decision Block: Data origin: " 
	<< tmp1 <<  "/ " << tmp2 <<  "/ 0x" << AliHLTLog::kHex << dataOrigin.fID
	<< " - Data Type: "
	<< tmp3 << " / " << tmp4 << " / 0x" << dataType.fID
	<< " - Data Specification: 0x" << AliHLTLog::kHex << dataSpec << " (" << AliHLTLog::kDec
	<< dataSpec << ")." << ENDLOG;
    }


AliHLTEventDoneData* AliHLTMakeEventDoneData( vector<AliHLTTriggerDecisionArray> tdbs, bool readoutOrMonitor=true );




inline AliUInt16_t AliHLTTriggerDecisionTPCGetMinSliceNr( AliUInt32_t dataSpec )
    {
    return (AliUInt16_t)( (dataSpec & 0x00FF0000) >> 16 );
    }

inline AliUInt16_t AliHLTTriggerDecisionTPCGetMaxSliceNr( AliUInt32_t dataSpec )
    {
    return (AliUInt16_t)( (dataSpec & 0xFF000000) >> 24 );
    }

inline AliUInt16_t AliHLTTriggerDecisionTPCGetMinPatchNr( AliUInt32_t dataSpec )
    {
    return (AliUInt16_t)( dataSpec & 0x000000FF );
    }

inline AliUInt16_t AliHLTTriggerDecisionTPCGetMaxPatchNr( AliUInt32_t dataSpec )
    {
    return (AliUInt16_t)( (dataSpec & 0x0000FF00) >> 8 );
    }

inline AliUInt32_t AliHLTTriggerDecisionMakeTPCDataSpecification( AliUInt16_t minSliceNr, 
								  AliUInt16_t maxSliceNr,
								  AliUInt16_t minPatchNr,
								  AliUInt16_t maxPatchNr )
    {
    return ( (maxSliceNr & 0xFF) << 24 ) | ( (minSliceNr & 0xFF) << 16 ) | ( (maxPatchNr & 0xFF) << 8 ) | (minPatchNr & 0xFF);
    }

#if 0
Different scheme now used by Dimuons
inline AliUInt16_t AliHLTTriggerDecisionDiMuonGetMinDDLNr( AliUInt32_t dataSpec )
    {
    return (AliUInt16_t)( dataSpec & 0x000000FF );
    }

inline AliUInt16_t AliHLTTriggerDecisionDiMuonGetMaxDDLNr( AliUInt32_t dataSpec )
    {
    return (AliUInt16_t)( (dataSpec & 0x0000FF00) >> 8 );
    }

inline AliUInt32_t AliHLTTriggerDecisionMakeDiMuonDataSpecification( AliUInt16_t minDDLNr, 
								  AliUInt16_t maxDDLNr )
    {
    return ( (maxDDLNr & 0xFF) << 8 ) | (minDDLNr & 0xFF);
    }

#endif

#if 0
Different encoding/storage scheme now used for Event Done Data (cf. Base/include/AliHLTEventDoneData.hpp
inline bool AliHLTHasValidTriggerDecisionBlocks( const AliHLTEventDoneData* edd )
    {
    if ( edd && !( edd->fDataWordCount*sizeof(AliUInt32_t)%sizeof(AliHLTTriggerDecisionBlock) ) )
	return true;
    return false;
    }

inline AliUInt32_t AliHLTGetTriggerDecisionBlockCount( const AliHLTEventDoneData* edd )
    {
    if ( !edd )
	return 0;
    return (edd->fDataWordCount*sizeof(AliUInt32_t)) / sizeof(AliHLTTriggerDecisionBlock);
    }

inline void AliHLTLogTriggerDecisionBlock( AliEventID_t eventID, const AliHLTTriggerDecisionBlock* tdb, AliHLTLog::TLogLevel logLevel, const char* origin, const char* keywords )
    {
    unsigned i;
    char tmp1[5], tmp2[5], tmp3[9], tmp4[9];
    char c;
    for ( i = 0; i < 4; i++ )
	{
	if ( tdb->fDataOrigin.fDescr[i] >= 32 )
	    c = tdb->fDataOrigin.fDescr[i];
	else
	    c = '?';
	tmp1[i] = c;
	tmp2[3-i] = c;
	}
    tmp1[4] = 0;
    tmp2[4] = 0;
    for ( i = 0; i < 8; i++ )
	{
	if ( tdb->fDataType.fDescr[i] >= 32 )
	    c = tdb->fDataType.fDescr[i];
	else
	    c = '?';
	tmp3[i] = c;
	tmp4[7-i] = c;
	}
    tmp3[8] = 0;
    tmp4[8] = 0;
    LOG( logLevel, origin, keywords )
	<< "Event 0x" << AliHLTLog::kHex << eventID << " (" << AliHLTLog::kDec << eventID << ") Trigger Decision Block: Data origin: " 
	<< tmp1 <<  "/ " << tmp2 <<  "/ 0x" << AliHLTLog::kHex << tdb->fDataOrigin.fID
	<< " - Data Type: "
	<< tmp3 << " / " << tmp4 << " / 0x" << tdb->fDataType.fID
	<< " - Data Specification: 0x" << AliHLTLog::kHex << tdb->fDataSpecification << " (" << AliHLTLog::kDec
	<< tdb->fDataSpecification << ")." << ENDLOG;
    }


AliHLTEventDoneData* AliHLTMakeEventDoneData( vector<AliHLTTriggerDecisionBlock*> tdbs );

const AliHLTTriggerDecisionBlock* AliHLTGetTriggerDecisionBlock( const AliHLTEventDoneData*, unsigned blockIndex );
void AliHLTGetTriggerDecisionBlocks( const AliHLTEventDoneData*, vector<const AliHLTTriggerDecisionBlock*>& tdbs );
#endif



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTTRIGGERDECISION_HPP_
