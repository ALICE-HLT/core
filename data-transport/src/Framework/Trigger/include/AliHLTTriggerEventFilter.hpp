#ifndef _ALIHLTTRIGGEREVENTFILTER_HPP_
#define _ALIHLTTRIGGEREVENTFILTER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTriggerDecision.h"
#include "AliHLTTriggerDecision.hpp"
#include "AliHLTSubEventDataDescriptor.h"
#include "AliHLTSubEventDescriptor.hpp"

class AliHLTTriggerEventFilter
    {
    public:

	//typedef bool (*DataSpecificationMatchFunction)( const AliHLTSubEventDataBlockDescriptor* edb, const AliHLTTriggerDecisionBlock* tdb );
	typedef bool (*DataSpecificationMatchFunction)( AliHLTEventDataType dataType, AliHLTEventDataOrigin dataOrigin, AliUInt32_t dataSpec, 
							const AliHLTTriggerDecisionArray tdb );

	AliHLTTriggerEventFilter()
		{
		fDataSpecMatchFunc = NULL;
		}
	virtual ~AliHLTTriggerEventFilter() {}

	void SetDataSpecificationMatchFunction( DataSpecificationMatchFunction matchFunc )
		{
		fDataSpecMatchFunc = matchFunc;
		}

	bool FilterEventDescriptorMatches( AliHLTEventDoneDataCommands const cmd, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventDoneData* edd, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks )
		{
		return FilterEventDescriptor( cmd, sedd, edd, outputBlocks, true );
		}
	bool FilterEventDescriptorNonMatches( AliHLTEventDoneDataCommands const cmd, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventDoneData* edd, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks )
		{
		return FilterEventDescriptor( cmd, sedd, edd, outputBlocks, false );
		}
	bool FilterEventDescriptor( AliHLTEventDoneDataCommands const cmd, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventDoneData* edd, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, bool matchOrNonMatch );

#if 0
Postponed
bool FilterEventDescriptor( AliHLTEventDoneDataCommands const cmd, AliHLTSubEventDataDescriptor* sedd, vector<const AliHLTTriggerDecisionBlock*> tdbs, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, bool matchOrNonMatch );
#endif




	static bool DataSpecificationComparer( AliHLTEventDataType, AliHLTEventDataOrigin, AliUInt32_t dataSpec, 
					       const AliHLTTriggerDecisionArray tdb )
		{
		return dataSpec==tdb[3];
		}


	static bool AliceDataSpecificationMatcher( AliHLTEventDataType, AliHLTEventDataOrigin dataOrigin, AliUInt32_t dataSpecification, 
						   const AliHLTTriggerDecisionArray tdb )
		{
		if ( tdb[3] == ~(AliUInt32_t)0 )
		    return true;
		if ( dataOrigin.fID == TPC_DATAORIGIN )
		    {
		    AliUInt16_t edbMinPatch, edbMaxPatch, edbMinSlice, edbMaxSlice;
		    AliUInt16_t tdbMinPatch, tdbMaxPatch, tdbMinSlice, tdbMaxSlice;

		    edbMinPatch = AliHLTTriggerDecisionTPCGetMinPatchNr( dataSpecification );
		    edbMaxPatch = AliHLTTriggerDecisionTPCGetMaxPatchNr( dataSpecification );
		    edbMinSlice = AliHLTTriggerDecisionTPCGetMinSliceNr( dataSpecification );
		    edbMaxSlice = AliHLTTriggerDecisionTPCGetMaxSliceNr( dataSpecification );

		    tdbMinPatch = AliHLTTriggerDecisionTPCGetMinPatchNr( tdb[3] );
		    tdbMaxPatch = AliHLTTriggerDecisionTPCGetMaxPatchNr( tdb[3] );
		    tdbMinSlice = AliHLTTriggerDecisionTPCGetMinSliceNr( tdb[3] );
		    tdbMaxSlice = AliHLTTriggerDecisionTPCGetMaxSliceNr( tdb[3] );

		    if ( ((edbMinPatch>=tdbMinPatch && edbMinPatch<=tdbMaxPatch) ||
			  (edbMaxPatch>=tdbMinPatch && edbMaxPatch<=tdbMaxPatch) ||
			  tdbMinPatch==0xFF || tdbMaxPatch==0xFF ) &&
			 ((edbMinSlice>=tdbMinSlice && edbMinSlice<=tdbMaxSlice) ||
			  (edbMaxSlice>=tdbMinSlice && edbMaxSlice<=tdbMaxSlice) ||
			  tdbMinSlice==0xFF || tdbMaxSlice==0xFF) )
			return true;
		    }
		else 
		    {
		    return (bool)( dataSpecification & tdb[3] );
		    }
#if 0
if ( dataOrigin.fID == DIMUON_DATAORIGIN )
		    {
		    AliUInt16_t edbMinDDLNr, edbMaxDDLNr;
		    AliUInt16_t tdbMinDDLNr, tdbMaxDDLNr;
		    
		    edbMinDDLNr = AliHLTTriggerDecisionDiMuonGetMinDDLNr( dataSpecification );
		    edbMaxDDLNr = AliHLTTriggerDecisionDiMuonGetMaxDDLNr( dataSpecification );

		    tdbMinDDLNr = AliHLTTriggerDecisionDiMuonGetMinDDLNr( tdb->fDataSpecification );
		    tdbMaxDDLNr = AliHLTTriggerDecisionDiMuonGetMaxDDLNr( tdb->fDataSpecification );
		    
		    if ( ((edbMinDDLNr>=tdbMinDDLNr && edbMinDDLNr<=tdbMaxDDLNr) ||
			  (edbMaxDDLNr>=tdbMinDDLNr && edbMaxDDLNr<=tdbMaxDDLNr) ||
			  tdbMinDDLNr==0xFF || tdbMaxDDLNr==0xFF) )
			return true;
		    }
#endif
		return false;
		}





#if 0
Different scheme now implemented
	bool FilterEventDescriptorMatches( AliHLTSubEventDataDescriptor* sedd, const AliHLTEventDoneData* edd )
		{
		if ( !AliHLTHasValidTriggerDecisionBlocks( edd ) )
		    return false;
		vector<const AliHLTTriggerDecisionBlock*> tdbs;
		AliHLTGetTriggerDecisionBlocks( edd, tdbs );
		return FilterEventDescriptor( sedd, tdbs, true );
		}
	bool FilterEventDescriptorMatches( AliHLTSubEventDataDescriptor* sedd, vector<const AliHLTTriggerDecisionBlock*> tdbs )
		{
		return FilterEventDescriptor( sedd, tdbs, true );
		}
	bool FilterEventDescriptorNonMatches( AliHLTSubEventDataDescriptor* sedd, const AliHLTEventDoneData* edd )
		{
		if ( !AliHLTHasValidTriggerDecisionBlocks( edd ) )
		    return false;
		vector<const AliHLTTriggerDecisionBlock*> tdbs;
		AliHLTGetTriggerDecisionBlocks( edd, tdbs );
		return FilterEventDescriptor( sedd, tdbs, false );
		}
	bool FilterEventDescriptorNonMatches( AliHLTSubEventDataDescriptor* sedd, vector<const AliHLTTriggerDecisionBlock*> tdbs )
		{
		return FilterEventDescriptor( sedd, tdbs, false );
		}

	bool FilterEventDescriptorMatches( AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, 
					   vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, 
					   const AliHLTEventDoneData* edd )
		{
		if ( !AliHLTHasValidTriggerDecisionBlocks( edd ) )
		    return false;
		vector<const AliHLTTriggerDecisionBlock*> tdbs;
		AliHLTGetTriggerDecisionBlocks( edd, tdbs );
		return FilterEventDescriptor( blockCnt, blocks, outputBlocks, tdbs, true );
		}
	bool FilterEventDescriptorMatches( AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, 
					   vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, 
					   vector<const AliHLTTriggerDecisionBlock*> tdbs )
		{
		return FilterEventDescriptor( blockCnt, blocks, outputBlocks, tdbs, true );
		}
	bool FilterEventDescriptorNonMatches( AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, 
					      vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, 
					      const AliHLTEventDoneData* edd )
		{
		if ( !AliHLTHasValidTriggerDecisionBlocks( edd ) )
		    return false;
		vector<const AliHLTTriggerDecisionBlock*> tdbs;
		AliHLTGetTriggerDecisionBlocks( edd, tdbs );
		return FilterEventDescriptor( blockCnt, blocks, outputBlocks, tdbs, false );
		}
	bool FilterEventDescriptorNonMatches( AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, 
					      vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, 
					      vector<const AliHLTTriggerDecisionBlock*> tdbs )
		{
		return FilterEventDescriptor( blockCnt, blocks, outputBlocks, tdbs, false );
		}

	bool FilterEventDescriptor( AliHLTSubEventDataDescriptor* sedd, vector<const AliHLTTriggerDecisionBlock*> tdbs, bool matchOrNonMatch );
	bool FilterEventDescriptor( AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, 
				    vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, 
				    vector<const AliHLTTriggerDecisionBlock*> tdbs, bool matchOrNonMatch );
#endif

    protected:

	DataSpecificationMatchFunction fDataSpecMatchFunc;

    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTTRIGGEREVENTFILTER_HPP_
