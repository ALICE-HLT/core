#ifndef _ALIHLTTRIGGERDECISION_H_
#define _ALIHLTTRIGGERDECISION_H_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTypes.h"
#include "AliEventID.h"
#include "AliHLTEventDataType.h"
#include "AliHLTEventDataOrigin.h"


#ifdef __cplusplus
extern "C" {
#endif

#if 0
    struct AliHLTTriggerDecisionBlock
	{
	    AliHLTEventDataType   fDataType;
	    AliHLTEventDataOrigin fDataOrigin;
	    AliUInt32_t fDataSpecification;
	};

    typedef struct AliHLTTriggerDecisionBlock AliHLTTriggerDecisionBlock;
#endif

    typedef AliUInt32_t AliHLTTriggerDecisionArray[4];

#if 0
    struct AliHLTTriggerDecisionArray
	{
	    AliUInt32_t fWords[4];
	    // Word 0: Low word of data type ID
	    // Word 1: High word of data type ID
	    // Word 2: data origin ID
	    // Word 3: data spec
	};
#endif


#ifdef __cplusplus
}
#endif




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif /* _ALIHLTTRIGGERDECISION_H_ */
