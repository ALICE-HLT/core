#ifndef _ALIHLTHLTEVENTTRIGGERDATA_HPP_
#define _ALIHLTHLTEVENTTRIGGERDATA_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTEventTriggerStruct.h"
#include "AliHLTBlockAttributes.h"
#include "AliHLTAlignment.h"
#include "AliHLTReadoutList.hpp"
#include "AliHLTConfig.hpp"
#include <cstring>

class MLUCString;

const AliUInt32_t gkDefaultCommonHeaderVersion = 3;
const AliUInt32_t gkDefaultCommonHeaderWrdCnt = 10;

struct AliHLTHLTEventTriggerData
    {
	AliHLTEventTriggerStruct fETS;
	AliUInt8_t fAttributes[kAliHLTBlockDAttributeCount]; // Assignment identical to data blocks;
	AliUInt64_t fHLTStatus; // Bit field
	AliUInt32_t fCommonHeaderWordCnt;
	AliUInt32_t fCommonHeader[gkDefaultCommonHeaderWrdCnt]; // Number of 32 bit words
	AliUInt32_t fReadoutList[READOUTLIST_MAX_WORDCOUNT+1]; // First word holds number of words (excluding first word)

	AliHLTHLTEventTriggerData()
		{
		Fill();
		ResetCachedObject();
		}
	void Fill()
		{
		fETS.fHeader.fLength = sizeof(AliHLTHLTEventTriggerData);
		fETS.fHeader.fType.fID = ALIL3EVENTTRIGGERSTRUCT_TYPE;
		fETS.fHeader.fSubType.fID = 0;
		fETS.fHeader.fVersion = gkDefaultCommonHeaderVersion;
		fETS.fDataWordCount = (sizeof(AliHLTHLTEventTriggerData)-sizeof(AliHLTEventTriggerStruct)) / sizeof(fETS.fDataWords[0]);
		fAttributes[kAliHLTBlockDByteOrderAttributeIndex] = kAliHLTNativeByteOrder;
		fAttributes[kAliHLTBlockD64BitAlignmentAttributeIndex] = AliHLTDetermineUInt64Alignment();
		fAttributes[kAliHLTBlockD32BitAlignmentAttributeIndex] = AliHLTDetermineUInt32Alignment();
		fAttributes[kAliHLTBlockD16BitAlignmentAttributeIndex] = AliHLTDetermineUInt16Alignment();
		fAttributes[kAliHLTBlockD8BitAlignmentAttributeIndex] = AliHLTDetermineUInt8Alignment();
		fAttributes[kAliHLTBlockDFloatAlignmentAttributeIndex] = AliHLTDetermineDoubleAlignment();
		fAttributes[kAliHLTBlockDDoubleAlignmentAttributeIndex] = AliHLTDetermineFloatAlignment();
		fCommonHeaderWordCnt = gkDefaultCommonHeaderWrdCnt;
		}
	void ResetCachedObject()
		{
		fHLTStatus = 0;
		memset( fCommonHeader, 0, sizeof(fCommonHeader[0])*fCommonHeaderWordCnt );
		AliHLTReadoutList rdLst( gReadoutListVersion, fReadoutList );
		rdLst.Reset();
		}

      void SetCommonHeader(void const* buf){
	AliUInt32_t* cdh = (AliUInt32_t*)buf;
	AliUInt32_t version = ( cdh[1] >> 24 ) & 0xFF;
	fCommonHeaderWordCnt=(version < 3 ? 8 : 10);
	memcpy( fCommonHeader, buf, sizeof(fCommonHeader[0])*fCommonHeaderWordCnt );
      }


    };


#define HLTEVENTRIGGERDATA_DATAID        (((AliUInt64_t)'HLTT')<<32 | 'RGDT')


void AliHLTHLTMergeEventTriggerData( AliHLTHLTEventTriggerData* dest, AliHLTHLTEventTriggerData const* mergeIn );

MLUCString AliHLTHLEventTriggerData2String( const AliHLTEventTriggerStruct* ets );



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTHLTEVENTTRIGGERDATA_HPP_
