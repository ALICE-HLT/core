#ifndef _ALIHLTTRIGGERSTACKMACHINE_HPP_
#define _ALIHLTTRIGGERSTACKMACHINE_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTSimpleStackMachine.hpp"

class AliHLTHLTEventTriggerData;

class AliHLTTriggerStackMachine: public AliHLTSimpleStackMachine
    {
    public:


	class TError: public MLUCSimpleStackMachine::TError
	    {
	    public:
		enum TCode { kNoHLTTriggerData=MLUCSimpleStackMachine::TError::kLastErrorCode+1, kWrongHLTTriggerDataSize };
		TError()
			{ }
		TError( TCode code, const char* descr ):
		    MLUCSimpleStackMachine::TError( (MLUCSimpleStackMachine::TError::TCode)code, descr )
			{ }
	    protected:
	    };


	AliHLTTriggerStackMachine();
	virtual ~AliHLTTriggerStackMachine();

      enum TInstructions { kPUSHCDHL1TRIGGERTYPE = 0x80, kPUSHCDHEVENTID, kPUSHCDHPARTSUBDET, kPUSHCDHBLOCKATTR, kPUSHCDHMINIEVENTID, kPUSHCDHSTATUSERROR, kPUSHCDHTRIGGERCLASSESLOW, kPUSHCDHTRIGGERCLASSESHIGH, kPUSHCDHROI };



    protected:

	virtual MLUCSimpleStackMachine::TError ExecNextStatement();

    private:
    };


bool StackMachineCompareEventTrigger( void* param, const AliHLTEventTriggerStruct* ettSubscriber, const AliHLTEventTriggerStruct* ettEvent );


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTTRIGGERSTACKMACHINE_HPP_
