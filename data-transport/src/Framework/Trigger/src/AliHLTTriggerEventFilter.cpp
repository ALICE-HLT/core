/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTriggerEventFilter.hpp"
#include "AliHLTEventDoneData.hpp"
#include "AliHLTSubEventDescriptor.hpp"
#include "AliHLTLog.hpp"


bool AliHLTTriggerEventFilter::FilterEventDescriptor( AliHLTEventDoneDataCommands const cmd, const AliHLTSubEventDataDescriptor* sedd, const AliHLTEventDoneData* edd, vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, bool matchOrNonMatch )
    {
    outputBlocks.clear();
    if ( !sedd || !edd || !fDataSpecMatchFunc )
	return false;
    
    unsigned long n = sedd->fDataBlockCount;
    outputBlocks.reserve( n );


#if 1

    char typeTmp[9];
    char originTmp[5];
    if ( n==0 )
	{
	LOG( AliHLTLog::kDebug, "AliHLTTriggerEventFilter::FilterEventDescriptor", "Input Data Block" )
	    << "No input data blocks" << ENDLOG;
	}
    for ( unsigned long ii = 0; ii < n; ii++ )
	{
	bool forward = false;
	if ( DOLOG(AliHLTLog::kDebug) )
	    {
	    for ( unsigned nn=0; nn<8; nn++ )
		typeTmp[nn] = sedd->fDataBlocks[ii].fDataType.fDescr[7-nn];
	    typeTmp[8] = 0;
	    for ( unsigned nn=0; nn<4; nn++ )
		originTmp[nn] = sedd->fDataBlocks[ii].fDataOrigin.fDescr[3-nn];
	    originTmp[4] = 0;
	    LOG( AliHLTLog::kDebug, "AliHLTTriggerEventFilter::FilterEventDescriptor", "Input Data Block" )
		<< "Input data block " << AliHLTLog::kDec << ii << ": Data type: 0x" << AliHLTLog::kHex << sedd->fDataBlocks[ii].fDataType.fID << " - '" << typeTmp
		<< "' - data origin: 0x" << sedd->fDataBlocks[ii].fDataOrigin.fID << " - '" << originTmp
		<< "' - data spec: 0x" << sedd->fDataBlocks[ii].fDataSpecification << " - " << AliHLTLog::kDec << sedd->fDataBlocks[ii].fDataSpecification
		<< ENDLOG;
	    }
	if ( (sedd->fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_FULLPASSTHROUGH) || (sedd->fDataBlocks[ii].fStatusFlags & ALIHLTSUBEVENTDATADESCRIPTOR_PASSTHROUGH) )
	    forward = true;
	else
	    {
	    AliHLTEventDoneDataProcessor eddp( edd );
	    AliHLTEventDoneDataProcessor::TStatus status;
	    AliUInt32_t const * nextCmd;
	    unsigned wordCount;
	    status = eddp.NextCommand( nextCmd, wordCount );
	    while ( status == AliHLTEventDoneDataProcessor::kOk )
		{
		if ( nextCmd[0] == (AliUInt32_t)cmd )
		    {
		    if ( wordCount<2 || wordCount!=2+nextCmd[1]*sizeof(AliHLTTriggerDecisionArray)/sizeof(AliUInt32_t) )
			return false;
		    for ( unsigned long nn = 0; nn<nextCmd[1]; nn++ )
			{
			AliHLTEventDataType   dataType;
			AliHLTEventDataOrigin dataOrigin;
			AliUInt32_t dataSpec;
			dataType.fID = ((AliUInt64_t)nextCmd[2+nn*sizeof(AliHLTTriggerDecisionArray)/sizeof(AliUInt32_t)+0]) | ( ((AliUInt64_t)nextCmd[2+nn*sizeof(AliHLTTriggerDecisionArray)/sizeof(AliUInt32_t)+1]) << 32 );
			dataOrigin.fID = nextCmd[2+nn*sizeof(AliHLTTriggerDecisionArray)/sizeof(AliUInt32_t)+2];
			dataSpec = nextCmd[2+nn*sizeof(AliHLTTriggerDecisionArray)/sizeof(AliUInt32_t)+3];
			if ( DOLOG(AliHLTLog::kDebug) )
			    {
			    for ( unsigned ii=0; ii<8; ii++ )
				typeTmp[ii] = dataType.fDescr[7-ii];
			    typeTmp[8] = 0;
			    for ( unsigned ii=0; ii<4; ii++ )
				originTmp[ii] = dataOrigin.fDescr[3-ii];
			    originTmp[4] = 0;
			    LOG( AliHLTLog::kDebug, "AliHLTTriggerEventFilter::FilterEventDescriptor", "Trigger Data" )
				<< "Trigger data: Data type: 0x" << AliHLTLog::kHex << dataType.fID << " - '" << typeTmp
				<< "' - data origin: 0x" << dataOrigin.fID << " - '" << originTmp
				<< "' - data spec: 0x" << dataSpec << " - " << AliHLTLog::kDec << dataSpec
				<< ENDLOG;
			    }
			if ( ((dataType.fID == ALL_DATAID ||
			       sedd->fDataBlocks[ii].fDataType.fID == ALL_DATAID ||
			       dataType.fID == sedd->fDataBlocks[ii].fDataType.fID) &&
			      (dataOrigin.fID == ALL_DATAORIGIN ||
			       sedd->fDataBlocks[ii].fDataOrigin.fID == ALL_DATAORIGIN ||
			       dataOrigin.fID == sedd->fDataBlocks[ii].fDataOrigin.fID) &&
			      fDataSpecMatchFunc( sedd->fDataBlocks[ii].fDataType, sedd->fDataBlocks[ii].fDataOrigin,
						  sedd->fDataBlocks[ii].fDataSpecification, nextCmd+2+nn*sizeof(AliHLTTriggerDecisionArray)/sizeof(AliUInt32_t) )) == matchOrNonMatch )
			    forward = true;
			}
		    }
		status = eddp.NextCommand( nextCmd, wordCount );
		}
	    if ( status != AliHLTEventDoneDataProcessor::kDone )
		return false;
	    }
	if ( forward )
	    {
	    AliHLTSubEventDescriptor::BlockData bd;
	    AliHLTSubEventDescriptor::Copy( sedd->fDataBlocks[ii], bd );
	    outputBlocks.push_back( bd );
	    }
	}
    if ( DOLOG(AliHLTLog::kDebug) )
	{
	vector<AliHLTSubEventDescriptor::BlockData>::iterator outIter, outEnd;
	outIter = outputBlocks.begin();
	outEnd = outputBlocks.end();
	unsigned long cnt=0;
	while ( outIter != outEnd )
	    {
	    for ( unsigned ii=0; ii<8; ii++ )
		typeTmp[ii] = outIter->fDataType.fDescr[7-ii];
	    typeTmp[8] = 0;
	    for ( unsigned ii=0; ii<4; ii++ )
		originTmp[ii] = outIter->fDataOrigin.fDescr[3-ii];
	    originTmp[4] = 0;
	    LOG( AliHLTLog::kDebug, "AliHLTTriggerEventFilter::FilterEventDescriptor", "Output Data Block" )
		<< "output data block " << AliHLTLog::kDec << cnt << ": Data type: 0x" << AliHLTLog::kHex << outIter->fDataType.fID << " - '" << typeTmp
		<< "' - data origin: 0x" << outIter->fDataOrigin.fID << " - '" << originTmp
		<< "' - data spec: 0x" << outIter->fDataSpecification << " - " << AliHLTLog::kDec << outIter->fDataSpecification
		<< ENDLOG;
	    ++cnt;
	    ++outIter;
	    }
	if ( outputBlocks.size()==0 )
	    {
	    LOG( AliHLTLog::kDebug, "AliHLTTriggerEventFilter::FilterEventDescriptor", "Output Data Block" )
		<< "No output data blocks" << ENDLOG;
	    }
	}
    return true;

#else

    AliHLTEventDoneDataProcessor eddp( edd );
    AliHLTEventDoneDataProcessor::TStatus status;
    AliUInt32_t const * nextCmd;
    unsigned wordCount;
    status = eddp.NextCommand( nextCmd, wordCount );
    while ( status == AliHLTEventDoneDataProcessor::kOk )
	{
	if ( nextCmd[0] == (AliUInt32_t)cmd )
	    {
	    if ( wordCount<2 && wordCount!=2+nextCmd[1]*sizeof(AliHLTTriggerDecisionArray)/sizeof(AliUInt32_t) )
		return false;
	    for ( unsigned long nn = 0; nn<nextCmd[1]; nn++ )
		{
		AliHLTEventDataType   dataType;
		AliHLTEventDataOrigin dataOrigin;
		AliUInt32_t dataSpec;
		dataType.fID = ((AliUInt64_t)nextCmd[2+nn*sizeof(AliHLTTriggerDecisionArray)/sizeof(AliUInt32_t)+0]) | ( ((AliUInt64_t)nextCmd[2+nn*sizeof(AliHLTTriggerDecisionArray)/sizeof(AliUInt32_t)+1]) << 32 );
		dataOrigin.fID = nextCmd[2+nn*sizeof(AliHLTTriggerDecisionArray)/sizeof(AliUInt32_t)+2];
		dataSpec = nextCmd[2+nn*sizeof(AliHLTTriggerDecisionArray)/sizeof(AliUInt32_t)+3];
		char typeTmp[9];
		memset( typeTmp, 0, 9 );
		memcpy( typeTmp, dataType.fDescr, 8 );
		char originTmp[5];
		memset( originTmp, 0, 5 );
		memcpy( originTmp, dataOrigin.fDescr, 4 );
		LOG( AliHLTLog::kDebug, "AliHLTTriggerEventFilter::FilterEventDescriptor", "Trigger Data" )
		    << "Trigger data: Data type: 0x" << AliHLTLog::kHex << dataType.fID << " - " << typeTmp
		    << " - data origin: 0x" << dataOrigin.fID << " - " << originTmp
		    << " - data spec: 0x" << dataSpec << " - " << AliHLTLog::kDec << dataSpec
		    << ENDLOG;
		for ( unsigned long ii = 0; ii < n; ii++ )
		    {
		    memset( typeTmp, 0, 9 );
		    memcpy( typeTmp, sedd->fDataBlocks[ii].fDataType.fDescr, 8 );
		    memset( originTmp, 0, 5 );
		    memcpy( originTmp, sedd->fDataBlocks[ii].fDataOrigin.fDescr, 4 );
		    LOG( AliHLTLog::kDebug, "AliHLTTriggerEventFilter::FilterEventDescriptor", "Data Block" )
			<< "Data block " << AliHLTLog::kDec << ii << ": Data type: 0x" << AliHLTLog::kHex << sedd->fDataBlocks[ii].fDataType.fID << " - " << typeTmp
			<< " - data origin: 0x" << sedd->fDataBlocks[ii].fDataOrigin.fID << " - " << originTmp
			<< " - data spec: 0x" << sedd->fDataBlocks[ii].fDataSpecification << " - " << AliHLTLog::kDec << sedd->fDataBlocks[ii].fDataSpecification
			<< ENDLOG;
		    if ( ((dataType.fID == ALL_DATAID ||
			   sedd->fDataBlocks[ii].fDataType.fID == ALL_DATAID ||
			   dataType.fID == sedd->fDataBlocks[ii].fDataType.fID) &&
			  (dataOrigin.fID == ALL_DATAORIGIN ||
			   sedd->fDataBlocks[ii].fDataOrigin.fID == ALL_DATAORIGIN ||
			   dataOrigin.fID == sedd->fDataBlocks[ii].fDataOrigin.fID) &&
			  fDataSpecMatchFunc( sedd->fDataBlocks[ii].fDataType, sedd->fDataBlocks[ii].fDataOrigin,
					      sedd->fDataBlocks[ii].fDataSpecification, nextCmd+1+nn*sizeof(AliHLTTriggerDecisionArray)/sizeof(AliUInt32_t) )) == matchOrNonMatch )
			{
			AliHLTSubEventDescriptor::BlockData bd;
			AliHLTSubEventDescriptor::Copy( sedd->fDataBlocks[ii], bd );
			outputBlocks.push_back( bd );
			}
			
		    }
		}
	    
	    }
	status = eddp.NextCommand( nextCmd, wordCount );
	}
    return status == AliHLTEventDoneDataProcessor::kDone;
#endif
    }



#if 0
Different scheme now implemented
bool AliHLTTriggerEventFilter::FilterEventDescriptor( AliHLTSubEventDataDescriptor* sedd, vector<const AliHLTTriggerDecisionBlock*> tdbs, bool matchOrNonMatch )
    {
    if ( !sedd || !fDataSpecMatchFunc )
	return false;
    unsigned long n, i, t;
    const AliHLTTriggerDecisionBlock* tdb;
    vector<unsigned long> blocks;

    n = sedd->fDataBlockCount;

    for ( i = 0; i < n; i++ )
	{
	t = 0;
	while ( t<tdbs.size() )
	    {
	    tdb = tdbs[t];
// 	    printf( "%s:%d: %d - %d - %d - %d - %d\n", __FILE__, __LINE__, 
// 		    (int)(tdb->fDataType.fID == ALL_DATAID), (int)(tdb->fDataType.fID == sedd->fDataBlocks[i].fDataType.fID), 
// 		    (int)(tdb->fDataOrigin.fID == ALL_DATAORIGIN), (int)(tdb->fDataOrigin.fID == sedd->fDataBlocks[i].fDataOrigin.fID), 
// 		    (int)(fDataSpecMatchFunc( &(sedd->fDataBlocks[i]), tdb )) );
	    if ( ((tdb->fDataType.fID == ALL_DATAID ||
		  sedd->fDataBlocks[i].fDataType.fID == ALL_DATAID ||
		  tdb->fDataType.fID == sedd->fDataBlocks[i].fDataType.fID) &&
		 (tdb->fDataOrigin.fID == ALL_DATAORIGIN ||
		  sedd->fDataBlocks[i].fDataOrigin.fID == ALL_DATAORIGIN ||
		  tdb->fDataOrigin.fID == sedd->fDataBlocks[i].fDataOrigin.fID) &&
		 fDataSpecMatchFunc( sedd->fDataBlocks[i].fDataType, sedd->fDataBlocks[i].fDataOrigin,
				     sedd->fDataBlocks[i].fDataSpecification, tdb )) == matchOrNonMatch )
		{
		blocks.insert( blocks.end(), i );
		break;
		}
	    t++;
	    }
	}

    unsigned long src, dest;
//     printf( "%s:%d - blocks: %lu\n", __FILE__, __LINE__, (unsigned long)blocks.size() );
    for ( i=0, src=0, dest=0; src<n && i<blocks.size(); src++ )
	{
	if ( src==blocks[i] )
	    {
// 	    printf( "%s:%d - i: %lu - blocks[%lu]: %lu\n", __FILE__, __LINE__, i, i, blocks[i] );
	    if ( src!=dest )
		sedd->fDataBlocks[dest] = sedd->fDataBlocks[src];
	    dest++;
	    i++;
	    }
	}
    sedd->fHeader.fLength -= (sedd->fDataBlockCount-blocks.size())*sizeof(AliHLTSubEventDataBlockDescriptor);
    sedd->fDataBlockCount = blocks.size();
    if ( sedd->fDataBlockCount )
	return true;
    else
	return false;
    }


bool AliHLTTriggerEventFilter::FilterEventDescriptor( AliUInt32_t blockCnt, AliHLTSubEventDescriptor::BlockData* blocks, 
						      vector<AliHLTSubEventDescriptor::BlockData>& outputBlocks, 
						      vector<const AliHLTTriggerDecisionBlock*> tdbs, bool matchOrNonMatch )
    {
    if ( !blockCnt || !blocks || !fDataSpecMatchFunc )
	return false;
    unsigned long n, i, t;
    const AliHLTTriggerDecisionBlock* tdb;

    n = blockCnt;
    outputBlocks.clear();

    for ( i = 0; i < n; i++ )
	{
	t = 0;
	while ( t<tdbs.size() )
	    {
	    tdb = tdbs[t];
// 	    printf( "%s:%d: %d - %d - %d - %d - %d\n", __FILE__, __LINE__, 
// 		    (int)(tdb->fDataType.fID == ALL_DATAID), (int)(tdb->fDataType.fID == sedd->fDataBlocks[i].fDataType.fID), 
// 		    (int)(tdb->fDataOrigin.fID == ALL_DATAORIGIN), (int)(tdb->fDataOrigin.fID == sedd->fDataBlocks[i].fDataOrigin.fID), 
// 		    (int)(fDataSpecMatchFunc( &(sedd->fDataBlocks[i]), tdb )) );
	    if ( ((tdb->fDataType.fID == ALL_DATAID ||
		  blocks[i].fDataType.fID == ALL_DATAID ||
		  tdb->fDataType.fID == blocks[i].fDataType.fID) &&
		 (tdb->fDataOrigin.fID == ALL_DATAORIGIN ||
		  blocks[i].fDataOrigin.fID == ALL_DATAORIGIN ||
		  tdb->fDataOrigin.fID == blocks[i].fDataOrigin.fID) &&
		 fDataSpecMatchFunc( blocks[i].fDataType, blocks[i].fDataOrigin,
				     blocks[i].fDataSpecification, tdb )) == matchOrNonMatch )
		{
		outputBlocks.insert( outputBlocks.end(), blocks[i] );
		break;
		}
	    t++;
	    }
	}

    if ( outputBlocks.size() )
	return true;
    else
	return false;
    }
#endif

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
