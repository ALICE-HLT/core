/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTHLTEventTriggerData.hpp"
#include "AliHLTDDLHeaderData.h"
#include "MLUCString.hpp"


void AliHLTHLTMergeEventTriggerData( AliHLTHLTEventTriggerData* dest, AliHLTHLTEventTriggerData const* mergeIn )
    {
    AliUInt64_t cdhData;
    cdhData = AliHLTGetDDLHeaderStatusError( dest->fCommonHeader ) | AliHLTGetDDLHeaderStatusError( mergeIn->fCommonHeader );
    AliHLTSetDDLHeaderStatusError( dest->fCommonHeader, cdhData );

    cdhData = AliHLTGetDDLHeaderL1TriggerType( dest->fCommonHeader ) | AliHLTGetDDLHeaderL1TriggerType( mergeIn->fCommonHeader );
    AliHLTSetDDLHeaderL1TriggerType( dest->fCommonHeader, cdhData );

    cdhData = AliHLTGetDDLHeaderTriggerClassesLow50( dest->fCommonHeader ) | AliHLTGetDDLHeaderTriggerClassesLow50( mergeIn->fCommonHeader );
    AliHLTSetDDLHeaderTriggerClassesLow50( dest->fCommonHeader, cdhData );
    
    cdhData = AliHLTGetDDLHeaderTriggerClassesHigh50( dest->fCommonHeader ) | AliHLTGetDDLHeaderTriggerClassesHigh50( mergeIn->fCommonHeader );
    AliHLTSetDDLHeaderTriggerClassesHigh50( dest->fCommonHeader, cdhData );
    
    cdhData = AliHLTGetDDLHeaderROI( dest->fCommonHeader ) | AliHLTGetDDLHeaderROI( mergeIn->fCommonHeader );
    AliHLTSetDDLHeaderROI( dest->fCommonHeader, cdhData );

#if 1
    AliUInt32_t *destPtr=dest->fReadoutList;
    const AliUInt32_t *srcPtr=mergeIn->fReadoutList;
    unsigned cnt = *destPtr<*srcPtr ? *destPtr : *srcPtr;
    destPtr++;
    srcPtr++;
    for ( unsigned i=0; i<cnt; i++, destPtr++, srcPtr++ )
	*destPtr |= *srcPtr;
#else
    unsigned cnt = dest->fReadoutList[0]<mergeIn->fReadoutList[0] ? dest->fReadoutList[0] : mergeIn->fReadoutList[0];
    for ( unsigned i=0; i<cnt; i++ )
	dest->fReadoutList[i+1] |= mergeIn->fReadoutList[i+1];
#endif
    }

MLUCString AliHLTHLEventTriggerData2String( const AliHLTEventTriggerStruct* ets )
    {
    MLUCString str;
    if ( ets->fHeader.fLength==sizeof(AliHLTHLTEventTriggerData) )
	{
	char tmp[256];
	str = "Status flag: ";
	snprintf( tmp, 256, "0x%016LX", (unsigned long long)((AliHLTHLTEventTriggerData*)ets)->fHLTStatus );
	str += tmp;
	str += " CDH: Version: ";
	snprintf( tmp, 256, "%3u", (unsigned)AliHLTGetDDLHeaderVersion( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	str += tmp;
	str += "Size: ";
	snprintf( tmp, 256, "%10u", (unsigned)AliHLTGetDDLHeaderBlockLength( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	str += tmp;
	str += " L1 Trigger Type: ";
	snprintf( tmp, 256, "0x%02X", (unsigned)AliHLTGetDDLHeaderL1TriggerType( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	str += tmp;
	str += " Event ID1: ";
	snprintf( tmp, 256, "0x%03X", (unsigned)AliHLTGetDDLHeaderEventID1( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	str += tmp;
	str += " Event ID2: ";
	snprintf( tmp, 256, "0x%06X", (unsigned)AliHLTGetDDLHeaderEventID2( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	str += tmp;
	str += " Participating Subdetectors: ";
	snprintf( tmp, 256, "0x%06X", (unsigned)AliHLTGetDDLHeaderPartSubDet( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	str += tmp;
	str += " Block Attributes: ";
	snprintf( tmp, 256, "0x%02X", (unsigned)AliHLTGetDDLHeaderBlockAttr( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	str += tmp;
	str += " Mini Event ID: ";
	snprintf( tmp, 256, "0x%03X", (unsigned)AliHLTGetDDLHeaderMiniEventID( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	str += tmp;
	str += " Status & Error: ";
	snprintf( tmp, 256, "0x%04X", (unsigned)AliHLTGetDDLHeaderStatusError( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	str += tmp;
	str += " Trigger Classes: ";
	snprintf( tmp, 256, "0x%013LX", (unsigned long long)AliHLTGetDDLHeaderTriggerClassesHigh50( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	str += tmp;
	snprintf( tmp, 256, "0x%013LX", (unsigned long long)AliHLTGetDDLHeaderTriggerClassesLow50( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	str += tmp;
	str += " Region of Interest: ";
	snprintf( tmp, 256, "0x%09LX", (unsigned long long)AliHLTGetDDLHeaderROI( ((AliHLTHLTEventTriggerData*)ets)->fCommonHeader ) );
	str += tmp;
	}
    else
	str = "[No HLT event trigger data found]";
    return str;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
