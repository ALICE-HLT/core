/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTriggerStackMachine.hpp"
#include "AliHLTHLTEventTriggerData.hpp"
#include "AliHLTDDLHeaderData.h"
#include "AliHLTLog.hpp"

AliHLTTriggerStackMachine::AliHLTTriggerStackMachine()
    {
    }

AliHLTTriggerStackMachine::~AliHLTTriggerStackMachine()
    {
    }


/*
	enum TInstructions { kPUSHCDHL1TRIGGERTYPE = 0x80, kPUSHCDHEVENTID, kPUSHCDHPARTSUBDET, kPUSHCDHBLOCKATTR, kPUSHCDHMINIEVENTID, kPUSHCDHSTATUSERROR, kPUSHCDHTRIGGERCLASSES, kPUSHCDHROI };
*/


MLUCSimpleStackMachine::TError AliHLTTriggerStackMachine::ExecNextStatement()
    {
    uint64 value0=0;
    switch ( (TInstructions)(fCodeWords[fIP]) )
	{
	case kPUSHCDHL1TRIGGERTYPE:
	    if ( !fEventTriggerStruct )
		return TError( TError::kNoHLTTriggerData, "No HLT trigger data configured" );
	    if ( fEventTriggerStruct->fHeader.fLength!=sizeof(AliHLTHLTEventTriggerData) )
		return TError( TError::kWrongHLTTriggerDataSize, "HLT trigger data has wrong size" );
	    Push( AliHLTGetDDLHeaderL1TriggerType( ((AliHLTHLTEventTriggerData*)fEventTriggerStruct)->fCommonHeader ) );
	    ++fIP;
	    break;
	case kPUSHCDHEVENTID:
	    if ( !fEventTriggerStruct )
		return TError( TError::kNoHLTTriggerData, "No HLT trigger data configured" );
	    if ( fEventTriggerStruct->fHeader.fLength!=sizeof(AliHLTHLTEventTriggerData) )
		return TError( TError::kWrongHLTTriggerDataSize, "HLT trigger data has wrong size" );
	    value0 = AliHLTGetDDLHeaderEventID2( ((AliHLTHLTEventTriggerData*)fEventTriggerStruct)->fCommonHeader );
	    value0 = (value0 << 12) | AliHLTGetDDLHeaderEventID1( ((AliHLTHLTEventTriggerData*)fEventTriggerStruct)->fCommonHeader );
	    Push( value0 );
	    ++fIP;
	    break;
	case kPUSHCDHPARTSUBDET:
	    if ( !fEventTriggerStruct )
		return TError( TError::kNoHLTTriggerData, "No HLT trigger data configured" );
	    if ( fEventTriggerStruct->fHeader.fLength!=sizeof(AliHLTHLTEventTriggerData) )
		return TError( TError::kWrongHLTTriggerDataSize, "HLT trigger data has wrong size" );
	    Push( AliHLTGetDDLHeaderPartSubDet( ((AliHLTHLTEventTriggerData*)fEventTriggerStruct)->fCommonHeader ) );
	    ++fIP;
	    break;
	case kPUSHCDHBLOCKATTR:
	    if ( !fEventTriggerStruct )
		return TError( TError::kNoHLTTriggerData, "No HLT trigger data configured" );
	    if ( fEventTriggerStruct->fHeader.fLength!=sizeof(AliHLTHLTEventTriggerData) )
		return TError( TError::kWrongHLTTriggerDataSize, "HLT trigger data has wrong size" );
	    Push( AliHLTGetDDLHeaderBlockAttr( ((AliHLTHLTEventTriggerData*)fEventTriggerStruct)->fCommonHeader ) );
	    ++fIP;
	    break;
	case kPUSHCDHMINIEVENTID:
	    if ( !fEventTriggerStruct )
		return TError( TError::kNoHLTTriggerData, "No HLT trigger data configured" );
	    if ( fEventTriggerStruct->fHeader.fLength!=sizeof(AliHLTHLTEventTriggerData) )
		return TError( TError::kWrongHLTTriggerDataSize, "HLT trigger data has wrong size" );
	    Push( AliHLTGetDDLHeaderMiniEventID( ((AliHLTHLTEventTriggerData*)fEventTriggerStruct)->fCommonHeader ) );
	    ++fIP;
	    break;
	case kPUSHCDHSTATUSERROR:
	    if ( !fEventTriggerStruct )
		return TError( TError::kNoHLTTriggerData, "No HLT trigger data configured" );
	    if ( fEventTriggerStruct->fHeader.fLength!=sizeof(AliHLTHLTEventTriggerData) )
		return TError( TError::kWrongHLTTriggerDataSize, "HLT trigger data has wrong size" );
	    Push( AliHLTGetDDLHeaderStatusError( ((AliHLTHLTEventTriggerData*)fEventTriggerStruct)->fCommonHeader ) );
	    ++fIP;
	    break;
	case kPUSHCDHTRIGGERCLASSESLOW:
	    if ( !fEventTriggerStruct )
		return TError( TError::kNoHLTTriggerData, "No HLT trigger data configured" );
	    if ( fEventTriggerStruct->fHeader.fLength!=sizeof(AliHLTHLTEventTriggerData) )
		return TError( TError::kWrongHLTTriggerDataSize, "HLT trigger data has wrong size" );
	    Push( AliHLTGetDDLHeaderTriggerClassesLow50( ((AliHLTHLTEventTriggerData*)fEventTriggerStruct)->fCommonHeader ) );
	    ++fIP;
	    break;
	case kPUSHCDHTRIGGERCLASSESHIGH:
	    if ( !fEventTriggerStruct )
		return TError( TError::kNoHLTTriggerData, "No HLT trigger data configured" );
	    if ( fEventTriggerStruct->fHeader.fLength!=sizeof(AliHLTHLTEventTriggerData) )
		return TError( TError::kWrongHLTTriggerDataSize, "HLT trigger data has wrong size" );
	    Push( AliHLTGetDDLHeaderTriggerClassesHigh50( ((AliHLTHLTEventTriggerData*)fEventTriggerStruct)->fCommonHeader ) );
	    ++fIP;
	    break;
	case kPUSHCDHROI:
	    if ( !fEventTriggerStruct )
		return TError( TError::kNoHLTTriggerData, "No HLT trigger data configured" );
	    if ( fEventTriggerStruct->fHeader.fLength!=sizeof(AliHLTHLTEventTriggerData) )
		return TError( TError::kWrongHLTTriggerDataSize, "HLT trigger data has wrong size" );
	    Push( AliHLTGetDDLHeaderROI( ((AliHLTHLTEventTriggerData*)fEventTriggerStruct)->fCommonHeader ) );
	    ++fIP;
	    break;
	default:
	    return MLUCSimpleStackMachine::ExecNextStatement();
	}
    return TError();
	
    }



bool StackMachineCompareEventTrigger( void* param, const AliHLTEventTriggerStruct* ettSubscriber, const AliHLTEventTriggerStruct* ettEvent )
    {
    if ( ettEvent->fHeader.fLength!=sizeof(AliHLTHLTEventTriggerData) )
	{
	LOGM( AliHLTLog::kInformational, "StackMachineCompareEventTrigger", "Incorrect trigger data size", 100 )
	    << "Trigger data does not have expected size for AliHLTHLTEventTriggerData. Assuming positive event trigger decision..." << ENDLOG;
	return true;
	}
    if ( !param )
	{
	LOG( AliHLTLog::kError, "StackMachineCompareEventTrigger", "No stack machine" )
	    << "No stack machine available via param (NULL pointer)" << ENDLOG;
	return false;
	}
    AliHLTTriggerStackMachine* stackMachine = reinterpret_cast<AliHLTTriggerStackMachine*>( param );
    MLUCSimpleStackMachine::TError err;
    err = stackMachine->SetCode( ettSubscriber->fDataWordCount >> 1, (uint64*)&(ettSubscriber->fDataWords[0]) );
    if ( !err.Ok() )
	{
	AliUInt64_t value0 = ~(AliUInt64_t)0;
	if ( ettEvent->fHeader.fLength==sizeof(AliHLTHLTEventTriggerData) )
	    {
	    value0 = AliHLTGetDDLHeaderEventID2( ((AliHLTHLTEventTriggerData*)ettEvent)->fCommonHeader );
	    value0 = (value0 << 12) | AliHLTGetDDLHeaderEventID1( ((AliHLTHLTEventTriggerData*)ettEvent)->fCommonHeader );
	    }
	LOG( AliHLTLog::kError, "StackMachineCompareEventTrigger", "Cannot set code for trigger stack machine" )
	    << "Error setting code for event trigger stack machine for event 0x" << AliHLTLog::kHex
	    << value0 << ": '" << err.Description() << "'." << ENDLOG;
	return true;
	}
    stackMachine->SetEventTriggerStruct( ettEvent );
    err = stackMachine->Run();
    if ( !err.Ok() )
	{
	AliUInt64_t value0 = ~(AliUInt64_t)0;
	if ( ettEvent->fHeader.fLength==sizeof(AliHLTHLTEventTriggerData) )
	    {
	    value0 = AliHLTGetDDLHeaderEventID2( ((AliHLTHLTEventTriggerData*)ettEvent)->fCommonHeader );
	    value0 = (value0 << 12) | AliHLTGetDDLHeaderEventID1( ((AliHLTHLTEventTriggerData*)ettEvent)->fCommonHeader );
	    }
	LOG( AliHLTLog::kError, "StackMachineCompareEventTrigger", "Error running trigger stack machine" )
	    << "Error running event trigger stack machine for event 0x" << AliHLTLog::kHex
	    << value0 << ": '" << err.Description() << "'." << ENDLOG;
	return true;
	}
    uint64 result;
    err = stackMachine->GetResult( result );
    if ( !err.Ok() )
	{
	AliUInt64_t value0 = ~(AliUInt64_t)0;
	if ( ettEvent->fHeader.fLength==sizeof(AliHLTHLTEventTriggerData) )
	    {
	    value0 = AliHLTGetDDLHeaderEventID2( ((AliHLTHLTEventTriggerData*)ettEvent)->fCommonHeader );
	    value0 = (value0 << 12) | AliHLTGetDDLHeaderEventID1( ((AliHLTHLTEventTriggerData*)ettEvent)->fCommonHeader );
	    }
	LOG( AliHLTLog::kError, "StackMachineCompareEventTrigger", "Error running trigger stack machine" )
	    << "Error running event trigger stack machine for event 0x" << AliHLTLog::kHex
	    << value0 << ": '" << err.Description() << "'." << ENDLOG;
	return true;
	}
    return (bool)result;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
