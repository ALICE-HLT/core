/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTriggerDecision.hpp"
#include "AliHLTEventDoneData.hpp"


AliHLTEventDoneData* AliHLTMakeEventDoneData( vector<AliHLTTriggerDecisionArray> tdbs, bool readoutOrMonitor )
    {
    AliHLTEventDoneData* edd;
    const unsigned long cnt = tdbs.size();
    unsigned long len = cnt*sizeof(AliHLTTriggerDecisionArray)/sizeof(AliUInt32_t)+2;

    edd = AliHLTMakeEventDoneData( len );
    if ( !edd )
	return NULL;
    AliUInt32_t* data = (AliUInt32_t*)edd->fDataWords;
    if ( readoutOrMonitor )
	data[0] = kAliHLTEventDoneReadoutListCmd;
    else
	data[0] = kAliHLTEventDoneMonitorListCmd;
    data[1] = cnt;
    for ( unsigned long ii=0; ii<cnt; ii++ )
	{
	data[2+ii*sizeof(AliHLTTriggerDecisionArray)/sizeof(AliUInt32_t)+0] = tdbs[ii][0];
	data[2+ii*sizeof(AliHLTTriggerDecisionArray)/sizeof(AliUInt32_t)+1] = tdbs[ii][1];
	data[2+ii*sizeof(AliHLTTriggerDecisionArray)/sizeof(AliUInt32_t)+2] = tdbs[ii][2];
	data[2+ii*sizeof(AliHLTTriggerDecisionArray)/sizeof(AliUInt32_t)+3] = tdbs[ii][3];
	}
    return edd;
    }


#if 0
Different encoding/storage scheme now used for Event Done Data (cf. Base/include/AliHLTEventDoneData.hpp

AliHLTEventDoneData* AliHLTMakeEventDoneData( vector<AliHLTTriggerDecisionBlock*> tdbs )
    {
    AliHLTEventDoneData* edd;
    vector<AliHLTTriggerDecisionBlock*>::iterator iter, end;
    unsigned long len=0;
    iter = tdbs.begin();
    end = tdbs.end();
    while ( iter!=end )
	{
	if ( *iter )
	    len += sizeof(AliHLTTriggerDecisionBlock);
	iter++;
	}
    len /= sizeof(AliUInt32_t);
    edd = AliHLTMakeEventDoneData( len );
    if ( !edd )
	return NULL;
    AliUInt8_t* data = (AliUInt8_t*)edd->fDataWords;
    iter = tdbs.begin();
    end = tdbs.end();
    while ( iter!=end )
	{
	if ( *iter )
	    {
	    memcpy( data, *iter, sizeof(AliHLTTriggerDecisionBlock) );
	    data += sizeof(AliHLTTriggerDecisionBlock);
	    }
	iter++;
	}
    return edd;
    }

const AliHLTTriggerDecisionBlock* AliHLTGetTriggerDecisionBlock( const AliHLTEventDoneData* edd, unsigned blockIndex )
    {
    if ( !edd )
	return NULL;
    unsigned long n;
    n = (edd->fDataWordCount*sizeof(AliUInt32_t)) / sizeof(AliHLTTriggerDecisionBlock);
    if ( blockIndex >= n )
	return NULL;
    return ((const AliHLTTriggerDecisionBlock*)edd->fDataWords) + blockIndex;
    }

void AliHLTGetTriggerDecisionBlocks( const AliHLTEventDoneData* edd, vector<const AliHLTTriggerDecisionBlock*>& tdbs )
    {
    tdbs.clear();
    if ( !edd )
	return;
    unsigned long n, i;
    n = (edd->fDataWordCount*sizeof(AliUInt32_t)) / sizeof(AliHLTTriggerDecisionBlock);
    for ( i = 0; i < n; i++ )
	tdbs.insert( tdbs.end(), ((const AliHLTTriggerDecisionBlock*)edd->fDataWords) + i );
    }

#endif






/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
