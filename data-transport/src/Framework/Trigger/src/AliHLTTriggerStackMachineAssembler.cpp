/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTTriggerStackMachineAssembler.hpp"
#include "AliHLTTriggerStackMachine.hpp"



AliHLTTriggerStackMachineAssembler::AliHLTTriggerStackMachineAssembler()
    {
    }

AliHLTTriggerStackMachineAssembler::~AliHLTTriggerStackMachineAssembler()
    {
    }


MLUCSimpleStackMachineAssembler::TError AliHLTTriggerStackMachineAssembler::Assemble( std::vector<MLUCString>& tokens )
    {
    if ( tokens.size()<=0 )
	return TError();
    if ( tokens[0]=="PUSHCDHL1TRIGGERTYPE" )
	{
	return AddWord( AliHLTTriggerStackMachine::kPUSHCDHL1TRIGGERTYPE );
	}
    else if ( tokens[0]=="PUSHCDHEVENTID" )
	{
	return AddWord( AliHLTTriggerStackMachine::kPUSHCDHEVENTID );
	}
    else if ( tokens[0]=="PUSHCDHPARTSUBDET" )
	{
	return AddWord( AliHLTTriggerStackMachine::kPUSHCDHPARTSUBDET );
	}
    else if ( tokens[0]=="PUSHCDHBLOCKATTR" )
	{
	return AddWord( AliHLTTriggerStackMachine::kPUSHCDHBLOCKATTR );
	}
    else if ( tokens[0]=="PUSHCDHMINIEVENTID" )
	{
	return AddWord( AliHLTTriggerStackMachine::kPUSHCDHMINIEVENTID );
	}
    else if ( tokens[0]=="PUSHCDHSTATUSERROR" )
	{
	return AddWord( AliHLTTriggerStackMachine::kPUSHCDHSTATUSERROR );
	}
    else if ( tokens[0]=="PUSHCDHTRIGGERCLASSESLOW" )
	{
	return AddWord( AliHLTTriggerStackMachine::kPUSHCDHTRIGGERCLASSESLOW );
	}
    else if ( tokens[0]=="PUSHCDHTRIGGERCLASSESHIGH" )
	{
	return AddWord( AliHLTTriggerStackMachine::kPUSHCDHTRIGGERCLASSESHIGH );
	}
    else if ( tokens[0]=="PUSHCDHROI" )
	{
	return AddWord( AliHLTTriggerStackMachine::kPUSHCDHROI );
	}
    else
	return MLUCSimpleStackMachineAssembler::Assemble( tokens );
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
