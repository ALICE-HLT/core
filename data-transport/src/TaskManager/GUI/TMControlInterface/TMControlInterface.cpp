/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "Python.h"
#include "TMControlMaster.hpp"
#include "TMSysException.hpp"
#include "MLUCThread.hpp"
#include "MLUCLog.hpp"
#include "MLUCLogServer.hpp"

/* 
** For platforms where this is not defined in the included Python headers
*/
#ifndef PyMODINIT_FUNC
#       if defined(__cplusplus)
#               define PyMODINIT_FUNC extern "C" void
#       else /* __cplusplus */
#               define PyMODINIT_FUNC void
#       endif /* __cplusplus */
#endif

extern "C"
{
    static PyObject * TMCIInit(PyObject *self, PyObject *arg);
    static PyObject * TMCITerminate(PyObject *self, PyObject *arg);
    static PyObject * TMCIQueryState(PyObject *self, PyObject *arg);
    static PyObject * TMCIQueryStatusData(PyObject *self, PyObject *arg);
    static PyObject * TMCIQueryChildren(PyObject *self, PyObject *arg);
    static PyObject * TMCIQueryStates(PyObject *self, PyObject *arg);
    static PyObject * TMCIQueryChildStates(PyObject *self, PyObject *arg);
    static PyObject * TMCISendCmd(PyObject *self, PyObject *arg);
    static PyObject * TMCIConnect(PyObject *self, PyObject *arg);
    static PyObject * TMCIDisconnect(PyObject *self, PyObject *arg);
}

static PyMethodDef TMControlInterfaceMethods[] = {
    {"Init",  TMCIInit, METH_VARARGS,
     "Initialize the control interface"},
    {"Terminate",  TMCITerminate, METH_VARARGS,
     "Terminate the control interface"},
    {"QueryState",  TMCIQueryState, METH_VARARGS,
     "Query a TaskManager's current state"},
    {"QueryStatusData",  TMCIQueryStatusData, METH_VARARGS,
     "Query a TaskManager's current status data"},
    {"QueryChildren",  TMCIQueryChildren, METH_VARARGS,
     "Obtain information about a TaskManager's child processes"},
    {"QueryStates",  TMCIQueryStates, METH_VARARGS,
     "Query a TaskManager's valid states"},
    {"QueryChildStates",  TMCIQueryChildStates, METH_VARARGS,
     "Query a child processes' valid states"},
    {"SendCmd",  TMCISendCmd, METH_VARARGS,
     "Send a command to a TaskManager"},
    {"Connect",  TMCIConnect, METH_VARARGS,
     "Connect to a TaskManager"},
    {"Disconnect",  TMCIDisconnect, METH_VARARGS,
     "Disconnect from a TaskManager"},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};


class TMControlInterfaceThread: public MLUCThread
    {
    public:
	TMControlInterfaceThread( TMControlMaster*& master ):
	    fMaster( master )
		{
		}
    protected:
	virtual void Run()
		{
		if ( fMaster )
		    fMaster->RunLoop();
		}

	TMControlMaster* fMaster;

    };


static TMControlMaster* gControlMaster = NULL;

static TMControlInterfaceThread* gInterfaceThread = NULL;

static MLUCStderrLogServer* gStderrLog = NULL;

PyMODINIT_FUNC
initTMControlInterface(void)
{
    (void) Py_InitModule( "TMControlInterface", TMControlInterfaceMethods );
//     if ( !gControlMaster )
// 	gControlMaster = new TMControlMaster;

//     gInterfaceThread = new TMControlInterfaceThread( gControlMaster );
//     gInterfaceThread.Start();

}


static PyObject * TMCIInit(PyObject*, PyObject *arg)
    {
    int ret;
    char* listen_address;
    ret = PyArg_ParseTuple( arg, "s", &listen_address );
    if ( !ret )
	return NULL;

    gStderrLog = new MLUCStderrLogServer;
    if ( gStderrLog )
	gLog.AddServer( gStderrLog );

    gLogLevel = 0x39;
    
    try
	{
	if ( !gControlMaster )
	    gControlMaster = new TMControlMaster;
	if ( !gInterfaceThread )
	    gInterfaceThread = new TMControlInterfaceThread( gControlMaster );

	gControlMaster->SetAddress( listen_address );
	gInterfaceThread->Start();
	PyObject* pyret = Py_BuildValue( "" );
	return pyret;
	}
    catch ( TMSysException e )
	{
	if ( gControlMaster )
	    {
// 	    printf( "Dobby is stopping Master\n" );
	    try
		{
		gControlMaster->StopLoop();
		}
	    catch ( TMSysException e ) {}
	    struct timeval start, now;
	    unsigned long long deltaT = 0;
	    const unsigned long long timeLimit = 20000000;
	    gettimeofday( &start, NULL );
	    while ( deltaT<timeLimit && gControlMaster->IsLoopRunning() )
		{
// 		printf( "Waiting for loop; %d\n", i );
		usleep( 200000 );
		gettimeofday( &now, NULL );
		deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
		}
// 	    printf( "Dobby has stopped Master\n" );
	    delete gControlMaster;
	    gControlMaster = NULL;
	    }
	if ( gInterfaceThread )
	    {
	    gInterfaceThread->Join();
	    delete gInterfaceThread;
	    gInterfaceThread = NULL;
	    }
	PyErr_SetString( PyExc_SystemError, e.GetErrorStr() );
	return NULL;
	}
    }

static PyObject * TMCITerminate(PyObject*, PyObject *arg)
    {
    int ret;
    ret = PyArg_ParseTuple( arg, "" );
    if ( !ret )
	return NULL;
    try
	{
	if ( gControlMaster )
	    {
// 	    printf( "Dobby is stopping Master\n" );
	    gControlMaster->StopLoop();
	    struct timeval start, now;
	    unsigned long long deltaT = 0;
	    const unsigned long long timeLimit = 20000000;
	    gettimeofday( &start, NULL );
	    while ( deltaT<timeLimit && gControlMaster->IsLoopRunning() )
		{
// 		printf( "Waiting for loop; %d\n", i );
		usleep( 200000 );
		gettimeofday( &now, NULL );
		deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
		}
// 	    printf( "Dobby has stopped Master\n" );
	    delete gControlMaster;
	    gControlMaster = NULL;
	    }
	if ( gInterfaceThread )
	    {
	    gInterfaceThread->Join();
	    delete gInterfaceThread;
	    gInterfaceThread = NULL;
	    }
	PyObject* pyret = Py_BuildValue( "" );
	return pyret;
	}
    catch ( TMSysException e )
	{
	PyErr_SetString( PyExc_SystemError, e.GetErrorStr() );
	return NULL;
	}
    }

static PyObject * TMCIQueryState(PyObject*, PyObject *arg)
    {
    int ret;
    char* address;
    MLUCString state;
    ret = PyArg_ParseTuple( arg, "s", &address );
    if ( !ret )
	return NULL;

    if ( !gControlMaster )
	{
	PyErr_SetString( PyExc_SystemError, "TaskManager control master interface not found" );
	return NULL;
	}

    void* address_bin = NULL;
    try
	{
	gControlMaster->ConvertAddress( address, &address_bin );
	gControlMaster->QueryState( address_bin, state );
	free( address_bin );
	address_bin = NULL;
	PyObject* pyret = Py_BuildValue( "s", state.c_str() );
	return pyret;
	}
    catch ( TMSysException e )
	{
	if ( address_bin )
	    free( address_bin );
	PyErr_SetString( PyExc_SystemError, e.GetErrorStr() );
	return NULL;
	}
    }

static PyObject * TMCIQueryStatusData(PyObject*, PyObject *arg)
    {
    int ret;
    char* address;
    MLUCString statusData;
    ret = PyArg_ParseTuple( arg, "s", &address );
    if ( !ret )
	return NULL;

    if ( !gControlMaster )
	{
	PyErr_SetString( PyExc_SystemError, "TaskManager control master interface not found" );
	return NULL;
	}

    void* address_bin = NULL;
    try
	{
	gControlMaster->ConvertAddress( address, &address_bin );
	gControlMaster->QueryStatusData( address_bin, statusData );
	free( address_bin );
	address_bin = NULL;
	PyObject* pyret = Py_BuildValue( "s", statusData.c_str() );
	return pyret;
	}
    catch ( TMSysException e )
	{
	if ( address_bin )
	    free( address_bin );
	PyErr_SetString( PyExc_SystemError, e.GetErrorStr() );
	return NULL;
	}
    }

static PyObject * TMCIQueryChildren(PyObject*, PyObject *arg)
    {
    int ret;
    char* address;
    MLUCString children;
    ret = PyArg_ParseTuple( arg, "s", &address );
    if ( !ret )
	return NULL;

    if ( !gControlMaster )
	{
	PyErr_SetString( PyExc_SystemError, "TaskManager control master interface not found" );
	return NULL;
	}

    void* address_bin = NULL;
    try
	{
	gControlMaster->ConvertAddress( address, &address_bin );
	gControlMaster->QueryChildren( address_bin, children );
// 	printf( "Children: %s\n", children.c_str() );
	free( address_bin );
	address_bin = NULL;
	PyObject* pyret = Py_BuildValue( "s", children.c_str() );
	return pyret;
	}
    catch ( TMSysException e )
	{
	if ( address_bin )
	    free( address_bin );
	PyErr_SetString( PyExc_SystemError, e.GetErrorStr() );
	return NULL;
	}
    }

static PyObject * TMCIQueryChildStates(PyObject*, PyObject *arg)
    {
    int ret;
    char* address;
    char* id;
    MLUCString states;
    ret = PyArg_ParseTuple( arg, "ss", &address, &id );
    if ( !ret )
	return NULL;

    if ( !gControlMaster )
	{
	PyErr_SetString( PyExc_SystemError, "TaskManager control master interface not found" );
	return NULL;
	}

    void* address_bin = NULL;
    try
	{
	gControlMaster->ConvertAddress( address, &address_bin );
	gControlMaster->QueryChildStates( address_bin, id, states );
// 	printf( "states: %s\n", states.c_str() );
	free( address_bin );
	address_bin = NULL;
	PyObject* pyret = Py_BuildValue( "s", states.c_str() );
	return pyret;
	}
    catch ( TMSysException e )
	{
	if ( address_bin )
	    free( address_bin );
	PyErr_SetString( PyExc_SystemError, e.GetErrorStr() );
	return NULL;
	}
    }

static PyObject * TMCIQueryStates(PyObject*, PyObject *arg)
    {
    int ret;
    char* address;
    MLUCString states;
    ret = PyArg_ParseTuple( arg, "s", &address );
    if ( !ret )
	return NULL;

    if ( !gControlMaster )
	{
	PyErr_SetString( PyExc_SystemError, "TaskManager control master interface not found" );
	return NULL;
	}
    void* address_bin = NULL;
    try
	{
	gControlMaster->ConvertAddress( address, &address_bin );
	gControlMaster->QueryStates( address_bin, states );
// 	printf( "States: %s\n", states.c_str() );
	free( address_bin );
	address_bin = NULL;
	PyObject* pyret = Py_BuildValue( "s", states.c_str() );
	return pyret;
	}
    catch ( TMSysException e )
	{
	if ( address_bin )
	    free( address_bin );
	PyErr_SetString( PyExc_SystemError, e.GetErrorStr() );
	return NULL;
	}
    }

static PyObject * TMCISendCmd(PyObject*, PyObject *arg)
    {
    int ret;
    char* address;
    char* command;
    ret = PyArg_ParseTuple( arg, "ss", &address, &command );
    if ( !ret )
	return NULL;

    if ( !gControlMaster )
	{
	PyErr_SetString( PyExc_SystemError, "TaskManager control master interface not found" );
	return NULL;
	}

    void* address_bin = NULL;
    try
	{
	gControlMaster->ConvertAddress( address, &address_bin );
	gControlMaster->SendCmd( address_bin, command );
	free( address_bin );
	address_bin = NULL;
	PyObject* pyret = Py_BuildValue( "" );
	return pyret;
	}
    catch ( TMSysException e )
	{
	if ( address_bin )
	    free( address_bin );
	PyErr_SetString( PyExc_SystemError, e.GetErrorStr() );
	return NULL;
	}
    }

static PyObject * TMCIConnect(PyObject*, PyObject *arg)
    {
    int ret;
    char* address;
    ret = PyArg_ParseTuple( arg, "s", &address );
    if ( !ret )
	return NULL;

    if ( !gControlMaster )
	{
	PyErr_SetString( PyExc_SystemError, "TaskManager control master interface not found" );
	return NULL;
	}

    void* address_bin = NULL;
    try
	{
	gControlMaster->ConvertAddress( address, &address_bin );
	gControlMaster->Connect( address_bin );
	free( address_bin );
	address_bin = NULL;
	PyObject* pyret = Py_BuildValue( "" );
	return pyret;
	}
    catch ( TMSysException e )
	{
	if ( address_bin )
	    free( address_bin );
	PyErr_SetString( PyExc_SystemError, e.GetErrorStr() );
	return NULL;
	}
    }

static PyObject * TMCIDisconnect(PyObject*, PyObject *arg)
    {
    int ret;
    char* address;
    ret = PyArg_ParseTuple( arg, "s", &address );
    if ( !ret )
	return NULL;

    if ( !gControlMaster )
	{
	PyErr_SetString( PyExc_SystemError, "TaskManager control master interface not found" );
	return NULL;
	}

    void* address_bin = NULL;
    try
	{
	gControlMaster->ConvertAddress( address, &address_bin );
	gControlMaster->Disconnect( address_bin );
	free( address_bin );
	address_bin = NULL;
	PyObject* pyret = Py_BuildValue( "" );
	return pyret;
	}
    catch ( TMSysException e )
	{
	if ( address_bin )
	    free( address_bin );
	PyErr_SetString( PyExc_SystemError, e.GetErrorStr() );
	return NULL;
	}
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
