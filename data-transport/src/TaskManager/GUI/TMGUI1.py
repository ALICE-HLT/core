#!/usr/bin/env python


import string, sys, os, os.path, re
from qt import *

uname=os.uname()
target = uname[0]+"-"+uname[4]
base = os.path.dirname( sys.argv[0] )
if ( os.path.basename( base )==target+"-debug" or os.path.basename( base )==target+"-releasedebug" ) and os.path.basename( os.path.dirname( base ) )=="bin":
    sys.path.append( os.path.join( os.path.dirname( os.path.dirname( base ) ), "lib", target+"-debug" ) )
    sys.path.append( os.path.join( os.path.dirname( os.path.dirname( base ) ), "lib", target+"-releasedebug" ) )
sys.path.append( os.path.join( base, "TMControlInterface", target+"-debug" ) )
sys.path.append( os.path.join( base, "TMControlInterface", target+"-releasedebug" ) )


from TMGUI1Form import TMGUI1Form
import TMControlInterface

class TMProcess:
    def __init__( self, ID, type, address, state ):
        self.fID = str(ID)
        self.fType = str(type)
        self.fAddress = str(address)
        self.fState = str(state)
        

class TMGUI1(TMGUI1Form):
    def __init__( self, app, address, childAddress = None, startDisconnected = 0, parent = None, name = None, fl = 0 ):
        TMGUI1Form.__init__( self, parent, name, fl )
        self.fChildren = []
        self.fCommands = []
        self.fStates = []
        self.fOldStateString = ""
        self.fCurrentState = ""
        self.fAddress = ""
        self.fInitialized = 0
        self.fAddressStack = []
        self.fAddressChildID = None
        self.fPollInterval = 2000
        self.fPollIntervalSpinbox.setValue( self.fPollInterval/1000 )
        self.fLastRunNumber = 0
        self.fInDDLList = ""
        self.fOutDDLList = ""
        self.fCTPTriggerClasses = ""
        self.fReadoutListVersion = None
        try:
            print "Using address: '"+address+"'"
            TMControlInterface.Init( address )
            self.fInitialized = 1
        except SystemError, e:
            self.fStatusLabel.setText( "Unable to initialize control interface: "+str(e) )
        self.fTimer = QTimer( self )
        self.connect( self.fTimer, SIGNAL("timeout()"),
                      self.TimerExpired )
        if childAddress!=None:
            self.fAddress = childAddress
            self.fAddressField.setText( self.fAddress )
            if not startDisconnected:
                self.Connect()
        self.fRunManager = False
        QObject.connect(app, SIGNAL('lastWindowClosed()'), self.DoDisconnect)

    def fileExit(self):
        self.close()

    def GetLastRunNumber( self ):
        return self.fLastRunNumber

    def SetInDDLList( self, ddlList ):
        self.fInDDLList = ddlList
        self.fInputDDLListEntry.setText( ddlList )
        

    def SetOutDDLList( self, ddlList ):
        self.fOutDDLList = ddlList
        self.fOutputDDLListEntry.setText( ddlList )
    
    def SetRunNumber( self, runnr ):
        self.fLastRunNumber = runnr
        self.fRunNumber.setValue( self.fLastRunNumber )
        #self.fSendRunParameters.setChecked(True)

    def SetCTPTriggerClasses( self, ctpTC ):
        self.fCTPTriggerClasses = ctpTC
        self.fCTPTriggerClassEntry.setText( ctpTC )

    def SetReadoutListVersion( self, rlv ):
        self.fReadoutListVersion = rlv
        
    def SendRunParameters( self ):
        self.fSendRunParameters.setChecked(True)
        
    def RunManager(self, runMan=True ):
        self.fRunManager = runMan

    def __del__( self ):
        try:
            TMControlInterface.Terminate()
        except SystemError:
            pass

    def Connect( self ):
        if self.fAddress=="" or self.fAddress==None or not self.fInitialized:
            return
        self.fAddressStack = []
        self.fDisconnectButton.setEnabled( 1 )
        self.fConnectButton.setEnabled( 0 )
        self.ConnectToAddress( self.fAddress )
        self.fTimer.start( self.fPollInterval, 1 )
        self.UpdateChildList()
        self.UpdateStateList()
        self.UpdateCommandList()

    def ConnectToChild( self ):
        if self.fAddress=="" or self.fAddress==None or not self.fInitialized:
            return
        childNdx = self.fChildListbox.currentItem()
        if childNdx==-1:
            return
        #print "Child Index: "+str(childNdx)
        #print "Length of Children: "+str(len(self.fChildren))
        #print "ConnectToChild:",self.fChildren[childNdx].fType
        if self.fChildren[childNdx].fType=="slaveprogram" or self.fChildren[childNdx].fType=="slave":
            self.fAddressChildID = None
            self.ConnectToAddress( self.fChildren[childNdx].fAddress )
            self.fParentConnectButton.setEnabled( 1 )
            self.UpdateChildList()
            self.UpdateStateList()
            self.UpdateCommandList()
        elif self.fChildren[childNdx].fType=="program":
            self.fAddressChildID = self.fChildren[childNdx].fID
            self.fParentConnectButton.setEnabled( 1 )
            self.UpdateChildList()
            self.UpdateStateList()
            self.UpdateCommandList()
            
    def ConnectToParent( self ):
        if self.fAddressChildID==None and len(self.fAddressStack)<=1:
            return
        self.PopConnection()
        self.UpdateChildList()
        self.UpdateStateList()
        self.UpdateCommandList()
        if len(self.fAddressStack)<=1:
            self.fParentConnectButton.setEnabled( 0 )

    def Disconnect( self ):
        self.DoDisconnect()
        self.fDisconnectButton.setEnabled( 0 )
        self.fConnectButton.setEnabled( 1 )

    def AddressEntered( self ):
        self.fAddress = str( self.fAddressField.text() )
        if self.fAddress!="" and self.fAddress!=None:
            self.fConnectButton.setEnabled( 1 )
        else:
            self.fConnectButton.setEnabled( 0 )

    def UpdateChildList(self):
        self.fChildren = []
        #self.fChildListbox.clear()
        children = self.QueryChildren()
        if children=="" or children==None:
            self.fChildListbox.clear()
            return
        childProcessList = string.split( children, "  " )
        for c in childProcessList:
            childProcess = string.split( c )
            if len(childProcess)>=4:
                proc = TMProcess( childProcess[0], childProcess[1], childProcess[2], childProcess[3] )
            elif len(childProcess)>=3:
                proc = TMProcess( childProcess[0], childProcess[1], childProcess[2], "UNKNOWN" )
            else:
                continue
            self.fChildren.append( proc )
        sels = []
        ndx = 0
        cnt=self.fChildListbox.count()
        for child in self.fChildren:
            if ndx <cnt:
                sel = self.fChildListbox.isSelected( ndx )
            else:
                sel = 0
            if sel:
                sels.append( 1 )
            else:
                sels.append( 0 )
            ndx=ndx+1
        ndx = 0
        for child in self.fChildren:
            item = child.fID+" - "+child.fState
            if ndx <cnt:
                self.fChildListbox.changeItem( item, ndx )
            else:
                self.fChildListbox.insertItem( item )
            #if child.fType=="slaveprogram" or child.fType=="slave":
            self.fChildListbox.item( ndx ).setSelectable( 1 )
            #else:
            #    self.fChildListbox.item( ndx ).setSelectable( 0 )
            ndx = ndx + 1
        while ndx < cnt:
            self.fChildListbox.removeItem( ndx )
            cnt=cnt-1
        ndx=-0
        for s in sels:
            self.fChildListbox.setSelected( ndx, s )
            ndx=ndx+1
        if len(self.fChildren)>0 and not self.fChildListbox.isEnabled():
            self.fChildListbox.setEnabled( 1 )
            self.fChildConnectButton.setEnabled( 1 )
        if len(self.fChildren)<=0 and self.fChildListbox.isEnabled():
            self.fChildListbox.setEnabled( 0 )
            self.fChildConnectButton.setEnabled( 0 )

    def UpdateStateList( self ):
        self.fStates = []
        #print "UpdateStateList"
        states = self.QueryStates()
        #print "statestring: "+states
        if states=="" or states==None:
            return
        self.fStates = string.split( states, ";" )
        #print "Statelist:",self.fStates
        
    def UpdateCommandList( self ):
        stateList = self.fStates
        #print "Current state: "+self.fCurrentState
        #print "States:",stateList
        tmpCmds = []
        for s in stateList:
            tmpList1 = string.split( s, ":" )
            tmpList = []
            for s in tmpList1:
                if len(tmpList)>0 and len(tmpList[-1])>0 and tmpList[-1][-1]=="\\":
                    tmpList[-1] = tmpList[-1][:-1]+":"+s
                else:
                    tmpList.append( s )
            # print "State: "+tmpList[0]
            if len(tmpList)>=2:
                if re.search(tmpList[0], self.fCurrentState )!=None:
                    # print "State found"
                    tmpCmds = string.split( tmpList[1], "," )
                    # print "Commands: "+tmpList[1]
                    break

        ok=1
        if len(tmpCmds)!=len(self.fCommands):
            ok=0
        else:
            for i in range( 0, len(self.fCommands) ):
                if self.fCommands[i]!=tmpCmds[i]:
                    ok=0

        #if ok:
        #    return
        self.fCommands = tmpCmds
        sels = []
        ndx = 0
        cnt=self.fCommandListbox.count()
        for cmd in self.fCommands:
            if ndx <cnt:
                sel = self.fCommandListbox.isSelected( ndx )
            else:
                sel = 0
            if sel:
                sels.append( 1 )
            else:
                sels.append( 0 )
            ndx=ndx+1
        ndx = 0
        for cmd in self.fCommands:
            if ndx <cnt:
                self.fCommandListbox.changeItem( cmd, ndx )
            else:
                self.fCommandListbox.insertItem( cmd )
            ndx = ndx + 1
        while ndx < cnt:
            self.fCommandListbox.removeItem( ndx )
            cnt=cnt-1
        ndx=0
        for s in sels:
            self.fCommandListbox.setSelected( ndx, s )
            ndx=ndx+1

        ##self.fCommandListbox.clear()
        ###self.fCommandListbox.setEnabled( 1 )
        ##for c in self.fCommands:
        ##    self.fCommandListbox.insertItem( c )
        if len(self.fCommands)>0 and not self.fCommandListbox.isEnabled():
            self.fCommandListbox.setEnabled( 1 )
        if len(self.fCommands)<=0 and self.fCommandListbox.isEnabled():
            self.fCommandListbox.setEnabled( 0 )

    def TimerExpired(self):
        if self.fAddress=="" or self.fAddress==None or not self.fInitialized:
            return
        self.QueryState()
        self.UpdateCommandList()
        self.UpdateChildList()
        self.fTimer.start( self.fPollInterval, 1 )

    def ChildSelected(self):
        if self.fAddress=="" or self.fAddress==None or not self.fInitialized:
            return
        childNdx = self.fChildListbox.currentItem()
        if childNdx==-1:
            return
        #print "Child Index: "+str(childNdx)
        #print "Length of Children: "+str(len(self.fChildren))
        #if self.fChildren[childNdx].fType=="slaveprogram" or self.fChildren[childNdx].fType=="slave":
        self.fChildConnectButton.setEnabled( 1 )
        #else:
        #    self.fChildConnectButton.setEnabled( 0 )

    def CmdSelected(self):
        cmdNdx = self.fCommandListbox.currentItem()
        if cmdNdx==-1:
            return
        cmd = self.fCommands[ cmdNdx ];
        self.SendCmd( cmd )

    
    def SetPollInterval(self):
        self.fPollInterval = self.fPollIntervalSpinbox.value()*1000
        try:
            self.fTimer.changeInterval( self.fPollInterval )
        except:
            pass

    def SendRunParametersChanged(self):
        if self.fSendRunParameters.isChecked():
            enabled=1
        else:
            enabled=0
        self.fRunNumber.setEnabled(enabled)
        self.fBeamType.setEnabled(enabled)
        self.fHLTMode.setEnabled(enabled)
        self.fRunType.setEnabled(enabled)
        self.fInputDDLListEntry.setEnabled(enabled)
        self.fOutputDDLListEntry.setEnabled(enabled)
        self.fCTPTriggerClassEntry.setEnabled(enabled)

    def DoConnect( self ):
        try:
            TMControlInterface.Connect( self.fAddressStack[-1] )
        except SystemError, e:
            self.fCurrentState = ""
            self.fStatusLabel.setText( "" )
            self.fMessageLabel.setText( "Unable to connect: "+str(e) )
            return
        self.QueryState()
        self.UpdateCommandList()
        #self.UpdateChildList()

    def PopConnection( self ):
        if self.fAddressChildID!=None:
            self.fAddressChildID = None
        else:
            self.fAddressStack = self.fAddressStack[:-1]
            self.DoConnect()
        
    def ConnectToAddress( self, address ):
        self.fAddressStack.append( address )
        self.DoConnect()

    def DoDisconnect( self ):
        try:
            TMControlInterface.Disconnect( self.fAddressStack[-1] )
        except:
            pass
        self.fTimer.stop()
        self.fStatusLabel.setText( "" )
        self.fMessageLabel.setText( "" )
        self.fChildListbox.clear()

    def QueryState( self ):
        #print "QueryState:",self.fAddressChildID
        try:
            if self.fAddressChildID!=None:
                children = TMControlInterface.QueryChildren( self.fAddressStack[-1] )
                #print "QueryState children:",children
                self.fMessageLabel.setText( "" )
                childProcessList = string.split( children, "  " )
                state = "UNKNOWN"
                for c in childProcessList:
                    #print c
                    childProcess = string.split( c )
                    #print childProcess,self.fAddressChildID
                    if childProcess[0]==self.fAddressChildID:
                        if len(childProcess)>=4:
                            #print "Found:",childProcess[3]
                            state = childProcess[3]
                        else:
                            state = "UNKNOWN"
            else:
                state = TMControlInterface.QueryState( self.fAddressStack[-1] )
            self.fCurrentState = state
            self.fStatusLabel.setText( state )
            self.fMessageLabel.setText( "" )
        except SystemError, e:
            self.fCurrentState = ""
            self.fStatusLabel.setText( "" )
            self.fMessageLabel.setText( "Unable to query state: "+str(e) )
            return

    def QueryChildren( self ):
        if self.fAddressChildID!=None:
            #self.fChildListbox.setEnabled( 0 )
            self.fMessageLabel.setText( "" )
            return ""
        try:
            children = TMControlInterface.QueryChildren( self.fAddressStack[-1] )
            #self.fChildListbox.setEnabled( 1 )
            self.fMessageLabel.setText( "" )
            return children
        except SystemError, e:
            self.fMessageLabel.setText( "Unable to query children: "+str(e) )
            #self.fChildListbox.setEnabled( 0 )
            return ""

    def QueryStates( self ):
        if self.fAddressChildID!=None:
            try:
                #print "QueryStates - child states"
                states = TMControlInterface.QueryChildStates( self.fAddressStack[-1], self.fAddressChildID )
                #print "QueryStates - child states:",states
                self.fMessageLabel.setText( "" )
                return states
            except SystemError, e:
                self.fMessageLabel.setText( "Unable to query child "+self.fAddressChildID+" states: "+str(e) )
        try:
            states = TMControlInterface.QueryStates( self.fAddressStack[-1] )
            self.fMessageLabel.setText( "" )
            return states
        except SystemError, e:
            self.fMessageLabel.setText( "Unable to query states: "+str(e) )

    def SendCmd( self, cmd ):
        if cmd=="start" and self.fSendRunParameters.isChecked():
            self.fInDDLList = self.fInputDDLListEntry.text()
            self.fOutDDLList = self.fOutputDDLListEntry.text()
            self.fCTPTriggerClasses = self.fCTPTriggerClassEntry.text()
            cmd="start;BEAM_TYPE=%s;HLT_IN_DDL_LIST=%s;HLT_OUT_DDL_LIST=%s;RUN_TYPE=%s;CTP_TRIGGER_CLASS=%s" % ( self.fBeamType.currentText(), self.fInDDLList, self.fOutDDLList, self.fRunType.text(), self.fCTPTriggerClasses )
            if not self.fRunManager:
                #if self.fLastRunNumber==self.fRunNumber.value():
                    #self.fRunNumber.setValue( self.fRunNumber.value()+1 )
                self.fLastRunNumber = self.fRunNumber.value()
                cmd+=";RUN_NUMBER=%d;HLT_MODE=%s" % ( self.fRunNumber.value(), self.fHLTMode.currentText() )
                if self.fReadoutListVersion!=None:
                    cmd+=";DATA_FORMAT_VERSION=%d" % self.fReadoutListVersion
        if cmd=="connect" and self.fSendRunParameters.isChecked() and self.fRunManager:
            #if self.fLastRunNumber==self.fRunNumber.value():
                #self.fRunNumber.setValue( self.fRunNumber.value()+1 )
            self.fLastRunNumber = self.fRunNumber.value()
            cmd="connect;RUN_NUMBER=%d;HLT_MODE=%s" % ( self.fRunNumber.value(), self.fHLTMode.currentText() )
        try:
            TMControlInterface.SendCmd( self.fAddressStack[-1], cmd )
            return
        except SystemError, e:
            self.fMessageLabel.setText( "Error sending command '"+cmd+"': "+str(e) )


if __name__ == '__main__':
    app = QApplication(sys.argv)

    childAddress = None
    startDisconnected = 0
    if len(sys.argv)<=1 or len(sys.argv)>4 or (len(sys.argv)==4 and sys.argv[3]!="-unconnected"):
        print "Usage: "+sys.argv[0]+" <own-address> (<remote-address>) (-unconnected)"
        sys.exit( -1 )
    address = sys.argv[1]
    print "Binding to local address "+address
    if len(sys.argv)>2:
        childAddress = sys.argv[2]
    if len(sys.argv)>3:
        startDisconnected = 1

    QObject.connect(app, SIGNAL('lastWindowClosed()'), app, SLOT('quit()'))

    win = TMGUI1( app, address, childAddress, startDisconnected )
    app.setMainWidget(win)
    win.show()
    app.exec_loop()

        
        
