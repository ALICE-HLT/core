#!/usr/bin/env python


import string, sys
import TMControlInterface
import XMLRead


def UsageErrorExit( str=None ):
    print "Usage: "+sys.argv[0]+" -address <local-address> ([ -control <remote-address> | -config <TaskManager-config-file> ]) [ -query state | -cmd <command> ]"
    if str != None:
        print str
    sys.exit( -1 )

def GetChildAddressFromConfigFile( configFile ):
    reader = XMLRead.XMLReadHelper()
    doc = reader.ReadXMLTree( configFile )
    taskNode = reader.GetChildNode( doc, "Task" )
    slaveControNode = reader.GetChildNode( taskNode, "SlaveControlConfig" )
    slaveAddressNode = reader.GetChildNode( slaveControNode, "Address" )
    childAddress = reader.GetNodeText( slaveAddressNode )
    return childAddress
    

if __name__ == '__main__':
    if len(sys.argv)<=3:
        UsageErrorExit()
        
    adress = None
    childAddress = None

    query = ""
    cmd = ""

    argList = sys.argv[1:]
    while len(argList)>0:
        if argList[0] == "-address":
            if len(argList)<2:
                UsageErrorExit( "Missing argument to -address." )
            address = argList[1]
            argList = argList[2:]
        elif argList[0] == "-control":
            if len(argList)<2:
                UsageErrorExit( "Missing argument to -control." )
            childAddress = argList[1]
            argList = argList[2:]
        elif argList[0] == "-config":
            if len(argList)<2:
                UsageErrorExit( "Missing argument to -config." )
            configFile = argList[1]
            argList = argList[2:]
        elif argList[0] == "-query":
            if len(argList)<2:
                UsageErrorExit( "Missing argument to -query." )
            query = argList[1]
            argList = argList[2:]
        elif argList[0] == "-cmd":
            if len(argList)<2:
                UsageErrorExit( "Missing argument to -cmd." )
            cmd = argList[1]
            argList = argList[2:]
        else:
            UsageErrorExit( "Unknown parameter "+argList[0]+"." )

    if address == None:
        UsageErrorExit( "Local address has to be specified." )
    if childAddress!=None and configFile!=None:
        UsageErrorExit( "Only one of -control or -config can be specified." )
    if query!="" and cmd!="":
        UsageErrorExit( "Only one of -query or -cmd can be specified." )
    if query=="" and cmd=="":
        UsageErrorExit( "One of -query or -cmd has to be specified." )
    if query!="":
        if query=="state":
            pass
        else:
            UsageErrorExit( "Only 'state' can be queried." )

    if configFile:
        childAddress = GetChildAddressFromConfigFile( configFile )
    
    try:
        print "Binding to local address: "+address
        TMControlInterface.Init( address )
    except SystemError, e:
        print "Unable to initialize control interface: "+str(e)
        sys.exit( -1 )
    print "Using remote address: "+childAddress
    if query!="":
        print "Querying "+query
    else:
        print "Sending command '"+cmd+"'"

    if query!="":
        if query=="state":
            try:
                state = TMControlInterface.QueryState( childAddress )
                print state
            except SystemError, e:
                pass

    else:
        try:
            TMControlInterface.SendCmd( childAddress, cmd )
        except SystemError, e:
            print "Error sending command '"+cmd+"': "+str(e)
        
    
    try:
        TMControlInterface.Terminate()
    except SystemError:
        pass

    
