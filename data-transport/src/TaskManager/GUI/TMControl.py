#!/usr/bin/env python

import TMControlInterface
import time, sys, string

class TMControl:

    def __init__( self, local_address, remote_address ):
        self.fLocalAddress = local_address
        self.fRemoteAddress = remote_address
        self.fSleepTime = 0.5
        self.fQuiet = True
        self.fCmdVars = {}

    def SetSleepTime( self, st ):
        self.fSleepTime = st

    def SetQuiet( self, quiet ):
        self.fQuiet = quiet

    def SetCommandVariables( self, variables ):
        self.fCmdVars = variables

    def Init( self ):
        TMControlInterface.Init( self.fLocalAddress )

    def Deinit( self ):
        TMControlInterface.Terminate()

    def SendCmd( self, cmd ):
        TMControlInterface.SendCmd( self.fRemoteAddress, cmd )

    def SendCmdChecked( self, cmd ):
        try:
            return self.SendCmd( cmd )
        except SystemError, e:
            return None

    def QueryState( self ):
        return TMControlInterface.QueryState( self.fRemoteAddress )

    def QueryStateChecked( self ):
        state = "" # "TM:DEAD"
        try:
            state = self.QueryState()
        except SystemError, e:
            if str(e)=="Connection timed out":
                return "TIMEOUT"
            print "'"+str(e)+"'"
            print e
            pass
        return state

    # [ is_state, cmd, max_count ]

    def Control( self, state_list, end_states, max_unknown_state_cnt ):
        states = state_list[:]
        for sl in states:
            sl.append(0)
            sl.append(0)
        state = self.QueryStateChecked()
        not_found = 0
        last_state=""
        cmdTime=None
        stateReachedTime=None
        while not state in end_states:
            #print "state: "+state
            state_changed=False
            if last_state != state:
                print last_state , "->" , state,
                state_changed=True
                stateReachedTime = time.time()
                if cmdTime!=None:
                    td = stateReachedTime-cmdTime
                    print "( %f s )" % td
                else:
                    print
            last_state = state
            not_found=not_found+1
            for sl in states:
                if state==sl[0]:
                    #print "found state:",sl
                    if state_changed:
                        sl[4]=0
                    not_found=0
                    if sl[2]>0 and sl[2]<=sl[4]:
                        return state
                    if sl[3]>0 and sl[3]<=sl[5]:
                        return state
                    #print "sending cmd "+sl[1]
                    if sl[1]!=None:
                        cmd = string.Template( sl[1] ).safe_substitute( self.fCmdVars )
                        self.SendCmdChecked( cmd )
                        cmdTime = time.time()
                    sl[4]=sl[4]+1
                    sl[5]=sl[5]+1
            if max_unknown_state_cnt>=0 and not_found>max_unknown_state_cnt:
                return state
            time.sleep( self.fSleepTime )
            state = self.QueryStateChecked()
        if last_state != state:
            print last_state , "->" , state,
            stateReachedTime = time.time()
            if cmdTime!=None:
                td = stateReachedTime-cmdTime
                print "( %f s )" % td
            else:
                print
        return state

    
        
        
