#!/usr/bin/env python


import string, sys
from qt import *
from TMGUI1Form import TMGUI1Form
from TMGUI1 import TMGUI1
import TMControlInterface
import libxml2


def UsageErrorExit( str=None ):
    print "Usage: "+sys.argv[0]+" -address <local-address> ([ -control <remote-address> | -config <TaskManager-config-file> ]) (-unconnected) (-sendrunparameters) (-runnumberfile <run-number-file>) (-inddllist <input-ddl-list>) (-outddllist <output-ddl-list>) (-runmanagercontrol) (-ctptriggerclasses <ctp-trigger-class-string>) (-readoutlistversion <readout-list-version>)"
    if str != None:
        print str
    sys.exit( -1 )

def GetChildAddressFromConfigFile( configFile ):
    xmlFile = libxml2.parseFile(configFile)
    xpathContext = xmlFile.xpathNewContext()
    return xpathContext.xpathEval("/Task/SlaveControlConfig/TargetAddress")[0].content
    

if __name__ == '__main__':
    app = QApplication(sys.argv)
    

    #if len(sys.argv)<=1 or len(sys.argv)>4 or (len(sys.argv)==4 and sys.argv[3]!="-unconnected"):
    #    print "Usage: "+sys.argv[0]+" -address <own-address> ([ -control <remote-address> | -config <TaskManager-config-file> ]) (-unconnected)"
    #    sys.exit( -1 )
    if len(sys.argv)<=3:
        UsageErrorExit()

    adress = None
    childAddress = None
    configFile = None
    startDisconnected = 0
    runNumberFile = ""
    sendRunParameters=False
    hltInDDLList=""
    hltOutDDLList=""
    hltCTPTCs=""
    runManagerControl=False
    readoutListVersion=None

    argList = sys.argv[1:]
    while len(argList)>0:
        if argList[0] == "-address":
            if len(argList)<2:
                UsageErrorExit( "Missing argument to -address." )
            address = argList[1]
            argList = argList[2:]
        elif argList[0] == "-control":
            if len(argList)<2:
                UsageErrorExit( "Missing argument to -control." )
            childAddress = argList[1]
            argList = argList[2:]
        elif argList[0] == "-config":
            if len(argList)<2:
                UsageErrorExit( "Missing argument to -config." )
            configFile = argList[1]
            argList = argList[2:]
            try:
                infile=open(configFile,"r")
            except IOError:
                UsageErrorExit( "Cannot open input file "+configFile+"." )
            infile.close()
        elif argList[0] == "-unconnected":
            startDisconnected = 1
            argList = argList[1:]
        elif argList[0] == "-sendrunparameters":
            sendRunParameters = 1
            argList = argList[1:]
        elif argList[0] == "-runnumberfile":
            if len(argList)<2:
                UsageErrorExit( "Missing argument to -runnumberfile." )
            runNumberFile = argList[1]
            argList = argList[2:]
            try:
                infile=open(runNumberFile,"r")
                line=infile.readline()
                #print "Line:",line
                runNumber = int(line)
                #print "runNumber:",runNumber
            except IOError:
                UsageErrorExit( "Cannot read from run number file "+runNumberFile+"." )
            except ValueError:
                UsageErrorExit( "Content of run number file "+runNumberFile+" is not a number." )
            infile.close()
        elif argList[0] == "-inddllist":
            if len(argList)<2:
                UsageErrorExit( "Missing argument to -inddllist." )
            hltInDDLList = argList[1]
            argList = argList[2:]
        elif argList[0] == "-outddllist":
            if len(argList)<2:
                UsageErrorExit( "Missing argument to -outddllist." )
            hltOutDDLList = argList[1]
            argList = argList[2:]
        elif argList[0] == "-ctptriggerclasses":
            if len(argList)<2:
                UsageErrorExit( "Missing argument to -ctptriggerclasses." )
            hltCTPTCs = argList[1]
            argList = argList[2:]
        elif argList[0] == "-readoutlistversion":
            if len(argList)<2:
                UsageErrorExit( "Missing argument to -readoutlistversion." )
            try:
                readoutListVersion=int(argList[1])
            except ValueError:
                UsageErrorExit( "Cannot convert readout list version specifier'"+argList[1]+"' to integer." )
            if readoutListVersion<1 or readoutListVersion>3:
                UsageErrorExit( "Only values between 1 and 3 allowed for readout list version specifier." )
            argList = argList[2:]
        elif argList[0]=="-runmanagercontrol":
            runManagerControl=True
            argList = argList[1:]
        else:
            UsageErrorExit( "Unknown parameter "+argList[0]+"." )

    if address == None:
        UsageErrorExit( "Local address has to be specified." )
    if childAddress!=None and configFile!=None:
        UsageErrorExit( "Only one of -control or -config can be specified." )

    if configFile:
        childAddress = GetChildAddressFromConfigFile( configFile )

    
    print "Binding to local address: "+address
    print "Using remote address: "+childAddress
    if startDisconnected:
        print "Starting disconnected"
    else:
        print "Attempting to start connected"

    QObject.connect(app, SIGNAL('lastWindowClosed()'), app, SLOT('quit()'))

    win = TMGUI1( app, address, childAddress, startDisconnected )
    if runNumberFile!="":
        #print "Setting runNumber:",runNumber
        win.SetRunNumber( runNumber )
    if sendRunParameters:
        win.SendRunParameters()
    win.SetInDDLList(hltInDDLList)
    win.SetOutDDLList(hltOutDDLList)
    win.SetCTPTriggerClasses( hltCTPTCs )
    if runManagerControl:
        win.RunManager( runManagerControl )
    if readoutListVersion!=None:
        win.SetReadoutListVersion( readoutListVersion )
    app.setMainWidget(win)
    win.show()
    app.exec_loop()

    if runNumberFile!="":
        runNumber = win.GetLastRunNumber()
        try:
            outfile=open(runNumberFile,"w")
            outfile.write( "%d\n" % runNumber )
            outfile.close()
        except IOError:
            sys.stderr.write( "Cannot write run number to file "+runNumberFile+".\n" )
            sys.exit(-1)
        

        


