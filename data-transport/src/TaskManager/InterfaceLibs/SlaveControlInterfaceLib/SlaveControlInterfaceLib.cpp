/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <InterfaceLib.h>
#include "TMControlMaster.hpp"
#include "TMSysException.hpp"
#include <MLUCLog.hpp>
#include <cerrno>
#include <cstdlib>

/* Startup, initialize any static structures needed by the library. */
int Initialize( void** private_lib_data, const char* listen_address )
    {
    TMControlMaster* master;
    master = new TMControlMaster();
    if ( master )
	{
	master->SetAddress( listen_address );
	*private_lib_data = (void*)master;
	return 0;
	}
    return ENOMEM;
    }

/* End, cleanup of library data */
int Terminate( void* private_lib_data )
    {
    if ( private_lib_data )
	{
	delete (TMControlMaster*)private_lib_data;
	return 0;
	}
    return EFAULT;
    }

/* Convert a given address string into a more compact binary
   representation for immediate usability.
   This can also be a copy of the given string if this is the most directly
   useful representation.
   The returned binary address will be released by a call to 
   ReleaseResource (below) */
int ConvertAddress( void* private_lib_data, const char* process_address_str, void** process_address_bin )
    {
    if ( private_lib_data )
	{
	try
	    {
	    ((TMControlMaster*)private_lib_data)->ConvertAddress( process_address_str, process_address_bin );
	    }
	catch ( TMSysException e )
	    {
	    LOG( MLUCLog::kError, "SlaveControlInterfaceLib ConvertAddress", "Error converting address" )
		<< "Error converting address '" << process_address_str
		<< "': " << e.GetErrorStr() << " (" << MLUCLog::kDec << e.GetErrno()
		<< ") " << e.GetDescription() << " (" << e.GetFile() << "/" << e.GetLine()
		<< ")." << ENDLOG;
	    return e.GetErrno();
	    }
	return 0;
	}
    return EFAULT;
    }

/*
  Compare two given binary address representations. 
  Return 0 if they are equal, -1 if addr1 is less than addr2, 
  +1 if addr1 is greater than addr2. If the magnitude comparisons
  do not apply return unequal 0 for inequality. */
int CompareAddresses( void* private_lib_data, void* addr1, void* addr2 )
    {
    if ( private_lib_data )
	{
	int ret;
	try
	    {
	    ret = ((TMControlMaster*)private_lib_data)->CompareAddresses( addr1, addr2 );
	    }
	catch ( TMSysException e )
	    {
	    LOG( MLUCLog::kError, "SlaveControlInterfaceLib CompareAddresses", "Error comparing addresses" )
		<< "Error comparing addresses: " << e.GetErrorStr() << " (" << MLUCLog::kDec << e.GetErrno()
		<< ") " << e.GetDescription() << " (" << e.GetFile() << "/" << e.GetLine()
		<< ")." << ENDLOG;
	    return -2;
	    }
	return ret;
	}
    return EFAULT;
    }

/* Query the state of a specific process 
** state is allocated as a string in the function and
** has to be released via ReleaseResource (below) */
int QueryState( void* private_lib_data,
                pid_t, 
                const char* process_id, 
                void* process_address, 
                char** state )
    {
    if ( private_lib_data )
	{
	MLUCString mystate;
	bool logErrors = !((TMControlMaster*)private_lib_data)->SuppressComErrorLogs( process_address );
	try
	    {
	    ((TMControlMaster*)private_lib_data)->QueryState( process_address, mystate );
	    }
	catch ( TMSysException e )
	    {
	    if ( logErrors )
		{
		LOG( MLUCLog::kError, "SlaveControlInterfaceLib QueryState", "Error querying slave state" )
		    << "Error querying slave '" << process_id << "'  state: " 
		    << e.GetErrorStr() << " (" << MLUCLog::kDec << e.GetErrno()
		    << ") " << e.GetDescription() << " (" << e.GetFile() << "/" << e.GetLine()
		    << ")." << ENDLOG;
		}
	    return e.GetErrno();
	    }
	*state = (char*)malloc( mystate.Size()+1 );
	if ( !*state )
	    return ENOMEM;
	strcpy( *state, mystate.c_str() );
	return 0;
	}
    return EFAULT;
    }

/* Obtain additional status data from a
** specific process.
** stat_data is allocated as a string in the 
*+ function and has to be released via 
** ReleaseResource (below)
*/
int QueryStatusData( void* private_lib_data,
		     pid_t,
		     const char* process_id, 
		     void* process_address, 
		     char** stat_data )
    {
    if ( private_lib_data )
	{
	MLUCString mystatus;
	bool logErrors = !((TMControlMaster*)private_lib_data)->SuppressComErrorLogs( process_address );
	try
	    {
	    ((TMControlMaster*)private_lib_data)->QueryStatusData( process_address, mystatus );
	    }
	catch ( TMSysException e )
	    {
	    if ( logErrors )
		{
		LOG( MLUCLog::kError, "SlaveControlInterfaceLib QueryStatusData", "Error querying slave status data" )
		    << "Error querying slave '" << process_id << "' status data: " 
		    << e.GetErrorStr() << " (" << MLUCLog::kDec << e.GetErrno()
		    << ") " << e.GetDescription() << " (" << e.GetFile() << "/" << e.GetLine()
		    << ")." << ENDLOG;
		}
	    return e.GetErrno();
	    }
	*stat_data = (char*)malloc( mystatus.Size()+1 );
	if ( !*stat_data )
	    return ENOMEM;
	strcpy( *stat_data, mystatus.c_str() );
	return 0;
	}
    return EFAULT;
    }

void ReleaseResource( void*, void* resource )
    {
    free( resource );
    }

/* Send the specified command to the given process */
int SendCommand( void* private_lib_data,
                 pid_t, 
                 const char* process_id, 
                 void* process_address, 
                 const char* command )
    {
    if ( private_lib_data )
	{
	bool logErrors = !((TMControlMaster*)private_lib_data)->SuppressComErrorLogs( process_address );
	try
	    {
	    ((TMControlMaster*)private_lib_data)->SendCmd( process_address, command );
	    }
	catch ( TMSysException e )
	    {
	    if ( logErrors )
		{
		LOG( MLUCLog::kError, "SlaveControlInterfaceLib SendCommand", "Error sending command" )
		    << "Error sending command to slave '" << process_id << "' : " 
		    << e.GetErrorStr() << " (" << MLUCLog::kDec << e.GetErrno()
		    << ") " << e.GetDescription() << " (" << e.GetFile() << "/" << e.GetLine()
		    << ")." << ENDLOG;
		}
	    return e.GetErrno();
	    }
	return 0;
	}
    return EFAULT;
    }

/* Enter a wait that calls the specified callback when a process 
** has triggered an interrupt  (LAM).
** The address of the triggering process is passed in the call 
** to the specified callback function.
** This is called in its own thread to run as an endless loop
** and only returns when the StopInterruptWait function
** (see below) is called.. */
int InterruptWait( void* private_lib_data,
                   void* opaque_arg, 
		   const char* listen_address, 
                   InterruptTriggerCallback_t callback )
    {
    if ( private_lib_data )
	{
	try
	    {
	    ((TMControlMaster*)private_lib_data)->SetInterruptCallback( callback );
	    ((TMControlMaster*)private_lib_data)->SetArg( opaque_arg );
	    ((TMControlMaster*)private_lib_data)->RunLoop();
	    }
	catch ( TMSysException e )
	    {
	    LOG( MLUCLog::kError, "SlaveControlInterfaceLib InterruptWait", "Error in interrupt wait loop" )
		<< "Error in interrupt wait loop for listen address '" 
		<< listen_address << "': : " << e.GetErrorStr() << " (" 
		<< MLUCLog::kDec << e.GetErrno()
		<< ") " << e.GetDescription() << " (" << e.GetFile() << "/" << e.GetLine()
		<< ")." << ENDLOG;
	    return e.GetErrno();
	    }
	return 0;
	}
    return EFAULT;
    }

/* Stop the interrupt wait loop gracefully. */
int StopInterruptWait( void* private_lib_data )
    {
    if ( private_lib_data )
	{
	((TMControlMaster*)private_lib_data)->StopLoop();
	return 0;
	}
    return EFAULT;
    }

/* Connection function.
   Establish a permanent connection to the given process.
   This function is optional and must not be supplied in the interface
   library for compatibility reasons.
*/
int ConnectToProcess( void* private_lib_data,
		      pid_t, 
		      const char* process_id, 
		      void* process_address )
    {
    if ( private_lib_data )
	{
	bool logErrors = !((TMControlMaster*)private_lib_data)->SuppressComErrorLogs( process_address );
	try
	    {
	    ((TMControlMaster*)private_lib_data)->Connect( process_address );
	    }
	catch ( TMSysException e )
	    {
	    if ( logErrors )
		{
		LOG( MLUCLog::kError, "SlaveControlInterfaceLib SendCommand", "Error connecting" )
		    << "Error connecting to slave '" << process_id << "' : " 
		    << e.GetErrorStr() << " (" << MLUCLog::kDec << e.GetErrno()
		    << ") " << e.GetDescription() << " (" << e.GetFile() << "/" << e.GetLine()
		    << ")." << ENDLOG;
		}
	    return e.GetErrno();
	    }
	return 0;
	}
    return EFAULT;
    }

/* Disconnection function.
   Severe a previously established permanent connection to the given 
   process.
   This function is optional and must not be supplied in the interface
   library for compatibility reasons.
*/
int DisconnectFromProcess( void* private_lib_data,
			   pid_t, 
			   const char* process_id, 
			   void* process_address )
    {
    if ( private_lib_data )
	{
	bool logErrors = !((TMControlMaster*)private_lib_data)->SuppressComErrorLogs( process_address );
	try
	    {
	    ((TMControlMaster*)private_lib_data)->Disconnect( process_address );
	    }
	catch ( TMSysException e )
	    {
	    if ( logErrors )
		{
		LOG( MLUCLog::kError, "SlaveControlInterfaceLib SendCommand", "Error disconnecting" )
		    << "Error disconnecting from slave '" << process_id << "' : " 
		    << e.GetErrorStr() << " (" << MLUCLog::kDec << e.GetErrno()
		    << ") " << e.GetDescription() << " (" << e.GetFile() << "/" << e.GetLine()
		    << ")." << ENDLOG;
		}
	    return e.GetErrno();
	    }
	return 0;
	}
    return EFAULT;
    }


int SuppressComErrorLogs( void* private_lib_data,
			  pid_t /*pid*/, 
			  const char* /*process_id*/, 
			  void* process_address_bin,
			  unsigned long timeout_us )
    {
    ((TMControlMaster*)private_lib_data)->SuppressComErrorLogs( process_address_bin, timeout_us );
    return 0;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
