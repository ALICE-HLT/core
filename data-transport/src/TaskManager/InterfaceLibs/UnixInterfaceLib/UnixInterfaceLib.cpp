/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <InterfaceLib.h>
#include <string>
#include <cstdio>
#include <sys/types.h>
#include <signal.h>
#include <sstream>
#include <fstream>
#include <unistd.h>
#include <cerrno>
#include <cstdlib>
#include <cstring>


/* Startup, initialize any static structures needed by the library. */
int Initialize( void** private_lib_data, const char* )
    {
    *private_lib_data = malloc( sizeof(unsigned long) );
    return 0;
    }

/* End, cleanup of library data */
int Terminate( void* private_lib_data )
    {
    if ( private_lib_data )
	free( private_lib_data );
    return 0;
    }

/* Convert a given address string into a more compact binary
   representation for immediate usability.
   This can also be a copy of the given string if this is the most directly
   useful representation.
   The returned binary address will be released by a call to 
   ReleaseResource (below) */
int ConvertAddress( void*, const char* process_address_str, void** process_address_bin )
    {
    *process_address_bin = malloc( strlen(process_address_str)+1 );
    if ( *process_address_bin )
	{
	strcpy( (char*)(*process_address_bin), process_address_str );
	return 0;
	}
    else
	return ENOMEM;
    }

/*
  Compare two given binary address representations. 
  Return 0 if they are equal, -1 if addr1 is less than addr2, 
  +1 if addr1 is greater than addr2. If the magnitude comparisons
  do not apply return unequal 0 for inequality. */
int CompareAddresses( void*, void* addr1, void* addr2 )
    {
    return strcmp( (char*)addr1, (char*)addr2 );
    }

/* Query the state of a specific process 
** state is allocated as a string in the function and
** has to be released via ReleaseResource (below) */
int QueryState( void*,
                pid_t pid, 
                const char*, 
                void*, 
                char** state )
    {
    std::string name;
    char tmp[ 1024];
    name = "/proc/";
    sprintf( tmp, "%u", (unsigned)pid );
    name += tmp;
    name += "/stat";
    //printf( "Name: %s\n", name.c_str() );
    FILE* file;
    *state = (char*)malloc( 22 );
    file = fopen( name.c_str(), "r" );
    if ( !file )
	strcpy( *state, "DEAD" );
    else
	{
	int ret;
	char stat;
	int fpid;
	char proc[4096];
	ret = fscanf( file, "%d %s %c", &fpid, proc, &stat );
	fclose( file );
	//printf( "PID: %d - Proc.: %s - State: %c\n", fpid, proc, stat );
	switch ( stat )
	    {
	    case 'S':
		strcpy( *state, "sleeping" );
		break;
	    case 'R':
		strcpy( *state, "running" );
		break;
	    case 'Z':
		strcpy( *state, "zombie" );
		break;
	    case 'D':
		strcpy( *state, "uninterruptible sleep" );
		break;
	    case 'T':
		strcpy( *state, "traced" );
		break;
	    }    
	}
    return 0;
    }

/* Obtain additional status data from a
** specific process.
** stat_data is allocated as a string in the 
*+ function and has to be released via 
** ReleaseResource (below)
*/
int QueryStatusData( void*,
		     pid_t pid,
		     const char*, 
		     void*, 
		     char** stat_data )
    {
    std::string name;
    char tmp[ 1024];
    name = "/proc/";
    sprintf( tmp, "%u", (unsigned)pid );
    name += tmp;
    name += "/stat";
    
    std::ifstream fin( name.c_str() );
    if (!fin)
	return ENOENT;
    
    std::string line;
    std::string cont;
    while ( getline( fin, line ) )
	cont += line;
    *stat_data = (char*)malloc( cont.size()+1 );
    if ( !*stat_data )
	return ENOMEM;
    strcpy( *stat_data, cont.c_str() );
    return 0;
    }

void ReleaseResource( void*, void* resource )
    {
    if ( resource )
	free( resource );
    }

/* Send the specified command to the given process */
int SendCommand( void*,
                 pid_t pid, 
                 const char*, 
                 void*, 
                 const char* command )
    {
    if ( pid<=0 )
	return EINVAL;
    std::string cmd = command;
    if ( cmd=="NOP" )
	return 0;
    int sig=SIGTERM;
    if ( cmd=="SIGQUIT" )
	sig = SIGQUIT;
    else if ( cmd=="SIGKILL" )
	sig = SIGKILL;
    else if ( cmd=="SIGHUP" )
	sig = SIGHUP;
    else if ( cmd=="SIGINT" )
	sig = SIGINT;
    else if ( cmd=="SIGILL" )
	sig = SIGILL;
    else if ( cmd=="SIGABRT" )
	sig = SIGABRT;
    else if ( cmd=="SIGFPE" )
	sig = SIGFPE;
    else if ( cmd=="SIGSEGV" )
	sig = SIGSEGV;
    else if ( cmd=="SIGPIPE" )
	sig = SIGPIPE;
    else if ( cmd=="SIGALRM" )
	sig = SIGALRM;
    else if ( cmd=="SIGTERM" )
	sig = SIGTERM;
    else if ( cmd=="SIGUSR1" )
	sig = SIGUSR1;
    else if ( cmd=="SIGUSR2" )
	sig = SIGUSR2;
    else if ( cmd=="SIGCHLD" )
	sig = SIGCHLD;
    else if ( cmd=="SIGCONT" )
	sig = SIGCONT;
    else if ( cmd=="SIGSTOP" )
	sig = SIGSTOP;
    else if ( cmd=="SIGTSTP" )
	sig = SIGTSTP;
    else if ( cmd=="SIGTTIN" )
	sig = SIGTTIN;
    else if ( cmd=="SIGTTOU" )
	sig = SIGTTOU;
    else if ( cmd=="SIGBUS" )
	sig = SIGBUS;
    else if ( cmd=="SIGPOLL" )
	sig = SIGPOLL;
    else if ( cmd=="SIGPROF" )
	sig = SIGPROF;
    else if ( cmd=="SIGSYS" )
	sig = SIGSYS;
    else if ( cmd=="SIGTRAP" )
	sig = SIGTRAP;
    else if ( cmd=="SIGURG" )
	sig = SIGURG;
    else if ( cmd=="SIGVTALRM" )
	sig = SIGVTALRM;
    else if ( cmd=="SIGXCPU" )
	sig = SIGXCPU;
    else if ( cmd=="SIGXFSZ" )
	sig = SIGXFSZ;
    else if ( cmd=="SIGIOT" )
	sig = SIGIOT;
    else if ( cmd=="SIGSTKFLT" )
	sig = SIGSTKFLT;
    else if ( cmd=="SIGIO" )
	sig = SIGIO;
    else if ( cmd=="SIGCLD" )
	sig = SIGCLD;
    else if ( cmd=="SIGPWR" )
	sig = SIGPWR;
    else if ( cmd=="SIGWINCH" )
	sig = SIGWINCH;
    else if ( cmd=="SIGUNUSED" )
	sig = SIGUNUSED;
    kill( pid, sig );
    return 0;
    }

/* Enter a wait that calls the specified callback when a process 
** has triggered an interrupt  (LAM).
** The address of the triggering process is passed in the call 
** to the specified callback function.
** This is called in its own thread to run as an endless loop
** and only returns when the StopInterruptWait function
** (see below) is called.. */
int InterruptWait( void* private_lib_data,
                   void*, 
		   const char*, 
                   InterruptTriggerCallback_t )
    {
    *((unsigned long*)private_lib_data) = 1;
    while ( *((unsigned long*)private_lib_data) )
	usleep( 100000 );
    return 0;
    }

/* Stop the interrupt wait loop gracefully. */
int StopInterruptWait( void* private_lib_data )
    {
    *((unsigned long*)private_lib_data) = 0;
    return 0;
    }





/* Connection function.
   Establish a permanent connection to the given process.
   This function is optional and must not be supplied in the interface
   library for compatibility reasons.
*/
int ConnectToProcess( void*,
		      pid_t, 
		      const char*, 
		      void* )
    {
    return 0;
    }

/* Disconnection function.
   Severe a previously established permanent connection to the given 
   process.
   This function is optional and must not be supplied in the interface
   library for compatibility reasons.
*/
int DisconnectFromProcess( void*,
			   pid_t, 
			   const char*, 
			   void* )
    {
    return 0;
    }

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
