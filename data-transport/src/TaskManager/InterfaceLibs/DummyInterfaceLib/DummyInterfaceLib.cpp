/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: DummyInterfaceLib.cpp 824 2006-01-30 12:37:20Z timm $ 
**
***************************************************************************
*/

#include "MLUCString.hpp"
#include "MLUCLog.hpp"
#include <InterfaceLib.h>
#include <string>
#include <stdio.h>
#include <sys/types.h>
#include <signal.h>
#include <sstream>
#include <fstream>
#include <unistd.h>
#include <cerrno>
#include <cstdlib>

struct DummyInterfaceLibData
    {
	MLUCString fState;
	MLUCString fStatusData;
	unsigned int fLoop;
    };


/* Startup, initialize any static structures needed by the library. */
int Initialize( void** private_lib_data, const char* )
    {
    *private_lib_data = (void*)new DummyInterfaceLibData;
    if ( !(*private_lib_data) )
	return ENOMEM;
    DummyInterfaceLibData* ld = (DummyInterfaceLibData*)(*private_lib_data);
    ld->fLoop = 0;
    return 0;
    }

/* End, cleanup of library data */
int Terminate( void* private_lib_data )
    {
    if ( private_lib_data )
	delete (DummyInterfaceLibData*)private_lib_data;
    return 0;
    }

/* Convert a given address string into a more compact binary
   representation for immediate usability.
   This can also be a copy of the given string if this is the most directly
   useful representation.
   The returned binary address will be released by a call to 
   ReleaseResource (below) */
int ConvertAddress( void*, const char* process_address_str, void** process_address_bin )
    {
    *process_address_bin = malloc( strlen(process_address_str)+1 );
    if ( *process_address_bin )
	{
	strcpy( (char*)(*process_address_bin), process_address_str );
	return 0;
	}
    else
	return ENOMEM;
    }

/*
  Compare two given binary address representations. 
  Return 0 if they are equal, -1 if addr1 is less than addr2, 
  +1 if addr1 is greater than addr2. If the magnitude comparisons
  do not apply return unequal 0 for inequality. */
int CompareAddresses( void*, void* addr1, void* addr2 )
    {
    return strcmp( (char*)addr1, (char*)addr2 );
    }

/* Query the state of a specific process 
** state is allocated as a string in the function and
** has to be released via ReleaseResource (below) */
int QueryState( void* private_lib_data,
                pid_t, 
                const char*, 
                void*, 
                char** state )
    {
    DummyInterfaceLibData* ld = (DummyInterfaceLibData*)private_lib_data;
    *state = (char*)malloc( ld->fState.Length()+1 );
    if ( !*state )
	return ENOMEM;
    strcpy( *state, ld->fState.c_str() );
    return 0;
    }

/* Obtain additional status data from a
** specific process.
** stat_data is allocated as a string in the 
*+ function and has to be released via 
** ReleaseResource (below)
*/
int QueryStatusData( void* private_lib_data,
		     pid_t,
		     const char*, 
		     void*, 
		     char** stat_data )
    {
    DummyInterfaceLibData* ld = (DummyInterfaceLibData*)private_lib_data;
    *stat_data = (char*)malloc( ld->fStatusData.Length()+1 );
    if ( !*stat_data )
	return ENOMEM;
    strcpy( *stat_data, ld->fStatusData.c_str() );
    return 0;
    }

void ReleaseResource( void*, void* resource )
    {
    if ( resource )
	free( resource );
    }

/* Send the specified command to the given process */
int SendCommand( void* private_lib_data,
                 pid_t, 
                 const char*, 
                 void*, 
                 const char* command )
    {
    DummyInterfaceLibData* ld = (DummyInterfaceLibData*)private_lib_data;
    if ( !strncmp( command, "SETSTATE ", strlen("SETSTATE " ) ) )
	{
	LOG( MLUCLog::kDebug, "", "Setting State" )
	    << "Setting state to '" << command+strlen("SETSTATE " ) << "'." << ENDLOG;
	printf( "Setting state to '%s'.\n", command+strlen("SETSTATE " ) );
	ld->fState = command+strlen("SETSTATE " );
	return 0;
	}
    else if ( !strncmp( command, "SETSTATUSDATA ", strlen("SETSTATUSDATA " ) ) )
	{
	LOG( MLUCLog::kDebug, "", "Setting Status Data" )
	    << "Setting statusData to '" << command+strlen("SETSTATUSDATA " ) << "'." << ENDLOG;
	ld->fState = command+strlen("SETSTATUSDATA " );
	return 0;
	}
    return EINVAL;
    }

/* Enter a wait that calls the specified callback when a process 
** has triggered an interrupt  (LAM).
** The address of the triggering process is passed in the call 
** to the specified callback function.
** This is called in its own thread to run as an endless loop
** and only returns when the StopInterruptWait function
** (see below) is called.. */
int InterruptWait( void* private_lib_data,
                   void*, 
		   const char*, 
                   InterruptTriggerCallback_t )
    {
    DummyInterfaceLibData* ld = (DummyInterfaceLibData*)private_lib_data;
    ld->fLoop = 1;
    while ( ld->fLoop )
	usleep( 100000 );
    return 0;
    }

/* Stop the interrupt wait loop gracefully. */
int StopInterruptWait( void* private_lib_data )
    {
    DummyInterfaceLibData* ld = (DummyInterfaceLibData*)private_lib_data;
    ld->fLoop = 0;
    return 0;
    }





/* Connection function.
   Establish a permanent connection to the given process.
   This function is optional and must not be supplied in the interface
   library for compatibility reasons.
*/
int ConnectToProcess( void*,
		      pid_t, 
		      const char*, 
		      void* )
    {
    return 0;
    }

/* Disconnection function.
   Severe a previously established permanent connection to the given 
   process.
   This function is optional and must not be supplied in the interface
   library for compatibility reasons.
*/
int DisconnectFromProcess( void*,
			   pid_t, 
			   const char*, 
			   void* )
    {
    return 0;
    }

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
