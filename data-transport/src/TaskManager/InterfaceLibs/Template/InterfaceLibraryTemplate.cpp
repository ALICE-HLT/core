/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <InterfaceLib.h>
#include <stdlib.h>

/* Startup, initialize any static structures needed by the library. */
int Initialize( void** /*private_lib_data*/, const char* /*interrupt_listen_address*/ )
    {
    return 0;
    }

/* End, cleanup of library data */
int Terminate( void* /*private_lib_data*/ )
    {
    return 0;
    }

/* Convert a given address string into a more compact binary
   representation for immediate usability.
   This can also be a copy of the given string if this is the most directly
   useful representation.
   The returned binary address will be released by a call to 
   ReleaseResource (below) */
int ConvertAddress( void* /*private_lib_data*/, const char* /*process_address_str*/, void** /*process_address_bin*/ )
    {
    return 0;
    }

/*
  Compare two given binary address representations. 
  Return 0 if they are equal, -1 if addr1 is less than addr2, 
  +1 if addr1 is greater than addr2. If the magnitude comparisons
  do not apply return unequal 0 for inequality. */
int CompareAddresses( void* /*private_lib_data*/, void* /*addr1*/, void* /*addr2*/ )
    {
    return 1;
    }

/* Query the state of a specific process 
** state is allocated as a string in the function and
** has to be released via ReleaseResource (below) */
int QueryState( void* /*private_lib_data*/,
                pid_t /*pid*/, 
                const char* /*process_id*/, 
                void* /*process_address*/, 
                char** /*state*/ )
    {
    return 0;
    }

/* Obtain additional status data from a
** specific process.
** stat_data is allocated as a string in the 
*+ function and has to be released via 
** ReleaseResource (below)
*/
int QueryStatusData( void* /*private_lib_data*/,
		     pid_t /*pid*/,
		     const char* /*process_id*/, 
		     void* /*process_address*/, 
		     char** /*stat_data*/ )
    {
    return 0;
    }

void ReleaseResource( void* /*private_lib_data*/, void* /*resource*/ )
    {
    }

/* Send the specified command to the given process */
int SendCommand( void* /*private_lib_data*/,
                 pid_t /*pid*/, 
                 const char* /*process_id*/, 
                 void* /*process_address*/, 
                 const char* /*command*/ )
    {
    return 0;
    }

/* Enter a wait that calls the specified callback when a process 
** has triggered an interrupt  (LAM).
** The address of the triggering process is passed in the call 
** to the specified callback function.
** This is called in its own thread to run as an endless loop
** and only returns when the StopInterruptWait function
** (see below) is called.. */
int InterruptWait( void* /*private_lib_data*/,
                   void* /*opaque_arg*/, 
		   const char* /*listen_address*/, 
                   InterruptTriggerCallback_t /*callback*/ )
    {
    return 0;
    }

/* Stop the interrupt wait loop gracefully. */
int StopInterruptWait( void* /*private_lib_data*/ )
    {
    return 0;
    }



/* Connection function.
   Establish a permanent connection to the given process.
   This function is optional and must not be supplied in the interface
   library for compatibility reasons.
*/
int ConnectToProcess( void* /*private_lib_data*/,
		      pid_t /*pid*/, 
		      const char* /*process_id*/, 
		      void* /*process_address*/ )
    {
    return 0;
    }

/* Disconnection function.
   Severe a previously established permanent connection to the given 
   process.
   This function is optional and must not be supplied in the interface
   library for compatibility reasons.
*/
int DisconnectFromProcess( void* /*private_lib_data*/,
			   pid_t /*pid*/, 
			   const char* /*process_id*/, 
			   void* /*process_address*/ )
    {
    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
