/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TMControlMaster.hpp"
#include "TMSysException.hpp"
#include "TMControlMessage.hpp"
#include "TMControlMessageTypes.hpp"
#include "TMDefinitions.hpp"
#include <BCLURLAddressDecoder.hpp>
#include <MLUCLog.hpp>
#include <cerrno>
#include <cstring>
#include <cstdlib>

TMControlMaster::TMControlMaster()
    {
    pthread_mutex_init( &fComSendMutex, NULL );
//     pthread_mutex_init( &fReplyMutex, NULL );
    pthread_mutex_init( &fComErrorLogSuppressionsMutex, NULL );
    fCom = NULL;
    fLocalAddress = NULL;
    fReceiveAddress = NULL;
    fLastReplyID = 0;
    fRunLoop = true;
    fLoopRunning = false;
    fArg = NULL;
    fCallback = NULL;
    fReplySignal.Unlock();
    gettimeofday( &fLastThoroughReplyCleanup, NULL );
    }

TMControlMaster::~TMControlMaster()
    {
    pthread_mutex_destroy( &fComSendMutex );
//     pthread_mutex_destroy( &fReplyMutex );
    }

void TMControlMaster::SetAddress( const char* addr )
    {
    int ret;
    bool isBlob;
    BCLCommunication* tmpCom;
    ret = BCLDecodeLocalURL( addr, tmpCom, fLocalAddress, isBlob );
    if ( ret )
	{
	LOG( MLUCLog::kError, "TMControlMaster::SetAddress", "Unable to decode address" )
	    << "Unable to decode address '" << addr << "': "
	    << strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
	    << ENDLOG;
	throw TMSysException( ret, strerror(ret), "Unable to decode address",
			      __FILE__, __LINE__ );
	}
    if ( isBlob )
	{
	LOG( MLUCLog::kError, "TMControlMaster::SetAddress", "Blob address not allowed" )
	    << "Blob address not allowed. Message address is required." 
	    << ENDLOG;
	throw TMSysException( EINVAL, strerror(EINVAL), "Blob address not allowed",
			      __FILE__, __LINE__ );
	}
    fCom = (BCLMsgCommunication*)tmpCom;
    fReceiveAddress = fCom->NewAddress();
    if ( !fReceiveAddress )
	{
	LOG( MLUCLog::kError, "TMControlMaster::SetAddress", "Out of memory" )
	    << "Unable to allocate address."
	    << ENDLOG;
	throw TMSysException( ENOMEM, strerror(ENOMEM), "Out of memory: Unable to allocate address",
			      __FILE__, __LINE__ );
	}

    ret = fCom->Bind( fLocalAddress );
    if ( ret )
	  {
	  LOG( MLUCLog::kError, "TMControlMaster::SetAddress", "Error binding to address" )
	      << "Error binding to specified local address '" << addr << "': "
	      << strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
	      << ENDLOG;
	  throw TMSysException( ret, strerror(ret), "Error binding to specified local address",
			      __FILE__, __LINE__ );
	  }
    }

void TMControlMaster::ClearAddress()
    {
    fCom->Unbind();
    fCom->DeleteAddress( fReceiveAddress ); 
    BCLFreeAddress( fCom, fLocalAddress );
    fCom = NULL;
    fLocalAddress = NULL;
    fReceiveAddress = NULL;
    }

void TMControlMaster::CleanupOldReplies()
    {
    bool thorough = false;
    struct timeval target_time;
    gettimeofday( &target_time, NULL );
    // Here, target_time is now
    if ( target_time.tv_sec >= fLastThoroughReplyCleanup.tv_sec+600 )
	{
	thorough = true;
	fLastThoroughReplyCleanup = target_time;
	}
    // Now really set target time
    if ( target_time.tv_sec >= 600 ) // 10min linger time
	target_time.tv_sec -= 600;
    else 
	target_time.tv_sec = 0;
    vector<ReplyStruct>::iterator replyIter, replyEnd;
    //fReplySignal.Lock();
    if ( thorough )
	{
	bool found=true;
	while ( found )
	    {
	    found=false;
	    replyIter = fReplies.begin();
	    replyEnd = fReplies.end();
	    while ( replyIter != replyEnd )
		{
		if ( replyIter->fTimestamp.tv_sec < target_time.tv_sec )
		    {
		    LOG( MLUCLog::kDebug, "TMControlMaster::CleanupOldReplies", "Cleaning up reply" )
			<< "Cleaning up reply " << MLUCLog::kDec << replyIter->fReplyID
			<< "." << ENDLOG;
		    fReplies.erase( replyIter );
		    found=true;
		    break;
		    }
		replyIter++;
		}
	    }
	}
    else
	{
	while ( fReplies.size()>0 && fReplies.begin()->fTimestamp.tv_sec < target_time.tv_sec )
	    {
	    LOG( MLUCLog::kDebug, "TMControlMaster::CleanupOldReplies", "Cleaning up reply" )
		<< "Cleaning up reply " << MLUCLog::kDec << fReplies.begin()->fReplyID
		<< "." << ENDLOG;
	    fReplies.erase( fReplies.begin() );
	    }
	}
    //fReplySignal.Unlock();
    }

void TMControlMaster::RunLoop()
    {
    fRunLoop = true;
    fLoopRunning = true;
    int ret;
    BCLMessageStruct* msg;
    TMControlMessage refMsg;
    do
	{
	ret = fCom->Receive( fReceiveAddress, msg, 50ul );
	if ( ret && ret!=ETIMEDOUT )
	    {
	    LOG( MLUCLog::kError, "TMControlMaster::RunLoop", "Error receiving message" )
		<< "Error receiving message: " << strerror(ret) << " ("
		<< MLUCLog::kDec << ret << ")." << ENDLOG;
	    }
	else if ( !ret )
	    {
	    switch ( msg->fMsgType )
		{
		case kTMControlStateReply: // Fallthrough
		case kTMControlStatusDataReply: // Fallthrough
		case kTMControlChildrenReply: // Fallthrough
		case kTMControlStatesReply: // Fallthrough
		case kTMControlChildStatesReply:
		    {
		    const char* reply;
		    switch ( msg->fMsgType )
			{
			case kTMControlStateReply:
			    reply = "State";
			    break;
			case kTMControlStatusDataReply:
			    reply = "Status Data";
			    break;
			case kTMControlChildrenReply:
			    reply = "Children";
			    break;
			case kTMControlStatesReply:
			    reply = "States";
			    break;
			case kTMControlChildStatesReply:
			    reply = "Child States";
			    break;
			default:
			    reply = "Wrong";
			    break;
			}
		    LOG( MLUCLog::kDebug, "TMControlMaster::RunLoop", "Reply received" )
			<< "Received " << reply << " message: '" << (const char*)(((TMControlMessageStruct*)msg)->fReply)
			<< "' (reply id: " << MLUCLog::kDec << ((TMControlMessageStruct*)msg)->fReplyID << ") ." << ENDLOG;
		    ReplyStruct rs;
		    rs.fReply = (const char*)(((TMControlMessageStruct*)msg)->fReply);
		    rs.fReplyID = ((TMControlMessageStruct*)msg)->fReplyID;
		    gettimeofday( &rs.fTimestamp, NULL );
		    fReplySignal.Lock();
// 		    pthread_mutex_lock( &fReplyMutex );
		    fReplies.insert( fReplies.end(), rs );
// 		    pthread_mutex_unlock( &fReplyMutex );
		    fReplySignal.Unlock();
		    fReplySignal.Signal();
		    break;
		    }
		case kTMControlInterrupt:
		    if ( fCallback )
			{
// 			char* url;
// 			BCLEncodeAddress( fReceiveAddress, false, url );
// 			fCallback( fArg, 0, NULL, url );
// 			BCLFreeEncodedAddress( url );
			char* notData = NULL;
			if ( msg->fLength!=refMsg.GetData()->fLength )
			    notData = (char*)(((uint8*)msg)+refMsg.GetData()->fLength);
			fCallback( fArg, 0, NULL, fReceiveAddress, notData );
			}
		    break;
		default:
		    LOG( MLUCLog::kError, "TMControlMaster::RunLoop", "Unexpected message" )
			<< "Unexpected message received: 0x" << MLUCLog::kHex << (unsigned long)msg->fMsgType 
			<< "/" << MLUCLog::kDec << (unsigned long)msg->fMsgType 
			<< "." << ENDLOG;
		    break;
		}
	    fCom->ReleaseMsg( msg );
	    }
	}
    while ( fRunLoop );
    LOG( MLUCLog::kInformational, "TMControlMaster::RunLoop", "Leaving Runloop" )
	<< "Leaving contro master runloop" << ENDLOG;
    fLoopRunning = false;
    }	


void TMControlMaster::StopLoop()
    {
    fRunLoop = false;
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 200000;
    gettimeofday( &start, NULL );
    while ( fLoopRunning && deltaT<timeLimit )
	{
	usleep(0);
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    }

void TMControlMaster::QueryState( void* address, MLUCString& state )
    {
    if ( !address )
	throw TMSysException( EFAULT, strerror(EFAULT), "No state query target address",
			      __FILE__, __LINE__ );
    TMControlMessage msg;
    uint64 id;
    int ret;
    bool logErrors = !SuppressComErrorLogs( address );
    msg.GetData()->fMsgType = kTMControlQueryState;
    BCLAbstrAddressStruct* addr = (BCLAbstrAddressStruct*)address;
    pthread_mutex_lock( &fComSendMutex );
    msg.GetData()->fReplyID = id = fLastReplyID++;
    ret = fCom->Send( addr, msg.GetData(), 2000 ); // 2.5s timeout should be enough...
    pthread_mutex_unlock( &fComSendMutex );
    if ( ret )
	{
	if ( logErrors )
	    {
	    LOG( MLUCLog::kError, "TMControlMaster::QueryState", "Error sending to state query target address" )
		<< "Error sending to target address: "
		<< strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
		<< ENDLOG;
	    }
	throw TMSysException( ret, strerror(ret), "Error sending to state query target address",
			      __FILE__, __LINE__ );
	}
    LOG( MLUCLog::kDebug, "TMControlMaster::QueryState", "Sent State Query" )
	<< "Sent state query with reply id " << MLUCLog::kDec
	<< id << "." << ENDLOG;

    struct timeval s, e;
    unsigned long long diff=0;
    bool found = false;
    gettimeofday( &s, NULL );
    do
	{
	fReplySignal.Lock();
	CleanupOldReplies();
	vector<ReplyStruct>::iterator iter, end;
	iter = fReplies.begin();
	end = fReplies.end();
	while ( iter != end )
	    {
	    if ( iter->fReplyID == id )
		{
		state = iter->fReply;
		fReplies.erase( iter );
		LOG( MLUCLog::kDebug, "TMControlMaster::QueryState", "Found State Reply" )
		    << "Found state reply with reply id " << MLUCLog::kDec
		    << id << "." << ENDLOG;
		found = true;
		break;
		}
	    iter++;
	    }
	if ( !found )
	    {
	    gettimeofday( &e, NULL );
	    diff = e.tv_sec-s.tv_sec;
	    diff *= 1000000;
	    diff += e.tv_usec-s.tv_usec;
	    }
	if ( !found && diff<2000000 )
	    fReplySignal.Wait( 2000 );
	fReplySignal.Unlock();
	}
    while ( !found && diff<2000000 ); // wait 5s at most...
    if ( !found )
	{
	state = "UNREACHABLE";
	if ( logErrors )
	    {
	    unsigned errorCount=0;
		{
		MLUCMutex::TLocker missingReplyLock( fMissingReplyDataMutex );
		vector<TMissingReplyData>::iterator iter, end;
		iter = fMissingReplyData.begin();
		end = fMissingReplyData.end();
		while ( iter!= end )
		    {
		    if ( iter->fAddress==address )
			{
			iter->fCnt++;
			if ( iter->fCnt>=gkReplyTimeoutErrorRepeatErrorCount )
			    errorCount = iter->fCnt;
			break;
			}
		    ++iter;
		    }
		if ( iter==end )
		    fMissingReplyData.push_back( TMissingReplyData(address) );
		}
		if ( errorCount )
		    {
		    LOG( MLUCLog::kWarning, "TMControlMaster::QueryState", "Timeout waiting for state reply" )
			<< "Timeout waiting for reply (" << MLUCLog::kDec << id << " from slave (" << errorCount
			<< " times)." << ENDLOG;
		    }
	    }
	throw TMSysException( ETIMEDOUT, strerror(ETIMEDOUT), "Timeout waiting for state reply",
			      __FILE__, __LINE__ );
	}
    else
	{
	MLUCMutex::TLocker missingReplyLock( fMissingReplyDataMutex );
	vector<TMissingReplyData>::iterator iter, end;
	iter = fMissingReplyData.begin();
	end = fMissingReplyData.end();
	while ( iter!= end )
	    {
	    if ( iter->fAddress==address )
		{
		fMissingReplyData.erase( iter );
		break;
		}
	    ++iter;
	    }
	}
    }

void TMControlMaster::QueryStatusData( void* address, MLUCString& status )
    {
    if ( !address )
	throw TMSysException( EFAULT, strerror(EFAULT), "No status data query target address",
			      __FILE__, __LINE__ );
    TMControlMessage msg;
    uint64 id;
    int ret;
    bool logErrors = !SuppressComErrorLogs( address );
    msg.GetData()->fMsgType = kTMControlQueryStatusData;
    BCLAbstrAddressStruct* addr = (BCLAbstrAddressStruct*)address;
    pthread_mutex_lock( &fComSendMutex );
    msg.GetData()->fReplyID = id = fLastReplyID++;
    ret = fCom->Send( addr, msg.GetData(), 2000 ); // 2.5s timeout should be enough...
    pthread_mutex_unlock( &fComSendMutex );
    if ( ret )
	{
	if ( logErrors )
	    {
	    LOG( MLUCLog::kError, "TMControlMaster::QueryStatusData", "Error sending to status data query target address" )
		<< "Error sending to target address: "
		<< strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
		<< ENDLOG;
	    }
	throw TMSysException( ret, strerror(ret), "Error sending to status data query target address",
			      __FILE__, __LINE__ );
	}
    LOG( MLUCLog::kDebug, "TMControlMaster::QueryStatusData", "Sent StatusData Query" )
	<< "Sent status data query with reply id " << MLUCLog::kDec
	<< id << "." << ENDLOG;

    struct timeval s, e;
    unsigned long long diff=0;
    bool found = false;
    gettimeofday( &s, NULL );
    do
	{
	fReplySignal.Lock();
	CleanupOldReplies();
	vector<ReplyStruct>::iterator iter, end;
	iter = fReplies.begin();
	end = fReplies.end();
	while ( iter != end )
	    {
	    if ( iter->fReplyID == id )
		{
		status = iter->fReply;
		fReplies.erase( iter );
		LOG( MLUCLog::kDebug, "TMControlMaster::QueryState", "Found StatusData Reply" )
		    << "Found status data reply with reply id " << MLUCLog::kDec
		    << id << "." << ENDLOG;
		found = true;
		break;
		}
	    iter++;
	    }
	if ( !found )
	    {
	    gettimeofday( &e, NULL );
	    diff = e.tv_sec-s.tv_sec;
	    diff *= 1000000;
	    diff += e.tv_usec-s.tv_usec;
	    }
	if ( !found && diff<2000000 )
	    fReplySignal.Wait( 2000 );
	fReplySignal.Unlock();
	}
    while ( !found && diff<2000000 ); // wait 5s at most...
    if ( !found )
	{
	status = "UNREACHABLE";
	if ( logErrors )
	    {
	    unsigned errorCount=0;
		{
		MLUCMutex::TLocker missingReplyLock( fMissingReplyDataMutex );
		vector<TMissingReplyData>::iterator iter, end;
		iter = fMissingReplyData.begin();
		end = fMissingReplyData.end();
		while ( iter!= end )
		    {
		    if ( iter->fAddress==address )
			{
			iter->fCnt++;
			if ( iter->fCnt>=gkReplyTimeoutErrorRepeatErrorCount )
			    errorCount = iter->fCnt;
			break;
			}
		    ++iter;
		    }
		if ( iter==end )
		    fMissingReplyData.push_back( TMissingReplyData(address) );
		}
		if ( errorCount )
		    {
		    LOG( MLUCLog::kWarning, "TMControlMaster::QueryStatusData", "Timeout waiting for status data reply" )
			<< "Timeout waiting for reply (" << MLUCLog::kDec << id << " from slave (" << errorCount
			<< " times)." << ENDLOG;
		    }
	    }
	throw TMSysException( ETIMEDOUT, strerror(ETIMEDOUT), "Timeout waiting for status data reply",
			      __FILE__, __LINE__ );
	}
    else
	{
	MLUCMutex::TLocker missingReplyLock( fMissingReplyDataMutex );
	vector<TMissingReplyData>::iterator iter, end;
	iter = fMissingReplyData.begin();
	end = fMissingReplyData.end();
	while ( iter!= end )
	    {
	    if ( iter->fAddress==address )
		{
		fMissingReplyData.erase( iter );
		break;
		}
	    ++iter;
	    }
	}
    }

void TMControlMaster::QueryChildren( void* address, MLUCString& children )
    {
    if ( !address )
	throw TMSysException( EFAULT, strerror(EFAULT), "No children query target address",
			      __FILE__, __LINE__ );
    TMControlMessage msg;
    uint64 id;
    int ret;
    bool logErrors = !SuppressComErrorLogs( address );
    msg.GetData()->fMsgType = kTMControlQueryChildren;
    BCLAbstrAddressStruct* addr = (BCLAbstrAddressStruct*)address;
    pthread_mutex_lock( &fComSendMutex );
    msg.GetData()->fReplyID = id = fLastReplyID++;
    ret = fCom->Send( addr, msg.GetData(), 2000 ); // 2.5s timeout should be enough...
    pthread_mutex_unlock( &fComSendMutex );
    if ( ret )
	{
	if ( logErrors )
	    {
	    LOG( MLUCLog::kError, "TMControlMaster::QueryChildren", "Error sending to children query target address" )
		<< "Error sending to target address: "
		<< strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
		<< ENDLOG;
	    }
	throw TMSysException( ret, strerror(ret), "Error sending to children query target address",
			      __FILE__, __LINE__ );
	}

    struct timeval s, e;
    unsigned long long diff=0;
    bool found = false;
    gettimeofday( &s, NULL );
    do
	{
	fReplySignal.Lock();
	vector<ReplyStruct>::iterator iter, end;
	iter = fReplies.begin();
	end = fReplies.end();
	while ( iter != end )
	    {
	    if ( iter->fReplyID == id )
		{
		children = iter->fReply;
		fReplies.erase( iter );
		found = true;
		break;
		}
	    iter++;
	    }
	if ( !found )
	    {
	    gettimeofday( &e, NULL );
	    diff = e.tv_sec-s.tv_sec;
	    diff *= 1000000;
	    diff += e.tv_usec-s.tv_usec;
	    }
	CleanupOldReplies();
	if ( !found && diff<2000000 )
	    fReplySignal.Wait( 2000 );
	fReplySignal.Unlock();
	}
    while ( !found && diff<2000000 ); // wait 5s at most...
    if ( !found )
	{
	children = "";
	if ( logErrors )
	    {
	    unsigned errorCount=0;
		{
		MLUCMutex::TLocker missingReplyLock( fMissingReplyDataMutex );
		vector<TMissingReplyData>::iterator iter, end;
		iter = fMissingReplyData.begin();
		end = fMissingReplyData.end();
		while ( iter!= end )
		    {
		    if ( iter->fAddress==address )
			{
			iter->fCnt++;
			if ( iter->fCnt>=gkReplyTimeoutErrorRepeatErrorCount )
			    errorCount = iter->fCnt;
			break;
			}
		    ++iter;
		    }
		if ( iter==end )
		    fMissingReplyData.push_back( TMissingReplyData(address) );
		}
		if ( errorCount )
		    {
		    LOG( MLUCLog::kWarning, "TMControlMaster::QueryChildren", "Timeout waiting for children reply" )
			<< "Timeout waiting for reply from slave (" << errorCount
			<< " times)." << ENDLOG;
		    }
	    }
	throw TMSysException( ETIMEDOUT, strerror(ETIMEDOUT), "Timeout waiting for children reply",
			      __FILE__, __LINE__ );
	}
    else
	{
	MLUCMutex::TLocker missingReplyLock( fMissingReplyDataMutex );
	vector<TMissingReplyData>::iterator iter, end;
	iter = fMissingReplyData.begin();
	end = fMissingReplyData.end();
	while ( iter!= end )
	    {
	    if ( iter->fAddress==address )
		{
		fMissingReplyData.erase( iter );
		break;
		}
	    ++iter;
	    }
	}
    }

void TMControlMaster::QueryStates( void* address, MLUCString& states )
    {
    if ( !address )
	throw TMSysException( EFAULT, strerror(EFAULT), "No states query target address",
			      __FILE__, __LINE__ );
    TMControlMessage msg;
    uint64 id;
    int ret;
    bool logErrors = !SuppressComErrorLogs( address );
    msg.GetData()->fMsgType = kTMControlQueryStates;
    BCLAbstrAddressStruct* addr = (BCLAbstrAddressStruct*)address;
    pthread_mutex_lock( &fComSendMutex );
    msg.GetData()->fReplyID = id = fLastReplyID++;
    ret = fCom->Send( addr, msg.GetData(), 2000 ); // 2.5s timeout should be enough...
    pthread_mutex_unlock( &fComSendMutex );
    if ( ret )
	{
	if ( logErrors )
	    {
	    LOG( MLUCLog::kError, "TMControlMaster::QueryStates", "Error sending to states query target address" )
		<< "Error sending to target address: "
		<< strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
		<< ENDLOG;
	    }
	throw TMSysException( ret, strerror(ret), "Error sending to states query target address",
			      __FILE__, __LINE__ );
	}

    struct timeval s, e;
    unsigned long long diff=0;
    bool found = false;
    gettimeofday( &s, NULL );
    do
	{
	fReplySignal.Lock();
	CleanupOldReplies();
	vector<ReplyStruct>::iterator iter, end;
	iter = fReplies.begin();
	end = fReplies.end();
	while ( iter != end )
	    {
	    if ( iter->fReplyID == id )
		{
		states = iter->fReply;
		fReplies.erase( iter );
		found = true;
		break;
		}
	    iter++;
	    }
	if ( !found )
	    {
	    gettimeofday( &e, NULL );
	    diff = e.tv_sec-s.tv_sec;
	    diff *= 1000000;
	    diff += e.tv_usec-s.tv_usec;
	    }
	if ( !found && diff<2000000 )
	    fReplySignal.Wait( 2000 );
	fReplySignal.Unlock();
	}
    while ( !found && diff<2000000 ); // wait 5s at most...
    if ( !found )
	{
	states = "";
	if ( logErrors )
	    {
	    unsigned errorCount=0;
		{
		MLUCMutex::TLocker missingReplyLock( fMissingReplyDataMutex );
		vector<TMissingReplyData>::iterator iter, end;
		iter = fMissingReplyData.begin();
		end = fMissingReplyData.end();
		while ( iter!= end )
		    {
		    if ( iter->fAddress==address )
			{
			iter->fCnt++;
			if ( iter->fCnt>=gkReplyTimeoutErrorRepeatErrorCount )
			    errorCount = iter->fCnt;
			break;
			}
		    ++iter;
		    }
		if ( iter==end )
		    fMissingReplyData.push_back( TMissingReplyData(address) );
		}
		if ( errorCount )
		    {
		    LOG( MLUCLog::kWarning, "TMControlMaster::QueryStates", "Timeout waiting for states reply" )
			<< "Timeout waiting for reply from slave (" << errorCount
			<< " times)." << ENDLOG;
		    }
	    }
	throw TMSysException( ETIMEDOUT, strerror(ETIMEDOUT), "Timeout waiting for states reply",
			      __FILE__, __LINE__ );
	}
    else
	{
	MLUCMutex::TLocker missingReplyLock( fMissingReplyDataMutex );
	vector<TMissingReplyData>::iterator iter, end;
	iter = fMissingReplyData.begin();
	end = fMissingReplyData.end();
	while ( iter!= end )
	    {
	    if ( iter->fAddress==address )
		{
		fMissingReplyData.erase( iter );
		break;
		}
	    ++iter;
	    }
	}
    }

void TMControlMaster::QueryChildStates( void* address, const char* child_id, MLUCString& states )
    {
    if ( !address )
	throw TMSysException( EFAULT, strerror(EFAULT), "No child states query target address",
			      __FILE__, __LINE__ );
    if ( !child_id )
	throw TMSysException( EFAULT, strerror(EFAULT), "No child states query child id",
			      __FILE__, __LINE__ );
    TMControlMessage msg;
    TMControlMessageStruct* msgP;
    uint64 id;
    int ret;
    bool logErrors = !SuppressComErrorLogs( address );
    msg.GetData()->fMsgType = kTMControlQueryChildStates;
    uint32 len = strlen(child_id)+1;
    if ( len % 4 )
	len += 4-(len%4);
    msgP = (TMControlMessageStruct*)malloc( msg.GetData()->fLength+len );
    if ( !msgP )
	{
	LOG( MLUCLog::kError, "TMControlMaster::QueryChildStates", "Out of memory" )
	    << "Out of memory trying to allocate query child states message of " << MLUCLog::kDec
	    << msg.GetData()->fLength+len  << " bytes." << ENDLOG;
	throw TMSysException( ENOMEM, strerror(ENOMEM), "Out of memory trying to allocate query child states message",
			      __FILE__, __LINE__ );
	}
    memcpy( msgP, msg.GetData(), msg.GetData()->fLength );
    msgP->fLength += len;
    LOG( MLUCLog::kDebug, "TMControlMaster::QueryChildStates", "Sending child state query" )
	<< "Sending query for child '" << child_id << "' states." << ENDLOG;
    strcpy( (char*)(msgP->fReply), child_id );
    BCLAbstrAddressStruct* addr = (BCLAbstrAddressStruct*)address;
    pthread_mutex_lock( &fComSendMutex );
    msgP->fReplyID = id = fLastReplyID++;
    ret = fCom->Send( addr, msgP, 2000 ); // 2.5s timeout should be enough...
    pthread_mutex_unlock( &fComSendMutex );
    free( msgP );
    if ( ret )
	{
	if ( logErrors )
	    {
	    LOG( MLUCLog::kError, "TMControlMaster::QueryStates", "Error sending child states query to target address" )
		<< "Error sending to target address: "
		<< strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
		<< ENDLOG;
	    }
	throw TMSysException( ret, strerror(ret), "Error sending child states query to target address",
			      __FILE__, __LINE__ );
	}

    struct timeval s, e;
    unsigned long long diff=0;
    bool found = false;
    gettimeofday( &s, NULL );
    do
	{
	fReplySignal.Lock();
	CleanupOldReplies();
	vector<ReplyStruct>::iterator iter, end;
	iter = fReplies.begin();
	end = fReplies.end();
	while ( iter != end )
	    {
	    if ( iter->fReplyID == id )
		{
		states = iter->fReply;
		fReplies.erase( iter );
		found = true;
		break;
		}
	    iter++;
	    }
	if ( !found )
	    {
	    gettimeofday( &e, NULL );
	    diff = e.tv_sec-s.tv_sec;
	    diff *= 1000000;
	    diff += e.tv_usec-s.tv_usec;
	    }
	if ( !found && diff<2000000 )
	    fReplySignal.Wait( 2000 );
	fReplySignal.Unlock();
	}
    while ( !found && diff<2000000 ); // wait 5s at most...
    if ( !found )
	{
	states = "";
	if ( logErrors )
	    {
	    unsigned errorCount=0;
		{
		MLUCMutex::TLocker missingReplyLock( fMissingReplyDataMutex );
		vector<TMissingReplyData>::iterator iter, end;
		iter = fMissingReplyData.begin();
		end = fMissingReplyData.end();
		while ( iter!= end )
		    {
		    if ( iter->fAddress==address )
			{
			iter->fCnt++;
			if ( iter->fCnt>=gkReplyTimeoutErrorRepeatErrorCount )
			    errorCount = iter->fCnt;
			break;
			}
		    ++iter;
		    }
		if ( iter==end )
		    fMissingReplyData.push_back( TMissingReplyData(address) );
		}
		if ( errorCount )
		    {
		    LOG( MLUCLog::kWarning, "TMControlMaster::QueryChildStates", "Timeout waiting for child states reply" )
			<< "Timeout waiting for reply " << MLUCLog::kDec << id << " from slave (" << errorCount
			<< " times)." << ENDLOG;
		    }
	    }
	throw TMSysException( ETIMEDOUT, strerror(ETIMEDOUT), "Timeout waiting for child states reply",
			      __FILE__, __LINE__ );
	}
    else
	{
	MLUCMutex::TLocker missingReplyLock( fMissingReplyDataMutex );
	vector<TMissingReplyData>::iterator iter, end;
	iter = fMissingReplyData.begin();
	end = fMissingReplyData.end();
	while ( iter!= end )
	    {
	    if ( iter->fAddress==address )
		{
		fMissingReplyData.erase( iter );
		break;
		}
	    ++iter;
	    }
	}
    }


void TMControlMaster::SendCmd( void* address, const char* cmd )
    {
    if ( !address )
	throw TMSysException( EFAULT, strerror(EFAULT), "No command target address",
			      __FILE__, __LINE__ );
    if ( !cmd )
	throw TMSysException( EFAULT, strerror(EFAULT), "Invalid (NULL pointer) command",
			      __FILE__, __LINE__ );
    TMControlMessage msg;
    TMControlMessageStruct* msgP;
    uint64 id;
    int ret;
    bool logErrors = !SuppressComErrorLogs( address );
    msg.GetData()->fMsgType = kTMControlCommand;
    uint32 len = strlen(cmd)+1;
    if ( len % 4 )
	len += 4-(len%4);
    msgP = (TMControlMessageStruct*)malloc( msg.GetData()->fLength+len );
    if ( !msgP )
	{
	LOG( MLUCLog::kError, "TMControlMaster::SendCmd", "Out of memory" )
	    << "Out of memory trying to allocate command message of " << MLUCLog::kDec
	    << msg.GetData()->fLength+len  << " bytes." << ENDLOG;
	throw TMSysException( ENOMEM, strerror(ENOMEM), "Out of memory trying to allocate command message",
			      __FILE__, __LINE__ );
	}
    memcpy( msgP, msg.GetData(), msg.GetData()->fLength );
    msgP->fLength += len;
    LOG( MLUCLog::kDebug, "TMControlMaster::SendCmd", "Sending command" )
	<< "Sending command '" << cmd << "'." << ENDLOG;
    strcpy( (char*)(msgP->fReply), cmd );
    BCLAbstrAddressStruct* addr = (BCLAbstrAddressStruct*)address;
    pthread_mutex_lock( &fComSendMutex );
    msg.GetData()->fReplyID = id = fLastReplyID++;
    msgP->fReplyID = id;
    ret = fCom->Send( addr, msgP, 2000 ); // 2.5s timeout should be enough...
    pthread_mutex_unlock( &fComSendMutex );
    free( msgP );
    if ( ret )
	{
	if ( logErrors )
	    {
	    LOG( MLUCLog::kError, "TMControlMaster::SendCmd", "Error sending to command target address" )
		<< "Error sending to target address: "
		<< strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
		<< ENDLOG;
	    }
	throw TMSysException( ret, strerror(ret), "Error sending to command target address",
			      __FILE__, __LINE__ );
	}
    }

void TMControlMaster::Connect( void* address )
    {
    if ( !address )
	throw TMSysException( EFAULT, strerror(EFAULT), "No connect target address",
			      __FILE__, __LINE__ );
    TMControlMessage msg;
    uint64 id;
    int ret;
    bool logErrors = !SuppressComErrorLogs( address );
    msg.GetData()->fMsgType = kTMControlConnect;

    BCLAbstrAddressStruct* addr = (BCLAbstrAddressStruct*)address;
    pthread_mutex_lock( &fComSendMutex );
    LOG( MLUCLog::kDebug, "TMControlMaster::Connect", "Attempting connection" )
	<< "Attempting to estalish connection target address" << ENDLOG;
#if 1
    ret = fCom->Connect( addr, 2000, false );
#else
    ret = fCom->Connect( addr, 2000, true );
#endif
    if ( !ret )
	{
	msg.GetData()->fReplyID = id = fLastReplyID++;
	ret = fCom->Send( addr, msg.GetData(), 2000 ); // 2.5s timeout should be enough...
	if ( ret )
	    fCom->Disconnect( addr, 2000 );
	}
    pthread_mutex_unlock( &fComSendMutex );
    if ( ret )
	{
	if ( logErrors )
	    {
	    LOG( MLUCLog::kError, "TMControlMaster::Connect", "Error sending to connection target address" )
		<< "Error sending to target address: "
		<< strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
		<< ENDLOG;
	    }
	throw TMSysException( ret, strerror(ret), "Error sending to connection target address",
			      __FILE__, __LINE__ );
	}
    }

void TMControlMaster::Disconnect( void* address )
    {
    if ( !address )
	throw TMSysException( EFAULT, strerror(EFAULT), "No disconnect target address",
			      __FILE__, __LINE__ );
    TMControlMessage msg;
    uint64 id;
    int ret1, ret2;
    bool logErrors = !SuppressComErrorLogs( address );
    msg.GetData()->fMsgType = kTMControlDisconnect;

    BCLAbstrAddressStruct* addr = (BCLAbstrAddressStruct*)address;
    pthread_mutex_lock( &fComSendMutex );
    msg.GetData()->fReplyID = id = fLastReplyID++;
    ret1 = fCom->Send( addr, msg.GetData(), 2000 ); // 2.5s timeout should be enough...
    ret2 = fCom->Disconnect( addr, 2000 );
    pthread_mutex_unlock( &fComSendMutex );
    if ( ret1 )
	{
	if ( logErrors )
	    {
	    LOG( MLUCLog::kError, "TMControlMaster::Disconnect", "Error sending to disconnect target address" )
		<< "Error sending to target address: "
		<< strerror(ret1) << MLUCLog::kDec << " (" << ret1 << ")."
		<< ENDLOG;
	    }
	throw TMSysException( ret1, strerror(ret1), "Error sending to disconnect target address",
			      __FILE__, __LINE__ );
	}
    if ( ret2 )
	{
	if ( logErrors )
	    {
	    LOG( MLUCLog::kError, "TMControlMaster::Disconnect", "Error sending to disconnect target address" )
		<< "Error sending to target address: "
		<< strerror(ret2) << MLUCLog::kDec << " (" << ret2 << ")."
		<< ENDLOG;
	    }
	throw TMSysException( ret2, strerror(ret2), "Error sending to disconnect target address",
			      __FILE__, __LINE__ );
	}
    }


void TMControlMaster::ConvertAddress( const char* process_address_str, void** process_address_bin )
    {
    int ret;
    BCLAbstrAddressStruct* addr;
    ret = BCLDecodeRemoteURL( process_address_str, addr );
    if ( ret )
 	{
 	LOG( MLUCLog::kError, "TMControlMaster::ConvertAddress", "Unable to decode address" )
 	    << "Unable to decode address '" << process_address_str << "': "
 	    << strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
 	    << ENDLOG;
 	throw TMSysException( ret, strerror(ret), "Unable to decode address",
 			      __FILE__, __LINE__ );
 	}
    unsigned long len = BCLGetAddressSize( addr );
    if ( !len )
 	{
 	LOG( MLUCLog::kError, "TMControlMaster::ConvertAddress", "Error determining binary address size" )
 	    << "Cannot determine size of binary representation of address '" << process_address_str 
	    << "'." << ENDLOG;
 	throw TMSysException( EMSGSIZE, strerror(EMSGSIZE), "Error determining binary address size",
 			      __FILE__, __LINE__ );
 	}
    *process_address_bin = malloc( len );
    if ( !*process_address_bin )
	{
 	LOG( MLUCLog::kError, "TMControlMaster::ConvertAddress", "Out of memory allocating address" )
 	    << "Out of memory allocating address of size '" << MLUCLog::kDec << len 
	    << "'." << ENDLOG;
 	throw TMSysException( ENOMEM, strerror(ENOMEM), "Out of memory allocating address",
 			      __FILE__, __LINE__ );
	}
    memcpy( *process_address_bin, addr, len );
    BCLFreeAddress( addr );
    }

int TMControlMaster::CompareAddresses( void* addr1, void* addr2 )
    {
    return BCLCompareAddresses( (BCLAbstrAddressStruct*)addr1, (BCLAbstrAddressStruct*)addr2 );
    }

void TMControlMaster::SuppressComErrorLogs( void* address, unsigned long timeout_us )
    {
    if ( !fCom )
	return;
    struct timeval now;
    bool remove = false;
    if ( timeout_us == 0 )
	{
	now.tv_sec = now.tv_usec = 0;
	}
    else if ( timeout_us == 0xFFFFFFFF )
	{
	now.tv_sec = now.tv_usec = 0;
	remove = true;
	}
    else
	{
	gettimeofday( &now, NULL );
	now.tv_usec += timeout_us;
	now.tv_sec += (now.tv_usec / 1000000 );
	now.tv_usec %= 1000000;
	}
    pthread_mutex_lock( &fComErrorLogSuppressionsMutex );
    vector<TComErrorLogSuppressionData>::iterator iter, end;
    iter = fComErrorLogSuppressions.begin();
    end = fComErrorLogSuppressions.end();
    while ( iter != end )
	{
	if ( fCom->AddressEquality( iter->fAddress, (BCLAbstrAddressStruct*)address ) )
	    {
	    if ( remove )
		{
		fCom->DeleteAddress( iter->fAddress );
		fComErrorLogSuppressions.erase( iter );
		break;
		}
	    else if ( ( iter->fUntil.tv_sec==0 && iter->fUntil.tv_usec==0 ) ||
		 iter->fUntil.tv_sec<now.tv_sec || 
		 (iter->fUntil.tv_sec==now.tv_sec && iter->fUntil.tv_usec<now.tv_usec ) )
		{
		iter->fUntil.tv_sec = now.tv_sec;
		iter->fUntil.tv_usec = now.tv_usec;
		break;
		}
	    }
	iter++;
	}
    if ( iter == end && !remove )
	{
	TComErrorLogSuppressionData celsd;
	//celsd.fAddressURL = addressURL;
	celsd.fAddress = fCom->NewAddress();
	if ( celsd.fAddress )
	    {
	    memcpy( celsd.fAddress, address, fCom->GetAddressLength() );
	    celsd.fUntil = now;
	    fComErrorLogSuppressions.push_back( celsd );
	    }
	}
    pthread_mutex_unlock( &fComErrorLogSuppressionsMutex );
    }


bool TMControlMaster::SuppressComErrorLogs( void* address )
    {
    if ( !fCom )
	return false;
    struct timeval now;
    gettimeofday( &now, NULL );
    pthread_mutex_lock( &fComErrorLogSuppressionsMutex );
    vector<TComErrorLogSuppressionData>::iterator iter, end;
    iter = fComErrorLogSuppressions.begin();
    end = fComErrorLogSuppressions.end();
    while ( iter != end )
	{
	if ( fCom->AddressEquality( iter->fAddress, (BCLAbstrAddressStruct*)address ) )
	    {
	    if ( ( iter->fUntil.tv_sec==0 && iter->fUntil.tv_usec==0 ) ||
		 iter->fUntil.tv_sec>now.tv_sec || 
		 (iter->fUntil.tv_sec==now.tv_sec && iter->fUntil.tv_usec>now.tv_usec ) )
		{
		pthread_mutex_unlock( &fComErrorLogSuppressionsMutex );
		return true;
		}
	    else if ( iter->fUntil.tv_sec<now.tv_sec || 
		 (iter->fUntil.tv_sec==now.tv_sec && iter->fUntil.tv_usec<now.tv_usec ) )
		{
		fCom->DeleteAddress( iter->fAddress );
		fComErrorLogSuppressions.erase( iter );
		pthread_mutex_unlock( &fComErrorLogSuppressionsMutex );
		return false;
		}
	    }
	iter++;
	}
    pthread_mutex_unlock( &fComErrorLogSuppressionsMutex );
    return false;
    }

bool TMControlMaster::ReportMissingReplyError( void* address )
    {
    MLUCMutex::TLocker missingReplyLock( fMissingReplyDataMutex );
    vector<TMissingReplyData>::iterator iter, end;
    iter = fMissingReplyData.begin();
    end = fMissingReplyData.end();
    while ( iter!= end )
	{
	if ( iter->fAddress==address )
	    return iter->fCnt>=gkReplyTimeoutErrorRepeatErrorCount;
	++iter;
	}
    return false;
    }



/*
bool TMControlMaster::NormalizeAddress( const char* address, MLUCString& newaddr )
    {
    BCLAbstrAddressStruct *addrP;
    
    int ret;
    ret = BCLDecodeRemoteURL( address, addrP );
    if ( ret )
	{
	LOG( MLUCLog::kError, "TMControlMaster::NormalizeAddress", "Unable to decode address" )
	    << "Unable to decode address '" << address << "': "
	    << strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
	    << ENDLOG;
	return false;
	}
    char *encAddr;
    BCLEncodeAddress( addrP, false, encAddr );
    newaddr = encAddr;
    BCLFreeEncodedAddress( encAddr );
    BCLFreeAddress( addrP );
    return true;
    }

*/


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
