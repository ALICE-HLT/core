/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TMInterruptWaitThread.hpp"
#include "TMSystem.hpp"
#include "TMInterfaceLibrary.hpp"
#include <MLUCLog.hpp>

TMInterruptWaitThread::TMInterruptWaitThread( TMSystem* system, const char* listen_addr )
    {
    fSystem = system;
    fProcess = NULL;
    fListenAddress = listen_addr;
    pthread_mutex_init( &fProcessMutex, NULL );
    fRunning = false;
    }

TMInterruptWaitThread::TMInterruptWaitThread( TMProcess* process, TMSystem* system, const char* listen_addr )
    {
    fSystem = system;
    fProcess = process;
    fListenAddress = listen_addr;
    pthread_mutex_init( &fProcessMutex, NULL );
    fRunning = false;
    }

TMInterruptWaitThread::~TMInterruptWaitThread()
    {
    pthread_mutex_destroy( &fProcessMutex );
    }

void TMInterruptWaitThread::AddProcess( unsigned long ndx, 
					const char* process_id,
					void* process_address )
    {
    ProcessData pd;
    pd.fNdx = ndx;
    pd.fPID = 0;
    pd.fID = process_id;
    pd.fAddress = process_address;
    pthread_mutex_lock( &fProcessMutex );
    fProcesses.insert( fProcesses.end(), pd );
    pthread_mutex_unlock( &fProcessMutex );
    }

void TMInterruptWaitThread::DeleteProcess( unsigned long ndx )
    {
    vector<ProcessData>::iterator iter, end;
    pthread_mutex_lock( &fProcessMutex );
    iter = fProcesses.begin();
    end = fProcesses.end();
    while ( iter != end )
	{
	if ( iter->fNdx == ndx )
	    {
	    fProcesses.erase( iter );
	    break;
	    }
	iter++;
	}
    pthread_mutex_unlock( &fProcessMutex );
    }


void TMInterruptWaitThread::SetProcessPID( unsigned long ndx, pid_t pid )
    {
    vector<ProcessData>::iterator iter, end;
    pthread_mutex_lock( &fProcessMutex );
    iter = fProcesses.begin();
    end = fProcesses.end();
    while ( iter != end )
	{
	if ( iter->fNdx == ndx )
	    {
	    iter->fPID = pid;
	    break;
	    }
	iter++;
	}
    pthread_mutex_unlock( &fProcessMutex );
    }

void TMInterruptWaitThread::SetListenAddress( const char* listen_addr )
    {
    fListenAddress = listen_addr;
    }


void TMInterruptWaitThread::Run()
    {
    fRunning = true;
    fSystem->GetInterfaceLibrary()->InterruptWait( this, fListenAddress.c_str(), 
						   TMInterruptWaitThread::InterruptCallbackWrapper );
    fRunning = false;
    }

void TMInterruptWaitThread::InterruptCallbackWrapper( void* opaque_arg, 
						      pid_t pid,
						      const char* process_id,
						      void* process_address,
						      const char* notificationData )
    {
    ((TMInterruptWaitThread*)opaque_arg)->InterruptCallback( pid, process_id, process_address, notificationData );
    }

void TMInterruptWaitThread::InterruptCallback( pid_t pid,
					       const char* process_id,
					       void* process_address,
					       const char* notificationData )
    {
    vector<ProcessData>::iterator iter, end;
    unsigned long ndx=(unsigned long)-1;
    bool found = false;
    pthread_mutex_lock( &fProcessMutex );
    iter = fProcesses.begin();
    end = fProcesses.end();
    if ( pid )
	{
	while ( iter != end )
	    {
	    if ( iter->fPID == pid )
		{
		ndx = iter->fNdx;
		found = true;
		break;
		}
	    iter++;
	    }
	}
    if ( !found && process_id )
	{
	while ( iter != end )
	    {
	    if ( iter->fID == process_id )
		{
		ndx = iter->fNdx;
		found = true;
		break;
		}
	    iter++;
	    }
	}
    if ( !found && process_address )
	{
	while ( iter != end )
	    {
	    //if ( fSystem->GetInterfaceLibrary()->AddressEquality( iter->fAddress.c_str(), process_address ) )
	    //if ( iter->fAddress == process_address )
	    if ( !fSystem->GetInterfaceLibrary()->CompareAddresses( iter->fAddress, process_address ) )
		{
		ndx = iter->fNdx;
		found = true;
		break;
		}
	    iter++;
	    }
	}
    if ( !found )
	{
	}
    pthread_mutex_unlock( &fProcessMutex );
    if ( found )
	{
	fSystem->Interrupt( ndx, notificationData );
	}
    else
	{
	LOG( MLUCLog::kWarning, "TMInterruptWaitThread::InterruptCallback", "Interrupting Process Not Found" )
	    << "Unable to find interrupting process (PID: 0x" << MLUCLog::kHex
	    << pid << "/" << MLUCLog::kDec << pid << (process_id ? " - ID: " : "") 
	    << (process_id ? process_id : "") << " - Address: 0x"
	    << MLUCLog::kHex << (process_address ? (unsigned long)process_address : (unsigned long)0 ) << ")." << ENDLOG;
	}
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
