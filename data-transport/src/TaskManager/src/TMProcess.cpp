/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TMPythonInterface.hpp"
#include "TMProcess.hpp"
#include "TMSysException.hpp"
#include "TMSystem.hpp"
#include "TMProcessExecutionStateHandler.hpp"
#include "TMInterfaceLibrary.hpp"
#include "TMInterruptWaitThread.hpp"
#include "TMConfig.hpp"
#include "TMDefinitions.hpp"
#include <MLUCCmdlineParser.hpp>
#include <MLUCLog.hpp>
#include <sys/types.h>
#include <signal.h>
#include <errno.h>
#include <sys/wait.h>
#include <poll.h>
#include <fcntl.h>


TMProcess::TMProcess( TMSystem* sys, const char* id, unsigned long proc_ndx, TMConfig::ProcessData& data ):
    fID( id ), fProcNdx( proc_ndx ), fStateChangeTimeoutCallback( this )
    {
    MLUCMutex::TLocker stateDataLocker( fStateDataMutex );
    fSystem = sys;
    fEnabled = true;
    fIsProgram = true;
    fIsSlave = false;
    fIsProxy = false;
    fPID = -1;
    fRetVal = 0;
    fNormalExit = false;
    fSignalExit = false;
    fStarted = false;
    fStartedBySelf = false;
    fTerminationFlag = true;
    fWasRunning = false;
    fFailOverPassiveMonitorPID = -1;

    fStateChangeForced = false;
    fConnectionRequested = false;
    fConnectionEstablished = false;

    if ( data.fType == TMConfig::ProcessData::kProgram )
	{
	fIsProgram = true;
	fIsSlave = false;
	fIsProxy = false;
	}
    else if ( data.fType == TMConfig::ProcessData::kSlave )
	{
	fIsProgram = false;
	fIsSlave = true;
	fIsProxy = false;
	}
    else if ( data.fType == TMConfig::ProcessData::kSlaveProgram )
	{
	fIsProgram = true;
	fIsSlave = true;
	fIsProxy = false;
	}
    else if ( data.fType == TMConfig::ProcessData::kProxy )
	{
	fIsProgram = false;
	fIsSlave = false;
	fIsProxy = true;
	}

    if ( data.fInterfaceLibrary.fActive )
	{
	fInterfaceLibrary = new TMInterfaceLibrary( data.fInterfaceLibrary.fPlatform.c_str(), data.fInterfaceLibrary.fBinary.c_str() );
	if ( !fInterfaceLibrary )
	    throw TMSysException( ENOMEM, strerror(ENOMEM), "Unable to create process private interface library object",
				  __FILE__, __LINE__ );
    	}
    else
	fInterfaceLibrary = NULL;

    // Create the interrupt wait thread.
    if ( data.fInterruptListenAddressActive )
	{
	fInterruptWaitThread = new TMInterruptWaitThread( this, fSystem, data.fInterruptListenAddress.c_str() );
	if ( !fInterruptWaitThread )
	    throw TMSysException( ENOMEM, strerror(ENOMEM), "Unable to create interrupt wait thread",
				  __FILE__, __LINE__ );
	}
    else
	fInterruptWaitThread = NULL;

    //fSystem->NormalizeAddress( data.fAddress.c_str(), fAddress );
    // fAddress = data.fAddress;
    fAddressString = data.fAddress;
    int ret = GetInterfaceLibrary()->ConvertAddress( fAddressString.c_str(), &fAddress );
    if ( ret )
    {
      char tmperrormsg[200];
      snprintf(tmperrormsg, 200, "Unable to convert address string %s", fAddressString.c_str());
	throw TMSysException( ret, strerror(ret), tmperrormsg,
			      __FILE__, __LINE__ );
    }

    vector<MLUCString> cmdTokens;
    MLUCCmdlineParser parser;
    if ( !parser.ParseCmd( data.fCommand.c_str(), cmdTokens ) )
	throw TMSysException( EINVAL, strerror(EINVAL), "Unable to parse process command",
				     __FILE__, __LINE__ );
    fCmd = data.fCommand;
    fCmdTokens = cmdTokens;

    fStateChangeAction = data.fStateChangeAction;
    fPreStartAction = data.fPreStartAction;
    fPostStartAction = data.fPostStartAction;
    fPreTerminateAction = data.fPreTerminateAction;
    fPostTerminateAction = data.fPostTerminateAction;
    fInterruptReceivedAction = data.fInterruptReceivedAction;
    fPreConfigChangePhase1Action = data.fPreConfigChangePhase1Action;
    fPreConfigChangePhase2Action = data.fPreConfigChangePhase2Action;
    fPreConfigChangePhase3Action = data.fPreConfigChangePhase3Action;
    fPostConfigChangeAction = data.fPostConfigChangeAction;

    vector<TMConfig::UsedResourceData>::iterator resIter, resEnd;
    TMUsedResourceData::UsedResourceType resType;
    TMUsedResourceData* resourceData;
    resIter = data.fUsedResources.begin();
    resEnd = data.fUsedResources.end();
    while ( resIter != resEnd )
	{
	if ( resIter->fType=="File" )
	    resType = TMUsedResourceData::kFile;
	else if ( resIter->fType=="SysVShm" )
	    resType = TMUsedResourceData::kSysVShm;
	else if ( resIter->fType=="GenericCleanup" )
	    resType = TMUsedResourceData::kGenericCleanup;
	else
	    {
	    LOG( MLUCLog::kFatal, "TMProcess::TMProcess", "Internal Error" )
		<< "Internal Error: Configuration supplied unknown resource type '"
		<< resIter->fType.c_str() << "'." << ENDLOG;
	    throw TMSysException( ECANCELED, strerror(ECANCELED), "Internal Error: Configuration supplied unknown resource type",
				__FILE__, __LINE__ );
	    }
	resourceData = new TMUsedResourceData( resType, resIter->fContent.c_str() );
	fUsedResources.insert( fUsedResources.end(), resourceData );
	resIter++;
	}
    fCleanupAction = data.fCleanupAction;
    fFailOverPassiveKillAction = data.fFailOverPassiveKillAction;
    fFailOverPassiveMonitorCommand = data.fFailOverPassiveMonitorCommand;
    fStateChangeTimeoutID = ~(uint32)0;
    fStateChangeTimeoutSet = false;
    pthread_mutex_init( &fStateChangeTimeoutMutex, NULL );

    fStatesData = data.fStatesData;

    pthread_mutex_init( &fStatesMutex, NULL );
    pthread_mutex_init( &fComErrorsSuppressionMutex, NULL );

    fComErrorsSuppressionTimeout.tv_sec = fComErrorsSuppressionTimeout.tv_usec = 0;

    fReplyTimeoutRepeatCnt = 0;

    fStdout = fStderr = -1;
    }

TMProcess::~TMProcess()
    {
    FreeUsedResourcesData();
    pthread_mutex_destroy( &fStateChangeTimeoutMutex );
    pthread_mutex_destroy( &fStatesMutex );
    pthread_mutex_destroy( &fComErrorsSuppressionMutex );
    if ( fStdout!=-1 )
	close( fStdout );
    if ( fStderr!=-1 )
	close( fStderr );
    }

void TMProcess::SetAddressFromString( const char* addrStr )
    {
    fAddressString = addrStr;
    int ret = GetInterfaceLibrary()->ConvertAddress( fAddressString.c_str(), &fAddress );
    if ( ret )
	throw TMSysException( ret, strerror(ret), "Unable to convert address string",
			      __FILE__, __LINE__ );
    }

void TMProcess::SetAddress( const char* addrStr, void* addr )
    {
    fAddressString = addrStr;
    fAddress = addr;
    }

bool TMProcess::SetCommand( const char* cmd ) // Returns false when command could not be parsed.
    {
    vector<MLUCString> cmdTokens;
    
    MLUCCmdlineParser parser;
    if ( !parser.ParseCmd( cmd, cmdTokens ) )
	return false;
    fCmd = cmd;
    fCmdTokens = cmdTokens;
    return true;
    }

void TMProcess::SetUsedResources( const vector<TMUsedResourceData*>& res )
    {
    fUsedResources = res;
    }

void TMProcess::GetUsedResources( vector<TMUsedResourceData*>& res )
    {
    res = fUsedResources;
    }

void TMProcess::FreeUsedResourcesData()
    {
    while ( fUsedResources.size()>0 )
	{
	delete *(fUsedResources.begin());
	fUsedResources.erase( fUsedResources.begin() );
	}
    }

void TMProcess::ForceStateChangeTimeout( unsigned long timeout_us )
    {
    if ( !fSystem )
	return;
    pthread_mutex_lock( &fStateChangeTimeoutMutex );
    if ( !fStateChangeTimeoutSet )
	{
	LOG( MLUCLog::kDebug, "TMProcess::ForceStateChangeTimeout", "Setting forced state change timeout" )
		 << "Setting forced state change timeout for process " << fID.c_str() << ENDLOG;
	fStateChangeTimeoutID = fSystem->GetTimer().AddTimeout( timeout_us/1000, &fStateChangeTimeoutCallback, 
								reinterpret_cast<uint64>( this ) );
	fStateChangeTimeoutSet = true;
	}
    pthread_mutex_unlock( &fStateChangeTimeoutMutex );
    }


void TMProcess::StateChange()
    {
    if ( fEnabled )
	{
	LOG( MLUCLog::kDebug, "TMProcess::StateChange", "Forcing state change" )
		 << "Forcing state change for process " << fID.c_str() << ENDLOG;
	fSystem->GetMainPythonInterface()->Run( GetStateChangeAction() );
	}
    }

void TMProcess::Interrupt()
    {
    if ( fEnabled )
	fSystem->GetMainPythonInterface()->Run( GetInterruptReceivedAction() );
    }



void TMProcess::Start()
    {
#if 1
    if ( !fEnabled )
	return;
    if ( fInterruptWaitThread && !GetInterfaceLibrary()->InterruptWaitRunning() )
	{
	fInterruptWaitThread->Start();
	}
    pid_t pid;
    unsigned long i, argc = fCmdTokens.size();
    char* argv[ argc+1 ];
    int ret;
    if ( fIsProgram )
	{
	MLUCMutex::TLocker stateDataLocker( fStateDataMutex );
	if ( fPID!=-1 )
	    throw TMSysException( EBUSY, strerror(EBUSY), "Process already started",
				  __FILE__, __LINE__ ); // Or just return??
	stateDataLocker.Unlock();
	for ( i = 0; i < argc; i++ )
	    {
	    argv[i] = (char*)fCmdTokens[i].c_str();
	    //  	printf( "Cmd token %d: '%s'\n", i, argv[i] );
	    }
	argv[argc] = NULL;
	//     printf( "Cmd token %d: '%s'\n", argc, argv[argc] );
	}
	
    fSystem->GetMainPythonInterface()->Run( GetPreStartAction() );

    bool failOverActive = fSystem->GetFailOverActive();
    fStartedBySelf = failOverActive;
    fTerminationFlag = false;
    
    if ( fIsProgram && failOverActive )
	{
	int fdOut[2] = { -1, -1 };
	int fdErr[2] = { -1, -1 };
	ret = pipe( fdOut );
	if ( ret )
	    {
	    LOG( MLUCLog::kWarning, "TMProcess::Start", "Pipe creation error" )
		<< "Error creating pipes for child process " << fID.c_str() << " output redirection: "
		<< strerror(errno) << " (" << MLUCLog::kDec << errno << "). Will not be able to capture process output (stdout)." << ENDLOG;
	    }
	ret = pipe( fdErr );
	if ( ret )
	    {
	    LOG( MLUCLog::kWarning, "TMProcess::Start", "Pipe creation error" )
		<< "Error creating pipes for child process " << fID.c_str() << " error output redirection: "
		<< strerror(errno) << " (" << MLUCLog::kDec << errno << "). Will not be able to capture process error output (stderr)." << ENDLOG;
	    }
	pid = fork();
	//     if ( pid==-1 )
	// 	throw TMSysException( errno, strerror(errno) );
	if ( !pid )
	    {
	    close( fdOut[0] );
	    dup2( fdOut[1], 1 );
	    setbuf( stdout, NULL );
	    // fcntl( 1, F_SETFL, O_NONBLOCK ); // Child should not not block (indeed double negative)
	    close( fdOut[1] );
	    close( fdErr[0] );
	    dup2( fdErr[1], 2 );
	    setbuf( stderr, NULL );
	    // fcntl( 2, F_SETFL, O_NONBLOCK ); // Child should not not block (indeed double negative)
	    close( fdErr[1] );
	    execvp( argv[0], argv );
	    int ret = errno;
	    LOG( MLUCLog::kError, "TMProcess::Cleanup", "Resource cleanup error" )
		<< "====================\nError starting process "
		<< fID.c_str() << ": " << strerror(ret) << MLUCLog::kDec
		<< " (" << ret << ")." << ENDLOG;
	    fSystem->GetProcessExecutionStateHandler()->SetProcessExecutionStatus( fProcNdx, ret );
	    exit( ret );
	    }
	else
	    {
	    close( fdOut[1] );
	    fcntl( fdOut[0], F_SETFL, O_NONBLOCK );
	    close( fdErr[1] );
	    fcntl( fdErr[0], F_SETFL, O_NONBLOCK );
	    fStdout = fdOut[0];
	    fStderr = fdErr[0];
	    fSystem->GetProcessExecutionStateHandler()->SetProcessExecutionStatus( pid, fProcNdx, pid==-1 ? false : true, pid==-1 ? errno : 0 );
	    if ( pid==-1 )
		throw TMSysException( errno, strerror(errno), "Error forking to execute process",
				      __FILE__, __LINE__ );
	    //usleep( 250000 );
	    MLUCMutex::TLocker stateDataLocker( fStateDataMutex );
	    fPID = pid;
	    fWasRunning = fStarted = true;
	    }
	}
    else if ( fIsProgram && !failOverActive )
	fStarted = true;
    if ( fIsProxy )
	ForceStateChange();

    if ( fStarted || !fIsProgram )
	fSystem->GetMainPythonInterface()->Run( GetPostStartAction() );

    if ( fIsSlave && failOverActive )
	{
	ret = GetInterfaceLibrary()->SendCommand( 0, fID.c_str(), fAddress, "TM:START" );
	if ( ret )
	    {
	    if ( !SuppressComErrorLogs() )
		{
		throw TMSysException( ret, strerror(ret), "Error sending 'TM:START' command to process",
				      __FILE__, __LINE__ );
		}
	    }
	}
#else
#warning Functionality ifdefd out
    printf( "Warning functionality ifdefd out...\n" );
#endif
    }

bool TMProcess::Kill( bool )
    {
#if 1
    MLUCMutex::TLocker stateDataLocker( fStateDataMutex );
    if ( (fPID<0 && fIsProgram) || !fEnabled )
	return true;
    stateDataLocker.Unlock();
    int retslave=0, retprogram=0;

    bool failOverActive = fSystem->GetFailOverActive();
    fTerminationFlag = true;

    fSystem->GetMainPythonInterface()->Run( GetPreTerminateAction() );

    if ( fIsSlave && !fIsProgram && failOverActive )
	{
	retslave = GetInterfaceLibrary()->SendCommand( 0, fID.c_str(), fAddress, "TM:KILL" );
// 	if ( ret )
// 	    {
// 	    throw TMSysException( ret, strerror(ret), "Error sending 'TM:KILL' command to process",
// 				  __FILE__, __LINE__ );
// 	    }
	}
    if ( fIsSlave && fIsProgram && failOverActive )
	{
	retslave = GetInterfaceLibrary()->SendCommand( 0, fID.c_str(), fAddress, "TM:TERMINATE" );
// 	if ( ret )
// 	    {
// 	    throw TMSysException( ret, strerror(ret), "Error sending 'TM:KILL' command to process",
// 				  __FILE__, __LINE__ );
// 	    }
	usleep( 5000 );
	}
    if ( fIsProgram && failOverActive )
	{
	stateDataLocker.Lock();
	if ( fStartedBySelf )
	    {
	    if ( fPID > 0 )
		{
		pid_t pid;
		int st;
		retprogram = kill( fPID, SIGQUIT );
		stateDataLocker.Unlock();
		usleep( 5000 );
		stateDataLocker.Lock();
		pid = waitpid( fPID, &st, WNOHANG );
		if ( pid==fPID )
		    {
		    fPID=-1;
		    if ( WIFEXITED(st) )
			{
			fNormalExit = true;
			fRetVal = WEXITSTATUS(st);
			}
		    else if ( WIFSIGNALED(st) )
			{
			fSignalExit = true;
			fRetVal = WTERMSIG(st);
			}
		    fWasRunning = fStarted = fStartedBySelf = false;
		    }
		
		if ( !retprogram && fPID>0 )
		    {
		    stateDataLocker.Unlock();
		    usleep( 5000 );
		    stateDataLocker.Lock();
		    retprogram = kill( fPID, SIGTERM );
		    stateDataLocker.Unlock();
		    usleep( 5000 );
		    stateDataLocker.Lock();
		    pid = waitpid( fPID, &st, WNOHANG );
		    if ( pid==fPID )
			{
			fPID=-1;
			if ( WIFEXITED(st) )
			    {
			    fNormalExit = true;
			    fRetVal = WEXITSTATUS(st);
			    }
			else if ( WIFSIGNALED(st) )
			    {
			    fSignalExit = true;
			    fRetVal = WTERMSIG(st);
			    }
			fWasRunning = fStarted = fStartedBySelf = false;
			}
		    }
		if ( !retprogram && fPID>0 )
		    {
		    stateDataLocker.Unlock();
		    usleep( 5000 );
		    stateDataLocker.Lock();
		    retprogram = kill( fPID, SIGKILL );
		    stateDataLocker.Unlock();
		    usleep( 5000 );
		    stateDataLocker.Lock();
		    pid = waitpid( fPID, &st, WNOHANG );
		    if ( pid==fPID )
			{
			fPID=-1;
			if ( WIFEXITED(st) )
			    {
			    fNormalExit = true;
			    fRetVal = WEXITSTATUS(st);
			    }
			else if ( WIFSIGNALED(st) )
			    {
			    fSignalExit = true;
			    fRetVal = WTERMSIG(st);
			    }
			fWasRunning = fStarted = fStartedBySelf = false;
			}
		    }
		}
	    stateDataLocker.Unlock();
	    }
	else
	    {
	    stateDataLocker.Unlock();
	    fSystem->GetMainPythonInterface()->Run( GetFailOverPassiveKillAction() );
	    fWasRunning = fStarted = false;
	    }
//     fPID = -1;
	fSystem->GetProcessExecutionStateHandler()->SetProcessExecutionStatus( fProcNdx );
	}
    fSystem->GetMainPythonInterface()->Run( GetPostTerminateAction() );
    if ( retprogram || retslave )
	return false;
#else
#warning Functionality ifdefd out
    printf( "Warning functionality ifdefd out...\n" );
#endif
    return true;
    }

bool TMProcess::Cleanup()
    {
    if ( !fSystem->GetFailOverActive() )
	{
	return true;
	}
    bool success = true;
#if 1
    vector<TMUsedResourceData*> resources;
    vector<TMUsedResourceData*>::iterator iter, end;
    GetUsedResources( resources );
    
    iter = resources.begin();
    end = resources.end();
    while ( iter != end )
	{
	if ( *iter )
	    {
	    try
		{
		(*iter)->Cleanup();
		}
	    catch ( TMSysException e )
		{
		LOG( MLUCLog::kWarning, "TMProcess::Cleanup", "Resource cleanup error" )
		    << "Process '" << GetID() << "' unable to cleanup " << (*iter)->GetTypeName()
		    << " resource '" << (*iter)->GetName() << "': " << e.GetErrorStr()
		    << MLUCLog::kDec << " (" << e.GetErrno() << "): "
		    << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
		    << e.GetLine() << ")" << "." << ENDLOG;
		success = false;
		}
	    }
	iter++;
	}
//     if ( !fIsProgram )
// 	{
//      int ret;
// 	ret = GetInterfaceLibrary()->SendCommand( 0, fID.c_str(), fAddress, "TM:CLEANUP" );
// 	}
    fSystem->GetMainPythonInterface()->Run( GetCleanupAction() );
#else
#warning Functionality ifdefd out
    printf( "Warning functionality ifdefd out...\n" );
#endif
    return success;
    }

// void TMProcess::HasTerminated( int& retval )
//     {
//     fPID = -1;
//     fSystem->GetProcessExecutionStateHandler()->ResetProcessExecutionStatus( fProcNdx );    
//     }

void TMProcess::CheckState( MLUCString& state )
    {
    QueryOldState( state );
#if 0
    bool hasChanged;
    bool running;
    bool normalExit;
    bool signalExit;
    int retval;
    CheckState( hasChanged, running, normalExit, signalExit, retval, state, false );
#endif
    }


void TMProcess::CheckState( bool& hasChanged, bool& running, bool& normalExit, bool& signalExit, 
			    int& retval, MLUCString& state, bool store )
    {
    if ( !fEnabled )
	{
	hasChanged = false;
	running = false;
	normalExit = false;
	signalExit = false;
	retval = -1;
	state = "TM:DISABLED";
	}
    bool failOverActive = fSystem->GetFailOverActive();
    bool finished = false;
    bool wasStartedBySelf = fStartedBySelf;
#if 0
    if ( fIsProgram && wasStartedBySelf )
	{
	if ( fStdout!=-1 )
	    {
	    struct pollfd pollFD;
	    pollFD.fd = fStdout;
	    pollFD.events = POLLIN;
	    pollFD.revents = 0;
	    int pollRet = poll( &pollFD, 1, 0 );
	    if ( pollRet>0 )
		{
		char buffer[4097];
		int ret;
		do
		    {
		    ret = read( fStdout, buffer, 4096 );
		    if ( ret>0 )
			{
			buffer[ret] = 0;
			fStdoutBuffer.Append( buffer );
			}
		    }
		while ( ret==4096 );
		MLUCString::TSizeType ndx;
		while ( (ndx = fStdoutBuffer.Find( '\n' ))!=MLUCString::kNoPos )
		    {
		    LOG( MLUCLog::kWarning, "TMProcess::CheckState", "Process Output" )
			<< "Process " << fID.c_str() << ": " << fStdoutBuffer.Substr( 0, ndx ).c_str() << ENDLOG;
		    fStdoutBuffer.Erase( 0, ndx+1 );
		    }
		}
	    }
	}
#endif
    if ( fIsProgram && ( fStartedBySelf || (failOverActive && !fStarted) ) )
	{
	pid_t pid;
	int st;
	bool didNotStart = false;
	MLUCMutex::TLocker stateDataLocker( fStateDataMutex );
	if ( fPID!=-1 )
	    {
	    pid = waitpid( fPID, &st, WNOHANG );
	    if ( pid==fPID )
		{
		fPID=-1;
		if ( WIFEXITED(st) )
		    {
		    fNormalExit = true;
		    fRetVal = WEXITSTATUS(st);
		    // 		printf( "Normal exit: %d\n", fRetVal );
		    }
		else if ( WIFSIGNALED(st) )
		    {
		    fSignalExit = true;
		    fRetVal = WTERMSIG(st);
		    // 		printf( "Signal exit: %d\n", fRetVal );
		    }
		fStartedBySelf = false;
		fStarted = false;
		fWasRunning = false;
		stateDataLocker.Unlock();
		TMProcessExecutionStateHandler::ProcessStatus procExecState;
		procExecState = fSystem->GetProcessExecutionStateHandler()->GetProcessStatus( fProcNdx );
		if ( procExecState==TMProcessExecutionStateHandler::kExecFailed )
		    {
		    didNotStart = true;
		    // 		printf( "Did not start\n", fRetVal );
		    }
		stateDataLocker.Lock();
		}
	    }
	if ( fPID==-1 )
	    {
	    running = false;
	    retval = fRetVal;
	    normalExit = fNormalExit;
	    signalExit = fSignalExit;
	    if ( didNotStart )
		state = "TM:COULDNOTSTART";
	    else if ( fTerminationFlag )
		state = "TM:NOTSTARTED";
	    else
		state = "TM:DEAD";
	    finished = true;
	    }
	}
    else if ( fIsProgram /* && failOverActive*/ && !fFailOverPassiveMonitorCommand.IsEmpty() && !fFailOverPassiveMonitorCommand.IsWhiteSpace() )
	{
	// We are currently the passive partner or the active partner in active passive failover, but did not start this process by ourselves...
	MLUCMutex::TLocker lock( fFailOverPassiveMonitorMutex );
	if ( fFailOverPassiveMonitorPID==-1 )
	    {
	    vector<MLUCString> cmdTokens;
	    MLUCCmdlineParser parser;
	    if ( parser.ParseCmd( fFailOverPassiveMonitorCommand.c_str(), cmdTokens ) )
		{
		unsigned long i, argc = cmdTokens.size();
		char* argv[ argc+1 ];
		for ( i = 0; i < argc; i++ )
		    {
		    argv[i] = (char*)cmdTokens[i].c_str();
		    }
		argv[argc] = NULL;
		
		fFailOverPassiveMonitorPID = fork();
		//     if ( pid==-1 )
		// 	throw TMSysException( errno, strerror(errno) );
		if ( !fFailOverPassiveMonitorPID )
		    {
		    execvp( argv[0], argv );
		    int ret = errno;
		    LOGM( MLUCLog::kError, "TMProcess::CheckState", "Error executing FailOver passive monitor command", 25 )
			<< "Error executing FailOver passive monitor command for process "
			<< fID.c_str() << ": " << strerror(ret) << MLUCLog::kDec
			<< " (" << ret << ")." << ENDLOG;
		    exit(-1);
		    }
		}
	    else
		{
		LOGM( MLUCLog::kError, "TMProcess::CheckState", "Cannot parse failover passive monitor command", 25 )
		    << "Unable to determine parse failover monitor command '"
		    << fFailOverPassiveMonitorCommand.c_str() << "' for process "
		    << fID.c_str() << ".." << ENDLOG;
		}
	    }
	if ( fFailOverPassiveMonitorPID!=-1 )
	    {
	    pid_t pid;
	    int st;
	    struct timeval start, now;
	    gettimeofday( &start, NULL );
	    unsigned long tdiff = 0;
	    int remote_retval = -1;
	    bool remote_normalExit = false;
	    do
		{
		if ( tdiff>0 )
		    usleep(0);
		pid = waitpid( fFailOverPassiveMonitorPID, &st, WNOHANG );
		if ( pid==fFailOverPassiveMonitorPID )
		    {
		    fFailOverPassiveMonitorPID=-1;
		    if ( WIFEXITED(st) )
			{
			remote_retval = WEXITSTATUS(st); // remote retval >0 signals not running (can be later differentiated further), remote retval==0 means running
			remote_normalExit = true;
			}
		    }
		else
		    {
		    gettimeofday( &now, NULL );
		    tdiff = (now.tv_sec-start.tv_sec)*1000000 + (now.tv_usec-start.tv_usec);
		    }
		}
	    while ( fFailOverPassiveMonitorPID!=-1 && tdiff<500000 );
	    if ( fFailOverPassiveMonitorPID!=-1 )
		{
		LOGM( MLUCLog::kError, "TMProcess::CheckState", "Unable to determine passive monitor process state", 25 )
		    << "Unable to determine process state for failover (remote partner) started process "
		    << fID.c_str() << ". Monitor command has not returned yet." << ENDLOG;
		}
	    else if ( remote_retval>0 )
		{
		running = false;
		retval = -1;
		normalExit = true;
		signalExit = false;
		if ( fWasRunning )
		    fStarted = false;
		if ( fTerminationFlag )
		    state = "TM:NOTSTARTED";
		else
		    state = "TM:DEAD";
		finished = true;
		fWasRunning = false;
		}
	    else
		fWasRunning = true;

	    }
	}
    if ( fIsProgram && wasStartedBySelf )
	{
	if ( fStderr!=-1 )
	    {
#if 1
	    struct pollfd pollFD;
	    pollFD.fd = fStderr;
	    pollFD.events = POLLIN;
	    pollFD.revents = 0;
	    int pollRet = poll( &pollFD, 1, 0 );
	    if ( pollRet>0 )
		{
		char buffer[4097];
		int ret;
		do
		    {
		    ret = read( fStderr, buffer, 4096 );
		    if ( ret>0 )
			{
			buffer[ret] = 0;
			fStderrBuffer.Append( buffer );
			}
		    }
		while ( ret==4096 );
		MLUCString::TSizeType ndx;
		while ( (ndx = fStderrBuffer.Find( '\n' ))!=MLUCString::kNoPos )
		    {
		    LOG( MLUCLog::kError, "TMProcess::CheckState", "Process Error Output" )
			<< "Process " << fID.c_str() << " error output: " << fStderrBuffer.Substr( 0, ndx ).c_str() << ENDLOG;
		    fStderrBuffer.Erase( 0, ndx+1 );
		    }
		}
#endif
	    if ( finished && !running )
		{
		if ( fStderrBuffer.Length()>0 )
		    {
		    LOG( MLUCLog::kError, "TMProcess::CheckState", "Process Error Output" )
			<< "Process " << fID.c_str() << " error output: " << fStderrBuffer.c_str() << ENDLOG;
		    fStderrBuffer = "";
		    }
		close( fStderr );
		fStderr = -1;
		}
	    }
	if ( fStdout!=-1 )
	    {
#if 1
	    struct pollfd pollFD;
	    pollFD.fd = fStdout;
	    pollFD.events = POLLIN;
	    pollFD.revents = 0;
	    int pollRet = poll( &pollFD, 1, 0 );
	    if ( pollRet>0 )
		{
		char buffer[4097];
		int ret;
		do
		    {
		    ret = read( fStdout, buffer, 4096 );
		    if ( ret>0 )
			{
			buffer[ret] = 0;
			fStdoutBuffer.Append( buffer );
			}
		    }
		while ( ret==4096 );
		MLUCString::TSizeType ndx;
		while ( (ndx = fStdoutBuffer.Find( '\n' ))!=MLUCString::kNoPos )
		    {
		    LOG( MLUCLog::kWarning, "TMProcess::CheckState", "Process Standard Output" )
			<< "Process " << fID.c_str() << " standard output: " << fStdoutBuffer.Substr( 0, ndx ).c_str() << ENDLOG;
		    fStdoutBuffer.Erase( 0, ndx+1 );
		    }
		}
#endif
	    if ( finished && !running )
		{
		if ( fStdoutBuffer.Length()>0 )
		    {
		    LOG( MLUCLog::kWarning, "TMProcess::CheckState", "Process Standard Output" )
			<< "Process " << fID.c_str() << " standard output: " << fStdoutBuffer.c_str() << ENDLOG;
		    fStdoutBuffer = "";
		    }
		close( fStdout );
		fStdout = -1;
		}
	    }
	}
    if ( finished )
	{
	MLUCMutex::TLocker stateDataLocker( fStateDataMutex );
	if ( fOldState!=state && ! ((fOldState == "running" && state == "busy") || (fOldState == "busy" && state == "running")))
	    {
	    hasChanged = true;
	    if ( store )
		fOldState = state;
	    }
	else
	    hasChanged = false;
	if ( fStateChangeForced )
	    {
	    hasChanged = true;
	    if ( store )
		fStateChangeForced = false;
	    }
	if ( hasChanged && fStateChangeTimeoutSet && store )
	    {
	    stateDataLocker.Unlock();
	    pthread_mutex_lock( &fStateChangeTimeoutMutex );
	    if ( fStateChangeTimeoutSet )
		{
		if ( fSystem )
		    fSystem->GetTimer().CancelTimeout( fStateChangeTimeoutID );
		fStateChangeTimeoutID = ~(uint32)0;
		fStateChangeTimeoutSet = false;
		}
	    pthread_mutex_unlock( &fStateChangeTimeoutMutex );
	    }
	return;
	}
    running = true;
    char* proc_state=NULL;
    int ret;
    ret = GetInterfaceLibrary()->QueryState( fPID, GetID(),
					     GetAddress(), 
					     &proc_state );
    if ( ret )
	{
	//state = "TM:UNKNOWN";
	state = "";
// 	throw TMSysException( ret, strerror(ret), "Error getting process state",
// 			      __FILE__, __LINE__ );
	if ( !SuppressComErrorLogs() && failOverActive && (ret!=ETIMEDOUT || ++fReplyTimeoutRepeatCnt>=gkReplyTimeoutErrorRepeatErrorCount) )
	    {
	    LOG( MLUCLog::kError, "TMProcess::CheckState", "Error getting process state" )
		<< "Error getting process '" << GetID() << "' state: " << strerror(ret) << MLUCLog::kDec
		<< " (" << ret << ")." << ENDLOG;
	    }
	}
    else
	{
	state = proc_state;
	GetInterfaceLibrary()->ReleaseResource( proc_state );
	fReplyTimeoutRepeatCnt = 0;
	}
    MLUCMutex::TLocker stateDataLocker( fStateDataMutex );
    if ( fOldState!=state )
	{
	hasChanged = true;
	if ( store )
	    fOldState = state;
	}
    else
	hasChanged = false;
    if ( fStateChangeForced )
	{
	hasChanged = true;
	if ( store )
	    fStateChangeForced = false;
	}
    if ( hasChanged && fStateChangeTimeoutSet )
	{
	stateDataLocker.Unlock();
	pthread_mutex_lock( &fStateChangeTimeoutMutex );
	if ( fStateChangeTimeoutSet )
	    {
	    if ( fSystem )
		fSystem->GetTimer().CancelTimeout( fStateChangeTimeoutID );
	    fStateChangeTimeoutID = ~(uint32)0;
	    fStateChangeTimeoutSet = false;
	    }
	pthread_mutex_unlock( &fStateChangeTimeoutMutex );
	}
    }

void TMProcess::GetStatusData( MLUCString& statusData )
    {
    if ( !fEnabled )
	{
	statusData = "";
	return;
	}
    bool hasChanged;
    bool running;
    bool normalExit;
    bool signalExit;
    MLUCString state;
    int retval;
    CheckState( hasChanged, running, normalExit, signalExit, retval, state, false );
    if ( !running )
	{
	statusData = "";
	return;
	}
    char* proc_status=NULL;
    int ret;
    pid_t pid;
	{
	MLUCMutex::TLocker stateDataLocker( fStateDataMutex );
	pid = fPID;
	}
    ret = GetInterfaceLibrary()->QueryStatusData( pid, GetID(),
						  GetAddress(), 
						  &proc_status );
    if ( ret )
	{
	statusData = "TM:UNKNOWN";
// 	throw TMSysException( ret, strerror(ret), "Error getting process status data",
// 			      __FILE__, __LINE__ );
	if ( !SuppressComErrorLogs() && (ret!=ETIMEDOUT || ++fReplyTimeoutRepeatCnt>=gkReplyTimeoutErrorRepeatErrorCount) )
	    {
	    LOG( MLUCLog::kError, "TMProcess::GetStatusData", "Error getting process status data" )
		<< "Error getting process '" << GetID() << "' status data: " << strerror(ret) << MLUCLog::kDec
		<< " (" << ret << ")." << ENDLOG;
	    }
	}
    else
	fReplyTimeoutRepeatCnt = 0;
    statusData = proc_status;
    GetInterfaceLibrary()->ReleaseResource( proc_status );
    }

void TMProcess::SuppressComErrorLogs( unsigned long timeout )
    {
    struct timeval now;
    gettimeofday( &now, NULL );
    now.tv_usec += timeout;
    while ( now.tv_usec>=1000000 )
	{
	now.tv_usec -= 1000000;
	now.tv_sec++;
	}
    pid_t pid;
	{
	MLUCMutex::TLocker stateDataLocker( fStateDataMutex );
	pid = fPID;
	}
    int ret = GetInterfaceLibrary()->SuppressComErrorLogs( pid, GetID(), GetAddress(), timeout );
    if ( ret )
	{
// 	throw TMSysException( ret, strerror(ret), "Error suppressing communication error logs",
// 			      __FILE__, __LINE__ );
	LOG( MLUCLog::kError, "TMProcess::GetStatusData", "Error suppressing communication error logs" )
	    << "Error suppressing process '" << GetID() << "' communication error logs: " << strerror(ret) << MLUCLog::kDec
	    << " (" << ret << ")." << ENDLOG;
	}
    pthread_mutex_lock( &fComErrorsSuppressionMutex );
    if ( fComErrorsSuppressionTimeout.tv_sec<now.tv_sec ||
	 ( fComErrorsSuppressionTimeout.tv_sec==now.tv_sec && fComErrorsSuppressionTimeout.tv_usec<now.tv_usec ) )
	{
	fComErrorsSuppressionTimeout = now;
	}
    pthread_mutex_unlock( &fComErrorsSuppressionMutex );
    }


bool TMProcess::SuppressComErrorLogs()
    {
    bool ret=false;
    struct timeval now;
    gettimeofday( &now, NULL );
    pthread_mutex_lock( &fComErrorsSuppressionMutex );
    if ( fComErrorsSuppressionTimeout.tv_sec>now.tv_sec ||
	 ( fComErrorsSuppressionTimeout.tv_sec==now.tv_sec && fComErrorsSuppressionTimeout.tv_usec>now.tv_usec ) )
	{
	ret=true;
	}
    pthread_mutex_unlock( &fComErrorsSuppressionMutex );
    return ret;
    }

int TMProcess::SendCommand( const char* cmd, unsigned long timeout )
    {
#if 1
    if ( !fSystem->GetFailOverActive() )
	{
	return 0;
	}
#endif
    TMInterfaceLibrary* lib = GetInterfaceLibrary();
    if ( !lib )
	{
	LOG( MLUCLog::kError, "TMProcess::SendCommand", "Cannot find Interface Library" )
	    << "Unable to get interface library." << ENDLOG;
	throw TMSysException( EFAULT, strerror(EFAULT), "Internal Error: No Interface library supplied",
				  __FILE__, __LINE__ );
	}
    int ret = 0;
    try
	{
	if ( fConnectionRequested && !fConnectionEstablished )
	    {
	    ret = lib->ConnectToProcess( GetPID(), fID.c_str(), fAddress );
	    if ( !ret )
		fConnectionEstablished = true;
	    }
	if ( !ret )
	    ret = lib->SendCommand( GetPID(), fID.c_str(), fAddress, cmd );
	}
    catch ( ... )
	{
	if ( timeout != ~(unsigned long)0 )
	    ForceStateChangeTimeout( timeout );
	throw;
	}
    if ( timeout != ~(unsigned long)0 )
	ForceStateChangeTimeout( timeout );
    return 0;
    }



void TMProcess::Reset()
    {
//     printf( "Reset called\n" );
    MLUCMutex::TLocker stateDataLocker( fStateDataMutex );
    if ( fPID==-1 )
	{
	fRetVal = 0;
	fNormalExit = false;
	fSignalExit = false;
	fStarted = false;
	fStartedBySelf = false;
	fWasRunning = false;
	fTerminationFlag = true;
	stateDataLocker.Unlock();
	fSystem->GetProcessExecutionStateHandler()->ResetProcessExecutionStatus( fProcNdx );    
// 	printf( "Reset executed\n" );
	}
    }

void TMProcess::ConfigChangingPhase1()
    {
    if ( fEnabled )
	fSystem->GetMainPythonInterface()->Run( GetPreConfigChangePhase1Action() );
    }

void TMProcess::ConfigChangingPhase2()
    {
    if ( fEnabled )
	fSystem->GetMainPythonInterface()->Run( GetPreConfigChangePhase2Action() );
    }

void TMProcess::ConfigChangingPhase3()
    {
    if ( fEnabled )
	fSystem->GetMainPythonInterface()->Run( GetPreConfigChangePhase3Action() );
    }

void TMProcess::ConfigChanged()
    {
    if ( fEnabled )
	fSystem->GetMainPythonInterface()->Run( GetPostConfigChangeAction() );
    }

void TMProcess::Set( TMConfig::ProcessData& data )
    {
    while ( fUsedResources.size()>0 )
	{
	delete *fUsedResources.begin();
	fUsedResources.erase( fUsedResources.begin() );
	}
    // The ID never changes.
    if ( data.fType == TMConfig::ProcessData::kProgram )
	{
	fIsProgram = true;
	fIsSlave = false;
	fIsProxy = false;
	}
    else if ( data.fType == TMConfig::ProcessData::kSlave )
	{
	fIsProgram = false;
	fIsSlave = true;
	fIsProxy = false;
	}
    else if ( data.fType == TMConfig::ProcessData::kSlaveProgram )
	{
	fIsProgram = true;
	fIsSlave = true;
	fIsProxy = false;
	}
    else if ( data.fType == TMConfig::ProcessData::kProxy )
	{
	fIsProgram = false;
	fIsSlave = false;
	fIsProxy = true;
	}

    bool iwtRunning = false;
    if ( fInterruptWaitThread )
	{
	iwtRunning = GetInterfaceLibrary()->InterruptWaitRunning();
	GetInterfaceLibrary()->StopInterruptWait();
	struct timeval start, now;
	unsigned long long deltaT = 0;
	const unsigned long long timeLimit = 1000000;
	gettimeofday( &start, NULL );
	while ( GetInterfaceLibrary()->InterruptWaitRunning() && deltaT<timeLimit )
	    {
	    usleep( 1000 );
	    gettimeofday( &now, NULL );
	    deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	    }
	if ( GetInterfaceLibrary()->InterruptWaitRunning() )
	    {
	    LOG( MLUCLog::kError, "TMProcess::Set", "Interrupt Wait Thread Does Not Terminate" )
		<< "Interrupt wait thread does not terminate normally. Aborting forcefully!!" << ENDLOG;
	    fInterruptWaitThread->Abort();
	    }
	fInterruptWaitThread->Join();
	delete fInterruptWaitThread;
	fInterruptWaitThread = NULL;
	}

    if ( data.fInterfaceLibrary.fActive )
	{
	if ( fInterfaceLibrary )
	    delete fInterfaceLibrary;
	fInterfaceLibrary = new TMInterfaceLibrary( data.fInterfaceLibrary.fPlatform.c_str(), data.fInterfaceLibrary.fBinary.c_str() );
	if ( !fInterfaceLibrary )
	    throw TMSysException( ENOMEM, strerror(ENOMEM), "Unable to create process private interface library object",
				  __FILE__, __LINE__ );
    	}
    else
	fInterfaceLibrary = NULL;

    // Create the interrupt wait thread if desired.
    if ( data.fInterruptListenAddressActive )
	{
	fInterruptWaitThread = new TMInterruptWaitThread( this, fSystem, data.fInterruptListenAddress.c_str() );
	if ( !fInterruptWaitThread )
	    throw TMSysException( ENOMEM, strerror(ENOMEM), "Unable to create interrupt wait thread",
				  __FILE__, __LINE__ );
	if ( iwtRunning )
	    fInterruptWaitThread->Start();
	}
    else
	fInterruptWaitThread = NULL;

    // fAddress = data.fAddress;
    //fSystem->NormalizeAddress( data.fAddress.c_str(), fAddress );
    fAddressString = data.fAddress;
    int ret = GetInterfaceLibrary()->ConvertAddress( fAddressString.c_str(), &fAddress );
    if ( ret )
	throw TMSysException( ret, strerror(ret), "Unable to convert address string",
			      __FILE__, __LINE__ );


    vector<MLUCString> cmdTokens;
    MLUCCmdlineParser parser;
    if ( !parser.ParseCmd( data.fCommand.c_str(), cmdTokens ) )
	throw TMSysException( EINVAL, strerror(EINVAL), "Unable to parse process command",
				     __FILE__, __LINE__ );
    fCmd = data.fCommand;
    fCmdTokens = cmdTokens;

    fStateChangeAction = data.fStateChangeAction;
    fPreStartAction = data.fPreStartAction;
    fPostStartAction = data.fPostStartAction;
    fPreTerminateAction = data.fPreTerminateAction;
    fPostTerminateAction = data.fPostTerminateAction;
    fInterruptReceivedAction = data.fInterruptReceivedAction;
    fPreConfigChangePhase1Action = data.fPreConfigChangePhase1Action;
    fPreConfigChangePhase2Action = data.fPreConfigChangePhase2Action;
    fPreConfigChangePhase3Action = data.fPreConfigChangePhase3Action;
    fPostConfigChangeAction = data.fPostConfigChangeAction;
  
    vector<TMConfig::UsedResourceData>::iterator resIter, resEnd;
    TMUsedResourceData::UsedResourceType resType;
    TMUsedResourceData* resourceData;
    resIter = data.fUsedResources.begin();
    resEnd = data.fUsedResources.end();
    while ( resIter != resEnd )
	{
	if ( resIter->fType=="File" )
	    resType = TMUsedResourceData::kFile;
	else if ( resIter->fType=="SysVShm" )
	    resType = TMUsedResourceData::kSysVShm;
	else if ( resIter->fType=="GenericCleanup" )
	    resType = TMUsedResourceData::kGenericCleanup;
	else
	    {
	    LOG( MLUCLog::kFatal, "TMPocess::Set", "Internal Error" )
		<< "Internal Error: Configuration supplied unknown resource type '"
		<< resIter->fType.c_str() << "'." << ENDLOG;
	    throw TMSysException( ECANCELED, strerror(ECANCELED), "Internal Error: Configuration supplied unknown resource type",
				  __FILE__, __LINE__ );
	    }
	resourceData = new TMUsedResourceData( resType, resIter->fContent.c_str() );
	fUsedResources.insert( fUsedResources.end(), resourceData );
	resIter++;
	}
    fCleanupAction = data.fCleanupAction;
    fFailOverPassiveKillAction = data.fFailOverPassiveKillAction;
    fFailOverPassiveMonitorCommand = data.fFailOverPassiveMonitorCommand;
    pthread_mutex_lock( &fStatesMutex );
    fStatesData = data.fStatesData;
    pthread_mutex_unlock( &fStatesMutex );
    }

TMInterruptWaitThread* TMProcess::GetInterruptWaitThread()
    {
    if ( fInterruptWaitThread )
	return fInterruptWaitThread;
    else if ( fSystem )
	return fSystem->GetInterruptWaitThread();
    else
	throw TMSysException( EFAULT, strerror(EFAULT), "Internal Error: No interrupt wait thread supplied",
			      __FILE__, __LINE__ );
    }
TMInterfaceLibrary* TMProcess::GetInterfaceLibrary()
    {
    if ( fInterfaceLibrary )
	return fInterfaceLibrary;
    else if ( fSystem )
	return fSystem->GetInterfaceLibrary();
    else
	throw TMSysException( EFAULT, strerror(EFAULT), "Internal Error: No Interface library supplied",
				  __FILE__, __LINE__ );
    }

void TMProcess::TStateChangeTimeoutCallback::TimerExpired( uint64 )
    {
    if ( !fProc )
	return;
    LOG( MLUCLog::kDebug, "TMProcess::TStateChangeTimeoutCallback::TimerExpired", "State change timer expired" )
	     << "State change timer expired for process " << fProc->fID.c_str() << ENDLOG;
    pthread_mutex_lock( &fProc->fStateChangeTimeoutMutex );
    if ( fProc->fStateChangeTimeoutSet )
	{
	fProc->fStateChangeTimeoutID = ~(uint32)0;
	fProc->fStateChangeTimeoutSet = false;
	}
    pthread_mutex_unlock( &fProc->fStateChangeTimeoutMutex );
    fProc->ForceStateChange();
    }


void TMProcess::GetStates( MLUCString& states )
    {
    vector<TMConfig::ProcessStateData> stateList;
    vector<TMConfig::ProcessStateData>::iterator stateIter, stateEnd;
    vector<MLUCString>::iterator cmdIter, cmdEnd;
    pthread_mutex_lock( &fStatesMutex );
    stateList = fStatesData;
    pthread_mutex_unlock( &fStatesMutex );

    stateIter = stateList.begin();
    stateEnd = stateList.end();
    while ( stateIter != stateEnd )
	{
	states += stateIter->fStateNamePattern;
	states += ":";
	cmdIter = stateIter->fAllowedCommands.begin();
	cmdEnd = stateIter->fAllowedCommands.end();
	while ( cmdIter != cmdEnd )
	    {
	    MLUCString tmp( *cmdIter );
	    std::vector<MLUCString> parts;
	    tmp.Split( parts, ':' );
	    std::vector<MLUCString>::iterator iter, end;
	    iter = parts.begin();
	    end = parts.end();
	    if ( iter!=end )
		{
		tmp = *iter;
		++iter;
		}
	    while ( iter != end )
		{
		tmp += "\\:";
		tmp += *iter;
		++iter;
		}
	    states += tmp;
	    cmdIter++;
	    if ( cmdIter!=cmdEnd )
		states += ",";
	    }
	states += ";";
	stateIter++;
	}
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
