/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TMPythonInterface.hpp"
#include "TMSysException.hpp"
#include "TMInterfaceLibrary.hpp"
#include "TMSystem.hpp"
#include "TMSlaveControl.hpp"
#include "TMProcess.hpp"
#include <MLUCLog.hpp>
#include <MLUCString.hpp>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>

TMMainPythonInterface* TMPythonInterface::gInterface = NULL;
//pthread_mutex_t TMPythonInterface::gMutex;
PyMethodDef* TMPythonInterface::gInterfaceMethods = NULL;
//pthread_t TMPythonInterface::gThreadID=0;
#ifndef TM_PYTHON_LOCAL_DICTIONARIES
PyObject* TMPythonInterface::gModule = NULL;
PyObject* TMPythonInterface::gDict = NULL;
#endif
PyInterpreterState* TMPythonInterface::gInterpreterState = NULL;
pthread_mutex_t TMPythonInterface::gVariableMutex;
vector<TMPythonInterface::PythonVariableData> TMPythonInterface::gVariables;
vector<TMPythonInterface::ThreadInterfaceData> TMPythonInterface::gThreadInterfaces;
pthread_mutex_t TMPythonInterface::gThreadInterfaceMutex;
MLUCMutex TMPythonInterface::gRunMutex;
MLUCMutex::TLocker TMPythonInterface::gRunMutexLocker( gRunMutex, false );



TMPythonInterface::TMPythonInterface( TMSystem* sys )
    {
    fThreadState = NULL;
    fSystem = sys;
    fRunCount = 0;
    }

TMPythonInterface::~TMPythonInterface()
    {
    }

void TMPythonInterface::Run( const char* action )
    {
    if ( !action || strlen(action)<=0 )
	return;
    bool saved = false;
    gRunMutexLocker.Lock();
#if 0
    if ( !fRunCount++ )
#endif
	{
	PyEval_RestoreThread( fThreadState );
	saved = true;
	}
    //PyRun_SimpleString( (char*)action );
    PyObject* pyret;
#ifndef TM_PYTHON_LOCAL_DICTIONARIES
    pyret = PyRun_String( (char*)action, Py_file_input, gDict, gDict );
#else
    pyret = PyRun_String( (char*)action, Py_file_input, fDict, fDict );
#endif
    if ( pyret )
	Py_DECREF(pyret);
    else
	{
	LOG( MLUCLog::kError, "TMPythonInterface::Run", "Python Interpreter Error Occured" )
	    << "Python error occured while attempting to execute script. (python code suppressed)" << ENDLOG;
//	LOG( MLUCLog::kError, "TMPythonInterface::Run", "Python Interpreter Error Occured" )
//	    << "(" << action << ")." << ENDLOG;
	LogPythonError();
	PyErr_Clear();
	}
#if 0
    --fRunCount;
#endif
    if ( saved )
	{
	PyEval_SaveThread();
	}
    gRunMutexLocker.Unlock();
    }

void TMPythonInterface::SetVar( const char* name, PyObject* data )
    {
    vector<PythonVariableData>::iterator iter, end;
    bool found = false;
    pthread_mutex_lock( &gVariableMutex );
    iter = gVariables.begin();
    end = gVariables.end();
    while ( iter != end )
	{
	if ( iter->fName == name )
	    {
	    Py_DECREF( iter->fValue );
	    iter->fValue = data;
	    Py_INCREF( iter->fValue );
	    found = true;
	    break;
	    }
	iter++;
	}
    if ( !found )
	{
	PythonVariableData pvdata;
	iter = gVariables.insert( gVariables.end(), pvdata );
	iter->fName = name;
	iter->fValue = data;
	Py_INCREF( iter->fValue );
	}
    pthread_mutex_unlock( &gVariableMutex );
    }

void TMPythonInterface::GetVar( const char* name, PyObject*& data )
    {
    vector<PythonVariableData>::iterator iter, end;
    bool found = false;
    pthread_mutex_lock( &gVariableMutex );
    iter = gVariables.begin();
    end = gVariables.end();
    while ( iter != end )
	{
	if ( iter->fName == name )
	    {
	    data = iter->fValue;
	    Py_INCREF( iter->fValue );
	    found = true;
	    break;
	    }
	iter++;
	}
    if ( !found )
	{
	data = Py_BuildValue( "" );
	}
    pthread_mutex_unlock( &gVariableMutex );
    }

void TMPythonInterface::LogPythonError()
    {
    PyObject *errType=NULL;
    PyObject *errValue=NULL;
    PyObject *errTraceback=NULL;
    PyErr_Fetch( &errType, &errValue, &errTraceback );
    if ( errType==NULL && errValue==NULL && errTraceback==NULL )
      return;
    else if ( !( errType!=NULL && errValue!=NULL && errTraceback!=NULL ) )
        {
	LOG( MLUCLog::kError, "TMPythonInterface::LogPythonError", "Inconsistent Python Errors" )
	  << "Inconsistent Python Error values from PyErr_Fetch: "
	  << "- errType" << (errType!=NULL ? "!=" : "==" ) << "NULL"
	  << "- errValue" << (errValue!=NULL ? "!=" : "==" ) << "NULL"
	  << "- errTraceback" << (errTraceback!=NULL ? "!=" : "==" ) << "NULL"
	  << "." << ENDLOG;
	}
    if ( errType!=NULL )
        {
	PyObject *errTypeString=NULL;
	errTypeString = PyObject_Str( errType );
	if ( errTypeString!=NULL )
	    {
	    const char* typeString = PyString_AsString( errTypeString );
	    if ( typeString!=NULL )
	        {
  	        LOG( MLUCLog::kError, "TMPythonInterface::LogPythonError", "Python Error Type" )
		  << "Python Error Type: '" << typeString << "'." << ENDLOG;
		}
	    else
	        {
  	        LOG( MLUCLog::kError, "TMPythonInterface::LogPythonError", "Python Error Type not string" )
		  << "Python Error Type String object string representation cannot be retrieved." << ENDLOG;
		}
	    Py_DECREF(errTypeString);
	    }
        }
    if ( errValue!=NULL )
        {
	PyObject *errValueString=NULL;
	errValueString = PyObject_Str( errValue );
	if ( errValueString!=NULL )
	    {
	    const char* valueString = PyString_AsString( errValueString );
	    if ( valueString!=NULL )
	        {
	        LOG( MLUCLog::kError, "TMPythonInterface::LogPythonError", "Python Error Value" )
		  << "Python Error Value: '" << valueString << "'." << ENDLOG;
		}
	    else
	        {
  	        LOG( MLUCLog::kError, "TMPythonInterface::LogPythonError", "Python Error Value not string" )
		  << "Python Error Value String object string representation cannot be retrieved." << ENDLOG;
		}
	    Py_DECREF(errValueString);
	    }
        }
    if ( errTraceback!=NULL )
        {
	PyObject *errTracebackString=NULL;
	errTracebackString = PyObject_Str( errTraceback );
	if ( errTracebackString!=NULL )
	    {
	    const char* tracebackString = PyString_AsString( errTracebackString );
	    if ( tracebackString!=NULL )
	        {
	        LOG( MLUCLog::kError, "TMPythonInterface::LogPythonError", "Python Error Traceback" )
		  << "Python Error Traceback: '" << tracebackString << "'." << ENDLOG;
		}
	    else
	        {
  	        LOG( MLUCLog::kError, "TMPythonInterface::LogPythonError", "Python Error Traceback not string" )
		  << "Python Error Traceback String object string representation cannot be retrieved." << ENDLOG;
		}
	    Py_DECREF(errTracebackString);
	    }
        }
    if ( errType!=NULL && errValue!=NULL && errTraceback!=NULL )
        {
	PyErr_Restore( errType, errValue, errTraceback );
	PyErr_Print();
	}
    else
        {
	if ( errType!=NULL )
	    Py_DECREF( errType );
	if ( errValue!=NULL )
	    Py_DECREF( errValue );
	if ( errTraceback!=NULL )
  	  Py_DECREF( errTraceback );
	}
    }


// Static Methods

#define INITSTATICMETHOD( cnt, name, func, args, descr ) do { gInterfaceMethods[cnt].ml_name = name; \
                                                              gInterfaceMethods[cnt].ml_meth = func; \
                                                              gInterfaceMethods[cnt].ml_flags = args; \
                                                              gInterfaceMethods[cnt].ml_doc = descr; \
                                                              cnt++; } while ( 0 );

bool TMPythonInterface::Init( TMMainPythonInterface* interface )
    {
    if ( !interface )
	throw TMPythonInterfaceException( TMPythonInterfaceException::kNoInterfaceSpecifiedError );
    if ( gInterface )
	throw TMPythonInterfaceException( TMPythonInterfaceException::kMainInterfaceDeclaredError );

    gInterfaceMethods = new PyMethodDef[ PYMETHODCNT+1 ];
    if ( !gInterfaceMethods )
	throw TMSysException( ENOMEM, strerror(ENOMEM), "Unable to create interface method table",
			      __FILE__, __LINE__ );

    gInterface = interface;
    //pthread_mutex_init( &gMutex, NULL );
    pthread_mutex_init( &gVariableMutex, NULL );
    pthread_mutex_init( &gThreadInterfaceMutex, NULL );
    unsigned long n = 0;

    INITSTATICMETHOD( n, "QueryState", QueryState, METH_VARARGS, "Query the state of a remote process" );
    INITSTATICMETHOD( n, "QueryStatusData", QueryStatusData, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "GetLAMProcessList", GetLAMProcessList, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "SendCommand", SendCommand, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "StartProcess", StartProcess, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "TerminateProcess", TerminateProcess, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "CleanupProcessResources", CleanupProcessResources, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "GetPID", GetPID, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "SetPollInterval", SetPollInterval, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "SetTaskState", SetTaskState, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "SetTaskStatusData", SetTaskStatusData, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "SetVar", SetVar, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "GetVar", GetVar, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "GetTaskCommand", GetTaskCommand, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "TaskInterrupt", TaskInterrupt, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "Log", Log, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "SetProcessStateChanged", SetProcessStateChanged, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "TerminateTask", TerminateTask, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "Connect", Connect, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "Disconnect", Disconnect, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "ReadConfiguration", ReadConfiguration, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "GetProcessConfigChangeState", GetProcessConfigChangeState, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "Reset", Reset, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "SetVerbosity", SetVerbosity, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "SuppressComErrorLogs", SuppressComErrorLogs, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "SuppressComErrorLogsQuery", SuppressComErrorLogsQuery, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "Yield", Yield, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "SetFailOverActive", SetFailOverActive, METH_VARARGS, "" );
    INITSTATICMETHOD( n, "PrepareProcessForTermination", PrepareProcessForTermination, METH_VARARGS, "" );
    INITSTATICMETHOD( n, NULL, (PyCFunction)NULL, 0, NULL );
    //     gInterfaceMethods[n++] = { "QueryState", QueryState, METH_VARARGS, "Query the state of a remote process" };
    //     gInterfaceMethods[n++] = { "QueryPreviousState", QueryPreviousState, METH_VARARGS, "" };
    //     gInterfaceMethods[n++] = { "QueryStatusData", QueryStatusData, METH_VARARGS, "" };
    //     gInterfaceMethods[n++] = { "GetLAMProcessList", GetLAMProcessList, METH_VARARGS, "" };
    //     gInterfaceMethods[n++] = { "SendCommand", SendCommand, METH_VARARGS, "" };
    //     gInterfaceMethods[n++] = { "TerminateProcess", TerminateProcess, METH_VARARGS, "" };
    //     gInterfaceMethods[n++] = { "CleanupProcessResources", CleanupProcessResources, METH_VARARGS, "" };
    //     gInterfaceMethods[n++] = { "GetPID", GetPID, METH_VARARGS, "" };
    //     gInterfaceMethods[n++] = { "SetPollInterval", SetPollInterval, METH_VARARGS, "" };
    //     gInterfaceMethods[n++] = { "SetPollInterval", SetPollInterval, METH_VARARGS, "" };
    //     gInterfaceMethods[n++] = { "SetTaskState", SetTaskState, METH_VARARGS, "" };
    //     gInterfaceMethods[n++] = { "SetVar", SetVar, METH_VARARGS, "" };
    //     gInterfaceMethods[n++] = { "GetVar", GetVar, METH_VARARGS, "" };
    //     gInterfaceMethods[n++] = { NULL, NULL, NULL, NULL };

    if ( n>PYMETHODCNT+1 )
	{
	LOG( MLUCLog::kFatal, "TMPythonInterface::Init", "INTERNAL ERROR: Too many Python methods" )
	    << "Fatal Internal Error: Too many Python methods defined. " << MLUCLog::kDec
	    << n << " defined and only " << PYMETHODCNT << " are allowed." << ENDLOG;
	exit( -1 );
	}
    
    
    Py_Initialize();
    PyEval_InitThreads();

    gInterface->fThreadState = PyThreadState_Get();
    if ( !gInterface->fThreadState )
	throw TMPythonInterfaceException( TMPythonInterfaceException::kUnknownError );
    gInterpreterState = gInterface->fThreadState->interp;
    
    Py_InitModule( (char*)"KIPTaskMan", gInterfaceMethods );
#ifndef TM_PYTHON_LOCAL_DICTIONARIES
    gModule = PyImport_AddModule("__main__");
    if ( gModule )
	{
	Py_INCREF( gModule );
	gDict = PyModule_GetDict( gModule );
	if ( gDict )
	    Py_INCREF( gDict );
	else
	    {
 	    LOG( MLUCLog::kError, "TMPythonInterface::Init", "No Global Python Module Dictionary" )
		<< "Unable to load global python module dictionary." << ENDLOG;
	    }
	}
    else
	{
	LOG( MLUCLog::kError, "TMPythonInterface::Init", "No Global Python Module" )
	    << "Unable to load global python module." << ENDLOG;
	}
#endif

    //     gInterfaceMethods[n].ml_name = fExtensionFuncs[n].fName;
    //     gInterfaceMethods[n].ml_meth = fExtensionFuncs[n].fFunc;
    //     gInterfaceMethods[n].ml_flags = METH_VARARGS;
    //     gInterfaceMethods[n].ml_doc = "MLUC library python interface function";    

    PyEval_SaveThread();
    return true;
    }

bool TMPythonInterface::Deinit( TMMainPythonInterface* interface )
    {
    if ( !gInterface )
	throw TMPythonInterfaceException( TMPythonInterfaceException::kNoInterfaceDeclaredError );
    if ( interface != gInterface )
	throw TMPythonInterfaceException( TMPythonInterfaceException::kWrongInterfaceError );
    PyEval_RestoreThread( gInterface->fThreadState );
#ifndef TM_PYTHON_LOCAL_DICTIONARIES
    if ( gModule )
	Py_DECREF( gModule );
    if ( gDict )
	Py_DECREF( gDict );
#endif
    Py_Finalize();
    if ( gInterfaceMethods )
	delete [] gInterfaceMethods;
    //pthread_mutex_destroy( &gMutex );
    pthread_mutex_destroy( &gThreadInterfaceMutex );
    pthread_mutex_destroy( &gVariableMutex );
    gInterface = NULL;
    return true;
    }

void TMPythonInterface::RegisterThreadInterface( TMPythonInterface* inter )
    {
    ThreadInterfaceData tid;
    tid.fThreadID = pthread_self();
    tid.fInterface = inter;
    pthread_mutex_lock( &gThreadInterfaceMutex );
    gThreadInterfaces.insert( gThreadInterfaces.end(), tid );
    pthread_mutex_unlock( &gThreadInterfaceMutex );
    }

void TMPythonInterface::UnregisterThreadInterface( TMPythonInterface* inter )
    {
    vector<ThreadInterfaceData>::iterator iter, end;
    pthread_mutex_lock( &gThreadInterfaceMutex );
    iter = gThreadInterfaces.begin();
    end = gThreadInterfaces.end();
    while ( iter != end )
	{
	if ( iter->fInterface == inter )
	    {
	    gThreadInterfaces.erase( iter );
	    break;
	    }
	iter++;
	}
    pthread_mutex_unlock( &gThreadInterfaceMutex );
    }



TMPythonInterface* TMPythonInterface::GetThreadInterface()
    {
    TMPythonInterface* inter = NULL;
    pthread_t self = pthread_self();
    vector<ThreadInterfaceData>::iterator iter, end;
    pthread_mutex_lock( &gThreadInterfaceMutex );
    iter = gThreadInterfaces.begin();
    end = gThreadInterfaces.end();
    while ( iter != end )
	{
	if ( iter->fThreadID == self )
	    {
	    inter = iter->fInterface;
	    break;
	    }
	iter++;
	}
    pthread_mutex_unlock( &gThreadInterfaceMutex );
    if ( !inter )
	throw TMPythonInterfaceException( TMPythonInterfaceException::kNoThreadInterface );
    return inter;
    }

void TMPythonInterface::Cleanup()
    {
    if ( fRunCount )
	{
	// All cancellation points are in areas where SaveThread has been called before.
	//PyEval_SaveThread();
	fRunCount = 0;
	}
    }



PyObject* TMPythonInterface::QueryState( PyObject*, PyObject* arg )
    {
    int ret;
    char* pid;
    ret = PyArg_ParseTuple( arg, "s", &pid );
    if ( !ret )
	return NULL;
    //     TMInterfaceLibrary* lib;
    //     lib = gInterface->fSystem->GetInterfaceLibrary();
    //     if ( !lib )
    // 	{
    // 	// Get an appropriate exception object and return NULL.
    // 	PyErr_SetString( PyExc_RuntimeError, "Unable to get interface library." );
    // 	return  NULL;
    // 	}
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    TMInterfaceLibrary* lib;
    lib = gInterface->fSystem->GetInterfaceLibrary();
    if ( !lib )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::QueryState", "Cannot find Interface Library" )
	    << "Unable to get interface library." << ENDLOG;
	// Get an appropriate exception object and return NULL.
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_RuntimeError, "Unable to get interface library." );
	return  NULL;
	}
    TMProcess* proc;
    proc = gInterface->fSystem->GetProcess( pid );
    if ( !proc )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::QueryState", "Cannot find process" )
	    << "Cannot find process '" << pid << "'." << ENDLOG;
	// Get an appropriate exception object and return NULL.
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_LookupError, "Could not find specified process." );
	return  NULL;
	}
    MLUCString state;
    try
	{
	ret = 0;
	if ( proc->GetConnectionRequested() && !proc->GetConnectionEstablished() )
	    {
	    ret = lib->ConnectToProcess( proc->GetPID(), pid, proc->GetAddress() );
	    if ( !ret )
		proc->SetConnectionEstablished( true );
	    }
	if ( ret && !proc->SuppressComErrorLogs() )
	    {
	    LOG( MLUCLog::kWarning, "TMPythonInterface::QueryState", "Unable to build explicit connection" )
		<< "Unable to build explicit connection to process " << pid << "." << ENDLOG;
	    }
	// Try to query state anyway, might return TM:DEAD, which is better than an empty string which would be returned otherwise...
	proc->CheckState( state );
	// 	char* state;
	// 	ret = lib->QueryState( proc->GetPID(), pid, proc->GetAddress(), &state );
	// 	if ( ret )
	// 	    {
	// 	    PyErr_SetString( PyExc_SystemError, strerror(ret) );
	// 	    return NULL;
	// 	    }
	// 	PyObject* pyret = Py_BuildValue( "s", state );
	// 	lib->ReleaseResource( state );
	}
    catch ( TMInterfaceLibrary::TMInterfaceLibraryException e )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::QueryState", "Dynamic Library Interface QueryState function exception" )
	    << "Error calling 'QueryState' function in dynamic library: " << e.GetName() << ": " 
	    << e.GetErrorTypeName() << " / " << e.GetError() << " (" << MLUCLog::kDec
	    << e.GetErrno() << ")." << ENDLOG;
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_SystemError, e.GetError() );
	return NULL;
	}
    catch ( TMSysException e )
	{
	if ( !proc->SuppressComErrorLogs() )
	    {
	    LOG( MLUCLog::kError, "TMPythonInterface::QueryState", "System exception" )
		<< "Error checking state for process " << pid << ": "
		<< e.GetDescription() << ": " 
		<< e.GetErrorStr() << " (" << MLUCLog::kDec
		<< e.GetErrno() << ") (" << e.GetFile() << ":" << e.GetLine() 
		<< ")." << ENDLOG;
	    }
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_SystemError, e.GetErrorStr() );
	return NULL;
	}
    PyEval_RestoreThread( _save );
    Py_DECREF( arg );
    //fRunCount = oldRunCount;
    PyObject* pyret = Py_BuildValue( "s", state.c_str() );
    return pyret;
    }

// PyObject* TMPythonInterface::QueryPreviousState( PyObject* self, PyObject* arg )
//     {
//     }

PyObject* TMPythonInterface::QueryStatusData( PyObject*, PyObject* arg )
    {
    int ret;
    char* pid;
    ret = PyArg_ParseTuple( arg, "s", &pid );
    if ( !ret )
	return NULL;
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    TMInterfaceLibrary* lib;
    lib = gInterface->fSystem->GetInterfaceLibrary();
    if ( !lib )
	{
	// Get an appropriate exception object and return NULL.
	LOG( MLUCLog::kError, "TMPythonInterface::QueryStatusData", "Cannot find Interface Library" )
	    << "Unable to get interface library." << ENDLOG;
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_RuntimeError, "Unable to get interface library." );
	return  NULL;
	}
    TMProcess* proc;
    proc = gInterface->fSystem->GetProcess( pid );
    if ( !proc )
	{
	// Get an appropriate exception object and return NULL.
	LOG( MLUCLog::kError, "TMPythonInterface::QueryStatusData", "Cannot find process" )
	    << "Cannot find process '" << pid << "'." << ENDLOG;
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_LookupError, "Could not find specified process." );
	return  NULL;
	}
    char* state;
    PyObject* pyret;
    if ( !proc->IsProgram() || proc->IsRunning() )
	{
	try
	    {
	    ret = 0;
	    if ( proc->GetConnectionRequested() && !proc->GetConnectionEstablished() )
		{
		ret = lib->ConnectToProcess( proc->GetPID(), pid, proc->GetAddress() );
		if ( !ret )
		    proc->SetConnectionEstablished( true );
		}
	    if ( !ret )
		ret = lib->QueryStatusData( proc->GetPID(), pid, proc->GetAddress(), &state );
	    }
	catch ( TMInterfaceLibrary::TMInterfaceLibraryException e )
	    {
	    LOG( MLUCLog::kError, "TMPythonInterface::QueryStatusData", "Dynamic Library Interface QueryStatusData function exception" )
		<< "Error calling 'QueryStatusData' function in dynamic library: " << e.GetName() << ": " 
		<< e.GetErrorTypeName() << " / " << e.GetError() << " (" << MLUCLog::kDec
		<< e.GetErrno() << ")." << ENDLOG;
	    PyEval_RestoreThread( _save );
	    Py_DECREF( arg );
	    //fRunCount = oldRunCount;
	    PyErr_SetString( PyExc_SystemError, e.GetError() );
	    return NULL;
	    }
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	if ( ret )
	    {
	    PyErr_SetString( PyExc_SystemError, strerror(ret) );
	    return NULL;
	    }
	pyret = Py_BuildValue( "s", state );
	//oldRunCount = fRunCount;
	//fRunCount = 0;
	_save = PyEval_SaveThread();
	try
	    {
	    lib->ReleaseResource( state );
	    }
	catch ( TMInterfaceLibrary::TMInterfaceLibraryException e )
	    {
	    LOG( MLUCLog::kError, "TMPythonInterface::QueryStatusData", "Dynamic Library Interface QueryStatusData function exception" )
		<< "Error calling 'ReleaseResource' function in dynamic library: " << e.GetName() << ": " 
		<< e.GetErrorTypeName() << " / " << e.GetError() << " (" << MLUCLog::kDec
		<< e.GetErrno() << ")." << ENDLOG;
	    PyEval_RestoreThread( _save );
	    //fRunCount = oldRunCount;
	    PyErr_SetString( PyExc_SystemError, e.GetError() );
	    return NULL;
	    }
	PyEval_RestoreThread( _save );
	}
    else
	{
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	pyret = Py_BuildValue( "s", "" );
	}
    //fRunCount = oldRunCount;
    return pyret;
    }

PyObject* TMPythonInterface::GetLAMProcessList( PyObject*, PyObject* )
    {
    vector<TMSystem::LAMData> procs;
    vector<TMSystem::LAMData>::iterator iter, end;
    gInterface->fSystem->GetLAMProcessList( procs );
    iter = procs.begin();
    end = procs.end();
    MLUCString procs_str;
    PyObject* all_list = PyList_New(0);
    if ( !all_list )
	return NULL;
    while ( iter != end )
	{
	if ( iter->fProcess )
	    {
	    PyObject* id = Py_BuildValue( "s", iter->fProcess->GetID() );
	    if ( !id )
		{
		Py_DECREF(all_list);
		return NULL;
		}
	    vector<MLUCString>::iterator notIter, notEnd;
	    notIter = iter->fNotificationData.begin();
	    notEnd = iter->fNotificationData.end();
	    while ( notIter != notEnd )
		{
		PyObject* list = PyList_New(0);
		if ( !list )
		    {
		    Py_DECREF(all_list);
		    Py_DECREF(id);
		    return NULL;
		    }
		PyObject* nD;
		if ( *notIter != "" )
		    nD = Py_BuildValue( "s", notIter->c_str() );
		else
		    nD = Py_BuildValue( "" );
		if ( !nD )
		    {
		    Py_DECREF(all_list);
		    Py_DECREF(list);
		    Py_DECREF(id);
		    return NULL;
		    }
		if ( PyList_Append( list, id )==-1 )
		    {
		    Py_DECREF(all_list);
		    Py_DECREF(list);
		    Py_DECREF(id);
		    Py_DECREF(nD);
		    return NULL;
		    }
		if ( PyList_Append( list, nD )==-1 )
		    {
		    Py_DECREF(all_list);
		    Py_DECREF(list);
		    Py_DECREF(nD);
		    Py_DECREF(id);
		    return NULL;
		    }
		Py_DECREF(nD);
		if ( PyList_Append( all_list, list )==-1 )
		    {
		    Py_DECREF(all_list);
		    Py_DECREF(list);
		    Py_DECREF(id);
		    return NULL;
		    }
		Py_DECREF(list);
		notIter++;
		}
	    Py_DECREF(id);
	    }
	iter++;
	}
    return all_list;
    }

PyObject* TMPythonInterface::SendCommand( PyObject*, PyObject* arg )
    {
    int ret;
    char* pid;
    char* cmd;
    unsigned long timeout = ~(unsigned long)0;
#if PY_VERSION_HEX>=0x02030000
    ret = PyArg_ParseTuple( arg, "ss|k", &pid, &cmd, &timeout );
#else
    ret = PyArg_ParseTuple( arg, "ss|l", &pid, &cmd, &timeout );
#endif
    if ( !ret )
	return NULL;
#if 1
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    TMProcess* proc;
    proc = gInterface->fSystem->GetProcess( pid );
    if ( !proc )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::SendCommand", "Cannot find process" )
	    << "Cannot find process '" << pid << "'." << ENDLOG;
	// Get an appropriate exception object and return NULL.
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_LookupError, "Could not find specified process." );
	return  NULL;
	}
    try
	{
	ret = proc->SendCommand( cmd, timeout );
	}
    catch ( TMInterfaceLibrary::TMInterfaceLibraryException e )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::SendCmd", "Dynamic Library Interface SendCmd function exception" )
	    << "Error calling function in dynamic library: " << e.GetName() << ": " 
	    << e.GetErrorTypeName() << " / " << e.GetError() << " (" << MLUCLog::kDec
	    << e.GetErrno() << ")." << ENDLOG;
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_SystemError, e.GetError() );
	return NULL;
	}
    catch ( TMSysException e )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::SendCmd", "Process SendCommand function exception" )
	    << "Error calling 'SendCommand' function for process '"
	    << pid << "': " << e.GetErrorStr() << " (" << MLUCLog::kDec
	    << e.GetErrno() << ") at " << e.GetFile() << ":" << e.GetLine()
	    << "." << ENDLOG;
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_SystemError, e.GetErrorStr() );
	return NULL;
	}
    PyEval_RestoreThread( _save );
    //fRunCount = oldRunCount;
    if ( ret )
	{
	//PyErr_SetString( PyExc_SystemError, strerror(ret) );
	//return NULL;
	PyObject* pyret = Py_BuildValue( "s", strerror(ret) );
	return pyret;
	}
    PyObject* pyret = Py_BuildValue( "" );
    Py_DECREF( arg );
    return pyret;
#else
    if ( !gInterface->fSystem->GetFailOverActive() )
	{
	PyObject* pyret = Py_BuildValue( "" );
	return pyret;
	}
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    TMInterfaceLibrary* lib;
    lib = gInterface->fSystem->GetInterfaceLibrary();
    if ( !lib )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::SendCommand", "Cannot find Interface Library" )
	    << "Unable to get interface library." << ENDLOG;
	// Get an appropriate exception object and return NULL.
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	//PyObject* pyret = Py_BuildValue( "s", state.c_str() );
	PyErr_SetString( PyExc_RuntimeError, "Unable to get interface library." );
	return  NULL;
	}
    TMProcess* proc;
    proc = gInterface->fSystem->GetProcess( pid );
    if ( !proc )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::SendCommand", "Cannot find process" )
	    << "Cannot find process '" << pid << "'." << ENDLOG;
	// Get an appropriate exception object and return NULL.
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_LookupError, "Could not find specified process." );
	return  NULL;
	}
    try
	{
	// 	if ( !proc->IsProgram() || proc->GetPID()>0 )
	ret = 0;
	if ( proc->GetConnectionRequested() && !proc->GetConnectionEstablished() )
	    {
	    ret = lib->ConnectToProcess( proc->GetPID(), pid, proc->GetAddress() );
	    if ( !ret )
		proc->SetConnectionEstablished( true );
	    }
	if ( !ret )
	    ret = lib->SendCommand( proc->GetPID(), pid, proc->GetAddress(), cmd );
	}
    catch ( TMInterfaceLibrary::TMInterfaceLibraryException e )
	{
	if ( timeout != ~(unsigned long)0 )
	    proc->ForceStateChangeTimeout( timeout );
	LOG( MLUCLog::kError, "TMPythonInterface::SendCmd", "Dynamic Library Interface SendCmd function exception" )
	    << "Error calling 'SendCmd' function in dynamic library: " << e.GetName() << ": " 
	    << e.GetErrorTypeName() << " / " << e.GetError() << " (" << MLUCLog::kDec
	    << e.GetErrno() << ")." << ENDLOG;
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_SystemError, e.GetError() );
	return NULL;
	}
    if ( timeout != ~(unsigned long)0 )
	proc->ForceStateChangeTimeout( timeout );
    PyEval_RestoreThread( _save );
    //fRunCount = oldRunCount;
    if ( ret )
	{
	//PyErr_SetString( PyExc_SystemError, strerror(ret) );
	//return NULL;
	PyObject* pyret = Py_BuildValue( "s", strerror(ret) );
	return pyret;
	}
    PyObject* pyret = Py_BuildValue( "" );
    Py_DECREF( arg );
    return pyret;
#endif
    }

PyObject* TMPythonInterface::TerminateProcess( PyObject*, PyObject* arg )
    {
    int ret;
    char* pid;
    ret = PyArg_ParseTuple( arg, "s", &pid );
    if ( !ret )
	return NULL;
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    TMProcess* proc;
    proc = gInterface->fSystem->GetProcess( pid );
    if ( !proc )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::TerminateProcess", "Cannot find process" )
	    << "Cannot find process '" << pid << "'." << ENDLOG;
	// Get an appropriate exception object and return NULL.
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_LookupError, "Could not find specified process." );
	return  NULL;
	}
    try
	{
	//proc->Kill( true );
	gInterface->fSystem->TerminateProcess( proc->GetNdx() );
	  
	}
    catch ( TMSysException e )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::TerminateProcess", "Error attempting to kill process" )
	    << "Error attempting to kill process '" << pid << "': " << e.GetErrorStr()
	    << " (" << e.GetErrno() << "): " << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
	    << e.GetLine() << ")" << "." << ENDLOG;
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_SystemError, e.GetErrorStr() );
	return NULL;
	}
    PyEval_RestoreThread( _save );
    Py_DECREF( arg );
    //fRunCount = oldRunCount;
    PyObject* pyret = Py_BuildValue( "" );
    return pyret;
    }

PyObject* TMPythonInterface::CleanupProcessResources( PyObject*, PyObject* arg )
    {
    int ret;
    char* pid;
    ret = PyArg_ParseTuple( arg, "s", &pid );
    if ( !ret )
	return NULL;
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    TMProcess* proc;
    proc = gInterface->fSystem->GetProcess( pid );
    if ( !proc )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::CleanupProcessResources", "Cannot find process" )
	    << "Cannot find process '" << pid << "'." << ENDLOG;
	// Get an appropriate exception object and return NULL.
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_LookupError, "Could not find specified process." );
	return  NULL;
	}
    try
	{
	//proc->Cleanup();
	gInterface->fSystem->CleanupProcessResources( proc->GetNdx() );
	}
    catch ( TMSysException e )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::CleanupProcessResources", "Error attempting to cleanup process resources" )
	    << "Error attempting to cleanup resources for process '" << pid << "': " << e.GetErrorStr()
	    << " (" << e.GetErrno() << "): " << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
	    << e.GetLine() << ")" << "." << ENDLOG;
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_SystemError, e.GetErrorStr() );
	return NULL;
	}
    PyEval_RestoreThread( _save );
    Py_DECREF( arg );
    //fRunCount = oldRunCount;
    PyObject* pyret = Py_BuildValue( "" );
    return pyret;
    }

PyObject* TMPythonInterface::GetPID( PyObject*, PyObject* arg )
    {
    int ret;
    char* pid;
    ret = PyArg_ParseTuple( arg, "s", &pid );
    if ( !ret )
	return NULL;
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    TMProcess* proc;
    proc = gInterface->fSystem->GetProcess( pid );
    if ( !proc )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::GetPID", "Cannot find process" )
	    << "Cannot find process '" << pid << "'." << ENDLOG;
	// Get an appropriate exception object and return NULL.
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_LookupError, "Could not find specified process." );
	return  NULL;
	}
    long spid = proc->GetPID();
    PyEval_RestoreThread( _save );
    Py_DECREF( arg );
    //fRunCount = oldRunCount;
    PyObject* pyret = Py_BuildValue( "l", spid );
    return pyret;
    }

PyObject* TMPythonInterface::SetPollInterval( PyObject*, PyObject* arg )
    {
    int ret;
    unsigned long inter;
#if PY_VERSION_HEX>=0x02030000
    ret = PyArg_ParseTuple( arg, "k", &inter );
#else
    ret = PyArg_ParseTuple( arg, "l", &inter );
#endif
    if ( !ret )
	return NULL;
    gInterface->fSystem->SetPeriodicPollInterval( inter );
    PyObject* pyret = Py_BuildValue( "" );
    return pyret;
    }

PyObject* TMPythonInterface::StartProcess( PyObject*, PyObject* arg )
    {
    int ret;
    char* pid;
    ret = PyArg_ParseTuple( arg, "s", &pid );
    if ( !ret )
	return NULL;
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    TMProcess* proc;
    proc = gInterface->fSystem->GetProcess( pid );
    if ( !proc )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::StartProcess", "Cannot find process" )
	    << "Cannot find process '" << pid << "'." << ENDLOG;
	// Get an appropriate exception object and return NULL.
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_LookupError, "Could not find specified process." );
	return  NULL;
	}
    try
	{
	//proc->Start();
	gInterface->fSystem->StartProcess( proc->GetNdx() );
	}
    catch ( TMSysException e )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::StartProcess", "Error attempting to start process" )
	    << "Error attempting to start process '" << pid << "': " << e.GetErrorStr()
	    << " (" << e.GetErrno() << "): " << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
	    << e.GetLine() << ")" << "." << ENDLOG;
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_SystemError, e.GetErrorStr() );
	return NULL;
	}
    PyEval_RestoreThread( _save );
    Py_DECREF( arg );
    //fRunCount = oldRunCount;
    PyObject* pyret = Py_BuildValue( "" );
    return pyret;
    }

PyObject* TMPythonInterface::SetTaskState( PyObject*, PyObject* arg )
    {
    int ret;
    char* state;
    ret = PyArg_ParseTuple( arg, "s", &state );
    if ( !ret )
	return NULL;
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    gInterface->fSystem->SetTaskState( state );
    PyEval_RestoreThread( _save );
    Py_DECREF( arg );
    //fRunCount = oldRunCount;
    PyObject* pyret = Py_BuildValue( "" );
    return pyret;
    }

PyObject* TMPythonInterface::SetTaskStatusData( PyObject*, PyObject* arg )
    {
    int ret;
    char* state;
    ret = PyArg_ParseTuple( arg, "s", &state );
    if ( !ret )
	return NULL;
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    gInterface->fSystem->SetTaskStatusData( state );
    PyEval_RestoreThread( _save );
    Py_DECREF( arg );
    //fRunCount = oldRunCount;
    PyObject* pyret = Py_BuildValue( "" );
    return pyret;
    }

/*
  struct VariableData
  {
  MLUCString fName;
  PyObject* fValue;
  };
  pthread_mutex_t fVariableMutex;
  vector<PythonVariableData> fVariables
*/
PyObject* TMPythonInterface::SetVar( PyObject*, PyObject* arg )
    {
    int ret;
    char* name;
    PyObject* val;
    ret = PyArg_ParseTuple( arg, "sO", &name, &val );
    if ( !ret )
	return NULL;
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    gInterface->SetVar( name, val );
    PyEval_RestoreThread( _save );
    Py_DECREF( arg );
    //fRunCount = oldRunCount;
    PyObject* pyret = Py_BuildValue( "" );
    return pyret;
    }

PyObject* TMPythonInterface::GetVar( PyObject*, PyObject* arg )
    {
    int ret;
    char* name;
    PyObject* val;
    ret = PyArg_ParseTuple( arg, "s", &name );
    if ( !ret )
	return NULL;
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    gInterface->GetVar( name, val );
    PyEval_RestoreThread( _save );
    Py_DECREF( arg );
    //fRunCount = oldRunCount;
    return val;
    }

PyObject* TMPythonInterface::GetTaskCommand( PyObject*, PyObject* arg )
    {
    int ret;
    MLUCString cmd;
    ret = PyArg_ParseTuple( arg, "" );
    if ( !ret )
	return NULL;
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    PyObject* pyret;
    if ( !gInterface->fSystem->GetSlaveControl() )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::GetTaskCommand", "Cannot find slave control object" )
	    << "No slave control object configured." << ENDLOG;
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_SystemError, "No slave control object configured" );
	return NULL;
	}
    if ( gInterface->fSystem->GetSlaveControl()->GetCommand( cmd ) )
	{
	PyEval_RestoreThread( _save );
	//fRunCount = oldRunCount;
	pyret = Py_BuildValue( "s", cmd.c_str() );
	}
    else
	{
	PyEval_RestoreThread( _save );
	//fRunCount = oldRunCount;
	pyret = Py_BuildValue( "" );
	}
    Py_DECREF( arg );
    return pyret;
    }


PyObject* TMPythonInterface::Log( PyObject*, PyObject* arg )
    {
    int ret;
    int lvl;
    char* msg;
    ret = PyArg_ParseTuple( arg, "is", &lvl, &msg );
    if ( !ret )
	return NULL;
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    MLUCLog::TLogLevel llvl = (MLUCLog::TLogLevel)lvl;
    LOG( llvl, "TMPythonInterface::Log", "Log" )
	<< msg << ENDLOG;
    PyEval_RestoreThread( _save );
    //fRunCount = oldRunCount;
    Py_DECREF( arg );
    PyObject* pyret = Py_BuildValue( "" );
    return pyret;
    }

PyObject* TMPythonInterface::SetProcessStateChanged( PyObject*, PyObject* arg )
    {
    int ret;
    char* pid;
    ret = PyArg_ParseTuple( arg, "s", &pid );
    if ( !ret )
	return NULL;
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    TMProcess* proc;
    proc = gInterface->fSystem->GetProcess( pid );
    if ( !proc )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::SetProcessStateChanged", "Cannot find process" )
	    << "Cannot find process '" << pid << "'." << ENDLOG;
	// Get an appropriate exception object and return NULL.
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_LookupError, "Could not find specified process." );
	return  NULL;
	}
    proc->ForceStateChange();
    PyEval_RestoreThread( _save );
    //fRunCount = oldRunCount;
    Py_DECREF( arg );
    PyObject* pyret = Py_BuildValue( "" );
    return pyret;
    }

PyObject* TMPythonInterface::TerminateTask( PyObject*, PyObject* arg )
    {
    int ret;
    ret = PyArg_ParseTuple( arg, "" );
    if ( !ret )
	return NULL;
    PyObject* pyret = Py_BuildValue( "" );
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    gInterface->fSystem->QuitTask();
    gInterface->fSystem->QuitSlave();
    PyEval_RestoreThread( _save );
    //fRunCount = oldRunCount;
    Py_DECREF( arg );
    return pyret;
    }

PyObject* TMPythonInterface::Connect( PyObject*, PyObject* arg )
    {
    int ret;
    char* pid;
    ret = PyArg_ParseTuple( arg, "s", &pid );
    if ( !ret )
	return NULL;
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    TMInterfaceLibrary* lib;
    lib = gInterface->fSystem->GetInterfaceLibrary();
    if ( !lib )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::Connect", "Cannot find Interface Library" )
	    << "Unable to get interface library." << ENDLOG;
	// Get an appropriate exception object and return NULL.
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_RuntimeError, "Unable to get interface library." );
	return  NULL;
	}
    TMProcess* proc;
    proc = gInterface->fSystem->GetProcess( pid );
    if ( !proc )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::Connect", "Cannot find process" )
	    << "Cannot find process '" << pid << "'." << ENDLOG;
	// Get an appropriate exception object and return NULL.
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_LookupError, "Could not find specified process." );
	return  NULL;
	}
    proc->SetConnectionRequested( true );
    try
	{
	// 	if ( !proc->IsProgram() || proc->GetPID()>0 )
	ret = lib->ConnectToProcess( proc->GetPID(), pid, proc->GetAddress() );
	}
    catch ( TMInterfaceLibrary::TMInterfaceLibraryException e )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::Connect", "Dynamic Library Interface Connect function exception" )
	    << "Error calling 'Connect' function in dynamic library: " << e.GetName() << ": " 
	    << e.GetErrorTypeName() << " / " << e.GetError() << " (" << MLUCLog::kDec
	    << e.GetErrno() << ")." << ENDLOG;
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_SystemError, e.GetError() );
	return NULL;
	}
    PyEval_RestoreThread( _save );
    Py_DECREF( arg );
    //fRunCount = oldRunCount;
    if ( ret )
	{
	//PyErr_SetString( PyExc_SystemError, strerror(ret) );
	//return NULL;
	PyObject* pyret = Py_BuildValue( "s", strerror(ret) );
	return pyret;
	}
    proc->SetConnectionEstablished( true );
    PyObject* pyret = Py_BuildValue( "" );
    return pyret;
    }

PyObject* TMPythonInterface::Disconnect( PyObject*, PyObject* arg )
    {
    int ret;
    char* pid;
    ret = PyArg_ParseTuple( arg, "s", &pid );
    if ( !ret )
	return NULL;
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    TMInterfaceLibrary* lib;
    lib = gInterface->fSystem->GetInterfaceLibrary();
    if ( !lib )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::Disconnect", "Cannot find Interface Library" )
	    << "Unable to get interface library." << ENDLOG;
	// Get an appropriate exception object and return NULL.
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_RuntimeError, "Unable to get interface library." );
	return  NULL;
	}
    TMProcess* proc;
    proc = gInterface->fSystem->GetProcess( pid );
    if ( !proc )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::Disconnect", "Cannot find process" )
	    << "Cannot find process '" << pid << "'." << ENDLOG;
	// Get an appropriate exception object and return NULL.
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_LookupError, "Could not find specified process." );
	return  NULL;
	}
    proc->SetConnectionEstablished( false ); // This is a bit premature, but I do not know, what could happen otherwise anyway at the moment.
    proc->SetConnectionRequested( false );
    try
	{
	// 	if ( !proc->IsProgram() || proc->GetPID()>0 )
	ret = lib->DisconnectFromProcess( proc->GetPID(), pid, proc->GetAddress() );
	}
    catch ( TMInterfaceLibrary::TMInterfaceLibraryException e )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::Disconnect", "Dynamic Library Interface Disconnect function exception" )
	    << "Error calling 'Disconnect' function in dynamic library: " << e.GetName() << ": " 
	    << e.GetErrorTypeName() << " / " << e.GetError() << " (" << MLUCLog::kDec
	    << e.GetErrno() << ")." << ENDLOG;
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_SystemError, e.GetError() );
	return NULL;
	}
    PyEval_RestoreThread( _save );
    Py_DECREF( arg );
    //fRunCount = oldRunCount;
    if ( ret )
	{
	//PyErr_SetString( PyExc_SystemError, strerror(ret) );
	//return NULL;
	PyObject* pyret = Py_BuildValue( "s", strerror(ret) );
	return pyret;
	}
    PyObject* pyret = Py_BuildValue( "" );
    return pyret;
    }

PyObject* TMPythonInterface::ReadConfiguration( PyObject*, PyObject* arg )
    {
    int ret;
    ret = PyArg_ParseTuple( arg, "" );
    if ( !ret )
	return NULL;
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    gInterface->fSystem->ReadNewConfig();
    PyEval_RestoreThread( _save );
    Py_DECREF( arg );
    //fRunCount = oldRunCount;
    PyObject* pyret = Py_BuildValue( "" );
    return pyret;
    }

PyObject* TMPythonInterface::GetProcessConfigChangeState( PyObject*, PyObject* arg )
    {
    int ret;
    char* pid;
    ret = PyArg_ParseTuple( arg, "s", &pid );
    if ( !ret )
	return NULL;
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    TMSystem::ConfigChangeProcessState changeState;
    try
	{
	changeState = gInterface->fSystem->GetProcessConfigChangeState( pid );
	}
    catch ( TMSysException e )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::GetProcessConfigChangeState", "System Exception" )
	    << "Error calling system 'GetProcessConfigChangeState' function: " 
	    << e.GetErrorStr() << " / " << e.GetDescription() << " (" << MLUCLog::kDec
	    << e.GetErrno() << ")." << ENDLOG;
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_SystemError, e.GetErrorStr() );
	return NULL;
	}
    PyEval_RestoreThread( _save );
    const char* stateStr=NULL;
    switch ( changeState )
	{
	case TMSystem::kUnknown:
	    stateStr = "unknown";
	    break;
	case TMSystem::kUnchanged:
	    stateStr = "unchanged";
	    break;
	case TMSystem::kAdded:
	    stateStr = "added";
	    break;
	case TMSystem::kRemoved:
	    stateStr = "removed";
	    break;
	case TMSystem::kChanged:
	    stateStr = "changed";
	    break;
	case TMSystem::kChangedNotStarted:
	    stateStr = "changed-not-started";
	    break;
	}
    PyObject* pyret = Py_BuildValue( "s", stateStr );
    Py_DECREF( arg );
    return pyret;
    }

PyObject* TMPythonInterface::SetVerbosity( PyObject*, PyObject* arg )
    {
    int ret;
    unsigned long verbosity;
    ret = PyArg_ParseTuple( arg, "i", &verbosity );
    if ( !ret )
	return NULL;
    gLogLevel = verbosity;
    PyObject* pyret = Py_BuildValue( "" );
    return pyret;
    }

PyObject* TMPythonInterface::Reset( PyObject*, PyObject* arg )
    {
    int ret;
    LOG( MLUCLog::kInformational, "TMPythonInterface::Reset", "TaskManager Reset" ) << "Reinitilializing Task Manager" << ENDLOG;
    gInterface->fSystem->Reset();
    PyObject* pyret = Py_BuildValue( "" );
    return pyret;
    }

PyObject* TMPythonInterface::SuppressComErrorLogs( PyObject*, PyObject* arg )
    {
    int ret;
    unsigned long timeout = 0;
    char* pid;
#if PY_VERSION_HEX>=0x02030000
    ret = PyArg_ParseTuple( arg, "sk", &pid, &timeout );
#else
    ret = PyArg_ParseTuple( arg, "sl", &pid, &timeout );
#endif
    if ( !ret )
	return NULL;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    TMInterfaceLibrary* lib;
    lib = gInterface->fSystem->GetInterfaceLibrary();
    if ( !lib )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::SuppressComErrorLogs", "Cannot find Interface Library" )
	    << "Unable to get interface library." << ENDLOG;
	// Get an appropriate exception object and return NULL.
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_RuntimeError, "Unable to get interface library." );
	return  NULL;
	}
    TMProcess* proc;
    proc = gInterface->fSystem->GetProcess( pid );
    if ( !proc )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::SuppressComErrorLogs", "Cannot find process" )
	    << "Cannot find process '" << pid << "'." << ENDLOG;
	// Get an appropriate exception object and return NULL.
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_LookupError, "Could not find specified process." );
	return  NULL;
	}
    proc->SuppressComErrorLogs( timeout );
    PyEval_RestoreThread( _save );
    Py_DECREF( arg );
    //fRunCount = oldRunCount;
    PyObject* pyret = Py_BuildValue( "" );
    return pyret;
    }

PyObject* TMPythonInterface::SuppressComErrorLogsQuery( PyObject*, PyObject* arg )
    {
    int ret;
    char* pid;
    ret = PyArg_ParseTuple( arg, "s", &pid );
    if ( !ret )
	return NULL;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    TMInterfaceLibrary* lib;
    lib = gInterface->fSystem->GetInterfaceLibrary();
    if ( !lib )
 	{
	LOG( MLUCLog::kError, "TMPythonInterface::SuppressComErrorLogs", "Cannot find Interface Library" )
	    << "Unable to get interface library." << ENDLOG;
	// Get an appropriate exception object and return NULL.
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_RuntimeError, "Unable to get interface library." );
	return  NULL;
 	}
    TMProcess* proc;
    proc = gInterface->fSystem->GetProcess( pid );
    if ( !proc )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::SuppressComErrorLogs", "Cannot find process" )
	    << "Cannot find process '" << pid << "'." << ENDLOG;
	// Get an appropriate exception object and return NULL.
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_LookupError, "Could not find specified process." );
	return  NULL;
	}
    int result = proc->SuppressComErrorLogs();
    PyEval_RestoreThread( _save );
    Py_DECREF( arg );
    //fRunCount = oldRunCount;
    PyObject* pyret = Py_BuildValue( "i", result );
    return pyret;
    }


PyObject* TMPythonInterface::TaskInterrupt( PyObject*, PyObject* arg )
    {
    int ret;
    char* notData = NULL;
    ret = PyArg_ParseTuple( arg, "|z", &notData );
    if ( !ret )
	return NULL;
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    if ( !gInterface->fSystem->GetSlaveControl() )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::TaskInterrupt", "Cannot find slave control object" )
	    << "No slave control object configured." << ENDLOG;
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_SystemError, "No slave control object configured" );
	return NULL;
	}
    gInterface->fSystem->GetSlaveControl()->Interrupt( notData );
    PyEval_RestoreThread( _save );
    Py_DECREF( arg );
    //fRunCount = oldRunCount;
    PyObject* pyret = Py_BuildValue( "" );
    return pyret;
    }


PyObject* TMPythonInterface::Yield( PyObject*, PyObject* arg )
    {
    int ret;
    unsigned long sleeptime = 0;
#if PY_VERSION_HEX>=0x02030000
    ret = PyArg_ParseTuple( arg, "k", &sleeptime );
#else
    ret = PyArg_ParseTuple( arg, "l", &sleeptime );
#endif
    if ( !ret )
	return NULL;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    gRunMutexLocker.Unlock();
    usleep( sleeptime );
    gRunMutexLocker.Lock();
    PyEval_RestoreThread( _save );
    Py_DECREF( arg );
    PyObject* pyret = Py_BuildValue( "" );
    return pyret;
    }


PyObject* TMPythonInterface::SetFailOverActive( PyObject*, PyObject* arg )
    {
    int ret;
    int set=0;
    ret = PyArg_ParseTuple( arg, "i", &set );
    gInterface->fSystem->SetFailOverActive( set );
    PyObject* pyret = Py_BuildValue( "" );
    return pyret;
    } 

PyObject* TMPythonInterface::PrepareProcessForTermination( PyObject*, PyObject* arg )
    {
    int ret;
    char* pid;
    ret = PyArg_ParseTuple( arg, "s", &pid );
    if ( !ret )
	return NULL;
    //unsigned int oldRunCount = fRunCount;
    //fRunCount = 0;
    Py_INCREF( arg );
    PyThreadState *_save = PyEval_SaveThread();
    TMProcess* proc;
    proc = gInterface->fSystem->GetProcess( pid );
    if ( !proc )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::PrepareProcessForTermination", "Cannot find process" )
	    << "Cannot find process '" << pid << "'." << ENDLOG;
	// Get an appropriate exception object and return NULL.
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_LookupError, "Could not find specified process." );
	return  NULL;
	}
    try
	{
	proc->SetTerminationFlag( true );
	}
    catch ( TMInterfaceLibrary::TMInterfaceLibraryException e )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::PrepareProcessForTermination", "Dynamic Library Interface SendCmd function exception" )
	    << "Error calling function in dynamic library: " << e.GetName() << ": " 
	    << e.GetErrorTypeName() << " / " << e.GetError() << " (" << MLUCLog::kDec
	    << e.GetErrno() << ")." << ENDLOG;
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_SystemError, e.GetError() );
	return NULL;
	}
    catch ( TMSysException e )
	{
	LOG( MLUCLog::kError, "TMPythonInterface::PrepareProcessForTermination", "Process SendCommand function exception" )
	    << "Error calling 'SendCommand' function for process '"
	    << pid << "': " << e.GetErrorStr() << " (" << MLUCLog::kDec
	    << e.GetErrno() << ") at " << e.GetFile() << ":" << e.GetLine()
	    << "." << ENDLOG;
	PyEval_RestoreThread( _save );
	Py_DECREF( arg );
	//fRunCount = oldRunCount;
	PyErr_SetString( PyExc_SystemError, e.GetErrorStr() );
	return NULL;
	}
    PyEval_RestoreThread( _save );
    PyObject* pyret = Py_BuildValue( "" );
    Py_DECREF( arg );
    return pyret;
    }

TMMainPythonInterface::TMMainPythonInterface( TMSystem* sys ):
    TMPythonInterface( sys )
    {
    Init( this );
    RegisterThreadInterface( this );
    // Note: Although we do this in the main as well as in the sub python interfaces,
    // we cannot put this into the base python interface constructor, as only the 
    // main python interface constructor initializes the python interpreter.
    PyEval_RestoreThread( fThreadState );
#ifdef TM_PYTHON_LOCAL_DICTIONARIES
    fModule = PyImport_AddModule("__main__");
    if ( fModule )
	{
	Py_INCREF( fModule );
	fDict = PyModule_GetDict( fModule );
	if ( fDict )
	    Py_INCREF( fDict );
	else
	    {
 	    LOG( MLUCLog::kError, "TMMainPythonInterface::TMMainPythonInterface", "No Global Python Module Dictionary" )
		<< "Unable to load global python module dictionary." << ENDLOG;
	    }
	}
    else
	{
	LOG( MLUCLog::kError, "TMMainPythonInterface::TMMainPythonInterface", "No Global Python Module" )
	    << "Unable to load global python module." << ENDLOG;
	}
#endif
    PyEval_SaveThread();
    }

TMMainPythonInterface::~TMMainPythonInterface()
    {
    PyEval_RestoreThread( fThreadState );
#ifdef TM_PYTHON_LOCAL_DICTIONARIES
    if ( fDict )
	{
	Py_DECREF( fDict );
	}
    if ( fModule )
	{
	Py_DECREF( fModule );
	}
#endif
    PyEval_SaveThread();
    UnregisterThreadInterface( this );
    Deinit( this );
    }

TMSubThreadPythonInterface::TMSubThreadPythonInterface( TMSystem* sys ):
    TMPythonInterface( sys )
    {
    if ( !gInterface )
	throw TMPythonInterfaceException( TMPythonInterfaceException::kNoInterfaceDeclaredError );

    PyEval_AcquireLock();
#ifndef TM_PYTHON_THREAD_PRIVATE_INTERPRETERS
    fThreadState = PyThreadState_New( gInterpreterState );
#else
    fThreadState = Py_NewInterpreter();
#endif
    if ( !fThreadState )
	throw TMPythonInterfaceException( TMPythonInterfaceException::kUnknownError );
    PyThreadState_Swap( fThreadState );
    Py_InitModule( (char*)"KIPTaskMan", gInterfaceMethods );
#ifdef TM_PYTHON_LOCAL_DICTIONARIES
    fModule = PyImport_AddModule("__main__");
    if ( fModule )
	{
	Py_INCREF( fModule );
	fDict = PyModule_GetDict( fModule );
	if ( fDict )
	    Py_INCREF( fDict );
	else
	    {
 	    LOG( MLUCLog::kError, "TMSubPythonInterface::TMSubPythonInterface", "No Global Python Module Dictionary" )
		<< "Unable to load global python module dictionary." << ENDLOG;
	    }
	}
    else
	{
	LOG( MLUCLog::kError, "TMSubPythonInterface::TMSubPythonInterface", "No Global Python Module" )
	    << "Unable to load global python module." << ENDLOG;
	}
#endif
    PyEval_SaveThread();
    RegisterThreadInterface( this );
    }

TMSubThreadPythonInterface::~TMSubThreadPythonInterface()
    {
    UnregisterThreadInterface( this );
    PyEval_AcquireLock();
#ifdef TM_PYTHON_LOCAL_DICTIONARIES
    if ( fDict )
	{
	Py_DECREF( fDict );
	}
    if ( fModule )
	{
	Py_DECREF( fModule );
	}
#endif
#ifndef TM_PYTHON_THREAD_PRIVATE_INTERPRETERS
    PyThreadState_Clear( fThreadState );
    PyThreadState_Delete( fThreadState );
#else
    PyThreadState_Swap( fThreadState );
    Py_EndInterpreter( fThreadState );
#endif
    PyEval_ReleaseLock();
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
