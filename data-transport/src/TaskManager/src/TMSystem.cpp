/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TMPythonInterface.hpp"
#include "TMSystem.hpp"
#include "TMProcess.hpp"
#include "TMConfig.hpp"
#include "TMSysException.hpp"
#include "TMInterfaceLibrary.hpp"
#include "TMProcessExecutionStateHandler.hpp"
#include "TMInterruptWaitThread.hpp"
#include "TMPythonRunThread.hpp"
#include "TMSlaveControl.hpp"
#include "TMPythonExtension.hpp"
#include <MLUCLog.hpp>
#include <errno.h>
#include <signal.h>
#include <map>
#include <omp.h>


TMSystem* TMSystem::gSystem = NULL;


TMSystem::TMSystem( const char* configfile, bool allowNoProcesses ):
//     fInterruptingProcesses( 4 ), fProcessesToStart( 4 ),
//     fProcessesToTerminate( 4 ), fProcessesToCleanup( 4 )
    fInterruptingProcesses( 4 ), fTaskActions( 4 )
    {
    if ( !gSystem )
	gSystem = this;
    fInterfaceLibrary = NULL;
    fMainPythonInterface = NULL;
    fConfig = NULL;
    fProcessExecutionStateHandler = NULL;
    fInterruptWaitThread = NULL;
    fPythonLoop = NULL;
    fPeriodicPollPythonLoop = NULL;
    fSlaveControl = NULL;

    fProcIDWrapOccured = false;
    fNextProcID = 0;

    fPollTime = 100000; // By default start polling every tenth of a second.
    

    // Create the configuration object
    fConfig = new TMConfig();
    if ( !fConfig )
	throw TMSysException( ENOMEM, strerror(ENOMEM), "Unable to create configuration object",
			      __FILE__, __LINE__ );
    // Read the configuration
    fConfig->ReadConfig( configfile, allowNoProcesses );
    fConfigFile = configfile;

    // Create the python interpreter object
    fMainPythonInterface = new TMMainPythonInterface( this );
    if ( !fMainPythonInterface )
	throw TMSysException( ENOMEM, strerror(ENOMEM), "Unable to create main python interface object",
			      __FILE__, __LINE__ );
    
    // Determine which interface library to use...
    TMConfig::InterfaceLibraryData interfaceLibData;
    interfaceLibData = fConfig->GetInterfaceLibraryData();
    // ... and create it
    fInterfaceLibrary = new TMInterfaceLibrary( interfaceLibData.fPlatform.c_str(), interfaceLibData.fBinary.c_str() );
    if ( !fInterfaceLibrary )
	throw TMSysException( ENOMEM, strerror(ENOMEM), "Unable to create interface library object",
			      __FILE__, __LINE__ );

    // Get the state supervision configuration data
    TMConfig::StateSupervisionConfigData supervisionData;
    supervisionData = fConfig->GetStateSupervisionConfigData();
    //fInterruptListenAddress = supervisionData.fInterruptListenAddress;
    fStartupAction = supervisionData.fStartupAction;
    fStateChangeAction = supervisionData.fStateChangeAction;
    fInterruptReceivedAction = supervisionData.fInterruptReceivedAction;
    fPeriodicPollAction = supervisionData.fPeriodicPollAction;
    fPeriodicPollInterval = supervisionData.fPeriodicPollInterval; // In microsec. 
    fLoopAction = supervisionData.fLoopAction;
    fLoopStopAction = supervisionData.fLoopStopAction;
    fFailOverActive = supervisionData.fFailOverActive;
    fPreConfigChangePhase1Action = supervisionData.fPreConfigChangePhase1Action;
    fPreConfigChangePhase2Action = supervisionData.fPreConfigChangePhase2Action;
    fPreConfigChangePhase3Action = supervisionData.fPreConfigChangePhase3Action;
    fPostConfigChangeAction = supervisionData.fPostConfigChangeAction;
    if ( supervisionData.fPollIntervalSet )
	fPollTime = supervisionData.fPollInterval;

    fInterruptListenAddress = supervisionData.fInterruptListenAddress;
    fInterfaceLibrary->Initialize( fInterruptListenAddress.c_str() );
    //NormalizeAddress( supervisionData.fInterruptListenAddress.c_str(), fInterruptListenAddress );
    fInterruptListenAddress = supervisionData.fInterruptListenAddress;
    // Create the interrupt wait thread.
    fInterruptWaitThread = new TMInterruptWaitThread( this, fInterruptListenAddress.c_str() );
    if ( !fInterruptWaitThread )
	throw TMSysException( ENOMEM, strerror(ENOMEM), "Unable to create interrupt wait thread",
			      __FILE__, __LINE__ );

    // Create the two python threads
    fPythonLoop = new TMPythonRunThread( this, fLoopAction.c_str() );
    if ( !fPythonLoop )
	throw TMSysException( ENOMEM, strerror(ENOMEM), "Unable to create python loop thread",
			      __FILE__, __LINE__ );
    fPeriodicPollPythonLoop = new TMPythonRunThread( this, fPeriodicPollAction.c_str(), 
						     fPeriodicPollInterval );
    if ( !fPeriodicPollPythonLoop )
	throw TMSysException( ENOMEM, strerror(ENOMEM), "Unable to create periodic python poll thread",
			      __FILE__, __LINE__ );


    TMConfig::SlaveControlConfigData slaveControlConfigData;
    slaveControlConfigData = fConfig->GetSlaveControlConfigData();
    if ( slaveControlConfigData.fActive )
	{
	fSlaveControl = new TMSlaveControl( this, slaveControlConfigData );
	}

    vector<TMConfig::PythonExtensionData> pythonExtensionData;
    pythonExtensionData = fConfig->GetPythonExtensionData();
    vector<TMConfig::PythonExtensionData>::iterator pyExtIter, pyExtEnd;
    pyExtIter = pythonExtensionData.begin();
    pyExtEnd = pythonExtensionData.end();
    TMPythonExtension* pyExt;
    while ( pyExtIter != pyExtEnd )
	{
	pyExt = new TMPythonExtension( pyExtIter->fModule.c_str(), pyExtIter->fPlatform.c_str(), pyExtIter->fBinary.c_str() );
	if ( !pyExt )
	    throw TMSysException( ENOMEM, strerror(ENOMEM), "Unable to create python extension object",
				  __FILE__, __LINE__ );
	pyExt->Init();
	fPythonExtensions.push_back( pyExt );
	pyExtIter++;
	}


    pthread_mutex_init( &fProcessMutex, NULL );
    // Get the data for the processes to be controlled
    vector<TMConfig::ProcessData> processes;
    vector<TMConfig::ProcessData>::iterator iter, end;
    //vector<TMConfig::UsedResourceData>::iterator resIter, resEnd;
    vector<TMUsedResourceData*> resources;
    TMProcess* proc;
    processes = fConfig->GetProcessData();
    // Create the process objects
    iter = processes.begin();
    end = processes.end();
    while ( iter != end )
	{
	proc = new TMProcess( this, iter->fID.c_str(), GetNextProcID( fProcesses ), *iter );
	if ( !proc )
	    throw TMSysException( ENOMEM, strerror(ENOMEM), "Unable to create process object",
				  __FILE__, __LINE__ );
	// 	proc->SetAddress( iter->fAddress.c_str() );
	// 	proc->SetCommand( iter->fCommand.c_str() );
	// 	proc->SetStateChangeAction( iter->fStateChangeAction.c_str() );
	// 	proc->SetPreStartAction( iter->fPreStartAction.c_str() );
	// 	proc->SetPostStartAction( iter->fPostStartAction.c_str() );
	// 	proc->SetPreTerminateAction( iter->fPreTerminateAction.c_str() );
	// 	proc->SetPostTerminateAction( iter->fPostTerminateAction.c_str() );
	// 	proc->SetInterruptReceivedAction( iter->fInterruptReceivedAction.c_str() );
	// 	proc->SetPreConfigChangeAction( iter->fPreConfigChangeAction.c_str() );
	// 	proc->SetPostConfigChangeAction( iter->fPostConfigChangeAction.c_str() );
	// 	proc->SetCleanupAction( iter->fCleanupAction.c_str() );
	// 	resIter = iter->fUsedResources.begin();
	// 	resEnd = iter->fUsedResources.end();
	// 	while ( resIter != resEnd )
	// 	    {
	// 	    if ( resIter->fType=="File" )
	// 		resType = TMUsedResourceData::kFile;
	// 	    else if ( resIter->fType=="SysVShm" )
	// 		resType = TMUsedResourceData::kSysVShm;
	// 	    else if ( resIter->fType=="GenericCleanup" )
	// 		resType = TMUsedResourceData::kGenericCleanup;
	// 	    else
	// 		{
	// 		LOG( MLUCLog::kFatal, "TMSystem::TMSystem", "Internal Error" )
	// 		    << "Internal Error: Configuration supplied unknown resource type '"
	// 		    << resIter->fType.c_str() << "'." << ENDLOG;
	// 		throw TMSysException( ECANCELED, strerror(ECANCELED) );
	// 		}
	// 	    resourceData = new TMUsedResourceData( resType, resIter->fContent.c_str() );
	// 	    resources.insert( resources.end(), resourceData );
	// 	    resIter++;
	// 	    }
	// 	proc->SetUsedResources( resources );
	fProcesses.insert( fProcesses.end(), proc );
	proc->GetInterruptWaitThread()->AddProcess( proc->GetNdx(), proc->GetID(), proc->GetAddress() );
	proc = NULL;
	iter++;
	}

    fProcessExecutionStateHandler = new TMProcessExecutionStateHandler( fProcesses.size() );
    if ( !fProcessExecutionStateHandler )
	throw TMSysException( ENOMEM, strerror(ENOMEM), "Unable to create process execution state handler object",
			      __FILE__, __LINE__ );

    //     pthread_mutex_init( &fInterruptingProcessMutex, NULL );
    pthread_mutex_init( &fTaskActionMutex, NULL );
//     pthread_mutex_init( &fProcessesToStartMutex, NULL );
//     pthread_mutex_init( &fProcessesToTerminateMutex, NULL );
//     pthread_mutex_init( &fProcessesToCleanupMutex, NULL );
    pthread_mutex_init( &fLAMProcessListMutex, NULL );
    pthread_mutex_init( &fTaskStateMutex, NULL );
    pthread_mutex_init( &fTaskStatusDataMutex, NULL );
    fReadNewConfig = false;
    fQuitTask = false;
    fQuitSlave = false;
#ifdef INITIAL_CONDITIONSEM_UNLOCK
    // Define this when condition semaphore objects should always be unlocked initially.
    fInterruptingProcesses.Unlock();
#endif

    if ( gSystem == this )
	{
	signal( SIGHUP, SignalHandler );
	signal( SIGQUIT, SignalHandler );
	}
    }

TMSystem::~TMSystem()
    {
    if ( fConfig )
	delete fConfig;
    if ( fInterfaceLibrary )
	{
	fInterfaceLibrary->Terminate();
	delete fInterfaceLibrary;
	}
    if ( fMainPythonInterface )
	delete fMainPythonInterface;
    if ( fProcessExecutionStateHandler )
	delete fProcessExecutionStateHandler;
    if ( fInterruptWaitThread )
	delete fInterruptWaitThread;
    if ( fPythonLoop )
	delete fPythonLoop;
    if ( fPeriodicPollPythonLoop )
	delete fPeriodicPollPythonLoop;
    if ( fSlaveControl )
	delete fSlaveControl;
    while ( fProcesses.size()>0 )
	{
	delete *(fProcesses.begin());
	fProcesses.erase( fProcesses.begin() );
	}
    pthread_mutex_destroy( &fProcessMutex );
    //     pthread_mutex_destroy( &fInterruptingProcessMutex );
    pthread_mutex_destroy( &fTaskActionMutex );
//     pthread_mutex_destroy( &fProcessesToStartMutex );
//     pthread_mutex_destroy( &fProcessesToTerminateMutex );
//     pthread_mutex_destroy( &fProcessesToCleanupMutex );
    pthread_mutex_destroy( &fLAMProcessListMutex );
    pthread_mutex_destroy( &fTaskStateMutex );
    pthread_mutex_destroy( &fTaskStatusDataMutex );
    }

// void TMSystem::AddProcess( TMProcess* proc )
//     {
//     fProcesses.insert( fProcesses.end(), proc );
//     }

TMProcess* TMSystem::GetProcess( const char* proc_id )
    {
    vector<TMProcess*>::iterator iter, end;
    TMProcess* proc = NULL;
    pthread_mutex_lock( &fProcessMutex );
    iter = fProcesses.begin();
    end = fProcesses.end();
    while ( iter != end )
	{
	if ( !strcmp( (*iter)->GetID(), proc_id ) )
	    {
	    proc = *iter;
	    break;
	    }
	iter++;
	}
    pthread_mutex_unlock( &fProcessMutex );
    return proc;
    }

TMProcess* TMSystem::GetProcess( unsigned long ndx )
    {
    vector<TMProcess*>::iterator iter, end;
    TMProcess* proc = NULL;
    pthread_mutex_lock( &fProcessMutex );
    iter = fProcesses.begin();
    end = fProcesses.end();
    while ( iter != end )
	{
	if ( (*iter)->GetNdx()==ndx )
	    {
	    proc = *iter;
	    break;
	    }
	iter++;
	}
    pthread_mutex_unlock( &fProcessMutex );
    return proc;
    }

unsigned long TMSystem::GetProcessCnt()
    {
    pthread_mutex_lock( &fProcessMutex );
    unsigned long tmp = fProcesses.size();
    pthread_mutex_unlock( &fProcessMutex );
    return tmp;
    }


void TMSystem::GetLAMProcessList( vector<LAMData>& procs )
    {
    pthread_mutex_lock( &fLAMProcessListMutex );
    procs = fLAMProcessList;
    fLAMProcessList.clear();
    pthread_mutex_unlock( &fLAMProcessListMutex );
    }

void TMSystem::SetPeriodicPollInterval( unsigned long interval )
    {
    if ( fPeriodicPollPythonLoop )
	fPeriodicPollPythonLoop->SetInterval( interval );
    }

void TMSystem::SetTaskState( const char* state )
    {
    pthread_mutex_lock( &fTaskStateMutex );
    fTaskState = state;
    LOG( MLUCLog::kDebug, "TMSystem::SetTaskState", "State set" )
	<< "Task state set to '" << state << "'." << ENDLOG;
    pthread_mutex_unlock( &fTaskStateMutex );
    }

void TMSystem::SetTaskStatusData( const char* data )
    {
    pthread_mutex_lock( &fTaskStatusDataMutex );
    fTaskStatusData = data;
    pthread_mutex_unlock( &fTaskStatusDataMutex );
    }

void TMSystem::GetTaskState( MLUCString& state )
    {
    pthread_mutex_lock( &fTaskStateMutex );
    state = fTaskState;
    pthread_mutex_unlock( &fTaskStateMutex );
    }

void TMSystem::GetTaskStatusData( MLUCString& data )
    {
    pthread_mutex_lock( &fTaskStatusDataMutex );
    data = fTaskStatusData;
    pthread_mutex_unlock( &fTaskStatusDataMutex );
    }

void TMSystem::GetChildren( MLUCString& data )
    {
    MLUCString tmp;
    tmp.SetSizeIncrement( 65536 );
    data = "";
    vector<TMProcess*>::iterator iter, end;
    pthread_mutex_lock( &fProcessMutex );
    iter = fProcesses.begin();
    end = fProcesses.end();
    while ( iter != end )
	{
	data += (*iter)->GetID();
	data += " ";
	if ( (*iter)->IsProgram() && (*iter)->IsSlave() )
	    data += "slaveprogram";
	else if ( (*iter)->IsSlave() )
	    data += "slave";
	else 
	    data += "program";
	data += " ";
	data += (*iter)->GetAddressString();
	(*iter)->QueryOldState( tmp );
	if ( tmp!="" && tmp!=(const char*)NULL )
	    {
	    data += " ";
	    data += tmp;
	    }
	iter++;
	if ( iter != end )
	    data += "  ";
	}
    data += "  ";
    data += "FailOverState ";
    data += "failover ";
    data += "- ";
    data += (fFailOverActive ? "active" : "passive");


    iter = fProcesses.begin();
    end = fProcesses.end();
    while ( iter != end )
	{
	data += "  failover_";
	data += (*iter)->GetID();
	data += " ";
	if ( (*iter)->IsProgram() && (*iter)->IsSlave() )
	    data += "failover_slaveprogram";
	else if ( (*iter)->IsSlave() )
	    data += "failover_slave";
	else 
	    data += "failover_program";
	data += " - ";
	data += ((*iter)->IsRunning() ? "running_local" : "not_running_local");
	data += ",";
	data += ((*iter)->IsStarted() ? "started" : "not_started");
	data += ",";
	data += ((*iter)->IsStartedBySelf() ? "self_started" : "not_self_started");
	data += ",";
	data += ((*iter)->GetTerminationFlag() ? "termination_flag" : "no_termination_flag");
	iter++;
	}
    
    pthread_mutex_unlock( &fProcessMutex );
    }

void TMSystem::GetChildStates( const char* proc_id, MLUCString& data )
    {
    data = "";
    TMProcess* child = GetProcess( proc_id );
    if ( !child )
	{
	LOG( MLUCLog::kError, "TMSystem::GetChildStates", "Process not found" )
	    << "Child process with ID '" << proc_id << "' could not be found."
	    << ENDLOG;
	return;
	}
    child->GetStates( data );
    }


void TMSystem::StartProcess( unsigned long ndx )
    {
//     pthread_mutex_lock( &fProcessesToStartMutex );
//     fProcessesToStart.Add( ndx );
//     pthread_mutex_unlock( &fProcessesToStartMutex );
    TaskAction act;
    act.fAction = TaskAction::kStart;
    act.fProcNdx = ndx;
    pthread_mutex_lock( &fTaskActionMutex );
    fTaskActions.Add( act );
    pthread_mutex_unlock( &fTaskActionMutex );
    }

void TMSystem::TerminateProcess( unsigned long ndx )
    {
//     pthread_mutex_lock( &fProcessesToTerminateMutex );
//     fProcessesToTerminate.Add( ndx );
//     pthread_mutex_unlock( &fProcessesToTerminateMutex );
    TaskAction act;
    act.fAction = TaskAction::kTerminate;
    act.fProcNdx = ndx;
    pthread_mutex_lock( &fTaskActionMutex );
    fTaskActions.Add( act );
    pthread_mutex_unlock( &fTaskActionMutex );
    }

void TMSystem::CleanupProcessResources( unsigned long ndx )
    {
//     pthread_mutex_lock( &fProcessesToCleanupMutex );
//     fProcessesToCleanup.Add( ndx );
//     pthread_mutex_unlock( &fProcessesToCleanupMutex );
    TaskAction act;
    act.fAction = TaskAction::kCleanup;
    act.fProcNdx = ndx;
    pthread_mutex_lock( &fTaskActionMutex );
    fTaskActions.Add( act );
    pthread_mutex_unlock( &fTaskActionMutex );
    }

void TMSystem::Interrupt( unsigned long ndx, const char* notificationData )
    {
    //   pthread_mutex_lock( &fInterruptingProcessMutex );
    //   fInterruptingProcesses.Add( ndx );
    //   pthread_mutex_unlock( &fInterruptingProcessMutex );
    InterruptingProcessData ipd;
    ipd.fNdx = ndx;
    ipd.fNotificationData = notificationData;
    fInterruptingProcesses.AddNotificationData( ipd );
    fInterruptingProcesses.Signal();
    }

bool TMSystem::WaitForInterrupt( unsigned long waitTimeout )
    {
    fInterruptingProcesses.Lock();
    if ( fInterruptingProcesses.HaveNotificationData() )
	{
	fInterruptingProcesses.Unlock();
	return true;
	}
    bool tmp = fInterruptingProcesses.Wait( waitTimeout/1000 );
    fInterruptingProcesses.Unlock();
    return tmp;
    }


void TMSystem::GetInterruptingProcesses( MLUCVector<InterruptingProcessData>& procs )
    {
    //   pthread_mutex_lock( &fInterruptingProcessMutex );
    //   while ( fInterruptingProcesses.
    //   pthread_mutex_unlock( &fInterruptingProcessMutex );
    fInterruptingProcesses.Lock();
    while ( fInterruptingProcesses.HaveNotificationData() )
	{
	procs.Add( fInterruptingProcesses.PopNotificationData() );
	}
    fInterruptingProcesses.Unlock();
    }

void TMSystem::GetTaskActions( MLUCVector<TaskAction>& acts )
    {
    pthread_mutex_lock( &fTaskActionMutex );
    while ( fTaskActions.GetCnt() )
	{
	acts.Add( fTaskActions.GetFirst() );
	fTaskActions.RemoveFirst();
	}
    pthread_mutex_unlock( &fTaskActionMutex );
    }

unsigned long TMSystem::GetNextProcID( const vector<TMProcess*>& procs)
    {
    unsigned long ret;
    if ( !fProcIDWrapOccured )
	{
	ret = fNextProcID++;
	if ( fNextProcID==~(unsigned long)0 )
	    {
	    fNextProcID = 0;
	    fProcIDWrapOccured= true;
	    }
	}
    else
	{
	vector<TMProcess*>::const_iterator iter, end;
	ret = fNextProcID;
	bool found=false;
	do
	    {
	    found=false;
	    iter = procs.begin();
	    end = procs.end();
	    while ( iter != end )
		{
		if ( (*iter)->GetNdx() == ret )
		    {
		    found=true;
		    ret++;
		    if ( ret == ~(unsigned long)0 )
			ret = 0;
		    break;
		    }
		iter++;
		}
	    }
	while ( found && ret!=fNextProcID);
	if ( found )
	    {
	    throw TMSysException( EBUSY, strerror(EBUSY), "Unable to find unused process index",
				  __FILE__, __LINE__ );
	    }
	fNextProcID = ret+1;
	if ( fNextProcID == ~(unsigned long)0 )
	    fNextProcID = 0;
	}
    LOG( MLUCLog::kDebug, "TMSystem::GetNextProcID", "Next Process ID" )
	<< "Next process ID to be used: " << MLUCLog::kDec << ret << " (0x"
	<< MLUCLog::kHex << ret << ") - Process ID wrap: "
	<< (fProcIDWrapOccured ? "yes" : "no" ) << " Next process ID: "
	<< MLUCLog::kDec << fNextProcID << " (0x"
	<< MLUCLog::kHex << fNextProcID << ")." << ENDLOG;
    return ret;
    }



// void TMSystem::GetProcessesToStart( MLUCVector<unsigned long>& procs )
//     {
//     pthread_mutex_lock( &fProcessesToStartMutex );
//     while ( fProcessesToStart.GetCnt() )
// 	{
// 	procs.Add( fProcessesToStart.GetFirst() );
// 	fProcessesToStart.RemoveFirst();
// 	}
//     pthread_mutex_unlock( &fProcessesToStartMutex );
//     }

// void TMSystem::GetProcessesToTerminate( MLUCVector<unsigned long>& procs )
//     {
//     pthread_mutex_lock( &fProcessesToTerminateMutex );
//     while ( fProcessesToTerminate.GetCnt() )
// 	{
// 	procs.Add( fProcessesToTerminate.GetFirst() );
// 	fProcessesToTerminate.RemoveFirst();
// 	}
//     pthread_mutex_unlock( &fProcessesToTerminateMutex );
//     }

// void TMSystem::GetProcessesToCleanup( MLUCVector<unsigned long>& procs )
//     {
//     pthread_mutex_lock( &fProcessesToCleanupMutex );
//     while ( fProcessesToCleanup.GetCnt() )
// 	{
// 	procs.Add( fProcessesToCleanup.GetFirst() );
// 	fProcessesToCleanup.RemoveFirst();
// 	}
//     pthread_mutex_unlock( &fProcessesToCleanupMutex );
//     }

void TMSystem::Run()
    {
    fTimer.Start();
    fInterruptWaitThread->Start();
    fPythonLoop->Start();
    fPeriodicPollPythonLoop->Start();
  
    if ( fSlaveControl )
	fSlaveControl->Start();
    while ( !fQuitSlave )
	{
	fQuitTask = false;
	//if ( !fSlaveControl || fSlaveControl->IsAutonomous() )
	Start();
	unsigned long pollFactor=1;
	while ( !fQuitTask && !fQuitSlave )
	    {
	    // 	  if ( CheckInterrupts() )
	    // 	    usleep( 10000 );
	    LOG( MLUCLog::kDebug, "TMSystem::Run", "TRACE / Main loop" ) << "Main loop trace " << MLUCLog::kDec << __FILE__ << ":" << __LINE__ << ENDLOG;
	    if ( CheckSlaveControlCommands() )
		{
	    LOG( MLUCLog::kDebug, "TMSystem::Run", "TRACE / Main loop" ) << "Main loop trace " << MLUCLog::kDec << __FILE__ << ":" << __LINE__ << ENDLOG;
		usleep( 0 );
		pollFactor = 1;
		}
	    LOG( MLUCLog::kDebug, "TMSystem::Run", "TRACE / Main loop" ) << "Main loop trace " << MLUCLog::kDec << __FILE__ << ":" << __LINE__ << ENDLOG;
	    if ( CheckProcessStates() )
		{
	    LOG( MLUCLog::kDebug, "TMSystem::Run", "TRACE / Main loop" ) << "Main loop trace " << MLUCLog::kDec << __FILE__ << ":" << __LINE__ << ENDLOG;
		usleep( 0 );
		pollFactor = 1;
		}
	    else
		{
		if ( pollFactor < 8 )
		    pollFactor *= 2;
		}
	    LOG( MLUCLog::kDebug, "TMSystem::Run", "TRACE / Main loop" ) << "Main loop trace " << MLUCLog::kDec << __FILE__ << ":" << __LINE__ << ENDLOG;
	    ExecuteTaskActions();
	    LOG( MLUCLog::kDebug, "TMSystem::Run", "TRACE / Main loop" ) << "Main loop trace " << MLUCLog::kDec << __FILE__ << ":" << __LINE__ << ENDLOG;
// 	    if ( StartRequiredProcesses() )
// 		usleep( 10000 );
// 	    if ( TerminateRequiredProcesses() )
// 		usleep( 10000 );
// 	    if ( CleanupRequiredProcesses() )
// 		usleep( 10000 );
	    LOG( MLUCLog::kDebug, "TMSystem::Run", "TRACE / Main loop" ) << "Main loop trace " << MLUCLog::kDec << __FILE__ << ":" << __LINE__ << ENDLOG;
	    if ( WaitForInterrupt( fPollTime*pollFactor ) )
		{
	    LOG( MLUCLog::kDebug, "TMSystem::Run", "TRACE / Main loop" ) << "Main loop trace " << MLUCLog::kDec << __FILE__ << ":" << __LINE__ << ENDLOG;
		if ( CheckInterrupts() )
		    {
	    LOG( MLUCLog::kDebug, "TMSystem::Run", "TRACE / Main loop" ) << "Main loop trace " << MLUCLog::kDec << __FILE__ << ":" << __LINE__ << ENDLOG;
		    usleep( 0 );
		    pollFactor = 1;
		    }
		}
	    LOG( MLUCLog::kDebug, "TMSystem::Run", "TRACE / Main loop" ) << "Main loop trace " << MLUCLog::kDec << __FILE__ << ":" << __LINE__ << ENDLOG;
	    if ( fReadNewConfig )
		{
		ReadConfig();
		fReadNewConfig = false;
		}
	    LOG( MLUCLog::kDebug, "TMSystem::Run", "TRACE / Main loop" ) << "Main loop trace " << MLUCLog::kDec << __FILE__ << ":" << __LINE__ << ENDLOG;
	    }
	Stop();
	Cleanup();
	if ( !fQuitSlave )
	    Reset();
	}
    if ( fSlaveControl )
	fSlaveControl->Stop();

    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 10000000;

    fPythonLoop->Quit();
    deltaT = 0;
    gettimeofday( &start, NULL );
    while ( fPythonLoop->IsRunning() && deltaT<=timeLimit )
	{
	usleep( 0 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( fPythonLoop->IsRunning() )
	fPythonLoop->Abort();
    else
	fPythonLoop->Join();

    fPeriodicPollPythonLoop->Quit();
    deltaT = 0;
    gettimeofday( &start, NULL );
    while ( fPeriodicPollPythonLoop->IsRunning() && deltaT<=timeLimit )
	{
	usleep( 0 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( fPeriodicPollPythonLoop->IsRunning() )
	fPeriodicPollPythonLoop->Abort();
    else
	fPeriodicPollPythonLoop->Join();

    fInterfaceLibrary->StopInterruptWait();
    if ( fInterruptWaitThread->IsRunning() )
	fInterruptWaitThread->Abort();
    else
	fInterruptWaitThread->Join();
    fTimer.Stop();
    }

void TMSystem::Start()
    {
//     fInterfaceLibrary->Initialize();
    if ( !fStartupAction.IsEmpty() && !fStartupAction.IsWhiteSpace() )
	{
	fMainPythonInterface->Run( fStartupAction.c_str() );
	}
    else
	{
	vector<TMProcess*> procs;
	pthread_mutex_lock( &fProcessMutex );
	procs = fProcesses;
	pthread_mutex_unlock( &fProcessMutex );
	vector<TMProcess*>::iterator iter, end;
	iter = procs.begin();
	end = procs.end();
	//#define USE_START_MODULO
#ifdef USE_START_MODULO
	const unsigned long startModulo = 10;
	unsigned long startCnt=0;
#endif
	while ( iter != end )
	    {
	    try
		{
		(*iter)->Start();
		(*iter)->GetInterruptWaitThread()->SetProcessPID( (*iter)->GetNdx(), (*iter)->GetPID() );
		}
	    catch ( TMSysException e )
		{
		LOG( MLUCLog::kError, "TMSystem::Start", "Error Starting Process" )
		    << "Error starting process '" << (*iter)->GetID()
		    << "' (Index " << MLUCLog::kDec << (*iter)->GetNdx()
		    << "): " << e.GetErrorStr() << " (" << e.GetErrno()
		    << "): " << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
		    << e.GetLine() << ")" << "." << ENDLOG;
		//(*iter)->Reset();
		}
#ifdef USE_START_MODULO
	    if ( !(++startCnt % startModulo) )
		usleep( 0 );
#endif
	    iter++;
	    }
	}
//     fInterfaceLibrary->Terminate();
    }

void TMSystem::Stop()
    {
    vector<TMProcess*> procs;
    pthread_mutex_lock( &fProcessMutex );
    procs = fProcesses;
    pthread_mutex_unlock( &fProcessMutex );
    vector<TMProcess*>::iterator iter, end;
    iter = procs.begin();
    end = procs.end();
    while ( iter != end )
	{
	try
	    {
	    if ( (*iter)->IsStarted() || (*iter)->IsSlave() )
		{
		(*iter)->Kill( true );
		(*iter)->GetInterruptWaitThread()->SetProcessPID( (*iter)->GetNdx(), 0 );
		}
	    }
	catch ( TMSysException e )
	    {
	    LOG( MLUCLog::kError, "TMSystem::Stop", "Error Killing Process" )
		<< "Error killing process '" << (*iter)->GetID()
		<< "' (Index " << MLUCLog::kDec << (*iter)->GetNdx()
		<< "): " << e.GetErrorStr() << " (" << e.GetErrno()
		<< "): " << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
		<< e.GetLine() << ")" << "." << ENDLOG;
	    //(*iter)->Reset();
	    }
	iter++;
	}
    }

void TMSystem::Cleanup()
    {
    vector<TMProcess*> procs;
    pthread_mutex_lock( &fProcessMutex );
    procs = fProcesses;
    pthread_mutex_unlock( &fProcessMutex );
    vector<TMProcess*>::iterator iter, end;
    iter = procs.begin();
    end = procs.end();
    while ( iter != end )
	{
	try
	    {
	    (*iter)->Cleanup();
	    }
	catch ( TMSysException e )
	    {
	    LOG( MLUCLog::kError, "TMSystem::Cleanup", "Error Cleaning Process Resources" )
		<< "Error cleanup process resources '" << (*iter)->GetID()
		<< "' (Index " << MLUCLog::kDec << (*iter)->GetNdx()
		<< "): " << e.GetErrorStr() << " (" << e.GetErrno()
		<< "): " << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
		<< e.GetLine() << ")" << "." << ENDLOG;
	    }
	iter++;
	}
    }

void TMSystem::Reset()
    {
    bool hasChanged;
    bool running;
    bool normalExit;
    bool signalExit;
    int retval;
    MLUCString state;
    vector<TMProcess*> procs;
    pthread_mutex_lock( &fProcessMutex );
    procs = fProcesses;
    pthread_mutex_unlock( &fProcessMutex );
    vector<TMProcess*>::iterator iter, end;
    iter = procs.begin();
    end = procs.end();
    while ( iter != end )
	{
	try
	    {
	    (*iter)->CheckState( hasChanged, running, normalExit, signalExit, retval, state );
	    }
	catch ( TMSysException e )
	    {
	    LOG( MLUCLog::kError, "TMSystem::Cleanup", "Error Cleaning Process Resources" )
		<< "Error cleanup process resources '" << (*iter)->GetID()
		<< "' (Index " << MLUCLog::kDec << (*iter)->GetNdx()
		<< "): " << e.GetErrorStr() << " (" << e.GetErrno()
		<< "): " << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
		<< e.GetLine() << ")" << "." << ENDLOG;
	    }
	(*iter)->Reset();
	iter++;
	}
    }


bool TMSystem::CheckSlaveControlCommands()
    {
    if ( !fSlaveControl )
	return false;
    bool cmdDone = false;
    MLUCString cmd;
    while ( fSlaveControl->GetSysCommand( cmd ) )
	{
	cmdDone = true;
	if ( cmd=="TM:START" )
	    Start();
	else if ( cmd=="TM:KILL" )
	    QuitTask();
	else if ( cmd=="TM:TERMINATE" )
	    QuitSlave();
	else if ( cmd =="TM:READCONFIG" )
	    ReadNewConfig();
	else
	    {
	    LOG( MLUCLog::kError, "TMSystem::CheckSlaveControlCommands", "Unexpected command" )
		<< "Unexpected command '" << cmd.c_str() << "' received." << ENDLOG;
	    }
	}
    if ( fSlaveControl->HasCommands() )
	{
	cmdDone = true;
	fSlaveControl->CommandReceived();
	}
    return cmdDone;
    }

bool TMSystem::CheckProcessStates()
    {
    bool change = false;
    vector<TMProcess*> procs;
    pthread_mutex_lock( &fProcessMutex );
    procs = fProcesses;
    pthread_mutex_unlock( &fProcessMutex );
//    vector<TMProcess*>::iterator iter, end;

#if 1

//    iter = procs.begin();
//    end = procs.end();

    int numProcs = procs.size();

    MLUCString st;
    GetTaskState(st);
    bool reduce_load = st == "busy" || st == "runing";
    int num_threads = reduce_load ? 1 : 24;
    
#pragma omp parallel for num_threads(num_threads)
    for (int i = 0;i < numProcs;i++)
	{
	TMProcess** iter = &procs[i];
	bool hasChanged;
	bool running;
	bool normalExit;
	bool signalExit;
	int retval;
	MLUCString state;
	try
	    {
	    hasChanged = false;
	    MLUCString oldState;
	    (*iter)->QueryOldState( oldState );
	    (*iter)->CheckState( hasChanged, running, normalExit, signalExit, retval, state );
#if 0
	    LOG( MLUCLog::kImportant, "TMSystem::CheckProcessStates", "Checking Process State" )
		<< "State of process '" << (*iter)->GetID()
		<< "' (Index " << MLUCLog::kDec << (*iter)->GetNdx()
		<< "): old state: " << oldState.c_str()
		<< " - state: " << state.c_str()
		<< " - hasChanged: " << (hasChanged ? "true" : "false")
		<< " - running: " << (running ? "true" : "false")
		<< " - normalExit: " << (normalExit ? "true" : "false")
		<< " - signalExit: " << (signalExit ? "true" : "false")
		<< " - retval: " << MLUCLog::kDec << retval
		<< ENDLOG;
#endif
	    if ( (oldState != state) && !hasChanged )
		{
		LOG( MLUCLog::kFatal, "TMSystem::CheckProcessStates", "Internal Error Checking Process State" )
		    << "Error checking state of process '" << (*iter)->GetID()
		    << "' (Index " << MLUCLog::kDec << (*iter)->GetNdx()
		    << "): State marked incorrectly as non-changed - old state: "
		    << oldState.c_str() << " - state: " << state.c_str()
		    << " - hasChanged: " << (hasChanged ? "true" : "false")
		    << " - running: " << (running ? "true" : "false")
		    << " - normalExit: " << (normalExit ? "true" : "false")
		    << " - signalExit: " << (signalExit ? "true" : "false")
		    << " - retval: " << MLUCLog::kDec << retval
		    << ENDLOG;
		hasChanged = true;
		}
	    if ( hasChanged )
		{
		change = true;
		//fMainPythonInterface->Run( (*iter)->GetStateChangeAction() );
		(*iter)->StateChange();
		}
	    }
	catch ( TMSysException e )
	    {
	    LOG( MLUCLog::kError, "TMSystem::CheckProcessStates", "Error Checking Process State" )
		<< "Error checking state of process '" << (*iter)->GetID()
		<< "' (Index " << MLUCLog::kDec << (*iter)->GetNdx()
		<< "): " << e.GetErrorStr() << " (" << e.GetErrno()
		<< "): " << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
		<< e.GetLine() << ")" << "." << ENDLOG;
	    }
	iter++;
	}
#else
    const unsigned workerThreadCnt = 60;
    TProcessStateWorkerThread workerThreads[ workerThreadCnt ];
    vector<TMProcess*> startedProcs;

    for ( unsigned nn=0; nn<workerThreadCnt; ++nn )
	{
	if ( procs.size()<=0 )
	    break;
	workerThreads[nn].SetProcess( (*procs.begin()) );
	MLUCThread::TState retval = workerThreads[nn].Start();
	if ( retval==MLUCThread::kStarted )
	    {
	    startedProcs.push_back( (*procs.begin()) );
	    procs.erase( procs.begin() );
	    }
	else
	    {
	    LOG( MLUCLog::kError, "TMSystem::CheckProcessStates", "Error Starting Thread to Check Process State" )
		<< "Error starting thread to check state of process " << (*procs.begin())->GetID() << "." << ENDLOG;
	    }
	}

    
    while ( (procs.size()+startedProcs.size())>0 )
	{
	for ( unsigned nn=0; nn<workerThreadCnt; ++nn )
	    {
	    if ( !workerThreads[nn].IsRunning() )
		{
		TMProcess* proc = workerThreads[nn].GetProcess();
		if ( proc )
		    {
		    iter = startedProcs.begin();
		    end = startedProcs.end();
		    while ( iter!=end )
			{
			if ( (*iter) == proc )
			    {
			    if ( workerThreads[nn].HasStateChanged() )
				change = true;
			    startedProcs.erase( iter );
			    break;
			    }
			++iter;
			}
		    workerThreads[nn].Join();
		    }
		if ( procs.size()>0 )
		    {
		    workerThreads[nn].SetProcess( (*procs.begin()) );
		    MLUCThread::TState retval = workerThreads[nn].Start();
		    if ( retval==MLUCThread::kStarted )
			{
			startedProcs.push_back( (*procs.begin()) );
			procs.erase( procs.begin() );
			}
		    else
			{
			LOG( MLUCLog::kError, "TMSystem::CheckProcessStates", "Error Starting Thread to Check Process State" )
			    << "Error starting thread to check state of process " << (*procs.begin())->GetID() << "." << ENDLOG;
			}
		    }
		else
		    workerThreads[nn].SetProcess( NULL );
		}
	    }
	if ( (procs.size()+startedProcs.size())>0 )
	    usleep( 1000 );
	}
    
#endif
    if ( change )
	fMainPythonInterface->Run( fStateChangeAction.c_str() );
    if (reduce_load) usleep(400000);
    return change;
    }

bool TMSystem::ExecuteTaskActions()
    {
    MLUCVector<TaskAction> acts;
    TaskAction act;
    TMProcess* proc;
    GetTaskActions( acts );
    bool execd = false;
    while ( acts.GetCnt()>0 )
	{
	act = acts.GetFirst();
	proc = GetProcess( act.fProcNdx );
	switch ( act.fAction )
	    {
	    case TaskAction::kNone:
		break;
	    case TaskAction::kStart:
		try
		    {
		    proc->Reset();
		    proc->Start();
		    proc->GetInterruptWaitThread()->SetProcessPID( proc->GetNdx(), proc->GetPID() );
		    }
		catch ( TMSysException e )
		    {
		    if ( !proc->SuppressComErrorLogs() )
			{
			LOG( MLUCLog::kError, "TMSystem::StartRequiredProcesses", "Error Starting Process" )
			    << "Error starting process '" << proc->GetID()
			    << "' (Index " << MLUCLog::kDec << proc->GetNdx()
			    << "): " << e.GetErrorStr() << " (" << e.GetErrno()
			    << "): " << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
			    << e.GetLine() << ")" << "." << ENDLOG;
			}
		    }
		execd = true;
		break;
	    case TaskAction::kTerminate:
		try
		    {
		    proc->Kill( true );
		    proc->GetInterruptWaitThread()->SetProcessPID( proc->GetNdx(), 0 );
		    }
		catch ( TMSysException e )
		    {
		    LOG( MLUCLog::kError, "TMSystem::TerminateRequiredProcesses", "Error Killing Process" )
			<< "Error killing process '" << proc->GetID()
			<< "' (Index " << MLUCLog::kDec << proc->GetNdx()
			<< "): " << e.GetErrorStr() << " (" << e.GetErrno()
			<< "): " << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
			<< e.GetLine() << ")" << "." << ENDLOG;
		    }
		execd = true;
		break;
	    case TaskAction::kCleanup:
		try
		    {
		    proc->Cleanup();
		    }
		catch ( TMSysException e )
		    {
		    LOG( MLUCLog::kError, "TMSystem::CleanupRequiredProcesses", "Error Cleaning Process Resources" )
			<< "Error cleanup process resources '" << proc->GetID()
			<< "' (Index " << MLUCLog::kDec << proc->GetNdx()
			<< "): " << e.GetErrorStr() << " (" << e.GetErrno()
			<< "): " << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
			<< e.GetLine() << ")" << "." << ENDLOG;
		    }
		execd = true;
		break;
	    }
	acts.RemoveFirst();
//	usleep( 10000 );// Wait necessary??
	}
    return execd;
    }


// bool TMSystem::StartRequiredProcesses()
//     {
//     MLUCVector<unsigned long> procs;
//     TMProcess* proc;
//     GetProcessesToStart( procs );
//     bool started = (procs.GetCnt()>0);
//     while ( procs.GetCnt()>0 )
// 	{
// 	proc = GetProcess( procs.GetFirst() );
// 	procs.RemoveFirst();
// 	    try
// 		{
// 		proc->Reset();
// 		proc->Start();
// 		fInterruptWaitThread->SetProcessPID( proc->GetNdx(), proc->GetPID() );
// 		}
// 	    catch ( TMSysException e )
// 		{
// 		LOG( MLUCLog::kError, "TMSystem::StartRequiredProcesses", "Error Starting Process" )
// 		    << "Error starting process '" << proc->GetID()
// 		    << "' (Index " << MLUCLog::kDec << proc->GetNdx()
// 		    << "): " << e.GetErrorStr() << " (" << e.GetErrno()
// 		    << "): " << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
// 		    << e.GetLine() << ")" << "." << ENDLOG;
// 		}
// 	}
//     return started;
//     }

// bool TMSystem::TerminateRequiredProcesses()
//     {
//     MLUCVector<unsigned long> procs;
//     TMProcess* proc;
//     GetProcessesToTerminate( procs );
//     bool killed = (procs.GetCnt()>0);
//     while ( procs.GetCnt()>0 )
// 	{
// 	proc = GetProcess( procs.GetFirst() );
// 	procs.RemoveFirst();
// 	try
// 	    {
// 	    proc->Kill( true );
// 	    fInterruptWaitThread->SetProcessPID( proc->GetNdx(), 0 );
// 	    }
// 	catch ( TMSysException e )
// 	    {
// 	    LOG( MLUCLog::kError, "TMSystem::TerminateRequiredProcesses", "Error Killing Process" )
// 		<< "Error killing process '" << proc->GetID()
// 		<< "' (Index " << MLUCLog::kDec << proc->GetNdx()
// 		<< "): " << e.GetErrorStr() << " (" << e.GetErrno()
// 		<< "): " << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
// 		<< e.GetLine() << ")" << "." << ENDLOG;
// 	    }
// 	}
//     return killed;
//     }

// bool TMSystem::CleanupRequiredProcesses()
//     {
//     MLUCVector<unsigned long> procs;
//     TMProcess* proc;
//     GetProcessesToCleanup( procs );
//     bool killed = (procs.GetCnt()>0);
//     while ( procs.GetCnt()>0 )
// 	{
// 	proc = GetProcess( procs.GetFirst() );
// 	procs.RemoveFirst();
// 	try
// 	    {
// 	    proc->Cleanup();
// 	    }
// 	catch ( TMSysException e )
// 	    {
// 	    LOG( MLUCLog::kError, "TMSystem::CleanupRequiredProcesses", "Error Cleaning Process Resources" )
// 		<< "Error cleanup process resources '" << proc->GetID()
// 		<< "' (Index " << MLUCLog::kDec << proc->GetNdx()
// 		<< "): " << e.GetErrorStr() << " (" << e.GetErrno()
// 		<< "): " << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
// 		<< e.GetLine() << ")" << "." << ENDLOG;
// 	    }
// 	}
//     return killed;
//     }

bool TMSystem::CheckInterrupts()
    {
    MLUCVector<InterruptingProcessData> procs;
    TMProcess* proc;
    GetInterruptingProcesses( procs );
    bool interrupted = (procs.GetCnt()>0);
    MLUCVector<InterruptingProcessData>::TIterator procIter;
    vector<LAMData>::iterator lamProcIter, lamProcEnd;
    procIter = procs.Begin();
    pthread_mutex_lock( &fLAMProcessListMutex );
    while ( procIter )
	{
	proc = GetProcess( procs.Get( procIter ).fNdx );
	lamProcIter = fLAMProcessList.begin();
	lamProcEnd = fLAMProcessList.end();
	while ( lamProcIter != lamProcEnd )
	    {
	    if ( lamProcIter->fProcess == proc )
		{
		lamProcIter->fNotificationData.push_back( procs.Get( procIter ).fNotificationData );
		break;
		}
	    lamProcIter++;
	    }
	if ( lamProcIter==lamProcEnd )
	    {
	    LAMData ld;
	    ld.fProcess = proc;
	    ld.fNotificationData.push_back( procs.Get( procIter ).fNotificationData );
	    fLAMProcessList.push_back( ld );
	    }
	++procIter;
	}
    pthread_mutex_unlock( &fLAMProcessListMutex );

    while ( procs.GetCnt()>0 )
	{
	proc = GetProcess( procs.GetFirst().fNdx );
	procs.RemoveFirst();
	proc->Interrupt();
	}
    if ( interrupted )
	fMainPythonInterface->Run( fInterruptReceivedAction.c_str() );
    
    return interrupted;
    }

void TMSystem::ReadConfig()
    {
#if 0
    LOG( MLUCLog::kError, "TMSystem::ReadConfig", "Not yet suported" )
	<< "Configuration reading not yet supported/enabled." << ENDLOG;
    return;
#warning Functionality currently still disabled...
#else

    // Read configuration file again. (Currently always the same file is read)
    TMConfig* newconf;
    TMConfig* oldconf;
    try
	{
	newconf = new TMConfig();
	if ( !newconf )
	    {
	    LOG( MLUCLog::kError, "TMSystem::ReadConfig", "Unable to create new configuration object" )
		<< "Unable to create new configuration object, presumably out of memory."
		<< ENDLOG;
	    return;
	    }
	newconf->ReadConfig( fConfigFile.c_str() );
	}
    catch ( TMConfig::TMConfigException e )
	{
	LOG( MLUCLog::kError, "TMSystem::ReadConfig", "Configuration Error" )
	    << "Error reading in new configuration from file '"
	    << e.GetFilename() << "': " << e.GetErrorTypeName()
	    << " (" << MLUCLog::kDec << e.GetErrorType() << ") - "
	    << e.GetError() << "." << ENDLOG;
	return;
	}
    oldconf = fConfig;
    fConfig = newconf;

    // Get the process configuration data from the old and new configurations.
    // (Old might not be necessary, can be retrieved from the process objects themselves)

    bool removed = false;

    vector<TMConfig::ProcessData> oldProcs, newProcs;
    vector<TMConfig::ProcessData>::iterator /*oldIter, oldEnd,*/ newIter, newEnd;
    oldProcs = oldconf->GetProcessData();
    newProcs = newconf->GetProcessData();
    unsigned long oldCnt = oldProcs.size();
    unsigned long n;
    // Keep track of the state of each of the processes
    //vector<ConfigChangeProcessState> procState;
    //map<unsigned long, ConfigChangeProcessState> procState;
    for ( n = 0; n < fProcesses.size(); n++ )
	{
	//fProcState.insert( fProcState.end(), kUnknown );
	fProcState[ fProcesses[n]->GetNdx() ] = kUnknown;
	}
    //vector<TMConfig::ProcessData*> procConfig;
    map<unsigned long, TMConfig::ProcessData*> procConfig;
    for ( n = 0; n < fProcesses.size(); n++ )
	{
	//procConfig.insert( procConfig.end(), NULL );
	procConfig[ fProcesses[n]->GetNdx() ] = NULL;
	}
    vector<TMProcess*> procs;
    vector<TMProcess*>::iterator procIter, procEnd;
    procs = fProcesses;

    // Compare the new with the old configuration to determine
    // which processes will be removed (made inactive), which 
    // have been added, or which have been changed
    newIter = newProcs.begin();
    newEnd = newProcs.end();
    while ( newIter != newEnd )
	{
	bool oldFound=false;
	procIter = procs.begin();
	procEnd = procs.end();
	while ( procIter != procEnd )
	    {
	    if ( newIter->fID==(*procIter)->GetID() )
		{
		// this process has been found in both configurations
		oldFound = true;
		break;
		}
	    procIter++;
	    }
	if ( oldFound )
	    {
	    if ( newIter->fCommand!=(*procIter)->GetCommand() )
		{
		// Command has changed, we have to restart the process.
		if ( newIter->fCommand.IsWhiteSpace() )
		    {
		    fProcState[(*procIter)->GetNdx()] = kRemoved; // Empty command, process is removed
		    }
		else
		    {
		    if ( (*procIter)->IsStarted() )
			fProcState[(*procIter)->GetNdx()] = kChanged; // Changed command
		    else
			fProcState[(*procIter)->GetNdx()] = kChangedNotStarted; // Changed command
		    }
		procConfig[(*procIter)->GetNdx()] = &(*newIter);
		}
	    else
		{
		// Command is unchanged, process is not restarted.
		fProcState[(*procIter)->GetNdx()] = kUnchanged;
		procConfig[(*procIter)->GetNdx()] = &(*newIter);
		}
	    }
	else
	    {
	    // Process has not been found among the existing processes, 
	    // it has to be added.
	    unsigned long newNdx = GetNextProcID( procs );
	    if ( fProcState.find( newNdx )!=fProcState.end() || procConfig.find( newNdx )!=procConfig.end() )
		{
		LOG( MLUCLog::kFatal, "TMSystem::ReadConfig", "Internal Error" )
		    << "Internal Error: Process Index " << MLUCLog::kDec
		    << newNdx << " (0x" << MLUCLog::kHex << newNdx
		    << ") present twice." << ENDLOG;
		fProcState.clear();
		delete oldconf;
		return;
		}
	    fProcState[ newNdx ] = kAdded;
	    procConfig[ newNdx ] = &(*newIter);
	    //fProcState.insert( fProcState.end(), kAdded );
	    //procConfig.insert( procConfig.end(), &(*newIter) );
	    TMProcess* proc;
	    proc = new TMProcess( this, newIter->fID.c_str(), newNdx, *newIter );
	    // Exceptions are not caught, 
	    // only exception thrown by process constructor signals fatal internal error
	    procs.insert( procs.end(), proc );
	    }
	newIter++;
	}
    for ( n = 0; n < oldCnt; n++ )
	{
	// Any process whose state we haven't set so far did not appear in the new configuration.
	if ( fProcState[procs[n]->GetNdx()]==kUnknown )
	    fProcState[procs[n]->GetNdx()] = kRemoved;
	}
    for ( n = 0; n < procs.size(); n++ )
	{
	LOG( MLUCLog::kDebug, "TMSystem::ReadConfig", "Process configuration change state" )
	    << "Process " << MLUCLog::kDec << n << " configuration change state: "
	    << (unsigned)fProcState[procs[n]->GetNdx()] << "." << ENDLOG;
	}

    fProcessExecutionStateHandler->Resize( procs.size() );
    procIter = procs.begin();
    procEnd = procs.end();
    while ( procIter != procEnd )
	{
	if ( fProcState[ (*procIter)->GetNdx() ] == kAdded )
	    fProcessExecutionStateHandler->AddProcess( (*procIter)->GetNdx() );
	procIter++;
	}


    // Start the configuration change
    fMainPythonInterface->Run( fPreConfigChangePhase1Action.c_str() );
    // Loop over all processes and do the first part of the action
    // for them. (execute pre config change phase 1 action)
    // Terminate removed processes and those that have to be restarted.
    procIter = procs.begin();
    procEnd = procs.end();
    while ( procIter != procEnd )
	{
	n = (*procIter)->GetNdx();
	switch ( fProcState[ n ] )
	    {
	    case kUnknown:
		LOG( MLUCLog::kFatal, "TMSystem::ReadConfig", "Internal Error" )
		    << "Internal Error: Process " << MLUCLog::kDec
		    << n << " (" << (*procIter)->GetID()
		    << ") has unknown state. Process is ignored." << ENDLOG;
		break;
	    case kUnchanged:
		break; // No action necessary
	    case kAdded:
		// Only phase 2 is executed for new processes
		break;
	    case kRemoved: // Fallthrough
	    case kChanged: // Fallthrough
	    case kChangedNotStarted:
		try
		    {
		    // Inform about the start of the configuration change phase 1
		    (*procIter)->ConfigChangingPhase1();
		    }
		catch ( TMSysException e )
		    {
		    LOG( MLUCLog::kError, "TMSystem::TerminateRequiredProcesses", "Error Executing Config Change Phase 1 Action for Process" )
			<< "Error executing config change phase 1 action for  changed process '" << (*procIter)->GetID()
			<< "' (Index " << MLUCLog::kDec << (*procIter)->GetNdx()
			<< "): " << e.GetErrorStr() << " (" << e.GetErrno()
			<< "): " << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
			<< e.GetLine() << ")" << "." << ENDLOG;
		    }
		break;
	    }
	procIter++;
	}

    // Obtain the new state supervision configuration 
    TMConfig::StateSupervisionConfigData supervisionData;
    supervisionData = newconf->GetStateSupervisionConfigData();
   
    // Note: Here we HAVE to use the action from the new configuration.
    fMainPythonInterface->Run( supervisionData.fPreConfigChangePhase2Action.c_str() );
    // Loop over all processes and do the second part of the action
    // for them. (Set the new configuration for all)
    // Terminate removed processes and those that have to be restarted.
    procIter = procs.begin();
    procEnd = procs.end();
    while ( procIter != procEnd )
	{
	n = (*procIter)->GetNdx();
#if 0
	// Set the new configuration
	// Done below.
	if ( procConfig[n] )
	    (*procIter)->Set( *procConfig[n] );
#endif
	// Inform about the start of the configuration change phase 2
	switch ( fProcState[ n ] )
	    {
	    case kUnknown:
		LOG( MLUCLog::kFatal, "TMSystem::ReadConfig", "Internal Error" )
		    << "Internal Error: Process " << MLUCLog::kDec
		    << n << " (" << (*procIter)->GetID()
		    << ") has unknown state. Process is ignored." << ENDLOG;
		break;
	    case kUnchanged:
		break; // No action necessary
	    case kRemoved:
		// Removed processed will not appear anymore in the new configuration...
		break;
	    case kAdded: // Fallthrough
	    case kChanged: // Fallthrough
	    case kChangedNotStarted:
		try
		    {
		    TMConfig::ProcessData* thisProcConfig = procConfig[n];
		    // Inform about the start of the configuration change phase 2
		    fMainPythonInterface->Run( thisProcConfig->fPreConfigChangePhase2Action.c_str() );
		    }
		catch ( TMSysException e )
		    {
		    LOG( MLUCLog::kError, "TMSystem::TerminateRequiredProcesses", "Error Executing Config Change Phase 2 Action for Process" )
			<< "Error executing config change phase 2 action for  changed process '" << (*procIter)->GetID()
			<< "' (Index " << MLUCLog::kDec << (*procIter)->GetNdx()
			<< "): " << e.GetErrorStr() << " (" << e.GetErrno()
			<< "): " << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
			<< e.GetLine() << ")" << "." << ENDLOG;
		    }
		break;
	    }
	procIter++;
	}

    bool restartInterruptWaitThread = false;
    bool newLib = false;
    bool restartPythonLoop = false;
    bool restartPeriodicPollPythonLoop = false;

    // Check whether the interface library has to be reloaded and
    // whether one or more of the background threads have
    // to be restarted.
    TMConfig::InterfaceLibraryData interfaceLibData;
    interfaceLibData = newconf->GetInterfaceLibraryData();
#define NOINTERRUPTWAITTHREADRESTART
#ifndef NOINTERRUPTWAITTHREADRESTART
    if ( interfaceLibData.fPlatform!=fInterfaceLibrary->GetPlatform() ||
	 interfaceLibData.fBinary!=fInterfaceLibrary->GetBinary() )
	{
	restartInterruptWaitThread = true;
	newLib = true;
	}
#endif

    if ( supervisionData.fPeriodicPollAction!=fPeriodicPollAction )
	restartPeriodicPollPythonLoop = true;
    if ( supervisionData.fLoopAction!=fLoopAction )
	restartPythonLoop = true;

    
    // Obtain the new slave control configuration data
    TMConfig::SlaveControlConfigData slaveControlConfigData;
    slaveControlConfigData = newconf->GetSlaveControlConfigData();
    bool slaveControlChange=false;
#define NOSLAVECONTROLCHANGE
#ifndef NOSLAVECONTROLCHANGE
    // Check whether the slave control object has changed.
    if ( (fSlaveControl && !slaveControlConfigData.fActive) ||
	 (!fSlaveControl && slaveControlConfigData.fActive) )
	slaveControlChange = true;
    if ( fSlaveControl )
	{
	if ( slaveControlConfigData.fAddress != fSlaveControl->GetAddress() )
	    slaveControlChange = true;
	if ( slaveControlConfigData.fCommandReceivedAction != fSlaveControl->GetCommandReceivedAction() )
	    slaveControlChange = true;

	vector<MLUCString> addresses;
	fSlaveControl->GetInterruptTargetAddresses( addresses );
	if ( addresses.size()!=slaveControlConfigData.fInterruptTargetAddresses.size() )
	    slaveControlChange = true;
	else
	    {
	    unsigned long i;
	    n = addresses.size();
	    for ( i = 0; i < n; i++ )
		{
		if ( addresses[i]!=slaveControlConfigData.fInterruptTargetAddresses[n] )
		    {
		    slaveControlChange = true;
		    break;
		    }
		}
	    }
	}
#endif

    vector<TMConfig::PythonExtensionData> pythonExtensionData;
    pythonExtensionData = newconf->GetPythonExtensionData();
    vector<TMConfig::PythonExtensionData>::iterator pyExtIter, pyExtEnd;
    vector<TMPythonExtension*>::iterator peIter, peEnd;
    pyExtIter = pythonExtensionData.begin();
    pyExtEnd = pythonExtensionData.end();
    TMPythonExtension* pyExt;
    bool found;
    while ( pyExtIter != pyExtEnd )
	{
	// Check whether we already have an extension object of that kind
	found=false;
	peIter = fPythonExtensions.begin();
	peEnd = fPythonExtensions.end();
	while ( peIter != peEnd )
	    {
	    if ( (*peIter)->GetModule() == pyExtIter->fModule &&
		 (*peIter)->GetPlatform() == pyExtIter->fPlatform &&
		 (*peIter)->GetBinary() == pyExtIter->fBinary )
		{
		found=true;
		break;
		}
	    peIter++;
	    }
	if ( !found )
	    {
	    pyExt = new TMPythonExtension( pyExtIter->fModule.c_str(), pyExtIter->fPlatform.c_str(), pyExtIter->fBinary.c_str() );
	    if ( !pyExt )
		throw TMSysException( ENOMEM, strerror(ENOMEM), "Unable to create python extension object",
				      __FILE__, __LINE__ );
	    pyExt->Init();
	    fPythonExtensions.push_back( pyExt );
	    }
	pyExtIter++;
	}

    do
	{
	removed = false;
	peIter = fPythonExtensions.begin();
	peEnd = fPythonExtensions.end();
	while ( peIter != peEnd )
	    {
	    found=false;
	    pyExtIter = pythonExtensionData.begin();
	    pyExtEnd = pythonExtensionData.end();
	    while ( pyExtIter != pyExtEnd )
		{
		if ( (*peIter)->GetModule() == pyExtIter->fModule &&
		     (*peIter)->GetPlatform() == pyExtIter->fPlatform &&
		     (*peIter)->GetBinary() == pyExtIter->fBinary )
		    {
		    found=true;
		    break;
		    }
		pyExtIter++;
		}
	    if ( !found )
		{
		delete *peIter;
		fPythonExtensions.erase( peIter );
		removed = true;
		break;
		}
	    peIter++;
	    }
	}
    while ( removed );


    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    // Stop any of the three background threads that has to be restarted.
    if ( restartInterruptWaitThread )
	{
	fInterfaceLibrary->StopInterruptWait();
	deltaT = 0;
	gettimeofday( &start, NULL );
	while ( fInterfaceLibrary->InterruptWaitRunning() && deltaT<timeLimit )
	    {
	    usleep( 1000 );
	    gettimeofday( &now, NULL );
	    deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	    }
	if ( fInterfaceLibrary->InterruptWaitRunning() )
	    {
	    LOG( MLUCLog::kError, "TMSystem::ReadConfig", "Interrupt Wait Thread Does Not Terminate" )
		<< "Interrupt wait thread does not terminate normally. Aborting forcefully!!" << ENDLOG;
	    fInterruptWaitThread->Abort();
	    }
	fInterruptWaitThread->Join();
	}
    if ( restartPythonLoop )
	{
	fPythonLoop->Quit();
	fMainPythonInterface->Run( fLoopStopAction.c_str() );
	deltaT = 0;
	gettimeofday( &start, NULL );
	while ( fPythonLoop->IsRunning() && deltaT<timeLimit )
	    {
	    usleep( 1000 );
	    gettimeofday( &now, NULL );
	    deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	    }
	if ( fPythonLoop->IsRunning() ) 
	    {
	    LOG( MLUCLog::kError, "TMSystem::ReadConfig", "Python Loop Thread Does Not Terminate" )
		<< "Python loop thread does not terminate normally. Aborting forcefully!!" << ENDLOG;
	    fPythonLoop->Abort();
	    }
	fPythonLoop->Join();
	}
    if ( restartPeriodicPollPythonLoop )
	{
	n=0;
	fPeriodicPollPythonLoop->Quit();
	//fMainPythonInterface->Run( fLoopStopAction.c_str() );
	deltaT = 0;
	gettimeofday( &start, NULL );
	while ( fPeriodicPollPythonLoop->IsRunning() && deltaT<timeLimit )
	    {
	    usleep( 1000 );
	    gettimeofday( &now, NULL );
	    deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	    }
	if ( fPeriodicPollPythonLoop->IsRunning() ) 
	    {
	    LOG( MLUCLog::kError, "TMSystem::ReadConfig", "Perodic Poll Python Thread Does Not Terminate" )
		<< "Periodic poll python thread does not terminate normally. Aborting forcefully!!" << ENDLOG;
	    fPeriodicPollPythonLoop->Abort();
	    }
	fPeriodicPollPythonLoop->Join();
	}

    if ( slaveControlChange )
	{
	if ( fSlaveControl )
	    fSlaveControl->Stop();
	if ( fSlaveControl && !slaveControlConfigData.fActive )
	    {
	    delete fSlaveControl;
	    fSlaveControl = NULL;
	    slaveControlChange = false;
	    }
	}

    // Activate the new supervision configuration
    fInterruptListenAddress = supervisionData.fInterruptListenAddress;
    //NormalizeAddress( supervisionData.fInterruptListenAddress.c_str(), fInterruptListenAddress );
    fStartupAction = supervisionData.fStartupAction;
    fStateChangeAction = supervisionData.fStateChangeAction;
    fInterruptReceivedAction = supervisionData.fInterruptReceivedAction;
    fPeriodicPollAction = supervisionData.fPeriodicPollAction;
    fPeriodicPollInterval = supervisionData.fPeriodicPollInterval; // In microsec. 
    fLoopAction = supervisionData.fLoopAction;
    fLoopStopAction = supervisionData.fLoopStopAction;
    fFailOverActive = supervisionData.fFailOverActive;
    fPreConfigChangePhase1Action = supervisionData.fPreConfigChangePhase1Action;
    fPreConfigChangePhase2Action = supervisionData.fPreConfigChangePhase2Action;
    fPreConfigChangePhase3Action = supervisionData.fPreConfigChangePhase3Action;
    fPostConfigChangeAction = supervisionData.fPostConfigChangeAction;
    if ( supervisionData.fPollIntervalSet )
	fPollTime = supervisionData.fPollInterval;

    if ( newLib )
	{
	// Reload the interface library
	fInterfaceLibrary->Terminate();
	TMInterfaceLibrary* newlib=NULL;
	try
	    {
	    newlib = new TMInterfaceLibrary( interfaceLibData.fPlatform.c_str(), interfaceLibData.fBinary.c_str() );
	    if ( !newlib )
		throw TMSysException( ENOMEM, strerror(ENOMEM), "Unable to create interface library object",
				      __FILE__, __LINE__ );
// 	    newlib->Initialize( fInterruptListenAddress.c_str() );
	    }
	catch ( TMInterfaceLibrary::TMInterfaceLibraryException e )
	    {
	    LOG( MLUCLog::kError, "TMSystem::ReadConfig", "Interface Library Error" )
		<< "Error accessing new interface library '"
		<< interfaceLibData.fBinary.c_str() << "': " << e.GetErrorTypeName()
		<< " for " << e.GetName() << ": " << e.GetError() << " ("
		<< MLUCLog::kDec << e.GetErrno() << ")." << ENDLOG;
	    }
	delete fInterfaceLibrary;
	fInterfaceLibrary = newlib;
	fInterfaceLibrary->Initialize( fInterruptListenAddress.c_str() );
	}

    // Start any of the three background threads that has to be restarted.
    if ( restartInterruptWaitThread )
	{
	fInterruptWaitThread->SetListenAddress( fInterruptListenAddress.c_str() );
	fInterruptWaitThread->Start();
	}
    if ( restartPythonLoop )
	{
	fPythonLoop->SetAction( fLoopAction.c_str() );
	fPythonLoop->Start();
	}
    if ( restartPeriodicPollPythonLoop )
	{
	fPeriodicPollPythonLoop->SetAction( fPeriodicPollAction.c_str() );
	fPeriodicPollPythonLoop->Start();
	}
    usleep( 5000 );

    if ( slaveControlChange )
	{
	if ( !fSlaveControl && slaveControlConfigData.fActive )
	    {
	    fSlaveControl = new TMSlaveControl( this, slaveControlConfigData );
	    if ( !fSlaveControl )
		{
		fProcState.clear();
		throw TMSysException( ENOMEM, strerror(ENOMEM), "Unable to create slave control object",
				      __FILE__, __LINE__ );
		}
	    }
	else
	    fSlaveControl->Set( slaveControlConfigData );
	fSlaveControl->Start();
	}
#ifdef NOSLAVECONTROLCHANGE
    // Check whether the slave control object has changed.
    if ( fSlaveControl && slaveControlConfigData.fCommandReceivedAction != fSlaveControl->GetCommandReceivedAction() )
	{
	fSlaveControl->SetCommandReceivedAction( slaveControlConfigData.fCommandReceivedAction );
	}
#endif

    // Now we can erase the removed processes from the new process list 
    // as well as the process execution state handler's and interrupt wait
    // thread's list.
    do
	{
	removed = false;
	procIter = procs.begin();
	procEnd = procs.end();
	while ( procIter != procEnd )
	    {
	    if ( fProcState[ (*procIter)->GetNdx() ] == kRemoved )
		{
		(*procIter)->Kill( true );
		fProcessExecutionStateHandler->DeleteProcess( (*procIter)->GetNdx() );
		(*procIter)->GetInterruptWaitThread()->DeleteProcess( (*procIter)->GetNdx() );
		fProcState.erase( (*procIter)->GetNdx() );
		delete (*procIter);
		procs.erase( procIter );
		removed = true;
		break;
		}
	    procIter++;
	    }
	}
    while ( removed );
    fProcessExecutionStateHandler->Resize( procs.size() );
    fProcessExecutionStateHandler->Compact();

    fProcesses = procs;

    procIter = fProcesses.begin();
    procEnd = fProcesses.end();
    while ( procIter != procEnd )
	{
	n = (*procIter)->GetNdx();
	if ( procConfig[n] )
	    (*procIter)->Set( *procConfig[n] );
	procIter++;
	}

    // Loop over all processes and do the next part of the action
    // for them. 
    // Start added processes and those that have to be restarted because of a configuration change.
    procIter = fProcesses.begin();
    procEnd = fProcesses.end();
    while ( procIter != procEnd )
	{
	n = (*procIter)->GetNdx();
	switch ( fProcState[ n ] )
	    {
	    case kUnknown: // Fallthrough
	    case kRemoved:
		LOG( MLUCLog::kFatal, "TMSystem::ReadConfig", "Internal Error" )
		    << "Internal Error: Process " << MLUCLog::kDec
		    << n << " (" << (*procIter)->GetID()
		    << ") has " << (fProcState[ n ]==kUnknown ? "unknown" : "removed") 
		    << " state. Process is ignored." << ENDLOG;
		break;
	    case kUnchanged:
		break; // No action necessary
	    case kAdded:
		try
		    {
		    // Inform about the start of the configuration change phase 3
		    (*procIter)->ConfigChangingPhase3();
		    if ( fStartupAction.IsEmpty() || fStartupAction.IsWhiteSpace() )
			{
			(*procIter)->Start();
			(*procIter)->GetInterruptWaitThread()->SetProcessPID( (*procIter)->GetNdx(), (*procIter)->GetPID() );
			}
		    }
		catch ( TMSysException e )
		    {
		    LOG( MLUCLog::kError, "TMSystem::ReadConfig", "Error Starting Process" )
			<< "Error starting process '" << (*procIter)->GetID()
			<< "' (Index " << MLUCLog::kDec << (*procIter)->GetNdx()
			<< "): " << e.GetErrorStr() << " (" << e.GetErrno()
			<< "): " << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
			<< e.GetLine() << ")" << "." << ENDLOG;
		    }
		break;
	    case kChanged: // Fallthrough
	    case kChangedNotStarted:
		try
		    {
		    // Inform about the start of the configuration change phase 3
		    (*procIter)->ConfigChangingPhase3();
		    if ( fProcState[ n ] != kChangedNotStarted )
			{
			(*procIter)->Start();
			(*procIter)->GetInterruptWaitThread()->SetProcessPID( (*procIter)->GetNdx(), (*procIter)->GetPID() );
			}
		    }
		catch ( TMSysException e )
		    {
		    LOG( MLUCLog::kError, "TMSystem::TerminateRequiredProcesses", "Error Killing Process" )
			<< "Error killing process '" << (*procIter)->GetID()
			<< "' (Index " << MLUCLog::kDec << (*procIter)->GetNdx()
			<< "): " << e.GetErrorStr() << " (" << e.GetErrno()
			<< "): " << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
			<< e.GetLine() << ")" << "." << ENDLOG;
		    }
		break;
	    }
	// Configuration change for this process is completed.
	(*procIter)->ConfigChanged();
	procIter++;
	}

    fProcessExecutionStateHandler->Compact();

    // Configuration change is completed.
    fMainPythonInterface->Run( fPostConfigChangeAction.c_str() );

    fProcState.clear();
    //fConfig = newconf;
    delete oldconf;
#endif
    }


// void TMSystem::NormalizeAddress( const char* origAddress, MLUCString& normAddress )
//     {
//     if ( !fInterfaceLibrary )
// 	throw TMSysException( EFAULT, strerror(EFAULT), "No interface library specified",
// 			      __FILE__, __LINE__ );
//     char* tmpAddr = fInterfaceLibrary->NormalizeAddress( origAddress );
//     if ( !tmpAddr )
// 	throw TMSysException( EINVAL, strerror(EINVAL), "Error normalizing address",
// 			      __FILE__, __LINE__ );
//     normAddress = tmpAddr;
//     fInterfaceLibrary->ReleaseResource( tmpAddr );
//     }

TMSystem::ConfigChangeProcessState TMSystem::GetProcessConfigChangeState( const char* ID )
    {
    TMProcess* proc = GetProcess( ID );
    if ( !proc )
	throw TMSysException( EINVAL, strerror(EINVAL), "Process ID not found",
			      __FILE__, __LINE__ );
    if ( fProcState.find( proc->GetNdx() )==fProcState.end() )
	throw TMSysException( EINVAL, strerror(EINVAL), "Process ID not found",
			      __FILE__, __LINE__ );
    return fProcState[ proc->GetNdx() ];
    }




void TMSystem::SignalHandler( int signum )
    {
    //printf( "Signal %d caught\n", signum );
    if ( gSystem )
	{
	if ( signum==SIGHUP )
	    gSystem->ReadNewConfig();
	else if ( signum==SIGQUIT )
	    gSystem->QuitSlave();
	}
    signal( signum, SignalHandler );

    }


void TMSystem::TProcessStateWorkerThread::Run()
    {
    bool hasChanged;
    bool running;
    bool normalExit;
    bool signalExit;
    int retval;
    MLUCString state;
    try
	{
	hasChanged = false;
	MLUCString oldState;
	fProc->QueryOldState( oldState );
	fProc->CheckState( hasChanged, running, normalExit, signalExit, retval, state );
	if ( (oldState != state) && !hasChanged )
	    {
	    LOG( MLUCLog::kFatal, "TMSystem::TProcessStateWorkerThread::Run", "Internal Error Checking Process State" )
		<< "Error checking state of process '" << fProc->GetID()
		<< "' (Index " << MLUCLog::kDec << fProc->GetNdx()
		<< "): State marked incorrectly as non-changed - old state: "
		<< oldState.c_str() << " - state: " << state.c_str()
		<< " - hasChanged: " << (hasChanged ? "true" : "false")
		<< " - running: " << (running ? "true" : "false")
		<< " - normalExit: " << (normalExit ? "true" : "false")
		<< " - signalExit: " << (signalExit ? "true" : "false")
		<< " - retval: " << MLUCLog::kDec << retval
		<< ENDLOG;
	    hasChanged = true;
	    }
	if ( hasChanged )
	    {
	    fStateChange = true;
	    fProc->StateChange();
	    }
	}
    catch ( TMSysException e )
	{
	LOG( MLUCLog::kError, "TMSystem::CheckProcessStates", "Error Checking Process State" )
	    << "Error checking state of process '" << fProc->GetID()
	    << "' (Index " << MLUCLog::kDec << fProc->GetNdx()
	    << "): " << e.GetErrorStr() << " (" << e.GetErrno()
	    << "): " << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
	    << e.GetLine() << ")" << "." << ENDLOG;
	}
    
	{
	MLUCMutex::TLocker runningLocker( fRunningMutex );
	fRunning = false;
	}
	
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
