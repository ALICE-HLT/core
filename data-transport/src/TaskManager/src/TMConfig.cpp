/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#include "TMConfig.hpp"
#include "TMSystem.hpp"
#include <MLUCLog.hpp>
#include <stdlib.h>


TMConfig::TMConfig()
    {
    // Perform per-process parser initialization. 
    // The locale is set if the Initialize() is invoked for the very first time.
    try 
	{
	XMLPlatformUtils::Initialize();
	}
    catch ( const XMLException& toCatch )
	{
	char* message = XMLString::transcode( toCatch.getMessage() );
	// 	LOG( MLUCLog::kError, "TMConfig::ReadConfig", "Error reading configuration file" )
	// 	    << "Error reading XML configuration file '" << filename << "': "
	// 	    << message << ENDLOG;
	MLUCString err( "Error initializing XML subsystem" );
	if ( message )
	    {
	    err += ": ";
	    err += message;
	    }
	err += ".";
	XMLString::release( &message );
 	LOG( MLUCLog::kError, "TMConfig::ReadConfig", "Error initializing XML subsystem" )
 	    << err.c_str() << ENDLOG;
	throw TMConfigException( TMConfigException::kXMLInitError, err.c_str() );
        }
    // Create parser
    XMLCh tempStr[129];
    const char* DOMImplName = "LS";
    if ( !XMLString::transcode( DOMImplName, tempStr, 128 ) )
	{
	LOG( MLUCLog::kError, "TMConfig::ReadConfig", "Error transcoding XML DOM implementation name" )
	    << "Error transcoding XML DOM implementation name '" << DOMImplName
	    << "'." << ENDLOG;
	throw TMConfigException( TMConfigException::kXMLInitError, "Error transcoding XML DOM implementation name" );
	}
#ifdef XERCESC_2
    fDOMImpl = DOMImplementationRegistry::getDOMImplementation( tempStr );
    if ( !fDOMImpl )
	{
 	LOG( MLUCLog::kError, "TMConfig::ReadConfig", "Cannot find XML DOM implementation" )
	    << "Unable to find XML DOM implementation '" << DOMImplName
	    << "'." << ENDLOG;
	MLUCString err = "Error transcoding XML DOM implementation name '";
	err += DOMImplName;
	err += "'";
	throw TMConfigException( TMConfigException::kXMLInitError, err.c_str() );
	}
#endif
    try 
	{
#ifdef XERCESC_2
	fParser = ((DOMImplementationLS*)fDOMImpl)->createDOMBuilder( DOMImplementationLS::MODE_SYNCHRONOUS, 0 );
#else
	fParser = new XercesDOMParser;
#endif
	}
    catch (const DOMException& toCatch) 
	{
	char* message = XMLString::transcode(toCatch.msg);
	MLUCString err( "Error while attempting to obtain XML parser" );
	if ( message )
	    {
	    err += ": ";
	    err += message;
	    }
	err += ".";
	XMLString::release( &message );
 	LOG( MLUCLog::kError, "TMConfig::ReadConfig", "Error obtaining XML parser" )
	    << err.c_str() << ENDLOG;
	throw TMConfigException( TMConfigException::kXMLInitError, err.c_str() );
        }
    if ( !fParser )
	{
 	LOG( MLUCLog::kError, "TMConfig::ReadConfig", "Cannot obtain XML parser" )
	    << "Unable to obtain XML parser." << ENDLOG;
	throw TMConfigException( TMConfigException::kXMLInitError, "Unable to obtain XML parser" );
	}

    // Set some features on this builder
#ifdef XERCESC_2
    if ( fParser->canSetFeature( XMLUni::fgDOMValidation, true ) )
	fParser->setFeature( XMLUni::fgDOMValidation, true );
    if ( fParser->canSetFeature( XMLUni::fgDOMNamespaces, true ) )
	fParser->setFeature( XMLUni::fgDOMNamespaces, true );
    if ( fParser->canSetFeature( XMLUni::fgDOMDatatypeNormalization, true ) )
	fParser->setFeature( XMLUni::fgDOMDatatypeNormalization, true );
#else
    fParser->setValidationScheme( XercesDOMParser::Val_Never );
    fParser->setDoNamespaces( false );
    fParser->setDoSchema( false );
    fParser->setLoadExternalDTD( false );
#endif
    }

TMConfig::~TMConfig()
    {
#ifdef XERCESC_2
    fParser->release();
#else
    if ( fParser )
	{
	delete fParser;
	fParser = NULL;
	}
#endif
    }

void TMConfig::ReadConfig( const char* filename, bool allowNoProcesses )
    {
    DOMDocument *doc = 0;
    DOMNode     *taskNode = 0;
    MLUCString taskName;

    // parse xml file
    ParseFile( filename, doc );

    DumpTags( filename, doc );
    
    // Get task 
    GetTask( filename, doc, taskNode, taskName );
    
    // Get/create interface library data.
    GetInterfaceLibrary( filename, taskNode, fInterfaceLibraryData );

    // Get/create the state supervision configuration
    GetStateSupervisionConfig( filename, taskNode, fStateSupervisionConfigData );

    // Get/create the different process configurations.
    GetProcesses( filename, taskNode, fProcesses, allowNoProcesses );

    // Get/create the slave control configuration
    GetSlaveControlConfig( filename, taskNode, fSlaveControlConfigData );

    // Get/create the python extensions configurations
    GetPythonExtensions( filename, taskNode, fPythonExtensions );

    }


void TMConfig::ParseFile( const char* filename, DOMDocument*& doc )
    {
    try 
	{
	// reset document pool
	fParser->resetDocumentPool();
	// parse document
#ifdef XERCESC_2
	doc = fParser->parseURI( filename );
#else
	fParser->parse( filename );
	doc = fParser->getDocument();
#endif
        }
    catch ( const XMLException& toCatch ) 
	{
	char* message = XMLString::transcode( toCatch.getMessage() );
	MLUCString err( message );
	LOG( MLUCLog::kError, "TMConfig::ParseFile", "XML Error" )
	    << "XML error parsing configuration file '" << filename
	    << "': " << message << ENDLOG;
	XMLString::release(&message);
	throw TMConfigException( TMConfigException::kXMLError, err.c_str(), filename );
        }
    catch ( const DOMException& toCatch )
	{
	char* message = XMLString::transcode(toCatch.msg);
	MLUCString err( message );
	LOG( MLUCLog::kError, "TMConfig::ParseFile", "XML DOM Error" )
	    << "XML DOM error parsing configuration file '" << filename
	    << "': " << message << ENDLOG;
	XMLString::release(&message);
	throw TMConfigException( TMConfigException::kXMLDOMError, err.c_str(), filename );
        }
    catch ( ... )
	{
	LOG( MLUCLog::kError, "TMConfig::ParseFile", "Unexpected Error" )
	    << "Unexpected error parsing configuration file." << ENDLOG;
	throw TMConfigException( TMConfigException::kUnknownError, "Unknown error", filename );
        }
    if ( !doc )
	{
	throw TMConfigException( TMConfigException::kUnknownError, "Unknown error", filename );
	}
    }

void TMConfig::GetTask( const char* filename, DOMDocument* doc, DOMNode*& taskNode, MLUCString& taskName )
    {
    // Search task tags
    XMLCh* tagname = XMLString::transcode( "Task" );
    if ( !tagname )
	{
	LOG( MLUCLog::kError, "TMConfig::GetTask", "Error transcoding tagname" )
	    << "Error transcoding tagname." << ENDLOG;
	throw TMConfigException( TMConfigException::kUnknownError, "Error transcoding tagname" );
	}
    DOMNodeList *taskDOMList = doc->getElementsByTagName( tagname );
    XMLString::release( &tagname );
    if ( !taskDOMList )
	{
	LOG( MLUCLog::kError, "TMConfig::GetTask", "Error getting task" )
	    << "Error getting task from parsed XML file '" 
	    << filename << "'." << ENDLOG;
	throw TMConfigException( TMConfigException::kConfigurationError, "Error getting task from parsed XML file", filename );
	}
    if ( taskDOMList->getLength()!=1 )
	{
	LOG( MLUCLog::kError, "TMConfig::GetTask", "Wrong number of tasks" )
	    << "Wrong number of task elements (" << MLUCLog::kDec
	    << taskDOMList->getLength() << ") in parsed XML file '" 
	    << filename << "' Need exactly one 'Task' tag!" << ENDLOG;
	throw TMConfigException( TMConfigException::kConfigurationError, "Wrong number of task tags in parsed XML file", filename );
	}
    taskNode = taskDOMList->item( 0 );
    if ( !taskNode )
	{
	LOG( MLUCLog::kError, "TMConfig::GetTask", "Error getting task" )
	    << "Error getting task node from parsed XML file '" 
	    << filename << "'." << ENDLOG;
	throw TMConfigException( TMConfigException::kConfigurationError, "Error getting task node from parsed XML file", filename );
	}
    taskNode->normalize();

    // Parse attribute(s), Exactly one "name" attribute is required and allowed.
    GetAttribute( filename, taskNode, "name", taskName );

    LOG( MLUCLog::kDebug, "TMConfig::GetTask", "Task found" )
	<< "Task '" << taskName.c_str() << "' found." << ENDLOG;
    }


void TMConfig::GetInterfaceLibrary( const char* filename, DOMNode* taskNode, InterfaceLibraryData& lib, bool topLevelLib )
    {
    DOMNode* childNode = NULL;
    MLUCString platform;
    MLUCString binary;    
  
    if ( topLevelLib )
	{
	CheckChildNodeCnt( filename, taskNode, "InterfaceLibrary", 1, "interface library", "interface library node" );
	GetChildNode( filename, taskNode, "InterfaceLibrary", childNode );
	}
    else
	{
	CheckChildNodeCnt( filename, taskNode, "InterfaceLibrary", 0, 1, "interface library", "interface library node" );
        GetChildNode( filename, taskNode, "InterfaceLibrary", childNode, true );
	if ( !childNode )
	    {
	    lib.fActive = false;
	    return;
	    }
	}

    
    // Parse attribute(s), A "platform" attribute is required and allowed.
    GetAttribute( filename, childNode, "platform", platform );

    // Get binary name
    GetTextContent( childNode, binary );
    //     tmp = XMLString::transcode( childNode->getTextContent() );
    //     MLUCString binary( tmp );
    //     XMLString::release( &tmp );

    lib.fPlatform = platform;
    lib.fBinary = binary;
    lib.fActive = true;
	    
    if ( topLevelLib )
	{
	LOG( MLUCLog::kDebug, "TMConfig::GetInterfaceLibrary", "Interface library found" )
	    << "Interface library found. Platform: '" << platform.c_str() << "' - Binary: '"
	    << binary.c_str() << "'." << ENDLOG;
	}
    }

void TMConfig::GetStateSupervisionConfig( const char* filename, DOMNode* taskNode, StateSupervisionConfigData& conf )
    {
    DOMNode* configNode = NULL;
    DOMNode* actionNode;
    MLUCString pollInterval;
    MLUCString flag;

    // get the state supervision child node of the top-level task node
    CheckChildNodeCnt( filename, taskNode, "StateSupervisionConfig", 1, "state supervision configuration", "state supervision configuration node" );
    GetChildNode( filename, taskNode, "StateSupervisionConfig", configNode );

    // Get the poll interval for the main loop, if present
    if ( GetAttribute( filename, configNode, "interval", pollInterval, true ) )
	{
	char* cpErr;
	conf.fPollInterval = strtoul( pollInterval.c_str(), &cpErr, 0 );
	conf.fPollIntervalSet = true;
	if ( *cpErr )
	    {
	    MLUCString err;
	    LOG( MLUCLog::kError, "TMConfig::GetStateSupervisionConfig", "Error converting main loop poll interval" )
		<< "Error converting interval specifier '" << pollInterval.c_str() 
		<< "' for main system loop in StateSupervisionConfig node in parsed XML file '" 
		<< filename << "' to numeric value as required." << ENDLOG;
	    err = "Error converting StateSupervisionConfig interval specifier in parsed XML file";
	    throw TMConfigException( TMConfigException::kConfigurationError, err.c_str(), filename );
	    }
	}
    else
	conf.fPollIntervalSet = false;

    if ( GetAttribute( filename, configNode, "failover_active", flag, true ) )
	{
	flag.ToLower();
	if ( flag == "true" || flag == "yes" || flag == "1" )
	    conf.fFailOverActive = true;
	else if ( flag == "false" || flag == "no" || flag == "0" )
	    conf.fFailOverActive = false;
	else
	    {
	    char* cpErr;
	    conf.fFailOverActive = (bool)strtoul( flag.c_str(), &cpErr, 0 );
	    if ( *cpErr )
		{
		MLUCString err;
		LOG( MLUCLog::kError, "TMConfig::GetStateSupervisionConfig", "Error converting failover active/passive flag" )
		    << "Error converting failover active/passive flag '" << flag.c_str() 
		    << "' in StateSupervisionConfig node in parsed XML file '" 
		    << filename << "' to boolean value as required." << ENDLOG;
		err = "Error converting StateSupervisionConfig failover active/passive flag specifier in parsed XML file";
		throw TMConfigException( TMConfigException::kConfigurationError, err.c_str(), filename );
		}
	    }
	}
    else
	conf.fFailOverActive = true; // If not configured assume single TM scenario (no-failover)

    // Check that only one each of the various action child nodes is present
    CheckChildNodeCnt( filename, configNode, "InterruptListenAddress", 1, "interrupt listen address", "state supervision configuration node" );
    CheckChildNodeCnt( filename, configNode, "StartupAction", 0, 1, "startup action", "state supervision configuration node" );
    CheckChildNodeCnt( filename, configNode, "StateChangeAction", 0, 1, "state change action", "state supervision configuration node" );
    CheckChildNodeCnt( filename, configNode, "InterruptReceivedAction", 0, 1, "interrupt received action", "state supervision configuration node" );
    CheckChildNodeCnt( filename, configNode, "PeriodicPollAction", 0, 1, "periodic poll action", "state supervision configuration node" );
    CheckChildNodeCnt( filename, configNode, "LoopAction", 0, 1, "loop action", "state supervision configuration node" );
    CheckChildNodeCnt( filename, configNode, "LoopStopAction", 0, 1, "loop stop action", "state supervision configuration node" );
    // PreConfigChangeAction only for backward compatibility
    CheckChildNodeCnt( filename, configNode, "PreConfigChangeAction", 0, 1, "pre-configuration change action", "state supervision configuration node" );
    CheckChildNodeCnt( filename, configNode, "PreConfigChangePhase1Action", 0, 1, "pre-configuration change phase 1 action", "state supervision configuration node" );
    CheckChildNodeCnt( filename, configNode, "PreConfigChangePhase2Action", 0, 1, "pre-configuration change phase 2 action", "state supervision configuration node" );
    CheckChildNodeCnt( filename, configNode, "PreConfigChangePhase3Action", 0, 1, "pre-configuration change phase 3 action", "state supervision configuration node" );
    CheckChildNodeCnt( filename, configNode, "PostConfigChangeAction", 0, 1, "post-configuration change action", "state supervision configuration node" );

    // Get the interrupt listen address
    actionNode = NULL;
    GetChildNode( filename, configNode, "InterruptListenAddress", actionNode );
    GetTextContent( actionNode, conf.fInterruptListenAddress );

    // Get the various action nodes and their content
    actionNode = NULL;
    GetChildNode( filename, configNode, "StartupAction", actionNode, true );
    if ( actionNode )
	GetTextContent( actionNode, conf.fStartupAction );
    
    actionNode = NULL;
    GetChildNode( filename, configNode, "StateChangeAction", actionNode, true );
    if ( actionNode )
	GetTextContent( actionNode, conf.fStateChangeAction );

    actionNode = NULL;
    GetChildNode( filename, configNode, "InterruptReceivedAction", actionNode, true );
    if ( actionNode )
	GetTextContent( actionNode, conf.fInterruptReceivedAction );

    actionNode = NULL;
    GetChildNode( filename, configNode, "PeriodicPollAction", actionNode, true );
    if ( actionNode )
	{
	GetTextContent( actionNode, conf.fPeriodicPollAction );
	// Here we need the poll interval in addition.
	GetAttribute( filename, actionNode, "interval", pollInterval );
	char* cpErr;
	conf.fPeriodicPollInterval = strtoul( pollInterval.c_str(), &cpErr, 0 );
	if ( *cpErr )
	    {
	    MLUCString err;
	    LOG( MLUCLog::kError, "TMConfig::GetStateSupervisionConfig", "Error converting poll interval" )
		<< "Error converting interval specifier '" << pollInterval.c_str() 
		<< "' of PeriodicPollAction node in parsed XML file '" 
		<< filename << "' to numeric value as required." << ENDLOG;
	    err = "Error converting PeriodicPollAction interval specifier in parsed XML file";
	    throw TMConfigException( TMConfigException::kConfigurationError, err.c_str(), filename );
	    }
	}

    actionNode = NULL;
    GetChildNode( filename, configNode, "LoopAction", actionNode, true );
    if ( actionNode )
	GetTextContent( actionNode, conf.fLoopAction );

    actionNode = NULL;
    GetChildNode( filename, configNode, "LoopStopAction", actionNode, true );
    if ( actionNode )
	GetTextContent( actionNode, conf.fLoopStopAction );

    bool preConfigChangeActionFound=false;
    actionNode = NULL;
    GetChildNode( filename, configNode, "PreConfigChangeAction", actionNode, true );
    if ( actionNode )
	{
	preConfigChangeActionFound=true;
	LOG( MLUCLog::kWarning, "TMConfig::GetStateSupervisionConfig", "Global PreConfigChangeAction is deprecated" )
	    << "PreConfigChangeAction is deprecated. Please use PreConfigChangePhase3Action instead." << ENDLOG;
	GetTextContent( actionNode, conf.fPreConfigChangePhase3Action );
	}

    actionNode = NULL;
    GetChildNode( filename, configNode, "PreConfigChangePhase1Action", actionNode, true );
    if ( actionNode )
	{
	if ( preConfigChangeActionFound )
	    {
	    MLUCString err;
	    LOG( MLUCLog::kError, "TMConfig::GetStateSupervisionConfig", "Global PreConfigChangeAction and PreConfigChangePhase1Action not allowed together" )
		<< "The actions PreConfigChangeAction and  PreConfigChangePhase1Action are not allowed in a configuration together." << ENDLOG;
	    err = "PreConfigChangeAction and  PreConfigChangePhase1Action not allowed together";
	    throw TMConfigException( TMConfigException::kConfigurationError, err.c_str(), filename );
	    }
	GetTextContent( actionNode, conf.fPreConfigChangePhase1Action );
	}

    actionNode = NULL;
    GetChildNode( filename, configNode, "PreConfigChangePhase2Action", actionNode, true );
    if ( actionNode )
	{
	if ( preConfigChangeActionFound )
	    {
	    MLUCString err;
	    LOG( MLUCLog::kError, "TMConfig::GetStateSupervisionConfig", "Global PreConfigChangeAction and PreConfigChangePhase2Action not allowed together" )
		<< "The actions PreConfigChangeAction and  PreConfigChangePhase2Action are not allowed in a configuration together." << ENDLOG;
	    err = "PreConfigChangeAction and  PreConfigChangePhase2Action not allowed together";
	    throw TMConfigException( TMConfigException::kConfigurationError, err.c_str(), filename );
	    }
	GetTextContent( actionNode, conf.fPreConfigChangePhase2Action );
	}

    actionNode = NULL;
    GetChildNode( filename, configNode, "PreConfigChangePhase3Action", actionNode, true );
    if ( actionNode )
	{
	if ( preConfigChangeActionFound )
	    {
	    MLUCString err;
	    LOG( MLUCLog::kError, "TMConfig::GetStateSupervisionConfig", "Global PreConfigChangeAction and PreConfigChangePhase3Action not allowed together" )
		<< "The actions PreConfigChangeAction and  PreConfigChangePhase3Action are not allowed in a configuration together." << ENDLOG;
	    err = "PreConfigChangeAction and  PreConfigChangePhase3Action not allowed together";
	    throw TMConfigException( TMConfigException::kConfigurationError, err.c_str(), filename );
	    }
	GetTextContent( actionNode, conf.fPreConfigChangePhase3Action );
	}

    actionNode = NULL;
    GetChildNode( filename, configNode, "PostConfigChangeAction", actionNode, true );
    if ( actionNode )
	GetTextContent( actionNode, conf.fPostConfigChangeAction );

    LOG( MLUCLog::kDebug, "TMConfig::GetStateSupervisionConfig", "State Supervsion Configuration read" )
	<< "State supervsion configuration read: StartupAction: '" << conf.fStartupAction.c_str() 
	<< "' - StateChangeAction: '" << conf.fStateChangeAction.c_str() << "' - InterruptReceivedAction: '" 
	<< conf.fInterruptReceivedAction.c_str() << "' - PeriodicPollAction: '" 
	<< conf.fPeriodicPollAction.c_str() << "' - periodic poll interval: "
	<< MLUCLog::kDec << conf.fPeriodicPollInterval << " microseconds - LoopAction: '" << conf.fLoopAction.c_str() 
	<< "' - PreConfigChangePhase1Action: '" << conf.fPreConfigChangePhase1Action.c_str() 
	<< "' - PreConfigChangePhase2Action: '" << conf.fPreConfigChangePhase2Action.c_str() 
	<< "' - PreConfigChangePhase3Action: '" << conf.fPreConfigChangePhase3Action.c_str() 
	<< "' - PostConfigChangeAction: '" << conf.fPostConfigChangeAction.c_str() << "' - InterruptListenAddress: '"
	<< conf.fInterruptListenAddress.c_str() << "'." << ENDLOG;
    }

void TMConfig::GetProcesses( const char* filename, DOMNode* taskNode, vector<ProcessData>& processes, bool allowNoProcesses )
    {
    DOMNode* processNode = NULL;
    DOMNode* childNode = NULL;
    DOMNode* resourceNode = NULL;
    DOMNode* statesNode = NULL;
    DOMNode* stateNode = NULL;

    if ( GetChildNodeCnt( filename, taskNode, "Process" )<=0 )
	{
	if ( allowNoProcesses )
	    return;
	LOG( MLUCLog::kError, "TMConfig::GetProcesses", "No process specified" )
	    << "No child Process node specified in parsed XML file '" 
	    << filename << "'." << ENDLOG;
	throw TMConfigException( TMConfigException::kConfigurationError, "No child Process node specified in parsed XML file", filename );
	}

    GetChildNode( filename, taskNode, "Process", processNode );
    while ( processNode )
	{
	ProcessData data;
	
	GetAttribute( filename, processNode, "ID", data.fID );
	MLUCString type;
	if ( GetAttribute( filename, processNode, "type", type, true ) )
	    {
	    if ( type=="program" )
		{
		data.fType = ProcessData::kProgram;
		}
	    else if ( type=="slave" )
		{
		data.fType = ProcessData::kSlave;
		}
	    else if ( type=="slaveprogram" )
		{
		data.fType = ProcessData::kSlaveProgram;
		}
	    else if ( type=="proxy" )
		{
		data.fType = ProcessData::kProxy;
		}
	    else
		{
		LOG( MLUCLog::kError, "TMConfig::GetProcesses", "Unknown type attribute value" )
		    << "Unknown 'type' attribute value '" << type.c_str() 
		    << "' specified. Only 'program' or 'slave' are currently supported."
		    << ENDLOG;
		MLUCString err;
		err = "Unknown 'type' attribute value '";
		err += type;
		err += "' specified";
		throw TMConfigException( TMConfigException::kConfigurationError, err.c_str(), filename );
		}
	    }
	else
	    data.fType = ProcessData::kProgram;

	CheckChildNodeCnt( filename, processNode, "Address", 1, "address", "process node" );
	if ( data.fType==ProcessData::kProgram || data.fType==ProcessData::kSlaveProgram )
	    {
	    CheckChildNodeCnt( filename, processNode, "Command", 1, "command", "process node" );
	    }
	CheckChildNodeCnt( filename, processNode, "StateChangeAction", 0, 1, "state change action", "process node" );
	CheckChildNodeCnt( filename, processNode, "PreStartAction", 0, 1, "pre-start action", "process node" );
	CheckChildNodeCnt( filename, processNode, "PostStartAction", 0, 1, "post-start action", "process node" );
	CheckChildNodeCnt( filename, processNode, "PreTerminateAction", 0, 1, "pre-terminate action", "process node" );
	CheckChildNodeCnt( filename, processNode, "PostTerminateAction", 0, 1, "post-terminate action", "process node" );
	CheckChildNodeCnt( filename, processNode, "InterruptReceivedAction", 0, 1, "interrupt received action", "process node" );
	CheckChildNodeCnt( filename, processNode, "PreConfigChangeAction", 0, 1, "pre-configuration change action", "process node" );
	CheckChildNodeCnt( filename, processNode, "PreConfigChangePhase1Action", 0, 1, "pre-configuration change phase 1 action", "process node" );
	CheckChildNodeCnt( filename, processNode, "PreConfigChangePhase2Action", 0, 1, "pre-configuration change phase 2 action", "process node" );
	CheckChildNodeCnt( filename, processNode, "PreConfigChangePhase3Action", 0, 1, "pre-configuration change phase 3 action", "process node" );
	CheckChildNodeCnt( filename, processNode, "PostConfigChangeAction", 0, 1, "post-configuration change action", "process node" );
	CheckChildNodeCnt( filename, processNode, "UsedResources", 0, 1, "used resources", "process node" );

	childNode = NULL;
	GetChildNode( filename, processNode, "Address", childNode );
	GetTextContent( childNode, data.fAddress );

	if ( data.fType == ProcessData::kProgram || data.fType==ProcessData::kSlaveProgram )
	    {
	    childNode = NULL;
	    GetChildNode( filename, processNode, "Command", childNode );
	    GetTextContent( childNode, data.fCommand );
	    }

	childNode = NULL;
	GetChildNode( filename, processNode, "StateChangeAction", childNode, true );
	if ( childNode )
	    GetTextContent( childNode, data.fStateChangeAction );

	childNode = NULL;
	GetChildNode( filename, processNode, "PreStartAction", childNode, true );
	if ( childNode )
	    GetTextContent( childNode, data.fPreStartAction );

	childNode = NULL;
	GetChildNode( filename, processNode, "PostStartAction", childNode, true );
	if ( childNode )
	    GetTextContent( childNode, data.fPostStartAction );

	childNode = NULL;
	GetChildNode( filename, processNode, "PreTerminateAction", childNode, true );
	if ( childNode )
	    GetTextContent( childNode, data.fPreTerminateAction );

	childNode = NULL;
	GetChildNode( filename, processNode, "PostTerminateAction", childNode, true );
	if ( childNode )
	    GetTextContent( childNode, data.fPostTerminateAction );

	childNode = NULL;
	GetChildNode( filename, processNode, "InterruptReceivedAction", childNode, true );
	if ( childNode )
	    GetTextContent( childNode, data.fInterruptReceivedAction );

	bool preConfigChangeActionFound=false;
	childNode = NULL;
	GetChildNode( filename, processNode, "PreConfigChangeAction", childNode, true );
	if ( childNode )
	    {
	    preConfigChangeActionFound=true;
	    LOG( MLUCLog::kWarning, "TMConfig::GetProcesses", "PreConfigChangeAction for processes is deprecated" )
		<< "PreConfigChangeAction is deprecated for processes. Please use PreConfigChangePhase3Action instead." << ENDLOG;
	    GetTextContent( childNode, data.fPreConfigChangePhase3Action );
	    }
	
	childNode = NULL;
	GetChildNode( filename, processNode, "PreConfigChangePhase1Action", childNode, true );
	if ( childNode )
	    {
	    if ( preConfigChangeActionFound )
		{
		MLUCString err;
		LOG( MLUCLog::kError, "TMConfig::GetProcesses", "PreConfigChangeAction and PreConfigChangePhase1Action not allowed together for processes" )
		    << "The actions PreConfigChangeAction and  PreConfigChangePhase1Action are not allowed in a process configuration together." << ENDLOG;
		err = "PreConfigChangeAction and  PreConfigChangePhase1Action not allowed together for processes";
		throw TMConfigException( TMConfigException::kConfigurationError, err.c_str(), filename );
		}
	    GetTextContent( childNode, data.fPreConfigChangePhase1Action );
	    }

	childNode = NULL;
	GetChildNode( filename, processNode, "PreConfigChangePhase2Action", childNode, true );
	if ( childNode )
	    {
	    if ( preConfigChangeActionFound )
		{
		MLUCString err;
		LOG( MLUCLog::kError, "TMConfig::GetProcesses", "PreConfigChangeAction and PreConfigChangePhase2Action not allowed together for processes" )
		    << "The actions PreConfigChangeAction and  PreConfigChangePhase2Action are not allowed in a process configuration together." << ENDLOG;
		err = "PreConfigChangeAction and  PreConfigChangePhase2Action not allowed together for processes";
		throw TMConfigException( TMConfigException::kConfigurationError, err.c_str(), filename );
		}
	    GetTextContent( childNode, data.fPreConfigChangePhase2Action );
	    }

	childNode = NULL;
	GetChildNode( filename, processNode, "PreConfigChangePhase3Action", childNode, true );
	if ( childNode )
	    {
	    if ( preConfigChangeActionFound )
		{
		MLUCString err;
		LOG( MLUCLog::kError, "TMConfig::GetProcesses", "PreConfigChangeAction and PreConfigChangePhase3Action not allowed together for processes" )
		    << "The actions PreConfigChangeAction and  PreConfigChangePhase3Action are not allowed in a process configuration together." << ENDLOG;
		err = "PreConfigChangeAction and  PreConfigChangePhase3Action not allowed together for processes";
		throw TMConfigException( TMConfigException::kConfigurationError, err.c_str(), filename );
		}
	    GetTextContent( childNode, data.fPreConfigChangePhase3Action );
	    }

	childNode = NULL;
	GetChildNode( filename, processNode, "PostConfigChangeAction", childNode, true );
	if ( childNode )
	    GetTextContent( childNode, data.fPostConfigChangeAction );

	LOG( MLUCLog::kDebug, "TMConfig::GetProcesses", "Process found" )
	    << "Process '" << data.fID.c_str() << "' found"
	    << " - Address: '" << data.fAddress.c_str()
	    << "' - Command: '" << data.fCommand.c_str()
	    << "' - StateChangeAction: '" << data.fStateChangeAction.c_str()
	    << "' - PreStartAction: '" << data.fPreStartAction.c_str()
	    << "' - PostStartAction: '" << data.fPostStartAction.c_str()
	    << "' - PreTerminateAction: '" << data.fPreTerminateAction.c_str()
	    << "' - PostTerminateAction: '" << data.fPostTerminateAction.c_str()
	    << "' - InterruptReceivedAction: '" << data.fInterruptReceivedAction.c_str()
	    << "' - PreConfigChangePhase1Action: '" << data.fPreConfigChangePhase1Action.c_str()
	    << "' - PreConfigChangePhase2Action: '" << data.fPreConfigChangePhase2Action.c_str()
	    << "' - PreConfigChangePhase3Action: '" << data.fPreConfigChangePhase3Action.c_str()
	    << "' - PostConfigChangeAction: '" << data.fPostConfigChangeAction.c_str()
	    << "'." << ENDLOG;


	GetInterfaceLibrary( filename, processNode, data.fInterfaceLibrary, false );

	// Get the interrupt listen address, if any is given
	DOMNode* addressNode = NULL;
	GetChildNode( filename, processNode, "InterruptListenAddress", addressNode, true );
	if ( addressNode )
	    {
	    GetTextContent( addressNode, data.fInterruptListenAddress );
	    data.fInterruptListenAddressActive = true;
	    }
	else
	    data.fInterruptListenAddressActive = false;

	childNode = NULL;
	GetChildNode( filename, processNode, "FailOverPassiveKillAction", childNode, true );
	if ( childNode )
	    GetTextContent( childNode, data.fFailOverPassiveKillAction );
	else
	    data.fFailOverPassiveKillAction = "";

	childNode = NULL;
	GetChildNode( filename, processNode, "FailOverPassiveMonitorCommand", childNode, true );
	if ( childNode )
	    GetTextContent( childNode, data.fFailOverPassiveMonitorCommand );
	else
	    data.fFailOverPassiveMonitorCommand = "";
	    
	childNode = NULL;
	GetChildNode( filename, processNode, "UsedResources", childNode, true );
	if ( childNode )
	    {
	    CheckChildNodeCnt( filename, childNode, "CleanupAction", 0, 1, "resource cleanup action", "used resources" );
	    resourceNode = NULL;
	    GetChildNode( filename, childNode, NULL, resourceNode, true );
	    while ( resourceNode )
		{
		UsedResourceData resData;
		GetNodeName( resourceNode, resData.fType );
		GetTextContent( resourceNode, resData.fContent );
		if ( resData.fType == "#text" )
		    {
		    GetChildNode( filename, childNode, NULL, resourceNode, true );
		    continue;
		    }
		if ( resData.fType!="File" && resData.fType!="SysVShm" && 
		     resData.fType!="GenericCleanup" && resData.fType!="CleanupAction" )
		    {
		    LOG( MLUCLog::kError, "TMConfig::GetProcesses", "Incorrect resource specified" )
			<< "Unknown used resource ('" << resData.fType.c_str()
			<< "') specified for process '" << data.fID.c_str() 
			<< "' node in parsed XML file '" << filename << "'." << ENDLOG;
		    throw TMConfigException( TMConfigException::kConfigurationError, "Incorrect process resource in parsed XML file", filename );
		    }
		LOG( MLUCLog::kDebug, "TMConfig::GetProcesses", "Process resource found" )
		    << "Process '" << data.fID.c_str() << "' resource '"
		    << resData.fType.c_str() << "' found: '" << resData.fContent.c_str()
		    << "'." << ENDLOG;
		if ( resData.fType == "CleanupAction" )
		    data.fCleanupAction = resData.fContent;
		else
		    data.fUsedResources.insert( data.fUsedResources.end(), resData );
		GetChildNode( filename, childNode, NULL, resourceNode, true );
		}
	    }

	statesNode=NULL;
	GetChildNode( filename, processNode, "States", statesNode, true );
	if ( statesNode )
	    {
	    MLUCString tmp;
	    stateNode = NULL;
	    GetChildNode( filename, statesNode, "State", stateNode, true );
	    while ( stateNode )
		{
		ProcessStateData sd;
		GetAttribute( filename, stateNode, "name", sd.fStateNamePattern );
		childNode = NULL;
		GetChildNode( filename, stateNode, "Command", childNode, true );
		while ( childNode )
		    {
		    GetAttribute( filename, childNode, "name", tmp );
		    sd.fAllowedCommands.insert( sd.fAllowedCommands.end(), tmp );
		    GetChildNode( filename, stateNode, "Command", childNode, true );
		    }
		data.fStatesData.push_back( sd );
		GetChildNode( filename, statesNode, "State", stateNode, true );
		}
	    }

	processes.insert( processes.end(), data );
	GetChildNode( filename, taskNode, "Process", processNode );
	}
    }

void TMConfig::GetSlaveControlConfig( const char* filename, DOMNode* taskNode, SlaveControlConfigData& conf )
    {
    DOMNode* configNode = NULL;
    DOMNode* stateNode = NULL;
    DOMNode* statesNode = NULL;
    DOMNode* childNode = NULL;
    MLUCString tmpStr;
    bool found;

    if ( GetChildNodeCnt( filename, taskNode, "SlaveControlConfig" )<=0 )
	{
	conf.fActive = false;
	return;
	}
    conf.fActive = true;
    conf.fAutonomous = false;
    
    GetChildNode( filename, taskNode, "SlaveControlConfig", configNode );

    tmpStr = "";
    found = GetAttribute( filename, configNode, "Autonomous", tmpStr, true );
    if ( found && tmpStr!="true" && tmpStr!="false" )
	{
	MLUCString err;
	LOG( MLUCLog::kError, "TMConfig::GetSlaveControlConfig", "Invalid value for Autonomous attribute" )
	    << "Invalid value '" << tmpStr.c_str() << "' for 'Autonomous' attribute in 'SlaveControlConfig' node."
	    << " Only 'true' and 'false' are allowed if the attribute is present." << ENDLOG;
	err = "Invalid value '";
	err += tmpStr;
	err += "' for 'Autonomous' attribute in 'SlaveControlConfig' node. Only 'true' and 'false' are allowed if the attribute is present.";
	throw TMConfigException( TMConfigException::kConfigurationError, err.c_str(), filename );
	}
    if ( tmpStr=="true" )
	conf.fAutonomous = true;
    else
	conf.fAutonomous = false;

    LOG( MLUCLog::kDebug, "TMConfig::GetSlaveControlConfig", "Slave control found" )
	<< "Found slave control configuration. Autonomous: " << (conf.fAutonomous ? "yes" : "no")
	<< "." << ENDLOG;

    CheckChildNodeCnt( filename, configNode, "Address", 1, "slave control address", "slave control address node" );
    CheckChildNodeCnt( filename, configNode, "CommandReceivedAction", 0, 1, "slave control command received action", "slave control command received action node" );

    GetChildNode( filename, configNode, "Address", childNode );
    GetTextContent( childNode, conf.fAddress );
//     printf( "Slave control address: %s\n", conf.fAddress.c_str() );
    
    childNode = NULL;
    GetChildNode( filename, configNode, "CommandReceivedAction", childNode, true );
    if ( childNode )
	GetTextContent( childNode, conf.fCommandReceivedAction );
//     printf( "Slave control action: %s\n", conf.fCommandReceivedAction.c_str() );

    MLUCString tmp;

    childNode = NULL;
    GetChildNode( filename, configNode, "InterruptTargetAddress", childNode, true );
    while ( childNode )
	{
	GetTextContent( childNode, tmp );
	conf.fInterruptTargetAddresses.insert( conf.fInterruptTargetAddresses.begin(), tmp );
	GetChildNode( filename, configNode, "InterruptTargetAddress", childNode, true );
	}

    GetChildNode( filename, configNode, "States", statesNode, true );
    if ( statesNode )
	{
	GetChildNode( filename, statesNode, "State", stateNode, true );
	while ( stateNode )
	    {
	    StateData sd;
	    GetAttribute( filename, stateNode, "name", sd.fStateName );
	    childNode = NULL;
	    GetChildNode( filename, stateNode, "Command", childNode, true );
	    while ( childNode )
		{
		GetAttribute( filename, childNode, "name", tmp );
		sd.fAllowedCommands.insert( sd.fAllowedCommands.end(), tmp );
		GetChildNode( filename, stateNode, "Command", childNode, true );
		}
	    conf.fStatesData.insert( conf.fStatesData.end(), sd );
	    GetChildNode( filename, statesNode, "State", stateNode, true );
	    }
	}
    LOG( MLUCLog::kDebug, "TMConfig::GetSlaveControlConfig", "slave Control Configuration read" )
	<< "Slave control configuration read: Autonomous: '" << ( conf.fAutonomous ? "Yes" : "No" )
	<< "' - Address: '" << conf.fAddress.c_str() << "' - CommandReceivedAction: '" 
	<< conf.fCommandReceivedAction.c_str() << "'." << ENDLOG;
    }

void TMConfig::GetPythonExtensions( const char* filename, DOMNode* taskNode, vector<PythonExtensionData>& extensions )
    {
    DOMNode* extensionNode = NULL;

    GetChildNode( filename, taskNode, "PythonExtension", extensionNode, true );
    while ( extensionNode )
	{
	PythonExtensionData extension;
	bool found;

	extension.fPlatform = "";
	found = GetAttribute( filename, extensionNode, "platform", extension.fPlatform );
	found = GetAttribute( filename, extensionNode, "module", extension.fModule );
	GetTextContent( extensionNode, extension.fBinary );
	extension.fActive = true;
	extensions.push_back( extension );
	
	GetChildNode( filename, taskNode, "PythonExtension", extensionNode, true );
	}
    }


bool TMConfig::GetAttribute( const char* filename, DOMNode* node, const char* attrName, MLUCString& attrValue, bool canFail )
    {
    unsigned m;
    char* dummy = XMLString::transcode( node->getNodeName() );
    MLUCString nodename( dummy );
    XMLString::release( &dummy );

    DOMNamedNodeMap *attrList = node->getAttributes();
    if ( !attrList )
	{
	MLUCString err;
	LOG( MLUCLog::kError, "TMConfig::GetAttribute", "Error obtaining attribute list" )
	    << "Error obtaining attribute list for node '" << nodename.c_str() << "' from parsed XML file '" 
	    << filename << "'." << ENDLOG;
	err = "Error obtaining attribute list for '";
	err += nodename;
	err += "'node in parsed XML file";
	throw TMConfigException( TMConfigException::kConfigurationError, err.c_str(), filename );
	}
    for ( m = 0; m < attrList->getLength(); m++ )
	{
	DOMAttr *attr = (DOMAttr*) attrList->item( m );
	if ( !attr )
	    {
	    MLUCString err;
	    LOG( MLUCLog::kError, "TMConfig::GetAttribute", "Error obtaining attribute" )
		<< "Error obtaining attribute for '" << nodename.c_str()
		<< "' node in parsed XML file '" << filename << "'." << ENDLOG;
	    err = "Error obtaining attribute for '";
	    err += nodename;
	    err += "' node in parsed XML file";
	    throw TMConfigException( TMConfigException::kConfigurationError, err.c_str(), filename );
	    }
	
	// get attribute type
	dummy = XMLString::transcode( attr->getName() );
	MLUCString thisAttrName( dummy );
	XMLString::release( &dummy );

	// Get attribute value
	dummy = XMLString::transcode( attr->getValue() );
	
	if ( thisAttrName==attrName )
	    {
	    attrValue = dummy;
	    XMLString::release( &dummy );
	    return true;
	    }
	XMLString::release( &dummy );
	}
    
    if ( !canFail )
	{
	LOG( MLUCLog::kError, "TMConfig::GetAttribute", "Attribute not found" )
	    << "'" << attrName << "' attribute not found for '"
	    << nodename.c_str() << "' node in parsed XML file '" 
	    << filename << "'." << ENDLOG;
	MLUCString err;
	err = "'";
	err += attrName;
	err += "' attribute not found for '";
	err += nodename;
	err += "' node in parsed XML file";
	throw TMConfigException( TMConfigException::kConfigurationError, err.c_str(), filename );
	}
    return false;
    }

void TMConfig::GetTextContent( DOMNode* node, MLUCString& content )
    {
    char* tmp;
    tmp = XMLString::transcode( node->getTextContent() );
    content = tmp;
    XMLString::release( &tmp );
    }

void TMConfig::GetNodeName( DOMNode* node, MLUCString& name )
    {
    char* tmp;
    tmp = XMLString::transcode( node->getNodeName() );
    name = tmp;
    XMLString::release( &tmp );
    }

void TMConfig::GetChildNode( const char* filename, DOMNode* node, const char* childNodeName, DOMNode*& child, bool allowFail )
    {
    DOMNodeList *children = node->getChildNodes();
    DOMNode* childNode;
    unsigned int n;
    char *tmp;
    tmp = XMLString::transcode( node->getNodeName() );
    MLUCString nodename( tmp );
    XMLString::release( &tmp );

    if ( !children )
	{
	LOG( MLUCLog::kError, "TMConfig::GetChildNode", "Error getting children" )
	    << "Error getting children for node '" << nodename.c_str() << "' from parsed XML file '" 
	    << filename << "'." << ENDLOG;
	MLUCString err;
	err = "Error getting children for node '";
	err += nodename;
	err += "' from parsed XML file";
	throw TMConfigException( TMConfigException::kConfigurationError, err.c_str(), filename );
	}

    // Inspect tags 
    bool lastFound = false;
    if ( !child )
	lastFound = true;
    for ( n = 0; n < children->getLength(); n++ )
	{
	childNode = children->item( n );
	if ( !childNode )
	    {
	    LOG( MLUCLog::kError, "TMConfig::GetChildNode", "Error getting children" )
		<< "Error getting child " << MLUCLog::kDec << n << " for node '" << nodename.c_str() << "' from parsed XML file '" 
		<< filename << "'." << ENDLOG;
	    MLUCString err;
	    err = "Error getting child for node '";
	    err += nodename;
	    err += "' from parsed XML file";
	    throw TMConfigException( TMConfigException::kConfigurationError, err.c_str(), filename );
	    }
	childNode->normalize();
        
        // Search interface library tag
        char* dummy = XMLString::transcode( childNode->getNodeName() );
        MLUCString tagname( dummy );
        XMLString::release( &dummy );
        
	//printf( "Tagname: %s\n", tagname.c_str() );
        if ( !childNodeName || tagname == childNodeName )
	    {
	    if ( lastFound )
		{
		child = childNode;
		return;
		}
	    if ( child == childNode )
		lastFound = true;
	    }
	}
    // No child node found
    if ( !child && !allowFail )
	{
	// No previous child present, therefore no matching child node is present
	LOG( MLUCLog::kError, "TMConfig::GetChildNode", "Child node not found" )
	    << (childNodeName ? "'" : "") << (childNodeName ? childNodeName : "") << (childNodeName ? "' child node not" : "No child node")
	    << " found in node '"
	    << nodename.c_str() << "' in parsed XML file '" 
	    << filename << "'." << ENDLOG;
	MLUCString err;
	if ( childNodeName )
	    {
	    err += "'";
	    err += childNodeName;
	    err += "' child node not";
	    }
	else
	    err += "No child node";
	err += " found in node '";
	err += nodename;
	err += "' in parsed XML file";
	throw TMConfigException( TMConfigException::kConfigurationError, err.c_str(), filename );
	}
    // the previous one was the last.
    child = NULL;
    }

unsigned long TMConfig::GetChildNodeCnt( const char* filename, DOMNode* node, const char* childNodeName )
    {
    unsigned long n;
    unsigned long cnt = 0;
    DOMNodeList *children = node->getChildNodes();
    DOMNode* childNode;
    char *tmp;
    tmp = XMLString::transcode( node->getNodeName() );
    MLUCString nodename( tmp );
    XMLString::release( &tmp );

    if ( !children )
	{
	LOG( MLUCLog::kError, "TMConfig::GetChildNode", "Error getting children" )
	    << "Error getting children for node '" << nodename.c_str() << "' from parsed XML file '" 
	    << filename << "'." << ENDLOG;
	MLUCString err;
	err = "Error getting children for node '";
	err += nodename;
	err += "' from parsed XML file";
	throw TMConfigException( TMConfigException::kConfigurationError, err.c_str(), filename );
	}

    // Inspect tags 
    for ( n = 0; n < children->getLength(); n++ )
	{
	childNode = children->item( n );
	childNode->normalize();
        
        // Search interface library tag
        char* dummy = XMLString::transcode( childNode->getNodeName() );
        MLUCString tagname( dummy );
        XMLString::release( &dummy );
        
	//printf( "Tagname: %s\n", tagname.c_str() );
        if ( tagname == childNodeName )
	    cnt++;
	}
    return cnt;
    }

void TMConfig::CheckChildNodeCnt( const char* filename, DOMNode* node, const char* childNodeName, 
				  unsigned long allowedCnt, const char* nodeSpec, const char* location )
    {
    CheckChildNodeCnt( filename, node, childNodeName, allowedCnt, allowedCnt, nodeSpec, location );
    }

void TMConfig::CheckChildNodeCnt( const char* filename, DOMNode* node, const char* childNodeName, unsigned long minAllowedCnt, unsigned long maxAllowedCnt, const char* nodeSpec, const char* location )
    {
    unsigned long cnt;
    if ( !nodeSpec )
	nodeSpec = childNodeName;

    cnt = GetChildNodeCnt( filename, node, childNodeName );
    if ( cnt<minAllowedCnt || cnt>maxAllowedCnt )
	{
	MLUCString err1;
	err1 = "Wrong number of ";
	err1 += nodeSpec;
	err1 += " child node tags";
	MLUCString err2;
	err2 = err1;
	err2 += " in parsed XML file";
	if ( minAllowedCnt==maxAllowedCnt )
	    {
	    LOG( MLUCLog::kError, "TMConfig::CheckChildNodeCnt", err1.c_str() )
		<< "Wrong number of " << nodeSpec << " child node tags (" << MLUCLog::kDec << cnt << ") "
		<< (location ? "in " : "") << (location ? location : "") << " in parsed XML file '" 
		<< filename << "'. " << minAllowedCnt << (minAllowedCnt==1 ? " node is" : " nodes are") << " allowed and required." << ENDLOG;
	    }
	else
	    {
	    LOG( MLUCLog::kError, "TMConfig::CheckChildNodeCnt", err1.c_str() )
		<< "Wrong number of " << nodeSpec << " child node tags (" << MLUCLog::kDec << cnt << ") "
		<< (location ? "in " : "") << (location ? location : "") << " in parsed XML file '" 
		<< filename << "'. Between " << minAllowedCnt << " and " << maxAllowedCnt 
		<< " nodes are allowed and required." << ENDLOG;
	    }
	throw TMConfigException( TMConfigException::kConfigurationError, err2.c_str(), filename );
	}
    }



void TMConfig::DumpTags( const char* filename, DOMDocument* doc )
    {
    // Search task tags
    XMLCh* tagname = XMLString::transcode( "*" );
    DOMNodeList *tagDOMList = doc->getElementsByTagName( tagname );
    XMLString::release( &tagname );
    if ( !tagDOMList )
	{
	LOG( MLUCLog::kError, "TMConfig::DumpTags", "Error getting elements" )
	    << "Error getting elements from parsed XML file '" 
	    << filename << "'." << ENDLOG;
	throw TMConfigException( TMConfigException::kConfigurationError, "Error getting elements from parsed XML file", filename );
	}
    LOG( MLUCLog::kDebug, "TMConfig::DumpTags", "Element number" )
	<< "Number of elements: " << MLUCLog::kDec
	<< tagDOMList->getLength() << "." << ENDLOG;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
