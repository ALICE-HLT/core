/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TMUsedResource.hpp"
#include "TMSysException.hpp"
#include <string.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>


void TMUsedResourceData::Cleanup()
    {
    int ret;
    switch ( fType )
	{
	case kNone:
	    break;
	case kFile:
	    if ( !unlink( fName.c_str() ) )
		throw TMSysException( errno, strerror(errno), "Cannot to remove file",
				      __FILE__, __LINE__ );
	    break;
	case kSysVShm:
	    {
	    int shmid;
	    char* cpErr;
	    shmid = (unsigned long)strtoul( fName.c_str(), &cpErr, 0);
	    if ( *cpErr!=0 )
		throw TMSysException( EINVAL, strerror(EINVAL), "Cannot convert shared memory id specifier",
				      __FILE__, __LINE__ );
	    ret = shmctl( shmid, IPC_RMID, NULL );
	    if ( ret )
		throw TMSysException( errno, strerror(errno), "Cannot remove shared memory",
				      __FILE__, __LINE__ );
	    }
	    break;
	case kGenericCleanup:
	    {
	    pid_t pid, xpid;
	    struct timeval st, ct;
	    unsigned long long dt;
	    pid = fork();
	    if ( pid )
		{
		if ( fUseTimeout )
		    {
		    gettimeofday( &st, NULL );
		    do
			{
			usleep ( 9000 );
			gettimeofday( &ct, NULL );
			dt = (ct.tv_sec - st.tv_sec);
			dt *= 1000000;
			dt += (ct.tv_usec - st.tv_usec);
			xpid = waitpid( pid, NULL, WNOHANG );
			}
		    while ( dt < fTimeout || !xpid );
		    }
		else
		    {
		    xpid = waitpid( pid, NULL, 0 );
		    }
		}
	    else
		{
		int ret = system( fName.c_str() );
		if ( ret )
		    {
		    // Make compiler happy...
		    }
		exit( 0 );
		}
	    }
	    break;
	}
    }



    /*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
