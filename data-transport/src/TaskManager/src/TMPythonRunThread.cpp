/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TMPythonInterface.hpp"
#include "TMPythonRunThread.hpp"
#include "TMSystem.hpp"
#include "TMSysException.hpp"
#include <errno.h>
#include <string.h>


TMPythonRunThread::TMPythonRunThread( TMSystem* system, const char* action )
    {
    fPython = NULL;
    fSystem = system;
    fLoop = false;
    fAction = action;
    fQuit = false;
    fRunning = false;
    pthread_mutex_init( &fActionMutex, NULL );
    }

TMPythonRunThread::TMPythonRunThread( TMSystem* system, const char* action, unsigned long interval )
    {
    fPython = NULL;
    fSystem = system;
    fLoop = true;
    if ( !interval )
	fInterval = 10000000;
    else
	fInterval = interval;
    fAction = action;
    fQuit = false;
    fRunning = false;
    pthread_mutex_init( &fActionMutex, NULL );
    }

TMPythonRunThread::~TMPythonRunThread()
    {
    pthread_mutex_destroy( &fActionMutex );
    }

void TMPythonRunThread::SetAction( const char* action )
    {
    pthread_mutex_lock( &fActionMutex );
    fAction = action;
    pthread_mutex_unlock( &fActionMutex );
    }

void TMPythonRunThread::Run()
    {
    fPython = new TMSubThreadPythonInterface( fSystem );
    if ( !fPython )
	throw TMSysException( ENOMEM, strerror(ENOMEM), "Unable to create sub-thread python interpreter object",
			      __FILE__, __LINE__ );
    fRunning = true;
    MLUCString action;
    pthread_cleanup_push( TMPythonRunThread::CleanupFuncWrapper, this );
    do
	{
	pthread_mutex_lock( &fActionMutex );
	action = fAction;
	pthread_mutex_unlock( &fActionMutex );
	if ( action!="" )
	    {
	    fPython->Run( action.c_str() );
	    if ( fLoop )
		{
		unsigned long interval = fInterval;
		const unsigned long step = 1000;
		do
		    {
		    if ( interval>step )
			{
			usleep( step );
			interval -= step;
			}
		    else
			{
			usleep( 0 );
			interval = 0;
			}
		    }
		while ( interval>0 && !fQuit );
		//usleep( fInterval );
		}
	    }
	else
	    {
	    unsigned long interval = 1000000;
	    const unsigned long step = 10000;
	    do
		{
		if ( interval>step )
		    {
		    usleep( step );
		    interval -= step;
		    }
		else
		    {
		    usleep( 0 );
		    interval = 0;
		    }
		}
	    while ( interval>0 && !fQuit );
	    //usleep( 1000000 );
	    }
	}
    while ( fLoop && !fQuit );
    pthread_cleanup_pop( 0 );
    delete fPython;
    fQuit = false;
    fRunning = false;
    }

void TMPythonRunThread::CleanupFuncWrapper( void* arg )
    {
    ((TMPythonRunThread*)arg)->CleanupFunc();
    }

void TMPythonRunThread::CleanupFunc()
    {
    fQuit = false;
    fRunning = false;
    pthread_mutex_unlock( &fActionMutex );
    fPython->Cleanup();
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
