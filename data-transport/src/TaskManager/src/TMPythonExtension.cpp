/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TMPythonExtension.hpp"
#include "TMSysException.hpp"
#include "MLUCLog.hpp"

TMPythonExtension::TMPythonExtension( const char* module, const char* platform, const char* binary ):
    fModule( module ),
    fPlatform( platform ),
    fBinary( binary )
    {
    fInitFunc = NULL;
    
    int ret;
    ret = fLibrary.Open( fBinary.c_str() );
    if ( ret )
	{
	MLUCString errStr;
	errStr = "Unable to open dynamic library object '";
	errStr += fBinary.c_str();
	errStr += "': ";
	errStr += fLibrary.GetLastError();
	LOG( MLUCLog::kError, "TMPythonExtension::TMPythonExtension", "Unable to open library" )
	    << errStr.c_str() << "/" << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	throw TMSysException( ret, strerror(ret), errStr.c_str(), __FILE__, __LINE__ );
	}
    MLUCString initFunc;
    initFunc = "init";
    initFunc += fModule;
    ret = GetDLSym<PythonModuleInitFunc_t>( fLibrary, initFunc.c_str(), fInitFunc );
    if ( ret )
	{
	MLUCString errStr;
	errStr = "Unable to get '";
	errStr += initFunc.c_str();
	errStr += "' from dynamic library '";
	errStr += fBinary.c_str();
	errStr += "': ";
	errStr += fLibrary.GetLastError();
	LOG( MLUCLog::kError, "TMPythonExtension::TMPythonExtension", "Unable to get library symbol" )
	    << errStr.c_str() << "/" << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	throw TMSysException( ret, strerror(ret), errStr.c_str(), __FILE__, __LINE__ );
	}
    }

void TMPythonExtension::Init()
    {
    if ( !fInitFunc )
	{
	MLUCString errStr;
	errStr = "Init function for python extension ";
	errStr += fModule.c_str();
	errStr += "/";
	errStr += fBinary.c_str();
	errStr += "/";
	errStr += fPlatform.c_str();
	errStr += " is not defined.";
	throw TMSysException( EINVAL, strerror(EINVAL), errStr.c_str(), __FILE__, __LINE__ );
	}
    (*fInitFunc)();
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
