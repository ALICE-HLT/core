/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TMInterfaceLibrary.hpp"
#include <MLUCLog.hpp>
#include <errno.h>

TMInterfaceLibrary::TMInterfaceLibrary( const char* archName, const char* libName, bool log )
    {
    fArchName = archName;
    fLibName = libName;
    fPrivateLibData = NULL;
    fInitialize = NULL;
    fTerminate = NULL;
    fQueryState = NULL;
    fQueryStatusData = NULL;
    fReleaseResource = NULL;
    fSendCommand = NULL;
    fInterruptWait = NULL;
    fStopInterruptWait = NULL;
    fConnectToProcess = NULL;
    fDisconnectFromProcess = NULL;
    fSuppressComErrorLogs = NULL;
    fLogging = log;
    fInterruptWaitRunning = false;
    int ret;

    ret = fLibrary.Open( fLibName.c_str() );
    if ( ret )
	{
	if ( log )
	    LOG( MLUCLog::kError, "TMInterfaceLibrary::Initialize", "Unable to open library" )
		<< "Unable to open dynamic library object '" << fLibName.c_str() << "': "
		<< strerror(ret) << " / " << fLibrary.GetLastError() << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kLibraryOpenError, 
					   fLibName.c_str(), ret, fLibrary.GetLastError() );
	}
    ret = GetDLSym<InitializeFunc>( fLibrary, "Initialize", fInitialize );
    if ( ret )
	{
	if ( log )
	    LOG( MLUCLog::kError, "TMInterfaceLibrary::Initialize", "Unable to get 'Initialize' function library symbol" )
		<< "Unable to get 'Initialize' function library symbol: "
		<< strerror(ret) << " / " << fLibrary.GetLastError() << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	fLibrary.Close();
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kFunctionLoadError, "Initialize", ret, fLibrary.GetLastError() );
	}
    ret = GetDLSym<TerminateFunc>( fLibrary, "Terminate", fTerminate );
    if ( ret )
	if ( log )
	    {
	    LOG( MLUCLog::kError, "TMInterfaceLibrary::Initialize", "Unable to get 'Terminate' function library symbol" )
		<< "Unable to get 'Terminate' function library symbol: "
		<< strerror(ret) << " / " << fLibrary.GetLastError() << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	    fLibrary.Close();
	    throw TMInterfaceLibraryException( TMInterfaceLibraryException::kFunctionLoadError, "Terminate", ret, fLibrary.GetLastError() );
	    }
    ret = GetDLSym<ConvertAddressFunc>( fLibrary, "ConvertAddress", fConvertAddress );
    if ( ret )
	{
	if ( log )
	    LOG( MLUCLog::kError, "TMInterfaceLibrary::Initialize", "Unable to get 'ConvertAddress' function library symbol" )
		<< "Unable to get 'ConvertAddress' function library symbol: "
		<< strerror(ret) << " / " << fLibrary.GetLastError() << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	fLibrary.Close();
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kFunctionLoadError, "ConvertAddress", ret, fLibrary.GetLastError() );
	}
    ret = GetDLSym<CompareAddressesFunc>( fLibrary, "CompareAddresses", fCompareAddresses );
    if ( ret )
	{
	if ( log )
	    LOG( MLUCLog::kError, "TMInterfaceLibrary::Initialize", "Unable to get 'CompareAddresses' function library symbol" )
		<< "Unable to get 'CompareAddresses' function library symbol: "
		<< strerror(ret) << " / " << fLibrary.GetLastError() << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	fLibrary.Close();
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kFunctionLoadError, "CompareAddresses", ret, fLibrary.GetLastError() );
	}
    ret = GetDLSym<QueryStateFunc>( fLibrary, "QueryState", fQueryState );
    if ( ret )
	{
	if ( log )
	    LOG( MLUCLog::kError, "TMInterfaceLibrary::Initialize", "Unable to get 'QueryState' function library symbol" )
		<< "Unable to get 'QueryState' function library symbol: "
		<< strerror(ret) << " / " << fLibrary.GetLastError() << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	fLibrary.Close();
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kFunctionLoadError, "QueryState", ret, fLibrary.GetLastError() );
	}
    ret = GetDLSym<QueryStatusDataFunc>( fLibrary, "QueryStatusData", fQueryStatusData );
    if ( ret )
	{
	if ( log )
	    LOG( MLUCLog::kError, "TMInterfaceLibrary::Initialize", "Unable to get 'QueryStatusData' function library symbol" )
		<< "Unable to get 'QueryStatusData' function library symbol: "
		<< strerror(ret) << " / " << fLibrary.GetLastError() << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	fLibrary.Close();
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kFunctionLoadError, "QueryStatusData", ret, fLibrary.GetLastError() );
	}
    ret = GetDLSym<ReleaseResourceFunc>( fLibrary, "ReleaseResource", fReleaseResource );
    if ( ret )
	{
	if ( log )
	    LOG( MLUCLog::kError, "TMInterfaceLibrary::Initialize", "Unable to get 'ReleaseResource' function library symbol" )
		<< "Unable to get 'ReleaseResource' function library symbol: "
		<< strerror(ret) << " / " << fLibrary.GetLastError() << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	fLibrary.Close();
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kFunctionLoadError, "ReleaseResource", ret, fLibrary.GetLastError() );
	}
    ret = GetDLSym<SendCommandFunc>( fLibrary, "SendCommand", fSendCommand );
    if ( ret )
	{
	if ( log )
	    LOG( MLUCLog::kError, "TMInterfaceLibrary::Initialize", "Unable to get 'SendCommand' function library symbol" )
		<< "Unable to get 'SendCommand' function library symbol: "
		<< strerror(ret) << " / " << fLibrary.GetLastError() << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	fLibrary.Close();
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kFunctionLoadError, "SendCommand", ret, fLibrary.GetLastError() );
	}
    ret = GetDLSym<InterruptWaitFunc>( fLibrary, "InterruptWait", fInterruptWait );
    if ( ret )
	{
	if ( log )
	    LOG( MLUCLog::kError, "TMInterfaceLibrary::Initialize", "Unable to get 'InterruptWait' function library symbol" )
		<< "Unable to get 'InterruptWait' function library symbol: "
		<< strerror(ret) << " / " << fLibrary.GetLastError() << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	fLibrary.Close();
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kFunctionLoadError, "InterruptWait", ret, fLibrary.GetLastError() );
	}
    ret = GetDLSym<StopInterruptWaitFunc>( fLibrary, "StopInterruptWait", fStopInterruptWait );
    if ( ret )
	{
	if ( log )
	    LOG( MLUCLog::kError, "TMInterfaceLibrary::Initialize", "Unable to get 'StopInterruptWait' function library symbol" )
		<< "Unable to get 'StopInterruptWait' function library symbol: "
		<< strerror(ret) << " / " << fLibrary.GetLastError() << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	fLibrary.Close();
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kFunctionLoadError, "StopInterruptWait", ret, fLibrary.GetLastError() );
	}
    ret = GetDLSym<ConnectToProcessFunc>( fLibrary, "ConnectToProcess", fConnectToProcess );
    if ( ret )
	{
	fConnectToProcess = NULL;
	// This function is optional
	}
    ret = GetDLSym<DisconnectFromProcessFunc>( fLibrary, "DisconnectFromProcess", fDisconnectFromProcess );
    if ( ret )
	{
	fDisconnectFromProcess = NULL;
	// This function is optional
	}
    ret = GetDLSym<SuppressComErrorLogsFunc>( fLibrary, "SuppressComErrorLogs", fSuppressComErrorLogs );
    if ( ret )
	{
	fSuppressComErrorLogs = NULL;
	// This function is optional
	}
    }

TMInterfaceLibrary::~TMInterfaceLibrary()
    {
    fLibrary.Close();
    }

int TMInterfaceLibrary::Initialize( const char* interrupt_listen_address )
    {
    if ( !fInitialize )
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kInvalidFunctionError, "Initialize", ENOSYS, strerror(ENOSYS) );
    return (*fInitialize)( &fPrivateLibData, interrupt_listen_address );
    }

int TMInterfaceLibrary::Terminate()
    {
    if ( !fTerminate )
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kInvalidFunctionError, "Terminate", ENOSYS, strerror(ENOSYS) );
    return (*fTerminate)( fPrivateLibData );
    }

int TMInterfaceLibrary::ConvertAddress( const char* process_address_str, void** process_address_bin )
    {
    if ( !fConvertAddress )
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kInvalidFunctionError, "ConvertAddress", ENOSYS, strerror(ENOSYS) );
    return (*fConvertAddress)( fPrivateLibData, process_address_str, process_address_bin );
    }

int TMInterfaceLibrary::CompareAddresses( void* addr1, void* addr2 )
    {
    if ( !fCompareAddresses )
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kInvalidFunctionError, "CompareAddresses", ENOSYS, strerror(ENOSYS) );
    return (*fCompareAddresses)( fPrivateLibData, addr1, addr2 );
    }

int TMInterfaceLibrary::QueryState( pid_t pid, const char* process_id,
				    void* process_address_bin, 
				    char** state )
    {
    if ( !fQueryState )
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kInvalidFunctionError, "QueryState", ENOSYS, strerror(ENOSYS) );
    return (*fQueryState)( fPrivateLibData, pid, process_id, process_address_bin, state );
    }

int TMInterfaceLibrary::QueryStatusData( pid_t pid, const char* process_id, 
					 void* process_address_bin, 
					 char** stat_data )
    {
    if ( !fQueryStatusData )
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kInvalidFunctionError, "QueryStatusData", ENOSYS, strerror(ENOSYS) );
    return (*fQueryStatusData)( fPrivateLibData, pid, process_id, process_address_bin, stat_data );
    }

void TMInterfaceLibrary::ReleaseResource( void* resource )
    {
    if ( !fReleaseResource )
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kInvalidFunctionError, "ReleaseResource", ENOSYS, strerror(ENOSYS) );
    return (*fReleaseResource)( fPrivateLibData, resource );
    }

int TMInterfaceLibrary::SendCommand( pid_t pid, const char* process_id,
				     void* process_address_bin,
				     const char* command )
    {
    if ( !fSendCommand )
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kInvalidFunctionError, "SendCommand", ENOSYS, strerror(ENOSYS) );
    return (*fSendCommand)( fPrivateLibData, pid, process_id, process_address_bin, command );
    }

int TMInterfaceLibrary::InterruptWait( void* opaque_arg, const char* listen_address, TMInterruptTriggerCallback_t callback )
    {
    if ( !fInterruptWait )
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kInvalidFunctionError, "InterruptWait", ENOSYS, strerror(ENOSYS) );
    int ret;
    pthread_cleanup_push( TMInterfaceLibrary::InterruptWaitCleanupFuncWrapper, this );
    fInterruptWaitRunning = true;
    ret = (*fInterruptWait)( fPrivateLibData, opaque_arg, listen_address, callback );
    fInterruptWaitRunning = false;
    pthread_cleanup_pop( 0 );
    return ret;
    }

int TMInterfaceLibrary::StopInterruptWait()
    {
    if ( !fStopInterruptWait )
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kInvalidFunctionError, "StopInterruptWait", ENOSYS, strerror(ENOSYS) );
    return (*fStopInterruptWait)( fPrivateLibData );
    }

int TMInterfaceLibrary::ConnectToProcess( pid_t pid, 
					  const char* process_id, 
					  void* process_address_bin )
    {
    if ( !fConnectToProcess )
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kInvalidFunctionError, "ConnectToProcess", ENOSYS, strerror(ENOSYS) );
    return (*fConnectToProcess)( fPrivateLibData, pid, process_id, process_address_bin );
    }

int TMInterfaceLibrary::DisconnectFromProcess( pid_t pid, 
					       const char* process_id, 
					       void* process_address_bin )
    {
    if ( !fDisconnectFromProcess )
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kInvalidFunctionError, "DisconnectFromProcess", ENOSYS, strerror(ENOSYS) );
    return (*fDisconnectFromProcess)( fPrivateLibData, pid, process_id, process_address_bin );
    }

int TMInterfaceLibrary::SuppressComErrorLogs( pid_t pid, 
					      const char* process_id, 
					      void* process_address_bin,
					      unsigned long timeout_us )
    {
    if ( !fSuppressComErrorLogs )
	throw TMInterfaceLibraryException( TMInterfaceLibraryException::kInvalidFunctionError, "SuppressComErrorLogs", ENOSYS, strerror(ENOSYS) );
    return (*fSuppressComErrorLogs)( fPrivateLibData, pid, process_id, process_address_bin, timeout_us );
    }


void* TMInterfaceLibrary::GetPrivateLibData()
    {
    return fPrivateLibData;
    }


void TMInterfaceLibrary::InterruptWaitCleanupFuncWrapper( void* arg )
    {
    ((TMInterfaceLibrary*)arg)->InterruptWaitCleanupFunc();
    }

void TMInterfaceLibrary::InterruptWaitCleanupFunc()
    {
    fInterruptWaitRunning = false;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
