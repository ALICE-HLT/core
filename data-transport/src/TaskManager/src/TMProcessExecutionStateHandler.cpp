/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TMProcessExecutionStateHandler.hpp"
#include "TMSysException.hpp"
#include "MLUCLog.hpp"
#include <errno.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <stdio.h>


TMProcessExecutionStateHandler::TMProcessExecutionStateHandler( unsigned long procCnt )
    {
    fProcessCnt = procCnt;
    if ( fProcessCnt<=0 )
	{
	fShmID = -1;
	fStates = (ProcessExecutionState*)(void*)-1;
	return;
	}
    fShmID = shmget( IPC_PRIVATE, procCnt*sizeof(ProcessExecutionState), IPC_CREAT|0755 );
    //fShmID = shmget( 43, procCnt*sizeof(ProcessExecutionState), IPC_CREAT|0755 );
    if ( fShmID==-1 )
	throw TMSysException( errno, strerror(errno), "Unable to get shared memory segment",
			      __FILE__, __LINE__ );
    fStates = (ProcessExecutionState*)shmat( fShmID, 0, 0 );
    if ( fStates==(void*)-1 )
	throw TMSysException( errno, strerror(errno), "Unable to get pointer to shared memory segment",
			      __FILE__, __LINE__ );
    unsigned long n;
    for ( n = 0; n < procCnt; n++ )
	{
	fStates[n].fNdx = n;
	fStates[n].fPID = -1;
	fStates[n].fStatus = kReset;
	fStates[n].fError = 0;
	}
    }

TMProcessExecutionStateHandler::~TMProcessExecutionStateHandler()
    {
    if ( fStates != (void*)-1 )
	shmdt( fStates );
    if ( fShmID != -1 )
	shmctl( fShmID, IPC_RMID, NULL );
    }

void TMProcessExecutionStateHandler::SetProcessExecutionStatus( unsigned long proc_ndx, int error )
    {
    // No error handling is necessary here. This method is only called from a
    // forked child process when the exec fails. The next thing it does is
    // always an exit()
    unsigned long pos = FindProcByNdx( proc_ndx );
    fStates[pos].fStatus = kExecFailed;
    fStates[pos].fError = error;
    }

void TMProcessExecutionStateHandler::SetProcessExecutionStatus( pid_t pid, unsigned long proc_ndx, bool started, int error )
    {
    unsigned long pos;
    try
	{
	pos = FindProcByNdx( proc_ndx );
	}
    catch ( TMSysException e )
	{
	LOG( MLUCLog::kError, "TMProcessExecutionStateHandler::SetProcessExecutionStatus", "Error Setting Process State" )
	    << e.GetDescription() << "/" << e.GetErrorStr() << " ("
	    << MLUCLog::kDec << e.GetErrno() << " (" << e.GetFile() << "/" << MLUCLog::kDec
	    << e.GetLine() << ")" << "." << ENDLOG;
	return;
	}
    fStates[pos].fPID = pid;
    if ( fStates[pos].fStatus!=kExecFailed )
	{
	fStates[pos].fStatus = (started ? kStarted : kForkFailed);
	fStates[pos].fError = error;
	}
    }

void TMProcessExecutionStateHandler::SetProcessExecutionStatus( unsigned long proc_ndx )
    {
    unsigned long pos;
    try
	{
	pos = FindProcByNdx( proc_ndx );
	}
    catch ( TMSysException e )
	{
	LOG( MLUCLog::kError, "TMProcessExecutionStateHandler::SetProcessExecutionStatus", "Error Setting Process State" )
	    << e.GetDescription() << "/" << e.GetErrorStr() << " ("
	    << MLUCLog::kDec << e.GetErrno() << " (" << e.GetFile() << "/" << MLUCLog::kDec
	    << e.GetLine() << ")" << "." << ENDLOG;
	return;
	}
    fStates[pos].fStatus = kKilled;
    }

void TMProcessExecutionStateHandler::ResetProcessExecutionStatus( unsigned long proc_ndx )
    {
    unsigned long pos;
    try
	{
	pos = FindProcByNdx( proc_ndx );
	}
    catch ( TMSysException e )
	{
	LOG( MLUCLog::kError, "TMProcessExecutionStateHandler::ResetProcessExecutionStatus", "Error Setting Process State" )
	    << e.GetDescription() << "/" << e.GetErrorStr() << " ("
	    << MLUCLog::kDec << e.GetErrno() << " (" << e.GetFile() << "/" << MLUCLog::kDec
	    << e.GetLine() << ")" << "." << ENDLOG;
	return;
	}
    fStates[pos].fPID = -1;
    fStates[pos].fStatus = kReset;
    fStates[pos].fError = 0;
    }

const TMProcessExecutionStateHandler::ProcessExecutionState& TMProcessExecutionStateHandler::GetProcessExecutionStatus( unsigned long proc_ndx )
    {
    unsigned long pos;
    try
	{
	pos = FindProcByNdx( proc_ndx );
	}
    catch ( TMSysException e )
	{
	LOG( MLUCLog::kError, "TMProcessExecutionStateHandler::GetProcessExecutionStatus", "Error Setting Process State" )
	    << e.GetDescription() << "/" << e.GetErrorStr() << " ("
	    << MLUCLog::kDec << e.GetErrno() << " (" << e.GetFile() << "/" << MLUCLog::kDec
	    << e.GetLine() << ")" << "." << ENDLOG;
	return fEmptyState;
	}
    return fStates[pos];
    }

TMProcessExecutionStateHandler::ProcessStatus TMProcessExecutionStateHandler::GetProcessStatus( unsigned long proc_ndx )
    {
    unsigned long pos;
    try
	{
	pos = FindProcByNdx( proc_ndx );
	}
    catch ( TMSysException e )
	{
	LOG( MLUCLog::kError, "TMProcessExecutionStateHandler::GetProcessStatus", "Error Setting Process State" )
	    << e.GetDescription() << "/" << e.GetErrorStr() << " ("
	    << MLUCLog::kDec << e.GetErrno() << " (" << e.GetFile() << "/" << MLUCLog::kDec
	    << e.GetLine() << ")" << "." << ENDLOG;
	return kInvalid;
	}
    return fStates[pos].fStatus;
    }

int TMProcessExecutionStateHandler::GetProcessError( unsigned long proc_ndx )
    {
    unsigned long pos;
    try
	{
	pos = FindProcByNdx( proc_ndx );
	}
    catch ( TMSysException e )
	{
	LOG( MLUCLog::kError, "TMProcessExecutionStateHandler::GetProcessError", "Error Setting Process State" )
	    << e.GetDescription() << "/" << e.GetErrorStr() << " ("
	    << MLUCLog::kDec << e.GetErrno() << " (" << e.GetFile() << "/" << MLUCLog::kDec
	    << e.GetLine() << ")" << "." << ENDLOG;
	return EINVAL;
	}
    return fStates[pos].fError;
    }


void TMProcessExecutionStateHandler::Resize( unsigned long newProcCnt )
    {
#if 0
    throw TMSysException( ENOTSUP, strerror(ENOTSUP), "Resize function not yet supported",
			  __FILE__, __LINE__ );
#endif
    int newShmID;
    ProcessExecutionState* newStates;
    newShmID = shmget( IPC_PRIVATE, newProcCnt*sizeof(ProcessExecutionState), IPC_CREAT|0755 );
    //fShmID = shmget( 43, procCnt*sizeof(ProcessExecutionState), IPC_CREAT|0755 );
    if ( newShmID==-1 )
	throw TMSysException( errno, strerror(errno), "Unable to get shared memory segment",
			      __FILE__, __LINE__ );
    newStates = (ProcessExecutionState*)shmat( newShmID, 0, 0 );
    if ( newStates==(void*)-1 )
	throw TMSysException( errno, strerror(errno), "Unable to get pointer to shared memory segment",
			      __FILE__, __LINE__ );
    unsigned long n;
    unsigned long cnt = (fProcessCnt<newProcCnt ? fProcessCnt : newProcCnt );
    for ( n = 0; n < cnt; n++ )
	{
	newStates[n].fNdx = fStates[n].fNdx;
	newStates[n].fPID = fStates[n].fPID;
	newStates[n].fStatus = fStates[n].fStatus;
	newStates[n].fError = fStates[n].fError;
	}
    for ( n = fProcessCnt; n < newProcCnt; n++ )
	{
	newStates[n].fNdx = ~(unsigned long)0;
	newStates[n].fPID = -1;
	newStates[n].fStatus = kInvalid;
	newStates[n].fError = 0;
	}
    shmdt( fStates );
    shmctl( fShmID, IPC_RMID, NULL );
    fStates = newStates;
    fShmID = newShmID;
    fProcessCnt = newProcCnt;
    }

void TMProcessExecutionStateHandler::AddProcess( unsigned long proc_ndx )
    {
    unsigned long n;
    for ( n = 0; n < fProcessCnt; n++ )
	{
	if ( fStates[n].fStatus == kInvalid )
	    break;
	}
    if ( n==fProcessCnt )
	{
	throw TMSysException( ENOSPC, strerror(ENOSPC), "No free process slot found",
			      __FILE__, __LINE__ );
	}
    fStates[n].fStatus = kReset;
    fStates[n].fNdx = proc_ndx;
    }

void TMProcessExecutionStateHandler::DeleteProcess( unsigned long proc_ndx )
    {
    unsigned long pos;
    try
	{
	pos = FindProcByNdx( proc_ndx );
	}
    catch ( TMSysException e )
	{
	LOG( MLUCLog::kError, "TMProcessExecutionStateHandler::DeleteProcess", "Error Setting Process State" )
	    << e.GetDescription() << "/" << e.GetErrorStr() << " ("
	    << MLUCLog::kDec << e.GetErrno() << " (" << e.GetFile() << "/" << MLUCLog::kDec
	    << e.GetLine() << ")" << "." << ENDLOG;
	return;
	}
    fStates[pos].fNdx = ~(unsigned long)0;
    fStates[pos].fStatus = kInvalid;
    fStates[pos].fPID = -1;
    fStates[pos].fError = 0;
    }

void TMProcessExecutionStateHandler::Compact()
    {
    unsigned long d, s;
    for ( d=0,s=0; s<fProcessCnt; s++ )
	{
	if ( fStates[s].fStatus != kInvalid )
	    fStates[d++] = fStates[s];
	}
    }


unsigned long TMProcessExecutionStateHandler::FindProcByNdx( unsigned long proc_ndx )
    {
    unsigned long n;
    for ( n = 0; n < fProcessCnt; n++ )
	{
	if ( fStates[n].fNdx == proc_ndx )
	    return n;
	}
    MLUCString errStr;
    errStr = "Process with specified index ";
    char tmp[128];
    sprintf( tmp, "%lu", proc_ndx );
    errStr += tmp;
    errStr += " not found";
    throw TMSysException( EINVAL, strerror(EINVAL), errStr.c_str(),
			  __FILE__, __LINE__ );
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
