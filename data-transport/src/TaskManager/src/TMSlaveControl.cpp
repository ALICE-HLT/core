/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TMPythonInterface.hpp"
#include "TMSlaveControl.hpp"
#include "TMSysException.hpp"
#include "TMControlMessage.hpp"
#include "TMControlMessageTypes.hpp"
#include "TMSystem.hpp"
#include <BCLURLAddressDecoder.hpp>
#include <BCLAddressLogger.hpp>
#include <MLUCLog.hpp>
#include <errno.h>

#ifndef VALGRIND_PARANOIA
//#define VALGRIND_PARANOIA
#endif


TMSlaveControl::TMSlaveControl( TMSystem* sys, TMConfig::SlaveControlConfigData& conf ):
    fReceiveThread( this, &TMSlaveControl::ReceiveLoop )
    {
    fSystem = sys;
    fAddress = NULL;
    fReceiveAddress = NULL;
    fCom = NULL;
    pthread_mutex_init( &fComMutex, NULL );
    fQuitReceiveThread = false;
    fReceiveThreadRunning = false;
    pthread_mutex_init( &fCommandMutex, NULL );
    pthread_mutex_init( &fSysCommandMutex, NULL );
    pthread_mutex_init( &fStateMutex, NULL );
    pthread_mutex_init( &fCommandReceivedActionMutex, NULL );

    fAddressString = conf.fAddress;
    //fSystem->NormalizeAddress( conf.fAddress.c_str(), fAddressString );
    bool isBlob;
    int ret;
    BCLCommunication* tmpCom;
    ret = BCLDecodeLocalURL( fAddressString.c_str(), tmpCom, fAddress, isBlob );
    if ( ret )
	{
 	LOG( MLUCLog::kError, "TMSlaveControl::TMSlaveControl", "Unable to decode receive address" )
	    << "Unable to decode receive address '" << fAddressString.c_str()
	    << "': " << strerror(ret) << " (" << MLUCLog::kDec
	    << ret << ")." << ENDLOG;
	throw TMSysException( ret, strerror(ret), "Unable to decode slave control receive address", __FILE__, __LINE__ );
	}
    if ( isBlob )
	{
	LOG( MLUCLog::kError, "TMSlaveControl::TMSlaveControl", "Blob receive address specified" )
	    << "Blob address type not allowed for receive address '" << fAddressString.c_str()
	    << "'. Message address is required." << ENDLOG;
	throw TMSysException( ret, strerror(ret), "Blob address type not allowed for slave control receive address", __FILE__, __LINE__ );
	}
    fCom = (BCLMsgCommunication*)tmpCom;
    
    fReceiveAddress = fCom->NewAddress();

    fCommandReceivedAction = conf.fCommandReceivedAction;
    
    vector<MLUCString>::iterator iter, end;
    iter = conf.fInterruptTargetAddresses.begin();
    end = conf.fInterruptTargetAddresses.end();
    while ( iter != end )
	{
	InterruptTargetAddressData itad;
	ret = BCLDecodeRemoteURL( iter->c_str(), itad.fAddress );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "TMSlaveControl::TMSlaveControl", "Unable to decode interrupt target address" )
		<< "Unable to decode interrupt target address '" << iter->c_str()
		<< "': " << strerror(ret) << " (" << MLUCLog::kDec
		<< ret << ")." << ENDLOG;
	    throw TMSysException( ret, strerror(ret), "Unable to decode slave control interrupt target address", __FILE__, __LINE__ );
	    }
	itad.fAddressString = *iter;
	//fSystem->NormalizeAddress( iter->c_str(), itad.fAddressString );
	fInterruptTargetAddresses.insert( fInterruptTargetAddresses.end(), itad );
	iter++;
	}

    fStatesData = conf.fStatesData;

    }

TMSlaveControl::~TMSlaveControl()
    {
    pthread_mutex_destroy( &fCommandReceivedActionMutex );
    pthread_mutex_destroy( &fComMutex );
    pthread_mutex_destroy( &fCommandMutex );
    pthread_mutex_destroy( &fStateMutex );
    pthread_mutex_destroy( &fSysCommandMutex );
    Clear();
    }

void TMSlaveControl::Start()
    {
    int ret;
    ret = fCom->Bind( fAddress );
    if ( ret )
	{
	  LOG( MLUCLog::kError, "TMSlaveControl::Start", "Error binding to address" )
	      << "Error binding to specified local address '" << fAddressString.c_str() << "': "
	      << strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
	      << ENDLOG;
	  throw TMSysException( ret, strerror(ret), "Error binding to specified local address",
			      __FILE__, __LINE__ );
	}
    fReceiveThread.Start();
    if ( fSystem->GetProcessCnt()<=0 )
	fSystem->SetTaskState( "TM:EMPTY" );
    else
	fSystem->SetTaskState( "TM:READY" );
    }
    
void TMSlaveControl::Stop()
    {
    if ( !QuitReceiveLoop() )
	fReceiveThread.Abort();
    fReceiveThread.Join();
    fCom->Unbind();
    }


// void TMSlaveControl::GetCommands( vector<MLUCString>& cmds )
//     {
//     pthread_mutex_lock( &fCommandMutex );
//     cmds = fCommands;
//     fCommands.clear();
//     pthread_mutex_unlock( &fCommandMutex );
//     }

bool TMSlaveControl::GetCommand( MLUCString& cmd )
    {
    bool ret=false;
    pthread_mutex_lock( &fCommandMutex );
    if ( fCommands.size()>0 )
	{
	ret = true;
	cmd = *fCommands.begin();
	fCommands.erase( fCommands.begin() );
	}
    pthread_mutex_unlock( &fCommandMutex );
    return ret;
    }

bool TMSlaveControl::GetSysCommand( MLUCString& cmd )
    {
    bool ret=false;
    pthread_mutex_lock( &fSysCommandMutex );
    if ( fSysCommands.size()>0 )
	{
	ret = true;
	cmd = *fSysCommands.begin();
	fSysCommands.erase( fSysCommands.begin() );
	}
    pthread_mutex_unlock( &fSysCommandMutex );
    return ret;
    }

bool TMSlaveControl::HasCommands()
    {
    bool ret=false;
    pthread_mutex_lock( &fCommandMutex );
    if ( fCommands.size()>0 )
	ret = true;
    pthread_mutex_unlock( &fCommandMutex );
    return ret;
    }

void TMSlaveControl::CommandReceived()
    {
    MLUCString action;
    pthread_mutex_lock( &fCommandReceivedActionMutex );
    action = fCommandReceivedAction;
    pthread_mutex_unlock( &fCommandReceivedActionMutex );
    fSystem->GetMainPythonInterface()->Run( action.c_str() );
    pthread_mutex_lock( &fCommandMutex );
    fCommands.clear();
    pthread_mutex_unlock( &fCommandMutex );
    }

void TMSlaveControl::Interrupt( const char* notificationData )
    {
    TMControlMessage msg;
    TMControlMessageStruct* msgData;
    msg.GetData()->fMsgType = kTMControlInterrupt;
    if ( notificationData )
	{
	unsigned long len = msg.GetData()->fLength+strlen(notificationData)+1;
	msgData = reinterpret_cast<TMControlMessageStruct*>( new uint8[ len ] );
	if ( !msgData )
	    {
	    LOG( MLUCLog::kError, "TMSlaveControl::Interrupt", "Out of memory" )
		<< "Out of memory allocating " << MLUCLog::kDec << len
		<< " bytes of memory for interrupt message. Sending message without notification data."
		<< ENDLOG;
	    msgData = msg.GetData();
	    }
	else
	    {
	    memcpy( msgData, msg.GetData(), msg.GetData()->fLength );
	    strcpy( (char*)(((uint8*)msgData)+msg.GetData()->fLength), notificationData );
	    msgData->fLength = len;
	    }
	}
    else
	msgData = msg.GetData();

    vector<InterruptTargetAddressData>::iterator iter, end;
    iter = fInterruptTargetAddresses.begin();
    end = fInterruptTargetAddresses.end();
    int ret;
    while ( iter != end )
	{
	if ( iter->fAddress )
	    {
	    pthread_mutex_lock( &fComMutex );
	    ret = fCom->Send( iter->fAddress, msgData, 1000 ); // half a second has to be sufficient.
	    if ( ret )
		{
		LOG( MLUCLog::kError, "TMSlaveControl::Interrupt", "Error sending interrupt message" )
		    << "Error sending interrupt message to address '" << iter->fAddressString.c_str() << "': "
		    << strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
		    << ENDLOG;
		}
	    pthread_mutex_unlock( &fComMutex );
	    }
	iter++;
	}
    if ( msgData != msg.GetData() )
	delete [] reinterpret_cast<uint8*>( msgData );
    }

void TMSlaveControl::Set( TMConfig::SlaveControlConfigData& conf )
    {
    Clear();

    fAutonomous = conf.fAutonomous;
    
    fAddressString = conf.fAddress;
    //fSystem->NormalizeAddress( conf.fAddress.c_str(), fAddressString );
    bool isBlob;
    int ret;
    BCLCommunication* tmpCom;
    ret = BCLDecodeLocalURL( fAddressString.c_str(), tmpCom, fAddress, isBlob );
    if ( ret )
	{
 	LOG( MLUCLog::kError, "TMSlaveControl::Set", "Unable to decode receive address" )
	    << "Unable to decode receive address '" << fAddressString.c_str()
	    << "': " << strerror(ret) << " (" << MLUCLog::kDec
	    << ret << ")." << ENDLOG;
	throw TMSysException( ret, strerror(ret), "Unable to decode slave control receive address", __FILE__, __LINE__ );
	}
    if ( isBlob )
	{
	LOG( MLUCLog::kError, "TMSlaveControl::Set", "Blob receive address specified" )
	    << "Blob address type not allowed for receive address '" << fAddressString.c_str()
	    << "'. Message address is required." << ENDLOG;
	throw TMSysException( ret, strerror(ret), "Blob address type not allowed for slave control receive address", __FILE__, __LINE__ );
	}
    fCom = (BCLMsgCommunication*)tmpCom;
    
    fReceiveAddress = fCom->NewAddress();

    fCommandReceivedAction = conf.fCommandReceivedAction;
    
    vector<MLUCString>::iterator iter, end;
    iter = conf.fInterruptTargetAddresses.begin();
    end = conf.fInterruptTargetAddresses.end();
    while ( iter != end )
	{
	InterruptTargetAddressData itad;
	ret = BCLDecodeRemoteURL( iter->c_str(), itad.fAddress );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "TMSlaveControl::Set", "Unable to decode interrupt target address" )
		<< "Unable to decode interrupt target address '" << fAddressString.c_str()
		<< "': " << strerror(ret) << " (" << MLUCLog::kDec
		<< ret << ")." << ENDLOG;
	    throw TMSysException( ret, strerror(ret), "Unable to decode slave control interrupt target address", __FILE__, __LINE__ );
	    }
	itad.fAddressString = *iter;
	//fSystem->NormalizeAddress( iter->c_str(), itad.fAddressString );
	fInterruptTargetAddresses.insert( fInterruptTargetAddresses.end(), itad );
	iter++;
	}

    pthread_mutex_lock( &fStateMutex );
    fStatesData = conf.fStatesData;
    pthread_mutex_unlock( &fStateMutex );
    }

void TMSlaveControl::SetCommandReceivedAction( const MLUCString& action )
    {
    pthread_mutex_lock( &fCommandReceivedActionMutex );
    fCommandReceivedAction = action;
    pthread_mutex_unlock( &fCommandReceivedActionMutex );
    }



void TMSlaveControl::GetInterruptTargetAddresses( vector<MLUCString>& addresses )
    {
    vector<InterruptTargetAddressData>::iterator iter, end;
    iter = fInterruptTargetAddresses.begin();
    end = fInterruptTargetAddresses.end();
    MLUCString tmp;
    addresses.clear();
    while ( iter != end )
	{
	tmp = iter->fAddressString;
	addresses.insert( addresses.end(), tmp );
	iter++;
	}
    }


void TMSlaveControl::Clear()
    {
    if ( fCom && fReceiveAddress )
	fCom->DeleteAddress( fReceiveAddress );
    fReceiveAddress = NULL;
    while ( fInterruptTargetAddresses.size()>0 )
	{
	if ( fInterruptTargetAddresses.begin()->fAddress )
	    BCLFreeAddress( fInterruptTargetAddresses.begin()->fAddress );
	fInterruptTargetAddresses.erase( fInterruptTargetAddresses.begin() );
	}
    BCLFreeAddress( fCom, fAddress );
    fCom = NULL;
    fAddress = NULL;
    fAddressString = "";
    fCommandReceivedAction = "";
    fAutonomous = false;
    }


void TMSlaveControl::ReceiveLoop()
    {
    fQuitReceiveThread = false;
    fReceiveThreadRunning = true;

    int ret;
    BCLMessageStruct* msg;
    do
	{
	ret = fCom->Receive( fReceiveAddress, msg, 50ul ); // Timeout of 50ms.
	if ( ret && ret!=ETIMEDOUT )
	    {
	    LOG( MLUCLog::kError, "TMSlaveControl::ReceiveLoop", "Error receiving message" )
		<< "Error receiving message: " << strerror(ret) << " ("
		<< MLUCLog::kDec << ret << ")." << ENDLOG;
	    }
	else if ( !ret )
	    {
	    switch ( msg->fMsgType )
		{
		case kTMControlQueryState: // Fallthrough
		case kTMControlQueryStatusData:
		    {
		    MLUCString state;
		    TMControlMessage cmsg;
		    const char* type;
		    if ( msg->fMsgType==kTMControlQueryState )
			{
			fSystem->GetTaskState( state );
			cmsg.GetData()->fMsgType = kTMControlStateReply;
			type = "state";
			}
		    else
			{
			fSystem->GetTaskStatusData( state );
			cmsg.GetData()->fMsgType = kTMControlStatusDataReply;
			type = "status data";
			}
		    LOG( MLUCLog::kDebug, "TMSlaveControl::ReceiveLoop", "Query received" )
			<< type << " query message (reply id " << MLUCLog::kDec << ((TMControlMessageStruct*)msg)->fReplyID
			<< ") received." << ENDLOG;
		    TMControlMessageStruct* msgP;
		    uint32 len = state.Size()+1;
		    if ( len % 4 )
			len += 4-(len%4);
		    msgP = (TMControlMessageStruct*)malloc( cmsg.GetData()->fLength+len );
		    if ( !msgP )
			{
			LOG( MLUCLog::kError, "TMSlaveControl::ReceiveLoop", "Out of memory" )
			    << "Out of memory trying to allocate " << type << " query reply message of " << MLUCLog::kDec
			    << cmsg.GetData()->fLength+len  << " bytes." << ENDLOG;
			break;
			}
#ifdef VALGRIND_PARANOIA
		    memset( ((uint8*)msgP)+cmsg.GetData()->fLength, 0,len );
#endif
		    memcpy( msgP, cmsg.GetData(), cmsg.GetData()->fLength );
		    BCLNetworkData::Transform( ((TMControlMessageStruct*)msg)->fReplyID, msgP->fReplyID, msg->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );
		    msgP->fLength += len;
		    strcpy( (char*)msgP->fReply, state.c_str() );
		    
		    pthread_mutex_lock( &fComMutex );
		    ret = fCom->Send( fReceiveAddress, msgP, 1000 ); // 2.5s have to be sufficient.
		    if ( ret )
			{
			LOG( MLUCLog::kWarning, "TMSlaveControl::ReceiveLoop", "Error sending reply message" )
			    << "Error sending " << type << " reply message: "
			    << strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
			    << ENDLOG;
			}
		    pthread_mutex_unlock( &fComMutex );
		    free( msgP );
		    break;
		    }
		case kTMControlCommand:
		    {
		    MLUCString cmd;
		    cmd = (char*)(((TMControlMessageStruct*)msg)->fReply);
		    LOG( MLUCLog::kInformational, "TMSlaveControl::ReceiveLoop", "Command received" )
			<< "Command '" << cmd.c_str() << "' received." << ENDLOG;
		    if ( !strncmp( cmd.c_str(), "TM:", 3 ) )
			{
			pthread_mutex_lock( &fSysCommandMutex );
			fSysCommands.insert( fSysCommands.end(), cmd );
			pthread_mutex_unlock( &fSysCommandMutex );
			}
		    else
			{
			pthread_mutex_lock( &fCommandMutex );
			fCommands.insert( fCommands.end(), cmd );
			pthread_mutex_unlock( &fCommandMutex );
			}
		    break;
		    }
		case kTMControlConnect:
		    {
		    pthread_mutex_lock( &fComMutex );
		    LOG( MLUCLog::kDebug, "TMSlaveControl::ReceiveLoop", "Attempting master connection" )
			<< "Attempting to estalish reverse connection to master address" << ENDLOG;
		    ret = fCom->Connect( fReceiveAddress, 2500, true ); // 2.5s have to be sufficient.
		    if ( ret )
			{
			LOG( MLUCLog::kError, "TMSlaveControl::ReceiveLoop", "Error establishing demanded connection" )
			    << "Error establishing demanded connection: "
			    << strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
			    << ENDLOG;
			}
		    pthread_mutex_unlock( &fComMutex );
		    break;
		    }
		case kTMControlDisconnect:
		    {
		    pthread_mutex_lock( &fComMutex );
		    ret = fCom->Disconnect( fReceiveAddress, 2500 ); // 2.5s have to be sufficient.
		    if ( ret )
			{
			if ( ret != EADDRNOTAVAIL )
			    {
			    MLUCLog::TLogMsg logMsg;
			    LOGG( MLUCLog::kError, "TMSlaveControl::ReceiveLoop", "Error severing connection", logMsg )
				<< "Error severing connection from '";
			    BCLAddressLogger::LogAddress( logMsg, *fReceiveAddress )
				<< "': "
				<< strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
				<< ENDLOG;
			    }
			else
			    {
			    LOGG( MLUCLog::kDebug, "TMSlaveControl::ReceiveLoop", "Address not yet connected", logMsg )
				<< "No connection yet established to '";
			    BCLAddressLogger::LogAddress( logMsg, *fReceiveAddress )
				<< "': "
				<< strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
				<< ENDLOG;
			    }
			}
		    pthread_mutex_unlock( &fComMutex );
		    break;
		    }
		case kTMControlQueryChildren:
		    {
		    MLUCString children;
		    TMControlMessage cmsg;
		    fSystem->GetChildren( children );
		    cmsg.GetData()->fMsgType = kTMControlChildrenReply;
		    LOG( MLUCLog::kDebug, "TMSlaveControl::ReceiveLoop", "Query received" )
			<< "children query message received." << ENDLOG;
		    TMControlMessageStruct* msgP;
		    uint32 len = children.Size()+1;
		    if ( len % 4 )
			len += 4-(len%4);
		    msgP = (TMControlMessageStruct*)malloc( cmsg.GetData()->fLength+len );
		    if ( !msgP )
			{
			LOG( MLUCLog::kError, "TMSlaveControl::ReceiveLoop", "Out of memory" )
			    << "Out of memory trying to allocate children query reply message of " << MLUCLog::kDec
			    << cmsg.GetData()->fLength+len  << " bytes." << ENDLOG;
			break;
			}
#ifdef VALGRIND_PARANOIA
		    memset( ((uint8*)msgP)+cmsg.GetData()->fLength, 0,len );
#endif
		    memcpy( msgP, cmsg.GetData(), cmsg.GetData()->fLength );
		    BCLNetworkData::Transform( ((TMControlMessageStruct*)msg)->fReplyID, msgP->fReplyID, msg->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );
		    msgP->fLength += len;
		    strcpy( (char*)msgP->fReply, children.c_str() );
		    
		    pthread_mutex_lock( &fComMutex );
		    ret = fCom->Send( fReceiveAddress, msgP, 1000 ); // half a second has to be sufficient.
		    if ( ret )
			{
			LOG( MLUCLog::kError, "TMSlaveControl::ReceiveLoop", "Error sending reply message" )
			    << "Error sending children reply message: "
			    << strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
			    << ENDLOG;
			}
		    pthread_mutex_unlock( &fComMutex );
		    free( msgP );
		    break;
		    }
		case kTMControlQueryStates:
		    {
		    MLUCString states;
		    TMControlMessage cmsg;
		    GetStates( states );
		    cmsg.GetData()->fMsgType = kTMControlStatesReply;
		    LOG( MLUCLog::kDebug, "TMSlaveControl::ReceiveLoop", "Query received" )
			<< "States query message received." << ENDLOG;
		    LOG( MLUCLog::kDebug, "TMSlaveControl::ReceiveLoop", "States" )
			<< "States string: '" << states.c_str() << "'." << ENDLOG;
		    TMControlMessageStruct* msgP;
		    uint32 len = states.Size()+1;
		    if ( len % 4 )
			len += 4-(len%4);
		    msgP = (TMControlMessageStruct*)malloc( cmsg.GetData()->fLength+len );
		    if ( !msgP )
			{
			LOG( MLUCLog::kError, "TMSlaveControl::ReceiveLoop", "Out of memory" )
			    << "Out of memory trying to allocate states query reply message of " << MLUCLog::kDec
			    << cmsg.GetData()->fLength+len  << " bytes." << ENDLOG;
			break;
			}
#ifdef VALGRIND_PARANOIA
		    memset( ((uint8*)msgP)+cmsg.GetData()->fLength, 0,len );
#endif
		    memcpy( msgP, cmsg.GetData(), cmsg.GetData()->fLength );
		    BCLNetworkData::Transform( ((TMControlMessageStruct*)msg)->fReplyID, msgP->fReplyID, msg->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );
		    msgP->fLength += len;
		    strcpy( (char*)msgP->fReply, states.c_str() );
		    
		    pthread_mutex_lock( &fComMutex );
		    ret = fCom->Send( fReceiveAddress, msgP, 1000 ); // half a second has to be sufficient.
		    if ( ret )
			{
			LOG( MLUCLog::kError, "TMSlaveControl::ReceiveLoop", "Error sending reply message" )
			    << "Error sending states reply message: "
			    << strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
			    << ENDLOG;
			}
		    pthread_mutex_unlock( &fComMutex );
		    free( msgP );
		    break;
		    }
		case kTMControlQueryChildStates:
		    {
		    MLUCString id;
		    id = (char*)(((TMControlMessageStruct*)msg)->fReply);
		    MLUCString states;
		    TMControlMessage csmsg;
		    LOG( MLUCLog::kDebug, "TMSlaveControl::ReceiveLoop", "Query received" )
			<< "child states query message received (ID: '"
			<< id.c_str() << "')." << ENDLOG;
		    fSystem->GetChildStates( id.c_str(), states );
		    csmsg.GetData()->fMsgType = kTMControlChildStatesReply;
		    LOG( MLUCLog::kDebug, "TMSlaveControl::ReceiveLoop", "Child states" )
			<< "child states: '" << states.c_str() << "'." << ENDLOG;
		    TMControlMessageStruct* msgP = NULL;
		    uint32 len = states.Size()+1;
		    if ( len % 4 )
			len += 4-(len%4);
		    msgP = (TMControlMessageStruct*)malloc( csmsg.GetData()->fLength+len );
		    if ( !msgP )
			{
			LOG( MLUCLog::kError, "TMSlaveControl::ReceiveLoop", "Out of memory" )
			    << "Out of memory trying to allocate child states query reply message of " << MLUCLog::kDec
			    << csmsg.GetData()->fLength+len  << " bytes." << ENDLOG;
			break;
			}
		    memcpy( msgP, csmsg.GetData(), csmsg.GetData()->fLength );
		    BCLNetworkData::Transform( ((TMControlMessageStruct*)msg)->fReplyID, msgP->fReplyID, msg->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );
		    msgP->fLength += len;
		    strcpy( (char*)msgP->fReply, states.c_str() );
		    
		    pthread_mutex_lock( &fComMutex );
		    ret = fCom->Send( fReceiveAddress, msgP, 1000 ); // half a second has to be sufficient.
		    if ( ret )
			{
			LOG( MLUCLog::kError, "TMSlaveControl::ReceiveLoop", "Error sending reply message" )
			    << "Error sending child states reply message: "
			    << strerror(ret) << MLUCLog::kDec << " (" << ret << ")."
			    << ENDLOG;
			}
		    pthread_mutex_unlock( &fComMutex );
		    free( msgP );
		    break;
		    }
		default:
		    LOG( MLUCLog::kError, "TMControlMaster::RunLoop", "Unexpected message" )
			<< "Unexpected message received: 0x" << MLUCLog::kHex << (unsigned long)msg->fMsgType 
			<< "/" << MLUCLog::kDec << (unsigned long)msg->fMsgType 
			<< "." << ENDLOG;
		    break;
		}
	    fCom->ReleaseMsg( msg );
	    }
	}
    while ( !fQuitReceiveThread );
    fReceiveThreadRunning = false;
    }

bool TMSlaveControl::QuitReceiveLoop()
    {
    fQuitReceiveThread = true;
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 5000000;
    gettimeofday( &start, NULL );
    while ( fReceiveThreadRunning && deltaT<timeLimit )
	{
	usleep( 1000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    return !fReceiveThreadRunning;
    }


void TMSlaveControl::GetStates( MLUCString& states )
    {
    vector<TMConfig::StateData> stateList;
    vector<TMConfig::StateData>::iterator stateIter, stateEnd;
    vector<MLUCString>::iterator cmdIter, cmdEnd;
    pthread_mutex_lock( &fStateMutex );
    stateList = fStatesData;
    pthread_mutex_unlock( &fStateMutex );

    stateIter = stateList.begin();
    stateEnd = stateList.end();
    while ( stateIter != stateEnd )
	{
	states += stateIter->fStateName;
	states += ":";
	cmdIter = stateIter->fAllowedCommands.begin();
	cmdEnd = stateIter->fAllowedCommands.end();
	while ( cmdIter != cmdEnd )
	    {
	    MLUCString tmp( *cmdIter );
	    std::vector<MLUCString> parts;
	    tmp.Split( parts, ':' );
	    std::vector<MLUCString>::iterator iter, end;
	    iter = parts.begin();
	    end = parts.end();
	    if ( iter!=end )
		{
		tmp = *iter;
		++iter;
		}
	    while ( iter != end )
		{
		tmp += "\\:";
		tmp += *iter;
		++iter;
		}
	    states += tmp;
	    cmdIter++;
	    if ( cmdIter!=cmdEnd )
		states += ",";
	    }
	states += ";";
	stateIter++;
	}
    }

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
