Interface Library Functions:

/* Startup, initialize any static structures needed by the library. */
int Initialize( void** private_lib_data, const char* interrupt_listen_address );

/* End, cleanup of library data */
int Terminate( void* private_lib_data );

/* Query the state of a specific process 
** state is allocated as a string in the function and
** has to be released via ReleaseResource (below) */
int QueryState( void* private_lib_data,
                pid_t pid, 
                const char* process_id, 
                const char* process_address, 
                char** state );

/* Obtain additional status data from a
** specific process.
** stat_data is allocated as a string in the 
*+ function and has to be released via 
** ReleaseResource (below)
*/
int QueryStatusData( void* private_lib_data,
                   pid_t pid,
                   const char* process_id, 
                   const char* process_address, 
                   char** stat_data );

void ReleaseResource( void* private_lib_data, char* resource );

/* Send the specified command to the given process */
int SendCommand( void* private_lib_data,
                 pid_t pid, 
                 const char* process_id, 
                 const char* process_address, 
                 const char* command );

/* Callback function for the interrupt wait function below */
typedef void (*InterruptTriggerCallback_t)( void* opaque_arg, 
					    pid_t pid,
					    const char* process_id,
                                            const char* process_address,
                                            const char* notificationData );

/* Enter a wait that calls the specified callback when a process 
** has triggered an interrupt  (LAM).
** The address of the triggering process is passed in the call 
** to the specified callback function.
** This is called in its own thread to run as an endless loop
** and only returns when the StopInterruptWait function
** (see below) is called.. */
int InterruptWait( const char* listen_address, 
		   void* private_lib_data,
                   void* opaque_arg, 
                   InterruptTriggerCallback_t callback );

/* Stop the interrupt wait loop gracefully. */
int StopInterruptWait( void* private_lib_data );

/* Address normalization function to format string addresses into a common format 
   for reliable comaprisons. Returned strings/char pointers must be released via 
   ReleaseResource. The function returns NULL if the address cannot be
   normalized. */
char* NormalizeAddress( void* private_lib_data, const char* address );

/* Connection function.
   Establish a permanent connection to the given process.
   This function is optional and must not be supplied in the interface
   library for compatibility reasons.
*/
int ConnectToProcess( void* private_lib_data,
		      pid_t pid, 
		      const char* process_id, 
		      const char* process_address );

/* Disconnection function.
   Severe a previously established permanent connection to the given 
   process.
   This function is optional and must not be supplied in the interface
   library for compatibility reasons.
*/
int DisconnectFromProcess( void* private_lib_data,
			   pid_t pid, 
			   const char* process_id, 
			   const char* process_address );


Added Python Functions:

# Query the state of the given process
QueryState( process_id )

# Return the previous state of the given process
# (after state change)
# Obsolete since one can save it using SetVar
# QueryPreviousState( process_id )

# Obtain status data from the given process
QueryStatusData( process_id )

# Return list of process IDs of the last processes
# that have sent an interrupt (LAM)
# (After interrupt has been received)
# (List is reset after query)
# List is a list of lists, each sublist contains two 
# elements, id of the interrupting process and a 
# notification data string (can be empty).
GetLAMProcessList()

# Send a command to the given process
SendCommand( process_id, cmd )

# Send a command to the given process. 
# Trigger a simulated state change after timeout, if no 
# real state change has taken place before.

# Start the specified process, 
# command & parameters are taken from 
# configuration file
# Involves calling the pre- and post-start
# actions
StartProcess( process_id )

# Terminate the given process
# Involves calling the pre- and post-terminate
# actions
TerminateProcess( process_id )

# Cleanup the processes' specified resources
# Involves calling the cleanup
# action
CleanupProcessResources( process_id )

# Get the PID of the specified process
GetPID( process_id )

# Set the interval between calls of the polling 
# code.
SetPollInterval( intervall_usec )

# Set the global state of this task (string)
SetTaskState( state )

# Set the global status data of this task (string)
SetTaskStatusData( state )

# Save a python variable
SetVar( varname, varval )

# Retrieve a python variable
GetVar( varname )

# Get a received (slave control) command for this task
GetTaskCommand()

# Send a task global interrupt
# NotificationData may be None or a string.
TaskInterrupt( notificationData )

# Log a message
Log( level, msg )

# Set the state of the specified process to changed (triggers state action execution)
SetProcessStateChanged( processID )

# Terminate the current task and its TaskManager
TerminateTask()

# Establish a permament connection to the given process
Connect( process_id )

# Severe an established permanent connection to the given process
Disconnect( process_id )

# Reread the configuration file
ReadConfiguration()

# Get the state of the process during a config change (added, removed, changed, ...)
GetProcessConfigChangeState( process_id )

# Set the global logging verbosity.
SetVerbosity( verbosity )

# Suppress communication error log messages for the given time (microsecs., 0 makes permament)
SuppressComErrorLogs( pid, timeout_us )

# Yield the interpreter lock for the specified amount of time...
Yield( sleeptime_us )

# Specify whether this TM is a active or passive in an active/passive failover setup
SetFailOverActive( flag )

# Prepare the given process for termination by a command (will set some internal flags that it is ok for this process to not run)
PrepareProcessForTermination( pid )