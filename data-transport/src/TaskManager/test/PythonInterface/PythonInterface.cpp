/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TMPythonInterface.hpp"
#include "TMInterfaceLibrary.hpp"
#include "TMSysException.hpp"
#include "TMSystem.hpp"
#include "TMProcess.hpp"
#include <MLUCLog.hpp>
#include <MLUCLogServer.hpp>


int main( int, char** )
    {
    MLUCStdoutLogServer stdLog;
    gLog.AddServer( stdLog );

    return 0;
#if 0

    if ( argc < 2 )
	{
	LOG( MLUCLog::kError, "PythonInterface Test", "Usage" )
	    << "Usage: " << argv[0] << "<libraryname>" << "." << ENDLOG;
	return -1;
	}
    try
	{
	TMSystem sys( NULL );
	TMInterfaceLibrary lib( "Any", argv[1], true );
	TMMainPythonInterface python;
	TMProcess proc_0( "proc_0", 0 );
	TMProcess proc_1( "proc_1", 1 );
	TMProcess proc_2( "proc_2", 2 );

	sys.AddProcess( &proc_0 );
	sys.AddProcess( &proc_1 );
	sys.AddProcess( &proc_2 );

	sys.SetInterfaceLibrary( &lib );
	sys.SetPythonInterface( &python );
	python.SetSystem( &sys );

	if ( argc<3 )
	    {
	    python.Run( "print \"Hallo, Welt\"" );
	    python.Run( "print \"Hallo, Welt\"\nprint \"Hello, World!\"" );
	    python.Run( "import KIPTaskMan\nstate = KIPTaskMan.QueryState( \"proc_0\" )\nprint \"Process state: '\"+state+\"'\"");
	    char* pycmd = "import KIPTaskMan\n"\
		 "state = KIPTaskMan.QueryState( \"proc_0\" )\n"\
			  "print \"Process state: '\"+state+\"'\"\n"\
	    "statusdata = KIPTaskMan.QueryStatusData( \"proc_1\" )\n"\
			  "print \"Process status data: '\"+statusdata+\"'\"\n"\
	      "proclist = KIPTaskMan.GetLAMProcessList()\n"\
			  "print \"LAM process list: '\"+proclist+\"'\"\n"\
			  "KIPTaskMan.SendCommand( \"proc_2\", \"a cmd\" )\n"\
			  "KIPTaskMan.StartProcess( \"proc_0\" )\n"\
			  "KIPTaskMan.TerminateProcess( \"proc_0\" )\n"\
			  "KIPTaskMan.CleanupProcessResources( \"proc_0\" )\n"\
		   "pid = KIPTaskMan.GetPID( \"proc_0\" )\n"\
			  "print \"PID: \",pid\n"\
			  "KIPTaskMan.SetPollInterval( 100000 )\n"\
			  "KIPTaskMan.SetTaskState( \"OK\" )\n"\
			  "KIPTaskMan.SetVar( \"State\", \"OK\" )\n"\
	     "taskstate = KIPTaskMan.GetVar( \"State\" )\n"\
			  "print \"Task state: '\"+taskstate+\"'\"\n"
			  "KIPTaskMan.SetVar( \"State\", \"Broken\" )\n"\
	     "taskstate = KIPTaskMan.GetVar( \"State\" )\n"\
			  "print \"new task state: '\"+taskstate+\"'\"\n";
	    
	    python.Run( pycmd );
	    }
	else
	    python.Run( argv[2] );
	}
    catch ( TMInterfaceLibrary::TMInterfaceLibraryException e )
	{
	LOG( MLUCLog::kError, "PythonInterface Test", "Library Error" )
	    << "Library exception caught: " << e.GetErrorTypeName()
	    << " for " << e.GetName() << ": " << e.GetError() << " ("
	    << MLUCLog::kDec << e.GetErrno() << ")." << ENDLOG;
	}
    catch ( TMSysException e )
	{
	LOG( MLUCLog::kError, "PythonInterface Test", "System Error" )
	    << "System exception caught: " << e.GetErrorStr() << " ("
	    << MLUCLog::kDec << e.GetErrno() << ")." << ENDLOG;
	}
#endif    
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
