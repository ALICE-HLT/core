/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TMSystem.hpp"
#include "TMConfig.hpp"
#include "TMSysException.hpp"
#include <MLUCLog.hpp>
#include <MLUCLogServer.hpp>


int main( int, char** )
    {
    MLUCStdoutLogServer stdLog;
    gLog.AddServer( stdLog );

    return 0;
#if 0

    if ( argc < 2 )
	{
	LOG( MLUCLog::kError, "XMLConfigTest", "Usage" )
	    << "Usage: " << argv[0] << "<configuration-file-name>" << "." << ENDLOG;
	return -1;
	}

    try
	{
	TMSystem sys;
	TMConfig conf;

	conf.ReadConfig( argv[1] );
	}
    catch ( TMConfig::TMConfigException e )
	{
	LOG( MLUCLog::kError, "XMLConfigTest Test", "Configuration Error" )
	    << "Configuration exception caught: " << e.GetErrorTypeName()
	    << " for file '" << e.GetFilename() << "': " << e.GetError()
	    << "." << ENDLOG;
	}
    catch ( TMSysException e )
	{
	LOG( MLUCLog::kError, "XMLConfigTest Test", "System Error" )
	    << "System exception caught: " << e.GetErrorStr() << " ("
	    << MLUCLog::kDec << e.GetErrno() << ")." << ENDLOG;
	}

#endif    
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
