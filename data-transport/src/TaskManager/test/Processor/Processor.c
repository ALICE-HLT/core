/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <signal.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

void SignalHandler( int signum );

int gRun = 0;
int gQuit = 0;

#define PROCTIME 5000000LLU

int main( int argc, char** argv )
    {
    unsigned long long proctime = PROCTIME;
    struct timeval s, e;
    unsigned long long diff;
    char* cpErr;

    if ( argc>1 )
	{
	proctime = strtoul( argv[1], &cpErr, 0 );
	if ( *cpErr!=0 )
	    {
	    fprintf( stderr, "%s: Unable to convert '%s' to processing time.\n",
		     argv[0], argv[1] );
	    return -1;
	    }
	}
/*     printf( "Using proctime: %Lu\n", proctime ); */

    signal( SIGUSR1, SignalHandler );
    signal( SIGUSR2, SignalHandler );
    signal( SIGQUIT, SignalHandler );

    while ( !gQuit )
	{
	if ( gRun )
	    {
 	    printf( "Now Running\n" ); //fflush(stdout);
	    diff = 0;
	    gettimeofday( &s, NULL );
	    while ( !gQuit && gRun && diff<proctime )
		{
		gettimeofday( &e, NULL );
		diff = e.tv_sec-s.tv_sec;
		diff *= 1000000;
		diff += e.tv_usec-s.tv_usec;
/* 		printf( "diff: %Lu\n", diff ); */
		}
	    printf( "Now Sleeping\n" ); // fflush(stdout);
	    gRun = 0;
	    }
	while ( !gQuit && !gRun )
	    usleep( 100000 );
	}
    return 0;
    }


void SignalHandler( int signum )
    {
/*     printf( "Received signal %d\n", signum ); */
    if ( signum==SIGUSR1 )
	gRun = 1;
    else if ( signum==SIGUSR2 )
	gRun = 0;
    else
	gQuit = 1;
    signal( signum, SignalHandler );
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
