/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TMInterfaceLibrary.hpp"
#include <MLUCLog.hpp>
#include <MLUCLogServer.hpp>


void InterruptTriggerCallback( void*, 
			       pid_t,
			       const char*,
			       void*,
			       const char* )
    {
    }

int main( int argc, char** argv )
    {
    MLUCStdoutLogServer stdLog;
    gLog.AddServer( stdLog );
    char* resource;
    void* libdata;

    if ( argc < 2 )
	{
	LOG( MLUCLog::kError, "InterfaceLib Test", "Usage" )
	    << "Usage: " << argv[0] << "<libraryname>" << "." << ENDLOG;
	return -1;
	}
    try
	{
	TMInterfaceLibrary lib( "Any", argv[1], true );

	lib.Initialize( "tcpmsg://localhost:0815/" );
	lib.Terminate();
	void *addr;
	lib.ConvertAddress( "tcpmsg://localhost:4711/", &addr );
	lib.QueryState( 4711, "MyProcess1",
			addr,
			&resource );
	LOG( MLUCLog::kDebug, "InterfaceLib Test", "Queried state" )
	    << "Queried State: '" << resource << "'." << ENDLOG;
	lib.ReleaseResource( resource );
	lib.QueryStatusData( 4711, "MyProcess1",
			     addr,
			     &resource );
	LOG( MLUCLog::kDebug, "InterfaceLib Test", "Queried status data" )
	    << "Queried Status Data: '" << resource << "'." << ENDLOG;
	lib.ReleaseResource( resource );
	lib.SendCommand( 4711, "MyProcess1",
			 addr, 
			 "No Cmd" );
	lib.InterruptWait( (void*)42, "tcpmsg://localhost:0815/", InterruptTriggerCallback );
	lib.StopInterruptWait();
	libdata = lib.GetPrivateLibData();
	LOG( MLUCLog::kDebug, "InterfaceLib Test", "Private library data" )
	    << "Private Library Data: 0x" << MLUCLog::kHex << (unsigned long)libdata << " (" << MLUCLog::kDec
	    << (unsigned long)libdata << ")." << ENDLOG;
	lib.ReleaseResource( addr );
	}
    catch ( TMInterfaceLibrary::TMInterfaceLibraryException e )
	{
	LOG( MLUCLog::kError, "InterfaceLib Test", "Library Error" )
	    << "Library exception caught: " << e.GetErrorTypeName()
	    << " for " << e.GetName() << ": " << e.GetError() << " ("
	    << MLUCLog::kDec << e.GetErrno() << ")." << ENDLOG;
	}

    

    return 0;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
