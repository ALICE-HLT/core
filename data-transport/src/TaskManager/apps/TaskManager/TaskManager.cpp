/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TMPythonInterface.hpp"
#include "TMSystem.hpp"
#include "TMSysException.hpp"
#include "TMConfig.hpp"
#include "TMInterfaceLibrary.hpp"
#include <MLUCLog.hpp>
#include <MLUCLogServer.hpp>
#include <MLUCSysMESLogServer.hpp>
#include <MLUCDynamicLibraryLogServer.hpp>
#include <BCLAddressLogger.hpp>


int main( int argc, char** argv )
    {
    MLUCStdoutLogServer stdLog;
    gLog.AddServer( stdLog );
    bool nostdout = false;
    bool allowNoProcesses = false;
    bool sysmesLogging = false;
    bool infoLogger= false;

    if ( argc < 2 )
	{
	LOG( MLUCLog::kError, "TaskManager", "Usage" )
	    << "Usage: " << argv[0] << "<xml-configuration-file> (-V <verbosity) (-nostdout)" << "." << ENDLOG;
	return -1;
	}

    int i = 2;
    const char* errorStr=NULL;
    char* cpErr;
    char* infoLoggerLib=NULL;
    while ( i<argc && !errorStr )
	{
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing verbosity specifier.";
		break;
		}
	    gLogLevel = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Invalid verbosity specifier.";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-infologger" ) )
            {
            if (argc <= i+1)
                {
                errorStr = "Missing library path for infoLoggerLib.";
                }
            else
                {
                infoLoggerLib = argv[i+1];
                infoLogger = true;
                }
            i += 2;
            continue;
            }
	if ( !strcmp( argv[i], "-nostdout" ) )
	    {
	    nostdout = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-sysmeslog" ) )
	    {
	    sysmesLogging = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-allownoprocesses" ) )
	    {
	    allowNoProcesses = true;
	    i += 1;
	    continue;
	    }
	errorStr = "Unknown option";
	break;
	}

    if ( errorStr )
	{
	LOG( MLUCLog::kError, "TaskManager", "Usage" )
	    << "Usage: " << argv[0] << "<xml-configuration-file> (-V <verbosity) (-nostdout) (-sysmeslogging)" << "." << ENDLOG;
	LOG( MLUCLog::kError, "TaskManager", "Usage" )
	    << errorStr << ENDLOG;
	if ( i<argc )
	    {
	    LOG( MLUCLog::kError, "TaskManager", "Usage" )
		<< "Offending argument: '" << argv[i] << "'." << ENDLOG;
	    }
	return -1;
	}

    BCLAddressLogger::Init();
    
    MLUCString logfile;
    MLUCString tmps;
    char* tmpp;
    tmpp=argv[1];
    while ( *tmpp )
	{
	if ( *tmpp=='/' )
	    tmps = "";
	else
	    tmps += *tmpp;
	tmpp++;
	}
    logfile = "TaskManager-";
    logfile += tmps;
    logfile +="-";
    MLUCFileLogServer filelog( logfile.c_str(), ".log", 2500000 );
    gLog.AddServer( filelog );

    if ( nostdout )
	{
	gLog.DelServer( stdLog );
	}

    MLUCSysMESLogServer sysMESLogServer;
    if ( sysmesLogging )
	gLog.AddServer( sysMESLogServer );

    MLUCDynamicLibraryLogServer* infoLoggerLogServer = NULL;
    if (infoLogger)
	{
	MLUCString facility;
	facility = "TaskManager ";
	facility += tmpp;
        infoLoggerLogServer = new MLUCDynamicLibraryLogServer( infoLoggerLib, tmps.c_str() );
        gLog.AddServer( *infoLoggerLogServer );
	}
    
    try
	{
	TMSystem system( argv[1], allowNoProcesses );

	system.Run();

	LOG( MLUCLog::kInformational, "TaskManager", "Ending" )
	    << argv[0] << " ending..." << ENDLOG;
	if ( infoLoggerLogServer )
	    {
	    gLog.DelServer( *infoLoggerLogServer );
	    delete infoLoggerLogServer;
	    infoLoggerLogServer = NULL;
	    }
	return 0;
	}
    catch ( TMConfig::TMConfigException e )
	{
	LOG( MLUCLog::kError, "TaskManager", "Configuration Error" )
	    << "Configuration exception caught: " << e.GetErrorTypeName()
	    << " for file '" << e.GetFilename() << "': " << e.GetError()
	    << "." << ENDLOG;
	}
    catch ( TMSysException e )
	{
	LOG( MLUCLog::kError, "TaskManager", "System Error" )
	    << "System exception caught: " << e.GetErrorStr() << " ("
	    << MLUCLog::kDec << e.GetErrno() << "): "
	    << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
	    << e.GetLine() << ")" << "." << ENDLOG;
	}
    catch ( TMInterfaceLibrary::TMInterfaceLibraryException e )
	{
	LOG( MLUCLog::kError, "TaskManager", "Library Error" )
	    << "Library exception caught: " << e.GetErrorTypeName()
	    << " for " << e.GetName() << ": " << e.GetError() << " ("
	    << MLUCLog::kDec << e.GetErrno() << ")." << ENDLOG;
	}
    catch ( TMPythonInterface::TMPythonInterfaceException e )
	{
	LOG( MLUCLog::kError, "TaskManager", "Python Error" )
	    << "Python exception caught: " << e.GetErrorName()
	    << " (" << MLUCLog::kDec << e.GetError() << ")." << ENDLOG;
	}

    return -2;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
