/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TMControlMaster.hpp"
#include "TMSysException.hpp"
#include <MLUCLog.hpp>
#include <MLUCLogServer.hpp>
#include <MLUCString.hpp>
#include <MLUCThread.hpp>
#include <cerrno>
#include <cstdlib>

void DummyInterruptTriggerCallback( void*, 
				    pid_t,
				    const char*,
				    void*,
				    const char*)
    {
    }


class ControlThread: public MLUCThread
    {
    public:
	ControlThread( TMControlMaster* master )
		{
		fMaster = master;
		}

    protected:
	TMControlMaster* fMaster;
	virtual void Run()
		{
		if ( fMaster )
		    fMaster->RunLoop();
		}
    };

int main( int argc, char** argv )
    {
    MLUCStdoutLogServer stdLog;
    gLog.AddServer( stdLog );

    if ( argc!=5 )
	{
 	LOG( MLUCLog::kError, argv[0], "Usage" )
	    << "Usage: " << argv[0] << " <address> <slave-address-url> [ query [state|statusdata] | cmd <command> ]" << ENDLOG;
	exit( -1 );
	}

    MLUCString address( argv[1] );
    MLUCString slaveAddressString( argv[2] );
    MLUCString type( argv[3] );
    MLUCString param( argv[4] );

    if ( (type!="cmd" && type!="query") || 
	 (type=="query" && param!="state" && param!="statusdata" ) )
	{
 	LOG( MLUCLog::kError, argv[0], "Usage" )
	    << "Usage: " << argv[0] << " <slave-address-url> [ state | cmd <command> ]" << ENDLOG;
	exit( -1 );
	}

    try
	{
	TMControlMaster master;
	ControlThread thread( &master );

	master.SetArg( NULL );
	master.SetInterruptCallback( DummyInterruptTriggerCallback );
	master.SetAddress( address.c_str() );
	void* slaveAddress;
	master.ConvertAddress( slaveAddressString.c_str(), &slaveAddress );
	
	thread.Start();

	if ( type=="query" )
	    {
	    MLUCString reply;
	    if ( param=="state" )
		master.QueryState( slaveAddress, reply );
	    else if ( param=="statusdata" )
		master.QueryStatusData( slaveAddress, reply );
	    printf( "%s\n", reply.c_str() );
	    }
	else if ( type=="cmd" )
	    {
	    master.SendCmd( slaveAddress, param.c_str() );
	    }

	master.StopLoop();
	if ( master.IsLoopRunning() )
	    thread.Abort();
	else
	    thread.Join();
	master.ClearAddress();
	}
    catch ( TMSysException e )
	{
	LOG( MLUCLog::kError, argv[0], "System Error" )
	    << "System exception caught: " << e.GetErrorStr() << " ("
	    << MLUCLog::kDec << e.GetErrno() << "): "
	    << e.GetDescription() << " (" << e.GetFile() << "/" << MLUCLog::kDec
	    << e.GetLine() << ")" << "." << ENDLOG;
	return -2;
	}
    
    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
