/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "InterfaceLib.h"
#include "TMInterfaceLibrary.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

TMInterfaceLibrary* gInterface = NULL;

void InterruptTriggerCallback( void* /*opaque_arg*/, 
			       pid_t pid,
			       const char* process_id,
			       void* /*process_address_bin*/,
			       const char* notificationData)
    {
    const char* msg = notificationData;
    if ( !notificationData )
	msg = "";
    printf( "Interrupt received from process %d/%s: Notification data: %s\n", (int)pid, process_id, msg );
    }

void Signalhandler( int )
    {
    if ( gInterface )
	gInterface->StopInterruptWait();
    }

int main( int argc, char** argv )
    {
    if ( argc != 3 )
	{
	fprintf( stderr, "Usage: %s <interface-library> <interrupt-listen-address>\n", argv[0] );
	return -1;
	}

    TMInterfaceLibrary interface( "", argv[1] );
    gInterface = &interface;

    signal( SIGQUIT, Signalhandler );

    interface.Initialize( argv[2] );
    interface.InterruptWait( NULL, argv[2], InterruptTriggerCallback );
    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
