#ifndef _PROCESSEXECUTIONSTATEHANDLER_HPP_
#define _PROCESSEXECUTIONSTATEHANDLER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <pthread.h>
#include <sys/types.h>

class TMProcessExecutionStateHandler
    {
    public:

	enum ProcessStatus { kInvalid=-1, kReset=0, kStarted=1, kForkFailed=2, kExecFailed=4, kFailed=6, kKilled=8 };

	struct ProcessExecutionState
	    {
		unsigned long fNdx;
		pid_t fPID;
		ProcessStatus fStatus;
		int fError;
	    };

	TMProcessExecutionStateHandler( unsigned long procCnt );
	~TMProcessExecutionStateHandler();

	void SetProcessExecutionStatus( unsigned long proc_ndx, int error ); // Sets to kExecFailed
	void SetProcessExecutionStatus( pid_t pid, unsigned long proc_ndx, bool started, int error ); // Sets to kStarted or kForkFailed
	void SetProcessExecutionStatus( unsigned long proc_ndx ); // Sets to kKilled
	void ResetProcessExecutionStatus( unsigned long proc_ndx ); // Sets to kReset

	const ProcessExecutionState& GetProcessExecutionStatus( unsigned long proc_ndx );
	ProcessStatus GetProcessStatus( unsigned long proc_ndx );
	int GetProcessError( unsigned long proc_ndx );

	void Resize( unsigned long newProcCnt );

	void AddProcess( unsigned long proc_ndx );
	void DeleteProcess( unsigned long proc_ndx );
	void Compact();

    protected:

	unsigned long FindProcByNdx( unsigned long proc_index );

	unsigned long fProcessCnt;
	int fShmID;
	ProcessExecutionState* fStates;
	pthread_mutex_t fMutex;
	ProcessExecutionState fEmptyState;

    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _PROCESSEXECUTIONSTATEHANDLER_HPP_
