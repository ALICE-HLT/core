#ifndef _TMCONTROLMASTER_HPP_
#define _TMCONTROLMASTER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "InterfaceLib.h"
#include <MLUCString.hpp>
#include <MLUCConditionSem.hpp>
#include <MLUCMutex.hpp>
#include <BCLAbstrAddress.hpp>
#include <BCLMessage.hpp>
#include <BCLMsgCommunication.hpp>
#include <pthread.h>
#include <vector>
#include <sys/time.h>


using namespace std;

class TMControlMaster
    {
    public:

	TMControlMaster();
	~TMControlMaster();

	void SetInterruptCallback( InterruptTriggerCallback_t cb )
		{
		fCallback = cb;
		}
	void SetArg( void* arg )
		{
		fArg = arg;
		}

	void SetAddress( const char* addr );
	void ClearAddress();

	void RunLoop();
	void StopLoop();
	bool IsLoopRunning() const
		{
		return fLoopRunning;
		}

	void QueryState( void* address, MLUCString& state );
	void QueryStatusData( void* address, MLUCString& status );
	void QueryChildren( void* address, MLUCString& children );
	void QueryStates( void* address, MLUCString& states );
	void QueryChildStates( void* address, const char* id, MLUCString& states );
	void SendCmd( void* address, const char* cmd );
	void Connect( void* address );
	void Disconnect( void* address );

	void ConvertAddress( const char* process_address_str, void** process_address_bin );
	int CompareAddresses( void* addr1, void* addr2 );

	void SuppressComErrorLogs( void* address, unsigned long timeout_us );

	bool SuppressComErrorLogs( void* address );

	bool ReportMissingReplyError( void* address );

    protected:

	void CleanupOldReplies(); // Must be called with fReplySignal locked


	bool fRunLoop;
	bool fLoopRunning;

	pthread_mutex_t fComSendMutex;
	BCLMsgCommunication* fCom;
	BCLAbstrAddressStruct* fLocalAddress;
	BCLAbstrAddressStruct* fReceiveAddress;

	InterruptTriggerCallback_t fCallback;
	void* fArg;

	MLUCString fAddress;

	struct ReplyStruct
	    {
		unsigned long long fReplyID;
		MLUCString fReply;
		struct timeval fTimestamp; // Timestamp, anything older than 10min (currently) is removed.
	    };

	MLUCConditionSem<uint8> fReplySignal;
	uint64 fLastReplyID;
// 	pthread_mutex_t fReplyMutex;
	vector<ReplyStruct> fReplies;

	struct timeval fLastThoroughReplyCleanup;


	struct TComErrorLogSuppressionData
	    {
		BCLAbstrAddressStruct* fAddress;
		struct timeval fUntil;
	    };
	vector<TComErrorLogSuppressionData> fComErrorLogSuppressions;
	pthread_mutex_t fComErrorLogSuppressionsMutex;

	struct TMissingReplyData
	    {
		TMissingReplyData( void* address ):
		    fAddress(address),
		    fCnt(1)
			{
			}
		void* fAddress;
		unsigned fCnt;
	    };
	vector<TMissingReplyData> fMissingReplyData;
	MLUCMutex fMissingReplyDataMutex;

    };


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _TMCONTROLMASTER_HPP_
