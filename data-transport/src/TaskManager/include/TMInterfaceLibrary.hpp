#ifndef _TMINTERFACELIBRARY_HPP_
#define _TMINTERFACELIBRARY_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <MLUCDynamicLibrary.hpp>
#include <MLUCString.hpp>
#include <exception>
#include <sys/types.h>

using namespace std;

typedef void (*TMInterruptTriggerCallback_t)( void* opaque_arg, 
					      pid_t pid,
					      const char* process_id,
					      void* process_address_bin,
					      const char* notificationData );

class TMInterfaceLibrary
    {
    public:

	class TMInterfaceLibraryException: public exception
	    {
	    public:

		enum ErrorType { kUnknownError, kLibraryOpenError,
				 kFunctionLoadError, kInvalidFunctionError };
		
		TMInterfaceLibraryException( ErrorType type, const char* name,
					     int this_errno, const char* this_error ) throw()
			{
			fType = type;
			fName = name;
			fThisErrno = this_errno;
			fThisError = this_error;
			}

		virtual ~TMInterfaceLibraryException() throw()
			{
			}

		ErrorType GetErrorType() const throw()
			{
			return fType;
			}

		const char* GetErrorTypeName() const throw()
			{
			switch ( fType )
			    {
			    case kUnknownError:
				return "Unknown Error";
			    case kLibraryOpenError:
				return "Library Open Error";
			    case kFunctionLoadError:
				return "Function Load Error";
			    case kInvalidFunctionError:
				return "Invalid Function Error";
			    default:
				return "Invalid Error Specifier (Internal Error)";
			    }
			}

		const char* GetName() const throw()
			{
			return fName.c_str();
			}

		int GetErrno() const throw()
			{
			return fThisErrno;
			}

		const char* GetError() const throw()
			{
			return fThisError.c_str();
			}

	    protected:
		ErrorType fType;
		MLUCString fName;
		int fThisErrno;
		MLUCString fThisError;

	    };

	TMInterfaceLibrary( const char* archName, const char* libName, bool log = true );
	~TMInterfaceLibrary();

	void SetLogging( bool log )
		{
		fLogging = log;
		}

	const char* GetPlatform() const
		{
		return fArchName.c_str();
		}
	const char* GetBinary() const
		{
		return fLibName.c_str();
		}

	int Initialize( const char* interrupt_listen_address );

	int Terminate();

	int ConvertAddress( const char* process_address_str, void** process_address_bin );

	int CompareAddresses( void* addr1, void* addr2 );

	int QueryState( pid_t pid, const char* process_id,
			void* process_address_bin, 
			char** state );

	int QueryStatusData( pid_t pid, const char* process_id, 
			     void* process_address_bin, 
			     char** stat_data );

	void ReleaseResource( void* resource );

	int SendCommand( pid_t pid, const char* process_id,
			 void* process_address_bin,
			 const char* command );

	int InterruptWait( void* opaque_arg, 
			   const char* listen_address, 
			   TMInterruptTriggerCallback_t callback );

	bool InterruptWaitRunning() const
		{
		return fInterruptWaitRunning;
		}

	int StopInterruptWait();

	int ConnectToProcess( pid_t pid, 
			      const char* process_id, 
			      void* process_address_bin );
	
	int DisconnectFromProcess( pid_t pid, 
				   const char* process_id, 
				   void* process_address_bin );

	int SuppressComErrorLogs( pid_t pid, 
				  const char* process_id, 
				  void* process_address_bin,
				  unsigned long timeout_us );


	void* GetPrivateLibData();

    protected:

	MLUCString fArchName;
	MLUCString fLibName;
	MLUCDynamicLibrary fLibrary;
	void* fPrivateLibData;

	bool fLogging;

	bool fInterruptWaitRunning;

	typedef int (*InitializeFunc)( void** private_lib_data, const char* interrupt_listen_address );
	typedef int (*TerminateFunc)( void* private_lib_data );
	typedef int (*ConvertAddressFunc)( void* private_lib_data, const char* process_address_str, void** process_address_bin );
	typedef int (*CompareAddressesFunc)( void* private_lib_data, void* addr1, void* addr2 );
	typedef int (*QueryStateFunc)( void* private_lib_data,
				       pid_t pid, const char* process_id,
				       void* process_address_bin, 
				       char** state );
	typedef int ( *QueryStatusDataFunc)( void* private_lib_data,
					     pid_t pid, const char* process_id, 
					     void* process_address_bin, 
					     char** stat_data );
	typedef void (*ReleaseResourceFunc)( void* private_lib_data, void* resource );
	typedef int (*SendCommandFunc)( void* private_lib_data,
					pid_t pid, const char* process_id,
					void* process_address_bin,
					const char* command );
	typedef int (*InterruptWaitFunc)( void* private_lib_data, void* opaque_arg, 
					  const char* listen_address, 
					  TMInterruptTriggerCallback_t callback );
	typedef int (*StopInterruptWaitFunc)( void* private_lib_data );
	typedef int (*ConnectToProcessFunc)( void* private_lib_data,
					     pid_t pid, 
					     const char* process_id, 
					     void* process_address_bin );
	typedef int (*DisconnectFromProcessFunc)( void* private_lib_data,
						  pid_t pid, 
						  const char* process_id, 
						  void* process_address_bin );
	typedef int (*SuppressComErrorLogsFunc)( void* private_lib_data,
						 pid_t pid, 
						 const char* process_id, 
						 void* process_address_bin,
						 unsigned long timeout_us );


	
	InitializeFunc fInitialize;
	TerminateFunc fTerminate;
	ConvertAddressFunc fConvertAddress;
	CompareAddressesFunc fCompareAddresses;
	QueryStateFunc fQueryState;
	QueryStatusDataFunc fQueryStatusData;
	ReleaseResourceFunc fReleaseResource;
	SendCommandFunc fSendCommand;
	InterruptWaitFunc fInterruptWait;
	StopInterruptWaitFunc fStopInterruptWait;
	ConnectToProcessFunc fConnectToProcess;
	DisconnectFromProcessFunc fDisconnectFromProcess;
	SuppressComErrorLogsFunc fSuppressComErrorLogs;
	

	static void InterruptWaitCleanupFuncWrapper( void* arg );
	void InterruptWaitCleanupFunc();
	
    };


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _TMINTERFACELIBRARY_HPP_
