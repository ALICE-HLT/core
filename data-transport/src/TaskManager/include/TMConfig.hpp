#ifndef _TMCONFIG_HPP_
#define _TMCONFIG_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <MLUCTypes.h>
#include <MLUCString.hpp>
#include <exception>
#include <vector>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/dom/DOMNode.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>


using namespace XERCES_CPP_NAMESPACE;
using namespace std;

class TMSystem;
class TMInterfaceLibrary;

class TMConfig
    {
    public:

	struct InterfaceLibraryData
	    {
		bool fActive;
		MLUCString fPlatform;
		MLUCString fBinary;
	    };

	struct PythonExtensionData
	    {
		bool fActive;
		MLUCString fPlatform;
		MLUCString fModule;
		MLUCString fBinary;
	    };

	struct StateSupervisionConfigData
	    {
		unsigned long fPollInterval; // In microsec.; for main loop
		bool fPollIntervalSet;
		bool fFailOverActive; // Active (true) or passive (false) in multi-TM failover scenario
	        MLUCString fInterruptListenAddress;
		MLUCString fStartupAction;
		MLUCString fStateChangeAction;
		MLUCString fInterruptReceivedAction;
		MLUCString fPeriodicPollAction;
		unsigned long fPeriodicPollInterval; // In microsec. 
		MLUCString fLoopAction;
		MLUCString fLoopStopAction;
		MLUCString fPreConfigChangePhase1Action;
		MLUCString fPreConfigChangePhase2Action;
		MLUCString fPreConfigChangePhase3Action;
		MLUCString fPostConfigChangeAction;
	    };

	struct UsedResourceData
	    {
		MLUCString fType;
		MLUCString fContent;
	    };

	struct ProcessStateData
	    {
		MLUCString fStateNamePattern;
		vector<MLUCString> fAllowedCommands;
	    };

	struct ProcessData
	    {
		enum TType { kUnknown=0, kProgram, kSlave, kSlaveProgram, kProxy };
		MLUCString fID;
		TType fType;
		MLUCString fAddress;
		MLUCString fCommand;
		MLUCString fStateChangeAction;
		MLUCString fPreStartAction;
		MLUCString fPostStartAction;
		MLUCString fPreTerminateAction;
		MLUCString fPostTerminateAction;
		MLUCString fInterruptReceivedAction;
		MLUCString fPreConfigChangePhase1Action;
		MLUCString fPreConfigChangePhase2Action;
		MLUCString fPreConfigChangePhase3Action;
		MLUCString fPostConfigChangeAction;
		vector<UsedResourceData> fUsedResources;
		MLUCString fCleanupAction;
		MLUCString fFailOverPassiveKillAction;
		MLUCString fFailOverPassiveMonitorCommand;
		InterfaceLibraryData fInterfaceLibrary;
	        MLUCString fInterruptListenAddress;
		bool fInterruptListenAddressActive;
		vector<ProcessStateData> fStatesData;
	    };

	struct StateData
	    {
		MLUCString fStateName;
		vector<MLUCString> fAllowedCommands;
	    };

	struct SlaveControlConfigData
	    {
		bool fActive;
		bool fAutonomous;
		MLUCString fAddress;
		MLUCString fCommandReceivedAction;
		vector<MLUCString> fInterruptTargetAddresses;
		vector<StateData> fStatesData;
	    };

	class TMConfigException: public exception
	    {
	    public:

		enum ErrorType { kUnknownError, kXMLInitError,
				 kXMLDOMError, kXMLError, kConfigurationError };
		
		TMConfigException( ErrorType type, const char* error ) throw()
			{
			fType = type;
			fThisError = error;
			}
		TMConfigException( ErrorType type, const char* error, const char* filename ) throw()
			{
			fType = type;
			fThisError = error;
			fFilename = filename;
			}

		virtual ~TMConfigException() throw()
			{
			}

		ErrorType GetErrorType() const throw()
			{
			return fType;
			}

		const char* GetErrorTypeName() const throw()
			{
			switch ( fType )
			    {
			    case kUnknownError:
				return "Unknown Error";
			    case kXMLInitError:
				return "XML Initialization Error";
			    case kXMLDOMError:
				return "XML DOM Error";
			    case kXMLError:
				return "XML Error";
			    case kConfigurationError:
				return "XML Configuration File Error";
			    default:
				return "Invalid Error Specifier (Internal Error)";
			    }
			}

		const char* GetError() const throw()
			{
			return fThisError.c_str();
			}

		const char* GetFilename() const throw()
			{
			return fFilename.c_str();
			}

	    protected:
		ErrorType fType;
		MLUCString fThisError;
		MLUCString fFilename;

	    };

	TMConfig();
	~TMConfig();

	void ReadConfig( const char* filename, bool allowNoProcesses=false );

	const InterfaceLibraryData& GetInterfaceLibraryData() const
		{
		return fInterfaceLibraryData;
		}

	const StateSupervisionConfigData& GetStateSupervisionConfigData() const
		{
		return fStateSupervisionConfigData;
		}

	const vector<ProcessData>& GetProcessData() const
		{
		return fProcesses;
		}

	const SlaveControlConfigData& GetSlaveControlConfigData() const
		{
		return fSlaveControlConfigData;
		}

	const vector<PythonExtensionData>& GetPythonExtensionData() const
		{
		return fPythonExtensions;
		}

    protected:

	void ParseFile( const char* filename, DOMDocument*& doc );
	void GetTask( const char* filename, DOMDocument* doc, DOMNode*& task, MLUCString& taskName );
	void GetInterfaceLibrary( const char* filename, DOMNode* taskNode, InterfaceLibraryData& lib, bool topLevelLib = true );
	void GetStateSupervisionConfig( const char* filename, DOMNode* taskNode, StateSupervisionConfigData& conf );
	void GetProcesses( const char* filename, DOMNode* taskNode, vector<ProcessData>& processes, bool allowNoProcesses );
	void GetSlaveControlConfig( const char* filename, DOMNode* taskNode, SlaveControlConfigData& conf );
	void GetPythonExtensions( const char* filename, DOMNode* taskNode, vector<PythonExtensionData>& extensions );

	bool GetAttribute( const char* filename, DOMNode* node, const char* attrName, MLUCString& attrValue, bool canFail=false );
	void GetTextContent( DOMNode* node, MLUCString& content );
	void GetNodeName( DOMNode* node, MLUCString& name );
	void GetChildNode( const char* filename, DOMNode* node, const char* childNodeName, DOMNode*& child, bool allowFail=false ); // if child is NULL finds the first one, otherwise the succeeding.
	unsigned long GetChildNodeCnt( const char* filename, DOMNode* node, const char* childNodeName );
	void CheckChildNodeCnt( const char* filename, DOMNode* node, const char* childNodeName, unsigned long allowedCnt, const char* nodeSpec=NULL, const char* location=NULL );
	void CheckChildNodeCnt( const char* filename, DOMNode* node, const char* childNodeName, unsigned long minAllowedCnt, unsigned long maxAllowedCnt, const char* nodeSpec=NULL, const char* location=NULL );
	void DumpTags( const char* filename, DOMDocument* doc );

#ifdef XERCESC_2
	DOMImplementation *fDOMImpl;
	DOMBuilder* fParser;
#else
	XercesDOMParser* fParser;
#endif

	InterfaceLibraryData fInterfaceLibraryData;
	StateSupervisionConfigData fStateSupervisionConfigData;
	vector<ProcessData> fProcesses;
	SlaveControlConfigData fSlaveControlConfigData;
	vector<PythonExtensionData> fPythonExtensions;

    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _TMCONFIG_HPP_
