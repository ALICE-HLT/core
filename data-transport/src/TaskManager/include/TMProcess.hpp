#ifndef _TMPROCESS_HPP_
#define _TMPROCESS_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TMUsedResource.hpp"
#include "TMConfig.hpp"
#include <MLUCString.hpp>
#include <MLUCTimer.hpp>
#include <MLUCMutex.hpp>
#include <vector>
#include <pthread.h>

using namespace std;

//class TMPythonInterface;
class TMSystem;
class TMInterruptWaitThread;

class TMProcess
    {
    public:
	TMProcess( TMSystem* sys, const char* id, unsigned long proc_ndx, TMConfig::ProcessData& data );
	~TMProcess();

// 	void Enable()
// 		{
// 		fEnabled = true;
// 		}
// 	void Disable()
// 		{
// 		fEnabled =false;
// 		}
// 	bool IsEnabled() const
// 		{
// 		return fEnabled;
// 		}

	void SetSlave( bool isSlave )
		{
		fIsSlave = isSlave;
		}
	void SetProgram( bool isProgram )
		{
		fIsProgram = isProgram;
		}
	bool IsProgram() const
		{
		return fIsProgram;
		}
	bool IsSlave() const
		{
		return fIsSlave;
		}

	unsigned long GetNdx() const
		{
		return fProcNdx;
		}

	const char* GetID() const
		{
		return fID.c_str();
		}

	void SetAddressFromString( const char* addrStr );
	void SetAddress( const char* addrStr, void* addr );
// 		{
// 		fAddress = addr;
// 		}
	const char* GetAddressString() const
		{
		return fAddressString.c_str();
		}
	void* GetAddress() const
		{
		return fAddress;
		}

	bool SetCommand( const char* cmd ); // Returns false when command could not be parsed.
	const char* GetCommand() const
		{
		return fCmd.c_str();
		}

	void SetStateChangeAction( const char* sca )
		{
		fStateChangeAction = sca;
		}
	const char* GetStateChangeAction() const
		{
		return fStateChangeAction.c_str();
		}

	void SetPreStartAction( const char* act )
		{
		fPreStartAction = act;
		}
	const char* GetPreStartAction() const
		{
		return fPreStartAction.c_str();
		}

	void SetPostStartAction( const char* act )
		{
		fPostStartAction = act;
		}
	const char* GetPostStartAction() const
		{
		return fPostStartAction.c_str();
		}

	void SetPreTerminateAction( const char* act )
		{
		fPreTerminateAction = act;
		}
	const char* GetPreTerminateAction() const
		{
		return fPreTerminateAction.c_str();
		}

	void SetPostTerminateAction( const char* act )
		{
		fPostTerminateAction = act;
		}
	const char* GetPostTerminateAction() const
		{
		return fPostTerminateAction.c_str();
		}

	void SetInterruptReceivedAction( const char* act )
		{
		fInterruptReceivedAction = act;
		}
	const char* GetInterruptReceivedAction() const
		{
		return fInterruptReceivedAction.c_str();
		}

	void SetPreConfigChangePhase1Action( const char* act )
		{
		fPreConfigChangePhase1Action = act;
		}
	const char* GetPreConfigChangePhase1Action() const
		{
		return fPreConfigChangePhase1Action.c_str();
		}

	void SetPreConfigChangePhase2Action( const char* act )
		{
		fPreConfigChangePhase2Action = act;
		}
	const char* GetPreConfigChangePhase2Action() const
		{
		return fPreConfigChangePhase2Action.c_str();
		}

	void SetPreConfigChangePhase3Action( const char* act )
		{
		fPreConfigChangePhase3Action = act;
		}
	const char* GetPreConfigChangePhase3Action() const
		{
		return fPreConfigChangePhase3Action.c_str();
		}

	void SetPostConfigChangeAction( const char* act )
		{
		fPostConfigChangeAction = act;
		}
	const char* GetPostConfigChangeAction() const
		{
		return fPostConfigChangeAction.c_str();
		}

	void SetFailOverPassiveKillAction( const char* act )
		{
		fFailOverPassiveKillAction = act;
		}
	const char* GetFailOverPassiveKillAction() const
		{
		return fFailOverPassiveKillAction.c_str();
		}

	void SetUsedResources( const vector<TMUsedResourceData*>& res );
	void GetUsedResources( vector<TMUsedResourceData*>& res );
	void FreeUsedResourcesData(); // Free the C++ data structures, not the resources described by them!!


	void SetCleanupAction( const char* sca )
		{
		fCleanupAction = sca;
		}
	const char* GetCleanupAction() const
		{
		return fCleanupAction.c_str();
		}

	void SetConnectionRequested( bool req )
		{
		fConnectionRequested = req;
		}
	bool GetConnectionRequested() const
		{
		return fConnectionRequested;
		}
	void SetConnectionEstablished( bool req )
		{
		fConnectionEstablished = req;
		}
	bool GetConnectionEstablished() const
		{
		return fConnectionEstablished;
		}

	void ForceStateChange()
		{
		MLUCMutex::TLocker stateDataLocker( fStateDataMutex );
		fStateChangeForced = true;
		}
	void ForceStateChangeTimeout( unsigned long timeout_us );
      
	void StateChange();
	void Interrupt();
      
	void Start();

	pid_t GetPID()
		{
		MLUCMutex::TLocker stateDataLocker( fStateDataMutex );
		return fPID;
		}

	void SetTerminationFlag( bool flag = true )
		{
		MLUCMutex::TLocker stateDataLocker( fStateDataMutex );
		fTerminationFlag = flag;
		}
	bool GetTerminationFlag() const
		{
		return fTerminationFlag;
		}
	bool IsStarted() const
		{
		return fStarted;
		}
	bool IsStartedBySelf() const
		{
		return fStartedBySelf;
		}

	bool Kill( bool minus9 = false );

	bool Cleanup();

	bool IsRunning()
		{
		MLUCMutex::TLocker stateDataLocker( fStateDataMutex );
		return fPID!=-1;
		}
	//bool HasTerminated( int& retval );
	void QueryOldState( MLUCString& state )
		{
		MLUCMutex::TLocker stateDataLocker( fStateDataMutex );
		state = fOldState;
		}
	void CheckState( MLUCString& state );
	void CheckState( bool& hasChanged, bool& running, bool& normalExit, bool& signalExit, int& retval, MLUCString& state, bool store=true );
	void GetStatusData( MLUCString& statusData );
	void Reset();

	// 	void SetPythonInterface( TMPythonInterface* pint )
	// 		{
	// 		fInterface = pint;
	// 		}

	void ConfigChangingPhase1();
	void ConfigChangingPhase2();
	void ConfigChangingPhase3();
	void ConfigChanged();

	void Set( TMConfig::ProcessData& data );

	TMInterruptWaitThread* GetInterruptWaitThread();

	void GetStates( MLUCString& states );

	void SuppressComErrorLogs( unsigned long timeout );

	bool SuppressComErrorLogs();

	int SendCommand( const char* cmd, unsigned long timeout );



    protected:

	bool ParseCmd();

	TMInterfaceLibrary* GetInterfaceLibrary();

	bool fEnabled;
	bool fIsProgram;
	bool fIsSlave;
	bool fIsProxy;

	MLUCString fID;

	unsigned long fProcNdx;

	pid_t fPID;
	int fRetVal;
	bool fNormalExit;
	bool fSignalExit;
	bool fStarted;

	vector<TMUsedResourceData*> fUsedResources;

	MLUCString fCmd;
	vector<MLUCString> fCmdTokens;

	MLUCString fAddressString;
	void* fAddress;

	MLUCString fStateChangeAction;
	MLUCString fPreStartAction;
	MLUCString fPostStartAction;
	MLUCString fPreTerminateAction;
	MLUCString fPostTerminateAction;
	MLUCString fInterruptReceivedAction;
	MLUCString fPreConfigChangePhase1Action;
	MLUCString fPreConfigChangePhase2Action;
	MLUCString fPreConfigChangePhase3Action;
	MLUCString fPostConfigChangeAction;
	MLUCString fCleanupAction;
	MLUCString fFailOverPassiveKillAction;

	MLUCString fFailOverPassiveMonitorCommand;

	// 	TMPythonInterface* fInterface;
	
	TMSystem* fSystem;

	TMInterfaceLibrary* fInterfaceLibrary;

	TMInterruptWaitThread* fInterruptWaitThread;

	MLUCMutex fStateDataMutex;
      
	MLUCString fOldState;

	bool fStateChangeForced;

	class TStateChangeTimeoutCallback: public MLUCTimerCallback
	    {
	    public:
		TStateChangeTimeoutCallback( TMProcess* proc )
			{
			fProc = proc;
			}
		
		virtual void TimerExpired( uint64 notData );

	    protected:
		TMProcess* fProc;
		
	    };

	TStateChangeTimeoutCallback fStateChangeTimeoutCallback;
	uint32 fStateChangeTimeoutID;
	bool fStateChangeTimeoutSet;
	pthread_mutex_t fStateChangeTimeoutMutex;

	bool fConnectionRequested;
	bool fConnectionEstablished;

	vector<TMConfig::ProcessStateData> fStatesData;
	pthread_mutex_t fStatesMutex;

	struct timeval fComErrorsSuppressionTimeout;
	pthread_mutex_t fComErrorsSuppressionMutex;

	bool fStartedBySelf;
	bool fTerminationFlag;
	bool fWasRunning;

	MLUCMutex fFailOverPassiveMonitorMutex;
	pid_t fFailOverPassiveMonitorPID;

	unsigned fReplyTimeoutRepeatCnt;

	int fStdout;
	MLUCString fStdoutBuffer;
	int fStderr;
	MLUCString fStderrBuffer;

    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _TMPROCESS_HPP_
