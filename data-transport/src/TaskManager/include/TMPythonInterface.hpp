#ifndef _TMPYTHONINTERFACE_HPP_
#define _TMPYTHONINTERFACE_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <Python.h>
#include <MLUCString.hpp>
#include <MLUCMutex.hpp>
#include <patchlevel.h>
#include <pthread.h>
#include <vector>

#define TM_PYTHON_LOCAL_DICTIONARIES
#define TM_PYTHON_THREAD_PRIVATE_INTERPRETERS

using namespace std;

class TMSystem;
class TMMainPythonInterface;

class TMPythonInterface
    {
    public:

	class TMPythonInterfaceException: public exception
	    {
	    public:

		enum ErrorType { kUnknownError, kMainInterfaceDeclaredError, 
				 kWrongInterfaceError, kNoInterfaceDeclaredError,
				 kNoInterfaceSpecifiedError, kNoThreadInterface };

		TMPythonInterfaceException( ErrorType type ) throw()
			{
			fType = type;
			}
		~TMPythonInterfaceException() throw()
			{
			}

		ErrorType GetError() const throw()
			{
			return fType;
			}

		const char* GetErrorName() const throw()
			{
			switch ( fType )
			    {
			    case kUnknownError:
				return "Unknown Error";
			    case kMainInterfaceDeclaredError:
				return "Main Interface Already Declared";
			    case kWrongInterfaceError:
				return "Wrong Interface";
			    case kNoInterfaceDeclaredError:
				return "No Interface Declared";
			    case kNoInterfaceSpecifiedError:
				return "No Interface Specified";
			    case kNoThreadInterface:
				return "No Interface Specified For This Thread";
			    default:
				return "Unexpected Error";
			    }
			}

	    protected:
		ErrorType fType;
	    };

 	virtual ~TMPythonInterface();

	void Run( const char* action );

	void SetSystem( TMSystem* sys )
		{
		fSystem = sys;
		}

	TMSystem* GetSystem()
		{
		return fSystem;
		}

	static TMPythonInterface* GetThreadInterface();

	virtual void Cleanup();

    protected:

	TMPythonInterface( TMSystem* sys );

        static bool Init( TMMainPythonInterface* interface );
	static bool Deinit( TMMainPythonInterface* interface );
	static void RegisterThreadInterface( TMPythonInterface* );
	static void UnregisterThreadInterface( TMPythonInterface* );
					   

	// Object is passed in as borrowed reference. 
	// Ownership is not transferred
	static void SetVar( const char* name, PyObject* data );
	// Object is returned as a owned !! reference.
	// Ownership is transferred.
	static void GetVar( const char* name, PyObject*& data );

	void LogPythonError();
 
	TMSystem* fSystem;

	static TMMainPythonInterface* gInterface;
	//static pthread_mutex_t gMutex;
	//static pthread_t gThreadID;
#ifndef TM_PYTHON_LOCAL_DICTIONARIES
	static PyObject* gModule;
	static PyObject* gDict;
#else
	PyObject* fModule;
	PyObject* fDict;
#endif
	static PyInterpreterState* gInterpreterState;

	struct ThreadInterfaceData
	    {
		pthread_t fThreadID;
		TMPythonInterface* fInterface;
	    };

	static vector<ThreadInterfaceData> gThreadInterfaces;
	static pthread_mutex_t gThreadInterfaceMutex;


#define PYMETHODCNT 29
	// Number of Python methods define below.
	// When you add new methods here remember to increase this number.


	static PyObject* QueryState( PyObject* self, PyObject* arg );
	// 	static PyObject* QueryPreviousState( PyObject* self, PyObject* arg );
	static PyObject* QueryStatusData( PyObject* self, PyObject* arg );
	static PyObject* GetLAMProcessList( PyObject* self, PyObject* arg );
	static PyObject* SendCommand( PyObject* self, PyObject* arg );
	static PyObject* StartProcess( PyObject* self, PyObject* arg );
	static PyObject* TerminateProcess( PyObject* self, PyObject* arg );
	static PyObject* CleanupProcessResources( PyObject* self, PyObject* arg );
	static PyObject* GetPID( PyObject* self, PyObject* arg );
	static PyObject* SetPollInterval( PyObject* self, PyObject* arg );
	static PyObject* SetTaskState( PyObject* self, PyObject* arg );
	static PyObject* SetTaskStatusData( PyObject* self, PyObject* arg );
	static PyObject* SetVar( PyObject* self, PyObject* arg );
	static PyObject* GetVar( PyObject* self, PyObject* arg );
	static PyObject* GetTaskCommand( PyObject* self, PyObject* arg );
	static PyObject* TaskInterrupt( PyObject* self, PyObject* arg );
	static PyObject* Log( PyObject* self, PyObject* arg );
	static PyObject* SetProcessStateChanged( PyObject* self, PyObject* arg );
	static PyObject* TerminateTask( PyObject* self, PyObject* arg );
	static PyObject* Connect( PyObject* self, PyObject* arg );
	static PyObject* Disconnect( PyObject* self, PyObject* arg );
	static PyObject* ReadConfiguration( PyObject* self, PyObject* arg );
	static PyObject* GetProcessConfigChangeState( PyObject* self, PyObject* arg );
	static PyObject* Reset( PyObject* self, PyObject* arg );
	static PyObject* SetVerbosity( PyObject* self, PyObject* arg );
	static PyObject* SuppressComErrorLogs( PyObject* self, PyObject* arg );
      	static PyObject* SuppressComErrorLogsQuery( PyObject* self, PyObject* arg );
	static PyObject* Yield( PyObject* self, PyObject* arg );
	static PyObject* SetFailOverActive( PyObject* self, PyObject* arg );
	static PyObject* PrepareProcessForTermination( PyObject* self, PyObject* arg );
	// remember to increase PYMETHODCNT above.

	PyThreadState* fThreadState;

	unsigned int fRunCount;

	static PyMethodDef* gInterfaceMethods;

	struct PythonVariableData
	    {
		MLUCString fName;
		PyObject* fValue;
	    };
	static pthread_mutex_t gVariableMutex;
	static vector<PythonVariableData> gVariables;


	static MLUCMutex gRunMutex;
	static MLUCMutex::TLocker gRunMutexLocker;

	
    };

class TMMainPythonInterface: public TMPythonInterface
    {
    public:
	TMMainPythonInterface( TMSystem* sys );
	virtual ~TMMainPythonInterface();
    };


class TMSubThreadPythonInterface: public TMPythonInterface
    {
    public:
	// Objects of this class can only be created after TMPythonInterface::Init has been called.

	TMSubThreadPythonInterface( TMSystem* sys );
	virtual ~TMSubThreadPythonInterface();

    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _TMPYTHONINTERFACE_HPP_
