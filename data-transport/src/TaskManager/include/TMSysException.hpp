#ifndef _TMSYSEXCEPTION_HPP_
#define _TMSYSEXCEPTION_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <exception>
#include <MLUCString.hpp>

class TMSysException: public std::exception
    {
    public:

	TMSysException( int error, const char* err_str, const char* description,
			const char* file, int line ) throw()
		{
		fErrno = error;
		fErrorStr = err_str;
		fDescription = description;
		fFile = file;
		fLine = line;
		}

	~TMSysException() throw()
		{
		}

	int GetErrno() const throw()
		{
		return fErrno;
		}

	const char* GetErrorStr() const throw()
		{
		return fErrorStr.c_str();
		}

	const char* GetDescription() const throw()
		{
		return fDescription.c_str();
		}

	const char* GetFile() const throw()
		{
		return fFile;
		}

	int GetLine() const throw()
		{
		return fLine;
		}

    protected:

	int fErrno;
	
	MLUCString fErrorStr;
	MLUCString fDescription;

	const char* fFile;
	int fLine;

    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _TMSYSEXCEPTION_HPP_
