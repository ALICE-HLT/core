#ifndef _TMINTERRUPTWAITTHREAD_HPP_
#define _TMINTERRUPTWAITTHREAD_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <MLUCThread.hpp>
#include <MLUCString.hpp>
#include <vector>
#include <pthread.h>
#include <map>

class TMSystem;
class TMProcess;

using namespace std;

class TMInterruptWaitThread: public MLUCThread
    {
    public:

	TMInterruptWaitThread( TMSystem* system, const char* listen_addr );
	TMInterruptWaitThread( TMProcess* proc, TMSystem* system, const char* listen_addr );
	~TMInterruptWaitThread();

	void AddProcess( unsigned long ndx, 
			 const char* process_id,
			 void* process_address );

	void DeleteProcess( unsigned long ndx );

	void SetProcessPID( unsigned long ndx, pid_t pid );

	void SetListenAddress( const char* listen_addr );

	bool IsRunning() const
		{
		return fRunning;
		}

    protected:
  
	virtual void Run();

	static void InterruptCallbackWrapper( void* opaque_arg, 
					      pid_t pid,
					      const char* process_id,
					      void* process_address,
					      const char* notificationData );

	void InterruptCallback( pid_t pid,
				const char* process_id,
				void* process_address,
				const char* notificationData );

	TMSystem* fSystem;

	TMProcess* fProcess;

	MLUCString fListenAddress;

	struct ProcessData
	    {
		unsigned long fNdx;
		pid_t fPID;
		MLUCString fID;
		void* fAddress;
	    };

	vector<ProcessData> fProcesses;
	pthread_mutex_t fProcessMutex;

	bool fRunning;

#if 0
        struct ProcessIdentityData
            {
	    MLUCString fID;
	    void* fAddress;
	    };

        std::map<ProcessIdentityData> fProcessIdentityData;
#endif

    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _TMINTERRUPTWAITTHREAD_HPP_
