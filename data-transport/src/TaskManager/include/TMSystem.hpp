#ifndef _TMSYSTEM_HPP_
#define _TMSYSTEM_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <MLUCString.hpp>
#include <MLUCVector.hpp>
#include <MLUCConditionSem.hpp>
#include <MLUCTimer.hpp>
#include <MLUCMutex.hpp>
#include <vector>
#include <map>
#include <pthread.h>

using namespace std;

class TMProcess;
class TMInterfaceLibrary;
class TMPythonInterface;
class TMMainPythonInterface;
class TMConfig;
class TMProcessExecutionStateHandler;
class TMInterruptWaitThread;
class TMPythonRunThread;
class TMSlaveControl;
class TMPythonExtension;
class TMSystem
    {
    public:

	struct LAMData
	    {
		TMProcess* fProcess;
		vector<MLUCString> fNotificationData;
	    };

	enum ConfigChangeProcessState { kUnknown=0, kUnchanged, kAdded, kRemoved, kChanged, kChangedNotStarted };

	TMSystem( const char* configfile, bool allowNoProcesses=false );
	~TMSystem();

	// 	void AddProcess( TMProcess* proc );
	TMProcess* GetProcess( const char* proc_id );
	TMProcess* GetProcess( unsigned long ndx );

	unsigned long GetProcessCnt();

	TMInterfaceLibrary* GetInterfaceLibrary() const
		{
		return fInterfaceLibrary;
		}
	// 	void SetInterfaceLibrary( TMInterfaceLibrary* lib )
	// 		{
	// 		fInterfaceLibrary = lib;
	// 		}
	
	TMInterruptWaitThread* GetInterruptWaitThread() const
		{
		return fInterruptWaitThread;
		}

	TMMainPythonInterface* GetMainPythonInterface() const
		{
		return fMainPythonInterface;
		}
	// 	void SetPythonInterface( TMPythonInterface* inter )
	// 		{
	// 		fPythonInterface = inter;
	// 		}

	TMProcessExecutionStateHandler* GetProcessExecutionStateHandler() const
		{
		return fProcessExecutionStateHandler;
		}

	TMSlaveControl* GetSlaveControl() const
		{
		return fSlaveControl;
		}

	void GetLAMProcessList( vector<LAMData>& procs );
	void SetPeriodicPollInterval( unsigned long interval );
	void SetTaskState( const char* state );
	void SetTaskStatusData( const char* data );
	void GetTaskState( MLUCString& state );
	void GetTaskStatusData( MLUCString& data );

	MLUCTimer& GetTimer()
		{
		return fTimer;
		}


	void GetChildren( MLUCString& data );
	void GetChildStates( const char* proc_id, MLUCString& data );

	void StartProcess( unsigned long ndx );
	void TerminateProcess( unsigned long ndx );
	void CleanupProcessResources( unsigned long ndx );

	void Interrupt( unsigned long ndx, const char* notificationData );
	bool WaitForInterrupt( unsigned long waitTimeout ); // Timeout in microsec.
      
	void Run();

	void Start();
	void Stop();
	void Cleanup();
	void Reset();

	bool CheckSlaveControlCommands();

	bool CheckProcessStates();
	bool ExecuteTaskActions();
// 	bool StartRequiredProcesses();
// 	bool TerminateRequiredProcesses();
// 	bool CleanupRequiredProcesses();
	bool CheckInterrupts();

	void ReadNewConfig()
		{
		fReadNewConfig = true;
		}
	void QuitTask()
		{
		fQuitTask = true;
		}
	void QuitSlave()
		{
		fQuitSlave = true;
		}

#if 0

	const char* GetStartupAction() const;
	const char* GetStateChangeAction() const;
	const char* GetInterruptReceivedAction() const;
	unsigned long GetPeriodicPollInterval() const;
	const char* GetPeriodicPollAction() const;
	const char* GetPreConfigChangeAction() const;
	const char* GetPostConfigChangeAction() const;
	const char* GetLoopAction() const;
	const char* GetLoopStopAction() const;


#endif
	bool GetFailOverActive() const
		{
		return fFailOverActive;
		}
	void SetFailOverActive( bool act )
		{
		fFailOverActive = act;
		}

	void ReadConfig();

	ConfigChangeProcessState GetProcessConfigChangeState( const char* ID );

	//void NormalizeAddress( const char* origAddress, MLUCString& normAddress );

    protected:

	struct TaskAction
	    {
		enum ActionType { kNone=0, kStart, kTerminate, kCleanup };
		ActionType fAction;
		unsigned long fProcNdx;
	    };

	struct ProcessInterruptWaitThread
	    {
		TMInterruptWaitThread* fInterruptWaitThread;
		MLUCString fID;
	    };
	struct InterruptingProcessData
	    {
		unsigned long fNdx;
		MLUCString fNotificationData;
	    };


	void GetInterruptingProcesses( MLUCVector<InterruptingProcessData>& procs );
	void GetTaskActions( MLUCVector<TaskAction>& acts );
// 	void GetProcessesToStart( MLUCVector<unsigned long>& procs );
// 	void GetProcessesToTerminate( MLUCVector<unsigned long>& procs );
// 	void GetProcessesToCleanup( MLUCVector<unsigned long>& procs );

	unsigned long GetNextProcID( const vector<TMProcess*>& procs);

	MLUCTimer fTimer;

	// Pointer to the first (and perferably only) TMSystem instance
	static TMSystem* gSystem;

	TMInterfaceLibrary* fInterfaceLibrary;

	TMMainPythonInterface* fMainPythonInterface;

	vector<TMProcess*> fProcesses;
	pthread_mutex_t fProcessMutex;

	TMProcessExecutionStateHandler* fProcessExecutionStateHandler;

	TMInterruptWaitThread* fInterruptWaitThread;
	TMPythonRunThread* fPythonLoop;
	TMPythonRunThread* fPeriodicPollPythonLoop;
      
	TMSlaveControl* fSlaveControl;

	vector<TMPythonExtension*> fPythonExtensions;

	MLUCString fConfigFile;
	TMConfig* fConfig;

        MLUCString fInterruptListenAddress;
	MLUCString fStartupAction;
	MLUCString fStateChangeAction;
	MLUCString fInterruptReceivedAction;
	MLUCString fPeriodicPollAction;
	unsigned long fPeriodicPollInterval; // In microsec. 
	MLUCString fLoopAction;
	MLUCString fLoopStopAction;
	bool fFailOverActive; // Set also to true in single TM scenarios
	MLUCString fPreConfigChangePhase1Action;
	MLUCString fPreConfigChangePhase2Action;
	MLUCString fPreConfigChangePhase3Action;
	MLUCString fPostConfigChangeAction;


	//       MLUCVector<unsigned long> fInterruptingProcesses;
	//       pthread_mutex_t fInterruptingProcessMutex;
	MLUCConditionSem<InterruptingProcessData> fInterruptingProcesses;

	MLUCVector<TaskAction> fTaskActions;
	pthread_mutex_t fTaskActionMutex;
      
// 	MLUCVector<unsigned long> fProcessesToStart;
// 	pthread_mutex_t fProcessesToStartMutex;
      
// 	MLUCVector<unsigned long> fProcessesToTerminate;
// 	pthread_mutex_t fProcessesToTerminateMutex;
      
// 	MLUCVector<unsigned long> fProcessesToCleanup;
// 	pthread_mutex_t fProcessesToCleanupMutex;

	vector<LAMData> fLAMProcessList;
	pthread_mutex_t fLAMProcessListMutex;

	MLUCString fTaskState;
	pthread_mutex_t fTaskStateMutex;
	MLUCString fTaskStatusData;
	pthread_mutex_t fTaskStatusDataMutex;

	bool fReadNewConfig;
	bool fQuitTask;
	bool fQuitSlave;

	unsigned fPollTime; // in microseconds

	static void SignalHandler( int signal );

	bool fProcIDWrapOccured;
	unsigned long fNextProcID;

	std::map<unsigned long, ConfigChangeProcessState> fProcState;

	class TProcessStateWorkerThread: public MLUCThread
	    {
	    public:
		
		TProcessStateWorkerThread():
		    fProc(NULL),
		    fStateChange(false),
		    fRunning(false)
			{
			}

		void SetProcess( TMProcess* proc )
			{
			fProc = proc;
			}

		TMProcess* GetProcess()
			{
			return fProc;
			}
		
		bool HasStateChanged()
			{
			return fStateChange;
			}

		bool IsRunning()
			{
			bool tmp;
			    {
			    MLUCMutex::TLocker runningLocker( fRunningMutex );
			    tmp = fRunning;
			    }
			return tmp;
			}

		virtual TState Start()
			{
			MLUCMutex::TLocker runningLocker( fRunningMutex );
			fRunning = true;
			TState state = MLUCThread::Start();
			if ( state==kFailed )
			    fRunning = false;
			return state;
			}

	    protected:

		virtual void Run();
		
		TMProcess* fProc;
		bool fStateChange;
		bool fRunning;
		MLUCMutex fRunningMutex;

	    };

    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _TMSYSTEM_HPP_
