#ifndef _TMPYTHONEXTENSION_HPP_
#define _TMPYTHONEXTENSION_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <Python.h>
#include <MLUCDynamicLibrary.hpp>
#include <MLUCString.hpp>
#include <exception>
#include <sys/types.h>


using namespace std;

//typedef PyMODINIT_FUNC (*PythonModuleInitFunc_t)(void);
typedef void (*PythonModuleInitFunc_t)(void);

class TMPythonExtension
    {
    public:

	TMPythonExtension( const char* module, const char* platform, const char* binary );

	void Init();

	MLUCString GetModule()
		{
		return fModule;
		}
	MLUCString GetPlatform()
		{
		return fPlatform;
		}
	MLUCString GetBinary()
		{
		return fBinary;
		}

    protected:

	MLUCString fModule;
	MLUCString fPlatform;
	MLUCString fBinary;
	MLUCDynamicLibrary fLibrary;

	PythonModuleInitFunc_t fInitFunc;

    private:
    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _TMPYTHONEXTENSION_HPP_
