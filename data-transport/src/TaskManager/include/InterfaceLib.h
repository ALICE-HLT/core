#ifndef _TMINTERFACELIBRARY_H_
#define _TMINTERFACELIBRARY_H_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

/* Interface Library Functions: */

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

    /* 
    ** Startup, initialize any static structures needed by the library. 
    ** Parameters:
    ** * private_lib_data: void* output parameter. Will point to memory allocated inside
    **                     Initialize function. Used by library to store internal data
    **                     across calls. Has to be passed to every further library call.
    ** * interrupt_listen_address: A string with the address on which the calling entity
    **                             will listen for interrupts from controlled programs.
    */
    int Initialize( void** private_lib_data, const char* interrupt_listen_address );

    /* 
    ** End, cleanup of library data
    ** Paramters:
    ** * private_lib_data: The private library data allocated in Initialize.
    */
    int Terminate( void* private_lib_data );

    /* 
    ** Convert a given address string into a more compact binary
    ** representation for immediate usability.
    ** This can also be a copy of the given string if this is the most directly
    ** useful representation.
    ** The returned binary address will be released by a call to 
    ** ReleaseResource (below).
    ** Parameters:
    ** * private_lib_data: The private library data allocated in Initialize.
    ** * process_address_str: Input parameter, string form of the address to be converted.
    ** * process_address_bin: Output parameter, binary form (void*) of the converted address.
    */
    int ConvertAddress( void* private_lib_data, const char* process_address_str, void** process_address_bin );

    /*
    ** Compare two given binary address representations. 
    ** Return 0 if they are equal, -1 if addr1 is less than addr2, 
    ** +1 if addr1 is greater than addr2. If the magnitude comparisons
    ** do not apply return unequal 0 for inequality. 
    ** Parameters:
    ** * private_lib_data: The private library data allocated in Initialize.
    ** * addr1: Input parameter, the first address to be compared
    ** * addr2: Input parameter, the second address to be compared
    */
    int CompareAddresses( void* private_lib_data, void* addr1, void* addr2 );

    /* 
    ** Query the state of a specific process 
    ** state is allocated as a string in the function and
    ** has to be released via ReleaseResource (below) 
    ** Parameters:
    ** * private_lib_data: The private library data allocated in Initialize.
    ** * pid: Input parameter, Unix pocess id of the process whose state is 
    **        to be queried.
    ** * process_id: Input parameter, TaskManager internal process id of the 
    **               process whose state is to be queried.
    ** * process_address_bin: Input parameter, Binary form of the address of the
    **                        process whose state is to be queried.
    ** * state: Output parameter, a char* to a string with the queried state.
    */
    int QueryState( void* private_lib_data,
		    pid_t pid, 
		    const char* process_id, 
		    void* process_address_bin, 
		    char** state );

    /* 
    ** Obtain additional status data from a
    ** specific process.
    ** stat_data is allocated as a string in the 
    ** function and has to be released via 
    ** Releaseresource (below)
    ** Parameters:
    ** * private_lib_data: The private library data allocated in Initialize.
    ** * pid: Input parameter, Unix pocess id of the process whose state is 
    **        to be queried.
    ** * process_id: Input parameter, TaskManager internal process id of the 
    **               process whose state is to be queried.
    ** * process_address_bin: Input parameter, Binary form of the address of the
    **                        process whose state is to be queried.
    ** * stat_data: Output parameter, a char* to a string with the queried status
    **              data.
    */
    int QueryStatusData( void* private_lib_data,
			 pid_t pid,
			 const char* process_id, 
			 void* process_address_bin, 
			 char** stat_data );

    /*
    ** Release a resource allocated in another library call.
    ** Parameters:
    ** * private_lib_data: The private library data allocated in Initialize.
    ** * resource: Input parameter, the resource to be freed.
    */
    void ReleaseResource( void* private_lib_data, void* resource );

    /* 
    ** Send the specified command to the given process.
    ** Parameters:
    ** * private_lib_data: The private library data allocated in Initialize.
    ** * pid: Input parameter, Unix pocess id of the process whose state is 
    **        to be queried.
    ** * process_id: Input parameter, TaskManager internal process id of the 
    **               process whose state is to be queried.
    ** * process_address_bin: Input parameter, binary form of the address of the
    **                        process whose state is to be queried.
    ** * command: Input parameter, a string with the command to be sent.
    */
    int SendCommand( void* private_lib_data,
		     pid_t pid, 
		     const char* process_id, 
		     void* process_address_bin, 
		     const char* command );

    /* 
    ** Callback function for the interrupt wait function below.
    ** The last three parameters are used to identify the process
    ** that triggered the interrupt. Only one of them (whichever
    ** suits the library implementation best) has to be present. 
    ** notificationData can be NULL, if no data was passed along
    ** with the interrupt. 
    ** Parameters:
    ** * opaque_arg: The second parameter passed to InterruptWait below,
    **               this is passed through to this function unchanged.
    ** * pid: Input parameter, Unix pocess id of the process that triggered
    **        the interrupt.
    ** * process_id: Input parameter, TaskManager internal process id of the 
    **               process that triggered the interrupt.
    ** * process_address_bin: Input parameter, binary form of the address of the
    **                        process that triggered the interrupt.
    ** * notificationData: Input parameter, a char* with a string containing
    **                     additional notification data. May be NULL.
    */
    typedef void (*InterruptTriggerCallback_t)( void* opaque_arg, 
						pid_t pid,
						const char* process_id,
						void* process_address_bin,
						const char* notificationData);

    /* 
    ** Enter a wait that calls the specified callback when a process 
    ** has triggered an interrupt  (LAM).
    ** The address of the triggering process is passed in the call 
    ** to the specified callback function.
    ** This is called in its own thread to run as an endless loop
    ** and only returns when the StopInterruptWait function
    ** (see below) is called.
    ** Parameters:
    ** * private_lib_data: The private library data allocated in Initialize.
    ** * opaque_arg: Input parameter, this parameter is passed through unchanged
    **               to the InterruptTriggerCallback_t function (see above) passed
    **               as the last parameter to this function.
    ** * listen_address: Input parameter, string form of the address on which the
    **                   function should listen for incoming interrupts.
    ** * callback: Input parameter, a callback function that is called for each 
    **             received interrupt.
    */
    int InterruptWait( void* private_lib_data,
		       void* opaque_arg, 
		       const char* listen_address, 
		       InterruptTriggerCallback_t callback );

    /* 
    ** Stop the interrupt wait loop gracefully. 
    ** Parameters:
    ** * private_lib_data: The private library data allocated in Initialize.
    */
    int StopInterruptWait( void* private_lib_data );

    /* Connection function.
    ** Establish a permanent connection to the given process.
    ** This function is optional and need not be supplied in the interface
    ** library for compatibility reasons.
    ** Parameters:
    ** * private_lib_data: The private library data allocated in Initialize.
    ** * pid: Input parameter, Unix pocess id of the process to whom the 
    **        connection is to be established
    ** * process_id: Input parameter, TaskManager internal process id of the 
    **               process to whom the connection is to be established.
    ** * process_address_bin: Input parameter, binary form of the address of the
    **                        process to whom the connection is to be established.
    */
    int ConnectToProcess( void* private_lib_data,
			  pid_t pid, 
			  const char* process_id, 
			  void* process_address_bin );

    /* Disconnection function.
    ** Severe a previously established permanent connection to the given 
    ** process.
    ** This function is optional and need not be supplied in the interface
    ** library for compatibility reasons.
    ** Parameters:
    ** * private_lib_data: The private library data allocated in Initialize.
    ** * pid: Input parameter, Unix pocess id of the process whose 
    **        connection is to be severed
    ** * process_id: Input parameter, TaskManager internal process id of the 
    **               process whose connection is to be severed.
    ** * process_address_bin: Input parameter, binary form of the address of the
    **                        process whose connection is to be severed.
    */
    int DisconnectFromProcess( void* private_lib_data,
			       pid_t pid, 
			       const char* process_id, 
			       void* process_address_bin );

    /*
    ** Communication error log message suppression function.
    ** This suppresses log messages for communication errors with 
    ** processes. If te timeout is !=0 this will last until the given timout has pased.
    ** This function is optional and need not be supplied in the interface
    ** library for compatibility reasons.
    */
    int SuppressComErrorLogs( void* private_lib_data,
			       pid_t pid, 
			       const char* process_id, 
			      void* process_address_bin,
			       unsigned long timeout_us );


#ifdef __cplusplus
}
#endif


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _TMINTERFACELIBRARY_H_
