#ifndef _TMCONTROLMESSAGETYPES_HPP_
#define _TMCONTROLMESSAGETYPES_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <MLUCTypes.h>

const uint32 kTMControlEmpty = 0;
const uint32 kTMControlQueryState = 1;
const uint32 kTMControlStateReply = 2;
const uint32 kTMControlQueryStatusData = 3;
const uint32 kTMControlStatusDataReply = 4;
const uint32 kTMControlCommand = 5;
const uint32 kTMControlInterrupt = 6;
const uint32 kTMControlQueryChildren = 7;
const uint32 kTMControlChildrenReply = 8;
const uint32 kTMControlQueryStates = 9;
const uint32 kTMControlStatesReply = 10;
const uint32 kTMControlConnect = 11;
const uint32 kTMControlDisconnect = 12;
const uint32 kTMControlQueryChildStates = 13;
const uint32 kTMControlChildStatesReply = 14;
const uint32 kTMControlChildIdentifyRequest = 15;
const uint32 kTMControlChildIdentifyReply = 16;






/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _TMCONTROLMESSAGETYPES_HPP_
