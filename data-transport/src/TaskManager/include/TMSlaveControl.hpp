#ifndef _TMSLAVECONTROL_HPP_
#define _TMSLAVECONTROL_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "TMConfig.hpp"
#include <MLUCString.hpp>
#include <MLUCThread.hpp>
#include <BCLAbstrAddress.hpp>
#include <BCLMsgCommunication.hpp>
#include <pthread.h>

class TMSystem;

class TMSlaveControl
    {
    public:
	TMSlaveControl( TMSystem* sys, TMConfig::SlaveControlConfigData& );
	~TMSlaveControl();

	void Start();
	void Stop();

// 	void GetCommands( vector<MLUCString>& cmds );
	bool GetCommand( MLUCString& cmd );
	bool GetSysCommand( MLUCString& cmd );
	bool HasCommands();

	void CommandReceived();

	void Interrupt( const char* notificationData );

	const char* GetAddress() const
		{
		return fAddressString.c_str();
		}
	const char* GetCommandReceivedAction() const
		{
		return fCommandReceivedAction.c_str();
		}
	void GetInterruptTargetAddresses( vector<MLUCString>& addresses );

	bool IsAutonomous() const
		{
		return fAutonomous;
		}

	void Set( TMConfig::SlaveControlConfigData& );

	void SetCommandReceivedAction( const MLUCString& action );
	

    protected:

	void Clear();
	void ReceiveLoop();
	bool QuitReceiveLoop();

	void GetStates( MLUCString& states );

	TMSystem* fSystem;

	bool fAutonomous;

	MLUCString fAddressString;
	BCLAbstrAddressStruct* fAddress;
	BCLAbstrAddressStruct* fReceiveAddress;
	BCLMsgCommunication* fCom;
	pthread_mutex_t fComMutex;

	MLUCString fCommandReceivedAction;
	pthread_mutex_t fCommandReceivedActionMutex;

	struct InterruptTargetAddressData
	    {
		MLUCString fAddressString;
		BCLAbstrAddressStruct* fAddress;
	    };
	vector<InterruptTargetAddressData> fInterruptTargetAddresses;

	MLUCObjectThread<TMSlaveControl> fReceiveThread;
	bool fQuitReceiveThread;
	bool fReceiveThreadRunning;

	vector<MLUCString> fCommands;
	pthread_mutex_t fCommandMutex;

	vector<MLUCString> fSysCommands;
	pthread_mutex_t fSysCommandMutex;

	vector<TMConfig::StateData> fStatesData;
	pthread_mutex_t fStateMutex;
	
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _TMSLAVECONTROL_HPP_
