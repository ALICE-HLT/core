#ifndef _PYTHONRUNTHREAD_HPP_
#define _PYTHONRUNTHREAD_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <MLUCThread.hpp>
#include <MLUCString.hpp>
#include <pthread.h>

class TMSystem;
class TMSubThreadPythonInterface;

class TMPythonRunThread: public MLUCThread
    {
    public:

	TMPythonRunThread( TMSystem* system, const char* action );
	TMPythonRunThread( TMSystem* system, const char* action, unsigned long interval );
	~TMPythonRunThread();

	bool IsRunning()
		{
		return fRunning;
		}
	void Quit()
		{
		fQuit = true;
		}

	void SetInterval( unsigned long interval )
		{
		fInterval = interval;
		fLoop = true;
		}

	void SetAction( const char* action );

	void StopAction()
		{
		fLoop = false;
		}

    protected:

	virtual void Run();

	TMSystem* fSystem;

	bool fQuit;
	bool fRunning;

	TMSubThreadPythonInterface* fPython;
	bool fLoop;
	unsigned long fInterval; // in microseconds
	MLUCString fAction;
	pthread_mutex_t fActionMutex;

	static void CleanupFuncWrapper( void* arg );
	void CleanupFunc();
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _PYTHONRUNTHREAD_HPP_
