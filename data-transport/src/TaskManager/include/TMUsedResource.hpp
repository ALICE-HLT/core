#ifndef _TMUSEDRESOURCE_HPP_
#define _TMUSEDRESOURCE_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <MLUCString.hpp>

class TMUsedResourceData
    {
    public:
	
	enum UsedResourceType { kNone = 0, kFile, kSysVShm, kGenericCleanup };
	
	TMUsedResourceData( UsedResourceType type, const char* name )
		{
		fType = type;
		fName = name;
		fUseTimeout = false;
		}

	TMUsedResourceData( UsedResourceType type, const char* name, unsigned long timeout_us )
		{
		fType = type;
		fName = name;
		fUseTimeout = true;
		fTimeout = timeout_us;
		}

	TMUsedResourceData( const TMUsedResourceData& urd )
		{
		fType = urd.fType;
		fName = urd.fName;
		fUseTimeout = urd.fUseTimeout;
		fTimeout = urd.fTimeout;
		}
	
	UsedResourceType GetType() const
		{
		return fType;
		}

	const char* GetTypeName() const
		{
		switch ( fType )
		    {
		    case kNone:
			return "None (Invalid Resource)";
		    case kFile:
			return "File";
		    case kSysVShm:
			return "System V Shared Memory";
		    case kGenericCleanup:
			return "Generic Cleanup Executable";
		    default:
			return "Unknown Resource Type";
		    }
		}
	
	const char* GetName() const
		{
		return fName.c_str();
		}
	
	void Cleanup();
	
    protected:
	
	UsedResourceType fType;
	MLUCString fName;
	bool fUseTimeout;
	unsigned long fTimeout; // In microseconds.
    };






/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _TMUSEDRESOURCE_HPP_
