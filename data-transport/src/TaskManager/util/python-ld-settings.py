#!/usr/bin/env python

import distutils.sysconfig


print "-L"+distutils.sysconfig.get_config_var('LIBDIR'),
print "-l"+distutils.sysconfig.get_config_var('LDLIBRARY')[3:-3],
print " "+distutils.sysconfig.get_config_var('SHLIBS'),
print " "+distutils.sysconfig.get_config_var('MODLIBS'),
print distutils.sysconfig.get_config_var('LINKFORSHARED')

