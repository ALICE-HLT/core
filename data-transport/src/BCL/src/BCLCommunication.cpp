/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLCommunication.hpp"
#include "BCLErrorCallback.hpp"

BCLCommunication::BCLCommunication()
    {
    pthread_mutex_init( &fErrorCallbackMutex, NULL );
    fAddress = NULL;
    fReceiveTimeout = 0;
    }

BCLCommunication::~BCLCommunication()
    {
    pthread_mutex_destroy( &fErrorCallbackMutex );
    }

void BCLCommunication::AddCallback( BCLErrorCallback* callback )
    {
    pthread_mutex_lock( &fErrorCallbackMutex );
    if ( callback )
	fErrorCallbacks.insert( fErrorCallbacks.end(), callback );
    pthread_mutex_unlock( &fErrorCallbackMutex );
    }

void BCLCommunication::DelCallback( const BCLErrorCallback* callback )
    {
    pthread_mutex_lock( &fErrorCallbackMutex );
    vector<BCLErrorCallback*>::iterator iter, end;
    iter = fErrorCallbacks.begin();
    end = fErrorCallbacks.end();
    while ( iter != end )
	{
	if ( (const BCLErrorCallback*)(*iter) == callback )
	    {
	    fErrorCallbacks.erase( iter );
	    break;
	    }
	iter++;
	}
    pthread_mutex_unlock( &fErrorCallbackMutex );
    }


void BCLCommunication::ConnectionTemporarilyLost( int error, BCLCommunication* com,
				       BCLAbstrAddressStruct* address, BCLErrorCallback * callback )
    {
    if ( callback )
	callback->ConnectionTemporarilyLost( error, com, address );
    vector<BCLErrorCallback*>::iterator iter, end;
    BCLErrorCallback* pCallback;
    pthread_mutex_lock( &fErrorCallbackMutex );
    iter = fErrorCallbacks.begin();
    end = fErrorCallbacks.end();
    while ( iter != end )
	{
	pCallback = *iter;
	if ( pCallback )
	    pCallback->ConnectionTemporarilyLost( error, com, address );
	iter++;
	}
    pthread_mutex_unlock( &fErrorCallbackMutex );
    }
	
void BCLCommunication::ConnectionError( int error, BCLCommunication* com,
					BCLAbstrAddressStruct* address, BCLErrorCallback* callback )
    {
    if ( callback )
	callback->ConnectionError( error, com, address );
    vector<BCLErrorCallback*>::iterator iter, end;
    BCLErrorCallback* pCallback;
    pthread_mutex_lock( &fErrorCallbackMutex );
    iter = fErrorCallbacks.begin();
    end = fErrorCallbacks.end();
    while ( iter != end )
	{
	pCallback = *iter;
	if ( pCallback )
	    pCallback->ConnectionError( error, com, address );
	iter++;
	}
    pthread_mutex_unlock( &fErrorCallbackMutex );
    }
	
void BCLCommunication::BindError( int error, BCLCommunication* com,
				     BCLAbstrAddressStruct* address,
				  BCLErrorCallback * callback )
    {
    if ( callback )
	callback->BindError( error, com, address );
    vector<BCLErrorCallback*>::iterator iter, end;
    BCLErrorCallback* pCallback;
    pthread_mutex_lock( &fErrorCallbackMutex );
    iter = fErrorCallbacks.begin();
    end = fErrorCallbacks.end();
    while ( iter != end )
	{
	pCallback = *iter;
	if ( pCallback )
	    pCallback->BindError( error, com, address );
	iter++;
	}
    pthread_mutex_unlock( &fErrorCallbackMutex );
    }
	
void BCLCommunication::LockError( int error, BCLCommunication* com,
				     BCLAbstrAddressStruct* address,
				  BCLErrorCallback * callback )
    {
    if ( callback )
	callback->LockError( error, com, address );
    vector<BCLErrorCallback*>::iterator iter, end;
    BCLErrorCallback* pCallback;
    pthread_mutex_lock( &fErrorCallbackMutex );
    iter = fErrorCallbacks.begin();
    end = fErrorCallbacks.end();
    while ( iter != end )
	{
	pCallback = *iter;
	if ( pCallback )
	    pCallback->LockError( error, com, address );
	iter++;
	}
    pthread_mutex_unlock( &fErrorCallbackMutex );
    }
	
void BCLCommunication::MsgSendError( int error, BCLCommunication* com,
				     BCLAbstrAddressStruct* address, 
				     BCLMessageStruct* msg, BCLErrorCallback * callback )
    {
    if ( callback )
	callback->MsgSendError( error, com, address, msg );
    vector<BCLErrorCallback*>::iterator iter, end;
    BCLErrorCallback* pCallback;
    pthread_mutex_lock( &fErrorCallbackMutex );
    iter = fErrorCallbacks.begin();
    end = fErrorCallbacks.end();
    while ( iter != end )
	{
	pCallback = *iter;
	if ( pCallback )
	    pCallback->MsgSendError( error, com, address, msg );
	iter++;
	}
    pthread_mutex_unlock( &fErrorCallbackMutex );
    }
	
void BCLCommunication::MsgReceiveError( int error, BCLCommunication* com,
					BCLAbstrAddressStruct* address, 
					BCLMessageStruct* msg, BCLErrorCallback * callback )
    {
    if ( callback )
	callback->MsgReceiveError( error, com, address, msg );
    vector<BCLErrorCallback*>::iterator iter, end;
    BCLErrorCallback* pCallback;
    pthread_mutex_lock( &fErrorCallbackMutex );
    iter = fErrorCallbacks.begin();
    end = fErrorCallbacks.end();
    while ( iter != end )
	{
	pCallback = *iter;
	if ( pCallback )
	    pCallback->MsgReceiveError( error, com, address, msg );
	iter++;
	}
    pthread_mutex_unlock( &fErrorCallbackMutex );
    }
	
void BCLCommunication::BlobPrepareError( int error, BCLCommunication* com,
					 BCLAbstrAddressStruct* address, BCLErrorCallback * callback )
    {
    if ( callback )
	callback->BlobPrepareError( error, com, address );
    vector<BCLErrorCallback*>::iterator iter, end;
    BCLErrorCallback* pCallback;
    pthread_mutex_lock( &fErrorCallbackMutex );
    iter = fErrorCallbacks.begin();
    end = fErrorCallbacks.end();
    while ( iter != end )
	{
	pCallback = *iter;
	if ( pCallback )
	    pCallback->BlobPrepareError( error, com, address );
	iter++;
	}
    pthread_mutex_unlock( &fErrorCallbackMutex );
    }

void BCLCommunication::BlobSendError( int error, BCLCommunication* com,
				      BCLAbstrAddressStruct* address, 
				      BCLTransferID transfer, BCLErrorCallback * callback )
    {
    if ( callback )
	callback->BlobSendError( error, com, address, transfer );
    vector<BCLErrorCallback*>::iterator iter, end;
    BCLErrorCallback* pCallback;
    pthread_mutex_lock( &fErrorCallbackMutex );
    iter = fErrorCallbacks.begin();
    end = fErrorCallbacks.end();
    while ( iter != end )
	{
	pCallback = *iter;
	if ( pCallback )
	    pCallback->BlobSendError( error, com, address, transfer );
	iter++;
	}
    pthread_mutex_unlock( &fErrorCallbackMutex );
    }

void BCLCommunication::BlobReceiveError( int error, BCLCommunication* com,
					 BCLTransferID transfer, BCLErrorCallback * callback )
    {
    if ( callback )
	callback->BlobReceiveError( error, com, transfer );
    vector<BCLErrorCallback*>::iterator iter, end;
    BCLErrorCallback* pCallback;
    pthread_mutex_lock( &fErrorCallbackMutex );
    iter = fErrorCallbacks.begin();
    end = fErrorCallbacks.end();
    while ( iter != end )
	{
	pCallback = *iter;
	if ( pCallback )
	    pCallback->BlobReceiveError( error, com, transfer );
	iter++;
	}
    pthread_mutex_unlock( &fErrorCallbackMutex );
    }

void BCLCommunication::AddressError( int error, BCLCommunication* com,
				     BCLAbstrAddressStruct* address, BCLErrorCallback * callback )
    {
    if ( callback )
	callback->AddressError( error, com, address );
    vector<BCLErrorCallback*>::iterator iter, end;
    BCLErrorCallback* pCallback;
    pthread_mutex_lock( &fErrorCallbackMutex );
    iter = fErrorCallbacks.begin();
    end = fErrorCallbacks.end();
    while ( iter != end )
	{
	pCallback = *iter;
	if ( pCallback )
	    pCallback->AddressError( error, com, address );
	iter++;
	}
    pthread_mutex_unlock( &fErrorCallbackMutex );
    }

void BCLCommunication::GeneralError( int error, BCLCommunication* com,
				     BCLAbstrAddressStruct* address, BCLErrorCallback * callback )
    {
    if ( callback )
	callback->GeneralError( error, com, address );
    vector<BCLErrorCallback*>::iterator iter, end;
    BCLErrorCallback* pCallback;
    pthread_mutex_lock( &fErrorCallbackMutex );
    iter = fErrorCallbacks.begin();
    end = fErrorCallbacks.end();
    while ( iter != end )
	{
	pCallback = *iter;
	if ( pCallback )
	    pCallback->GeneralError( error, com, address );
	iter++;
	}
    pthread_mutex_unlock( &fErrorCallbackMutex );
    }

// MLUCLog& BCLCommunication::LogAddress( MLUCLog& log, BCLAbstrAddressStruct& addr )
//     {
//     switch ( addr.fComID )
// 	{
// 	case kSCIComID: 
// 	    return log << (BCLSCIAddressStruct&)addr;
// 	case kTCPComID: 
// 	    return log << (BCLTCPAddressStruct&)addr;
// 	default: 
// 	    return log << "Unknown address type";
// 	}
//     }






/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


