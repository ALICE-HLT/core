/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLNetworkData.hpp"


void BCLNetworkData::TransformTo( uint8 format )
    {
    if ( format == fData->fDataFormats[kCurrentByteOrderIndex] )
	return;
    Transform( fData->fLength, fData->fDataFormats[kCurrentByteOrderIndex],
	       format );
    fData->fDataFormats[kCurrentByteOrderIndex] = format;
    }

void BCLNetworkData::TransformToNative()
    {
    if ( kNativeByteOrder == fData->fDataFormats[kCurrentByteOrderIndex] )
	return;
    Transform( fData->fLength, fData->fDataFormats[kCurrentByteOrderIndex],
	       kNativeByteOrder );
    fData->fDataFormats[kCurrentByteOrderIndex] = kNativeByteOrder;
    }
	
void BCLNetworkData::TransformToOriginal()
    {
    if ( fData->fDataFormats[kOriginalByteOrderIndex] == fData->fDataFormats[kCurrentByteOrderIndex] )
	return;
    Transform( fData->fLength, fData->fDataFormats[kCurrentByteOrderIndex],
	       fData->fDataFormats[kOriginalByteOrderIndex] );
    fData->fDataFormats[kCurrentByteOrderIndex] = fData->fDataFormats[kOriginalByteOrderIndex];
    }

void BCLNetworkData::TransformFullyTo( uint8 format )
    {
    if ( format == fData->fDataFormats[kCurrentByteOrderIndex] )
	return;
    Transform( fData->fLength, fData->fDataFormats[kCurrentByteOrderIndex],
	       format );
    fData->fDataFormats[kCurrentByteOrderIndex] = format;
    }

void BCLNetworkData::TransformFullyToNative()
    {
    if ( kNativeByteOrder == fData->fDataFormats[kCurrentByteOrderIndex] )
	return;
    Transform( fData->fLength, fData->fDataFormats[kCurrentByteOrderIndex],
	       kNativeByteOrder );
    fData->fDataFormats[kCurrentByteOrderIndex] = kNativeByteOrder;
    }
	
void BCLNetworkData::TransformFullyToOriginal()
    {
    if ( fData->fDataFormats[kOriginalByteOrderIndex] == fData->fDataFormats[kCurrentByteOrderIndex] )
	return;
    Transform( fData->fLength, fData->fDataFormats[kCurrentByteOrderIndex],
	       fData->fDataFormats[kOriginalByteOrderIndex] );
    fData->fDataFormats[kCurrentByteOrderIndex] = fData->fDataFormats[kOriginalByteOrderIndex];
    }
	
void BCLNetworkData::SetTo( BCLNetworkData* dataClass, bool transformToNative )
    {
    if ( !dataClass )
	{
// 	fData = &fBCLNetworkData;
// 	fData->fDataFormats[kCurrentByteOrderIndex] = kNativeByteOrder;
// 	fData->fLength = sizeof(BCLNetworkDataStruct);
	return;
	}
    else
	{
	BCLNetworkDataStruct* data = dataClass->GetData();
	if ( transformToNative && data->fDataFormats[kCurrentByteOrderIndex]!=kNativeByteOrder )
	    {
	    Transform( data->fLength, fData->fLength, 
		       data->fDataFormats[kCurrentByteOrderIndex],
		       kNativeByteOrder );
	    fData->fDataFormats[kCurrentByteOrderIndex] = kNativeByteOrder;
	    fData->fDataFormats[kOriginalByteOrderIndex] = data->fDataFormats[kOriginalByteOrderIndex];
	    }
	else
	    {
	    *fData = *data;
	    }
	}
    }
	
void BCLNetworkData::SetTo( BCLNetworkDataStruct* data, bool transformToNative )
    {
    if ( !data )
	{
// 	fData = &fBCLNetworkData;
// 	fData->fDataFormats[kCurrentByteOrderIndex] = kNativeByteOrder;
// 	fData->fLength = sizeof(BCLNetworkDataStruct);
	return;
	}
    else
	{
	if ( transformToNative && data->fDataFormats[kCurrentByteOrderIndex]!=kNativeByteOrder )
	    {
	    Transform( data->fLength, fData->fLength, 
		       data->fDataFormats[kCurrentByteOrderIndex],
		       kNativeByteOrder );
	    fData->fDataFormats[kCurrentByteOrderIndex] = kNativeByteOrder;
	    fData->fDataFormats[kOriginalByteOrderIndex] = data->fDataFormats[kOriginalByteOrderIndex];
	    }
	else
	    {
	    *fData = *data;
	    }
	}
    }

void BCLNetworkData::Adopt( BCLNetworkDataStruct* data, bool transformToNative )
    {
    if ( !data )
	{
// 	fData = &fBCLNetworkData;
// 	fData->fDataFormats[kCurrentByteOrderIndex] = kNativeByteOrder;
// 	fData->fLength = sizeof(BCLNetworkDataStruct);
	return;
	}
    else
	{
	fData = data;
	if ( transformToNative && data->fDataFormats[kCurrentByteOrderIndex]!=kNativeByteOrder )
	    {
	    Transform( fData->fLength, 
		       fData->fDataFormats[kCurrentByteOrderIndex],
		       kNativeByteOrder );
	    fData->fDataFormats[kCurrentByteOrderIndex] = kNativeByteOrder;
	    }
	}
    //fData->fDataFormats[kNativeByteOrderIndex] = kNativeByteOrder;
    }


void BCLNetworkData::AdoptFully( BCLNetworkDataStruct* data, bool transformToNative )
    {
    if ( !data )
	{
// 	fData = &fBCLNetworkData;
// 	fData->fDataFormats[kCurrentByteOrderIndex] = kNativeByteOrder;
// 	fData->fLength = sizeof(BCLNetworkDataStruct);
	return;
	}
    else
	{
	fData = data;
	if ( transformToNative && data->fDataFormats[kCurrentByteOrderIndex]!=kNativeByteOrder )
	    {
	    Transform( fData->fLength, 
		       fData->fDataFormats[kCurrentByteOrderIndex],
		       kNativeByteOrder );
	    fData->fDataFormats[kCurrentByteOrderIndex] = kNativeByteOrder;
	    }
	}
    //fData->fDataFormats[kNativeByteOrderIndex] = kNativeByteOrder;
    }















/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
