/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <BCLUDAPLBlobCommunication.hpp>
#include <BCLUDAPLComID.hpp>
#include <BCLUDAPLAddress.hpp>
#include <BCLMsgCommunication.hpp>
#include <BCLUDAPLAddressOps.hpp>
#include <cerrno>
#ifdef UDAPL_EXPLICIT_TRANSMISSION_CONNECT
#include <netinet/in.h>
#endif


//#define DO_TIMEMARKS
#ifdef DO_TIMEMARKS
#define DECL_TIMEMARKS(cnt)  const unsigned timeMarkCnt = cnt;unsigned timeMark = 0;struct timeval timeMarks[ timeMarkCnt ];int timeMarkLines[ timeMarkCnt ];
#define TIMEMARK             if ( timeMark < timeMarkCnt ) { gettimeofday( timeMarks+timeMark, NULL ); timeMarkLines[timeMark]= __LINE__; timeMark++; }
#define DUMP_TIMEMARKS 	{\
            unsigned long td;for ( unsigned tmn = 0; tmn < timeMark; tmn++ ){\
	    if ( tmn<=0 )\
		{\
		LOG( MLUCLog::kBenchmark, "BCLTCPBlobCommunication::NewData", "Timemarks" )\
		    << "Timemark " << MLUCLog::kDec << MLUCLog::kPrec << 3 << tmn\
		    << " (line " << MLUCLog::kPrec << 0 << timeMarkLines[tmn] << "): "\
                    << (timeMarks+tmn)->tv_sec << "."\
		    << MLUCLog::kPrec << 6\
		    << (timeMarks+tmn)->tv_usec << " s." << ENDLOG;\
		}\
	    else\
		{\
		td = ((timeMarks+tmn)->tv_sec - (timeMarks+tmn-1)->tv_sec)*1000000 + ((timeMarks+tmn)->tv_usec-(timeMarks+tmn-1)->tv_usec);\
		LOG( MLUCLog::kBenchmark, "BCLTCPBlobCommunication::NewData", "Timemarks" )\
		    << "Timemark " << MLUCLog::kDec << MLUCLog::kPrec << 3 << tmn\
		    << " (line " << MLUCLog::kPrec << 0 << timeMarkLines[tmn] << "): "\
		    << (timeMarks+tmn)->tv_sec << "."\
		    << MLUCLog::kPrec << 6\
		    << (timeMarks+tmn)->tv_usec << " s - timediff.: " << MLUCLog::kPrec << 0\
		    << td << " usec." << ENDLOG;\
		}\
	    }\
	td = ((timeMarks+timeMark-1)->tv_sec - (timeMarks)->tv_sec)*1000000 + ((timeMarks+timeMark-1)->tv_usec-(timeMarks)->tv_usec);\
		LOG( MLUCLog::kBenchmark, "BCLTCPBlobCommunication::NewData", "Timemarks" )\
		    << "Total timediff. for " << MLUCLog::kDec << timeMark << "timemarks: " << MLUCLog::kPrec << 0\
		    << td << " usec." << ENDLOG;\
	}\

#else
#define TIMEMARK
#define DECL_TIMEMARKS(cnt)
#define DUMP_TIMEMARKS
#endif



const int kUDAPLBlobQueryAddressMsg = kBlobMsg+1;
const int kUDAPLBlobAddressMsg = kBlobMsg+2;
const int kUDAPLBlobQueryFreeBlockMsg = kBlobMsg+3;
const int kUDAPLBlobFreeBlockMsg = kBlobMsg+4;
const int kUDAPLBlobConnectRequestMsg = kBlobMsg+5;
const int kUDAPLBlobDisconnectMsg = kBlobMsg+6;
const int kUDAPLBlobQuitMsg = kBlobMsg+7;
const int kUDAPLBlobQueryBlobSizeMsg = kBlobMsg+8;
const int kUDAPLBlobBlobSizeReplyMsg = kBlobMsg+9;


BCLUDAPLBlobCommunication::BCLUDAPLBlobCommunication( int slotCntExp2 ):
    fComHelper( this ),
    fDataArrivedSignal( 5 ),
    fMsgs( slotCntExp2, true ),
    fRequestThread( this, &BCLUDAPLBlobCommunication::RequestHandler ),
    fDataCallback( this ),
    fReceivedBlocks( slotCntExp2 ),
    fBlobReceivedSignal( slotCntExp2 )
    {
    fCurrentQueryID = 0;
    fCurrentQueryCount = 0;

    pthread_mutex_init( &fBlockMutex, NULL );

    fDataArrivedSignal.Unlock();

    BCLUDAPLAddress addr;
    fUDAPLAddress = *(addr.GetData());

    fBlobReceiveCnt = 0;
    fDataArrivedSignal.Unlock();
    }

BCLUDAPLBlobCommunication::BCLUDAPLBlobCommunication( BCLMsgCommunication* msgCom, int slotCntExp2 ):
    BCLBlobCommunication( msgCom ), fComHelper( this ),
    fDataArrivedSignal( 5 ),
    fMsgs( slotCntExp2, true ),
    fRequestThread( this, &BCLUDAPLBlobCommunication::RequestHandler ),
    fDataCallback( this ),
    fReceivedBlocks( slotCntExp2 ),
    fBlobReceivedSignal( slotCntExp2 )
    {
    fCurrentQueryID = 0;
    fCurrentQueryCount = 0;

    pthread_mutex_init( &fBlockMutex, NULL );

    fDataArrivedSignal.Unlock();

    BCLUDAPLAddress addr;
    fUDAPLAddress = *(addr.GetData());

    fBlobReceiveCnt = 0;
    fDataArrivedSignal.Unlock();
    }

BCLUDAPLBlobCommunication::~BCLUDAPLBlobCommunication()
    {
    }

int BCLUDAPLBlobCommunication::Bind( BCLAbstrAddressStruct* address )
    {
    if ( fAddress )
	{
	LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::Bind", "Already bound" )
	    << "UDAPL Blob object already bound. Unbind first." << ENDLOG;
	return EALREADY;
	}
    if ( !address )
	return EFAULT;
    if ( address->fComID != kUDAPLComID )
	{
	LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::Bind", "Address error" )
	    << "Wrong address ID " << address->fComID << " in UDAPL Blob Bind. (expected " 
	    << kUDAPLComID << ")."<< ENDLOG;
	BindError( EINVAL, this, address, NULL );
	return EINVAL;
	}

    if ( !fBlobBuffer )
	{
	LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::Bind", "No blob buffer" )
	    << "Blob buffer has to be set prior to Bind." << ENDLOG;
	return EIO;
	}
    int ret;
    fAddress = NewAddress();
    if ( !fAddress )
	{
	LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::Bind", "Out of memory" )
	    << "Out of memory trying to allocate new address." << ENDLOG;
	return ENOMEM;
	}
    *(BCLUDAPLAddressStruct*)fAddress = *(BCLUDAPLAddressStruct*)address;
    fUDAPLAddress = *(BCLUDAPLAddressStruct*)address;
    

    ret = fComHelper.Bind( fBlobBuffer, fBlobBufferSize, BCLIntUDAPLComHelper::gkMaxMessageSize, 
			   BCLIntUDAPLComHelper::gkMaxMessageSize, &fUDAPLAddress, &fDataCallback );
    if ( ret )
	{
	LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::Bind", "Com helper bind error" )
	    << "Error binding UDAPL communication helper object: "
	    << strerror(ret) << " (" << MLUCLog::kDec << ret
	    << ")." << ENDLOG;
	DeleteAddress( fAddress );
	fAddress = NULL;
	return ret;
	}
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	{
	LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::Bind", "Block mutex" )
	    << "Error attempting to lock block mutex. Reason: " 
	    << strerror(ret) << " (" << MLUCLog::kDec << ret << "). Continuing..." << ENDLOG;
	}
    TUDAPLBlockData block;
    block.fBlockOffset = 0;
    block.fBlockSize = fBlobBufferSize;
    fFreeBlocks.insert( fFreeBlocks.begin(), block );
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	{
	LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::Bind", "Block mutex" )
	    << "Error attempting to unlock block mutex. Reason: " 
	    << strerror(ret) << " (" << MLUCLog::kDec << ret << "). Continuing..." << ENDLOG;
	}

    fRequestThread.Start();
    return 0;
    }

int BCLUDAPLBlobCommunication::Unbind()
    {
    if ( !fAddress )
	return 0;

    QuitRequestHandler();

    fComHelper.Unbind();

    DeleteAddress( fAddress );
    fAddress = NULL;
    
    return 0;
    }

int BCLUDAPLBlobCommunication::Connect( BCLAbstrAddressStruct* address, bool openOnDemand )
    {
    return Connect( address, NULL, openOnDemand, false, 0 );
    }

int BCLUDAPLBlobCommunication::Connect( BCLAbstrAddressStruct* address, unsigned long timeout, bool openOnDemand )
    {
    return Connect( address, NULL, openOnDemand, true, timeout );
    }

int BCLUDAPLBlobCommunication::Disconnect( BCLAbstrAddressStruct* address )
    {
    return Disconnect( address, NULL, true, false, 0 );
    }

int BCLUDAPLBlobCommunication::Disconnect( BCLAbstrAddressStruct* address, unsigned long timeout )
    {
    return Disconnect( address, NULL, true, true, timeout );
    }

bool BCLUDAPLBlobCommunication::CanLockConnection()
    {
    return false;
    }
int BCLUDAPLBlobCommunication::LockConnection( BCLAbstrAddressStruct* )
    {
    return ENOTSUP;
    }

int BCLUDAPLBlobCommunication::LockConnection( BCLAbstrAddressStruct*, unsigned long )
    {
    return ENOTSUP;
    }

int BCLUDAPLBlobCommunication::UnlockConnection( BCLAbstrAddressStruct* )
    {
    return ENOTSUP;
    }

int BCLUDAPLBlobCommunication::UnlockConnection( BCLAbstrAddressStruct*, unsigned long )
    {
    return ENOTSUP;
    }

// Determine the size of the remote blob buffer.
uint32 BCLUDAPLBlobCommunication::GetRemoteBlobSize( BCLAbstrAddressStruct* msgAddress, unsigned long timeout_ms )
    {
    if ( !msgAddress )
	{
	LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::GetRemoteBlobSize", "Address error" )
	    << "Wrong address (NULL pointer) in BCLUDAPLBlobCommunication::GetRemoteBlobSize" << ENDLOG;
	BlobPrepareError( EFAULT, this, msgAddress );
	return (BCLTransferID)-1;
	}
    if ( !fMsgCom )
	{
	LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::GetRemoteBlobSize", "No msg communication" )
	    << "No message communication object configured in BCLUDAPLBlobCommunication::GetRemoteBlobSize" << ENDLOG;
	BlobPrepareError( ENODEV, this, msgAddress );
	return (BCLTransferID)-1;
	}
    int ret;
    BCLIntUDAPLBlobComMsgStruct* msg;
    BCLIntUDAPLBlobComMsg msgClass;
    msg = msgClass.GetData();
    msg->fMsgType = kUDAPLBlobQueryBlobSizeMsg;
    ret = GetQueryID( msg->fQueryID );
    if ( ret )
	{
	BlobPrepareError( ret, this, msgAddress );
	return (BCLTransferID)-1;
	}
    fDataArrivedSignal.Lock();
    if ( timeout_ms )
	ret = fMsgCom->Send( msgAddress, msg, timeout_ms );
    else
	ret = fMsgCom->Send( msgAddress, msg );
    if ( ret )
	{
	fDataArrivedSignal.Unlock();
	BlobPrepareError( ret, this, msgAddress );
	return (BCLTransferID)-1;
	}
    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::GetRemoteBlobSize", "Query blob size" )
	<< "Query Blob Size message sent." << ENDLOG;
    ret = WaitForMessage( msg->fQueryID, msg, timeout_ms, timeout_ms );
    fDataArrivedSignal.Unlock();
    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::GetRemoteBlobSize", "Blob Size" )
	<< "Received blob size " << msg->fBlockSize << "." << ENDLOG;
    if ( ret )
	return (uint32)0;
    return msg->fBlockSize;
    }

// Blob transfer sender side.
// The msgAddress parameter passed to PrepareBlobTransfer is the address of the 
// msg communication object used by the blob transfer object on the other side.
// (See SetMsgCommunication)
BCLTransferID BCLUDAPLBlobCommunication::PrepareBlobTransfer( BCLAbstrAddressStruct* msgAddress, uint64 size,
							      unsigned long timeout )
    {
    if ( !msgAddress )
	{
	LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::PrepareBlobTransfer", "Address error" )
	    << "Wrong address (NULL pointer) in BCLUDAPLBlobCommunication::PrepareBlobTransfer" << ENDLOG;
	BlobPrepareError( EFAULT, this, msgAddress );
	return (BCLTransferID)-1;
	}
    if ( !fMsgCom )
	{
	LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::PrepareBlobTransfer", "No msg communication" )
	    << "No message communication object configured in BCLUDAPLBlobCommunication::PrepareBlobTransfer" << ENDLOG;
	BlobPrepareError( ENODEV, this, msgAddress );
	return (BCLTransferID)-1;
	}
    int ret;
    BCLIntUDAPLBlobComMsgStruct* msg;
    BCLIntUDAPLBlobComMsg msgClass;
    msg = msgClass.GetData();
    msg->fMsgType = kUDAPLBlobQueryFreeBlockMsg;
    msg->fBlockSize = size;
    ret = GetQueryID( msg->fQueryID );
    if ( ret )
	{
	BlobPrepareError( ret, this, msgAddress );
	return (BCLTransferID)-1;
	}
    fDataArrivedSignal.Lock();
    if ( timeout )
	ret = fMsgCom->Send( msgAddress, msg, timeout );
    else
	ret = fMsgCom->Send( msgAddress, msg );
    if ( ret )
	{
	fDataArrivedSignal.Unlock();
	BlobPrepareError( ret, this, msgAddress );
	return (BCLTransferID)-1;
	}
    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::PrepareBlobTransfer", "Query free block" )
	<< "Query Free Block message sent." << ENDLOG;
    ret = WaitForMessage( msg->fQueryID, msg, timeout, timeout );
    fDataArrivedSignal.Unlock();
    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::PrepareBlobTransfer", "Blob transfer id" )
	<< "Received blob transfer id " << msg->fBlockOffset << "." << ENDLOG;
    if ( ret )
	return (BCLTransferID)-1;
    if ( msg->fBlockOffset == (uint32)-1 )
	return (BCLTransferID)-1;
    return (BCLTransferID)msg->fBlockOffset;
    }

int BCLUDAPLBlobCommunication::TransferBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, uint8* data,
					     uint64 offset, uint64 size, unsigned long timeout,
					     BCLErrorCallback *callback )
    {
    vector<uint8*> datas;
    datas.push_back( data );
    vector<uint64> offsets;
    offsets.push_back( offset );
    vector<uint64> sizes;
    sizes.push_back( size );
    return TransmitBlob( msgAddress, id, datas, offsets, sizes, true, timeout, callback );
    }

int BCLUDAPLBlobCommunication::TransferBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, vector<uint8*> datas,
					     vector<uint64> offsets, vector<uint64> sizes, unsigned long timeout,
					     BCLErrorCallback *callback )
    {
    return TransmitBlob( msgAddress, id, datas, offsets, sizes, true, timeout, callback );
    }

// These two are basically identical, but they do not wait for the notification from the receiver
// that the transfer has been complete
// This might lead to overtaking by other messages...
int BCLUDAPLBlobCommunication::PushBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, uint8* data,
					 uint64 offset, uint64 size, unsigned long timeout,
					 BCLErrorCallback *callback )
    {
    vector<uint8*> datas;
    datas.push_back( data );
    vector<uint64> offsets;
    offsets.push_back( offset );
    vector<uint64> sizes;
    sizes.push_back( size );
    return TransmitBlob( msgAddress, id, datas, offsets, sizes, false, timeout, callback );
    }

int BCLUDAPLBlobCommunication::PushBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, vector<uint8*> datas,
					 vector<uint64> offsets, vector<uint64> sizes, unsigned long timeout,
					 BCLErrorCallback *callback )
    {
    return TransmitBlob( msgAddress, id, datas, offsets, sizes, false, timeout, callback );
    }

// Not supported currently; Might be introduced back in later versions. 
//virtual bool IsTransferComplete( BCLTransferID transfer );

int BCLUDAPLBlobCommunication::WaitForReceivedTransfer( BCLTransferID& transfer, vector<uint64>& offsets, vector<uint64>& sizes, unsigned long timeout )
    {
    fQuitReceivedBlobWait = false;
    bool found = false;
    struct timeval start, stop;
    unsigned long tdiff, twait;
    fBlobReceivedSignal.Lock();
    gettimeofday( &start, NULL );
    do
	{    
	if ( !fQuitReceivedBlobWait && fReceivedBlocks.GetCnt()>0 )
	    {
	    offsets.clear();
	    sizes.clear();
	    found = true;
	    TUDAPLReceivedBlobData* blob = fReceivedBlocks.GetFirstPtr();
	    transfer = blob->fTransferID;
	    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::WaitForReceivedTransfer", "Found new transfer" )
		<< "Found new received transfer - TransferID: " << MLUCLog::kDec << transfer << " (0x" 
		<< MLUCLog::kHex << transfer << ")." << ENDLOG;
	    offsets.resize( blob->fTransferBlocksLeft+1 );
	    sizes.resize( blob->fTransferBlocksLeft+1 );
	    unsigned long ndx=0;
	    do
		{
		LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::WaitForReceivedTransfer", "Transfer block" )
		    << "Transfer " << MLUCLog::kDec << transfer << " (0x" 
		    << MLUCLog::kHex << transfer << ") block offset: " << MLUCLog::kDec
		    << blob->fBlockOffset << " (0x" << MLUCLog::kHex << blob->fBlockOffset
		    << " - block size: " << MLUCLog::kDec << blob->fBlockSize << " (0x" 
		    << MLUCLog::kDec << blob->fBlockSize << ")." << ENDLOG;
		offsets[ndx] = blob->fBlockOffset;
		sizes[ndx] = blob->fBlockSize;
		ndx++;
		if ( blob->fTransferBlocksLeft )
		    {
		    fReceivedBlocks.RemoveFirst();
		    blob = fReceivedBlocks.GetFirstPtr();
		    }
		else
		    {
		    blob = NULL;
		    fReceivedBlocks.RemoveFirst();
		    }
		}
	    while ( blob );
	    }
	else if ( !fQuitReceivedBlobWait )
	    {
	    gettimeofday( &stop, NULL );
	    tdiff = (stop.tv_sec-start.tv_sec)*1000+(stop.tv_usec-start.tv_usec)/1000;
	    if ( timeout && tdiff>timeout )
		{
		fBlobReceivedSignal.Unlock();
		return ETIMEDOUT;
		}
	    if ( timeout )
		{
		twait = timeout-tdiff;
		fBlobReceivedSignal.Wait( twait );
		}
	    else
		fBlobReceivedSignal.Wait();
	    }
	}
    while ( !found && !fQuitReceivedBlobWait );
    fBlobReceivedSignal.Unlock();
    if ( fQuitReceivedBlobWait )
	{
	fQuitReceivedBlobWait = false;
	return ECANCELED;
	}
    return 0;
    }

void BCLUDAPLBlobCommunication::AbortWaitForReceivedTransfer()
    {
    fBlobReceivedSignal.Lock();
    fQuitReceivedBlobWait = true;
    fBlobReceivedSignal.Unlock();
    fBlobReceivedSignal.Signal();
    }

// Blob transfer receiver side. The transfer ID has to be transmitted by explicit 
// user level communication between blob sender and receiver.
uint8* BCLUDAPLBlobCommunication::GetBlobData( BCLTransferID transfer )
    {
    int ret;
    uint64 offset = (uint64)transfer;
    if ( offset >= fBlobBufferSize )
	{
	BlobReceiveError( ENOENT, this, transfer, NULL );
	return NULL;
	}
#ifndef NO_PARANOIA
    TUDAPLBlockDataContType::iterator iter, end;
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	{
	BlobReceiveError( ENXIO, this, transfer, NULL );
	return NULL;
	}
    iter = fUsedBlocks.begin();
    end = fUsedBlocks.end();
    while ( iter != end )
	{
	if ( iter->fBlockOffset == offset )
	    break;
	iter++;
	}
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	{
	BlobReceiveError( ENXIO, this, transfer, NULL );
	return NULL;
	}
    if ( iter == end )
	{
	BlobReceiveError( ENOENT, this, transfer, NULL );
	return NULL;
	}
#endif
    return fBlobBuffer+offset;
    }

uint64 BCLUDAPLBlobCommunication::GetBlobOffset( BCLTransferID transfer )
    {
    int ret;
    uint64 offset = (uint64)transfer;
    if ( offset >= fBlobBufferSize )
	{
	BlobReceiveError( ENOENT, this, transfer, NULL );
	return (uint64)-1;
	}
#ifndef NO_PARANOIA
    TUDAPLBlockDataContType::iterator iter, end;
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	{
	BlobReceiveError( ENXIO, this, transfer, NULL );
	return (uint64)-1;
	}
    iter = fUsedBlocks.begin();
    end = fUsedBlocks.end();
    while ( iter != end )
	{
	if ( iter->fBlockOffset == offset )
	    break;
	iter++;
	}
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	{
	BlobReceiveError( ENXIO, this, transfer, NULL );
	return (uint64)-1;
	}
    if ( iter == end )
	{
	BlobReceiveError( ENOENT, this, transfer, NULL );
	return (uint64)-1;
	}
#endif
    return offset;
    }

void BCLUDAPLBlobCommunication::ReleaseBlob( BCLTransferID transfer )
    {
    int ret;
    uint64 offset = (uint64)transfer;
    if ( offset >= fBlobBufferSize )
	{
	LOG( MLUCLog::kWarning, "BCLUDAPLBlobCommunication::ReleaseBlob", "Blob offset too large" )
	    << "Blob offset " << MLUCLog::kDec << offset << " (0x" << MLUCLog::kHex << offset 
	    << ") derived from transfer id 0x" << transfer << " is larger than buffer size " << MLUCLog::kDec
	    << fBlobBufferSize << "." << ENDLOG;
	GeneralError( ENOENT, this, NULL );
	return;
	}
    TUDAPLBlockDataContType::iterator iter, end;
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	{
	LOG( MLUCLog::kWarning, "BCLUDAPLBlobCommunication::ReleaseBlob", "Mutex lock error" )
	    << "Error locking block mutex: " << strerror(ret) << " (" << MLUCLog::kDec << ret
	    << ")." << ENDLOG;
	GeneralError( ENXIO, this, NULL );
	return;
	}
    iter = fUsedBlocks.begin();
    end = fUsedBlocks.end();
    bool found = false;
    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::ReleaseBlob", "Block to Free" )
	<< "Block to free Offset: " << MLUCLog::kDec << offset 
	<< "." << ENDLOG;
    while ( iter != end )
	{
	LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::ReleaseBlob", "Used Block" )
	    << "Used block offset: " << MLUCLog::kDec << iter->fBlockOffset << " - size: "
	    << iter->fBlockSize << "." << ENDLOG;
	if ( iter->fBlockOffset == offset )
	    {
	    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::ReleaseBlob", "Used Block Found" )
		<< "Used block found, offset: " << MLUCLog::kDec << iter->fBlockOffset << " - size: "
		<< iter->fBlockSize << "." << ENDLOG;
	    found = true;
	    bool freeFound = false;
	    TUDAPLBlockDataContType::iterator freeIter, freeEnd;
	    freeIter = fFreeBlocks.begin();
	    freeEnd = fFreeBlocks.end();
	    while ( freeIter != freeEnd )
		{
		LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::ReleaseBlob", "Free Block" )
		    << "Free block offset: " << MLUCLog::kDec << freeIter->fBlockOffset << " - size: "
		    << freeIter->fBlockSize << "." << ENDLOG;
		if ( iter->fBlockOffset+iter->fBlockSize==freeIter->fBlockOffset )
		    {
		    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::ReleaseBlob", "Free Block Merged" )
			<< "Merged used block to beginning of free..." << ENDLOG;
		    freeIter->fBlockOffset = iter->fBlockOffset;
		    freeIter->fBlockSize += iter->fBlockSize;
		    freeFound = true;
		    break;
		    }
		if ( iter->fBlockOffset==freeIter->fBlockOffset+freeIter->fBlockSize )
		    {
		    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::ReleaseBlob", "Free Block Merged" )
			<< "Merged used block to end of free..." << ENDLOG;
		    freeIter->fBlockSize += iter->fBlockSize;
		    if ( freeIter!=freeEnd && 
			 (freeIter+1)->fBlockOffset==(freeIter->fBlockOffset+freeIter->fBlockSize) )
			{
			LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::ReleaseBlob", "Free Block Merged" )
			    << "Merged following free block..." << ENDLOG;

			freeIter->fBlockSize += (freeIter+1)->fBlockSize;
			fFreeBlocks.erase( freeIter+1 );
			}
		    freeFound = true;
		    break;
		    }
		if ( freeIter->fBlockOffset > iter->fBlockOffset )
		    {
		    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::ReleaseBlob", "Free Block Merged" )
			<< "Inserted used block..." << ENDLOG;

		    fFreeBlocks.insert( freeIter, *iter );
		    freeFound = true;
		    break;
		    }
		freeIter++;
		}
	    if ( !freeFound )
		{
		LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::ReleaseBlob", "Free Block Merged" )
		    << "Inserted used block at end..." << ENDLOG;
		fFreeBlocks.insert( fFreeBlocks.end(), *iter );
		}
	    fUsedBlocks.erase( iter );
	    break;
	    }
	iter++;
	}
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	{
	LOG( MLUCLog::kWarning, "BCLUDAPLBlobCommunication::ReleaseBlob", "Mutex unlock error" )
	    << "Error unlocking block mutex: " << strerror(ret) << " (" << MLUCLog::kDec << ret
	    << ")." << ENDLOG;
	GeneralError( ENXIO, this, NULL );
	return;
	}
    if ( !found )
	{
	LOG( MLUCLog::kWarning, "BCLUDAPLBlobCommunication::ReleaseBlob", "No free block" )
	    << "No used block with the needed offset of " << MLUCLog::kDec << offset << " (0x" << MLUCLog::kHex << offset 
	    << ") derived from transfer id 0x" << transfer << " could be found." << ENDLOG;
	GeneralError( ENOENT, this, NULL );
	return;
	}
    return;
    }

uint32 BCLUDAPLBlobCommunication::GetAddressLength()
    {
    return sizeof(BCLUDAPLAddressStruct);
    }

uint32 BCLUDAPLBlobCommunication::GetComID()
    {
    return kUDAPLComID;
    }

BCLUDAPLAddressStruct* BCLUDAPLBlobCommunication::NewAddress()
    {
    BCLUDAPLAddressStruct* addr = new BCLUDAPLAddressStruct;
    if ( !addr )
	return NULL;
    BCLUDAPLAddress addrClass;
    *addr = *(addrClass.GetData());
    addr->fComID = kUDAPLComID;
    return addr;
    }

void BCLUDAPLBlobCommunication::DeleteAddress( BCLAbstrAddressStruct* address )
    {
    delete (BCLUDAPLAddressStruct*)address;
    }

bool BCLUDAPLBlobCommunication::AddressEquality( const BCLAbstrAddressStruct* addr1, const BCLAbstrAddressStruct* addr2 )
    {
    if ( addr1->fComID != kUDAPLComID || addr2->fComID != kUDAPLComID )
	return false;
    return *(BCLUDAPLAddressStruct*)addr1 == *(BCLUDAPLAddressStruct*)addr2;
    }

BCLUDAPLBlobCommunication::operator bool()
    {
    return (bool)fComHelper;
    }

int BCLUDAPLBlobCommunication::Connect( BCLAbstrAddressStruct* msgAddress, BCLUDAPLAddressStruct* blobAddress, bool openOnDemand, bool useTimeout, unsigned long timeout_ms )
    {
    if ( !msgAddress )
	{
	LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::Connect", "Address error" )
	    << "Wrong address (NULL pointer) in BCLUDAPLBlobCommunication::Connect" << ENDLOG;
	ConnectionError( EFAULT, this, msgAddress );
	return EFAULT;
	}
    if ( !fMsgCom )
	{
	LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::Connect", "No msg communication" )
	    << "No message communication object configured in BCLUDAPLBlobCommunication::Connect" << ENDLOG;
	ConnectionError( ENODEV, this, msgAddress );
	return ENODEV;
	}
    int ret;
    if ( useTimeout )
	ret = fMsgCom->Connect( msgAddress, timeout_ms, openOnDemand );
    else
	ret = fMsgCom->Connect( msgAddress, openOnDemand );
    if ( ret )
	{
	ConnectionError( ret, this, msgAddress );
	return ret;
	}
    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::Connect", "Msg com connected" )
	<< "Message communication object connected." << ENDLOG;
    BCLUDAPLAddressStruct remoteBlobAddress;
    if ( !blobAddress )
	{
	ret = GetRemoteBlobAddress( msgAddress, &remoteBlobAddress, useTimeout, timeout_ms );
	if ( ret )
	    {
	    ConnectionError( ret, this, msgAddress );
	    return ret;
	    }
	LOG( MLUCLog::kInformational, "BCLUDAPLBlobCommunication::Connect", "Remote Blob Address" )
	    << "Got remote Blob address: " << remoteBlobAddress << "." << ENDLOG;
	}
    else
	{
	remoteBlobAddress = *blobAddress;
	LOG( MLUCLog::kInformational, "BCLUDAPLBlobCommunication::Connect", "Remote Blob Address" )
	    << "Remote Blob address set to " << remoteBlobAddress << "." << ENDLOG;
	}
    ret = fComHelper.Connect( &remoteBlobAddress, openOnDemand, useTimeout, timeout_ms );
    if ( ret )
	{
	if ( useTimeout )
	    fMsgCom->Disconnect( msgAddress, timeout_ms );
	else
	    fMsgCom->Disconnect( msgAddress );
	return ret;
	}
    TUDAPLRemoteBlobAddress rba;
    rba.fMsgAddress = fMsgCom->NewAddress();
    if ( rba.fMsgAddress && rba.fMsgAddress->fLength==msgAddress->fLength)
	{
	//*(rba.fMsgAddress) = *msgAddress;
	memcpy( rba.fMsgAddress, msgAddress, msgAddress->fLength );
	rba.fBlobAddress = remoteBlobAddress;
	fRemoteBlobAddresses.insert( fRemoteBlobAddresses.end(), rba );
	}
    if ( !blobAddress )
	{
	BCLIntUDAPLBlobComMsg msg;
	msg.GetData()->fMsgType = kUDAPLBlobConnectRequestMsg;
	memcpy( msg.GetData()->fIAName, fUDAPLAddress.fIAName, 32 );
	msg.GetData()->fSinFamily = fUDAPLAddress.fSinFamily;
	msg.GetData()->fSinPort = fUDAPLAddress.fSinPort;
	msg.GetData()->fNodeID = fUDAPLAddress.fNodeID;
	msg.GetData()->fConnQual = fUDAPLAddress.fConnQual;
	//GetQueryID( msg.GetData()->fQueryID );
	LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::Connect", "Sending connection request" )
	    << "Sending connection request to remote side." << ENDLOG;
	if ( useTimeout )
	    ret = fMsgCom->Send( msgAddress, msg.GetData(), timeout_ms );
	else
	    ret = fMsgCom->Send( msgAddress, msg.GetData() );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::Connect", "Connection request" )
		<< "Error sending connection request to remote msg com object" << ENDLOG;
	    ConnectionError( ret, this, msgAddress );
	    return ret;
	    }
	LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::Connect", "Connection request" )
	    << "Connection Request message sent." << ENDLOG;
	}
    LOG( MLUCLog::kInformational, "BCLUDAPLBlobCommunication::Connect", "Connection established" )
	<< "Connection to established." << ENDLOG;
    return 0;
    }

int BCLUDAPLBlobCommunication::Disconnect( BCLAbstrAddressStruct* msgAddress, BCLUDAPLAddressStruct* blobAddress, bool initiator, bool useTimeout, unsigned long timeout_ms )
    {
    if ( !msgAddress )
	{
	LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::Disconnect", "Address error" )
	    << "Wrong address (NULL pointer) in BCLUDAPLBlobCommunication::Disconnect" << ENDLOG;
	ConnectionError( EFAULT, this, msgAddress );
	return EFAULT;
	}
    if ( !fMsgCom )
	{
	LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::Disconnect", "No msg communication" )
	    << "No message communication object configured in BCLUDAPLBlobCommunication::Disconnect" << ENDLOG;
	ConnectionError( ENODEV, this, msgAddress );
	return ENODEV;
	}
    int ret, ret1;
    BCLUDAPLAddressStruct remoteBlobAddress;
    if ( !blobAddress )
	{
	ret = GetRemoteBlobAddress( msgAddress, &remoteBlobAddress, useTimeout, timeout_ms );
	if ( ret )
	    {
	    ConnectionError( ret, this, msgAddress );
	    return ret;
	    }
	blobAddress = &remoteBlobAddress;
	}
    ret1 = fComHelper.Disconnect( blobAddress, useTimeout, timeout_ms );
    if ( initiator )
	{
	BCLIntUDAPLBlobComMsg msg;
	msg.GetData()->fMsgType = kUDAPLBlobDisconnectMsg;
	memcpy( msg.GetData()->fIAName, fUDAPLAddress.fIAName, 32 );
	msg.GetData()->fSinFamily = fUDAPLAddress.fSinFamily;
	msg.GetData()->fSinPort = fUDAPLAddress.fSinPort;
	msg.GetData()->fNodeID = fUDAPLAddress.fNodeID;
	msg.GetData()->fConnQual = fUDAPLAddress.fConnQual;
	//GetQueryID( msg.GetData()->fQueryID );
	if ( useTimeout )
	    fMsgCom->Send( msgAddress, msg.GetData(), timeout_ms );
	else
	    fMsgCom->Send( msgAddress, msg.GetData() );
	LOG( MLUCLog::kInformational, "BCLUDAPLBlobCommunication::Disconnect", "Sent disconnect" )
	    << "Disconnect message sent." << ENDLOG;
	}
    if ( useTimeout )
	ret = fMsgCom->Disconnect( msgAddress, timeout_ms );
    else
	ret = fMsgCom->Disconnect( msgAddress );
    vector<TUDAPLRemoteBlobAddress>::iterator iter, end;
    iter = fRemoteBlobAddresses.begin();
    end = fRemoteBlobAddresses.end();
    while ( iter != end )
	{
	if ( fMsgCom->AddressEquality( iter->fMsgAddress, msgAddress ) || 
	     iter->fBlobAddress == remoteBlobAddress )
	    {
	    fMsgCom->DeleteAddress( iter->fMsgAddress );
	    fRemoteBlobAddresses.erase( iter );
	    break;
	    }
	iter++;
	}
    if ( ret1 )
	return ret1;
    if ( ret )
	return ret;
    return 0;
    }

int BCLUDAPLBlobCommunication::TransmitBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, vector<uint8*> datas,
					     vector<uint64> offsets, vector<uint64> sizes, bool, 
					     unsigned long timeout, BCLErrorCallback *callback )
    {
    DECL_TIMEMARKS( 50 );
    TIMEMARK;
    if ( id == (BCLTransferID)-1 )
	{
	BlobSendError( EINVAL, this, msgAddress, id, callback );
	return EINVAL;
	}
    if ( !msgAddress )
	{
	AddressError( EFAULT, this, msgAddress, callback );
	return EFAULT;
	}
    if ( !fMsgCom )
	{
	BlobSendError( ENODEV, this, msgAddress, id, callback );
	return ENODEV;
	}
    if ( !sizes.size() )
	{
	return 0;
	}
    if ( sizes.size()!=datas.size() )
	{
	BlobSendError( EINVAL, this, msgAddress, id, callback );
	return EINVAL;
	}
    if ( sizeof(BCLTransferID)+sizeof(uint32)*(2*sizes.size()+1) > BCLIntUDAPLComHelper::gkMaxMessageSize )
	{
	BlobSendError( E2BIG, this, msgAddress, id, callback );
	return E2BIG;
	}
    TIMEMARK;
    int ret;
    BCLUDAPLAddressStruct remoteBlobAddress;
    ret = GetRemoteBlobAddress( msgAddress, &remoteBlobAddress, timeout, timeout );
    if ( ret )
	{
	BlobSendError( ret, this, msgAddress, id, callback );
	return ret;
	}
    bool failure = true;
    while ( failure )
	{
	failure = false;
	TIMEMARK;
	BCLIntUDAPLComHelper::ConnectionData cd;
	cd.fAddress = remoteBlobAddress;
	ret = fComHelper.ConnectToRemote( &cd, true, timeout, timeout );
	if ( ret )
	    {
	    // Only hard failure here.
	    BlobSendError( ret, this, msgAddress, id, callback );
	    return ret;
	    }
	TIMEMARK;
	
	uint32 blockCnt = sizes.size();
	unsigned long offset = 0;
	DAT_LMR_HANDLE lmrHandle;
	DAT_RETURN dret;
	TIMEMARK;
	for ( unsigned n = 0; n < blockCnt; n++ )
	    {
	    DAT_REGION_DESCRIPTION region;
	    DAT_VADDR data;
	    DAT_VLEN size;
	    DAT_LMR_CONTEXT lmrContext;
	
	    region.for_va = datas[n];
	    TIMEMARK;
	    bool useCopy = false;
	    if ( sizes[n]<=128*1024 ) // experimental parameter, might vary unfortunately; found to be actually 256k on arminius cluster in paderborn.
		useCopy = true;
	    if ( !useCopy )
		{
		dret = dat_lmr_create( fComHelper.fIA,
				       DAT_MEM_TYPE_VIRTUAL,
				       region,
				       sizes[n],
				       fComHelper.fPZ,
				       DAT_MEM_PRIV_ALL_FLAG,
				       &lmrHandle,
				       &lmrContext,
				       NULL,
				       &size,
				       &data );
		TIMEMARK;
		if ( dret != DAT_SUCCESS )
		    {
		    LOG( MLUCLog::kWarning, "BCLUDAPLBlobCommunication::TransmitBlob", "Error creating lrm send block" )
			<< "Error creating sending block " << MLUCLog::kDec << n
			<< " of transfer " << (unsigned long)id << " (0x"
			<< MLUCLog::kHex << (unsigned long)id << "): "
			<< DatStrError(dret).c_str() << " ("
			<< MLUCLog::kDec << dret << ") - Trying copy into send buffer."
			<< ENDLOG;
		    useCopy = true;
		    lmrHandle = DAT_HANDLE_NULL;
		    }
		}
	    else
		lmrHandle = DAT_HANDLE_NULL;
	    if ( reinterpret_cast<uint8*>(data) != datas[n] )
		{
		LOGM( MLUCLog::kInformational, "BCLUDAPLBlobCommunication::TransmitBlob", "LMR buffer beginning shifted", 100 )
		    << "Beginning of block LMR buffer is different from block buffer for sending block " << MLUCLog::kDec << n
		    << " of transfer " << (unsigned long)id << " (0x"
		    << MLUCLog::kHex << (unsigned long)id << ") - Trying copy into send buffer."
		    << ENDLOG;
		useCopy = true;
		}
	    if ( (uint32)size != sizes[n] )
		{
		LOGM( MLUCLog::kInformational, "BCLUDAPLBlobCommunication::TransmitBlob", "LMR buffer size shifted", 100 )
		    << "Size of block LMR buffer is different from block size for sending block " << MLUCLog::kDec << n
		    << " of transfer " << (unsigned long)id << " (0x"
		    << MLUCLog::kHex << (unsigned long)id << ") - Trying copy into send buffer."
		    << ENDLOG;
		useCopy = true;
		}
	    unsigned long sent = 0;
	    if ( useCopy )
		{
		if ( lmrHandle != DAT_HANDLE_NULL )
		    dat_lmr_free( lmrHandle );
		lmrHandle = DAT_HANDLE_NULL;
		}
	    TIMEMARK;
	    while ( sent < sizes[n] )
		{
		DAT_LMR_TRIPLET l_iov;
		l_iov.pad = 0;
		l_iov.segment_length = sizes[n]-sent;
		if ( useCopy && l_iov.segment_length>BCLIntUDAPLComHelper::gkMaxMessageSize )
		    l_iov.segment_length = BCLIntUDAPLComHelper::gkMaxMessageSize;
		if ( useCopy )
		    {
		    l_iov.lmr_context = fComHelper.fSendLMRContext;
		    memcpy( reinterpret_cast<void*>(fComHelper.fSendBuffer), datas[n]+sent, l_iov.segment_length );
		    l_iov.virtual_address = fComHelper.fSendBuffer;
		    }
		else
		    {
		    l_iov.lmr_context = lmrContext;
		    l_iov.virtual_address = data;
		    }
		if ( offsets.size()>n )
		    offset = offsets[n];
		DAT_DTO_COOKIE cookie;
		cookie.as_64 = BCLIntUDAPLComHelper::kBlobTransferDTOCookie;
		DAT_RMR_TRIPLET r_iov;
		r_iov = cd.fBlobRMRTriplet;
		r_iov.target_address += (uint32)id;
		r_iov.target_address += offset;
		r_iov.target_address += sent;
		r_iov.segment_length = l_iov.segment_length;
		
		TIMEMARK;
		dret = dat_ep_post_rdma_write( cd.fEP, 1, &l_iov, cookie, &r_iov, DAT_COMPLETION_DEFAULT_FLAG );
		TIMEMARK;
		if ( dret != DAT_SUCCESS )
		    {
		    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::TransmitBlob", "Error posting DMA write" )
			<< "Error posting DMA transfer for block " << MLUCLog::kDec
			<< n << " for transfer " << id << " (0x"
			<< MLUCLog::kHex << id << "): "
			<< DatStrError(dret).c_str() << " ("
			<< MLUCLog::kDec << dret << ")." << ENDLOG;
		    failure = true;
		    break;
		    }
		DAT_EVENT blobEvt;
		DAT_COUNT qNr;
		TIMEMARK;
		if ( timeout )
		    dret = dat_evd_wait( cd.fDataEVD, timeout*1000, 1, &blobEvt, &qNr );
		else
		    dret = dat_evd_wait( cd.fDataEVD, DAT_TIMEOUT_INFINITE, 1, &blobEvt, &qNr );
		TIMEMARK;
		if ( dret != DAT_SUCCESS )
		    {
		    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::TransmitBlob", "Error waiting for DMA write completion" )
			<< "Error waiting for completion of DMA transfer for block " << MLUCLog::kDec
			<< n << " for transfer " << id << " (0x"
			<< MLUCLog::kHex << id << "): "
			<< DatStrError(dret).c_str() << " ("
			<< MLUCLog::kDec << dret << ")." << ENDLOG;
		    failure = true;
		    break;
		    }
		if ( blobEvt.event_number != DAT_DTO_COMPLETION_EVENT ||
		     blobEvt.event_data.dto_completion_event_data.status != DAT_DTO_SUCCESS )
		    {
		    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::TransmitBlob", "Error on block transmission" )
			<< "Error transmitting block " << MLUCLog::kDec << n
			<< " for transfer " << id << " (0x"
			<< MLUCLog::kHex << id << "): 0x"
			<< blobEvt.event_number << "/0x"
			<< blobEvt.event_data.dto_completion_event_data.status
			<< MLUCLog::kDec << " ("
			<< blobEvt.event_number << "/"
			<< blobEvt.event_data.dto_completion_event_data.status
			<< ")." << ENDLOG;
		    failure = true;
		    break;
		    }
		sent += l_iov.segment_length;
		}
	    if ( failure )
		break;
	    
	    if ( !useCopy && lmrHandle != DAT_HANDLE_NULL )
		dat_lmr_free( lmrHandle );
	    offset += sizes[n];
	    
	    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::TransmitBlob", "Block transmitted successfully" )
		<< "Successfully transmitted block " << MLUCLog::kDec << n
		<< " for transfer " << id << " (0x"
		<< MLUCLog::kHex << id << ")." << ENDLOG;
	    }
	
	if ( failure )
	    {
	    if ( lmrHandle != DAT_HANDLE_NULL && lmrHandle!=fComHelper.fSendLMRHandle )
		dat_lmr_free( lmrHandle );
	    fComHelper.DisconnectFromRemote( &cd, timeout, timeout, true, false );
	    continue;
	    }
	
	*reinterpret_cast<BCLTransferID*>(fComHelper.fSendBuffer) = id;
	uint32* ptr = reinterpret_cast<uint32*>( reinterpret_cast<uint8*>(fComHelper.fSendBuffer)+sizeof(BCLTransferID) );
	ptr[0] = sizes.size();
	offset = 0;
	for ( unsigned n = 0; n < blockCnt; n++ )
	    {
	    if ( offsets.size() > n )
		offset = offsets[n];
	    ptr[ 1+n*2 ] = offset;
	    ptr[ 1+n*2+1 ] = sizes[n];
	    offset += sizes[n];
	    }
	
	DAT_LMR_TRIPLET l_iov;
	l_iov.pad = 0;
	l_iov.segment_length = sizeof(BCLTransferID)+sizeof(uint32)*(2*sizes.size()+1);
	l_iov.lmr_context = fComHelper.fSendLMRContext;
	l_iov.virtual_address = fComHelper.fSendBuffer;
	DAT_DTO_COOKIE cookie;
	cookie.as_64 = BCLIntUDAPLComHelper::kBlockVectorSendDTOCookie;
	
	TIMEMARK;
	dret = dat_ep_post_send( cd.fEP, 1, &l_iov, cookie, DAT_COMPLETION_DEFAULT_FLAG );
	TIMEMARK;
	if ( dret != DAT_SUCCESS )
	    {
	    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::TransmitBlob", "Error posting block vector send" )
		<< "Error posting block vector send for transfer " << MLUCLog::kDec << id << " (0x"
		<< MLUCLog::kHex << id << "): "
		<< DatStrError(dret).c_str() << " ("
		<< MLUCLog::kDec << dret << ")." << ENDLOG;
	    fComHelper.DisconnectFromRemote( &cd, timeout, timeout, true, false );
	    failure = true;
	    continue;
	    }
	DAT_EVENT blobEvt;
	DAT_COUNT qNr;
	TIMEMARK;
	if ( timeout )
	    dret = dat_evd_wait( cd.fDataEVD, timeout*1000, 1, &blobEvt, &qNr );
	else
	    dret = dat_evd_wait( cd.fDataEVD, DAT_TIMEOUT_INFINITE, 1, &blobEvt, &qNr );
	TIMEMARK;
	if ( dret != DAT_SUCCESS )
	    {
	    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::TransmitBlob", "Error waiting for block vector send completion" )
		<< "Error waiting for completion of block vector send for transfer " << MLUCLog::kDec << id << " (0x"
		<< MLUCLog::kHex << id << "): "
		<< DatStrError(dret).c_str() << " ("
		<< MLUCLog::kDec << dret << ")." << ENDLOG;
	    fComHelper.DisconnectFromRemote( &cd, timeout, timeout, true, false );
	    failure = true;
	    continue;
	    }
	
	if ( blobEvt.event_number != DAT_DTO_COMPLETION_EVENT ||
	     blobEvt.event_data.dto_completion_event_data.status != DAT_DTO_SUCCESS )
	    {
	    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::TransmitBlob", "Error on block vector transmission" )
		<< "Error transmitting block vector for transfer " 
		<< MLUCLog::kDec << id << " (0x"
		<< MLUCLog::kHex << id << "): 0x"
		<< blobEvt.event_number << "/0x"
		<< blobEvt.event_data.dto_completion_event_data.status
		<< MLUCLog::kDec << " ("
		<< blobEvt.event_number << "/"
		<< blobEvt.event_data.dto_completion_event_data.status
		<< ")." << ENDLOG;
	    fComHelper.DisconnectFromRemote( &cd, timeout, timeout, true, false );
	    failure = true;
	    continue;
	    }
	
	LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::TransmitBlob", "Blob transmission completed successfully" )
	    << "Successfully completed transmission of transfer " 
	    << id << " (0x"
	    << MLUCLog::kHex << id << ")." << ENDLOG;
	
	TIMEMARK;
	TIMEMARK;
	fComHelper.DisconnectFromRemote( &cd, timeout, timeout );
	}
    while ( failure );
    TIMEMARK;
    DUMP_TIMEMARKS;
    return 0;
    }

int BCLUDAPLBlobCommunication::GetRemoteBlobAddress( BCLAbstrAddressStruct* msgAddress, BCLUDAPLAddressStruct* blobAddress, bool useTimeout, unsigned long timeout_ms )
    {
    int ret;
    BCLIntUDAPLBlobComMsg msg;
    if ( !fMsgCom )
	return ENODEV;

    vector<TUDAPLRemoteBlobAddress>::iterator iter, end;
    iter = fRemoteBlobAddresses.begin();
    end = fRemoteBlobAddresses.end();
    while ( iter != end )
	{
	if ( fMsgCom->AddressEquality( iter->fMsgAddress, msgAddress ) )
	    {
	    *blobAddress = iter->fBlobAddress;
	    return 0;
	    }
	iter++;
	}

    msg.GetData()->fMsgType = kUDAPLBlobQueryAddressMsg;
    ret = GetQueryID( msg.GetData()->fQueryID );
    if ( ret )
	return ret;
    fDataArrivedSignal.Lock();
    if ( useTimeout )
	ret = fMsgCom->Send( msgAddress, msg.GetData(), timeout_ms );
    else
	ret = fMsgCom->Send( msgAddress, msg.GetData() );
    LOG( MLUCLog::kDebug, "UDAPL Blob Connect", "Address request" )
	<< "Remote Blob Address Request message sent." << ENDLOG;
    if ( ret )
	{
	LOG( MLUCLog::kError, "UDAPL Blob Connect", "Address request" )
	    << "Error sending Remote Blob Address Request message: " 
	    << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	fDataArrivedSignal.Unlock();
	return ret;
	}
    ret = WaitForMessage( msg.GetData()->fQueryID, msg.GetData(), useTimeout, timeout_ms );
    fDataArrivedSignal.Unlock();
    if ( ret )
	return ret;
    BCLUDAPLAddress tac;
    *blobAddress = *(tac.GetData());
    memcpy( blobAddress->fIAName, msg.GetData()->fIAName, 32 );
    blobAddress->fSinFamily = msg.GetData()->fSinFamily;
    blobAddress->fSinPort = msg.GetData()->fSinPort;
    blobAddress->fNodeID = msg.GetData()->fNodeID;
    blobAddress->fConnQual = msg.GetData()->fConnQual;
    return 0;
    }


int BCLUDAPLBlobCommunication::GetQueryID( uint32& id )
    {
    fDataArrivedSignal.Lock();
    id = fCurrentQueryID++;
    fCurrentQueryCount++;
    fDataArrivedSignal.Unlock();
    return 0;
    }

// Note: fDataArrivedSignal must be locked upon entry into this function.
int BCLUDAPLBlobCommunication::WaitForMessage( uint32 queryID, BCLIntUDAPLBlobComMsgStruct* msg, bool useTimeout, unsigned long timeout_ms ) // The fDataArrivedSignal must be locked before entering this method
    {
    bool found = false;
    //     int ret=0;
    BCLIntUDAPLBlobComMsg msgClass;
    //TBCLIntUDAPLBlobComMsgStructContType::iterator iter, end;
    unsigned long msgNdx;
    struct timeval start, stop;
    unsigned long tdiff, twait;
    gettimeofday( &start, NULL );
    //     fDataArrivedSignal.Lock();
    do
	{
	// 	ret = pthread_mutex_lock( &fMsgMutex );
	// 	if ( ret )
	// 	    {
	// 	    return ENXIO;
	// 	    }
	if ( MLUCVectorSearcher<BCLIntUDAPLBlobComMsgStruct,uint32>::FindElement( fMsgs, &FindMsgByQueryID, queryID, msgNdx ) )
	    {
	    found = true;
	    *msg = *(fMsgs.GetPtr( msgNdx ));
	    fMsgs.Remove( msgNdx );
	    fCurrentQueryCount--;
	    }
	else
	    {
	    // 	ret = pthread_mutex_unlock( &fMsgMutex );
	    // 	if ( found )
	    // 	    {
	    // 	    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::WaitForMessage", "Message found" )
	    // 		<< "Desired message (0x" << MLUCLog::kHex << queryID << "  found"
	    // 		<< " ." << ENDLOG;
	    // 	    return 0;
	    // 	    }
	    LOGM( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::WaitForMessage", "Message not found", 200 )
		<< "Desired message (0x" << MLUCLog::kHex << queryID << " not found" 
		<< " ." << ENDLOG;
	    gettimeofday( &stop, NULL );
	    tdiff = (stop.tv_sec-start.tv_sec)*1000+(stop.tv_usec-start.tv_usec)/1000;
	    if ( (useTimeout && tdiff>timeout_ms ) || tdiff > fComHelper.fConnectionTimeout*5 )
		{
		LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::WaitForMessage", "Message not found" )
		    << "Desired message (0x" << MLUCLog::kHex << queryID << "  not found after "
		    << MLUCLog::kDec << tdiff << "ms ." << ENDLOG;
		fDataArrivedSignal.Unlock();
		return ETIMEDOUT;
		}
	    // 	if ( ret )
	    // 	    {
	    // 	    fDataArrivedSignal.Unlock();
	    // 	    return ENXIO;
	    // 	    }
	    if ( useTimeout )
		twait = timeout_ms-tdiff;
	    else
		twait = fComHelper.fConnectionTimeout*5-tdiff;
	    //fDataArrivedSignal.Wait( fComHelper.fConnectionTimeout );
	    fDataArrivedSignal.Wait( twait );
	    }
	}
    while ( !found );
    //     fDataArrivedSignal.Unlock();
    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::WaitForMessage", "Message found" )
	<< "Desired message (0x" << MLUCLog::kHex << queryID << "  found"
	<< " ." << ENDLOG;

    return 0;
    }

bool BCLUDAPLBlobCommunication::FindMsgByQueryID( const BCLIntUDAPLBlobComMsgStruct& msg, const uint32& reference )
    {
    BCLIntUDAPLBlobComMsg msgClass( &msg );
    return msgClass.GetData()->fQueryID == reference;
    }


int BCLUDAPLBlobCommunication::GetFreeBlock( uint64 size, TUDAPLBlockData& block )
    {
    int ret;
    block.fBlockOffset = (uint64)-1;
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	return ret;
    TUDAPLBlockDataContType::iterator iter, end, bestFit;
    bool bestFitSet = false;
    iter = fFreeBlocks.begin();
    end = fFreeBlocks.end();
#if __GNUC__<3
    bestFit = NULL;
#endif
    while ( iter != end )
	{
	if ( iter->fBlockSize >= size )
	    {
	    if ( !bestFitSet || bestFit->fBlockSize>iter->fBlockSize )
		{
		bestFitSet = true;
		bestFit = iter;
		}
	    }
	iter++;
	}
    if ( bestFitSet )
	{
	block.fBlockOffset = bestFit->fBlockOffset;
	block.fBlockSize = size;
	bestFit->fBlockOffset += size;
	bestFit->fBlockSize -= size;
	if ( bestFit->fBlockSize <= 0 )
	    fFreeBlocks.erase( bestFit );
	fUsedBlocks.insert( fUsedBlocks.end(), block );
	}
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	return ret;
    //if ( !bestFit )
    //return ENOSPC;
    return 0;
    }

void BCLUDAPLBlobCommunication::RequestHandler()
    {
    int ret;
    fQuitRequestHandler = false;
    BCLAbstrAddressStruct* addr;
    addr = fMsgCom->NewAddress();
    if ( !addr )
	{
	LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::RequestHandler", "Allocating message address" )
	    << "Out of memory allocating message communication address structure." << ENDLOG;
	GeneralError( ENOMEM, this, NULL );
	return;
	}
    BCLMessageStruct* gMsg;
    BCLIntUDAPLBlobComMsgStruct* msg;
    fRequestHandlerQuitted = false;
    do
	{
	ret = fMsgCom->Receive( addr, gMsg );
	
	if ( !this  )
	    {
	    LOG( MLUCLog::kFatal, "BCLUDAPLBlobCommunication::RequestHandler", "Message receive" )
		<< "No this pointer available. Receive return value: " << strerror(ret) << " ("
		<< MLUCLog::kDec << ret << "). Aborting..." << ENDLOG;
	    return;
	    }
	if ( !fMsgCom  )
	    {
	    LOG( MLUCLog::kFatal, "BCLUDAPLBlobCommunication::RequestHandler", "Message receive" )
		<< "No msg com object available. Receive return value: " << strerror(ret) << " ("
		<< MLUCLog::kDec << ret << "). Aborting..." << ENDLOG;
	    fRequestHandlerQuitted = true;
	    return;
	    }
	if ( ret == ETIMEDOUT )
	    {
	    continue;
	    }
	if ( ret )
	    {
	    switch ( ret )
		{
		case ENXIO: 
		    // Fallthrough
		case ENODEV:
		    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::RequestHandler", "Message receive" )
			<< "Error when trying to to receive message: " << strerror(ret) << " (" << ret 
			<< "). Aborting message loop..." << ENDLOG;
		    GeneralError( ENODEV, this, NULL );
		    fRequestHandlerQuitted = true;
		    return;
		default:
		    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::RequestHandler", "Message receive" )
			<< "Error when trying to to receive message: " << strerror(ret) << " (" << ret << ")."
			<< ENDLOG;
		    break;
		}
	    continue;
	    }
	if ( !gMsg )
	    {
	    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::RequestHandler", "Message receive" )
		<< "Error when trying to to receive message: " << strerror(ret) << " (" << ret << ")."
		<< ENDLOG;
	    GeneralError( ENODEV, this, NULL );
	    continue;
	    }
	msg = (BCLIntUDAPLBlobComMsgStruct*)gMsg;
	LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::RequestHandler", "Message received" )
	    << "Received message with query ID 0x" << MLUCLog::kHex << msg->fQueryID
	    << " (" << MLUCLog::kDec << msg->fQueryID << ")." << ENDLOG;
	
	switch ( msg->fMsgType )
	    {
	    case kUDAPLBlobQueryAddressMsg:
		LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::RequestHandler", "Address query" )
		    << "Address query received from remote node " << *addr << "." << ENDLOG;
		memcpy( msg->fIAName, fUDAPLAddress.fIAName, 32 );
		msg->fSinFamily = fUDAPLAddress.fSinFamily;
		msg->fSinPort = fUDAPLAddress.fSinPort;
		msg->fNodeID = fUDAPLAddress.fNodeID;
		msg->fConnQual = fUDAPLAddress.fConnQual;
		msg->fMsgType = kUDAPLBlobAddressMsg;
		if ( fReplyTimeout )
		    ret = fMsgCom->Send( addr, msg, fReplyTimeout );
		else
		    ret = fMsgCom->Send( addr, msg );
		if ( ret )
		    {
		    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::RequestHandler", "Address message send" )
			<< "Error when trying to send address message reply: " << strerror(ret) << " ("
			<< ret << ")." << ENDLOG;
		    GeneralError( ret, this, addr );
		    }
		LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::RequestHandler", "Address query" )
		    << "Address reply sent to remote node " << *addr << "." << ENDLOG;
		break;
	    case kUDAPLBlobQueryBlobSizeMsg:
		LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::RequestHandler", "Blob size query" )
		    << "Blob size query received from remote node " << *addr 
		    << ": 0x" << MLUCLog::kHex << fBlobBufferSize << " (" << MLUCLog::kDec
		    << fBlobBufferSize << ")." << ENDLOG;
		msg->fMsgType = kUDAPLBlobBlobSizeReplyMsg;
		msg->fBlockSize = fBlobBufferSize;
		if ( fReplyTimeout )
		    ret = fMsgCom->Send( addr, msg, fReplyTimeout );
		else
		    ret = fMsgCom->Send( addr, msg );
		if ( ret )
		    {
		    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::RequestHandler", "Blob size message send" )
			<< "Error when trying to send blob size message reply: " << strerror(ret) << " ("
			<< ret << ")." << ENDLOG;
		    GeneralError( ret, this, addr );
		    }
		LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::RequestHandler", "Blob size query" )
		    << "Blob size reply sent to remote node " << *addr << "." << ENDLOG;
		break;
	    case kUDAPLBlobQueryFreeBlockMsg:
		LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::RequestHandler", "Blob query" )
		    << "Blob query for 0x" << MLUCLog::kHex << msg->fBlockSize << " (" << MLUCLog::kDec
		    << msg->fBlockSize << ") bytes received from remote node " << *addr 
		    << "." << ENDLOG;
		TUDAPLBlockData block;
		ret = GetFreeBlock( msg->fBlockSize, block );
		if ( ret )
		    {
		    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::RequestHandler", "Free block request" )
			<< "Error while trying to allocate free block request for " << MLUCLog::kDec 
			<< msg->fBlockSize
			<< " bytes: " << strerror(ret) << " (" << ret << ")." << ENDLOG;
		    GeneralError( ret, this, addr );
		    }
		if ( block.fBlockOffset==(uint32)-1 )
		    {
		    LOG( MLUCLog::kWarning, "BCLUDAPLBlobCommunication::RequestHandler", "Free block request" )
			<< "Could not allocate requested free block of " << MLUCLog::kDec << msg->fBlockSize
			<< " bytes." << ENDLOG;
		    }
		msg->fBlockOffset = block.fBlockOffset;
		msg->fMsgType = kUDAPLBlobFreeBlockMsg;
		if ( fReplyTimeout )
		    ret = fMsgCom->Send( addr, msg, fReplyTimeout );
		else
		    ret = fMsgCom->Send( addr, msg );
		if ( ret )
		    {
		    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::RequestHandler", "block message send" )
			<< "Error when trying to send block message reply: " << strerror(ret) << " ("
			<< ret << ")." << ENDLOG;
		    GeneralError( ret, this, addr );
		    }
		break;
	    case kUDAPLBlobConnectRequestMsg:
		{
		LOG( MLUCLog::kInformational, "BCLUDAPLBlobCommunication::RequestHandler", "Connection request" )
		    << "Connection request received." << ENDLOG;
		BCLUDAPLAddress blbAddr;
		memcpy( blbAddr.GetData()->fIAName, msg->fIAName, 32 );
		blbAddr.GetData()->fSinFamily = msg->fSinFamily;
		blbAddr.GetData()->fSinPort = msg->fSinPort;
		blbAddr.GetData()->fNodeID = msg->fNodeID;
		blbAddr.GetData()->fConnQual = msg->fConnQual;
		ret = Connect( addr, blbAddr.GetData(), false, fReplyTimeout, fReplyTimeout );
		if ( ret )
		    {
		    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::RequestHandler", "Requested connect failed" )
			<< "Error making requested connection to " << *addr << ": " << strerror(ret) 
			<< " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
		    GeneralError( ret, this, addr );
		    }
		break;
		}
	    case kUDAPLBlobDisconnectMsg:
		{
		LOG( MLUCLog::kInformational, "BCLUDAPLBlobCommunication::RequestHandler", "Disconnect request" )
		    << "Disonnect request received." << ENDLOG;
		BCLUDAPLAddress blbAddr;
		memcpy( blbAddr.GetData()->fIAName, msg->fIAName, 32 );
		blbAddr.GetData()->fSinFamily = msg->fSinFamily;
		blbAddr.GetData()->fSinPort = msg->fSinPort;
		blbAddr.GetData()->fNodeID = msg->fNodeID;
		blbAddr.GetData()->fConnQual = msg->fConnQual;
		Disconnect( addr, blbAddr.GetData(), false, fReplyTimeout, fReplyTimeout );
		break;
		}
	    case kUDAPLBlobAddressMsg:
		if ( 1 )
		    {
		    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::RequestHandler", "Address reply" )
			<< "Address reply received from node " << *addr << "." << ENDLOG;
		    }
		else
		    case kUDAPLBlobFreeBlockMsg:
    if ( 1 )
	{
	LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::RequestHandler", "Blob reply" )
	    << "Blob reply received from node " << *addr << "." << ENDLOG;
	}
    else
	case kUDAPLBlobBlobSizeReplyMsg:
    {
    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::RequestHandler", "Blob size reply" )
	<< "Blob size reply received from node " << *addr << "." << ENDLOG;
    }
		// 		ret = pthread_mutex_lock( &fMsgMutex );
		// 		if ( ret )
		// 		    {
		// 		    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::RequestHandler", "Mutex error" )
		// 			<< "Error trying to lock message mutex for inserting message: " 
		// 			<< strerror(ret) << " (" << ret << ")." << ENDLOG;
		// 		    GeneralError( ret, this, addr );
		// 		    break;
		// 		    }
		fDataArrivedSignal.Lock();
		fMsgs.Add( *msg );
		fDataArrivedSignal.Unlock();
		// 		ret = pthread_mutex_unlock( &fMsgMutex );
		// 		if ( ret )
		// 		    {
		// 		    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::RequestHandler", "Mutex error" )
		// 			<< "Error trying to unlock message mutex for inserting message: " 
		// 			<< strerror(ret) << " (" << ret << ")." << ENDLOG;
		// 		    GeneralError( ret, this, addr );
		// 		    }
		fDataArrivedSignal.Signal();
		break;
	    case kUDAPLBlobQuitMsg:
		fQuitRequestHandler = true; 
		break;
	    }
	if ( gMsg )
	    fMsgCom->ReleaseMsg( gMsg );
	}
    while ( !fQuitRequestHandler );
    LOG( MLUCLog::kInformational, "BCLUDAPLBlobCommunication::RequestHandler", "Request handler shutting down" )
	<< "Shutting down UDAPL blob com request handler..." << ENDLOG;
    fRequestHandlerQuitted = true;
    }

void BCLUDAPLBlobCommunication::QuitRequestHandler()
    {
    //fQuitRequestHandler = true;
    int ret=-1;
    BCLIntUDAPLBlobComMsg msg;
    msg.GetData()->fMsgType = kUDAPLBlobQuitMsg;
#ifdef UDAPL_SIMPLE_TIMEOUT
    unsigned long timeoutCount=0;
#endif
    if ( !fMsgCom )
	{
	LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::QuitRequestHandler", "No msg com object" )
	    << "Error sending request handler quit message. No msg communication object..." << ENDLOG;
	GeneralError( EFAULT, this, &fUDAPLAddress );
	}
#ifdef UDAPL_TIMEOUT
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 3000000;
    gettimeofday( &start, NULL );
#endif
    while ( !fRequestHandlerQuitted )
	{
	BCLAbstrAddressStruct* addr = fMsgCom->NewAddress();
	memcpy( addr, fMsgCom->GetAddress(), fMsgCom->GetAddress()->fLength );
	//*addr = *(fMsgCom->GetAddress());
	LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::QuitRequestHandler", "Sending Quit message" )
	    << "Trying to send quit message to request handler at address " << *addr << "." << ENDLOG;
	if ( addr->fComID == kUDAPLComID )
	    {
	    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::QuitRequestHandler", "Sending Quit message" )
		<< "Request handler address: " << *(BCLUDAPLAddressStruct*)addr << "." << ENDLOG;
	    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::QuitRequestHandler", "Sending Quit message" )
		<< "Msg com address: " << *(BCLUDAPLAddressStruct*)(fMsgCom->GetAddress()) << "." << ENDLOG;
	    }
	ret = fMsgCom->Send( addr, msg.GetData() );
	fMsgCom->DeleteAddress( addr );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::QuitRequestHandler", "Sending Quit message" )
		<< "Error trying to send quit message to request handler: " << strerror(ret) << " ("
		<< ret << ")." << ENDLOG;
	    GeneralError( ret, this, &fUDAPLAddress );
	    // XXX
	    if ( ret == ENODEV || ret == ENXIO )
		break;
	    }
	usleep( 0 );
#ifdef UDAPL_TIMEOUT
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	if ( deltaT>timeLimit )
	    break;
#endif
#ifdef UDAPL_SIMPLE_TIMEOUT
	if ( timeoutCount++ >= SIMPLE_TIMEOUT_MAX )
	    break;
#endif
	}
#ifdef UDAPL_SIMPLE_TIMEOUT
    if ( timeoutCount >= SIMPLE_TIMEOUT_MAX || ret )
	{
	fRequestThread.Abort();
	fRequestHandlerQuitted = true;
	return;
	}
#endif
    fRequestThread.Join();
    }


int BCLUDAPLBlobCommunication::DataReceived( DAT_EVD_HANDLE /*evd*/, 
	DAT_EP_HANDLE ep, 
	DAT_VADDR recvBuffer,
	DAT_VLEN recvBufferSize,
	DAT_LMR_CONTEXT recvLMRContext,
	DAT_LMR_TRIPLET* iov, 
	uint64 old_cookie, 
	DAT_EVENT& event )
    {
    DAT_RETURN ret;
    
    iov->lmr_context = recvLMRContext;
    iov->pad = 0;
    iov->virtual_address = recvBuffer;
    iov->segment_length = recvBufferSize;
    DAT_DTO_COOKIE cookie;
    cookie.as_64 = old_cookie;
    cookie.as_64++;

    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::DataReceived", "New Data" )
	<< "New data available on connection endpoint " << MLUCLog::kDec << (unsigned long)ep
	<< "." << ENDLOG;
    if ( event.event_number != DAT_DTO_COMPLETION_EVENT || 
	 event.event_data.dto_completion_event_data.status != DAT_DTO_SUCCESS )
	{
	LOG( MLUCLog::kWarning, "BCLUDAPLBlobCommunication::DataReceived", "Unexpected event type" )
	    << "Unexpected event type received: " << MLUCLog::kDec
	    << (unsigned long)event.event_number << " / "
	    << event.event_data.dto_completion_event_data.status
	    << " - expected: " << MLUCLog::kDec
	    << (unsigned long)DAT_DTO_COMPLETION_EVENT << " / "
	    << DAT_DTO_SUCCESS
	    << "." << ENDLOG;
	if ( event.event_number == DAT_DTO_COMPLETION_EVENT )
	    ret = dat_ep_post_recv( ep, 
				    1, iov,
				    cookie, DAT_COMPLETION_DEFAULT_FLAG );
	
	if ( ret != DAT_SUCCESS )
	    {
	    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::DataReceived", "Error posting connection receive" )
		<< "Error trying to post message receive for incoming connection: " << DatStrError(ret).c_str() << " ("
		<< MLUCLog::kDec << ")." << ENDLOG;
	    // XXX Cleanup properly
	    return EIO;
	    }
	if ( event.event_number == DAT_DTO_COMPLETION_EVENT )
	    return EAGAIN;
	else
	    return EBADMSG;
	}
    if ( event.event_data.dto_completion_event_data.user_cookie.as_64 != old_cookie )
	{
	LOG( MLUCLog::kWarning, "BCLUDAPLBlobCommunication::DataReceived", "Unexpected DTO Type" )
	    << "Unexpected DTO event type received: " << MLUCLog::kDec
	    << (unsigned long long)event.event_data.dto_completion_event_data.user_cookie.as_64
	    << " received; " << (unsigned long long)old_cookie
	    << " expected." << ENDLOG;
	ret = dat_ep_post_recv( ep, 
				1, iov,
				cookie, DAT_COMPLETION_DEFAULT_FLAG );
	
	if ( ret != DAT_SUCCESS )
	    {
	    LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::DataReceived", "Error posting connection receive" )
		<< "Error trying to post message receive for incoming connection: " << DatStrError(ret).c_str() << " ("
		<< MLUCLog::kDec << ")." << ENDLOG;
	    // XXX Cleanup properly
	    return EIO;
	    }
	return EAGAIN;
	}
    BCLTransferID transferID;
    transferID = *reinterpret_cast<BCLTransferID*>( recvBuffer );
    uint32* ptr = reinterpret_cast<uint32*>( (reinterpret_cast<uint8*>(recvBuffer))
					     + sizeof(BCLTransferID) );
    uint32 cnt = ptr[0];
    bool notify = false;
    // Remove the other blocks from the backlog queue:
    if ( fReceiveNotificationEnabled )
	{
	notify = true;
	fBlobReceivedSignal.Lock();
	}
    for ( unsigned long n = 0; n<cnt; n++ )
	{
	LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::DataReceived", "Data Block Received" )
	    << "Received data block " << MLUCLog::kDec << n << ": Transfer ID: "
	    << transferID << " (0x" << MLUCLog::kHex << transferID << ") /  Offset: "
	    << MLUCLog::kDec << ptr[ 1+n*2 ] << " (0x" << MLUCLog::kHex
	    << ptr[ 1+n*2 ] << ") / Size: " << MLUCLog::kDec << ptr[ 1+n*2+1 ]
	    << " (0x" << ptr[ 1+n*2+1 ] << ")." << ENDLOG;
	if ( notify )
	    {
	    TUDAPLReceivedBlobData rbd;
	    rbd.fTransferID = transferID;
	    rbd.fTransferBlocksLeft = cnt-1-n;
	    rbd.fBlockOffset = ptr[ 1+n*2 ];
	    rbd.fBlockSize = ptr[ 1+n*2+1 ];
	    fReceivedBlocks.Add( rbd );
	    }
	}
    if ( notify )
	{
	fBlobReceivedSignal.Unlock();
	fBlobReceivedSignal.Signal();
	}

    LOG( MLUCLog::kDebug, "BCLUDAPLBlobCommunication::DataReceived", "Data receive finished" )
	<< "Finished receiving of data." << ENDLOG;
    ret = dat_ep_post_recv( ep, 
			    1, iov,
			    cookie, DAT_COMPLETION_DEFAULT_FLAG );
	
    if ( ret != DAT_SUCCESS )
	{
	LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::DataReceived", "Error posting connection receive" )
	    << "Error trying to post message receive for incoming connection: " << DatStrError(ret).c_str() << " ("
	    << MLUCLog::kDec << ")." << ENDLOG;
	// XXX Cleanup properly
	return EIO;
	}
    return 0;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
