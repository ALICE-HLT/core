/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#ifdef USE_TCP

#include "BCLTCPMsgCommunication.hpp"
#include "BCLTCPAddress.hpp"
#include "BCLTCPAddressOps.hpp"
#include "BCLMessage.hpp"
#include "BCLAddressLogger.hpp"
#include "MLUCLog.hpp"
#include <errno.h>
#include <stdlib.h>
#include <memory.h>
#include <sys/time.h>
#include <poll.h>


BCLTCPMsgCommunication::BCLTCPMsgCommunication( int slotCntExp2 ):
    fComHelper( this ),
    fMessageAllocCache( 3, true, false ), 
    fAddressAllocCache( 3, true, false ), 
    fMessages( slotCntExp2, true ),
    fMsgProgress( slotCntExp2, true )
    {
#ifdef INITIAL_CONDITIONSEM_UNLOCK
    // Define this when condition semaphore objects should always be unlocked initially.
    fDataArrivedSignal.Unlock();
#endif
    fMsgSeqNr = 0;
#ifdef BIGTCPLOCK
    pthread_mutex_init( &fBigTCPLockTest, NULL );
#endif
    }

BCLTCPMsgCommunication::~BCLTCPMsgCommunication()
    {
    if ( fComHelper.fSocket != -1 )
	Unbind();
#ifdef BIGTCPLOCK
    pthread_mutex_destroy( &fBigTCPLockTest );
#endif
    }

int BCLTCPMsgCommunication::Bind( BCLAbstrAddressStruct* address )
    {
#ifdef BIGTCPLOCK
    pthread_mutex_lock( &fBigTCPLockTest );
#endif
    int ret;
    if ( !address )
	{
#ifdef BIGTCPLOCK
	pthread_mutex_unlock( &fBigTCPLockTest );
#endif
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Bind", "Address error" )
	    << "Wrong address (NULL pointer) in TCP Bind" << ENDLOG;
	BindError( EFAULT, this, address, NULL );
	return EFAULT;
	}
    if ( address->fComID != kTCPComID )
	{
#ifdef BIGTCPLOCK
	pthread_mutex_unlock( &fBigTCPLockTest );
#endif
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Bind", "Address error" )
	    << "Wrong address ID " << address->fComID << " in TCP Bind. (expected " 
	    << kTCPComID << ")."<< ENDLOG;
	BindError( EINVAL, this, address, NULL );
	return EINVAL;
	}
    fTCPAddress = *(BCLTCPAddressStruct*)address;
    fAddress = &fTCPAddress;
    ret = fComHelper.Bind( (BCLTCPAddressStruct*)fAddress );
#ifdef BIGTCPLOCK
    pthread_mutex_unlock( &fBigTCPLockTest );
#endif
    return ret;
    }

int BCLTCPMsgCommunication::Unbind()
    {
#ifdef BIGTCPLOCK
    pthread_mutex_lock( &fBigTCPLockTest );
#endif
    int tmp = fComHelper.Unbind();
#ifdef BIGTCPLOCK
    pthread_mutex_unlock( &fBigTCPLockTest );
#endif
    return tmp;
    }

int BCLTCPMsgCommunication::Connect( BCLAbstrAddressStruct* address, bool openOnDemand )
    {
#ifdef BIGTCPLOCK
    pthread_mutex_lock( &fBigTCPLockTest );
#endif
    if ( !address )
	{
#ifdef BIGTCPLOCK
	pthread_mutex_unlock( &fBigTCPLockTest );
#endif
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Connect", "Address error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address (NULL pointer) in TCP Connect" << ENDLOG;
	ConnectionError( EFAULT, this, address );
	return EFAULT;
	}
    if ( address->fComID != kTCPComID )
	{
#ifdef BIGTCPLOCK
	pthread_mutex_unlock( &fBigTCPLockTest );
#endif
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Connect", "Address error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address ID " << address->fComID << " in TCP Connect. (expected " 
	    << kTCPComID << ")."<< ENDLOG;
	ConnectionError( EINVAL, this, address );
	return EINVAL;
	}
    int tmp = fComHelper.Connect( (BCLTCPAddressStruct*)address, false, 0, openOnDemand );
#ifdef BIGTCPLOCK
    pthread_mutex_unlock( &fBigTCPLockTest );
#endif
    return tmp;
    }

int BCLTCPMsgCommunication::Connect( BCLAbstrAddressStruct* address, unsigned long timeout_ms, bool openOnDemand )
    {
#ifdef BIGTCPLOCK
    pthread_mutex_lock( &fBigTCPLockTest );
#endif
    if ( !address )
	{
#ifdef BIGTCPLOCK
	pthread_mutex_unlock( &fBigTCPLockTest );
#endif
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Connect", "Address error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address (NULL pointer) in TCP Connect" << ENDLOG;
	ConnectionError( EFAULT, this, address );
	return EFAULT;
	}
    if ( address->fComID != kTCPComID )
	{
#ifdef BIGTCPLOCK
	pthread_mutex_unlock( &fBigTCPLockTest );
#endif
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Connect", "Address error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address ID " << address->fComID << " in TCP Connect. (expected " 
	    << kTCPComID << ")."<< ENDLOG;
	ConnectionError( EINVAL, this, address );
	return EINVAL;
	}
    int tmp = fComHelper.Connect( (BCLTCPAddressStruct*)address, true, timeout_ms, openOnDemand );
#ifdef BIGTCPLOCK
    pthread_mutex_unlock( &fBigTCPLockTest );
#endif
    return tmp;
    }

bool BCLTCPMsgCommunication::CanLockConnection()
    {
    return false;
    }

int BCLTCPMsgCommunication::LockConnection( BCLAbstrAddressStruct* )
    {
    return ENOTSUP;
    }

int BCLTCPMsgCommunication::LockConnection( BCLAbstrAddressStruct*, unsigned long )
    {
    return ENOTSUP;
    }

int BCLTCPMsgCommunication::UnlockConnection( BCLAbstrAddressStruct* )
    {
    return ENOTSUP;
    }

int BCLTCPMsgCommunication::UnlockConnection( BCLAbstrAddressStruct*, unsigned long )
    {
    return ENOTSUP;
    }

int BCLTCPMsgCommunication::Disconnect( BCLAbstrAddressStruct* address )
    {
#ifdef BIGTCPLOCK
    pthread_mutex_lock( &fBigTCPLockTest );
#endif
    if ( !address )
	{
#ifdef BIGTCPLOCK
	pthread_mutex_unlock( &fBigTCPLockTest );
#endif
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Disconnect", "Address error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address (NULL pointer) in TCP Disconnect" << ENDLOG;
	ConnectionError( EFAULT, this, address );
	return EFAULT;
	}
    if ( address->fComID != kTCPComID )
	{
#ifdef BIGTCPLOCK
	pthread_mutex_unlock( &fBigTCPLockTest );
#endif
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Disconnect", "Address error" )
	    << "Wrong address ID " << address->fComID << " in TCP Disconnect. (expected " 
	    << kTCPComID << ")."<< ENDLOG;
	ConnectionError( EINVAL, this, address );
	return EINVAL;
	}
    int tmp = fComHelper.Disconnect( (BCLTCPAddressStruct*)address, false, 0 );
#ifdef BIGTCPLOCK
    pthread_mutex_unlock( &fBigTCPLockTest );
#endif
    return tmp;
    }

int BCLTCPMsgCommunication::Disconnect( BCLAbstrAddressStruct* address, unsigned long timeout_ms )
    {
#ifdef BIGTCPLOCK
    pthread_mutex_lock( &fBigTCPLockTest );
#endif
    if ( !address )
	{
#ifdef BIGTCPLOCK
	pthread_mutex_unlock( &fBigTCPLockTest );
#endif
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Disconnect", "Address error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address (NULL pointer) in TCP Disconnect" << ENDLOG;
	ConnectionError( EFAULT, this, address );
	return EFAULT;
	}
    if ( address->fComID != kTCPComID )
	{
#ifdef BIGTCPLOCK
	pthread_mutex_unlock( &fBigTCPLockTest );
#endif
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Disconnect", "Address error" )
	    << "Wrong address ID " << address->fComID << " in TCP Disconnect. (expected " 
	    << kTCPComID << ")."<< ENDLOG;
	ConnectionError( EINVAL, this, address );
	return EINVAL;
	}
    int tmp = fComHelper.Disconnect( (BCLTCPAddressStruct*)address, true, timeout_ms );
#ifdef BIGTCPLOCK
    pthread_mutex_unlock( &fBigTCPLockTest );
#endif
    return tmp;
    }

int BCLTCPMsgCommunication::Send( BCLAbstrAddressStruct* address, BCLMessageStruct* msg, 
				  BCLErrorCallback* callback )
    {
#ifdef BIGTCPLOCK
    pthread_mutex_lock( &fBigTCPLockTest );
#endif
    int tmp = Send( address, msg, false, 0, callback );
#ifdef BIGTCPLOCK
    pthread_mutex_unlock( &fBigTCPLockTest );
#endif
    return tmp;
    }

int BCLTCPMsgCommunication::Send( BCLAbstrAddressStruct* address, BCLMessageStruct* msg, 
				  unsigned long timeout_ms, BCLErrorCallback* callback )
    {
#ifdef BIGTCPLOCK
    pthread_mutex_lock( &fBigTCPLockTest );
#endif
    int tmp = Send( address, msg, true, timeout_ms, callback );
#ifdef BIGTCPLOCK
    pthread_mutex_unlock( &fBigTCPLockTest );
#endif
    return tmp;
    }

int BCLTCPMsgCommunication::Receive( BCLAbstrAddressStruct* addr, BCLMessageStruct*& msg, BCLErrorCallback* callback )
    {
    int tmp = Receive( (BCLTCPAddressStruct*)addr, msg, false, 0, callback );
    return tmp;
    }

int BCLTCPMsgCommunication::Receive( BCLAbstrAddressStruct* addr, BCLMessageStruct*& msg, unsigned long timeout, BCLErrorCallback* callback )
    {
    int tmp = Receive( (BCLTCPAddressStruct*)addr, msg, true, timeout, callback );
    return tmp;
    }

void BCLTCPMsgCommunication::Reset()
    {
    // Is there anything to reset for this??
    }

int BCLTCPMsgCommunication::ReleaseMsg( BCLMessageStruct* msg )
    {
#ifdef BIGTCPLOCK
    pthread_mutex_lock( &fBigTCPLockTest );
#endif
    FreeMessage( msg );
#ifdef BIGTCPLOCK
    pthread_mutex_unlock( &fBigTCPLockTest );
#endif
    return 0;
    }

uint32 BCLTCPMsgCommunication::GetAddressLength()
    {
    return sizeof(BCLTCPAddressStruct);
    }

uint32 BCLTCPMsgCommunication::GetComID()
    {
    return kTCPComID;
    }

BCLTCPAddressStruct* BCLTCPMsgCommunication::NewAddress()
    {
#ifdef BIGTCPLOCK
    pthread_mutex_lock( &fBigTCPLockTest );
#endif
    BCLTCPAddressStruct* addr = (BCLTCPAddressStruct*)fAddressAllocCache.Get( sizeof(BCLTCPAddressStruct) );
    if ( !addr )
	{
#ifdef BIGTCPLOCK
	pthread_mutex_unlock( &fBigTCPLockTest );
#endif
	return NULL;
	}
    *addr = *(fDefaultAddress.GetData());
//     BCLTCPAddressStruct* addr = new BCLTCPAddressStruct;
//     if ( !addr )
// 	return NULL;
//     BCLTCPAddress addrClass;
//     *addr = *(addrClass.GetData());
    addr->fComID = kTCPComID;
#ifdef BIGTCPLOCK
    pthread_mutex_unlock( &fBigTCPLockTest );
#endif
    return addr;
    }

void BCLTCPMsgCommunication::DeleteAddress( BCLAbstrAddressStruct* address )
    {
#ifdef BIGTCPLOCK
    pthread_mutex_lock( &fBigTCPLockTest );
#endif
    fAddressAllocCache.Release( (uint8*)address );
#ifdef BIGTCPLOCK
    pthread_mutex_unlock( &fBigTCPLockTest );
#endif
    //delete (BCLTCPAddressStruct*)address;
    }

bool BCLTCPMsgCommunication::AddressEquality( const BCLAbstrAddressStruct* addr1, const BCLAbstrAddressStruct* addr2 )
    {
    if ( addr1->fComID != kTCPComID || addr2->fComID != kTCPComID )
	return false;
    return *(BCLTCPAddressStruct*)addr1 == *(BCLTCPAddressStruct*)addr2;
    }

BCLTCPMsgCommunication::operator bool()
    {
    return true;
    }

BCLMessageStruct* BCLTCPMsgCommunication::NewMessage( uint32 size )
    {
#if 1
    return (BCLMessageStruct*)fMessageAllocCache.Get( size );
#else
#warning Debug code for testing, real code that should be used is above
    return (BCLMessageStruct*)malloc( size );
#endif
//     return (BCLMessageStruct*)malloc( size );
    //return (BCLMessageStruct*)new uint8[ size ];
    }

void BCLTCPMsgCommunication::FreeMessage( BCLMessageStruct* msg )
    {
#if 1
    fMessageAllocCache.Release( (uint8*)msg );
#else
#warning Debug code for testing, real code that should be used is above
    free( msg );
#endif
//     free( msg );
    //delete [] (uint8*)msg;
    }

int BCLTCPMsgCommunication::Send( BCLAbstrAddressStruct* address, BCLMessageStruct* msg, 
				  bool useTimeout, unsigned long timeout_ms, BCLErrorCallback* callback )
    {
    // Check that the address and msg are valid.
    if ( !address || !msg )
	{
	if ( !address )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Send", "Address error" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address (NULL pointer) in TCP Send" << ENDLOG;
	    }
	else
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Send", "Message error" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong message (NULL pointer) in TCP Send" << ENDLOG;
	    }
	MsgSendError( EFAULT, this, address, msg, callback );
	return EFAULT;
	}
    // Check that the address is of the right type.
    if ( address->fComID != kTCPComID )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Send", "Address error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address ID " << address->fComID << " in TCP Send. (expected " 
	    << kTCPComID << ")."<< ENDLOG;
	MsgSendError( EINVAL, this, address, msg, callback );
	return EINVAL;
	}
    // Check that the address has the correct length.
    if ( address->fLength != sizeof(BCLTCPAddressStruct) )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Send", "Address error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address size " << MLUCLog::kDec << address->fLength 
	    << " (expected " << sizeof(BCLTCPAddressStruct) << ") in TCP Send." << ENDLOG;
	AddressError( EMSGSIZE, this, address, callback );
	return EMSGSIZE;
	}
    // Check that the message has a certain mimimum required size.
    // Beyond that size we cannot assume anything.
    if ( msg->fLength < sizeof(BCLMessageStruct) )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Send", "Message error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong message size " << MLUCLog::kDec << msg->fLength 
	    << " (expected at least" << sizeof(BCLMessageStruct) << ") in TCP Send." << ENDLOG;
	MsgSendError( EMSGSIZE, this, address, msg, callback );
	return EMSGSIZE;
	}
    // Make sure that possible SEGVs don't occur while the other side is waiting 
    // for the rest of our data...
    uint8 *tmp, b;
    tmp = (uint8*)msg;
    b = *tmp;
    b = *(tmp+msg->fLength-1);
    uint32 tmpMsgID;
    BCLNetworkData::Transform( fMsgSeqNr, tmpMsgID, kNativeByteOrder, msg->fDataFormats[kCurrentByteOrderIndex] );
    fMsgSeqNr++;
    msg->fMsgID = tmpMsgID;
    struct timeval oldTimeout;

#ifdef LOG_MESSAGE_CONTENTS
    MLUCString tmp1;
    char tmp2[1024];
    for ( unsigned b=0; b<msg->fLength; b++ )
	{
	sprintf( tmp2, " 0x%02X", (unsigned)((uint8*)msg)[b] );
	tmp1 += tmp2;
	}
    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Send", "New Message" )
	<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << " message to send dump:" 
	<< tmp1.c_str() << "." << ENDLOG;
#endif

    bool transferAborted = false;
    do
	{
	BCLIntTCPComHelper::TTCPConnectionData condat;
	BCLIntTCPComHelper::TTCPConnectionData* connectData = &condat;
	condat.fAddress = *(BCLTCPAddressStruct*)address;
	int ret;
	// Check for a connection and establish if needed...
	ret = fComHelper.ConnectToRemote( (BCLTCPAddressStruct*)address, (BCLIntTCPComHelper::TTCPConnectionData*)connectData, true, useTimeout, timeout_ms );
	if ( ret )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Send", "Connection error" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Error building connection to remote address " << *(BCLTCPAddressStruct*)address
		<< " in TCP Send: " << strerror( errno ) << " (" << errno << ")." << ENDLOG;
	    MsgSendError( ret, this, address, msg, callback );
	    fComHelper.DisconnectFromRemote( (BCLIntTCPComHelper::TTCPConnectionData*)connectData, useTimeout, timeout_ms );
	    return ret;
	    }
	transferAborted = false;

	if ( useTimeout )
	    {
	    socklen_t sockoptlen = sizeof(oldTimeout);
	    ret = getsockopt( connectData->fSocket, SOL_SOCKET, SO_SNDTIMEO, &oldTimeout, &sockoptlen );
	    if ( ret==-1 )
		{
		ret = errno;

		
		//LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Send", "Socket Timeout Get Error" )
		LOGCG( gBCLLogLevelRef, MLUCLog::kError, "TCP Send", "Socket Timeout Get Error", tmpLog )
		    << "Error getting TCP send timeout from socket " << MLUCLog::kDec << connectData->fSocket
		    << " while trying to send to message address ";
		BCLAddressLogger::LogAddress( tmpLog, *address ) 
		    << ": " << strerror(ret) << " (" 
		    << MLUCLog::kDec << ret << ")." << ENDLOG;
		}
	    else
		{
		struct timeval newTimeout;
		newTimeout.tv_sec = timeout_ms/1000;
		newTimeout.tv_usec = (timeout_ms-(newTimeout.tv_sec*1000))*1000;
		ret = setsockopt( connectData->fSocket, SOL_SOCKET, SO_SNDTIMEO, &newTimeout, sizeof(newTimeout) );
		if ( ret==-1 )
		    {
		    ret = errno;
		    //LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Send", "Socket Timeout Set Error" )
		    LOGCG( gBCLLogLevelRef, MLUCLog::kError, "TCP Send", "Socket Timeout Set Error", tmpLog )
			<< "Error getting TCP send timeout from socket while trying to send to message address ";
		    BCLAddressLogger::LogAddress( tmpLog, *address ) 
			<< ": " << strerror(ret) << " (" 
			<< MLUCLog::kDec << ret << ")." << ENDLOG;
		    }
		}
	    }
    
	
	// Really do the send now.
	// First we send our address
	unsigned int count = 0;
	bool looped = false;
#ifdef TCPCOM_NO_POLL
	fd_set sockets;
#endif
#ifdef TCPCOM_NO_POLL
	struct timeval tv, *ptv;
#endif

	// Now we send the message.
	BCLMessage l_msg( msg );
	uint8* bp = (uint8*)msg;
	do
	    {
	    ret = write( connectData->fSocket, bp+count, l_msg.GetData()->fLength-count );
	    if ( ret<0 && errno!=EAGAIN )
		break;
	    if ( ret==0 && errno!=EAGAIN && looped )
		break;
	    looped = true;
	    count += ret;
	    if ( count >= l_msg.GetData()->fLength /*-count*/ )
		break;
	    do
		{
#ifdef TCPCOM_NO_POLL
		FD_ZERO( &sockets );
		FD_SET( connectData->fSocket, &sockets );
		errno = 0;
		if ( useTimeout )
		    {
		    tv.tv_sec = timeout_ms/1000;
		    tv.tv_usec = (timeout_ms-tv.tv_sec*1000)*1000;
		    ptv = &tv;
		    }
		else
		    ptv = NULL;
		ret = select( connectData->fSocket+1, NULL, &sockets, NULL, ptv );
#else
		pollfd pollSock = { connectData->fSocket, POLLOUT, 0 };
		ret = poll( &pollSock, 1, useTimeout ? timeout_ms : -1 );
#endif
		// XXX No further measures for timeouts here ??
		}
	    while ( ret<=0 && (errno==EINTR || errno==EAGAIN) );
	    if ( ret != 1 )
		break;
	    }
	while ( count < l_msg.GetData()->fLength /*-count*/ );
#if 0
	if ( (ret<0 && errno == EPIPE) || ret==0 )
	    {
	    transferAborted = true;
	    fComHelper.ConnectionBroken( connectData );
	    continue;
	    }
	if ( ret < 0 )
	    {
	    ret = errno;
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Send", "Send error" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Error sending message to remote address " << *(BCLTCPAddressStruct*)address
		<< " in TCP Send: " << strerror( ret ) << " (" << ret << ")." << ENDLOG;
	    MsgSendError( ret, this, address, msg, callback );
	    if ( useTimeout )
		setsockopt( connectData->fSocket, SOL_SOCKET, SO_SNDTIMEO, &oldTimeout, sizeof(oldTimeout) );
	    fComHelper.DisconnectFromRemote( (BCLIntTCPComHelper::TTCPConnectionData*)connectData, useTimeout, timeout_ms, true ); // Force socket close
	    return ret;
	    }
	
	// We are done...
	if ( useTimeout )
	    setsockopt( connectData->fSocket, SOL_SOCKET, SO_SNDTIMEO, &oldTimeout, sizeof(oldTimeout) );
	fComHelper.DisconnectFromRemote( (BCLIntTCPComHelper::TTCPConnectionData*)connectData, useTimeout, timeout_ms );
#else
	if ( ret<0 || ret==0 )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Send", "Send error" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Error sending message to remote address " << *(BCLTCPAddressStruct*)address
		<< " in TCP Send: " << strerror( ret ) << " (" << ret << ") (Will attempt retry...)." << ENDLOG;
	    fComHelper.ConnectionBroken( connectData );
	    transferAborted = true;
	    }
	if ( useTimeout )
	    setsockopt( connectData->fSocket, SOL_SOCKET, SO_SNDTIMEO, &oldTimeout, sizeof(oldTimeout) );
	fComHelper.DisconnectFromRemote( (BCLIntTCPComHelper::TTCPConnectionData*)connectData, useTimeout, timeout_ms, transferAborted ); // Force socket close if transfer aborted
#endif
	}
    while ( transferAborted );
    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Send", "Sending" )
	<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": Finished sending to address" 
	<< *((BCLTCPAddressStruct*)address) << "" << ENDLOG;
#ifdef LOG_MESSAGE_CONTENTS
    for ( unsigned b=0; b<msg->fLength; b++ )
	{
	sprintf( tmp2, " 0x%02X", (unsigned)((uint8*)msg)[b] );
	tmp1 += tmp2;
	}
    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Send", "Message Sent" )
	<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << " sent message dump:" 
	<< tmp1.c_str() << "." << ENDLOG;
#endif

    return 0;
    }

int BCLTCPMsgCommunication::Receive( BCLTCPAddressStruct* address, BCLMessageStruct*& msg, bool useTimeout, unsigned long timeout, BCLErrorCallback* callback )
    {
    msg =NULL;
    if ( !address )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Receive", "Address error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr 
	    << ": " << "Wrong address (NULL pointer) in TCP Receive" << ENDLOG;
	MsgReceiveError( EFAULT, this, address, msg, callback );
	return EFAULT;
	}
    uint32 st=0;
    BCLNetworkData::Transform( address->fLength, st, address->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );    
    if ( st != sizeof(BCLTCPAddressStruct) )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Receive", "Address error" )
	    << "Port 0x" << MLUCLog::kHex << fComHelper.fPortNr << " (" << MLUCLog::kDec << fComHelper.fPortNr << "): " 
	    << "Wrong address size " << MLUCLog::kDec << address->fLength 
	    << " (expected " << sizeof(BCLTCPAddressStruct) << ") in TCP Receive." << ENDLOG;
	AddressError( EMSGSIZE, this, address, callback );
	return EMSGSIZE;
	}
    if ( fComHelper.fSocket == -1 )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Receive", "Communication error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr 
	    << ": " << "Helper object not ready." << ENDLOG;
	MsgReceiveError( ENXIO, this, address, msg, callback );
	return ENXIO;
	}
//     uint32 st;
//     BCLNetworkData::Transform( address->fLength, st, address->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );    
//     if ( st != sizeof(BCLTCPAddressStruct) )
// 	{
// 	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Receive", "Address error" )
// 	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address size " << MLUCLog::kDec << address->fLength 
// 	    << " (expected " << sizeof(BCLTCPAddressStruct) << ") in TCP Receive." << ENDLOG;
// 	AddressError( EMSGSIZE, this, address, callback );
// 	return EMSGSIZE;
// 	}

    fDataArrivedSignal.Lock();
    bool success = true;
    do
	{
	if ( fMessages.GetCnt()<=0 )
	    {
	    if ( useTimeout )
		success = fDataArrivedSignal.Wait( timeout );
	    else
		fDataArrivedSignal.Wait();
	    }
	if ( fMessages.GetCnt()>0 )
	    {
	    *address = fMessages.GetFirstPtr()->fAddress;
	    msg = fMessages.GetFirstPtr()->fMsg;
	    fMessages.RemoveFirst();
	    fDataArrivedSignal.Unlock();
	    return 0;
	    }
	}
    while ( success && fComHelper.fSocket != -1 );
    fDataArrivedSignal.Unlock();
    if ( fComHelper.fSocket != -1 )
	return ETIMEDOUT;
    else
	return ENXIO;
    }



bool BCLTCPMsgCommunication::NewConnection( BCLIntTCPComHelper::TAcceptedConnectionData& con )
    {
    LOGC( gBCLLogLevelRef, MLUCLog::kInformational, "TCP Msg Receive", "New Connection" )
	<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << " new connection on socket " << con.fSocket
	<< "." << ENDLOG;
    return true;
    }

int BCLTCPMsgCommunication::NewData( BCLIntTCPComHelper::TAcceptedConnectionData& con )
    {
    LOGC( gBCLLogLevelRef, MLUCLog::kInformational, "TCP Msg Receive", "New Data" )
	<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << " new data on socket " << con.fSocket
	<< "." << ENDLOG;
    // Check reads with select and store state in fMsgProgress
    int totalRead = 0;
    BCLTCPAddress ta;
    BCLNetworkData nd;
    //TTCPMsgProgressDataContType::iterator iter, end, msgProgress;
    TCPMsgProgressData* msgProgress = NULL;
    bool looped = false;
    bool found = false;
    unsigned long mpNdx = ~0UL;
    TCPMsgProgressData mpd;
    if ( MLUCVectorSearcher<TCPMsgProgressData,int>::FindElement( fMsgProgress, &FindMsgProgressBySocket, con.fSocket, mpNdx ) )
	{
	msgProgress = fMsgProgress.GetPtr( mpNdx );
	found = true;
	}
    if ( !found )
	{
	memset( &mpd, 0, sizeof(mpd) );
	mpd.fSocket = con.fSocket;
	mpd.fBytesRead = 0;
	msgProgress = &mpd;
	/*
	fMsgProgress.Add( mpd, mpNdx );
	msgProgress = fMsgProgress.GetPtr( mpNdx );
	*/
	}
    int ret;
#ifdef TCPCOM_NO_POLL
    struct timeval tv;
#endif
    struct timeval oldRcvTimeout;
    bool resetTimeout = false;
    if ( fReceiveTimeout )
	{
	con.fTimeout.tv_sec = fReceiveTimeout/1000;
	con.fTimeout.tv_usec = (fReceiveTimeout-(con.fTimeout.tv_sec*1000))*1000;
	socklen_t sockoptlen = sizeof(oldRcvTimeout);
	ret = getsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, &sockoptlen );
	if ( ret==-1 )
	    {
	    ret = errno;
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob New Data Receive", "Socket Timeout Get Error" )
		<< "Error getting TCP receive timeout from socket in TCP blob receive." << ENDLOG;
	    }
	else
	    {
	    struct timeval newTimeout;
	    newTimeout.tv_sec = 0; //fReceiveTimeout/1000;
	    newTimeout.tv_usec = 0; //(fReceiveTimeout-(newTimeout.tv_sec*1000))*1000;
	    ret = setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &newTimeout, sizeof(newTimeout) );
	    if ( ret==-1 )
		{
		ret = errno;
		LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob New Data Receive", "Socket Timeout Set Error" )
		    << "Error getting TCP receive timeout from socket in TCP blob receive." << ENDLOG;
		}
	    resetTimeout = true;
	    }
	}
#ifdef TCPCOM_NO_POLL
    fd_set sockets;
#endif
    uint8* bp = (uint8*)&msgProgress->fAddress;
    memcpy( &msgProgress->fAddress, &con.fAddress, con.fAddress.fLength );
  
    //BCLMessageStruct msgs;
    BCLMessage msg;
    //BCLMessageStruct* msgsp;
    bp = (uint8*)&msgProgress->fMsgS;
    ret = 0;
    looped = false;
    bool lastWasSelect = true;
    while ( msgProgress->fBytesRead < sizeof(BCLNetworkDataStruct) )
	{
	ret = fComHelper.Read( con, bp+msgProgress->fBytesRead, 
		    sizeof(BCLNetworkDataStruct)-msgProgress->fBytesRead );
	if ( ret<0 && errno!=EAGAIN )
	    break;
	if ( ret==0 && lastWasSelect ) //  && looped )
	    break;
	lastWasSelect = false;
	looped = true;
	if ( ret > 0 )
	    {
	    totalRead += ret;
	    msgProgress->fBytesRead += ret;
	    }
	if ( msgProgress->fBytesRead >= sizeof(BCLNetworkDataStruct) )
	    break;
	do
	    {
#ifdef TCPCOM_NO_POLL
	    FD_ZERO( &sockets );
	    FD_SET( msgProgress->fSocket, &sockets );
	    tv.tv_sec = tv.tv_usec = 0;
	    errno = 0;
	    ret = select( msgProgress->fSocket+1, &sockets, NULL, NULL, &tv );
#else
	    pollfd pollSock = { msgProgress->fSocket, POLLIN, 0 };
	    errno = 0;
	    ret = poll( &pollSock, 1, 0 );
#endif
	    lastWasSelect = true;
	    }
	while ( ret==0 && (errno==EINTR || errno==EAGAIN) );
	if ( ret!=1 )
	    {
	    if ( !found )
		fMsgProgress.Add( mpd );
	    if ( resetTimeout )
		setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	    return totalRead;
	    }
	}
    if ( msgProgress->fBytesRead < sizeof(BCLNetworkDataStruct) && ret<=0 )
	{
	if ( ret < 0 )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP New Data Receive", "Read Message Error" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		<< "Error reading message header from socket " << con.fSocket
		<< ": " << strerror(errno) << " (" << errno << ")." << ENDLOG;
	    }
	else
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP New Data Receive", "Connection closed" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		<< "Incoming connection closed while reading message header from socket " << con.fSocket
		<< "." << ENDLOG;
	    }
	// XXX
	if ( found )
	    fMsgProgress.Remove( mpNdx );
	if ( resetTimeout )
	    setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	return -1;
	}
    if ( looped )
	{
	// Check the correct size (minimum) of the message
	nd.SetTo( (BCLNetworkDataStruct*)&msgProgress->fMsgS );
	if ( nd.GetData()->fLength < sizeof(BCLMessageStruct) )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP New Data Receive", "Message Size Error" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		<< "Message read from socket " << con.fSocket << " has wrong size: " 
		<< nd.GetData()->fLength
		<< " (0x" << MLUCLog::kHex << nd.GetData()->fLength << ") (expected at least "
		<< MLUCLog::kDec << sizeof(BCLMessageStruct) << " (0x" << MLUCLog::kHex
		<< sizeof(BCLMessageStruct) << "))." << ENDLOG;
#ifdef LOG_MESSAGE_CONTENTS
	    MLUCString tmp1;
	    char tmp2[1024];
	    for ( unsigned b=0; b<msgProgress->fBytesRead; b++ )
		{
		sprintf( tmp2, " 0x%02X", (unsigned)((uint8*)&(msgProgress->fMsgS))[b] );
		tmp1 += tmp2;
		}
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP New Data Receive", "New Message" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << " new message dump:" 
		<< tmp1.c_str() << "." << ENDLOG;
#endif
	    // XXX
	    // Read in rest of structure according to passed length??
	  
	    // XXX
	    if ( found )
		fMsgProgress.Remove( mpNdx );
	    if ( resetTimeout )
		setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	    return -1;
	    }
	msgProgress->fMsgLen = nd.GetData()->fLength;
	msgProgress->fMsg = NewMessage( nd.GetData()->fLength );
	if ( !msgProgress->fMsg )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP New Data Receive", "Out Of Memory" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		<< "Out of memory trying to allocate message of size "
		<< nd.GetData()->fLength << " (0x" << MLUCLog::kHex 
		<< nd.GetData()->fLength << ")." << ENDLOG;
	    // XXX
	    // try to read in remaining message to ensure no leftovers?
	  
	    // XXX
	    if ( found )
		fMsgProgress.Remove( mpNdx );
	    if ( resetTimeout )
		setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	    return -1;
	    }
	memcpy( msgProgress->fMsg, &msgProgress->fMsgS, sizeof(BCLNetworkDataStruct) );
	}
    bp = (uint8*)msgProgress->fMsg;
    ret = 0;
    looped = false;
    while ( msgProgress->fBytesRead < msgProgress->fMsgLen )
	{
	ret = fComHelper.Read( con, bp+msgProgress->fBytesRead, 
		    msgProgress->fMsgLen-msgProgress->fBytesRead,
		    true );
	if ( ret <=0 && errno!=EAGAIN )
	    break;
	if ( ret==0 && lastWasSelect )
	    break;
	lastWasSelect = false;
	looped = true;
	if ( ret > 0 )
	    {
	    totalRead += ret;
	    msgProgress->fBytesRead += ret;
	    }
	if ( msgProgress->fBytesRead >= msgProgress->fMsgLen )
	    break;
	do
	    {
#ifdef TCPCOM_NO_POLL
	    FD_ZERO( &sockets );
	    FD_SET( msgProgress->fSocket, &sockets );
	    tv.tv_sec = tv.tv_usec = 0;
	    errno = 0;
	    ret = select( msgProgress->fSocket+1, &sockets, NULL, NULL, &tv );
#else
	    pollfd pollSock = { msgProgress->fSocket, POLLIN, 0 };
	    ret = poll( &pollSock, 1, 0 );
#endif
	    lastWasSelect = true;
	    }
	while ( ret==0 && (errno==EINTR || errno==EAGAIN) );
	if ( ret!=1 )
	    {
	    if ( !found )
		fMsgProgress.Add( mpd );
	    if ( resetTimeout )
		setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	    return totalRead;
	    }
	}
    if ( msgProgress->fBytesRead < msgProgress->fMsgLen && ret<=0 )
	{
	if ( ret < 0 )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP New Data Receive", "Read Message Error" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		<< "Error reading message header from socket " << con.fSocket
		<< ": " << strerror(errno) << " (" << errno << ")." << ENDLOG;
	    }
	else
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP New Data Receive", "Connection closed" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		<< "Incoming connection closed while reading message header from socket " << con.fSocket
		<< "." << ENDLOG;
	    }
	// XXX
	if ( found )
	    fMsgProgress.Remove( mpNdx );
	if ( resetTimeout )
	    setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	return -1;
	}
    if ( msgProgress->fBytesRead >= msgProgress->fMsgLen )
	{
	TCPMsgData msgData;
	unsigned long dumpLen = msgProgress->fMsgLen;
	if ( msgProgress->fBytesRead<dumpLen )
	    dumpLen = msgProgress->fBytesRead;
	msgData.fAddress = msgProgress->fAddress;
	msgData.fMsg = msgProgress->fMsg;
#ifdef LOG_MESSAGE_CONTENTS
	MLUCString tmp1;
	char tmp2[1024];
	for ( unsigned b=0; b<dumpLen; b++ )
	    {
	    sprintf( tmp2, " 0x%02X", (unsigned)((uint8*)msgProgress->fMsg)[b] );
	    tmp1 += tmp2;
	    }
#endif
	if ( found )
	    fMsgProgress.Remove( mpNdx );
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP New Data Receive", "New Message Signal Sent" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << " new message " << *(msgData.fMsg) << " received signal sent ("
	    << (found ? "continued" : "in-one") << ")." << ENDLOG;
#ifdef LOG_MESSAGE_CONTENTS
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP New Data Receive", "New Message" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << " new message dump:" 
	    << tmp1.c_str() << "." << ENDLOG;
#endif
	fDataArrivedSignal.Lock();
	fMessages.Add( msgData );
	fDataArrivedSignal.Unlock();
	fDataArrivedSignal.Signal();
	}
    else
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kFatal, "TCP New Data Receive", "Internal receive problem" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << " new message receive internal inconsistency:" 
	    << ENDLOG;
	}
    if ( resetTimeout )
	setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
    con.fTimeout.tv_sec = con.fTimeout.tv_usec = 0;
    return totalRead;
    }

bool BCLTCPMsgCommunication::ConnectionTimeout( BCLIntTCPComHelper::TAcceptedConnectionData& con )
    {
    LOGC( gBCLLogLevelRef, MLUCLog::kInformational, "TCP Msg Receive", "Connection Timed Out" )
	<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << " connection on socket " << con.fSocket
	<< " timed out." << ENDLOG;
    return false;
    }

void BCLTCPMsgCommunication::ConnectionClosed( BCLIntTCPComHelper::TAcceptedConnectionData& con )
    {
    // Check reads with select and store state in fMsgProgress
    unsigned long mpNdx;
    if ( MLUCVectorSearcher<TCPMsgProgressData,int>::FindElement( fMsgProgress, &FindMsgProgressBySocket, con.fSocket, mpNdx ) )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kInformational, "TCP Msg Receive", "Connection Closed" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << " connection on socket " << con.fSocket
	    << " closed." << ENDLOG;
	fMsgProgress.Remove( mpNdx );
	}
    }

bool BCLTCPMsgCommunication::FindMsgProgressBySocket( const TCPMsgProgressData& msgProgress, const int& refSocket )
    {
    return msgProgress.fSocket == refSocket;
    }


#endif // USE_TCP





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
