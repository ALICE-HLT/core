/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLIntSCIComHelper.hpp"
#include "BCLCommunication.hpp"
#include "BCLSCIAddressOps.hpp"
#include "MLUCLog.hpp"
#include <errno.h>
#include <asm/page.h>


#ifndef SISCI_NO_DMA
static const unsigned int kBCLIntSCILocalSegmentIDStart = 0x00010000; // 16 bit usable for user level segment IDs. Anything above available for internal use.
static const unsigned int kBCLIntSCILocalSegmentIDMax =   0xFFFFFFFF; // Assume 32 bit ID size.
#endif


#define CONNECTIONTRY 4
#define INTERRUPT_WAIT busy_wait(2);

#ifdef SCI_SIMPLE_TIMEOUT
#define SIMPLE_TIMEOUT_MAX 1000000
#endif

//#define SEQUENCE_BENCH


void busy_wait( unsigned long time_musec )
    {
    struct timeval t1, t2;
    unsigned long diff;
    gettimeofday( &t1, NULL );
    do
	{
	gettimeofday( &t2, NULL );
	diff = (t2.tv_usec-t1.tv_usec)+(t2.tv_sec-t1.tv_sec)*1000000;
	}
    while ( diff < time_musec );
    }



BCLIntSCIComHelper::BCLIntSCIComHelper( BCLCommunication* com )
    {
    fCom = com;
    
    SCIInitialize( 0, &fSCIError );
    if ( fSCIError != SCI_ERR_OK )
	{
	LOG( MLUCLog::kError, "BCLSCIMsgCommunication::BCLSCIMsgCommunication", "SCI initialize" )
	    << "Error initializing SCI device. SCI Error code: " << (uint32)fSCIError << ENDLOG;
	fSCIDesc = NULL;
	}

    SCIOpen( &fSCIDesc, 0, &fSCIError );
    fIsSCIOpen = true;
    if ( fSCIError != SCI_ERR_OK )
	{
	LOG( MLUCLog::kError, "BCLSCIMsgCommunication::BCLSCIMsgCommunication", "SCI device open" )
	    << "Error opening SCI device. SCI Error code: " << (uint32)fSCIError << ENDLOG;
	fIsSCIOpen = false;
	fSCIDesc = NULL;
	}
    fSegmentID = 0;
    fSegmentAddress = NULL;
    fBufferAddress = NULL;
    fControlData = NULL;
    fBufferSize = 0;
    fRequestInterrupt = NULL;
    fSegment = NULL;
    fSegmentMap = NULL;
    fConnectionTimeout = 1000; // in millisec.
#ifdef DEBUG
    fConnectionTimeout = SCI_INFINITE_TIMEOUT;
#endif
#ifdef USE_INTERRUPT_CALLBACKS
    fRequestInterruptCallback = NULL;
    fRequestInterruptData = NULL;
#endif
    }

BCLIntSCIComHelper::~BCLIntSCIComHelper()
    {
    vector<TSCIConnectionData>::iterator iter, end;
    iter = fConnections.begin();
    end = fConnections.end();
    while ( iter != end )
	{
	Disconnect( &(iter->fAddress), NULL, true, 10000 );
	iter++;
	}
    Unbind();
    if ( fIsSCIOpen )
	{
	SCIClose( fSCIDesc, 0, &fSCIError );
	if ( fSCIError != SCI_ERR_OK )
	    {
	    LOG( MLUCLog::kError, "BCLSCIMsgCommunication::~BCLSCIMsgCommunication", "SCI device close" )
		<< "Error closing SCI device. SCI Error code: " << (uint32)fSCIError << ENDLOG;
	    }
	}
    }

int BCLIntSCIComHelper::Bind( uint32 size, BCLSCIAddressStruct* address )
    {
    unsigned int inr;
    sci_error_t mySCIError;
    fBufferSize = size;
    fAddress = address;
    BCLIntSCIControlData scd;
    if ( !fIsSCIOpen )
	{
	LOG( MLUCLog::kError, "SCI Bind", "SCI not opened" )
	    << "SCI device not open" << ENDLOG;
	BindError( ENODEV, fCom, address, NULL );
	return ENODEV;
	}
    if ( !address )
	{
	LOG( MLUCLog::kError, "SCI Bind", "No address" )
	    << "No address (Null pointer) given" << ENDLOG;
	AddressError( EFAULT, fCom, address );
	return EFAULT;
	}
    fAddress->fNodeID = GetLocalNodeID( fAddress->fAdapterNr );
    if ( fSCIError != SCI_ERR_OK ) 
	{
	LOG( MLUCLog::kError, "SCI Bind", "Error getting local node ID" )
	    << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " while trying to get id of local node." << ENDLOG;
	BindError( ENODEV, fCom, address, NULL );
	return ENODEV;
	}
    fSegmentID = (fAddress->fNodeID << 16) | fAddress->fSegmentID;
    if ( fSegmentID == ~(uint32)0 )
	{
	LOG( MLUCLog::kError, "SCI Bind", "Wrong segment id" )
	    << "Segment ID created from address node and segment id results in invalid value (0x" 
	    << MLUCLog::kHex << fSegmentID << ")." << ENDLOG;
	AddressError( EINVAL, fCom, address );
	return EINVAL;
	}
#ifndef _SCALI_INTERRUPT_BUGFIX_
#ifdef USE_INTERRUPT_CALLBACKS
    SCICreateInterrupt( fSCIDesc, &fRequestInterrupt, fAddress->fAdapterNr, &inr,
			&(BCLIntSCIComHelper::IntRequestInterruptCallback), this, 0, &mySCIError );

#else
    SCICreateInterrupt( fSCIDesc, &fRequestInterrupt, fAddress->fAdapterNr, &inr,
			NULL, NULL, 0, &mySCIError );
#endif
    fSCIError = mySCIError;
    if ( fSCIError != SCI_ERR_OK || !fRequestInterrupt)
	{
	LOG( MLUCLog::kError, "SCI Bind", "Error creating request interrupt" )
	    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " creating request interrupt" << ENDLOG;
	Unbind();
	BindError( EBUSY, fCom, address,NULL );
	return EBUSY;
	}
#endif
    SCICreateSegment( fSCIDesc, &fSegment, fSegmentID, fBufferSize+PAGE_SIZE,
		      NULL, NULL, 0, &mySCIError );
    fSCIError = mySCIError;
    if ( fSCIError != SCI_ERR_OK || !fSegment )
      {
	LOG( MLUCLog::kError, "SCI Bind", "Error creating segment" )
	  << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " creating SCI memory segment, id 0x" << MLUCLog::kHex << fSegmentID 
	  << MLUCLog::kDec << " of size " << fBufferSize+PAGE_SIZE
	  << " (0x" << MLUCLog::kHex << fBufferSize+PAGE_SIZE << ")." << ENDLOG;
	int error;
	switch ( fSCIError )
	  {
	  case SCI_ERR_SEGMENTID_USED:
	    error = EBUSY; break;
	  case SCI_ERR_SIZE_ALIGNMENT:
	    error = ENOTSUP; break;
	  default:
	    error = ENXIO; break;
	  }
	BindError( error, fCom, address, NULL );
	return error;
      }
    SCIPrepareSegment(fSegment, fAddress->fAdapterNr, 0, &mySCIError);
    fSCIError = mySCIError;
    if ( fSCIError != SCI_ERR_OK )
	{
	LOG( MLUCLog::kError, "SCI Bind", "Error preparing segment" )
	    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " preparing SCI memory segment, id 0x" << MLUCLog::kHex << fSegmentID 
	    << MLUCLog::kDec << " of size " << fBufferSize+PAGE_SIZE
	    << " (0x" << MLUCLog::kHex << fBufferSize+PAGE_SIZE << ")." << ENDLOG;
	BindError( EIO, fCom, address, NULL );
	return EIO;
	}
    fSegmentAddress = (uint8*)SCIMapLocalSegment(fSegment, &fSegmentMap, 0, fBufferSize+PAGE_SIZE, 
						 NULL, 0, &mySCIError);
    fSCIError = mySCIError;
    if ( fSCIError != SCI_ERR_OK )
	{
	LOG( MLUCLog::kError, "SCI Bind", "Error mapping segment" )
	    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " mapping SCI memory segment, id 0x" << MLUCLog::kHex << fSegmentID 
	    << MLUCLog::kDec << " of size " << fBufferSize+PAGE_SIZE
	    << " (0x" << MLUCLog::kHex << fBufferSize+PAGE_SIZE << ")." << ENDLOG;
	Unbind();
	int error;
	switch ( fSCIError )
	    {
	    case SCI_ERR_OUT_OF_RANGE:
		error = ERANGE; break;
	    case SCI_ERR_SIZE_ALIGNMENT:
		error = ENOTSUP; break;
	    case SCI_ERR_OFFSET_ALIGNMENT:
		error = ENOTSUP; break;
	    default:
		error = ENXIO; break;
	    }
	BindError( error, fCom, address, NULL );
	return error;
	}
    fControlData = (BCLIntSCIControlDataStruct*)fSegmentAddress;
    memcpy( fControlData, scd.GetData(), scd.GetData()->fLength );
    fBufferAddress = fSegmentAddress+fControlData->fLength;
    fControlData->fRequestInterruptNr = ~(uint32)0;
#ifndef _SCALI_INTERRUPT_BUGFIX_
    fControlData->fRequestInterruptNr = inr;
#endif
    fControlData->fSenderRequest = ~(uint32)0;
    fControlData->fCurrentSender = ~(uint32)0;
    fControlData->fCurrentSenderLocked = 0;
    fControlData->fReadIndex = 0;
    fControlData->fWriteIndex = 0;
    fControlData->fSize = fBufferSize;
    SCISetSegmentAvailable( fSegment, fAddress->fAdapterNr, 0, &mySCIError );
    fSCIError = mySCIError;
    if ( fSCIError != SCI_ERR_OK )
	{
	LOG( MLUCLog::kError, "SCI Bind", "Error making segment available" )
	    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " making SCI memory segment, id 0x" << MLUCLog::kHex << fSegmentID 
	    << MLUCLog::kDec << " of size " << fBufferSize+PAGE_SIZE
	    << " (0x" << MLUCLog::kHex << fBufferSize+PAGE_SIZE 
	    << ") available." << ENDLOG;
	Unbind();
	BindError( EIO, fCom, address, NULL );
	return EIO;
	}
    return 0;
    }

int BCLIntSCIComHelper::Unbind()
    {
    sci_error_t mySCIError;
    if ( fSegment != NULL )
	SCISetSegmentUnavailable( fSegment, fAddress->fAdapterNr, 0, &mySCIError );
    fSCIError = mySCIError;
    if ( fSegmentAddress )
	{
	if ( fControlData )
	    {
#ifndef _SCALI_INTERRUPT_BUGFIX_
	    if ( fControlData->fRequestInterruptNr != ~(uint32)0 )
		SCIRemoveInterrupt( fRequestInterrupt, 0, &mySCIError );
#else
	    mySCIError = SCI_ERR_OK;
#endif
	    fSCIError = mySCIError;
	    fControlData = NULL;
	    }
	if ( fSegmentMap )
	    SCIUnmapSegment( fSegmentMap, 0, &mySCIError );
	fSegmentMap = NULL;
	fSCIError = mySCIError;
	fSegmentAddress = NULL;
	fBufferAddress = NULL;
	}
    if ( fSegment )
	SCIRemoveSegment( fSegment, 0, &mySCIError );
    fSegment = NULL;
    fSCIError = mySCIError;
    return 0;
    }

int BCLIntSCIComHelper::Connect( BCLSCIAddressStruct* address, BCLErrorCallback* callback, bool useTimeout, uint32 timeout_ms, bool openOnDemand )
    {
    if ( !fIsSCIOpen )
	{
	LOG( MLUCLog::kError, "SCI Connect", "SCI not opened" )
	    << "SCI device not opened" << ENDLOG;
	ConnectionError( ENODEV, fCom, address );
	return ENODEV;
	}
    TSCIConnectionData connectData;
    connectData.fAddress = *address;
    connectData.fUsageCount = 0;
    int ret;
    bool found;
    ret = ConnectToRemote( address, &connectData, !openOnDemand, true, found, callback, useTimeout, timeout_ms );
    if ( ret )
	{
	LOG( MLUCLog::kError, "SCI Connect", "Connection error" )
	    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI connection error" << ENDLOG;
	ConnectionError( ret, fCom, address );
	return ret;
	}
    return 0;
    }

int BCLIntSCIComHelper::Disconnect( BCLSCIAddressStruct* address, BCLErrorCallback* callback, bool useTimeout, uint32 timeout_ms )
    {
    int ret;
    TSCIConnectionData connectData;
    connectData.fAddress = *address;
    connectData.fUsageCount = 0;
    bool found;
    ret = ConnectToRemote( address, &connectData, false, false, found, callback, useTimeout, timeout_ms );
    if ( found )
	{
	DisconnectFromRemote( &connectData, callback, useTimeout, timeout_ms );
	return 0;
	}
    return EINVAL;
    }

int BCLIntSCIComHelper::LockConnection( BCLSCIAddressStruct* address, BCLErrorCallback* callback, bool useTimeout, uint32 timeout_ms )
    {
    TSCIConnectionData* connectData = NULL;
    vector<TSCIConnectionData>::iterator iter, end;
    iter = fConnections.begin();
    end = fConnections.end();
    while ( iter != end )
	{
	if ( iter->fAddress == *address )
	    {
#if __GNUC__>=3
	    connectData = iter.base();
#else
	    connectData = iter;
#endif
	    break;
	    }
	iter++;
	}
    if ( !connectData )
	return EINVAL;

    if ( !connectData->fRemoteAddr )
	{
	connectData->fLocked = true;
	return 0;
	}
    if ( connectData->fLocked )
	return 0;


    int ret;
    ret = LockConnection( address, connectData, callback, useTimeout, timeout_ms );
    if ( ret )
	return ret;

    connectData->fLocked = true;

    if ( connectData->fRemoteCD )
	{
	do
	    {
	    ret = StartSequence( connectData, useTimeout, timeout_ms );
	    if ( ret )
		{
		LOG( MLUCLog::kError, "SCI LockConnection", "Start sequence error" )
		    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "Error starting map sequence for connection lock to remote address " << *(BCLSCIAddressStruct*)address
		    << " in SCI LockConnection: " << strerror( ret ) << " (" << ret << ")." << ENDLOG;
		return ret;
		}
	    connectData->fRemoteCD->fCurrentSenderLocked = ~(uint32)0;
	    ret = CheckSequence( connectData, useTimeout, timeout_ms );
	    }
	while ( ret == EAGAIN );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "SCI LockConnection", "Send Error" )
		<< "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "Error writing ID to remote address " << *(BCLSCIAddressStruct*)address
		<< " for connection lock in SCI LockConnection: " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	    return ret;
	    }
	}

    ret = UpdateCachedCD( address, NULL, useTimeout, timeout_ms );
    if ( ret )
	{
	LOG( MLUCLog::kError, "SCI LockConnection", "Internal Error" )
	    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "Internal error updating control data from remote address " << *(BCLSCIAddressStruct*)address
	    << ": " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	}

    return 0;
    }

int BCLIntSCIComHelper::UnlockConnection( BCLSCIAddressStruct* address, BCLErrorCallback* callback, bool useTimeout, uint32 timeout_ms )
    {
    int ret;
    TSCIConnectionData* connectData = NULL;
    vector<TSCIConnectionData>::iterator iter, end;
    iter = fConnections.begin();
    end = fConnections.end();
    while ( iter != end )
	{
	if ( iter->fAddress == *address )
	    {
#if __GNUC__>=3
	    connectData = iter.base();
#else
	    connectData = iter;
#endif
	    break;
	    }
	iter++;
	}
    if ( !connectData )
	return EINVAL;

    if ( !connectData->fRemoteAddr )
	{
	connectData->fLocked = false;
	return 0;
	}
    if ( !connectData->fLocked )
	return 0;

    if ( connectData->fRemoteCD )
	{
	do
	    {
	    ret = StartSequence( connectData, useTimeout, timeout_ms );
	    if ( ret )
		{
		LOG( MLUCLog::kError, "SCI LockConnection", "Start sequence error" )
		    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "Error starting map sequence for connection lock to remote address " << *(BCLSCIAddressStruct*)address
		    << " in SCI LockConnection: " << strerror( ret ) << " (" << ret << ")." << ENDLOG;
		return ret;
		}
	    connectData->fRemoteCD->fCurrentSenderLocked = 0;
	    ret = CheckSequence( connectData, useTimeout, timeout_ms );
	    }
	while ( ret == EAGAIN );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "SCI LockConnection", "Send Error" )
		<< "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "Error writing ID to remote address " << *(BCLSCIAddressStruct*)address
		<< " for connection lock in SCI LockConnection: " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	    return ret;
	    }
	}

    ret = UnlockConnection( address, connectData, callback, useTimeout, timeout_ms );
    if ( ret )
	return ret;

    connectData->fLocked = false;
    return 0;
    }

BCLIntSCIComHelper::operator bool()
    {
    return fIsSCIOpen && fSCIError==SCI_ERR_OK;
    }

uint16 BCLIntSCIComHelper::GetLocalNodeID( uint16 adapterNr )
    {
    sci_error_t mySCIError;
    sci_query_adapter_t queryAdapter;
    unsigned int myID;
    
    queryAdapter.subcommand = SCI_Q_ADAPTER_NODEID;
    queryAdapter.localAdapterNo = adapterNr;
    queryAdapter.data = &myID; 
    
    SCIQuery(SCI_Q_ADAPTER,&queryAdapter,0,&mySCIError);
    fSCIError = mySCIError;
    return (uint16)myID;
    }

int BCLIntSCIComHelper::ConnectToRemote( BCLSCIAddressStruct* address, TSCIConnectionData* parConnectData, bool now, bool store, bool& wasStored, BCLErrorCallback* callback, bool useTimeout, uint32 timeout_ms )
    {
    struct timeval tStart, tNow;
    sci_error_t mySCIError;
    TSCIConnectionData* connectData = parConnectData;
    vector<TSCIConnectionData>::iterator iter, end;
    iter = fConnections.begin();
    end = fConnections.end();
    wasStored = false;
    while ( iter != end )
	{
	if ( iter->fAddress == *address )
	    {
	    wasStored = true;
	    if ( store )
		iter->fUsageCount++;
	    if ( !now || (now && iter->fRemoteAddr) )
		{
		*parConnectData = *iter;
		return 0;
		}
#if __GNUC__>=3
	    connectData = iter.base();
#else
	    connectData = iter;
#endif
	    break;
	    }
	iter++;
	}
    
    connectData->fRemoteAddr = NULL;
    // XXX ???
#ifndef SISCI_NO_DMA
    connectData->fLocalSegment = NULL;
    connectData->fLocalMap = NULL;
    connectData->fLocalAddr = NULL;
    connectData->fLocalSize = 0;
    connectData->fLocalUsage = 0;
#endif    
    if ( !wasStored )
	connectData->fLocked = false;
    if ( now )
	{
	unsigned int segID;
	segID = (address->fNodeID << 16) | address->fSegmentID;
	if ( segID == ~(uint32)0 )
	    {
	    return EINVAL;
	    }
	SCIOpen( &(connectData->fSCIDesc), 0, &mySCIError );
	if ( mySCIError != SCI_ERR_OK )
	    {
	    LOG( MLUCLog::kError, "BCLSCIMsgCommunication::Connect", "SCI device open" )
		<< "Error opening SCI device. SCI Error code: " << (uint32)mySCIError << ENDLOG;
	    connectData->fSCIDesc = NULL;
	    //ConnectionError( ENODEV, this, address );
	    return ENODEV;
	    }
	//connectData->fSCIDesc = fSCIDesc;
	
	uint32 firstTry = 0;
	fSCIError = SCI_ERR_OK;
	while ( firstTry<CONNECTIONTRY ) // was 2 (try 1)
	    {
	    SCIConnectSegment( connectData->fSCIDesc, &(connectData->fRemoteSegment), address->fNodeID, 
			       segID, (unsigned int)address->fAdapterNr, 
			       NULL, NULL, fConnectionTimeout, 0, &mySCIError );
	    fSCIError = mySCIError;
	    if ( fSCIError == SCI_ERR_CONNECTION_REFUSED || mySCIError == SCI_ERR_CANCELLED )
		{
		firstTry++;
		INTERRUPT_WAIT;
		}
	    else
		firstTry = CONNECTIONTRY;
	    }
	if ( fSCIError != SCI_ERR_OK )
	    {
	    LOG( MLUCLog::kError, "SCI Connect", "Error connecting segment" )
		<< "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " connecting to segment 0x"
		<< MLUCLog::kHex << segID << "." << ENDLOG;
	    int error;
	    switch ( fSCIError )
		{
		case SCI_ERR_NO_SUCH_SEGMENT:
		    error = EINVAL; break;
		case SCI_ERR_CONNECTION_REFUSED:
		    error = EACCES; break;
		case SCI_ERR_TIMEOUT:
		    error = ETIMEDOUT; break;
		case SCI_ERR_NO_LINK_ACCESS:
		    error = EIO; break;
		case SCI_ERR_NO_REMOTE_LINK_ACCESS:
		    error = ENXIO; break;
		default:
		    error = ENXIO; break;
		}
	    //ConnectionError( error, fCom, address );
	    SCIClose( connectData->fSCIDesc, 0, &fSCIError );
	    return error;
	    }
	connectData->fRemoteAddr = (uint8*)SCIMapRemoteSegment( connectData->fRemoteSegment, &(connectData->fRemoteMap),
								0, ((sizeof( BCLIntSCIControlDataStruct )/8)+1)*8, NULL, 0, &mySCIError );
	fSCIError = mySCIError;
	if ( fSCIError != SCI_ERR_OK )
	    {
	    LOG( MLUCLog::kError, "SCI Connect", "Error mapping segment" )
		<< "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " mapping segment "
		<< MLUCLog::kHex << segID << " control data part of size " 
		<< ((sizeof( BCLIntSCIControlDataStruct )/8)+1)*8 << "." << ENDLOG;
	    int error;
	    switch ( fSCIError )
		{
		case SCI_ERR_OUT_OF_RANGE:
		    error = ERANGE; break;
		case SCI_ERR_SIZE_ALIGNMENT:
		    error = ENOTSUP; break;
		case SCI_ERR_OFFSET_ALIGNMENT:
		    error = ENOTSUP; break;
		default:
		    error = ENXIO; break;
		}
	    SCIDisconnectSegment( connectData->fRemoteSegment, 0, &mySCIError );
	    fSCIError = mySCIError;
	    //ConnectionError( error, fCom, address );
	    SCIClose( connectData->fSCIDesc, 0, &fSCIError );
	    return error;
	    }
	connectData->fRemoteCD = (BCLIntSCIControlDataStruct*)(connectData->fRemoteAddr);
	uint32 size;
	BCLIntSCIControlData scd( connectData->fRemoteCD );
	BCLIntSCIControlDataStruct *controlData = scd.GetData();
	size = controlData->fSize;
	size += sizeof(BCLIntSCIControlDataStruct);
	size = (size/8 + 1)*8;
	SCIUnmapSegment( connectData->fRemoteMap, 0, &mySCIError );
	fSCIError = mySCIError;
	
	connectData->fRemoteAddr = (uint8*)SCIMapRemoteSegment( connectData->fRemoteSegment, &(connectData->fRemoteMap),
								0, size, NULL, 0, &mySCIError );
	fSCIError = mySCIError;
	if ( fSCIError != SCI_ERR_OK )
	    {
	    LOG( MLUCLog::kError, "SCI Connect", "Error mapping segment" )
		<< "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " mapping segment "
		<< MLUCLog::kHex << segID << " of size " << MLUCLog::kDec << size << " (0x" << MLUCLog::kHex
		<< size << ") completely." << ENDLOG;
	    int error;
	    switch ( fSCIError )
		{
		case SCI_ERR_OUT_OF_RANGE:
		    error = ERANGE; break;
		case SCI_ERR_SIZE_ALIGNMENT:
		    error = ENOTSUP; break;
		case SCI_ERR_OFFSET_ALIGNMENT:
		    error = ENOTSUP; break;
		default:
		    error = ENXIO; break;
		}
	    SCIDisconnectSegment( connectData->fRemoteSegment, 0, &mySCIError );
	    //ConnectionError( error, fCom, address );
	    SCIClose( connectData->fSCIDesc, 0, &fSCIError );
	    return error;
	    }

	sci_sequence_status_t sequenceStatus;
	SCICreateMapSequence( connectData->fRemoteMap, &(connectData->fMapSequence), SCI_FLAG_FAST_BARRIER, &mySCIError );
	if ( mySCIError!=SCI_ERR_OK )
	    {
	    SCICreateMapSequence( connectData->fRemoteMap, &(connectData->fMapSequence), 0, &mySCIError );
	    if ( mySCIError!=SCI_ERR_OK )
		{
		LOG( MLUCLog::kError, "SCI Send", "Sequence error" )
		    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "Error creating map sequence for connection to remote address " << *(BCLSCIAddressStruct*)address
		    << " in SCI Connect: 0x" << mySCIError << "." << ENDLOG;
		SCIUnmapSegment( connectData->fRemoteMap, 0, &mySCIError );
		SCIDisconnectSegment( connectData->fRemoteSegment, 0, &mySCIError );
		return ENXIO;
		}
	    }
	if ( useTimeout )
	    gettimeofday( &tStart, NULL );
	do
	    {
	    if ( useTimeout )
		{
		gettimeofday( &tNow, NULL );
		unsigned long td;
		td = (tNow.tv_sec-tStart.tv_sec)*1000 + (tNow.tv_usec-tStart.tv_usec)/1000;
		if ( td > timeout_ms )
		    {
		    LOG( MLUCLog::kError, "SCI Send", "Start Sequence error" )
			<< "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "Timeout trying to start sequence for connection to remote address " << *(BCLSCIAddressStruct*)address
			<< " in SCI Connect: 0x" << mySCIError << "." << ENDLOG;
		    SCIRemoveSequence( connectData->fMapSequence, 0, &mySCIError );
		    SCIUnmapSegment( connectData->fRemoteMap, 0, &mySCIError );
		    SCIDisconnectSegment( connectData->fRemoteSegment, 0, &mySCIError );
		    return ETIMEDOUT;
		    }
		}
	    sequenceStatus = SCIStartSequence( connectData->fMapSequence, 0, &mySCIError );
	    if ( mySCIError != SCI_ERR_OK )
		{
		LOG( MLUCLog::kError, "SCI Send", "Start Sequence error" )
		    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "Error starting sequence for connection to remote address " << *(BCLSCIAddressStruct*)address
		    << " in SCI Connect: 0x" << mySCIError << "." << ENDLOG;
		SCIRemoveSequence( connectData->fMapSequence, 0, &mySCIError );
		SCIUnmapSegment( connectData->fRemoteMap, 0, &mySCIError );
		SCIDisconnectSegment( connectData->fRemoteSegment, 0, &mySCIError );
		return ENXIO;
		}
	    }
	while ( sequenceStatus != SCI_SEQ_OK );

	connectData->fRemoteCD = (BCLIntSCIControlDataStruct*)(connectData->fRemoteAddr);
	connectData->fRemoteMsgs = connectData->fRemoteAddr + sizeof( BCLIntSCIControlDataStruct);
	connectData->fRemoteSize = size;
	unsigned int inr;
	inr = controlData->fRequestInterruptNr;
	firstTry = 0;
#ifndef _SCALI_INTERRUPT_BUGFIX_
	while ( firstTry<CONNECTIONTRY ) // was 2
	    {
	    SCIConnectInterrupt( connectData->fSCIDesc, &(connectData->fRemoteRequestInt), address->fNodeID,
				 address->fAdapterNr, inr, fConnectionTimeout, 0, &mySCIError );
	    fSCIError = mySCIError;
	    if ( fSCIError == SCI_ERR_CONNECTION_REFUSED || mySCIError == SCI_ERR_CANCELLED )
		{
		firstTry++;
		INTERRUPT_WAIT;
		}
	    else
		firstTry = CONNECTIONTRY;
	    }
	if ( fSCIError != SCI_ERR_OK )
	    {
	    LOG( MLUCLog::kError, "SCI Connect", "Error connecting request interrupt" )
		<< "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " connecting remote request interrupt "
		<< MLUCLog::kHex << inr << "." << ENDLOG;
	    int error;
	    switch ( fSCIError )
		{
		case SCI_ERR_CONNECTION_REFUSED:
		    error = EACCES; break;
		    // 	    case SCI_ERR_NO_SUCH_INTNO:
		    // 		error = ENOTSUP; break;
		case SCI_ERR_TIMEOUT:
		    error = ETIMEDOUT; break;
		default:
		    error = ENXIO; break;
		}
	    SCIRemoveSequence( connectData->fMapSequence, 0, &mySCIError );
	    SCIUnmapSegment( connectData->fRemoteMap, 0, &mySCIError );
	    SCIDisconnectSegment( connectData->fRemoteSegment, 0, &mySCIError );
	    //ConnectionError( error, fCom, address );
	    SCIClose( connectData->fSCIDesc, 0, &fSCIError );
	    return error;
	    }
#endif
	//connectData->fAddress = *address;
	}
    if ( connectData->fLocked )
	LockConnection( &(connectData->fAddress), parConnectData, callback, useTimeout, timeout_ms );
    if ( store && connectData==parConnectData )
	{
	parConnectData->fUsageCount = 1;
	fConnections.insert( fConnections.end(), *parConnectData );
	}
    if ( connectData != parConnectData )
	*parConnectData = *connectData;
    return 0;
    }

void BCLIntSCIComHelper::DisconnectFromRemote( TSCIConnectionData* connectData, BCLErrorCallback* callback, bool useTimeout, uint32 timeout_ms )
    {
    sci_error_t mySCIError;
    vector<TSCIConnectionData>::iterator iter, end;
    iter = fConnections.begin();
    end = fConnections.end();
    while ( iter != end )
	{
	if ( iter->fAddress == connectData->fAddress )
	    {
	    iter->fUsageCount--;
	    if ( iter->fUsageCount <= 0 )
		{
#ifndef SISCI_NO_DMA
		// XXX ???
#if __GNUC__>=3
		DestroyLocalSegment( iter.base() );
#else
		DestroyLocalSegment( iter );
#endif
#endif
#if __GNUC__>=3
		if ( iter->fLocked && iter->fRemoteAddr )
		    UnlockConnection( &(iter->fAddress), iter.base(), callback, useTimeout, timeout_ms );
#else
		if ( iter->fLocked && iter->fRemoteAddr )
		    UnlockConnection( &(iter->fAddress), iter, callback, useTimeout, timeout_ms );
#endif
#ifndef _SCALI_INTERRUPT_BUGFIX_
		SCIDisconnectInterrupt( iter->fRemoteRequestInt, 0, &mySCIError );
#endif
		SCIRemoveSequence( iter->fMapSequence, 0, &mySCIError );
		SCIUnmapSegment( iter->fRemoteMap, 0, &mySCIError );
		SCIDisconnectSegment( iter->fRemoteSegment, 0, &mySCIError );
		SCIClose( iter->fSCIDesc, 0, &fSCIError );
		fConnections.erase( iter );
		}
	    return;
	    }
	iter++;
	}
    if ( connectData->fLocked && connectData->fRemoteAddr )
	UnlockConnection( &(connectData->fAddress), connectData, callback, useTimeout, timeout_ms );
#ifndef _SCALI_INTERRUPT_BUGFIX_
    SCIDisconnectInterrupt( connectData->fRemoteRequestInt, 0, &mySCIError );
#endif
    SCIRemoveSequence( connectData->fMapSequence, 0, &mySCIError );
    SCIUnmapSegment( connectData->fRemoteMap, 0, &mySCIError );
    SCIDisconnectSegment( connectData->fRemoteSegment, 0, &mySCIError );
    SCIClose( connectData->fSCIDesc, 0, &fSCIError );
    return;
    }

int BCLIntSCIComHelper::LockConnection( BCLSCIAddressStruct* address, TSCIConnectionData* connectData, BCLErrorCallback* callback, bool useTimeout, uint32 timeout_ms )
    {
    sci_error_t mySCIError;
    int ret;


    BCLIntSCIControlData scd( connectData->fRemoteCD );
    uint8 remoteByteOrder = scd.GetOriginalByteOrder();
    //BCLIntSCIControlDataStruct* controlData = scd.GetData();
    uint32 myTransID;
    BCLNetworkData::Transform( GetSegmentID(), myTransID, kNativeByteOrder, remoteByteOrder );

    struct timeval start, now;
    unsigned long tdiff;
    gettimeofday( &start, NULL );

    do
	{
	while ( connectData->fRemoteCD->fCurrentSender != ~(uint32)0 && connectData->fRemoteCD->fCurrentSender != myTransID )
	    {
	    gettimeofday( &now, NULL );
	    tdiff = (now.tv_sec-start.tv_sec)*1000 + (now.tv_usec-start.tv_usec)/1000;
	    if ( (useTimeout && tdiff > timeout_ms) || tdiff > fConnectionTimeout*5 )
		{
		LOG( MLUCLog::kError, "SCI Lock", "Connection timeout" )
		    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "Timeout building connection to remote address " << *(BCLSCIAddressStruct*)address
		    << " in SCI Lock." << ENDLOG;
		LockError( ETIMEDOUT, fCom, address, callback );
		return ETIMEDOUT;
		}
	    INTERRUPT_WAIT;
	    }
	if (  connectData->fRemoteCD->fCurrentSender == myTransID )
	    break;
	do
	    {
	    ret = StartSequence( connectData, useTimeout, timeout_ms );
	    if ( ret )
		{
		LOG( MLUCLog::kError, "SCI LockConnection", "Start sequence error" )
		    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "Error starting map sequence for connection lock to remote address " << *(BCLSCIAddressStruct*)address
		    << " in SCI LockConnection: " << strerror( ret ) << " (" << ret << ")." << ENDLOG;
		return ret;
		}
	    connectData->fRemoteCD->fSenderRequest = myTransID;
	    ret = CheckSequence( connectData, useTimeout, timeout_ms );
	    }
	while ( ret == EAGAIN );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "SCI LockConnection", "Send Error" )
		<< "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "Error writing ID to remote address " << *(BCLSCIAddressStruct*)address
		<< " for connection lock in SCI LockConnection: " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	    return ret;
	    }


#ifndef _SCALI_INTERRUPT_BUGFIX_
	SCITriggerInterrupt( connectData->fRemoteRequestInt, 0, &mySCIError );
	fSCIError = mySCIError;
	if ( fSCIError != SCI_ERR_OK )
	    {
	    LOG( MLUCLog::kError, "SCI Lock", "Request interrupt error" )
		<< "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error " << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " triggering remote request interrupt "
		<< (uint32)connectData->fRemoteRequestInt << "." << ENDLOG;
	    LockError( EIO, fCom, address, callback );
	    return EIO;
	    }
#endif
	gettimeofday( &now, NULL );
	tdiff = (now.tv_sec-start.tv_sec)*1000 + (now.tv_usec-start.tv_usec)/1000;
	if ( (useTimeout && tdiff > timeout_ms) || tdiff > fConnectionTimeout*5 )
	    {
	    LOG( MLUCLog::kError, "SCI Lock", "Connection timeout" )
		<< "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "Timeout building connection to remote address " << *(BCLSCIAddressStruct*)address
		<< " in SCI Lock." << ENDLOG;
	    LockError( ETIMEDOUT, fCom, address, callback );
	    return ETIMEDOUT;
	    }
	}
    while ( connectData->fRemoteCD->fCurrentSender != myTransID );
    do
	{
	ret = StartSequence( connectData, useTimeout, timeout_ms );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "SCI LockConnection", "Start sequence error" )
		<< "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "Error starting map sequence for connection lock to remote address " << *(BCLSCIAddressStruct*)address
		<< " in SCI LockConnection: " << strerror( ret ) << " (" << ret << ")." << ENDLOG;
	    return ret;
	    }
	connectData->fRemoteCD->fSenderRequest = ~(uint32)0;
	ret = CheckSequence( connectData, useTimeout, timeout_ms );
	}
    while ( ret == EAGAIN );
    if ( ret )
	{
	LOG( MLUCLog::kError, "SCI LockConnection", "Send Error" )
	    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "Error writing ID to remote address " << *(BCLSCIAddressStruct*)address
	    << " for connection lock in SCI LockConnection: " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	return ret;
	}
    return 0;
    }

int BCLIntSCIComHelper::UnlockConnection( BCLSCIAddressStruct* address, TSCIConnectionData* connectData, BCLErrorCallback* callback, bool useTimeout, uint32 timeout_ms )
    {
    sci_error_t mySCIError;
    int ret;

    BCLIntSCIControlData scd( connectData->fRemoteCD );
    uint8 remoteByteOrder = scd.GetOriginalByteOrder();
    uint32 myTransID;
    BCLNetworkData::Transform( GetSegmentID(), myTransID, kNativeByteOrder, remoteByteOrder );

    do
	{
	ret = StartSequence( connectData, useTimeout, timeout_ms );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "SCI UnlockConnection", "Start sequence error" )
		<< "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "Error starting map sequence for connection lock to remote address " << *(BCLSCIAddressStruct*)address
		<< " in SCI UnlockConnection: " << strerror( ret ) << " (" << ret << ")." << ENDLOG;
	    return ret;
	    }
	
	connectData->fRemoteCD->fCurrentSender = ~(uint32)0;
	
	ret = CheckSequence( connectData, useTimeout, timeout_ms );
	}
    while ( ret == EAGAIN );
    if ( ret )
	{
	LOG( MLUCLog::kError, "SCI UnlockConnection", "Send Error" )
	    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "Error writing ID to remote address " << *(BCLSCIAddressStruct*)address
	    << " for connection lock in SCI UnlockConnection: " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	return ret;
	}
    
#ifndef _SCALI_INTERRUPT_BUGFIX_
    SCITriggerInterrupt( connectData->fRemoteRequestInt, 0, &mySCIError );
#else
    mySCIError = SCI_ERR_OK;
#endif
    return 0;
    }


#ifndef SISCI_NO_DMA
int BCLIntSCIComHelper::CreateLocalSegment( TSCIConnectionData* connectData, unsigned int size )
    {
    if ( !connectData )
	return EFAULT;

    sci_error_t mySCIError;
    TSCIConnectionData* localConnectData = NULL;
    vector<TSCIConnectionData>::iterator iter, end;
    iter = fConnections.begin();
    end = fConnections.end();
    while ( iter != end )
	{
	if ( iter->fAddress == connectData->fAddress )
	    {
#if __GNUC__>=3
	    localConnectData = iter.base();
#else
	    localConnectData = iter;
#endif
	    break;
	    }
	iter++;
	}

    if ( connectData->fLocalSegment )
	{
	connectData->fLocalUsage++;
	if ( localConnectData )
	    localConnectData->fLocalUsage++;
	return 0;
	}
    

    // Find an unused segment ID between kBCLIntSCILocalSegmentIDStart and kBCLIntSCILocalSegmentIDMax
    bool found = false;
    unsigned int idStart = kBCLIntSCILocalSegmentIDStart;
    do
	{
	found = true;
	SCICreateSegment( connectData->fSCIDesc, &(connectData->fLocalSegment), idStart, size,
			  NULL, NULL, 0, &mySCIError );
	if ( mySCIError == SCI_ERR_SEGMENTID_USED )
	    {
	    found = false;
	    if ( idStart == kBCLIntSCILocalSegmentIDMax )
		break;
	    idStart++;
	    continue;
	    }
	if ( mySCIError != SCI_ERR_OK || !connectData->fLocalSegment )
	    {
	    LOG( MLUCLog::kError, "SCI CreateLocalSegment", "Error creating local segment" )
		<< "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)mySCIError << MLUCLog::kDec << " creating local SCI memory segment " 
		<< MLUCLog::kDec << "of size " << size << " (0x" << MLUCLog::kHex << size << ")." << ENDLOG;
	    int error;
	    switch ( mySCIError )
		{
		case SCI_ERR_SIZE_ALIGNMENT:
		    error = ENOTSUP; break;
		default:
		    error = ENXIO; break;
		}
	    return error;
	    }
	}
    while ( !found );
    if ( !found )
	{
	LOG( MLUCLog::kError, "SCI CreateLocalSegment", "No free local segment ID" )
	    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "No free local segment ID available." << ENDLOG;
	return EBUSY;
	}
    LOG( MLUCLog::kDebug, "SCI CreateLocalSegment", "Local segment ID" )
	<< "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "Local segment ID: 0x" << idStart
	<< " (" << MLUCLog::kDec << idStart << ")." << ENDLOG;

    SCIPrepareSegment( connectData->fLocalSegment, fAddress->fAdapterNr, 0, &mySCIError);
    if ( mySCIError != SCI_ERR_OK )
	{
	LOG( MLUCLog::kError, "SCI CreateLocalSegment", "Error preparing local segment" )
	    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)mySCIError << MLUCLog::kDec << " preparing local SCI memory segment " 
	    << MLUCLog::kDec << "of size " << size
	    << " (0x" << MLUCLog::kHex << size << ")." << ENDLOG;
	DestroyLocalSegment( connectData );
	return EIO;
	}
    connectData->fLocalAddr = (uint8*)SCIMapLocalSegment( connectData->fLocalSegment, &(connectData->fLocalMap), 0, size, 
						 NULL, 0, &mySCIError);
    if ( mySCIError != SCI_ERR_OK )
	{
	LOG( MLUCLog::kError, "SCI CreateLocalSegment", "Error mapping local segment" )
	    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " mapping local SCI memory segment "
	    << MLUCLog::kDec << "of size " << size
	    << " (0x" << MLUCLog::kHex << size << ")." << ENDLOG;
	int error;
	switch ( mySCIError )
	    {
	    case SCI_ERR_OUT_OF_RANGE:
		error = ERANGE; break;
	    case SCI_ERR_SIZE_ALIGNMENT:
		error = ENOTSUP; break;
	    case SCI_ERR_OFFSET_ALIGNMENT:
		error = ENOTSUP; break;
	    default:
		error = ENXIO; break;
	    }
	DestroyLocalSegment( connectData );
	return error;
	}
    connectData->fLocalSize = size;
    connectData->fLocalUsage++;

    if ( localConnectData )
	{
	localConnectData->fLocalSegment = connectData->fLocalSegment;
	localConnectData->fLocalAddr = connectData->fLocalAddr;
	localConnectData->fLocalMap = connectData->fLocalMap;
	localConnectData->fLocalSize = size;
	connectData->fLocalUsage++;
	}
    return 0;
    }

int BCLIntSCIComHelper::DestroyLocalSegment( TSCIConnectionData* connectData )
    {
    if ( !connectData )
	return EFAULT;
    if ( !connectData->fLocalSegment )
	return 0;

    sci_error_t mySCIError;
    TSCIConnectionData* localConnectData = NULL;
    vector<TSCIConnectionData>::iterator iter, end;
    iter = fConnections.begin();
    end = fConnections.end();
    while ( iter != end )
	{
	if ( iter->fAddress == connectData->fAddress )
	    {
#if __GNUC__>=3
	    localConnectData = iter.base();
#else
	    localConnectData = iter;
#endif
	    break;
	    }
	iter++;
	}

    connectData->fLocalUsage--;
    if ( localConnectData )
	{
	localConnectData->fLocalUsage--;
	if ( localConnectData->fLocalUsage )
	    {
	    connectData->fLocalSegment = NULL;
	    connectData->fLocalAddr = NULL;
	    connectData->fLocalMap = NULL;
	    connectData->fLocalSize = 0;
	    return 0;
	    }
	}

    if ( connectData->fLocalMap )
	{
	SCIUnmapSegment( connectData->fLocalMap, 0, &mySCIError );
	connectData->fLocalMap = NULL;
	connectData->fLocalAddr = NULL;
	}
    if ( connectData->fLocalSegment )
	{
	SCIRemoveSegment( connectData->fLocalSegment, 0, &mySCIError );
	connectData->fLocalSegment = NULL;
	}
    connectData->fLocalSize = 0;
    connectData->fLocalUsage = 0;

    if ( localConnectData )
	{
	localConnectData->fLocalSegment = NULL;
	localConnectData->fLocalAddr = NULL;
	localConnectData->fLocalMap = NULL;
	localConnectData->fLocalSize = 0;
	}
    return 0;
    }
#endif // SISCI_NO_DMA


int BCLIntSCIComHelper::StartSequence( TSCIConnectionData* connectData, bool useTimeout, uint32 timeout_ms )
    {
#if 1
    sci_sequence_status_t sequenceStatus;
    sci_error_t mySCIError;
    struct timeval start, now;
#ifdef SEQUENCE_BENCH
    struct timeval bs, bn;
    unsigned long long btd;
    gettimeofday( &bs, NULL );
#endif
    if ( useTimeout )
	gettimeofday( &start, NULL );
    do
	{
	if ( useTimeout )
	    {
	    unsigned long td;
	    gettimeofday( &now, NULL );
	    td = (now.tv_sec-start.tv_sec)*1000 + (now.tv_usec-start.tv_usec)/1000;
	    if ( td > timeout_ms )
		{
		LOG( MLUCLog::kError, "SCI Start Sequence", "Start Sequence error" )
		    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "Timeout trying to start sequence for connection to remote address " << connectData->fAddress
		    << " in SCI Start Sequence: 0x" << mySCIError << "." << ENDLOG;
		return ETIMEDOUT;
		}
	    }
	sequenceStatus = SCIStartSequence( connectData->fMapSequence, 0, &mySCIError );
	if ( mySCIError != SCI_ERR_OK )
	    {
	    LOG( MLUCLog::kError, "SCI Start Sequence", "Start Sequence error" )
		<< "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "Error starting sequence for connection to remote address " << connectData->fAddress
		<< " in SCI Start Sequence: 0x" << mySCIError << "." << ENDLOG;
	    return ENXIO;
	    }
	}
    while ( sequenceStatus != SCI_SEQ_OK );
#ifdef SEQUENCE_BENCH
    gettimeofday( &bn, NULL );
    btd = (bn.tv_sec-bs.tv_sec);
    btd *= 1000000;
    btd += (bn.tv_usec-bs.tv_usec);
    LOG( MLUCLog::kBenchmark, "SCI Start Sequence", "Time" )
	<< "Time: " << MLUCLog::kDec << btd << " usec." << ENDLOG;
#endif
#endif
    return 0;
    }

int BCLIntSCIComHelper::CheckSequence( TSCIConnectionData* connectData, bool useTimeout, uint32 timeout_ms )
    {
#ifdef SEQUENCE_BENCH
    struct timeval bs, bn;
    unsigned long long btd;
    gettimeofday( &bs, NULL );
#endif
#if 0
    SCIStoreBarrier( connectData->fMapSequence, 0 );
#else
    sci_error_t mySCIError;
    sci_sequence_status_t sequenceStatus;
    sequenceStatus = SCICheckSequence( connectData->fMapSequence, 0, &mySCIError );
#ifdef SEQUENCE_BENCH
    gettimeofday( &bn, NULL );
    btd = (bn.tv_sec-bs.tv_sec);
    btd *= 1000000;
    btd += (bn.tv_usec-bs.tv_usec);
    LOG( MLUCLog::kBenchmark, "SCI Check Sequence", "Time" )
	<< "Time: " << MLUCLog::kDec << btd << " usec." << ENDLOG;
#endif
#endif
#if 0
    return 0;
#else
    if ( mySCIError != SCI_ERR_OK )
	{
	return ENXIO;
	}
    switch ( sequenceStatus )
	{
	case SCI_SEQ_OK:
	    return 0;
	case SCI_SEQ_NOT_RETRIABLE:
	    return EIO;
	case SCI_SEQ_PENDING: // Fallthrough
	case SCI_SEQ_RETRIABLE:
	    return EAGAIN;
	default:
	    return ENXIO;
	}
#endif
    }


int BCLIntSCIComHelper::UpdateCachedCD( BCLSCIAddressStruct* address, BCLIntSCIControlDataStruct* remoteCachedCD, bool useTimeout, uint32 timeout_ms )
    {
    TSCIConnectionData* connectData = NULL;
    vector<TSCIConnectionData>::iterator iter, end;
    iter = fConnections.begin();
    end = fConnections.end();
    while ( iter != end )
	{
	if ( iter->fAddress == *address )
	    {
#if __GNUC__>=3
	    connectData = iter.base();
#else
	    connectData = iter;
#endif
	    break;
	    }
	iter++;
	}
    if ( !connectData )
	return EINVAL;

    if ( !connectData->fLocked )
	return EPERM;

    memcpy( &(connectData->fRemoteCachedCD), connectData->fRemoteCD, sizeof(connectData->fRemoteCachedCD) );
    if ( remoteCachedCD )
	memcpy( remoteCachedCD, &(connectData->fRemoteCachedCD), sizeof(connectData->fRemoteCachedCD) );
    return 0;
    }


int BCLIntSCIComHelper::WriteBackCachedCD( BCLSCIAddressStruct* address, BCLIntSCIControlDataStruct* cachedCD )
    {
    TSCIConnectionData* connectData = NULL;
    vector<TSCIConnectionData>::iterator iter, end;
    iter = fConnections.begin();
    end = fConnections.end();
    while ( iter != end )
	{
	if ( iter->fAddress == *address )
	    {
#if __GNUC__>=3
	    connectData = iter.base();
#else
	    connectData = iter;
#endif
	    break;
	    }
	iter++;
	}
    if ( !connectData )
	return EINVAL;

    if ( !connectData->fLocked )
	return EPERM;

    if ( cachedCD )
	memcpy( &(connectData->fRemoteCachedCD), cachedCD, sizeof(connectData->fRemoteCachedCD) );
    return 0;
    }



#ifdef USE_INTERRUPT_CALLBACKS
void BCLIntSCIComHelper::SetRequestCallback( void (*reqCB)( void*), void* cbArg )
    {
    fRequestInterruptCallback = reqCB;
    fRequestInterruptData = cbArg;
    }

sci_callback_action_t BCLIntSCIComHelper::IntRequestInterruptCallback( void* arg, sci_local_interrupt_t interrupt, sci_error_t status )
    {
    if ( ((BCLIntSCIComHelper*)arg)->fRequestInterruptCallback )
	(*(((BCLIntSCIComHelper*)arg)->fRequestInterruptCallback))( ((BCLIntSCIComHelper*)arg)->fRequestInterruptData );
    return SCI_CALLBACK_CONTINUE;
    }
#endif


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
