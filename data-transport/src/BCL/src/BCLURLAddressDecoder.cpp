/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLURLAddressDecoder.hpp"
#include "MLUCLog.hpp"
#include "MLUCString.hpp"
#ifdef USE_SCI
#include "BCLSCIMsgCommunication.hpp"
#include "BCLSCIBlobCommunication.hpp"
#include "BCLSCIAddress.hpp"
#include "BCLSCIComID.hpp"
#endif
#ifdef USE_TCP
#include "BCLTCPComID.hpp"
#include "BCLTCPAddress.hpp"
#include "BCLTCPMsgCommunication.hpp"
#include "BCLTCPBlobCommunication.hpp"
#endif
#ifdef USE_UDAPL
#include "BCLUDAPLComID.hpp"
#include "BCLUDAPLAddress.hpp"
#if 0
#include "BCLUDAPLMsgCommunication.hpp"
#endif
#include "BCLUDAPLBlobCommunication.hpp"
#endif
#include <pthread.h>
#if defined(USE_TCP) or defined(USE_UDAPL)
#include <sys/utsname.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
extern int h_errno;
#endif
#include <cerrno>
#include <cstdlib>

#ifdef USE_SCI
#include <fstream>
#endif


#ifdef USE_SCI
int BCLGetSCINodeIDFromName( const char* nodeName, uint16 adapterNr, uint16& nodeID );

static const char* kBCLSCINodenameFile = "/etc/scinodes";

#endif


pthread_mutex_t tcpMutex = PTHREAD_MUTEX_INITIALIZER;


int BCLCheckURL( const char* address_url, bool isLocal, bool& isBlob )
    {
    MLUCString tmp;
    MLUCString url = address_url;
    MLUCString::TSizeType urllen = url.Length(), tmplen;
    MLUCString::TSizeType protpos;
    protpos =  url.Find( "://" );
    if ( protpos >= urllen || protpos == MLUCString::kNoPos )
	return EINVAL;
    MLUCString prot = url.Substr( 0, protpos );
    tmplen = prot.Length();
#if 0
    MLUCString tmpstr;
    tmpstr = prot.Substr( tmplen-3, tmplen-1 );
#endif
    if ( tmplen >= 4 && prot.Substr( tmplen-4, 4 ) == "blob" )
	{
	prot.Erase( tmplen-4, 4 );
	isBlob = true;
	}
    else if ( tmplen >= 3 && prot.Substr( tmplen-3, 3 ) == "msg" )
	{
	prot.Erase( tmplen-3, 3 );
	isBlob = false;
	}
    else
	isBlob = false;

#ifdef USE_SCI  
    if ( prot == "sci" )
	{
	int ret;
	MLUCString::TSizeType nodeidpos, adaptnopos, segidpos, endpos;
	nodeidpos = protpos+3;
	adaptnopos = url.Find( ".", protpos+3 );
	if ( adaptnopos == MLUCString::kNoPos ) 
	    return EINVAL;
	segidpos = url.Find( ":", adaptnopos+1 );
	if ( segidpos == MLUCString::kNoPos )
	    return EINVAL;
	endpos = url.Find( "/", segidpos+1 );
	MLUCString nodeids, adaptnos, segids;
	nodeids = url.Substr( protpos+3, adaptnopos-(protpos+1)-2 );
	adaptnos = url.Substr( adaptnopos+1, segidpos-(adaptnopos+1) );
	if ( endpos == MLUCString::kNoPos )
	    segids = url.Substr( segidpos+1, endpos );
	else
	    segids = url.Substr( segidpos+1, endpos-(segidpos+1) );
	uint16 nodeid, adaptno, segid;
	char* err;
	adaptno = (uint16)strtoul( adaptnos.c_str(), &err, 0 );
	if ( *err != 0 )
	    return EINVAL;
	nodeid = (uint16)strtoul( nodeids.c_str(), &err, 0 );
	if ( *err != 0 )
	    {
	    ret = BCLGetSCINodeIDFromName( nodeids.c_str(), adaptno, nodeid );
	    if ( ret )
		return ret;
	    }
	segid = (uint16)strtoul( segids.c_str(), &err, 0 );
	if ( *err != 0 )
	    return EINVAL;
	return 0;
	}
#endif

#ifdef USE_TCP
    if ( prot == "tcp" )
	{
	pthread_mutex_lock( &tcpMutex );
	MLUCString::TSizeType ippos, portnopos, endpos;
	ippos = protpos+3;
	portnopos = url.Find( ":", protpos+3 );
	if ( portnopos == MLUCString::kNoPos )
	    {
	    //printf( "%s:%d\n", __FILE__, __LINE__ );
	    pthread_mutex_unlock( &tcpMutex );
	    return EINVAL;
	    }
	endpos = url.Find( "/", portnopos+1 );
	MLUCString ips, portnos;
	if ( portnopos>protpos+3 )
	    ips = url.Substr( protpos+3, portnopos-1-(protpos+2) );
	else
	    {
	    if ( !isLocal )
		{
		pthread_mutex_unlock( &tcpMutex );
		return EINVAL;
		}
	    ips = "";
	    }
	//printf( "ips: %s\n", ips.c_str() );
	if ( endpos == MLUCString::kNoPos )
	    portnos = url.Substr( portnopos+1 );
	else
	    portnos = url.Substr( portnopos+1, endpos-1-(portnopos) );

	MLUCString::TSizeType optpos, optendpos;
	optendpos = endpos;
	int slotCntExp2 = -1;
	while ( optendpos != MLUCString::kNoPos )
	    {
	    char* err;
	    MLUCString opt, optname, optval;
	    MLUCString::TSizeType eqpos;
	    optpos = optendpos+1;
	    bool hasVal = false;
	    optendpos = url.Find( "/", optpos );
	    if ( optendpos == MLUCString::kNoPos )
		opt = url.Substr( optpos );
	    else
		opt = url.Substr( optpos, optendpos-optpos );
	    ////printf( "Opt: %s\n", opt.c_str() );
	    eqpos = opt.Find( "=" );
	    if ( eqpos != MLUCString::kNoPos )
		{
		optname = opt.Substr( 0, eqpos );
		optval = opt.Substr( eqpos+1 );
		////printf( "eqpos: %d - optname: %s - optval: %s\n", eqpos, optname.c_str(), optval.c_str() );
		hasVal = true;
		}
	    else
		optname = opt;
	    if ( optname == "slotslog2" && hasVal )
		{
		slotCntExp2 = (int)strtol( optval.c_str(), &err, 0 );
		////printf( "slotCntExp2 == %d\n", slotCntExp2 );
		if ( *err != 0 )
		    {
		    //printf( "%s:%d\n", __FILE__, __LINE__ );
		    pthread_mutex_unlock( &tcpMutex );
		    return EINVAL;
		    }
		}
	    else if ( optname == "" ) 
		{
		// Empty, nothing happens here, this occurs when the address ends with a "/"
		} 
	    else 
		{
		//printf( "%s:%d\n", __FILE__, __LINE__ );
		pthread_mutex_unlock( &tcpMutex );
		return EINVAL;
		}
	    }

	uint32 ip;
	uint16 portno;
	char* err;
	// The code below just accepts IP addresses...
// 	ip = (uint32)ntohl( inet_addr( ips.c_str() ) );
// 	if ( ip == (uint32)-1 )
// 	    return EINVAL;

	// This code also accepts hostnames in addition to IP addresses.
	// Invalid hostnames are ignored for local addresses (with a communication object)
        struct addrinfo hints;
        struct addrinfo *result, *rp;
        int s;
        memset(&hints, 0, sizeof(struct addrinfo));
        hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
        hints.ai_socktype = SOCK_STREAM; /* Datagram socket */
        hints.ai_flags = AI_PASSIVE;    /* For wildcard IP address */
        hints.ai_protocol = IPPROTO_TCP;
        hints.ai_canonname = NULL;
        hints.ai_addr = NULL;
        hints.ai_next = NULL;

	ip = (uint32)-1;
	if ( ips.Size()>0 )
	    {
            s = getaddrinfo(ips.c_str(), NULL, &hints, &result);
            if ( s != 0 )
                {
                  LOG( MLUCLog::kError, "BCLURLAddressDecoder::BCLDecodeURL", "Error resoving address" )
                    << "Error resolving '" << ips.c_str() << "': "
                    << gai_strerror(s) << " (" << MLUCLog::kDec << s << ")." << ENDLOG;
                //printf( "%s:%d\n", __FILE__, __LINE__ );
		pthread_mutex_unlock( &tcpMutex );
                return EINVAL;
                }
            if ( s != 0 )
		{
		// Invalid hostname or IP address for local address. We get our own hostname
		// 	    char myname[ SYS_NMLN ];
		// 	    if ( gethostname( myname, SYS_NMLN )==-1 )
		// 		return EINVAL;
		// 	    he = gethostbyname( myname );
		// 	    if ( he == NULL )
		// 		return EINVAL;
		// Invalid hostname or IP address for local address, we let the system decide.
		ip = INADDR_ANY;
		}
	    else
		{
                for(rp=result; rp!=NULL; rp=rp->ai_next)
                  {
                    if (rp->ai_addrlen == sizeof(struct sockaddr_in)){
                      struct sockaddr_in* addr = (struct sockaddr_in*) result->ai_addr;
                      ip = ntohl( addr->sin_addr.s_addr );
                      break;
                    }
                  }
		freeaddrinfo(result);
		if ( ip == (uint32)-1 )
		    {
		    //printf( "%s:%d\n", __FILE__, __LINE__ );
		    pthread_mutex_unlock( &tcpMutex );
		    return EINVAL;
		    }
		}
	    }
	ip = INADDR_ANY;

	portno = (uint16)strtoul( portnos.c_str(), &err, 0 );
	if ( *err != (char)0 )
	    {
	    //printf( "%s:%d\n", __FILE__, __LINE__ );
	    pthread_mutex_unlock( &tcpMutex );
	    return EINVAL;
	    }
	pthread_mutex_unlock( &tcpMutex );
	return 0;
	}
#endif


#ifdef USE_UDAPL
    if ( prot == "udapl" )
	{
	if ( !isBlob )
	    return ENOTSUP;
	MLUCString::TSizeType adapterpos, ippos, connqualpos, endpos;
	adapterpos = protpos+3;
	ippos = url.Find( ":", adapterpos );
	if ( ippos == MLUCString::kNoPos )
	    {
	    //printf( "%s:%d\n", __FILE__, __LINE__ );
	    return EINVAL;
	    }
	connqualpos = url.Find( ":", ippos+1 );
	if ( connqualpos == MLUCString::kNoPos )
	    {
	    //printf( "%s:%d\n", __FILE__, __LINE__ );
	    return EINVAL;
	    }
	endpos = url.Find( "/", connqualpos+1 );
	MLUCString adapters, ips, connquals;
	if ( ippos>adapterpos )
	  adapters = url.Substr( adapterpos, ippos-adapterpos ); // I know the two 1s cancel each other, but this way it is easier to understand (later for me...)
	else
	  return EINVAL;
	if ( adapters.Length()>31 )
	  return EINVAL;
	//printf( "adapters: %s\n", adapters.c_str() );
	if ( connqualpos>ippos+1 )
	    ips = url.Substr( ippos+1, connqualpos-(ippos+1) );
	else
	    {
	    if ( !isLocal )
		return EINVAL;
	    ips="";
	    }
	//printf( "ips: %s\n", ips.c_str() );
	if ( endpos == MLUCString::kNoPos )
	    connquals = url.Substr( connqualpos+1 );
	else
	    connquals = url.Substr( connqualpos+1, endpos-(connqualpos+1) );
	//printf( "connquals: %s\n", connquals.c_str() );

	MLUCString::TSizeType optpos, optendpos;
	optendpos = endpos;
	int slotCntExp2 = -1;
	while ( optendpos != MLUCString::kNoPos )
	    {
	    char* err;
	    MLUCString opt, optname, optval;
	    MLUCString::TSizeType eqpos;
	    optpos = optendpos+1;
	    bool hasVal = false;
	    optendpos = url.Find( "/", optpos );
	    if ( optendpos == MLUCString::kNoPos )
		opt = url.Substr( optpos );
	    else
		opt = url.Substr( optpos, optendpos-optpos );
	    ////printf( "Opt: %s\n", opt.c_str() );
	    eqpos = opt.Find( "=" );
	    if ( eqpos != MLUCString::kNoPos )
		{
		optname = opt.Substr( 0, eqpos );
		optval = opt.Substr( eqpos+1 );
		////printf( "eqpos: %d - optname: %s - optval: %s\n", eqpos, optname.c_str(), optval.c_str() );
		hasVal = true;
		}
	    else
		optname = opt;
	    if ( optname == "slotslog2" && hasVal )
		{
		slotCntExp2 = (int)strtol( optval.c_str(), &err, 0 );
		////printf( "slotCntExp2 == %d\n", slotCntExp2 );
		if ( *err != 0 )
		    {
		    //printf( "%s:%d\n", __FILE__, __LINE__ );
		    return EINVAL;
		    }
		}
	    else if ( optname == "" ) 
		{
		// Empty, nothing happens here, this occurs when the address ends with a "/"
		} 
	    else 
		{
		//printf( "%s:%d\n", __FILE__, __LINE__ );
		return EINVAL;
		}
	    }

	uint32 ip;
	uint16 connqual;
	char* err;
	// The code below just accepts IP addresses...
// 	ip = (uint32)ntohl( inet_addr( ips.c_str() ) );
// 	if ( ip == (uint32)-1 )
// 	    return EINVAL;

	// This code also accepts hostnames in addition to IP addresses.
	// Invalid hostnames are ignored for local addresses (with a communication object)
	struct addrinfo *target;

	ip = (uint32)-1;
	if ( ips.Size()>0 )
	    {
	    int ret;
	    ret = getaddrinfo( ips.c_str(), NULL, NULL, &target );
	    if ( ret != 0 && !isLocal )
		return EINVAL;
	    ip= ((struct sockaddr_in *)target->ai_addr)->sin_addr.s_addr;
	    if ( ip == (uint32)-1 )
		return EINVAL;
	    }
	else
	    ip = INADDR_ANY;

	connqual = (uint16)strtoul( connquals.c_str(), &err, 0 );
	if ( *err != (char)0 )
	    return EINVAL;
	return 0;
	}
#endif
    return EINVAL;
    }

int BCLDecodeURL( const char* address_url, BCLCommunication** com_obj,
		  BCLAbstrAddressStruct** address, bool& isBlob )
    {
    if ( com_obj )
	*com_obj = NULL;
    if ( address )
	*address = NULL;

    MLUCString tmp;
    MLUCString url = address_url;
    MLUCString::TSizeType urllen = url.Length(), tmplen;;
    uint32 comid;
    MLUCString::TSizeType protpos;
    protpos =  url.Find( "://" );
    if ( protpos >= urllen || protpos == MLUCString::kNoPos )
	return EINVAL;
    MLUCString prot = url.Substr( 0, protpos );
    tmplen = prot.Length();
    MLUCString tmpstr;
    tmpstr = prot.Substr( tmplen-3, tmplen-1 );
    if ( tmplen >= 4 && prot.Substr( tmplen-4, 4 ) == "blob" )
	{
	prot.Erase( tmplen-4, 4 );
	isBlob = true;
	}
    else if ( tmplen >= 3 && prot.Substr( tmplen-3, 3 ) == "msg" )
	{
	prot.Erase( tmplen-3, 3 );
	isBlob = false;
	}
    else
	isBlob = false;

#ifdef USE_SCI  
    if ( prot == "sci" )
	{
	int ret;
	comid = kSCIComID;
	MLUCString::TSizeType nodeidpos, adaptnopos, segidpos, endpos;
	nodeidpos = protpos+3;
	adaptnopos = url.Find( ".", protpos+3 );
	if ( adaptnopos == MLUCString::kNoPos ) 
	    return EINVAL;
	segidpos = url.Find( ":", adaptnopos+1 );
	if ( segidpos == MLUCString::kNoPos )
	    return EINVAL;
	endpos = url.Find( "/", segidpos+1 );
	MLUCString nodeids, adaptnos, segids;
	nodeids = url.Substr( protpos+3, adaptnopos-(protpos+1)-2 );
	adaptnos = url.Substr( adaptnopos+1, segidpos-(adaptnopos+1) );
	if ( endpos == MLUCString::kNoPos )
	    segids = url.Substr( segidpos+1, endpos );
	else
	    segids = url.Substr( segidpos+1, endpos-(segidpos+1) );
	uint16 nodeid, adaptno, segid;
	char* err;
	adaptno = (uint16)strtoul( adaptnos.c_str(), &err, 0 );
	if ( *err != 0 )
	    return EINVAL;
	nodeid = (uint16)strtoul( nodeids.c_str(), &err, 0 );
	if ( *err != 0 )
	    {
	    ret = BCLGetSCINodeIDFromName( nodeids.c_str(), adaptno, nodeid );
	    if ( ret )
		return ret;
	    }
	segid = (uint16)strtoul( segids.c_str(), &err, 0 );
	if ( *err != 0 )
	    return EINVAL;
	BCLSCIAddressStruct* sciAddr = new BCLSCIAddressStruct;
	if ( !sciAddr )
	    return ENOMEM;
	BCLSCIAddress sciAddrC;
	BCLSCIAddressStruct* sciAddrTmp = sciAddrC.GetData();
	*sciAddr = *sciAddrTmp;
	if ( com_obj )
	    {
	    if ( !isBlob )
		*com_obj = new BCLSCIMsgCommunication();
	    else
		*com_obj = new BCLSCIBlobCommunication();
	    if ( !*com_obj )
		{
		delete sciAddr;
		return ENOMEM;
		}
	    }
	sciAddr->fNodeID = nodeid;
	sciAddr->fAdapterNr = adaptno;
	sciAddr->fSegmentID = segid;
	sciAddr->fComID = comid;
	*address = (BCLAbstrAddressStruct*)sciAddr;
	return 0;
	}
#endif

#ifdef USE_TCP
    if ( prot == "tcp" )
	{
	comid = kTCPComID;
	MLUCString::TSizeType ippos, portnopos, endpos;
	ippos = protpos+3;
	portnopos = url.Find( ":", protpos+3 );
	if ( portnopos == MLUCString::kNoPos )
	    {
	    //printf( "%s:%d\n", __FILE__, __LINE__ );
	    return EINVAL;
	    }
	endpos = url.Find( "/", portnopos+1 );
	MLUCString ips, portnos;
	if ( portnopos>protpos+3 )
	    ips = url.Substr( protpos+3, portnopos-1-(protpos+2) );
	else
	    {
	    if ( !com_obj )
		return EINVAL;
	    ips="";
	    }
	//printf( "ips: %s\n", ips.c_str() );
	if ( endpos == MLUCString::kNoPos )
	    portnos = url.Substr( portnopos+1 );
	else
	    portnos = url.Substr( portnopos+1, endpos-1-(portnopos) );

	MLUCString::TSizeType optpos, optendpos;
	optendpos = endpos;
	int slotCntExp2 = -1;
	while ( optendpos != MLUCString::kNoPos )
	    {
	    char* err;
	    MLUCString opt, optname, optval;
	    MLUCString::TSizeType eqpos;
	    optpos = optendpos+1;
	    bool hasVal = false;
	    optendpos = url.Find( "/", optpos );
	    if ( optendpos == MLUCString::kNoPos )
		opt = url.Substr( optpos );
	    else
		opt = url.Substr( optpos, optendpos-optpos );
	    ////printf( "Opt: %s\n", opt.c_str() );
	    eqpos = opt.Find( "=" );
	    if ( eqpos != MLUCString::kNoPos )
		{
		optname = opt.Substr( 0, eqpos );
		optval = opt.Substr( eqpos+1 );
		////printf( "eqpos: %d - optname: %s - optval: %s\n", eqpos, optname.c_str(), optval.c_str() );
		hasVal = true;
		}
	    else
		optname = opt;
	    if ( optname == "slotslog2" && hasVal )
		{
		slotCntExp2 = (int)strtol( optval.c_str(), &err, 0 );
		////printf( "slotCntExp2 == %d\n", slotCntExp2 );
		if ( *err != 0 )
		    {
		    //printf( "%s:%d\n", __FILE__, __LINE__ );
		    return EINVAL;
		    }
		}
	    else if ( optname == "" ) 
		{
		// Empty, nothing happens here, this occurs when the address ends with a "/"
		} 
	    else 
		{
		//printf( "%s:%d\n", __FILE__, __LINE__ );
		return EINVAL;
		}
	    }

	uint32 ip;
	uint16 portno;
	char* err;
	// The code below just accepts IP addresses...
// 	ip = (uint32)ntohl( inet_addr( ips.c_str() ) );
// 	if ( ip == (uint32)-1 )
// 	    return EINVAL;

	// This code also accepts hostnames in addition to IP addresses.
	// Invalid hostnames are ignored for local addresses (with a communication object)
	struct addrinfo hints;
	struct addrinfo *result, *rp;
	int s;
	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
	hints.ai_socktype = SOCK_STREAM; /* Datagram socket */
	hints.ai_flags = AI_PASSIVE;    /* For wildcard IP address */
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_canonname = NULL;
	hints.ai_addr = NULL;
	hints.ai_next = NULL;
	
	ip = (uint32)-1;
	if ( ips.Size()>0 )
	    {
	    s = getaddrinfo(ips.c_str(), NULL, &hints, &result);
	    if ( s != 0 && !com_obj )
		{
		  LOG( MLUCLog::kError, "BCLURLAddressDecoder::BCLDecodeURL", "Error resoving address" )
		    << "Error resolving '" << ips.c_str() << "': "
		    << gai_strerror(s) << " (" << MLUCLog::kDec << s << ")." << ENDLOG;
		//printf( "%s:%d\n", __FILE__, __LINE__ );
		return EINVAL;
		}
	    if ( s != 0 )
		{
		// Invalid hostname or IP address for local address. We get our own hostname
		// 	    char myname[ SYS_NMLN ];
		// 	    if ( gethostname( myname, SYS_NMLN )==-1 )
		// 		return EINVAL;
		// 	    he = gethostbyname( myname );
		// 	    if ( he == NULL )
		// 		return EINVAL;
		// Invalid hostname or IP address for local address, we let the system decide.
		ip = INADDR_ANY;
		}
	    else
		{
		for(rp=result; rp!=NULL; rp=rp->ai_next)
		  {
		    if (rp->ai_addrlen == sizeof(struct sockaddr_in)){
		      struct sockaddr_in* addr = (struct sockaddr_in*) result->ai_addr;
		      ip = ntohl( addr->sin_addr.s_addr );
		      break;
		    }
		  }
		freeaddrinfo(result);
		if ( ip == (uint32)-1 )
		    {
		    //printf( "%s:%d\n", __FILE__, __LINE__ );
		    return EINVAL;
		    }
		}
	    }
	else
	    ip = INADDR_ANY;

	portno = (uint16)strtoul( portnos.c_str(), &err, 0 );
	if ( *err != (char)0 )
	    {
	    //printf( "%s:%d\n", __FILE__, __LINE__ );
	    return EINVAL;
	    }
	BCLTCPAddressStruct* tcpAddr = new BCLTCPAddressStruct;
	if ( !tcpAddr )
	    {
	    //printf( "%s:%d\n", __FILE__, __LINE__ );
	    return ENOMEM;
	    }
	BCLTCPAddress tcpAddrC;
	BCLTCPAddressStruct* tcpAddrTmp = tcpAddrC.GetData();
	*tcpAddr = *tcpAddrTmp;
	if ( com_obj )
	    {
	    if ( !isBlob )
		*com_obj = new BCLTCPMsgCommunication( slotCntExp2 );
	    else
		*com_obj = new BCLTCPBlobCommunication( slotCntExp2 );
	    if ( !*com_obj )
		{
		//printf( "%s:%d\n", __FILE__, __LINE__ );
		delete tcpAddr;
		return ENOMEM;
		}
	    }
	tcpAddr->fIPNr = ip;
	tcpAddr->fPortNr = portno;
	tcpAddr->fComID = comid;
	*address = (BCLAbstrAddressStruct*)tcpAddr;
	return 0;
	}
#endif


#ifdef USE_UDAPL
    if ( prot == "udapl" )
	{
	if ( !isBlob )
	    return ENOTSUP;
	comid = kUDAPLComID;
	MLUCString::TSizeType adapterpos, ippos, connqualpos, endpos;
	adapterpos = protpos+3;
	ippos = url.Find( ":", adapterpos );
	if ( ippos == MLUCString::kNoPos )
	    {
	    //printf( "%s:%d\n", __FILE__, __LINE__ );
	    return EINVAL;
	    }
	connqualpos = url.Find( ":", ippos+1 );
	if ( connqualpos == MLUCString::kNoPos )
	    {
	    //printf( "%s:%d\n", __FILE__, __LINE__ );
	    return EINVAL;
	    }
	endpos = url.Find( "/", connqualpos+1 );
	MLUCString adapters, ips, connquals;
	if ( ippos>adapterpos )
	  adapters = url.Substr( adapterpos, ippos-adapterpos ); // I know the two 1s cancel each other, but this way it is easier to understand (later for me...)
	else
	  return EINVAL;
	if ( adapters.Length()>31 )
	  return EINVAL;
	//printf( "adapters: %s\n", adapters.c_str() );
	if ( connqualpos>ippos+1 )
	    ips = url.Substr( ippos+1, connqualpos-(ippos+1) );
	else
	    {
	    if ( !com_obj )
		return EINVAL;
	    ips="";
	    }
	//printf( "ips: %s\n", ips.c_str() );
	if ( endpos == MLUCString::kNoPos )
	    connquals = url.Substr( connqualpos+1 );
	else
	    connquals = url.Substr( connqualpos+1, endpos-(connqualpos+1) );
	//printf( "connquals: %s\n", connquals.c_str() );

	MLUCString::TSizeType optpos, optendpos;
	optendpos = endpos;
	int slotCntExp2 = -1;
	while ( optendpos != MLUCString::kNoPos )
	    {
	    char* err;
	    MLUCString opt, optname, optval;
	    MLUCString::TSizeType eqpos;
	    optpos = optendpos+1;
	    bool hasVal = false;
	    optendpos = url.Find( "/", optpos );
	    if ( optendpos == MLUCString::kNoPos )
		opt = url.Substr( optpos );
	    else
		opt = url.Substr( optpos, optendpos-optpos );
	    ////printf( "Opt: %s\n", opt.c_str() );
	    eqpos = opt.Find( "=" );
	    if ( eqpos != MLUCString::kNoPos )
		{
		optname = opt.Substr( 0, eqpos );
		optval = opt.Substr( eqpos+1 );
		////printf( "eqpos: %d - optname: %s - optval: %s\n", eqpos, optname.c_str(), optval.c_str() );
		hasVal = true;
		}
	    else
		optname = opt;
	    if ( optname == "slotslog2" && hasVal )
		{
		slotCntExp2 = (int)strtol( optval.c_str(), &err, 0 );
		////printf( "slotCntExp2 == %d\n", slotCntExp2 );
		if ( *err != 0 )
		    {
		    //printf( "%s:%d\n", __FILE__, __LINE__ );
		    return EINVAL;
		    }
		}
	    else if ( optname == "" ) 
		{
		// Empty, nothing happens here, this occurs when the address ends with a "/"
		} 
	    else 
		{
		//printf( "%s:%d\n", __FILE__, __LINE__ );
		return EINVAL;
		}
	    }

	uint32 ip;
	uint16 connqual;
	char* err;
	// The code below just accepts IP addresses...
// 	ip = (uint32)ntohl( inet_addr( ips.c_str() ) );
// 	if ( ip == (uint32)-1 )
// 	    return EINVAL;

	// This code also accepts hostnames in addition to IP addresses.
	// Invalid hostnames are ignored for local addresses (with a communication object)
	struct addrinfo *target;

	ip = (uint32)-1;
	if ( ips.Size()>0 )
	    {
	    int ret;
	    ret = getaddrinfo( ips.c_str(), NULL, NULL, &target );
	    if ( ret && !com_obj )
		return EINVAL;
	    if ( ret )
		ip = INADDR_ANY;
	    else
		{
		ip= ((struct sockaddr_in *)target->ai_addr)->sin_addr.s_addr;
		if ( ip == (uint32)-1 )
		    return EINVAL;
		}
	    }
	else
	    ip = INADDR_ANY;

	connqual = (uint16)strtoul( connquals.c_str(), &err, 0 );
	if ( *err != (char)0 )
	    return EINVAL;
	BCLUDAPLAddressStruct* udaplAddr = new BCLUDAPLAddressStruct;
	if ( !udaplAddr )
	    {
	    //printf( "%s:%d\n", __FILE__, __LINE__ );
	    return ENOMEM;
	    }
	BCLUDAPLAddress udaplAddrC;
	BCLUDAPLAddressStruct* udaplAddrTmp = udaplAddrC.GetData();
	*udaplAddr = *udaplAddrTmp;
	if ( com_obj )
	    {
#if 0
	    if ( !isBlob )
		*com_obj = new BCLUDAPLMsgCommunication( slotCntExp2 );
	    else
#endif
		*com_obj = new BCLUDAPLBlobCommunication( slotCntExp2 );
	    if ( !*com_obj )
		{
		//printf( "%s:%d\n", __FILE__, __LINE__ );
		delete udaplAddr;
		return ENOMEM;
		}
	    }
	strncpy( reinterpret_cast<char*>( udaplAddr->fIAName ), adapters.c_str(), 31 );
	udaplAddr->fSinFamily = ((struct sockaddr_in *)target->ai_addr)->sin_family;
	udaplAddr->fSinPort = ((struct sockaddr_in *)target->ai_addr)->sin_port;
	udaplAddr->fNodeID = ((struct sockaddr_in *)target->ai_addr)->sin_addr.s_addr;
	udaplAddr->fConnQual = connqual;
	udaplAddr->fComID = comid;
	*address = (BCLAbstrAddressStruct*)udaplAddr;
	return 0;
	}
#endif
    return EINVAL;
    }

int BCLDecodeLocalURL( const char* address_url, BCLCommunication*& com_obj,
		       BCLAbstrAddressStruct*& address, bool& isBlob )
    {
    return BCLDecodeURL( address_url, &com_obj, &address, isBlob );
    }


int BCLDecodeRemoteURL( const char* address_url, BCLAbstrAddressStruct*& address )
    {
    bool tmp;
    int ret = BCLDecodeURL( address_url, NULL, &address, tmp );
    if ( tmp )
	{
	delete address;
	address = NULL;
	return EINVAL;
	}
    return ret;
    }

void BCLFreeAddress( BCLCommunication* com_obj, BCLAbstrAddressStruct* address )
    {
    if ( com_obj )
	delete com_obj;
    if ( address )
	delete address;
    }

void BCLFreeAddress( BCLAbstrAddressStruct* address )
    {
    if ( address )
	{
	switch ( address->fComID )
	    {
#ifdef USE_TCP
	    case kTCPComID:
		delete (BCLTCPAddressStruct*)address;
		break;
#endif
#ifdef USE_UDAPL
	    case kUDAPLComID:
		delete (BCLUDAPLAddressStruct*)address;
		break;
#endif
#ifdef USE_SCI
	    case kSCIComID:
		delete (BCLSCIAddressStruct*)address;
		break;
#endif
	    default:
		LOG( MLUCLog::kWarning, "BCLFreeAddress(BCLAbstrAddressStruct*)", "Unknown Address ID" )
		    << "Unknown Address ID 0x" << MLUCLog::kHex << address->fComID << " (" << MLUCLog::kDec
		    << address->fComID << " in address 0x" << MLUCLog::kHex << (unsigned long)address << "." << ENDLOG;
		break;
	    }
	}
    }


void BCLGetAllowedURLs( vector<MLUCString>& msgURLs, vector<MLUCString>& blobURLs )
    {
    /*
    ** sci[.(msg|blob)]://<SCI Node ID>[.<Adapter-Nr>]:<Segment ID>/
    ** tcp[.(msg|blob)]://<IP Nr>:<Port Nr>/
    */
    msgURLs.clear();
    blobURLs.clear();
    MLUCString msg, blob;
#ifdef USE_SCI
    msg = "scimsg://<SCI Node ID>.<Adapter-Nr>:<Segment ID>/";
    msgURLs.insert( msgURLs.begin(), msg );
#endif
#ifdef USE_TCP
    msg = "tcpmsg://[<IP Nr>|<hostname>]:<Port Nr>/[slotslog2=<pre-alloc-slots>]/";
    msgURLs.insert( msgURLs.begin(), msg );
#endif
#ifdef USE_UDAPL
    msg = "udaplmsg://[<IP Nr>|<hostname>]:<Port Nr>/[slotslog2=<pre-alloc-slots>]/";
    msgURLs.insert( msgURLs.begin(), msg );
#endif

#ifdef USE_SCI
    blob = "sciblob://<SCI Node ID>.<Adapter-Nr>:<Segment ID>/";
    blobURLs.insert( blobURLs.begin(), blob );
#endif
#ifdef USE_TCP
    blob = "tcpblob://[<IP Nr>|<hostname>]:<Port Nr>/[slotslog2=<pre-alloc-slots>]/";
    blobURLs.insert( blobURLs.begin(), blob );
#endif
#ifdef USE_UDAPL
    blob = "udaplblob://[<IP Nr>|<hostname>]:<Port Nr>/[slotslog2=<pre-alloc-slots>]/";
    blobURLs.insert( blobURLs.begin(), blob );
#endif
    }


#ifdef USE_SCI

bool BCLGetSCINodeIDFromNameReadLine( istream& istr, MLUCString& line )
    {
    char tmp[ 256 ];
    char peeked;
    line = "";
    do
	{
	if ( !istr.good() )
	    return false;
	istr.get( tmp, 256, '\n' );
	line += (char*)tmp;
	if ( istr.eof() )
	    {
	    return true;
	    }
	peeked = istr.peek();
	if ( peeked == '\n' )
	    {
	    peeked = istr.get();
	    return true;
	    }
	}
    while ( true );
    }

bool BCLGetSCINodeIDFromNameGetToken( const char* text, MLUCString& token, const char*& pos )
    {
    if ( !text )
	return false;
    token = "";
    const char* iter = text;
    while ( *iter && (*iter==' ' || *iter=='\t' || *iter=='\n') )
	iter++;
    if ( !*iter || *iter=='#' )
	{
	pos = NULL;
	return true;
	}
    pos = iter;
    while ( *iter && *iter!=' ' && *iter!='\t' && *iter!='\n' && *iter!='#' )
	{
	token += *iter;
	iter++;
	}
    pos = iter;
    return true;
    }


int BCLGetSCINodeIDFromName( const char* nodeName, uint16 adapterNr, uint16& nodeID )
    {
    ifstream file;
    char* cpErr = NULL;
    uint16 nr = 0;

    file.open( kBCLSCINodenameFile );
    if ( !file.good() )
	return ENOENT;
    
    MLUCString line, token;
    const char* pos;
    while ( BCLGetSCINodeIDFromNameReadLine( file, line ) )
	{
	if ( !BCLGetSCINodeIDFromNameGetToken( line.c_str(), token, pos ) )
	    return EIO;
	if ( !pos )
	    continue; // Empty line.
	
	if ( !(token == nodeName) )
	    continue;
	
	nr = 0;
	while ( BCLGetSCINodeIDFromNameGetToken( line.c_str(), token, pos ) )
	    {
	    if ( nr == adapterNr )
		break;
	    nr++;
	    }
	if ( nr == adapterNr )
	    {
	    nodeID = strtoul( token.c_str(), &cpErr, 0 );
	    if ( cpErr && *cpErr!=0 )
		return EIO;
	    return 0;
	    }
	    else
		return EINVAL;
	}
    return EINVAL;
    }


#endif


void BCLEncodeAddress( BCLAbstrAddressStruct* address, bool isBlob, char*& url )
    {
    if ( !address )
	{
	url = NULL;
	return;
	}
    MLUCString tmp;
    MLUCString kind;
    if ( isBlob )
	kind = "blob";
    else
	kind = "msg";
    char buf[ 1024];
#ifdef USE_TCP
    if ( address->fComID == kTCPComID )
	{
	tmp = "tcp";
	tmp += kind;
	tmp += "://";
	for ( int i = 0; i < 4; i++ )
	    {
// 	    printf( "BCLEncodeAddress: %d - %u / 0x%08lX\n", i, (unsigned)( ((BCLTCPAddressStruct*)address)->fIPNr >> ((3-i)*8)) & 0xFF,
// 		    (unsigned)( ((BCLTCPAddressStruct*)address)->fIPNr >> ((3-i)*8)) & 0xFF );
	    sprintf( buf, "%u", (unsigned)( ((BCLTCPAddressStruct*)address)->fIPNr >> ((3-i)*8)) & 0xFF );
	    tmp += buf;
	    if ( i<3 )
		tmp += ".";
	    }
	tmp += ":";
	sprintf( buf, "%hu", ((BCLTCPAddressStruct*)address)->fPortNr );
	tmp += buf;
	tmp += "/";
	}
    else
#endif
#ifdef USE_UDAPL
    if ( address->fComID == kUDAPLComID )
	{
	BCLUDAPLAddressStruct *udaplAddr = (BCLUDAPLAddressStruct*)address;
	tmp = "udapl";
	tmp += kind;
	tmp += "://";
	memset( buf, 0, 33 );
	strncpy( buf, (char*)udaplAddr->fIAName, 32 );
	tmp += buf;
	tmp += ":";
	for ( int i = 0; i < 4; i++ )
	    {
// 	    printf( "BCLEncodeAddress: %d - %u / 0x%08lX\n", i, (unsigned)( ((BCLUDAPLAddressStruct*)address)->fIPNr >> ((3-i)*8)) & 0xFF,
// 		    (unsigned)( ((BCLUDAPLAddressStruct*)address)->fIPNr >> ((3-i)*8)) & 0xFF );
	    sprintf( buf, "%u", (unsigned)( ((BCLUDAPLAddressStruct*)address)->fNodeID >> ((3-i)*8)) & 0xFF );
	    tmp += buf;
	    if ( i<3 )
		tmp += ".";
	    }
	tmp += ":";
	sprintf( buf, "%u", ((BCLUDAPLAddressStruct*)address)->fConnQual );
	tmp += buf;
	tmp += "/";
	}
    else
#endif
#ifdef USE_SCI
    if ( address->fComID == kSCIComID )
	{
	tmp = "sci";
	tmp += kind;
	tmp += "://";
	sprintf( buf, "%hu", ((BCLSCIAddressStruct*)address)->fNodeID );
	tmp += buf;
	tmp += ".";
	sprintf( buf, "%hu", ((BCLSCIAddressStruct*)address)->fAdapterNr );
	tmp += buf;
	tmp += ":";
	sprintf( buf, "%hu", ((BCLSCIAddressStruct*)address)->fSegmentID );
	tmp += buf;
	tmp += "/";
	}
    else
#endif
	tmp = "unknown address";
    url = (char*)malloc( tmp.Size()+1 );
    if ( url )
	strcpy( url, tmp.c_str() );
    }


void BCLFreeEncodedAddress( char* url )
    {
    if ( url )
	free( url );
    }

int BCLGetEmptyAddressFromURL( const char* address_url, BCLAbstrAddressStruct*& address )
    {
MLUCString tmp;
    MLUCString url = address_url;
    MLUCString::TSizeType urllen = url.Length(), tmplen;;
    MLUCString::TSizeType protpos;
    protpos =  url.Find( "://" );
    if ( protpos >= urllen || protpos == MLUCString::kNoPos )
        return EINVAL;
    MLUCString prot = url.Substr( 0, protpos );
    tmplen = prot.Length();
    MLUCString tmpstr;
    tmpstr = prot.Substr( tmplen-3, tmplen-1 );
    if ( tmplen >= 4 && prot.Substr( tmplen-4, 4 ) == "blob" )
        {
        prot.Erase( tmplen-4, 4 );
        }
    else if ( tmplen >= 3 && prot.Substr( tmplen-3, 3 ) == "msg" )
        {
        prot.Erase( tmplen-3, 3 );
        }

#ifdef USE_SCI
    if ( prot == "sci" )
        {
        BCLSCIAddressStruct* sciAddr = new BCLSCIAddressStruct;
        if ( !sciAddr )
            return ENOMEM;
        BCLSCIAddress sciAddrC;
        BCLSCIAddressStruct* sciAddrTmp = sciAddrC.GetData();
        *sciAddr = *sciAddrTmp;
        sciAddr->fNodeID = 0;
        sciAddr->fAdapterNr = 0;
        sciAddr->fSegmentID = 0;
        sciAddr->fComID = kSCIComID;
        address = (BCLAbstrAddressStruct*)sciAddr;
        return 0;
        }
#endif

#ifdef USE_TCP
    if ( prot == "tcp" )
        {
        BCLTCPAddressStruct* tcpAddr = new BCLTCPAddressStruct;
        if ( !tcpAddr )
            {
            return ENOMEM;
            }
        BCLTCPAddress tcpAddrC;
        BCLTCPAddressStruct* tcpAddrTmp = tcpAddrC.GetData();
        *tcpAddr = *tcpAddrTmp;
        tcpAddr->fIPNr = INADDR_ANY;
        tcpAddr->fPortNr = (uint16)-1;
        tcpAddr->fComID = kTCPComID;
        address = (BCLAbstrAddressStruct*)tcpAddr;
        return 0;
        }
#endif
#ifdef USE_UDAPL
    if ( prot == "udapl" )
        {
        BCLUDAPLAddressStruct* udaplAddr = new BCLUDAPLAddressStruct;
        if ( !udaplAddr )
            {
            return ENOMEM;
            }
        BCLUDAPLAddress udaplAddrC;
        BCLUDAPLAddressStruct* udaplAddrTmp = udaplAddrC.GetData();
        *udaplAddr = *udaplAddrTmp;
        udaplAddr->fNodeID = INADDR_ANY;
        udaplAddr->fConnQual = (uint32)-1;
        udaplAddr->fComID = kUDAPLComID;
        address = (BCLAbstrAddressStruct*)udaplAddr;
        return 0;
        }
#endif
    return EINVAL;
    }

unsigned long BCLGetAddressSize( BCLAbstrAddressStruct* address )
    {
    if ( !address )
	return 0;
#ifdef USE_TCP
    if ( address->fComID == kTCPComID )
	return sizeof(BCLTCPAddressStruct);
#endif
#ifdef USE_UDAPL
    if ( address->fComID == kUDAPLComID )
	return sizeof(BCLUDAPLAddressStruct);
#endif
#ifdef USE_SCI
    if ( address->fComID == kSCIComID )
	return sizeof(BCLSCIAddressStruct);
#endif
    return 0;
    }

int BCLCompareAddresses( BCLAbstrAddressStruct* address1, BCLAbstrAddressStruct* address2 )
    {
    if ( !address1 && !address2 )
	return 0;
    if ( !address1 )
	return -1;
    if ( !address2 )
	return 1;
    if ( address1->fComID != address2->fComID )
	return (address1->fComID < address2->fComID) ? -1 : 1;
#ifdef USE_TCP
    if ( address1->fComID == kTCPComID )
	{
	if ( ((BCLTCPAddressStruct*)address1)->fIPNr==((BCLTCPAddressStruct*)address2)->fIPNr &&
	     ((BCLTCPAddressStruct*)address1)->fPortNr==((BCLTCPAddressStruct*)address2)->fPortNr )
	    return 0;
	if ( ((BCLTCPAddressStruct*)address1)->fIPNr!=((BCLTCPAddressStruct*)address2)->fIPNr )
	    return (((BCLTCPAddressStruct*)address1)->fIPNr<((BCLTCPAddressStruct*)address2)->fIPNr) ? -1 : 1;
	return (((BCLTCPAddressStruct*)address1)->fPortNr<((BCLTCPAddressStruct*)address2)->fPortNr) ? -1 : 1;
	}
#endif
#ifdef USE_UDAPL
    if ( address1->fComID == kUDAPLComID )
	{
	if ( !strncmp( (const char*)(((BCLUDAPLAddressStruct*)address1)->fIAName), (const char*)(((BCLUDAPLAddressStruct*)address2)->fIAName), 32 ) &&
	     ((BCLUDAPLAddressStruct*)address1)->fNodeID==((BCLUDAPLAddressStruct*)address2)->fNodeID &&
	     ((BCLUDAPLAddressStruct*)address1)->fConnQual==((BCLUDAPLAddressStruct*)address2)->fConnQual )
	    return 0;
	if ( strncmp( (const char*)(((BCLUDAPLAddressStruct*)address1)->fIAName), (const char*)(((BCLUDAPLAddressStruct*)address2)->fIAName), 32 ) )
	    return strncmp( (const char*)(((BCLUDAPLAddressStruct*)address1)->fIAName), (const char*)(((BCLUDAPLAddressStruct*)address2)->fIAName), 32 );
	if ( ((BCLUDAPLAddressStruct*)address1)->fNodeID!=((BCLUDAPLAddressStruct*)address2)->fNodeID )
	    return (((BCLUDAPLAddressStruct*)address1)->fNodeID<((BCLUDAPLAddressStruct*)address2)->fNodeID) ? -1 : 1;
	return (((BCLUDAPLAddressStruct*)address1)->fConnQual<((BCLUDAPLAddressStruct*)address2)->fConnQual) ? -1 : 1;
	}
#endif
#ifdef USE_SCI
    if ( address1->fComID == kSCIComID )
	{
	if ( ((BCLSCIAddressStruct*)address1)->fNodeID==((BCLSCIAddressStruct*)address2)->fNodeID && 
	     ((BCLSCIAddressStruct*)address1)->fAdapterNr==((BCLSCIAddressStruct*)address2)->fAdapterNr && 
	     ((BCLSCIAddressStruct*)address1)->fSegmentID==((BCLSCIAddressStruct*)address2)->fSegmentID )
	    return 0;
	if ( ((BCLSCIAddressStruct*)address1)->fNodeID==((BCLSCIAddressStruct*)address2)->fNodeID )
	    return (((BCLSCIAddressStruct*)address1)->fNodeID<((BCLSCIAddressStruct*)address2)->fNodeID) ? -1 : 1;
	if ( ((BCLSCIAddressStruct*)address1)->fAdapterNr==((BCLSCIAddressStruct*)address2)->fAdapterNr )
	    return (((BCLSCIAddressStruct*)address1)->fAdapterNr<((BCLSCIAddressStruct*)address2)->fAdapterNr) ? -1 : 1;
	return (((BCLSCIAddressStruct*)address1)->fSegmentID<((BCLSCIAddressStruct*)address2)->fSegmentID) ? -1 : 1;
	}
#endif
    return -2;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
