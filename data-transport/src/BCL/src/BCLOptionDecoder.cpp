/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLOptionDecoder.hpp"
#ifdef USE_TCP
#include "BCLTCPComID.hpp"
#include "BCLTCPAddress.hpp"
#include "BCLTCPMsgCommunication.hpp"
#include "BCLTCPBlobCommunication.hpp"
#endif
#include "MLUCString.hpp"
#include <cerrno>
#include <cstdlib>


int BCLApplyOption( const char* option, BCLCommunication* com_obj )
    {
    bool isBlob = false;
    if ( com_obj == NULL )
	return EFAULT;

    MLUCString opt = option;
    MLUCString::TSizeType typepos, parampos, optlen = opt.Length();

    typepos =  opt.Find( "." );
    if ( typepos >= optlen || typepos == MLUCString::kNoPos )
	return EINVAL;
    MLUCString type = opt.Substr( 0, typepos );
    parampos = opt.Find( "=" );
    if ( parampos >= optlen || parampos == MLUCString::kNoPos || parampos <= typepos+1 || parampos==optlen-1 )
	return EINVAL;

    MLUCString param, val;
    param = opt.Substr( typepos+1, parampos-(typepos+1) );
    val = opt.Substr( parampos+1, optlen-(parampos+1) );

    MLUCString::TSizeType tmplen = type.Length();
    if ( tmplen >= 4 && type.Substr( tmplen-4, 4 ) == "blob" )
	{
	type.Erase( tmplen-4, 4 );
	isBlob = true;
	}
    else if ( tmplen >= 3 && type.Substr( tmplen-3, 3 ) == "msg" )
	{
	type.Erase( tmplen-3, 3 );
	isBlob = false;
	}
    else
	return EINVAL;

#ifdef USE_SCI
    if ( type == "sci" )
	{
	return EINVAL;
	}
#endif

#ifdef USE_TCP
    if ( type == "tcp" )
	{
	if ( val == "transferblocksize" )
	    {
	    uint32 blockSize;
	    char* cpErr = NULL;
	    blockSize = strtoul( param.c_str(), &cpErr, 0 );
	    if ( *cpErr )
		{
		return EINVAL;
		}
	    if ( com_obj->GetComType()!=kBlobComType || com_obj->GetComID()!=kTCPComID )
		return ENOTSUP;
	    else
		((BCLTCPBlobCommunication*)com_obj)->SetTransferBlockSize( blockSize );
	    return 0;
	    }
	else
	    return EINVAL;
	}
#endif

    return EINVAL;
    }



void BCLGetAllowedOptions( vector<const char*>& options,vector<const char*>& explanations )
    {
    options.insert( options.end(), "tcpblob.transferblocksize=<size>" );
    explanations.insert( explanations.end(), "Sets the amount of bytes that a TCP Blob communication object will try to transfer in one block." );
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
