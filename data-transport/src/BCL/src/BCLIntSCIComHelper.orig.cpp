/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLIntSCIComHelper.hpp"
#include "BCLCommunication.hpp"
#include "BCLSCIAddressOps.hpp"
#include "MLUCLog.hpp"
#include <errno.h>

#define CONNECTIONTRY 4
#define INTERRUPT_WAIT busy_wait(5);


void busy_wait( unsigned long time_musec )
    {
    struct timeval t1, t2;
    unsigned long diff;
    gettimeofday( &t1, NULL );
    do
	{
	gettimeofday( &t2, NULL );
	diff = (t2.tv_usec-t1.tv_usec)+(t2.tv_sec-t1.tv_sec)*1000000;
	}
    while ( diff < time_musec );
    }



BCLIntSCIComHelper::BCLIntSCIComHelper( BCLCommunication* com )
    {
    fCom = com;
    SCIOpen( &fSCIDesc, 0, &fSCIError );
    fIsSCIOpen = true;
    if ( fSCIError != SCI_ERR_OK )
	{
	LOG( MLUCLog::kError, "BCLSCIMsgCommunication::BCLSCIMsgCommunication", "SCI device open" )
	    << "Error opening SCI device. SCI Error code: " << (uint32)fSCIError << ENDLOG;
	fIsSCIOpen = false;
	fSCIDesc = NULL;
	}
    fSegmentID = 0;
    fSegmentAddress = NULL;
    fBufferAddress = NULL;
    fControlData = NULL;
    fBufferSize = 0;
    fRequestInterrupt = NULL;
    fSegment = NULL;
    fSegmentMap = NULL;
    fConnectionTimeout = 1000; // in millisec.
#ifdef DEBUG
    fConnectionTimeout = SCI_INFINITE_TIMEOUT;
#endif
#ifdef USE_INTERRUPT_CALLBACKS
    fRequestInterruptCallback = NULL;
    fRequestInterruptData = NULL;
#endif
    }

BCLIntSCIComHelper::~BCLIntSCIComHelper()
    {
    vector<TSCIConnectionData>::iterator iter, end;
    iter = fConnections.begin();
    end = fConnections.end();
    while ( iter != end )
	{
	Disconnect( &(iter->fAddress) );
	iter++;
	}
    Unbind();
    if ( fIsSCIOpen )
	{
	SCIClose( fSCIDesc, 0, &fSCIError );
	if ( fSCIError != SCI_ERR_OK )
	    {
	    LOG( MLUCLog::kError, "BCLSCIMsgCommunication::~BCLSCIMsgCommunication", "SCI device close" )
		<< "Error closing SCI device. SCI Error code: " << (uint32)fSCIError << ENDLOG;
	    }
	}
    }

int BCLIntSCIComHelper::Bind( uint32 size, BCLSCIAddressStruct* address )
    {
    unsigned int inr;
    sci_error_t mySCIError;
    fBufferSize = size;
    fAddress = address;
    BCLIntSCIControlData scd;
    if ( !fIsSCIOpen )
	{
	LOG( MLUCLog::kError, "SCI Bind", "SCI not opened" )
	    << "SCI device not open" << ENDLOG;
	BindError( ENODEV, fCom, address, NULL );
	return ENODEV;
	}
    if ( !address )
	{
	LOG( MLUCLog::kError, "SCI Bind", "No address" )
	    << "No address (Null pointer) given" << ENDLOG;
	AddressError( EFAULT, fCom, address );
	return EFAULT;
	}
    fAddress->fNodeID = GetLocalNodeID( fAddress->fAdapterNr );
    if ( fSCIError != SCI_ERR_OK ) 
	{
	LOG( MLUCLog::kError, "SCI Bind", "Error getting local node ID" )
	    << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " while trying to get id of local node." << ENDLOG;
	BindError( ENODEV, fCom, address, NULL );
	return ENODEV;
	}
    fSegmentID = (fAddress->fNodeID << 16) | fAddress->fSegmentID;
    if ( fSegmentID == (uint32)-1 )
	{
	LOG( MLUCLog::kError, "SCI Bind", "Wrong segment id" )
	    << "Segment ID created from address node and segment id results in invalid value (0x" 
	    << MLUCLog::kHex << fSegmentID << ")." << ENDLOG;
	AddressError( EINVAL, fCom, address );
	return EINVAL;
	}

    SCICreateSegment( fSCIDesc, &fSegment, fSegmentID, ((fBufferSize+sizeof(BCLIntSCIControlDataStruct))/8+1)*8,
		      NULL, NULL, 0, &mySCIError );
    fSCIError = mySCIError;
    if ( fSCIError != SCI_ERR_OK || !fSegment )
	{
	LOG( MLUCLog::kError, "SCI Bind", "Error creating segment" )
	    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " creating SCI memory segment, id 0x" << MLUCLog::kHex << fSegmentID 
	    << MLUCLog::kDec << " of size " << ((fBufferSize+sizeof(BCLIntSCIControlDataStruct))/8+1)*8
	    << " (0x" << MLUCLog::kHex << ((fBufferSize+sizeof(BCLIntSCIControlDataStruct))/8+1)*8 << ")." << ENDLOG;
	int error;
	switch ( fSCIError )
	    {
	    case SCI_ERR_SEGMENTID_USED:
		error = EBUSY; break;
	    case SCI_ERR_SIZE_ALIGNMENT:
		error = ENOTSUP; break;
	    default:
		error = ENXIO; break;
	    }
	BindError( error, fCom, address, NULL );
	return error;
	}
    SCIPrepareSegment(fSegment, fAddress->fAdapterNr, 0, &mySCIError);
    fSCIError = mySCIError;
    if ( fSCIError != SCI_ERR_OK )
	{
	LOG( MLUCLog::kError, "SCI Bind", "Error preparing segment" )
	    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " preparing SCI memory segment, id 0x" << MLUCLog::kHex << fSegmentID 
	    << MLUCLog::kDec << " of size " << ((fBufferSize+sizeof(BCLIntSCIControlDataStruct))/8+1)*8
	    << " (0x" << MLUCLog::kHex << ((fBufferSize+sizeof(BCLIntSCIControlDataStruct))/8+1)*8 << ")." << ENDLOG;
	BindError( EIO, fCom, address, NULL );
	return EIO;
	}
    fSegmentAddress = (uint8*)SCIMapLocalSegment(fSegment, &fSegmentMap, 0, ((fBufferSize+sizeof(BCLIntSCIControlDataStruct))/8+1)*8, 
						 NULL, 0, &mySCIError);
    fSCIError = mySCIError;
    if ( fSCIError != SCI_ERR_OK )
	{
	LOG( MLUCLog::kError, "SCI Bind", "Error mapping segment" )
	    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " mapping SCI memory segment, id 0x" << MLUCLog::kHex << fSegmentID 
	    << MLUCLog::kDec << " of size " << ((fBufferSize+sizeof(BCLIntSCIControlDataStruct))/8+1)*8
	    << " (0x" << MLUCLog::kHex << ((fBufferSize+sizeof(BCLIntSCIControlDataStruct))/8+1)*8 << ")." << ENDLOG;
	Unbind();
	int error;
	switch ( fSCIError )
	    {
	    case SCI_ERR_OUT_OF_RANGE:
		error = ERANGE; break;
	    case SCI_ERR_SIZE_ALIGNMENT:
		error = ENOTSUP; break;
	    case SCI_ERR_OFFSET_ALIGNMENT:
		error = ENOTSUP; break;
	    default:
		error = ENXIO; break;
	    }
	BindError( error, fCom, address, NULL );
	return error;
	}
    fControlData = (BCLIntSCIControlDataStruct*)fSegmentAddress;
    memcpy( fControlData, scd.GetData(), scd.GetData()->fLength );
    fBufferAddress = fSegmentAddress+fControlData->fLength;
    fControlData->fRequestInterruptNr = (uint32)-1;
#ifdef USE_INTERRUPT_CALLBACKS
    SCICreateInterrupt( fSCIDesc, &fRequestInterrupt, fAddress->fAdapterNr, &inr,
			&(BCLIntSCIComHelper::IntRequestInterruptCallback), this, 0, &mySCIError );

#else
    SCICreateInterrupt( fSCIDesc, &fRequestInterrupt, fAddress->fAdapterNr, &inr,
			NULL, NULL, 0, &mySCIError );
#endif
    fSCIError = mySCIError;
    if ( fSCIError != SCI_ERR_OK || !fRequestInterrupt)
	{
	LOG( MLUCLog::kError, "SCI Bind", "Error creating request interrupt" )
	    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " creating request interrupt" << ENDLOG;
	Unbind();
	BindError( EBUSY, fCom, address,NULL );
	return EBUSY;
	}
    fControlData->fRequestInterruptNr = inr;
    fControlData->fSenderRequest = (uint32)-1;
    fControlData->fCurrentSender = (uint32)-1;
    fControlData->fReadIndex = 0;
    fControlData->fWriteIndex = 0;
    fControlData->fReadCount = 0;
    fControlData->fWriteCount = 0;
    fControlData->fSize = fBufferSize;
    SCISetSegmentAvailable( fSegment, fAddress->fAdapterNr, 0, &mySCIError );
    fSCIError = mySCIError;
    if ( fSCIError != SCI_ERR_OK )
	{
	LOG( MLUCLog::kError, "SCI Bind", "Error making segment available" )
	    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " making SCI memory segment, id 0x" << MLUCLog::kHex << fSegmentID 
	    << MLUCLog::kDec << " of size " << ((fBufferSize+sizeof(BCLIntSCIControlDataStruct))/8+1)*8
	    << " (0x" << MLUCLog::kHex << ((fBufferSize+sizeof(BCLIntSCIControlDataStruct))/8+1)*8 
	    << ") available." << ENDLOG;
	Unbind();
	BindError( EIO, fCom, address, NULL );
	return EIO;
	}
    return 0;
    }

int BCLIntSCIComHelper::Unbind()
    {
    sci_error_t mySCIError;
    if ( fSegment != NULL )
	SCISetSegmentUnavailable( fSegment, fAddress->fAdapterNr, 0, &mySCIError );
    fSCIError = mySCIError;
    if ( fSegmentAddress )
	{
	if ( fControlData )
	    {
	    if ( fControlData->fRequestInterruptNr != (uint32)-1 )
		SCIRemoveInterrupt( fRequestInterrupt, 0, &mySCIError );
	    fSCIError = mySCIError;
	    fControlData = NULL;
	    }
	if ( fSegmentMap )
	    SCIUnmapSegment( fSegmentMap, 0, &mySCIError );
	fSegmentMap = NULL;
	fSCIError = mySCIError;
	fSegmentAddress = NULL;
	fBufferAddress = NULL;
	}
    if ( fSegment )
	SCIRemoveSegment( fSegment, 0, &mySCIError );
    fSegment = NULL;
    fSCIError = mySCIError;
    return 0;
    }

int BCLIntSCIComHelper::Connect( BCLSCIAddressStruct* address, bool openOnDemand = false )
    {
    if ( !fIsSCIOpen )
	{
	LOG( MLUCLog::kError, "SCI Connect", "SCI not opened" )
	    << "SCI device not opened" << ENDLOG;
	ConnectionError( ENODEV, fCom, address );
	return ENODEV;
	}
    TSCIConnectionData connectData;
    connectData.fAddress = *address;
    connectData.fUsageCount = 0;
    int ret;
    bool found;
    ret = ConnectToRemote( address, &connectData, !openOnDemand, true, found );
    if ( ret )
	{
	LOG( MLUCLog::kError, "SCI Connect", "Connection error" )
	    << "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI connection error" << ENDLOG;
	ConnectionError( ret, fCom, address );
	return ret;
	}
    return 0;
    }

int BCLIntSCIComHelper::Disconnect( BCLSCIAddressStruct* address )
    {
    int ret;
    TSCIConnectionData connectData;
    connectData.fAddress = *address;
    connectData.fUsageCount = 0;
    bool found;
    ret = ConnectToRemote( address, &connectData, false, false, found );
    if ( found )
	{
	DisconnectFromRemote( &connectData );
	return 0;
	}
    return EINVAL;
    }

BCLIntSCIComHelper::operator bool()
    {
    return fIsSCIOpen && fSCIError==SCI_ERR_OK;
    }

uint16 BCLIntSCIComHelper::GetLocalNodeID( uint16 adapterNr )
    {
    sci_error_t mySCIError;
    struct sci_query_adapter queryAdapter;
    unsigned int myID;
    
    queryAdapter.subcommand = SCI_Q_ADAPTER_NODEID;
    queryAdapter.localAdapterNo = adapterNr;
    queryAdapter.data = &myID; 
    
    SCIQuery(SCI_Q_ADAPTER,&queryAdapter,0,&mySCIError);
    fSCIError = mySCIError;
    return (uint16)myID;
    }

int BCLIntSCIComHelper::ConnectToRemote( BCLSCIAddressStruct* address, TSCIConnectionData* parConnectData, bool now, bool store, bool& wasStored )
    {
    sci_error_t mySCIError;
    TSCIConnectionData* connectData = parConnectData;
    vector<TSCIConnectionData>::iterator iter, end;
    iter = fConnections.begin();
    end = fConnections.end();
    wasStored = false;
    while ( iter != end )
	{
	if ( iter->fAddress == *address )
	    {
	    wasStored = true;
	    if ( store )
		iter->fUsageCount++;
	    if ( !now || (now && iter->fRemoteAddr) )
		{
		*parConnectData = *iter;
		return 0;
		}
	    connectData = iter;
	    break;
	    }
	iter++;
	}
    
    connectData->fRemoteAddr = NULL;
    if ( now )
	{
	unsigned int segID;
	segID = (address->fNodeID << 16) | address->fSegmentID;
	if ( segID == (uint32)-1 )
	    {
	    return EINVAL;
	    }
	SCIOpen( &(connectData->fSCIDesc), 0, &mySCIError );
	if ( mySCIError != SCI_ERR_OK )
	    {
	    LOG( MLUCLog::kError, "BCLSCIMsgCommunication::Connect", "SCI device open" )
		<< "Error opening SCI device. SCI Error code: " << (uint32)mySCIError << ENDLOG;
	    connectData->fSCIDesc = NULL;
	    //ConnectionError( ENODEV, this, address );
	    return ENODEV;
	    }
	//connectData->fSCIDesc = fSCIDesc;
	
	uint32 firstTry = 0;
	fSCIError = SCI_ERR_OK;
	while ( firstTry<CONNECTIONTRY ) // was 2 (try 1)
	    {
	    SCIConnectSegment( connectData->fSCIDesc, &(connectData->fRemoteSegment), address->fNodeID, 
			       segID, (unsigned int)address->fAdapterNr, 
			       NULL, NULL, fConnectionTimeout, 0, &mySCIError );
	    fSCIError = mySCIError;
	    if ( fSCIError == SCI_ERR_CONNECTION_REFUSED || mySCIError == SCI_ERR_CANCELLED )
		{
		firstTry++;
		INTERRUPT_WAIT;
		}
	    else
		firstTry = CONNECTIONTRY;
	    }
	if ( fSCIError != SCI_ERR_OK )
	    {
	    LOG( MLUCLog::kError, "SCI Connect", "Error connecting segment" )
		<< "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " connecting to segment "
		<< MLUCLog::kHex << segID << "." << ENDLOG;
	    int error;
	    switch ( fSCIError )
		{
		case SCI_ERR_NO_SUCH_SEGMENT:
		    error = EINVAL; break;
		case SCI_ERR_CONNECTION_REFUSED:
		    error = EACCES; break;
		case SCI_ERR_TIMEOUT:
		    error = ETIMEDOUT; break;
		case SCI_ERR_NO_LINK_ACCESS:
		    error = EIO; break;
		case SCI_ERR_NO_REMOTE_LINK_ACCESS:
		    error = ENXIO; break;
		default:
		    error = ENXIO; break;
		}
	    //ConnectionError( error, fCom, address );
	    SCIClose( connectData->fSCIDesc, 0, &fSCIError );
	    return error;
	    }
	connectData->fRemoteAddr = (uint8*)SCIMapRemoteSegment( connectData->fRemoteSegment, &(connectData->fRemoteMap),
								0, ((sizeof( BCLIntSCIControlDataStruct )/8)+1)*8, NULL, 0, &mySCIError );
	fSCIError = mySCIError;
	if ( fSCIError != SCI_ERR_OK )
	    {
	    LOG( MLUCLog::kError, "SCI Connect", "Error mapping segment" )
		<< "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " mapping segment "
		<< MLUCLog::kHex << segID << " control data part of size " 
		<< ((sizeof( BCLIntSCIControlDataStruct )/8)+1)*8 << "." << ENDLOG;
	    int error;
	    switch ( fSCIError )
		{
		case SCI_ERR_OUT_OF_RANGE:
		    error = ERANGE; break;
		case SCI_ERR_SIZE_ALIGNMENT:
		    error = ENOTSUP; break;
		case SCI_ERR_OFFSET_ALIGNMENT:
		    error = ENOTSUP; break;
		default:
		    error = ENXIO; break;
		}
	    SCIDisconnectSegment( connectData->fRemoteSegment, 0, &mySCIError );
	    fSCIError = mySCIError;
	    //ConnectionError( error, fCom, address );
	    SCIClose( connectData->fSCIDesc, 0, &fSCIError );
	    return error;
	    }
	connectData->fRemoteCD = (BCLIntSCIControlDataStruct*)(connectData->fRemoteAddr);
	uint32 size;
	BCLIntSCIControlData scd( connectData->fRemoteCD );
	BCLIntSCIControlDataStruct *controlData = scd.GetData();
	size = controlData->fSize;
	size += sizeof(BCLIntSCIControlDataStruct);
	size = (size/8 + 1)*8;
	SCIUnmapSegment( connectData->fRemoteMap, 0, &mySCIError );
	fSCIError = mySCIError;
	
	connectData->fRemoteAddr = (uint8*)SCIMapRemoteSegment( connectData->fRemoteSegment, &(connectData->fRemoteMap),
								0, size, NULL, 0, &mySCIError );
	fSCIError = mySCIError;
	if ( fSCIError != SCI_ERR_OK )
	    {
	    LOG( MLUCLog::kError, "SCI Connect", "Error mapping segment" )
		<< "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " mapping segment "
		<< MLUCLog::kHex << segID << " of size " << MLUCLog::kDec << size << " (0x" << MLUCLog::kHex
		<< size << ") completely." << ENDLOG;
	    int error;
	    switch ( fSCIError )
		{
		case SCI_ERR_OUT_OF_RANGE:
		    error = ERANGE; break;
		case SCI_ERR_SIZE_ALIGNMENT:
		    error = ENOTSUP; break;
		case SCI_ERR_OFFSET_ALIGNMENT:
		    error = ENOTSUP; break;
		default:
		    error = ENXIO; break;
		}
	    SCIDisconnectSegment( connectData->fRemoteSegment, 0, &mySCIError );
	    //ConnectionError( error, fCom, address );
	    SCIClose( connectData->fSCIDesc, 0, &fSCIError );
	    return error;
	    }
	connectData->fRemoteCD = (BCLIntSCIControlDataStruct*)(connectData->fRemoteAddr);
	connectData->fRemoteMsgs = connectData->fRemoteAddr + sizeof( BCLIntSCIControlDataStruct);
	unsigned int inr;
	inr = controlData->fRequestInterruptNr;
	firstTry = 0;
	while ( firstTry<CONNECTIONTRY ) // was 2
	    {
	    SCIConnectInterrupt( connectData->fSCIDesc, &(connectData->fRemoteRequestInt), address->fNodeID,
				 address->fAdapterNr, inr, fConnectionTimeout, 0, &mySCIError );
	    fSCIError = mySCIError;
	    if ( fSCIError == SCI_ERR_CONNECTION_REFUSED || mySCIError == SCI_ERR_CANCELLED )
		{
		firstTry++;
		INTERRUPT_WAIT;
		}
	    else
		firstTry = CONNECTIONTRY;
	    }
	if ( fSCIError != SCI_ERR_OK )
	    {
	    LOG( MLUCLog::kError, "SCI Connect", "Error connecting request interrupt" )
		<< "Segment 0x" << MLUCLog::kHex << fSegmentID << ": " << "SCI error 0x" << MLUCLog::kHex << (long unsigned int)fSCIError << MLUCLog::kDec << " connecting remote request interrupt "
		<< MLUCLog::kHex << inr << "." << ENDLOG;
	    int error;
	    switch ( fSCIError )
		{
		case SCI_ERR_CONNECTION_REFUSED:
		    error = EACCES; break;
		    // 	    case SCI_ERR_NO_SUCH_INTNO:
		    // 		error = ENOTSUP; break;
		case SCI_ERR_TIMEOUT:
		    error = ETIMEDOUT; break;
		default:
		    error = ENXIO; break;
		}
	    SCIUnmapSegment( connectData->fRemoteMap, 0, &mySCIError );
	    SCIDisconnectSegment( connectData->fRemoteSegment, 0, &mySCIError );
	    //ConnectionError( error, fCom, address );
	    SCIClose( connectData->fSCIDesc, 0, &fSCIError );
	    return error;
	    }
	//connectData->fAddress = *address;
	}
    if ( store && connectData==parConnectData )
	{
	parConnectData->fUsageCount = 1;
	fConnections.insert( fConnections.end(), *parConnectData );
	}
    if ( connectData != parConnectData )
	*parConnectData = *connectData;
    return 0;
    }

void BCLIntSCIComHelper::DisconnectFromRemote( TSCIConnectionData* connectData )
    {
    sci_error_t mySCIError;
    vector<TSCIConnectionData>::iterator iter, end;
    iter = fConnections.begin();
    end = fConnections.end();
    while ( iter != end )
	{
	if ( iter->fAddress == connectData->fAddress )
	    {
	    iter->fUsageCount--;
	    if ( iter->fUsageCount <= 0 )
		{
		SCIDisconnectInterrupt( iter->fRemoteRequestInt, 0, &mySCIError );
		SCIUnmapSegment( iter->fRemoteMap, 0, &mySCIError );
		SCIDisconnectSegment( iter->fRemoteSegment, 0, &mySCIError );
		SCIClose( iter->fSCIDesc, 0, &fSCIError );
		fConnections.erase( iter );
		}
	    return;
	    }
	iter++;
	}
    return;
    }

#ifdef USE_INTERRUPT_CALLBACKS
void BCLIntSCIComHelper::SetRequestCallback( void (*reqCB)( void*), void* cbArg )
    {
    fRequestInterruptCallback = reqCB;
    fRequestInterruptData = cbArg;
    }

sci_callback_action_t BCLIntSCIComHelper::IntRequestInterruptCallback( void* arg, sci_local_interrupt_t interrupt, sci_error_t status )
    {
    if ( ((BCLIntSCIComHelper*)arg)->fRequestInterruptCallback )
	(*(((BCLIntSCIComHelper*)arg)->fRequestInterruptCallback))( ((BCLIntSCIComHelper*)arg)->fRequestInterruptData );
    return SCI_CALLBACK_CONTINUE;
    }
#endif


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
