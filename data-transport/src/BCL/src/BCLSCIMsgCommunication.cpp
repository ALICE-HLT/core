/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#ifdef USE_SCI

#include "BCLSCIMsgCommunication.hpp"
#include "BCLSCIAddressOps.hpp"
#include "BCLMessage.hpp"
#include "BCLIntSCIControlData.hpp"
#include "MLUCLog.hpp"
#include <errno.h>
#include <stdlib.h>
#include <memory.h>
#include <sys/time.h>
#include <unistd.h>


#define INTERRUPT_WAIT busy_wait(2);
#ifdef SCI_SIMPLE_TIMEOUT
#define SIMPLE_TIMEOUT_MAX 1000000
#endif

#define CONNECTIONTRY 4

void busy_wait( unsigned long time_musec );





BCLSCIMsgCommunication::BCLSCIMsgCommunication( uint32 bufferSize ):
    fComHelper( this )
#ifndef USE_INTERRUPT_CALLBACKS
#ifndef DEBUG_NOTHREADS
								   ,
    //#if __GNUC__>=3
    fRequestIntThread( this, &BCLSCIMsgCommunication::RequestInterruptHandler )
    //fRequestIntThread( this, &(BCLSCIMsgCommunication::RequestInterruptHandler) )
#endif
#endif
    {
    fMsgBufferSize = bufferSize;
    fMsgBufferSize = (fMsgBufferSize/4)*4;
    if ( !fMsgBufferSize )
	fMsgBufferSize = kDefaultMsgBufferSize;
    fMsgBufferAddress = NULL;
    fControlData = NULL;
    fPreStatusOk = false;
#ifdef USE_INTERRUPT_CALLBACKS
    fComHelper.SetRequestCallback( &(BCLSCIMsgCommunication::RequestInterruptHandler), this );
    pthread_mutex_init( &fRequestMutex, NULL );
#endif
    fQuitRequestInterruptHandler = false;
    fRequestInterruptHandlerQuitted = true;
    fDataArrivedSignal.Unlock();
    fPauseRequests = false;
    fMsgSeqNr = 0;
    }

BCLSCIMsgCommunication::~BCLSCIMsgCommunication()
    {
#ifndef USE_INTERRUPT_CALLBACKS
#ifndef DEBUG_NOTHREADS
    //fRequestIntThread.Abort();
#endif
#else
    pthread_mutex_destroy( &fRequestMutex );
#endif
    Unbind();
    }

int BCLSCIMsgCommunication::Bind( BCLAbstrAddressStruct* address )
    {
    int ret;
    if ( !address )
	{
	LOG( MLUCLog::kError, "SCI Bind", "Address error" )
	    << "Wrong address (NULL pointer) in SCI Bind" << ENDLOG;
	BindError( EFAULT, this, address, NULL );
	return EFAULT;
	}
    if ( address->fComID != kSCIComID )
	{
	LOG( MLUCLog::kError, "SCI Bind", "Address error" )
	    << "Wrong address ID " << address->fComID << " in SCI Bind. (expected " 
	    << kSCIComID << ")."<< ENDLOG;
	BindError( EINVAL, this, address, NULL );
	return EINVAL;
	}
    fSCIAddress = *(BCLSCIAddressStruct*)address;
    fAddress = &fSCIAddress;
    ret = fComHelper.Bind( fMsgBufferSize, (BCLSCIAddressStruct*)fAddress );
    if ( !ret )
	{
	fMsgBufferAddress = fComHelper.GetDataBuffer();
	fControlData = fComHelper.GetControlData();
#ifndef USE_INTERRUPT_CALLBACKS
#ifndef DEBUG_NOTHREADS
	fRequestIntThread.Start();
#endif
#endif
	}
    return ret;
    }

int BCLSCIMsgCommunication::Unbind()
    {
#ifndef USE_INTERRUPT_CALLBACKS
#ifndef DEBUG_NOTHREADS
    //fRequestIntThread.Abort();
    QuitRequestInterruptHandler();
#endif
#endif
    fMsgBufferAddress = NULL;
    fControlData = NULL;
    return fComHelper.Unbind();
    }

int BCLSCIMsgCommunication::Connect( BCLAbstrAddressStruct* address, bool openOnDemand )
    {
    if ( !address )
	{
	LOG( MLUCLog::kError, "SCI Connect", "Address error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address (NULL pointer) in SCI Connect" << ENDLOG;
	ConnectionError( EFAULT, this, address );
	return EFAULT;
	}
    if ( address->fComID != kSCIComID )
	{
	LOG( MLUCLog::kError, "SCI Connect", "Address error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address ID " << address->fComID << " in SCI Connect. (expected " 
	    << kSCIComID << ")."<< ENDLOG;
	ConnectionError( EINVAL, this, address );
	return EINVAL;
	}
    return fComHelper.Connect( (BCLSCIAddressStruct*)address, NULL, false, 0, openOnDemand );
    }

int BCLSCIMsgCommunication::Connect( BCLAbstrAddressStruct* address, unsigned long timeout_ms, bool openOnDemand )
    {
    if ( !address )
	{
	LOG( MLUCLog::kError, "SCI Connect", "Address error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address (NULL pointer) in SCI Connect" << ENDLOG;
	ConnectionError( EFAULT, this, address );
	return EFAULT;
	}
    if ( address->fComID != kSCIComID )
	{
	LOG( MLUCLog::kError, "SCI Connect", "Address error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address ID " << address->fComID << " in SCI Connect. (expected " 
	    << kSCIComID << ")."<< ENDLOG;
	ConnectionError( EINVAL, this, address );
	return EINVAL;
	}
    return fComHelper.Connect( (BCLSCIAddressStruct*)address, NULL, true, timeout_ms, openOnDemand );
    }

int BCLSCIMsgCommunication::Disconnect( BCLAbstrAddressStruct* address )
    {
    if ( !address )
	{
	LOG( MLUCLog::kError, "SCI Disconnect", "Address error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address (NULL pointer) in SCI Disconnect" << ENDLOG;
	ConnectionError( EFAULT, this, address );
	return EFAULT;
	}
    if ( address->fComID != kSCIComID )
	{
	LOG( MLUCLog::kError, "SCI Disconnect", "Address error" )
	    << "Wrong address ID " << address->fComID << " in SCI Disconnect. (expected " 
	    << kSCIComID << ")."<< ENDLOG;
	ConnectionError( EINVAL, this, address );
	return EINVAL;
	}
    return fComHelper.Disconnect( (BCLSCIAddressStruct*)address, NULL, false, 0 );
    }

int BCLSCIMsgCommunication::Disconnect( BCLAbstrAddressStruct* address, unsigned long timeout_ms )
    {
    if ( !address )
	{
	LOG( MLUCLog::kError, "SCI Disconnect", "Address error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address (NULL pointer) in SCI Disconnect" << ENDLOG;
	ConnectionError( EFAULT, this, address );
	return EFAULT;
	}
    if ( address->fComID != kSCIComID )
	{
	LOG( MLUCLog::kError, "SCI Disconnect", "Address error" )
	    << "Wrong address ID " << address->fComID << " in SCI Disconnect. (expected " 
	    << kSCIComID << ")."<< ENDLOG;
	ConnectionError( EINVAL, this, address );
	return EINVAL;
	}
    return fComHelper.Disconnect( (BCLSCIAddressStruct*)address, NULL, true, timeout_ms );
    }

bool BCLSCIMsgCommunication::CanLockConnection()
    {
    return true;
    }

//int BCLSCIMsgCommunication::LockConnection( BCLAbstrAddressStruct* address, BCLErrorCallback* callback = NULL )
int BCLSCIMsgCommunication::LockConnection( BCLAbstrAddressStruct* address )
    {
    BCLErrorCallback* callback = NULL;
    if ( !(bool)fComHelper )
	{
	LOG( MLUCLog::kError, "SCI Lock", "Communication helper error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Error with communication helper object" << ENDLOG;
	LockError( ENODEV, this, address, callback );
	return ENODEV;
	}
    if ( !address )
	{
	if ( !address )
	    {
	    LOG( MLUCLog::kError, "SCI Lock", "Address error" )
		<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address (NULL pointer) in SCI Lock" << ENDLOG;
	    }
	LockError( EFAULT, this, address, callback );
	return EFAULT;
	}
    if ( address->fComID != kSCIComID )
	{
	LOG( MLUCLog::kError, "SCI Lock", "Address error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address ID " << address->fComID << " in SCI Lock. (expected " 
	    << kSCIComID << ")."<< ENDLOG;
	LockError( EINVAL, this, address, callback );
	return EINVAL;
	}
    if ( address->fLength != sizeof(BCLSCIAddressStruct) )
	{
	LOG( MLUCLog::kError, "SCI Lock", "Address error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address size " << MLUCLog::kDec << address->fLength 
	    << " (expected " << sizeof(BCLSCIAddressStruct) << ") in SCI Lock." << ENDLOG;
	AddressError( EMSGSIZE, this, address, callback );
	return EMSGSIZE;
	}
    return fComHelper.LockConnection( (BCLSCIAddressStruct*)address, callback, false, 0 );
    }

int BCLSCIMsgCommunication::LockConnection( BCLAbstrAddressStruct* address, unsigned long timeout_ms )
    {
    BCLErrorCallback* callback = NULL;
    if ( !(bool)fComHelper )
	{
	LOG( MLUCLog::kError, "SCI Lock", "Communication helper error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Error with communication helper object" << ENDLOG;
	LockError( ENODEV, this, address, callback );
	return ENODEV;
	}
    if ( !address )
	{
	if ( !address )
	    {
	    LOG( MLUCLog::kError, "SCI Lock", "Address error" )
		<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address (NULL pointer) in SCI Lock" << ENDLOG;
	    }
	LockError( EFAULT, this, address, callback );
	return EFAULT;
	}
    if ( address->fComID != kSCIComID )
	{
	LOG( MLUCLog::kError, "SCI Lock", "Address error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address ID " << address->fComID << " in SCI Lock. (expected " 
	    << kSCIComID << ")."<< ENDLOG;
	LockError( EINVAL, this, address, callback );
	return EINVAL;
	}
    if ( address->fLength != sizeof(BCLSCIAddressStruct) )
	{
	LOG( MLUCLog::kError, "SCI Lock", "Address error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address size " << MLUCLog::kDec << address->fLength 
	    << " (expected " << sizeof(BCLSCIAddressStruct) << ") in SCI Lock." << ENDLOG;
	AddressError( EMSGSIZE, this, address, callback );
	return EMSGSIZE;
	}
    return fComHelper.LockConnection( (BCLSCIAddressStruct*)address, callback, true, timeout_ms );
    }

//int BCLSCIMsgCommunication::UnlockConnection( BCLAbstrAddressStruct* address, BCLErrorCallback* callback = NULL )
int BCLSCIMsgCommunication::UnlockConnection( BCLAbstrAddressStruct* address )
    {
    BCLErrorCallback* callback = NULL;
    if ( !(bool)fComHelper )
	{
	LOG( MLUCLog::kError, "SCI Unlock", "Communication helper error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Error with communication helper object" << ENDLOG;
	LockError( ENODEV, this, address, callback );
	return ENODEV;
	}
    if ( !address )
	{
	if ( !address )
	    {
	    LOG( MLUCLog::kError, "SCI Unlock", "Address error" )
		<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address (NULL pointer) in SCI Unlock" << ENDLOG;
	    }
	LockError( EFAULT, this, address, callback );
	return EFAULT;
	}
    if ( address->fComID != kSCIComID )
	{
	LOG( MLUCLog::kError, "SCI Unlock", "Address error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address ID " << address->fComID << " in SCI Unlock. (expected " 
	    << kSCIComID << ")."<< ENDLOG;
	LockError( EINVAL, this, address, callback );
	return EINVAL;
	}
    if ( address->fLength != sizeof(BCLSCIAddressStruct) )
	{
	LOG( MLUCLog::kError, "SCI Unlock", "Address error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address size " << MLUCLog::kDec << address->fLength 
	    << " (expected " << sizeof(BCLSCIAddressStruct) << ") in SCI Unlock." << ENDLOG;
	AddressError( EMSGSIZE, this, address, callback );
	return EMSGSIZE;
	}
    return fComHelper.UnlockConnection( (BCLSCIAddressStruct*)address, callback, false, 0 );
    }

int BCLSCIMsgCommunication::UnlockConnection( BCLAbstrAddressStruct* address, unsigned long timeout_ms )
    {
    BCLErrorCallback* callback = NULL;
    if ( !(bool)fComHelper )
	{
	LOG( MLUCLog::kError, "SCI Unlock", "Communication helper error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Error with communication helper object" << ENDLOG;
	LockError( ENODEV, this, address, callback );
	return ENODEV;
	}
    if ( !address )
	{
	if ( !address )
	    {
	    LOG( MLUCLog::kError, "SCI Unlock", "Address error" )
		<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address (NULL pointer) in SCI Unlock" << ENDLOG;
	    }
	LockError( EFAULT, this, address, callback );
	return EFAULT;
	}
    if ( address->fComID != kSCIComID )
	{
	LOG( MLUCLog::kError, "SCI Unlock", "Address error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address ID " << address->fComID << " in SCI Unlock. (expected " 
	    << kSCIComID << ")."<< ENDLOG;
	LockError( EINVAL, this, address, callback );
	return EINVAL;
	}
    if ( address->fLength != sizeof(BCLSCIAddressStruct) )
	{
	LOG( MLUCLog::kError, "SCI Unlock", "Address error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address size " << MLUCLog::kDec << address->fLength 
	    << " (expected " << sizeof(BCLSCIAddressStruct) << ") in SCI Unlock." << ENDLOG;
	AddressError( EMSGSIZE, this, address, callback );
	return EMSGSIZE;
	}
    return fComHelper.UnlockConnection( (BCLSCIAddressStruct*)address, callback, true, timeout_ms );
    }


int BCLSCIMsgCommunication::Send( BCLAbstrAddressStruct* address, BCLMessageStruct* msg, 
				  BCLErrorCallback* callback )
    {
    return Send( address, msg, false, 0, callback );
    }

int BCLSCIMsgCommunication::Send( BCLAbstrAddressStruct* address, BCLMessageStruct* msg, 
				  unsigned long timeout_ms, BCLErrorCallback* callback )
    {
    return Send( address, msg, true, timeout_ms, callback );
    }

int BCLSCIMsgCommunication::Receive( BCLAbstrAddressStruct* addr, BCLMessageStruct*& msg, BCLErrorCallback* callback )
    {
    return Receive( (BCLSCIAddressStruct*)addr, msg, false, 0, callback );
    }

int BCLSCIMsgCommunication::Receive( BCLAbstrAddressStruct* addr, BCLMessageStruct*& msg, unsigned long timeout, BCLErrorCallback* callback )
    {
    return Receive( (BCLSCIAddressStruct*)addr, msg, true, timeout, callback );
    }

void BCLSCIMsgCommunication::Reset()
    {
    fPauseRequests = true;
    while ( fControlData->fCurrentSender != (uint32)-1 )
	usleep( 0 );
    fControlData->fReadIndex = fControlData->fWriteIndex;
    fPauseRequests = false;
    }

int BCLSCIMsgCommunication::ReleaseMsg( BCLMessageStruct* msg )
    {
    FreeMessage( msg );
    return 0;
    }

uint32 BCLSCIMsgCommunication::GetAddressLength()
    {
    return sizeof(BCLSCIAddressStruct);
    }

uint32 BCLSCIMsgCommunication::GetComID()
    {
    return kSCIComID;
    }

BCLSCIAddressStruct* BCLSCIMsgCommunication::NewAddress()
    {
    BCLSCIAddressStruct* addr = new BCLSCIAddressStruct;
    if ( !addr )
	return NULL;
    BCLSCIAddress addrClass;
    *addr = *(addrClass.GetData());
    addr->fComID = kSCIComID;
    return addr;
    }

void BCLSCIMsgCommunication::DeleteAddress( BCLAbstrAddressStruct* address )
    {
    delete (BCLSCIAddressStruct*)address;
    }

bool BCLSCIMsgCommunication::AddressEquality( const BCLAbstrAddressStruct* addr1, const BCLAbstrAddressStruct* addr2 )
    {
    if ( addr1->fComID != kSCIComID || addr2->fComID != kSCIComID )
	return false;
    return *(BCLSCIAddressStruct*)addr1 == *(BCLSCIAddressStruct*)addr2;
    }

BCLSCIMsgCommunication::operator bool()
    {
    return fSCIError==SCI_ERR_OK && (bool)fComHelper;
    }

BCLMessageStruct* BCLSCIMsgCommunication::NewMessage( uint32 size )
    {
    return (BCLMessageStruct*)malloc( size );
    //return (BCLMessageStruct*)new uint8[ size ];
    }

void BCLSCIMsgCommunication::FreeMessage( BCLMessageStruct* msg )
    {
    free( msg );
    //delete [] (uint8*)msg;
    }

int BCLSCIMsgCommunication::Send( BCLAbstrAddressStruct* address, BCLMessageStruct* msg, 
				  bool useTimeout, unsigned long timeout_ms, BCLErrorCallback* callback )
    {
    int retval = 0;
    sci_error_t mySCIError;
    if ( !(bool)fComHelper )
	{
	LOG( MLUCLog::kError, "SCI Send", "Communication helper error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Error with communication helper object" << ENDLOG;
	MsgSendError( ENODEV, this, address, msg, callback );
	return ENODEV;
	}
    if ( !address || !msg )
	{
	if ( !address )
	    {
	    LOG( MLUCLog::kError, "SCI Send", "Address error" )
		<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address (NULL pointer) in SCI Send" << ENDLOG;
	    }
	else
	    {
	    LOG( MLUCLog::kError, "SCI Send", "Message error" )
		<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong message (NULL pointer) in SCI Send" << ENDLOG;
	    }
	MsgSendError( EFAULT, this, address, msg, callback );
	return EFAULT;
	}
    if ( address->fComID != kSCIComID )
	{
	LOG( MLUCLog::kError, "SCI Send", "Address error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address ID " << address->fComID << " in SCI Send. (expected " 
	    << kSCIComID << ")."<< ENDLOG;
	MsgSendError( EINVAL, this, address, msg, callback );
	return EINVAL;
	}
    if ( address->fLength != sizeof(BCLSCIAddressStruct) )
	{
	LOG( MLUCLog::kError, "SCI Send", "Address error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address size " << MLUCLog::kDec << address->fLength 
	    << " (expected " << sizeof(BCLSCIAddressStruct) << ") in SCI Send." << ENDLOG;
	AddressError( EMSGSIZE, this, address, callback );
	return EMSGSIZE;
	}
    if ( msg->fLength < sizeof(BCLMessageStruct) )
	{
	LOG( MLUCLog::kError, "SCI Send", "Message error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong message size " << MLUCLog::kDec << msg->fLength 
	    << " (expected " << sizeof(BCLMessageStruct) << ") in SCI Send." << ENDLOG;
	MsgSendError( EMSGSIZE, this, address, msg, callback );
	return EMSGSIZE;
	}
    // Make sure that possible SEGVs don't occur while the other side is waiting 
    // for our interrupt...
    uint8 *tmp, b;
    tmp = (uint8*)msg;
    b = *tmp;
    b = *(tmp+msg->fLength);
    uint32 tmpMsgID;
    BCLNetworkData::Transform( fMsgSeqNr, tmpMsgID, kNativeByteOrder, msg->fDataFormats[kCurrentByteOrderIndex] );
    fMsgSeqNr++;
    msg->fMsgID = tmpMsgID;

    bool found;
    BCLIntSCIComHelper::TSCIConnectionData condat;
    BCLIntSCIComHelper::TSCIConnectionData* connectData = &condat;
    condat.fAddress = *(BCLSCIAddressStruct*)address;
    int ret;
    ret = fComHelper.ConnectToRemote( (BCLSCIAddressStruct*)address, connectData, true, false, found, NULL, useTimeout, timeout_ms );
    if ( ret )
	{
	LOG( MLUCLog::kError, "SCI Send", "Connection error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Error building connection to remote address " << *(BCLSCIAddressStruct*)address
	    << " in SCI Send: " << strerror( ret ) << " (" << ret << ")." << ENDLOG;
	MsgSendError( ret, this, address, msg, callback );
	return ret;
	}


    //SCIFlushReadBuffers( connectData->fMapSequence );

    // Really do the send now.
    BCLIntSCIControlDataStruct* controlData;
    if ( connectData->fLocked )
	controlData = &(connectData->fRemoteCachedCD);
    else
	controlData = connectData->fRemoteCD;
    BCLIntSCIControlData scd( controlData );

    controlData = scd.GetData();
    uint32 myTransID;
    uint8 remoteByteOrder = scd.GetOriginalByteOrder();
    BCLNetworkData::Transform( fComHelper.GetSegmentID(), myTransID, kNativeByteOrder, remoteByteOrder );


#if defined(SCI_SIMPLE_TIMEOUT) && !defined(NO_SIMPLE_SEND_TEST)
    unsigned long timeoutCount=0;
#endif

    retval = 0;
    if ( !connectData->fLocked || controlData->fCurrentSender != fComHelper.GetSegmentID() )
	{
	ret = fComHelper.LockConnection( (BCLSCIAddressStruct*)address, connectData, callback, useTimeout, timeout_ms );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "SCI Send", "Locking error" )
		<< "Error locking SCI segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID 
		<< " for exclusive write: " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." 
		<< ENDLOG;
	    retval = ret;
	    }
	}
	
    uint32 freeBuffer = 0;
    if ( !retval )
	{
	int pass = 0;
	do
	    {

	    if ( controlData->fReadIndex < controlData->fWriteIndex )
		freeBuffer = controlData->fSize - (controlData->fWriteIndex - controlData->fReadIndex) - 1;
	    if ( controlData->fReadIndex > controlData->fWriteIndex )
		freeBuffer = controlData->fReadIndex - controlData->fWriteIndex - 1;
	    if ( controlData->fReadIndex == controlData->fWriteIndex )
		freeBuffer = controlData->fSize - 1; 	// buffer is empty

	
	    if ( pass==0 && connectData->fLocked && freeBuffer < sizeof(BCLSCIAddressStruct)+msg->fLength )
		{
		ret = fComHelper.UpdateCachedCD( (BCLSCIAddressStruct*)address, &(connectData->fRemoteCachedCD), useTimeout, timeout_ms );
		if ( ret )
		    {
		    LOG( MLUCLog::kError, "SCI Send", "Internal Error" )
			<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << 
			" Internal error updating remote cached control data from remote address " << *(BCLSCIAddressStruct*)address
			<< ": " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
		    
		    retval = ENXIO;
		    MsgSendError( retval, this, address, msg, callback );
		    pass = 2;
		    }
		else
		    {
		    scd.SetTo( &(connectData->fRemoteCachedCD) );
		    controlData = scd.GetData();
		    pass = 1;
		    }
		}
	    else
		pass = 2;
	    }
	while ( pass != 2 );
	}
    
    if ( !retval && freeBuffer < sizeof(BCLSCIAddressStruct)+msg->fLength )
	{
	LOG( MLUCLog::kError, "SCI Send", "Out of buffer" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Not enough buffer space while sending (" << MLUCLog::kDec
	    << freeBuffer << " available, " << sizeof(BCLSCIAddressStruct)+msg->fLength
	    << " needed)." << ENDLOG;
	MsgSendError( ENOSPC, this, address, msg, callback );
	retval = ENOSPC;
	}
    else if ( !retval )
	{ // Do the write
	// We have enough buffer space left to proceed.
	// Start loop around sequence checking to see wether we succeeded or have to retry or abort.
	do
	    {
	    ret = fComHelper.StartSequence( connectData, useTimeout, timeout_ms );
	    if ( ret )
		{
		LOG( MLUCLog::kError, "SCI Send", "Start sequence error" )
		    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Error starting map sequence for connection to remote address " << *(BCLSCIAddressStruct*)address
		    << " in SCI Send: " << strerror( ret ) << " (" << ret << ")." << ENDLOG;
		MsgSendError( ret, this, address, msg, callback );
		return ret;
		}
	    
	    if ( controlData->fWriteIndex+sizeof(BCLSCIAddressStruct) <= controlData->fSize )
		{
		// no wrap
		memcpy( connectData->fRemoteMsgs+controlData->fWriteIndex, fAddress, sizeof(BCLSCIAddressStruct) );

		controlData->fWriteIndex += sizeof(BCLSCIAddressStruct);
		if ( controlData->fWriteIndex >= controlData->fSize )
		    controlData->fWriteIndex -= controlData->fSize;
		}    
	    else
		{
		// wrap address
		uint32 tmp = 0;
		tmp = controlData->fWriteIndex;
		memcpy( connectData->fRemoteMsgs+controlData->fWriteIndex, fAddress, controlData->fSize-controlData->fWriteIndex );
		memcpy( connectData->fRemoteMsgs, ((uint8*)fAddress)+(controlData->fSize-controlData->fWriteIndex), 
			sizeof(BCLSCIAddressStruct)-(controlData->fSize-controlData->fWriteIndex) );

		controlData->fWriteIndex = sizeof(BCLSCIAddressStruct)-(controlData->fSize-controlData->fWriteIndex);
		}
	
	    if ( controlData->fWriteIndex+msg->fLength <= controlData->fSize )
		{
		// Don't wrap message 
		memcpy( connectData->fRemoteMsgs+controlData->fWriteIndex, msg, msg->fLength );

		controlData->fWriteIndex += msg->fLength;
		if ( controlData->fWriteIndex >= controlData->fSize )
		    controlData->fWriteIndex -= controlData->fSize;

	    
		}
	    else
		{
		// Wrap message.
		memcpy( connectData->fRemoteMsgs+controlData->fWriteIndex, msg, controlData->fSize-controlData->fWriteIndex );
		memcpy( connectData->fRemoteMsgs, ((uint8*)msg)+(controlData->fSize-controlData->fWriteIndex), 
			msg->fLength-(controlData->fSize-controlData->fWriteIndex) );

		controlData->fWriteIndex = msg->fLength-(controlData->fSize-controlData->fWriteIndex );
		}

	    ret = fComHelper.CheckSequence( connectData, useTimeout, timeout_ms );
	    if ( ret==EAGAIN )
		{
		LOG( MLUCLog::kWarning, "SCI Send", "CheckSequence Retry 1" ) // LOG( MLUCLog::kDebug, "SCI Send", "CurrentSenderDone updated :)" )
		    << "CheckSequence Retry 1" << ENDLOG;
		}
	    }
	while ( ret == EAGAIN );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "SCI Send", "Send Error" )
		<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Error writing message to remote address " << *(BCLSCIAddressStruct*)address
		<< " in SCI Send: " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	    MsgSendError( ret, this, address, msg, callback );
	    return ret;
	    }

	
	scd.TransformToOriginal();
	if ( controlData->fWriteIndex == controlData->fReadIndex )
	    {
	    LOG( MLUCLog::kError, "SCI Send", "awi: Write Index == Read Index after writing" )
		<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ENDLOG;
	    }
	if ( connectData->fLocked )
	    connectData->fRemoteCachedCD.fWriteIndex = controlData->fWriteIndex;
	fComHelper.WriteBackCachedCD( (BCLSCIAddressStruct*)address, &(connectData->fRemoteCachedCD) );
	do
	    {
	    ret = fComHelper.StartSequence( connectData, useTimeout, timeout_ms );
	    if ( ret )
		{
		LOG( MLUCLog::kError, "SCI Send", "Start sequence error" )
		    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Error starting map sequence for connection to remote address " << *(BCLSCIAddressStruct*)address
		    << " in SCI Send: " << strerror( ret ) << " (" << ret << ")." << ENDLOG;
		MsgSendError( ret, this, address, msg, callback );
		return ret;
		}
	    connectData->fRemoteCD->fWriteIndex = controlData->fWriteIndex;
	    ret = fComHelper.CheckSequence( connectData, useTimeout, timeout_ms );
	    if ( ret==EAGAIN )
		{
		LOG( MLUCLog::kWarning, "SCI Send", "CheckSequence Retry 2" ) // LOG( MLUCLog::kDebug, "SCI Send", "CurrentSenderDone updated :)" )
		    << "CheckSequence Retry 2" << ENDLOG;
		}
	    }
	while ( ret == EAGAIN );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "SCI Send", "Send Error" )
		<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Error writing back new write index to remote address " << *(BCLSCIAddressStruct*)address
		<< " in SCI Send: " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	    MsgSendError( ret, this, address, msg, callback );
	    return ret;
	    }



	if ( !connectData->fLocked )
	    {
	    ret = fComHelper.UnlockConnection( (BCLSCIAddressStruct*)address, connectData, callback, useTimeout, timeout_ms );
	    if ( ret )
		{
		LOG( MLUCLog::kError, "SCI Send", "Unlocking error" )
		    << "Error unlocking SCI segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID 
		    << " from exclusive write: " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." 
		    << ENDLOG;
		}
	    }
	
#ifndef _SCALI_INTERRUPT_BUGFIX_
	SCITriggerInterrupt( connectData->fRemoteRequestInt, 0, &mySCIError );
#else
	mySCIError = SCI_ERR_OK;
#endif
	}

#if 0 // Unneccessary, functionality already in CheckSequence.
    SCIStoreBarrier( connectData->fMapSequence, 0 );
#endif
	
    if ( !found )
	fComHelper.DisconnectFromRemote( connectData, callback, useTimeout, timeout_ms );
    LOG( MLUCLog::kDebug, "SCI Send", "Sending" )
	<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Sender 0x" << MLUCLog::kHex << myTransID << " has finished sendint to address" << *((BCLSCIAddressStruct*)address) << "" << ENDLOG;
 
    return retval;
    }

int BCLSCIMsgCommunication::Receive( BCLSCIAddressStruct* address, BCLMessageStruct*& msg, bool useTimeout, unsigned long timeout, BCLErrorCallback* callback )
    {
    uint32 tmpRNdx;

    msg =NULL;
    if ( !(bool)fComHelper )
	{
	LOG( MLUCLog::kError, "SCI Receive", "Communication helper error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Error with communication helper object" << ENDLOG;
	MsgReceiveError( ENODEV, this, address, msg, callback );
	return ENODEV;
	}
    if ( !address )
	{
	LOG( MLUCLog::kError, "SCI Receive", "Address error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address (NULL pointer) in SCI Receive" << ENDLOG;
	MsgReceiveError( EFAULT, this, address, msg, callback );
	return EFAULT;
	}
    uint32 st;
    BCLNetworkData::Transform( address->fLength, st, address->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );    
    if ( st != sizeof(BCLSCIAddressStruct) )
	{
	LOG( MLUCLog::kError, "SCI Receive", "Address error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address size " << MLUCLog::kDec << address->fLength 
	    << " (expected " << sizeof(BCLSCIAddressStruct) << ") in SCI Receive." << ENDLOG;
	AddressError( EMSGSIZE, this, address, callback );
	return EMSGSIZE;
	}
    if ( !fControlData || !fMsgBufferAddress )
	{
	if ( !fControlData )
	    {
	    LOG( MLUCLog::kError, "SCI Receive", "Remote buffer error" )
		<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "No remote buffer control data in SCI Receive" << ENDLOG;
	    }
	if ( !fMsgBufferAddress )
	    {
	    LOG( MLUCLog::kError, "SCI Receive", "Remote buffer error" )
		<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "No remote data buffer in SCI Receive" << ENDLOG;
	    }
	MsgReceiveError( ENODEV, this, address, msg, callback );
	return ENODEV;
	}
#ifdef DEBUG
    BCLIntSCIControlData scd( fControlData );
    BCLIntSCIControlDataStruct* localControlData = scd.GetData();
#endif
    fDataArrivedSignal.Lock();
    bool success = true;
    if ( fControlData->fReadIndex == fControlData->fWriteIndex )
	{ // buffer is empty ...
#ifndef DEBUG_NOTHREADS
	if ( useTimeout )
	    success = fDataArrivedSignal.Wait( timeout );
	else
	    fDataArrivedSignal.Wait();
#else
	RequestInterruptHandler();
#endif
	}
#ifdef DEBUG
    scd.SetTo( fControlData );
#endif

    // we got a trigger, so there should be something in the buffer, please have a look ...
    unsigned int counter = 0;
    while( counter < 500 && (fControlData->fReadIndex == fControlData->fWriteIndex))
	{
	busy_wait(10);
	counter++;
	//cout << "Counter " << counter << endl;
	}
     
    if ( fControlData->fReadIndex != fControlData->fWriteIndex )
	{
	fDataArrivedSignal.Unlock();
	if ( fControlData->fReadIndex<fControlData->fWriteIndex )
	    {
	    // No wrap around.
	    if ( fControlData->fWriteIndex - fControlData->fReadIndex < sizeof(BCLSCIAddressStruct)+sizeof(BCLMessageStruct) )
		{
		LOG( MLUCLog::kError, "SCI Receive", "Address/Message error" )
		    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address+message size " 
		    << MLUCLog::kDec << fControlData->fWriteIndex - fControlData->fReadIndex
		    << " (expected at least " << sizeof(BCLSCIAddressStruct)+sizeof(BCLMessageStruct) << ") in SCI Receive." << ENDLOG;
		MsgReceiveError( EMSGSIZE, this, address, msg, callback );
		return EMSGSIZE;
		}
	    BCLSCIAddressStruct* baddr = (BCLSCIAddressStruct*)(fMsgBufferAddress+fControlData->fReadIndex);
	    BCLNetworkData::Transform( baddr->fLength, st, baddr->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );
	    // 	    convAddr.Adopt( baddr );
	    if ( st != sizeof(BCLSCIAddressStruct) )
		{
		LOG( MLUCLog::kError, "SCI Receive", "Address error" )
		    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong address size in buffer" << MLUCLog::kDec << st
		    << " (expected " << sizeof(BCLSCIAddressStruct) << ") in SCI Receive." << ENDLOG;
		fControlData->fReadIndex += st;
		AddressError( EMSGSIZE, this, address, callback );
		return EMSGSIZE;
		}
	    tmpRNdx = fControlData->fReadIndex;
	    memcpy( address, baddr, sizeof(BCLSCIAddressStruct) );
	    tmpRNdx += sizeof(BCLSCIAddressStruct);
	    fControlData->fReadIndex = tmpRNdx;
	    msg = NULL;
	    BCLMessageStruct* bmsg = (BCLMessageStruct*)(fMsgBufferAddress+fControlData->fReadIndex);
	    // 	    BCLMessageStruct bmsgS = *bmsg;
	    // 	    //convMsg.Adopt( bmsg );
	    // 	    convMsg.SetTo( &bmsgS );
	    // 	    //uint32 st = bmsg->fLength;
	    // 	    uint32 st = convMsg.GetData()->fLength;
	    // 	    //convMsg.TransformToOriginal();
	    BCLNetworkData::Transform( bmsg->fLength, st, bmsg->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );
	    if ( st < sizeof(BCLMessageStruct) )
		{
		LOG( MLUCLog::kError, "SCI Receive", "Message error" )
		    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong message size in buffer: " << MLUCLog::kDec << st
		    << " (expected at least " << sizeof(BCLMessageStruct) << ") in SCI Receive." << ENDLOG;
		fControlData->fReadIndex += st;
		MsgReceiveError( EMSGSIZE, this, address, msg, callback );
		return EMSGSIZE;
		}
	    msg = NewMessage( st );
	    if ( !msg )
		{
		LOG( MLUCLog::kError, "SCI Receive", "Out of memory" )
		    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Out of memory allocating message of size " << MLUCLog::kDec << st 
		    << " in SCI Receive." << ENDLOG;
		// XXX ???
		// 		    if ( fControlData->fReadIndex+st<=fControlData->fSize )
		// 			fControlData->fReadIndex += st;
		// XXX ???
		MsgReceiveError( ENOMEM, this, address, msg, callback );
		return ENOMEM;
		}
	    tmpRNdx = fControlData->fReadIndex;
	    memcpy( msg, bmsg, st );
	    tmpRNdx += st;
	    }
	else
	    {
	    // Wrap around.
	    if ( fControlData->fSize - fControlData->fReadIndex < sizeof(BCLSCIAddressStruct) )
		{
		// Address wraps
		tmpRNdx = fControlData->fReadIndex;
		memcpy( address, fMsgBufferAddress+tmpRNdx, 
			fControlData->fSize-tmpRNdx );
		memcpy( ((uint8*)address)+fControlData->fSize-tmpRNdx, fMsgBufferAddress, 
			sizeof(BCLSCIAddressStruct)-(fControlData->fSize-tmpRNdx) );
		tmpRNdx = sizeof(BCLSCIAddressStruct)-(fControlData->fSize-tmpRNdx);
		fControlData->fReadIndex = tmpRNdx;
		
		BCLMessageStruct* bmsg = (BCLMessageStruct*)(fMsgBufferAddress+fControlData->fReadIndex);
		//		convMsg.Adopt( bmsg );
		msg = NULL;
		uint32 st = bmsg->fLength;
		BCLNetworkData::Transform( st, bmsg->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );
		if ( st < sizeof(BCLMessageStruct) )
		    {
		    LOG( MLUCLog::kError, "SCI Receive", "Message error" )
			<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong message size in buffer" << MLUCLog::kDec << st
			<< " (expected at least " << sizeof(BCLMessageStruct) << ") in SCI Receive." << ENDLOG;
		    // XXX ???
		    fControlData->fReadIndex += st;
		    // XXX ???
		    MsgReceiveError( EMSGSIZE, this, address, msg, callback );
		    return EMSGSIZE;
		    }
		msg = NewMessage( st );
		if ( !msg )
		    {
		    LOG( MLUCLog::kError, "SCI Receive", "Out of memory" )
			<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Out of memory allocating message of size " << MLUCLog::kDec << st 
			<< " in SCI Receive." << ENDLOG;
		    // XXX ???
		    //fControlData->fReadIndex += st;
		    // XXX ???
		    MsgReceiveError( ENOMEM, this, address, msg, callback );
		    return ENOMEM;
		    }
		tmpRNdx = fControlData->fReadIndex;
		memcpy( msg, bmsg, st );
		tmpRNdx += st;
		}
	    else
		{
		// Message wraps.
		uint32 st;
		tmpRNdx = fControlData->fReadIndex;
		memcpy( address, fMsgBufferAddress+tmpRNdx, sizeof(BCLSCIAddressStruct) );
		if( fControlData->fSize - tmpRNdx == sizeof(BCLSCIAddressStruct)) 
		    tmpRNdx = 0;
		else
		    tmpRNdx += sizeof(BCLSCIAddressStruct);
		fControlData->fReadIndex = tmpRNdx;
		
		msg = NULL;
		if ( fControlData->fSize - fControlData->fReadIndex < sizeof(BCLMessageStruct) )
		    {
		    BCLMessageStruct tmpMsg;
		    memcpy( &tmpMsg, fMsgBufferAddress+fControlData->fReadIndex, 
			    fControlData->fSize-fControlData->fReadIndex );
		    memcpy( ((uint8*)&tmpMsg)+fControlData->fSize-fControlData->fReadIndex, fMsgBufferAddress, 
			    sizeof(BCLMessageStruct)-(fControlData->fSize-fControlData->fReadIndex) );
		    //convMsg.Adopt( &tmpMsg );
		    st = tmpMsg.fLength;
		    BCLNetworkData::Transform( st, tmpMsg.fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );
		    }
		else
		    {
		    BCLMessageStruct* bmsg = (BCLMessageStruct*)(fMsgBufferAddress+fControlData->fReadIndex);
		    //convMsg.Adopt( bmsg );
		    st = bmsg->fLength;
		    //convMsg.TransformToOriginal();
		    BCLNetworkData::Transform( st, bmsg->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );
		    }
		if ( st < sizeof(BCLMessageStruct) )
		    {
		    LOG( MLUCLog::kError, "SCI Receive", "Message error" )
			<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Wrong message size in buffer" << MLUCLog::kDec << st
			<< " (expected at least " << sizeof(BCLMessageStruct) << ") in SCI Receive." << ENDLOG;
		    // XXX ???
		    fControlData->fReadIndex += st;
		    if(fControlData->fSize - fControlData->fReadIndex == st) 
			fControlData->fReadIndex = 0;
		    else
			fControlData->fReadIndex += st;
		    
		    // XXX ???
		    MsgReceiveError( EMSGSIZE, this, address, msg, callback );
		    return EMSGSIZE;
		    }
		msg = NewMessage( st );
		if ( !msg )
		    {
		    LOG( MLUCLog::kError, "SCI Receive", "Out of memory" )
			<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Out of memory allocating message of size " << MLUCLog::kDec << st 
			<< " in SCI Receive." << ENDLOG;
		    // XXX ???
		    //fControlData->fReadIndex += st;
		    // XXX ???
		    MsgReceiveError( ENOMEM, this, address, msg, callback );
		    return ENOMEM;
		    }
		if ( fControlData->fSize - fControlData->fReadIndex < st )
		    {
		    uint32 tmp = 0;
		    tmpRNdx = fControlData->fReadIndex;
		    memcpy( msg, fMsgBufferAddress+tmpRNdx, 
			    fControlData->fSize-tmpRNdx );
		    memcpy( ((uint8*)msg)+fControlData->fSize-tmpRNdx, fMsgBufferAddress, 
			    st-(fControlData->fSize-tmpRNdx) );
		    tmp = fControlData->fReadIndex;
		    tmpRNdx = st-(fControlData->fSize-tmpRNdx);
		    }
		else
		    {
		    tmpRNdx = fControlData->fReadIndex;
		    memcpy( msg, fMsgBufferAddress+tmpRNdx, st );
		    if(fControlData->fSize - tmpRNdx == st ) 
			tmpRNdx = 0;
		    else
			tmpRNdx += st;
		    }
		}
	    }
	fControlData->fReadIndex = tmpRNdx;
	return 0;
	}
    fDataArrivedSignal.Unlock();
    
    return ETIMEDOUT;
    }

#ifdef USE_INTERRUPT_CALLBACKS

#error Interrupt callback usage not properly supported yet!

void BCLSCIMsgCommunication::RequestInterruptHandler( void *arg )
    {
    if ( !arg )
	return;
    if ( !fControlData  )
	{
	LOG( MLUCLog::kError, "SCI Request interrupt handler", "Internal Error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Internal SCI error handling remote request interrupt" << ENDLOG;
	BindError( ENXIO, this, NULL, NULL );
	return;
	}

    pthread_mutex_lock( &( ((BCLSCIMsgCommunication*)arg)->fRequestMutex ) );
    
    if ( !((BCLSCIMsgCommunication*)arg)->fRequestActive )
	{
	if ( fControlData->fSenderRequest == (uint32)-1 )
	    {
	    continue;
	    }
	// Save values in case a timeout occurs...
	fPreReadIndex = fControlData->fReadIndex;
	fPreWriteIndex = fControlData->fWriteIndex;
	fPreStatusOk = true;
	active = true;
	fControlData->fCurrentSender = fControlData->fSenderRequest;
	}
    else
	{
	active = false;
	if ( mySCIError == SCI_ERR_TIMEOUT )
	    {
	    // Timeout: Restore original values...
	    fControlData->fReadIndex = fPreReadIndex;
	    fControlData->fWriteIndex = fPreWriteIndex;
	    fPreStatusOk = false;
	    fControlData->fSenderRequest = (uint32)-1;
	    fControlData->fCurrentSender = (uint32)-1;
	    continue;
	    }
	fDataArrivedSignal.Signal();
	}
    }


#else

#ifndef DEBUG_NOTHREADS

void BCLSCIMsgCommunication::QuitRequestInterruptHandler()
    {
    if ( fRequestInterruptHandlerQuitted || !fControlData || !fAddress )
	return;
    fQuitRequestInterruptHandler = true;
    sci_remote_interrupt_t remInt;
    sci_error_t mySCIError;
    do
	{
	if ( fRequestInterruptHandlerQuitted )
	    return;
	uint32 firstTry = 0;
#ifndef _SCALI_INTERRUPT_BUGFIX_
	while ( firstTry<CONNECTIONTRY && !fRequestInterruptHandlerQuitted ) // was firstTry<2
	    {
	    SCIConnectInterrupt( fComHelper.fSCIDesc, &remInt, fSCIAddress.fNodeID, 
				 fSCIAddress.fAdapterNr, fControlData->fRequestInterruptNr, 
				 fComHelper.fConnectionTimeout, 0, &mySCIError );
	    if ( mySCIError == SCI_ERR_CONNECTION_REFUSED || mySCIError == SCI_ERR_CANCELLED )
		{
		firstTry++;
		INTERRUPT_WAIT;
		}
	    else
		firstTry=CONNECTIONTRY;
	    }
#else
	mySCIError = SCI_ERR_OK;
#endif
	} 
    while ( mySCIError == SCI_ERR_CANCELLED && !fRequestInterruptHandlerQuitted );
    if ( mySCIError != SCI_ERR_OK && !fRequestInterruptHandlerQuitted ) // was ... || !remInt
	{
	LOG( MLUCLog::kError, "SCI Request interrupt handler shutdown", "Error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "SCI error " << MLUCLog::kHex << (long unsigned int)mySCIError << MLUCLog::kDec << " in SCIConnectInterrupt trying to shut down remote request interrupt handler" << ENDLOG;
	return;
	}
    if ( fRequestInterruptHandlerQuitted )
	return;
#ifndef _SCALI_INTERRUPT_BUGFIX_
    SCITriggerInterrupt( remInt, 0, &mySCIError );
    if ( mySCIError != SCI_ERR_OK )
	{
	LOG( MLUCLog::kError, "SCI Request interrupt handler shutdown", "Error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "SCI error " << MLUCLog::kHex << (long unsigned int)mySCIError << MLUCLog::kDec << " in SCITriggerInterrupt trying to shut down remote request interrupt handler" << ENDLOG;
	return;
	}
    else
	fRequestIntThread.Join();
    SCIDisconnectInterrupt( remInt, 0, &mySCIError );
    if ( mySCIError != SCI_ERR_OK )
	{
	LOG( MLUCLog::kError, "SCI Request interrupt handler shutdown", "Error" )
	    << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "SCI error " << MLUCLog::kHex << (long unsigned int)mySCIError << MLUCLog::kDec << " in SCIDisconnectInterrupt trying to shut down remote request interrupt handler" << ENDLOG;
	return;
	}
#endif
    //    uint32 count=0;
    //     while ( !fRequestInterruptHandlerQuitted && count++<100000 ) 
    // 	;
    //     if ( !fRequestInterruptHandlerQuitted )
    // 	{
    // 	fRequestIntThread.Abort();
    // 	}
    //     else
    // 	{
    // 	fRequestIntThread.Join();
    // 	}
    
    }
#endif

void BCLSCIMsgCommunication::RequestInterruptHandler()
    {
    fQuitRequestInterruptHandler = false;
    fRequestInterruptHandlerQuitted = false;
    unsigned int timeout;
    sci_error_t mySCIError;
    uint32 senderRequest;
    uint32 oldWriteNdx;
    struct timeval start, now;
    unsigned long td;
#ifdef DEBUG
    BCLIntSCIControlData scd;
    BCLIntSCIControlDataStruct *controlData;
    controlData = scd.GetData();
    scd.SetTo( fControlData );
#endif

    while ( !fQuitRequestInterruptHandler )
	{
	if ( !this )
	    {
	    LOG( MLUCLog::kFatal, "SCI Request interrupt handler", "No this" )
		<< "Fatal Error: this pointer unavailable." << ENDLOG;
	    return;
	    }

	oldWriteNdx = fControlData->fWriteIndex;
	senderRequest = fControlData->fSenderRequest;

// 	if ( fControlData->fCurrentSender==~(uint32)0 && fControlData->fCurrentSenderLocked )
// 	    fControlData->fCurrentSenderLocked = 0;

	if ( senderRequest != ~(uint32)0 && fControlData->fCurrentSender==~(uint32)0 )
	    {
	    fControlData->fCurrentSenderLocked = 0;
	    fControlData->fCurrentSender = senderRequest;
	    }
	
	if ( fControlData->fCurrentSender != ~(uint32)0 )
	    timeout = fComHelper.fConnectionTimeout;
	else
	    timeout = SCI_INFINITE_TIMEOUT;

	gettimeofday( &start, NULL );
#ifndef _SCALI_INTERRUPT_BUGFIX_
	SCIWaitForInterrupt( fComHelper.fRequestInterrupt, timeout, 0, &mySCIError );
#else
	mySCIError = SCI_ERR_OK;
	INTERRUPT_WAIT;
#endif
	
	if ( !fControlData  )
	    {
	    LOG( MLUCLog::kError, "SCI Request interrupt handler", "Internal Error" )
		<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Internal SCI error handling remote request interrupt" << ENDLOG;
	    BindError( ENXIO, this, NULL, NULL );
	    break;
	    }
#ifdef DEBUG
	scd.SetTo( fControlData );
#endif
	
	if ( mySCIError != SCI_ERR_OK && mySCIError!=SCI_ERR_CANCELLED )
	    {
	    LOG( MLUCLog::kError, "SCI Request interrupt handler", "SCI Error" )
		<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "SCI error 0x"
		<< mySCIError << " handling remote request interrupt" << ENDLOG;
	    BindError( ENXIO, this, NULL, NULL );
	    break;
	    }
	
	if ( fQuitRequestInterruptHandler )
	    {
	    LOG( MLUCLog::kInformational, "SCI Request interrupt handler", "Handler shutdown requested" )
		<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Request interrupt handler shut down requested." << ENDLOG;
	    break;
	    }
	
	
	if ( fControlData->fWriteIndex != fControlData->fReadIndex )
	    {
	    fDataArrivedSignal.Signal();
	    gettimeofday( &start, NULL );
	    }

	if ( fControlData->fWriteIndex != oldWriteNdx )
	    {
	    // Something was written since WaitForInterrupt was started. Reset timeout
	    gettimeofday( &start, NULL );
	    }
	
	gettimeofday( &now, NULL );
	td = (now.tv_sec-start.tv_sec)*1000 + (now.tv_usec-start.tv_usec)/1000;
	if ( td > fComHelper.fConnectionTimeout && fControlData->fCurrentSender != ~(uint32)0 
	     && !fControlData->fCurrentSenderLocked )
	    {
	    LOG( MLUCLog::kInformational, "SCI Request interrupt handler", "Sender timeout" )
		<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Sender 0x"
		<< fControlData->fCurrentSender << " timed out." << ENDLOG;
	    fControlData->fCurrentSender = ~(uint32)0;
	    }
	
	}
    LOG( MLUCLog::kInformational, "SCI Request interrupt handler", "Handler shutdown" )
	<< "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Request interrupt handler shutting down." << ENDLOG;
    fRequestInterruptHandlerQuitted = true;
    }
#endif    


//     protected:

// 	uint32 fID;
// 	static const uint32 kInvalidSender = (uint32)-1;

// 	struct TSCIControlData
// 	    {
// 		uint32 fSenderRequest;
// 		uint32 fCurrentSender;
// 		uint32 fReadIndex;
// 		uint32 fWriteIndex;
// 		uint32 fReadCount;
// 		uint32 fWriteCount;
// 		uint32 fSize;
// 		uint8  fData[0];
// 	    };

//   	uint8* fSegmentAddress;

// 	  TSCIControlData* fControlData;

// 	sci_desc_t fSCIDesc;
// 	sci_local_interrupt_t fRequestInterrupt;
// 	sci_local_interrupt_t fDataInterrupt;
// 	sci_local_segment_t   fSegment;
// 	sci_map_t fSegmentMap;

// 	struct TSCIConnectionData
// 	    {
// 		BCLSCIAddressStruct fAddress;
// 		remote_segment_t fRemoteSegID;
// 		sci_map_t fRemoteMap;
// 		uint8* fRemoteAddr;
// 		TSCIControlData* fRemoteCD;
// 	    };

// 	vector<TSCIConnectionData> fConnections;


#endif // USE_SCI





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
