/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLBlobCommunication.hpp"

BCLBlobCommunication::BCLBlobCommunication():
    fMsgErrorCallback( this )
    {
    fMsgCom = NULL;
    fBlobBuffer = NULL;
    fBlobBufferSize = 0;
    fBlobBufferAllocated = false;
    fReplyTimeout = 0;
    fReceiveNotificationEnabled = false;
    }

BCLBlobCommunication::BCLBlobCommunication( BCLMsgCommunication* msgCom ):
    fMsgErrorCallback( this )
    {
    fMsgCom = msgCom;
    fBlobBuffer = NULL;
    fBlobBufferSize = 0;
    fBlobBufferAllocated = false;
    fReplyTimeout = 0;
    fReceiveNotificationEnabled = false;
    }

BCLBlobCommunication::~BCLBlobCommunication()
    {
    if ( fBlobBufferAllocated )
	delete [] fBlobBuffer;
    }

bool BCLBlobCommunication::SetBlobBuffer( uint64 size, uint8* blobBuffer )
    {
    if ( fBlobBufferAllocated )
	delete [] fBlobBuffer;
    fBlobBufferAllocated = false;
    fBlobBufferSize = 0;
    fBlobBuffer = NULL;
    if ( !size && !blobBuffer )
	return false;
    if ( !blobBuffer )
	{
	fBlobBuffer = new uint8[ size ];
	if ( fBlobBuffer )
	    fBlobBufferAllocated = true;
	else
	    return false;
	}
    else
	fBlobBuffer = blobBuffer;
    fBlobBufferSize = size;
    return true;
    }

// Attach a message communication object used to send control
// and synchronization messages between the blob transfer objects.
// This object has to be available exclusively to this object!
// The address passed specifies the address of the 
void BCLBlobCommunication::SetMsgCommunication( BCLMsgCommunication* msgCom )
    {
    fMsgCom = msgCom;
    }







/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
