/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLIntTCPComHelper.hpp"
#include "BCLCommunication.hpp"
#include "BCLTCPMsgCommunication.hpp"
#include "BCLTCPBlobCommunication.hpp"
#include "BCLTCPAddressOps.hpp"
#include "MLUCLog.hpp"
#include <errno.h>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>                                                                                                                                                                       
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>


#define CONNECTIONTRY 4


void (*BCLIntTCPComHelper::fOldSigPipeHandler)( int ) = NULL;



BCLIntTCPComHelper::BCLIntTCPComHelper( BCLCommunication* com ):
    fAcceptThread( this )
    {
    fCom = com;
    fConnectionTimeout = 5000; // in millisec.
    fSocket = -1;
    fPortNr = 0;
    fAddress = NULL;
    fBackLog = 40; // backlog specifier for listen
    fQuitAccept = false;
    fAcceptQuitted = true;
    fSelectErrorLimit = 10;

    void (*tmpSigHandler)(int);
    tmpSigHandler = signal ( SIGPIPE, &SigPipeHandler );
    if ( tmpSigHandler != SigPipeHandler && tmpSigHandler != NULL 
	 && tmpSigHandler != SIG_DFL )
	fOldSigPipeHandler = tmpSigHandler;
    pthread_mutex_init( &fConnectionMutex, NULL );
    }

BCLIntTCPComHelper::~BCLIntTCPComHelper()
    {
    pthread_mutex_destroy( &fConnectionMutex );
    vector<TTCPConnectionData>::iterator iter, end;
    iter = fConnections.begin();
    end = fConnections.end();
    while ( iter != end )
	{
	Disconnect( &(iter->fAddress), true, 10000 );
	iter++;
	}
    if ( fSocket != -1 )
	Unbind();
    }

int BCLIntTCPComHelper::Bind( BCLTCPAddressStruct* address )
    {
    if ( fSocket != -1 )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Bind", "Already Bound" )
	    << "Already bound on address 0x" << MLUCLog::kHex
	    << fIPNr << " (" << MLUCLog::kDec << fIPNr << ") : "
	    << fPortNr << " (0x" << MLUCLog::kHex << fPortNr
	    << ")." << ENDLOG;
	AddressError( EBUSY, fCom, address );
	return EBUSY;
	}
    if ( !address )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Bind", "No address" )
	    << "No address (Null pointer) given" << ENDLOG;
	AddressError( EFAULT, fCom, address );
	return EFAULT;
	}
    BCLTCPAddress addr( address );
    fIPNr = addr.GetData()->fIPNr;
    if ( fIPNr == (uint32)NULL )
	fIPNr = INADDR_ANY;
    fPortNr = addr.GetData()->fPortNr;

    int ret, proto;
    ret = GetProtocolByName( "tcp", &proto );
    if ( ret )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Bind", "Protocol File Error" )
	    << "Error reading TCP protocol number from /etc/protocols file: " 
	    << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	BindError( ECANCELED, fCom, address, NULL );
	return ECANCELED;
	}

    int one=1;
    fSocket = socket( AF_INET, SOCK_STREAM, proto );
    if ( fSocket == -1 )
	{
	ret = errno;
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Bind", "Socket Creation Error" )
	    << "Error creating TCP socket (protocol " << proto << "): " 
	    << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	BindError( ret, fCom, address, NULL );
	return ret;
	}
    
    ret = setsockopt( fSocket, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one) );
    if ( ret )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Bind", "Socket reuse addr" )
	    << "Error setting sockets options to reuse addr: "
	    << strerror(errno) << " (" << MLUCLog::kDec << errno << ")." << ENDLOG;
	}
    int socketSize = 1024*1024;
    ret = setsockopt( fSocket, SOL_SOCKET, SO_SNDBUF, (char *)&socketSize, sizeof(int));
    if ( ret < 0 )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Bind", "Socket send buffer size" )
	    << "Error setting sockets send buffer size option: "
	    << strerror(errno) << " (" << MLUCLog::kDec << errno << ")." << ENDLOG;
	}
    struct sockaddr_in sock_addr;
    sock_addr.sin_family = AF_INET;
    sock_addr.sin_port = htons( fPortNr );
    sock_addr.sin_addr.s_addr = htonl( fIPNr );
    memset(&(sock_addr.sin_zero), '\0', 8);
    ret = bind( fSocket,  (const sockaddr*)&sock_addr, sizeof(struct sockaddr) );
    if ( ret==-1 )
	{
	ret = errno;
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Bind", "Socket Bind Error" )
	    << "Error bind'ing TCP socket on port " << MLUCLog::kDec << fPortNr
	    << " (0x" << MLUCLog::kHex << fPortNr << "): "
	    << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	BindError( ret, fCom, address, NULL );
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP", "Socket close" ) << "Closing Socket " << MLUCLog::kDec << fSocket << "." << ENDLOG;
	close( fSocket );
	fSocket = -1;
	return ret;
	}

    ret = listen( fSocket, fBackLog );
    if ( ret == -1 )
	{
	ret = errno;
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Bind", "Socket Listen Error" )
	    << "Error listen'ing with TCP socket on port " << MLUCLog::kDec << fPortNr
	    << " (0x" << MLUCLog::kHex << fPortNr << "): "
	    << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	BindError( ret, fCom, address, NULL );
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP", "Socket close" ) << "Closing Socket " << MLUCLog::kDec << fSocket << "." << ENDLOG;
	close( fSocket );
	fSocket = -1;
	return ret;
	}
    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Bind", "Socket Listening" )
	<< "listen'ing with TCP socket " << MLUCLog::kDec << fSocket 
	<< " on port " << fPortNr
	<< " (0x" << MLUCLog::kHex << fPortNr << ")." << ENDLOG;

    fAddress = address;
    fAcceptThread.Start();
    return 0;
    }

int BCLIntTCPComHelper::Unbind()
    {
    if ( fSocket == -1 )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "TCP Unbind", "Not Bound" )
	    << "TCP Communication helper object is not bound." << ENDLOG;
	return 0;
	}
    fQuitAccept = true;
    int tmpsock;

    int ret, proto;
    ret = GetProtocolByName( "tcp", &proto );
    if ( ret )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "TCP Unbind", "Protocol File Error" )
	    << "Error reading TCP protocol number from /etc/protocols file: " 
	    << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	}
    else
	{
	tmpsock = socket( AF_INET, SOCK_STREAM, proto );
	if ( tmpsock == -1 )
	    {
	    ret = errno;
	    LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "TCP Unbind", "Socket Creation Error" )
		<< "Error creating TCP socket (protocol " << proto << "): " 
		<< strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	    }
	else
	    {
	    fcntl( tmpsock, F_SETFL, O_NONBLOCK );
	    struct timeval newTimeout;
	    newTimeout.tv_sec = 0;
	    newTimeout.tv_usec = 0;
	    ret = setsockopt( tmpsock, SOL_SOCKET, SO_SNDTIMEO, &newTimeout, sizeof(newTimeout) );
	    struct sockaddr_in dest_addr;
	    dest_addr.sin_family = AF_INET;
	    dest_addr.sin_port = htons(fPortNr);
	    dest_addr.sin_addr.s_addr = htonl( (unsigned long)fIPNr );
	    memset(&(dest_addr.sin_zero), '\0', 8);
	    connect( tmpsock, (struct sockaddr *)&dest_addr, sizeof(struct sockaddr) );
	    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP", "Socket close" ) << "Closing Socket " << MLUCLog::kDec << tmpsock << "." << ENDLOG;
	    close( tmpsock );
	    }
	}

    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    gettimeofday( &start, NULL );
    while ( !fAcceptQuitted && deltaT<=timeLimit )
	{
	usleep( 10000 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}

    if ( !fAcceptQuitted )
	{
	fAcceptThread.Abort();
	fAcceptQuitted = true;
	}
    else
	fAcceptThread.Join();
    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Unbind", "Closing Socket" )
	<< "Closing TCP listening socket." << ENDLOG;
    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP", "Socket close" ) << "Closing Socket " << MLUCLog::kDec << fSocket << "." << ENDLOG;
    close( fSocket );
    fSocket = -1;
    fAddress = NULL;
    return 0;
    }

int BCLIntTCPComHelper::Connect( BCLTCPAddressStruct* address, bool useTimeout, uint32 timeout_ms, bool openOnDemand )
    {
    TTCPConnectionData connectData;
    connectData.fAddress = *address;
    connectData.fUsageCount = 0;
    connectData.fInProgress = false;
    connectData.fSocket = -1;
    int ret;
    ret = ConnectToRemote( address, &connectData, !openOnDemand, useTimeout, timeout_ms );
    if ( ret )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Connect", "Connection error" )
	    << "Address 0x" << MLUCLog::kHex
	    << (address ? address->fIPNr : 0xFFFFFFFF) << " (" << MLUCLog::kDec << (address ? address->fIPNr : 0xFFFFFFFF) << ") : "
	    << (address ? address->fPortNr : 0xFFFF) << " (0x" << MLUCLog::kHex << (address ? address->fPortNr : 0xFFFF)
	    << " TCP connection error" << ENDLOG;
	ConnectionError( ret, fCom, address );
	return ret;
	}
    return 0;
    }

int BCLIntTCPComHelper::Disconnect( BCLTCPAddressStruct* address, bool useTimeout, uint32 timeout_ms )
    {
    TTCPConnectionData connectData;
    connectData.fAddress = *address;
    connectData.fUsageCount = 0;
    connectData.fInProgress = false;
    connectData.fSocket = -1;
    return DisconnectFromRemote( &connectData, useTimeout, timeout_ms );
    }

int BCLIntTCPComHelper::ConnectToRemote( BCLTCPAddressStruct* address, TTCPConnectionData* parConnectData, bool now, bool useTimeout, uint32 timeout_ms )
    {
    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::ConnectToRemote", "Remote connect" )
	<< "Connection attempt: " << MLUCLog::kDec << *address << " now: " << (now ? "true" : "false" )
	<< "." << ENDLOG;
    // First we check to see, wether that connection has already been established
    TTCPConnectionData* connectData = parConnectData;
    vector<TTCPConnectionData>::iterator iter, end;
    parConnectData->fSocket = -1;
    TTCPConnectionData connectionData = *parConnectData;
    bool retry=false;
    do
	{
	retry=false;
	pthread_mutex_lock( &fConnectionMutex );
	iter = fConnections.begin();
	end = fConnections.end();
	while ( iter != end )
	    {
	    if ( iter->fAddress == *address )
		{
		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::ConnectToRemote", "Remote connect" )
		    << "Address found: " << MLUCLog::kDec << *address << " socket: " << iter->fSocket << "." << ENDLOG;
		// We found it.
		if ( iter->fInProgress )
		    {
		    retry = true;
		    break;
		    }
		iter->fUsageCount++;
		if ( !now || (now && iter->fSocket!=-1) )
		    {
		    *parConnectData = *iter;
		    pthread_mutex_unlock( &fConnectionMutex );
		    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::ConnectToRemote", "Remote connect" )
			<< "Found and done: " << MLUCLog::kDec << *address << "." << ENDLOG;
		    return 0;
		    }
		iter->fInProgress = true;
#if __GNUC__>=3
		connectData = iter.base();
#else
		connectData = iter;
#endif
		connectionData = *iter;
		break;
		}
	    iter++;
	    }
	pthread_mutex_unlock( &fConnectionMutex );
	if ( retry )
	    fConnectionUpdateMutex.Wait( 200 );
	}
    while ( retry );
    
    connectionData.fSocket = -1;
    if ( now )
	{
#if 0
	bool retry=false;
	unsigned retryCnt=0;
	const unsigned maxRetryCnt=4;
	do
	    {
#endif
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::ConnectToRemote", "Remote connect" )
	    << "Attempting to connect: " << MLUCLog::kDec << *address << "." << ENDLOG;
	uint16 portNr;
	uint32 ipNr;
	BCLTCPAddress addr( address );
	ipNr = addr.GetData()->fIPNr;
	if ( ipNr == (uint32)NULL )
	    ipNr = INADDR_ANY;
	portNr = addr.GetData()->fPortNr;
	int ret, proto;
	ret = GetProtocolByName( "tcp", &proto );
	if ( ret )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kFatal, "TCP ConnectToRemote", "Protocol File Error" )
		<< "Error reading TCP protocol number from /etc/protocols file: " 
		<< strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	    pthread_mutex_lock( &fConnectionMutex );
	    iter = fConnections.begin();
	    end = fConnections.end();
	    while ( iter != end )
		{
		if ( iter->fAddress == *address )
		    {
		    // We found it again.
		    iter->fInProgress = false;
		    break;
		    }
		iter++;
		}
	    pthread_mutex_unlock( &fConnectionMutex );
	    fConnectionUpdateMutex.Signal();
	    return ret;
	    }
	else
	    {
	    connectionData.fSocket = socket( AF_INET, SOCK_STREAM, proto );
	    if ( connectionData.fSocket == -1 )
		{
		ret = errno;
		LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP ConnectToRemote", "Socket Creation Error" )
		    << "Error creating TCP socket (protocol " << proto << "): " 
		    << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
		pthread_mutex_lock( &fConnectionMutex );
		iter = fConnections.begin();
		end = fConnections.end();
		while ( iter != end )
		    {
		    if ( iter->fAddress == *address )
			{
			// We found it again.
			iter->fInProgress = false;
			break;
			}
		    iter++;
		    }
		pthread_mutex_unlock( &fConnectionMutex );
		fConnectionUpdateMutex.Signal();
		return ret;
		}
	    else
		{
		int one=1;
		ret = setsockopt( connectionData.fSocket, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one) );
		if ( ret )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP ConnectToRemote", "Socket reuse addr" )
			<< "Error setting sockets options to reuse addr: "
			<< strerror(errno) << " (" << MLUCLog::kDec << errno << ")." << ENDLOG;
		    }
		bool nonblockset=false;
		ret = fcntl( connectionData.fSocket, F_GETFL );
		if ( ret==-1 )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "TCP ConnectToRemote", "fcntl get flag error" )
			<< "Error determining file control flags for new connection socket " << MLUCLog::kDec
			<< connectionData.fSocket << ": " << strerror(errno) << " (" << errno << ")." << ENDLOG;
		    }
		else
		    {
		    if ( ret & O_NONBLOCK )
			{
			LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "TCP ConnectToRemote", "non block flag set" )
			    << "Non-blocking file control flag is set for new connection socket " << MLUCLog::kDec
			    << connectionData.fSocket << ". Trying to clear now." << ENDLOG;
			nonblockset = true;
			}
		    ret = ret & ~O_NONBLOCK;
		    ret = fcntl( connectionData.fSocket, F_SETFL, ret );
		    if ( ret==-1 )
			{
			LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "TCP ConnectToRemote", "fcntl non block flag clear error" )
			<< "Error trying to clear non-blocking file control flag for new connection socket " << MLUCLog::kDec
			<< connectionData.fSocket << ": " << strerror(errno) << " (" << errno << ")." << ENDLOG;
			}
		    }
		ret = setsockopt( connectionData.fSocket, SOL_TCP, TCP_NODELAY, &one, sizeof(one) );
		if ( ret )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "TCP ConnectToRemote", "TCP_NODELAY error" )
			<< "Error activating TCP_NODELAY option for new connection socket " << MLUCLog::kDec
			<< connectionData.fSocket << ": " << strerror(errno) << " (" << errno << ")." << ENDLOG;
		    }
		ret = setsockopt( connectionData.fSocket, SOL_SOCKET, SO_KEEPALIVE, &one, sizeof(one) );
		if ( ret )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "TCP ConnectToRemote", "SO_KEEPALIVE error" )
			<< "Error activating SO_KEEPALIVE option for new connection socket " << MLUCLog::kDec
			<< connectionData.fSocket << ": " << strerror(errno) << " (" << errno << ")." << ENDLOG;
		    }
		
		struct timeval oldTimeout;
		if ( useTimeout )
		    {
		    socklen_t sockoptlen = sizeof(oldTimeout);
		    ret = getsockopt( connectionData.fSocket, SOL_SOCKET, SO_SNDTIMEO, &oldTimeout, &sockoptlen );
		    if ( ret==-1 )
			{
			ret = errno;
			LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP ConnectToRemote", "Socket Timeout Get Error" )
			    << "Error getting TCP send timeout from socket to remote address 0x" << MLUCLog::kHex
			    << ipNr << " (" << MLUCLog::kDec << ipNr << ") : "
			    << portNr << " (0x" << MLUCLog::kHex << portNr << "): " 
			    << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
			}
		    else
			{
			struct timeval newTimeout;
			newTimeout.tv_sec = timeout_ms/1000;
			newTimeout.tv_usec = (timeout_ms-(newTimeout.tv_sec*1000))*1000;
			ret = setsockopt( connectionData.fSocket, SOL_SOCKET, SO_SNDTIMEO, &newTimeout, sizeof(newTimeout) );
			if ( ret==-1 )
			    {
			    ret = errno;
			    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP ConnectToRemote", "Socket Timeout Set Error" )
				<< "Error getting TCP send timeout from socket to remote address 0x" << MLUCLog::kHex
				<< ipNr << " (" << MLUCLog::kDec << ipNr << ") : "
				<< portNr << " (0x" << MLUCLog::kHex << portNr << "): " 
				<< strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
			    }
			}
		    }
		if ( fIPNr!=INADDR_ANY )
		    {
		    struct sockaddr_in local_sock_addr;
		    local_sock_addr.sin_family = AF_INET;
		    local_sock_addr.sin_port = INADDR_ANY;
		    local_sock_addr.sin_addr.s_addr = htonl( fIPNr );
		    memset(&(local_sock_addr.sin_zero), '\0', 8);
		    ret = bind( connectionData.fSocket,  (const sockaddr*)&local_sock_addr, sizeof(struct sockaddr) );
		    if ( ret==-1 )
			{
			ret = errno;
			LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP ConnectToRemote", "Connect Socket Bind Error" )
			    << "Error bind'ing TCP connection socket on port " << MLUCLog::kDec << INADDR_ANY
			    << " (0x" << MLUCLog::kHex << fPortNr << "): "
			    << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
			LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP", "TCP ConnectToRemote" ) << "Closing Socket " << MLUCLog::kDec << connectionData.fSocket << "." << ENDLOG;
			close( connectionData.fSocket );
			connectionData.fSocket = -1;
			pthread_mutex_lock( &fConnectionMutex );
			iter = fConnections.begin();
			end = fConnections.end();
			while ( iter != end )
			    {
			    if ( iter->fAddress == *address )
				{
				// We found it again.
				iter->fSocket = -1;
				iter->fInProgress = false;
				break;
				}
			    iter++;
			    }
			pthread_mutex_unlock( &fConnectionMutex );
			fConnectionUpdateMutex.Signal();
			return ret;
			}
		    }
		struct sockaddr_in dest_addr;
		dest_addr.sin_family = AF_INET;
		dest_addr.sin_port = htons(portNr);
		dest_addr.sin_addr.s_addr = htonl( (unsigned long)ipNr );
		memset(&(dest_addr.sin_zero), '\0', 8);
		bool retry=false;
		unsigned retryCnt=0;
		const unsigned maxRetryCnt=1;
		do
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP ConnectToRemote", "connect" )
			<< "connect call to remote endpoint IP: " << ntohl(  dest_addr.sin_addr.s_addr ) << " (0x"
			<< MLUCLog::kHex << ntohl( dest_addr.sin_addr.s_addr ) << ")." << ENDLOG;
		    ret = connect( connectionData.fSocket, (struct sockaddr *)&dest_addr, sizeof(struct sockaddr) );
		    if ( ret==-1 && (errno==EINPROGRESS || errno==EALREADY) && retryCnt<maxRetryCnt )
			    {
			    usleep( 0 );
			    ++retryCnt;
			    retry = true;
			    continue;
			    }
		    if ( retry && ret==-1 && errno==EISCONN )
			{
			ret=0;
			errno = 0;
			}
		    if ( ret==-1 && errno!=EAGAIN )
			{
			ret = errno;
			LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP ConnectToRemote", "Socket Connect Error" )
			    << "Error connecting TCP socket to remote address 0x" << MLUCLog::kHex
			    << ipNr << " (" << MLUCLog::kDec << ipNr << ") : "
			    << portNr << " (0x" << MLUCLog::kHex << portNr << "): " 
			    << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
			LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP", "Socket close" ) << "Closing Socket " << MLUCLog::kDec << connectionData.fSocket << "." << ENDLOG;
			close( connectionData.fSocket );
			connectionData.fSocket = -1;
			pthread_mutex_lock( &fConnectionMutex );
			iter = fConnections.begin();
			end = fConnections.end();
			while ( iter != end )
			    {
			    if ( iter->fAddress == *address )
				{
				// We found it again.
				iter->fSocket = -1;
				iter->fInProgress = false;
				break;
				}
			    iter++;
			    }
			pthread_mutex_unlock( &fConnectionMutex );
			fConnectionUpdateMutex.Signal();
			return ret;
			}
		    }
		while ( ret==-1 && errno==EAGAIN );
		if ( nonblockset )
		    {
		    ret = fcntl( connectionData.fSocket, F_GETFL );
		    if ( ret==-1 )
			{
			LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "TCP ConnectToRemote", "fcntl get flag error" )
			    << "Error determining file control flags for new connection socket " << MLUCLog::kDec
			    << connectionData.fSocket << ": " << strerror(errno) << " (" << errno << ")." << ENDLOG;
			}
		    else
			{
			ret = ret | O_NONBLOCK;
			ret = fcntl( connectionData.fSocket, F_SETFL, ret );
			if ( ret==-1 )
			    {
			    LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "TCP ConnectToRemote", "fcntl non block flag clear error" )
				<< "Error trying to re-set non-blocking file control flag for new connection socket " << MLUCLog::kDec
				<< connectionData.fSocket << ": " << strerror(errno) << " (" << errno << ")." << ENDLOG;
			    }
			}
		    }
		// XXXX ???
// 		ret = fcntl( connectionData.fSocket, F_SETFL, O_NONBLOCK );
// 		if ( ret )
// 		    {
// 		    LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "TCP ConnectToRemote", "O_NONBLOCK error" )
// 			<< "Error O_NONBLOCK for new connection socket " << MLUCLog::kDec
// 			<< connectionData.fSocket << ": " << strerror(errno) << " (" << errno << ")." << ENDLOG;
// 		    }

		//uint16 portNr = htons( fAddress->fPortNr );
		portNr = htons( fAddress->fPortNr );
		bool looped = false;
		unsigned int count = 0;
		do
		    {
		    ret = write( connectionData.fSocket, &portNr, sizeof(uint16) );
		    if ( ret<0 && errno!=EAGAIN )
			break;
		    if ( ret==0 && looped )
			break;
		    looped = true;
		    count += ret;
		    if ( count >= sizeof(uint16) )
			break;
		    do
			{
#ifdef TCPCOM_NO_POLL
			fd_set sockets;
			struct timeval tv, *ptv;
			FD_ZERO( &sockets );
			FD_SET( connectionData.fSocket, &sockets );
			errno = 0;
			if ( useTimeout )
			    {
			    tv.tv_sec = timeout_ms/1000;
			    tv.tv_usec = (timeout_ms-tv.tv_sec*1000)*1000;
			    ptv = &tv;
			    }
			else
			    ptv = NULL;
			ret = select( connectionData.fSocket+1, NULL, &sockets, NULL, ptv );
#else
			pollfd poll_socket = { connectionData.fSocket, POLLOUT, 0 };
			errno = 0;
			ret = poll( &poll_socket, 1, useTimeout ? timeout_ms : -1 );
#endif
			// XXX No further measures for timeouts here ??
			}
		    while ( ret==0 && (errno==EINTR || errno==EAGAIN) );
		    if ( ret != 1 )
			break;
		    }
		while ( count < sizeof(uint16) );
		if ( count < sizeof(uint16) )
		    {
		    ret = errno;
		    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP ConnectToRemote", "Socket Connect Error" )
			<< "Error writing TCP socket initial connection data to remote address 0x" << MLUCLog::kHex
			    << ipNr << " (" << MLUCLog::kDec << ipNr << ") : "
			    << portNr << " (0x" << MLUCLog::kHex << portNr << "): " 
			    << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
		    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP", "Socket close" ) << "Closing Socket " << MLUCLog::kDec << connectionData.fSocket << "." << ENDLOG;
		    close( connectionData.fSocket );
		    pthread_mutex_lock( &fConnectionMutex );
		    iter = fConnections.begin();
		    end = fConnections.end();
		    while ( iter != end )
			{
			if ( iter->fAddress == *address )
			    {
			    // We found it again.
			    iter->fSocket = -1;
			    iter->fInProgress = false;
			    break;
			    }
			iter++;
			}
		    pthread_mutex_unlock( &fConnectionMutex );
		    fConnectionUpdateMutex.Signal();
		    return ret;
		    }

		if ( useTimeout )
		    setsockopt( connectionData.fSocket, SOL_SOCKET, SO_SNDTIMEO, &oldTimeout, sizeof(oldTimeout) );
		
		
		}
	    }
	}
    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::ConnectToRemote", "Remote connect" )
	<< "Connection established: " << MLUCLog::kDec << *address << "." << ENDLOG;
    if ( connectData==parConnectData )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::ConnectToRemote", "Remote connect" )
	    << "Inserting connection: " << MLUCLog::kDec << *address << "." << ENDLOG;
	connectionData.fUsageCount = 1;
	connectionData.fInProgress = false;
	pthread_mutex_lock( &fConnectionMutex );
	fConnections.insert( fConnections.end(), connectionData );
	pthread_mutex_unlock( &fConnectionMutex );
	}
    if ( connectData != parConnectData )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::ConnectToRemote", "Remote connect" )
	    << "Updating connection: " << MLUCLog::kDec << *address << "." << ENDLOG;
	connectionData.fInProgress = false;
	pthread_mutex_lock( &fConnectionMutex );
	//*parConnectData = *connectData;
	iter = fConnections.begin();
	end = fConnections.end();
	while ( iter != end )
	    {
	    if ( iter->fAddress == *address )
		{
		// We found it again.
		*iter = connectionData;
		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::ConnectToRemote", "Remote connect" )
		    << "Updated connection: " << MLUCLog::kDec << *address << "." << ENDLOG;
		break;
		}
	    iter++;
	    }
	pthread_mutex_unlock( &fConnectionMutex );
	fConnectionUpdateMutex.Signal();
	if ( iter == end )
	    {
	    // Not inserted, have to close connection again
	    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP", "Socket close" ) << "Closing Socket " << MLUCLog::kDec << connectionData.fSocket << "." << ENDLOG;
	    close( connectionData.fSocket );
	    parConnectData->fSocket = -1;
	    return EAGAIN;
	    }
	}
    *parConnectData = connectionData;
    return 0;
    }

int BCLIntTCPComHelper::DisconnectFromRemote( TTCPConnectionData* connectData, bool useTimeout, uint32 timeout_ms, bool forceSocketClose )
    {
    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::DisconnectFromRemote", "Remote disconnect" )
	<< "Disconnect requested from address " << MLUCLog::kDec << connectData->fAddress << "." << ENDLOG;
    vector<TTCPConnectionData>::iterator iter, end;
    pthread_mutex_lock( &fConnectionMutex );
    iter = fConnections.begin();
    end = fConnections.end();
    while ( iter != end )
	{
	if ( iter->fAddress == connectData->fAddress )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::DisconnectFromRemote", "Remote disconnect" )
		<< "Found address " << MLUCLog::kDec << connectData->fAddress << "." << ENDLOG;
	    iter->fUsageCount--;
	    if ( iter->fUsageCount <= 0 )
		{
		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::DisconnectFromRemote", "Remote disconnect" )
		    << "Closing connection to " << MLUCLog::kDec << connectData->fAddress << "." << ENDLOG;
		int s = iter->fSocket;
		fConnections.erase( iter );
		pthread_mutex_unlock( &fConnectionMutex );
		if ( useTimeout )
		    {
		    struct timeval newTimeout;
		    newTimeout.tv_sec = timeout_ms/1000;
		    newTimeout.tv_usec = (timeout_ms-(newTimeout.tv_sec*1000))*1000;
		    setsockopt( s, SOL_SOCKET, SO_SNDTIMEO, &newTimeout, sizeof(newTimeout) );
		    }
		if ( s != -1 )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP", "Socket close" ) << "Closing Socket " << MLUCLog::kDec << s << "." << ENDLOG;
		    close( s );
		    }
		}
	    else
		{
		if ( forceSocketClose )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP", "Socket close" ) << "Closing Socket " << MLUCLog::kDec << iter->fSocket << "." << ENDLOG;
		    close( iter->fSocket );
		    iter->fSocket = -1;
		    }
		pthread_mutex_unlock( &fConnectionMutex );
		}
	    return 0;
	    }
	iter++;
	}
    pthread_mutex_unlock( &fConnectionMutex );
    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::DisconnectFromRemote", "Remote disconnect" )
	<< "Cannot find connection to " << MLUCLog::kDec << connectData->fAddress << "." << ENDLOG;
    if ( useTimeout )
	{
	struct timeval newTimeout;
	newTimeout.tv_sec = timeout_ms/1000;
	newTimeout.tv_usec = (timeout_ms-(newTimeout.tv_sec*1000))*1000;
	setsockopt( connectData->fSocket, SOL_SOCKET, SO_SNDTIMEO, &newTimeout, sizeof(newTimeout) );
	}
    if ( connectData->fSocket != -1 )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP", "Socket close" ) << "Closing Socket " << MLUCLog::kDec << connectData->fSocket << "." << ENDLOG;
	close( connectData->fSocket );
	}
    return EADDRNOTAVAIL;
    }


void BCLIntTCPComHelper::Accept()
    {
    fAcceptQuitted = false;
    int select_error_cnt = 0;
    vector< vector<TAcceptedConnectionData>::iterator > timeoutCons;
    vector<int> closeSockets;
    vector<int>::iterator cs_iter, cs_end;
    while ( !fQuitAccept )
	{
	int ret;
	struct timeval conTimeout, selectStart, selectEnd;
	vector<TAcceptedConnectionData>::iterator sock_iter, sock_end;
#ifdef TCPCOM_NO_POLL
	fd_set sockets, err_sockets;
	int highest_sock;
	struct timeval tv;
#else
	pollfd poll_sockets[fAcceptedSockets.size()+1];
	int pollTimeout;
#endif
	int sock_count;
	// Prepare the socket set for select, we add our accept socket
	// for new incoming connections and all accepted connections
	// to watch for new data to receive.
	
	// Now we do the select loop, this is done as long as 
	// there are no new connections or data (select returned because of timeout), 
	// no timeouts, no errors, and fQuitAccept hasn't been set to true.
	timeoutCons.clear();
	do
	    {
	    sock_count = 0;
#ifdef TCPCOM_NO_POLL
	    FD_ZERO( &sockets);
	    FD_ZERO( &err_sockets);
	    FD_SET( fSocket, &sockets );
	    FD_SET( fSocket, &err_sockets );
	    highest_sock = fSocket;
#else
	    poll_sockets[sock_count].fd = fSocket;
	    poll_sockets[sock_count].events = POLLIN;
	    poll_sockets[sock_count].revents = 0;
	    ++sock_count;
#endif
 	    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::Accept", "Added listen socket" )
 		<< "Port " << MLUCLog::kDec << fPortNr << " (0x" << MLUCLog::kHex << fPortNr << ") added listen socket " << MLUCLog::kDec << fSocket << " to select set." << ENDLOG;
	    gettimeofday( &selectStart, NULL );
	    conTimeout.tv_sec = conTimeout.tv_usec = 0;
	    sock_iter = fAcceptedSockets.begin();
	    sock_end = fAcceptedSockets.end();
	    while ( sock_iter != sock_end )
		{
#ifdef TCPCOM_NO_POLL
		FD_SET( sock_iter->fSocket, &sockets );
		FD_SET( sock_iter->fSocket, &err_sockets );
		if ( sock_iter->fSocket > highest_sock )
		    highest_sock = sock_iter->fSocket;
#else
		poll_sockets[sock_count].fd = sock_iter->fSocket;
		poll_sockets[sock_count].events = POLLIN|POLLERR;
		poll_sockets[sock_count].revents = 0;
		++sock_count;
#endif
 		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::Accept", "Added socket" )
 		    << "Port " << MLUCLog::kDec << fPortNr << " (0x" << MLUCLog::kHex << fPortNr << ") added socket " << MLUCLog::kDec << sock_iter->fSocket << " to select set." << ENDLOG;
		if ( sock_iter->fTimeout.tv_sec > conTimeout.tv_sec || 
		     (sock_iter->fTimeout.tv_sec == conTimeout.tv_sec && sock_iter->fTimeout.tv_usec > conTimeout.tv_usec) )
		    conTimeout = sock_iter->fTimeout;
		sock_iter++;
		}
	    // We use at most a half a second timeout for select to periodically check 
	    // fQuitAccept wether the loop should be quitted. (See Unbind)
#ifdef TCPCOM_NO_POLL
	    if ( conTimeout.tv_sec > 0 || conTimeout.tv_usec > 500000 || conTimeout.tv_usec==0 )
		{
		tv.tv_sec = 0;
		tv.tv_usec = 500000;
		}
	    else
		tv = conTimeout;
	    
// 	    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::Accept", "entering select" )
// 		<< "Port " << MLUCLog::kDec << fPortNr << " entering select - Timeout: "
// 		<< tv.tv_sec << " s " << tv.tv_usec << " usec." << ENDLOG;
	    ret = select( highest_sock+1, &sockets, NULL, &err_sockets, &tv );
  	    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::Accept", "leaving select" )
  		<< "Port " << MLUCLog::kDec << fPortNr << " leaving select (ret: " << 
  		ret << ")." << ENDLOG;
#else
	    if ( conTimeout.tv_sec > 0 || conTimeout.tv_usec > 500000 || conTimeout.tv_usec==0 )
		{
		pollTimeout = 500;
		}
	    else
		pollTimeout = conTimeout.tv_sec*1000 + conTimeout.tv_usec/1000;
	    ret = poll( poll_sockets, sock_count, pollTimeout );
#endif
	    // Calculate elapsed time for select.
	    gettimeofday( &selectEnd, NULL );
	    if ( selectEnd.tv_usec < selectStart.tv_usec )
		{
		selectEnd.tv_usec += 1000000;
		selectEnd.tv_sec  -= 1;
		}
	    selectEnd.tv_usec -= selectStart.tv_usec;
	    selectEnd.tv_sec  -= selectStart.tv_sec;
	    
	    // Check sockets for expired timeouts and calculate new timeout values
	    sock_iter = fAcceptedSockets.begin();
	    sock_end = fAcceptedSockets.end();
#ifndef TCPCOM_NO_POLL
	    unsigned int sockIndex=1;
#endif
	    while ( sock_iter != sock_end )
		{
		// Timeout of 0 means disabled.
		if ( sock_iter->fTimeout.tv_sec || sock_iter->fTimeout.tv_usec )
		    {
		    // Check for expired timeouts.
		    if ( selectEnd.tv_sec > sock_iter->fTimeout.tv_sec ||
			 (selectEnd.tv_sec == sock_iter->fTimeout.tv_sec && selectEnd.tv_usec > sock_iter->fTimeout.tv_usec) )
			{
			// Check wether new data is available. Timeout only without available data...
#ifdef TCPCOM_NO_POLL
			if ( !FD_ISSET( sock_iter->fSocket, &sockets ) )
#else
			if ( !( poll_sockets[sockIndex].revents & POLLIN ) )
#endif
			    {
			    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::Accept", "Timeout Expired" )
				<< "Timeout expired for socket " << MLUCLog::kDec 
				<< sock_iter->fSocket << "." << ENDLOG;
			    timeoutCons.insert( timeoutCons.end(), sock_iter );
			    }
			else
			    sock_iter->fTimeout.tv_sec = sock_iter->fTimeout.tv_usec = 0;
			}
		    else
			{
			// Calculate new timeout by subtracting elapsed time
			if ( sock_iter->fTimeout.tv_usec < selectEnd.tv_usec )
			    {
			    sock_iter->fTimeout.tv_usec += 1000000;
			    sock_iter->fTimeout.tv_sec  -= 1;
			    }
			sock_iter->fTimeout.tv_usec -= selectEnd.tv_usec;
			sock_iter->fTimeout.tv_sec  -= selectEnd.tv_sec;
			}
		    }
		sock_iter++;
#ifndef TCPCOM_NO_POLL
		++sockIndex;
#endif
		}
	    }
	while ( ret==0 && timeoutCons.size()<=0 && !fQuitAccept );

  	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::Accept", "select loop left" )
  	    << "Port " << MLUCLog::kDec << fPortNr << " left select loop with ret=="
  	    << ret << " - " << timeoutCons.size() << " timeouts - fQuitAccept=="
  	    << fQuitAccept << "." << ENDLOG;

	if ( fQuitAccept )
	    break;
	
	if ( ret>0 )
	    {
	    // No error occured
	    select_error_cnt = 0;
	    sock_count = ret;
	    // Now we check which socket triggered us.
	    // First the connection socket
#ifdef TCPCOM_NO_POLL
	    if ( FD_ISSET( fSocket, &sockets ) )
#else
	    if ( poll_sockets[0].revents & POLLIN )
#endif
		{
		// A new connection for us...
		TAcceptedConnectionData con;
		con.fAddrLen = sizeof(con.fAddr);
		con.fSocket = accept( fSocket, (struct sockaddr*)&con.fAddr, &con.fAddrLen );
 		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::Accept", "New connection socket" )
 		    << "Port " << MLUCLog::kDec << fPortNr << " (0x" << MLUCLog::kHex << fPortNr << ") new connection socket " << MLUCLog::kDec << con.fSocket << "." << ENDLOG;
		struct sockaddr_in rem_addr;
		socklen_t rem_addrlen=sizeof(struct sockaddr_in);
		memset( &rem_addr, 0, sizeof(rem_addr) );
		ret = getpeername( con.fSocket, (struct sockaddr*)&rem_addr, &rem_addrlen );
		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Accept Routine", "getpeername" )
		    << "getpeername call to determine the connection's remote endpoint IP: " << strerror(errno) << " ("
		    << MLUCLog::kDec << errno << ") - " << ret << ": " << ntohl(  rem_addr.sin_addr.s_addr ) << " (0x"
		    << MLUCLog::kHex << ntohl( rem_addr.sin_addr.s_addr ) << ")." << ENDLOG;
		if ( ret )
		    {
		    ret = errno;
		    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Accept Routine", "Error in getpeername" )
			<< "An error occured in the getpeername call to determine the connection's remote endpoint IP: " << strerror(ret) << " ("
			<< MLUCLog::kDec << ret << ")." << ENDLOG;
		    }
		BCLTCPAddress tmpAddr;
		tmpAddr.GetData()->fComID = kTCPComID;
		if ( !ret )
		    {
		    tmpAddr.GetData()->fIPNr = ntohl( rem_addr.sin_addr.s_addr );
		    }
		memcpy( &con.fAddress, tmpAddr.GetData(), tmpAddr.GetData()->fLength );
		uint16 portNr;
		struct timeval start, now;
		long long tdiff;
		gettimeofday( &start, NULL );
		do
		    {
#ifdef TCPCOM_NO_POLL
		    fd_set tmp_sockets;
		    FD_ZERO( &tmp_sockets );
		    FD_SET( con.fSocket, &tmp_sockets );
		    tv.tv_sec = fConnectionTimeout/1000;
		    tv.tv_usec = (fConnectionTimeout % 1000) * 1000;
		    ret = select( con.fSocket+1, &tmp_sockets, NULL, NULL, &tv );
#else
		    pollfd tmp_poll_sock = { con.fSocket, POLLIN, 0 };
		    ret = poll( &tmp_poll_sock, 1, fConnectionTimeout );
#endif
		    int old_errno = errno;
		    gettimeofday( &now, NULL );
		    errno = old_errno;
		    tdiff = now.tv_sec-start.tv_sec;
		    tdiff *= 1000000;
		    tdiff += now.tv_usec-start.tv_usec;
		    // Protection against negative tdiff
		    // can happen when clock is adjusted by ntp during the operation
		    if( tdiff < 0 )
		        {
			LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "TCP Accept Routine", "Clock skew detected." )
			  << "Clock skew detected while attempting to read the connection's remote endpoint socket: " 
			  << "Time for operation is negative: " << MLUCLog::kDec << tdiff << " microsec. " << ENDLOG;
			}
		    if ( tdiff > ((long long) (fConnectionTimeout))*1000LL )
			{
			ret=0;
			errno=ETIMEDOUT;
			}
		    }
		while ( ret!=1 && (errno==EINTR || errno==EAGAIN) );
		if ( ret!=1 )
		    {
		    ret = errno;
		    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Accept Routine", "Error while attempting to read remote port" )
			<< "An error occured while attempting to read the connection's remote endpoint socket: " << strerror(ret) << " ("
			<< MLUCLog::kDec << ret << ") - Time for operation: " << MLUCLog::kDec << tdiff << " microsec. - timeout: " << fConnectionTimeout*1000 << " microsec." << ENDLOG;
		    }
		else
		    {
		    ret = read( con.fSocket, &portNr, sizeof(uint16) );
		    if ( ret != sizeof(uint16) )
			{
			ret = errno;
			LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Accept Routine", "Error while reading remote port" )
			    << "An error occured in the read call to determine the connection's remote endpoint socket: " << strerror(ret) << " ("
			    << MLUCLog::kDec << ret << ")." << ENDLOG;
			}
		    else
			con.fAddress.fPortNr = ntohs( portNr );
		    }
		con.fBufferContentStart = 0;
		con.fBufferContentSize = 0;
		con.fTimeout.tv_sec = 0;
		con.fTimeout.tv_usec = 0; // Timeout of zero means disabled.
		// Inform our parent object about the new connection
		if ( NewConnection( con ) )
		    {
		    fcntl( con.fSocket, F_SETFL, O_NONBLOCK );
		    int socketSize = 1024*1024;
		    ret = setsockopt( con.fSocket, SOL_SOCKET, SO_RCVBUF, (char *)&socketSize, sizeof(int));
		    if ( ret < 0 )
			{
			LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Accept Routine", "Socket receive buffer size" )
			    << "Error setting sockets receive buffer size option: "
			    << strerror(errno) << " (" << MLUCLog::kDec << errno << ")." << ENDLOG;
			}
		    int one=1;
		    ret = setsockopt( con.fSocket, SOL_SOCKET, SO_KEEPALIVE, &one, sizeof(one) );
		    if ( ret )
			{
			LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Accept Routine", "SO_KEEPALIVE error" )
			    << "Error activating SO_KEEPALIVE option for new connection socket " << MLUCLog::kDec
			    << con.fSocket << ": " << strerror(errno) << " (" << errno << ")." << ENDLOG;
			}
		    ret = setsockopt( con.fSocket, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one) );
		    if ( ret )
			{
			LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Accept Routine", "Socket reuse addr" )
			    << "Error setting sockets options to reuse addr: "
			    << strerror(errno) << " (" << MLUCLog::kDec << errno << ")." << ENDLOG;
			}
		    fAcceptedSockets.insert( fAcceptedSockets.end(), con );
		    }
		else
		    {
		    // Connection was denied by parent object
		    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP", "Socket close" ) << "Closing Socket " << MLUCLog::kDec << con.fSocket << "." << ENDLOG;
		    close( con.fSocket );
		    }
		sock_count--;
		}
	    
	    // Now check all the connections themselves.
	    if ( sock_count>0 )
		{
		sock_iter = fAcceptedSockets.begin();
		sock_end = fAcceptedSockets.end();
		unsigned long sock_index = 1;
		while ( sock_iter != sock_end )
		    {
#ifdef TCPCOM_NO_POLL
		    if ( FD_ISSET( sock_iter->fSocket, &sockets ) )
#else
		    if ( poll_sockets[sock_index].revents & POLLIN )
#endif
			{
			// Some new data for us...
			bool readData = false;
			do
			    {
			    ret = NewData( *sock_iter );
			    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Accept Routine", "New Data" )
				<< "Port " << MLUCLog::kDec << fPortNr << " Socket " << sock_iter->fSocket 
				<< " NewData returned " << MLUCLog::kDec
				<< (int)ret << " (0x" << MLUCLog::kHex << (int)ret << ")." << ENDLOG;
			    if ( ret>0 )
				readData = true;
			    }
			while ( (int)ret > 0 );
			LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Accept Routine", "New Data loop left" )
			    << "Port " << MLUCLog::kDec << fPortNr << fPortNr << " Socket " 
			    << sock_iter->fSocket  << " NewData loop left (readData: " << (readData ? "yes" : "no" ) << ")." << ENDLOG;
			if ( ret < 0 || (ret==0 && !readData) )
			    {
			    // XXX
			    // ...
			    // An error occured, close socket later
			    closeSockets.insert( closeSockets.end(), sock_iter->fSocket );
			    }
			}
#ifdef TCPCOM_NO_POLL
		    if ( FD_ISSET( sock_iter->fSocket, &err_sockets ) )
#else
		    if ( poll_sockets[sock_index].revents & POLLERR )
#endif
			{
			// XXX
			// ...
			// An error occured, close socket later
			LOGC( gBCLLogLevelRef, MLUCLog::kInformational, "TCP Accept Routine", "Exception for socket" )
			    << "Port " << MLUCLog::kDec << fPortNr << fPortNr << " Socket " 
			    << sock_iter->fSocket  << " exception." << ENDLOG;
			closeSockets.insert( closeSockets.end(), sock_iter->fSocket );
			}
		    sock_iter++;
		    ++sock_index;
		    }
		}
	    }
	else if ( timeoutCons.size()>0 )
	    {
	    // Some timeout happened.
	    vector< vector<TAcceptedConnectionData>::iterator >::iterator timeoutIter, timeoutEnd;
	    timeoutIter = timeoutCons.begin();
	    timeoutEnd = timeoutCons.end();
	    while ( timeoutIter != timeoutEnd )
		{
		if ( !ConnectionTimeout( **timeoutIter ) )
		    {
		    // Abort connection. 
		    closeSockets.insert( closeSockets.end(), (*timeoutIter)->fSocket );
		    }
		timeoutIter++;
		}
	    }
	else 
	    {
	    // An error occured in the select call...
	    ret = errno;
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Accept Routine", "Error in select" )
		<< "Port " << MLUCLog::kDec << fPortNr << " (0x" << MLUCLog::kHex << fPortNr << "): An error occured in the select call: " << strerror(ret) << " ("
		<< MLUCLog::kDec << ret << ")." << ENDLOG;
	    bool found_err = false;
	    bool listen_sock_err = false;
	    if ( ret == EBADF )
		{
#ifdef TCPCOM_NO_POLL
		int err_highest_sock = -1;
		fd_set err_socks;
		struct timeval err_tv;
#endif
		sock_iter = fAcceptedSockets.begin();
		sock_end = fAcceptedSockets.end();
		while ( sock_iter != sock_end )
		    {
 		    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::Accept", "Checking socket" )
 			<< "Port " << MLUCLog::kDec << fPortNr << " (0x" << MLUCLog::kHex << fPortNr << ") checking socket " << MLUCLog::kDec << sock_iter->fSocket << "." << ENDLOG;
#ifdef TCPCOM_NO_POLL
		    FD_ZERO( &err_socks);
		    FD_SET( sock_iter->fSocket, &err_socks );
		    err_highest_sock = sock_iter->fSocket;
		    err_tv.tv_sec = 0;
		    err_tv.tv_usec = 0;
		    ret = select( err_highest_sock+1, &err_socks, NULL, NULL, &err_tv );
#else
		    pollfd err_poll_sock = { sock_iter->fSocket, POLLIN, 0 };
		    ret = poll( &err_poll_sock, 1, 0 );
#endif
		    if ( ret == -1 )
			{
			found_err = true;
			// XXX
			// ...
			// An error occured, close socket later
			LOGC( gBCLLogLevelRef, MLUCLog::kInformational, "TCP Accept Routine", "Invalid socket" )
			    << "Port " << MLUCLog::kDec << fPortNr << fPortNr << " Socket " 
			    << sock_iter->fSocket  << " invalid." << ENDLOG;
			closeSockets.insert( closeSockets.end(), sock_iter->fSocket );
			}
		    sock_iter++;
		    }
 		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::Accept", "Checking listen socket" )
 		    << "Port " << MLUCLog::kDec << fPortNr << " (0x" << MLUCLog::kHex << fPortNr << ") checking listen socket " << MLUCLog::kDec << fSocket << "." << ENDLOG;
#ifdef TCPCOM_NO_POLL
		FD_ZERO( &err_socks);
		FD_SET( fSocket, &err_socks );
		err_highest_sock = fSocket;
		err_tv.tv_sec = 0;
		err_tv.tv_usec = 0;
		ret = select( err_highest_sock+1, &err_socks, NULL, NULL, &err_tv );
#else
		pollfd err_poll_sock = { fSocket, POLLIN, 0 };
		ret = poll( &err_poll_sock, 1, 0 );
#endif
		if ( ret == -1 )
		    {
		    listen_sock_err = true;
		    // XXX
		    // ...
		    // An error occured, close socket later
		    LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "TCP Accept Routine", "Invalid listen socket" )
			<< "Port " << MLUCLog::kDec << fPortNr << fPortNr << " listen socket " 
			<< fSocket  << " invalid." << ENDLOG;
		    }
		}
	    if ( ret != EBADF || listen_sock_err )
		{
 		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Accept Routine", "Listen socket" )
 		    << "Port " << MLUCLog::kDec << fPortNr << " (0x" << MLUCLog::kHex << fPortNr << ") listen socket: " << MLUCLog::kDec << fSocket << "." << ENDLOG;
		select_error_cnt++;
		if ( select_error_cnt >= fSelectErrorLimit )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kFatal, "TCP Accept Routine", "Too many errors in select" )
			<< "Port " << MLUCLog::kDec << fPortNr << " (0x" << MLUCLog::kHex << fPortNr << "): Too many continuous select errors. Aborting Accept routine." 
			<< ENDLOG;
		    fQuitAccept = true;
		    }
		}
	    }
	cs_iter = closeSockets.begin();
	cs_end = closeSockets.end();
	while ( cs_iter != cs_end )
	    {
	    sock_iter = fAcceptedSockets.begin();
	    sock_end = fAcceptedSockets.end();
	    while ( sock_iter != sock_end )
		{
		if ( sock_iter->fSocket == *cs_iter )
		    {
 		    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLIntTCPComHelper::Accept", "Closing connection socket" )
 			<< "Port " << MLUCLog::kDec << fPortNr << " (0x" << MLUCLog::kHex << fPortNr << ") closing connection socket " << MLUCLog::kDec << sock_iter->fSocket << "." << ENDLOG;
		    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP", "Socket close" ) << "Closing Socket " << MLUCLog::kDec << sock_iter->fSocket << "." << ENDLOG;
		    close( sock_iter->fSocket );
		    ConnectionClosed( *sock_iter );
		    fAcceptedSockets.erase( sock_iter );
		    break;
		    }
		sock_iter++;
		}
	    cs_iter++;
	    }
	closeSockets.clear();
	}
    fAcceptQuitted = true;
    }

ssize_t BCLIntTCPComHelper::Read( const TAcceptedConnectionData& conPara, void *buf, size_t count, bool immediateRead )
    {
    TAcceptedConnectionData* con;
    con = (TAcceptedConnectionData*)&conPara;
#if __GNUC__>=3
    if ( con < fAcceptedSockets.begin().base() && con >= fAcceptedSockets.end().base() )
	return EINVAL;
#else
    if ( con < fAcceptedSockets.begin() && con >= fAcceptedSockets.end() )
	return EINVAL;
#endif
    size_t readBytes = 0;
    int ret;
    bool looped = false;
    do
	{
	if ( con->fBufferContentSize )
	    {
	    unsigned amount;
	    if ( count-readBytes <= con->fBufferContentSize )
		amount = count-readBytes;
	    else
		amount = con->fBufferContentSize;
	    if ( amount > gTCPMsgReadBufferSize )
		amount = gTCPMsgReadBufferSize;
	    memcpy( ((uint8*)buf)+readBytes, con->fReadBuffer + con->fBufferContentStart, amount );
	    con->fBufferContentStart += amount;
	    con->fBufferContentSize -= amount;
	    readBytes += amount;
	    if ( readBytes == count )
		return readBytes;
	    }
	if ( looped )
	    return readBytes;
	if ( count-readBytes <= gTCPMsgReadBufferSize )
	    looped = true;
	else
	    immediateRead = true;
	if ( immediateRead )
	    {
	    do
		{
		ret = read( con->fSocket, ((uint8*)buf)+readBytes, count-readBytes );
		}
	    while ( ret<0 && errno==EINTR );
	    if ( ret > 0 )
		readBytes += ret;
	    else
		{
		if ( !readBytes )
		    readBytes = ret;
		}
	    return readBytes;
	    }
	else
	    {
	    do
		{
		ret = read( con->fSocket, con->fReadBuffer, gTCPMsgReadBufferSize );
		}
	    while ( ret<0 && errno==EINTR );
	    if ( ret > 0 )
		{
		con->fBufferContentStart = 0;
		con->fBufferContentSize = ret;
		}
	    else
		{
		if ( !readBytes )
		    return ret;
		}
	    }
	}
    while ( 1 );
    }

ssize_t BCLIntTCPComHelper::ReadV( const TAcceptedConnectionData& conPara, const struct iovec *vector_orig, int count, bool immediateRead )
    {
    TAcceptedConnectionData* con;
    con = (TAcceptedConnectionData*)&conPara;
#if __GNUC__>=3
    if ( con < fAcceptedSockets.begin().base() && con >= fAcceptedSockets.end().base() )
	return EINVAL;
#else
    if ( con < fAcceptedSockets.begin() && con >= fAcceptedSockets.end() )
	return EINVAL;
#endif
    int ret;
    size_t totalToRead = 0;
    size_t totalRead = 0;
    int readBlocks=0;
    iovec io_vector[count];
    memcpy( io_vector, vector_orig, count*sizeof(struct iovec) );
    for ( int n = 0; n < count; n++ )
	totalToRead += io_vector[n].iov_len;
    size_t amount=0;
    size_t readBytes = 0;
    size_t readForBlock = 0;
    do
	{
	for ( int n = readBlocks; n < count && con->fBufferContentSize; n++ )
	    {
	    if ( io_vector[n].iov_len-readBytes <= con->fBufferContentSize )
		amount = io_vector[n].iov_len-readBytes;
	    else
		amount = con->fBufferContentSize;
	    if ( amount > gTCPMsgReadBufferSize )
		amount = gTCPMsgReadBufferSize;
	    memcpy( ((uint8*)io_vector[n].iov_base)+readBytes, con->fReadBuffer + con->fBufferContentStart, amount );
	    con->fBufferContentStart += amount;
	    con->fBufferContentSize -= amount;
	    totalRead += amount;
	    readForBlock += amount;
	    if ( totalRead == totalToRead )
		return totalRead;
#if 1
	    //#warning Following line is bug fix to be tested
	    if ( readForBlock == io_vector[n].iov_len )
#else // Old, non bug fix code
	    if ( amount == io_vector[n].iov_len )
#endif
		{
		readBlocks++;
		amount=0;
		readForBlock = 0;
		//#warning Following line is bug fix to be tested
		readBytes = 0;
		}
	    }
	if ( totalToRead-totalRead > gTCPMsgReadBufferSize )
	    immediateRead = true;
	if ( immediateRead )
	    {
	    readBytes=0;
	    do
		{
		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP read", "read offsets" )
		    << "Block " << MLUCLog::kDec << readBlocks << " start shifted by "
		    << readForBlock << " bytes." << ENDLOG;
		io_vector[readBlocks].iov_base = (void*)( ((uint8*)io_vector[readBlocks].iov_base)+readForBlock );
		io_vector[readBlocks].iov_len -= readForBlock;
		ret = readv( con->fSocket, io_vector+readBlocks, count-readBlocks );
		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP read", "read offsets" )
		    << "readv ret: " << MLUCLog::kDec << ret << "." << ENDLOG;
		io_vector[readBlocks].iov_base = (void*)( ((uint8*)io_vector[readBlocks].iov_base)-readForBlock );
		io_vector[readBlocks].iov_len += readForBlock;
		}
	    while ( ret<0 && errno==EINTR );
	    if ( ret > 0 )
		{
		totalRead += ret;
		while ( ret>0 )
		    {
		    if ( (size_t)ret>=io_vector[readBlocks].iov_len-readForBlock )
			{
			ret -= (io_vector[readBlocks].iov_len-readForBlock);
			readBlocks++;
			readForBlock = 0;
			}
		    else
			{
			readForBlock += ret;
			readBytes = readForBlock;
			ret = 0;
			}
		    amount=0;
		    }
		}
	    else
		{
		if ( !totalRead )
		    return ret;
		else
		    return totalRead;
		}
	    }
	else
	    {
	    readBytes = amount;
	    do
		{
		ret = read( con->fSocket, con->fReadBuffer, gTCPMsgReadBufferSize );
		}
	    while ( ret<0 && errno==EINTR );
	    if ( ret > 0 )
		{
		con->fBufferContentStart = 0;
		con->fBufferContentSize = ret;
		}
	    else
		{
		if ( !(totalRead) )
		    return ret;
		else
		    return totalRead;
		}
	    }
	}
    while ( totalRead < totalToRead );
    return totalRead;
    }




void BCLIntTCPComHelper::ConnectionBroken( const TTCPConnectionData* conData )
    {
    LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "BCLIntTCPComHelper::ConnectionBroken", "Connection broken" )
	<< "Connection on socket " << MLUCLog::kDec << conData->fSocket << " broken."
	<< ENDLOG;
    // First we check that the connection has really been established
    vector<TTCPConnectionData>::iterator iter, end;
    int oldSock = conData->fSocket;
    pthread_mutex_lock( &fConnectionMutex );
    iter = fConnections.begin();
    end = fConnections.end();
    while ( iter != end )
	{
	if ( iter->fSocket == conData->fSocket )
	    {
	    // We found it.
	    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP", "Socket close" ) << "Closing Socket " << MLUCLog::kDec << iter->fSocket << "." << ENDLOG;
	    close( iter->fSocket );
	    iter->fSocket = -1;
	    pthread_mutex_unlock( &fConnectionMutex );
	    return;
	    }
	iter++;
	}
    pthread_mutex_unlock( &fConnectionMutex );
    if ( iter != end )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "BCLIntTCPComHelper::ConnectionBroken", "Connection broken" )
	    << "Socket " << MLUCLog::kDec << oldSock << " resetted." << ENDLOG;
	}
    }

#if 0
void BCLIntTCPComHelper::SigPipeHandler( int signum )
#else
void BCLIntTCPComHelper::SigPipeHandler( int )
#endif
    {
    LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "BCLIntTCPComHelper::SigPipeHandler", "SIGPIPE" )
	<< "SIGPIPE signal received." << ENDLOG;
#if 0
    if ( fOldSigPipeHandler != NULL && fOldSigPipeHandler != SIG_DFL )
	(*fOldSigPipeHandler)( signum );
#endif
    }

int BCLIntTCPComHelper::GetProtocolByName(const char* name, int* proto) {
  int buflen=1024;
  struct protoent result_buf;
  struct protoent *result;
  char buf[buflen];

  int ret = getprotobyname_r(name, &result_buf, buf, buflen, &result);
  if ( ret == 0 && result) {
    *proto=result_buf.p_proto;
  } 
  else {
    *proto=-1;
  }
  return ret;
}


BCLIntTCPMsgComHelper::BCLIntTCPMsgComHelper( BCLTCPMsgCommunication* com ):
    BCLIntTCPComHelper( com )
    {
    fMsgCom = com;
    }

BCLIntTCPMsgComHelper::~BCLIntTCPMsgComHelper()
    {
    }

bool BCLIntTCPMsgComHelper::NewConnection( TAcceptedConnectionData& con )
    {
    if ( fMsgCom )
	return fMsgCom->NewConnection( con );
    else
	return false;
    }

int BCLIntTCPMsgComHelper::NewData( TAcceptedConnectionData& con )
    {
    if ( fMsgCom )
	return fMsgCom->NewData( con );
    else
	return false;
    }

bool BCLIntTCPMsgComHelper::ConnectionTimeout( TAcceptedConnectionData& con )
    {
    if ( fMsgCom )
	return fMsgCom->ConnectionTimeout( con );
    else
	return false;
    }

void BCLIntTCPMsgComHelper::ConnectionClosed( TAcceptedConnectionData& con )
    {
    if ( fMsgCom )
	fMsgCom->ConnectionClosed( con );
    }




BCLIntTCPBlobComHelper::BCLIntTCPBlobComHelper( BCLTCPBlobCommunication* com ):
    BCLIntTCPComHelper( com )
    {
    fBlobCom = com;
    }

BCLIntTCPBlobComHelper::~BCLIntTCPBlobComHelper()
    {
    }

bool BCLIntTCPBlobComHelper::NewConnection( TAcceptedConnectionData& con )
    {
    if ( fBlobCom )
	return fBlobCom->NewConnection( con );
    else
	return false;
    }

int BCLIntTCPBlobComHelper::NewData( TAcceptedConnectionData& con )
    {
    if ( fBlobCom )
	return fBlobCom->NewData( con );
    else
	return false;
    }

bool BCLIntTCPBlobComHelper::ConnectionTimeout( TAcceptedConnectionData& con )
    {
    if ( fBlobCom )
	return fBlobCom->ConnectionTimeout( con );
    else
	return false;
    }

void BCLIntTCPBlobComHelper::ConnectionClosed( TAcceptedConnectionData& con )
    {
    if ( fBlobCom )
	fBlobCom->ConnectionClosed( con );
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
