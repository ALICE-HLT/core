/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#ifdef USE_TCP

//#define TCP_NO_SIMPLE_TRANSFER_COMPLETE


#include "BCLTCPBlobCommunication.hpp"
#include "BCLMsgCommunication.hpp"
#include "BCLTCPComID.hpp"
#include "BCLTCPAddressOps.hpp"
#include "BCLIntTCPComHelper.hpp"
#include "BCLIntTCPBlobComMsg.hpp"
#include "BCLIntTCPBlobComHeader.hpp"
#include "BCLAddressLogger.hpp"
#include "MLUCTracebackBuffer.hpp"
#include <errno.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/tcp.h>
#include <poll.h>

#ifdef TCP_SIMPLE_TIMEOUT
#define SIMPLE_TIMEOUT_MAX 1000
#endif




//#define DO_TIMEMARKS
#ifdef DO_TIMEMARKS
#define DECL_TIMEMARKS(cnt)  const unsigned timeMarkCnt = cnt;unsigned timeMark = 0;struct timeval timeMarks[ timeMarkCnt ];int timeMarkLines[ timeMarkCnt ];
#define TIMEMARK             if ( timeMark < timeMarkCnt ) { gettimeofday( timeMarks+timeMark, NULL ); timeMarkLines[timeMark]= __LINE__; timeMark++; }
#define DUMP_TIMEMARKS 	{\
            unsigned long td;for ( unsigned tmn = 0; tmn < timeMark; tmn++ ){\
	    if ( tmn<=0 )\
		{\
		LOGC( gBCLLogLevelRef, MLUCLog::kBenchmark, "BCLTCPBlobCommunication::NewData", "Timemarks" )\
		    << "Timemark " << MLUCLog::kDec << MLUCLog::kPrec << 3 << tmn\
		    << " (line " << MLUCLog::kPrec << 0 << timeMarkLines[tmn] << "): "\
                    << (timeMarks+tmn)->tv_sec << "."\
		    << MLUCLog::kPrec << 6\
		    << (timeMarks+tmn)->tv_usec << " s." << ENDLOG;\
		}\
	    else\
		{\
		td = ((timeMarks+tmn)->tv_sec - (timeMarks+tmn-1)->tv_sec)*1000000 + ((timeMarks+tmn)->tv_usec-(timeMarks+tmn-1)->tv_usec);\
		LOGC( gBCLLogLevelRef, MLUCLog::kBenchmark, "BCLTCPBlobCommunication::NewData", "Timemarks" )\
		    << "Timemark " << MLUCLog::kDec << MLUCLog::kPrec << 3 << tmn\
		    << " (line " << MLUCLog::kPrec << 0 << timeMarkLines[tmn] << "): "\
		    << (timeMarks+tmn)->tv_sec << "."\
		    << MLUCLog::kPrec << 6\
		    << (timeMarks+tmn)->tv_usec << " s - timediff.: " << MLUCLog::kPrec << 0\
		    << td << " usec." << ENDLOG;\
		}\
	    }\
	td = ((timeMarks+timeMark-1)->tv_sec - (timeMarks)->tv_sec)*1000000 + ((timeMarks+timeMark-1)->tv_usec-(timeMarks)->tv_usec);\
		LOGC( gBCLLogLevelRef, MLUCLog::kBenchmark, "BCLTCPBlobCommunication::NewData", "Timemarks" )\
		    << "Total timediff. for " << MLUCLog::kDec << timeMark << "timemarks: " << MLUCLog::kPrec << 0\
		    << td << " usec." << ENDLOG;\
	}\

#else
#define TIMEMARK
#define DECL_TIMEMARKS(cnt)
#define DUMP_TIMEMARKS
#endif




//const int kBlobMsg = 0x8000;
const int kTCPBlobQueryAddressMsg = kBlobMsg+1;
const int kTCPBlobAddressMsg = kBlobMsg+2;
const int kTCPBlobQueryFreeBlockMsg = kBlobMsg+3;
const int kTCPBlobFreeBlockMsg = kBlobMsg+4;
const int kTCPBlobConnectRequestMsg = kBlobMsg+5;
const int kTCPBlobDisconnectMsg = kBlobMsg+6;
const int kTCPBlobQuitMsg = kBlobMsg+7;
const int kTCPBlobQueryBlobSizeMsg = kBlobMsg+8;
const int kTCPBlobBlobSizeReplyMsg = kBlobMsg+9;

BCLTCPBlobCommunication::BCLTCPBlobCommunication( int slotCntExp2 ):
    fComHelper( this ),
#ifndef DEBUG_NOTHREADS
    fRequestThread( this, &BCLTCPBlobCommunication::RequestHandler ),
#endif
    fReceivedBlobs( 8, true ),
    fMsgs( slotCntExp2, true ),
#ifndef COMPATIBILITY_TRANSFER_MODE
    fHeaderAllocCache( 3, true, false ),
#endif
    fBlobProgress( slotCntExp2, true )
    {
    BCLTCPAddress addr;
    fTCPAddress = *addr.GetData();
    fQuitRequestHandler = false;
    fRequestHandlerQuitted = true;
//     pthread_mutex_init( &fMsgMutex, NULL );
    pthread_mutex_init( &fBlockMutex, NULL );
    fCurrentQueryID = 0;
    fCurrentQueryCount = 0;
    fDataArrivedSignal.Unlock();
    fTransferBlockSize = TCPBLOB_DEFAULT_TRANSFERBLOCKSIZE;
    fQuitReceivedBlobWait = false;
    fBlobReceivedSignal.Unlock();
    }

BCLTCPBlobCommunication::BCLTCPBlobCommunication( BCLMsgCommunication* msgCom, int slotCntExp2 ):
    BCLBlobCommunication( msgCom ), fComHelper( this ),
#ifndef DEBUG_NOTHREADS
    fRequestThread( this, &BCLTCPBlobCommunication::RequestHandler ),
#endif
    fReceivedBlobs( 8, true ),
    fMsgs( slotCntExp2, true ),
#ifndef COMPATIBILITY_TRANSFER_MODE
    fHeaderAllocCache( 3, true, false ),
#endif
    fBlobProgress( slotCntExp2, true )
    {
    BCLTCPAddress addr;
    fTCPAddress = *addr.GetData();
    fQuitRequestHandler = false;
    fRequestHandlerQuitted = true;
//     pthread_mutex_init( &fMsgMutex, NULL );
    pthread_mutex_init( &fBlockMutex, NULL );
    fCurrentQueryID = 0;
    fCurrentQueryCount = 0;
    fDataArrivedSignal.Unlock();
    fTransferBlockSize = TCPBLOB_DEFAULT_TRANSFERBLOCKSIZE;
    fQuitReceivedBlobWait = false;
    fBlobReceivedSignal.Unlock();
    if ( slotCntExp2>=0 && slotCntExp2<32 )
	{
	fFreeBlocks.reserve( 1<<slotCntExp2 );
	fUsedBlocks.reserve( 1<<slotCntExp2 );
	}
    }

BCLTCPBlobCommunication::~BCLTCPBlobCommunication()
    {
    if ( fComHelper.fSocket != -1 )
	Unbind();
    }

int BCLTCPBlobCommunication::Bind( BCLAbstrAddressStruct* address )
    {
    int ret;
    if ( !address )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob Bind", "Address error" )
	    << "Wrong address (NULL pointer) in TCP Blob Bind" << ENDLOG;
	BindError( EFAULT, this, address, NULL );
	return EFAULT;
	}
    if ( address->fComID != kTCPComID )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob Bind", "Address error" )
	    << "Wrong address ID " << address->fComID << " in TCP Blob Bind. (expected " 
	    << kTCPComID << ")."<< ENDLOG;
	BindError( EINVAL, this, address, NULL );
	return EINVAL;
	}
    if ( !fMsgCom )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob Bind", "No msg communication" )
	    << "No message communication object configured in TCP Blob Bind" << ENDLOG;
	BindError( ENODEV, this, address, NULL );
	return ENODEV;
	}
    fTCPAddress = *(BCLTCPAddressStruct*)address;
    fAddress = &fTCPAddress;
    // We use the first page for storing our header/control structure information...
    ret = fComHelper.Bind( (BCLTCPAddressStruct*)fAddress );
    if ( !ret )
	{
	ret = pthread_mutex_lock( &fBlockMutex );
	if ( ret )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::Bind", "Block mutex" )
		<< "Error attempting to lock block mutex. Reason: " 
		<< strerror(ret) << " (" << MLUCLog::kDec << ret << "). Continuing..." << ENDLOG;
	    }
	TTCPBlockData block;
	block.fBlockOffset = 0;
	block.fBlockSize = fBlobBufferSize;
	fFreeBlocks.insert( fFreeBlocks.begin(), block );
	ret = pthread_mutex_unlock( &fBlockMutex );
	if ( ret )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::Bind", "Block mutex" )
		<< "Error attempting to unlock block mutex. Reason: " 
		<< strerror(ret) << " (" << MLUCLog::kDec << ret << "). Continuing..." << ENDLOG;
	    }
#ifndef DEBUG_NOTHREADS
	fRequestThread.Start();
#endif
	}
    return ret;
    }

int BCLTCPBlobCommunication::Unbind()
    {
#ifndef DEBUG_NOTHREADS
    QuitRequestHandler();
#endif
    return fComHelper.Unbind();
    }

int BCLTCPBlobCommunication::Connect( BCLAbstrAddressStruct* address, bool openOnDemand )
    {
    return Connect( address, NULL, openOnDemand, false, 0 );
    }

int BCLTCPBlobCommunication::Connect( BCLAbstrAddressStruct* address, unsigned long timeout_ms, bool openOnDemand )
    {
    return Connect( address, NULL, openOnDemand, true, timeout_ms );
    }

int BCLTCPBlobCommunication::Disconnect( BCLAbstrAddressStruct* address )
    {
    return Disconnect( address, NULL, true, false, 0 );
    }

int BCLTCPBlobCommunication::Disconnect( BCLAbstrAddressStruct* address, unsigned long timeout_ms )
    {
    return Disconnect( address, NULL, true, true, timeout_ms );
    }

int BCLTCPBlobCommunication::InterruptBlobConnection( BCLAbstrAddressStruct* msgAddress )
    {
    if ( !msgAddress )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob Connection Interrupt", "Address error" )
	    << "Wrong address (NULL pointer) in TCP Blob Connection Interrupt" << ENDLOG;
	ConnectionError( EFAULT, this, msgAddress );
	return EFAULT;
	}
    if ( !fMsgCom )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob Connection Interrupt", "No msg communication" )
	    << "No message communication object configured in TCP Blob Connection Interrupt" << ENDLOG;
	ConnectionError( ENODEV, this, msgAddress );
	return ENODEV;
	}
    int ret;
    BCLTCPAddressStruct remoteBlobAddress;
    ret = GetRemoteBlobAddress( msgAddress, &remoteBlobAddress, true, 0 );
    if ( ret )
	{
	ConnectionError( ret, this, msgAddress );
	return ret;
	}

    BCLIntTCPComHelper::TTCPConnectionData condat;
    BCLIntTCPComHelper::TTCPConnectionData* connectData = &condat;
    condat.fAddress = remoteBlobAddress;
    ret = fComHelper.ConnectToRemote( &remoteBlobAddress, connectData, false, true, 0 );
    if ( ret )
	{
	return ret;
	}
    close( connectData->fSocket );
    return fComHelper.DisconnectFromRemote( connectData, true, 0 );
    }


bool BCLTCPBlobCommunication::CanLockConnection()
    {
    return false;
    }

int BCLTCPBlobCommunication::LockConnection( BCLAbstrAddressStruct* )
    {
    return ENOTSUP;
    }

int BCLTCPBlobCommunication::LockConnection( BCLAbstrAddressStruct*, unsigned long )
    {
    return ENOTSUP;
    }

int BCLTCPBlobCommunication::UnlockConnection( BCLAbstrAddressStruct* )
    {
    return ENOTSUP;
    }

int BCLTCPBlobCommunication::UnlockConnection( BCLAbstrAddressStruct*, unsigned long )
    {
    return ENOTSUP;
    }

int BCLTCPBlobCommunication::Disconnect( BCLAbstrAddressStruct* msgAddress, BCLTCPAddressStruct* blobAddress, bool initiator, bool useTimeout, unsigned long timeout_ms )
    {
    if ( !msgAddress )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob Disconnect", "Address error" )
	    << "Wrong address (NULL pointer) in TCP Blob Disconnect" << ENDLOG;
	ConnectionError( EFAULT, this, msgAddress );
	return EFAULT;
	}
    if ( !fMsgCom )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob Disconnect", "No msg communication" )
	    << "No message communication object configured in TCP Blob Disconnect" << ENDLOG;
	ConnectionError( ENODEV, this, msgAddress );
	return ENODEV;
	}
    int ret, ret1;
    BCLTCPAddressStruct remoteBlobAddress;
    if ( !blobAddress )
	{
	ret = GetRemoteBlobAddress( msgAddress, &remoteBlobAddress, useTimeout, timeout_ms );
	if ( ret )
	    {
	    ConnectionError( ret, this, msgAddress );
	    return ret;
	    }
	blobAddress = &remoteBlobAddress;
	}
    ret1 = fComHelper.Disconnect( blobAddress, useTimeout, timeout_ms );
    if ( initiator )
	{
	BCLIntTCPBlobComMsg msg;
	msg.GetData()->fMsgType = kTCPBlobDisconnectMsg;
	msg.GetData()->fIPNr = fTCPAddress.fIPNr;
	msg.GetData()->fPortNr = fTCPAddress.fPortNr;
	GetQueryID( msg.GetData()->fQueryID );
	fMsgCom->Send( msgAddress, msg.GetData() );
	LOGC( gBCLLogLevelRef, MLUCLog::kInformational, "TCP Blob Disconnect", "Sent disconnect" )
	    << "Disconnect message sent." << ENDLOG;
	}
    if ( useTimeout )
	ret = fMsgCom->Disconnect( msgAddress, timeout_ms );
    else
	ret = fMsgCom->Disconnect( msgAddress );
    vector<TTCPRemoteBlobAddress>::iterator iter, end;
    MLUCMutex::TLocker remoteBlobAddressLock( fRemoteBlobAddressMutex );
    iter = fRemoteBlobAddresses.begin();
    end = fRemoteBlobAddresses.end();
    while ( iter != end )
	{
	if ( fMsgCom->AddressEquality( iter->fMsgAddress, msgAddress ) || 
	   iter->fBlobAddress == remoteBlobAddress )
	    {
	    fMsgCom->DeleteAddress( iter->fMsgAddress );
	    fRemoteBlobAddresses.erase( iter );
	    break;
	    }
	iter++;
	}
    if ( ret1 )
	return ret1;
    if ( ret )
	return ret;
    return 0;
    }




void BCLTCPBlobCommunication::SetMsgCommunication( BCLMsgCommunication* msgCom )
    {
    uint32 cc = fCurrentQueryCount;
    if ( cc>0 )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::SetMsgCommunication", "Msg communication busy" )
	    << "Current Message communication busy. " << cc << " queries outstanding." << ENDLOG;
	GeneralError( EBUSY, this, NULL );
	return;
	}
    fMsgCom = msgCom;
    }


uint64 BCLTCPBlobCommunication::GetRemoteBlobSize( BCLAbstrAddressStruct* msgAddress, unsigned long timeout_ms )
    {
    if ( !msgAddress )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Get Remote Blob Size", "Address error" )
	    << "Wrong address (NULL pointer) in TCP Get Remote Blob Size" << ENDLOG;
	BlobPrepareError( EFAULT, this, msgAddress );
	return (BCLTransferID)-1;
	}
    if ( !fMsgCom )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Get Remote Blob Size", "No msg communication" )
	    << "No message communication object configured in TCP Get Remote Blob Size" << ENDLOG;
	BlobPrepareError( ENODEV, this, msgAddress );
	return (BCLTransferID)-1;
	}
    int ret;
    BCLIntTCPBlobComMsgStruct* msg;
    BCLIntTCPBlobComMsg msgClass;
    msg = msgClass.GetData();
    msg->fMsgType = kTCPBlobQueryBlobSizeMsg;
    ret = GetQueryID( msg->fQueryID );
    if ( ret )
	{
	BlobPrepareError( ret, this, msgAddress );
	return (BCLTransferID)-1;
	}
    fDataArrivedSignal.Lock();
    if ( timeout_ms )
	ret = fMsgCom->Send( msgAddress, msg, timeout_ms );
    else
	ret = fMsgCom->Send( msgAddress, msg );
    if ( ret )
	{
	fDataArrivedSignal.Unlock();
	BlobPrepareError( ret, this, msgAddress );
	return (BCLTransferID)-1;
	}
    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::GetRemoteBlobSize", "Query blob size" )
	<< "Query Blob Size message sent." << ENDLOG;
    ret = WaitForMessage( msg->fQueryID, msg, timeout_ms, timeout_ms );
    fDataArrivedSignal.Unlock();
    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::GetRemoteBlobSize", "Blob Size" )
	<< "Received blob size " << msg->fBlockSize << "." << ENDLOG;
    if ( ret )
	return (uint32)0;
    return msg->fBlockSize;
    }

BCLTransferID BCLTCPBlobCommunication::PrepareBlobTransfer( BCLAbstrAddressStruct* msgAddress, uint64 size,
							    unsigned long timeout )
    {
    DECL_TIMEMARKS(30);
    TIMEMARK;
    if ( !msgAddress )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Prepare Blob Transfer", "Address error" )
	    << "Wrong address (NULL pointer) in TCP Prepare Blob Transfer" << ENDLOG;
	BlobPrepareError( EFAULT, this, msgAddress );
	return (BCLTransferID)-1;
	}
    if ( !fMsgCom )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Prepare Blob Transfer", "No msg communication" )
	    << "No message communication object configured in TCP Prepare Blob Transfer" << ENDLOG;
	BlobPrepareError( ENODEV, this, msgAddress );
	return (BCLTransferID)-1;
	}
    int ret;
//     BCLTCPAddressStruct remoteBlobAddress;
//     ret = GetRemoteBlobAddress( msgAddress, &remoteBlobAddress );
//     if ( ret )
// 	{
// 	BlobPrepareError( ret, this, msgAddress );
// 	return (BCLTransferID)-1;
// 	}
    TIMEMARK;
    BCLIntTCPBlobComMsgStruct* msg;
    BCLIntTCPBlobComMsg msgClass;
    msg = msgClass.GetData();
    msg->fMsgType = kTCPBlobQueryFreeBlockMsg;
    msg->fBlockSize = size;
    ret = GetQueryID( msg->fQueryID );
    if ( ret )
	{
	BlobPrepareError( ret, this, msgAddress );
	return (BCLTransferID)-1;
	}
    TIMEMARK;
    fDataArrivedSignal.Lock();
    if ( timeout )
	ret = fMsgCom->Send( msgAddress, msg, timeout );
    else
	ret = fMsgCom->Send( msgAddress, msg );
    if ( ret )
	{
	fDataArrivedSignal.Unlock();
	BlobPrepareError( ret, this, msgAddress );
	return (BCLTransferID)-1;
	}
    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::PrepareBlobTransfer", "Query free block" )
	<< "Query Free Block message sent." << ENDLOG;
    TIMEMARK;
    ret = WaitForMessage( msg->fQueryID, msg, timeout, timeout );
    TIMEMARK;
    fDataArrivedSignal.Unlock();
    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::PrepareBlobTransfer", "Blob transfer id" )
	<< "Received blob transfer id " << msg->fBlockOffset << "." << ENDLOG;
    TIMEMARK;
    DUMP_TIMEMARKS;
    if ( ret )
	return (BCLTransferID)-1;
    if ( msg->fBlockOffset == (uint32)-1 )
	return (BCLTransferID)-1;
    return (BCLTransferID)msg->fBlockOffset;
    }

int BCLTCPBlobCommunication::TransferBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, uint8* data,
					   uint64 offset, uint64 size, unsigned long timeout,
					   BCLErrorCallback *callback )
    {
    vector<uint8*> datas;
    vector<uint64> offsets;
    vector<uint64> sizes;
    datas.insert( datas.end(), data );
    offsets.insert( offsets.end(), offset );
    sizes.insert( sizes.end(), size );
    return TransferBlob( true, msgAddress, id, datas, offsets, sizes, timeout, callback );
    }

int BCLTCPBlobCommunication::TransferBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, vector<uint8*> datas,
					   vector<uint64> offsets, vector<uint64> sizes, unsigned long timeout,
					   BCLErrorCallback *callback )
    {
    return TransferBlob( true, msgAddress, id, datas,
			 offsets, sizes, timeout,
			 callback );
    }

int BCLTCPBlobCommunication::PushBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, uint8* data,
					   uint64 offset, uint64 size, unsigned long timeout,
					   BCLErrorCallback *callback )
    {
    vector<uint8*> datas;
    vector<uint64> offsets;
    vector<uint64> sizes;
    datas.insert( datas.end(), data );
    offsets.insert( offsets.end(), offset );
    sizes.insert( sizes.end(), size );
    return TransferBlob( false, msgAddress, id, datas, offsets, sizes, timeout, callback );
    }

int BCLTCPBlobCommunication::PushBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, vector<uint8*> datas,
					   vector<uint64> offsets, vector<uint64> sizes, unsigned long timeout,
					   BCLErrorCallback *callback )
    {
    return TransferBlob( false, msgAddress, id, datas,
			 offsets, sizes, timeout,
			 callback );
    }

int BCLTCPBlobCommunication::TransferBlob( bool waitForCompletion, BCLAbstrAddressStruct* msgAddress, BCLTransferID id, vector<uint8*> datas,
					   vector<uint64> offsets, vector<uint64> sizes, unsigned long timeout,
					   BCLErrorCallback *callback )
    {
    DECL_TIMEMARKS(30);
    TIMEMARK;
    int retval = 0;
    if ( id == (BCLTransferID)-1 )
	{
#if 0
	LOG( MLUCLog::kError, "BCLTCPBlobCommunication::TransferBlob", "EINVAL 1" )
	    << "EINVAL 1." << ENDLOG;
#endif
	BlobSendError( EINVAL, this, msgAddress, id, callback );
	return EINVAL;
	}
    if ( !msgAddress )
	{
	AddressError( EFAULT, this, msgAddress, callback );
	return EFAULT;
	}
    if ( !fMsgCom )
	{
	BlobSendError( ENODEV, this, msgAddress, id, callback );
	return ENODEV;
	}
    if ( !sizes.size() )
	{
	return 0;
	}
    if ( sizes.size()!=datas.size() )
	{
#if 0
	LOG( MLUCLog::kError, "BCLTCPBlobCommunication::TransferBlob", "EINVAL 2" )
	    << "EINVAL 2." << ENDLOG;
#endif
	BlobSendError( EINVAL, this, msgAddress, id, callback );
	return EINVAL;
	}
    TIMEMARK;
    int ret;
#ifdef TCP_SIMPLE_TIMEOUT
    unsigned long timeoutCount=0;
#endif
    BCLTCPAddressStruct remoteBlobAddress;
    ret = GetRemoteBlobAddress( msgAddress, &remoteBlobAddress, timeout, timeout );
    if ( ret )
	{
	BlobSendError( ret, this, msgAddress, id, callback );
	return ret;
	}
    BCLIntTCPComHelper::TTCPConnectionData condat;
    BCLIntTCPComHelper::TTCPConnectionData* connectData = &condat;
    condat.fAddress = remoteBlobAddress;
    TIMEMARK;
    ret = fComHelper.ConnectToRemote( &remoteBlobAddress, connectData, true, timeout, timeout );
    if ( ret )
	{
	BlobSendError( ret, this, msgAddress, id, callback );
	fComHelper.DisconnectFromRemote( connectData, timeout, timeout );
	return ret;
	}
    TIMEMARK;
    vector<uint8*>::iterator dataIter, dataEnd;
    vector<uint64>::iterator sizeIter, sizeEnd;
    vector<uint64>::iterator offsIter, offsEnd;
#ifdef TCPCOM_NO_POLL
    fd_set sockets;
#endif
#ifdef TCPCOM_NO_POLL
    struct timeval tv, *ptv;
#endif
    dataIter = datas.begin();
    dataEnd = datas.end();
    sizeIter = sizes.begin();
    sizeEnd = sizes.end();
    offsIter = offsets.begin();
    offsEnd = offsets.end();
    struct timeval oldTimeout;
    if ( timeout )
	{
	socklen_t sockoptlen = sizeof(oldTimeout);
	ret = getsockopt( connectData->fSocket, SOL_SOCKET, SO_SNDTIMEO, &oldTimeout, &sockoptlen );
	if ( ret==-1 )
	    {
	    ret = errno;
	    LOGCG( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::Transferblob", "Socket Timeout Get Error", tmpLog )
		<< "Error getting TCP send timeout from socket while trying to do transfer " << MLUCLog::kDec << id 
		<< " to message address ";
	    BCLAddressLogger::LogAddress( tmpLog, *msgAddress ) << " / blob address ";
	    BCLAddressLogger::LogAddress( tmpLog, remoteBlobAddress ) 
		<< ": " << strerror(ret) << " (" 
		<< MLUCLog::kDec << ret << ")." << ENDLOG;
	    }
	else
	    {
	    struct timeval newTimeout;
	    newTimeout.tv_sec = timeout/1000;
	    newTimeout.tv_usec = (timeout-(newTimeout.tv_sec*1000))*1000;
	    ret = setsockopt( connectData->fSocket, SOL_SOCKET, SO_SNDTIMEO, &newTimeout, sizeof(newTimeout) );
	    if ( ret==-1 )
		{
		ret = errno;
		LOGCG( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::Transferblob", "Socket Timeout Set Error", tmpLog )
		    << "Error getting TCP send timeout from socket while trying to do transfer " << MLUCLog::kDec << id 
		    << " to message address ";
		BCLAddressLogger::LogAddress( tmpLog, *msgAddress ) << " / blob address ";
		BCLAddressLogger::LogAddress( tmpLog, remoteBlobAddress ) 
		    << ": " << strerror(ret) << " (" 
		    << MLUCLog::kDec << ret << ")." << ENDLOG;
		}
	    }
	}


    BCLIntTCPBlobComHeader comHeaderClass;
    uint32 blockCnt = sizes.size();
    uint32 blockCntWr = htonl(blockCnt);
    uint32 completionIndicator = (uint32)waitForCompletion;
    BCLIntTCPBlobComHeaderStruct comHeaders[ blockCnt ];
    uint32 offset = 0;
    for ( unsigned n = 0; n < blockCnt; n++ )
	{
	comHeaders[n] = *(comHeaderClass.GetData());
	comHeaders[n].fTransferID = id;
	if ( n<offsets.size() )
	    offset = comHeaders[n].fBlockOffset = offsets[n];
	else
	    comHeaders[n].fBlockOffset = offset;
	comHeaders[n].fBlockSize = sizes[n];
	offset += sizes[n];
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::TransferBlob", "Transfer block" )
	    //LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "BCLTCPBlobCommunication::TransferBlob", "Transfer block" )
	    << "Transfer " << MLUCLog::kDec << id << " (0x" 
	    << MLUCLog::kHex << id << ") block offset: " << MLUCLog::kDec
	    << comHeaders[n].fBlockOffset << " (0x" << MLUCLog::kHex << comHeaders[n].fBlockOffset
	    << " - block size: " << MLUCLog::kDec << comHeaders[n].fBlockSize << " (0x" 
	    << MLUCLog::kDec << comHeaders[n].fBlockSize << ")." << ENDLOG;
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::TransferBlob", "Transfer block" )
	    //LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "BCLTCPBlobCommunication::TransferBlob", "Transfer block" )
	    << "Header length: " << MLUCLog::kDec << comHeaders[n].fLength
	    << " (0x" << MLUCLog::kHex << comHeaders[n].fLength << ")"
	    << "." << ENDLOG;
	}
    
    unsigned nextBuffer = 0;
    uint64 szWritten = 0;
    uint64 toWrite[ blockCnt+3 ];
    toWrite[0] = sizeof(blockCnt);
    toWrite[1] = toWrite[0]+sizeof(completionIndicator);
    toWrite[2] = toWrite[1]+sizeof(BCLIntTCPBlobComHeaderStruct)*blockCnt;
    for ( unsigned ic=0; ic<blockCnt; ic++ )
	{
	toWrite[ic+3] = toWrite[ic+2]+sizes[ic];
	}
    uint64 totalToWrite = toWrite[blockCnt+2];
    bool retry=false;
    //struct timeval *ptv, tv, s, e;
    unsigned tempCount, bi, tempOffset;
    struct iovec ioVecs[blockCnt+2];


    while ( szWritten < totalToWrite )
	{
	for ( unsigned ic=0; ic < blockCnt ; ic++ )
	    {
	    if ( comHeaders[ic].fLength != comHeaderClass.GetData()->fLength )
		{
		LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::TransferBlob", "Transfer block" )
		    << "comHeaders[" << MLUCLog::kDec << ic << "] length is unequal header class length: "
		    << comHeaders[ic].fLength << " != " << comHeaderClass.GetData()->fLength
		    << MLUCLog::kHex << " (0x" << comHeaders[ic].fLength << " != 0x" << comHeaderClass.GetData()->fLength
		    << ")." << ENDLOG;
		}
	    }
	retry = true;
	retval=0;
	do
	    {
	    tempCount=0;
	    bi = nextBuffer;
	    if ( bi>0 )
		tempOffset = szWritten-toWrite[bi-1];
	    else
		tempOffset = szWritten;
	    if ( bi==0 )
		{
// 		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP blob send", "Buffer offsets" )
// 		    << "Block count (" << MLUCLog::kDec << tempCount << ") is being written from offset "
// 		    << tempOffset 
// 		    << " - Length to be written: " << sizeof(blockCnt)-tempOffset << " of "
// 		    << sizeof(blockCnt) << " bytes." << ENDLOG;
		ioVecs[0].iov_base = ((uint8*)&blockCntWr)+tempOffset;
		ioVecs[0].iov_len = sizeof(blockCntWr)-tempOffset;
		tempCount = 1;
		bi++;
		tempOffset = 0;
		}
	    if ( bi==1 )
		{
// 		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP blob send", "Buffer offsets" )
// 		    << "Completion indicator (" << MLUCLog::kDec << tempCount << ") is being written from offset "
// 		    << tempOffset 
// 		    << " - Length to be written: " << sizeof(completionIndicator)-tempOffset << " of "
// 		    << sizeof(completionIndicator) << " bytes." << ENDLOG;
		ioVecs[tempCount].iov_base = ((uint8*)&completionIndicator)+tempOffset;
		ioVecs[tempCount].iov_len = sizeof(completionIndicator)-tempOffset;
		tempCount++;
		bi++;
		tempOffset = 0;
		}
	    if ( bi==2 )
		{
// 		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP blob send", "Buffer offsets" )
// 		    << "Block headers (" << MLUCLog::kDec << tempCount << ") are being written from offset "
// 		    << tempOffset 
// 		    << " - Length to be written: " << sizeof(BCLIntTCPBlobComHeaderStruct)*blockCnt-tempOffset << " of "
// 		    << sizeof(BCLIntTCPBlobComHeaderStruct)*blockCnt << " bytes." << ENDLOG;
		ioVecs[tempCount].iov_base = ((uint8*)comHeaders)+tempOffset;
		ioVecs[tempCount].iov_len = sizeof(BCLIntTCPBlobComHeaderStruct)*blockCnt-tempOffset;
		tempCount++;
		bi++;
		tempOffset = 0;
		}
	    while ( bi<blockCnt+3 )
		{
// 		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP blob send", "Buffer offsets" )
// 		    << "Block " << MLUCLog::kDec << (unsigned)(bi-3) << "(" << tempCount << ") is being written from offset "
// 		    << (unsigned)(datas[bi-3]) << " + " << tempOffset 
// 		    << " = " << (unsigned)(datas[bi-3]+tempOffset)
// 		    << " - Length to be written: " << (unsigned)(sizes[bi-3]-tempOffset) << " of "
// 		    << (unsigned)(sizes[bi-3]) << " bytes." << ENDLOG;
		ioVecs[tempCount].iov_base = datas[bi-3]+tempOffset;
		ioVecs[tempCount].iov_len = sizes[bi-3]-tempOffset;
		tempCount++;
		bi++;
		tempOffset = 0;
		}
	    ret = writev( connectData->fSocket, ioVecs, tempCount );
	    if ( ret > 0 )
		{
		szWritten += ret;
		if ( szWritten<totalToWrite )
		    {
		    while ( nextBuffer<blockCnt+3 && toWrite[nextBuffer]<=szWritten )
			nextBuffer++;
		    }
		}
	    if ( ret<0 && errno == EPIPE )
		{
		fComHelper.ConnectionBroken( connectData );
		retval = EPIPE;
		break;
		}
	    if ( ret < 0 && errno!=EAGAIN && errno!=EINTR )
		{
		LOGCG( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::TransferBlob", "Write error", tmpLog )
		    << "Error on writev call for transfer " << MLUCLog::kDec << id 
		    << " to message address ";
		BCLAddressLogger::LogAddress( tmpLog, *msgAddress ) << " / blob address ";
		BCLAddressLogger::LogAddress( tmpLog, remoteBlobAddress ) 
		    << ": " << strerror(errno) << " (" 
		    << MLUCLog::kDec << errno << ")." << ENDLOG;
		BlobSendError( EIO, this, &remoteBlobAddress, id, callback );
		retval = EIO;
		break;
		}
	    if ( ret==0 || (ret < 0 && errno==EAGAIN) )
		retry = false;
	    }
	while ( retry && szWritten < totalToWrite );
	if ( szWritten >= totalToWrite )
	    break;
#if 0
	if ( retval==EPIPE ) // XXX
	    {
	    LOG( MLUCLog::kError, "BCLTCPBlobCommunication::Transferblob", "Pipe error" )
		<< "Pipe error" << ENDLOG;
#else
	if ( retval!=0 )
	    {
	    LOG( MLUCLog::kError, "BCLTCPBlobCommunication::Transferblob", "Communication error" )
		<< "Communication error. Attempting recovery" << ENDLOG;
#endif
	    fComHelper.DisconnectFromRemote( connectData, timeout, timeout, true ); // Force socket close
	    ret = fComHelper.ConnectToRemote( &remoteBlobAddress, connectData, true, timeout, timeout );
	    if ( ret != 0 )
		{
		LOG( MLUCLog::kError, "BCLTCPBlobCommunication::Transferblob", "Communication error" )
		    << "Communication error. Recovery failed: " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
		retval = EIO;
		break;
		}
	    if ( timeout )
		{
		struct timeval newTimeout;
		newTimeout.tv_sec = timeout/1000;
		newTimeout.tv_usec = (timeout-(newTimeout.tv_sec*1000))*1000;
		ret = setsockopt( connectData->fSocket, SOL_SOCKET, SO_SNDTIMEO, &newTimeout, sizeof(newTimeout) );
		if ( ret==-1 )
		    {
		    ret = errno;
		    LOGCG( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::Transferblob", "Socket Timeout Set Error", tmpLog )
			<< "Error getting TCP send timeout from socket while trying to do transfer " << MLUCLog::kDec << id 
			<< " to message address ";
		    BCLAddressLogger::LogAddress( tmpLog, *msgAddress ) << " / blob address ";
		    BCLAddressLogger::LogAddress( tmpLog, remoteBlobAddress ) 
			<< ": " << strerror(ret) << " (" 
			<< MLUCLog::kDec << ret << ")." << ENDLOG;
		    }
		}
	    szWritten = 0;
	    nextBuffer = 0;
	    continue;
	    }
	if ( retval != 0 )
	    break;
	do
	    {
#ifdef TCPCOM_NO_POLL
	    FD_ZERO( &sockets );
	    FD_SET( connectData->fSocket, &sockets );
	    if ( timeout )
		{
		tv.tv_sec = timeout/1000;
		tv.tv_usec = (timeout-tv.tv_sec*1000)*1000;
		ptv = &tv;
		}
	    else
		ptv = NULL;
	    errno = 0;
	    ret = select( connectData->fSocket+1, NULL, &sockets, NULL, ptv );
#else
	    pollfd pollSock = { connectData->fSocket, POLLOUT, 0 };
	    errno = 0;
	    ret = poll( &pollSock, 1, timeout ? timeout : -1 );
#endif
	    }
	while ( ret<=0 && (errno==EINTR || errno==EAGAIN) );
	TIMEMARK;
	if ( ret == 0 )
	    {
	    LOGCG( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::Transferblob", "Timeout expired", tmpLog )
		<< "Timeout expired while trying to do transfer " << MLUCLog::kDec << id 
		<< " to message address ";
	    BCLAddressLogger::LogAddress( tmpLog, *msgAddress ) << " / blob address ";
	    BCLAddressLogger::LogAddress( tmpLog, remoteBlobAddress ) 
		<< ": " << strerror(errno) << " (" 
		<< MLUCLog::kDec << errno << ")." << ENDLOG;
	    BlobSendError( ETIMEDOUT, this, &remoteBlobAddress, id, callback );
	    retval = ETIMEDOUT;
	    break;
	    }
	if ( ret < 0 )
	    {
	    LOGCG( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::Transferblob", "Select error", tmpLog )
		<< "Error on select while trying to do transfer " << MLUCLog::kDec << id 
		<< " to message address ";
	    BCLAddressLogger::LogAddress( tmpLog, *msgAddress ) << " / blob address ";
	    BCLAddressLogger::LogAddress( tmpLog, remoteBlobAddress ) 
		<< ": " << strerror(errno) << " (" 
		<< MLUCLog::kDec << errno << ")." << ENDLOG;
	    BlobSendError( ENXIO, this, &remoteBlobAddress, id, callback );
	    retval = ENXIO;
	    break;
	    }
	}
    for ( unsigned n = 0; n < blockCnt; n++ )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::TransferBlob", "Transfer block" )
	    //LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "BCLTCPBlobCommunication::TransferBlob", "Transfer block" )
	    << "Transfer " << MLUCLog::kDec << id << " (0x" 
	    << MLUCLog::kHex << id << ") block offset: " << MLUCLog::kDec
	    << comHeaders[n].fBlockOffset << " (0x" << MLUCLog::kHex << comHeaders[n].fBlockOffset
	    << " - block size: " << MLUCLog::kDec << comHeaders[n].fBlockSize << " (0x" 
	    << MLUCLog::kDec << comHeaders[n].fBlockSize << ")." << ENDLOG;
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::TransferBlob", "Transfer block" )
	    //LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "BCLTCPBlobCommunication::TransferBlob", "Transfer block" )
	    << "Header length: " << MLUCLog::kDec << comHeaders[n].fLength
	    << " (0x" << MLUCLog::kHex << comHeaders[n].fLength << ")"
	    << "." << ENDLOG;
	}
    if ( timeout )
	{
	setsockopt( connectData->fSocket, SOL_SOCKET, SO_SNDTIMEO, &oldTimeout, sizeof(oldTimeout) );
	}

    if ( completionIndicator )
	{
	if ( !retval )
	    {
	    do
		{
#ifdef TCPCOM_NO_POLL
		TIMEMARK;
		FD_ZERO( &sockets );
		TIMEMARK;
		FD_SET( connectData->fSocket, &sockets );
		TIMEMARK;
		if ( timeout )
		    {
		    tv.tv_sec = timeout/1000;
		    tv.tv_usec = (timeout-tv.tv_sec*1000)*1000;
		    ptv = &tv;
		    }
		else
		    ptv = NULL;
		errno = 0;
		TIMEMARK;
		ret = select( connectData->fSocket+1, &sockets, NULL, NULL, ptv );
		TIMEMARK;
#else
		pollfd pollSock = { connectData->fSocket, POLLIN, 0 };
		errno = 0;
		ret = poll( &pollSock, 1, timeout ? timeout : -1 );
#endif
		}
	    while ( ret<=0 && (errno==EINTR || errno==EAGAIN) );
	    TIMEMARK;
	    if ( ret != 1 )
		{
		if ( ret == 0 )
		    errno = ETIMEDOUT;
		LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::TransferBlob", "Transfer Complete Wait Error" )
		    << "Error waiting for transfer complete from remote partner: " << MLUCLog::kDec << strerror(errno)
		    << " (" << errno << ")." << ENDLOG;
		fComHelper.DisconnectFromRemote( connectData, timeout, timeout );
		return errno;
		}
	    else
		{
		uint32 ack;
		do
		    {
		    ret = read( connectData->fSocket, &ack, sizeof(uint32) );
		    }
		while ( ret<0 && errno==EINTR );
		if ( ret != sizeof(uint32) )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::TransferBlob", "Transfer Complete Read Error" )
			<< "Error when trying to read transfer complete from remote partner: " << strerror(errno)
			<< " (" << MLUCLog::kDec << errno << ")." << ENDLOG;
		    }
		if ( ack != id )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::TransferBlob", "Wrong Transfer Complete Data" )
			<< "Error when trying to read transfer complete from remote partner. Read unexpected value "
			<< MLUCLog::kDec << ack << " (0x" << MLUCLog::kHex << ack << "). Expected " << MLUCLog::kDec
			<< id << " (0x" << MLUCLog::kHex << id << ")." << ENDLOG;
		    }
		}
	    TIMEMARK;
	    }
	}

    fComHelper.DisconnectFromRemote( connectData, timeout, timeout );
    TIMEMARK;
    DUMP_TIMEMARKS;
    return retval;
    }


// Not supported currently; Might be introduced back in later versions. 
// bool BCLTCPBlobCommunication::IsTransferComplete( BCLTransferID transfer )
//     {
//     return true;
//     }

int BCLTCPBlobCommunication::WaitForReceivedTransfer( BCLTransferID& transfer, vector<uint64>& offsets, vector<uint64>& sizes, unsigned long timeout )
    {
    bool found = false;
    struct timeval start, stop;
    unsigned long tdiff, twait;
    fBlobReceivedSignal.Lock();
    gettimeofday( &start, NULL );
    do
	{    
	if ( !fQuitReceivedBlobWait && fReceivedBlobs.GetCnt()>0 )
	    {
	    offsets.clear();
	    sizes.clear();
	    found = true;
	    TTCPReceivedBlobData* blob = fReceivedBlobs.GetFirstPtr();
	    transfer = blob->fTransferID;
	    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::WaitForReceivedTransfer", "Found new transfer" )
		<< "Found new received transfer - TransferID: " << MLUCLog::kDec << transfer << " (0x" 
		<< MLUCLog::kHex << transfer << ")." << ENDLOG;
	    offsets.resize( blob->fTransferBlocksLeft+1 );
	    sizes.resize( blob->fTransferBlocksLeft+1 );
	    unsigned long ndx=0;
	    do
		{
		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::WaitForReceivedTransfer", "Transfer block" )
		    << "Transfer " << MLUCLog::kDec << transfer << " (0x" 
		    << MLUCLog::kHex << transfer << ") block offset: " << MLUCLog::kDec
		    << blob->fBlockOffset << " (0x" << MLUCLog::kHex << blob->fBlockOffset
		    << " - block size: " << MLUCLog::kDec << blob->fBlockSize << " (0x" 
		    << MLUCLog::kDec << blob->fBlockSize << ")." << ENDLOG;
		offsets[ndx] = blob->fBlockOffset;
		sizes[ndx] = blob->fBlockSize;
		ndx++;
		if ( blob->fTransferBlocksLeft )
		    {
		    fReceivedBlobs.RemoveFirst();
		    blob = fReceivedBlobs.GetFirstPtr();
		    }
		else
		    {
		    blob = NULL;
		    fReceivedBlobs.RemoveFirst();
		    }
		}
	    while ( blob );
	    }
	else if ( !fQuitReceivedBlobWait )
	    {
	    gettimeofday( &stop, NULL );
	    tdiff = (stop.tv_sec-start.tv_sec)*1000+(stop.tv_usec-start.tv_usec)/1000;
	    if ( timeout && tdiff>timeout )
		{
		fBlobReceivedSignal.Unlock();
		return ETIMEDOUT;
		}
	    if ( timeout )
		{
		twait = timeout-tdiff;
		fBlobReceivedSignal.Wait( twait );
		}
	    else
		fBlobReceivedSignal.Wait();
	    }
	}
    while ( !found && !fQuitReceivedBlobWait );
    fBlobReceivedSignal.Unlock();
    if ( fQuitReceivedBlobWait )
	{
	fQuitReceivedBlobWait = false;
	return ECANCELED;
	}
    return 0;
    }

void BCLTCPBlobCommunication::AbortWaitForReceivedTransfer()
    {
    fBlobReceivedSignal.Lock();
    fQuitReceivedBlobWait = true;
    fBlobReceivedSignal.Unlock();
    fBlobReceivedSignal.Signal();
    }



uint8* BCLTCPBlobCommunication::GetBlobData( BCLTransferID transfer )
    {
    int ret;
    uint32 offset = (uint32)transfer;
    if ( offset >= fBlobBufferSize )
	{
	BlobReceiveError( ENOENT, this, transfer, NULL );
	return NULL;
	}
#ifndef NO_PARANOIA
    TTTCPBlockDataContType::iterator iter, end;
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	{
	BlobReceiveError( ENXIO, this, transfer, NULL );
	return NULL;
	}
    iter = fUsedBlocks.begin();
    end = fUsedBlocks.end();
    while ( iter != end )
	{
	if ( iter->fBlockOffset == offset )
	    break;
	iter++;
	}
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	{
	BlobReceiveError( ENXIO, this, transfer, NULL );
	return NULL;
	}
    if ( iter == end )
	{
	BlobReceiveError( ENOENT, this, transfer, NULL );
	return NULL;
	}
#endif
    return fBlobBuffer+offset;
    }

uint64 BCLTCPBlobCommunication::GetBlobOffset( BCLTransferID transfer )
    {
    int ret;
    uint64 offset = (uint64)transfer;
    if ( offset >= fBlobBufferSize )
	{
	BlobReceiveError( ENOENT, this, transfer, NULL );
	return (uint64)-1;
	}
#ifndef NO_PARANOIA
    TTTCPBlockDataContType::iterator iter, end;
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	{
	BlobReceiveError( ENXIO, this, transfer, NULL );
	return (uint64)-1;
	}
    iter = fUsedBlocks.begin();
    end = fUsedBlocks.end();
    while ( iter != end )
	{
	if ( iter->fBlockOffset == offset )
	    break;
	iter++;
	}
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	{
	BlobReceiveError( ENXIO, this, transfer, NULL );
	return (uint64)-1;
	}
    if ( iter == end )
	{
	BlobReceiveError( ENOENT, this, transfer, NULL );
	return (uint64)-1;
	}
#endif
    return offset;
    }

void BCLTCPBlobCommunication::ReleaseBlob( BCLTransferID transfer )
    {
    int ret;
    uint64 offset = (uint64)transfer;
    if ( offset >= fBlobBufferSize )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "BCLTCPBlobCommunication::ReleaseBlob", "Blob offset too large" )
	    << "Blob offset " << MLUCLog::kDec << offset << " (0x" << MLUCLog::kHex << offset 
	    << ") derived from transfer id 0x" << transfer << " is larger than buffer size " << MLUCLog::kDec
	    << fBlobBufferSize << "." << ENDLOG;
	GeneralError( ENOENT, this, NULL );
	return;
	}
    TTTCPBlockDataContType::iterator iter, end;
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "BCLTCPBlobCommunication::ReleaseBlob", "Mutex lock error" )
	    << "Error locking block mutex: " << strerror(ret) << " (" << MLUCLog::kDec << ret
	    << ")." << ENDLOG;
	GeneralError( ENXIO, this, NULL );
	return;
	}
    iter = fUsedBlocks.begin();
    end = fUsedBlocks.end();
    bool found = false;
    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::ReleaseBlob", "Block to Free" )
	<< "Block to free Offset: " << MLUCLog::kDec << offset 
	<< "." << ENDLOG;
    while ( iter != end )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::ReleaseBlob", "Used Block" )
	    << "Used block offset: " << MLUCLog::kDec << iter->fBlockOffset << " - size: "
	    << iter->fBlockSize << "." << ENDLOG;
	if ( iter->fBlockOffset == offset )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::ReleaseBlob", "Used Block Found" )
		<< "Used block found, offset: " << MLUCLog::kDec << iter->fBlockOffset << " - size: "
		<< iter->fBlockSize << "." << ENDLOG;
	    found = true;
	    bool freeFound = false;
	    TTTCPBlockDataContType::iterator freeIter, freeEnd;
	    freeIter = fFreeBlocks.begin();
	    freeEnd = fFreeBlocks.end();
	    while ( freeIter != freeEnd )
		{
		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::ReleaseBlob", "Free Block" )
		    << "Free block offset: " << MLUCLog::kDec << freeIter->fBlockOffset << " - size: "
		    << freeIter->fBlockSize << "." << ENDLOG;
		if ( iter->fBlockOffset+iter->fBlockSize==freeIter->fBlockOffset )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::ReleaseBlob", "Free Block Merged" )
			<< "Merged used block to beginning of free..." << ENDLOG;
		    freeIter->fBlockOffset = iter->fBlockOffset;
		    freeIter->fBlockSize += iter->fBlockSize;
		    freeFound = true;
		    break;
		    }
		if ( iter->fBlockOffset==freeIter->fBlockOffset+freeIter->fBlockSize )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::ReleaseBlob", "Free Block Merged" )
			<< "Merged used block to end of free..." << ENDLOG;
		    freeIter->fBlockSize += iter->fBlockSize;
		    if ( freeIter!=freeEnd && 
			 (freeIter+1)->fBlockOffset==(freeIter->fBlockOffset+freeIter->fBlockSize) )
			{
			LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::ReleaseBlob", "Free Block Merged" )
			    << "Merged following free block..." << ENDLOG;

			freeIter->fBlockSize += (freeIter+1)->fBlockSize;
			fFreeBlocks.erase( freeIter+1 );
			}
		    freeFound = true;
		    break;
		    }
		if ( freeIter->fBlockOffset > iter->fBlockOffset )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::ReleaseBlob", "Free Block Merged" )
			<< "Inserted used block..." << ENDLOG;

		    fFreeBlocks.insert( freeIter, *iter );
		    freeFound = true;
		    break;
		    }
		freeIter++;
		}
	    if ( !freeFound )
		{
		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::ReleaseBlob", "Free Block Merged" )
		    << "Inserted used block at end..." << ENDLOG;
		fFreeBlocks.insert( fFreeBlocks.end(), *iter );
		}
	    fUsedBlocks.erase( iter );
	    break;
	    }
	iter++;
	}
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "BCLTCPBlobCommunication::ReleaseBlob", "Mutex unlock error" )
	    << "Error unlocking block mutex: " << strerror(ret) << " (" << MLUCLog::kDec << ret
	    << ")." << ENDLOG;
	GeneralError( ENXIO, this, NULL );
	return;
	}
    if ( !found )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "BCLTCPBlobCommunication::ReleaseBlob", "No free block" )
	    << "No used block with the needed offset of " << MLUCLog::kDec << offset << " (0x" << MLUCLog::kHex << offset 
	    << ") derived from transfer id 0x" << transfer << " could be found." << ENDLOG;
	GeneralError( ENOENT, this, NULL );
	return;
	}
    return;
    }


uint32 BCLTCPBlobCommunication::GetAddressLength()
    {
    return sizeof(BCLTCPAddressStruct);
    }

uint32 BCLTCPBlobCommunication::GetComID()
    {
    return kTCPComID;
    }

BCLTCPAddressStruct* BCLTCPBlobCommunication::NewAddress()
    {
    BCLTCPAddressStruct* addr = new BCLTCPAddressStruct;
    if ( !addr )
	return NULL;
    BCLTCPAddress addrClass;
    *addr = *(addrClass.GetData());
    addr->fComID = kTCPComID;
    return addr;
    }

void BCLTCPBlobCommunication::DeleteAddress( BCLAbstrAddressStruct* address )
    {
    delete (BCLTCPAddressStruct*)address;
    }

bool BCLTCPBlobCommunication::AddressEquality( const BCLAbstrAddressStruct* addr1, const BCLAbstrAddressStruct* addr2 )
    {
    if ( addr1->fComID != kTCPComID || addr2->fComID != kTCPComID )
	return false;
    return *(BCLTCPAddressStruct*)addr1 == *(BCLTCPAddressStruct*)addr2;
    }

BCLTCPBlobCommunication::operator bool()
    {
    return true;
    }

void BCLTCPBlobCommunication::QuitRequestHandler()
    {
    //fQuitRequestHandler = true;
    int ret=-1;
    BCLIntTCPBlobComMsg msg;
    msg.GetData()->fMsgType = kTCPBlobQuitMsg;
#ifdef TCP_SIMPLE_TIMEOUT
    unsigned long timeoutCount=0;
#endif
    if ( !fMsgCom )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::QuitRequestHandler", "No msg com object" )
	    << "Error sending request handler quit message. No msg communication object..." << ENDLOG;
	GeneralError( EFAULT, this, &fTCPAddress );
	}
#ifdef TCP_TIMEOUT
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 3000000;
    gettimeofday( &start, NULL );
#endif
    while ( !fRequestHandlerQuitted )
	{
	BCLAbstrAddressStruct* addr = fMsgCom->NewAddress();
	memcpy( addr, fMsgCom->GetAddress(), fMsgCom->GetAddress()->fLength );
	//*addr = *(fMsgCom->GetAddress());
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::QuitRequestHandler", "Sending Quit message" )
	    << "Trying to send quit message to request handler at address " << *addr << "." << ENDLOG;
	if ( addr->fComID == kTCPComID )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::QuitRequestHandler", "Sending Quit message" )
		<< "Request handler address: " << *(BCLTCPAddressStruct*)addr << "." << ENDLOG;
	    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::QuitRequestHandler", "Sending Quit message" )
		<< "Msg com address: " << *(BCLTCPAddressStruct*)(fMsgCom->GetAddress()) << "." << ENDLOG;
	    }
	ret = fMsgCom->Send( addr, msg.GetData() );
	fMsgCom->DeleteAddress( addr );
	if ( ret )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::QuitRequestHandler", "Sending Quit message" )
		<< "Error trying to send quit message to request handler: " << strerror(ret) << " ("
		<< ret << ")." << ENDLOG;
	    GeneralError( ret, this, &fTCPAddress );
	    // XXX
	    if ( ret == ENODEV || ret == ENXIO )
		break;
	    }
	usleep( 0 );
#ifdef TCP_TIMEOUT
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	if ( deltaT>timeLimit )
	    break;
#endif
#ifdef TCP_SIMPLE_TIMEOUT
	if ( timeoutCount++ >= SIMPLE_TIMEOUT_MAX )
	    break;
#endif
	}
#ifdef TCP_SIMPLE_TIMEOUT
    if ( timeoutCount >= SIMPLE_TIMEOUT_MAX || ret )
	{
	fRequestThread.Abort();
	return;
	}
#endif
    fRequestThread.Join();
    }

void BCLTCPBlobCommunication::RequestHandler()
    {
    int ret;
    fQuitRequestHandler = false;
    BCLAbstrAddressStruct* addr;
    addr = fMsgCom->NewAddress();
    if ( !addr )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::RequestHandler", "Allocating message address" )
	    << "Out of memory allocating message communication address structure." << ENDLOG;
	GeneralError( ENOMEM, this, NULL );
	return;
	}
    BCLMessageStruct* gMsg;
    BCLIntTCPBlobComMsgStruct* msg;
    fRequestHandlerQuitted = false;
    do
	{
	ret = fMsgCom->Receive( addr, gMsg );
	
	if ( !this  )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kFatal, "BCLTCPBlobCommunication::RequestHandler", "Message receive" )
		<< "No this pointer available. Receive return value: " << strerror(ret) << " ("
		<< MLUCLog::kDec << ret << "). Aborting..." << ENDLOG;
	    return;
	    }
	if ( !fMsgCom  )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kFatal, "BCLTCPBlobCommunication::RequestHandler", "Message receive" )
		<< "No msg com object available. Receive return value: " << strerror(ret) << " ("
		<< MLUCLog::kDec << ret << "). Aborting..." << ENDLOG;
	    fRequestHandlerQuitted = true;
	    return;
	    }
	if ( ret == ETIMEDOUT )
	    {
	    continue;
	    }
	if ( ret )
	    {
	    switch ( ret )
		{
		case ENXIO: 
		    // Fallthrough
		case ENODEV:
		    LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::RequestHandler", "Message receive" )
			<< "Error when trying to to receive message: " << strerror(ret) << " (" << ret 
			<< "). Aborting message loop..." << ENDLOG;
		    GeneralError( ENODEV, this, NULL );
		    fRequestHandlerQuitted = true;
		    return;
		default:
		    LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::RequestHandler", "Message receive" )
			<< "Error when trying to to receive message: " << strerror(ret) << " (" << ret << ")."
			<< ENDLOG;
		    break;
		}
	    continue;
	    }
	if ( !gMsg )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::RequestHandler", "Message receive" )
		<< "Error when trying to to receive message: " << strerror(ret) << " (" << ret << ")."
		<< ENDLOG;
	    GeneralError( ENODEV, this, NULL );
	    continue;
	    }
	msg = (BCLIntTCPBlobComMsgStruct*)gMsg;
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::RequestHandler", "Message received" )
	    << "Received message with query ID 0x" << MLUCLog::kHex << msg->fQueryID
	    << " (" << MLUCLog::kDec << msg->fQueryID << ")." << ENDLOG;
	
	switch ( msg->fMsgType )
	    {
	    case kTCPBlobQueryAddressMsg:
		LOGCG( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::RequestHandler", "Address query", tmpLog )
		    << "Address query received from remote node ";
		BCLAddressLogger::LogAddress( tmpLog, *addr ) << "." << ENDLOG;
		msg->fIPNr = fTCPAddress.fIPNr;
		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::RequestHandler", "Address query" )
		    << "Address reply " << MLUCLog::kDec << fTCPAddress.fIPNr << "(0x" << MLUCLog::kHex << fTCPAddress.fIPNr << ")." << ENDLOG;
		msg->fPortNr = fTCPAddress.fPortNr;
		msg->fMsgType = kTCPBlobAddressMsg;
		if ( fReplyTimeout )
		    ret = fMsgCom->Send( addr, msg, fReplyTimeout );
		else
		    ret = fMsgCom->Send( addr, msg );
		if ( ret )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::RequestHandler", "Address message send" )
			<< "Error when trying to send address message reply: " << strerror(ret) << " ("
			<< ret << ")." << ENDLOG;
		    GeneralError( ret, this, addr );
		    }
		LOGCG( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::RequestHandler", "Address query", tmpLog )
		    << "Address reply sent to remote node ";
		BCLAddressLogger::LogAddress( tmpLog, *addr ) << "." << ENDLOG;
		break;
	    case kTCPBlobQueryBlobSizeMsg:
		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::RequestHandler", "Blob size query" )
		    << "Blob size query received from remote node " << *addr 
		    << ": 0x" << MLUCLog::kHex << fBlobBufferSize << " (" << MLUCLog::kDec
		    << fBlobBufferSize << ")." << ENDLOG;
		msg->fMsgType = kTCPBlobBlobSizeReplyMsg;
		msg->fBlockSize = fBlobBufferSize;
		if ( fReplyTimeout )
		    ret = fMsgCom->Send( addr, msg, fReplyTimeout );
		else
		    ret = fMsgCom->Send( addr, msg );
		if ( ret )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::RequestHandler", "Blob size message send" )
			<< "Error when trying to send blob size message reply: " << strerror(ret) << " ("
			<< ret << ")." << ENDLOG;
		    GeneralError( ret, this, addr );
		    }
		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::RequestHandler", "Blob size query" )
		    << "Blob size reply sent to remote node " << *addr << "." << ENDLOG;
		break;
	    case kTCPBlobQueryFreeBlockMsg:
		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::RequestHandler", "Blob query" )
		    << "Blob query for 0x" << MLUCLog::kHex << msg->fBlockSize << " (" << MLUCLog::kDec
		    << msg->fBlockSize << ") bytes received from remote node " << *addr 
		    << "." << ENDLOG;
		TTCPBlockData block;
		ret = GetFreeBlock( msg->fBlockSize, block );
		if ( ret )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::RequestHandler", "Free block request" )
			<< "Error while trying to allocate free block request for " << MLUCLog::kDec 
			<< msg->fBlockSize
			<< " bytes: " << strerror(ret) << " (" << ret << ")." << ENDLOG;
		    GeneralError( ret, this, addr );
		    }
		if ( block.fBlockOffset==(uint32)-1 )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "BCLTCPBlobCommunication::RequestHandler", "Free block request" )
			<< "Could not allocate requested free block of " << MLUCLog::kDec << msg->fBlockSize
			<< " bytes." << ENDLOG;
		    }
		msg->fBlockOffset = block.fBlockOffset;
		msg->fMsgType = kTCPBlobFreeBlockMsg;
		if ( fReplyTimeout )
		    ret = fMsgCom->Send( addr, msg, fReplyTimeout );
		else
		    ret = fMsgCom->Send( addr, msg );
		if ( ret )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::RequestHandler", "block message send" )
			<< "Error when trying to send block message reply: " << strerror(ret) << " ("
			<< ret << ")." << ENDLOG;
		    GeneralError( ret, this, addr );
		    }
		break;
	    case kTCPBlobConnectRequestMsg:
		{
		LOGC( gBCLLogLevelRef, MLUCLog::kInformational, "BCLTCPBlobCommunication::RequestHandler", "Connection request" )
		    << "Connection request received." << ENDLOG;
		BCLTCPAddress blbAddr;
		blbAddr.GetData()->fIPNr = msg->fIPNr;
		if ( blbAddr.GetData()->fIPNr == INADDR_ANY )
		    {
		    if ( addr->fComID == kTCPComID )
			blbAddr.GetData()->fIPNr = ((BCLTCPAddressStruct*)addr)->fIPNr;
		    else
			{
			LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::RequestHandler", "Connection request unable to determine rmote IP" )
			    << "Cannot determine remote IP address from message from remote address " << *addr << "." << ENDLOG;
			GeneralError( ret, this, addr );
			break;
			}
		    }
		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::RequestHandler", "LOGC( gBCLLogLevelRef, Connection request IP" )
		    << "Connection request IP: " << MLUCLog::kDec << msg->fIPNr << "(0x" << MLUCLog::kHex << msg->fIPNr
		    << ")." << ENDLOG;
		if ( addr->fComID == kTCPComID )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::RequestHandler", "Remote IP" )
			<< "Remote IP: " << MLUCLog::kDec << ((BCLTCPAddressStruct*)addr)->fIPNr << "(0x" << MLUCLog::kHex << ((BCLTCPAddressStruct*)addr)->fIPNr
			<< ")." << ENDLOG;
		    }
		else
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::RequestHandler", "No Remote IP" )
			<< "No Remote IP." << ENDLOG;
		    }
		
		blbAddr.GetData()->fPortNr = msg->fPortNr;
		ret = Connect( addr, blbAddr.GetData(), false, fReplyTimeout, fReplyTimeout );
		if ( ret )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::RequestHandler", "Requested connect failed" )
			<< "Error making requested connection to " << *addr << ": " << strerror(ret) 
			<< " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
		    GeneralError( ret, this, addr );
		    }
		break;
		}
	    case kTCPBlobDisconnectMsg:
		{
		LOGC( gBCLLogLevelRef, MLUCLog::kInformational, "BCLTCPBlobCommunication::RequestHandler", "Disconnect request" )
		    << "Disonnect request received." << ENDLOG;
		BCLTCPAddress blobAddr;
		blobAddr.GetData()->fIPNr = msg->fIPNr;
		blobAddr.GetData()->fPortNr = msg->fPortNr;
		Disconnect( addr, blobAddr.GetData(), false, fReplyTimeout, fReplyTimeout );
		break;
		}
	    case kTCPBlobAddressMsg:
		if ( 1 )
		    {
		    LOGCG( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::RequestHandler", "Address reply", tmpLog )
			<< "Address reply received from node ";
		    BCLAddressLogger::LogAddress( tmpLog, *addr ) << "." << ENDLOG;
		    if ( msg->fIPNr == INADDR_ANY )
			{
			if ( addr->fComID == kTCPComID )
			    {
			    msg->fIPNr = reinterpret_cast<BCLTCPAddressStruct*>( addr )->fIPNr;
			    }
			else
			    {
			    LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::RequestHandler", "Blob Address Host unresolved" )
				<< "Cannot resolve remote blob host IP address in blob address reply sent from node " << *addr << " (Non TCP message address)." << ENDLOG;
			    }
			}
		    }
		else
		    case kTCPBlobFreeBlockMsg:
                    if ( 1 )
			{
			LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::RequestHandler", "Blob reply" )
			    << "Blob reply received from node " << *addr << "." << ENDLOG;
			}
		    else
			case kTCPBlobBlobSizeReplyMsg:
                        {
			LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::RequestHandler", "Blob size reply" )
			    << "Blob size reply received from node " << *addr << "." << ENDLOG;
			}
// 		ret = pthread_mutex_lock( &fMsgMutex );
// 		if ( ret )
// 		    {
// 		    LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::RequestHandler", "Mutex error" )
// 			<< "Error trying to lock message mutex for inserting message: " 
// 			<< strerror(ret) << " (" << ret << ")." << ENDLOG;
// 		    GeneralError( ret, this, addr );
// 		    break;
// 		    }
		fDataArrivedSignal.Lock();
		fMsgs.Add( *msg );
		fDataArrivedSignal.Unlock();
// 		ret = pthread_mutex_unlock( &fMsgMutex );
// 		if ( ret )
// 		    {
// 		    LOGC( gBCLLogLevelRef, MLUCLog::kError, "BCLTCPBlobCommunication::RequestHandler", "Mutex error" )
// 			<< "Error trying to unlock message mutex for inserting message: " 
// 			<< strerror(ret) << " (" << ret << ")." << ENDLOG;
// 		    GeneralError( ret, this, addr );
// 		    }
		fDataArrivedSignal.Signal();
		break;
	    case kTCPBlobQuitMsg:
		fQuitRequestHandler = true; 
		break;
	    }
	if ( gMsg )
	    fMsgCom->ReleaseMsg( gMsg );
	}
    while ( !fQuitRequestHandler );
    LOGC( gBCLLogLevelRef, MLUCLog::kInformational, "BCLTCPBlobCommunication::RequestHandler", "Request handler shutting down" )
	<< "Shutting down TCP blob com request handler..." << ENDLOG;
    fRequestHandlerQuitted = true;
    }

int BCLTCPBlobCommunication::Connect( BCLAbstrAddressStruct* msgAddress, BCLTCPAddressStruct* blobAddress, bool openOnDemand, bool useTimeout, unsigned long timeout_ms )
    {
    if ( !msgAddress )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob Connect", "Address error" )
	    << "Wrong address (NULL pointer) in TCP Blob Connect" << ENDLOG;
	ConnectionError( EFAULT, this, msgAddress );
	return EFAULT;
	}
    if ( !fMsgCom )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob Connect", "No msg communication" )
	    << "No message communication object configured in TCP Blob Connect" << ENDLOG;
	ConnectionError( ENODEV, this, msgAddress );
	return ENODEV;
	}
    int ret;
    int retries = 0;
    do
    {
	if (retries) usleep(100000);
	if ( useTimeout )
	    ret = fMsgCom->Connect( msgAddress, timeout_ms, openOnDemand );
	else
	    ret = fMsgCom->Connect( msgAddress, openOnDemand );
    } while ((ret == 110 || ret == 113) && ++retries < 5);
    if ( ret )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob Connect", "No msg communication" )
	 << "Number of retries before giving up: " << retries << ENDLOG;
	ConnectionError( ret, this, msgAddress );
	return ret;
	}
    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Blob Connect", "Msg com connected" )
	<< "Message communication object connected to remote address " << *msgAddress
	<< "." << ENDLOG;
    BCLTCPAddressStruct remoteBlobAddress;
    if (retries)
    {
	LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "TCP Blob Connect", "Connection retry needed" )
	    << "Needed " << retries << " retries to establish connection" << ENDLOG;
    }
    if ( !blobAddress )
	{
	ret = GetRemoteBlobAddress( msgAddress, &remoteBlobAddress, useTimeout, timeout_ms );
	if ( ret )
	    {
	    ConnectionError( ret, this, msgAddress );
	    return ret;
	    }
	LOGC( gBCLLogLevelRef, MLUCLog::kInformational, "TCP Blob Connect", "Remote Blob Address" )
	    << "Got remote Blob address: " << MLUCLog::kDec << remoteBlobAddress << " (hex: " << MLUCLog::kHex 
	    << remoteBlobAddress << ")." << ENDLOG;
	}
    else
	{
	remoteBlobAddress = *blobAddress;
	LOGC( gBCLLogLevelRef, MLUCLog::kInformational, "TCP Blob Connect", "Remote Blob Address" )
	    << "Remote Blob address set to " << MLUCLog::kDec << remoteBlobAddress << " (hex: " << MLUCLog::kHex 
	    << remoteBlobAddress << ")." << ENDLOG;
	}
    ret = fComHelper.Connect( &remoteBlobAddress, useTimeout, timeout_ms, openOnDemand );
    if ( ret )
	{
	if ( useTimeout )
	    fMsgCom->Disconnect( msgAddress, timeout_ms );
	else
	    fMsgCom->Disconnect( msgAddress );
	return ret;
	}
    TTCPRemoteBlobAddress rba;
    rba.fMsgAddress = fMsgCom->NewAddress();
    if ( rba.fMsgAddress && rba.fMsgAddress->fLength==msgAddress->fLength)
	{
	//*(rba.fMsgAddress) = *msgAddress;
	memcpy( rba.fMsgAddress, msgAddress, msgAddress->fLength );
	rba.fBlobAddress = remoteBlobAddress;
	MLUCMutex::TLocker remoteBlobAddressLock( fRemoteBlobAddressMutex );
	fRemoteBlobAddresses.insert( fRemoteBlobAddresses.end(), rba );
	}
    if ( !blobAddress )
	{
	BCLIntTCPBlobComMsg msg;
	msg.GetData()->fMsgType = kTCPBlobConnectRequestMsg;
	msg.GetData()->fIPNr = fTCPAddress.fIPNr;
	msg.GetData()->fPortNr = fTCPAddress.fPortNr;
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Blob Connect", "Connection request IP Address" )
	    << "Connection request IP: " << MLUCLog::kDec << fTCPAddress.fIPNr << "(0x" << MLUCLog::kHex << fTCPAddress.fIPNr
	    << ")." << ENDLOG;
	GetQueryID( msg.GetData()->fQueryID );
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Blob Connect", "Sending connection request" )
	    << "Sending connection request to remote side." << ENDLOG;
	if ( useTimeout )
	    ret = fMsgCom->Send( msgAddress, msg.GetData(), timeout_ms );
	else
	    ret = fMsgCom->Send( msgAddress, msg.GetData() );
	if ( ret )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob Connect", "Connection request" )
		<< "Error sending connection request to remote msg com object" << ENDLOG;
	    ConnectionError( ret, this, msgAddress );
	    return ret;
	    }
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Blob Connect", "Connection request" )
	    << "Connection Request message sent." << ENDLOG;
	}
    LOGC( gBCLLogLevelRef, MLUCLog::kInformational, "TCP Blob Connect", "Connection established" )
	<< "Connection to established." << ENDLOG;
    return 0;
    }


int BCLTCPBlobCommunication::GetFreeBlock( uint64 size, TTCPBlockData& block )
    {
    int ret;
    block.fBlockOffset = (uint64)-1;
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	return ret;
    TTTCPBlockDataContType::iterator iter, end, bestFit;
    bool bestFitSet = false;
    iter = fFreeBlocks.begin();
    end = fFreeBlocks.end();
    //start with first
    bestFit = iter;
    while ( iter != end )
	{
	if ( iter->fBlockSize >= size )
	    {
	    if ( !bestFitSet || bestFit->fBlockSize>iter->fBlockSize )
		{
		bestFitSet = true;
		bestFit = iter;
		}
	    }
	iter++;
	}
    if ( bestFitSet )
	{
	block.fBlockOffset = bestFit->fBlockOffset;
	block.fBlockSize = size;
	bestFit->fBlockOffset += size;
	bestFit->fBlockSize -= size;
	if ( bestFit->fBlockSize <= 0 )
	    fFreeBlocks.erase( bestFit );
	fUsedBlocks.insert( fUsedBlocks.end(), block );
	}
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	return ret;
    //if ( !bestFit )
    //return ENOSPC;
    return 0;
    }



int BCLTCPBlobCommunication::GetRemoteBlobAddress( BCLAbstrAddressStruct* msgAddress, 
						   BCLTCPAddressStruct* blobAddress, bool useTimeout, unsigned long timeout_ms )
    {
    int ret;
    BCLIntTCPBlobComMsg msg;
    if ( !fMsgCom )
	return ENODEV;

    // Standalone code block
	{
	MLUCMutex::TLocker remoteBlobAddressLock( fRemoteBlobAddressMutex );
	vector<TTCPRemoteBlobAddress>::iterator iter, end;
	iter = fRemoteBlobAddresses.begin();
	end = fRemoteBlobAddresses.end();
	while ( iter != end )
	    {
	    if ( fMsgCom->AddressEquality( iter->fMsgAddress, msgAddress ) )
		{
		*blobAddress = iter->fBlobAddress;
		return 0;
		}
	    iter++;
	    }
	}

    msg.GetData()->fMsgType = kTCPBlobQueryAddressMsg;
    ret = GetQueryID( msg.GetData()->fQueryID );
    if ( ret )
	return ret;
    fDataArrivedSignal.Lock();
    if ( useTimeout )
	ret = fMsgCom->Send( msgAddress, msg.GetData(), timeout_ms );
    else
	ret = fMsgCom->Send( msgAddress, msg.GetData() );
    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Blob Connect", "Address request" )
	<< "Remote Blob Address Request message sent." << ENDLOG;
    if ( ret )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob Connect", "Address request" )
	    << "Error sending Remote Blob Address Request message: " 
	    << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	fDataArrivedSignal.Unlock();
	return ret;
	}
    ret = WaitForMessage( msg.GetData()->fQueryID, msg.GetData(), useTimeout, timeout_ms );
    fDataArrivedSignal.Unlock();
    if ( ret )
	return ret;
    BCLTCPAddress tac;
    *blobAddress = *(tac.GetData());
    blobAddress->fComID = kTCPComID;
    blobAddress->fIPNr = msg.GetData()->fIPNr;
    blobAddress->fPortNr = msg.GetData()->fPortNr;
    return 0;
    }


int BCLTCPBlobCommunication::GetQueryID( uint64& id )
    {
//     int ret;
//     ret = pthread_mutex_lock( &fMsgMutex );
//     if ( ret )
// 	{
// 	return ENXIO;
// 	}
    fDataArrivedSignal.Lock();
    id = fCurrentQueryID++;
    fCurrentQueryCount++;
    fDataArrivedSignal.Unlock();
//     ret = pthread_mutex_unlock( &fMsgMutex );
//     if ( ret )
// 	{
// 	return ENXIO;
// 	}
    return 0;
    }


// Note: fDataArrivedSignal must be locked upon entry into this function.
int BCLTCPBlobCommunication::WaitForMessage( uint32 queryID, BCLIntTCPBlobComMsgStruct* msg, bool useTimeout, unsigned long timeout_ms )
    {
    bool found = false;
//     int ret=0;
    BCLIntTCPBlobComMsg msgClass;
    //TBCLIntTCPBlobComMsgStructContType::iterator iter, end;
    unsigned long msgNdx;
    struct timeval start, stop;
    unsigned long tdiff, twait;
    gettimeofday( &start, NULL );
//     fDataArrivedSignal.Lock();
    do
	{
// 	ret = pthread_mutex_lock( &fMsgMutex );
// 	if ( ret )
// 	    {
// 	    return ENXIO;
// 	    }
	if ( MLUCVectorSearcher<BCLIntTCPBlobComMsgStruct,uint32>::FindElement( fMsgs, &FindMsgByQueryID, queryID, msgNdx ) )
	    {
	    found = true;
	    *msg = *(fMsgs.GetPtr( msgNdx ));
	    fMsgs.Remove( msgNdx );
	    fCurrentQueryCount--;
	    }
	else
	    {
// 	ret = pthread_mutex_unlock( &fMsgMutex );
// 	if ( found )
// 	    {
// 	    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::WaitForMessage", "Message found" )
// 		<< "Desired message (0x" << MLUCLog::kHex << queryID << "  found"
// 		<< " ." << ENDLOG;
// 	    return 0;
// 	    }
	    LOGMC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::WaitForMessage", "Message not found", 200 )
		<< "Desired message (0x" << MLUCLog::kHex << queryID << " not found" 
		<< " ." << ENDLOG;
	    gettimeofday( &stop, NULL );
	    tdiff = (stop.tv_sec-start.tv_sec)*1000+(stop.tv_usec-start.tv_usec)/1000;
	    if ( (useTimeout && tdiff>timeout_ms ) || tdiff > fComHelper.fConnectionTimeout*5 )
		{
		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::WaitForMessage", "Message not found" )
		    << "Desired message (0x" << MLUCLog::kHex << queryID << "  not found after "
		    << MLUCLog::kDec << tdiff << "ms ." << ENDLOG;
		fDataArrivedSignal.Unlock();
		return ETIMEDOUT;
		}
	    // 	if ( ret )
	    // 	    {
	    // 	    fDataArrivedSignal.Unlock();
	    // 	    return ENXIO;
	    // 	    }
	    if ( useTimeout )
		twait = timeout_ms-tdiff;
	    else
		twait = fComHelper.fConnectionTimeout*5-tdiff;
	    //fDataArrivedSignal.Wait( fComHelper.fConnectionTimeout );
	    fDataArrivedSignal.Wait( twait );
	    }
	}
    while ( !found );
//     fDataArrivedSignal.Unlock();
    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::WaitForMessage", "Message found" )
	<< "Desired message (0x" << MLUCLog::kHex << queryID << "  found"
	<< " ." << ENDLOG;

    return 0;
    }



bool BCLTCPBlobCommunication::NewConnection( BCLIntTCPComHelper::TAcceptedConnectionData& con )
    {
    LOGC( gBCLLogLevelRef, MLUCLog::kInformational, "TCP Blob Receive", "New Connection" )
	<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << " new connection on socket " << con.fSocket
	<< "." << ENDLOG;
    int ret, one=1;
    ret = setsockopt( con.fSocket, SOL_TCP, TCP_NODELAY, &one, sizeof(one) );
    if ( ret )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kWarning, "BCLTCPBlobCommunication::NewConnection", "TCP_NODELAY error" )
	    << "Error activating TCP_NODELAY option for new connection socket " << MLUCLog::kDec
	    << con.fSocket << ": " << strerror(errno) << " (" << errno << ")." << ENDLOG;
	}
    return true;
    }


int BCLTCPBlobCommunication::NewData( BCLIntTCPComHelper::TAcceptedConnectionData& con )
    {
    //MLUCTracebackBuffer tbb;
    LOGC( gBCLLogLevelRef, MLUCLog::kInformational, "TCP Blob Receive", "New Data" )
	<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << " new data on socket " << con.fSocket
	<< "." << ENDLOG;
    DECL_TIMEMARKS(30);
    if ( !fMsgCom )
	return -1;
    int totalRead = 0;
    TIMEMARK;
    // Check reads with select and store state in fBlobProgress
    BCLIntTCPBlobComHeader tbch;
    BCLNetworkData nd;
    TCPBlobProgressData *blobProgress = NULL;
    //TTCPBlobProgressDataContType::iterator iter, end;
    unsigned long bpNdx=~0UL;
    bool looped = false;
    bool found = false;
    TCPBlobProgressData bpd;
    TIMEMARK;
    if ( MLUCVectorSearcher<TCPBlobProgressData,int>::FindElement( fBlobProgress, &FindBlobProgressBySocket, con.fSocket, bpNdx ) )
	{
	blobProgress = fBlobProgress.GetPtr( bpNdx );
	found = true;
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Blob New Data Receive", "blob progress found" )
	    << "Blob progress found at index " << MLUCLog::kDec << bpNdx 
	    << "." << ENDLOG;
	if ( !fBlobProgress.IsValid( bpNdx ) )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob New Data Receive", "invalid blob progress found" )
		<< "Blob progress found at index " << MLUCLog::kDec << bpNdx 
		<< " IS INVALID." << ENDLOG;
	    }
	}
    if ( !found )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Blob New Data Receive", "blob progress not found" )
	    << "Blob progress not found, creating..." << ENDLOG;
	bpd.fSocket = con.fSocket;
	bpd.fBytesRead = 0;
	bpd.fHeaders = NULL;
	blobProgress = &bpd;
	memset( &blobProgress->fHeader1, 0, sizeof(blobProgress->fHeader1) );
	/*
	fBlobProgress.Add( bpd, bpNdx );
	blobProgress = fBlobProgress.GetPtr( bpNdx );
	*/
	}
    TIMEMARK;
    int ret;
#ifdef TCPCOM_NO_POLL
    struct timeval tv;
#endif
    struct timeval oldRcvTimeout;
    bool resetTimeout = false;
    if ( fReceiveTimeout )
	{
	con.fTimeout.tv_sec = fReceiveTimeout/1000;
	con.fTimeout.tv_usec = (fReceiveTimeout-(con.fTimeout.tv_sec*1000))*1000;
	socklen_t sockoptlen = sizeof(oldRcvTimeout);
	ret = getsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, &sockoptlen );
	if ( ret==-1 )
	    {
	    ret = errno;
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob New Data Receive", "Socket Timeout Get Error" )
		<< "Error getting TCP receive timeout from socket in TCP blob receive." << ENDLOG;
	    }
	else
	    {
	    struct timeval newTimeout;
	    newTimeout.tv_sec = 0; // fReceiveTimeout/1000;
	    newTimeout.tv_usec = 0; // (fReceiveTimeout-(newTimeout.tv_sec*1000))*1000;
	    ret = setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &newTimeout, sizeof(newTimeout) );
	    if ( ret==-1 )
		{
		ret = errno;
		LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob New Data Receive", "Socket Timeout Set Error" )
		    << "Error getting TCP receive timeout from socket in TCP blob receive." << ENDLOG;
		}
	    resetTimeout = true;
	    }
	}

#ifdef TCPCOM_NO_POLL
    fd_set sockets;
#endif

    bool lastWasSelect = true;

    struct iovec ioVecs1[3];
    ioVecs1[0].iov_base = &(blobProgress->fBlockCnt);
    ioVecs1[0].iov_len = sizeof(blobProgress->fBlockCnt);
    ioVecs1[1].iov_base = &(blobProgress->fCompletionIndicator);
    ioVecs1[1].iov_len = sizeof(blobProgress->fCompletionIndicator);
    ioVecs1[2].iov_base = &(blobProgress->fHeader1);
    ioVecs1[2].iov_len = sizeof(blobProgress->fHeader1);
    ret = 0;
    while ( blobProgress->fBytesRead < sizeof(blobProgress->fBlockCnt)+sizeof(blobProgress->fCompletionIndicator)+sizeof(blobProgress->fHeader1) )
	{
	int count;
	if ( blobProgress->fBytesRead < sizeof(blobProgress->fBlockCnt) )
	    {
	    ioVecs1[0].iov_base = ((uint8*)&(blobProgress->fBlockCnt))+blobProgress->fBytesRead;
	    ioVecs1[0].iov_len = sizeof(blobProgress->fBlockCnt)-blobProgress->fBytesRead;
	    ioVecs1[1].iov_base = ((uint8*)&(blobProgress->fCompletionIndicator));
	    ioVecs1[1].iov_len = sizeof(blobProgress->fCompletionIndicator);
	    ioVecs1[2].iov_base = &(blobProgress->fHeader1);
	    ioVecs1[2].iov_len = sizeof(blobProgress->fHeader1);
	    count=3;
	    }
	else if ( blobProgress->fBytesRead < sizeof(blobProgress->fBlockCnt)+sizeof(blobProgress->fCompletionIndicator) )
	    {
	    ioVecs1[0].iov_base = ((uint8*)&(blobProgress->fCompletionIndicator))+blobProgress->fBytesRead-sizeof(blobProgress->fBlockCnt);
	    ioVecs1[0].iov_len = sizeof(blobProgress->fCompletionIndicator)-(blobProgress->fBytesRead-sizeof(blobProgress->fBlockCnt));
	    ioVecs1[1].iov_base = &(blobProgress->fHeader1);
	    ioVecs1[1].iov_len = sizeof(blobProgress->fHeader1);
	    count=2;
	    }
	else
	    {
	    ioVecs1[0].iov_base = ((uint8*)&(blobProgress->fHeader1))+(blobProgress->fBytesRead-(sizeof(blobProgress->fBlockCnt)+sizeof(blobProgress->fCompletionIndicator)));
	    ioVecs1[0].iov_len = sizeof(blobProgress->fHeader1)-(blobProgress->fBytesRead-(sizeof(blobProgress->fBlockCnt)+sizeof(blobProgress->fCompletionIndicator)));
	    count=1;
	    }
 	ret = fComHelper.ReadV( con, ioVecs1, count );
	if ( ret<0 && errno!=EAGAIN )
	    break;
	if ( ret==0 && lastWasSelect )
	    break;
	lastWasSelect = false;
	looped = true;
	if ( ret > 0 )
	    {
	    totalRead += ret;
	    if ( blobProgress->fBytesRead<sizeof(blobProgress->fBlockCnt) && ret+blobProgress->fBytesRead>=sizeof(blobProgress->fBlockCnt) )
		blobProgress->fBlockCnt = ntohl(blobProgress->fBlockCnt);
	    blobProgress->fBytesRead += ret;
	    }
	if ( blobProgress->fBytesRead >= sizeof(blobProgress->fBlockCnt)+sizeof(blobProgress->fCompletionIndicator)+sizeof(blobProgress->fHeader1) )
	    break;

	do
	    {
#ifdef TCPCOM_NO_POLL
	    FD_ZERO( &sockets );
	    FD_SET( blobProgress->fSocket, &sockets );
	    tv.tv_sec = tv.tv_usec = 0;
	    errno = 0;
	    ret = select( blobProgress->fSocket+1, &sockets, NULL, NULL, &tv );
#else
	    pollfd pollSock = { blobProgress->fSocket, POLLIN, 0 };
	    errno = 0;
	    ret = poll( &pollSock, 1, 0 );
#endif
	    lastWasSelect = true;
	    }
	while ( ret<=0 && (errno==EINTR || errno==EAGAIN) );
	if ( ret!=1 )
	    {
	    if ( !found )
		fBlobProgress.Add( bpd );
	    if ( resetTimeout )
		setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	    return totalRead;
	    }
	}

    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Blob New Data Receive", "Data Read" )
	<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
	<< blobProgress->fBytesRead << " bytes read - ret: " << ret << "." << ENDLOG;


    if ( blobProgress->fBytesRead < sizeof(blobProgress->fBlockCnt)+sizeof(blobProgress->fCompletionIndicator)+sizeof(blobProgress->fHeader1) && ret<=0 )
	{
	if ( ret < 0 )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob New Data Receive", "Read Address Error" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		<< "Error reading blob header header from socket " << con.fSocket
		<< ": " << strerror(errno) << " (" << errno << ")." << ENDLOG;
	    BlobReceiveError( ret, this, blobProgress->fHeader1.fTransferID, NULL );
	    }
	else
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Blob New Data Receive", "Connection closed" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		<< "Incoming connection closed while reading blob header header from socket " << con.fSocket
		<< "." << ENDLOG;
	    }
	// XXX
	if ( found )
	    fBlobProgress.Remove( bpNdx );
	if ( resetTimeout )
	    setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	return -1;
	}
    if ( looped )
	{
	nd.SetTo( (BCLNetworkDataStruct*)&(blobProgress->fHeader1) );
	if ( nd.GetData()->fLength != sizeof(BCLIntTCPBlobComHeaderStruct) )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP New Data Receive", "Header Size Error" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		<< "Blob com header read from socket " << con.fSocket << " has wrong size: " 
		<< nd.GetData()->fLength
		<< " (0x" << MLUCLog::kHex << nd.GetData()->fLength
		<< ") (original byte order: 0x" << MLUCLog::kHex << blobProgress->fHeader1.fLength
		<< " (" << MLUCLog::kDec << blobProgress->fHeader1.fLength << ")) (expected "
		<< MLUCLog::kDec << sizeof(BCLIntTCPBlobComHeaderStruct) << " (0x" << MLUCLog::kHex
		<< sizeof(BCLIntTCPBlobComHeaderStruct) << "))." << ENDLOG;

	    // XXX
	    // Read in rest of structure according to passed length??
	  
	    // XXX
	    BlobReceiveError( EMSGSIZE, this, blobProgress->fHeader1.fTransferID, NULL );
	    if ( found )
		fBlobProgress.Remove( bpNdx );
	    if ( resetTimeout )
		setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	    return -1;
	    }
// 	tbch.SetTo( &(blobProgress->fHeader1) );
// 	blobProgress->fHeader1 = *(tbch.GetData());
	tbch.AdoptFully( &(blobProgress->fHeader1) );
	}

    // XXX TEST
#if 0
#warning Artifical size limitation for testing purposes
#define ALLOWED (8*1024*1024)
    if ( blobProgress->fBlockCnt*sizeof(*(blobProgress->fHeaders)) > ALLOWED )
	{
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP New Data Receive", "Size Error" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		<< "Blob com header read from socket " << con.fSocket << " gives too much data: " 
		<< blobProgress->fBlockCnt*sizeof(*(blobProgress->fHeaders))
		<< " (0x" << MLUCLog::kHex << blobProgress->fBlockCnt*sizeof(*(blobProgress->fHeaders))
		<< ") (allowed: " << MLUCLog::kDec << ALLOWED << " (0x"
		<< MLUCLog::kHex << ALLOWED << "))." << ENDLOG;

	    // XXX
	    // Read in rest of structure according to passed length??
	  
	    // XXX
	    BlobReceiveError( EMSGSIZE, this, blobProgress->fHeader1.fTransferID, NULL );
	    if ( found )
		fBlobProgress.Remove( bpNdx );
	    if ( resetTimeout )
		setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	    return -1;
	}
#endif

    if ( blobProgress->fBlockCnt>1 )
	{
	if ( !blobProgress->fHeaders )
	    {
	    blobProgress->fHeaders = (BCLIntTCPBlobComHeaderStruct*)fHeaderAllocCache.Get( blobProgress->fBlockCnt*sizeof(*(blobProgress->fHeaders)) );
	    if ( blobProgress->fHeaders )
		memcpy( blobProgress->fHeaders, &(blobProgress->fHeader1), sizeof(blobProgress->fHeader1) );
	    }
	if ( !blobProgress->fHeaders )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP New Data Receive", "Out of memory" )
		<< "Out of memory trying to allocate " << MLUCLog::kDec <<blobProgress->fBlockCnt 
		<< " blob com headers of " << blobProgress->fBlockCnt*sizeof(*(blobProgress->fHeaders))
		<< " bytes." << ENDLOG;
	    if ( !found )
		fBlobProgress.Add( bpd );
	    if ( resetTimeout )
		setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	    return totalRead;
	    }
	while ( blobProgress->fBytesRead < sizeof(blobProgress->fBlockCnt)+sizeof(blobProgress->fCompletionIndicator)+blobProgress->fBlockCnt*sizeof(blobProgress->fHeader1) )
	    {
	    ret = fComHelper.Read( con, ((uint8*)blobProgress->fHeaders)+blobProgress->fBytesRead-(sizeof(blobProgress->fBlockCnt)+sizeof(blobProgress->fCompletionIndicator)),
				   sizeof(blobProgress->fBlockCnt)+sizeof(blobProgress->fCompletionIndicator)+blobProgress->fBlockCnt*sizeof(blobProgress->fHeader1)-blobProgress->fBytesRead, true );
	    if ( ret<0 && errno!=EAGAIN )
		break;
	    if ( ret==0 && lastWasSelect )
		break;
	    lastWasSelect = false;
	    looped = true;
	    if ( ret > 0 )
		{
		totalRead += ret;
		blobProgress->fBytesRead += ret;
		}
	    if ( blobProgress->fBytesRead >= sizeof(blobProgress->fBlockCnt)+sizeof(blobProgress->fCompletionIndicator)+blobProgress->fBlockCnt*sizeof(blobProgress->fHeader1) )
		break;
	    
	    do
		{
#ifdef TCPCOM_NO_POLL
		FD_ZERO( &sockets );
		FD_SET( blobProgress->fSocket, &sockets );
		tv.tv_sec = tv.tv_usec = 0;
		errno = 0;
		ret = select( blobProgress->fSocket+1, &sockets, NULL, NULL, &tv );
#else
		pollfd pollSock = { blobProgress->fSocket, POLLIN, 0 };
		errno = 0;
		ret = poll( &pollSock, 1, 0 );
#endif
		lastWasSelect = true;
		}
	    while ( ret<=0 && (errno==EINTR || errno==EAGAIN) );
	    if ( ret!=1 )
		{
		if ( !found )
		    fBlobProgress.Add( bpd );
		if ( resetTimeout )
		    setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
		return totalRead;
		}
	    }

	LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Blob New Data Receive", "Data Read" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
	    << blobProgress->fBytesRead << " bytes read - ret: " << ret << "." << ENDLOG;
	

	if ( blobProgress->fBytesRead < sizeof(blobProgress->fBlockCnt)+sizeof(blobProgress->fCompletionIndicator)+blobProgress->fBlockCnt*sizeof(blobProgress->fHeader1) && ret<=0 )
	    {
	    if ( ret < 0 )
		{
		LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob New Data Receive", "Read blob com headers Error" )
		    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		    << "Error reading blob com headers from socket " << con.fSocket
		    << ": " << strerror(errno) << " (" << errno << ")." << ENDLOG;
		BlobReceiveError( ret, this, blobProgress->fHeader1.fTransferID, NULL );
		}
	    else
		{
		LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Blob New Data Receive", "Connection closed" )
		    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		    << "Incoming connection closed while reading blob com headers from socket " << con.fSocket
		    << "." << ENDLOG;
		}
	    // XXX
	    if ( blobProgress->fHeaders )
		fHeaderAllocCache.Release( (uint8*)blobProgress->fHeaders );
	    if ( found )
		fBlobProgress.Remove( bpNdx );
	    if ( resetTimeout )
		setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	    return -1;
	    }
	if ( looped )
	    {
	    for ( uint32 n = 1; n < blobProgress->fBlockCnt; n++ )
		{
		nd.SetTo( (BCLNetworkDataStruct*)&(blobProgress->fHeaders[n]) );
		if ( nd.GetData()->fLength != sizeof(BCLIntTCPBlobComHeaderStruct) )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP New Data Receive", "Blob Com Header Size Error" )
			<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
			<< "Blob com header " << n << " read from socket " << con.fSocket << " has wrong size: " 
			<< nd.GetData()->fLength
			<< " (0x" << MLUCLog::kHex << nd.GetData()->fLength
			<< ") (original bytes order: 0x" << MLUCLog::kHex << blobProgress->fHeader1.fLength
			<< " (" << MLUCLog::kDec << blobProgress->fHeader1.fLength << ")) (expected "
			<< MLUCLog::kDec << sizeof(BCLIntTCPBlobComHeaderStruct) << " (0x" << MLUCLog::kHex
			<< sizeof(BCLIntTCPBlobComHeaderStruct) << "))." << ENDLOG;
		    // XXX
		    // Read in rest of structure according to passed length??
		    
		    // XXX
		    BlobReceiveError( EMSGSIZE, this, blobProgress->fHeader1.fTransferID, NULL );
		    if ( blobProgress->fHeaders )
			fHeaderAllocCache.Release( (uint8*)blobProgress->fHeaders );
		    if ( found )
			fBlobProgress.Remove( bpNdx );
		    if ( resetTimeout )
			setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
		    return -1;
		    }
// 		tbch.SetTo( &(blobProgress->fHeaders[n]) );
// 		blobProgress->fHeaders[n] = *(tbch.GetData());
		tbch.AdoptFully( &(blobProgress->fHeaders[n]) );
		}
	    }
	}
    else
	blobProgress->fHeaders = &(blobProgress->fHeader1);

    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Blob New Data Receive", "Data Read" )
	<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
	<< blobProgress->fBytesRead << " bytes read - ret: " << ret << "." << ENDLOG;



    struct iovec ioVecs2[blobProgress->fBlockCnt];
    const unsigned long startBytes = sizeof(blobProgress->fBlockCnt)+sizeof(blobProgress->fCompletionIndicator)+blobProgress->fBlockCnt*sizeof(blobProgress->fHeader1);
    unsigned long toRead[blobProgress->fBlockCnt];
    toRead[0] = blobProgress->fHeaders[0].fBlockSize;
    for ( unsigned long n = 1; n < blobProgress->fBlockCnt; n++ )
	{
	toRead[n] = toRead[n-1]+blobProgress->fHeaders[n].fBlockSize;
	}
    const unsigned long totalToRead = toRead[blobProgress->fBlockCnt-1];

    TTTCPBlockDataContType::iterator blIter, blEnd;
    unsigned long transferBlockSize=0;
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	{
	// XXX
	}
    blIter = fUsedBlocks.begin();
    blEnd = fUsedBlocks.end();
    while ( blIter != blEnd )
	{
	if ( blIter->fBlockOffset == blobProgress->fHeaders[0].fTransferID )
	    {
	    transferBlockSize = blIter->fBlockSize;
	    break;
	    }
	blIter++;
	}
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	{
	// XXX
	}
    if ( blIter==blEnd )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob New Data Receive", "Internal Error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
	    << "Blob transfer ID received (" << blobProgress->fHeaders[0].fTransferID << " (0x"
	    << MLUCLog::kHex << blobProgress->fHeaders[0].fTransferID << ") is not known."
	    << ENDLOG;
	BlobReceiveError( EMSGSIZE, this, blobProgress->fHeader1.fTransferID, NULL );
	if ( blobProgress->fHeaders && blobProgress->fHeaders!=&(blobProgress->fHeader1) )
	    fHeaderAllocCache.Release( (uint8*)blobProgress->fHeaders );
	if ( found )
	    fBlobProgress.Remove( bpNdx );
	if ( resetTimeout )
	    setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	return -1;
	}
    for ( unsigned long n = 0; n < blobProgress->fBlockCnt; n++ )
	{
	if ( blobProgress->fHeaders[n].fBlockOffset+blobProgress->fHeaders[n].fBlockSize > transferBlockSize )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob New Data Receive", "Internal Error" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		<< "Blob transfer block " << n << " (offset " << blobProgress->fHeaders[n].fBlockOffset << " (0x"
		<< MLUCLog::kHex << blobProgress->fHeaders[n].fBlockOffset << ") / size " << MLUCLog::kDec
		<< blobProgress->fHeaders[n].fBlockSize << " (0x" << MLUCLog::kHex
		<< blobProgress->fHeaders[n].fBlockSize << ") reaches over end of reserved transfer block area (size "
		<< MLUCLog::kDec << transferBlockSize << " (0x" << MLUCLog::kHex << transferBlockSize
		<< ")."
		<< ENDLOG;
	    BlobReceiveError( EMSGSIZE, this, blobProgress->fHeader1.fTransferID, NULL );
	    if ( blobProgress->fHeaders && blobProgress->fHeaders!=&(blobProgress->fHeader1) )
		fHeaderAllocCache.Release( (uint8*)blobProgress->fHeaders );
	    if ( found )
		fBlobProgress.Remove( bpNdx );
	    if ( resetTimeout )
		setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	    return -1;
	    }
	}
    uint8* blobBase = fBlobBuffer+blobProgress->fHeaders[0].fTransferID;

    unsigned long tempOffset, tempCount, bi, nextBuffer = 0;
    while ( nextBuffer<blobProgress->fBlockCnt && toRead[nextBuffer]<=blobProgress->fBytesRead-startBytes )
	nextBuffer++;
    looped = false;
    while ( blobProgress->fBytesRead<startBytes+totalToRead )
	{
	tempCount=0;
	bi = nextBuffer;
	if ( bi>0 )
	    tempOffset = blobProgress->fBytesRead-startBytes-toRead[bi-1];
	else
	    tempOffset = blobProgress->fBytesRead-startBytes;
	while ( bi < blobProgress->fBlockCnt )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP blob receive", "Buffer offsets" )
		<< "Block " << MLUCLog::kDec << tempCount << " is being read into to offset "
		<< blobProgress->fHeaders[bi].fBlockOffset << " + " << tempOffset 
		<< " = " << blobProgress->fHeaders[bi].fBlockOffset+tempOffset
		<< " - Length to be read: " << blobProgress->fHeaders[bi].fBlockSize-tempOffset << " of "
		<< blobProgress->fHeaders[bi].fBlockSize << " bytes." << ENDLOG;
	    ioVecs2[tempCount].iov_base = blobBase+blobProgress->fHeaders[bi].fBlockOffset+tempOffset;
	    ioVecs2[tempCount].iov_len = blobProgress->fHeaders[bi].fBlockSize-tempOffset;
	    bi++;
	    tempCount++;
	    tempOffset = 0;
	    }
	ret = fComHelper.ReadV( con, ioVecs2, tempCount, true );
	if ( ret<0 && errno!=EAGAIN )
	    break;
	if ( ret==0 && lastWasSelect )
	    break;
	lastWasSelect = false;
	if ( ret > 0 )
	    {
	    totalRead += ret;
	    blobProgress->fBytesRead += ret;
	    if ( blobProgress->fBytesRead-startBytes >= totalToRead )
		break;
	    while ( nextBuffer<blobProgress->fBlockCnt && toRead[nextBuffer]<=blobProgress->fBytesRead-startBytes )
		nextBuffer++;
	    }
	looped = true;
	do
	    {
#ifdef TCPCOM_NO_POLL
	    FD_ZERO( &sockets );
	    FD_SET( blobProgress->fSocket, &sockets );
	    tv.tv_sec = tv.tv_usec = 0;
	    errno = 0;
	    ret = select( blobProgress->fSocket+1, &sockets, NULL, NULL, &tv );
#else
	    pollfd pollSock = { blobProgress->fSocket, POLLIN, 0 };
	    errno = 0;
	    ret = poll( &pollSock, 1, 0 );
#endif
	    lastWasSelect = true;
	    }
	while ( ret<=0 && (errno==EINTR || errno==EAGAIN) );
	if ( ret!=1 )
	    {
	    if ( !found )
		fBlobProgress.Add( bpd );
	    if ( resetTimeout )
		setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	    return totalRead;
	    }
	}

    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Blob New Data Receive", "Data Read" )
	<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
	<< blobProgress->fBytesRead << " bytes read - ret: " << ret << "." << ENDLOG;


    if ( blobProgress->fBytesRead<startBytes+totalToRead && ret<=0 )
	{
	if ( ret < 0 )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP Blob New Data Receive", "Read blob data Error" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		<< "Error reading blob data from socket " << con.fSocket
		<< ": " << strerror(errno) << " (" << errno << ")." << ENDLOG;
	    BlobReceiveError( ret, this, blobProgress->fHeader1.fTransferID, NULL );
	    }
	else
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Blob New Data Receive", "Connection closed" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		<< "Incoming connection closed while reading blob data from socket " << con.fSocket
		<< "." << ENDLOG;
	    }
	// XXX
	if ( blobProgress->fHeaders && blobProgress->fHeaders!=&(blobProgress->fHeader1) )
	    fHeaderAllocCache.Release( (uint8*)blobProgress->fHeaders );
	if ( found )
	    fBlobProgress.Remove( bpNdx );
	if ( resetTimeout )
	    setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	return -1;
	}
    


    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP Blob New Data Receive", "Data Read" )
	<< "DONE: Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
	<< blobProgress->fBytesRead << " bytes read - ret: " << ret << "." << ENDLOG;


    // #ifdef DEBUG
    //     LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP New Blob Data Receive", "Transfer ID" )
    // 	<< "Transfer ID is " << MLUCLog::kDec << blobProgress->fHeader.fTransferID
    // 	<< " (0x" << MLUCLog::kHex << blobProgress->fHeader.fTransferID << ")." 
    // 	<< ENDLOG;
    // #endif


    if ( blobProgress->fCompletionIndicator )
	{
#ifdef TCPCOM_NO_POLL
	struct timeval* ptv;
#endif
	unsigned long timeout = 0;
	do
	    {
#ifdef TCPCOM_NO_POLL
	    FD_ZERO( &sockets );
	    FD_SET( blobProgress->fSocket, &sockets );
	    if ( timeout != ~(uint32)0 )
		{
		tv.tv_sec = timeout/1000;
		tv.tv_usec = (timeout-tv.tv_sec*1000)*1000;
		ptv = &tv;
		}
	    else
		ptv = NULL;
	    //tv.tv_sec = tv.tv_usec = 0;
	    errno = 0;
	    ret = select( blobProgress->fSocket+1, NULL, &sockets, NULL, ptv );
#else
	    pollfd pollSock = { blobProgress->fSocket, POLLOUT, 0 };
	    errno = 0;
	    ret = poll( &pollSock, 1, (timeout != ~(uint32)0) ? timeout : -1 );
#endif
	    }
	while ( ret<=0 && (errno==EINTR || errno==EAGAIN) );
	TIMEMARK;
	if ( ret!=1 )
	    {
	    // 	if ( ret == 0 )
	    // 	    errno = ETIMEDOUT;
	    if ( ret != 0 )
		{
		LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP New Data Receive", "Transfer Complete Write Error" )
		    << "Socket " << MLUCLog::kDec << blobProgress->fSocket 
		    << " not ready to write transfer complete message: " << strerror(errno)
		    << " (" << errno << ")." << ENDLOG;
		//return true;
		}
	    }
	else
	    {
	    do
		{
		ret = write( blobProgress->fSocket, &(blobProgress->fHeader1.fTransferID), sizeof(uint32) );
		if ( ret != sizeof(uint32) )
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kError, "TCP New Data Receive", "Transfer Complete Write Error" )
			<< "Error writing transfer complete (transfer ID) to remote sender: " << strerror(errno)
			<< " (" << MLUCLog::kDec << errno << ")." << ENDLOG;
		    //return true;
		    }
		else
		    {
		    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP New Data Receive", "Transfer Complete Write" )
			<< "Transfer complete (transfer ID) 0x" << MLUCLog::kHex << blobProgress->fHeader1.fTransferID
			<< " (" << MLUCLog::kDec << blobProgress->fHeader1.fTransferID 
			<< ") written to remote sender: " << strerror(errno)
			<< " (" << MLUCLog::kDec << errno << ")." << ENDLOG;
		    }
		}
	    while ( ret<=0 && (errno==EINTR || errno==EAGAIN) );
	    }
	TIMEMARK;
	}

    if ( fReceiveNotificationEnabled )
	{
	TTCPReceivedBlobData blobData;
	blobData.fTransferID = blobProgress->fHeader1.fTransferID;
	fBlobReceivedSignal.Lock();
	for ( unsigned n = 0; n < blobProgress->fBlockCnt; n++ )
	    {
	    LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "BCLTCPBlobCommunication::NewData", "Transfer block" )
		<< "Transfer " << MLUCLog::kDec << blobProgress->fHeader1.fTransferID << " (0x" 
		    << MLUCLog::kHex << blobProgress->fHeader1.fTransferID << ") block offset: " << MLUCLog::kDec
		    << blobProgress->fHeaders[n].fBlockOffset << " (0x" << MLUCLog::kHex << blobProgress->fHeaders[n].fBlockOffset
		    << " - block size: " << MLUCLog::kDec << blobProgress->fHeaders[n].fBlockSize << " (0x" 
		    << MLUCLog::kDec << blobProgress->fHeaders[n].fBlockSize << ")." << ENDLOG;
	    blobData.fBlockOffset = blobProgress->fHeaders[n].fBlockOffset;
	    blobData.fBlockSize = blobProgress->fHeaders[n].fBlockSize;
	    blobData.fTransferBlocksLeft = blobProgress->fBlockCnt-n-1;
	    fReceivedBlobs.Add( blobData );
	    }
	fBlobReceivedSignal.Unlock();
	fBlobReceivedSignal.Signal();
	}

    // #ifdef DEBUG
    //     LOGC( gBCLLogLevelRef, MLUCLog::kDebug, "TCP New Blob Data Receive", "Transfer ID" )
    // 	<< "Transfer ID is " << MLUCLog::kDec << blobProgress->fHeader.fTransferID
    // 	<< " (0x" << MLUCLog::kHex << blobProgress->fHeader.fTransferID << ")." 
    // 	<< ENDLOG;
    // #endif

    TIMEMARK;
    if ( resetTimeout )
	setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
    if ( blobProgress->fHeaders && blobProgress->fHeaders!=&(blobProgress->fHeader1) )
	fHeaderAllocCache.Release( (uint8*)blobProgress->fHeaders );
    if ( found )
	fBlobProgress.Remove( bpNdx );
    con.fTimeout.tv_sec = con.fTimeout.tv_usec = 0;
    TIMEMARK;
    DUMP_TIMEMARKS;
    return totalRead;
    }

bool BCLTCPBlobCommunication::ConnectionTimeout( BCLIntTCPComHelper::TAcceptedConnectionData& con )
    {
    LOGC( gBCLLogLevelRef, MLUCLog::kInformational, "TCP Blob Receive", "Connection Timed Out" )
	<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << " connection on socket " << con.fSocket
	<< " timed out." << ENDLOG;
    return false;
    }

void BCLTCPBlobCommunication::ConnectionClosed( BCLIntTCPComHelper::TAcceptedConnectionData& con )
    {
    unsigned long bpNdx;
    if ( MLUCVectorSearcher<TCPBlobProgressData,int>::FindElement( fBlobProgress, &FindBlobProgressBySocket, con.fSocket, bpNdx ) )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kInformational, "TCP Blob Receive", "Connection Closed" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << " connection on socket " << con.fSocket
	    << " closed." << ENDLOG;
	fBlobProgress.Remove( bpNdx );
	}
    }


bool BCLTCPBlobCommunication::FindMsgByQueryID( const BCLIntTCPBlobComMsgStruct& msg, const uint32& reference )
    {
    BCLIntTCPBlobComMsg msgClass( &msg );
    return msgClass.GetData()->fQueryID == reference;
    }

bool BCLTCPBlobCommunication::FindBlobProgressBySocket( const TCPBlobProgressData& blobProgress, const int& refSocket )
    {
    return blobProgress.fSocket == refSocket;
    }


#endif

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

