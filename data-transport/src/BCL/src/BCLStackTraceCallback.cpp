/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLStackTraceCallback.hpp"
#include "BCLLog.hpp"
#include "MLUCLog.hpp"
#ifndef NO_STACK_TRACE
#include <execinfo.h>
#endif
#include <cstdlib>



BCLStackTraceCallback::BCLStackTraceCallback( int depth )
    {
    fDepth = (depth+2);
    fBuffer = (void**)malloc( sizeof(void*)*fDepth );
    if ( !fBuffer )
        fDepth = 0;
    pthread_mutex_init( &fMutex, NULL );
    }

BCLStackTraceCallback::~BCLStackTraceCallback()
    {
    if ( fBuffer )
	free( fBuffer );
    pthread_mutex_destroy( &fMutex );
    }


BCLErrorAction BCLStackTraceCallback::ConnectionTemporarilyLost( int, BCLCommunication*,
								 BCLAbstrAddressStruct* )
    {
    PrintTrace( "Connection temporarily lost" );
    return kAbort;
    }
BCLErrorAction BCLStackTraceCallback::ConnectionError( int, BCLCommunication*,
						       BCLAbstrAddressStruct* )
    {
    PrintTrace( "Connection Error" );
    return kAbort;
    }

BCLErrorAction BCLStackTraceCallback::BindError( int, BCLCommunication*,
						 BCLAbstrAddressStruct*, 
						 BCLMessageStruct* )
    {
    PrintTrace( "Bind Error" );
    return kAbort;
    }

BCLErrorAction BCLStackTraceCallback::MsgSendError( int, BCLCommunication*,
						    BCLAbstrAddressStruct*, 
						    BCLMessageStruct* )
    {
    PrintTrace( "Message Send Error" );
    return kAbort;
    }

BCLErrorAction BCLStackTraceCallback::MsgReceiveError( int, BCLCommunication*,
						       BCLAbstrAddressStruct*, 
						       BCLMessageStruct* )
    {
    PrintTrace( "Message Receive Error" );
    return kAbort;
    }

BCLErrorAction BCLStackTraceCallback::BlobPrepareError( int, BCLCommunication*,
							BCLAbstrAddressStruct* )
    {
    PrintTrace( "Blob Preparation Error" );
    return kAbort;
    }

BCLErrorAction BCLStackTraceCallback::BlobSendError( int, BCLCommunication*,
						     BCLAbstrAddressStruct*, 
						     BCLTransferID )
    {
    PrintTrace( "Blob Send Error" );
    return kAbort;
    }

BCLErrorAction BCLStackTraceCallback::BlobReceiveError( int, BCLCommunication*,
							BCLTransferID )
    {
    PrintTrace( "Blob Receive Error" );
    return kAbort;
    }

BCLErrorAction BCLStackTraceCallback::AddressError( int, BCLCommunication*,
						    BCLAbstrAddressStruct* )
    {
    PrintTrace( "Address Error" );
    return kAbort;
    }

BCLErrorAction BCLStackTraceCallback::GeneralError( int, BCLCommunication*,
						    BCLAbstrAddressStruct* )
    {
    PrintTrace( "General Error" );
    return kAbort;
    }


void BCLStackTraceCallback::PrintTrace( const char* text )
    {
#ifndef NO_STACK_TRACE
    if ( fDepth <= 0 )
	return;
    pthread_mutex_lock( &fMutex );
    int count, i;
    count = backtrace( fBuffer, fDepth );
    if ( !count )
	return;
    char** syms;
    syms = backtrace_symbols( fBuffer, count );
    if ( !syms )
	return;
    for ( i = 2; i < count; i++ )
	{
	LOGC( gBCLLogLevelRef, MLUCLog::kError, "Stacktrace", text )
	    << text << " stacktrace " << MLUCLog::kDec << i-2 << "/" << count-2 << ": "
	    << syms[i] << "." << ENDLOG;
	}
    free( syms );
    pthread_mutex_unlock( &fMutex );
#else
    LOGCG( gBCLLogLevelRef, MLUCLog::kError, "Stacktrace", "Not Supported" )
      << "Stacktrace not supported on this platform." << ENDLOG;
#endif
    }







/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
