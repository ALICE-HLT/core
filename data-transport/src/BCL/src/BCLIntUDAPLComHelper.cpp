/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLIntUDAPLComHelper.hpp"
#include <BCLUDAPLAddressOps.hpp>
#include <MLUCLog.hpp>
#include <cerrno>
#include <cstring>
#include <netinet/in.h>

const unsigned BCLIntUDAPLComHelper::gkConnectionBackLogs = 8;
const unsigned BCLIntUDAPLComHelper::gkEventQueueSize = 32;
const unsigned BCLIntUDAPLComHelper::gkMaxMessageSize = 1*1024*1024;
const unsigned BCLIntUDAPLComHelper::gkMaxRDMASize = 32*1024*1024;
const unsigned BCLIntUDAPLComHelper::gkMaxIOVs = 28;
const unsigned BCLIntUDAPLComHelper::gkPrivateMsgBufferSize = 32*1024;

void DumpData2Log( MLUCLog::TLogLevel severity, const char* origin, const char* kw, void* data, unsigned long len )
    {
    MLUCString tmpS;
    char tmpA[1024];
    for ( unsigned i = 0; i < len; i++ )
	{
	sprintf( tmpA, " 0x%02X", (unsigned)( ((uint8*)data)[i] ) );
	tmpS += tmpA;
	}
    LOG( severity, origin, kw )
	<< "Data (" << MLUCLog::kDec << len << " bytes):" << tmpS.c_str() << ENDLOG;
    }


BCLIntUDAPLComHelper::BCLIntUDAPLComHelper( BCLCommunication* com ):
    fAcceptedConnections( 6 ),
    fRemovedConnections( 3 ),
    fConnections( 5 ),
    fEventLoopThread( this )
    {
    pthread_mutex_init( &fAcceptedConnectionMutex, NULL );
    pthread_mutex_init( &fRemovedConnectionMutex, NULL );
    pthread_mutex_init( &fConnectionMutex, NULL );
    
    fCom= com;

    fConnectionTimeout = 5000; // in millisec.

    fDataReceivedCallback = NULL;

    fIA = DAT_HANDLE_NULL;
    fEVD = DAT_HANDLE_NULL;
    fPZ = DAT_HANDLE_NULL;
    fCNO = DAT_HANDLE_NULL;
    fEvtLoopEVD = DAT_HANDLE_NULL;

    fRecvEVD = DAT_HANDLE_NULL;
    fConnReqEVD = DAT_HANDLE_NULL;

    fRecvBufferAlloc = NULL;
    fRecvBufferSizeAlloc = 0;
    fRecvBuffer = 0;
    fRecvBufferSize = 0;
    fRecvLMRHandle = DAT_HANDLE_NULL;
    fRecvLMRContext = 0;
    fRecvRMRContext = 0;

    fSendBufferAlloc = NULL;
    fSendBufferSizeAlloc = 0;
    fSendBuffer = 0;
    fSendBufferSize = 0;
    fSendLMRHandle = DAT_HANDLE_NULL;

    fPSP = DAT_HANDLE_NULL;

    }

BCLIntUDAPLComHelper::~BCLIntUDAPLComHelper()
    {
    Unbind();
    pthread_mutex_destroy( &fAcceptedConnectionMutex );
    pthread_mutex_destroy( &fRemovedConnectionMutex );
    pthread_mutex_destroy( &fConnectionMutex );
    }

int BCLIntUDAPLComHelper::Bind( uint8* blobRecvBuffer, uint32 blobRecvSize, uint32 msgBufferRecvSize, uint32 sendBufferSize, BCLUDAPLAddressStruct* address, DataReceiveCallbackBase* drc )
    {
    DAT_RETURN ret;

    LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::Bind", "IA name" )
	<< "Interface adapter name: " << reinterpret_cast<char*>( address->fIAName ) << ENDLOG;
    ret = dat_ia_open( reinterpret_cast<char*>( address->fIAName ), 8, &fEVD, &fIA );
    if(ret != DAT_SUCCESS)
	{
	LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::BCLIntUDAPLComHelper", "Error opening adaptor" )
	    << "Error opening uDAPL adaptor: " << DatStrError(ret).c_str() << " ("
	    << MLUCLog::kDec << ret << ")." << ENDLOG;
	fIA = DAT_HANDLE_NULL;
	fEVD = DAT_HANDLE_NULL;
	return ENXIO;
	}
    else
	{
	LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::BCLIntUDAPLComHelper", "Adaptor opened" )
	    << "Opened uDAPL adaptor." << ENDLOG;
	}
    ret = dat_pz_create( fIA, &fPZ );
    if ( ret != DAT_SUCCESS )
	{
	LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::BCLIntUDAPLComHelper", "Error creating protection zone" )
	    << "Error creating uDAPL protection zone: " << DatStrError(ret).c_str() << " ("
	    << MLUCLog::kDec << ret << ")." << ENDLOG;
	dat_ia_close( fIA, DAT_CLOSE_ABRUPT_FLAG );
	fIA = DAT_HANDLE_NULL;
	fEVD = DAT_HANDLE_NULL;
	fPZ = DAT_HANDLE_NULL;
	return ENXIO;
	}
    else
	{
	LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::BCLIntUDAPLComHelper", "Protection zone created" )
	    << "Created uDAPL protection zone." << ENDLOG;
	}

    ret = dat_cno_create( fIA, DAT_OS_WAIT_PROXY_AGENT_NULL, &fCNO  );
    if ( ret != DAT_SUCCESS )
	{
	LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::BCLIntUDAPLComHelper", "Error creating consumer notification object" )
	    << "Error creating uDAPL consumer notification object: " << DatStrError(ret).c_str() << " ("
	    << MLUCLog::kDec << ret << ")." << ENDLOG;
	dat_pz_free( fPZ );
	dat_ia_close( fIA, DAT_CLOSE_ABRUPT_FLAG );
	fIA = DAT_HANDLE_NULL;
	fEVD = DAT_HANDLE_NULL;
	fPZ = DAT_HANDLE_NULL;
	return ENXIO;
	}
    else
	{
	LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::BCLIntUDAPLComHelper", "Consumer notification object created" )
	    << "Created uDAPL consumer notification object." << ENDLOG;
	}
    ret = dat_evd_modify_cno( fEVD, fCNO );
    if ( ret != DAT_SUCCESS )
	{
	LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::BCLIntUDAPLComHelper", "Error modifying evd cno" )
	    << "Error modifying uDAPL consumer notification object for interface adapter event dispatcher: " << DatStrError(ret).c_str() << " ("
	    << MLUCLog::kDec << ret << ")." << ENDLOG;
	dat_cno_free( fCNO );
	dat_pz_free( fPZ );
	dat_ia_close( fIA, DAT_CLOSE_ABRUPT_FLAG );
	fIA = DAT_HANDLE_NULL;
	fEVD = DAT_HANDLE_NULL;
	fPZ = DAT_HANDLE_NULL;
	fCNO = DAT_HANDLE_NULL;
	return ENXIO;
	}
    else
	{
	LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::BCLIntUDAPLComHelper", "Modified evd cno" )
	    << "Modified uDAPL consumer notification object for interface adapter event dispatcher." << ENDLOG;
	}
    ret = dat_evd_create( fIA, gkEventQueueSize, fCNO, DAT_EVD_SOFTWARE_FLAG, &fEvtLoopEVD  );
    if ( ret != DAT_SUCCESS )
	{
	LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::BCLIntUDAPLComHelper", "Error creating event loop evd" )
	    << "Error creating uDAPL event loop event dispatcher: " << DatStrError(ret).c_str() << " ("
	    << MLUCLog::kDec << ret << ")." << ENDLOG;
	dat_cno_free( fCNO );
	dat_pz_free( fPZ );
	dat_ia_close( fIA, DAT_CLOSE_ABRUPT_FLAG );
	fIA = DAT_HANDLE_NULL;
	fEVD = DAT_HANDLE_NULL;
	fPZ = DAT_HANDLE_NULL;
	fCNO = DAT_HANDLE_NULL;
	return ENXIO;
	}
    else
	{
	LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::BCLIntUDAPLComHelper", "Event loop evd created" )
	    << "Created uDAPL event loop event dispatcher." << ENDLOG;
	}

    ret = dat_evd_create( fIA, gkConnectionBackLogs, fCNO, DAT_EVD_CR_FLAG, &fConnReqEVD  );
    if ( ret != DAT_SUCCESS )
	{
	LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::Bind", "Error creating connection request evd" )
	    << "Error creating uDAPL connection request event dispatcher: " << DatStrError(ret).c_str() << " ("
	    << MLUCLog::kDec << ret << ")." << ENDLOG;
	return ENXIO;
	}
    else
	{
	LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::Bind", "Connection request evd created" )
	    << "Created uDAPL connection request event dispatcher." << ENDLOG;
	}
    
    //     ret = dat_evd_create( fIA, gkConnectionBackLogs, fCNO, DAT_EVD_CONNECTION_FLAG, &fConnEVD  );
    //     if ( ret != DAT_SUCCESS )
    // 	{
    // 	LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::Bind", "Error creating connection evd" )
    // 	    << "Error creating uDAPL connection event dispatcher: " << DatStrError(ret).c_str() << " ("
    // 	    << MLUCLog::kDec << ")." << ENDLOG;
    // 	dat_evd_free( fConnReqEVD );
    // 	return ENXIO;
    // 	}
    //     else
    // 	{
    // 	LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::Bind", "Connection evd created" )
    // 	    << "Created uDAPL connection event dispatcher." << ENDLOG;
    // 	}
    //     ret = dat_evd_create( fIA, gkEventQueueSize, fCNO, DAT_EVD_DTO_FLAG, &fReqEVD  );
    //     if ( ret != DAT_SUCCESS )
    // 	{
    // 	LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::Bind", "Error creating request evd" )
    // 	    << "Error creating uDAPL request event dispatcher: " << DatStrError(ret).c_str() << " ("
    // 	    << MLUCLog::kDec << ")." << ENDLOG;
    // 	dat_evd_free( fConnEVD );
    // 	dat_evd_free( fConnReqEVD );
    // 	return ENXIO;
    // 	}
    //     else
    // 	{
    // 	LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::Bind", "Request evd created" )
    // 	    << "Created uDAPL request event dispatcher." << ENDLOG;
    // 	}

    ret = dat_evd_create( fIA, gkEventQueueSize, fCNO, DAT_EVD_DTO_FLAG, &fRecvEVD  );
    if ( ret != DAT_SUCCESS )
	{
	LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::Bind", "Error creating receive evd" )
	    << "Error creating uDAPL receive event dispatcher: " << DatStrError(ret).c_str() << " ("
	    << MLUCLog::kDec << ret << ")." << ENDLOG;
	// 	dat_evd_free( fReqEVD );
	// 	dat_evd_free( fConnEVD );
	dat_evd_free( fConnReqEVD );
	return ENXIO;
	}
    else
	{
	LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::Bind", "Receive evd created" )
	    << "Created uDAPL receive event dispatcher." << ENDLOG;
	}

    DAT_REGION_DESCRIPTION region;
    DAT_MEM_PRIV_FLAGS lmr_flags;
    if ( blobRecvBuffer )
	{
	fRecvBufferSizeAlloc = blobRecvSize;
	fRecvBufferAlloc = blobRecvBuffer;
	fRecvBufferSize = 0;
	fRecvBuffer = 0;
	if ( !fRecvBufferAlloc )
	    {
	    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::Bind", "No receive buffer" )
		<< "No receive buffer specified." << ENDLOG;
	    dat_evd_free( fRecvEVD );
	    //  	dat_evd_free( fReqEVD );
	    //  	dat_evd_free( fConnEVD );
	    dat_evd_free( fConnReqEVD );
	    return ENOMEM;
	    }
	else
	    {
	    LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::Bind", "Using receive buffer" )
		<< "Using recv buffer of " << MLUCLog::kDec
		<< blobRecvSize << " (0x" << MLUCLog::kHex << blobRecvSize
		<< ") bytes." << ENDLOG;
	    }
	region.for_va = fRecvBufferAlloc;
	lmr_flags = (DAT_MEM_PRIV_FLAGS)( DAT_MEM_PRIV_ALL_FLAG|DAT_MEM_PRIV_REMOTE_WRITE_FLAG );
	ret = dat_lmr_create( fIA,
			      DAT_MEM_TYPE_VIRTUAL,
			      region,
			      fRecvBufferSizeAlloc,
			      fPZ,
			      lmr_flags,
			      &fRecvLMRHandle,
			      &fRecvLMRContext,
			      &fRecvRMRContext,
			      &fRecvBufferSize,
			      &fRecvBuffer );
	if ( ret != DAT_SUCCESS )
	    {
	    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::Bind", "Error creating receive lmr" )
		<< "Error creating local receive memory region: " << DatStrError(ret).c_str() << " ("
		<< MLUCLog::kDec << ret << ")." << ENDLOG;
	    // 	dat_evd_free( fSendEVD );
	    dat_evd_free( fRecvEVD );
	    // 	dat_evd_free( fReqEVD );
	    // 	dat_evd_free( fConnEVD );
	    dat_evd_free( fConnReqEVD );
	    return ENXIO;
	    }
	else
	    {
	    LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::Bind", "Created receive lmr" )
		<< "Created local receive memory region." << ENDLOG;
	    }
	}

    if ( msgBufferRecvSize>0 )
	{
	fMsgRecvBufferSizeAlloc = msgBufferRecvSize;
	fMsgRecvBufferAlloc = (uint8*)malloc( msgBufferRecvSize );
	fMsgRecvBufferSize = 0;
	fMsgRecvBuffer = 0;
	if ( !fMsgRecvBufferAlloc )
	    {
	    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::Bind", "Out of memory msg receive buffer" )
		<< "Out of memory trying to allocate message receive buffer of " << MLUCLog::kDec
		<< msgBufferRecvSize << " (0x" << MLUCLog::kHex << msgBufferRecvSize
		<< ") bytes." << ENDLOG;
	    dat_lmr_free( fRecvLMRHandle );
	    dat_evd_free( fRecvEVD );
	    //  	dat_evd_free( fReqEVD );
	    //  	dat_evd_free( fConnEVD );
	    dat_evd_free( fConnReqEVD );
	    return ENOMEM;
	    }
	else
	    {
	    LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::Bind", "Created msg receive buffer" )
		<< "Created message receive buffer of " << MLUCLog::kDec
		<< msgBufferRecvSize << " (0x" << MLUCLog::kHex << msgBufferRecvSize
		<< ") bytes." << ENDLOG;
	    }
	region.for_va = fMsgRecvBufferAlloc;
	DAT_MEM_PRIV_FLAGS lmr_flags;
	lmr_flags = (DAT_MEM_PRIV_FLAGS)( DAT_MEM_PRIV_ALL_FLAG|DAT_MEM_PRIV_REMOTE_WRITE_FLAG );
	ret = dat_lmr_create( fIA,
			      DAT_MEM_TYPE_VIRTUAL,
			      region,
			      fMsgRecvBufferSizeAlloc,
			      fPZ,
			      lmr_flags,
			      &fMsgRecvLMRHandle,
			      &fMsgRecvLMRContext,
			      &fMsgRecvRMRContext,
			      &fMsgRecvBufferSize,
			      &fMsgRecvBuffer );
	if ( ret != DAT_SUCCESS )
	    {
	    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::Bind", "Error creating receive lmr" )
		<< "Error creating local receive memory region: " << DatStrError(ret).c_str() << " ("
		<< MLUCLog::kDec << ret << ")." << ENDLOG;
	    // 	dat_evd_free( fSendEVD );
	    free( fMsgRecvBufferAlloc );
	    dat_lmr_free( fRecvLMRHandle );
	    dat_evd_free( fRecvEVD );
	    // 	dat_evd_free( fReqEVD );
	    // 	dat_evd_free( fConnEVD );
	    dat_evd_free( fConnReqEVD );
	    return ENXIO;
	    }
	else
	    {
	    LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::Bind", "Created receive lmr" )
		<< "Created local receive memory region." << ENDLOG;
	    }
	}


    if ( sendBufferSize > 0 )
        {
	fSendBufferSizeAlloc = sendBufferSize;
	fSendBufferAlloc = (uint8*)malloc( sendBufferSize );
	fSendBufferSize = 0;
	fSendBuffer = 0;
	if ( !fSendBufferAlloc )
	    {
	    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::Bind", "Out of memory" )
		<< "Out of memory trying to allocate send buffer of " << MLUCLog::kDec
		<< sendBufferSize << " (0x" << MLUCLog::kHex << sendBufferSize
		<< ") bytes." << ENDLOG;
	    dat_lmr_free( fMsgRecvLMRHandle );
	    free( fMsgRecvBufferAlloc );
	    dat_lmr_free( fRecvLMRHandle );
	    // 	dat_evd_free( fSendEVD );
	    dat_evd_free( fRecvEVD );
	    // 	dat_evd_free( fReqEVD );
	    // 	dat_evd_free( fConnEVD );
	    dat_evd_free( fConnReqEVD );
	    return ENOMEM;
	    }
	else
	    {
	    LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::Bind", "Created send buffer" )
		<< "Created send buffer of " << MLUCLog::kDec
		<< sendBufferSize << " (0x" << MLUCLog::kHex << sendBufferSize
		<< ") bytes." << ENDLOG;
	    }
	region.for_va = fSendBufferAlloc;
	lmr_flags = (DAT_MEM_PRIV_FLAGS)( DAT_MEM_PRIV_ALL_FLAG|DAT_MEM_PRIV_REMOTE_WRITE_FLAG );
	ret = dat_lmr_create( fIA,
			      DAT_MEM_TYPE_VIRTUAL,
			      region,
			      fSendBufferSizeAlloc,
			      fPZ,
			      lmr_flags,
			      &fSendLMRHandle,
			      &fSendLMRContext,
			      &fSendRMRContext,
			      &fSendBufferSize,
			      &fSendBuffer );
	if ( ret != DAT_SUCCESS )
	    {
	    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::Bind", "Error creating send lmr" )
		<< "Error creating local send memory region: " << DatStrError(ret).c_str() << " ("
		<< MLUCLog::kDec << ret << ")." << ENDLOG;
	    free( fSendBufferAlloc );
	    dat_lmr_free( fMsgRecvLMRHandle );
	    free( fMsgRecvBufferAlloc );
	    dat_lmr_free( fRecvLMRHandle );
	    // 	dat_evd_free( fSendEVD );
	    dat_evd_free( fRecvEVD );
	    //  	dat_evd_free( fReqEVD );
	    // 	dat_evd_free( fConnEVD );
	    dat_evd_free( fConnReqEVD );
	    return ENXIO;
	    }
	else
	    {
	    LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::Bind", "Created send lmr" )
		<< "Created local send memory region." << ENDLOG;
	    }
	}

    fAddress = *address;
    ret = dat_psp_create( fIA,
			  fAddress.fConnQual,
			  fConnReqEVD,
			  DAT_PSP_CONSUMER_FLAG,
			  &fPSP );
    if ( ret != DAT_SUCCESS )
	{
	LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::Bind", "Error creating psp" )
	    << "Error creating public service point: " << DatStrError(ret).c_str() << " ("
	    << MLUCLog::kDec << ret << ")." << ENDLOG;
	free( fSendBufferAlloc );
	dat_lmr_free( fSendLMRHandle );
	dat_lmr_free( fMsgRecvLMRHandle );
	free( fMsgRecvBufferAlloc );
	dat_lmr_free( fRecvLMRHandle );
	// 	dat_evd_free( fSendEVD );
	dat_evd_free( fRecvEVD );
	// 	dat_evd_free( fReqEVD );
	// 	dat_evd_free( fConnEVD );
	dat_evd_free( fConnReqEVD );
	return ENXIO;
	}
    else
	{
	LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::Bind", "Created psp" )
	    << "Created public service point." << ENDLOG;
	}

    fDataReceivedCallback = drc;
    
    fQuitEventLoop = false;
    fEventLoopQuitted = true;
    fEventLoopThread.Start();

    return 0;
    }

int BCLIntUDAPLComHelper::Unbind()
    {
    fQuitEventLoop = true;
    
    DAT_EVENT event;
    memset( &event, 0, sizeof(event) );
    event.event_number = DAT_SOFTWARE_EVENT;
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 1000000;
    gettimeofday( &start, NULL );
    while ( !fEventLoopQuitted && deltaT<timeLimit )
	{
	dat_evd_post_se( fEvtLoopEVD, &event );
	usleep( 0 );
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    if ( fEventLoopQuitted )
	fEventLoopThread.Join();
    else
	{
	fEventLoopThread.Abort();
	fEventLoopQuitted = true;
	}

    if ( fPSP != DAT_HANDLE_NULL )
	dat_psp_free( fPSP );
    fPSP = DAT_HANDLE_NULL;

    if ( fSendLMRHandle != DAT_HANDLE_NULL )
	dat_lmr_free( fSendLMRHandle );
    fSendLMRHandle = DAT_HANDLE_NULL;
    //     fSendLMRContext = DAT_HANDLE_NULL;

    if ( fSendBufferAlloc )
	free( fSendBufferAlloc );
    fSendBufferAlloc = NULL;
    fSendBufferSizeAlloc = 0;
    fSendBuffer = 0;
    fSendBufferSize = 0;

    if ( fMsgRecvLMRHandle != DAT_HANDLE_NULL )
	dat_lmr_free( fMsgRecvLMRHandle );
    fMsgRecvLMRHandle = DAT_HANDLE_NULL;
    //     fMsgRecvLMRContext = DAT_HANDLE_NULL;

    if ( fMsgRecvBufferAlloc )
	free( fMsgRecvBufferAlloc );
    fMsgRecvBufferAlloc = NULL;
    fMsgRecvBufferSizeAlloc = 0;
    fMsgRecvBuffer = 0;
    fMsgRecvBufferSize = 0;

    if ( fRecvLMRHandle != DAT_HANDLE_NULL )
	dat_lmr_free( fRecvLMRHandle );
    fRecvLMRHandle = DAT_HANDLE_NULL;
    //     fRecvLMRContext = DAT_HANDLE_NULL;
    //     fRecvRMRContext = DAT_HANDLE_NULL;

    fRecvBufferAlloc = NULL;
    fRecvBufferSizeAlloc = 0;
    fRecvBuffer = 0;
    fRecvBufferSize = 0;

    //     if ( fSendEVD != DAT_HANDLE_NULL )
    // 	dat_evd_free( fSendEVD );
    
    //     dat_evd_free( fReqEVD );
    // 	dat_evd_free( fConnEVD );
    if ( fRecvEVD != DAT_HANDLE_NULL )
	dat_evd_free( fRecvEVD );
    fRecvEVD = DAT_HANDLE_NULL;

    if ( fConnReqEVD != DAT_HANDLE_NULL )
	dat_evd_free( fConnReqEVD );
    fConnReqEVD = DAT_HANDLE_NULL;

    // Post notification to terminate loop.
    if ( fEvtLoopEVD != DAT_HANDLE_NULL )
	dat_evd_free( fEvtLoopEVD );
    fEvtLoopEVD = DAT_HANDLE_NULL;
    if ( fCNO != DAT_HANDLE_NULL )
	dat_cno_free( fCNO );
    fCNO = DAT_HANDLE_NULL;
    if ( fPZ != DAT_HANDLE_NULL )
	dat_pz_free( fPZ );
    fPZ = DAT_HANDLE_NULL;
    if ( fIA != DAT_HANDLE_NULL )
	dat_ia_close( fIA, DAT_CLOSE_ABRUPT_FLAG );
    fIA = DAT_HANDLE_NULL;
    fEVD = DAT_HANDLE_NULL;

    return 0;
    }

int BCLIntUDAPLComHelper::Connect( BCLUDAPLAddressStruct* address, bool useTimeout, uint32 timeout_ms, bool openOnDemand )
    {
    ConnectionData connectData;
    connectData.fAddress = *address;
    connectData.fUsageCount = 0;
    int ret;
    ret = ConnectToRemote( &connectData, !openOnDemand, useTimeout, timeout_ms );
    if ( ret )
	{
	LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::Connect", "Connection error" )
	    << "Address 0x" << MLUCLog::kHex
	    << (address ? address->fNodeID : 0xFFFFFFFF) << " (" << MLUCLog::kDec << (address ? address->fNodeID : 0xFFFFFFFF) << ") : "
	    << (address ? address->fConnQual : 0xFFFF) << " (0x" << MLUCLog::kHex << (address ? address->fConnQual : 0xFFFF)
	    << " UDAPL connection error" << ENDLOG;
	ConnectionError( ret, fCom, address );
	return ret;
	}
    return 0;
    }

int BCLIntUDAPLComHelper::Disconnect( BCLUDAPLAddressStruct* address, bool useTimeout, uint32 timeout_ms )
    {
    int ret;
    ConnectionData connectData;
    connectData.fAddress = *address;
    connectData.fUsageCount = 0;
    ret = DisconnectFromRemote( &connectData, useTimeout, timeout_ms );
    return ret;
    }



int BCLIntUDAPLComHelper::ConnectToRemote( ConnectionData* connectData, bool now, bool useTimeout, uint32 timeout_ms )
    {
    int iret;
    bool found=false;
    unsigned long ndx=0;
    iret = pthread_mutex_lock( &fConnectionMutex );
    PConnectionData conn;
    if ( MLUCVectorSearcher<ConnectionData,PConnectionData>::FindElement( fConnections, &ConnectionSearchFunc, connectData, ndx ) )
	{
	found=true;
	conn = fConnections.GetPtr( ndx );
	conn->fUsageCount++;
	memcpy( connectData, conn, sizeof(*connectData) );
	if ( !now || conn->fEP != DAT_HANDLE_NULL )
	    {
	    iret = pthread_mutex_unlock( &fConnectionMutex );
	    return 0;
	    }
	}
    ConnectionData cd;
    if ( found )
	cd = *conn;
    iret = pthread_mutex_unlock( &fConnectionMutex );
    
    if ( now )
	{
	DAT_RETURN ret;

	ret = dat_evd_create( fIA, gkEventQueueSize, fCNO, DAT_EVD_DTO_FLAG, &cd.fDataEVD  ); // Leave out fCNO?
	if ( ret != DAT_SUCCESS )
	    {
	    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::ConnectToRemote", "Error creating connection data evd" )
		<< "Error creating uDAPL connection data event dispatcher: " << DatStrError(ret).c_str() << " ("
		<< MLUCLog::kDec << ret << ")." << ENDLOG;
	    return ENXIO;
	    }
	else
	    {
	    LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::ConnectToRemote", "Connection data evd created" )
		<< "Created uDAPL connection data event dispatcher." << ENDLOG;
	    }

	ret = dat_evd_create( fIA, gkEventQueueSize, DAT_HANDLE_NULL, DAT_EVD_CONNECTION_FLAG, &cd.fConnEVD  );
	if ( ret != DAT_SUCCESS )
	    {
	    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::ConnectToRemote", "Error creating connection evd" )
		<< "Error creating uDAPL connection event dispatcher: " << DatStrError(ret).c_str() << " ("
		<< MLUCLog::kDec << ret << ")." << ENDLOG;
	    dat_evd_free( cd.fDataEVD );
	    return ENXIO;
	    }
	else
	    {
	    LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::ConnectToRemote", "Connection evd created" )
		<< "Created uDAPL connection event dispatcher." << ENDLOG;
	    }

	// Note: Providing the request EVD is necessary, even though we do not expect to send anything,
	// as the call otherwise fails with an invalid handle error.
	ret = dat_ep_create( fIA, fPZ, cd.fDataEVD, cd.fDataEVD, cd.fConnEVD, 
			     NULL, &cd.fEP );
	if ( ret != DAT_SUCCESS )
	    {
	    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::ConnectToRemote", "Error creating connection ep" )
		<< "Error creating uDAPL connection endpoint: " << DatStrError(ret).c_str() << " ("
		<< MLUCLog::kDec << ret << ")." << ENDLOG;
	    // XXX Cleanup properly
	    dat_evd_free( cd.fConnEVD );
	    dat_evd_free( cd.fDataEVD );
	    return ENXIO;
	    }
	else
	    {
	    LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::ConnectToRemote", "Connection ep created" )
		<< "Created uDAPL connection endpoint." << ENDLOG;
	    }

	cd.fUsageCount = 1;
	cd.fAddress = connectData->fAddress;

	// Now try to connect to the remote side.
	struct sockaddr_in           remoteAddr;
	remoteAddr.sin_family = cd.fAddress.fSinFamily;
	remoteAddr.sin_port = cd.fAddress.fSinPort;
	remoteAddr.sin_addr.s_addr = cd.fAddress.fNodeID;
	DAT_TIMEOUT datTimeout;
	if ( useTimeout )
	    datTimeout = timeout_ms*1000;
	else
	    datTimeout = DAT_TIMEOUT_INFINITE;
	ret = dat_ep_connect( cd.fEP, (DAT_SOCK_ADDR*)&remoteAddr, cd.fAddress.fConnQual,
			      datTimeout,
			      0, (DAT_PVOID)0,
			      DAT_QOS_BEST_EFFORT, DAT_CONNECT_DEFAULT_FLAG
			    );
	if ( ret != DAT_SUCCESS )
	    {
	    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::ConnectToRemote", "Error connecting" )
		<< "Error connecting to remote uDAPL endpoint: " << DatStrError(ret).c_str() << " ("
		<< MLUCLog::kDec << ret << ")." << ENDLOG;
	    // XXX Cleanup properly
	    dat_ep_free( cd.fEP );
	    dat_evd_free( cd.fConnEVD );
	    dat_evd_free( cd.fDataEVD );
	    return ENXIO;
	    }
	else
	    {
	    LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::ConnectToRemote", "Connected to ep" )
		<< "Connected to remote uDAPL endpoint." << ENDLOG;
	    }

	if ( useTimeout )
	    datTimeout = timeout_ms*1000;
	else
	    datTimeout = DAT_TIMEOUT_INFINITE;
	DAT_EVENT event;
	DAT_COUNT qNr;
	memset( &event, 0, sizeof(event) );
	event.event_data.connect_event_data.private_data_size = 2*sizeof(DAT_RMR_TRIPLET);
	ret = dat_evd_wait( cd.fConnEVD, datTimeout, 1, &event, &qNr );
	if ( ret != DAT_SUCCESS )
	    {
	    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::ConnectToRemote", "Error waiting for connection" )
		<< "Error waiting for connection to remote uDAPL endpoint: " << DatStrError(ret).c_str() << " ("
		<< MLUCLog::kDec << ret << ")." << ENDLOG;
	    // XXX Cleanup properly
	    dat_ep_free( cd.fEP );
	    dat_evd_free( cd.fConnEVD );
	    dat_evd_free( cd.fDataEVD );
	    return ENXIO;
	    }
	else
	    {
	    LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::ConnectToRemote", "Connection to ep established" )
		<< "Connection established to remote uDAPL endpoint." << ENDLOG;
	    }
#if 0
	DumpData2Log( MLUCLog::kDebug, "BCLIntUDAPLComHelper::ConnectToRemote", "Connection Data received",
		      event.event_data.connect_event_data.private_data,
		      event.event_data.connect_event_data.private_data_size );
#endif
	if ( event.event_number != DAT_CONNECTION_EVENT_ESTABLISHED )
	    {
	    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::ConnectToRemote", "Error in connection establishing" )
		<< "Error in establishing connection. Connection event number from remote uDAPL endpoint: " 
		<< MLUCLog::kDec << event.event_number << ")." << ENDLOG;
	    // XXX Cleanup properly
	    dat_ep_free( cd.fEP );
	    dat_evd_free( cd.fConnEVD );
	    dat_evd_free( cd.fDataEVD );
	    return ENXIO;
	    }

	if ( event.event_data.connect_event_data.ep_handle != cd.fEP )
	    {
	    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::ConnectToRemote", "Endpoint mismatch in connection event" )
		<< "Endpoint mismatch in connection event from remote uDAPL endpoint: " 
		<< MLUCLog::kDec << (unsigned long)event.event_data.connect_event_data.ep_handle << " received, "
		<< (unsigned long)cd.fEP << " expected." << ENDLOG;
	    // XXX Cleanup properly
	    dat_ep_free( cd.fEP );
	    dat_evd_free( cd.fConnEVD );
	    dat_evd_free( cd.fDataEVD );
	    return ENXIO;
	    }
    
#ifdef STRICT_CONNECTION_DATA_SIZE_CHECK
	if ( event.event_data.connect_event_data.private_data_size != 2*sizeof(DAT_RMR_TRIPLET) )
	    {
	    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::ConnectToRemote", "Connection data mismatch in connection event" )
		<< "Connection data size mismatch in connection event from remote uDAPL endpoint: " 
		<< MLUCLog::kDec << event.event_data.connect_event_data.private_data_size << " received, "
		<< 2*sizeof(DAT_RMR_TRIPLET) << " expected." << ENDLOG;
	    // XXX Cleanup properly
	    dat_ep_free( cd.fEP );
	    dat_evd_free( cd.fConnEVD );
	    dat_evd_free( cd.fDataEVD );
	    return ENXIO;
	    }
#else
	if ( event.event_data.connect_event_data.private_data_size < (DAT_COUNT)(2*sizeof(DAT_RMR_TRIPLET)) )
	    {
	    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::ConnectToRemote", "Connection data mismatch in connection event" )
		<< "Connection data size mismatch in connection event from remote uDAPL endpoint: " 
		<< MLUCLog::kDec << event.event_data.connect_event_data.private_data_size << " received, at least "
		<< 2*sizeof(DAT_RMR_TRIPLET) << " expected." << ENDLOG;
	    // XXX Cleanup properly
	    dat_ep_free( cd.fEP );
	    dat_evd_free( cd.fConnEVD );
	    dat_evd_free( cd.fDataEVD );
	    return ENXIO;
	    }

	bool foundNonZero=false;
	DAT_COUNT n=0;
	for ( n=(DAT_COUNT)2*sizeof(DAT_RMR_TRIPLET); n < event.event_data.connect_event_data.private_data_size; n++ )
	    {
	    if ( ((uint8*)event.event_data.connect_event_data.private_data)[n] != 0 )
	        {
		foundNonZero = true;
		break;
	        }
	    }

	if ( foundNonZero )
  	    {
	    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::ConnectToRemote", "Data mismatch in connection event" )
		<< "Connection data mismatch in connection event from remote uDAPL endpoint: Found unexpected data which is non-zero ("
		<< MLUCLog::kDec << 2*sizeof(DAT_RMR_TRIPLET) << " bytes expected) = index: " 
		<< n
		<< ENDLOG;
	    DumpData2Log( MLUCLog::kError, "BCLIntUDAPLComHelper::ConnectToRemote", "Connection Data received",
			  event.event_data.connect_event_data.private_data,
			  event.event_data.connect_event_data.private_data_size );
	    // XXX Cleanup properly
	    dat_ep_free( cd.fEP );
	    dat_evd_free( cd.fConnEVD );
	    dat_evd_free( cd.fDataEVD );
	    return ENXIO;
	    }

#endif
#if 0
	cd.fMsgRMRTriplet = ((DAT_RMR_TRIPLET*)event.event_data.connect_event_data.private_data)[1];
	cd.fBlobRMRTriplet = ((DAT_RMR_TRIPLET*)event.event_data.connect_event_data.private_data)[0];
#endif
	BCLNetworkData::Transform( ((DAT_RMR_TRIPLET*)event.event_data.connect_event_data.private_data)[1].rmr_context, cd.fMsgRMRTriplet.rmr_context, kNetworkByteOrder, kNativeByteOrder );
	BCLNetworkData::Transform( ((DAT_RMR_TRIPLET*)event.event_data.connect_event_data.private_data)[1].pad, cd.fMsgRMRTriplet.pad, kNetworkByteOrder, kNativeByteOrder );
	BCLNetworkData::Transform( ((DAT_RMR_TRIPLET*)event.event_data.connect_event_data.private_data)[1].target_address, cd.fMsgRMRTriplet.target_address, kNetworkByteOrder, kNativeByteOrder );
	BCLNetworkData::Transform( ((DAT_RMR_TRIPLET*)event.event_data.connect_event_data.private_data)[1].segment_length, cd.fMsgRMRTriplet.segment_length, kNetworkByteOrder, kNativeByteOrder );
	BCLNetworkData::Transform( ((DAT_RMR_TRIPLET*)event.event_data.connect_event_data.private_data)[0].rmr_context, cd.fBlobRMRTriplet.rmr_context, kNetworkByteOrder, kNativeByteOrder );
	BCLNetworkData::Transform( ((DAT_RMR_TRIPLET*)event.event_data.connect_event_data.private_data)[0].pad, cd.fBlobRMRTriplet.pad, kNetworkByteOrder, kNativeByteOrder );
	BCLNetworkData::Transform( ((DAT_RMR_TRIPLET*)event.event_data.connect_event_data.private_data)[0].target_address, cd.fBlobRMRTriplet.target_address, kNetworkByteOrder, kNativeByteOrder );
	BCLNetworkData::Transform( ((DAT_RMR_TRIPLET*)event.event_data.connect_event_data.private_data)[0].segment_length, cd.fBlobRMRTriplet.segment_length, kNetworkByteOrder, kNativeByteOrder );

	// Set fCNO for cd.fConnEVD
	ret=dat_evd_modify_cno( cd.fConnEVD, fCNO );
	if ( ret != DAT_SUCCESS )
	    {
	    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::ConnectToRemote", "Error setting CNO for connection EVD" )
		<< "Error setting uDAPL consumer notification object for connection event dispatcher: " 
		<< DatStrError(ret).c_str() << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	    // XXX Cleanup properly
	    dat_ep_disconnect( cd.fEP, DAT_CLOSE_ABRUPT_FLAG );
	    dat_ep_free( cd.fEP );
	    dat_evd_free( cd.fConnEVD );
	    dat_evd_free( cd.fDataEVD );
	    return ENXIO;
	    }
	}
    else
	{
	cd.fEP = DAT_HANDLE_NULL;
	cd.fDataEVD = DAT_HANDLE_NULL;
	cd.fConnEVD = DAT_HANDLE_NULL;
	cd.fMsgRMRTriplet.rmr_context = 0;
	cd.fMsgRMRTriplet.pad = 0;
	cd.fMsgRMRTriplet.target_address = 0;
	cd.fMsgRMRTriplet.segment_length = 0;
	cd.fBlobRMRTriplet.rmr_context = 0;
	cd.fBlobRMRTriplet.pad = 0;
	cd.fBlobRMRTriplet.target_address = 0;
	cd.fBlobRMRTriplet.segment_length = 0;
	}
    cd.fUsageCount = 1;
    
    iret = pthread_mutex_lock( &fConnectionMutex );
    fConnections.Add( cd, ndx );
    conn = fConnections.GetPtr( ndx );
    memcpy( connectData, conn, sizeof(*connectData) );
    iret = pthread_mutex_unlock( &fConnectionMutex );
	
    return 0;
    }

int BCLIntUDAPLComHelper::DisconnectFromRemote( ConnectionData* connectData, bool useTimeout, uint32 timeout_ms, 
						bool error, bool asyncError )
    {
    bool removed;
    return DisconnectFromRemote( true, removed, connectData, useTimeout, timeout_ms, error, asyncError );
    }

int BCLIntUDAPLComHelper::DisconnectFromRemote( bool lock, bool& removed, ConnectionData* connectData, bool /*useTimeout*/, uint32 /*timeout_ms*/, 
						bool error, bool asyncError )
    {
    int iret;
    bool found=false;
    unsigned long ndx=0;
    if ( lock )
	iret = pthread_mutex_lock( &fConnectionMutex );
    PConnectionData conn;
    ConnectionData cd;
    if ( MLUCVectorSearcher<ConnectionData,PConnectionData>::FindElement( fConnections, &ConnectionSearchFunc, connectData, ndx ) )
	{
	found=true;
	conn = fConnections.GetPtr( ndx );
	if ( !asyncError )
	    conn->fUsageCount--;
	if ( conn->fUsageCount>0 && !error )
	    {
	    if ( lock )
		iret = pthread_mutex_unlock( &fConnectionMutex );
	    return 0;
	    }
	memcpy( &cd, conn, sizeof(*connectData) );
	if ( conn->fUsageCount<=0 )
	    {
	    removed = true;
	    fConnections.Remove( ndx );
	    }
	else
	    {
	    conn->fEP = DAT_HANDLE_NULL;
	    conn->fDataEVD = DAT_HANDLE_NULL;
	    conn->fConnEVD = DAT_HANDLE_NULL;
	    conn->fMsgRMRTriplet.rmr_context = 0;
	    conn->fMsgRMRTriplet.pad = 0;
	    conn->fMsgRMRTriplet.target_address = 0;
	    conn->fMsgRMRTriplet.segment_length = 0;
	    conn->fBlobRMRTriplet.rmr_context = 0;
	    conn->fBlobRMRTriplet.pad = 0;
	    conn->fBlobRMRTriplet.target_address = 0;
	    conn->fBlobRMRTriplet.segment_length = 0;
	    }
	}
    if ( lock )
	iret = pthread_mutex_unlock( &fConnectionMutex );
    if ( !found )
	return EINVAL;
    
    DAT_RETURN ret;

    if ( cd.fConnEVD != DAT_HANDLE_NULL )
	ret=dat_evd_modify_cno( cd.fConnEVD, DAT_HANDLE_NULL );
    
    if ( cd.fDataEVD != DAT_HANDLE_NULL )
	ret=dat_evd_modify_cno( cd.fDataEVD, DAT_HANDLE_NULL );

    if ( cd.fEP != DAT_HANDLE_NULL )
	{
	dat_ep_disconnect( cd.fEP, DAT_CLOSE_ABRUPT_FLAG );
	dat_ep_free( cd.fEP );
	}
    
    if ( cd.fConnEVD != DAT_HANDLE_NULL )
	dat_evd_free( cd.fConnEVD );

    if ( cd.fDataEVD != DAT_HANDLE_NULL )
	dat_evd_free( cd.fDataEVD );

    return 0;
    }

BCLIntUDAPLComHelper::operator bool()
    {
    return fIA!=DAT_HANDLE_NULL;
    }

bool BCLIntUDAPLComHelper::ConnectionSearchFunc( const ConnectionData& el, const PConnectionData& searchData )
    {
#if 0
    return (el.fAddress.fSinFamily == searchData->fAddress.fSinFamily) && (el.fAddress.fSinPort == searchData->fAddress.fSinPort) && 
				    (el.fAddress.fNodeID == searchData->fAddress.fNodeID) && (el.fAddress.fConnQual == searchData->fAddress.fConnQual) &&
				    !strncmp( el.fAddress.fIAName, searchData->fAddress.fIAName, 32 );
#else
    return el.fAddress == searchData->fAddress;
#endif
    }

void BCLIntUDAPLComHelper::EventLoop()
    {
    fEventLoopQuitted = false;
    do
	{
	DAT_EVD_HANDLE evd = DAT_HANDLE_NULL;
	DAT_RETURN ret;
	// timeout presumed to be in microseconds, doc doesn't specify
	ret = dat_cno_wait( fCNO, 250000, &evd );

	if ( fQuitEventLoop )
	    break;

	if ( DAT_GET_TYPE(ret)!=DAT_QUEUE_EMPTY && DAT_GET_TYPE(ret) != DAT_TIMEOUT_EXPIRED && evd!=DAT_HANDLE_NULL )
	    {
	    DAT_EVENT event;
	    if ( fConnReqEVD!=DAT_HANDLE_NULL )
		{
		do
		    {
		    ret = dat_evd_dequeue( fConnReqEVD, &event );
		    if ( DAT_GET_TYPE(ret) == DAT_QUEUE_EMPTY || DAT_GET_TYPE(ret) == DAT_TIMEOUT_EXPIRED )
			break;
		    if ( ret != DAT_SUCCESS )
			{
			LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::EventLoop", "Error retrieving event" )
			    << "Error retrieving event from connection request event dispatcher: " << DatStrError(ret).c_str() << " ("
			    << MLUCLog::kDec << ret << ")." << ENDLOG;
			break;
			}
		    switch ( event.event_number )
			{
			case DAT_CONNECTION_REQUEST_EVENT:
			    {
			    if ( (event.event_data.cr_arrival_event_data.conn_qual != fAddress.fConnQual) ||
				 (event.event_data.cr_arrival_event_data.sp_handle.psp_handle != fPSP) )
				{
				LOG( MLUCLog::kWarning, "BCLIntUDAPLComHelper::EventLoop", "Wrong connection request" )
				    << "Wrong connection request event received: " << MLUCLog::kDec
				    << (unsigned long)event.event_data.cr_arrival_event_data.conn_qual << "/"
				    << (unsigned long)event.event_data.cr_arrival_event_data.sp_handle.psp_handle
				    << " found instead of " << (unsigned long)fAddress.fConnQual << "/"
				    << (unsigned long)fPSP << "." << ENDLOG;
				break;
				}
			    AcceptedConnectionData acd;
			    acd.fRecvEVD = DAT_HANDLE_NULL;
			    acd.fReqEVD = DAT_HANDLE_NULL;
			    acd.fConnEVD = DAT_HANDLE_NULL;
			    acd.fEP = DAT_HANDLE_NULL;
			    acd.fIOV = NULL;
			    acd.fReceiveThread = NULL;
			    acd.fLastCookie = 0;
			    acd.fRecvBufferAlloc = (uint8*)malloc( gkPrivateMsgBufferSize );
			    if ( !acd.fRecvBufferAlloc )
				{
				LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::EventLoop", "Out of memory" )
				    << "Out of memory trying to allocate recv buffer of " << MLUCLog::kDec 
				    << gkPrivateMsgBufferSize << " bytes for incoming connection." 
				    << ENDLOG;
				// XXX Cleanup properly
				break;
				}
                            acd.fIOV = new DAT_LMR_TRIPLET;
			    if ( !acd.fIOV )
			        {
				LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::EventLoop", "Out of memory" )
				    << "Out of memory trying to allocate lmr triplet  of " << MLUCLog::kDec 
				    << sizeof(DAT_LMR_TRIPLET) << " bytes for incoming connection." 
				    << ENDLOG;
				// XXX Cleanup properly
				free( acd.fRecvBufferAlloc );
				break;
			        }
			    DAT_REGION_DESCRIPTION region;
			    region.for_va = acd.fRecvBufferAlloc;
			    DAT_MEM_PRIV_FLAGS lmr_flags;
			    lmr_flags = (DAT_MEM_PRIV_FLAGS)( DAT_MEM_PRIV_ALL_FLAG|DAT_MEM_PRIV_REMOTE_WRITE_FLAG );
			    ret = dat_lmr_create( fIA,
						  DAT_MEM_TYPE_VIRTUAL,
						  region,
						  gkPrivateMsgBufferSize,
						  fPZ,
						  lmr_flags,
						  &acd.fRecvLMRHandle,
						  &acd.fRecvLMRContext,
						  &acd.fRecvRMRContext,
						  &acd.fRecvBufferSize,
						  &acd.fRecvBuffer );
			    if ( ret != DAT_SUCCESS )
				{
				LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::EventLoop", "Error creating receive buffer lmr" )
				    << "Error creating local receive buffer memory region for incoming connection: " << DatStrError(ret).c_str() << " ("
				    << MLUCLog::kDec << ret << ")." << ENDLOG;
				// XXX Cleanup properly
				delete acd.fIOV;
				free( acd.fRecvBufferAlloc );
				break;
				}
			    ret = dat_evd_create( fIA, gkEventQueueSize, DAT_HANDLE_NULL, DAT_EVD_DTO_FLAG, &acd.fReqEVD  );
			    if ( ret != DAT_SUCCESS )
				{
				LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::EventLoop", "Error creating request evd" )
				    << "Error creating uDAPL request event dispatcher for incoming connection: " << DatStrError(ret).c_str() << " ("
				    << MLUCLog::kDec << ")." << ENDLOG;
				// XXX Cleanup properly
				dat_lmr_free( acd.fRecvLMRHandle );
				delete acd.fIOV;
				free( acd.fRecvBufferAlloc );
				break;
				}
			    else
				{
				LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::EventLoop", "Connection request evd created" )
				    << "Created uDAPL request event dispatcher for incoming connection." << ENDLOG;
				}
			    ret = dat_evd_create( fIA, gkEventQueueSize, fCNO, DAT_EVD_DTO_FLAG, &acd.fRecvEVD  );
			    if ( ret != DAT_SUCCESS )
				{
				LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::EventLoop", "Error creating receive evd" )
				    << "Error creating uDAPL receive event dispatcher for incoming connection: " << DatStrError(ret).c_str() << " ("
				    << MLUCLog::kDec << ")." << ENDLOG;
				// XXX Cleanup properly
				dat_evd_free( acd.fReqEVD );
				dat_lmr_free( acd.fRecvLMRHandle );
				delete acd.fIOV;
				free( acd.fRecvBufferAlloc );
				break;
				}
			    else
				{
				LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::EventLoop", "Connection receive evd created" )
				    << "Created uDAPL receive event dispatcher for incoming connection." << ENDLOG;
				}
			    ret = dat_evd_create( fIA, gkEventQueueSize, fCNO, DAT_EVD_CONNECTION_FLAG, &acd.fConnEVD  );
			    if ( ret != DAT_SUCCESS )
				{
				LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::EventLoop", "Error creating connection evd" )
				    << "Error creating uDAPL connection event dispatcher for incoming connection: " << DatStrError(ret).c_str() << " ("
				    << MLUCLog::kDec << ")." << ENDLOG;
				// XXX Cleanup properly
				dat_evd_free( acd.fRecvEVD );
				dat_evd_free( acd.fReqEVD );
				dat_lmr_free( acd.fRecvLMRHandle );
				delete acd.fIOV;
				free( acd.fRecvBufferAlloc );
				break;
				}
			    else
				{
				LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::EventLoop", "Connection connection evd created" )
				    << "Created uDAPL connection event dispatcher for incoming connection." << ENDLOG;
				}
			    ret = dat_ep_create( fIA, fPZ, acd.fRecvEVD, acd.fReqEVD, acd.fConnEVD, 
						 NULL, &acd.fEP );
			    if ( ret != DAT_SUCCESS )
				{
				LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::EventLoop", "Error creating request ep" )
				    << "Error creating uDAPL request endpoint for incoming connection: " << DatStrError(ret).c_str() << " ("
				    << MLUCLog::kDec << ")." << ENDLOG;
				// XXX Cleanup properly
				dat_evd_free( acd.fConnEVD );
				dat_evd_free( acd.fRecvEVD );
				dat_evd_free( acd.fReqEVD );
				dat_lmr_free( acd.fRecvLMRHandle );
				delete acd.fIOV;
				free( acd.fRecvBufferAlloc );
				break;
				}
			    else
				{
				LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::EventLoop", "Connection request ep created" )
				    << "Created uDAPL request endpoint for incoming connection." << ENDLOG;
				}

#ifdef UDAPL_RECEIVE_NOTIFICATION_WORKAROUND
			    acd.fReceiveThread = new ReceiveNotificationHelperThread( acd.fRecvEVD, acd.fEP, acd.fRecvBuffer,
										      acd.fRecvBufferSize, acd.fRecvLMRContext,
										      acd.fIOV, 
										      fDataReceivedCallback,
										      fReceiveThreadStartedSignal );
			    if ( !acd.fReceiveThread )
				{
				LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::EventLoop", "Error creating receive thread" )
				    << "Error creating receive thread for incoming connection: " << DatStrError(ret).c_str() << " ("
				    << MLUCLog::kDec << ")." << ENDLOG;
				// XXX Cleanup properly
				dat_ep_free( acd.fEP );
				dat_evd_free( acd.fConnEVD );
				dat_evd_free( acd.fRecvEVD );
				dat_evd_free( acd.fReqEVD );
				dat_lmr_free( acd.fRecvLMRHandle );
				delete acd.fIOV;
				free( acd.fRecvBufferAlloc );
				break;
				}
			    else
				{
				LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::EventLoop", "Connection receive thread created" )
				    << "Created receive event thread for incoming connection." << ENDLOG;
				}
#endif

			    DAT_CR_HANDLE cr;
			    cr = event.event_data.cr_arrival_event_data.cr_handle;
			    LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::EventLoop", "Accepting new connection" )
				<< "Accepting incoming connection." << ENDLOG;
			    DAT_RMR_TRIPLET rmrTriplet[2];
#if 0
			    rmrTriplet[0].rmr_context = fRecvRMRContext;
			    rmrTriplet[0].pad = 0;
			    rmrTriplet[0].target_address = fRecvBuffer;
			    rmrTriplet[0].segment_length = fRecvBufferSize;
			    rmrTriplet[1].rmr_context = acd.fRecvRMRContext;
			    rmrTriplet[1].pad = 0;
			    rmrTriplet[1].target_address = acd.fRecvBuffer;
			    rmrTriplet[1].segment_length = acd.fRecvBufferSize;
#endif
			    BCLNetworkData::Transform( acd.fRecvRMRContext, rmrTriplet[1].rmr_context, kNativeByteOrder, kNetworkByteOrder );
			    BCLNetworkData::Transform( (uint32)0, rmrTriplet[1].pad, kNativeByteOrder, kNetworkByteOrder );
			    BCLNetworkData::Transform( acd.fRecvBuffer, rmrTriplet[1].target_address, kNativeByteOrder, kNetworkByteOrder );
			    BCLNetworkData::Transform( acd.fRecvBufferSize, rmrTriplet[1].segment_length, kNativeByteOrder, kNetworkByteOrder );
			    BCLNetworkData::Transform( fRecvRMRContext, rmrTriplet[0].rmr_context, kNativeByteOrder, kNetworkByteOrder );
			    BCLNetworkData::Transform( (uint32)0, rmrTriplet[0].pad, kNetworkByteOrder, kNativeByteOrder );
			    BCLNetworkData::Transform( fRecvBuffer, rmrTriplet[0].target_address, kNetworkByteOrder, kNativeByteOrder );
			    BCLNetworkData::Transform( fRecvBufferSize, rmrTriplet[0].segment_length, kNativeByteOrder, kNetworkByteOrder );
			    
#if 0
			    DumpData2Log( MLUCLog::kDebug, "BCLIntUDAPLComHelper::EventLoop", "Sending Connection Data",
					  (void*)rmrTriplet, sizeof(rmrTriplet) );
#endif

			    acd.fIOV->lmr_context = acd.fRecvLMRContext;
			    acd.fIOV->pad = 0;
			    acd.fIOV->virtual_address = acd.fRecvBuffer;
			    acd.fIOV->segment_length = acd.fRecvBufferSize;
			    DAT_DTO_COOKIE cookie;
			    cookie.as_64 = acd.fLastCookie;

			    ret = dat_ep_post_recv( acd.fEP, 
						    1, acd.fIOV,
						    cookie, DAT_COMPLETION_DEFAULT_FLAG );
			    
			    if ( ret != DAT_SUCCESS )
				{
				LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::EventLoop", "Error posting connection receive" )
				    << "Error trying to post message receive for incoming connection: " << DatStrError(ret).c_str() << " ("
				    << MLUCLog::kDec << ")." << ENDLOG;
				// XXX Cleanup properly
#ifdef UDAPL_RECEIVE_NOTIFICATION_WORKAROUND
				delete acd.fReceiveThread;
#endif
				dat_ep_free( acd.fEP );
				dat_evd_free( acd.fConnEVD );
				dat_evd_free( acd.fRecvEVD );
				dat_evd_free( acd.fReqEVD );
				dat_lmr_free( acd.fRecvLMRHandle );
				delete acd.fIOV;
				free( acd.fRecvBufferAlloc );
				break;
				}

			    ret = dat_cr_accept( cr, acd.fEP, sizeof(rmrTriplet), (DAT_PVOID)&rmrTriplet );
			    if ( ret != DAT_SUCCESS )
				{
				LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::EventLoop", "Error accepting connection" )
				    << "Error trying to accept incoming connection: " << DatStrError(ret).c_str() << " ("
				    << MLUCLog::kDec << ")." << ENDLOG;
				// XXX Cleanup properly
#ifdef UDAPL_RECEIVE_NOTIFICATION_WORKAROUND
				delete acd.fReceiveThread;
#endif
				dat_ep_free( acd.fEP );
				dat_evd_free( acd.fConnEVD );
				dat_evd_free( acd.fRecvEVD );
				dat_evd_free( acd.fReqEVD );
				dat_lmr_free( acd.fRecvLMRHandle );
				delete acd.fIOV;
				free( acd.fRecvBufferAlloc );
				break;
				}
			    else
				{
				LOG( MLUCLog::kDebug, "BCLIntUDAPLComHelper::EventLoop", "Accepted connection" )
				    << "Accepted incoming connection." << ENDLOG;
				}

#ifdef UDAPL_RECEIVE_NOTIFICATION_WORKAROUND
			    fReceiveThreadStartedSignal.Lock();
			    if ( acd.fReceiveThread->Start()==MLUCThread::kStarted )
				fReceiveThreadStartedSignal.Wait();
			    else
				{
#ifdef UDAPL_RECEIVE_NOTIFICATION_WORKAROUND
				delete acd.fReceiveThread;
#endif
				dat_ep_free( acd.fEP );
				dat_evd_free( acd.fConnEVD );
				dat_evd_free( acd.fRecvEVD );
				dat_evd_free( acd.fReqEVD );
				dat_lmr_free( acd.fRecvLMRHandle );
				delete acd.fIOV;
				free( acd.fRecvBufferAlloc );
				}
			    fReceiveThreadStartedSignal.Unlock();
#endif

			    pthread_mutex_lock( &fAcceptedConnectionMutex );
			    fAcceptedConnections.Add( acd );
			    pthread_mutex_unlock( &fAcceptedConnectionMutex );
			    }
			    break;
			default:
			    LOG( MLUCLog::kWarning, "BCLIntUDAPLComHelper::EventLoop", "Unknown message for conn req evd" )
				<< "Unknown message 0x" << MLUCLog::kHex << event.event_number << " ("
				<< MLUCLog::kDec << event.event_number << ") received by connection request event dispatcher."
				<< ENDLOG;
			    break;
			}
		    }
		while ( ret == DAT_SUCCESS );
		}
	    MLUCVector<AcceptedConnectionData>::TIterator iter, end;
	    pthread_mutex_lock( &fAcceptedConnectionMutex );
	    iter = fAcceptedConnections.Begin();
	    while ( iter )
		{
#ifndef UDAPL_RECEIVE_NOTIFICATION_WORKAROUND
		if ( iter->fRecvEVD != DAT_HANDLE_NULL )
		    {
		    do
			{
			ret = dat_evd_dequeue( iter->fRecvEVD, &event );
			if ( DAT_GET_TYPE(ret) == DAT_QUEUE_EMPTY || DAT_GET_TYPE(ret) == DAT_TIMEOUT_EXPIRED )
			    break;
			if ( ret != DAT_SUCCESS )
			    {
			    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::EventLoop", "Error retrieving event" )
				<< "Error retrieving event from connection receive event dispatcher: " << DatStrError(ret).c_str() << " ("
				<< MLUCLog::kDec << ret << ")." << ENDLOG;
			    break;
			    }
			switch ( event.event_number )
			    {
			    case DAT_DTO_COMPLETION_EVENT:
				if ( event.event_data.dto_completion_event_data.status  == DAT_DTO_SUCCESS )
				    {
				    int iret;
				    iret = fDataReceivedCallback->DataReceived( iter->fEVD, iter->fEP,
										iter->fRecvBuffer, iter->fRecvBufferSize,
										iter->fRecvLMRContext, iter->fIOV, 
										iter->fLastCookie, event );
				    if ( !iret || iret == EAGAIN )
					iter->fLastCookie++;
				    }
				else
				    {
				    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::EventLoop", "Error on DTO" )
					<< "DTO Error for connection endpoint " << MLUCLog::kDec
					<< (unsigned long)iter->fEP << ": " 
					<< " (" << (unsigned long)event.event_data.dto_completion_event_data.status << ")." << ENDLOG;
				    }
				break;
			    default:
			        LOG( MLUCLog::kWarning, "BCLIntUDAPLComHelper::EventLoop", "Unknown message for accepted connection evd" )
				    << "Unknown message 0x" << MLUCLog::kHex << (unsigned long)event.event_number << " ("
				    << MLUCLog::kDec << (unsigned long)event.event_number << ") received by accepted connection event dispatcher."
				    << ENDLOG;
				break;
			    }
			}
		    while ( ret == DAT_SUCCESS );
		    }
#endif
		if ( iter->fReqEVD != DAT_HANDLE_NULL )
		    {
		    do
			{
			ret = dat_evd_dequeue( iter->fReqEVD, &event );
			if ( DAT_GET_TYPE(ret) == DAT_QUEUE_EMPTY || DAT_GET_TYPE(ret) == DAT_TIMEOUT_EXPIRED )
			    break;
			if ( ret != DAT_SUCCESS )
			    {
			    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::EventLoop", "Error retrieving event" )
				<< "Error retrieving event from connection request event dispatcher: " << DatStrError(ret).c_str() << " ("
				<< MLUCLog::kDec << ret << ")." << ENDLOG;
			    break;
			    }
			switch ( event.event_number )
			    {
			    default:
			        LOG( MLUCLog::kWarning, "BCLIntUDAPLComHelper::EventLoop", "Unknown message for accepted connection evd" )
				    << "Unknown message 0x" << MLUCLog::kHex << (unsigned long)event.event_number << " ("
				    << MLUCLog::kDec << (unsigned long)event.event_number << ") received by accepted connection event dispatcher."
				    << ENDLOG;
				break;
			    }
			}
		    while ( ret == DAT_SUCCESS );
		    }
		++iter;
		}
	    bool removedConn = false;
	    do
		{
		removedConn = false;
		iter = fAcceptedConnections.Begin();
		while ( iter )
		    {
		    if ( iter->fConnEVD != DAT_HANDLE_NULL )
			{
			do
			    {
			    ret = dat_evd_dequeue( iter->fConnEVD, &event );
			    if ( DAT_GET_TYPE(ret) == DAT_QUEUE_EMPTY || DAT_GET_TYPE(ret) == DAT_TIMEOUT_EXPIRED )
				break;
			    if ( ret != DAT_SUCCESS )
				{
				LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::EventLoop", "Error retrieving event" )
				    << "Error retrieving event from connection request event dispatcher: " << DatStrError(ret).c_str() << " ("
				    << MLUCLog::kDec << ret << ")." << ENDLOG;
				break;
				}
			    switch ( event.event_number )
				{
				case DAT_CONNECTION_EVENT_DISCONNECTED:
				    LOG( MLUCLog::kInformational, "BCLIntUDAPLComHelper::EventLoop", "Connection terminated" )
					<< "Connection with endpoint " << (unsigned long)iter->fEP << " is terminated." << ENDLOG;
				    fRemovedConnections.Add( *iter );
				    fAcceptedConnections.Remove( iter.GetNdx() );
				    removedConn = true;
				    break;
				default:
				    LOG( MLUCLog::kWarning, "BCLIntUDAPLComHelper::EventLoop", "Unknown message for accepted connection evd" )
					<< "Unknown message 0x" << MLUCLog::kHex << (unsigned long)event.event_number << " ("
					<< MLUCLog::kDec << (unsigned long)event.event_number << ") received by accepted connection event dispatcher."
					<< ENDLOG;
				    break;
				}
			    }
			while ( ret == DAT_SUCCESS && !removedConn );
			}
		    if ( removedConn )
			break;
		    ++iter;
		    }
		}
	    while ( removedConn );
	    pthread_mutex_unlock( &fAcceptedConnectionMutex );


	    pthread_mutex_lock( &fConnectionMutex );
	    MLUCVector<ConnectionData>::TIterator cdIter;
	    bool outConnRemoved=false;
	    do
		{
		outConnRemoved=false;
		cdIter = fConnections.Begin();
		while ( cdIter )
		    {
		    if ( cdIter->fConnEVD != DAT_HANDLE_NULL )
			{
			do
			    {
			    ret = dat_evd_dequeue( cdIter->fConnEVD, &event );
			    if ( DAT_GET_TYPE(ret) == DAT_QUEUE_EMPTY || DAT_GET_TYPE(ret) == DAT_TIMEOUT_EXPIRED )
				break;
			    if ( ret != DAT_SUCCESS )
				{
				LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::EventLoop", "Error retrieving event" )
				    << "Error retrieving event from outgoing connection event dispatcher: " << DatStrError(ret).c_str() << " ("
				    << MLUCLog::kDec << ret << ")." << ENDLOG;
				break;
				}
			    switch ( event.event_number )
				{
				case DAT_CONNECTION_EVENT_DISCONNECTED:
				    LOG( MLUCLog::kInformational, "BCLIntUDAPLComHelper::EventLoop", "Connection terminated" )
					<< "Outgoing connection with endpoint " << (unsigned long)cdIter->fEP << " is terminated." << ENDLOG;
				    DisconnectFromRemote( false, outConnRemoved, &(*cdIter), true, 0, true, true );
				    break;
				default:
				    LOG( MLUCLog::kWarning, "BCLIntUDAPLComHelper::EventLoop", "Unknown message for outgoing connection evd" )
					<< "Unknown message 0x" << MLUCLog::kHex << (unsigned long)event.event_number << " ("
					<< MLUCLog::kDec << (unsigned long)event.event_number << ") received by outgoing connection event dispatcher."
					<< ENDLOG;
				    break;
				}
			    }
			while ( ret == DAT_SUCCESS && !removedConn );
			}
		    if ( outConnRemoved )
			break;
		    ++cdIter;
		    }
		}
	    while ( outConnRemoved );
	    pthread_mutex_unlock( &fConnectionMutex );

	    }
	bool removedConn = fRemovedConnections.GetCnt() > 0;
	while ( removedConn )
	    {
	    removedConn = false;
	    MLUCVector<AcceptedConnectionData>::TIterator iter;
	    iter = fRemovedConnections.Begin();
	    while ( iter )
		{
		AcceptedConnectionData acd = *iter;

#ifdef UDAPL_RECEIVE_NOTIFICATION_WORKAROUND
		if ( !acd.fReceiveThread || acd.fReceiveThread->Stop() )
		    {
#endif		    
		    if ( acd.fEP!=DAT_HANDLE_NULL )
			ret = dat_ep_free( acd.fEP );
		    if ( acd.fConnEVD!=DAT_HANDLE_NULL )
			dat_evd_free( acd.fConnEVD  );
#ifdef UDAPL_RECEIVE_NOTIFICATION_WORKAROUND
		    if ( acd.fReceiveThread )
			delete acd.fReceiveThread;
#endif
		    if ( acd.fRecvEVD!=DAT_HANDLE_NULL )
			dat_evd_free( acd.fRecvEVD  );
		    if ( acd.fReqEVD!=DAT_HANDLE_NULL )
			dat_evd_free( acd.fReqEVD  );
		    if ( acd.fRecvLMRHandle!=DAT_HANDLE_NULL )
			dat_lmr_free( acd.fRecvLMRHandle );
		    if ( acd.fIOV )
			delete acd.fIOV;
		    if ( acd.fRecvBufferAlloc )
			free( acd.fRecvBufferAlloc );
		    
		    fRemovedConnections.Remove( iter.GetNdx() );
		    removedConn = true;
		    break;
#ifdef UDAPL_RECEIVE_NOTIFICATION_WORKAROUND
		    }
#endif
		++iter;
		}
	    }

	    
	}
    while ( !fQuitEventLoop );
    fEventLoopQuitted = true;
    }


MLUCString DatStrError (DAT_RETURN ret_value)
    {
    MLUCString tmp( "" );
    const char *maj;
    const char* min;
    if ( dat_strerror( ret_value, &maj, &min ) != DAT_SUCCESS )
	return tmp;
    tmp = maj;
    tmp += " (";
    tmp += min;
    tmp += ")";
    return tmp;
    }


#ifdef UDAPL_RECEIVE_NOTIFICATION_WORKAROUND

BCLIntUDAPLComHelper::ReceiveNotificationHelperThread::ReceiveNotificationHelperThread( DAT_EVD_HANDLE evd, 
											DAT_EP_HANDLE ep, 
											DAT_VADDR recvBuffer,
											DAT_VLEN recvBufferSize,
											DAT_LMR_CONTEXT recvLMRContext,
											DAT_LMR_TRIPLET* iov,
											DataReceiveCallbackBase* dataReceivedCallback,
											MLUCConditionSem<uint8>& startSignal ):
  fStartSignal( startSignal )
{
  fEVD = evd;
  fEP = ep;
  fRecvBuffer = recvBuffer;
  fRecvBufferSize = recvBufferSize;
  fRecvLMRContext = recvLMRContext;
  fIOV = iov;
  pthread_mutex_init( &fInUseLock, NULL );
  fDataReceivedCallback = dataReceivedCallback;
  fLastCookie = 0;

  fQuit = false;
}

BCLIntUDAPLComHelper::ReceiveNotificationHelperThread::~ReceiveNotificationHelperThread()
{
  pthread_mutex_destroy( &fInUseLock );
}

bool BCLIntUDAPLComHelper::ReceiveNotificationHelperThread::Stop()
    {
    fQuit = true;
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 25000;
    gettimeofday( &start, NULL );
    while ( deltaT<timeLimit )
	{
	usleep( 0 );
	int ret;
	ret = pthread_mutex_trylock( &fInUseLock );
	if ( ret != EBUSY )
	    {
	    pthread_mutex_unlock( &fInUseLock );
	    return true;
	    }
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    return false;
    }

void BCLIntUDAPLComHelper::ReceiveNotificationHelperThread::Run()
    {
    DAT_RETURN ret;
    DAT_EVENT event;
    DAT_COUNT qNr;
    pthread_mutex_lock( &fInUseLock );
    while ( !fQuit )
	{
	ret = dat_evd_wait( fEVD, 200000, 1, &event, &qNr );
	if ( fQuit )
	    break;
	if ( DAT_GET_TYPE(ret) == DAT_QUEUE_EMPTY || DAT_GET_TYPE(ret) == DAT_TIMEOUT_EXPIRED )
	    continue;
	if ( ret != DAT_SUCCESS )
	    {
	    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::ReceiveNotificationHelperThread::Run", "Error retrieving event" )
		<< "Error retrieving event from connection receive event dispatcher: " << DatStrError(ret).c_str() << " ("
		<< MLUCLog::kDec << ret << ")." << ENDLOG;
	    break;
	    }
	switch ( event.event_number )
	    {
	    case DAT_DTO_COMPLETION_EVENT:
		if ( event.event_data.dto_completion_event_data.status == DAT_DTO_SUCCESS )
		    {
		    int iret;
		    iret = fDataReceivedCallback->DataReceived( fEVD, fEP,
								fRecvBuffer, fRecvBufferSize,
								fRecvLMRContext, fIOV, 
								fLastCookie, event );
		    if ( !iret || iret == EAGAIN )
		        fLastCookie++;
		    }
		else if ( event.event_data.dto_completion_event_data.status == DAT_DTO_ERR_FLUSHED )
		  {
		    LOG( MLUCLog::kInformational, "BCLIntUDAPLComHelper::ReceiveNotificationHelperThread::Run", "Incoming connection closed" )
		      << "Incoming connection for endpoint " << MLUCLog::kDec
		      << (unsigned long)fEP << " closed." << ENDLOG;
		    fQuit = true;
		  }
		else
		    {
		    LOG( MLUCLog::kError, "BCLIntUDAPLComHelper::ReceiveNotificationHelperThread::Run", "Error on DTO" )
			<< "DTO Error for connection endpoint " << MLUCLog::kDec
			<< (unsigned long)fEP << ": " 
			<< " (" << (unsigned long)event.event_data.dto_completion_event_data.status << ")." << ENDLOG;
		    }
		break;
	    default:
		LOG( MLUCLog::kWarning, "BCLIntUDAPLComHelper::ReceiveNotificationHelperThread::Run", "Unknown message for accepted connection evd" )
		    << "Unknown message 0x" << MLUCLog::kHex << (unsigned long)event.event_number << " ("
		    << MLUCLog::kDec << (unsigned long)event.event_number << ") received by accepted connection event dispatcher."
		    << ENDLOG;
		break;
	    }
	}
    
    pthread_mutex_unlock( &fInUseLock );
    }
#endif


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
