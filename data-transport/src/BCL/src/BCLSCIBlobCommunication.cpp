/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLSCIBlobCommunication.hpp"
#include "BCLMsgCommunication.hpp"
#include "BCLSCIComID.hpp"
#include "BCLSCIAddressOps.hpp"
#include "BCLIntSCIComHelper.hpp"
#include "BCLIntSCIControlData.hpp"
#include <errno.h>
#include <asm/page.h>

#ifdef SCI_SIMPLE_TIMEOUT
#define SIMPLE_TIMEOUT_MAX 1000
#endif


//const int kBlobMsg = 0x8000;
const int kSCIBlobQueryAddressMsg = kBlobMsg+1;
const int kSCIBlobAddressMsg = kBlobMsg+2;
const int kSCIBlobQueryFreeBlockMsg = kBlobMsg+3;
const int kSCIBlobFreeBlockMsg = kBlobMsg+4;
const int kSCIBlobConnectRequestMsg = kBlobMsg+5;
const int kSCIBlobDisconnectMsg = kBlobMsg+6;
const int kSCIBlobQuitMsg = kBlobMsg+7;
const int kSCIBlobQueryBlobSizeMsg = kBlobMsg+8;
const int kSCIBlobBlobSizeReplyMsg = kBlobMsg+9;
const int kSCIBlobLockRequestMsg = kBlobMsg+10;
const int kSCIBlobUnlockMsg = kBlobMsg+11;

BCLSCIBlobCommunication::BCLSCIBlobCommunication():
    fComHelper( this )
#ifndef DEBUG_NOTHREADS
    , fRequestThread( this, &BCLSCIBlobCommunication::RequestHandler )
#endif
    {
    fControlData = NULL;
    fBufferAddress = NULL;
    fBufferSize = 0;
    BCLSCIAddress addr;
    fSCIAddress = *addr.GetData();
    fSCIError = SCI_ERR_OK;
    fQuitRequestHandler = false;
    fRequestHandlerQuitted = true;
    pthread_mutex_init( &fMsgMutex, NULL );
    pthread_mutex_init( &fBlockMutex, NULL );
    fCurrentQueryID = 0;
    fCurrentQueryCount = 0;
#ifndef SISCI_NO_DMA
    fDMA = true;
    fDMAMaxCopyBufferSize = 1048576;
    sci_query_adapter_t queryData;
    sci_error_t mySCIError;
    queryData.localAdapterNo = fSCIAddress.fAdapterNr;

    queryData.subcommand = SCI_Q_ADAPTER_DMA_SIZE_ALIGNMENT;
    queryData.data = &fDMASizeAlign;
    SCIQuery( SCI_Q_ADAPTER, &queryData, 0, &mySCIError );
    if ( mySCIError != SCI_ERR_OK )
	{
	// Use some defaults. (From experience and guessing)
	fDMASizeAlign = 64;
	}

    queryData.subcommand = SCI_Q_ADAPTER_DMA_OFFSET_ALIGNMENT;
    queryData.data = &fDMAOffsetAlign;
    SCIQuery( SCI_Q_ADAPTER, &queryData, 0, &mySCIError );
    if ( mySCIError != SCI_ERR_OK )
	{
	// Use some defaults. (From experience and guessing)
	fDMAOffsetAlign = 64;
	}
    if ( fDMAOffsetAlign > fDMASizeAlign )
	fDMAAlign = fDMAOffsetAlign;
    else
	fDMAAlign = fDMASizeAlign;
    queryData.subcommand = SCI_Q_ADAPTER_DMA_MTU;
    queryData.data = &fDMAMaxSize;
    SCIQuery( SCI_Q_ADAPTER, &queryData, 0, &mySCIError );
    if ( mySCIError != SCI_ERR_OK )
	{
	// Use some defaults. (From experience and guessing)
	fDMAMaxSize = 0xFFFFFFFF;
	}
    
#endif
    fDataArrivedSignal.Unlock();
    }

BCLSCIBlobCommunication::BCLSCIBlobCommunication( BCLMsgCommunication* msgCom ):
    BCLBlobCommunication( msgCom ), fComHelper( this )
#ifndef DEBUG_NOTHREADS
    , fRequestThread( this, &BCLSCIBlobCommunication::RequestHandler )
#endif
    {
    fControlData = NULL;
    fBufferAddress = NULL;
    fBufferSize = 0;
    BCLSCIAddress addr;
    fSCIAddress = *addr.GetData();
    fSCIError = SCI_ERR_OK;
    fQuitRequestHandler = false;
    fRequestHandlerQuitted = true;
    pthread_mutex_init( &fMsgMutex, NULL );
    pthread_mutex_init( &fBlockMutex, NULL );
    fCurrentQueryID = 0;
    fCurrentQueryCount = 0;
#ifndef SISCI_NO_DMA
    fDMA = true;
    fDMAMaxCopyBufferSize = 1048576;
    sci_query_adapter_t queryData;
    sci_error_t mySCIError;
    queryData.localAdapterNo = fSCIAddress.fAdapterNr;

    queryData.subcommand = SCI_Q_ADAPTER_DMA_SIZE_ALIGNMENT;
    queryData.data = &fDMASizeAlign;
    SCIQuery( SCI_Q_ADAPTER, &queryData, 0, &mySCIError );
    if ( mySCIError != SCI_ERR_OK )
	{
	// Use some defaults. (From experience and guessing)
	fDMASizeAlign = 64;
	}

    queryData.subcommand = SCI_Q_ADAPTER_DMA_OFFSET_ALIGNMENT;
    queryData.data = &fDMAOffsetAlign;
    SCIQuery( SCI_Q_ADAPTER, &queryData, 0, &mySCIError );
    if ( mySCIError != SCI_ERR_OK )
	{
	// Use some defaults. (From experience and guessing)
	fDMAOffsetAlign = 64;
	}
    if ( fDMAOffsetAlign > fDMASizeAlign )
	fDMAAlign = fDMAOffsetAlign;
    else
	fDMAAlign = fDMASizeAlign;
    queryData.subcommand = SCI_Q_ADAPTER_DMA_MTU;
    queryData.data = &fDMAMaxSize;
    SCIQuery( SCI_Q_ADAPTER, &queryData, 0, &mySCIError );
    if ( mySCIError != SCI_ERR_OK )
	{
	// Use some defaults. (From experience and guessing)
	fDMAMaxSize = 0xFFFFFFFF;
	}
    
#endif
    fDataArrivedSignal.Unlock();
    }

BCLSCIBlobCommunication::~BCLSCIBlobCommunication()
    {
    Unbind();
    }

bool BCLSCIBlobCommunication::SetBlobBuffer( uint32 size, uint8* blobBuffer )
    {
    if ( blobBuffer != NULL )
	{
	LOG( MLUCLog::kError, "BCLSCOBlobCommunication::SetBlobBuffer", "Blob buffer address setting" )
	    << "Setting of blob buffer memory address currently not supported for SCI based communication"
	    << ENDLOG;
	GeneralError( ENOTSUP, this, NULL );
	return false;
	}
    if ( fBufferAddress )
	{
	LOG( MLUCLog::kError, "BCLSCOBlobCommunication::SetBlobBuffer", "Blob buffer size setting" )
	    << "Blob buffer currently in use. Unbind first."
	    << ENDLOG;
	GeneralError( EBUSY, this, NULL );
	return false;
	}
    fBufferSize = size;
//     if ( fBufferSize % PAGE_SIZE )
// 	fBufferSize += PAGE_SIZE - (fBufferSize % PAGE_SIZE);
    fBufferAddress = NULL;
    LOG( MLUCLog::kInformational, "BCLSCIBlobCommunication::SetBlobBuffer", "Blob buffer setting" )
      << "Blob buffer set to 0x" << MLUCLog::kHex << reinterpret_cast<uint64>(fBufferAddress) << " size " << MLUCLog::kDec
      << fBufferSize << "." << ENDLOG;
    return true;
    }

int BCLSCIBlobCommunication::Bind( BCLAbstrAddressStruct* address )
    {
    int ret;
    if ( !address )
	{
	LOG( MLUCLog::kError, "SCI Blob Bind", "Address error" )
	    << "Wrong address (NULL pointer) in SCI Blob Bind" << ENDLOG;
	BindError( EFAULT, this, address, NULL );
	return EFAULT;
	}
    if ( address->fComID != kSCIComID )
	{
	LOG( MLUCLog::kError, "SCI Blob Bind", "Address error" )
	    << "Wrong address ID " << address->fComID << " in SCI Blob Bind. (expected " 
	    << kSCIComID << ")."<< ENDLOG;
	BindError( EINVAL, this, address, NULL );
	return EINVAL;
	}
    if ( !fMsgCom )
	{
	LOG( MLUCLog::kError, "SCI Blob Bind", "No msg communication" )
	    << "No message communication object configured in SCI Blob Bind" << ENDLOG;
	BindError( ENODEV, this, address, NULL );
	return ENODEV;
	}
    fSCIAddress = *(BCLSCIAddressStruct*)address;
    fAddress = &fSCIAddress;
    // We use the first page for storing our header/control structure information...
    ret = fComHelper.Bind( PAGE_SIZE+fBufferSize, (BCLSCIAddressStruct*)fAddress );
    if ( !ret )
	{
	uint8* tmpP = fComHelper.GetSegmentAddress();
	fBufferAddress = tmpP+PAGE_SIZE;
	fControlData = fComHelper.GetControlData();
	//fBufferAddress = fComHelper.GetDataBuffer();
	BCLBlobCommunication::SetBlobBuffer( fBufferSize, fBufferAddress );
	ret = pthread_mutex_lock( &fBlockMutex );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "BCLSCIBlobCommunication::Bind", "Block mutex" )
		<< "Error attempting to lock block mutex. Reason: " 
		<< strerror(ret) << " (" << MLUCLog::kDec << ret << "). Continuing..." << ENDLOG;
	    }
	TSCIBlockData block;
	block.fBlockOffset = 0;
	block.fBlockSize = fBufferSize;
	fFreeBlocks.insert( fFreeBlocks.begin(), block );
	ret = pthread_mutex_unlock( &fBlockMutex );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "BCLSCIBlobCommunication::Bind", "Block mutex" )
		<< "Error attempting to unlock block mutex. Reason: " 
		<< strerror(ret) << " (" << MLUCLog::kDec << ret << "). Continuing..." << ENDLOG;
	    }
#ifndef DEBUG_NOTHREADS
	fRequestThread.Start();
#endif
	}
    return ret;
    }

int BCLSCIBlobCommunication::Unbind()
    {
#ifndef DEBUG_NOTHREADS
    QuitRequestHandler();
#endif
    fBufferAddress = NULL;
    fControlData = NULL;
    return fComHelper.Unbind();
    }

int BCLSCIBlobCommunication::Connect( BCLAbstrAddressStruct* address, bool openOnDemand )
    {
    return Connect( address, NULL, openOnDemand, false, 0 );
    }

int BCLSCIBlobCommunication::Connect( BCLAbstrAddressStruct* address, unsigned long timeout_ms, bool openOnDemand )
    {
    return Connect( address, NULL, openOnDemand, true, timeout_ms );
    }

int BCLSCIBlobCommunication::Disconnect( BCLAbstrAddressStruct* address )
    {
    return Disconnect( address, NULL, false, 0 );
    }

int BCLSCIBlobCommunication::Disconnect( BCLAbstrAddressStruct* address, unsigned long timeout_ms )
    {
    return Disconnect( address, NULL, true, timeout_ms );
    }


bool BCLSCIBlobCommunication::CanLockConnection()
    {
    if ( fMsgCom && fMsgCom->CanLockConnection() )
	return true;
    return false;
    }

int BCLSCIBlobCommunication::LockConnection( BCLAbstrAddressStruct* msgAddress )
    {
    return LockConnection( msgAddress, true, false, 0 );
    }

int BCLSCIBlobCommunication::LockConnection( BCLAbstrAddressStruct* msgAddress, unsigned long timeout_ms )
    {
    return LockConnection( msgAddress, true, true, timeout_ms );
    }

int BCLSCIBlobCommunication::UnlockConnection( BCLAbstrAddressStruct* address )
    {
    return UnlockConnection( address, true, false, 0 );
    }

int BCLSCIBlobCommunication::UnlockConnection( BCLAbstrAddressStruct* address, unsigned long timeout_ms )
    {
    return UnlockConnection( address, true, true, timeout_ms );
    }


void BCLSCIBlobCommunication::SetMsgCommunication( BCLMsgCommunication* msgCom )
    {
    uint32 cc = fCurrentQueryCount;
    if ( cc>0 )
	{
	LOG( MLUCLog::kError, "BCLSCIBlobCommunication::SetMsgCommunication", "Msg communication busy" )
	    << "Current Message communication busy. " << cc << " queries outstanding." << ENDLOG;
	GeneralError( EBUSY, this, NULL );
	return;
	}
    fMsgCom = msgCom;
    }


uint32 BCLSCIBlobCommunication::GetRemoteBlobSize( BCLAbstrAddressStruct* msgAddress, unsigned long timeout_ms )
    {
    if ( !msgAddress )
	{
	LOG( MLUCLog::kError, "SCI Get Remote Blob Size", "Address error" )
	    << "Wrong address (NULL pointer) in SCI Get Remote Blob Size" << ENDLOG;
	BlobPrepareError( EFAULT, this, msgAddress );
	return (BCLTransferID)-1;
	}
    if ( !fMsgCom )
	{
	LOG( MLUCLog::kError, "SCI Get Remote Blob Size", "No msg communication" )
	    << "No message communication object configured in SCI Get Remote Blob Size" << ENDLOG;
	BlobPrepareError( ENODEV, this, msgAddress );
	return (BCLTransferID)-1;
	}
    int ret;
    BCLIntSCIBlobComMsgStruct* msg;
    BCLIntSCIBlobComMsg msgClass;
    msg = msgClass.GetData();
    msg->fMsgType = kSCIBlobQueryBlobSizeMsg;
    ret = GetQueryID( msg->fQueryID );
    if ( ret )
	{
	BlobPrepareError( ret, this, msgAddress );
	return (BCLTransferID)-1;
	}
    fDataArrivedSignal.Lock();
    if ( timeout_ms )
	ret = fMsgCom->Send( msgAddress, msg, timeout_ms );
    else
	ret = fMsgCom->Send( msgAddress, msg );
    if ( ret )
	{
	fDataArrivedSignal.Unlock();
	BlobPrepareError( ret, this, msgAddress );
	return (BCLTransferID)-1;
	}
    LOG( MLUCLog::kDebug, "BCLSCIBlobCommunication::GetRemoteBlobSize", "Query blob size" )
	<< "Query Blob Size message sent." << ENDLOG;
    ret = WaitForMessage( msg->fQueryID, msg, timeout_ms, timeout_ms );
    fDataArrivedSignal.Unlock();
    LOG( MLUCLog::kDebug, "BCLSCIBlobCommunication::GetRemoteBlobSize", "Blob Size" )
	<< "Received blob size " << msg->fBlockSize << "." << ENDLOG;
    if ( ret )
	return (uint32)0;
    return msg->fBlockSize;
    }

BCLTransferID BCLSCIBlobCommunication::PrepareBlobTransfer( BCLAbstrAddressStruct* msgAddress, uint64 size,
							    unsigned long timeout )
    {
    if ( !msgAddress )
	{
	LOG( MLUCLog::kError, "SCI Prepare Blob Transfer", "Address error" )
	    << "Wrong address (NULL pointer) in SCI Prepare Blob Transfer" << ENDLOG;
	BlobPrepareError( EFAULT, this, msgAddress );
	return (BCLTransferID)-1;
	}
    if ( !fMsgCom )
	{
	LOG( MLUCLog::kError, "SCI Prepare Blob Transfer", "No msg communication" )
	    << "No message communication object configured in SCI Prepare Blob Transfer" << ENDLOG;
	BlobPrepareError( ENODEV, this, msgAddress );
	return (BCLTransferID)-1;
	}
    int ret;
//     BCLSCIAddressStruct remoteBlobAddress;
//     ret = GetRemoteBlobAddress( msgAddress, &remoteBlobAddress );
//     if ( ret )
// 	{
// 	BlobPrepareError( ret, this, msgAddress );
// 	return (BCLTransferID)-1;
// 	}
    BCLIntSCIBlobComMsgStruct* msg;
    BCLIntSCIBlobComMsg msgClass;
    msg = msgClass.GetData();
    msg->fMsgType = kSCIBlobQueryFreeBlockMsg;
    msg->fBlockSize = size;
    ret = GetQueryID( msg->fQueryID );
    if ( ret )
	{
	BlobPrepareError( ret, this, msgAddress );
	return (BCLTransferID)-1;
	}
    fDataArrivedSignal.Lock();
    if ( timeout )
	ret = fMsgCom->Send( msgAddress, msg, timeout );
    else
	ret = fMsgCom->Send( msgAddress, msg );
    if ( ret )
	{
	fDataArrivedSignal.Unlock();
	BlobPrepareError( ret, this, msgAddress );
	return (BCLTransferID)-1;
	}
    ret = WaitForMessage( msg->fQueryID, msg, timeout, timeout );
    fDataArrivedSignal.Unlock();
    LOG( MLUCLog::kInformational, "BCLSCIBlobCommunication::PrepareBlobTransfer", "Blob transfer id" )
	<< "Received blob transfer id " << msg->fBlockOffset << "." << ENDLOG;
    if ( ret )
	return (BCLTransferID)-1;
    if ( msg->fBlockOffset == (uint32)-1 )
	return (BCLTransferID)-1;
    return (BCLTransferID)msg->fBlockOffset;
    }

int BCLSCIBlobCommunication::TransferBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, uint8* data,
					   uint64 offset, uint64 size, unsigned long timeout,
					   BCLErrorCallback *callback )
    {
    vector<uint8*> datas;
    vector<uint64> offsets;
    vector<uint64> sizes;
    datas.insert( datas.end(), data );
    offsets.insert( offsets.end(), offset );
    sizes.insert( sizes.end(), size );
    return TransferBlob( msgAddress, id, datas, offsets, sizes, timeout, callback );
    }

int BCLSCIBlobCommunication::TransferBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, vector<uint8*> datas,
					   vector<uint64> offsets, vector<uint64> sizes, unsigned long timeout,
					   BCLErrorCallback *callback )
    {
    if ( id == (BCLTransferID)-1 )
	{
	BlobSendError( EINVAL, this, msgAddress, id, callback );
	return EINVAL;
	}
    if ( !msgAddress )
	{
	AddressError( EFAULT, this, msgAddress, callback );
	return EFAULT;
	}
    if ( !sizes.size() )
	{
	return 0;
	}
    if ( sizes.size()!=datas.size() )
	{
	BlobSendError( EINVAL, this, msgAddress, id, callback );
	return EINVAL;
	}
    int ret;
    struct timeval start;
#ifndef NOREADBACKCHECKS
    struct timeval  now;
    unsigned long tdiff;
#endif
    BCLSCIAddressStruct remoteBlobAddress;
    ret = GetRemoteBlobAddress( msgAddress, &remoteBlobAddress, timeout, timeout );
    if ( ret )
	{
	BlobSendError( ret, this, msgAddress, id, callback );
	return ret;
	}
    BCLIntSCIComHelper::TSCIConnectionData condat;
    BCLIntSCIComHelper::TSCIConnectionData* connectData = &condat;
    condat.fAddress = remoteBlobAddress;
    bool found;
    ret = fComHelper.ConnectToRemote( &remoteBlobAddress, connectData, true, false, found, callback, timeout, timeout );
    if ( ret )
	{
	BlobSendError( ret, this, msgAddress, id, callback );
	return ret;
	}
#ifndef SISCI_NO_DMA
    if ( fDMA )
	{
	unsigned int localSegSize;
	localSegSize = connectData->fRemoteSize >> 2; // use a quarter of the remote size as a local DMA buffer...
	if ( localSegSize > fDMAMaxCopyBufferSize )
	    localSegSize = fDMAMaxCopyBufferSize; // ... but at most fDMAMaxCopyBufferSize
	ret = fComHelper.CreateLocalSegment( connectData, localSegSize );
	}
#endif
    BCLIntSCIControlData scd( connectData->fRemoteCD );
    BCLIntSCIControlDataStruct* controlData = scd.GetData();
    uint32 bufLen = controlData->fSize;
    uint32 offset = (uint32)id;
    if ( offset >= bufLen )
	{
	BlobSendError( ENOSPC, this, msgAddress, id, callback );
	return ENOSPC;
	}
    vector<uint8*>::iterator dataIter, dataEnd;
    vector<uint64>::iterator sizeIter, sizeEnd;
    vector<uint64>::iterator offsIter, offsEnd;
    uint64 size;//, totalSize = 0;
    uint64 blOffset, nextOffset = 0;
    uint8* data;
    dataIter = datas.begin();
    dataEnd = datas.end();
    sizeIter = sizes.begin();
    sizeEnd = sizes.end();
    offsIter = offsets.begin();
    offsEnd = offsets.end();

    gettimeofday( &start, NULL );

#ifndef SISCI_NO_DMA
    bool dmaOK = true;
    //vector<uint32> dmaPreSizes, dmaPostOffsets, dmaPostSizes;
    sci_error_t mySCIError;
    sci_dma_queue_t dmaQueue;
    sci_dma_queue_state_t dmaQueueState;
    if ( fDMA && !connectData->fLocalSegment )
	dmaOK = false;
    if ( fDMA && dmaOK )
	{
	SCICreateDMAQueue( connectData->fSCIDesc, &dmaQueue, connectData->fAddress.fAdapterNr,
			   datas.size(), 0, &mySCIError );
	if ( mySCIError != SCI_ERR_OK )
	    {
	    dmaOK = false;
	    LOG( MLUCLog::kError, "BCLSCIBlobCommunication::TransferBlob", "DMA Queue Error" )
		<< "Error creating SCI DMA queue: 0x" << MLUCLog::kHex << mySCIError << " ("
		<< MLUCLog::kDec << mySCIError << ") Not using DMA." << ENDLOG;
	    }
	}
#endif


    while ( dataIter != dataEnd && sizeIter != sizeEnd )
	{
	size = *sizeIter;
	data = *dataIter;
	if ( offsIter != offsEnd )
	    blOffset = *offsIter;
	else
	    blOffset = nextOffset;

	if ( !data )
	    {
	    BlobSendError( EFAULT, this, msgAddress, id, callback );
	    continue;
	    }
	if ( offset+blOffset+size > bufLen )
	    {
	    BlobSendError( ENOSPC, this, msgAddress, id, callback );
	    return ENOSPC;
	    }
	uint8* dest = connectData->fRemoteAddr+PAGE_SIZE+offset+blOffset;
#ifndef NO_SIMPLE_SEND_TEST
	uint8* endPD = dest+size-1;
	uint8* endPS = data+size-1;
	*endPD = ~(*endPS);
#endif

 	bool ok = true;
#ifndef SISCI_NO_DMA

	// XXX

	if ( fDMA && dmaOK )
	    {
	    // We have to split of unaligned slack at the beginning and end
	    unsigned int remBlockOffset = PAGE_SIZE+offset+blOffset;
	    unsigned int preSlack = 0;
	    unsigned int postSlack = 0;
	    unsigned int dmaSize = 0;
	    unsigned int dmaD = 0;
	    unsigned int thisDMASize;
	    if ( remBlockOffset%fDMAOffsetAlign )
		preSlack = fDMAOffsetAlign - (remBlockOffset%fDMAOffsetAlign);
	    postSlack = (size-preSlack)%fDMASizeAlign;
	    dmaSize = size-preSlack-postSlack;
	    //printf( "fDMAOffsetAlign: %u - fDMASizeAlign: %u\n", fDMAOffsetAlign, fDMASizeAlign );
	    //printf( "preSlack: %u - postSlack: %u - dmaSize: %u\n", preSlack, postSlack, dmaSize );
	    memcpy( dest, data, preSlack );
	    while ( dmaD < dmaSize )
		{
		if ( connectData->fLocalSize < (dmaSize-dmaD) )
		    thisDMASize = connectData->fLocalSize;
		else
		    thisDMASize = (dmaSize-dmaD);
		// Copy a chunk into the DMA source area...
		//printf( "thisDMASize: %u - dmaD: %u\n", thisDMASize, dmaD );
		memcpy( connectData->fLocalAddr, data+preSlack+dmaD, thisDMASize );
		// .. and DMA it to the remote blob buffer.
		SCIEnqueueDMATransfer( dmaQueue, connectData->fLocalSegment, connectData->fRemoteSegment, 0, remBlockOffset+dmaD, thisDMASize, 0, &mySCIError );
		if ( mySCIError != SCI_ERR_OK )
		    {
		    LOG( MLUCLog::kWarning, "BCLSCIBlobCommunication::TransferBlob", "DMA Transfer Enqueue Error" )
			<< "Error enqueuing DMA transfer: 0x" << MLUCLog::kHex << mySCIError << " ("
			<< MLUCLog::kDec << mySCIError << ") Not using DMA." << ENDLOG;
		    // BlobSendError( ENXIO, this, msgAddress, id, callback );
		    ok = false;
		    goto notOK;
		    }
		SCIPostDMAQueue( dmaQueue, 0, 0, 0, &mySCIError );
		if ( mySCIError != SCI_ERR_OK )
		    {
		    LOG( MLUCLog::kWarning, "BCLSCIBlobCommunication::TransferBlob", "DMA Post Error" )
			<< "Error posting DMA queue: 0x" << MLUCLog::kHex << mySCIError << " ("
			<< MLUCLog::kDec << mySCIError << ") Not using DMA." << ENDLOG;
		    // BlobSendError( ENXIO, this, msgAddress, id, callback );
		    ok = false;
		    goto notOK;
		    }

		SCIWaitForDMAQueue( dmaQueue, SCI_INFINITE_TIMEOUT, 0, &mySCIError );
		if ( mySCIError != SCI_ERR_OK )
		    {
		    LOG( MLUCLog::kWarning, "BCLSCIBlobCommunication::TransferBlob", "DMA Wait Error" )
			<< "Error waiting for DMA queue: 0x" << MLUCLog::kHex << mySCIError << " ("
			<< MLUCLog::kDec << mySCIError << ") Not using DMA." << ENDLOG;
		    // BlobSendError( ENXIO, this, msgAddress, id, callback );
		    ok = false;
		    goto notOK;
		    }

		dmaQueueState = SCIDMAQueueState( dmaQueue );
		if ( dmaQueueState != SCI_DMAQUEUE_DONE )
		    {
		    LOG( MLUCLog::kWarning, "BCLSCIBlobCommunication::TransferBlob", "DMA Queue State Error" )
			<< "Error determining DMA queue state: 0x" << MLUCLog::kHex << mySCIError << " ("
			<< MLUCLog::kDec << mySCIError << ") Not using DMA." << ENDLOG;
		    // BlobSendError( ENXIO, this, msgAddress, id, callback );
		    ok = false;
		    }
		
		notOK: // If we come here by fallthrough (not via goto), it is actually ok...
		SCIResetDMAQueue( dmaQueue, 0, &mySCIError );

		if ( !ok )
		    memcpy( dest+preSlack+dmaD, data+preSlack+dmaD, thisDMASize );
		dmaD += thisDMASize;
		ok = true;
		}
	    }
	else
	    {
	    ok = false;
	    }

#else
	ok = false;
#endif
	if ( !ok )
	    {
	    memcpy( dest, data, size );
	    }
#ifndef NO_SIMPLE_SEND_TEST
	gettimeofday( &start, NULL );
	while ( *endPD != *endPS )
	  {
	  if ( timeout )
	      {
	      gettimeofday( &now, NULL );
	      tdiff = (now.tv_sec-start.tv_sec)*1000 + (now.tv_usec-start.tv_usec)/1000;
	      if ( tdiff > timeout ) 
		  {
		  LOG( MLUCLog::kError, "SCI Blob Transfer", "Connection timeout" )
		      << "Segment 0x" << MLUCLog::kHex << fComHelper.fSegmentID << ": " << "Timeout writing message to remote address " << *(BCLSCIAddressStruct*)msgAddress
		      << " in SCI blob transfer." << ENDLOG;
		  BlobSendError( ETIMEDOUT, this, msgAddress, id, callback );
		  return ETIMEDOUT;
		  }
	      }
	  }
#endif
	nextOffset = blOffset+size;
	dataIter++;
	sizeIter++;
	if ( offsIter != offsEnd )
	    offsIter++;
	}
#ifndef SISCI_NO_DMA
    if ( fDMA && dmaOK )
	SCIRemoveDMAQueue( dmaQueue, 0, &mySCIError );
#endif
    fComHelper.DestroyLocalSegment( connectData );
    if ( !found )
	fComHelper.DisconnectFromRemote( connectData, callback, timeout, timeout );
    return 0;
    }


// Not supported currently; Might be introduced back in later versions. 
// bool BCLSCIBlobCommunication::IsTransferComplete( BCLTransferID transfer )
//     {
//     return true;
//     }

uint8* BCLSCIBlobCommunication::GetBlobData( BCLTransferID transfer )
    {
    int ret;
    uint64 offset = (uint64)transfer;
    if ( offset >= fBufferSize )
	{
	BlobReceiveError( ENOENT, this, transfer, NULL );
	return NULL;
	}
#ifndef NO_PARANOIA
    vector<TSCIBlockData>::iterator iter, end;
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	{
	BlobReceiveError( ENXIO, this, transfer, NULL );
	return NULL;
	}
    iter = fUsedBlocks.begin();
    end = fUsedBlocks.end();
    while ( iter != end )
	{
	if ( iter->fBlockOffset == offset )
	    break;
	iter++;
	}
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	{
	BlobReceiveError( ENXIO, this, transfer, NULL );
	return NULL;
	}
    if ( iter == end )
	{
	BlobReceiveError( ENOENT, this, transfer, NULL );
	return NULL;
	}
#endif
    return fBufferAddress+offset;
    }

uint64 BCLSCIBlobCommunication::GetBlobOffset( BCLTransferID transfer )
    {
    int ret;
    uint64 offset = (uint64)transfer;
    if ( offset >= fBufferSize )
	{
	BlobReceiveError( ENOENT, this, transfer, NULL );
	return (uint64)-1;
	}
#ifndef NO_PARANOIA
    vector<TSCIBlockData>::iterator iter, end;
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	{
	BlobReceiveError( ENXIO, this, transfer, NULL );
	return (uint64)-1;
	}
    iter = fUsedBlocks.begin();
    end = fUsedBlocks.end();
    while ( iter != end )
	{
	if ( iter->fBlockOffset == offset )
	    break;
	iter++;
	}
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	{
	BlobReceiveError( ENXIO, this, transfer, NULL );
	return (uint64)-1;
	}
    if ( iter == end )
	{
	BlobReceiveError( ENOENT, this, transfer, NULL );
	return (uint64)-1;
	}
#endif
    return offset;
    }

void BCLSCIBlobCommunication::ReleaseBlob( BCLTransferID transfer )
    {
    int ret;
    uint64 offset = (uint64)transfer;
    if ( offset >= fBufferSize )
	{
	LOG( MLUCLog::kWarning, "BCLSCIBlobCommunication::ReleaseBlob", "Blob offset too large" )
	    << "Blob offset " << MLUCLog::kDec << offset << " (0x" << MLUCLog::kHex << offset 
	    << ") derived from transfer id 0x" << transfer << " is larger than buffer size " << MLUCLog::kDec
	    << fBufferSize << "." << ENDLOG;
	GeneralError( ENOENT, this, NULL );
	return;
	}
    vector<TSCIBlockData>::iterator iter, end;
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	{
	LOG( MLUCLog::kWarning, "BCLSCIBlobCommunication::ReleaseBlob", "Mutex lock error" )
	    << "Error locking block mutex: " << strerror(ret) << " (" << MLUCLog::kDec << ret
	    << ")." << ENDLOG;
	GeneralError( ENXIO, this, NULL );
	return;
	}
    iter = fUsedBlocks.begin();
    end = fUsedBlocks.end();
    bool found = false;
	LOG( MLUCLog::kDebug, "BCLSCIBlobCommunication::ReleaseBlob", "Block to Free" )
	    << "Block to free Offset: " << MLUCLog::kDec << offset 
	    << "." << ENDLOG;
    while ( iter != end )
	{
	LOG( MLUCLog::kDebug, "BCLSCIBlobCommunication::ReleaseBlob", "Used Block" )
	    << "Used block offset: " << MLUCLog::kDec << iter->fBlockOffset << " - size: "
	    << iter->fBlockSize << "." << ENDLOG;
	if ( iter->fBlockOffset == offset )
	    {
	    LOG( MLUCLog::kDebug, "BCLSCIBlobCommunication::ReleaseBlob", "Used Block Found" )
		<< "Used block found, offset: " << MLUCLog::kDec << iter->fBlockOffset << " - size: "
		<< iter->fBlockSize << "." << ENDLOG;
	    found = true;
	    bool freeFound = false;
	    vector<TSCIBlockData>::iterator freeIter, freeEnd;
	    freeIter = fFreeBlocks.begin();
	    freeEnd = fFreeBlocks.end();
	    while ( freeIter != freeEnd )
		{
		LOG( MLUCLog::kDebug, "BCLSCIBlobCommunication::ReleaseBlob", "Free Block" )
		    << "Free block offset: " << MLUCLog::kDec << freeIter->fBlockOffset << " - size: "
		    << freeIter->fBlockSize << "." << ENDLOG;
		if ( iter->fBlockOffset+iter->fBlockSize==freeIter->fBlockOffset )
		    {
		    LOG( MLUCLog::kDebug, "BCLSCIBlobCommunication::ReleaseBlob", "Free Block Merged" )
			<< "Merged used block to beginning of free..." << ENDLOG;
		    freeIter->fBlockOffset = iter->fBlockOffset;
		    freeIter->fBlockSize += iter->fBlockSize;
		    freeFound = true;
		    break;
		    }
		if ( iter->fBlockOffset==freeIter->fBlockOffset+freeIter->fBlockSize )
		    {
		    LOG( MLUCLog::kDebug, "BCLSCIBlobCommunication::ReleaseBlob", "Free Block Merged" )
			<< "Merged used block to end of free..." << ENDLOG;
		    freeIter->fBlockSize += iter->fBlockSize;
		    if ( freeIter!=freeEnd && 
			 (freeIter+1)->fBlockOffset==(freeIter->fBlockOffset+freeIter->fBlockSize) )
			{
			LOG( MLUCLog::kDebug, "BCLSCIBlobCommunication::ReleaseBlob", "Free Block Merged" )
			    << "Merged following free block..." << ENDLOG;

			freeIter->fBlockSize += (freeIter+1)->fBlockSize;
			fFreeBlocks.erase( freeIter+1 );
			}
		    freeFound = true;
		    break;
		    }
		if ( freeIter->fBlockOffset > iter->fBlockOffset )
		    {
		    LOG( MLUCLog::kDebug, "BCLSCIBlobCommunication::ReleaseBlob", "Free Block Merged" )
			<< "Inserted used block..." << ENDLOG;

		    fFreeBlocks.insert( freeIter, *iter );
		    freeFound = true;
		    break;
		    }
		freeIter++;
		}
	    if ( !freeFound )
		{
		    LOG( MLUCLog::kDebug, "BCLSCIBlobCommunication::ReleaseBlob", "Free Block Merged" )
			<< "Inserted used block at end..." << ENDLOG;
		fFreeBlocks.insert( fFreeBlocks.end(), *iter );
		}
	    fUsedBlocks.erase( iter );
	    break;
	    }
	iter++;
	}
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	{
	LOG( MLUCLog::kWarning, "BCLSCIBlobCommunication::ReleaseBlob", "Mutex unlock error" )
	    << "Error unlocking block mutex: " << strerror(ret) << " (" << MLUCLog::kDec << ret
	    << ")." << ENDLOG;
	GeneralError( ENXIO, this, NULL );
	return;
	}
    if ( !found )
	{
	LOG( MLUCLog::kWarning, "BCLSCIBlobCommunication::ReleaseBlob", "No free block" )
	    << "No used block with the needed offset of " << MLUCLog::kDec << offset << " (0x" << MLUCLog::kHex << offset 
	    << ") derived from transfer id 0x" << transfer << " could be found." << ENDLOG;
	GeneralError( ENOENT, this, NULL );
	return;
	}
    return;
    }


uint32 BCLSCIBlobCommunication::GetAddressLength()
    {
    return sizeof(BCLSCIAddressStruct);
    }

uint32 BCLSCIBlobCommunication::GetComID()
    {
    return kSCIComID;
    }

BCLSCIAddressStruct* BCLSCIBlobCommunication::NewAddress()
    {
    BCLSCIAddressStruct* addr = new BCLSCIAddressStruct;
    if ( !addr )
	return NULL;
    BCLSCIAddress addrClass;
    *addr = *(addrClass.GetData());
    addr->fComID = kSCIComID;
    return addr;
    }

void BCLSCIBlobCommunication::DeleteAddress( BCLAbstrAddressStruct* address )
    {
    delete (BCLSCIAddressStruct*)address;
    }

bool BCLSCIBlobCommunication::AddressEquality( const BCLAbstrAddressStruct* addr1, const BCLAbstrAddressStruct* addr2 )
    {
    if ( addr1->fComID != kSCIComID || addr2->fComID != kSCIComID )
	return false;
    return *(BCLSCIAddressStruct*)addr1 == *(BCLSCIAddressStruct*)addr2;
    }

BCLSCIBlobCommunication::operator bool()
    {
    return (bool)fComHelper;
    }

#ifndef DEBUG_NOTHREADS
void BCLSCIBlobCommunication::QuitRequestHandler()
    {
    //fQuitRequestHandler = true;
    int ret=-1;
    BCLIntSCIBlobComMsg msg;
    msg.GetData()->fMsgType = kSCIBlobQuitMsg;
#ifdef SCI_SIMPLE_TIMEOUT
    unsigned long timeoutCount=0;
#endif
    if ( !fMsgCom )
	{
	LOG( MLUCLog::kError, "BCLSCIBlobCommunication::QuitRequestHandler", "No msg com object" )
	    << "Error sending request handler quit message. No msg communication object..." << ENDLOG;
	GeneralError( EFAULT, this, &fSCIAddress );
	}
    while ( !fRequestHandlerQuitted )
	{
	BCLAbstrAddressStruct* addr = fMsgCom->NewAddress();
	memcpy( addr, fMsgCom->GetAddress(), fMsgCom->GetAddress()->fLength );
	//*addr = *(fMsgCom->GetAddress());
	LOG( MLUCLog::kDebug, "BCLSCIBlobCommunication::QuitRequestHandler", "Sending Quit message" )
	    << "Trying to send quit message to request handler at address " << *addr << "." << ENDLOG;
	if ( addr->fComID == kSCIComID )
	    {
	    LOG( MLUCLog::kDebug, "BCLSCIBlobCommunication::QuitRequestHandler", "Sending Quit message" )
		<< "Request handler address: " << *(BCLSCIAddressStruct*)addr << "." << ENDLOG;
	    LOG( MLUCLog::kDebug, "BCLSCIBlobCommunication::QuitRequestHandler", "Sending Quit message" )
		<< "Msg com address: " << *(BCLSCIAddressStruct*)(fMsgCom->GetAddress()) << "." << ENDLOG;
	    }
	ret = fMsgCom->Send( addr, msg.GetData() );
	fMsgCom->DeleteAddress( addr );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "BCLSCIBlobCommunication::QuitRequestHandler", "Sending Quit message" )
		<< "Error trying to send quit message to request handler: " << strerror(ret) << " ("
		<< ret << ")." << ENDLOG;
	    GeneralError( ret, this, &fSCIAddress );
	    // XXX
	    if ( ret == ENODEV )
		break;
	    }
	usleep( 0 );
#ifdef SCI_SIMPLE_TIMEOUT
	if ( timeoutCount++ >= SIMPLE_TIMEOUT_MAX )
	    break;
#endif
	}
#ifdef SCI_SIMPLE_TIMEOUT
    if ( timeoutCount >= SIMPLE_TIMEOUT_MAX || ret )
	{
	fRequestThread.Abort();
#endif
	return;
	}
    fRequestThread.Join();
    }
#endif



void BCLSCIBlobCommunication::RequestHandler()
    {
    int ret;
    fQuitRequestHandler = false;
    BCLAbstrAddressStruct* addr;
    addr = fMsgCom->NewAddress();
    if ( !addr )
	{
	LOG( MLUCLog::kError, "BCLSCIBlobCommunication::RequestHandler", "Allocating message address" )
	    << "Out of memory allocating message communication address structure." << ENDLOG;
	GeneralError( ENOMEM, this, NULL );
	return;
	}
    BCLMessageStruct* gMsg;
    BCLIntSCIBlobComMsgStruct* msg;
    fRequestHandlerQuitted = false;
    do
	{
	ret = fMsgCom->Receive( addr, gMsg );
	if ( !this  )
	    {
	    LOG( MLUCLog::kFatal, "BCLSCIBlobCommunication::RequestHandler", "Message receive" )
		<< "No this pointer available. Receive return value: " << strerror(ret) << " ("
		<< MLUCLog::kDec << ret << "). Aborting..." << ENDLOG;
	    return;
	    }
	if ( !fMsgCom  )
	    {
	    LOG( MLUCLog::kFatal, "BCLSCIBlobCommunication::RequestHandler", "Message receive" )
		<< "No msg com object available. Receive return value: " << strerror(ret) << " ("
		<< MLUCLog::kDec << ret << "). Aborting..." << ENDLOG;
	    fRequestHandlerQuitted = true;
	    return;
	    }
	if ( ret )
	    {
	    switch ( ret )
		{
		case ENODEV:
		    LOG( MLUCLog::kError, "BCLSCIBlobCommunication::RequestHandler", "Message receive" )
			<< "Error when trying to to receive message: " << strerror(ret) << " (" << ret 
			<< "). Aborting message loop..." << ENDLOG;
		    GeneralError( ENODEV, this, NULL );
		    fQuitRequestHandler = true;
		    return;
		default:
		    LOG( MLUCLog::kError, "BCLSCIBlobCommunication::RequestHandler", "Message receive" )
			<< "Error when trying to to receive message: " << strerror(ret) << " (" << ret << ")."
			<< ENDLOG;
		    break;
		}
	    continue;
	    }
	if ( !gMsg )
	    {
	    LOG( MLUCLog::kError, "BCLSCIBlobCommunication::RequestHandler", "Message receive" )
		<< "Error when trying to to receive message: " << strerror(ret) << " (" << ret << ")."
		<< ENDLOG;
	    GeneralError( ENODEV, this, NULL );
	    continue;
	    }
	msg = (BCLIntSCIBlobComMsgStruct*)gMsg;
	switch ( msg->fMsgType )
	    {
	    case kSCIBlobQueryAddressMsg:
		LOG( MLUCLog::kInformational, "BCLSCIBlobCommunication::RequestHandler", "Address query" )
		    << "Address query received from remote node " << *addr << "." << ENDLOG;
		msg->fNodeID = fSCIAddress.fNodeID;
		msg->fAdapterNr = fSCIAddress.fAdapterNr;
		msg->fSegmentID = fSCIAddress.fSegmentID;
		msg->fMsgType = kSCIBlobAddressMsg;
		if ( fReplyTimeout )
		    ret = fMsgCom->Send( addr, msg, fReplyTimeout );
		else
		    ret = fMsgCom->Send( addr, msg );
		if ( ret )
		    {
		    LOG( MLUCLog::kError, "BCLSCIBlobCommunication::RequestHandler", "Address message send" )
			<< "Error when trying to send address message reply: " << strerror(ret) << " ("
			<< ret << ")." << ENDLOG;
		    GeneralError( ret, this, addr );
		    }
		fMsgCom->ReleaseMsg( gMsg );
		break;
	    case kSCIBlobQueryBlobSizeMsg:
		LOG( MLUCLog::kDebug, "BCLSCIBlobCommunication::RequestHandler", "Blob size query" )
		    << "Blob size query received from remote node " << *addr 
		    << "." << ENDLOG;
		msg->fMsgType = kSCIBlobBlobSizeReplyMsg;
		msg->fBlockSize = fBlobBufferSize;
		if ( fReplyTimeout )
		    ret = fMsgCom->Send( addr, msg, fReplyTimeout );
		else
		    ret = fMsgCom->Send( addr, msg );
		if ( ret )
		    {
		    LOG( MLUCLog::kError, "BCLSCIBlobCommunication::RequestHandler", "Blob size message send" )
			<< "Error when trying to send blob size message reply: " << strerror(ret) << " ("
			<< ret << ")." << ENDLOG;
		    GeneralError( ret, this, addr );
		    }
		LOG( MLUCLog::kDebug, "BCLSCIBlobCommunication::RequestHandler", "Blob size query" )
		    << "Blob size reply sent to remote node " << *addr << "." << ENDLOG;
		break;
	    case kSCIBlobQueryFreeBlockMsg:
		LOG( MLUCLog::kInformational, "BCLSCIBlobCommunication::RequestHandler", "Blob query" )
		    << "Blob query for 0x" << MLUCLog::kHex << msg->fBlockSize << " (" << MLUCLog::kDec
		    << msg->fBlockSize << ") bytes received from remote node " << *addr 
		    << "." << ENDLOG;
		TSCIBlockData block;
		ret = GetFreeBlock( msg->fBlockSize, block );
		if ( ret )
		    {
		    LOG( MLUCLog::kError, "BCLSCIBlobCommunication::RequestHandler", "Free block request" )
			<< "Error while trying to allocate free block request for " << MLUCLog::kDec 
			<< msg->fBlockSize
			<< " bytes: " << strerror(ret) << " (" << ret << ")." << ENDLOG;
		    GeneralError( ret, this, addr );
		    }
		if ( block.fBlockOffset==(uint32)-1 )
		    {
		    LOG( MLUCLog::kWarning, "BCLSCIBlobCommunication::RequestHandler", "Free block request" )
			<< "Could not allocate requested free block of " << MLUCLog::kDec << msg->fBlockSize
			<< " bytes." << ENDLOG;
		    }
		msg->fBlockOffset = block.fBlockOffset;
		msg->fMsgType = kSCIBlobFreeBlockMsg;
		if ( fReplyTimeout )
		    ret = fMsgCom->Send( addr, msg, fReplyTimeout );
		else
		    ret = fMsgCom->Send( addr, msg );
		if ( ret )
		    {
		    LOG( MLUCLog::kError, "BCLSCIBlobCommunication::RequestHandler", "block message send" )
			<< "Error when trying to send block message reply: " << strerror(ret) << " ("
			<< ret << ")." << ENDLOG;
		    GeneralError( ret, this, addr );
		    }
		fMsgCom->ReleaseMsg( gMsg );
		break;
	    case kSCIBlobConnectRequestMsg:
		{
		LOG( MLUCLog::kInformational, "BCLSCIBlobCommunication::RequestHandler", "Connection request" )
		    << "Connection request received." << ENDLOG;
		BCLSCIAddress blbAddr;
		blbAddr.GetData()->fNodeID = msg->fNodeID;
		blbAddr.GetData()->fAdapterNr = msg->fAdapterNr;
		blbAddr.GetData()->fSegmentID = msg->fSegmentID;
		ret = Connect( addr, blbAddr.GetData(), false, fReplyTimeout, fReplyTimeout );
		if ( ret )
		    {
		    LOG( MLUCLog::kError, "BCLSCIBlobCommunication::RequestHandler", "Requested connect failed" )
			<< "Error making requested connection to " << *addr << ": " << strerror(ret) 
			<< " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
		    GeneralError( ret, this, addr );
		    }
		break;
		}
	    case kSCIBlobDisconnectMsg:
		{
		LOG( MLUCLog::kInformational, "BCLSCIBlobCommunication::RequestHandler", "Disconnect request" )
		    << "Disonnect request received." << ENDLOG;
		BCLSCIAddress blAddr;
		blAddr.GetData()->fNodeID = msg->fNodeID;
		blAddr.GetData()->fAdapterNr = msg->fAdapterNr;
		blAddr.GetData()->fSegmentID = msg->fSegmentID;
		Disconnect( addr, blAddr.GetData(), fReplyTimeout, fReplyTimeout );
		break;
		}
	    case kSCIBlobLockRequestMsg:
		{
		LOG( MLUCLog::kInformational, "BCLSCIBlobCommunication::RequestHandler", "Lock request" )
		    << "Lock request received." << ENDLOG;
		ret = LockConnection( addr, false, fReplyTimeout, fReplyTimeout );
		if ( ret )
		    {
		    LOG( MLUCLog::kError, "BCLSCIBlobCommunication::RequestHandler", "Requested lock failed" )
			<< "Error making requested lock to " << *addr << ": " << strerror(ret) 
			<< " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
		    GeneralError( ret, this, addr );
		    }
		break;
		}
	    case kSCIBlobUnlockMsg:
		LOG( MLUCLog::kInformational, "BCLSCIBlobCommunication::RequestHandler", "Unlock request" )
		    << "Unlock request received." << ENDLOG;
		UnlockConnection( addr, false, fReplyTimeout, fReplyTimeout );
		break;
	    case kSCIBlobAddressMsg:
		if ( 1 )
		    {
		    LOG( MLUCLog::kInformational, "BCLSCIBlobCommunication::RequestHandler", "Address reply" )
			<< "Address reply received from node " << *addr << "." << ENDLOG;
		    }
		else
		    case kSCIBlobFreeBlockMsg:
                    if ( 1 )
			{
			LOG( MLUCLog::kInformational, "BCLSCIBlobCommunication::RequestHandler", "Blob reply" )
			    << "Blob reply received from node " << *addr << "." << ENDLOG;
			}
		    else
			case kSCIBlobBlobSizeReplyMsg:
                        {
			LOG( MLUCLog::kInformational, "BCLSCIBlobCommunication::RequestHandler", "Blob size reply" )
			    << "Blob size reply received from node " << *addr << "." << ENDLOG;
			}
		ret = pthread_mutex_lock( &fMsgMutex );
		if ( ret )
		    {
		    LOG( MLUCLog::kError, "BCLSCIBlobCommunication::RequestHandler", "Mutex error" )
			<< "Error trying to lock message mutex for inserting message: " 
			<< strerror(ret) << " (" << ret << ")." << ENDLOG;
		    GeneralError( ret, this, addr );
		    break;
		    }
		fMsgs.insert( fMsgs.end(), *msg );
		ret = pthread_mutex_unlock( &fMsgMutex );
		if ( ret )
		    {
		    LOG( MLUCLog::kError, "BCLSCIBlobCommunication::RequestHandler", "Mutex error" )
			<< "Error trying to unlock message mutex for inserting message: " 
			<< strerror(ret) << " (" << ret << ")." << ENDLOG;
		    GeneralError( ret, this, addr );
		    }
		fDataArrivedSignal.Signal();
		break;
	    case kSCIBlobQuitMsg:
		fQuitRequestHandler = true; 
		break;
	    }
	}
    while ( !fQuitRequestHandler );
    LOG( MLUCLog::kInformational, "BCLSCIBlobCommunication::RequestHandler", "Request handler shutting down" )
	<< "Shutting down SCI blob com request handler..." << ENDLOG;
    fRequestHandlerQuitted = true;
    }

int BCLSCIBlobCommunication::Connect( BCLAbstrAddressStruct* msgAddress, BCLSCIAddressStruct* blobAddress, bool openOnDemand, bool useTimeout, unsigned long timeout_ms )
    {
    if ( !msgAddress )
	{
	LOG( MLUCLog::kError, "SCI Blob Connect", "Address error" )
	    << "Wrong address (NULL pointer) in SCI Blob Connect" << ENDLOG;
	ConnectionError( EFAULT, this, msgAddress );
	return EFAULT;
	}
    if ( !fMsgCom )
	{
	LOG( MLUCLog::kError, "SCI Blob Connect", "No msg communication" )
	    << "No message communication object configured in SCI Blob Connect" << ENDLOG;
	ConnectionError( ENODEV, this, msgAddress );
	return ENODEV;
	}
    int ret;
    if ( useTimeout )
	ret = fMsgCom->Connect( msgAddress, openOnDemand, timeout_ms );
    else
	ret = fMsgCom->Connect( msgAddress, openOnDemand );
    if ( ret )
	{
	ConnectionError( ret, this, msgAddress );
	return ret;
	}
    LOG( MLUCLog::kInformational, "SCI Blob Connect", "Msg com connected" )
	<< "Message communication object connected." << ENDLOG;
    BCLSCIAddress rbaC;
    BCLSCIAddressStruct remoteBlobAddress, *rba = rbaC.GetData();
    remoteBlobAddress = *rba;
    if ( !blobAddress )
	{
	ret = GetRemoteBlobAddress( msgAddress, &remoteBlobAddress, useTimeout, timeout_ms );
	if ( ret )
	    {
	    ConnectionError( ret, this, msgAddress );
	    return ret;
	    }
	LOG( MLUCLog::kInformational, "SCI Blob Connect", "Remote Blob Address" )
	    << "Got remote Blob address: " << remoteBlobAddress << "." << ENDLOG;
	}
    else
	{
	remoteBlobAddress = *blobAddress;
	LOG( MLUCLog::kInformational, "SCI Blob Connect", "Remote Blob Address" )
	    << "Remote Blob address set to " << remoteBlobAddress << "." << ENDLOG;
	}
    ret = fComHelper.Connect( &remoteBlobAddress, NULL, openOnDemand, useTimeout, timeout_ms );
    if ( ret )
	{
	if ( useTimeout )
	    fMsgCom->Disconnect( msgAddress, timeout_ms );
	else
	    fMsgCom->Disconnect( msgAddress );
	return ret;
	}
    TSCIRemoteBlobAddress rbas;
    rbas.fMsgAddress = fMsgCom->NewAddress();
    if ( rbas.fMsgAddress && rbas.fMsgAddress->fLength==msgAddress->fLength )
	{
	//*(rbas.MsgAddress) = *msgAddress;
	memcpy( rbas.fMsgAddress, msgAddress, msgAddress->fLength );
	rbas.fBlobAddress = remoteBlobAddress;
	fRemoteBlobAddresses.insert( fRemoteBlobAddresses.end(), rbas );
	}
    if ( !blobAddress )
	{
	BCLIntSCIBlobComMsg msg;
	msg.GetData()->fMsgType = kSCIBlobConnectRequestMsg;
	msg.GetData()->fNodeID = fSCIAddress.fNodeID;
	msg.GetData()->fAdapterNr = fSCIAddress.fAdapterNr;
	msg.GetData()->fSegmentID = fSCIAddress.fSegmentID;
	LOG( MLUCLog::kInformational, "SCI Blob Connect", "Sending connection request" )
	    << "Sending connection request to remote side." << ENDLOG;
	if ( useTimeout )
	    ret = fMsgCom->Send( msgAddress, msg.GetData(), timeout_ms );
	else
	    ret = fMsgCom->Send( msgAddress, msg.GetData() );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "SCI Blob Connect", "Connection request" )
		<< "Error sending connection request to remote msg com object" << ENDLOG;
	    ConnectionError( ret, this, msgAddress );
	    return ret;
	    }
	}
    LOG( MLUCLog::kInformational, "SCI Blob Connect", "Connection established" )
	<< "Connection to established." << ENDLOG;
    return 0;
    }

int BCLSCIBlobCommunication::Disconnect( BCLAbstrAddressStruct* address, BCLSCIAddressStruct* blobAddress, bool useTimeout, unsigned long timeout_ms )
    {
    if ( !address )
	{
	LOG( MLUCLog::kError, "SCI Blob Disconnect", "Address error" )
	    << "Wrong address (NULL pointer) in SCI Blob Disconnect" << ENDLOG;
	ConnectionError( EFAULT, this, address );
	return EFAULT;
	}
    if ( !fMsgCom )
	{
	LOG( MLUCLog::kError, "SCI Blob Disconnect", "No msg communication" )
	    << "No message communication object configured in SCI Blob Disconnect" << ENDLOG;
	ConnectionError( ENODEV, this, address );
	return ENODEV;
	}
    int ret, ret1;
    BCLSCIAddressStruct remoteBlobAddress;
    if ( !blobAddress )
	{
	ret = GetRemoteBlobAddress( address, &remoteBlobAddress, useTimeout, timeout_ms );
	if ( ret )
	    {
	    ConnectionError( ret, this, address );
	    return ret;
	    }
	}
    else
	remoteBlobAddress = *blobAddress;
    ret1 = fComHelper.Disconnect( &remoteBlobAddress, NULL, useTimeout, timeout_ms );
    if ( blobAddress )
	{
	BCLIntSCIBlobComMsg msg;
	msg.GetData()->fMsgType = kSCIBlobDisconnectMsg;
	msg.GetData()->fNodeID = fSCIAddress.fNodeID;
	msg.GetData()->fAdapterNr = fSCIAddress.fAdapterNr;
	msg.GetData()->fSegmentID = fSCIAddress.fSegmentID;
	if ( useTimeout )
	    fMsgCom->Send( address, msg.GetData(), timeout_ms );
	else
	    fMsgCom->Send( address, msg.GetData() );
	}
    if ( useTimeout )
	ret = fMsgCom->Disconnect( address, timeout_ms );
    else
	ret = fMsgCom->Disconnect( address );
    vector<TSCIRemoteBlobAddress>::iterator iter, end;
    iter = fRemoteBlobAddresses.begin();
    end = fRemoteBlobAddresses.end();
    while ( iter != end )
	{
	if ( fMsgCom->AddressEquality( iter->fMsgAddress, address ) || 
	   iter->fBlobAddress == remoteBlobAddress )
	    {
	    fMsgCom->DeleteAddress( iter->fMsgAddress );
	    fRemoteBlobAddresses.erase( iter );
	    break;
	    }
	iter++;
	}
    if ( ret1 )
	return ret1;
    if ( ret )
	return ret;
    return 0;
    }

int BCLSCIBlobCommunication::LockConnection( BCLAbstrAddressStruct* msgAddress, bool initiator, bool useTimeout, unsigned long timeout_ms )
    {
    if ( !msgAddress )
	{
	LOG( MLUCLog::kError, "SCI Blob Lock", "Address error" )
	    << "Wrong address (NULL pointer) in SCI Blob Lock" << ENDLOG;
	LockError( EFAULT, this, msgAddress );
	return EFAULT;
	}
    if ( !fMsgCom )
	{
	LOG( MLUCLog::kError, "SCI Blob Lock", "No msg communication" )
	    << "No message communication object configured in SCI Blob Lock" << ENDLOG;
	LockError( ENODEV, this, msgAddress );
	return ENODEV;
	}
    if ( !fMsgCom->CanLockConnection() )
	{
	LOG( MLUCLog::kWarning, "SCI Blob Lock", "Msg com object unable to lock" )
	    << "Message communication object unabel to lock connections in SCI Blob Lock" << ENDLOG;
	//LockError( ENOTSUP, this, msgAddress );
	return ENOTSUP;
	}
    int ret;
    if ( useTimeout )
	ret = fMsgCom->LockConnection( msgAddress, timeout_ms );
    else
	ret = fMsgCom->LockConnection( msgAddress );
    if ( ret )
	{
	LockError( ret, this, msgAddress );
	return ret;
	}
    LOG( MLUCLog::kInformational, "SCI Blob Lock", "Msg com Locked" )
	<< "Message communication object locked." << ENDLOG;
    if ( initiator )
	{
	BCLIntSCIBlobComMsg msg;
	msg.GetData()->fMsgType = kSCIBlobLockRequestMsg;
	msg.GetData()->fNodeID = fSCIAddress.fNodeID;
	msg.GetData()->fAdapterNr = fSCIAddress.fAdapterNr;
	msg.GetData()->fSegmentID = fSCIAddress.fSegmentID;
	LOG( MLUCLog::kInformational, "SCI Blob Lock", "Sending lock request" )
	    << "Sending connection request to remote side." << ENDLOG;
	if ( useTimeout )
	    ret = fMsgCom->Send( msgAddress, msg.GetData(), timeout_ms );
	else
	    ret = fMsgCom->Send( msgAddress, msg.GetData() );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "SCI Blob Connect", "Connection request" )
		<< "Error sending connection request to remote msg com object" << ENDLOG;
	    LockError( ret, this, msgAddress );
	    return ret;
	    }
	}
    LOG( MLUCLog::kInformational, "SCI Blob Lock", "Lock established" )
	<< "Lock to established." << ENDLOG;
    return 0;
    }

int BCLSCIBlobCommunication::UnlockConnection( BCLAbstrAddressStruct* address, bool initiator, bool useTimeout, unsigned long timeout_ms )
    {
    if ( !address )
	{
	LOG( MLUCLog::kError, "SCI Blob Unlock", "Address error" )
	    << "Wrong address (NULL pointer) in SCI Blob Unlock" << ENDLOG;
	LockError( EFAULT, this, address );
	return EFAULT;
	}
    if ( !fMsgCom )
	{
	LOG( MLUCLog::kError, "SCI Blob Unlock", "No msg communication" )
	    << "No message communication object configured in SCI Blob Unlock" << ENDLOG;
	LockError( ENODEV, this, address );
	return ENODEV;
	}
    int ret = 0;
    if ( initiator )
	{
	BCLIntSCIBlobComMsg msg;
	msg.GetData()->fMsgType = kSCIBlobUnlockMsg;
	if ( useTimeout )
	    fMsgCom->Send( address, msg.GetData(), timeout_ms );
	else
	    fMsgCom->Send( address, msg.GetData() );
	}
    if ( useTimeout )
	ret = fMsgCom->UnlockConnection( address, timeout_ms );
    else
	ret = fMsgCom->UnlockConnection( address );
    return ret;
    }


int BCLSCIBlobCommunication::GetFreeBlock( uint64 size, TSCIBlockData& block )
    {
    int ret;
#ifndef SISCI_NO_DMA
    if ( size % fDMAAlign )
	size += fDMAAlign - (size % fDMAAlign);
#endif
    block.fBlockOffset = (uint64)-1;
    ret = pthread_mutex_lock( &fBlockMutex );
    if ( ret )
	return ret;
    vector<TSCIBlockData>::iterator iter, end, bestFit;
    bool bestFitSet = false;
    iter = fFreeBlocks.begin();
    end = fFreeBlocks.end();
#if __GNUC__<3
    bestFit = NULL;
#endif
    while ( iter != end )
	{
	if ( iter->fBlockSize >= size )
	    {
	    if ( !bestFitSet || bestFit->fBlockSize>iter->fBlockSize )
		{
		bestFitSet = true;
		bestFit = iter;
		}
	    }
	iter++;
	}
    if ( bestFitSet )
	{
	block.fBlockOffset = bestFit->fBlockOffset;
	block.fBlockSize = size;
	bestFit->fBlockOffset += size;
	bestFit->fBlockSize -= size;
	if ( bestFit->fBlockSize <= 0 )
	    fFreeBlocks.erase( bestFit );
	fUsedBlocks.insert( fUsedBlocks.end(), block );
	}
    ret = pthread_mutex_unlock( &fBlockMutex );
    if ( ret )
	return ret;
    //if ( !bestFit )
    //return ENOSPC;
    return 0;
    }



int BCLSCIBlobCommunication::GetRemoteBlobAddress( BCLAbstrAddressStruct* msgAddress, 
						   BCLSCIAddressStruct* blobAddress, bool useTimeout, unsigned long timeout_ms )
    {
    int ret;
    BCLIntSCIBlobComMsg msg;
    if ( !fMsgCom )
	return ENODEV;

    vector<TSCIRemoteBlobAddress>::iterator iter, end;
    iter = fRemoteBlobAddresses.begin();
    end = fRemoteBlobAddresses.end();
    while ( iter != end )
	{
	if ( fMsgCom->AddressEquality( iter->fMsgAddress, msgAddress ) )
	    {
	    *blobAddress = iter->fBlobAddress;
	    return 0;
	    }
	iter++;
	}

    msg.GetData()->fMsgType = kSCIBlobQueryAddressMsg;
    ret = GetQueryID( msg.GetData()->fQueryID );
    if ( ret )
	return ret;
    fDataArrivedSignal.Lock();
    if ( useTimeout )
	ret = fMsgCom->Send( msgAddress, msg.GetData(), timeout_ms );
    else
	ret = fMsgCom->Send( msgAddress, msg.GetData() );
    if ( ret )
	{
	fDataArrivedSignal.Unlock();
	return ret;
	}
    ret = WaitForMessage( msg.GetData()->fQueryID, msg.GetData(), useTimeout, timeout_ms );
    fDataArrivedSignal.Unlock();
    if ( ret )
	return ret;
    BCLSCIAddress sac;
    *blobAddress = *(sac.GetData());
    blobAddress->fNodeID = msg.GetData()->fNodeID;
    blobAddress->fAdapterNr = msg.GetData()->fAdapterNr;
    blobAddress->fSegmentID = msg.GetData()->fSegmentID;
    return 0;
    }


int BCLSCIBlobCommunication::GetQueryID( uint32& id )
    {
    int ret;
    ret = pthread_mutex_lock( &fMsgMutex );
    if ( ret )
	{
	return ENXIO;
	}
    id = fCurrentQueryID++;
    fCurrentQueryCount++;
    ret = pthread_mutex_unlock( &fMsgMutex );
    if ( ret )
	{
	return ENXIO;
	}
    return 0;
    }



int BCLSCIBlobCommunication::WaitForMessage( uint32 queryID, BCLIntSCIBlobComMsgStruct* msg, bool useTimeout, unsigned long timeout_ms )
    {
    bool found = false;
    int ret;
    BCLIntSCIBlobComMsg msgClass;
    vector<BCLIntSCIBlobComMsgStruct>::iterator iter, end;
    struct timeval start, now;
    unsigned long tdiff, twait;
    gettimeofday( &start, NULL );
    do
	{
	ret = pthread_mutex_lock( &fMsgMutex );
	if ( ret )
	    {
	    return ENXIO;
	    }
	iter = fMsgs.begin();
	end = fMsgs.end();
	while ( iter != end )
	    {
#if __GNUC__>=3
	    msgClass.Adopt( iter.base() );
#else
	    msgClass.Adopt( iter );
#endif
	    if ( iter->fQueryID == queryID )
		{
		found = true;
		*msg = *iter;
		fMsgs.erase( iter );
		fCurrentQueryCount--;
		break;
		}
	    iter++;
	    }
	ret = pthread_mutex_unlock( &fMsgMutex );
	if ( found )
	    {
	    LOG( MLUCLog::kDebug, "BCLSCIBlobCommunication::WaitForMessage", "Message not found" )
		<< "Desired message (0x" << MLUCLog::kHex << queryID << "  found." << ENDLOG;
	    return 0;
	    }
	LOGM( MLUCLog::kDebug, "BCLSCIBlobCommunication::WaitForMessage", "Message not found", 200 )
	    << "Desired message (0x" << MLUCLog::kHex << queryID << " not found." << ENDLOG;
	if ( ret )
	    {
	    return ENXIO;
	    }
	gettimeofday( &now, NULL );
	tdiff = (now.tv_sec-start.tv_sec)*1000 + (now.tv_usec-start.tv_usec)/1000;
	if ( (useTimeout && tdiff > timeout_ms) || tdiff > fComHelper.fConnectionTimeout )
	    return ETIMEDOUT;
	if ( useTimeout )
	    twait = timeout_ms;
	else
	    twait = fComHelper.fConnectionTimeout;
	fDataArrivedSignal.Wait( twait );
	}
    while ( !found );
    LOG( MLUCLog::kDebug, "BCLSCIBlobCommunication::WaitForMessage", "Message not found" )
	<< "Desired message (0x" << MLUCLog::kHex << queryID << "  found." << ENDLOG;

    return 0;
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

