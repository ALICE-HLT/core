/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLAddressLogger.hpp"
#ifdef USE_TCP
#include "BCLTCPAddress.hpp"
#include "BCLTCPComID.hpp"
#endif
#ifdef USE_SCI
#include "BCLSCIAddress.hpp"
#include "BCLSCIComID.hpp"
#endif

bool BCLAddressLogger::fInited = false;
bool BCLAddressLogger::fIniting = false;
//BCLAddressLogger BCLAddressLogger::fLogger;
vector<BCLAddressLogger::TLogFuncData> BCLAddressLogger::fLogFuncData;
pthread_mutex_t BCLAddressLogger::fLogFuncMutex;


void BCLAddressLogger::Init()
    {
    if ( fInited )
	return;
    if ( fIniting )
	return;
    fIniting = true;
    pthread_mutex_init( &fLogFuncMutex, NULL );

#ifdef USE_TCP
    AddLogFunc( kTCPComID, LogTCPAddress );
#endif

#ifdef USE_SCI
    AddLogFunc( kSCIComID, LogSCIAddress );
#endif
    }

void BCLAddressLogger::AddLogFunc( uint32 comID, TLogFunc logFunc )
    {
    int ret;
    ret = pthread_mutex_lock( &fLogFuncMutex );
    if ( ret )
	{
	// XXX
	}
    bool found = false;
    vector<TLogFuncData>::iterator iter, end;
    iter = fLogFuncData.begin();
    end = fLogFuncData.end();
    while ( iter != end )
	{
	if ( iter->fComID == comID )
	    {
	    found = true;
	    break;
	    }
	iter++;
	}
    if ( !found )
	{
	TLogFuncData lfd;
	lfd.fComID = comID;
	lfd.fLogFunc = logFunc;
	fLogFuncData.insert( fLogFuncData.end(), lfd );
	}
    ret = pthread_mutex_unlock( &fLogFuncMutex );
    if ( ret )
	{
	// XXX
	}
    }

void BCLAddressLogger::DelLogFunc( uint32 comID )
    {
    int ret;
    ret = pthread_mutex_lock( &fLogFuncMutex );
    if ( ret )
	{
	// XXX
	}
    vector<TLogFuncData>::iterator iter, end;
    iter = fLogFuncData.begin();
    end = fLogFuncData.end();
    while ( iter != end )
	{
	if ( iter->fComID == comID )
	    {
	    fLogFuncData.erase( iter );
	    break;
	    }
	iter++;
	}
    ret = pthread_mutex_unlock( &fLogFuncMutex );
    if ( ret )
	{
	// XXX
	}
    }

MLUCLog::TLogMsg& BCLAddressLogger::LogAddress( MLUCLog::TLogMsg& log, const BCLAbstrAddressStruct& addr )
    {
    int ret;
    ret = pthread_mutex_lock( &fLogFuncMutex );
    if ( ret )
	{
	// XXX
	}
    vector<TLogFuncData>::iterator iter, end;
    bool found = false;
    iter = fLogFuncData.begin();
    end = fLogFuncData.end();
    while ( iter != end )
	{
	if ( iter->fComID == addr.fComID )
	    {
	    found = true;
	    (*iter->fLogFunc)( log, addr );
	    break;
	    }
	iter++;
	}
    ret = pthread_mutex_unlock( &fLogFuncMutex );
    if ( ret )
	{
	// XXX
	}
    if ( !found )
	log << "Unknown address with address id " << addr.fComID;
    return log;
    }

#ifdef USE_TCP	
MLUCLog::TLogMsg& BCLAddressLogger::LogTCPAddress( MLUCLog::TLogMsg& log, const BCLAbstrAddressStruct& addr )
    {
    const BCLTCPAddressStruct& tcpAddr = (const BCLTCPAddressStruct&)addr;
    return log << tcpAddr << " (Dot Notation IP: " << MLUCLog::kDec << ((tcpAddr.fIPNr & 0xFF000000) >> 24) << "."
	       << ((tcpAddr.fIPNr & 0x00FF0000) >> 16) << "." << ((tcpAddr.fIPNr & 0x0000FF00) >> 8) << "."
	       << ((tcpAddr.fIPNr & 0x000000FF) >> 0) << " - " << tcpAddr.fPortNr << ")";
    }
#endif

#ifdef USE_SCI
MLUCLog::TLogMsg& BCLAddressLogger::LogSCIAddress( MLUCLog::TLogMsg& log, const BCLAbstrAddressStruct& addr )
    {
    const BCLSCIAddressStruct& sciAddr = (const BCLSCIAddressStruct&)addr;
    return log << sciAddr;
    }
#endif



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
