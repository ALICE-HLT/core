/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: BCLUDAPLMsgCommunication.cpp 738 2005-11-16 07:51:25Z timm $ 
**
***************************************************************************
*/

#ifdef USE_UDAPL

#include "BCLUDAPLMsgCommunication.hpp"
#include "BCLUDAPLAddress.hpp"
#include "BCLUDAPLAddressOps.hpp"
#include "BCLMessage.hpp"
#include "BCLAddressLogger.hpp"
#include "MLUCLog.hpp"
#include <errno.h>
#include <stdlib.h>
#include <memory.h>
#include <sys/time.h>



BCLUDAPLMsgCommunication::BCLUDAPLMsgCommunication( int slotCntExp2 ):
    fComHelper( this ),
    fMessageAllocCache( 3, true, false ), 
    fAddressAllocCache( 3, true, false ), 
    fMessages( slotCntExp2, true )
    {
#ifdef INITIAL_CONDITIONSEM_UNLOCK
    // Define this when condition semaphore objects should always be unlocked initially.
    fDataArrivedSignal.Unlock();
#endif
    fMsgSeqNr = 0;
    }

BCLUDAPLMsgCommunication::~BCLUDAPLMsgCommunication()
    {
    if ( fComHelper.fSocket != -1 )
	Unbind();
    }

int BCLUDAPLMsgCommunication::Bind( BCLAbstrAddressStruct* address )
    {
    int ret;
    if ( !address )
	{
	LOG( MLUCLog::kError, "UDAPL Bind", "Address error" )
	    << "Wrong address (NULL pointer) in UDAPL Bind" << ENDLOG;
	BindError( EFAULT, this, address, NULL );
	return EFAULT;
	}
    if ( address->fComID != kUDAPLComID )
	{
	LOG( MLUCLog::kError, "UDAPL Bind", "Address error" )
	    << "Wrong address ID " << address->fComID << " in UDAPL Bind. (expected " 
	    << kUDAPLComID << ")."<< ENDLOG;
	BindError( EINVAL, this, address, NULL );
	return EINVAL;
	}
    fUDAPLAddress = *(BCLUDAPLAddressStruct*)address;
    fAddress = &fUDAPLAddress;
    ret = fComHelper.Bind( NULL, 0, BCLIntUDAPLComHelper::gkMaxMessageSize, 
			   BCLIntUDAPLComHelper::gkMaxMessageSize, &fUDAPLAddress, &fDataCallback );
    if ( ret )
	{
	LOG( MLUCLog::kError, "BCLUDAPLBlobCommunication::Bind", "Com helper bind error" )
	    << "Error binding UDAPL communication helper object: "
	    << strerror(ret) << " (" << MLUCLog::kDec << ret
	    << ")." << ENDLOG;
	fAddress = NULL;
	}
    return ret;
    }

int BCLUDAPLMsgCommunication::Unbind()
    {
    return fComHelper.Unbind();
    }

int BCLUDAPLMsgCommunication::Connect( BCLAbstrAddressStruct* address, bool openOnDemand )
    {
    if ( !address )
	{
	LOG( MLUCLog::kError, "UDAPL Connect", "Address error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address (NULL pointer) in UDAPL Connect" << ENDLOG;
	ConnectionError( EFAULT, this, address );
	return EFAULT;
	}
    if ( address->fComID != kUDAPLComID )
	{
	LOG( MLUCLog::kError, "UDAPL Connect", "Address error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address ID " << address->fComID << " in UDAPL Connect. (expected " 
	    << kUDAPLComID << ")."<< ENDLOG;
	ConnectionError( EINVAL, this, address );
	return EINVAL;
	}
    return fComHelper.Connect( (BCLUDAPLAddressStruct*)address, false, 0, openOnDemand );
    }

int BCLUDAPLMsgCommunication::Connect( BCLAbstrAddressStruct* address, unsigned long timeout_ms, bool openOnDemand )
    {
    if ( !address )
	{
	LOG( MLUCLog::kError, "UDAPL Connect", "Address error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address (NULL pointer) in UDAPL Connect" << ENDLOG;
	ConnectionError( EFAULT, this, address );
	return EFAULT;
	}
    if ( address->fComID != kUDAPLComID )
	{
	LOG( MLUCLog::kError, "UDAPL Connect", "Address error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address ID " << address->fComID << " in UDAPL Connect. (expected " 
	    << kUDAPLComID << ")."<< ENDLOG;
	ConnectionError( EINVAL, this, address );
	return EINVAL;
	}
    return fComHelper.Connect( (BCLUDAPLAddressStruct*)address, true, timeout_ms, openOnDemand );
    }

bool BCLUDAPLMsgCommunication::CanLockConnection()
    {
    return false;
    }

int BCLUDAPLMsgCommunication::LockConnection( BCLAbstrAddressStruct* )
    {
    return ENOTSUP;
    }

int BCLUDAPLMsgCommunication::LockConnection( BCLAbstrAddressStruct*, unsigned long )
    {
    return ENOTSUP;
    }

int BCLUDAPLMsgCommunication::UnlockConnection( BCLAbstrAddressStruct* )
    {
    return ENOTSUP;
    }

int BCLUDAPLMsgCommunication::UnlockConnection( BCLAbstrAddressStruct*, unsigned long )
    {
    return ENOTSUP;
    }

int BCLUDAPLMsgCommunication::Disconnect( BCLAbstrAddressStruct* address )
    {
    if ( !address )
	{
	LOG( MLUCLog::kError, "UDAPL Disconnect", "Address error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address (NULL pointer) in UDAPL Disconnect" << ENDLOG;
	ConnectionError( EFAULT, this, address );
	return EFAULT;
	}
    if ( address->fComID != kUDAPLComID )
	{
	LOG( MLUCLog::kError, "UDAPL Disconnect", "Address error" )
	    << "Wrong address ID " << address->fComID << " in UDAPL Disconnect. (expected " 
	    << kUDAPLComID << ")."<< ENDLOG;
	ConnectionError( EINVAL, this, address );
	return EINVAL;
	}
    return fComHelper.Disconnect( (BCLUDAPLAddressStruct*)address, false, 0 );
    }

int BCLUDAPLMsgCommunication::Disconnect( BCLAbstrAddressStruct* address, unsigned long timeout_ms )
    {
    if ( !address )
	{
	LOG( MLUCLog::kError, "UDAPL Disconnect", "Address error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address (NULL pointer) in UDAPL Disconnect" << ENDLOG;
	ConnectionError( EFAULT, this, address );
	return EFAULT;
	}
    if ( address->fComID != kUDAPLComID )
	{
	LOG( MLUCLog::kError, "UDAPL Disconnect", "Address error" )
	    << "Wrong address ID " << address->fComID << " in UDAPL Disconnect. (expected " 
	    << kUDAPLComID << ")."<< ENDLOG;
	ConnectionError( EINVAL, this, address );
	return EINVAL;
	}
    return fComHelper.Disconnect( (BCLUDAPLAddressStruct*)address, true, timeout_ms );
    }

int BCLUDAPLMsgCommunication::Send( BCLAbstrAddressStruct* address, BCLMessageStruct* msg, 
				  BCLErrorCallback* callback )
    {
    return Send( address, msg, false, 0, callback );
    }

int BCLUDAPLMsgCommunication::Send( BCLAbstrAddressStruct* address, BCLMessageStruct* msg, 
				  unsigned long timeout_ms, BCLErrorCallback* callback )
    {
    return Send( address, msg, true, timeout_ms, callback );
    }

int BCLUDAPLMsgCommunication::Receive( BCLAbstrAddressStruct* addr, BCLMessageStruct*& msg, BCLErrorCallback* callback )
    {
    return Receive( (BCLUDAPLAddressStruct*)addr, msg, false, 0, callback );
    }

int BCLUDAPLMsgCommunication::Receive( BCLAbstrAddressStruct* addr, BCLMessageStruct*& msg, unsigned long timeout, BCLErrorCallback* callback )
    {
    return Receive( (BCLUDAPLAddressStruct*)addr, msg, true, timeout, callback );
    }

void BCLUDAPLMsgCommunication::Reset()
    {
    // Is there anything to reset for this??
    }

int BCLUDAPLMsgCommunication::ReleaseMsg( BCLMessageStruct* msg )
    {
    FreeMessage( msg );
    return 0;
    }

uint32 BCLUDAPLMsgCommunication::GetAddressLength()
    {
    return sizeof(BCLUDAPLAddressStruct);
    }

uint32 BCLUDAPLMsgCommunication::GetComID()
    {
    return kUDAPLComID;
    }

BCLUDAPLAddressStruct* BCLUDAPLMsgCommunication::NewAddress()
    {
    BCLUDAPLAddressStruct* addr = (BCLUDAPLAddressStruct*)fAddressAllocCache.Get( sizeof(BCLUDAPLAddressStruct) );
    if ( !addr )
	return NULL;
    *addr = *(fDefaultAddress.GetData());
//     BCLUDAPLAddressStruct* addr = new BCLUDAPLAddressStruct;
//     if ( !addr )
// 	return NULL;
//     BCLUDAPLAddress addrClass;
//     *addr = *(addrClass.GetData());
    addr->fComID = kUDAPLComID;
    return addr;
    }

void BCLUDAPLMsgCommunication::DeleteAddress( BCLAbstrAddressStruct* address )
    {
    fAddressAllocCache.Release( (uint8*)address );
//     delete (BCLUDAPLAddressStruct*)address;
    }

bool BCLUDAPLMsgCommunication::AddressEquality( const BCLAbstrAddressStruct* addr1, const BCLAbstrAddressStruct* addr2 )
    {
    if ( addr1->fComID != kUDAPLComID || addr2->fComID != kUDAPLComID )
	return false;
    return *(BCLUDAPLAddressStruct*)addr1 == *(BCLUDAPLAddressStruct*)addr2;
    }

BCLUDAPLMsgCommunication::operator bool()
    {
    return true;
    }

BCLMessageStruct* BCLUDAPLMsgCommunication::NewMessage( uint32 size )
    {
    return (BCLMessageStruct*)fMessageAllocCache.Get( size );
//     return (BCLMessageStruct*)malloc( size );
    //return (BCLMessageStruct*)new uint8[ size ];
    }

void BCLUDAPLMsgCommunication::FreeMessage( BCLMessageStruct* msg )
    {
    fMessageAllocCache.Release( (uint8*)msg );
//     free( msg );
    //delete [] (uint8*)msg;
    }

int BCLUDAPLMsgCommunication::Send( BCLAbstrAddressStruct* address, BCLMessageStruct* msg, 
				  bool useTimeout, unsigned long timeout_ms, BCLErrorCallback* callback )
    {
    // Check that the address and msg are valid.
    if ( !address || !msg )
	{
	if ( !address )
	    {
	    LOG( MLUCLog::kError, "UDAPL Send", "Address error" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address (NULL pointer) in UDAPL Send" << ENDLOG;
	    }
	else
	    {
	    LOG( MLUCLog::kError, "UDAPL Send", "Message error" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong message (NULL pointer) in UDAPL Send" << ENDLOG;
	    }
	MsgSendError( EFAULT, this, address, msg, callback );
	return EFAULT;
	}
    // Check that the address is of the right type.
    if ( address->fComID != kUDAPLComID )
	{
	LOG( MLUCLog::kError, "UDAPL Send", "Address error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address ID " << address->fComID << " in UDAPL Send. (expected " 
	    << kUDAPLComID << ")."<< ENDLOG;
	MsgSendError( EINVAL, this, address, msg, callback );
	return EINVAL;
	}
    // Check that the address has the correct length.
    if ( address->fLength != sizeof(BCLUDAPLAddressStruct) )
	{
	LOG( MLUCLog::kError, "UDAPL Send", "Address error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address size " << MLUCLog::kDec << address->fLength 
	    << " (expected " << sizeof(BCLUDAPLAddressStruct) << ") in UDAPL Send." << ENDLOG;
	AddressError( EMSGSIZE, this, address, callback );
	return EMSGSIZE;
	}
    // Check that the message has a certain mimimum required size.
    // Beyond that size we cannot assume anything.
    if ( msg->fLength < sizeof(BCLMessageStruct) )
	{
	LOG( MLUCLog::kError, "UDAPL Send", "Message error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong message size " << MLUCLog::kDec << msg->fLength 
	    << " (expected at least" << sizeof(BCLMessageStruct) << ") in UDAPL Send." << ENDLOG;
	MsgSendError( EMSGSIZE, this, address, msg, callback );
	return EMSGSIZE;
	}
    // Make sure that possible SEGVs don't occur while the other side is waiting 
    // for the rest of our data...
    uint8 *tmp, b;
    tmp = (uint8*)msg;
    b = *tmp;
    b = *(tmp+msg->fLength-1);
    uint32 tmpMsgID;
    BCLNetworkData::Transform( fMsgSeqNr, tmpMsgID, kNativeByteOrder, msg->fDataFormats[kCurrentByteOrderIndex] );
    fMsgSeqNr++;
    msg->fMsgID = tmpMsgID;

    bool failure = true;
    DAT_RETURN dret;
    while ( failure )
	{
	failure = false;

	BCLIntUDAPLComHelper::ConnectionData cd;
	cd.fAddress = remoteBlobAddress;
	ret = fComHelper.ConnectToRemote( &cd, true, useTimeout, timeout_ms );
	if ( ret )
	    {
	    // Only hard failure here.
	    MsgSendError( ret, this, msgAddress, id, callback );
	    return ret;
	    }


	memcpy( (uint8*)fComHelper.fSendBuffer, (uint8*)msg, msg->fLength );
	DAT_LMR_TRIPLET l_iov;
	l_iov.pad = 0;
	l_iov.segment_length = msg->fLength;
	l_iov.lmr_context = fComHelper.fSendLMRContext;
	l_iov.virtual_address = fComHelper.fSendBuffer;
	DAT_DTO_COOKIE cookie;
	cookie.as_64 = BCLIntUDAPLComHelper::kMsgSendDTOCookie;

	dret = dat_ep_post_send( cd.fEP, 1, &l_iov, cookie, DAT_COMPLETION_DEFAULT_FLAG );
	
	if ( dret != DAT_SUCCESS )
	    {
	    LOG( MLUCLog::kError, "BCLUDAPLmsgCommunication::Send", "Error posting msg send" )
		<< "Error posting message send: "
		<< DatStrError(dret).c_str() << " ("
		<< MLUCLog::kDec << dret << ")." << ENDLOG;
	    fComHelper.DisconnectFromRemote( &cd, useTimeout, timeout_ms, true, false );
	    failure = true;
	    continue;
	    }
	DAT_EVENT blobEvt;
	DAT_COUNT qNr;
	if ( useTimeout )
	    dret = dat_evd_wait( cd.fDataEVD, timeout_ms*1000, 1, &blobEvt, &qNr );
	else
	    dret = dat_evd_wait( cd.fDataEVD, DAT_TIMEOUT_INFINITE, 1, &blobEvt, &qNr );
	if ( dret != DAT_SUCCESS )
	    {
	    LOG( MLUCLog::kError, "BCLUDAPLmsgCommunication::Send", "Error waiting for message send completion" )
		<< "Error waiting for completion of message send: "
		<< DatStrError(dret).c_str() << " ("
		<< MLUCLog::kDec << dret << ")." << ENDLOG;
	    fComHelper.DisconnectFromRemote( &cd, useTimeout, timeout_ms, true, false );
	    failure = true;
	    continue;
	    }
	
	if ( blobEvt.event_number != DAT_DTO_COMPLETION_EVENT ||
	     blobEvt.event_data.dto_completion_event_data.status != DAT_DTO_SUCCESS )
	    {
	    LOG( MLUCLog::kError, "BCLUDAPLmsgCommunication::Send", "Error on message transmission" )
		<< "Error transmitting message." << ENDLOG;
	    fComHelper.DisconnectFromRemote( &cd, useTimeout, timeout_ms, true, false );
	    failure = true;
	    continue;
	    }
	
	LOG( MLUCLog::kDebug, "BCLUDAPLmsgCommunication::Send", "Msg transmission completed successfully" )
	    << "Successfully completed transmission of message." << ENDLOG;
	
	fComHelper.DisconnectFromRemote( &cd, useTimeout, timeout_ms );
	}
    LOG( MLUCLog::kDebug, "UDAPL Send", "Sending" )
	<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": Finished sending to address" 
	<< *((BCLUDAPLAddressStruct*)address) << "" << ENDLOG;
    return 0;
    }

int BCLUDAPLMsgCommunication::Receive( BCLUDAPLAddressStruct* address, BCLMessageStruct*& msg, bool useTimeout, unsigned long timeout, BCLErrorCallback* callback )
    {
    msg =NULL;
    if ( !address )
	{
	LOG( MLUCLog::kError, "UDAPL Receive", "Address error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr 
	    << ": " << "Wrong address (NULL pointer) in UDAPL Receive" << ENDLOG;
	MsgReceiveError( EFAULT, this, address, msg, callback );
	return EFAULT;
	}
    uint32 st;
    BCLNetworkData::Transform( address->fLength, st, address->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );    
    if ( st != sizeof(BCLUDAPLAddressStruct) )
	{
	LOG( MLUCLog::kError, "UDAPL Receive", "Address error" )
	    << "Port 0x" << MLUCLog::kHex << fComHelper.fPortNr << " (" << MLUCLog::kDec << fComHelper.fPortNr << "): " 
	    << "Wrong address size " << MLUCLog::kDec << address->fLength 
	    << " (expected " << sizeof(BCLUDAPLAddressStruct) << ") in UDAPL Receive." << ENDLOG;
	AddressError( EMSGSIZE, this, address, callback );
	return EMSGSIZE;
	}
    if ( fComHelper.fSocket == -1 )
	{
	LOG( MLUCLog::kError, "UDAPL Receive", "Communication error" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr 
	    << ": " << "Helper object not ready." << ENDLOG;
	MsgReceiveError( ENXIO, this, address, msg, callback );
	return ENXIO;
	}
//     uint32 st;
//     BCLNetworkData::Transform( address->fLength, st, address->fDataFormats[kCurrentByteOrderIndex], kNativeByteOrder );    
//     if ( st != sizeof(BCLUDAPLAddressStruct) )
// 	{
// 	LOG( MLUCLog::kError, "UDAPL Receive", "Address error" )
// 	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " << "Wrong address size " << MLUCLog::kDec << address->fLength 
// 	    << " (expected " << sizeof(BCLUDAPLAddressStruct) << ") in UDAPL Receive." << ENDLOG;
// 	AddressError( EMSGSIZE, this, address, callback );
// 	return EMSGSIZE;
// 	}

    fDataArrivedSignal.Lock();
    bool success = true;
    do
	{
	if ( fMessages.GetCnt()<=0 )
	    {
	    if ( useTimeout )
		success = fDataArrivedSignal.Wait( timeout );
	    else
		fDataArrivedSignal.Wait();
	    }
	if ( fMessages.GetCnt()>0 )
	    {
	    *address = fMessages.GetFirstPtr()->fAddress;
	    msg = fMessages.GetFirstPtr()->fMsg;
	    fMessages.RemoveFirst();
	    fDataArrivedSignal.Unlock();
	    return 0;
	    }
	}
    while ( success && fComHelper.fSocket != -1 );
    fDataArrivedSignal.Unlock();
    if ( fComHelper.fSocket != -1 )
	return ETIMEDOUT;
    else
	return ENXIO;
    }

int BCLUDAPLBlobCommunication::DataReceived( DAT_EVD_HANDLE /*evd*/, 
	DAT_EP_HANDLE ep, 
	DAT_VADDR recvBuffer,
	DAT_VLEN recvBufferSize,
	DAT_LMR_CONTEXT recvLMRContext,
	DAT_LMR_TRIPLET* iov, 
	uint64 old_cookie, 
	DAT_EVENT& event )
    {
    LOG( MLUCLog::kInformational, "UDAPL Msg Receive", "New Data" )
	<< "New data on endpoint " << ep
	<< "." << ENDLOG;
    UDAPLMsgData msgData;
    msgData.fAddress = msgProgress->fAddress;
    msgData.fMsg = msgProgress->fMsg;


    fDataArrivedSignal.Lock();
    fMessages.Add( msgData );
    fDataArrivedSignal.Unlock();
    LOG( MLUCLog::kDebug, "UDAPL New Data Receive", "New Message Signal Sent" )
	<< "New message " << *(msgData.fMsg) << " received signal sent." << ENDLOG;
    fDataArrivedSignal.Signal();
    if ( resetTimeout )
	setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
    con.fTimeout.tv_sec = con.fTimeout.tv_usec = 0;
    }


bool BCLUDAPLMsgCommunication::NewConnection( BCLIntUDAPLComHelper::TAcceptedConnectionData& con )
    {
    LOG( MLUCLog::kInformational, "UDAPL Msg Receive", "New Connection" )
	<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << " new connection on socket " << con.fSocket
	<< "." << ENDLOG;
    return true;
    }

bool BCLUDAPLMsgCommunication::NewData( BCLIntUDAPLComHelper::TAcceptedConnectionData& con )
    {
    LOG( MLUCLog::kInformational, "UDAPL Msg Receive", "New Data" )
	<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << " new data on socket " << con.fSocket
	<< "." << ENDLOG;
    // Check reads with select and store state in fMsgProgress
    BCLUDAPLAddress ta;
    BCLNetworkData nd;
    //TUDAPLMsgProgressDataContType::iterator iter, end, msgProgress;
    UDAPLMsgProgressData* msgProgress = NULL;
    bool looped = false;
    bool found = false;
    unsigned long mpNdx;
    UDAPLMsgProgressData mpd;
    if ( MLUCVectorSearcher<UDAPLMsgProgressData,int>::FindElement( fMsgProgress, &FindMsgProgressBySocket, con.fSocket, mpNdx ) )
	{
	msgProgress = fMsgProgress.GetPtr( mpNdx );
	found = true;
	}
    if ( !found )
	{
	mpd.fSocket = con.fSocket;
	mpd.fBytesRead = 0;
	msgProgress = &mpd;
	/*
	fMsgProgress.Add( mpd, mpNdx );
	msgProgress = fMsgProgress.GetPtr( mpNdx );
	*/
	}
    int ret;
    struct timeval tv, oldRcvTimeout;
    bool resetTimeout = false;
    if ( fReceiveTimeout )
	{
	con.fTimeout.tv_sec = fReceiveTimeout/1000;
	con.fTimeout.tv_usec = (fReceiveTimeout-(con.fTimeout.tv_sec*1000))*1000;
	socklen_t sockoptlen = sizeof(oldRcvTimeout);
	ret = getsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, &sockoptlen );
	if ( ret==-1 )
	    {
	    ret = errno;
	    LOG( MLUCLog::kError, "UDAPL Blob New Data Receive", "Socket Timeout Get Error" )
		<< "Error getting UDAPL receive timeout from socket in UDAPL blob receive." << ENDLOG;
	    }
	else
	    {
	    struct timeval newTimeout;
	    newTimeout.tv_sec = fReceiveTimeout/1000;
	    newTimeout.tv_usec = (fReceiveTimeout-(newTimeout.tv_sec*1000))*1000;
	    ret = setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &newTimeout, sizeof(newTimeout) );
	    if ( ret==-1 )
		{
		ret = errno;
		LOG( MLUCLog::kError, "UDAPL Blob New Data Receive", "Socket Timeout Set Error" )
		    << "Error getting UDAPL receive timeout from socket in UDAPL blob receive." << ENDLOG;
		}
	    resetTimeout = true;
	    }
	}
    fd_set sockets;
    uint8* bp = (uint8*)&msgProgress->fAddress;
    memcpy( &msgProgress->fAddress, &con.fAddress, con.fAddress.fLength );
  
    //BCLMessageStruct msgs;
    BCLMessage msg;
    //BCLMessageStruct* msgsp;
    bp = (uint8*)&msgProgress->fMsgS;
    ret = 0;
    looped = false;
    while ( msgProgress->fBytesRead < sizeof(BCLNetworkDataStruct) )
	{
	ret = fComHelper.Read( con, bp+msgProgress->fBytesRead, 
		    sizeof(BCLNetworkDataStruct)-msgProgress->fBytesRead );
	if ( ret<0 && errno!=EAGAIN )
	    break;
	if ( ret==0 && looped )
	    break;
	looped = true;
	if ( ret > 0 )
	    msgProgress->fBytesRead += ret;
	if ( msgProgress->fBytesRead >= sizeof(BCLNetworkDataStruct) )
	    break;
	do
	    {
	    FD_ZERO( &sockets );
	    FD_SET( msgProgress->fSocket, &sockets );
	    tv.tv_sec = tv.tv_usec = 0;
	    errno = 0;
	    ret = select( msgProgress->fSocket+1, &sockets, NULL, NULL, &tv );
	    }
	while ( ret==0 && (errno==EINTR || errno==EAGAIN) );
	if ( ret!=1 )
	    {
	    if ( !found )
		fMsgProgress.Add( mpd );
	    if ( resetTimeout )
		setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	    return true;
	    }
	}
    if ( msgProgress->fBytesRead < sizeof(BCLNetworkDataStruct) && ret<=0 )
	{
	if ( ret < 0 )
	    {
	    LOG( MLUCLog::kError, "UDAPL New Data Receive", "Read Message Error" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		<< "Error reading message header from socket " << con.fSocket
		<< ": " << strerror(errno) << " (" << errno << ")." << ENDLOG;
	    }
	else
	    {
	    LOG( MLUCLog::kDebug, "UDAPL New Data Receive", "Connection closed" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		<< "Incoming connection closed while reading message header from socket " << con.fSocket
		<< "." << ENDLOG;
	    }
	// XXX
	if ( found )
	    fMsgProgress.Remove( mpNdx );
	if ( resetTimeout )
	    setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	return false;
	}
    if ( looped )
	{
	// Check the correct size (minimum) of the message
	nd.SetTo( (BCLNetworkDataStruct*)&msgProgress->fMsgS );
	if ( nd.GetData()->fLength < sizeof(BCLMessageStruct) )
	    {
	    LOG( MLUCLog::kError, "UDAPL New Data Receive", "Message Size Error" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		<< "Message read from socket " << con.fSocket << " has wrong size: " 
		<< nd.GetData()->fLength
		<< " (0x" << MLUCLog::kHex << nd.GetData()->fLength << ") (expected at least "
		<< MLUCLog::kDec << sizeof(BCLMessageStruct) << " (0x" << MLUCLog::kHex
		<< sizeof(BCLMessageStruct) << "))." << ENDLOG;
	    // XXX
	    // Read in rest of structure according to passed length??
	  
	    // XXX
	    if ( found )
		fMsgProgress.Remove( mpNdx );
	    if ( resetTimeout )
		setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	    return false;
	    }
	msgProgress->fMsgLen = nd.GetData()->fLength;
	msgProgress->fMsg = NewMessage( nd.GetData()->fLength );
	if ( !msgProgress->fMsg )
	    {
	    LOG( MLUCLog::kError, "UDAPL New Data Receive", "Out Of Memory" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		<< "Out of memory trying to allocate message of size "
		<< nd.GetData()->fLength << " (0x" << MLUCLog::kHex 
		<< nd.GetData()->fLength << ")." << ENDLOG;
	    // XXX
	    // try to read in remaining message to ensure no leftovers?
	  
	    // XXX
	    if ( found )
		fMsgProgress.Remove( mpNdx );
	    if ( resetTimeout )
		setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	    return false;
	    }
	memcpy( msgProgress->fMsg, &msgProgress->fMsgS, sizeof(BCLNetworkDataStruct) );
	}
    bp = (uint8*)msgProgress->fMsg;
    ret = 0;
    looped = false;
    while ( msgProgress->fBytesRead < msgProgress->fMsgLen )
	{
	ret = fComHelper.Read( con, bp+msgProgress->fBytesRead, 
		    msgProgress->fMsgLen-msgProgress->fBytesRead,
		    true );
	if ( ret <=0 && errno!=EAGAIN )
	    break;
	if ( ret==0 && looped )
	    break;
	looped = true;
	if ( ret > 0 )
	    msgProgress->fBytesRead += ret;
	if ( msgProgress->fBytesRead >= msgProgress->fMsgLen )
	    break;
	do
	    {
	    FD_ZERO( &sockets );
	    FD_SET( msgProgress->fSocket, &sockets );
	    tv.tv_sec = tv.tv_usec = 0;
	    errno = 0;
	    ret = select( msgProgress->fSocket+1, &sockets, NULL, NULL, &tv );
	    }
	while ( ret==0 && (errno==EINTR || errno==EAGAIN) );
	if ( ret!=1 )
	    {
	    if ( !found )
		fMsgProgress.Add( mpd );
	    if ( resetTimeout )
		setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	    return true;
	    }
	}
    if ( msgProgress->fBytesRead < msgProgress->fMsgLen && ret<=0 )
	{
	if ( ret < 0 )
	    {
	    LOG( MLUCLog::kError, "UDAPL New Data Receive", "Read Message Error" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		<< "Error reading message header from socket " << con.fSocket
		<< ": " << strerror(errno) << " (" << errno << ")." << ENDLOG;
	    }
	else
	    {
	    LOG( MLUCLog::kDebug, "UDAPL New Data Receive", "Connection closed" )
		<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << ": " 
		<< "Incoming connection closed while reading message header from socket " << con.fSocket
		<< "." << ENDLOG;
	    }
	// XXX
	if ( found )
	    fMsgProgress.Remove( mpNdx );
	if ( resetTimeout )
	    setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
	return false;
	}
    UDAPLMsgData msgData;
    msgData.fAddress = msgProgress->fAddress;
    msgData.fMsg = msgProgress->fMsg;
    if ( found )
	fMsgProgress.Remove( mpNdx );
    fDataArrivedSignal.Lock();
    fMessages.Add( msgData );
    fDataArrivedSignal.Unlock();
    LOG( MLUCLog::kDebug, "UDAPL New Data Receive", "New Message Signal Sent" )
	<< "New message " << *(msgData.fMsg) << " received signal sent." << ENDLOG;
    fDataArrivedSignal.Signal();
    if ( resetTimeout )
	setsockopt( con.fSocket, SOL_SOCKET, SO_RCVTIMEO, &oldRcvTimeout, sizeof(oldRcvTimeout) );
    con.fTimeout.tv_sec = con.fTimeout.tv_usec = 0;
    return true;
    }

bool BCLUDAPLMsgCommunication::ConnectionTimeout( BCLIntUDAPLComHelper::TAcceptedConnectionData& con )
    {
    LOG( MLUCLog::kInformational, "UDAPL Msg Receive", "Connection Timed Out" )
	<< "Port " << MLUCLog::kDec << fComHelper.fPortNr << " connection on socket " << con.fSocket
	<< " timed out." << ENDLOG;
    return false;
    }

void BCLUDAPLMsgCommunication::ConnectionClosed( BCLIntUDAPLComHelper::TAcceptedConnectionData& con )
    {
    // Check reads with select and store state in fMsgProgress
    unsigned long mpNdx;
    if ( MLUCVectorSearcher<UDAPLMsgProgressData,int>::FindElement( fMsgProgress, &FindMsgProgressBySocket, con.fSocket, mpNdx ) )
	{
	LOG( MLUCLog::kInformational, "UDAPL Msg Receive", "Connection Closed" )
	    << "Port " << MLUCLog::kDec << fComHelper.fPortNr << " connection on socket " << con.fSocket
	    << " closed." << ENDLOG;
	fMsgProgress.Remove( mpNdx );
	}
    }

bool BCLUDAPLMsgCommunication::FindMsgProgressBySocket( const UDAPLMsgProgressData& msgProgress, const int& refSocket )
    {
    return msgProgress.fSocket == refSocket;
    }


#endif // USE_UDAPL





/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: BCLUDAPLMsgCommunication.cpp 738 2005-11-16 07:51:25Z timm $ 
**
***************************************************************************
*/
