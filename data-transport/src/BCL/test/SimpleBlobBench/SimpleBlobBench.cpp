/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

//#include "BCLSCIMsgCommunication.hpp"
//#include "BCLSCIBlobCommunication.hpp"
//#include "BCLSCIAddress.hpp"
#include "BCLMsgCommunication.hpp"
#include "BCLBlobCommunication.hpp"
#include "BCLAbstrAddress.hpp"
#include "SimpleBlobMsg.hpp"
#include "MLUCLog.hpp"
#include "MLUCLogServer.hpp"
#include "BCLErrorLogCallback.hpp"
#include "BCLStackTraceCallback.hpp"
#include "BCLURLAddressDecoder.hpp"
#include "BCLAddressLogger.hpp"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <fstream>
#ifdef USE_SCI
#include "BCLSCIBlobCommunication.hpp"
#include "BCLSCIComID.hpp"
#endif
#include <cstdlib>


const uint32 kBlobBenchDataMsg = 1;
const uint32 kBlobBenchPreAllocDataMsg = 2;
const uint32 kBlobBenchQuitMsg = 3;

// #define BLOBSIZEMIN 1
// #define BLOBSIZEMAX 65535
// #define BLOBSIZEMIN 128
// #define BLOBSIZEMAX 256
#define DEFAULTBLOBSIZEMIN 128000
#define DEFAULTBLOBSIZEMAX 128000


#ifdef DEBUG
#undef DEBUG
#endif



// MLUCLog& LogAddress( BCLAbstrAddressStruct& addr )
//     {
//     switch ( addr.fComID )
// 	{
// 	case kSCIComID: 
// 	    return gLog << (BCLSCIAddressStruct&)addr;
// 	default: 
// 	    return gLog << "Unknown address type";
// 	}
//     }


class ExitOnErrorCallback: public BCLErrorCallback
    {
    public:
 
 	ExitOnErrorCallback()
 		{
 		fMsgCom = NULL;
 		fBlobCom = NULL;
 		fDoUnbind = false;
 		fDoDisconnect = false;
 		fDisconnectAddress = NULL;
 		fDoUnlock = false;
 		fUnlockAddress = NULL;
 		}

	virtual ~ExitOnErrorCallback() {};
 
 	void DoUnbind()
 		{
 		fDoUnbind = true;
 		}
 
 	void DoDisconnect( BCLAbstrAddressStruct* address )
 		{
 		fDoDisconnect = true;
 		fDisconnectAddress = address;
 		}
 
 	void DoUnlock( BCLAbstrAddressStruct* address )
 		{
 		fDoUnlock = true;
 		fUnlockAddress = address;
 		}
 	
 	void SetMsgCom( BCLMsgCommunication* com )
 		{
 		fMsgCom = com;
 		}
 
 	void SetBlobCom( BCLBlobCommunication* com )
 		{
 		fBlobCom = com;
 		}
 
 	void Exit( int eval )
 		{
 		if ( fBlobCom )
 		    {
 		    if ( fDoUnlock )
 			fBlobCom->UnlockConnection( fUnlockAddress );
 		    if ( fDoDisconnect )
 			fBlobCom->Disconnect( fDisconnectAddress );
 		    if ( fDoUnbind )
 			fBlobCom->Unbind();
 		    }
 		if ( fMsgCom )
 		    {
 		    if ( fDoUnlock )
 			fMsgCom->UnlockConnection( fUnlockAddress );
 		    if ( fDoDisconnect )
 			fMsgCom->Disconnect( fDisconnectAddress );
 		    if ( fDoUnbind )
 			fMsgCom->Unbind();
 		    }
 		exit( eval );
 		}
 
 	virtual BCLErrorAction ConnectionTemporarilyLost( int, BCLCommunication*,
							  BCLAbstrAddressStruct* ) { Exit(-1); return kAbort; };
 
 	virtual BCLErrorAction ConnectionError( int, BCLCommunication*,
						BCLAbstrAddressStruct* ) { Exit(-1); return kAbort; };
 
 	virtual BCLErrorAction BindError( int, BCLCommunication*,
					  BCLAbstrAddressStruct* ) { Exit(-1); return kAbort; };
 
 	virtual BCLErrorAction LockError( int, BCLCommunication*,
					  BCLAbstrAddressStruct* ) { Exit(-1); return kAbort; };
 
 	virtual BCLErrorAction MsgSendError( int, BCLCommunication*,
					     BCLAbstrAddressStruct*, 
 					     BCLMessageStruct* ) { Exit(-1); return kAbort; };
 
 	virtual BCLErrorAction MsgReceiveError( int, BCLCommunication*,
						BCLAbstrAddressStruct*, 
 						BCLMessageStruct* ) { Exit(-1); return kAbort; };
 
 	virtual BCLErrorAction BlobPrepareError( int, BCLCommunication*,
						 BCLAbstrAddressStruct* ) { Exit(-1); return kAbort; };
 
 	virtual BCLErrorAction BlobSendError( int, BCLCommunication*,
 					      BCLAbstrAddressStruct*, 
 					      BCLTransferID ) { Exit(-1); return kAbort; };
 
 	virtual BCLErrorAction BlobReceiveError( int, BCLCommunication*,
 						 BCLTransferID ) { Exit(-1); return kAbort; };
 
 	virtual BCLErrorAction AddressError( int, BCLCommunication*,
					     BCLAbstrAddressStruct* ) { Exit(-1); return kAbort; };
 
 	virtual BCLErrorAction GeneralError( int, BCLCommunication*,
					     BCLAbstrAddressStruct* ) { Exit(-1); return kAbort; };
 
    protected:
 	
 	BCLMsgCommunication* fMsgCom;
 	BCLBlobCommunication* fBlobCom;
 	bool fDoUnbind;
 	bool fDoDisconnect;
 	BCLAbstrAddressStruct* fDisconnectAddress;
 	bool fDoUnlock;
 	BCLAbstrAddressStruct* fUnlockAddress;
 
    };
 


int main( int argc, char** argv )
    {
    uint32 i, j;
    const char* errorStr = NULL;
    const char* usage1 = "Usage: ";
    const char* usage2 = " [ [ -serverblobmsg server-blobmsg-address-URL ] [ -serverblob blobsize server-blob-address-URL ] [ -servermsg server-msg-address-URL ] (-delay delaytime_in_us) | [-clientblobmsg client-blobmsg-address-URL server-blobmsg-address-URL ] [ -clientblob blobsize client-blob-addres-URL ] [ -clientmsg client-msg-address-URL server-msg-address-URL ] (-count msgcount) (-minsize <min-block-size>) (-maxsize <max-block-size>) (-mincnt <min-block-count>) (-maxcnt <max-block-count>) (-connect) (-lock) (-nowait)] (-nomsg) (-V verbosity_levels) (-file filename) (-nodma) (-exitonerror) (-prealloc)";
    char* cpErr;
    MLUCFilteredStdoutLogServer sOut;
    gLogLevel = MLUCLog::kAll;
    gLog.AddServer( sOut );
    BCLErrorLogCallback logCallback;
    BCLStackTraceCallback traceCallback( 7 );
    gLogLevel = MLUCLog::kWarning|MLUCLog::kError|MLUCLog::kFatal;
    BCLAddressLogger::Init();
    ExitOnErrorCallback msgExitCallback;
    ExitOnErrorCallback blobExitCallback;

    BCLMsgCommunication* blobMsgCom = NULL;
    BCLMsgCommunication* msgCom = NULL;
    BCLBlobCommunication* blobCom = NULL;
    BCLAbstrAddressStruct *ownBlobMsgAddress=NULL, 
			     *ownBlobAddress=NULL, 
			      *ownMsgAddress=NULL,
		       *remoteBlobMsgAddress=NULL,
			   *remoteMsgAddress=NULL;
    uint32 msgCount=1, msgNr;
    bool server = false, client = false;
    bool connect = false;
    bool do_lock = false;
    bool noWait=false;
    bool noMsg = false;
    uint64 blobBufferSize = 0;
    uint64 blobSizeMin = DEFAULTBLOBSIZEMIN;
    uint64 blobSizeMax = DEFAULTBLOBSIZEMAX;
    uint32 blobCntMin = 1;
    uint32 blobCntMax = 1;
    bool nodma = false;
    bool prealloc = false;
    bool exitonerror = false;
    unsigned long delayTime = 0;
    i = 1;
    while ( (i < (uint32)argc) && !errorStr )
	{
	if ( !strcmp( argv[i], "-serverblobmsg" ) )
	    {
	    server = true;
	    if ( (uint32)argc <= i +1 )
		{
		errorStr = "Missing server blob msg address options.";
		break;
		}
	    bool isBlob;
	    int ret;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+1], com_tmp, ownBlobMsgAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid server blobmsg address URL";
		break;
		}
	    if ( isBlob )
		{
		errorStr = "No blob addresses allowed in server blobmsg URL";
		break;
		}
	    //BCLDecodeRemoteURL( argv[i+1], remoteBlobMsgAddress );
	    BCLGetEmptyAddressFromURL( argv[i+1], remoteBlobMsgAddress );
	    blobMsgCom = (BCLMsgCommunication*)com_tmp;
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-serverblob" ) )
	    {
	    server = true;
	    if ( (uint32)argc <= i +2 )
		{
		errorStr = "Missing server blob options.";
		break;
		}
	    blobBufferSize = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting server blob buffersize argument.";
		break;
		}
	    bool isBlob;
	    int ret;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+2], com_tmp, ownBlobAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid server blob address URL";
		break;
		}
	    if ( !isBlob )
		{
		errorStr = "No msg addresses allowed in server blob URL";
		break;
		}
	    blobCom = (BCLBlobCommunication*)com_tmp;
   	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-servermsg" ) )
	    {
	    server = true;
	    if ( (uint32)argc <= i +1 )
		{
		errorStr = "Missing server msg address options.";
		break;
		}
	    bool isBlob;
	    int ret;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+1], com_tmp, ownMsgAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid server msg address URL";
		break;
		}
	    if ( isBlob )
		{
		errorStr = "No blob addresses allowed in server msg URL";
		break;
		}
	    //BCLDecodeRemoteURL( argv[i+1], remoteMsgAddress );
	    BCLGetEmptyAddressFromURL( argv[i+1], remoteMsgAddress );
	    msgCom = (BCLMsgCommunication*)com_tmp;
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-clientblob" ) )
	    {
	    client = true;
	    if ( (uint32)argc <= i +2 )
		{
		errorStr = "Missing client blob address options.";
		break;
		}
	    blobBufferSize = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting client blob buffersize argument.";
		break;
		}
	    bool isBlob;
	    int ret;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+2], com_tmp, ownBlobAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid client blob address URL";
		break;
		}
	    if ( !isBlob )
		{
		errorStr = "No msg addresses allowed in client blob URL";
		break;
		}
	    blobCom = (BCLBlobCommunication*)com_tmp;
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-clientblobmsg" ) )
	    {
	    client = true;
	    if ( (uint32)argc <= i + 2 )
		{
		errorStr = "Missing client blob msg address options.";
		break;
		}
	    bool isBlob;
	    int ret;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+1], com_tmp, ownBlobMsgAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid client blob msg address URL";
		break;
		}
	    if ( isBlob )
		{
		errorStr = "No blob addresses allowed in client blob msg address URL";
		break;
		}
	    ret = BCLDecodeRemoteURL( argv[i+2], remoteBlobMsgAddress );
	    if ( ret )
		{
		errorStr = "Invalid server blob msg address URL";
		break;
		}
	    blobMsgCom = (BCLMsgCommunication*)com_tmp;
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-clientmsg" ) )
	    {
	    client = true;
	    if ( (uint32)argc <= i +2 )
		{
		errorStr = "Missing client msg address options.";
		break;
		}
	    bool isBlob;
	    int ret;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+1], com_tmp, ownMsgAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid client msg address URL";
		break;
		}
	    if ( isBlob )
		{
		errorStr = "No blob addresses allowed in client msg address URL";
		break;
		}
	    ret = BCLDecodeRemoteURL( argv[i+2], remoteMsgAddress );
	    if ( ret )
		{
		errorStr = "Invalid server msg address URL";
		break;
		}
	    msgCom = (BCLMsgCommunication*)com_tmp;
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-connect" ) )
	    {
	    connect = true;
	    i++;
	    continue;
	    }
	if ( !strcmp( argv[i], "-count" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing count specifier.";
		break;
		}
	    msgCount = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-minsize" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing minimum block size specifier.";
		break;
		}
	    blobSizeMin = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting minimum block size specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-maxsize" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing maximum block size specifier.";
		break;
		}
	    blobSizeMax = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting maximum block size specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-mincnt" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing minimum block count specifier.";
		break;
		}
	    blobCntMin = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting minimum block count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-maxcnt" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing maximum block count specifier.";
		break;
		}
	    blobCntMax = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting maximum block count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
 	if ( !strcmp( argv[i], "-nodma" ) )
 	    {
	    nodma = true;
 	    i += 1;
 	    continue;
 	    }
 	if ( !strcmp( argv[i], "-prealloc" ) )
 	    {
	    prealloc = true;
 	    i += 1;
 	    continue;
 	    }
	if ( !strcmp( argv[i], "-nomsg" ) )
	    {
	    noMsg = true;
	    i++;
	    continue;
	    }
	if ( !strcmp( argv[i], "-nowait" ) )
	    {
	    noWait = true;
	    i++;
	    continue;
	    }
 	if ( !strcmp( argv[i], "-exitonerror" ) )
 	    {
 	    exitonerror = true;
 	    i++;
 	    continue;
 	    }
	if ( !strcmp( argv[i], "-delay" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing delaytime specifier.";
		break;
		}
	    delayTime = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting delaytime specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing verbosity level specifier.";
		break;
		}
	    gLogLevel = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting verbosity level specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-lock" ) )
	    {
	    do_lock = true;
	    i++;
	    continue;
	    }
	errorStr = "Unknown option";
	}
    if ( client==server && !errorStr )
	errorStr = "Must specify either one server or client option.";
    if ( server )
	{
	if ( (!blobCom || !msgCom || !blobMsgCom) && !errorStr )
	    errorStr = "Must specify one each of the server blob, blobmsg and msg parameters";
	}
    else
	{
	if ( (!blobCom || !msgCom || !blobMsgCom) && !errorStr )
	    errorStr = "Must specify one each of the client blob, blobmsg and msg parameters";
	}
    if ( server && connect  && !errorStr)
	errorStr = "'-connect' argument only valid for clients.";
    if ( server && msgCount!= 1  && !errorStr)
	errorStr = "'-count' argument only valid for clients.";
    if ( client && delayTime!=0  && !errorStr)
	errorStr = "'-delay' argument only valid for server";
    if ( errorStr )
	{
	LOG( MLUCLog::kError, "SimpleBlobBench", "Command line options" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	MLUCString urls;
	vector<MLUCString> msgurls, bloburls;
	BCLGetAllowedURLs( msgurls, bloburls );
	vector<MLUCString>::iterator iter, end;
	iter = msgurls.begin();
	end = msgurls.end();
	while ( iter != end )
	    {
	    urls += *iter;
	    if ( iter+1 != end )
		urls += ", ";
	    iter++;
	    }

	iter = bloburls.begin();
	end = bloburls.end();
	if ( iter != end )
	    urls += ", ";
	while ( iter != end )
	    {
	    urls += *iter;
	    if ( iter+1 != end )
		urls += ", ";
	    iter++;
	    }
	LOG( MLUCLog::kError, "SimpleBlobBench", "Command line options" )
	    << "Allowed address URLS: " << urls.c_str() << "." << ENDLOG;
	LOG( MLUCLog::kError, "SimpleBlobBench", "Command line options" )
	    << errorStr << ENDLOG;
	return -1;
	}

    blobCom->SetMsgCommunication( blobMsgCom );

    ofstream of;
    MLUCString name;
    name = "SimpleBlobBench-";
    if ( server )
	name += "Server";
    else
	name += "Client";
    name += ".log";
    of.open( name.c_str() );
    MLUCStreamLogServer streamOut( of );
    gLog.AddServer( streamOut );

    
    msgCom->AddCallback( &logCallback );
    blobMsgCom->AddCallback( &logCallback );
    blobCom->AddCallback( &logCallback );
    msgCom->AddCallback( &traceCallback );
    blobMsgCom->AddCallback( &traceCallback );
    blobCom->AddCallback( &traceCallback );
    if ( exitonerror )
 	msgCom->AddCallback( &msgExitCallback );
    if ( exitonerror )
 	blobCom->AddCallback( &blobExitCallback );
    blobMsgCom->AddCallback( &blobExitCallback );
 
    msgExitCallback.SetMsgCom( msgCom );
    blobExitCallback.SetBlobCom( blobCom );
    blobExitCallback.SetMsgCom( blobMsgCom );

    blobCom->SetBlobBuffer( blobBufferSize );

    if ( blobCom->GetBlobBuffer() )
 	memset( blobCom->GetBlobBuffer(), 0, blobCom->GetBlobBufferSize() );

#ifdef DEBUG_SCIADDRESSES
	{
	BCLSCIAddress testSCIAddr;
	BCLSCIAddressStruct* testSCIAddrS = testSCIAddr.GetData();
	LOG( MLUCLog::kDebug, "SimpleBlobBench", "struct size debug" )
	    << "BCLSCIAddressStruct->fLength: " << MLUCLog::kDec
	    << testSCIAddr.GetData()->fLength << " - sizeof(BCLSCIAddressStruct): "
	    << sizeof(BCLSCIAddressStruct) << "..." << ENDLOG;
	LOG( MLUCLog::kDebug, "SimpleBlobBench", "struct size debug" )
	    << "&BCLSCIAddressStruct->fDataFormats[0]: " << MLUCLog::kDec
	    << (uint32)( &(testSCIAddrS->fDataFormats[0]) ) - (uint32)( &(testSCIAddrS->fDataFormats[0]) ) << " - "
	    << "&BCLSCIAddressStruct->fLength: " << MLUCLog::kDec
	    << (uint32)( &(testSCIAddrS->fLength) ) - (uint32)( &(testSCIAddrS->fDataFormats[0]) ) << " - "
	    << "&BCLSCIAddressStruct->fComID: " << MLUCLog::kDec
	    << (uint32)( &(testSCIAddrS->fComID) ) - (uint32)( &(testSCIAddrS->fDataFormats[0]) ) << " - "
	    << "&BCLSCIAddressStruct->fNodeID: " << MLUCLog::kDec
	    << (uint32)( &(testSCIAddrS->fNodeID) ) - (uint32)( &(testSCIAddrS->fDataFormats[0]) ) << " - "
	    << "&BCLSCIAddressStruct->fAdapterNr: " << MLUCLog::kDec
	    << (uint32)( &(testSCIAddrS->fAdapterNr) ) - (uint32)( &(testSCIAddrS->fDataFormats[0]) ) << " - "
	    << "&BCLSCIAddressStruct->fSegmentID: " << MLUCLog::kDec
	    << (uint32)( &(testSCIAddrS->fSegmentID) ) - (uint32)( &(testSCIAddrS->fDataFormats[0]) ) << ENDLOG;

	}
#endif

	if ( server )
	    {
	    // ------------------------------------------------------
	    // Server code
	    LOGG( MLUCLog::kInformational, "SimpleBlobBench-Server", "Listening", tmpLog )
		<< "Server @ " << "Blob Msg Address: " ;
		BCLAddressLogger::LogAddress( tmpLog, *ownBlobMsgAddress ) << " " ;
		//gLog << *(BCLSCIAddressStruct*)ownBlobMsgAddress ;
		tmpLog << "Blob Address: " ;
		BCLAddressLogger::LogAddress( tmpLog, *ownBlobAddress ) << " " ;
		//gLog << *(BCLSCIAddressStruct*)ownBlobAddress ;
		tmpLog << "Msg Address: " ;
		BCLAddressLogger::LogAddress( tmpLog, *ownMsgAddress) << " listening" << ENDLOG; ;
		//gLog << *(BCLSCIAddressStruct*)ownMsgAddress << ENDLOG;
	    blobMsgCom->Bind( ownBlobMsgAddress );
#ifndef _SCALI_SIMPLE_BUGFIX1_
	    LOGG( MLUCLog::kDebug, "SimpleBlobBench-Server", "Blob Msg com addr", tmpLog )
		<< "Blob message communication listening at ";
		BCLAddressLogger::LogAddress( tmpLog, *blobMsgCom->GetAddress() )
		<< "." << ENDLOG;
#endif
	    blobCom->Bind( ownBlobAddress );
#ifndef _SCALI_SIMPLE_BUGFIX1_
 	    LOGG( MLUCLog::kDebug, "SimpleBlobBench-Server", "Blob com addr", tmpLog )
 		<< "Blob communication listening at ";
		BCLAddressLogger::LogAddress( tmpLog, *blobCom->GetAddress() )
		<< "." << ENDLOG;
#endif
	    msgCom->Bind( ownMsgAddress );
#ifndef _SCALI_SIMPLE_BUGFIX1_
	    LOGG( MLUCLog::kDebug, "SimpleBlobBench-Server", "Msg com addr", tmpLog )
		<< "Message communication listening at " ;
		BCLAddressLogger::LogAddress( tmpLog, *msgCom->GetAddress() )
		<< "." << ENDLOG;
#endif
	    msgNr = 0;

	    SimpleBlobMsgStruct* msg=NULL;
	    SimpleBlobMsg msgClass;
	    BCLMessageStruct* bmsg = NULL;
	    BCLTransferID transferID;
	    uint8* data;
	    uint64 dataSize;
	    uint64 dataOffset = ~(uint64)0;
	    uint64 tSize = 0;
	    bool quit = false;
	    *remoteMsgAddress = *ownMsgAddress;
	    *remoteBlobMsgAddress = *ownBlobMsgAddress;
	    if ( delayTime )
		usleep( delayTime );
	    if ( !noMsg )
		{
		do
		    {
		    msg = NULL;
		    msgCom->Receive( remoteMsgAddress, bmsg );
		    msg = (SimpleBlobMsgStruct*)bmsg;
		    if ( !msg )
			break;
		    LOG( MLUCLog::kInformational, "SimpleBlobBench-Server", "Message receive" )
			<< "Server received message " << *msg << " from " << *remoteMsgAddress << ENDLOG;
		    if ( msg->fMsgType == kBlobBenchQuitMsg )
			quit = true;
		    if ( msg->fMsgType == kBlobBenchDataMsg || msg->fMsgType == kBlobBenchPreAllocDataMsg )
			{
			transferID = msg->fTransfer;
			dataOffset = msg->fBlobOffset;
			dataSize = msg->fBlobSize;
			data = blobCom->GetBlobData( transferID );
			LOG( MLUCLog::kInformational, "SimpleBlobBench-Server", "Blob receive" )
			    << "Server received blob 0x" << MLUCLog::kHex
			    << transferID << " of size 0x" << dataSize
			    << " (" << MLUCLog::kDec << dataSize << ") at offset 0x"
			    << MLUCLog::kHex << dataOffset << " (" << MLUCLog::kDec
			    << dataOffset << ")- data index " 
			    << tSize << " (0x" << MLUCLog::kHex << tSize << ")." << ENDLOG;
			if ( !data )
			    {
			    LOG( MLUCLog::kError, "SimpleBlobBench-Server", "Blob receive" )
				<< "Error getting blob data for transfer " << MLUCLog::kHex << transferID 
				<< "." << ENDLOG;
			    continue;
			    }
			tSize += dataSize;
			if ( msg->fMsgType == kBlobBenchDataMsg )
			    blobCom->ReleaseBlob( transferID );
			}
		    }
		while ( !quit );
		}
	    else
		{
	    vector<uint64> offsets;
	    vector<uint64> sizes;
	    blobCom->EnableReceiveNotification( true );
	    bool preAlloc = true;
	    BCLTransferID oldTransferID =~(BCLTransferID)0;
	    int ret;
	    do
		{
		ret = blobCom->WaitForReceivedTransfer( transferID, offsets, sizes, 1000 );
		if ( ret == ETIMEDOUT )
		    {
		    msg = NULL;
		    msgCom->Receive( remoteMsgAddress, bmsg, 100 );
		    msg = (SimpleBlobMsgStruct*)bmsg;
		    if ( msg )
			{
			LOG( MLUCLog::kInformational, "SimpleBlobBench-Server", "Message receive" )
			    << "Server received message " << *msg << " from " << *remoteMsgAddress << ENDLOG;
			if ( msg->fMsgType == kBlobBenchQuitMsg )
			    quit = true;
			}
		    }
		else if ( ret )
		    {
		    LOG( MLUCLog::kError, "SimpleBlobBench-Server", "Blob receive" )
			<< "Error receiving blob data." << ENDLOG;
		    }
		else
		    {
		    if ( preAlloc && oldTransferID != ~(BCLTransferID)0 && oldTransferID != transferID )
			{
			preAlloc = false;
			blobCom->ReleaseBlob( oldTransferID );
			}
		    vector<uint64>::iterator offsetSizeIter, offsetSizeEnd;
		    offsetSizeIter = sizes.begin();
		    offsetSizeEnd = sizes.end();
		    dataSize = 0;
		    while ( offsetSizeIter != offsetSizeEnd )
			{
			dataSize += *offsetSizeIter;
			offsetSizeIter++;
			}
		    data = blobCom->GetBlobData( transferID );
		    LOG( MLUCLog::kInformational, "SimpleBlobBench-Server", "Blob receive" )
			<< "Server received blob 0x" << MLUCLog::kHex
			<< transferID << " of size 0x" << dataSize
			<< " (" << MLUCLog::kDec << dataSize << ") at offset 0x"
			<< MLUCLog::kHex << dataOffset << " (" << MLUCLog::kDec
			<< dataOffset << ")- data index " 
			<< tSize << " (0x" << MLUCLog::kHex << tSize << ")." << ENDLOG;
		    if ( !data )
			{
			LOG( MLUCLog::kError, "SimpleBlobBench-Server", "Blob receive" )
			    << "Error getting blob data for transfer " << MLUCLog::kHex << transferID 
			    << "." << ENDLOG;
			continue;
			}
		    tSize += dataSize;
		    if ( !preAlloc )
			blobCom->ReleaseBlob( transferID );
		    }
		}
	    while ( !quit );
		}
	    LOG( MLUCLog::kDebug, "SimpleBlobBench-Server", "Loop quitted" )
		<< "Receiver loop quitted..." << ENDLOG;
	    blobCom->Unbind();
	    LOG( MLUCLog::kDebug, "SimpleBlobBench-Server", "Blob com unbound" )
		<< "Blob com object unbound..." << ENDLOG;
	    blobMsgCom->Unbind();
	    LOG( MLUCLog::kDebug, "SimpleBlobBench-Server", "Blob msg com unbound" )
		<< "Blob msg com object unbound..." << ENDLOG;
	    msgCom->Unbind();
	    LOG( MLUCLog::kDebug, "SimpleBlobBench-Server", "Msg com unbound" )
		<< "Msg com object unbound..." << ENDLOG;
	    LOG( MLUCLog::kBenchmark, "SimpleBlobBench-Server", "Bytes read" )
		<< MLUCLog::kDec << tSize << " (0x" << MLUCLog::kHex << tSize
		<< ") bytes read." << ENDLOG;
	    }
	else
	    {
	    // Client Code
#ifdef USE_SCI
	    if ( nodma && blobCom->GetComID() == kSCIComID )
		((BCLSCIBlobCommunication*)blobCom)->AllowDMA( false );
#endif
	    uint64 tSize = 0;
	    uint64 dtime = 0;
	    uint8* data = new uint8[ blobSizeMax*blobCntMax ];
	    if ( !data )
		{
		LOG( MLUCLog::kError, "SimpleBlobBench-Client", "Blob data" )
		    << "Server out of memory allocating blob buffer of 0x"
		    << MLUCLog::kHex << blobSizeMax << " (" << MLUCLog::kDec
		    << blobSizeMax << ")." << ENDLOG;
		return -1;
		}
	    LOGG( MLUCLog::kInformational, "SimpleBlobBench-Client", "Listening", tmpLog )
		<< "Client @ " ;
		BCLAddressLogger::LogAddress( tmpLog, *ownBlobMsgAddress ) << "/";
		BCLAddressLogger::LogAddress( tmpLog, *ownBlobAddress ) << "/";
		BCLAddressLogger::LogAddress( tmpLog, *ownMsgAddress) << " listening..." << ENDLOG;
	    blobMsgCom->Bind( ownBlobMsgAddress );
	    //  	    LOG( MLUCLog::kDebug, "SimpleBlobBench-Server", "Blob Msg com addr" )
	    //  		<< "Blob message communication listening at " 
	    //  		<< *(BCLSCIAddressStruct*)blobMsgCom->GetAddress()
	    //  		<< "." << ENDLOG;
	    blobCom->Bind( ownBlobAddress );
	    //  	    LOG( MLUCLog::kDebug, "SimpleBlobBench-Server", "Blob com addr" )
	    //  		<< "Blob message communication listening at " 
	    //  		<< *(BCLSCIAddressStruct*)blobCom->GetAddress()
	    //  		<< "." << ENDLOG;
	    msgCom->Bind( ownMsgAddress );
	    //  	    LOG( MLUCLog::kDebug, "SimpleBlobBench-Server", "Msg com addr" )
	    //  		<< "Blob message communication listening at " 
	    //  		<< *(BCLSCIAddressStruct*)msgCom->GetAddress()
	    //  		<< "." << ENDLOG;

	    if ( connect )
		{
		LOGG( MLUCLog::kInformational, "SimpleBlobBench-Client", "Connect", tmpLog )
		    << "Client @ " ;
		    BCLAddressLogger::LogAddress( tmpLog, *ownBlobMsgAddress ) << "/";
		    BCLAddressLogger::LogAddress( tmpLog, *ownBlobAddress ) << "/";
		    BCLAddressLogger::LogAddress( tmpLog, *ownMsgAddress) << " connecting to " ;
		    BCLAddressLogger::LogAddress( tmpLog, *remoteBlobMsgAddress ) << "/";
		    BCLAddressLogger::LogAddress( tmpLog, *remoteMsgAddress ) << ENDLOG;
		msgCom->Connect( remoteMsgAddress );
		if ( do_lock )
		    msgCom->LockConnection( remoteMsgAddress );
		usleep( 250000 );
		blobCom->Connect( remoteBlobMsgAddress );
		if ( do_lock )
		    blobCom->LockConnection( remoteBlobMsgAddress );
		usleep( 250000 );
		}
	    uint64 remoteBufferSize=0;
	    BCLTransferID transferID=(BCLTransferID)-1;
	    uint64 offset=0;
	    remoteBufferSize = blobCom->GetRemoteBlobSize( remoteBlobMsgAddress );
	    if ( prealloc )
		{
		transferID = blobCom->PrepareBlobTransfer( remoteBlobMsgAddress, remoteBufferSize );
		if ( transferID == (BCLTransferID)-1 )
		    {
		    LOG( MLUCLog::kError, "SimpleBlobBench", "Transfer preparation" )
			<< "Failed to get transfer id for full buffer of 0x" << MLUCLog::kHex << remoteBufferSize
			<< " (" << MLUCLog::kDec << remoteBufferSize << ") bytes" << ENDLOG;
		    prealloc = false;
		    }
		else
		    {
		    LOG( MLUCLog::kInformational, "SimpleBlobBench-Client", "Buffer pre-allocated" )
			<< "Buffer of " << MLUCLog::kDec << remoteBufferSize << " (0x" << MLUCLog::kHex
			<< remoteBufferSize << ") pre-allocated." << ENDLOG;
		    }
		}
	    uint64 blobSize;
	    struct timeval start, end;
	    uint64 totalBlobSize;
	    double blobSizeTmp;
	    uint32 blobCnt;
	    double blobCntTmp;
	    SimpleBlobMsgStruct* msg=NULL;
	    SimpleBlobMsg msgClass;
	    msg = msgClass.GetData();
	    if ( !prealloc )
		msg->fMsgType = kBlobBenchDataMsg;
	    else
		msg->fMsgType = kBlobBenchPreAllocDataMsg;
	    vector<uint8*> datas;
	    vector<uint64> sizes;
	    vector<uint64> offsets;
	    srandom( time(NULL) );
	    usleep( 500000 );
	    gettimeofday( &start, NULL );
	    for ( i = 0; i < msgCount; i++ )
		{
		blobCntTmp = (blobCntMax-blobCntMin);
		blobCntTmp *= random();
		blobCntTmp /= (double)RAND_MAX;
		blobCnt = blobCntMin+(uint32)blobCntTmp;
		if ( blobCnt > blobCntMax )
		    blobCnt = blobCntMax;
		totalBlobSize = 0;
		datas.clear();
		sizes.clear();
		for ( j = 0; j < blobCnt; j++ )
		    {
		    blobSizeTmp = (blobSizeMax-blobSizeMin);
		    blobSizeTmp *= random();
		    blobSizeTmp /= (double)RAND_MAX;
		    blobSize = blobSizeMin+(uint64)blobSizeTmp;
		    if ( blobSize > blobSizeMax )
			blobSize = blobSizeMax;
		    datas.insert( datas.end(), data+totalBlobSize );
		    sizes.insert( sizes.end(), blobSize );
		    totalBlobSize += blobSize;
		    }
		LOG( MLUCLog::kInformational, "SimpleBlobBench-Client", "Transferring blob" )
		    << "Transferring blob with " << MLUCLog::kDec << blobCnt << " blocks of total size " << MLUCLog::kDec << totalBlobSize
		    << " (0x" << MLUCLog::kHex << totalBlobSize << ") - data index " << MLUCLog::kDec
		    << tSize << " (0x" << MLUCLog::kHex << tSize << ")." << ENDLOG;
		LOGG( MLUCLog::kInformational, "SimpleBlobBench-Client", "Transferring blob", tmpLog )
		    << "Client @ " ;
		    BCLAddressLogger::LogAddress( tmpLog, *ownBlobMsgAddress ) << "/";
		    BCLAddressLogger::LogAddress( tmpLog, *ownBlobAddress ) << "/";
		    BCLAddressLogger::LogAddress( tmpLog, *ownMsgAddress) << " preparing blob transfer." 
									<< ENDLOG;
		if ( !prealloc )
		    {
		    offset = 0;
		    transferID = blobCom->PrepareBlobTransfer( remoteBlobMsgAddress, blobSize  );
		    if ( transferID == (BCLTransferID)-1 )
			{
			LOG( MLUCLog::kError, "SimpleBlobBench", "Transfer preparation" )
			    << "Failed to get transfer id for blob of 0x" << MLUCLog::kHex << blobSize
			    << " (" << MLUCLog::kDec << blobSize << ") bytes" << ENDLOG;
			continue;
			}
		    }
		LOGG( MLUCLog::kInformational, "SimpleBlobBench-Client", "Transferring blob", tmpLog )
		    << "Client @ " ;
		    BCLAddressLogger::LogAddress( tmpLog, *ownBlobMsgAddress ) << "/";
		    BCLAddressLogger::LogAddress( tmpLog, *ownBlobAddress ) << "/";
		    BCLAddressLogger::LogAddress( tmpLog, *ownMsgAddress) << " transfering blob to " ;
		    BCLAddressLogger::LogAddress( tmpLog, *remoteBlobMsgAddress ) << "/";
		    BCLAddressLogger::LogAddress( tmpLog, *remoteMsgAddress ) << ENDLOG;
		if ( blobSize > remoteBufferSize )
		    {
		    LOG( MLUCLog::kInformational, "SimpleBlobBench-Client", "Sending message" )
			<< "Blob of " << MLUCLog::kDec << blobSize << " (0x" << MLUCLog::kHex
			<< blobSize << ") too large for buffer of " << MLUCLog::kDec
			<< remoteBufferSize << " (0x" << MLUCLog::kHex << remoteBufferSize
			<< ") bytes." << ENDLOG;
		    continue;
		    }
		if ( !noWait )
		    {
		    blobCom->TransferBlob( remoteBlobMsgAddress, transferID, datas, offsets, sizes );
		    }
		else
		    {
		    blobCom->PushBlob( remoteBlobMsgAddress, transferID, datas, offsets, sizes );
		    }
		tSize += totalBlobSize;
		if ( !noMsg )
		    {
		    msg->fTransfer = transferID;
		    msg->fBlobSize = blobSize;
		    msg->fBlobOffset = offset;
		    LOGG( MLUCLog::kInformational, "SimpleBlobBench-Client", "Sending message", tmpLog )
			<< "Client @ " ;
			BCLAddressLogger::LogAddress( tmpLog, *ownBlobMsgAddress ) << "/";
			BCLAddressLogger::LogAddress( tmpLog, *ownBlobAddress ) << "/";
			BCLAddressLogger::LogAddress( tmpLog, *ownMsgAddress) << " sending message to " ;
			BCLAddressLogger::LogAddress( tmpLog, *remoteBlobMsgAddress ) << "/";
			BCLAddressLogger::LogAddress( tmpLog, *remoteMsgAddress ) << ENDLOG;
		    msgCom->Send( remoteMsgAddress, msg );
		    LOG( MLUCLog::kInformational, "SimpleBlobBench-Client", "Message sent" )
			<< "Message sent." << ENDLOG;
		    }
		if ( prealloc )
		    {
		    offset += blobSize;
		    if ( offset >= remoteBufferSize )
			offset = 0;
		    }
		}
	    gettimeofday( &end, NULL );
	    //usleep( 10000 );
	    msg->fMsgType = kBlobBenchQuitMsg;
	    LOG( MLUCLog::kInformational, "SimpleBlobBench-Client", "Sending quit message" )
		<< "Sending quit message to server." << ENDLOG;
	    msgCom->Send( remoteMsgAddress, msg );
	    LOG( MLUCLog::kInformational, "SimpleBlobBench-Client", "Quit message sent" )
		<< "Quit message sent..." << ENDLOG;

	    if ( connect )
		{
		LOGG( MLUCLog::kInformational, "SimpleBlobBench-Client", "Disconnect", tmpLog )
		    << "Client @ " ;
		    BCLAddressLogger::LogAddress( tmpLog, *ownBlobMsgAddress ) << "/";
		    BCLAddressLogger::LogAddress( tmpLog, *ownBlobAddress ) << "/";
		    BCLAddressLogger::LogAddress( tmpLog, *ownMsgAddress) << " disconnecting from " ;
		    BCLAddressLogger::LogAddress( tmpLog, *remoteBlobMsgAddress ) << "/";
		    BCLAddressLogger::LogAddress( tmpLog, *remoteMsgAddress ) << ENDLOG;
		if ( do_lock )
		    blobCom->UnlockConnection( remoteBlobMsgAddress );
		blobCom->Disconnect( remoteBlobMsgAddress );
		if ( do_lock )
		    msgCom->UnlockConnection( remoteMsgAddress );
		msgCom->Disconnect( remoteMsgAddress );
		}

	    blobCom->Unbind();
	    LOG( MLUCLog::kDebug, "SimpleBlobBench-Client", "Blob com unbound" )
		<< "Blob com object unbound..." << ENDLOG;
	    blobMsgCom->Unbind();
	    LOG( MLUCLog::kDebug, "SimpleBlobBench-Client", "Blob msg com unbound" )
		<< "Blob msg com object unbound..." << ENDLOG;
	    msgCom->Unbind();
	    LOG( MLUCLog::kDebug, "SimpleBlobBench-Client", "Msg com unbound" )
		<< "Msg com object unbound..." << ENDLOG;
	    delete [] data;
	    LOG( MLUCLog::kDebug, "SimpleBlobBench-Client", "Data deleted" )
		<< "Data buffer deleted..." << ENDLOG;
	    LOG( MLUCLog::kDebug, "SimpleBlobBench-Client", "Deleting Address" )
		<< "Deleting remote msg address..." << ENDLOG;
	    BCLFreeAddress( remoteMsgAddress );
	    LOG( MLUCLog::kDebug, "SimpleBlobBench-Client", "Deleting Address" )
		<< "Deleting remote blob msg address..." << ENDLOG;
	    BCLFreeAddress( remoteBlobMsgAddress );

	    dtime = (end.tv_sec-start.tv_sec)*1000000+(end.tv_usec-start.tv_usec);
	    double rate;
	    rate = ((double)tSize)/((double)dtime);
	    rate *= 1000000.0;
	    LOG( MLUCLog::kBenchmark, "SimpleBlobBench-Client", "Bytes written" )
		<< MLUCLog::kDec << tSize << " (0x" << MLUCLog::kHex << tSize
		<< ") bytes written in " << MLUCLog::kDec << dtime << " usec -> "
		<< rate << "B/s == " << rate/(1024.0) << " kB/s == "
		<< rate/(1024.0*1024.0) << " MB/s." << ENDLOG;

	    rate = ((double)msgCount)/((double)dtime);
	    rate *= 1000000.0;
	    LOG( MLUCLog::kBenchmark, "SimpleBlobBench-Client", "Blobs written" )
		<< MLUCLog::kDec << msgCount << " (0x" << MLUCLog::kHex << msgCount
		<< ") blobs written in " << MLUCLog::kDec << dtime << " usec -> "
		<< rate << " Hz == " << rate/(1000.0) << " kHz == "
		<< rate/(1000000.0) << " MHz." << ENDLOG;

	    }
	LOG( MLUCLog::kDebug, "SimpleBlobBench-Client", "Deleting Com Object" )
	    << "Deleting remote msg object..." << ENDLOG;
	BCLFreeAddress( msgCom, ownMsgAddress );
	LOG( MLUCLog::kDebug, "SimpleBlobBench-Client", "Deleting Com Object" )
	    << "Deleting remote blob object..." << ENDLOG;
	BCLFreeAddress( blobCom, ownBlobAddress );
	LOG( MLUCLog::kDebug, "SimpleBlobBench-Client", "Deleting Com Object" )
	    << "Deleting remote blobmsg object..." << ENDLOG;
	BCLFreeAddress( blobMsgCom, ownBlobMsgAddress );
    }









/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
