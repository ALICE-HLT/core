/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLMsgCommunication.hpp"
#ifdef USE_SCI
#include "BCLSCIMsgCommunication.hpp"
#include "BCLSCIAddress.hpp"
#endif
#include "BCLMessage.hpp"
#ifdef USE_TCP
#include "BCLTCPAddress.hpp"
#include "BCLTCPComID.hpp"
#endif
#include "SimpleTestMsg.hpp"
#include "MLUCLog.hpp"
#include "MLUCLogServer.hpp"
#include "BCLErrorLogCallback.hpp"
#include "BCLURLAddressDecoder.hpp"
#include <cerrno>
#include <iostream>
#include <fstream>
#include <cstdlib>

#define TESTMSG 1
#define QUITMSG 2



MLUCLog::TLogMsg& LogAddress( MLUCLog::TLogMsg& log, BCLAbstrAddressStruct& addr )
    {
    switch ( addr.fComID )
	{
#ifdef USE_SCI
	case kSCIComID: 
	    return log << (BCLSCIAddressStruct&)addr;
#endif
#ifdef USE_TCP
	case kTCPComID: 
	    return log << (BCLTCPAddressStruct&)addr;
#endif
	default: 
	    return log << "Unknown address type";
	}
    }

class ExitOnErrorCallback: public BCLErrorCallback
    {
    public:

	ExitOnErrorCallback()
		{
		fCom = NULL;
		fDoUnbind = false;
		fDoDisconnect = false;
		fDisconnectAddress = NULL;
		fDoUnlock = false;
		fUnlockAddress = NULL;
		}

	virtual ~ExitOnErrorCallback() {};

	void DoUnbind()
		{
		fDoUnbind = true;
		}

	void DoDisconnect( BCLAbstrAddressStruct* address )
		{
		fDoDisconnect = true;
		fDisconnectAddress = address;
		}

	void DoUnlock( BCLAbstrAddressStruct* address )
		{
		fDoUnlock = true;
		fUnlockAddress = address;
		}
	
	void SetCom( BCLMsgCommunication* com )
		{
		fCom = com;
		}

	void Exit( int eval )
		{
		if ( fCom )
		    {
		    if ( fDoUnlock )
			fCom->UnlockConnection( fUnlockAddress );
		    if ( fDoDisconnect )
			fCom->Disconnect( fDisconnectAddress );
		    if ( fDoUnbind )
			fCom->Unbind();
		    }
		exit( eval );
		}

	virtual BCLErrorAction ConnectionTemporarilyLost( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { Exit(-1); return kAbort; };

	virtual BCLErrorAction ConnectionError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { Exit(-1); return kAbort; };

	virtual BCLErrorAction BindError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { Exit(-1); return kAbort; };

	virtual BCLErrorAction LockError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { Exit(-1); return kAbort; };

	virtual BCLErrorAction MsgSendError( int, BCLCommunication*,
					       BCLAbstrAddressStruct*, 
					     BCLMessageStruct* ) { Exit(-1); return kAbort; };

	virtual BCLErrorAction MsgReceiveError( int, BCLCommunication*,
					       BCLAbstrAddressStruct*, 
						BCLMessageStruct* ) { Exit(-1); return kAbort; };

	virtual BCLErrorAction BlobPrepareError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { Exit(-1); return kAbort; };

	virtual BCLErrorAction BlobSendError( int, BCLCommunication*,
					      BCLAbstrAddressStruct*, 
					      BCLTransferID ) { Exit(-1); return kAbort; };

	virtual BCLErrorAction BlobReceiveError( int, BCLCommunication*,
						 BCLTransferID ) { Exit(-1); return kAbort; };

	virtual BCLErrorAction AddressError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { Exit(-1); return kAbort; };

	virtual BCLErrorAction GeneralError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { Exit(-1); return kAbort; };

    protected:
	
	BCLMsgCommunication* fCom;
	bool fDoUnbind;
	bool fDoDisconnect;
	BCLAbstrAddressStruct* fDisconnectAddress;
	bool fDoUnlock;
	BCLAbstrAddressStruct* fUnlockAddress;

    };



int main( int argc, char** argv )
    {
    uint32 i;
    const char* errorStr = NULL;
    const char* usage1 = "Usage: ";
    const char* usage2 = " [ -server server-address-URL (-delay delaytime_in_us) | -client client-adress-url server-address-url (-count msgcount) (-msgsize <msg-size>) (-connect) (-lock)] (-V verbosity_levels) (-exitonerror)";
    char* cpErr;
    MLUCFilteredStdoutLogServer sOut;
    gLogLevel = MLUCLog::kAll;
    gLog.AddServer( sOut );
    BCLErrorLogCallback logCallback;
    gLogLevel = MLUCLog::kWarning|MLUCLog::kError|MLUCLog::kFatal;
    ExitOnErrorCallback exitCallback;
    
    BCLMsgCommunication* com = NULL;
    BCLAbstrAddressStruct *ownAddress=NULL, *remoteAddress=NULL;
    uint32 msgCount=1, msgNr;
    bool server = false, client = false;
    bool connect = false;
    bool do_lock = false;
    unsigned long delayTime = 0;
    unsigned long msgSize = sizeof(SimpleTestMsgStruct);
    bool exitonerror = false;
    i = 1;
    while ( (i < (uint32)argc) && !errorStr )
	{
	if ( !strcmp( argv[i], "-server" ) )
	    {
	    server = true;
	    if ( (uint32)argc <= i +1 )
		{
		errorStr = "Missing server address URL options.";
		break;
		}
	    bool isBlob;
	    int ret;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+1], com_tmp, ownAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid server address URL";
		break;
		}
	    if ( isBlob )
		{
		errorStr = "No server Blob addresses allowed";
		break;
		}
	    //BCLDecodeRemoteURL( argv[i+1], remoteAddress );
	    BCLGetEmptyAddressFromURL( argv[i+1], remoteAddress );
	    com = (BCLMsgCommunication*)com_tmp;
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-client" ) )
	    {
	    client = true;
	    if ( (uint32)argc <= i +2 )
		{
		errorStr = "Missing client address options.";
		break;
		}
	    bool isBlob;
	    int ret;
	    BCLCommunication* com_tmp;
	    ret = BCLDecodeLocalURL( argv[i+1], com_tmp, ownAddress, isBlob );
	    if ( ret )
		{
		errorStr = "Invalid client address URL";
		break;
		}
	    if ( isBlob )
		{
		errorStr = "No client Blob addresses allowed";
		break;
		}
	    ret = BCLDecodeRemoteURL( argv[i+2], remoteAddress );
	    if ( ret )
		{
		errorStr = "Invalid server address URL";
		break;
		}
	    com = (BCLMsgCommunication*)com_tmp;
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-connect" ) )
	    {
	    connect = true;
	    i++;
	    continue;
	    }
	if ( !strcmp( argv[i], "-count" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing count specifier.";
		break;
		}
	    msgCount = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-msgsize" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing message size specifier.";
		break;
		}
	    msgSize = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting message size specifier..";
		break;
		}
	    if ( msgSize < sizeof(SimpleTestMsgStruct) )
		{
		errorStr = "Specified message size too small.";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-delay" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing delaytime specifier.";
		break;
		}
	    delayTime = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting delaytime specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( (uint32)argc < i +1 )
		{
		errorStr = "Missing verbosity level specifier.";
		break;
		}
	    gLogLevel = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting verbosity level specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-lock" ) )
	    {
	    do_lock = true;
	    i++;
	    continue;
	    }
	if ( !strcmp( argv[i], "-exitonerror" ) )
	    {
	    exitonerror = true;
	    i++;
	    continue;
	    }
	errorStr = "Unknown option";
	}
    if ( !errorStr )
	{
	if ( client==server )
	    errorStr = "Must specify either one server or client option.";
	if ( server && connect )
	    errorStr = "'-connect' argument only valid for clients.";
	if ( server && msgCount!= 1 )
	    errorStr = "'-count' argument only valid for clients.";
	if ( client && delayTime!=0 )
	    errorStr = "'-delay' argument only valid for server";
	}

    if ( errorStr )
	{
	LOG( MLUCLog::kError, "SimpleMsgTest", "Command line options" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	MLUCString urls;
	vector<MLUCString> msgurls, bloburls;
	BCLGetAllowedURLs( msgurls, bloburls );
	vector<MLUCString>::iterator iter, end;
	iter = msgurls.begin();
	end = msgurls.end();
	while ( iter != end )
	    {
	    urls += *iter;
	    if ( iter+1 != end )
		urls += ", ";
	    iter++;
	    }
	LOG( MLUCLog::kError, "SimpleMsgTest", "Command line options" )
	    << "Allowed address URLS: " << urls.c_str() << "." << ENDLOG;
	LOG( MLUCLog::kError, "SimpleMsgTest", "Command line options" )
	    << errorStr << ENDLOG;
	return -1;
	}


    ofstream of;
    MLUCString name;
    name = "SimpleMsgTest-";
    if ( server )
	name += "Server";
    else
	name += "Client";
    name += ".log";
    of.open( name.c_str() );
    MLUCStreamLogServer streamOut( of );
    gLog.AddServer( streamOut );
    
    com->AddCallback( &logCallback );
    if ( exitonerror )
	com->AddCallback( &exitCallback );
    exitCallback.SetCom( com );
    if ( server )
	{
	// ------------------------------------------------------
	// Server code
	LOGG( MLUCLog::kInformational, "SimpleMsgTest-Server", "Listening", tmpLog )
	    << "Server @ " ;
	    LogAddress( tmpLog, *ownAddress ) << " listening..." << ENDLOG;
#ifdef USE_SCI
	// XXX
 	if ( ownAddress->fComID == kSCIComID )
 	    ((BCLSCIMsgCommunication*)com)->SetBufferSize( 4096 );
#endif
	com->Bind( ownAddress );
	exitCallback.DoUnbind();
	msgNr = 0;
	SimpleTestMsgStruct* msg;
	SimpleTestMsg testMsg;
	BCLMessageStruct* bmsg = NULL;
	bool quit = false;
	int ret;
	*remoteAddress = *ownAddress;
	if ( delayTime )
	    usleep( delayTime );
	do
	    {
	    msg = NULL;
	    ret = com->Receive( remoteAddress, bmsg );
	    msg = (SimpleTestMsgStruct*)bmsg;
	    if ( ret )
		{
		LOG( MLUCLog::kError, "SimpleMsgTest-Server", "Receive" )
		    << "Error on receive: " << strerror(ret) << " (" << MLUCLog::kDec
		    << ret << ") - (Expected message nr: " << msgNr << ")." << ENDLOG;
		break;
		}
	    if ( !msg )
		continue;
	    LOGG( MLUCLog::kInformational, "SimpleMsgTest-Server", "Message receive", tmpLog )
		<< "Server @ " ;
		LogAddress( tmpLog, *ownAddress ) << " received message " << *msg << " from ";
		LogAddress( tmpLog, *remoteAddress ) << ENDLOG;
	    if ( msg->fNr != msgNr )
		{
		LOG( MLUCLog::kWarning, "SimpleMsgTest-Server", "Message receive" )
		    << "Received message contained wrong sequence number " << msg->fNr
		    << "; Expected " << msgNr << ENDLOG;
		msgNr = msg->fNr;
		}
	    if ( msg->fMsg == QUITMSG )
		{
		LOG( MLUCLog::kInformational, "SimpleMsgTest-Server", "Quit Message receive" )
		    << "Received QUIT message." << ENDLOG;
		quit = true;
		}
	    if ( msg )
		com->ReleaseMsg( msg );
	    msgNr++;
	    }
	while ( !quit );
	com->Unbind();
	}
    else
	{
	// ------------------------------------------------------
	// Client code
	struct timeval start, stop;
	LOGG( MLUCLog::kInformational, "SimpleMsgTest-Client", "Bind", tmpLog )
	    << "Client @ " ;
	    LogAddress( tmpLog, *ownAddress ) << " binding..." << ENDLOG;
	com->Bind( ownAddress );
	exitCallback.DoUnbind();
	if ( connect )
	    {
	    LOGG( MLUCLog::kInformational, "SimpleMsgTest-Client", "Connect", tmpLog )
		<< "Client @ " ;
		LogAddress( tmpLog, *ownAddress ) << " connecting to " ;
	    LogAddress( tmpLog, *remoteAddress ) << ENDLOG;
	    com->Connect( remoteAddress );
	    exitCallback.DoDisconnect( remoteAddress );
	    if( do_lock )
		{
		com->LockConnection( remoteAddress );
		exitCallback.DoUnlock( remoteAddress );
		}
	    }
	SimpleTestMsgStruct* msg;
	SimpleTestMsg testMsg;
	msg = (SimpleTestMsgStruct*)new uint8[ msgSize ];
	if ( !msg )
	    {
	    LOG( MLUCLog::kError, "SimpleMsgTest-Client", "Out Of Memory" )
		<< "Out of memory trying to allocate message of " << MLUCLog::kDec
		<< msgSize << " bytes." << ENDLOG;
	    return 0;
	    }
	memcpy( msg, testMsg.GetData(), testMsg.GetData()->fLength );
	msg->fLength = msgSize;
	msg->fMsg = TESTMSG;
	gettimeofday( &start, NULL );
	for ( i = 0; i < msgCount; i++ )
	    {
	    msg->fNr = i;
	    LOGG( MLUCLog::kInformational, "SimpleMsgTest-Client", "Send", tmpLog )
		<< "Client @ " ;
		LogAddress( tmpLog, *ownAddress ) << " sending message " << *msg << ENDLOG;
	    com->Send( remoteAddress, msg );
	    LOGG( MLUCLog::kInformational, "SimpleMsgTest-client", "Message send", tmpLog )
		<< "Client @ " ;
		LogAddress( tmpLog, *ownAddress ) << " sent message " << i << " to " ;
		LogAddress( tmpLog, *remoteAddress ) << ENDLOG;
	    }
	gettimeofday( &stop, NULL );
	msg->fNr = msgCount;
	msg->fMsg = QUITMSG;
	com->Send( remoteAddress, msg );
	if( do_lock ) 
	    {
	    com->UnlockConnection( remoteAddress );
	    }
	if ( connect )	    
	    {
	    com->Disconnect( remoteAddress );
	    }
	com->Unbind();
	delete [] (uint8*)msg;
 	unsigned long long tdiff;
	tdiff = stop.tv_sec-start.tv_sec;
	tdiff *= 1000000;
	tdiff += (stop.tv_usec-start.tv_usec);
	//tdiff = (stop.tv_sec-start.tv_sec)*1000000+(stop.tv_usec-start.tv_usec);
	LOG( MLUCLog::kBenchmark, "SimpleMsgTest-client", "Message Results" )
	    << MLUCLog::kDec << msgCount << " messages sent in " << tdiff 
	    << " usec: " << ((double)msgCount)/((double)tdiff) << " msg/usec - "
	    << ((double)msgCount*1000000)/((double)tdiff) << " msg/s." << ENDLOG;
	
	}
    delete remoteAddress;
    delete ownAddress;
    delete com;
    return 0;
    }

/*

***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/



