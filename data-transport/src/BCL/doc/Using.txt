
Basic Communication Library (BCL)

Using the library


First of all: This library has only been developed and tested on an
Intel SuSE Linux 7.0 systems. Your mileage may vary on other systems.

Careful on non-Linux systems. Some Linux specific features might be 
used in the software so that it will compile but might not work on 
your system. Or it might not compile at all.

If you encounter problems, please write to me and I will try to help.
If you encounter problems and are able to solve them, please write to
as well, with the problem and the solution and I will try to integrate 
this into the library.

EMail: timm@kip.uni-heidelberg.de


To use the library you have to do a number of steps:

The first one is of course to include the proper files and reference the 
classes you want to use in your source code.

In your Makefile you can include the conf/Makefile.extern.conf file. This
will contain the setting you will have to use to link/include the library.
The Makefile variable "BCLINCLUDE" contains the compiler include path (incl.
"-I" prefix), the "BCLDEFINES" variable will hold any preprocessor definitions
needed on the command line. The "BCLLIBDIR" contains the library path for the linker and
the "BCLLIBS" variable the libraries needed by applications, both the
BCL libraries themselves as well as the libraries needed by BCL.

But, as nothing is for free, you will have to define one Makefile variable
in your Makefile before using the above variables. 
"BCLCTOPDIR" has to be set to the absolute path of the top-level directory 
of the BCL tree. 

Note that you also have to include support for the MLUC library on which BCL
depends. For information on how to do this read the file doc/Using.txt in the 
MLUC directory tree.


If you are using the shared library version of BCL, you may have to take some
additional steps. You can either take these steps as a precaution before any
problems arise or when you get error messages when you try to run programs
linked to the library complaining that a file named libBCL.so* cannot be found.
This means, that you will have to take care that the dynamic linker actually
finds the dynamic version of the library. Basically there are two ways to do this:
The first alternative is to place the file(s) in one of the system wide directories
where the dynamic linker looks by default, this is usually one of /lib, /usr/lib, 
/usr/X11R6/lib, etc. On Linux it should be one of the directories listed in ld.so.conf.
For this you will of course need root privileges on most systems.
Please take care of the symbolic links involved. Copy the target file of 
the links to the directory of your choice and then set (or copy) the symbolic
links to the target file.
Alternatively you can set an environment variable that specifies additional directories
for the linker to look for shared libraries. This environment variable is called
LD_LIBRARY_PATH and it is a colon separated list of directories. Thus if you are 
using the bash shell, to add the directory where the shared BCL library is located 
you could enter the line 
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/BCL-directory/lib/`uname -s`-`uname -m`/
on the command line or into your .bashrc, adapted to whatever the path to the BCL 
shared library actually is. For other shells you would have to use the corresponding
equivalent command.

The above is also relevant for shared library version of the MLUC library, as
is detailed in the MLUC Using.txt documentation file.
