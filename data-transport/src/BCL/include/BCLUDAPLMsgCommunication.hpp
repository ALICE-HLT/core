/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _BCLUDAPLMSGCOMMUNICATION_HPP_
#define _BCLUDAPLMSGCOMMUNICATION_HPP_

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: BCLUDAPLMsgCommunication.hpp 738 2005-11-16 07:51:25Z timm $ 
**
***************************************************************************
*/

#ifdef USE_UDAPL


#include "BCLMsgCommunication.hpp"
#include "BCLUDAPLAddress.hpp"
#include "BCLIntUDAPLComHelper.hpp"
#include "MLUCThread.hpp"
#include "MLUCConditionSem.hpp"
#include "MLUCVector.hpp"
#include "MLUCDynamicAllocCache.hpp"
#include "BCLUDAPLComID.hpp"
#include "BCLMessage.hpp"
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include <list>



class BCLUDAPLMsgCommunication: public BCLMsgCommunication
    {
    public:

	BCLUDAPLMsgCommunication( int slotCntExp2 = -1 );
	virtual ~BCLUDAPLMsgCommunication();

	virtual const BCLUDAPLAddressStruct* GetAddress()
		{
		return (BCLUDAPLAddressStruct*)fAddress;
		}

	virtual int Bind( BCLAbstrAddressStruct* abstrAddress );
	virtual int Unbind();

	virtual int Connect( BCLAbstrAddressStruct* abstrAddress, bool openOnDemand = false );
	virtual int Connect( BCLAbstrAddressStruct* address, unsigned long timeout_ms, bool openOnDemand = false );
	virtual int Disconnect( BCLAbstrAddressStruct* abstrAddress );
	virtual int Disconnect( BCLAbstrAddressStruct* address, unsigned long timeout_ms );

	virtual bool CanLockConnection();
	virtual int LockConnection( BCLAbstrAddressStruct* address );
	virtual int LockConnection( BCLAbstrAddressStruct* address, unsigned long timeout_ms );
	virtual int UnlockConnection( BCLAbstrAddressStruct* address );
	virtual int UnlockConnection( BCLAbstrAddressStruct* address, unsigned long timeout_ms );

	virtual int Send( BCLAbstrAddressStruct* abstrAddress, BCLMessageStruct* msg, 
			  BCLErrorCallback* callback = NULL );
	virtual int Send( BCLAbstrAddressStruct* address, BCLMessageStruct* msg, 
			  unsigned long timeout_ms, BCLErrorCallback* callback = NULL );
	
	virtual int Receive( BCLAbstrAddressStruct* abstrAddr, BCLMessageStruct*& msg, BCLErrorCallback* callback = NULL );
	virtual int Receive( BCLAbstrAddressStruct* abstrAddr, BCLMessageStruct*& msg, unsigned long timeout_ms, BCLErrorCallback* callback = NULL );
	virtual int ReleaseMsg( BCLMessageStruct* msg );

	virtual void Reset();


	virtual uint32 GetAddressLength();
	virtual uint32 GetComID();
	virtual BCLUDAPLAddressStruct* NewAddress();
	virtual void DeleteAddress( BCLAbstrAddressStruct* address );
	virtual bool AddressEquality( const BCLAbstrAddressStruct* addr1, const BCLAbstrAddressStruct* addr2 );

	virtual operator bool();

    protected:
	
	virtual int Send( BCLAbstrAddressStruct* abstrAddress, BCLMessageStruct* msg, 
			  bool useTimeout, unsigned long timeout_ms, BCLErrorCallback* callback = NULL );

	    friend class BCLIntUDAPLMsgComHelper;

	virtual BCLMessageStruct* NewMessage( uint32 size );
	virtual void FreeMessage( BCLMessageStruct* msg );

	virtual int Receive( BCLUDAPLAddressStruct* addr, BCLMessageStruct*& msg, bool useTimeout, unsigned long timeout, BCLErrorCallback* callback );

	BCLUDAPLAddressStruct fUDAPLAddress;

	BCLIntUDAPLMsgComHelper fComHelper;


	MLUCConditionSem<uint8> fDataArrivedSignal;

	MLUCDynamicAllocCache fMessageAllocCache;
	MLUCDynamicAllocCache fAddressAllocCache;
	BCLUDAPLAddress fDefaultAddress;

	uint32 fMsgSeqNr;


	struct UDAPLMsgData
	    {
		BCLUDAPLAddressStruct fAddress;
		BCLMessageStruct* fMsg;
	    };


	//typedef list<UDAPLMsgData> TUDAPLMsgDataContType;
	typedef MLUCVector<UDAPLMsgData> TUDAPLMsgDataContType;
        TUDAPLMsgDataContType fMessages;
      
    private:
    };




#endif // USE_UDAPL

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: BCLUDAPLMsgCommunication.hpp 738 2005-11-16 07:51:25Z timm $ 
**
***************************************************************************
*/

#endif // _BCLUDAPLMSGCOMMUNICATION_HPP_
