/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _BCL_ERRORLOGCALLBACK_H__
#define _BCL_ERRORLOGCALLBACK_H__

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLTypes.h"
#include "BCLErrorCallback.hpp"
#include "MLUCLog.hpp"
#include "BCLAbstrAddress.hpp"
#include "BCLAddressLogger.hpp"
//#include "BCLSCIAddress.hpp"
//#include "BCLSCIComID.hpp"
//#include "BCLTCPAddress.hpp"
//#include "BCLTCPComID.hpp"
#include <errno.h>
#include <string.h>


class BCLCommunication;
struct BLCMessageStruct;
struct BCLAbstrAddressStruct;


class BCLErrorLogCallback: public BCLErrorCallback
    {
    public:

	virtual BCLErrorAction ConnectionTemporarilyLost( int error, BCLCommunication*,
							  BCLAbstrAddressStruct* address ) 
		{
		LOGCG( gBCLLogLevelRef, MLUCLog::kError, "BCLErrorLogCallback", "Connection temporarily lost", tmpLog )
		    << "Connection to address " ;
		    if ( address )
			BCLAddressLogger::LogAddress( tmpLog, *address );
		    else
			tmpLog << "(Null pointer)";
		    tmpLog << " temporarily lost. " 
			 << "Reason: " << strerror(error) << " (" << error << ")." << ENDLOG;
		return kAbort; 
		};

	virtual BCLErrorAction ConnectionError( int error, BCLCommunication*,
						BCLAbstrAddressStruct* address )  
		{
		LOGCG( gBCLLogLevelRef, MLUCLog::kError, "BCLErrorLogCallback", "Connection error", tmpLog )
		    << "Connection error to address " ;
		    if ( address )
			BCLAddressLogger::LogAddress( tmpLog, *address );
		    else
			tmpLog << "(Null pointer)";
		    tmpLog << ". " 
			 << "Reason: " << strerror(error) << " (" << error << ")." << ENDLOG;
		return kAbort; 
		};

	virtual BCLErrorAction BindError( int error, BCLCommunication*,
					  BCLAbstrAddressStruct* address )  
		{
		LOGCG( gBCLLogLevelRef, MLUCLog::kError, "BCLErrorLogCallback", "Bind error", tmpLog )
		    << "Bind error on address " ;
		    if ( address )
			BCLAddressLogger::LogAddress( tmpLog, *address );
		    else
			tmpLog << "(Null pointer)";
		    tmpLog << ". " 
			 << "Reason: " << strerror(error) << " (" << error << ")." << ENDLOG;
		return kAbort; 
		};

	virtual BCLErrorAction LockError( int error, BCLCommunication*,
					  BCLAbstrAddressStruct* address )  
		{
		LOGCG( gBCLLogLevelRef, MLUCLog::kError, "BCLErrorLogCallback", "Lock error", tmpLog )
		    << "Lock error on address " ;
		    if ( address )
			BCLAddressLogger::LogAddress( tmpLog, *address );
		    else
			tmpLog << "(Null pointer)";
		    tmpLog << ". " 
			 << "Reason: " << strerror(error) << " (" << error << ")." << ENDLOG;
		return kAbort; 
		};

	virtual BCLErrorAction MsgSendError( int error, BCLCommunication*,
					     BCLAbstrAddressStruct* address, 
					     BCLMessageStruct* )  
		{
		LOGCG( gBCLLogLevelRef, MLUCLog::kError, "BCLErrorLogCallback", "Message send error", tmpLog )
		    << "Error sending message " ;
		    if ( address )
			BCLAddressLogger::LogAddress( tmpLog, *address );
		    else
			tmpLog << "(Null pointer)";
		    tmpLog << ". " 
			 << "Reason: " << strerror(error) << " (" << error << ")." << ENDLOG;
		return kAbort; 
		};

	virtual BCLErrorAction MsgReceiveError( int error, BCLCommunication*,
						BCLAbstrAddressStruct* address, 
						BCLMessageStruct* )  
		{
		LOGCG( gBCLLogLevelRef, MLUCLog::kError, "BCLErrorLogCallback", "Message receive error", tmpLog )
		    << "Error receiving message from address " ;
		    if ( address )
			BCLAddressLogger::LogAddress( tmpLog, *address );
		    else
			tmpLog << "(Null pointer)";
		    tmpLog << ". "
			 << "Reason: " << strerror(error) << " (" << error << ")." << ENDLOG;
		return kAbort; 
		};
	
	virtual BCLErrorAction BlobPrepareError( int error, BCLCommunication*,
						 BCLAbstrAddressStruct* address )  
		{
		LOGCG( gBCLLogLevelRef, MLUCLog::kError, "BCLErrorLogCallback", "Blob transfer preparation error", tmpLog )
		    << "Error preparing blob transfer to address " ;
		    if ( address )
			BCLAddressLogger::LogAddress( tmpLog, *address );
		    else
			tmpLog << "(Null pointer)";
		    tmpLog << ". " 
			 << "Reason: " << strerror(error) << " (" << error << ")." << ENDLOG;
		return kAbort; 
		};

	virtual BCLErrorAction BlobSendError( int error, BCLCommunication*,
					      BCLAbstrAddressStruct* address, 
					      BCLTransferID )  
		{
		LOGCG( gBCLLogLevelRef, MLUCLog::kError, "BCLErrorLogCallback", "Blob send error", tmpLog )
		    << "Error sending blob to address " ;
		    if ( address )
			BCLAddressLogger::LogAddress( tmpLog, *address );
		    else
			tmpLog << "(Null pointer)";
		    tmpLog << ". " 
			 << "Reason: " << strerror(error) << " (" << error << ")." << ENDLOG;
		return kAbort; 
		};

	virtual BCLErrorAction BlobReceiveError( int error, BCLCommunication*,
						 BCLTransferID transfer )  
		{
		LOGCG( gBCLLogLevelRef, MLUCLog::kError, "BCLErrorLogCallback", "Blob receive error", tmpLog )
		    << "Error receiving blob 0x" << MLUCLog::kHex << transfer 
		    << ". Reason: " << strerror(error) << " (" << error << ")." << ENDLOG;
		return kAbort; 
		};
	
	virtual BCLErrorAction AddressError( int error, BCLCommunication*,
					     BCLAbstrAddressStruct* address )  
		{
		LOGCG( gBCLLogLevelRef, MLUCLog::kError, "BCLErrorLogCallback", "Address error", tmpLog )
		    << "Error in address " ;
		    if ( address )
			BCLAddressLogger::LogAddress( tmpLog, *address );
		    else
			tmpLog << "(Null pointer)";
		    tmpLog << ". " 
			 << "Reason: " << strerror(error) << " (" << error << ")." << ENDLOG;
		return kAbort; 
		};
	
	virtual BCLErrorAction GeneralError( int error, BCLCommunication*,
					     BCLAbstrAddressStruct* address )  
		{
		LOGCG( gBCLLogLevelRef, MLUCLog::kError, "BCLErrorLogCallback", "General error", tmpLog )
		    << "General error with address " ;
		    if ( address )
			BCLAddressLogger::LogAddress( tmpLog, *address );
		    else
			tmpLog << "(Null pointer)";
		    tmpLog << "." 
			 << "Reason: " << strerror(error) << " (" << error << ")." << ENDLOG;
		return kAbort; 
		};
	
    protected:
	

    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _BCL_ERRORLOGCALLBACK_H__
