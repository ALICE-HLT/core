/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _BCLINTTCPHELPER_HPP_
#define _BCLINTTCPHELPER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLTCPAddress.hpp"
#include "BCLCommunication.hpp"
#include "MLUCThread.hpp"
#include "MLUCConditionSem.hpp"
#include <pthread.h>
#include <vector>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>


#if 0
#define TCPCOM_NO_POLL
#endif



class BCLTCPMsgCommunication;
class BCLTCPBlobCommunication;

static const unsigned gTCPMsgReadBufferSize = 4096;


class BCLIntTCPComHelper: public BCLIntComHelper
    {
    protected:
	struct TTCPConnectionData;

    public:
	
	BCLIntTCPComHelper( BCLCommunication* com );
	virtual ~BCLIntTCPComHelper();

	int Bind( BCLTCPAddressStruct* address );
	int Unbind();

	int Connect( BCLTCPAddressStruct* address, bool useTimeout, uint32 timeout_ms, bool openOnDemand = false );
	int Disconnect( BCLTCPAddressStruct* address, bool useTimeout, uint32 timeout_ms );

	int ConnectToRemote( BCLTCPAddressStruct* address, TTCPConnectionData* connectData, bool now, bool useTimeout, uint32 timeout_ms );
	int DisconnectFromRemote( TTCPConnectionData* connectData, bool useTimeout, uint32 timeout_ms, bool forceSocketClose=false );

	void CloseAcceptedConnection( int socket );

	static const uint32 kInvalidSender = (uint32)-1;

	void SetTimeout( uint32 timeout_sec, uint32 timeout_musec );
	
	int GetSockOpt( BCLTCPAddressStruct* address, int level, int optname, void *optval, socklen_t *optlen );
      
	int SetSockOpt( BCLTCPAddressStruct* address, int level, int optname,  const  void *optval, socklen_t optlen );

	int GetSockOpt( int level, int optname, void *optval, socklen_t *optlen );
      
	int SetSockOpt( int level, int optname,  const  void *optval, socklen_t optlen );
      
	void SetBackLog( int backlog )
		{
		fBackLog = backlog;
		}

	void SetSelectErrorLimit( int limit )
		{
		fSelectErrorLimit = limit;
		}

    protected:
	
	    friend class BCLTCPMsgCommunication;
	    friend class BCLTCPBlobCommunication;

	BCLCommunication* fCom;
	
	uint32 fIPNr;
	uint16 fPortNr;

	int fSocket;
	unsigned int fConnectionTimeout;
	int fBackLog;
	// This is the limit on how many select calls are allowed to return
	// with errors in a row before the Accept routine terminates...
	int fSelectErrorLimit;


	struct TTCPConnectionData
	    {
		int fSocket;
		BCLTCPAddressStruct fAddress;
		uint32 fUsageCount;
		bool fInProgress;
	    };

	pthread_mutex_t fConnectionMutex;
	MLUCConditionSem<uint8> fConnectionUpdateMutex;
	vector<TTCPConnectionData> fConnections;

	BCLTCPAddressStruct* fAddress;


	struct TAcceptedConnectionData
	    {
		int fSocket;
		struct sockaddr_in fAddr;
		socklen_t fAddrLen;
		BCLTCPAddressStruct fAddress;
		uint16 fBufferContentStart;
		uint16 fBufferContentSize;
		struct timeval fTimeout;
		uint8 fReadBuffer[ gTCPMsgReadBufferSize ];
	    };

	vector<TAcceptedConnectionData> fAcceptedSockets;

	bool fQuitAccept;
	bool fAcceptQuitted;
	void Accept();

	class TAcceptThread: public MLUCThread
	    {
	    public:
		
		TAcceptThread( BCLIntTCPComHelper* helper )
			{
			fHelper = helper;
			}

		~TAcceptThread() {};
		    
		virtual void Run()
			{
			fHelper->Accept();
			}

	    protected:

		BCLIntTCPComHelper* fHelper;

	    private:
	    };

	    friend class TAcceptThread;

	TAcceptThread fAcceptThread;

	ssize_t Read( const TAcceptedConnectionData& con, void *buf, size_t count, bool immediateRead = false );
	ssize_t ReadV( const TAcceptedConnectionData& con, const struct iovec *vector, int count, bool immediateRead = false );

	virtual bool NewConnection( TAcceptedConnectionData& con ) = 0;
	virtual int NewData( TAcceptedConnectionData& con ) = 0;
	virtual bool ConnectionTimeout( TAcceptedConnectionData& con ) = 0;
	virtual void ConnectionClosed( TAcceptedConnectionData& con ) = 0;

	void ConnectionBroken( const TTCPConnectionData* conData );


	static void (*fOldSigPipeHandler)(int);
	static void SigPipeHandler( int signum );

        static int GetProtocolByName(const char* name, int* proto);

    private:
    };

class BCLIntTCPMsgComHelper: public BCLIntTCPComHelper
    {
    public:

	BCLIntTCPMsgComHelper( BCLTCPMsgCommunication* com );
	~BCLIntTCPMsgComHelper();

    protected:

	virtual bool NewConnection( TAcceptedConnectionData& con );
	virtual int NewData( TAcceptedConnectionData& con );
	virtual bool ConnectionTimeout( TAcceptedConnectionData& con );
	virtual void ConnectionClosed( TAcceptedConnectionData& con );
	
	BCLTCPMsgCommunication* fMsgCom;

    };


class BCLIntTCPBlobComHelper: public BCLIntTCPComHelper
    {
    public:

	BCLIntTCPBlobComHelper( BCLTCPBlobCommunication* com );
	~BCLIntTCPBlobComHelper();

    protected:

	virtual bool NewConnection( TAcceptedConnectionData& con );
	virtual int NewData( TAcceptedConnectionData& con );
	virtual bool ConnectionTimeout( TAcceptedConnectionData& con );
	virtual void ConnectionClosed( TAcceptedConnectionData& con );
	
	BCLTCPBlobCommunication* fBlobCom;

    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _BCLINTTCPHELPER_HPP_
