/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _BCLCOMMUNICATION_HPP_
#define _BCLCOMMUNICATION_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLTypes.h"
#include "BCLLog.hpp"
#include "MLUCLog.hpp"
#include <vector>
#include <pthread.h>

struct BCLAbstrAddressStruct;
class BCLErrorCallback;
struct BCLMessageStruct;
class BCLIntComHelper;

const uint32 kMsgComType  = 1;
const uint32 kBlobComType = 2; 

class BCLCommunication
    {
    public:
	
	BCLCommunication();
	virtual ~BCLCommunication();

	virtual const BCLAbstrAddressStruct* GetAddress()
		{
		return fAddress;
		}

	virtual void SetReceiveTimeout( unsigned long recTimeout_ms )
		{
		fReceiveTimeout = recTimeout_ms;
		}

	virtual int Bind( BCLAbstrAddressStruct* address ) = 0;
	virtual int Unbind() = 0;

	virtual int Connect( BCLAbstrAddressStruct* address, bool openOnDemand = false ) = 0;
	virtual int Connect( BCLAbstrAddressStruct* address, unsigned long timeout_ms, bool openOnDemand = false ) = 0;
	virtual int Disconnect( BCLAbstrAddressStruct* address ) = 0;
	virtual int Disconnect( BCLAbstrAddressStruct* address, unsigned long timeout_ms ) = 0;

	virtual bool CanLockConnection() = 0;
	virtual int LockConnection( BCLAbstrAddressStruct* address ) = 0;
	virtual int LockConnection( BCLAbstrAddressStruct* address, unsigned long timeout_ms ) = 0;
	virtual int UnlockConnection( BCLAbstrAddressStruct* address ) = 0;
	virtual int UnlockConnection( BCLAbstrAddressStruct* address, unsigned long timeout_ms ) = 0;

	void AddCallback( BCLErrorCallback* callback );
	void DelCallback( const BCLErrorCallback* callback );

	virtual uint32 GetAddressLength() = 0;
	virtual uint32 GetComID() = 0;
	virtual BCLAbstrAddressStruct* NewAddress() = 0;
	virtual void DeleteAddress( BCLAbstrAddressStruct* address ) = 0;
	virtual bool AddressEquality( const BCLAbstrAddressStruct* addr1, const BCLAbstrAddressStruct* addr2 ) = 0;

	virtual uint32 GetComType() const = 0; // Return either kMsgComType or kBlobComType

	virtual operator bool() = 0;

	void ConnectionError( int error, BCLCommunication* com,
			      BCLAbstrAddressStruct* address, BCLErrorCallback* callback = NULL );
	
	void BindError( int error, BCLCommunication* com,
			   BCLAbstrAddressStruct* address, BCLErrorCallback* callback = NULL );

	void LockError( int error, BCLCommunication* com,
			   BCLAbstrAddressStruct* address, BCLErrorCallback* callback = NULL );
	
	void MsgSendError( int error, BCLCommunication* com,
			   BCLAbstrAddressStruct* address, 
			   BCLMessageStruct* msg, BCLErrorCallback* callback = NULL );
	
	void MsgReceiveError( int error, BCLCommunication* com,
			      BCLAbstrAddressStruct* address, 
			      BCLMessageStruct* msg, BCLErrorCallback* callback = NULL );
	
	void BlobPrepareError( int error, BCLCommunication* com,
			       BCLAbstrAddressStruct* address, BCLErrorCallback* callback = NULL );
	
	void BlobSendError( int error, BCLCommunication* com,
			    BCLAbstrAddressStruct* address, 
			    BCLTransferID transfer, BCLErrorCallback* callback = NULL );
	
	void BlobReceiveError( int error, BCLCommunication* com,
			       BCLTransferID transfer, BCLErrorCallback* callback = NULL );
	
	void AddressError( int error, BCLCommunication* com,
			   BCLAbstrAddressStruct* address, BCLErrorCallback* callback = NULL );

	void GeneralError( int error, BCLCommunication* com,
			   BCLAbstrAddressStruct* address, BCLErrorCallback* callback = NULL );

    protected:

	vector<BCLErrorCallback*> fErrorCallbacks;
	pthread_mutex_t fErrorCallbackMutex;
	BCLAbstrAddressStruct* fAddress;

	unsigned long fReceiveTimeout;  // in milliseconds.

	void ConnectionTemporarilyLost( int error, BCLCommunication* com,
			     BCLAbstrAddressStruct* address, BCLErrorCallback* callback = NULL );
	

	    friend class BCLIntComHelper;

    private:
    };

class BCLIntComHelper
    {
    public:

	void ConnectionTemporarilyLost( int error, BCLCommunication* com,
			     BCLAbstrAddressStruct* address, BCLErrorCallback* callback = NULL )
		{
		com->ConnectionTemporarilyLost( error, com, address, callback );
		}
	
	void ConnectionError( int error, BCLCommunication* com,
			      BCLAbstrAddressStruct* address, BCLErrorCallback* callback = NULL )
		{
		com->ConnectionError( error, com, address, callback );
		}
	
	void BindError( int error, BCLCommunication* com,
			   BCLAbstrAddressStruct* address, BCLErrorCallback* callback = NULL )
		{
		com->BindError( error, com, address, callback );
		}
	
	void LockError( int error, BCLCommunication* com,
			   BCLAbstrAddressStruct* address, BCLErrorCallback* callback = NULL )
		{
		com->LockError( error, com, address, callback );
		}
	
	void MsgSendError( int error, BCLCommunication* com,
			   BCLAbstrAddressStruct* address, 
			   BCLMessageStruct* msg, BCLErrorCallback* callback = NULL )
		{
		com->MsgSendError( error, com, address, msg, callback );
		}
	
	void MsgReceiveError( int error, BCLCommunication* com,
			      BCLAbstrAddressStruct* address, 
			      BCLMessageStruct* msg, BCLErrorCallback* callback = NULL )
		{
		com->MsgReceiveError( error, com, address, msg, callback );
		}
	
	void BlobPrepareError( int error, BCLCommunication* com,
			       BCLAbstrAddressStruct* address, BCLErrorCallback* callback = NULL )
		{
		com->BlobPrepareError( error, com, address, callback );
		}
	
	void BlobSendError( int error, BCLCommunication* com,
			    BCLAbstrAddressStruct* address, 
			    BCLTransferID transfer, BCLErrorCallback* callback = NULL )
		{
		com->BlobSendError( error, com, address, transfer, callback );
		}
	
	void BlobReceiveError( int error, BCLCommunication* com,
			       BCLTransferID transfer, BCLErrorCallback* callback = NULL )
		{
		com->BlobReceiveError( error, com, transfer, callback );
		}
	
	void AddressError( int error, BCLCommunication* com,
			   BCLAbstrAddressStruct* address, BCLErrorCallback* callback = NULL )
		{
		com->AddressError( error, com, address, callback );
		}

	void GeneralError( int error, BCLCommunication* com,
			   BCLAbstrAddressStruct* address, BCLErrorCallback* callback = NULL )
		{
		com->GeneralError( error, com, address, callback );
		}


    };


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _BCLCOMMUNICATION_HPP_
