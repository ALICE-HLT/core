/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _BCLTCPMSGCOMMUNICATION_HPP_
#define _BCLTCPMSGCOMMUNICATION_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#ifdef USE_TCP


#include "BCLMsgCommunication.hpp"
#include "BCLTCPAddress.hpp"
#include "BCLIntTCPComHelper.hpp"
#include "MLUCThread.hpp"
#include "MLUCConditionSem.hpp"
#include "MLUCVector.hpp"
#include "MLUCDynamicAllocCache.hpp"
#include "BCLTCPComID.hpp"
#include "BCLMessage.hpp"
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include <list>

#if 0
#define BIGTCPLOCK
#warning Big TPC Lock defined
#endif

#if 0
#define LOG_MESSAGE_CONTENTS
#endif


class BCLTCPMsgCommunication: public BCLMsgCommunication
    {
    public:

	BCLTCPMsgCommunication( int slotCntExp2 = -1 );
	virtual ~BCLTCPMsgCommunication();

	virtual const BCLTCPAddressStruct* GetAddress()
		{
		return (BCLTCPAddressStruct*)fAddress;
		}

	virtual int Bind( BCLAbstrAddressStruct* abstrAddress );
	virtual int Unbind();

	virtual int Connect( BCLAbstrAddressStruct* abstrAddress, bool openOnDemand = false );
	virtual int Connect( BCLAbstrAddressStruct* address, unsigned long timeout_ms, bool openOnDemand = false );
	virtual int Disconnect( BCLAbstrAddressStruct* abstrAddress );
	virtual int Disconnect( BCLAbstrAddressStruct* address, unsigned long timeout_ms );

	virtual bool CanLockConnection();
	virtual int LockConnection( BCLAbstrAddressStruct* address );
	virtual int LockConnection( BCLAbstrAddressStruct* address, unsigned long timeout_ms );
	virtual int UnlockConnection( BCLAbstrAddressStruct* address );
	virtual int UnlockConnection( BCLAbstrAddressStruct* address, unsigned long timeout_ms );

	virtual int Send( BCLAbstrAddressStruct* abstrAddress, BCLMessageStruct* msg, 
			  BCLErrorCallback* callback = NULL );
	virtual int Send( BCLAbstrAddressStruct* address, BCLMessageStruct* msg, 
			  unsigned long timeout_ms, BCLErrorCallback* callback = NULL );
	
	virtual int Receive( BCLAbstrAddressStruct* abstrAddr, BCLMessageStruct*& msg, BCLErrorCallback* callback = NULL );
	virtual int Receive( BCLAbstrAddressStruct* abstrAddr, BCLMessageStruct*& msg, unsigned long timeout_ms, BCLErrorCallback* callback = NULL );
	virtual int ReleaseMsg( BCLMessageStruct* msg );

	virtual void Reset();


	virtual uint32 GetAddressLength();
	virtual uint32 GetComID();
	virtual BCLTCPAddressStruct* NewAddress();
	virtual void DeleteAddress( BCLAbstrAddressStruct* address );
	virtual bool AddressEquality( const BCLAbstrAddressStruct* addr1, const BCLAbstrAddressStruct* addr2 );

	virtual operator bool();

    protected:
	
	virtual int Send( BCLAbstrAddressStruct* abstrAddress, BCLMessageStruct* msg, 
			  bool useTimeout, unsigned long timeout_ms, BCLErrorCallback* callback = NULL );

	    friend class BCLIntTCPMsgComHelper;

	virtual BCLMessageStruct* NewMessage( uint32 size );
	virtual void FreeMessage( BCLMessageStruct* msg );

	virtual int Receive( BCLTCPAddressStruct* addr, BCLMessageStruct*& msg, bool useTimeout, unsigned long timeout, BCLErrorCallback* callback );

	BCLTCPAddressStruct fTCPAddress;

	BCLIntTCPMsgComHelper fComHelper;


	MLUCConditionSem<uint8> fDataArrivedSignal;

	MLUCDynamicAllocCache fMessageAllocCache;
	MLUCDynamicAllocCache fAddressAllocCache;
	BCLTCPAddress fDefaultAddress;

	uint32 fMsgSeqNr;


	struct TCPMsgData
	    {
		BCLTCPAddressStruct fAddress;
		BCLMessageStruct* fMsg;
	    };


	struct TCPMsgProgressData
	    {
		BCLTCPAddressStruct fAddress;
		BCLMessageStruct fMsgS;
		BCLMessageStruct* fMsg;
		uint32 fMsgLen;
		uint32 fBytesRead;
		int fSocket;
	    };

	//typedef list<TCPMsgData> TTCPMsgDataContType;
	typedef MLUCVector<TCPMsgData> TTCPMsgDataContType;
        TTCPMsgDataContType fMessages;
      
	typedef MLUCVector<TCPMsgProgressData> TTCPMsgProgressDataContType;
	//typedef list<TCPMsgProgressData> TTCPMsgProgressDataContType;
	TTCPMsgProgressDataContType fMsgProgress;

	bool NewConnection( BCLIntTCPComHelper::TAcceptedConnectionData& con );

	int NewData( BCLIntTCPComHelper::TAcceptedConnectionData& con );

	bool ConnectionTimeout( BCLIntTCPComHelper::TAcceptedConnectionData& con );

	void ConnectionClosed( BCLIntTCPComHelper::TAcceptedConnectionData& con );

	static bool FindMsgProgressBySocket( const TCPMsgProgressData& msgProgress, const int& refSocket );

#ifdef BIGTCPLOCK
	pthread_mutex_t fBigTCPLockTest;
#endif

    private:
    };




#endif // USE_TCP

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _BCLTCPMSGCOMMUNICATION_HPP_
