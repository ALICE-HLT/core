/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#ifndef _BCLADDRESSLOGGER_HPP_
#define _BCLADDRESSLOGGER_HPP_


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLAbstrAddress.hpp"
#include "MLUCTypes.h"
#include <vector>
#include <pthread.h>

class BCLAddressLogger
    {
    public:
	static void Init();

	typedef MLUCLog::TLogMsg& (*TLogFunc)( MLUCLog::TLogMsg&, const BCLAbstrAddressStruct& );

	static void AddLogFunc( uint32 comID, TLogFunc logFunc );
	static void DelLogFunc( uint32 comID );

	static MLUCLog::TLogMsg& LogAddress( MLUCLog::TLogMsg& log, const BCLAbstrAddressStruct& addr );
	
    protected:

	struct TLogFuncData
	    {
		uint32 fComID;
		TLogFunc fLogFunc;
	    };

	static vector<TLogFuncData> fLogFuncData;
	static pthread_mutex_t fLogFuncMutex;
	static bool fInited;
	static bool fIniting;
		
	//static BCLAddressLogger fLogger;
	
#ifdef USE_TCP
	static MLUCLog::TLogMsg& LogTCPAddress( MLUCLog::TLogMsg& log, const BCLAbstrAddressStruct& addr );
#endif

#ifdef USE_SCI
	static MLUCLog::TLogMsg& LogSCIAddress( MLUCLog::TLogMsg& log, const BCLAbstrAddressStruct& addr );
#endif

    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _BCLADDRESSLOGGER_HPP_
