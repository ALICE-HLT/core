/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _BCL_STACKTRACECALLBACK_H__
#define _BCL_STACKTRACECALLBACK_H__

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

// #include "BCLTypes.h"
// #include "BCLErrorCallback.hpp"
// #include <pthread.h>

#include "BCLTypes.h"
#include "BCLErrorCallback.hpp"
#include "MLUCLog.hpp"
#include "BCLAbstrAddress.hpp"
#include <errno.h>
#include <string.h>


class BCLCommunication;
struct BCLMessageStruct;
struct BCLAbstrAddressStruct;



class BCLStackTraceCallback: public BCLErrorCallback
    {
    public:

	BCLStackTraceCallback( int depth );
 	virtual ~BCLStackTraceCallback();

	virtual BCLErrorAction ConnectionTemporarilyLost( int error, BCLCommunication* com,
							  BCLAbstrAddressStruct* address );
	virtual BCLErrorAction ConnectionError( int error, BCLCommunication* com,
						BCLAbstrAddressStruct* address ); 
	virtual BCLErrorAction BindError( int error, BCLCommunication* com,
					  BCLAbstrAddressStruct* address, 
					  BCLMessageStruct* msg );
	virtual BCLErrorAction MsgSendError( int error, BCLCommunication* com,
					     BCLAbstrAddressStruct* address, 
					     BCLMessageStruct* msg );
	virtual BCLErrorAction MsgReceiveError( int error, BCLCommunication* com,
						BCLAbstrAddressStruct* address, 
						BCLMessageStruct* msg );
	virtual BCLErrorAction BlobPrepareError( int error, BCLCommunication* com,
						 BCLAbstrAddressStruct* address ); 
	virtual BCLErrorAction BlobSendError( int error, BCLCommunication* com,
					      BCLAbstrAddressStruct* address, 
					      BCLTransferID transfer );
	virtual BCLErrorAction BlobReceiveError( int error, BCLCommunication* com,
						 BCLTransferID transfer );
	virtual BCLErrorAction AddressError( int error, BCLCommunication* com,
					     BCLAbstrAddressStruct* address ); 
	virtual BCLErrorAction GeneralError( int error, BCLCommunication* com,
					     BCLAbstrAddressStruct* address ); 
	
	void PrintTrace( const char* text );
	
    protected:

	int fDepth;

	void** fBuffer;

	pthread_mutex_t fMutex;

    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _BCL_STACKTRACECALLBACK_H__
