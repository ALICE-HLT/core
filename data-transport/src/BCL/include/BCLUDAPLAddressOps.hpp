/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _BCLUDAPLADDRESSOPS_HPP_
#define _BCLUDAPLADDRESSOPS_HPP_

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: BCLUDAPLAddressOps.hpp 98 2001-07-18 09:20:22Z timm $ 
**
***************************************************************************
*/

#include "BCLUDAPLAddress.hpp"
#include <cstring>

inline bool operator == ( const BCLUDAPLAddressStruct& addr1, const BCLUDAPLAddressStruct& addr2 )
    {
    return (addr1.fSinFamily == addr2.fSinFamily) && (addr1.fSinPort == addr2.fSinPort) && 
      (addr1.fNodeID == addr2.fNodeID) && (addr1.fConnQual == addr2.fConnQual) &&
      !strncmp( reinterpret_cast<const char*>( addr1.fIAName ), 
		 reinterpret_cast<const char*>( addr2.fIAName ), 32 );
    }


/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: BCLUDAPLAddressOps.hpp 98 2001-07-18 09:20:22Z timm $ 
**
***************************************************************************
*/

#endif // _BCLUDAPLADDRESSOPS_HPP_
