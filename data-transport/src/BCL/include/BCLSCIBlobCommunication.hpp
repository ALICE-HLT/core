/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _BCLSCIBLOBCOOMMUNICATION_HPP_
#define _BCLSCIBLOBCOOMMUNICATION_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#ifdef USE_SCI


#include "BCLBlobCommunication.hpp"
#include "BCLIntSCIComHelper.hpp"
#include "BCLIntSCIBlobComMsg.hpp"
#include "BCLSCIAddress.hpp"
#include "MLUCThread.hpp"
#include "MLUCConditionSem.hpp"
extern "C" {
#include "sisci_error.h"
#include "sisci_api.h"
}


struct BCLIntSCIControlDataStruct;

class BCLSCIBlobCommunication: public BCLBlobCommunication
    {
    public:

	BCLSCIBlobCommunication();
	BCLSCIBlobCommunication( BCLMsgCommunication* msgCom );
	virtual ~BCLSCIBlobCommunication();

	virtual bool SetBlobBuffer( uint32 size, uint8* blobBuffer = NULL );

	virtual const BCLSCIAddressStruct* GetAddress()
		{
		return (BCLSCIAddressStruct*)fAddress;
		}

	void SetDMAMaxCopyBufferSize( unsigned int size )
		{
		fDMAMaxCopyBufferSize = size;
		}

	unsigned int GetDMAMaxCopyBufferSize() const
		{
		return fDMAMaxCopyBufferSize;
		}

	virtual int Bind( BCLAbstrAddressStruct* address );
	virtual int Unbind();

	virtual int Connect( BCLAbstrAddressStruct* address, bool openOnDemand = false );
	virtual int Connect( BCLAbstrAddressStruct* address, unsigned long timeout_ms, bool openOnDemand = false );
	virtual int Disconnect( BCLAbstrAddressStruct* address );
	virtual int Disconnect( BCLAbstrAddressStruct* address, unsigned long timeout_ms );

	virtual bool CanLockConnection();
	virtual int LockConnection( BCLAbstrAddressStruct* address );
	virtual int LockConnection( BCLAbstrAddressStruct* address, unsigned long timeout_ms );
	virtual int UnlockConnection( BCLAbstrAddressStruct* address );
	virtual int UnlockConnection( BCLAbstrAddressStruct* address, unsigned long timeout_ms );

	// Attach a message communication object used to send control
	// and synchronization messages between the blob transfer objects.
	// This object has to be available exclusively to this object!
	// The address passed specifies the address of the 
	virtual void SetMsgCommunication( BCLMsgCommunication* msgCom );

	// Determine the size of the remote blob buffer.
	virtual uint64 GetRemoteBlobSize( BCLAbstrAddressStruct* msgAddress, unsigned long timeout_ms = 0 );

	// Blob transfer sender side.
	// The msgAddress parameter passed to PrepareBlobTransfer is the address of the 
	// msg communication object used by the blob transfer object on the other side.
	// (See SetMsgCommunication)
	virtual BCLTransferID PrepareBlobTransfer( BCLAbstrAddressStruct* msgAddress, uint32 size,
						   unsigned long timeout = 0 );
	virtual int TransferBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, uint8* data,
				  uint64 offset, uint64 size, unsigned long timeout=0,
				  BCLErrorCallback *callback = NULL );
	virtual int TransferBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, vector<uint8*> datas,
				  vector<uint64> offsets, vector<uint64> sizes, unsigned long timeout=0,
				  BCLErrorCallback *callback = NULL );
	// These two are basically identical, but they do not wait for the notification from the receiver
	// that the transfer has been complete
	// This might lead to overtaking by other messages...
	virtual int PushBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, uint8* data,
				  uint64 offset, uint64 size, unsigned long timeout=0,
				  BCLErrorCallback *callback = NULL )
		{
		return TransferBlob( msgAddress, id, data,
				  offset, size, timeout,
				     callback );
		}
	virtual int PushBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, vector<uint8*> datas,
				  vector<uint64> offsets, vector<uint64> sizes, unsigned long timeout=0,
				  BCLErrorCallback *callback = NULL )
		{
		return TransferBlob( msgAddress, id, datas,
				     offsets, sizes, timeout,
				     callback );
		}



	// Not supported currently; Might be introduced back in later versions. 
	//virtual bool IsTransferComplete( BCLTransferID transfer );

	// Blob transfer receiver side. The transfer ID has to be transmitted by explicit 
	// user level communication between blob sender and receiver.
	virtual uint8* GetBlobData( BCLTransferID transfer );
	virtual uint64 GetBlobOffset( BCLTransferID transfer );
	virtual void ReleaseBlob( BCLTransferID transfer );

	virtual uint32 GetAddressLength();
	virtual uint32 GetComID();
	virtual BCLSCIAddressStruct* NewAddress();
	virtual void DeleteAddress( BCLAbstrAddressStruct* address );
	virtual bool AddressEquality( const BCLAbstrAddressStruct* addr1, const BCLAbstrAddressStruct* addr2 );

	virtual operator bool();

#ifndef SISCI_NO_DMA
	bool IsDMAAllowed()
		{
		return fDMA;
		}

	void AllowDMA( bool dma )
		{
		fDMA = dma;
		}
#endif

    protected:

	struct TSCIBlockData;

	int GetRemoteBlobAddress( BCLAbstrAddressStruct* msgAddress, BCLSCIAddressStruct* blobAddress, bool useTimeout, unsigned long timeout );
	int GetQueryID( uint32& id );
	int WaitForMessage( uint32 queryID, BCLIntSCIBlobComMsgStruct* msg, bool useTimeout, unsigned long timeout ); // The fDataArrivedSignal must be locked before entering this method

	int Connect( BCLAbstrAddressStruct* msgAddress, BCLSCIAddressStruct* blobAddress, bool openOnDemand, bool useTimeout, unsigned long timeout );
	int Disconnect( BCLAbstrAddressStruct* address, BCLSCIAddressStruct* blobAddress, bool useTimeout, unsigned long timeout );

	int LockConnection( BCLAbstrAddressStruct* address, bool initiator, bool useTimeout, unsigned long timeout );
	int UnlockConnection( BCLAbstrAddressStruct* address, bool initiator, bool useTimeout, unsigned long timeout );

	int GetFreeBlock( uint64 size, TSCIBlockData& block );

	BCLIntSCIControlDataStruct* fControlData;
	uint8 *fBufferAddress;
	uint64 fBufferSize;
	BCLSCIAddressStruct fSCIAddress;

	sci_error_t fSCIError;

	BCLIntSCIComHelper fComHelper;

	struct TSCIRemoteBlobAddress
	    {
		BCLAbstrAddressStruct* fMsgAddress;
		BCLSCIAddressStruct fBlobAddress;
	    };
	
	vector<TSCIRemoteBlobAddress> fRemoteBlobAddresses;


#ifndef DEBUG_NOTHREADS
	class BCLIntSCIHelperThread: public MLUCThread
	    {
	    public:

		BCLIntSCIHelperThread( BCLSCIBlobCommunication* com, void (BCLSCIBlobCommunication::*func)(void) )
			{
			fCom = com;
			fFunc = func;
			}
	    protected:

		BCLSCIBlobCommunication* fCom;
		void (BCLSCIBlobCommunication::*fFunc)(void);

		virtual void Run()
			{
			(fCom->*fFunc)();
			}
	    };

	void QuitRequestHandler();

	BCLIntSCIHelperThread fRequestThread;
#endif // DEBUG_NOTHREADS
	bool fQuitRequestHandler;
	bool fRequestHandlerQuitted;
	void RequestHandler();

	MLUCConditionSem fDataArrivedSignal;

	struct TSCIBlockData
	    {
		uint64 fBlockOffset;
		uint64 fBlockSize;
	    };

	vector<TSCIBlockData> fFreeBlocks;
	vector<TSCIBlockData> fUsedBlocks;
	pthread_mutex_t fBlockMutex;

	vector<BCLIntSCIBlobComMsgStruct> fMsgs;
	pthread_mutex_t fMsgMutex;
	uint32 fCurrentQueryID;
	uint32 fCurrentQueryCount;

#ifndef SISCI_NO_DMA
	bool fDMA;
	unsigned int fDMASizeAlign;
	unsigned int fDMAOffsetAlign;
	unsigned int fDMAAlign;
	unsigned int fDMAMaxSize;
	unsigned int fDMAMaxCopyBufferSize;

#endif

    private:
    };



#endif // USE_SCI

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _BCLSCIBLOBCOOMMUNICATION_HPP_
