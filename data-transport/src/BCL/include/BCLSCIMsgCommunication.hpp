/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _BCLSCIMSGCOMMUNICATION_HPP_
#define _BCLSCIMSGCOMMUNICATION_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#ifdef USE_SCI


#include "BCLMsgCommunication.hpp"
#include "BCLSCIAddress.hpp"
#include "BCLIntSCIControlData.hpp"
#include "BCLIntSCIComHelper.hpp"
#include "MLUCThread.hpp"
#include "MLUCConditionSem.hpp"
#include "BCLSCIComID.hpp"
extern "C" {
#include "sisci_error.h"
#include "sisci_api.h"
}

const uint32 kDefaultMsgBufferSize = 65536;

class BCLSCIMsgCommunication: public BCLMsgCommunication
    {
    public:

	BCLSCIMsgCommunication( uint32 bufferSize = 0 );
	virtual ~BCLSCIMsgCommunication();

	virtual const BCLSCIAddressStruct* GetAddress()
		{
		return (BCLSCIAddressStruct*)fAddress;
		}
	uint32 GetBufferSize()
		{
		return fMsgBufferSize;
		}

	void SetBufferSize( uint32 bufferSize )
		{
		if ( !fMsgBufferAddress )
		    fMsgBufferSize = bufferSize;
		}

	virtual int Bind( BCLAbstrAddressStruct* abstrAddress );
	virtual int Unbind();

	virtual int Connect( BCLAbstrAddressStruct* abstrAddress, bool openOnDemand = false );
	virtual int Connect( BCLAbstrAddressStruct* abstrAddress, unsigned long timeout_ms, bool openOnDemand = false );
	virtual int Disconnect( BCLAbstrAddressStruct* abstrAddress );
	virtual int Disconnect( BCLAbstrAddressStruct* abstrAddress, unsigned long timeout_ms );

	virtual bool CanLockConnection();
	virtual int LockConnection( BCLAbstrAddressStruct* address );
	virtual int LockConnection( BCLAbstrAddressStruct* address, unsigned long timeout_ms );
	virtual int UnlockConnection( BCLAbstrAddressStruct* address );
	virtual int UnlockConnection( BCLAbstrAddressStruct* address, unsigned long timeout_ms );
// 	virtual int LockConnection( BCLAbstrAddressStruct* address, BCLErrorCallback* callback = NULL );
// 	virtual int UnlockConnection( BCLAbstrAddressStruct* address, BCLErrorCallback* callback = NULL );

	virtual int Send( BCLAbstrAddressStruct* abstrAddress, BCLMessageStruct* msg, 
			  BCLErrorCallback* callback = NULL );

	virtual int Send( BCLAbstrAddressStruct* abstrAddress, BCLMessageStruct* msg, 
			  unsigned long timeout_ms, BCLErrorCallback* callback = NULL );
	
	virtual int Receive( BCLAbstrAddressStruct* abstrAddr, BCLMessageStruct*& msg, BCLErrorCallback* callback = NULL );
	virtual int Receive( BCLAbstrAddressStruct* abstrAddr, BCLMessageStruct*& msg, unsigned long timeout, BCLErrorCallback* callback = NULL );
	virtual int ReleaseMsg( BCLMessageStruct* msg );

	virtual void Reset();


	virtual uint32 GetAddressLength();
	virtual uint32 GetComID();
	virtual BCLSCIAddressStruct* NewAddress();
	virtual void DeleteAddress( BCLAbstrAddressStruct* address );
	virtual bool AddressEquality( const BCLAbstrAddressStruct* addr1, const BCLAbstrAddressStruct* addr2 );

	virtual operator bool();

    protected:
	
	virtual int Send( BCLAbstrAddressStruct* abstrAddress, BCLMessageStruct* msg, 
			  bool useTimeout, unsigned long timeout_ms, BCLErrorCallback* callback = NULL );

	    friend class BCLIntSCIComHelper;

	virtual BCLMessageStruct* NewMessage( uint32 size );
	virtual void FreeMessage( BCLMessageStruct* msg );

	virtual int Receive( BCLSCIAddressStruct* addr, BCLMessageStruct*& msg, bool useTimeout, unsigned long timeout, BCLErrorCallback* callback );

	BCLIntSCIControlDataStruct* fControlData;
	uint8 *fMsgBufferAddress;

	uint32 fMsgBufferSize;
	BCLSCIAddressStruct fSCIAddress;

	sci_error_t fSCIError;

	BCLIntSCIComHelper fComHelper;


#ifndef USE_INTERRUPT_CALLBACKS
#ifndef DEBUG_NOTHREADS
	class BCLIntSCIHelperThread: public MLUCThread
	    {
	    public:

		BCLIntSCIHelperThread( BCLSCIMsgCommunication* com, void (BCLSCIMsgCommunication::*func)(void) )
			{
			fCom = com;
			fFunc = func;
			}
	    protected:

		BCLSCIMsgCommunication* fCom;
		void (BCLSCIMsgCommunication::*fFunc)(void);

		virtual void Run()
			{
			(fCom->*fFunc)();
			}
	    };

	void QuitRequestInterruptHandler();

	BCLIntSCIHelperThread fRequestIntThread;

	// awi
#endif
	
	bool fPauseRequests;

	// awi
	// #endif // DEBUG_NOTHREADS
	bool fQuitRequestInterruptHandler;
	bool fRequestInterruptHandlerQuitted;
	void RequestInterruptHandler();
#else
	static void RequestInterruptHandler( void *arg );
	bool fRequestActive;
	pthread_mutex_t fRequestMutex;
#endif

	MLUCConditionSem<uint8> fDataArrivedSignal;

	uint32 fMsgSeqNr;

	bool fPreStatusOk;
	uint32 fPreReadIndex;
	uint32 fPreWriteIndex;
	uint32 fPreReadCount;
	uint32 fPreWriteCount;

    private:
    };




#endif // USE_SISCI

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _BCLSCIMSGCOMMUNICATION_HPP_
