/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#ifndef _BCLURLADDRESSDECODER_HPP_
#define _BCLURLADDRESSDECODER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTypes.h"
#include "MLUCString.hpp"
#include "BCLCommunication.hpp"
#include <string.h>
#include <vector>

/*
** Address URL format:
**
** sci[(msg|blob)]://<SCI Node ID>[.<Adapter-Nr>]:<Segment ID>/
** tcp[(msg|blob)]://<IP Nr>:<Port Nr>/
** udapl[(msg|blob)]://<IP Nr>:<Conn. qualifier>/
**
** Optional items (items in square brackets) are not necessary for 
** remote addresses.
** Remote Addresses cannot be specified as blob addresses.
**
*/


int BCLCheckURL( const char* address_url, bool isLocal, bool& isBlob );
int BCLDecodeLocalURL( const char* address_url, BCLCommunication*& com_obj,
		       BCLAbstrAddressStruct*& address, bool& isBlob );

int BCLDecodeRemoteURL( const char* address_url, BCLAbstrAddressStruct*& address );

void BCLFreeAddress( BCLCommunication* com_obj, BCLAbstrAddressStruct* address );
void BCLFreeAddress( BCLAbstrAddressStruct* address );

void BCLGetAllowedURLs( vector<MLUCString>& msgURLs, vector<MLUCString>& blobURLs );

void BCLEncodeAddress( BCLAbstrAddressStruct* address, bool isBlob, char*& url );
void BCLFreeEncodedAddress( char* url );

unsigned long BCLGetAddressSize( BCLAbstrAddressStruct* address );

int BCLGetEmptyAddressFromURL( const char* address_url, BCLAbstrAddressStruct*& address );

int BCLCompareAddresses( BCLAbstrAddressStruct* address1, BCLAbstrAddressStruct* address2 );


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif //  _BCLURLADDRESSDECODER_HPP_
