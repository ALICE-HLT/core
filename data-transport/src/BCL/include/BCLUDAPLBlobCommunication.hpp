#ifndef _BCLUDAPLBLOBCOMMUNICATION_HPP_
#define _BCLUDAPLBLOBCOMMUNICATION_HPP_

/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <BCLIntUDAPLComHelper.hpp>
#include <BCLBlobCommunication.hpp>
#include <BCLIntUDAPLBlobComMsg.hpp>
#include <MLUCConditionSem.hpp>


class BCLUDAPLBlobCommunication: public BCLBlobCommunication
    {
    public:

	BCLUDAPLBlobCommunication( int slotCntExp2 = -1 );
	BCLUDAPLBlobCommunication( BCLMsgCommunication* msgCom, int slotCntExp2 = -1 );
	virtual ~BCLUDAPLBlobCommunication();

	virtual const BCLUDAPLAddressStruct* GetAddress()
		{
		return (BCLUDAPLAddressStruct*)fAddress;
		}

	virtual int Bind( BCLAbstrAddressStruct* address );
	virtual int Unbind();

	virtual int Connect( BCLAbstrAddressStruct* address, bool openOnDemand = false );
	virtual int Connect( BCLAbstrAddressStruct* address, unsigned long timeout, bool openOnDemand = false );
	virtual int Disconnect( BCLAbstrAddressStruct* address );
	virtual int Disconnect( BCLAbstrAddressStruct* address, unsigned long timeout );

	virtual bool CanLockConnection();
	virtual int LockConnection( BCLAbstrAddressStruct* address );
	virtual int LockConnection( BCLAbstrAddressStruct* address, unsigned long timeout );
	virtual int UnlockConnection( BCLAbstrAddressStruct* address );
	virtual int UnlockConnection( BCLAbstrAddressStruct* address, unsigned long timeout );

	// Determine the size of the remote blob buffer.
	virtual uint64 GetRemoteBlobSize( BCLAbstrAddressStruct* msgAddress, unsigned long timeout_ms = 0 );

	// Blob transfer sender side.
	// The msgAddress parameter passed to PrepareBlobTransfer is the address of the 
	// msg communication object used by the blob transfer object on the other side.
	// (See SetMsgCommunication)
	virtual BCLTransferID PrepareBlobTransfer( BCLAbstrAddressStruct* msgAddress, uint32 size,
						   unsigned long timeout = 0 );
	virtual int TransferBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, uint8* data,
				  uint64 offset, uint64 size, unsigned long timeout=0,
				  BCLErrorCallback *callback = NULL );
	virtual int TransferBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, vector<uint8*> datas,
				  vector<uint64> offsets, vector<uint64> sizes, unsigned long timeout=0,
				  BCLErrorCallback *callback = NULL );
	// These two are basically identical, but they do not wait for the notification from the receiver
	// that the transfer has been complete
	// This might lead to overtaking by other messages...
	virtual int PushBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, uint8* data,
			      uint64 offset, uint64 size, unsigned long timeout=0,
			      BCLErrorCallback *callback = NULL );
	virtual int PushBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, vector<uint8*> datas,
			      vector<uint64> offsets, vector<uint64> sizes, unsigned long timeout=0,
			      BCLErrorCallback *callback = NULL );

	// Not supported currently; Might be introduced back in later versions. 
	//virtual bool IsTransferComplete( BCLTransferID transfer );

	virtual int WaitForReceivedTransfer( BCLTransferID& transfer, vector<uint64>& offsets, vector<uint64>& sizes, unsigned long timeout = 0 );
	virtual void AbortWaitForReceivedTransfer();

	// Blob transfer receiver side. The transfer ID has to be transmitted by explicit 
	// user level communication between blob sender and receiver.
	virtual uint8* GetBlobData( BCLTransferID transfer );
	virtual uint64 GetBlobOffset( BCLTransferID transfer );
	virtual void ReleaseBlob( BCLTransferID transfer );

	virtual uint32 GetAddressLength();
	virtual uint32 GetComID();
	virtual BCLUDAPLAddressStruct* NewAddress();
	virtual void DeleteAddress( BCLAbstrAddressStruct* address );
	virtual bool AddressEquality( const BCLAbstrAddressStruct* addr1, const BCLAbstrAddressStruct* addr2 );

	virtual operator bool();

    protected:

	    friend class BCLIntUDAPLComHelper::DataReceivedCallback<BCLUDAPLBlobCommunication>;

	struct TUDAPLRemoteBlobAddress
	    {
		BCLAbstrAddressStruct* fMsgAddress;
		BCLUDAPLAddressStruct fBlobAddress;
	    };
	
	struct TUDAPLBlockData
	    {
		uint64 fBlockOffset;
		uint64 fBlockSize;
	    };

	struct TUDAPLReceivedBlobData
	    {
		BCLTransferID fTransferID;
		unsigned long fTransferBlocksLeft;
		uint64 fBlockOffset;
		uint64 fBlockSize;
	    };


	class BCLIntUDAPLHelperThread: public MLUCThread
	    {
	    public:

		BCLIntUDAPLHelperThread( BCLUDAPLBlobCommunication* com, void (BCLUDAPLBlobCommunication::*func)(void) )
			{
			fCom = com;
			fFunc = func;
			}
	    protected:

		BCLUDAPLBlobCommunication* fCom;
		void (BCLUDAPLBlobCommunication::*fFunc)(void);

		virtual void Run()
			{
			(fCom->*fFunc)();
			}
	    };


	int Connect( BCLAbstrAddressStruct* msgAddress, BCLUDAPLAddressStruct* blobAddress, bool openOnDemand, bool useTimeout, unsigned long timeout );
	int Disconnect( BCLAbstrAddressStruct* msgAddress, BCLUDAPLAddressStruct* blobAddress, bool initiator, bool useTimeout, unsigned long timeout );

	int TransmitBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, vector<uint8*> datas,
			  vector<uint64> offsets, vector<uint64> sizes, bool waitForCompletion, 
			  unsigned long timeout=0, BCLErrorCallback *callback = NULL );

	int GetRemoteBlobAddress( BCLAbstrAddressStruct* msgAddress, BCLUDAPLAddressStruct* blobAddress, bool useTimeout, unsigned long timeout_ms );
	int GetQueryID( uint32& id );
	// Note: fDataArrivedSignal must be locked upon entry into this function.
	int WaitForMessage( uint32 queryID, BCLIntUDAPLBlobComMsgStruct* msg, bool useTimeout, unsigned long timeout_ms ); // The fDataArrivedSignal must be locked before entering this method

	int GetFreeBlock( uint64 size, TUDAPLBlockData& block );

	BCLIntUDAPLComHelper fComHelper;

	vector<TUDAPLRemoteBlobAddress> fRemoteBlobAddresses;

	MLUCConditionSem<uint8> fDataArrivedSignal;

	BCLUDAPLAddressStruct fUDAPLAddress;

	typedef MLUCVector<BCLIntUDAPLBlobComMsgStruct> TBCLIntUDAPLBlobComMsgStructContType;
	TBCLIntUDAPLBlobComMsgStructContType fMsgs;
	uint32 fCurrentQueryID;
	uint32 fCurrentQueryCount;

	static bool FindMsgByQueryID( const BCLIntUDAPLBlobComMsgStruct& msg, const uint32& reference );

	typedef vector<TUDAPLBlockData> TUDAPLBlockDataContType;
	TUDAPLBlockDataContType fFreeBlocks;
	TUDAPLBlockDataContType fUsedBlocks;
	pthread_mutex_t fBlockMutex;


	BCLIntUDAPLHelperThread fRequestThread;
	bool fQuitRequestHandler;
	bool fRequestHandlerQuitted;
	void RequestHandler();
	void QuitRequestHandler();

	virtual int DataReceived( DAT_EVD_HANDLE evd, 
				   DAT_EP_HANDLE ep, 
				   DAT_VADDR recvBuffer,
				   DAT_VLEN recvBufferSize,
				   DAT_LMR_CONTEXT recvLMRContext,
				   DAT_LMR_TRIPLET* iov, 
				   uint64 cookie, 
				   DAT_EVENT& event );
	BCLIntUDAPLComHelper::DataReceivedCallback<BCLUDAPLBlobCommunication> fDataCallback;

	uint32 fBlobReceiveCnt;

	MLUCVector<TUDAPLReceivedBlobData> fReceivedBlocks;

	MLUCConditionSem<uint8> fBlobReceivedSignal;
	bool fQuitReceivedBlobWait;

    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _BCLUDAPLBLOBCOMMUNICATION_HPP_
