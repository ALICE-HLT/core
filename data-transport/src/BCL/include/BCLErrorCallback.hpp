/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _BCL_ERRORCALLBACK_H__
#define _BCL_ERRORCALLBACK_H__

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLTypes.h"


class BCLCommunication;
struct BCLMessageStruct;
struct BCLAbstrAddressStruct;


enum BCLErrorAction { kIgnore, kAbort, kRetry };


class BCLErrorCallback
    {
    public:

	virtual ~BCLErrorCallback() {};

	virtual BCLErrorAction ConnectionTemporarilyLost( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { return kAbort; };

	virtual BCLErrorAction ConnectionError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { return kAbort; };

	virtual BCLErrorAction BindError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { return kAbort; };

	virtual BCLErrorAction LockError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { return kAbort; };

	virtual BCLErrorAction MsgSendError( int, BCLCommunication*,
					       BCLAbstrAddressStruct*, 
					     BCLMessageStruct* ) { return kAbort; };

	virtual BCLErrorAction MsgReceiveError( int, BCLCommunication*,
					       BCLAbstrAddressStruct*, 
						BCLMessageStruct* ) { return kAbort; };

	virtual BCLErrorAction BlobPrepareError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { return kAbort; };

	virtual BCLErrorAction BlobSendError( int, BCLCommunication*,
					      BCLAbstrAddressStruct*, 
					      BCLTransferID ) { return kAbort; };

	virtual BCLErrorAction BlobReceiveError( int, BCLCommunication*,
						 BCLTransferID ) { return kAbort; };

	virtual BCLErrorAction AddressError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { return kAbort; };

	virtual BCLErrorAction GeneralError( int, BCLCommunication*,
					       BCLAbstrAddressStruct* ) { return kAbort; };

    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _BCL_ERRORCALLBACK_H__
