/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _BCLNETWORKDATA_HPP_
#define _BCLNETWORKDATA_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTypes.h"
#include "BCLTypes.h"
#include "MLUCLog.hpp"

const int kOriginalByteOrderIndex = 0;
const int kCurrentByteOrderIndex = 1;

const uint8 kLittleEndian = 1;
const uint8 kBigEndian = 2;

#if defined(__i386__) or defined(__arm__) or defined(__x86_64__)
const uint8 kNativeByteOrder = kLittleEndian;
#else
#if defined(__powerpc__)
const uint8 kNativeByteOrder = kBigEndian;
#else
#error Byte format (little/big endian) currently not defined for platforms other than intel i386 compatible, x86-64 (AMD64) and arm...
#endif
#endif

const uint8 kNetworkByteOrder = kBigEndian;


struct BCLNetworkDataStruct
    {
	uint8 fDataFormats[4]; // [0] is original (at creation time), [1] is current, 
                               // [2],[3] are currently unused
	uint32 fLength;
    };

inline MLUCLog::TLogMsg& operator<<( MLUCLog::TLogMsg& log, const BCLNetworkDataStruct& nds )
    {
    unsigned int ob=nds.fDataFormats[ kOriginalByteOrderIndex ], cb=nds.fDataFormats[ kCurrentByteOrderIndex ];
#ifndef _SCALI_SIMPLE_BUGFIX1_
    return log << "( BCLNetworkData: " << MLUCLog::kDec << ob
  	       << ", " << cb 
  	       << ", " << nds.fLength << " )";
#else
    //log << "( BCLNetworkData: " << MLUCLog::kDec << ob << "," ; 
    log << "( " << MLUCLog::kDec << ob << ", " << cb << ", " << nds.fLength << " )" ; 
    return log;
#endif
    }


class BCLNetworkData
    {
    public:
	BCLNetworkData()
		{
		fData = &fBCLNetworkData;
		fData->fLength = sizeof(BCLNetworkDataStruct);
		fData->fDataFormats[kCurrentByteOrderIndex] = kNativeByteOrder;
		fData->fDataFormats[kOriginalByteOrderIndex] = kNativeByteOrder;
// #define VALGRIND_PARANOIA
#ifdef VALGRIND_PARANOIA
#warning DEBUG valgrind paranoia
		fData->fDataFormats[2] = 0;
		fData->fDataFormats[3] = 0;

#endif
		}
	BCLNetworkData( BCLNetworkData* dataClass, bool transformToNative = true )
		{
		fData = &fBCLNetworkData;
		if ( !dataClass )
		    {
		    fData->fDataFormats[kCurrentByteOrderIndex] = kNativeByteOrder;
		    fData->fDataFormats[kOriginalByteOrderIndex] = kNativeByteOrder;
		    fData->fLength = sizeof(BCLNetworkDataStruct);
		    }
		else
		    {
		    BCLNetworkDataStruct* data = dataClass->GetData();
		    if ( transformToNative && data->fDataFormats[kCurrentByteOrderIndex]!=kNativeByteOrder )
			{
			Transform( data->fLength, fData->fLength, 
				   data->fDataFormats[kCurrentByteOrderIndex],
				   kNativeByteOrder );
			fData->fDataFormats[kCurrentByteOrderIndex] = kNativeByteOrder;
			fData->fDataFormats[kOriginalByteOrderIndex] = data->fDataFormats[kOriginalByteOrderIndex];
			}
		    else
			{
			*fData = *data;
			}
		    }
		}
	BCLNetworkData( BCLNetworkDataStruct* data, bool transformToNative  = true)
		{
		fData = &fBCLNetworkData;
		if ( !data )
		    {
		    fData->fDataFormats[kCurrentByteOrderIndex] = kNativeByteOrder;
		    fData->fDataFormats[kOriginalByteOrderIndex] = kNativeByteOrder;
		    fData->fLength = sizeof(BCLNetworkDataStruct);
		    }
		else
		    {
		    if ( transformToNative && data->fDataFormats[kCurrentByteOrderIndex]!=kNativeByteOrder )
			{
			Transform( data->fLength, fData->fLength, 
				   data->fDataFormats[kCurrentByteOrderIndex],
				   kNativeByteOrder );
			fData->fDataFormats[kCurrentByteOrderIndex] = kNativeByteOrder;
			fData->fDataFormats[kOriginalByteOrderIndex] = data->fDataFormats[kOriginalByteOrderIndex];
			}
		    else
			{
			fBCLNetworkData = *data;
			}
		    }
		}
	/*virtual*/ ~BCLNetworkData() {};

	/*virtual*/ void TransformTo( uint8 format );
	/*virtual*/ void TransformToNative();
	/*virtual*/ void TransformToOriginal();

	/*virtual*/ void TransformFullyTo( uint8 format );
	/*virtual*/ void TransformFullyToNative();
	/*virtual*/ void TransformFullyToOriginal();
	
	/*virtual*/ void SetTo( BCLNetworkData* data, bool transformToNative = true );
	/*virtual*/ void SetTo( BCLNetworkDataStruct* data, bool transformToNative  = true);

	/*virtual*/ void Adopt( BCLNetworkDataStruct* data, bool transformToNative = true );
	/*virtual*/ void AdoptFully( BCLNetworkDataStruct* data, bool transformToNative = true );

	BCLNetworkDataStruct* GetData()
		{
		return fData;
		}

	uint8 GetCurrentByteOrder()
		{
		return fData->fDataFormats[kCurrentByteOrderIndex];
		}

	uint8 GetOriginalByteOrder()
		{
		return fData->fDataFormats[kOriginalByteOrderIndex];
		}

	static void Transform( uint64& data, uint8 srcFormat, uint8 destFormat )
		{
		uint8 tmp;
		if ( srcFormat == destFormat )
		    return;
		if ( srcFormat == kLittleEndian )
		    {
		    tmp = *(uint8*)&data;
		    *(uint8*)&data = *(((uint8*)&data)+7);
		    *(((uint8*)&data)+7) = tmp;
		    tmp = *(((uint8*)&data)+1);
		    *(((uint8*)&data)+1) = *(((uint8*)&data)+6);
		    *(((uint8*)&data)+6) = tmp;
		    tmp = *(((uint8*)&data)+2);
		    *(((uint8*)&data)+2) = *(((uint8*)&data)+5);
		    *(((uint8*)&data)+5) = tmp;
		    tmp = *(((uint8*)&data)+3);
		    *(((uint8*)&data)+3) = *(((uint8*)&data)+4);
		    *(((uint8*)&data)+4) = tmp;
		    }
		if ( srcFormat == kBigEndian )
		    {
		    tmp = *(uint8*)&data;
		    *(uint8*)&data = *(((uint8*)&data)+7);
		    *(((uint8*)&data)+7) = tmp;
		    tmp = *(((uint8*)&data)+1);
		    *(((uint8*)&data)+1) = *(((uint8*)&data)+6);
		    *(((uint8*)&data)+6) = tmp;
		    tmp = *(((uint8*)&data)+2);
		    *(((uint8*)&data)+2) = *(((uint8*)&data)+5);
		    *(((uint8*)&data)+5) = tmp;
		    tmp = *(((uint8*)&data)+3);
		    *(((uint8*)&data)+3) = *(((uint8*)&data)+4);
		    *(((uint8*)&data)+4) = tmp;
		    }
		}
	static void Transform( uint64 volatile& data, uint8 srcFormat, uint8 destFormat )
		{
		uint8 tmp;
		if ( srcFormat == destFormat )
		    return;
		if ( srcFormat == kLittleEndian )
		    {
		    tmp = *(uint8*)&data;
		    *(uint8*)&data = *(((uint8*)&data)+7);
		    *(((uint8*)&data)+7) = tmp;
		    tmp = *(((uint8*)&data)+1);
		    *(((uint8*)&data)+1) = *(((uint8*)&data)+6);
		    *(((uint8*)&data)+6) = tmp;
		    tmp = *(((uint8*)&data)+2);
		    *(((uint8*)&data)+2) = *(((uint8*)&data)+5);
		    *(((uint8*)&data)+5) = tmp;
		    tmp = *(((uint8*)&data)+3);
		    *(((uint8*)&data)+3) = *(((uint8*)&data)+4);
		    *(((uint8*)&data)+4) = tmp;
		    }
		if ( srcFormat == kBigEndian )
		    {
		    tmp = *(uint8*)&data;
		    *(uint8*)&data = *(((uint8*)&data)+7);
		    *(((uint8*)&data)+7) = tmp;
		    tmp = *(((uint8*)&data)+1);
		    *(((uint8*)&data)+1) = *(((uint8*)&data)+6);
		    *(((uint8*)&data)+6) = tmp;
		    tmp = *(((uint8*)&data)+2);
		    *(((uint8*)&data)+2) = *(((uint8*)&data)+5);
		    *(((uint8*)&data)+5) = tmp;
		    tmp = *(((uint8*)&data)+3);
		    *(((uint8*)&data)+3) = *(((uint8*)&data)+4);
		    *(((uint8*)&data)+4) = tmp;
		    }
		}
	static void Transform( const uint64& srcData, uint64& destData, uint8 srcFormat, uint8 destFormat )
		{
		if ( srcFormat == destFormat )
		    {
		    destData = srcData;
		    return;
		    }
		if ( srcFormat == kLittleEndian )
		    {
		    *(((uint8*)(&destData))+0) = *(((uint8*)(&srcData))+7);
		    *(((uint8*)(&destData))+1) = *(((uint8*)(&srcData))+6);
		    *(((uint8*)(&destData))+2) = *(((uint8*)(&srcData))+5);
		    *(((uint8*)(&destData))+3) = *(((uint8*)(&srcData))+4);
		    *(((uint8*)(&destData))+4) = *(((uint8*)(&srcData))+3);
		    *(((uint8*)(&destData))+5) = *(((uint8*)(&srcData))+2);
		    *(((uint8*)(&destData))+6) = *(((uint8*)(&srcData))+1);
		    *(((uint8*)(&destData))+7) = *(((uint8*)(&srcData))+0);
		    }
		if ( srcFormat == kBigEndian )
		    {
		    *(((uint8*)(&destData))+0) = *(((uint8*)(&srcData))+7);
		    *(((uint8*)(&destData))+1) = *(((uint8*)(&srcData))+6);
		    *(((uint8*)(&destData))+2) = *(((uint8*)(&srcData))+5);
		    *(((uint8*)(&destData))+3) = *(((uint8*)(&srcData))+4);
		    *(((uint8*)(&destData))+4) = *(((uint8*)(&srcData))+3);
		    *(((uint8*)(&destData))+5) = *(((uint8*)(&srcData))+2);
		    *(((uint8*)(&destData))+6) = *(((uint8*)(&srcData))+1);
		    *(((uint8*)(&destData))+7) = *(((uint8*)(&srcData))+0);
		    }
		}
	static void Transform( const uint64& srcData, uint64 volatile& destData, uint8 srcFormat, uint8 destFormat )
		{
		if ( srcFormat == destFormat )
		    {
		    destData = srcData;
		    return;
		    }
		if ( srcFormat == kLittleEndian )
		    {
		    *(((uint8*)(&destData))+0) = *(((uint8*)(&srcData))+7);
		    *(((uint8*)(&destData))+1) = *(((uint8*)(&srcData))+6);
		    *(((uint8*)(&destData))+2) = *(((uint8*)(&srcData))+5);
		    *(((uint8*)(&destData))+3) = *(((uint8*)(&srcData))+4);
		    *(((uint8*)(&destData))+4) = *(((uint8*)(&srcData))+3);
		    *(((uint8*)(&destData))+5) = *(((uint8*)(&srcData))+2);
		    *(((uint8*)(&destData))+6) = *(((uint8*)(&srcData))+1);
		    *(((uint8*)(&destData))+7) = *(((uint8*)(&srcData))+0);
		    }
		if ( srcFormat == kBigEndian )
		    {
		    *(((uint8*)(&destData))+0) = *(((uint8*)(&srcData))+7);
		    *(((uint8*)(&destData))+1) = *(((uint8*)(&srcData))+6);
		    *(((uint8*)(&destData))+2) = *(((uint8*)(&srcData))+5);
		    *(((uint8*)(&destData))+3) = *(((uint8*)(&srcData))+4);
		    *(((uint8*)(&destData))+4) = *(((uint8*)(&srcData))+3);
		    *(((uint8*)(&destData))+5) = *(((uint8*)(&srcData))+2);
		    *(((uint8*)(&destData))+6) = *(((uint8*)(&srcData))+1);
		    *(((uint8*)(&destData))+7) = *(((uint8*)(&srcData))+0);
		    }
		}
	static void Transform( const uint64 volatile& srcData, uint64 volatile& destData, uint8 srcFormat, uint8 destFormat )
		{
		if ( srcFormat == destFormat )
		    {
		    destData = srcData;
		    return;
		    }
		if ( srcFormat == kLittleEndian )
		    {
		    *(((uint8*)(&destData))+0) = *(((uint8*)(&srcData))+7);
		    *(((uint8*)(&destData))+1) = *(((uint8*)(&srcData))+6);
		    *(((uint8*)(&destData))+2) = *(((uint8*)(&srcData))+5);
		    *(((uint8*)(&destData))+3) = *(((uint8*)(&srcData))+4);
		    *(((uint8*)(&destData))+4) = *(((uint8*)(&srcData))+3);
		    *(((uint8*)(&destData))+5) = *(((uint8*)(&srcData))+2);
		    *(((uint8*)(&destData))+6) = *(((uint8*)(&srcData))+1);
		    *(((uint8*)(&destData))+7) = *(((uint8*)(&srcData))+0);
		    }
		if ( srcFormat == kBigEndian )
		    {
		    *(((uint8*)(&destData))+0) = *(((uint8*)(&srcData))+7);
		    *(((uint8*)(&destData))+1) = *(((uint8*)(&srcData))+6);
		    *(((uint8*)(&destData))+2) = *(((uint8*)(&srcData))+5);
		    *(((uint8*)(&destData))+3) = *(((uint8*)(&srcData))+4);
		    *(((uint8*)(&destData))+4) = *(((uint8*)(&srcData))+3);
		    *(((uint8*)(&destData))+5) = *(((uint8*)(&srcData))+2);
		    *(((uint8*)(&destData))+6) = *(((uint8*)(&srcData))+1);
		    *(((uint8*)(&destData))+7) = *(((uint8*)(&srcData))+0);
		    }
		}
	static void Transform( uint32& data, uint8 srcFormat, uint8 destFormat )
		{
		uint8 tmp;
		if ( srcFormat == destFormat )
		    return;
		if ( srcFormat == kLittleEndian )
		    {
		    tmp = *(uint8*)&data;
		    *(uint8*)&data = *(((uint8*)&data)+3);
		    *(((uint8*)&data)+3) = tmp;
		    tmp = *(((uint8*)&data)+1);
		    *(((uint8*)&data)+1) = *(((uint8*)&data)+2);
		    *(((uint8*)&data)+2) = tmp;
		    }
		if ( srcFormat == kBigEndian )
		    {
		    tmp = *(uint8*)&data;
		    *(uint8*)&data = *(((uint8*)&data)+3);
		    *(((uint8*)&data)+3) = tmp;
		    tmp = *(((uint8*)&data)+1);
		    *(((uint8*)&data)+1) = *(((uint8*)&data)+2);
		    *(((uint8*)&data)+2) = tmp;
		    }
		}
	static void Transform( uint32 volatile& data, uint8 srcFormat, uint8 destFormat )
		{
		uint8 tmp;
		if ( srcFormat == destFormat )
		    return;
		if ( srcFormat == kLittleEndian )
		    {
		    tmp = *(uint8*)&data;
		    *(uint8*)&data = *(((uint8*)&data)+3);
		    *(((uint8*)&data)+3) = tmp;
		    tmp = *(((uint8*)&data)+1);
		    *(((uint8*)&data)+1) = *(((uint8*)&data)+2);
		    *(((uint8*)&data)+2) = tmp;
		    }
		if ( srcFormat == kBigEndian )
		    {
		    tmp = *(uint8*)&data;
		    *(uint8*)&data = *(((uint8*)&data)+3);
		    *(((uint8*)&data)+3) = tmp;
		    tmp = *(((uint8*)&data)+1);
		    *(((uint8*)&data)+1) = *(((uint8*)&data)+2);
		    *(((uint8*)&data)+2) = tmp;
		    }
		}
	static void Transform( const uint32& srcData, uint32& destData, uint8 srcFormat, uint8 destFormat )
		{
		if ( srcFormat == destFormat )
		    {
		    destData = srcData;
		    return;
		    }
		if ( srcFormat == kLittleEndian )
		    {
		    *(((uint8*)(&destData))+0) = *(((uint8*)(&srcData))+3);
		    *(((uint8*)(&destData))+1) = *(((uint8*)(&srcData))+2);
		    *(((uint8*)(&destData))+2) = *(((uint8*)(&srcData))+1);
		    *(((uint8*)(&destData))+3) = *(((uint8*)(&srcData))+0);
		    }
		if ( srcFormat == kBigEndian )
		    {
		    *(((uint8*)(&destData))+0) = *(((uint8*)(&srcData))+3);
		    *(((uint8*)(&destData))+1) = *(((uint8*)(&srcData))+2);
		    *(((uint8*)(&destData))+2) = *(((uint8*)(&srcData))+1);
		    *(((uint8*)(&destData))+3) = *(((uint8*)(&srcData))+0);
		    }
		}
	static void Transform( const uint32& srcData, uint32 volatile& destData, uint8 srcFormat, uint8 destFormat )
		{
		if ( srcFormat == destFormat )
		    {
		    destData = srcData;
		    return;
		    }
		if ( srcFormat == kLittleEndian )
		    {
		    *(((uint8*)(&destData))+0) = *(((uint8*)(&srcData))+3);
		    *(((uint8*)(&destData))+1) = *(((uint8*)(&srcData))+2);
		    *(((uint8*)(&destData))+2) = *(((uint8*)(&srcData))+1);
		    *(((uint8*)(&destData))+3) = *(((uint8*)(&srcData))+0);
		    }
		if ( srcFormat == kBigEndian )
		    {
		    *(((uint8*)(&destData))+0) = *(((uint8*)(&srcData))+3);
		    *(((uint8*)(&destData))+1) = *(((uint8*)(&srcData))+2);
		    *(((uint8*)(&destData))+2) = *(((uint8*)(&srcData))+1);
		    *(((uint8*)(&destData))+3) = *(((uint8*)(&srcData))+0);
		    }
		}
	static void Transform( const uint32 volatile& srcData, uint32 volatile& destData, uint8 srcFormat, uint8 destFormat )
		{
		if ( srcFormat == destFormat )
		    {
		    destData = srcData;
		    return;
		    }
		if ( srcFormat == kLittleEndian )
		    {
		    *(((uint8*)(&destData))+0) = *(((uint8*)(&srcData))+3);
		    *(((uint8*)(&destData))+1) = *(((uint8*)(&srcData))+2);
		    *(((uint8*)(&destData))+2) = *(((uint8*)(&srcData))+1);
		    *(((uint8*)(&destData))+3) = *(((uint8*)(&srcData))+0);
		    }
		if ( srcFormat == kBigEndian )
		    {
		    *(((uint8*)(&destData))+0) = *(((uint8*)(&srcData))+3);
		    *(((uint8*)(&destData))+1) = *(((uint8*)(&srcData))+2);
		    *(((uint8*)(&destData))+2) = *(((uint8*)(&srcData))+1);
		    *(((uint8*)(&destData))+3) = *(((uint8*)(&srcData))+0);
		    }
		}
	static void Transform( uint16& data, uint8 srcFormat, uint8 destFormat )
		{
		uint8 tmp;
		if ( srcFormat == destFormat )
		    return;
		if ( srcFormat == kLittleEndian )
		    {
		    tmp = *(uint8*)&data;
		    *(uint8*)&data = *(((uint8*)&data)+1);
		    *(((uint8*)&data)+1) = tmp;
		    }
		if ( srcFormat == kBigEndian )
		    {
		    tmp = *(uint8*)&data;
		    *(uint8*)&data = *(((uint8*)&data)+1);
		    *(((uint8*)&data)+1) = tmp;
		    }
		}
	static void Transform( uint16 volatile& data, uint8 srcFormat, uint8 destFormat )
		{
		uint8 tmp;
		if ( srcFormat == destFormat )
		    return;
		if ( srcFormat == kLittleEndian )
		    {
		    tmp = *(uint8*)&data;
		    *(uint8*)&data = *(((uint8*)&data)+1);
		    *(((uint8*)&data)+1) = tmp;
		    }
		if ( srcFormat == kBigEndian )
		    {
		    tmp = *(uint8*)&data;
		    *(uint8*)&data = *(((uint8*)&data)+1);
		    *(((uint8*)&data)+1) = tmp;
		    }
		}
	static void Transform( const uint16& srcData, uint16& destData, uint8 srcFormat, uint8 destFormat )
		{
		if ( srcFormat == destFormat )
		    {
		    destData = srcData;
		    return;
		    }
		if ( srcFormat == kLittleEndian )
		    {
		    *(((uint8*)(&destData))+0) = *(((uint8*)(&srcData))+1);
		    *(((uint8*)(&destData))+1) = *(((uint8*)(&srcData))+0);
		    }
		if ( srcFormat == kBigEndian )
		    {
		    *(((uint8*)(&destData))+0) = *(((uint8*)(&srcData))+1);
		    *(((uint8*)(&destData))+1) = *(((uint8*)(&srcData))+0);
		    }
		}
	static void Transform( const uint16& srcData, uint16 volatile& destData, uint8 srcFormat, uint8 destFormat )
		{
		if ( srcFormat == destFormat )
		    {
		    destData = srcData;
		    return;
		    }
		if ( srcFormat == kLittleEndian )
		    {
		    *(((uint8*)(&destData))+0) = *(((uint8*)(&srcData))+1);
		    *(((uint8*)(&destData))+1) = *(((uint8*)(&srcData))+0);
		    }
		if ( srcFormat == kBigEndian )
		    {
		    *(((uint8*)(&destData))+0) = *(((uint8*)(&srcData))+1);
		    *(((uint8*)(&destData))+1) = *(((uint8*)(&srcData))+0);
		    }
		}
	static void Transform( const uint16 volatile& srcData, uint16 volatile& destData, uint8 srcFormat, uint8 destFormat )
		{
		if ( srcFormat == destFormat )
		    {
		    destData = srcData;
		    return;
		    }
		if ( srcFormat == kLittleEndian )
		    {
		    *(((uint8*)(&destData))+0) = *(((uint8*)(&srcData))+1);
		    *(((uint8*)(&destData))+1) = *(((uint8*)(&srcData))+0);
		    }
		if ( srcFormat == kBigEndian )
		    {
		    *(((uint8*)(&destData))+0) = *(((uint8*)(&srcData))+1);
		    *(((uint8*)(&destData))+1) = *(((uint8*)(&srcData))+0);
		    }
		}

	static void Transform( uint8&, uint8, uint8 )
		{
		}
	static void Transform( uint8 volatile&, uint8, uint8 )
		{
		}
	static void Transform( const uint8& srcData, uint8& destData, uint8, uint8 )
		{
		destData = srcData;
		}

	static void Transform( const uint8& srcData, uint8 volatile& destData, uint8, uint8 )
		{
		destData = srcData;
		}
	static void Transform( const uint8 volatile& srcData, uint8 volatile& destData, uint8, uint8 )
		{
		destData = srcData;
		}

	static void Transform( float&, uint8, uint8 )
		{
		}
	static void Transform( float volatile&, uint8, uint8 )
		{
		}
	static void Transform( const float& srcData, float& destData, uint8, uint8 )
		{
		destData = srcData;
		}
	static void Transform( const float& srcData, float volatile& destData, uint8, uint8 )
		{
		destData = srcData;
		}
	static void Transform( const float volatile& srcData, float volatile& destData, uint8, uint8 )
		{
		destData = srcData;
		}

	static void Transform( double&, uint8, uint8 )
		{
		}
	static void Transform( double volatile&, uint8, uint8 )
		{
		}
	static void Transform( const double& srcData, double& destData, uint8, uint8 )
		{
		destData = srcData;
		}
	static void Transform( const double& srcData, double volatile& destData, uint8, uint8 )
		{
		destData = srcData;
		}
	static void Transform( const double volatile& srcData, double volatile& destData, uint8, uint8 )
		{
		destData = srcData;
		}
    protected:

	BCLNetworkDataStruct* fData;
	
	BCLNetworkDataStruct fBCLNetworkData;
	
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _BCLNETWORKDATA_HPP_
