/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _BCLTCPBLOBCOOMMUNICATION_HPP_
#define _BCLTCPBLOBCOOMMUNICATION_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#ifdef USE_TCP


#include "BCLBlobCommunication.hpp"
#include "BCLIntTCPComHelper.hpp"
#include "BCLIntTCPBlobComMsg.hpp"
#include "BCLIntTCPBlobComHeader.hpp"
#include "BCLTCPAddress.hpp"
#include "MLUCThread.hpp"
#include "MLUCConditionSem.hpp"
#include "MLUCVector.hpp"
#include "MLUCMutex.hpp"
#include "MLUCDynamicAllocCache.hpp"


#define TCPBLOB_DEFAULT_TRANSFERBLOCKSIZE 4

class BCLTCPBlobCommunication: public BCLBlobCommunication
    {
    public:

	BCLTCPBlobCommunication( int slotCntExp2 = -1 );
	BCLTCPBlobCommunication( BCLMsgCommunication* msgCom, int slotCntExp2 = -1 );
	virtual ~BCLTCPBlobCommunication();

	void SetTransferBlockSize( uint32 blockSize )
		{
		fTransferBlockSize = blockSize;
		}

	uint64 GetTransferBlockSize()
		{
		return fTransferBlockSize;
		}

	virtual const BCLTCPAddressStruct* GetAddress()
		{
		return (BCLTCPAddressStruct*)fAddress;
		}

	virtual int Bind( BCLAbstrAddressStruct* address );
	virtual int Unbind();

	virtual int Connect( BCLAbstrAddressStruct* address, bool openOnDemand = false );
	virtual int Connect( BCLAbstrAddressStruct* address, unsigned long timeout, bool openOnDemand = false );
	virtual int Disconnect( BCLAbstrAddressStruct* address );
	virtual int Disconnect( BCLAbstrAddressStruct* address, unsigned long timeout );
	virtual int InterruptBlobConnection( BCLAbstrAddressStruct* address );


	virtual bool CanLockConnection();
	virtual int LockConnection( BCLAbstrAddressStruct* address );
	virtual int LockConnection( BCLAbstrAddressStruct* address, unsigned long timeout );
	virtual int UnlockConnection( BCLAbstrAddressStruct* address );
	virtual int UnlockConnection( BCLAbstrAddressStruct* address, unsigned long timeout );

	// Attach a message communication object used to send control
	// and synchronization messages between the blob transfer objects.
	// This object has to be available exclusively to this object!
	// The address passed specifies the address of the of the 
	// msg communication object used by the blob transfer object on the other side.
	virtual void SetMsgCommunication( BCLMsgCommunication* msgCom );

	// Determine the size of the remote blob buffer.
	virtual uint64 GetRemoteBlobSize( BCLAbstrAddressStruct* msgAddress, unsigned long timeout_ms = 0 );

	// Blob transfer sender side.
	// The msgAddress parameter passed to PrepareBlobTransfer is the address of the 
	// msg communication object used by the blob transfer object on the other side.
	// (See SetMsgCommunication)
	virtual BCLTransferID PrepareBlobTransfer( BCLAbstrAddressStruct* msgAddress, uint64 size,
						   unsigned long timeout = 0 );
	virtual int TransferBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, uint8* data,
				  uint64 offset, uint64 size, unsigned long timeout=0,
				  BCLErrorCallback *callback = NULL );
	virtual int TransferBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, vector<uint8*> datas,
				  vector<uint64> offsets, vector<uint64> sizes, unsigned long timeout=0,
				  BCLErrorCallback *callback = NULL );
	// These two are basically identical, but they do not wait for the notification from the receiver
	// that the transfer has been complete
	// This might lead to overtaking by other messages...
	virtual int PushBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, uint8* data,
				  uint64 offset, uint64 size, unsigned long timeout=0,
				  BCLErrorCallback *callback = NULL );
	virtual int PushBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, vector<uint8*> datas,
				  vector<uint64> offsets, vector<uint64> sizes, unsigned long timeout=0,
				  BCLErrorCallback *callback = NULL );

	// Not supported currently; Might be introduced back in later versions. 
	//virtual bool IsTransferComplete( BCLTransferID transfer );

	virtual int WaitForReceivedTransfer( BCLTransferID& transfer, vector<uint64>& offsets, vector<uint64>& sizes, unsigned long timeout = 0 );
	virtual void AbortWaitForReceivedTransfer();

	// Blob transfer receiver side. The transfer ID has to be transmitted by explicit 
	// user level communication between blob sender and receiver.
	virtual uint8* GetBlobData( BCLTransferID transfer );
	virtual uint64 GetBlobOffset( BCLTransferID transfer );
	virtual void ReleaseBlob( BCLTransferID transfer );

	virtual uint32 GetAddressLength();
	virtual uint32 GetComID();
	virtual BCLTCPAddressStruct* NewAddress();
	virtual void DeleteAddress( BCLAbstrAddressStruct* address );
	virtual bool AddressEquality( const BCLAbstrAddressStruct* addr1, const BCLAbstrAddressStruct* addr2 );

	virtual operator bool();

    protected:

	    friend class BCLIntTCPBlobComHelper;

	struct TTCPBlockData;

	int Connect( BCLAbstrAddressStruct* msgAddress, BCLTCPAddressStruct* blobAddress, bool openOnDemand, bool useTimeout, unsigned long timeout );

	virtual int Disconnect( BCLAbstrAddressStruct* msgAddress, BCLTCPAddressStruct* blobAddress, bool initiator, bool useTimeout, unsigned long timeout_ms );

	int GetRemoteBlobAddress( BCLAbstrAddressStruct* msgAddress, BCLTCPAddressStruct* blobAddress, bool useTimeout, unsigned long timeout_ms );
	int GetQueryID( uint64& id );
	// Note: fDataArrivedSignal must be locked upon entry into this function.
	int WaitForMessage( uint32 queryID, BCLIntTCPBlobComMsgStruct* msg, bool useTimeout, unsigned long timeout_ms ); // The fDataArrivedSignal must be locked before entering this method

	int GetFreeBlock( uint64 size, TTCPBlockData& block );

	virtual int TransferBlob( bool waitForCompletion, BCLAbstrAddressStruct* msgAddress, BCLTransferID id, vector<uint8*> datas,
				  vector<uint64> offsets, vector<uint64> sizes, unsigned long timeout=0,
				  BCLErrorCallback *callback = NULL );


	BCLTCPAddressStruct fTCPAddress;


	BCLIntTCPBlobComHelper fComHelper;

	struct TTCPRemoteBlobAddress
	    {
		BCLAbstrAddressStruct* fMsgAddress;
		BCLTCPAddressStruct fBlobAddress;
	    };
	
	vector<TTCPRemoteBlobAddress> fRemoteBlobAddresses;
	MLUCMutex fRemoteBlobAddressMutex;


#ifndef DEBUG_NOTHREADS
	class BCLIntTCPHelperThread: public MLUCThread
	    {
	    public:

		BCLIntTCPHelperThread( BCLTCPBlobCommunication* com, void (BCLTCPBlobCommunication::*func)(void) )
			{
			fCom = com;
			fFunc = func;
			}
	    protected:

		BCLTCPBlobCommunication* fCom;
		void (BCLTCPBlobCommunication::*fFunc)(void);

		virtual void Run()
			{
			(fCom->*fFunc)();
			}
	    };

	void QuitRequestHandler();

	BCLIntTCPHelperThread fRequestThread;
#endif // DEBUG_NOTHREADS
	bool fQuitRequestHandler;
	bool fRequestHandlerQuitted;
	void RequestHandler();

	MLUCConditionSem<uint8> fDataArrivedSignal;

	struct TTCPBlockData
	    {
		uint64 fBlockOffset;
		uint64 fBlockSize;
	    };

	typedef vector<TTCPBlockData> TTTCPBlockDataContType;
	//typedef list<TTCPBlockData> TTTCPBlockDataContType;
	TTTCPBlockDataContType fFreeBlocks;
	TTTCPBlockDataContType fUsedBlocks;
	pthread_mutex_t fBlockMutex;

	MLUCConditionSem<uint8> fBlobReceivedSignal;

	struct TTCPReceivedBlobData
	    {
		BCLTransferID fTransferID;
		unsigned long fTransferBlocksLeft;
		uint64 fBlockOffset;
		uint64 fBlockSize;
	    };

	MLUCVector<TTCPReceivedBlobData> fReceivedBlobs;

	bool fQuitReceivedBlobWait;
		

	typedef MLUCVector<BCLIntTCPBlobComMsgStruct> TBCLIntTCPBlobComMsgStructContType;
	//typedef list<BCLIntTCPBlobComMsgStruct> TBCLIntTCPBlobComMsgStructContType;
	TBCLIntTCPBlobComMsgStructContType fMsgs;
// 	pthread_mutex_t fMsgMutex;
	uint32 fCurrentQueryID;
	uint32 fCurrentQueryCount;

	static bool FindMsgByQueryID( const BCLIntTCPBlobComMsgStruct& msg, const uint32& reference );

	uint32 fTransferBlockSize;

	MLUCDynamicAllocCache fHeaderAllocCache;

	struct TCPBlobProgressData
	    {
		uint32 fBlockCnt;
		uint32 fCompletionIndicator;
		BCLIntTCPBlobComHeaderStruct fHeader1;
		BCLIntTCPBlobComHeaderStruct* fHeaders;
		uint64 fBytesRead;
		int fSocket;
	    };

	typedef MLUCVector<TCPBlobProgressData> TTCPBlobProgressDataContType;
	//typedef list<TCPBlobProgressData> TTCPBlobProgressDataContType;
	TTCPBlobProgressDataContType fBlobProgress;

	bool NewConnection( BCLIntTCPComHelper::TAcceptedConnectionData& con );
	
	int NewData( BCLIntTCPComHelper::TAcceptedConnectionData& con );

	bool ConnectionTimeout( BCLIntTCPComHelper::TAcceptedConnectionData& con );

	void ConnectionClosed( BCLIntTCPComHelper::TAcceptedConnectionData& con );

	static bool FindBlobProgressBySocket( const TCPBlobProgressData& msgProgress, const int& refSocket );

    private:
    };



#endif

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _BCLTCPBLOBCOOMMUNICATION_HPP_
