/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _BCLINTSCIHELPER_HPP_
#define _BCLINTSCIHELPER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLSCIAddress.hpp"
#include "BCLIntSCIControlData.hpp"
#include "BCLCommunication.hpp"
extern "C" {
#include "sisci_error.h"
#include "sisci_api.h"
}
#include <pthread.h>
#include <vector>

class BCLSCIMsgCommunication;
class BCLSCIBlobCommunication;


class BCLIntSCIComHelper: public BCLIntComHelper
    {
    protected:
	struct TSCIConnectionData;

    public:

	BCLIntSCIComHelper( BCLCommunication* com );
	virtual ~BCLIntSCIComHelper();

	int Bind( uint32 size, BCLSCIAddressStruct* address );
	int Unbind();

	int Connect( BCLSCIAddressStruct* address, BCLErrorCallback* callback, bool useTimeout, uint32 timeout_ms, bool openOnDemand = false );
	int Disconnect( BCLSCIAddressStruct* address, BCLErrorCallback* callback, bool useTimeout, uint32 timeout_ms );

	int LockConnection( BCLSCIAddressStruct* address, BCLErrorCallback* callback, bool useTimeout, uint32 timeout_ms );
	int UnlockConnection( BCLSCIAddressStruct* address, BCLErrorCallback* callback, bool useTimeout, uint32 timeout_ms );

	operator bool();

	BCLIntSCIControlDataStruct* GetControlData()
		{
		return fControlData;
		}

	uint8* GetDataBuffer()
		{
		return fBufferAddress;
		}

	uint32 GetSegmentID()
		{
		return fSegmentID;
		}

	uint8* GetSegmentAddress()
		{
		return fSegmentAddress;
		}

	sci_local_segment_t GetSegment()
		{
		return fSegment;
		}
	
	uint16 GetLocalNodeID( uint16 adapterNr );
	int ConnectToRemote( BCLSCIAddressStruct* address, TSCIConnectionData* connectData, bool now, bool store, bool& wasStored, BCLErrorCallback* callback, bool useTimeout, uint32 timeout_ms );
	void DisconnectFromRemote( TSCIConnectionData* connectData, BCLErrorCallback* callback, bool useTimeout, uint32 timeout_ms );

	int LockConnection( BCLSCIAddressStruct* address, TSCIConnectionData* connectData, BCLErrorCallback* callback, bool useTimeout, uint32 timeout_ms );
	int UnlockConnection( BCLSCIAddressStruct* address, TSCIConnectionData* connectData, BCLErrorCallback* callback, bool useTimeout, uint32 timeout_ms );

#ifndef SISCI_NO_DMA
	int CreateLocalSegment( TSCIConnectionData* connectData, unsigned int size );
	int DestroyLocalSegment( TSCIConnectionData* connectData );
#endif

	int StartSequence( TSCIConnectionData* connectData, bool useTimeout, uint32 timeout_ms );
	int CheckSequence( TSCIConnectionData* connectData, bool useTimeout, uint32 timeout_ms );

	int UpdateCachedCD( BCLSCIAddressStruct* address, BCLIntSCIControlDataStruct* remoteCachedCD, bool useTimeout, uint32 timeout_ms );

	int WriteBackCachedCD( BCLSCIAddressStruct* address, BCLIntSCIControlDataStruct* cachedCD );

	static const uint32 kInvalidSender = (uint32)-1;

    protected:

	    friend class BCLSCIMsgCommunication;
	    friend class BCLSCIBlobCommunication;
	

	uint32 fSegmentID;

// 	struct TSCIControlData
// 	    {
// 		uint32 fRequestInterruptNr;
// 		uint32 fSenderRequest;
// 		uint32 fCurrentSender;
// 		uint32 fReadIndex;
// 		uint32 fWriteIndex;
// 		uint32 fReadCount;
// 		uint32 fWriteCount;
// 		uint32 fSize;
// 		uint8  fData[0];
// 	    };

	uint8* fSegmentAddress;

	BCLIntSCIControlDataStruct* fControlData;
	uint8 *fBufferAddress;

	sci_desc_t fSCIDesc;
	bool fIsSCIOpen;
	sci_error_t fSCIError;
	sci_local_interrupt_t fRequestInterrupt;
	sci_local_segment_t   fSegment;
	sci_map_t fSegmentMap;

	uint32 fBufferSize;
	unsigned int fConnectionTimeout; // in millisec.

	struct TSCIConnectionData
	    {
		sci_desc_t fSCIDesc;
		BCLSCIAddressStruct fAddress;
		sci_remote_segment_t fRemoteSegment;
		sci_map_t fRemoteMap;
		sci_sequence_t fMapSequence;
		uint8* fRemoteAddr;
		sci_remote_interrupt_t fRemoteRequestInt;
		BCLIntSCIControlDataStruct* fRemoteCD;
		uint8* fRemoteMsgs;
		uint32 fUsageCount;
		uint32 fRemoteSize;
		bool fLocked;
		BCLIntSCIControlDataStruct fRemoteCachedCD;
#ifndef SISCI_NO_DMA
		sci_local_segment_t fLocalSegment;
		sci_map_t fLocalMap;
		uint8* fLocalAddr;
		uint32 fLocalSize;
		uint32 fLocalUsage;
#endif
	    };

	vector<TSCIConnectionData> fConnections;


	BCLSCIAddressStruct* fAddress;

	BCLCommunication* fCom;
#ifdef USE_INTERRUPT_CALLBACKS
	void (*fRequestInterruptCallback)( void* arg );
	void* fRequestInterruptData;

	void SetRequestCallback( void (*reqCB)( void*), void* cbArg );
	
	static sci_callback_action_t IntRequestInterruptCallback( void* arg, sci_local_interrupt_t interrupt, sci_error_t status );
#endif

    private:
    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _BCLINTSCIHELPER_HPP_
