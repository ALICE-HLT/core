/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _BCLMSGCOMMUNICATION_HPP_
#define _BCLMSGCOMMUNICATION_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLCommunication.hpp"

class BCLBlobCommunication;
class BCLAbstrAddressStruct;
class BCLMessageStruct;
class BCLErrorCallback;

class BCLMsgCommunication: public BCLCommunication
    {
    public:

	BCLMsgCommunication();
	virtual ~BCLMsgCommunication();

	//virtual int LockConnection( BCLAbstrAddressStruct* address, BCLErrorCallback* callback ) = 0;
	//virtual int UnlockConnection( BCLAbstrAddressStruct* address, BCLErrorCallback* callback ) = 0;

	virtual int Send( BCLAbstrAddressStruct* address, BCLMessageStruct* msg, 
			  BCLErrorCallback* callback = NULL ) = 0;
	virtual int Send( BCLAbstrAddressStruct* address, BCLMessageStruct* msg, 
			  unsigned long timeout_ms, BCLErrorCallback* callback = NULL ) = 0;
	
	virtual int Receive( BCLAbstrAddressStruct* addr, BCLMessageStruct*& msg, BCLErrorCallback* callback = NULL ) = 0;
	virtual int Receive( BCLAbstrAddressStruct* addr, BCLMessageStruct*& msg, unsigned long timeout_ms, BCLErrorCallback* callback = NULL ) = 0;
	virtual int ReleaseMsg( BCLMessageStruct* msg ) = 0;

	virtual void Reset() = 0;

	virtual uint32 GetComType() const
		{
		return kMsgComType;
		}
	
    protected:

	// Called by Receive to allocate memory space for a message
	// that is to be received...
	virtual BCLMessageStruct* NewMessage( uint32 size ) = 0;
	// Called by ReleaseMsg to free a message received (and allocated
	// by NewMessage above)
	virtual void FreeMessage( BCLMessageStruct* msg ) = 0;



    private:
    };

const int kBlobMsg = 0x8000;




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _BCLMSGCOMMUNICATION_HPP_
