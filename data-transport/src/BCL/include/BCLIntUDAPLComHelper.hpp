#ifndef _BCLINTUDAPLCOMHELPER_HPP_
#define _BCLINTUDAPLCOMHELPER_HPP_

/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <BCLCommunication.hpp>
#include <BCLUDAPLAddress.hpp>
#include <MLUCThread.hpp>
#include <MLUCVector.hpp>
#include <MLUCConditionSem.hpp>
#ifdef UDAPL_RECEIVE_NOTIFICATION_WORKAROUND
#include <MLUCThread.hpp>
#endif
#include <MLUCString.hpp>
extern "C"
{
#include    "dat/udat.h"
}

#ifndef DAPL_PROVIDER
#define DAPL_PROVIDER "OpenIB-ib0"
#endif


class BCLUDAPLMsgCommunication;
class BCLUDAPLBlobCommunication;
class BCLCommuniation;



class BCLIntUDAPLComHelper: public BCLIntComHelper
    {
    protected:



	    friend class BCLUDAPLMsgCommunication;
	    friend class BCLUDAPLBlobCommunication;

	class DataReceiveCallback;
	    friend class DataReceiveCallback;

	enum { kBlobTransferDTOCookie = 1, kBlockVectorSendDTOCookie = 2 /*, kBlockVectorRecvDTOCookie = 3*/, kMsgSendDTOCookie } TDTOCookies;

	struct ConnectionData
	    {
		BCLUDAPLAddressStruct fAddress;
		DAT_EP_HANDLE fEP;
		DAT_EVD_HANDLE fDataEVD;
		DAT_EVD_HANDLE fConnEVD;
		DAT_RMR_TRIPLET fMsgRMRTriplet;
		DAT_RMR_TRIPLET fBlobRMRTriplet;
		unsigned fUsageCount;
	    };
	typedef ConnectionData* PConnectionData;

#ifdef UDAPL_RECEIVE_NOTIFICATION_WORKAROUND
	class ReceiveNotificationHelperThread;
	friend class ReceiveNotificationHelperThread;
#endif

	struct AcceptedConnectionData
	    {
		DAT_EVD_HANDLE fRecvEVD;
		DAT_EVD_HANDLE fReqEVD;
		DAT_EVD_HANDLE fConnEVD;
		DAT_EP_HANDLE fEP;
		uint8* fRecvBufferAlloc;
		DAT_VADDR fRecvBuffer;
		DAT_VLEN fRecvBufferSize;
		DAT_LMR_HANDLE fRecvLMRHandle;
		DAT_LMR_CONTEXT fRecvLMRContext;
		DAT_RMR_CONTEXT fRecvRMRContext;
	        DAT_LMR_TRIPLET* fIOV;
#ifdef UDAPL_RECEIVE_NOTIFICATION_WORKAROUND
		ReceiveNotificationHelperThread* fReceiveThread;
#endif
		uint64 fLastCookie;
	    };

	class DataReceiveCallbackBase
	    {
	    public:

		virtual int DataReceived( DAT_EVD_HANDLE evd, 
					   DAT_EP_HANDLE ep, 
					   DAT_VADDR recvBuffer,
					   DAT_VLEN recvBufferSize,
					   DAT_LMR_CONTEXT recvLMRContext,
					   DAT_LMR_TRIPLET* iov,
					   uint64 cookie, 
					   DAT_EVENT& event ) = 0;

	    };

	template<class TCallee>
	class DataReceivedCallback: public DataReceiveCallbackBase
	    {
	    public:

		DataReceivedCallback( TCallee* com )
			{
			fCom = com;
			}

		virtual int DataReceived( DAT_EVD_HANDLE evd, 
					   DAT_EP_HANDLE ep, 
					   DAT_VADDR recvBuffer,
					   DAT_VLEN recvBufferSize,
					   DAT_LMR_CONTEXT recvLMRContext,
					   DAT_LMR_TRIPLET* iov, 
					   uint64 cookie, 
					   DAT_EVENT& event )
			{
			return fCom->DataReceived( evd, ep, recvBuffer, recvBufferSize, recvLMRContext, iov, cookie, event );
			}

	    protected:
		
		TCallee* fCom;

	    };


    public:

	BCLIntUDAPLComHelper( BCLCommunication* com );
	~BCLIntUDAPLComHelper();

	int Bind( uint8* blobRecvBuffer, uint32 blobRecvSize, uint32 msgBufferRecvSize, uint32 sendBufferSize, BCLUDAPLAddressStruct* address, DataReceiveCallbackBase* drc );
	int Unbind();

	int Connect( BCLUDAPLAddressStruct* address, bool useTimeout, uint32 timeout_ms, bool openOnDemand );
	int Disconnect( BCLUDAPLAddressStruct* address, bool useTimeout, uint32 timeout_ms );

	int ConnectToRemote( ConnectionData* connectData, bool now, bool useTimeout, uint32 timeout_ms );
	int DisconnectFromRemote( ConnectionData* connectData, bool useTimeout, uint32 timeout_ms, bool error=false, bool asyncError=false );

	operator bool();

    protected:

	int DisconnectFromRemote( bool lock, bool& removed, ConnectionData* connectData, bool useTimeout, uint32 timeout_ms, bool error, bool asyncError );

	void EventLoop();

	BCLCommunication *fCom;

	DataReceiveCallbackBase* fDataReceivedCallback;

	unsigned long fConnectionTimeout;

	DAT_IA_HANDLE fIA;
	DAT_EVD_HANDLE fEVD;
	DAT_PZ_HANDLE fPZ;
	DAT_CNO_HANDLE fCNO;
	DAT_EVD_HANDLE fConnReqEVD;
	// 	DAT_EVD_HANDLE fConnEVD;
	DAT_EVD_HANDLE fRecvEVD;
	DAT_EVD_HANDLE fReqEVD;
	DAT_EVD_HANDLE fEvtLoopEVD;
	DAT_PSP_HANDLE fPSP;

	BCLUDAPLAddressStruct fAddress;

	uint8* fRecvBufferAlloc;
	unsigned long fRecvBufferSizeAlloc;
	DAT_VADDR fRecvBuffer;
	DAT_VLEN fRecvBufferSize;
	DAT_LMR_HANDLE fRecvLMRHandle;
	DAT_LMR_CONTEXT fRecvLMRContext;
        DAT_RMR_CONTEXT fRecvRMRContext;

	uint8* fMsgRecvBufferAlloc;
	unsigned long fMsgRecvBufferSizeAlloc;
	DAT_VADDR fMsgRecvBuffer;
	DAT_VLEN fMsgRecvBufferSize;
	DAT_LMR_HANDLE fMsgRecvLMRHandle;
	DAT_LMR_CONTEXT fMsgRecvLMRContext;
	DAT_RMR_CONTEXT fMsgRecvRMRContext;

	uint8* fSendBufferAlloc;
	unsigned long fSendBufferSizeAlloc;
	DAT_VADDR fSendBuffer;
	DAT_VLEN fSendBufferSize;
	DAT_LMR_HANDLE fSendLMRHandle;
	DAT_LMR_CONTEXT fSendLMRContext;
        DAT_RMR_CONTEXT fSendRMRContext;

	pthread_mutex_t fAcceptedConnectionMutex;
	MLUCVector<AcceptedConnectionData> fAcceptedConnections;

	pthread_mutex_t fRemovedConnectionMutex;
	MLUCVector<AcceptedConnectionData> fRemovedConnections;

	pthread_mutex_t fConnectionMutex;
	MLUCVector<ConnectionData> fConnections;

	static bool ConnectionSearchFunc( const ConnectionData& el, const PConnectionData& searchData );

	typedef uint32 BlockPositionSpecifier;

	class EventLoopThread: public MLUCThread
	    {
	    public:

		EventLoopThread( BCLIntUDAPLComHelper* comHelper )
			{
			fComHelper = comHelper;
			}

		virtual void Run()
			{
			if ( fComHelper )
			    fComHelper->EventLoop();
			}
		

	    protected:
		BCLIntUDAPLComHelper* fComHelper;
	    };

	EventLoopThread fEventLoopThread;
	bool fQuitEventLoop;
	bool fEventLoopQuitted;


	static const unsigned gkConnectionBackLogs;
	static const unsigned gkEventQueueSize;
	static const unsigned gkMaxMessageSize;
	static const unsigned gkMaxRDMASize;
	static const unsigned gkMaxIOVs;
	static const unsigned gkPrivateMsgBufferSize;


#ifdef UDAPL_RECEIVE_NOTIFICATION_WORKAROUND

	MLUCConditionSem<uint8> fReceiveThreadStartedSignal;

	struct AcceptedConnectionData;

	class ReceiveNotificationHelperThread: public MLUCThread
	    {
	    public:

		ReceiveNotificationHelperThread( DAT_EVD_HANDLE evd, 
						 DAT_EP_HANDLE ep, 
						 DAT_VADDR recvBuffer,
						 DAT_VLEN recvBufferSize,
						 DAT_LMR_CONTEXT recvLMRContext,
						 DAT_LMR_TRIPLET* iov,
						 DataReceiveCallbackBase* dataReceivedCallback,
						 MLUCConditionSem<uint8>& startSignal );
	      virtual ~ReceiveNotificationHelperThread();
		
		bool Stop();
	    protected:

		virtual void Run();

		MLUCConditionSem<uint8>& fStartSignal;
		pthread_mutex_t fInUseLock;

		DAT_EVD_HANDLE fEVD;
		DAT_EP_HANDLE fEP;
		DAT_VADDR fRecvBuffer;
		DAT_VLEN fRecvBufferSize;
		DAT_LMR_CONTEXT fRecvLMRContext;
	        DAT_LMR_TRIPLET* fIOV;

	      DataReceiveCallbackBase* fDataReceivedCallback;

		bool fQuit;

	      uint64 fLastCookie;

	    };
	
#endif


    private:
    };


MLUCString DatStrError (DAT_RETURN ret_value);




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _BCLINTUDAPLCOMHELPER_HPP_
