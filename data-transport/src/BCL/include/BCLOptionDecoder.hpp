/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#ifndef _BCLOPTIONDECODER_HPP_
#define _BCLOPTIONDECODER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTypes.h"
#include "BCLCommunication.hpp"
#include <string>
#include <vector>

/*
** Option format:
** comtype.param=value
** comtype can be any combination of tcp or sci and msg or blob.
**
*/

int BCLApplyOption( const char* option, BCLCommunication* com_obj );

void BCLGetAllowedOptions( vector<const char*>& options, vector<const char*>& explanations );


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif //  _BCLOPTIONDECODER_HPP_
