/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _BCLBLOBCOMMUNICATION_HPP_
#define _BCLBLOBCOMMUNICATION_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "BCLCommunication.hpp"
#include "BCLErrorCallback.hpp"
#include <vector>

class BCLMsgCommunication;


class BCLBlobCommunication: public BCLCommunication
    {
    public:

	BCLBlobCommunication();
	BCLBlobCommunication( BCLMsgCommunication* msgCom );
	virtual ~BCLBlobCommunication();

	virtual bool SetBlobBuffer( uint64 size, uint8* blobBuffer = NULL );
	uint8* GetBlobBuffer()
		{
		return fBlobBuffer;
		}

	void SetReplyTimeout( unsigned long timeout_ms ) // 0 disables the timeout
		{
		fReplyTimeout = timeout_ms;
		}
      
	unsigned long GetBlobBufferSize()
		{
		return fBlobBufferSize;
		}

	// Attach a message communication object used to send control
	// and synchronization messages between the blob transfer objects.
	// This object has to be available exclusively to this object!
	// The address passed specifies the address of the 
	virtual void SetMsgCommunication( BCLMsgCommunication* msgCom );

	// Determine the size of the remote blob buffer.
	virtual uint64 GetRemoteBlobSize( BCLAbstrAddressStruct* msgAddress, unsigned long timeout_ms = 0 ) = 0;

	// Blob transfer sender side.
	// The msgAddress parameter passed to PrepareBlobTransfer is the address of the 
	// msg communication object used by the blob transfer object on the other side.
	// (See SetMsgCommunication)
	virtual BCLTransferID PrepareBlobTransfer( BCLAbstrAddressStruct* msgAddress, uint64 size,
						   unsigned long timeout = 0 ) = 0;
	// uint32 wait = kDontCare, might be added again later...
	virtual int TransferBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, uint8* data,
				  uint64 offset, uint64 size, unsigned long timeout=0,
				  BCLErrorCallback *callback = NULL ) = 0;
	// This is for a transfer where the data to be transferred in one transfer is scattered
	// over multiple memory blocks.
	// If the offsets vector contains less elements than the other two vectrors (data, sizes) the 
	// remaining offsets will be calculated, so that the blocks will be adjacent in the target
	// memory. This means that if the offsets vector is empty, all the blocks will be placed
	// together, starting at the beginning of the transfer area.
	// uint32 wait = kDontCare, might be added again later...
	virtual int TransferBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, vector<uint8*> data,
				  vector<uint64> offsets, vector<uint64> sizes, unsigned long timeout=0,
				  BCLErrorCallback *callback = NULL ) = 0;

	// These two are basically identical, but they do not wait for the notification from the receiver
	// that the transfer has been complete
	// This might lead to overtaking by other messages...
	virtual int PushBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, uint8* data,
				  uint64 offset, uint64 size, unsigned long timeout=0,
				  BCLErrorCallback *callback = NULL ) = 0;
	virtual int PushBlob( BCLAbstrAddressStruct* msgAddress, BCLTransferID id, vector<uint8*> datas,
				  vector<uint64> offsets, vector<uint64> sizes, unsigned long timeout=0,
				  BCLErrorCallback *callback = NULL ) = 0;

	// Not supported currently; Might be introduced back in later versions. 
	//virtual bool IsTransferComplete( BCLTransferID transfer ) = 0;

	void EnableReceiveNotification( bool enable )
		{
		fReceiveNotificationEnabled = enable;
		}

	virtual int WaitForReceivedTransfer( BCLTransferID& transfer, vector<uint64>& offsets, vector<uint64>& sizes, unsigned long timeout = 0 ) = 0;

	virtual void AbortWaitForReceivedTransfer() = 0;

	// Blob transfer receiver side. The transfer ID has to be transmitted by explicit 
	// user level communication between blob sender and receiver or by use of the WaitForReceivedTransfer
	// method (after enabling receive notification).
	virtual uint8* GetBlobData( BCLTransferID transfer ) = 0;
	virtual uint64 GetBlobOffset( BCLTransferID transfer ) = 0;
	virtual void ReleaseBlob( BCLTransferID transfer ) = 0;

	virtual uint32 GetComType() const
		{
		return kBlobComType;
		}

	virtual int InterruptBlobConnection( BCLAbstrAddressStruct* address ) = 0;

    protected:

	uint8* fBlobBuffer;
	uint64 fBlobBufferSize;
	bool fBlobBufferAllocated;

	unsigned long fReplyTimeout;  // in milliseconds.

	class BCLBlobErrorCallback: public BCLErrorCallback
	    {
	    public:

		virtual ~BCLBlobErrorCallback() {};
		
		BCLBlobErrorCallback( BCLBlobCommunication* blobCom )
			{
			fBlobCom = blobCom;
			}

	    protected:
		
		BCLBlobCommunication* fBlobCom;
		
	    };

	BCLBlobErrorCallback fMsgErrorCallback;
	BCLMsgCommunication* fMsgCom;

	bool fReceiveNotificationEnabled;

	    friend class BCBBlobErrorCallback;
	
    private:
    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _BCLBLOBCOMMUNICATION_HPP_
