###########################################################################
##
## $Author$ - Initial Version by Timm Morten Steinbeck
##
## $Id$ 
##
###########################################################################

# Define the following variables depending on
# wether you want to build shared and/or static
# version of this library.
BUILD_SHARED_LIBS=1
BUILD_STATIC_LIBS=1


# The name of the target we compile for
# This is a combination of OS and CPU.
# Currently this only sets the target directory for the
# compiled library files.
ifndef TARGET
ifdef PRODUCTION
BUILD:=release
else
BUILD:=debug
endif

ARCH := $(shell uname -m)
SYSTEM := $(shell uname -s)
TARGET := $(SYSTEM)-$(ARCH)-$(BUILD)
endif


# Set this to the top-level directory of the MLUC library.
# This has to be an absolute path. TOPDIR is set to the name
# of the BCL top-level directory.
MLUCTOPDIR=$(TOPDIR)/../MLUC


ifndef PRODUCTION
DEBUG := 1
#PARANOIDWARNINGS := 1
endif

###########################################################################
# You should not need to set anything beyond here...
###########################################################################

BCLTOPDIR=$(TOPDIR)


include $(MLUCTOPDIR)/conf/Makefile.extern.conf
include $(TOPDIR)/conf/Makefile.extern.conf


DOCDIR=doc
DOCPATH=$(TOPDIR)/$(DOCDIR)
SRCPATH=$(TOPDIR)/src
OBJDIR=$(TOPDIR)/lib/$(TARGET)
HTMLDIR=$(DOCPATH)/html
LATEXDIR=$(DOCPATH)/tex

SYSTEMLIBS=$(BCLLIBDIR) $(BCLLIBS) $(MLUCLIBDIR) $(MLUCLIBS) 


INCLUDEDIR=$(BCLINCLUDEDIR)
INCLUDEPATH=$(BCLINCLUDEPATH)



SRCSUF=cpp
HEADERSUF=hpp
VSTDLSUF=vstdl
OBJSUF=o
HTMLSUF=html
KDOCSUF=kdoc
SOPREF=lib
SOSUF=so
APREF=lib
ASUF=a

SONAME_FULLVERSION=$(SOPREF)$(BASENAME).$(SOSUF).$(MAJORVERSION).$(MINORVERSION).$(REVISION)
SONAME=$(OBJDIR)/$(SONAME_FULLVERSION)
SONAME_MAJORVERSION=$(SOPREF)$(BASENAME).$(SOSUF).$(MAJORVERSION)
SONAME_NOVERSION=$(SOPREF)$(BASENAME).$(SOSUF)
ANAME=$(OBJDIR)/$(APREF)$(BASENAME).$(ASUF)
KDOCNAME=$(TOPDIR)/$(BASENAME).$(KDOCSUF)
CHANGELOG=$(DOCPATH)/ChangeLog


SONAMELINK=$(SONAME_MAJORVERSION)
MLUCLIBLINK=$(SOPREF)$(MLUCBASENAME).$(SOSUF).$(MLUCMAJORVERSION)
MLUCLIBLINKTARGET=$(MLUCLIBPATH)/$(MLUCLIBLINK)




CC=g++
LD=g++

CCOPT := -fPIC -Wall -W
LDOPT := -fPIC -Wall -W
ifdef DEBUG
DBG := -ggdb3
CCOPT := $(CCOPT) $(DBG)
LDOPT := $(LDOPT) $(DBG)
#DEBUGDEFINES=-DDEBUG -DPARANOID  -DVALGRIND_PARANOIA
DEBUGDEFINES=-DVALGRIND_PARANOIA
else
OPT := -O3 -march=pentium3 -mmmx -msse -mfpmath=sse,387
CCOPT := $(CCOPT) $(OPT)
LDOPT := $(LDOPT) $(OPT)
DEBUGDEFINES=
endif

ifdef PARANOIDWARNINGS
WARN := -Wfloat-equal -Wshadow -Wunreachable-code
CCOPT := $(CCOPT) $(WARN)
LDOPT := $(LDOPT) $(WARN)
endif

INCLUDE=$(BCLINCLUDE)

# includes passed to the compiler but NOT to makedepend
LIBINCLUDE=$(MLUCINCLUDE) $(BCLINCLUDE)

#defines to use
DEFINES=$(BCLDEFINES) $(DEBUGDEFINES)

LDSOOPT=-Wl,-soname,$(SONAME_MAJORVERSION) -shared
AR=ar
AROPT=rv
KDOC=kdoc



###########################################################################
##
## $Author$ - Initial Version by Timm Morten Steinbeck
##
## $Id$ 
##
###########################################################################

