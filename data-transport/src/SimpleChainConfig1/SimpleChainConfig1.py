
##
## This file is property of and copyright by the Technical Computer
## Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
## University, Heidelberg, Germany, 2004
## This file has been written by Timm Morten Steinbeck, 
## timm@kip.uni-heidelberg.de
##
##
## See the file license.txt for details regarding usage, modification,
## distribution and warranty.
## Important: This file is provided without any warranty, including
## fitness for any particular purpose.
##
##
## Newer versions of this file's package will be made available from 
## http://www.ti.uni-hd.de/HLT/software/software.html 
## or the corresponding page of the Heidelberg Alice HLT group.
##


import sys, os, os.path, re, time, string, types, thread, threading, array
import pprint

class Runner(threading.Thread):
    def __init__(self, func, param ):
        threading.Thread.__init__(self)
        self.fFunc = func
        self.fParam = param

    def run(self):
        #print self.fNode,self.fCmd
        #print "ssh "+self.fNode+" "+self.fCmd
        self.fFunc( self.fParam )




import traceback

def MakeLocalURL( url ):
    parts = url.split( "/" )
    parts[2] = ":"+parts[2].split( ":" )[1]
    return string.join( parts, "/" )

def AddItem( dict, key, item ):
    if not dict.has_key(key):
        dict[key] = set()
    dict[key].add( item )
    

class ChainConfigException (Exception):
    def __init__(self, value):
        self.fValue = value
    def __str__(self):
        return repr(self.fValue)


class Node:
    def __init__( self, ID, hostname, platform ):
        self.fID = ID
        self.fHostname = hostname
        self.fPlatform = platform

    def GetID( self ):
        return self.fID

    def GetHostname( self ):
        return self.fHostname

    def GetPlatform( self ):
        return self.fPlatform

    def __str__(self):
        return "ID: " + self.fID + " hostname: " + self.fHostname + " platform: " + str(self.fPlatform)

class NodeGroup:
    def __init__( self, ID, groupMasterNode ):
        self.fMembers = []
        self.fID = ID
        self.fGroupMasterNode = groupMasterNode
        self.fLevel = 0
        self.fParent = None
        self.fSlaveControlSocket = None
        self.fListenSocket = None
        
    def Add( self, member ):
        self.fMembers.append( member )
        
    def SetGroupParent( self, parent ):
        self.fParent = parent
        
    def SetSlaveControlSocket( self, socket ):
        self.fSlaveControlSocket = socket
        
    def SetListenSocket( self, socket ):
        self.fListenSocket = socket
        
    def GetID( self ):
        return self.fID
        
    def GetMembers( self ):
        return self.fMembers[:]

    def IsMember( self, ID ):
        return ID in self.fMembers
        #return self.fMembers.count(ID)>0
            
    def GetGroupMasterNode( self ):
        return self.fGroupMasterNode
        
    def GetGroupParent( self ):
        return self.fParent
        
    def GetLevel( self ):
        return self.fLevel

    def GetSlaveControlSocket( self ):
        return self.fSlaveControlSocket
        
#    def GetControlAddress( self ):
#        return "tcpmsg://"+self.fGroupMasterNode+":"+( "%d" % ( self.fSlaveControlSocket ) )
        
    def GetListenSocket( self ):
        return self.fListenSocket
        
#    def GetListenAddress( self ):
#        return "tcpmsg://"+self.fGroupMasterNode+":"+( "%d" % ( self.fListenSocket ) )

    def __str__(self):
        string = "Members: " + str(self.fMembers)
        string = string + "\nID: " + self.fID + ", Group master node: " + self.fGroupMasterNode
        string = string + "\nLevel: " + str(self.fLevel) + ", Parent: " + str(self.fParent)
        string = string + "\nSlaveControlSocket: " + str(self.fSlaveControlSocket) + ", ListenSocket: " + str(self.fListenSocket)
        return string
        

    

class Process:
    def __init__( self, ID, type, cmdLine, configParams, parentList, nodeList, shmType, blockSize, blockCount, delay, singleton, filteredParents, eventModulo, eventTypeCode, internalID=None, processGroup=None, processGroupNode=None, processGroupNodeNdx=None, parentGroupOutput=None ):
        self.fID = ID
        self.fType = type
        self.fComponent = 0
        self.fCmdLine = cmdLine
        self.fDelay = delay
        self.fLevel = -1
        #if parentList == []:
        #    self.fParentList = None
        #else:
        self.fParentList = parentList[:]
        self.fParentList.sort()
        self.fNodeList = nodeList[:]
        # self.fNodeList.sort() # Sorting not possble because of association with instance commands!!!
        self.fControlURL = {}
        self.fInternalIDCache = internalID
        if shmType == "invalid":
            self.fShmType = None
            self.fBlockSize = None
            self.fBlockCount = None
        else:
            self.fShmType = shmType
            self.fBlockSize = blockSize
            self.fBlockCount = blockCount
        self.fConfigParams = configParams

        self.fSingleton = singleton
        self.fFilteredParents = filteredParents
        self.fEventModulo = eventModulo
        self.fEventTypeCode = eventTypeCode
        self.fProcessGroup = processGroup
        self.fProcessGroupNode = processGroupNode
        self.fProcessGroupNodeNdx = processGroupNodeNdx
        self.fParentGroupOutput = parentGroupOutput

    def __str__(self):
        string = "\n\nID: " + self.fIDName + ", Type: " + self.fType + ", Cmd: "  + str(self.fCmdLine)
        string = string + "\nParents: " + str(self.fParentList)
        string = string + "\nNodes: " + str(self.fNodeList)
        if self.fShmType!=None:
            string = string + "\nShmType: " + self.fShmType + ", Blocksize: " + str(self.fBlockSize) + ", Blockcount: " + str(self.fBlockCount)
        else:
            string = string+"\nNo shm"
        return string

    def Singleton( self, singleton=None ):
        retval = self.fSingleton
        if singleton!=None:
            self.fSingleton = singleton
        return retval

    def HasFilteredParents( self ):
        return len(self.fFilteredParents)>0

    def GetEventModulo( self ):
        return self.fEventModulo

    def GetEventTypeCode( self ):
        return self.fEventTypeCode

    def IsInProcessGroup( self, groupID=None ):
        if groupID==None:
            return self.fProcessGroup!=None
        else:
            return self.fProcessGroup!=None and self.fProcessGroup.GetID()==groupID

    def GetProcessGroup( self ):
        return self.fProcessGroup

    def GetProcessGroupID( self ):
        if self.fProcessGroup!=None:
            return self.fProcessGroup.GetID()
        return None

    def GetProcessGroupNode( self ):
        return self.fProcessGroupNode

    #def GetProcessGroupNodeNodeNdx( self ):
    def GetProcessGroupNodeNdx( self ):
        return self.fProcessGroupNodeNdx

    def GetParentGroupOutput( self ):
        return self.fParentGroupOutput


    ### def GetProcessGroupNodeNdx( self ):
    ###     if self.fProcessGroup==None:
    ###         return None
    ###     nodes = self.fProcessGroup.GetNodes()
    ###     ndx=0
    ###     cnt=0
    ###     for n in nodes:
    ###         ### if n==self.fProcessGroupNode:
    ###         ###     ### if ndx==self.fProcessGroupNodeNdx:
    ###         ###     ###     ### return cnt
    ###         ###     ### ndx += 1
    ###         ### cnt += 1
    ###     return None

    def IsParentFiltered( self, ID, filterType=None ):
        if filterType==None:
            return ID in self.fFilteredParents.keys()
        else:
            return (ID in self.fFilteredParents.keys()) and self.fFilteredParents[ID]==filterType

    def GetParentFilterType( self, ID ):
        if not ID in self.fFilteredParents.keys():
            return "none"
        else:
            return self.fFilteredParents[ID]

    def ReplaceParent( self, origParentID, newParentList ):
        if not origParentID in self.fParentList:
            return
        while self.fParentList.count(origParentID)>1:
            self.fParentList.remove( origParentID )
        ndx = self.fParentList.index( origParentID )
        self.fParentList = self.fParentList[:ndx] + newParentList + self.fParentList[ndx+1:]
        if origParentID in self.fFilteredParents.keys():
            for npi in newParentList:
                self.fFilteredParents[npi] = self.fFilteredParents[origParentID]
            del(self.fFilteredParents[origParentID])

    def SetControlURL( self, nodeIndex, url ):
        self.fControlURL[nodeIndex] = url

    def GetControlURL( self, nodeIndex ):
        if self.fControlURL.has_key( nodeIndex ):
            return self.fControlURL[nodeIndex]
        else:
            return ""

    def GetConfigParams( self ):
        return self.fConfigParams
        
    def GetID( self ):
        return self.fID

    def GetParents( self ):
        if self.fParentList!=None:
            return self.fParentList[:]
        else:
            return [] # None

    def GetDelay( self ):
        return self.fDelay

    def GetLevel( self ):
        return self.fLevel

    def SetLevel( self, level ):
        self.fLevel = level

    def HasParent( self, potentialParent ):
        return self.fParentList.count(potentialParent)>0
        #for p in self.fParentList:
        #    if p == potentialParent:
        #        return 1
        #return 0

    def GetNodes( self ):
        if self.fNodeList!=None:
            return self.fNodeList[:]
        else:
            return [] # None

    def SetNodes( self, nodeList ):
        self.fNodeList = nodeList[:]

    def IsOnNode( self, potentialNode ):
        return potentialNode in self.fNodeList
        #return self.fNodeList.count(potentialNode)>0
        #for n in self.fNodeList:
        #    if n == potentialNode:
        #        return 1
        #return 0

    def IsFullyOnNode( self, potentialNode ):
        return len(self.fNodeList)==self.fNodeList.count(potentialNode)
        #for n in self.fNodeList:
        #    if n != potentialNode:
        #        return 0
        #return 1

    def ProcCntOnNode( self, potentialNode ):
        return self.fNodeList.count(potentialNode)
        #cnt=0
        #for n in self.fNodeList:
        #    if n == potentialNode:
        #        cnt=cnt+1
        #return cnt

    def GetNodeList( self ): # Unique, each node exactly once
        return list(set(self.fNodeList))
        #nodes = []
        #for n in self.fNodeList:
        #    if nodes.count(n)<1:
        #        nodes.append( n )
        #return nodes

    def FindNodeIndex( self, ID, ndx ):
        cnt=0
        b = 0
        for n in self.fNodeList:
            #print "FindNodeIndex, n, ID, b, ndx, cnt:",n, ID, b, ndx, cnt
            if n == ID:
                if b==ndx:
                    return cnt
                else:
                    b=b+1
            cnt=cnt+1
        nodeStr = "["
        for n in self.fNodeList[:-1]:
            nodeStr = nodeStr+" "+n+","
        nodeStr = nodeStr+" "+self.fNodeList[-1]+" ]"
        raise Exception( "Process "+self.fID+" unable to find node index for node '"+ID+( "' at index %d - nodes: " % (ndx) )+nodeStr  )

    def FindNodeNodeIndex( self, ndx ):
        node = self.fNodeList[ndx]
        count=0
        i=0
        while i<ndx:
            if self.fNodeList[i]==node:
                count += 1
            i += 1
        return count

    def GetCmd( self, index = -1 ):
        if self.fCmdLine.has_key( index ):
            return self.fCmdLine[index]
        else:
            return self.fCmdLine[-1]

    def GetType( self ):
        return self.fType

    def GetShmType( self ):
        if self.fShmType!=None:
            return self.fShmType
        else:
            return "invalid"

    def GetBlockSize( self ):
        return self.fBlockSize

    def GetBlockCount( self ):
        return self.fBlockCount



class ProcessGroup:
    def __init__( self, ID, nodes, processIDs, outputProcesses ):
        self.fID = ID
        self.fNodes = nodes[:]
        self.fProcessIDs = processIDs[:]
        self.fOutputProcesses = outputProcesses[:]
        self.fFullOutputProcesses = None

    def GetID( self ):
        return self.fID

    def GetNodes( self ):
        return self.fNodes[:]

    def SetNodes( self, nodes ):
        self.fNodes = nodes[:]

    def GetProcessIDs( self ):
        return self.fProcessIDs
    
    def GetOutputProcesses( self ):
        return self.fOutputProcesses[:]

    def GetProcessPrefix( self, nodeIndex ):
        #if node in self.fNodes:
        #    ndx=0
        #    cnt=0
        #    for n in self.fNodes:
        #        if n==node:
        #            if cnt==nodeIndex:
        #                break
        #            cnt += 1
        #        ndx += 1
        #    #return self.fID+"-"+node+"-"+self.FormatNodeIndex(nodeIndex)+"-"
        #    return self.fID+"-"+self.FormatNodeIndex(ndx)+"-"
        return self.fID+"-"+self.FormatNodeIndex(nodeIndex)+"-"

    def GetProcessPrefixes( self ):
        pl = {}
        cnt=0
        for node in self.fNodes:
            pl[node] = self.GetProcessPrefix( cnt )
            cnt += 1
        return pl

    def GetInternalProcessID( self, procID, config ):
        return config.fProcesses[procID].fInternalIDCache
#        for proc in self.fProcessIDs:
#            # nodeDir = {}
#            ndx = 0
#            for node in self.fNodes:
#                # ndx = self.GetNodeIndex(nodeDir,node)
#                if procID==self.GetProcessPrefix( ndx )+proc:
#                    return proc
#                ndx += 1
#        return None
                

    def GetFullOutputProcessList( self, config ):
        pl = []
        for proc in self.fOutputProcesses:
            opl = []
            ndx=0
            for node in self.fNodes:
                if hasattr(config, 'fProcessIDMap'):
                    opl.append( config.fProcessIDMap[self.GetProcessPrefix( ndx )+proc] )
                else:
                    opl.append( self.GetProcessPrefix( ndx )+proc )
                ndx += 1
            pl.append( opl )
        return pl

    def GetFullOutputProcessListFlat( self, config ):
        if self.fFullOutputProcesses != None:
            return self.fFullOutputProcesses
        opl = []
        ndx=0
        for node in self.fNodes:
            for proc in self.fOutputProcesses:
                if hasattr(config, 'fProcessIDMap'):
                    opl.append( config.fProcessIDMap[self.GetProcessPrefix( ndx )+proc] )
                else:
                    opl.append( self.GetProcessPrefix( ndx )+proc )
            ndx += 1
        self.fFullOutputProcesses = opl
        return opl

    def FormatNodeIndex( self, ndx ):
        return "%03u" % ndx
    
    def GetNodeNodeIndex( self, nodeDir, node ):
        if not nodeDir.has_key(node):
            nodeDir[node] = 0
        ret = nodeDir[node]
        nodeDir[node] += 1
        return ret

    def GetNodeIndexDict( self, nodeDir, node ):
        nodeNodeIndex = self.GetNodeNodeIndex( nodeDir, node )
        ndx=0
        cnt=0
        for n in self.fNodes:
            if n==node:
                if ndx==nodeNodeIndex:
                    return cnt
                ndx += 1
            cnt += 1
        return None

    def GetNodeIndex( self, node, nodeNodeIndex ):
        ndx=0
        cnt=0
        for n in self.fNodes:
            if n==node:
                if ndx==nodeNodeIndex:
                    return cnt
                ndx += 1
            cnt += 1
        return None


class SimpleChainConfig:
    def __init__( self, name ):
        self.fName = name
        #self.fNodes = []
        self.fNodes = {}
        self.fNodeGroups = {}
        #self.fProcesses = []
        self.fProcesses = {}
        self.fProcessGroups = {}
        self.fVerbosity = "0x39"
        self.fDefaultDelay_ms = 2000
        self.fDefaultBridgeDelay_ms = 1000
        self.fFilePublisherNames= [ "ControlledFilePublisher", "FilePublisher" ]
        self.fChildProcesses = {}
        self.fNodeProcesses = {}
        self.fNodeParentList = {}
        self.fNodeGroupParentList = {}
        self.fTotalDelay = {}
        self.fNumericMaps = 0

    def __str__(self):
        string = "Confguration name: " + self.fName
        string = string + " Verbosity: " + self.fVerbosity
        string = string + " FilePublisher names: " + str(self.fFilePublisherNames)
       
        string = string + "\nNodes:\n"
 
        for node in self.fNodes.keys():
            string = string + str(self.fNodes[node]) + "\n"

        string = string + "\nNodeGroups:\n"

        for nodeGroup in self.fNodeGroups:
            string = string + str(nodeGroup) + "\n"

        string = string + "\nProcesses:\n"

        for proc in self.fProcesses:
            string = string + str(self.fProcesses[proc]) + "\n"

        return string

    def BuildNumericProcessMap(self):
        processesNew = range(len(self.fProcesses))
        self.fProcessIDMap = {}
        self.fChildProcesses = range(len(self.fProcesses))
        self.fGroupChildProcesses = {}
        procNum = 0
        for (id, p) in self.fProcesses.items():
            self.fChildProcesses[procNum] = []
            self.fProcessIDMap[id] = procNum
            p.fIDName = id
            p.fID = procNum
            processesNew[procNum] = p
            procNum+=1
        for g in self.fProcessGroups:
            self.fGroupChildProcesses[g] = []
        self.fProcesses = processesNew

        nodesNew = range(len(self.fNodes))
        self.fNodeIDMap = {}
        nodeNum = 0
        for (id, n) in self.fNodes.items():
            self.fNodeIDMap[id] = nodeNum
            n.fIDName = id
            n.fID = nodeNum
            nodesNew[nodeNum] = n
            nodeNum += 1
        self.fNodes = nodesNew

        self.fNodeGroupOffset = nodeNum
        nodeGroupsNew = range(nodeNum + len(self.fNodeGroups))
        for (id, ng) in self.fNodeGroups.items():
            self.fNodeIDMap[id] = nodeNum
            ng.fIDName = id
            ng.fID = nodeNum
            nodeGroupsNew[nodeNum] = ng
            nodeNum += 1
        self.fNodeGroups = nodeGroupsNew

        for ng in self.fNodeGroups[self.fNodeGroupOffset:]:
            ng.fGroupMasterNode = self.fNodeIDMap[ng.fGroupMasterNode]
            newMembers = []
            for nn in ng.fMembers:
                newMembers.append(self.fNodeIDMap[nn])
            ng.fMembers = newMembers;

        for (i, p) in enumerate(self.fProcesses):
            if p.fParentList != None:
                newParents = []
                for pp in p.fParentList:
                    if self.fProcessIDMap.has_key(pp):
                        ppnum = self.fProcessIDMap[pp]
                        newParents.append(ppnum)
                        self.fChildProcesses[ppnum].append(i)
                    elif self.fProcessGroups.has_key(pp):
                        newParents.append(pp)
                        self.fGroupChildProcesses[pp].append(i)
                    else:
                        raise ChainConfigException( "Invalid Process " + pp )
                p.fParentList = newParents
            newNodes = []
            for nn in p.fNodeList:
                newNodes.append(self.fNodeIDMap[nn])
            p.fNodeList = newNodes

        for (i, g) in self.fProcessGroups.items():
            for p in g.fProcessIDs:
                if self.fProcessIDMap.has_key(p):
                    p = self.fProcessIDMap[p]
#                else:
#                    raise ChainConfigException( "Process " + p + " missing, defined in group " + i )
            for p in g.fOutputProcesses:
                if self.fProcessIDMap.has_key(p):
                    p = self.fProcessIDMap[p]
#                else:
#                    raise ChainConfigException( "Process " + p + " missing, defined in output of group " + i )
            newNodes = []
            for nn in g.fNodes:
                newNodes.append(self.fNodeIDMap[nn])
            g.fNodes = newNodes

        for i in xrange(len(self.fProcesses)):
            if self.fProcesses[i].IsInProcessGroup():
                procGroup = self.fProcesses[i].GetProcessGroup()
                if i in procGroup.GetFullOutputProcessListFlat(self):
                    self.fProcesses[i].IsInProcessGroupOutputList = 1
                else:
                    self.fProcesses[i].IsInProcessGroupOutputList = 0
            else:
                self.fProcesses[i].IsInProcessGroupOutputList = 0

        self.fNumericMaps = 1

    def GetName( self ):
        return self.fName
    
    def SetVerbosity( self, verbosity ):
        self.fVerbosity = verbosity

    def GetVerbosity( self ):
        return self.fVerbosity

    def GetDefaultDelay( self ):
        return self.fDefaultDelay_ms

    def GetDefaultBridgeDelay( self ):
        return self.fDefaultBridgeDelay_ms

    def AddNode( self, node ):
        if self.fNodes.has_key(node.fID):
            raise ChainConfigException( "Node ID '"+node.fID+"' is used multiple times." )
        self.fNodes[node.fID] = node
        #self.fNodes.append( node )
        #self.fNodes.sort()

    def AddProcess( self, process ):
        if self.fProcesses.has_key(process.fID):
            raise ChainConfigException( "Process ID '"+process.fID+"' is used multiple times." )
        if self.fProcessGroups.has_key(process.fID):
            raise ChainConfigException( "Process ID '"+process.fID+"' is already as a process group ID." )
        self.fProcesses[process.fID] = process
        
        #if process!=None:
        #    self.fProcesses.append( process )
            #self.fProcesses.sort()

    def AddProcessGroup( self, processGroup ):
        if self.fProcessGroups.has_key(processGroup.fID):
            raise ChainConfigException( "Process group ID '"+processGroup.fID+"' is used multiple times." )
        if self.fProcesses.has_key(processGroup.fID):
            raise ChainConfigException( "Process group ID '"+processGroup.fID+"' is already as a process ID." )
        self.fProcessGroups[processGroup.fID] = processGroup
            
    def AddNodeGroup( self, group ):
        if self.fNodeGroups.has_key(group.fID):
            raise ChainConfigException( "Node group ID '"+group.fID+"' is used multiple times." )
        self.fNodeGroups[group.fID] = group
        #if group != None:
        #    self.fNodeGroups.append( group )

    def GetNode( self, ID ):
        if not self.fNumericMaps:
            if self.fNodes.has_key(ID):
                return self.fNodes[ID]
        elif ID < self.fNodeGroupOffset:
            return self.fNodes[ID]
        return None
        #return self.fNOdes
        #for n in self.fNodes:
        #    if n.fID==ID:
        #        return n
        #return None
        
    def GetNodeGroup( self, ID ):
        if self.fNodeGroups.has_key(ID):
            return self.fNodeGroups[ID]
        return  None
        #for g in self.fNodeGroups:
        #    if g.fID == ID:
        #        return g
        #return None

    def GetProcess( self, ID ):
        return self.fProcesses[ID]

    def GetProcessGroup( self, ID ):
        return self.fProcessGroups[ID]

    def GetProcessGroupIDList( self ):
        return self.fProcessGroups.keys()

    def IsProcessGroup( self, ID ):
        return self.fProcessGroups.has_key( ID )
        
    def GetNodeParent( self, ID ):
        if self.fNodeParentList.has_key(ID):
            return self.fNodeParentList[ID]
        for g in self.fNodeGroups[self.fNodeGroupOffset:]:
            if g.IsMember(ID):
                self.fNodeParentList[ID] = g
                return g
        self.fNodeParentList[ID] = None
        return None
    
    def GetNodeGroupParent( self, ID ):
        if self.fNodeGroupParentList.has_key(ID):
            return self.fNodeGroupParentList[ID]
        for g in self.fNodeGroups[self.fNodeGroupOffset:]:
            if g.IsMember(ID):
                self.fNodeGroupParentList[ID] = g
                return g
        self.fNodeGroupParentList[ID] = None
        return None

    def GetChildProcesses( self, ID ):
        return self.fChildProcesses[ID]

    def GetGroupChildProcesses( self, ID ):
        return self.fGroupChildProcesses[ID]

    def GetNodeProcesses( self, ID ):
        if not self.fNodeProcesses.has_key(ID):
            processes = []
            for p in self.fProcesses.values():
                if p.IsOnNode( ID ):
                    processes.append( p.fID )
            self.fNodeProcesses[ID] = processes
        return self.fNodeProcesses[ID]

    def GetSources( self ):
        sources = []
        for p in self.fProcesses:
            if len(p.GetParents())<=0:
                sources.append( p.fID )
        return sources

    def GetProcesses( self ):
        return self.fProcesses.values()

    def GetNodes( self ):
        # return self.fNodes[:]
        if self.fNumericMaps:
            return self.fNodes
        return self.fNodes.values()
            
    def GetNodeGroups( self ):
        if self.fNumericMaps:
            return self.fNodeGroups[self.fNodeGroupOffset:]
        return self.fNodeGroups.values()

    def GetProcessGroups( self ):
        return self.fProcessGroups.values()

    def GetProcessIDs( self ):
        return self.fProcesses.keys()
        #procs = []
        #for p in self.fProcesses:
        #    procs.append( p.fID )
        #return procs
        
    def GetNodeIDs( self ):
        return range(len(self.fNodes))
        #nodes = []
        #for n in self.fNodes:
        #    nodes.append( n.fID )
        #return nodes

    def GetSourceNodes( self ):
        sources = self.GetSources()
        sourceNodes = set()
        for s in sources:
            for tn in self.GetProcess( s ).GetNodes():
                sourceNodes.add(tn)
        return list(sourceNodes)
        #sourceNodes = []
        #for s in sources:
        #    tmpNodes = self.GetProcess( s ).GetNodes()
        #    for tn in tmpNodes:
        #        found=0
        #        for sn in sourceNodes:
        #            if tn==sn:
        #                found=1
        #        if not found:
        #            sourceNodes.append( tn )
        #return sourceNodes

    def GetNodesWithChildren( self, ID ):
        nodes = set()
        children = self.GetChildProcesses( ID )
        for c in children:
            for cn in self.GetProcess( c ).GetNodes():
                nodes.add(cn)
        return list(nodes)
        #nodes = []
        #children = self.GetChildProcesses( ID )
        #for c in children:
        #    for cn in self.GetProcess( c ).GetNodes():
        #        found=0
        #        for n in nodes:
        #            if n == cn:
        #                found=1
        #        if not found:
        #            nodes.append( cn )
        #return nodes

    def GetNodesWithNodeChildren( self, node ):
        nodes = set()
        procs = self.GetNodeProcesses( node )
        for ID in procs:
            children = self.GetChildProcesses( ID )
            for c in children:
                for cn in self.GetProcess( c ).GetNodes():
                    nodes.add(cn)
        return list(nodes)
        #nodes = []
        #procs = self.GetNodeProcesses( node )
        #for ID in procs:
        #    children = self.GetChildProcesses( ID )
        #    for c in children:
        #        for cn in self.GetProcess( c ).GetNodes():
        #            found=0
        #            for n in nodes:
        #                if n == cn:
        #                    found=1
        #            if not found:
        #                nodes.append( cn )
        #return nodes

    def GetNodesWithNodeParents( self, node ):
        nodes = set()
        procs = self.GetNodeProcesses( node )
        for ID in procs:
            parents = self.GetProcess( ID ).GetParents()
            for p in parents:
                if self.GetProcess( p )==None:
                    print "GetNodesWithNodeParents:",ID,p,node
                for pn in self.GetProcess( p ).GetNodes():
                    nodes.add( pn )
        return nodes
        #nodes = []
        #procs = self.GetNodeProcesses( node )
        #for ID in procs:
        #    parents = self.GetProcess( ID ).GetParents()
        #    for p in parents:
        #        for pn in self.GetProcess( p ).GetNodes():
        #            found=0
        #            for n in nodes:
        #                if n == pn:
        #                    found=1
        #            if not found:
        #                nodes.append( pn )
        #return nodes

    def GetChildrenOnNode( self, procID, nodeID ):
        nodeChildren = []
        children = self.GetChildProcesses( procID )
        for c in children:
            if self.GetProcess(c).IsOnNode(nodeID):
                nodeChildren.append( c.fID )
        return nodeChildren
        #nodeChildren = []
        #children = self.GetChildProcesses( procID )
        #for c in children:
        #    for n in c.GetNodes():
        #        if n == nodeID:
        #            nodeChildren.append( c.fID )
        #            break
        #return nodeChildren

    def GetNodeChildrenOnNode( self, parentNodeID, childNodeID ):
        nodeChildren = set()
        for procID in self.GetNodeProcesses( parentNodeID ):
            children = self.GetChildProcesses( procID )
            for c in children:
                if self.GetProcess(c).IsOnNode(childNodeID):
                    nodeChildren.add( c )
        return list(nodeChildren)
        #nodeChildren = []
        #for procID in self.GetNodeProcesses( parentNodeID ):
        #    children = self.GetChildProcesses( procID )
        #    for c in children:
        #        for n in self.GetProcess( c ).GetNodes():
        #            if n == childNodeID:
        #                found=0
        #                for nc in nodeChildren:
        #                    if nc==c:
        #                        found=1
        #                if not found:
        #                    nodeChildren.append( c )
        #                break
        #return nodeChildren

    def GetParentsOnNode( self, procID, nodeID ):
        tmpParents = self.GetProcess(procID).GetParents()
        parents = []
        for p in tmpParents:
            if self.GetProcess(procID).IsOnNode( nodeID ):
                parents.append( p )
        return parents

    def GetParentNodes( self, procID ):
        tmpParents = self.GetProcess(procID).GetParents()
        nodes = []
        for p in tmpParents:
            nodes.append(self.GetProcess(p).GetNodes())
        return nodes

    def GetParentNodeList( self, procID ):
        tmpParents = self.GetProcess(procID).GetParents()
        nodes = set()
        for p in tmpParents:
            for n in self.GetProcess(p).GetNodeList():
                nodes.add( n )
        return nodes
        #tmpParents = self.GetProcess(procID).GetParents()
        #nodes = []
        #for p in tmpParents:
        #    for n in self.GetProcess(p).GetNodeList():
        #        if nodes.count(n)<=0:
        #            nodes.append( n )
        #return nodes

    def Check( self ):
        for p in self.fProcesses.values():
            for n in p.GetNodes():
                if self.GetNode( n )==None:
                    raise ChainConfigException( "Node '"+n+"' specified for process '"+p.fID+"' cannot be found." )
            for par in p.GetParents():
                if self.GetProcess( par )==None:
                    raise ChainConfigException( "Parent process '"+par+"' specified for process '"+p.fID+"' cannot be found." )
            cmds = string.split( p.GetCmd()+" "+p.GetConfigParams() )
            if len(cmds)>1:
                bin = string.split( cmds[0], "/" )[-1]
                for fpn in self.fFilePublisherNames:
                    if bin==fpn:
                        found=0
                        for c in cmds[1:]:
                            if c=="-file" or c=="-datafile" or c=="-olddatafile" or c=="-datafilelist" or c=="-olddatafilelist":
                                found=1
                                break
                        if not found:
                            raise ChainConfigException( "No data file specified for file publisher process '"+p.fID+"'." )
                        break
                    
        #for i in range( len(self.fProcesses ) ):
        #    for j in range( i+1, len(self.fProcesses ) ):
        #        if self.fProcesses[i].fID==self.fProcesses[j].fID:
        #            raise ChainConfigException( "Process ID '"+self.fProcesses[i].fID+"'  is used multiple times." )
        #for i in range( len(self.fNodes ) ):
        #    for j in range( i+1, len(self.fNodes ) ):
        #        if self.fNodes[i].fID==self.fNodes[j].fID:
        #            raise ChainConfigException( "Node ID '"+self.fNodes[i].fID+"'  is used multiple times." )
        for gi in self.fNodeGroups.keys():
            if self.fNodes.has_key(gi):
                raise ChainConfigException( "Node group ID '"+self.fNodeGroups[gi].fID+"'  is also used as node ID." )
            count=0
            for gii in self.fNodeGroups.keys():
                if gi==gii:
                    continue
                count += self.fNodeGroups[gii].GetMembers().count(gi)
            if count>1:
                raise ChainConfigException( "Node group ID '"+gi+"' is used in more than one parent group (not supported yet)." )
            if self.fNodeGroups[gi].GetMembers().count(gi)>0:
                raise ChainConfigException( "Node group ID '"+self.fNodeGroups[gi].fID+"' is used recursively." )
        #for i in range( 0, len(self.fNodeGroups) ):
            #for j in range( i+1, len(self.fNodeGroups) ):
            #    if self.fNodeGroups[i].fID == self.fNodeGroups[j].fID:
            #        raise ChainConfigException( "Node group ID '"+self.fNodeGroups[i].fID+"'  is used multiple times." )
            #for n in self.fNodes:
            #    if self.fNodeGroups[i].fID == n.fID:
            #        raise ChainConfigException( "Node group ID '"+self.fNodeGroups[i].fID+"'  is also used as node ID." )
            #count=0
            #for j in range( 0, i ):
            #    for ngm in self.fNodeGroups[j].GetMembers():
            #        if ngm == self.fNodeGroups[i].fID:
            #            count=count+1
            #for j in range( i+1, len(self.fNodeGroups) ):
            #    for ngm in self.fNodeGroups[j].GetMembers():
            #        if ngm == self.fNodeGroups[i].fID:
            #            count=count+1
            #if count>1:
            #    raise ChainConfigException( "Node group ID '"+self.fNodeGroups[i].fID+"' is used in more than one parent group (not supported yet)." )
            #for ngm in self.fNodeGroups[i].GetMembers():
            #    if ngm == self.fNodeGroups[i].fID:
            #        raise ChainConfigException( "Node group ID '"+self.fNodeGroups[i].fID+"' is used recursively." )
        # Check for more complex recursion??
        # Implicit check in MakeGroupLevels??
        procs = self.GetProcesses()
        procGroupIDs = self.GetProcessGroupIDList()
        procGroups = {}
        groupProcIDs = set()
        for pgi in procGroupIDs:
            procGroups[pgi] = self.GetProcessGroup(pgi)
            groupProcIDs |= set( procGroups[pgi].GetProcessIDs() )
        for proc in procs:
            if proc.fID in groupProcIDs:
                groups = []
                for pgi in procGroupIDs:
                    if proc.fID in procGroups[pgi].GetProcessIDs():
                        groups.append( pgi )
                raise ChainConfigException( "Process ID "+proc.fID+" is used at top level as well as in process groups "+str(groups)+" (ambivalent, not supported)." )
                    
                    
    def MakeGroupLevels( self ):
        changed = 1
        while changed:
            changed = 0
            for g in self.fNodeGroups.values():
                if g.fLevel!=0:
                    continue
                thischanged = 0
                for ng in self.fNodeGroups.values():
                    if ng.fID != g.fID:
                        for ngm in ng.GetMembers():
                            if ngm == g.fID:
                                changed = 1
                                thischanged = 1
                                if ng.fLevel==0:
                                    continue
                                g.fLevel = ng.fLevel+1
                                g.SetGroupParent( ng.fID )
                if g.fLevel==0 and not thischanged:
                    g.fLevel = 1
                    changed = 1
                    g.SetGroupParent( None )


    def MakeProcessLevels( self ):
        parentsNeeded = array.array('i', (0 for _ in xrange(len(self.fProcesses))))
        levels = array.array('i', (0 for _ in xrange(len(self.fProcesses))))
        for i in range(len(self.fProcesses)):
            numParents = 0
            for par in self.fProcesses[i].GetParents():
                if self.IsProcessGroup(par):
                    numParents += len(self.GetProcessGroup(par).GetFullOutputProcessListFlat(self))
                else:
                    numParents += 1
            parentsNeeded[i] = numParents
            levels[i] = 0

        need = 1
        while need:
            need = 0
            done = 0
            for i in range(len(self.fProcesses)):
                if parentsNeeded[i] > 0:
                    need = 1
                elif parentsNeeded[i] == 0:
                    parentsNeeded[i] = -100
                    done = 1
                    for j in self.GetChildProcesses(i):
                        if levels[i] >= levels[j]:
                            levels[j] = levels[i] + 1
                        parentsNeeded[j] -= 1
                    if self.fProcesses[i].IsInProcessGroupOutputList:
                        procGroup = self.fProcesses[i].GetProcessGroup()
                        for j in self.GetGroupChildProcesses(procGroup.GetID()):
                            if levels[i] >= levels[j]:
                                levels[j] = levels[i] + 1
                            parentsNeeded[j] -= 1

            if done == 0:
                raise ChainConfigException( "Internal Error in MakeProcessLevels" )

        for i in range(len(self.fProcesses)):
            self.fProcesses[i].SetLevel(levels[i])
        return

#        doneProcs = self.GetSources()
#        procs = set()
#        for p in doneProcs:
#            self.GetProcess(p).SetLevel( 0 )
#            procs |= set(self.GetChildProcesses( p ))
#
#        while len(procs)>0:
#            tmpSet = set()
#            completed = set()
#            for proc in procs:
#                parents = []
#                for par in self.GetProcess(proc).GetParents():
#                    if self.IsProcessGroup(par):
#                        parents += self.GetProcessGroup(par).GetFullOutputProcessListFlat(self)
#                    else:
#                        parents.append( par )
#                maxLevel = None
#                incomplete = False
#                for par in parents:
#                    if not par in doneProcs:
#                        incomplete = True
#                        break
#                    if maxLevel==None or self.GetProcess(par).GetLevel()>maxLevel:
#                        maxLevel = self.GetProcess(par).GetLevel()
#                if not incomplete:
#                    self.GetProcess(proc).SetLevel( maxLevel+1 )
#                    doneProcs.append( proc )
#                    completed.add( proc )
#                tmpSet |= set(self.GetChildProcesses( proc ))
#                if self.GetProcess(proc).IsInProcessGroup():
#                    procGroup = self.GetProcess(proc).GetProcessGroup()
#                    if proc in procGroup.GetFullOutputProcessListFlat(self):
#                        tmpSet |= set(self.GetGroupChildProcesses( procGroup.GetID() ))
#            if len(tmpSet-procs)<=0 and len(completed)<=0:
#                print "procs:",procs
#                print "tmpSet:", tmpSet
#                print "completed:", completed
#                print "doneProcs:", doneProcs
#                raise ChainConfigException( "Cannot determine all parent processes : "+str(procs) )
#            procs |= tmpSet
#            procs -= completed


    def GetTotalProcessDelay( self, proc, nodeID=None, includeSelf=1 ):
        if proc.GetType()=="src":
            return 0
        nID="None"
        if nodeID != None:
            nID=nodeID
        if not proc.fID in self.fTotalDelay.keys():
            self.fTotalDelay[proc.fID]={}
        elif nID in self.fTotalDelay[proc.fID].keys():
            return self.fTotalDelay[proc.fID][nID]
        
        parentIds = proc.GetParents()
        parents = []
        for p in parentIds:
            if self.IsProcessGroup(p):
                for gop in self.GetProcessGroup(p).GetFullOutputProcessListFlat(self):
                    pp = self.GetProcess( gop )
                    if pp==None:
                        raise ChainConfigException( "Parent group output process '"+gop+"' not found." )
                    parents.append( pp )
            else:
                pp = self.GetProcess( p )
                if pp==None:
                    raise ChainConfigException( "Parent process '"+p+"' not found." )
                parents.append( pp )
        delay=0
        maxDelay=0
        for p in parents:
            delay = self.GetTotalProcessDelay( p )
            if len(proc.GetNodeList())==1:
                if not p.IsFullyOnNode( proc.GetNodeList()[0] ):
                    delay += self.GetDefaultBridgeDelay()
            else:
                delay += self.GetDefaultBridgeDelay()
            if delay>maxDelay:
                maxDelay = delay
            #print proc.fID,"parent",p.fID,"delay:",delay,"- maxParentDelay:",maxDelay
        if includeSelf:
            maxDelay += proc.GetDelay()
        #print proc.fID,"IsFullyonNode(",nodeID,"):",proc.IsFullyOnNode( nodeID )
        if nodeID!=None:
            if not proc.IsFullyOnNode( nodeID ):
                maxDelay += self.GetDefaultBridgeDelay()
        #print proc.fID,"total delay:",maxDelay
        self.fTotalDelay[proc.fID][nID]=maxDelay
        return maxDelay

    def ResolveProcessGroupParents( self ):
        procs = self.GetProcesses()
        procGroupIDs = self.GetProcessGroupIDList()
        procGroups = {}
        for pgi in procGroupIDs:
            procGroups[pgi] = self.GetProcessGroup(pgi)
        for proc in procs:
            parents = proc.GetParents()
            for par in parents:
                if par in procGroupIDs:
                    proc.ReplaceParent( par, procGroups[par].GetFullOutputProcessList(self) )

                


class MappedProcess:
    def __init__( self, ID, type, command, configureParameters, controlUrl, parents, node, virtualParents=None ):
        self.fID = ID
        self.fType = type
        # type can be src, prc, snk, em, eg, es, sbh, pbh
        self.fCommand = command
        self.fControlURL = controlUrl
        #print "MappedProcess.__init__, node, ID, type, parents, virtualParents:", node,ID, type, parents, virtualParents
        self.fParents = parents
        self.fNode = node
        self.fVirtualParents = virtualParents
        self.fConfigParams = configureParameters
        self.fReceivedDDLsOutput = []
        self.fEventModulo = None
        self.fEventTypeCode = None
        self.fMappedParents = None

    def GetNode( self ):
        return self.fNode

    def GetType( self ):
        return self.fType

    def GetTypeString( self ):
        if len(self.fType)==2:
            return " "+self.fType
        else:
            return self.fType

    def GetCmd( self ):
        return self.fCommand

    def GetConfigParams( self ):
        return self.fConfigParams

    def GetID( self ):
        return self.fID

    def GetControlURL( self ):
        return self.fControlURL

    def GetVirtualParents( self ):
        return self.fVirtualParents

    def GetVirtualParentString( self ):
        str = "[ "
        for p in self.fVirtualParents[:-1]:
            str = str+p+", "
        if len(self.fVirtualParents)>0:
            str = str+self.fVirtualParents[-1]+" "
        str = str+"]"
        return str

    def GetParents( self ):
        return self.fParents

    def GetParentString( self ):
        str = "[ "
        for p in self.fParents[:-1]:
            str = str+p+", "
        if len(self.fParents)>0:
            str = str+self.fParents[-1]+" "
        str = str+"]"
        return str

    def SetReceivedDDLsOutput( self, outputDDLs ):
        self.fReceivedDDLsOutput = outputDDLs

    def AddReceivedDDLOutput( self, outputDDL ):
        self.fReceivedDDLsOutput.append( outputDDL )

    def AddReceivedDDLsOutput( self, outputDDLs ):
        self.fReceivedDDLsOutput += outputDDLs

    def GetReceivedDDLsOutput( self ):
        return self.fReceivedDDLsOutput

    def GetEventModulo( self ):
        return self.fEventModulo

    def SetEventModulo( self, modulo ):
        self.fEventModulo = modulo

    def GetEventTypeCode( self ):
        return self.fEventTypeCode

    def SetEventTypeCode( self, etc ):
        self.fEventTypeCode = etc



class DataFlowProcess(Process):
    def __init__( self, ID, type, cmdLine, configParams, parentList, nodeList, shmType, shmSize, blockSize, virtualParents, controlURL, otherParams={} ):
        Process.__init__( self, ID, type, { -1 : cmdLine }, configParams, parentList, nodeList, shmType, shmSize, blockSize, 0, False, {}, None, None )
        self.fVirtualParents = virtualParents
        self.SetControlURL( 0, controlURL )
        self.fOtherParams = otherParams
        if len(self.fNodeList)>1:
            self.fNode = None
        self.fNode = self.fNodeList[0]

    def GetVirtualParents( self ):
        return self.fVirtualParents

    def GetOtherParameters( self ):
        return self.fOtherParams

    def GetNode(self):
        if len(self.fNodeList)>1:
            return None
        return self.fNodeList[0]

    def __str__(self):
        string = Process.__str__( self )
        string = string+"\nVirtual parents: "+str(self.fVirtualParents)
        string = string+"\nOther parameters: "+str(self.fOtherParams)
        return string



class ChainConfigMapper:
    def __init__( self, frameworkdir, platform ):
        if frameworkdir!=None:
            self.fFrameworkDir = os.path.expanduser( frameworkdir )
        else:
            self.fFrameworkDir = None
        self.fFrameworkPlatform = platform
        self.fStartSocket = 5000
        self.fEventMergerTimeout = 0
        self.fEventMergerInputTimeout = 0
        self.fEventMergerTimeoutIncrease = 0
        self.fStartShm = 0x10
        self.fBridgeMsgSocketOffset = 0
        self.fBridgeBlobMsgSocketOffset = 1
        self.fBridgeBlobSocketOffset = 2
        self.fUseBridgeSlots = 0
        self.InitLists()
        self.fProduction = 0
        self.fPublisherBridgeHeadDefaultShmType = "sysv"
        self.fPublisherBridgeHeadShmTypes = {}
        self.fAliceHLT = False
        self.fScattererIDMap = {}
        #self.fGathererIDMap = {}
        self.fPublisher2Proc = {}
        self.fProcID2Publisher = {}
        self.fSubscription2Proc = {}
        self.fProcID2Subscription = {}
        self.fPubSubConnections = {}
        self.fSubscriberID2Attributes = {}
        self.fCreateParentNodeGatherersCache = {}
        self.fCreateParentNodeFiltersCache = {}
        self.fCreateParentNodeMergersCache = {}
        self.fCreateParentNodeScatterersCache = {}
        self.fCreateParentNodeBridgeHeadsAndAuxGatherersCache = {}
        self.fCreateChildFilterProcessCache = {}
        self.fCreateChildNodeGatherersCache = {}
        self.fCreateChildNodeMergersCache = {}
        self.fCreateChildNodeScatterersCache = {}
        self.fCreateChildNodeBridgeHeadsCache = {}
        self.fCreateParentMergeListCache = {}
        self.fCreateParentScattererListCache = {}
        self.CreateMergedBridgeListCache = {}
        self.fCreateParentNodeBridgeListCache = {}
        self.fCreateChildNodeBridgeListCache = {}
        self.fCreateGathererChildNodeListCache = {}
        self.fBridgeSenderProcID2Msg = {}
        self.fBridgeReceiverMsg2Proc = {}
        self.fQuiet=False
        self.fSysMESLogging = False
        self.fInfoLoggerLibrary = None
        self.fMaxSubscriberRetryCount=None
        self.fBridgeShmNodeLimit = 0
        self.fBridgeShmLimit = 0
        self.fShmNodeLimit = {}
        self.fShmNodeSpecificLimit = {}
        self.fShmNodeSpecificLimitRECache = {}
        self.fShmPerProcessLimit = {}
        self.fEventAccounting = None
        self.fMergerTriggerClassInputDisabling = True
        self.fTriggerClasses = {}
        self.fTriggerClassesByID = {}
        self.fEventMergerDynamicTimeout_MaxTimeout_ms = 0
        self.fEventMergerDynamicInputXabling_MaxMissingEventCnt = 0
        self.fEventMergerDynamicInputXabling_MinMissingEventCnt = 0
        self.fScattererParam = None
        self.fEventModuloPreDivisor = None
        self.fEventTypeCodeMacros = {}
        self.fCTPTriggerClassGroups = {}
        self.fFastMapping = True
        self.fMergerInputCountLimit=12
        #self.fMergerInputCountLimit=0
        self.fReadoutListVersion = None
        self.fMaxEventsInChain = None
        self.fMaxEventsInChainExp2 = None
        #self.fAllowScattererSeqNrDistribution = False
        self.fAllowScattererSeqNrDistribution = True
        self.fNoStrictSOR = False
        self.fMergerCmdLineOptions = ""
        self.fScattererCmdLineOptions = ""
        self.fGathererCmdLineOptions = ""
        self.fFilterCmdLineOptions = ""
        self.fSubscriberBridgeCmdLineOptions = ""
        self.fPublisherBridgeCmdLineOptions = ""
        self.fLocalDataFlowCmdLineOptions = ""
        self.fBridgeCmdLineOptions = ""
        self.fDataFlowCmdLineOptions = ""
        self.fCommonCommandLineCache = None
        pass

    def Publisher2Proc( self, publisherID ):
        if self.fPublisher2Proc.has_key(publisherID):
            return self.fPublisher2Proc[publisherID]
        return None
    def ProcID2PublisherList( self, procID ):
        if self.fProcID2Publisher.has_key(procID):
            return self.fProcID2Publisher[procID]
        return []
    def Subscription2Proc( self, publisherID ):
        if self.fSubscription2Proc.has_key(publisherID):
            return self.fSubscription2Proc[publisherID]
        #print "Warning, None has to be returned actually, this is just for debugging!!!!!!!!!!!!!!!!!! (line 1014, Subscription2Proc)"
        #return []
        return None
    def ProcID2SubscriptionList( self, procID ):
        if self.fProcID2Subscription.has_key(procID):
            return self.fProcID2Subscription[procID]
        return []

    def SetFastMapping( self, fastMapping ):
        self.fFastMapping = fastMapping

    def SetReadoutListVersion( self, readoutListVersion ):
        self.fReadoutListVersion = readoutListVersion

    def NoStrictSOR( self ):
        self.fNoStrictSOR = True

    def SetMergerCmdLineOptions( self, opt ):
        self.fMergerCmdLineOptions = opt

    def SetScattererCmdLineOptions( self, opt ):
        self.fScattererCmdLineOptions = opt

    def SetGathererCmdLineOptions( self, opt ):
        self.fGathererCmdLineOptions = opt

    def SetFilterCmdLineOptions( self, opt ):
        self.fFilterCmdLineOptions = opt

    def SetSubscriberBridgeCmdLineOptions( self, opt ):
        self.fSubscriberBridgeCmdLineOptions = opt

    def SetPublisherBridgeCmdLineOptions( self, opt ):
        self.fPublisherBridgeCmdLineOptions = opt

    def SetLocalDataFlowCmdLineOptions( self, opt ):
        self.fLocalDataFlowCmdLineOptions = opt

    def SetBridgeCmdLineOptions( self, opt ):
        self.fBridgeCmdLineOptions = opt

    def SetDataFlowCmdLineOptions( self, opt ):
        self.fDataFlowCmdLineOptions = opt

    def SetMaxEventsInChain( self, maxEventsInChain ):
        self.fMaxEventsInChain = maxEventsInChain
        tmp = 1
        self.fMaxEventsInChainExp2 = 0
        while tmp>0 and tmp<maxEventsInChain:
            self.fMaxEventsInChainExp2 = self.fMaxEventsInChainExp2 + 1
            tmp = 1 << self.fMaxEventsInChainExp2

    def SetBridgeShmLimits( self, bridgeLimit, nodeLimit ):
        self.fBridgeShmNodeLimit = nodeLimit
        self.fBridgeShmLimit = bridgeLimit

    def SetShmLimits( self, procLimit, nodeLimit, nodeSpecificLimit=None ):
        for k in procLimit.keys():
            self.fShmPerProcessLimit[k] = procLimit[k]
        for k in nodeLimit.keys():
            self.fShmNodeLimit[k] = nodeLimit[k]
        if nodeSpecificLimit!=None:
            for n in nodeSpecificLimit.keys():
                if not self.fShmNodeSpecificLimit.has_key(n):
                    self.fShmNodeSpecificLimit[n] = {}
                    self.fShmNodeSpecificLimitRECache[n] = re.compile( n )
                for k in nodeSpecificLimit[n]:
                    self.fShmNodeSpecificLimit[n][k] = nodeSpecificLimit[n][k]

    def GetNodeShmLimit( self, node,shmType ):
        for n in self.fShmNodeSpecificLimit.keys():
            if not self.fShmNodeSpecificLimit[n].has_key(shmType):
                continue
            match = self.fShmNodeSpecificLimitRECache[n].search( node )
            if match != None:
                return self.fShmNodeSpecificLimit[n][shmType]
        if self.fShmNodeLimit.has_key(shmType):
            return self.fShmNodeLimit[shmType]
        return 0
        #if self.fShmNodeSpecificLimit.has_key(node) and self.fShmNodeSpecificLimit[node].has_key(shmType):
        #    nodeShmLimit = self.fShmNodeSpecificLimit[node][shmType]
        #elif self.fShmNodeLimit.has_key(shmType):
        #    nodeShmLimit = self.fShmNodeLimit[shmType]
        #else:
        #    nodeShmLimit = 0
        #return nodeShmLimit

    def GetNodeShmTypes( self, node ):
        for n in self.fShmNodeSpecificLimit.keys():
            match = self.fShmNodeSpecificLimitRECache[n].search( node )
            if match != None:
                return set(self.fShmNodeSpecificLimit[n].keys())
        return set()

    def AddEventTypeCodeMacro( self, macroID, macro ):
        self.fEventTypeCodeMacros[macroID] = macro

    def AddCTPTriggerClassGroup( self, ID, spec ):
        self.fCTPTriggerClassGroups[ID] = spec

    def Quiet(self):
        self.fQuiet=True

    def SysMESLogging(self):
        self.fSysMESLogging = True

    def InfoLoggerLogging(self, dynLib ):
        self.fInfoLoggerLibrary = dynLib

    def SetEventAccounting( self, eventAccounting ):
        self.fEventAccounting = eventAccounting

    def SetTriggerClassDDLs( self, triggerClass, ddlList, ID=None ):
        if type(triggerClass)!=types.IntType:
            raise ChainConfigException( "Given trigger class '"+str(triggerClass)+"' is not an integer" )
        if ID!=None:
            self.fTriggerClassesByID[ID] = triggerClass
        self.fTriggerClasses[triggerClass] = ddlList

    def EnableEventMergerDynamicTimeouts( self, minTimeout_ms, maxTimeout_ms ):
        self.fEventMergerDynamicTimeout_MinTimeout_ms = minTimeout_ms
        self.fEventMergerDynamicTimeout_MaxTimeout_ms = maxTimeout_ms

    def EnableEventMergerInputTimeout( self, inputTimeout ):
        self.fEventMergerInputTimeout = inputTimeout

    def EnableEventMergerDynamicInputXabling( self, maxMissingEventCnt ):
        self.fEventMergerDynamicInputXabling_MaxMissingEventCnt = maxMissingEventCnt

    def SetScattererParam( self, param ):
        self.fScattererParam = param

    def SetEventModuloPreDivisor( self, preDiv ):
        self.fEventModuloPreDivisor = preDiv
        

    def IDFromFilterTypeID( self, filteredID ):
        return "___".join(filteredID.split("___")[:-1])
    def FilterTypeFromFilterTypeID( self, filteredID ):
        return filteredID.split("___")[-1]
    def IDFilterAddition( self, filterType ):
        if filterType=="readout":
            return "-ReadoutFilter"
        elif filterType=="monitor":
            return "-MonitorFilter"
        return ""
    def FullFilteredID( self, filterTypeID, node ):
        filterType = filterTypeID.split("___")[-1]
        tmpNode = node+"-"
        if filterType=="readout":
            tmpNode = node+"-"
            add = "-ReadoutFilter"
        elif filterType=="monitor":
            tmpNode = node+"-"
            add = "-MonitorFilter"
        else:
            tmpNode = ""
            add = ""
        return tmpNode+"___".join(filterTypeID.split("___")[:-1])+add
        

    def GetNodePlatform( self, node ):
        # print node
        platform = node.GetPlatform()
        if platform==None:
            platform = self.fFrameworkPlatform
        return platform

    def GetScattererIDSuffix( self, ID, mult, node ):
        highest_suffix=-1
        if self.fScattererIDMap.has_key( ID ):
            for g in self.fScattererIDMap[ID]:
                if g["mult"] == mult and g["node"]==node:
                    return g["suffix"]
                if g["suffix"]>highest_suffix:
                    highest_suffix = g["suffix"]
        else:
            self.fScattererIDMap[ID] = []
        self.fScattererIDMap[ID].append( { "suffix" : highest_suffix+1, "mult" : mult, "node" : node } )
        return highest_suffix+1

    def SetProduction( self, production ):
        self.fProduction = production

    def SetPublisherBridgeHeadShmType( self, shmType, node=None ):
        if node==None:
            self.fPublisherBridgeHeadDefaultShmType = shmType
        else:
            self.fPublisherBridgeHeadShmTypes[node] = shmType

    def SetMaxSubscriberRetryCount(self,count):
        self.fMaxSubscriberRetryCount = count

    def Reset( self ):
        self.InitLists()
        
    def SetStartSocket( self, start_socket ):
        self.fStartSocket = start_socket

    def SetStartShmID( self, start_shm ):
        self.fStartShm = start_shm

    def SetEventMergerTimeout( self, event_merger_timeout, event_merger_timeout_increase=None ):
        self.fEventMergerTimeout = event_merger_timeout
        if event_merger_timeout_increase==None:
            self.fEventMergerTimeoutIncrease = event_merger_timeout/20
        else:
            self.fEventMergerTimeoutIncrease = event_merger_timeout_increase

    def AliceHLT( self ):
        self.fAliceHLT = True
        
    def SetMergerTriggerClassInputDisabling( self, mergerInputDisabling ):
        self.fMergerTriggerClassInputDisabling = mergerInputDisabling

    def CreateCommonCmdLineOptions( self, config, node, procID, eventsExp2, eventModulo ):
        cmdLine = " -name "+procID
        if eventModulo and self.fEventModuloPreDivisor!=None:
            cmdLine = cmdLine+(" -eventmodulopredivisor %u" % self.fEventModuloPreDivisor)
        if eventsExp2!=None:
            cmdLine = cmdLine + " -eventsexp2 %d" % ( eventsExp2+1 )
        controlURL = "tcpmsg://"+config.GetNode( node ).GetHostname()+":"+("%d" % self.GetNextSocket( config, node, controlled_process_id=procID ))+"/"
        cmdLine = cmdLine + " -control "+MakeLocalURL( controlURL )
        if self.fCommonCommandLineCache == None:
            cmdLine2 = " -V "+config.GetVerbosity()+" -nostdout"
            if self.fAliceHLT:
                cmdLine2 = cmdLine2 + " -alicehlt"
                if self.fReadoutListVersion!=None:
                    cmdLine2 = cmdLine2 + " -readoutlistversion %d" % self.fReadoutListVersion
            if self.fSysMESLogging:
                cmdLine2 = cmdLine2 + " -sysmeslog"
            if self.fInfoLoggerLibrary!=None:
                cmdLine2 = cmdLine2 + " -infologger "+self.fInfoLoggerLibrary
            if self.fEventAccounting!=None:
                cmdLine2 = cmdLine2 + " -eventaccounting "+self.fEventAccounting
            if self.fAliceHLT:
                for g in self.fCTPTriggerClassGroups.keys():
                    cmdLine2 = cmdLine2 + " -ctptriggerclassgroup "+g+" "+self.fCTPTriggerClassGroups[g]
            self.fCommonCommandLineCache = cmdLine2
        cmdLine += self.fCommonCommandLineCache
        return ( cmdLine, controlURL )

    def CreateFilterCmdLineOptions( self, config, filterDict, isScattererGathererMerger, subscriberID ):
        cmdLine = ""
        params = { "eventmodulo" : { True : "-subscribereventmodulo", False : "-eventmodulo" },
                   "eventtypecode" : { True : "-subscribereventtypecode", False : "-eventtypecode" } }
        for p in ( "eventmodulo", "eventtypecode" ):
            if filterDict[p]!=None:
                # print "filterDict["+p+"]",type(filterDict[p]),filterDict[p]
                cmdLine += " "+params[p][isScattererGathererMerger]+( " "+subscriberID if isScattererGathererMerger else "" )+" "+str(filterDict[p])

        return cmdLine

    def CreatePlatform( self, config, node ):
        platform = self.GetNodePlatform( config.GetNode( node ) )
        if self.fProduction:
            platform = platform+"-releasedebug"
        else:
            platform = platform+"-debug"
        return platform


    def CreateGatherer( self, config, procID, gathererID, node, parents, eventsExp2, filters ):
        platform = self.CreatePlatform( config, node )
        if self.fFrameworkDir!=None:
            cmdLine = self.fFrameworkDir+"/bin/"+platform+"/"
        else:
            cmdLine = ""
        configParams = ""
        publisherID = gathererID+"Pub"
        cmdLine += "EventGathererNew"+" -publisher "+publisherID
        ndx=0
        for p in parents:
            subscriberID = gathererID+("%d" % ndx )+"Sub"
            configParams += " -subscribe "+p+"Pub" + " "+subscriberID
            if p in filters.keys():
                configParams += self.CreateFilterCmdLineOptions( config, filters[p], True, subscriberID )
            ndx += 1

        if self.fNoStrictSOR:
            cmdLine = cmdLine + " -nostrictsor"

        opt = " ".join( [
            self.fDataFlowCmdLineOptions,
            self.fLocalDataFlowCmdLineOptions,
            self.fGathererCmdLineOptions,
            ] )
        if len(opt)>0:
            cmdLine = cmdLine+" "+opt
        cclo = self.CreateCommonCmdLineOptions( config, node, gathererID, eventsExp2, True )
        controlURL = cclo[1]
        cmdLine = cmdLine+cclo[0]
        #return DataFlowProcess( gathererID, "eg", cmdLine, configParams, parents, [ node ], "invalid", 0, 0, [ procID+"___none" ], controlURL )
        return MappedProcess( gathererID, "eg", cmdLine, configParams, controlURL, parents, node, [ procID+"___none" ] )


    def CreateFilter( self, config, procID, filterID, node, parents, eventsExp2, filters, filterType ):
        platform = self.CreatePlatform( config, node )
        if self.fFrameworkDir!=None:
            cmdLine = self.fFrameworkDir+"/bin/"+platform+"/"
        else:
            cmdLine = ""
        configParams = ""
        publisherID = filterID+"Pub"
        cmdLine = cmdLine+"TriggeredFilter"+" -publisher "+publisherID
        if len(parents)>1:
            raise ChainConfigException( "INTERNAL ERROR: More than 1 parent for filter "+filterID+" for process "+procID+": "+str(parents) )
        cmdLine = cmdLine + " -subscribe "+parents[0]+"Pub "+filterID+"Sub"
        if parents[0] in filters.keys():
            cmdLine += self.CreateFilterCmdLineOptions( config, filters[parents[0]], False, filterID+"Sub" )
        if filterType == "monitor":
            cmdLine = cmdLine + " -monitorfilter"
        elif filterType == "readout":
            cmdLine = cmdLine + " -readoutfilter"
        cmdLine = cmdLine + " -shm sysv %d 4096 -minblocksize 4096" % (self.GetNextShmID( config, node, shm_id=filterID ))

        opt = " ".join( [
            self.fDataFlowCmdLineOptions,
            self.fFilterCmdLineOptions,
            ] )
        if len(opt)>0:
            cmdLine = cmdLine+" "+opt
        cclo = self.CreateCommonCmdLineOptions( config, node, filterID, None, True )
        controlURL = cclo[1]
        cmdLine = cmdLine+cclo[0]

        #return DataFlowProcess( filterID, "tf", cmdLine, configParams, [parents[0]], [ node ], "invalid", 0, 0, [ procID+"___none" ], controlURL )
        return MappedProcess( filterID, "tf", cmdLine, configParams, controlURL, parents[0:1], node, [ procID+"___none" ] )


    def CreateScatterer( self, config, procID, scattererID, node, parents, eventsExp2, filters, outputs, seqNrDistribution ):
        platform = self.CreatePlatform( config, node )
        if self.fFrameworkDir!=None:
            cmdLine = self.fFrameworkDir+"/bin/"+platform+"/"
        else:
            cmdLine = ""
        configParams = ""
        cmdLine += "EventScattererNew"
        if len(parents)>1:
            raise ChainConfigException( "INTERNAL ERROR: More than 1 parent for scatterer "+scattererID+" for process "+config.fProcesses[procID].fIDName+": "+str(parents) )
        cmdLine = cmdLine + " -subscribe "+parents[0]+"Pub " + scattererID+"Sub"
        if parents[0] in filters.keys():
            cmdLine += self.CreateFilterCmdLineOptions( config, filters[parents[0]], True, scattererID+"Sub" )
        for output in outputs:
            configParams = configParams + " -publisher "+output+"Pub"

        if seqNrDistribution:
            cmdLine = cmdLine + " -seqnrdistribution"
        elif self.fScattererParam!=None:
            cmdLine = cmdLine + " -scattererparam %lu" % ( self.fScattererParam )
        if self.fNoStrictSOR:
            cmdLine = cmdLine + " -nostrictsor"
        opt = " ".join( [
            self.fDataFlowCmdLineOptions,
            self.fLocalDataFlowCmdLineOptions,
            self.fScattererCmdLineOptions,
            ] )
        if len(opt)>0:
            cmdLine = cmdLine+" "+opt
        cclo = self.CreateCommonCmdLineOptions( config, node, scattererID, eventsExp2, True )
        controlURL = cclo[1]
        cmdLine = cmdLine+cclo[0]
        #return DataFlowProcess( scattererID, "es", cmdLine, configParams, parents, [ node ], "invalid", 0, 0, [ procID+"___none" ], controlURL )
        return MappedProcess( scattererID, "es", cmdLine, configParams, controlURL, parents, node, [ config.fProcesses[procID].fIDName+"___none" ] )

    def CreateBridgeHeads( self, config, parentID, childID, parentBridgeID, childBridgeID, parentNode, childNode, parents, eventsExp2, filters ):
        bridgeData = self.GetNewBridgeData( config, parentID, childID, parentNode, childNode )
        pproc = config.GetProcess( parentID )
        blocksize = pproc.GetBlockSize()
        blockcount = pproc.GetBlockCount()
        platform = self.CreatePlatform( config, parentNode )
        if self.fFrameworkDir!=None:
            cmdLine = self.fFrameworkDir+"/bin/"+platform+"/"
        else:
            cmdLine = ""
        cmdLine = cmdLine+"SubscriberBridgeHeadNew -name "+parentBridgeID
        if len(parents)>1:
            raise ChainConfigException( "INTERNAL ERROR: More than 1 parent for subscriber bridge head "+parentBridgeID+" for process "+childID+": "+str(parents) )
        cmdLine = cmdLine + " -subscribe "+parents[0]+"Pub " + parentBridgeID+"Sub"
        if parents[0] in filters.keys():
            cmdLine += self.CreateFilterCmdLineOptions( config, filters[parents[0]], False, parentBridgeID+"Sub" )
        cmdLine = cmdLine + " -minblocksize %d" % (blocksize+2048)

            
        if self.fUseBridgeSlots:
            cmdLine = cmdLine + " -msg tcpmsg://"+config.GetParentNode( parentNode ).GetHostname()+":" + ( "%d" % (bridgeData["SenderBaseSocket"]+self.fBridgeMsgSocketOffset) )+"/slotslog2=8/ tcpmsg://"+config.GetNode( childNode ).GetHostname()+":"+( "%d" % (bridgeData["ReceiverBaseSocket"]+self.fBridgeMsgSocketOffset))
            cmdLine = cmdLine + " -blobmsg tcpmsg://"+config.GetNode( parentNode ).GetHostname()+":" + ( "%d" % (bridgeData["SenderBaseSocket"]+self.fBridgeBlobMsgSocketOffset) )+"/slotslog2=8/ tcpmsg://"+config.GetNode( childNode ).GetHostname()+":"+( "%d" % (bridgeData["ReceiverBaseSocket"]+self.fBridgeBlobMsgSocketOffset))
            cmdLine = cmdLine + " -blob " + ("%d" % 4096 ) +" tcpblob://"+config.GetNode( parentNode ).GetHostname()+":" + ( "%d" % (bridgeData["SenderBaseSocket"]+self.fBridgeBlobSocketOffset) ) + "/slotslog2=8/"
        else:
            cmdLine = cmdLine + " -msg tcpmsg://"+config.GetNode( parentNode ).GetHostname()+":" + ( "%d" % (bridgeData["SenderBaseSocket"]+self.fBridgeMsgSocketOffset) )+" tcpmsg://"+config.GetNode( childNode ).GetHostname()+":"+( "%d" % (bridgeData["ReceiverBaseSocket"]+self.fBridgeMsgSocketOffset))
            cmdLine = cmdLine + " -blobmsg tcpmsg://"+config.GetNode( parentNode ).GetHostname()+":" + ( "%d" % (bridgeData["SenderBaseSocket"]+self.fBridgeBlobMsgSocketOffset) )+" tcpmsg://"+config.GetNode( childNode ).GetHostname()+":"+( "%d" % (bridgeData["ReceiverBaseSocket"]+self.fBridgeBlobMsgSocketOffset))
            cmdLine = cmdLine + " -blob " + ("%d" % 4096 ) +" tcpblob://"+config.GetNode( parentNode ).GetHostname()+":" + ( "%d" % (bridgeData["SenderBaseSocket"]+self.fBridgeBlobSocketOffset) )


        opt = " ".join( [
            self.fDataFlowCmdLineOptions,
            self.fBridgeCmdLineOptions,
            self.fSubscriberBridgeCmdLineOptions,
            ] )
        if len(opt)>0:
            cmdLine = cmdLine+" "+opt
        cclo = self.CreateCommonCmdLineOptions( config, parentNode, parentBridgeID, eventsExp2, False )
        controlURL = cclo[1]
        cmdLine = cmdLine+cclo[0]

        #sbhProc = DataFlowProcess( parentBridgeID, "sbh", cmdLine, "", [ parents[0] ], [ parentNode ], "invalid", 0, 0, parents, controlURL )
        sbhProc = MappedProcess( parentBridgeID, "sbh", cmdLine, "", controlURL, parents[0:1], parentNode, childID )


        
        platform = self.CreatePlatform( config, childNode )
        if self.fFrameworkDir!=None:
            cmdLine = self.fFrameworkDir+"/bin/"+platform+"/"
        else:
            cmdLine = ""
        cmdLine = cmdLine+"PublisherBridgeHeadNew -name "+childBridgeID
        cmdLine = cmdLine + " -publisher " + childBridgeID+"Pub"

        if self.fPublisherBridgeHeadShmTypes.has_key(childNode):
            shmType = self.fPublisherBridgeHeadShmTypes[childNode]
        else:
            shmType = self.fPublisherBridgeHeadDefaultShmType
        cmdLine = cmdLine + " -shm "+shmType+" " + ( "%d" % self.GetNextShmID( config, childNode, shm_id=childBridgeID ) )

            
        if self.fUseBridgeSlots:
            cmdLine = cmdLine + " -msg tcpmsg://"+config.GetNode( childNode ).GetHostname()+":" + ( "%d" % ( bridgeData["ReceiverBaseSocket"]+self.fBridgeMsgSocketOffset) )+"/slotslog2=8/ tcpmsg://"+config.GetNode(parentNode).GetHostname()+":"+( "%d" % (bridgeData["SenderBaseSocket"]+self.fBridgeMsgSocketOffset))
            cmdLine = cmdLine + " -blobmsg tcpmsg://"+config.GetNode( childNode ).GetHostname()+":" + ( "%d" % (bridgeData["ReceiverBaseSocket"]+self.fBridgeBlobMsgSocketOffset) )+"/slotslog2=8/ tcpmsg://"+config.GetNode(parentNode).GetHostname()+":"+( "%d" % (bridgeData["SenderBaseSocket"]+self.fBridgeBlobMsgSocketOffset))
            cmdLine = cmdLine + " -blob " + ("%d" % ((blocksize+2048)*blockcount) ) +" tcpblob://"+config.GetNode( childNode ).GetHostname()+":" + ( "%d" % (bridgeData["ReceiverBaseSocket"]+self.fBridgeBlobSocketOffset) ) + "/slotslog2=8/"
        else:
            cmdLine = cmdLine + " -msg tcpmsg://"+config.GetNode( childNode ).GetHostname()+":" + ( "%d" % ( bridgeData["ReceiverBaseSocket"]+self.fBridgeMsgSocketOffset) )+" tcpmsg://"+config.GetNode(parentNode).GetHostname()+":"+( "%d" % (bridgeData["SenderBaseSocket"]+self.fBridgeMsgSocketOffset))
            cmdLine = cmdLine + " -blobmsg tcpmsg://"+config.GetNode( childNode ).GetHostname()+":" + ( "%d" % (bridgeData["ReceiverBaseSocket"]+self.fBridgeBlobMsgSocketOffset) )+" tcpmsg://"+config.GetNode(parentNode).GetHostname()+":"+( "%d" % (bridgeData["SenderBaseSocket"]+self.fBridgeBlobMsgSocketOffset))
            cmdLine = cmdLine + " -blob " + ("%d" % ((blocksize+2048)*blockcount) ) +" tcpblob://"+config.GetNode( childNode ).GetHostname()+":" + ( "%d" % (bridgeData["ReceiverBaseSocket"]+self.fBridgeBlobSocketOffset) )
            

        opt = " ".join( [
            self.fDataFlowCmdLineOptions,
            self.fBridgeCmdLineOptions,
            self.fPublisherBridgeCmdLineOptions,
            ] )
        if len(opt)>0:
            cmdLine = cmdLine+" "+opt
        cclo = self.CreateCommonCmdLineOptions( config, childNode, childBridgeID, eventsExp2, True )
        controlURL = cclo[1]
        cmdLine = cmdLine+cclo[0]

        #pbhProc = DataFlowProcess( childBridgeID, "pbh", cmdLine, "", [], [ childNode ], "invalid", 0, 0, parents, controlURL )
        pbhProc = MappedProcess( childBridgeID, "pbh", cmdLine, "", controlURL, [], childNode, childID )

        return ( sbhProc, pbhProc )

    def CreateMerger( self, config, procID, mergerID, node, parents, eventsExp2, filters, maxLevel, add_timeout=0, shift_timeout=0, staged = 0 ):
        proc = config.GetProcess( procID )
        virtParents = proc.GetParents()
        platform = self.CreatePlatform( config, node )
        if self.fFrameworkDir!=None:
            cmdLine = self.fFrameworkDir+"/bin/"+platform+"/"
        else:
            cmdLine = ""
        configParams = ""
        if hasattr(proc, "fMergeCount") and staged == 0:
            nodeIndex = proc.FindNodeIndex( node, 0 )
            suffix = self.GetProcSuffix( proc, nodeIndex, config )
            instanceID = proc.fIDName+suffix
            publisherID = instanceID+"Pub"
            proc.fMergeCount = -1
        else:
            publisherID = mergerID+"Pub"
        cmdLine += "EventMergerNew"+" -publisher "+publisherID
        ndx=0
        for p in parents:
            #subscriberID = mergerID+("%d" % ndx )+"Sub"
            subscriberID = mergerID+"-"+p+"Sub"
            cmdLine += " -subscribe "+p+"Pub" + " "+subscriberID
            if p in filters.keys():
                cmdLine += self.CreateFilterCmdLineOptions( config, filters[p], True, subscriberID )
            ndx += 1

        if maxLevel == 0 and self.fEventMergerInputTimeout!=0:
            cmdLine = cmdLine + ( " -eventtimeout %u" % (self.fEventMergerInputTimeout) )
        else:
            if self.fEventMergerTimeout!=0:
                cmdLine = cmdLine + ( " -eventtimeout %u" % (self.fEventMergerTimeout+add_timeout) )
            if self.fEventMergerDynamicTimeout_MaxTimeout_ms!=0:
                cmdLine = cmdLine + ( " -dynamictimeouts %u %u" % (self.fEventMergerDynamicTimeout_MinTimeout_ms, self.fEventMergerDynamicTimeout_MaxTimeout_ms) )
                if shift_timeout:
                    cmdLine = cmdLine + ( " -eventtimeoutshift %u" % (shift_timeout) )
        if self.fEventMergerDynamicInputXabling_MaxMissingEventCnt!=0:
            cmdLine = cmdLine + ( " -dynamicinputXable %u" % (self.fEventMergerDynamicInputXabling_MaxMissingEventCnt) )

        if self.fNoStrictSOR:
            cmdLine = cmdLine + " -nostrictsor"

        opt = " ".join( [
            self.fDataFlowCmdLineOptions,
            self.fLocalDataFlowCmdLineOptions,
            self.fMergerCmdLineOptions,
            ] )
        if len(opt)>0:
            cmdLine = cmdLine+" "+opt
        cclo = self.CreateCommonCmdLineOptions( config, node, mergerID, eventsExp2, True )
        controlURL = cclo[1]
        cmdLine = cmdLine+cclo[0]
        #return DataFlowProcess( mergerID, "em", cmdLine, configParams, parents, [ node ], "invalid", 0, 0, virtParents, controlURL )
        ret = MappedProcess( mergerID, "em", cmdLine, configParams, controlURL, parents, node, virtParents )
        if hasattr(proc, "fMergeCount") and proc.fMergeCount == -1 and not staged:
            ret.fReplaces = instanceID
        return ret


    def CreateWorkerInstance( self, config, procID, node, parents, eventsExp2, filters, nodeIndex ):
        proc = config.GetProcess( procID )
        type = proc.GetType()
        cmdLine = os.path.expanduser( proc.GetCmd(nodeIndex) )
        suffix = self.GetProcSuffix( proc, nodeIndex, config )
        instanceID = proc.fIDName+suffix
        cmdLine = cmdLine + " -name "+instanceID
        if type != "snk" or proc.fComponent:
            publisherID = instanceID+"Pub"
            cmdLine = cmdLine + " -publisher "+publisherID
        if type != "src":
            if len(parents)>1:
                raise ChainConfigException( "INTERNAL ERROR: More than 1 parent for worker instance "+instanceID+" for process "+proc.fIDName+": "+str(parents) )
            cmdLine = cmdLine + " -subscribe "+parents[0]+"Pub "+instanceID+"Sub"
            if parents[0] in filters.keys():
                cmdLine += self.CreateFilterCmdLineOptions( config, filters[parents[0]], False, instanceID+"Sub" )
            myParents = parents[0:1]
        else:
            myParents = []
        if type == "src" and self.fMaxEventsInChain!=None:
            cmdLine = cmdLine + " -maxpendingevents %u" % self.fMaxEventsInChain
        # if proc.GetControlURL( nodeIndex )=="":
        #     controlURL = "tcpmsg://"+config.GetNode( node ).GetHostname()+":"+("%d" % self.GetNextSocket( config, node, controlled_process_id=instanceID ))+"/"
        #     proc.SetControlURL( nodeIndex, controlURL )
        # cmdLine = cmdLine + " -control "+MakeLocalURL( proc.GetControlURL( thisNodeIndex ) )
        if proc.GetShmType()!="invalid":
            if proc.GetShmType()=="librorc":
                shmtype = "librorc"
            elif proc.GetShmType()=="sysv":
                shmtype = "sysv"
            cmdLine = cmdLine + " -shm "+shmtype+(" %d" % (self.GetNextShmID( config, node, shm_id=instanceID )))+(" %d" % (proc.GetBlockSize()*proc.GetBlockCount()))+" -minblocksize "+("%d" % (proc.GetBlockSize()))

        if self.fMaxSubscriberRetryCount!=None and type=="prc":
            cmdLine = cmdLine + "  -subscriberretrycount %d" % self.fMaxSubscriberRetryCount
        if nodeIndex:
            cmdLine = cmdLine + " -secondary"

        cclo = self.CreateCommonCmdLineOptions( config, node, instanceID, None, type!="snk" )
        controlURL = cclo[1]
        proc.SetControlURL( nodeIndex, controlURL )
        cmdLine = cmdLine+cclo[0]
        
        ret = MappedProcess( instanceID, proc.GetType(), cmdLine, proc.GetConfigParams(), proc.GetControlURL( nodeIndex ), myParents, node )
        if type == "snk" and proc.fComponent:
            ret.fIsSink = 1
        return ret

    def CreateProcessList( self, config ):
#TIME        times = {}
#TIME        times[-1] = time.time()
        debugOutput = False
        #debugOutput = True
        if debugOutput:
            print "============== Handle eventsexp2 for all processes"
            print "!CreateProcess start"
       #graphVizNodes = set()
       #graphVizConnections = set()
        config.MakeProcessLevels()
        procList = []
        processList = []
#TIME        times[0] = time.time()
        parentsNeeded = array.array('i', (0 for _ in xrange(len(config.fProcesses))))
        for i in range(len(config.fProcesses)):
            numParents = 0
            for par in config.fProcesses[i].GetParents():
                if config.IsProcessGroup(par):
                    numParents += len(config.GetProcessGroup(par).GetFullOutputProcessListFlat(config))
                else:
                    numParents += 1
            parentsNeeded[i] = numParents

        for p in range(len(config.fProcesses)):
            if parentsNeeded[p]:
                continue
            parentsNeeded[p] = -200
           #graphVizNodes.add( p )
            if debugOutput:
                print "!CreateProcess source",p
            proc = config.GetProcess( p )
            if self.fMaxEventsInChainExp2==None:
                tmp = 1
                eventsexp2 = 0
                while tmp>0 and tmp<proc.GetBlockCount():
                    eventsexp2 = eventsexp2 + 1
                    tmp = 1 << eventsexp2
            else:
                eventsexp2 = self.fMaxEventsInChainExp2
            processList.append( self.CreateWorkerInstance( config, p, proc.GetNodes()[0], [], eventsexp2, [], 0 ) ) # Sources only have one node and no parents 
#TIME        times[1] = time.time()
        
        parentfilter = 0
        parentgatherer = 1
        parentscatterer = 2
        parentregatherer = 3
        parentbridge = 4
        childgatherer = 5
        childmerger = 6
        childscatterer = 7

        levelParentsNum = 8
        
        includedLevelParents = [0, 1, 2, 3, 4, 5, 6, 7]
        includedLevelParents[parentfilter] = [ parentfilter ]
        includedLevelParents[parentgatherer] = [ parentfilter,parentgatherer ]
        includedLevelParents[parentscatterer] = [ parentfilter, parentgatherer, parentscatterer ]
        includedLevelParents[parentregatherer] = [ parentfilter, parentgatherer, parentscatterer, parentregatherer ]
        includedLevelParents[parentbridge] = [ parentfilter, parentgatherer, parentscatterer, parentregatherer, parentbridge ]
        includedLevelParents[childgatherer] = [ parentfilter, childgatherer, parentregatherer, parentscatterer, parentgatherer ]
        includedLevelParents[childmerger] = [ parentfilter, childgatherer, parentregatherer, parentscatterer, parentgatherer, childmerger ]
        includedLevelParents[childscatterer] = [ parentfilter, childgatherer, parentregatherer, parentscatterer, parentgatherer, childmerger, childscatterer ]
        
        changed=True
#TIME        times[2] = time.time()
        for p in range(len(config.fProcesses)):
            if parentsNeeded[p] == -200:
                for j in config.GetChildProcesses(p):
                    parentsNeeded[j] -= 1
                if config.fProcesses[p].IsInProcessGroupOutputList:
                    procGroup = config.fProcesses[p].GetProcessGroup()
                    for j in config.GetGroupChildProcesses(procGroup.GetID()):
                        parentsNeeded[j] -= 1

#TIME        times[3] = time.time()
#TIME        ndxs = times.keys()
#TIME        ndxs.sort()
#TIME        for i in ndxs:
#TIME            if times.has_key(i) and times.has_key(i-1):
#TIME                print "di0%d: %f" % (i, times[i]-times[i-1])

#TIME        times = {}
#TIME        times[-1] = time.time()
#TIME        times[0] = times[1] = times[2] = times[3] = times[4] = times[5] = times[6] = times[7] = times[8] = 0
#TIME        times[0] = time.time()

        groupParentScatterers = {}
        groupChildGatherers = {}
        groupChildMergers = {}
        groupChildScatterers = {}
        groupChildReGatherers = {}
        while changed:
            changed = False

            for c in range(len(config.fProcesses)):
#TIME                times[1] += time.time()

                if parentsNeeded[c] == 0:
                    parentsNeeded[c] = -100
                    cproc = config.GetProcess(c)
                    parProcs = set([ x for x in cproc.GetParents() if not config.IsProcessGroup(x) ])
                    parGroups = set([ x for x in cproc.GetParents() if config.IsProcessGroup(x) ])

                    if cproc.GetEventTypeCode() and self.fAliceHLT:
                        temp = string.Template( proc.GetEventTypeCode() )
                        etc = temp.substitute( self.fEventTypeCodeMacros )
                    else:
                        etc = None
                    baseFilters = { "eventmodulo" : cproc.GetEventModulo(),
                                "eventtypecode" : etc }
                    changed = True
                    if self.fMaxEventsInChainExp2==None:
                        maxEventsExp2 = 0
                    else:
                        maxEventsExp2 = self.fMaxEventsInChainExp2
                    seqNrDistribution = self.fAllowScattererSeqNrDistribution
                    if (len(parProcs)+len(parGroups))>1:
                        seqNrDistribution = False
                    childNodes = cproc.GetNodes()
                    differentParGroup = False
                    childGroup = cproc.GetProcessGroup()
#TIME                    times[2] += time.time()
                    if childGroup!=None:
                        childGroupInternalID = childGroup.GetInternalProcessID( c, config )
#TIME                    times[3] += time.time()
                    levelParents = range(levelParentsNum)
                    for l in range(levelParentsNum):
                        levelParents[l] = range(len(config.fNodes))
                        for n in levelParents[l]:
                            levelParents[l][n] = []
                    #levelParents = [[[] for x in xrange(len(config.fNodes))] for y in xrange(levelParentsNum)]
                    #Well, creating this multi-dimensional list takes 5 seconds. If anyone can come up with a faster variant....

                    if childGroup!=None:
                        if childGroupInternalID==None:
                            print "INTERNAL Error: Cannot find child group internal ID",c, childGroup.GetID()
#TIME                    times[4] += time.time()
                #        levelParentsCache[c] = levelParents
                    #else:
                    #    levelParents = levelParentsCache[c]
                    parList = []
#                    maxParentDelay=0
#                    minParentDelay=-1
                    maxLevel=0
                    for p in parProcs:
                        filters = {}
                        parProc = config.GetProcess( p )
                        parNodes = parProc.GetNodes()
                        parGroup = parProc.GetProcessGroup()
                        attrStr = ""
                        childIDName = ""
                        if childGroup!=None and (parGroup==None or parGroup.GetID()!=childGroup.GetID()):
                            differentParGroup = True
                            childID = childGroup.GetID()+"-"+childGroupInternalID
                            childIDName = childID
                        else:
                            childID = c
                            childIDName = config.fProcesses[childID].fIDName
                        if self.fMaxEventsInChainExp2==None:
                            tmp = 1
                            eventsexp2 = 0
                            while tmp>0 and tmp<parProc.GetBlockCount():
                                eventsexp2 = eventsexp2 + 1
                                tmp = 1 << eventsexp2
                            if eventsexp2>maxEventsExp2:
                                maxEventsExp2 = eventsexp2
                        else:
                            eventsexp2 = self.fMaxEventsInChainExp2
                        if maxLevel<parProc.GetLevel():
                            maxLevel = parProc.GetLevel()
                        for node in set(parNodes):
#                            parentDelay = config.GetTotalProcessDelay( parProc, node )
#                            if parentDelay>maxParentDelay:
#                                maxParentDelay = parentDelay
#                            if minParentDelay==-1 or parentDelay<minParentDelay:
#                                minParentDelay=parentDelay
                            if childGroup!=None:
                                if not groupParentScatterers.has_key( p ):
                                    groupParentScatterers[p] = {}
                                if not groupParentScatterers[p].has_key( node ):
                                    groupParentScatterers[p][node] = {}
                                if not groupParentScatterers[p][node].has_key( childGroup.GetID() ):
                                    groupParentScatterers[p][node][childGroup.GetID()] = set()
                                    
                                if not groupChildGatherers.has_key( p ):
                                    groupChildGatherers[p] = {}
                                if not groupChildGatherers[p].has_key( node ):
                                    groupChildGatherers[p][node] = {}
                                if not groupChildGatherers[p][node].has_key( childGroup.GetID() ):
                                    groupChildGatherers[p][node][childGroup.GetID()] = set()
                                    
                                   
                            parProcCntOnNode = parProc.ProcCntOnNode(node)
                            for n in range(parProcCntOnNode):
                                filters[parProc.fIDName+self.GetProcSuffix( parProc, parProc.FindNodeIndex( node, n ) , config)] = baseFilters.copy()
                            if cproc.IsParentFiltered(p):
                                filterType = cproc.GetParentFilterType(p)
                                for n in range(parProcCntOnNode):
                                    filterID = node+"-"+p+"-4-"+childIDName+"-"+filterType[0].upper()+filterType[1:]+"Filter"+self.GetProcSuffix( parProc, parProc.FindNodeIndex( node, n ), config )
                                    levelParents[parentgatherer][node].append( filterID )
                                    parents = [ parProc.fID+self.GetProcSuffix( parProc, parProc.FindNodeIndex( node, n ) , config) ]
                                   #for pp in parents:
                                       #graphVizConnections.add( ( pp, filterID, attrStr ) )
                                   #graphVizNodes.add( filterID )
                                    if debugOutput:
                                        print "!CreateProcess ParentFilter",node,":",p,"->",c,"/",childIDName,"(",parents,")","-",filterID,"- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                                    processList.append( self.CreateFilter( config, p, filterID, node, parents, eventsexp2, filters, filterType ) )
                            else:
                                for n in range(parProcCntOnNode):
                                    levelParents[parentgatherer][node].append( parProc.fIDName+self.GetProcSuffix( parProc, parProc.FindNodeIndex( node, n ), config ) )
                            if not differentParGroup:
                                otherNodeCnt = len(set(cproc.GetNodeList())-set([node]))
                            else:
                                otherNodeCnt = len(childGroup.GetNodes())-childGroup.GetNodes().count(node)
                            if parProcCntOnNode>1 and otherNodeCnt>0:
                                # Create Parent Node Gatherer
                                gathererID = node+"-"+p+"-4-"+childIDName+"-NodeGatherer"
                                parents = []
                                for ilp in includedLevelParents[parentgatherer]:
                                    parents += levelParents[ilp][node]
                                    levelParents[ilp][node] = []
                               #for pp in parents:
                                   #graphVizConnections.add( ( pp, gathererID, attrStr ) )
                               #graphVizNodes.add( gathererID )
                                if debugOutput:
                                    print "!CreateProcess ParentNodeGatherer",node,":",config.fProcesses[p].fIDName,"->",config.fProcesses[c].fIDName,"/",childIDName,"(",parents,")","-",gathererID,"- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]

                                processList.append( self.CreateGatherer( config, config.fProcesses[p].fIDName, gathererID, node, parents, eventsexp2, filters ) )
                                levelParents[parentscatterer][node].append( gathererID )
                            nodeSpecificParents = {}
                            bridgeNodeSet = set()
                            if not differentParGroup:
                                if len(cproc.GetNodeList())>1:
                                    # Create Parent Node Scatterer
                                    scattererID = node+"-"+p+"-4-"+childIDName+"-Scatterer"
                                    parents = []
                                    for ilp in includedLevelParents[parentscatterer]:
                                        parents += levelParents[ilp][node]
                                        levelParents[ilp][node] = []
                                   #for pp in parents:
                                       #graphVizConnections.add( ( pp, scattererID, attrStr ) )
                                   #graphVizNodes.add( scattererID )
                                    for childNode in cproc.GetNodeList():
                                        cnt = cproc.ProcCntOnNode(childNode)
                                        nodeSpecificParents[childNode] = [ scattererID+"-%s-%03d" % (config.fNodes[childNode].fIDName,n) for n in range(cnt) ]
                                       #for nsp in nodeSpecificParents[childNode]:
                                           #graphVizConnections.add( ( scattererID, nsp, attrStr ) )
                                        levelParents[parentregatherer][node] += nodeSpecificParents[childNode]
                                        # levelParents[parentregatherer][childNode] += [ scattererID+"-%03d" % (b+n) for n in range(cnt) ]
                                    if debugOutput:
                                        print "!CreateProcess ParentNodeScatterer",node,":",p,"->",c,"/",childIDName,"(",parents,")","-",scattererID,"- # outputs:",len(childNodes),"- outputs:",levelParents[parentregatherer],"- nodeSpecificParents:",nodeSpecificParents,"- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                                    processList.append( self.CreateScatterer( config, c, scattererID, node, parents, eventsexp2, filters, levelParents[parentregatherer][node], seqNrDistribution ) )
                                bridgeNodeSet = (set(childNodes)-set([node]))
                                doBridge = True
                            else:
                                groupNodeMult = len(childGroup.GetNodes())
                                procMult = len(cproc.GetNodes())
                                childGroupScattererHandled = False
                                doBridge = True
                                if childGroupInternalID in groupParentScatterers[p][node][childGroup.GetID()]:
                                    childGroupScattererHandled = True
                                # if groupNodeMult*procMult>1:
                                if len(set(childGroup.GetNodes()))>1:
                                    scattererID = config.fNodes[node].fIDName+"-"+config.fProcesses[p].fIDName+"-4-"+childIDName+"-GroupScatterer"
                                    nodeCnt = {}
                                    for childNode in set(childGroup.GetNodes()):
                                        nodeCnt[childNode] = 0
                                        nodeSpecificParents[childNode] = []
                                    for childNode in childGroup.GetNodes():
                                        nodeSpecificParents[childNode] += [ scattererID+"-%s-%03d-%03d" % (config.fNodes[childNode].fIDName,nodeCnt[childNode],n) for n in range(procMult) ]
                                        nodeCnt[childNode] += 1
                                    parents = []
                                    for ilp in includedLevelParents[parentscatterer]:
                                        parents += levelParents[ilp][node]
                                        levelParents[ilp][node] = []
                                    if not childGroupScattererHandled:
                                        outputs = []
                                       #for pp in parents:
                                           #graphVizConnections.add( ( pp, scattererID, attrStr ) )
                                       #graphVizNodes.add( scattererID )
                                        for childNode in set(childGroup.GetNodes()):
                                           #for nsp in nodeSpecificParents[childNode]:
                                               #graphVizConnections.add( ( scattererID, nsp, attrStr ) )
                                            #levelParents[parentregatherer][node] += nodeSpecificParents[childNode]
                                            # levelParents[parentregatherer][childNode] += [ scattererID+"-%03d" % (b+n) for n in range(cnt) ]
                                            outputs += nodeSpecificParents[childNode]
                                        if debugOutput:
                                            print "!CreateProcess Group Child ParentNodeScatterer",node,":",config.fProcesses[p].fIDName,"->",config.fProcesses[c].fIDName,"/",childIDName,"(",parents,")","-",scattererID,"- # outputs:",groupNodeMult*procMult,"- outputs:",outputs,"- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                                        groupSeqNrDistribution=False
                                        processList.append( self.CreateScatterer( config, c, scattererID, node, parents, eventsexp2, filters, outputs, groupSeqNrDistribution ) )
                                        groupParentScatterers[p][node][childGroup.GetID()].add( childGroupInternalID )
                                    levelParents[parentregatherer][node] += nodeSpecificParents[cproc.GetNodes()[0]] # Group process, only one one in there with multiple instances
                                # bridgeNodeSet = (set(childGroup.GetNodes())-set([node]))
                                bridgeNodeSet = ( set(cproc.GetNodes())-set([node]) )
                            if doBridge:
                                for childNode in bridgeNodeSet:
                                    parents = []
                                    if len(nodeSpecificParents.keys())>0:
                                        parents = nodeSpecificParents[childNode]
                                        for pp in parents:
                                            levelParents[parentregatherer][node].remove( pp )
                                    else:
                                        for ilp in includedLevelParents[parentregatherer]:
                                            parents += levelParents[ilp][node]
                                            levelParents[ilp][node] = []
                                    # for ilp in includedLevelParents[parentregatherer]:
                                    #     parents += levelParents[ilp][childNode]
                                    #     levelParents[ilp][childNode] = []
                                    if len(parents)>1:
                                        gathererID = config.fNodes[node].fIDName+"-"+config.fProcesses[p].fIDName+"-4-"+childIDName+"-"+config.fNodes[childNode].fIDName+"-ReGatherer"
                                        if debugOutput:
                                            print "!CreateProcess ParentReGatherer",config.fNodes[node].fIDName,":",p,"->",config.fProcesses[c].fIDName,"/",childIDName,"(",config.fNodes[childNode].fIDName,")","(",parents,")","-",gathererID,"- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                                        processList.append( self.CreateGatherer( config, config.fProcesses[p].fIDName, gathererID, node, parents, eventsexp2, filters ) )
                                       #for pp in parents:
                                           #graphVizConnections.add( ( pp, gathererID, attrStr ) )
                                       #graphVizNodes.add( gathererID )
                                        parents = [ gathererID ]
                                    senderBridgeID = config.fNodes[node].fIDName+"-"+config.fProcesses[p].fIDName+"-4-"+childIDName+"-Bridge-"+config.fNodes[node].fIDName+"-2-"+config.fNodes[childNode].fIDName
                                    if debugOutput:
                                        print "!CreateProcess ParentBridge",config.fNodes[node].fIDName,":",config.fProcesses[p].fIDName,"->",childIDName,"(",childNode,")","(",parents,")",senderBridgeID,"- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                                   #for pp in parents:
                                       #graphVizConnections.add( ( pp, senderBridgeID, attrStr ) )
                                   #graphVizNodes.add( senderBridgeID )
                                    receiverBridgeID = config.fNodes[childNode].fIDName+"-"+config.fProcesses[p].fIDName+"-4-"+childIDName+"-Bridge-"+config.fNodes[node].fIDName+"-2-"+config.fNodes[childNode].fIDName
                                    if debugOutput:
                                        print "!CreateProcess ChildBridge",config.fNodes[childNode].fIDName,":",config.fProcesses[p].fIDName,"->",childIDName,"(",childNode,")","(",parents,")",receiverBridgeID,"- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                                    bhs = self.CreateBridgeHeads( config, p, c, senderBridgeID, receiverBridgeID, node, childNode, parents, eventsexp2, filters )
                                    for bh in bhs:
                                        processList.append( bh )
                                   #graphVizConnections.add( ( senderBridgeID, receiverBridgeID, attrStr ) )
                                   #graphVizNodes.add( receiverBridgeID )
                                    levelParents[childgatherer][childNode].append( receiverBridgeID )
                        for node in set(childNodes):
                            if childGroup!=None:
                                if not groupChildGatherers.has_key( p ):
                                    groupChildGatherers[p] = {}
                                if not groupChildGatherers[p].has_key( node ):
                                    groupChildGatherers[p][node] = {}
                                if not groupChildGatherers[p][node].has_key( childGroup.GetID() ):
                                    groupChildGatherers[p][node][childGroup.GetID()] = set()
                            parents = []
                            for ilp in includedLevelParents[childgatherer]:
                                parents += levelParents[ilp][node]
                                levelParents[ilp][node] = []
                            childGathererHandled = False
                            if differentParGroup and childGroupInternalID in groupChildGatherers[p][node][childGroup.GetID()]:
                                childGathererHandled = True
                            if len(parents)>1:
                                if hasattr(cproc, "fKeepScatteredInput"):
                                    keepScatteredParents = parents
                                elif not childGathererHandled:
                                    gathererID = config.fNodes[node].fIDName+"-"+config.fProcesses[p].fIDName+"-4-"+childIDName+"-Gatherer"
                                   #for pp in parents:
                                       #graphVizConnections.add( ( pp, gathererID, attrStr ) )
                                   #graphVizNodes.add( gathererID )
                                    if debugOutput:
                                        print "!CreateProcess ChildGatherer",node,":",config.fProcesses[p].fIDName,"->",childIDName,"(",parents,")", gathererID,"- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                                    processList.append( self.CreateGatherer( config, config.fProcesses[p].fIDName, gathererID, node, parents, eventsexp2, filters ) )
                                    levelParents[childmerger][node].append( gathererID )
                            else:
                                levelParents[childmerger][node] += parents
                            if differentParGroup:
                                groupChildGatherers[p][node][childGroup.GetID()].add( childGroupInternalID )
                            
                                        
                                            
#TIME                    times[5] += time.time()
                    for g in parGroups:
                        parGroup = config.GetProcessGroup( g )
                        outputProcs = parGroup.GetOutputProcesses()
                        if len(outputProcs)<=0:
                            raise ChainConfigException( "Process group "+g+" used as parent is specified as having no output processes." )
                        groupNodes = parGroup.GetNodes()
                        childIDName = ""
                        if childGroup!=None:
                            differentParGroup = True
                            childID = childGroup.GetID()+"-"+childGroupInternalID
                            childIDName = childID
                        else:
                            childID = c
                            childIDName = config.fProcesses[childID].fIDName
                        for op in outputProcs:
                            parentGroupOutput=cproc.GetParentGroupOutput()
                            if parentGroupOutput != None and g in parentGroupOutput.keys() and not op in parentGroupOutput[g]:
                                continue
                            for node in set(groupNodes):
                                if not differentParGroup:
                                    otherNodeCnt = len(set(cproc.GetNodeList())-set([node]))
                                else:
                                    otherNodeCnt = len(childGroup.GetNodes())-childGroup.GetNodes().count(node)
                                if childGroup!=None:
                                    if not groupParentScatterers.has_key( g+"/"+op ):
                                        groupParentScatterers[g+"/"+op] = {}
                                    if not groupParentScatterers[g+"/"+op].has_key( node ):
                                        groupParentScatterers[g+"/"+op][node] = {}
                                    if not groupParentScatterers[g+"/"+op][node].has_key( childGroup.GetID() ):
                                        groupParentScatterers[g+"/"+op][node][childGroup.GetID()] = set()
                                    
                                    if not groupChildGatherers.has_key( g+"/"+op ):
                                        groupChildGatherers[g+"/"+op] = {}
                                    if not groupChildGatherers[g+"/"+op].has_key( node ):
                                        groupChildGatherers[g+"/"+op][node] = {}
                                    if not groupChildGatherers[g+"/"+op][node].has_key( childGroup.GetID() ):
                                        groupChildGatherers[g+"/"+op][node][childGroup.GetID()] = set()
                                        
                                cntOnNode = groupNodes.count(node)
                                for n in range(cntOnNode):
                                    parGroupNdx = parGroup.GetNodeIndex( node, n )
                                    opID = parGroup.GetProcessPrefix( parGroupNdx )+op
                                    parProc = config.GetProcess( config.fProcessIDMap[opID] )
#                                    parentDelay = config.GetTotalProcessDelay( parProc, node )
#                                    if parentDelay>maxParentDelay:
#                                        maxParentDelay = parentDelay
#                                    if minParentDelay==-1 or parentDelay<minParentDelay:
#                                        minParentDelay=parentDelay
                                    if maxLevel<parProc.GetLevel():
                                        maxLevel = parProc.GetLevel()
                                    if self.fMaxEventsInChainExp2==None:
                                        tmp = 1
                                        eventsexp2 = 0
                                        while tmp>0 and tmp<parProc.GetBlockCount():
                                            eventsexp2 = eventsexp2 + 1
                                            tmp = 1 << eventsexp2
                                        if eventsexp2>maxEventsExp2:
                                            maxEventsExp2 = eventsexp2
                                    else:
                                        eventsexp2 = self.fMaxEventsInChainExp2
                                    mult = len(parProc.GetNodes()) # All in node as it is a group process
                                    for m in range(mult):
                                        filters[opID+self.GetProcSuffix( parProc, parProc.FindNodeIndex( node, m ), config )] = baseFilters.copy()
                                    if cproc.IsParentFiltered(g):
                                        filterType = cproc.GetParentFilterType(g)
                                        for m in range(mult):
                                            filterID = node+"-"+g+"-"+op+"-4-"+childIDName+"-"+filterType[0].upper()+filterType[1:]+"Filter"+self.GetProcSuffix( parProc, parProc.FindNodeIndex( node, m ), config )
                                            levelParents[parentgatherer][node].append( filterID )
                                            parents = [ opID+self.GetProcSuffix( parProc, parProc.FindNodeIndex( node, m ) , config) ]
                                           #for pp in parents:
                                               #graphVizConnections.add( ( pp, filterID, attrStr ) )
                                           #graphVizNodes.add( filterID )
                                            if debugOutput:
                                                print "!CreateProcess GroupParentFilter",node,":",g,"/",op,"->",c,"/",childIDName,"(",parents,")","-",filterID,"- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                                            processList.append( self.CreateFilter( config, g+"/"+op, filterID, node, parents, eventsexp2, filters, filterType ) )
                                    else:
                                        for m in range(mult):
                                            levelParents[parentgatherer][node].append( opID+self.GetProcSuffix( parProc, parProc.FindNodeIndex( node, m ) , config) )
                                if len(levelParents[parentgatherer][node])>1 and otherNodeCnt>0:
                                    # Create Parent Node Gatherer
                                    gathererID = config.fNodes[node].fIDName+"-"+g+"-"+op+"-4-"+childIDName+"-NodeGatherer"
                                    parents = []
                                    for ilp in includedLevelParents[parentgatherer]:
                                        parents += levelParents[ilp][node]
                                        levelParents[ilp][node] = []
                                   #for pp in parents:
                                       #graphVizConnections.add( ( pp, gathererID, attrStr ) )
                                   #graphVizNodes.add( gathererID )
                                    if debugOutput:
                                        print "!CreateProcess GroupParentNodeGatherer",node,":",g,"/",op,"->",c,"/",childIDName,"(",parents,")","-",gathererID,"- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                                    processList.append( self.CreateGatherer( config, g+"/"+op, gathererID, node, parents, eventsexp2, filters ) )
                                    levelParents[parentscatterer][node].append( gathererID )
                                nodeSpecificParents = {}
                                bridgeNodeSet = set()
                                if not differentParGroup:
                                    if len(cproc.GetNodeList())>1:
                                        # Create Parent Node Scatterer
                                        scattererID = node+"-"+g+"-"+op+"-4-"+childIDName+"-Scatterer"
                                        parents = []
                                        for ilp in includedLevelParents[parentscatterer]:
                                            parents += levelParents[ilp][node]
                                            levelParents[ilp][node] = []
                                       #for pp in parents:
                                           #graphVizConnections.add( ( pp, scattererID, attrStr ) )
                                       #graphVizNodes.add( scattererID )
                                        for childNode in cproc.GetNodeList():
                                            cnt = cproc.ProcCntOnNode(childNode)
                                            nodeSpecificParents[childNode] = [ scattererID+"-%s-%03d" % (config.fNodes[childNode].fIDName,n) for n in range(cnt) ]
                                           #for nsp in nodeSpecificParents[childNode]:
                                               #graphVizConnections.add( ( scattererID, nsp, attrStr ) )
                                            levelParents[parentregatherer][node] += nodeSpecificParents[childNode]
                                            # levelParents[parentregatherer][childNode] += [ scattererID+"-%03d" % (b+n) for n in range(cnt) ]
                                        if debugOutput:
                                            print "!CreateProcess GroupParentNodeScatterer",node,":",g+"/"+op,"->",c,"/",childIDName,"(",parents,")","-",scattererID,"- # outputs:",len(childNodes),"- outputs:",levelParents[parentregatherer],"- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                                        processList.append( self.CreateScatterer( config, c, scattererID, node, parents, eventsexp2, filters, levelParents[parentregatherer][node], seqNrDistribution ) )
                                    bridgeNodeSet = (set(childNodes)-set([node]))
                                    doBridge = True
                                else:
                                    groupNodeMult = len(childGroup.GetNodes())
                                    procMult = len(cproc.GetNodes())
                                    childGroupScattererHandled = False
                                    doBridge = True
                                    if childGroupInternalID in groupParentScatterers[g+"/"+op][node][childGroup.GetID()]:
                                        childGroupScattererHandled = True
                                    # if groupNodeMult*procMult>1:
                                    if len(set(childGroup.GetNodes()))>1:
                                        scattererID = config.fNodes[node].fIDName+"-"+g+"-"+op+"-4-"+childIDName+"-GroupScatterer"
                                        nodeCnt = {}
                                        for childNode in set(childGroup.GetNodes()):
                                            nodeCnt[childNode] = 0
                                            nodeSpecificParents[childNode] = []
                                        for childNode in childGroup.GetNodes():
                                            nodeSpecificParents[childNode] += [ scattererID+"-%s-%03d-%03d" % (config.fNodes[childNode].fIDName,nodeCnt[childNode],n) for n in range(procMult) ]
                                            nodeCnt[childNode] += 1
                                        parents = []
                                        for ilp in includedLevelParents[parentscatterer]:
                                            parents += levelParents[ilp][node]
                                            levelParents[ilp][node] = []
                                        if not childGroupScattererHandled:
                                           #for pp in parents:
                                               #graphVizConnections.add( ( pp, scattererID, attrStr ) )
                                           #graphVizNodes.add( scattererID )
                                            outputs = []
                                            for childNode in set(childGroup.GetNodes()):
                                               #for nsp in nodeSpecificParents[childNode]:
                                                   #graphVizConnections.add( ( scattererID, nsp, attrStr ) )
                                                outputs += nodeSpecificParents[childNode]
                                            # levelParents[parentregatherer][node] += nodeSpecificParents[childNode]
                                            # levelParents[parentregatherer][childNode] += [ scattererID+"-%03d" % (b+n) for n in range(cnt) ]
                                            if debugOutput:
                                                print "!CreateProcess Group Child GroupParentNodeScatterer",node,":",g+"/"+op,"->",c,"/",childIDName,"(",parents,")","-",scattererID,"- # outputs:",len(childNodes),"- outputs:",outputs,"- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                                            processList.append( self.CreateScatterer( config, c, scattererID, node, parents, eventsexp2, filters, outputs, seqNrDistribution ) )
                                            groupParentScatterers[g+"/"+op][node][childGroup.GetID()].add( childGroupInternalID )
                                        levelParents[parentregatherer][node] += nodeSpecificParents[cproc.GetNodes()[0]]
                                    # bridgeNodeSet = (set(childGroup.GetNodes())-set([node]))
                                    bridgeNodeSet = ( set(cproc.GetNodes())-set([node]) )
                                if doBridge:
                                    if not groupChildReGatherers.has_key( g+"/"+op ):
                                        groupChildReGatherers[g+"/"+op] = {}
                                    if not groupChildReGatherers[g+"/"+op].has_key( node ):
                                        groupChildReGatherers[g+"/"+op][node] = set()
                                    for childNode in bridgeNodeSet:
                                        parents = []
                                        if len(nodeSpecificParents.keys())>0:
                                            parents = nodeSpecificParents[childNode]
                                            for pp in parents:
                                                levelParents[parentregatherer][node].remove( pp )
                                        else:
                                            for ilp in includedLevelParents[parentregatherer]:
                                                parents += levelParents[ilp][node]
                                                levelParents[ilp][node] = []
                                        # for ilp in includedLevelParents[parentregatherer]:
                                        #     parents += levelParents[ilp][childNode]
                                        #     levelParents[ilp][childNode] = []
                                        if not childNode in groupChildReGatherers[g+"/"+op][node]:
                                            groupChildReGatherers[g+"/"+op][node].add(childNode)
                                            if len(parents)>1:
                                                gathererID = config.fNodes[node].fIDName+"-"+g+"-"+op+"-4-"+childIDName+"-"+config.fNodes[childNode].fIDName+"-ReGatherer"
                                                if debugOutput:
                                                    print "!CreateProcess ParentReGatherer",config.fNodes[node].fIDName,":",g,"/",op,"->",config.fProcesses[c].fIDName,"/",childIDName,"(",config.fNodes[childNode].fIDName,")","(",parents,")","-",gathererID,"- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                                                processList.append( self.CreateGatherer( config, g+"/"+op, gathererID, node, parents, eventsexp2, filters ) )
                                               #for pp in parents:
                                                   #graphVizConnections.add( ( pp, gathererID, attrStr ) )
                                               #graphVizNodes.add( gathererID )
                                                parents = [ gathererID ]
                                            senderBridgeID = config.fNodes[node].fIDName+"-"+g+"-"+op+"-4-"+childIDName+"-Bridge-"+config.fNodes[node].fIDName+"-2-"+config.fNodes[childNode].fIDName
                                            if debugOutput:
                                                print "!CreateProcess ParentBridge",node,":",g+"/"+op,"->",childIDName,"(",childNode,")","(",parents,")",senderBridgeID,"- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                                           #for pp in parents:
                                               #graphVizConnections.add( ( pp, senderBridgeID, attrStr ) )
                                           #graphVizNodes.add( senderBridgeID )
                                            receiverBridgeID = config.fNodes[childNode].fIDName+"-"+g+"-"+op+"-4-"+childIDName+"-Bridge-"+config.fNodes[node].fIDName+"-2-"+config.fNodes[childNode].fIDName
                                            if debugOutput:
                                                print "!CreateProcess ChildBridge",childNode,":",g+"/"+op,"->",childIDName,"(",childNode,")","(",parents,")",receiverBridgeID,"- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                                            bhs = self.CreateBridgeHeads( config, config.fProcessIDMap[opID], c, senderBridgeID, receiverBridgeID, node, childNode, parents, eventsexp2, filters )
                                            for bh in bhs:
                                                processList.append( bh )
                                           #graphVizConnections.add( ( senderBridgeID, receiverBridgeID, attrStr ) )
                                           #graphVizNodes.add( receiverBridgeID )
                                            levelParents[childgatherer][childNode].append( receiverBridgeID )
                            for node in set(childNodes):
                                if childGroup!=None:
                                    if not groupChildGatherers.has_key( g+"/"+op ):
                                        groupChildGatherers[g+"/"+op] = {}
                                    if not groupChildGatherers[g+"/"+op].has_key( node ):
                                        groupChildGatherers[g+"/"+op][node] = {}
                                    if not groupChildGatherers[g+"/"+op][node].has_key( childGroup.GetID() ):
                                        groupChildGatherers[g+"/"+op][node][childGroup.GetID()] = set()
                                parents = []
                                for ilp in includedLevelParents[childgatherer]:
                                    parents += levelParents[ilp][node]
                                    levelParents[ilp][node] = []
                                childGathererHandled = False
                                if differentParGroup and childGroupInternalID in groupChildGatherers[g+"/"+op][node][childGroup.GetID()]:
                                    childGathererHandled = True
                                if len(parents)>1:
                                    if not childGathererHandled:
                                        gathererID = config.fNodes[node].fIDName+"-"+g+"-"+op+"-4-"+childIDName+"-Gatherer"
                                       #for pp in parents:
                                           #graphVizConnections.add( ( pp, gathererID, attrStr ) )
                                       #graphVizNodes.add( gathererID )
                                        if debugOutput:
                                            print "!CreateProcess ChildGatherer",node,":",g,"/",op,"->",childIDName,"(",parents,")",gathererID,"- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                                        processList.append( self.CreateGatherer( config, g+"/"+op, gathererID, node, parents, eventsexp2, filters ) )
                                        levelParents[childmerger][node].append( gathererID )
                                else:
                                    levelParents[childmerger][node] += parents
                                if differentParGroup:
                                    groupChildGatherers[g+"/"+op][node][childGroup.GetID()].add( childGroupInternalID )
                                            
                                    
                                    
                    
#TIME                    times[6] += time.time()
                    for node in set(childNodes):
                        if childGroup!=None:
                            if not groupChildMergers.has_key( childGroup.GetID() ):
                                groupChildMergers[childGroup.GetID()] = {}
                            if not groupChildMergers[childGroup.GetID()].has_key( node ):
                                groupChildMergers[childGroup.GetID()][node] = set()
                                    
                            if not groupChildScatterers.has_key( childGroup.GetID() ):
                                groupChildScatterers[childGroup.GetID()] = {}
                            if not groupChildScatterers[childGroup.GetID()].has_key( node ):
                                groupChildScatterers[childGroup.GetID()][node] = set()
                        parents = []
                        for ilp in includedLevelParents[childmerger]:
                            parents += levelParents[ilp][node]
                            levelParents[ilp][node] = []
                            
                        childIDName = ''
                        if differentParGroup:
                            childID = childGroup.GetID()+"-"+childGroupInternalID
                            childIDName = childID
                        else:
                            childID = c
                            childIDName = config.GetProcess(childID).fIDName
                        childGroupMergerHandled = False
                        if differentParGroup and childGroupInternalID in groupChildMergers[childGroup.GetID()][node]:
                            childGroupMergerHandled = True
                        if len(parents)>1:
                            if not childGroupMergerHandled:
                                mergerID = config.fNodes[node].fIDName+"-"+childIDName+"-Merger"
                                if debugOutput:
                                    print "!CreateProcess ChildMerger",node,":",config.fProcesses[c].fIDName,"/",childIDName,"(",parents,")",mergerID,"- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                                do_event_timeout = True
                                stage = 0
                                if not hasattr(cproc, "fMergeCount") or cproc.fMergeCount == 0:
                                    myMergerInputCountLimit = self.fMergerInputCountLimit
                                else:
                                    myMergerInputCountLimit = cproc.fMergeCount
                                if myMergerInputCountLimit>0:
                                    while len(parents)>myMergerInputCountLimit:
                                        # print "Merger stage",stage,inputs
                                        new_parents = []
                                        mergerCount = len(parents) / myMergerInputCountLimit
                                        if len(parents) % myMergerInputCountLimit:
                                            mergerCount += 1
                                        mergerInputs = [ len(parents)/mergerCount ]*mergerCount
                                        tmp=mergerInputs[0]*mergerCount
                                        ndx=0
                                        while tmp<len(parents):
                                            mergerInputs[ndx] += 1
                                            tmp += 1
                                            ndx = (ndx+1) % mergerCount
                                        offset = 0
                                        for ndx in range(0,len(mergerInputs)):
                                            parent_sublist = parents[offset:offset+mergerInputs[ndx]]
                                            myMergerID = mergerID+("-stage-%d-%d" % ( stage, ndx ))
#                                            myDelay = maxParentDelay-minParentDelay
                                            myDelay = 3000 #Let us just assume 3000 for everything
                                            processList.append( self.CreateMerger( config, c, myMergerID, node, parent_sublist, eventsexp2, filters, maxLevel, myDelay+maxLevel*self.fEventMergerTimeoutIncrease, maxLevel*self.fEventMergerTimeoutIncrease+((self.fEventMergerTimeoutIncrease/10)*stage), 1 ) )
                                           #for p in parent_sublist:
                                               #graphVizConnections.add( ( p, myMergerID, attrStr ) )
                                           #graphVizNodes.add( myMergerID )
                                            offset += mergerInputs[ndx]
                                            new_parents.append( myMergerID )
                                        parents = new_parents
                                        stage += 1
                                        # do_event_timeout = False


#                                myDelay = maxParentDelay-minParentDelay
                                myDelay = 3000 #Let us just assume 3000 for everything
                                processList.append( self.CreateMerger( config, c, mergerID, node, parents, eventsexp2, filters, maxLevel, myDelay+maxLevel*self.fEventMergerTimeoutIncrease, maxLevel*self.fEventMergerTimeoutIncrease+((self.fEventMergerTimeoutIncrease/10)*stage) ) )
                                levelParents[childscatterer][node].append( mergerID )
                               #for pp in parents:
                                   #graphVizConnections.add( ( pp, mergerID, attrStr ) )
                               #graphVizNodes.add( mergerID )
                        else:
                            levelParents[childscatterer][node] += parents
                        if differentParGroup:
                            groupChildMergers[childGroup.GetID()][node].add( childGroupInternalID )
                        if differentParGroup:
                            cntOnNode = childGroup.GetNodes().count(node)*len(cproc.GetNodes())
                        else:
                            cntOnNode = cproc.ProcCntOnNode(node)
                        childGroupScattererHandled = False
                        if differentParGroup and childGroupInternalID in groupChildScatterers[childGroup.GetID()][node]:
                            childGroupScattererHandled = True
                        scattererID = config.fNodes[node].fIDName+"-"+childIDName+"-NodeScatterer"
                        if cntOnNode>1 and not childGroupScattererHandled and not hasattr(cproc, "fKeepScatteredInput"):
                            parents = levelParents[childscatterer][node]
                           #for pp in parents:
                               #graphVizConnections.add( ( pp, scattererID, attrStr ) )
                           #graphVizNodes.add( scattererID )
                            scattererOutputs = [ scattererID+"-%03d" % n for n in range(cntOnNode) ]
                           #for so in scattererOutputs:
                               #graphVizConnections.add( ( scattererID, so, attrStr ) )
                               #graphVizNodes.add( so )
                            if debugOutput:
                                print "!CreateProcess ChildScatterer",node,":",config.fProcesses[c].fIDName,"/",childIDName,"(",parents,")",scattererID,"- # outputs:",len(scattererOutputs),"- outputs:",scattererOutputs,"- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                            processList.append( self.CreateScatterer( config, c, scattererID, node, parents, eventsexp2, filters, scattererOutputs, self.fAllowScattererSeqNrDistribution ) )
                        if differentParGroup:
                            groupChildScatterers[childGroup.GetID()][node].add( childGroupInternalID )
                        workerProcesses = []
                        if cntOnNode>1 :
                            if hasattr(c, "fMergeCount"):
                                raise ChainConfigException("Cannot create an explicit merger with multiplicity")
                            if differentParGroup:
                                inputNdx=0
                                for gm in range(childGroup.GetNodes().count(node)):
                                    parGroupNdx = childGroup.GetNodeIndex( node, gm )
                                    procID = config.fProcessIDMap[childGroup.GetProcessPrefix( parGroupNdx )+childGroupInternalID]
                                    if procID==c:
                                        for pm in range(cproc.ProcCntOnNode(node)):
                                            instanceID = config.fProcesses[procID].fIDName+self.GetProcSuffix( cproc, cproc.FindNodeIndex( node, pm), config )
                                           #graphVizNodes.add( instanceID )
                                            if hasattr(cproc, "fKeepScatteredInput"):
                                                parentID = keepScatteredParents[inputNdx+pm]
                                            else:
                                                parentID = scattererID+"-%03d" % ( inputNdx+pm )
                                           #graphVizConnections.add( ( parentID, instanceID, attrStr ) )
                                            if debugOutput:
                                                print "!CreateProcess Process",node,instanceID,"/",config.fProcesses[c].fIDName,"/",childIDName,"(",parentID,")","- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                                            processList.append( self.CreateWorkerInstance( config, procID, node, [parentID], eventsexp2, filters, cproc.FindNodeIndex( node, pm ) ) )
                                            workerProcesses.append( processList[-1] )
                                    inputNdx += cproc.ProcCntOnNode(node)
                            else:
                                for pm in range(cproc.ProcCntOnNode(node)):
                                    instanceID = config.fProcesses[c].fIDName+self.GetProcSuffix( cproc, cproc.FindNodeIndex( node, pm), config )
                                   #graphVizNodes.add( instanceID )
                                    if hasattr(cproc, "fKeepScatteredInput"):
                                        parentID = keepScatteredParents[pm]
                                    else:
                                        parentID = scattererID+"-%03d" % ( pm )
                                   #graphVizConnections.add( ( parentID, instanceID, attrStr ) )
                                    if debugOutput:
                                        print "!CreateProcess Process",node,instanceID,"/",config.fProcesses[c].fIDName,"/",childIDName,"(",parentID,")","- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                                    processList.append( self.CreateWorkerInstance( config, c, node, [parentID], eventsexp2, filters, cproc.FindNodeIndex( node, pm ) ) )
                                    workerProcesses.append( processList[-1] )
                        elif not (hasattr(config.fProcesses[c], "fMergeCount") and config.fProcesses[c].fMergeCount == -1):
#                        else:
                            instanceID = config.GetProcess(c).fIDName+self.GetProcSuffix( cproc, cproc.FindNodeIndex( node, 0 ), config )
                           #graphVizNodes.add( instanceID )
                            parentID = levelParents[childscatterer][node][0]
                           #graphVizConnections.add( ( parentID, instanceID, attrStr ) )
                            if debugOutput:
                                print "!CreateProcess Process",node,instanceID,"/",config.fProcesses[c].fIDName,"/",childIDName,"(",parentID,")","- filters:",[ filters[x] for x in ( set(parents) & set(filters.keys()) ) ]
                            processList.append( self.CreateWorkerInstance( config, c, node, [parentID], eventsexp2, filters, cproc.FindNodeIndex( node, 0 ) ) )
                            workerProcesses.append( processList[-1] )
                        for workerProc in workerProcesses:
                            workerProc.SetEventModulo(  cproc.GetEventModulo() )
                            workerProc.SetEventTypeCode(  cproc.GetEventTypeCode() )
                        

                            
#TIME                    times[7] += time.time()
                    for j in config.GetChildProcesses(c):
                        parentsNeeded[j] -= 1
                    if config.fProcesses[c].IsInProcessGroupOutputList:
                        procGroup = config.fProcesses[c].GetProcessGroup()
                        for j in config.GetGroupChildProcesses(procGroup.GetID()):
                            parentsNeeded[j] -= 1
#TIME                    times[8] += time.time()

#TIME        ndxs = times.keys()
#TIME        ndxs.sort()
#TIME        for i in ndxs:
#TIME            if times.has_key(i) and times.has_key(i-1) and i != 1 and i != 3:
#TIME                print "dx%d: %f" % (i, times[i]-times[i-1])


#TIME        times = {}
#TIME        times[-1] = time.time()
        if debugOutput:
            print "Created Processlist:",processList
            for proc in processList:
                print proc.GetType(),":",proc.GetCmd(),"(",proc.GetConfigParams(),")"
            outFile = open( config.GetName()+"-CreateProcessListNew-ProcessList.out", "w" )
            for node in config.GetNodes():
                outFile.write( node.fID+":\n\n" )
                for proc in [ p for p in processList if p.GetNode()==node.fID ]:
                    outFile.write( proc.GetType()+" : "+proc.GetCmd()+" ( "+proc.GetConfigParams()+" )\n\n" )
                outFile.write( "\n\n" )
            outFile.close()

#            outFile = open( config.GetName()+"-CreateProcessListNew-Processes-Graph.dot", "w" )
#            outFile.write( "digraph G {\n" )
#            outFile.write( "node [shape=box,height=0]\n" )
#            connectedNodes = set()
#            for c ingraphVizConnections:
#                outFile.write( "\""+c[0]+"\" -> \""+c[1]+"\""+c[2]+"\n" )
#                connectedNodes |= set( [ c[0], c[1] ] )
#            # unconnectedNodes = connectedNodes -graphVizNodes
#            unconnectedNodes = set(config.GetSources()) - connectedNodes
#            for n in unconnectedNodes:
#                outFile.write( "\""+proc.fID+"\"\n" )
#            outFile.write( "}\n" )
#            outFile.close()

            print "!CreateProcess stop"

        #workaround, ProcessGroup feature sometime produces duplicate processes
#        uniqueProcesses = {}
#        double = 0
#        for proc in processList:
#            if uniqueProcesses.has_key(proc.GetID()):
#                print "Double ", proc.GetID()
#        #        print uniqueProcesses[proc.GetID()]
#        #        print proc
#                double += 1
#            uniqueProcesses[proc.GetID()] = proc
#        print "Doubles ", double
#
#        processList=uniqueProcesses.values()

        for proc in processList:
            self.fPubSubConnections[proc.fID] = {}
            pubNext=False
            subNext=False
            subIDNext=False
            subPubID=None
            msgNextNext=False
            msgNext=False
            filterModuloSubscriberIDNextNext=False
            filterModuloSubscriberIDNext=False
            filterModuloSubscriberID=None
            filterETCSubscriberIDNextNext=False
            filterETCSubscriberIDNext=False
            filterETCSubscriberID=None
            filterETC=None
            
            for tok in (proc.GetCmd()+" "+proc.GetConfigParams()).split(" "):
                if pubNext:
                    self.fPublisher2Proc[tok] = proc
                    AddItem(self.fProcID2Publisher, proc.fID, tok )
                    pubNext=False
                    continue
                if subIDNext:
                    subIDNext=False
                    filterModuloSubscriberID = tok
                    filterETCSubscriberID = tok
                    AddItem( self.fPubSubConnections[proc.fID], subPubID, tok )
                    continue
                if subNext:
                    AddItem( self.fSubscription2Proc, tok, proc )
                    AddItem(self.fProcID2Subscription, proc.fID, tok )
                    subNext=False
                    subIDNext=True
                    subPubID=tok
                    continue
                if filterModuloSubscriberIDNext:
                    AddItem( self.fSubscriberID2Attributes, filterModuloSubscriberID, ( "modulo", tok ) )
                    filterModuloSubscriberIDNext=False
                    continue
                if filterModuloSubscriberIDNextNext:
                    filterModuloSubscriberID = tok
                    filterModuloSubscriberIDNextNext=False
                    filterModuloSubscriberIDNext=True
                    continue
                if filterETCSubscriberIDNext:
                    if filterETC==None:
                        if tok[0]!="\"":
                            AddItem( self.fSubscriberID2Attributes, filterETCSubscriberID, ( "etc", tok ) )
                            filterETCSubscriberIDNext=False
                        else:
                            filterETC=tok[1:]
                    else:
                        if tok[-1]=="\"":
                            AddItem( self.fSubscriberID2Attributes, filterETCSubscriberID, ( "etc", filterETC+" "+tok[:-1] ) )
                            filterETCSubscriberIDNext=False
                            filterETC=None
                        else:
                            filterETC = filterETC + " " + tok
                    continue
                if filterETCSubscriberIDNextNext:
                    filterETCSubscriberID = tok
                    filterETCSubscriberIDNextNext=False
                    filterETCSubscriberIDNext=True
                    continue
                if tok=="-publisher":
                    pubNext=True
                    continue
                if tok=="-subscribe":
                    subNext=True
                    continue
                if tok=="-eventmodulo":
                    filterModuloSubscriberIDNext=True
                    continue
                if tok=="-subscribereventmodulo":
                    filterModuloSubscriberIDNextNext=True
                    continue
                if tok=="-eventtypecode":
                    filterETCSubscriberIDNext=True
                    continue
                if tok=="-subscribereventtypecode":
                    filterETCSubscriberIDNextNext=True
                    continue
                if msgNext:
                    # if proc.GetType()=="pbh":
                    self.fBridgeReceiverMsg2Proc[tok] = proc
                    msgNext=False
                    continue
                if msgNextNext:
                    # if proc.GetType()=="sbh":
                    self.fBridgeSenderProcID2Msg[proc.fID] = tok
                    msgNext=True
                    msgNextNext=False
                    continue
                if tok=="-msg":
                    msgNextNext=True
                    continue
#TIME        times[0] = time.time()

        self.CheckShms( config, processList )
        #for proc in procList:
        #    print proc.fNode+":"+proc.fID,":",proc.fCommand,"("+proc.fConfigParams,")"
#TIME        times[1] = time.time()
        if self.fAliceHLT and self.fMergerTriggerClassInputDisabling:
            self.AddMergerTriggerClassInputDisabling( config, processList)
#TIME        times[2] = time.time()
        self.AddMergerInputModulosAndEventTypeCode( config, processList)
#TIME        times[3] = time.time()
#TIME        ndxs = times.keys()
#TIME        ndxs.sort()
#TIME        for i in ndxs:
#TIME            if times.has_key(i) and times.has_key(i-1):
#TIME                print "di2%d: %f" % (i, times[i]-times[i-1])
        return processList


    def AddMergerTriggerClassInputDisabling( self, config, procList ):
        # print "WARNING: DISABLED AddMergerTriggerClassInputDisabling"
        # return 
#TIME        times = {}
#TIME        times[-1] = time.time();
        procs = {}
        procKeys = []
        ddlInputs = {}
        mergerKeys = []
        for proc in procList:
            if proc.GetType()=="src":
                cmds = (proc.GetCmd()+" "+proc.GetConfigParams()).split()
                if cmds.count("-ddlid")<=0:
                    raise ChainConfigException( "No -ddlid parameter for process '"+proc.fID )
                if cmds.count("-ddlid")>1:
                    raise ChainConfigException( "More than one -ddlid parameter for process '"+proc.fID )
                i=cmds.index("-ddlid")
                if i+1<len(cmds):
                    ddlInputs[proc.GetID()] = [cmds[i+1]]
                else:
                    raise ChainConfigException( "No DDL ID specified after -ddlid parameter for process '"+proc.fID )
                for cp in self.GetChildren( procList, proc ):
                    if not procs.has_key(cp.fID):
                        procs[cp.fID] = cp
                        procKeys.append(cp.fID)
        found=True
#TIME        times[0] = time.time();
        while len(procKeys)>0 and found:
            found=False
            retryProcs = []
            for procKey in procKeys:
                proc = procs[procKey]
                ddls = []
                parents = self.GetParents( procList, proc )
                parStr=""
                aborted=False
                for par in parents:
                    if not ddlInputs.has_key(par.fID):
                        retryProcs.append(procKey)
                        aborted=True
                        break
                    ddls += ddlInputs[par.fID]
                    parStr += " "+par.fID
                if aborted:
                    continue # parent not found in for loop above, try next process
                found=True
                ddls = list(set(ddls))
                ddls.sort()
                ddlInputs[procKey] = ddls
                if hasattr(proc, "fReplaces"):
                    ddlInputs[proc.fReplaces] = ddls
                if proc.GetType()=="em":
                    mergerKeys.append(procKey)
                if proc.GetType()=="sbh":
                    pbh = self.GetBridgeReceiverPartner( proc )
                    pbhID = pbh.fID
                    procs[pbhID] = pbh
                    ddlInputs[pbhID] = ddls
                    for cp in self.GetChildren( procList, pbh ):
                        if not procs.has_key(cp.fID):
                            procs[cp.fID] = cp
                            retryProcs.append(cp.fID)
                else:
                    for cp in self.GetChildren( procList, proc ):
                        if not procs.has_key(cp.fID):
                            procs[cp.fID] = cp
                            retryProcs.append(cp.fID)
            procKeys = retryProcs
        if len(procKeys)>0:
            IDs = ""
            for k in procKeys:
                IDs += " "+k
            raise ChainConfigException( "Error determining parent DDL IDs for processes "+IDs )
        
#TIME        times[1] = time.time();
        for merger in mergerKeys:
            proc = procs[merger]

            pubIDs = self.ProcID2SubscriptionList(proc.fID)
            for pubID in pubIDs:
                pp = self.Publisher2Proc(pubID)
                if pp!=None:
                    ddls = ddlInputs[pp.fID]
                    ddlStr=""
                    for ddl in ddls:
                        ddlStr += ddl+","
                    proc.fCommand = proc.fCommand + " -subscriberinputddllist "+merger+"-"+pubID[:-3]+"Sub "+ddlStr[:-1]
                else:
                    raise ChainConfigException( "INTERNAL ERROR (A): Parent publisher process "+pubID+" for process "+proc.fID+" cannot be found (A)." )

#TIME        times[2] = time.time();
#TIME        ndxs = times.keys()
#TIME        ndxs.sort()
#TIME        for i in ndxs:
#TIME            if times.has_key(i) and times.has_key(i-1):
#TIME                print "dm2%d: %f" % (i, times[i]-times[i-1])


            ### Disabled because -triggerclassddllist is not needed in dataflow components anymore.
            ### for tc in self.fTriggerClasses.keys():
            ###     ddls = list(self.fTriggerClasses[tc])
            ###     ddls.sort()
            ###     ddlStr=""
            ###     for ddl in ddls:
            ###         ddlStr += str(ddl)+","
            ###     proc.fCommand = proc.fCommand + " -triggerclassddllist "+("%d " % tc)+ddlStr[:-1]

    def lcm_stein( self, a, b ):
        origa,origb=a,b
        # Calculates least common multiple lcm, uses stein's algorithm for greatest common denominator gcd and then uses the relation lcm = a*b/gcd
        k = 0
        while (not (a&1)) and (not (b&1)):
            a = a >> 1
            b = b >> 1
            k += 1
        if a & 1:
            t = -b
        else:
            t = a
        while t:
            while not (t&1):
                t = t>>1
            if t>0:
                a = t
            else:
                b = -t
            t = a-b
        gcd = a*(1<<k)
        return origa*origb/gcd
            
             

    def AddMergerInputModulosAndEventTypeCode( self, config, procList ):
        # print "WARNING: DISABLED AddMergerInputModulosAndEventTypeCode"
        # return
        procs = {}
        procKeys = []
        modulos = {}
        eventTypeCodes = {}
        mergerKeys = []
        # print "Processes:",
        # for proc in procList[:-1]:
        #     print proc.fID,",",
        # print procList[-1].fID
        for proc in procList:
            if proc.GetType()=="src":
                cmds = (proc.GetCmd()+" "+proc.GetConfigParams()).split()
                modulos[proc.fID] = [1]
                eventTypeCodes[proc.fID] = ""
                # print "Process "+proc.GetID()+" DDLS: "+str(ddlInputs[proc.GetID()])
                for cp in self.GetChildren( procList, proc ):
                    if not procs.has_key(cp.fID):
                        # print "Adding process "+proc.GetID()+" child "+cp.fID
                        procs[cp.fID] = cp
                        procKeys.append(cp.fID)
        found=True
        while len(procKeys)>0 and found:
            # print "(Re-)start loop",len(procKeys),procKeys
            found=False
            retryProcs = []
            while len(procKeys)>0:
                # print procKeys[0]
                proc = procs[procKeys[0]]
                procModulo = proc.GetEventModulo()
                procETT = proc.GetEventTypeCode()
                if procModulo==None or procModulo==0:
                    modulo = 1
                else:
                    modulo = procModulo
                if procETT==None or procETT=="":
                    ett = ""
                else:
                    ett = procETT
                procModulos = []
                procETTs = []
                #parents = proc.GetParents()
                parents = self.GetParents( procList, proc )
                parStr=""
                for par in parents:
                    parStr += " "+par.fID
                # print proc.fID,"parents:",parStr
                aborted=False
                for par in parents:
                    if not modulos.has_key(par.fID) or not eventTypeCodes.has_key(par.fID):
                        # print "Parent not found for "+procKeys[0]+": "+par.fID
                        # print procKeys[1:]
                        # print "modulos:",modulos
                        # print "eventTypeCodes:",eventTypeCodes
                        retryProcs.append(procKeys[0])
                        procKeys = procKeys[1:]
                        aborted=True
                        break
                    #parentModulos += modulos[par.fID]
                    for pm in modulos[par.fID]:
                        procModulos.append( self.lcm_stein( modulo, pm ) )
                    for et in eventTypeCodes[par.fID]:
                        if ett=="":
                            procETTs.append( et )
                        else:
                            procETTs.append( ett+";"+et+";LAND" )
                if aborted:
                    continue # parent not found in for loop above, try next process
                if not len(procModulos):
                    procModulos.append(modulo)
                if not len(procETTs):
                    procETTs.append( ett )
                if 1 in procModulos:
                    procModulos = [ 1 ]
                if "" in procETTs:
                    procETTs = []
                modulos[procKeys[0]] = procModulos
                eventTypeCodes[procKeys[0]] = procETTs
                if hasattr(proc, "fReplaces"):
                    modulos[proc.fReplaces] = procModulos
                    eventTypeCodes[proc.fReplaces] = procETTs
                found=True
                # print "Process "+procKeys[0]+" DDLS: "+str(ddls)
                if proc.GetType()=="em":
                    mergerKeys.append(procKeys[0])
                if proc.GetType()=="sbh":
                    # print "sbh"
                    # print proc.fID
                    pbh = self.GetBridgeReceiverPartner( proc )
                    # print "pbh:",pbh.fID
                    pbhID = pbh.fID
                    procs[pbhID] = pbh
                    modulos[pbhID] = procModulos
                    eventTypeCodes[pbhID] = procETTs
                    # print "pbh children:",[ p.fID for p in self.GetChildren( procList, pbh ) ]
                    for cp in self.GetChildren( procList, pbh ):
                        if not procs.has_key(cp.fID):
                            # print "Adding process "+proc.GetID()+" pbh "+pbhID+" child "+cp.fID
                            procs[cp.fID] = cp
                            procKeys.append(cp.fID)
                        # else:
                        #     print "Process already found "+proc.GetID()+" pbh "+pbhID+" child "+cp.fID
                else:
                    for cp in self.GetChildren( procList, proc ):
                        if not procs.has_key(cp.fID):
                            # print "Adding process "+proc.GetID()+" child "+cp.fID
                            procs[cp.fID] = cp
                            procKeys.append(cp.fID)
                procKeys = procKeys[1:]
                # print procKeys
            procKeys += retryProcs
##            for rp in retryProcs:
##                foundretry=False
##                for prc in procList:
##                    if prc.fID==rp:
##                        print "Found retry proc",rp," in proclist"
##                        #found=True
##                        foundretry=True
##                        break
##                if not foundretry:
##                    print "Cannot find retry proc",rp," in proclist"
##                    break
        if len(procKeys)>0:
            missingMod = [ x for x in procKeys if not x in modulos.keys() ]
            missingETT = [ x for x in procKeys if not x in eventTypeCodes.keys() ]
            print "modulos:",modulos
            print "missing modulos:",missingMod
            print "eventTypeCodes:",eventTypeCodes
            print "missing eventTypeCodes:",missingETT
            IDs = ""
            for k in procKeys:
                IDs += " "+k
            raise ChainConfigException( "Error determining all parent modulos for processes "+IDs )

        
        for merger in mergerKeys:
            proc = procs[merger]
            #parents = proc.GetParents()

            
##            parents = self.GetParents( procList, proc )
##            for par in parents:
##                ddls = ddlInputs[par.fID]
##                ddlStr=""
##                for ddl in ddls:
##                    ddlStr += ddl+","
##                proc.fCommand = proc.fCommand + " -subscriberinputddllist "+merger+"-"+par.fID+"Sub "+ddlStr[:-1]

            pubIDs = self.ProcID2SubscriptionList(proc.fID)
            for pubID in pubIDs:
                pp = self.Publisher2Proc(pubID)
                if pp!=None:
                    procModulos = modulos[pp.fID]
                    for mod in procModulos:
                        if mod>1:
                            proc.fCommand = proc.fCommand + " -subscriberinputmodulo "+merger+"-"+pubID[:-3]+"Sub "+("%d" % mod)
                    procETTs = eventTypeCodes[pp.fID]
                    for et in procETTs:
                        if et!="":
                            proc.fCommand = proc.fCommand + " -subscriberinputeventtypecode "+merger+"-"+pubID[:-3]+"Sub "+("\"%s\"" % et)
                else:
                    raise ChainConfigException( "INTERNAL ERROR (B): Parent publisher process "+pubID+" for process "+proc.fID+" cannot be found (B)." )

                

            

    def InitLists( self ): # , config
        self.fNodeSockets = {}
        self.fNodeShms = {}
        self.fNodeBridgeSockets = {}
        self.fProcs = []
        self.fBridges = {}
        self.fMergerIDs = {}
        self.fControlProcSockets = {}
        self.fShmIDs = {}
        #tmpProcs = config.GetProcesses()
        #for p in tmpProcs:
        #    self.fProcs.append( " "+tmpProcs )

    def GetControlProcessSocketList( self ):
        return self.fControlProcSockets

    def AppendControlProcessSocketList( self, socket_list ):
        for k in socket_list.keys():
            self.fControlProcSockets[k] = socket_list[k]
            node = string.split(k,"/")[0]
            if not self.fNodeSockets.has_key( node ) or self.fNodeSockets[node]<socket_list[k][0]+socket_list[k][1]:
                self.fNodeSockets[node] = socket_list[k][0]+socket_list[k][1]

    def GetNextSocket( self, config, nodeID, offset = 1, controlled_process_id=None ):
        node = config.GetNode( nodeID ).GetHostname()
        if offset < 1:
            offset = 1
        if controlled_process_id!=None and self.fControlProcSockets.has_key(node+"/"+controlled_process_id):
            #print "Lookup "+node+"/"+controlled_process_id
            return self.fControlProcSockets[node+"/"+controlled_process_id][0]
        if self.fNodeSockets.has_key( node ):
            socket = self.fNodeSockets[node]
        else:
            socket = self.fStartSocket
        self.fNodeSockets[node] = socket+offset
        if controlled_process_id!=None:
            self.fControlProcSockets[node+"/"+controlled_process_id] = [socket,offset]
        return socket

    def GetShmIDs( self ):
        return self.fShmIDs

    def AppendShmIDs( self, shm_list ):
        for s in shm_list.keys():
            self.fShmIDs[s] = shm_list[s]
            node = string.split(s,"/")[0]
            if not self.fNodeShms.has_key(node) or self.fNodeShms[node]<shm_list[s]+1:
                self.fNodeShms[node] = shm_list[s]+1
    
    def GetNextShmID( self, config, nodeID, shm_id=None ):
        node = config.GetNode( nodeID ).GetHostname()
        if shm_id!=None and self.fShmIDs.has_key(node+"/"+shm_id):
            return self.fShmIDs[node+"/"+shm_id]
        if self.fNodeShms.has_key( node ):
            shm = self.fNodeShms[node]
        else:
            shm = self.fStartShm
        self.fNodeShms[node] = shm+1
        if shm_id!=None:
            self.fShmIDs[node+"/"+shm_id] = shm
        return shm

    def GetProcSuffix( self, p, nodeIndex, config ):
        nodes = p.GetNodes()
        thisNode = nodes[nodeIndex]
        needNode = 0
        needNdx = 0
        i=0
        ndx = 0
        
        for n in nodes:
            #if n!=thisNode:
            #    needNode = 1
            #if n == thisNode and i!=nodeIndex:
            #    needNdx = 1
            if n == thisNode and i<nodeIndex:
                ndx = ndx + 1
            i = i+1
        if p.GetType()!="src" and p.GetType()!="pbh":
            needNode = 1
            needNdx = 1
        suffix = ""
        if needNode:
            suffix = suffix + "-" + config.fNodes[thisNode].fIDName
        if needNdx:
            suffix = suffix + "-%03d" % ndx
        return suffix
          
    # Determine a list of children of processes on the given node,
    # with each child appearing only once
    def FindChildrenOfNode( self, config, node ):
        procs = config.GetNodeProcesses( node )
        children = set() # Set, each entry is contained only once.
        mergers = []
        for p in procs:
            my_children = config.GetChildProcesses( p )
            children |= set(my_children)
        return list(children)
    
    def CreateParentMergeList( self, config, node ):
        if self.fCreateParentMergeListCache.has_key(node):
            return self.fCreateParentMergeListCache[node]
        # For now no mergers on parent node until synchronization between different multiplicities
        # has been solved.
        # Solved, seems to work
        ### return []
        children = self.FindChildrenOfNode( config, node )
        
        procs = config.GetNodeProcesses( node )
        # Determine a list of all parent lists for all children
        # with at least one parent on this node
        parents = []
        for c in children:
            my_parents = config.GetProcess( c ).GetParents()
            for ndx in range(len(my_parents)):
                my_parents[ndx] = my_parents[ndx]+"___"+config.GetProcess( c ).GetParentFilterType(my_parents[ndx])
            if len(my_parents)>1:
                pfound=0
                for p in parents:
                    if len(my_parents)==len(p):
                        found=1
                        for mps in my_parents:
                            if not mps in p:
                                found=0
                                break
                        if found:
                            pfound=1
                            break
##                    if len(my_parents)==len(p) and len(set(my_parents)^set(p))==0:
##                        pfound=1
##                        break
##                    if len(my_parents)==len(p):
##                        found=1
##                        for mps in my_parents:
##                            tmpfound=0
##                            for ps in p:
##                                if mps == ps:
##                                    tmpfound=1
##                                    break
##                            if not tmpfound:
##                                found=0
##                                break
##                        if found:
##                            pfound=1
##                            break
                if not pfound:
                    parents.append( my_parents )
        # Determine whether we have obtained lists with at least one parent
        # not on this node
        #print parents
        nodeParents = []
        for ps in parents:
            found=1
            for p in ps:
                pid="___".join( p.split("___")[:-1] )
                if config.GetProcess( pid )==None:
                    print "CreateParentMergeList:",ps,pid
                if not config.GetProcess( pid ).IsFullyOnNode( node ):
                    found=0
            if found:
                nodeParents.append( ps )
        # now nodeParents holds lists of parents with all of them on this node

        if False:
            # Disabled, because of instabilities...

            # If we have merged data to send to one node but another process
            # on that node also needs partial unmerged data of this set, then
            # we have to take this data out of the merge list
            
            # For this, first we get all nodes with at least on child process
            # of a process on this node
            childNodes = []
            for p in procs:
                tmpChildNodes = config.GetNodesWithChildren( p )
                for n in tmpChildNodes:
                    found=0
                    for cn in childNodes:
                        if cn==n:
                            found=1
                            break
                    if not found:
                        childNodes.append( n )


            # When we have the childnodes we look at all processes on them
            # to see if one uses only a subset of a merge set that we found.
            # If so we delete this subset from the found set.
            # XXX TODO: The deleted subset could be used as an own separate
            # set.
            tmpList = []
            for cn in childNodes:
                childNodeProcs = config.GetNodeProcesses( cn )
                for ps in nodeParents:
                    for cnp in childNodeProcs:
                        finds=[]
                        for np in ps:
                            tmpfound=0
                            for par in config.GetProcess( cnp ).GetParents():
                                if np == par:
                                    tmpfound=1
                                    break
                            if tmpfound:
                                finds.append( np )
                        if len(finds)>0 and len(finds)<len(ps):
                            tmpSubList = []
                            for np in ps:
                                found=0
                                for f in finds:
                                    if f == np:
                                        found=1
                                        break
                                if not found:
                                    tmpSubList.append( np )
                        else:
                            tmpSubList = ps
                        tmpList.append( tmpSubList )

            nodeParents = tmpList
        
            # Iterate the list again, to check whether we have some duplicates:
            tmpList = []
            for n in nodeParents:
                # print "CreateParentMergeList: n:",n,"tmpList:",tmpList
                for t in tmpList:
                    found=1
                    # print "CreateParentMergeList: t:",t
                    if len(t)!=len(n):
                        found=0
                        # print "CreateParentMergeList: not found (1)",t,n
                    else:
                        for ne in n:
                            # print "CreateParentMergeList: ne",ne
                            tmpfound=0
                            for te in t:
                                # print "CreateParentMergeList: te",te
                                if te==ne:
                                    tmpfound=1
                                    # print "CreateParentMergeList: not found (2)",te,ne
                                    break
                            if not tmpfound:
                                found=0
                                # print "CreateParentMergeList: not found (3)",ne
                                break
                    if found:
                        break
                if not found or len(tmpList)<=0:
                    # print "CreateParentMergeList: not found (4)"
                    tmpList.append( n )

            nodeParents = tmpList

        self.fCreateParentMergeListCache[node] = nodeParents
        return nodeParents

    def CreateMergerID( self, config, node, mergeList ):
        mergerID=""
        mergerID=node+"-"
        first=1
        for p in mergeList:
            if not first:
                mergerID=mergerID+"-"
            mergerID=mergerID+"___".join( p.split("___")[:-1] )
            if p.split("___")[-1]!="none":
                mergerID=mergerID+"-filter-"+p.split("___")[-1]
            first=0
        mergerID=mergerID+"-Merger"
        mergerIDLenLimit = 60
        if len(mergerID)>mergerIDLenLimit:
            origMergerID = mergerID
            mergerID = node+"-"+"___".join( mergeList[0].split("___")[:-1] )+"___"+"___".join( mergeList[-1].split("___")[:-1] )+"-Merger"
            if len(mergerID)>mergerIDLenLimit:
                mergerID = node+"-"+"___".join( mergeList[0].split("___")[:-1] )+"-EtAl-Merger"
            if len(mergerID)>mergerIDLenLimit:
                mergerID = node+"-Merger"
            if not self.fMergerIDs.has_key(mergerID):
                self.fMergerIDs[mergerID] =  [ origMergerID ]
                ndx=0
            else:
                if self.fMergerIDs[mergerID].count(origMergerID)<=0:
                    self.fMergerIDs[mergerID].append(origMergerID)
                ndx = self.fMergerIDs[mergerID].index(origMergerID)
            mergerID = mergerID+( "%u" % (ndx) )
        return mergerID
        
    def CreateParentScattererList( self, config, node ):
        if self.fCreateParentScattererListCache.has_key(node):
            return self.fCreateParentScattererListCache[node]
        children = self.FindChildrenOfNode( config, node )
        mergers = self.CreateParentMergeList( config, node )

        # XXX TODO:
        # Implement weights for the scatterer program's outputs and use only one output
        # per weight per node, e.g. merge three outputs to the same node into one
        # output with weight three.

        # Determine the different multiplicities needed for each input combination.
        scatterer_mult = {}
        scatterer_parents = {}
        for c in children:
            # print c
            proc = config.GetProcess( c )
            #mult = proc.ProcCntOnNode( node )
            nodes = proc.GetNodes()
            mult = len(nodes)
            # print c, nodes, mult, nodes.count(nodes[0]), len(nodes)
            if mult<=1 or nodes.count(nodes[0])==len(nodes):
                continue
            tmpParents = config.GetProcess( c ).GetParents()
            parents = []
            for par in tmpParents:
                if config.GetProcess(par).ProcCntOnNode( node )>0:
                    parents.append( par+"___"+proc.GetParentFilterType(par) )
            # Find largest merger subset of input
            cur_mergers = self.FindMergerSubsets( parents, mergers )
            for cur_merger in cur_mergers:
                merger_id = self.CreateMergerID( config, node, cur_merger )
                if scatterer_mult.has_key( merger_id ):
                    scatterer_mult[ merger_id ].append( mult )
                else:
                    scatterer_mult[ merger_id ] = [ mult ]
                    scatterer_parents[ merger_id ] = cur_merger
            # Find out remaining parent processes, not in merger list
            for p in parents:
                found=0
                for cur_merger in cur_mergers:
                    for m in cur_merger:
                        if m == p:
                            found=1
                            break
                if not found:
                    if scatterer_mult.has_key( p ):
                        scatterer_mult[ p ].append( mult )
                    else:
                        scatterer_mult[ p ] = [ mult ]
                        #scatterer_parents[ p ] = ["___".join(p.split("___")[:-1])]
                        scatterer_parents[ p ] = [p]
        # Remove duplicates
        for p in scatterer_mult.keys():
            mults = scatterer_mult[p]
            new_mults = []
            for m in mults:
                found=0
                for nm in new_mults:
                    if nm==m:
                        found=1
                        break
                if not found:
                    new_mults.append( m )
            scatterer_mult[p] = new_mults

        scatterer_data = {}
        scatterer_data[ "mults" ] = scatterer_mult
        scatterer_data[ "parents" ] = scatterer_parents
        self.fCreateParentScattererListCache[node] = scatterer_data
        return scatterer_data


    def CreateMergedBridgeList( self, config, fromNodeID, toNodeID ):
        cacheID = fromNodeID+"-"+toNodeID
        if self.CreateMergedBridgeListCache.has_key(cacheID):
            return self.CreateMergedBridgeListCache[cacheID]
        # print "CMBL:",cacheID,"start"
        bridges = []
        nodeChildProcs = config.GetNodeChildrenOnNode( fromNodeID, toNodeID )
        mergers = self.CreateParentMergeList( config, fromNodeID )
        for proc in nodeChildProcs:
            p = config.GetProcess( proc )
            tmpParents = p.GetParents()
            parents = []
            for par in tmpParents:
                if config.GetProcess(par).ProcCntOnNode( fromNodeID )>0:
                    parents.append( par+"___"+p.GetParentFilterType(par) )
            # print "CMBL:",proc,parents,tmpParents
            cur_mergers = self.FindMergerSubsets( parents, mergers )
            for par in self.RemoveMergedParents( parents, mergers ):
                cur_mergers.append( [par] )
            for m in cur_mergers:
                bridge = {}
                pnms = {}
                pms = {}
                for mp in m:
                    mpID = "___".join(mp.split("___")[:-1])
                    pnms[mp]= config.GetProcess(mpID).ProcCntOnNode( fromNodeID )
                    pms[mp] = len( config.GetProcess(mpID).GetNodes() )
                bridge["parent"] = m[:]
                bridge["nodeMult"] = p.ProcCntOnNode( toNodeID )
                bridge["mult"] = len( p.GetNodes() )
                bridge["parentNodeMult"] = pnms
                bridge["parentMult"] = pms
                bridge["nodeCnt"] = len( p.GetNodeList() )
                bridge["id"] = [proc]
                found=0
                # print bridge
                for b in bridges:
                    if len(bridge["parent"])!=len(b["parent"]):
                        continue
                    found=1
                    for par in bridge["parent"]:
                        tmpfound=0
                        for bp in b["parent"]:
                            if bp==par:
                                tmpfound=1
                                break
                        if not tmpfound:
                            found=0
                            break
                    if bridge["nodeMult"]!=b["nodeMult"] or bridge["mult"]!=b["mult"]:
                        found=0
                    if found:
                        b["id"].append( proc )
                        break
                if not found:
                    bridges.append( bridge )

        # Disabled, because of instabilities...
        # XXX Fix CreateChildNodeMergers before activating this.
        modified=False
        while modified:
            modified = False
            bridges.sort( key=lambda x: len(x["parent"]) )
            # print bridges
            newBridges = []
            for b in bridges:
                for nb in newBridges:
                    if b["nodeMult"]!=nb["nodeMult"] or b["mult"]!=nb["mult"]:
                        continue
                    found=False
                    for nbp in nb["parent"]:
                        if not nbp in b["parent"]:
                            # Item from shorter list not found in longer list, we cannot use shorter list as subset of longer list
                            found=True
                            break
                    if found:
                        continue
                    for nbp in nb["parent"]:
                        b["parent"].remove( nbp )
                    for bid in b["id"]:
                        nb["id"].append(bid)
                    modified = True
                if len(b["parent"])>0:
                    newBridges.append(b)
                        
            bridges = newBridges
        self.CreateMergedBridgeListCache[cacheID] = bridges
        # print "CMBL:",cacheID,bridges
        return bridges
        
    def CreateParentNodeBridgeList( self, config, fromNodeID ):
        if self.fCreateParentNodeBridgeListCache.has_key(fromNodeID):
            return self.fCreateParentNodeBridgeListCache[fromNodeID]
        childNodes = config.GetNodesWithNodeChildren( fromNodeID )
        bridges = {}
        for cn in childNodes:
            if cn!=fromNodeID:
                bridges[cn] = self.CreateMergedBridgeList( config, fromNodeID, cn )
        self.fCreateParentNodeBridgeListCache[fromNodeID] = bridges
        return bridges

    def CreateChildNodeBridgeList( self, config, toNodeID ):
        if self.fCreateChildNodeBridgeListCache.has_key(toNodeID):
            return self.fCreateChildNodeBridgeListCache[toNodeID]
        parentNodes = config.GetNodesWithNodeParents( toNodeID )
        bridges = {}
        for pn in parentNodes:
            if pn!=toNodeID:
                bridges[pn] = self.CreateMergedBridgeList( config, pn, toNodeID )

                # if toNodeID=="feptpcai00" and pn=="feptpcci12":
                # print pn,"->",toNodeID,":",bridges[pn]
        self.fCreateChildNodeBridgeListCache[toNodeID] = bridges
        return bridges

    def GetNewBridgeData( self, config, parent, child, fromNode, toNode ):
        idstring = str(parent) + "#" + str(child) + "#" + str(fromNode) + "#" + str(toNode)
        if self.fBridges.has_key(idstring):
            return self.fBridges[idstring]
#        for br in self.fBridges:
#            if br["parent"] == parent and br["child"] == child and br["fromNode"]==fromNode and br["toNode"]==toNode:
#                return br
        br = {}
        br["parent"] = parent
        br["child"] = child
        br["fromNode"] = fromNode
        br["toNode"] = toNode
##        if self.fNodeSockets.has_key(fromNode):
##            br["SenderBaseSocket"] = self.fNodeSockets[fromNode]
##        else:
##            br["SenderBaseSocket"] = self.fStartSocket
##        self.fNodeSockets[fromNode] = br["SenderBaseSocket"]
##        if self.fNodeSockets.has_key(toNode):
##            br["ReceiverBaseSocket"] = self.fNodeSockets[toNode]
##        else:
##            br["ReceiverBaseSocket"] = self.fStartSocket
##        self.fNodeSockets[toNode] = br["ReceiverBaseSocket"]
        control_id = str(config.fProcesses[parent].fIDName)+"-"+str(config.fProcesses[child].fIDName)+"-"+str(config.fNodes[fromNode].fIDName)+"-"+str(config.fNodes[toNode].fIDName)
        br["SenderBaseSocket"] = self.GetNextSocket( config, fromNode, 3, controlled_process_id=control_id+"-SenderBaseSocket" )
        br["ReceiverBaseSocket"] = self.GetNextSocket( config, toNode, 3, controlled_process_id=control_id+"-ReceiverBaseSocket" )
#        self.fBridges.append( br )
        self.fBridges[idstring] = br
        return br


    def GetNewBridgeDataOld( self, parents, mult, nodeMult, fromNode, toNode ):
        for br in self.fBridges:
            if br["parents"] == parents and br["mult"]==mult and br["nodeMult"]==nodeMult and br["fromNode"]==fromNode and br["toNode"]==toNode:
                return br
        br = {}
        br["parents"] = parents
        br["mult"] = mult
        br["nodeMult"] = nodeMult
        br["fromNode"] = fromNode
        br["toNode"] = toNode
##        if self.fNodeSockets.has_key(fromNode):
##            br["SenderBaseSocket"] = self.fNodeSockets[fromNode]
##        else:
##            br["SenderBaseSocket"] = self.fStartSocket
##        self.fNodeSockets[fromNode] = br["SenderBaseSocket"]
##        if self.fNodeSockets.has_key(toNode):
##            br["ReceiverBaseSocket"] = self.fNodeSockets[toNode]
##        else:
##            br["ReceiverBaseSocket"] = self.fStartSocket
##        self.fNodeSockets[toNode] = br["ReceiverBaseSocket"]
        control_id = str(parents)+"-"+str(mult)+"-"+str(nodeMult)+"-"+str(fromNode)+"-"+str(toNode)
        br["SenderBaseSocket"] = self.GetNextSocket( self, fromNode, 3, controlled_process_id=control_id+"-SenderBaseSocket" )
        br["ReceiverBaseSocket"] = self.GetNextSocket( self, toNode, 3, controlled_process_id=control_id+"-ReceiverBaseSocket" )
        self.fBridges.append( br )
        return br

    def FindMergerSubsets( self, parents, mergeList ):
        cur_mergers = []
        cur_merger = [1]
        tmpParents = parents[:]
        
        while len(cur_merger)>0:
            cur_merger = []
            for m in mergeList:
                tmp_merger = []
                for p in tmpParents:
                    for mip in m:
                        if mip==p:
                            tmp_merger.append( mip )
                    # If at least one element from the merger input list
                    # is not an input to the child, the merger cannot be used. 
                if len(tmp_merger)!=len(m):
                    tmp_merger = []
                if len(tmp_merger)>len(cur_merger):
                    cur_merger = tmp_merger
            if len(cur_merger)>0:
                cur_mergers.append( cur_merger )
                tmpParents = self.RemoveMergedParents( tmpParents, [ cur_merger ] )
                #for m in cur_merger:
                #    tmpParents.remove( m )
        # print "FindMergerSubsets:",cur_mergers
        return cur_mergers

    def RemoveMergedParents( self, parents, mergeList ):
        #print "RemoveMergedParents:",parents,mergeList
        tmpParents = parents
        for cur_merger in mergeList:
            found=True
            for cme in cur_merger:
                if not cme in tmpParents:
                    found=False
            if found:
                for m in cur_merger:
                    if tmpParents.count(m)>0:
                        tmpParents.remove( m )
                    else:
                        # print "RemoveMergedParents: parent",m,"not found in list"
                        # traceback.print_stack()
                        # print "RemoveMergedParents: parents",parents,"mergeList:",mergeList,"cur_merger:",cur_merger,"m:",m
                        pass
        return tmpParents
    

    def CreateGathererChildNodeList( self, config, node ):
        if self.fCreateGathererChildNodeListCache.has_key(node):
            return self.fCreateGathererChildNodeListCache[node]
        bridges = self.CreateChildNodeBridgeList( config, node )
        #nodeMergers = self.CreateParentMergeList( config, node )
        #procs = config.GetNodeProcesses( node )

        #for p in procs:
        #    parents = procs.GetParents()
            

        gathererList = {}

        for brk in bridges.keys():
            # if config.GetProcess( brk["id"][0] ).Singleton():
            #     print "CreateGathererChildNodeList singleton (1):",brk["id"][0]
            #     continue
            # brk is the ID of the node from which bridges are now constructed.
            mergers = self.CreateParentMergeList( config, brk )
            nodeBridges = bridges[brk]
            #bridges.del( brk )
            for nbk in nodeBridges:
                parents = nbk["parent"][:]
                cur_mergers = self.FindMergerSubsets( parents, mergers )
                for p in self.RemoveMergedParents( parents, mergers ):
                    cur_mergers.append( [p] )
                for m in cur_mergers:
                    if len( config.GetProcess( self.IDFromFilterTypeID(m[0]) ).GetNodeList() )<=1:
                        continue
                    if len(m)>1:
                        id = self.CreateMergerID( config, node, m )
                    else:
                        id = self.IDFromFilterTypeID(m[0])
                        filterType = self.FilterTypeFromFilterTypeID(m[0])
                        # Inlcude node name in filter IDs?
                        if filterType=="readout":
                            id += "-ReadoutFilter"
                        elif filterType=="monitor":
                            id += "-MonitorFilter"
                    id = id+"_%u_%u" % ( nbk["nodeMult"],nbk["mult"] )
                    # print id,nbk["nodeCnt"],nbk["nodeMult"],nbk["mult"],gathererList.has_key(id)
                    if not gathererList.has_key(id):
                        # print "CreateGathererChildNodeList - parentList",m,nbk["id"]
                        gathererList[id] = { "parentList":m, "nodeList":[], "mult" : nbk["nodeCnt"], "id" : nbk["id"],
                                             "nodeMult" : nbk["nodeMult"], "totalMult" : nbk["mult"],
                                             "regather" : False }
                    gathererList[id]["nodeList"].append( brk )

        for gk in gathererList.keys():
            for i in range( config.GetProcess( self.IDFromFilterTypeID( gathererList[gk]["parentList"][0] ) ).ProcCntOnNode( node ) ):
                gathererList[gk]["nodeList"].append( node )
            #while len(gathererList[gk]["nodeList"]) < gathererList[gk]["mult"]:
            #    gathererList[gk]["nodeList"].append( node )
            gathererList[gk]["nodeList"].sort()

        #print "XXX"
            
        procs = config.GetNodeProcesses( node )

        nodeChildProcs = config.GetNodeChildrenOnNode( node, node )
        for proc in nodeChildProcs:
            p = config.GetProcess( proc )
            nodeMult = p.ProcCntOnNode(node)
            mult = len(p.GetNodes())
            nodeCnt = len(p.GetNodeList())
            # print "CreateGathererChildNodeList",node,p.fID,nodeMult,mult
            if nodeMult<=1 or nodeMult==mult: # or p.Singleton():
                # if p.Singleton():
                #     print "CreateGathererChildNodeList singleton (2):",proc
                continue
            tmpParents = p.GetParents()
            parents = []
            foundOffNode=False
            for par in tmpParents:
                if config.GetProcess(par).ProcCntOnNode( node )>0:
                    parents.append( par+"___"+p.GetParentFilterType(par) )
                if config.GetProcess(par).ProcCntOnNode( node )!=len(config.GetProcess(par).GetNodes()):
                    foundOffNode = True
            # print "CreateGathererChildNodeList",node,p.fID,foundOffNode,parents
            if len(parents)<=0 or not foundOffNode:
                continue
            for par in parents:
                #gID=par+"-ReGatherer-"+node+"-%d" % ( nodeMult )
                id = self.IDFromFilterTypeID(par)+self.IDFilterAddition(self.FilterTypeFromFilterTypeID(par))+("_%u_%u" % ( nodeMult, mult ))
                if not gathererList.has_key(id):
                    #print "CreateGathererChildNodeList - parentList",m
                    gathererList[id] = { "parentList":[par], "nodeList":[], "mult" : nodeCnt, "id" : [p.fID],
                                         "nodeMult" : nodeMult, "totalMult" : mult,
                                             "regather" : True }
                    for i in range( nodeMult ):
                        gathererList[id]["nodeList"].append( node )
                    gathererList[id]["nodeList"].sort()
                    



        #for glk in gathererList.keys():
        #    print "CreateGathererChildNodeList",node,":",glk
        self.fCreateGathererChildNodeListCache[node] = gathererList
        return gathererList

    def GetChildren( self, procList, proc ):
        pubIDs = self.ProcID2PublisherList(proc.fID)
        children = []
        for pubID in pubIDs:
            cpl = self.Subscription2Proc(pubID)
            if cpl!=None:
                #children.append(cp)
                children += cpl
            else:
                #print "procList:",procList
                #print "proc:",proc.fID,proc
                #print "self.fProcID2Publisher:",self.fProcID2Publisher
                #print "self.fPublisher2Proc:",self.fPublisher2Proc
                #print "self.fSubscription2Proc:",self.fSubscription2Proc
                #print "self.fProcID2Subscription:",self.fProcID2Subscription
                #raise ChainConfigException( "INTERNAL ERROR: Child process "+pubID+" for publisher process "+proc.fID+" cannot be found." )
                if not self.fQuiet and not hasattr(proc, "fIsSink"):
                    print( "Warning: No child process using "+pubID+" of publisher process "+proc.fID+" can be found. (A)" )
        return children
#        publisherMatch = re.compile( "-publisher\s+(\S+)\s?(.*)$" )
#        subscriberMatch = re.compile( "-subscribe\s+(\S+)\s+(\S+)\s?(.*)$" )
#        myIDs = []
#        myNode = proc.GetNode()
#        myID = proc.fID
#        #print "For "+myID
#        l = proc.GetCmd()+" "+proc.GetConfigParams()
#        while l != "":
#            match = publisherMatch.search( l )
#            if match != None:
#                myIDs.append( match.group( 1 ) )
#                #print "Found publisher : "+pub+" - node: "+node
#                l = match.group( 2 )
#            else:
#                l = ""
#        #print "Pubs:",myIDs
#        children = []
#        for p in procList:
#            if p.fID!=myID and p.GetNode()==myNode:
#                #print p.fID+": "+p.GetCmd()+" "+p.GetConfigParams()
#                l = p.GetCmd()+" "+p.GetConfigParams()
#                while l != "":
#                    match = subscriberMatch.search( l )
#                    if match != None:
#                        for id in myIDs:
#                            if match.group( 1 ) == id:
#                                children.append( p )
#                                break
#                        l = match.group( 3 )
#                    else:
#                        l = ""
#        #print "Subs:",
#        #for c in children:
#        #    print c.fID,",",
#        #print
#        return children

    def GetSubscriptionAttributes( self, parent, child, ndx ):
        # print "GetSubscriptionAttributes:", parent, child, ndx, self.fSubscriberID2Attributes
        pubIDs = self.ProcID2PublisherList(parent)
        # print "pubIDs:",pubIDs
        children = []
        for pubID in pubIDs:
            cpl = self.Subscription2Proc(pubID)
            if cpl==None:
                print "pubID",pubID,"children:"
                print "pubID",pubID,"children:",[ c.fID for c in cpl ] if cpl!=None else "None"
                print "DISABLE CONTINUE!!!!!"
                continue
            for c in cpl:
                # print c.fID,child
                if c.fID==child:
                    subsIDs = self.fPubSubConnections[child][pubID]
                    # print parent,child,"subsIDs:",subsIDs
                    if ndx<len(subsIDs):
                        subsID = list(subsIDs)[ndx]
                        if self.fSubscriberID2Attributes.has_key(subsID):
                            return self.fSubscriberID2Attributes[subsID]
        return ()

    def GetInlaws( self, procList, proc ):
        children = self.GetChildren( procList, proc )
        inlaws = set()
        for c in children:
            inlaws |= set(self.GetParents( procList, c ))
        inlaws -= set([proc])
        return list(inlaws)
##            for par in parents:
##                if par.fID!=proc.fID or par.GetNode()!=proc.GetNode():
##                    found=0
##                    for il in inlaws:
##                        if par.fID==il.fID and par.GetNode==il.GetNode():
##                            found=1
##                            break
##                    if not found:
##                        inlaws.append( par )
##        return inlaws

    def GetParents( self, procList, proc ):
        if proc.fMappedParents != None:
            return proc.fMappedParents
        pubIDs = self.ProcID2SubscriptionList(proc.fID)
        parents = []
        for pubID in pubIDs:
            pp = self.Publisher2Proc(pubID)
            if pp!=None:
                parents.append(pp)
            else:
                raise ChainConfigException( "INTERNAL ERROR (C): Parent publisher process "+pubID+" for process "+proc.fID+" cannot be found (C)." )
        proc.fMappedParents = parents
        return parents
#        publisherMatch = re.compile( "-publisher\s+(\S+)\s?(.*)$" )
#        subscriberMatch = re.compile( "-subscribe\s+(\S+)\s+(\S+)\s?(.*)$" )
#        parentPubs = []
#        myNode = proc.GetNode()
#        # print "For:",proc.fID
#        l = proc.GetCmd()+" "+proc.GetConfigParams()
#        while l != "":
#            match = subscriberMatch.search( l )
#            if match != None:
#                # print "Found Sub:",match.group( 1 )
#                parentPubs.append( match.group( 1 ) )
#                l = match.group( 3 )
#            else:
#                l = ""
#
#        parents = []
#                
#        for pb in parentPubs:
#            for p in procList:
#                if p.GetNode()==myNode:
#                    l = p.GetCmd()+" "+p.GetConfigParams()
#                    while l != "":
#                        match = publisherMatch.search( l )
#                        if match != None:
#                            # print "Found Pub:",match.group( 1 )
#                            if match.group( 1 ) == pb:
#                                parents.append( p )
#                                break
#                            l = match.group( 2 )
#                        else:
#                            l = ""
#        return parents

    def GetBridgeReceiverPartner( self, proc ):
        msg = self.fBridgeSenderProcID2Msg[proc.fID]
        recvProc = self.fBridgeReceiverMsg2Proc[msg]
        return recvProc

    def CheckBridgeShms( self, config, procList ):
        # Only PublisherBridgeHeads, SBHs do not have significant shm
        # procList has to come out of BuildChildNodeDataFlowCmdlines, objects have to be DataFlowProcesses
        for node in config.GetNodeIDs():
            nodeBridges = []
            nodeBridgeShmSize=0
            for proc in procList:
                if proc.GetType()=="pbh" and proc.GetNode()==node:
                    nodeBridges.append( proc )
                    memSize = 0
                    toks=(proc.GetCmd(-1)+" "+proc.GetConfigParams()).split(" ")
                    n = toks.index("-blob")
                    memSize = int(toks[n+1])
                    nodeBridgeShmSize += memSize

            if (self.fBridgeShmNodeLimit!=0 and nodeBridgeShmSize>self.fBridgeShmNodeLimit) or self.fBridgeShmLimit!=0:
                if self.fBridgeShmNodeLimit!=0 and nodeBridgeShmSize>self.fBridgeShmNodeLimit:
                    factor = float(self.fBridgeShmNodeLimit)/float(nodeBridgeShmSize)
                    print "BridgeShmNodeLimit exceeded [A]. Limit ", self.fBridgeShmNodeLimit, ", requested", nodeBridgeShmSize
                else:
                    factor = 1.0
                for proc in nodeBridges:
                    blobSizeNext = False
                    toks=proc.GetCmd(-1).split(" ")
                    if toks.count("-blob")>0:
                        n = toks.index("-blob")
                        if n+1<len(toks):
                            newSize = int(float(toks[n+1])*factor)
                            if self.fBridgeShmLimit!=0 and newSize>self.fBridgeShmLimit:
                                print "BridgeShmNodeLimit exceeded [B]. Limit ", self.fBridgeShmNodeLimit, ", requested", newSize
                                newSize = self.fBridgeShmLimit
                            newSize = (newSize // 4096)*4096
                            toks[n+1] = "%d" % newSize
                        proc.fCmdLine[-1] = " ".join( toks )
                    toks=proc.GetConfigParams().split(" ")
                    if toks.count("-blob")>0:
                        n = toks.index("-blob")
                        if n+1<len(toks):
                            newSize = int(float(toks[n+1])*factor)
                            if self.fBridgeShmLimit!=0 and newSize>self.fBridgeShmLimit:
                                print "BridgeShmNodeLimit exceeded [C]. Limit ", self.fBridgeShmNodeLimit, ", requested", newSize
                                newSize = self.fBridgeShmLimit
                            newSize = (newSize // 4096)*4096
                            toks[n+1] = "%d" % newSize
                        proc.fConfigParams = " ".join( toks )


    def CheckShms( self, config, procList ):
#TIMES        times = {}
#TIMES        times[-1] = time.time()
        nodeShms = config.GetNodeIDs()
        nodeProcs = config.GetNodeIDs()
        for node in config.GetNodeIDs():
            nodeProcs[node] = []
            nodeShms[node]={}
        for proc in procList:
            node = proc.GetNode()
            nodeProcs[node].append( proc )
            memSize = 0
            memType = ""
            toks=(proc.GetCmd()+" "+proc.GetConfigParams()).split(" ")
            if proc.GetType()=="pbh":
                n = toks.index("-blob")
                memType = toks[toks.index("-shm")+1]
                memSize = int(toks[n+1])
            elif proc.GetType()=="src" or proc.GetType()=="prc" or proc.GetType()=="snk":
                n = toks.index("-shm")
                memSize = int(toks[n+3])
                memType = toks[n+1]
            if memType !="" and not nodeShms[node].has_key(memType):
                nodeShms[node][memType] = 0
            if memType !="":
                #print "SHM Calculaten node ", node.ljust(10), "(" , memType, "), proc ", proc.GetID().ljust(90), ": adding ", str(memSize / 1024 / 1024).rjust(6), " MB, total: ", str((nodeShms[node][memType] + memSize) / 1024 / 1024).rjust(6), " MB"
                nodeShms[node][memType] += memSize
        for node in config.GetNodeIDs():
            shmTypes = set(nodeShms[node].keys()) | set(self.fShmNodeLimit.keys()) | set(self.fShmPerProcessLimit.keys())
            # if self.fShmNodeSpecificLimit.has_key(node):
            #     shmTypes |= set(self.fShmNodeSpecificLimit[node].keys())
            shmTypes |= self.GetNodeShmTypes( node )
            for type in shmTypes:
                # if self.fShmNodeSpecificLimit.has_key(node) and self.fShmNodeSpecificLimit[node].has_key(type):
                #     nodeShmLimit = self.fShmNodeSpecificLimit[node][type]
                # elif self.fShmNodeLimit.has_key(type):
                #     nodeShmLimit = self.fShmNodeLimit[type]
                # else:
                #     nodeShmLimit = 0
                nodeShmLimit = self.GetNodeShmLimit( node, type )
                #print "Node Shm limit:",node,type,nodeShmLimit
                #print "Node:",node,"- shmType:",type,"- nodeShms[node]:",nodeShms[node],"- self.fShmNodeLimit[type]:",self.fShmNodeLimit[type]
                if (nodeShms[node].has_key(type) and nodeShmLimit!=0 and nodeShms[node][type]>nodeShmLimit) or (self.fShmPerProcessLimit.has_key(type) and self.fShmPerProcessLimit[type]!=0):
                    if nodeShms[node].has_key(type) and nodeShmLimit!=0 and nodeShms[node][type]>nodeShmLimit:
                        factor = float(nodeShmLimit)/float(nodeShms[node][type])
                        print "SHM Limit on node ", config.fNodes[node].fIDName, " exceeded. Limit: ", nodeShmLimit, ", requested: ", nodeShms[node][type]
                    else:
                        factor = 1.0
                    for proc in nodeProcs[node]:
                        toks=proc.GetCmd().split(" ")
                        newSize=-1
                        shmType=""
                        if toks.count("-shm"):
                            shmType=toks[toks.index("-shm")+1]
                        if proc.GetType()=="pbh":
                            if toks.count("-blob"):
                                n = toks.index("-blob")
                                newSize = int(float(toks[n+1])*factor)
                        elif proc.GetType()=="src" or proc.GetType()=="prc" or proc.GetType()=="snk":
                            if toks.count("-shm"):
                                n = toks.index("-shm")
                                newSize = int(float(toks[n+3])*factor)
                        if self.fShmPerProcessLimit.has_key(type) and self.fShmPerProcessLimit[type]!=0 and newSize>self.fShmPerProcessLimit[type]:
                            print "SHMPerProcess limit [A] on node ", node, " exceeded. Limit: ", self.fShmPerProcessLimit[type], ", requested: ", newSize
                            newSize = self.fShmPerProcessLimit[type]
                        if newSize>=0 and shmType==type:
                            newSize = (newSize // 4096)*4096
                            if newSize==0:
                                newSize = 4096
                            if proc.GetType()=="pbh":
                                toks[n+1] = "%d" % newSize
                            elif proc.GetType()=="src" or proc.GetType()=="prc" or proc.GetType()=="snk":
                                toks[n+3] = "%d" % newSize
                            proc.fCommand = " ".join( toks )
                        toks=proc.GetConfigParams().split(" ")
                        if toks.count("-shm"):
                            shmType=toks[toks.index("-shm")+1]
                        newSize = -1
                        if proc.GetType()=="pbh":
                            if toks.count("-blob"):
                                n = toks.index("-blob")
                                newSize = int(float(toks[n+1])*factor)
                        elif proc.GetType()=="src" or proc.GetType()=="prc" or proc.GetType()=="snk":
                            if toks.count("-shm"):
                                n = toks.index("-shm")
                                newSize = int(float(toks[n+3])*factor)
                        if self.fShmPerProcessLimit.has_key(type) and self.fShmPerProcessLimit[type]!=0 and newSize>self.fShmPerProcessLimit[type]:
                            print "SHMPerProcess limit [B] on node ", node, " exceeded. Limit: ", self.fShmPerProcessLimit[type], ", requested: ", newSize
                            newSize = self.fShmPerProcessLimit[type]
                        if newSize>=0 and shmType==type:
                            newSize = (newSize // 4096)*4096
                            if newSize==0:
                                newSize = 4096
                            if proc.GetType()=="pbh":
                                toks[n+1] = "%d" % newSize
                            elif proc.GetType()=="src" or proc.GetType()=="prc" or proc.GetType()=="snk":
                                toks[n+3] = "%d" % newSize
                            proc.fConfigParams = " ".join( toks )
#TIMES        times[0] = time.time()
#TIMES        print "dshm %f" % (times[0] - times[-1])

    def GetRealParents(self, procList, proc):
        ret=[]
        for parent in self.GetParents(procList, proc):
            cmdline=parent.GetCmd().split()
            if '-shm' in cmdline:
                ret.append(parent)
            else:
                ret += self.GetRealParents(procList,parent)
        return ret

    def SetPreMapRorcBuffers(self, procList):
        for proc in procList:
            if not proc.fType in ['snk','prc']:
                continue
            bufferIDs=''
            parents=self.GetRealParents(procList, proc)
            for par in parents:
                cmdline=par.GetCmd().split()
                if 'librorc' in cmdline:
                    bufferIDs+=cmdline[cmdline.index('librorc')+1] + ','
            #print proc.fType,proc.fID,bufferID
            if len(bufferIDs) > 0:
                proc.fCommand += ' -premaprorcbuffers ' + bufferIDs


class SimpleProcListOutputter:
    def __init__( self ):
        pass

    def OutputProcessList( self, config, procList, file=sys.stdout ):
        for node in config.GetNodes():
            #file.write( "node:",node+"\n" )
            file.write( node.GetHostname()+": "+"\n" )
            for proc in procList:
                #file.write( "proc.GetNode():",proc.GetNode()+"\n" )
                if proc.GetNode()==node.fID:
                    file.write( "      "+proc.GetTypeString()+": "+proc.GetCmd()+" ( "+proc.GetConfigParams()+" )\n" )
                    #file.write( "          ",proc.GetVirtualParents()+"\n" )
                    #file.write( "          ",proc.GetParents()+"\n" )
                    #file.write( "          "+proc.GetVirtualParentString()+"\n" )
                    #file.write( "          "+proc.GetParentString()+"\n" )
                    file.write( "\n" )
            file.write( "\n" )
            file.write( "\n" )
            file.write( "\n" )


class FileOutputWriter:
    def __init__(self, filename):
        self.fAggregateLines = True  # 'True': collect all lines and flush in FinishOutput(); 'False': write immediately
        self.fUseSysOut = False      # 'True' uses write(); 'False' uses os.write()
        self.fOpenBuffering = -1     # -1: System default; 0: no buffering; 1: buffer line; >1: appox. buffer size
        self.__fd = open(filename, "w", self.fOpenBuffering)
        self.__lines = []

    def OutputLine(self, outputLine):
        if self.fAggregateLines:
            self.__lines.append(outputLine)
        else:
            self.write(outputLine)

    def write(self, string):
        if self.fUseSysOut:
          os.write(self.__fd.fileno(), string)
        else:
          self.__fd.write(string)

    def FinishOutput(self):
        if self.fAggregateLines:
            self.write("".join(self.__lines))
        self.__fd.close()


class TaskManagerOutputter:
    def __init__( self, masterNodes, useSSH, includePaths, taskManDir, frameworkDir, platform, runDir, outputDir, prestartExec, postTerminateExec, scriptdir, additionalInterruptTargets ):
        self.fMasterNodes = masterNodes
        self.fUseSSH = useSSH
        self.fIncludePaths = []
        for ip in includePaths:
            if ip != None:
                self.fIncludePaths.append( os.path.expanduser( ip ) )
        if taskManDir != None:
            self.fTaskManDir = os.path.expandvars( os.path.expanduser( taskManDir ) )
        else:
            self.fTaskManDir = None
        if frameworkDir != None:
            self.fFrameworkDir = os.path.expandvars( os.path.expanduser( frameworkDir ) )
        else:
            self.fFrameworkDir = None
        self.fPlatform = platform
        self.fSlavePollInterval = 100000
        self.fInterruptAddresses = {}
        self.fSlaveControlAddresses = {}
        if runDir != None:
            self.fRunDir = os.path.expandvars( os.path.expanduser( runDir ) )
        else:
            self.fRunDir = None
        if outputDir != None:
            self.fOutputDir = os.path.expandvars( os.path.expanduser( outputDir ) )
        else:
            self.fOutputDir = None
        if prestartExec != None:
            self.fPrestartExec = os.path.expandvars( os.path.expanduser( prestartExec ) )
        else:
            self.fPrestartExec = None
        if postTerminateExec != None:
            self.fPostTerminateExec = os.path.expandvars( os.path.expanduser( postTerminateExec ) )
        else:
            self.fPostTerminateExec = None
        self.fScriptDir = scriptdir
        self.fMasterIsAutonomous = 0
        self.fIncludepatternstr = "([^$]*[^\\\\])?\$\(include +([^)]+)\)(.*)"
        self.fIncludepattern = re.compile( self.fIncludepatternstr, re.DOTALL )
        self.fVarpatternstr = "([^$]*[^\\\\])?\$\(([a-zA-z_0-9]+)\)(.*)"
        self.fVarpattern = re.compile( self.fVarpatternstr, re.DOTALL )
        self.fMasterListenSockets = {}
        self.fMasterSlaveControlSockets = {}
        self.fSlaveListenSockets = {}
        self.fSlaveSlaveControlSockets = {}
        self.fAdditionalInterruptTargets = additionalInterruptTargets[:]
        self.fEmptyNodes = 0
        self.fProduction = 0
        self.fProcessCountOnNodeCache = {}
        self.fNodeHasProcessesCache = {}
        self.fNodeProcessesCache = {}
        self.fTaskManagerOptions = ""
        self.fRunDirPermissions = None
        self.fRunStopTimeout = 0
        self.fStopTimeout = 0
        self.fConfigCreateLate = False
        self.fConfigCreateLateProgram = None
        self.fConfigCreateLateParameters = ""
        self.fConfigCreateLateFiles = []
        self.fOnlySlaveNode = None
        self.fMasterInterruptAddresses = {}
        self.fIncludeFiles = {}

    def AddIncludePath( self, incPath ):
        self.fIncludePaths.append( incPath )

    def SetSlavePollInterval( self, interval ):
        self.fSlavePollInterval = interval

    def SetMasterAutonomy( self, auto ):
        self.fMasterIsAutonomous = auto

    def SetEmptyNodes( self,empty_nodes ):
        self.fEmptyNodes = empty_nodes

    def SetProduction( self,production ):
        self.fProduction = production

    def SetTaskManagerOptions( self, options ):
        self.fTaskManagerOptions = options

    def SetRunDirPermissions( self, perms ):
        self.fRunDirPermissions = perms

    def RunStopTimeout( self, timeout ):
        self.fRunStopTimeout = timeout

    def StopTimeout( self, timeout ):
        self.fStopTimeout = timeout

    def ConfigCreateLate( self, program, parameters, files=[] ):
        self.fConfigCreateLate = True
        self.fConfigCreateLateProgram = program
        self.fConfigCreateLateParameters = parameters
        self.fConfigCreateLateFiles = files[:]

    def OnlySlaveNode( self, slaveNode ):
        self.fOnlySlaveNode = slaveNode
        
    def GetNodePlatform( self, node ):
        platform = node.GetPlatform()
        if platform==None:
            platform = self.fPlatform
        if self.fProduction:
            return platform+"-releasedebug"
        else:
            return platform+"-debug"
        #return platform

    def GenerateTaskManagerFiles( self, config, mapper, procList, parallel ):
        newpid = 0
        for node in config.GetNodes():
            self.GenerateTaskManagerSlaveFilePre( config, mapper, procList, node )
        if parallel > 0:
            newpid = os.fork()
        if newpid == 0:
            if parallel > 0:
                pernode = (len(config.GetNodes()) + parallel - 1) / parallel
            else:
                pernode = len(config.GetNodes())
            myprocid = 0
            waitpids = []
            for i in range(0, parallel):
                newpid2 = os.fork()
                if newpid2 == 0:
                    break
                waitpids.append(newpid2)
                myprocid += 1
            numnode = 0
            for node in config.GetNodes():
                if numnode >= myprocid * pernode and numnode < (myprocid + 1) * pernode:
                    #print "Process ", myprocid, " creating ", node
                    self.GenerateTaskManagerSlaveFile( config, mapper, procList, node )
                numnode += 1
            if parallel > 0:
                if myprocid == parallel:
                    for pid in waitpids:
                        os.waitpid(pid, 0)
                sys.exit(0)
        if self.fOnlySlaveNode!=None:
            return
        level = 1
        count=1
        while count>0:
            count=0
            for ng in config.GetNodeGroups():
                # print count,ng,ng.GetLevel()
                if ng.GetLevel()==level:
                    self.GenerateTaskManagerServantFile( config, mapper, procList, ng )
                    count=count+1
            level = level+1
        self.GenerateTaskManagerMasterFile( config, mapper, procList )
#        print "Master finished"
        if parallel:
            os.waitpid(newpid, 0)
#        print "Servants finished"

    def GenerateTaskManagerSlaveFilePre( self, config, mapper, procList, node ):
        self.fInterruptAddresses[node.fID] = "tcpmsg://"+node.GetHostname()+":"+( "%d" % (self.GetSlaveListenSocket(config, mapper, node.fID) ) )+"/"
        self.fSlaveControlAddresses[node.fID] = "tcpmsg://"+node.GetHostname()+":"+( "%d" % (self.GetSlaveSlaveControlSocket( config, mapper, node.fID )) )+"/"

    def GenerateTaskManagerSlaveFile( self, config, mapper, procList, node ):
        if self.fConfigCreateLate:
            return
        if self.fOnlySlaveNode!=None and self.fOnlySlaveNode!=node.fID:
            return
        platform = self.GetNodePlatform( node )
        if not self.fEmptyNodes and not self.NodeHasProcesses( config, procList, node ):
            return
        place = 0
        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1
        processes = self.GetNodeProcesses( config, procList, node )
        outFileText = ""
        outFileLines = []
        outFileName = self.GetSlaveConfigName( config, node )
        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1
        try:
            outFile = FileOutputWriter(outFileName)
        except IOError:
            raise ChainConfigException( "Unable to open output file'"+outFileName+"' for node '"+node.fID+"' TaskManager slave configuration." )
        outFile.OutputLine("<Task name=\""+config.GetName()+"-SlaveNode-"+node.fIDName+"\">\n")
        outFile.OutputLine("<InterfaceLibrary platform=\""+platform+"\">"+self.fFrameworkDir+"/lib/"+platform+"/libTaskManLib.so</InterfaceLibrary>\n")

        outFile.OutputLine("<StateSupervisionConfig interval=\"%d\">\n" % ( self.fSlavePollInterval ))
        outFile.OutputLine("<InterruptListenAddress>"+MakeLocalURL( self.fInterruptAddresses[node.fID] )+"</InterruptListenAddress>\n")
        outFile.OutputLine("<StartupAction>import KIPTaskMan,string\n</StartupAction>\n")
        outFile.OutputLine("<StateChangeAction>")

        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1
        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1
        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1
        input = self.ReadIncludeFile("TaskManager-Slave-StateChangeAction-Prelude-Template.py")
        outFile.OutputLine(input)
        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1
        for proc in processes:
            if proc.GetType()=="src":
                IDList = "SourceIDs"
            elif proc.GetType()=="sbh":
                IDList = "ConnectCmdIDs"
            elif proc.GetType()=="pbh":
                IDList = "ConnectNoCmdIDs"
            else:
                IDList = "ProcSinkIDs"
            outFile.OutputLine(IDList+".append( \""+proc.fID+"\" )\n")
        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1
        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1
        input = self.ReadIncludeFile("TaskManager-Slave-StateChangeAction-Main-Template.py")
        processedInput = self.ProcessString( input, { "STOPRUNTIMEOUT" : str(self.fRunStopTimeout), "STOPTIMEOUT" : str(self.fStopTimeout), }, self.fIncludePaths, 1 )
        outFile.OutputLine(processedInput+"\n")
        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1
        outFile.OutputLine("</StateChangeAction>\n")
        outFile.OutputLine("\n\n")
        if 1:
            outFile.OutputLine("<!-- --> <PeriodicPollAction interval=\"2000000\">")
            input = self.ReadIncludeFile("TaskManager-Slave-PeriodicPollAction-Prelude-Template.py")
            outFile.OutputLine(input)
            for proc in processes:
                if proc.GetType()=="src":
                    IDList = "SourceIDs"
                elif proc.GetType()=="sbh":
                    IDList = "ConnectCmdIDs"
                elif proc.GetType()=="pbh":
                    IDList = "ConnectNoCmdIDs"
                else:
                    IDList = "ProcSinkIDs"
                outFile.OutputLine(IDList+".append( \""+proc.fID+"\" )\n")
            input = self.ReadIncludeFile("TaskManager-Slave-PeriodicPollAction-Main-Template.py")
            processedInput = self.ProcessString( input, { "STOPRUNTIMEOUT" : str(self.fRunStopTimeout), "STOPTIMEOUT" : str(self.fStopTimeout), }, self.fIncludePaths, 1 )
            outFile.OutputLine(processedInput+"\n")
            outFile.OutputLine("</PeriodicPollAction> <!-- -->\n")
    
        outFile.OutputLine("\n\n")
        outFile.OutputLine("<InterruptReceivedAction>")
        input = self.ReadIncludeFile("TaskManager-Slave-InterruptReceivedAction-Prelude-Template.py")
        outFile.OutputLine(input)
        input = self.ReadIncludeFile("TaskManager-Slave-InterruptReceivedAction-Main-Template.py")
        outFile.OutputLine(input)
        outFile.OutputLine("</InterruptReceivedAction>\n")

        outFile.OutputLine("<PostConfigChangeAction>")
        input = self.ReadIncludeFile("TaskManager-Slave-PostConfigChangeAction-Prelude-Template.py")
        #childSubscriber=
        #configEntries={ "ID" : proc.fID }
        configEntries={  }
        processedInput = self.ProcessString( input, configEntries, self.fIncludePaths, 1 )
        outFile.OutputLine(processedInput+"\n")
        input = self.ReadIncludeFile("TaskManager-Slave-PostConfigChangeAction-Main-Template.py")
        processedInput = self.ProcessString( input, configEntries, self.fIncludePaths, 1 )
        outFile.OutputLine(processedInput+"\n")
        outFile.OutputLine("</PostConfigChangeAction>\n")
        
        outFile.OutputLine("</StateSupervisionConfig>\n")

        
        outFile.OutputLine("\n\n")
        for proc in processes:
            #print "GenerateTaskManagerSlaveFile",proc,"-",place,":",time.clock()
            place=place+1
            children = self.GetChildren( procList, proc, mapper )
            inlaws = self.GetInlaws( procList, proc, mapper )
            parents = self.GetParents( procList, proc, mapper )
            # print proc.fID," children:",
            # for c in children: print c.fID,
            # print
            # print proc.fID," parents:",
            # for c in parents: print c.fID,
            # print
            # print "GenerateTaskManagerSlaveFile",proc,"-",place,":",time.clock()
            place=place+1
            outFile.OutputLine("\n\n")
            outFile.OutputLine("  <Process ID=\""+proc.fID+"\" type=\"program\">\n")
            outFile.OutputLine("    <Address>"+proc.GetControlURL()+"</Address>\n")
            cmd = proc.GetCmd()
            cmd = cmd.replace( "&", "&amp;" )
            cmd = cmd.replace( "<", "&lt;" )
            cmd = cmd.replace( ">", "&gt;" )
            outFile.OutputLine("    <Command>"+cmd+"</Command>\n")
            outFile.OutputLine("    <PostStartAction>import KIPTaskMan,string\n")
            outFile.OutputLine("KIPTaskMan.Log( 2, \"Connecting to process '"+proc.fID+"'\" )\n")
            outFile.OutputLine("try:\n")
            outFile.OutputLine("    connected=1\n")
            outFile.OutputLine("    KIPTaskMan.Connect( \""+proc.fID+"\" )\n")
            outFile.OutputLine("except:\n")
            outFile.OutputLine("    connected=0\n")
            outFile.OutputLine("    pass\n")
            outFile.OutputLine("if not connected:\n")
            outFile.OutputLine("    KIPTaskMan.Log( 4, \"Connection to process '"+proc.fID+"' failed\" )\n")
            outFile.OutputLine("</PostStartAction>\n")
            outFile.OutputLine("    <PreTerminateAction>import KIPTaskMan,string\n")
            outFile.OutputLine("KIPTaskMan.SuppressComErrorLogs( \""+proc.fID+"\", 30000000 )\n")
            outFile.OutputLine("KIPTaskMan.Log( 2, \"Disconnecting from process '"+proc.fID+"'\" )\n")
            outFile.OutputLine("KIPTaskMan.Disconnect( \""+proc.fID+"\" )\n")
            outFile.OutputLine("</PreTerminateAction>\n")
            outFile.OutputLine("    <StateChangeAction>")
            input = self.ReadIncludeFile("TaskManager-Slave-Process-StateChangeAction-"+proc.GetType()+"-Prelude-Template.py")
            outFile.OutputLine(input)
            outFile.OutputLine("\n")
            children.sort( key=lambda x: x.fID )
            for c in children:
                outFile.OutputLine("children.append( \""+c.fID+"\" )\n")
                outFile.OutputLine("child_subscribe_keyword[\""+c.fID+"\"] = \"")
                if c.GetType()=="snk" or c.GetType()=="sbh":
                    outFile.OutputLine("Configured")
                else:
                    outFile.OutputLine("Accepting-Subscriptions")
                outFile.OutputLine("\"\n")
            inlaws.sort( key=lambda x: x.fID )
            for il in inlaws:
                outFile.OutputLine("inlaws.append( \""+il.fID+"\" )\n")
            if proc.GetType()=="eg" or proc.GetType()=="em":
                for par in parents:
                    outFile.OutputLine("parents.append( \""+par.fID+"\" )\n")
            outFile.OutputLine("\n")
            inputStr = self.ReadIncludeFile("TaskManager-Slave-Process-StateChangeAction-"+proc.GetType()+"-Main-Template.py")
            #print "GenerateTaskManagerSlaveFile",proc,"-",place,":",time.clock()
            place=place+1
            configureParameters=proc.GetConfigParams()
            if proc.GetType()=="src" or proc.GetType()=="pbh":
                processedInput = self.ProcessString( inputStr, { "ID" : proc.fID, "CONFIGUREPARAMETERS" : proc.GetConfigParams() }, self.fIncludePaths, 1 )
            else:
                # print proc, "-", proc.fID, "-", parents, proc.fVirtualParents, "-", proc.fParents
                processedInput = self.ProcessString( inputStr, { "ID" : proc.fID, "ParentID" : parents[0].fID, "CONFIGUREPARAMETERS" : proc.GetConfigParams() }, self.fIncludePaths, 1 )
            #print "GenerateTaskManagerSlaveFile",proc,"-",place,":",time.clock()
            place=place+1
            outFile.OutputLine(processedInput+"\n")
            #print "GenerateTaskManagerSlaveFile",proc,"-",place,":",time.clock()
            place=place+1
            outFile.OutputLine("</StateChangeAction>\n")
            if proc.GetType()=="prc":
                outFile.OutputLine("<PostConfigChangeAction>")
                inputStr = self.ReadIncludeFile( "TaskManager-Slave-Process-PostConfigChangeAction-"+proc.GetType()+"-Prelude-Template.py")
                configEntries={ "ID" : proc.fID }
                processedInput = self.ProcessString( inputStr, configEntries, self.fIncludePaths, 1 )
                outFile.OutputLine(processedInput+"\n")
                inputStr = self.ReadIncludeFile("TaskManager-Slave-Process-PostConfigChangeAction-"+proc.GetType()+"-Main-Template.py")
                processedInput = self.ProcessString( inputStr, configEntries, self.fIncludePaths, 1 )
                outFile.OutputLine(processedInput+"\n")
                outFile.OutputLine("</PostConfigChangeAction>\n")

                outFile.OutputLine("<PreConfigChangePhase1Action>")
                inputStr = self.ReadIncludeFile("TaskManager-Slave-Process-PreConfigChangePhase1Action-"+proc.GetType()+"-Prelude-Template.py")
                #childSubscriber=
                #print proc.fID, parents, children
                #configEntries={ "ID" : proc.fID, "ParentID" : parents[0].fID, "ParentPublisher" : self.GetPublisherIDForSubscriber([proc],proc.fID+"Sub"), "ChildID" : children[0].fID, "ChildSubscriber" : self.GetSubscriberIDForPublisher(children,proc.fID+"Pub") }
                configEntries={ "ID" : proc.fID, "ParentID" : parents[0].fID, "ParentPublisher" : self.GetPublisherIDForSubscriber([proc],proc.fID+"Sub"),  }
                if len(children)>0:
                    configEntries["ChildID"] = children[0].fID
                    configEntries["ChildSubscriber"] = self.GetSubscriberIDForPublisher(children,proc.fID+"Pub")
                    configEntries["HasChild"] = "True"
                else:
                    configEntries["ChildID"] = ""
                    configEntries["ChildSubscriber"] = ""
                    configEntries["HasChild"] = "False"
                processedInput = self.ProcessString( inputStr, configEntries, self.fIncludePaths, 1 )
                outFile.OutputLine(processedInput+"\n")
                inputStr = self.ReadIncludeFile("TaskManager-Slave-Process-PreConfigChangePhase1Action-"+proc.GetType()+"-Main-Template.py")
                processedInput = self.ProcessString( inputStr, configEntries, self.fIncludePaths, 1 )
                outFile.OutputLine(processedInput+"\n")
                outFile.OutputLine("</PreConfigChangePhase1Action>\n")

                outFile.OutputLine("<PreConfigChangePhase3Action>")
                inputStr = self.ReadIncludeFile("TaskManager-Slave-Process-PreConfigChangePhase3Action-"+proc.GetType()+"-Prelude-Template.py")
                #childSubscriber=
                #configEntries={ "ID" : proc.fID, "ParentID" : parents[0].fID, "ParentPublisher" : self.GetPublisherIDForSubscriber([proc],proc.fID+"Sub"), "ChildID" : children[0].fID, "ChildSubscriber" : self.GetSubscriberIDForPublisher(children,proc.fID+"Pub") }
                configEntries={ "ID" : proc.fID, "ParentID" : parents[0].fID, "ParentPublisher" : self.GetPublisherIDForSubscriber([proc],proc.fID+"Sub"),  }
                if len(children)>0:
                    configEntries["ChildID"] = children[0].fID
                    configEntries["ChildSubscriber"] = self.GetSubscriberIDForPublisher(children,proc.fID+"Pub")
                    configEntries["HasChild"] = "True"
                else:
                    configEntries["ChildID"] = ""
                    configEntries["ChildSubscriber"] = ""
                    configEntries["HasChild"] = "False"
                processedInput = self.ProcessString( inputStr, configEntries, self.fIncludePaths, 1 )
                outFile.OutputLine(processedInput+"\n")
                inputStr = self.ReadIncludeFile("TaskManager-Slave-Process-PreConfigChangePhase3Action-"+proc.GetType()+"-Main-Template.py")
                processedInput = self.ProcessString( inputStr, configEntries, self.fIncludePaths, 1 )
                outFile.OutputLine(processedInput+"\n")
                outFile.OutputLine("</PreConfigChangePhase3Action>\n")
            if mapper.fAliceHLT and proc.GetType()=="prc":
                inputStr = self.ReadIncludeFile("TaskManager-Slave-Process-StateList-"+proc.GetType()+"-alicehlt-Template.xml")
            else:
                inputStr = self.ReadIncludeFile("TaskManager-Slave-Process-StateList-"+proc.GetType()+"-Template.xml")
            configEntries={ "ID" : proc.fID }
            if len(parents)>0:
                configEntries["ParentID"] = parents[0].fID
            processedInput = self.ProcessString( inputStr, configEntries, self.fIncludePaths, 1 )
            outFile.OutputLine(processedInput)
            outFile.OutputLine("</Process>\n")


        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1
        outFile.OutputLine("\n\n\n\n")
        outFile.OutputLine("<SlaveControlConfig Autonomous=\"false\">\n")
        group = config.GetNodeParent( node.fID )
        itas = self.GetInterruptTargetAddresses( config, mapper, group )
        for ita in itas:
            outFile.OutputLine("<InterruptTargetAddress>"+ita+"</InterruptTargetAddress>\n")
        for p in self.fAdditionalInterruptTargets:
            outFile.OutputLine("<InterruptTargetAddress>"+p+"</InterruptTargetAddress>\n")
        outFile.OutputLine("<Address>"+MakeLocalURL( self.fSlaveControlAddresses[node.fID] )+"</Address>\n")
        outFile.OutputLine("<CommandReceivedAction>")

        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1
        input = self.ReadIncludeFile("TaskManager-Slave-CommandReceivedAction-Prelude-Template.py")
        outFile.OutputLine(input)
        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1
        for proc in processes:
            if proc.GetType()=="src":
                IDList = "SourceIDs"
            elif proc.GetType()=="sbh":
                IDList = "ConnectCmdIDs"
            elif proc.GetType()=="pbh":
                IDList = "ConnectNoCmdIDs"
            else:
                IDList = "ProcSinkIDs"
            outFile.OutputLine(IDList+".append( \""+proc.fID+"\" )\n")
        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1
        input = self.ReadIncludeFile("TaskManager-Slave-CommandReceivedAction-Main-Template.py")
        outFile.OutputLine(input)
        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1
        outFile.OutputLine("</CommandReceivedAction>\n\n")

        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1
        if mapper.fAliceHLT:
            input = self.ReadIncludeFile("TaskManager-Slave-StateList-alicehlt-Template.xml")
        else:
            input = self.ReadIncludeFile("TaskManager-Slave-StateList-alicehlt-Template.xml")
        outFile.OutputLine(input)
        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1

        outFile.OutputLine("\n</SlaveControlConfig>\n")
        outFile.OutputLine("</Task>\n\n")

        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1
        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1
        outFile.FinishOutput()
        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1

    def GenerateTaskManagerMasterFile( self, config, mapper, procList ):
        if len(self.fMasterNodes)>1:
            active=True
        else:
            active=None
        for mnnr in range(len(self.fMasterNodes)):
            masterNode = self.fMasterNodes[mnnr]
            nextMasterNode = self.fMasterNodes[(mnnr+1) % len(self.fMasterNodes)]
            slaveNodes = []
            slaveNodeGroups = []
            for node in config.GetNodes():
                if (self.fEmptyNodes or  self.NodeHasProcesses( config, procList, node )) and config.GetNodeParent( node.fID )==None:
                    slaveNodes.append( node )
            for ng in config.GetNodeGroups():
                if config.GetNodeGroupParent( ng.fID )==None:
                    slaveNodeGroups.append( ng )
            node = config.GetNode( masterNode )
            nextNode = config.GetNode( nextMasterNode )
            platform = self.GetNodePlatform( node )
            outFileText = ""
            outFileLines = []
            outFileName = config.GetName()+"-"+node.fIDName+"-"+node.GetHostname()+"-Master.xml"
            try:
                outFile = FileOutputWriter(outFileName)
            except IOError:
                raise ChainConfigException( "Unable to open output file'"+outFileName+"' for node '"+node+"' TaskManager master configuration." )
            outFile.OutputLine("<Task name=\""+config.GetName()+"-MasterNode-"+node.fIDName+"\">\n")
            outFile.OutputLine("<InterfaceLibrary platform=\""+platform+"\">"+self.fTaskManDir+"/lib/"+platform+"/SlaveControlInterfaceLib.so</InterfaceLibrary>\n")

            if active!=None:
                if active:
                    actStr = "yes"
                else:
                    actStr = "no"
                failOverActive = " failover_active=\"%s\"" % ( actStr )
            else:
                failOverActive=""
            outFile.OutputLine("<StateSupervisionConfig interval=\"%d\"%s>\n" % ( self.fSlavePollInterval, failOverActive ))
            self.fMasterInterruptAddresses[masterNode] = self.GetInterruptTargetAddresses( config, mapper, None, masterNode = masterNode )[0]
            outFile.OutputLine("<InterruptListenAddress>"+MakeLocalURL( self.fMasterInterruptAddresses[masterNode] )+"</InterruptListenAddress>\n")
            outFile.OutputLine("<StartupAction>import KIPTaskMan,string\n</StartupAction>\n")
            outFile.OutputLine("<StateChangeAction>")
    
            input = self.ReadIncludeFile("TaskManager-Master-StateChangeAction-Prelude-Template.py")
            outFile.OutputLine(input)
            for sn in slaveNodes:
                outFile.OutputLine("IDs.append( \""+config.GetName()+"-Node-"+sn.fIDName+"\" )\n")
            for sng in slaveNodeGroups:
                outFile.OutputLine("IDs.append( \""+config.GetName()+"-NodeGroup-"+sng.fIDName+"\" )\n")
            input = self.ReadIncludeFile("TaskManager-Master-StateChangeAction-Main-Template.py")
            processedInput = self.ProcessString( input, { "STOPRUNTIMEOUT" : str(self.fRunStopTimeout), "STOPTIMEOUT" : str(self.fStopTimeout), }, self.fIncludePaths, 1 )
            outFile.OutputLine(processedInput+"\n")
            outFile.OutputLine("</StateChangeAction>\n")
            outFile.OutputLine("\n\n")
            if 0:
                outFile.OutputLine("<!-- <PeriodicPollAction interval=\"2000000\">")
                input = self.ReadIncludeFile("TaskManager-Master-PeriodicPollAction-Prelude-Template.py")
                outFile.OutputLine(input)
                for sn in slaveNodes:
                    outFile.OutputLine("IDs.append( \""+config.GetName()+"-Node-"+sn.fIDName+"\" )\n")
                for sng in slaveNodeGroups:
                    outFile.OutputLine("IDs.append( \""+config.GetName()+"-NodeGroup-"+sng.fIDName+"\" )\n")
                input = self.ReadIncludeFile("TaskManager-Master-PeriodicPollAction-Main-Template.py")
                processedInput = self.ProcessString( input, { "STOPRUNTIMEOUT" : str(self.fRunStopTimeout), "STOPTIMEOUT" : str(self.fStopTimeout), }, self.fIncludePaths, 1 )
                outFile.OutputLine(processedInput+"\n")
                outFile.OutputLine("</PeriodicPollAction> -->\n")
        
            outFile.OutputLine("\n\n")
            outFile.OutputLine("<InterruptReceivedAction>")
            input = self.ReadIncludeFile("TaskManager-Master-InterruptReceivedAction-Prelude-Template.py")
            outFile.OutputLine(input)
            input = self.ReadIncludeFile("TaskManager-Master-InterruptReceivedAction-Main-Template.py")
            outFile.OutputLine(input)
            outFile.OutputLine("</InterruptReceivedAction>\n")
            outFile.OutputLine("</StateSupervisionConfig>\n")
            
            outFile.OutputLine("\n\n")
            for sng in slaveNodeGroups:
                outFile.OutputLine("\n\n")
                outFile.OutputLine("<Process ID=\""+config.GetName()+"-NodeGroup-"+sng.fIDName+"\" type=\"slaveprogram\">\n")
                outFile.OutputLine("<Address>tcpmsg://"+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+":"+( "%d" % ( sng.GetSlaveControlSocket() ) )+"/</Address>\n")
                if self.fUseSSH:
                    shell = "ssh"
                else:
                    shell = "rsh"
                if self.fPrestartExec=="" or self.fPrestartExec==None:
                    prestart = ""
                else:
                    prestart = self.fPrestartExec+" ;"
                if self.fPostTerminateExec=="" or self.fPostTerminateExec==None:
                    postTerminate = ""
                else:
                    postTerminate = " ; "+self.fPostTerminateExec
                servant_platform=self.GetNodePlatform( config.GetNode( sng.GetGroupMasterNode() ) )
                outFile.OutputLine("<Command>"+shell+" "+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+" mkdir -p "+self.fRunDir+" ; ")
                dir=""
                if self.fRunDirPermissions!=None:
                    for p in self.fRunDir.split("/")[1:]:
                        dir=dir+"/"+p
                    outFile.OutputLine("chmod",self.fRunDirPermissions,dir+" 2>/dev/null ; ")
                outFile.OutputLine("cd "+self.fRunDir+" ; "+prestart+" "+self.fTaskManDir+"/bin/"+servant_platform+"/TaskManager "+self.fOutputDir+"/"+self.GetServantConfigName( config, sng )+" -V "+config.GetVerbosity()+" -nostdout"+self.fTaskManagerOptions)
                if mapper.fSysMESLogging:
                    outFile.OutputLine(" -sysmeslog")
                if mapper.fInfoLoggerLibrary!=None:
                    outFile.OutputLine(" -infologger "+mapper.fInfoLoggerLibrary)
                if postTerminate!="":
                    outFile.OutputLine(postTerminate)
                outFile.OutputLine("</Command>\n")
                outFile.OutputLine("    <PostStartAction>import KIPTaskMan,string\n")
                outFile.OutputLine("KIPTaskMan.Log( 2, \"Connecting to process '"+config.GetName()+"-NodeGroup-"+sng.fIDName+"'\" )\n")
                outFile.OutputLine("try:\n")
                outFile.OutputLine("    connected=1\n")
                outFile.OutputLine("    KIPTaskMan.Connect( \""+config.GetName()+"-NodeGroup-"+sng.fIDName+"\" )\n")
                outFile.OutputLine("except:\n")
                outFile.OutputLine("    connected=0\n")
                outFile.OutputLine("    pass\n")
                outFile.OutputLine("if not connected:\n")
                outFile.OutputLine("    KIPTaskMan.Log( 4, \"Connection to process '"+config.GetName()+"-NodeGroup-"+sng.fIDName+"' failed\" )\n")
                outFile.OutputLine("</PostStartAction>\n")
                outFile.OutputLine("    <PreTerminateAction>import KIPTaskMan,string\n")
                outFile.OutputLine("KIPTaskMan.SuppressComErrorLogs( \""+sng.fIDName+"\", 30000000 )\n")
                outFile.OutputLine("KIPTaskMan.Log( 2, \"Disconnecting from process '"+config.GetName()+"-NodeGroup-"+sng.fIDName+"'\" )\n")
                outFile.OutputLine("KIPTaskMan.Disconnect( \""+config.GetName()+"-NodeGroup-"+sng.fIDName+"\" )\n")
                outFile.OutputLine("</PreTerminateAction>\n")
                outFile.OutputLine("<FailOverPassiveKillAction>"+self.fScriptDir+"/kill_on_node.sh "+nextNode.GetHostname()+" \""+self.fTaskManDir+"/bin/"+servant_platform+"/TaskManager "+self.fOutputDir+"/"+self.GetServantConfigName( config, sng )+"\"</FailOverPassiveKillAction>\n")
                outFile.OutputLine("<FailOverPassiveMonitorCommand>"+self.fScriptDir+"/check_running_on_node.sh "+nextNode.GetHostname()+" \""+self.fTaskManDir+"/bin/"+servant_platform+"/TaskManager "+self.fOutputDir+"/"+self.GetServantConfigName( config, sng )+"\"</FailOverPassiveMonitorCommand>\n")
                outFile.OutputLine("</Process>\n")
            outFile.OutputLine("\n")
    
            for sn in slaveNodes:
                outFile.OutputLine("\n\n")
                outFile.OutputLine("<Process ID=\""+config.GetName()+"-Node-"+sn.fIDName+"\" type=\"slaveprogram\">\n")
                outFile.OutputLine("<Address>"+self.fSlaveControlAddresses[sn.fID]+"</Address>\n")
                if self.fUseSSH:
                    shell = "ssh"
                else:
                    shell = "rsh"
                if self.fPrestartExec=="" or self.fPrestartExec==None:
                    prestart = ""
                else:
                    prestart = self.fPrestartExec+" ;"
                if self.fPostTerminateExec=="" or self.fPostTerminateExec==None:
                    postTerminate = ""
                else:
                    postTerminate = " ; "+self.fPostTerminateExec
                slave_platform=self.GetNodePlatform( sn )
                outFile.OutputLine("<Command>"+shell+" "+sn.GetHostname()+" mkdir -p "+self.fRunDir+" ; ")
                dir=""
                if self.fRunDirPermissions!=None:
                    for p in self.fRunDir.split("/")[1:]:
                        dir=dir+"/"+p
                        outFile.OutputLine("chmod",self.fRunDirPermissions,dir+" 2>/dev/null ; ")
                #outFile.OutputLine("cd "+self.fRunDir+" ; "+prestart+" "+self.fTaskManDir+"/bin/"+slave_platform+"/TaskManager "+self.fOutputDir+"/"+self.GetSlaveConfigName( config, sn )+" -V "+config.GetVerbosity()+" -nostdout"+self.fTaskManagerOptions)
                outFile.OutputLine("cd "+self.fRunDir+" ; "+prestart+" ")
                if self.fConfigCreateLate:
                    if len(self.fConfigCreateLateFiles)<=0:
                        outFile.OutputLine(self.fConfigCreateLateProgram+" "+self.fConfigCreateLateParameters+" -latecreationdata "+self.fOutputDir+"/"+config.GetName()+"-ProcessList.pickle")
                    else:
                        outFile.OutputLine(self.fConfigCreateLateProgram+" "+self.fConfigCreateLateParameters)
                        for cclf in self.fConfigCreateLateFiles:
                            outFile.OutputLine(" -latecreationconfig "+self.fOutputDir+"/"+cclf)
                    outFile.OutputLine(" -latecreationnode "+sn.fID+" ; ")
                outFile.OutputLine(self.fTaskManDir+"/bin/"+slave_platform+"/TaskManager ")
                if self.fConfigCreateLate:
                    outFile.OutputLine(self.fRunDir)
                else:
                    outFile.OutputLine(self.fOutputDir)
                outFile.OutputLine("/"+self.GetSlaveConfigName( config, sn )+" -V "+config.GetVerbosity()+" -nostdout"+self.fTaskManagerOptions)
                if self.fEmptyNodes:
                    outFile.OutputLine(" -allownoprocesses")
                if mapper.fSysMESLogging:
                    outFile.OutputLine(" -sysmeslog")
                if mapper.fInfoLoggerLibrary!=None:
                    outFile.OutputLine(" -infologger "+mapper.fInfoLoggerLibrary)
                if postTerminate!="":
                    outFile.OutputLine(postTerminate)
                outFile.OutputLine("</Command>\n")
                outFile.OutputLine("    <PostStartAction>import KIPTaskMan,string\n")
                outFile.OutputLine("KIPTaskMan.Log( 2, \"Connecting to process '"+config.GetName()+"-Node-"+sn.fIDName+"'\" )\n")
                outFile.OutputLine("try:\n")
                outFile.OutputLine("    connected=1\n")
                outFile.OutputLine("    KIPTaskMan.Connect( \""+config.GetName()+"-Node-"+sn.fIDName+"\" )\n")
                outFile.OutputLine("except:\n")
                outFile.OutputLine("    connected=0\n")
                outFile.OutputLine("    pass\n")
                outFile.OutputLine("if not connected:\n")
                outFile.OutputLine("    KIPTaskMan.Log( 4, \"Connection to process '"+config.GetName()+"-Node-"+sn.fIDName+"' failed\" )\n")
                outFile.OutputLine("</PostStartAction>\n")
                outFile.OutputLine("    <PreTerminateAction>import KIPTaskMan,string\n")
                outFile.OutputLine("KIPTaskMan.SuppressComErrorLogs( \""+sn.fIDName+"\", 30000000 )\n")
                outFile.OutputLine("KIPTaskMan.Log( 2, \"Disconnecting from process '"+config.GetName()+"-Node-"+sn.fIDName+"'\" )\n")
                outFile.OutputLine("KIPTaskMan.Disconnect( \""+config.GetName()+"-Node-"+sn.fIDName+"\" )\n")
                outFile.OutputLine("</PreTerminateAction>\n")
                outFile.OutputLine("<FailOverPassiveKillAction>"+self.fScriptDir+"/kill_on_node.sh "+nextNode.GetHostname()+" \""+self.fTaskManDir+"/bin/"+slave_platform+"/TaskManager ")
                if self.fConfigCreateLate:
                    outFile.OutputLine(self.fRunDir)
                else:
                    outFile.OutputLine(self.fOutputDir)
                outFile.OutputLine("/"+self.GetSlaveConfigName( config, sn )+"\"</FailOverPassiveKillAction>\n")
                outFile.OutputLine("<FailOverPassiveMonitorCommand>"+self.fScriptDir+"/check_running_on_node.sh "+nextNode.GetHostname()+" \""+self.fTaskManDir+"/bin/"+slave_platform+"/TaskManager ")
                if self.fConfigCreateLate:
                    outFile.OutputLine(self.fRunDir)
                else:
                    outFile.OutputLine(self.fOutputDir)
                outFile.OutputLine("/"+self.GetSlaveConfigName( config, sn )+"\"</FailOverPassiveMonitorCommand>\n")
                outFile.OutputLine("</Process>\n")
    
            outFile.OutputLine("\n\n\n\n")
    
            if self.fMasterIsAutonomous:
                auto = "true"
            else:
                auto = "false"
            outFile.OutputLine("<SlaveControlConfig Autonomous=\""+auto+"\">\n")
            for p in self.fAdditionalInterruptTargets:
                outFile.OutputLine("<InterruptTargetAddress>"+p+"</InterruptTargetAddress>\n")
            self.fMasterSlaveControlAddress = "tcpmsg://"+node.GetHostname()+":"+( "%d" % (self.GetMasterSlaveControlSocket( config, mapper, node.fID)) )+"/"
            outFile.OutputLine("<Address>"+MakeLocalURL( self.fMasterSlaveControlAddress )+"</Address>\n")
            outFile.OutputLine("<TargetAddress>"+self.fMasterSlaveControlAddress+"</TargetAddress>\n")
            outFile.OutputLine("<CommandReceivedAction>")
    
            input = self.ReadIncludeFile("TaskManager-Master-CommandReceivedAction-Prelude-Template.py")
            outFile.OutputLine(input)
            for sn in slaveNodes:
                outFile.OutputLine("IDs.append( \""+config.GetName()+"-Node-"+sn.fIDName+"\" )\n")
            for sng in slaveNodeGroups:
                outFile.OutputLine("IDs.append( \""+config.GetName()+"-NodeGroup-"+sng.fIDName+"\" )\n")
            input = self.ReadIncludeFile("TaskManager-Master-CommandReceivedAction-Main-Template.py")
            processedInput = self.ProcessString( input, { "STOPRUNTIMEOUT" : str(self.fRunStopTimeout), "STOPTIMEOUT" : str(self.fStopTimeout), }, self.fIncludePaths, 1 )
            outFile.OutputLine(processedInput+"\n")
            outFile.OutputLine("</CommandReceivedAction>\n\n")
    
            if mapper.fAliceHLT:
                input = self.ReadIncludeFile("TaskManager-Master-StateList-alicehlt-Template.xml")
            else:
                input = self.ReadIncludeFile("TaskManager-Master-StateList-Template.xml")
            outFile.OutputLine(input)
    
            outFile.OutputLine("\n</SlaveControlConfig>\n")
            outFile.OutputLine("</Task>\n\n")
    
            outFile.FinishOutput()
            active=False
    
    def GenerateTaskManagerServantFile( self, config, mapper, procList, nodeGroup ):
        slaveNodes = []
        slaveNodeGroups = []
        for node in config.GetNodes():
            if (self.fEmptyNodes or self.NodeHasProcesses( config, procList, node )) and config.GetNodeParent( node.fID )!=None and config.GetNodeParent( node.fID ).fID==nodeGroup.fID:
                slaveNodes.append( node )
        for ng in config.GetNodeGroups():
            if config.GetNodeGroupParent( ng.fID )!=None and config.GetNodeGroupParent( ng.fID ).fID==nodeGroup.fID:
                slaveNodeGroups.append( ng )
        node = config.GetNode( nodeGroup.GetGroupMasterNode() )
        platform = self.GetNodePlatform( node )
        outFileText = ""
        outFileLines = []
        outFileName = self.GetServantConfigName( config, nodeGroup )
        try:
            outFile = FileOutputWriter(outFileName)
        except IOError:
            raise ChainConfigException( "Unable to open output file'"+outFileName+"' for node '"+node+"' TaskManager servant "+nodeGroup.fID+" configuration." )
        outFile.OutputLine("<Task name=\""+config.GetName()+"-ServantNode-"+nodeGroup.fIDName+"-"+node.fIDName+"\">\n")
        outFile.OutputLine("<InterfaceLibrary platform=\""+platform+"\">"+self.fTaskManDir+"/lib/"+platform+"/SlaveControlInterfaceLib.so</InterfaceLibrary>\n")

        outFile.OutputLine("<StateSupervisionConfig interval=\"%d\">\n" % ( self.fSlavePollInterval ))
        interruptAddress = self.GetInterruptTargetAddresses( config, mapper, nodeGroup )[0]
        outFile.OutputLine("<InterruptListenAddress>"+MakeLocalURL( interruptAddress )+"</InterruptListenAddress>\n")
        #outFile.OutputLine("<StartupAction>import KIPTaskMan,string\n</StartupAction>\n")
        outFile.OutputLine("<StartupAction>")
        input = self.ReadIncludeFile("TaskManager-Servant-StartupAction-Prelude-Template.py")
        outFile.OutputLine(input)
        for sn in slaveNodes:
            outFile.OutputLine("IDs.append( \""+config.GetName()+"-Node-"+sn.fIDName+"\" )\n")
        for sng in slaveNodeGroups:
            outFile.OutputLine("IDs.append( \""+config.GetName()+"-NodeGroup-"+sng.fIDName+"\" )\n")
        input = self.ReadIncludeFile("TaskManager-Servant-StartupAction-Main-Template.py")
        outFile.OutputLine(input)
        outFile.OutputLine("\n</StartupAction>\n")

        outFile.OutputLine("<StateChangeAction>")

        input = self.ReadIncludeFile("TaskManager-Servant-StateChangeAction-Prelude-Template.py")
        outFile.OutputLine(input)
        for sn in slaveNodes:
            outFile.OutputLine("IDs.append( \""+config.GetName()+"-Node-"+sn.fIDName+"\" )\n")
        for sng in slaveNodeGroups:
            outFile.OutputLine("IDs.append( \""+config.GetName()+"-NodeGroup-"+sng.fIDName+"\" )\n")
        input = self.ReadIncludeFile("TaskManager-Servant-StateChangeAction-Main-Template.py")
        outFile.OutputLine(input)
        outFile.OutputLine("</StateChangeAction>\n")
        outFile.OutputLine("\n\n")
        if 0:
            outFile.OutputLine("<!-- <PeriodicPollAction interval=\"2000000\">")
            input = self.ReadIncludeFile("TaskManager-Servant-PeriodicPollAction-Prelude-Template.py")
            outFile.OutputLine(input)
            for sn in slaveNodes:
                outFile.OutputLine("IDs.append( \""+config.GetName()+"-Node-"+sn.fID+"\" )\n")
            for sng in slaveNodeGroups:
                outFile.OutputLine("IDs.append( \""+config.GetName()+"-NodeGroup-"+sng.fID+"\" )\n")
            input = self.ReadIncludeFile("TaskManager-Servant-PeriodicPollAction-Main-Template.py")
            outFile.OutputLine(input)
            outFile.OutputLine("</PeriodicPollAction> -->\n")
    
        outFile.OutputLine("\n\n")
        outFile.OutputLine("<InterruptReceivedAction>")
        input = self.ReadIncludeFile("TaskManager-Servant-InterruptReceivedAction-Prelude-Template.py")
        outFile.OutputLine(input)
        input = self.ReadIncludeFile("TaskManager-Servant-InterruptReceivedAction-Main-Template.py")
        outFile.OutputLine(input)
        outFile.OutputLine("</InterruptReceivedAction>\n")
        outFile.OutputLine("</StateSupervisionConfig>\n")
        
        outFile.OutputLine("\n\n")
        for sng in slaveNodeGroups:
            outFile.OutputLine("\n\n")
            outFile.OutputLine("<Process ID=\""+config.GetName()+"-NodeGroup-"+sng.fIDName+"\" type=\"slaveprogram\">\n")
            outFile.OutputLine("<Address>tcpmsg://"+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+":"+( "%d" % ( sng.GetSlaveControlSocket() ) )+"/</Address>\n")
            if self.fUseSSH:
                shell = "ssh"
            else:
                shell = "rsh"
            if self.fPrestartExec=="" or self.fPrestartExec==None:
                prestart = ""
            else:
                prestart = self.fPrestartExec+" ;"
            if self.fPostTerminateExec=="" or self.fPostTerminateExec==None:
                postTerminate = ""
            else:
                postTerminate = " ; "+self.fPostTerminateExec
            servant_platform=self.GetNodePlatform( config.GetNode( sng.GetGroupMasterNode() ) )
            outFile.OutputLine("<Command>"+shell+" "+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+" mkdir -p "+self.fRunDir+" ; ")
            dir=""
            if self.fRunDirPermissions!=None:
                for p in self.fRunDir.split("/")[1:]:
                    dir=dir+"/"+p
                    outFile.OutputLine("chmod",self.fRunDirPermissions,dir+" 2>/dev/null ; ")
            outFile.OutputLine("cd "+self.fRunDir+" ; "+prestart+" "+self.fTaskManDir+"/bin/"+servant_platform+"/TaskManager "+self.fOutputDir+"/"+self.GetServantConfigName( config, sng )+" -V "+config.GetVerbosity()+" -nostdout"+self.fTaskManagerOptions)
            if mapper.fSysMESLogging:
                outFile.OutputLine(" -sysmeslog")
            if mapper.fInfoLoggerLibrary!=None:
                outFile.OutputLine(" -infologger "+mapper.fInfoLoggerLibrary)
            if postTerminate!="":
                outFile.OutputLine(postTerminate)
            outFile.OutputLine("</Command>\n")
            outFile.OutputLine("    <PostStartAction>import KIPTaskMan,string\n")
            outFile.OutputLine("KIPTaskMan.Log( 2, \"Connecting to process '"+config.GetName()+"-NodeGroup-"+sng.fIDName+"'\" )\n")
            outFile.OutputLine("try:\n")
            outFile.OutputLine("    connected=1\n")
            outFile.OutputLine("    KIPTaskMan.Connect( \""+config.GetName()+"-NodeGroup-"+sng.fIDName+"\" )\n")
            outFile.OutputLine("except:\n")
            outFile.OutputLine("    connected=0\n")
            outFile.OutputLine("    pass\n")
            outFile.OutputLine("if not connected:\n")
            outFile.OutputLine("    KIPTaskMan.Log( 4, \"Connection to process '"+config.GetName()+"-NodeGroup-"+sng.fIDName+"' failed\" )\n")
            outFile.OutputLine("</PostStartAction>\n")
            outFile.OutputLine("    <PreTerminateAction>import KIPTaskMan,string\n")
            outFile.OutputLine("KIPTaskMan.SuppressComErrorLogs( \""+sng.fIDName+"\", 30000000 )\n")
            outFile.OutputLine("KIPTaskMan.Log( 2, \"Disconnecting from process '"+config.GetName()+"-NodeGroup-"+sng.fIDName+"'\" )\n")
            outFile.OutputLine("KIPTaskMan.Disconnect( \""+config.GetName()+"-NodeGroup-"+sng.fIDName+"\" )\n")
            outFile.OutputLine("</PreTerminateAction>\n")
            outFile.OutputLine("</Process>\n")
        outFile.OutputLine("\n")

        for sn in slaveNodes:
            outFile.OutputLine("\n\n")
            outFile.OutputLine("<Process ID=\""+config.GetName()+"-Node-"+sn.fIDName+"\" type=\"slaveprogram\">\n")
            outFile.OutputLine("<Address>"+self.fSlaveControlAddresses[sn.fID]+"</Address>\n")
            if self.fUseSSH:
                shell = "ssh"
            else:
                shell = "rsh"
            if self.fPrestartExec=="" or self.fPrestartExec==None:
                prestart = ""
            else:
                prestart = self.fPrestartExec+" ;"
            if self.fPostTerminateExec=="" or self.fPostTerminateExec==None:
                postTerminate = ""
            else:
                postTerminate = " ; "+self.fPostTerminateExec
            slave_platform=self.GetNodePlatform( sn )
            outFile.OutputLine("<Command>"+shell+" "+sn.GetHostname()+" mkdir -p "+self.fRunDir+" ; ")
            dir=""
            if self.fRunDirPermissions!=None:
                for p in self.fRunDir.split("/")[1:]:
                    dir=dir+"/"+p
                    outFile.OutputLine("chmod",self.fRunDirPermissions,dir+" 2>/dev/null ; ")
            outFile.OutputLine("cd "+self.fRunDir+" ; "+prestart+" ")
            if self.fConfigCreateLate:
                if len(self.fConfigCreateLateFiles)<=0:
                    outFile.OutputLine(self.fConfigCreateLateProgram+" "+self.fConfigCreateLateParameters+" -latecreationdata "+self.fOutputDir+"/"+config.GetName()+"-ProcessList.pickle")
                else:
                    outFile.OutputLine(self.fConfigCreateLateProgram+" "+self.fConfigCreateLateParameters)
                    for cclf in self.fConfigCreateLateFiles:
                        outFile.OutputLine(" -latecreationconfig "+self.fOutputDir+"/"+cclf)
                outFile.OutputLine(" -latecreationnode "+sn.fIDName+" ; ")
            outFile.OutputLine(self.fTaskManDir+"/bin/"+slave_platform+"/TaskManager ")
            if self.fConfigCreateLate:
                outFile.OutputLine(self.fRunDir)
            else:
                outFile.OutputLine(self.fOutputDir)
            outFile.OutputLine("/"+self.GetSlaveConfigName( config, sn )+" -V "+config.GetVerbosity()+" -nostdout"+self.fTaskManagerOptions)
            if self.fEmptyNodes:
                outFile.OutputLine(" -allownoprocesses")
            if mapper.fSysMESLogging:
                outFile.OutputLine(" -sysmeslog")
            if mapper.fInfoLoggerLibrary!=None:
                outFile.OutputLine(" -infologger "+mapper.fInfoLoggerLibrary)
            if postTerminate!="":
                outFile.OutputLine(postTerminate)
            outFile.OutputLine("</Command>\n")
            outFile.OutputLine("    <PostStartAction>import KIPTaskMan,string\n")
            outFile.OutputLine("KIPTaskMan.Log( 2, \"Connecting to process '"+config.GetName()+"-Node-"+sn.fIDName+"'\" )\n")
            outFile.OutputLine("try:\n")
            outFile.OutputLine("    connected=1\n")
            outFile.OutputLine("    KIPTaskMan.Connect( \""+config.GetName()+"-Node-"+sn.fIDName+"\" )\n")
            outFile.OutputLine("except:\n")
            outFile.OutputLine("    connected=0\n")
            outFile.OutputLine("    pass\n")
            outFile.OutputLine("if not connected:\n")
            outFile.OutputLine("    KIPTaskMan.Log( 4, \"Connection to process '"+config.GetName()+"-Node-"+sn.fIDName+"' failed\" )\n")
            outFile.OutputLine("</PostStartAction>\n")
            outFile.OutputLine("    <PreTerminateAction>import KIPTaskMan,string\n")
            outFile.OutputLine("KIPTaskMan.SuppressComErrorLogs( \""+sn.fIDName+"\", 30000000 )\n")
            outFile.OutputLine("KIPTaskMan.Log( 2, \"Disconnecting from process '"+config.GetName()+"-Node-"+sn.fIDName+"'\" )\n")
            outFile.OutputLine("KIPTaskMan.Disconnect( \""+config.GetName()+"-Node-"+sn.fIDName+"\" )\n")
            outFile.OutputLine("</PreTerminateAction>\n")
            outFile.OutputLine("</Process>\n")

        outFile.OutputLine("\n\n\n\n")

##~         if self.fMasterIsAutonomous:
##~             auto = "true"
##~         else:
##~             auto = "false"
        auto="false"
        outFile.OutputLine("<SlaveControlConfig Autonomous=\""+auto+"\">\n")
        group = config.GetNodeParent( node.fID )
        itas = self.GetInterruptTargetAddresses( config, mapper, group )
        for ita in itas:
            outFile.OutputLine("<InterruptTargetAddress>"+ita+"</InterruptTargetAddress>\n")
        for p in self.fAdditionalInterruptTargets:
            outFile.OutputLine("<InterruptTargetAddress>"+p+"</InterruptTargetAddress>\n")
        self.fSlaveControlAddress = "tcpmsg://"+node.GetHostname()+":"+( "%d" % (nodeGroup.GetSlaveControlSocket()) )+"/"
        outFile.OutputLine("<Address>"+MakeLocalURL( self.fSlaveControlAddress )+"</Address>\n")
        outFile.OutputLine("<CommandReceivedAction>")

        input = self.ReadIncludeFile("TaskManager-Servant-CommandReceivedAction-Prelude-Template.py")
        outFile.OutputLine(input)
        for sn in slaveNodes:
            outFile.OutputLine("IDs.append( \""+config.GetName()+"-Node-"+sn.fIDName+"\" )\n")
        for sng in slaveNodeGroups:
            outFile.OutputLine("IDs.append( \""+config.GetName()+"-NodeGroup-"+sng.fIDName+"\" )\n")
        input = self.ReadIncludeFile("TaskManager-Servant-CommandReceivedAction-Main-Template.py")
        outFile.OutputLine(input)
        outFile.OutputLine("</CommandReceivedAction>\n\n")

        if mapper.fAliceHLT:
            input = self.ReadIncludeFile("TaskManager-Servant-StateList-alicehlt-Template.xml")
        else:
            input = self.ReadIncludeFile("TaskManager-Servant-StateList-Template.xml")
        outFile.OutputLine(input)

        outFile.OutputLine("\n</SlaveControlConfig>\n")
        outFile.OutputLine("</Task>\n\n")

        outFile.FinishOutput()
    
    
    def GetProcessCountOnNode( self, config, procList, node ):
        if self.fProcessCountOnNodeCache.has_key(node):
            return self.fProcessCountOnNodeCache[node]
        cnt = 0
        for p in procList:
            if p.GetNode()==node.fID:
                cnt = cnt + 1
        self.fProcessCountOnNodeCache[node] = cnt
        return cnt

    def NodeHasProcesses( self, config, procList, node ):
        if self.fNodeHasProcessesCache.has_key(node):
            return self.fNodeHasProcessesCache[node]
        found=False
        nodeID=node.fID
        for p in procList:
            #if p.GetNode()==node.fID:
            if p.fNode==nodeID:
                found=True
                break
        self.fNodeHasProcessesCache[node] = found
        return found
    

    def GetNodeProcesses( self, config, procList, node ):
        if self.fNodeProcessesCache.has_key(node):
            return self.fNodeProcessesCache[node]
        nodeID=node.fID
        procs = []
        for p in procList:
            #if p.GetNode()==node.fID:
            if p.fNode==nodeID:
                procs.append( p )
        self.fNodeProcessesCache[node] = procs
        return procs

    def OpenIncludeFile( self, filename ):
        success = 0
        for ip in self.fIncludePaths:
            success=1
            try:
                file = open( ip+"/"+filename, "r" )
                break
            except IOError:
                success=0
        if not success:
            raise ChainConfigException( "Unable to open include file'"+filename+"'." )
        return file

    def ReadIncludeFile(self, filename):
        if not self.fIncludeFiles.has_key(filename):
            fh = self.OpenIncludeFile(filename)
            self.fIncludeFiles[filename] = fh.read()
            fh.close()
        return self.fIncludeFiles[filename]
    

    def ProcessString( self, string, varList, includePath, topLevel, incFileName = None ):
        for var in varList.keys():
            tmp = "$("+var+")"
            string = string.replace( tmp, varList[var] )
        return string

    def GetChildren( self, procList, proc, mapper ):
        return mapper.GetChildren( procList, proc )
#        pubIDs = mapper.ProcID2PublisherList(proc.fID)
#        children = []
#        for pubID in pubIDs:
#            cpl = mapper.Subscription2Proc(pubID)
#            if cpl!=None:
#                #children.append(cp)
#                children += cpl
#            else:
#                #print "procList:",procList
#                #print "proc:",proc.fID,proc
#                #print "mapper.fProcID2Publisher:",mapper.fProcID2Publisher
#                #print "mapper.fPublisher2Proc:",mapper.fPublisher2Proc
#                #print "mapper.fSubscription2Proc:",mapper.fSubscription2Proc
#                #print "mapper.fProcID2Subscription:",mapper.fProcID2Subscription
#                #raise ChainConfigException( "INTERNAL ERROR: Child process "+pubID+" for publisher process "+proc.fID+" cannot be found." )
#                if not self.fQuiet:
#                    print( "Warning: No child process using "+pubID+" of publisher process "+proc.fID+" can be found. (B)" )
#        return children
#        publisherMatch = re.compile( "-publisher\s+(\S+)\s?(.*)$" )
#        subscriberMatch = re.compile( "-subscribe\s+(\S+)\s+(\S+)\s?(.*)$" )
#        myIDs = []
#        myNode = proc.GetNode()
#        myID = proc.fID
#        #print "For "+myID
#        l = proc.GetCmd()+" "+proc.GetConfigParams()
#        while l != "":
#            match = publisherMatch.search( l )
#            if match != None:
#                myIDs.append( match.group( 1 ) )
#                #print "Found publisher : "+pub+" - node: "+node
#                l = match.group( 2 )
#            else:
#                l = ""
#        #print "Pubs:",myIDs
#        children = []
#        for p in procList:
#            if p.fID!=myID and p.GetNode()==myNode:
#                #print p.fID+": "+p.GetCmd()+" "+p.GetConfigParams()
#                l = p.GetCmd()+" "+p.GetConfigParams()
#                while l != "":
#                    match = subscriberMatch.search( l )
#                    if match != None:
#                        for id in myIDs:
#                            if match.group( 1 ) == id:
#                                children.append( p )
#                                break
#                        l = match.group( 3 )
#                    else:
#                        l = ""
#        #print "Subs:",
#        #for c in children:
#        #    print c.fID,",",
#        #print
#        return children

    def GetInlaws( self, procList, proc, mapper ):
        return mapper.GetInlaws( procList, proc )
#        children = self.GetChildren( procList, proc, mapper )
#        inlaws = set()
#        for c in children:
#            inlaws |= set(self.GetParents( procList, c, mapper ))
#        inlaws -= set([proc])
#        return list(inlaws)
##            for par in parents:
##                if par.fID!=proc.fID or par.GetNode()!=proc.GetNode():
##                    found=0
##                    for il in inlaws:
##                        if par.fID==il.fID and par.GetNode==il.GetNode():
##                            found=1
##                            break
##                    if not found:
##                        inlaws.append( par )
##        return inlaws

    def GetParents( self, procList, proc, mapper ):
        return mapper.GetParents( procList, proc )
#        pubIDs = mapper.ProcID2SubscriptionList(proc.fID)
#        parents = []
#        for pubID in pubIDs:
#            pp = mapper.Publisher2Proc(pubID)
#            if pp!=None:
#                parents.append(pp)
#            else:
#                raise ChainConfigException( "INTERNAL ERROR: Parent publisher process "+pubID+" for process "+proc.fID+" cannot be found (D)." )
#        return parents
#        publisherMatch = re.compile( "-publisher\s+(\S+)\s?(.*)$" )
#        subscriberMatch = re.compile( "-subscribe\s+(\S+)\s+(\S+)\s?(.*)$" )
#        parentPubs = []
#        myNode = proc.GetNode()
#        # print "For:",proc.fID
#        l = proc.GetCmd()+" "+proc.GetConfigParams()
#        while l != "":
#            match = subscriberMatch.search( l )
#            if match != None:
#                # print "Found Sub:",match.group( 1 )
#                parentPubs.append( match.group( 1 ) )
#                l = match.group( 3 )
#            else:
#                l = ""
#
#        parents = []
#                
#        for pb in parentPubs:
#            for p in procList:
#                if p.GetNode()==myNode:
#                    l = p.GetCmd()+" "+p.GetConfigParams()
#                    while l != "":
#                        match = publisherMatch.search( l )
#                        if match != None:
#                            # print "Found Pub:",match.group( 1 )
#                            if match.group( 1 ) == pb:
#                                parents.append( p )
#                                break
#                            l = match.group( 2 )
#                        else:
#                            l = ""
#        return parents

    def GetPublisherIDForSubscriber( self, procList, sub ):
        subscriberMatch = re.compile( "-subscribe\s+(\S+)\s+(\S+)\s?(.*)$" )
        for proc in procList:
            myNode = proc.GetNode()
            #print "For:",proc.fID
            l = proc.GetCmd()+" "+proc.GetConfigParams()
            while l != "":
                match = subscriberMatch.search( l )
                if match != None:
                    #print "Found Sub:",match.group( 1 )
                    if match.group(2)==sub:
                        return match.group(1)
                    l = match.group( 3 )
                else:
                    l = ""
        return None

    def GetSubscriberIDForPublisher( self, procList, pub ):
        subscriberMatch = re.compile( "-subscribe\s+(\S+)\s+(\S+)\s?(.*)$" )
        for proc in procList:
            myNode = proc.GetNode()
            #print "For:",proc.fID
            l = proc.GetCmd()+" "+proc.GetConfigParams()
            while l != "":
                match = subscriberMatch.search( l )
                if match != None:
                    #print "Found Sub:",match.group( 1 )
                    if match.group(1)==pub:
                        return match.group(2)
                    l = match.group( 3 )
                else:
                    l = ""
        return None

    def GetInterruptTargetAddresses( self, config, mapper, group, masterNode=None ):
        if group==None:
            if masterNode!=None and masterNode in self.fMasterNodes:
                return ( "tcpmsg://"+config.GetNode( masterNode ).GetHostname()+":"+( "%d" % (self.GetMasterListenSocket( config, mapper, masterNode)) )+"/", )
            else:
                return ( ("tcpmsg://"+config.GetNode( mn ).GetHostname()+":"+( "%d" % (self.GetMasterListenSocket( config, mapper, mn)) )+"/") for mn in self.fMasterNodes )
        else:
            return ( "tcpmsg://"+config.GetNode( group.GetGroupMasterNode() ).GetHostname()+":"+( "%d" % (group.GetListenSocket()) )+"/", )

        
    def GetServantConfigName( self, config, nodeGroup ):
        node = config.GetNode( nodeGroup.GetGroupMasterNode() )
        return config.GetName()+"-"+node.fIDName+"-"+node.GetHostname()+"-Servant-"+nodeGroup.fIDName+".xml"

    def GetSlaveConfigName( self, config, node ):
        return config.GetName()+"-"+node.fIDName+"-"+node.GetHostname()+"-Slave.xml"
        
    def SetMasterListenSockets( self, sockets ):
        self.fMasterListenSockets = sockets

    def SetMasterSlaveControlSockets( self, sockets ):
        self.fMasterSlaveControlSockets = sockets

    def SetSlaveListenSockets( self, sockets ):
        self.fSlaveListenSockets = sockets

    def SetSlaveSlaveControlSockets( self, sockets ):
        self.fSlaveSlaveControlSockets = sockets

    def GetMasterListenSocket( self, config, mapper, node ):
        if not self.fMasterListenSockets.has_key( node ):
            self.fMasterListenSockets[ node ] = mapper.GetNextSocket( config, node )
        return self.fMasterListenSockets[ node ]

    def GetMasterSlaveControlSocket( self, config, mapper, node ):
        if not self.fMasterSlaveControlSockets.has_key( node ):
            self.fMasterSlaveControlSockets[ node ] = mapper.GetNextSocket( config, node )
        return self.fMasterSlaveControlSockets[ node ]

    def GetSlaveListenSocket( self, config, mapper, node ):
        if not self.fSlaveListenSockets.has_key( node ):
            self.fSlaveListenSockets[ node ] = mapper.GetNextSocket( config, node )
        return self.fSlaveListenSockets[ node ]

    def GetSlaveSlaveControlSocket( self, config, mapper, node ):
        if not self.fSlaveSlaveControlSockets.has_key( node ):
            self.fSlaveSlaveControlSockets[ node ] = mapper.GetNextSocket( config, node )
        return self.fSlaveSlaveControlSockets[ node ]



class TaskManagerCheckOutputter(TaskManagerOutputter):
    def __init__( self, masterNode, useSSH, includePaths, taskManDir, frameworkDir, platform, runDir, outputDir, prestartExec, postTerminateExec, scriptdir ):
        TaskManagerOutputter.__init__( self, masterNode, useSSH, includePaths, taskManDir, frameworkDir, platform, runDir, outputDir, prestartExec, postTerminateExec, scriptdir, [] )
        self.fTCPPortPattern=re.compile( "tcp(msg|blob)://(\w*):(\d+)" )

    def GetSlaveConfigName( self, config, node ):
        return config.GetName()+"-"+node.fID+"-"+node.GetHostname()+"-Slave-Check.sh"


    def GenerateTaskManagerSlaveFile( self, config, mapper, procList, node ):
        platform = self.GetNodePlatform( node )
        if not self.fEmptyNodes and not self.NodeHasProcesses( config, procList, node ):
            return
        place = 0
        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1
        processes = self.GetNodeProcesses( config, procList, node )
        outFileText = ""
        outFileLines = []
        outFileName = self.GetSlaveConfigName( config, node )
        #print "GenerateTaskManagerSlaveFile",place,":",time.clock()
        place=place+1
        try:
            outFile = FileOutputWriter(outFileName)
        except IOError:
            raise ChainConfigException( "Unable to open output file'"+outFileName+"' for node '"+node.fID+"' TaskManager slave configuration." )
        outFile.OutputLine("#!/bin/bash\n")
        outFile.OutputLine("HADERROR=0\n")
        outFile.OutputLine("echo Checking Slave node "+node.fID+"/"+node.GetHostname()+" \\(System hostname: `hostname -s`\\)\n")
        outFile.OutputLine("if [[ ! -x "+self.fScriptDir+"/checkbinary.sh ]] ; then echo cannot execute "+self.fScriptDir+"/checkbinary.sh ; exit 1 ; fi\n")
        outFile.OutputLine("if ! "+self.fScriptDir+"/checkbinary.sh "+self.fScriptDir+"/checktcpport.sh "+self.fScriptDir+"/checklogin.sh ; then exit 1 ; fi\n")
        outFile.OutputLine("if ! "+ self.fScriptDir+"/checkbinary.sh \""+self.fFrameworkDir+"/bin/"+platform+"/CheckShm\" \""+self.fFrameworkDir+"/bin/"+platform+"/CheckOldDataFileType\"")
        outFile.OutputLine("if ! "+ self.fScriptDir+"/checklibraries.sh \""+self.fFrameworkDir+"/lib/"+platform+"/libTaskManLib.so\"w")
        for proc in processes:
            cmds=string.split( proc.GetCmd() )
            if len(cmds)>0:
                outFile.OutputLine(" \""+cmds[0]+"\"")
        outFile.OutputLine(" ; then HADERROR=1 ; fi\n")
        outFile.OutputLine("if ! "+self.fScriptDir+("/checktcpport.sh %d %d" % (self.GetSlaveListenSocket(config, mapper, node.fID),self.GetSlaveSlaveControlSocket( config, mapper, node.fID ))))
        for proc in processes:
            cmds=string.split( proc.GetCmd() )
            for c in cmds:
                match=self.fTCPPortPattern.search(c)
                if match != None and node.GetHostname()==match.group(2):
                    outFile.OutputLine(" %d" % (int(match.group(3))))
        outFile.OutputLine(" ; then HADERROR=1 ; fi\n")
        shms = ""
        for proc in processes:
            cmds=string.split( proc.GetCmd() )
            if proc.GetType()=="pbh":
                i=0
                shmsize = "XXX"
                while i<len(cmds):
                    if cmds[i] == "-shm" and i+3<len(cmds):
                        shms=shms+" "+cmds[i]+" "+cmds[i+1]+" "+cmds[i+2]
                        i=i+3
                    if cmds[i] == "-blob" and i+1<len(cmds):
                        shmsize = cmds[i+1]
                        i=i+1
                    i=i+1
                shms=shms+" "+shmsize
            else:
                i=0
                while i<len(cmds):
                    if cmds[i] == "-shm" and i+3<len(cmds):
                        shms=shms+" "+cmds[i]+" "+cmds[i+1]+" "+cmds[i+2]+" "+cmds[i+3]
                        i=i+3
                    i=i+1
        if len(shms)>0:
            outFile.OutputLine("if ! "+self.fFrameworkDir+"/bin/"+platform+"/CheckShm"+shms+" ; then HADERROR=1 ; fi\n")
        oldfiles=[]
        newfiles=[]
        fileblocksizes={}
        for proc in processes:
            if proc.GetType()=="src":
                cmds=string.split( proc.GetCmd() )
                dirs=string.split( cmds[0], "/" )
                if dirs[-1]=="ControlledFilePublisher" or dirs[-1]=="FilePublisher":
                    i=1
                    blksz = None
                    while i<len(cmds):
                        if cmds[i]=="-minblocksize" and i+1<len(cmds):
                            blksz=cmds[i+1]
                            break
                        i=i+1
                    i=1
                    while i<len(cmds):
                        if cmds[i]=="-datafile" and i+1<len(cmds):
                            newfiles.append(cmds[i+1])
                            if blksz != None:
                                fileblocksizes[cmds[i+1]] = blksz
                            i=i+1
                        if (cmds[i]=="-olddatafile" or cmds[i]=="-file") and i+1<len(cmds):
                            oldfiles.append(cmds[i+1])
                            if blksz != None:
                                fileblocksizes[cmds[i+1]] = blksz
                            i=i+1
                        i=i+1
        for f in oldfiles+newfiles:
            outFile.OutputLine("if [ ! -e "+f+" ] ; then echo Input data file "+f+" does not exist ; HADERROR=1 ; fi\n")
            outFile.OutputLine("if [ -e "+f+" -a \\( -b "+f+" -o \\( -c "+f+" -o \\( -d "+f+" -o \\( -p "+f+" -o -S "+f+" \\) \\) \\) \\) ] ; then echo Input data file "+f+" is not a real file ; HADERROR=1 ; fi\n")
            outFile.OutputLine("if [ -e "+f+" -a \\( ! -r "+f+" \\) ] ; then echo Input data file "+f+" is not readable ; HADERROR=1 ; fi\n")
            outFile.OutputLine("if [ -e "+f+" -a \\( ! -s "+f+" \\) ] ; then echo Input data file "+f+" has size 0 ; HADERROR=1 ; fi\n")
        if len(oldfiles)>0:
            outFile.OutputLine("if "+self.fFrameworkDir+"/bin/"+platform+"/CheckOldDataFileType")
            for f in oldfiles:
                outFile.OutputLine(" "+f)
            outFile.OutputLine(" ; then HADERROR=1 ; fi\n")
        for f in newfiles:
            outFile.OutputLine("if "+self.fFrameworkDir+"/bin/"+platform+"/CheckOldDataFileType "+f+" >/dev/null ; then echo File "+f+" is specified as new file format, but it might be old format \\(-olddatafile option\\) ; fi\n")
        for f in newfiles:
            outFile.OutputLine("if [ -e "+f+" -a ! \\( -b "+f+" -o -c "+f+" -o -d "+f+" -o -p "+f+" -o -S "+f+" -o ! -r "+f+" -o ! -s "+f+" \\) ] ; then SIZE=`wc -c "+f+" | awk '{print $1}'` ; if [ -n \"${SIZE}\" -a \"${SIZE}\" -gt "+fileblocksizes[f]+" ] ; then echo Data size of new type file "+f+" is greater than specified block size of "+fileblocksizes[f]+" bytes. ; HADERROR=1 ; fi ; fi\n" )
        for f in oldfiles:
            outFile.OutputLine("if [ -e "+f+" -a ! \\( -b "+f+" -o -c "+f+" -o -d "+f+" -o -p "+f+" -o -S "+f+" -o ! -r "+f+" -o ! -s "+f+" \\) ] ; then SIZE=`wc -c "+f+" | awk '{print $1}'` ; if [ -n \"${SIZE}\" -a \"${SIZE}\" -gt "+("%d" % ( int(fileblocksizes[f])+4))+" ] ; then echo Data size of old type file "+f+" is greater than specified block size of "+fileblocksizes[f]+" bytes. ; HADERROR=1 ; fi ; fi\n" )
            
                    

        # TODO Check memories

        outFile.OutputLine("exit $HADERROR\n")

        outFile.FinishOutput()
        os.chmod(outFileName, 0755)
        


    def GetServantConfigName( self, config, nodeGroup ):
        node = config.GetNode( nodeGroup.GetGroupMasterNode() )
        return config.GetName()+"-"+node.fID+"-"+node.GetHostname()+"-Servant-"+nodeGroup.fID+"-Check.sh"

    def GenerateTaskManagerServantFile( self, config, mapper, procList, nodeGroup ):
        slaveNodes = []
        slaveNodeGroups = []
        for node in config.GetNodes():
            if (self.fEmptyNodes or self.NodeHasProcesses( config, procList, node )) and config.GetNodeParent( node.fID )!=None and config.GetNodeParent( node.fID ).fID==nodeGroup.fID:
                slaveNodes.append( node )
        for ng in config.GetNodeGroups():
            if config.GetNodeGroupParent( ng.fID )!=None and config.GetNodeGroupParent( ng.fID ).fID==nodeGroup.fID:
                slaveNodeGroups.append( ng )
        node = config.GetNode( nodeGroup.GetGroupMasterNode() )
        platform = self.GetNodePlatform( node )
        outFileText = ""
        outFileLines = []
        outFileName = self.GetServantConfigName( config, nodeGroup )
        try:
            outFile = FileOutputWriter(outFileName)
        except IOError:
            raise ChainConfigException( "Unable to open output file'"+outFileName+"' for node '"+node+"' TaskManager servant "+nodeGroup.fID+" configuration." )
        if self.fUseSSH:
            shell = "ssh"
        else:
            shell = "rsh"
        simple_remote_return_value = self.fUseSSH
        #simple_remote_return_value = 0
        #print "Warning: Setting simple_remote_return_value to",simple_remote_return_value
        if self.fPrestartExec=="" or self.fPrestartExec==None:
            prestart = ""
        else:
            prestart = self.fPrestartExec+" \\;"
        if self.fPostTerminateExec=="" or self.fPostTerminateExec==None:
            postTerminate = ""
        else:
            postTerminate = " \\; "+self.fPostTerminateExec
        outFile.OutputLine("#!/bin/bash\n")
        outFile.OutputLine("HADERROR=0\n")
        outFile.OutputLine("echo Checking Servant node "+node.fID+"/"+node.GetHostname()+" \\(System hostname: `hostname -s`\\)\n")
        outFile.OutputLine("if [[ ! -x "+self.fScriptDir+"/checkbinary.sh ]] ; then echo cannot execute "+self.fScriptDir+"/checkbinary.sh ; exit 1 ; fi\n")
        outFile.OutputLine("if ! "+self.fScriptDir+"/checkbinary.sh "+self.fScriptDir+"/checktcpport.sh "+self.fScriptDir+"/checklogin.sh ; then exit 1 ; fi\n")
        outFile.OutputLine("if ! "+self.fScriptDir+"/checkbinary.sh "+self.fTaskManDir+"/bin/"+platform+"/TaskManager ; then HADERROR=1 ; fi \n")
        outFile.OutputLine("if ! "+self.fScriptDir+"/checklibraries.sh "+self.fTaskManDir+"/lib/"+platform+"/SlaveControlInterfaceLib.so ; then HADERROR=1 ; fi \n")
        outFile.OutputLine("if ! "+self.fScriptDir+("/checktcpport.sh %d %d" % (nodeGroup.GetListenSocket(),nodeGroup.GetSlaveControlSocket()))+" ; then HADERROR=1 ; fi\n")
        if len(slaveNodeGroups)+len(slaveNodes)>0:
            outFile.OutputLine("if ! "+self.fScriptDir+"/checklogin.sh")
            for sng in slaveNodeGroups:
                outFile.OutputLine(" "+shell+":"+config.GetNode( sng.GetGroupMasterNode() ).GetHostname())
            for sn in slaveNodes:
                outFile.OutputLine(" "+shell+":"+sn.GetHostname())
            outFile.OutputLine(" ; then HADERROR=1 ; fi\n")

        for sng in slaveNodeGroups:
            remotecmd = shell+" "+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+" "+prestart+" "+self.fOutputDir+"/"+self.GetServantConfigName( config, sng )+postTerminate
            outFile.OutputLine("if "+self.fScriptDir+"/checklogin.sh "+shell+":"+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+" >/dev/null ; then "+remotecmd)
            if not simple_remote_return_value:
                outFile.OutputLine(" \\; echo $? | python -c \"import os,sys\nlastline=\\\"\\\"\nline=sys.stdin.readline()\nwhile line != \\\"\\\":\n    if lastline != \\\"\\\":\n        print lastline,\n    lastline=line\n    line=sys.stdin.readline()\nif len(lastline)>0 and lastline[-1]==\\\"\\\\n\\\":\n    tmplastline=lastline[:-1]\nelse:\n    tmplastline=lastline\nretval=0\ntry:\n    retval=int(tmplastline)\nexcept:\n    print lastline,\nsys.exit( retval )\n\"")
            outFile.OutputLine(" ; if [[ ! $? ]] ; then HADERROR=1 ; fi")
            outFile.OutputLine(" ; fi\n")
        for sn in slaveNodes:
            remotecmd = shell+" "+sn.GetHostname()+" "+prestart+" "+self.fOutputDir+"/"+self.GetSlaveConfigName( config, sn )+postTerminate
            outFile.OutputLine("if "+self.fScriptDir+"/checklogin.sh "+shell+":"+sn.GetHostname()+" >/dev/null ; then "+remotecmd)
            if not simple_remote_return_value:
                outFile.OutputLine(" \\; echo $? | python -c \"import os,sys\nlastline=\\\"\\\"\nline=sys.stdin.readline()\nwhile line != \\\"\\\":\n    if lastline != \\\"\\\":\n        print lastline,\n    lastline=line\n    line=sys.stdin.readline()\nif len(lastline)>0 and lastline[-1]==\\\"\\\\n\\\":\n    tmplastline=lastline[:-1]\nelse:\n    tmplastline=lastline\nretval=0\ntry:\n    retval=int(tmplastline)\nexcept:\n    print lastline,\nsys.exit( retval )\n\"")
            outFile.OutputLine(" ; if [[ ! $? ]] ; then HADERROR=1 ; fi")
            outFile.OutputLine(" ; fi\n")
##        for sng in slaveNodeGroups:
##            outFile.OutputLine("if "+self.fScriptDir+"/checklogin.sh "+shell+":"+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+" >/dev/null ; then "+shell+" "+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+" "+prestart+" "+self.fOutputDir+"/"+self.GetServantConfigName( config, sng )+" ; fi\n")
##        for sn in slaveNodes:
##            outFile.OutputLine("if "+self.fScriptDir+"/checklogin.sh "+shell+":"+sn.GetHostname()+" >/dev/null ; then "+shell+" "+sn.GetHostname()+" "+prestart+" "+self.fOutputDir+"/"+self.GetSlaveConfigName( config, sn )+" ; fi\n")

        outFile.OutputLine("exit $HADERROR\n")

        outFile.FinishOutput()
        os.chmod(outFileName, 0755)



    def GenerateTaskManagerMasterFile( self, config, mapper, procList ):
        for masterNode in self.fMasterNodes:
            slaveNodes = []
            slaveNodeGroups = []
            for node in config.GetNodes():
                if (self.fEmptyNodes or self.NodeHasProcesses( config, procList, node )) and config.GetNodeParent( node.fID )==None:
                    slaveNodes.append( node )
            for ng in config.GetNodeGroups():
                if config.GetNodeGroupParent( ng.fID )==None:
                    slaveNodeGroups.append( ng )
            node = config.GetNode( masterNode )
            platform = self.GetNodePlatform( node )
            outFileText = ""
            outFileLines = []
            outFileName = config.GetName()+"-"+node.fID+"-"+node.GetHostname()+"-Master-Check.sh"
            try:
                outFile = FileOutputWriter(outFileName)
            except IOError:
                raise ChainConfigException( "Unable to open output file'"+outFileName+"' for node '"+node+"' TaskManager master configuration check script." )
            if self.fUseSSH:
                shell = "ssh"
            else:
                shell = "rsh"
            simple_remote_return_value = self.fUseSSH
            #simple_remote_return_value = 0
            #print "Warning: Setting simple_remote_return_value to",simple_remote_return_value
            if self.fPrestartExec=="" or self.fPrestartExec==None:
                prestart = ""
            else:
                prestart = self.fPrestartExec+" \\;"
            if self.fPostTerminateExec=="" or self.fPostTerminateExec==None:
                postTerminate = ""
            else:
                postTerminate = " \\; "+self.fPostTerminateExec
            
            outFile.OutputLine("#!/bin/bash\n")
            outFile.OutputLine("HADERROR=0\n")
            outFile.OutputLine("echo Checking Master node "+node.fID+"/"+node.GetHostname()+" \\(System hostname: `hostname -s`\\)\n")
            outFile.OutputLine("if [[ ! -x "+self.fScriptDir+"/checkbinary.sh ]] ; then echo cannot execute "+self.fScriptDir+"/checkbinary.sh ; exit 1 ; fi\n")
            outFile.OutputLine("if ! "+self.fScriptDir+"/checkbinary.sh "+self.fScriptDir+"/checktcpport.sh "+self.fScriptDir+"/checklogin.sh ; then exit 1 ; fi\n")
            outFile.OutputLine("if ! "+self.fScriptDir+"/checkbinary.sh "+self.fTaskManDir+"/bin/"+platform+"/TaskManager ; then HADERROR=1 ; fi\n")
            outFile.OutputLine("if ! "+self.fScriptDir+"/checklibraries.sh "+self.fTaskManDir+"/lib/"+platform+"/SlaveControlInterfaceLib.so ; then HADERROR=1 ; fi\n")
            outFile.OutputLine("if ! "+self.fScriptDir+("/checktcpport.sh %d %d" % (self.GetMasterListenSocket( config, mapper, node.fID),self.GetMasterSlaveControlSocket( config, mapper, node.fID)))+" ; then HADERROR=1 ; fi\n")
            if len(slaveNodeGroups)+len(slaveNodes)>0:
                outFile.OutputLine("if ! "+self.fScriptDir+"/checklogin.sh")
                for sng in slaveNodeGroups:
                    outFile.OutputLine(" "+shell+":"+config.GetNode( sng.GetGroupMasterNode() ).GetHostname())
                for sn in slaveNodes:
                    outFile.OutputLine(" "+shell+":"+sn.GetHostname())
                outFile.OutputLine(" ; then HADERROR=1 ; fi\n")
    
            for sng in slaveNodeGroups:
                remotecmd = shell+" "+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+" "+prestart+" "+self.fOutputDir+"/"+self.GetServantConfigName( config, sng )+postTerminate
                outFile.OutputLine("if "+self.fScriptDir+"/checklogin.sh "+shell+":"+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+" >/dev/null ; then "+remotecmd)
                if not simple_remote_return_value:
                    outFile.OutputLine(" \\; echo $? | python -c \"import os,sys\nlastline=\\\"\\\"\nline=sys.stdin.readline()\nwhile line != \\\"\\\":\n    if lastline != \\\"\\\":\n        print lastline,\n    lastline=line\n    line=sys.stdin.readline()\nif len(lastline)>0 and lastline[-1]==\\\"\\\\n\\\":\n    tmplastline=lastline[:-1]\nelse:\n    tmplastline=lastline\nretval=0\ntry:\n    retval=int(tmplastline)\nexcept:\n    print lastline,\nsys.exit( retval )\n\"")
                outFile.OutputLine(" ; if [[ ! $? ]] ; then HADERROR=1 ; fi")
                outFile.OutputLine(" ; fi\n")
            for sn in slaveNodes:
                remotecmd = shell+" "+sn.GetHostname()+" "+prestart+" "+self.fOutputDir+"/"+self.GetSlaveConfigName( config, sn )+postTerminate
                outFile.OutputLine("if "+self.fScriptDir+"/checklogin.sh "+shell+":"+sn.GetHostname()+" >/dev/null ; then "+remotecmd)
                if not simple_remote_return_value:
                    outFile.OutputLine(" \\; echo $? | python -c \"import os,sys\nlastline=\\\"\\\"\nline=sys.stdin.readline()\nwhile line != \\\"\\\":\n    if lastline != \\\"\\\":\n        print lastline,\n    lastline=line\n    line=sys.stdin.readline()\nif len(lastline)>0 and lastline[-1]==\\\"\\\\n\\\":\n    tmplastline=lastline[:-1]\nelse:\n    tmplastline=lastline\nretval=0\ntry:\n    retval=int(tmplastline)\nexcept:\n    print lastline,\nsys.exit( retval )\n\"")
                outFile.OutputLine(" ; if [[ ! $? ]] ; then HADERROR=1 ; fi")
                outFile.OutputLine(" ; fi\n")
    
                #outFile.OutputLine("if "+self.fScriptDir+"/checklogin.sh "+shell+":"+sn.GetHostname()+" >/dev/null ; then "+shell+" "+sn.GetHostname()+" "+prestart+" "+self.fOutputDir+"/"+self.GetSlaveConfigName( config, sn )+postTerminate+" ; fi\n")
    
            outFile.OutputLine("exit $HADERROR\n")
            
            outFile.FinishOutput()
            os.chmod(outFileName, 0755)
    

class TaskManagerStartupScriptOutputter(TaskManagerOutputter):
    def __init__( self, masterNode, useSSH, includePaths, taskManDir, frameworkDir, platform, runDir, outputDir, prestartExec, postTerminateExec ):
        TaskManagerOutputter.__init__( self, masterNode, useSSH, includePaths, taskManDir, frameworkDir, platform, runDir, outputDir, prestartExec, postTerminateExec, None, [] )

    def GenerateStartupScripts( self, config, mapper, procList ):
        self.GenerateStartupScript( config, mapper, procList, self.fMasterNodes[0], "" )
        if len(self.fMasterNodes)>1:
            for masterNode in self.fMasterNodes:
                self.GenerateStartupScript( config, mapper, procList, masterNode, "-"+masterNode )
            
    def GenerateStartupScript( self, config, mapper, procList, masterNode, fileNameSuffix ):
        slaveNodes = []
        slaveNodeGroups = []
        for node in config.GetNodes():
            if (self.fEmptyNodes or self.NodeHasProcesses( config, procList, node )) and config.GetNodeParent( node.fID )==None:
                slaveNodes.append( node )
        for ng in config.GetNodeGroups():
            if config.GetNodeGroupParent( ng.fID )==None:
                slaveNodeGroups.append( ng )
        node = config.GetNode( masterNode )
        platform = self.GetNodePlatform( node )
        outFileText = ""
        outFileLines = []
        outFileName = config.GetName()+fileNameSuffix+".sh"
        try:
            outFile = FileOutputWriter(outFileName)
        except IOError:
            raise ChainConfigException( "Unable to open output file'"+outFileName+"' for node '"+node+"' TaskManager master startup script." )
        if self.fUseSSH:
            shell = "ssh"
        else:
            shell = "rsh"
        if self.fPrestartExec=="" or self.fPrestartExec==None:
            prestart = ""
        else:
            prestart = self.fPrestartExec+" ; "
        if self.fPostTerminateExec=="" or self.fPostTerminateExec==None:
            postTerminate = ""
        else:
            postTerminate = " ; "+self.fPostTerminateExec
        
        outFile.OutputLine("#!/bin/bash\n")
        outFile.OutputLine("if [[ `hostname -s` != \""+string.split( node.GetHostname(), "." )[0]+"\" ]] ; then echo This script must be started on host '"+node.GetHostname()+"' ; exit 1 ; fi\n")
        outFile.OutputLine("if ! "+self.fOutputDir+"/"+config.GetName()+"-"+node.fID+"-"+node.GetHostname()+"-Master-Check.sh ; then echo There have been errors ; echo Aborting TaskManager execution ; exit 1 ; fi\n")
        outFile.OutputLine("echo Starting Master TaskManager\n")
        outFile.OutputLine(self.fTaskManDir+"/bin/"+platform+"/TaskManager "+self.fOutputDir+"/"+config.GetName()+"-"+node.fID+"-"+node.GetHostname()+"-Master.xml -V "+config.GetVerbosity()+" -nostdout")
        if mapper.fSysMESLogging:
            outFile.OutputLine(" -sysmeslog")
        if mapper.fInfoLoggerLibrary!=None:
            outFile.OutputLine(" -infologger "+mapper.fInfoLoggerLibrary)
        outFile.OutputLine(" "+self.fTaskManagerOptions+postTerminate+"\n")

        outFile.FinishOutput()
        os.chmod(outFileName, 0755)
    




class TaskManagerKillScriptOutputter(TaskManagerOutputter):
    def __init__( self, masterNode, useSSH, includePaths, taskManDir, frameworkDir, platform, runDir, outputDir, prestartExec, postTerminateExec, scriptDir ):
        TaskManagerOutputter.__init__( self, masterNode, useSSH, includePaths, taskManDir, frameworkDir, platform, runDir, outputDir, prestartExec, postTerminateExec, scriptDir, [] )
        pass

    def GetSlaveConfigName( self, config, node ):
        return config.GetName()+"-"+node.fID+"-"+node.GetHostname()+"-Slave-Kill.sh"

    def GenerateTaskManagerSlaveFile( self, config, mapper, procList, node ):
        if not self.fEmptyNodes and not self.NodeHasProcesses( config, procList, node ):
            return
        processes = self.GetNodeProcesses( config, procList, node )
        outFileText = ""
        outFileLines = []
        outFileName = self.GetSlaveConfigName( config, node )
        try:
            outFile = FileOutputWriter(outFileName)
        except IOError:
            raise ChainConfigException( "Unable to open output file'"+outFileName+"' for node '"+node.fID+"' TaskManager slave kill script." )
        outFile.OutputLine("#!/bin/bash\n")
        outFile.OutputLine("echo Killing processes on slave node "+node.fID+"/"+node.GetHostname()+" \\(System hostname: `hostname -s`\\)\n")
        outFile.OutputLine("TMPIDs=`ps x | grep \"TaskManager "+self.fOutputDir+"/"+TaskManagerOutputter.GetSlaveConfigName( self, config, node )+"\"|grep -v grep |awk {'print $1'}`\n")
        outFile.OutputLine("\n")
        outFile.OutputLine("for pid in ${TMPIDs} ; do\n")
        outFile.OutputLine("    kill ${pid}\n")
        outFile.OutputLine("done\n")
        outFile.OutputLine("\n")
        #outFile.OutputLine("sleep 1\n")
        outFile.OutputLine("\n")
        outFile.OutputLine("for pid in ${TMPIDs} ; do\n")
        outFile.OutputLine("    kill -9 ${pid}\n")
        outFile.OutputLine("done\n\n")
        # procList = " "+"TaskManager"
        procList = " "
        for proc in processes:
            cmds=string.split( proc.GetCmd() )
            if len(cmds)>0:
                paths=cmds[0].split( "/" )
                if len(paths)>0:
                    if procList.count( paths[-1] )<=0:
                        procList = procList+" "+cmds[0]+" "+paths[-1]
        outFile.OutputLine("killall"+procList+"\n")
        #outFile.OutputLine("sleep 1\n")
        outFile.OutputLine("killall -9"+procList+"\n")
            
        outFile.FinishOutput()
        os.chmod(outFileName, 0755)
        


    def GetServantConfigName( self, config, nodeGroup ):
        node = config.GetNode( nodeGroup.GetGroupMasterNode() )
        return config.GetName()+"-"+node.fID+"-"+node.GetHostname()+"-Servant-"+nodeGroup.fID+"-Kill.sh"

    def GenerateTaskManagerServantFile( self, config, mapper, procList, nodeGroup ):
        slaveNodes = []
        slaveNodeGroups = []
        for node in config.GetNodes():
            if (self.fEmptyNodes or self.NodeHasProcesses( config, procList, node )) and config.GetNodeParent( node.fID )!=None and config.GetNodeParent( node.fID ).fID==nodeGroup.fID:
                slaveNodes.append( node )
        for ng in config.GetNodeGroups():
            if config.GetNodeGroupParent( ng.fID )!=None and config.GetNodeGroupParent( ng.fID ).fID==nodeGroup.fID:
                slaveNodeGroups.append( ng )
        node = config.GetNode( nodeGroup.GetGroupMasterNode() )
        outFileText = ""
        outFileLines = []
        outFileName = self.GetServantConfigName( config, nodeGroup )
        try:
            outFile = FileOutputWriter(outFileName)
        except IOError:
            raise ChainConfigException( "Unable to open output file'"+outFileName+"' for node '"+node+"' TaskManager servant "+nodeGroup.fID+" kill script." )
        if self.fUseSSH:
            shell = "ssh"
        else:
            shell = "rsh"
        simple_remote_return_value = self.fUseSSH
        #simple_remote_return_value = 0
        #print "Warning: Setting simple_remote_return_value to",simple_remote_return_value
        if self.fPrestartExec=="" or self.fPrestartExec==None:
            prestart = ""
        else:
            prestart = self.fPrestartExec+" \\;"
        if self.fPostTerminateExec=="" or self.fPostTerminateExec==None:
            postTerminate = ""
        else:
            postTerminate = " \\; "+self.fPostTerminateExec
        outFile.OutputLine("#!/bin/bash\n")
        outFile.OutputLine("echo Killing processes on servant node "+node.fID+"/"+node.GetHostname()+" \\(System hostname: `hostname -s`\\)\n")
        outFile.OutputLine("#killall TaskManager\n")
        outFile.OutputLine("#sleep 1\n")
        outFile.OutputLine("#killall -9 TaskManager\n")
        outFile.OutputLine("TMPIDs=`ps x | grep \"TaskManager "+self.fOutputDir+"/"+TaskManagerOutputter.GetServantConfigName( self, config, nodeGroup )+"\"|grep -v grep |awk {'print $1'}`\n")
        outFile.OutputLine("\n")
        outFile.OutputLine("for pid in ${TMPIDs} ; do\n")
        outFile.OutputLine("    kill ${pid}\n")
        outFile.OutputLine("done\n")
        outFile.OutputLine("\n")
        #outFile.OutputLine("sleep 1\n")
        outFile.OutputLine("\n")
        outFile.OutputLine("for pid in ${TMPIDs} ; do\n")
        outFile.OutputLine("    kill -9 ${pid}\n")
        outFile.OutputLine("done\n\n")
        outFile.OutputLine(self.fScriptDir+"/remote_starter.py -shell "+shell)
        for sng in slaveNodeGroups:
            outFile.OutputLine(" -nodecommand "+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+" \""+prestart+" "+self.fOutputDir+"/"+self.GetServantConfigName( config, sng )+postTerminate+"\"")
            #remotecmd = shell+" "+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+" "+prestart+" "+self.fOutputDir+"/"+self.GetServantConfigName( config, sng )+postTerminate
            #outFile.OutputLine("if "+self.fScriptDir+"/checklogin.sh "+shell+":"+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+" >/dev/null ; then "+remotecmd )#+" &")
            #outFile.OutputLine(" ; fi\n")
        for sn in slaveNodes:
            outFile.OutputLine(" -nodecommand "+sn.GetHostname()+" \""+prestart+" "+self.fOutputDir+"/"+self.GetSlaveConfigName( config, sn )+postTerminate+"\"")
            #remotecmd = shell+" "+sn.GetHostname()+" "+prestart+" "+self.fOutputDir+"/"+self.GetSlaveConfigName( config, sn )+postTerminate
            #outFile.OutputLine("if "+self.fScriptDir+"/checklogin.sh "+shell+":"+sn.GetHostname()+" >/dev/null ; then "+remotecmd )#+" &")
            #outFile.OutputLine(" ; fi\n")
        outFile.OutputLine("\n")
        
        outFile.FinishOutput()
        os.chmod(outFileName, 0755)



    def GenerateTaskManagerMasterFile( self, config, mapper, procList ):
        for masterNode in self.fMasterNodes:
            slaveNodes = []
            slaveNodeGroups = []
            for node in config.GetNodes():
                if (self.fEmptyNodes or self.NodeHasProcesses( config, procList, node )) and config.GetNodeParent( node.fID )==None:
                    slaveNodes.append( node )
            for ng in config.GetNodeGroups():
                if config.GetNodeGroupParent( ng.fID )==None:
                    slaveNodeGroups.append( ng )
            node = config.GetNode( masterNode )
            outFileText = ""
            outFileLines = []
            outFileName = config.GetName()+"-"+node.fID+"-"+node.GetHostname()+"-Master-Kill.sh"
            try:
                outFile = FileOutputWriter(outFileName)
            except IOError:
                raise ChainConfigException( "Unable to open output file'"+outFileName+"' for node '"+node+"' TaskManager master kill script." )
            if self.fUseSSH:
                shell = "ssh"
            else:
                shell = "rsh"
            simple_remote_return_value = self.fUseSSH
            #simple_remote_return_value = 0
            #print "Warning: Setting simple_remote_return_value to",simple_remote_return_value
            if self.fPrestartExec=="" or self.fPrestartExec==None:
                prestart = ""
            else:
                prestart = self.fPrestartExec+" \\;"
            if self.fPostTerminateExec=="" or self.fPostTerminateExec==None:
                postTerminate = ""
            else:
                postTerminate = " \\; "+self.fPostTerminateExec
            
            outFile.OutputLine("#!/bin/bash\n")
            outFile.OutputLine("echo Killing processes on master node "+node.fID+"/"+node.GetHostname()+" \\(System hostname: `hostname -s`\\)\n")
            outFile.OutputLine("#killall TaskManager\n")
            outFile.OutputLine("#sleep 1\n")
            outFile.OutputLine("#killall -9 TaskManager\n")
            outFile.OutputLine("TMPIDs=`ps x | grep \"TaskManager "+self.fOutputDir+"/"+config.GetName()+"-"+node.fID+"-"+node.GetHostname()+"-Master.xml\"|grep -v grep |awk {'print $1'}`\n")
            outFile.OutputLine("\n")
            outFile.OutputLine("for pid in ${TMPIDs} ; do\n")
            outFile.OutputLine("    kill ${pid}\n")
            outFile.OutputLine("done\n")
            outFile.OutputLine("\n")
            #outFile.OutputLine("sleep 1\n")
            outFile.OutputLine("\n")
            outFile.OutputLine("for pid in ${TMPIDs} ; do\n")
            outFile.OutputLine("    kill -9 ${pid}\n")
            outFile.OutputLine("done\n\n")
            outFile.OutputLine(self.fScriptDir+"/remote_starter.py -shell "+shell)
            for sng in slaveNodeGroups:
                outFile.OutputLine(" -nodecommand "+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+" \""+prestart+" "+self.fOutputDir+"/"+self.GetServantConfigName( config, sng )+postTerminate+"\"")
                #remotecmd = shell+" "+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+" "+prestart+" "+self.fOutputDir+"/"+self.GetServantConfigName( config, sng )+postTerminate
                #outFile.OutputLine("if "+self.fScriptDir+"/checklogin.sh "+shell+":"+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+" >/dev/null ; then "+remotecmd )#+" &")
                #outFile.OutputLine(" ; fi\n")
            for sn in slaveNodes:
                outFile.OutputLine(" -nodecommand "+sn.GetHostname()+" \""+prestart+" "+self.fOutputDir+"/"+self.GetSlaveConfigName( config, sn )+postTerminate+"\"")
                #remotecmd = shell+" "+sn.GetHostname()+" "+prestart+" "+self.fOutputDir+"/"+self.GetSlaveConfigName( config, sn )+postTerminate
                #outFile.OutputLine("if "+self.fScriptDir+"/checklogin.sh "+shell+":"+sn.GetHostname()+" >/dev/null ; then "+remotecmd )#+" &")
                #outFile.OutputLine(" ; fi\n")
            outFile.OutputLine("\n")
    
            outFile.FinishOutput()
            os.chmod(outFileName, 0755)
    


class TaskManagerCleanScriptOutputter(TaskManagerOutputter):
    def __init__( self, masterNode, useSSH, includePaths, taskManDir, frameworkDir, platform, runDir, outputDir, prestartExec, postTerminateExec, scriptdir ):
        TaskManagerOutputter.__init__( self, masterNode, useSSH, includePaths, taskManDir, frameworkDir, platform, runDir, outputDir, prestartExec, postTerminateExec, scriptdir, [] )
        pass

    def GetSlaveConfigName( self, config, node ):
        return config.GetName()+"-"+node.fID+"-"+node.GetHostname()+"-Slave-Cleanup.sh"

    def GenerateTaskManagerSlaveFile( self, config, mapper, procList, node ):
        if not self.fEmptyNodes and not self.NodeHasProcesses( config, procList, node ):
            return
        processes = self.GetNodeProcesses( config, procList, node )
        outFileText = ""
        outFileLines = []
        outFileName = self.GetSlaveConfigName( config, node )
        try:
            outFile = FileOutputWriter(outFileName)
        except IOError:
            raise ChainConfigException( "Unable to open output file'"+outFileName+"' for node '"+node.fID+"' TaskManager slave cleanup script." )
        outFile.OutputLine("#!/bin/bash\n")
        outFile.OutputLine("echo Cleaning up run directory on slave node "+node.fID+"/"+node.GetHostname()+" \\(System hostname: `hostname -s`\\)\n")
        if self.fRunDir == None:
            outFile.OutputLine("echo No rundir set. Aborting cleanup\n")
        else:
            outFile.OutputLine("rm -rf "+self.fRunDir+"/*.log\n")
            
        outFile.FinishOutput()
        os.chmod(outFileName, 0755)
        


    def GetServantConfigName( self, config, nodeGroup ):
        node = config.GetNode( nodeGroup.GetGroupMasterNode() )
        return config.GetName()+"-"+node.fID+"-"+node.GetHostname()+"-Servant-"+nodeGroup.fID+"-Cleanup.sh"

    def GenerateTaskManagerServantFile( self, config, mapper, procList, nodeGroup ):
        slaveNodes = []
        slaveNodeGroups = []
        for node in config.GetNodes():
            if (self.fEmptyNodes or self.NodeHasProcesses( config, procList, node )) and config.GetNodeParent( node.fID )!=None and config.GetNodeParent( node.fID ).fID==nodeGroup.fID:
                slaveNodes.append( node )
        for ng in config.GetNodeGroups():
            if config.GetNodeGroupParent( ng.fID )!=None and config.GetNodeGroupParent( ng.fID ).fID==nodeGroup.fID:
                slaveNodeGroups.append( ng )
        node = config.GetNode( nodeGroup.GetGroupMasterNode() )
        outFileText = ""
        outFileLines = []
        outFileName = self.GetServantConfigName( config, nodeGroup )
        try:
            outFile = FileOutputWriter(outFileName)
        except IOError:
            raise ChainConfigException( "Unable to open output file'"+outFileName+"' for node '"+node+"' TaskManager servant "+nodeGroup.fID+" cleanup script." )
        if self.fUseSSH:
            shell = "ssh"
        else:
            shell = "rsh"
        simple_remote_return_value = self.fUseSSH
        #simple_remote_return_value = 0
        #print "Warning: Setting simple_remote_return_value to",simple_remote_return_value
        if self.fPrestartExec=="" or self.fPrestartExec==None:
            prestart = ""
        else:
            prestart = self.fPrestartExec+" \\;"
        if self.fPostTerminateExec=="" or self.fPostTerminateExec==None:
            postTerminate = ""
        else:
            postTerminate = " \\; "+self.fPostTerminateExec
        outFile.OutputLine("#!/bin/bash\n")
        outFile.OutputLine("echo Cleaning up run directory on servant node "+node.fID+"/"+node.GetHostname()+" \\(System hostname: `hostname -s`\\)\n")
        if self.fRunDir == None:
            outFile.OutputLine("echo No rundir set. Aborting cleanup\n")
        else:
            outFile.OutputLine("rm -rf "+self.fRunDir+"/*\n")
        for sng in slaveNodeGroups:
            remotecmd = shell+" "+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+" "+prestart+" "+self.fOutputDir+"/"+self.GetServantConfigName( config, sng )+postTerminate
            outFile.OutputLine("if "+self.fScriptDir+"/checklogin.sh "+shell+":"+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+" >/dev/null ; then "+remotecmd)
            outFile.OutputLine(" ; fi\n")
        for sn in slaveNodes:
            remotecmd = shell+" "+sn.GetHostname()+" "+prestart+" "+self.fOutputDir+"/"+self.GetSlaveConfigName( config, sn )+postTerminate
            outFile.OutputLine("if "+self.fScriptDir+"/checklogin.sh "+shell+":"+sn.GetHostname()+" >/dev/null ; then "+remotecmd)
            outFile.OutputLine(" ; fi\n")

        outFile.FinishOutput()
        os.chmod(outFileName, 0755)



    def GenerateTaskManagerMasterFile( self, config, mapper, procList ):
        for masterNode in self.fMasterNodes:
            slaveNodes = []
            slaveNodeGroups = []
            for node in config.GetNodes():
                if (self.fEmptyNodes or self.NodeHasProcesses( config, procList, node )) and config.GetNodeParent( node.fID )==None:
                    slaveNodes.append( node )
            for ng in config.GetNodeGroups():
                if config.GetNodeGroupParent( ng.fID )==None:
                    slaveNodeGroups.append( ng )
            node = config.GetNode( masterNode )
            outFileText = ""
            outFileLines = []
            outFileName = config.GetName()+"-"+node.fID+"-"+node.GetHostname()+"-Master-Cleanup.sh"
            try:
                outFile = FileOutputWriter(outFileName)
            except IOError:
                raise ChainConfigException( "Unable to open output file'"+outFileName+"' for node '"+node+"' TaskManager master cleanup script." )
            if self.fUseSSH:
                shell = "ssh"
            else:
                shell = "rsh"
            simple_remote_return_value = self.fUseSSH
            #simple_remote_return_value = 0
            #print "Warning: Setting simple_remote_return_value to",simple_remote_return_value
            if self.fPrestartExec=="" or self.fPrestartExec==None:
                prestart = ""
            else:
                prestart = self.fPrestartExec+" \\;"
            if self.fPostTerminateExec=="" or self.fPostTerminateExec==None:
                postTerminate = ""
            else:
                postTerminate = " \\; "+self.fPostTerminateExec
            
            outFile.OutputLine("#!/bin/bash\n")
            outFile.OutputLine("echo Cleaning up on master node "+node.fID+"/"+node.GetHostname()+" \\(System hostname: `hostname -s`\\)\n")
            outFile.OutputLine("rm -rf ./TaskManager-"+config.GetName()+"-"+node.fID+"-"+node.GetHostname()+"*.log\n")
            for sng in slaveNodeGroups:
                remotecmd = shell+" "+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+" "+prestart+" "+self.fOutputDir+"/"+self.GetServantConfigName( config, sng )+postTerminate
                outFile.OutputLine("if "+self.fScriptDir+"/checklogin.sh "+shell+":"+config.GetNode( sng.GetGroupMasterNode() ).GetHostname()+" >/dev/null ; then "+remotecmd)
                outFile.OutputLine(" ; fi\n")
            for sn in slaveNodes:
                remotecmd = shell+" "+sn.GetHostname()+" "+prestart+" "+self.fOutputDir+"/"+self.GetSlaveConfigName( config, sn )+postTerminate
                outFile.OutputLine("if "+self.fScriptDir+"/checklogin.sh "+shell+":"+sn.GetHostname()+" >/dev/null ; then "+remotecmd)
                outFile.OutputLine(" ; fi\n")
    
            outFile.FinishOutput()
            os.chmod(outFileName, 0755)
    
class SCC1Outputter(object):
    def __init__( self ):
        pass
    
    def GenerateSCC1File( self, config ):
        configID = config.GetName()
        outFileName = configID+"-SCC1-Generate.xml"
        try:
            outFile = open( outFileName, "w" )
        except IOError:
            raise ChainConfigException( "Unable to open output file'"+outFileName+"' for SimpleChainConfig1 XML output." )
        outFile.write( "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n" )
        outFile.write( "<SimpleChainConfig1 ID=\""+configID+"\" verbosity=\"0x39\">\n\n" )

        outFile.write( "\n<!-- ==================================== -->\n" )
        outFile.write( "\n<!-- Nodes -->\n" )
        nodes = config.GetNodes()
        nodes.sort( key=lambda x: x.fID )
        for node in nodes:
            outFile.write( "<Node ID=\""+node.fID+"\" hostname=\""+node.GetHostname()+"\" platform=\""+node.GetPlatform()+"\"/>\n" )

        outFile.write( "\n<!-- ==================================== -->\n" )
        outFile.write( "\n\n<!-- NodeGroups -->\n" )

        ngs = config.GetNodeGroups()
        ngs.sort( key=lambda x: x.fID )
        for ng in ngs:
            outFile.write( "<NodeGroup ID=\""+ng.fID+"\" Master=\""+ng.GetGroupMasterNode()+"\">\n" )
            for gm in ng.GetMembers():
                outFile.write( "  <Member>"+gm+"</Member>\n" )
            outFile.write( "</NodeGroup>\n\n" )

        outFile.write( "\n<!-- ==================================== -->\n" )
        outFile.write( "\n\n<!-- Processes -->\n" )
        procs = config.GetProcesses()
        procs.sort( key=lambda x: x.fID )
        for proc in procs:
            if proc.IsInProcessGroup():
                continue
            self.WriteProcess( config, outFile, proc, "", None )
            ### if proc.GetDelay()!=config.GetDefaultDelay() and proc.GetDelay()!=0:
            ###     delay = " delay=\"%u\"" % proc.GetDelay()
            ### else:
            ###     delay=""
            ### outFile.write( "<Proc ID=\""+proc.fID+"\" type=\""+proc.GetType()+"\""+delay+">\n" )
            ### parents = proc.GetParents()
            ### nodes = proc.GetNodes()
            ### cmd = proc.GetCmd()
            ### cmd = cmd.replace( "&", "&amp;" )
            ### cmd = cmd.replace( "<", "&lt;" )
            ### cmd = cmd.replace( ">", "&gt;" )
            ### outFile.write( "  <Cmd>"+cmd+"</Cmd>\n" )
            ### for instance in range(len(nodes)):
            ###     if proc.GetCmd(instance)!=cmd:
            ###         outFile.write( ("  <Cmd instance=\"%d\">"+proc.GetCmd(instance)+"</Cmd>\n") % instance )
            ### for node in nodes:
            ###     outFile.write( "  <Node>"+node+"</Node>\n" )
            ### for parent in parents:
            ###     if proc.IsParentFiltered(parent):
            ###         filterStr = " filtered=\""+proc.GetParentFilterType(parent)+"\""
            ###     else:
            ###         filterStr = ""
            ###     outFile.write( "  <Parent"+filterStr+">"+parent+"</Parent>\n" )
            ### outFile.write( "  <Shm blocksize=\"%u\" blockcount=\"%u\" type=\"%s\"/>\n" % (proc.GetBlockSize(), proc.GetBlockCount(), proc.GetShmType()) )
            ### # XXX TODO Also print event modulo and event type code
            ### outFile.write( "</Proc>\n\n" )
        groupIDs = config.GetProcessGroupIDList()
        for groupID in groupIDs:
            group = config.GetProcessGroup(groupID)
            prefix=group.GetProcessPrefix( 0 )
            allGroupProcs = [ p for p in config.GetProcesses() if p.IsInProcessGroup(groupID) ]
            # for proc in allGroupProcs:
            #     outFile.write( "  <!-- "+proc.fID+"\n" )
            #     for node in proc.GetNodes():
            #         outFile.write( "    "+node+"\n" )
            #     outFile.write( "  -->\n" )
            groupProcs = [ p for p in config.GetProcesses() if (p.IsInProcessGroup(groupID) and p.fID[:len(prefix)]==prefix) ]
            outFile.write( "<ProcGroup ID=\""+group.fID+"\">\n" )
            outProcs = group.GetOutputProcesses()
            for proc in groupProcs:
                self.WriteProcess( config, outFile, proc, "    ", group )
            for node in group.GetNodes():
                outFile.write( "  <Node>"+node+"</Node>\n" )
            if len(outProcs)>0:
                for op in outProcs:
                    outFile.write( "  <OutputProcess>"+op+"</OutputProcess>\n" )
            else:
                outFile.write( "  <NoOutputProcesses/>\n" )
            outFile.write( "</ProcGroup>\n\n" )

        
        outFile.write( "</SimpleChainConfig1>\n" )

    def WriteProcess( self, config, outFile, proc, indentStr, group ):
        if proc.GetDelay()!=config.GetDefaultDelay() and proc.GetDelay()!=0:
            delay = " delay=\"%u\"" % proc.GetDelay()
        else:
            delay=""
        if group!=None:
            prefix=group.GetProcessPrefix( 0 )
            procID = proc.fID[len(prefix):]
        else:
            procID = proc.fID
        outFile.write( indentStr+"<Proc ID=\""+procID+"\" type=\""+proc.GetType()+"\""+delay+">\n" )
        outFile.write( indentStr+"   <!-- "+proc.fID+" -->\n" )
        parents = proc.GetParents()
        nodes = proc.GetNodes()
        cmd = proc.GetCmd()
        cmd = cmd.replace( "&", "&amp;" )
        cmd = cmd.replace( "<", "&lt;" )
        cmd = cmd.replace( ">", "&gt;" )
        outFile.write( indentStr+"  <Cmd>"+cmd+"</Cmd>\n" )
        for instance in range(len(nodes)):
            if proc.GetCmd(instance)!=cmd:
                outFile.write( (indentStr+"  <Cmd instance=\"%d\">"+proc.GetCmd(instance)+"</Cmd>\n") % instance )
        if group==None:
            for node in nodes:
                outFile.write( indentStr+"  <Node>"+node+"</Node>\n" )
        else:
            outFile.write( indentStr+"  <Multiplicity>%u</Multiplicty>\n" % len(nodes) )
        for parent in parents:
            if proc.IsParentFiltered(parent):
                filterStr = " filtered=\""+proc.GetParentFilterType(parent)+"\""
            else:
                filterStr = ""
            if group!=None and not config.IsProcessGroup(parent) and config.GetProcess(parent).IsInProcessGroup( group.GetID() ):
                parentID = parent[len(prefix):]
            else:
                parentID = parent
            outFile.write( indentStr+"  <Parent"+filterStr+">"+parentID+"</Parent>\n" )
        outFile.write( indentStr+"  <Shm blocksize=\"%u\" blockcount=\"%u\" type=\"%s\"/>\n" % (proc.GetBlockSize(), proc.GetBlockCount(), proc.GetShmType()) )
        # XXX TODO Also print event modulo and event type code
        outFile.write( indentStr+"</Proc>\n\n" )
        

        

class ListFileHandler:
    def OutputControlSockets( self, config, mapper ):
        ID=config.GetName()
        outFileName=ID+"-Config-ProcessControlPort.list"
        try:
            outFile = open( outFileName, "w" )
        except IOError:
            raise ChainConfigException( "Unable to open output file'"+outFileName+"' for control process socket list." )

        socket_list = mapper.GetControlProcessSocketList()
        for k in socket_list.keys():
            outFile.write( k+":%d:%d" % (socket_list[k][0],socket_list[k][1])+"\n" )
        outFile.close()

    def OutputShmIDs( self, config, mapper ):
        ID=config.GetName()
        outFileName=ID+"-Config-Shms.list"
        try:
            outFile = open( outFileName, "w" )
        except IOError:
            raise ChainConfigException( "Unable to open output file'"+outFileName+"' for shared memory ID list." )

        shm_list = mapper.GetShmIDs()
        for k in shm_list.keys():
            outFile.write( k+":%d" % shm_list[k]+"\n" )
        outFile.close()

    def InputControlSockets( self, config, prefix="", quiet=False ):
        socket_list = {}
        ID=config.GetName()
        inFileName=prefix+ID+"-Config-ProcessControlPort.list"
        try:
            inFile = open( inFileName, "r" )
        except IOError:
            # raise ChainConfigException( "Unable to open input file'"+inFileName+"' for control process socket list." )
            if not quiet:
                print( "Warning: Unable to open input file'"+inFileName+"' for control process socket list - Assuming empty list." )
            return socket_list
        line=inFile.readline()
        lineNr=1
        while line!="":
            #print "Line: "+line,
            tokens = string.split( line, ":" )
            try:
                socket_list[tokens[0]] = [ int(tokens[1]), int(tokens[2]) ]
            except IndexError:
                raise ChainConfigException( ("Error parsing line %d in input file'" % lineNr)+inFileName+"' for control process socket list." )
            except ValueError:
                raise ChainConfigException( ("Error parsing port number in line %d in input file'" % lineNr)+inFileName+"' for control process socket list." )
            line=inFile.readline()
            lineNr+=1
        inFile.close()
        return socket_list

    def InputShmIDs( self, config, prefix="", quiet=False ):
        shm_list = {}
        ID=config.GetName()
        inFileName=prefix+ID+"-Config-Shms.list"
        try:
            inFile = open( inFileName, "r" )
        except IOError:
            #raise ChainConfigException( "Unable to open input file'"+inFileName+"' for shared memory ID list." )
            if not quiet:
                print( "Warning: Unable to open input file'"+inFileName+"' for shared memory ID list - Assuming empty list." )
            return shm_list
        line=inFile.readline()
        lineNr=1
        while line!="":
            #print "Line: "+line,
            tokens = string.split( line, ":" )
            try:
                shm_list[tokens[0]] = int(tokens[1])
            except IndexError:
                raise ChainConfigException( ("Error parsing line %d in input file'" % lineNr)+inFileName+"' for shared memory ID list." )
            except ValueError:
                raise ChainConfigException( ("Error parsing port number in line %d in input file'" % lineNr)+inFileName+"' for shared memory ID list." )
            line=inFile.readline()
            lineNr+=1
        inFile.close()
        return shm_list


def OutputGraphviz( config, mapper, processes ):
    ID=config.GetName()
    outFileName=ID+"-Processes-Graph.dot"
    try:
        outFile = open( outFileName, "w" )
    except IOError:
        raise ChainConfigException( "Unable to open output file'"+outFileName+"' for graphViz process graph." )
    outFile.write( "digraph G {\n" )
    outFile.write( "node [shape=box,height=0]\n" )
    cons = {}
    for proc in processes:
        if not cons.has_key( proc.fID ):
            cons[proc.fID] = {}
        children = mapper.GetChildren( processes, proc )
        for child in children:
            if not cons[proc.fID].has_key( child.fID ):
                cons[proc.fID][child.fID] = 0
            attrs = mapper.GetSubscriptionAttributes( proc.fID, child.fID, cons[proc.fID][child.fID] )
            attrStr=""
            for attr in attrs:
                attrStr = attrStr + ", " + attr[0]+":"+attr[1]
            if len(attrStr)>0:
                attrStr = "[ label=\""+attrStr[2:]+"\" ]"
            outFile.write( "\""+proc.fID+"\" -> \""+child.fID+"\""+attrStr+"\n" )
            cons[proc.fID][child.fID] = cons[proc.fID][child.fID] + 1
        if len(children)<=0:
            outFile.write( "\""+proc.fID+"\"\n" )
        #publishers = mapper.ProcID2PublisherList(proc.fID):
        #    outFile.write( "\""+proc.fID+"\" -> \""+child.fID+"\"\n" )
        #for publisher in publishers:
        sbh = False
        # for tok in proc.fID.split("-"):
        #     if tok=="SBH":
        #         sbh = True
        #         break
        if proc.GetType()=="sbh":
            sbh = True
        if sbh:
            pbh = mapper.GetBridgeReceiverPartner(proc)
            outFile.write( "\""+proc.fID+"\" -> \""+pbh.fID+"\"\n" )
    outFile.write( "}\n" )
    outFile.close()



def OutputProcessData( config, mapper, processes, settings ):
    # pickle.dump(x, f)
    ID=config.GetName()
    outFileName=ID+"-ProcessList.pickle"
    try:
        outFile = open( outFileName, "w" )
    except IOError:
        raise ChainConfigException( "Unable to open output file'"+outFileName+"' for saving process list." )
    import pickle
    pickle.dump( { "config" : config, "mapper" : mapper, "processes" : processes, "settings" : settings }, outFile )
    outFile.close()


def InputProcessData( inFileName ):
    # pickle.dump(x, f)
    try:
        inFile = open( inFileName, "r" )
    except IOError:
        raise ChainConfigException( "Unable to open input file'"+inFileName+"' for reading process list." )
    import pickle
    data = pickle.load( inFile )
    inFile.close()
    return data
    
