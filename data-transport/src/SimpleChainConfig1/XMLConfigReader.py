#!/usr/bin/env pyhton

# This is a test implementation of a new XMLConfigReader
# In addition to be used as a class it can be run standalone for testing:
# python XMLConfigReader.py /path/to/config.xml

# Heavily documented as it is also intended to serve as an example of 
# how libxml2 can be used from python.

# Method names are copied from the original XMLConfigReader

import sys
import libxml2

from SimpleChainConfig1 import *

class XMLConfigReader:

	def __init__(self, filename):

		self.fFilePublisherNames= ["ControlledFilePublisher"]

		# Parse the xml file and initialize xpath context for later xpathEval calls
		self.xmlFile = libxml2.parseFile(filename)
		self.xpathContext = self.xmlFile.xpathNewContext()


	def ReadSimpleChainConfig(self):

		# Get the attributes of an xml node
		name = self.xpathContext.xpathEval("/SimpleChainConfig1/@ID")[0]
		verbosity = self.xpathContext.xpathEval("/SimpleChainConfig1/@verbosity")[0]

		# Use nodeName.content to get the content of the node
		self.cfg = SimpleChainConfig(name.content)

		if verbosity:
			self.cfg.SetVerbosity(verbosity.content)

		self.ReadNodes()
		self.ReadNodeGroups()
		self.ReadProcesses()
		self.ReadProcessGroups()

		# self.cfg.ResolveProcessGroupParents()
		# print "Re-enable self.cfg.Check() and self.cfg.MakeGroupLevels()"
		self.cfg.Check()
		self.cfg.MakeGroupLevels()

		return self.cfg


	# Note that ID, hostname and platform will get the value None 
	# if the attributes can not be found in the xml node
	# Maybe do something like: if not None: generateThisConfElem
	def ReadNodes(self):

		nodes = self.xpathContext.xpathEval("/SimpleChainConfig1/Node")

		# nodes can be iterated as most other python objects
		for node in nodes:
			# You can use prop("attributeName") to get attributes when you 
			# have a reference to the node.
			ID = node.prop("ID")
			hostname = node.prop("hostname")
			platform = node.prop("platform")

			self.cfg.AddNode(Node(ID, hostname, platform))

	def ReadNodeGroups(self):
		nodeGroups = self.xpathContext.xpathEval("/SimpleChainConfig1/NodeGroup")

		for group in nodeGroups:
			ID = group.prop("ID")
			Master = group.prop("Master")

			nodeGroup = NodeGroup(ID, Master)

			# for memeber in xpathEval("./Member"):
			#	nodeGroup.Add(member.content)
			for child in group.children:
				if child.name == "Member":
					nodeGroup.Add(child.content)

			self.cfg.AddNodeGroup(nodeGroup)

	def ReadProcesses(self):
		processes = self.xpathContext.xpathEval("/SimpleChainConfig1/Proc")

		for procNode in processes:
			procObj = self.ReadProcess(procNode )
			self.cfg.AddProcess(procObj)

	def ReadProcessGroups( self ):
		processGroups = self.xpathContext.xpathEval("/SimpleChainConfig1/ProcGroup")
		
		for groupNode in processGroups:
			# ID, outputProcessList, parentList, nodeList, filteredParents, eventModulo, eventTypeCode
			ID = groupNode.prop("ID")

			outputProcs = []
			noOPNodes = groupNode.xpathEval("./NoOutputProcesses")
			if len(noOPNodes)<=0:
				opNodes = groupNode.xpathEval("./OutputProcess")
			
				if len(opNodes) < 1:
					raise ChainConfigException("There needs to be at least one output process for each process group (process group "+ID+") if NoOutputProcesses tag is not specified.")
				else:
					for opNode in opNodes:
						outputProcs.append(opNode.content)
			else:
				outputProcs = []


			nodes = []

			nodeNodes = groupNode.xpathEval("./Node")
			
			if len(nodeNodes) < 1:
				raise ChainConfigException("There needs to be at least one node for each process group (process group "+ID+").")
			else:
				for nNode in nodeNodes:
					nodes.append(nNode.content)
				# if len(set(nodes))!=len(nodes): # At least one duplicate entry...
				# 	for n in set(nodes):
				# 		if nodes.count(n)>1:
				# 			raise ChainConfigException("Duplicate entry for node "+n+" in process group "+ID+".")


			processes = groupNode.xpathEval("./Proc")

			groupProcIDs = [ procNode.prop("ID") for procNode in processes ]
			group = ProcessGroup( ID, nodes, groupProcIDs, outputProcs )
			for ID in outputProcs:
				if not ID in groupProcIDs:
					raise ChainConfigException("Process "+ID+" specified as output process for group "+ID+" does not exist in group")
				
			# nodeDir = {}
			ndx = 0
			for node in nodes:
				# ndx = group.GetNodeIndex( nodeDir, node )
				for procNode in processes:
					procObj = self.ReadProcess(procNode,node,ndx, groupProcIDs,group)
					groupParents = set(groupProcIDs) & set(procObj.GetParents())
					nonGroupParents = set(procObj.GetParents()) - groupParents
					if len(groupParents)>0 and len(nonGroupParents)>0:
						raise ChainConfigException("Process "+ID+" of group "+ID+" has mixed non-group and in-group parents. This is currently not supported... (in-group: "+str(groupParents)+" - non-group: "+str(nonGroupParents)+")")
					self.cfg.AddProcess(procObj)
				ndx += 1

			self.cfg.AddProcessGroup(group)


	def ReadProcess( self,procNode, myNode=None, myNodeIndex=None, groupProcIDs=None, processGroup=None ):
		ID = procNode.prop("ID")
		if processGroup!=None:
			prefix = processGroup.GetProcessPrefix( myNode, myNodeIndex )
			ID = prefix+ID
		type = procNode.prop("type")
		delay = procNode.prop("delay")
		if delay == None:
			delay = self.cfg.GetDefaultDelay()
		singleton = procNode.prop( "singleton" )
		if singleton!=None and singleton!="true" and singleton!="false" and singleton!="1" and singleton!="0" and singleton!="yes" and singleton!="no":
			raise ChainConfigException("Invalid singleton attribute specifier (allowed: true, false, 1, 0, yes, no")
		if singleton=="true" or singleton=="yes" or singleton=="1":
			singleton = True
		else:
			singleton = False

		if type != "src" and type != "prc" and type != "snk":
			raise ChainConfigException("Invalid or process type specified. (src, prc, snk)")

		commands = {}
		# A list is always returned form a xpathEval call.
		# Therefore use [0] to get the first/only element in the list.
		# command = procNode.xpathEval("./Cmd")[0].content
		for cmdNode in procNode.xpathEval("./Cmd"):
			if len(cmdNode.xpathEval("./@instance"))>0:
				try:
					commands[int(cmdNode.xpathEval("./@instance")[0].content)] = cmdNode.content
				except ValueError:
					raise ChainConfigException( "Instance attribute '%s' for process '%s' is not a number" % (cmdNode.xpathEval("./@instance")[0].content,ID) )
			else:
				commands[-1] = cmdNode.content
			

		# Is the code that iterates through self.fFilePublisherNames and
		# the splittet command string really needed?

		parents = []
		filteredParents = {}

		if type != "src":
			parentNodes = procNode.xpathEval("./Parent")

			if len(parentNodes) < 1:
				raise ChainConfigException("Need to specify at least one parent for " + type + " type processes. (process "+ID+")")
			else:
				for pNode in parentNodes:
					parID = pNode.content
					if processGroup!=None:
						if parID in groupProcIDs:
							parID = prefix+parID
					parents.append(parID)
					if len(pNode.xpathEval( "./@filtered" ))>0:
						filtered = pNode.xpathEval( "./@filtered" )[0].content
						if filtered!="readout" and filtered!="monitor" and filtered!="none":
							raise ChainConfigException("filtered Parent node attribute must be either readout, monitor, or none (process "+ID+")")
						if filtered!="none":
							filteredParents[parID] = filtered
				parentSet = set(parents)
				if len(parentSet)!=len(parents):
					raise ChainConfigException("Each process is allowed to list a parent only once (process "+ID+")")

		nodes = []

		if processGroup==None:
			# Src can only have one node?
			nodeNodes = procNode.xpathEval("./Node")
			
			if singleton and len(nodeNodes)>1:
				raise ChainConfigException("Singleton processes can have only one node for each process (process "+ID+").")
			if len(nodeNodes) < 1:
				raise ChainConfigException("There needs to be at least one node for each process (process "+ID+").")
			elif type == "src" and len(nodeNodes) != 1:
				raise ChainConfigException("There can only be one node in a src process (process "+ID+").")
			else:
				for nNode in nodeNodes:
					nodes.append(nNode.content)
			multiplicity = None
		else:
			if type == "src":
				raise ChainConfigException("Process groups cannot contain src processes (group "+processGroup.GetID()+", process "+ID+").")
			multNodes = procNode.xpathEval("./Multiplicity")
			if len(multNodes)>1:
				raise ChainConfigException("Only one multiplicity specification allowed for each group process (group "+processGroup.GetID()+", process "+ID+").")
			if len(multNodes)==1:
				try:
					multiplicity = int(multNodes[0].content,0)
				except ValueError:
					raise ChainConfigException("Multiplicity specification must be a number (group "+processGroup.GetID()+", process "+ID+").")
			else:
				multiplicity = 1
			nodes = [ myNode ] * multiplicity

		for cmdInst in commands.keys():
			if cmdInst<-1 or cmdInst>=len(nodes):
				raise ChainConfigException("Instance %d given for command of process '%s' is not available" % (cmdInst,ID))

		modulo = None
		if len(procNode.xpathEval( "./EventModulo"))>0:
			moduloStr = procNode.xpathEval( "./EventModulo")[0].content
			try:
				modulo = int(moduloStr)
			except ValueError:
				raise ChainConfigException("Event modulo '%s' given for command of proces '%s' cannot be converted to an integer" % (moduloStr,ID))

		eventTypeCode = None
		if len(procNode.xpathEval( "./EventTypeCode"))>0:
			eventTypeCode = procNode.xpathEval( "./EventTypeCode")[0].content
			

#			if type != "snk":
		shmNode = procNode.xpathEval("./Shm")[0]
				
		shmType = shmNode.prop("type")
		blockSize = self.StringSize2Int(shmNode.prop("blocksize"))
		blockCount = self.StringSize2Int(shmNode.prop("blockcount"))

		procObj = Process(ID, type, commands, "", parents, nodes, shmType, blockSize, blockCount, int(delay), singleton, filteredParents, modulo, eventTypeCode, processGroup, myNode, myNodeIndex )
		return procObj



    # Taken from the original XMLConfigReader
	def StringSize2Int(self, sizeStr):
		if sizeStr[-1:] == "k":
			factor = 1024
			sizeStr = sizeStr[:-1]
		elif sizeStr[-1:] == "M":
			factor = 1024*1024
			sizeStr = sizeStr[:-1]
		elif sizeStr[-1:] == "G":
			factor = 1024*1024*1024
			sizeStr = sizeStr[:-1]
		else:
			factor = 1
		return int( sizeStr )*factor

# Code below here is for standalone run. This will print most of the information
# from a SimpleChainConfig to screen.
def main():
	configReader = XMLConfigReader2(sys.argv[1])
	config = configReader.ReadSimpleChainConfig()

	print config


if __name__ == "__main__":
	main()
