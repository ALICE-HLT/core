#!/usr/bin/env python

import os, sys, string

def GetNextID( dir, ID ):
    if not dir.has_key(ID):
        dir[ID] = 0
    else:
        dir[ID] += 1
    return ID + ( "-%03d" % dir[ID] )

def AddMap( map, k, v ):
    map[k] = v

def GetMapValue( map, k ):
    if map.has_key(k):
        return map[k]
    return None

if len(sys.argv)>1:
    infiles = [ open(a,"r") for a in sys.argv[1:] ]
else:
    infiles = [ sys.stdin ]


for infile in infiles:
    publishers = {}
    subscribers = {}
    publisherMap = {}
    lines=infile.readlines()
    infile.close ()
    for line in lines:
        # print "line: '"+line[:-1]+"'"
        # print "line: '"+line+"'"
        if line=="\n":
            continue
        toks = line.split()
        
        i=2
        params = {}
        while i<len(toks):
            if toks[i]=="-name":
                params["-name"] = [ toks[i+1] ]
                break
            i += 1
        i=2
        while i<len(toks):
            if toks[i]=="-publisher":
                newPubID = GetNextID( publishers, params["-name"][0]+"-Pub" )
                AddMap( publisherMap, toks[i+1], newPubID )
            i += 1
    node = "UNKNOWN"
    comps = {}
    for line in lines:
        # print "line: '"+line[:-1]+"'"
        # print "line: '"+line+"'"
        if line=="\n":
            continue
        toks = line.split()
        if line[:len(toks[0])]==toks[0] and toks[0][-1]==":":
            node = toks[0][:-1]

        
        i=2
        params = {}
        while i<len(toks):
            if toks[i]=="-name":
                params["-name"] = [ toks[i+1] ]
                break
            i += 1
        i=2
        while i<len(toks):
            if toks[i][0]=="-":
                k = toks[i]
                shm=False
                publisher=False
                subscriber=False
                if k=="-shm":
                    shm=True
                elif k=="-publisher":
                    publisher=True
                elif k=="-subscribe":
                    subscriber=True
                i += 1
                values = []
                n=0
                while i<len(toks) and toks[i][0]!="-":
                    if not toks[i] in ( "(", ")" ):
                        if toks[i].split(":")[0] in ( "tcpmsg", "tcpblob"):
                            values.append( ":".join( toks[i].split(":")[:-1]+["PORT"] ) )
                        elif shm and n==1:
                            values.append( "SHMID" )
                        elif publisher and n==0:
                            v = GetMapValue( publisherMap, toks[i] )
                            if v==None:
                                print "GetMapValue( publisherMap, ",toks[i]," ):",v
                            values.append( v )
                        elif subscriber and n==0:
                            v = GetMapValue( publisherMap, toks[i] )
                            if v==None:
                                print "GetMapValue( publisherMap, ",toks[i]," ):",v
                            values.append( v )
                        elif subscriber and n==1:
                            values.append( GetNextID( subscribers, params["-name"][0]+"-Sub" ) )
                        else:
                            values.append( toks[i] )
                    i += 1
                    n += 1
                params[k] = values
            else:
                i += 1
        if params.has_key("-name"):
            keys = params.keys()
            keys.sort()
            #print node,":",toks[0][:-1],":",params["-name"][0],":"," ".join( [toks[1]] + [ " ".join( [k]+params[k]  ) for k in keys ] )
            for k in keys:
                if None in params[k]:
                    print k," : ",params[k]
            # print "toks:",toks
            
            comps[ node+":"+toks[0][:-1]+":"+params["-name"][0] ] = node+" : "+toks[0][:-1]+" : "+params["-name"][0]+" : "+" ".join( [toks[1]] + [ " ".join( [k]+params[k]  ) for k in keys ] )
            
    
    keys = comps.keys()
    keys.sort()
    for k in keys:
        print comps[k]
        print
