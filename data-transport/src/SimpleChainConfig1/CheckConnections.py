#!/usr/bin/env python

##
## This file is property of and copyright by the Technical Computer
## Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
## University, Heidelberg, Germany, 2004
## This file has been written by Timm Morten Steinbeck, 
## timm@kip.uni-heidelberg.de
##
##
## See the file license.txt for details regarding usage, modification,
## distribution and warranty.
## Important: This file is provided without any warranty, including
## fitness for any particular purpose.
##
##
## Newer versions of this file's package will be made available from 
## http://www.ti.uni-hd.de/HLT/software/software.html 
## or the corresponding page of the Heidelberg Alice HLT group.
##

import os, sys, re

if len(sys.argv)<=1:
    files = [ sys.stdin ]
else:
    files = []
    for i in range( 1, len( sys.argv ) ):
        try:
            file = open( sys.argv[i], "r" )
        except IOError, (errno, strerror):
            print "Unable to open file %s" % ( sys.argv[i] )
            sys.exit( -1 )
        files.append( file )

nodeMatch = re.compile( "^(\S+):" )
publisherMatch = re.compile( "-publisher\s+(\S+)\s?(.*)$" )
subscriberMatch = re.compile( "-subscribe\s+(\S+)\s+(\S+)\s?(.*)$" )

publisherNodes = {}
subscribers = {}

for file in files:
    line = file.readline()
    node = ""
    while line != "":
        match = nodeMatch.search( line )
        if match != None:
            node = match.group( 1 )
            #print "Found node: "+node
        else:
            l = line
            while l != "":
                match = publisherMatch.search( l )
                if match != None:
                    pub = match.group( 1 )
                    #print "Found publisher : "+pub+" - node: "+node
                    if not publisherNodes.has_key( pub ):
                        publisherNodes[pub] = [ node ]
                    else:
                        publisherNodes[pub].append( node )
                    l = match.group( 2 )
                else:
                    l = ""
            l = line
            while l != "":
                match = subscriberMatch.search( l )
                if match != None:
                    subData = {}
                    subData["pub"] = match.group( 1 )
                    subData["node"] = node
                    sub = match.group( 2 )
                    #print "Found subscriber: "+sub+" - publisher : "+pub+" - node: "+node
                    if not subscribers.has_key( sub ):
                        subscribers[sub] = [ subData ]
                    else:
                        subscribers[sub].append( subData )
                    l = match.group( 3 )
                else:
                    l = ""
        line = file.readline()


for sub in subscribers.keys():
    for subData in subscribers[sub]:
        if not publisherNodes.has_key( subData["pub"] ):
            print "Publisher '"+subData["pub"]+"' for subscriber '"+sub+"' on node '"+subData["node"]+"' cannot be found."
            continue
        found=0
        for node in publisherNodes[subData["pub"]]:
            if node == subData["node"]:
                found=1
        if not found:
            print "No publisher '"+subData["pub"]+"' available on subscriber '"+sub+"' node '"+subData["node"]+"'."
            continue
        

