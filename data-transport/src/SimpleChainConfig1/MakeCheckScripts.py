#!/usr/bin/env python

##
## This file is property of and copyright by the Technical Computer
## Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
## University, Heidelberg, Germany, 2004
## This file has been written by Timm Morten Steinbeck, 
## timm@kip.uni-heidelberg.de
##
##
## See the file license.txt for details regarding usage, modification,
## distribution and warranty.
## Important: This file is provided without any warranty, including
## fitness for any particular purpose.
##
##
## Newer versions of this file's package will be made available from 
## http://www.ti.uni-hd.de/HLT/software/software.html 
## or the corresponding page of the Heidelberg Alice HLT group.
##


import SimpleChainConfig1, sys, os, os.path

#mport psyco
#syco.full()

#if len(sys.argv)<=1:
#    print "Usage: "+sys.argv[0]+" -masternode <master-node-hostname> (-usessh) -taskmandir <TaskManager-base-directory> -frameworkdir <AliHLT-framework-directory> -platform <platform> (-rundir <run-directory>) (-prestart-exec <slave-prestart-executable>) -config <config-filename>"
#    sys.exit( 1 )

base_socket = None

argList = sys.argv[1:]
configfiles = []
if os.environ.has_key( "TMCONFIG_MASTERNODE" ):
    masterNode = os.environ["TMCONFIG_MASTERNODE"]
else:
    masterNode = None
if os.environ.has_key( "TMCONFIG_USESSH" ):
    useSSH = os.environ["TMCONFIG_USESSH"]
else:
    useSSH = 0
if os.environ.has_key( "TMCONFIG_TASKMANDIR" ):
    taskManDir = os.environ["TMCONFIG_TASKMANDIR"]
else:
    taskManDir = None
if os.environ.has_key( "TMCONFIG_FRAMEWORKDIR" ):
    frameworkDir = os.environ["TMCONFIG_FRAMEWORKDIR"]
else:
    frameworkDir = None
if os.environ.has_key( "TMCONFIG_PLATFORM" ):
    platform = os.environ["TMCONFIG_PLATFORM"]
else:
    platform = os.uname()[0]+"-"+os.uname()[4]
if os.environ.has_key( "TMCONFIG_RUNDIR" ):
    runDir = os.environ["TMCONFIG_RUNDIR"]
else:
    runDir = "/tmp"
if os.environ.has_key( "TMCONFIG_PRESTARTEXEC" ):
    prestartExec = os.environ["TMCONFIG_PRESTARTEXEC"]
else:
    prestartExec = None
if os.environ.has_key( "TMCONFIG_BASESOCKET" ):
    base_socket = int( os.environ["TMCONFIG_BASESOCKET"] )
else:
    base_socket = None
    
usagestring = " -masternode <master-node-hostname> (-usessh) -taskmandir <TaskManager-base-directory> -frameworkdir <AliHLT-framework-directory> (-platform <platform>) (-rundir <run-directory>) (-prestartexec <slave-prestart-executable>) (-basesocket <base-socket>) -config <config-filename>"
    
try:
    while len(argList)>0:
        if argList[0] == "-masternode":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -masternode." )
            masterNode = argList[1]
            argList = argList[2:]
        elif argList[0] == "-usessh":
            useSSH = 1
            argList = argList[1:]
        elif argList[0] == "-taskmandir":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -taskmandir." )
            taskManDir = argList[1]
            argList = argList[2:]
        elif argList[0] == "-frameworkdir":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -frameworkdir." )
            frameworkDir = argList[1]
            argList = argList[2:]
        elif argList[0] == "-platform":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -platform." )
            platform = argList[1]
            argList = argList[2:]
        elif argList[0] == "-rundir":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -rundir." )
            runDir = argList[1]
            argList = argList[2:]
        elif argList[0] == "-prestartexec":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -prestart-exec." )
            prestartExec = argList[1]
            argList = argList[2:]
        elif argList[0] == "-basesocket":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -basesocket." )
            base_socket = int( argList[1] )
            argList = argList[2:]
        elif argList[0] == "-config":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -config." )
            configfiles.append( argList[1] )
            argList = argList[2:]
        else:
            raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nUnknown parameter '"+argList[0]+"'." )

    if len(configfiles)<=0:
        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nAt least one config file has to be specified." )
    if masterNode == None:
        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMaster node has to be specified." )
    if taskManDir == None:
        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nTaskManager base directory has to be specified." )
    if frameworkDir == None:
        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nAliHLT framework base directory has to be specified." )
    if platform == None:
        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nPlatform has to be specified." )
    
    outputDir = os.path.realpath( "." )
    #print "outputDir:",outputDir
    baseDir = os.path.dirname( os.path.realpath( sys.argv[0] ) )
    #print "baseDir:",baseDir
    includePaths = [ outputDir+"/templates", baseDir+"/templates" ]

# Things to check:
# Start of TaskManager on master
# passwordless login to each slave
# Start of TaskManager on each slave once logged in
# Start of each used component on each slave once logged in
# memory segments (already allocated or still free, if allocated large enough)
# TCP ports available
    for config in configfiles:
        configReader = SimpleChainConfig1.XMLConfigReader()
        config = configReader.ReadSimpleChainConfig( config, 1 )
        #config.Check()
        configMapper = SimpleChainConfig1.ChainConfigMapper( frameworkDir, platform )

        if base_socket!=None:
            configMapper.SetStartSocket( base_socket )
        master_listen_sockets = {}
        master_slave_control_sockets = {}
        slave_listen_sockets = {}
        slave_slave_control_sockets = {}
        for n in config.GetNodes():
            master_slave_control_sockets[n.GetID()] = configMapper.GetNextSocket( n.GetID() )
            master_listen_sockets[n.GetID()] = configMapper.GetNextSocket( n.GetID() )
            
        for ng in config.GetNodeGroups():
            ng.SetSlaveControlSocket( configMapper.GetNextSocket( ng.GetGroupMasterNode() ) )
            ng.SetListenSocket( configMapper.GetNextSocket( ng.GetGroupMasterNode() ) )

        for n in config.GetNodes():
            slave_slave_control_sockets[n.GetID()] = configMapper.GetNextSocket( n.GetID() )
            slave_listen_sockets[n.GetID()] = configMapper.GetNextSocket( n.GetID() )
            

        procList = configMapper.CreateProcessList( config )

        outputter = SimpleChainConfig1.TaskManagerCheckOutputter( masterNode, useSSH, includePaths, taskManDir, frameworkDir, platform, runDir, outputDir, prestartExec, baseDir+"/scripts" )
        outputter.SetMasterSlaveControlSockets( master_slave_control_sockets )
        outputter.SetMasterListenSockets( master_listen_sockets )
        outputter.SetSlaveSlaveControlSockets( slave_slave_control_sockets )
        outputter.SetSlaveListenSockets( slave_listen_sockets )
        outputter.GenerateTaskManagerFiles( config, configMapper, procList )

except SimpleChainConfig1.ChainConfigException, e:
    sys.stderr.write(  "Exception caught: "+e.fValue+"\n" )
    sys.exit( -1 )

