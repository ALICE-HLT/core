#!/usr/bin/env python

import sys, os, os.path, re, string

verbose=1
if os.path.basename( sys.argv[0] )=="GetTotalShmSizeBrief.py":
    verbose=0

if len(sys.argv)!=3:
    print "Usage: "+sys.argv[0]+" <TM slave config file> <shm type>"
    sys.exit(1)

memory=sys.argv[2]

bridgeheadpattern = re.compile( "PublisherBridgeHeadNew .* -shm "+memory+" \d+ .* -blob (\d+) " )

anapattern = re.compile( "-shm "+memory+" \d+ (\d+) " )

try:
    infile=open( sys.argv[1], "r" )
except IOError:
    print "Error opening infile "+sys.argv[1]
    sys.exit(1)

totalSize=0

line=infile.readline()
while line != "":
    match=bridgeheadpattern.search(line)
    if match != None:
        #if verbose:
        #    print "Found pbh: %d" % ( int(match.group(1)) )
        totalSize = totalSize + int(match.group(1))
    else:
        match = anapattern.search(line)
        if match != None:
            #if verbose:
            #    print "Found prc: %d" % ( int(match.group(1)) )
            totalSize = totalSize + int(match.group(1))
    line=infile.readline()
    
if verbose:
    print "Found total "+memory+" shared memory of %d (0x%X) B / %d (0x%X) kB / %d (0x%X) MB" % ( totalSize, totalSize, totalSize/1024, totalSize/1024, totalSize/(1024*1024), totalSize/(1024*1024) )
else:
    print totalSize




