#!/usr/bin/env python

##
## This file is property of and copyright by the Technical Computer
## Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
## University, Heidelberg, Germany, 2004
## This file has been written by Timm Morten Steinbeck, 
## timm@kip.uni-heidelberg.de
##
##
## See the file license.txt for details regarding usage, modification,
## distribution and warranty.
## Important: This file is provided without any warranty, including
## fitness for any particular purpose.
##
##
## Newer versions of this file's package will be made available from 
## http://www.ti.uni-hd.de/HLT/software/software.html 
## or the corresponding page of the Heidelberg Alice HLT group.
##


import SimpleChainConfig1, sys, os, os.path
import XMLConfigReader

import time
times = {}

#mport psyco
#syco.full()

#if len(sys.argv)<=1:
#    print "Usage: "+sys.argv[0]+" -masternode <master-node-hostname> (-usessh) -taskmandir <TaskManager-base-directory> -frameworkdir <AliHLT-framework-directory> -platform <platform> (-rundir <run-directory>) (-prestart-exec <slave-prestart-executable>) (-postterminateexec <slave-post-terminate-executable>) -config <config-filename>"
#    sys.exit( 1 )

times[-1] = time.time()

base_socket = None

empty_nodes = 0

reconfigure = 0

interrupt_targets = []

debug=False

argList = sys.argv[1:]
configfiles = []
lateCreationDataFile = None
lateCreationNode = None
lateCreationConfigFiles = []
masterNodes = []

if os.environ.has_key( "TMCONFIG_MASTERNODE" ):
    masterNodes.append( os.environ["TMCONFIG_MASTERNODE"] )
if os.environ.has_key( "TMCONFIG_MASTERNODES" ):
    masterNodes += os.environ["TMCONFIG_MASTERNODES"].split(",")
if os.environ.has_key( "TMCONFIG_USESSH" ):
    useSSH = os.environ["TMCONFIG_USESSH"]
else:
    useSSH = 0
if os.environ.has_key( "TMCONFIG_TASKMANDIR" ):
    taskManDir = os.environ["TMCONFIG_TASKMANDIR"]
else:
    taskManDir = None
if os.environ.has_key( "TMCONFIG_FRAMEWORKDIR" ):
    frameworkDir = os.environ["TMCONFIG_FRAMEWORKDIR"]
else:
    frameworkDir = None
if os.environ.has_key( "TMCONFIG_PLATFORM" ):
    platform = os.environ["TMCONFIG_PLATFORM"]
else:
    platform = os.uname()[0]+"-"+os.uname()[4]
if os.environ.has_key( "TMCONFIG_RUNDIR" ):
    runDir = os.environ["TMCONFIG_RUNDIR"]
else:
    runDir = "/tmp"
if os.environ.has_key( "TMCONFIG_PRESTARTEXEC" ):
    prestartExec = os.environ["TMCONFIG_PRESTARTEXEC"]
else:
    prestartExec = None
if os.environ.has_key( "TMCONFIG_POSTTERMINATEEXEC" ):
    postTerminateExec = os.environ["TMCONFIG_POSTTERMINATEEXEC"]
else:
    postTerminateExec = None
if os.environ.has_key( "TMCONFIG_BASESOCKET" ):
    base_socket = int( os.environ["TMCONFIG_BASESOCKET"] )
else:
    base_socket = None
if os.environ.has_key( "TMCONFIG_BASESHM" ):
    base_shm = int( os.environ["TMCONFIG_BASESHM"] )
else:
    base_shm = None
if os.environ.has_key( "TMCONFIG_EMPTYNODES" ):
    empty_nodes = int( os.environ["TMCONFIG_EMPTYNODES"] )
    
if os.environ.has_key( "TMCONFIG_EVENTMERGERTIMEOUT" ):
    event_merger_timeout = int( os.environ["TMCONFIG_EVENTMERGERTIMEOUT"] )
else:
    event_merger_timeout = None

if os.environ.has_key( "TMCONFIG_PRODUCTION" ):
    production = int( os.environ["TMCONFIG_PRODUCTION"] )
else:
    production = 0

if os.environ.has_key( "TMCONFIG_BRIDGESHMTYPE" ):
    bridgeShmType = os.environ["TMCONFIG_BRIDGESHMTYPE"]
else:
    bridgeShmType = None

if os.environ.has_key( "TMCONFIG_ALICEHLT" ):
    aliceHLT = os.environ["TMCONFIG_ALICEHLT"]
else:
    aliceHLT = None

if os.environ.has_key( "TMCONFIG_EVENTACCOUNTING" ):
    eventAccounting = os.environ["TMCONFIG_EVENTACCOUNTING"]
else:
    eventAccounting = None

if os.environ.has_key( "TMCONFIG_EVENTMERGERDYNAMICMAXTIMEOUT" ):
    event_merger_dynamic_max_timeout = os.environ["TMCONFIG_EVENTMERGERDYNAMICMAXTIMEOUT"]
else:
    event_merger_dynamic_max_timeout = None

if os.environ.has_key( "TMCONFIG_EVENTMERGERDYNAMICMinTIMEOUT" ):
    event_merger_dynamic_min_timeout = os.environ["TMCONFIG_EVENTMERGERDYNAMICMinTIMEOUT"]
else:
    event_merger_dynamic_min_timeout = None

if os.environ.has_key( "TMCONFIG_EVENTMERGERDYNAMICINPUTXABLING" ):
    event_merger_dynamic_input_Xabling = os.environ["TMCONFIG_EVENTMERGERDYNAMICINPUTXABLING"]
else:
    event_merger_dynamic_input_Xabling = None

if os.environ.has_key( "TMCONFIG_RUNSTOPTIMEOUT" ):
    runstop_timeout = int(os.environ["TMCONFIG_RUNSTOPTIMEOUT"])
else:
    runstop_timeout = None


if os.environ.has_key( "TMCONFIG_SCATTERERPARAM" ):
    scattererParam = int(os.environ["TMCONFIG_SCATTERERPARAM"])
else:
    scattererParam = None

if os.environ.has_key( "TMCONFIG_MODULOPREDIVISOR" ):
    eventModuloPreDivisor = int(os.environ["TMCONFIG_MODULOPREDIVISOR"])
else:
    eventModuloPreDivisor = None

if os.environ.has_key( "TMCONFIG_CONFIGCREATELATE" ):
    configCreateLate = os.environ["TMCONFIG_CONFIGCREATELATE"]
else:
    configCreateLate = None

if os.environ.has_key( "TMCONFIG_CONFIGCREATELATECONFIGFILES" ) and os.environ["TMCONFIG_CONFIGCREATELATECONFIGFILES"].tolower() in ( "true", "1", "yes" ):
    configCreateLateConfigFiles = True
else:
    configCreateLateConfigFiles = False

if os.environ.has_key( "TMCONFIG_READOUTLISTVERSION" ):
    readoutListVersion = os.environ["TMCONFIG_READOUTLISTVERSION"]
else:
    readoutListVersion = None

if os.environ.has_key( "TMCONFIG_MAXEVENTSINCHAIN" ):
    maxEventsInChain = os.environ["TMCONFIG_MAXEVENTSINCHAIN"]
else:
    maxEventsInChain = None

if os.environ.has_key( "TMCONFIG_NOSTRICTSOR" ):
    noStrictSOR = os.environ["TMCONFIG_NOSTRICTSOR"]
else:
    noStrictSOR = None

if os.environ.has_key( "TMCONFIG_GATHEREROPTS" ):
    gathererOpts = os.environ["TMCONFIG_GATHEREROPTS"]
else:
    gathererOpts = None

if os.environ.has_key( "TMCONFIG_SCATTEREROPTS" ):
    scattererOpts = os.environ["TMCONFIG_SCATTEREROPTS"]
else:
    scattererOpts = None

if os.environ.has_key( "TMCONFIG_MERGEROPTS" ):
    mergerOpts = os.environ["TMCONFIG_MERGEROPTS"]
else:
    mergerOpts = None

if os.environ.has_key( "TMCONFIG_FILTEROPTS" ):
    filterOpts = os.environ["TMCONFIG_FILTEROPTS"]
else:
    filterOpts = None

if os.environ.has_key( "TMCONFIG_SBHOPTS" ):
    sbhOpts = os.environ["TMCONFIG_SBHOPTS"]
else:
    sbhOpts = None

if os.environ.has_key( "TMCONFIG_PBHOPTS" ):
    pbhOpts = os.environ["TMCONFIG_PBHOPTS"]
else:
    pbhOpts = None

if os.environ.has_key( "TMCONFIG_LOCALDATAFLOWOPTS" ):
    localDataFlowOpts = os.environ["TMCONFIG_LOCALDATAFLOWOPTS"]
else:
    localDataFlowOpts = None

if os.environ.has_key( "TMCONFIG_BRIDGEOPTS" ):
    bridgeOpts = os.environ["TMCONFIG_BRIDGEOPTS"]
else:
    bridgeOpts = None

if os.environ.has_key( "TMCONFIG_DATAFLOWOPTS" ):
    dataFlowOpts = os.environ["TMCONFIG_DATAFLOWOPTS"]
else:
    dataFlowOpts = None

if os.environ.has_key( "TMCONFIG_LISTFILEDIR" ):
    listFileDir = os.environ["TMCONFIG_LISTFILEDIR"]
else:
    listFileDir = None

    



usagestring = " -masternode <master-node-hostname> (-masternode <master-node-hostname>) (-usessh) -taskmandir <TaskManager-base-directory> -frameworkdir <AliHLT-framework-directory> (-platform <platform>) (-rundir <run-directory>) (-prestartexec <slave-prestart-executable>) (-postterminateexec <slave-post-terminate-executable>) (-basesocket <base-socket>) (-baseshm <base-shm-id>) (-eventmergertimeout <event-merger-timeout-ms>) (-interrupttarget <interrupt-target-address>)* (-emptynodes) (-production) (-bridgeshmtype [librorc|sysv]) -config <config-filename> (-reconfigure) (-alicehlt) (-eventaccounting <event-accounting-path>) (-eventmergerdynamictimeouts <min. dynamically calculated timeout value> <max. dynamically calculated timeout value>) (-eventmergerdynamicinputXabling <max. number of tolerated missing events before iput subscriber channel is deactivated>) (-runstoptimeout <run stop timeout in millisecs>) (-scattererparam <Event Scatterer parameter>) (-eventmodulopredivisor <event-modulo-pre-divisor specifier>) (-configcreatelate) (-configcreatelateconfigfiles) (-latecreationdata <late-creation-data-file>) (-latecreationconfig <late-creation-config-input-file>) (-latecreationnode <late-creation-slave-node>) (-readoutlistversion <readout-list-data-format-version-number>) (-maxeventsinchain <maximum-allowed-events-in-chain>) (-nostrictsor) (-gathereroptions <options for event gatherer>) (-scattereroptions <options for event scatterer>) (-mergeroptions <options for event merger>) (-filteroptions <options for filters>) (-sbhoptions <options for subscriber bridge head>) (-pbhoptions <options for publisher bridge head>) (-localdataflowoptions <options for node-local data flow components (scatterer/gatherer/merger (NOT filters))>) (-bridgeoptions <options for bridge components>) (-dataflowoptions <options for all data flow components (scatterer/gatherer/merger/filter/bridges>) (-listfiledir <directory-for-input-list-files (sockets and shm ids)>)"

times[0] = time.time()
    
try:
    while len(argList)>0:
        if argList[0] == "-masternode":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -masternode." )
            masterNodes.append( argList[1] )
            argList = argList[2:]
        elif argList[0] == "-usessh":
            useSSH = 1
            argList = argList[1:]
        elif argList[0] == "-taskmandir":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -taskmandir." )
            taskManDir = argList[1]
            argList = argList[2:]
        elif argList[0] == "-frameworkdir":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -frameworkdir." )
            frameworkDir = argList[1]
            argList = argList[2:]
        elif argList[0] == "-platform":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -platform." )
            platform = argList[1]
            argList = argList[2:]
        elif argList[0] == "-rundir":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -rundir." )
            runDir = argList[1]
            argList = argList[2:]
        elif argList[0] == "-prestartexec":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -prestartexec." )
            prestartExec = argList[1]
            argList = argList[2:]
        elif argList[0] == "-postterminateexec":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -postterminateexec." )
            postTerminateExec = argList[1]
            argList = argList[2:]
        elif argList[0] == "-basesocket":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -basesocket." )
            base_socket = int( argList[1] )
            argList = argList[2:]
        elif argList[0] == "-baseshm":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -baseshm." )
            base_shm = int( argList[1] )
            argList = argList[2:]
        elif argList[0] == "-eventmergertimeout":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -eventmergertimeout." )
            event_merger_timeout = int( argList[1] )
            argList = argList[2:]
        elif argList[0] == "-config":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -config." )
            configfiles.append( argList[1] )
            argList = argList[2:]
        elif argList[0] == "-latecreationdata":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -latecreationdata." )
            if lateCreationDataFile!=None:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nOnly one occurance of -latecreationdata is allowed." )
            lateCreationDataFile = argList[1]
            argList = argList[2:]
        elif argList[0] == "-latecreationconfig":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -latecreationconfig." )
            lateCreationConfigFiles.append( argList[1] )
            argList = argList[2:]
        elif argList[0] == "-latecreationnode":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -latecreationnode." )
            if lateCreationNode!=None:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nOnly one occurance of -latecreationnode is allowed." )
            lateCreationNode = argList[1]
            argList = argList[2:]
        elif argList[0] == "-interrupttarget":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -interrupttarget." )
            interrupt_targets.append( argList[1] )
            argList = argList[2:]
        elif argList[0] == "-emptynodes":
            empty_nodes = 1
            argList = argList[1:]
        elif argList[0] == "-production":
            production = 1
            argList = argList[1:]
        elif argList[0] == "-bridgeshmtype":
            bridgeShmType = argList[1]
            argList = argList[2:]
        elif argList[0] == "-reconfigure":
            reconfigure=1
            argList = argList[1:]
        elif argList[0] == "-alicehlt":
            aliceHLT=1
            argList = argList[1:]
        elif argList[0] == "-debug":
            debug=True
            argList = argList[1:]
        elif argList[0] == "-eventaccounting":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -eventaccounting." )
            eventAccounting = argList[1]
            argList = argList[2:]
        elif argList[0] == "-eventmergerdynamictimeouts":
            if len(argList)<=2:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -eventmergerdynamictimeouts." )
            event_merger_dynamic_min_timeout = int( argList[1] )
            event_merger_dynamic_max_timeout = int( argList[2] )
            if event_merger_dynamic_max_timeout<=0:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\n-eventmergerdynamictimeouts maximum timeout must be bigger than 0." )
            if event_merger_dynamic_max_timeout<event_merger_dynamic_min_timeout:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\n-eventmergerdynamictimeouts maximum timeout must be bigger minimum timeout." )
            argList = argList[3:]
        elif argList[0] == "-eventmergerdynamicinputXabling":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -eventmergerdynamicinputXabling." )
            event_merger_dynamic_input_Xabling = int( argList[1] )
            argList = argList[2:]
        elif argList[0] == "-runstoptimeout":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -runstoptimeout." )
            runstop_timeout = int( argList[1] )
            argList = argList[2:]
        elif argList[0] == "-scattererparam":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -scattererparam." )
            scattererParam = int( argList[1] )
            argList = argList[2:]
        elif argList[0] == "-eventmodulopredivisor":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -eventmodulopredivisor." )
            eventModuloPreDivisor = int( argList[1] )
            argList = argList[2:]
        elif argList[0] == "-configcreatelate":
            configCreateLate = True
            argList = argList[1:]
        elif argList[0] == "-configcreatelateconfigfiles":
            configCreateLateConfigFiles = True
            argList = argList[1:]
        elif argList[0] == "-readoutlistversion":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -readoutlistversion." )
            readoutListVersion = int( argList[1] )
            argList = argList[2:]
        elif argList[0] == "-maxeventsinchain":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -maxeventsinchain." )
            maxEventsInChain = int( argList[1] )
            argList = argList[2:]
        elif argList[0] == "-nostrictsor":
            noStrictSOR = True
            argList = argList[1:]
        elif argList[0] == "-gathereroptions":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -gathereroptions." )
            gathererOpts = argList[1]
            argList = argList[2:]
        elif argList[0] == "-scattereroptions":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -scattereroptions." )
            scattererOpts = argList[1]
            argList = argList[2:]
        elif argList[0] == "-mergeroptions":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -mergeroptions." )
            mergerOpts = argList[1]
            argList = argList[2:]
        elif argList[0] == "-filteroptions":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -filteroptions." )
            filterOpts = argList[1]
            argList = argList[2:]
        elif argList[0] == "-sbhoptions":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -sbhoptions." )
            sbhOpts = argList[1]
            argList = argList[2:]
        elif argList[0] == "-pbhoptions":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -pbhoptions." )
            pbhOpts = argList[1]
            argList = argList[2:]
        elif argList[0] == "-localdataflowoptions":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -localdataflowoptions." )
            localDataFlowOpts = argList[1]
            argList = argList[2:]
        elif argList[0] == "-bridgeoptions":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -bridgeoptions." )
            bridgeOpts = argList[1]
            argList = argList[2:]
        elif argList[0] == "-dataflowoptions":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -dataflowoptions." )
            dataFlowOpts = argList[1]
            argList = argList[2:]
        elif argList[0] == "-listfiledir":
            if len(argList)<=1:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -listfiledir." )
            listFileDir = argList[1]
            argList = argList[2:]
        else:
            raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nUnknown parameter '"+argList[0]+"'." )

    if len(configfiles)<=0 and lateCreationDataFile==None and len(lateCreationConfigFiles)<=0:
        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nAt least one config or late-creation-data file has to be specified." )
    if (len(configfiles)>0 and lateCreationDataFile!=None) or (len(configfiles)>0 and len(lateCreationConfigFiles)>0) or (lateCreationDataFile!=None and len(lateCreationConfigFiles)>0):
        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nConfig, late-creation-config-files, or late-creation-data files cannot be specified together." )
    if configCreateLate and (lateCreationDataFile!=None or len(lateCreationConfigFiles)>0):
        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\n-configcreatelate and -latecreationdata or -latecreationconfig cannot be specified together." )
    if (lateCreationDataFile==None and len(lateCreationConfigFiles)<=0) and len(masterNodes) < 1:
        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nAt least one master node has to be specified." )
    if (lateCreationDataFile==None and len(lateCreationConfigFiles)<=0) and len(masterNodes) > 2:
        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nAt most two master nodes can be specified." )
    if (lateCreationDataFile==None and len(lateCreationConfigFiles)<=0) and taskManDir == None:
        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nTaskManager base directory has to be specified." )
    if (lateCreationDataFile==None and len(lateCreationConfigFiles)<=0) and frameworkDir == None:
        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nAliHLT framework base directory has to be specified." )
    if (lateCreationDataFile==None and len(lateCreationConfigFiles)<=0) and platform == None:
        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nPlatform has to be specified." )
    if bridgeShmType!=None and bridgeShmType!="sysv" and bridgeShmType!="librorc":
        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nBridge shm type has to be either sysv or librorc." )
    if eventModuloPreDivisor==0:
        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nEvent modulo pre-divisor specifier must be non-zero." )
    if aliceHLT and readoutListVersion==None:
        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nReadoutlist version must be specified in ALICE HLT mode (2 or 3 (suggested currently (20101115))" )
    
    #outputDir = os.path.realpath( "." )
    #outputDir = os.getcwd()
    # The following hack seems necessary as both os.getcwd and os.path.realpath seem to expand symlinks...
    pwdfile=os.popen("pwd")
    outputDir=pwdfile.read()[:-1]
    pwdfile.close()
    del(pwdfile)
    #print "outputDir:",outputDir
    baseDir = os.path.dirname( os.path.realpath( sys.argv[0] ) )
    #print "baseDir:",baseDir
    includePaths = [ outputDir+"/templates", baseDir+"/templates" ]
    if listFileDir==None:
        listFileDir = outputDir
    scriptDir = baseDir+"/scripts"
    if os.path.basename( baseDir )=="bin":
        install_dir = os.path.dirname( baseDir )
        includePaths.append( os.path.join( install_dir, "share", "simplechainconfig1", "templates" ) )
        scriptDir = os.path.join( install_dir, "share", "simplechainconfig1", "scripts" )

    times[1] = time.time()

    lateCreationConfigParameterData = { "-taskmandir" : 1, "-frameworkdir" : 1, "-platform" : 1, "-basesocket" : 1, "-baseshm" : 1, "-eventmergertimeout" : 1,
                                        "-interrupttarget" : 1, "-production" : 0, "-bridgeshmtype" : 1, "-reconfigure" : 0, "-alicehlt" : 0, "-eventaccounting" : 1,
                                        "-eventmergerdynamictimeouts" : 2, "-eventmergerdynamicinputXabling" : 1, "-runstoptimeout" : 1, "-scattererparam" : 1,
                                        "-eventmodulopredivisor" : 1, "-readoutlistversion" : 1, "-maxeventsinchain" : 1, "-nostrictsor" : 0, "-gathereroptions" : 1,
                                        "-scattereroptions" : 1, "-mergeroptions" : 1, "-filteroptions" : 1, "-sbhoptions" : 1, "-pbhoptions" : 1, "-localdataflowoptions" : 1,
                                        "-bridgeoptions" : 1, "-dataflowoptions" : 1 }
    lateCreationConfigParameters = ""
    for lccpd in lateCreationConfigParameterData.keys():
        start=0
        while sys.argv[start:].count(lccpd)>0:
            start = sys.argv[start:].index(lccpd)
            count = lateCreationConfigParameterData[lccpd]+1
            for n in range(count):
                lateCreationConfigParameters += " "+sys.argv[start+n]
            start += count
    lateCreationConfigParameters += " -listfiledir "+listFileDir
    

    processedConfigs = []
    for configFile in configfiles + lateCreationConfigFiles:

        times[2] = time.time()

        listFileHandler = SimpleChainConfig1.ListFileHandler()

        times[3] = time.time()
        
        configReader = XMLConfigReader.XMLConfigReader(configFile)

        config = configReader.ReadSimpleChainConfig()

        times[4] = time.time()
        #config.Check()
        configMapper = SimpleChainConfig1.ChainConfigMapper( frameworkDir, platform )

        times[5] = time.time()

        for masterNode in masterNodes:
            if config.GetNode(masterNode)==None:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nSpecified master node is not listed in config file." )


        if base_socket!=None:
            configMapper.SetStartSocket( base_socket )
        if base_shm!=None:
            configMapper.SetStartShmID( base_shm )
        if event_merger_timeout != None:
            configMapper.SetEventMergerTimeout( event_merger_timeout )
        if event_merger_dynamic_max_timeout != None:
            configMapper.EnableEventMergerDynamicTimeouts( event_merger_dynamic_min_timeout, event_merger_dynamic_max_timeout )
        if event_merger_dynamic_input_Xabling != None:
            configMapper.EnableEventMergerDynamicInputXabling( event_merger_dynamic_input_Xabling )
        if scattererParam!=None:
            configMapper.SetScattererParam( scattererParam )
        if eventModuloPreDivisor!=None:
            configMapper.SetEventModuloPreDivisor( eventModuloPreDivisor )
        if readoutListVersion!=None:
            configMapper.SetReadoutListVersion( readoutListVersion )
        if maxEventsInChain!=None:
            configMapper.SetMaxEventsInChain( maxEventsInChain )
        if noStrictSOR:
            configMapper.NoStrictSOR()
            
        if gathererOpts!=None:
            configMapper.SetGathererCmdLineOptions( gathererOpts )
        if scattererOpts!=None:
            configMapper.SetScattererCmdLineOptions( scattererOpts )
        if mergerOpts!=None:
            configMapper.SetMergerCmdLineOptions( mergerOpts )
        if filterOpts!=None:
            configMapper.SetFilterCmdLineOptions( filterOpts )
        if sbhOpts!=None:
            configMapper.SetSubscriberBridgeCmdLineOptions( sbhOpts )
        if pbhOpts!=None:
            configMapper.SetPublisherBridgeCmdLineOptions( pbhOpts )
        if localDataFlowOpts!=None:
            configMapper.SetLocalDataFlowCmdLineOptions( localDataFlowOpts )
        if bridgeOpts!=None:
            configMapper.SetBridgeCmdLineOptions( bridgeOpts )
        if dataFlowOpts!=None:
            configMapper.SetDataFlowCmdLineOptions( dataFlowOpts )

            
        if production:
            configMapper.SetProduction( production )
        if bridgeShmType!=None:
            configMapper.SetPublisherBridgeHeadShmType( bridgeShmType )
        if aliceHLT!=None and aliceHLT:
            configMapper.AliceHLT()
        master_listen_sockets = {}
        master_slave_control_sockets = {}
        slave_listen_sockets = {}
        slave_slave_control_sockets = {}
        for n in config.GetNodes():
            master_slave_control_sockets[n.GetID()] = configMapper.GetNextSocket( config, n.GetID() )
            master_listen_sockets[n.GetID()] = configMapper.GetNextSocket( config, n.GetID() )
            
        for n in config.GetNodes():
            slave_slave_control_sockets[n.GetID()] = configMapper.GetNextSocket( config, n.GetID() )
            slave_listen_sockets[n.GetID()] = configMapper.GetNextSocket( config, n.GetID() )
            
        for ng in config.GetNodeGroups():
            ng.SetSlaveControlSocket( configMapper.GetNextSocket( config, ng.GetGroupMasterNode() ) )
            ng.SetListenSocket( configMapper.GetNextSocket( config, ng.GetGroupMasterNode() ) )

        if reconfigure:
            socket_list = listFileHandler.InputControlSockets( config, listFileDir+"/" )
            configMapper.AppendControlProcessSocketList( socket_list )
            shm_list = listFileHandler.InputShmIDs( config, listFileDir+"/" )
            configMapper.AppendShmIDs( shm_list )

        if eventAccounting!=None:
            configMapper.SetEventAccounting( eventAccounting )

        times[6] = time.time()

        procList = configMapper.CreateProcessList( config )
        #        for p in procList: print p.GetCmd()

        if configCreateLate and not configCreateLateConfigFiles:
            settings = {
                "masterNodes" : masterNodes,
                "useSSH" : useSSH,
                "includePaths" : includePaths,
                "taskManDir" : taskManDir,
                "frameworkDir" : frameworkDir,
                "platform" : platform,
                "runDir" : runDir,
                "outputDir" : outputDir,
                "prestartExec" : prestartExec,
                "postTerminateExec" : postTerminateExec,
                "interrupt_targets" : interrupt_targets,
                "master_slave_control_sockets" : master_slave_control_sockets,
                "master_listen_sockets" : master_listen_sockets,
                "slave_slave_control_sockets" : slave_slave_control_sockets,
                "slave_listen_sockets" : slave_listen_sockets,
                "empty_nodes" : empty_nodes,
                "production" : production,
                "runstop_timeout" : runstop_timeout,
                "baseDir" : baseDir,
                }
            SimpleChainConfig1.OutputProcessData( config, configMapper, procList, settings )
            #sys.exit(0)

        processedConfigs.append( { "config" : config, "mapper" : configMapper, "processes" : procList } )

    if lateCreationDataFile:
        processedConfigs.append( SimpleChainConfig1.InputProcessData(lateCreationDataFile) )
        masterNodes = processedConfigs[-1]["settings"]["masterNodes"]
        useSSH = processedConfigs[-1]["settings"]["useSSH"]
        includePaths = processedConfigs[-1]["settings"]["includePaths"]
        taskManDir = processedConfigs[-1]["settings"]["taskManDir"]
        frameworkDir = processedConfigs[-1]["settings"]["frameworkDir"]
        platform = processedConfigs[-1]["settings"]["platform"]
        runDir = processedConfigs[-1]["settings"]["runDir"]
        outputDir = processedConfigs[-1]["settings"]["outputDir"]
        prestartExec = processedConfigs[-1]["settings"]["prestartExec"]
        postTerminateExec = processedConfigs[-1]["settings"]["postTerminateExec"]
        interrupt_targets = processedConfigs[-1]["settings"]["interrupt_targets"]
        master_slave_control_sockets = processedConfigs[-1]["settings"]["master_slave_control_sockets"]
        master_listen_sockets = processedConfigs[-1]["settings"]["master_listen_sockets"]
        slave_slave_control_sockets = processedConfigs[-1]["settings"]["slave_slave_control_sockets"]
        slave_listen_sockets = processedConfigs[-1]["settings"]["slave_listen_sockets"]
        empty_nodes = processedConfigs[-1]["settings"]["empty_nodes"]
        production = processedConfigs[-1]["settings"]["production"]
        runstop_timeout = processedConfigs[-1]["settings"]["runstop_timeout"]
        baseDir = processedConfigs[-1]["settings"]["baseDir"]

    for procConfig in processedConfigs:
        config,configMapper,procList = procConfig["config"],procConfig["mapper"],procConfig["processes"]
        
        times[7] = time.time()


        if lateCreationNode==None:
            processListFile = open( config.GetName()+"-ProcessList.out", "w" )
            outputterProcessList = SimpleChainConfig1.SimpleProcListOutputter()
            outputterProcessList.OutputProcessList( config, procList, processListFile )
            processListFile.close()

        if lateCreationNode==None:
            SimpleChainConfig1.OutputGraphviz( config, configMapper, procList )


        times[8] = time.time()

        outputter1 = SimpleChainConfig1.TaskManagerOutputter( masterNodes, useSSH, includePaths, taskManDir, frameworkDir, platform, runDir, outputDir, prestartExec, postTerminateExec, scriptDir, interrupt_targets )
        outputter1.SetMasterSlaveControlSockets( master_slave_control_sockets )
        outputter1.SetMasterListenSockets( master_listen_sockets )
        outputter1.SetSlaveSlaveControlSockets( slave_slave_control_sockets )
        outputter1.SetSlaveListenSockets( slave_listen_sockets )
        outputter1.SetEmptyNodes( empty_nodes )
        outputter1.SetProduction( production )
        if configCreateLate:
            if configCreateLateConfigFiles:
                outputter1.ConfigCreateLate( baseDir+"/"+os.path.basename(sys.argv[0]), lateCreationConfigParameters, [ configFile ] )
            else:
                outputter1.ConfigCreateLate( baseDir+"/"+os.path.basename(sys.argv[0]), "" )
        if runstop_timeout!=None:
            outputter1.RunStopTimeout( runstop_timeout )
        if lateCreationNode!=None:
            # print lateCreationNode
            outputter1.OnlySlaveNode( lateCreationNode )
        outputter1.GenerateTaskManagerFiles( config, configMapper, procList )

        if lateCreationNode!=None:
            continue

        times[9] = time.time()

        outputter2 = SimpleChainConfig1.TaskManagerCheckOutputter( masterNodes, useSSH, includePaths, taskManDir, frameworkDir, platform, runDir, outputDir, prestartExec, postTerminateExec, scriptDir )
        outputter2.SetMasterSlaveControlSockets( master_slave_control_sockets )
        outputter2.SetMasterListenSockets( master_listen_sockets )
        outputter2.SetSlaveSlaveControlSockets( slave_slave_control_sockets )
        outputter2.SetSlaveListenSockets( slave_listen_sockets )
        outputter2.SetEmptyNodes( empty_nodes )
        outputter2.SetProduction( production )
        outputter2.GenerateTaskManagerFiles( config, configMapper, procList )

        times[10] = time.time()

        outputter3 = SimpleChainConfig1.TaskManagerStartupScriptOutputter( masterNodes, useSSH, includePaths, taskManDir, frameworkDir, platform, runDir, outputDir, prestartExec, postTerminateExec )
        outputter3.SetMasterSlaveControlSockets( master_slave_control_sockets )
        outputter3.SetMasterListenSockets( master_listen_sockets )
        outputter3.SetSlaveSlaveControlSockets( slave_slave_control_sockets )
        outputter3.SetSlaveListenSockets( slave_listen_sockets )
        outputter3.SetEmptyNodes( empty_nodes )
        outputter3.SetProduction( production )
        outputter3.GenerateStartupScripts( config, configMapper, procList )

        times[11] = time.time()

        outputter4 = SimpleChainConfig1.TaskManagerKillScriptOutputter( masterNodes, useSSH, includePaths, taskManDir, frameworkDir, platform, runDir, outputDir, prestartExec, postTerminateExec, scriptDir )
        outputter4.SetMasterSlaveControlSockets( master_slave_control_sockets )
        outputter4.SetMasterListenSockets( master_listen_sockets )
        outputter4.SetSlaveSlaveControlSockets( slave_slave_control_sockets )
        outputter4.SetSlaveListenSockets( slave_listen_sockets )
        outputter4.SetEmptyNodes( empty_nodes )
        outputter4.SetProduction( production )
        outputter4.GenerateTaskManagerFiles( config, configMapper, procList )

        times[12] = time.time()

        outputter5 = SimpleChainConfig1.TaskManagerCleanScriptOutputter( masterNodes, useSSH, includePaths, taskManDir, frameworkDir, platform, runDir, outputDir, prestartExec, postTerminateExec, scriptDir )
        outputter5.SetMasterSlaveControlSockets( master_slave_control_sockets )
        outputter5.SetMasterListenSockets( master_listen_sockets )
        outputter5.SetSlaveSlaveControlSockets( slave_slave_control_sockets )
        outputter5.SetSlaveListenSockets( slave_listen_sockets )
        outputter5.SetEmptyNodes( empty_nodes )
        outputter5.SetProduction( production )
        outputter5.GenerateTaskManagerFiles( config, configMapper, procList )

        times[13] = time.time()


        listFileHandler.OutputControlSockets( config, configMapper )
        listFileHandler.OutputShmIDs( config, configMapper )


except SimpleChainConfig1.ChainConfigException, e:
    sys.stderr.write(  "Exception caught: "+e.fValue+"\n" )
    if debug:
        raise
    sys.exit( -1 )

# for i in range(0,14):
#     print "dt%d: %f" % (i, times[i]-times[i-1])
