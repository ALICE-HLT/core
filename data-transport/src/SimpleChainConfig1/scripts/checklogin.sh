#!/bin/bash

if [[ $# -lt 1 ]] ; then
  echo Usage: $0 \<loginprogram:hostname\> \(\<loginprogram:hostname\> ...\) >&2
  exit 1
fi

for ph in $* ; do 
    LOGINPROG=`echo $ph|awk -F: '{print $1}'`
    HOSTNAME=`echo $ph|awk -F: '{print $2}'`
    if [[ $LOGINPROG = "ssh" ]] ; then
        REALLOGINPROG="$LOGINPROG -o BatchMode=yes"
    else
       REALLOGINPROG="$LOGINPROG"
    fi
    #echo LOGINPROG $LOGINPROG
    #echo HOSTNAME $HOSTNAME
    #echo Attempting to login to $HOSTNAME. If a password prompt or something similar appears, please abort with Ctrl-C.
    ECHO=`$REALLOGINPROG $HOSTNAME echo -n Works </dev/zero 2>/dev/null |grep Works`
    if [[ $ECHO != "Works" ]] ; then
        echo Unable to login to $HOSTNAME without a password using $LOGINPROG
        HADERROR=1
    fi
done

if [[ $HADERROR ]] ; then
    exit 1
else
    exit 0
fi
