#!/bin/bash

if [ $# -ne 2 ] ; then
  echo Usage: $0 "<node>" "<command>" >&2
  exit 125
fi

NODE=$1
CMD=$2

RUNNING=`ping -c 1 -w 1 ${NODE} 2>/dev/null|grep "1 received"`

if [ -z "${RUNNING}" ] ; then
  exit 3
fi


RUNNING=`ssh ${NODE} ps x 2>&1| grep -- "${CMD}"|grep -v grep`

#echo RUNNING: ${RUNNING}

if [ -z "${RUNNING}" ] ; then
  exit 4
fi



exit 0