#!/bin/bash

if [[ $# -lt 1 ]] ; then
  echo Usage: $0 \<programname\> \(\<programname\> ...\) >&2
  exit 1
fi

CHECKEXECUTABLE=1
if [[ `basename $0` = "checklibraries.sh" ]] ; then CHECKEXECUTABLE=0 ; fi


for pn in $* ; do 
  if [[ -f $pn ]] ; then
    EXECUTABLE=$pn
  else
    EXECUTABLE=`which $pn 2>/dev/null`
  fi
  if [[ -z "$EXECUTABLE" ]] ; then
    if [[ $HADERROR ]] ; then echo ; fi ; HADERROR=1
    echo $pn:
    echo "   "Cannot find executable.
    echo "   "Please check the following:
    echo "   *" The program name is written correctly.
    echo "   *" The program"'"s executable is located in a directory contained in the PATH environment variable.
    echo "       " or
    echo "   *" The full path to the program"'"s executable has been supplied to this program.
  else
    SHOWEDNAME=0
    if [ "$CHECKEXECUTABLE" = 1 -a ! -x $EXECUTABLE ] ; then
        if [[ $HADERROR ]] ; then echo ; fi ; HADERROR=1
        if [[ $SHOWEDNAME = 0 ]] ; then echo $pn: ; fi ; SHOWEDNAME=1
        echo "   "The located executable $EXECUTABLE actually is not executable.
    fi
    FILE=`file -Lb $EXECUTABLE`
    if [[ -n "`echo $FILE | grep ELF`" ]] ; then 
        #echo binary
        LDDs=`ldd $EXECUTABLE 2>/dev/null|grep "not found"|awk '{print $1}'`
        if [[ -n "$LDDs" ]] ; then
            if [[ $HADERROR ]] ; then echo ; fi ; HADERROR=1
            if [[ $SHOWEDNAME = 0 ]] ; then echo $pn: ; fi ; SHOWEDNAME=1
            echo "   "The following shared libraries cannot be found for the located executable $EXECUTABLE.
            for nfl in $LDDs ; do
                echo "   *" $nfl
            done
            echo "   "Please check the following:
            echo "   *" Each shared library executable is located in one of the system default library directories \(e.g. /lib or /usr/lib\).
            echo "       " or
            echo "   *" Each shared library executable is located in a directory configured in /etc/ld.so.conf.
            echo "       " or
            echo "   *" Each shared library executable is located in a directory listed in the LD_LIBRARY_PATH environment variable.
        fi
    elif [[ -n "`echo $FILE | grep python`" ]] ; then
        echo python >/dev/null 2>&1
        MODULES1=`grep -E "^[:space:]*import" $EXECUTABLE | python -c "import string,sys
line=sys.stdin.readline()
while line!=\"\":
    words=string.split(line)
    first=1
    for w in words[1:]:
        if not first:
            print \" \",
        first=0
        if w[-1]==\",\":
            w=w[:-1]
        print w,
    line=sys.stdin.readline()
 
"`
        MODULES2=`grep -E "^[[:space:]]*from[[:space:]]+[[:alnum:]_]+[[:space:]]+import" $EXECUTABLE | python -c "import string,sys
line=sys.stdin.readline()
while line!=\"\":
    words=string.split(line)
    print words[1]
    line=sys.stdin.readline()
"`
        MODULES="$MODULES1 $MODULES2"
        #echo Python modules: $MODULES
        NOTLOADED=""
        for pm in $MODULES ; do
            if [[ -n "`python -c \"import $pm\" 2>&1`" ]] ; then
                if [[ -e `dirname $EXECUTABLE`/$pm.py ]] ; then
                    NOTLOADEDDEPS="$NOTLOADEDDEPS $pm"
                    `dirname $0`/checklibraries.sh `dirname $EXECUTABLE`/$pm.py
                else
                    NOTLOADED="$NOTLOADED $pm"
                fi
            fi
        done
        if [[ -n "$NOTLOADED" ]] ; then
            if [[ $HADERROR ]] ; then echo ; fi ; HADERROR=1
            if [[ $SHOWEDNAME = 0 ]] ; then echo $pn: ; fi ; SHOWEDNAME=1
            echo "   "The following Python modules could not be loaded.
            for pm in $NOTLOADED ; do
                echo "   *" $pm
            done
            echo "   "Please check the following:
            echo "   *" Each python module is located in a system python module path
            echo "       " or
            echo "   *" Each python module is located in the same directory as its calling executable
            echo "       " or
            echo "   *" A link to each python module is located in the same directory as its calling executable
        fi
        if [[ -n "$NOTLOADEDDEPS" ]] ; then
            if [[ $HADERROR ]] ; then echo ; fi ; HADERROR=1
            if [[ $SHOWEDNAME = 0 ]] ; then echo $pn: ; fi ; SHOWEDNAME=1
            echo "   "The following Python modules could not be loaded, potentially because of dependencies on other modules.
            for pm in $NOTLOADEDDEPS ; do
                echo "   *" $pm
            done
            echo "   "Please check the following for each of these modules and all their dependencies:
            echo "   *" Each python module is located in a system python module path
            echo "       " or
            echo "   *" Each python module is located in the same directory as its calling executable
            echo "       " or
            echo "   *" A link to each python module is located in the same directory as its calling executable
        fi
    elif [[ -n "`echo $FILE | grep shell`" ]] ; then
        echo shellscript >/dev/null 2>&1
    else
        if [[ $HADERROR ]] ; then echo ; fi ; HADERROR=1
        if [[ $SHOWEDNAME = 0 ]] ; then echo $pn: ; fi ; SHOWEDNAME=1
        echo "   "Cannot further evaluate located executable $EXECUTABLE. Unsupported file type $FILE.
    fi
  fi
done

if [[ $HADERROR ]] ; then
    exit 1
else
    exit 0
fi
