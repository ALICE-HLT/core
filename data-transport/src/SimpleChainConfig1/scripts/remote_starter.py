#!/usr/bin/env python

import thread, threading, os, string, sys, time

class Starter(threading.Thread):
    def __init__(self, node, shell, cmd ):
        threading.Thread.__init__(self)
        self.fNode = node
        self.fShell = shell
        self.fCmd = cmd
        self.fOutput = ""
        self.fReturnValue = None

    def GetOutput(self):
        return self.fOutput
    def GetNode(self):
        return self.fNode
    def GetCmd(self):
        return self.fCmd
    def GetCmd(self):
        return self.fShell
    def GetReturnValue(self):
        return self.fReturnValue

    def run(self):
        #print self.fNode,self.fCmd
        #print "ssh "+self.fNode+" "+self.fCmd
        #inp = os.popen( self.fShell+" "+self.fNode+" "+self.fCmd+" \\; echo $?", "r" )
        inp = os.popen( "if ping -c 1 -w 1 "+self.fNode+" >/dev/null 2>/dev/null ; then ssh -o ConnectTimeout=3 "+self.fNode+" "+self.fCmd+" \\; echo $? 2>&1; else echo Not reachable ; echo 1 ; fi", "r" )
        
        line=inp.readline()
        lastline=""
        while line !="":
            self.fOutput += lastline
            lastline = line
            line=inp.readline()
        try:
            self.fReturnValue = int(line)
        except ValueError:
            self.fReturnValue = 0
        self.fOutput += line
        inp.close()


if __name__=="__main__":
    maxThreads = 20

    usage = "Usage: "+sys.argv[0]+" (-maxthreads <max.-thread-count (default %d)>) (-quiet) [ -command <cmd> -nodes <nodelist> | -nodecommand <node> <command for node> ] \n" % (maxThreads)

    cmd = None
    nodecommands = {}
    nodes = []
    quiet = False
    shell="ssh"
    returnvalue = 0

    try:
        args = sys.argv[1:]
        while len(args)>0:
            if args[0]=="-maxthreads":
                if len(args)==1:
                    raise Exception( "Missing -maxthreads argument" )
                try:
                    maxThreads = int(args[1])
                except ValueError:
                    raise Exception( "-maxthreads argument is not an integer value" )
                args = args[2:]
            elif args[0]=="-shell":
                if len(args)==1:
                    raise Exception( "Missing -shell argument" )
                shell = args[1]
                args = args[2:]
            elif args[0]=="-quiet":
                quiet = True
                args = args[1:]
            elif args[0]=="-nodes":
                if len(args)==1:
                    raise Exception( "Missing -nodes argument" )
                # nodes.append(args[1])
                nodes = args[1].split( " " )
                args = args[2:]
            elif args[0]=="-command":
                if len(args)==1:
                    raise Exception( "Missing -command argument" )
                if cmd!=None:
                    raise Exception( "-command argument may only be given once" )
                cmd = args[1]
                args = args[2:]
            elif args[0]=="-nodecommand":
                if len(args)<3:
                    raise Exception( "Missing -nodecommand argument" )
                if not nodecommands.has_key(args[1]):
                    nodecommands[args[1]] = []
                nodecommands[args[1]].append(args[2])
                args = args[3:]
            else:
                raise Exception( "Unknown argument '"+args[0]+"'" )
        if (len(nodes)>0 or cmd!=None) and len(nodecommands.keys())>0:
            raise Exception( "-node/-command must not be used together with -nodecommand" )
        elif len(nodes)>0 and cmd==None:
            raise Ecception( "Command must be given via -command" )
        elif cmd!=None and len(nodes)<=0:
            raise Exception( "At least one node must be specified via -node" )
        elif len(nodes)<=0 and cmd==None and len(nodecommands.keys())<=0:
            raise Exception( "Nothing specified" )
        
    except Exception,e:
        sys.stderr.write( usage )
        sys.stderr.write( str(e)+"\n" )
        sys.exit(1)

        
    if len(nodes)>0:
        for node in nodes:
            if node!="":
                nodecommands[node] = [cmd]

    threads=[]
    for t in range(maxThreads):
        threads.append(None)

    threadsActive = True
    activity = False
    while len(nodecommands.keys())>0 or threadsActive:
        if not activity:
            time.sleep(0.2)
        activity = False
        threadsActive = False
        for t in range(maxThreads):
            #print t,threads[t],
            #if threads[t]!=None:
            #    print threads[t].isAlive()
            #else:
            #    print
            if threads[t]!=None and not threads[t].isAlive():
                threads[t].join()
                if threads[t].GetReturnValue():
                    returnvalue = 1
                if not quiet:
                    print threads[t].GetNode(),":",threads[t].GetCmd(),":"
                print threads[t].GetOutput(),
                threads[t] = None
                activity = True
            if threads[t]==None and len(nodecommands.keys())>0:
                node = nodecommands.keys()[0]
                threads[t] = Starter( node, shell, nodecommands[node][0] )
                threads[t].start()
                nodecommands[node] = nodecommands[node][1:]
                if len(nodecommands[node])<=0:
                    del nodecommands[node]
                activity = True
            if threads[t]!=None and threads[t].isAlive():
                threadsActive = True


sys.exit(returnvalue)
