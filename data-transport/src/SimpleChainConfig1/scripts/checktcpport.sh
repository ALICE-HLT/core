#!/bin/bash

if [[ $# -lt 1 ]] ; then
  echo Usage: $0 \<tcpport\> \(\<tcpport\> ...\) >&2
  exit 1
fi

for port in $* ; do
    PROGRAM=`netstat -atnp 2>/dev/null | grep -v TIME_WAIT | python -c "import sys, string
line=sys.stdin.readline()
while line != \"\":
    words=string.split( line )
    if len(words)==7:
        port=string.split(words[3], \":\")[1]
        if port == \"$port\":
            print words[6]
            break
    line=sys.stdin.readline()
"`
    if [[ -n "$PROGRAM" ]] ; then
        if [[ "$PROGRAM" = "-" ]] ; then
            echo Port $port is already in use by unknown program
        else
            PID=`echo $PROGRAM | awk -F/ '{print $1}'`
            EXEC=`echo $PROGRAM | awk -F/ '{print $2}'`
            echo Port $port is already in use by program $EXEC \(PID $PID\)
            HADERROR=1
        fi
    fi
done

if [[ $HADERROR ]] ; then
    exit 1
else
    exit 0
fi
