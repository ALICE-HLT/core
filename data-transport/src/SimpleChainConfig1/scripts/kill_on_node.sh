#!/bin/bash

if [ $# -ne 2 ] ; then
  echo Usage: $0 "<node>" "<command>" >&2
  exit 125
fi

NODE=$1
CMD=$2

# First send SIGQUIT, nudge nudge process gently, know what I mean

RUNNINGNODE=`ping -c 1 -w 1 ${NODE} 2>/dev/null|grep "1 received"`
if [ -z "${RUNNINGNODE}" ] ; then
  exit 3
fi


RUNNINGPROC=`ssh ${NODE} ps x 2>&1| grep -- "${CMD}"|grep -v grep`

#echo RUNNINGPROC: ${RUNNINGPROC}

if [ -z "${RUNNINGPROC}" ] ; then
  exit 0
fi

PIDs=`echo ${RUNNINGPROC}|awk {'print $1'}`

for pid in ${PIDs} ; do
  RUNNINGNODE=`ping -c 1 -w 1 ${NODE} 2>/dev/null|grep "1 received"`
  if [ -z "${RUNNINGNODE}" ] ; then
    exit 3
  fi
  ssh ${NODE} kill -QUIT ${pid}
done


# Now send SIGTERM, poke process a bit more forcefully with a banana


RUNNINGNODE=`ping -c 1 -w 1 ${NODE} 2>/dev/null|grep "1 received"`
if [ -z "${RUNNINGNODE}" ] ; then
  exit 3
fi

RUNNINGPROC=`ssh ${NODE} ps x 2>&1| grep -- "${CMD}"|grep -v grep`

#echo RUNNINGPROC: ${RUNNINGPROC}

if [ -z "${RUNNINGPROC}" ] ; then
  exit 0
fi



PIDs=`echo ${RUNNINGPROC}|awk {'print $1'}`

for pid in ${PIDs} ; do
  RUNNINGNODE=`ping -c 1 -w 1 ${NODE} 2>/dev/null|grep "1 received"`
  if [ -z "${RUNNINGNODE}" ] ; then
    exit 3
  fi
  ssh ${NODE} kill ${pid}
done


# Finally send SIGKILL, drop a 16t weight on process's head



RUNNINGNODE=`ping -c 1 -w 1 ${NODE} 2>/dev/null|grep "1 received"`
if [ -z "${RUNNINGNODE}" ] ; then
  exit 3
fi

RUNNINGPROC=`ssh ${NODE} ps x 2>&1| grep -- "${CMD}"|grep -v grep`

#echo RUNNINGPROC: ${RUNNINGPROC}

if [ -z "${RUNNINGPROC}" ] ; then
  exit 0
fi



PIDs=`echo ${RUNNINGPROC}|awk {'print $1'}`

for pid in ${PIDs} ; do
  RUNNINGNODE=`ping -c 1 -w 1 ${NODE} 2>/dev/null|grep "1 received"`
  if [ -z "${RUNNINGNODE}" ] ; then
    exit 3
  fi
  ssh ${NODE} kill -9 ${pid}
done



exit 0

