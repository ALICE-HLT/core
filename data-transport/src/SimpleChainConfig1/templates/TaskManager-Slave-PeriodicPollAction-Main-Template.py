procs = ConnectNoCmdIDs+ConnectCmdIDs+SourceIDs+ProcSinkIDs

mode = KIPTaskMan.GetVar( "Mode" )
if mode==None:
    mode = ""
if mode=="stopping_run" and $(STOPRUNTIMEOUT)!=0 and len(procs)&gt;0:
    now=time.time()
    cmdRecv = KIPTaskMan.GetVar( "CommandReceived" )
    if cmdRecv == None:
        cmdRecv = now
    tdiff = float(now-cmdRecv)*1000.0 # Convert to millisecs.
    if tdiff&gt;$(STOPRUNTIMEOUT):
        KIPTaskMan.SetProcessStateChanged( procs[0] )

if mode=="stopping" and $(STOPTIMEOUT)!=0 and len(procs)&gt;0:
    now=time.time()
    cmdRecv = KIPTaskMan.GetVar( "CommandReceived" )
    if cmdRecv == None:
        cmdRecv = now
    tdiff = float(now-cmdRecv)*1000.0 # Convert to millisecs.
    if tdiff&gt;$(STOPTIMEOUT):
        KIPTaskMan.SetProcessStateChanged( procs[0] )


remSubs = KIPTaskMan.GetVar( "RemoveScheduledSubscribers" )
if remSubs==None:
    remSubs=()

yieldTime = 100
#alarm=""
status=""
alarm=0
alarm_text=""
runNumber = KIPTaskMan.GetVar( "RunNumber" )
if runNumber==None:
    runNumber=0
status = "RunNumber=%d;" % runNumber
for id in procs:
    if id in remSubs:
        continue
    try:
        state = KIPTaskMan.QueryState( id )
    except:
        if not KIPTaskMan.SuppressComErrorLogsQuery( id ) and mode!="stopping":
            KIPTaskMan.Log( 8, "Unable to query state from process "+id )
        continue
    KIPTaskMan.Yield( yieldTime )
    try:
        statData = KIPTaskMan.QueryStatusData( id )
    except:
        if not KIPTaskMan.SuppressComErrorLogsQuery( id ) and mode!="stopping":
            KIPTaskMan.Log( 8, "Unable to query status data from process "+id )
        continue
    KIPTaskMan.Yield( yieldTime )
    KIPTaskMan.Log( 2, "Process "+id+" status data: '"+statData+"'" )
    data = string.split( statData, ":" )
    proc_status="name="+id+":"+statData+";"
    status=status+proc_status
    total=0.0
    free=0.0
    for item in data:
        entries=item.split( "=" )
        if len(entries)&gt;=2:
            if entries[0]=="TotalOutputBuffer":
                total = float(entries[1])
            elif entries[0]=="FreeOutputBuffer":
                free=float(entries[1])
    if total==0.0:
        continue
    ratio=free/total
    KIPTaskMan.Log( 2, "Buffer free for process "+id+" is "+("%f (%f/%f)" % (ratio,total,free)) )
    if ratio &lt;= 0.15:
        #KIPTaskMan.Log( 8, "Buffer free for process "+id+" is only "+("%f" % (ratio)) )
        alarm=1
        alarm_text=alarm_text+proc_status
    
status=status[:-1]+""

KIPTaskMan.SetTaskStatusData( status )

if alarm:
    KIPTaskMan.TaskInterrupt( "HIGHWATERMARK;"+alarm_text[:-1] )

