KIPTaskMan.Log( 2, "Executing $(ID) PreConfigChangePhase3Action" )

changeState = KIPTaskMan.GetProcessConfigChangeState( "$(ID)" )

KIPTaskMan.Log( 2, "$(ID) config change state: "+changeState )

if changeState=="added":
    KIPTaskMan.SendCommand( "$(ParentID)", "PauseProcessing" )
    KIPTaskMan.SendCommand( "$(ParentID)", "AddPublisher $(ParentPublisher)" )
    KIPTaskMan.SendCommand( "$(ParentID)", "ResumeProcessing" )

    addSubs = KIPTaskMan.GetVar( "SubscriberAdditions" )
    if addSubs==None:
        addSubs = {}
    KIPTaskMan.Log( 2, "$(ID) subscriber additions: "+str(addSubs) )

    if addSubs.has_key( "$(ID)" ):
        addSubs["$(ID)"].append( [ "$(ChildID)", "$(ID)Pub", "$(ChildSubscriber)" ] )
    else:
        addSubs["$(ID)"] = [ [ "$(ChildID)", "$(ID)Pub", "$(ChildSubscriber)"] ]

    KIPTaskMan.SetVar( "SubscriberAdditions", addSubs )


