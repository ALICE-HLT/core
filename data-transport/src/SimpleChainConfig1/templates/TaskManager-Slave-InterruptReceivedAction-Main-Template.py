
mode = KIPTaskMan.GetVar( "Mode" )
if mode==None:
    mode = ""

lam_procs = KIPTaskMan.GetLAMProcessList()
alarm=0
text="HIGHWATERMARK;;"
for lp in lam_procs:
    nd=""
    if len(lp)&gt;0 and lp[1]!=None:
        nd=lp[1]
    id=lp[0]
    KIPTaskMan.Log( 4, "Interrupt/LAM received from process "+id+" with notification data '"+nd+"'" )
    try:
        state = KIPTaskMan.QueryState( id )
    except:
        if not KIPTaskMan.SuppressComErrorLogsQuery( id ) and mode!="stopping":
            KIPTaskMan.Log( 8, "Unable to query state from process "+id )
        continue
    try:
        statData = KIPTaskMan.QueryStatusData( id )
    except:
        if not KIPTaskMan.SuppressComErrorLogsQuery( id ) and mode!="stopping":
            KIPTaskMan.Log( 8, "Unable to query status data from process "+id )
        continue
    KIPTaskMan.Log( 2, "Process "+id+" status data: '"+statData+"'" )
    data = string.split( statData, "::" )
    if len(data)&lt;3 or data[1]=="" or data[2]=="":
        continue
    total = float(data[1])
    free = float(data[2])
    if total==0:
        continue
    ratio=free/total
    KIPTaskMan.Log( 2, "Buffer free for process "+id+" is "+("%f (%d/%d)" % (ratio,total,free)) )
    if ratio &lt;= 0.15:
        KIPTaskMan.Log( 8, "Buffer free for process "+id+" is only "+("%f" % (ratio)) )
        alarm=1
        text=text+id+"::"+state+"::"+data[1]+"::"+data[2]+";;"
    if string.find( state,"Error" )!=-1:
        KIPTaskMan.SetProcessStateChanged( id )

if alarm:
    KIPTaskMan.TaskInterrupt( text )
