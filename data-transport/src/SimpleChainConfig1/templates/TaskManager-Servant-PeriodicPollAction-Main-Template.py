
mode = KIPTaskMan.GetVar( "Mode" )
if mode==None:
    mode = ""

#alarm=""
status=""
alarm=0
alarm_text=""
lowest_ratio=1.0
lowest_free=0.0
lowest_total=0.0
for id in IDs:
    try:
        state = KIPTaskMan.QueryState( id )
    except:
        if not KIPTaskMan.SuppressComErrorLogsQuery( id ) and mode!="slave_kill":
            KIPTaskMan.Log( 8, "Unable to query status data from process "+id )
        continue
    try:
        statData = KIPTaskMan.QueryStatusData( id )
    except:
        if not KIPTaskMan.SuppressComErrorLogsQuery( id ) and mode!="slave_kill":
            KIPTaskMan.Log( 8, "Unable to query status data from process "+id )
        continue
    KIPTaskMan.Log( 2, "Process "+id+" status data: '"+statData+"'" )
    for pd in string.split(statData, ";" ):
        proc_status=pd
        proc_id="UNKNOWN"
        entries = string.split( pd, ":" )
        total=0.0
        free=0.0
        proc_status=""
        for entry in entries:
            data=string.split(entry,"=")
            if len(entries)&gt;=2:
                if entries[0]=="name":
                    proc_id = id+"/"+entries[1]
                    entries = [ entries[0], proc_id ]
                elif entries[0]=="TotalOutputBuffer":
                    total = float(entries[1])
                elif entries[0]=="FreeOutputBuffer":
                    free=float(entries[1])
                proc_status=proc_status+entries[0]+"="+entries[1]+":"
        if total==0.0:
            continue
        ratio=free/total
        if ratio&lt;lowest_ratio:
            lowest_ratio=ratio
            lowest_total=total
            lowest_free=free
        status=status+proc_status[:-1]+";"
    
        KIPTaskMan.Log( 2, "Buffer free for process "+proc_id+" is "+("%f (%f/%f)" % (lowest_ratio,lowest_total,lowest_free)) )
        if lowest_ratio &lt;= 0.15:
            #KIPTaskMan.Log( 8, "Lowest buffer free for process "+id+" is only "+("%f" % (lowest_ratio)) )
            alarm=1
            alarm_text=alarm_text+proc_status

KIPTaskMan.SetTaskStatusData( status[:-1] )

if alarm:
    KIPTaskMan.TaskInterrupt( "HIGHWATERMARK;"+alarm_text[:-1] )

