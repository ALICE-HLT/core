IDs = SourceIDs + ProcSinkIDs + ConnectNoCmdIDs + ConnectCmdIDs

KIPTaskMan.Log( 2, "IDs (1): "+str(IDs) )

remSubs = KIPTaskMan.GetVar( "RemoveScheduledSubscribers" )
if remSubs==None:
    remSubs=()

states = {}
for p in IDs:
    if p in remSubs:
        states[p]=""
        continue
    states[p] = KIPTaskMan.QueryState( p )



state = KIPTaskMan.GetVar( "State" )
if state==None:
    state=""
mode = KIPTaskMan.GetVar( "Mode" )
if mode==None:
    mode=""
oldmode = mode

now = time.time()
KIPTaskMan.SetVar( "CommandReceived", now )

KIPTaskMan.Log( 2,  "Command Received" )
cmd = KIPTaskMan.GetTaskCommand()

while cmd!=None:
    KIPTaskMan.Log( 2,  "Command: "+cmd+" - State: "+state )
    KIPTaskMan.Log( 2, "IDs (2): "+str(IDs) )
    parameters = cmd.split( " " )[1:]
    KIPTaskMan.Log( 2,  "Parameters: "+str(parameters) )
    fullcmd = cmd
    cmd = cmd.split( " " )[0]
    #if cmd=="reconfigure" and (state=="TM:EMPTY" or state==""):
    if cmd=="reconfigure":
        KIPTaskMan.ReadConfiguration()
##    if cmd=="cleanup" and state=="dead":
##        os.system( "rm -rf $(storage_name)" )
##        #os.system( "find . -type p -exec rm \\\"{}\\\" \\;" )
##        #os.system( "rm *.log" )
    if (cmd=="start") and state=="dead":
        KIPTaskMan.SetVar( "StopCommandReceivedTimeoutExpireMessage", False )
        configParams = {}
        if len(parameters)>0:
            configParameters = parameters[0].split(";")
            for p in configParameters:
                ps = p.split("=")
                if len(ps)==2:
                    configParams[ps[0]] = ps[1]
                if len(ps)&gt;2:
                    configParams[ps[0]] = "=".join( ps[1:] )
        KIPTaskMan.SetVar( "ConfigureParameters", configParams )
        try:
            runNumber = int(configParams["RUN_NUMBER"])
            KIPTaskMan.SetVar( "RunNumber", runNumber )
            KIPTaskMan.SetTaskStatusData( "RunNumber=%d;" % runNumber )
        except:
            KIPTaskMan.SetVar( "RunNumber", 0 )
        for p in IDs:
            if p in remSubs:
                continue
#            if states[p]=="TM:NOTSTARTED" or states[p]=="TM:DEAD": ##XXXX FixMe!
            if states[p]=="TM:NOTSTARTED":
                KIPTaskMan.Log( 2,  "Starting process "+p )
                KIPTaskMan.SuppressComErrorLogs( p, 120000000 )
                KIPTaskMan.StartProcess( p )
        mode="starting"
        state="starting"
    if cmd=="connect":
        if state=="ready_local":
            for p in ConnectCmdIDs:
                if p in remSubs:
                    continue
                KIPTaskMan.Log( 2,  "Sending 'Connect' command to process "+p )
                KIPTaskMan.SendCommand( p, "Connect", 2000000 )
        if state=="ready_local" or (state=="ready" and len(ConnectCmdIDs)==0):
            mode="connecting"
        if state=="ready_local":
            state="connecting"
        if len(ConnectCmdIDs)==0 and len(IDs)&gt;0 and state!="ready":
            KIPTaskMan.SetProcessStateChanged( IDs[0] )
    if (cmd=="quit" or cmd=="kill_slaves") and (state=="dead" or state=="TM:EMPTY"):
        KIPTaskMan.TerminateTask()
    if cmd=="kill_processes":
        KIPTaskMan.Log( 2, "IDs (3): "+str(IDs) )
        for p in IDs:
            KIPTaskMan.Log( 2,  "Terminating process "+p )
            KIPTaskMan.SuppressComErrorLogs( p, 90000000 )
            try:
                KIPTaskMan.Disconnect( p )
            except:
                pass
            KIPTaskMan.PrepareProcessForTermination( p )
            KIPTaskMan.TerminateProcess( p )
        mode=""
    if cmd=="stop" and (state=="ready_local" or state=="error" or state=="paused"):
        #for p in ConnectCmdIDs:
        #    if p in remSubs:
        #        continue
        #    KIPTaskMan.Log( 2,  "Sending 'Disconnect' command to process "+p )
        #    KIPTaskMan.SendCommand( p, "Disconnect", 2000000 )
        for p in SourceIDs:
            if p in remSubs:
                continue
            KIPTaskMan.Log( 2,  "Sending 'StopSubscriptions' command to process "+p )
            KIPTaskMan.SendCommand( p, "StopSubscriptions", 2000000 )
        for p in ProcSinkIDs+ConnectCmdIDs+ConnectNoCmdIDs:
            if p in remSubs:
                continue
            KIPTaskMan.Log( 2,  "Sending 'StopProcessing' command to process "+p )
            KIPTaskMan.SendCommand( p, "StopProcessing", 2000000 )
        mode="stopping"
        state="stopping"
    if cmd=="start_run" and state=="ready":
        KIPTaskMan.SetVar( "RunStopCommandReceivedTimeoutExpireMessage", False )
        for p in SourceIDs:
            if p in remSubs:
                continue
            KIPTaskMan.Log( 2,  "Sending 'StartProcessing' command to process "+p )
            if len(parameters)>0:
                startCmd = "StartProcessing "+parameters[0]
            else:
                startCmd = "StartProcessing"
            KIPTaskMan.SendCommand( p, startCmd, 2000000 )
        for p in IDs:
            if p in remSubs:
                continue
            KIPTaskMan.SetProcessStateChanged( p )
        mode="starting_run"
        state="starting_run"
    if (cmd=="disconnect_prepare") and state=="ready":
        mode="ready_local_prepare"
        state="ready_local_prepare"
        if len(IDs)&gt;0:
            KIPTaskMan.SetProcessStateChanged( IDs[0] )
    if (cmd=="disconnect") and state=="ready_local_prepare":
        if False: # True
            for p in IDs:
                KIPTaskMan.Log( 2,  "Terminating process "+p )
                KIPTaskMan.SuppressComErrorLogs( p, 90000000 )
                try:
                    KIPTaskMan.Disconnect( p )
                except:
                    pass
                KIPTaskMan.PrepareProcessForTermination( p )
                KIPTaskMan.TerminateProcess( p )
            mode=""
        else:
            for p in ConnectCmdIDs:
                if p in remSubs:
                    continue
                KIPTaskMan.Log( 2,  "Sending 'Disconnect' command to process "+p )
                KIPTaskMan.SendCommand( p, "Disconnect", 2000000 )
            for p in ConnectNoCmdIDs:
                if p in remSubs:
                    continue
                KIPTaskMan.Log( 2,  "Sending 'NOP' command to process "+p )
                KIPTaskMan.SendCommand( p, "NOP", int(2000000*2.5) )
            mode="disconnecting"
            state="disconnecting"
        if len(ConnectCmdIDs)==0 and len(IDs)&gt;0:
            KIPTaskMan.SetProcessStateChanged( IDs[0] )
    if cmd=="pause" and (state=="running" or state=="busy"):
        for p in IDs:
            if p in remSubs:
                continue
            KIPTaskMan.Log( 2,  "Sending 'PauseProcessing' command to process "+p )
            KIPTaskMan.SendCommand( p, "PauseProcessing", 2000000 )
        state="pausing"
        mode="pausing"
    if (cmd=="stop_run") and (state=="running" or state=="paused" or state=="busy"):
        for p in SourceIDs:
            if p in remSubs:
                continue
            KIPTaskMan.Log( 2,  "Sending 'StopProcessing' command to process "+p )
            KIPTaskMan.SendCommand( p, "StopProcessing", 2000000 )
        for p in IDs:
            KIPTaskMan.Log( 2,  "Process "+p+" state '"+states[p]+"'" )
            if p in remSubs:
                continue
            if states[p].find( "Paused" )!=-1:
                KIPTaskMan.Log( 2,  "Sending 'ResumeProcessing' command to Process "+p )
                KIPTaskMan.SendCommand( p, "ResumeProcessing", 1000000 )
        mode="stopping_run"
        state="stopping_run"
        if len(SourceIDs)==0 and len(IDs)&gt;0:
            KIPTaskMan.SetProcessStateChanged( IDs[0] )
    if (cmd=="stop" or cmd=="disconnect") and (state=="running" or state=="paused"):
        for p in IDs:
            if p in remSubs:
                continue
            KIPTaskMan.Log( 2,  "Sending 'StopProcessing' command to process "+p )
            KIPTaskMan.SendCommand( p, "StopProcessing", 2000000 )
        if cmd=="stop":
            mode="stopping"
            state="stopping"
        else:
            mode="disconnecting"
            state="disconnecting"
    if cmd=="resume" and state=="paused":
        for p in IDs:
            if p in remSubs:
                continue
            KIPTaskMan.Log( 2,  "Sending 'ResumeProcessing' command to process "+p )
            KIPTaskMan.SendCommand( p, "ResumeProcessing", 2000000 )
        state="resuming"
        mode="resuming"
    if cmd=="proc_cmd":
        if len(parameters)&gt;0:
            p = parameters[0]
            proc_cmd = " ".join( parameters[1:] )
            KIPTaskMan.Log( 2,  "Sending command '"+proc_cmd+"' to process "+p )
            KIPTaskMan.SendCommand( p, proc_cmd, 2000000 )
    if cmd=="kill_proc":
        if len(parameters)&gt;0:
            p = parameters[0]
            KIPTaskMan.SuppressComErrorLogs( p, 90000000 )
            KIPTaskMan.PrepareProcessForTermination( p )
            KIPTaskMan.TerminateProcess( p )
    if cmd=="DUMPSTATES":
        stateHist = KIPTaskMan.GetVar( "StateHist" )
        if stateHist!=None:
            stStr = "Slave TM State History: "
            for i in range(0,3):
                if not stateHist.has_key(i):
                    continue
                stStr += "\n  State history %u" % i
                stStr += "\n    Time: '"+time.strftime( "%Y%m%d.%H%M%S",time.localtime(stateHist[i]["time"]) )+".%06u" % ((stateHist[i]["time"]-int(stateHist[i]["time"]))*1000000)
                stStr += "\n    Mode: '"+stateHist[i]["mode"]+"'"
                stStr += "\n    States: "
                for p in stateHist[i]["states"].keys():
                    stStr += "\n        "+p+" : "+stateHist[i]["states"][p]
                stStr += "\n    Vars: "
                for v in stateHist[i]["vars"].keys():
                    stStr += "\n        "+v+" : %d" % stateHist[i]["vars"][v]
                stStr += "\n    State: '"+stateHist[i]["state"]+"'"
        else:
            stStr = "No state history stored"
        KIPTaskMan.Log( 8, stStr )
    if cmd=="DumpEvents":
        for n in SourceIDs:
            KIPTaskMan.SendCommand( n, "DumpEvents" )
    if cmd=="DumpEventMetaData":
        for n in IDs:
            KIPTaskMan.SendCommand( n, "DumpEventMetaData" )
    if cmd=="DumpEventAccounting":
        for n in IDs:
            KIPTaskMan.SendCommand( n, "DumpEventAccounting" )
    if cmd=="SetVerbosity":
        KIPTaskMan.Log( 2, "Setting Verbosity to "+parameters[0] )
        try:
            verb = int(parameters[0],0)
            KIPTaskMan.SetVerbosity(verb)
        except ValueError:
            KIPTaskMan.Log( 0x10, "Error determining verbosity from '"+parameters[0]+"'" )
        for n in IDs:
            try:
                KIPTaskMan.SendCommand( n, fullcmd )
            except:
                pass
    if fullcmd.split(" ")[0]=="SetSubsystemVerbosity":
        KIPTaskMan.Log( 2, "Setting subsystemVerbosity to "+fullcmd.split(" ")[1] )
        for n in IDs:
            try:
                KIPTaskMan.SendCommand( n, fullcmd )
            except:
                pass
    if (cmd=="trigger_reconfigure_event" or cmd=="trigger_dcs_update_event") and (state=="running" or state=="busy" or state=="paused" or state=="ready" or state=="ready_local"):
        if cmd=="trigger_reconfigure_event":
            sendcmd="InitiateReconfigureEvent"
        elif cmd=="trigger_dcs_update_event":
            sendcmd="InitiateUpdateDCSEvent"
        else:
            sendcmd="NOP"
        if len(parameters)&gt;0:
            for n in SourceIDs:
                if states[n].find( "Started")!=-1:
                    KIPTaskMan.SendCommand( n, sendcmd+" "+" ".join( parameters ) )

    cmd = KIPTaskMan.GetTaskCommand()

KIPTaskMan.Log( 2,  "Slave Command Received Action setting Mode to "+mode )
KIPTaskMan.SetVar( "Mode", mode )

if mode!=oldmode:
    KIPTaskMan.SetVar( "LastModeChangeTime", now )
