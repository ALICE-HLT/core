remSubs = KIPTaskMan.GetVar( "RemoveScheduledSubscribers" )
if remSubs==None:
    remSubs=()

KIPTaskMan.Log( 2, "Remove scheduled subscribers: "+str(remSubs) )

if not "$(ID)" in remSubs:
    
    # Determine the current state of this process
    state = KIPTaskMan.QueryState( "$(ID)" )
    KIPTaskMan.Log( 2,  "Process $(ID) state: "+state )
    
    # Determine parent process state (Needed (potentially) later under these conditions)
    parentstate = KIPTaskMan.QueryState( "$(ParentID)" )
    KIPTaskMan.Log( 2,  "Parent process $(ParentID) state: "+parentstate )
    
    mode = KIPTaskMan.GetVar( "Mode" )
    if mode==None:
        mode = ""

    if mode=="stopping" and ( state.find( "Subscribed" )!=-1 or state.find( "With-Subscribers" )!=-1) and state.find( "Running" )==-1:
        KIPTaskMan.SendCommand( "$(ID)", "PURGEALLEVENTS" )


    if state=="TM:DEAD" or state=="TM:COULDNOTSTART":
        # Process is dead
        KIPTaskMan.Log( 2,  "Process $(ID) died" )
        #KIPTaskMan.SuppressComErrorLogs( "$(ID)", 90000000 )
        #KIPTaskMan.StartProcess( "$(ID)" )
    elif string.find( state, "Running" )!=-1 and string.find( state, "Changing-State" )==-1:
        # Everything ok so far, process is activated
        if mode=="stopping":
            KIPTaskMan.SendCommand( "$(ID)", "StopProcessing", 500000 )
        pass
    elif (mode=="stopping") and string.find( state, "Configured" )!=-1 and string.find( state, "Accepting-Subscriptions" )!=-1 and string.find( state, "Error" )==-1 and string.find( state, "Changing-State" )==-1:
        KIPTaskMan.Log( 2,  "Stopping subscriptions for process $(ID)" )
        KIPTaskMan.SendCommand( "$(ID)", "StopSubscriptions", 500000 )
    elif (mode=="stopping") and string.find( state, "Subscribed" )!=-1 and string.find( state, "Error" )==-1 and string.find( parentstate, "Error" )==-1 and string.find( state, "Changing-State" )==-1:
        KIPTaskMan.Log( 2,  "Unsubscribing process $(ID)" )
        KIPTaskMan.SendCommand( "$(ID)", "Unsubscribe", 500000 )
    elif (mode=="starting" or mode=="connecting" or mode=="starting_run" or mode=="stopping_run" or mode=="disconnecting") and string.find( state, "Accepting-Subscriptions" )!=-1 and string.find( state, "Subscribed" )!=-1 and string.find( state, "Error" )==-1 and string.find( parentstate, "Error" )==-1 and string.find( state, "Changing-State" )==-1:
        # Process is ready, subscribed, and accepting subscriptions and can be activated
        KIPTaskMan.Log( 2,  "Process $(ID) ready" )
        KIPTaskMan.SendCommand( "$(ID)", "StartProcessing", 500000 )
    elif string.find( state, "Configured" )!=-1 and string.find( state, "Accepting-Subscriptions" )==-1 and string.find( state, "Error" )==-1 and string.find( state, "Changing-State" )==-1:
        # Process is configured and ready to accept subscriptions
        if mode=="starting" or mode=="connecting" or mode=="starting_run" or mode=="stopping_run" or mode=="disconnecting":
            KIPTaskMan.Log( 2,  "Process $(ID) accepting subscriptions" )
            KIPTaskMan.SendCommand( "$(ID)", "AcceptSubscriptions", 500000 )
        if (mode=="stopping") and string.find( state, "With-Subscribers" )==-1:
            KIPTaskMan.Log( 2,  "Process $(ID) unconfiguring" )
            KIPTaskMan.SendCommand( "$(ID)", "Unconfigure", 500000 )
    elif string.find( state, "Started")!=-1 and string.find( state, "Changing-State" )==-1:
        KIPTaskMan.SetVar( "$(ID)Pinned", 0 )
        # Process is started and can be configured.
        if (mode=="starting" or mode=="connecting" or mode=="starting_run" or mode=="stopping_run" or mode=="disconnecting") and string.find( state, "Configured" )==-1 and string.find( state, "Error" )==-1:
            KIPTaskMan.Log( 2,  "Configuring process $(ID)" )
            params = KIPTaskMan.GetVar( "ConfigureParameters" )
            KIPTaskMan.Log( 2,  "DEBUG 0" )
            configparms = "$(CONFIGUREPARAMETERS)"
            args = { "BEAM_TYPE" : "-beamtype", 
                     "HLT_MODE" : "-hltmode", 
                     "RUN_NUMBER" : "-runnr", 
                     "RUN_TYPE" : "-runtype", 
                     "CTP_TRIGGER_CLASS" : "-ctptriggerclasses",
                     "DATA_FORMAT_VERSION" : "-readoutlistversion",
                     }
            for p in args.keys():
                if params.has_key(p) and params[p]!="":
                    configparms = configparms+" "+args[p]+" "+params[p]
            if configparms!="":
                configparms=" 0 0"+configparms
            KIPTaskMan.Log( 2,  "DEBUG 3" )
            KIPTaskMan.SendCommand( "$(ID)", "Configure"+configparms, 500000 )
            KIPTaskMan.SuppressComErrorLogs( "$(ID)", 0xFFFFFFFF )
            KIPTaskMan.Log( 2,  "DEBUG 4" )
        if (mode=="stopping") and string.find( state, "Configured" )==-1:
            KIPTaskMan.Log( 2,  "Ending process $(ID)" )
            KIPTaskMan.SuppressComErrorLogs( "$(ID)", 90000000 )
            KIPTaskMan.Disconnect( "$(ID)" )
            KIPTaskMan.PrepareProcessForTermination( "$(ID)" )
            KIPTaskMan.SendCommand( "$(ID)", "EndProgram", 500000 )
    
    
    if (mode=="starting" or mode=="connecting" or mode=="starting_run" or mode=="stopping_run" or mode=="disconnecting") and string.find( state, "Configured" )!=-1 and string.find( state, "Accepting-Subscriptions" )!=-1 and string.find( state, "Error" )==-1 and string.find( state, "Subscribed" )==-1 and string.find( parentstate, "Accepting-Subscriptions" )!=-1 and string.find( parentstate, "Error" )==-1 and string.find( state, "Changing-State" )==-1 and string.find( parentstate, "Changing-State" )==-1:
        # This process and its parent are ready to subscribe this process at its parent.
        KIPTaskMan.Log( 2,  "Subscribing $(ID) at $(ParentID)" )
        KIPTaskMan.SendCommand( "$(ID)", "Subscribe", 500000 )
    
    
    if string.find( parentstate, "Error" )!=-1 or (parentstate=="TM:DEAD" and mode!="" and mode!="stopping") or parentstate=="TM:COULDNOTSTART":
        # Something has changed here and our parent has an error, maybe our change was triggered by the parent,
        # so we inform it as well.
        KIPTaskMan.SetProcessStateChanged( "$(ParentID)" )
       
    pinned=0
    parentbusy=1
    parentpinned=0
    taskstate=""
    in_sub=0
    all_in_sub=1
    
    # Determine state of all child processes (Needed (potentially) later under these conditions)
    for c in children:
        childstates[c] = KIPTaskMan.QueryState( c )
        if string.find( childstates[c], "Subscribed" )!=-1:
            # At least one child is still subscribed
            in_sub = 1
        else:
            # At least one child is NOT subscribed
            all_in_sub = 0
    
    
    
    if (mode=="starting" or mode=="connecting" or mode=="starting_run" or mode=="stopping_run" or mode=="disconnecting") and string.find( state, "Accepting-Subscriptions" )!=-1:
        for c in children:
            addSubs = KIPTaskMan.GetVar( "SubscriberAdditions" )
            if addSubs==None:
                addSubs = {}
            KIPTaskMan.Log( 2, "$(ID) subscriber additions: "+str(addSubs) )
            if addSubs.has_key( "$(ID)" ):
                newPs=[]
                for p in addSubs["$(ID)"]:
                    if p[0]==c:
                        KIPTaskMan.SendCommand( c, "PauseProcessing" )
                        KIPTaskMan.SendCommand( c, "AddSubscriber "+p[1]+" "+p[2] )
                        KIPTaskMan.SendCommand( c, "ResumeProcessing" )
                    else:
                        newPs.append(p)
                if len(newPs)&gt;0:
                    addSubs["$(ID)"]=newPs
                else:
                    del(addSubs["$(ID)"])
                KIPTaskMan.SetVar( "SubscriberAdditions", addSubs )
            if string.find( childstates[c], "Subscribed" )==-1:
                KIPTaskMan.SetProcessStateChanged( c )
                #KIPTaskMan.SendCommand( c, "Subscribe", 500000 )
                
    
    if string.find( state, "Paused" )!=-1:
        for c in children:
            if string.find( childstates[c], "Error" )!=-1 or childstates[c]=="TM:DEAD" or childstates[c]=="TM:COULDNOTSTART":
                # Something changed here and this child has an error,
                # the change might have been triggered by the child,
                # so we inform it about the change.
                KIPTaskMan.SetProcessStateChanged( c )
    
    if string.find( state, "Error" )!=-1:
        # This process has an error. Before proceeding we first pin
        # those events that it currently has. (To avoid them being freed
        # when we later unsubscribe its children) (If they have not been
        # pinned already)
        KIPTaskMan.Log( 0x10,  "Process $(ID) has error" )
        pinned = KIPTaskMan.GetVar( "$(ID)Pinned" )
        if not pinned:
            # Pin the events now
            KIPTaskMan.Log( 4,  "Pinning events for process $(ID)" )
            KIPTaskMan.SendCommand( "$(ID)", "PublisherPinEvents", 500000 )
            KIPTaskMan.SetVar( "$(ID)Pinned", 1 )
            pinned=1
    
    
    KIPTaskMan.Log( 2,  "(1) Process $(ID) state: "+state )
    if (state=="TM:DEAD" and mode!="" and mode!="stopping") or state=="TM:COULDNOTSTART" or string.find( state, "Error" )!=-1:
        # Process either is not active or has an error.
        # Collect some information about parent process
        KIPTaskMan.Log( 2,  "(2) Process $(ID) state: "+state+" / parent $(ParentID) state: "+parentstate )
        if string.find( parentstate, "Paused" )!=-1 and string.find( parentstate, "Changing-State" )==-1:
            KIPTaskMan.Log( 2,  "(2) Parent $(ParentID) is NOT busy" )
            parentbusy=0
        parentpinned=KIPTaskMan.GetVar( "$(ParentID)Pinned" )
        if parentpinned==None:
            parentpinned=0
        taskstate = KIPTaskMan.GetVar( "State" )
        if taskstate==None:
            taskstate=""
    
        # Determine what to do with our child processes.
        for c in children:
            if string.find( childstates[c], "Busy" )==-1 and string.find( childstates[c], "Running")!=-1 and string.find( childstates[c], "Changing-State" )==-1:
                # Process is still running. Processing has to be deactivated before we can proceed.
                KIPTaskMan.Log( 4,  "Stopping $(ID) child process "+c+" processing" )
                KIPTaskMan.SendCommand( c, "StopProcessing", 500000 )
            if string.find( childstates[c], "Running")==-1 and string.find( childstates[c], "Subscribed" )!=-1 and string.find( childstates[c], "Changing-State" )==-1:
                # Child process can be unsubscribed
                KIPTaskMan.Log( 4,  "Unsubscribing $(ID) child process "+c+" processing" )
                if state=="TM:DEAD" or state=="TM:COULDNOTSTART":
                    KIPTaskMan.SendCommand( c, "AbortSubscription", 500000 )
                else:
                    KIPTaskMan.SendCommand( c, "Unsubscribe", 500000 )
    
        if parentbusy:
            # Parent is still processing, we have to pause it.
            KIPTaskMan.Log( 4,  "Pausing $(ID) parent $(ParentID)" )
            KIPTaskMan.SendCommand( "$(ParentID)", "PauseProcessing", 500000 )
        if not parentpinned:
            # Parent's events are not pinned yet. We will pin it, in case we have to unsubscribe
            # to avoid losing events
            KIPTaskMan.Log( 4,  "Pinning events for $(ID) parent process $(ParentID)" )
            KIPTaskMan.SendCommand( "$(ParentID)", "PublisherPinEvents", 500000 )
            KIPTaskMan.SetVar( "$(ParentID)Pinned", 1 )
            parentpinned=1
    
    
    if (state=="TM:DEAD" and mode!="" and mode!="stopping") or state=="TM:COULDNOTSTART":
        # This process has died.
        KIPTaskMan.Log( 0x10,  "Process $(ID) died" )
        #KIPTaskMan.SuppressComErrorLogs( "$(ID)", 90000000 )
        #KIPTaskMan.StartProcess( "$(ID)" )
        if parentpinned and not parentbusy and not in_sub and mode!="stopping":
            # If events in parent process are pinned and it is not busy,
            # we can tell it to remove this process from its subscription list.
            # No child process still thinks it is subscribed, so we can now safely
            # restart this process.
            KIPTaskMan.Log( 4,  "Removing dead process $(ID) from parent $(ParentID)" )
            KIPTaskMan.SendCommand( "$(ParentID)", "RemoveSubscriber 0 0 $(ID)Sub", 500000 )
            KIPTaskMan.Log( 4,  "Restarting dead process $(ID)" )
            KIPTaskMan.SuppressComErrorLogs( "$(ID)", 90000000 )
            #### XXXX REENABLE AND FIX BEHAVIOUR
            #### Also look in TaskManager-Slave-StateChangeAction-Main-Template.py
            #### KIPTaskMan.StartProcess( "$(ID)" )
    
    if string.find( state, "Error" )!=-1:
        # This process had an error earlier.
        if not in_sub and not parentbusy and parentpinned:
            # if no child processes are still subscribed, and the parent
            # is not still busy processing events, and the parent's events
            # are pinned, then we can start to slowly back down until
            # we are error free.
            if string.find( state, "Running" )!=-1 and string.find( state, "Changing-State" )==-1:
                # If we are still running, we stop.
                KIPTaskMan.Log( 4,  "Stopping processing in process $(ID)" )
                KIPTaskMan.SendCommand( "$(ID)", "StopProcessing", 500000 )
            if string.find( state, "Running" )==-1 and string.find( state, "With-Subscribers" )!=-1 and string.find( state, "Changing-State" )==-1:
                # If we still have some subscriptions lingering (should not be real, all our children
                # have unsubscribed (see above), or configuration error), we cancel all of them
                KIPTaskMan.Log( 4,  "Cancelling subscriptions to process $(ID)" )
                KIPTaskMan.SendCommand( "$(ID)", "CancelSubscriptions", 500000 )
            if string.find( state, "Running" )==-1 and string.find( state, "With-Subscribers" )==-1 and string.find( state, "Accepting-Subscriptions" )!=-1 and string.find( state, "Changing-State" )==-1:
                # If we would accept new subscriptions we stop this now.
                KIPTaskMan.Log( 4,  "Stopping subscriptions to process $(ID)" )
                KIPTaskMan.SendCommand( "$(ID)", "StopSubscriptions", 500000 )
            if string.find( state, "Running" )==-1 and string.find( state, "Subscribed" )!=-1 and string.find( state, "Changing-State" )==-1:
                if string.find( parentstate, "Error" )==-1 and parentstate!="TM:DEAD" and parentstate!="TM:COULDNOTSTART":
                    # We unsubscribe ourselves now from our parent
                    KIPTaskMan.Log( 4,  "Unsubscribing process $(ID)" )
                    KIPTaskMan.SendCommand( "$(ID)", "Unsubscribe", 500000 )
                else:
                    # We now abort the subscription to our parent
                    KIPTaskMan.Log( 4,  "Aborting subscription of process $(ID)" )
                    KIPTaskMan.SendCommand( "$(ID)", "AbortSubscription", 500000 )
            if string.find( state, "Accepting-Subscriptions" )==-1 and string.find( state, "Subscribed" )==-1 and string.find( state, "Configured" )!=-1 and string.find( state, "Changing-State" )==-1:
                # If we are configured and still have an error (that's why we are here), then we have a more sever problem.
                # To ensure proper cleanup we now, after all, have to unpin our events, so that they can be cleaned up.
                if pinned:
                    KIPTaskMan.Log( 4,  "Unpinning events for process $(ID) before unconfiguration" )
                    KIPTaskMan.SendCommand( "$(ID)", "PublisherUnpinEvents", 500000 )
                    KIPTaskMan.SetVar( "$(ID)Pinned", 0 )
                    pinned=0
                    # And then we can unconfigure ourselves.
                KIPTaskMan.Log( 4,  "Unconfiguring process $(ID)" )
                KIPTaskMan.SendCommand( "$(ID)", "Unconfigure", 500000 )
            if string.find( state, "Configured" )==-1 and string.find( state, "Changing-State" )==-1:
                # If we are unconfigured and still have an error, then we try a restart.
                KIPTaskMan.Log( 4,  "Terminating process $(ID)" )
                KIPTaskMan.SuppressComErrorLogs( "$(ID)", 90000000 )
                KIPTaskMan.PrepareProcessForTermination( "$(ID)" )
                KIPTaskMan.TerminateProcess( "$(ID)" )
    else:
        # Process has no more error.
        pinned = KIPTaskMan.GetVar( "$(ID)Pinned" )
        if all_in_sub and pinned:
            # All children are subscribed again (or still) and our events are pinned,
            # so we can safely unpin them now.
            KIPTaskMan.Log( 4,  "Unpinning events for process $(ID)" )
            KIPTaskMan.SendCommand( "$(ID)", "PublisherUnpinEvents", 500000 )
            KIPTaskMan.SetVar( "$(ID)Pinned", 0 )
            pinned=0
        if string.find( state, "Subscribed" )!=-1 and string.find( state, "Changing-State" )==-1:
            # We are again subscribed to our parent.
            parentpinned=KIPTaskMan.GetVar( "$(ParentID)Pinned" )
            if string.find( parentstate, "Paused" )!=-1 and string.find( parentstate, "Changing-State" )==-1:
                KIPTaskMan.Log( 2,  "(2) Parent $(ParentID) is NOT busy" )
                parentbusy=0
            if parentpinned==None:
                parentpinned=0
            if parentpinned:
                # Parent events can now be safely unpinned as well.
                KIPTaskMan.Log( 4,  "Unpinning events for $(ID) parent process $(ParentID)" )
                KIPTaskMan.SendCommand( "$(ParentID)", "PublisherUnpinEvents", 500000 )
                KIPTaskMan.SetVar( "$(ParentID)Pinned", 0 )
                parentpinned=0
            if not parentbusy and auto_resume:
                # And parent can now be safely restarted as well.
                KIPTaskMan.Log( 4,  "Unpausing $(ID) parent $(ParentID)" )
                KIPTaskMan.SendCommand( "$(ParentID)", "ResumeProcessing", 500000 )
                
                
    if (mode=="starting" or mode=="connecting" or mode=="starting_run" or mode=="stopping_run" or mode=="disconnecting") and string.find( state, "Accepting-Subscriptions" )!=-1 and string.find( state, "Error" )==-1 and string.find( state, "Changing-State" )==-1:
        # We are ready to accept subscriptions and have no error,
        # check our children if each of these can be subscribed
        inlawsok=1
        for i in inlaws:
            inlawstate=KIPTaskMan.QueryState( i )
            if string.find( inlawstate,  "Accepting-Subscriptions" )==-1 or string.find( inlawstate, "Error" )!=-1 or string.find( inlawstate, "Changing-State" )!=-1:
                inlawsok=0
        if inlawsok:
            for c in children:
                if string.find( childstates[c], child_subscribe_keyword[c] )!=-1 and string.find( childstates[c], "Error" )==-1 and string.find( childstates[c], "Configured" )!=-1 and string.find( childstates[c], "Subscribed" )==-1 and string.find( childstates[c], "Changing-State" )==-1:
                    # This child is ready to subscribe to us, so we instruct it to do so.
                    KIPTaskMan.Log( 2, "Subscribing "+c+" at $(ID)" )
                    KIPTaskMan.SetProcessStateChanged( c )
                    # KIPTaskMan.SendCommand( c, "Subscribe", 500000 )
                
     
if mode=="stopping" and string.find( state, "Accepting-Subscriptions" )==-1 and string.find( state, "With-Subscribers" )!=-1:
    if in_sub:
        KIPTaskMan.SendCommand( "$(ID)", "NOP", 250000 ) # just to trigger run through state change action again...
    else:
        KIPTaskMan.SendCommand( "$(ID)", "CancelSubscriptions", 1000000 )
