
IDs = SourceIDs + ProcSinkIDs + ConnectNoCmdIDs + ConnectCmdIDs
ConnectIDs = ConnectNoCmdIDs + ConnectCmdIDs

remSubs = KIPTaskMan.GetVar( "RemoveScheduledSubscribers" )
if remSubs==None:
    remSubs=()


states = {}
lastGoodProcessStates = KIPTaskMan.GetVar( "LastGoodProcessStates" )
if lastGoodProcessStates==None:
    lastGoodProcessStates = {}
    for p in IDs:
        lastGoodProcessStates[p] = ""
blankStates=False
blankStateIDList = []
for p in IDs:
    if p in remSubs:
        states[p] = ""
        continue
    cnt=0
    states[p] = ""
    while states[p]=="" and cnt&lt;3:
        states[p] = KIPTaskMan.QueryState( p )
        cnt += 1
    if states[p]=="" and (lastGoodProcessStates[p]!="" and lastGoodProcessStates[p][:3]!="TM:"):
        blankStates=True
        blankStateIDList.append( p )
    if mode=="stopping" and (states[p]=="" or states[p][:3]=="TM:"):
        KIPTaskMan.SuppressComErrorLogs( p, 90000000 )
        KIPTaskMan.Disconnect( p ) # Process might or seems to be dead already
    KIPTaskMan.Log( 2,  "Process "+p+" state: "+states[p] )

oldState = KIPTaskMan.GetVar( "State" )

if blankStates:
    blankStateCounter=KIPTaskMan.GetVar( "StateChangeActionBlankStateCounter" )
    if blankStateCounter==None:
        blankStateCounter=0
    blankStateCounter += 1
    #if blankStateCounter>10:
    #    blankStates=False
    #    blankStateCounter=0
    if (blankStateCounter % 20)==0:
        KIPTaskMan.Log( 8,  "Unable to correctly read states from all subordinate processes after %d tries; State determination logic may be unreliable" % blankStateCounter )
        idStr = blankStateIDList[0]
        for bsi in blankStateIDList[1:]:
            idStr += ", "+bsi
        KIPTaskMan.Log( 8, "Subordinates with blank states: "+idStr )
        blankStates=False
else:
    blankStateCounter=0
KIPTaskMan.SetVar( "StateChangeActionBlankStateCounter", blankStateCounter )

if not blankStates:

    if mode=="stopping":
        killedProcess = 0
        now=time.time()
        cmdRecv = KIPTaskMan.GetVar( "CommandReceived" )
        if cmdRecv == None:
            cmdRecv = now
        tdiff = float(now-cmdRecv)*1000.0 # Convert to millisecs.
        if $(STOPTIMEOUT)!=0 and tdiff&gt;$(STOPTIMEOUT):
            for p in IDs:
                if states[p][:3]!="TM:":
                    crtem = KIPTaskMan.GetVar( "StopCommandReceivedTimeoutExpireMessage" )
                    if crtem==None or crtem==False:
                        KIPTaskMan.Log( 0x10, "Stop timeout has expired after %f millisecs." % tdiff )
                        KIPTaskMan.SetVar( "StopCommandReceivedTimeoutExpireMessage", True )
                    break
            for p in IDs:
                if states[p][:3]!="TM:":
                    KIPTaskMan.Log( 2,  "Forcefully terminating process "+p )
                    KIPTaskMan.SuppressComErrorLogs( p, 90000000 )
                    try:
                        KIPTaskMan.Disconnect( p )
                    except:
                        pass
                    KIPTaskMan.PrepareProcessForTermination( p )
                    KIPTaskMan.TerminateProcess( p )
                    states[p] = "TM:NOTSTARTED"
                    procStarted[p] = 0
                    lastGoodProcessStates[p] = "TM:NOTSTARTED"
                    killedProcess = 1
        #No longer needed, we reset all slave taskmanagers when they have reached dead state after stopping proceses anyway
        #if killedProcess:
        #    KIPTaskMan.Log( 0x8, "Slave processes terminated, reinitializing TaskManager" )
        #    KIPTaskMan.Reset()


    empty = 0
    dead = 1
    error = 0
    busy = 0
    paused = 0
    running = 1
    ready = 1
    connected = 1
    changing = 0
    network_error = 0
    started = 0
    
    procStarted = KIPTaskMan.GetVar( "ProcStart" )
    if procStarted == None:
        procStarted = {}
    
    stateHist = KIPTaskMan.GetVar( "StateHist" )
    if stateHist==None:
        stateHist = {}
    oldStates = 2
    for i in range( oldStates, 0,-1):
        if stateHist.has_key(i-1):
            stateHist[i] = stateHist[i-1]
    
    #if stateHist.has_key(1):
    #    statehist[2] = stateHist[1]
    #if stateHist.has_key(0):
    #    statehist[1] = stateHist[0]
    stateHist[0] = { "states":states,
                     "vars":{},
                     "mode":"",
                     "state":"",
                     "time":time.time(),
                     }
    
    
    mode = KIPTaskMan.GetVar( "Mode" )
    if mode==None:
        mode = ""
    
    if len(IDs)==0:
        empty = 1
        
    for p in IDs:
        if p in remSubs:
            continue
        if states[p]=="":
            if (mode=="stopping" and oldState=="dead") or (mode in ( "starting", "disconnecting" ) and oldState=="ready_local") or (mode=="ready_local_prepare" and state=="ready_local_prepare") or (mode in( "connecting", "stopping_run" ) and oldState=="ready") or (mode in ( "starting_run", "resuming" ) and oldState in ( "running", "busy" )) or (mode=="pausing" and oldState=="paused"):
                continue
            running = 0
            ready = 0
            continue
        if string.find( states[p], "Error" )!=-1:
            KIPTaskMan.Log( 0x10, "Process "+p+" has an error." )
            error = 1
        if states[p]=="TM:DEAD" and mode!="stopping" and mode!="":
        	KIPTaskMan.TaskInterrupt("DumpEvents")
        if (states[p]=="TM:DEAD" or states[p]=="TM:COULDNOTSTART" or states[p]=="TM:NOTSTARTED") and mode!="stopping" and mode!="" and mode != "dead" and not (mode == "starting" and states[p] == "TM:NOTSTARTED"):
            KIPTaskMan.Log( 0x10, "Process "+p+" is dead (B)." )
            if not procStarted.has_key(p):
                procStarted[p] = 0
            maxStartTries = 1 # was 10
            #### XXXX REENABLE AND FIX BEHAVIOUR
            #### Also look in TaskManager-Slave-Process-StateChangeAction-prc-Main-Template.py and potentially the other TaskManager-Slave-Process-StateChangeAction-*-Main-Template.py files.
            #### if procStarted[p]&lt;=maxStartTries: # was 10
            if (mode=="starting") and procStarted[p]&lt;=maxStartTries and lastGoodProcessStates[p].find( "Subscribed" )==-1 and lastGoodProcessStates[p].find( "With-Subscribers" )==-1 and lastGoodProcessStates[p].find( "Accepting-Subscriptions" )==-1: # was 10
                KIPTaskMan.SuppressComErrorLogs( p, 90000000 )
                KIPTaskMan.StartProcess( p )
                procStarted[p] = procStarted[p]+1
            elif mode!="starting" or lastGoodProcessStates[p].find( "Subscribed" )!=-1 or lastGoodProcessStates[p].find( "With-Subscribers" )!=-1 or lastGoodProcessStates[p].find( "Accepting-Subscriptions" )!=-1:
                KIPTaskMan.Log( 0x10, "Dead process %s was already connected to other process. Not trying to restart..." % ( p ) )
                error = 1
            else:
                KIPTaskMan.Log( 0x10, "Attempted %d times to start up and connect process %s. Giving up..." % ( maxStartTries, p ) )
                error = 1
        # if states[p][:3]!="TM:" and states[p]!="":
        #     procStarted[p] = 0
        if string.find( states[p], "Paused" )!=-1:
            paused = 1
        if string.find( states[p], "Busy" )!=-1:
            busy = 1
        if string.find( states[p], "Running" )==-1:
            running = 0
        else:
            procStarted[p] = 0
        if string.find( states[p], "TM:" )==-1 and states[p]!="":
            dead = 0
            started = 1
        if string.find( states[p], "Changing-State" )!=-1:
            changing = 1
    
    for p in ProcSinkIDs+ConnectIDs:
        if p in remSubs:
            continue
        if string.find( states[p], "Running" )==-1:
            ready = 0
    
    for p in SourceIDs:
        if p in remSubs:
            continue
        if string.find( states[p], "Accepting-Subscriptions" )==-1:
            ready = 0
    
        
    
    for p in ConnectIDs:
        if p in remSubs:
            continue
        if states[p]=="":
            connected = 0
            continue
        if string.find( states[p], "Connected" )==-1:
            connected = 0
        if (string.find( states[p], "Error" )!=-1 or states[p]=="TM:DEAD" or states[p]=="TM:COULDNOTSTART") and mode!="stopping" and mode!="":
            KIPTaskMan.Log( 0x10, "Process "+p+" is dead (C)." )
            network_error = 1
    
    if len(ConnectIDs)==0 and connected:
        if mode=="starting" or mode=="disconnecting":
            connected = 0

    if busy and mode!="stopping_run" and mode!="starting_run" and mode!="resuming" and mode!="pausing":
        busy = 0
        #running = 1
        
    
    if empty:
        state = "TM:EMPTY"
    elif dead:
        state = "dead"
        if mode=="stopping":
            procStarted = None
    elif error:
        state = "error"
    elif busy and (mode=="starting_run" or mode=="resuming" or mode=="pausing" or mode=="stopping_run"):
        if mode=="stopping_run":
            now=time.time()
            cmdRecv = KIPTaskMan.GetVar( "CommandReceived" )
            if cmdRecv == None:
                cmdRecv = now
            tdiff = float(now-cmdRecv)*1000.0 # Convert to millisecs.
            if $(STOPRUNTIMEOUT)!=0 and tdiff&gt;$(STOPRUNTIMEOUT):
                crtem = KIPTaskMan.GetVar( "RunStopCommandReceivedTimeoutExpireMessage" )
                if crtem==None or crtem==False:
                    KIPTaskMan.Log( 0x10, "Run Stop timeout has expired after %f millisecs." % tdiff )
                    KIPTaskMan.SetVar( "RunStopCommandReceivedTimeoutExpireMessage", True )
                state = "ready"
            else:
                state = "stopping_run"
        else:
            state = "busy"
            
    elif paused:
        if mode=="stopping_run":
            state = "stopping_run"
        else:
            state = "paused"
    elif running and connected and (mode=="starting_run" or mode=="resuming" or mode=="pausing"):
        if changing:
            #if mode=="starting_run":
            #    state = "starting_run"
            #elif mode=="pausing":
            #    state = "pausing"
            #elif mode=="resuming":
            #    state = "resuming"
            #else:
            #    state = "stopping_run"
            state = mode
        else:
            state = "running"
    elif running and connected and (mode=="connecting" or mode=="stopping_run"):
        if changing:
            #if mode=="connecting":
            #    state = "connecting"
            #else:
            #    state = "stopping_run"
            state = mode
        else:
            state = "ready"
    elif running and connected and (mode=="ready_local_prepare" ):
        state = "ready_local_prepare"
    elif running and connected and (mode=="starting" ):
        if changing:
            #if mode=="starting":
            #    state = "starting"
            #else:
            #    state = "disconnecting"
            state = mode
        else:
            state = "ready"
    elif running and connected and (mode=="disconnecting" ):
        state = mode
    elif running:
        if changing:
            #if mode=="starting":
            #    state = "starting"
            #else:
            #    state = "stopping"
            state = mode
        elif mode=="stopping_run":
            state = "ready"
        elif mode=="ready_local_prepare":
            state = "ready_local_prepare"
        else:
            state = "ready_local"
    elif connected and ready:
        if changing:
            #if mode=="starting_run" or mode=="connecting":
            #    state = "connecting"
            #else:
            #    state = "disconnecting"
            state = mode
        elif mode=="ready_local_prepare":
            state = "ready_local_prepare"
        else:
            state = "ready"
    elif ready:
        if changing:
            #if mode=="starting":
            #    state = "starting"
            #elif mode=="connecting" or mode=="starting_run":
            #    state = "connecting"
            #else:
            #    state = "stopping"
            state = mode
        elif mode=="stopping_run":
            state = "ready"
        elif mode=="ready_local_prepare":
            state = "ready_local_prepare"
        else:
            state = "ready_local"
    elif started:
        state = mode
        #if mode=="starting" or mode=="connecting" or mode=="starting_run":
        #    state = "starting"
        #else:
        #    state = "stopping"
    
    KIPTaskMan.Log( 2,  "state: "+state+" - mode: "+mode+" - empty: %d - dead: %d - ready: %d - connected: %d - changing: %d - running: %d - paused: %d - busy: %d - started: %d - network_error: %d - error: %d" %
                    (empty, dead, ready, connected, changing, running, paused, busy, started, network_error, error ))


    for p in IDs:
        if states[p][:3]!="TM:" and states[p]!="":
            lastGoodProcessStates[p] = states[p]
    KIPTaskMan.SetVar( "LastGoodProcessStates", lastGoodProcessStates )
    
    KIPTaskMan.SetVar( "State", state )
    KIPTaskMan.SetTaskState( state )
    
    KIPTaskMan.SetVar( "ProcStart", procStarted )
    
    stateHist[0]["vars"] = {
    "empty" : empty,
    "dead" : dead,
    "ready" : ready,
    "running" : running,
    "paused" : paused,
    "busy" : busy,
    "error" : error,
    "connected" : connected,
    "changing" : changing,
    "network_error" : network_error,
    "started" : started,
    }
    stateHist[0]["mode"] = mode
    stateHist[0]["state"] = state

    KIPTaskMan.SetVar( "StateHist", stateHist )

    if dead:
        if mode=="stopping":
            KIPTaskMan.Log( 0x2, "All processes stopped. Reinitializing slave TaskManager as precaution." )
            KIPTaskMan.Reset()
            KIPTaskMan.SetVar("Mode", "dead")
