states = {}
blankStates=False
blankStateIDList = []
oldState = KIPTaskMan.GetVar( "State" )
mode = KIPTaskMan.GetVar( "Mode" )
if mode==None:
    mode = ""
    
for p in IDs:
    cnt=0
    states[p] = ""
    while states[p]=="" and cnt&lt;3:
        states[p] = KIPTaskMan.QueryState( p )
        cnt += 1
    if states[p]=="" and not (mode=="starting_slaves" and not oldState in ( "slaves_dead", "starting_slaves" ) ):
        # KIPTaskMan.Log( 64,  "Process "+p+" state: '"+str(states[p])+"' - mode: '"+str(mode)+"' - oldState: '"+str(oldState)+"'" )
        blankStates=True
        blankStateIDList.append( p )
    KIPTaskMan.Log( 2,  "Process "+p+" state: "+states[p] )


if blankStates:
    blankStateCounter=KIPTaskMan.GetVar( "StateChangeActionBlankStateCounter" )
    if blankStateCounter==None:
        blankStateCounter=0
    blankStateCounter += 1
    #if blankStateCounter&gt;10:
    #    blankStates=False
    #    blankStateCounter=0
    if (blankStateCounter % 20)==0 and mode!="":
        KIPTaskMan.Log( 8,  "Unable to correctly read states from all subordinate TaskManagers after %d tries; State determination logic may be unreliable" % blankStateCounter )
        idStr = blankStateIDList[0]
        for bsi in blankStateIDList[1:]:
            idStr += ", "+bsi
        KIPTaskMan.Log( 8, "Subordinates with blank states: "+idStr )
        blankStates=False
else:
    blankStateCounter=0
KIPTaskMan.SetVar( "StateChangeActionBlankStateCounter", blankStateCounter )
    

    
    
if not blankStates:
    empty = 1
    notstarted = 0
    dead = 1
    slaves_dead = 1
    starting = 0
    ready_local = 1
    connecting = 0
    ready = 1
    starting_run = 0
    running = 1
    paused = 0
    busy = 0
    stopping_run = 0
    disconnect_preparing = 0
    ready_local_prepare = 1
    disconnecting = 0
    stopping = 0
    error = 0
    ready_local_cnt = 0
    ready_cnt = 0
    
    procStarted = KIPTaskMan.GetVar( "ProcStart" )
    if procStarted == None:
        procStarted = {}
    
    stateHist = KIPTaskMan.GetVar( "StateHist" )
    if stateHist==None:
        stateHist = {}
    oldStates = 2
    for i in range( oldStates, 0,-1):
        if stateHist.has_key(i-1):
            stateHist[i] = stateHist[i-1]
    
    #if stateHist.has_key(1):
    #    statehist[2] = stateHist[1]
    #if stateHist.has_key(0):
    #    statehist[1] = stateHist[0]
    stateHist[0] = { "states":states,
                     "vars":{},
                     "mode":"",
                     "state":"",
                     "time":time.time(),
                     }
    
    
    for p in IDs:
    ##    if string.find( states[p], "TM:" )!=-1 and mode!="slave_kill":
    ##        KIPTaskMan.SuppressComErrorLogs( p, 90000000 )
    ##        KIPTaskMan.StartProcess( p )
    ##        KIPTaskMan.Log( 2,  "Starting slave process "+p )
    ##        notstarted = 1
        if states[p]=="TM:EMPTY" or states[p]=="processes_empty":
            continue
        if states[p]=="":
            if (mode in ("starting_slaves", "stopping") and oldState=="processes_dead") or (mode in ( "starting", "disconnecting" ) and oldState=="ready_local") or (mode in( "connecting", "stopping_run" ) and oldState=="ready") or (mode in ( "starting_run", "resuming" ) and oldState in ( "running", "busy" )) or (mode=="pausing" and oldState=="paused"):
                continue
            empty = 0
            dead = 0
            slaves_dead = 0
            ready_local = 0
            ready = 0
            running = 0
            continue
        empty = 0
        if (states[p][:3]=="TM:") and mode!="slave_kill" and mode!="":
            dead=0
            slaves_dead=0
            if states[p]!="TM:READY":
                KIPTaskMan.Log( 0x10, "Process "+p+" is dead. (D)" )
                if not procStarted.has_key(p):
                    procStarted[p] = 0
                if procStarted[p]&lt;=1: ## XXXX FixMe, was 10
                    KIPTaskMan.SuppressComErrorLogs( p, 90000000 )
                    KIPTaskMan.StartProcess( p )
                    procStarted[p] = procStarted[p]+1
                else:
                    error = 1
        if states[p][:3]!="TM:":
            procStarted[p] = 0
        if string.find( states[p], "TM:" )==-1: # and states[p]!="slaves_dead":
            slaves_dead = 0
        if states[p] != "dead" and states[p] != "processes_dead" and states[p]!="TM:EMPTY" and states[p]!="TM:READY":
            dead = 0
        if states[p]!="TM:NOTSTARTED" and states[p]!="TM:READY":
            notstarted = 0
        #if (states[p]=="processes_dead" or states[p]=="dead") and mode=="slave_kill":
        if mode=="slave_kill" and states[p][:3]!="TM:":
            KIPTaskMan.SuppressComErrorLogs( p, 30000000 )
            KIPTaskMan.PrepareProcessForTermination( p )
            KIPTaskMan.SendCommand( p, "kill_slaves", 500000 ) # 2000000
        if states[p]=="slaves_dead" and (mode=="starting" or mode=="connecting" or mode=="starting_run"):
            KIPTaskMan.SendCommand( p, "start_slaves", 2000000 )
        if (states[p]=="processes_dead" or states[p]=="dead") and (mode=="starting" or mode=="connecting" or mode=="starting_run"):
            parameters = KIPTaskMan.GetVar( "ConfigureParameters" )
            if parameters == None:
                parameters = []
            KIPTaskMan.SendCommand( p, "start "+string.join(parameters), 2000000 )
        if states[p]=="ready_local" and (mode=="connecting" or mode=="starting_run"):
            KIPTaskMan.SendCommand( p, "connect", 2000000 )
        if states[p]=="ready" and mode=="starting_run":
            KIPTaskMan.SendCommand( p, "start_run", 2000000 )
        if states[p]=="ready" and (mode=="disconnect_preparing" or mode=="disconnecting"):
            KIPTaskMan.SendCommand( p, "disconnect_prepare", 2000000 )
        if states[p]=="ready_local_prepare" and mode=="disconnecting":
            KIPTaskMan.SendCommand( p, "disconnect", 2000000 )
        if states[p]=="ready_local" and mode=="stopping":
            KIPTaskMan.SendCommand( p, "stop", 2000000 )
        if states[p] == "starting":
            starting = 1
        if states[p] != "ready_local":
            ready_local = 0
        if states[p] == "ready_local":
            ready_local_cnt += 1
        if states[p] != "ready_local_prepare":
            ready_local_prepare = 0
        if states[p] == "connecting":
            connecting = 1
        if states[p] != "ready":
            ready = 0
        if states[p] == "ready":
            busy = 0
            ready_cnt += 1
        if states[p] == "starting_run":
            starting_run = 1
        if states[p] != "running":
            running = 0
        if states[p] == "paused":
            paused = 1
        if states[p] == "busy":
            busy = 1
        if states[p] == "stopping_run":
            stopping_run = 1
        if states[p] == "disconnect_preparing":
            disconnect_preparing = 1
        if states[p] == "disconnecting":
            disconnecting = 1
        if states[p] == "stopping":
            stopping = 1
        if states[p] == "error":
            KIPTaskMan.Log( 0x10, "Process "+p+" is in error state." )
            error = 1

    if slaves_dead:
        state = "slaves_dead"
    elif empty:
        state = "processes_empty"
    elif dead:
        state = "processes_dead"
    elif error:
        state = "error"
    elif busy:
        state = "busy"
    elif paused:
        state = "paused"
    elif running:
        state = "running"
    elif starting_run:
        state = "starting_run"
    elif stopping_run:
        state = "stopping_run"
    elif ready:
        state = "ready"
    elif connecting:
        state = "connecting"
    elif disconnect_preparing:
        state = "disconnect_preparing"
    elif ready_local_prepare:
        state = "ready_local_prepare"
    elif disconnecting:
        state = "disconnecting"
    elif ready_local:
        state = "ready_local"
    elif stopping:
        state = "stopping"
    elif starting:
        state = "starting"
    else:
        state = mode
        if mode=="stopping_run" and (ready_cnt+ready_local_cnt==len(IDs)):
            state = "ready"
        #state = "unknown"
    
    KIPTaskMan.Log( 2,  "state: "+state+" - dead: %d - starting: %d - ready_local: %d - connecting: %d - ready: %d - starting_run: %d - running: %d - paused: %d - busy: %d - stopping_run: %d - disconnecting: %d - stopping: %d - error: %d" %
                    (dead, starting, ready_local, connecting, ready, starting_run, running, paused, busy, stopping_run, disconnecting, stopping, error ))
    
    
    if slaves_dead and mode=="slave_kill":
        KIPTaskMan.TerminateTask()
    

        
    KIPTaskMan.SetVar( "State", state )
    KIPTaskMan.SetTaskState( state )
    
    KIPTaskMan.SetVar( "ProcStart", procStarted )
    
    stateHist[0]["vars"] = {
    "empty" : empty,
    "notstarted" : notstarted,
    "dead" : dead,
    "slaves_dead" : slaves_dead,
    "starting" : starting,
    "ready_local" : ready_local,
    "connecting" : connecting,
    "ready" : ready,
    "starting_run" : starting_run,
    "running" : running,
    "paused" : paused,
    "busy" : busy,
    "stopping_run" : stopping_run,
    "disconnecting" : disconnecting,
    "stopping" : stopping,
    "error" : error,
    }
    stateHist[0]["mode"] = mode
    stateHist[0]["state"] = state
    
    KIPTaskMan.SetVar( "StateHist", stateHist )
    
