KIPTaskMan.Log( 2,  "Command Received" )
cmd = KIPTaskMan.GetTaskCommand()

state = KIPTaskMan.GetVar( "State" )
mode = KIPTaskMan.GetVar( "Mode" )
triggeredEvents = KIPTaskMan.GetVar( "TriggeredEvents" )
if triggeredEvents==None:
    triggeredEvents = []

while cmd!=None:
    KIPTaskMan.Log( 2,  "Full command "+cmd )
    fullcmd = cmd
    arguments = cmd.split(";")
    cmd = arguments[0]
    parameters = {}
    for arg in arguments[1:]:
        pars = arg.split("=")
        if len(pars)&gt;1:
            parameters[pars[0]] = pars[1]
    KIPTaskMan.Log( 64,  "Command "+cmd )
    for p in parameters.keys():
        KIPTaskMan.Log( 64,  "parameter["+p+"]: "+parameters[p] )
    if cmd=="reconfigure":
        for n in IDs:
            KIPTaskMan.SendCommand( n, "reconfigure" )
        KIPTaskMan.ReadConfiguration()
    if cmd=="start_slaves" and state=="slaves_dead":
        for n in IDs:
            KIPTaskMan.SuppressComErrorLogs( n, 90000000 )
            KIPTaskMan.StartProcess( n )
        mode="starting_slaves"
    if cmd=="quit" and (state=="slaves_dead" or state=="processes_empty"):
        KIPTaskMan.TerminateTask()
    if cmd=="cleanup" and (state=="processes_dead" or state=="slaves_dead" or state=="processes_empty"):
        for n in IDs:
            KIPTaskMan.SendCommand( n, "cleanup" )
    if cmd=="DUMPSTATES":
        stateHist = KIPTaskMan.GetVar( "StateHist" )
        if stateHist!=None:
            stStr = "Master TM State History: "
            for i in range(0,3):
                if not stateHist.has_key(i):
                    continue
                stStr += "\n  State history %u" % i
                stStr += "\n    Time: '"+time.strftime( "%Y%m%d.%H%M%S",time.localtime(stateHist[i]["time"]) )+".%06u" % ((stateHist[i]["time"]-int(stateHist[i]["time"]))*1000000)
                stStr += "\n    Mode: '"+stateHist[i]["mode"]+"'"
                stStr += "\n    States: "
                for p in stateHist[i]["states"].keys():
                    stStr += "\n        "+p+" : "+stateHist[i]["states"][p]
                stStr += "\n    Vars: "
                for v in stateHist[i]["vars"].keys():
                    stStr += "\n        "+v+" : %d" % stateHist[i]["vars"][v]
                stStr += "\n    State: '"+stateHist[i]["state"]+"'"
        else:
            stStr = "No state history stored"
        KIPTaskMan.Log( 8, stStr )
        for n in IDs:
            KIPTaskMan.SendCommand( n, "DUMPSTATES" )
    if cmd=="DumpEvents":
        for n in IDs:
            KIPTaskMan.SendCommand( n, "DumpEvents" )
    if cmd=="DumpEventMetaData":
        for n in IDs:
            KIPTaskMan.SendCommand( n, "DumpEventMetaData" )
    if cmd=="DumpEventAccounting":
        for n in IDs:
            KIPTaskMan.SendCommand( n, "DumpEventAccounting" )
    if fullcmd.split(" ")[0]=="SetVerbosity":
        KIPTaskMan.Log( 2, "Setting Verbosity to "+fullcmd.split(" ")[1] )
        try:
            verb = int(fullcmd.split(" ")[1],0)
            KIPTaskMan.SetVerbosity(verb)
        except ValueError:
            KIPTaskMan.Log( 0x10, "Error determining verbosity from '"+fullcmd.split(" ")[1]+"'" )
        for n in IDs:
            try:
                KIPTaskMan.SendCommand( n, fullcmd )
            except:
                pass
    if fullcmd.split(" ")[0]=="SetSubsystemVerbosity":
        KIPTaskMan.Log( 2, "Setting subsystemVerbosity to "+fullcmd.split(" ")[1] )
        for n in IDs:
            try:
                KIPTaskMan.SendCommand( n, fullcmd )
            except:
                pass
    if cmd=="set_active":
        KIPTaskMan.Log( 0x40, "Setting Master TaskManager to fail over state ACTIVE" )
        KIPTaskMan.SetFailOverActive( 1 )
    if cmd=="set_passive":
        KIPTaskMan.Log( 0x40, "Setting Master TaskManager to fail over state PASSIVE" )
        KIPTaskMan.SetFailOverActive( 0 )
    ## if state=="slaves_dead":
    ##     cmd = KIPTaskMan.GetTaskCommand()
    ##     continue
    if cmd=="start" and state=="processes_dead":
        KIPTaskMan.SetVar( "ReconfigureEventNr", -1L )
        KIPTaskMan.SetVar( "StopCommandReceived", None )
        params = ""
        #defaults = { "BEAM_TYPE" : "pp", "HLT_MODE" : "A", "RUN_NUMBER" : 0, "RUN_TYPE" : "UNKNOWN" }
        #for p in [ "BEAM_TYPE", "HLT_MODE", "RUN_NUMBER", "RUN_TYPE", "CTP_TRIGGER_CLASS" ]:
        for p in parameters.keys():
            # if parameters.has_key( p ):
            if params!="":
                params += ";"
            params += p+"="+parameters[p]
            #else:
            #params += p+":"+defaults[p]
        KIPTaskMan.SetVar( "ConfigureParameters", parameters )
        try:
            runNumber = int(parameters["RUN_NUMBER"])
            KIPTaskMan.SetVar( "RunNumber", runNumber )
            KIPTaskMan.SetTaskStatusData( "RunNumber=%d;" % runNumber )
        except:
            KIPTaskMan.SetVar( "RunNumber", 0 )
        for n in IDs:
            KIPTaskMan.SendCommand( n, "start "+params, 2000000 )
        mode="starting"
        state="starting"
    if cmd=="connect" and state=="ready_local":
        for n in IDs:
            KIPTaskMan.SendCommand( n, "connect", 2000000 )
        mode="connecting"
        state="connecting"
    if cmd=="kill_slaves" and ( (state=="processes_dead" or state=="error" or state=="processes_empty") or mode=="slave_kill" ):
        mode="slave_kill"
        for n in IDs:
            KIPTaskMan.SuppressComErrorLogs( n, 30000000 )
            try:
                KIPTaskMan.Disconnect( n )
            except:
                pass
            KIPTaskMan.PrepareProcessForTermination( n )
            KIPTaskMan.SendCommand( n, "kill_slaves", 500000 ) # 2000000
            #KIPTaskMan.TerminateProcess( n )
    if cmd=="kill_processes" and state!="processes_dead":
        for n in IDs:
            KIPTaskMan.SendCommand( n, "kill_processes" )
        mode=""
    if cmd=="stop" and (state=="ready_local" or state=="ready" or state=="error"):
        cmdRecvd = KIPTaskMan.GetVar( "StopCommandReceived" )
        if cmdRecvd==None:
            now = time.time()
            KIPTaskMan.SetVar( "StopCommandReceived", now )
        for n in IDs:
            KIPTaskMan.SendCommand( n, "stop", 2000000 )
        mode="stopping"
        state="stopping"

    if (fullcmd.split(" ")[0]=="trigger_reconfigure_event" or fullcmd.split(" ")[0]=="trigger_dcs_update_event"):
        triggeredEvents.append( fullcmd )
        KIPTaskMan.Log( 2, "Reconfigure event command received for configuration event" )
    if (fullcmd.split(" ")[0]=="trigger_reconfigure_event_test" or fullcmd.split(" ")[0]=="trigger_dcs_update_event_test"):
        real_cmd = "_".join( fullcmd.split(" ")[0].split("_")[:-1] ) # Remove _test appendix from command...
        eventNr = KIPTaskMan.GetVar( "ReconfigureEventNr" )
        if eventNr==None:
            eventNr = 0L
        else:
            eventNr += 1L
        KIPTaskMan.SetVar( "ReconfigureEventNr", eventNr )
        KIPTaskMan.Log( 64, fullcmd.split(" ")[0]+" command received for configuration event %d" % eventNr )
        for n in IDs:
            KIPTaskMan.Log( 64, ("Would send command '"+real_cmd+" %d" % eventNr)+" ".join( fullcmd.split(" ")[1:] )+"'to procss "+n+" (only reconfigure test command received)" )

    # Do this AFTER new reconfigure event commands have been added, but BEFORE start_run command is sent
    if state in ( "ready_local", "connecting", "ready", "starting_run", "running", "busy", "pausing", "paused", "resuming" ) and len(triggeredEvents)>0:
        eventNr = KIPTaskMan.GetVar( "ReconfigureEventNr" )
        if eventNr==None:
            eventNr = 0L
        while len(triggeredEvents)>0:
            KIPTaskMan.Log( 2, "Sending reconfigure event %d" % eventNr )
            for n in IDs:
                KIPTaskMan.SendCommand( n, fullcmd.split(" ")[0]+(" %d" % eventNr)+" "+" ".join( fullcmd.split(" ")[1:] ) )
            triggeredEvents = triggeredEvents[1:]
            eventNr += 1L
        KIPTaskMan.SetVar( "ReconfigureEventNr", eventNr )

    if cmd=="start_run" and state=="ready":
        KIPTaskMan.SetVar( "RunStopCommandReceived", None )
        startTimestamp = int(time.time())+15
        for n in IDs:
            KIPTaskMan.SendCommand( n, "start_run %d" % startTimestamp, 2000000 )
        mode="starting_run"
        state="starting_run"
    if cmd=="disconnect" and state=="ready":
        now = time.time()
        KIPTaskMan.SetVar( "StopCommandReceived", now )
        for n in IDs:
            KIPTaskMan.SendCommand( n, "disconnect_prepare", 2000000 )
        mode="disconnecting"
        state="disconnecting"
    if cmd=="pause" and (state=="running" or state=="busy"):
        for n in IDs:
            KIPTaskMan.SendCommand( n, "pause", 2000000 )
        state="pausing"
        mode="pausing"
    if cmd=="stop_run" and (state=="running" or state=="paused" or state=="busy"):
        now = time.time()
        KIPTaskMan.SetVar( "RunStopCommandReceived", now )
        for n in IDs:
            KIPTaskMan.SendCommand( n, "stop_run", 2000000 )
        mode="stopping_run"
        state="stopping_run"
    if cmd=="resume" and state=="paused":
        for n in IDs:
            KIPTaskMan.SendCommand( n, "resume" )
        state="resuming"
        mode="resuming"
        
        
    cmd = KIPTaskMan.GetTaskCommand()

mode = KIPTaskMan.SetVar( "Mode", mode )
KIPTaskMan.SetVar( "TriggeredEvents", triggeredEvents )
