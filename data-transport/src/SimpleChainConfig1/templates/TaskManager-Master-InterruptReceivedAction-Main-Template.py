
mode = KIPTaskMan.GetVar( "Mode" )
if mode==None:
    mode = ""

lam_procs = KIPTaskMan.GetLAMProcessList()
alarm=0
text="HIGHWATERMARK;;"
lowest_ratio=1
lowest_free=0
lowest_total=0
for lp in lam_procs:
    nd=""
    if len(lp)&gt;0 and lp[1]!=None:
        nd=lp[1]
    id=lp[0]
    KIPTaskMan.Log( 2, "Interrupt/LAM received from process "+id+" with notification data '"+nd+"'" )
    if nd == "DumpEvents":
    	for n in IDs:
            KIPTaskMan.SendCommand( n, "DumpEvents" )
    try:
        state = KIPTaskMan.QueryState( id )
    except:
        if not KIPTaskMan.SuppressComErrorLogsQuery( id ) and mode!="slave_kill":
            KIPTaskMan.Log( 8, "Unable to query state from process "+id )
        continue
    try:
        statData = KIPTaskMan.QueryStatusData( id )
    except:
        if not KIPTaskMan.SuppressComErrorLogsQuery( id ) and mode!="slave_kill":
            KIPTaskMan.Log( 8, "Unable to query status data from process "+id )
        continue
    KIPTaskMan.Log( 2, "Process "+id+" status data: '"+statData+"'" )
    for pd in string.split(statData, ";;" )[1:]:
        data = string.split( pd, "::" )
        if len(data)&lt;4 or data[1]=="" or data[2]=="" or data[3]=="":
            continue
        total = float(data[2])
        free = float(data[3])
        if total==0:
            continue
        ratio=free/total
        if ratio&lt;lowest_ratio:
            lowest_ratio=ratio
            lowest_total=total
            lowest_free=free
    KIPTaskMan.Log( 2, "Lowest buffer free for process "+id+" is "+("%f (%d/%d)" % (lowest_ratio,lowest_total,lowest_free)) )
    if lowest_ratio &lt;= 0.15:
        #KIPTaskMan.Log( 8, "Lowest buffer free for process "+id+" is only "+("%f" % (lowest_ratio)) )
        alarm=1
        text=text+id+"::"+state+"::"+( "%u::%u" % ( lowest_total, lowest_free ) )+";;"
    if string.find( state,"Error" )!=-1:
        KIPTaskMan.SetProcessStateChanged( id )

if alarm:
    KIPTaskMan.TaskInterrupt( text )
