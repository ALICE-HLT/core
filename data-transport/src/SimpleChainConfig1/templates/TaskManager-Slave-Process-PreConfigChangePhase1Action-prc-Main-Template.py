KIPTaskMan.Log( 2, "Executing $(ID) PreConfigChangePhase1Action" )

changeState = KIPTaskMan.GetProcessConfigChangeState( "$(ID)" )

KIPTaskMan.Log( 2, "$(ID) config change state: "+changeState )

if changeState=="removed":
    KIPTaskMan.Log( 2, "Starting to remove $(ID)" )
    remSubs = KIPTaskMan.GetVar( "RemoveScheduledSubscribers" )
    if remSubs==None:
        remSubs = ()
    KIPTaskMan.Log( 2, "$(ID) subscribers scheduled for removal: "+str(remSubs) )

    if not "$(ID)" in remSubs:
        remSubs = remSubs + ( "$(ID)", )

    KIPTaskMan.SetVar( "RemoveScheduledSubscribers", remSubs )


    # Check whether parent was paused before
    state = KIPTaskMan.QueryState( "$(ParentID)" )
    i=0
    while (state=="" or state==None) and i!=10:
        state = KIPTaskMan.QueryState( "$(ParentID)" )
        i = i+1
    if state.find( "Paused" )!=-1:
        parentPaused=True
    else:
        parentPaused=False

    # Set parent to paused if it is not already
    i=0
    while (state=="" or state==None or state.find( "Paused" )==-1) and i!=100:
        state = KIPTaskMan.QueryState( "$(ParentID)" )
        time.sleep( 0.001 )
        i = i+1
        if state=="" or state==None or state.find( "Paused" )==-1:
            KIPTaskMan.SendCommand( "$(ParentID)", "PauseProcessing" )
    KIPTaskMan.Log( 2,  "Process $(ParentID) state: '"+state+"'" )

    #Disable parent's publisher for this component
    KIPTaskMan.SendCommand( "$(ParentID)", "DisablePublisher $(ParentPublisher)" )
    time.sleep( 0.2 )

    # Stop processing
    state = KIPTaskMan.QueryState( "$(ID)" )
    i=0
    while (state=="" or state==None or state.find( "Running" )!=-1) and i!=100:
        state = KIPTaskMan.QueryState( "$(ID)" )
        time.sleep( 0.001 )
        i = i+1
        if state.find( "Running" )!=-1 or state=="" or state==None:
            KIPTaskMan.SendCommand( "$(ID)", "StopProcessing" )
    KIPTaskMan.Log( 2,  "Process $(ID) state: '"+state+"'" )

    # Unsubscribe
    i=0
    while (state=="" or state==None or state.find( "Subscribed" )!=-1) and i!=100:
        state = KIPTaskMan.QueryState( "$(ID)" )
        time.sleep( 0.001 )
        i = i+1
        if state.find( "Subscribed" )!=-1 or state=="" or state==None:
            KIPTaskMan.SendCommand( "$(ID)", "Unsubscribe" )
    KIPTaskMan.Log( 2,  "Process $(ID) state: '"+state+"'" )

    # Delete publisher from parent
    KIPTaskMan.SendCommand( "$(ParentID)", "DelPublisher $(ParentPublisher)" )

    # Unsubscribe child gatherer
    if $(HasChild):
        KIPTaskMan.SendCommand( "$(ChildID)", "DelSubscriber $(ID)Pub $(ChildSubscriber)" )

    # Stop subscriptions
    state = KIPTaskMan.QueryState( "$(ID)" )
    i=0
    while (state=="" or state==None or state.find( "AcceptingSubscriptions" )!=-1) and i!=100:
        state = KIPTaskMan.QueryState( "$(ID)" )
        time.sleep( 0.001 )
        i = i+1
        if state.find( "AcceptingSubscriptions" )!=-1 or state=="" or state==None:
            KIPTaskMan.SendCommand( "$(ID)", "StopSubscriptions" )
    KIPTaskMan.Log( 2,  "Process $(ID) state: '"+state+"'" )

    # Unconfigure
    state = KIPTaskMan.QueryState( "$(ID)" )
    i=0
    while (state=="" or state==None or state.find( "Configured" )!=-1) and i!=100:
        state = KIPTaskMan.QueryState( "$(ID)" )
        time.sleep( 0.001 )
        i = i+1
        if state.find( "Configured" )!=-1 or state=="" or state==None:
            KIPTaskMan.SendCommand( "$(ID)", "Unconfigure" )
    KIPTaskMan.Log( 2,  "Process $(ID) state: '"+state+"'" )

    # EndProgram
    KIPTaskMan.SuppressComErrorLogs( "$(ID)", 30000000 )
    KIPTaskMan.SendCommand( "$(ID)", "EndProgram" )

    if not parentPaused:
        KIPTaskMan.SendCommand( "$(ParentID)", "ResumeProcessing" )

