KIPTaskMan.Log( 2,  "Command Received" )
cmd = KIPTaskMan.GetTaskCommand()

state = KIPTaskMan.GetVar( "State" )
mode = KIPTaskMan.GetVar( "Mode" )

while cmd!=None:
    KIPTaskMan.Log( 2,  "Command "+cmd )
    parameters = cmd.split( " " )[1:]
    fullcmd = cmd
    cmd = cmd.split( " " )[0]
    if cmd=="reconfigure":
        for n in IDs:
            KIPTaskMan.SendCommand( n, "reconfigure" )
        KIPTaskMan.ReadConfiguration()
    if cmd=="start_slaves" and state=="slaves_dead":
        for n in IDs:
            KIPTaskMan.SuppressComErrorLogs( n, 90000000 )
            KIPTaskMan.StartProcess( n )
    if cmd=="quit" and (state=="slaves_dead" or state=="processes_empty"):
        KIPTaskMan.TerminateTask()
    if cmd=="cleanup" and (state=="processes_dead" or state=="slaves_dead" or state=="processes_empty"):
        for n in IDs:
            KIPTaskMan.SendCommand( n, "cleanup" )
    if cmd=="DUMPSTATES":
        stateHist = KIPTaskMan.GetVar( "StateHist" )
        if stateHist!=None:
            stStr = "Servant TM State History: "
            for i in range(0,3):
                if not stateHist.has_key(i):
                    continue
                stStr += "\n  State history %u" % i
                stStr += "\n    Time: '"+time.strftime( "%Y%m%d.%H%M%S",time.localtime(stateHist[i]["time"]) )+".%06u" % ((stateHist[i]["time"]-int(stateHist[i]["time"]))*1000000)
                stStr += "\n    Mode: '"+stateHist[i]["mode"]+"'"
                stStr += "\n    States: "
                for p in stateHist[i]["states"].keys():
                    stStr += "\n        "+p+" : "+stateHist[i]["states"][p]
                stStr += "\n    Vars: "
                for v in stateHist[i]["vars"].keys():
                    stStr += "\n        "+v+" : %d" % stateHist[i]["vars"][v]
                stStr += "\n    State: '"+stateHist[i]["state"]+"'"
        else:
            stStr = "No state history stored"
        KIPTaskMan.Log( 8, stStr )
        for n in IDs:
            KIPTaskMan.SendCommand( n, "DUMPSTATES" )
    if cmd=="DumpEvents":
        for n in IDs:
            KIPTaskMan.SendCommand( n, "DumpEvents" )
    if cmd=="DumpEventMetaData":
        for n in IDs:
            KIPTaskMan.SendCommand( n, "DumpEventMetaData" )
    if cmd=="DumpEventAccounting":
        for n in IDs:
            KIPTaskMan.SendCommand( n, "DumpEventAccounting" )
    if cmd=="SetVerbosity":
        KIPTaskMan.Log( 2, "Setting Verbosity to "+parameters[0] )
        try:
            verb = int(parameters[0],0)
            KIPTaskMan.SetVerbosity(verb)
        except ValueError:
            KIPTaskMan.Log( 0x10, "Error determining verbosity from '"+parameters[0]+"'" )
        for n in IDs:
            try:
                KIPTaskMan.SendCommand( n, fullcmd )
            except:
                pass
    if fullcmd.split(" ")[0]=="SetSubsystemVerbosity":
        KIPTaskMan.Log( 2, "Setting subsystemVerbosity to "+fullcmd.split(" ")[1] )
        for n in IDs:
            try:
                KIPTaskMan.SendCommand( n, fullcmd )
            except:
                pass
    if state=="slaves_dead":
        cmd = KIPTaskMan.GetTaskCommand()
        continue
    if cmd=="start" and state=="processes_dead":
        KIPTaskMan.SetVar( "ConfigureParameters", parameters )
        params = {}
        for param in parameters:
            pars = param.split("=")
            if len(pars)&gt;1:
                params[pars[0]] = pars[1]
        try:
            runNumber = int(params["RUN_NUMBER"])
            KIPTaskMan.SetVar( "RunNumber", runNumber )
            KIPTaskMan.SetTaskStatusData( "RunNumber=%d;" % runNumber )
        except:
            KIPTaskMan.SetVar( "RunNumber", 0 )
        for n in IDs:
            KIPTaskMan.SendCommand( n, "start "+string.join(parameters), 2000000 )
        mode="starting"
        state="starting"
    if cmd=="connect" and state=="ready_local":
        for n in IDs:
            KIPTaskMan.SendCommand( n, "connect", 2000000 )
        mode="connecting"
        state="connecting"
    if cmd=="kill_slaves" and ( (state=="processes_dead" or state=="error" or state=="processes_empty") or mode=="slave_kill" ):
        mode="slave_kill"
        for n in IDs:
            KIPTaskMan.SuppressComErrorLogs( n, 30000000 )
            try:
                KIPTaskMan.Disconnect( n )
            except:
                pass
            KIPTaskMan.PrepareProcessForTermination( n )
            KIPTaskMan.SendCommand( n, "kill_slaves", 500000 ) # 2000000
            #KIPTaskMan.TerminateProcess( n )
    if cmd=="kill_processes" and state!="processes_dead":
        for n in IDs:
            KIPTaskMan.SendCommand( n, "kill_processes" )
        mode=""
    if cmd=="stop" and (state=="ready_local" or state=="ready" or state=="error"):
        for n in IDs:
            KIPTaskMan.SendCommand( n, "stop", 2000000 )
        mode="stopping"
        state="stopping"
    if cmd=="start_run" and state=="ready":
        for n in IDs:
            KIPTaskMan.SendCommand( n, fullcmd, 2000000 )
        mode="starting_run"
        state="starting_run"
    if cmd=="disconnect_prepare" and state=="ready":
        for n in IDs:
            KIPTaskMan.SendCommand( n, "disconnect_prepare", 2000000 )
        mode="disconnect_preparing"
        state="disconnecting"
    if cmd=="disconnect" and state=="ready_local_prepare":
        for n in IDs:
            KIPTaskMan.SendCommand( n, "disconnect", 2000000 )
        mode="disconnecting"
        state="disconnecting"
    if cmd=="pause" and (state=="running" or state=="busy"):
        for n in IDs:
            KIPTaskMan.SendCommand( n, "pause" )
        state="pausing"
    if cmd=="stop_run" and (state=="running" or state=="paused" or state=="busy"):
        for n in IDs:
            KIPTaskMan.SendCommand( n, "stop_run", 2000000 )
        mode="stopping_run"
        state="stopping_run"
    if cmd=="resume" and state=="paused":
        for n in IDs:
            KIPTaskMan.SendCommand( n, "resume" )
        state="resuming"
    if cmd=="noop":
        pass
    if (cmd=="trigger_reconfigure_event" or cmd=="trigger_dcs_update_event") and (state=="running" or state=="busy" or state=="paused" or state=="ready" or state=="ready_local"):
        for n in IDs:
            KIPTaskMan.SendCommand( n, cmd+" "+" ".join( parameters ) )
            
    cmd = KIPTaskMan.GetTaskCommand()

mode = KIPTaskMan.SetVar( "Mode", mode )
