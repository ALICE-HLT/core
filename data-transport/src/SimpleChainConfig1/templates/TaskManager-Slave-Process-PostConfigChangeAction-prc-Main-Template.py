mode = KIPTaskMan.GetVar( "Mode" )
if mode==None:
    mode = ""

KIPTaskMan.Log( 2, "Post Config Change Action for '$(ID)' (mode: '"+mode+"')" )

if mode=="starting" or mode=="connecting" or mode=="starting_run" or mode=="stopping_run" or mode=="disconnecting":
    KIPTaskMan.SuppressComErrorLogs( "$(ID)", 90000000 )
    KIPTaskMan.StartProcess( "$(ID)" )
