states = {}
blankStates=False
blankStateIDList = []
oldState = KIPTaskMan.GetVar( "State" )
mode = KIPTaskMan.GetVar( "Mode" )
if mode==None:
    mode = ""
for p in IDs:
    cnt=0
    states[p] = ""
    while states[p]=="" and cnt&lt;3:
        states[p] = KIPTaskMan.QueryState( p )
        cnt += 1
    if states[p]=="" and not (mode=="starting_slaves" and not oldState in ( "slaves_dead", "starting_slaves" ) ):
        # KIPTaskMan.Log( 64,  "Process "+p+" state: '"+str(states[p])+"' - mode: '"+str(mode)+"' - oldState: '"+str(oldState)+"'" )
        blankStates=True
        blankStateIDList.append( p )
    KIPTaskMan.Log( 2,  "Process "+p+" state: "+states[p] )


if blankStates:
    blankStateCounter=KIPTaskMan.GetVar( "StateChangeActionBlankStateCounter" )
    if blankStateCounter==None:
        blankStateCounter=0
    blankStateCounter += 1
    #if blankStateCounter&gt;20:
    #    blankStates=False
    #    blankStateCounter=0
    if (blankStateCounter % 20)==0 and mode!="":
        KIPTaskMan.Log( 8,  "Unable to correctly read states from all subordinate TaskManagers after %d tries; State determination logic may be unreliable" % blankStateCounter )
        idStr = blankStateIDList[0]
        for bsi in blankStateIDList[1:]:
            idStr += ", "+bsi
        KIPTaskMan.Log( 8, "Subordinates with blank states: "+idStr )
        blankStates=False
else:
    blankStateCounter=0
KIPTaskMan.SetVar( "StateChangeActionBlankStateCounter", blankStateCounter )
    

    
    
if not blankStates:
    empty = 1
    notstarted = 1
    dead = 1
    slaves_dead = 1
    starting = 0
    ready_local = 1
    connecting = 0
    ready = 1
    starting_run = 0
    running = 1
    paused = 0
    busy = 0
    stopping_run = 0
    disconnecting = 0
    stopping = 0
    error = 0
    
    procStarted = KIPTaskMan.GetVar( "ProcStart" )
    if procStarted == None:
        procStarted = {}
    
    stateHist = KIPTaskMan.GetVar( "StateHist" )
    if stateHist==None:
        stateHist = {}
    oldStates = 2
    for i in range( oldStates, 0,-1):
        if stateHist.has_key(i-1):
            stateHist[i] = stateHist[i-1]
    
    #if stateHist.has_key(1):
    #    statehist[2] = stateHist[1]
    #if stateHist.has_key(0):
    #    statehist[1] = stateHist[0]
    stateHist[0] = { "states":states,
                     "vars":{},
                     "mode":"",
                     "state":"",
                     "time":time.time(),
                     }
        
    
    
    for p in IDs:
    ##    if string.find( states[p], "TM:" )!=-1 and mode!="slave_kill":
    ##        KIPTaskMan.SuppressComErrorLogs( p, 90000000 )
    ##        KIPTaskMan.StartProcess( p )
    ##        KIPTaskMan.Log( 2,  "Starting slave process "+p )
        
        if states[p]=="TM:EMPTY": # or states[p]=="processes_empty":
            slaves_dead = 0
            continue
        if states[p]=="":
            if (mode in ("starting_slaves", "stopping") and oldState=="processes_dead") or (mode in ( "starting", "disconnecting" ) and oldState=="ready_local") or (mode in( "connecting", "stopping_run" ) and oldState=="ready") or (mode in ( "starting_run", "resuming" ) and oldState in ( "running", "busy" )) or (mode=="pausing" and oldState=="paused"):
                continue
            empty = 0
            notstarted = 0
            dead = 0
            slaves_dead = 0
            ready_local = 0
            ready = 0
            running = 0
            continue
        empty = 0
        #if (states[p]=="TM:DEAD" or states[p]=="TM:COULDNOTSTART") and mode!="slave_kill":
        if (states[p][:3]=="TM:") and mode!="slave_kill" and mode!="":
            dead=0
            slaves_dead=0
            if states[p]!="TM:READY":
                KIPTaskMan.Log( 0x10, "Process "+p+" is dead (A)." )
                if not procStarted.has_key(p):
                    procStarted[p] = 0
                if procStarted[p]&lt;=1: ## XXXX Fix, was 10
                    KIPTaskMan.SuppressComErrorLogs( p, 90000000 )
                    KIPTaskMan.StartProcess( p )
                    procStarted[p] = procStarted[p]+1
                else:
                    error = 1
        if states[p][:3]!="TM:":
            procStarted[p] = 0
        if string.find( states[p], "TM:" )==-1: # and states[p]!="slaves_dead":
            slaves_dead = 0
        if states[p] != "TM:EMPTY" and states[p] != "processes_empty":
            empty = 0
        if states[p] != "dead" and states[p] != "processes_dead" and states[p][:3]!="TM:" and states[p]!="":
            dead = 0
        if states[p]!="TM:NOTSTARTED" and states[p]!="TM:READY":
            notstarted = 0
        if states[p] == "starting":
            starting = 1
        if states[p] != "ready_local":
            ready_local = 0
        if states[p] == "connecting":
            connecting = 1
        if states[p] != "ready":
            ready = 0
        if states[p] == "ready":
            busy = 0
        if states[p] == "starting_run":
            starting_run = 1
        if states[p] != "running" and states[p] != "busy":
            running = 0
        if states[p] == "paused":
            paused = 1
        if states[p] == "busy":
            busy = 1
        if states[p] == "stopping_run":
            stopping_run = 1
        if states[p] in ( "disconnect_preparing", "ready_local_prepare", "disconnecting" ):
            disconnecting = 1
        if states[p] == "stopping":
            stopping = 1
        if states[p] == "error":
            KIPTaskMan.Log( 0x10, "Process "+p+" is in error state." )
            error = 1
    
    
    for p in IDs:
    ##    if string.find( states[p], "TM:" )!=-1 and mode!="slave_kill":
    ##        KIPTaskMan.SuppressComErrorLogs( p, 90000000 )
    ##        KIPTaskMan.StartProcess( p )
    ##        KIPTaskMan.Log( 2,  "Starting slave process "+p )
        
        if states[p]=="TM:EMPTY": # or states[p]=="processes_empty":
            slaves_dead = 0
            continue
        if states[p]=="":
            continue
        if mode=="starting" and states[p]=="processes_dead":
            parameters = KIPTaskMan.GetVar( "ConfigureParameters" )
            if parameters==None:
                parameters = {}
            params = ""
            # for par in [ "BEAM_TYPE", "HLT_MODE", "RUN_NUMBER", "RUN_TYPE" ]:
            #     if parameters.has_key( par ):
            for par in parameters.keys():
                if params!="":
                    params += ";"
                params += par+"="+parameters[par]
            KIPTaskMan.SendCommand( p, "start "+params, 2000000 )
        if mode=="connecting" and states[p]=="ready_local":
            KIPTaskMan.SendCommand( p, "connect", 2000000 )
        if mode=="starting_run" and states[p]=="ready":
            KIPTaskMan.SendCommand( p, "start_run", 2000000 )
        if mode=="pausing" and (states[p]=="running" or states[p]=="busy"):
            KIPTaskMan.SendCommand( p, "pause", 2000000 )
        if mode=="resuming" and states[p]=="paused":
            KIPTaskMan.SendCommand( p, "resume", 2000000 )
        if mode=="stopping_run" and (states[p]=="running" or states[p]=="busy" or states[p]=="paused"):
            KIPTaskMan.SendCommand( p, "stop_run", 2000000 )
        if mode=="disconnecting" and states[p]=="ready":
            KIPTaskMan.SendCommand( p, "disconnect_prepare", 2000000 )
        if mode=="disconnecting" and states[p]=="ready_local_prepare":
            KIPTaskMan.SendCommand( p, "disconnect", 2000000 )
        if mode=="stopping" and states[p]=="ready_local":
            KIPTaskMan.SendCommand( p, "stop", 2000000 )
        #if mode=="slave_kill" and (states[p]=="processes_dead" or states[p]=="dead"):
        if mode=="slave_kill" and states[p][:3]!="TM:":
            KIPTaskMan.SuppressComErrorLogs( p, 30000000 )
            KIPTaskMan.PrepareProcessForTermination( p )
            KIPTaskMan.SendCommand( p, "kill_slaves", 500000 ) # 2000000
            
    
    if slaves_dead:
        state = "slaves_dead"
    elif empty:
        state = "processes_empty"
    elif dead:
        state = "processes_dead"
    elif error:
        state = "error"
    elif paused:
        if mode=="stopping_run":
            state = "stopping_run"
        else:
            state = "paused"
    elif starting_run:
        state = "starting_run"
    elif stopping_run:
        state = "stopping_run"
    elif busy and mode=="stopping_run":
        state = "stopping_run"
    elif busy and running:
        state = "busy"
    elif running:
        state = "running"
    elif ready:
        state = "ready"
    elif connecting:
        state = "connecting"
    elif disconnecting:
        state = "disconnecting"
    elif ready_local:
        state = "ready_local"
    elif stopping:
        state = "stopping"
    elif starting:
        state = "starting"
    else:
        state=mode
    #elif mode=="starting" or mode=="connecting":
    #    state="starting"
    #else:
    #    
    #    state = "unknown"
    
    KIPTaskMan.Log( 2,  "state: '"+state+"' - mode: '"+mode+"' - slaves_dead: %d - dead: %d - starting: %d - ready_local: %d - connecting: %d - ready: %d - starting_run: %d - running: %d - paused: %d - busy: %d - stopping_run: %d - disconnecting: %d - stopping: %d - error: %d" %
                    (slaves_dead, dead, starting, ready_local, connecting, ready, starting_run, running, paused, busy, stopping_run, disconnecting, stopping, error ))

    if (((mode=="stopping" and state!="processes_dead") or (mode=="disconnecting" and state!="ready_local")) and $(STOPTIMEOUT)!=0) or ((mode=="stopping_run" and state!="ready") and $(STOPRUNTIMEOUT)!=0):
        now = time.time()
        if mode=="stopping_run":
            cmdRecvd = KIPTaskMan.GetVar( "RunStopCommandReceived" )
            timeout = $(STOPRUNTIMEOUT)
        else:
            cmdRecvd = KIPTaskMan.GetVar( "StopCommandReceived" )
            timeout = $(STOPTIMEOUT)
        if cmdRecvd!=None:
            tdiff = now-cmdRecvd
            if tdiff&gt;timeout*1.2:
                stateHist = KIPTaskMan.GetVar( "StateHist" )
                if stateHist!=None:
                    stStr = "Master TM State History: "
                    for i in range(0,3):
                        if not stateHist.has_key(i):
                            continue
                        stStr += "\n  State history %u" % i
                        stStr += "\n    Time: '"+time.strftime( "%Y%m%d.%H%M%S",time.localtime(stateHist[i]["time"]) )+".%06u" % ((stateHist[i]["time"]-int(stateHist[i]["time"]))*1000000)
                        stStr += "\n    Mode: '"+stateHist[i]["mode"]+"'"
                        stStr += "\n    States: "
                        for p in stateHist[i]["states"].keys():
                            stStr += "\n        "+p+" : "+stateHist[i]["states"][p]
                        stStr += "\n    Vars: "
                        for v in stateHist[i]["vars"].keys():
                            stStr += "\n        "+v+" : %d" % stateHist[i]["vars"][v]
                        stStr += "\n    State: '"+stateHist[i]["state"]+"'"
                else:
                    stStr = "No state history stored"
                KIPTaskMan.Log( 8, stStr )
                for n in IDs:
                    KIPTaskMan.SendCommand( n, "DUMPSTATES" )
            elif len(IDs)&gt;0:
                KIPTaskMan.SendCommand( IDs[0], "noop", 2000000 )

                 
    
    
        
    KIPTaskMan.SetVar( "State", state )
    KIPTaskMan.SetTaskState( state )
    
    KIPTaskMan.SetVar( "ProcStart", procStarted )
    
    stateHist[0]["vars"] = {
    "empty" : empty,
    "notstarted" : notstarted,
    "dead" : dead,
    "slaves_dead" : slaves_dead,
    "starting" : starting,
    "ready_local" : ready_local,
    "connecting" : connecting,
    "ready" : ready,
    "starting_run" : starting_run,
    "running" : running,
    "paused" : paused,
    "busy" : busy,
    "stopping_run" : stopping_run,
    "disconnecting" : disconnecting,
    "stopping" : stopping,
    "error" : error,
    }
    stateHist[0]["mode"] = mode
    stateHist[0]["state"] = state
    
    KIPTaskMan.SetVar( "StateHist", stateHist )
    
