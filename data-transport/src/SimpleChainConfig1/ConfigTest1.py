#!/usr/bin/env python

##
## This file is property of and copyright by the Technical Computer
## Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
## University, Heidelberg, Germany, 2004
## This file has been written by Timm Morten Steinbeck, 
## timm@kip.uni-heidelberg.de
##
##
## See the file license.txt for details regarding usage, modification,
## distribution and warranty.
## Important: This file is provided without any warranty, including
## fitness for any particular purpose.
##
##
## Newer versions of this file's package will be made available from 
## http://www.ti.uni-hd.de/HLT/software/software.html 
## or the corresponding page of the Heidelberg Alice HLT group.
##


import SimpleChainConfig1, sys

if len(sys.argv)<=1:
    print "Usage: "+sys.argv[0]+" <config-filename>"
    sys.exit( 1 )

try:
    configReader = SimpleChainConfig1.XMLConfigReader()
    config = configReader.ReadSimpleChainConfig( sys.argv[1], 1 )
    #print "config:", config
    config.Check()
    configMapper = SimpleChainConfig1.ChainConfigMapper( "/home/timm/src/AliHLT", "Linux-i686" )
    procList = configMapper.CreateProcessList( config )
    #print "procList:", procList
    #for p in procList:
        #print p, p.GetNode(), p.GetTypeString(), p.GetCmd()
    outputter = SimpleChainConfig1.SimpleProcListOutputter()
    outputter.OutputProcessList( config, procList )

except SimpleChainConfig1.ChainConfigException, e:
    sys.stderr.write(  "Exception caught:"+e.fValue+"\n" )
    sys.exit( -1 )

