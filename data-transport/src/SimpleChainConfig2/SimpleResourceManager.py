#!/usr/bin/env python

from ResourceManagerInterface import ResourceManagerInterface

import Detector

import os

import Exceptions

gkConnectionFactor = 0.05


class SimpleResourceManager(ResourceManagerInterface):

    class Node(object):
        def __init__( self, ID, cpuCount, platform, attributes, hostname, ddls={} ):
            self.fID=ID
            self.fCPUCount=cpuCount
            self.fProcesses = {}
            self.fLowLoadProcesses = []
            self.fDDLs = ddls
            self.fPlatform = platform
            self.fHostname = hostname
            self.fGroups = {}
            self.fMaster = False
            self.fAttributes = attributes
            self.fConnections = 0
            self.fConnectionFactor = gkConnectionFactor
            self.fProcessConnections = {}
        def AddDDLs( self, ddls ):
            for ddl in ddls.keys():
                self.fDDLs[ddl] = ddls[ddl]
        def AddProcess( self, proc, connections, group=None, lowLoad=False, weight=1.0 ):
            #self.fProcesses.append(proc)
            if not self.fProcesses.has_key(proc):
                self.fProcesses[proc] = []
            factor = 1.0
            if lowLoad:
                self.fLowLoadProcesses.append(proc)
                factor = 0.1
            self.fProcesses[proc].append( factor*weight )
            if group!=None:
                if self.fGroups.has_key(group):
                    self.fGroups[group].append(proc)
                else:
                    self.fGroups[group] = [proc]
            self.fProcessConnections[proc] = connections
            self.fConnections += connections
        def RemoveProcess( self, proc ):
            for g in self.fGroups.keys():
                if self.fGroups[g].has_key(proc):
                    del self.fGroups[g][proc]
            if self.fProcessConnections.has_key(proc):
                self.fConnections -= self.fProcessConnections[proc]
                del self.fProcessConnections[proc]
            if self.fLowLoadProcesses.count(proc)>0:
                self.fLowLoadProcesses.remove(proc)
            if self.fProcesses.has_key(proc):
                self.fProcesses[proc] = self.fProcesses[proc][1:]
                if len(self.fProcesses[proc])<=0:
                    del(self.fProcesses[proc])
        def Master(self):
            self.fMaster = True
        def RemoveDDL(self,ddl):
            if self.fDDLs.has_key(ddl):
                del(self.fDDLs[ddl])
        def GetID(self):
            return self.fID
        def GetProcesses(self):
            return self.fProcesses.keys()
        def GetTotalProcessWeights(self):
            weight=0
            for p in self.fProcesses.keys():
                for w in self.fProcesses[p]:
                    weight += w
            return weight
        def GetUtilization(self):
            #return len(self.fProcesses)+self.fConnections*self.fConnectionFactor-len(self.fLowLoadProcesses)*0.9
            return self.GetTotalProcessWeights()+self.fConnections*self.fConnectionFactor
        def GetCPUCount(self):
            return self.fCPUCount
        def GetPlatform(self):
            return self.fPlatform
        def GetHostname(self):
            return self.fHostname
        def HasGroup(self,group):
            return self.fGroups.has_key(group)
        def GetBiggestGroup(self):
            groups = self.fGroups.keys()
            if len(groups)<=0:
                return None
            bg = groups[0]
            for g in groups[1:]:
                if len(self.fGroups[g])>len(self.fGroups[bg]):
                    bg = g
            return bg
        def GetAttributeValues(self,attrVal):
            if self.fAttributes.has_key(attrVal):
                return self.fAttributes[attrVal]
            return None
        def GetProcessUtilization( self, proc ):
            # if self.fLowLoadProcesses.count(proc)>0:
            #     util = 0.1
            # else:
            #     util = 1.0
            util=0
            if proc in self.fProcesses.keys():
                for w in self.fProcesses[proc]:
                    util += w
            if self.fProcessConnections.has_key(proc):
                util += self.fConnectionFactor*self.fProcessConnections[proc]
            return util
        
    
            
    def __init__(self):
        ResourceManagerInterface.__init__(self)
        self.fWorkerNodes = []
        self.fDDLLocations = {}
        self.fNodes = {}
        self.fDefaultCPUCountPerNode = 4
        self.fDefaultPlatform = os.uname()[0]+"-"+os.uname()[4]
        self.fDDL2Node = {}
        self.fProcess2Nodes = {}
        self.fNonDDLNodes = {}
        self.fMasterNodes = []
        self.fRestrictedAttributeNodes = set()
        self.fCompactNodeAssignment = False # if True, multiple instances of same process are attempted to be placed on same node as much as possible

    def SetWorkerNodes( self, workerNodeList ):
        self.fWorkerNodes += workerNodeList.keys()
        for p in workerNodeList.keys():
            if not self.fNodes.has_key(p):
                self.fNodes[p] = self.Node( p, workerNodeList[p]["cpus"], workerNodeList[p]["platform"], workerNodeList[p]["attributes"], workerNodeList[p]["hostname"] )
                self.fNonDDLNodes[p] = self.fNodes[p]
                if workerNodeList[p].has_key("master") and workerNodeList[p]["master"]:
                    self.fNodes[p].Master()
                    self.fMasterNodes.append(p)

    def SetCompactNodeAssignment( self, set=True ):
        self.fCompactNodeAssignment = set

    def SetDDLLocations( self, ddlLocs ):
        for dl in ddlLocs.keys():
            self.fDDLLocations[dl] = ddlLocs[dl]
            if not self.fNodes.has_key(ddlLocs[dl]["node"]):
                #self.fNodes[ddlLocs[dl]["node"]] = self.Node( ddlLocs[dl]["node"], self.fDefaultCPUCountPerNode, self.fDefaultPlatform, {dl:ddlLocs[dl]} )
                #raise Exceptions.SCC2Exception( "Node %s for DDL %d not defined in nodelist" % ( ddlLocs[dl]["node"], dl ) )
                pass
            else:
                self.fNodes[ddlLocs[dl]["node"]].AddDDLs( {dl:ddlLocs[dl]} )
            self.fDDL2Node[dl] = ddlLocs[dl]["node"]
            if self.fNonDDLNodes.has_key(ddlLocs[dl]["node"]):
                del self.fNonDDLNodes[ddlLocs[dl]["node"]]

    def RestrictDDLs( self, ddlList ):
        allDDLs = set(self.fDDLLocations.keys())
        excludedDDLs = allDDLs - set(ddlList)
        for ddl in excludedDDLs:
            node = self.fDDL2Node[ddl]
            if not self.fNonDDLNodes.has_key(node) and self.fNodes.has_key(node):
                self.fNonDDLNodes[node] = self.fNodes[node]
                self.fNodes[node].RemoveDDL(ddl)
        for ddl in ddlList:
            node = self.fDDL2Node[ddl]
            if self.fNonDDLNodes.has_key(node):
                del self.fNonDDLNodes[node]
            if self.fNodes.has_key(node):
                self.fNodes[node].AddDDLs( {ddl:self.fDDLLocations[ddl]} )

    def PlaceProcess( self, procID, existingNodeList, group, connectionCnt=0, lowLoad=False ):
        if not self.fProcess2Nodes.has_key(procID):
            self.fProcess2Nodes[procID] = []
        for node in existingNodeList:
            if self.fNodes.has_key(node):
                self.fNodes[node].AddProcess(procID,connectionCnt, group,lowLoad)
                self.fProcess2Nodes[procID].append(node)

    def RequestNodes(self, procMult, procID, group, connectionCnt, ddlID=None, slice=None, detID=None, existingNodeList=[], lowLoad=False, forceFEP = False, parentNodes=[], placeExistingNodes=True, instanceWeight=1.0 ):
        allocNodes = []
        remMult = procMult
        # Determine which nodes are already used
        if self.fProcess2Nodes.has_key(procID):
            usedNodes = self.fProcess2Nodes[procID]
        else:
            usedNodes = []
            self.fProcess2Nodes[procID] = usedNodes
        # First fill up with nodes from supplied request list
        for node in existingNodeList:
            if self.fNodes.has_key(node):
                allocNodes.append(node)
                if placeExistingNodes:
                    self.fNodes[node].AddProcess(procID,connectionCnt,group,lowLoad,weight=instanceWeight)
                    self.fProcess2Nodes[procID].append(node)
                remMult -= 1
            else:
                raise Exceptions.SCC2Exception( "Requested node %s not known to node list" % ( node ) )
        if remMult<=0:
            return allocNodes

        # Try to fill up parent nodes (e.g. nodes on which at least one of which our parent processes runs) to keep network traffic down, if possible.
        for pn in parentNodes:
            if (self.fNodes[pn].GetUtilization()+instanceWeight)<=self.fNodes[pn].GetCPUCount() or lowLoad:
                if (( self.fNodes[pn].GetCPUCount()-len(self.fNodes[pn].GetProcesses()) )/instanceWeight)<remMult and not lowLoad:
                    ddlMult = int( (self.fNodes[pn].GetCPUCount()-self.fNodes[pn].GetUtilization())/instanceWeight )
                else:
                    ddlMult = remMult
                for i in range( ddlMult):
                    self.fNodes[pn].AddProcess( procID, connectionCnt, group, lowLoad, weight=instanceWeight )
                    self.fProcess2Nodes[procID].append(pn)
                    allocNodes.append(pn)
                remMult -= ddlMult
            if remMult<=0:
                return allocNodes

        if ddlID!=None:
            ddlNode = self.fDDL2Node[ddlID]

            # Next try to put as much on node with given DDL
            if (self.fNodes[ddlNode].GetUtilization()+instanceWeight)<=self.fNodes[ddlNode].GetCPUCount() or lowLoad or forceFEP:
                if ((self.fNodes[ddlNode].GetCPUCount()-self.fNodes[ddlNode].GetUtilization())/instanceWeight)<remMult and not lowLoad and not forceFEP:
                    ddlMult = int( (self.fNodes[ddlNode].GetCPUCount()-self.fNodes[ddlNode].GetUtilization()) / instanceWeight )
                else:
                    ddlMult = remMult
                for i in range( ddlMult):
                    self.fNodes[ddlNode].AddProcess( procID, connectionCnt, group, lowLoad, weight=instanceWeight )
                    self.fProcess2Nodes[procID].append(ddlNode)
                    allocNodes.append(ddlNode)
                remMult -= ddlMult
                    
            if remMult<=0:
                return allocNodes

            

        # Now try any nodes, but priority goes to all nodes that we already have processes on
        # until they are full (i.e. #processes==#cpus)
        #usedNodes = usedNodes+allocNodes
        #allocNodes = []
        freeUsedNodes = {}
        if self.fCompactNodeAssignment:
            for node in set(usedNodes+allocNodes):
                if self.fNodes.has_key(node) and (self.fNodes[node].GetUtilization()+instanceWeight)<=self.fNodes[node].GetCPUCount():
                    freeUsedNodes[node] = int( (self.fNodes[node].GetCPUCount()-self.fNodes[node].GetUtilization())/instanceWeight )
        for i in range(remMult):
            if len(freeUsedNodes)>0:
                # We can use one of the nodes already used
                node = freeUsedNodes.keys()[0]
                allocNodes.append(node)
                self.fNodes[node].AddProcess(procID, connectionCnt, group, lowLoad, weight=instanceWeight )
                self.fProcess2Nodes[procID].append(node)
                freeUsedNodes[node] = freeUsedNodes[node]-1
                if freeUsedNodes[node]<=1:
                    del freeUsedNodes[node]
            else:
                # We have to request a new node
                node=self.GetAvailableNode(group, weight=instanceWeight)
                allocNodes.append(node)
                self.fNodes[node].AddProcess(procID, connectionCnt, group, lowLoad, weight=instanceWeight )
                self.fProcess2Nodes[procID].append(node)
                if self.fCompactNodeAssignment and (self.fNodes[node].GetUtilization()+instanceWeight)<=self.fNodes[node].GetCPUCount():
                    freeUsedNodes[node] = int( (self.fNodes[node].GetCPUCount()-self.fNodes[node].GetUtilization()) / instanceWeight )
        return allocNodes

    def RestrictNodesWithAttributes( self, attributeList ):
        restrNodes = set(self.GetNodesWithAttributes( attributeList ))
        self.fRestrictedAttributeNodes |= restrNodes

    def GetNodesWithAttributes( self, attributeList ):
        # attributeList is dictionary, attrnames are keys, required attrValues are in list at attrname key
        nodes=[]
        for node in self.fNodes.keys():
            found=True
            for attr in attributeList.keys():
                nodeAttrValues = self.fNodes[node].GetAttributeValues(attr)
                if nodeAttrValues == None:
                    found=False
                    break
                for av in attributeList[attr]:
                    if av not in nodeAttrValues:
                        found=False
                        break
                if not found:
                    break
            if found:
                nodes.append(node)
        return nodes

    def ReleaseNodes(self, procID, existingNodeList ):
        self.NotImplemented( "ResourceManager.ReleaseNodes" )

    def GetNodePlatform( self, node ):
        if self.fNodes.has_key(node):
            return self.fNodes[node].GetPlatform()
        return None

    def GetNodeHostname( self, node ):
        if self.fNodes.has_key(node):
            return self.fNodes[node].GetHostname()
        return None
        
    def GetDDLLocation( self, ddl ):
        if self.fDDLLocations.has_key(ddl) and self.fNodes.has_key( self.fDDLLocations[ddl]["node"] ):
            return self.fDDLLocations[ddl]
        if not self.fDDLLocations.has_key(ddl):
            raise Exceptions.SCC2Exception( "DDL location for DDL with ID %d not known" % ( ddl ) )
        raise Exceptions.SCC2Exception( "Node %s for DDL with ID %d not known in node list" % ( self.fDDLLocations[ddl]["node"], ddl ) )

    def GetAvailableNode( self, group=None, weight=1.0 ):
        outDDLs = Detector.NameToDDLList("HLT")
        outNodes = set()
        for ddl in outDDLs:
            try:
                outNodes |= set([self.GetDDLLocation(ddl)["node"]])
            except Exceptions.SCC2Exception:
                pass
        if self.fCompactNodeAssignment and group!=None:
            availNodes = list((set(self.fNonDDLNodes.keys()) & set(self.GetAllNodesForGroup(group)))-set(self.fMasterNodes)-outNodes-self.fRestrictedAttributeNodes)
            availNodes.sort( key=lambda x: (self.fNodes[x].GetUtilization()-self.fNodes[x].GetCPUCount()) )
            if len(availNodes)<=0 or ((self.fNodes[availNodes[0]].GetUtilization()+weight)>self.fNodes[availNodes[0]].GetCPUCount()):
                availNodes = list((set(self.fNodes.keys()) & set(self.GetAllNodesForGroup(group)))-set(self.fMasterNodes)-self.fRestrictedAttributeNodes)
                availNodes.sort( key=lambda x: (self.fNodes[x].GetUtilization()-self.fNodes[x].GetCPUCount()) )
            if len(availNodes)>0 and ((self.fNodes[availNodes[0]].GetUtilization()+weight)<=self.fNodes[availNodes[0]].GetCPUCount()):
                #availNodes.sort( key=lambda x: len(self.fNodes[x].GetProcesses()) )
                #GetUtilization
                #availNodes.sort( key=lambda x: self.fNodes[x].GetUtilization() )
                return availNodes[0]
        availNodes = list(set(self.fNonDDLNodes.keys()) - set(self.fMasterNodes)-outNodes-self.fRestrictedAttributeNodes)
        availNodes.sort( key=lambda x: (self.fNodes[x].GetUtilization()-self.fNodes[x].GetCPUCount()) )
        if len(availNodes)<=0 or ((self.fNodes[availNodes[0]].GetUtilization()+weight)>self.fNodes[availNodes[0]].GetCPUCount()):
            availNodes = list(set(self.fNodes.keys()) - set(self.fMasterNodes)-outNodes-self.fRestrictedAttributeNodes)
            #availNodes.sort( key=lambda x: len(self.fNodes[x].GetProcesses()) )
        if len(availNodes)<=0:
            print availNodes
            raise Exceptions.SCC2Exception( "FATAL: No nodes at all available" )
        availNodes.sort( key=lambda x: (self.fNodes[x].GetUtilization()-self.fNodes[x].GetCPUCount()) )
        return availNodes[0]

    def GetNodesForGroup( self, group ):
        nodes = []
        for node in self.fNodes.keys():
            #print "GetNodesForGroup:",node,self.fNodes[node].GetBiggestGroup()
            if self.fNodes[node].GetBiggestGroup()==group:
                nodes.append(node)
        #print "GetNodesForGroup:",group,nodes
        return nodes

    def GetAllNodesForGroup( self, group ):
        nodes = []
        for node in self.fNodes.keys():
            if self.fNodes[node].HasGroup(group):
                nodes.append(node)
        return nodes

    def GetUtilizationList( self ):
        util = {}
        for node in self.fNodes.keys():
            util[node] = self.fNodes[node].GetUtilization()
        return util

    def GetNodeList(self):
        return self.fNodes
            
                
        
        

