#!/usr/bin/env python

import Detector

ddlList = list(Detector.NameToDDLList("ALL"))
ddlList.sort()

for ddl in ddlList:
    print ddl,Detector.GetDetectorID(ddl),Detector.GetSubDetectorID(ddl),Detector.GetSubSubDetectorID(ddl)

    
