#!/usr/bin/env python

import libxml2
import Exceptions

class DDLListException(Exceptions.SCC2Exception):
    def __init__(self,message):
        Exceptions.SCC2Exception.__init__(self,message)
        pass


def ReadXML( filename ):
    ddlLoc = {}
    xmlFile = libxml2.parseFile(filename)
    xPathContext = xmlFile.xpathNewContext()
    if len(xPathContext.xpathEval("/SimpleChainConfig2DDLList"))!=1:
        raise DDLListException( "Exactly one top level SimpleChainConfig2DDLList node allowed and required in XML file" )
    ddls = xPathContext.xpathEval("/SimpleChainConfig2DDLList/DDL")
    for ddl in ddls:
        try:
            id=int(ddl.prop("ID"))
        except ValueError:
            raise DDLListException( "DDL ID does not contain an integer number" )
        if id==None:
            raise DDLListException( "ID attribute required for DDL node" )
        if len(ddl.xpathEval("./Node"))!=1:
            raise DDLListException( "Exactly one Node node required for DDL" )
        node = ddl.xpathEval("./Node")[0].content
        if len(ddl.xpathEval("./RORCId"))!=1:
            raise DDLListException( "Exactly one RORCId node required for DDL" )
        try:
            rorcid = int(ddl.xpathEval("./RORCId")[0].content)
        except ValueError:
            raise DDLListException( "RORCId node does not contain an integer number" )
        if len(ddl.xpathEval("./Channel"))!=1:
            raise DDLListException( "Exactly one Channel node required for DDL" )
        try:
            channel = int(ddl.xpathEval("./Channel")[0].content)
        except ValueError:
            raise DDLListException( "Channel node does not contain an integer number" )
        ddlLoc[id] = { "node" : node, "rorcid" : rorcid, "channel" : channel }
    return ddlLoc


if __name__ == "__main__":
    import sys
    if len(sys.argv)<=1:
        print "Usage: "+sys.argv[0]+" <ddl-list-xml>"
        sys.exit(1)
    ddlLoc = ReadXML( sys.argv[1])
    ddls = ddlLoc.keys()
    ddls.sort()
    for ddl in ddls:
        print ddl,ddlLoc[ddl]["node"],ddlLoc[ddl]["rorcid"],ddlLoc[ddl]["channel"]
