#!/usr/bin/env python

#!/usr/bin/env python

import os, string, re, sys, os.path
import SimpleChainConfig1

import ConfigReader
import SiteConfig
import SimpleResourceManager
import DDLLocations
import NodeList
import Exceptions
import Detector

def retrieve_file( retriever, snippet_type, snippet_id=None, detector_id=None ):
    cmd = retriever+" -filetype="+snippet_type
    if snippet_id!=None:
        cmd += " -id="+snippet_id
    if detector_id!=None:
        cmd += " -snippetdetectorid="+detector_id
    # print cmd
    cmdfile=os.popen(cmd)
    lines = cmdfile.readlines()
    cmdfile.close()
    del(cmdfile)
    # print lines
    return [ line[:-1] if line[-1]=="\n" else line for line in lines ]
    

import time
times = {}

times[-1] = time.time()
#  -infologger ${ALIHLT_DC_DIR}/lib/Linux-x86_64-release/libInfoLoggerLogServer.so
setup = {
    "Quiet" : [0],
    "Debug" : [0],
    "AutoReadoutListGeneration" : [0],
    }

commandLineSetupMap = {
    "-ddllist" : "DDLList",
    "-nodelist" : "NodeList",
    "-ecsinputlist" : "ECSInputList",
    "-ecsoutputlist" : "ECSOutputList",
    "-quiet" : "Quiet",
    "-debug" : "Debug",
    "-autoreadoutlistgeneration" : "AutoReadoutListGeneration",
    "-ecsdetectors" : "ECSDetectors",
    "-ecshlttrigger" : "ECSHLTTrigger",
    "-ecsruntype" : "ECSRunType",
    "-fileretriever" : "FileRetriever",
    "-readoutlistversion" : "ReadoutListVersion",
    "-ignorefile" : "IgnoreFile",
    }

envSetupMap = {
    "TMCONFIG_DDLLIST" : "DDLFile",
    "TMCONFIG_NODELIST" : "NodeFile",
    "TMCONFIG_ECSINPUTLIST" : "ECSInputList",
    "TMCONFIG_ECSOUTPUTLIST" : "ECSOutputList",
    "TMCONFIG_DEBUG" : "Debug",
    "TMCONFIG_AUTOREADOUTLISTGENERATION" : "AutoReadoutListGeneration",
    "TMCONFIG_ECSDETECTORS" : "ECSDetectors",
    "TMCONFIG_ECSHLTTRIGGER" : "ECSHLTTrigger",
    "TMCONFIG_ECSRUNTYPE" : "ECSRunType",
    "TMCONFIG_FILERETRIEVER" : "FileRetriever",
    "TMCONFIG_READOUTLISTVERSION" : "ReadoutListVersion",
    "TMCONFIG_IGNOREFILE" : "IgnoreFile",
    }

setupIntKeys = [
    "Quiet",
    "Debug",
    "AutoReadoutListGeneration",
    "ReadoutListVersion",
    ]

configfiles = []

for env in envSetupMap.keys():
    if os.environ.has_key( env ):
        SiteConfig.AddItem( setup, envSetupMap[env], os.environ[env] )


times[0] = time.time()

        

usagestring = " -config <config-filename> (-siteconfig <site-config-filename>) (-ddllist <ddl-list-xml-file>) (-nodelist <node-list-xml-file>) (-ecsinputlist <input-ddl-list-from-ecs>) (-ecsoutputlist <output-ddl-list-from-ecs>) (-quiet <quiet-flag (0 or 1)>) (-debug <debug-flag (0 or 1)>) (-autoreadlistgeneration <auto readout list flag (0 or 1)>) (-ecsdetectors <detector list from ECS (comma-separated)>) (-ecsruntype <runtype form ECS>) (-ignorefile <ignore-file-name> (ECS runtype must be specified)) -fileretriever <executable that will retrieve XML component snippets>"


try:
    argList = sys.argv[1:]
    while len(argList)>0:
        if argList[0]=="-siteconfig":
            if len(argList)<2:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -siteconfig." )
            try:
                SiteConfig.Read( setup, argList[1] )
            except SiteConfig.SiteConfigException,e:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\n"+str(e) )
        argList = argList[2:]
        

    argList = sys.argv[1:]
    while len(argList)>0:
        if argList[0]=="-config":
            if len(argList)<2:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -config." )
            configfiles.append(argList[1])
            argList = argList[2:]
        elif argList[0]=="-configlist":
            if len(argList)<2:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -configlist." )
            try:
                configinfile=open( argList[1], "r" )
            except IOError:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nUnable to open config list input file '"+argList[1]+"'." )
            line=configinfile.readline()
            while line != "":
                configfiles.append(line[:-1])
                line=configinfile.readline()
            configinfile.close()
            argList = argList[2:]
        elif argList[0]=="-siteconfig":
            if len(argList)<2:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -siteconfig." )
            argList = argList[2:]
            continue
        elif commandLineSetupMap.has_key(argList[0]):
            temp = string.Template( argList[1] )
            try:
                value = temp.substitute( os.environ )
            except KeyError,e:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nUnknown environment variable "+str(e) )
            SiteConfig.AddItem( setup, commandLineSetupMap[argList[0]], value )
            argList = argList[2:]
        else:
            raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nUnknown parameter '"+argList[0]+"'." )

    #print "Setup:",setup


    for i in setupIntKeys:
        if setup.has_key(i):
            try:
                if type(setup[i][-1])!=type(1):
                    setup[i][-1] = int(setup[i][-1],0)
            except ValueError:
                cmdLine="UNKNOWN"
                envEntry="UNKNOWN"
                for k in commandLineSetupMap.keys():
                    if commandLineSetupMap[k]==i:
                        cmdLine=k
                for k in envSetupMap.keys():
                    if envSetupMap[k]==i:
                        envEntry=k
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\n'"+i+"' option value '"+setup[i][-1]+"' (command line '"+cmdLine+"', environment variable '"+envEntry+"', xml site config '"+i+"') is not a number." )

    if len(configfiles)<=0:
        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nAt least one config file has to be specified." )

    if not setup.has_key("FileRetriever"):
        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nFile retriever binary has to be provided." )

    if not setup.has_key("ReadoutListVersion"):
        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nReadout list data format version has to be specified." )
    Detector.ReadoutListVersion = setup["ReadoutListVersion"][-1]

    ignores = {}
    if setup.has_key("IgnoreFile"):
        if not setup.has_key("ECSRunType"):
            raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nECS runtype (-ecsruntype/ECSRunType) must be specified for ignore-file to be usable." )
        ignorefilename = setup["IgnoreFile"][-1]
        try:
            ignorefile = open( ignorefilename, "r" )
        except IOError,e:
            raise SimpleChainConfig1.ChainConfigException( "Error opening ignore-file "+ignorefilename+": "+str(e) )
        line = ignorefile.readline()
        while line!="":
            if line[-1]=="\n":
                line = line[:-1]
            toks = line.split(":")
            allowed_ignore_types = ( "beamtype", "triggermenu", "detectors" )
            allowed_ignore_types_string = "("+allowed_ignore_types[0]
            for at in allowed_ignore_types[1:]:
                allowed_ignore_types_string += "|"+at
            allowed_ignore_types_string += ")"
            if len(toks)!=2 or not (toks[1] in allowed_ignore_types):
                print "toks:",str(toks)
                raise SimpleChainConfig1.ChainConfigException( "Incorrect ignore-file "+ignorefilename+": Lines must have the form '<runtype>:"+allowed_ignore_types_string+"'" )
            if not ignores.has_key(toks[0]):
                ignores[toks[0]] = set()
            ignores[toks[0]].add( toks[1] )
            line = ignorefile.readline()
        ignorefile.close()

    ECSRunType = None
    if setup.has_key("ECSRunType"):
        ECSRunType = setup["ECSRunType"][-1]
    if not ignores.has_key(ECSRunType):
        ignores[ECSRunType] = set()

    times[1] = time.time()

    # The following hack seems necessary as both os.getcwd and os.path.realpath seem to expand symlinks...
    pwdfile=os.popen("pwd")
    outputDir=pwdfile.read()[:-1]
    pwdfile.close()
    del(pwdfile)

    baseDir = os.path.dirname( os.path.realpath( sys.argv[0] ) )
    includePaths = [ outputDir+"/templates", baseDir+"/templates" ]

    times[2] = time.time()

    times[3] = time.time()

    listFileHandler = SimpleChainConfig1.ListFileHandler()

    resman = SimpleResourceManager.SimpleResourceManager()

    if setup.has_key( "NodeList" ):
        workerNodes = NodeList.ReadXML( setup["NodeList"][-1] )
        resman.SetWorkerNodes( workerNodes )

    if setup.has_key( "DDLList" ):
        ddlLocations = DDLLocations.ReadXML( setup["DDLList"][-1] )
        resman.SetDDLLocations( ddlLocations )

    missingComponents = set([None])

    
    if setup.has_key("ECSDetectors") and not ("detectors" in ignores[ECSRunType]):
        for detID in setup["ECSDetectors"][-1].split(","):
            lines = retrieve_file( setup["FileRetriever"][-1], "detectorbase", detector_id=detID )
            for line in lines:
                if line[:len("Error")]=="Error":
                    print "Error retrieving snippet "+detID+" of type 'detectorbase':"
                    for line2 in lines:
                        print line2
                    sys.exit( 2 )
                if line[:len("Warning")]=="Warning":
                    print line
            if len(lines)>=1:
                filename = lines[0]
            if not setup["Quiet"][-1]:
                print "Adding config file",filename
            configfiles.append( filename )
            if not ("beamtype" in ignores[ECSRunType]):
                lines = retrieve_file( setup["FileRetriever"][-1], "detectorbeam", detector_id=detID )
                for line in lines:
                    if line[:len("Error")]=="Error":
                        print "Error retrieving snippet "+detID+" of type 'detectorbeam':"
                        for line2 in lines:
                            print line2
                        sys.exit( 2 )
                    if line[:len("Warning")]=="Warning":
                        print line
                if len(lines)>=1:
                    filename = lines[0]
                    if not setup["Quiet"][-1]:
                        print "Adding config file",filename
                    configfiles.append( filename )
            lines = retrieve_file( setup["FileRetriever"][-1], "detectorrun", detector_id=detID )
            for line in lines:
                if line[:len("Error")]=="Error":
                    print "Error retrieving snippet "+detID+" of type 'detectorrun':"
                    for line2 in lines:
                        print line2
                    sys.exit( 2 )
                if line[:len("Warning")]=="Warning":
                    print line
            if len(lines)>=1:
                filename = lines[0]
                if not setup["Quiet"][-1]:
                    print "Adding config file",filename
                configfiles.append( filename )
            if not ("beamtype" in ignores[ECSRunType]):
                lines = retrieve_file( setup["FileRetriever"][-1], "detectorbeamrun", detector_id=detID )
                for line in lines:
                    if line[:len("Error")]=="Error":
                        print "Error retrieving snippet "+detID+" of type 'detectorbeamrun':"
                        for line2 in lines:
                            print line2
                        sys.exit( 2 )
                    if line[:len("Warning")]=="Warning":
                        print line
                if len(lines)>=1:
                    filename = lines[0]
                    if not setup["Quiet"][-1]:
                        print "Adding config file",filename
                    configfiles.append( filename )
    
    if setup.has_key("ECSHLTTrigger") and not ("triggermenu" in ignores[ECSRunType]):
        lines = retrieve_file( setup["FileRetriever"][-1], "triggerlist", snippet_id=setup["ECSHLTTrigger"][-1] )
        for line in lines:
            if line[:len("Error")]=="Error":
                print "Error retrieving triggerlist "+setup["ECSHLTTrigger"][-1]+":"
                for line2 in lines:
                    print line2
                sys.exit( 2 )
            if line[:len("Warning")]=="Warning":
                print line
        if len(lines)>=1:
            triggerListFile = lines[0]
        infile = open( triggerListFile, "r" )
        cl = infile.readline()
        while cl!="":
            if cl[-1]=="\n":
                cl = cl[:-1]
            if cl=="":
                continue
            lines = retrieve_file( setup["FileRetriever"][-1], "trigger", snippet_id=cl )
            for line in lines:
                if line[:len("Error")]=="Error":
                    print "Error retrieving snippet "+cl+" of type 'trigger':"
                    for line2 in lines:
                        print line2
                    sys.exit( 2 )
                if line[:len("Warning")]=="Warning":
                    print line
            if len(lines)>=1:
                filename = lines[0]
            if not setup["Quiet"][-1]:
                print "Adding config file",filename
            configfiles.append( filename )
            cl = infile.readline()
        infile.close()
                
            

    while len(missingComponents)>0:
        # print configfiles,"  --  ",missingComponents
        oldLen = len(configfiles)
        reader = ConfigReader.ConfigReader( configfiles )
    
        reader.SetRetrievalMode( True )
    
        if setup["Quiet"][-1]:
            reader.Quiet( setup["Quiet"][-1] )
        reader.SetResourceManager( resman )
    
    
        if setup.has_key( "DDLList" ):
            reader.SetDDLLocations( ddlLocations )
    
    
        if setup.has_key("ECSInputList"):
            reader.SetECSInputList(setup["ECSInputList"][-1])
        if setup.has_key("ECSOutputList"):
            reader.SetECSOutputList(setup["ECSOutputList"][-1])
        if setup.has_key("AutoReadoutListGeneration"):
            reader.SetAutoDetectorReadoutList( setup["AutoReadoutListGeneration"][-1] )
        if setup.has_key("ECSDetectors"):
            dets = set()
            for det in setup["ECSDetectors"][-1].split(","):
                if det[-4:]=="_HLT":
                    det = det[:-4]
                dets.add( det )
            reader.SetPartipicatingDetectors( dets )
    
    
        times[4] = time.time()
        config = reader.Read()
        times[5] = time.time()
    
        if len(reader.GetDDLList())<=0:
            raise SimpleChainConfig1.ChainConfigException( "Input DDL list obtained from ANDing input DDL list specified in configuration and DDL input list received from ECS do not overlap (empty intersection set from configuration input DDL list and ECS DDL input list)." )
    
        missingComponents = reader.GetMissingParentComponents()
        # print missingComponents
        # print reader.fProcessList
        # print reader.fProcesses

        iteration = 0
        # print "Iteration",iteration,":",missingComponents
        for mc in missingComponents:
            detID=None
            filename=None
            if mc.has_key("detector"):
                detID = mc["detector"]
            lines = retrieve_file( setup["FileRetriever"][-1], "component", snippet_id=mc["id"], detector_id=detID )
            for line in lines:
                if line[:len("Error")]=="Error":
                    if detID==None:
                        detID = "(No Detector)"
                    print "Error retrieving snippet "+detID+"/"+mc["id"]+" of type 'component':"
                    for line2 in lines:
                        print line2
                    sys.exit( 2 )
                if line[:len("Warning")]=="Warning":
                    print line
            if len(lines)>=1:
                filename = lines[0]
            if filename!=None and not filename in configfiles:
                if not setup["Quiet"][-1]:
                    print "Adding config file",filename
                configfiles.append( filename )
            # elif filename != None:
            #     print filename," already added"
            elif filename==None:
                print "No filename can be found"
                print lines
        iteration += 1

        # print "oldLen:",oldLen,"- len(configfiles):",len(configfiles),"- len(missingComponents):",len(missingComponents)
        if oldLen == len(configfiles) and len(missingComponents)>0:
            # print "No change, aborting"
            print "Missing componnents cannot be found:",missingComponents
            sys.exit(1)
                

    for cf in configfiles[:-1]:
        print cf,
    print configfiles[-1]
        
    

except (SimpleChainConfig1.ChainConfigException,ConfigReader.ConfigException, Exceptions.SCC2Exception), e:
    sys.stderr.write(  "Exception caught: "+e.fValue+"\n" )
    sys.stderr.write(  "\n" )
    if setup["Debug"][-1]:
        raise
    sys.exit( -1 )
    
        
if setup["Debug"][-1]:
    for i in range(0,16):
        if times.has_key(i) and times.has_key(i-1):
            print "dt%d: %f" % (i, times[i]-times[i-1])

