#!/usr/bin/env python

import libxml2
import os, sys, string, re

def IncNode( nodelist, node ):
    if not nodelist.has_key(node):
        nodelist[node] = 0
    nodelist[node] += 1

if len(sys.argv)<=1:
    sys.stderr.write( "Usage: "+sys.argv[0]+" <xml-input-filename>\n" )
    sys.exit(1)

totalNodeList = {}
for filename in sys.argv[1:]:
    print filename
    nodeList = {}
    xmlFile = libxml2.parseFile(filename)
    ctxt = xmlFile.xpathNewContext()

    procs = ctxt.xpathEval("/SimpleChainConfig1/Proc")
    for p in procs:
        procNodes = p.xpathEval("./Node")
        for n in procNodes:
            IncNode( totalNodeList, n.content )
            IncNode( nodeList, n.content )

    nodes = nodeList.keys()
    nodes.sort()
    for n in nodes:
        print "%30s : %d" % ( n, nodeList[n] )
    print "\n"

if len(sys.argv)>2:
    print "\nTotal"
    nodes = totalNodeList.keys()
    nodes.sort()
    for n in nodes:
        print "%30s : %d" % ( n, totalNodeList[n] )


