#!/usr/bin/env python

import Exceptions

class ResourceManagerInterface(object):
    #def __init__(self):
    #    pass

    def RequestNodes(self, procMult, procID, group, connectionCnt, ddlID=None, slice=None, detID=None, existingNodeList=[], lowLoad=False, forceFEP = False, parentNodes=[], placeExistingNodes=True, instanceWeight=1.0 ):
        self.NotImplemented( "ResourceManager.GetNodes" )

    def GetNodesWithAttributes( self, attributeList ):
        # attributeList is dictionary, attrnames are keys, allowed attrValues are in list
        self.NotImplemented( "ResourceManager.GetNodesWithAttributes" )

    def RestrictNodesWithAttributes( self, attributeList ):
        self.NotImplemented( "ResourceManager.RestrictNodesWithAttributes" )

    def RestrictDDLs( self, ddlList ):
        self.NotImplemented( "ResourceManager.RestrictDDLs" )

    def PlaceProcess( self, procID, existingNodeList, group, connectionCnt=0, lowLoad=False ):
        self.NotImplemented( "ResourceManager.PlaceProcess" )
        
    def ReleaseNodes(self, procID, existingNodeList ):
        self.NotImplemented( "ResourceManager.ReleaseNodes" )

    def GetNodePlatform( self, node ):
        self.NotImplemented( "ResourceManager.GetNodePlatform" )

    def GetNodeHostname( self, node ):
        self.NotImplemented( "ResourceManager.GetNodeHostname" )

    def GetDDLLocation( self, ddl ):
        self.NotImplemented( "ResourceManager.GetDDLLocation" )

    def GetNodesForGroup( self, group ):
        self.NotImplemented( "ResourceManager.GetNodesForGroup" )

    def GetUtilizationList( self ):
        self.NotImplemented( "ResourceManager.GetUtilizationList" )

    def NotImplemented(self,sig):
        raise Exception.SC2Exception( sig+" not implemented - Abstract Base Class" )

