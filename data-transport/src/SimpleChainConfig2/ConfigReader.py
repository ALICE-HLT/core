#!/usr/bin/env python

import libxml2
import re
import string
import os
import SimpleChainConfig1
import XMLConfigReader
import Exceptions
import Detector
import time

class ConfigException(Exceptions.SCC2Exception):
    def __init__(self,message):
        Exceptions.SCC2Exception.__init__(self,message)
        pass
    

class ConfigReader(object):
    def __init__( self, filenames ):
        self.fQuiet = False
        self.fFilenames = filenames
        self.fXmlFiles = []
        self.fXPathContexts = []
        for filename in filenames:
            self.fXmlFiles.append( libxml2.parseFile(filename) )
            self.fXPathContexts.append( self.fXmlFiles[-1].xpathNewContext() )
        ### XXX MULT
        self.fFilename = self.fFilenames[0]
        self.fXmlFile = self.fXmlFiles[0]
        self.fXPathContext = self.fXPathContexts[0]
        self.fWorkerNodes = []
        self.fSourceType="ddl"
        self.fSourceFileDirs = []
        self.fSourceHWCoProcDirs = []
        self.fOutputType = "ddl"
        self.fOutputDir = ""
        self.fOutputPrefix = ""
        self.fOutputNodes = []
        self.fOutputShmSize = None
        self.fFormatterShmBlocksize = None
        self.fFormatterShmBlockcount = None
        self.fFormatterShmType = None
        self.fOutputShmType = None
        self.fOutputParents = set()
        self.fOutputParentFilters = {}
        self.fRequiredOutputContributors = set()
        self.fDDLs = set()
        self.fConfigDDLs = set()
        #self.fRORCs = { "GENERIC" : {"options":"","blocksize":0,"blockcount":0} }
        self.fRORCs = { "GENERIC.enable" : True, "GENERIC.options" : "", "GENERIC.blocksize":0, "GENERIC.blockcount":0, "GENERIC.fileoptions" : "", "GENERIC.hwcoproc" : False, "GENERIC.hwcoprocrorcoptions" : None, "GENERIC.hwcoprocfileoptions" : None, "GENERIC.hwcoprocblocksize" : 0, "GENERIC.hwcoprocblockcount" : 0 }
        # "GENERIC.datatype" : None,
        # "GENERIC.hwcoprocdatatype" : None, 
        # "GENERIC.datatype" : "DDL_RWPK",
        # "GENERIC.hwcoprocdatatype" : "DDLPRPRC", 
        self.fDDLLocations = {}
        self.fOutputDDLs = set()
        self.fOutputFormatterMultiplicity = -1
        self.fOutputWriterOptions = ""
        self.fHighestDumpID = 0
        self.fProcesses = { "ALICE" : {} }
        self.fResMan = None
        self.fDefaultTDSPort = 65000
        self.fNodeHierarchy = False
        self.fDetailedNodeHierarchy = False
        self.fExplicitSourceOptions = False
        self.fNodeGroups = {}
        self.fTDSPorts = {}
        self.fNodeTDS = True
        self.fECSInputList = None
        self.fECSOutputList = None
        self.fMaxEventAge = None
        self.fVerbosity = None
        self.fVerbosityExclude = None
        self.fFXSBase = None
        self.fFXSDB = None
        self.fFXSNodes = []
        self.fFXSParents = []
        self.fFXSParentFilters = {}
        self.fRequiredFXSContributors = set()
        self.fFXSDBUser = None
        self.fFXSDBPwd = None
        self.fTriggerTypes = [ "readout", "monitor" ]
        self.fDefaultTriggerType = self.fTriggerTypes[0]
        self.fOutputTriggerType = self.fTriggerTypes[0]
        self.fTriggerTypeComponentSpecifier = { "readout" : "", "monitor" : "Monitor" }
        self.fTriggerParents = dict([(x,[]) for x in self.fTriggerTypes ])
        self.fTriggerMultiplicities = dict([(x,None) for x in self.fTriggerTypes ])
        self.fTriggerNodes = dict([(x,[]) for x in self.fTriggerTypes ])
        self.fTriggerParentFilters = dict([(x,{}) for x in self.fTriggerTypes ])
        self.fRequiredTriggerContributors = dict([(x,set()) for x in self.fTriggerTypes ])
        self.fParticipatingDetectors = set()
        self.fTriggeredDetectors = set()
        self.fAutoDetectorReadoutList = False
        self.fRequiredContributors = set()
        self.fAllContributors = set()
        self.fProcessList = {}
        self.fRetrievalMode = False
        self.f2LevelDetectors = [ "FMD", "ACORDE", "TRG", "ZDC", "V0", "T0", "PMD", "AD", "CPV" ]
        self.f3LevelDetectors = [ "TRD", "TPC", "PHOS", "EMCAL", "SPD", "SSD", "SDD", "TOF", "HMPID" ]
        self.fCTPTriggerClassGroups = {}
        self.fECSRunType = None
        self.fMacros = {}
        self.fPreDefinedMacros = ["ID", "DDL", "DETECTOR", ]
        self.fNodeAliasList = {}
        self.fProcessNodeRequestData = {}
        self.fFiles = {}
        self.fConfigEntries = {}
        self.fAllowedConfigEntries = ( )
        self.fAWSOptions = ""
        self.fAWSAddLibraries = []
        self.fAWSAddLibrariesBefore = {}
        self.fCanTraceCacheA = {}
        self.fCanTraceCacheB = {}

    def Quiet( self, quiet ):
        self.fQuiet = quiet

    def AddAWSOption( self, opt ):
        self.fAWSOptions += opt

    def SetAWSOption( self, opt ):
        self.fAWSOptions = opt

    def AddAWSLibrary( self, lib ):
        self.fAWSAddLibraries.append( lib )

    def AddAWSLibraryBefore( self, lib, beforeLib ):
        self.fAWSAddLibraries.append( lib )
        self.fAWSAddLibrariesBefore[lib] = beforeLib

    def SetRetrievalMode( self, mode ):
        self.fRetrievalMode = mode

    def SetWorkerNodes( self, workerNodeList ):
        self.fWorkerNodes += workerNodeList

    def SetResourceManager( self, resman ):
        self.fResMan = resman

    def SetDDLLocations( self, ddlLocs ):
        for dl in ddlLocs.keys():
            self.fDDLLocations[dl] = ddlLocs[dl]

    def CheckForNode( self, nodeID, nodeType=None ):
        if self.fResMan.GetNodePlatform(nodeID)==None:
            if nodeType==None:
                msg = "Specified node '"+nodeID+"' cannot be found"
            else:
                msg = "Specified "+nodeType+" node '"+nodeID+"' cannot be found"
            raise ConfigException( msg )

            
    def GetNodePlatform( self, nodeID ):
        platform = self.fResMan.GetNodePlatform( self.GetNodeAliasID( nodeID ) )
        if platform==None:
            if self.GetNodeAliasID( nodeID )!=nodeID:
                ConfigException( "Aliased node ID "+str(self.GetNodeAliasID( nodeID ))+" from original ID "+nodeID+" cannot be found in resource manager when trying to determine node platform." )
            else:
                ConfigException( "Node ID "+nodeID+" cannot be found in resource manager when trying to determine node platform." )
        return platform

    def GetNodeHostname( self, nodeID ):
        hostname = self.fResMan.GetNodeHostname( self.GetNodeAliasID( nodeID ) )
        if hostname==None:
            if self.GetNodeAliasID( nodeID )!=nodeID:
                ConfigException( "Aliased node ID "+str(self.GetNodeAliasID( nodeID ))+" from original ID "+nodeID+" cannot be found in resource manager when trying to determine node hostname." )
            else:
                ConfigException( "Node ID "+nodeID+" cannot be found in resource manager when trying to determine node hostname." )
        return hostname

    def SetNodeHostnameAliasList( self, lst ):
        self.fNodeAliasList = lst

    def GetNodeAliasID( self, hostID ):
        if self.fNodeAliasList.has_key(hostID):
            return self.fNodeAliasList[hostID]
        else:
            return hostID

    def SetECSInputList( self, inputList ):
        self.fECSInputList = Detector.ECSReadoutListString2DDLList(inputList)
        if len(self.fDDLs)>=0:
            self.fDDLs &= self.fECSInputList

    def SetECSOutputList( self, outputList ):
        self.fECSOutputList = Detector.ECSReadoutListString2DDLList(outputList)
        if len(self.fOutputDDLs)>0:
            if len(self.fECSOutputList-self.fOutputDDLs)>0:
                raise ConfigException( "Some ECS specified output links are not specified in configuration output links ( "+str(self.fECSOutputList)+" - "+str(self.fOutputDDLs)+" = "+str(self.fECSOutputList-self.fOutputDDLs)+" )" )
            self.fOutputDDLs &= self.fECSOutputList

    def SetECSRunType( self, runType ):
        self.fECSRunType = runType

    def SetPartipicatingDetectors( self, detectors ):
        self.fParticipatingDetectors = detectors

    def SetAutoDetectorReadoutList( self, arl ):
        self.fAutoDetectorReadoutList = arl

    def SetMaxEventAge( self, age ):
        self.fMaxEventAge = age

    def SetVerbosity( self, verbosity, verbosityExclude ):
        self.fVerbosity = verbosity
        self.fVerbosityExclude = verbosityExclude


    def GetDDLList( self ):
        return self.fDDLs

    def GetCTPTriggerClassGroups( self ):
        return self.fCTPTriggerClassGroups

    def AddRequiredContributor( self, par ):
        self.fRequiredContributors.add( par )

    def AddContributor( self, contrib ):
        self.fAllContributors.add( contrib )

    def AddMacros( self, macroDict ):
        for macro in macroDict.keys():
            if self.fMacros.has_key(macro):
                raise ConfigException( "Duplicate macro definition '"+macro+"'" )
            if macro in self.fPreDefinedMacros:
                raise ConfigException( "Macro definition '"+macro+"' conflicts with pre-defined macro" )
            self.fMacros[macro] = macroDict[macro]

    def GetXMLFilenameFromXMLContext( self, xmlContext ):
        for n in range(len(self.fXPathContexts)):
            if self.fXPathContexts[n]==xmlContext:
                return self.fFilenames[n]

    def SetAllowedConfigEntries( self, configEntries ):
        self.fAllowedConfigEntries = tuple ( list(configEntries)[:] )

    def ReadConfigEntries( self ):
        for ctxt in self.fXPathContexts:
            self.ReadNodeConfigEntries( ctxt.xpathEval("/SimpleChainConfig2")[0] )
            self.ReadNodeConfigEntries( ctxt.xpathEval("/SimpleChainConfig2/ALICE")[0] )

    def ReadNodeConfigEntries( self, xmlNode ):
        for ace in self.fAllowedConfigEntries:
            ces = xmlNode.xpathEval( "./Config/"+ace )
            if len(ces)>0:
                if not self.fConfigEntries.has_key(ace):
                    self.fConfigEntries[ace] = []
                for n in ces:
                    self.fConfigEntries[ace].append( n.content )

    def SetConfigEntry( self, key, valueList ):
        self.fConfigEntries[key] = valueList[:]

    def AddConfigEntry( self, key, value ):
        if not self.fConfigEntries.has_key(key):
            self.fConfigEntries[key] = []
        self.fConfigEntries[key].append( value )
        
    def GetConfigEntries( self ):
        return self.fConfigEntries

    def Read( self ):
#TIME        times = {}
#        times[-1] = time.time()
        
        if len(self.fXPathContexts[0].xpathEval("/SimpleChainConfig1"))==1:
            if len(self.fXPathContexts)>1:
                raise ConfigException( "Only one SimpleChainConfig1 XML file allowed" )
            configReader = XMLConfigReader.XMLConfigReader(self.fFilenames[0])
            config = configReader.ReadSimpleChainConfig()
#TIME            times[0] = time.time()
#            print "dconf%d: %f" % (0, times[0]-times[-1])
            return config
##        configNodes = []
##        for context in self.fXPathContexts:
##            if len(context.xpathEval("/SimpleChainConfig2"))!=1:
##                raise ConfigException( "Exactly one top level SimpleChainConfig2 node allowed and required in XML file" )
##            configNodes.append( self.fXPathContext.xpathEval("/SimpleChainConfig2")[0] )
##        ### XXX MULT
##        configNode = configNodes[0]

        #print configNode
        #print dir(configNode)
        #tmp=self.fXPathContext.xpathEval("/SimpleChainConfig2/ALICE/TPC/Slice")[0]
        #print tmp.xpathEval( "@ID")[0].content
        #print tmp.xpathEval( "Partition" )[0].content
        #return
        # Get the attributes of an xml node
        # Initial node determines ID
        name = self.fXPathContexts[0].xpathEval("/SimpleChainConfig2/@ID")[0]
        self.fConfig = SimpleChainConfig1.SimpleChainConfig( name.content )
#TIME        times[0] = time.time()
        verb = 0
        if self.fVerbosity!=None:
            verb |= self.fVerbosity
        for ctxt in self.fXPathContexts:
            verbosity = ctxt.xpathEval("/SimpleChainConfig2/@verbosity")[0]
            if verbosity:
                verb |= int(verbosity.content,0)

#TIME        times[1] = time.time()
        if self.fVerbosityExclude!=None:
            verb &= ~self.fVerbosityExclude
        verb = "0x%X" % verb
        self.fConfig.SetVerbosity(verb)

        self.fResMan.RestrictNodesWithAttributes( { "FXS" : [] } )
        self.fResMan.RestrictNodesWithAttributes( { "HLTOUT" : [] } )

#TIME        times[2] = time.time()
        self.ReadALICE()

#TIME        times[3] = time.time()
        self.AssignNodes()

#TIME        times[4] = time.time()
        self.LoadBalance()

#TIME        times[5] = time.time()
        self.CreateNodeGroups()

#TIME        times[6] = time.time()
        self.CreateNodeTDSs()

#TIME        times[7] = time.time()
        self.fConfig.MakeGroupLevels()

#TIME        times[8] = time.time()
#        ndxs = times.keys()
#        ndxs.sort()
#        for i in ndxs:
#            if times.has_key(i) and times.has_key(i-1):
#                print "dconf%d: %f" % (i, times[i]-times[i-1])

        return self.fConfig

    def ReadALICE( self ):
#TIME        times = {}
#        times[-1] = time.time()
        
        count=0
        for ctxt in self.fXPathContexts:
            if len(ctxt.xpathEval("/SimpleChainConfig2/ALICE"))!=1:
                raise ConfigException( "Exactly one SimpleChainConfig2/ALICE node allowed in "+self.fFilenames[count]+" XML file" )
            count += 1

        count=0
        for ctxt in self.fXPathContexts:
            macros = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Macro" )
            for macro in macros:
                IDs = macro.xpathEval( "./@ID" )
                if len(IDs)>0:
                    ID = IDs[0].content
                    content = macro.content
                else:
                    toks = macro.content.split("=")
                    if len(toks)!=2:

                        raise ConfigException( "Macro definition '"+macro.content+"' does not match required syntax in "+self.fFilenames[count]+" XML file" )
                    ID = toks[0]
                    content = toks[1]
                if self.fMacros.has_key(ID):
                    raise ConfigException( "Duplicate macro definition '"+macro.content+"' (duplicate found in "+self.fFilenames[count]+" XML file)" )
                if ID in self.fPreDefinedMacros:
                    raise ConfigException( "Macro definition '"+macro.content+"' in "+self.fFilenames[count]+" XML file conflicts with pre-defined macro" )
                self.fMacros[ID] = content
            count += 1

        self.ReadCTPTriggerClassGroups()
        self.ReadSources()
        for triggerType in self.fTriggerTypes:
            self.ReadTrigger( triggerType )

        blockSizes = []
        blockCounts = []

#TIME        times[0] = time.time()
            
        self.ReadOutput()
        self.ReadFXS()

                
        for ctxt in self.fXPathContexts:
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/RORCShm/@blocksize" ))>0:
                blockSizes.append( ctxt.xpathEval( "/SimpleChainConfig2/ALICE/RORCShm/@blocksize" )[0] )
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/RORCShm/@blockcount" ))>0:
                blockCounts.append( ctxt.xpathEval( "/SimpleChainConfig2/ALICE/RORCShm/@blockcount" )[0] )
        if len(blockSizes)<=0:
            raise ConfigException( "ALICE/RORCShm blocksize needs to be set in one configuration file." )
        if len(blockSizes)>1 and not self.fQuiet:
            print "Warning: ALICE/RORCShm blocksize must only be set in one configuration file. Using only first occurence."
        if len(blockCounts)<=0:
            raise ConfigException( "ALICE/RORCShm blockcount needs to be set" )
        if len(blockCounts)>1 and not self.fQuiet:
            print "Warning: ALICE/RORCShm blockcount must only be set in one configuration file. Using only first occurence."
        for ctxt in self.fXPathContexts:
            self.ApplyDetectorOptions( ctxt.xpathEval( "/SimpleChainConfig2/ALICE" )[0], None, False )
        #print "self.fRORCs:",self.fRORCs
        
#TIME        times[1] = time.time()
        self.ReadDimuon()
        for det in self.f2LevelDetectors:
            self.Read2LevelDetector(det)

        for det in self.f3LevelDetectors:
            self.Read3LevelDetector(det,Detector.Get3LevelDetectorLevel1ElementName(det))
#TIME        times[2] = time.time()

        remDetectors = self.fParticipatingDetectors - self.fTriggeredDetectors
        remDetectors = set() # Feature effectively disabled for now ; inconsistencies , e.g. TPC was treated as 2 level detector
        for det in remDetectors:
            self.Read2LevelDetector(det,True) # Read default entry for this detector.
        # print self.fProcesses
        # print self.fProcessList

        topProcesses = self.ReadALICEProcesses()
        self.fProcessList["ALICE"] = self.CheckALICEProcesses( topProcesses )

#TIME        times[3] = time.time()

        self.MakeDimuonProcesses()
        for det in self.f2LevelDetectors:
            self.Make2LevelDetectorProcesses(det)

        for det in self.f3LevelDetectors:
            self.Make3LevelDetectorProcesses(det,Detector.Get3LevelDetectorLevel1ElementName(det))

        # for det in remDetectors:
        #     self.Make2LevelDetectorProcesses(det) # Read default entry for this detector.

#TIME        times[4] = time.time()
        for triggerType in self.fTriggerTypes:
            self.CheckTrigger( triggerType )
            tmpDict = self.CreateTriggerProcesses( triggerType )
            for k in tmpDict.keys():
                self.fProcessList["ALICE"][k] = tmpDict[k]

#TIME        times[4] = time.time()
        for proc in self.fProcessList["ALICE"].keys():
            self.MakeProcess( self.fProcessList["ALICE"][proc], self.fProcesses.keys(), detID="ALICE" )

#TIME        times[5] = time.time()

        self.CheckOutput()
        tmpDict = self.CreateOutputProcesses()
        for k in tmpDict.keys():
            self.fProcessList["ALICE"][k] = tmpDict[k]
        for k in tmpDict.keys():
            self.MakeProcess( tmpDict[k], self.fProcesses.keys(), detID="ALICE" )
            
#TIME        times[6] = time.time()
        self.CheckFXS()
        tmpDict = self.CreateFXSProcesses()
        for k in tmpDict.keys():
            self.fProcessList["ALICE"][k] = tmpDict[k]
        for k in tmpDict.keys():
            self.MakeProcess( tmpDict[k], self.fProcesses.keys(), detID="ALICE" )
#TIME        times[7] = time.time()

#TIME        ndxs = times.keys()
#        ndxs.sort()
#        for i in ndxs:
#            if times.has_key(i) and times.has_key(i-1):
#                print "dconfalice%d: %f" % (i, times[i]-times[i-1])


    def ReadCTPTriggerClassGroups( self ):
        for ctxt in self.fXPathContexts:
            groups = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/CTP/TriggerClassGroup" )
            for g in groups:
                if len(g.xpathEval( "./@id" ))<=0:
                    raise ConfigException( "CTPTriggerClassGroup must have an 'id' attribute." )
                ID = g.xpathEval( "./@id" )[0].content
                pattern = g.content
                self.fCTPTriggerClassGroups[ID] = pattern



    def GetMissingParentComponents( self ):
        components = []
        # for ci in self.GetMissingTPCParentComponents():
        #     components.append( { "id" : ci, "detector" : "TPC" } )
        # print "TPC :",components

        for ci in self.GetMissingDimuonParentComponents():
            components.append( { "id" : ci, "detector" : "DIMU" } )
        # print "DIMU :",components

        # for ci in self.GetMissingPHOSParentComponents():
        #     components.append( { "id" : ci, "detector" : "PHOS" } )
        # print "PHOS :",components

        for det in self.f2LevelDetectors:
            for ci in self.GetMissing2LevelDetectorParentComponents(det):
                components.append( { "id" : ci, "detector" : det } )
            # print det,":",components

        for det in self.f3LevelDetectors:
            for ci in self.GetMissing3LevelDetectorParentComponents(det,Detector.Get3LevelDetectorLevel1ElementName(det)):
                components.append( { "id" : ci, "detector" : det } )
            # print det,":",components

        for ci in self.GetMissingALICEParentComponents():
            if len(ci.split("/"))<=1:
                components.append( { "id" : ci, "detector" : "ALICE" } )
            else:
                components.append( { "id" : ci.split("/")[-1], "detector" : ci.split("/")[0] } )
        # print "ALICE :",components
        return components


    def ReadSources( self ):

        if self.fRetrievalMode:
            self.fDDLs = Detector.NameToDDLList( "ALL" ) - Detector.NameToDDLList( "HLT" )
            return

        self.fSourceType = None
        specificRuntypeSourceTypeSet = False
        for ctxt in self.fXPathContexts:
            sources = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Sources" )
            if len(sources)<=0:
                continue
            for s in sources:
                runTypes = s.xpathEval( "./@runtype" )
                if len(runTypes)>0 and (((len(runTypes[0].content)<=0 or runTypes[0].content[0]!="!") and self.fECSRunType!=runTypes[0].content) or (len(runTypes[0].content)>0 and runTypes[0].content[0]=="!" and self.fECSRunType==runTypes[0].content[1:])) and (self.fECSRunType!=None or runTypes[0].content!="" ):
                    continue
                sourceTypes = s.xpathEval( "./@type" )
                # if len(sourceTypes)<=0:
                #    raise ConfigException( "Source tag type attribute has to be specified ('file' or 'ddl')" )
                if len(sourceTypes)>0:
                    if self.fSourceType==None or (len(runTypes)>0 and not specificRuntypeSourceTypeSet):
                        self.fSourceType = sourceTypes[0].content
                        if len(runTypes)>0:
                            specificRuntypeSourceTypeSet = True
                    elif not self.fQuiet and (len(runTypes)>0 or  not specificRuntypeSourceTypeSet):
                        print "Warning: Source tag type attribute must only be specified in one configuration file. Using first occurrance..."
        if self.fSourceType == None:
            raise ConfigException( "Source tag type attribute has to be specified in one configuration file ('file' or 'ddl')" )
        if self.fSourceType!="file" and self.fSourceType!="ddl":
            raise ConfigException( "Wrong source type '%s'. Only 'file' or 'ddl' allowed" )
        if self.fSourceType=="file":
            for ctxt in self.fXPathContexts:
                sources = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Sources" )
                for s in sources:
                    runTypes = s.xpathEval( "./@runtype" )
                    if len(runTypes)>0 and (((len(runTypes[0].content)<=0 or runTypes[0].content[0]!="!") and self.fECSRunType!=runTypes[0].content) or (len(runTypes[0].content)>0 and runTypes[0].content[0]=="!" and self.fECSRunType==runTypes[0].content[1:])) and (self.fECSRunType!=None or runTypes[0].content!=""):
                        continue
                    dirNodes = s.xpathEval( "./Directory" )
                    for dn in dirNodes:
                        self.fSourceFileDirs.append( dn.content )
                    dirNodes = s.xpathEval( "./HWCoProcDirectory" )
                    for dn in dirNodes:
                        self.fSourceHWCoProcDirs.append( dn.content )
        
        #print self.fSourceFileDirs
        for ctxt in self.fXPathContexts:
            sources = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Sources" )
            for s in sources:
                runTypes = s.xpathEval( "./@runtype" )
                if len(runTypes)>0 and (((len(runTypes[0].content)<=0 or runTypes[0].content[0]!="!") and self.fECSRunType!=runTypes[0].content) or (len(runTypes[0].content)>0 and runTypes[0].content[0]=="!" and self.fECSRunType==runTypes[0].content[1:])) and (self.fECSRunType!=None or runTypes[0].content!=""):
                    continue
                ddls = s.xpathEval( "./DDL" )
                for ddl in ddls:
                    self.fDDLs |= self.ParseList( ddl.content, numbers=True, StringLookup=Detector.NameToDDLList )
        if len(self.fDDLs)<=0:
            self.fDDLs = self.ParseList( "ALL", numbers=True, StringLookup=Detector.NameToDDLList )
            self.fDDLs -= self.ParseList( "HLT", numbers=True, StringLookup=Detector.NameToDDLList )

        DDLExcludes = set()
        for ctxt in self.fXPathContexts:
            sources = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Sources" )
            for s in sources:
                runTypes = s.xpathEval( "./@runtype" )
                if len(runTypes)>0 and (((len(runTypes[0].content)<=0 or runTypes[0].content[0]!="!") and self.fECSRunType!=runTypes[0].content) or (len(runTypes[0].content)>0 and runTypes[0].content[0]=="!" and self.fECSRunType==runTypes[0].content[1:])) and (self.fECSRunType!=None or runTypes[0].content!=""):
                    continue
                ddlexcludes = s.xpathEval( "./DDLExclude" )
                for exddl in ddlexcludes:
                    DDLExcludes |= self.ParseList( exddl.content, numbers=True, StringLookup=Detector.NameToDDLList )
        self.fDDLs -= DDLExcludes
        self.fConfigDDLs = self.fDDLs
        if self.fECSInputList!=None:
            self.fDDLs &= self.fECSInputList

        nodeSet = set()
        for ddl in self.fDDLs:
            if not ddl in self.fDDLLocations.keys():
                raise ConfigException( "Location of DDL with ID %d not known in DDL list" % ddl )
            if self.fResMan.GetDDLLocation(ddl)==None:
                raise ConfigException( "Location of DDL with ID %d not known in node list" % ddl )
            nodeSet |= set([self.fDDLLocations[ddl]["node"]])
        for ID in nodeSet:
            self.fConfig.AddNode( SimpleChainConfig1.Node(ID, self.GetNodeHostname(ID), self.GetNodePlatform(ID)) )
        self.fResMan.RestrictDDLs( self.fDDLs )


        for ddl in self.fDDLs:
            if not self.fRORCs["GENERIC.enable"] and not (self.fRORCs.has_key("%d.enable" % ddl) and self.fRORCs["%d.enable" % ddl]) and not self.fRORCs["GENERIC.hwcoproc"] and not (self.fRORCs.has_key("%d.hwcoproc" % ddl) and self.fRORCs["%d.hwcoproc" % ddl]):
                raise ConfigException( ( "Neither normal DDL/RORC readout nor HW-CoProcessor activated for DDL %d)" ) % (ddl) )
        #print "Nodes: ["
        #for node in self.fConfig.GetNodes():
        #    print node,","
        #print "]"

    def ReadOutput( self ):

        self.fOutputType = None
        specificRuntypeOutputTypeSet = False
        outputFound=False
        for ctxt in self.fXPathContexts:
            currentFile = self.GetXMLFilenameFromXMLContext( ctxt )
            outputs = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Output" )
            if len(outputs)<=0:
                continue
            if not self.fFiles.has_key("ALICE/Output"):
                self.fFiles["ALICE/Output"] = { "files" : set(), "ddls" : set(), "parents" : set(), "optparents" : set() }
            for output in outputs:
                self.fFiles["ALICE/Output"]["files"].add( currentFile )
                runTypes = output.xpathEval( "./@runtype" )
                if len(runTypes)>0 and (((len(runTypes[0].content)<=0 or runTypes[0].content[0]!="!") and self.fECSRunType!=runTypes[0].content) or (len(runTypes[0].content)>0 and runTypes[0].content[0]=="!" and self.fECSRunType==runTypes[0].content[1:])) and (self.fECSRunType!=None or runTypes[0].content!=""):
                    continue
                outputFound=True
                outputTypes = output.xpathEval( "./@type" )
                if len(outputTypes)>0:
                    if self.fOutputType==None or (len(runTypes)>0 and not specificRuntypeOutputTypeSet):
                        self.fOutputType = outputTypes[0].content
                        if len(runTypes)>0:
                            specificRuntypeOutputTypeSet = True
                    elif not self.fQuiet and (len(runTypes)>0 or  not specificRuntypeOutputTypeSet):
                        print "Warning: Output tag type attribute must only be specified in one configuration file. Using first occurrence..."
        if not outputFound:
            return
        if self.fOutputType == None:
            raise ConfigException( "Output tag type attribute has to be specified in one configuration file ('file' or 'ddl')" )
        #if self.fOutputType!="file":
        # if True:
        if self.fRetrievalMode:
            self.fOutputDDLs = Detector.NameToDDLList( "HLT" )
        else:
            for ctxt in self.fXPathContexts:
                outputs = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Output" )
                for output in outputs:
                    runTypes = output.xpathEval( "./@runtype" )
                    if len(runTypes)>0 and (((len(runTypes[0].content)<=0 or runTypes[0].content[0]!="!") and self.fECSRunType!=runTypes[0].content) or (len(runTypes[0].content)>0 and runTypes[0].content[0]=="!" and self.fECSRunType==runTypes[0].content[1:])) and (self.fECSRunType!=None or runTypes[0].content!=""):
                        continue
                    ddls = output.xpathEval( "./DDL" )
                    for ddl in ddls:
                        self.fOutputDDLs |= self.ParseList( ddl.content, numbers=True, StringLookup=Detector.NameToDDLList )
            if len(self.fOutputDDLs)<=0:
                self.fOutputDDLs = self.ParseList( "HLT", numbers=True, StringLookup=Detector.NameToDDLList )
            OutputDDLExcludes = set()
            for ctxt in self.fXPathContexts:
                outputs = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Output" )
                for output in outputs:
                    runTypes = output.xpathEval( "./@runtype" )
                    if len(runTypes)>0 and (((len(runTypes[0].content)<=0 or runTypes[0].content[0]!="!") and self.fECSRunType!=runTypes[0].content) or (len(runTypes[0].content)>0 and runTypes[0].content[0]=="!" and self.fECSRunType==runTypes[0].content[1:])) and (self.fECSRunType!=None or runTypes[0].content!=""):
                        continue
                    ddlexcludes = output.xpathEval( "./DDLExclude" )
                    for exddl in ddlexcludes:
                        OutputDDLExcludes |= self.ParseList( exddl.content, numbers=True, StringLookup=Detector.NameToDDLList )
            self.fOutputDDLs -= OutputDDLExcludes
            if self.fECSOutputList!=None:
                if len(self.fECSOutputList-self.fOutputDDLs)>0:
                    raise ConfigException( "Some ECS specified output links are not specified in configuration output links ( "+str(self.fECSOutputList)+" - "+str(self.fOutputDDLs)+" = "+str(self.fECSOutputList-self.fOutputDDLs)+" )" )
                self.fOutputDDLs &= self.fECSOutputList
            self.fFiles["ALICE/Output"]["ddls"] = self.fOutputDDLs


            nodeSet = set()
            for ddl in self.fOutputDDLs:
                if not ddl in self.fDDLLocations.keys():
                    raise ConfigException( "Location of DDL with ID %d not known" % ddl )
                nodeSet |= set([self.fDDLLocations[ddl]["node"]])
            for ID in nodeSet:
                if self.fConfig.GetNode(ID)==None:
                    self.fConfig.AddNode( SimpleChainConfig1.Node(ID, self.GetNodeHostname(ID), self.GetNodePlatform(ID)) )
            self.fResMan.RestrictDDLs( self.fOutputDDLs )

        owOptCnt = 0
        owOpts = None
        for ctxt in self.fXPathContexts:
            outputs = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Output" )
            for output in outputs:
                runTypes = output.xpathEval( "./@runtype" )
                if len(runTypes)>0 and (((len(runTypes[0].content)<=0 or runTypes[0].content[0]!="!") and self.fECSRunType!=runTypes[0].content) or (len(runTypes[0].content)>0 and runTypes[0].content[0]=="!" and self.fECSRunType==runTypes[0].content[1:])) and (self.fECSRunType!=None or runTypes[0].content!=""):
                    continue
                owOptCnt += len(output.xpathEval( "./OutWriterOptions" ))
                if len(output.xpathEval( "./OutWriterOptions" ))>0 and owOpts==None:
                    owOpts = output.xpathEval( "./OutWriterOptions" )[0].content
        
        if owOptCnt>1:
                raise ConfigException( "At most one OutWriterOptions node can be specified for Output node." )
        if owOpts!=None:
            self.fOutputWriterOptions = owOpts
        else:
            self.fOutputWriterOptions = ""
            
        optCnt = 0
        self.fOutputFormatterMultiplicity = -1
        for ctxt in self.fXPathContexts:
            outputs = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Output" )
            for output in outputs:
                runTypes = output.xpathEval( "./@runtype" )
                if len(runTypes)>0 and (((len(runTypes[0].content)<=0 or runTypes[0].content[0]!="!") and self.fECSRunType!=runTypes[0].content) or (len(runTypes[0].content)>0 and runTypes[0].content[0]=="!" and self.fECSRunType==runTypes[0].content[1:])) and (self.fECSRunType!=None or runTypes[0].content!=""):
                    continue
                optCnt += len(output.xpathEval( "./FormatterMultiplicity" ))
                if len(output.xpathEval( "./FormatterMultiplicity" ))>0 and self.fOutputFormatterMultiplicity==-1:
                    try:
                        self.fOutputFormatterMultiplicity = int(output.xpathEval( "./FormatterMultiplicity" )[0].content)
                    except ValueError:
                        raise ConfigException( "FormatterMultiplicity node does not contain a number ("+output.xpathEval( "./FormatterMultiplicity" )[0].content+")." )
        if optCnt>1:
            raise ConfigException( "At most one FormatterMultiplicity node can be specified for Output node." )

        optCnt1 = 0
        optCnt2 = 0
        self.fFormatterShmType = None
        self.fOutputShmType = None
        for ctxt in self.fXPathContexts:
            outputs = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Output" )
            for output in outputs:
                runTypes = output.xpathEval( "./@runtype" )
                if len(runTypes)>0 and (((len(runTypes[0].content)<=0 or runTypes[0].content[0]!="!") and self.fECSRunType!=runTypes[0].content) or (len(runTypes[0].content)>0 and runTypes[0].content[0]=="!" and self.fECSRunType==runTypes[0].content[1:])) and (self.fECSRunType!=None or runTypes[0].content!=""):
                    continue
                optCnt1 += len(output.xpathEval( "./FormatterShmType" ))
                if len(output.xpathEval( "./FormatterShmType" ))>0 and self.fFormatterShmType==None:
                    try:
                        self.fFormatterShmType = output.xpathEval( "./FormatterShmType" )[0].content
                    except ValueError:
                        raise ConfigException( "FormatterShmType invalid ("+output.xpathEval( "./FormatterShmType" )[0].content+")." )
                optCnt2 += len(output.xpathEval( "./OutputShmType" ))
                if len(output.xpathEval( "./OutputShmType" ))>0 and self.fOutputShmType==None:
                    try:
                        self.fOutputShmType = output.xpathEval( "./OutputShmType" )[0].content
                    except ValueError:
                        raise ConfigException( "OutputShmType invalid ("+output.xpathEval( "./OutputShmType" )[0].content+")." )
        if optCnt1>1 or optCnt2>1:
            raise ConfigException( "At most one ShmType node can be specified for Output node." )
        if self.fFormatterShmType == None:
            self.fFormatterShmType = "librorc"
        if self.fOutputShmType == None:
            self.fOutputShmType = "librorc"

        optCnt1 = 0
        optCnt2 = 0
        self.fFormatterShmType = None
        self.fOutputShmType = None
        for ctxt in self.fXPathContexts:
            outputs = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Output" )
            for output in outputs:
                runTypes = output.xpathEval( "./@runtype" )
                if len(runTypes)>0 and (((len(runTypes[0].content)<=0 or runTypes[0].content[0]!="!") and self.fECSRunType!=runTypes[0].content) or (len(runTypes[0].content)>0 and runTypes[0].content[0]=="!" and self.fECSRunType==runTypes[0].content[1:])) and (self.fECSRunType!=None or runTypes[0].content!=""):
                    continue
                optCnt1 += len(output.xpathEval( "./FormatterShmType" ))
                if len(output.xpathEval( "./FormatterShmType" ))>0 and self.fFormatterShmType==None:
                    try:
                        self.fFormatterShmType = output.xpathEval( "./FormatterShmType" )[0].content
                    except ValueError:
                        raise ConfigException( "FormatterShmType invalid ("+output.xpathEval( "./FormatterShmType" )[0].content+")." )
                optCnt2 += len(output.xpathEval( "./OutputShmType" ))
                if len(output.xpathEval( "./OutputShmType" ))>0 and self.fOutputShmType==None:
                    try:
                        self.fOutputShmType = output.xpathEval( "./OutputShmType" )[0].content
                    except ValueError:
                        raise ConfigException( "OutputShmType invalid ("+output.xpathEval( "./OutputShmType" )[0].content+")." )
        if optCnt1>1 or optCnt2>1:
            raise ConfigException( "At most one ShmType node can be specified for Output node." )
        if self.fFormatterShmType == None:
            self.fFormatterShmType = "librorc"
        if self.fOutputShmType == None:
            self.fOutputShmType = "librorc"

        for ctxt in self.fXPathContexts:
            outputs = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Output" )
            for output in outputs:
                runTypes = output.xpathEval( "./@runtype" )
                if len(runTypes)>0 and (((len(runTypes[0].content)<=0 or runTypes[0].content[0]!="!") and self.fECSRunType!=runTypes[0].content) or (len(runTypes[0].content)>0 and runTypes[0].content[0]=="!" and self.fECSRunType==runTypes[0].content[1:])) and (self.fECSRunType!=None or runTypes[0].content!=""):
                    continue
                if len(output.xpathEval( "./ShmSize" ))>0:
                    try:
                        shmSize = int(output.xpathEval( "./ShmSize" )[0].content)
                    except ValueError:
                        raise ConfigException( "Output ShmSize node does not contain a number ("+output.xpathEval( "./ShmSize" )[0].content+")." )
                    if self.fOutputShmSize==None:
                        self.fOutputShmSize = shmSize
                    else:
                        self.fOutputShmSize += shmSize

        
        for ctxt in self.fXPathContexts:
            outputs = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Output" )
            for output in outputs:
                runTypes = output.xpathEval( "./@runtype" )
                if len(runTypes)>0 and (((len(runTypes[0].content)<=0 or runTypes[0].content[0]!="!") and self.fECSRunType!=runTypes[0].content) or (len(runTypes[0].content)>0 and runTypes[0].content[0]=="!" and self.fECSRunType==runTypes[0].content[1:])) and (self.fECSRunType!=None or runTypes[0].content!=""):
                    continue
                parents = output.xpathEval( "./Parent" )
                for parent in parents:
                    self.fOutputParents.add( parent.content )
                    filterType = parent.prop("filtered")
                    if filterType!=None and filterType!="none":
                        self.fOutputParentFilters[parent.content] = filterType
                for p in output.xpathEval("./RequiredContributor"):
                    self.fRequiredOutputContributors.add( p.content )
                    if p.content.find("/")!=-1:
                        self.AddRequiredContributor( p.content )
                    else:
                        self.AddRequiredContributor( "ALICE/"+p.content )

                # check if Formatter Shm Sizes are given in XML
                if (len(output.xpathEval("./FormatterShmSize/@blockcount")) > 0) and \
                    (len(output.xpathEval("./FormatterShmSize/@blocksize")) > 0):
                    try:
                        self.fFormatterShmBlocksize = self.StringSize2Int( \
                            (output.xpathEval("./FormatterShmSize/@blocksize")[0].content))
                        self.fFormatterShmBlockcount = int(output.xpathEval( \
                            "./FormatterShmSize/@blockcount")[0].content)
                    except ValueError:
                        raise ConfigException( "Output FormatterShmSize node does not " \
                            "contain a number (" + \
                            output.xpathEval( "./FormatterShmSize" )[0].content + ")." )

        self.fFiles["ALICE/Output"]["parents"] = self.fOutputParents
        #self.fFiles["ALICE/Output"]["optparents"] |= set(tmpProc[proc]["optparents"])


        if len(self.fOutputParents)<=0 and self.fECSOutputList!=None and len(self.fECSOutputList)>0 and len(self.fOutputDDLs)>0:
            raise ConfigException( "At least one Parent node has to be specified for Output node." )


        if self.fOutputType!="file" and self.fOutputType!="ddl" and self.fOutputType!="both":
            raise ConfigException( "Wrong output type '%s'. Only 'file', 'ddl', or 'both' allowed" )
        if self.fOutputType!="ddl":
            self.fOutputDir = None
            self.fOutputPrefix = None
            for ctxt in self.fXPathContexts:
                outputs = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Output" )
                for output in outputs:
                    runTypes = output.xpathEval( "./@runtype" )
                    if len(runTypes)>0 and (((len(runTypes[0].content)<=0 or runTypes[0].content[0]!="!") and self.fECSRunType!=runTypes[0].content) or (len(runTypes[0].content)>0 and runTypes[0].content[0]=="!" and self.fECSRunType==runTypes[0].content[1:])) and (self.fECSRunType!=None or runTypes[0].content!=""):
                        continue
                    if len(output.xpathEval( "./Directory" ))>1:
                        raise ConfigException( "Exactly one Directory node must be specified for Output node." )
                    if len(output.xpathEval( "./Directory" ))==1:
                        if self.fOutputDir==None:
                            self.fOutputDir = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Output/Directory" )[0].content
                        else:
                            raise ConfigException( "Exactly one Directory node must be specified for Output node in all input files." )
                    if len(output.xpathEval( "./FilePrefix" ))>1:
                        raise ConfigException( "Exactly one FilePrefix node must be specified for Output node." )
                    if len(output.xpathEval( "./FilePrefix" ))==1:
                        if self.fOutputPrefix==None:
                            self.fOutputPrefix = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Output/FilePrefix" )[0].content
                        else:
                            raise ConfigException( "Exactly one FilePrefix node must be specified for Output node in all input files." )
                    if len(output.xpathEval( "./Multiplicity" ))>1:
                        raise ConfigException( "At most one Multiplicity node must be specified for Output node." )
            if self.fOutputDir==None:
                raise ConfigException( "One Directory node must be specified for Output node in one input file." )
            if self.fOutputPrefix==None:
                raise ConfigException( "One FilePrefix node must be specified for Output node in one input file." )
                
                
            for ctxt in self.fXPathContexts:
                outputs = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Output" )
                for output in outputs:
                    runTypes = output.xpathEval( "./@runtype" )
                    if len(runTypes)>0 and (((len(runTypes[0].content)<=0 or runTypes[0].content[0]!="!") and self.fECSRunType!=runTypes[0].content) or (len(runTypes[0].content)>0 and runTypes[0].content[0]=="!" and self.fECSRunType==runTypes[0].content[1:])) and (self.fECSRunType!=None or runTypes[0].content!=""):
                        continue
                    if len(output.xpathEval( "./Node" ))>0:
                        nodes = output.xpathEval( "./Node" )
                        for node in nodes:
                            if self.fResMan.GetNodeHostname(node.content)==None:
                                raise ConfigException( "Output node "+node.content+" is not contained in node list." )
                            self.fOutputNodes.append( node.content )

            if len(self.fOutputNodes)<=0:
                if not self.fQuiet:
                    print "Warning : No explicit output nodes specified (one or more Node tags in Output section needed)."
                if len(self.fOutputDDLs)>0:
                    for ddl in self.fOutputDDLs:
                        self.fOutputNodes.append( self.fResMan.GetDDLLocation( ddl )["node"] )
                    if self.fOutputType=="file":
                        self.fOutputNodes = list(set(self.fOutputNodes))
                    if not self.fQuiet:
                        print "Using nodes with output DDLs automatically"
                elif not self.fQuiet:
                    print "Warning : No file output will be available."

        if self.fOutputFormatterMultiplicity==-1 and len(self.fOutputDDLs)==0:
            if not self.fQuiet:
                print "Warning : No output formatter will be used. Please specify either an explicit FormatterMultiplicity node in the output section or provide an output DDL list to change this"
                
        if self.fOutputFormatterMultiplicity==-1 or self.fOutputType!="file":
            self.fOutputFormatterMultiplicity = len(self.fOutputDDLs)

        if self.fOutputFormatterMultiplicity<=0 and self.fOutputType!="file":
            raise ConfigException( "No output formatter will be used." )


        # Actual processes will be create in ReadALICEProcesses


    def ReadFXS( self ):
        fxscnt=0
        for ctxt in self.fXPathContexts:
            fxscnt += len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/FXS" ))
        if fxscnt<=0:
            return

        if not self.fFiles.has_key("ALICE/FXS"):
            self.fFiles["ALICE/FXS"] = { "files" : set(), "ddls" : set(), "parents" : set(), "optparents" : set() }


        self.fFXSBase = None
        self.fFXSDB = None
        self.fFXSDBUser = None
        self.fFXSDBPwd = None
        for ctxt in self.fXPathContexts:
            currentFile = self.GetXMLFilenameFromXMLContext( ctxt )
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/FXS" ))>0:
                self.fFiles["ALICE/FXS"]["files"].add( currentFile )
                self.fFiles["ALICE/FXS"]["ddls"] |= Detector.NameToDDLList("ALICE") & self.fDDLs
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/FXS/Base" ))>1:
                raise ConfigException( "Only one FXS/Base tag may be specified" )
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/FXS/Base" ))>0 and self.fFXSBase==None:
                self.fFXSBase = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/FXS/Base" )[0].content
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/FXS/DB" ))>1:
                raise ConfigException( "Only one FXS/DB tag may be specified" )
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/FXS/DB" ))>0 and self.fFXSDB==None:
                self.fFXSDB = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/FXS/DB" )[0].content
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/FXS/User" ))>1:
                raise ConfigException( "Only one FXS/User tag may be specified" )
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/FXS/User" ))>0 and self.fFXSDBUser==None:
                self.fFXSDBUser = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/FXS/User" )[0].content
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/FXS/Password" ))>1:
                raise ConfigException( "Only one FXS/Password tag may be specified" )
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/FXS/Password" ))>0 and self.fFXSDBPwd==None:
                self.fFXSDBPwd = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/FXS/Password" )[0].content
                
        if self.fFXSBase==None:
            raise ConfigException( "FXS/Base tag has to be specified in one input file if FXS is used" )
        if self.fFXSDB==None:
            raise ConfigException( "FXS/DB tag has to be specified in one input file if FXS is used" )
        if self.fFXSDBUser==None:
            raise ConfigException( "FXS/User tag has to be specified in one input file if FXS is used" )
        if self.fFXSDBPwd==None:
            raise ConfigException( "FXS/Password tag has to be specified in one input file if FXS is used" )

        
        self.fFXSNodes = self.fResMan.GetNodesWithAttributes( { "FXS" : [] } )
        for ctxt in self.fXPathContexts:
            for node in ctxt.xpathEval( "/SimpleChainConfig2/ALICE/FXS/Node" ):
                self.CheckForNode( node.content, "FXS" )
                # if self.fResMan.GetNodePlatform(node.content)==None:
                #     raise ConfigException( "Specified FXS node '"+node.content+"' cannot be found" )
                self.fFXSNodes.append( node.content )

        for ctxt in self.fXPathContexts:
            for node in ctxt.xpathEval( "/SimpleChainConfig2/ALICE/FXS/NodeExclude" ):
                while self.fFXSNodes.count(node.content)>0:
                    del self.fFXSNodes[self.fFXSNodes.index(node.content)]
        self.fFXSNodes = list(set(self.fFXSNodes))

        self.fFXSNodes = list(set(self.fFXSNodes))
                
        if len(self.fFXSNodes)<=0:
            raise ConfigException( "No FXS nodes available (none explicitly specified in FXS/Node tags and no nodes with FXS attribute available from ResourceManager or all nodes excluded by NodeExclude tags)" )


        for ctxt in self.fXPathContexts:
            for node in ctxt.xpathEval( "/SimpleChainConfig2/ALICE/FXS/Parent" ):
                self.fFXSParents.append( node.content )
                self.fFiles["ALICE/FXS"]["parents"].add( node.content )
                filterType = node.prop("filtered")
                if filterType!=None and filterType!="none":
                    if self.fFXSParentFilters.has_key(node.content) and self.fFXSParentFilters[node.content]!=filterType:
                        raise ConfigException( "FXS parent '"+node.content+"' given multiple times with different filter specifications" )
                    self.fFXSParentFilters[node.content] = filterType
            for p in ctxt.xpathEval("/SimpleChainConfig2/ALICE/FXS/RequiredContributor"):
                self.fRequiredFXSContributors.add( p.content )
                if p.content.find("/")!=-1:
                    self.AddRequiredContributor( p.content )
                else:
                    self.AddRequiredContributor( "ALICE/"+p.content )
                    
        self.fFXSParents = list(set(self.fFXSParents))


    def ReadTrigger( self, triggerType ):
        triggercnt=0
        for ctxt in self.fXPathContexts:
            triggercnt += len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Trigger" ))
        if triggercnt<=0:
            return


        for ctxt in self.fXPathContexts:
            for triggerNode in ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Trigger" ):
                currentFile = self.GetXMLFilenameFromXMLContext( ctxt )
                nodeTriggerType = triggerNode.prop("type")
                if nodeTriggerType==None:
                    nodeTriggerType = self.fDefaultTriggerType
                if nodeTriggerType!=triggerType:
                    continue
                if not self.fFiles.has_key("ALICE/Global"+self.fTriggerTypeComponentSpecifier[triggerType]+"Trigger"):
                    self.fFiles["ALICE/Global"+self.fTriggerTypeComponentSpecifier[triggerType]+"Trigger"] = { "files" : set(), "ddls" : set(), "parents" : set(), "optparents" : set() }
                self.fFiles["ALICE/Global"+self.fTriggerTypeComponentSpecifier[triggerType]+"Trigger"]["files"].add( currentFile )
                self.fFiles["ALICE/Global"+self.fTriggerTypeComponentSpecifier[triggerType]+"Trigger"]["ddls"] |= Detector.NameToDDLList("ALICE") & self.fDDLs
                if len(triggerNode.xpathEval("./Multiplicity"))>0:
                    try:
                        tmp = int(triggerNode.xpathEval("./Multiplicity")[0].content,0)
                    except ValueError:
                        raise ConfigException( triggerType+" trigger multiplicites cannot be converted to an integer" )
                    if self.fTriggerMultiplicities[triggerType]!=None and self.fTriggerMultiplicities[triggerType]!=tmp:
                        raise ConfigException( triggerType+" trigger multiplicites given multiple times with values" )
                    self.fTriggerMultiplicities[triggerType] = tmp
                for node in triggerNode.xpathEval("./Parent"):
                    self.fTriggerParents[triggerType].append( node.content )
                    filterType = node.prop("filtered")
                    if filterType!=None and filterType!="none":
                        if self.fTriggerParentFilters[triggerType].has_key(node.content) and self.fTriggerParentFilters[triggerType][node.content]!=filterType:
                            raise ConfigException( triggerType+" trigger parent '"+node.content+"' given multiple times with different filter specifications" )
                        self.fTriggerParentFilters[triggerType][node.content] = filterType
                for p in triggerNode.xpathEval("./RequiredContributor"):
                    self.fRequiredTriggerContributors[triggerType].add( p.content )
                    if p.content.find("/")!=-1:
                        self.AddRequiredContributor( p.content )
                    else:
                        self.AddRequiredContributor( "ALICE/"+p.content )
        if self.fFiles.has_key("ALICE/Global"+self.fTriggerTypeComponentSpecifier[triggerType]+"Trigger"):
            self.fFiles["ALICE/Global"+self.fTriggerTypeComponentSpecifier[triggerType]+"Trigger"]["parents"] = set(self.fTriggerParents[triggerType])
        #self.fFiles["ALICE/Global"+self.fTriggerTypeComponentSpecifier[triggerType]+"Trigger"]["optparents"] |= set(tmpProc[proc]["optparents"])

        mult = self.fTriggerMultiplicities[triggerType]
        nodes=[]
        for ctxt in self.fXPathContexts:
            for triggerNode in ctxt.xpathEval( "/SimpleChainConfig2/ALICE/Trigger" ):
                if len(triggerNode.xpathEval("./Node"))>0:
                    for n in triggerNode.xpathEval("./Node"):
                        if self.fResMan.GetNodeHostname(n.content)==None:
                            raise ConfigException( "Trigger node "+n.content+" is not contained in node list." )
                        nodes.append( n.content )
        if len(nodes)>0:
            if mult!=None and len(nodes)>mult:
                raise ConfigException( "Total number of nodes given for global %s trigger can be at most as large as given multiplicity" % triggerType )
            if mult==None:
                self.fTriggerMultiplicities[triggerType] = len(nodes)
            self.fTriggerNodes[triggerType] = nodes


                    
        self.fTriggerParents[triggerType] = list(set(self.fTriggerParents[triggerType]))
        self.fProcesses["ALICE"]["Global"+self.fTriggerTypeComponentSpecifier[triggerType]+"Trigger"] = 1

    def OutputGraphFile( self ):
        configName = self.fConfig.GetName()
        filename = configName+"-ProcessGraph.dot"
        try:
            
            graphOutFile = open( filename, "w" )
        except IOError,e:
            print "Error writing to process graph output file '"+filename+"': "+str(e)
            return
        graphOutFile.write( "DiGraph "+configName+" {\n" )
        for det in self.f3LevelDetectors:
            self.Output3LevelDetectorGraph( graphOutFile, det, Detector.Get3LevelDetectorLevel1ElementName(det) )
        self.OutputDimuonGraph( graphOutFile )
        for det in self.f2LevelDetectors:
            self.Output2LevelDetectorGraph( graphOutFile, det )

        self.OutputALICEGraph( graphOutFile )
        graphOutFile.write( "}\n" )
        graphOutFile.close()
        
    def OutputGraphProcesses( self, outputFile, detectorID, ddlPrefix, idPrefix, indent ):
        for proc in self.fFiles.keys():
            if proc[:len(idPrefix)]==idPrefix and proc.count("/")==idPrefix.count("/")+1:
                if proc.count("/")>0:
                    procID = proc.split("/")[-1]
                else:
                    procID = proc
                fileList = list(self.fFiles[proc]["files"])
                fileList.sort()
                fileStr = fileList[0]
                for f in fileList[1:]:
                    fileStr += ","+f
                ddlList = self.fFiles[proc]["ddls"] & self.fDDLs
                if detectorID=="DIMU" and ddlPrefix=="DIMU_TRG" and len(Detector.GetDimuonDDLList( "TRG" )-ddlList)<=0:
                    ddlListStr = ddlPrefix
                elif detectorID=="DIMU" and ddlPrefix=="DIMU_TRG" and len(Detector.GetDimuonDDLList( "TRG" )-ddlList)<(len(Detector.GetDimuonDDLList( "TRG" ))/2):
                    ddlListStr = ddlPrefix+"-["+self.MakeListString( Detector.GetDimuonDDLList( "TRG" )-ddlList )+"]"
                elif detectorID=="DIMU" and ddlPrefix=="DIMU_TRK" and len(Detector.GetDimuonDDLList( "TRK" )-ddlList)<=0:
                    ddlListStr = ddlPrefix
                elif detectorID=="DIMU" and ddlPrefix=="DIMU_TRK" and len(Detector.GetDimuonDDLList( "TRK" )-ddlList)<(len(Detector.GetDimuonDDLList( "TRK" ))/2):
                    ddlListStr = ddlPrefix+"-["+self.MakeListString( Detector.GetDimuonDDLList( "TRK" )-ddlList )+"]"
                elif len(Detector.NameToDDLList(detectorID)-ddlList)<=0:
                    ddlListStr = detectorID
                elif len(Detector.NameToDDLList(detectorID)-ddlList)<(len(Detector.NameToDDLList(detectorID))/2):
                    ddlListStr = detectorID+"-["+self.MakeListString( Detector.NameToDDLList(detectorID)-ddlList )+"]"
                elif detectorID=="ALICE":
                    ddlListTmp = ddlList
                    ddlListStr = ""
                    notFound=False
                    for det in Detector.IDs:
                        if len(Detector.NameToDDLList(det)-ddlListTmp)<=0:
                            ddlListStr += ","+det
                            ddlListTmp -= Detector.NameToDDLList(det)
                        else:
                            notFound=True
                    ddlListStr += ","+self.MakeListString( ddlListTmp )
                    if not notFound:
                        ddlListStr = "ALL"
                    if ddlListStr[0]==",":
                        ddlListStr[1:]
                else:
                    ddlListStr = self.MakeListString( ddlList )
                outputFile.write( indent+detectorID+"_"+procID+" [shape=record label=\"<ID>"+procID+" | DDLs='"+ddlListStr+"' | Files="+fileStr+"\"];\n" )
                for p in self.fFiles[proc]["parents"] | self.fFiles[proc]["optparents"]:
                    foundPar = False
                    if p in ( "DDL", "HWCoProc" ):
                        foundPar = True
                    if not foundPar:
                        for parProc in self.fFiles.keys():
                            parProcID = parProc.split("/")[0]+"/"+parProc.split("/")[-1]
                            if parProcID==p or (p.count("/")==0 and parProc.split("/")[0]==detectorID and parProc.split("/")[-1]==p):
                                foundPar = True
                                break
                    if not foundPar:
                        continue
                    if p.count("/")>0:
                        parIDs = ( "_".join( p.split("/") ), )
                    elif p in ( "DDL", "HWCoProc" ) and detectorID!=ddlPrefix:
                        parIDs = ( ddlPrefix+"_"+p, )
                    elif p in ( "DDL", "HWCoProc" ) and detectorID=="DIMU":
                        parIDs = ( "DIMU_TRG_DDL", "DIMU_TRK_DDL" )
                    else:
                        parIDs = ( detectorID+"_"+p, )

                    for parID in parIDs:
                        outputFile.write( indent+parID+":ID -> "+detectorID+"_"+procID+":ID;\n" )
        

#### Generic 3-level detector
    def Read3LevelDetector( self, detectorID, level1ElementName ):
        foundDet=False
        for ctxt in self.fXPathContexts:
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+detectorID ))>0:
                foundDet=True
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+detectorID ))>1:
                raise ConfigException( "Only one /SimpleChainConfig2/ALICE/"+detectorID+" node allowed in each XML file" )

        if not foundDet:
            return
        # First read in the detector option overrides for the various TPC parts
        self.Read3LevelDetectorOptions( detectorID, level1ElementName )
        
        # Create all the source processes
        for i in Detector.NameToDDLList( detectorID ) & self.fDDLs:
            self.MakeDDLSource( i )
        #print "Sources: ["
        #for p in self.fConfig.GetProcesses():
        #    print p,","
        #print "]"

        # Read in partition, slice, and TPC global processes
        partProcesses,level1Processes,detProcesses = self.Read3LevelDetectorProcesses( detectorID, level1ElementName )
        # print detectorID,"partition processes:",partProcesses
        # print detectorID,level1ElementName,"processes:",level1Processes
        # print detectorID,"processes:",detProcesses
        #print "TPC Partition processes:",partProcesses
        #print "TPC Slice processes:",sliceProcesses
        #print "TPC processes:",tpcProcesses

        # Iterate all processes, check parents, mark which process to use for which unit/part
        self.fProcessList[detectorID+"/"+level1ElementName+"/Partition"] = self.Check3LevelDetectorPartitionProcesses( detectorID, level1ElementName, partProcesses )
        #print "Sources+TPC Partition Processes: ["
        #for p in self.fConfig.GetProcesses():
        #    print p,","
        #print "]"
        
        self.fProcessList[detectorID+"/"+level1ElementName] = self.Check3LevelDetectorLevel1Processes( detectorID, level1ElementName, level1Processes, self.fProcessList[detectorID+"/"+level1ElementName+"/Partition"] )

        self.Check3LevelDetectorProcesses( detectorID, level1ElementName, detProcesses, level1Processes, partProcesses )
        self.fProcessList[detectorID] = detProcesses

           

    def Read3LevelDetectorOptions( self, detectorID, level1ElementName ):
        for ctxt in self.fXPathContexts:
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+detectorID ))<=0:
                continue
            self.ApplyDetectorOptions( ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+detectorID )[0], Detector.Get3LevelDetectorDDLList( detectorID ), False )
            
        # print "self.fRORCs (Apply TPC Options):",self.fRORCs
        for ctxt in self.fXPathContexts:
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+detectorID ))<=0:
                continue
            level1Elements = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+detectorID+"/"+level1ElementName )
            # print "slices:",slices
            for level1Node in level1Elements:
                if len(level1Node.xpathEval( "@ID" ))>0:
                    level1IDs = self.ParseList( level1Node.xpathEval( "@ID" )[0].content, numbers=True, StringLookup=lambda x: set([int(x)]) )
                else:
                    level1IDs=[None]
                for level1ID in level1IDs:
                    self.ApplyDetectorOptions( level1Node, Detector.Get3LevelDetectorDDLList(detectorID, level1Element=level1ID), level1ID!=None )
                    if level1ID==None:
                        level1Print=-1
                    else:
                        level1Print=level1ID
                # print "self.fRORCs (Apply TPC Slice %d Options):"%slicePrint,self.fRORCs
        for ctxt in self.fXPathContexts:
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+detectorID ))<=0:
                continue
            level1Elements = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+detectorID+"/"+level1ElementName )
            # print "level1Element:",level1Elements
            for level1Node in level1Elements:
                if len(level1Node.xpathEval( "@ID" ))>0:
                    level1IDs = self.ParseList( level1Node.xpathEval( "@ID" )[0].content, numbers=True, StringLookup=lambda x: set([int(x)]) )
                else:
                    level1IDs=[None]
                partitions = level1Node.xpathEval( "Partition" )
                for level1ID in level1IDs:
                    if level1ID==None:
                        level1Print=-1
                    else:
                        level1Print=level1ID
                    for partitionNode in partitions:
                        if len(partitionNode.xpathEval( "@ID" ))>0:
                            # partIDs = self.ParseList( partitionNode.xpathEval( "@ID" )[0].content, numbers=True, StringLookup=lambda x: set([Detector.GetDetectorPartitionDDL( detectorID, int(x))]), applyStringLookupToRangeElements=True )
                            partIDs = self.ParseList( partitionNode.xpathEval( "@ID" )[0].content, numbers=True, StringLookup=lambda x: set(Detector.Get3LevelDetectorDDLList(detectorID, level1Element=level1ID, partition=int(x))), applyStringLookupToRangeElements=True )
                        else:
                            partIDs=[None]
                        for partID in partIDs:
                            self.ApplyDetectorOptions( partitionNode, Detector.Get3LevelDetectorDDLList(detectorID, level1Element=level1ID, partition=partID), partID!=None )
                            if partID==None:
                                partPrint=-1
                            else:
                                partPrint=partID
                            # print "self.fRORCs (Apply TPC Slice %d Partition %d Options):"%(slicePrint,partPrint),self.fRORCs

    def Read3LevelDetectorProcesses(self, detectorID, level1ElementName):
        # Read in partition, level 1 (e,.g TPC slice), and detector global processes
        partProcesses = {}
        level1Processes = {}
        detProcesses = {}
        fileCompID = []
        for ctxt in self.fXPathContexts:
            currentFile = self.GetXMLFilenameFromXMLContext( ctxt )
            fileCompID.append( detectorID )
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+detectorID ))<=0:
                continue
            level1Elements = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+detectorID+"/"+level1ElementName )
            # print detectorID,"level1Elements:",level1Elements
            for level1Node in level1Elements:
                if len(level1Node.xpathEval( "@ID" ))>0:
                    level1IDs = self.ParseList( level1Node.xpathEval( "@ID" )[0].content, numbers=True, StringLookup=lambda x: set([int(x)]) )
                else:
                    level1IDs=[None]
                fileCompID.append ( fileCompID[-1]+"/"+level1ElementName )
                partitions = level1Node.xpathEval( "./Partition" )
                for level1ID in level1IDs:
                    if level1ID==None:
                        level1Print = -1
                    else:
                        level1Print = level1ID
                    if not partProcesses.has_key(level1Print):
                        partProcesses[level1Print] = {}
                    for partNode in partitions:
                        if len(partNode.xpathEval( "@ID" ))>0:
                            partIDs = self.ParseList( partNode.xpathEval( "@ID" )[0].content, numbers=True, StringLookup=lambda x: Detector.Get3LevelDetectorDDLList( detectorID, level1ID, int(x)), applyStringLookupToRangeElements=True )
                        else:
                            partIDs=[None]
                        fileCompID.append ( fileCompID[-1]+"/Partition" )
                        for partID in partIDs:
                            if partID==None:
                                partPrint = -1
                            else:
                                if not Detector.Is3LevelDetectorRelativePartitionID( detectorID, partID) and not (partID in Detector.Get3LevelDetectorDDLList(detectorID)):
                                    raise ConfigException( detectorID+" Partition ID "+str(partID)+" not known" )
                                #partPrint = Detector.GetTPCPartition(partID)
                                partPrint = partID
                            ddlList = Detector.Get3LevelDetectorDDLList( detectorID, level1ID, partID )

                            tmpProc = self.ReadProcesses( partNode, detectorID+" "+level1ElementName+" %d Partition %d" % (level1Print, partPrint), ddlList, set([detectorID]), detector=detectorID )
                            if not partProcesses[level1Print].has_key(partPrint):
                                partProcesses[level1Print][partPrint] = {}
                                

                            for proc in tmpProc.keys():
                                fileCompID.append ( fileCompID[-1]+"/"+proc )
                                if partProcesses[level1Print][partPrint].has_key(proc):
                                    raise ConfigException( detectorID+" partition process with ID '"+proc+"' for "+level1ElementName+" "+str(level1Print)+" - partition "+str(partPrint)+" is defined in multiple input files" )
                                if not self.fFiles.has_key(fileCompID[-1]):
                                    self.fFiles[fileCompID[-1]] = { "files" : set(), "ddls" : set(), "parents" : set(), "optparents" : set() }
                                self.fFiles[fileCompID[-1]]["files"].add( currentFile )
                                self.fFiles[fileCompID[-1]]["ddls"] |= ddlList
                                self.fFiles[fileCompID[-1]]["parents"] |= set(tmpProc[proc]["parents"])
                                self.fFiles[fileCompID[-1]]["optparents"] |= set(tmpProc[proc]["optparents"])
                                partProcesses[level1Print][partPrint][proc] = tmpProc[proc]
                                fileCompID = fileCompID[:-1]
                        fileCompID = fileCompID[:-1]
                    ddlList = Detector.Get3LevelDetectorDDLList( detectorID, level1ID )
                    tmpProc = self.ReadProcesses( level1Node, detectorID+" "+level1ElementName+" %d" % (level1Print), ddlList, set([detectorID]), detector=detectorID )
                    if not level1Processes.has_key(level1Print):
                        level1Processes[level1Print] = {}
                    for proc in tmpProc.keys():
                        fileCompID.append ( fileCompID[-1]+"/"+proc )
                        if level1Processes[level1Print].has_key(proc):
                            raise ConfigException( detectorID+" "+level1ElementName+" process with ID '"+proc+"' is defined in multiple input files" )
                        if not self.fFiles.has_key(fileCompID[-1]):
                            self.fFiles[fileCompID[-1]] = { "files" : set(), "ddls" : set(), "parents" : set(), "optparents" : set() }
                        self.fFiles[fileCompID[-1]]["files"].add( currentFile )
                        self.fFiles[fileCompID[-1]]["ddls"] |= ddlList 
                        self.fFiles[fileCompID[-1]]["parents"] |= set(tmpProc[proc]["parents"])
                        self.fFiles[fileCompID[-1]]["optparents"] |= set(tmpProc[proc]["optparents"])
                        level1Processes[level1Print][proc] = tmpProc[proc]
                        fileCompID = fileCompID[:-1]
                fileCompID = fileCompID[:-1]
            ddlList = Detector.Get3LevelDetectorDDLList(detectorID)
            tmpProc = self.ReadProcesses( ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+detectorID )[0], detectorID, ddlList, set([detectorID]), detector=detectorID )
            for proc in tmpProc.keys():
                fileCompID.append ( fileCompID[-1]+"/"+proc )
                if detProcesses.has_key(proc):
                    raise ConfigException( detectorID+" process with ID '"+proc+"' is defined in multiple input files" )
                if not self.fFiles.has_key(fileCompID[-1]):
                    self.fFiles[fileCompID[-1]] = { "files" : set(), "ddls" : set(), "parents" : set(), "optparents" : set() }
                self.fFiles[fileCompID[-1]]["files"].add( currentFile )
                self.fFiles[fileCompID[-1]]["ddls"] |= ddlList 
                self.fFiles[fileCompID[-1]]["parents"] |= set(tmpProc[proc]["parents"])
                self.fFiles[fileCompID[-1]]["optparents"] |= set(tmpProc[proc]["optparents"])
                detProcesses[proc] = tmpProc[proc]
                fileCompID = fileCompID[:-1]
            fileCompID = fileCompID[:-1]
        return ( partProcesses, level1Processes, detProcesses )

    def Check3LevelDetectorPartitionProcesses( self, detectorID, level1ElementName, partProcesses ):
        partProcessList = {}
        self.fProcesses[detectorID+"/"+level1ElementName+"/Partition"] = {}
        level1IDList = partProcesses.keys()
        level1IDList.sort()
        for level1ID in level1IDList:
            partIDList = partProcesses[level1ID].keys()
            partIDList.sort()
            for partID in partIDList:
                for procID in partProcesses[level1ID][partID].keys():
                    if not partProcessList.has_key(procID):
                        partProcessList[procID] = {}
                    #print sliceID,partID,procID,partProcesses[sliceID][partID][procID]
                    ddls = Detector.Get3LevelDetectorDDLList( detectorID, level1ID, partID ) & self.fDDLs
                    for par in partProcesses[level1ID][partID][procID]["parents"]:
                        if par=="DDL":
                            for ddl in ddls:
                                if not self.fRORCs["GENERIC.enable"] and not (self.fRORCs.has_key("%d.enable" % ddl) and self.fRORCs["%d.enable" % ddl]):
                                    # raise ConfigException( ( "DDL parent given for "+detectorID+" partition process '%s' cannot be found (DDL %d not activated)" ) % (procID, ddl) )
                                    print ( "Warning: DDL parent given for "+detectorID+" partition process '%s' cannot be found (DDL %d not activated)" ) % (procID, ddl)
                            continue
                        if par=="HWCoProc":
                            for ddl in ddls:
                                if not self.fRORCs["GENERIC.hwcoproc"] and not (self.fRORCs.has_key("%d.hwcoproc" % ddl) and self.fRORCs["%d.hwcoproc" % ddl]):
                                    # raise ConfigException( ( "HWCoProc parent given for "+detectorID+" partition process '%s' cannot be found (HWCoProc not activated for DDL %d)" ) % (procID, ddl) )
                                    print ( "Warning: HWCoProc parent given for "+detectorID+" partition process '%s' cannot be found (HWCoProc not activated for DDL %d)" ) % (procID, ddl)
                            continue
                        found=False
                        for level1ID2 in partProcesses.keys():
                            for partID2 in partProcesses[level1ID2].keys():
                                if partProcesses[level1ID2][partID2].has_key( par ):
                                    found=True
                                    break
                        if not found and not self.fRetrievalMode:
                            raise ConfigException( ( "Parent ID '%s' given for "+detectorID+" partition process '%s' is not defined (has to be defined as a partition process)" ) % (par,procID) )
                    
                    if self.fProcesses[detectorID+"/"+level1ElementName+"/Partition"].has_key(procID):
                        self.fProcesses[detectorID+"/"+level1ElementName+"/Partition"][procID] |= ddls
                    else:
                        self.fProcesses[detectorID+"/"+level1ElementName+"/Partition"][procID] = ddls
                    for ddl in ddls:
                        partProcessList[procID][ddl] = partProcesses[level1ID][partID][procID]
        return partProcessList

    def Check3LevelDetectorLevel1Processes( self, detectorID, level1ElementName, level1Processes, partProcessList ):
        level1ProcessList = {}
        self.fProcesses[detectorID+"/"+level1ElementName] = {}
        for level1ID in level1Processes.keys():
            for procID in level1Processes[level1ID].keys():
                if not level1ProcessList.has_key(procID):
                    level1ProcessList[procID] = {}
                if self.fProcesses[detectorID+"/"+level1ElementName+"/Partition"].has_key(procID):
                    raise ConfigException( ("ID '%s' given both as "+detectorID+" partition as well as "+detectorID+" "+level1ElementName+" process") % procID )
                ddls = Detector.Get3LevelDetectorDDLList( detectorID, level1ID ) & self.fDDLs
                for par in level1Processes[level1ID][procID]["parents"]:
                    if par=="DDL":
                        for ddl in ddls:
                            if not self.fRORCs["GENERIC.enable"] and not (self.fRORCs.has_key("%d.enable" % ddl) and self.fRORCs["%d.enable" % ddl]):
                                # raise ConfigException( ("DDL parent given for "+detectorID+" "+level1ElementName+" process '%s' cannot be found (DDL %d not activated)") % (procID, ddl) )
                                print ("Warning: DDL parent given for "+detectorID+" "+level1ElementName+" process '%s' cannot be found (DDL %d not activated)") % (procID, ddl)
                        continue
                    if par=="HWCoProc":
                        for ddl in ddls:
                            if not self.fRORCs["GENERIC.hwcoproc"] and not (self.fRORCs.has_key("%d.hwcoproc" % ddl) and self.fRORCs["%d.hwcoproc" % ddl]):
                                # raise ConfigException( ("HWCoProc parent given for "+detectorID+" "+level1ElementName+" process '%s' cannot be found (HWCoProc not activated for DDL %d)") % (procID, ddl) )
                                print ("Warning: HWCoProc parent given for "+detectorID+" "+level1ElementName+" process '%s' cannot be found (HWCoProc not activated for DDL %d)") % (procID, ddl)
                        continue
                    found=partProcessList.has_key(par)
                    if not found:
                        for ddlID2 in level1Processes.keys():
                            if level1Processes[ddlID2].has_key( par ):
                                found=True
                                break
                    if not found and not self.fRetrievalMode:
                        raise ConfigException( ("Parent ID '%s' given for "+detectorID+" "+level1ElementName+" process '%s' is not defined (has to be defined as a partition or "+level1ElementName+" process)") % (par,procID) )
                if level1ID==-1:
                    level1List=set(range(0, Detector.Get3LevelDetectorLevel1ElementCount(detectorID))) # level1List=set(range(0,36))
                else:
                    level1List = set([level1ID])
                if self.fProcesses[detectorID+"/"+level1ElementName].has_key(procID):
                    self.fProcesses[detectorID+"/"+level1ElementName][procID] |= level1List
                else:
                    self.fProcesses[detectorID+"/"+level1ElementName][procID] = level1List
                for level1Element in level1List:
                    level1ProcessList[procID][level1Element] = level1Processes[level1ID][procID]
        return level1ProcessList

    def Check3LevelDetectorProcesses( self, detectorID, level1ElementName, detProcesses, level1Processes, partProcesses ):
        self.fProcesses[detectorID] = {}
        for procID in detProcesses.keys():
            if self.fProcesses[detectorID+"/"+level1ElementName+"/Partition"].has_key(procID):
                raise ConfigException( ("ID '%s' given both as "+detectorID+" partition as well as whole "+detectorID+" process") % procID )
            if self.fProcesses[detectorID+"/"+level1ElementName].has_key(procID):
                raise ConfigException( ("ID '%s' given both as "+detectorID+" "+level1ElementName+" as well as whole "+detectorID+" process") % procID )
            ddls = Detector.Get3LevelDetectorDDLList(detectorID) & self.fDDLs
            for par in detProcesses[procID]["parents"]:
                if par=="DDL":
                    for ddl in ddls:
                        if not self.fRORCs["GENERIC.enable"] and not (self.fRORCs.has_key("%d.enable" % ddl) and self.fRORCs["%d.enable" % ddl]):
                            # raise ConfigException( ("DDL parent given for "+detectorID+" process '%s' cannot be found (DDL %d not activated)") % (procID, ddl) )
                            print ("Warning: DDL parent given for "+detectorID+" process '%s' cannot be found (DDL %d not activated)") % (procID, ddl)
                    continue
                if par=="HWCoProc":
                    for ddl in ddls:
                        if not self.fRORCs["GENERIC.hwcoproc"] and not (self.fRORCs.has_key("%d.hwcoproc" % ddl) and self.fRORCs["%d.hwcoproc" % ddl]):
                            # raise ConfigException( ("HWCoProc parent given for "+detectorID+" process '%s' cannot be found (HWCoProc not activated for DDL %d)") % (procID, ddl) )
                            print ("Warning: HWCoProc parent given for "+detectorID+" process '%s' cannot be found (HWCoProc not activated for DDL %d)") % (procID, ddl)
                    continue
                found=False
                for level1ID2 in partProcesses.keys():
                    for partID2 in partProcesses[level1ID2].keys():
                        if partProcesses[level1ID2][partID2].has_key( par ):
                            found=True
                            break
                if not found:
                    for ddlID2 in level1Processes.keys():
                        if level1Processes[ddlID2].has_key( par ):
                            found=True
                            break
                if not found and not detProcesses.has_key( par ) and not self.fRetrievalMode:
                    raise ConfigException( ("Parent ID '%s' given for "+detectorID+" process '%s' is not defined (has to be defined as a partition, "+level1ElementName+", or "+detectorID+" process)") % (par,procID) )
            self.fProcesses[detectorID][procID] = 1

    def Make3LevelDetectorProcesses( self, detectorID, level1ElementName ):
        # This order is necessary, since specific settings can override generic ones
        # Only after iterating over all slices and partitions is this determined
        # Also, we need to define all possible parents before starting to create processes
        
        if self.fProcessList.has_key(detectorID+"/"+level1ElementName+"/Partition"):
            partProcessList = self.fProcessList[detectorID+"/"+level1ElementName+"/Partition"]
            for proc in partProcessList.keys():
                for ddl in partProcessList[proc].keys():
                    self.MakeProcess( partProcessList[proc][ddl], [detectorID+"/"+level1ElementName+"/Partition"], ddlID=ddl,detID=detectorID )
        # print "Make3LevelDetectorProcesses",detectorID,level1ElementName
        #print "Sources+TPC Partition Processes: ["
        #for p in self.fConfig.GetProcesses():
        #    print p,","
        #print "]"
        if self.fProcessList.has_key(detectorID+"/"+level1ElementName):
            level1ProcessList = self.fProcessList[detectorID+"/"+level1ElementName]
            # print "1:",sliceProcessList.keys()
            for proc in level1ProcessList.keys():
                # print "2:",proc,level1ProcessList[proc].keys()
                for level1Element in level1ProcessList[proc].keys():
                    # print "3:",proc,slice
                    self.MakeProcess( level1ProcessList[proc][level1Element], [detectorID+"/"+level1ElementName+"/Partition",detectorID+"/"+level1ElementName],detID=detectorID, level1Element=level1Element )

        #print "Sources+TPC Partition+Slice Processes: ["
        #for p in self.fConfig.GetProcesses():
        #    print p,","
        #print "]"

        if self.fProcessList.has_key(detectorID):
            detProcesses = self.fProcessList[detectorID]
            for proc in detProcesses.keys():
                self.MakeProcess( detProcesses[proc], [detectorID+"/"+level1ElementName+"/Partition",detectorID+"/"+level1ElementName, detectorID], detID=detectorID )

        #print "Sources+All TPC Processes: ["
        #for p in self.fConfig.GetProcesses():
        #    print p,","
        #print "]"

    def GetMissing3LevelDetectorParentComponents( self, detectorID, level1ElementName ):
        missing = set()
        if self.fProcessList.has_key(detectorID+"/"+level1ElementName+"/Partition"):
            partProcessList = self.fProcessList[detectorID+"/"+level1ElementName+"/Partition"]
            # print detectorID,partProcessList.keys()
            for proc in partProcessList.keys():
                for ddl in partProcessList[proc].keys():
                    missing |= self.GetMissingParentsForComponent( partProcessList[proc][ddl], [detectorID+"/"+level1ElementName+"/Partition"], ddlID=ddl,detID=detectorID )
        if self.fProcessList.has_key(detectorID+"/"+level1ElementName):
            level1ProcessList = self.fProcessList[detectorID+"/"+level1ElementName]
            # print "1:",level1ProcessList.keys()
            for proc in level1ProcessList.keys():
                # print "2:",proc,level1ProcessList[proc].keys()
                for level1Element in level1ProcessList[proc].keys():
                    # print "3:",proc,level1Element
                    missing |= self.GetMissingParentsForComponent( level1ProcessList[proc][level1Element], [detectorID+"/"+level1ElementName+"/Partition",detectorID+"/"+level1ElementName], level1Element=level1Element,detID=detectorID )
                    # print "4:",proc,level1Element,self.GetMissingParentsForComponent( level1ProcessList[proc][level1Element], [detectorID+"/"+level1ElementName+"/Partition",detectorID+"/"+level1ElementName], level1Element=level1Element,detID=detectorID )
                    # if len(self.GetMissingParentsForComponent( level1ProcessList[proc][level1Element], [detectorID+"/"+level1ElementName+"/Partition",detectorID+"/"+level1ElementName], level1Element=level1Element,detID=detectorID ))!=0:
                    #     print "4a:",proc,level1Element,self.GetMissingParentsForComponent( level1ProcessList[proc][level1Element], [detectorID+"/"+level1ElementName+"/Partition",detectorID+"/"+level1ElementName], level1Element=level1Element,detID=detectorID ), level1ProcessList[proc][level1Element], [detectorID+"/"+level1ElementName+"/Partition",detectorID+"/"+level1ElementName], level1Element,detectorID
        if self.fProcessList.has_key(detectorID):
            detProcesses = self.fProcessList[detectorID]
            for proc in detProcesses.keys():
                missing |= self.GetMissingParentsForComponent( detProcesses[proc], [detectorID+"/"+level1ElementName+"/Partition",detectorID+"/"+level1ElementName, detectorID], detID=detectorID )
        return missing

    def Output3LevelDetectorGraph( self, outputFile, detectorID, level1ElementName ):
        foundDetector=False
        for proc in self.fFiles.keys():
            if proc[:len(detectorID)]==detectorID:
                foundDetector=True
        if not foundDetector:
            return
        outputFile.write( "\n" )
        outputFile.write( "  subgraph cluster_"+detectorID+" {\n" )
        outputFile.write( "\n" )
        outputFile.write( "    subgraph cluster_"+detectorID+"_"+level1ElementName+" {\n" )
        outputFile.write( "      subgraph cluster_"+detectorID+"_"+level1ElementName+"_Partition {\n" )
        idPrefix = detectorID+"/"+level1ElementName+"/Partition"
        outputFile.write( "        node [ style=filled color=black fillcolor=white]\n" )
        ddlList = self.fDDLs
        if len(Detector.Get3LevelDetectorDDLList(detectorID)-ddlList)<=0:
            ddlListStr = detectorID
        elif len(Detector.Get3LevelDetectorDDLList(detectorID)-ddlList)<(len(Detector.Get3LevelDetectorDDLList(detectorID))/2):
            ddlListStr = detectorID+"-["+self.MakeListString( Detector.Get3LevelDetectorDDLList(detectorID)-ddlList )+"]"
        else:
            ddlListStr = self.MakeListString( ddlList & Detector.Get3LevelDetectorDDLList(detectorID) )
        outputFile.write( "        "+detectorID+"_DDL [shape=record label=\"<ID>DDL | DDLs='"+ddlListStr+"'\"];\n" )
        self.OutputGraphProcesses( outputFile, detectorID, detectorID, idPrefix, "        " )
        #outputFile.write( "        TPC_CF [shape=record label="<ID>CF | IDs='768-983' | File=Components-TPC-CF.xml"];\n" )
        #outputFile.write( "        TPC_DDL:ID -> TPC_CF:ID;\n" )
        outputFile.write( "        label = \"Partition\";\n" )
        outputFile.write( "	   color=black;\n" )
        outputFile.write( "        fillcolor=white;\n" )
        outputFile.write( "        style=filled;\n" )
        outputFile.write( "      }\n" )
        idPrefix = detectorID+"/"+level1ElementName
        outputFile.write( "      node [ style=filled color=black fillcolor=white]\n" )
        self.OutputGraphProcesses( outputFile, detectorID, detectorID, idPrefix, "      " )
        #outputFile.write( "      TPC_CF:ID -> TPC_TR:ID;\n" )
        #outputFile.write( "      TPC_TR [shape=record label="<ID>TR | Slices='0-36' | File=Components-TPC-TR.xml"];\n" )
        outputFile.write( "      label = \""+level1ElementName+"\";\n" )
        outputFile.write( "      color=black;\n" )
        outputFile.write( "      fillcolor=lightgrey;\n" )
        outputFile.write( "      style=filled;\n" )
        outputFile.write( "    }\n" )
        idPrefix = detectorID
        outputFile.write( "    node [ style=filled color=black fillcolor=white]\n" )
        self.OutputGraphProcesses( outputFile, detectorID, detectorID, idPrefix, "    " )
        #outputFile.write( "    TPC_GM [shape=record label="<ID>GM | File=Components-TPC-GM.xml"];\n" )
        #outputFile.write( "    TPC_TR:ID -> TPC_GM:ID;\n" )
        outputFile.write( "    label = \""+detectorID+"\";\n" )
        outputFile.write( "    color=black;\n" )
        outputFile.write( "    fillcolor=grey;\n" )
        outputFile.write( "    style=filled;\n" )
        outputFile.write( "  }\n" )
        outputFile.write( "\n" )

    def ReadDimuon( self ):
        foundDIMU=False
        for ctxt in self.fXPathContexts:
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/DIMU" ))>0:
                foundDIMU=True
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/DIMU" ))>1:
                raise ConfigException( "Only one /SimpleChainConfig2/ALICE/DIMU node allowed in each XML file" )

        if not foundDIMU:
            return
        # First read in the detector option overrides for the various Dimuon parts
        self.ReadDimuonDetectorOptions()

        # Create all the source processes
        for i in Detector.NameToDDLList( "MUTG" ) & self.fDDLs:
            self.MakeDDLSource( i )
        for i in Detector.NameToDDLList( "MUTK" ) & self.fDDLs:
            self.MakeDDLSource( i )

        partProcesses = {}
        subdetProcesses = {}
        partProcesses["TRG"],partProcesses["TRK"],subdetProcesses["TRG"],subdetProcesses["TRK"],dimuProcesses = self.ReadDimuonProcesses()

        # Iterate all processes, check parents, mark which process to use for which unit/part
        for subDet in ["TRG", "TRK"]:
            self.fProcessList["DIMU/"+subDet+"/Partition"] = self.CheckDimuonPartitionProcesses( subDet, partProcesses[subDet] )
            self.fProcessList["DIMU/"+subDet] = self.CheckDimuonSubdetectorProcesses( subDet, subdetProcesses[subDet], self.fProcessList["DIMU/"+subDet+"/Partition"] )
        self.CheckDimuonProcesses( dimuProcesses, subdetProcesses, partProcesses )
        self.fProcessList["DIMU"] = dimuProcesses
        

            
        

    def ReadDimuonDetectorOptions(self):
        for ctxt in self.fXPathContexts:
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/DIMU" ))<=0:
                continue
            self.ApplyDetectorOptions( ctxt.xpathEval( "/SimpleChainConfig2/ALICE/DIMU" )[0], Detector.GetDimuonDDLList(), False )
        #print "self.fRORCs (Apply TPC Options):",self.fRORCs
        for ctxt in self.fXPathContexts:
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/DIMU" ))<=0:
                continue
            for subDet in [ "TRG", "TRK" ]:
                nodes = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/DIMU/"+subDet )
                if len(nodes)<=0:
                    continue
                self.ApplyDetectorOptions( nodes[0], Detector.GetDimuonDDLList(subDet=subDet), False )
        for ctxt in self.fXPathContexts:
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/DIMU" ))<=0:
                continue
            for subDet in [ "TRG", "TRK" ]:
                nodes = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/DIMU/"+subDet )
                if len(nodes)<=0:
                    continue
                partitions = nodes[0].xpathEval( "Partition" )
                for partitionNode in partitions:
                    if len(partitionNode.xpathEval( "@ID" ))>0:
                        partIDs = self.ParseList( partitionNode.xpathEval( "@ID" )[0].content, numbers=True, StringLookup=lambda x: set([Detector.GetDetectorPartitionDDL( "MU"+subDet[0]+subDet[2], int(x))]), applyStringLookupToRangeElements=True )
                    else:
                        partIDs = [-1]
                    for partID in partIDs:
                        self.ApplyDetectorOptions( partitionNode, Detector.GetDimuonDDLList(subDet=subDet, partition=partID), partID!=None )

    def ReadDimuonProcesses(self):
        partProcesses = { "TRK" : {},"TRG" : {} }
        subdetProcesses = { "TRK" : {},"TRG" : {} }
        dimuProcesses = {}
        fileCompID = []
        for ctxt in self.fXPathContexts:
            currentFile = self.GetXMLFilenameFromXMLContext( ctxt )
            fileCompID.append( "DIMU" )
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/DIMU" ))<=0:
                continue
            for subDet in [ "TRG", "TRK" ]:
                fileCompID.append ( fileCompID[-1]+"/"+subDet )
                nodes = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/DIMU/"+subDet )
                if len(nodes)<=0:
                    continue
                partitions = nodes[0].xpathEval( "./Partition" )
                if not partProcesses.has_key(subDet):
                    partProcesses[subDet] = {}
                for partNode in partitions:
                    fileCompID.append ( fileCompID[-1]+"/Partition" )
                    if len(partNode.xpathEval( "@ID" ))>0:
                        partIDs = self.ParseList( partNode.xpathEval( "@ID" )[0].content, numbers=True, StringLookup=lambda x: set([Detector.GetDetectorPartitionDDL( "MU"+subDet[0]+subDet[2], int(x))]), applyStringLookupToRangeElements=True )
                    else:
                        partIDs = [-1]
                    for partID in partIDs:
                        if partID!=-1 and not (partID in Detector.GetDimuonDDLList()):
                            raise ConfigException( "Dimuon Partition ID "+str(partID)+" not known" )

                        ddlList = Detector.GetDimuonDDLList( subDet, partID )
                        tmpProc = self.ReadProcesses( partNode, "Dimuon %s %d" % (subDet, partID), ddlList, set(["MUON"+subDet]), detector="MUON"+subDet )
                        if not partProcesses[subDet].has_key(partID):
                            partProcesses[subDet][partID] = {}
                        for proc in tmpProc.keys():
                            fileCompID.append ( fileCompID[-1]+"/"+proc )
                            if partProcesses[subDet][partID].has_key(proc ):
                                raise ConfigException( "Dimuon "+subDet+" partition process with ID '"+proc+"' is defined in multiple input files" )
                            if not self.fFiles.has_key(fileCompID[-1]):
                                self.fFiles[fileCompID[-1]] = { "files" : set(), "ddls" : set(), "parents" : set(), "optparents" : set() }
                            self.fFiles[fileCompID[-1]]["files"].add( currentFile )
                            self.fFiles[fileCompID[-1]]["ddls"] |= ddlList
                            self.fFiles[fileCompID[-1]]["parents"] |= set(tmpProc[proc]["parents"])
                            self.fFiles[fileCompID[-1]]["optparents"] |= set(tmpProc[proc]["optparents"])
                            partProcesses[subDet][partID][proc] = tmpProc[proc]
                            fileCompID = fileCompID[:-1]
                    fileCompID = fileCompID[:-1]
                ddlList = Detector.GetDimuonDDLList( subDet )
                tmpProc = self.ReadProcesses( nodes[0], "Dimuon %s" % (subDet), ddlList, set(["MUON"+subDet]), detector="MUON"+subDet )
                if not subdetProcesses.has_key(subDet):
                    subdetProcesses[subDet] = {}
                for proc in tmpProc.keys():
                    fileCompID.append ( fileCompID[-1]+"/"+proc )
                    if subdetProcesses[subDet].has_key(proc):
                        raise ConfigException( "Dimuon "+subDet+" process with ID '"+proc+"' is defined in multiple input files" )
                    if not self.fFiles.has_key(fileCompID[-1]):
                        self.fFiles[fileCompID[-1]] = { "files" : set(), "ddls" : set(), "parents" : set(), "optparents" : set() }
                    self.fFiles[fileCompID[-1]]["files"].add( currentFile )
                    self.fFiles[fileCompID[-1]]["ddls"] |= ddlList
                    self.fFiles[fileCompID[-1]]["parents"] |= set(tmpProc[proc]["parents"])
                    self.fFiles[fileCompID[-1]]["optparents"] |= set(tmpProc[proc]["optparents"])
                    subdetProcesses[subDet][proc] = tmpProc[proc]
                    fileCompID = fileCompID[:-1]
                fileCompID = fileCompID[:-1]
            ddlList = Detector.GetDimuonDDLList()
            tmpProc = self.ReadProcesses( ctxt.xpathEval( "/SimpleChainConfig2/ALICE/DIMU" )[0], "DIMU", ddlList, set( [ "MUONTRK", "MUONTRG" ] ), detector="DIMU" )
            for proc in tmpProc:
                fileCompID.append ( fileCompID[-1]+"/"+proc )
                if dimuProcesses.has_key(proc):
                    raise ConfigException( "Dimuon process with ID '"+proc+"' is defined in multiple input files" )
                if not self.fFiles.has_key(fileCompID[-1]):
                    self.fFiles[fileCompID[-1]] = { "files" : set(), "ddls" : set(), "parents" : set(), "optparents" : set() }
                self.fFiles[fileCompID[-1]]["files"].add( currentFile )
                self.fFiles[fileCompID[-1]]["ddls"] |= ddlList
                self.fFiles[fileCompID[-1]]["parents"] |= set(tmpProc[proc]["parents"])
                self.fFiles[fileCompID[-1]]["optparents"] |= set(tmpProc[proc]["optparents"])
                dimuProcesses[proc] = tmpProc[proc]
                fileCompID = fileCompID[:-1]
            fileCompID = fileCompID[:-1]
        return ( partProcesses["TRG"], partProcesses["TRK"], subdetProcesses["TRG"], subdetProcesses["TRK"], dimuProcesses )
        
    def CheckDimuonPartitionProcesses( self, subDet, partProcesses ):
        partProcessList = {}
        self.fProcesses["DIMU/"+subDet+"/Partition"] = {}
        partIDList = partProcesses.keys()
        partIDList.sort()
        for partID in partIDList:
            for procID in partProcesses[partID].keys():
                if not partProcessList.has_key(procID):
                    partProcessList[procID] = {}
                    #print sliceID,partID,procID,partProcesses[sliceID][partID][procID]
                ddls = Detector.GetDimuonDDLList( subDet, partID ) & self.fDDLs
                for par in partProcesses[partID][procID]["parents"]:
                    if par=="DDL":
                        for ddl in ddls:
                            if not self.fRORCs["GENERIC.enable"] and not (self.fRORCs.has_key("%d.enable" % ddl) and self.fRORCs["%d.enable" % ddl]):
                                # raise ConfigException( "DDL parent given for Dimuon %s partition process '%s' cannot be found (DDL %d not activated)" % (subDet, procID, ddl) )
                                print "Warning: DDL parent given for Dimuon %s partition process '%s' cannot be found (DDL %d not activated)" % (subDet, procID, ddl)
                        continue
                    if par=="HWCoProc":
                        for ddl in ddls:
                            if not self.fRORCs["GENERIC.hwcoproc"] and not (self.fRORCs.has_key("%d.hwcoproc" % ddl) and self.fRORCs["%d.hwcoproc" % ddl]):
                                # raise ConfigException( "HWCoProc parent given for Dimuon %s partition process '%s' cannot be found (HWCoProc not activated for DDL %d)" % (subDet, procID, ddl) )
                                print "Warning: HWCoProc parent given for Dimuon %s partition process '%s' cannot be found (HWCoProc not activated for DDL %d)" % (subDet, procID, ddl)
                        continue
                    found=False
                    for partID2 in partProcesses.keys():
                        if partProcesses[partID2].has_key( par ):
                            found=True
                            break
                    if not found and not self.fRetrievalMode:
                        raise ConfigException( "Parent ID '%s' given for Dimuon %s partition process '%s' is not defined (has to be defined as a partition process)" % (par,subDet,procID) )
                if self.fProcesses["DIMU/"+subDet+"/Partition"].has_key(procID):
                    self.fProcesses["DIMU/"+subDet+"/Partition"][procID] |= ddls
                else:
                    self.fProcesses["DIMU/"+subDet+"/Partition"][procID] = ddls
                for ddl in ddls:
                    partProcessList[procID][ddl] = partProcesses[partID][procID]
        return partProcessList

    def CheckDimuonSubdetectorProcesses( self, subDet, subdetProcesses, partProcessList ):
        subdetProcessList = {}
        self.fProcesses["DIMU/"+subDet] = {}
        for procID in subdetProcesses.keys():
            if not subdetProcessList.has_key(procID):
                subdetProcessList[procID] = {}
            if self.fProcesses["DIMU/"+subDet+"/Partition"].has_key(procID):
                raise ConfigException( "ID '%s' given both as Dimuon %s partition process as well as Diumon %s process" % (procID,subDet,subDet) )
            ddls = Detector.GetDimuonDDLList( subDet ) & self.fDDLs
            for par in subdetProcesses[procID]["parents"]:
                if par=="DDL":
                    for ddl in ddls:
                        if not self.fRORCs["GENERIC.enable"] and not (self.fRORCs.has_key("%d.enable" % ddl) and self.fRORCs["%d.enable" % ddl]):
                            # raise ConfigException( "DDL parent given for Dimuon %s process '%s' cannot be found (DDL %d not activated)" % (subDet, procID, ddl) )
                            print "Warning: DDL parent given for Dimuon %s process '%s' cannot be found (DDL %d not activated)" % (subDet, procID, ddl)
                    continue
                if par=="HWCoProc":
                    for ddl in ddls:
                        if not self.fRORCs["GENERIC.hwcoproc"] and not (self.fRORCs.has_key("%d.hwcoproc" % ddl) and self.fRORCs["%d.hwcoproc" % ddl]):
                            # raise ConfigException( "HWCoProc parent given for Dimuon %s process '%s' cannot be found (HWCoProc not activated for DDL %d)" % (subDet, procID, ddl) )
                            print "Warning: HWCoProc parent given for Dimuon %s process '%s' cannot be found (HWCoProc not activated for DDL %d)" % (subDet, procID, ddl)
                    continue
                found=partProcessList.has_key(par) or subdetProcesses.has_key( par )
                if not found and not self.fRetrievalMode:
                    raise ConfigException( "Parent ID '%s' given for Dimuon %s process '%s' is not defined (has to be defined as a partition or subdetector process)" % (par,subDet,procID) )
                if not self.fProcesses["DIMU/"+subDet].has_key(procID):
                    self.fProcesses["DIMU/"+subDet][procID] = 1
                # for subdet in subdetList:
                subdetProcessList[procID] = subdetProcesses[procID]
        return subdetProcessList


    def CheckDimuonProcesses( self, dimuProcesses, subdetProcesses, partProcesses ):
        self.fProcesses["DIMU"] = {}
        for procID in dimuProcesses.keys():
            for subDet in [ "TRG", "TRK" ]:
                if self.fProcesses["DIMU/"+subDet+"/Partition"].has_key(procID):
                    raise ConfigException( "ID '%s' given both as Dimuon %s partition process as well as whole Dimuon process" % (procID,subDet) )
                if self.fProcesses["DIMU/"+subDet].has_key(procID):
                    raise ConfigException( "ID '%s' given both as Dimuon %s process as well as whole Dimuon process" % (procID,subDet) )
            ddls = Detector.GetDimuonDDLList() & self.fDDLs
            for par in dimuProcesses[procID]["parents"]:
                if par=="DDL":
                    for ddl in ddls:
                        if not self.fRORCs["GENERIC.enable"] and not (self.fRORCs.has_key("%d.enable" % ddl) and self.fRORCs["%d.enable" % ddl]):
                            # raise ConfigException( "DDL parent given for Dimuon process '%s' cannot be found (DDL %d not activated)" % (procID, ddl) )
                            print "Warning: DDL parent given for Dimuon process '%s' cannot be found (DDL %d not activated)" % (procID, ddl)
                    continue
                if par=="HWCoProc":
                    for ddl in ddls:
                        if not self.fRORCs["GENERIC.hwcoproc"] and not (self.fRORCs.has_key("%d.hwcoproc" % ddl) and self.fRORCs["%d.hwcoproc" % ddl]):
                            # raise ConfigException( "HWCoProc parent given for Dimuon process '%s' cannot be found (HWCoProc not activated for DDL %d)" % (procID, ddl) )
                            print "Warning: HWCoProc parent given for Dimuon process '%s' cannot be found (HWCoProc not activated for DDL %d)" % (procID, ddl)
                    continue
                found=False
                for subDet in [ "TRG", "TRK" ]:
                    for partID2 in partProcesses[subDet].keys():
                        if partProcesses[subDet][partID2].has_key( par ):
                            found=True
                            break
                    if not found:
                        for ddlID2 in subdetProcesses[subDet].keys():
                            if subdetProcesses[subDet].has_key( par ):
                                found=True
                                break
                    if found:
                        break
                if not found and not dimuProcesses.has_key( par ) and not self.fRetrievalMode:
                    raise ConfigException( "Parent ID '%s' given for Dimuon process '%s' is not defined (has to be defined as a partition, subdetector, or Dimuon process)" % (par,procID) )
            self.fProcesses["DIMU"][procID] = 1


##        subdetProcessList = {}
##        self.fProcesses["DIMU/"+subDet] = {}
##        for procID in subdetProcesses.keys():
##            if not subdetProcessList.has_key(procID):
##                subdetProcessList[procID] = {}
##                for par in subdetProcesses[procID]["parents"]:
##                    if par=="DDL":
##                        continue
##                    found=False
##                    for subdetID2 in subdetProcesses.keys():
##                        if subdetProcesses[subdetID2].has_key( par ):
##                            found=True
##                            break
##                        if 
##                        if not found:
##                            raise ConfigException( "Parent ID '%s' given for Dimuon %s partition process '%s' is not defined (has to be defined as a partition process)" % (par,subDet,procID) )
##                    ddls = Detector.GetDimuonDDLList( subDet, subdetID ) & self.fDDLs
##                    if self.fProcesses["DIMU/"+subDet+"/Partition"].has_key(procID):
##                        self.fProcesses["DIMU/"+subDet+"/Partition"][procID] |= ddls
##                    else:
##                        self.fProcesses["DIMU/"+subDet+"/Partition"][procID] = ddls
##                    for ddl in ddls:
##                        subdetProcessList[procID][ddl] = subdetProcesses[subdetID][procID]
##        return subdetProcessList

    def MakeDimuonProcesses( self ):
        partProcessList = {}
        subdetProcessList = {}
        for subDet in ["TRG", "TRK"]:
            if self.fProcessList.has_key("DIMU/"+subDet+"/Partition"):
                partProcessList[subDet] = self.fProcessList["DIMU/"+subDet+"/Partition"]
                for proc in partProcessList[subDet].keys():
                    for ddl in partProcessList[subDet][proc].keys():
                        self.MakeProcess( partProcessList[subDet][proc][ddl], ["DIMU/"+subDet+"/Partition"], ddlID=ddl,detID="DIMU" )

        for subDet in ["TRG", "TRK"]:
            if self.fProcessList.has_key("DIMU/"+subDet):
                subdetProcessList[subDet] = self.fProcessList["DIMU/"+subDet]
                for proc in subdetProcessList[subDet].keys():
                    self.MakeProcess( subdetProcessList[subDet][proc], ["DIMU/"+subDet+"/Partition","DIMU/"+subDet], subDetID=subDet, detID="DIMU" )

        if self.fProcessList.has_key("DIMU"):
            dimuProcesses = self.fProcessList["DIMU"]
            for proc in dimuProcesses.keys():
                self.MakeProcess( dimuProcesses[proc], ["DIMU/TRG/Partition","DIMU/TRG","DIMU/TRK/Partition","DIMU/TRK","DIMU"], detID="DIMU" )



    def GetMissingDimuonParentComponents( self ):
        missing = set()
        for subDet in ["TRG", "TRK"]:
            if self.fProcessList.has_key("DIMU/"+subDet+"/Partition"):
                partProcessList = self.fProcessList["DIMU/"+subDet+"/Partition"]
                for proc in partProcessList.keys():
                    for ddl in partProcessList[proc].keys():
                        missing |= self.GetMissingParentsForComponent( partProcessList[proc][ddl], ["DIMU/"+subDet+"/Partition"], ddlID=ddl,detID="DIMU" )

        for subDet in ["TRG", "TRK"]:
            if self.fProcessList.has_key("DIMU/"+subDet):
                subdetProcessList = self.fProcessList["DIMU/"+subDet]
                for proc in subdetProcessList.keys():
                    missing |= self.GetMissingParentsForComponent( subdetProcessList[proc], ["DIMU/"+subDet+"/Partition","DIMU/"+subDet], subDetID=subDet, detID="DIMU" )
                    
        if self.fProcessList.has_key("DIMU"):
            dimuProcesses = self.fProcessList["DIMU"]
            for proc in dimuProcesses.keys():
                missing |= self.GetMissingParentsForComponent( dimuProcesses[proc], ["DIMU/TRG/Partition","DIMU/TRG","DIMU/TRK/Partition","DIMU/TRK","DIMU"], detID="DIMU" )
        return missing

    def OutputDimuonGraph( self, outputFile ):
        foundDetector=False
        detectorID = "DIMU"
        for proc in self.fFiles.keys():
            if proc[:len(detectorID)]==detectorID:
                foundDetector=True
        if not foundDetector:
            return
        outputFile.write( "\n" )
        outputFile.write( "  subgraph cluster_"+detectorID+" {\n" )
        outputFile.write( "\n" )
        for subDet in ( "TRG", "TRK" ):
            outputFile.write( "    subgraph cluster_"+detectorID+"_"+subDet+" {\n" )
            outputFile.write( "      subgraph cluster_"+detectorID+"_"+subDet+"_Partition {\n" )
            idPrefix = detectorID+"/"+subDet+"/Partition"
            outputFile.write( "        node [ style=filled color=black fillcolor=white]\n" )
            ddlList = self.fDDLs
            if len(Detector.GetDimuonDDLList(subDet)-ddlList)<=0:
                ddlListStr = detectorID+"_"+subDet
            elif len(Detector.GetDimuonDDLList(subDet)-ddlList)<(len(Detector.GetDimuonDDLList(subDet))/2):
                ddlListStr = detectorID+"_"+subDet+"-["+self.MakeListString( Detector.GetDimuonDDLList(subDet)-ddlList )+"]"
            else:
                ddlListStr = self.MakeListString( ddlList & Detector.GetDimuonDDLList(subDet) )
            outputFile.write( "        "+detectorID+"_"+subDet+"_DDL [shape=record label=\"<ID>DDL | DDLs='"+ddlListStr+"'\"];\n" )
            self.OutputGraphProcesses( outputFile, detectorID, detectorID+"_"+subDet, idPrefix, "        " )
            # outputFile.write( "        TPC_CF [shape=record label="<ID>CF | IDs='768-983' | File=Components-TPC-CF.xml"];\n" )
            # outputFile.write( "        TPC_DDL:ID -> TPC_CF:ID;\n" )
            outputFile.write( "        label = \"Partition\";\n" )
            outputFile.write( "	   color=black;\n" )
            outputFile.write( "        fillcolor=white;\n" )
            outputFile.write( "        style=filled;\n" )
            outputFile.write( "      }\n" )
            idPrefix = detectorID+"/"+subDet
            outputFile.write( "      node [ style=filled color=black fillcolor=white]\n" )
            self.OutputGraphProcesses( outputFile, detectorID, detectorID+"_"+subDet, idPrefix, "      " )
            # outputFile.write( "      TPC_CF:ID -> TPC_TR:ID;\n" )
            # outputFile.write( "      TPC_TR [shape=record label="<ID>TR | Slices='0-36' | File=Components-TPC-TR.xml"];\n" )
            outputFile.write( "      label = \""+subDet+"\";\n" )
            outputFile.write( "      color=black;\n" )
            outputFile.write( "      fillcolor=lightgrey;\n" )
            outputFile.write( "      style=filled;\n" )
            outputFile.write( "    }\n" )
        idPrefix = detectorID
        outputFile.write( "    node [ style=filled color=black fillcolor=white]\n" )
        self.OutputGraphProcesses( outputFile, detectorID, detectorID, idPrefix, "    " )
        #outputFile.write( "    TPC_GM [shape=record label="<ID>GM | File=Components-TPC-GM.xml"];\n" )
        #outputFile.write( "    TPC_TR:ID -> TPC_GM:ID;\n" )
        outputFile.write( "    label = \""+detectorID+"\";\n" )
        outputFile.write( "    color=black;\n" )
        outputFile.write( "    fillcolor=grey;\n" )
        outputFile.write( "    style=filled;\n" )
        outputFile.write( "  }\n" )
        outputFile.write( "\n" )


    def Read2LevelDetector( self, detector, readDefault=False ):
        foundDet=False
        if not readDefault:
            xmlDetID = detector
        else:
            xmlDetID = "DEFAULT"
        for ctxt in self.fXPathContexts:
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+xmlDetID ))>0:
                foundDet=True
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+xmlDetID ))>1:
                raise ConfigException( "Only one /SimpleChainConfig2/ALICE/"+xmlDetID+" node allowed in each XML file" )

        if not foundDet:
            return
        # First read in the detector option overrides for the various TPC parts
        self.Read2LevelDetectorOptions( detector, readDefault )
        
        # Create all the source processes
        for i in Detector.NameToDDLList( detector ) & self.fDDLs:
            self.MakeDDLSource( i )
        #print "Sources: ["
        #for p in self.fConfig.GetProcesses():
        #    print p,","
        #print "]"

        # Read in partition, slice, and TPC global processes
        partProcesses,detProcesses = self.Read2LevelProcesses( detector, readDefault )
        # print "2Level "+detector+" Partition processes:",partProcesses
        #print "TPC Slice processes:",sliceProcesses
        # print "2Level "+detector+" processes:",detProcesses

        # Iterate all processes, check parents, mark which process to use for which unit/part
        self.fProcessList[detector+"/Partition"] = self.Check2LevelPartitionProcesses( detector, partProcesses, readDefault )
        # print self.fProcessList[detector+"/Partition"]

        self.Check2LevelProcesses( detector, detProcesses, partProcesses, readDefault )
        self.fProcessList[detector] = detProcesses

            

    def Read2LevelDetectorOptions(self,detector,readDefault=False):
        if not readDefault:
            xmlDetID = detector
        else:
            xmlDetID = "DEFAULT"
        for ctxt in self.fXPathContexts:
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+xmlDetID ))<=0:
                continue
            self.ApplyDetectorOptions( ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+xmlDetID )[0], Detector.NameToDDLList(detector), False )
        #print "self.fRORCs (Apply TPC Options):",self.fRORCs
        for ctxt in self.fXPathContexts:
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+xmlDetID ))<=0:
                continue
            partitions = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+xmlDetID+"/Partition" )
            for partitionNode in partitions:
                if len(partitionNode.xpathEval( "@ID" ))>0:
                    partIDs = self.ParseList( partitionNode.xpathEval( "@ID" )[0].content, numbers=True, StringLookup=lambda x: set([Detector.GetDetectorPartitionDDL( detector, int(x))]), applyStringLookupToRangeElements=True )
                else:
                    partIDs = [None]
                for partID in partIDs:
                    if partID==None:
                        ddls = Detector.NameToDDLList(detector)
                    else:
                        ddls = set([partID])

                    self.ApplyDetectorOptions( partitionNode, ddls, partID!=None )
                # if partID==None:
                #    partPrint=-1
                # else:
                #    partPrint=partID
                # print "self.fRORCs (Apply TPC Slice %d Partition %d Options):"%(slicePrint,partPrint),self.fRORCs

    def Read2LevelProcesses(self,detector,readDefault=False):
        # Read in partition and global processes for detector
        if not readDefault:
            xmlDetID = detector
            defErrorMes = ""
        else:
            xmlDetID = "DEFAULT"
            defErrorMes = " (in DEFAULT tag)"
        partProcesses = {}
        detProcesses = {}
        partProcesses = {}
        fileCompID = []
        for ctxt in self.fXPathContexts:
            currentFile = self.GetXMLFilenameFromXMLContext( ctxt )
            fileCompID.append( detector )
            if len(ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+xmlDetID ))<=0:
                continue
            partitions = ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+xmlDetID+"/Partition" )
            for partNode in partitions:
                if len(partNode.xpathEval( "@ID" ))>0:
                    partIDs = self.ParseList( partNode.xpathEval( "@ID" )[0].content, numbers=True, StringLookup=lambda x: set([Detector.GetDetectorPartitionDDL( detector, int(x))]), applyStringLookupToRangeElements=True )
                else:
                    partIDs = [None]
                fileCompID.append ( fileCompID[-1]+"/Partition" )
                for partID in partIDs:
                    if partID==None:
                        partPrint = -1
                    else:
                        partPrint = partID
                        partID = Detector.GetDetectorPartitionDDL( detector, partID )
                        if not (partID in Detector.NameToDDLList(detector)):
                            raise ConfigException( detector+" Partition ID "+str(partID)+" not known"+defErrorMes )
                    if partID==None:
                        ddlList = Detector.NameToDDLList(detector)
                    else:
                        ddlList = set([partID])
                    tmpProc = self.ReadProcesses( partNode, detector+" Partition %d" % (partPrint), ddlList, set([detector]), detector=detector )
                    if not partProcesses.has_key(partPrint):
                        partProcesses[partPrint] = {}
                    for proc in tmpProc.keys():
                        fileCompID.append ( fileCompID[-1]+"/"+proc )
                        if partProcesses[partPrint].has_key(proc):
                            raise ConfigException( detector+" partition process with ID '"+proc+"' is defined in multiple input files"+defErrorMes )
                        if not self.fFiles.has_key(fileCompID[-1]):
                            self.fFiles[fileCompID[-1]] = { "files" : set(), "ddls" : set(), "parents" : set(), "optparents" : set() }
                        self.fFiles[fileCompID[-1]]["files"].add( currentFile )
                        self.fFiles[fileCompID[-1]]["ddls"] |= ddlList
                        self.fFiles[fileCompID[-1]]["parents"] |= set(tmpProc[proc]["parents"])
                        self.fFiles[fileCompID[-1]]["optparents"] |= set(tmpProc[proc]["optparents"])
                        partProcesses[partPrint][proc] = tmpProc[proc]
                        fileCompID = fileCompID[:-1]
                fileCompID = fileCompID[:-1]
            
            ddlList = Detector.NameToDDLList(detector)
            tmpProc = self.ReadProcesses( ctxt.xpathEval( "/SimpleChainConfig2/ALICE/"+xmlDetID )[0], detector, ddlList, set([detector]), detector=detector )
            #print xmlDetID,ctxt,ctxt.contextDoc(),tmpProc.keys()
            for proc in tmpProc.keys():
                fileCompID.append ( fileCompID[-1]+"/"+proc )
                if detProcesses.has_key(proc):
                    raise ConfigException( detector+" process with ID '"+proc+"' is defined in multiple input files"+defErrorMes )
                if not self.fFiles.has_key(fileCompID[-1]):
                    self.fFiles[fileCompID[-1]] = { "files" : set(), "ddls" : set(), "parents" : set(), "optparents" : set() }
                self.fFiles[fileCompID[-1]]["files"].add( currentFile )
                self.fFiles[fileCompID[-1]]["ddls"] |= ddlList
                self.fFiles[fileCompID[-1]]["parents"] |= set(tmpProc[proc]["parents"])
                self.fFiles[fileCompID[-1]]["optparents"] |= set(tmpProc[proc]["optparents"])
                detProcesses[proc] = tmpProc[proc]
                fileCompID = fileCompID[:-1]
            fileCompID = fileCompID[:-1]
        return ( partProcesses, detProcesses )

    def Check2LevelPartitionProcesses( self, detector, partProcesses, readDefault=False ):
        if not readDefault:
            xmlDetID = detector
            defErrorMes = ""
        else:
            xmlDetID = "DEFAULT"
            defErrorMes = " (in DEFAULT tag)"
        partProcessList = {}
        self.fProcesses[detector+"/Partition"] = {}
        partIDList = partProcesses.keys()
        partIDList.sort()
        for partID in partIDList:
            for procID in partProcesses[partID].keys():
                if not partProcessList.has_key(procID):
                    partProcessList[procID] = {}
                    #print sliceID,partID,procID,partProcesses[sliceID][partID][procID]
                if partID==-1 or partID==None:
                    ddls = Detector.NameToDDLList(detector) & self.fDDLs
                else:
                    ddls = set( [partID] ) & self.fDDLs
                for par in partProcesses[partID][procID]["parents"]:
                    if par=="DDL":
                        for ddl in ddls:
                            if not self.fRORCs["GENERIC.enable"] and not (self.fRORCs.has_key("%d.enable" % ddl) and self.fRORCs["%d.enable" % ddl]):
                                # raise ConfigException( ("DDL parent given for "+detector+" partition process '%s' cannot be found (DDL %d not activated)") % (procID, ddl) )
                                print ("Warning: DDL parent given for "+detector+" partition process '%s' cannot be found (DDL %d not activated)") % (procID, ddl)
                        continue
                    if par=="HWCoProc":
                        for ddl in ddls:
                            if not self.fRORCs["GENERIC.hwcoproc"] and not (self.fRORCs.has_key("%d.hwcoproc" % ddl) and self.fRORCs["%d.hwcoproc" % ddl]):
                                # raise ConfigException( ("HWCoProc parent given for "+detector+" partition process '%s' cannot be found (HWCoProc not activated for DDL %d)") % (procID, ddl) )
                                print ("Warning: HWCoProc parent given for "+detector+" partition process '%s' cannot be found (HWCoProc not activated for DDL %d)") % (procID, ddl)
                        continue
                    found=False
                    for partID2 in partProcesses.keys():
                        if partProcesses[partID2].has_key( par ):
                            found=True
                            break
                    if not found and not self.fRetrievalMode:
                        raise ConfigException( (("Parent ID '%s' given for "+detector+" partition process '%s' is not defined (has to be defined as a partition process)") % (par,procID))+defErrorMes )
                if self.fProcesses[detector+"/Partition"].has_key(procID):
                    self.fProcesses[detector+"/Partition"][procID] |= ddls
                else:
                    self.fProcesses[detector+"/Partition"][procID] = ddls
                for ddl in ddls:
                    partProcessList[procID][ddl] = partProcesses[partID][procID]
        return partProcessList

    def Check2LevelProcesses( self, detector, detProcesses, partProcesses, readDefault=False ):
        if not readDefault:
            xmlDetID = detector
            defErrorMes = ""
        else:
            xmlDetID = "DEFAULT"
            defErrorMes = " (in DEFAULT tag)"
        self.fProcesses[detector] = {}
        for procID in detProcesses.keys():
            if self.fProcesses[detector+"/Partition"].has_key(procID):
                raise ConfigException( (("ID '%s' given both as "+detector+" partition as well as whole "+detector+" process") % procID)+defErrorMes )
            ddls = Detector.NameToDDLList(detector) & self.fDDLs
            for par in detProcesses[procID]["parents"]:
                if par=="DDL":
                    for ddl in ddls:
                        if not self.fRORCs["GENERIC.enable"] and not (self.fRORCs.has_key("%d.enable" % ddl) and self.fRORCs["%d.enable" % ddl]):
                            # raise ConfigException( ("DDL parent given for "+detector+" partition process '%s' cannot be found (DDL %d not activated)") % (procID, ddl) )
                            print ("Warning: DDL parent given for "+detector+" partition process '%s' cannot be found (DDL %d not activated)") % (procID, ddl)
                    continue
                if par=="HWCoProc":
                    for ddl in ddls:
                        if not self.fRORCs["GENERIC.hwcoproc"] and not (self.fRORCs.has_key("%d.hwcoproc" % ddl) and self.fRORCs["%d.hwcoproc" % ddl]):
                            # raise ConfigException( ("HWCoProc parent given for "+detector+" partition process '%s' cannot be found (HWCoProc not activated for DDL %d)") % (procID, ddl) )
                            print ("Warning: HWCoProc parent given for "+detector+" partition process '%s' cannot be found (HWCoProc not activated for DDL %d)") % (procID, ddl)
                    continue
                found=False
                for partID2 in partProcesses.keys():
                    if partProcesses[partID2].has_key( par ):
                        found=True
                        break
                if not found and not detProcesses.has_key( par ) and not self.fRetrievalMode:
                    raise ConfigException( (("Parent ID '%s' given for "+detector+" process '%s' is not defined (has to be defined as a partition or "+detector+" process)") % (par,procID))+defErrorMes )
            self.fProcesses[detector][procID] = 1

    def Make2LevelDetectorProcesses( self, detector ):
        # This order is necessary, since specific settings can override generic ones
        # Only after iterating over all slices and partitions is this determined
        # Also, we need to define all possible parents before starting to create processes
        if self.fProcessList.has_key(detector+"/Partition"):
            partProcessList = self.fProcessList[detector+"/Partition"]
            for proc in partProcessList.keys():
                for ddl in partProcessList[proc].keys():
                    self.MakeProcess( partProcessList[proc][ddl], [detector+"/Partition"], ddlID=ddl,detID=detector )

        #print "Sources+TPC Partition Processes: ["
        #for p in self.fConfig.GetProcesses():
        #    print p,","
        #print "]"

        if self.fProcessList.has_key(detector):
            detProcesses = self.fProcessList[detector]
            for proc in detProcesses.keys():
                self.MakeProcess( detProcesses[proc], [detector+"/Partition", detector], detID=detector )

        #print "Sources+All TPC Processes: ["
        #for p in self.fConfig.GetProcesses():
        #    print p,","
        #print "]"
        

    def GetMissing2LevelDetectorParentComponents( self, detector ):
        missing = set()
        if self.fProcessList.has_key(detector+"/Partition"):
            partProcessList = self.fProcessList[detector+"/Partition"]
            for proc in partProcessList.keys():
                for ddl in partProcessList[proc].keys():
                    missing |= self.GetMissingParentsForComponent( partProcessList[proc][ddl], [detector+"/Partition"], ddlID=ddl,detID=detector )
                    
        if self.fProcessList.has_key(detector):
            detProcesses = self.fProcessList[detector]
            for proc in detProcesses.keys():
                missing |= self.GetMissingParentsForComponent( detProcesses[proc], [detector+"/Partition", detector], detID=detector )
        return missing

    def Output2LevelDetectorGraph( self, outputFile, detectorID ):
        foundDetector=False
        for proc in self.fFiles.keys():
            if proc[:len(detectorID)]==detectorID:
                foundDetector=True
        if not foundDetector:
            return
        outputFile.write( "\n" )
        outputFile.write( "  subgraph cluster_"+detectorID+" {\n" )
        outputFile.write( "\n" )
        outputFile.write( "      subgraph cluster_"+detectorID+"_Partition {\n" )
        idPrefix = detectorID+"/Partition"
        outputFile.write( "        node [ style=filled color=black fillcolor=white]\n" )
        ddlList = self.fDDLs
        if len(Detector.NameToDDLList(detectorID)-ddlList)<=0:
            ddlListStr = detectorID
        elif len(Detector.NameToDDLList(detectorID)-ddlList)<(len(Detector.NameToDDLList(detectorID))/2):
            ddlListStr = detectorID+"-["+self.MakeListString( Detector.NameToDDLList(detectorID)-ddlList )+"]"
        else:
            ddlListStr = self.MakeListString( ddlList & Detector.NameToDDLList(detectorID) )
        outputFile.write( "        "+detectorID+"_DDL [shape=record label=\"<ID>DDL | DDLs='"+ddlListStr+"'\"];\n" )
        self.OutputGraphProcesses( outputFile, detectorID, detectorID, idPrefix, "        " )
        #outputFile.write( "        TPC_CF [shape=record label="<ID>CF | IDs='768-983' | File=Components-TPC-CF.xml"];\n" )
        #outputFile.write( "        TPC_DDL:ID -> TPC_CF:ID;\n" )
        outputFile.write( "        label = \"Partition\";\n" )
        outputFile.write( "	   color=black;\n" )
        outputFile.write( "        fillcolor=white;\n" )
        outputFile.write( "        style=filled;\n" )
        outputFile.write( "      }\n" )
        idPrefix = detectorID
        outputFile.write( "    node [ style=filled color=black fillcolor=white]\n" )
        self.OutputGraphProcesses( outputFile, detectorID, detectorID, idPrefix, "    " )
        #outputFile.write( "    TPC_GM [shape=record label="<ID>GM | File=Components-TPC-GM.xml"];\n" )
        #outputFile.write( "    TPC_TR:ID -> TPC_GM:ID;\n" )
        outputFile.write( "    label = \""+detectorID+"\";\n" )
        outputFile.write( "    color=black;\n" )
        outputFile.write( "    fillcolor=grey;\n" )
        outputFile.write( "    style=filled;\n" )
        outputFile.write( "  }\n" )
        outputFile.write( "\n" )


    def ReadALICEProcesses(self):
        # Read in global processes
        procs = {}
        for ctxt in self.fXPathContexts:
            currentFile = self.GetXMLFilenameFromXMLContext( ctxt )
            tmpProc = self.ReadProcesses( ctxt.xpathEval( "/SimpleChainConfig2/ALICE" )[0], "ALICE", Detector.NameToDDLList( "ALICE" ) )
            for proc in tmpProc.keys():
                if procs.has_key(proc):
                    raise ConfigException( "ALICE global process with ID '"+proc+"' is defined in multiple input files" )
                if not self.fFiles.has_key("ALICE/"+proc):
                    self.fFiles["ALICE/"+proc] = { "files" : set(), "ddls" : set(), "parents" : set(), "optparents" : set() }
                self.fFiles["ALICE/"+proc]["files"].add( currentFile )
                self.fFiles["ALICE/"+proc]["ddls"] |= Detector.NameToDDLList("ALICE") & self.fDDLs
                self.fFiles["ALICE/"+proc]["parents"] |= set(tmpProc[proc]["parents"])
                self.fFiles["ALICE/"+proc]["optparents"] |= set(tmpProc[proc]["optparents"])
                procs[proc] = tmpProc[proc]
        return procs

    def CheckALICEProcesses( self, processes ):
        procs = {}
        # print self.fProcesses
        for procID in processes.keys():
            ddls = Detector.NameToDDLList("ALICE") & self.fDDLs
            for ext_par in processes[procID]["parents"]:
                par=ext_par.split('::')[0]
                if par=="DDL" or par.split("/")[-1]=="DDL" or processes.has_key(par) or (len(par.split("/")) >= 2 and par.split("/")[-2] == "DDL"):
                    if len(par.split("/"))>1:
                        ddls = Detector.NameToDDLList(par.split("/")[0]) & self.fDDLs
                    else:
                        ddls = Detector.NameToDDLList("ALICE") & self.fDDLs
                    for ddl in ddls:
                        if not self.fRORCs["GENERIC.enable"] and not (self.fRORCs.has_key("%d.enable" % ddl) and self.fRORCs["%d.enable" % ddl]):
                            # raise ConfigException( "DDL parent given for ALICE process '%s' cannot be found (DDL %d not activated)" % (procID, ddl) )
                            print "Warning: DDL parent given for ALICE process '%s' cannot be found (DDL %d not activated)" % (procID, ddl)
                    continue
                if par=="HWCoProc" or par.split("/")[-1]=="HWCoProc":
                    if len(par.split("/"))>1:
                        ddls = Detector.NameToDDLList(par.split("/")[0]) & self.fDDLs
                    else:
                        ddls = Detector.NameToDDLList("ALICE") & self.fDDLs
                    for ddl in ddls:
                        if not self.fRORCs["GENERIC.hwcoproc"] and not (self.fRORCs.has_key("%d.hwcoproc" % ddl) and self.fRORCs["%d.hwcoproc" % ddl]):
                            # raise ConfigException( "HWCoProc parent given for ALICE process '%s' cannot be found (HWCoProc not activated for DDL %d)" % (procID, ddl) )
                            print "Warning: HWCoProc parent given for ALICE process '%s' cannot be found (HWCoProc not activated for DDL %d)" % (procID, ddl)
                    continue
                found=False
                for pp in self.fProcesses.keys():
                    if (len(pp.split("/"))>1 or len(par.split("/"))>1) and pp.split("/")[0]!=par.split("/")[0]:
                        continue
                    if self.fProcesses[pp].has_key(par.split("/")[-1]):
                        found=True
                        break
                if not found and not self.fRetrievalMode:
                    raise ConfigException( "Parent ID '%s' given for ALICE process '%s' is not defined" % (par,procID) )
            self.fProcesses["ALICE"][procID] = 1
            procs[procID] = processes[procID]
        return procs

    def OutputALICEGraph( self, outputFile ):
        outputFile.write( "\n" )
        outputFile.write( "  node [ style=filled color=black fillcolor=white]\n" )
        self.OutputGraphProcesses( outputFile, "ALICE", "ALICE", "ALICE", "  " )
        #outputFile.write( "    TPC_GM [shape=record label="<ID>GM | File=Components-TPC-GM.xml"];\n" )
        #outputFile.write( "    TPC_TR:ID -> TPC_GM:ID;\n" )
        outputFile.write( "\n" )



    def CheckOutput( self ):
        ### if len(self.fOutputDDLs)<=0 or len(self.fOutputParents)<=0:
        if (self.fOutputType!="file" and len(self.fOutputDDLs)<=0) or (self.fOutputType!="ddl" and len(self.fOutputNodes)<=0) or len(self.fOutputParents)<=0:
            return
        ddls = Detector.NameToDDLList("ALICE") & self.fDDLs
        for ext_par in self.fOutputParents:
            par=ext_par.split('::')[0]
            if par=="DDL" or par.split("/")[-1]=="DDL":
                if len(par.split("/"))>1:
                    ddls = Detector.NameToDDLList(par.split("/")[0]) & self.fDDLs
                else:
                    ddls = Detector.NameToDDLList("ALICE") & self.fDDLs
                for ddl in ddls:
                    if not self.fRORCs["GENERIC.enable"] and not (self.fRORCs.has_key("%d.enable" % ddl) and self.fRORCs["%d.enable" % ddl]):
                        # raise ConfigException( "DDL parent given for output cannot be found (DDL %d not activated)" % (ddl) )
                        print "Warning: DDL parent given for output cannot be found (DDL %d not activated)" % (ddl)
                continue
            if par=="HWCoProc" or par.split("/")[-1]=="HWCoProc":
                if len(par.split("/"))>1:
                    ddls = Detector.NameToDDLList(par.split("/")[0]) & self.fDDLs
                else:
                    ddls = Detector.NameToDDLList("ALICE") & self.fDDLs
                for ddl in ddls:
                    if not self.fRORCs["GENERIC.hwcoproc"] and not (self.fRORCs.has_key("%d.hwcoproc" % ddl) and self.fRORCs["%d.hwcoproc" % ddl]):
                        # raise ConfigException( "HWCoProc parent given for output cannot be found (HWCoProc not activated for DDL %d)" % (ddl) )
                        print "Warning: HWCoProc parent given for output cannot be found (HWCoProc not activated for DDL %d)" % (ddl)
                continue
            found=False
            for pp in self.fProcesses.keys():
                if (len(pp.split("/"))>1 or len(par.split("/"))>1) and pp.split("/")[0]!=par.split("/")[0]:
                    continue
                if self.fProcesses[pp].has_key(par.split("/")[-1]):
                    found=True
                    break
            if not found and not self.fRetrievalMode:
                raise ConfigException( "Parent ID '%s' given for output is not defined" % (par) )
        if self.fOutputType!="file" or self.fOutputFormatterMultiplicity!=0:
            self.fProcesses["ALICE"]["HOF"] = 1
        if self.fOutputType!="ddl":
            self.fProcesses["ALICE"]["HODW"] = 1
        elif self.fOutputType!="file":
            self.fProcesses["ALICE"]["HOWS"] = 1

    def CheckFXS( self ):
        if self.fFXSBase==None or len(self.fFXSNodes)<=0 or len(self.fFXSParents)<=0:
            return
        ddls = Detector.NameToDDLList("ALICE") & self.fDDLs
        for par in self.fFXSParents:
            if par=="DDL" or par.split("/")[-1]=="DDL":
                if len(par.split("/"))>1:
                    ddls = Detector.NameToDDLList(par.split("/")[0]) & self.fDDLs
                else:
                    ddls = Detector.NameToDDLList("ALICE") & self.fDDLs
                for ddl in ddls:
                    if not self.fRORCs["GENERIC.enable"] and not (self.fRORCs.has_key("%d.enable" % ddl) and self.fRORCs["%d.enable" % ddl]):
                        # raise ConfigException( "DDL parent given for FXS cannot be found (DDL %d not activated)" % (ddl) )
                        print "Warning: DDL parent given for FXS cannot be found (DDL %d not activated)" % (ddl)
                continue
            if par=="HWCoProc" or par.split("/")[-1]=="HWCoProc":
                if len(par.split("/"))>1:
                    ddls = Detector.NameToDDLList(par.split("/")[0]) & self.fDDLs
                else:
                    ddls = Detector.NameToDDLList("ALICE") & self.fDDLs
                for ddl in ddls:
                    if not self.fRORCs["GENERIC.hwcoproc"] and not (self.fRORCs.has_key("%d.hwcoproc" % ddl) and self.fRORCs["%d.hwcoproc" % ddl]):
                        # raise ConfigException( "HWCoProc parent given for FXS cannot be found (HWCoProc not activated for DDL %d)" % (ddl) )
                        print "Warning: HWCoProc parent given for FXS cannot be found (HWCoProc not activated for DDL %d)" % (ddl)
                continue
            found=False
            for pp in self.fProcesses.keys():
                if (len(pp.split("/"))>1 or len(par.split("/"))>1) and pp.split("/")[0]!=par.split("/")[0]:
                    continue
                if self.fProcesses[pp].has_key(par.split("/")[-1]):
                    found=True
                    break
            if not found and not self.fRetrievalMode:
                raise ConfigException( "Parent ID '%s' given for FXS is not defined" % (par) )
        self.fProcesses["ALICE"]["FXS"] = 1

    def CheckTrigger( self, triggerType ):
        if len(self.fTriggerParents[triggerType])<=0:
            return
        ddls = Detector.NameToDDLList("ALICE") & self.fDDLs
        for par in self.fTriggerParents[triggerType]:
            if par=="DDL" or par.split("/")[-1]=="DDL":
                if len(par.split("/"))>1:
                    ddls = Detector.NameToDDLList(par.split("/")[0]) & self.fDDLs
                else:
                    ddls = Detector.NameToDDLList("ALICE") & self.fDDLs
                for ddl in ddls:
                    if not self.fRORCs["GENERIC.enable"] and not (self.fRORCs.has_key("%d.enable" % ddl) and self.fRORCs["%d.enable" % ddl]):
                        # raise ConfigException( "DDL parent given for Trigger cannot be found (DDL %d not activated)" % (ddl) )
                        print "Warning: DDL parent given for Trigger cannot be found (DDL %d not activated)" % (ddl)
                continue
            if par=="HWCoProc" or par.split("/")[-1]=="HWCoProc":
                if len(par.split("/"))>1:
                    ddls = Detector.NameToDDLList(par.split("/")[0]) & self.fDDLs
                else:
                    ddls = Detector.NameToDDLList("ALICE") & self.fDDLs
                for ddl in ddls:
                    if not self.fRORCs["GENERIC.hwcoproc"] and not (self.fRORCs.has_key("%d.hwcoproc" % ddl) and self.fRORCs["%d.hwcoproc" % ddl]):
                        # raise ConfigException( "HWCoProc parent given for Trigger cannot be found (HWCoProc not activated for DDL %d)" % (ddl) )
                        print "Warning: HWCoProc parent given for Trigger cannot be found (HWCoProc not activated for DDL %d)" % (ddl)
                continue
            found=False
            for pp in self.fProcesses.keys():
                if (len(pp.split("/"))>1 or len(par.split("/"))>1) and pp.split("/")[0]!=par.split("/")[0]:
                    continue
                if self.fProcesses[pp].has_key(par.split("/")[-1]):
                    found=True
                    break
            if not found and not self.fRetrievalMode:
                raise ConfigException( "Parent ID '%s' given for Trigger is not defined" % (par) )
        self.fProcesses["ALICE"]["Global"+self.fTriggerTypeComponentSpecifier[triggerType]+"Trigger"] = 1


    def GetMissingALICEParentComponents( self ):
        missing = set()
        if self.fProcessList.has_key("ALICE"):
            topProcesses = self.fProcessList["ALICE"]
            for proc in topProcesses.keys():
                missing |= self.GetMissingParentsForComponent( topProcesses[proc], self.fProcesses.keys(), detID="ALICE" )
        return missing

    def CreateOutputProcesses( self ):
        outputGroupID =  "HLTOutput"
        procs = {}
        # print "CreateOutputProcesses:",self.fOutputDDLs,self.fOutputParents
        if (self.fOutputType!="file" and len(self.fOutputDDLs)<=0) or (self.fOutputType!="ddl" and len(self.fOutputNodes)<=0) or len(self.fOutputParents)<=0:
            return procs
        if self.fOutputType!="file" or self.fOutputFormatterMultiplicity!=0:
            nodes = []
            ddls = list(self.fOutputDDLs)
            for ndx in range(len(ddls)):
                ddl = ddls[ndx]
                loc = self.fDDLLocations[ddl]
                node = loc["node"]
                nodes.append( node )
            nodes = nodes[:self.fOutputFormatterMultiplicity]
            if self.fFormatterShmBlocksize!=None and self.fFormatterShmBlockcount!=None:
                maxBlockCount = self.fFormatterShmBlockcount
                blocksize = self.fFormatterShmBlocksize
                if blocksize % 4096:
                    blocksize = ((blocksize >> 12) + 1) << 12

            elif not self.fRetrievalMode:
                parentIDs,parentFilterIDs,parentGroupOutput=self.MakeParentList(self.fOutputParents, self.fProcesses.keys(), detID="ALICE" )
                if len(parentIDs)<=0:
                    return procs
                blocksize = 1024 # Account for some initial overhead
                blocks=0
                maxBlockCount=0
                if not self.fRetrievalMode:
                    for parID in parentIDs:
                        parProcs = []
                        if parID in self.fConfig.GetProcessGroupIDList():
                            parGroup = self.fConfig.GetProcessGroup(parID)
                            for opgroup in parGroup.GetFullOutputProcessList(self.fConfig):
                                opgroupid = parGroup.GetInternalProcessID(opgroup[0], self.fConfig )
                                if parentGroupOutput!=None and parID in parentGroupOutput.keys() and not opgroupid in parentGroupOutput[parID]:
                                    continue
                                parProcs.append(self.fConfig.GetProcess(opgroup[0]))
                        else:
                            parProcs.append(self.fConfig.GetProcess(parID))
                        for parProc in parProcs:
                            blocks += 1 # Assumption: Only one block per parent
                            blocksize += parProc.GetBlockSize()
                            if parProc.GetBlockCount()>maxBlockCount:
                                maxBlockCount = parProc.GetBlockCount()
                    blocksize += blocks*256 # Account for some per block overhead
            else:
                # Set to some more or less random numbers, we only need something here
                blocks = 1 
                blocksize = 65536
                maxBlockCount = 2
                
            addParents = []
            remDetCmd = ""
            # print self.fAutoDetectorReadoutList, self.fParticipatingDetectors,self.fTriggeredDetectors
            if self.fAutoDetectorReadoutList and self.fParticipatingDetectors!=None:
                remDetectors = self.fParticipatingDetectors - self.fTriggeredDetectors
                for det in remDetectors:
                    remDetCmd = remDetCmd + " -setdetectorddls "+det
            hofproc = { 
                "id" : "HOF",
                "type" : "proc",
                "proctype" : "prc",
                "command" : {-1 : "HLTOutFormatter"+remDetCmd },
                "groupopts" : {-1 : "", 0 : " -broadcasteventmaster " },
                "parents" : list(self.fOutputParents)+addParents,
                "mult" : 1, #self.fOutputFormatterMultiplicity,
                "shm" : {"blocksize" : blocksize, "blockcount" : maxBlockCount},
                #"nodes" : nodes,
                "ddllist" : Detector.NameToDDLList( "ALICE" ),
                "parentfilters" : self.fOutputParentFilters,
                "processorgroup" : "process",
                "groupid" : outputGroupID,
                }
            procs["HOF"] = hofproc
            if self.fOutputType!="file":
                hofproc["shmtype"]=self.fFormatterShmType
            #print "HOF:",hofproc
            # self.MakeProcess( hofproc, self.fProcesses.keys(), detID="ALICE" )
        if self.fOutputType!="ddl":
            if self.fOutputFormatterMultiplicity==0:
                parents = self.fOutputParents
            else:
                parents = ["HOF"]
            bdwproc = {
                "id" : "HODW",
                "type" : "proc",
                "proctype" : "snk",
                "command" : { -1 : "BinaryDataWriter -filenameprefix "+self.fOutputDir+"/"+self.fOutputPrefix },
                "parents" : parents,
                "mult" : 1, #len(self.fOutputNodes),
                "shm" : {"blocksize" : 4096, "blockcount" : 1},
                #"nodes" : self.fOutputNodes,
                "ddllist" : Detector.NameToDDLList( "ALICE" ),
                "processorgroup" : "process",
                "groupid" : outputGroupID,
                }
            if self.fOutputFormatterMultiplicity==0:
                bdwproc["parentfilters"] = self.fOutputParentFilters
            procs["HODW"] = bdwproc
            # self.MakeProcess( bdwproc, self.fProcesses.keys(), detID="ALICE" )
        if self.fOutputType!="file":
            if self.fOutputShmSize==None:
                if not self.fRetrievalMode:
                    parentIDs,parentFilterIDs,parentGroupOutput=self.MakeParentList(self.fOutputParents, self.fProcesses.keys(), detID="ALICE" )
                    blocksize=0
                    if not self.fRetrievalMode:
                        for parID in parentIDs:
                            parProcs = []
                            if parID in self.fConfig.GetProcessGroupIDList():
                                parGroup = self.fConfig.GetProcessGroup(parID)
                                for opgroup in parGroup.GetFullOutputProcessList(self.fConfig):
                                    opgroupid = parGroup.GetInternalProcessID(opgroup[0], self.fConfig )
                                    if parentGroupOutput!=None and parID in parentGroupOutput.keys() and not opgroupid in parentGroupOutput[parID]:
                                        continue
                                    parProcs.append(self.fConfig.GetProcess(opgroup[0]))
                            else:
                                parProcs.append(self.fConfig.GetProcess(parID))
                            for parProc in parProcs:
                                blocksize += parProc.GetBlockSize()
                    self.fOutputShmSize = blocksize * 3 # factor of three safety margin
                else:
                    self.fOutputShmSize = blocksize = 65536
                if self.fOutputShmSize % 4096:
                    self.fOutputShmSize += 4096 - (self.fOutputShmSize % 4096)
            commands = { -1 : "" }
            nodes = []
            ddls = list(self.fOutputDDLs)
            for ndx in range(len(ddls)):
                ddl = ddls[ndx]
                loc = self.fDDLLocations[ddl]
                node = loc["node"]
                rorcid = loc["rorcid"]
                channel = loc["channel"]
                commands[ndx] = (" -device %d -channel %d -ddlid %d -sleep -sleeptime 100 -eventdonepollwarning 10000000 -repeateventdonepollwarning" % ( rorcid, channel, ddl ))+" "+self.fOutputWriterOptions
                if ndx==0:
                    commands[ndx] = commands[ndx]+" -broadcasteventmaster"
                #print "HOWS",ndx,"(",type(ndx),"):",commands[ndx]
                nodes.append( node )
            howsproc = {
                "id" : "HOWS",
                "type" : "proc",
                "proctype" : "snk",
                "command" : {-1 : "CRORCOutWriterSubscriber" },
                "groupopts" : commands,
                "parents" : ["HOF"],
                "mult" : 1, #len(ddls),
                "shm" : {"blocksize" : self.fOutputShmSize, "blockcount" : 1},
                "shmtype" : self.fOutputShmType,
                #"nodes" : nodes,
                "ddllist" : Detector.NameToDDLList( "ALICE" ),
                "processorgroup" : "process",
                "groupid" : outputGroupID,
                }
            procs["HOWS"] = howsproc
            # self.MakeProcess( howsproc, self.fProcesses.keys(), detID="ALICE" )

        nodes=[]
        ddls = list(self.fOutputDDLs)
        for ndx in range(len(ddls)):
            ddl = ddls[ndx]
            loc = self.fDDLLocations[ddl]
            node = loc["node"]
            nodes.append( node )

        groups = {}
        groups[outputGroupID] = { 
            "id" : outputGroupID,
            "processes" : procs,
            "nodes" : nodes,
            "outputprocesses" : [],
            "mult" :  len(ddls),
            "processorgroup" : "group",
            "detectors" : set(["ALICE"]),
            "parents" : list(self.fOutputParents),
            "optparents" : [],
            "weight" : 2.,
            "onyoneparentfromgroup" : [],
            "optonyoneparentfromgroup" : [],
            }
        return groups
            
    def CreateFXSProcesses( self ):
        procs = {}
        if self.fFXSBase==None or len(self.fFXSNodes)<=0 or len(self.fFXSParents)<=0:
            return {}
        if not self.fRetrievalMode:
            parentIDs,parentFilterIDs,parentGroupOutput=self.MakeParentList(self.fFXSParents, self.fProcesses.keys(), detID="ALICE" )
            if len(parentIDs)<=0:
                return {}
        for node in self.fFXSNodes:
            fxsproc = { 
                "id" : "FXS_"+node,
                "type" : "proc",
                "proctype" : "snk",
                "command" : {-1 : "FXSSubscriber -FXSBase "+self.fFXSBase+" -DBName "+self.fFXSDB+" -DBHost localhost -DBUser "+self.fFXSDBUser+" -DBPasswd "+self.fFXSDBPwd },
                "parents" : list(self.fFXSParents),
                "mult" : 1,
                "shm" : {"blocksize" : 4096, "blockcount" : 1},
                "shmtype" : "sysv",
                "nodes" : [node],
                "ddllist" : Detector.NameToDDLList( "ALICE" ),
                "parentfilters" : self.fFXSParentFilters,
                "processorgroup" : "process",
                }
            procs[fxsproc["id"]] = fxsproc
            # self.MakeProcess( fxsproc, self.fProcesses.keys(), detID="ALICE" )
        return procs


    def CreateTriggerProcesses( self, triggerType ):
        procs = {}
        if len(self.fTriggerParents[triggerType])<=0:
            return {}
        if not self.fRetrievalMode:
            parentIDs,parentFilterID,parentGroupOutputs=self.MakeParentList(self.fTriggerParents[triggerType], self.fProcesses.keys(), detID="ALICE" )
            if len(parentIDs)<=0:
                return {}
        triggerproc = { 
            "id" : "Global"+self.fTriggerTypeComponentSpecifier[triggerType]+"Trigger",
            "type" : "comp",
            "component": "HLTGlobal"+self.fTriggerTypeComponentSpecifier[triggerType]+"Trigger",
            "parents":list(self.fTriggerParents[triggerType]),
            "mult": 1,
            "shm" : { "blocksize":len(self.fTriggerParents[triggerType])*1024*1024, "blockcount":512 },
            "forcefep" : 0,
            "parentfilters" : self.fTriggerParentFilters[triggerType],
            "awsparameters" : "-forceeventdonedataforward",
            "lowload" : 0,
            "optparents" : set(),
            "optparentfilters" : {},
            "optonlyoneparentfromgroup" : set(),
            "reqparents" : self.fRequiredTriggerContributors[triggerType],
            "library" : "libAliHLTTrigger.so",
            "processorgroup" : "process",
            }
        if self.fTriggerMultiplicities[triggerType]!=None:
            triggerproc["mult"] = self.fTriggerMultiplicities[triggerType]
        if len(self.fTriggerNodes[triggerType])>0:
            triggerproc["nodes"] = self.fTriggerNodes[triggerType]
        procs[triggerproc["id"]] = triggerproc
        # self.MakeProcess( triggerproc, self.fProcesses.keys(), detID="ALICE" )

        if triggerType==self.fOutputTriggerType:
            self.fOutputParents.add( triggerproc["id"] )
        return procs


            
    def CreateNodeGroups( self ):
        nodeObjs = self.fConfig.GetNodes()
        nodes=[]
        for n in nodeObjs:
            nodes.append(n.GetID())
        ###  Workaround
        ### if len(nodes)<=1:
            ### return
        #print "self.fNodeGroups.keys():",self.fNodeGroups.keys()
        empty_groups=set()
        for ng in self.fNodeGroups.keys():
            stack=[ ng ]
            totnodes=set()
            while len(stack)>0:
                #print "stack:",ng,self.fNodeGroups[ng],stack
                totnodes |= set(self.fResMan.GetNodesForGroup(stack[0]))
                stack = stack[1:]+list(self.fNodeGroups[stack[0]])
            if len(totnodes)<=0:
                empty_groups.add(ng)

        for eg in empty_groups:
            del self.fNodeGroups[eg]
        for eg in empty_groups:
            for ng in self.fNodeGroups.keys():
                if eg in self.fNodeGroups[ng]:
                    self.fNodeGroups[ng].remove(eg)
                    
        for ng in self.fNodeGroups.keys():
            gnodes = self.fResMan.GetNodesForGroup(ng)
            gmnodes = gnodes[:]
            subGroups = list(self.fNodeGroups[ng])
            while len(subGroups)>0:
                # print "subGroups:",subGroups,gmnodes,gnodes
                gmnodes += self.fResMan.GetNodesForGroup(subGroups[0])
                subGroups = subGroups[1:]+list(self.fNodeGroups[subGroups[0]])
            # print "subGroups:",subGroups,gmnodes,gnodes
            # print ng,gmnodes,gnodes
            #print ng,"nodes:",set(gmnodes),set(nodes),gmnodes
            #print ng,"nodes:",set(gnodes),set(nodes),gnodes
            gmnodes = list( set(gmnodes) & set(nodes) )
            gnodes = list( set(gnodes) & set(nodes) )
            if len(gmnodes)<=0:
                gmnodes = [ nodes[0] ]
                #continue
            # print ng,gmnodes,gnodes
            ngo = SimpleChainConfig1.NodeGroup( ng, gmnodes[0] )
            for m in list(self.fNodeGroups[ng])+gnodes:
                ngo.Add( m )
            self.fConfig.AddNodeGroup( ngo )

    def CreateNodeTDSs(self):
        return
        #nodes = fConfig.GetNodes()
        #for node in nodes:
        #    np = self.fConfig.GetNodeProcesses(node.GetID())

    def AssignNodes( self ):
        procIDs = self.fConfig.GetProcessGroupIDList()
        for id in procIDs:
            nrq = self.fProcessNodeRequestData[id]
            group = self.fConfig.GetProcessGroup( id )
            nodes = group.GetNodes()
            if nodes.count(None)>0:
                cnt = len(nodes)
                newNodes = []
                for i in range(cnt):
                    if nodes[i]!=None:
                        newNodes.append(nodes[i])
                newNodes = self.fResMan.RequestNodes( nrq["mult"], id, nrq["nodeGroup"], nrq["parentIDCnt"], nrq["ddlID"], nrq["level1Element"], nrq["detID"], newNodes, nrq["lowLoad"], nrq["forceFEP"], placeExistingNodes=False, instanceWeight=nrq["groupweight"] )
                group.SetNodes( newNodes )
                for n in newNodes:
                    if self.fConfig.GetNode(n)==None:
                        self.fConfig.AddNode( SimpleChainConfig1.Node( n, self.GetNodeHostname(n), self.GetNodePlatform(n) ) )
                groupProcs = [ p for p in self.fConfig.GetProcesses() if p.IsInProcessGroup(id) ]
                for p in groupProcs:
                    p.SetNodes( [ newNodes[ p.GetProcessGroupNodeNdx() ] ]*len(p.GetNodes()) )

        procIDs = self.fConfig.GetProcessIDs()
        for id in procIDs:
            if self.fConfig.GetProcess(id).GetType()=="src":
                continue
            proc = self.fConfig.GetProcess( id )
            if proc.IsInProcessGroup():
                continue
            nrq = self.fProcessNodeRequestData[id]
            nodes = proc.GetNodes()
            # print id,":",nodes
            if nodes.count(None)>0:
                cnt = len(nodes)
                newNodes = []
                for i in range(cnt):
                    if nodes[i]!=None:
                        newNodes.append(nodes[i])
                newNodes = self.fResMan.RequestNodes( nrq["mult"], id, nrq["nodeGroup"], nrq["parentIDCnt"], nrq["ddlID"], nrq["level1Element"], nrq["detID"], newNodes, nrq["lowLoad"], nrq["forceFEP"], placeExistingNodes=False )
                proc.SetNodes( newNodes )
                for n in newNodes:
                    if self.fConfig.GetNode(n)==None:
                        self.fConfig.AddNode( SimpleChainConfig1.Node( n, self.GetNodeHostname(n), self.GetNodePlatform(n) ) )
        # print "AssignNodes done:"
        # procIDs = self.fConfig.GetProcessIDs()
        # for id in procIDs:
        #     proc = self.fConfig.GetProcess( id )
        #     nodes = proc.GetNodes()
        #     print id,"id:",nodes
        
                    

    def LoadBalance( self ):
        # XXXXX
        return
        # XXXXX
        #maxIterations = 1000
        #nodes = self.fResMan.GetNodes()
        #nodeIDs = nodes.keys()
        #nodeIDs.sort( key=lambda x: nodes[x].GetUtilization() )
        #iterations=0
        #while (nodes[nodeIDs[0]].GetUtilization()-nodes[nodeIDs[-1]].GetUtilization())>1 and iterations<maxIterations:

        #    diff = nodes[nodeIDs[0]].GetUtilization()-nodes[nodeIDs[-1]].GetUtilization()
        #    
        #    iterations += 1
        #    nodeIDs.sort( key=lambda x: nodes[x].GetUtilization() )
        #pass
        

    def ParseList( self, listString, numbers=False, StringLookup=lambda x: x, applyStringLookupToRangeElements=False ):
        """Parse a list string consisting of elements separated by , (separator) and - (range)
        Returns a set with all unique elements of the specified list string."""
        elements = listString.split( "," )
        if numbers:
            makeelement=lambda x: StringLookup(x)
        else:
            makeelement=lambda x: set([x])
        listSet=set()
        for el in elements:
            ranges=el.split("-")
            if len(ranges)<0 or len(ranges)>2:
                raise ConfigException( "Cannot parse list string '"+listString+"' (only ',' or '-' allowed to separate list elements)" )
            if len(ranges)==1:
                # print "ParseList:",ranges[0]
                listSet |= makeelement(ranges[0])
                # print "ParseList set:",listSet
            elif numbers:
                if applyStringLookupToRangeElements:
                    # print "ParseList 0:",ranges[0],makeelement(ranges[0])
                    # print "ParseList 1:",ranges[1],makeelement(ranges[1])
                    ranges0 = list(makeelement(ranges[0]))
                    ranges1 = list(makeelement(ranges[1]))
                    rangeCnt=len(ranges0)
                    if rangeCnt != len(ranges1):
                        raise ConfigException( "Range lists have different number of elements..." )
                else:
                    ranges0 = [int(ranges[0])]
                    ranges1 = [int(ranges[1])]
                    rangeCnt = 1
                for n in range(rangeCnt):
                    r0 = ranges0[n]
                    r1 = ranges1[n]+1
                    for i in range(r0,r1):
                        listSet.add( i )
            else:
                listSet |= makeelement(ranges[0])
                listSet |= makeelement(ranges[1])
        # print listString,"->",listSet
        return listSet

    def MakeListString( self, elementListInput ):
        if len(elementListInput)<=0:
            return ""
        elementList = list(elementListInput)[:]
        elementList.sort()
        ranges = []
        ranges.append( [ elementList[0] ] )
        lastElement = elementList[0]
        for el in elementList[1:]:
            if lastElement+1==el:
                lastElement = el
                continue
            ranges[-1].append( lastElement )
            ranges.append( [ el ] )
            lastElement = el
        ranges[-1].append( lastElement )
        if ranges[0][0]!=ranges[0][1]:
            outStr = "%u-%u" % ( ranges[0][0], ranges[0][1] )
        else:
            outStr = "%u" % ( ranges[0][0] )
        for r in ranges[1:]:
            if r[0]!=r[1]:
                outStr += ",%u-%u" % ( r[0], r[1] )
            else:
                outStr += ",%u" % ( r[0] )
        return outStr
                             
    def MakeDDLSource( self, ddlID ):
        #print "MakeDDLSource:",ddlID
        if self.fRetrievalMode:
            return
        if not ddlID in self.fDDLs:
            return
        if not ddlID in self.fDDLLocations.keys():
            raise ConfigException( "Location of DDL with ID %d not known" % ddlID )
        detID=Detector.GetDetectorID(ddlID)
        loc = self.fDDLLocations[ddlID]
        node = loc["node"]
        rorcid = loc["rorcid"]
        channel = loc["channel"]
        activateKeys = { "DDL" : "enable", "HWCoProc" : "hwcoproc" }
        generate = {}
        for dataSourceType in ( "DDL", "HWCoProc" ):
            if self.fRORCs.has_key( "%d.%s" % ( ddlID, activateKeys[dataSourceType] ) ):
                generate[dataSourceType] = self.fRORCs["%d.%s" % ( ddlID, activateKeys[dataSourceType] )]
            else:
                generate[dataSourceType] = self.fRORCs["GENERIC.%s" % ( activateKeys[dataSourceType] ) ]
            
        #generate = { "DDL" : True, "HWCoProc" : False }
        #if self.fRORCs.has_key( "%d.hwcoproc" % ddlID ):
        #    generate["HWCoProc"] = self.fRORCs["%d.hwcoproc" % ddlID]
        #else:
        #    generate["HWCoProc"] = self.fRORCs["GENERIC.hwcoproc"]
        blockSizeKeys = { "DDL" : "blocksize", "HWCoProc" : "hwcoprocblocksize" }
        blockSizeFallbackKeys = { "HWCoProc" : "blocksize" }
        blockCountKeys = { "DDL" : "blockcount", "HWCoProc" : "hwcoprocblockcount" }
        blockCountFallbackKeys = { "HWCoProc" : "blockcount" }
        addID = { "DDL" : "", "HWCoProc" : "_HWCoProc" }
        shmTypes = { "ddl" : "librorc", "file" : "sysv" }
        baseCommands = { "ddl" : "CRORCPublisher -device %d -channel %d ",
                         "file" : "FilePublisher -diskpublisher " }
        commands = { "ddl" : { "DDL" : baseCommands["ddl"] % ( rorcid,channel ),
                               "HWCoProc" : baseCommands["ddl"] % ( rorcid,channel ) },
                     "file" : { "DDL" : baseCommands["file"],
                                "HWCoProc" : baseCommands["file"] } }
        optKeys = { "ddl" : { "DDL" : "options", "HWCoProc" : "hwcoprocrorcoptions" },
                    "file" : { "DDL" : "fileoptions", "HWCoProc" : "hwcoprocfileoptions" } }
        optFallbackKeys = { "ddl" : { "HWCoProc" : "options" },
                    "file" : { "HWCoProc" : "fileoptions" } }
        sourceFileDirs = { "DDL" : self.fSourceFileDirs, "HWCoProc" : self.fSourceHWCoProcDirs }
        addOpts = { "DDL" : "", "HWCoProc" : "-hwcoproc" }

        #### if self.fConfigEntries.has_key("NoStrictSOR"):
        ####     if self.fConfigEntries["NoStrictSOR"][-1]:
        ####         for aok in addOpts.keys():
        ####             addOpts[aok] += " -nostrictsor"

        #addDataType = { "DDL" : self.fExplicitSourceOptions, "HWCoProc" : self.fExplicitSourceOptions or generate["HWCoProc"] }
        explicitDataTypeKeys = { "DDL" : "datatype", "HWCoProc" : "hwcoprocdatatype" }
        explicitDataTypes = { "DDL" : "DDL_RAW", "HWCoProc" : Detector.GetDetectorHWCoProcDataTypes(ddlID) }
        explicitDataTypeOpts = { "DDL" : "-datatype", "HWCoProc" : "-hwcoprocdatatype" }
        for dataSourceType in ( "DDL", "HWCoProc" ):
            if not generate[dataSourceType]:
                continue
            blockSize = 0
            if self.fRORCs.has_key( "%d.%s" % ( ddlID, blockSizeKeys[dataSourceType] ) ):
                blockSize = self.fRORCs["%d.%s" % ( ddlID, blockSizeKeys[dataSourceType] )]
            if blockSize<=0:
                blockSize = self.fRORCs["GENERIC.%s" % blockSizeKeys[dataSourceType]]
            if blockSize<=0 and blockSizeFallbackKeys.has_key(dataSourceType):
                if self.fRORCs.has_key( "%d.%s" % ( ddlID, blockSizeFallbackKeys[dataSourceType] ) ):
                    blockSize = self.fRORCs["%d.%s" % ( ddlID, blockSizeFallbackKeys[dataSourceType] )]
                if blockSize<=0:
                    blockSize = self.fRORCs["GENERIC.%s" % blockSizeFallbackKeys[dataSourceType]]
            blockCount = 0
            if self.fRORCs.has_key( "%d.%s" % ( ddlID, blockCountKeys[dataSourceType] ) ):
                blockCount = self.fRORCs["%d.%s" % ( ddlID, blockCountKeys[dataSourceType] )]
            if blockCount<=0:
                blockCount = self.fRORCs["GENERIC.%s" % blockCountKeys[dataSourceType]]
            if blockCount<=0 and blockCountFallbackKeys.has_key(dataSourceType):
                if self.fRORCs.has_key( "%d.%s" % ( ddlID, blockCountFallbackKeys[dataSourceType] ) ):
                    blockCount = self.fRORCs["%d.%s" % ( ddlID, blockCountFallbackKeys[dataSourceType] )]
                if blockCount<=0:
                    blockCount = self.fRORCs["GENERIC.%s" % blockCountFallbackKeys[dataSourceType]]
                    
            ID=self.MakeSourceID( ddlID, dataSourceType )
            opt = None
            if self.fRORCs.has_key( "%d.%s" % ( ddlID, optKeys[self.fSourceType][dataSourceType] ) ):
                opt = self.fRORCs["%d.%s" % ( ddlID,optKeys[self.fSourceType][dataSourceType] ) ]
            if opt==None:
                opt = self.fRORCs["GENERIC.%s" % optKeys[self.fSourceType][dataSourceType]]
            if opt==None and optFallbackKeys[self.fSourceType].has_key(dataSourceType):
                if self.fRORCs.has_key( "%d.%s" % ( ddlID, optFallbackKeys[self.fSourceType][dataSourceType] ) ):
                    opt = self.fRORCs["%d.%s" % ( ddlID, optFallbackKeys[self.fSourceType][dataSourceType] )]
                if opt==None:
                    opt = self.fRORCs["GENERIC.%s" % ( optFallbackKeys[self.fSourceType][dataSourceType] )]
            if opt==None:
                opt = ""
                
            command = commands[self.fSourceType][dataSourceType]+opt+" "
            shmType = shmTypes[self.fSourceType]
            if self.fSourceType=="file":
                for d in sourceFileDirs[dataSourceType]:
                    command += "-datafilelist "+d+"/"+Detector.GetLongDetectorID(ddlID)+"_%d.ddl " % ddlID
            command += addOpts[dataSourceType]

            if self.fExplicitSourceOptions or (dataSourceType=="HWCoProc" and (self.fRORCs.has_key( "%d.%s" % ( ddlID, explicitDataTypeKeys[dataSourceType] ) ) or self.fRORCs.has_key( "GENERIC.%s" % ( explicitDataTypeKeys[dataSourceType] ) ) or Detector.HasSpecificDetectorHWCoProcDataTypes(ddlID)) ):
                dataType=None
                if self.fRORCs.has_key( "%d.%s" % ( ddlID, explicitDataTypeKeys[dataSourceType] ) ):
                    dataType = self.fRORCs["%d.%s" % ( ddlID,explicitDataTypeKeys[dataSourceType] ) ]
                if dataType==None and self.fRORCs.has_key( "GENERIC.%s" % explicitDataTypeKeys[dataSourceType] ):
                    dataType = self.fRORCs["GENERIC.%s" % explicitDataTypeKeys[dataSourceType]]
                if dataType==None:
                    dataType = explicitDataTypes[dataSourceType]
                command += " "+explicitDataTypeOpts[dataSourceType]+" "+dataType
            if self.fExplicitSourceOptions:
                command += " -dataorigin "+Detector.GetDetectorID(ddlID)+" -dataspec "+Detector.GetDataSpec(ddlID)
            else:
                command += " -alicehlt -ddlid "+("%d" % ddlID)
            if self.fMaxEventAge != None:
                command += " -maxeventage %d" % self.fMaxEventAge
            nodeGroup=None
            if 0 and self.fNodeHierarchy:
                nodeHierarchy = []
                if ddlID!=None:
                    det=Detector.GetDetectorID(ddlID)
                    nodeHierarchy.append(det)
                    if Detector.GetSubDetectorID(ddlID=ddlID)!=None:
                        nodeHierarchy.append(det+"_"+Detector.GetSubDetectorID(ddlID=ddlID))
                    ssdid = Detector.GetSubSubDetectorID(ddlID)
                    if self.fDetailedNodeHierarchy and ssdid!=None:
                        if Detector.GetSubDetectorID(ddlID=ddlID)!=None:
                            nodeHierarchy.append(det+"_"+Detector.GetSubDetectorID(ddlID=ddlID)+"_"+ssdid)
                        else:
                            nodeHierarchy.append(det+"_"+ssdid)
                #elif slice!=None:
                #    nodeHierarchy.append("TPC")
                #    nodeHierarchy.append("TPC_"+Detector.GetSubDetectorID(slice=slice))
                #elif detID=="DIMU" and subDetID!=None:
                #    nodeHierarchy.append("DIMU")
                #    nodeHierarchy.append("DIMU"+subDetID)
                #else:
                #    nodeHierarchy.append(detID)
                nodeGroup = nodeHierarchy[-1]
            cmd = command
            command = self.ReplaceVariables( cmd, None, ddlID, { "detector" : [detID] } )
            self.fResMan.PlaceProcess( ID, [node], nodeGroup, 0, lowLoad=True )
            self.fConfig.AddProcess( SimpleChainConfig1.Process( ID, "src", { -1 : command }, "", [], [node], shmType, blockSize, blockCount, 0, False, {}, None, None , "ABCD") )

    def GetActualParentList( self, process ):
        parents = process["parents"][:]
        procs = self.fRequiredContributors|self.fAllContributors
        if process.has_key("detectors"):
            dets = process["detectors"]
        else:
            dets = set()
        if process.has_key("optparents"):
            # The following was:
            # optAddedParents = (self.fRequiredContributors|self.fAllContributors) & set(process["optparents"])
            # But this could not take into account automatic detector parents
            optAddedParents = set()
            for opp in set(process["optparents"]):
                found=False
                if opp in procs:
                    found=True
                elif opp.find("/")==-1:
                    for det in dets:
                        if det+"/"+opp in procs:
                            found=True
                            break
                if found:
                    optAddedParents.add( opp )
            parents += list(optAddedParents)
        else:
            optAddedParents = set()
        return parents,optAddedParents

    def CanTraceToSource( self, firstParentList, parentProcessLists, ddlID=None, detID=None, subDetID=None, level1Element=None, myNode=None, myNodeIndex=None, groupProcIDs=None, processGroup=None, processGroupDict=None ):
        # print "CanTraceToSource start",firstParentList, parentProcessLists, ddlID, detID, subDetID, level1Element, myNode, myNodeIndex, groupProcIDs, processGroup, processGroupDict
        # debugPrint = False
        # if process["id"]=="TR" and detID=="TPC":
        #     debugPrint = True
        # print "CanTraceToSource: ",firstParentList,detID,ddlID,subDetID,level1Element
        # parentList = set(self.GetActualParentList(process)[0])
        parentList = set(firstParentList)
        iteration=0
        level1ElementCache = level1Element
        detIDCache = detID
        while len(parentList)>0:
            # print "CanTraceToSource parent list:",parentList,"(",iteration,")"
            iteration += 1
            # if "DDL" in [ x.split("/")[-1] for x in parentList ]:
            #     print "CanTraceToSource YES"
            #     return True
            pl = set(list(parentList)[:])
            parentList = set()
            for ext_parent in pl:
                level1Element = level1ElementCache
                detID = detIDCache
                parent=ext_parent.split('::')[0]
                par = parent.split("/")
                if len(par) >= 2 and par[-2] == "DDL":
                    level1Element = int(par[-1])
                    if len(par) >= 3:
                        detID = par[0]
                    par.pop()
                if processGroupDict!=None and parent in processGroupDict["processes"].keys():
                    parentList |= set(processGroupDict["processes"][parent]["parents"])
                    continue
                if len(par)==1:
                    ppls = parentProcessLists[:]
                else:
                    ppls = []
                    tmp_ppl = "/".join( par[:-1] )
                    for ppl in parentProcessLists:
                        if ppl[:len(tmp_ppl)]==tmp_ppl:
                            ppls.append( ppl )
                par = par[-1]
                # print "CanTraceToSource ppls:",ppls,"(",iteration,")"
                for ppl in ppls:
                    # print ppl,self.fProcessList.has_key( ppl )
                    pplLevel1Element = False
                    if ppl.split("/")[0] in self.f3LevelDetectors and ppl==ppl.split("/")[0]+"/"+Detector.Get3LevelDetectorLevel1ElementName(ppl.split("/")[0]):
                        pplLevel1Element = True
                    if self.fProcessList.has_key( ppl ):
                        if ppl.split("/")[-1]=="Partition":
                            # print par,ppl,ddlID,detID,ppl.split("/")[0]
                            if ddlID==None:
                                if len(ppl.split("/"))>0:
                                    ddls = Detector.NameToDDLList( ppl.split("/")[0] )
                                # elif detID!=None:
                                #     ddls = Detector.NameToDDLList( detID )
                                else:
                                    ddls = Detector.NameToDDLList( "ALICE" )
                                if detID!=None and detID in self.f3LevelDetectors:
                                    ddls &= Detector.Get3LevelDetectorDDLList( detID, level1Element )
                                elif detID=="DIMU":
                                    ddls &= Detector.GetDimuonDDLList( subDetID )
                                elif detID!=None:
                                    ddls &= Detector.NameToDDLList( detID )
                            else:
                                ddls = set([ddlID])
                            ddls &= self.fDDLs
                            # print "DDLs:",ddls,"(",ddlID,")",detID,self.fDDLs
                            if par=="HWCoProc":
                                for ddl in ddls:
                                    if self.fRORCs["GENERIC.hwcoproc"] or (self.fRORCs.has_key("%d.hwcoproc" % ddl) and self.fRORCs["%d.hwcoproc" % ddl]):
                                        # print "CanTraceToSource YES (HWCoProc)",firstParentList, parentProcessLists, ddlID, detID, subDetID, level1Element, myNode, myNodeIndex, groupProcIDs, processGroup, processGroupDict
                                        return True
                            if par=="DDL":
                                for ddl in ddls:
                                    if self.fRORCs["GENERIC.enable"] or (self.fRORCs.has_key("%d.enable" % ddl) and self.fRORCs["%d.enable" % ddl]):
                                        # print "CanTraceToSource YES (DDL)",firstParentList, parentProcessLists, ddlID, detID, subDetID, level1Element, myNode, myNodeIndex, groupProcIDs, processGroup, processGroupDict
                                        return True
                            if self.fProcessList[ppl].has_key(par):
                                # print parent,par,ppl,self.fProcessList[ppl][par].keys()
                                for ddlID in set(self.fProcessList[ppl][par].keys()) & ddls:
                                    # print "ADD 1:  ",ppl,ddlID,"part procs:",self.fProcessList[ppl][par][ddlID]
                                    if len(ppl.split("/"))>0:
                                        prefix = ppl.split("/")[0]+"/"
                                    else:
                                        prefix = ""
                                    # parentList |= set(prefix+self.GetActualParentList( self.fProcessList[ppl][par][ddlID] )[0])
                                    # print [ prefix+X if len(X.split("/"))<=1 else X for X in self.GetActualParentList( self.fProcessList[ppl][par][ddlID] )[0] ]
                                    parentList |= set([ prefix+X if len(X.split("/"))<=1 else X for X in self.GetActualParentList( self.fProcessList[ppl][par][ddlID] )[0] ] )

                        elif pplLevel1Element:
                            if level1Element==None:
                                level1Elements = set(range(0,Detector.Get3LevelDetectorLevel1ElementCount(ppl.split("/")[0])))
                            else:
                                level1Elements = set([level1Element])
                            # print "TPC slices:",slices
                            if self.fProcessList[ppl].has_key(par):
                                for subID in set(self.fProcessList[ppl][par].keys()) & level1Elements:
                                    # print "ADD 2: ",ppl,subID,"slice procs:",self.fProcessList[ppl][par][subID]
                                    if len(ppl.split("/"))>0:
                                        prefix = ppl.split("/")[0]+"/"
                                    else:
                                        prefix = ""
                                    parentList |= set([ prefix+X if len(X.split("/"))<=1 else X for X in self.GetActualParentList( self.fProcessList[ppl][par][subID] )[0] ] )

                        #elif ppl=="TPC/Slice":
                        #    if level1Element==None:
                        #        level1Elements = set(range(0,36))
                        #    else:
                        #        level1Elements = set([level1Element])
                        #    # print "TPC slices:",slices
                        #    if self.fProcessList[ppl].has_key(par):
                        #        for subID in set(self.fProcessList[ppl][par].keys()) & level1Elements:
                        #            # print "ADD 2: ",ppl,subID,"slice procs:",self.fProcessList[ppl][par][subID]
                        #            if len(ppl.split("/"))>0:
                        #                prefix = ppl.split("/")[0]+"/"
                        #            else:
                        #                prefix = ""
                        #            parentList |= set([ prefix+X if len(X.split("/"))<=1 else X for X in self.GetActualParentList( self.fProcessList[ppl][par][subID] )[0] ] )
                        #elif ppl=="PHOS/Module":
                        #    if level1Element==None:
                        #        level1Elements = set(range(0,5))
                        #    else:
                        #        level1Elements = set([level1Element])
                        #    if self.fProcessList[ppl].has_key(par):
                        #        for modID in set(self.fProcessList[ppl][par].keys()) & level1Elements:
                        #            if len(ppl.split("/"))>0:
                        #                prefix = ppl.split("/")[0]+"/"
                        #            else:
                        #                prefix = ""
                        #            parentList |= set([ prefix+X if len(X.split("/"))<=1 else X for X in self.GetActualParentList( self.fProcessList[ppl][par][modID])[0] ] )
                        #elif ppl=="EMCAL/Module":
                        #    if level1Element==None:
                        #        level1Elements = set(range(0,12))
                        #    else:
                        #        level1Elements = set([level1Element])
                        #    if self.fProcessList[ppl].has_key(par):
                        #        for modID in set(self.fProcessList[ppl][par].keys()) & level1Elements:
                        #            if len(ppl.split("/"))>0:
                        #                prefix = ppl.split("/")[0]+"/"
                        #            else:
                        #                prefix = ""
                        #            parentList |= set([ prefix+X if len(X.split("/"))<=1 else X for X in self.GetActualParentList( self.fProcessList[ppl][par][modID])[0] ] )
                        else:
                            # print "XXX:",par,ppl,self.fProcessList[ppl]
                            if self.fProcessList[ppl].has_key(par):
                                # print "ADD 3:  ",ppl,"procs:",self.fProcessList[ppl][par]
                                if len(ppl.split("/"))>0:
                                    prefix = ppl.split("/")[0]+"/"
                                else:
                                    prefix = ""
                                # parentList |= set(self.GetActualParentList( self.fProcessList[ppl][par] )[0] )
                                parentList |= set([ prefix+X if len(X.split("/"))<=1 else X for X in self.GetActualParentList( self.fProcessList[ppl][par] )[0] ] )
                                # print "CanTraceToSource parent list end loop:",parentList,"(",iteration,")",pl
        # print "CanTraceToSource NO",firstParentList, parentProcessLists, ddlID, detID, subDetID, level1Element, myNode, myNodeIndex, groupProcIDs, processGroup, processGroupDict
        return False
    

    def GetMissingParentsForComponent( self, process, parentProcessLists, ddlID=None, detID=None, subDetID=None, level1Element=None ):
        debugPrint = False
        # debugPrint = True
        if debugPrint:
            print "GetMissingParentsForComponent:",detID,process["id"],process

        parents = self.GetActualParentList( process )[0]
        if debugPrint:
            print "parents:",parents
        missing = set()

        parentList = set(self.GetActualParentList(process)[0])
        # print "parentList:",parentList
        # print parentList,parents
        iteration=0
        # print parentProcessLists
        for parent_ext in parents:
            parent=parent_ext.split('::')[0]
            # print "searching",parent
            # if parent=="DDL" or parent.split("/")[-1]=="DDL":
            #     continue
            if len(parent.split("/"))==1:
                ppls = parentProcessLists[:]
            else:
                # ppls = list( set(parentProcessLists) & set( ["/".join( parent.split("/")[:-1] ) ] ) )
                tmp = "/".join( parent.split("/")[:-1] )
                ppls =  []
                for p in set(parentProcessLists):
                    if p[:len(tmp)]==tmp:
                        ppls.append( p )
            # print ppls,set(parentProcessLists),set( [ "/".join( parent.split("/")[:-1] ) ] )
            par = parent.split("/")[-1]
            found = False
            for ppl in ppls:
                # print parent,par,ppl,self.fProcessList.has_key( ppl )
                pplLevel1Element = False
                if ppl.split("/")[0] in self.f3LevelDetectors and ppl==ppl.split("/")[0]+"/"+Detector.Get3LevelDetectorLevel1ElementName(ppl.split("/")[0]):
                    pplLevel1Element = True
                if self.fProcessList.has_key( ppl ):
                    if ppl.split("/")[-1]=="Partition":
                        if ddlID==None:
                            if len(ppl.split("/"))>0:
                                ddls = Detector.NameToDDLList( ppl.split("/")[0] )
                            # elif detID!=None:
                            #     ddls = Detector.NameToDDLList( detID )
                            else:
                                ddls = Detector.NameToDDLList( "ALICE" )
                            if detID!=None and detID in self.f3LevelDetectors:
                                ddls &= Detector.Get3LevelDetectorDDLList( detID, level1Element )
                            elif detID=="DIMU":
                                ddls &= Detector.GetDimuonDDLList( subDetID )
                            elif detID!=None:
                                ddls &= Detector.NameToDDLList( detID )
                        else:
                            ddls = set([ddlID])
                        ddls &= self.fDDLs
                        # if ddlID==None:
                        #     if detID!=None:
                        #         ddls = Detector.NameToDDLList( detID ) & self.fDDLs
                        #     else:
                        #         ddls = Detector.NameToDDLList( "ALICE" ) & self.fDDLs
                        # else:
                        #     ddls = set([ddlID]) & self.fDDLs
                        # print "TPC DDLs:",ddls,"(",ddlID,")",detID,self.fDDLs
                        if par=="HWCoProc":
                            for ddl in ddls:
                                if self.fRORCs["GENERIC.hwcoproc"] or (self.fRORCs.has_key("%d.hwcoproc" % ddl) and self.fRORCs["%d.hwcoproc" % ddl]):
                                    found=True
                                    break
                            if found:
                                break
                        if par=="DDL":
                            for ddl in ddls:
                                if self.fRORCs["GENERIC.enable"] or (self.fRORCs.has_key("%d.enable" % ddl) and self.fRORCs["%d.enable" % ddl]):
                                    found=True
                                    break
                            if found:
                                break
                        if self.fProcessList[ppl].has_key(par):
                            #if process["id"]=="TR" and par=="CF":
                            #    print set(self.fProcessList[ppl][par].keys()), ddls, set(self.fProcessList[ppl][par].keys()) & ddls
                            #    print self.fECSInputList
                            #if  len(set(self.fProcessList[ppl][par].keys()) & ddls)>0:
                            #    found = True
                            #    break
                            found = True
                            break

                    elif pplLevel1Element:
                        if level1Element==None:
                            level1Elements = set(range(0,Detector.Get3LevelDetectorLevel1ElementCount(ppl.split("/")[0])))
                        else:
                            level1Elements = set([level1Element])
                        # print "TPC slices:",slices
                        if self.fProcessList[ppl].has_key(par):
                            if len(set(self.fProcessList[ppl][par].keys()) & level1Elements):
                                found = True
                                break

                    #elif ppl=="TPC/Slice":
                    #    if level1Element==None:
                    #        level1Elements = set(range(0,36))
                    #    else:
                    #        level1Elements = set([level1Element])
                    #    # print "TPC slices:",slices
                    #    if self.fProcessList[ppl].has_key(par):
                    #        if len(set(self.fProcessList[ppl][par].keys()) & level1Elements):
                    #            found = True
                    #            break
                    #elif ppl=="PHOS/Module":
                    #    if level1Element==None:
                    #        level1Elements = set(range(0,5))
                    #    else:
                    #        level1Elements = set([level1Element])
                    #    if self.fProcessList[ppl].has_key(par):
                    #        if len(set(self.fProcessList[ppl][par].keys()) & level1Elements):
                    #            found = True
                    #            break
                    #elif ppl=="EMCAL/Module":
                    #    if level1Element==None:
                    #        level1Elements = set(range(0,12))
                    #    else:
                    #        level1Elements = set([level1Element])
                    #    if self.fProcessList[ppl].has_key(par):
                    #        if len(set(self.fProcessList[ppl][par].keys()) & level1Elements):
                    #            found = True
                    #            break
                    else:
                        if self.fProcessList[ppl].has_key(par):
                            found = True
                            break
            if not found:
                missing.add( parent )

        return missing
    
    def MakeProcess( self, process, parentProcessLists, ddlID=None, detID=None, subDetID=None, level1Element=None, myNode=None, myNodeIndex=None, groupProcIDs=None, processGroup=None, processGroupDict=None ):
        if self.fRetrievalMode:
            # print "return self.fRetrievalMode"
            return None
        # print process
        isGroup = (process["processorgroup"]=="group")
        isProcess = not isGroup
        
        procType="prc"
        ID=self.MakeID( process["id"], ddlID, detID, subDetID, level1Element )
        if processGroup!=None:
            groupPrefix = processGroup.GetProcessPrefix( myNodeIndex )
            # ID = groupPrefix+ID
            ID = groupPrefix+process["id"]

        parents,addParents = self.GetActualParentList( process )
        # print "MakeProcess parents",process["id"],ID,parents

        if len(parents)<=0:
            # print "return len(parents)<=0"
            return None
        if (processGroup == None or myNodeIndex == 0) and not self.CanTraceToSource( self.GetActualParentList(process)[0][:], parentProcessLists, ddlID=ddlID, detID=detID, subDetID=subDetID, level1Element=level1Element, myNode=myNode, myNodeIndex=myNodeIndex, groupProcIDs=groupProcIDs, processGroup=processGroup, processGroupDict=processGroupDict ):
            if not self.fQuiet:
                # print process
                # print process["id"], process["parents"]
                # print parentProcessLists
                procID = ""
                if detID!=None:
                    procID += detID+"/"
                if subDetID!=None:
                    procID += subDetID+"/"
                if level1Element!=None:
                    procID += "%d/" % level1Element
                procID += process["id"]
                if ddlID!=None:
                    procID+= ":(DDL %d)" % ddlID
                print "Warning: Process "+procID+" cannot be traced down to a DDL data source."
            # print "return not self.CanTraceToSource( self.GetActualParentList(process)[0][:], parentProcessLists, ddlID=ddlID, detID=detID, subDetID=subDetID, level1Element=level1Element )"
            return
#        elif False:
#            if not self.fQuiet:
#                # print process
#                # print process["id"], process["parents"]
#                # print parentProcessLists
#                procID = ""
#                if detID!=None:
#                    procID += detID+"/"
#                if subDetID!=None:
#                    procID += subDetID+"/"
#                if level1Element!=None:
#                    procID += "%d/" % level1Element
#                procID += process["id"]
#                if ddlID!=None:
#                    procID+= ":(DDL %d)" % ddlID
#                print "Process "+procID+" CAN be traced down to a DDL data source."
        
        
        if process.has_key("parentfilters"):
            parentFilters = process["parentfilters"]
        else:
            parentFilters = {}
        if process.has_key("optparentfilters") and len(addParents)>0:
            for par in addParents:
                if process["optparentfilters"].has_key(par):
                    parentFilters[par] = process["optparentfilters"][par]
                
        # print "MakeProcess:",ID
        if process.has_key("onlyoneparentfromgroup"):
            onlyOneParentFromGroup = process["onlyoneparentfromgroup"][:]
        else:
            onlyOneParentFromGroup = []
        # print "MakeProcess onlyOneParentFromGroup:",onlyOneParentFromGroup
        if process.has_key("optonlyoneparentfromgroup"):
            onlyOneParentFromGroup += list( addParents & set(process["optonlyoneparentfromgroup"]) )
        # print "MakeProcess onlyOneParentFromGroup:",onlyOneParentFromGroup
        parentIDs,parentFilterIDs,parentGroupOutput=self.MakeParentList(parents, parentProcessLists, ddlID, detID, subDetID, level1Element, parentFilters=parentFilters, onlyOneParentFromGroup=onlyOneParentFromGroup, myNode=myNode, myNodeIndex=myNodeIndex, groupProcIDs=groupProcIDs, processGroup=processGroup, processGroupDict=processGroupDict  )
        # print "MakeProcess:",ID,"parentIDs:",parentIDs,len(parentIDs),process["parents"]



        if len(parentIDs)<=0:
            # print "MakeProcess len(parentIDs)<=0:",process["id"],ID,parentIDs,process["parents"]
            print "Error: Process without parent ID"
            return

        nodes=[]
        if processGroup!=None:
            nodes = [ myNode ] * process["mult"]
        else:
            if process.has_key("nodes"):
                for n in process["nodes"]:
                    nodes.append(n)

        nodeGroup=None
        if 0 and self.fNodeHierarchy:
            nodeHierarchy = []
            if ddlID!=None:
                det=Detector.GetLongDetectorID(ddlID)
                nodeHierarchy.append(det)
                if Detector.GetSubDetectorID(ddlID=ddlID)!=None:
                    nodeHierarchy.append(det+"_"+Detector.GetSubDetectorID(ddlID=ddlID))
                ssdid = Detector.GetSubSubDetectorID(ddlID)
                if self.fDetailedNodeHierarchy and ssdid!=None:
                    if Detector.GetSubDetectorID(ddlID=ddlID)!=None:
                        nodeHierarchy.append(det+"_"+Detector.GetSubDetectorID(ddlID=ddlID)+"_"+ssdid)
                    else:
                        nodeHierarchy.append(det+"_"+ssdid)
            elif detID in self.f3LevelDetectors and level1Element!=None:
                nodeHierarchy.append(detID)
                nodeHierarchy.append(detID+"_"+Detector.GetSubDetectorID(detID=detID, level1Element=level1Element))
            #elif detID=="TPC" and level1Element!=None:
            #    nodeHierarchy.append("TPC")
            #    nodeHierarchy.append("TPC_"+Detector.GetSubDetectorID(detID="TPC", level1Element=level1Element))
            #elif detID=="PHOS" and level1Element!=None:
            #    nodeHierarchy.append("PHOS")
            #    nodeHierarchy.append("PHOS_"+Detector.GetSubDetectorID(detID="PHOS",level1Element=level1Element))
            #elif detID=="EMCAL" and level1Element!=None:
            #    nodeHierarchy.append("EMCAL")
            #    nodeHierarchy.append("EMCAL_"+Detector.GetSubDetectorID(detID="EMCAL",level1Element=level1Element))
            elif detID=="DIMU" and subDetID!=None:
                nodeHierarchy.append("DIMU")
                nodeHierarchy.append("DIMU"+subDetID)
            else:
                nodeHierarchy.append(detID)
            nodeGroup = nodeHierarchy[-1]

                
        forceFEP = (process.has_key("forcefep") and process["forcefep"])
        lowLoad = (isProcess and process["type"]=="dump") or (process.has_key("lowload") and process["lowload"])
        #### print "RequestNodes:",process["id"],nodes
        #### nodes = self.fResMan.RequestNodes( process["mult"], process["id"], nodeGroup, len(parentIDs), ddlID, slice, detID, existingNodeList=nodes, lowLoad=lowLoad, forceFEP=forceFEP )
        #### print "requested nodes:",process["id"],nodes
        self.fResMan.PlaceProcess( process["id"], nodes, nodeGroup, len(parentIDs), lowLoad=lowLoad )
        existingNodes = nodes[:]
        nodes = nodes + [ None for n in range(process["mult"]-len(nodes)) ]
        nodeRequestData = { "mult" : process["mult"], "id" : process["id"], "nodeGroup" : nodeGroup, "parentIDCnt" : len(parentIDs), "ddlID" : ddlID, "level1Element" : level1Element, "detID" : detID, "existingNodeList" : nodes, "lowLoad" : lowLoad, "forceFEP" : forceFEP, "processorgroup" : process["processorgroup"] }
        #print "NodeGroup:",nodeGroup,nodes


        if 0 and len(nodeHierarchy)>0:
            while len(nodeHierarchy)>1:
                if self.fNodeGroups.has_key(nodeHierarchy[0]):
                    self.fNodeGroups[nodeHierarchy[0]].add(nodeHierarchy[1] )
                else:
                    self.fNodeGroups[nodeHierarchy[0]] = set( [nodeHierarchy[1]] )
                nodeHierarchy = nodeHierarchy[1:]
            if not self.fNodeGroups.has_key(nodeHierarchy[0]):
                self.fNodeGroups[nodeHierarchy[0]] = set( [] )
                
        for n in existingNodes:
           if n!=None and self.fConfig.GetNode(n)==None:
               self.fConfig.AddNode( SimpleChainConfig1.Node( n, self.GetNodeHostname(n), self.GetNodePlatform(n) ) )

        if isProcess:
            if process["type"]=="comp":
                command = {-1 : "AliRootWrapperSubscriber "+process["awsparameters"]+" -componentid "+process["component"] }
                if process.has_key( "options" ):
                    command[-1] = command[-1]+" -componentargs \""+process["options"]+"\""
                if process.has_key( "library" ):
                    command[-1] = command[-1]+" -componentlibrary "+process["library"]
                for al in self.fAWSAddLibraries:
                    if self.fAWSAddLibrariesBefore.has_key( al ):
                        command[-1] = command[-1]+" -addlibrarybefore "+al+" "+self.fAWSAddLibrariesBefore[al]
                    else:
                        command[-1] = command[-1]+" -addlibrary "+al
                if process.has_key( "addlibraries" ):
                    for al in process["addlibraries"]:
                        if process["beforelibraries"].has_key(al):
                            command[-1] = command[-1]+" -addlibrarybefore "+al+" "+process["beforelibraries"][al]
                        else:
                            command[-1] = command[-1]+" -addlibrary "+al
                command[-1] += self.fAWSOptions
                procType=process["proctype"]
            elif process["type"]=="proc":
                command = dict(process["command"])
                if "groupopts" in process.keys() and myNodeIndex in process["groupopts"].keys():
                    for k in command.keys():
                        command[k] += process["groupopts"][myNodeIndex]
                procType=process["proctype"]
            elif process["type"]=="dump":
                if process["dumptype"]=="tcp":
                    port = None # self.fDefaultTDSPort
                    if process.has_key("port"):
                        port = process["port"]
                    port = self.GetTDSPort(nodes[0],port)
                    command = { -1 : "TCPDumpSubscriber -port %d" % port }
                    ID=self.MakeID( process["id"]+"_%d" % port, ddlID, detID, level1Element=level1Element )
                    procType="snk"
                else:
                    raise Exceptions.SCC2Exception( "INTERNAL ERROR: Unknown dump type '%s' (ID='%s')" % (process["dumptype"],process["id"]) )
                if process["requestinterval"]!=0 or not process["allevents"]:
                    command[-1] = command[-1]+" -onlymonitorevents -onlyadvancemonitorevents"
                if process["requestinterval"]!=0:
                    command[-1] = command[-1]+" -monitoreventrequestinterval %u" % process["requestinterval"]
            elif process["type"]=="nodedump":
                pass
            else:
                raise Exceptions.SCC2Exception( "INTERNAL ERROR: Unknown process type '%s' (ID='%s')" % (process["type"],process["id"]) )


            ##### IMPORTANT
            # Add any pre-defined macros used here also to the self.fPreDefinedMacros member
            # macros = {}
            # for ev in os.environ.keys():
            #     macros[ev] = os.environ[ev]
            # macros["ID"] = ID
            # if ddlID!=None:
            #     macros["DDL"] = ddlID
            # if process.has_key("detector") and len(process["detector"])==1:
            #     macros["DETECTOR"] = list(process["detector"])[0]
            # for mk in self.fMacros.keys():
            #     macros[mk] = self.fMacros[mk]
            # cmd = {}
            # for k in command.keys():
            #     temp = string.Template( command[k] )
            #     cmd[k] = temp.substitute( macros )

            cmd = {}
            for k in command.keys():
                cmd[k] = self.ReplaceVariables( command[k], ID, ddlID, process )

            shmtype="sysv"
            if process.has_key("shmtype"):
                shmtype = process["shmtype"]

            eventModulo = None
            if process.has_key("eventmodulo"):
                eventModulo = process["eventmodulo"]

            eventTypeCode = None
            if process.has_key("eventtypecode"):
                eventTypeCode = process["eventtypecode"]


            #print "MakeProcess:",ID,"parentIDs:",parentIDs,len(parentIDs)
            if processGroup != None:
                internalID = process["id"]
            else:
                internalID = None
            returnObj = SimpleChainConfig1.Process( ID, procType, cmd, "", parentIDs, nodes, shmtype, process["shm"]["blocksize"], process["shm"]["blockcount"], 0, False, parentFilterIDs, eventModulo, eventTypeCode, internalID, processGroup, myNode, myNodeIndex, parentGroupOutput )
            if process.has_key("mergecount"):
                returnObj.fMergeCount = process["mergecount"]
            if process.has_key("keepscatteredinput"):
                returnObj.fKeepScatteredInput = process["keepscatteredinput"]
            if process["type"]=="comp":
                returnObj.fComponent = 1
            self.fConfig.AddProcess( returnObj )
        self.fProcessNodeRequestData[ID] = nodeRequestData
        if isGroup and processGroup==None:
            groupProcIDs = process["processes"].keys()
            for opID in process["outputprocesses"]:
                if not opID in groupProcIDs:
                    raise SimpleChainConfig1.ChainConfigException("Process "+opID+" specified as output process for group "+ID+" does not exist in group")
            # outputProcs = [ self.MakeID( x, ddlID, detID, subDetID, level1Element ) for x in process["outputprocesses"] ]
            outputProcs = process["outputprocesses"][:]
            groupObj = SimpleChainConfig1.ProcessGroup( ID, nodes, groupProcIDs, outputProcs )

            for procEntry in process["processes"].values():
                ndx = 0
                for node in nodes:
                    procObj = self.MakeProcess( procEntry, parentProcessLists, ddlID=ddlID, detID=detID, subDetID=subDetID, level1Element=level1Element, myNode=node, myNodeIndex=ndx, groupProcIDs=groupProcIDs, processGroup=groupObj, processGroupDict=process )
                    if procObj == None:
                        #This failed for one node, we assume it will fail for all nodes
                        continue 
                    if ndx == 0:
                        #We only have to check this for one node, the other nodes will be the same
                        groupParents = set(groupProcIDs) & set(procObj.GetParents())
                        nonGroupParents = set(procObj.GetParents()) - groupParents
                        if len(groupParents)>0 and len(nonGroupParents)>0:
                            raise SimpleChainConfig1.ChainConfigException("Process "+procEntry["id"]+" of group "+ID+" has mixed non-group and in-group parents. This is currently not supported... (in-group: "+str(groupParents)+" - non-group: "+str(nonGroupParents)+")")
                    ndx += 1

            self.fConfig.AddProcessGroup( groupObj )
            self.fProcessNodeRequestData[ID]["groupweight"] = process["weight"]
            returnObj = groupObj
        return returnObj

    def ReplaceVariables( self, cmd, ID, ddlID, process ):
        ##### IMPORTANT
        # Add any pre-defined macros used here also to the self.fPreDefinedMacros member
        macros = {}
        for ev in os.environ.keys():
            macros[ev] = os.environ[ev]
        if ID!=None:
            macros["ID"] = ID
        if ddlID!=None:
            macros["DDL"] = ddlID
        if process.has_key("detector") and len(process["detector"])==1:
            macros["DETECTOR"] = list(process["detector"])[0]
        for mk in set(macros.keys())-set(os.environ.keys()):
            if not mk in self.fPreDefinedMacros:
                Exceptions.SCC2Exception( "INTERNAL ERROR: Macro "+mk+" not contained in pre-defined macros list. ( macros: "+str(macros.keys())+" - environment: "+str(os.environ.keys())+" - pre defined macros: "+str(self.fPreDefinedMacros)+" )" )
        for mk in self.fMacros.keys():
            macros[mk] = self.fMacros[mk]
        temp = string.Template( cmd )
        return temp.substitute( macros )
        


    def MakeParentList( self, parents, parentProcessLists, ddlID=None, detID=None, subDetID=None, level1Element=None, parentFilters={}, onlyOneParentFromGroup=[], myNode=None, myNodeIndex=None, groupProcIDs=None, processGroup=None, processGroupDict=None ):
        # print "MakeParentList:",parents,parentProcessLists,detID,subDetID,level1Element
        parentIDs = []
        parentFilterIDs = {}
        parentGroupOutput = {}
        level1ElementCache = level1Element
        detIDCache = detID
        subDetIDCache = subDetID
        for ext_p in parents:
            level1Element = level1ElementCache
            detID = detIDCache
            subDetID = subDetIDCache
            p=ext_p.split('::')[0]
            oldParCnt=len(parentIDs)
            # print "   ",p
            found=False
            if processGroupDict!=None and p in processGroupDict["processes"].keys():
                found = True
                # newID = processGroup.GetProcessPrefix( myNodeIndex )+self.MakeID( p, ddlID, detID, subDetID, level1Element )
                newID = processGroup.GetProcessPrefix( myNodeIndex )+p
                parentIDs.append( newID )
                if parentFilters.has_key(p):
                    parentFilterIDs[newID] = parentFilters[p]
                continue
            ps = p.split("/")
            if len(ps) >= 2 and ps[-2] == "DDL":
                level1Element = int(ps[-1])
                if len(ps) >= 3:
                    if len(ps) >= 4:
                        subDetID = ps[-3]
                        detID = ps[-4]
                    else:
                        detID = ps[-3]
                ps.pop()
            if p=="DDL" or p=="HWCoProc" or ps[-1]=="DDL" or ps[-1]=="HWCoProc":
                found=True
                if ddlID!=None:
                    ddlList = [ddlID]
                elif detID in self.f3LevelDetectors and level1Element!=None:
                    ddlList = Detector.Get3LevelDetectorDDLList(detID, level1Element ) & self.fDDLs
                #elif detID=="TPC" and level1Element!=None:
                #    ddlList = Detector.GetTPCDDLList( level1Element ) & self.fDDLs
                #elif detID=="PHOS" and level1Element!=None:
                #    ddlList = Detector.Get3LevelDetectorDDLList(detID, level1Element ) & self.fDDLs
                #elif detID=="EMCAL" and level1Element!=None:
                #    ddlList = Detector.Get3LevelDetectorDDLList(detID, level1Element ) & self.fDDLs
                elif subDetID!=None and detID=="DIMU":
                    ddlList = Detector.GetDimuonDDLList( subDetID, None, level1Element ) & self.fDDLs
                elif len(ps)>1:
                    ddlList = Detector.NameToDDLList( ps[0] ) & self.fDDLs
                elif detID!=None:
                    ddlList = Detector.NameToDDLList( detID ) & self.fDDLs
                else:
                    print "parents:",parents, "(",p,")"
                    raise Exceptions.SCC2Exception( "INTERNAL ERROR: Either ddlID, level1Element, or detID have to be specified" )
                for ddl in ddlList:
                    if p=="HWCoProc" or ps[-1]=="HWCoProc":
                        if not self.fRORCs["GENERIC.hwcoproc"] and not (self.fRORCs.has_key("%d.hwcoproc" % ddl) and self.fRORCs["%d.hwcoproc" % ddl]):
                            continue
                    if p=="DDL" or ps[-1]=="DDL":
                        if not self.fRORCs["GENERIC.enable"] and not (self.fRORCs.has_key("%d.enable" % ddl) and self.fRORCs["%d.enable" % ddl]):
                            continue
                    if len(ps)>1:
                        p = ps[-1]
                    newID = self.MakeSourceID( ddl, p )
                    parentIDs.append( newID )
                    if parentFilters.has_key(p):
                        parentFilterIDs[newID] = parentFilters[p]
                    if p in onlyOneParentFromGroup:
                        break
                continue
            canTraceID = p + '#' + str(ddlID) + '#' + str(detID) + '#' + str(subDetID) + '#' + str(level1Element)
            if not self.fCanTraceCacheA.has_key(canTraceID):
                self.fCanTraceCacheA[canTraceID] = self.CanTraceToSource( [p], parentProcessLists, ddlID=ddlID, detID=detID, subDetID=subDetID, level1Element=level1Element, myNode=myNode, myNodeIndex=myNodeIndex, groupProcIDs=groupProcIDs, processGroup=processGroup, processGroupDict=processGroupDict )

            if not self.fCanTraceCacheA[canTraceID]:
                if not self.fQuiet:
                    # print process
                    # print process["id"], process["parents"]
                    procID = ""
                    if detID!=None:
                        procID += detID+"/"
                    if subDetID!=None:
                        procID += subDetID+"/"
                    if level1Element!=None:
                        procID += "%d/" % level1Element
                    procID += p
                    if ddlID!=None:
                        procID+= ":(DDL %d)" % ddlID
                    print "Warning: Parent process "+procID+" cannot be traced down to a DDL data source."
                continue
            for ppl in parentProcessLists:
                #if (len(ppl.split("/"))>1 or len(p.split("/"))>1) and ppl.split("/")[0]!=p.split("/")[0]:
                # print p, ppl
                if len(p.split("/"))>1 and ppl.split("/")[0]!=p.split("/")[0]:
                    continue
                partCnt=len(p.split("/"))
                if len(ppl.split("/"))<partCnt:
                    partCnt=len(ppl.split("/"))
                partCnt-=1
                do_cont=False
                for l in range(partCnt):
                    if ppl.split("/")[l]!=p.split("/")[l]:
                        do_cont=True
                        break
                if do_cont:
                    continue
                pplLevel1Element = False
                if ppl.split("/")[0] in self.f3LevelDetectors and ppl==ppl.split("/")[0]+"/"+Detector.Get3LevelDetectorLevel1ElementName(ppl.split("/")[0]):
                    pplLevel1Element = True
                # print self.fProcesses[ppl].has_key(p.split("/")[-1]), ppl, p
                if self.fProcesses[ppl].has_key(p.split("/")[-1]):
                    found=True
                    if ddlID!=None and ppl.split("/")[-1]=="Partition":
                        # print "MakeParentList,1"
                        if ddlID in self.fDDLs:
                            newID = self.MakeID( p.split("/")[-1], ddlID, level1Element=None )
                            parentIDs.append( newID )
                            if parentFilters.has_key(p):
                                parentFilterIDs[newID] = parentFilters[p]

                    elif detID in self.f3LevelDetectors and level1Element!=None and ppl==detID+"/"+Detector.Get3LevelDetectorLevel1ElementName(detID)+"/Partition":
                        # print "MakeParentList,2"
                        parentDDLs = self.fProcesses[detID+"/"+Detector.Get3LevelDetectorLevel1ElementName(detID)+"/Partition"][p.split("/")[-1]]
                        for ddl in Detector.Get3LevelDetectorDDLList(detID, level1Element ) & self.fDDLs & parentDDLs:
                            newID = self.MakeID( p.split("/")[-1], ddl, level1Element=None )
                            parentIDs.append( newID )
                            if parentFilters.has_key(p):
                                parentFilterIDs[newID] = parentFilters[p]
                            if p in onlyOneParentFromGroup:
                                break


                    #elif detID=="TPC" and level1Element!=None and ppl=="TPC/Slice/Partition":
                    #    #print "MakeParentList,2"
                    #    parentDDLs = self.fProcesses["TPC/Slice/Partition"][p.split("/")[-1]]
                    #    for ddl in Detector.GetTPCDDLList( level1Element ) & self.fDDLs & parentDDLs:
                    #        newID = self.MakeID( p.split("/")[-1], ddl, level1Element=None )
                    #        parentIDs.append( newID )
                    #        if parentFilters.has_key(p):
                    #            parentFilterIDs[newID] = parentFilters[p]
                    #        if p in onlyOneParentFromGroup:
                    #            break
                    #elif detID=="PHOS" and level1Element!=None and ppl=="PHOS/Module/Partition":
                    #    #print "MakeParentList,2"
                    #    parentDDLs = self.fProcesses["PHOS/Module/Partition"][p.split("/")[-1]]
                    #    for ddl in Detector.Get3LevelDetectorDDLList(detID, level1Element ) & self.fDDLs:
                    #        newID = self.MakeID( p.split("/")[-1], ddl, level1Element=None )
                    #        parentIDs.append( newID )
                    #        if parentFilters.has_key(p):
                    #            parentFilterIDs[newID] = parentFilters[p]
                    #        if p in onlyOneParentFromGroup:
                    #            break
                    #elif detID=="EMCAL" and level1Element!=None and ppl=="EMCAL/Module/Partition":
                    #    #print "MakeParentList,2"
                    #    parentDDLs = self.fProcesses["EMCAL/Module/Partition"][p.split("/")[-1]]
                    #    for ddl in Detector.Get3LevelDetectorDDLList(detID, level1Element ) & self.fDDLs:
                    #        newID = self.MakeID( p.split("/")[-1], ddl, level1Element=None )
                    #        parentIDs.append( newID )
                    #        if parentFilters.has_key(p):
                    #            parentFilterIDs[newID] = parentFilters[p]
                    #        if p in onlyOneParentFromGroup:
                    #            break


                    elif detID in self.f3LevelDetectors and level1Element!=None and ppl==detID+"/"+Detector.Get3LevelDetectorLevel1ElementName(detID):
                        # print "MakeParentList,3"
                        parentLevel1Elements = self.fProcesses[detID+"/"+Detector.Get3LevelDetectorLevel1ElementName(detID)][p.split("/")[-1]]
                        if (len(Detector.Get3LevelDetectorDDLList(detID, level1Element ) & self.fDDLs))>0 and level1Element in parentLevel1Elements:
                            newID = self.MakeID( p.split("/")[-1], detID=detID,level1Element=level1Element )
                            parentIDs.append( newID )
                            if parentFilters.has_key(p):
                                parentFilterIDs[newID] = parentFilters[p]

                    #elif detID=="TPC" and level1Element!=None and ppl=="TPC/Slice":
                    #    #print "MakeParentList,3"
                    #    parentSlices = self.fProcesses["TPC/Slice"][p.split("/")[-1]]
                    #    if (len(Detector.GetTPCDDLList( level1Element ) & self.fDDLs))>0 and level1Element in parentSlices:
                    #        newID = self.MakeID( p.split("/")[-1], detID=detID,level1Element=level1Element )
                    #        parentIDs.append( newID )
                    #        if parentFilters.has_key(p):
                    #            parentFilterIDs[newID] = parentFilters[p]
                    #elif detID=="PHOS" and level1Element!=None and ppl=="PHOS/Module":
                    #    #print "MakeParentList,3"
                    #    parentModules = self.fProcesses["PHOS/Module"][p.split("/")[-1]]
                    #    if (len(Detector.Get3LevelDetectorDDLList(detID, level1Element ) & self.fDDLs))>0 and level1Element in parentModules:
                    #        newID = self.MakeID( p.split("/")[-1], detID=detID, level1Element=level1Element )
                    #        parentIDs.append( newID )
                    #        if parentFilters.has_key(p):
                    #            parentFilterIDs[newID] = parentFilters[p]
                    #elif detID=="EMCAL" and level1Element!=None and ppl=="EMCAL/Module":
                    #    #print "MakeParentList,3"
                    #    parentModules = self.fProcesses["EMCAL/Module"][p.split("/")[-1]]
                    #    if (len(Detector.Get3LevelDetectorDDLList(detID, level1Element ) & self.fDDLs))>0 and level1Element in parentModules:
                    #        newID = self.MakeID( p.split("/")[-1], detID=detID, level1Element=level1Element )
                    #        parentIDs.append( newID )
                    #        if parentFilters.has_key(p):
                    #            parentFilterIDs[newID] = parentFilters[p]
                    elif detID=="DIMU" and subDetID!=None and ppl=="DIMU/"+subDetID+"/Partition":
                        # print "MakeParentList,4"
                        parentDDLs = self.fProcesses[ppl][p.split("/")[-1]]
                        for ddl in (Detector.GetDimuonDDLList( subDetID, ddlID ) & self.fDDLs & parentDDLs):
                            newID = self.MakeID( p.split("/")[-1], detID=detID, subDetID=subDetID, ddlID=ddl )
                            parentIDs.append( newID )
                            if parentFilters.has_key(p):
                                parentFilterIDs[newID] = parentFilters[p]
                            if p in onlyOneParentFromGroup:
                                break
                    elif detID!=None and subDetID!=None and ppl==detID+"/"+subDetID:
                        # print "MakeParentList,5"
                        if len(Detector.NameToDDLList( detID ) & self.fDDLs)>0:
                            newID = self.MakeID( p.split("/")[-1], detID=detID, subDetID=subDetID )
                            parentIDs.append( newID )
                            if parentFilters.has_key(p):
                                parentFilterIDs[newID] = parentFilters[p]
                    elif (detID=="DIMU" or detID=="ALICE") and ppl.split("/")[-1]=="Partition" and (ppl.split("/")[0]==detID or detID=="ALICE") and ppl.split("/")[0]=="DIMU":
                        # print "MakeParentList,6"
                        parentDDLs = self.fProcesses[ppl][p.split("/")[-1]]
                        for ddl in Detector.GetDimuonDDLList( ppl.split("/")[1] ) & self.fDDLs & parentDDLs:
                            newID = self.MakeID( p.split("/")[-1], ddl, level1Element=None )
                            parentIDs.append( newID )
                            if parentFilters.has_key(p):
                                parentFilterIDs[newID] = parentFilters[p]
                            if p in onlyOneParentFromGroup:
                                break
                    elif detID!=None and ppl.split("/")[-1]=="Partition" and (ppl.split("/")[0]==detID or detID=="ALICE"):
                        #print "MakeParentList,7"
                        parentDDLs = self.fProcesses[ppl][p.split("/")[-1]]
                        for ddl in Detector.NameToDDLList( ppl.split("/")[0] ) & self.fDDLs & parentDDLs:
                            newID = self.MakeID( p.split("/")[-1], ddl, level1Element=None )
                            parentIDs.append( newID )
                            if parentFilters.has_key(p):
                                parentFilterIDs[newID] = parentFilters[p]
                            if p in onlyOneParentFromGroup:
                                break
                    elif (detID=="DIMU" or detID=="ALICE") and "/".join( ppl.split("/")[0:2])=="DIMU/TRG":
                        #print "MakeParentList,8a"
                        if len(Detector.GetDimuonDDLList( "TRG" ) & self.fDDLs)>0:
                            newID = self.MakeID( p.split("/")[-1], detID="DIMU", subDetID="TRG" )
                            parentIDs.append( newID )
                            if parentFilters.has_key(p):
                                parentFilterIDs[newID] = parentFilters[p]
                    elif (detID=="DIMU" or detID=="ALICE") and "/".join( ppl.split("/")[0:2])=="DIMU/TRK":
                        #print "MakeParentList,8b"
                        if len(Detector.GetDimuonDDLList( "TRK" ) & self.fDDLs)>0:
                            newID = self.MakeID( p.split("/")[-1], detID="DIMU", subDetID="TRK" )
                            parentIDs.append( newID )
                            if parentFilters.has_key(p):
                                parentFilterIDs[newID] = parentFilters[p]
                    elif (detID=="DIMU" or detID=="ALICE") and ppl.split("/")[0]=="DIMU":
                        #print "MakeParentList,8c"
                        if len(Detector.NameToDDLList( ppl ) & self.fDDLs)>0:
                            newID = self.MakeID( p.split("/")[-1], detID="DIMU", subDetID=subDetID )
                            parentIDs.append( newID )
                            if parentFilters.has_key(p):
                                parentFilterIDs[newID] = parentFilters[p]

                    elif ( (detID in self.f3LevelDetectors and ppl.split("/")[0]==detID) or detID=="ALICE") and pplLevel1Element:
                        # print "MakeParentList,9"
                        #print self.fProcesses[ppl][p.split("/")[-1]]
                        #for sliceID in range(0,36):
                        parentLevel1Elements = self.fProcesses[ppl][p.split("/")[-1]]
                        # print self.fProcesses[ppl][p.split("/")[-1]]
                        for level1ElementID in self.fProcesses[ppl][p.split("/")[-1]] & parentLevel1Elements:

                            myPPL = []
                            if ppl.split("/")[0]=="ALICE" or detID=="ALICE":
                                myPPL += parentProcessLists
                            else:
                                level1ElementName = Detector.Get3LevelDetectorLevel1ElementName( detID )
                                myPPL = [ detID+"/"+level1ElementName+"/Partition", detID+"/"+level1ElementName, detID ]
                            # print "myPPL:",myPPL,"level1ElementID:",level1ElementID,"(",type(level1ElementID),")"
                            canTraceID = p + '#' + str(detID) + '#' + str(subDetID) + '#' + str(level1ElementID)
                            if not self.fCanTraceCacheB.has_key(canTraceID):
                                self.fCanTraceCacheB[canTraceID] = self.CanTraceToSource( [p], myPPL, ddlID=None, detID=detID, subDetID=subDetID, level1Element=level1ElementID, myNode=myNode, myNodeIndex=myNodeIndex, groupProcIDs=groupProcIDs, processGroup=processGroup, processGroupDict=processGroupDict ) # parentProcessLists
                            if not self.fCanTraceCacheB[canTraceID]:
                                # print "TMP",level1ElementID,p,"cannot trace to source"
                                continue

                            # print level1ElementID,p.split("/")[-1],self.fProcessList[ppl][p.split("/")[-1]][level1ElementID]["ddllist"]
                            # print level1ElementID,Detector.Get3LevelDetectorDDLList( ppl.split("/")[0], level1ElementID ), ( Detector.Get3LevelDetectorDDLList( ppl.split("/")[0], level1ElementID ) & self.fDDLs )
                            if len(Detector.Get3LevelDetectorDDLList( ppl.split("/")[0], level1ElementID ) & self.fDDLs):
                                newID = self.MakeID( p.split("/")[-1], ddlID=None, detID=ppl.split("/")[0], level1Element=level1ElementID )
                                parentIDs.append( newID )
                                if parentFilters.has_key(p):
                                    parentFilterIDs[newID] = parentFilters[p]
                            if p in onlyOneParentFromGroup:
                                break

                    #elif (detID=="TPC" or detID=="ALICE") and ppl=="TPC/Slice":
                    #    #print "MakeParentList,9"
                    #    #print self.fProcesses[ppl][p.split("/")[-1]]
                    #    #for sliceID in range(0,36):
                    #    parentSlices = self.fProcesses["TPC/Slice"][p.split("/")[-1]]
                    #    for sliceID in self.fProcesses[ppl][p.split("/")[-1]] & parentSlices:
                    #        if len(Detector.GetTPCDDLList( sliceID ) & self.fDDLs):
                    #            newID = self.MakeID( p.split("/")[-1], ddlID=None, detID="TPC", level1Element=sliceID )
                    #            parentIDs.append( newID )
                    #            if parentFilters.has_key(p):
                    #                parentFilterIDs[newID] = parentFilters[p]
                    #        if p in onlyOneParentFromGroup:
                    #            break
                    #elif (detID=="PHOS" or detID=="ALICE") and ppl=="PHOS/Module":
                    #    #print "MakeParentList,9"
                    #    #print self.fProcesses[ppl][p.split("/")[-1]]
                    #    #for moduleID in range(0,36):
                    #    parentModules = self.fProcesses["PHOS/Module"][p.split("/")[-1]]
                    #    for moduleID in self.fProcesses[ppl][p.split("/")[-1]] & parentModules:
                    #        if len(Detector.Get3LevelDetectorDDLList("PHOS", moduleID ) & self.fDDLs):
                    #            newID = self.MakeID( p.split("/")[-1], ddlID=None, detID="PHOS", level1Element=moduleID )
                    #            parentIDs.append( newID )
                    #            if parentFilters.has_key(p):
                    #                parentFilterIDs[newID] = parentFilters[p]
                    #        if p in onlyOneParentFromGroup:
                    #            break
                    #elif (detID=="EMCAL" or detID=="ALICE") and ppl=="EMCAL/Module":
                    #    #print "MakeParentList,9"
                    #    #print self.fProcesses[ppl][p.split("/")[-1]]
                    #    #for moduleID in range(0,36):
                    #    parentModules = self.fProcesses["EMCAL/Module"][p.split("/")[-1]]
                    #    for moduleID in self.fProcesses[ppl][p.split("/")[-1]] & parentModules:
                    #        if len(Detector.Get3LevelDetectorDDLList("EMCAL", moduleID ) & self.fDDLs):
                    #            newID = self.MakeID( p.split("/")[-1], ddlID=None, detID="EMCAL", level1Element=moduleID )
                    #            parentIDs.append( newID )
                    #            if parentFilters.has_key(p):
                    #                parentFilterIDs[newID] = parentFilters[p]
                    #        if p in onlyOneParentFromGroup:
                    #            break
                    elif detID==ppl or detID=="ALICE":
                        #print "MakeParentList,A"
                        #print p, detID, ppl, Detector.NameToDDLList( ppl ), self.fDDLs, Detector.NameToDDLList( ppl ) & self.fDDLs
                        if len(Detector.NameToDDLList( ppl ) & self.fDDLs)>0:
                            newID = self.MakeID( p.split("/")[-1], ddlID=None, level1Element=None, detID=ppl.split("/")[0] )
                            parentIDs.append( newID )
                            if parentFilters.has_key(p):
                                parentFilterIDs[newID] = parentFilters[p]
                    else:
                        print p,ppl,ddlID,level1Element,detID,parents,parentProcessLists,self.fProcesses
                        raise Exceptions.SCC2Exception( "INTERNAL ERROR: Cannot determine parent process list (1)" )
                    continue
            if not found:
                print p,ppl,ddlID,level1Element,detID,parents,parentProcessLists,self.fProcesses
                raise Exceptions.SCC2Exception( "INTERNAL ERROR: Cannot determine parent process list (2)" )
            if len(ext_p.split('::')) > 1:
                newParCnt=len(parentIDs)
                for i in range(oldParCnt, newParCnt):
                    newID=parentIDs[i]
                    if newID in parentGroupOutput.keys():
                        parentGroupOutput[newID].append(ext_p.split('::')[1])
                    else:
                        parentGroupOutput[newID]=[ext_p.split('::')[1]]
        return parentIDs,parentFilterIDs,parentGroupOutput
                            
    def MakeSourceID( self, ddlID, dataSourceType ):
        baseID = { "ddl" : "RP", "file" : "FP" }
        addID = { "DDL" : "", "HWCoProc" : "_HWCoProc" }
        if not self.fSourceType in baseID:
            raise ConfigException( "FATAL INTERNAL ERROR - Only data sources file or ddl allowed" )
        if not dataSourceType in addID.keys():
            raise ConfigException( "FATAL INTERNAL ERROR - Only data source types DDL or HWCoProc allowed" )
        return self.MakeID( baseID[self.fSourceType], ddlID=ddlID )+addID[dataSourceType]
            

    def MakeID( self, baseID, ddlID=None, detID=None, subDetID=None, level1Element=None ):
        origDetID = detID
        if ddlID==None and level1Element==None and detID==None and subDetID==None:
            raise Exceptions.SCC2Exception( "INTERNAL ERROR: Either ddlID, level1Element, detID, or subDetID have to be specified" )
        if level1Element!=None and detID==None:
            raise Exceptions.SCC2Exception( "INTERNAL ERROR: If level1Element is specified, detID must be specified as well" )
        if ddlID!=None and detID==None:
            detID=Detector.GetLongDetectorID(ddlID)
        elif detID=="DIMU" and ddlID!=None:
            detID = "MUON"+Detector.GetSubDetectorID(ddlID=ddlID)
        elif origDetID=="DIMU" and subDetID!=None:
            detID = "MUON"+subDetID
        #elif origDetID=="DIMU":
        #    detID = "MUON"
        elif detID!=None:
            detID = Detector.DetectorID2LongDetectorID( detID )

        if origDetID=="DIMU":
            subDet = ""
        elif (ddlID!=None or level1Element!=None) and Detector.GetSubDetectorID(ddlID=ddlID,detID=detID,level1Element=level1Element)!=None:
            subDet="_"+Detector.GetSubDetectorID(ddlID=ddlID,detID=detID,level1Element=level1Element)
        elif subDetID!=None:
            subDet = "_"+subDetID
        else:
            subDet = ""
        subSubDetID=Detector.GetSubSubDetectorID(ddlID)
        if subSubDetID==None:
            subSubDetID=""
        else:
            subSubDetID="_"+subSubDetID
        # print ddlID,slice,detID,subDetID
        # print detID+"_"+baseID+subDet
        # print detID+"_"+baseID+subDet+subSubDetID
        return detID+"_"+baseID+subDet+subSubDetID

    def ReadProcessGroups( self, xPathNode, processType, ddlList, detectors=None, detector=None ):
        procGroups = xPathNode.xpathEval("./ProcessGroup")
        groups = {}
        for pg in procGroups:
            pgIDs = pg.xpathEval( "./@ID" )
            if len(pgIDs)!=1:
                raise ConfigException( "%s process group given without ID or more than one ID" % processType )
            groupID = pgIDs[0].content
            tmpProcs = self.ReadProcesses( pg, processType, ddlList, detectors, detector, groupID )

            outputProcs = [ op.content for op in pg.xpathEval( "./OutputProcess" ) ]
            if len(outputProcs)<=0:
                raise ConfigException( "At least one OutputProcess tag must be given for %s process group %s" % (processType,groupID) )

            nodes = [ nn.content for nn in pg.xpathEval("./Node") ]
            for nn in nodes:
                if self.fResMan.GetNodeHostname(nn)==None:
                    raise ConfigException( "Process group "+groupID+" node "+nn+" is not contained in node list." )
                
            if len(pg.xpathEval("./Multiplicity"))>0:
                if len(pg.xpathEval("./Multiplicity"))>1:
                    raise ConfigException( "Only one Multiplicity tag may be given for %s process group %s" % (processType,xPathNode.groupID) )
                try:
                    mult = int(pg.xpathEval("./Multiplicity")[0].content)
                except ValueError:
                    raise ConfigException( "Multiplicity given for %s process group %s must be an integer" % (processType,groupID) )
                if mult<=0:
                    raise ConfigException( "Multiplicity given for %s process group %s must be at least 1" % (processType,groupID) )
            else:
                raise ConfigException( "At least one Multiplicity tag must be given for %s process group %s" % (processType,groupID) )
            if len(nodes)>mult:
                raise ConfigException( "Multiplicity given for %s process group %s (%d) must be at least as large as number of nodes specified (%d)" % (processType,groupID, mult, len(nodes)) )

            parents = set()
            optparents = set()
            groupWeight = 0.0
            onlyOneParentFromGroup = set()
            optOnlyOneParentFromGroup = set()
            for proc in tmpProcs.values():
                onlyOneParentFromGroup |= set(proc["onlyoneparentfromgroup"])
                optOnlyOneParentFromGroup |= set(proc["onlyoneparentfromgroup"])
            for proc in tmpProcs.values():
                onlyOneParentFromGroup -= ( set(proc["parents"]) - set(proc["onlyoneparentfromgroup"])  )
                optOnlyOneParentFromGroup -= ( set(proc["optparents"]) - set(proc["optonlyoneparentfromgroup"])  )
                parents |= set(proc["parents"])
                optparents |= set(proc["optparents"])
                if proc["lowload"]:
                    groupWeight += 0.1*proc["mult"]
                else:
                    groupWeight += proc["mult"]
            # nodes = nodes + [ None ]*(mult-len(nodes)) # done in MakeProcess
            
            groupProcIDs = tmpProcs.keys()
            parents -= set(groupProcIDs) # Determine parents outside of group
            optparents -= set(groupProcIDs)

            # group = SimpleChainConfig1.ProcessGroup( groupID, nodes, groupProcIDs, outputProcs )
            
            # procs = {}
            # for p in tmpProcs.keys():
            #     procs[groupID+"/"+p] = tmpProcs[p]
            procs = tmpProcs
            
            groups[groupID] = { "id" : groupID,
                                "processes" : procs,
                                "nodes" : nodes,
                                "outputprocesses" : outputProcs,
                                "mult" : mult,
                                "processorgroup" : "group",
                                "parents" : list(parents),
                                "optparents" : list(optparents),
                                "weight" : groupWeight,
                                "onyoneparentfromgroup" : list(onlyOneParentFromGroup),
                                "optonyoneparentfromgroup" : list(optOnlyOneParentFromGroup),
                                }
            if detectors!=None:
                groups[groupID]["detectors"] = detectors
        return groups

    def ReadProcesses( self, xPathNode, processType, ddlList, detectors=None, detector=None, groupID=None ):
        processes    = {}
        comps = xPathNode.xpathEval("./Component")
        for comp in comps:
            processes[comp.xpathEval("./@ID")[0].content] = self.ReadComponent( comp, processType, ddlList, False, detectors, detector )
        trigcomps = xPathNode.xpathEval("./TriggerComponent")
        for trigcomp in trigcomps:
            processes[trigcomp.xpathEval("./@ID")[0].content] = self.ReadComponent( trigcomp, processType, ddlList, True, detectors, detector )
        procs = xPathNode.xpathEval("./Process")
        for proc in procs:
            processes[proc.xpathEval("./@ID")[0].content] = self.ReadProcess( proc, processType, ddlList, False, detectors, detector )
        trigprocs = xPathNode.xpathEval("./TriggerProcess")
        for trigproc in trigprocs:
            processes[trigproc.xpathEval("./@ID")[0].content] = self.ReadProcess( trigproc, processType, ddlList, True, detectors, detector )
        dumps = xPathNode.xpathEval("./Dump")
        for dump in dumps:
            dumpEntry = self.ReadDump( dump, processType, ddlList, detectors, detector )
            processes[dumpEntry["id"]] = dumpEntry
        if groupID==None:
            groups = self.ReadProcessGroups( xPathNode, processType, ddlList, detectors, detector )
            for groupID in groups.keys():
                processes[groupID] = groups[groupID]
        return processes
                
    def ReadComponent(self, comp, processType, ddlList, triggerComponent=False, detectors=None, detector=None, groupID=None ):
        if len(comp.xpathEval("./@ID"))!=1:
            raise ConfigException( "%s component given without ID or more than one ID" % processType )
        if triggerComponent and len(comp.xpathEval("./@detector"))<1 and detectors==None:
            raise ConfigException( "%s trigger component %s given without detector" % ( processType, comp.xpathEval("./@ID")[0].content ) )
        if len(comp.xpathEval("./ComponentID"))!=1:
            raise ConfigException( "Exactly one component ID must be given for %s component %s" % (processType,comp.xpathEval("./@ID")[0].content) )
            
        if len(comp.xpathEval("./Shm"))!=1:
            raise ConfigException( "Exactly one shm must be given for %s component %s" % (processType,comp.xpathEval("./@ID")[0].content) )
        if len(comp.xpathEval("./Shm/@blocksize"))!=1:
            raise ConfigException( "Shm tag for %s component %s must have one blocksize attribute" % (processType,comp.xpathEval("./@ID")[0].content) )
        try:
            blocksize = self.StringSize2Int( comp.xpathEval("./Shm/@blocksize")[0].content )
        except ValueError:
            raise ConfigException( "Shm blocksize attribute for %s component %s is not an integer" % (processType,comp.xpathEval("./@ID")[0].content) )
        if len(comp.xpathEval("./Shm/@blockcount"))!=1:
            raise ConfigException( "Shm tag for %s component %s must have one blockcount attribute" % (processType,comp.xpathEval("./@ID")[0].content) )
        try:
            blockcount = int( comp.xpathEval("./Shm/@blockcount")[0].content )
        except ValueError:
            raise ConfigException( "Shm blockcount attribute for %s component %s is not an integer" % (processType,comp.xpathEval("./@ID")[0].content) )


                
        compEntry = {
            "id" : comp.xpathEval("./@ID")[0].content,
            "type":"comp",
            "component":comp.xpathEval("./ComponentID")[0].content,
            "mult":1,
            "shm" : { "blocksize":blocksize, "blockcount":blockcount },
            "forcefep" : 0,
            "awsparameters" : "",
            "lowload" : 0,
            }

        if len(comp.xpathEval("./@type")):
            compEntry["proctype"] = comp.xpathEval("./@type")[0].content;
        else:
            compEntry["proctype"] = "prc"

        if triggerComponent:
            compEntry["trigger4detector"] = set()
            for cont in comp.xpathEval("./@detector"):
                for det in cont.content.split(","):
                    compEntry["trigger4detector"].add( det )
                    self.fTriggeredDetectors.add( det )
            if len(compEntry["trigger4detector"])<=0 and detectors!=None:
                for det in detectors:
                    compEntry["trigger4detector"].add( det )
                    self.fTriggeredDetectors.add( det )
        if detectors!=None:
            compEntry["detectors"] = detectors
        if len(comp.xpathEval("./Options"))>0:
            if len(comp.xpathEval("./Options"))>1:
                raise ConfigException( "Only one Options tag may be given for %s component %s" % (processType,comp.xpathEval("./@ID")[0].content) )
            if len(comp.xpathEval("./Options")[0].content)>0 and not comp.xpathEval("./Options")[0].content.isspace():
                compEntry["options"] = comp.xpathEval("./Options")[0].content
        if len(comp.xpathEval("./Library"))>0:
            if len(comp.xpathEval("./Library"))>1:
                raise ConfigException( "Only one Library tag may be given for %s component %s" % (processType,comp.xpathEval("./@ID")[0].content) )
            if len(comp.xpathEval("./Library")[0].content)>0 and not comp.xpathEval("./Library")[0].content.isspace():
                compEntry["library"] = comp.xpathEval("./Library")[0].content
        if len(comp.xpathEval("./AddLibrary"))>0:
            compEntry["addlibraries"] = []
            compEntry["beforelibraries"] = {}
            for al in comp.xpathEval("./AddLibrary"):
                if len(al.content)>0 and not al.content.isspace():
                    compEntry["addlibraries"].append( al.content )
                    if al.prop( "before" )!=None:
                        compEntry["beforelibraries"][al.content] = al.prop( "before" )
        if len(comp.xpathEval("./Multiplicity"))>0:
            if len(comp.xpathEval("./Multiplicity"))>1:
                raise ConfigException( "Only one Multiplicity tag may be given for %s component %s" % (processType,comp.xpathEval("./@ID")[0].content) )
            try:
                compEntry["mult"] = int(comp.xpathEval("./Multiplicity")[0].content)
            except ValueError:
                raise ConfigException( "Multiplicity given for %s component %s must be an integer" % (processType,comp.xpathEval("./@ID")[0].content) )
            if compEntry["mult"]<=0:
                raise ConfigException( "Multiplicity given for %s component %s must be at least 1" % (processType,comp.xpathEval("./@ID")[0].content) )
        if len(comp.xpathEval("./Node"))>0:
            if len(comp.xpathEval("./Node"))>compEntry["mult"]:
                raise ConfigException( "Number of nodes given for %s component %s can be at most as large as given multiplicity" % (processType,comp.xpathEval("./@ID")[0].content) )
            nodes=[]
            for n in comp.xpathEval("./Node"):
                if self.fResMan.GetNodeHostname(n.content)==None:
                    raise ConfigException( "Node "+n.content+" given for "+processType+" component "+comp.xpathEval("./@ID")[0].content+" is not contained in node list." )
                nodes.append( n.content )
            compEntry["nodes"] = nodes
        if len(comp.xpathEval("./ForceFEP"))>0:
            compEntry["forcefep"] = 1
        for n in comp.xpathEval("./AWSOptions"):
            compEntry["awsparameters"] = compEntry["awsparameters"]+" "+n.content
        if len(comp.xpathEval("./LowLoad"))>0:
            compEntry["lowload"] = 1
        if len(comp.xpathEval("./KeepScatteredInput")):
            compEntry["keepscatteredinput"] = 1
        compEntry["ddllist"] = ddlList

        self.ReadComponentProcessorDump( "component", compEntry, comp, processType, ddlList, triggerComponent, detectors, detector, groupID )

        if compEntry["id"].find("/")!=-1 or detector==None:
            self.AddContributor( compEntry["id"] )
        else:
            self.AddContributor( detector+"/"+compEntry["id"] )

        return compEntry


    def ReadProcess( self, proc, processType, ddlList, triggerProcess=False, detectors=None, detector=None, groupID=None ):
        if len(proc.xpathEval("./@ID"))!=1:
            raise ConfigException( "%s process given without ID or more than one ID" % processType )
        if triggerProcess and len(proc.xpathEval("./@detector"))<1 and detectors==None:
            raise ConfigException( "%s trigger component %s given without detector" % ( processType, proc.xpathEval("./@ID")[0].content ) )
        if len(proc.xpathEval("./Command"))!=1 and len(proc.xpathEval("./ExplicitMerger")) != 1:
            raise ConfigException( "Exactly one command must be given for %s process %s" % (processType,proc.xpathEval("./@ID")[0].content) )
        if len(proc.xpathEval("./Command")) and len(proc.xpathEval("./ExplicitMerger")):
            raise ConfigException( "There must be either Command or ExplicitMerger for %s process %s" % (processType,proc.xpathEval("./@ID")[0].content) )
        if len(proc.xpathEval("./@type"))!=1:
            raise ConfigException( "Exactly one type attribute (snk or prc) must be given for %s process %s" % (processType,proc.xpathEval("./@ID")[0].content) )
        if proc.xpathEval("./@type")[0].content!="snk" and proc.xpathEval("./@type")[0].content!="prc":
            raise ConfigException( "type attribute must be 'snk' or 'prc' for %s process %s" % (processType,proc.xpathEval("./@ID")[0].content) )
            
        if len(proc.xpathEval("./Shm"))!=1:
            raise ConfigException( "Exactly one shm must be given for %s process %s" % (processType,proc.xpathEval("./@ID")[0].content) )
        if len(proc.xpathEval("./Shm/@blocksize"))!=1:
            raise ConfigException( "Shm tag for %s process %s must have one blocksize attribute" % (processType,proc.xpathEval("./@ID")[0].content) )
        try:
            blocksize = self.StringSize2Int( proc.xpathEval("./Shm/@blocksize")[0].content )
        except ValueError:
            raise ConfigException( "Shm blocksize attribute for %s process %s is not an integer" % (processType,proc.xpathEval("./@ID")[0].content) )
        if len(proc.xpathEval("./Shm/@blockcount"))!=1:
            raise ConfigException( "Shm tag for %s process %s must have one blockcount attribute" % (processType,proc.xpathEval("./@ID")[0].content) )
        try:
            blockcount = int( proc.xpathEval("./Shm/@blockcount")[0].content )
        except ValueError:
            raise ConfigException( "Shm blockcount attribute for %s process %s is not an integer" % (processType,proc.xpathEval("./@ID")[0].content) )

        if len(proc.xpathEval("./ExplicitMerger")) == 1:
            procCmd = "Relay" #If there is only one parent after the ddl list anding, there won't be a merger, so a relay must stay here.
        else:
            procCmd = proc.xpathEval("./Command")[0].content
        procEntry = {
            "id" : proc.xpathEval("./@ID")[0].content,
            "type":"proc",
            "proctype" : proc.xpathEval("./@type")[0].content,
            "command":{ -1 : procCmd },
            "mult":1,
            "shm" : { "blocksize":blocksize, "blockcount":blockcount },
            "forcefep" : 0,
            "lowload" : 0,
            }
        if len(proc.xpathEval("./ExplicitMerger")):
            procEntry["mergecount"] = proc.xpathEval("./ExplicitMerger")[0].content
            if procEntry["mergecount"] == "":
                procEntry["mergecount"] = 0
            else:
                procEntry["mergecount"] = int(procEntry["mergecount"])
        if triggerProcess:
            procEntry["trigger4detector"] = set()
            for cont in proc.xpathEval("./@detector"):
                for det in cont.content.split(","):
                    procEntry["trigger4detector"].add( det )
                    self.fTriggeredDetectors.add( det )
            if len(procEntry["trigger4detector"])<=0 and detectors!=None:
                for det in detectors:
                    procEntry["trigger4detector"].add( det )
                    self.fTriggeredDetectors.add( det )
        if detectors!=None:
            procEntry["detectors"] = detectors
        if len(proc.xpathEval("./Multiplicity"))>0:
            if len(proc.xpathEval("./Multiplicity"))>1:
                raise ConfigException( "Only one Multiplicity tag may be given for %s process %s" % (processType,proc.xpathEval("./@ID")[0].content) )
            try:
                procEntry["mult"] = int(proc.xpathEval("./Multiplicity")[0].content)
            except ValueError:
                raise ConfigException( "Multiplicity given for %s process %s must be an integer" % (processType,proc.xpathEval("./@ID")[0].content) )
            if procEntry["mult"]<=0:
                raise ConfigException( "Multiplicity given for %s process %s must be at least 1" % (processType,proc.xpathEval("./@ID")[0].content) )
        if len(proc.xpathEval("./Node"))>0:
            if len(proc.xpathEval("./Node"))>procEntry["mult"]:
                raise ConfigException( "Number of nodes given for %s process %s can be at most as large as given multiplicity" % (processType,proc.xpathEval("./@ID")[0].content) )
            nodes=[]
            for n in proc.xpathEval("./Node"):
                if self.fResMan.GetNodeHostname(n.content)==None:
                    raise ConfigException( "Node "+n.content+" given for "+processType+" process "+proc.xpathEval("./@ID")[0].content+" is not contained in node list." )
                nodes.append( n.content )
            procEntry["nodes"] = nodes
        if len(proc.xpathEval("./ForceFEP"))>0:
            procEntry["forcefep"] = 1
        if len(proc.xpathEval("./ShmType"))>0:
            procEntry["shmtype"] = proc.xpathEval("./ShmType")[0].content
        if len(proc.xpathEval("./LowLoad"))>0:
            procEntry["lowload"] = 1
        procEntry["ddllist"] = ddlList

        self.ReadComponentProcessorDump( "process", procEntry, proc, processType, ddlList, triggerProcess, detectors, detector, groupID )

        if procEntry["id"].find("/")!=-1 or detector==None:
            self.AddContributor( procEntry["id"] )
        else:
            self.AddContributor( detector+"/"+procEntry["id"] )
        return procEntry

    def ReadDump( self, dump, processType, ddlList, detectors=None, detector=None, groupID=None ):
        if len(dump.xpathEval("./Type"))!=1:
            raise ConfigException( "Exactly one type tag must be given for %s dump" % (processType) )
        if dump.xpathEval("./Type")[0].content!="tcp":
            raise ConfigException( "Only tcp type allowed for %s dump" % (processType) )
        dumpID = "TDS"
            
        dumpEntry = {
            "id":"%s_%d" % (dumpID,self.fHighestDumpID),
            "type":"dump",
            "dumptype":dump.xpathEval("./Type")[0].content,
            "mult":1,
            "shm" : { "blocksize":4096, "blockcount":1 },
            "forcefep" : 0,
            "requestinterval" : 0,
            "allevents" : 0,
            "lowload" : 1,
            "modulo" : None,
            }
        if detectors!=None:
            dumpEntry["detectors"] = detectors
        self.fHighestDumpID += 1
        if len(dump.xpathEval("./Node"))>0:
            if len(dump.xpathEval("./Node"))>1:
                raise ConfigException( "Only one node may be given for %s dump %s" % (processType,dump.xpathEval("./@ID")[0].content) )
            dumpEntry["nodes"] = [ dump.xpathEval("./Node")[0].content ]
            for n in dumpEntry["nodes"]:
                if self.fResMan.GetNodeHostname(n)==None:
                    raise ConfigException( "Node "+n+" given for "+processType+" dump "+dump.xpathEval("./@ID")[0].content+" is not contained in node list." )
        if len(dump.xpathEval("./Port"))>0:
            if len(dump.xpathEval("./Port"))>1:
                raise ConfigException( "Only one port number may be given for %s dump %s" % (processType,dump.xpathEval("./@ID")[0].content) )
            try:
                dumpEntry["port"] = int(dump.xpathEval("./Port")[0].content)
            except ValueError:
                raise ConfigException( "Port '%s' given for  %s dump %s is not a number" % (dump.xpathEval("./Port")[0].content,processType,dump.xpathEval("./@ID")[0].content) )
        if len(dump.xpathEval("./ForceFEP"))>0:
            dumpEntry["forcefep"] = 1
        if len(dump.xpathEval("./RequestMonitorInterval"))>0:
            if len(dump.xpathEval("./RequestMonitorInterval"))>1:
                raise ConfigException( "Only one monitor event request interval may be given for %s dump %s" % (processType,dump.xpathEval("./@ID")[0].content) )
            try:
                dumpEntry["requestinterval"] = int(dump.xpathEval("./RequestMonitorInterval")[0].content)
            except ValueError:
                raise ConfigException( "Monitor event request interval '%s' given for  %s dump %s is not a number" % (dump.xpathEval("./RequestMonitorInterval")[0].content,processType,dump.xpathEval("./@ID")[0].content) )
        if len(dump.xpathEval("./AllEvents"))>0:
            dumpEntry["allevents"] = 1
        if len(dump.xpathEval("./EventModulo"))>0:
            try:
                dumpEntry["modulo"] = int(dump.xpathEval("./EventModulo")[0].content,0)
            except ValueError:
                raise ConfigException( "Event modulo specifier '%s' given for  %s dump %s is not a number" % (dump.xpathEval("./EventModulo")[0].content,processType,dump.xpathEval("./@ID")[0].content) )
        dumpEntry["ddllist"] = ddlList

        self.ReadComponentProcessorDump( "dump", dumpEntry, dump, processType, ddlList, False, detectors, detector, groupID )


        return dumpEntry

    def ReadComponentProcessorDump( self, compOrProc, compProcEntry, compProc, compProcType, ddlList, triggerCompProc=False, detectors=None, detector=None, groupID=None ):
        if len(compProc.xpathEval("./Parent"))+len(compProc.xpathEval("./OptionalParent"))<1:
            raise ConfigException( "No parent given for %s %s %s" % (compProcType,compOrProc,compProc.xpathEval("./@ID")[0].content) )
        parents=[]
        parentFilters = {}
        onlyOneParentFromGroup = []
        for p in compProc.xpathEval("./Parent"):
            par = self.ReplaceVariables( p.content, None, None, {} )
            # par = string.Template( p.content ).substitute( self.fMacros )
            parents.append( par )
            filterType = p.prop("filtered")
            if filterType!=None and filterType!="none":
                parentFilters[par] = filterType
            selectionType = p.prop("selection")
            if selectionType=="single":
                onlyOneParentFromGroup.append(par)
            elif selectionType!=None and selectionType!="all":
                raise ConfigException( "Unknown parent selection type '%s' for parent %s (%s) of %s %s" % (selectionType,par,p.content,compOrProc,compProc.xpathEval("./@ID")[0].content) )
            
        optParents = set()
        optParentFilters = {}
        optOnlyOneParentFromGroup = set()
        for p in compProc.xpathEval("./OptionalParent"):
            par = self.ReplaceVariables( p.content, None, None, {} )
            # par = string.Template( p.content ).substitute( self.fMacros )
            optParents.add( par )
            filterType = p.prop("filtered")
            if filterType!=None and filterType!="none":
                optParentFilters[par] = filterType
            selectionType = p.prop("selection")
            if selectionType=="single":
                optOnlyOneParentFromGroup.add(par)
            elif selectionType!=None and selectionType!="all":
                raise ConfigException( "Unknown parent selection type '%s' for optional parent %s (%s) of %s %s" % (selectionType,par,p.content,compOrProc,compProc.xpathEval("./@ID")[0].content) )

        reqParents = set()
        reqParentFilters = {}
        reqOnlyOneParentFromGroup = set()
        for p in compProc.xpathEval("./RequiredContributor"):
            par = self.ReplaceVariables( p.content, None, None, {} )
            # par = string.Template( p.content ).substitute( self.fMacros )
            reqParents.add( par )
            if p.content.find("/")!=-1 or detector==None:
                self.AddRequiredContributor( par )
            else:
                self.AddRequiredContributor( detector+"/"+par )

        compProcEntry["parents"] = parents
        compProcEntry["parentfilters"] = parentFilters
        compProcEntry["onlyoneparentfromgroup"] = onlyOneParentFromGroup
        compProcEntry["optparents"] = optParents
        compProcEntry["optparentfilters"] = optParentFilters
        compProcEntry["optonlyoneparentfromgroup"] = optOnlyOneParentFromGroup
        compProcEntry["reqparents"] = reqParents
        compProcEntry["groupid"] = groupID
        compProcEntry["processorgroup"] = "process"

        if len(compProc.xpathEval("./EventModulo"))>0:
            evalStr = self.ReplaceVariables( compProc.xpathEval("./EventModulo")[0].content, None, None, {} )
            # temp = string.Template( compProc.xpathEval("./EventModulo")[0].content )
            # evalStr = temp.substitute( self.fMacros )

            try:
                eventModulo = int( evalStr )
            except ValueError:
                raise ConfigException( "Content of EventModulo tag for %s %s %s is not an integer" % (compProcType,compOrProc,compProc.xpathEval("./@ID")[0].content) )
            compProcEntry["eventmodulo"] = eventModulo

        if len(compProc.xpathEval("./EventTypeCode"))>0:
            eventTypeCode = self.ReplaceVariables( compProc.xpathEval("./EventTypeCode")[0].content, None, None, {} )
            # temp = string.Template( compProc.xpathEval("./EventTypeCode")[0].content )
            # eventTypeCode = temp.substitute( self.fMacros )
            compProcEntry["eventtypecode"] = eventTypeCode



    def ReadNodeDump( self, dump, processType ):
        if len(dump.xpathEval("./Type"))!=1:
            raise ConfigException( "Exactly one type tag must be given for %s dump" % (processType) )
        if dump.xpathEval("./Type")[0].content!="tcp":
            raise ConfigException( "Only tcp type allowed for %s dump" % (processType) )
        dumpID = "TDS"
        # if len(dump.xpathEval("./Node"))!=1:
        #    raise ConfigException( "Exactly one node must be given for %s dump %s" % (processType,dump.xpathEval("./@ID")[0].content) )
        dumpEntry = {
            "id":"%s_%d" % (dumpID,self.fHighestDumpID),
            "type":"nodedump",
            "dumptype":dump.xpathEval("./Type")[0].content,
            # "node":dump.xpathEval("./Parent")[0].content,
            "mult":1,
            "shm" : { "blocksize":4096, "blockcount":1 },
            "lowload" : 1,
            "processorgroup" : "process",
            }
        self.fHighestDumpID += 1
##        if len(dump.xpathEval("./Node"))>0:
##            nodes=[]
##            for n in dump.xpathEval("./Node"):
##                nodes.append( n.content )
##            dumpEntry["nodes"] = nodes
        if len(dump.xpathEval("./Port"))>0:
            if len(dump.xpathEval("./Port"))>1:
                raise ConfigException( "Only one port number may be given for %s dump %s" % (processType,dump.xpathEval("./@ID")[0].content) )
            try:
                dumpEntry["port"] = int(dump.xpathEval("./port")[0].content)
            except ValueError:
                raise ConfigException( "Port '%s' given for  %s dump %s is not a number" % (dump.xpathEval("./port")[0].content,processType,dump.xpathEval("./@ID")[0].content) )
        return dumpEntry

        
    def StringSize2Int(self, sizeStr):
        if sizeStr[-1:] == "k":
            factor = 1024
            sizeStr = sizeStr[:-1]
        elif sizeStr[-1:] == "M":
            factor = 1024*1024
            sizeStr = sizeStr[:-1]
        elif sizeStr[-1:] == "G":
            factor = 1024*1024*1024
            sizeStr = sizeStr[:-1]
        else:
            factor = 1
        return int( sizeStr )*factor

    def StringFlag2Bool(self, flagStr):
        if flagStr.lower() in ( "1", "true", "yes" ):
            return True
        elif flagStr.lower() in ( "0", "false", "no" ):
            return False
        raise ValueError( "Value '"+flagStr+"' cannot be converted to boolean (true,false,yes,no,0,1 are allowed)" )


    def ApplyDetectorOptions( self, xPathNode, ddlList, force ):
        optionKeys = { "RORCPublisherOptions" : "options",
                       "RORCShm/@blocksize" : "blocksize",
                       "RORCShm/@blockcount" : "blockcount",
                       "RORCDataType" : "datatype",
                       "FilePublisherOptions" : "fileoptions",
                       "RORC/@active" : "enable",
                       "HWCoProc/@active" : "hwcoproc",
                       "HWCoProc/OutputDataType" : "hwcoprocdatatype",
                       "HWCoProc/RORCPublisherOptions" : "hwcoprocrorcoptions",
                       "HWCoProc/FilePublisherOptions" : "hwcoprocfileoptions",
                       "HWCoProc/RORCShm/@blocksize" : "hwcoprocblocksize",
                       "HWCoProc/RORCShm/@blockcount" : "hwcoprocblockcount",
                       }
        transFuncs = { "RORCPublisherOptions" : lambda x: x,
                       "RORCShm/@blocksize" : self.StringSize2Int,
                       "RORCShm/@blockcount" : int,
                       "RORCDataType" : lambda x: x,
                       "FilePublisherOptions" : lambda x: x,
                       "RORC/@active" : self.StringFlag2Bool,
                       "HWCoProc/@active" : self.StringFlag2Bool,
                       "HWCoProc/OutputDataType" : lambda x: x,
                       "HWCoProc/RORCPublisherOptions" : lambda x: x,
                       "HWCoProc/FilePublisherOptions" : lambda x: x,
                       "HWCoProc/RORCShm/@blocksize" : self.StringSize2Int,
                       "HWCoProc/RORCShm/@blockcount" : int,
                       }
        for k in optionKeys.keys():
            if len(xPathNode.xpathEval( k ))>0:
                opt = transFuncs[k]( xPathNode.xpathEval( k )[0].content )
                if k =="RORCPublisherOptions" and ddlList!=None:
                    if len(xPathNode.xpathEval( k )[0].xpathEval( "@mode"))>0 and xPathNode.xpathEval( k )[0].xpathEval( "@mode")[0].content=="append":
                        opt = self.fRORCs["GENERIC.options"] + " " + opt
                if k =="FilePublisherOptions" and ddlList!=None:
                    if len(xPathNode.xpathEval( k )[0].xpathEval( "@mode"))>0 and xPathNode.xpathEval( k )[0].xpathEval( "@mode")[0].content=="append":
                        opt = self.fRORCs["GENERIC.fileoptions"] + " " + opt
                if k =="HWCoProc/RORCPublisherOptions" and ddlList!=None:
                    if len(xPathNode.xpathEval( k )[0].xpathEval( "@mode"))>0 and xPathNode.xpathEval( k )[0].xpathEval( "@mode")[0].content=="append":
                        if self.fRORCs["GENERIC.hwcoprocrorcoptions"]!=None:
                            baseopt = self.fRORCs["GENERIC.hwcoprocrorcoptions"]
                        else:
                            baseopt = self.fRORCs["GENERIC.options"]
                        opt = baseopt + " " + opt
                if k =="HWCoProc/FilePublisherOptions" and ddlList!=None:
                    if len(xPathNode.xpathEval( k )[0].xpathEval( "@mode"))>0 and xPathNode.xpathEval( k )[0].xpathEval( "@mode")[0].content=="append":
                        if self.fRORCs["GENERIC.hwcoprocfileoptions"]!=None:
                            baseopt = self.fRORCs["GENERIC.hwcoprocfileoptions"]
                        else:
                            baseopt = self.fRORCs["GENERIC.fileoptions"]
                        opt = baseopt + " " + opt
                if ddlList!=None:
                    for i in ddlList:
                        if (force or not self.fRORCs.has_key( "%d.%s" % ( i, optionKeys[k] ) )) and (i in self.fDDLs):
                            self.fRORCs["%d.%s" % ( i, optionKeys[k] )] = opt
                else:
                    self.fRORCs["GENERIC.%s" % optionKeys[k]] = opt
        
        
    def GetTDSPort(self,nodeID,suggest=None):
        node = self.GetNodeHostname( nodeID )
        if suggest==None:
            value = self.fDefaultTDSPort+1
        else:
            value = suggest
        if self.fTDSPorts.has_key(node):
            while value in self.fTDSPorts[node] and value<65536:
                value += 1
            if value==65536:
                value=1025
            while value in self.fTDSPorts[node] and value<suggest:
                value += 1
            if not value in self.fTDSPorts[node]:
                self.fTDSPorts[node].add(value)
        else:
            self.fTDSPorts[node] = set([value])
        return value
            

    
    
#def IntString2Set( x ):
    


