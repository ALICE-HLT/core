#!/usr/bin/env python

import Exceptions
import types


IDs = { "SPD" : 0, "SDD" : 1, "SSD" : 2, "TPC" : 3, "TRD" : 4,
        "TOF" : 5, "HMPI" : 6, "PHOS" : 7, "CPV" : 8, "PMD" : 9,
        "MUTK" : 10, "MUTG" : 11, "FMD" : 12, "T0" : 13, "V0" : 14,
        "ZDC" : 15, "ACOR" : 16, "TRG" : 17, "EMCA" : 18, "DAQT" : 19,
        "AD" : 21,
        "HLT" : 30,
         }

ReadoutListVersion = 0

DDLCounts = [
    { "SPD" : 0, "SDD" : 0, "SSD" : 0, "TPC" : 0, "TRD" : 0,
      "TOF" : 0, "HMPI" : 0, "PHOS" : 0, "CPV" : 0, "PMD" : 0,
      "MUTK" : 0, "MUTG" : 0, "FMD" : 0, "T0" : 0, "V0" : 0,
      "ZDC" : 0, "ACOR" : 0, "TRG" : 0, "EMCA" : 0, "DAQT" : 0,
      "AD" : 0,
      "HLT" : 0,
      },
    { "SPD" : 20, "SDD" : 24, "SSD" : 16, "TPC" : 216, "TRD" : 18,
      "TOF" : 72, "HMPI" : 14, "PHOS" : 20, "CPV" : 10, "PMD" : 6,
      "MUTK" : 20, "MUTG" : 2, "FMD" : 3, "T0" : 1, "V0" : 1,
      "ZDC" : 1, "ACOR" : 1, "TRG" : 1, "EMCA" : 24, "DAQT" : 1,
      "AD" : 0,
      "HLT" : 10,
      },
    { "SPD" : 20, "SDD" : 24, "SSD" : 16, "TPC" : 216, "TRD" : 18,
      "TOF" : 72, "HMPI" : 14, "PHOS" : 20, "CPV" : 10, "PMD" : 6,
      "MUTK" : 20, "MUTG" : 2, "FMD" : 3, "T0" : 1, "V0" : 1,
      "ZDC" : 1, "ACOR" : 1, "TRG" : 1, "EMCA" : 24, "DAQT" : 1,
      "AD" : 0,
      "HLT" : 10,
      },
    { "SPD" : 20, "SDD" : 24, "SSD" : 16, "TPC" : 216, "TRD" : 18,
      "TOF" : 72, "HMPI" : 14, "PHOS" : 20, "CPV" : 10, "PMD" : 6,
      "MUTK" : 20, "MUTG" : 2, "FMD" : 3, "T0" : 1, "V0" : 1,
      "ZDC" : 1, "ACOR" : 1, "TRG" : 1, "EMCA" : 46, "DAQT" : 1,
      "AD" : 0,
      "HLT" : 28,
      },
    { "SPD" : 20, "SDD" : 24, "SSD" : 16, "TPC" : 216, "TRD" : 18,
      "TOF" : 72, "HMPI" : 14, "PHOS" : 21, "CPV" : 10, "PMD" : 6,
      "MUTK" : 20, "MUTG" : 2, "FMD" : 3, "T0" : 1, "V0" : 1,
      "ZDC" : 1, "ACOR" : 1, "TRG" : 1, "EMCA" : 46, "DAQT" : 1,
      "AD" : 0,
      "HLT" : 28,
      },
    { "SPD" : 20, "SDD" : 24, "SSD" : 16, "TPC" : 216, "TRD" : 18,
      "TOF" : 72, "HMPI" : 14, "PHOS" : 21, "CPV" : 10, "PMD" : 6,
      "MUTK" : 20, "MUTG" : 2, "FMD" : 3, "T0" : 1, "V0" : 1,
      "ZDC" : 1, "ACOR" : 1, "TRG" : 1, "EMCA" : 46, "DAQT" : 1,
      "AD" : 1,
      "HLT" : 28,
      },
    ]



ReadoutListWord2Detector = [
    [
        "SPD",  # 0
        "SDD", # 1
        "SSD", # 2
        "TPC", "TPC", "TPC", "TPC", "TPC", "TPC", "TPC", "TPC", # 3-10
        "TRD", # 11
        "TOF", "TOF", "TOF", # 12-14
        "HMPI", # 15
        "PHOS", # 16
        "CPV", # 17
        "PMD", # 18
        "MUTK", # 19
        "MUTG", # 20
        "FMD", # 21
        "T0", # 22
        "V0", # 23
        "ZDC", # 24
        "ACOR", # 25
        "TRG", # 26
        "EMCA", # 27
        "DAQT", # 28
        "HLT" # 29
        ],
    [
        "SPD",  # 0
        "SDD", # 1
        "SSD", # 2
        "TPC", "TPC", "TPC", "TPC", "TPC", "TPC", "TPC", "TPC", # 3-10
        "TRD", # 11
        "TOF", "TOF", "TOF", # 12-14
        "HMPI", # 15
        "PHOS", # 16
        "CPV", # 17
        "PMD", # 18
        "MUTK", # 19
        "MUTG", # 20
        "FMD", # 21
        "T0", # 22
        "V0", # 23
        "ZDC", # 24
        "ACOR", # 25
        "TRG", # 26
        "EMCA", # 27
        "DAQT", # 28
        "HLT" # 29
        ],
    [
        "SPD",  # 0
        "SDD", # 1
        "SSD", # 2
        "TPC", "TPC", "TPC", "TPC", "TPC", "TPC", "TPC", "TPC", # 3-10
        "TRD", # 11
        "TOF", "TOF", "TOF", # 12-14
        "HMPI", # 15
        "PHOS", # 16
        "CPV", # 17
        "PMD", # 18
        "MUTK", # 19
        "MUTG", # 20
        "FMD", # 21
        "T0", # 22
        "V0", # 23
        "ZDC", # 24
        "ACOR", # 25
        "TRG", # 26
        "EMCA", # 27
        "DAQT", # 28
        "HLT" # 29
        ],
    [
        "SPD",  # 0
        "SDD", # 1
        "SSD", # 2
        "TPC", "TPC", "TPC", "TPC", "TPC", "TPC", "TPC", "TPC", # 3-10
        "TRD", # 11
        "TOF", "TOF", "TOF", # 12-14
        "HMPI", # 15
        "PHOS", # 16
        "CPV", # 17
        "PMD", # 18
        "MUTK", # 19
        "MUTG", # 20
        "FMD", # 21
        "T0", # 22
        "V0", # 23
        "ZDC", # 24
        "ACOR", # 25
        "TRG", # 26
        "EMCA", "EMCA", # 27-28
        "DAQT", # 29
        "HLT" # 30
        ],
    [
        "SPD",  # 0
        "SDD", # 1
        "SSD", # 2
        "TPC", "TPC", "TPC", "TPC", "TPC", "TPC", "TPC", "TPC", # 3-10
        "TRD", # 11
        "TOF", "TOF", "TOF", # 12-14
        "HMPI", # 15
        "PHOS", # 16
        "CPV", # 17
        "PMD", # 18
        "MUTK", # 19
        "MUTG", # 20
        "FMD", # 21
        "T0", # 22
        "V0", # 23
        "ZDC", # 24
        "ACOR", # 25
        "TRG", # 26
        "EMCA", "EMCA", # 27-28
        "DAQT", # 29
        "HLT" # 30
        ],
    [
        "SPD",  # 0
        "SDD", # 1
        "SSD", # 2
        "TPC", "TPC", "TPC", "TPC", "TPC", "TPC", "TPC", "TPC", # 3-10
        "TRD", # 11
        "TOF", "TOF", "TOF", # 12-14
        "HMPI", # 15
        "PHOS", # 16
        "CPV", # 17
        "PMD", # 18
        "MUTK", # 19
        "MUTG", # 20
        "FMD", # 21
        "T0", # 22
        "V0", # 23
        "ZDC", # 24
        "ACOR", # 25
        "TRG", # 26
        "EMCA", "EMCA", # 27-28
        "DAQT", # 29
        "AD", # 30
        "HLT" # 31
        ],
    ]

FirstReadoutListWord = [
    {
        "SPD" : 0,
        "SDD" : 1,
        "SSD" : 2,
        "TPC" : 3,
        "TRD" : 11,
        "TOF" : 12,
        "HMPI" : 15,
        "PHOS" : 16,
        "CPV" : 17,
        "PMD" : 18,
        "MUTK" : 19,
        "MUTG" : 20,
        "FMD" : 21,
        "T0" : 22,
        "V0" : 23,
        "ZDC" : 24,
        "ACOR" : 25,
        "TRG" : 26,
        "EMCA" : 27,
        "DAQT" : 28,
        "AD" : 0,
        "HLT" : 29,
        },
    {
        "SPD" : 0,
        "SDD" : 1,
        "SSD" : 2,
        "TPC" : 3,
        "TRD" : 11,
        "TOF" : 12,
        "HMPI" : 15,
        "PHOS" : 16,
        "CPV" : 17,
        "PMD" : 18,
        "MUTK" : 19,
        "MUTG" : 20,
        "FMD" : 21,
        "T0" : 22,
        "V0" : 23,
        "ZDC" : 24,
        "ACOR" : 25,
        "TRG" : 26,
        "EMCA" : 27,
        "DAQT" : 28,
        "AD" : 0,
        "HLT" : 29,
        },
    {
        "SPD" : 0,
        "SDD" : 1,
        "SSD" : 2,
        "TPC" : 3,
        "TRD" : 11,
        "TOF" : 12,
        "HMPI" : 15,
        "PHOS" : 16,
        "CPV" : 17,
        "PMD" : 18,
        "MUTK" : 19,
        "MUTG" : 20,
        "FMD" : 21,
        "T0" : 22,
        "V0" : 23,
        "ZDC" : 24,
        "ACOR" : 25,
        "TRG" : 26,
        "EMCA" : 27,
        "DAQT" : 28,
        "AD" : 0,
        "HLT" : 29,
        },
    {
        "SPD" : 0,
        "SDD" : 1,
        "SSD" : 2,
        "TPC" : 3,
        "TRD" : 11,
        "TOF" : 12,
        "HMPI" : 15,
        "PHOS" : 16,
        "CPV" : 17,
        "PMD" : 18,
        "MUTK" : 19,
        "MUTG" : 20,
        "FMD" : 21,
        "T0" : 22,
        "V0" : 23,
        "ZDC" : 24,
        "ACOR" : 25,
        "TRG" : 26,
        "EMCA" : 27,
        "DAQT" : 29,
        "AD" : 0,
        "HLT" : 30,
        },
    {
        "SPD" : 0,
        "SDD" : 1,
        "SSD" : 2,
        "TPC" : 3,
        "TRD" : 11,
        "TOF" : 12,
        "HMPI" : 15,
        "PHOS" : 16,
        "CPV" : 17,
        "PMD" : 18,
        "MUTK" : 19,
        "MUTG" : 20,
        "FMD" : 21,
        "T0" : 22,
        "V0" : 23,
        "ZDC" : 24,
        "ACOR" : 25,
        "TRG" : 26,
        "EMCA" : 27,
        "DAQT" : 29,
        "AD" : 0,
        "HLT" : 30,
        },
    {
        "SPD" : 0,
        "SDD" : 1,
        "SSD" : 2,
        "TPC" : 3,
        "TRD" : 11,
        "TOF" : 12,
        "HMPI" : 15,
        "PHOS" : 16,
        "CPV" : 17,
        "PMD" : 18,
        "MUTK" : 19,
        "MUTG" : 20,
        "FMD" : 21,
        "T0" : 22,
        "V0" : 23,
        "ZDC" : 24,
        "ACOR" : 25,
        "TRG" : 26,
        "EMCA" : 27,
        "DAQT" : 29,
        "AD" : 30,
        "HLT" : 31,
        },
]

LongDetectorIDs = {
    "SSD"  : "ITSSSD",
    "SPD"  : "ITSSPD",
    "SDD"  : "ITSSDD",
    "ACOR" : "ACORDE",
    "HMPI" : "HMPID",
    "MUTK" : "MUONTRK",
    "MUTG" : "MUONTRG",
    "EMCA" : "EMCAL",
    "V0"   : "VZERO",
    "DAQT" : "DAQ_TEST",
}


gDetectorHWCoProcDataTypes = {
    "TPC" : "HWCLUST1",
    }

InverseLongDetectorIDs = {}
for k in LongDetectorIDs.keys():
    InverseLongDetectorIDs[LongDetectorIDs[k]] = k


def Get3LevelDetectorDDLList( detectorID, level1Element=None, partition=None ):
    if InverseLongDetectorIDs.has_key(detectorID):
        detID = InverseLongDetectorIDs[detectorID]
    else:
        detID = detectorID
    if gk3LevelDetectorDDLProperties[detID]["ddlList"]!=None:
        return gk3LevelDetectorDDLProperties[detID]["ddlList"]( level1Element, partition )
    if level1Element==None or level1Element==-1:
        if partition==None or partition==-1:
            ddlList = set(range((IDs[detID]<<8),(IDs[detID]<<8)+DDLCounts[ReadoutListVersion][detID]))
        else:
            ddlList = set()
            if partition<gk3LevelDetectorDDLProperties[detID]["partitionsPerLevel1Element"]:
                for level1Element in range(0, (DDLCounts[ReadoutListVersion][detID]/gk3LevelDetectorDDLProperties[detID]["partitionsPerLevel1Element"]) ): # Number of level 1 elements
                    ddlList |= set( [ (IDs[detID]<<8)+level1Element*gk3LevelDetectorDDLProperties[detID]["partitionsPerLevel1Element"]+partition ] )
            elif partition in range((IDs[detID]<<8),(IDs[detID]<<8)+DDLCounts[ReadoutListVersion][detID]):
                ddlList = set( [partition] )
                
    elif partition==None or partition==-1:
        ddlList = set( range( (IDs[detID]<<8)+level1Element*gk3LevelDetectorDDLProperties[detID]["partitionsPerLevel1Element"], (IDs[detID]<<8)+(level1Element+1)*gk3LevelDetectorDDLProperties[detID]["partitionsPerLevel1Element"] ) )
    else:
        if partition<gk3LevelDetectorDDLProperties[detID]["partitionsPerLevel1Element"]:
            ddlList = set( [ (IDs[detID]<<8)+level1Element*gk3LevelDetectorDDLProperties[detID]["partitionsPerLevel1Element"]+partition ] )
        elif partition in range((IDs[detID]<<8)+level1Element*gk3LevelDetectorDDLProperties[detID]["partitionsPerLevel1Element"],(IDs[detID]<<8)+level1Element*gk3LevelDetectorDDLProperties[detID]["partitionsPerLevel1Element"]+gk3LevelDetectorDDLProperties[detID]["partitionsPerLevel1Element"]):
            ddlList = set( [partition] )
        else:
            ddlList = set()
    return ddlList

def Get3LevelDetectorLevel1Element( detectorID, ddlID ):
    if InverseLongDetectorIDs.has_key(detectorID):
        detID = InverseLongDetectorIDs[detectorID]
    else:
        detID = detectorID
    if gk3LevelDetectorDDLProperties[detID]["level1Element"]!=None:
        return gk3LevelDetectorDDLProperties[detID]["level1Element"]( ddlID )
    offset=(IDs[detID]<<8)
    factor=gk3LevelDetectorDDLProperties[detID]["partitionsPerLevel1Element"]
    return (ddlID-offset)//factor


def Get3LevelDetectorLevel2Element( detectorID, ddlID ):
    if InverseLongDetectorIDs.has_key(detectorID):
        detID = InverseLongDetectorIDs[detectorID]
    else:
        detID = detectorID
    if gk3LevelDetectorDDLProperties[detID]["level2Element"]!=None:
        return gk3LevelDetectorDDLProperties[detID]["level2Element"]( ddlID )
    if ddlID<gk3LevelDetectorDDLProperties[detID]["partitionsPerLevel1Element"]:
        return ddlID
    offset=(IDs[detID]<<8)
    factor=gk3LevelDetectorDDLProperties[detID]["partitionsPerLevel1Element"]
    return (ddlID-offset)%factor

def Get3LevelDetectorLevel1ElementName( detectorID ):
    if InverseLongDetectorIDs.has_key(detectorID):
        detID = InverseLongDetectorIDs[detectorID]
    else:
        detID = detectorID
    return gk3LevelDetectorDDLProperties[detID]["level1ElementName"]

def Get3LevelDetectorLevel1ElementCount( detectorID ):
    if InverseLongDetectorIDs.has_key(detectorID):
        detID = InverseLongDetectorIDs[detectorID]
    else:
        detID = detectorID
    return DDLCounts[ReadoutListVersion][detID]/gk3LevelDetectorDDLProperties[detID]["partitionsPerLevel1Element"]

def Is3LevelDetectorRelativePartitionID( detectorID, relPartID ):
    if relPartID>=0 and relPartID<gk3LevelDetectorDDLProperties[detectorID]["partitionsPerLevel1Element"]:
        return True
    return False

def GetTPCDDLList( slice=None, partition=None ):
    if slice==None or slice==-1:
        if partition==None or partition==-1:
            ddlList = set(range((IDs["TPC"]<<8),(IDs["TPC"]<<8)+DDLCounts[ReadoutListVersion]["TPC"]))
        else:
            ddlList = set()
            if partition<6:
                for slice in range(0,36):
                    if partition<2:
                        ddlList |= set( [ (IDs["TPC"]<<8)+slice*2+partition ] )
                    else:
                        ddlList |= set( [ (IDs["TPC"]<<8)+72+slice*4+partition-2 ] )
            elif partition in range((IDs["TPC"]<<8),(IDs["TPC"]<<8)+DDLCounts[ReadoutListVersion]["TPC"]):
                ddlList = set( [partition] )
                
    elif partition==None or partition==-1:
        ddlList = set( [ (IDs["TPC"]<<8)+slice*2, (IDs["TPC"]<<8)+slice*2+1, (IDs["TPC"]<<8)+72+slice*4, (IDs["TPC"]<<8)+72+slice*4+1, (IDs["TPC"]<<8)+72+slice*4+2, (IDs["TPC"]<<8)+72+slice*4+3 ] )
    else:
        if partition<2:
            ddlList = set( [ (IDs["TPC"]<<8)+slice*2+partition ] )
        elif partition<6:
            ddlList = set( [ (IDs["TPC"]<<8)+72+slice*4+partition-2 ] )
        elif partition in range((IDs["TPC"]<<8)+slice*2,(IDs["TPC"]<<8)+slice*2+2) or partition in range((IDs["TPC"]<<8)+72+slice*4,(IDs["TPC"]<<8)+72+slice*4+4):
            ddlList = set( [partition] )
        else:
            ddlList = set()
    return ddlList

def GetTPCSlice( ddlID ):
    if ddlID<(IDs["TPC"]<<8)+72:
        offset=0+(IDs["TPC"]<<8)
        factor=2
    else:
        offset=72+(IDs["TPC"]<<8)
        factor=4
    return (ddlID-offset)//factor

def GetTPCPartition( ddlID ):
    if ddlID<6:
        return ddlID
    if ddlID<(IDs["TPC"]<<8)+72:
        offset=0+(IDs["TPC"]<<8)
        factor=2
        add=0
    else:
        offset=72+(IDs["TPC"]<<8)
        factor=4
        add=2
    return (ddlID-offset)%factor+add


def GetDimuonDDLList( subDet=None, partition=None, level1=None ):
    if subDet=="TRG":
        subDet="MUTG"
    elif subDet=="TRK":
        subDet="MUTK"
    if subDet!=None and subDet!=-1 and subDet!="MUTK" and subDet!="MUTG":
        raise Exceptions.SCC2Exception( "INTERNAL ERROR: Unsupported subdetector ID "+str(subDet)+" for GetDimuonDDLList" )
    if subDet==None or subDet==-1:
        if partition==None or partition==-1:
            ddlList = set(range((IDs["MUTK"]<<8),(IDs["MUTK"]<<8)+DDLCounts[ReadoutListVersion]["MUTK"])) | set(range((IDs["MUTG"]<<8),(IDs["MUTG"]<<8)+DDLCounts[ReadoutListVersion]["MUTG"]))
        else:
            if (partition in range((IDs["MUTK"]<<8),(IDs["MUTK"]<<8)+DDLCounts[ReadoutListVersion]["MUTK"])) or (partition in range((IDs["MUTG"]<<8),(IDs["MUTG"]<<8)+DDLCounts[ReadoutListVersion]["MUTG"])):
                ddlList = set( [partition] )
            else:
                ddlList = set()
                
    else:
        if partition==None or partition==-1:
            rmin = (IDs[subDet]<<8)
            rmax = (IDs[subDet]<<8)+DDLCounts[ReadoutListVersion][subDet]
            if level1 != None: #This level1 variable is used to separate the MUON TRK links from the two FEP nodes.
                rmin += level1 * 12
                rmax = min(rmax, rmin + (level1 + 1) * 12)
            ddlList = set(range(rmin,rmax))
        else:
            if partition in set(range((IDs[subDet]<<8),(IDs[subDet]<<8)+DDLCounts[ReadoutListVersion][subDet])):
                ddlList = set( [partition] )
            else:
                ddlList = set()
    return ddlList

nodedcal0 = [4632, 4633, 4636, 4637, 4640, 4641, 4634, 4635]
nodedcal1 = [4638, 4639, 4642, 4643, 4652, 4644, 4645, 4646, 4653]
nodeemcala05 = [4608, 4609, 4612, 4613, 4616, 4617, 4620, 4621, 4624, 4625, 4628]
nodeemcalc05 = [4610, 4611, 4614, 4615, 4618, 4619, 4622, 4623, 4626, 4627, 4630, 4631]

def GetEMCALDDLList( nodeNum=None, partition=None ):
    ret = set()
    if nodeNum==None or nodeNum==-1:
        if partition==None or partition==-1:
            ret |= set(nodedcal0)
            ret |= set(nodedcal1)
            ret |= set(nodeemcala05)
            ret |= set(nodeemcalc05)
        else:
            if len(nodedcal0) > partition:
                ret |= set(nodedcal0[partition])
            if len(nodedcal1) > partition:
                ret |= set(nodedcal1[partition])
            if len(nodeemcala05) > partition:
                ret |= set(nodeemcala05[partition])
            if len(nodeemcalc05) > partition:
                ret |= set(nodeemcalc05[partition])
    elif nodeNum == 0:
        if partition==None or partition==-1:
            ret |= set(nodedcal0)
        elif len(nodedcal0) > partition:
            ret |= set(nodedcal0[partition])
    elif nodeNum == 1:
        if partition==None or partition==-1:
            ret |= set(nodedcal1)
        elif len(nodedcal1) > partition:
            ret |= set(nodedcal1[partition])
    elif nodeNum == 2:
        if partition==None or partition==-1:
            ret |= set(nodeemcala05)
        elif len(nodeemcala05) > partition:
            ret |= set(nodeemcala05[partition])
    elif nodeNum == 3:
        if partition==None or partition==-1:
            ret |= set(nodeemcalc05)
        elif len(nodeemcalc05) > partition:
            ret |= set(nodeemcalc05[partition])
    return ret

def GetEMCALPartition( ddlID ):
    if ddlID in nodedcal0:
        return nodedcal0.index(ddlID)
    elif ddlID in nodedcal1:
        return nodedcal1.index(ddlID)
    elif ddlID in nodeemcala05:
        return nodeemcala05.index(ddlID)
    elif ddlID in nodeemcalc05:
        return nodeemcalc05.index(ddlID)
    else:
        return None

def GetEMCALNodeNum( ddlID ):
    if ddlID in nodedcal0:
        return 0
    elif ddlID in nodedcal1:
        return 1
    elif ddlID in nodeemcala05:
        return 2
    elif ddlID in nodeemcalc05:
        return 3
    else:
        return None

nodetrd0 = [1024, 1025, 1026, 1033, 1034, 1035]
nodetrd1 = [1027, 1028, 1029, 1036, 1037, 1038]
nodetrd2 = [1030, 1031, 1032, 1039, 1040, 1041]

def GetTRDDDLList( nodeNum=None, partition=None ):
    ret = set()
    if nodeNum==None or nodeNum==-1:
        if partition==None or partition==-1:
            ret |= set(nodetrd0)
            ret |= set(nodetrd1)
            ret |= set(nodetrd2)
        else:
            if len(nodetrd0) > partition:
                ret |= set(nodetrd0[partition])
            if len(nodetrd1) > partition:
                ret |= set(nodetrd1[partition])
            if len(nodetrd2) > partition:
                ret |= set(nodetrd2[partition])
    elif nodeNum == 0:
        if partition==None or partition==-1:
            ret |= set(nodetrd0)
        elif len(nodetrd0) > partition:
            ret |= set(nodetrd0[partition])
    elif nodeNum == 1:
        if partition==None or partition==-1:
            ret |= set(nodetrd1)
        elif len(nodetrd1) > partition:
            ret |= set(nodetrd1[partition])
    elif nodeNum == 2:
        if partition==None or partition==-1:
            ret |= set(nodetrd2)
        elif len(nodetrd2) > partition:
            ret |= set(nodetrd2[partition])
    return ret

def GetTRDPartition( ddlID ):
    if ddlID in nodetrd0:
        return nodetrd0.index(ddlID)
    elif ddlID in nodetrd1:
        return nodetrd1.index(ddlID)
    elif ddlID in nodetrd2:
        return nodetrd2.index(ddlID)
    else:
        return None

def GetTRDNodeNum( ddlID ):
    if ddlID in nodetrd0:
        return 0
    elif ddlID in nodetrd1:
        return 1
    elif ddlID in nodetrd2:
        return 2
    else:
        return None

nodetofa1 = [1280, 1281, 1284, 1285, 1288, 1289, 1292, 1293, 1296, 1297, 1300, 1301]
nodetofa2 = [1304, 1305, 1308, 1309, 1312, 1313, 1316, 1317, 1320, 1321, 1324, 1325]
nodetofa3 = [1328, 1329, 1332, 1333, 1336, 1337, 1340, 1341, 1344, 1345, 1348, 1349]
nodetofc1 = [1282, 1283, 1286, 1287, 1290, 1291, 1294, 1295, 1298, 1299, 1302, 1303]
nodetofc2 = [1306, 1307, 1310, 1311, 1314, 1315, 1318, 1319, 1322, 1323, 1326, 1327]
nodetofc3 = [1330, 1331, 1334, 1335, 1338, 1339, 1342, 1343, 1346, 1347, 1350, 1351]

def GetTOFDDLList( nodeNum=None, partition=None ):
    ret = set()
    if nodeNum==None or nodeNum==-1:
        if partition==None or partition==-1:
            ret |= set(nodetofa1)
            ret |= set(nodetofa2)
            ret |= set(nodetofa3)
            ret |= set(nodetofc1)
            ret |= set(nodetofc2)
            ret |= set(nodetofc3)
        else:
            if len(nodetofa1) > partition:
                ret |= set(nodetofa1[partition])
            if len(nodetofa2) > partition:
                ret |= set(nodetofa2[partition])
            if len(nodetofa3) > partition:
                ret |= set(nodetofa3[partition])
            if len(nodetofc1) > partition:
                ret |= set(nodetofc1[partition])
            if len(nodetofc2) > partition:
                ret |= set(nodetofc2[partition])
            if len(nodetofc2) > partition:
                ret |= set(nodetofc2[partition])
    elif nodeNum == 0:
        if partition==None or partition==-1:
            ret |= set(nodetofa1)
        elif len(nodetofa1) > partition:
            ret |= set(nodetofa1[partition])
    elif nodeNum == 1:
        if partition==None or partition==-1:
            ret |= set(nodetofa2)
        elif len(nodetofa2) > partition:
            ret |= set(nodetofa2[partition])
    elif nodeNum == 2:
        if partition==None or partition==-1:
            ret |= set(nodetofa3)
        elif len(nodetofa3) > partition:
            ret |= set(nodetofa3[partition])
    elif nodeNum == 3:
        if partition==None or partition==-1:
            ret |= set(nodetofc1)
        elif len(nodetofc1) > partition:
            ret |= set(nodetofc1[partition])
    elif nodeNum == 4:
        if partition==None or partition==-1:
            ret |= set(nodetofc2)
        elif len(nodetofc2) > partition:
            ret |= set(nodetofc2[partition])
    elif nodeNum == 5:
        if partition==None or partition==-1:
            ret |= set(nodetofc3)
        elif len(nodetofc3) > partition:
            ret |= set(nodetofc3[partition])
    return ret

def GetTOFPartition( ddlID ):
    if ddlID in nodetofa1:
        return nodetofa1.index(ddlID)
    elif ddlID in nodetofa2:
        return nodetofa2.index(ddlID)
    elif ddlID in nodetofa3:
        return nodetofa3.index(ddlID)
    elif ddlID in nodetofc1:
        return nodetofc1.index(ddlID)
    elif ddlID in nodetofc2:
        return nodetofc2.index(ddlID)
    elif ddlID in nodetofc3:
        return nodetofc3.index(ddlID)
    else:
        return None

def GetTOFNodeNum( ddlID ):
    if ddlID in nodetofa1:
        return 0
    elif ddlID in nodetofa2:
        return 1
    elif ddlID in nodetofa3:
        return 2
    elif ddlID in nodetofc1:
        return 3
    elif ddlID in nodetofc2:
        return 4
    elif ddlID in nodetofc3:
        return 5
    else:
        return None


def GetDetectorID( ddlID ):
    for det in IDs.keys():
        if ddlID in range( IDs[det]<<8, (IDs[det]<<8)+DDLCounts[ReadoutListVersion][det] ):
            if det=="MUTG" or det=="MUTK":
                return "DIMU"
            return det
    return None

def GetLongDetectorID( ddlID ):
    detID = GetDetectorID( ddlID )
    if LongDetectorIDs.has_key( detID ):
        return LongDetectorIDs[detID]
    if detID=="DIMU":
        return "MUON"+GetSubDetectorID(ddlID)
    return detID

def DetectorID2LongDetectorID( detID ):
    if detID in LongDetectorIDs.keys():
        return LongDetectorIDs[detID]
    if detID in IDs:
        return detID
    if detID=="DIMU":
        return detID
    if detID in InverseLongDetectorIDs.keys():
        return detID # Is already long detector ID
    if detID=="ALICE" or detID=="ALL":
        return "ALICE"
    print "DetectorID2LongDetectorID not found:",detID
    return None

def GetRealDetectorID( ddlID ):
    for det in IDs.keys():
        if ddlID in range( IDs[det]<<8, (IDs[det]<<8)+DDLCounts[ReadoutListVersion][det] ):
            return det
    return None

def GetSubDetectorID( ddlID=None, detID=None, level1Element=None, subDetID=None ):
    if ddlID==None and level1Element==None and subDetID==None:
        raise Exceptions.SCC2Exception( "INTERNAL ERROR: Either ddlID, level1Element, or subDetID have to be specified" )
    if detID==None and level1Element!=None:
        raise Exceptions.SCC2Exception( "INTERNAL ERROR: If level1Element is specified, detID has to be specified as well" )
    if ddlID==None and subDetID!=None:
        return subDetID
    elif detID==None and ddlID!=None:
        detID=GetDetectorID(ddlID)
    if detID==None:
        return None
    elif detID=="TPC":
        if level1Element!=None:
            if level1Element<18:
                return "A%02d" % level1Element
            else:
                return "C%02d" % (level1Element-18)
        if ddlID<(IDs[detID]<<8)+36 or (ddlID>=(IDs[detID]<<8)+72 and ddlID<(IDs[detID]<<8)+72+72):
            side="A"
        else:
            side="C"
        sector = GetTPCSlice(ddlID)
        if sector>=18:
            sector-=18
        return side+"%02d" % sector
    elif gk3LevelDetectorDDLProperties.has_key(detID) or (InverseLongDetectorIDs.has_key(detID) and gk3LevelDetectorDDLProperties.has_key(InverseLongDetectorIDs[detID])):
        if level1Element!=None:
            #print module
            return "%02d" % level1Element
        if gk3LevelDetectorDDLProperties.has_key(detID):
            mod = Get3LevelDetectorLevel1Element(detID, ddlID)
        else:
            mod = Get3LevelDetectorLevel1Element(InverseLongDetectorIDs[detID], ddlID)
        return "%02d" % mod
    #elif detID=="PHOS":
    #    if level1Element!=None:
    #        #print module
    #        return "%02d" % level1Element
    #    mod = GetPHOSModule(ddlID)
    #    return "%02d" % mod
    #elif detID=="EMCAL" or detID=="EMCA":
    #    if level1Element!=None:
    #        #print module
    #        return "%02d" % level1Element
    #    mod = GetEMCALModule(ddlID)
    #    return "%02d" % mod
    elif detID=="DIMU":
        if ddlID in range((IDs["MUTK"]<<8),(IDs["MUTK"]<<8)+DDLCounts[ReadoutListVersion]["MUTK"]):
            return "TRK"
        if ddlID in range((IDs["MUTG"]<<8),(IDs["MUTG"]<<8)+DDLCounts[ReadoutListVersion]["MUTG"]):
            return "TRG"
    else:
        return None
        # detID=GetDetectorID(ddlID)
        # return "%03d" % (ddlID-(IDs[detID]<<8))


def GetSubSubDetectorID( ddlID ):
    if ddlID==None:
        return None
    detID=GetDetectorID(ddlID)
    if detID==None:
        return None
    elif detID=="TPC":
##        if ddlID<IDs[det]<<8+36:
##            offset=0+IDs[det]<<8
##            factor=2
##        elif ddlID<IDs[det]<<8+72:
##            offset=36+IDs[det]<<8
##            factor=2
##        elif ddlID<IDs[det]<<8+72+144:
##            offset=0+IDs[det]<<8
##            factor=4
##        else:
##            offset=72+144+IDs[det]<<8
##            factor=4
        #return "%02d" % ((ddlID-offset)%factor)
        return "%02d" % GetTPCPartition(ddlID)
    elif gk3LevelDetectorDDLProperties.has_key(detID) or (InverseLongDetectorIDs.has_key(detID) and gk3LevelDetectorDDLProperties.has_key(InverseLongDetectorIDs[detID])):
        if gk3LevelDetectorDDLProperties.has_key(detID):
            return "%02d" % Get3LevelDetectorLevel2Element(detID,ddlID)
        else:
            return "%02d" % Get3LevelDetectorLevel2Element(InverseLongDetectorIDs[detID],ddlID)
    #elif detID=="PHOS":
    #    return "%02d" % GetPHOSPartition(ddlID)
    #elif detID=="EMCAL" or detID=="EMCA":
    #    return "%02d" % GetEMCALPartition(ddlID)
    elif detID=="DIMU":
        realDetID=GetRealDetectorID( ddlID )
        return "%03d" % (ddlID-(IDs[realDetID]<<8))
    else:
        detID=GetDetectorID(ddlID)
        return "%03d" % (ddlID-(IDs[detID]<<8))
        # return None

def GetDataSpec( ddlID ):
    detID=GetDetectorID(ddlID)
    if detID==None:
        return
    elif detID=="TPC":
        slice=GetTPCSlice(ddlID)
        partition=GetTPCPartition(ddlID)
        return "0x%02X%02X%02X%02X" % ( slice, slice, partition, partition)
    elif detID=="DIMU":
        subDetID = GetSubDetectorID( ddlID=ddlID )
        if subDetID=="TRG":
            shift = (ddlID-IDs["MUTG"]<<8) + DDLCounts[ReadoutListVersion]["MUTK"]
        else:
            shift = (ddlID-IDs["MUTK"]<<8)
        return "0x%08X" % ( 1 << shift )
    else:
        return "0x%08X" % ( 1 << ( ddlID-(IDs[detID]<<8) ) )

def NameToDDLList( name ):
    if name in IDs.keys():
        return set(range((IDs[name]<<8),(IDs[name]<<8)+DDLCounts[ReadoutListVersion][name]))
    if name in InverseLongDetectorIDs.keys() and InverseLongDetectorIDs[name] in IDs.keys():
        return set(range((IDs[InverseLongDetectorIDs[name]]<<8),(IDs[InverseLongDetectorIDs[name]]<<8)+DDLCounts[ReadoutListVersion][InverseLongDetectorIDs[name]]))
    if name=="DIMU":
        return set(range((IDs["MUTG"]<<8),(IDs["MUTG"]<<8)+DDLCounts[ReadoutListVersion]["MUTG"])) | set(range((IDs["MUTK"]<<8),(IDs["MUTK"]<<8)+DDLCounts[ReadoutListVersion]["MUTK"]))
    if name=="ALL" or name=="ALICE":
        thisSet=set()
        for d in IDs.keys():
            thisSet |= set(range((IDs[d]<<8),(IDs[d]<<8)+DDLCounts[ReadoutListVersion][d]))
        return thisSet
    else:
        return set([int(name)])

def DetectorNumberToDDLList( detIDNr ):
    for name in IDs.keys():
        if IDs[name]==detIDNr:
            return NameToDDLList(name)
    print "Not found:",detIDNr
    return set()

def ReadoutListBit2DDL( wordIndexOrPosString, bitIndex=None ):
    if bitIndex==None and type(wordIndexOrPosString)!=types.StringType:
        raise Exceptions.SCC2Exception( "INTERNAL ERROR: Usage: ReadoutListBit2DDL( bitPosString ) or ReadoutListBit2DDL( wordIndexNumber, bitIndexNumber )" )
    if type(wordIndexOrPosString)==types.StringType:
        words=wordIndexOrPosString.split("-")
        return ReadoutListBit2DDL( int(words[0]), int(words[1]) )
    if wordIndexOrPosString>=len(ReadoutListWord2Detector[ReadoutListVersion]):
        raise Exceptions.SCC2Exception( "Word index %d is larger than readout list size" % wordIndexOrPosString )
    detector = ReadoutListWord2Detector[ReadoutListVersion][wordIndexOrPosString]
    ddlpos = (wordIndexOrPosString-FirstReadoutListWord[ReadoutListVersion][detector])*32+bitIndex
    if ddlpos>=DDLCounts[ReadoutListVersion][detector]:
        raise Exceptions.SCC2Exception( "DDL position %d in word %d is too big for detector %s" % ( bitIndex, wordIndexOrPosString, detector ) )
    return (IDs[detector]<<8)+ddlpos

def ECSReadoutListString2DDLList( readoutList ):
    ddls = set()
    for word in readoutList.split(","):
        if len(word.split(":"))>1:
            ddls.add( ReadoutListBit2DDL( word.split(":")[1] ) )
    return ddls

def GetDetectorPartitionDDL( detID, partNr ):
    if detID in IDs.keys():
        if partNr in range((IDs[detID]<<8),(IDs[detID]<<8)+DDLCounts[ReadoutListVersion][detID]):
            return partNr
        if partNr>=0 and partNr<DDLCounts[ReadoutListVersion][detID]:
            return (IDs[detID]<<8)+partNr
    return None


def GetDetectorHWCoProcDataTypes( ddlID ):
    det = GetDetectorID( ddlID )
    if gDetectorHWCoProcDataTypes.has_key(det):
        return gDetectorHWCoProcDataTypes[det]
    return "DDLPRPRC";



def HasSpecificDetectorHWCoProcDataTypes( ddlID ):
    det = GetDetectorID( ddlID )
    if gDetectorHWCoProcDataTypes.has_key(det):
        return True
    return False

gk3LevelDetectorDDLProperties = {
    "TPC" : { "ddlList" : GetTPCDDLList, "level1Element" : GetTPCSlice, "level2Element" : GetTPCPartition, "level1ElementName" : "Slice", "partitionsPerLevel1Element" : 6, },
    "TRD" : { "ddlList" : GetTRDDDLList, "level1Element" : GetTRDNodeNum, "level2Element" : GetTRDPartition, "level1ElementName" : "LinksPerNode", "partitionsPerLevel1Element" : 6, },
    "SPD" : { "ddlList" : None, "level1Element" : None, "level2Element" : None, "level1ElementName" : "LinksPerNode", "partitionsPerLevel1Element" : 10, },
    "SDD" : { "ddlList" : None, "level1Element" : None, "level2Element" : None, "level1ElementName" : "LinksPerNode", "partitionsPerLevel1Element" : 12, },
    "SSD" : { "ddlList" : None, "level1Element" : None, "level2Element" : None, "level1ElementName" : "LinksPerNode", "partitionsPerLevel1Element" : 8, },
    "PHOS" : { "ddlList" : None, "level1Element" : None, "level2Element" : None, "level1ElementName" : "Module", "partitionsPerLevel1Element" : 4, },
    "EMCA" : { "ddlList" : GetEMCALDDLList, "level1Element" : GetEMCALNodeNum, "level2Element" : GetEMCALPartition, "level1ElementName" : "LinksPerNode", "partitionsPerLevel1Element" : 12, },
    "HMPI" : { "ddlList" : None, "level1Element" : None, "level2Element" : None, "level1ElementName" : "LinksPerNode", "partitionsPerLevel1Element" : 12, },
    "TOF" : { "ddlList" : GetTOFDDLList, "level1Element" : GetTOFNodeNum, "level2Element" : GetTOFPartition, "level1ElementName" : "LinksPerNode", "partitionsPerLevel1Element" : 12, },
}


