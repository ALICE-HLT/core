#!/usr/bin/env python

import ComponentOutputterBase

class SimpleComponentScriptOutputter(ComponentOutputterBase):
    def __init__(self, outputDir ):
        SimpleComponentScriptOutputter.__init__( self, outputDir )

    # Return the file suffix (not necessarily .something can also be something.somethingelse)
    # First part of the filename will be the configuration ID
    def GetOutputFileSuffix( self ):
        return "-SimpleComponentScript.sh"

    # Write out the file as needed
    # outFile is the file object to write to
    # config is the configuration object, if any information from that is needed
    # sources is a list of dictionaties with the following keys: id, ddlid, type, filespecs (if type==file)
    # components is a list of dictionaries with the following keys: id, componentid, componentlibrary, componentargs, addlibraries, addlibrariesbefore, parents
    def WriteOutputFile( self, outfile, config, sources, components ):
        sourceIDs = [ src["id"] for src in sources ]
        compIDs = [ comp["id"] for comp in components ]
        doneComps = {}
        for src in sourceIDs:
            doneComps[src]
        
