#!/bin/bash

LFILE="$(mktemp)"
EFILE="$(mktemp)"

ibqueryerrors | grep -i "rcv\|symbole\|phys\|down" | grep -v "ALL" > "$EFILE"
iblinkinfo > "$LFILE"

cat "$EFILE" | while read text guid text2 port errors; do
    host="$(cat $LFILE | grep $guid -A${port%:} | tail -n1 | cut -d\" -f2)"
    echo $host $errors
done

rm "$LFILE" "$EFILE"
