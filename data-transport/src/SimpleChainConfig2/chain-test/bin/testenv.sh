source ${HOME}/src/alice/bin/env.sh
export TESTBASEDIR="${ALIHLT_DC_SRCDIR}/SimpleChainConfig2/chain-test"
export TESTRUNDIR="/var/tmp/test"
export TESTCONFIGSRCSIR="${TESTBASEDIR}/config-src"
export TESTCONFIGDSTDIR="${TESTBASEDIR}/config"
export TESTDATADIR="/var/tmp/data"
export TESTBASEPORT="30000"
export ALIHLT_HCDBDIR="/var/tmp/data/HCDB"
export PATH="${TESTBASEDIR}/bin:$PATH"
export HEADNODE="ecs0"

export PDSH_CMD="pdsh -f300 -w ^$TESTBASEDIR/nodes-ib.txt"

ulimit -c unlimited

