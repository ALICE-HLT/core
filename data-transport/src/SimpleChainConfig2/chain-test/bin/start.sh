#!/bin/bash

[[ -z ${TESTRUNDIR} ]] && exit 1
[[ -d ${TESTRUNDIR} ]] || exit 1

pushd $TESTRUNDIR || exit 1
rm -f ./*.log

#$PDSH_CMD "${TESTBASEDIR}/bin/clean.sh; rm -rf ${TESTRUNDIR}/*; echo clean"

TaskManager ${TESTCONFIGDSTDIR}/ALICE-master-${HEADNODE}-ib-Master.xml -V 0x78 -nostdout &

popd

CONNECTPARAMETERS=";RUN_NUMBER=169838;HLT_MODE=E"

STARTPARAMETERS=";BEAM_TYPE=AA;RUN_TYPE=PHYSICS${CONNECTPARAMETERS}"
#BEAM_TYPE=%s;HLT_IN_DDL_LIST=%s;HLT_OUT_DDL_LIST=%s;RUN_TYPE=%s;CTP_TRIGGER_CLASS=%s

let start_time=$(date +%s)

cmd=""
prev_cmd=""
time while true; do
    state="$(SlaveController tcpmsg://:48997 tcpmsg://${HEADNODE}:${TESTBASEPORT} query state | grep -v :)"
    prev_cmd="$cmd"
    cmd=""
    case $state in
	slaves_dead)
	    cmd="start_slaves" ;;
	processes_dead)
	    cmd="start${STARTPARAMETERS}" ;;
	ready_local)
	    cmd="connect${CONNECTPARAMETERS}" ;;
	ready)
	    cmd="start_run" ;;
	*)
	    ;;
    esac
    echo "Current state: $state"
    [[ "$state" == "running" ]] && break
    [[ "$state" == "busy" ]] && break
    if [[ -n "$cmd" ]]; then
	if [[ "$cmd" == "$prev_cmd" ]]; then
	    echo "waiting..."
	else
	    echo "Sending command \"$cmd\""
	    SlaveController tcpmsg://:48997 tcpmsg://${HEADNODE}:${TESTBASEPORT} cmd "$cmd" &>/dev/null || \
		echo "Sending command failed..."
	    let current_time=$(date +%s)-start_time
	    log -i "Command ($current_time s): $cmd"
	fi
    else
	echo "waiting..."
    fi
    sleep 1
done
let current_time=$(date +%s)-start_time
log -i "Started ($current_time s) ....."
