#!/bin/bash

export SCC1_CONFIGFILE="$TESTCONFIGDSTDIR/ALICE-SCC1-Generate.xml"

grep "Proc ID" $SCC1_CONFIGFILE | sed -e 's/.*ID=\"//' | sed -e 's/\".*//' | sed -e 's/[0-9]//g' |  sort | uniq -c

