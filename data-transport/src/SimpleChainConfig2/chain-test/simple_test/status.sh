#!/bin/bash

BASEPORT=20000

MAX=3

if [[ -n "$1" ]]; then
    MAX="$1"
fi

for i in $(seq 0 $MAX); do
    let nextport="BASEPORT + i"
    ComponentInterface -address tcpmsg://:9000 -statusdata tcpmsg://$(hostname):$nextport 2>/dev/null
done
