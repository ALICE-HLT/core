#!/bin/bash

#time between events in us
EVENTTIME=100

#log level
LOGLEVEL="0x78"

BASEPORT=20000
LISTENBASEPORT=9000

#leave as is
MAXPENDINGEVENTS=2000
HLTMODE="C"

#doesn't really matter
RUNNUMBER=200000
BEAMTYPE="pp"
RUNTYPE="PHYSICS"

startHLTOut() {
    id="$1"
    files="$2"

    let ddl_in="768 + id"
    let shmid="80 + 2 * id"
    let shmid_out="shmid + 1"
    let shmsize="999994880"
    let fport="BASEPORT + 2 * id "
    let hport="fport + 1"

    FilePublisher  \
	-diskpublisher -eventtime $EVENTTIME  -maxpendingevents $MAXPENDINGEVENTS \
	-datafilelist "$files/TPC_${ddl_in}.ddl" -datatype "HLTOUTPT" -dataorigin HLT -dataspec 0xFFFFFFFF \
	-name FP$id -publisher FP${id}Pub \
	-shm sysv $shmid $shmsize -minblocksize 5506816 \
	-eventmodulopredivisor 4793 -runnr $RUNNUMBER -beamtype $BEAMTYPE \
	-hltmode $HLTMODE -runtype $RUNTYPE -readoutlistversion 3 \
	-control tcpmsg://:${fport}/ -nostdout -V $LOGLEVEL &

    DummyLoad \
	-loadtype none -dataoutput 1 -proctime const 0 \
	-name DL$id -publisher DL${id}Pub -subscribe FP${id}Pub DL${id}Sub \
	-shm sysv $shmid_out 802816 -minblocksize 401408 \
	-eventmodulopredivisor 4793 -runnr $RUNNUMBER -beamtype $BEAMTYPE \
	-hltmode $HLTMODE -runtype $RUNTYPE -readoutlistversion 3 -alicehlt \
	-control tcpmsg://:${hport}/ -nostdout -V $LOGLEVEL &

    sleep 0.5

    ComponentInterface -address tcpmsg://:$listenport -command tcpmsg://$(hostname):${fport}/ Configure
    ComponentInterface -address tcpmsg://:$listenport -command tcpmsg://$(hostname):${hport}/ Configure
    #sleep 0.5
    ComponentInterface -address tcpmsg://:$listenport -command tcpmsg://$(hostname):${fport}/ AcceptSubscriptions 
    ComponentInterface -address tcpmsg://:$listenport -command tcpmsg://$(hostname):${hport}/ Subscribe
    #sleep 0.5
    ComponentInterface -address tcpmsg://:$listenport -command tcpmsg://$(hostname):${fport}/ StartProcessing
    ComponentInterface -address tcpmsg://:$listenport -command tcpmsg://$(hostname):${hport}/ StartProcessing

}

ID=0

if [[ -n "$1" ]]; then
    ID="$1"
fi

BASEDIR="$(dirname $(readlink -f $0))"


startHLTOut $ID "$BASEDIR/raw*"

