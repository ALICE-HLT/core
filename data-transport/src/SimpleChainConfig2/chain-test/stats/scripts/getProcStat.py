#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import subprocess
import re
import socket

def getCommandOutput(command):
  ps = subprocess.Popen(command, stdout=subprocess.PIPE).communicate()[0]
  lines = ps.split('\n')
  ret = []
  sep = re.compile('[\s]+')
  for row in lines:
    ret.append(sep.split(row))
  return ret


def getProcs(extended = 0):
  out = getCommandOutput(['ps', 'ux'])
  procs={}

  for tmp in out[1:]:
    if len(tmp) > 10:
      pid=tmp[1]
      command=tmp[10].rpartition('/')[2]
      if \
        "bash" in command or \
        "sshd" in command or \
        "python" in command or \
        "ps" in command:
        continue
      if len(tmp) > 11 and extended:
        for opt in tmp[11:]:
          command = command + " " + opt
      if not extended and command == "AliRootWrapperSubscriber":
        command = tmp [12]
      procs[pid]=command
  return procs



def getStats(pids, interval):
  command = ['pidstat']
  for pid in pids:
    command.append('-p')
    command.append(pid)
  command.append('-u')
  command.append(interval)
  command.append('1')
  out = getCommandOutput(command)
  stats={}
  for tmp in out:
    if len(tmp) == 9 and tmp[0] == 'Average:' and tmp[1] != 'UID':
      pid = tmp[2]
#      cpu = tmp[5].partition('.')[0]
      cpu = tmp[6]
      stats[pid]=cpu
  return stats


hostname=socket.gethostname()
procs = getProcs()
stats = getStats(procs.keys(), "60")
result=[]
for pid in procs:
  command=procs[pid]
  cpu=""
  if pid in stats:
    cpu=stats[pid]
  else:
    cpu="0"
  print hostname, cpu, command
