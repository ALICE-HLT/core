#!/usr/bin/python

import os
import sys

def readFiles(dir):
    proc_list = {}
    proc_stats = {}
    node_stats = []
    files = os.listdir(dir)
    for file in files:
        if not file.endswith('stats'):
            continue
        path = os.path.join(dir,file)
        f = open(path, 'r')
        s = f.read()
        f.close()
        table = s.split('\n')
        tot_cpu=0.
        host=''
        for line in table:
            entries = line.split(' ')
            if len(entries) < 3:
                continue
            host = entries[0]
            cpu = float(entries[1])
            tot_cpu+=cpu
            name = entries[2]
            if not name in proc_list:
                proc_list[name]=[cpu]
            else:
                proc_list[name].append(cpu)
        node_stats.append((host,tot_cpu))
    for proc in proc_list.keys():
        stats = proc_list[proc]
        no = len(stats)
        tot = sum(stats)
        mean = tot / no
        proc_stats[proc]=[mean, min(stats), max(stats), no, tot]
    return (proc_stats,node_stats)


filter=[
    'SCREEN','dbus-daemon','dbus-launch','ecs_gui','gconfd-2',
    'hlt_SM_proxy','kdeinit4:','knotify4', 'sleep','gnome-pty-helper',
    'emacs','[EventMergerNew]','gdb','HLTAbortButton','root','root.exe',
    'tee', 'kate', 'wish', 'su', 'vi', 'awk', 'grep',
    'konsole','ssh','top','tail','less','dsh','screen','gnome-terminal']

dataflowFilter=[
    'EventGathererNew','EventMergerNew','EventScattererNew','TaskManager',
    'FilePublisher','PublisherBridgeHeadNew','SubscriberBridgeHeadNew', 'Relay'
    ]

if not len(sys.argv) > 1:
    sys.exit(1)
ret=readFiles(sys.argv[1])
stats=ret[0]
nodestats=ret[1]
#skip=False
tot_analysis=0.
tot_framework=0.
proc_analysis=0
proc_framework=0
if len(sys.argv) > 2:
    skip=True
print "##                   \033[1;32mCommand   mean  min  max  multipl.   total\033[0m"
print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
for proc in sorted(stats, key=lambda proc: stats[proc][4]):
    mark_in=''
    mark_out=''
    if stats[proc][2] > 50.:
        mark_in='\033[1;33m'
        mark_out='\033[0m'
    if stats[proc][2] > 80.:
        mark_in='\033[1;31m'
        mark_out='\033[0m'
    if proc not in filter:
        if proc not in dataflowFilter:
            tot_analysis+=stats[proc][4]
            proc_analysis+=stats[proc][3]
        else:
            tot_framework+=stats[proc][4]
            proc_framework+=stats[proc][3]
        print "%28s    %3d  %3d  %s%3d%s     %4d    %5d" % (proc, stats[proc][0], stats[proc][1], mark_in, stats[proc][2], mark_out, stats[proc][3], stats[proc][4])
print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
print "Total # cores  :  %6.2f" % (float(tot_analysis+tot_framework)/100.)
print "Total Analysis :  %6.2f" % (float(tot_analysis)/100.)
print "Total Framework:  %6.2f" % (float(tot_framework)/100.)
print "Ratio (Analysis / Framework): %3.2f" % (float(tot_analysis)/float(tot_framework))
print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
print "Total # procs  :  %6d" % (proc_analysis+proc_framework)
print "Total Analysis :  %6d" % (proc_analysis)
print "Total Framework:  %6d" % (proc_framework)
print "Ratio (Analysis / Framework): %3.2f" % (float(proc_analysis)/float(proc_framework))
print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
print "Total number of nodes: %d" % len(nodestats)
print "nodes with highest CPU usage:"
nodestats.sort(key=lambda entry: entry[1],reverse=True)
j=0
for i in range(10):
    while j<len(nodestats) and not nodestats[j][0].startswith('cn'):
        j+=1
    if j<len(nodestats):
        host=nodestats[j][0]
        cpu=nodestats[j][1]
        ratio=float(cpu)/24.
        print "%s %4d (%2.1f%%)" % (host,cpu,ratio)
        j+=1
