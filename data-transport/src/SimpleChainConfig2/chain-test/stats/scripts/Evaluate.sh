#!/bin/bash

BINDIR="$(dirname $(readlink $0))"
echo $BINDIR

$BINDIR/Evaluate.py $1
echo "===================="
grep "Maximum received event rate" $1/status
grep "Event count currently" $1/status