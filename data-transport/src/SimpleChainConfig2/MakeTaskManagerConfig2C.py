#!/usr/bin/env python
    
import os, string, re, sys, os.path
import SimpleChainConfig1
    
import ConfigReader
import SiteConfig
import SimpleResourceManager
import DDLLocations
import NodeList
import AliasList
import Exceptions
import Detector
    
import time

def MakeTaskManagerConfigMain():
#TIME    times = {}
    
#TIME    times[-1] = time.time()
    #  -infologger ${ALIHLT_DC_DIR}/lib/Linux-x86_64-release/libInfoLoggerLogServer.so
    setup = {
        "BaseSocket" : [25000],
        "BaseShm" : [0x10],
        "UseSSH" : [1],
        "Platform" : [os.uname()[0]+"-"+os.uname()[4]],
        "ProductionRelease" : [1],
        "BridgeShmType" : ["sysv"],
        "Output" : [],
        "RunDir" : [ "/tmp" ],
        "PreStartExec" : [ None ],
        "PostTerminateExec" : [ None ],
        "InterruptTarget" : [],
        "EmptyNodes" : [0],
    #    "ECSInputList" : [],
    #    "ECSOutputList" : [],
        "Quiet" : [0],
    #    "BridgeNodeShmLimit" : [0],
    #    "BridgeShmLimit" : [0],
        "NodeShmLimit" : [],
        "ShmLimit" : [],
        "Debug" : [0],
        "TriggerClassDDLs" : [],
        "EventMergerMinDynamicTimeout" : [0],
        "AutoReadoutListGeneration" : [0],
        "ConfigCreateLate" : [0],
        "ConfigCreateLateConfigFiles" : [0],
        "CompactNodeAssignment" : [0],
        "Macro" : [],
        "NoStrictSOR" : [0],
        "AWSAddLibrary" : [],
        "AWSAddLibraryBefore" : [],
        "PreMapRorcBuffers" : [0],
        }
    
    commandLineSetupMap = {
        "-basesocket" : "BaseSocket",
        "-baseshm" : "BaseShm",
        "-masternode" : "MasterNode",
        "-usessh" : "UseSSH",
        "-taskmandir" : "TaskManagerDir",
        "-frameworkdir" : "FrameworkDir",
        "-platform" : "Platform",
        "-rundir" : "RunDir",
        "-prestartexec" : "PreStartExec",
        "-postterminateexec" : "PostTerminateExec",
        "-eventmergertimeout" : "EventMergerTimeout",
        "-production" : "ProductionRelease",
        "-bridgeshmtype" : "BridgeShmType",
        "-interrupttarget" : "InterruptTarget",
        "-output" : "Output",
        "-ddllist" : "DDLList",
        "-nodelist" : "NodeList",
        "-interrupttarget" : "InterruptTarget",
        "-emptynodes" : "EmptyNodes",
        "-ecsinputlist" : "ECSInputList",
        "-ecsoutputlist" : "ECSOutputList",
        "-quiet" : "Quiet",
        "-maxeventage" : "MaxEventAge",
        "-sysmeslog" : "SysMESLog",
        "-infologger" : "InfoLogger",
        "-eventaccounting" : "EventAccounting",
        "-subscriberretrycount" : "SubscriberRetryCount",
    #    "-bridgeshmlimit" : "BridgeShmLimit",
    #    "-bridgenodeshmlimit" : "BridgeNodeShmLimit",
        "-shmlimit" : "ShmLimit",
        "-nodeshmlimit" : "NodeShmLimit",
        "-debug" : "Debug",
        "-taskmanageroptions" : "TaskManagerOptions",
        "-verbosity" : "Verbosity",
        "-verbosityexclude" : "VerbosityExclude",
        "-rundirpermissions" : "RunDirPermissions",
        "-triggerclassddls" : "TriggerClassDDLs",
        "-ctptriggerclasses" : "CTPTriggerClasses",
        "-runstoptimeout" : "RunStopTimeout",
        "-stoptimeout" : "StopTimeout",
        "-eventmergerdynamictimeouts" : "EventMergerDynamicTimeouts",
        "-eventmergermindynamictimeout" : "EventMergerMinDynamicTimeout",
        "-eventmergerdynamicinputXabling" : "EventMergerDynamicInputXabling",
        "-scattererparam" : "ScattererParam",
        "-autoreadoutlistgeneration" : "AutoReadoutListGeneration",
        "-ecsdetectors" : "ECSDetectors",
        "-eventmodulopredivisor" : "EventModuloPreDivisor",
        "-configcreatelate" : "ConfigCreateLate",
        "-configcreatelateconfigfiles" : "ConfigCreateLateConfigFiles",
        "-premaprorcbuffers" : "PreMapRorcBuffers",
        "-ecsruntype" : "ECSRunType",
        "-ecsbeamtype" : "ECSBeamType",
        "-compactnodeassignment" : "CompactNodeAssignment",
        "-macro" : "Macro",
        "-hostnamealiaslist" : "HostnameAliasList",
        "-readoutlistversion" : "ReadoutListVersion",
        "-maxeventsinchain" : "MaxEventsInChain",
        "-nostrictsor" : "NoStrictSOR",
        "-gahereropts" : "GathererOpts",
        "-scattereropts" : "ScattererOpts",
        "-mergeropts" : "MergerOpts",
        "-filteropts" : "FilterOpts",
        "-sbhopts" : "SBHOpts",
        "-pbhopts" : "PBHOpts",
        "-localdataflowopts" : "LocalDataFlowOpts",
        "-bridgeopts" : "BridgeOpts",
        "-dataflowopts" : "DataFlowOpts",
        "-awsoptions" : "AWSOptions",
        "-awsaddlibrary" : "AWSAddLibrary",
        "-awsaddlibrarybefore" : "AWSAddLibraryBefore",
        "-listfiledir" : "ListFileDir",
        }
    
    noConfigCreatelateSetupOptions = (
        "ConfigCreateLate",
        "ConfigCreateLateConfigFiles",
        "Output",
        )
    
    envSetupMap = {
        "TMCONFIG_MASTERNODE" : "MasterNode",
        "TMCONFIG_MASTERNODES" : "MasterNodes",
        "TMCONFIG_USESSH" : "UseSSH",
        "TMCONFIG_TASKMANDIR" : "TaskManagerDir",
        "TMCONFIG_FRAMEWORKDIR" : "FrameworkDir",
        "TMCONFIG_PLATFORM" : "Platform",
        "TMCONFIG_RUNDIR" : "RunDir",
        "TMCONFIG_PRESTARTEXEC" : "PreStartExec",
        "TMCONFIG_POSTTERMINATEEXEC" : "PostTerminateExec",
        "TMCONFIG_BASESOCKET" : "BaseSocket",
        "TMCONFIG_BASESHM" : "BaseShm",
        "TMCONFIG_EVENTMERGERTIMEOUT" : "EventMergerTimeout",
        "TMCONFIG_PRODUCTION" : "ProductionRelease",
        "TMCONFIG_BRIDGESHMTYPE" : "BridgeShmType",
        "TMCONFIG_OUTPUT" : "Output",
        "TMCONFIG_DDLLIST" : "DDLFile",
        "TMCONFIG_NODELIST" : "NodeFile",
        "TMCONFIG_EMPTYNODES" : "EmptyNodes",
        "TMCONFIG_ECSINPUTLIST" : "ECSInputList",
        "TMCONFIG_ECSOUTPUTLIST" : "ECSOutputList",
        "TMCONFIG_MAXEVENTAGE" : "MaxEventAge",
        "TMCONFIG_SYSMESLOG" : "SysMESLog",
        "TMCONFIG_INFOLOGGER" : "InfoLogger",
        "TMCONFIG_EVENTACCOUNTING" : "EventAccounting",
        "TMCONFIG_SUBSCRIBERRETRYCOUNT" : "SubscriberRetryCount",
    #    "TMCONFIG_BRIDGESHMLIMIT" : "BridgeShmLimit",
    #    "TMCONFIG_BRIDGENODESHMLIMIT" : "BridgeNodeShmLimit",
        "TMCONFIG_SHMLIMIT" : "ShmLimit",
        "TMCONFIG_NODESHMLIMIT" : "NodeShmLimit",
        "TMCONFIG_DEBUG" : "Debug",
        "TMCONFIG_TASKMANAGEROPTIONS" : "TaskManagerOptions",
        "TMCONFIG_VERBOSITY" : "Verbosity",
        "TMCONFIG_VERBOSITYEXCLUDE" : "VerbosityExclude",
        "TMCONFIG_RUNDIRPERMISSIONS" : "RunDirPermissions",
        "TMCONFIG_TRIGGERCLASSDDLS" : "TriggerClassDDLs",
        "TMCONFIG_CTPTRIGGERCLASSES" : "CTPTriggerClasses",
        "TMCONFIG_RUNSTOPTIMEOUT" : "RunStopTimeout",
        "TMCONFIG_STOPTIMEOUT" : "StopTimeout",
        "TMCONFIG_EVENTMERGERDYNAMICTIMEOUTS" : "EventMergerDynamicTimeouts",
        "TMCONFIG_EVENTMERGERMINDYNAMICTIMEOUT" : "EventMergerMinDynamicTimeout",
        "TMCONFIG_EVENTMERGERDYNAMICINPUTXABLING" : "EventMergerDynamicInputXabling",
        "TMCONFIG_SCATTERERPARAM" : "ScattererParam",
        "TMCONFIG_AUTOREADOUTLISTGENERATION" : "AutoReadoutListGeneration",
        "TMCONFIG_ECSDETECTORS" : "ECSDetectors",
        "TMCONFIG_EVENTMODULOPREDIVISOR" : "EventModuloPreDivisor",
        "TMCONFIG_CONFIGCREATELATE" : "ConfigCreateLate",
        "TMCONFIG_CONFIGCREATELATECONFIGFILES" : "ConfigCreateLateConfigFiles",
        "TMCONFIG_ECSRUNTYPE" : "ECSRunType",
        "TMCONFIG_ECSBEAMTYPE" : "ECSBeamType",
        "TMCONFIG_COMPACTNODEASSIGNMENT" : "CompactNodeAssignment",
        "TMCONFIG_MACRO" : "Macro",
        "TMCONFIG_HOSTNAMEALIASLIST" : "HostnameAliasList",
        "TMCONFIG_READOUTLISTVERSION" : "ReadoutListVersion",
        "TMCONFIG_MAXEVENTSINCHAIN" : "MaxEventsInChain",
        "TMCONFIG_NOSTRICTSOR" : "NoStrictSOR",
        "TMCONFIG_GATHEREROPTS" : "GathererOpts",
        "TMCONFIG_SCATTEREROPTS" : "ScattererOpts",
        "TMCONFIG_MERGEROPTS" : "MergerOpts",
        "TMCONFIG_FILTEROPTS" : "FilterOpts",
        "TMCONFIG_SBHOPTS" : "SBHOpts",
        "TMCONFIG_PBHOPTS" : "PBHOpts",
        "TMCONFIG_LOCALDATAFLOWOPTS" : "LocalDataFlowOpts",
        "TMCONFIG_BRIDGEOPTS" : "BridgeOpts",
        "TMCONFIG_DATAFLOWOPTS" : "DataFlowOpts",
        "TMCONFIG_AWSOPTIONS" : "AWSOptions",
        "TMCONFIG_AWSADDLIBRARY" : "AWSAddLibrary",
        "TMCONFIG_AWSADDLIBRARYBEFORE" : "AWSAddLibraryBefore",
        "TMCONFIG_LISTFILEDIR" : "ListFileDir",
        }
    
    
    setupAllowedConfigEntries = (
        "BaseSocket",
        "BaseShm",
        "MasterNode",
        "UseSSH",
        "TaskManagerDir",
        "FrameworkDir",
        "Platform",
        "RunDir",
        "PreStartExec",
        "PostTerminateExec",
        "EventMergerTimeout",
        "EventMergerInputTimeout",
        "ProductionRelease",
        "BridgeShmType",
        "InterruptTarget",
        "Output",
        "InterruptTarget",
        "EmptyNodes",
        "MaxEventAge",
        "SysMESLog",
        "InfoLogger",
        "EventAccounting",
        "SubscriberRetryCount",
    #    "BridgeShmLimit",
    #    "BridgeNodeShmLimit",
        "ShmLimit",
        "NodeShmLimit",
        "TaskManagerOptions",
        "Verbosity",
        "VerbosityExclude",
        "RunDirPermissions",
        "TriggerClassDDLs",
        "CTPTriggerClasses",
        "RunStopTimeout",
        "StopTimeout",
        "EventMergerDynamicTimeouts",
        "EventMergerMinDynamicTimeout",
        "EventMergerDynamicInputXabling",
        "ScattererParam",
        "EventModuloPreDivisor",
        "MaxEventsInChain",
        "NoStrictSOR",
        "GathererOpts",
        "ScattererOpts",
        "MergerOpts",
        "FilterOpts",
        "SBHOpts",
        "PBHOpts",
        "LocalDataFlowOpts",
        "BridgeOpts",
        "DataFlowOpts",
        "AWSOptions",
        "AWSAddLibrary",
        "AWSAddLibraryBefore",
        "PreMapRorcBuffers",
        )
    
    
    setupIntKeys = (
        "BaseSocket",
        "BaseShm",
        "UseSSH",
        "ProductionRelease",
        "EventMergerTimeout",
        "EventMergerInputTimeout",
        "Quiet",
        "MaxEventAge",
        "SysMESLog",
        "SubscriberRetryCount",
    #    "BridgeShmLimit",
    #    "BridgeNodeShmLimit",
        "Debug",
        "Verbosity",
        "VerbosityExclude",
        "EventMergerDynamicTimeouts",
        "EventMergerMinDynamicTimeout",
        "EventMergerDynamicInputXabling",
    #    "ScattererParam",
        "AutoReadoutListGeneration",
        "EventModuloPreDivisor",
        "ConfigCreateLate",
        "ConfigCreateLateConfigFiles",
        "CompactNodeAssignment",
        "ReadoutListVersion",
        "MaxEventsInChain",
        "NoStrictSOR",
        "PreMapRorcBuffers",
        )
    
    configfiles = []
    lateCreationDataFile = None
    lateCreationNode = None
    lateCreationConfigFiles = []
    
    for env in envSetupMap.keys():
        if os.environ.has_key( env ):
            SiteConfig.AddItem( setup, envSetupMap[env], os.environ[env] )
    
    if setup.has_key( "MasterNodes" ):
        for mn in setup["MasterNodes"][-1].split(","):
            SiteConfig.AddItem( setup, "MasterNode", mn )
        del(setup["MasterNodes"])
    
#TIME    times[0] = time.time()
    
            
    
    usagestring = " -masternode <master-node-hostname> (-usessh <ssh-flag (0 or 1)>) -taskmandir <TaskManager-base-directory> -frameworkdir <AliHLT-framework-directory> (-platform <platform>) (-rundir <run-directory>) (-prestartexec <slave-prestart-executable>) (-postterminateexec <slave-post-terminate-executable>) (-basesocket <base-socket>) (-baseshm <base-shm-id>) (-eventmergertimeout <event-merger-timeout-ms>) (-interrupttarget <interrupt-target-address>)* (-production <production-flag>) (-bridgeshmtype [librorc|sysv]) -config <config-filename> (-siteconfig <site-config-filename>) (-output [ TM | SCC1 | GraphViz ]) (-ddllist <ddl-list-xml-file>) (-nodelist <node-list-xml-file>) (-emptynodes) (-ecsinputlist <input-ddl-list-from-ecs>) (-ecsoutputlist <output-ddl-list-from-ecs>) (-quiet <quiet-flag (0 or 1)>) (-maxeventage <maximum-event-age-in-seconds>) (-sysmeslog <SysMES logging flag (0 or 1)>) (-infologger <info-logger-logserver-shared-library-path>) (-subscriberretrycount <retry-count-for-subscribers>) (-bridgeshmlimit <max.shm-size-per-bridge-process>) (-bridgenodeshmlimit <max.shm-size-total-per-node>) (-shmlimit <shm-type>:<max.shm-size-per-process>) (-nodeshmlimit (nodeID:)<shm-type>:<max.shm-size-total-per-node>) (-debug <debug-flag (0 or 1)>) (-rundirpermissions <access permissions for run directory (will also be applied to settable parent directories!)>) (-triggerclassddls <trigger-class>:<ddl-list>) (-ctptriggerclasses <ctp-trigger-classes-string>) (-runstoptimeout <timeout for run stop command in millisecs.>) (-toptimeout <timeout for stop command in millisecs.>) (-eventmergerdynamictimeouts <max. dynamically calculated timeout value>) (-eventmergermindynamictimeout <minimum timeout value allowed for dynamic timeouts, default 0>) (-eventmergerdynamicinputXabling <max. number of tolerated missing events before iput subscriber channel is deactivated>) (-scattererparam <(<runtype>:)scatterer parameter>) (-autoreadlistgeneration <auto readout list flag (0 or 1)>) (-ecsdetectors <detector list from ECS (comma-separated)>) (-eventmodulopredivisor <event-modulo-pre-divisor specifier>) (-configcreatelate <late config creation flag (0 or 1)>) (-configcreatelateconfigfiles <late config creation via config files flag (0 or 1)>) (-latecreationdata <late-creation-data-file>) (-latecreationconfig <late-creation-config-input-file>) (-latecreationnode <late-creation-slave-node>) (-ecsruntype <ecs-runtype-parameter-string>) (-ecsbeamtype <ecs-beamtype-parameter-string>) (-macro <macro-ID>=<macro-string-content>) (-hostnamealiaslist <node-hostname-alias-list-xml-file>) (-readoutlistversion <readout-list-version number (1,2,3)>) (-nostrictsor <no strict sor flag (0 (default), 1)>) (-gahereropts <options for event gatherer components>) (-scattereropts <options for event scatterer components>) (-mergeropts <options for merger event components>) (-filteropts <options for filter components>) (-sbhopts <options for subscriber bridge head components) (-pbhopts <options for publisher bridge head components) (-localdataflowopts <common options for all local data flow components (gatherers/scatterers/mergers (NOT filters))>) (-bridgeopts <common options for all bridge components>) (-dataflowopts <common options for all data flow components (gatherers/scatterers/mergers/filters/subscriber bridge heads/publisher bridge heads)) (-awsoptions <additional command line options for AliRootWrapperSubscriber>) (-awsaddlibrary <additional library to load for AliRootWrapperSusbcriber>) (-awsaddlibrarybefore <additional library to load for AliRootWrapperSubscriber>:<library before which additional library has to be loaded>) (-compactnodeassignment <use-compact-node-assignment flag (1 or 0, default 0)>) (-premaprorcbuffers <1>)"
    
    
    try:
        argList = sys.argv[1:]
        while len(argList)>0:
            if argList[0]=="-siteconfig":
                if len(argList)<2:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -siteconfig." )
                try:
                    SiteConfig.Read( setup, argList[1] )
                except SiteConfig.SiteConfigException,e:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\n"+str(e) )
            argList = argList[2:]
            
    
        argList = sys.argv[1:]
        while len(argList)>0:
            if argList[0]=="-config":
                if len(argList)<2:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -config." )
                configfiles.append(argList[1])
                argList = argList[2:]
            elif argList[0]=="-configlist":
                if len(argList)<2:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -configlist." )
                try:
                    configinfile=open( argList[1], "r" )
                except IOError:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nUnable to open config list input file '"+argList[1]+"'." )
                line=configinfile.readline()
                while line != "":
                    configfiles.append(line[:-1])
                    line=configinfile.readline()
                configinfile.close()
                argList = argList[2:]
            elif argList[0] == "-latecreationdata":
                if len(argList)<2:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing arguments to -latecreationdata." )
                if lateCreationDataFile!=None:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nOnly one occurence of -latecreationdata is allowed." )
                lateCreationDataFile = argList[1]
                argList = argList[2:]
            elif argList[0] == "-latecreationconfig":
                if len(argList)<2:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing arguments to -latecreationdata." )
                if lateCreationDataFile!=None:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nOnly one occurence of -latecreationdata is allowed." )
                lateCreationConfigFiles.append( argList[1] )
                argList = argList[2:]
            elif argList[0] == "-latecreationnode":
                if len(argList)<2:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing arguments to -latecreationnode." )
                if lateCreationNode!=None:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nOnly one occurence of -latecreationdata is allowed." )
                lateCreationNode = argList[1]
                argList = argList[2:]
            elif argList[0]=="-siteconfig":
                if len(argList)<2:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -siteconfig." )
                argList = argList[2:]
                continue
            elif commandLineSetupMap.has_key(argList[0]):
                if len(argList)<2:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to '"+argList[0]+"'." )
                temp = string.Template( argList[1] )
                try:
                    value = temp.substitute( os.environ )
                except KeyError,e:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nUnknown environment variable "+str(e) )
                SiteConfig.AddItem( setup, commandLineSetupMap[argList[0]], value )
                argList = argList[2:]
            else:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nUnknown parameter '"+argList[0]+"'." )
    
        #print "Setup:",setup
    
    
        for i in setupIntKeys:
            if setup.has_key(i):
                try:
                    if type(setup[i][-1])!=type(1):
                        setup[i][-1] = int(setup[i][-1],0)
                except ValueError:
                    cmdLine="UNKNOWN"
                    envEntry="UNKNOWN"
                    for k in commandLineSetupMap.keys():
                        if commandLineSetupMap[k]==i:
                            cmdLine=k
                    for k in envSetupMap.keys():
                        if envSetupMap[k]==i:
                            envEntry=k
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\n'"+i+"' option value '"+setup[i][-1]+"' (command line '"+cmdLine+"', environment variable '"+envEntry+"', xml site config '"+i+"') is not a number." )
    
        if len(configfiles)<=0 and lateCreationDataFile==None and len(lateCreationConfigFiles)<=0:
            raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nAt least one config or late-creation-data file has to be specified." )
        if (len(configfiles)>0 and lateCreationDataFile!=None) or (len(configfiles)>0 and len(lateCreationConfigFiles)>0) or (lateCreationDataFile!=None and len(lateCreationConfigFiles)>0):
            raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nConfig, late-creation-config, or late-creation-data files cannot be specified together." )
        if setup["ConfigCreateLate"][-1] and (lateCreationDataFile!=None or len(lateCreationConfigFiles)>0):
            raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\n-configcreatelate and -latecreationdata cannot be specified together." )
        if lateCreationDataFile==None and not setup.has_key("MasterNode"):
            raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMaster node has to be specified." )
        if lateCreationDataFile==None and setup.has_key("MasterNode") and len(setup["MasterNode"])>2:
            raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nAt most two master node can be specified." )
        if lateCreationDataFile==None and not setup.has_key( "TaskManagerDir"):
            raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nTaskManager main directory has to be specified." )
        if lateCreationDataFile==None and not setup.has_key("FrameworkDir"):
            raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nPubSub Framework main directory has to be specified." )
        if lateCreationDataFile==None and not setup.has_key("ReadoutListVersion"):
            raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nReadout list data format version has to be specified." )
        if lateCreationDataFile==None:
            Detector.ReadoutListVersion = setup["ReadoutListVersion"][-1]
        #if setup["BridgeShmType"][-1]!="sysv" and setup["BridgeShmType"][-1]!="librorc":
        #    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nBridge shm type has to be either sysv or librorc." )
        for bst in setup["BridgeShmType"]:
            toks = bst.split(":")
            if len(toks)==1 and not bst in [ "sysv", "librorc" ]:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nBridge shm type has to be either sysv or librorc." )
            if len(toks)==2 and not toks[1] in [ "sysv", "librorc" ]:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nBridge shm type has to be either sysv or librorc." )
            if len(toks)>2:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nSyntax error in bridge shm type specification (Must be (<node>:)<shm-type> )." )
        for output in setup["Output"]:
            if output!="TM" and output!="SCC1" and output!="GraphViz":
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nOutput type has to be either TM (TaskManager XML files), SCC1 (SimpleChainConfig1 XML input file), or GraphViz." )
        if len(setup["Output"])<=0:
            setup["Output"].append( "TM" )
        procShmLimits = {}
        nodeShmLimits = {}
        nodeSpecificShmLimits = {}
        for k in setup["ShmLimit"]:
            toks = k.split(":")
            if len(toks)!=2:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nSyntax error in per process shared memory limit (Must be <shm-type>:<maximum-shm-segment-size>)." )
            if not toks[0] in [ "sysv", "librorc" ]:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nUnknown shared memory type in per process shared memory limit (Must be 'sysv' or 'librorc')." )
            if toks[1][-1]=="G":
                factor = 1024*1024*1024
                val=toks[1][:-1]
            elif toks[1][-1]=="M":
                factor = 1024*1024
                val=toks[1][:-1]
            elif toks[1][-1]=="k":
                factor = 1024
                val=toks[1][:-1]
            else:
                factor = 1
                val = toks[1]
            try:
                shmSize = int(val)*factor
            except ValueError:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nShared memory size in per process shared memory limit is not a number." )
            procShmLimits[toks[0]] = shmSize
        for k in setup["NodeShmLimit"]:
            toks = k.split(":")
            if len(toks)<2 or len(toks)>3:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nSyntax error in per node shared memory limit (Must be (<node-name>:)<shm-type>:<maximum-shm-segment-size>)." )
            if len(toks)==2:
                typeIndex = 0
                sizeIndex = 1
            elif len(toks)==3:
                typeIndex = 1
                sizeIndex = 2
            if len(toks)==2 and not toks[typeIndex] in [ "sysv", "librorc" ]:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nUnknown shared memory type in per node shared memory limit (Must be 'sysv' or 'librorc')." )
            if toks[sizeIndex][-1]=="G":
                factor = 1024*1024*1024
                val=toks[sizeIndex][:-1]
            elif toks[sizeIndex][-1]=="M":
                factor = 1024*1024
                val=toks[sizeIndex][:-1]
            elif toks[sizeIndex][-1]=="k":
                factor = 1024
                val=toks[sizeIndex][:-1]
            else:
                factor = 1
                val = toks[sizeIndex]
            try:
                shmSize = int(val)*factor
            except ValueError:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nShared memory size in per node shared memory limit is not a number." )
            if len(toks)==2:
                nodeShmLimits[toks[0]] = shmSize
            elif len(toks)==3:
                if not nodeSpecificShmLimits.has_key(toks[0]):
                    nodeSpecificShmLimits[toks[0]] = {}
                nodeSpecificShmLimits[toks[0]][toks[1]] = shmSize
        if setup.has_key("EventModuloPreDivisor") and setup["EventModuloPreDivisor"][-1]==0:
            raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nEvent modulo pre-divisor specifier must be non-zero." )
        if setup.has_key("ECSRunType"):
            ecsRunType = setup["ECSRunType"][-1]
        else:
            ecsRunType = None
        scattererParam = None
        if setup.has_key("ScattererParam"):
            foundSpecificScattererParam=False
            for sp in setup["ScattererParam"]:
                toks = sp.split(":")
                if len(toks)==2 and (ecsRunType==toks[0] or (ecsRunType==None and toks[0]=="")):
                    try:
                        scattererParam = int(toks[1],0)
                        foundSpecificScattererParam = True
                    except ValueError:
                        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nScatterer parameter '"+toks[1]+" (taken from '"+sp+"') cannot be converted to an integer." )
                        
                elif len(toks)==1 and not foundSpecificScattererParam:
                    try:
                        scattererParam = int(sp,0)
                    except ValueError:
                        raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nScatterer parameter '"+sp+"' cannot be converted to an integer." )
                elif not len(toks) in (1,2):
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nScatterer parameter '"+sp+"' does not match allowed format (either '<runtype>:<scatterer-param>' or '<scatterer-param>')." )
    
        macros = {}
        for k in setup["Macro"]:
            toks = k.split("=")
            if len(toks)!=2:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMacro definition '"+k+"' does not match allowed format (<macro-ID>=<macro-value-string>)." )
            if macros.has_key(toks[0]):
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMacro '"+toks[0]+"' defined multiple times." )
            macros[toks[0]] = toks[1]
    
        for k in setup["AWSAddLibraryBefore"]:
            toks = k.split( ":" )
            if len(toks)!=2:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nAliRootWrapperSubscriber additional library before specification '"+k+"' does not match allowed format (<library>:<before-library>)." )
            
#TIME        times[1] = time.time()
    
        # The following hack seems necessary as both os.getcwd and os.path.realpath seem to expand symlinks...
        pwdfile=os.popen("pwd")
        outputDir=pwdfile.read()[:-1]
        pwdfile.close()
        del(pwdfile)
    
        baseDir = os.path.dirname( os.path.realpath( sys.argv[0] ) )
        includePaths = [ outputDir+"/templates", baseDir+"/templates" ]
        scriptDir = baseDir+"/scripts"
        if os.path.basename( baseDir )=="bin":
            includePaths.append(baseDir)
            install_dir = os.path.dirname( baseDir )
            includePaths.append( os.path.join( install_dir, "templates" ))
            scriptDir = os.path.join( install_dir, "scripts" )
    
    
        if lateCreationDataFile==None:
    
#TIME            times[2] = time.time()
    
#TIME            times[3] = time.time()
    
            listFileHandler = SimpleChainConfig1.ListFileHandler()
    
            reader = ConfigReader.ConfigReader( configfiles+lateCreationConfigFiles )
    
            reader.SetAllowedConfigEntries( setupAllowedConfigEntries )
    
            reader.ReadConfigEntries()
            readerConfigEntries = reader.GetConfigEntries()
            for rcek in readerConfigEntries.keys():
                for rce in readerConfigEntries[rcek]:
                    if rce in setupIntKeys and type(rce)!=type(1):
                        try:
                            SiteConfig.AddItem( setup, rcek, int(rce,0) )
                        except ValueError:
                            raise SimpleChainConfig1.ChainConfigException( "Value '"+rce+"' for option '"+rcek+" set in XML config file is not a number." )
                    else:
                        SiteConfig.AddItem( setup, rcek, rce )
    
            for sace in setupAllowedConfigEntries:
                if setup.has_key(sace):
                    reader.SetConfigEntry( sace, setup[sace] )
    
            if not setup.has_key("ListFileDir"):
                setup["ListFileDir"] = [ outputDir ]
    
            if setup["Quiet"][-1]:
                reader.Quiet( setup["Quiet"][-1] )
    
            resman = SimpleResourceManager.SimpleResourceManager()
            resman.SetCompactNodeAssignment( setup["CompactNodeAssignment"][-1] )
    
    
            reader.SetResourceManager( resman )
    
            reader.AddMacros( macros )
    
            if setup.has_key( "AWSOptions" ):
                reader.AddAWSOption( " ".join( setup["AWSOptions"] ) )
            for k in setup["AWSAddLibrary"]:
                reader.AddAWSLibrary( k )
            for k in setup["AWSAddLibraryBefore"]:
                toks = k.split(":")
                reader.AddAWSLibraryBefore( toks[0], toks[1] )
    
            if setup.has_key( "NodeList" ):
                workerNodes = NodeList.ReadXML( setup["NodeList"][-1] )
                resman.SetWorkerNodes( workerNodes )
    
            if setup.has_key( "DDLList" ):
                ddlLocations = DDLLocations.ReadXML( setup["DDLList"][-1] )
                resman.SetDDLLocations( ddlLocations )
                reader.SetDDLLocations( ddlLocations )
    
            for masterNode in setup["MasterNode"]:
                if resman.GetNodePlatform(masterNode)==None:
                    # print resman.fNodes
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nSpecified master node "+masterNode+" is not available in resource manager." )
    
            if setup.has_key("ECSInputList"):
                reader.SetECSInputList(setup["ECSInputList"][-1])
            if setup.has_key("ECSOutputList"):
                reader.SetECSOutputList(setup["ECSOutputList"][-1])
            if setup.has_key("AutoReadoutListGeneration"):
                reader.SetAutoDetectorReadoutList( setup["AutoReadoutListGeneration"][-1] )
            if setup.has_key("ECSDetectors"):
                dets = set()
                for det in setup["ECSDetectors"][-1].split(","):
                    if det[-4:]=="_HLT":
                        det = det[:-4]
                    dets.add( det )
                reader.SetPartipicatingDetectors( dets )
            if ecsRunType!=None:
                reader.SetECSRunType( ecsRunType )
    
            if setup.has_key("HostnameAliasList"):
                aliasList = AliasList.AliasList()
                aliasList.ReadXML( setup["HostnameAliasList"][-1] )
                reader.SetNodeHostnameAliasList( aliasList.GetAliases() )
    
    
            if setup.has_key("MaxEventAge"):
                reader.SetMaxEventAge( setup["MaxEventAge"][-1] )
    
            if setup.has_key( "Verbosity" ):
                verb = setup["Verbosity"][-1]
            else:
                verb=None
            if setup.has_key( "VerbosityExclude" ):
                verbEx = setup["VerbosityExclude"][-1]
            else:
                verbEx=None
            reader.SetVerbosity( verb, verbEx )
    
#TIME            times[4] = time.time()
            config = reader.Read()
#TIME            times[5] = time.time()
    
    
    
#            if lateCreationDataFile==None and len(lateCreationConfigFiles)<=0:
#                reader.OutputGraphFile()
            
            if len(reader.GetDDLList())<=0:
                raise SimpleChainConfig1.ChainConfigException( "Input DDL list obtained from ANDing input DDL list specified in configuration and DDL input list received from ECS (empty intersection set from configuration input DDL list and ECS DDL input list)." )
    
            for masterNode in setup["MasterNode"]:
                if config.GetNode(masterNode)==None:
                    config.AddNode( SimpleChainConfig1.Node( masterNode, resman.GetNodeHostname(masterNode), resman.GetNodePlatform(masterNode) ) )
    
            config.BuildNumericProcessMap()
            newMaster = []
            for masterNode in setup["MasterNode"]:
                newMaster.append(config.fNodeIDMap[masterNode])
            setup["MasterNode"] = newMaster
#TIME            times[6] = time.time()

    
            configMapper = SimpleChainConfig1.ChainConfigMapper( setup["FrameworkDir"][-1], setup["Platform"][-1] )
            if setup["Quiet"][-1]:
                configMapper.Quiet()
            if setup.has_key("SubscriberRetryCount"):
                configMapper.SetMaxSubscriberRetryCount( setup["SubscriberRetryCount"][-1] )
            # configMapper.SetBridgeShmLimits( setup["BridgeShmLimit"][-1], setup["BridgeNodeShmLimit"][-1] )
            # configMapper.SetShmLimits( setup["ShmLimit"][-1], setup["NodeShmLimit"][-1] )
            configMapper.SetShmLimits( procShmLimits, nodeShmLimits, nodeSpecificShmLimits )
    
            configMapper.AddEventTypeCodeMacro( "LASEREVENT", "PUSHCONST C0LSR-E-NOPF-TPC ; PUSHCDHTRIGGERCLASSES ; BAND ; " )
            configMapper.AddEventTypeCodeMacro( "PHYSICSTRIGGER", "PUSHCONST 1 ; PUSHCDHL1TRIGGERTYPE ; BAND ; LNOT ; " )
            configMapper.AddEventTypeCodeMacro( "SOFTWARETRIGGER", "PUSHCONST 1 ; PUSHCDHL1TRIGGERTYPE ; BAND ; " )
            configMapper.AddCTPTriggerClassGroup( "CALIBRATION", "^C0L.*" )
            ctpTCG = reader.GetCTPTriggerClassGroups()
            for gid in ctpTCG.keys():
                configMapper.AddCTPTriggerClassGroup( gid, ctpTCG[gid] )
            configMapper.AddEventTypeCodeMacro( "CALIBRATIONEVENT", "PUSHCONST CALIBRATION ; PUSHCDHTRIGGERCLASSES ; BAND ; " )
    
            for k in setup["TriggerClassDDLs"]:
                if len(k.split(":"))>2:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nWrong format for trigger class DDL specification '"+k+"'." )
                tcStr = k.split(":")[0]
                ddlList = k.split(":")[1].split(",")
                try:
                    triggerClass = int(tcStr,0)
                except ValueError:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nTrigger class '"+tcStr+"' is not a number." )
                bit=1
                count=0
                while bit<=triggerClass and bit!=0:
                    if bit & triggerClass:
                        count += 1
                    bit <<= 1
                if count<=0:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nNo bit set in trigger class '"+tcStr+"'." )
                if count>1:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMore than one bit set in trigger class '"+tcStr+"'." )
                print "TriggerClass:",triggerClass,"DDLList:",ddlList
                configMapper.SetTriggerClassDDLs( triggerClass, ddlList )
            if setup.has_key("CTPTriggerClasses"):
                for tc in setup["CTPTriggerClasses"][-1].split(","):
                    tcs = tc.split(":")
                    tcBit = int(tcs[0],0)
                    tcDetIDs = tcs[2]
                    ddlList = set()
                    for det in tcDetIDs.split("-"):
                        ddlList |= Detector.DetectorNumberToDDLList(int(det,0))
                    configMapper.SetTriggerClassDDLs( 1<<tcBit, ddlList, tcDetIDs )
                    
            if scattererParam!=None:
                configMapper.SetScattererParam( scattererParam )
            if setup.has_key("EventModuloPreDivisor"):
                configMapper.SetEventModuloPreDivisor( setup["EventModuloPreDivisor"][-1] )
            configMapper.SetReadoutListVersion( setup["ReadoutListVersion"][-1] )
    
    
#TIME            times[7] = time.time()
            
            if setup.has_key("BaseSocket"):
                configMapper.SetStartSocket( setup["BaseSocket"][-1] )
            if setup.has_key("BaseShm"):
                configMapper.SetStartShmID( setup["BaseShm"][-1] )
            if setup.has_key("EventMergerTimeout"):
                configMapper.SetEventMergerTimeout( setup["EventMergerTimeout"][-1] )
            if setup.has_key("EventMergerDynamicTimeouts"):
                configMapper.EnableEventMergerDynamicTimeouts( setup["EventMergerMinDynamicTimeout"][-1], setup["EventMergerDynamicTimeouts"][-1] )
            if setup.has_key("EventMergerInputTimeout"):
                configMapper.EnableEventMergerInputTimeout( setup["EventMergerInputTimeout"][-1] )
            if setup.has_key("EventMergerDynamicInputXabling"):
                configMapper.EnableEventMergerDynamicInputXabling( setup["EventMergerDynamicInputXabling"][-1] )
            
            if setup.has_key("ProductionRelease"):
                configMapper.SetProduction( setup["ProductionRelease"][-1] )
            for bst in setup["BridgeShmType"]:
                toks = bst.split(":")
                if len(toks)==1:
                    configMapper.SetPublisherBridgeHeadShmType( bst )
                if len(toks)==2:
                    configMapper.SetPublisherBridgeHeadShmType( toks[1], toks[0] )
            if setup.has_key("SysMESLog") and setup["SysMESLog"][-1]:
                configMapper.SysMESLogging()
            if setup.has_key("InfoLogger"):
                configMapper.InfoLoggerLogging( setup["InfoLogger"][-1] )
    
            if setup.has_key("EventAccounting"):
                configMapper.SetEventAccounting( setup["EventAccounting"][-1] )
    
            if setup.has_key("MaxEventsInChain"):
                configMapper.SetMaxEventsInChain( setup["MaxEventsInChain"][-1] )
            if setup["NoStrictSOR"][-1]:
                configMapper.NoStrictSOR()
    
            if setup.has_key( "GathererOpts" ):
                configMapper.SetGathererCmdLineOptions( " ".join( setup["GathererOpts"] ) )
            if setup.has_key( "ScattererOpts" ):
                configMapper.SetScattererCmdLineOptions( " ".join( setup["ScattererOpts"] ) )
            if setup.has_key( "MergerOpts" ):
                configMapper.SetMergerCmdLineOptions( " ".join( setup["MergerOpts"] ) )
            if setup.has_key( "FilterOpts" ):
                configMapper.SetFilterCmdLineOptions( " ".join( setup["FilterOpts"] ) )
            if setup.has_key( "SBHOpts" ):
                configMapper.SetSubscriberBridgeCmdLineOptions( " ".join( setup["SBHOpts"] ) )
            if setup.has_key( "PBHOpts" ):
                configMapper.SetPublisherBridgeCmdLineOptions( " ".join( setup["PBHOpts"] ) )
            if setup.has_key( "LocalDataFlowOpts" ):
                configMapper.SetLocalDataFlowCmdLineOptions( " ".join( setup["LocalDataFlowOpts"] ) )
            if setup.has_key( "BridgeOpts" ):
                configMapper.SetBridgeCmdLineOptions( " ".join( setup["BridgeOpts"] ) )
    
    
            configMapper.AliceHLT()
            
            master_listen_sockets = {}
            master_slave_control_sockets = {}
            slave_listen_sockets = {}
            slave_slave_control_sockets = {}
            for n in config.GetNodes():
                master_slave_control_sockets[n.GetID()] = configMapper.GetNextSocket( config, n.GetID() )
                master_listen_sockets[n.GetID()] = configMapper.GetNextSocket( config, n.GetID() )
    
            for n in config.GetNodes():
                slave_slave_control_sockets[n.GetID()] = configMapper.GetNextSocket( config, n.GetID() )
                slave_listen_sockets[n.GetID()] = configMapper.GetNextSocket( config, n.GetID() )
                
            for ng in config.GetNodeGroups():
                ng.SetSlaveControlSocket( configMapper.GetNextSocket( config, ng.GetGroupMasterNode() ) )
                ng.SetListenSocket( configMapper.GetNextSocket( config, ng.GetGroupMasterNode() ) )
    
            #socket_list = listFileHandler.InputControlSockets( config, setup["ListFileDir"][-1]+"/", setup["Quiet"][-1] )
            #configMapper.AppendControlProcessSocketList( socket_list )
            #shm_list = listFileHandler.InputShmIDs( config, setup["ListFileDir"][-1]+"/", setup["Quiet"][-1] )
            #configMapper.AppendShmIDs( shm_list )
    
#TIME            times[8] = time.time()
    
            if setup["Output"].count("SCC1")>0:
                outputter6 = SimpleChainConfig1.SCC1Outputter()
                outputter6.GenerateSCC1File( config )
    
            if setup["Output"].count("TM")>0 or setup["Output"].count("GraphViz")>0:
    
#TIME                times[9] = time.time()
                procList = configMapper.CreateProcessList( config )
                #        for p in procList: print p.GetCmd()
#TIME                times[10] = time.time()
#                if lateCreationNode==None:
#                    processListFile = open( config.GetName()+"-ProcessList.out", "w" )
#                    outputterProcessList = SimpleChainConfig1.SimpleProcListOutputter()
#                    outputterProcessList.OutputProcessList( config, procList, processListFile )
#                    processListFile.close()
#TIME                times[11] = time.time()
    
    
            if setup["ConfigCreateLate"][-1] and not setup["ConfigCreateLateConfigFiles"][-1]:
                ## settings = {
                ##     "masterNodes" : setup["MasterNode"],
                ##     "useSSH" : setup["UseSSH"][-1],
                ##     "includePaths" : includePaths,
                ##     "taskManDir" : setup["TaskManagerDir"][-1],
                ##     "frameworkDir" : setup["FrameworkDir"][-1],
                ##     "platform" : setup["Platform"][-1],
                ##     "runDir" : setup["RunDir"][-1],
                ##     "outputDir" : outputDir,
                ##     "prestartExec" : setup["PreStartExec"][-1],
                ##     "postTerminateExec" : setup["PostTerminateExec"][-1],
                ##     "interrupt_targets" : setup["InterruptTarget"],
                ##     "master_slave_control_sockets" : master_slave_control_sockets,
                ##     "master_listen_sockets" : master_listen_sockets,
                ##     "slave_slave_control_sockets" : slave_slave_control_sockets,
                ##     "slave_listen_sockets" : slave_listen_sockets,
                ##     "empty_nodes" : setup["EmptyNodes"][-1],
                ##     "production" : setup["ProductionRelease"][-1],
                ##     #"taskmanageroptions" : setup["TaskManagerOptions"][-1],
                ##     #"rundirpermissions" : setup["RunDirPermissions"][-1],
                ##     #"runstop_timeout" : setup["RunStopTimeout"][-1],
                ##     #"stop_timeout" : setup["StopTimeout"][-1],
                ##     "baseDir" : baseDir,
                ##     }
                ## if setup.has_key("TaskManagerOptions"):
                ##     settings["taskmanageroptions"] = setup["TaskManagerOptions"][-1]
                ## if setup.has_key("RunDirPermissions"):
                ##     settings["rundirpermissions"] = setup["RunDirPermissions"][-1]
                ## if setup.has_key("RunStopTimeout"):
                ##     settings["runstop_timeout"] = setup["RunStopTimeout"][-1]
                ## if setup.has_key("StopTimeout"):
                ##     settings["stop_timeout"] = setup["StopTimeout"][-1]
                settings = setup.copy()
                settings["includePaths"] = includePaths
                settings["outputDir"] = outputDir
                settings["baseDir"] = baseDir
                settings["master_slave_control_sockets"] = master_slave_control_sockets
                settings["master_listen_sockets"] = master_listen_sockets
                settings["slave_slave_control_sockets"] = slave_slave_control_sockets,
                settings["slave_listen_sockets"] = slave_listen_sockets
#TIME                times[12] = time.time()
                SimpleChainConfig1.OutputProcessData( config, configMapper, procList, settings )
#TIME                times[13] = time.time()
            
    
        if lateCreationDataFile!=None:
            procConfig = SimpleChainConfig1.InputProcessData(lateCreationDataFile)
            settings = procConfig["settings"]
            for rcek in settings.keys():
                for rce in settings[rcek]:
                    if rce in setupIntKeys and type(rce)!=type(1):
                        try:
                            SiteConfig.AddItem( setup, rcek, int(rce,0) )
                        except ValueError:
                            raise SimpleChainConfig1.ChainConfigException( "Value '"+rce+"' for option '"+rcek+" set in XML config file is not a number." )
                    else:
                        SiteConfig.AddItem( setup, rcek, rce )
            
            if not setup.has_key(""):
                setup[""] = []
            config,configMapper,procList = procConfig["config"],procConfig["mapper"],procConfig["processes"]
            ## if not setup.has_key("MasterNode"):
            ##     setup["MasterNode"] = []
            ## setup["MasterNode"].append( procConfig["settings"]["masterNodes"] )
            ## setup["UseSSH"].append( procConfig["settings"]["useSSH"] )
            ## if not setup.has_key("TaskManagerDir"):
            ##     setup["TaskManagerDir"] = []
            ## setup["TaskManagerDir"].append( procConfig["settings"]["taskManDir"] )
            ## if not setup.has_key("FrameworkDir"):
            ##     setup["FrameworkDir"] = []
            ## setup["FrameworkDir"].append( procConfig["settings"]["frameworkDir"] )
            ## setup["Platform"].append( procConfig["settings"]["platform"] )
            ## setup["RunDir"].append( procConfig["settings"]["runDir"] )
            ## setup["PreStartExec"].append( procConfig["settings"]["prestartExec"] )
            ## setup["PostTerminateExec"].append( procConfig["settings"]["postTerminateExec"] )
            ## setup["InterruptTarget"] = procConfig["settings"]["interrupt_targets"]
            ## setup["EmptyNodes"].append( procConfig["settings"]["empty_nodes"] )
            ## setup["ProductionRelease"].append( procConfig["settings"]["production"] )
            ## if procConfig["settings"].has_key("taskmanageroptions"):
            ##     if not setup.has_key("TaskManagerOptions"):
            ##         setup["TaskManagerOptions"] = []
            ##     setup["TaskManagerOptions"].append( procConfig["settings"]["taskmanageroptions"] )
            ## if procConfig["settings"].has_key("rundirpermissions"):
            ##     if not setup.has_key("RunDirPermissions"):
            ##         setup["RunDirPermissions"] = []
            ##     setup["RunDirPermissions"].append( procConfig["settings"]["rundirpermissions"] )
            ## if procConfig["settings"].has_key("runstop_timeout"):
            ##     if not setup.has_key("RunStopTimeout"):
            ##         setup["RunStopTimeout"] = []
            ##     setup["RunStopTimeout"].append( procConfig["settings"]["runstop_timeout"] )
            ## if procConfig["settings"].has_key("stop_timeout"):
            ##     if not setup.has_key("StopTimeout"):
            ##         setup["StopTimeout"] = []
            ##     setup["StopTimeout"].append( procConfig["settings"]["stop_timeout"] )
            includePaths = procConfig["settings"]["includePaths"]
            baseDir = procConfig["settings"]["baseDir"]
            outputDir = procConfig["settings"]["outputDir"]
            master_slave_control_sockets = procConfig["settings"]["master_slave_control_sockets"]
            master_listen_sockets = procConfig["settings"]["master_listen_sockets"]
            slave_slave_control_sockets = procConfig["settings"]["slave_slave_control_sockets"]
            slave_listen_sockets = procConfig["settings"]["slave_listen_sockets"]
    
        if setup["PreMapRorcBuffers"][-1] == 1:
            configMapper.SetPreMapRorcBuffers(procList)
            
        if setup["Output"].count("GraphViz")>0 and lateCreationNode==None:
#TIME            times[14] = time.time()
            SimpleChainConfig1.OutputGraphviz( config, configMapper, procList )
#TIME            times[15] = time.time()
    
        lateCreationConfigParameters = ""
        for clsmk in commandLineSetupMap.keys():
            if not setup.has_key(commandLineSetupMap[clsmk]) or (commandLineSetupMap[clsmk] in noConfigCreatelateSetupOptions):
                continue
            for v in setup[commandLineSetupMap[clsmk]]:
                lateCreationConfigParameters += " "+clsmk+" \""+str(v)+"\""
        lateCreationConfigParameters += " -output TM"
        if setup.has_key("ListFileDir"):
            lateCreationConfigParameters += " -listfiledir "+setup["ListFileDir"][-1]
    
        if setup["Output"].count("TM")>0:
    
            
#TIME            times[16] = time.time()
            outputter1 = SimpleChainConfig1.TaskManagerOutputter( setup["MasterNode"], setup["UseSSH"][-1], includePaths, setup["TaskManagerDir"][-1], setup["FrameworkDir"][-1], setup["Platform"][-1], setup["RunDir"][-1], outputDir, setup["PreStartExec"][-1], setup["PostTerminateExec"][-1], scriptDir, setup["InterruptTarget"] )
            outputter1.SetMasterSlaveControlSockets( master_slave_control_sockets )
            outputter1.SetMasterListenSockets( master_listen_sockets )
            outputter1.SetSlaveSlaveControlSockets( slave_slave_control_sockets )
            outputter1.SetSlaveListenSockets( slave_listen_sockets )
            outputter1.SetEmptyNodes( setup["EmptyNodes"][-1] )
            outputter1.SetProduction( setup["ProductionRelease"][-1] )
            if setup["ConfigCreateLate"][-1]:
                if setup["ConfigCreateLateConfigFiles"][-1]:
                    outputter1.ConfigCreateLate( baseDir+"/"+os.path.basename(sys.argv[0]), lateCreationConfigParameters, configfiles )
                else:
                    outputter1.ConfigCreateLate( baseDir+"/"+os.path.basename(sys.argv[0]), "" )
            if setup.has_key("TaskManagerOptions"):
                outputter1.SetTaskManagerOptions( setup["TaskManagerOptions"][-1] )
            if setup.has_key("RunDirPermissions"):
                outputter1.SetRunDirPermissions( setup["RunDirPermissions"][-1] )
            if setup.has_key("RunStopTimeout"):
                outputter1.RunStopTimeout( setup["RunStopTimeout"][-1] )
            if setup.has_key("StopTimeout"):
                outputter1.StopTimeout( setup["StopTimeout"][-1] )
            if lateCreationNode!=None:
                # print lateCreationNode
                outputter1.OnlySlaveNode( lateCreationNode )
            outputter1.GenerateTaskManagerFiles( config, configMapper, procList, 24 )
    
            if lateCreationNode!=None:
                sys.exit(0)
    
#TIME            times[17] = time.time()
    
    #        outputter2 = SimpleChainConfig1.TaskManagerCheckOutputter( setup["MasterNode"], setup["UseSSH"][-1], includePaths, setup["TaskManagerDir"][-1], setup["FrameworkDir"][-1], setup["Platform"][-1], setup["RunDir"][-1], outputDir, setup["PreStartExec"][-1], setup["PostTerminateExec"][-1], scriptDir )
    #        outputter2.SetMasterSlaveControlSockets( master_slave_control_sockets )
    #        outputter2.SetMasterListenSockets( master_listen_sockets )
    #        outputter2.SetSlaveSlaveControlSockets( slave_slave_control_sockets )
    #        outputter2.SetSlaveListenSockets( slave_listen_sockets )
    #        outputter2.SetEmptyNodes( setup["EmptyNodes"][-1] )
    #        outputter2.SetProduction( setup["ProductionRelease"][-1] )
    #        outputter2.GenerateTaskManagerFiles( config, configMapper, procList, 4 )
    
    #        times[18] = time.time()
    
    #        outputter3 = SimpleChainConfig1.TaskManagerStartupScriptOutputter( setup["MasterNode"], setup["UseSSH"][-1], includePaths, setup["TaskManagerDir"][-1], setup["FrameworkDir"][-1], setup["Platform"][-1], setup["RunDir"][-1], outputDir, setup["PreStartExec"][-1], setup["PostTerminateExec"][-1] )
    #        outputter3.SetMasterSlaveControlSockets( master_slave_control_sockets )
    #        outputter3.SetMasterListenSockets( master_listen_sockets )
    #        outputter3.SetSlaveSlaveControlSockets( slave_slave_control_sockets )
    #        outputter3.SetSlaveListenSockets( slave_listen_sockets )
    #        outputter3.SetEmptyNodes( setup["EmptyNodes"][-1] )
    #        outputter3.SetProduction( setup["ProductionRelease"][-1] )
    #        if setup.has_key("TaskManagerOptions"):
    #            outputter3.SetTaskManagerOptions( setup["TaskManagerOptions"][-1] )
    #        outputter3.GenerateStartupScripts( config, configMapper, procList)
    
    #        times[19] = time.time()
    
    #        outputter4 = SimpleChainConfig1.TaskManagerKillScriptOutputter( setup["MasterNode"], setup["UseSSH"][-1], includePaths, setup["TaskManagerDir"][-1], setup["FrameworkDir"][-1], setup["Platform"][-1], setup["RunDir"][-1], outputDir, setup["PreStartExec"][-1], setup["PostTerminateExec"][-1], scriptDir )
    #        outputter4.SetMasterSlaveControlSockets( master_slave_control_sockets )
    #        outputter4.SetMasterListenSockets( master_listen_sockets )
    #        outputter4.SetSlaveSlaveControlSockets( slave_slave_control_sockets )
    #        outputter4.SetSlaveListenSockets( slave_listen_sockets )
    #        outputter4.SetEmptyNodes( setup["EmptyNodes"][-1] )
    #        outputter4.SetProduction( setup["ProductionRelease"][-1] )
    #        outputter4.GenerateTaskManagerFiles( config, configMapper, procList, 4 )
    
    #        times[20] = time.time()
    
    #        outputter5 = SimpleChainConfig1.TaskManagerCleanScriptOutputter( setup["MasterNode"], setup["UseSSH"][-1], includePaths, setup["TaskManagerDir"][-1], setup["FrameworkDir"][-1], setup["Platform"][-1], setup["RunDir"][-1], outputDir, setup["PreStartExec"][-1], setup["PostTerminateExec"][-1], scriptDir )
    #        outputter5.SetMasterSlaveControlSockets( master_slave_control_sockets )
    #        outputter5.SetMasterListenSockets( master_listen_sockets )
    #        outputter5.SetSlaveSlaveControlSockets( slave_slave_control_sockets )
    #        outputter5.SetSlaveListenSockets( slave_listen_sockets )
    #        outputter5.SetEmptyNodes( setup["EmptyNodes"][-1] )
    #        outputter5.SetProduction( setup["ProductionRelease"][-1] )
    #        outputter5.GenerateTaskManagerFiles( config, configMapper, procList, 4 )

    #        times[21] = time.time()

        listFileHandler.OutputControlSockets( config, configMapper )
        listFileHandler.OutputShmIDs( config, configMapper )


    except (SimpleChainConfig1.ChainConfigException,ConfigReader.ConfigException, Exceptions.SCC2Exception), e:
        sys.stderr.write(  "Exception caught: "+e.fValue+"\n" )
        if setup["Debug"][-1]:
            raise
        sys.exit( -1 )


#    if setup["Debug"][-1]:
#TIME        ndxs = times.keys()
#        ndxs.sort()
#        #for i in range(0,21):
#        for i in ndxs:
#            if times.has_key(i) and times.has_key(i-1):
#                print "dt%d: %f" % (i, times[i]-times[i-1])
