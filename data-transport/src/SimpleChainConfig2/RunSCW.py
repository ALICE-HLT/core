#!/usr/bin/env python

from ConfigReader import ConfigReader, ConfigException
import SiteConfig
import ResourceManagerInterface
import DDLLocations
import NodeList
import AliasList
import Exceptions
import Detector
import SimpleChainConfig1

import time, glob, re, os, string, sys

class SCWRunnerException:
    def __init__( self, myType, message ):
        self.fType = myType
        self.fMessage = message

    def GetType(self):
        return self.fType

    def GetMessage(self):
        return self.fMessage

    def __str__(self):
        return str(self.fType)+" : "+str(self.fMessage)


    
class DummyResourceManager(ResourceManagerInterface.ResourceManagerInterface):
    def __init__(self,dummyName):
        self.fDummyName = dummyName
        

    #def MakeNodeList( dummyName ):
    #    nodeList = {}
    #    platform = os.uname()[0]+"-"+os.uname()[4]
    #    workerID = dummyName+"-worker"
    #    nodeList[workerID] = { "cpus" : 100, "platform" : platform, "hostname" : workerID, "attributes" : {} }
    #    masterID = dummyName+"-master"
    #    nodeList[masterID] = { "cpus" : 100, "platform" : platform, "hostname" : masterID, "attributes" : { "Master" : 1 }, "master" : 1 }
    #    return nodeList

    def GetWorkerNodeName(self):
        return self.fDummyName+"-worker"

    def MakeDDLList( self ):
        ddls = Detector.NameToDDLList( "ALICE" )
        ddlLoc = {}
        workerID = self.GetWorkerNodeName()
        for ddlID in ddls:
            ddlLoc[ddlID] = { "node" : workerID, "rorcid" : 0, "channel" : 0 }
        return ddlLoc

    def RequestNodes(self, procMult, procID, group, connectionCnt, ddlID=None, slice=None, detID=None, existingNodeList=[], lowLoad=False, forceFEP = False, parentNodes=[], placeExistingNodes=True, instanceWeight=1.0 ):
        return [ self.GetWorkerNodeName() ] * procMult
    
    def GetNodesWithAttributes( self, attributeList ):
        if attributeList == ["Master" ]:
            return self.fDummyName+"-master"
        return []

    def RestrictNodesWithAttributes( self, attributeList ):
        pass

    def RestrictDDLs( self, ddlList ):
        pass

    def PlaceProcess( self, procID, existingNodeList, group, connectionCnt=0, lowLoad=False ):
        pass

    def ReleaseNodes(self, procID, existingNodeList ):
        pass

    def GetNodePlatform( self, node ):
        return os.uname()[0]+"-"+os.uname()[4]

    def GetNodeHostname( self, node ):
        return node

    def GetDDLLocation( self, ddl ):
        return self.GetWorkerNodeName()

    def GetNodesForGroup( self, group ):
        return [self.GetWorkerNodeName()]

    def GetUtilizationList( self ):
        return 100



class SCWRunner(ConfigReader):
    def __init__( self, filenames ):
        ConfigReader.__init__( self, filenames )
        self.fDataSourceTypes = ( "DDL", "HWCoProc" )
        self.fInputDirs = {}
        for d in self.fDataSourceTypes:
            self.fInputDirs[d] = []
        self.fKeepIntermediateFiles = True
        self.fProcessIDs = {}
        self.fOutputParentIDs = set()
        #self.fAvailableFiles = []
        self.fAvailableFiles = {}
        self.fBlockLinePattern = re.compile( "Block (\d+) - size: (\d+) - type: '(........)' - origin: '(....)'- spec: (0x[0-9A-F]+)" )
        self.fNameLinePattern = re.compile( "   Output filename: (.*)" )
        self.fEventDoneDataLinePattern = re.compile( "   Event Done Data Output filename: (.*)" )
        self.fEventTriggerDataLinePattern = re.compile( "   Event Trigger Output filename: (.*)" )
        self.fEventStartPattern = re.compile( "Event (\d+): Time needed per event" )
        self.fRunNr = None
        self.fRemoveIntermediateFiles = False
        self.fRemoveOutputFiles = False
        self.fLoops = 1
        self.fProcessTolerant = True
        self.fECSParameters = {}
        self.fECSString = ""
        self.fEventListPrototypeLen = 0
        self.fSourceIDs = set()
        self.fSCWDummyMode = False
        self.fProtocolFiles = []

        print "TODO: Check similar lengths for DDL and HWCoProc if both used"

    def GetEventListPrototype(self):
        l = []
        for i in range(self.fEventListPrototypeLen*self.fLoops):
            l.append( [] )
        return l

    def SetSCWDummyMode( self, scwMode ):
        self.fSCWDummyMode = scwMode

    def SetLoops( self, loops ):
        self.fLoops = loops

    def AddProtocolFile( self, protFile ):
        self.fProtocolFiles.append( protFile )

    def WriteProtocol( self, line ):
        for f in self.fProtocolFiles:
            f.write( line )

    def GetProtocolFiles(self):
        return self.fProtocolFiles[:]

    def SetECSParameters( self, ecsString ):
        self.fECSString = ecsString
        params = ecsString.split( ";" )
        for param in params:
            toks = param.split("=")
            if len(toks)!=2:
                raise SCWRunnerException( "Incorrect ECS Parameter String", "Incorrect ECS parameter string, more than one '=' in '"+param+"'" )
            self.fECSParameters[toks[0]] = toks[1]
        self.SetECSInputList( self.fECSParameters["HLT_IN_DDL_LIST"] )
        self.SetECSOutputList( self.fECSParameters["HLT_OUT_DDL_LIST"] )
        self.SetECSRunType( self.fECSParameters["RUN_TYPE"] )
        dets = set()
        for det in self.fECSParameters["DETECTOR_LIST"].split(","):
            if det[-4:]=="_HLT":
                det = det[:-4]
            dets.add( det )
        self.SetPartipicatingDetectors( dets )
        try:
            self.fRunNr = int(self.fECSParameters["RUN_NUMBER"],0)
        except ValueError:
            raise SCWRunnerException( "Invalid Run Number", "Run number '"+self.fECSParameters["RUN_NUMBER"]+"' cannot be converted to an integer" )

    def CheckForNode( self, nodeID, nodeType ):
        return
                
    def ReadSources( self ):
        # Read in source tags
        retval = ConfigReader.ReadSources( self )
        # Expand file globbing in specified source parameters (done in FilePublisher for online chains)
        sourceFileDirs = { "DDL" : self.fSourceFileDirs, "HWCoProc" : self.fSourceHWCoProcDirs }
        maxLen = None
        for dataSourceType in self.fDataSourceTypes:
            for d in sourceFileDirs[dataSourceType]:
                self.fInputDirs[dataSourceType] += glob.glob( d )
            if maxLen==None:
                maxLen = len(self.fInputDirs[dataSourceType])
            else:
                if maxLen!=len(self.fInputDirs[dataSourceType]):
                    print "Warning, length %d of event list for %s is different from previously set event list length of %d" % ( len(self.fInputDirs[dataSourceType]), dataSourceType, maxLen )
                if maxLen<len(self.fInputDirs[dataSourceType]):
                    maxLen=len(self.fInputDirs[dataSourceType])
        if maxLen<=0 or maxLen==None:
            raise SCWRunnerException( "No Files", "No files/directories specified or found for processing." )
        self.fEventListPrototypeLen = maxLen
        return retval
        
    def MakeDDLSource( self, ddlID ):
        ConfigReader.MakeDDLSource( self, ddlID )
        activateKeys = { "DDL" : "enable", "HWCoProc" : "hwcoproc" }
        generate = {}
        for dataSourceType in ( "DDL", "HWCoProc" ):
            if self.fRORCs.has_key( "%d.%s" % ( ddlID, activateKeys[dataSourceType] ) ):
                generate[dataSourceType] = self.fRORCs["%d.%s" % ( ddlID, activateKeys[dataSourceType] )]
            else:
                generate[dataSourceType] = self.fRORCs["GENERIC.%s" % ( activateKeys[dataSourceType] ) ]
        explicitDataTypeKeys = { "DDL" : "datatype", "HWCoProc" : "hwcoprocdatatype" }
        explicitDataTypes = { "DDL" : "DDL_RAW", "HWCoProc" : Detector.GetDetectorHWCoProcDataTypes(ddlID) }

        for dataSourceType in self.fDataSourceTypes:
            dataType=None
            if self.fRORCs.has_key( "%d.%s" % ( ddlID, explicitDataTypeKeys[dataSourceType] ) ):
                dataType = self.fRORCs["%d.%s" % ( ddlID,explicitDataTypeKeys[dataSourceType] ) ]
            if dataType==None and self.fRORCs.has_key( "GENERIC.%s" % explicitDataTypeKeys[dataSourceType] ):
                dataType = self.fRORCs["GENERIC.%s" % explicitDataTypeKeys[dataSourceType]]
            if dataType==None:
                dataType = explicitDataTypes[dataSourceType]
            dataOrigin = Detector.GetDetectorID(ddlID)
            dataSpec = Detector.GetDataSpec(ddlID)
            if not generate[dataSourceType]:
                continue
            ID=self.MakeSourceID( ddlID, dataSourceType )

            self.fSourceIDs.add( ID )

            # if len(self.fAvailableFiles[ID]) < len(self.fInputDirs[dataSourceType]):
            #     self.fAvailableFiles[ID] += [[]] * ( len(self.fInputDirs[dataSourceType]) - len(self.fAvailableFiles) )

            self.fAvailableFiles[ID] = self.GetEventListPrototype()

            ndx=0
            for i in range(self.fLoops):
                for d in self.fInputDirs[dataSourceType]:
                    #self.fAvailableFiles[ID][ndx].append( ( "data", d+"/"+Detector.GetLongDetectorID(ddlID)+"_%d.ddl " % ddlID, dataType, dataOrigin, dataSpec, ddlID ) )
                    self.fAvailableFiles[ID][ndx].append( ( "ddldata", d+"/"+Detector.GetLongDetectorID(ddlID)+"_%d.ddl " % ddlID, dataType, dataOrigin, dataSpec, ddlID ) )
                    self.fAvailableFiles[ID][ndx].append( ( "triggerdatafrominput", ) )
                    ndx += 1


    def MakeProcess( self, process, parentProcessLists, ddlID=None, detID=None, subDetID=None, level1Element=None, myNode=None, myNodeIndex=None, groupProcIDs=None, processGroup=None, processGroupDict=None ):
        ConfigReader.MakeProcess( self, process, parentProcessLists, ddlID, detID, subDetID, level1Element, myNode, myNodeIndex, groupProcIDs, processGroup, processGroupDict )
        isGroup = (process["processorgroup"]=="group")
        isProcess = not isGroup
        
        procType="prc"
        ID=self.MakeID( process["id"], ddlID, detID, subDetID, level1Element )
        if processGroup!=None:
            groupPrefix = processGroup.GetProcessPrefix( myNodeIndex )
            ID = groupPrefix+process["id"]

        parents,addParents = self.GetActualParentList( process )

        if len(parents)<=0:
            return None
        if not self.CanTraceToSource( self.GetActualParentList(process)[0][:], parentProcessLists, ddlID=ddlID, detID=detID, subDetID=subDetID, level1Element=level1Element, myNode=myNode, myNodeIndex=myNodeIndex, groupProcIDs=groupProcIDs, processGroup=processGroup, processGroupDict=processGroupDict ):
            if not self.fQuiet:
                procID = ""
                if detID!=None:
                    procID += detID+"/"
                if subDetID!=None:
                    procID += subDetID+"/"
                if level1Element!=None:
                    procID += "%d/" % level1Element
                procID += process["id"]
                if ddlID!=None:
                    procID+= ":(DDL %d)" % ddlID
                print "Warning: Process "+procID+" cannot be traced down to a DDL data source."
            return

        if process.has_key("parentfilters"):
            parentFilters = process["parentfilters"]
        else:
            parentFilters = {}
        if process.has_key("optparentfilters") and len(addParents)>0:
            for par in addParents:
                if process["optparentfilters"].has_key(par):
                    parentFilters[par] = process["optparentfilters"][par]
                
        if process.has_key("onlyoneparentfromgroup"):
            onlyOneParentFromGroup = process["onlyoneparentfromgroup"][:]
        else:
            onlyOneParentFromGroup = []
        if process.has_key("optonlyoneparentfromgroup"):
            onlyOneParentFromGroup += list( addParents & set(process["optonlyoneparentfromgroup"]) )

        parentIDs,parentFilterIDs=self.MakeParentList(parents, parentProcessLists, ddlID, detID, subDetID, level1Element, parentFilters=parentFilters, onlyOneParentFromGroup=onlyOneParentFromGroup, myNode=myNode, myNodeIndex=myNodeIndex, groupProcIDs=groupProcIDs, processGroup=processGroup, processGroupDict=processGroupDict  )

        proc = {
            "id" : ID,
            "process" : process,
            "parents" : parentIDs,
            "addparents" : addParents,
            "processed" : False,
            "outputfiles" : [],
            }
        self.fProcessIDs[ID] = proc

        self.fAvailableFiles[ID] = self.GetEventListPrototype()
        if process["id"]=="HOF":
            self.fOutputParentIDs |= set(parentIDs)

    def RunComponent( self, proc, inputFiles, outputPrefix ):
        print "Running component",proc["id"]
        process = proc["process"]
        procType = process["type"]
        if procType=="comp":
            scwParams = " -componentid "+process["component"]
            if process.has_key( "options" ):
                scwParams += " -componentargs \""+process["options"]+"\""
            if process.has_key( "library" ):
                scwParams += " -componentlibrary "+process["library"]
            for al in self.fAWSAddLibraries:
                if self.fAWSAddLibrariesBefore.has_key( al ):
                    scwParams += " -addlibrarybefore "+al+" "+self.fAWSAddLibrariesBefore[al]
                else:
                    scwParams += " -addlibrary "+al
            if process.has_key( "addlibraries" ):
                for al in process["addlibraries"]:
                    if process["beforelibraries"].has_key(al):
                        scwParams += " -addlibrarybefore "+al+" "+process["beforelibraries"][al]
                    else:
                        scwParams += " -addlibrary "+al
            scwParams += " -V 0x%0X" % ( self.fVerbosity & ~self.fVerbosityExclude )
            if self.fSCWDummyMode:
                scwParams += " -dummymode"
            commandFileName = proc["id"]+".commandfile"
            commandFile = open( commandFileName, "w" )
            commandFile.write( "sorevent\n" )
            commandFile.write( "outfile "+outputPrefix+proc["id"]+"-sor-event-Block-%N.eventdata\n" )
            commandFile.write( "eventdoneoutfile "+outputPrefix+proc["id"]+"-sor-event.eventdonedata\n" )
            commandFile.write( "eventtriggeroutfile "+outputPrefix+proc["id"]+"-sor-event.eventtriggerdata\n" )
            commandFile.write( "nextevent\n" )
            eventIndex=0
            for event in inputFiles:
                for inputFile in event:
                    # ( d+"/"+Detector.GetLongDetectorID(ddlID)+"_%d.ddl " % ddlID, dataType, dataOrigin, dataSpec )
                    if inputFile[0] in ( "data", "ddldata" ):
                        commandFile.write( "datatype "+inputFile[2]+" "+inputFile[3]+"\n" )
                        commandFile.write( "dataspec "+inputFile[4]+"\n" )
                        if inputFile[0]=="ddldata":
                            commandFile.write( "ddlid %u\n" % inputFile[5] )
                        commandFile.write( "infile "+inputFile[1]+"\n" )
                    elif inputFile[0]=="donedata":
                        pass
                    elif inputFile[0]=="triggerdata":
                        commandFile.write( "eventtriggerinfile "+inputFile[1]+"\n" )
                    elif inputFile[0]=="triggerdatafrominput":
                        commandFile.write( "eventtriggerfrominfile\n" )
                    else:
                        print "Unknown input file",str(inputFile)
                commandFile.write( "outfile "+outputPrefix+proc["id"]+("-event-%012Lu" % eventIndex)+"-Block-%N.eventdata\n" )
                commandFile.write( "eventdoneoutfile "+outputPrefix+proc["id"]+("-event-%012Lu" % eventIndex)+".eventdonedata\n" )
                commandFile.write( "eventtriggeroutfile "+outputPrefix+proc["id"]+("-event-%012Lu" % eventIndex)+".eventtriggerdata\n" )
                commandFile.write( "nextevent\n" )
                eventIndex += 1
            commandFile.write( "eorevent\n" )
            commandFile.write( "outfile "+outputPrefix+proc["id"]+"-eor-event-Block-%N.eventdata\n" )
            commandFile.write( "eventdoneoutfile "+outputPrefix+proc["id"]+"-eor-event.eventdonedata\n" )
            commandFile.write( "eventtriggeroutfile "+outputPrefix+proc["id"]+"-eor-event.eventtriggerdata\n" )
            commandFile.write( "nextevent\n" )
            commandFile.close()
            ioParams = " -commandfile "+commandFileName
            # -outfile 
            scwCommand = "SimpleComponentWrapper -runnr "+self.fECSParameters["RUN_NUMBER"]+" -hltmode "+self.fECSParameters["HLT_MODE"]+" -beamtype "+self.fECSParameters["BEAM_TYPE"]+" -runtype "+self.fECSParameters["RUN_TYPE"]+" -ecsparameters '"+self.fECSString+"' "+scwParams+" "+ioParams+" 2>&1"
            if not self.fQuiet:
                print "Running command '"+scwCommand+"'"
            processOutput = []
            self.WriteProtocol( scwCommand+"\n" )
            processFile = os.popen( scwCommand )
            processOutput = processFile.readlines()
            processFile.close()
            if not self.fQuiet:
                print "Command output: "
                for ol in processOutput:
                    prefix="    "
                    if ol[-1]=="\n":
                        print prefix+ol,
                    else:
                        print prefix+ol
            filename=None
            dataType=None
            dataOrigin=None
            dataSpec=None
            outputFiles = []
            # print "WARNING"
            # outputFiles = [  [ ( "data", proc["id"]+"-DummyOutput.data", "DUMYDATA", "HLT", "0" ) ] ] * len(inputFiles)
            for line in processOutput:
                match = self.fEventStartPattern.search( line )
                if match!=None:
                    outputFiles.append( [] )
                match = self.fBlockLinePattern.search( line )
                if match!=None:
                    dataType = match.group(3)
                    dataOrigin = match.group(4)
                    dataSpec = match.group(5)
                    # print prefix+"datatype "+dataType+" "+dataOrigin+separator+prefix"dataspec "+dataSpec
                match = self.fNameLinePattern.search( line )
                if match != None:
                    filename = match.group(1)
                if filename!=None and dataType!=None and dataOrigin!=None and dataSpec!=None:
                    if not self.fQuiet:
                        print "Found component output file:",filename,dataType,dataOrigin,dataSpec
                    outputFiles[-1].append( ( "data", filename, dataType, dataOrigin, dataSpec ) )
                    filename=None
                    dataType=None
                    dataOrigin=None
                    dataSpec=None
                match = self.fEventDoneDataLinePattern.search( line )
                if match != None:
                    outputFiles[-1].append( ( "donedata", match.group(1) ) )
                match = self.fEventTriggerDataLinePattern.search( line )
                if match != None:
                    outputFiles[-1].append( ( "triggerdata", match.group(1) ) )
            if self.fRemoveIntermediateFiles:
                os.unlink( commandFileName )
            return outputFiles[1:-1] # Cut of SOR/EOR
        elif procType=="proc":
            commands = process["command"]
            for ck in commands.keys():
                command = commands[ck]
                binary = os.path.basename(command).split(" ")[0]
                # if not self.fQuiet:
                #    print "Process binary '"+binary+"'"
                if binary=="Relay":
                    return inputFiles
                if binary in ( "HLTOutFormatter", "CRORCOutWriterSubscriber" ):
                    return []
                # No process command/eecutable that I know about
                if self.fProcessTolerant:
                    print "Cannot run process",proc["id"],"do not know how to handle binary", binary
                    return []
                else:
                    raise SCWRunnerException( "Unknown Process", "Process command '"+command+"' not known, do not know how to proceed, giving up..." )
        pass

        
    def Run( self ):
        if self.fRunNr==None:
            raise SCWRunnerException( "Missing run number", "Run number must be set" )
        
        producedFiles = set()
        producedOutputFiles = set()
        totalComponentsRan = 0
        componentsRan = 1
        availableComponents = self.fSourceIDs.copy()
        procKeys = self.fProcessIDs.keys()
        procKeys.sort()
        while componentsRan>0:
            componentsRan = 0
            for procID in procKeys:
                if procID in availableComponents:
                    continue
                proc = self.fProcessIDs[procID]
                parents = proc["parents"]
                if len(set(parents)-availableComponents)<=0:
                    # All parents already processed
                    componentsRan += 1
                    totalComponentsRan += 1
                    inputFiles = self.GetEventListPrototype()
                    for par in parents:
                        eventLen = len(self.fAvailableFiles[par])
                        for eventIndex in range(eventLen):
                            inputFiles[eventIndex] += self.fAvailableFiles[par][eventIndex]
                    outputFiles = self.RunComponent( proc, inputFiles, self.fConfig.GetName()+"-" )
                    print self.fAvailableFiles[procID]
                    print outputFiles
                    for eventIndex in range(len(outputFiles)):
                        print eventIndex,len(self.fAvailableFiles[procID]),len(outputFiles)
                        self.fAvailableFiles[procID][eventIndex] += outputFiles[eventIndex]
                    for outSet in outputFiles:
                        producedFiles |= set( outSet )
                    if procID in self.fOutputParentIDs:
                        for outSet in outputFiles:
                            producedOutputFiles |= set( outSet )
                            
                    availableComponents.add( procID )
                    if self.fRemoveIntermediateFiles or self.fRemoveOutputFiles:
                        missingProcs = set(self.fProcessIDs.keys())-availableComponents
                        missingParents = set()
                        for mp in missingProcs:
                            missingParents |= set(self.fProcessIDs[mp]["parents"])
                        finishedIDs = (availableComponents-self.fSourceIDs) - missingParents
                        # IDs of those components whose output is not needed anymore and whose files can be deleted...
                        deleteFiles = set()
                        if self.fRemoveIntermediateFiles:
                            for fp in finishedIDs - self.fOutputParentIDs:
                                for event in self.fAvailableFiles[fp]:
                                    deleteFiles |= set( [f[1] for f in event] )
                                self.fAvailableFiles[fp] = self.GetEventListPrototype()
                        if self.fRemoveOutputFiles:
                            for fp in finishedIDs & self.fOutputParentIDs:
                                for event in self.fAvailableFiles[fp]:
                                    deleteFiles |= set( [f[1] for f in event] )
                                self.fAvailableFiles[fp] = self.GetEventListPrototype()
                        for df in deleteFiles:
                            os.unlink( df )
                    

        if componentsRan<=0 and len(set(self.fProcessIDs.keys())-availableComponents)>0:
            raise SCWRunnerException( "Missing parents", "Cannot finish 'run', missing parents: %s" % ( str(set(self.fProcessIDs.keys())-availableComponents) ) )

    def GetOutputFiles( self ):
        if self.fRemoveOutputFiles:
            return []
        outputFiles = self.GetEventListPrototype()
        for oi in self.fOutputParentIDs:
            eventIndex=0
            for event in self.fAvailableFiles[oi]:
                outputFiles[eventIndex] += event
                eventIndex += 1
        return outputFiles
        



if __name__ == "__main__":
    setup = {
        "Quiet" : [0],
        "Debug" : [0],
        # "TriggerClassDDLs" : [],
        # "AutoReadoutListGeneration" : [0],
        "Macro" : [],
        "AWSAddLibrary" : [],
        "AWSAddLibraryBefore" : [],
        "Verbosity" : [0xFFFF],
        "VerbosityExclude" : [0x0000],
        "Loops" : [1],
        "SCWDummyMode" : [0],
        "ProtocolFile" : [],
        }
    
    commandLineSetupMap = {
        "-ddllist" : "DDLList",
        "-nodelist" : "NodeList",
        "-ecsinputlist" : "ECSInputList",
        "-ecsoutputlist" : "ECSOutputList",
        "-quiet" : "Quiet",
        "-debug" : "Debug",
        "-verbosity" : "Verbosity",
        "-verbosityexclude" : "VerbosityExclude",
        # "-triggerclassddls" : "TriggerClassDDLs",
        # "-ctptriggerclasses" : "CTPTriggerClasses",
        # "-autoreadoutlistgeneration" : "AutoReadoutListGeneration",
        "-ecsdetectors" : "ECSDetectors",
        "-ecsruntype" : "ECSRunType",
        "-ecsbeamtype" : "ECSBeamType",
        "-macro" : "Macro",
        "-readoutlistversion" : "ReadoutListVersion",
        "-awsaddlibrary" : "AWSAddLibrary",
        "-awsaddlibrarybefore" : "AWSAddLibraryBefore",
        "-loops" : "Loops",
        "-scwdummymode" : "SCWDummyMode",
        "-protocolfile" : "ProtocolFile",
        }
    
    
    envSetupMap = {
        "TMCONFIG_DDLLIST" : "DDLFile",
        "TMCONFIG_NODELIST" : "NodeFile",
        "TMCONFIG_ECSINPUTLIST" : "ECSInputList",
        "TMCONFIG_ECSOUTPUTLIST" : "ECSOutputList",
        "TMCONFIG_DEBUG" : "Debug",
        "TMCONFIG_VERBOSITY" : "Verbosity",
        "TMCONFIG_VERBOSITYEXCLUDE" : "VerbosityExclude",
        # "TMCONFIG_TRIGGERCLASSDDLS" : "TriggerClassDDLs",
        # "TMCONFIG_CTPTRIGGERCLASSES" : "CTPTriggerClasses",
        # "TMCONFIG_AUTOREADOUTLISTGENERATION" : "AutoReadoutListGeneration",
        "TMCONFIG_ECSDETECTORS" : "ECSDetectors",
        "TMCONFIG_ECSRUNTYPE" : "ECSRunType",
        "TMCONFIG_ECSBEAMTYPE" : "ECSBeamType",
        "TMCONFIG_MACRO" : "Macro",
        "TMCONFIG_READOUTLISTVERSION" : "ReadoutListVersion",
        "TMCONFIG_AWSADDLIBRARY" : "AWSAddLibrary",
        "TMCONFIG_AWSADDLIBRARYBEFORE" : "AWSAddLibraryBefore",
        "TMCONFIG_LOOPS" : "Loops",
        "TMCONFIG_SCWDUMMYMODE" : "SCWDummyMode",
        "TMCONFIG_PROTOCOLFILE" : "ProtocolFile",
        }
    

    setupAllowedConfigEntries = (
        "Verbosity",
        "VerbosityExclude",
        # "TriggerClassDDLs",
        # "CTPTriggerClasses",
        "AWSAddLibrary",
        "AWSAddLibraryBefore",
        "Loops",
        "SCWDummyMode",
        "ProtocolFile",
        )
    
    
    setupIntKeys = (
        "Quiet",
        "Debug",
        "Verbosity",
        "VerbosityExclude",
        "AutoReadoutListGeneration",
        "ReadoutListVersion",
        "Loops",
        "SCWDummyMode",
        )

    ecsParameterList = [
        "DETECTOR_LIST",              # Comma separated list of detector ID strings
        "DATA_FORMAT_VERSION",        # Number of data format version exchanged between HLT Out and DAQ, currently 1
        "BEAM_TYPE",                  # pp (or PP), pA (PA), AA
        "HLT_TRIGGER_CODE",           # String identifier into data base used by HLT to map to trigger component identifiers (O/HCDB?)
        "HLT_IN_DDL_LIST",            # <DDL ID>:<Word Index in HLT Readout List>-<Bit position in word in HLT Readout List>,<DDL ID>:<Word Index in HLT Readout List>-<Bit position in word in HLT Readout List>,...
        "HLT_OUT_DDL_LIST",            # Identical to HLT_IN_DDL_LIST (Only word 29 (HLT Output) allowed
        "RUN_TYPE",                   # Run type specifier string, mostly PHYSICS, STANDALONE, ...
        "HLT_MODE",                   # A, B, C, D, E
        "RUN_NUMBER",                 # String with run number representation (decimal)
        "CTP_TRIGGER_CLASS",          # <bit position>:<Trigger class identifier string>:<detector-id-nr>-<detector-id-nr>-...,<bit position>:<Trigger class identifier string>:<detector-id-nr>-<detector-id-nr>-...,...
        ]
    
    ecsParameters = dict([ (x, None) for x in ecsParameterList ])
    
    ecsSettingsFileMappings = {
        "DetectorListDefault" : "DETECTOR_LIST",
        "BeamTypeDefault" : "BEAM_TYPE",
        "DataFormatVersionDefault" : "DATA_FORMAT_VERSION",
        "HLTTriggerCodeDefault" : "HLT_TRIGGER_CODE",
        "CTPTriggerClassDefault" : "CTP_TRIGGER_CLASS",
        "HLTInDDLListDefault" : "HLT_IN_DDL_LIST",
        "HLTOutDDLListDefault" : "HLT_OUT_DDL_LIST",
        "RunTypeDefault" : "RUN_TYPE",
        }
    
    ecsCommandLineMappings = {
        "-ecsdetectors" : "DETECTOR_LIST",
        "-readoutlistversion" : "DATA_FORMAT_VERSION",
        "-ecsbeamtype" : "BEAM_TYPE",
        "-ecshlttriggercode" : "HLT_TRIGGER_CODE",
        "-ecsinputlist" : "HLT_IN_DDL_LIST",
        "-ecsoutputlist" : "HLT_OUT_DDL_LIST",
        "-ecsruntype" : "RUN_TYPE",
        "-ecshltmode" : "HLT_MODE",
        "-ecsrunnumber" : "RUN_NUMBER",
        "-ecsrunnr" : "RUN_NUMBER",
        "-ecsctptriggerclass" : "CTP_TRIGGER_CLASS",
        }

    ecsParameterExplanations = {
        "DETECTOR_LIST" : "Comma separated list of detector ID strings",
        "DATA_FORMAT_VERSION" : "Number of data format version exchanged between HLT Out and DAQ, currently 3 (supported 1, 2, 3)",
        "BEAM_TYPE" : "beamtype - pp (or PP), pA (PA), AA",
        "HLT_TRIGGER_CODE" : "String identifier into data base used by HLT to map to trigger component identifiers (O/HCDB?)",
        "HLT_IN_DDL_LIST" : "Input DDL list - <DDL ID>:<Word Index in HLT Readout List>-<Bit position in word in HLT Readout List>,<DDL ID>:<Word Index in HLT Readout List>-<Bit position in word in HLT Readout List>,...",
        "HLT_OUT_DDL_LIST" : "Output DDL list - Format identical to HLT_IN_DDL_LIST (Only word 29 (HLT Output) allowed",
        "RUN_TYPE" : "Run type specifier string, mostly PHYSICS, TECHNICAL, PHYSICS_TEST, ...",
        "HLT_MODE" : "HLT mode specifier - A, B, C, D, E",
        "RUN_NUMBER" : "String with run number representation (decimal)",
        "CTP_TRIGGER_CLASS" : "CTP trigger class specification - <bit position>:<Trigger class identifier string>:<detector-id-nr>-<detector-id-nr>-...,<bit position>:<Trigger class identifier string>:<detector-id-nr>-<detector-id-nr>-...,...",
        }

    
    configfiles = []

    for env in envSetupMap.keys():
        if os.environ.has_key( env ):
            SiteConfig.AddItem( setup, envSetupMap[env], os.environ[env] )



    usagestring = " -config <config-filename> (-siteconfig <site-config-filename>) (-ddllist <ddl-list-xml-file>) (-nodelist <node-list-xml-file>) (-quiet <quiet-flag (0 or 1)>) (-debug <debug-flag (0 or 1)>) (-autoreadlistgeneration <auto readout list flag (0 or 1)>) (-macro <macro-ID>=<macro-string-content>) (-awsaddlibrary <additional library to load for AliRootWrapperSusbcriber>) (-awsaddlibrarybefore <additional library to load for AliRootWrapperSubscriber>:<library before which additional library has to be loaded>)"
     # (-triggerclassddls <trigger-class>:<ddl-list>) (-ctptriggerclasses <ctp-trigger-classes-string>) 

    usagestring += "(-ecssettingsfile <ECS-settings-filename>) "+( " ".join([ "("+x+" "+ecsParameterExplanations[ecsCommandLineMappings[x]]+")" for x in ecsCommandLineMappings.keys()] ) )


    protocolFiles = []
    exitValue = 0
    try:
        argList = sys.argv[1:]
        while len(argList)>0:
            if argList[0]=="-siteconfig":
                if len(argList)<2:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -siteconfig." )
                try:
                    SiteConfig.Read( setup, argList[1] )
                except SiteConfig.SiteConfigException,e:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\n"+str(e) )
            argList = argList[2:]
        

        argList = sys.argv[1:]
        while len(argList)>0:
            if argList[0]=="-config":
                if len(argList)<2:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -config." )
                configfiles.append(argList[1])
                argList = argList[2:]
            elif argList[0]=="-configlist":
                if len(argList)<2:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -configlist." )
                try:
                    configinfile=open( argList[1], "r" )
                except IOError:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nUnable to open config list input file '"+argList[1]+"'." )
                line=configinfile.readline()
                while line != "":
                    configfiles.append(line[:-1])
                    line=configinfile.readline()
                configinfile.close()
                argList = argList[2:]
            elif argList[0]=="-siteconfig":
                if len(argList)<2:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -siteconfig." )
                argList = argList[2:]
                continue
            elif argList[0]=="-ecssettingsfile":
                if len(argList)<2:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to -ecssettingsfile." )
                infile = open( argList[1], "r" )
                line = infile.readline()
                while line != "":
                    for key in ecsSettingsFileMappings.keys():
                        # print key+" - "+line
                        patString = "^\s*\["+key+"\] *= *(\S*) *$"
                        # print patString+" - "+line
                        pat = re.compile( patString )
                        match = pat.match( line )
                        if match != None:
                            # print "Match : "+patString+" - "+line
                            param = match.group( 1 )
                            print key+" -> "+ecsSettingsFileMappings[key]+" -> "+param
                            ecsParameters[ecsSettingsFileMappings[key]] = param
                            break
                    line = infile.readline()
                infile.close()
                argList = argList[2:]
                continue
            elif ecsCommandLineMappings.has_key(argList[0]):
                if len(argList)<2:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to '"+argList[0]+"'." )
                ecsParameters[ecsCommandLineMappings[argList[0]]] = argList[1]
                argList = argList[2:]
            elif commandLineSetupMap.has_key(argList[0]):
                if len(argList)<2:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMissing argument to '"+argList[0]+"'." )
                temp = string.Template( argList[1] )
                try:
                    value = temp.substitute( os.environ )
                except KeyError,e:
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nUnknown environment variable "+str(e) )
                SiteConfig.AddItem( setup, commandLineSetupMap[argList[0]], value )
                argList = argList[2:]
            else:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nUnknown parameter '"+argList[0]+"'." )

        for param in ecsParameterList:
            if ecsParameters[param]==None:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nECS parameter '"+param+"' not defined." )

        ecsString = ";".join( [ x+"="+ecsParameters[x] for x in ecsParameterList ] )

        setup["ReadoutListVersion"] = [ ecsParameters["DATA_FORMAT_VERSION"] ]

        for i in setupIntKeys:
            if setup.has_key(i):
                try:
                    if type(setup[i][-1])!=type(1):
                        setup[i][-1] = int(setup[i][-1],0)
                except ValueError:
                    cmdLine="UNKNOWN"
                    envEntry="UNKNOWN"
                    for k in commandLineSetupMap.keys():
                        if commandLineSetupMap[k]==i:
                            cmdLine=k
                    for k in envSetupMap.keys():
                        if envSetupMap[k]==i:
                            envEntry=k
                    raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\n'"+i+"' option value '"+setup[i][-1]+"' (command line '"+cmdLine+"', environment variable '"+envEntry+"', xml site config '"+i+"') is not a number." )

        if len(configfiles)<=0:
            raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nAt least one config or late-creation-data file has to be specified." )
        if not setup.has_key("ReadoutListVersion"):
            raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nReadout list data format version has to be specified." )
        Detector.ReadoutListVersion = setup["ReadoutListVersion"][-1]
        if setup.has_key("ECSRunType"):
            ecsRunType = setup["ECSRunType"][-1]
        else:
            ecsRunType = None

        macros = {}
        for k in setup["Macro"]:
            toks = k.split("=")
            if len(toks)!=2:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMacro definition '"+k+"' does not match allowed format (<macro-ID>=<macro-value-string>)." )
            if macros.has_key(toks[0]):
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nMacro '"+toks[0]+"' defined multiple times." )
            macros[toks[0]] = toks[1]

        for k in setup["AWSAddLibraryBefore"]:
            toks = k.split( ":" )
            if len(toks)!=2:
                raise SimpleChainConfig1.ChainConfigException( "Usage: "+sys.argv[0]+usagestring+"\nAliRootWrapperSubscriber additional library before specification '"+k+"' does not match allowed format (<library>:<before-library>)." )
        
        # The following hack seems necessary as both os.getcwd and os.path.realpath seem to expand symlinks...
        pwdfile=os.popen("pwd")
        outputDir=pwdfile.read()[:-1]
        pwdfile.close()
        del(pwdfile)

        reader = SCWRunner( configfiles )

        reader.SetAllowedConfigEntries( setupAllowedConfigEntries )

        reader.SetECSParameters( ecsString )

        reader.ReadConfigEntries()
        readerConfigEntries = reader.GetConfigEntries()
        for rcek in readerConfigEntries.keys():
            for rce in readerConfigEntries[rcek]:
                if rce in setupIntKeys and type(rce)!=type(1):
                    try:
                        SiteConfig.AddItem( setup, rcek, int(rce,0) )
                    except ValueError:
                        raise SimpleChainConfig1.ChainConfigException( "Value '"+rce+"' for option '"+rcek+" set in XML config file is not a number." )
                else:
                    SiteConfig.AddItem( setup, rcek, rce )

        for sace in setupAllowedConfigEntries:
            if setup.has_key(sace):
                reader.SetConfigEntry( sace, setup[sace] )

        reader.SetSCWDummyMode( setup["SCWDummyMode"][-1] )

        if setup["Quiet"][-1]:
            reader.Quiet( setup["Quiet"][-1] )

        resman = DummyResourceManager( "RunSCW-Dummy" )

        reader.SetResourceManager( resman )

        reader.AddMacros( macros )

        if setup.has_key( "AWSOptions" ):
            reader.AddAWSOption( " ".join( setup["AWSOptions"] ) )
        for k in setup["AWSAddLibrary"]:
            reader.AddAWSLibrary( k )
        for k in setup["AWSAddLibraryBefore"]:
            toks = k.split(":")
            reader.AddAWSLibraryBefore( toks[0], toks[1] )


        ddlLocations = resman.MakeDDLList()
        reader.SetDDLLocations( ddlLocations )

        if setup.has_key("AutoReadoutListGeneration"):
            reader.SetAutoDetectorReadoutList( setup["AutoReadoutListGeneration"][-1] )

        if setup.has_key( "Verbosity" ):
            verb = setup["Verbosity"][-1]
        else:
            verb=None
        if setup.has_key( "VerbosityExclude" ):
            verbEx = setup["VerbosityExclude"][-1]
        else:
            verbEx=None
        reader.SetVerbosity( verb, verbEx )

        reader.SetLoops( setup["Loops"][-1] )

        config = reader.Read()

        for pf in setup["ProtocolFile"]:
            try:
                protocolFiles.append( open( pf, "w" ) )
            except IOError:
                raise SimpleChainConfig1.ChainConfigException( "Unable to open protocol file '"+pf+"' for writing." )
            reader.AddProtocolFile( protocolFiles[-1] )
    
        reader.Run()

        outputFiles = reader.GetOutputFiles()
        print "Processing finished. Produced output files:"
        eventIndex=0
        for event in outputFiles:
            print "Event",eventIndex,":"," ".join([ file[0] for file in event ] )
            eventIndex += 1
        
    except (SimpleChainConfig1.ChainConfigException,ConfigException, Exceptions.SCC2Exception), e:
        sys.stderr.write(  "Exception caught: "+e.fValue+"\n" )
        if setup["Debug"][-1]:
            raise
        exitValue = 1
    except (SCWRunnerException), e:
        sys.stderr.write(  "Exception caught: "+str(e)+"\n" )
        if setup["Debug"][-1]:
            raise
        exitValue = 1
    finally:
        for pf in protocolFiles:
            pf.close()

sys.exit( exitValue )
