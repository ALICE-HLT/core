#!/usr/bin/env python

import os, os.path, string, sys

class ComponentOutputterBase:
    def __init__( self, outputDir ):
        self.fOutputDir = outputDir

    def OutputMappedProcesses( self, config, processList ):
        sources = []
        components = []
        for proc in processList:
            toks = proc.GetCmd().split()
            if proc.GetType()=="src":
                src = {}
                params = {
                    "-ddlid" : "ddlid",
                    }
                src["id"] = proc.GetID()
                for pk in params.keys():
                    if pk in toks and toks.index(pk)+1<len(toks):
                        src[params[pk]] = int(toks[toks.index()+1],0)
                    else:
                        print pk+" parameter not found in source '"+proc.GetID()+"' command: '"+proc.GetCmd()+"'."
                if os.path.basename( toks[0] )=="FilePublisher":
                    src["type"] = "file"
                    filespecs=[]
                    for fsp in ( "-datafile", "-datafilelist" ):
                        ndx=0
                        while fsp in toks[ndx:]:
                            ndx=toks.index(fsp)+1
                            if ndx<len(toks):
                                filespecs.append( toks[ndx] )
                            else:
                                print "Could not find "+fsp+" parameter for source '"+proc.GetID()+"' command: '"+proc.GetCmd()+"'."
                    src["filespecs"] = "filespecs"
                elif os.path.basename( toks[0] )=="CRORCPublisher":
                    src["type"] = "ddl"
                sources.append( src )
            elif os.path.basename( toks[0] )=="AliRootWrapperSubscriber":
                comp = {}
                comp["id"] = proc.GetID()
                params = {
                    "-componentid" : "componentid",
                    "-componentlibrary" : "componentlibrary",
                    "-componentargs" : "componentargs",
                    }
                for pk in params.keys():
                    if pk in toks and toks.index(pk)+1<len(toks):
                        comp[params[pk]] = int(toks[toks.index()+1],0)
                    else:
                        print pk+" parameter not found in component '"+proc.GetID()+"' command: '"+proc.GetCmd()+"'."
                ndx=0
                addlibs = []
                while "-addlibrary" in toks[ndx:]:
                    ndx=toks.index("-addlibrary")+1
                    if ndx<len(toks):
                        addlibs.append( toks[ndx] )
                    else:
                        print "Could not find -addlibrary parameter for component '"+proc.GetID()+"' command: '"+proc.GetCmd()+"'."
                comp["addlibraries"] = addlibs
                ndx=0
                addlibsbefore = {}
                while "-addlibrarybefore" in toks[ndx:]:
                    ndx=toks.index("-addlibrarybefore")+1
                    if ndx+1<len(toks):
                        addlibsbefore[ toks[ndx] ] = toks[ndx+1]
                    else:
                        print "Could not find -addlibrarybefore parameters for component '"+proc.GetID()+"' command: '"+proc.GetCmd()+"'."
                comp["addlibrariesbefore"] = addlibsbefore
                ndx=0
                comp["parents"] = proc.GetParents()
            components.append( comp )

        outfile = open( os.path.join( self.fOutputDir, config.GetName()+self.GetOutputFileSuffix() ), "w" )

        self.WriteOutputFile( outfile, config, sources, components )

        outfile.close()



    # Implement this to return the file suffix (not necessarily .something can also be something.somethingelse)
    # First part of the filename will be the configuration ID
    def GetOutputFileSuffix( self ):
        raise Exception( "Not implemented" )

    # Implement this to write out the file as needed
    # outFile is the file object to write to
    # config is the configuration object, if any information from that is needed
    # sources is a list of dictionaties with the following keys: id, ddlid, type, filespecs (if type==file)
    # components is a list of dicitonaries with the following keys: id, componentid, componentlibrary, componentargs, addlibraries, addlibrariesbefore, parents
    def WriteOutputFile( self, outfile, config, sources, components ):
        raise Exception( "Not implemented" )

