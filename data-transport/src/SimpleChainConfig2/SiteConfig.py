#!/usr/bin/env python

import libxml2
import Exceptions
import string, os

class SiteConfigException(Exceptions.SCC2Exception):
    def __init__(self,message):
        Exceptions.SCC2Exception.__init__(self,message)
        pass

def AddItem( siteconfig, key, value ):
    if siteconfig.has_key(key):
        siteconfig[key].append(value)
    else:
        siteconfig[key] = [ value ]


def Read( siteconfig, filename ):
    xmlFile = libxml2.parseFile(filename)
    xPathContext = xmlFile.xpathNewContext()
    if len(xPathContext.xpathEval("/SimpleChainConfig2Site"))!=1:
        raise SiteConfigException( "Exactly one top level SimpleChainConfig2Site node allowed and required in XML file" )
    items = xPathContext.xpathEval("/SimpleChainConfig2Site/*")
    for i in items:
        temp = string.Template( i.content )
        try:
            value = temp.substitute( os.environ )
        except KeyError,e:
            raise SiteConfigException( "Unknown environment variable "+str(e) )
        AddItem( siteconfig, i.name, value )


if __name__ == "__main__":
    import sys
    if len(sys.argv)<=1:
        print "Usage: "+sys.argv[0]+" <site-config-xml>"
        sys.exit(1)
    settings = {}
    Read( settings, sys.argv[1] )
    keys = settings.keys()
    keys.sort()
    for k in keys:
        print k,settings[k]

        
