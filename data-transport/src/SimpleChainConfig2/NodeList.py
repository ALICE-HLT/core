#!/usr/bin/env python

#!/usr/bin/env python

import libxml2
import Exceptions

class NodeListException(Exceptions.SCC2Exception):
    def __init__(self,message):
        Exceptions.SCC2Exception.__init__(self,message)
        pass


def ReadXML( filename ):
    nodeList = {}
    xmlFile = libxml2.parseFile(filename)
    xPathContext = xmlFile.xpathNewContext()
    if len(xPathContext.xpathEval("/SimpleChainConfig2NodeList"))!=1:
        raise NodeListException( "Exactly one top level SimpleChainConfig2NodeList node allowed and required in XML file" )
    nodes = xPathContext.xpathEval("/SimpleChainConfig2NodeList/Node")
    for node in nodes:
        id=node.prop("ID")
        if id==None:
            raise NodeListException( "ID attribute required for Node node" )
        if len(node.xpathEval("./Platform"))!=1:
            raise NodeListException( "Exactly one Platform node required for Node "+id )
        platform = node.xpathEval("./Platform")[0].content
        if len(node.xpathEval("./Hostname"))>1:
            raise NodeListException( "At most one Hostname node required for Node "+id )
        if len(node.xpathEval("./Hostname"))==1:
            hostname = node.xpathEval("./Hostname")[0].content
        else:
            hostname = id
        if len(node.xpathEval("./CPUs"))!=1:
            raise NodeListException( "Exactly one CPUs node required for Node "+id )
        try:
            cpus = int(node.xpathEval("./CPUs")[0].content)
        except ValueError:
            raise NodeListException( "CPUs node does not contain an integer number "+id )
        nodeList[id] = { "cpus" : cpus, "platform" : platform, "hostname" : hostname }
        if len(node.xpathEval("./Master"))>0:
            if len(node.xpathEval("./Master"))>1:
                raise NodeListException( "At most one Master node allowed for Node "+id )
            try:
                master = int(node.xpathEval("./Master")[0].content)
            except ValueError:
                raise NodeListException( "Master node content must be a number for Node "+id )
            nodeList[id]["master"] = master
        attrs = {}
        for attr in node.xpathEval("./Attribute"):
            attrName = attr.prop("name")
            attrVal = attr.prop("value")
            if not attrs.has_key(attrName):
                attrs[attrName] = []
            attrs[attrName].append( attrVal )
        nodeList[id]["attributes"] = attrs
    return nodeList

if __name__ == "__main__":
    import sys
    if len(sys.argv)<=1:
        print "Usage: "+sys.argv[0]+" <nodelist.xml>"
        sys.exit(1)
    nodeLoc = ReadXML( sys.argv[1])
    nodes = nodeLoc.keys()
    nodes.sort()
    for node in nodes:
        print node,nodeLoc[node]["hostname"],nodeLoc[node]["cpus"]

