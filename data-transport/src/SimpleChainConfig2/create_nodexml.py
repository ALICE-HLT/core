#!/usr/bin/env python

import Detector
import DDLLocations

file="ddllist0.xml"
ddls = DDLLocations.ReadXML(file)
nodes = set()

for ddl in ddls.keys():
    nodes.add( ddls[ddl]["node"])
    
nodes.add("portal-ecs0")
nodes.add("portal-ecs1")

masters = [
    "portal-ecs0",
    "portal-ecs1"
    ]

nodes=list(nodes)

nodes.sort()



print( "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" )
print( "<SimpleChainConfig2NodeList>" )


for node in nodes:
    if masters.count(node)>0:
        master="<Master>1</Master>"
    else:
        master=""
    print( "    <Node ID=\""+node+"\"><CPUs>4</CPUs><Platform>Linux-x86_64</Platform>"+master+"</Node>" )
    


print( "</SimpleChainConfig2NodeList>" )
