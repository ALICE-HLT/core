#!/usr/bin/env python

import libxml2
import Exceptions

class AliasListException(Exceptions.SCC2Exception):
    def __init__(self,message):
        Exceptions.SCC2Exception.__init__(self,message)
        pass

class AliasList:
    def __init__( self ):
        self.fAliases = {}
        self.fReverseAliases = {}

    def ReadXML( self, filename ):
        xmlFile = libxml2.parseFile(filename)
        xPathContext = xmlFile.xpathNewContext()
        if len(xPathContext.xpathEval("/SimpleChainConfig2AliasList"))!=1:
            raise AliasListException( "Exactly one top level SimpleChainConfig2AliasList node allowed and required in XML file" )
        aliases = xPathContext.xpathEval("/SimpleChainConfig2AliasList/Alias")
        for alias in aliases:
            id=alias.prop("ID")
            aliasID=alias.prop("hostname")
        if id==None:
            raise AliasListException( "ID attribute required for Alias node" )
        if aliasID==None:
            raise AliasListException( "hostname attribute required for Alias node" )
        if self.fReverseAliases.has_key(aliasID):
            raise AliasListException( "hostname attribute '"+aliasID+"' already used" )
        if self.fAliases.has_key(id):
            raise AliasListException( "ID attribute '"+id+"' already used" )
        self.fAliases[id] = aliasID
        self.fReverseAliases[aliasID] = id

    def GetAliases( self ):
        return self.fAliases

    def GetReverseAliases( self ):
        return self.fReverseAliases

if __name__ == "__main__":
    import sys
    if len(sys.argv)<=1:
        print "Usage: "+sys.argv[0]+" <alias-list-xml>"
        sys.exit(1)
    aliasList = AliasList()
    aliasList.ReadXML( sys.argv[1])
    aliases = aliasList.GetAliases()
    ids = aliases.keys()
    ids.sort()
    for id in ids:
        print id," -> ",aliases[id]
