#!/bin/bash


# -- DEFAULTS --
# --------------
OUTPUTDIR="."
INPUTPREFIX=""
DETECTORID=""

# -- Check Comand Line Arguments --
# ---------------------------------
USAGE="Usage : $0 -filetype=<file type (partition|triggerlist|trigger|component|detectorbase|detectorbeamrun)> (-id=<snippet identifier>) (-snippetdetectorid=<detector identifier>) (-outputdir=<Output-Dir>) (-prefix=<input-filename-prefix>)"

for cmd in $* ; do

    if [[ "$cmd" == -* || "$cmd" == -h ]] ; then
	
        # Get  option
	arg=`echo $cmd | sed 's|-[-_a-zA-Z0-9]*=||'`
	
	case "$cmd" in

	    # get detector
	    -detector* ) DETECTOR=$arg ;;    

	    # get beamtype, pp or AA
	    -beamtype* )  BEAMTYPE=$arg ;;

	    # get runtype
	    -runtype* )  RUNTYPE=$arg ;;

	    # get filetype
	    -filetype* )  FILETYPE=$arg ;;

	    # get snippet-id
	    -id* )  SNIPPETID=$arg ;;

	    # get output directory (checkout target)
	    -outputdir* )  OUTPUTDIR=$arg ;;

	    # get input filename prefix
	    -prefix* )  INPUTPREFIX=$arg ;;

	    # get detector identifier
	    -snippetdetectorid* )  DETECTORID=$arg ;;

	    * ) echo "Error : Argument not known: $cmd"; echo ${USAGE}; exit 1;;
	esac		

    else
	echo "Error : Argument not known: $cmd"
	echo ${USAGE} 
	exit 1
    fi
done

# -- Check usage --
# -----------------
if [[ ! ${DETECTOR} || ! ${BEAMTYPE} || ! ${RUNTYPE} ]] ; then
    echo "Error : " ${USAGE} ; echo
    exit 1
fi

if [ ! -d ${OUTPUTDIR} ] ; then 
    mkdir ${OUTPUTDIR}
fi

if [ ! -d ${OUTPUTDIR} ] ; then 
    echo "Error : Unable to create directory ${OUTPUTDIR}" ; echo
    exit 2
fi

#echo ${FILETYPE}
if [ "${FILETYPE}" != "partition" -a "${FILETYPE}" != "trigger" -a "${FILETYPE}" != "triggerlist" -a "${FILETYPE}" != "component" -a "${FILETYPE}" != "detectorbase" -a "${FILETYPE}" != "detectorbeamrun" ] ; then
    echo "Error : " ${USAGE} "(Unknown specifier for -filetype): ${FILETYPE}" ; echo
    exit 1
fi

if [ "${FILETYPE}" = "detectorbase" -a -z "${DETECTORID}" ] ; then
  echo "-snippetdetectorid needs to be passed for type detectorbase" ; echo
  exit 1
fi

if [ "${FILETYPE}" = "detectorbeamrun" -a -z "${DETECTORID}" ] ; then
  echo "-snippetdetectorid needs to be passed for type detectorbeamrun" ; echo
  exit 1
fi

if [ "${FILETYPE}" = "component" -a -z "${DETECTORID}" ] ; then
  echo "-snippetdetectorid needs to be passed for type component" ; echo
  exit 1
fi

if [ "${FILETYPE}" = "component" -a -z "${SNIPPETID}" ] ; then
  echo "-id needs to be passed for type component" ; echo
  exit 1
fi

if [ "${FILETYPE}" = "triggerlist" -a -z "${SNIPPETID}" ] ; then
  echo "-id needs to be passed for type triggerlist" ; echo
  exit 1
fi

if [ "${FILETYPE}" = "trigger" -a -z "${SNIPPETID}" ] ; then
  echo "-id needs to be passed for type trigger" ; echo
  exit 1
fi


INPUTFILENAME=${INPUTPREFIX}

if [ -n "${DETECTORID}" ] ; then
  INPUTFILENAME="${INPUTFILENAME}-${DETECTORID}"
fi

case "${FILETYPE}" in
    "partition") 
      INPUTFILENAME="${INPUTFILENAME}-${DETECTOR}-${BEAMTYPE}-${RUNTYPE}.xml"
      ;;
    "trigger")
      INPUTFILENAME="${INPUTFILENAME}-Trigger-${SNIPPETID}.xml"
      ;;
    "triggerlist")
      INPUTFILENAME="${INPUTFILENAME}-TriggerList-${SNIPPETID}"
      ;;
    "component")
      INPUTFILENAME="${INPUTFILENAME}-${SNIPPETID}.xml"
      ;;
    "detectorbase")
      INPUTFILENAME="${INPUTFILENAME}.xml"
      ;;
    "detectorbeamrun")
      INPUTFILENAME="${INPUTFILENAME}-${BEAMTYPE}-${RUNTYPE}.xml"
      ;;
esac

if [ ! -f "${INPUTFILENAME}" ] ; then
  if [ "${FILETYPE}" != "detectorbeamrun" ] ; then
      echo "Input file ${INPUTFILENAME} does not exist. Aborting..." ; echo
      exit 2
  else
      exit 0
  fi
fi
if [ ! -r "${INPUTFILENAME}" ] ; then
  echo "Input file ${INPUTFILENAME} is not readable. Aborting..." ; echo
  exit 2
fi




OUTPUTFILENAME="${OUTPUTDIR}"/`basename "${INPUTFILENAME}"`

#echo cp ${INPUTFILENAME} ${OUTPUTFILENAME}
cp ${INPUTFILENAME} ${OUTPUTFILENAME}
echo ${OUTPUTFILENAME}

