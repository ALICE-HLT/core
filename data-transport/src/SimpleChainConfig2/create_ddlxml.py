#!/usr/bin/env python

import Detector

print( "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" )
print( "<SimpleChainConfig2DDLList>" )


print( "\n<!-- TPC -->\n" )
ddls = Detector.GetTPCDDLList()
for ddl in ddls:
    slice = Detector.GetTPCSlice(ddl)
    part = Detector.GetTPCPartition(ddl)
    if slice<18:
        side="a"
    else:
        side="c"
    if part<2:
        roc="i"
        nr = ((slice%18)//2)*2
    else:
        roc="o"
        nr = slice%18
    node = "feptpc"+side+roc+"%02d"%nr
    bus =  (1-((ddl & 0xFF) % 4) // 2) + 2
    slot = 5- (1-( ((ddl & 0xFF) % 4) // 2 ))
    function = 0
    diu = (ddl & 0xFF) % 2
    print( "    <DDL ID=\""+("%d"%ddl)+"\"><Node>"+node+"</Node><Bus>"+("%d"%bus)+"</Bus><Slot>"+("%d"%slot)+"</Slot><Function>"+("%d"%function)+"</Function><DIU>"+("%d"%diu)+"</DIU></DDL>" )


print( "\n<!-- DIMU-TRG -->\n" )
ddls = Detector.NameToDDLList("MUTG")
for ddl in ddls:
    nodebase = "fepdimutrg"
    bus =  (1-((ddl & 0xFF) % 4)) + 2
    slot = 5- (1-( (ddl & 0xFF) % 4))
    function = 0
    diu = 0
    print( "    <DDL ID=\""+("%d"%ddl)+"\"><Node>"+nodebase+"</Node><Bus>"+("%d"%bus)+"</Bus><Slot>"+("%d"%slot)+"</Slot><Function>"+("%d"%function)+"</Function><DIU>"+("%d"%diu)+"</DIU></DDL>" )

print( "\n<!-- DIMU-TRK -->\n" )
ddls = Detector.NameToDDLList("MUTK")
for ddl in ddls:
    nodebase = "fepdimutrk"
    nr = (ddl & 0xFF)
    nodenr = nr // 4 + 1
    bus =  (1-((ddl & 0xFF) % 4) // 2) + 2
    slot = 5- (1-( ((ddl & 0xFF) % 4) // 2 ))
    function = 0
    diu = (ddl & 0xFF) % 2
    print( "    <DDL ID=\""+("%d"%ddl)+"\"><Node>"+nodebase+("%d"%nodenr)+"</Node><Bus>"+("%d"%bus)+"</Bus><Slot>"+("%d"%slot)+"</Slot><Function>"+("%d"%function)+"</Function><DIU>"+("%d"%diu)+"</DIU></DDL>" )

print( "\n<!-- HMPID -->\n" )
ddls = Detector.NameToDDLList("HMPI")
for ddl in ddls:
    nodebase = "fephmpid"
    nr = (ddl & 0xFF)
    nodenr = nr // 4
    bus =  (1-((ddl & 0xFF) % 4) // 2) + 2
    slot = 5- (1-( ((ddl & 0xFF) % 4) // 2 ))
    function = 0
    diu = (ddl & 0xFF) % 2
    print( "    <DDL ID=\""+("%d"%ddl)+"\"><Node>"+nodebase+("%d"%nodenr)+"</Node><Bus>"+("%d"%bus)+"</Bus><Slot>"+("%d"%slot)+"</Slot><Function>"+("%d"%function)+"</Function><DIU>"+("%d"%diu)+"</DIU></DDL>" )

print( "\n<!-- TriggerDet -->\n" )
ddls = Detector.NameToDDLList("TRIG")
for ddl in ddls:
    nodebase = "feptriggerdet"
    nr = (ddl & 0xFF)
    bus =  (1-((ddl & 0xFF) % 4) // 2) + 2
    slot = 5- (1-( ((ddl & 0xFF) % 4) // 2 ))
    function = 0
    diu = (ddl & 0xFF) % 2
    print( "    <DDL ID=\""+("%d"%ddl)+"\"><Node>"+nodebase+"</Node><Bus>"+("%d"%bus)+"</Bus><Slot>"+("%d"%slot)+"</Slot><Function>"+("%d"%function)+"</Function><DIU>"+("%d"%diu)+"</DIU></DDL>" )

print( "\n<!-- ACORDE -->\n" )
ddls = Detector.NameToDDLList("ACOR")
for ddl in ddls:
    nodebase = "fepfmdaccorde"
    nr = 3
    bus =  (1-((ddl & 0xFF) % 4) // 2) + 2
    slot = 5- (1-( ((ddl & 0xFF) % 4) // 2 ))
    function = 0
    diu = (nr) % 2
    print( "    <DDL ID=\""+("%d"%ddl)+"\"><Node>"+nodebase+"</Node><Bus>"+("%d"%bus)+"</Bus><Slot>"+("%d"%slot)+"</Slot><Function>"+("%d"%function)+"</Function><DIU>"+("%d"%diu)+"</DIU></DDL>" )


print( "\n<!-- TRD -->\n" )
ddls = Detector.NameToDDLList("TRD")
for ddl in ddls:
    nodebase = "feptrd"
    nr = (ddl & 0xFF)
    nodenr = (nr // 4)*4
    if nr>=10:
        nodenr = ((nr+2) // 4)*4-2
    node = nodebase+"%02d" % nodenr
    if nr<8:
        bus =  (1-((ddl & 0xFF) % 4) // 2) + 2
        slot = 5- (1-( ((ddl & 0xFF) % 4) // 2 ))
        function = 0
        diu = (ddl & 0xFF) % 2
    elif nr<10:
        bus =  (1-(ddl & 0xFF) % 4) + 2
        slot = 5- (1- ((ddl & 0xFF) % 4))
        function = 0
        diu = 0
    else:
        bus =  (1-(((ddl & 0xFF)+2) % 4) // 2) + 2
        slot = 5- (1-( (((ddl & 0xFF)+2) % 4) // 2 ))
        function = 0
        diu = ((ddl & 0xFF)+2) % 2
    print( "    <DDL ID=\""+("%d"%ddl)+"\"><Node>"+node+"</Node><Bus>"+("%d"%bus)+"</Bus><Slot>"+("%d"%slot)+"</Slot><Function>"+("%d"%function)+"</Function><DIU>"+("%d"%diu)+"</DIU></DDL>" )



for det in Detector.IDs.keys():
    if det=="TPC" or det=="MUTG" or det=="MUTK" or det=="HMPI" or det=="TRIG" or det=="ACOR" or det =="TRD" or det=="HLT":
        continue
    print( "\n<!-- "+det+" -->\n" )
    ddls = Detector.NameToDDLList(det)
    nodebase = "fep"+det.lower()
    for ddl in ddls:
        nr = (ddl & 0xFF)
        nodenr = nr // 4
        node = nodebase+"%d" % nodenr
        bus =  (1-((ddl & 0xFF) % 4) // 2) + 2
        slot = 5- (1-( ((ddl & 0xFF) % 4) // 2 ))
        function = 0
        diu = (ddl & 0xFF) % 2
        print( "    <DDL ID=\""+("%d"%ddl)+"\"><Node>"+node+"</Node><Bus>"+("%d"%bus)+"</Bus><Slot>"+("%d"%slot)+"</Slot><Function>"+("%d"%function)+"</Function><DIU>"+("%d"%diu)+"</DIU></DDL>" )


print( "\n<!-- HLT -->\n" )
ddls = Detector.NameToDDLList("HLT")
for ddl in ddls:
    nodebase = "fephltout"
    nr = (ddl & 0xFF)
    nodenr = nr//2
    bus =  1-(nr % 2) + 2
    slot = 5- (1-( (nr % 2) ))
    function = 0
    diu = 0
    print( "    <DDL ID=\""+("%d"%ddl)+"\"><Node>"+nodebase+("%d"%nodenr)+"</Node><Bus>"+("%d"%bus)+"</Bus><Slot>"+("%d"%slot)+"</Slot><Function>"+("%d"%function)+"</Function><DIU>"+("%d"%diu)+"</DIU></DDL>" )


print( "</SimpleChainConfig2DDLList>" )
