/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTReadoutList.hpp"
#include <cstring>
#include <cstdlib>
#include <iostream>

const unsigned gkReadoutListVersions = 5; // If you update this, also make sure that the gkReadoutListWordCounts array below has the necessary number of elements!!!
const unsigned gkReadoutListWordCounts[] = { READOUTLIST_MAX_WORDCOUNT, READOUTLIST_VERSION1_WORDCOUNT, READOUTLIST_VERSION2_WORDCOUNT, READOUTLIST_VERSION3_WORDCOUNT, READOUTLIST_VERSION4_WORDCOUNT, READOUTLIST_VERSION5_WORDCOUNT };

const char* gkDetectorNames[] = { "ITSSPD", "ITSSDD", "ITSSSD", "TPC",
				  "TRD", "TOF", "HMPID", "PHOS", "CPV", 
				  "PMD", "MUONTRK", "MUONTRG", "FMD",
				  "T0", "VZERO", "ZDC", "ACORDE", "TRG",
				  "EMCAL", "DAQ_TEST",
				  "UNUSED", "AD", "UNUSED", "UNUSED", 
				  "UNUSED", "UNUSED", "UNUSED", "UNUSED", 
				  "UNUSED", "UNUSED", "HLT" };
const char* gkDetectorShortNames[] = { "ISPD", "ISDD", "ISSD", "TPC",
				       "TRD", "TOF", "HMPI", "PHOS", "CPV", 
				       "PMD", "MTRK", "MTRG", "FMD",
				       "T0", "VZRO", "ZDC", "ACOR", "TRG",
				       "EMCA", "DAQT", 
				       "NUSE", "AD", "NUSE", "NUSE", "NUSE", 
				       "NUSE", "NUSE", "NUSE", "NUSE", "NUSE", 
				       "HLT" };

static unsigned gkDetectorListWords0[] = { 0, 0, 0, 0, 0, 0, 
					   0, 0, 0, 0, 0, 0,
					   0, 0, 0, 0, 0, 0, 
					   0, 0, 0, 0, 0, 0, 
					   0, 0, 0, 0, 0, 0, 
					   0 };
static unsigned gkDetectorListWords1[] = { 1, 1, 1, 8, 1, 3, 
					   1, 1, 1, 1, 1, 1,
					   1, 1, 1, 1, 1, 1, 
					   1, 1, 0, 0, 0, 0, 
					   0, 0, 0, 0, 0, 0, 
					   1 };
static unsigned gkDetectorListWords2[] = { 1, 1, 1, 8, 1, 3, 
					   1, 1, 1, 1, 1, 1,
					   1, 1, 1, 1, 1, 1, 
					   1, 1, 0, 0, 0, 0, 
					   0, 0, 0, 0, 0, 0, 
					   1 };
static unsigned gkDetectorListWords3[] = { 1, 1, 1, 8, 1, 3, 
					   1, 1, 1, 1, 1, 1,
					   1, 1, 1, 1, 1, 1, 
					   2, 1, 0, 0, 0, 0,
					   0, 0, 0, 0, 0, 0, 
					   1 };
static unsigned gkDetectorListWords4[] = { 1, 1, 1, 8, 1, 3, 
					   1, 1, 1, 1, 1, 1,
					   1, 1, 1, 1, 1, 1, 
					   2, 1, 0, 0, 0, 0,
					   0, 0, 0, 0, 0, 0, 
					   1 };
static unsigned gkDetectorListWords5[] = { 1, 1, 1, 8, 1, 3, 
					   1, 1, 1, 1, 1, 1,
					   1, 1, 1, 1, 1, 1, 
					   2, 1, 0, 1, 0, 0,
					   0, 0, 0, 0, 0, 0, 
					   1 };
const unsigned* gkDetectorListWords[] = { gkDetectorListWords0,
					  gkDetectorListWords1,
					  gkDetectorListWords2,
					  gkDetectorListWords3,
					  gkDetectorListWords4,
					  gkDetectorListWords5 };
const unsigned gkDetectorListUsedWords0[] = { 0, 0, 0, 0, 0, 0, 
					      0, 0, 0, 0, 0, 0,
					      0, 0, 0, 0, 0, 0, 
					      0, 0, 0, 0, 0, 0, 
					      0, 0, 0, 0, 0, 0, 
					      0 };
const unsigned gkDetectorListUsedWords1[] = { 1, 1, 1, 7, 1, 3, 
					      1, 1, 1, 1, 1, 1,
					      1, 1, 1, 1, 1, 1, 
					      1, 1, 0, 0, 0, 0, 
					      0, 0, 0, 0, 0, 0, 
					      1 };
const unsigned gkDetectorListUsedWords2[] = { 1, 1, 1, 7, 1, 3, 
					      1, 1, 1, 1, 1, 1,
					      1, 1, 1, 1, 1, 1, 
					      1, 1, 0, 0, 0, 0, 
					      0, 0, 0, 0, 0, 0, 
					      1 };
const unsigned gkDetectorListUsedWords3[] = { 1, 1, 1, 7, 1, 3, 
					      1, 1, 1, 1, 1, 1,
					      1, 1, 1, 1, 1, 1, 
					      2, 1, 0, 0, 0, 0, 
					      0, 0, 0, 0, 0, 0, 
					      1 };
const unsigned gkDetectorListUsedWords4[] = { 1, 1, 1, 7, 1, 3, 
					      1, 1, 1, 1, 1, 1,
					      1, 1, 1, 1, 1, 1, 
					      2, 1, 0, 0, 0, 0, 
					      0, 0, 0, 0, 0, 0, 
					      1 };
const unsigned gkDetectorListUsedWords5[] = { 1, 1, 1, 7, 1, 3, 
					      1, 1, 1, 1, 1, 1,
					      1, 1, 1, 1, 1, 1, 
					      2, 1, 0, 1, 0, 0, 
					      0, 0, 0, 0, 0, 0, 
					      1 };
const unsigned* gkDetectorListUsedWords[] = { 
    gkDetectorListUsedWords0,
    gkDetectorListUsedWords1,
    gkDetectorListUsedWords2,
    gkDetectorListUsedWords3,
    gkDetectorListUsedWords4,
    gkDetectorListUsedWords5,
};

#if 0
const unsigned gkDetectorListUsedWordsStart[][] = {
    { 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 
      0 },
    { 0, 1, 2, 3, 11, 12, 
      15, 16, 17, 18, 19, 20,
      21, 22, 23, 24, 25, 26, 
      27, 28, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 
      29 },
    { 0, 1, 2, 3, 11, 12, 
      15, 16, 17, 18, 19, 20,
      21, 22, 23, 24, 25, 26, 
      27, 28, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 
      29 },
    { 0, 1, 2, 3, 11, 12, 
      15, 16, 17, 18, 19, 20,
      21, 22, 23, 24, 25, 26, 
      27, 29, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 
      30 },
    { 0, 1, 2, 3, 11, 12, 
      15, 16, 17, 18, 19, 20,
      21, 22, 23, 24, 25, 26, 
      27, 29, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 
      30 },
    { 0, 1, 2, 3, 11, 12, 
      15, 16, 17, 18, 19, 20,
      21, 22, 23, 24, 25, 26, 
      27, 29, 0, 30, 0, 0, 
      0, 0, 0, 0, 0, 0, 
      31 },
};
const unsigned gkDetectorListUsedWordsEnd[][] = {
    { 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 
      0 },
    { 0, 1, 2, 10, 11, 14, 
      15, 16, 17, 18, 19, 20,
      21, 22, 23, 24, 25, 26, 
      27, 28, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 
      29 },
    { 0, 1, 2, 10, 11, 14, 
      15, 16, 17, 18, 19, 20,
      21, 22, 23, 24, 25, 26, 
      27, 28, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 
      29 },
    { 0, 1, 2, 10, 11, 14, 
      15, 16, 17, 18, 19, 20,
      21, 22, 23, 24, 25, 26, 
      28, 29, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 
      30 },
    { 0, 1, 2, 10, 11, 14, 
      15, 16, 17, 18, 19, 20,
      21, 22, 23, 24, 25, 26, 
      28, 29, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 
      30 },
    { 0, 1, 2, 10, 11, 14, 
      15, 16, 17, 18, 19, 20,
      21, 22, 23, 24, 25, 26, 
      28, 29, 0, 30, 0, 0, 
      0, 0, 0, 0, 0, 0, 
      31 },
};
#endif
const unsigned gkDetectorListDDLs0[] = { 0, 0, 0, 0, 0, 0, 
					 0, 0, 0, 0,   0, 0,
					 0, 0, 0, 0, 0, 0, 
					 0, 0, 0, 0, 0, 0, 0, 
					 0, 0, 0, 0, 0, 0 };
const unsigned gkDetectorListDDLs1[] = { 20, 24, 16, 216, 18, 72, 
					 14, 20, 10, 6,   20, 2,
					 3, 1, 1, 1, 1, 1, 
					 24, 32, 0, 0, 0, 0, 0, 
					 0, 0, 0, 0, 0, 10 };
const unsigned gkDetectorListDDLs2[] = { 20, 24, 16, 216, 18, 72, 
					 14, 20, 10, 6,   20, 2,
					 3, 1, 1, 1, 1, 1, 
					 24, 32, 0, 0, 0, 0, 0, 
					 0, 0, 0, 0, 0, 28 };
const unsigned gkDetectorListDDLs3[] = { 20, 24, 16, 216, 18, 72, 
					 14, 20, 10, 6,   20, 2,
					 3, 1, 1, 1, 1, 1, 
					 46, 32, 0, 0, 0, 0, 0, 
					 0, 0, 0, 0, 0, 28 };
const unsigned gkDetectorListDDLs4[] = { 20, 24, 16, 216, 18, 72, 
					 14, 21, 10, 6,   20, 2,
					 3, 1, 1, 1, 1, 1, 
					 46, 32, 0, 0, 0, 0, 0, 
					 0, 0, 0, 0, 0, 28 };
const unsigned gkDetectorListDDLs5[] = { 20, 24, 16, 216, 18, 72, 
					 14, 21, 10, 6,   20, 2,
					 3, 1, 1, 1, 1, 1, 
					 46, 32, 0, 1, 0, 0, 0, 
					 0, 0, 0, 0, 0, 28 };
const unsigned* gkDetectorListDDLs[] = { 
    gkDetectorListDDLs0,
    gkDetectorListDDLs1,
    gkDetectorListDDLs2,
    gkDetectorListDDLs3,
    gkDetectorListDDLs4,
    gkDetectorListDDLs5,
};


const hltreadout_uint32_t gkDetectorListDDLBitmap0[] = { 0x00000000, 
							 0x00000000, 
							 0x00000000, 
							 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
							 0x00000000, 
							 0x00000000, 0x00000000, 0x00000000, 
							 0x00000000, 
							 0x00000000, 
							 0x00000000, 
							 0x00000000, 
							 0x00000000, 
							 0x00000000,
							 0x00000000, 
							 0x00000000, 
							 0x00000000, 
							 0x00000000, 
							 0x00000000, 
							 0x00000000, 
							 0x00000000, 
							 0x00000000, 
							 0x00000000 };
const hltreadout_uint32_t gkDetectorListDDLBitmap1[] = { 0x000FFFFF, 
							 0x00FFFFFF, 
							 0x0000FFFF, 
							 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00FFFFFF, 0x00000000,
							 0x0003FFFF, 
							 0xFFFFFFFF, 0xFFFFFFFF, 0x000000FF, 
							 0x00003FFF, 
							 0x000FFFFF, 
							 0x000003FF, 
							 0x0000003F, 
							 0x000FFFFF, 
							 0x00000003,
							 0x00000007, 
							 0x00000001, 
							 0x00000001, 
							 0x00000001, 
							 0x00000001, 
							 0x00000001, 
							 0x00FFFFFF, 
							 0xFFFFFFFF, 
							 0x000003FF };
const hltreadout_uint32_t gkDetectorListDDLBitmap2[] = { 0x000FFFFF, 
							 0x00FFFFFF, 
							 0x0000FFFF, 
							 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00FFFFFF, 0x00000000,
							 0x0003FFFF, 
							 0xFFFFFFFF, 0xFFFFFFFF, 0x000000FF, 
							 0x00003FFF, 
							 0x000FFFFF, 
							 0x000003FF, 
							 0x0000003F, 
							 0x000FFFFF, 
							 0x00000003,
							 0x00000007, 
							 0x00000001, 
							 0x00000001, 
							 0x00000001, 
							 0x00000001, 
							 0x00000001, 
							 0x00FFFFFF, 
							 0xFFFFFFFF, 
							 0x0FFFFFFF };
const hltreadout_uint32_t gkDetectorListDDLBitmap3[] = { 0x000FFFFF, 
							 0x00FFFFFF, 
							 0x0000FFFF, 
							 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00FFFFFF, 0x00000000,
							 0x0003FFFF, 
							 0xFFFFFFFF, 0xFFFFFFFF, 0x000000FF, 
							 0x00003FFF, 
							 0x000FFFFF, 
							 0x000003FF, 
							 0x0000003F, 
							 0x000FFFFF, 
							 0x00000003,
							 0x00000007, 
							 0x00000001, 
							 0x00000001, 
							 0x00000001, 
							 0x00000001, 
							 0x00000001, 
							 0xFFFFFFFF, 0x00003FFF,
							 0xFFFFFFFF, 
							 0x0FFFFFFF };
const hltreadout_uint32_t gkDetectorListDDLBitmap4[] = { 0x000FFFFF, 
							 0x00FFFFFF, 
							 0x0000FFFF, 
							 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00FFFFFF, 0x00000000,
							 0x0003FFFF, 
							 0xFFFFFFFF, 0xFFFFFFFF, 0x000000FF, 
							 0x00003FFF, 
							 0x001FFFFF, 
							 0x000003FF, 
							 0x0000003F, 
							 0x000FFFFF, 
							 0x00000003,
							 0x00000007, 
							 0x00000001, 
							 0x00000001, 
							 0x00000001, 
							 0x00000001, 
							 0x00000001, 
							 0xFFFFFFFF, 0x00003FFF,
							 0xFFFFFFFF, 
							 0x0FFFFFFF };
const hltreadout_uint32_t gkDetectorListDDLBitmap5[] = { 0x000FFFFF, 
							 0x00FFFFFF, 
							 0x0000FFFF, 
							 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00FFFFFF, 0x00000000,
							 0x0003FFFF, 
							 0xFFFFFFFF, 0xFFFFFFFF, 0x000000FF, 
							 0x00003FFF, 
							 0x001FFFFF, 
							 0x000003FF, 
							 0x0000003F, 
							 0x000FFFFF, 
							 0x00000003,
							 0x00000007, 
							 0x00000001, 
							 0x00000001, 
							 0x00000001, 
							 0x00000001, 
							 0x00000001, 
							 0xFFFFFFFF, 0x00003FFF,
							 0xFFFFFFFF, 
							 0x1, 
							 0x0FFFFFFF };
const hltreadout_uint32_t* gkDetectorListDDLBitmap[] = { 
    gkDetectorListDDLBitmap0,
    gkDetectorListDDLBitmap1,
    gkDetectorListDDLBitmap2,
    gkDetectorListDDLBitmap3,
    gkDetectorListDDLBitmap4,
    gkDetectorListDDLBitmap5,
};


extern const hltreadout_uint32_t gkDDLIDDetectorMask = 0xFFFFFF00;
extern const hltreadout_uint32_t gkDDLIDDetectorLShift = 8;


bool AliHLTReadoutList::Reset()
    {
    if ( !fList )
	return false;
    const unsigned n=GetReadoutListDDLWordCount();
    fList[0] = n;
    memset( (void*)(fList+1), 0, sizeof(fList[1])*n );
    return true;
    }

bool AliHLTReadoutList::RevertByteOrder()
    {
    if ( !fList )
	return false;
    const unsigned n=GetReadoutListDDLWordCount()+1;
    for ( unsigned i=0; i<n; i++ )
	{
	unsigned char tmp;
	tmp = *( ((char*)(&fList[i]))+3 );
	*( ((char*)(&fList[i]))+3 ) = *( ((char*)(&fList[i]))+0 );
	*( ((char*)(&fList[i]))+0 ) = tmp;
	tmp = *( ((char*)(&fList[i]))+2 );
	*( ((char*)(&fList[i]))+2 ) = *( ((char*)(&fList[i]))+1 );
	*( ((char*)(&fList[i]))+1 ) = tmp;
	}
    return true;
    }


bool AliHLTReadoutList::SetReadoutListMask( TDetectorID detector, unsigned wordNdx, hltreadout_uint32_t value )
    {
    if ( !fList )
	return false;
    if ( detector > kMAXID )
	return false;
    if ( wordNdx >= gkDetectorListUsedWords[fReadoutListVersion][detector] )
	return false;
    unsigned wordOffset = wordNdx;
    for ( unsigned i = 0; i < (unsigned)detector; i++ )
	wordOffset += gkDetectorListWords[fReadoutListVersion][i];
    if ( (value & ~gkDetectorListDDLBitmap[fReadoutListVersion][wordOffset]) )
	return false;
    ++wordOffset; // Take word count word into account
    fList[wordOffset] = value;
    return true;
    }

bool AliHLTReadoutList::SetReadoutListMask( char const* const detector, unsigned wordNdx, hltreadout_uint32_t value )
    {
    if ( !fList )
	return false;
    TDetectorID id;
    for ( id=kFIRSTID; id<kENDID; id=(TDetectorID)((unsigned)id+1) )
	{
	if ( !strcmp( gkDetectorNames[id], detector ) )
	    break;
	}
    if ( id > kMAXID )
	return false;
    if ( wordNdx >= gkDetectorListUsedWords[fReadoutListVersion][id] )
	return false;
    unsigned wordOffset = wordNdx;
    for ( unsigned i = 0; i < (unsigned)id; i++ )
	wordOffset += gkDetectorListWords[fReadoutListVersion][i];
    if ( (value & ~gkDetectorListDDLBitmap[fReadoutListVersion][wordOffset]) )
	return false;
    ++wordOffset; // Take word count word into account
    fList[wordOffset] = value;
    return true;
    }

bool AliHLTReadoutList::SetDDL( TDetectorID detector, unsigned ddlNdx, bool set )
    {
    if ( !fList )
	return false;
    if ( detector > kMAXID )
	return false;
    if ( ddlNdx >= gkDetectorListDDLs[fReadoutListVersion][detector] )
	return false;
    unsigned wordNdx = ddlNdx / (sizeof(hltreadout_uint32_t)*8);
    unsigned ndxInWord = ddlNdx % (sizeof(hltreadout_uint32_t)*8);
    hltreadout_uint32_t bit = 1 << ndxInWord;
    unsigned wordOffset = wordNdx;
    for ( unsigned i = 0; i < (unsigned)detector; i++ )
	wordOffset += gkDetectorListWords[fReadoutListVersion][i];
    if ( (bit & ~gkDetectorListDDLBitmap[fReadoutListVersion][wordOffset]) )
	return false;
    ++wordOffset; // Take word count word into account
    if ( set )
	fList[wordOffset] = fList[wordOffset] | bit;
    else
	fList[wordOffset] = fList[wordOffset] & ~bit;
    return true;
    }

bool AliHLTReadoutList::SetDDL( char const*const detector, unsigned ddlNdx, bool set )
    {
    if ( !fList )
	return false;
    TDetectorID id;
    for ( id=kFIRSTID; id<kENDID; id=(TDetectorID)((unsigned)id+1) )
	{
	if ( !strcmp( gkDetectorNames[id], detector ) )
	    break;
	}
    if ( id > kMAXID )
	return false;
    if ( ddlNdx >= gkDetectorListDDLs[fReadoutListVersion][id] )
	return false;
    unsigned wordNdx = ddlNdx / (sizeof(hltreadout_uint32_t)*8);
    unsigned ndxInWord = ddlNdx % (sizeof(hltreadout_uint32_t)*8);
    hltreadout_uint32_t bit = 1 << ndxInWord;
    unsigned wordOffset =+wordNdx;
    for ( unsigned i = 0; i < (unsigned)id; i++ )
	wordOffset += gkDetectorListWords[fReadoutListVersion][i];
    if ( (bit & ~gkDetectorListDDLBitmap[fReadoutListVersion][wordOffset]) )
	return false;
    ++wordOffset; // Take word count word into account
    if ( set )
	fList[wordOffset] = fList[wordOffset] | bit;
    else
	fList[wordOffset] = fList[wordOffset] & ~bit;
    return true;
    }

bool AliHLTReadoutList::SetDDL( hltreadout_uint32_t ddlID, bool set )
    {
    if ( !fList )
	return false;
    hltreadout_uint32_t detectorID = (ddlID & gkDDLIDDetectorMask) >> gkDDLIDDetectorLShift;
    if ( detectorID>=kENDID )
	return false;
    hltreadout_uint32_t ddlNdx = ddlID & ~gkDDLIDDetectorMask;
    if ( ddlNdx >= gkDetectorListDDLs[fReadoutListVersion][detectorID] )
	return false;
    unsigned wordNdx = ddlNdx / (sizeof(hltreadout_uint32_t)*8);
    unsigned ndxInWord = ddlNdx % (sizeof(hltreadout_uint32_t)*8);
    hltreadout_uint32_t bit = 1 << ndxInWord;
    unsigned wordOffset = wordNdx;
    for ( unsigned i = 0; i < (unsigned)detectorID; i++ )
	wordOffset += gkDetectorListWords[fReadoutListVersion][i];
    if ( (bit & ~gkDetectorListDDLBitmap[fReadoutListVersion][wordOffset]) )
	return false;
    ++wordOffset; // Take word count word into account
    if ( set )
	fList[wordOffset] = fList[wordOffset] | bit;
    else
	fList[wordOffset] = fList[wordOffset] & ~bit;
    return true;
    }

bool AliHLTReadoutList::SetDetectorDDLs( TDetectorID detector, bool set )
    {
    if ( !fList )
	return false;
    if ( detector > kMAXID )
	return false;
    unsigned wordOffset = 0;
    volatile_hltreadout_uint32_t value = 0;
    if ( set )
	value = ~value;
    for ( unsigned i = 0; i < (unsigned)detector; i++ )
	wordOffset += gkDetectorListWords[fReadoutListVersion][i];
    for ( unsigned i = 0; i < gkDetectorListUsedWords[fReadoutListVersion][detector]; i++ )
	fList[wordOffset+i+1] = (value & gkDetectorListDDLBitmap[fReadoutListVersion][wordOffset+i]);  // Take word count word into account
    return true;
    }

bool AliHLTReadoutList::SetDetectorDDLs( char const*const detector, bool set )
    {
    if ( !fList )
	return false;
    TDetectorID id;
    for ( id=kFIRSTID; id<kENDID; id=(TDetectorID)((unsigned)id+1) )
	{
	if ( !strcmp( gkDetectorNames[id], detector ) )
	    break;
	}
    if ( id > kMAXID )
	return false;
    return SetDetectorDDLs( id, set );
    }


bool AliHLTReadoutList::GetReadoutListMask( TDetectorID detector, unsigned wordNdx, hltreadout_uint32_t& value ) const
    {
    if ( !fList )
	return false;
    if ( detector > kMAXID )
	return false;
    if ( wordNdx >= gkDetectorListUsedWords[fReadoutListVersion][detector] )
	return false;
    unsigned wordOffset = 1+wordNdx; // Start past word count word
    for ( unsigned i = 0; i < (unsigned)detector; i++ )
	wordOffset += gkDetectorListWords[fReadoutListVersion][i];
    value = fList[wordOffset];
    return true;
    }

bool AliHLTReadoutList::GetReadoutListMask( char const* const detector, unsigned wordNdx, hltreadout_uint32_t& value ) const
    {
    if ( !fList )
	return false;
    TDetectorID id;
    for ( id=kFIRSTID; id<kENDID; id=(TDetectorID)((unsigned)id+1) )
	{
	if ( !strcmp( gkDetectorNames[id], detector ) )
	    break;
	}
    if ( id > kMAXID )
	return false;
    if ( wordNdx >= gkDetectorListUsedWords[fReadoutListVersion][id] )
	return false;
    unsigned wordOffset = 1+wordNdx; // Start past word count word
    for ( unsigned i = 0; i < (unsigned)id; i++ )
	wordOffset += gkDetectorListWords[fReadoutListVersion][i];
    value = fList[wordOffset];
    return true;
    }

bool AliHLTReadoutList::GetDDL( TDetectorID detector, unsigned ddlNdx, bool& set ) const
    {
    if ( !fList )
	return false;
    if ( detector > kMAXID )
	return false;
    if ( ddlNdx >= gkDetectorListDDLs[fReadoutListVersion][detector] )
	return false;
    unsigned wordNdx = ddlNdx / (sizeof(hltreadout_uint32_t)*8);
    unsigned ndxInWord = ddlNdx % (sizeof(hltreadout_uint32_t)*8);
    hltreadout_uint32_t bit = 1 << ndxInWord;
    unsigned wordOffset = 1+wordNdx; // Start past word count word
    for ( unsigned i = 0; i < (unsigned)detector; i++ )
	wordOffset += gkDetectorListWords[fReadoutListVersion][i];
    set = fList[wordOffset] | bit;
    return true;
    }

bool AliHLTReadoutList::GetDDL( char const*const detector, unsigned ddlNdx, bool& set ) const
    {
    if ( !fList )
	return false;
    TDetectorID id;
    for ( id=kFIRSTID; id<kENDID; id=(TDetectorID)((unsigned)id+1) )
	{
	if ( !strcmp( gkDetectorNames[id], detector ) )
	    break;
	}
    if ( id > kMAXID )
	return false;
    if ( ddlNdx >= gkDetectorListDDLs[fReadoutListVersion][id] )
	return false;
    unsigned wordNdx = ddlNdx / (sizeof(hltreadout_uint32_t)*8);
    unsigned ndxInWord = ddlNdx % (sizeof(hltreadout_uint32_t)*8);
    hltreadout_uint32_t bit = 1 << ndxInWord;
    unsigned wordOffset = 1+wordNdx; // Start past word count word
    for ( unsigned i = 0; i < (unsigned)id; i++ )
	wordOffset += gkDetectorListWords[fReadoutListVersion][i];
    set = fList[wordOffset] | bit;
    return true;
    }

bool AliHLTReadoutList::GetDDL( hltreadout_uint32_t ddlID, bool& set ) const
    {
    if ( !fList )
	return false;
    hltreadout_uint32_t detectorID = (ddlID & gkDDLIDDetectorMask) >> gkDDLIDDetectorLShift;
    if ( detectorID>=kENDID )
	return false;
    hltreadout_uint32_t ddlNdx = ddlID & ~gkDDLIDDetectorMask;
    if ( ddlNdx >= gkDetectorListDDLs[fReadoutListVersion][detectorID] )
	return false;
    unsigned wordNdx = ddlNdx / (sizeof(hltreadout_uint32_t)*8);
    unsigned ndxInWord = ddlNdx % (sizeof(hltreadout_uint32_t)*8);
    hltreadout_uint32_t bit = 1 << ndxInWord;
    unsigned wordOffset = 1+wordNdx; // Start past word count word
    for ( unsigned i = 0; i < (unsigned)detectorID; i++ )
	wordOffset += gkDetectorListWords[fReadoutListVersion][i];
    set = fList[wordOffset] | bit;
    return true;
    }

bool AliHLTReadoutList::GetDetectorDDLs( TDetectorID detector, bool& set ) const
    {
    if ( !fList )
	return false;
    if ( detector > kMAXID )
	return false;
    set = false;
    unsigned wordOffset = 0;
    for ( unsigned i = 0; i < (unsigned)detector; i++ )
	wordOffset += gkDetectorListWords[fReadoutListVersion][i];
    for ( unsigned i = 0; i < gkDetectorListUsedWords[fReadoutListVersion][detector]; i++ )
	{
	if ( fList[wordOffset+i+1] & gkDetectorListDDLBitmap[fReadoutListVersion][wordOffset+i])  // Take word count word into account
	    {
	    set = true;
	    break;
	    }
	}
    return true;
    }

bool AliHLTReadoutList::GetDetectorDDLs( char const*const detector, bool& set ) const
    {
    if ( !fList )
	return false;
    TDetectorID id;
    for ( id=kFIRSTID; id<kENDID; id=(TDetectorID)((unsigned)id+1) )
	{
	if ( !strcmp( gkDetectorNames[id], detector ) )
	    break;
	}
    if ( id > kMAXID )
	return false;
    return GetDetectorDDLs( id, set );
    }


bool AliHLTReadoutList::CopyFrom( volatile_hltreadout_uint32_t* lst )
    {
    if ( !fList || !lst )
	return false;
    const unsigned n=GetReadoutListDDLWordCount()+1;
    for ( unsigned i=0; i<n; i++ )
	fList[i] = lst[i];
    return true;
    }

bool AliHLTReadoutList::Import( volatile_hltreadout_uint32_t* lst, unsigned readoutListVersion, bool resetAdditionalBits ) // Incompatible version number
    {
    if ( readoutListVersion==0 )
	{
	readoutListVersion = GetReadoutListVersion( lst );
	if ( readoutListVersion==0 )
	    return false;
	}
    if ( (readoutListVersion==fReadoutListVersion ) ||
 	 (readoutListVersion==1 && fReadoutListVersion==2) ||
 	 (readoutListVersion==2 && fReadoutListVersion==1) )
	return CopyFrom( lst );
    if ( (readoutListVersion==2 && fReadoutListVersion==3 ) ||
	 (readoutListVersion==2 && fReadoutListVersion==4 ) )
	{
	// Expand by one EMCAL word
	unsigned src=1, dest=1;
	for ( TDetectorID id=kFIRSTID; id<kENDID; id=(TDetectorID)( ((unsigned)id)+1 ) )
	    {
	    if ( id==kEMCAL )
		{
		if ( resetAdditionalBits || !( lst[src] & gkDetectorListDDLBitmap[readoutListVersion][src-1]) )
		    {
		    // Reset additional bits or no bit set in original readout list
		    for ( unsigned long i=0; i<gkDetectorListWords[readoutListVersion][id]; ++i )
			fList[dest+i] = lst[src+i];
		    fList[dest+gkDetectorListWords[readoutListVersion][id]] = 0;
		    }
		else
		    {
		    for ( unsigned long i=0; i<gkDetectorListWords[fReadoutListVersion][id]; ++i )
			fList[dest+i] = gkDetectorListDDLBitmap[fReadoutListVersion][dest+i-1];
		    }
		src += gkDetectorListWords[readoutListVersion][id];
		dest += gkDetectorListWords[fReadoutListVersion][id];
		}
	    else
		{
		for ( unsigned long i=0; i<gkDetectorListWords[fReadoutListVersion][id]; ++i )
		    fList[dest+i] = lst[src+i];
		src += gkDetectorListWords[fReadoutListVersion][id];
		dest += gkDetectorListWords[fReadoutListVersion][id];
		}
	    }
	return true;
	}
    if ( ( readoutListVersion==3 && fReadoutListVersion==2 ) ||
	 ( readoutListVersion==4 && fReadoutListVersion==2 ) )
	{
	// Shorten by one EMCAL word
	unsigned src=1, dest=1;
	for ( TDetectorID id=kFIRSTID; id<kENDID; id=(TDetectorID)( ((unsigned)id)+1 ) )
	    {
	    if ( id==kEMCAL )
		{
		for ( unsigned long i=0; i<gkDetectorListWords[fReadoutListVersion][id]; ++i )
		    fList[dest+i] = lst[src+i];
		src += gkDetectorListWords[readoutListVersion][id];
		dest += gkDetectorListWords[fReadoutListVersion][id];
		}
	    else
		{
		for ( unsigned long i=0; i<gkDetectorListWords[fReadoutListVersion][id]; ++i )
		    fList[dest+i] = lst[src+i];
		src += gkDetectorListWords[fReadoutListVersion][id];
		dest += gkDetectorListWords[fReadoutListVersion][id];
		}
	    }
	return true;
	}
    if ( (readoutListVersion==3 && fReadoutListVersion==5 ) ||
	 (readoutListVersion==4 && fReadoutListVersion==5 ) )
	{
	// Expand by one AD word
	unsigned src=1, dest=1;
	for ( TDetectorID id=kFIRSTID; id<kENDID; id=(TDetectorID)( ((unsigned)id)+1 ) )
	    {
	    if ( id==kAD )
		{
		if ( resetAdditionalBits || !( lst[src] & gkDetectorListDDLBitmap[readoutListVersion][src-1]) )
		    {
		    // Reset additional bits or no bit set in original readout list
		    for ( unsigned long i=0; i<gkDetectorListWords[readoutListVersion][id]; ++i )
			fList[dest+i] = lst[src+i];
		    fList[dest+gkDetectorListWords[readoutListVersion][id]] = 0;
		    }
		else
		    {
		    for ( unsigned long i=0; i<gkDetectorListWords[fReadoutListVersion][id]; ++i )
			fList[dest+i] = gkDetectorListDDLBitmap[fReadoutListVersion][dest+i-1];
		    }
		src += gkDetectorListWords[readoutListVersion][id];
		dest += gkDetectorListWords[fReadoutListVersion][id];
		}
	    else
		{
		for ( unsigned long i=0; i<gkDetectorListWords[fReadoutListVersion][id]; ++i )
		    fList[dest+i] = lst[src+i];
		src += gkDetectorListWords[fReadoutListVersion][id];
		dest += gkDetectorListWords[fReadoutListVersion][id];
		}
	    }
	return true;
	}
    if ( ( readoutListVersion==5 && fReadoutListVersion==3 ) ||
	 ( readoutListVersion==5 && fReadoutListVersion==4 ) )
	{
	// Shorten by one AD word
	unsigned src=1, dest=1;
	for ( TDetectorID id=kFIRSTID; id<kENDID; id=(TDetectorID)( ((unsigned)id)+1 ) )
	    {
	    if ( id==kAD )
		{
		for ( unsigned long i=0; i<gkDetectorListWords[fReadoutListVersion][id]; ++i )
		    fList[dest+i] = lst[src+i];
		src += gkDetectorListWords[readoutListVersion][id];
		dest += gkDetectorListWords[fReadoutListVersion][id];
		}
	    else
		{
		for ( unsigned long i=0; i<gkDetectorListWords[fReadoutListVersion][id]; ++i )
		    fList[dest+i] = lst[src+i];
		src += gkDetectorListWords[fReadoutListVersion][id];
		dest += gkDetectorListWords[fReadoutListVersion][id];
		}
	    }
	return true;
	}
    return false;
    }

AliHLTReadoutList& AliHLTReadoutList::operator&=( AliHLTReadoutList const& lst )
    {
    if ( !fList || !lst.fList )
	{
	Reset();
	return *this;
	}
    if ( fList[0] != lst.fList[0] )
	{
	Reset();
	return *this;
	}
    const unsigned n=GetReadoutListDDLWordCount();
    for ( unsigned i=0; i<n; i++ )
	fList[i+1] &= lst.fList[i+1];
    return *this;
    }

AliHLTReadoutList& AliHLTReadoutList::operator|=( AliHLTReadoutList const& lst )
    {
    if ( !fList || !lst.fList )
	{
	Reset();
	return *this;
	}
    if ( fList[0] != lst.fList[0] )
	{
	Reset();
	return *this;
	}
    const unsigned n=GetReadoutListDDLWordCount();
    for ( unsigned i=0; i<n; i++ )
	fList[i+1] |= lst.fList[i+1];
    return *this;
    }

AliHLTReadoutList::operator bool()
    {
    const unsigned n=GetReadoutListDDLWordCount();
    for ( unsigned i=0; i<n; i++ )
	if ( fList[i+1] )
	    return true;
    return false;
    }

// Comma separated list of DDL IDs
bool AliHLTReadoutList::ParseDDLList( char const* ddlStr )
    {
    char const* next=ddlStr;
    char* cpErr = NULL;
    unsigned long ddlID=0;
    do
	{
	ddlID = strtoul( next, &cpErr, 0 );
	switch ( *cpErr )
	    {
	    case '\0': // Fallthrough
	    case ',':
		if ( !SetDDL( ddlID ) )
		    return false;
		if ( *cpErr=='\0' )
		    return true;
		next = cpErr+1;
		if ( *next=='\0' )
		    return true;
		break;
	    case '-':
		{
		char const* nextEnd=cpErr+1;
		if ( *nextEnd=='\0' )
		    return false;
		unsigned long ddlIDEnd=0;
		char* cpErrEnd=NULL;
		ddlIDEnd = strtoul( nextEnd, &cpErrEnd, 0 );
		switch ( *cpErrEnd )
		    {
		    case '\0': // Fallthrough
		    case ',':
			for ( unsigned long id=ddlID; id<=ddlIDEnd; id++ )
			    if ( !SetDDL( id ) )
				return false;
			if ( *cpErrEnd=='\0' )
			    return true;
			next = cpErrEnd+1;
			if ( *next=='\0' )
			    return true;
			break;
		    default:
			return false;
		    }
		break;
		}
	    default:
		return false;
	    }
	}
    while ( *next!='\0' );
    return true;
    
    

#if 0
    MLUCString ddlString( ddlStr );
    std::vector<MLUCString> ddlEntries;
    ddlString.Split( ddlEntries, ',' );
    std::vector<MLUCString>::iterator iter, end;
    iter = ddlEntries.begin();
    end = ddlEntries.end();
    char* cpErr = NULL;
    unsigned long ddlID=0;
    while ( iter != end )
	{
	ddlID = strtoul( iter->c_str(), &cpErr, 0 );
	if ( *cpErr )
	    return false;
	SetDDL( ddlID );
	iter++;
	}
#endif
    return true;
    }


#if 0
// Start of string conversion. Abandoned because the need for it disappeared. Code kept here in case it appears again...

unsigned long AliHLTReadoutList::GetLengthAsString() const
    {
    return fList[0]
    }

bool AliHLTReadoutList::MakeString( char* buffer, unsigned long bufferLen ) const
    {
    int ret;
    unsigned long written = 0;
    for ( id=kFIRSTID; id<kENDID; id=(TDetectorID)((unsigned)id+1) )
	{
	unsigned detWords = GetReadoutListDetectorWordCount( id );
	if ( id==kFIRSTID )
	    ret = snprintf( buffer, bufferLen, "%s:", GetDetectorShortName(id) );
	else
	    ret = snprintf( buffer+written, bufferLen-written, " - %s: ", GetDetectorShortName(id) );
	if ( ret>0 )
	    written += ret;
	if ( written>=bufferLen )
	    return false;
	for ( unsigned word=0; word<detWords; word++ )
	    {
	    hltreadout_uint32_t& wordValue=0;
	    if ( !GetReadoutListMask( id, word, wordValue ) )
		{
		if ( word<detWords-1 )
		    ret = snprintf( buffer+written, bufferLen-written, "??????????," );
	    else if ( word<detWords-1 )
		ret = snprintf( buffer+written, bufferLen-written, "0x%08X,", ;
	    }
	}


    ret = snprintf( "%"
    }


#endif

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
