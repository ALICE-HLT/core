/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTReadoutList.hpp"
#include <iostream>
#include <cstdio>

bool gTestFailure=false;

#define TEST( test, text ) { if ( !(test) ) { std::cerr << __FILE__ << ":" << __LINE__ << ": TEST FAILURE: " << text << std::endl; gTestFailure=true; } }

int main()
    {
    unsigned testVersions[] = { 2, 3, ~0U };
    unsigned testVersionNdx=0;

    unsigned detectorOffsets[(unsigned)kENDID];

    while ( testVersions[testVersionNdx]!=~0U )
	{
	unsigned offset=0;
	for ( unsigned i=0; i<(unsigned)kENDID; ++i )
	    {
	    detectorOffsets[i] = offset;
	    offset += gkDetectorListWords[testVersions[testVersionNdx]][i];
	    }
	std::cerr << "Version: " << testVersions[testVersionNdx] << std::endl;
	AliHLTReadoutList rdLst0( testVersions[testVersionNdx] );
	TEST( (!rdLst0.Reset()), "Empty list reset" );
	TEST( (!rdLst0.SetReadoutListMask(kHLT, 0, 0x00000000)), "Empty list usage0" );
	TEST( (!rdLst0.SetReadoutListMask("HLT", 0, 0x00000000)), "Empty list usage1" );
	TEST( (!rdLst0.SetDDL(kHLT, 0)), "Empty list usage2" );
	TEST( (!rdLst0.SetDDL("HLT", 0)), "Empty list usage3" );
	hltreadout_uint32_t bitmapValue=0;
	TEST( (!rdLst0.GetReadoutListMask(kHLT, 0, bitmapValue)), "Empty list usage4" );
	TEST( (!rdLst0.GetReadoutListMask("HLT", 0, bitmapValue)), "Empty list usage5" );
	bool bitValue=false;
	TEST( (!rdLst0.GetDDL(kHLT, 0, bitValue )), "Empty list usage6" );
	TEST( (!rdLst0.GetDDL("HLT", 0, bitValue)), "Empty list usage7" );
	TEST( (rdLst0.GetReadoutListDDLWordCount()==gkReadoutListWordCounts[testVersions[testVersionNdx]]), "Readout list statis method word count" );
	hltreadout_uint32_t listValues[rdLst0.GetReadoutListDDLWordCount()+1];
	AliHLTReadoutList rdLst1(testVersions[testVersionNdx],listValues);
	TEST( (rdLst1.Reset()), "List reset" );
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kITS_SPD )==1), "ITS_SPD word count" );
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kITS_SDD )==1), "ITS_SDD word count" );
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kITS_SSD )==1), "ITS_SSD word count" );
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kTPC )==8), "TPC word count" );
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kTRD )==1), "TRD word count" );
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kTOF )==3), "TOF word count" );
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kHMPID )==1), "HMPID word count" );
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kPHOS )==1), "PHOS word count" );
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kCPV )==1), "CPV word count" );
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kPMD )==1), "PMD word count" );
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kMUON_TRK )==1), "MUON_TRK word count" );
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kMUON_TRG )==1), "MUON_TRG word count" );
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kFMD )==1), "FMD word count" );
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kT0 )==1), "T0 word count" );
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kVZERO )==1), "VZERO word count" );
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kZDC )==1), "ZDC word count" );
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kACORDE )==1), "ACORDE word count" );
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kTRG )==1), "TRG word count" );
	if ( testVersions[testVersionNdx]<3 )
	    {
	    TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kEMCAL )==1), "EMCAL word count" );
	    }
	else
	    {
	    TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kEMCAL )==2), "EMCAL word count" );
	    }
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kDAQTest )==1), "DAQ Test word count" );
	TEST( (AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], kHLT )==1), "HLT word count" );

	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kITS_SPD )==1), "ITS_SPD word count" );
	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kITS_SDD )==1), "ITS_SDD word count" );
	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kITS_SSD )==1), "ITS_SSD word count" );
	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kTPC )==8), "TPC word count" );
	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kTRD )==1), "TRD word count" );
	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kTOF )==3), "TOF word count" );
	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kHMPID )==1), "HMPID word count" );
	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kPHOS )==1), "PHOS word count" );
	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kCPV )==1), "CPV word count" );
	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kPMD )==1), "PMD word count" );
	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kMUON_TRK )==1), "MUON_TRK word count" );
	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kMUON_TRG )==1), "MUON_TRG word count" );
	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kFMD )==1), "FMD word count" );
	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kT0 )==1), "T0 word count" );
	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kVZERO )==1), "VZERO word count" );
	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kZDC )==1), "ZDC word count" );
	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kACORDE )==1), "ACORDE word count" );
	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kTRG )==1), "TRG word count" );
	if ( testVersions[testVersionNdx]<3 )
	    {
	    TEST( (rdLst0.GetReadoutListDetectorWordCount(  kEMCAL )==1), "EMCAL word count" );
	    }
	else
	    {
	    TEST( (rdLst0.GetReadoutListDetectorWordCount(  kEMCAL )==2), "EMCAL word count" );    
	    }
	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kDAQTest )==1), "DAQ Test word count" );
	TEST( (rdLst0.GetReadoutListDetectorWordCount(  kHLT )==1), "HLT word count" );
	
	for ( unsigned id=(unsigned)kFIRSTID; id<(unsigned)kENDID; id++ )
	    {
	    for ( unsigned word=0; word<AliHLTReadoutList::GetReadoutListDetectorWordCount( testVersions[testVersionNdx], (TDetectorID)id ); word++ )
		{
		char tmp[4096];
		sprintf( tmp, "List read 0 %u - %u", id, word );
		if ( (TDetectorID)id==kTPC && word==7 )
		    TEST( (!rdLst1.GetReadoutListMask( (TDetectorID)id, word, bitmapValue)), tmp )
		    else
			{
			TEST( (rdLst1.GetReadoutListMask( (TDetectorID)id, word, bitmapValue)), tmp );
			sprintf( tmp, "Read value 0 %u - %u", id, word );
			TEST( (bitmapValue==0), tmp );
			}
		sprintf( tmp, "List write 0 %u - %u", id, word );
		if ( (TDetectorID)id==kTPC && word==7 )
		    {
		    TEST( (!rdLst1.SetReadoutListMask( (TDetectorID)id, word, 1)), tmp );
		    }
		else
		    {
		    TEST( (rdLst1.SetReadoutListMask( (TDetectorID)id, word, 1)), tmp );
		    }
		sprintf( tmp, "List read 1 %u - %u", id, word );
		if ( (TDetectorID)id==kTPC && word==7 )
		    {
		    TEST( (!rdLst1.GetReadoutListMask( (TDetectorID)id, word, bitmapValue)), tmp );
		    }
		else
		    {
		    TEST( (rdLst1.GetReadoutListMask( (TDetectorID)id, word, bitmapValue)), tmp );
		    sprintf( tmp, "Read value 0 %u - %u", id, word );
		    TEST( (bitmapValue==1), tmp );
		    }
		}
	    }
	
	TEST( (rdLst1.Reset()), "List reset" );
	for ( unsigned id=(unsigned)kFIRSTID; id<(unsigned)kENDID; id++ )
	    {
	    char tmp[4096];
	    if ( rdLst1.GetReadoutListDetectorWordCount( (TDetectorID)id )<=0 )
		continue;
	    sprintf( tmp, "DDL set 0 %u", id );
	    TEST( (rdLst1.SetDDL( (TDetectorID)id, 0 )), tmp );
	    sprintf( tmp, "List read 2 %u", id );
	    TEST( (rdLst1.GetReadoutListMask( (TDetectorID)id, 0, bitmapValue)), tmp );
	    sprintf( tmp, "Read value 2 %u", id );
	    TEST( (bitmapValue==1), tmp );
	    }

	TEST( (rdLst1.Reset()), "List reset" );
	for ( unsigned id=(unsigned)kFIRSTID; id<(unsigned)kENDID; id++ )
	    {
	    if ( rdLst1.GetReadoutListDetectorWordCount( (TDetectorID)id )<=0 )
		continue;
	    char tmp[4096];
	    sprintf( tmp, "DDL set 2 %u", id );
	    TEST( (rdLst1.SetDDL( (TDetectorID)id, gkDetectorListDDLs[testVersions[testVersionNdx]][(TDetectorID)id]-1 )), tmp );
	    sprintf( tmp, "List read 4 %u", id );
	    TEST( (rdLst1.GetReadoutListMask( (TDetectorID)id, gkDetectorListUsedWords[testVersions[testVersionNdx]][(TDetectorID)id]-1, bitmapValue)), tmp );
	    sprintf( tmp, "Read value 4 %u", id );
	    TEST( (bitmapValue==(1U << ((gkDetectorListDDLs[testVersions[testVersionNdx]][(TDetectorID)id]-1)%(sizeof(hltreadout_uint32_t)*8)))), tmp );
	
	    }
	TEST( (rdLst1.Reset()), "List reset" );
	TEST( (rdLst1.SetDetectorDDLs( kITS_SPD )), "SetDetectorDDLs - SPD 0" );
	TEST( rdLst1.GetReadoutListMask( kITS_SPD, 0, bitmapValue ), "GetReadoutListMask" );
	TEST( (bitmapValue==0x000FFFFF), "SPD Readout list" );
	TEST( (rdLst1.Reset()), "List reset" );
	TEST( (rdLst1.SetDetectorDDLs( kTPC )), "SetDetectorDDLs - TPC 0" );
	TEST( rdLst1.GetReadoutListMask( kTPC, 0, bitmapValue ), "GetReadoutListMask" );
	TEST( (bitmapValue==0xFFFFFFFF), "TPC Readout list 0" );
	TEST( rdLst1.GetReadoutListMask( kTPC, 6, bitmapValue ), "GetReadoutListMask" );
	TEST( (bitmapValue==0x00FFFFFF), "TPC Readout list 6" );

	TEST( (rdLst1.Reset()), "List reset" );
	TEST( (rdLst1.SetDetectorDDLs( "ITSSPD" )), "SetDetectorDDLs - SPD 1" );
	TEST( rdLst1.GetReadoutListMask( kITS_SPD, 0, bitmapValue ), "GetReadoutListMask" );
	TEST( (bitmapValue==0x000FFFFF), "SPD Readout list" );
	TEST( (rdLst1.Reset()), "List reset" );
	TEST( (rdLst1.SetDetectorDDLs( "TPC" )), "SetDetectorDDLs - TPC 1" );
	TEST( rdLst1.GetReadoutListMask( kTPC, 0, bitmapValue ), "GetReadoutListMask" );
	TEST( (bitmapValue==0xFFFFFFFF), "TPC Readout list 0" );
	TEST( rdLst1.GetReadoutListMask( kTPC, 6, bitmapValue ), "GetReadoutListMask" );
	TEST( (bitmapValue==0x00FFFFFF), "TPC Readout list 6" );


	TEST( (rdLst1.Reset()), "List reset" );
	TEST( (rdLst1.SetDDL( "ITSSPD", 0 )), "SetDDL - SPD 1" );
	TEST( (rdLst1.GetDetectorDDLs( "ITSSPD", bitValue )), "GetDetectorDDLs - SPD 1" );
	TEST( bitValue, "GetDetectorDDLs Result - SPD 1" );
    

	TEST( (rdLst1.Reset()), "List reset" );
	for ( unsigned id=(unsigned)kFIRSTID; id<(unsigned)kENDID; id++ )
	    {
	    if ( rdLst1.GetReadoutListDetectorWordCount( (TDetectorID)id )<=0 )
		continue;
	    char tmp[4096];
	    for ( unsigned ddl=0; ddl<rdLst1.GetReadoutListDetectorDDLCount( (TDetectorID)id ); ddl++ )
		{
		sprintf( tmp, "DDL set 3 %u - %u", id, ddl );
		TEST( (rdLst1.SetDDL( (TDetectorID)id, ddl )), tmp );
		}
	    for ( unsigned word=0; word<rdLst1.GetReadoutListDetectorWordCount( (TDetectorID)id ); word++ )
		{
		sprintf( tmp, "List read 5 %u - %u", id, word );
		if ( word<gkDetectorListUsedWords[testVersions[testVersionNdx]][(TDetectorID)id] )
		    {
		    TEST( (rdLst1.GetReadoutListMask( (TDetectorID)id, word, bitmapValue)), tmp );
		    //std::cout << tmp << ": " << std::hex << bitmapValue << std::endl;
		    }
		else
		    TEST( (!rdLst1.GetReadoutListMask( (TDetectorID)id, word, bitmapValue)), tmp );
		sprintf( tmp, "Read value 5 %u", id );
		if ( word<gkDetectorListUsedWords[testVersions[testVersionNdx]][(TDetectorID)id] )
		    TEST( (bitmapValue==gkDetectorListDDLBitmap[testVersions[testVersionNdx]][detectorOffsets[id]+word]), tmp );
		}
	    }

	TEST( (rdLst1.Reset()), "List reset" );
	TEST( (!rdLst1), "List bool 0" );

	hltreadout_uint32_t listValues2[AliHLTReadoutList::GetReadoutListDDLWordCount(testVersions[testVersionNdx])+1];
	AliHLTReadoutList rdLst2(testVersions[testVersionNdx],listValues2);
	TEST( (rdLst2.Reset()), "List reset" );

	rdLst1.SetDDL( 1 );
	rdLst1.SetDDL( 2 );
	rdLst2 = rdLst1;
	TEST( (rdLst1), "List bool 1" );
	TEST( (listValues2[1] == 0x00000006), "Assignment test 0" );
	TEST( (listValues2[2] == 0x00000000), "Assignment test 1" );
    
	TEST( (rdLst1.Reset()), "List reset" );
	TEST( (rdLst2.Reset()), "List reset" );
    
	rdLst1.SetDDL( 1 );
	rdLst1.SetDDL( 2 );
	rdLst2.SetDDL( 2 );
	rdLst2.SetDDL( 3 );
    
	rdLst2 &= rdLst1;
	TEST( (listValues2[1] == 0x00000004), "AND test 0" );
	TEST( (listValues2[2] == 0x00000000), "AND test 1" );
	TEST( (rdLst2), "List bool 1" );


	TEST( (rdLst1.Reset()), "List reset" );
	TEST( (rdLst2.Reset()), "List reset" );
    
	rdLst1.SetDDL( 1 );
	rdLst1.SetDDL( 2 );
	rdLst2.SetDDL( 2 );
	rdLst2.SetDDL( 3 );
    
	rdLst2 |= rdLst1;
	TEST( (listValues2[1] == 0x0000000E), "OR test 0" );
	TEST( (listValues2[2] == 0x00000000), "OR test 1" );
    

	TEST( (rdLst1.Reset()), "List reset" );
	TEST( (rdLst1.ParseDDLList( "1,2" )), "Parsing 0" );
	TEST( (listValues[1] == 0x00000006), "Parse test 0" );
	TEST( (listValues[2] == 0x00000000), "Parse test 1" );


	TEST( (rdLst1.Reset()), "List reset" );
	TEST( (rdLst1.ParseDDLList( "1,2,3,256-263" )), "Parsing 1" );
	TEST( (listValues[1] == 0x0000000E), "Parse test 2" );
	TEST( (listValues[2] == 0x000000FF), "Parse test 3" );
	TEST( (listValues[3] == 0x00000000), "Parse test 4" );

	TEST( (rdLst1.Reset()), "List reset" );
	TEST( (!rdLst1.ParseDDLList( "1,2,3,256-A" )), "Parsing 2" );
	TEST( (!rdLst1.ParseDDLList( "A" )), "Parsing 3" );
	++testVersionNdx;
	}
    // Test conversion
    volatile_hltreadout_uint32_t rdLstData0[gkReadoutListWordCounts[3]+1];
    AliHLTReadoutList rdLst0( 3, rdLstData0 );
    volatile_hltreadout_uint32_t rdLstData1[gkReadoutListWordCounts[2]+1];
    AliHLTReadoutList rdLst1( 2, rdLstData1 );

    rdLst1.Reset();
    for ( TDetectorID id=kFIRSTID; id<kENDID; id=(TDetectorID)( ((unsigned)id)+1 ) )
	{
	rdLst1.SetDetectorDDLs( id );
	}

    rdLst0.Reset();
    TEST( (rdLst0.Import( rdLstData1, 2 )), "Import 0" );
    TEST( (rdLstData0[27]==gkDetectorListDDLBitmap[3][26]), "Import 0 TRG word 0" );
    //std::cerr << std::hex << "0x" << rdLstData0[28] << " - 0x" << gkDetectorListDDLBitmap[3][27] << std::endl;
    TEST( (rdLstData0[28]==gkDetectorListDDLBitmap[3][27]), "Import 0 EMCAL word 0" );
    TEST( (rdLstData0[29]==gkDetectorListDDLBitmap[3][28]), "Import 0 EMCAL word 1" );
    TEST( (rdLstData0[31]==gkDetectorListDDLBitmap[3][30]), "Import 0 HLT word 0" );

    rdLst0.Reset();
    TEST( (rdLst0.Import( rdLstData1, 2, true )), "Import 1" );
    TEST( (rdLstData0[27]==gkDetectorListDDLBitmap[3][26]), "Import 0 TRG word 0" );
    TEST( (rdLstData0[28]==rdLstData1[28]), "Import 1 EMCAL word 0" );
    TEST( (rdLstData0[29]==0), "Import 1 EMCAL word 1" );
    TEST( (rdLstData0[31]==gkDetectorListDDLBitmap[3][30]), "Import 0 HLT word 0" );

    rdLst0.Reset();
    rdLst1.SetDetectorDDLs( kEMCAL, false );
    TEST( (rdLst0.Import( rdLstData1, 2 )), "Import 2" );
    TEST( (rdLstData0[27]==gkDetectorListDDLBitmap[3][26]), "Import 0 TRG word 0" );
    TEST( (rdLstData0[28]==0), "Import 1 EMCAL word 0" );
    TEST( (rdLstData0[29]==0), "Import 2 EMCAL word 1" );
    TEST( (rdLstData0[31]==gkDetectorListDDLBitmap[3][30]), "Import 0 HLT word 0" );


    if ( gTestFailure )
	return -1;
    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
