#ifndef _ALIHLTREADOUTLIST_HPP_
#define _ALIHLTREADOUTLIST_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <climits>

#if !defined(USE_ROOT) && !defined(__CINT__)

// First hltreadout_uint32_t
#if USHRT_MAX==4294967295
typedef unsigned short hltreadout_uint32_t;
#else // USHRT_MAX==4294967295

#if UINT_MAX==4294967295
typedef unsigned int hltreadout_uint32_t;
#else // UINT_MAX==4294967295

#if ULONG_MAX==4294967295l
typedef unsigned long hltreadout_uint32_t;
#else // ULONG_MAX==4294967295l

#error Could not typedef hltreadout_uint32_t

#endif // ULONG_MAX==4294967295l

#endif // UINT_MAX==4294967295

#endif // USHRT_MAX==4294967295

#else // !USE_ROOT && !CINT
typedef ULong_t hltreadout_uint32_t;

#endif // USE_ROOT
typedef volatile hltreadout_uint32_t volatile_hltreadout_uint32_t;


#define READOUTLIST_VERSION1_WORDCOUNT 29U
#define READOUTLIST_VERSION2_WORDCOUNT 30U
//+DCAL
#define READOUTLIST_VERSION3_WORDCOUNT 31U
//+PHOS STU
#define READOUTLIST_VERSION4_WORDCOUNT 31U
//+AD
#define READOUTLIST_VERSION5_WORDCOUNT 32U

#define READOUTLIST_MAX_WORDCOUNT READOUTLIST_VERSION5_WORDCOUNT

typedef hltreadout_uint32_t AliHLTReadoutListData[READOUTLIST_MAX_WORDCOUNT+1];

enum TDetectorID { kFIRSTID=0, kITS_SPD=0, kITS_SDD=1, kITS_SSD=2, kTPC=3,
		   kTRD=4, kTOF=5, kHMPID=6, kPHOS=7, kCPV=8, 
		   kPMD=9, kMUON_TRK=10, kMUON_TRG=11, kFMD=12,
		   kT0=13, kVZERO=14, kZDC=15, kACORDE=16, kTRG=17,
		   kEMCAL=18, kDAQTest=19, kUnused20=20, kAD=21,
		   kUnused22=22, kUnused23=23, kUnused24=24, kUnused25=25,
		   kUnused26=26, kUnused27=27, kUnused28=28, kUnused29=29,
		   kHLT=30, kMAXID=30, kENDID=31 };

extern const unsigned gkReadoutListVersions;
extern const unsigned gkReadoutListWordCounts[];

extern const char* gkDetectorNames[];
extern const char* gkDetectorShortNames[];
extern const unsigned* gkDetectorListWords[];
extern const unsigned* gkDetectorListUsedWords[];
extern const unsigned* gkDetectorListDDLs[];
extern const hltreadout_uint32_t* gkDetectorListDDLBitmap[];

extern const hltreadout_uint32_t gkDDLIDDetectorMask;
extern const hltreadout_uint32_t gkDDLIDDetectorLShift;



class AliHLTReadoutList
    {
    public:

	AliHLTReadoutList( unsigned readoutListVersion, volatile_hltreadout_uint32_t* lst=0 ):
	    fReadoutListVersion( readoutListVersion ),
	    fList( (volatile_hltreadout_uint32_t*)lst )
		{
		}
	~AliHLTReadoutList() {}

	void SetList( hltreadout_uint32_t* lst )
		{
		fList = lst;
		}

	static bool IsVersionSupported( unsigned readoutListVersion )
		{
		return readoutListVersion>0 && readoutListVersion<=gkReadoutListVersions;
		}
	bool IsVersionSupported()
		{
		return IsVersionSupported( fReadoutListVersion );
		}
	static unsigned GetReadoutlistVersionFromSize( unsigned long readoutListSize )
		{
		for ( unsigned v=gkReadoutListVersions; v>0; --v )
		    {
		    if ( (gkReadoutListWordCounts[v]+1)*sizeof(hltreadout_uint32_t)==readoutListSize )
			return v;
		    }
		return 0;
		}
	static unsigned GetReadoutlistVersionFromWordCount( unsigned long readoutListWordCount )
		{
		for ( unsigned v=gkReadoutListVersions; v>0; --v )
		    {
		    if ( gkReadoutListWordCounts[v]==readoutListWordCount )
			return v;
		    }
		return 0;
		}
	static unsigned GetReadoutListVersion( volatile_hltreadout_uint32_t* lst )
		{
		if ( !lst )
		    return 0;
		for ( unsigned v=gkReadoutListVersions; v>0; --v )
		    {
		    if ( gkReadoutListWordCounts[v]==lst[0] )
			return v;
		    }
		return 0;
		}


	static unsigned long GetReadoutListSize( unsigned readoutListVersion )
		{
		if ( readoutListVersion<=gkReadoutListVersions )
		    return (gkReadoutListWordCounts[readoutListVersion]+1)*sizeof(hltreadout_uint32_t);
		return 0;
		}

	static unsigned GetReadoutListDDLWordCount( unsigned readoutListVersion )
		{
		if ( readoutListVersion<=gkReadoutListVersions )
		    return gkReadoutListWordCounts[readoutListVersion];
		return 0;
		}

	static unsigned GetReadoutListDetectorWordCount( unsigned readoutListVersion, TDetectorID id )
		{
		if ( readoutListVersion<=gkReadoutListVersions && id<kENDID )
		    return gkDetectorListWords[readoutListVersion][(unsigned)id];
		return 0;
		}
	static unsigned GetReadoutListDetectorDDLCount( unsigned readoutListVersion, TDetectorID id )
		{
		if ( readoutListVersion<=gkReadoutListVersions && id<kENDID )
		    return gkDetectorListDDLs[readoutListVersion][(unsigned)id];
		return 0;
		}
	static const char* GetDetectorShortName( TDetectorID id )
		{
		if ( id<kENDID )
		    return gkDetectorShortNames[(unsigned)id];
		return 0;
		}
	static const char* GetDetectorName( TDetectorID id )
		{
		if ( id<kENDID )
		    return gkDetectorNames[(unsigned)id];
		return 0;
		}

	static TDetectorID GetDetectorID( hltreadout_uint32_t ddlID )
		{
		if ( ((ddlID & gkDDLIDDetectorMask)>>gkDDLIDDetectorLShift)<kENDID )
		    return (TDetectorID)( (ddlID & gkDDLIDDetectorMask)>>gkDDLIDDetectorLShift );
		return (TDetectorID)( 0xFFFFFFFF >> gkDDLIDDetectorLShift );
		}
	static unsigned GetDetectorDDLNr( hltreadout_uint32_t ddlID )
		{
		return ddlID & ~gkDDLIDDetectorMask;
		}
	static unsigned GetTPCSlice( hltreadout_uint32_t ddlID )
		{
		if ( ((ddlID & gkDDLIDDetectorMask)>>gkDDLIDDetectorLShift) != kTPC )
		    return ~0U;
		if (ddlID < (unsigned)( (kTPC<<gkDDLIDDetectorLShift)|72 ) )
		    return (ddlID & ~gkDDLIDDetectorMask) / 2;
		else
		    return ((ddlID & ~gkDDLIDDetectorMask)-72) / 4;
		}
	static unsigned GetTPCPatch( hltreadout_uint32_t ddlID )
		{
		if ( ((ddlID & gkDDLIDDetectorMask)>>gkDDLIDDetectorLShift) != kTPC )
		    return ~0U;
		if (ddlID < (unsigned)( (kTPC<<gkDDLIDDetectorLShift)|72 ) )
		    return (ddlID & ~gkDDLIDDetectorMask) % 2;
		else
		    return (((ddlID & ~gkDDLIDDetectorMask)-72) % 4)+2;
		}

	unsigned GetReadoutListVersion()
		{
		return fReadoutListVersion;
		}
	unsigned long GetReadoutListSize()
		{
		return GetReadoutListSize( fReadoutListVersion );
		}

	unsigned GetReadoutListDDLWordCount()
		{
		return GetReadoutListDDLWordCount( fReadoutListVersion );
		}

	unsigned GetReadoutListDetectorWordCount( TDetectorID id )
		{
		return GetReadoutListDetectorWordCount( fReadoutListVersion, id );
		}
	unsigned GetReadoutListDetectorDDLCount( TDetectorID id )
		{
		return GetReadoutListDetectorDDLCount( fReadoutListVersion, id );
		}

	bool Reset();

	bool RevertByteOrder();

	bool SetReadoutListMask( TDetectorID detector, unsigned wordNdx, hltreadout_uint32_t value );
	bool SetReadoutListMask( char const* const detector, unsigned wordNdx, hltreadout_uint32_t value );
	bool SetDDL( TDetectorID detector, unsigned ddlNdx, bool set = true );
	bool SetDDL( char const*const detector, unsigned ddlNdx, bool set = true );
	bool SetDDL( hltreadout_uint32_t ddlID, bool set = true );
	bool SetDetectorDDLs( TDetectorID detector, bool set = true );
	bool SetDetectorDDLs( char const*const detector, bool set = true );

	bool GetReadoutListMask( TDetectorID detector, unsigned wordNdx, hltreadout_uint32_t& value ) const;
	bool GetReadoutListMask( char const* const detector, unsigned wordNdx, hltreadout_uint32_t& value ) const;
	bool GetDDL( TDetectorID detector, unsigned ddlNdx, bool& set ) const;
	bool GetDDL( char const*const detector, unsigned ddlNdx, bool& set ) const;
	bool GetDDL( hltreadout_uint32_t ddlID, bool& set ) const;
	bool GetDetectorDDLs( TDetectorID detector, bool& set ) const; // All detector DDls ORed
	bool GetDetectorDDLs( char const*const detector, bool& set ) const; // All detector DDls ORed

	bool CopyFrom( volatile_hltreadout_uint32_t* lst ); // Compatible version number
        bool Import( volatile_hltreadout_uint32_t* lst, unsigned readoutListVersion=0, bool resetAdditionalBits=false ); // Incompatible version number, readoutListVersion==0 : try to determine version from lis size
	AliHLTReadoutList& operator=( AliHLTReadoutList const& lst ) { CopyFrom( lst.fList ); return *this; }

	AliHLTReadoutList& operator&=( AliHLTReadoutList const& lst );
	AliHLTReadoutList& operator|=( AliHLTReadoutList const& lst );

	operator bool();

	bool ParseDDLList( char const* bitString ); // Comma separated list of DDL IDs

#if 0
	// Start of string conversion. Abandoned because the need for it disappeared. Code kept here in case it appears again...
	unsigned long GetLengthAsString() const;
	bool MakeString( char* buffer, unsigned long bufferLen ) const;
#endif
	

    protected:

	unsigned fReadoutListVersion;

	volatile_hltreadout_uint32_t* fList;

    private:

	AliHLTReadoutList( AliHLTReadoutList const& ) {}
	
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _ALIHLTREADOUTLIST_HPP_
