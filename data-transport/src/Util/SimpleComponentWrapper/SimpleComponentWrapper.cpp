
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <vector>
#include <fstream>
#include "MLUCString.hpp"
#include "MLUCCmdlineParser.hpp"
#include "MLUCDynamicLibrary.hpp"
#include "MLUCLog.hpp"
#include "MLUCLogServer.hpp"
#include "MLUCPlatformAttributes.hpp"
#include "AliHLTDataTypes.h"
#include "AliRootInterfaceLibrary.hpp"
#include "AliHLTReadoutList.hpp"
#include "NOPEEvents.hpp"
#include "CommonDataHeader.hpp"

#define printcdh( cdh ) printf( "%s : 0x%08lX - 0x%08lX - 0x%08lX - 0x%08lX - 0x%08lX - 0x%08lX - 0x%08lX - 0x%08lX\n", \
    #cdh, \
				(unsigned long)cdh[0], \
				(unsigned long)cdh[1], \
				(unsigned long)cdh[2], \
				(unsigned long)cdh[3], \
				(unsigned long)cdh[4], \
				(unsigned long)cdh[5], \
				(unsigned long)cdh[6], \
				(unsigned long)cdh[7] )


AliHLTComponentLogSeverity gVerbosity = (AliHLTComponentLogSeverity)0xFF;

void* AllocMemory( void*, unsigned long size )
    {
    return (void*)malloc( size );
    }

void FreeMemory( void*, void* ptr )
    {
    free( ptr );
    }

int GetEventDoneData( void* param, AliHLTEventID_t, unsigned long dataSize, AliHLTComponentEventDoneData** edd )
    {
    *edd = reinterpret_cast<AliHLTComponentEventDoneData*>( AllocMemory( param, sizeof(AliHLTComponentEventDoneData)+dataSize ) );
    if ( !*edd )
	return ENOMEM;
    (*edd)->fStructSize = sizeof(AliHLTComponentEventDoneData);
    (*edd)->fDataSize = dataSize;
    (*edd)->fData = (void*)( ((uint8*)(*edd))+sizeof(AliHLTComponentEventDoneData) );
    return 0;
    }

int Logging( void*, AliHLTComponentLogSeverity severity, const char* origin, const char* keyword, const char* message )
    {
#if 1
    LOG( (MLUCLog::TLogLevel)severity, origin, keyword )
	<< message << ENDLOG;
#else
    if ( severity & gVerbosity )
	printf( "Level: %d - %s - %s: %s\n", (int)severity, origin, keyword, message );
#endif
    return 0;
    }

struct BlockData
    {
	AliHLTComponentBlockData fBlock;
	uint8* fRealPtr;
	unsigned long fEvtOffset;
    };

struct EventData
    {
	std::vector<BlockData> fBlocks;
	std::vector<std::string> fOutFiles;
	std::string fEDDOutFile;
	std::string fEventTriggerOutFile;
	std::vector<AliHLTEventTriggerData*> fEventTriggerData;
	uint8* fOutputPtr;
	AliHLTComponentBlockData *fInputBlocks;
	AliHLTUInt32_t fOutputSize;
#if 0
	unsigned long fEvtOffset;
#endif
	uint32 fEventType;
	bool fHasNOPEBlocks;
	EventData()
		{
		Reset();
		}
	void Reset()
		{
		fBlocks.clear();
		fEventTriggerData.clear();
		fOutFiles.clear();
		fOutputPtr = NULL;
		fInputBlocks = NULL;
		fOutputSize = 0;
		fHasNOPEBlocks = false;
		fEventType = gkAliEventTypeData;
		fEDDOutFile = "";
		fEventTriggerOutFile = "";
		}
    };


int main( int argc, char** argv )
    {
    MLUCFilteredStdoutLogServer logStdout;
    gLog.AddServer( logStdout );

    int inFH = -1;

    AliRootInterface aliRootInterface;

    int i = 1;
    const char* error = NULL;
    int errorArg = -1;
    int errorParam = -1;
    const char* componentID = NULL;
    const char* compOptions = "";
    const char* compInfo = "";
    AliHLTComponentDataType dataType;
    AliHLTUInt32_t dataSpec = ~(AliHLTUInt32_t)0;
    unsigned long ddlID = ~0UL;
    dataType.fStructSize = sizeof(dataType);
    memset( dataType.fID, '*', 8 );
    memset( dataType.fOrigin, '*', 4 );
    //std::vector<AliHLTComponentBlockData> blocks;
    std::vector<EventData> events;
    EventData curEvent;
    unsigned long totalRead = 0;
    unsigned long eventIterations = 1;
    unsigned long iterations = 1;
    unsigned long eventDuplicates = 1;
    unsigned long evtOffset = 0;
    MLUCString errorStringText;
    unsigned long runNumber = 0;
    unsigned long beamType = 0;
    unsigned long hltMode = 0;
    const char* runType="";
    const char* ecsParameters = "";
    bool inputFileUsedAsEventTriggerData = false;
    MLUCLog::gLogLevel = 0xFF;
    unsigned readoutListVersion = gkReadoutListVersions;
    AliRootInterface::TDummyInterface* dummyInterface = NULL;
    bool showInputBlocks = false;


    while ( i < argc )
	{
	if ( !strcmp( argv[i], "-dummymode" ) )
	    {
	    dummyInterface = new AliRootInterface::TDummyInterface( true );
	    aliRootInterface.SetDummyInterface( dummyInterface );
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-showinputblocks" ) )
	    {
	    showInputBlocks = true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-readoutlistversion" ) )
	    {
	    if ( argc <= i +1 )
		{
		error = "Missing readout list version specifier.";
		errorArg = i;
		break;
		}
	    char* cpErr;
	    readoutListVersion = (unsigned)strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		error = "Error converting readout list version specifier.";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-runnr" ) )
	    {
	    if ( argc <= i +1 )
		{
		error = "Missing run number specifier.";
		errorArg = i;
		break;
		}
	    char* cpErr;
	    runNumber = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		error = "Error converting run number specifier.";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-runnumber" ) )
	    {
	    if ( argc <= i +1 )
		{
		error = "Missing run number specifier.";
		errorArg = i;
		break;
		}
	    char* cpErr;
	    runNumber = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		error = "Error converting run number specifier.";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-beamtype" ) )
	    {
	    if ( argc <= i + 1 )
		{
		error = "Missing beam type specifier.";
		errorArg = i;
		break;
		}
	    beamType = 0;
	    if ( !strcmp(argv[i+1], "pp") )
		beamType = 1;
	    else if ( !strcmp(argv[i+1], "AA") )
		beamType = 2;
	    else if ( !strcmp(argv[i+1], "pA") )
		beamType = 3;
	    else
		{
		error = "Wrong first parameter for beam type (must be 'pp' or 'AA').";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-hltmode" ) )
	    {
	    if ( argc <= i + 1 )
		{
		error = "Missing HLT mode specifier.";
		errorArg = i;
		break;
		}
	    hltMode = 0;
	    if ( !strcmp(argv[i+1],"A") )
		hltMode = 1;
	    else if ( !strcmp(argv[i+1],"B") )
		hltMode = 2;
	    else if ( !strcmp(argv[i+1],"C") )
		hltMode = 3;
	    else if ( !strcmp(argv[i+1],"D") )
		hltMode = 4;
	    else if ( !strcmp(argv[i+1],"E") )
		hltMode = 5;
	    else
		{
		error = "Wrong second parameter for HLT mode (must be 'A', 'B', 'C', 'D' or 'E').";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-runtype" ) )
	    {
	    if ( argc <= i + 1 )
		{
		error = "Missing run type specifier.";
		errorArg = i;
		break;
		}
	    runType = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-ecsparameters" ) )
	    {
	    if ( argc <= i + 1 )
		{
		error = "Missing ECS parameter specifier.";
		errorArg = i;
		break;
		}
	    ecsParameters = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( "-infile", argv[i] ) )
	    {
	    if ( i+1>=argc )
		{
		error = "Missing input filename";
		errorArg = i;
		break;
		}
	    bool isNopeEvent = false;
	    int nopeRet = TNOPEEvent::IsNopeEvent( argv[i+1], isNopeEvent );
	    if ( nopeRet )
		{
		error = "Cannot determine NOPE event status for input file";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    if ( !isNopeEvent )
		{
		inFH = open( argv[i+1], O_RDONLY );
		if ( inFH == -1 )
		    {
		    error = "Unable to open input file for reading";
		    errorArg = i;
		    errorParam = i+1;
		    break;
		    }
		BlockData newBlock;
		newBlock.fBlock.fStructSize = sizeof(AliHLTComponentBlockData);
		memcpy( &(newBlock.fBlock.fDataType), &dataType, sizeof(dataType) );
		newBlock.fBlock.fSpecification = dataSpec;
		newBlock.fBlock.fShmKey.fStructSize = sizeof(AliHLTComponentShmData);
		newBlock.fBlock.fShmKey.fShmType = gkAliHLTComponentInvalidShmType;
		newBlock.fBlock.fShmKey.fShmID = gkAliHLTComponentInvalidShmID;
		newBlock.fEvtOffset = evtOffset;
		off_t sz = lseek( inFH, 0, SEEK_END );
		lseek( inFH, 0, SEEK_SET );
		newBlock.fRealPtr = new uint8[ sz+newBlock.fEvtOffset ];
		if ( !newBlock.fRealPtr )
		    {
		    fprintf( stderr, "Out of memory trying to allocate memory for input file '%s' of %lu bytes.\n",
			     argv[i+1], sz+newBlock.fEvtOffset );
		    return -1;
		    }
		newBlock.fBlock.fPtr = newBlock.fRealPtr+newBlock.fEvtOffset;
		newBlock.fBlock.fOffset = 0;
		unsigned long curSize = 0;
		int ret;
		do
		    {
		    ret = read( inFH, ((uint8*)newBlock.fBlock.fPtr)+curSize, sz-curSize );
		    if ( ret >= 0 )
			{
			curSize += (unsigned long)ret;
			if ( curSize >= (unsigned long)sz )
			    {
			    newBlock.fBlock.fSize = sz;
			    curEvent.fBlocks.push_back( newBlock );
			    break;
			    }
			}
		    else
			{
			fprintf( stderr, "%s error reading data from input file %s after %lu bytes.\n", 
				 argv[0], argv[i+1], curSize );
			return -1;
			}
		    }
		while ( ret >0 );
		close( inFH );
		inputFileUsedAsEventTriggerData = false;
		}
	    else
		curEvent.fHasNOPEBlocks = true;
	    i += 2;
	    continue;
	    }
	if ( !strcmp( "-outfile", argv[i] ) )
	    {
	    if ( i+1>=argc )
		{
		error = "Missing output filename";
		errorArg = i;
		break;
		}
	    curEvent.fOutFiles.push_back( argv[i+1] );
	    i += 2;
	    continue;
	    }
	if ( !strcmp( "-eventdoneoutfile", argv[i] ) )
	    {
	    if ( i+1>=argc )
		{
		error = "Missing event done data output filename";
		errorArg = i;
		break;
		}
	    curEvent.fEDDOutFile = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( "-eventtriggeroutfile", argv[i] ) )
	    {
	    if ( i+1>=argc )
		{
		error = "Missing event trigger data output filename";
		errorArg = i;
		break;
		}
	    curEvent.fEventTriggerOutFile = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( "-eventtriggerfrominfile", argv[i] ) )
	    {
	    if ( ddlID == ~0UL )
		{
		error = "DDL ID required for determination of event trigger data from data input file";
		errorArg = i;
		break;
		}
	    if ( inputFileUsedAsEventTriggerData )
		{
		error = "Most recent data input file already used for event trigger data for this event";
		errorArg = i;
		break;
		}
	    unsigned long blockCnt = curEvent.fBlocks.size();
	    if ( blockCnt<=0 && !curEvent.fHasNOPEBlocks )
		{
		error = "At least one data input file must be defined for event in order to be able to fill event trigger data from input file";
		errorArg = i;
		break;
		}
	    
	    if ( blockCnt>0 )
		{
		AliHLTEventTriggerData* etd = new AliHLTEventTriggerData;
		if ( curEvent.fBlocks[blockCnt-1].fBlock.fSize<TCommonDataHeader::GetCurrentHeaderSize() )
		    {
		    error = "Most recent data input file smaller than CDH.";
		    errorArg = i;
		    break;
		    }
		MLUCPlatformAttributes::FillBlockAttributes( etd->fAttributes );
		etd->fHLTStatus = 0;
		etd->fCommonHeaderWordCnt = gkAliHLTCommonHeaderCount;
		memcpy( etd->fCommonHeader, curEvent.fBlocks[blockCnt-1].fBlock.fPtr, TCommonDataHeader::GetCurrentHeaderSize() );
		AliHLTReadoutList readoutList( readoutListVersion, etd->fReadoutList.fList );
		etd->fReadoutList.fCount = readoutList.GetReadoutListDDLWordCount();
		readoutList.Reset();
		readoutList.SetDDL( ddlID );
		curEvent.fEventTriggerData.push_back( etd );
		inputFileUsedAsEventTriggerData = true;
		}
	    i += 1;
	    continue;
	    }
	if ( !strcmp( "-eventtriggerinfile", argv[i] ) )
	    {
	    if ( i+1>=argc )
		{
		error = "Missing event trigger input filename";
		errorArg = i;
		break;
		}
	    AliHLTEventTriggerData* etd = new AliHLTEventTriggerData;
	    inFH = open( argv[i+1], O_RDONLY );
	    if ( inFH == -1 )
		{
		error = "Unable to open event trigger input file for reading";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    off_t sz = lseek( inFH, 0, SEEK_END );
	    if ( (unsigned)sz<(unsigned)sizeof(*etd) )
		{
		error = "Input file is smaller than size of event trigger data.";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    lseek( inFH, 0, SEEK_SET );
	    unsigned long curSize = 0;
	    sz = sizeof(AliHLTEventTriggerData);
	    int ret;
	    do
		{
		ret = read( inFH, ((uint8*)etd)+curSize, sz-curSize );
		if ( ret >= 0 )
		    {
		    curSize += (unsigned long)ret;
		    if ( curSize >= (unsigned long)sz )
			{
			curEvent.fEventTriggerData.push_back( etd );
			break;
			}
		    }
		else
		    {
		    fprintf( stderr, "%s error reading data from event trigger input file %s after %lu bytes.\n", 
			     argv[0], argv[i+1], curSize );
		    return -1;
		    }
		}
	    while ( ret >0 );
	    close( inFH );
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-componentid" ) )
	    {
	    if ( argc <= i+1 )
		{
		error = "Missing component ID parameter";
		errorArg = i;
		break;
		}
	    componentID = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-componentlibrary" ) )
	    {
	    if ( argc <= i+1 )
		{
		error = "Missing component library path parameter";
		errorArg = i;
		break;
		}
	    aliRootInterface.SetLibrary( argv[i+1] );
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-componentargs" ) )
	    {
	    if ( argc <= i+1 )
		{
		error = "Missing component argument string parameter";
		errorArg = i;
		break;
		}
	    compOptions = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-componentinfo" ) )
	    {
	    if ( argc <= i+1 )
		{
		error = "Missing component information string parameter";
		errorArg = i;
		break;
		}
	    compInfo = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-addlibrary" ) )
	    {
	    if ( argc <= i+1 )
		{
		error = "Missing library name parameter.";
		errorArg = i;
		break;
		}
	    aliRootInterface.AddLibrary( argv[i+1] );
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-addlibrarybefore" ) )
	    {
	    if ( argc <= i+2 )
		{
		error = "Missing \"before\" library name parameter.";
		errorArg = i;
		break;
		}
	    if ( argc <= i+1 )
		{
		error = "Missing additional and \"before\" library name parameter.";
		errorArg = i;
		break;
		}
	    aliRootInterface.AddLibrary( argv[i+1], argv[i+2] );
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-datatype" ) )
	    {
	    if ( i+2 >= argc )
		{
		error = "Missing data type or origin specifier";
		errorArg = i;
		break;
		}
	    unsigned int j;
	    for ( j = 0; j < strlen(argv[i+1]) && j < 8; j++ )
		dataType.fID[j] = argv[i+1][j];
	    for ( ; j < 8; j++ )
		dataType.fID[j] = ' ';
	    for ( j = 0; j < strlen(argv[i+2]) && j < 4; j++ )
		dataType.fOrigin[j] = argv[i+2][j];
	    for ( ; j < 4; j++ )
		dataType.fOrigin[j] = ' ';
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-dataspec" ) )
	    {
	    if ( i+1 >= argc )
		{
		error = "Missing data specification specifier";
		errorArg = i;
		break;
		}
	    char* cpErr;
	    dataSpec = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		error = "Error converting specification specifier";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( i+1 >= argc )
		{
		error = "Missing verbosity specifier";
		errorArg = i;
		break;
		}
	    char* cpErr;
	    gVerbosity = (AliHLTComponentLogSeverity)strtoul( argv[i+1], &cpErr, 0 );
	    MLUCLog::gLogLevel = gVerbosity;
	    if ( *cpErr )
		{
		error = "Error converting verbosity specifier";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-eventiterations" ) )
	    {
	    if ( i+1 >= argc )
		{
		error = "Missing event iteration specifier";
		errorArg = i;
		break;
		}
	    char* cpErr;
	    eventIterations = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		error = "Error converting event iteration specifier";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-duplicatelastevent" ) )
	    {
	    if ( i+1 >= argc )
		{
		error = "Missing event duplicate count specifier";
		errorArg = i;
		break;
		}
	    char* cpErr;
	    eventDuplicates = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		error = "Error converting file duplicate count specifier";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    if ( events.size()>0 )
		{
		for ( unsigned long edc = 0; edc < eventDuplicates; edc++ )
		    {
		    EventData nextEvent;
		    std::vector<EventData>::iterator lastEvt = events.end();
		    lastEvt--;
		    BlockData newBlock;
		    for ( unsigned long edcb = 0; edcb < lastEvt->fBlocks.size(); edcb++ )
			{
			newBlock = lastEvt->fBlocks[edcb];
			newBlock.fEvtOffset = evtOffset;
			newBlock.fRealPtr = new uint8[ lastEvt->fBlocks[edcb].fBlock.fSize + newBlock.fEvtOffset ];
			if ( !newBlock.fRealPtr )
			    {
			    fprintf( stderr, "Out of memory trying to allocate memory for event duplicate of %lu bytes.\n",
				     lastEvt->fBlocks[edcb].fBlock.fSize + newBlock.fEvtOffset );
			    return -1;
			    }
			newBlock.fBlock.fPtr = newBlock.fRealPtr+newBlock.fEvtOffset;
			memcpy( newBlock.fBlock.fPtr, lastEvt->fBlocks[edcb].fBlock.fPtr, lastEvt->fBlocks[edcb].fBlock.fSize );
			nextEvent.fBlocks.push_back( newBlock );
			}
		    }
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-eventmemoffset" ) )
	    {
	    if ( i+1 >= argc )
		{
		error = "Missing event memory offset specifier";
		errorArg = i;
		break;
		}
	    char* cpErr;
	    evtOffset = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		error = "Error converting event memory offset specifier";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-iterations" ) )
	    {
	    if ( i+1 >= argc )
		{
		error = "Missing iteration specifier";
		errorArg = i;
		break;
		}
	    char* cpErr;
	    iterations = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		error = "Error converting iteration specifier";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-sorevent" ) )
	    {
	    curEvent.fEventType = gkAliEventTypeStartOfRun;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-eorevent" ) )
	    {
	    curEvent.fEventType = gkAliEventTypeEndOfRun;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-nextevent" ) )
	    {
	    if ( !curEvent.fHasNOPEBlocks || curEvent.fBlocks.size()>0 )
		events.push_back( curEvent );
#if 1
	    curEvent.Reset();
#else
	    curEvent.fBlocks.clear();
	    curEvent.fOutFiles.clear();
	    curEvent.fEventType = gkAliEventTypeData;
	    curEvent.fHasNOPEBlocks = false;
	    curEvent.fEventTriggerData = NULL;
#endif
    
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-ddlid" ) )
	    {
	    if ( i+1 >= argc )
		{
		error = "Missing DDL ID specifier";
		errorArg = i;
		break;
		}
	    char* cpErr;
	    ddlID = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		error = "Error converting DDL ID specifier";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    TDetectorID detID;
	    detID = AliHLTReadoutList::GetDetectorID( ddlID );
	    const char* shortName = AliHLTReadoutList::GetDetectorShortName( detID );
	    const char* rawDataType = "DDL_RAW";
	    unsigned int j;
	    for ( j = 0; j < strlen(rawDataType) && j < 8; j++ )
		dataType.fID[j] = rawDataType[j];
	    for ( ; j < 8; j++ )
		dataType.fID[j] = ' ';
	    for ( j = 0; j < strlen(shortName) && j < 4; j++ )
		dataType.fOrigin[j] = shortName[j];
	    for ( ; j < 4; j++ )
		dataType.fOrigin[j] = ' ';
	    if ( detID==kTPC )
		{
		dataSpec = 0;
		unsigned slice, patch;
		slice = AliHLTReadoutList::GetTPCSlice( ddlID );
		patch = AliHLTReadoutList::GetTPCPatch( ddlID );
		dataSpec |= (slice&0xFF)<<24;
		dataSpec |= (slice&0xFF)<<16;
		dataSpec |= (patch&0xFF)<<8;
		dataSpec |= (patch&0xFF);
		}
	    else
		{
		dataSpec = 1 << AliHLTReadoutList::GetDetectorDDLNr( ddlID );
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-commandfile" ) )
	    {
	    if ( i+1 >= argc )
		{
		error = "Missing file name specifier";
		errorArg = i;
		break;
		}
	    std::ifstream infile( argv[i+1] );
	    if ( !infile.good() )
		{
		error = "Cannot open file";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    unsigned long lineNr=0;
	    while ( infile.good() && !error )
		{
		++lineNr;
		MLUCString line;
		char tmpBuf[1024];
		do
		    {
		    memset( tmpBuf, 0, 1024 );
		    infile.getline( tmpBuf, 1024 );
		    line += tmpBuf;
		    }
		while ( infile.gcount()==1024 && tmpBuf[1022]!='\n' && infile.good() );
		if ( !infile.good() && !infile.eof() )
		    break;
		vector<MLUCString> args;
		if ( MLUCCmdlineParser::ParseCmd( line.c_str(), args ) && args.size()>0 )
		    {
		    if ( args[0]=="infile" )
			{
			if ( 1>=args.size() )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'infile')";
			    errorStringText += ": ";
			    errorStringText += "Missing input filename";
			    error = errorStringText.c_str();
			    break;
			    }
			bool isNopeEvent = false;
			int nopeRet = TNOPEEvent::IsNopeEvent( args[1].c_str(), isNopeEvent );
			if ( nopeRet )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'infile')";
			    errorStringText += ": ";
			    errorStringText += "Cannot determine NOPE event status for input file";
			    error = errorStringText.c_str();
			    errorArg = i;
			    errorParam = i+1;
			    break;
			    }
			std::cout << args[1].c_str() << " is " << ( isNopeEvent ? "" : "not" ) << " a NOPE event" << std::endl;
			if ( !isNopeEvent )
			    {
			    inFH = open( args[1].c_str(), O_RDONLY );
			    if ( inFH == -1 )
				{
				char tmpText[512];
				errorStringText = "Error parsing command input file '";
				errorStringText += argv[i+1];
				errorStringText += "' line ";
				sprintf( tmpText, "%lu", lineNr );
				errorStringText += tmpText;
				errorStringText += " (command 'infile')";
				errorStringText += ": ";
				errorStringText += "Unable to open input file for reading";
				error = errorStringText.c_str();
				errorArg = i;
				errorParam = i+1;
				break;
				}
			    BlockData newBlock;
			    newBlock.fBlock.fStructSize = sizeof(AliHLTComponentBlockData);
			    memcpy( &(newBlock.fBlock.fDataType), &dataType, sizeof(dataType) );
			    newBlock.fBlock.fSpecification = dataSpec;
			    newBlock.fBlock.fShmKey.fStructSize = sizeof(AliHLTComponentShmData);
			    newBlock.fBlock.fShmKey.fShmType = gkAliHLTComponentInvalidShmType;
			    newBlock.fBlock.fShmKey.fShmID = gkAliHLTComponentInvalidShmID;
			    newBlock.fEvtOffset = evtOffset;
			    off_t sz = lseek( inFH, 0, SEEK_END );
			    lseek( inFH, 0, SEEK_SET );
			    newBlock.fRealPtr = new uint8[ sz+newBlock.fEvtOffset ];
			    if ( !newBlock.fRealPtr )
				{
				fprintf( stderr, "Out of memory trying to allocate memory for input file '%s' of %lu bytes.\n",
					 args[1].c_str(), sz+newBlock.fEvtOffset );
				return -1;
				}
			    newBlock.fBlock.fPtr = newBlock.fRealPtr+newBlock.fEvtOffset;
			    newBlock.fBlock.fOffset = 0;
			    unsigned long curSize = 0;
			    int ret;
			    do
				{
				ret = read( inFH, ((uint8*)newBlock.fBlock.fPtr)+curSize, sz-curSize );
				if ( ret >= 0 )
				    {
				    curSize += (unsigned long)ret;
				    if ( curSize >= (unsigned long)sz )
					{
					newBlock.fBlock.fSize = sz;
					curEvent.fBlocks.push_back( newBlock );
					break;
					}
				    }
				else
				    {
				    fprintf( stderr, "%s error reading data from input file %s after %lu bytes.\n", 
					     argv[0], args[1].c_str(), curSize );
				    return -1;
				    }
				}
			    while ( ret >0 );
			    close( inFH );
			    inputFileUsedAsEventTriggerData = false;
			    }
			else
			    curEvent.fHasNOPEBlocks = true;
			}
		    else if ( args[0]=="sorevent" )
			{
			curEvent.fEventType = gkAliEventTypeStartOfRun;
			}
		    else if ( args[0]=="eorevent"  )
			{
			curEvent.fEventType = gkAliEventTypeEndOfRun;
			}
		    else if ( args[0]=="nextevent" )
			{
			if ( !curEvent.fHasNOPEBlocks || curEvent.fBlocks.size()>0 )
			    events.push_back( curEvent );
#if 1
			curEvent.Reset();
#else
			curEvent.fBlocks.clear();
			curEvent.fOutFiles.clear();
			curEvent.fEventType = gkAliEventTypeData;
			curEvent.fHasNOPEBlocks = false;
			curEvent.fEventTriggerData = NULL;
#endif
			}
		    else if ( args[0]=="datatype" )
			{
			if ( 2 >= args.size() )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'datatype')";
			    errorStringText += ": ";
			    errorStringText += "Missing data type or origin specifier";
			    error = errorStringText.c_str();
			    errorArg = i;
			    break;
			    }
			unsigned int j;
			for ( j = 0; j < strlen(args[1].c_str()) && j < 8; j++ )
			    dataType.fID[j] = args[1][j];
			for ( ; j < 8; j++ )
			    dataType.fID[j] = ' ';
			for ( j = 0; j < strlen(args[2].c_str()) && j < 4; j++ )
			    dataType.fOrigin[j] = args[2][j];
			for ( ; j < 4; j++ )
			    dataType.fOrigin[j] = ' ';
			}
		    else if ( args[0]=="dataspec" )
			{
			if ( 1 >= args.size() )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'dataspec')";
			    errorStringText += ": ";
			    errorStringText += "Missing data specification specifier";
			    error = errorStringText.c_str();
			    errorArg = i;
			    break;
			    }
			char* cpErr;
			dataSpec = strtoul( args[1].c_str(), &cpErr, 0 );
			if ( *cpErr )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'dataspec')";
			    errorStringText += ": ";
			    errorStringText += "Error converting specification specifier";
			    error = errorStringText.c_str();
			    errorArg = i;
			    errorParam = i+1;
			    break;
			    }
			}
		    else if ( args[0]=="ddlid" )
			{
			if ( 1 >= args.size() )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'ddlid')";
			    errorStringText += ": ";
			    errorStringText += "Missing DDL ID specifier";
			    error = errorStringText.c_str();
			    errorArg = i;
			    break;
			    }
			char* cpErr;
			ddlID = strtoul( args[1].c_str(), &cpErr, 0 );
			if ( *cpErr!='\0' && *cpErr!='\n' )
			    {
			    printf( "%s - %c - %d\n", cpErr, *cpErr, (int)*cpErr );
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'ddlid')";
			    errorStringText += ": ";
			    errorStringText += "Error converting DDL ID specifier";
			    error = errorStringText.c_str();
			    errorArg = i;
			    errorParam = i+1;
			    break;
			    }
			TDetectorID detID;
			detID = AliHLTReadoutList::GetDetectorID( ddlID );
			const char* shortName = AliHLTReadoutList::GetDetectorShortName( detID );
			const char* rawDataType = "DDL_RWPK";
			unsigned int j;
			for ( j = 0; j < strlen(rawDataType) && j < 8; j++ )
			    dataType.fID[j] = rawDataType[j];
			for ( ; j < 8; j++ )
			    dataType.fID[j] = ' ';
			for ( j = 0; j < strlen(shortName) && j < 4; j++ )
			    dataType.fOrigin[j] = shortName[j];
			for ( ; j < 4; j++ )
			    dataType.fOrigin[j] = ' ';
			if ( detID==kTPC )
			    {
			    dataSpec = 0;
			    unsigned slice, patch;
			    slice = AliHLTReadoutList::GetTPCSlice( ddlID );
			    patch = AliHLTReadoutList::GetTPCPatch( ddlID );
			    dataSpec |= (slice&0xFF)<<24;
			    dataSpec |= (slice&0xFF)<<16;
			    dataSpec |= (patch&0xFF)<<8;
			    dataSpec |= (patch&0xFF);
			    }
			else
			    {
			    dataSpec = 1 << AliHLTReadoutList::GetDetectorDDLNr( ddlID );
			    }
			}
		    else if ( args[0]=="outfile" )
			{
			if ( 1 >= args.size() )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'outfile')";
			    errorStringText += ": ";
			    errorStringText += "Missing output filename specifier";
			    error = errorStringText.c_str();
			    errorArg = i;
			    break;
			    }
			curEvent.fOutFiles.push_back( std::string(args[1].c_str()) );
			}
		    else if ( args[0]=="eventdoneoutfile" )
			{
			if ( 1 >= args.size() )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'outfile')";
			    errorStringText += ": ";
			    errorStringText += "Missing event done data output filename specifier";
			    error = errorStringText.c_str();
			    errorArg = i;
			    break;
			    }
			curEvent.fEDDOutFile = args[1].c_str();
			}
		    else if ( args[0]=="eventtriggeroutfile" )
			{
			if ( 1 >= args.size() )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'outfile')";
			    errorStringText += ": ";
			    errorStringText += "Missing event trigger data output filename specifier";
			    error = errorStringText.c_str();
			    errorArg = i;
			    break;
			    }
			curEvent.fEventTriggerOutFile = args[1].c_str();
			}
		    else if ( args[0]=="eventtriggerfrominfile" )
			{
			if ( ddlID == ~0UL )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'outfile')";
			    errorStringText += ": ";
			    errorStringText += "DDL ID required for determination of event trigger data from data input file";
			    error = errorStringText.c_str();
			    break;
			    }
			if ( inputFileUsedAsEventTriggerData )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'outfile')";
			    errorStringText += ": ";
			    errorStringText += "Most recent data input file already used for event trigger data for this event";
			    error = errorStringText.c_str();
			    break;
			    }
			unsigned long blockCnt = curEvent.fBlocks.size();
			std::cout << "blockCnt: " << blockCnt << "curEvent.fHasNOPEBlocks: " << ( curEvent.fHasNOPEBlocks ? "Yes" : "No" ) << std::endl;
			if ( blockCnt<=0 && !curEvent.fHasNOPEBlocks )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'outfile')";
			    errorStringText += ": ";
			    errorStringText += "At least one data input file must be defined for event in order to be able to fill event trigger data from input file";
			    error = errorStringText.c_str();
			    break;
			    }
			if ( blockCnt>0 )
			    {
			    AliHLTEventTriggerData* etd = new AliHLTEventTriggerData;
			    if ( curEvent.fBlocks[blockCnt-1].fBlock.fSize<TCommonDataHeader::GetCurrentHeaderSize() )
				{
				char tmpText[512];
				errorStringText = "Error parsing command input file '";
				errorStringText += argv[i+1];
				errorStringText += "' line ";
				sprintf( tmpText, "%lu", lineNr );
				errorStringText += tmpText;
				errorStringText += " (command 'outfile')";
				errorStringText += ": ";
				errorStringText += "Most recent data input file smaller than CDH";
				error = errorStringText.c_str();
				break;
				}
			    
			    MLUCPlatformAttributes::FillBlockAttributes( etd->fAttributes );
			    etd->fHLTStatus = 0;
			    etd->fCommonHeaderWordCnt = gkAliHLTCommonHeaderCount;
			    memcpy( etd->fCommonHeader, curEvent.fBlocks[blockCnt-1].fBlock.fPtr, TCommonDataHeader::GetCurrentHeaderSize() );
			    AliHLTReadoutList readoutList( readoutListVersion, etd->fReadoutList.fList );
			    etd->fReadoutList.fCount = readoutList.GetReadoutListDDLWordCount();
			    readoutList.Reset();
			    readoutList.SetDDL( ddlID );
			    curEvent.fEventTriggerData.push_back( etd );
			    inputFileUsedAsEventTriggerData = true;
			    }
			i += 1;
			continue;
			}
		    else if ( args[0]=="eventtriggerinfile" )
			{
			if ( 1 >= args.size() )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'outfile')";
			    errorStringText += ": ";
			    errorStringText += "Missing event trigger input filename";
			    error = errorStringText.c_str();
			    errorArg = i;
			    break;
			    }
			AliHLTEventTriggerData* etd = new AliHLTEventTriggerData;
			inFH = open( args[1].c_str(), O_RDONLY );
			if ( inFH == -1 )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'outfile')";
			    errorStringText += ": ";
			    errorStringText += "Unable to open event trigger input file for reading";
			    error = errorStringText.c_str();
			    errorArg = i;
			    errorParam = i+1;
			    break;
			    }
			off_t sz = lseek( inFH, 0, SEEK_END );
			if ( (unsigned)sz<(unsigned)sizeof(*etd) )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'outfile')";
			    errorStringText += ": ";
			    errorStringText += "Input file is smaller than size of event trigger data";
			    error = errorStringText.c_str();
			    errorArg = i;
			    errorParam = i+1;
			    break;
			    }
			lseek( inFH, 0, SEEK_SET );
			unsigned long curSize = 0;
			sz = sizeof(AliHLTEventTriggerData);
			int ret;
			do
			    {
			    ret = read( inFH, ((uint8*)etd)+curSize, sz-curSize );
			    if ( ret >= 0 )
				{
				curSize += (unsigned long)ret;
				if ( curSize >= (unsigned long)sz )
				    {
				    curEvent.fEventTriggerData.push_back( etd );
				    break;
				    }
				}
			    else
				{
				fprintf( stderr, "%s error reading data from event trigger input file %s after %lu bytes.\n", 
					 argv[0], argv[i+1], curSize );
				return -1;
				}
			    }
			while ( ret >0 );
			close( inFH );
			i += 2;
			continue;
			}
		    }
		}
	    if ( !infile.good() && !infile.eof() )
		{
		error = "Error reading from file";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	error = "Unknown argument";
	errorArg = i;
	errorParam = -1;
	break;
	}
    if ( !error )
	{
	if ( !componentID )
	    {
	    error = "No component ID specified (e.g. via -componentid command line argument)";
	    }
	}
    if ( error )
	{
	fprintf( stderr, "Usage: %s (-dummymode) (-showinputblocks) -componentid <component-ID> (-infile <input-file-name>) (-eventtriggerfrominfile) (-eventtriggerinfile <event-trigger-data-input-file-name>) (-outfile <output-file-name>) (-eventdoneoutfile <event-done-data-output-file-name>) (-eventtriggeroutfile <event-trigger-data-output-file-name>) (-componentlibrary <component-lirbary>) (-componentargs <component-arguments>) (-addlibrary <additional-shared-library>) (-datatype <data-type> <data-origin>) (-dataspec <data-specification>) (-V <verbostity>) (-eventiterations <iterations-per-event>) (-iterations <iterations>) (-nextevent) (-eventfileduplicates <duplicate-count>) (-eventmemoffset <event-offset-in-memory>) (-alicehlt) (-ddlid <DDL identifier>) (-commandfile <command input file>) (-readoutlistversion <readout-list-version number (1-%u, current default %u)>) (-runnr <run-number>) (-runnumber <run-number>) (-hltmode <HLT Mode (A|B|C|D|E)>) (-beamtype <beam type (pp|AA)>) (-runtype <run type specification string>) (-ecsparameters <ECS parameter string>)\n", argv[0], gkReadoutListVersions, readoutListVersion );
 	fprintf( stderr, "Error: %s\n", error );
	if ( errorArg>=0 )
	    fprintf( stderr, "Offending argument: %s\n", argv[errorArg] );
	if ( errorParam>=0 )
	    fprintf( stderr, "Offending parameter: %s\n", argv[errorParam] );
	return -1;
	}
    if ( curEvent.fBlocks.size()>0 )
	{
	events.push_back( curEvent );
	}

    uint8* XORData = NULL;
    uint32* XORStructData = NULL;
    BlockData SORDataBlock;
    BlockData EORDataBlock;
    BlockData xORDataBlocks[2];
    unsigned xORDataBlockCnt=0;
    unsigned XORDataSize = 0;

    const unsigned XORWordCount = 3;
    const unsigned XORStructSize = XORWordCount*sizeof(uint32);
    const unsigned long runTypeLen = strlen(runType)+1;
    unsigned long ecsParamLen = strlen(ecsParameters)+1;
    XORDataSize = XORStructSize+runTypeLen+ecsParamLen;
    XORData = new uint8[ XORDataSize ];
    if ( !XORData )
	    {
	    fprintf( stderr, "Out of memory trying to allocate memory for (Start/End)-Of-Run data of %lu bytes.\n",
		     (unsigned long)XORDataSize );
	    return -1;
	    }
    XORStructData = (uint32*)XORData;
    XORStructData[0] = XORStructSize;
    XORStructData[1] = runNumber;
    XORStructData[2] = beamType | (hltMode << 3);
    
    if ( 1 )
	{
	SORDataBlock.fBlock.fStructSize = sizeof(AliHLTComponentBlockData);
	uint64 dataType = (((uint64)'STAR')<<32 | 'TOFR');
	//memcpy( SORDataBlock.fBlock.fDataType.fID, &dataType, 8 );
	for ( unsigned b = 0; b < 8; b++ )
	    SORDataBlock.fBlock.fDataType.fID[7-b] = ((char*)(&dataType))[b];
	uint32 dataOrigin = ((uint32)'PRIV');
	//memcpy( SORDataBlock.fBlock.fDataType.fOrigin, &dataOrigin, 4 );
	for ( unsigned b = 0; b < 4; b++ )
	    SORDataBlock.fBlock.fDataType.fOrigin[3-b] = ((char*)(&dataOrigin))[b];
	SORDataBlock.fBlock.fSpecification = 0;
	SORDataBlock.fBlock.fShmKey.fStructSize = sizeof(AliHLTComponentShmData);
	SORDataBlock.fBlock.fShmKey.fShmType = gkAliHLTComponentInvalidShmType;
	SORDataBlock.fBlock.fShmKey.fShmID = gkAliHLTComponentInvalidShmID;
	SORDataBlock.fEvtOffset = 0;
	SORDataBlock.fRealPtr = (uint8*)XORStructData;
	SORDataBlock.fBlock.fPtr = SORDataBlock.fRealPtr;
	SORDataBlock.fBlock.fSize = XORStructSize;
	SORDataBlock.fBlock.fOffset = 0;
	
	EORDataBlock = SORDataBlock;
	
	dataType = (((uint64)'ENDO')<<32 | 'FRUN');
	//memcpy( EORDataBlock.fBlock.fDataType.fID, &dataType, 8 );
	for ( unsigned b = 0; b < 8; b++ )
	    EORDataBlock.fBlock.fDataType.fID[7-b] = ((char*)(&dataType))[b];
	}
    
    if ( runTypeLen>1 )
	{
	char* runTypePtr = ((char*)XORData)+XORStructSize;
	strcpy( runTypePtr, runType );
	
	xORDataBlocks[xORDataBlockCnt].fBlock.fStructSize = sizeof(AliHLTComponentBlockData);
	uint64 dataType = (((uint64)'RUNT')<<32 | 'YPE ');
	//memcpy( xORDataBlocks[xORDataBlockCnt].fBlock.fDataType.fID, &dataType, 8 );
	for ( unsigned b = 0; b < 8; b++ )
	    xORDataBlocks[xORDataBlockCnt].fBlock.fDataType.fID[7-b] = ((char*)(&dataType))[b];
	uint32 dataOrigin = ((uint32)'PRIV');
	//memcpy( xORDataBlocks[xORDataBlockCnt].fBlock.fDataType.fOrigin, &dataOrigin, 4 );
	for ( unsigned b = 0; b < 4; b++ )
	    xORDataBlocks[xORDataBlockCnt].fBlock.fDataType.fOrigin[3-b] = ((char*)(&dataOrigin))[b];
	xORDataBlocks[xORDataBlockCnt].fBlock.fSpecification = 0;
	xORDataBlocks[xORDataBlockCnt].fBlock.fShmKey.fStructSize = sizeof(AliHLTComponentShmData);
	xORDataBlocks[xORDataBlockCnt].fBlock.fShmKey.fShmType = gkAliHLTComponentInvalidShmType;
	xORDataBlocks[xORDataBlockCnt].fBlock.fShmKey.fShmID = gkAliHLTComponentInvalidShmID;
	xORDataBlocks[xORDataBlockCnt].fEvtOffset = 0;
	xORDataBlocks[xORDataBlockCnt].fRealPtr = (uint8*)runTypePtr;
	xORDataBlocks[xORDataBlockCnt].fBlock.fPtr = xORDataBlocks[xORDataBlockCnt].fRealPtr;
	xORDataBlocks[xORDataBlockCnt].fBlock.fSize = runTypeLen;
	xORDataBlocks[xORDataBlockCnt].fBlock.fOffset = 0;
	
	++xORDataBlockCnt;
	}
    if ( ecsParamLen>1 )
	{
	char* ecsParamPtr = ((char*)XORData)+XORStructSize+runTypeLen;
	strcpy( ecsParamPtr, ecsParameters );
	
	xORDataBlocks[xORDataBlockCnt].fBlock.fStructSize = sizeof(AliHLTComponentBlockData);
	uint64 dataType = (((uint64)'ECSP')<<32 | 'ARAM');
	//memcpy( xORDataBlocks[xORDataBlockCnt].fBlock.fDataType.fID, &dataType, 8 );
	for ( unsigned b = 0; b < 8; b++ )
	    xORDataBlocks[xORDataBlockCnt].fBlock.fDataType.fID[7-b] = ((char*)(&dataType))[b];
	uint32 dataOrigin = ((uint32)'PRIV');
	//memcpy( xORDataBlocks[xORDataBlockCnt].fBlock.fDataType.fOrigin, &dataOrigin, 4 );
	for ( unsigned b = 0; b < 4; b++ )
	    xORDataBlocks[xORDataBlockCnt].fBlock.fDataType.fOrigin[3-b] = ((char*)(&dataOrigin))[b];
	xORDataBlocks[xORDataBlockCnt].fBlock.fSpecification = 0;
	xORDataBlocks[xORDataBlockCnt].fBlock.fShmKey.fStructSize = sizeof(AliHLTComponentShmData);
	xORDataBlocks[xORDataBlockCnt].fBlock.fShmKey.fShmType = gkAliHLTComponentInvalidShmType;
	xORDataBlocks[xORDataBlockCnt].fBlock.fShmKey.fShmID = gkAliHLTComponentInvalidShmID;
	xORDataBlocks[xORDataBlockCnt].fEvtOffset = 0;
	xORDataBlocks[xORDataBlockCnt].fRealPtr = (uint8*)ecsParamPtr;
	xORDataBlocks[xORDataBlockCnt].fBlock.fPtr = xORDataBlocks[xORDataBlockCnt].fRealPtr;
	xORDataBlocks[xORDataBlockCnt].fBlock.fSize = ecsParamLen;
	xORDataBlocks[xORDataBlockCnt].fBlock.fOffset = 0;

	++xORDataBlockCnt;

	}
	    

    curEvent.fOutFiles.clear();

    if ( !events.empty() && events.begin()->fEventType!=gkAliEventTypeStartOfRun )
	{
	curEvent.fBlocks.clear();
	curEvent.fEventType = gkAliEventTypeStartOfRun;
	events.insert( events.begin(), curEvent );
	}
    if ( !events.empty() )
	{
	events.begin()->fBlocks.push_back( SORDataBlock );
	for ( unsigned i=0; i<xORDataBlockCnt; ++i )
	    events.begin()->fBlocks.push_back( xORDataBlocks[i] );
	}

    if ( !events.empty() && events.rbegin()->fEventType!=gkAliEventTypeEndOfRun )
	{
	curEvent.fBlocks.clear();
	curEvent.fEventType = gkAliEventTypeEndOfRun;
	events.insert( events.end(), curEvent );
	}
    if ( !events.empty() )
	{
	events.rbegin()->fBlocks.push_back( EORDataBlock );
	for ( unsigned i=0; i<xORDataBlockCnt; ++i )
	    events.rbegin()->fBlocks.push_back( xORDataBlocks[i] );
	}

    BlockData eventTypeBlock;
    eventTypeBlock.fBlock.fStructSize = sizeof(AliHLTComponentBlockData);
    uint64 blockDataType = (((uint64)'EVEN')<<32 | 'TTYP');
    //memcpy( eventTypeBlock.fBlock.fDataType.fID, &dataType, 8 );
    for ( unsigned b = 0; b < 8; b++ )
	eventTypeBlock.fBlock.fDataType.fID[7-b] = ((char*)(&blockDataType))[b];
    uint32 blockDataOrigin = ((uint32)'PRIV');
    //memcpy( eventTypeBlock.fBlock.fDataType.fOrigin, &dataOrigin, 4 );
    for ( unsigned b = 0; b < 4; b++ )
	eventTypeBlock.fBlock.fDataType.fOrigin[3-b] = ((char*)(&blockDataOrigin))[b];
    eventTypeBlock.fBlock.fSpecification = 0;
    eventTypeBlock.fBlock.fShmKey.fStructSize = sizeof(AliHLTComponentShmData);
    eventTypeBlock.fBlock.fShmKey.fShmType = gkAliHLTComponentInvalidShmType;
    eventTypeBlock.fBlock.fShmKey.fShmID = gkAliHLTComponentInvalidShmID;
    eventTypeBlock.fEvtOffset = 0;
    eventTypeBlock.fRealPtr = NULL;
    eventTypeBlock.fBlock.fPtr = NULL;
    eventTypeBlock.fBlock.fSize = 0;

    std::vector<EventData>::iterator evtIter, evtEnd;
    evtIter = events.begin();
    evtEnd = events.end();
    while ( evtIter != evtEnd )
	{
	eventTypeBlock.fBlock.fSpecification = evtIter->fEventType;
	evtIter->fBlocks.push_back( eventTypeBlock );
	evtIter++;
	}
    
    struct timeval sInitT, eInitT;
    gettimeofday( &sInitT, NULL );
    
    int ret;
    ret = aliRootInterface.SetEnvironment( NULL, &(AllocMemory), &(GetEventDoneData),&(Logging) );
    if ( ret )
	{
	fprintf( stderr, "Error setting up AliRoot interface environment: %s (%d).", strerror(ret), ret );
	return -1;
	}

    ret = aliRootInterface.SetComponent( componentID, compOptions, compInfo );
    if ( ret )
	{
	fprintf( stderr, "Error setting up AliRoot component: %s (%d).", strerror(ret), ret );
	return -1;
	}

    ret = aliRootInterface.SetRunInformation( beamType, hltMode, runNumber, runType );
    if ( ret )
	{
	fprintf( stderr, "Error setting up AliRoot run information: %s (%d).", strerror(ret), ret );
	return -1;
	}
    
    ret = aliRootInterface.Load();
    if ( ret )
	{
	fprintf( stderr, "Error loading AliRoot interface: %s (%d).", strerror(ret), ret );
	return -1;
	}

    AliHLTComponentDataType outputType;
    outputType.fStructSize = sizeof(outputType);
    ret = aliRootInterface.GetOutputDataType( outputType );
    if ( ret )
	{
	fprintf( stderr, "Error getting output data type from AliRoot interface: %s (%d).", strerror(ret), ret );
	return -1;
	}

    gettimeofday( &eInitT, NULL );
    unsigned long long dInit;
    dInit = eInitT.tv_sec-sInitT.tv_sec;
    dInit *= 1000000ULL;
    dInit += eInitT.tv_usec-sInitT.tv_usec;
    double dInitd = ((double)dInit);
    printf( "Init: Time needed for initialization: %f microsec. / %f millisec. / %f s\n", 
	    dInitd, dInitd/1000.0, dInitd/1000000.0 );
	    


    evtIter = events.begin();
    evtEnd = events.end();
    while ( evtIter != evtEnd )
	{
	unsigned long inputBlockCnt = evtIter->fBlocks.size();
	evtIter->fInputBlocks = new AliHLTComponentBlockData[ inputBlockCnt ];
	if ( !evtIter->fInputBlocks )
	    {
	    fprintf( stderr, "Out of memory allocating %lu block data structures.\n", inputBlockCnt );
	    return -1;
	    }
	totalRead = 0;
	for ( unsigned long bn = 0; bn < inputBlockCnt; bn++ )
	    {
	    evtIter->fInputBlocks[bn] = evtIter->fBlocks[bn].fBlock;
	    totalRead += evtIter->fBlocks[bn].fBlock.fSize;
	    }
	
	unsigned long constEventBase=0;
	unsigned long constBlockBase=0;
	double inputMultiplier=0.0;
	ret = aliRootInterface.GetOutputDataSize( constEventBase, constBlockBase, inputMultiplier );
	if ( ret )
	    {
	    fprintf( stderr, "Unable to determine output data size parameters from AliRoot interface: %s (%d).\n", strerror(ret), ret );
	    return -1;
	    }
	
	AliHLTUInt32_t outputSize = (AliHLTUInt32_t)( 256 + constEventBase + (128*constBlockBase)*inputBlockCnt + totalRead * inputMultiplier ); // Various safety measures
	
	evtIter->fOutputPtr = new uint8[ outputSize ];
	if ( !evtIter->fOutputPtr )
	    {
	    fprintf( stderr, "Out of memory allocating %lu bytes of output memory.\n", (unsigned long)outputSize );
	    return -1;
	    }
	evtIter->fOutputSize = outputSize;
	    
	evtIter++;
	}
    double totalT = 0;

    if ( showInputBlocks )
	{
	evtIter = events.begin();
	evtEnd = events.end();
	AliHLTEventID_t evtID = 0;
	
	while ( evtIter != evtEnd )
	    {
	    printf( "Event %Lu", (unsigned long long)evtID );
	    if ( evtIter->fEventType==gkAliEventTypeStartOfRun )
		{
		printf( "(Start-Of-Run/Data)" );
		}
	    else if ( evtIter->fEventType==gkAliEventTypeData )
		{
		printf( "(Data/Physics Event)" );
		}
	    else if ( evtIter->fEventType==gkAliEventTypeEndOfRun )
		{
		printf( "(End-Of-Run/Data)" );
		}
	    else
		{
		printf( "(Unknown Event Type %lu)", (unsigned long)evtIter->fEventType );
		}
	    printf( "\n" );
	    unsigned long inputBlockCnt = evtIter->fBlocks.size();
	    if ( evtIter->fEventTriggerData.size()>0 )
		{
		}
	    printf( "  %lu input data blocks\n", (unsigned long)inputBlockCnt );
	    for ( unsigned long long nn=0; nn<inputBlockCnt; ++nn )
		{
		printf( "    Block %lu - size: %lu - type: '%c%c%c%c%c%c%c%c' - origin: '%c%c%c%c'- spec: 0x%08lX\n", 
			(unsigned long)nn, (unsigned long)evtIter->fInputBlocks[nn].fSize,
			evtIter->fInputBlocks[nn].fDataType.fID[0], evtIter->fInputBlocks[nn].fDataType.fID[1], 
			evtIter->fInputBlocks[nn].fDataType.fID[2], evtIter->fInputBlocks[nn].fDataType.fID[3], 
			evtIter->fInputBlocks[nn].fDataType.fID[4], evtIter->fInputBlocks[nn].fDataType.fID[5], 
			evtIter->fInputBlocks[nn].fDataType.fID[6], evtIter->fInputBlocks[nn].fDataType.fID[7], 
			evtIter->fInputBlocks[nn].fDataType.fOrigin[0], evtIter->fInputBlocks[nn].fDataType.fOrigin[1], 
			evtIter->fInputBlocks[nn].fDataType.fOrigin[2], evtIter->fInputBlocks[nn].fDataType.fOrigin[3],
			(unsigned long)evtIter->fInputBlocks[nn].fSpecification );
		}
	    ++evtID;
	    ++evtIter;
	    }
	}
    for ( unsigned long ii=0; ii<iterations; ii++ )
	{
	evtIter = events.begin();
	evtEnd = events.end();
	AliHLTEventID_t evtID = 0;
	
	while ( evtIter != evtEnd )
	    {
	    unsigned long inputBlockCnt = evtIter->fBlocks.size();
	    struct timeval now;
	    gettimeofday( &now, NULL );
	    AliHLTComponentEventData compEventData;
	    compEventData.fStructSize = sizeof(AliHLTComponentEventData);
	    compEventData.fEventID = evtID;
	    compEventData.fEventCreation_s = now.tv_sec;
	    compEventData.fEventCreation_us = now.tv_usec;
	    compEventData.fBlockCnt = inputBlockCnt;
	    
	    AliHLTComponentTriggerData trigData;
	    trigData.fStructSize = sizeof(AliHLTComponentTriggerData);
	    trigData.fDataSize = 0;
	    trigData.fData = NULL;
	    if ( evtIter->fEventTriggerData.size()>0 )
		{
		AliHLTEventTriggerData *tmpTrigData = new AliHLTEventTriggerData;
		trigData.fData = tmpTrigData;
		trigData.fDataSize = sizeof(*tmpTrigData);
		memset( tmpTrigData, 0, sizeof(AliHLTEventTriggerData) );
		MLUCPlatformAttributes::FillBlockAttributes( tmpTrigData->fAttributes );
		tmpTrigData->fHLTStatus = 0;
		tmpTrigData->fCommonHeaderWordCnt = gkAliHLTCommonHeaderCount;
		AliHLTReadoutList readoutList( readoutListVersion, tmpTrigData->fReadoutList.fList );
		tmpTrigData->fReadoutList.fCount = readoutList.GetReadoutListDDLWordCount();
		readoutList.Reset();
		TCommonDataHeader cdh( tmpTrigData->fCommonHeader );
		cdh.Reset();
		cdh.SetEventID( evtID, true );
		for ( unsigned ietd=0; ietd<evtIter->fEventTriggerData.size(); ietd++ )
		    {
		    TCommonDataHeader thisCDH( evtIter->fEventTriggerData[ietd]->fCommonHeader );
		    cdh |= thisCDH;
		    AliHLTReadoutList thisReadoutList( readoutListVersion, evtIter->fEventTriggerData[ietd]->fReadoutList.fList );
		    readoutList |= thisReadoutList;
		    tmpTrigData->fHLTStatus |= evtIter->fEventTriggerData[ietd]->fHLTStatus;
		    }
		}
	    
	    AliHLTComponentEventDoneData* edd = NULL;
	    AliHLTComponentBlockData* outputBlocks = NULL;
	    AliHLTUInt32_t outputBlockCnt = 0;
	    
	    // run first time without time measurement
	    if ( eventIterations>1 ) {
	      if ( edd ) {
		FreeMemory( (void*)NULL, edd );
		edd = NULL;
	      }
	      if ( outputBlocks ) {
		FreeMemory( (void*)NULL, outputBlocks );
		outputBlocks = NULL;
	      }
	      AliHLTUInt32_t outputSize = evtIter->fOutputSize;
	      ret = aliRootInterface.ProcessEvent( &compEventData, evtIter->fInputBlocks, 
						   &trigData, evtIter->fOutputPtr,
						   outputSize, outputBlockCnt, 
						   outputBlocks,
						   edd );	      
	    }

	    struct timeval startT, endT;
	    gettimeofday( &startT, NULL );
	    for ( unsigned long eeii=0; eeii<eventIterations; eeii++ ) {
	      if ( edd ) {
		FreeMemory( (void*)NULL, edd );
		edd = NULL;
	      }
	      if ( outputBlocks ) {
		FreeMemory( (void*)NULL, outputBlocks );
		outputBlocks = NULL;
	      }
	      AliHLTUInt32_t outputSize = evtIter->fOutputSize;
	      ret = aliRootInterface.ProcessEvent( &compEventData, evtIter->fInputBlocks, 
						   &trigData, evtIter->fOutputPtr,
						   outputSize, outputBlockCnt, 
						   outputBlocks,
						   edd );
	    }
	    gettimeofday( &endT, NULL );
	    unsigned long long dt;
	    dt = endT.tv_sec-startT.tv_sec;
	    dt *= 1000000ULL;
	    dt += endT.tv_usec-startT.tv_usec;
	    double dtd = ((double)dt);
	    totalT += dtd;
	    dtd = dtd / (double)eventIterations;
	    if ( iterations<=1 )
		printf( "Event %Lu: Time needed per event: %f microsec. / %f millisec. / %f s\n", 
			evtID, dtd, dtd/1000.0, dtd/1000000.0 );
	    
	    
	    
	    if ( ret )
		{
		fprintf( stderr, "Error processing event: %s (%d).\n", strerror(ret), ret );
		return -1;
		}
	    
	    if ( iterations<=1 )
		{
		printf( "%lu output data blocks\n", (unsigned long)outputBlockCnt );
		unsigned long totalWritten = 0;
		for ( unsigned long n = 0; n < outputBlockCnt; n++ )
		    {
		    std::vector<std::string>::iterator outFHIter = evtIter->fOutFiles.begin();
		    bool equal=true;
		    for ( unsigned b = 0; b < 8; b++ )
			{
			if ( outputBlocks[n].fDataType.fID[b] !='*' )
			    {
			    equal=false;
			    break;
			    }
			}
		    for ( unsigned b = 0; b < 4; b++ )
			{
			if ( outputBlocks[n].fDataType.fOrigin[b] !='*' )
			    {
			    equal=false;
			    break;
			    }
			}
		    if ( equal )
			{
			memcpy( outputBlocks[n].fDataType.fID, outputType.fID, 8 );
			memcpy( outputBlocks[n].fDataType.fOrigin, outputType.fOrigin, 4 );
			}
		    printf( "Block %lu - size: %lu - type: '%c%c%c%c%c%c%c%c' - origin: '%c%c%c%c'- spec: 0x%08lX\n", 
			    (unsigned long)n, (unsigned long)outputBlocks[n].fSize,
			    outputBlocks[n].fDataType.fID[0], outputBlocks[n].fDataType.fID[1], 
			    outputBlocks[n].fDataType.fID[2], outputBlocks[n].fDataType.fID[3], 
			    outputBlocks[n].fDataType.fID[4], outputBlocks[n].fDataType.fID[5], 
			    outputBlocks[n].fDataType.fID[6], outputBlocks[n].fDataType.fID[7], 
			    outputBlocks[n].fDataType.fOrigin[0], outputBlocks[n].fDataType.fOrigin[1], 
			    outputBlocks[n].fDataType.fOrigin[2], outputBlocks[n].fDataType.fOrigin[3],
			    (unsigned long)outputBlocks[n].fSpecification );
// 		    if ( outFHIter != evtIter->fOutFiles.end() )
// 			printf( "Outputfile: %s\n", outFHIter->c_str() );
		    if ( outFHIter != evtIter->fOutFiles.end() )
			{
			MLUCString filename( outFHIter->c_str() );
			bool wildCardName = false;
			if ( filename.Find( '%' ) )
			    {
			    std::vector<MLUCString> tokens;
			    filename.Split( tokens, '%' );
			    std::vector<MLUCString>::iterator iter=tokens.begin(), end=tokens.end();
			    filename = *iter;
			    ++iter;
			    while ( iter != end )
				{
				switch ( (*iter)[0] )
				    {
				    case 'N':
					char tmp[256];
					snprintf( tmp, 256, "%08lu", n );
					filename += tmp;
					filename += iter->Substr( 1 );
					wildCardName = true;
					break;
				    default:
					filename += *iter;
					break;
				    }
				++iter;
				}
			    }
			int outfh = open( filename.c_str(), O_WRONLY|O_CREAT|O_TRUNC, 0666 );
			if ( outfh == -1 )
			    {
			    fprintf( stderr, "Unable to open output file %s for writing: %s (%d)",
				     filename.c_str(), strerror(errno), errno );
			    return -1;
			    }
			unsigned long written = 0;
			while ( written < outputBlocks[n].fSize )
			    {
			    ret = write( outfh, evtIter->fOutputPtr+outputBlocks[n].fOffset+written, outputBlocks[n].fSize-written );
			    if ( ret <= 0 )
				{
				ret = errno;
				fprintf( stderr, "Error writing to output file after %lu bytes: %s (%d).\n",
					 totalWritten, strerror(ret), ret );
				return -1;
				}
			    written += ret;
			    totalWritten += ret;
			    }
			close( outfh );
			printf( "   Output filename: %s\n", filename.c_str() );
			if ( !wildCardName )
			    evtIter->fOutFiles.erase( outFHIter );
			}
		    else if ( outputBlocks[n].fSize )
			{
			fprintf( stderr, "No output file specified for output block %lu\n",
				 n );
			}
		    }
		if ( edd && !evtIter->fEDDOutFile.empty() )
		    {
		    // Write out event done data block
		    printf( "EventDoneData Block size: %lu\n", 
			    (unsigned long)edd->fDataSize );
		    int outfh = open( evtIter->fEDDOutFile.c_str(), O_WRONLY|O_CREAT|O_TRUNC, 0666 );
		    if ( outfh == -1 )
			{
			fprintf( stderr, "Unable to open event done data output file %s for writing: %s (%d)",
				 evtIter->fEDDOutFile.c_str(), strerror(errno), errno );
			return -1;
			}
		    unsigned long written = 0;
		    while ( written < edd->fDataSize )
			{
			ret = write( outfh, ((uint8*)edd->fData)+written, edd->fDataSize-written );
			if ( ret <= 0 )
			    {
			    ret = errno;
			    fprintf( stderr, "Error writing to event done data output file after %lu bytes: %s (%d).\n",
				     written, strerror(ret), ret );
			    return -1;
			    }
			written += ret;
			}
		    close( outfh );
		    printf( "   Event Done Data Output filename: %s\n", evtIter->fEDDOutFile.c_str() );
		    }
		if ( trigData.fData && !evtIter->fEventTriggerOutFile.empty() )
		    {
		    // Write out event done data block
		    printf( "EventTrigger Block size: %lu\n", 
			    (unsigned long)trigData.fDataSize );
		    int outfh = open( evtIter->fEventTriggerOutFile.c_str(), O_WRONLY|O_CREAT|O_TRUNC, 0666 );
		    if ( outfh == -1 )
			{
			fprintf( stderr, "Unable to open event trigger output file %s for writing: %s (%d)",
				 evtIter->fEventTriggerOutFile.c_str(), strerror(errno), errno );
			return -1;
			}
		    unsigned long written = 0;
		    while ( written < trigData.fDataSize )
			{
			ret = write( outfh, ((uint8*)trigData.fData)+written, trigData.fDataSize-written );
			if ( ret <= 0 )
			    {
			    ret = errno;
			    fprintf( stderr, "Error writing to event trigger output file after %lu bytes: %s (%d).\n",
				     written, strerror(ret), ret );
			    return -1;
			    }
			written += ret;
			}
		    close( outfh );
		    printf( "   Event Trigger Output filename: %s\n", evtIter->fEventTriggerOutFile.c_str() );
		    }
		}
// 	    if ( inputBlocks )
// 		delete [] inputBlocks;
// 	    if ( outputPtr )
// 		delete [] outputPtr;
	    if ( edd )
		FreeMemory( (void*)NULL, edd );
	    edd = NULL;
	    if ( outputBlocks )
		FreeMemory( (void*)NULL, outputBlocks );
	    outputBlocks = NULL;
	    if ( trigData.fData )
		{
		delete reinterpret_cast<AliHLTEventTriggerData*>( trigData.fData );
		trigData.fData = NULL;
		}
	    evtID++;
	    evtIter++;
	    }
	if ( XORStructData )
	    ++(XORStructData[1]);

	}

    double tdtd = totalT / (eventIterations*events.size()*iterations);
    printf( "Total average time needed per event: %f microsec. / %f millisec. / %f s\n", 
	    tdtd, tdtd/1000.0, tdtd/1000000.0 );
    
    aliRootInterface.Unload();

#if 1
    std::vector<EventData>::iterator eventIter, eventEnd;
    eventIter = events.begin();
    eventEnd = events.end();
    while ( eventIter != eventEnd )
	{
	std::vector<BlockData>::iterator blockIter, blockEnd;
	blockIter = eventIter->fBlocks.begin();
	blockEnd = eventIter->fBlocks.end();
	while ( blockIter != blockEnd )
	    {
	    uint8*& realPtr = blockIter->fRealPtr;
	    if ( realPtr && (realPtr<XORData ||  realPtr>=(XORData+XORDataSize)) )
		delete [] realPtr;
	    realPtr = NULL;
	    ++blockIter;
	    }
	while ( eventIter->fEventTriggerData.size()>0 )
	    {
	    delete *(eventIter->fEventTriggerData.begin());
	    eventIter->fEventTriggerData.erase( eventIter->fEventTriggerData.begin() );
	    }
	if ( eventIter->fInputBlocks )
	    {
	    delete [] eventIter->fInputBlocks;
	    eventIter->fInputBlocks = NULL;
	    }
	if ( eventIter->fOutputPtr )
	    {
	    delete [] eventIter->fOutputPtr;
	    eventIter->fOutputPtr = NULL;
	    }
	++eventIter;
	}
#else
    while ( !events.empty() )
	{
	while ( !events.begin()->fBlocks.empty() )
	    {
	    uint8*& realPtr = events.begin()->fBlocks.begin()->fRealPtr;
	    if ( realPtr && (realPtr<XORData ||  realPtr>=(XORData+XORDataSize)) )
		delete [] realPtr;
	    realPtr = NULL;
	    events.begin()->fBlocks.erase( events.begin()->fBlocks.begin() );
	    }
	if ( events.begin()->fInputBlocks )
	    {
	    delete [] events.begin()->fInputBlocks;
	    events.begin()->fInputBlocks = NULL;
	    }
	if ( events.begin()->fOutputPtr )
	    {
	    delete [] events.begin()->fOutputPtr;
	    events.begin()->fOutputPtr = NULL;
	    }
	events.erase( events.begin() );
	}
#endif

    if ( XORData )
	delete [] XORData;
    XORData = NULL;


    if ( dummyInterface )
	{
	aliRootInterface.SetDummyInterface( NULL );
	delete dummyInterface;
	dummyInterface = NULL;
	}
    
    return 0;
    }
