#!/usr/bin/env python

import os, sys, string, re

### ECS parameters:
### 
###     * <command>;<parameterkey>=<parametervalue>;<parameterkey>=<parametervalue>;...
###           o Key: DETECTOR_LIST
###                 + ??Value: Comma separated list of detector ID strings??
###           o Key: DATA_FORMAT_VERSION
###                 + Value: Number of data format version exchanged between HLT Out and DAQ, currently 1
###           o Key: BEAM_TYPE
###                 + Value: pp (or PP), pA (PA), AA
###           o Key: HLT_TRIGGER_CODE
###                 + Value: String identifier into data base used by HLT to map to trigger component identifiers (O/HCDB?)
###           o Key: HLT_IN_DDL_LIST
###                 + Value: <DDL ID>:<Word Index in HLT Readout List>-<Bit position in word in HLT Readout List>,<DDL ID>:<Word Index in HLT Readout List>-<Bit position in word in HLT Readout List>,...
###                       # E.g.: 768:3-0,769:3-1
###           o Key: HLT_OUT_DD_LIST
###                 + Value: Identical to HLT_IN_DDL_LIST (Only word 29 (HLT Output) allowed
###           o Key: RUN_TYPE
###                 + Value: Run type specifier string, mostly PHYSICS, STANDALONE, ...
### 
###           o Key: HLT_MODE
###                 + Value: A, B, C, D, E
###           o Key: RUN_NUMBER
###                 + Value: String with run number representation (decimal)
###           o Key: CTP_TRIGGER_CLASS
###           o Value: <bit position>:<Trigger class identifier string>:<detector-id-nr>-<detector-id-nr>-...,<bit position>:<Trigger class identifier string>:<detector-id-nr>-<detector-id-nr>-...,...

ECSParameterList = [
    "DETECTOR_LIST",              # Comma separated list of detector ID strings
    "DATA_FORMAT_VERSION",        # Number of data format version exchanged between HLT Out and DAQ, currently 1
    "BEAM_TYPE",                  # pp (or PP), pA (PA), AA
    "HLT_TRIGGER_CODE",           # String identifier into data base used by HLT to map to trigger component identifiers (O/HCDB?)
    "HLT_IN_DDL_LIST",            # <DDL ID>:<Word Index in HLT Readout List>-<Bit position in word in HLT Readout List>,<DDL ID>:<Word Index in HLT Readout List>-<Bit position in word in HLT Readout List>,...
    "HLT_OUT_DDL_LIST",            # Identical to HLT_IN_DDL_LIST (Only word 29 (HLT Output) allowed
    "RUN_TYPE",                   # Run type specifier string, mostly PHYSICS, STANDALONE, ...
    "HLT_MODE",                   # A, B, C, D, E
    "RUN_NUMBER",                 # String with run number representation (decimal)
    "CTP_TRIGGER_CLASS",          # <bit position>:<Trigger class identifier string>:<detector-id-nr>-<detector-id-nr>-...,<bit position>:<Trigger class identifier string>:<detector-id-nr>-<detector-id-nr>-...,...
]

ECSParameters = dict([ (x, None) for x in ECSParameterList ])

settingsFileMappings = {
    "DetectorListDefault" : "DETECTOR_LIST",
    "BeamTypeDefault" : "BEAM_TYPE",
    "DataFormatVersionDefault" : "DATA_FORMAT_VERSION",
    "HLTTriggerCodeDefault" : "HLT_TRIGGER_CODE",
    "CTPTriggerClassDefault" : "CTP_TRIGGER_CLASS",
    "HLTInDDLListDefault" : "HLT_IN_DDL_LIST",
    "HLTOutDDLListDefault" : "HLT_OUT_DDL_LIST",
    "RunTypeDefault" : "RUN_TYPE",
}

commandLineMappings = {
    "-detectorlist" : "DETECTOR_LIST",
    "-dataformatversion" : "DATA_FORMAT_VERSION",
    "-beamtype" : "BEAM_TYPE",
    "-hlttriggercode" : "HLT_TRIGGER_CODE",
    "-hltinddllist" : "HLT_IN_DDL_LIST",
    "-hltoutddllist" : "HLT_OUT_DDL_LIST",
    "-runtype" : "RUN_TYPE",
    "-hltmode" : "HLT_MODE",
    "-runnumber" : "RUN_NUMBER",
    "-runnr" : "RUN_NUMBER",
    "-ctptriggerclass" : "CTP_TRIGGER_CLASS",
}

parameterExplanations = {
    "DETECTOR_LIST" : "Comma separated list of detector ID strings",
    "DATA_FORMAT_VERSION" : "Number of data format version exchanged between HLT Out and DAQ, currently 1",
    "BEAM_TYPE" : "beamtype - pp (or PP), pA (PA), AA",
    "HLT_TRIGGER_CODE" : "String identifier into data base used by HLT to map to trigger component identifiers (O/HCDB?)",
    "HLT_IN_DDL_LIST" : "Input DDL list - <DDL ID>:<Word Index in HLT Readout List>-<Bit position in word in HLT Readout List>,<DDL ID>:<Word Index in HLT Readout List>-<Bit position in word in HLT Readout List>,...",
    "HLT_OUT_DDL_LIST" : "Output DDL list - Format identical to HLT_IN_DDL_LIST (Only word 29 (HLT Output) allowed",
    "RUN_TYPE" : "Run type specifier string, mostly PHYSICS, STANDALONE, ...",
    "HLT_MODE" : "HLT mode specifier - A, B, C, D, E",
    "RUN_NUMBER" : "String with run number representation (decimal)",
    "CTP_TRIGGER_CLASS" : "CTP trigger class specification - <bit position>:<Trigger class identifier string>:<detector-id-nr>-<detector-id-nr>-...,<bit position>:<Trigger class identifier string>:<detector-id-nr>-<detector-id-nr>-...,...",
}

usage = "(-settingsfile <ECS-settings-filename>) "+( " ".join([ "("+x+" "+parameterExplanations[commandLineMappings[x]]+")" for x in commandLineMappings.keys()] ) )+" (--) <SimpleComponentWrapperParameters>*"

# print usage

args = sys.argv[1:]
error = None

scwParams = ""
paramsFinished = False

while len(args)>0 and error==None:
    if args[0]=="--":
        paramsFinished = True
        args = args[1:]
        continue
    elif not paramsFinished and args[0]=="-settingsfile":
        if len(args)<=1:
            error = "Missing settings filename parameter"
            break
        infile = open( args[1], "r" )
        line = infile.readline()
        while line != "":
            for key in settingsFileMappings.keys():
                # print key+" - "+line
                patString = "^\s*\["+key+"\] *= *(\S*) *$"
                # print patString+" - "+line
                pat = re.compile( patString )
                match = pat.match( line )
                if match != None:
                    # print "Match : "+patString+" - "+line
                    param = match.group( 1 )
                    # print key+" -> "+settingsFileMappings[key]+" -> "+param
                    ECSParameters[settingsFileMappings[key]] = param
                    break
            line = infile.readline()
        infile.close()
        args = args[2:]
        continue
    elif not paramsFinished and args[0] in commandLineMappings:
        if len(args)<=1:
            error = "Missing "+args[0]+" ECS parameter"
            break
        ECSParameters[commandLineMappings[args[0]]] = args[1]
        args = args[2:]
        continue
    else:
        scwParams = scwParams+" \""+args[0]+"\""
        args = args[1:]
        continue
    

if error==None:
    for param in ECSParameterList:
        if ECSParameters[param]==None:
            error = "ECS parameter '"+param+"' is not defined."
            break

if error!=None:
    sys.stderr.write( "Usage: "+sys.argv[0]+" "+usage+"\n" )
    sys.stderr.write( "Error: "+error+"\n" )
    sys.exit(1)


#print ECSParameters

ECSString = ";".join( [ x+"="+ECSParameters[x] for x in ECSParameterList ] )

# print ECSString

scwCommand = "SimpleComponentWrapper -alicehlt -runnr "+ECSParameters["RUN_NUMBER"]+" -hltmode "+ECSParameters["HLT_MODE"]+" -beamtype "+ECSParameters["BEAM_TYPE"]+" -runtype "+ECSParameters["RUN_TYPE"]+" -ecsparameters '"+ECSString+"' "+scwParams
print "Running "+scwCommand
    
os.system( scwCommand )

    
    
