/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <MLUCString.hpp>
#include <vector>
#include <iostream>
#include <cstring>
#include <glob.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <cstdlib>
#include <unistd.h>

bool gQuit=false;

void QuitSignalHandler( int sig );

void QuitSignalHandler( int )
{
    gQuit = true;
}

bool gHangup=false;

void HangupSignalHandler( int sig );

void HangupSignalHandler( int )
{
    gHangup = true;
}


struct TLogFileData
    {
	MLUCString fBaseName;
	unsigned long fCurrentIndex;
	unsigned long fMaxIndex;
	unsigned long fNewMaxIndex;
	unsigned long fNewMinIndex;
	bool fNewFound;
	int fFH;
	MLUCString fCurrentFilename;
	MLUCString fLineInProgress;
    };

void SplitFilename( MLUCString const& name, MLUCString& base, unsigned long& ndx )
    {
    MLUCString ndxStr;
    if ( name.Length()<=2+8+1+3 )
	{
	base = "";
	ndx = ~0UL;
	return;
	}
    base = name.Substr( 0, name.Length()-2-8-1-3 );
    ndxStr = name.Substr( name.Length()-2-8-1-3, 10 );
    char* cpErr;
    ndx = strtoul( ndxStr.c_str(), &cpErr, 0 );
    if ( *cpErr != '\0' )
	{
	base = "";
	ndx = ~0UL;
	return;
	}
    }

void AssembleName( MLUCString const& base, const unsigned long ndx, MLUCString& name )
    {
    char tmp[1024];
    sprintf( tmp, "0x%08lX", ndx );
    name = base;
    name += tmp;
    name += ".log";
    }

void DumpLine( const char* origCmd, const char* filename, const char* message )
    {
    MLUCString fullCmd;
    fullCmd = "bash -c \"";
    char cmd[strlen(origCmd)+1];
    strcpy( cmd, origCmd );

    unsigned long start=0, pos=0, len=strlen(cmd);
    while ( pos < len )
	{
	if ( cmd[pos]=='$' )
	    {
	    cmd[pos] = '\0';
	    fullCmd += cmd+start;
	    cmd[pos] = '$';
	    if ( pos+1<len )
		{
		switch ( cmd[pos+1] )
		    {
		    case '$': 
			start = pos+1;
			pos +=2;
			break;
		    case 'f':
			start = pos+2;
			fullCmd += filename;
			pos += 2;
			break;
		    case 'm':
			start = pos+2;
			fullCmd += message;
			pos += 2;
			break;
		    default:
			start = pos+1;
			pos +=2;
			break;
			
		    }
		}
	    }
	else
	    pos++;
	}
    fullCmd += cmd+start;
    fullCmd += "\"";
    system( fullCmd.c_str() );
    //std::cout << fullCmd.c_str() << std::endl;

    
			//std::cout << "File: '" << filename << " - Line: '" << line << "'" << std::endl;
    }

int main( int argc, char** argv )
    {
    int i=1;
    const char* logFileDirectory = "/var/log";
    const char* error = NULL;
    int errorParam=-1;
    int errorArg=-1;
    bool help=false;
    bool debug=false;
    unsigned long blockReadSize = 1024*1024;
    const char* cmd=NULL;

    while ( i < argc && !error )
	{
	if ( !strcmp( argv[i], "-logdir" ) )
	    {
	    if ( argc<=i+1 )
		{
		errorParam = i;
		error = "Missing log file directory parameter";
		break;
		}
	    logFileDirectory = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-command" ) )
	    {
	    if ( argc<=i+1 )
		{
		errorParam = i;
		error = "Missing command parameter";
		break;
		}
	    cmd = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-help" ) )
	    {
	    help=true;
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-debug" ) )
	    {
	    debug=true;
	    i += 1;
	    continue;
	    }
	errorParam = i;
	error = "Unknown parameter";
	}
    if ( !error )
	{
	if ( !cmd )
	    error = "Command for execution upon log message reading has to be specified (e.g. w. -command)";
	}
    if ( error || help )
	{
	std::cerr << "Usage: " << argv[0] << " (-help) -command <command> (-logdir <log-file-directory>) (-debug)" << std::endl;
	if ( error )
	    std::cerr << "Error: " << error << std::endl;
	if ( errorParam!=-1 )
	    std::cerr << "Offening parameter: " << argv[errorParam] << std::endl;
	if ( errorArg!=-1 )
	    std::cerr << "Offening argument: " << argv[errorArg] << std::endl;
	return -1;
	}

    std::vector<TLogFileData> logFiles;
    std::vector<TLogFileData>::iterator iter, end;

    MLUCString fileSpec;
    fileSpec = logFileDirectory;
    fileSpec += "/*0x????????.log";
    glob_t globDat;

    signal( SIGQUIT, QuitSignalHandler );
    signal( SIGHUP, HangupSignalHandler );

    do
	{
	if ( gHangup )
	    {
	    iter = logFiles.begin();
	    end = logFiles.end();
	    while ( iter != end )
		{
		if ( iter->fFH!=-1 )
		    {
		    if ( debug ) std::cerr << __FILE__ << ":" << __LINE__ << ": " << "Closing file '" << iter->fCurrentFilename.c_str() << std::endl;
		    close( iter->fFH );
		    iter->fFH = -1;
		    }
		iter++;
		}
	    logFiles.clear();
	    gHangup = false;
	    }
	if ( debug ) std::cerr << __FILE__ << ":" << __LINE__ << ": " << std::endl;
	globDat.gl_offs = 0;
	globDat.gl_pathc = 0;
	globDat.gl_pathv = NULL;
	int ret = glob( fileSpec.c_str(), GLOB_MARK, NULL, &globDat );
	if ( !ret && globDat.gl_pathc>0 )
	    {
	    iter = logFiles.begin();
	    end = logFiles.end();
	    while ( iter != end )
		{
		iter->fNewMaxIndex = 0;
		iter->fNewMinIndex = ~0UL;
		iter->fNewFound = false;
		iter++;
		}
	    for ( unsigned n=0; n<globDat.gl_pathc; n++ )
		{
		if ( debug ) std::cerr << __FILE__ << ":" << __LINE__ << ": " << "Found file '" << globDat.gl_pathv[n] << "'" << std::endl;
		MLUCString base;
		unsigned long ndx;
		SplitFilename( globDat.gl_pathv[n], base, ndx );
		if ( base!="" && ndx != ~0UL )
		    {
		    iter = logFiles.begin();
		    end = logFiles.end();
		    while ( iter != end )
			{
			if ( iter->fBaseName == base )
			    {
			    iter->fNewFound = true;
			    if ( iter->fNewMaxIndex < ndx )
				iter->fNewMaxIndex = ndx;
			    if ( iter->fMaxIndex < ndx )
				iter->fMaxIndex = ndx;
			    if ( iter->fNewMinIndex > ndx )
				iter->fNewMinIndex = ndx;
			    if ( iter->fFH==-1 && iter->fCurrentIndex > ndx )
				iter->fCurrentIndex = ndx;
			    if ( debug ) std::cerr << __FILE__ << ":" << __LINE__ << ": " << "Base '" << iter->fBaseName.c_str() << "' - current: " << iter->fCurrentIndex << " - max: " << iter->fMaxIndex << std::endl;
			    break;
			    }
			iter++;
			}
		    if ( iter==end )
			{
			TLogFileData lfd;
			lfd.fBaseName = base;
			lfd.fCurrentIndex = ndx;
			lfd.fMaxIndex = ndx;
			lfd.fNewFound = true;
			lfd.fNewMaxIndex = ndx;
			lfd.fNewMinIndex = ndx;
			lfd.fFH = -1;
			logFiles.push_back( lfd );
			if ( debug ) std::cerr << __FILE__ << ":" << __LINE__ << ": " << "New Base '" << lfd.fBaseName.c_str() << "' - current: " << lfd.fCurrentIndex << " - max: " << lfd.fMaxIndex << std::endl;
			}
		    }
		}
	    }
	globfree( &globDat );

	bool erased=false;
	do
	    {
	    erased=false;
	    iter = logFiles.begin();
	    end = logFiles.end();
	    while ( iter != end )
		{
		if ( !iter->fNewFound )
		    {
		    erased=true;
		    if ( iter->fFH!=-1 )
			{
			if ( debug ) std::cerr << __FILE__ << ":" << __LINE__ << ": " << "Closing file '" << iter->fCurrentFilename.c_str() << std::endl;
			close( iter->fFH );
			}
		    logFiles.erase( iter );
		    break;
		    }
		if ( iter->fNewMaxIndex < iter->fMaxIndex )
		    {
		    if ( iter->fFH!=-1 )
			{
			if ( debug ) std::cerr << __FILE__ << ":" << __LINE__ << ": " << "Closing file '" << iter->fCurrentFilename.c_str() << std::endl;
			close( iter->fFH );
			iter->fFH=-1;
			}
		    iter->fMaxIndex = iter->fNewMaxIndex;
		    iter->fCurrentIndex = iter->fNewMinIndex;
		    }
		iter++;
		}
	    }
	while ( erased );

	iter = logFiles.begin();
	end = logFiles.end();
	while ( iter != end )
	    {
	    if ( iter->fFH == -1 )
		{
		MLUCString name;
		AssembleName( iter->fBaseName, iter->fCurrentIndex, name );
		if ( debug ) std::cerr << __FILE__ << ":" << __LINE__ << ": " << "Opening file '" << name.c_str() << std::endl;
		iter->fFH = open( name.c_str(), O_RDONLY );
		iter->fCurrentFilename = name;
		}
	    iter++;
	    }


	iter = logFiles.begin();
	end = logFiles.end();
	while ( iter != end )
	    {
	    if ( iter->fFH!=-1 )
		{
		char tmp[blockReadSize+1];
		bool retry=false;
		do
		    {
		    do
			{
			retry = false;
			ret = read( iter->fFH, tmp, blockReadSize );
			if ( debug ) std::cerr << __FILE__ << ":" << __LINE__ << ": " << "Read " << ret << " bytes from group '" << iter->fBaseName.c_str() << "' - " << iter->fCurrentIndex << std::endl;
			if ( ret<0 || (ret==0 && iter->fMaxIndex > iter->fCurrentIndex) )
			    {
			    retry = true;
			    if ( debug ) std::cerr << __FILE__ << ":" << __LINE__ << ": " << "Closing file '" << iter->fCurrentFilename.c_str() << std::endl;
			    close( iter->fFH );
			    iter->fFH = -1;
			    iter->fCurrentIndex++;
			    MLUCString name;
			    AssembleName( iter->fBaseName, iter->fCurrentIndex, name );
			    if ( debug ) std::cerr << __FILE__ << ":" << __LINE__ << ": " << "Opening file '" << name.c_str() << std::endl;
			    iter->fFH = open( name.c_str(), O_RDONLY );
			    iter->fCurrentFilename = name;
			    }
			}
		    while ( retry && iter->fFH!=-1 );
		    if ( ret>0 && iter->fFH!=-1 )
			{
			unsigned lastStart=0, i=0, n=(unsigned)ret;
			tmp[n] = 0;
			while ( i<n )
			    {
			    if ( tmp[i]=='\n' )
				{
				tmp[i] = 0;
				iter->fLineInProgress += tmp+lastStart;
				DumpLine( cmd, iter->fCurrentFilename.c_str(), iter->fLineInProgress.c_str() );
				iter->fLineInProgress= "";
				lastStart = i+1;
				}
			    i++;
			    }
			iter->fLineInProgress += tmp+lastStart; // Save, because it is always zero terminated.
			}
		    }
		while ( (unsigned)ret>0 );
		}
	    iter++;
	    }

	usleep( 500000 );
	}
    while ( !gQuit );
    
    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
