#!/bin/bash

echo Phase 1: Cleaning up
rm -f {FP,DL,ERS}*.log 

echo Phase 2: Preparing
for p in FP DL ERS ; do
  for l in 0 1 2 ; do  
    for (( i=0; i<100; i++ )) ; do
      echo -n $i": " >> ${p}0x0000000${l}.log
      date +%Y%m%d.%H%M%S >> ${p}0x0000000${l}.log
    done
  done
done

echo Phase 3: Running
l=2
while true ; do
  let l=l+1
  for (( i=0; i<100; i++ )) ; do
    for p in FP DL ERS ; do
      echo -n $i": " >> ${p}0x0000000${l}.log
      date +%Y%m%d.%H%M%S >> ${p}`printf "0x%08X" ${l}`.log
      sleep 2
    done
  done
done
