# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ERSGUIForm.ui'
#
# Created: Mon Nov 8 14:45:51 2004
#      by: The PyQt User Interface Compiler (pyuic) 3.8
#
# WARNING! All changes made in this file will be lost!


from qt import *


class ERSGUIForm(QMainWindow):
    def __init__(self,parent = None,name = None,fl = 0):
        QMainWindow.__init__(self,parent,name,fl)
        self.statusBar()

        if not name:
            self.setName("ERSGUIForm")


        self.setCentralWidget(QWidget(self,"qt_central_widget"))
        ERSGUIFormLayout = QVBoxLayout(self.centralWidget(),11,6,"ERSGUIFormLayout")

        layout4 = QHBoxLayout(None,0,6,"layout4")

        self.fEventCountTitle = QLabel(self.centralWidget(),"fEventCountTitle")
        layout4.addWidget(self.fEventCountTitle)

        self.fEventCountLabel = QLineEdit(self.centralWidget(),"fEventCountLabel")
        layout4.addWidget(self.fEventCountLabel)
        ERSGUIFormLayout.addLayout(layout4)

        layout5 = QHBoxLayout(None,0,6,"layout5")

        self.fCurrentEventRateTitle = QLabel(self.centralWidget(),"fCurrentEventRateTitle")
        layout5.addWidget(self.fCurrentEventRateTitle)

        self.fCurrentEventRateLabel = QLineEdit(self.centralWidget(),"fCurrentEventRateLabel")
        layout5.addWidget(self.fCurrentEventRateLabel)
        ERSGUIFormLayout.addLayout(layout5)

        layout6 = QHBoxLayout(None,0,6,"layout6")

        self.fAverageEventRateTitle = QLabel(self.centralWidget(),"fAverageEventRateTitle")
        layout6.addWidget(self.fAverageEventRateTitle)

        self.fAverageEventRateLabel = QLineEdit(self.centralWidget(),"fAverageEventRateLabel")
        layout6.addWidget(self.fAverageEventRateLabel)
        ERSGUIFormLayout.addLayout(layout6)

        layout7 = QHBoxLayout(None,0,6,"layout7")

        self.fAverageFirstEventRateTitle = QLabel(self.centralWidget(),"fAverageFirstEventRateTitle")
        layout7.addWidget(self.fAverageFirstEventRateTitle)

        self.fAverageFirstEventRateLabel = QLineEdit(self.centralWidget(),"fAverageFirstEventRateLabel")
        self.fAverageFirstEventRateLabel.setAcceptDrops(0)
        self.fAverageFirstEventRateLabel.setReadOnly(1)
        layout7.addWidget(self.fAverageFirstEventRateLabel)
        ERSGUIFormLayout.addLayout(layout7)

        layout8 = QHBoxLayout(None,0,6,"layout8")

        self.fFilenameTitle = QLabel(self.centralWidget(),"fFilenameTitle")
        layout8.addWidget(self.fFilenameTitle)

        self.fFilenameLabel = QLineEdit(self.centralWidget(),"fFilenameLabel")
        self.fFilenameLabel.setAcceptDrops(0)
        self.fFilenameLabel.setReadOnly(1)
        layout8.addWidget(self.fFilenameLabel)
        ERSGUIFormLayout.addLayout(layout8)



        self.languageChange()

        self.resize(QSize(506,457).expandedTo(self.minimumSizeHint()))
        self.clearWState(Qt.WState_Polished)


    def languageChange(self):
        self.setCaption(self.__tr("Event Rate"))
        self.fEventCountTitle.setText(self.__tr("Events Received:"))
        self.fEventCountLabel.setText(self.__tr("0"))
        self.fCurrentEventRateTitle.setText(self.__tr("Current event rate:"))
        self.fCurrentEventRateLabel.setText(self.__tr("0 Hz"))
        self.fAverageEventRateTitle.setText(self.__tr("Average event rate since start of program:"))
        self.fAverageEventRateLabel.setText(self.__tr("0 Hz"))
        self.fAverageFirstEventRateTitle.setText(self.__tr("Average event rate since first event:"))
        self.fAverageFirstEventRateLabel.setText(self.__tr("0 Hz"))
        self.fFilenameTitle.setText(self.__tr("File used:"))


    def __tr(self,s,c = None):
        return qApp.translate("ERSGUIForm",s,c)
