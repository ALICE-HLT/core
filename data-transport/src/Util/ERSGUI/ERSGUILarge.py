#!/usr/bin/env python

from qt import *
import sys, os, posix

from ERSGUILargeForm import ERSGUILargeForm
import re

class ERSFile:
    def __init__( self, filenameprefix ):
        self.fFilenamePrefix = filenameprefix
        self.fFile = None
        self.OpenFile( self.GetLastLog( self.fFilenamePrefix ) )
        self.fEventCount = "0"
        self.fEventRateLocal = "0"
        self.fEventRateGlobal = "0"
        self.fEventRateGlobalFirst = "0"
        self.fLocalPattern = re.compile( "[a-zA-Z0-9_\.]+: (\d\d\d\d)(\d\d)(\d\d)\.(\d\d)(\d\d)(\d\d)\.(\d\d\d\d\d\d) +\d+ .+ Benchmark: Current Event Rate during last \S+ milliseconds \(\d+ events\): (\d+(\.\d+)?) Hz" )
        self.fGlobalFirstPattern = re.compile( "[a-zA-Z0-9_\.]+: (\d\d\d\d)(\d\d)(\d\d)\.(\d\d)(\d\d)(\d\d)\.(\d\d\d\d\d\d) +\d+ .+ Benchmark: Global Event Rate since first event after (\d+) events: (\d+(\.\d+)?)" )
        self.fGlobalPattern = re.compile( "[a-zA-Z0-9_\.]+: (\d\d\d\d)(\d\d)(\d\d)\.(\d\d)(\d\d)(\d\d)\.(\d\d\d\d\d\d) +\d+ .+ Benchmark: .*Global Event Rate after (\d+) events: (\d+(\.\d+)?)" )

    def GetEventCount( self ):
        return self.fEventCount

    def GetEventRateLocal( self ):
        return self.fEventRateLocal

    def GetEventRateGlobal( self ):
        return self.fEventRateGlobal

    def GetEventRateGlobalFirst( self ):
        return self.fEventRateGlobalFirst

    def GetFilename( self ):
        return self.fFilename

    def OpenFile( self, filename ):
        #print "Using file '"+filename+"'"
        if filename==None:
            self.fFile = None
            self.fFilename = None
            return
        if self.fFile!=None:
            self.fFile.close()
        self.fFilename = filename
        try:
            self.fFile = open( self.fFilename, "r" )
        except IOError:
            self.fFile = None
            self.fFilename = None
            return
        self.fFile.seek( 0, 2 )

    def GetLastLog( self, prefix ):
        found_list = []
        
        # Get all files
        files = posix.listdir( "." )
        
        # For each file found check, wether it is a numbered log file.
        for file in files:
            #print "Processing file '"+file+"'"
            spl = self.SplitFilename( file )
            #print "spl:",spl
            if spl != None:
                # If it is a numbered logfile, check if if matches the specified pattern
                #print "File prefix: "+spl[0]
                if spl[0]==prefix:
                    found_list.append( spl[1] )
        
        found_list.sort()
        if len(found_list)>0:
            return sys.argv[1]+found_list[-1]+".log"
        return None
    
    def SplitFilename( self, filename ):
            """Split up a past filename.
            
            Splits up a passed filename according to the template
            'Test-0x00000000.log`, where the prefix 'Test-' is
            arbitrary, '0x00000000' is an eight digit hex number,
            and '.log' is fixed. Returns a
            tuple consisting of the prefix and the number."""
            match = re.compile( "(.*)(0x[0-9A-F]{8})(\.log)" ).search( filename );
            if ( match == None ):
                return None
            prefix = match.group( 1 )
            number = match.group( 2 )
            return ( prefix, number )

    def GetLine( self ):
        if self.fFile==None:
            return ""
        tmp = self.fFile.readline()
        line = tmp
        while line[:-1] != '\n' and tmp!="":
            tmp = self.fFile.readline()
            line = line+tmp
        return line

    def Update( self ):
        nextFile = self.GetLastLog( self.fFilenamePrefix )
        if nextFile != self.fFilename:
            self.OpenFile( nextFile )
        if self.fFile==None:
            self.fEventCount = "0"
            self.fEventRateLocal = "0"
            self.fEventRateGlobal = "0"
            self.fEventRateGlobalFirst = "0"
            return
        line = self.GetLine()
        #print "Read line: '"+line+"'"
        while line != "":
            match = self.fLocalPattern.search( line )
            if match != None:
                self.fEventRateLocal = match.group( 8 )
                #print "Found local rate: '"+self.fEventRateLocal+"'"
            match = self.fGlobalPattern.search( line )
            if match != None:
                self.fEventRateGlobal = match.group( 9 )
                self.fEventCount = match.group( 8 )
                #print "Found global rate: '"+self.fEventRateGlobal+"'"
            match = self.fGlobalFirstPattern.search( line )
            if match != None:
                self.fEventRateGlobalFirst = match.group( 9 )
                self.fEventCount = match.group( 8 )
                #print "Found global first rate: '"+self.fEventRateGlobalFirst+"'"
            line = self.GetLine()
            
            
class ERSGUI(ERSGUILargeForm):
    def __init__( self, filename, parent = None, name = None, fl = 0 ):
        ERSGUILargeForm.__init__( self, parent, name, fl )
        self.fFilename = filename
        self.fFile = ERSFile( filename )
        self.fTimer = QTimer( self )
        self.connect( self.fTimer, SIGNAL("timeout()"),
                      self.TimerExpired )
        self.fTimer.start( 1000, 1 )
        font = QFont( QString( "Helvetica" ), 22, 50, 1 )
        self.fEventCountLabel.setFont( font )
        self.fCurrentEventRateLabel.setFont( font )
        self.fAverageFirstEventRateLabel.setFont( font )
        self.fEventCountTitle.setFont( font )
        self.fCurrentEventRateTitle.setFont( font )
        self.fAverageFirstEventRateTitle.setFont( font )


    def TimerExpired(self):
        self.fFile.Update()
        self.fEventCountLabel.setText( self.fFile.GetEventCount() )
        self.fCurrentEventRateLabel.setText( self.fFile.GetEventRateLocal()+" Hz" )
        self.fAverageFirstEventRateLabel.setText( self.fFile.GetEventRateGlobalFirst()+" Hz" )
        self.fTimer.start( 1000, 1 )
        
if __name__ == '__main__':
    app = QApplication(sys.argv)

    if len(sys.argv)!=2:
        print "Usage: "+sys.argv[0]+" <logfile-prefix>"
        sys.exit( -1 )
    filename = sys.argv[1]
    QObject.connect(app, SIGNAL('lastWindowClosed()'),
                    app, SLOT('quit()'))
    win = ERSGUI( filename )
    app.setMainWidget(win)
    win.show()
    app.exec_loop()

