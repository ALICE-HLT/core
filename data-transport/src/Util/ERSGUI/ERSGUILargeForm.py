# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ERSGUILargeForm.ui'
#
# Created: Mon Nov 8 14:53:51 2004
#      by: The PyQt User Interface Compiler (pyuic) 3.8
#
# WARNING! All changes made in this file will be lost!


from qt import *


class ERSGUILargeForm(QMainWindow):
    def __init__(self,parent = None,name = None,fl = 0):
        QMainWindow.__init__(self,parent,name,fl)
        self.statusBar()

        if not name:
            self.setName("ERSGUILargeForm")


        self.setCentralWidget(QWidget(self,"qt_central_widget"))
        ERSGUILargeFormLayout = QVBoxLayout(self.centralWidget(),11,6,"ERSGUILargeFormLayout")

        layout4 = QHBoxLayout(None,0,6,"layout4")

        self.fEventCountTitle = QLabel(self.centralWidget(),"fEventCountTitle")
        layout4.addWidget(self.fEventCountTitle)

        self.fEventCountLabel = QLineEdit(self.centralWidget(),"fEventCountLabel")
        layout4.addWidget(self.fEventCountLabel)
        ERSGUILargeFormLayout.addLayout(layout4)

        layout5 = QHBoxLayout(None,0,6,"layout5")

        self.fCurrentEventRateTitle = QLabel(self.centralWidget(),"fCurrentEventRateTitle")
        layout5.addWidget(self.fCurrentEventRateTitle)

        self.fCurrentEventRateLabel = QLineEdit(self.centralWidget(),"fCurrentEventRateLabel")
        layout5.addWidget(self.fCurrentEventRateLabel)
        ERSGUILargeFormLayout.addLayout(layout5)

        layout7 = QHBoxLayout(None,0,6,"layout7")

        self.fAverageFirstEventRateTitle = QLabel(self.centralWidget(),"fAverageFirstEventRateTitle")
        layout7.addWidget(self.fAverageFirstEventRateTitle)

        self.fAverageFirstEventRateLabel = QLineEdit(self.centralWidget(),"fAverageFirstEventRateLabel")
        self.fAverageFirstEventRateLabel.setAcceptDrops(0)
        self.fAverageFirstEventRateLabel.setReadOnly(1)
        layout7.addWidget(self.fAverageFirstEventRateLabel)
        ERSGUILargeFormLayout.addLayout(layout7)



        self.languageChange()

        self.resize(QSize(390,320).expandedTo(self.minimumSizeHint()))
        self.clearWState(Qt.WState_Polished)


    def languageChange(self):
        self.setCaption(self.__tr("Event Rate"))
        self.fEventCountTitle.setText(self.__tr("Events Received:"))
        self.fEventCountLabel.setText(self.__tr("0"))
        self.fCurrentEventRateTitle.setText(self.__tr("Current event rate:"))
        self.fCurrentEventRateLabel.setText(self.__tr("0 Hz"))
        self.fAverageFirstEventRateTitle.setText(self.__tr("Average event rate:"))
        self.fAverageFirstEventRateLabel.setText(self.__tr("0 Hz"))


    def __tr(self,s,c = None):
        return qApp.translate("ERSGUILargeForm",s,c)
