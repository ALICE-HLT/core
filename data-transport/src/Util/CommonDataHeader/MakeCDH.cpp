/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <fstream>
#include <iostream>
#include <istream>
#include <ios>
#include <iomanip>
#include <vector>
#include <cstring>
#include <cerrno>
#include <climits>
#include <cstdlib>
#include "CommonDataHeader.hpp"



int main( int argc, char** argv )
    {
    const char* usage = "(-blocklength <block length>) (-formatversion <format version>) (-l1triggertype <L1 trigger type>) (-eventid1 <event ID 1>) (-eventid2 <event ID 2>) (-eventid <event ID>) (-partsubdet <participating sub-detectors>) (-blockattr <block attributes>) (-minieventid <mini event ID>) (-statuserrorbits <status & error bits>) (-triggerclasseslow <trigger classes low>) (-triggerclassesmidlow <trigger classes mid-low>) (-triggerclassesmidhigh <trigger classes mid-high>)(-triggerclasseshigh <trigger classes high>) (-roilow <RoI low>) (-roihigh <RoI high>) (-roi <RoI>)";
    
    int i=1;
    std::vector<char*> inputFilenames;
    const char* errorStr=NULL;
    int errorArg = -1;
    int errorParam = -1;
    char* cpErr;
    

    TCommonDataHeader::uint32 CDH[TCommonDataHeader::GetCurrentHeaderWordCount()];
    TCommonDataHeader cdh( CDH );
    
    cdh.Reset();
    


    while ( i<argc && !errorStr )
        {
        if ( std::string(argv[i])=="-blocklength" )
            {
            if ( i+1>=argc )
                {
                errorStr = "Missing block length parameter";
                errorArg = i;
                break;
                }
            unsigned long blocklength = strtoul( argv[i+1], &cpErr, 0 );
            if ( *cpErr != '\0' )
                {
                errorStr = "Cannot convert block length parameter to unsigned";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( blocklength > UINT_MAX )
                {
                errorStr = "Specified block length parameter is too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( !cdh.SetBlockLength( (unsigned)blocklength ) )
                {
                errorStr = "Error setting specified block length parameter in common data header. Specified value might be too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            i += 2;
            continue;
            }
        if ( std::string(argv[i])=="-formatversion" )
            {
            if ( i+1>=argc )
                {
                errorStr = "Missing format version parameter";
                errorArg = i;
                break;
                }
            unsigned long formatversion = strtoul( argv[i+1], &cpErr, 0 );
            if ( *cpErr != '\0' )
                {
                errorStr = "Cannot convert format version parameter to unsigned";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( formatversion > UINT_MAX )
                {
                errorStr = "Specified format version parameter is too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( !cdh.SetFormatVersion( (unsigned)formatversion ) )
                {
                errorStr = "Error setting specified format version parameter in common data header. Specified value might be too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            i += 2;
            continue;
            }
        if ( std::string(argv[i])=="-l1triggertype" )
            {
            if ( i+1>=argc )
                {
                errorStr = "Missing L1 trigger type parameter";
                errorArg = i;
                break;
                }
            unsigned long l1triggertype = strtoul( argv[i+1], &cpErr, 0 );
            if ( *cpErr != '\0' )
                {
                errorStr = "Cannot convert L1 trigger type parameter to unsigned";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( l1triggertype > UINT_MAX )
                {
                errorStr = "Specified L1 trigger type parameter is too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( !cdh.SetL1TriggerType( (unsigned)l1triggertype ) )
                {
                errorStr = "Error setting specified L1 trigger type parameter in common data header. Specified value might be too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            i += 2;
            continue;
            }
        if ( std::string(argv[i])=="-eventid1" )
            {
            if ( i+1>=argc )
                {
                errorStr = "Missing event ID 1 parameter";
                errorArg = i;
                break;
                }
            unsigned long eventID1 = strtoul( argv[i+1], &cpErr, 0 );
            if ( *cpErr != '\0' )
                {
                errorStr = "Cannot convert event ID 1 parameter to unsigned";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( eventID1 > UINT_MAX )
                {
                errorStr = "Specified event ID 1 parameter is too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( !cdh.SetEventID1( (unsigned)eventID1 ) )
                {
                errorStr = "Error setting specified event ID 1 parameter in common data header. Specified value might be too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            i += 2;
            continue;
            }
        if ( std::string(argv[i])=="-eventid2" )
            {
            if ( i+1>=argc )
                {
                errorStr = "Missing event ID 2 parameter";
                errorArg = i;
                break;
                }
            unsigned long eventID2 = strtoul( argv[i+1], &cpErr, 0 );
            if ( *cpErr != '\0' )
                {
                errorStr = "Cannot convert event ID 2 parameter to unsigned";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( eventID2 > UINT_MAX )
                {
                errorStr = "Specified event ID 2 parameter is too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( !cdh.SetEventID2( (unsigned)eventID2 ) )
                {
                errorStr = "Error setting specified event ID 2 parameter in common data header. Specified value might be too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            i += 2;
            continue;
            }
        if ( std::string(argv[i])=="-eventid" )
            {
            if ( i+1>=argc )
                {
                errorStr = "Missing event ID parameter";
                errorArg = i;
                break;
                }
            unsigned long long eventID = strtoull( argv[i+1], &cpErr, 0 );
            if ( *cpErr != '\0' )
                {
                errorStr = "Cannot convert event ID parameter to unsigned";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( !cdh.SetEventID( eventID ) )
                {
                errorStr = "Error setting specified event ID parameter in common data header. Specified value might be too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            i += 2;
            continue;
            }
        if ( std::string(argv[i])=="-partsubdet" )
            {
            if ( i+1>=argc )
                {
                errorStr = "Missing participating sub-detectors parameter";
                errorArg = i;
                break;
                }
            unsigned long partSubDet = strtoul( argv[i+1], &cpErr, 0 );
            if ( *cpErr != '\0' )
                {
                errorStr = "Cannot convert participating sub-detectors parameter to unsigned";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( partSubDet > UINT_MAX )
                {
                errorStr = "Specified participating sub-detectors parameter is too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( !cdh.SetParticipatingSubDetectors( (unsigned)partSubDet ) )
                {
                errorStr = "Error setting specified participating sub-detectors parameter in common data header. Specified value might be too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            i += 2;
            continue;
            }
        if ( std::string(argv[i])=="-blockattr" )
            {
            if ( i+1>=argc )
                {
                errorStr = "Missing block attributes parameter";
                errorArg = i;
                break;
                }
            unsigned long blockAttr = strtoul( argv[i+1], &cpErr, 0 );
            if ( *cpErr != '\0' )
                {
                errorStr = "Cannot convert block attributes parameter to unsigned";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( blockAttr > UINT_MAX )
                {
                errorStr = "Specified block attributes parameter is too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( !cdh.SetBlockAttributes( (unsigned)blockAttr ) )
                {
                errorStr = "Error setting specified block attributes parameter in common data header. Specified value might be too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            i += 2;
            continue;
            }
        if ( std::string(argv[i])=="-minieventid" )
            {
            if ( i+1>=argc )
                {
                errorStr = "Missing mini event ID parameter";
                errorArg = i;
                break;
                }
            unsigned long miniEventID = strtoul( argv[i+1], &cpErr, 0 );
            if ( *cpErr != '\0' )
                {
                errorStr = "Cannot convert mini event ID parameter to unsigned";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( miniEventID > UINT_MAX )
                {
                errorStr = "Specified mini event ID parameter is too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( !cdh.SetMiniEventID( (unsigned)miniEventID ) )
                {
                errorStr = "Error setting specified mini event ID parameter in common data header. Specified value might be too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            i += 2;
            continue;
            }
        if ( std::string(argv[i])=="-statuserrorbits" )
            {
            if ( i+1>=argc )
                {
                errorStr = "Missing status & error bits parameter";
                errorArg = i;
                break;
                }
            unsigned long statusErrorBits = strtoul( argv[i+1], &cpErr, 0 );
            if ( *cpErr != '\0' )
                {
                errorStr = "Cannot convert status & error bits parameter to unsigned";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( statusErrorBits > UINT_MAX )
                {
                errorStr = "Specified status & error bits parameter is too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( !cdh.SetStatusErrorBits( (unsigned)statusErrorBits ) )
                {
                errorStr = "Error setting specified status & error bits parameter in common data header. Specified value might be too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            i += 2;
            continue;
            }
        if ( std::string(argv[i])=="-triggerclasseslow" )
            {
            if ( i+1>=argc )
                {
                errorStr = "Missing trigger classes low parameter";
                errorArg = i;
                break;
                }
            unsigned long triggerClassesLow = strtoul( argv[i+1], &cpErr, 0 );
            if ( *cpErr != '\0' )
                {
                errorStr = "Cannot convert trigger classes low parameter to unsigned";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( triggerClassesLow > UINT_MAX )
                {
                errorStr = "Specified trigger classes low parameter is too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( !cdh.SetTriggerClassesLow( (unsigned)triggerClassesLow ) )
                {
                errorStr = "Error setting specified trigger classes low parameter in common data header. Specified value might be too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            i += 2;
            continue;
            }
        if ( std::string(argv[i])=="-triggerclassesmidlow" )
            {
            if ( i+1>=argc )
                {
                errorStr = "Missing trigger classes mid-low parameter";
                errorArg = i;
                break;
                }
            unsigned long triggerClassesMidLow = strtoul( argv[i+1], &cpErr, 0 );
            if ( *cpErr != '\0' )
                {
                errorStr = "Cannot convert trigger classes mid-low parameter to unsigned";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( triggerClassesMidLow > UINT_MAX )
                {
                errorStr = "Specified trigger classes mid-low parameter is too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( !cdh.SetTriggerClassesMidLow( (unsigned)triggerClassesMidLow ) )
                {
                errorStr = "Error setting specified trigger classes mid-low parameter in common data header. Specified value might be too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            i += 2;
            continue;
            }
        if ( std::string(argv[i])=="-triggerclassesmidhigh" )
            {
            if ( i+1>=argc )
                {
                errorStr = "Missing trigger classes mid-high parameter";
                errorArg = i;
                break;
                }
            unsigned long triggerClassesMidHigh = strtoul( argv[i+1], &cpErr, 0 );
            if ( *cpErr != '\0' )
                {
                errorStr = "Cannot convert trigger classes mid-high parameter to unsigned";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( triggerClassesMidHigh > UINT_MAX )
                {
                errorStr = "Specified trigger classes mid-high parameter is too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( !cdh.SetTriggerClassesMidHigh( (unsigned)triggerClassesMidHigh ) )
                {
                errorStr = "Error setting specified trigger classes mid-high parameter in common data header. Specified value might be too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            i += 2;
            continue;
            }
        if ( std::string(argv[i])=="-triggerclasseshigh" )
            {
            if ( i+1>=argc )
                {
                errorStr = "Missing trigger classes high parameter";
                errorArg = i;
                break;
                }
            unsigned long triggerClassesHigh = strtoul( argv[i+1], &cpErr, 0 );
            if ( *cpErr != '\0' )
                {
                errorStr = "Cannot convert trigger classes high parameter to unsigned";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( triggerClassesHigh > UINT_MAX )
                {
                errorStr = "Specified trigger classes high parameter is too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( !cdh.SetTriggerClassesHigh( (unsigned)triggerClassesHigh ) )
                {
                errorStr = "Error setting specified trigger classes high parameter in common data header. Specified value might be too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            i += 2;
            continue;
            }
        if ( std::string(argv[i])=="-roilow" )
            {
            if ( i+1>=argc )
                {
                errorStr = "Missing RoI low parameter";
                errorArg = i;
                break;
                }
            unsigned long roiLow = strtoul( argv[i+1], &cpErr, 0 );
            if ( *cpErr != '\0' )
                {
                errorStr = "Cannot convert RoI low parameter to unsigned";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( roiLow > UINT_MAX )
                {
                errorStr = "Specified RoI low parameter is too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( !cdh.SetRoILow( (unsigned)roiLow ) )
                {
                errorStr = "Error setting specified RoI low parameter in common data header. Specified value might be too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            i += 2;
            continue;
            }
        if ( std::string(argv[i])=="-roihigh" )
            {
            if ( i+1>=argc )
                {
                errorStr = "Missing RoI high parameter";
                errorArg = i;
                break;
                }
            unsigned long roiHigh = strtoul( argv[i+1], &cpErr, 0 );
            if ( *cpErr != '\0' )
                {
                errorStr = "Cannot convert RoI high parameter to unsigned";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( roiHigh > UINT_MAX )
                {
                errorStr = "Specified RoI high parameter is too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( !cdh.SetRoIHigh( (unsigned)roiHigh ) )
                {
                errorStr = "Error setting specified RoI high parameter in common data header. Specified value might be too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            i += 2;
            continue;
            }
        if ( std::string(argv[i])=="-roi" )
            {
            if ( i+1>=argc )
                {
                errorStr = "Missing RoI parameter";
                errorArg = i;
                break;
                }
            unsigned long long roi = strtoull( argv[i+1], &cpErr, 0 );
            if ( *cpErr != '\0' )
                {
                errorStr = "Cannot convert RoI parameter to unsigned";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            if ( !cdh.SetRoI( (unsigned long long)roi ) )
                {
                errorStr = "Error setting specified RoI parameter in common data header. Specified value might be too big";
                errorArg = i;
                errorParam = i+1;
                break;
                }
            i += 2;
            continue;
            }
	if ( std::string(argv[i])=="-h" || std::string(argv[i])=="-help" || std::string(argv[i])=="--help" || std::string(argv[i])=="-?" )
	    {
	    errorStr = "Help requested";
	    break;
	    }
	errorStr = "Unknown argument";
	errorArg = i;
	break;
        }
    
    if ( errorStr )
        {
        std::cerr << "Usage: " << argv[0] << " " << usage << std::endl;
	if ( std::string(errorStr) != "Help requested" )
	    std::cerr << "Error: " << errorStr << std::endl;
        if ( errorArg!=-1 )
            std::cerr << "Offending argument: '" << argv[errorArg] << "'" << std::endl;
        if ( errorParam!=-1 )
            std::cerr << "Offending parameter: '" << argv[errorParam] << "'" << std::endl;
        return -1;
        }

    std::cout.write( (char*)CDH, TCommonDataHeader::GetCurrentHeaderSize() );
	if ( !std::cout.good() )
	    {
	    std::cerr << "Error writing common data header to standard output: "
                  << strerror(errno) << " (" << errno << ")" << std::endl;
        return -1;
	    }
    return 0;

    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
