/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <fstream>
#include <iostream>
#include <istream>
#include <ios>
#include <iomanip>
#include <vector>
#include <cstring>
#include <cerrno>
#include <climits>
#include "CommonDataHeader.hpp"



int main( int argc, char** argv )
    {
    int i=1;
    std::vector<char*> inputFilenames;
    const char* errorStr=NULL;
    int errorArg = -1;
    int errorParam = -1;
    bool onlyOpts=false;
    bool showWords=false;
    bool showFields=false;

    while ( i<argc && !errorStr )
	{
	if ( !strcmp( argv[i], "-" ) )
	    {
	    inputFilenames.push_back( argv[i] );
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "--" ) )
	    {
	    onlyOpts = true;
	    i += 1;
	    continue;
	    }
	if ( !onlyOpts && argv[i][0]=='-' )
	    {
	    // Decode option
	    if ( !strcmp( "-f", argv[i] ) || !strcmp( "--fields", argv[i] ) )
		{
		showFields = true;
		i += 1;
		continue;
		}
	    if ( !strcmp( "-w", argv[i] ) || !strcmp( "--words", argv[i] ) )
		{
		showWords = true;
		i += 1;
		continue;
		}
	    errorStr = "Unknown option";
	    errorArg = i;
	    break;
	    }
	inputFilenames.push_back( argv[i] );
	i += 1;
	continue;
	}

    if ( !errorStr )
	{
	if ( inputFilenames.size()<=0 )
	    errorStr = "No input filename given";
	}
    if ( errorStr )
	{
	std::cerr << "Usage: " << argv[0] << " (-w||--words) (-f|--fields) (-|<input-filename>)+" << std::endl;
	std::cerr << "Error: " << errorStr << std::endl;
	if ( errorArg!=-1 )
	    std::cerr << "Offending argument: '" << argv[errorArg] << "'" << std::endl;
	if ( errorParam!=-1 )
	    std::cerr << "Offending parameter: '" << argv[errorParam] << "'" << std::endl;
	return -1;
	}

    if ( !showFields && !showWords )
	showWords=true;

    TCommonDataHeader::uint32 CDH[TCommonDataHeader::GetCurrentHeaderWordCount()];

    using std::ios_base;
    using std::setfill;
    using std::setw;
    using std::cout;

    std::vector<char*>::iterator inputFilenameIter, inputFilenameEnd;
    inputFilenameIter = inputFilenames.begin();
    inputFilenameEnd = inputFilenames.end();
    while ( inputFilenameIter!=inputFilenameEnd )
	{
	std::istream* input = NULL;
	std::ifstream infile;
	if ( !strcmp( *inputFilenameIter, "-" ) )
	    {
	    input = &std::cin;
	    }
	else
	    {
	    infile.open( *inputFilenameIter, ios_base::in|ios_base::binary );
	    if ( !infile.good() )
		{
		std::cerr << "Unable to open file '" << *inputFilenameIter << "': "
			  << strerror(errno) << " (" << errno << ")" << std::endl;
		inputFilenameIter++;
		continue;
		}
	    input = &infile;
	    }
	
	input->read( (char*)CDH, TCommonDataHeader::GetCurrentHeaderSize() );
	if ( !input->good() )
	    {
	    std::cerr << "Error reading common data header from file '" << *inputFilenameIter << "': "
		      << strerror(errno) << " (" << errno << ")" << std::endl;
	    }
	else
	    {
	    // Have to use printf here, ios::width somehow does not work...
	    printf( "\n" );
	    printf( "%s:\n", *inputFilenameIter );
	    
	    TCommonDataHeader cdh( CDH );
	    
	    TCommonDataHeader::uint32 formatVersion = cdh.GetFormatVersion(); //Set value to format version
	    TCommonDataHeader::uint32 MBZ0 = cdh.GetMBZ0(); // Set value to MBZ
	    TCommonDataHeader::uint32 l1TriggerMessage = cdh.GetL1TriggerType(); // Set value to L1 trigger message
	    TCommonDataHeader::uint32 MBZ1 = cdh.GetMBZ1(); // Set value to MBZ
	    TCommonDataHeader::uint32 eventID1 = cdh.GetEventID1(); // Set value to event ID 1 (BC)
	    
	    TCommonDataHeader::uint32 MBZ2 = cdh.GetMBZ2(); // Set value to MBZ
	    TCommonDataHeader::uint32 eventID2 = cdh.GetEventID2(); // Set value to event ID 2 (orbit counter)
	    
	    TCommonDataHeader::uint32 PARrequests = cdh.GetPARrequests(); // 
	    TCommonDataHeader::uint32 participatingSubDetectors = cdh.GetParticipatingSubDetectors(); // Set value to participating sub detectors
	    TCommonDataHeader::uint32 blockAttributes = cdh.GetBlockAttributes(); // Set value to block attributes
	    
	    TCommonDataHeader::uint32 miniEventID = cdh.GetMiniEventID(); // Set value to mini event ID (BC)
	    TCommonDataHeader::uint32 statusError = cdh.GetStatusErrorBits(); // Set value to status & error bits
	    TCommonDataHeader::uint32 MBZ3 = cdh.GetMBZ3(); // Set value to MBZ
	    
	    TCommonDataHeader::uint32 triggerClassesHigh = cdh.GetTriggerClassesHigh();
	    TCommonDataHeader::uint32 triggerClassesLow = cdh.GetTriggerClassesLow();
	    TCommonDataHeader::uint32 triggerClassesMidLow = cdh.GetTriggerClassesMidLow();
	    TCommonDataHeader::uint32 triggerClassesMidHigh = cdh.GetTriggerClassesMidHigh();
	    TCommonDataHeader::uint32 MBZ4 = cdh.GetMBZ4(); // Set value to MBZ
	    TCommonDataHeader::uint32 roiLow = cdh.GetRoILow(); // Set value to RoI Low bits
	    TCommonDataHeader::uint32 roiHigh = cdh.GetRoIHigh(); // Set value to RoI High bits
	    TCommonDataHeader::uint32 MBZ5 = cdh.GetMBZ5(); // Set value to MBZ
	    
	    unsigned long long eventID;
	    eventID=eventID2;
	    eventID <<= 12;
	    eventID |= eventID1;
	    
	    unsigned long long roi;
	    roi = roiHigh;
	    roi <<= 4; 
	    roi = roiLow;

	    if ( showWords ) {
	      if( formatVersion < 3 ) {
		printf( "Word 0: 0x%08X ( %10u ) --- Block length: 0x%08X ( %u )\n",
			(unsigned)CDH[0], (unsigned)CDH[0], (unsigned)CDH[0], (unsigned)CDH[0] );
		
		printf( "Word 1: 0x%08X ( %10u ) --- Format version: 0x%02X ( %u ) --- MBZ: 0x%01X ( %u ) --- L1 trigger message: 0x%03X ( %u ) --- MBZ: 0x%01X ( %u ) --- Event ID 1 (BC): 0x%03X ( %u )\n",
			(unsigned)CDH[1], (unsigned)CDH[1], 
			(unsigned)formatVersion, (unsigned)formatVersion, 
			(unsigned)MBZ0, (unsigned)MBZ0, 
			(unsigned)l1TriggerMessage, (unsigned)l1TriggerMessage, 
			(unsigned)MBZ1, (unsigned)MBZ1, 
			(unsigned)eventID1, (unsigned)eventID1 );
		
		printf( "Word 2: 0x%08X ( %10u ) --- MBZ: 0x%02X ( %u ) --- Event ID 2 (Orbit): 0x%06X ( %u )\n",
			(unsigned)CDH[2], (unsigned)CDH[2], 
			(unsigned)MBZ2, (unsigned)MBZ2, 
			(unsigned)eventID2, (unsigned)eventID2 );
		
		printf( "Word 3: 0x%08X ( %10u ) --- Block Attributes: 0x%02X ( %u ) --- Participating Sub Detectors: 0x%06X ( %u )\n",
			(unsigned)CDH[3], (unsigned)CDH[3], 
			(unsigned)blockAttributes, (unsigned)blockAttributes, 
			(unsigned)participatingSubDetectors, (unsigned)participatingSubDetectors );
		
		printf( "Word 4: 0x%08X ( %10u ) --- MBZ: 0x%02X ( %u ) --- Status & Error Bits: 0x%04X ( %u ) --- Mini Event ID (BC): 0x%03X ( %u )\n",
			(unsigned)CDH[4], (unsigned)CDH[4], 
			(unsigned)MBZ3, (unsigned)MBZ3, 
			(unsigned)statusError, (unsigned)statusError, 
			(unsigned)miniEventID, (unsigned)miniEventID );
		
		printf( "Word 5: 0x%08X ( %10u ) --- Trigger classes low: 0x%08X ( %u )\n",
			(unsigned)CDH[5], (unsigned)CDH[5], (unsigned)triggerClassesLow, (unsigned)triggerClassesLow );
		
		printf( "Word 6: 0x%08X ( %10u ) --- RoI Low: 0x%01X ( %u ) --- MBZ: 0x%03X ( %u ) --- Trigger Classes High: 0x%05X ( %u )\n",
			(unsigned)CDH[6], (unsigned)CDH[6], 
			(unsigned)roiLow, (unsigned)roiLow, 
			(unsigned)MBZ4, (unsigned)MBZ4, 
			(unsigned)triggerClassesHigh, (unsigned)triggerClassesHigh );
		
		printf( "Word 7: 0x%08X ( %10u ) --- RoI High: 0x%08X ( %u )\n",
			(unsigned)CDH[7], (unsigned)CDH[7], (unsigned)roiHigh, (unsigned)roiHigh );
	      } else {
		printf( "Word 0: 0x%08X ( %10u ) --- Block length: 0x%08X ( %u )\n",
			(unsigned)CDH[0], (unsigned)CDH[0], (unsigned)CDH[0], (unsigned)CDH[0] );
		
		printf( "Word 1: 0x%08X ( %10u ) --- Format version: 0x%02X ( %u ) --- MBZ: 0x%01X ( %u ) --- L1 trigger message: 0x%03X ( %u ) --- MBZ: 0x%01X ( %u ) --- Event ID 1 (BC): 0x%03X ( %u )\n",
			(unsigned)CDH[1], (unsigned)CDH[1], 
			(unsigned)formatVersion, (unsigned)formatVersion, 
			(unsigned)MBZ0, (unsigned)MBZ0, 
			(unsigned)l1TriggerMessage, (unsigned)l1TriggerMessage, 
			(unsigned)MBZ1, (unsigned)MBZ1, 
			(unsigned)eventID1, (unsigned)eventID1 );
		
		printf( "Word 2: 0x%08X ( %10u ) --- PAR: 0x%02X ( %u ) --- Event ID 2 (Orbit): 0x%06X ( %u )\n",
			(unsigned)CDH[2], (unsigned)CDH[2], 
			(unsigned)PARrequests, (unsigned)PARrequests, 
			(unsigned)eventID2, (unsigned)eventID2 );
		
		printf( "Word 3: 0x%08X ( %10u ) --- Block Attributes: 0x%02X ( %u ) --- Participating Sub Detectors: 0x%06X ( %u )\n",
			(unsigned)CDH[3], (unsigned)CDH[3], 
			(unsigned)blockAttributes, (unsigned)blockAttributes, 
			(unsigned)participatingSubDetectors, (unsigned)participatingSubDetectors );
		
		printf( "Word 4: 0x%08X ( %10u ) --- Status & Error Bits: 0x%04X ( %u ) --- Mini Event ID (BC): 0x%03X ( %u )\n",
			(unsigned)CDH[4], (unsigned)CDH[4], 
			(unsigned)statusError, (unsigned)statusError, 
			(unsigned)miniEventID, (unsigned)miniEventID );
		
		printf( "Word 5: 0x%08X ( %10u ) --- Trigger classes low: 0x%08X ( %u )\n",
			(unsigned)CDH[5], (unsigned)CDH[5], (unsigned)triggerClassesLow, (unsigned)triggerClassesLow );
		
		printf( "Word 6: 0x%08X ( %10u ) --- Trigger Classes middle-low: 0x%08X ( %u )\n",
			(unsigned)CDH[6], (unsigned)CDH[6], 
			(unsigned)triggerClassesMidLow, (unsigned)triggerClassesMidLow);

		printf( "Word 7: 0x%08X ( %10u ) --- Trigger Classes middle-high: 0x%08X ( %u )\n",
			(unsigned)CDH[7], (unsigned)CDH[7],
			(unsigned)triggerClassesMidHigh, (unsigned)triggerClassesMidHigh);

		printf( "Word 8: 0x%08X ( %10u ) --- RoI Low: 0x%01X ( %u ) --- MBZ: 0x%03X ( %u ) --- Trigger Classes High: 0x%01X ( %u )\n",
			(unsigned)CDH[8], (unsigned)CDH[8], 
			(unsigned)roiLow, (unsigned)roiLow, 
			(unsigned)MBZ5, (unsigned)MBZ5, 
			(unsigned)triggerClassesHigh, (unsigned)triggerClassesHigh);
		
		printf( "Word 9: 0x%08X ( %10u ) --- RoI High: 0x%08X ( %u )\n",
			(unsigned)CDH[9], (unsigned)CDH[9], (unsigned)roiHigh, (unsigned)roiHigh );
	      }
	    }
	    if ( showFields )
		{
		
		printf( "Block length: 0x%08X ( %u )\n",
			(unsigned)CDH[0], (unsigned)CDH[0] );
		
		printf( "Format version: 0x%02X ( %u )\n",
			(unsigned)formatVersion, (unsigned)formatVersion );
		
		printf( "L1 trigger message: 0x%03X ( %u )\n",
			(unsigned)l1TriggerMessage, (unsigned)l1TriggerMessage );
		
		printf( "Event ID: 0x%09LX ( %Lu )\n",
			(unsigned long long)eventID, (unsigned long long)eventID );
		
		printf( "Block Attributes: 0x%02X ( %u )\n",
			(unsigned)blockAttributes, (unsigned)blockAttributes );
		
		printf( "Participating Sub Detectors: 0x%06X ( %u )\n",
			(unsigned)participatingSubDetectors, (unsigned)participatingSubDetectors );
		
		printf( "Status & Error Bits: 0x%04X ( %u )\n",
			(unsigned)statusError, (unsigned)statusError );
		
		printf( "Mini Event ID (BC): 0x%03X ( %u )\n",
			(unsigned)miniEventID, (unsigned)miniEventID );
		
		printf( "Trigger Classes Low: 0x%08LX ( %Lu )\n",
			(unsigned long long)triggerClassesLow, (unsigned long long)triggerClassesLow );

                if ( formatVersion >= 3 ) {
                        printf( "Trigger Classes Mid-Low: 0x%08LX ( %Lu )\n",
                                (unsigned long long)triggerClassesMidLow, (unsigned long long)triggerClassesMidLow );

                        printf( "Trigger Classes Mid-High: 0x%08LX ( %Lu )\n",
                                (unsigned long long)triggerClassesMidHigh, (unsigned long long)triggerClassesMidHigh );
                }

		printf( "Trigger Classes High: 0x%05LX ( %Lu )\n",
			(unsigned long long)triggerClassesHigh, (unsigned long long)triggerClassesHigh );
		
		printf( "RoI: 0x%09LX ( %Lu )\n",
			(unsigned long long)roi, (unsigned long long)roi );
		}
	    
	    }

	if ( input == &infile )
	    infile.close();
	input = NULL;
	inputFilenameIter++;
	}
    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
