#ifndef _COMMONDATAHEADER_HPP_
#define _COMMONDATAHEADER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <climits>
#include <cassert>
#include <cstring>
#include <iostream>


class TCommonDataHeader
    {
    public:


#if !defined(USE_ROOT) && !defined(__CINT__)

	typedef unsigned char uint8;


// First cdh_uint32_t
#if USHRT_MAX==4294967295
	typedef unsigned short uint32;
#else // USHRT_MAX==4294967295

#if UINT_MAX==4294967295
	typedef unsigned int uint32;
#else // UINT_MAX==4294967295

#if ULONG_MAX==4294967295l
	typedef unsigned long uint32;
#else // ULONG_MAX==4294967295l

#error Could not typedef TCommonDataHeader::uint32

#endif // ULONG_MAX==4294967295l

#endif // UINT_MAX==4294967295

#endif // USHRT_MAX==4294967295

#else // !USE_ROOT && !CINT
	typedef UChar_t uint8;
	typedef ULong_t uint32;

#endif // USE_ROOT
	typedef volatile uint32 volatile_uint32;


	static const unsigned gkCommonDataHeaderVersions = 3;
	static const unsigned gkCommonDataHeaderCurrentVersion = 3;
	static const unsigned gkCommonDataHeaderBiggestWordCount = 10;
	static const unsigned gkCommonDataHeaderWordCounts[];

	static unsigned GetHeaderWordCountFromVersion( unsigned version )
		{
		if ( version>gkCommonDataHeaderVersions )
		    return ~(unsigned)0;
		return gkCommonDataHeaderWordCounts[version];
		}
	static unsigned GetCurrentHeaderWordCount()
		{
		return gkCommonDataHeaderWordCounts[gkCommonDataHeaderCurrentVersion];
		}
	static unsigned GetHeaderSizeFromVersion( unsigned version )
		{
		if ( version>gkCommonDataHeaderVersions )
		    return ~(unsigned)0;
		return gkCommonDataHeaderWordCounts[version]*sizeof(uint32);
		}
	static unsigned GetCurrentHeaderSize()
		{
		return gkCommonDataHeaderWordCounts[gkCommonDataHeaderCurrentVersion]*sizeof(uint32);
		}
		

	TCommonDataHeader( uint32* cdhData )
		{
		fData = cdhData;
		for ( unsigned nn=0; nn<gkCommonDataHeaderVersions; nn++ )
		    {
		    assert( gkCommonDataHeaderWordCounts[nn]<gkCommonDataHeaderBiggestWordCount );
		    }
		}

	void Reset( unsigned version=gkCommonDataHeaderCurrentVersion )
		{
        memset( fData, 0, GetHeaderSizeFromVersion(version) );
		SetBlockLength( 0xFFFFFFFFU );
		SetHeaderVersion( version );
		}

	unsigned GetHeaderVersion() const
		{
		return (fData[1] >> 24) & 0xFF;
		}
	bool SetHeaderVersion( unsigned newVersion )
		{
		if ( newVersion>gkCommonDataHeaderVersions )
		    return false;
        fData[1] = (fData[1] & ~(0xFF << 24)) | ( (newVersion & 0xFF) << 24 );
        
		return true;
		}

	unsigned GetHeaderWordCount() const
		{
		if ( GetHeaderVersion()>gkCommonDataHeaderVersions )
		    return ~(unsigned)0;
		return gkCommonDataHeaderWordCounts[GetHeaderVersion()];
		}
	unsigned GetHeaderSize() const
		{
		if ( GetHeaderVersion()>gkCommonDataHeaderVersions )
		    return ~(unsigned)0;
		return gkCommonDataHeaderWordCounts[GetHeaderVersion()]*sizeof(uint32);
		}

	unsigned GetBlockLength() const
		{
		return fData[0];
		}
	bool SetBlockLength( unsigned newLength )
		{
		if ( newLength>0xFFFFFFFFU )
		    return false;
		fData[0] = newLength;
		return true;
		}

	unsigned GetFormatVersion() const
		{
		return GetHeaderVersion();
		}
	bool SetFormatVersion( unsigned newVersion )
		{
		return SetHeaderVersion( newVersion );
		}
	

	unsigned GetL1TriggerType() const
		{
		return (fData[1] >>14) & 0xFF;
		}
	bool SetL1TriggerType( unsigned newTriggerType)
		{
		if ( newTriggerType>0xFF )
		    return false;
		fData[1] = (fData[1] & ~(0xFF << 14)) | ( (newTriggerType & 0xFF) << 14 );
		return true;
		}
	
	unsigned GetEventID1() const
		{
		return fData[1] & 0xFFF;
		}
	bool SetEventID1( unsigned newEventID1 )
		{
		if ( newEventID1>0xFFF )
		    return false;
		fData[1] = (fData[1] & ~(0xFFF)) | (newEventID1 & 0xFFF);
		return true;
		}

	unsigned GetEventID2() const
		{
		return fData[2] & 0xFFFFFF;
		}
	bool SetEventID2( unsigned newEventID2 )
		{
		if ( newEventID2>0xFFFFFF )
		    return false;
		fData[2] = (fData[2] & ~(0xFFFFFF)) | (newEventID2 & 0xFFFFFF);
		return true;
		}
	
	unsigned long long GetEventID() const
		{
		return ( ((unsigned long long)GetEventID2()) << 12 ) | ((unsigned long long)GetEventID1());
		}
	bool SetEventID( unsigned long long newEventID, bool includeMiniEventID=false )
		{
		if (newEventID>0xFFFFFFFFFULL )
		    return false;
		SetEventID1( newEventID & 0xFFF );
		SetEventID2( (newEventID >> 12) & 0xFFFFFF );
		if ( includeMiniEventID )
		    SetMiniEventID( newEventID & 0xFFF );
		return true;
		}

      unsigned GetPARrequests() const {
	return ( GetHeaderVersion() < 3 ? 0 : (fData[2] >> 24) & 0xFF);
      }
      bool SetPARrequests(unsigned newPAR) {
	if ( GetHeaderVersion() < 3 || newPAR>0xFF )
	  return false;
	fData[2] = (fData[2] & ~(0xFF << 24)) | ( (newPAR & 0xFF) << 24 );
        return true;
      }

	unsigned GetParticipatingSubDetectors() const
		{
		return fData[3] & 0xFFFFFF;
		}
	bool SetParticipatingSubDetectors( unsigned newPartSubDet )
		{
		if ( newPartSubDet>0xFFFFFF )
		    return false;
		fData[3] = (fData[3] & ~(0xFFFFFF)) | (newPartSubDet & 0xFFFFFF);
		return true;
		}

	unsigned GetBlockAttributes() const
		{
		return (fData[3] >> 24) & 0xFF;
		}
	bool SetBlockAttributes( unsigned newAttrs )
		{
		if ( newAttrs>0xFF )
		    return false;
		fData[3] = (fData[3] & ~(0xFF << 24)) | ( (newAttrs & 0xFF) << 24 );
		return true;
		}

	unsigned GetMiniEventID() const
		{
		return fData[4] & 0xFFF;
		}
	bool SetMiniEventID( unsigned newMiniEventID )
		{
		if ( newMiniEventID>0xFFF )
		    return false;
		fData[4] = (fData[4] & ~(0xFFF)) | (newMiniEventID & 0xFFF);
		return true;
		}

	unsigned GetStatusErrorBits() const
		{
		  unsigned int mask = (GetHeaderVersion() < 3 ? 0xFFFF : 0xFFFFF);
		return (fData[4] >> 12) & mask;
		}
	bool SetStatusErrorBits( unsigned newBits )
		{
		  unsigned int mask = (GetHeaderVersion() < 3 ? 0xFFFF : 0xFFFFF);
		if ( newBits>mask )
		    return false;
		fData[4] = (fData[4] & ~(mask << 12)) | ( (newBits & mask) << 12);
		return true;
		}

	unsigned GetTriggerClassesLow() const
		{
		return fData[5];
		}
	bool SetTriggerClassesLow( unsigned newTriggerClasses )
		{
		if ( newTriggerClasses>0xFFFFFFFFU )
		    return false;
		fData[5] = newTriggerClasses;
		return true;
		}

	unsigned GetTriggerClassesMidLow() const
		{
                if (GetHeaderVersion() < 3) {
		    return 0;
                } else {
                    return fData[6];
                }
		}
	bool SetTriggerClassesMidLow( unsigned newTriggerClasses )
		{
                if (GetHeaderVersion() < 3) {
                    return false;
                }
		if ( newTriggerClasses>0xFFFFFFFFU )
		    return false;
		fData[6] = newTriggerClasses;
		return true;
		}

	unsigned GetTriggerClassesMidHigh() const
		{
                if (GetHeaderVersion() < 3) {
		    return 0;
                } else {
                    return fData[7];
                }
		}
	bool SetTriggerClassesMidHigh( unsigned newTriggerClasses )
		{
                if (GetHeaderVersion() < 3) {
                    return false;
                }
		if ( newTriggerClasses>0xFFFFFFFFU )
		    return false;
		fData[7] = newTriggerClasses;
		return true;
		}

	unsigned GetTriggerClassesHigh() const
		{
                if (GetHeaderVersion() < 3) {
		    return fData[6] & 0x3FFFF;
                } else {
                    return fData[8] & 0x0000000F;
                }
		}
	bool SetTriggerClassesHigh( unsigned newTriggerClasses )
            {
                if (GetHeaderVersion() < 3) {
                    if ( newTriggerClasses>0x3FFFF )
                        return false;
                    fData[6] = (fData[6] & ~0x3FFFF) | (newTriggerClasses & 0x3FFFF);
                    return true;
                } else {
                    if (newTriggerClasses > 0x0000000F) {
                        return false;
                    }
                    fData[8] = (fData[8] & 0xFFFFFFF0) | (newTriggerClasses & 0x0000000F);
                    return true;
                }
		}

	unsigned GetRoILow() const
		{
		  unsigned word=(GetHeaderVersion() < 3 ? 6 : 8);
		return (fData[word] >> 28) & 0xF;
		}
	bool SetRoILow( unsigned newRoI )
		{
		  unsigned word=(GetHeaderVersion() < 3 ? 6 : 8);
		if ( newRoI >0xF )
		    return false;
		fData[word] = (fData[word] & ~(0xF << 28)) | ( (newRoI & 0xF) << 28 );
		return true;
		}

	unsigned GetRoIHigh() const
		{
		  unsigned word=(GetHeaderVersion() < 3 ? 7 : 9);
		return fData[word];
		}
	bool SetRoIHigh( unsigned newRoI )
		{
		  unsigned word=(GetHeaderVersion() < 3 ? 7 : 9);
		if ( newRoI>0xFFFFFFFFU )
		    return false;
		fData[word] = newRoI;
		return true;
		}

	unsigned long long GetRoI() const
		{
		return ( ((unsigned long long)GetRoIHigh()) << 4 ) | ((unsigned long long)GetRoILow());
		}
	bool SetRoI( unsigned long long newRoI )
		{
		if ( newRoI>0xFFFFFFFFFULL )
		    return false;
		SetRoIHigh( (unsigned)(newRoI >> 4) & 0xFFFFFFFFULL );
		SetRoILow( (unsigned)( newRoI & 0xF ) );
		return true;
		}

	TCommonDataHeader& operator |=( const TCommonDataHeader& op )
		{
		SetBlockLength( 0xFFFFFFFF ); // Means unspecified
		SetL1TriggerType( GetL1TriggerType() | op.GetL1TriggerType() );
		SetParticipatingSubDetectors( GetParticipatingSubDetectors() | op.GetParticipatingSubDetectors() );
		SetBlockAttributes( GetBlockAttributes() | op.GetBlockAttributes() );
		SetStatusErrorBits( GetStatusErrorBits() | op.GetStatusErrorBits() );
                SetTriggerClassesLow( GetTriggerClassesLow() | op.GetTriggerClassesLow() );
                SetTriggerClassesMidLow( GetTriggerClassesMidLow() | op.GetTriggerClassesMidLow() );
                SetTriggerClassesMidHigh( GetTriggerClassesMidHigh() | op.GetTriggerClassesMidHigh() );
                SetTriggerClassesHigh( GetTriggerClassesHigh() | op.GetTriggerClassesHigh() );
		SetRoI( GetRoI() | op.GetRoI() );
		return *this;
		}


	unsigned GetMBZ0() const
		{
		return (fData[1] >> 12) & 0x3;
		}
	unsigned GetMBZ1() const
		{
		return (fData[1] >> 22) & 0x3;
		}
	unsigned GetMBZ2() const
		{
		  return ( GetHeaderVersion() < 3 ? ((fData[2] >> 24) & 0xFF) : 0 );
		}
	unsigned GetMBZ3() const
		{
		  return ( GetHeaderVersion() < 3 ? ((fData[4] >> 28) & 0xF) : 0 );
		}
	unsigned GetMBZ4() const
		{
		  return ( GetHeaderVersion() < 3 ? ((fData[6] >> 18) & 0x3FF) : 0);
		}
	unsigned GetMBZ5() const
		{
		  return ( GetHeaderVersion() < 3 ? 0 : ((fData[8] >> 4) & 0x00FFFFFF));
		}


    protected:
	uint32* fData;

    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _COMMONDATAHEADER_HPP_
