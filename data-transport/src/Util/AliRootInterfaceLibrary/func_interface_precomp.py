#!/usr/bin/env python

import sys, os, re, string

#infiles = []
#if len(sys.argv)<=1:
#    infiles.append(sys.stdin)
#else:
#    for inf in sys.argv[1:]:
#        if inf=="-":
#            infiles.append(sys.stdin)
#        try:
#            infiles.append( open( inf, "r" ) )
#        except IOError,e:
#            print "Error opening input file '"+inf+"': "+str(e)
#            sys.exit(1)

if len(sys.argv)<=1 or sys.argv[1]=="-":
    infile = sys.stdin
else:
    try:
        infile = open( sys.argv[1], "r" )
    except IOError,e:
        print "Error opening input file '"+sys.argv[1]+"': "+str(e)
        sys.exit(1)

if len(sys.argv)<=2 or sys.argv[2]=="-":
    outfile = sys.stdout
else:
    try:
        outfile = open( sys.argv[2], "w" )
    except IOError,e:
        print "Error opening output file '"+sys.argv[2]+"': "+str(e)
        sys.exit(1)

func_pat = re.compile( "^([^\(]*) ([_a-zA-Z][_a-zA-Z0-9]*)(\(.*)(#.*)?$" )

outfile.write( "typedef void (*TVoidVoidDummyFunc)();\n" )
line=infile.readline()
while line!="":
    if line[0]=="#":
        line=infile.readline()
        continue
    if line[-1]=="\n":
        line=line[:-1]
    match = func_pat.search( line )
    if match != None:
        #print "Match:",line
        #print match.group(1)
        #print match.group(2)
        #print match.group(3)
        outfile.write(  "#ifdef __cplusplus\n" )
        outfile.write(  "extern \"C\" {\n" )
        outfile.write(  "#endif /* __cplusplus */\n" )
        outfile.write(  "typedef "+match.group(1)+" (*T"+match.group(2)+"Func)"+match.group(3)+";\n" )
        outfile.write(  "#ifdef __cplusplus\n" )
        outfile.write(  "} /* extern \"C\" */\n" )
        outfile.write(  "#endif /* __cplusplus */\n" )
        outfile.write(  "inline T"+match.group(2)+"Func Load"+match.group(2)+"Func( TVoidVoidDummyFunc (*loadFunc)(const char*) )\n" )
        outfile.write(  "    {\n" )
        outfile.write(  "    return reinterpret_cast<T"+match.group(2)+"Func>( loadFunc( \""+line+"\" ) );\n" )
        outfile.write(  "    }\n" )
        outfile.write( "\n" )
        outfile.write( "\n" )
    line=infile.readline()
