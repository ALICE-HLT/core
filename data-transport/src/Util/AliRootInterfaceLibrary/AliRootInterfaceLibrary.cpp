/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#if 1
#define DUMMY_INTERFACE_OUTPUT
#endif

#include "AliRootInterfaceLibrary.hpp"
#include "AliHLTDataTypes.h"
#include "MLUCLog.hpp"
#include "MLUCCmdlineParser.hpp"
#include <cerrno>
#include <csignal>
#include <cstdlib>
#include <vector>
#ifdef DUMMY_INTERFACE_OUTPUT
#include <iostream>
#endif

static char dummyid[] = { 'D','U','M','Y','D','A','T','A' };
static char dummyorigin[] = { 'D','U','M','Y' };

AliRootInterface::TDummyInterface::TDummyInterface( bool produceEDD ):
    fProduceEDD(produceEDD)
    {
    memset( &fEnv, 0, sizeof(fEnv) );
    }

void AliRootInterface::TDummyInterface::SetEnvironment( AliHLTAnalysisEnvironment* env )
    {
    memcpy( &fEnv, env, env->fStructSize );
    }


int AliRootInterface::TDummyInterface::GetOutputDataSize( unsigned long& constEventBase, unsigned long& constBlockBase, double& inputBlocksizeMultiplier )
    {
    constEventBase = 0; constBlockBase = 0; inputBlocksizeMultiplier = 1.0;
    return 0;
    }
int AliRootInterface::TDummyInterface::GetOutputDataType( AliHLTComponentDataType& dataType )
    {
    if ( dataType.fStructSize != sizeof(AliHLTComponentDataType) )
        return ENXIO;
    memcpy( dataType.fID, dummyid, kAliHLTComponentDataTypefIDsize );
    memcpy( dataType.fOrigin, dummyorigin, kAliHLTComponentDataTypefOriginSize );
    return 0;
    }
int AliRootInterface::TDummyInterface::ProcessEvent( const AliHLTComponentEventData* eventData, const AliHLTComponentBlockData* blockData, AliHLTComponentTriggerData* triggerData, AliHLTUInt8_t* outputPtr, AliHLTUInt32_t& size, AliHLTUInt32_t& outputBlockCnt, AliHLTComponentBlockData*& outputBlocks, AliHLTComponentEventDoneData*& edd )
    {
    if ( !fEnv.fAllocMemoryFunc )
        return ENXIO;
    unsigned long blockCnt=0;
    for ( unsigned nn=0; nn<eventData->fBlockCnt; nn++ )
        {
	if ( blockData[nn].fSize>0 )
	    ++blockCnt;
	}
    outputBlocks = reinterpret_cast<AliHLTComponentBlockData*>( fEnv.fAllocMemoryFunc( fEnv.fParam, sizeof(AliHLTComponentBlockData)*blockCnt ) );
    memset( outputBlocks, 0, sizeof(AliHLTComponentBlockData)*blockCnt );
    unsigned long long offset = 0;
    const char privOrigin[] = {'P','R','I','V'};
    const char sorDataType[] = kAliHLTSORDataTypeID; //      {'S','T','A','R','T','O','F','R'}
    const char eorDataType[] = kAliHLTEORDataTypeID; //      {'E','N','D','O','F','R','U','N'}
    const char runtypeDataType[] = kAliHLTRunTypeDataTypeID; //  {'R','U','N','T','Y','P','E',' '}
    const char eventtypDataType[] = kAliHLTEventDataTypeID; //    {'E','V','E','N','T','T','Y','P'}
    const char ecsparamDataType[] = kAliHLTECSParamDataTypeID; // {'E','C','S','P','A','R','A','M'}

    for ( unsigned long nn=0, dest=0; nn<eventData->fBlockCnt; nn++ )
        {
#ifdef DUMMY_INTERFACE_OUTPUT
	std::cout << "Input Block " << nn << ":" <<  std::endl;
	std::cout << "  Size: " << blockData[nn].fSize << std::endl;
	std::cout << "  Offset: " << blockData[nn].fOffset << std::endl;
	std::cout << "  Data Type: ";
	for ( unsigned mm=0; mm<8; ++mm )
	    {
	    std::cout << ( blockData[nn].fDataType.fID[mm]!='\0' ? blockData[nn].fDataType.fID[mm] : ' ' );
	    }
	std::cout << std::endl;
	std::cout << "  Data Origin: ";
	for ( unsigned mm=0; mm<4; ++mm )
	    {
	    std::cout << ( blockData[nn].fDataType.fOrigin[mm]!='\0' ? blockData[nn].fDataType.fOrigin[mm] : ' ' );
	    }
	std::cout << std::endl;
	std::cout << "  Data Spec.: 0x" << std::hex << blockData[nn].fSpecification << std::dec << std::endl;
#endif
	if ( blockData[nn].fSize<=0 )
	    continue;
        memcpy( outputPtr+offset, blockData[nn].fPtr, blockData[nn].fSize );
        outputBlocks[dest].fStructSize = sizeof(outputBlocks[nn]);
        outputBlocks[dest].fOffset = offset;
	outputBlocks[dest].fShmKey.fStructSize = sizeof(outputBlocks[dest].fShmKey);
	outputBlocks[dest].fShmKey.fShmType = 0;
	outputBlocks[dest].fShmKey.fShmID = 0;
        outputBlocks[dest].fSize = blockData[nn].fSize;
	if ( !strncmp( blockData[nn].fDataType.fOrigin, privOrigin, kAliHLTComponentDataTypefOriginSize ) && 
	     ( !strncmp( blockData[nn].fDataType.fID, sorDataType, kAliHLTComponentDataTypefIDsize ) ||
	       !strncmp( blockData[nn].fDataType.fID, eorDataType, kAliHLTComponentDataTypefIDsize ) ||
	       !strncmp( blockData[nn].fDataType.fID, runtypeDataType, kAliHLTComponentDataTypefIDsize ) ||
	       !strncmp( blockData[nn].fDataType.fID, eventtypDataType, kAliHLTComponentDataTypefIDsize ) ||
	       !strncmp( blockData[nn].fDataType.fID, ecsparamDataType, kAliHLTComponentDataTypefIDsize ) )
	   )
	    {
	    memcpy( outputBlocks[dest].fDataType.fID, blockData[nn].fDataType.fID, kAliHLTComponentDataTypefIDsize );
	    memcpy( outputBlocks[dest].fDataType.fOrigin, blockData[nn].fDataType.fOrigin, kAliHLTComponentDataTypefOriginSize );
	    outputBlocks[dest].fSpecification = 0;
	    }
	else
	    {
	    memcpy( outputBlocks[dest].fDataType.fID, dummyid, kAliHLTComponentDataTypefIDsize );
	    memcpy( outputBlocks[dest].fDataType.fOrigin, dummyorigin, kAliHLTComponentDataTypefOriginSize );
	    outputBlocks[dest].fSpecification = 0;
	    }
        offset += blockData[dest].fSize;
	++dest;
        }
    size = offset;
    outputBlockCnt = blockCnt;
    if ( fProduceEDD )
	{
#if 1
	edd = NULL;
	int ret = fEnv.fGetEventDoneDataFunc( fEnv.fParam, eventData->fEventID, triggerData->fDataSize, &edd );
	if ( !ret && edd && edd->fData )
	    {
	    memcpy( edd->fData, triggerData->fData, triggerData->fDataSize );
	    edd->fDataSize = triggerData->fDataSize;
	    }
	else if ( edd )
	    edd->fDataSize = 0;
#else
	edd = reinterpret_cast<AliHLTComponentEventDoneData*>( fEnv.fAllocMemoryFunc( fEnv.fParam, sizeof(AliHLTComponentEventDoneData) ) );
	edd->fStructSize = sizeof(AliHLTComponentEventDoneData);
	edd->fData = reinterpret_cast<AliHLTComponentEventDoneData*>( fEnv.fAllocMemoryFunc( fEnv.fParam, triggerData->fDataSize ) );
	if ( edd->fData )
	    {
	    memcpy( edd->fData, triggerData->fData, triggerData->fDataSize );
	    edd->fDataSize = triggerData->fDataSize;
	    }
	else
	    edd->fDataSize = 0;
#endif
	}
    else
	edd = NULL;
    return 0;
    }


/**
 * Initialize AliRoot interface library
*/
AliRootInterface::AliRootInterface():
    /* Initialize old compatibility interface */
    fAliHLT_C_Component_InitSystem( NULL ),
    fAliHLT_C_Component_DeinitSystem( NULL ),
    fAliHLT_C_Component_LoadLibrary( NULL ),
    fAliHLT_C_Component_UnloadLibrary( NULL ),
    fAliHLT_C_CreateComponent( NULL ),
    fAliHLT_C_DestroyComponent( NULL ),
    fAliHLT_C_ProcessEvent( NULL ),
    fAliHLT_C_GetOutputDataType( NULL ),
    fAliHLT_C_GetOutputSize( NULL ),
    fSetRunDescription( NULL ),
    /* Initialize new/current interface */
    fAliHLTAnalysisGetInterfaceCall( NULL ),
    fAliHLTAnalysisInitSystemFunc( NULL ),
    fAliHLTAnalysisLoadLibraryFunc( NULL ),
    fAliHLTAnalysisCreateComponentFunc( NULL ),
    fAliHLTAnalysisGetOutputSizeFunc( NULL ),
    fAliHLTAnalysisGetOutputDataTypeFunc( NULL ),
    fAliHLTAnalysisDestroyComponentFunc( NULL ),
    fAliHLTAnalysisUnloadLibraryFunc( NULL ),
    fAliHLTAnalysisDeinitSystemFunc( NULL ),
    fAliHLTAnalysisProcessEventFunc( NULL ),
    fCallbackParameter( NULL ),
    fUseDynamicInterface( true ),
    fLibrarySet( false ),
    fLibraryLoaded( false ),
    fBeamType( 0 ),
    fHLTMode( 0 ),
    fRunNumber( 0 ),
    fComponentIDSet( false ),
    fCompHandle( kEmptyHLTComponentHandle ),
    fDummyInterface(NULL),
    fConstructorOk(true)
    {
    /* Initialize old compatibility interface */
    fEnvironment.fStructSize = sizeof(fEnvironment);
    fEnvironment.fAllocMemoryFunc = NULL;
    fEnvironment.fGetEventDoneDataFunc = NULL;
    fEnvironment.fLoggingFunc = NULL;


    /* Initialize new/current interface */
    fAnalysisEnvironment.fStructSize = sizeof(fAnalysisEnvironment);
    fAnalysisEnvironment.fAllocMemoryFunc = NULL;
    fAnalysisEnvironment.fGetEventDoneDataFunc = NULL;
    fAnalysisEnvironment.fLoggingFunc = NULL;

    if ( getenv( "ALIHLT_AUTOLOAD_INTERFACE_DEFAULT_LIBRARIES" ) != NULL )
	{
	LOG( MLUCLog::kImportant, "AliRootInterface::AliRootInterface", "Auto pre-loading shared libraries" )
	    << "Automatically pre-loading pre-defined set of default shared ROOT/AliRoot libraries (triggered by defined 'ALIHLT_AUTOLOAD_INTERFACE_DEFAULT_LIBRARIES' environment variable)."
	    << ENDLOG;
	fAddShLibNames.push_back( "libCint.so" );
	fAddShLibNames.push_back( "libCore.so" );
	fAddShLibNames.push_back( "libRIO.so" );
	fAddShLibNames.push_back( "libMathCore.so" );
	fAddShLibNames.push_back( "libNet.so" );
	fAddShLibNames.push_back( "libTree.so" );
	fAddShLibNames.push_back( "libGeom.so" );
	fAddShLibNames.push_back( "libEG.so" );
	fAddShLibNames.push_back( "libVMC.so" );
	fAddShLibNames.push_back( "libMatrix.so" );
	fAddShLibNames.push_back( "libHist.so" );
	fAddShLibNames.push_back( "libGraf.so" );
	fAddShLibNames.push_back( "libGpad.so" );
	fAddShLibNames.push_back( "libGraf3d.so" );
	fAddShLibNames.push_back( "libProof.so" );
	fAddShLibNames.push_back( "libEGPythia6.so" );
	fAddShLibNames.push_back( "libGui.so" );
	
	fAddShLibNames.push_back( "libSTEERBase.so" );
	fAddShLibNames.push_back( "libESD.so" );
	fAddShLibNames.push_back( "libRAWDatabase.so" );
	fAddShLibNames.push_back( "libRAWDatarec.so" );
	fAddShLibNames.push_back( "libCDB.so" );
	fAddShLibNames.push_back( "libMinuit.so" );
	fAddShLibNames.push_back( "libSTEER.so" );
	fAddShLibNames.push_back( "libPhysics.so" );
	fAddShLibNames.push_back( "libEVGEN.so" );
	fAddShLibNames.push_back( "libpythia6.so" );
	fAddShLibNames.push_back( "libFASTSIM.so" );
	fAddShLibNames.push_back( "libAliPythia6.so" );
	fAddShLibNames.push_back( "libHLTbase.so" );
	}
    if ( getenv( "ALIHLT_AUTOLOAD_INTERFACE_LIBRARIES" ) != NULL )
	{
	LOG( MLUCLog::kImportant, "AliRootInterface::AliRootInterface", "Pre-loading shared libraries" )
	    << "Pre-loading set of shared ROOT/AliRoot libraries defined in 'ALIHLT_AUTOLOAD_INTERFACE_LIBRARIES' environment variable."
	    << ENDLOG;
	MLUCString libs_str( getenv( "ALIHLT_AUTOLOAD_INTERFACE_LIBRARIES" ) );
	vector<MLUCString> lib_list;
	libs_str.Split( lib_list, ':' );
	vector<MLUCString>::iterator iter, end;
	iter = lib_list.begin();
	end = lib_list.end();
	while ( iter != end )
	    {
	    if ( !iter->IsEmpty() && !iter->IsWhiteSpace() )
		{
		vector<MLUCString> lib_before;
		iter->Split( lib_before, ',' );
		if ( lib_before.size()>2 )
		    {
		    LOG( MLUCLog::kError, "AliRootInterface::AliRootInterface", "Error parsing ALIHLT_AUTOLOAD_INTERFACE_LIBRARIES" )
			<< "Error parsing 'ALIHLT_AUTOLOAD_INTERFACE_LIBRARIES' environment variable; allowed syntax '<append-library>;<append-library>;<insert-library>:<before-library>;...' ."
			<< ENDLOG;
		    fConstructorOk = false;
		    break;
		    }
		if ( lib_before.size()==2 )
		    AddLibrary( lib_before[0].c_str(), lib_before[1].c_str() );
		else
		    AddLibrary( iter->c_str() );
		}
	    ++iter;
	    }
	}

    }

/**
 * Cleanup AliRoot interface library
 */
AliRootInterface::~AliRootInterface()
    {
    }

/**
 * Setup environment data from which AliRoot interface is called
 */
int AliRootInterface::SetEnvironment( void* callbackParameter, 
				      void* (*allocMemoryFunc)( void* param, unsigned long size ), 
				      int (*getEventDoneDataFunc)( void* param, AliHLTEventID_t eventID, unsigned long size, AliHLTComponentEventDoneData** edd ),
				      AliHLTfctLogging dlLoggingFunc )
    {
    fCallbackParameter = callbackParameter;
    if ( allocMemoryFunc )
	{
	fEnvironment.fAllocMemoryFunc = allocMemoryFunc;
	fAnalysisEnvironment.fAllocMemoryFunc = allocMemoryFunc;
	}
    if ( getEventDoneDataFunc )
	{
	fEnvironment.fGetEventDoneDataFunc = getEventDoneDataFunc;
	fAnalysisEnvironment.fGetEventDoneDataFunc = getEventDoneDataFunc;
	}
    if ( dlLoggingFunc )
	{
	fEnvironment.fLoggingFunc = dlLoggingFunc;
	fAnalysisEnvironment.fLoggingFunc = dlLoggingFunc;
	}
    if ( fDummyInterface )
	fDummyInterface->SetEnvironment( &fAnalysisEnvironment );
    return 0;
    }


/**
 * Add an additional library
 */
int AliRootInterface::AddLibrary( const char* libraryName, const char* before )
    {
    if ( before )
	{
	std::vector<MLUCString>::iterator iter, end;
	iter = fAddShLibNames.begin();
	end = fAddShLibNames.end();
	while ( iter != end )
	    {
	    if ( *iter == MLUCString(before) )
		{
		fAddShLibNames.insert( iter, MLUCString(before) );
		return 0;
		}
	    ++iter;
	    }
	}
    fAddShLibNames.push_back( libraryName );
    return 0;
    }

/**
 * Specify the library from which to load the component
 * Optional, has to be speicified if library is not contained in default set of libraries to be loaded
 */
int AliRootInterface::SetLibrary( const char* libraryName )
    {
    fLibrarySet = true;
    fLibraryName = libraryName;
    return 0;
    }

/**
 * Set the component to be used with its arguments.
 */
int AliRootInterface::SetComponent( const char* componentID, const char* componentArgs, const char* componentInfo )
    {
    fComponentID = componentID;
    fComponentIDSet = true;
    fComponentInfo = componentInfo;
    if ( !MLUCCmdlineParser::ParseCmd( componentArgs, fCompOptions ) )
	{
	return EINVAL;
	}
    return 0;
    }




/**
 * Load old compatibility interface
 * Internal function, not exported
 */
bool AliRootInterface::LoadCompatibilityInterface()
    {
    int ret;
    char const*const libraryName = "libHLTbase.so";
    ret = fOldInterfaceLibrary.Open( libraryName );
    if ( ret )
	{
	LOG( MLUCLog::kError, "AliRootInterface::LoadCompatibilityInterface", "Unable to load compatibility interface library" )
	    << "Unable to load compatibility interface shared library "
	     << libraryName << ": " << strerror(ret) << " (" << MLUCLog::kDec << ret << ") - "
	    << fOldInterfaceLibrary.GetLastError() << "." << ENDLOG;
	return false;
	}

    const char* funcName;
    funcName = "AliHLT_C_Component_InitSystem";
    ret = GetDLSym<TAliHLT_C_Component_InitSystemFunc>( fOldInterfaceLibrary, funcName, fAliHLT_C_Component_InitSystem );
    if ( ret )
	{
	LOG( MLUCLog::kError, "AliRootInterface::LoadCompatibilityInterface", "Unable to load compatibility interface function" )
	    << "Unable to load compatibility interface function " << funcName << " from shared library "
	     << libraryName << ": " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	return false;
	}
    funcName = "AliHLT_C_Component_DeinitSystem";
    ret = GetDLSym<TAliHLT_C_Component_DeinitSystemFunc>( fOldInterfaceLibrary, funcName, fAliHLT_C_Component_DeinitSystem );
    if ( ret )
	{
	LOG( MLUCLog::kError, "AliRootInterface::LoadCompatibilityInterface", "Unable to load compatibility interface function" )
	    << "Unable to load compatibility interface function " << funcName << " from shared library "
	     << libraryName << ": " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	return false;
	}
    funcName = "AliHLT_C_Component_LoadLibrary";
    ret = GetDLSym<TAliHLT_C_Component_LoadLibraryFunc>( fOldInterfaceLibrary, funcName, fAliHLT_C_Component_LoadLibrary );
    if ( ret )
	{
	LOG( MLUCLog::kError, "AliRootInterface::LoadCompatibilityInterface", "Unable to load compatibility interface function" )
	    << "Unable to load compatibility interface function " << funcName << " from shared library "
	     << libraryName << ": " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	return false;
	}
    funcName = "AliHLT_C_Component_UnloadLibrary";
    ret = GetDLSym<TAliHLT_C_Component_UnloadLibraryFunc>( fOldInterfaceLibrary, funcName, fAliHLT_C_Component_UnloadLibrary );
    if ( ret )
	{
	LOG( MLUCLog::kError, "AliRootInterface::LoadCompatibilityInterface", "Unable to load compatibility interface function" )
	    << "Unable to load compatibility interface function " << funcName << " from shared library "
	     << libraryName << ": " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	return false;
	}
    funcName = "AliHLT_C_CreateComponent";
    ret = GetDLSym<TAliHLT_C_CreateComponentFunc>( fOldInterfaceLibrary, funcName, fAliHLT_C_CreateComponent );
    if ( ret )
	{
	LOG( MLUCLog::kError, "AliRootInterface::LoadCompatibilityInterface", "Unable to load compatibility interface function" )
	    << "Unable to load compatibility interface function " << funcName << " from shared library "
	     << libraryName << ": " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	return false;
	}
    funcName = "AliHLT_C_DestroyComponent";
    ret = GetDLSym<TAliHLT_C_DestroyComponentFunc>( fOldInterfaceLibrary, funcName, fAliHLT_C_DestroyComponent );
    if ( ret )
	{
	LOG( MLUCLog::kError, "AliRootInterface::LoadCompatibilityInterface", "Unable to load compatibility interface function" )
	    << "Unable to load compatibility interface function " << funcName << " from shared library "
	     << libraryName << ": " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	return false;
	}
    funcName = "AliHLT_C_ProcessEvent";
    ret = GetDLSym<TAliHLT_C_ProcessEventFunc>( fOldInterfaceLibrary, funcName, fAliHLT_C_ProcessEvent );
    if ( ret )
	{
	LOG( MLUCLog::kError, "AliRootInterface::LoadCompatibilityInterface", "Unable to load compatibility interface function" )
	    << "Unable to load compatibility interface function " << funcName << " from shared library "
	     << libraryName << ": " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	return false;
	}
    funcName = "AliHLT_C_GetOutputDataType";
    ret = GetDLSym<TAliHLT_C_GetOutputDataTypeFunc>( fOldInterfaceLibrary, funcName, fAliHLT_C_GetOutputDataType );
    if ( ret )
	{
	LOG( MLUCLog::kError, "AliRootInterface::LoadCompatibilityInterface", "Unable to load compatibility interface function" )
	    << "Unable to load compatibility interface function " << funcName << " from shared library "
	     << libraryName << ": " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	return false;
	}
    funcName = "AliHLT_C_GetOutputSize";
    ret = GetDLSym<TAliHLT_C_GetOutputSizeFunc>( fOldInterfaceLibrary, funcName, fAliHLT_C_GetOutputSize );
    if ( ret )
	{
	LOG( MLUCLog::kError, "AliRootInterface::LoadCompatibilityInterface", "Unable to load compatibility interface function" )
	    << "Unable to load compatibility interface function " << funcName << " from shared library "
	     << libraryName << ": " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	return false;
	}
    funcName = "AliHLT_C_SetRunDescription";
    ret = GetDLSym<TSetRunDescriptionFunc>( fOldInterfaceLibrary, funcName, fSetRunDescription );
    if ( ret )
	{
	fSetRunDescription = NULL;
	LOG( MLUCLog::kInformational, "AliRootInterface::LoadCompatibilityInterface", "Cannot find run descriptor function" )
	    << "Cannot find run descriptor function (AliHLT_C_SetRunDescription) in HLT base shared library. Continuing without this functionality."
	    << ENDLOG;
	}
    return true;
    }


/**
 * Load AliRoot interface
 */
int AliRootInterface::Load()
    {
    if ( fDummyInterface )
	return 0;
    if ( !fConstructorOk )
	return ENXIO;
    int ret;
    // Load additional shared libraries; do this first, as some of these libraries might be needed for the interface library/-ies
    vector<MLUCString>::iterator iter, end;
    iter = fAddShLibNames.begin();
    end = fAddShLibNames.end();
    while ( iter != end  )
	{
	MLUCDynamicLibrary* dl = new MLUCDynamicLibrary;
	ret = dl->Open( iter->c_str() );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "AliRootInterface::Load", "Init Component Handler" )
		<< "Error loading additional shared library '" << iter->c_str() << "': " << strerror(ret) << " (" << MLUCLog::kDec
		<< ret << ") - " << dl->GetLastError() << " ." << ENDLOG;
	    dl->Close();
	    delete dl;
	    return EIO;
	    }
	fAddShLibs.insert( fAddShLibs.begin(), dl ); // Insert at beginning, ensures that we can release from the beginning without violating dependencies later
	++iter;
	}

    // Load the new/current dynamic interface
    if ( fUseDynamicInterface )
	{
	ret = fDynamicInterface.Open( "libHLTinterface.so" );
	if ( ret )
	    {
	    LOG( MLUCLog::kImportant, "AliRootInterface::Load", "Init Component Handler" )
		<< "Unable to load dynamic library for new dynamic interface to AliRoot. Using old compatibility interface... - error message: "
		<< fDynamicInterface.GetLastError() << " ." << ENDLOG;
	    fUseDynamicInterface = false;
	    }
	}
    if ( fUseDynamicInterface )
        {
	ret = GetDLSym<TAliHLTAnalysisGetInterfaceCall>( fDynamicInterface, "AliHLTAnalysisGetInterfaceCall", fAliHLTAnalysisGetInterfaceCall );
	if ( ret )
	    {
	    LOG( MLUCLog::kImportant, "AliRootInterface::Load", "Interface function load" )
	      << "Unable to load dynamic library interface loader function 'AliHLTAnalysisGetInterfaceCall'. Using old compatibility interface..." << ENDLOG;
	    fUseDynamicInterface = false;
	    }
	}
    if ( !fUseDynamicInterface )
	{
	// If the new dynamic interface could not be loaded try the old compatibility interface as a fallback (might be old version of AliRoot not yet supporting the new version
	if ( !LoadCompatibilityInterface() )
	    {
	    LOG( MLUCLog::kError, "AliRootInterface::Load", "Cannot load compatibility interface" )
	      << "Unable to load compatibility interface. No component interface available. Aborting..." << ENDLOG;
	    return EIO;
	    }
	}

    // Load the different functions from the new dynamic library interface
    if ( fUseDynamicInterface )
        {
	fAliHLTAnalysisInitSystemFunc = LoadAliHLTAnalysisInitSystemFunc( fAliHLTAnalysisGetInterfaceCall );
	if ( !fAliHLTAnalysisInitSystemFunc )
	    {
	    LOG( MLUCLog::kError, "AliRootInterface::Load", "Interface function load" )
	      << "Unable to load dynamic library interface AliHLTAnalysisInitSystem function." << ENDLOG;
	    return EIO;
	    }
	fAliHLTAnalysisLoadLibraryFunc = LoadAliHLTAnalysisLoadLibraryFunc( fAliHLTAnalysisGetInterfaceCall );
	if ( !fAliHLTAnalysisLoadLibraryFunc )
	    {
	    LOG( MLUCLog::kError, "AliRootInterface::Load", "Interface function load" )
	      << "Unable to load dynamic library interface AliHLTAnalysisLoadLibrary function." << ENDLOG;
	    return EIO;
	    }
	fAliHLTAnalysisCreateComponentFunc = LoadAliHLTAnalysisCreateComponentFunc( fAliHLTAnalysisGetInterfaceCall );
	if ( !fAliHLTAnalysisCreateComponentFunc )
	    {
	    LOG( MLUCLog::kError, "AliRootInterface::Load", "Interface function load" )
	      << "Unable to load dynamic library interface AliHLTAnalysisCreateComponent function." << ENDLOG;
	    return EIO;
	    }
	fAliHLTAnalysisGetOutputSizeFunc = LoadAliHLTAnalysisGetOutputSizeFunc( fAliHLTAnalysisGetInterfaceCall );
	if ( !fAliHLTAnalysisGetOutputSizeFunc )
	    {
	    LOG( MLUCLog::kError, "AliRootInterface::Load", "Interface function load" )
	      << "Unable to load dynamic library interface AliHLTAnalysisGetOutputSize function." << ENDLOG;
	    return EIO;
	    }
	fAliHLTAnalysisGetOutputDataTypeFunc = LoadAliHLTAnalysisGetOutputDataTypeFunc( fAliHLTAnalysisGetInterfaceCall );
	if ( !fAliHLTAnalysisGetOutputDataTypeFunc )
	    {
	    LOG( MLUCLog::kError, "AliRootInterface::Load", "Interface function load" )
	      << "Unable to load dynamic library interface AliHLTAnalysisGetOutputDataType function." << ENDLOG;
	    return EIO;
	    }
	fAliHLTAnalysisProcessEventFunc = LoadAliHLTAnalysisProcessEventFunc( fAliHLTAnalysisGetInterfaceCall );
	if ( !fAliHLTAnalysisProcessEventFunc )
	    {
	    LOG( MLUCLog::kError, "AliRootInterface::Load", "Interface function load" )
	      << "Unable to load dynamic library interface AliHLTAnalysisProcessEvent function." << ENDLOG;
	    return EIO;
	    }
	fAliHLTAnalysisDestroyComponentFunc = LoadAliHLTAnalysisDestroyComponentFunc( fAliHLTAnalysisGetInterfaceCall );
	if ( !fAliHLTAnalysisDestroyComponentFunc )
	    {
	    LOG( MLUCLog::kError, "AliRootInterface::Load", "Interface function load" )
	      << "Unable to load dynamic library interface AliHLTAnalysisDestroyComponent function." << ENDLOG;
	    return EIO;
	    }
	fAliHLTAnalysisUnloadLibraryFunc = LoadAliHLTAnalysisUnloadLibraryFunc( fAliHLTAnalysisGetInterfaceCall );
	if ( !fAliHLTAnalysisUnloadLibraryFunc )
	    {
	    LOG( MLUCLog::kError, "AliRootInterface::Load", "Interface function load" )
	      << "Unable to load dynamic library interface AliHLTAnalysisUnloadLibrary function." << ENDLOG;
	    return EIO;
	    }
	fAliHLTAnalysisDeinitSystemFunc = LoadAliHLTAnalysisDeinitSystemFunc( fAliHLTAnalysisGetInterfaceCall );
	if ( !fAliHLTAnalysisDeinitSystemFunc )
	    {
	    LOG( MLUCLog::kError, "AliRootInterface::Load", "Interface function load" )
	      << "Unable to load dynamic library interface AliHLTAnalysisDeinitSystem function." << ENDLOG;
	    return EIO;
	    }
        }

    // Call the init function for the AliRoot side of things
    if ( fUseDynamicInterface )
        {
	ret = fAliHLTAnalysisInitSystemFunc( ALIHLT_DATA_TYPES_VERSION, &fAnalysisEnvironment, fRunNumber, fRunType.c_str() );
	}
    else
        {
	ret = fAliHLT_C_Component_InitSystem( &fEnvironment );
	}
    if ( ret )
	{
	LOG( MLUCLog::kError, "AliRootInterface::Load", "Init Component Handler" )
	    << "Error initializing C component handler interface: " << strerror(ret) << " (" << MLUCLog::kDec
	    << ret << ")." << ENDLOG;
	return EIO;
	}

    if ( fLibrarySet )
	{
	if ( fUseDynamicInterface )
	    {
	    ret = fAliHLTAnalysisLoadLibraryFunc( fLibraryName.c_str() );
	    }
	else
	    {
	    ret = fAliHLT_C_Component_LoadLibrary( fLibraryName.c_str() );
	    }
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "AliRootInterface::Load", "Load Component Library" )
		<< "Error loading additional component library '" << fLibraryName.c_str() << "': " 
		<< strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	    return false;
	    }
	fLibraryLoaded = true;
	}

    if ( !fUseDynamicInterface && fSetRunDescription )
	{
	AliHLTRunDesc runDescr;
	runDescr.fStructSize = sizeof(AliHLTRunDesc);
	runDescr.fRunNo = fRunNumber;
	runDescr.fRunType = fBeamType | (fHLTMode << 3);
	// fRunType
	fSetRunDescription( &runDescr, fRunType.c_str() );
	}

    int argc = fCompOptions.size();
    const char** argv = new const char*[ argc+1 ];
    if ( !argv )
	{
	LOG( MLUCLog::kError, "AliRootInterface::Load", "Out of memory" )
	    << "Out of memory allocating char* array of " << MLUCLog::kDec << argc << " members."
	    << ENDLOG;
	return EIO;
	}
    for ( int i = 0; i < argc; i++ )
	argv[i] = fCompOptions[i].c_str();
    argv[argc] = NULL;
    if ( fUseDynamicInterface )
        {
	ret = fAliHLTAnalysisCreateComponentFunc( fComponentID.c_str(), fCallbackParameter, argc, argv, &fCompHandle, fComponentInfo.c_str() );
	}
    else
        ret = fAliHLT_C_CreateComponent( fComponentID.c_str(), fCallbackParameter, argc, argv, &fCompHandle );
    if ( ret )
	{
	LOG( MLUCLog::kError, "AliRootInterface::Load", "Error creating AliRoot component" )
	    << "Error creating AliRoot component of type '" << fComponentID.c_str()
	    << "': " << strerror(ret) << " (" << MLUCLog::kDec << ret << ")."
	    << ENDLOG;
	fCompHandle = kEmptyHLTComponentHandle;
	return EIO;
	}
    delete [] argv;
    // Reset signal handler for SIGSEGV to default to get core files instead of only root stack traces...
    signal(SIGSEGV, SIG_DFL);
    return 0;
    }


int AliRootInterface::Unload()
    {
    if ( fDummyInterface )
	return 0;
    // Cleanup in reverse order as done while loading

    if ( fCompHandle != kEmptyHLTComponentHandle )
	{
	if ( fUseDynamicInterface && fAliHLTAnalysisDestroyComponentFunc )
	    fAliHLTAnalysisDestroyComponentFunc( fCompHandle );
	else if ( fAliHLT_C_DestroyComponent )
	    fAliHLT_C_DestroyComponent( fCompHandle );
	fCompHandle = kEmptyHLTComponentHandle;
	}
    if ( fLibraryLoaded )
	{
	if ( fUseDynamicInterface && fAliHLTAnalysisUnloadLibraryFunc )
	    fAliHLTAnalysisUnloadLibraryFunc( fLibraryName.c_str() );
	else if ( fAliHLT_C_Component_UnloadLibrary )
	    fAliHLT_C_Component_UnloadLibrary( fLibraryName.c_str() );
	fLibraryLoaded = false;
	}

    if ( fUseDynamicInterface && fAliHLTAnalysisDeinitSystemFunc )
	{
        fAliHLTAnalysisDeinitSystemFunc();
	fAliHLTAnalysisDeinitSystemFunc = NULL;
	}
    else if ( fAliHLT_C_Component_DeinitSystem )
	{
        fAliHLT_C_Component_DeinitSystem();
	fAliHLT_C_Component_DeinitSystem = NULL;
	}


#if 0
    fDynamicInterface.Close();

    while ( fAddShLibs.size()>0 )
	{
	(*fAddShLibs.begin())->Close();
	delete (*fAddShLibs.begin());
	fAddShLibs.erase( fAddShLibs.begin() );
	}
#else
#warning Unloading of libraries disabled to avoid crashes for now.
    // Maybe some proper sequence of libraries has to be found.
#endif
    return 0;
    }


int AliRootInterface::GetOutputDataSize( unsigned long& constEventBase, unsigned long& constBlockBase, double& inputBlocksizeMultiplier )
    {
    if ( fDummyInterface )
	{
	return fDummyInterface->GetOutputDataSize( constEventBase, constBlockBase, inputBlocksizeMultiplier );
	}
    if ( fCompHandle == kEmptyHLTComponentHandle )
	return ENODEV;
    int ret;
    if ( fUseDynamicInterface )
	ret = fAliHLTAnalysisGetOutputSizeFunc( fCompHandle, &constEventBase, &constBlockBase, &inputBlocksizeMultiplier );
    else
	{
	constEventBase = 0;
	ret = fAliHLT_C_GetOutputSize( fCompHandle, &constBlockBase, &inputBlocksizeMultiplier );
	}
    return ret;
    }

/**
 * Get component's output data type
 */
int AliRootInterface::GetOutputDataType( AliHLTComponentDataType& dataType )
    {
    if ( fDummyInterface )
	{
	return fDummyInterface->GetOutputDataType( dataType );
	}
    if ( fCompHandle == kEmptyHLTComponentHandle )
	return ENODEV;
    int ret;
    dataType.fStructSize = sizeof(dataType);
    if ( fUseDynamicInterface )
	ret = fAliHLTAnalysisGetOutputDataTypeFunc( fCompHandle, &dataType );
    else
	ret = fAliHLT_C_GetOutputDataType( fCompHandle, &dataType );
    return ret;
    }


/**
 * Process an event
 */
int AliRootInterface::ProcessEvent( const AliHLTComponentEventData* eventData, const AliHLTComponentBlockData* blockData, AliHLTComponentTriggerData* triggerData, AliHLTUInt8_t* outputPtr, AliHLTUInt32_t& size, AliHLTUInt32_t& outputBlockCnt, AliHLTComponentBlockData*& outputBlocks, AliHLTComponentEventDoneData*& edd )
    {
    if ( fDummyInterface )
	{
	return fDummyInterface->ProcessEvent( eventData, blockData, triggerData, outputPtr, size, outputBlockCnt, outputBlocks, edd );
	}
    if ( fCompHandle == kEmptyHLTComponentHandle )
	return ENODEV;
    int ret;
    if ( fUseDynamicInterface )
        ret = fAliHLTAnalysisProcessEventFunc( fCompHandle, eventData, blockData, 
					  triggerData, outputPtr,
					  &size, &outputBlockCnt, 
					  &outputBlocks,
					  &edd );
    else
        ret = fAliHLT_C_ProcessEvent( fCompHandle, eventData, blockData, 
				      triggerData, outputPtr,
				      &size, &outputBlockCnt, 
				      &outputBlocks,
				      &edd );
    return ret;
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

