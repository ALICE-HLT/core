#ifndef __ALIROOTINTERFACELIBRARY_HPP__
#define __ALIROOTINTERFACELIBRARY_HPP__
/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTDataTypes.h"
#include "AliHLTFunctionInterface.h"
#include "MLUCDynamicLibrary.hpp"


class AliRootInterface
    {
    public:

        /**
         * Dummy class for standalone test running without real AliRoot
         */
        class TDummyInterface
            {
            public:
                /**
                 * Default constructor
                 */
                TDummyInterface( bool produceEDD=false );
                /**
                 * Set the callback environment
                 */
                virtual void SetEnvironment( AliHLTAnalysisEnvironment* env );
                /**
                 * Return the output data size characteristics of the dummy component
                 */
                virtual int GetOutputDataSize( unsigned long& constEventBase, unsigned long& constBlockBase, double& inputBlocksizeMultiplier );
                /**
                 * Return the output data type of the dummy component
                 */
                virtual int GetOutputDataType( AliHLTComponentDataType& dataType );
                /**
                 * Perform the dummy event processing
                 */
                virtual int ProcessEvent( const AliHLTComponentEventData* eventData, const AliHLTComponentBlockData* blockData, AliHLTComponentTriggerData* triggerData, AliHLTUInt8_t* outputPtr, AliHLTUInt32_t& size, AliHLTUInt32_t& outputBlockCnt, AliHLTComponentBlockData*& outputBlocks, AliHLTComponentEventDoneData*& edd );
            protected:
                AliHLTAnalysisEnvironment fEnv;
		bool fProduceEDD;
            };

	/**
	 * Initialize AliRoot interface library
	 */
	AliRootInterface();

	/**
	 * Cleanup AliRoot interface library
	 */
	~AliRootInterface();

	/**
	 * Set the dummy interface for test usage without a full AliRoot
	 */
	void SetDummyInterface( TDummyInterface* dummyInterface )
		{
		fDummyInterface = dummyInterface;
		}
    
	/**
	 * Setup data for environment from which AliRoot interface is called
	 */
	int SetEnvironment( void* callbackParameter, 
			    void* (*allocMemoryFunc)( void* param, unsigned long size ), 
			    int (*getEventDoneDataFunc)( void* param, AliHLTEventID_t eventID, unsigned long size, AliHLTComponentEventDoneData** edd ),
			    AliHLTfctLogging dlLoggingFunc );

	/**
	 * Add an additional library
	 */
	int AddLibrary( const char* libraryName, const char* before=NULL );

	/**
	 * Specify the library from which to load the component
	 * Optional, has to be speicified if library is not contained in default set of libraries to be loaded
	 */
	int SetLibrary( const char* libraryName );

	/**
	 * Set the component to be used with its arguments.
	 */
	int SetComponent( const char* componentID, const char* componentArgs, const char* componentInfo );
    
	/**
	 * Set the run information
	 */
	int SetRunInformation( unsigned long beamType, unsigned long hltMode,  unsigned long runNumber, const char* runType )
		{
		fBeamType = beamType;
		fHLTMode = hltMode;
		fRunNumber = runNumber;
		fRunType = runType;
		return 0;
		}

	/**
	 * Load the AliRoot interface
	 */
	int Load();

	/**
	 * Unload the AliRoot interface
	 */
	int Unload();

	/**
	 * Get numbers for predictions of an event's output data size.
	 */
	int GetOutputDataSize( unsigned long& constEventBase, unsigned long& constBlockBase, double& inputBlocksizeMultiplier );

	/**
	 * Get component's output data type
	 */
	int GetOutputDataType( AliHLTComponentDataType& dataType );

	/**
	 * Process an event
	 */
	int ProcessEvent( const AliHLTComponentEventData* eventData, const AliHLTComponentBlockData* blockData, AliHLTComponentTriggerData* triggerData, AliHLTUInt8_t* outputPtr, AliHLTUInt32_t& size, AliHLTUInt32_t& outputBlockCnt, AliHLTComponentBlockData*& outputBlocks, AliHLTComponentEventDoneData*& edd );

    protected:


	/* Function typedefs for old compatibiltiy interface */
	typedef int (*TAliHLT_C_Component_InitSystemFunc)( AliHLTComponentEnvironment* environ );
	typedef int (*TAliHLT_C_Component_DeinitSystemFunc)();
	typedef int (*TAliHLT_C_Component_LoadLibraryFunc)( const char* libraryPath );
	typedef int (*TAliHLT_C_Component_UnloadLibraryFunc)( const char* libraryPath );
	typedef int (*TAliHLT_C_CreateComponentFunc)( const char* componentType, void* environ_param, int argc, const char** argv, AliHLTComponentHandle* handle );
	typedef void (*TAliHLT_C_DestroyComponentFunc)( AliHLTComponentHandle );
	typedef int (*TAliHLT_C_ProcessEventFunc)( AliHLTComponentHandle handle, const AliHLTComponentEventData* evtData, const AliHLTComponentBlockData* blocks, 
						   AliHLTComponentTriggerData* trigData, AliHLTUInt8_t* outputPtr,
						   AliHLTUInt32_t* size, AliHLTUInt32_t* outputBlockCnt, 
						   AliHLTComponentBlockData** outputBlocks,
						   AliHLTComponentEventDoneData** edd );
	typedef int (*TAliHLT_C_GetOutputDataTypeFunc)( AliHLTComponentHandle, AliHLTComponentDataType* dataType );
	typedef int (*TAliHLT_C_GetOutputSizeFunc)( AliHLTComponentHandle, unsigned long* constBase, double* inputMultiplier );
	typedef int (*TSetRunDescriptionFunc)(const AliHLTRunDesc* desc, const char* runType);
	
	
	/* Function typedef for new/current interface */
	typedef TVoidVoidDummyFunc (*TAliHLTAnalysisGetInterfaceCall)( const char* signature );

	bool LoadCompatibilityInterface();
	

	/* Member variables for old compatibility interface */
	/* Function pointers */
	TAliHLT_C_Component_InitSystemFunc fAliHLT_C_Component_InitSystem;
	TAliHLT_C_Component_DeinitSystemFunc fAliHLT_C_Component_DeinitSystem;
	TAliHLT_C_Component_LoadLibraryFunc fAliHLT_C_Component_LoadLibrary;
	TAliHLT_C_Component_UnloadLibraryFunc fAliHLT_C_Component_UnloadLibrary;
	TAliHLT_C_CreateComponentFunc fAliHLT_C_CreateComponent;
	TAliHLT_C_DestroyComponentFunc fAliHLT_C_DestroyComponent;
	TAliHLT_C_ProcessEventFunc fAliHLT_C_ProcessEvent;
	TAliHLT_C_GetOutputDataTypeFunc fAliHLT_C_GetOutputDataType;
	TAliHLT_C_GetOutputSizeFunc fAliHLT_C_GetOutputSize;
	TSetRunDescriptionFunc fSetRunDescription;

	MLUCDynamicLibrary fOldInterfaceLibrary;
	AliHLTComponentEnvironment fEnvironment;


	/* Member variables for new/current interface */
	/* Get interface function pointer */
	TAliHLTAnalysisGetInterfaceCall fAliHLTAnalysisGetInterfaceCall;
	/* Actual "do-work" function pointers */
        TAliHLTAnalysisInitSystemFunc fAliHLTAnalysisInitSystemFunc;
        TAliHLTAnalysisLoadLibraryFunc fAliHLTAnalysisLoadLibraryFunc;
        TAliHLTAnalysisCreateComponentFunc fAliHLTAnalysisCreateComponentFunc;
        TAliHLTAnalysisGetOutputSizeFunc fAliHLTAnalysisGetOutputSizeFunc;
        TAliHLTAnalysisGetOutputDataTypeFunc fAliHLTAnalysisGetOutputDataTypeFunc;
        TAliHLTAnalysisDestroyComponentFunc fAliHLTAnalysisDestroyComponentFunc;
        TAliHLTAnalysisUnloadLibraryFunc fAliHLTAnalysisUnloadLibraryFunc;
        TAliHLTAnalysisDeinitSystemFunc fAliHLTAnalysisDeinitSystemFunc;
        TAliHLTAnalysisProcessEventFunc fAliHLTAnalysisProcessEventFunc;

	AliHLTAnalysisEnvironment fAnalysisEnvironment;

	/**
	 * Callback parameter for environment setup
	 */
	void* fCallbackParameter;

	/* Shared libraries that have to be loaded in addition to the base library */
	std::vector<MLUCString> fAddShLibNames;
	std::vector<MLUCDynamicLibrary*> fAddShLibs;

	MLUCDynamicLibrary fDynamicInterface;
        bool fUseDynamicInterface;

	bool fLibrarySet;
	MLUCString fLibraryName;
	bool fLibraryLoaded;

	unsigned long fBeamType;
	unsigned long fHLTMode;
	unsigned long fRunNumber;
	MLUCString fRunType;

	MLUCString fComponentID;
	bool fComponentIDSet;
	MLUCString fComponentInfo;
	vector<MLUCString> fCompOptions;
	AliHLTComponentHandle fCompHandle;


	TDummyInterface* fDummyInterface;

	bool fConstructorOk;


    private:
	AliRootInterface( const AliRootInterface& ) {}
	AliRootInterface& operator=( const AliRootInterface& ) { return *this; }
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif /* __ALIROOTINTERFACELIBRARY_HPP__ */
