#ifndef ALICE_HLT_PENDOLINO_SLAVE_LIB_WRAPPER_HPP
#define ALICE_HLT_PENDOLINO_SLAVE_LIB_WRAPPER_HPP

/************************************************************************
 **
 **
 ** This file is property of and copyright by the Department of Physics
 ** Institute for Physic and Technology, University of Bergen,
 ** Bergen, Norway, 2007
 ** This file has been written by Sebastian Bablok,
 ** sebastian.bablok@ift.uib.no
 **
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 *************************************************************************/

#include <string>


namespace alice { namespace hlt { namespace pendolino {
		
/**
 * This simple class wrappes the calls to the SlaveControlLib of the HLT
 * TaskManger system and sends notification about updated HCDBs to the 
 * TaskManger.
 *
 * @author Sebastian Bablok <Sebastian.Bablok@ift.uib.no>
 *
 * @date 2007-10-01
 */
class SlaveLibWrapper {
	public:
		/**
		 * Constructor for the SlaveLibWrapper.
		 *
		 * @param listenAddr the listen address of the local SlaveControl Lib.
		 * @param tmAddr the address of the connectd TaskManager
		 * @param command the command string that notifies the TaskManager 
		 * 			about the update of the HCDB. (Default is "HCDBupdated")
		 */
		SlaveLibWrapper(const char* listenAddr, const char* tmAddr, 
				const char* command = "trigger_reconfigure_event temporary");

		/**
		 * Destructor for the SlaveLibWrapper.
		 */
		virtual ~SlaveLibWrapper();

		/**
		 * Function to request if SlaveLibWrapper is initialized.
		 *
		 * @return true if initialized, else false
		 */
		bool isInit();

		/**
		 * Function to request if SlaveLibWrapper is connected to TM
		 * 
		 * @return true if connected, else false
		 */
        bool isConnected();

		/**
		 * Function to signal an HCDB update to the TaskManager.
		 * If no command string is provided, the last / initial one is used
		 *
		 * @param command the command string that shall be sent as signal
		 *
		 * @return 0 on success, else an error code is returned
		 */
		int signalHCDBupdated(const char* command = 0);

		/**
		 * Function to initialize the SlaveControlLib and the SlaveLibWrapper.
		 *
		 * @return 0 on success, else an error code
		 */
		int init();

		/**
		 * Function to connect to a given TaskManager
		 *
		 * @return 0 on success, else an error code
		 */
		int connect();

		/**
		 * Function to terminate the SlaveControl Lib.
		 * The SlaveLibWrapper is afterwards deinitialized.
		 */
		void deinit();

	private:
		/**
		 * Stores the listen address of the local SlaveControl Lib
		 */
		std::string mListenAddress;

		/**
		 * Stores the Address of the connectd TaskManager
		 */
		std::string mTMAddress;

		/**
		 * Stores the command string
		 */
		std::string mCmd;

		/**
		 * Stores init value
		 */
		bool mInit;

		/**
		 * Inidicates if SlaveLibWrapper is connected to TaskManager
		 */
		bool mConnected;

		/** 
		 * This pointer points to the intial data, produced by the InterfacLib
		 * during its Initialize(...) call.
		 */
		void* mpInterfaceLib_data;

		/**
		 * This pointer contains the binary version of the TaskManager
		 * address. (The convertion is done by the InterfaceLib).
		 */
		void* mpTMaddress_bin;
		
}; // end of class

} } } // end of namespaces


#endif // ALICE_HLT_PENDOLINO_SLAVE_LIB_WRAPPER_HPP

