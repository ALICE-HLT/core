/************************************************************************
 **
 **
 ** This file is property of and copyright by the Department of Physics
 ** Institute for Physic and Technology, University of Bergen,
 ** Bergen, Norway, 2007
 ** This file has been written by Sebastian Bablok,
 ** sebastian.bablok@ift.uib.no
 **
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 *************************************************************************/

#include "SlaveLibWrapper.hpp"

#include "InterfaceLib.h"
#include <iostream>


using namespace std;

using namespace alice::hlt::pendolino;


SlaveLibWrapper::SlaveLibWrapper(const char* listenAddr, const char* tmAddr,
				const char* command) {
	mListenAddress = listenAddr;
	mTMAddress = tmAddr;
	mInit = false;
	mConnected = false;
	mCmd = command;
	mpInterfaceLib_data = 0;
	mpTMaddress_bin = 0;
}

SlaveLibWrapper::~SlaveLibWrapper() {

}

int SlaveLibWrapper::init() {
	int nRet = 0;
	
	if (mInit) {
		return nRet;
	}
	
	nRet = Initialize(&mpInterfaceLib_data, mListenAddress.c_str());

	if (nRet != 0) {
		cout << "   *** Init of SlaveControl Lib failed ('" << nRet << "')." 
				<< endl;
		mpInterfaceLib_data = 0;
		return nRet;
	}
	mInit = true;

	nRet = ConvertAddress(mpInterfaceLib_data, mTMAddress.c_str(), 
			&mpTMaddress_bin);
	if (nRet != 0) {
		cout << "   *** Convertion of TaskManger address failed ('" << nRet << "')."
				<< endl;
		mpTMaddress_bin = 0;
		deinit();
		return nRet;
	}

	nRet = connect();
	
	return nRet;
}


void SlaveLibWrapper::deinit() {
	int nRet = 0;
	
	if (!mInit) {
		return;
	}

	// disconnect from Process if connect
	if (mConnected) {
		nRet = DisconnectFromProcess(mpInterfaceLib_data, 0, 0, mpTMaddress_bin);	
		if (nRet != 0) {
			cout << "   *** Disconnecting from TaskManager ('" << mTMAddress << 
					"') failed ('" << nRet << "')." << endl;
		}
		mConnected = false;
	}
	
	// release resources acquired by lib
	if (mpTMaddress_bin != 0) {
		ReleaseResource(mpInterfaceLib_data, mpTMaddress_bin);
	}
	
	// terminate lib
	nRet = Terminate(mpInterfaceLib_data);
	if (nRet != 0) {
		cout << "   *** Terminating of SlaveControl Lib failed ('" << nRet << "')."
				<< endl;
	}
	mpInterfaceLib_data = 0;
	mpTMaddress_bin = 0;
	mInit = false;
}


bool SlaveLibWrapper::isInit() {
	return mInit;
}

bool SlaveLibWrapper::isConnected() {
	return mConnected;
}

int SlaveLibWrapper::connect() {
	int nRet = 0;
	
	nRet = ConnectToProcess(mpInterfaceLib_data, 0, 0, mpTMaddress_bin);
    if (nRet != 0) {
		cout << "   *** Connecting to TaskManger ('" << mTMAddress <<
				"') failed ('" << nRet << "')." << endl;
		return nRet;
	} 
	mConnected = true;
				
	return nRet;
}

int SlaveLibWrapper::signalHCDBupdated(const char* command) {
	int nRet = 0;
	
	if (command != 0) {
		mCmd = command;
	}

	if (!mInit) {
		cout << "   *** Unable to notify TaskManager, SlaveLibWrapper not initialized."
				<< endl;
		return -1;
	}

	nRet = SendCommand(mpInterfaceLib_data, 0, 0, mpTMaddress_bin, mCmd.c_str());
	if (nRet != 0) {	
		cout << "   *** Notifying TaskManager about updated HCDB failed ('" 
				<< nRet << "')." << endl;
	}
	return nRet;
}

