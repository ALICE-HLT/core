
/************************************************************************
 **
 **
 ** This file is property of and copyright by the Department of Physics
 ** Institute for Physic and Technology, University of Bergen,
 ** Bergen, Norway, 2007
 ** This file has been written by Sebastian Bablok,
 ** sebastian.bablok@ift.uib.no
 **
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 *************************************************************************/

#include <signal.h>
#include <errno.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "SlaveLibWrapper.hpp"


using namespace std;

using namespace alice::hlt::pendolino;

//Function declaration
/**
 * Function to be called, when the TaskManager shall be notified about
 * an update in the HCDB.
 *
 * @return true, when successful, else false
 */
bool notifyTM();

/**
 * Handler function that will catch signal to inform Master TMs.
 *
 * @param sig the signal caught by the handler function.
 */
void notifyHandler(int sig);

/**
 * Handler function to perform a proper exit. This also include terminating
 * the SlaveControlLib.
 *
 * @param sig the signal caught by the handler function.
 */
void cleanUp(int sig);

/**
 * Function to print out usage.
 */
void printUsage();


// Global variables
SlaveLibWrapper* relayStation = 0;
char* appName = 0;

// Main
int main(int argc, char** argv) {
	int retVal = 0;
	struct sigaction newAction;
	struct sigaction exitAction;
	char* listenAddr = 0;
	char* tmAddr = 0;
	char* command = 0;
	char* direct = 0;

	appName = argv[0];

	if ((argc < 4) || (argc > 5)) {
		cout << "   *** Argument(s) missing. See usage:" << endl;
		printUsage(); 
		exit(17);
	}

	listenAddr = argv[1];
	tmAddr = argv[2];
	command = argv[3];

	if (argc == 5) {
		direct = argv[4];
		if (strcmp(direct, "--direct") != 0) {
			cout << "   *** Argument number 4 is unknown, see usage:" << endl;
			printUsage();
			exit(16);
		}
	}

	// DEBUG
//	cout << " parameters:" << endl;
//	cout << " 1: " << listenAddr << endl;
//	cout << " 2: " << tmAddr << endl;
//	cout << " 3: " << command << endl;
//	cout << " 4: " << direct << endl;

	// defining handler
	newAction.sa_handler = notifyHandler;
	exitAction.sa_handler = cleanUp;
	relayStation = new SlaveLibWrapper(listenAddr, tmAddr, command);

	if (direct != 0) {
		if (notifyTM()) {
			exit(0);
		} else {
			exit(3);
		}
		// leave program, if '--direct'' as parameter is provided
	}

	retVal = relayStation->init();

	if (retVal != 0) {
		cout << "   *** Init of SlaveLibWrapper failed, unable to contact TaskManager."
				<< endl;
//		exit(16); // -> Don't exit, try reconnect each time a signal is caught
	}
	
    // setting mask for handler [exit]
	retVal = sigaddset(&exitAction.sa_mask, SIGINT);
	retVal += sigaddset(&exitAction.sa_mask, SIGTERM);
	retVal += sigaddset(&exitAction.sa_mask, SIGQUIT);
	retVal += sigaddset(&exitAction.sa_mask, SIGUSR1);
    if (retVal != 0) {
		cout << "   ~~~ Setting mask for signal handler failed. Trying wihtout ..."
				<< endl;
	}
	 
	// setting signal handler [exit]
    retVal = sigaction(SIGINT, &exitAction, NULL);
    retVal += sigaction(SIGQUIT, &exitAction, NULL);
    retVal += sigaction(SIGTERM, &exitAction, NULL);
	retVal += sigaction(SIGUSR1, &exitAction, NULL);
    if (retVal != 0) {
		cout << "   ~~~ Setting exit handler failed. Trying wihtout ..."
				<< endl;
	}
	
	// setting mask for handler [notifier]
	retVal = sigaddset(&newAction.sa_mask, SIGUSR2);
	if (retVal != 0) {
		cout << "   ~~~ Setting mask for signal handler failed. Trying wihtout ..." 
				<< endl;
    }

	// setting signal handler [notifier]
	retVal = sigaction(SIGUSR2, &newAction, NULL);
	if (retVal != 0) {
		cout << "   *** Registering signal handler for TaskManager notifier failed ('"
				<< errno << "')!" << endl;
		cout << "   *** Pendolino is unable to notify TMs, exiting '" << 
				argv[0] << "'." << endl;
		exit(19);	
	}

	// wait for ever
	while (true) {
		sleep(1);
	}
	
	
	exit(0);
}


// Function definition
bool notifyTM() {
	if (relayStation == 0) {
		cout << "   *** TM-SlaveControl lib wrapper not existing, exiting '" <<
				appName << "'." << endl << endl;
		exit(18);
	}
	if ((!(relayStation->isInit())) && (relayStation->init() != 0)) {
		cout << "   *** Init of TM-SlaveControl lib failed again, unable to notify TM."
				<< endl;
		return false; 	// unable to send notification, trying on next signal again
	}

	if ((!(relayStation->isConnected())) && (relayStation->connect() != 0)) {
		cout << "   *** Connect to TaskManager failed again, unable to notify TM."
				<< endl << endl;
		return false;     // unable to send notification, trying on next signal again
	}
	
	cout << "   --- Notifying TM of HCDB update." << endl;
	
	// send command to TM to notify about new HCDB entries
//	if (relayStation->signalHCDBupdated("trigger_reconfigure_event_test temporary") != 0) {
//	if (relayStation->signalHCDBupdated("trigger_reconfigure_event temporary") != 0) {
	if (relayStation->signalHCDBupdated() != 0) {
		cout << "   *** HCDB update notification to TaskManager failed." << endl
				<< endl;
		return false;		// unable to send notification, trying on next signal again
	}
	cout << "   +++ TaskManager notified of HCDB update." << endl << endl;
	return true;
}

void notifyHandler(int /*sig*/) {
	notifyTM();
}

void cleanUp(int /*sig*/) {
	cout << "   --- Performing proper exit of " << appName << "." << endl;
	relayStation->deinit();
	exit(0);	
}


void printUsage() {
	cout << "   USAGE:" << endl;
	cout << "      " << appName <<
			" <SlaveLib_ListenAddress> <TaskManager_Address> <Command> [--direct]" << endl;
	cout << endl;
	cout << "      SlaveLib_ListenAddress: local listen address of the slave lib."
			<< endl;
	cout << "          e.g.: 'tcpmsg://portal-dcs0.internal:42024'" << endl;
	cout << endl;
	cout << "      TaskManager_Address: address of the TaskManger to notify."
			<< endl;
	cout << "          e.g.: 'tcpmsg://portal-ecs0.internal:20000'" << endl;
	cout << endl;
	cout << "      Command: command that notifies the TaskManager about the updated HCDB."
	        << endl;
	cout << "          e.g.: 'HCDBupdate' - has to be synchronized with TaskManager!" 
			<< endl;
	cout << endl;
	cout << "      --direct: (optional) If '--direct' is provided as fourth parameter,"
		   	<< endl;
	cout << "          the command is sent directly to the Master TaskManager and the"
		   	<< endl;
	cout << "          program exits afterwards immediately. If left blank the program"
			<< endl;
	cout << "          runs continuously and waits for signals 'SIGUSR2' to send command"
			<< endl;
	cout << "          to Master TaskManager." << endl;
			
}
		
