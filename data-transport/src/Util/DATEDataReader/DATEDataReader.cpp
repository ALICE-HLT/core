/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Jochen Th�der & Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "DATEDataReader.hpp"
#include "stdio.h"
#include <iostream>

DATEDataReader::DATEDataReader( std::vector<MLUCString> filenames, equipmentIdType equipmentID ):
    fFilenames( filenames ),
    fEquipmentID( equipmentID ),
    fFileIter( fFilenames.begin() ),
    fEquipmentData( 0 ),
    fEquipmentDataSize( 0 )
    {
    }

DATEDataReader::~DATEDataReader()
    {
    Reset();
    }

void DATEDataReader::Reset()
    {
    fInFile.close();
    fFileIter = fFilenames.begin();
    if ( fEquipmentData )
	{
	delete [] fEquipmentData;
	fEquipmentData = 0;
	fEquipmentDataSize = 0;
	}
    }
    
bool DATEDataReader::NextEvent()
    {
    fEquipmentsFound.clear();
    if ( fEquipmentData )
	{
	delete [] fEquipmentData;
	fEquipmentData = 0;
	fEquipmentDataSize = 0;
	}
    eventHeaderStruct evHeaderBase; 
    do
	{
	if ( fInFile.eof() )
	    {
	    fInFile.close();
	    if ( fFileIter==fFilenames.end() ) // Necessary to check for already encountered end markers before increasing iterator again.
		return false;
	    fFileIter++;
	    if ( fFileIter==fFilenames.end() )
		return false;
	    }
	if ( !fInFile.is_open() )
	    fInFile.open( fFileIter->c_str() );
	if ( !fInFile.good() )
	    return false;
	
	
	// First read event base header
	fInFile.read( reinterpret_cast<char*>( &evHeaderBase ), sizeof(evHeaderBase) );
	}
    while ( fInFile.gcount()==0 && fInFile.eof() );
    if ( !fInFile.good() )
	return false;

    unsigned int baseEventReadCount = sizeof(evHeaderBase);

    while ( baseEventReadCount < evHeaderBase.eventSize )
	{
	// Read event header, we may have multiple of these per event, maybe sub-events
	eventHeaderStruct evHeader;
	fInFile.read( reinterpret_cast<char*>( &evHeader ), sizeof(evHeader) );
	baseEventReadCount += sizeof(evHeader);
	if ( fInFile.eof() && (baseEventReadCount >= evHeaderBase.eventSize) )
	    return true;
	if ( !fInFile.good() )
	    return false;
	baseEventReadCount += (evHeader.eventSize-sizeof(evHeader));
	unsigned int eventReadCount = sizeof(evHeader);

	while ( eventReadCount < evHeader.eventSize )
	    {
	    // Read equipment header, we can have multiuple of these per sub-event
	    equipmentHeaderStruct eqHeader;
	    fInFile.read( reinterpret_cast<char*>( &eqHeader ), sizeof(eqHeader));
	    eventReadCount += sizeof(eqHeader);
	    if ( fInFile.eof() && (eventReadCount >= evHeader.eventSize) )
		return true;
	    if ( !fInFile.good() )
		return false;
	    unsigned long equipmentDataSize = eqHeader.equipmentSize-sizeof(eqHeader);
	    fEquipmentsFound.push_back( eqHeader.equipmentId );
	    if ( eqHeader.equipmentId == fEquipmentID )
		{
		fEquipmentDataSize = equipmentDataSize;
		// Read the requested equipment data.
		if ( fEquipmentData )
		    return false; // Duplicate equipment
		fEquipmentData = new uint8[ fEquipmentDataSize ];
		if ( !fEquipmentData )
		    return false;
		fInFile.read( reinterpret_cast<char*>( fEquipmentData ), fEquipmentDataSize );
		eventReadCount += fEquipmentDataSize;
		if ( fInFile.eof() && (eventReadCount >= evHeader.eventSize) )
		    return true;
		if ( !fInFile.good() )
		    return false;
		}
	    else
		{
		fInFile.seekg( equipmentDataSize, std::ios_base::cur );
		eventReadCount += equipmentDataSize;
		if ( fInFile.eof() && (eventReadCount >= evHeader.eventSize) )
		    return true;
		if ( !fInFile.good() )
		    return false;
		}
	  
	    }
	}
    return true;
    }

unsigned long DATEDataReader::GetEventSize() const
    {
    return fEquipmentDataSize;
    }

bool DATEDataReader::CopyEvent( void* dest, unsigned long size ) const
    {
    if ( !fEquipmentData )
	return false;
    if ( !dest )
	return false;
    if ( size>fEquipmentDataSize )
	return false;
    memcpy( dest, fEquipmentData, size );
    return true;
    }

bool DATEDataReader::GetFoundEquipments( std::vector<equipmentIdType>& equipments ) const
    {
    equipments.clear();
    if ( fEquipmentsFound.size()<=0 )
	return false;
    equipments = fEquipmentsFound;
    return true;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Jochen Th�der & Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
