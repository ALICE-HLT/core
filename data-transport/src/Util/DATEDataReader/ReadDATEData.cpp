#include "DATEDataReader.hpp"
#include <MLUCString.hpp>
#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>

int main( int argc, char** argv )
{
  int i = 1;
  const char* error = NULL;
  int errorArg = -1;
  int errorParam = -1;
  std::vector<MLUCString> filenames;
  equipmentIdType equipmentID=0;
  bool equipmentIDSet = false;
  unsigned long eventStart = 0;
  unsigned long eventStop = ~0UL;
  MLUCString outputPrefix( "DATEEvent" );
  bool showEquipments = false;
  while ( i< argc && !error )
    {
      if ( MLUCString( argv[i])=="-file" )
	{
	  if ( i+1>=argc )
	    {
	      error = "Missing file specifier";
	      errorArg = i;
	      break;
	    }
	  filenames.push_back( MLUCString( argv[i+1] ) );
	  i += 2;
	  continue;
	}
      if ( MLUCString( argv[i])=="-outputprefix" )
	{
	  if ( i+1>=argc )
	    {
	      error = "Missing output prefix specifier";
	      errorArg = i;
	      break;
	    }
	  outputPrefix = argv[i+1];
	  i += 2;
	  continue;
	}
      if ( MLUCString(argv[i])=="-equipmentid" )
	{
	  if ( i+1>=argc )
	    {
	      error = "Missing equipment ID specifier";
	      errorArg = i;
	      break;
	    }
	  char* cpErr=NULL;
	  equipmentID = strtoul( argv[i+1], &cpErr, 0 );
	  if ( *cpErr != '\0' )
	    {
	      error = "Error converting equipment ID specifier";
	      errorArg = i;
	      errorParam = i+1;
	      break;
	    }
	  equipmentIDSet = true;
	  i += 2;
	  continue;
	}

      if ( MLUCString(argv[i])=="-eventstart" )
	{
	  if ( i+1>=argc )
	    {
	      error = "Missing event start specifier";
	      errorArg = i;
	      break;
	    }
	  char* cpErr=NULL;
	  eventStart = strtoul( argv[i+1], &cpErr, 0 );
	  if ( *cpErr != '\0' )
	    {
	      error = "Error converting event start specifier";
	      errorArg = i;
	      errorParam = i+1;
	      break;
	    }
	  i += 2;
	  continue;
	}
      if ( MLUCString(argv[i])=="-eventstop" )
	{
	  if ( i+1>=argc )
	    {
	      error = "Missing event stop specifier";
	      errorArg = i;
	      break;
	    }
	  char* cpErr=NULL;
	  eventStop = strtoul( argv[i+1], &cpErr, 0 );
	  if ( *cpErr != '\0' )
	    {
	      error = "Error converting event stop specifier";
	      errorArg = i;
	      errorParam = i+1;
	      break;
	    }
	  i += 2;
	  continue;
	}
      if ( MLUCString(argv[i])=="-showequipments" )
	{
	  showEquipments = true;
	  i += 1;
	  continue;
	}
      error = "Unknown argument";
    }

  if ( !error )
    {
      if ( filenames.size()<=0 )
	error = "Filename has to be set using -file option";
      else if ( !equipmentIDSet && !showEquipments )
	error = "Equipment ID has to be set using -equipmentid option or -showequipments has to be selected.";
    }

  if ( error )
    {
      std::cerr << "Usage: " << argv[0] << " -file <filename> -equipmentid <equipment-id> (-eventstart <first-event>) (-eventstop <last-event>) (-outputprefix <output-filename-prefix>) (-showequipments)" << std::endl;
      std::cerr << "       " << error << std::endl;
      if ( errorArg!=-1 )
	std::cerr << "       Offending argument: " << argv[errorArg] << std::endl;
      if ( errorParam!=-1 )
	std::cerr << "       Offending parameter: " << argv[errorParam] << std::endl;
      return -1;
    }

  DATEDataReader reader( filenames, equipmentID );

  unsigned long eventCount = 0;

  while ( reader.NextEvent() && eventCount<eventStop )
    {
      if ( eventCount >= eventStart )
	{
	  if ( showEquipments )
	    {
	      std::vector<equipmentIdType> equipments;
	      reader.GetFoundEquipments( equipments );
	      std::vector<equipmentIdType>::iterator iter = equipments.begin();
	      while ( iter != equipments.end() )
		{
		  std::cout << std::dec << *iter << " / 0x" << std::hex << *iter << std::endl;
		  iter++;
		}
	    }
	  else
	    {
	      unsigned long sz = reader.GetEventSize();
	      uint8* data = new uint8[ sz ];
	      if ( !data )
		{
		  std::cerr << "Out of memory trying to allocate " << sz << " bytes." << std::endl;
		  return -1;
		}
	      if ( !reader.CopyEvent( data, sz ) )
		{
		  std::cerr << "Error reading event " << eventCount << "." << std::endl;
		}
	      else
		{
		  char fileIndex[128];
		  sprintf( fileIndex, "0x%016lX", (unsigned long)eventCount );
		  MLUCString filename = outputPrefix;
		  filename += "-Event-";
		  filename += fileIndex;
		  sprintf( fileIndex, "0x%016lX", (unsigned long)equipmentID );
		  filename += "-Equipment-";
		  filename += fileIndex;
		  filename += ".dateevent";
		  std::ofstream outfile( filename.c_str() );
		  outfile.write( (const char*)data, sz );
		}
	      delete [] data;
	      data = NULL;
	    }
	}
      eventCount++;
    }

  return 0;

}
