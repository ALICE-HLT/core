/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

#ifndef _DATEDATAREADER_HPP_
#define _DATEDATAREADER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Jochen Th�der & Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <vector>
#include <fstream>
#include <MLUCString.hpp>
#include "event.h"

class DATEDataReader
{
public:
  DATEDataReader( std::vector<MLUCString> filenames, equipmentIdType equipmentID );
    ~DATEDataReader();

    void Reset();
    
  bool NextEvent();
  unsigned long GetEventSize() const;
  bool CopyEvent( void* dest, unsigned long size ) const;

  bool GetFoundEquipments( std::vector<equipmentIdType>& equipments ) const;

protected:

  std::vector<MLUCString> fFilenames;
  equipmentIdType fEquipmentID; 

  std::ifstream fInFile;

  std::vector<MLUCString>::iterator fFileIter;

  uint8* fEquipmentData;
  unsigned long fEquipmentDataSize;

  std::vector<equipmentIdType> fEquipmentsFound;
#if 0
  File fFp;
  unsigned int fBaseEventSizeCount;
  unsigned int fEventSizeCount;
  unsigned int fEquipmentSizeCount;
#endif
};


/*
***************************************************************************
**
** $Author$ - Initial Version by Jochen Th�der & Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
#endif // _DATEDATAREADER_HPP_
