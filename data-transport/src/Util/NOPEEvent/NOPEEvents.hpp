#ifndef _NOPEEVENT_HPP_
#define _NOPEEVENT_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <cerrno>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#if ULONG_MAX==18446744073709551615UL
typedef unsigned long TNopeEventUInt64_t;
#else // ULONG_MAX==18446744073709551615UL

#if defined __GNUC__
typedef unsigned long long TNopeEventUInt64_t;
#else // defined __GNUC__

#error Could not typedef TNopeEventUInt64_t

#endif // defined __GNUC__

#endif // ULONG_MAX==18446744073709551615UL


const TNopeEventUInt64_t gNOPEEventIdentifier = 0x0408015016023042;

class TNOPEEvent
    {
    public:

	static int IsNopeEvent( const char* filename, bool& isNopeEvent )
		{
		isNopeEvent = false;
		int fh;
		fh = open( filename, O_RDONLY );
		if ( fh==-1 )
		    return errno;
		off_t insize;
		insize = lseek( fh, 0, SEEK_END );
		if ( insize==(off_t)-1 )
		    {
		    close( fh );
		    return errno;
		    }
		lseek( fh, 0, SEEK_SET );
		if ( insize==sizeof(gNOPEEventIdentifier) )
		    {
		    TNopeEventUInt64_t fileContent;
		    int ret = read( fh, &fileContent, sizeof(fileContent) );
		    if ( ret != sizeof(fileContent) )
			{
			if ( errno )
			    ret = errno;
			else
			    ret = EIO;
			close( fh );
			return ret;
			}
		    if ( fileContent==gNOPEEventIdentifier )
			isNopeEvent = true;
		    }
		close( fh );
		return 0;
		}
	
    protected:
    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _NOPEEVENT_HPP_
