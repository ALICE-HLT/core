/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "NOPEEvents.hpp"
#include "AliHLTReadoutList.hpp"
#include <fstream>
#include <iostream>
#include <istream>
#include <ios>
#include <iomanip>
#include <vector>
#include <cstring>
#include <cerrno>
#include <climits>
#include <string>

using namespace std;

int main( int, char** )
    {
    for ( TDetectorID id=kFIRSTID; id<kENDID; id=(TDetectorID)((unsigned)id+1) )
	{
	unsigned long ddlCnt = AliHLTReadoutList::GetReadoutListDetectorDDLCount( gkReadoutListVersions, id );
	const char* detName = AliHLTReadoutList::GetDetectorName( id );
	string filename;
	if ( id==kHLT || string(detName)=="UNUSED" )
	    continue;
	cout << detName << endl;
	for ( unsigned long ddl=(id << gkDDLIDDetectorLShift); ddl<((id <<gkDDLIDDetectorLShift)+ddlCnt); ++ddl )
	    {
	    filename = detName;
	    char tmp[128];
	    sprintf( tmp, "_%u.ddl", (unsigned )ddl );
	    filename += tmp;
	    // cout << filename << endl;
	    ifstream infile;
	    infile.open( filename.c_str(), ios_base::in|ios_base::binary );
	    if ( !infile.good() )
		{
#if 1
		ofstream ofile( filename.c_str(), ios_base::out|ios_base::binary|ios_base::trunc );
		if ( !ofile.good() )
		    {
		    cout << "   Error creating " << filename << endl;
		    }
		else
		    {
		    ofile.write( (const char*)&gNOPEEventIdentifier, sizeof(gNOPEEventIdentifier) );
		    ofile.close();
		    cout << "   " << filename << " created" << endl;
		    }
#else
		cout << "   " << filename << " missing" << endl;
#endif
		}
	    else
		infile.close();
	    }
	}

    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
