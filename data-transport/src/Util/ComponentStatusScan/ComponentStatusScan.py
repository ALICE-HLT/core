#!/usr/bin/env python

import os, string, sys
import time
import libxml2
import glob
import rrdtool

uname=os.uname()
target = uname[0]+"-"+uname[4]
base = os.path.dirname( sys.argv[0] )
if ( os.path.basename( base )==target+"-debug" or os.path.basename( base )==target+"-release" or os.path.basename( base )==target+"-releasedebug" ) and os.path.basename( os.path.dirname( base ) )=="bin":
    sys.path.append( os.path.join( os.path.dirname( os.path.dirname( base ) ), "lib", target+"-debug" ) )
    sys.path.append( os.path.join( os.path.dirname( os.path.dirname( base ) ), "lib", target+"-release" ) )
    sys.path.append( os.path.join( os.path.dirname( os.path.dirname( base ) ), "lib", target+"-releasedebug" ) )
sys.path.append( os.path.join( base, "module-links", target+"-debug" ) )
sys.path.append( os.path.join( base, "module-links", target+"-release" ) )
sys.path.append( os.path.join( base, "module-links", target+"-releasedebug" ) )

import TMControlInterface

aliceHLT = False
connectAddresses=[]

def UsageErrorExit( str=None ):
    print "Usage: " + sys.argv[0] + " -address <local-address> ([ -control <remote-address> | -config <TaskManager-config-file> | -inputfile <input-data-dump-file> | -inputfilelist <input-data-dump-file-glob-pattern> ]) (-dump) (-dump2) (-v) (-sort [ id | key ] ) (-check) (-alicehlt) (-loop) (-loopinterval <loop-interval-seconds>) (-stats) (-state) (-outputfile <outputfilename>) (-processlist <propcess list file> (-checklostevents)) (-losteventmincnt <minimum-limit-for-lost-events>) (-losteventrepeatlimit <number-of-consecutive-lost-events>) (-checkmsglogbinary <binary-for-logging-check-message>) (-nodetaileddetectors) -rrd-directory <directory> (-color) (-max-events-warning <num>) (-quit-on-run-change)"
    if str != None:
        print str
    sys.exit( -1 )


def GetChildAddressFromConfigFile( configFile ):
    xmlFile = libxml2.parseFile(configFile)
    xpathContext = xmlFile.xpathNewContext()
    childAddress = xpathContext.xpathEval("/Task/SlaveControlConfig/TargetAddress")[0].content
    return childAddress

def StatusData2Dict( statusData ):
    d = {}
    for c in statusData.split(":"):
        cd = c.split("=")
        if len(cd)==2:
            d[cd[0]]=cd[1]
    return d

def DumpData( slave, statusDict, keys=None ):
    dumps=[]
    if keys==None:
        keys=statusDict.keys()
        if not "name" in keys:
            return dumps
        keys.remove("name")
    keys.sort()
    if statusDict.has_key("State"):
        compType = statusDict["State"].split(":")[0]
    else:
        compType="UNKNOWN"
    for k in keys:
        dumps.append( compType+" "+slave+"/"+statusDict["name"]+":"+k+"="+statusDict[k] )
    return dumps

def GetTopLevelState( target ):
    if target!=None:
        address = target
    else:
        if verbose:
            print "Determining top level state target address from config file "+targetConfig+"..."
        address = GetChildAddressFromConfigFile( targetConfig )
        if verbose:
            print "Done"
    try:
        state = TMControlInterface.QueryState( address )
    except SystemError, e:
        print "Unable to query state from top level TM (address "+address+")."
        state = "UNKNOWN"
    return state

def GetTopLevelStateFromFile( targetInputFile ):
    state = "UNKNOWN"
    try:
        infile = open( targetInputFile, "r" )
    except IOError,e:
        print "Error opening input file '"+targetInputFile+"': "+str(e)
        return state
    line=infile.readline()
    while line!="":
        if line [:len("Global State:")]=="Global State:":
            state = line.split(" ")[-1]
            break
        line=infile.readline()
    infile.close()
    return state

def GetSlaveList( target ):

    retry = True
    retryCnt = 0
    maxRetryCnt = 3
    while retry and retryCnt<=maxRetryCnt:
        retry = False
        retryCnt += 1

        TMAddresses = {}
        slaves = []
        slaveChildStates = {}
        if target!=None:
            TMAddresses["master"] = target
        else:
            if verbose:
                print "Determining target address from config file "+targetConfig+"..."
            TMAddresses["master"] = GetChildAddressFromConfigFile( targetConfig )
            if verbose:
                print "Done"
        TMs = [ "master" ]

        while len(TMs)>0:
            try:
                if verbose:
                    print "Querying child list from TM "+TMs[0]+" at address "+TMAddresses[TMs[0]]
                if not TMAddresses[TMs[0]] in connectAddresses:
                    TMControlInterface.Connect( TMAddresses[TMs[0]] )
                    connectAddresses.append(TMAddresses[TMs[0]])
                children = TMControlInterface.QueryChildren( TMAddresses[TMs[0]] )
                childProcessList = children.split( "  " )
                hasSlave=False
                hasProgram=False
                childStates = {}
                for c in childProcessList:
                    if verbose:
                        print "Processing child "+str(c)
                    childProcess = c.split( " " ) # ID, type, address, state
                    if len(childProcess)<4:
                        continue
                    if childProcess[1]=="slave" or childProcess[1]=="slaveprogram":
                        hasSlave=True
                    if childProcess[1]=="program":
                        hasProgram=True
                    if childProcess[3][:3]=="TM:":
                        continue
                    if childProcess[1]=="slave" or childProcess[1]=="slaveprogram":
                        TMAddresses[childProcess[0]] = childProcess[2]
                        TMs.append( childProcess[0] )
                    if childProcess[1]=="program":
                        childStates[childProcess[0]] = childProcess[3]
                    if hasSlave==hasProgram:
                        if hasSlave:
                            has=["both","and"]
                        else:
                            has=["neither","nor"]
                        print "Inconsistency program. TM "+TMs[0]+" has "+has[0]+" TM slaves "+has[1]+" programs"
                        continue
                if hasProgram:
                    slaves.append( TMs[0] )
                    slaveChildStates[TMs[0]] = childStates
                
            except SystemError, e:
                print "Unable to query children from TM "+TMs[0]+" (address "+TMAddresses[TMs[0]]+")."
                retry = True
                break
            TMs = TMs[1:]
    
    slaves.sort()
    return (slaves,TMAddresses,slaveChildStates)

def GetSlaveStatusData( slaves, TMAddresses ):
    slaveStatusData = {}

    for slave in slaves:
        try:
            if verbose:
                print "Querying status data from TM "+slave+" at address "+TMAddresses[slave]
                start = time.time()
            slaveStatusData[slave] = TMControlInterface.QueryStatusData( TMAddresses[slave] )
            if verbose:
                print slave+": %f s" % ( time.time()-start )
            if verbose:
                print "Received status data from TM "+slave+": "+slaveStatusData[slave]
        except SystemError, e:
            print "Unable to query status data from TM "+slave+" (address "+TMAddresses[slave]+")."
    return slaveStatusData

def ParseSlaveStatusData( slaves, slaveStatusData, slaveChildStates ):
    slaveChildData = {}
    for slave in slaves:
        slaveChildData[slave] = {}
        if not slaveStatusData.has_key(slave):
            continue
        procList = slaveStatusData[slave].split(";")
        for proc in procList:
            statusDict = StatusData2Dict( proc )
            if statusDict.has_key("name") and slaveChildStates[slave].has_key(statusDict["name"]):
                state=slaveChildStates[slave][statusDict["name"]]
            else:
                if statusDict.has_key("RunNumber"):
                    slaveChildData[slave]["RunNumber"] = statusDict
                    continue
                state="UNKNOWN"
            #compType = state.split(":")[0]
            statusDict["State"] = state
            if statusDict.has_key("name"):
                slaveChildData[slave][statusDict["name"]] = statusDict
    return slaveChildData


def GetData( target ):
    times = {}
    timecnt=-1
    times[timecnt] = time.time()
    timecnt += 1
##    TMAddresses = {}
##    slaves = []
##    slaveChildStates = {}
##    if target!=None:
##        TMAddresses["master"] = target
##    else:
##        if verbose:
##            print "Determining target address from config file "+targetConfig+"..."
##        TMAddresses["master"] = GetChildAddressFromConfigFile( targetConfig )
##        if verbose:
##            print "Done"
##    TMs = [ "master" ]

##    while len(TMs)>0:
##        try:
##            if verbose:
##                print "Querying child list from TM "+TMs[0]+" at address "+TMAddresses[TMs[0]]
##            children = TMControlInterface.QueryChildren( TMAddresses[TMs[0]] )
##            childProcessList = children.split( "  " )
##            hasSlave=False
##            hasProgram=False
##            childStates = {}
##            for c in childProcessList:
##                if verbose:
##                    print "Processing child "+str(c)
##                childProcess = c.split( " " ) # ID, type, address, state
##                if len(childProcess)<4:
##                    continue
##                if childProcess[1]=="slave" or childProcess[1]=="slaveprogram":
##                    TMAddresses[childProcess[0]] = childProcess[2]
##                    TMs.append( childProcess[0] )
##                    hasSlave=True
##                if childProcess[1]=="program":
##                    hasProgram=True
##                    childStates[childProcess[0]] = childProcess[3]
##                if childProcess[3][:3]=="TM:":
##                    continue
##                if hasSlave==hasProgram:
##                    if hasSlave:
##                        has=["both","and"]
##                    else:
##                        has=["neither","nor"]
##                    print "Inconsistency program. TM "+TMs[0]+" has "+has[0]+" TM slaves "+has[1]+" programs"
##                    continue
##            if hasProgram:
##                slaves.append( TMs[0] )
##                slaveChildStates[TMs[0]] = childStates
            
##        except SystemError, e:
##            print "Unable to query children from TM "+TMs[0]+" (address "+TMAddresses[TMs[0]]+")."
##        TMs = TMs[1:]

##    slaves.sort()
    slaves,TMAddresses,slaveChildStates = GetSlaveList(target)
    times[timecnt] = time.time()
    timecnt += 1

##    slaveStatusData = {}

##    for slave in slaves:
##        try:
##            if verbose:
##                print "Querying status data from TM "+slave+" at address "+TMAddresses[slave]
##            slaveStatusData[slave] = TMControlInterface.QueryStatusData( TMAddresses[slave] )
##            if verbose:
##                print "Received status data from TM "+slave+": "+slaveStatusData[slave]
##        except SystemError, e:
##            print "Unable to query status data from TM "+slave+" (address "+TMAddresses[slave]+")."

    slaveStatusData = GetSlaveStatusData( slaves, TMAddresses )
    times[timecnt] = time.time()
    timecnt += 1

##    slaveChildData = {}
##    for slave in slaves:
##        slaveChildData[slave] = {}
##        if not slaveStatusData.has_key(slave):
##            continue
##        procList = slaveStatusData[slave].split(";")
##        for proc in procList:
##            statusDict = StatusData2Dict( proc )
##            if statusDict.has_key("name") and slaveChildStates[slave].has_key(statusDict["name"]):
##                state=slaveChildStates[slave][statusDict["name"]]
##            else:
##                state="UNKNOWN"
##            compType = state.split(":")[0]
##            statusDict["State"] = state
##            if statusDict.has_key("name"):
##                slaveChildData[slave][statusDict["name"]] = statusDict
##    return slaveChildData

    slaveChildData = ParseSlaveStatusData( slaves, slaveStatusData, slaveChildStates )
    times[timecnt] = time.time()
    timecnt += 1

#    for i in range(timecnt):
#        print "Time %d: %f" % (i, times[i]-times[i-1])
        
    return slaveChildData

def GetFileData( targetInputFile ):
    data = {}
    childStates = {}
    try:
        infile = open( targetInputFile, "r" )
    except IOError,e:
        print "Error opening input file '"+targetInputFile+"': "+str(e)
        return data
    line=infile.readline()
    while line!="":
        if line[:len("Run Number")]=="Run Number":
            data["RunNumber"] = { "RunNumber" : "RunNumber="+line.split(" ")[-1] }
        if line.count(" ")==1 and line.count("/")==1 and line.count(":")>0 and line.count("=")==1 and (line[:len("Controlled")]=="Controlled" or line[:len("Event")]=="Event"):
            slaveTM = (line.split()[1]).split("/")[0]
            proc = ((line.split()[1]).split("/")[1]).split(":")[0]
            metric = ":".join(((line.split()[1]).split("/")[1]).split(":")[1:])
            metricName = metric.split("=")[0]
            if not data.has_key(slaveTM):
                data[slaveTM] = {}
            if not childStates.has_key(slaveTM):
                childStates[slaveTM] = {}
            if not data[slaveTM].has_key(proc):
                data[slaveTM][proc] = "name="+proc
            data[slaveTM][proc] = data[slaveTM][proc]+":"+metric
            if metricName=="State" and not childStates[slaveTM].has_key(proc):
                childStates[slaveTM][proc] = metric.split("=")[1]
        line=infile.readline()
    infile.close()
    for slave in data.keys():
        data[slave] = ";".join(data[slave].values())
    return ParseSlaveStatusData( data.keys(), data, childStates )


def Dump( slaveChildData, sortOrder ):
    dumps = []
    if sortOrder=="id":
        for slave in slaveChildData.keys():
            if not slaveChildData.has_key(slave):
                continue
            procList = slaveChildData[slave].keys()
            procList.sort()
            for proc in procList:
                dumps.extend( DumpData( slave, slaveChildData[slave][proc], keys=None ) )
    elif sortOrder=="key":
        keySet = set()
        for slave in slaveChildData.keys():
            if not slaveChildData.has_key(slave):
                continue
            procList = slaveChildData[slave].keys()
            for proc in procList:
                keys = slaveChildData[slave][proc].keys()
                keys.remove("name")
                keySet |= set(keys)
        keyList = list(keySet)
        keyList.sort()
        for k in keyList:
            for slave in slaveChildData.keys():
                if not slaveChildData.has_key(slave):
                    continue
                procList = slaveChildData[slave].keys()
                procList.sort()
                for proc in procList:
                    dumps.extend( DumpData( slave, slaveChildData[slave][proc], keys=[k] ) )
    return dumps


def Print( slaveChildData, sortOrder, loop=False, screen=None ):
    for d in Dump( slaveChildData, sortOrder ):
        if loop:
            if not rrd:
                screen.addstr( d+"\n" )
        else:
            print d

def Check( slaveChildData ):
    results = []
    totalAnnounceCount = 0
    for slave in slaveChildData.keys():
        if not slaveChildData.has_key(slave):
            continue
        procList = slaveChildData[slave].keys()
        procList.sort()
        for proc in procList:
            statusDict = slaveChildData[slave][proc]
            if statusDict.has_key("State"):
                compType = statusDict["State"].split(":")[0]
                if compType=="ControlledDataSource":
                    if statusDict.has_key("AnnouncedEventCount"):
                        totalAnnounceCount += int(statusDict["AnnouncedEventCount"])
            else:
                compType = "UNKNOWN"

    for slave in slaveChildData.keys():
        if not slaveChildData.has_key(slave):
            continue
        procList = slaveChildData[slave].keys()
        procList.sort()
        for proc in procList:
            statusDict = slaveChildData[slave][proc]
            if statusDict.has_key("State"):
                compType = statusDict["State"].split(":")[0]
            else:
                compType = "UNKNOWN"
            if compType=="ControlledDataProcessor" or compType=="ControlledDataSource":
                if statusDict.has_key("TotalOutputBuffer") and statusDict.has_key("FreeOutputBuffer") and float(statusDict["TotalOutputBuffer"])>0 and float(statusDict["FreeOutputBuffer"])/float(statusDict["TotalOutputBuffer"])<=0.2:
                    results.append( compType+" "+slave+"/"+statusDict["name"]+" has only %d%% free output buffer space" % int(float(statusDict["FreeOutputBuffer"])/float(statusDict["TotalOutputBuffer"])*100.0) )
                if statusDict.has_key("CurrentAnnounceRate") and float(statusDict["CurrentAnnounceRate"])<=0.1 and totalAnnounceCount>0:
                    results.append( compType+" "+slave+"/"+statusDict["name"]+" has announced only at a rate of %f Hz during the last measurement interval" % float(statusDict["CurrentAnnounceRate"]) )
            if compType=="ControlledDataProcessor":
                if statusDict.has_key("ReceivedEventCount") and statusDict.has_key("AnnouncedEventCount") and int(statusDict["ReceivedEventCount"])>100 and float(statusDict["AnnouncedEventCount"])/float(statusDict["ReceivedEventCount"])<=0.1:
                    results.append( compType+" "+slave+"/"+statusDict["name"]+" has processed only %d%% of its input events" % int(float(statusDict["AnnouncedEventCount"])/float(statusDict["ReceivedEventCount"])*100.0) )
            if compType=="ControlledDataProcessor" or compType=="ControlledDataSink":
                if statusDict.has_key("PendingInputEventCount") and statusDict.has_key("PendingOutputEventCount") and ( int(statusDict["PendingInputEventCount"])-int(statusDict["PendingOutputEventCount"]) )>=1000:
                    results.append( compType+" "+slave+"/"+statusDict["name"]+" has %d pending input events" % ( int(statusDict["PendingInputEventCount"])-int(statusDict["PendingOutputEventCount"]) ) )
            if compType=="ControlledDataSource":
                if statusDict.has_key("AnnouncedEventCount") and totalAnnounceCount>0 and int(statusDict["AnnouncedEventCount"])==0:
                    results.append( compType+" "+slave+"/"+statusDict["name"]+" has not announced any events" )
    return results

def Statistics( slaveChildData, lostEvents ):
    results = { "results" : {}, "descriptions" : {}, "order" : [] }
    runNumber = 0
    maxAnnounceCount = 0
    minAnnounceCount = None
    totalAnnounceCount = 0
    totalAnnounceSize = 0
    avgCurrentAnnounceRate = 0.0
    minCurrentAnnounceRate = None
    maxCurrentAnnounceRate = 0.0
    avgTotalAnnounceRate = 0.0
    minTotalAnnounceRate = None
    maxTotalAnnounceRate = 0.0
    maxEventsInChain = 0
    avgCurrentProcessRate=0.0
    minCurrentProcessRate=None
    maxCurrentProcessRate=0.0
    avgTotalProcessRate=0.0
    minTotalProcessRate=None
    maxTotalProcessRate=0.0
    maxRoundtripProcessedCount = 0
    maxFinishedCount = 0
    totalFinishedSize = 0
    totalFinishedCount_OUT = 0
    totalFinishedSize_OUT = 0
    currentReceiveRate_OUT = 0.0
    totalReceiveRate_OUT = 0.0
    currentSendRate_OUT = 0.0
    totalSendRate_OUT = 0.0
    sumPending_OUT = 0
    maxSinglePending_OUT = 0
    runningSourcesCnt=0
    runningProcessorsCnt=0
    runningSinksCnt=0
    runningDataFlowProcsCnt=0
    deadProcsCnt=0
    sourceCnt=0.0
    procCnt=0.0
    maxPendingInputProcessorSinks = [ None, None, None ]
    maxPendingInputProcessorSinkCounts = []
    minFreeBufferUtil=[ 100., 100., 100. ]
    minFreeBufferProc=[ "", "", "" ]
    for i in maxPendingInputProcessorSinks:
        maxPendingInputProcessorSinkCounts.append( 0 )
    maxPendingInputBridges = [ None, None, None ]
    maxPendingInputBridgeCounts = []
    for i in maxPendingInputBridges:
        maxPendingInputBridgeCounts.append( 0 )
    maxPendingInputMergers = [ None, None, None ]
    maxPendingInputMergerCounts = []
    for i in maxPendingInputMergers:
        maxPendingInputMergerCounts.append( 0 )

    detTotalAnnounceCount = {}
    detTotalAnnounceSize = {}
    detAvgCurrentAnnounceRates = {}
    detAvgTotalAnnounceRates = {}
    detSourceCnts = {}
    detList = set()
    totalMaxEventLifetime = 0
    totalMinEventLifetime = 0
    currentAvgEventLifetime = 0
    runningOutWriterSubscribers = 0
    maxOutOfOrderOrbits = 0
    
    for slave in slaveChildData.keys():
        if not slaveChildData.has_key(slave):
            continue
        procList = slaveChildData[slave].keys()
        procList.sort()
        for proc in procList:
            if verbose:
                print "Slave:",slave," - Proc:",proc
            if proc=="RunNumber":
                if slaveChildData[slave][proc].has_key("RunNumber") and runNumber==0:
                    runNumber = int(slaveChildData[slave][proc]["RunNumber"])
                continue
            statusDict = slaveChildData[slave][proc]
            compType = statusDict["State"].split(":")[0]
            if statusDict.has_key("FreeOutputBuffer") and statusDict.has_key("TotalOutputBuffer") and float(statusDict["TotalOutputBuffer"]) > 0.:
                buffUsage=float(statusDict["FreeOutputBuffer"])*100./float(statusDict["TotalOutputBuffer"])
                procName=statusDict["name"]
                procType=2
                if compType=="ControlledDataSource":
                    procType=0
                elif compType=="ControlledDataProcessor":
                    procType=1
                if dump2:
                    print procName, ": ", buffUsage, " (", statusDict["FreeOutputBuffer"], " / ", statusDict["TotalOutputBuffer"], ")"
                if buffUsage < minFreeBufferUtil[procType]:
                    minFreeBufferUtil[procType]=buffUsage
                    minFreeBufferProc[procType]=procName
            if compType=="ControlledDataSource":
                if statusDict.has_key("State") and statusDict["State"].split(":")[0]!="TM":
                    runningSourcesCnt += 1
                if statusDict.has_key("AnnouncedEventCount"):
                    totalAnnounceCount += int(statusDict["AnnouncedEventCount"])
                if statusDict.has_key("AnnouncedEventCount") and maxAnnounceCount<int(statusDict["AnnouncedEventCount"]):
                    maxAnnounceCount = int(statusDict["AnnouncedEventCount"])
                if statusDict.has_key("AnnouncedEventCount") and (minAnnounceCount==None or minAnnounceCount>int(statusDict["AnnouncedEventCount"]) ):
                    minAnnounceCount = int(statusDict["AnnouncedEventCount"])
                if statusDict.has_key("TotalProcessedOutputDataSize"):
                    totalAnnounceSize += int(statusDict["TotalProcessedOutputDataSize"])
                if statusDict.has_key("AnnouncedEventCount") and statusDict.has_key("PendingOutputEventCount") and maxRoundtripProcessedCount<(int(statusDict["AnnouncedEventCount"])-int(statusDict["PendingOutputEventCount"])):
                    maxRoundtripProcessedCount = int(statusDict["AnnouncedEventCount"])-int(statusDict["PendingOutputEventCount"])
                if statusDict.has_key("PendingOutputEventCount") and maxEventsInChain<int(statusDict["PendingOutputEventCount"]):
                    maxEventsInChain = int(statusDict["PendingOutputEventCount"])
                if statusDict.has_key("CurrentAnnounceRate"):
                    if minCurrentAnnounceRate==None or minCurrentAnnounceRate>float(statusDict["CurrentAnnounceRate"]):
                        minCurrentAnnounceRate = float(statusDict["CurrentAnnounceRate"])
                    if maxCurrentAnnounceRate < float(statusDict["CurrentAnnounceRate"]):
                        maxCurrentAnnounceRate = float(statusDict["CurrentAnnounceRate"])
                    avgCurrentAnnounceRate += float(statusDict["CurrentAnnounceRate"])
                if statusDict.has_key("AvgAnnounceRate"):
                    if minTotalAnnounceRate==None or minTotalAnnounceRate>float(statusDict["AvgAnnounceRate"]):
                        minTotalAnnounceRate = float(statusDict["AvgAnnounceRate"])
                    if maxTotalAnnounceRate < float(statusDict["AvgAnnounceRate"]):
                        maxTotalAnnounceRate = float(statusDict["AvgAnnounceRate"])
                    avgTotalAnnounceRate += float(statusDict["AvgAnnounceRate"])
                if aliceHLT and detailedDetectors and statusDict.has_key("name"):
                    toks = statusDict["name"].split("_")
                    det = toks[0]
                    detList.add(det)
                    if not detAvgCurrentAnnounceRates.has_key(det):
                        detAvgCurrentAnnounceRates[det] = 0.0
                    if statusDict.has_key("CurrentAnnounceRate"):
                        detAvgCurrentAnnounceRates[det] += float(statusDict["CurrentAnnounceRate"])
                    if not detAvgTotalAnnounceRates.has_key(det):
                        detAvgTotalAnnounceRates[det] = 0.0
                    if statusDict.has_key("AvgAnnounceRate"):
                        detAvgTotalAnnounceRates[det] += float(statusDict["AvgAnnounceRate"])
                    if not detSourceCnts.has_key(det):
                        detSourceCnts[det] = 0.0
                    if not detTotalAnnounceCount.has_key(det):
                        detTotalAnnounceCount[det] = 0.0
                    if statusDict.has_key("AnnouncedEventCount"):
                        detTotalAnnounceCount[det] += int(statusDict["AnnouncedEventCount"])
                    if not detTotalAnnounceSize.has_key(det):
                        detTotalAnnounceSize[det] = 0.0
                    if statusDict.has_key("TotalProcessedOutputDataSize"):
                        detTotalAnnounceSize[det] += int(statusDict["TotalProcessedOutputDataSize"])
                    detSourceCnts[det] += 1.0
                sourceCnt += 1.0
            elif compType=="ControlledDataSink":
                if statusDict.has_key("State") and statusDict["State"].split(":")[0]!="TM":
                    runningSinksCnt += 1
                #if not aliceHLT:
                if statusDict.has_key("ProcessedEventCount") and maxFinishedCount<int(statusDict["ProcessedEventCount"]):
                    maxFinishedCount = int(statusDict["ProcessedEventCount"])
                if statusDict.has_key("TotalProcessedInputDataSize"):
                    totalFinishedSize += int(statusDict["TotalProcessedInputDataSize"])
                if aliceHLT:
                    outIDStart="ALICE_HLTOutput"
                    outIDMid="HOWS"
                    if proc[:len(outIDStart)]==outIDStart and proc[len(outIDStart)+5:len(outIDStart)+5+len(outIDMid)]:
                        if statusDict.has_key("ProcessedEventCount"):
                            totalFinishedCount_OUT += int(statusDict["ProcessedEventCount"])
                        if statusDict.has_key("TotalProcessedInputDataSize"):
                            totalFinishedSize_OUT += int(statusDict["TotalProcessedInputDataSize"])
                        if statusDict.has_key("PendingInputEventCount"):
                            sumPending_OUT += int(statusDict["PendingInputEventCount"])
                            if maxSinglePending_OUT<int(statusDict["PendingInputEventCount"]):
                                maxSinglePending_OUT = int(statusDict["PendingInputEventCount"])
                        if statusDict.has_key("AvgReceiveRate"):
                            totalReceiveRate_OUT += float(statusDict["AvgReceiveRate"])
                        if statusDict.has_key("CurrentReceiveRate"):
                            currentReceiveRate_OUT += float(statusDict["CurrentReceiveRate"])
                        if statusDict.has_key("AvgProcessRate"):
                            totalSendRate_OUT += float(statusDict["AvgProcessRate"])
                        if statusDict.has_key("CurrentProcessRate"):
                            currentSendRate_OUT += float(statusDict["CurrentProcessRate"])
                        if statusDict.has_key("TotalMaxEventLifetime") and totalMaxEventLifetime<int(statusDict["TotalMaxEventLifetime"]):
                            totalMaxEventLifetime = int(statusDict["TotalMaxEventLifetime"])
                        if statusDict.has_key("TotalMinEventLifetime") and (totalMinEventLifetime>int(statusDict["TotalMinEventLifetime"]) or totalMinEventLifetime==0) and int(statusDict["TotalMinEventLifetime"])>0:
                            totalMinEventLifetime = int(statusDict["TotalMinEventLifetime"])
                        if statusDict.has_key("CurrentAvgEventLifetime") and int(statusDict["CurrentAvgEventLifetime"])>0:
                            currentAvgEventLifetime += int(statusDict["CurrentAvgEventLifetime"])
                            runningOutWriterSubscribers += 1
                        if statusDict.has_key("MaxOutOfOrderOrbits") and maxOutOfOrderOrbits<int(statusDict["MaxOutOfOrderOrbits"]):
                            maxOutOfOrderOrbits = int(statusDict["MaxOutOfOrderOrbits"])
                if statusDict.has_key("PendingInputEventCount"):
                    for i in range(len(maxPendingInputProcessorSinks)):
                        if maxPendingInputProcessorSinks[i]==None or maxPendingInputProcessorSinkCounts[i]<int(statusDict["PendingInputEventCount"]):
                            for j in range( len(maxPendingInputProcessorSinks)-1, i,-1 ):
                                maxPendingInputProcessorSinks[j] = maxPendingInputProcessorSinks[j-1]
                                maxPendingInputProcessorSinkCounts[j] = maxPendingInputProcessorSinkCounts[j-1]
                            maxPendingInputProcessorSinks[i] = statusDict["name"]
                            maxPendingInputProcessorSinkCounts[i] = int(statusDict["PendingInputEventCount"])
                            break
            elif compType=="ControlledDataProcessor":
                if statusDict.has_key("State") and statusDict["State"].split(":")[0]!="TM":
                    runningProcessorsCnt += 1
                if statusDict.has_key("CurrentProcessRate"):
                    avgCurrentProcessRate += float(statusDict["CurrentProcessRate"])
                    if minCurrentProcessRate==None or float(statusDict["CurrentProcessRate"])<minCurrentProcessRate:
                        minCurrentProcessRate = float(statusDict["CurrentProcessRate"])
                    if float(statusDict["CurrentProcessRate"])>maxCurrentProcessRate:
                        maxCurrentProcessRate = float(statusDict["CurrentProcessRate"])
                if statusDict.has_key("AvgProcessRate"):
                    avgTotalProcessRate += float(statusDict["AvgProcessRate"])
                    if minTotalProcessRate==None or float(statusDict["AvgProcessRate"])<minTotalProcessRate:
                        minTotalProcessRate = float(statusDict["AvgProcessRate"])
                    if float(statusDict["AvgProcessRate"])>maxTotalProcessRate:
                        maxTotalProcessRate = float(statusDict["AvgProcessRate"])
                if statusDict.has_key("PendingInputEventCount"):
                    for i in range(len(maxPendingInputProcessorSinks)):
                        if maxPendingInputProcessorSinks[i]==None or maxPendingInputProcessorSinkCounts[i]<int(statusDict["PendingInputEventCount"]):
                            for j in range( len(maxPendingInputProcessorSinks)-1, i,-1 ):
                                maxPendingInputProcessorSinks[j] = maxPendingInputProcessorSinks[j-1]
                                maxPendingInputProcessorSinkCounts[j] = maxPendingInputProcessorSinkCounts[j-1]
                            maxPendingInputProcessorSinks[i] = statusDict["name"]
                            maxPendingInputProcessorSinkCounts[i] = int(statusDict["PendingInputEventCount"])
                            break
                procCnt += 1.0
            else:
                if statusDict.has_key("State") and statusDict["State"].split(":")[0]!="TM" and statusDict["State"]!="UNKNOWN":
                    runningDataFlowProcsCnt += 1
                if statusDict.has_key("State") and (statusDict["State"].split(":")[0]=="TM" or statusDict["State"]=="UNKNOWN"):
                    deadProcsCnt += 1
                if compType=="ControlledBridge" and statusDict.has_key("PendingInputEventCount"):
                    for i in range(len(maxPendingInputBridges)):
                        if maxPendingInputBridges[i]==None or maxPendingInputBridgeCounts[i]<int(statusDict["PendingInputEventCount"]):
                            for j in range( len(maxPendingInputBridges)-1, i,-1 ):
                                maxPendingInputBridges[j] = maxPendingInputBridges[j-1]
                                maxPendingInputBridgeCounts[j] = maxPendingInputBridgeCounts[j-1]
                            maxPendingInputBridges[i] = statusDict["name"]
                            maxPendingInputBridgeCounts[i] = int(statusDict["PendingInputEventCount"])
                            break
                if compType=="EventMerger" and statusDict.has_key("PendingInputEventCount"):
                    for i in range(len(maxPendingInputMergers)):
                        if maxPendingInputMergers[i]==None or maxPendingInputMergerCounts[i]<int(statusDict["PendingInputEventCount"]):
                            for j in range( len(maxPendingInputMergers)-1, i,-1 ):
                                maxPendingInputMergers[j] = maxPendingInputMergers[j-1]
                                maxPendingInputMergerCounts[j] = maxPendingInputMergerCounts[j-1]
                            maxPendingInputMergers[i] = statusDict["name"]
                            maxPendingInputMergerCounts[i] = int(statusDict["PendingInputEventCount"])
                            break

    global maxEventsWarningState
    if maxEventsWarning > 0 and maxEventsInChain >= maxEventsWarning:
        if maxEventsWarningState >= 2:
            os.system("log -e \"Too many events in HLT chain: " + str(maxEventsInChain) + "\"")
            if os.getenv('CLUSTER') == 'prod' and os.path.isfile(os.getenv('HLT_CONFIG_SRCDIR') + '/hltmode_production'):
                os.system("ssh -i ~/.ssh/id_dsa.acr hlt@arcs04.cern.ch 'DATE_RUN_NUMBER=" + str(runNumber) + " ~/log -f HLT -d HLT -s error -l OPS -p PHYSICS_1 \"Too many events in HLT chain: " + str(maxEventsInChain) + "\"'")
        else:
            maxEventsWarningState += 1
    elif maxEventsWarningState > 0 and maxEventsInChain < 500:
        os.system("log -m \"HLT recovered. Events in chain: " + str(maxEventsInChain) + "\"")
        if os.getenv('CLUSTER') == 'prod' and os.path.isfile(os.getenv('HLT_CONFIG_SRCDIR') + '/hltmode_production'):
            os.system("ssh -i ~/.ssh/id_dsa.acr hlt@arcs04.cern.ch 'DATE_RUN_NUMBER=" + str(runNumber) + " ~/log -f HLT -d HLT -s info -l OPS -p PHYSICS_1 \"HLT recovered. Events in chain: " + str(maxEventsInChain) + "\"'")
        maxEventsWarningState = 0

    if sourceCnt>0:
        avgCurrentAnnounceRate /= sourceCnt
        avgTotalAnnounceRate /= sourceCnt
        avgTotalAnnounceCount = totalAnnounceCount/sourceCnt
        if aliceHLT and len(detList)>0:
            for det in detList:
                detAvgCurrentAnnounceRates[det] /= detSourceCnts[det]
                detAvgTotalAnnounceRates[det] /= detSourceCnts[det]
                detTotalAnnounceCount[det] /= detSourceCnts[det]
    else:
        avgTotalAnnounceCount = 0
        minAnnounceCount = 0
        minCurrentAnnounceRate = 0.0
        minTotalAnnounceRate = 0.0
    if procCnt>0:
        avgCurrentProcessRate /= procCnt
        avgTotalProcessRate /= procCnt
    else:
        minCurrentProcessRate = 0.0
        minTotalProcessRate = 0.0

    if runningOutWriterSubscribers:
        currentAvgEventLifetime /= runningOutWriterSubscribers
        currentAvgEventLifetime = str(currentAvgEventLifetime/1000)
    else:
        currentAvgEventLifetime = "-"

    for i in range(len(maxPendingInputProcessorSinks)):
        if maxPendingInputProcessorSinks[i]==None:
            maxPendingInputProcessorSinks[i] = "---"
            maxPendingInputProcessorSinkCounts[i] = 0

    for i in range(len(maxPendingInputBridges)):
        if maxPendingInputBridges[i]==None:
            maxPendingInputBridges[i] = "---"
            maxPendingInputBridgeCounts[i] = 0

    for i in range(len(maxPendingInputMergers)):
        if maxPendingInputMergers[i]==None:
            maxPendingInputMergers[i] = "---"
            maxPendingInputMergerCounts[i] = 0

    global lastRunNumber
    global forceQuit
    if lastRunNumber != 0 and lastRunNumber != runNumber:
        forceQuit = 1
    if quitOnRunChange and runNumber != 0:
        lastRunNumber = runNumber

    results["results"] = {
        "runNumber" : runNumber,
        "maxAnnounceCount" : maxAnnounceCount,
        "minAnnounceCount" : minAnnounceCount,
        "avgAnnounceCount" : avgTotalAnnounceCount,
        "totalAnnounceSize" : totalAnnounceSize/1024,
        "minCurrentAnnounceRate" : minCurrentAnnounceRate,
        "avgCurrentAnnounceRate" : avgCurrentAnnounceRate,
        "maxCurrentAnnounceRate" : maxCurrentAnnounceRate,
        "minTotalAnnounceRate" : minTotalAnnounceRate,
        "avgTotalAnnounceRate" : avgTotalAnnounceRate,
        "maxTotalAnnounceRate" : maxTotalAnnounceRate,
        "maxEventsInChain" : maxEventsInChain,
        "minCurrentProcessRate" : minCurrentProcessRate,
        "avgCurrentProcessRate" : avgCurrentProcessRate,
        "maxCurrentProcessRate" : maxCurrentProcessRate,
        "minAvgProcessRate" : minTotalProcessRate,
        "avgAvgProcessRate" : avgTotalProcessRate,
        "maxAvgProcessRate" : maxTotalProcessRate,
        "maxRoundtripProcessedCount" : maxRoundtripProcessedCount,
        "maxFinishedCount" : maxFinishedCount,
        "totalFinishedSize" : totalFinishedSize/1024,
        "totalFinishedCount_OUT" : totalFinishedCount_OUT,
        "totalFinishedSize_OUT" : totalFinishedSize_OUT/1024,
        "sumPending_OUT" : sumPending_OUT,
        "maxSinglePending_OUT" : maxSinglePending_OUT,
        "currentReceiveRate_OUT" : currentReceiveRate_OUT,
        "totalReceiveRate_OUT" : totalReceiveRate_OUT,
        "currentSendRate_OUT" : currentSendRate_OUT,
        "totalSendRate_OUT" : totalSendRate_OUT,
        "runningSourcesCnt" : runningSourcesCnt,
        "runningProcessorsCnt" : runningProcessorsCnt,
        "runningSinksCnt" : runningSinksCnt,
        "runningDataFlowProcsCnt" : runningDataFlowProcsCnt,
        "deadProcsCnt" : deadProcsCnt,
        "maximumPendingInputComponent0" : maxPendingInputProcessorSinks[0],
        "maximumPendingInputComponentCount0" : maxPendingInputProcessorSinkCounts[0],
        "maximumPendingInputComponent1" : maxPendingInputProcessorSinks[1],
        "maximumPendingInputComponentCount1" : maxPendingInputProcessorSinkCounts[1],
        "maximumPendingInputComponent2" : maxPendingInputProcessorSinks[2],
        "maximumPendingInputComponentCount2" : maxPendingInputProcessorSinkCounts[2],
        "maximumPendingInputBridge0" : maxPendingInputBridges[0],
        "maximumPendingInputBridgeCount0" : maxPendingInputBridgeCounts[0],
        "maximumPendingInputBridge1" : maxPendingInputBridges[1],
        "maximumPendingInputBridgeCount1" : maxPendingInputBridgeCounts[1],
        "maximumPendingInputBridge2" : maxPendingInputBridges[2],
        "maximumPendingInputBridgeCount2" : maxPendingInputBridgeCounts[2],
        "maximumPendingInputMerger0" : maxPendingInputMergers[0],
        "maximumPendingInputMergerCount0" : maxPendingInputMergerCounts[0],
        "maximumPendingInputMerger1" : maxPendingInputMergers[1],
        "maximumPendingInputMergerCount1" : maxPendingInputMergerCounts[1],
        "maximumPendingInputMerger2" : maxPendingInputMergers[2],
        "maximumPendingInputMergerCount2" : maxPendingInputMergerCounts[2],
        "minFreeBufferUtilSrc" : minFreeBufferUtil[0],
        "minFreeBufferProcSrc" : minFreeBufferProc[0],
        "minFreeBufferUtilPrc" : minFreeBufferUtil[1],
        "minFreeBufferProcPrc" : minFreeBufferProc[1],
        "minFreeBufferUtilOth" : minFreeBufferUtil[2],
        "minFreeBufferProcOth" : minFreeBufferProc[2],
        "currentEventLifetime" : str(totalMinEventLifetime/1000)+" / "+currentAvgEventLifetime+" / "+str(totalMaxEventLifetime/1000),
        "maxOutOfOrderOrbits"  : hex(maxOutOfOrderOrbits)+" / "+ str((maxOutOfOrderOrbits*1000*1492)>>24)+ " ms"
        }
    if aliceHLT and len(detList)>1:
        for det in detList:
            results["results"][det+"avgAnnounceCount"] = detTotalAnnounceCount[det]
            results["results"][det+"avgAnnounceSize"] = detTotalAnnounceSize[det]/1024
            results["results"][det+"avgCurrentAnnounceRate"] = detAvgCurrentAnnounceRates[det]
            results["results"][det+"avgTotalAnnounceRate"] = detAvgTotalAnnounceRates[det]
    if lostEvents!=None:
        for i in range(0,3):
            if i<len(lostEvents):
                results["results"]["maximumLostEvents%d" % i] = lostEvents[i]["parent"]+" -> "+lostEvents[i]["child"]
                results["results"]["maximumLostEventCnt%d" % i] = "%u of %u" % ( lostEvents[i]["parentEvents"]-lostEvents[i]["childEvents"], lostEvents[i]["parentEvents"] )
            else:
                results["results"]["maximumLostEvents%d" % i] = "---"
                results["results"]["maximumLostEventCnt%d" % i] = 0
    results["descriptions"] = {
        "runNumber" : "Run Number",
        "maxAnnounceCount" : "Received event count (max. from all sources)",
        "minAnnounceCount" : "Received event count (min. from all sources)",
        "avgAnnounceCount" : "Received event count (avg. over all sources)",
        "totalAnnounceSize" : "Received data size (total sum over all sources) (kB)",
        "minCurrentAnnounceRate" : "Minimum received event rate during last second (Hz)",
        "avgCurrentAnnounceRate" : "Average received event rate during last second (Hz)",
        "maxCurrentAnnounceRate" : "Maximum received event rate during last second (Hz)",
        "minTotalAnnounceRate" : "Minimum received event rate since start of run (Hz)",
        "avgTotalAnnounceRate" : "Average received event rate since start of run (Hz)",
        "maxTotalAnnounceRate" : "Maximum received event rate since start of run (Hz)",
        "maxEventsInChain" : "Event count currently processed in chain (max. from all sources)",
        "minCurrentProcessRate" : "Minimum processing rate during last second (Hz)",
        "avgCurrentProcessRate" : "Average processing rate during last second (Hz)",
        "maxCurrentProcessRate" : "Maximum processing rate during last second (Hz)",
        "minAvgProcessRate" : "Minimum processing rate since start of run (Hz)",
        "avgAvgProcessRate" : "Average processing rate since start of run (Hz)",
        "maxAvgProcessRate" : "Maximum processing rate since start of run (Hz)",
        "maxRoundtripProcessedCount" : "Finished events (roundtrip finished, max. from all sources)",
        "maxFinishedCount" : "Event count passed to end of chain (max. from all sinks)",
        "totalFinishedSize" : "Data size passed to end of chain (total sum over all sinks) (kB)",
        "totalFinishedCount_OUT" : "Event count passed to HLT output (sum over all HLT output sinks)",
        "totalFinishedSize_OUT" : "Data size passed to HLT output (sum over all HLT output sinks) (kB)",
        "currentReceiveRate_OUT" : "Total rate received by HLT output during last second (Hz)",
        "totalReceiveRate_OUT" : "Total rate received by HLT output since start of run (Hz)",
        "currentSendRate_OUT" : "Total rate sent by HLT output to DAQ during last second (Hz)",
        "totalSendRate_OUT" : "Total rate sent by HLT output to DAQ since start of run (Hz)",
        "sumPending_OUT" : "Sum of pending HLT output events (sum over all HLT output sinks)",
        "maxSinglePending_OUT" : "Single largest queue of pending HLT output events.",
        "runningSourcesCnt" : "Running source components",
        "runningProcessorsCnt" : "Running processing components",
        "runningSinksCnt" : "Running sink components",
        "runningDataFlowProcsCnt" : "Running data flow components",
        "deadProcsCnt" : "Dead components",
        "maximumPendingInputComponent0" : "Component w. largest pending input events",
        "maximumPendingInputComponentCount0" : "Largest pending input event count",
        "maximumPendingInputComponent1" : "Component w. second largest pending input events",
        "maximumPendingInputComponentCount1" : "Second largest pending input event count",
        "maximumPendingInputComponent2" : "Component w. third largest pending input events",
        "maximumPendingInputComponentCount2" : "Third largest pending input event count",
        "maximumPendingInputBridge0" : "Bridge w. largest pending input events",
        "maximumPendingInputBridgeCount0" : "Largest bridge pending input event count",
        "maximumPendingInputBridge1" : "Bridge w. second largest pending input events",
        "maximumPendingInputBridgeCount1" : "Second largest bridge pending input event count",
        "maximumPendingInputBridge2" : "Bridge w. third largest pending input events",
        "maximumPendingInputBridgeCount2" : "Third largest bridge pending input event count",
        "maximumPendingInputMerger0" : "Merger w. largest pending input events",
        "maximumPendingInputMergerCount0" : "Largest merger pending input event count",
        "maximumPendingInputMerger1" : "Merger w. second largest pending input events",
        "maximumPendingInputMergerCount1" : "Second largest merger pending input event count",
        "maximumPendingInputMerger2" : "Merger w. third largest pending input events",
        "maximumPendingInputMergerCount2" : "Third largest merger pending input event count",
        "maximumLostEvents0" : "Component pair with largest event loss",
        "maximumLostEventCnt0" : "Largest event loss",
        "maximumLostEvents1" : "Component pair with second largest event loss",
        "maximumLostEventCnt1" : "Second largest event loss",
        "maximumLostEvents2" : "Component pair with third largest event loss",
        "maximumLostEventCnt2" : "Third largest event loss",
        "minFreeBufferUtilSrc" : "Min free buffer (source)",
        "minFreeBufferProcSrc" : "Src /w min free buffer",
        "minFreeBufferUtilPrc" : "Min free buffer (process)",
        "minFreeBufferProcPrc" : "Prc /w min free buffer",
        "minFreeBufferUtilOth" : "Min free buffer (other)",
        "minFreeBufferProcOth" : "Oth /w min free buffer",
        "currentEventLifetime" : "Event lifetimes (min./cur-avg./max. from all sinks) (ms)",
        "maxOutOfOrderOrbits"  : "Maximum Out-Of-Order Orbit ID difference (max. from all sinks)",
        }
    if aliceHLT and len(detList)>1:
        for det in detList:
            results["descriptions"][det+"avgAnnounceCount"] = det+" received event count (avg. over all "+det+" sources)"
            results["descriptions"][det+"avgAnnounceSize"] = det+" received data size (total sum over all "+det+" sources) (kB)"
            results["descriptions"][det+"avgCurrentAnnounceRate"] = det+" average received event rate during last second (Hz)"
            results["descriptions"][det+"avgTotalAnnounceRate"] = det+" average received event rate since start of run (Hz)"
    results["order"] = [
        "runNumber",
        "runningSourcesCnt",
        "runningProcessorsCnt",
        "runningSinksCnt",
        "runningDataFlowProcsCnt",
        "deadProcsCnt",
        "maxAnnounceCount",
        "avgAnnounceCount",
        "minAnnounceCount",
        "totalAnnounceSize",
        ]
    # if not aliceHLT or len(detList)<=0:
    if 1:
        for oe in [
            "minCurrentAnnounceRate",
            "avgCurrentAnnounceRate",
            "maxCurrentAnnounceRate",
            "minTotalAnnounceRate",
            "avgTotalAnnounceRate",
            "maxTotalAnnounceRate",
            ]:
            results["order"].append( oe )
    if aliceHLT and len(detList)>1:
        sortedDetList = list(detList)
        sortedDetList.sort()
        for det in sortedDetList:
            results["order"].append( det+"avgAnnounceCount" )
            results["order"].append( det+"avgAnnounceSize" )
            results["order"].append( det+"avgCurrentAnnounceRate" )
            results["order"].append( det+"avgTotalAnnounceRate" )
    #else:
    for oe in [
        "maxEventsInChain",
        "minCurrentProcessRate",
        "avgCurrentProcessRate",
        "maxCurrentProcessRate",
        "minAvgProcessRate",
        "avgAvgProcessRate",
        "maxAvgProcessRate",
        "maxRoundtripProcessedCount",
        "maxFinishedCount",
        "totalFinishedSize",
        "currentEventLifetime",
        "maxOutOfOrderOrbits",
        ]:
        results["order"].append( oe )
    if aliceHLT:
        results["order"].append( "totalFinishedCount_OUT" )
        results["order"].append( "totalFinishedSize_OUT" )
        results["order"].append( "sumPending_OUT" )
        results["order"].append( "maxSinglePending_OUT" )
        results["order"].append( "currentReceiveRate_OUT" )
        results["order"].append( "totalReceiveRate_OUT" )
        results["order"].append( "currentSendRate_OUT" )
        results["order"].append( "totalSendRate_OUT" )
    results["order"].append( "maximumPendingInputComponent0" )
    results["order"].append( "maximumPendingInputComponentCount0" )
    results["order"].append( "maximumPendingInputComponent1" )
    results["order"].append( "maximumPendingInputComponentCount1" )
    results["order"].append( "maximumPendingInputComponent2" )
    results["order"].append( "maximumPendingInputComponentCount2" )
    results["order"].append( "maximumPendingInputBridge0" )
    results["order"].append( "maximumPendingInputBridgeCount0" )
    results["order"].append( "maximumPendingInputBridge1" )
    results["order"].append( "maximumPendingInputBridgeCount1" )
    results["order"].append( "maximumPendingInputBridge2" )
    results["order"].append( "maximumPendingInputBridgeCount2" )
    results["order"].append( "maximumPendingInputMerger0" )
    results["order"].append( "maximumPendingInputMergerCount0" )
    results["order"].append( "maximumPendingInputMerger1" )
    results["order"].append( "maximumPendingInputMergerCount1" )
    results["order"].append( "maximumPendingInputMerger2" )
    results["order"].append( "maximumPendingInputMergerCount2" )
    results["order"].append( "minFreeBufferUtilSrc" )
    results["order"].append( "minFreeBufferProcSrc" )
    results["order"].append( "minFreeBufferUtilPrc" )
    results["order"].append( "minFreeBufferProcPrc" )
    results["order"].append( "minFreeBufferUtilOth" )
    results["order"].append( "minFreeBufferProcOth" )
    if lostEvents!=None:
        for i in range(0,3):
            # if i<len(lostEvents):
            results["order"].append( "maximumLostEvents%d" % i )
            results["order"].append( "maximumLostEventCnt%d" % i )
    
    return results


def ReadProcessListFile( processListFileName ):
    try:
        infile = open( processListFileName, "r" )
    except IOError,e:
        print "Error opening process list input file '"+processListFileName+"': "+str(e)
        return {}
    publishers = {}
    subscribers = {}
    modulos = {}
    line = infile.readline()
    oldproc = ""
    while line!="":
        if line=="\n":
            if oldproc!="":
                toks = ":".join(oldproc.split(":")[1:]).split()
                if toks.count("-name")>0 and (toks.count("-subscribe")>0 or toks.count("-publisher")>0 or (oldproc[:10]=="      pbh:" and toks.count("-msg")>0) or (oldproc[:10]=="      sbh:" and toks.count("-msg")>0) ):
                    name = toks[toks.index("-name")+1]
                    subs = set()
                    pubs = set()
                    tmp=toks[:]
                    while tmp.count("-subscribe")>0:
                        subs.add(tmp[tmp.index("-subscribe")+1])
                        tmp = tmp[tmp.index("-subscribe")+1:]
                    if oldproc[:10]=="      pbh:":
                        tmp=toks[:]
                        while tmp.count("-msg")>0:
                            subs.add(tmp[tmp.index("-msg")+2])
                            tmp = tmp[tmp.index("-msg")+2:]
                    tmp=toks[:]
                    while tmp.count("-publisher")>0:
                        pubs.add(tmp[tmp.index("-publisher")+1])
                        tmp = tmp[tmp.index("-publisher")+1:]
                    if oldproc[:10]=="      sbh:":
                        tmp=toks[:]
                        while tmp.count("-msg")>0:
                            pubs.add(tmp[tmp.index("-msg")+1])
                            tmp = tmp[tmp.index("-msg")+1:]
                    tmp=toks[:]
                    while tmp.count("-eventmodulo" )>0:
                        modulos[name] = int(tmp[tmp.index("-eventmodulo")+1])
                        tmp = tmp[tmp.index("-eventmodulo")+1:]
                            
                    publishers[name] = pubs
                    subscribers[name] = subs
            oldproc = ""
        if line[:6]=="      ":
            oldproc = line[:-1]
        elif oldproc!="":
            oldproc = oldproc+" "+line[:-1]
        
        line = infile.readline()
    infile.close()
    children = {}
    parents = {}
    names = set(publishers.keys()+subscribers.keys())
    for name in names:
        children[name] = set()
        parents[name] = set()
    for name_p in names:
        pubs = publishers[name_p]
        for name_c in names:
            if name_p==name_c:
                continue
            subs = subscribers[name_c]
            for p in pubs:
                if p in subs:
                    children[name_p].add( name_c )
                    parents[name_c].add( name_p )
    
    return { 
        "publishers" : publishers, 
        "subscribers" : subscribers,
        "parents" : parents,
        "children" : children,
        "modulos" : modulos,
        }

def CheckLostEvents( slaveChildData, parents, publishers, modulos ):
    proc2Slave = {}
    for slave in slaveChildData.keys():
        if slave=="RunNumber":
            continue
        procList = slaveChildData[slave].keys()
        for proc in procList:
            if proc=="RunNumber":
                continue
            #print "proc:",proc,slave,parents[proc]
            proc2Slave[proc] = slave
    lostEvents = []
    for name in parents.keys():
        if not proc2Slave.has_key(name):
            continue
        slave = proc2Slave[name]
        statusDict = slaveChildData[slave][name]
        if not statusDict.has_key("PendingOutputEventCount") or not statusDict.has_key("PendingInputEventCount"):
            continue
        childEvents = int(statusDict["PendingOutputEventCount"])+int(statusDict["PendingInputEventCount"])
        pars = parents[name]
        parentEvents = 0
        pendingOutParentEvents = 0
        parentPublisherCount = 0
        maxEventParent = None
        for parent in pars:
            if not proc2Slave.has_key(parent):
                continue
            parentSlave = proc2Slave[parent]
            parentStatusDict = slaveChildData[parentSlave][parent]
            if not parentStatusDict.has_key("PendingOutputEventCount"):
                continue
            thisParentEvents = int(parentStatusDict["PendingOutputEventCount"])
            if parentStatusDict["State"].split(":")[0] == "EventScatterer":
                thisParentEvents /= len(publishers[parent])
            if modulos.has_key(name):
                thisParentEvents /= modulos[name]
            if parentEvents<thisParentEvents:
                parentEvents = thisParentEvents
                maxEventParent = parent
                pendingOutParentEvents = int(parentStatusDict["PendingOutputEventCount"])
                parentPublisherCount = len(publishers[parent])
                
        if childEvents<parentEvents*0.8:
            lostEvents.append( { 
                    "child" : name,
                    "parent" : maxEventParent,
                    "childEvents" : childEvents,
                    "parentEvents" : parentEvents,
                    "allParents" : pars,
                    "childPendingInput" : int(statusDict["PendingInputEventCount"]),
                    "childPendingOutput" : int(statusDict["PendingOutputEventCount"]),
                    "parentPendingOutput" : pendingOutParentEvents,
                    "parentPublisherCount" : parentPublisherCount,
                    } )
    return lostEvents

logMessageRepeatCnt = {}
logMessageRepeatBase = 10
def LogMessage( logMsgBinary, msg, hash ):
    if not logMessageRepeatCnt.has_key(hash):
        logMessageRepeatCnt[hash] = 0
    base=1
    while logMessageRepeatCnt[hash] > base*logMessageRepeatBase:
        base *= logMessageRepeatBase
    if logMessageRepeatCnt[hash]>=base-1 and logMessageRepeatCnt[hash]<=base+logMessageRepeatBase-1:
        os.system( logMsgBinary+" \""+msg+" (%u times)\"" % logMessageRepeatCnt[hash] )
    logMessageRepeatCnt[hash] += 1
    
                   
class rrdLogger:
    """Class rrdLogger
    
    Some little helper class to log selected values from the status scan
    to an rrd database for later analysis/plotting...

    Arguments:
    directory - base directory where all rrd files are stored
    
    """
    directoy = ""
    def __init__(self, directory):
        self.directory = directory
        # RRA Options: step 1s, save LAST value, 36k rows = 10h
        rra_options = 'RRA:LAST:0.5:1:36000'
        # check that directory already exists, if not create
        if (os.path.exists(self.directory) == False):
            os.mkdir(self.directoy,mode=0755)

        rrdtool.create(self.rrdFile('EventsInChain.rrd'),'--start','-10s',
            '--step', '1s', 'DS:EventsInChain:GAUGE:10:U:U', rra_options)

        rrdtool.create(self.rrdFile('CurrentAnnounceRate.rrd'),'--start','-10s',
            '--step', '1s', 'DS:CurrentAnnounceRate:GAUGE:10:U:U', rra_options)

        rrdtool.create(self.rrdFile('CurrentSendRate.rrd'),'--start','-10s',
            '--step', '1s', 'DS:CurrentSendRate:GAUGE:10:U:U', rra_options)

        #if (os.path.isfile(self.rrdFile('EventsInChain.rrd')) == False):
        #    rrdtool.create(self.rrdFile('EventsInChain.rrd'),'--start','-10s', '--step', '1s', ,'DS:EventsInChain:GAUGE:10:U:U', 'RRA:AVERAGE:0.5:1:3600','RRA:AVERAGE:0.5:10:25920', 'RRA:MAX:0.5:10:25920','RRA:MIN:0.5:10:25920')
        #if (os.path.isfile(self.rrdFile('CurrentAnnounceRate.rrd')) == False):
        #    rrdtool.create(self.rrdFile('CurrentAnnounceRate.rrd'),'--start','-10s','--step','1s','DS:CurrentAnnounceRate:GAUGE:10:U:U','RRA:AVERAGE:0.5:1:3600','RRA:AVERAGE:0.5:10:8640','RRA:MAX:0.5:10:25920','RRA:MIN:0.5:10:25920')
        #if (os.path.isfile(self.rrdFile('CurrentSendRate.rrd')) == False):
        #    rrdtool.create(self.rrdFile('CurrentSendRate.rrd'),'--start','-10s','--step','1s','DS:CurrentSendRate:GAUGE:10:U:U','RRA:AVERAGE:0.5:1:3600','RRA:AVERAGE:0.5:10:8640','RRA:MAX:0.5:10:25920','RRA:MIN:0.5:10:25920')
    def rrdFile(self, name):
        """ Return full path of the rrd database file <name>. """
        return os.path.join(self.directory, name)
    def update(self, rrdName, value):
        """ Update rrd database file <rrdName> with <value>. """
        rrdtool.update(self.rrdFile(rrdName),'N:%s'%(value))    
    
if __name__ == "__main__":
    args = sys.argv[1:]
    
    dump = False
    dump2 = False
    check = False
    statistics = False
    state = False
    address=None
    target = None
    targetConfig=None
    targetInputFiles=[]
    verbose=False
    sortOrder="id"
    loop=False
    color=False
    maxEventsWarning = 0
    maxEventsWarningState = 0
    quitOnRunChange = 0
    lastRunNumber = 0
    forceQuit = 0
    interval = 1.0
    outputFileName = None
    outputFile = None
    processListFile = None
    checkLostEvents = False
    lostEventRepeatLimit = 1
    lostEventMinCnt = 1
    logMsgBinary = None
    detailedDetectors = True
    rrd = None
    
    
    while len(args)>0:
        if args[0]=="-dump":
            dump=True
            args=args[1:]
        if args[0]=="-dump2":
            dump2=True
            state=False
            statistics=True
            args=args[1:]
        elif args[0]=="-check":
            check=True
            args=args[1:]
        elif args[0]=="-stats":
            statistics=True
            args=args[1:]
        elif args[0]=="-state":
            state=True
            args=args[1:]
        elif args[0]=="-v":
            verbose=True
            args=args[1:]
        elif args[0]=="-color":
            color=True
            args=args[1:]
        elif args[0]=="-quit-on-run-change":
            quitOnRunChange = 1
            args=args[1:]
        elif args[0]=="-max-events-warning":
            if len(args) < 2:
                UsageErrorExit( "Missing -max-events-warning parameter" )
            maxEventsWarning = int(args[1])
            args=args[2:]
        elif args[0]=="-loop":
            loop=True
            args=args[1:]
        elif args[0]=="-loopinterval":
            if len(args)<2:
                UsageErrorExit( "Missing -loopinterval parameter" )
            try:
                interval = float(args[1])
            except ValueError:
                UsageErrorExit( "Error converting -loopinterval parameter (must be a floating point number)" )
            args=args[2:]
        elif args[0]=="-alicehlt":
            aliceHLT=True
            args=args[1:]
        elif args[0]=="-address":
            if len(args)<2:
                UsageErrorExit( "Missing -address parameter" )
            address = args[1]
            args=args[2:]
        elif args[0]=="-control":
            if len(args)<2:
                UsageErrorExit( "Missing -control parameter" )
            target = args[1]
            args=args[2:]
        elif args[0]=="-config":
            if len(args)<2:
                UsageErrorExit( "Missing -config parameter" )
            targetConfig = args[1]
            args=args[2:]
        elif args[0]=="-inputfile":
            if len(args)<2:
                UsageErrorExit( "Missing -inputfile parameter" )
            targetInputFiles.append( args[1] )
            args=args[2:]
        elif args[0]=="-inputfilelist":
            if len(args)<2:
                UsageErrorExit( "Missing -inputfilelist parameter" )
            targetInputFiles += glob.glob( args[1] )
            args=args[2:]
        elif args[0]=="-sort":
            if len(args)<2:
                UsageErrorExit( "Missing -sort parameter" )
            if args[1]!="id" and args[1]!="key":
                UsageErrorExit( "Unknown -sort parameter ('id' or 'key')" )
            sortOrder = args[1]
            args=args[2:]
        elif args[0]=="-outputfile":
            if len(args)<2:
                UsageErrorExit( "Missing -outputfile parameter" )
            outputFileName = args[1]
            outputFile = open( outputFileName, "w" )
            args=args[2:]
        elif args[0]=="-processlist":
            processListFile = args[1]
            args=args[2:]
        elif args[0]=="-checklostevents":
            checkLostEvents = True
            args=args[1:]
        elif args[0]=="-losteventrepeatlimit":
            lostEventRepeatLimit = int(args[1])
            args=args[2:]
        elif args[0]=="-losteventmincnt":
            lostEventMinCnt = int(args[1])
            args=args[2:]
        elif args[0]=="-checkmsglogbinary":
            logMsgBinary = args[1]
            args=args[2:]
        elif args[0]=="-nodetaileddetectors":
            detailedDetectors = False
            args=args[1:]
        elif args[0] == "-rrd-directory":
            if len(args) < 2:
                UsageErrorExit("Missing -rrd-directory parameter")
            # makes only sense if we are looping and producing statistics as wel
            statistics = True
            loop = True
            rrd = rrdLogger(args[1])
            args = args[2:]
        else:
            UsageErrorExit( "Unknown parameter "+args[0] )
    
    if address==None and len(targetInputFiles)<=0:
        UsageErrorExit( "Missing local address specification (-address)" )
    if target==None and targetConfig==None and len(targetInputFiles)<=0:
        UsageErrorExit( "Missing target specification (-control, -config, or -inputfile" )
    if (target!=None and targetConfig!=None) or (target!=None and len(targetInputFiles)>0) or (len(targetInputFiles)>0 and targetConfig!=None) or (target!=None and targetConfig!=None and len(targetInputFiles)>0):
        UsageErrorExit( "Too many target specifications (Only one of -control, -config, or -inputfile)" )
    #if len(targetInputFiles)==1 and loop:
    #    UsageErrorExit( "-inputfile and -loop cannot be combined" )
    #if len(targetInputFiles)>1 and not loop:
    #    UsageErrorExit( "-loop must be given for multiple -inputfile options" )
    if len(targetInputFiles)>0 and loop:
        UsageErrorExit( "-inputfile and -loop cannot be combined" )
    if checkLostEvents and processListFile==None:
        UsageErrorExit( "-checklostevents requires -processlist" )

    if dump2==False and dump==False and check==False and statistics==False and checkLostEvents==False:
        dump=True
    
    if loop and not rrd:
        import curses
        screen = curses.initscr()
        if color:
            curses.start_color()
            if curses.has_colors():
                if curses.can_change_color():
                    curses.init_color(0, 0, 0, 0)
                curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
                curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)
                curses.init_pair(3, curses.COLOR_YELLOW, curses.COLOR_BLACK)
                curses.init_pair(4, curses.COLOR_CYAN, curses.COLOR_BLACK)
                curses.init_pair(5, curses.COLOR_MAGENTA, curses.COLOR_BLACK)
        curses.nonl()
        curses.noecho()
        curses.cbreak()
        screen.nodelay(1)

    #if outputFile!=None:
    #    titleWritten = False
    lastOutputFilesEntries = 0   

    if len(targetInputFiles)<=0:
        try:
            if verbose:
                print "Initializing TM interface..."
            TMControlInterface.Init( address )
            if verbose:
                print "Done"
        except:
            if loop and not rrd:
                screen.nodelay(0)
                curses.echo()
                curses.nocbreak()
                curses.endwin()
            if outputFile!=None:
                outputFile.close()
            raise

    processListData = {}
    processListDataRead = 0
    lostEventCnt = {}

    try:
        if targetConfig!=None:
            if verbose:
                print "Determining target address from config file "+targetConfig+"..."
            target = GetChildAddressFromConfigFile( targetConfig )
            if verbose:
                print "Done"
    
        doLoop=True
        inputFileNdx=0
        while doLoop:
            s=time.time()
            if processListFile!=None:
                processListFileStats = os.stat( processListFile )
                if processListFileStats.st_ctime > processListFileStats.st_mtime:
                    processListFileTimestamp = processListFileStats.st_ctime
                else:
                    processListFileTimestamp = processListFileStats.st_mtime
                if processListDataRead<processListFileTimestamp:
                    processListDataRead = time.time()
                    start = time.time()
                    processData = ReadProcessListFile( processListFile )
                    stop = time.time()
                    if verbose:
                        print "ReadProcessListFile time:",stop-start,"s"
                    # print "process data:",processData
                    # for k in processData.keys():
                    #     print k,":"
                    #     for l in processData[k].keys():
                    #         print "    ",k,":",l,":",processData[k][l]
                    #     print

                    for k in processData["parents"]:
                        if not lostEventCnt.has_key(k):
                            lostEventCnt[k] = 0
            if loop and not rrd:
                key=screen.getch()
                if key==ord("q") or key==ord("Q"):
                    break
            if len(targetInputFiles)<=0:
                slaveChildData = GetData( target )
            else:
                slaveChildData = GetFileData( targetInputFiles[inputFileNdx] )
    
            if loop and not rrd:
                screen.clear()

            if checkLostEvents or (statistics and processListFile!=None):
                start = time.time()
                lostEventsTmp = CheckLostEvents( slaveChildData, processData["parents"], processData["publishers"], processData["modulos"] )
                stop = time.time()
                #print "lostEventsTmp:",lostEventsTmp
                #print "CheckLostEvents time:",stop-start,"s"
                lostEvents = []
                lostProcs = set()
                for ev in lostEventsTmp:
                    lostEventCnt[ev["child"]] += 1
                    lostProcs.add( ev["child"] )
                    #print lostEventCnt[ev["child"]], ev
                    if lostEventCnt[ev["child"]]>=lostEventRepeatLimit and ev["parentEvents"]-ev["childEvents"]>=lostEventMinCnt:
                        lostEvents.append( ev )
                lostEvents.sort( key=lambda x: (x["parentEvents"]-x["childEvents"]), reverse=True )

                for proc in (set(processData["parents"])-lostProcs):
                    lostEventCnt[proc] = 0
            else:
                lostEvents = None

            if statistics:
                results = Statistics( slaveChildData, lostEvents )
                maxDescrLen = 0
                for k in results["order"]:
                    if maxDescrLen<len(results["descriptions"][k]):
                        maxDescrLen = len(results["descriptions"][k])
            if (state or statistics) and outputFile!=None:
                outputFileEntries = 1 # Time
                if state:
                    outputFileEntries += 1 # State 
                if statistics:
                    outputFileEntries += len(results["order"])
                if outputFileEntries!=lastOutputFilesEntries:
                    outputFile.write( "# Time" )
                    if state:
                        outputFile.write( " --- State" )
                    if statistics:
                        for k in results["order"]:
                            outputFile.write( " --- "+results["descriptions"][k] )
                    outputFile.write( "\n" )
                    lastOutputFilesEntries = outputFileEntries
                    fileOutStart = time.time()
                #fileOutNow = time.time()-fileOutStart
                fileOutNow = time.time()
                outputFile.write( str(fileOutNow) )
                        
            if state and not statistics:
                maxDescrLen = len("Global State")
                
            if state:
                if target!=None:
                    state = GetTopLevelState( target )
                elif len(targetInputFiles)>0:
                    state = GetTopLevelStateFromFile( targetInputFile[inputFileIndex] )
                else:
                    state = "UNKNOWN"
                fill=" "*(maxDescrLen-len("Global State")+5)
                if outputFile!=None:
                    outputFile.write( " "+state )
                state = "Global State:"+fill+state
                if loop:
                    try:
                        if not rrd:
                            screen.addstr( state+"\n" )
                    except:
                        pass
                else:
                    print state
                    

            if statistics and not dump2:
    ##            results = Statistics( slaveChildData )
    ##            maxDescrLen = 0
    ##            for k in results["order"]:
    ##                if maxDescrLen<len(results["descriptions"][k]):
    ##                    maxDescrLen = len(results["descriptions"][k])
                for k in results["order"]:
                    fill=" "*(maxDescrLen-len(results["descriptions"][k])+5)
                    stat = results["descriptions"][k]+":"+fill+str(results["results"][k])
                    if loop:
                        try:
                            if not rrd:
                                if not color or not curses.has_colors():
                                    screen.addstr( stat+"\n")
                                elif k == "maxEventsInChain":
                                    screen.addstr( stat+"\n", curses.color_pair(3))
                                elif "maximumPendingInputComponent" in k:
                                    screen.addstr( stat+"\n", curses.color_pair(2))
                                elif "maximumPendingInputBridge" in k:
                                    screen.addstr( stat+"\n", curses.color_pair(3))
                                elif "maximumPendingInputMerger" in k:
                                    screen.addstr( stat+"\n", curses.color_pair(4))
                                elif "Current" in k and "Rate" in k:
                                    screen.addstr( stat+"\n", curses.color_pair(4))
                                elif "_OUT" in k:
                                    screen.addstr( stat+"\n", curses.color_pair(5))
                                else:
                                    screen.addstr( stat+"\n")
                        except:
                            pass
                    else:
                        print stat
                if outputFile!=None:
                    for k in results["order"]:
                        if k in ( "maximumLostEvents0", "maximumLostEvents1", "maximumLostEvents2" ):
                            res = "".join( results["results"][k].split(" ") )
                        elif k in ( "maximumLostEventCnt0", "maximumLostEventCnt1", "maximumLostEventCnt2" ) and type(results["results"][k])==type(""):
                            res = results["results"][k].split(" ")[0]
                        else:
                            res = str(results["results"][k])
                        outputFile.write( " "+res )
                    outputFile.write( "\n" )

            if check:
                for r in Check( slaveChildData ):
                    if loop:
                        try:
                            if not rrd:
                                screen.addstr( r+"\n" )
                        except:
                            pass
                    else:
                        print r
                    if logMsgBinary!=None:
                        LogMessage( logMsgBinary, r, "Check"+r[:64] )
            if checkLostEvents:
                for ev in lostEvents:
                    # r = "%s has lost events %u times (%u of %u events, relevant parent '%s') (%u pending input # child, %u pending output # child, %u pending output # parent, %u # parent publishers)" % ( ev["child"], lostEventCnt[ev["child"]], ev["parentEvents"]-ev["childEvents"], ev["parentEvents"], ev["parent"], ev["childPendingInput"], ev["childPendingOutput"], ev["parentPendingOutput"], ev["parentPublisherCount"] )
                    r = "%u of %u events  lost %u times between %s -> %s (%u pending input # child, %u pending output # child, %u pending output # parent, %u # parent publishers)" % ( ev["parentEvents"]-ev["childEvents"], ev["parentEvents"], lostEventCnt[ev["child"]], ev["parent"], ev["child"], ev["childPendingInput"], ev["childPendingOutput"], ev["parentPendingOutput"], ev["parentPublisherCount"] )
                    if loop:
                        try:
                            if not rrd:
                                screen.addstr( r+"\n" )
                        except:
                            pass
                    else:
                        print r
                    if logMsgBinary!=None:
                        LogMessage( logMsgBinary, r, "Lost-"+ev["parent"]+"-"+ev["child"] )

                    
            if dump:
                if loop:
                    for d in Dump( slaveChildData, sortOrder ):
                        try:
                            if not rrd:
                                screen.addstr( d+"\n" )
                        except:
                            pass
                else:
                    Print( slaveChildData, sortOrder )
                if outputFile!=None:
                    for d in Dump( slaveChildData, sortOrder ):
                        outputFile.write( str(fileOutNow)+" ComponentData "+d+"\n" )
                        
            if rrd:
                # fill rrd database - we rely on results to be already properly filled
                # by the statistics function
                rrd.update("EventsInChain.rrd", results["results"]["maxEventsInChain"])
                rrd.update("CurrentAnnounceRate.rrd", results["results"]["avgCurrentAnnounceRate"])
                rrd.update("CurrentSendRate.rrd", results["results"]["currentSendRate_OUT"])
                        
            e=time.time()
            if not loop and not dump2:
                print "Time: ",e-s
    
            if not loop:
                inputFileNdx += 1
                if inputFileNdx>=len(targetInputFiles):
                    doLoop=False
                #doLoop=False
            else:
                if not rrd:
                    screen.refresh()
                    slpInt = interval
                    while slpInt>0:
                        if slpInt>0.5:
                            t=0.5
                        else:
                            t=slpInt
                        slpInt -= t
                        time.sleep( t )
                        key=screen.getch()
                        if key==ord("q") or key==ord("Q"):
                            break
                else:
                    time.sleep(interval)
                if not rrd and ( key==ord("q") or key==ord("Q") or forceQuit ):
                    break
    except Exception,e:
        print "Exception caught:",e
    finally:
        if loop and not rrd:
            screen.nodelay(0)
            curses.echo()
            curses.nocbreak()
            curses.endwin()

        for addr in connectAddresses:
            try:
                TMControlInterface.Disconnect( addr )
            except Exception,e:
                print "Exception caught (2):",e

        TMControlInterface.Terminate()
        if outputFile!=None:
            outputFile.close()
    if forceQuit:
        sys.exit(102)
    else:
        sys.exit(0)
