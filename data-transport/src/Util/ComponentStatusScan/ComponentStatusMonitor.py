#!/usr/bin/python

import TMControlInterface
import argparse
import os
import sys
import time
import rrdtool
import traceback
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import json
import threading
import copy
import re

API_VERSION = 1

OUTPUT_BUFFER_ERROR_THRESHOLD_PERCENT = 10
PENDING_INPUT_THRESHOLD = 100
PENDING_INPUT_DIFF_FACTOR = 3
NUMBER_OF_LIST_ENTRIES = 10
MAX_EVENTS_LOW_WATERMARK = 500
INFOLOGGER_MSG_FACILITY = "ComponentStatusMonitor"

# number of iterations a component has to be stuck in order to trigger a warning to infoLogger
STUCK_COMPONENT_WARNING_ITERATIONS = 2

# trim sequences:
# keep latest [KEEP_SAMPLES_FULL] samples from every captured timestamp,
# then [KEEP_SAMPLES_INCREMENT] samples from only every second timestamp,
# then [KEEP_SAMPLES_INCREMENT] samples from only every forth timestamp
# ...and so on
KEEP_SAMPLES_FULL = 300
KEEP_SAMPLES_INCREMENT = 150


class InfoBrowserLogger(object):
    __binary__ = "/usr/bin/log"
    __dolog__ = False
    __severities__ = ['i', 'm', 'w', 'e', 'f']
    def __init__(self, binary=None):
        if binary:
            self.__binary__ = binary
        if os.path.isfile(self.__binary__) and os.access(self.__binary__, os.X_OK):
            self.__dolog__ = True
        else:
            sys.stderr.write("WARNING: Log binary '%s' is either not available or " \
                             "not executable - not writing anything to InfoLogger!\n" % \
                             (self.__binary__))

    def log(self, msg, severity="i", facility=INFOLOGGER_MSG_FACILITY):
        if not self.__dolog__:
            return
        if not severity in self.__severities__:
            sys.stderr.write("ERROR: invalid log severity '%s'\n" % (severity))
            return
        cmd = ""
        if facility:
            cmd = "DATE_FACILITY=\"%s\" " % (facility)
        cmd += "%s -%s \"%s\"" % (self.__binary__, severity, msg)
        #print cmd
        os.system(cmd)


class TMReader(object):
    __connected_addrs__ = []
    __target__ = ""
    slaveChildData = {}
    def __init__(self, address, target):
        TMControlInterface.Init(address)
        self.__target__ = target

    def disconnect(self):
        for addr in self.__connected_addrs__:
            try:
                TMControlInterface.Disconnect(addr)
            except:
                pass
        self.__connected_addrs__ = []

    def cleanup(self):
        self.disconnect()
        try:
            TMControlInterface.Terminate()
        except:
            pass

    def StatusData2Dict(self, statusData):
        d = {}
        for c in statusData.split(":"):
            cd = c.split("=")
            if len(cd) == 2:
                d[cd[0]] = cd[1]
        return d

    def query_top_level_state(self):
        try:
            topLevelState = TMControlInterface.QueryState(self.__target__)
            self.slaveChildData["master"] = {"state" : topLevelState}
        except SystemError:
            self.disconnect()
            raise SystemError("TMControlInterface - failed to query top level state from %s" % \
                              (self.__target__))

    def get_data(self):
        # Get Slave List
        TMAddresses = {}
        slaves = []
        slaveChildStates = {}
        TMAddresses["master"] = self.__target__
        TMs = ["master"]
        while len(TMs):
            name = TMs[0]
            addr = TMAddresses[name]
            if not addr in self.__connected_addrs__:
                try:
                    TMControlInterface.Connect(addr)
                except SystemError:
                    self.disconnect()
                    raise SystemError("TMControlInterface - failed to connect to %s" % (addr))
                self.__connected_addrs__.append(addr)
            try:
                children = TMControlInterface.QueryChildren(addr)
            except SystemError:
                self.disconnect()
                traceback.print_exc(file=sys.stdout)
                raise SystemError("TMControlInterface - failed to query children of %s" % (addr))
            childProcessList = children.split("  ")
            hasProgram = False
            childStates = {}
            for child in childProcessList:
                charr = child.split(" ")
                if len(charr) != 4:
                    continue
                chId, chType, chAddr, chState = charr
                if chType == "program":
                    hasProgram = True
                    childStates[chId] = chState
                if chType == "slave" or chType == "slaveprogram":
                    TMAddresses[chId] = chAddr
                    TMs.append(chId)
            if hasProgram:
                slaves.append(name)
                slaveChildStates[name] = childStates
            TMs = TMs[1:]

        # Get & Parse Slave Status Data
        self.slaveChildData = {}
        for slave in slaves:
            try:
                slaveStatusData = TMControlInterface.QueryStatusData(TMAddresses[slave])
            except SystemError:
                self.disconnect()
                raise SystemError("TMControlInterface - failed to query status data from " \
                                "%s (%s)" % (slave, TMAddresses[slave]))
            self.slaveChildData[slave] = {}
            procList = slaveStatusData.split(";")
            for proc in procList:
                statusDict = self.StatusData2Dict(proc)
                if statusDict.has_key("name") and \
                   slaveChildStates[slave].has_key(statusDict["name"]):
                    state = slaveChildStates[slave][statusDict["name"]]
                else:
                    if statusDict.has_key("RunNumber"):
                        self.slaveChildData[slave]["RunNumber"] = statusDict
                        continue
                    state = "UNKNOWN"

                statusDict["state"] = state
                statusDict["host"] = self.getHostname(TMAddresses[slave])
                if statusDict.has_key("name"):
                    self.slaveChildData[slave][statusDict["name"]] = statusDict

        self.query_top_level_state()
        return self.slaveChildData

    def getHostname(self, address):
        regrp = re.search("tcpmsg://([\d\w\-\._]+):\d+", address)
        if regrp and regrp.group(1):
            return regrp.group(1)
        else:
            return ""

class StatusParser(object):

    def __init__(self):
        self.__data = []

        self.runNumber = ""
        self.maxPendingOutputEventCount = [0, ""]
        self.maxPendingInputEventCount = [0, ""]
        self.maxPendingOutputsComponents = []
        self.maxPendingInputsComponents = []
        self.maxPendingInputsMergers = []
        self.maxPendingInputsBridges = []

        self.minFreeOutputBufferSrc = []
        self.minFreeOutputBufferPrc = []
        self.minFreeOutputBufferOther = []

        self.curMinAnnounceRate = [0, ""]
        self.curMaxAnnounceRate = [0, ""]
        self.curAvgAnnounceRate = 0
        self.sumOfInputAnnouceRates = 0
        self.minAnnounceCount = [0, ""]
        self.avgAnnounceCount = 0
        self.maxAnnounceCount = [0, ""]
        self.sumOfInputAnnounceCounts = 0
        self.curMinProcessRate = [0, ""]
        self.curMaxProcessRate = [0, ""]
        self.curAvgProcessRate = 0
        self.sumOfProcessingRates = 0
        self.minEventLifetime = [0, ""]
        self.avgEventLifetime = 0
        self.maxEventLifetime = [0, ""]
        self.sumOfCurrentAvgLifetimes = 0
        self.avgInputEventSize = 0
        self.avgOutputEventSize = 0
        self.maxOutOfOrderOrbits = [0, ""]

        self.hltInputDataRate = 0
        self.hltInputTotalSize = 0
        self.hltOutputDataRate = 0
        self.hltOutputEventRate = 0
        self.hltOutputTotalSize = 0
        self.hltmessages = []
        self.componentStats = {}
        self.hist_bufferUsageSrc = [0]*101
        self.hist_bufferUsagePrc = [0]*101
        self.hist_bufferUsageOther = [0]*101
        self.stuckComponentList = {}

        self.sourceCount = 0
        self.procCount = 0
        self.sinkCount = 0
        self.compCount = 0
        self.deadCount = 0
        self.topLevelState = "UNKNOWN"

        self.detectors = ['tpc', 'trd', 'itsspd', 'itsssd', 'itssdd', 'emcal', 'tzero', 'vzero', 'zdc', 'tof']
        self.detectorMetrics = ['InputEventRate', 'InputDataRate']
        self.detectorValues = {}
        for det in self.detectors:
            for mtr in self.detectorMetrics:
                metric = det+mtr
                if "Event" in metric:
                    self.detectorValues[metric] = [-1, ""]
                else:
                    self.detectorValues[metric] = 0
        self.datarateDetectors = self.detectors
        self.datarateDetectorMin = {}
        self.datarateDetectorMax = {}
        self.datarateDetectorMedian = {}
        self.datarateDetectorList = {}
        for det in self.datarateDetectors:
            self.datarateDetectorList[det] = []
            self.datarateDetectorMedian[det] = 0
            self.datarateDetectorMin[det] = [0, ""]
            self.datarateDetectorMax[det] = [0, ""]

    def min(self, dictionary, key, prev_min):
        if not dictionary.has_key(key) or (float(dictionary[key]) >= prev_min[0] and prev_min[1]):
            return prev_min
        return [float(dictionary[key]), dictionary["name"]]

    def max(self, dictionary, key, prev_max):
        if not dictionary.has_key(key) or (float(dictionary[key]) <= prev_max[0] and prev_max[1]):
            return prev_max
        return [float(dictionary[key]), dictionary["name"]]

    def max_of(self, dictionary, key, cur_list, entries):
        if not dictionary.has_key(key):
            if not self.isFloat(key):
                return cur_list
            else:
                val = key
        else:
            val = float(dictionary[key])
        name = dictionary["name"]

        cur_list.append({'name' : name, 'value': val})
        cur_list.sort(reverse=True, key=lambda v: v['value'])
        if entries and len(cur_list) > entries:
            cur_list = cur_list[:entries]
        return cur_list

    def min_of(self, dictionary, key, cur_list, entries):
        if not dictionary.has_key(key):
            if not self.isFloat(key):
                return cur_list
            else:
                val = key
        else:
            val = float(dictionary[key])
        name = dictionary["name"]

        cur_list.append({'name' : name, 'value': val})
        cur_list.sort(key=lambda v: v['value'])
        if entries and len(cur_list) > entries:
            cur_list = cur_list[:entries]
        return cur_list

    def isFloat(self, val):
        try:
            float(val)
            return True
        except:
            return False

    def sum(self, dictionary, key, prev_sum):
        if not dictionary.has_key(key):
            return prev_sum
        return prev_sum + float(dictionary[key])

    def getField(self, dictionary, key, default):
        if dictionary.has_key(key):
            return dictionary[key]
        else:
            return default

    def get_free_output_buffer_percent(self, proc):
        totalOutputBuffer = int(self.getField(proc, "TotalOutputBuffer", 0))
        freeOutputBuffer = int(self.getField(proc, "FreeOutputBuffer", 0))
        if freeOutputBuffer > totalOutputBuffer: # FreeOutputBuffer underrun
            freeOutputBuffer = 0
        if totalOutputBuffer:
            return 100 * freeOutputBuffer / totalOutputBuffer
        else:
            return 100

    def update_source_stats(self, prc):
        self.curMinAnnounceRate = self.min(prc, "CurrentAnnounceRate", self.curMinAnnounceRate)
        self.curMaxAnnounceRate = self.max(prc, "CurrentAnnounceRate", self.curMaxAnnounceRate)
        self.sumOfInputAnnouceRates = self.sum(prc, "CurrentAnnounceRate", \
                                               self.sumOfInputAnnouceRates)
        self.hltInputDataRate = self.sum(prc, "CurrentProcessedOutputDataSize", \
                                         self.hltInputDataRate)
        self.minAnnounceCount = self.min(prc, "AnnouncedEventCount", self.minAnnounceCount)
        self.sumOfInputAnnounceCounts = self.sum(prc, "AnnouncedEventCount", \
                                                 self.sumOfInputAnnounceCounts)
        self.maxAnnounceCount = self.max(prc, "AnnouncedEventCount", self.maxAnnounceCount)
        self.hltInputTotalSize = self.sum(prc, "TotalProcessedOutputDataSize", \
                                          self.hltInputTotalSize)
        self.minFreeOutputBufferSrc = self.min_of(prc, "FreeOutputBufferPercent", \
                                                  self.minFreeOutputBufferSrc, \
                                                  NUMBER_OF_LIST_ENTRIES)

        detname = str.lower(prc['name'].split("_")[0])
        if detname in self.detectors:
            key = detname+"InputEventRate"
            self.detectorValues[key] = self.max(prc, "CurrentAnnounceRate", \
                                                self.detectorValues[key])
            key = detname+"InputDataRate"
            self.detectorValues[key] = self.sum(prc, "CurrentProcessedOutputDataSize", \
                                                self.detectorValues[key])
        if detname in self.datarateDetectors:
            self.datarateDetectorList[detname] = self.max_of(prc, "CurrentProcessedOutputDataSize", \
                                                             self.datarateDetectorList[detname], 0)
            self.datarateDetectorMax[detname] = self.max(prc, "CurrentProcessedOutputDataSize", \
                                                         self.datarateDetectorMax[detname])
            self.datarateDetectorMin[detname] = self.min(prc, "CurrentProcessedOutputDataSize", \
                                                         self.datarateDetectorMin[detname])

    def update_sink_stats(self, prc):
        self.hltOutputTotalSize = self.sum(prc, "TotalProcessedInputDataSize", \
                                           self.hltOutputTotalSize)
        self.hltOutputDataRate = self.sum(prc, "CurrentProcessedInputDataSize", \
                                          self.hltOutputDataRate)
        self.hltOutputEventRate = self.sum(prc, "CurrentProcessedEventCount", \
                                           self.hltOutputEventRate)
        self.minEventLifetime = self.min(prc, "TotalMinEventLifetime", \
                                         self.minEventLifetime)
        self.maxEventLifetime = self.max(prc, "TotalMaxEventLifetime", \
                                         self.maxEventLifetime)
        self.sumOfCurrentAvgLifetimes = self.sum(prc, "CurrentAvgEventLifetime", \
                                                 self.sumOfCurrentAvgLifetimes)
        self.maxOutOfOrderOrbits = self.max(prc, "MaxOutOfOrderOrbits", \
                                            self.maxOutOfOrderOrbits)

    def update_proc_stats(self, prc):
        self.curMinProcessRate = self.min(prc, "CurrentProcessRate", \
                                          self.curMinProcessRate)
        self.curMaxProcessRate = self.max(prc, "CurrentProcessRate", \
                                          self.curMaxProcessRate)
        self.maxPendingInputsComponents = self.max_of(prc, "PendingInputEventCount", \
                                                      self.maxPendingInputsComponents, \
                                                      NUMBER_OF_LIST_ENTRIES)
        self.sumOfProcessingRates = self.sum(prc, "CurrentProcessRate", \
                                             self.sumOfProcessingRates)
        self.minFreeOutputBufferPrc = self.min_of(prc, "FreeOutputBufferPercent", \
                                                  self.minFreeOutputBufferPrc, \
                                                  NUMBER_OF_LIST_ENTRIES)

    def check_stuck_component(self, prc):
        pending_inputs = int(self.getField(prc, "PendingInputEventCount", 0))
        pending_outputs = int(self.getField(prc, "PendingOutputEventCount", 0))
        cur_proc_events = int(self.getField(prc, "CurrentProcessedEventCount", 0))
        if (pending_inputs - pending_outputs) > \
           (cur_proc_events * PENDING_INPUT_DIFF_FACTOR) and \
           pending_inputs > PENDING_INPUT_THRESHOLD:
            self.stuckComponentList[prc["name"]] = [pending_inputs, pending_outputs, \
                                                    cur_proc_events, prc["host"]]

    def check_output_buffer_fill_state(self, prc):
        if prc["FreeOutputBufferPercent"] < OUTPUT_BUFFER_ERROR_THRESHOLD_PERCENT:
            pending_inputs = int(self.getField(prc, "PendingInputEventCount", 0))
            pending_outputs = int(self.getField(prc, "PendingOutputEventCount", 0))
            msg = "Output buffer full: Component: %s on %s, pendingInputs: %d, " \
                  "pendingOutputs: %d, freeBuffer: %d%%" % \
                  (prc['name'], prc['host'], pending_inputs,
                   pending_outputs, prc["FreeOutputBufferPercent"])
            self.hltmessages.append({'msg':msg, 'severity':"e"})

    def parse_status(self, data):
        self.__data = data
        self.sourceCount = 0
        self.procCount = 0
        self.sinkCount = 0
        self.compCount = 0
        self.deadCount = 0
        self.hist_bufferUsageSrc = [0]*101
        self.hist_bufferUsagePrc = [0]*101
        self.hist_bufferUsageOther = [0]*101
        self.stuckComponentList = {}

        self.sumOfInputAnnouceRates = 0
        self.sumOfInputAnnounceCounts = 0
        self.sumOfCurrentAvgLifetimes = 0
        self.sumOfProcessingRates = 0

        for slave in self.__data:
            if slave == "master":
                self.topLevelState = self.__data[slave]["state"]
                continue
            for proc in self.__data[slave]:
                p = self.__data[slave][proc]
                if p.has_key("RunNumber"):
                    self.runNumber = int(p["RunNumber"])
                    continue
                if not p.has_key('state'):
                    continue
                statearr = p['state'].split(":")
                if len(statearr) != 2:
                    continue
                compType, compStatus = statearr
                p['FreeOutputBufferPercent'] = self.get_free_output_buffer_percent(p)
                bufferUsage = int(100 - p['FreeOutputBufferPercent'])

                self.maxPendingOutputEventCount = self.max(p, "PendingOutputEventCount", \
                                                           self.maxPendingOutputEventCount)
                self.maxPendingInputEventCount = self.max(p, "PendingInputEventCount", \
                                                          self.maxPendingInputEventCount)
                self.check_output_buffer_fill_state(p)

                if not compType in ["ControlledDataSource", "ControlledDataProcessor"]:
                    self.minFreeOutputBufferOther = self.min_of(p, "FreeOutputBufferPercent", \
                                                                self.minFreeOutputBufferOther, \
                                                                NUMBER_OF_LIST_ENTRIES)
                    self.hist_bufferUsageOther[bufferUsage] += 1

                if not compType in ["ControlledDataSource", "ControlledDataProcessor", \
                        "ControlledDataSink"]:
                    if compType != "TM":
                        if compStatus != "UNKNOWN":
                            self.compCount += 1
                        else:
                            self.deadCount += 1

                if compType == "ControlledDataSource":
                    self.update_source_stats(p)
                    self.hist_bufferUsageSrc[bufferUsage] += 1
                    self.sourceCount += 1

                elif compType == "ControlledDataSink":
                    if not "HOWS" in p["name"]:
                        continue
                    self.update_sink_stats(p)
                    self.sinkCount += 1

                elif compType == "ControlledDataProcessor":
                    self.update_proc_stats(p)
                    self.hist_bufferUsagePrc[bufferUsage] += 1
                    self.check_stuck_component(p)
                    self.procCount += 1

                elif compType == "EventMerger":
                    self.maxPendingInputsMergers = self.max_of(p, "PendingInputEventCount", \
                                                               self.maxPendingInputsMergers, \
                                                               NUMBER_OF_LIST_ENTRIES)

                elif compType == "ControlledBridge":
                    self.maxPendingInputsBridges = self.max_of(p, "PendingInputEventCount", \
                                                              self.maxPendingInputsBridges, \
                                                              NUMBER_OF_LIST_ENTRIES)


        if self.hltOutputEventRate:
            self.avgOutputEventSize = self.hltOutputDataRate / self.hltOutputEventRate
        else:
            self.avgOutputEventSize = 0

        if self.sourceCount:
            self.curAvgAnnounceRate = self.sumOfInputAnnouceRates / self.sourceCount
            self.avgAnnounceCount = self.sumOfInputAnnounceCounts / self.sourceCount

        if self.procCount:
            self.curAvgProcessRate = self.sumOfProcessingRates / self.procCount

        if self.sinkCount:
            self.avgEventLifetime = self.sumOfCurrentAvgLifetimes / self.sinkCount

        if self.curAvgAnnounceRate:
            self.avgInputEventSize = self.hltInputDataRate / self.curAvgAnnounceRate
        else:
            self.avgInputEventSize = 0

        for det in self.datarateDetectors:
            self.datarateDetectorMedian[det] = self.median(self.datarateDetectorList[det])

    def median(self, numbers):
        center = len(numbers) / 2
        if len(numbers) == 0:
            return 0
        if center < 1:
            return numbers[0]['value']
        if len(numbers) % 2 == 0:
            return (numbers[center-1]['value'] + numbers[center]['value']) / 2.0
        else:
            return numbers[center]['value']



    def __str__(self):
        str_ = "RunNumber: %s\n" % (self.runNumber)
        str_ += "maxPendingOutputEventCount: %s\n" % (self.maxPendingOutputEventCount)
        str_ += "curMinAnnounceRate: %s Hz\n" % (self.curMinAnnounceRate)
        str_ += "curMaxAnnounceRate: %s Hz\n" % (self.curMaxAnnounceRate)
        str_ += "curAvgAnnounceRate: %s Hz\n" % (self.curAvgAnnounceRate)
        str_ += "hltInputDataRate: %s MB/s\n" % (self.hltInputDataRate/1000/1000)
        str_ += "hltOutputDataRate: %s MB/s\n" % (self.hltOutputDataRate/1000/1000)
        str_ += "hltOutputEventRate: %s Hz\n" % (self.hltOutputEventRate)
        return str_

class StatCollector(object):

    def __init__(self):
        self.maxEventsWarningThreshold = 0
        self.maxEventsWarningState = 0
        self.logger = None
        self.s = {}
        self.seq_appendCount = 0
        self.stuckComponentList = {}
        self.s['messages'] = []
        self.s['proc_stats'] = []
        self.s['framework_stats'] = []
        self.s['component_stats'] = []
        self.seq_lists = ['seq_time', 'seq_maxPendingOutputEventCount', \
                          'seq_maxPendingInputEventCount', 'seq_hltInputDataRate', \
                          'seq_hltOutputDataRate', 'seq_hltInputEventRate', \
                          'seq_hltOutputEventRate', 'seq_hltInputAvgEventSize', \
                          'seq_hltOutputAvgEventSize']
        self.detectors = ['tpc', 'trd', 'itsspd', 'itsssd', 'itssdd', 'emcal', 'vzero', 'tzero', 'tof', 'zdc']
        self.detectorMetrics = ['InputEventRate', 'InputDataRate']
        for det in self.detectors:
            for mtr in self.detectorMetrics:
                key = "seq_"+det+mtr
                if not key in self.seq_lists:
                    self.seq_lists.append(key)
        self.datarateDetectors = self.detectors
        for det in self.datarateDetectors:
            keybase = "seq_"+det+"_linkdatarate_";
            self.seq_lists.append(keybase+"min")
            self.seq_lists.append(keybase+"median")
            self.seq_lists.append(keybase+"max")
        self.initializeSequenceLists()

    def initializeSequenceLists(self):
        for entry in self.seq_lists:
            self.s[entry] = []
        self.seq_appendCount = 0

    def checkMaxEventsInChain(self, stats):
        if self.maxEventsWarningThreshold == 0 or self.logger == None:
            return
        if stats.maxPendingOutputEventCount[0] >= self.maxEventsWarningThreshold:
            if self.maxEventsWarningState >= 2:
                msg = "Too many events in HLT chain: %d (%s)" % \
                      (stats.maxPendingOutputEventCount[0], stats.maxPendingOutputEventCount[1])
                self.logger.log(msg, 'e')
            else:
                self.maxEventsWarningState += 1
        elif self.maxEventsWarningState > 0 and \
                 stats.maxPendingOutputEventCount[0] < MAX_EVENTS_LOW_WATERMARK:
            self.maxEventsWarningState = 0
            msg = "HLT recovered. Events in chain: %d" % (stats.maxPendingOutputEventCount[0])
            self.logger.log(msg, 'm')

    def checkStuckComponents(self, stats):
        # remove obsolete/recovered components from dict
        for key in self.stuckComponentList.keys():
            if not key in stats.stuckComponentList.keys():
                if self.stuckComponentList[key] >= STUCK_COMPONENT_WARNING_ITERATIONS and \
                   self.logger:
                    msg = "Component recovered: %s" % (key)
                    self.logger.log(msg, 'i')
                del self.stuckComponentList[key]
        # update refcounts
        for key in stats.stuckComponentList.keys():
            if not key in self.stuckComponentList.keys():
                self.stuckComponentList[key] = 1
            else:
                self.stuckComponentList[key] += 1
            if self.stuckComponentList[key] == STUCK_COMPONENT_WARNING_ITERATIONS and self.logger:
                msg = "Pending inputs exceed processing rate - Component: " \
                      "%s on %s, pendingInputs: %d, pendingOutputs: %d, procRate: %d" % \
                      (key, stats.stuckComponentList[key][3], stats.stuckComponentList[key][0], \
                       stats.stuckComponentList[key][1], stats.stuckComponentList[key][2])
                self.logger.log(msg, 'w')

    def sendLogMessages(self, stats):
        if not self.logger:
            return
        for msg in stats.hltmessages:
            self.logger.log(msg['msg'], msg['severity'])

    def update(self, stats):
        self.sendLogMessages(stats)
        self.checkStuckComponents(stats)
        self.checkMaxEventsInChain(stats)
        # a new run is starting...
        if stats.topLevelState in ['starting', 'connecting']:
            self.initializeSequenceLists() # clear sequences
            self.maxEventsWarningState = 0 # clear MaxEventsWarningState
        self.prepareJsonData(stats)

    def prepareJsonData(self, stats):
        self.s['api_version'] = API_VERSION
        self.s['proc_stats'] = []
        self.s['framework_stats'] = []
        self.s['component_stats'] = []
        self.s['framework_stats'] += [{'name' : "Global State", 'value' : stats.topLevelState}]
        self.s['framework_stats'] += [{'name' : "Run Number", 'value' : stats.runNumber}]
        self.s['framework_stats'] += [{'name' : "Running source components", \
                                       'value' : stats.sourceCount}]
        self.s['framework_stats'] += [{'name' : "Running processing components", \
                                       'value' : stats.procCount}]
        self.s['framework_stats'] += [{'name' : "Running sink components", \
                                       'value' : stats.sinkCount}]
        self.s['framework_stats'] += [{'name' : "Running dataflow components", \
                                       'value' : stats.compCount}]
        self.s['framework_stats'] += [{'name' : "Dead components", \
                                       'value' : stats.deadCount}]
        self.s['proc_stats'] += [{'name' : "Received event count", \
                                  'value' : \
                                  {'min' : int(stats.minAnnounceCount[0]), \
                                   'avg' : ("%.1f" % (stats.avgAnnounceCount)), \
                                   'max' : int(stats.maxAnnounceCount[0])}}]
        self.s['proc_stats'] += [{'name' : "Received event rate during last second (Hz)", \
                                  'value' : \
                                  {'min' : int(stats.curMinAnnounceRate[0]), \
                                   'avg' : int(stats.curAvgAnnounceRate), \
                                   'max' : int(stats.curMaxAnnounceRate[0])}}]
        self.s['proc_stats'] += [{'name' : 'Max. event count currently processed in chain',
                                  'value': stats.maxPendingOutputEventCount[0]}]
        self.s['proc_stats'] += [{'name' : "Current processing rate during last second (Hz)", \
                                  'value' : \
                                  {'min' : int(stats.curMinProcessRate[0]), \
                                   'avg' : int(stats.curAvgProcessRate), \
                                   'max' : int(stats.curMaxProcessRate[0])}}]
        self.s['proc_stats'] += [{'name' : "Event lifetimes from all sinks (ms)", \
                                  'value' : \
                                  {'min' : int(stats.minEventLifetime[0]/1000), \
                                   'avg' : int(stats.avgEventLifetime/1000), \
                                   'max' : int(stats.maxEventLifetime[0]/1000)}}]
        self.s['proc_stats'] += [{'name' : "Maximum Out-of-Order OrbitID difference (ms)",\
                                  'value': ((int(stats.maxOutOfOrderOrbits[0]) * 1000 * 1492) >> 24)}]
        self.s['proc_stats'] += [{'name' : "Average Input Event Size (kB)", \
                                  'value' : int(stats.avgInputEventSize/1000)}]
        self.s['proc_stats'] += [{'name' : "Average Output Event Size (kB)", \
                                  'value' : int(stats.avgOutputEventSize/1000)}]
        self.s['proc_stats'] += [{'name' : "Total Input Size (MB)", \
                                  'value' : "%.1f" % (stats.hltInputTotalSize/1000/1000)}]
        self.s['proc_stats'] += [{'name' : "Total Output Size (MB)", \
                                  'value' : "%.1f" % (stats.hltOutputTotalSize/1000/1000)}]

        self.seq_appendCount += 1
        self.s['seq_time'].append(int(time.time()))
        self.s['seq_maxPendingInputEventCount'].append(int(stats.maxPendingInputEventCount[0]))
        self.s['seq_maxPendingOutputEventCount'].append(int(stats.maxPendingOutputEventCount[0]))
        self.s['seq_hltInputDataRate'].append(int(stats.hltInputDataRate/1000/1000))
        self.s['seq_hltOutputDataRate'].append(int(stats.hltOutputDataRate/1000/1000))
        self.s['seq_hltInputEventRate'].append(int(stats.curMaxAnnounceRate[0]))
        self.s['seq_hltOutputEventRate'].append(int(stats.hltOutputEventRate))
        self.s['seq_hltOutputAvgEventSize'].append(int(stats.avgOutputEventSize/1000))
        self.s['seq_hltInputAvgEventSize'].append(int(stats.avgInputEventSize/1000))
        for det in self.detectors:
            for mtr in self.detectorMetrics:
                metric = det+mtr
                key = "seq_"+metric
                val = stats.detectorValues[metric]
                if isinstance(val, list): # list
                    val = val[0]
                if "Data" in metric:
                    val = val / 1000 / 1000 # in MB/s
                self.s[key].append("%.1f" % (val))
        for det in self.datarateDetectors:
            keybase = "seq_"+det+"_linkdatarate_"
            self.s[keybase+"min"].append("%.2f" % (stats.datarateDetectorMin[det][0]/1000/1000))
            self.s[keybase+"median"].append("%.2f" % (stats.datarateDetectorMedian[det]/1000/1000))
            self.s[keybase+"max"].append("%.2f" % (stats.datarateDetectorMax[det][0]/1000/1000))

        # trim sequences:
        # keep latest [KEEP_SAMPLES_FULL] samples from every captured timestamp,
        # then [KEEP_SAMPLES_INCREMENT] samples from only every second timestamp,
        # then [KEEP_SAMPLES_INCREMENT] samples from only every forth timestamp
        # ...and so on
        limit = KEEP_SAMPLES_FULL
        iteration = 1
        while limit < len(self.s['seq_time']):
            mask = (1 << iteration) - 1
            if self.seq_appendCount & mask == mask:
                index = len(self.s['seq_time']) - limit
                for seq in self.seq_lists:
                    self.s[seq].remove(self.s[seq][index])
            iteration += 1
            limit += KEEP_SAMPLES_INCREMENT

        self.s['list_maxPendingInputsComponents'] = stats.maxPendingInputsComponents
        self.s['list_maxPendingInputsMergers'] = stats.maxPendingInputsMergers
        self.s['list_maxPendingInputsBridges'] = stats.maxPendingInputsBridges

        self.s['list_minFreeOutputBufferSrc'] = stats.minFreeOutputBufferSrc
        self.s['list_minFreeOutputBufferPrc'] = stats.minFreeOutputBufferPrc
        self.s['list_minFreeOutputBufferOther'] = stats.minFreeOutputBufferOther

        for det in stats.datarateDetectorList:
            self.s['list_linkrate_'+det] = stats.datarateDetectorList[det]

        self.s['hist_bufferUsageSrc'] = stats.hist_bufferUsageSrc
        self.s['hist_bufferUsagePrc'] = stats.hist_bufferUsagePrc
        self.s['hist_bufferUsageOther'] = stats.hist_bufferUsageOther

    def queueMessage(self, msg):
        if not msg in self.s['messages']:
            self.s['messages'].append(msg)

    def clearMessages(self):
        self.s['messages'] = []



class rrdLogger(object):
    """Class rrdLogger

    Some little helper class to log selected values from the status scan
    to an rrd database for later analysis/plotting...

    Arguments:
    directory - base directory where all rrd files are stored

    """
    directoy = ""
    metrics = []
    def __init__(self, directory="."):
        self.directory = directory
        # RRA Options: step 1s, save LAST value, 36k rows = 10h
        # check that directory already exists, if not create
        if not os.path.exists(self.directory):
            os.mkdir(self.directoy, mode=0755)

    def createRrdFile(self, name):
        rra_options = 'RRA:LAST:0.5:1:36000'
        rrdtool.create(self.rrdFile(name), '--start', '-10s', \
                       '--step', '1s', 'DS:%s:GAUGE:10:U:U' % (name), rra_options)

    def addMetric(self, name):
        self.createRrdFile(name)
        self.metrics.append(name)

    def rrdFile(self, name):
        """ Return full path of the rrd database file <name>. """
        return os.path.join(self.directory, "%s.rrd" % (name))

    def pngFile(self, name):
        return os.path.join(self.directory, "%s.png" % (name))

    def update(self, rrdName, value):
        """ Update rrd database file <rrdName> with <value>. """
        if rrdName in self.metrics:
            if not os.path.exists(self.directory):
                os.mkdir(self.directoy, mode=0755)
            if not os.path.isfile(self.rrdFile(rrdName)):
                self.createRrdFile(rrdName)
            rrdtool.update(self.rrdFile(rrdName), 'N:%s'%(value))
            #info = rrdtool.info(self.rrdFile(rrdName))

    def graphAll(self):
        for rrdName in self.metrics:
            DEF = "DEF:%s=%s:%s:LAST" % (rrdName, self.rrdFile(rrdName), rrdName)
            LINE = "LINE:%s#000000:%s" % (rrdName, rrdName)
            try:
                rrdtool.graph(self.pngFile(rrdName), '--imgformat', 'PNG', '--width', '800', \
                              '--height', '200', '--start', '-600', '--end', '-1', \
                              DEF, LINE)
            except:
                traceback.print_exc(file=sys.stdout)
                pass


class HttpHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.send_header('Access-Control-Allow-Origin', '*')
        self.end_headers()
        self.wfile.write(json.dumps(self.server.userdata))
        return

    def log_message(self, format, *args):
        """
        by default any request is logged to stderr, overriding log_ message to silence it
        """
        return

class myHTTPServer(HTTPServer):
    userdata = {}
    def update_data(self, data):
        self.userdata = copy.deepcopy(data)



parser = argparse.ArgumentParser()
parser.add_argument("--target", help="target TaskManager address, " \
                    "typically tcpmsg://[masternode]:[hlt_run_baseport]", type=str, required=True)
parser.add_argument("--address", help="ComponentStatusMonitors own address, " \
                    "typically tcpmsg://:[port]", type=str, required=True)
parser.add_argument("--logbinary", help="InfoLogger log binary, optional, " \
                    "default: /usr/bin/log", type=str)
parser.add_argument("--loop", help="Run contiuously, argument defines " \
                    "wait time between iterations.", type=int)
parser.add_argument("--logtohlt", help="Send messages to HLT infologger", \
                    action="store_true", default=False)
parser.add_argument("--rrddir", help="directory for rrd files, disabled if not set", \
                    default=None)
parser.add_argument("--rrdgraphs", help="create PNGs from rrd files", \
                    action="store_true", default=False)
parser.add_argument("--httpserver", help="start http server, argument defines port", \
                    type=int, default=0)
parser.add_argument("--max-events-warning", help="send error message when number of events " \
                    "in chain exceeds argument", type=int)
parser.add_argument("--verbose", help="show some debug output", action="store_true", default=False)

args = parser.parse_args()
if args.logtohlt:
    logger = InfoBrowserLogger(args.logbinary)

try:
    Reader = TMReader(args.address, args.target)
except Exception, e:
    sys.stderr.write("ERROR: Failed to initialize TMReader: %s\n" % (e))
    sys.exit(-1)

if args.rrddir:
    rrd = rrdLogger(args.rrddir)
    rrd.addMetric("EventsInChain")
    rrd.addMetric("AvgInputEventRate")
    rrd.addMetric("MaxInputEventRate")
    rrd.addMetric("InputDataRate")
    rrd.addMetric("OutputEventRate")
    rrd.addMetric("OutputDataRate")
    rrd.addMetric("InputAvgEventSize")
    rrd.addMetric("OutputAvgEventSize")

def runServer(server, run_event):
    server.timeout = 1
    while run_event.is_set():
        server.handle_request()
    server.socket.close()

# Start HTTP server
if args.httpserver:
    run_event = threading.Event()
    run_event.set()
    server = myHTTPServer(('', args.httpserver), HttpHandler)
    http_thread = threading.Thread(target=runServer, args=[server, run_event])
    http_thread.start()

sc = StatCollector()
if args.max_events_warning:
    sc.maxEventsWarningThreshold = args.max_events_warning
if args.logtohlt:
    sc.logger = logger


loop = True
try:
    while loop:
        sts = StatusParser()
        try:
            tmdata = Reader.get_data()
            sts.parse_status(tmdata)
            if args.verbose:
                print sts

            sc.update(sts)

            if args.httpserver:
                server.update_data(sc.s)

            if args.rrddir:
                try:
                    rrd.update("EventsInChain", sts.maxPendingOutputEventCount[0])
                    rrd.update("AvgInputEventRate", sts.curAvgAnnounceRate)
                    rrd.update("MaxInputEventRate", sts.curMaxAnnounceRate[0])
                    rrd.update("InputDataRate", sts.hltInputDataRate)
                    rrd.update("OutputEventRate", sts.hltOutputEventRate)
                    rrd.update("OutputDataRate", sts.hltOutputDataRate)
                    rrd.update("InputAvgEventSize", sts.avgInputEventSize)
                    rrd.update("OutputAvgEventSize", sts.avgOutputEventSize)
                    if args.rrdgraphs:
                        rrd.graphAll()
                except:
                    traceback.print_exc(file=sys.stdout)

            # no error, clear previous messages
            if args.httpserver:
                sc.clearMessages()

        except SystemError, e:
            if args.httpserver:
                sc.update(sts)
                sc.queueMessage({'facility':sys.argv[0], 'severity':'e', 'msg':str(e)})
                server.update_data(sc.s)
            sys.stdout.write("ERROR: Failed to get data from TaskManager: %s\n" % str(e))
            traceback.print_exc(file=sys.stdout)
            pass
        except Exception, e:
            loop = False
            sys.stdout.write("ERROR: Failed to get data from TaskManager: %s\n" % str(e))
            traceback.print_exc(file=sys.stdout)

        if not args.loop:
            loop = False
        elif loop:
            time.sleep(args.loop)

finally:
    if args.httpserver:
        run_event.clear()
        http_thread.join()
    Reader.cleanup()
