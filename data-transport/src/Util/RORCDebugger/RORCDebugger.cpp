/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "AliHLTRORC6Handler.hpp"
#include "AliHLTShmID.h"
#include "AliHLTShmManager.hpp"
#include "AliHLTSharedMemory.hpp"
#include "MLUCLog.hpp"
#include "MLUCLogServer.hpp"
#include <iostream>
#include <csignal>
#include <cerrno>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstdlib>


bool gQuit = false;

void QuitSignalHandler( int sig );

void QuitSignalHandler( int )
    {
    gQuit = true;
    }

int main( int argc, char** argv )
    {
    gLogLevel = 0x3F;
    int i = 1;
    int errorArg = -1;
    const char* errorStr = NULL;
    char* cpErr;

    bool pciDeviceInfoSet=false;
    bool vendorDeviceIndexOrBusSlotFunction=false;
    AliUInt16_t pciVendorIDOrBus=0;
    AliUInt16_t pciDeviceIDOrSlot=0;
    AliUInt16_t pciDeviceIndexOrFunction=0;
    AliUInt16_t pciDeviceBarIndex=0;

    AliHLTShmID shmKey;
    unsigned long bufferSize = 0;
    unsigned long rorcBlockSize = 256;
    unsigned long maxPreEventCountExp2 = ~0UL;

    bool noStdoutLogging = false;

    uint8* referenceData = NULL;
    unsigned long referenceDataSize = 0;

    const char* dumpBaseName = "RORCDebugger";
    bool dumpGoodEvents = false;
    bool dumpCorruptEvents = false;

    while ( i < argc && !errorStr )
	{
	if ( !strcmp( argv[i], "-device" ) )
	    {
	    if ( argc <= i+4 )
		{
		errorStr = "Missing pci device information parameter";
		break;
		}
	    pciVendorIDOrBus = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorArg = i+1;
		errorStr = "Error converting pci vendor specifier.";
		break;
		}
	    pciDeviceIDOrSlot = strtoul( argv[i+2], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorArg = i+2;
		errorStr = "Error converting pci device specifier.";
		break;
		}
	    pciDeviceIndexOrFunction = strtoul( argv[i+3], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorArg = i+3;
		errorStr = "Error converting pci device index specifier.";
		break;
		}
	    pciDeviceBarIndex = strtoul( argv[i+4], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorArg = i+4;
		errorStr = "Error converting pci device bar index specifier.";
		break;
		}
	    vendorDeviceIndexOrBusSlotFunction = true;
	    pciDeviceInfoSet = true;
	    i += 5;
	    continue;
	    }
	if ( !strcmp( argv[i], "-slot" ) )
	    {
	    if ( argc <= i+4 )
		{
		errorStr = "Missing pci geographical address parameter";
		break;
		}
	    pciVendorIDOrBus = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorArg = i+1;
		errorStr = "Error converting pci bus specifier.";
		break;
		}
	    pciDeviceIDOrSlot = strtoul( argv[i+2], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorArg = i+2;
		errorStr = "Error converting pci slot specifier.";
		break;
		}
	    pciDeviceIndexOrFunction = strtoul( argv[i+3], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorArg = i+3;
		errorStr = "Error converting pci device function specifier.";
		break;
		}
	    pciDeviceBarIndex = strtoul( argv[i+4], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorArg = i+4;
		errorStr = "Error converting pci device bar index specifier.";
		break;
		}
	    vendorDeviceIndexOrBusSlotFunction = false;
	    pciDeviceInfoSet = true;
	    i += 5;
	    continue;
	    }
	if ( !strcmp( argv[i], "-rorcblocksize" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing RORC block size specifier.";
		break;
		}
	    rorcBlockSize = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorArg = i+1;
		errorStr = "Error converting RORC block size specifier.";
		break;
		}
	    if ( rorcBlockSize % 4 )
		{
		errorArg = i+1;
		errorStr = "RORC block size must be divisible by 4.";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-shm" ) )
	    {
	    if ( argc <= i+3 )
		{
		errorStr = "Missing shmkey or shmsize parameter";
		break;
		}
	    if ( !strcmp( argv[i+1], "bigphys" ) )
		shmKey.fShmType = kBigPhysShmType;
	    else if ( !strcmp( argv[i+1], "physmem" ) )
		shmKey.fShmType = kPhysMemShmType;
	    else if ( !strcmp( argv[i+1], "sysv" ) )
		shmKey.fShmType = kSysVShmType;
	    else
		{
		errorArg = i+1;
		errorStr = "Invalid shm type specifier...";
		break;
		}
	    shmKey.fKey.fID = strtoul( argv[i+2], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorArg = i+2;
		errorStr = "Error converting shmkey specifier..";
		break;
		}
	    bufferSize = strtoul( argv[i+3], &cpErr, 0 );
	    if ( *cpErr == 'k')
		bufferSize *= 1024;
	    else if ( *cpErr == 'M')
		bufferSize *= 1024*1024;
	    else if ( *cpErr == 'G')
		bufferSize *= 1024*1024*1024;
	    else if ( *cpErr )
		{
		errorArg = i+3;
		errorStr = "Error converting shmsize specifier..";
		break;
		}
	    i += 4;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing verbosity level specifier.";
		break;
		}
	    gLogLevel = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorArg = i+1;
		errorStr = "Error converting verbosity level specifier.";
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-nostdout" ) )
	    {
	    noStdoutLogging = true;
	    i++;
	    continue;
	    }
	if ( !strcmp( argv[i], "-dumpgoodevents" ) )
	    {
	    dumpGoodEvents = true;
	    i++;
	    continue;
	    }
	if ( !strcmp( argv[i], "-dumpcorruptevents" ) )
	    {
	    dumpCorruptEvents = true;
	    i++;
	    continue;
	    }
	if ( !strcmp( argv[i], "-dumpfilebasename" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing verbosity level specifier.";
		break;
		}
	    dumpBaseName = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-referencefile" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing RORC block size specifier.";
		break;
		}
	    i += 2;
	    int inFH = open( argv[i+1], O_RDONLY );
	    if ( inFH == -1 )
		{
		errorStr = "Unable to open reference file for reading";
		errorArg = i+1;
		break;
		}
	    off_t sz = lseek( inFH, 0, SEEK_END );
	    referenceDataSize = sz;
	    lseek( inFH, 0, SEEK_SET );
	    referenceData = new uint8[ sz ];
	    if ( !referenceData )
		{
		errorStr = "Out of memory trying to read in reference file";
		break;
		}
	    unsigned long curSize = 0;
	    int ret;
	    do
		{
		ret = read( inFH, referenceData+curSize, sz-curSize );
		if ( ret >= 0 )
		    {
		    curSize += (unsigned long)ret;
		    if ( curSize >= (unsigned long)sz )
			break;
		    }
		else
		    {
		    errorStr = "Error trying to read in reference file";
		    break;
		    }
		}
	    while ( ret >0 );
	    close( inFH );
	    i += 2;
	    continue;
	    }
	errorStr = "Unknown argument";
	break;
	}

    if ( !errorStr )
	{
	if ( !pciDeviceInfoSet )
	    errorStr = "PCI device info must be set, e.g. using -device or -slot parameter";
	if ( !bufferSize )
	    errorStr = "Shared memory must be set using -shm parameter.";
	}
    if ( errorStr )
	{
	MLUCStdoutLogServer stdoutLog;
	gLog.AddServer( stdoutLog );
	LOG( MLUCLog::kError, "RORCDebugger", "Usage" ) << "Usage: " << argv[0] << " [ -device <pciVendorID> <pciDeviceID> <pciDeviceIndex> <pciDeviceBarIndex> | -slot <pciBusNo> <pciSlotNo> <pciFunctionNo> <pciDeviceBarIndex> ] (-rorcblocksize <rorc-block-size>) -shm [bigphys|physmem|sysv] <shmkey> <shmsize> (-V <verbosity>) (-nostdout) (-dumpgoodevents) (-dumpcorrupevents) (-dumpfilebasename <dump-file-base-name>) (-referencefile <reference-file-name>)" << ENDLOG;
	LOG( MLUCLog::kError, "RORCDebugger", "Usage" ) << "Error: " << errorStr << ENDLOG;
	if ( i<argc )
	    LOG( MLUCLog::kError, "RORCDebugger", "Usage" ) << "Offending argument: " << argv[i] << " (" << i << ")"  << ENDLOG;
	if ( errorArg >=0 && errorArg<argc )
	    LOG( MLUCLog::kError, "RORCDebugger", "Usage" ) << "Offending parameter: " << argv[errorArg] << " (" << errorArg << ")" << ENDLOG;
	return -1;
	}

    MLUCStdoutLogServer stdoutLog;
    if ( !noStdoutLogging )
	gLog.AddServer( stdoutLog );
    MLUCFileLogServer fileLog( "RORCDebugger", ".log", 2500000 );
    gLog.AddServer( fileLog );


    signal( SIGQUIT, QuitSignalHandler );

    AliHLTSharedMemory sharedMemory;
    //AliHLTShmManager shmMan( 10 );
    
    AliUInt32_t outputShm = sharedMemory.GetShm( shmKey, bufferSize );
    if ( outputShm == (AliUInt32_t)-1 )
	{
	LOG( MLUCLog::kError, "RORCDebugger", "Out of shared memory" ) << "Error obtaining shared memory of " << bufferSize << " bytes." << ENDLOG;
	return -1;
	}
    AliUInt8_t* shmPtr = (AliUInt8_t*)sharedMemory.LockShm( outputShm );
    if ( !shmPtr )
	{
	LOG( MLUCLog::kError, "RORCDebugger", "Error locking shared memory" ) << "Error locking shared memory." << ENDLOG;
	return -1;
	}

    AliHLTRORC6Handler rorcHandler( bufferSize, rorcBlockSize, maxPreEventCountExp2 );

    int ret;
    ret = rorcHandler.Open( vendorDeviceIndexOrBusSlotFunction, pciVendorIDOrBus, 
			    pciDeviceIDOrSlot, pciDeviceIndexOrFunction, pciDeviceBarIndex );
    if ( ret )
	{
	LOG( MLUCLog::kError, "RORCDebugger", "RORC Error" ) << "Error opening RORC: " << strerror(ret) << " (" << ret << ")." << ENDLOG;
	return -1;
	}

    tRegion eventBufferRegion;
    if ( sharedMemory.GetPSIRegion( shmKey, eventBufferRegion ) )
	rorcHandler.SetEventBufferRegion( eventBufferRegion );
    
    ret = rorcHandler.SetEventBuffer( shmPtr, bufferSize, true );
    if ( ret )
	{
	LOG( MLUCLog::kError, "RORCDebugger", "RORC Error" ) << "Error setting RORC event buffer: " << strerror(ret) << " ("
							     << ret << ")." << ENDLOG;
	return -1;
	}

    ret = rorcHandler.InitializeRORC( true );
    if ( ret )
	{
	LOG( MLUCLog::kError, "RORCDebugger", "RORC Error" ) << "Error initializing RORC hardware: " << strerror(ret) << " ("
							     << ret << ")." << ENDLOG;
	return -1;
	}
    ret = rorcHandler.InitializeFromRORC( true );
    if ( ret )
	{
	LOG( MLUCLog::kError, "RORCDebugger", "RORC Error" ) << "Error initializing RORC handler object from RORC hardware: " << strerror(ret) << " ("
							     << ret << ")." << ENDLOG;
	return -1;
	}
    
    rorcHandler.EnumerateEventIDs( true );
    rorcHandler.SetHeaderSearch( false );
    rorcHandler.SetReportWordConsistencyCheck( false );
    rorcHandler.SetReportWordUseBlockSize( false );
    rorcHandler.SetTPCDataFix( false );

    AliUInt64_t dataOffset, dataSize, dataWrappedSize;
    bool dataCorrupt = false;
    bool eventSuppressed = false;
    AliUInt64_t unsuppressedDataSize = 0;
    AliEventID_t eventID;

    unsigned long long eventCounter=0;

    struct timeval lastLog;
    lastLog.tv_sec = lastLog.tv_usec = 0;

    rorcHandler.ActivateRORC();
    do
	{
	struct timeval now;
	gettimeofday( &now, NULL );
	dataCorrupt = false;
	ret = rorcHandler.PollForEvent( eventID, 
					dataOffset, dataSize, dataWrappedSize,
					dataCorrupt, eventSuppressed, unsuppressedDataSize );
	if ( !ret )
	    {
	    AliUInt8_t* dataPtr = shmPtr+dataOffset;  
	    if ( dataWrappedSize>0 )
		{
		dataPtr = new AliUInt8_t[dataSize];
		if ( !dataPtr )
		    {
		    LOG( MLUCLog::kError, "RORC Debugger", "Out of memory" )
			<< "Out of memory trying to unwrap event 0x" << MLUCLog::kHex << eventID
			<< ENDLOG;
		    }
		else
		    {
		    memcpy( dataPtr, shmPtr+dataOffset, dataSize-dataWrappedSize );
		    memcpy( dataPtr+dataSize-dataWrappedSize, shmPtr, dataWrappedSize );
		    }
		}
	    ++eventCounter;
	    LOG( MLUCLog::kDebug, "RORC Debugger", "Event found" )
		<< "Found event 0x" << MLUCLog::kHex << eventID
		<< MLUCLog::kDec << " (" << eventCounter
		<< ")" << ENDLOG;
	    lastLog = now;
	    bool comparisonError=false;
	    if ( dataCorrupt )
		{
		rorcHandler.SignalErrorToRORC();
		}
	    else if ( referenceData && dataPtr )
		{
		AliUInt32_t len1, len2;
		len1 = dataSize;
		len2 = referenceDataSize;
		AliUInt32_t nlp, nl, n;
		AliUInt32_t* lp1 = (AliUInt32_t*)dataPtr;
		nlp = len1/4;
		nl = len1;
		AliUInt32_t* lp2 = (AliUInt32_t*)referenceData;
		if ( len1 != len2 )
		    {
		    LOG( MLUCLog::kError, "RORC Debugger", "Event Block Size Differs" )
			<< "Event 0x" << MLUCLog::kHex << eventID << " (" << MLUCLog::kDec << eventID
			<< ") differs in size from reference data ( " << len1 << " <-> "
			<< len2 << " ) ( 0x" << MLUCLog::kHex << len1 << " <-> 0x" << len2 << " )." 
			<< ENDLOG;
		    if ( len2 < len1 )
			{
			nlp = len2/4;
			nl = len2;
			}
		    //ok = false;
		    }
		for ( n = 0; n < nlp; n++ )
		    {
		    lp1 = (AliUInt32_t*)( dataPtr + n*4 );
		    lp2 = (AliUInt32_t*)( referenceData + n*4 );
		    if ( *lp1 != *lp2 )
			{
			LOG( MLUCLog::kError, "RORC debugger", "Event Blocks Differ" )
			    << "Event 0x" << MLUCLog::kHex << eventID << " (" << MLUCLog::kDec << eventID
			    << ") differs from reference data at position " << n*4
			    << ": " << *lp1 << " <-> " << *lp2 << " ( 0x" << MLUCLog::kHex << *lp1 
			    << " <-> 0x" << *lp2 << " )." 
			    << ENDLOG;
			comparisonError = true;
			break;
			}
		    }
		if ( !comparisonError )
		    {
		    for ( n = nlp*4; n < nl; n++ )
			{
			if ( *(dataPtr+n) != *(referenceData+n) )
			    {
			    LOG( MLUCLog::kError, "AliHLTBlockCompareSubscriber::ProcessEvent", "Event Blocks Differ" )
				<< "Event 0x" << MLUCLog::kHex << eventID << " (" << MLUCLog::kDec << eventID
				<< ") differs from reference data at position " << n
				<< ": " << *(dataPtr+n) << " <-> " << *(referenceData+n) << " ( 0x" << MLUCLog::kHex << *(dataPtr+n) 
				<< " <-> 0x" << *(referenceData+n) << " )." 
				<< ENDLOG;
			    comparisonError = true;
			    break;
			    }
			}
		    }
		if ( !comparisonError )
		    {
		    LOG( MLUCLog::kInformational, "AliHLTBlockCompareSubscriber::ProcessEvent", "No Block Difference" )
			<< "Event 0x" << MLUCLog::kHex << eventID << " (" << MLUCLog::kDec << eventID
			<< ") does not differ from reference data." << ENDLOG;
		    }
		}

	    if ( (dumpCorruptEvents && (dataCorrupt || comparisonError)) ||
		 (dumpGoodEvents && !dataCorrupt && !comparisonError) )
		{
		MLUCString name( dumpBaseName );
		if ( dataCorrupt )
		    name += "-corrupt";
		if ( comparisonError )
		    name += "-referenceerror";
		if ( !dataCorrupt && !comparisonError )
		  name += "-good";
		char tmp[1024];
		sprintf( tmp, "0x%08X-0x%016LX",(unsigned)eventID.fType,(unsigned long long)eventID.fNr );
		name += tmp;
		name += ".bin";
		int outFH = open( name.c_str(), O_WRONLY|O_CREAT|O_TRUNC, 0777 );
		if ( outFH == -1 )
		    {
		    LOG( MLUCLog::kError, "RORC Debugger", "Output file" )
			<< "Error opening output file '" << name.c_str()
			<< "'." << ENDLOG;
		    }
		else
		    {
		    unsigned long curSize = 0;
		    int ret;
		    do
			{
			ret = write( outFH, shmPtr+dataOffset+curSize, dataSize-curSize );
			if ( ret >= 0 )
			    {
			    curSize += (unsigned long)ret;
			    if ( curSize >= (unsigned long)dataSize )
				break;
			    }
			else
			    {
			    LOG( MLUCLog::kError, "RORC Debugger", "Output file" )
				<< "Error writing to output file '" << name.c_str()
				<< "'." << ENDLOG;
			    break;
			    }
			}
		    while ( ret >0 );
		    close( outFH );
		    }
		}

	    if ( dataWrappedSize && dataPtr )
		delete [] dataPtr;
	    dataPtr = NULL;
	    rorcHandler.ReleaseEvent( eventID );
	    }
	else if ( ret!=EAGAIN )
	    {
	    rorcHandler.SignalErrorToRORC();
	    }
	if ( ( (now.tv_sec-lastLog.tv_sec)*1000000+now.tv_usec-lastLog.tv_usec ) >= 1000000 )
	    {
	    LOG( MLUCLog::kInformational, "RORC Debugger", "Events" )
		<< MLUCLog::kDec << eventCounter
		<< " events found" << ENDLOG;
	    lastLog = now;
	    }
	}
    while ( !gQuit );

    rorcHandler.DeactivateRORC();

    rorcHandler.DeinitializeRORC();
    rorcHandler.Close();
	    

    
    sharedMemory.UnlockShm( outputShm );
    sharedMemory.ReleaseShm( outputShm );


    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
