#ifndef ALI_HLT_SHUTTLE_PORTAL_LOGGER_HPP
#define ALI_HLT_SHUTTLE_PORTAL_LOGGER_HPP

#include <sstream>
#include <string>



/**
 * Class that handles logging on the Shuttle portal. From this class the PubSub
 * component should inherit its own logger and then adapt it to the PubSub
 * logging interface
 *
 * @date 19.06.2007
 *
 * @author Sebastian Bablok <Sebastian.Bablok@uib.no>
 */
class AliHLTShuttlePortalLogger {
	public:

		/**
		 * Return value for logging OK
		 */
		static const int ALI_HLT_LOGGING_OK;

		/**
		 * Return value for logging failed
		 */
		static const int ALI_HLT_LOGGING_FAILED;

		/**
		 * enum for the different logging levels. These are set as a bitmask
		 * and a message will be logged, if the appropriate bit is set. Thus
		 * for example kDebug and kWarning messages could be printed out while
		 * kInformational messages would be suppressed.
		 */
		enum AliHLTShuttlePortalLogLevel {mNone = 0, mBenchmark = 0x01,
				mDebug = 0x02, mInfo = 0x04, mWarning = 0x08, mError = 0x10,
				mFatal = 0x20, mPrimary = 0x80, mAll = 0xBF};
		/**
		 * Constructor for creating a Shuttle portal Logger
		 * This class is pure virtual, since the Logger should be implemented in
		 * derived classes. The log level is set to mAll.
		 */
		AliHLTShuttlePortalLogger();

		/**
		 * Constructor for creating a Shuttle portal Logger and set the initial
		 * log level. This class is pure virtual, since the Logger should be
		 * implemented in derived classes.
		 */
		AliHLTShuttlePortalLogger(AliHLTShuttlePortalLogLevel logLevel);

		/**
		 * Destructur for the Shuttle portal Logger.
		 */
		virtual ~AliHLTShuttlePortalLogger();

		/**
		 * Logging function
		 *
		 * @param logLevel the log level of this message
		 * @param msg stringstream containing th message
		 *
		 * @return success state of the function call
		 */
		virtual int logMessage(AliHLTShuttlePortalLogLevel logLevel,
				std::stringstream msg);

		/**
		 * Logging function
		 *
		 * @param logLevel the log level of this message
		 * @param msg string containing th message
		 *
		 * @return success state of the function call
		 */
		virtual int logMessage(AliHLTShuttlePortalLogLevel logLevel,
				std::string msg);

		/**
		 * Function to set the log Level
		 *
		 * @param logLevel the level to set
		 */
		virtual void setLogLevel(AliHLTShuttlePortalLogLevel logLevel);

		/**
		 * Function to get the current log Level
		 *
		 * @return the current log Level
		 */
//		virtual AliHLTShuttlePortalLogLevel getLogLevel();

		/**
		 * Static function to convert an return code into a meanigful string
		 *
		 * @param code the return value to convert to string
		 *
		 * @return the human readable convertion of "code"
		 */
		static std::string convertReturnCode(int code);

		/**
		 * Static function to convert the shuttle portal state into a meanigful
		 * string
		 *
		 * @param state the state value to convert to string
		 *
		 * @return the human readable convertion of "state"
		 */
		static std::string convertState(int state);


	protected:
		/**
		 * Pushes the log message to the desired output
		 *
		 * @param logLevel the log level of this message
		 * @param msg the message to be logged
		 *
		 * @return success state of the function call
		 */
		virtual int pushLogMessage(AliHLTShuttlePortalLogLevel logLevel,
				std::string msg) = 0;

		/**
		 * Pushes the log message to the desired output
		 *
		 * @param msg the message to be logged
		 *
		 * @return success state of the function call
		 */
		virtual int pushLogMessage(std::string msg) = 0;

		/**
		 * Function that handles the checking of the log level.
		 * Implementations derived of AliHLTShuttlePortalLogger can override
		 * this functionality with their own method.
		 * If an implementation want to discard the checking, the overwritten
		 * function just has to return ALI_HLT_LOGGING_OK.
		 *
		 * @param logLevel the input log level to check
		 *
		 * @return ALI_HLT_LOGGING_OK if check passed, ALI_HLT_LOGGING_FAILED
		 *				if input log level is not in the current set level
		 */
		virtual int checkLogLevel(AliHLTShuttlePortalLogLevel logLevel);

		/**
		 * virtual function to convert the shuttle portal log level into a
		 * meanigful string. Can be overwritten in inherited class.
		 *
		 * @param level the log level to convert to string
		 *
		 * @return the human readable convertion of "level"
		 */
		virtual std::string convertLogLevel(AliHLTShuttlePortalLogLevel level);

		/**
		 * The current log level
		 */
		AliHLTShuttlePortalLogLevel mLogLevel;

	private:

};




#endif // ALI_HLT_SHUTTLE_PORTAL_LOGGER_HPP
