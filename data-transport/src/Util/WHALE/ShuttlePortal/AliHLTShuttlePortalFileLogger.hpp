#ifndef ALI_HLT_SHUTTLE_PORTAL_FILE_LOGGER_HPP
#define ALI_HLT_SHUTTLE_PORTAL_FILE_LOGGER_HPP

#include "AliHLTShuttlePortalLogger.hpp"

#include <string>


/**
 * Implementation of a Shuttle Portal logger. This logger pushes all messages
 * to a given file.
 *
 * @date 19.06.2007
 *
 * @author Sebastian Bablok <Sebastian.Bablok@uib.no>
 */
class AliHLTShuttlePortalFileLogger : public AliHLTShuttlePortalLogger {
	public:
		/**
		 * Std-Constructor for creating a Shuttle portal File Logger
		 * All messages are writen to a file named "log-AliHLTShuttlePortal.log"
		 */
		AliHLTShuttlePortalFileLogger() :
				mLogFilename("log-AliHLTShuttlePortal.log") {};

		/**
		 * Constructor for creating a Shuttle portal File Logger which accepts
		 * a file name.
		 *
		 * @param filename the desired filename of the log file
		 */
		AliHLTShuttlePortalFileLogger(std::string filename);

		/**
		 * Constructor for creating a Shuttle portal File Logger which accepts
		 * a file name.
		 *
		 * @param filename the desired filename of the log file
		 */
		AliHLTShuttlePortalFileLogger(std::string filename,
				AliHLTShuttlePortalLogLevel logLevel);

		/**
		 * Destructur for the Shuttle portal File Logger.
		 */
		virtual ~AliHLTShuttlePortalFileLogger();


	protected:
		/**
		 * Pushes the log message to the desired output
		 *
		 * @param logLevel the log level of this message
		 * @param msg the message to be logged
		 *
		 * @return success state of the function call
		 */
		virtual int pushLogMessage(AliHLTShuttlePortalLogLevel logLevel, std::string msg);

		/**
		 * Pushes the log message to the desired output
		 *
		 * @param msg the message to be logged
		 *
		 * @return success state of the function call
		 */
		virtual int pushLogMessage(std::string msg);


	private:
		/**
		 * the log file name
		 */
		std::string mLogFilename;

};

#endif // ALI_HLT_SHUTTLE_PORTAL_FILE_LOGGER_HPP
