#ifndef ALI_HLT_SHUTTLE_PORTAL_HPP
#define ALI_HLT_SHUTTLE_PORTAL_HPP


#include <string>
#include <pthread.h>

#include "AliHLTCalibrationObject.hpp"
#include "AliHLTFESConnector.hpp"
#include "AliHLTShuttleDBConnector.hpp"
#include "AliHLTShuttlePortalLogger.hpp"

/**
 * Class that handles the task of the Shuttle portal:<br>
 * - initializing contact to the MySQL DB and the FileExchangeServer<br>
 * - store away recevied AliHLTCalibrationObject objects<br>
 * - closing the intialized connections <br>
 * (ev. - inform TaskManager, when task is finished)<br>
 *
 * @date 20.04.2007
 *
 * @author Sebastian Bablok <Sebastian.Bablok@uib.no>
 */
class AliHLTShuttlePortal {
	public:

		/**
		 * Std. Contructor for the AliHLTShuttlePortal.
		 * The object is not completely initialized, so call initPortal()
		 * afterwards.
		 *
		 * @param aLogger pointer to the Shuttle portal logger
		 */
		AliHLTShuttlePortal(AliHLTShuttlePortalLogger* aLogger) :
				mIsInit(false), mDBcon(), mFEScon(), mpLogger(aLogger) {};

        /**
         * Contructor and initializer for the AliHLTShuttlePortal.
         * The object is initialized afterwards and can be directly used to
		 * store away AliHLTCalibrationObject objectss.
         */
//        AliHLTShuttlePortal(std::string fesBasePath, std::string db,
//				std::string host, std::string user, std::string passwd);

		/**
		 * Destructor for the AliHLTShuttlePortal.
		 * If object is initialized and deinitPortal() has not been called
		 * before, the deinit process is issued before destructing the object.
		 */
		~AliHLTShuttlePortal();

		/**
		 * Initializes the Portal.
		 * This includes opening the connection to the MySQL DB and setting the
		 * path to the FileExchangeServer.
		 *
		 * @param fesBasePath the path to the base directory of the FES
		 * @param db name to the Shuttle MySQL DB
		 * @param host the host or IP of the MySQL server
		 * @param user the username of the MySQL DB
		 * @param passwd the password of the MySQL DB
		 *
		 * @return ALIHLT_OK if successfull, else an error code
		 */
		int init(std::string fesBasePath, std::string db, std::string host,
				std::string user, std::string passwd);

		/**
		 * Deinitializes the Portal.
		 * This includes closing the connection to the MySQL DB.
		 * Maybe also informing the TaskManager, that the state "COMPLETING"
		 * can be finished.
		 */
		void deinit();

		/**
		 * Function informs, if init has been called sucessfully before.
		 *
		 * @return true, if AliHLTShuttlePortal has been successfully initialized
		 */
		bool isInit();

		/**
		 * Function that handles the mutex, which protects the storing of a 
		 * calibration object and stores away a given AliHLTCalibrationObject
		 * The given Calibartion object inclusive meta data will be analyzed.
		 * Afterwards meta data will be stored to the MySQL DB and the Calibration
		 * BLOB saved inside the FileExchangeServer.
		 * NOTE: the meta data might be changed, if the same UNIQUE key already
		 * exists in the MySQL DB -> the version number of the Calibration object
		 * is increased and the fileId receives a postfix: "_<versionNumber>",
		 * e.g.: "CalibObject_1". If this is the first entry, it is post fixed
		 * by "_0". A link with the original name is pointing to the file with
		 * the latest version number for this calibration object in the FXS.
		 *
		 * @param calibObject the reference to a calibration object received from
		 *			the AliHLTShuttlePortal-PubSub component (Contains the
		 *			calibration object as BLOB and its meta data, which can be
		 *			provided by the DetectorAlgorithm (DA))
		 *
		 * @return ALIHLT_OK in case of success, else on error code
		 */
		int storeCalibObject(AliHLTCalibrationObject& calibObject);

	protected:
		/**
         * Function actually stores away a given AliHLTCalibrationObject
         * The given Calibartion object inclusive meta data will be analyzed.
         * Afterwards meta data will be stored to the MySQL DB and the Calibration
         * BLOB saved inside the FileExchangeServer.
		 * NOTE: the meta data might be changed, if the same UNIQUE key already
         * exists in the MySQL DB -> the version number of the Calibration object
         * is increased and the fileId receives a postfix: "_<versionNumber>",
         * e.g.: "CalibObject_1". If this is the first entry, it is post fixed
         * by "_0". A link with the original name is pointing to the file with
         * the latest version number for this calibration object in the FXS.
         *
         * @param calibObject the reference to a calibration object received from
         *          the AliHLTShuttlePortal-PubSub component (Contains the
         *          calibration object as BLOB and its meta data, which can be
         *          provided by the DetectorAlgorithm (DA))
         *
         * @return ALIHLT_OK in case of success, else on error code
         */
		int doStore(AliHLTCalibrationObject& calibObject);
		
	private:
		/**
		 * Disabled Standart constructor for AliHLTShuttlePortal.
		 */
		AliHLTShuttlePortal();
		
		/**
		 * Function to initialize the store mutex
		 *
		 * @return true if successfull, else false
		 */
		bool initStoreMutex();

		/**
		 * Function to deinitialize the store mutex
		 *
		 * @return true if successfull, else false
		 */
		bool deinitStoreMutex();

		/**
		 * Function to lock the mutex protecting the store procedure of a 
		 * calibration object.
		 *
		 * @return true if successfull, else false
		 */
		bool lockStoreMutex();

		/**
         * Function to unlock the mutex protecting the store procedure of a
         * calibration object.
         *
         * @return true if successfull, else false
         */
        bool unlockStoreMutex();

		/**
		 * Member that defines stores if object is in initialized state.
		 */
		bool mIsInit;

		/**
		 * Member for object that handles the DB connection
		 */
		AliHLTShuttleDBConnector mDBcon;

		/**
		 * Member for object that handles the FES connection
		 */
		AliHLTFESConnector mFEScon;

		/**
		 * Member pointer to the local logger
		 */
		AliHLTShuttlePortalLogger* mpLogger;

		/**
		 * Mutex to protect the store procedure
		 */
		pthread_mutex_t mStoreMutex;

		/*
		 * Maybe some additional data to contact the TaskManager
		 */


}; // end of AliHLShuttlePortal


inline bool AliHLTShuttlePortal::isInit() {
	return mIsInit;
}


inline bool AliHLTShuttlePortal::lockStoreMutex() {
	return ((pthread_mutex_lock(&mStoreMutex) == 0) ? true : false);
}


inline bool AliHLTShuttlePortal::unlockStoreMutex() {
	return ((pthread_mutex_unlock(&mStoreMutex) == 0) ? true : false);
}

#endif // ALI_HLT_SHUTTLE_PORTAL_HPP
