#include "AliHLTShuttlePortalLogger.hpp"
#include "AliHLTShuttlePortalDefines.h"

#include <sstream>

using namespace std;



const int AliHLTShuttlePortalLogger::ALI_HLT_LOGGING_OK = 0;

const int AliHLTShuttlePortalLogger::ALI_HLT_LOGGING_FAILED = -1;


AliHLTShuttlePortalLogger::AliHLTShuttlePortalLogger() {
	mLogLevel = mAll;
}

AliHLTShuttlePortalLogger::AliHLTShuttlePortalLogger(
			AliHLTShuttlePortalLogLevel logLevel) : mLogLevel(logLevel) {
}


AliHLTShuttlePortalLogger::~AliHLTShuttlePortalLogger() {

}

int AliHLTShuttlePortalLogger::logMessage(AliHLTShuttlePortalLogLevel logLevel,
			std::string msg) {
	if (checkLogLevel(logLevel) == ALI_HLT_LOGGING_OK) {
		return pushLogMessage(logLevel, msg);
	}
	return ALI_HLT_LOGGING_FAILED;
}


int AliHLTShuttlePortalLogger::logMessage(AliHLTShuttlePortalLogLevel logLevel,
			std::stringstream msg) {
	return logMessage(logLevel, msg.str());
}


void AliHLTShuttlePortalLogger::setLogLevel(
			AliHLTShuttlePortalLogLevel logLevel) {
	mLogLevel = logLevel;
}


int AliHLTShuttlePortalLogger::checkLogLevel(
			AliHLTShuttlePortalLogLevel logLevel) {
	return ((logLevel & mLogLevel) ? ALI_HLT_LOGGING_OK : ALI_HLT_LOGGING_FAILED);
}


string AliHLTShuttlePortalLogger::convertReturnCode(int code) {
	stringstream retStr;

	switch (code) {
		case (ALIHLT_OK):
			retStr << "OK";
			break;

		case (ALIHLT_NOT_INIT):
			retStr << "Module not initialized";
			break;

        case (ALIHLT_MUTEX_INIT_FAILED):
            retStr << "Store Mutex not initialized";
            break;

        case (ALIHLT_MUTEX_LOCK_FAILED):
            retStr << "Store Mutex could not be locked";
            break;

		case (ALIHLT_SHUTTLE_PORTAL_FATAL_ERROR):
			retStr << "Fatal Shuttle Portal error, need restart";
			break;

		case (ALIHLT_DB_ENTRY_NOT_SET):
			retStr << "Setting of DB entry failed";
			break;

		case (ALIHLT_DB_ENTRY_NOT_UPDATED):
			retStr << "Updating of DB entry failed";
			break;

		case (ALIHLT_DB_CONNECTION_FAILED):
			retStr << "Connection to DB failed";
			break;

		case (ALIHLT_DB_ENTRY_NOT_REMOVED):
			retStr << "Removing of DB entry failed";
			break;

        case (ALIHLT_DB_TEST_SET_FAILED):
            retStr << "Test set (fetching version number) of DB entry failed";
            break;

		case (ALIHLT_FES_STORING_FAILED):
			retStr << "Storing of Calibration BLOB to FXS failed";
			break;

		case (ALIHLT_FES_DIRECTORY_PATH_ERROR):
			retStr << "Creating path in FXS failed";
			break;

		case (ALIHLT_FES_CONNECTION_FAILED):
			retStr << "Connection to FXS failed";
			break;

		case (ALIHLT_FES_INVALID_BASEPATH):
			retStr << "Invalid base path to FXS";
			break;

		case (ALIHLT_DB_RENAMING_UNIQUE_KEY_FAILED):
			retStr << "DB - Renaming unique key (filePath) failed";
			break;

		case (ALIHLT_FES_CALCULATING_CHECKSUM_FAILED):
			retStr << "FXS - Calculating of checksum failed";
			break;
			
		case (ALIHLT_FES_SETTING_LINK_FAILED):
			retStr << "FXS - (Re-)Setting of link to latest file version failed";
			break;

		default:
			retStr << "Unknown error code (" << code << ") received";
	}

	return retStr.str();
}


string AliHLTShuttlePortalLogger::convertState(int state) {
	stringstream retStr;

	switch (state) {
		case (ALIHLT_RECEIVED):
			retStr << "Calibration object received but not saved";
			break;

		case (ALIHLT_DB_TESTED):
			retStr << "MySQL DB tested for duplicity entry";
			break;

		case (ALIHLT_FES_FILLED):
			retStr << "Calibration BLOB stored to FXS";
			break;

		case (ALIHLT_STORAGE_FINISHED):
			retStr << "MySQL DB and FXS successfully filled";
			break;

		case (ALIHLT_STORAGE_ERROR):
			retStr << "Unrecorable ERROR during storage pocedure";
			break;

		default:
			retStr << "Unknown state (" << state << ") of Shuttle Portal";
	}

	return retStr.str();
}

string AliHLTShuttlePortalLogger::convertLogLevel(AliHLTShuttlePortalLogLevel level) {
	stringstream retStr;

	switch (level) {
		case (mNone):
			retStr << "MSG_NONE ";
			break;

		case (mBenchmark):
			retStr << "MSG_BENCH";
			break;

		case (mDebug):
			retStr << "MSG_DEBUG";
			break;

		case (mInfo):
			retStr << "MSG_INFO ";
			break;

		case (mWarning):
			retStr << "MSG_WARN ";
			break;

		case (mError):
			retStr << "MSG_ERROR";
			break;

		case (mFatal):
			retStr << "MSG_FATAL";
			break;

		case (mPrimary):
			retStr << "MSG_PRIM ";
			break;

		default:
			retStr << "MSG_UNKNW";
	}

	return retStr.str();
}


/*
AliHLTShuttlePortalLogLevel AliHLTShuttlePortalLogger::getLogLevel() {
	return mLogLevel;
}
*/

/*
int AliHLTShuttlePortalLogger::logMessage(unsigned int logLevel,
			stringstream msg) {
	int nRet = 0;

	nRet = pushLogMessage(logLevel, msg.str());

	return nRet;
}
*/




