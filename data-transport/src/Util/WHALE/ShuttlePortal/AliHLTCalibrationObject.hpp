#ifndef ALI_HLT_CALIBRATION_OBJECT_HPP
#define ALI_HLT_CALIBRATION_OBJECT_HPP

#include "AliHLTShuttlePortalDefines.h"


#if !defined(_MSC_VER) || _MSC_VER >= 1400
#	define MYSQLPP_SSQLS_NO_STATICS
#	include "AliHLTCalib_DBstruct.h"
//#	include "calib.h"
#endif

#ifdef __DEBUG
#include <iostream>
#endif


#include <string>
#include <sstream>
#include <cstdio>

class AliHLTCalibrationObject {
	public:
		/**
		 * Constructor for a AliHLTCalibrationObject object
		 * all requird initial values are handed in.
		 *
		 * @param runNumber the run number of the calibration object
		 * @param detector the detector of the calibration object
		 * @param fileID the file identifier of the calibration object
		 * @param ddlNumbers the participating DDL numbers of the calibration
		 * 			object
		 * @param blob pointer to the data of the calibration object in memory
		 * @param blobSize size of the calibration object (BLOB) in memory
		 * 			in Bytes
		 * @param blobType type of the calibration data (BLOB, possible types:
		 *			binary, ascii, (ROOT-object), unknown. - default is binary
		 */
		AliHLTCalibrationObject(int runNumber, const char* detector,
				const char* fileID, const char* ddlNumbers, void* blob,
				int blobSize, int blobType = ALIHLT_CALIB_DATA_BIN);


		/**
		 * Destructor for a AliHLTCalibrationObject object.
		 */
		~AliHLTCalibrationObject();

		/**
		 * Copy Constructor for a AliHLTCalibrationObject object.
		 *
		 * @param copyObj reference to the AliHLTCalibrationObject object that
		 * 			shall be copied.
		 */
		AliHLTCalibrationObject(AliHLTCalibrationObject& copyObj);

		/**
		 * Assignment operator for a AliHLTCalibrationObject object.
		 *
		 * @param rhs reference to the AliHLTCalibrationObject object that
		 *			shall be assigned.
		 *
		 * @return a new AliHLTCalibrationObject object, that is an exact copy
		 *			of the original.
         */
		AliHLTCalibrationObject& operator=(AliHLTCalibrationObject& rhs);

		/**
		 * Relational operator for two AliHLTCalibrationObject object.
		 *
		 * @param rhs reference to the AliHLTCalibrationObject object that
		 *			shall be compared.
		 *
		 * @return true, if objects are equal, else false.
		 */
		bool operator==(AliHLTCalibrationObject& rhs);

		/**
		 * Function to see if file identifier has been renamed.
		 * This happens in case of duplicity.
		 * Normally a "_<number>" will be added (e.g. "_2")
		 *
		 * @return true, if file identifier has been renamed, else true
		 */
		bool isRenamed();

		/**
		 * Getter for the current store status
		 * Possible statui: <pre>
         * * ALIHLT_RECEIVED: object just received, but not yet filled anything in portal
         * * ALIHLT_DB_TESTED: MySQL DB contacted to check for duplicity, but not finally entered
         * * ALIHLT_FES_FILLED: Calibration BLOB stored to FileExchangeServer
         * * ALIHLT_STORAGE_FINISHED: MySQL DB updated with all entries, storage procedure finished
         * * ALIHLT_STORAGE_ERROR: indicates, if an unrecoverable error occured during storing
         * </pre>
		 *
		 * @return the current store status (possible values see above)
		 */
		unsigned short getStoreStatus();

		/**
		 * Renames the file identifier of the calibration object.
		 * Should only be called in case of duplicity of the UNIQUE key in the
		 * MySQL DB.This function will add the number to the name like:
		 * "_<number>" will be added (e.g. "CalibFile_2").
		 * In case of the file identifier is longer than 124 chars, it will cut
		 * of the last 4 chars to add the number. Only a positive number of two
		 * digits is allowed.
		 * This function also sets the renamed flag and corrects the file path.
		 *
		 * @param num the number of duplicity, which will be added to file
		 *			identifier. (must be positive and <= 99)
		 * @return true, if successful (number below or equal 99), else false
		 */
		bool renameFileID(unsigned short num);

		/**
		 * Getter for the run number
		 *
		 * @return the run number of this calibration object
		 */
		int	getRunNumber();

		/**
		 * Getter for a string representation of the run number.
		 *
		 * @return string representation of the run number
		 */
		std::string getRunNumberString();

		/**
		 * Getter for the detector
		 *
		 * @return the detector of this calibration object
		 */
		std::string getDetector();

		/**
		 * Getter for the modofied file identifier (using suffix "_<number>").
		 *
		 * @return the modified fileId of this calibration object
		 */
		std::string getFileId();

        /**
		 * Getter for the original file identifier (without suffix "_<number>").
		 * The original fileId is used for the link name pointing to the latest
		 * available calibration object with this specific name
		 *
		 * @return the original fileId of this calibration object
		 */
        std::string getOrigFileId();


		/**
		 * Getter for the DDL number encoded in 64 char string
		 *
		 * @return the DDL numbers of this calibration object
		 */
		std::string getDDLnumbers();

		/**
		 * Getter for the file path of the calibration object.
		 * The File path is relative to the FES root directory.
		 * The file name will always include a suffix of "_<number>" to the 
		 * original file name. <number> starts with '0', meaning the first
		 * entry for one calibartion object will be "CalibName_0".
		 *
		 * @return the relative file path of the calibration object
		 */
		std::string getFilePath();

        /**
         * Getter for the link path for the calibration object.
         * The link path is relative to the FES root directory.
		 * The difference between link path and file path is, that the link
		 * name does NOT contain the suffix "_<number>", but uses the original 
		 * name of the calibration object.
         *
         * @return the relative file path of the calibration object
         */
        std::string getLinkPath();

		/**
		 * Getter for the calibration data in memory.
		 * Returned as a const pointer, so manipulation of data is prohibited.
		 *
		 * @return pointer to calibration data (const)
		 */
		const void* getCalibrationBLOB();

		/**
		 * Getter for the BLOB size
		 *
		 * @return size of the calibration BLOB
		 */
		int getBLOBSize();

		/**
		 * Function to get the time_created value of the calibration file.
		 *
		 * @return doulbe vaue representing the time_created timestamp
		 */
		long long getTimeCreated();

		/**
		 * Getter for a string representation of the time_created member.
		 *
		 * @return string representation of the time_created member
		 */
		std::string getTimeCreatedString();

		/**
		 * Function to get the size of the stored calibration file in the FES.
		 *
		 * @return size of the stored calibration file.
		 */
		int getFileSize();

		/**
		 * Getter for a string representation of the file size.
		 *
		 * @return string representation of the file size
		 */
		std::string getFileSizeString();

		/**
		 * Function to get the checksum of the stored calibration file in the FES.
		 *
		 * @return the checksum of the stored file
		 */
		const char* getFileChecksum();

		/**
		 * Getter for the version number.
		 * Note: the can change after checking if this object already exists
		 * in the MySQL DB. Then the version number is the latest version number
		 * increased by one.
		 *
		 * @return the version number for this object
		 */
		unsigned short getVersionNumber();

		/**
         * Getter for a string representation of the version number.
         *
         * @return string representation of the version number
         */
        std::string getVersionNumberString();

		/**
         * Setter for a new store status
         * Possible statui: <pre>
         * * ALIHLT_RECEIVED: object just received, but not yet filled anything in portal
         * * ALIHLT_DB_TESTED: MySQL DB contacted to check for duplicity, but not finally entered
         * * ALIHLT_FES_FILLED: Calibration BLOB stored to FileExchangeServer
         * * ALIHLT_STORAGE_FINISHED: MySQL DB updated with all entries, storage procedure finished
         * * ALIHLT_STORAGE_ERROR: indicates, if an unrecoverable error occured during storing
         * </pre>
         *
         * @param the new store status (possible values see above)
		 *
		 * @return true, if new status is valid, else false
         */
        bool setStoreStatus(unsigned short newStatus);

		/**
		 * Setter for the file size of the stored calibration object in bytes.
		 *
		 * @param size the file size in bytes
		 */
		void setFileSize(int size);

		/**
		 * Setter for the md5 checksum of the stored calibration file.
		 *
		 * @param checksum string containing the checksum of the stored
		 * 			calibration file.
		 *
		 * @return true, if setting was successful, false in case of checksum
		 *			to long (max 64 chars. normally 32)
		 */
		bool setChecksum(std::string checksum);

		/**
		 * Setter for the time created timestamp
		 * This shall be the time, when calibration file has been stored to FES
		 * as a double.
		 *
		 * @param time time stamp when calibration file has been stored.
		 */
		void setTimeCreated(long long time);

		/**
		 * Creates a time stamp for the time created member using the current
		 * time.
		 *
		 * @return true if creating of time stamp has been successful, else
		 *			false.
		 */
		bool createTimestampCreated();

		/**
		 * Function to set the version number of this calibration object
		 *
		 * @param version the version number for this object
		 */
//		void setVersionNumber(unsigned short version); // NOT needed !!

#ifdef __USE_SSQLS
		/**
		 * Function to create a MySQL++ DB entry struct containing the elements
		 * of this calibration object. If the handed in parameter is set to
		 * true, the struct is also stored locally (to the
		 * AliHLTCalibrationObject object, that receives the call).
		 * A stored AliHLTCalib_DBstruct can be retrieved afterwards with
		 * getStoredDBEntryStruct(): @see getStoredDBEntryStruct()
		 *
		 * @param store if true, the object is also stored inside; if false
		 *			the struct is only created and handed out.
		 *
		 * @return a MsSQL++ DB entry struct as AliHLTCalib_DBstruct (SSQLS)
		 */
		AliHLTCalib_DBstruct createDBEntryStruct(bool store);

		/**
		 * Getter for the originally stored DB entry (MySQL++ struct) of a
		 * calibration object for the Shuttle MySQL DB.
		 * Note: This entry struct can be uninitialized, if it has not been set
		 * before.
		 *
		 * @return stored DB entry (MySQL++ struct - SSQLS)
		 */
		AliHLTCalib_DBstruct getStoredDBEntryStruct();

		/**
		 * Function to store a MySQL++ DB entry struct for later comparision
		 * of DB entries.
		 *
		 * @param store the AliHLTCalib_DBstruct (SSQLS) to store in this
		 *			object.
		 *
		 * @return the old stored MySQL++ DB entry struct
		 *			(Note: can be uninitialized.)
		 */
//		AliHLTCalib_DBstruct storeDBEntryStruct(AliHLTCalib_DBstruct store);
#endif // __USE_SSQLS


	private:
		/**
		 * Std Constructor for a AliHLTCalibrationObject.
		 * The standart C-tor is private, because only a sufficiently filled
		 * AliHLTCalibrationObject shall be created -> no uninitialized object.
		 */
		AliHLTCalibrationObject();

		/**
		 * Run number of the Calibration object
		 */
		int mRunNumber;

        /**
         * Detector of the Calibration object
         */
		char mDetector[4];

        /**
         * File identifier of the Calibration object
         */
		char mFileID[128];

		/**
		 * Original file name
		 */
		char mOrigFileID[128];

        /**
         * Participating DDL numbers of the Calibration object
         */
		char mDDLNumbers[64];

        /**
         * Relative file path of the Calibration object containing the suffix.
		 * (Relative to FES base directory)
         */
		char mFilePath[256];

		/**
 		 * Relative link path of the Calibration object 
 		 * (does not contain the suffix)
 		 * Relative to FES base directory
 		 */
		char mLinkPath[256];

        /**
         * Time stamp of the creation of the Calibration object file
         */
		long long mTimeCreated;

        /**
         * Time stamp of the processing of the Calibration object file
		 * When filling in the Shuttle protal, this will always be zero.
         */
		double mTimeProcessed;

        /**
         * Time stamp of the deletion of the Calibration object file
		 * When filling in the Shuttle protal, this will always be zero.
         */
		double mTimeDeleted;

        /**
         * Size in Bytes of the Calibration object file.
		 * Note: this is the size of the file, when stored to FES. It might be
		 * different to mBLOBSize, the size of the pure BLOB data.
         */
		int mSize;

        /**
         * MD5 checksum of the Calibration object file
         */
		char mFileChecksum[64];

		/**
		 * Pointer to the data of the BLOB containing the calibration object.
		 */
		void* mCalibrationBLOB;

		/**
		 * Size of the BLOB, received from the PubSub component.
		 * NOTE: Might differ from the file size, @see mSize.
		 */
		int mBLOBSize;

		/**
		 * Defines the type of the calibration BLOB data
		 * possible types: binary, ascii, (maybe also ROOT - object), unknown.
		 * Unknown will be handled like binary data.
		 */
		int mBLOBType;

		/**
		 * Shows, if the file identifier has to have been renamed due to
		 * duplicity. Normally a "_<number>" will be added (e.g. "_2")
		 */
		bool mRenamed;

		/**
		 * Indicates the store status of the Calibration object.
		 * Possible statui: <pre>
		 * * ALIHLT_RECEIVED: object just received, but not yet filled anything in portal
		 * * ALIHLT_DB_TESTED: MySQL DB contacted to check for duplicity, but not finally entered
		 * * ALIHLT_FES_FILLED: Calibration BLOB stored to FileExchangeServer
		 * * ALIHLT_STORAGE_FINISHED: MySQL DB updated with all entries, storage procedure finished
		 * * ALIHLT_STORAGE_ERROR: indicates, if an unrecoverable error occured during storing
		 * </pre>
		 */
		unsigned short mStoreStatus;

		/**
		 * Contains the version number for this object.
		 * This number is the latest version number when this object shall be 
		 * stored incremented by one.
		 */
		unsigned short mVersionNumber;

#ifdef __USE_SSQLS
		/**
		 * Pointer to the orignal DB entry struct.
		 */
		AliHLTCalib_DBstruct mStoredCalibEntry;
#endif // __USE_SSQLS

};

inline bool AliHLTCalibrationObject::isRenamed() {
	return mRenamed;
}

inline unsigned short AliHLTCalibrationObject::getStoreStatus() {
	return mStoreStatus;
}

inline int AliHLTCalibrationObject::getRunNumber() {
	return mRunNumber;
}

inline std::string AliHLTCalibrationObject::getRunNumberString() {
	std::stringstream ssRet;
	ssRet << mRunNumber;
	return ssRet.str();
}

inline std::string AliHLTCalibrationObject::getDetector() {
	std::string detector(mDetector);
	return detector;
}

inline std::string AliHLTCalibrationObject::getFileId() {
	std::string fileId(mFileID);
	return fileId;
}

inline std::string AliHLTCalibrationObject::getOrigFileId() {
	std::string origFileId(mOrigFileID);
	return origFileId;
}

inline std::string AliHLTCalibrationObject::getFilePath() {
	std::string path(mFilePath);
	return path;
}

inline std::string AliHLTCalibrationObject::getLinkPath() {
	std::string linkPath(mLinkPath);
	return linkPath;
}

inline std::string AliHLTCalibrationObject::getDDLnumbers() {
	std::string ddlNumbers(mDDLNumbers);
	return ddlNumbers;
}


inline const void* AliHLTCalibrationObject::getCalibrationBLOB() {
	return mCalibrationBLOB;
}

inline int AliHLTCalibrationObject::getBLOBSize() {
	return mBLOBSize;
}

inline long long AliHLTCalibrationObject::getTimeCreated() {
	return mTimeCreated;
}

inline std::string AliHLTCalibrationObject::getTimeCreatedString() {
	std::stringstream ssRet;
	ssRet << mTimeCreated;
	return ssRet.str();
}

inline int AliHLTCalibrationObject::getFileSize() {
	return mSize;
}

inline std::string AliHLTCalibrationObject::getFileSizeString() {
	std::stringstream ssRet;
	ssRet << mSize;
	return ssRet.str();
}

inline const char* AliHLTCalibrationObject::getFileChecksum() {
	return mFileChecksum;
}

inline unsigned short AliHLTCalibrationObject::getVersionNumber() {
	return mVersionNumber;
}

inline std::string AliHLTCalibrationObject::getVersionNumberString() {
    std::stringstream ssRet;
    ssRet << mVersionNumber;
    return ssRet.str();
}


#ifdef __USE_SSQLS
inline AliHLTCalib_DBstruct AliHLTCalibrationObject::getStoredDBEntryStruct() {
	return mStoredCalibEntry;
}
#endif // __USE_SSQLS


// --- Setter Functions --

inline bool AliHLTCalibrationObject::setStoreStatus(unsigned short newStatus) {
	// Validity check for the new state
	switch (newStatus) {
 		case (ALIHLT_RECEIVED):
		case (ALIHLT_DB_TESTED):
		case (ALIHLT_FES_FILLED):
		case (ALIHLT_STORAGE_FINISHED):
		case (ALIHLT_STORAGE_ERROR):
			mStoreStatus = newStatus;
			break;
		default:
			// TODO !!! log message wrong store status
			return false;
	}
	return true;
}

inline void AliHLTCalibrationObject::setFileSize(int size) {
	mSize = size;
}

inline bool AliHLTCalibrationObject::setChecksum(std::string checksum) {
	if (checksum.size() > 64) {
		return false;
	}
	mFileChecksum[sprintf(mFileChecksum, "%s", checksum.c_str())] = 0;
	return true;
}

inline void AliHLTCalibrationObject::setTimeCreated(long long time) {
	mTimeCreated = time;
}

/*
//  THIS FUNCTION IS NOT NEEDED !! 
inline void AliHLTCalibrationObject::setVersionNumber(unsigned short version) {
	mVersionNumber = version;
}
*/

#endif // ALI_HLT_CALIBRATION_OBJECT_HPP

