#include "AliHLTShuttleDBConnector.hpp"

#include "AliHLTShuttleDB_Defines.h"

#include <iostream>
#include <mysql++/result.h>

using namespace mysqlpp;
using namespace std;


AliHLTShuttleDBConnector::AliHLTShuttleDBConnector() {
	mConnected = false;
	mCon = Connection(true); // uses/throws exceptions
	mDb = "";
	mHost = "";
	mUser = "";
	mPasswd = "";
}


AliHLTShuttleDBConnector::AliHLTShuttleDBConnector(string db, string host,
		string user, string passwd) {
	mConnected = false;
	mDb = db;
	mHost = host;
	mUser = user;
	mPasswd = passwd;

	try {
		mCon = Connection(db.c_str(), host.c_str(), user.c_str(), passwd.c_str());
#if MYSQL_VERSION_ID<50067
		if (!mCon.success()) {
#else
		if (!mCon.connected()) {
#endif
			// TODO: call failed ...
			// throw exception ...
		}

		mConnected = true;
	} catch (const mysqlpp::Exception& er) {
		// Catch-all for any other MySQL++ exceptions
		// TODO log message
#ifdef __DEBUG
		cout << "Error: " << er.what() << endl;
#endif
		// throw own exception
	}
}


AliHLTShuttleDBConnector::~AliHLTShuttleDBConnector() {

}


int AliHLTShuttleDBConnector::connect(string db, string host, string user,
		string passwd) {

	try {
		if ((mCon.connected()) && (mConnected) && (mDb.compare(db) == 0) && (mHost.compare(host) == 0)
				&& (mUser.compare(user) == 0)) {
			return ALIHLT_OK;
		}

		mDb = db;
		mHost = host;
		mUser = user;
		mPasswd = passwd;

		if (!mCon.connect(db.c_str(), host.c_str(), user.c_str(), passwd.c_str())) {
			// TODO: call failed ...
			mConnected = false;
			return ALIHLT_DB_CONNECTION_FAILED;
		}

	} catch (const mysqlpp::Exception& er) {
		// Catch-all for any other MySQL++ exceptions
		// TODO log message
#ifdef __DEBUG
		cout << "Error: " << er.what() << endl;
#endif
		mConnected = false;
		return ALIHLT_DB_CONNECTION_FAILED;
	}

	mConnected = true;
	return ALIHLT_OK;
}


void AliHLTShuttleDBConnector::disconnect() {
	if (mCon.connected()) {
#if MYSQL_VERSION_ID<50067
		mCon.close();
#else
		mCon.disconnect();
#endif

	}
	mConnected = false;
}


int AliHLTShuttleDBConnector::setEntry(AliHLTCalibrationObject& calibObject) {
	int nRet = ALIHLT_DB_ENTRY_NOT_SET;
	
	Query query = mCon.query();
	try {
// uses SSSQLS is not usable at the moment with new table structure
/*
#ifdef __USE_SSQLS
		AliHLTCalib_DBstruct calibEntry = calibObject.createDBEntryStruct(true);
		query.insert(calibEntry);
#else
*/
		query << "INSERT INTO " << ALI_HLT_SHUTTLE_DB_COMPLETE_TABLE << 
				" VALUES (" <<
				calibObject.getRunNumber() << ", '" <<
				calibObject.getDetector() << "', '" <<
				calibObject.getOrigFileId() << "', '" <<
				calibObject.getDDLnumbers() << "', '" <<
				calibObject.getLinkPath() << "', " <<
				"0, 0, 0, 0, '0', 0);"; 
				// timestamp (3x), fileSize, checksum, version number
#ifdef __DEBUG	
#if MYSQL_VERSION_ID<50067
		cout << "Executed query: " << query.preview() << endl;
#endif
#endif
//#endif // __USE_SSQLS

		query.execute();
		nRet = ALIHLT_OK;

	} catch (const mysqlpp::BadQuery& e) {
		// TODO !!! make log message
		// Something went wrong with the SQL query.
#ifdef __DEBUG	
		cout << "Set Entry query failed: " << e.what() << endl;
		cout << "File:'" + calibObject.getLinkPath() +
				"' might already exists." << endl;
#endif
		nRet = ALIHLT_DB_ENTRY_NOT_SET;
	} catch (const mysqlpp::Exception& er) {
		// TODO !!! make log message
		// Catch-all for any other MySQL++ exceptions
#ifdef __DEBUG	
		cout << "Set Entry-Error: " << er.what() << endl;
#endif
		nRet = ALIHLT_DB_ENTRY_NOT_SET;
	}

	// set new status in calibration object
	if (nRet == ALIHLT_OK) {
		calibObject.setStoreStatus(ALIHLT_DB_TESTED);
	} else {
		calibObject.setStoreStatus(ALIHLT_STORAGE_ERROR);
	}
	return nRet;
}


int AliHLTShuttleDBConnector::updateEntry(AliHLTCalibrationObject& calibObject) {
	int nRet = ALIHLT_DB_ENTRY_NOT_UPDATED;

	Query query = mCon.query();
	try {
// uses SSSQLS is not usable at the moment with new table structure
/*
#ifdef __USE_SSQLS
		AliHLTCalib_DBstruct calibEntryUpdate = calibObject.createDBEntryStruct(false);
		query.update(calibObject.getStoredDBEntryStruct(), calibEntryUpdate);
#else
*/
		query << "UPDATE " << ALI_HLT_SHUTTLE_DB_TABLE << " SET " <<
				ALI_HLT_SHUTTLE_DB_TIME_CREATED << "=" <<
				calibObject.getTimeCreated() << ", " <<
				ALI_HLT_SHUTTLE_DB_FILE_SIZE << "=" <<
				calibObject.getFileSize() << ", " <<
				ALI_HLT_SHUTTLE_DB_FILE_CHECKSUM << "='" <<
				calibObject.getFileChecksum() << "', " <<
				ALI_HLT_SHUTTLE_DB_FILE_VERSION_NUMBER << "=" << 
				calibObject.getVersionNumber() << " WHERE " <<				
				ALI_HLT_SHUTTLE_DB_KEY << "='" <<
				calibObject.getLinkPath() << "';";
#ifdef __DEBUG	
#if MYSQL_VERSION_ID<50067
		cout << "Executed query: " << query.preview() << endl;
#endif
#endif
//#endif // __USE_SSQLS

		query.execute();

		if (!query) {
			// TODO : log message
#ifdef __DEBUG
			cout << "Update of calib data failed" << endl;
#endif
		} else {
			// TODO : log message
#ifdef __DEBUG
			cout << "hlt_logbook updated successful" << endl;
#endif
			nRet = ALIHLT_OK;
		}

	} catch (const mysqlpp::BadQuery& e) {
		// Something went wrong with the SQL query.
#ifdef __DEBUG
		cout << "Update query failed: " << e.what() << endl;
#endif
		// TODO : log message
	} catch (const mysqlpp::Exception& er) {
		// Catch-all for any other MySQL++ exceptions
#ifdef __DEBUG
		cout << "Update-Error: " << er.what() << endl;
#endif
		// TODO : log message
	}

	// set new status in calibration object
	if (nRet == ALIHLT_OK) {
		calibObject.setStoreStatus(ALIHLT_STORAGE_FINISHED);
	} else {
		calibObject.setStoreStatus(ALIHLT_STORAGE_ERROR);
	}

	return nRet;
}

int AliHLTShuttleDBConnector::removeEntry(AliHLTCalibrationObject& calibObject) {
	int nRet = ALIHLT_DB_ENTRY_NOT_REMOVED;
	Query query = mCon.query();

	try {
// uses SSSQLS is not usable at the moment with new table structure
/*
#ifdef __USE_SSQLS
		// no similar function yet available ion SSQLS
#else
*/
		query << "DELETE FROM " << ALI_HLT_SHUTTLE_DB_TABLE << " WHERE " <<
				ALI_HLT_SHUTTLE_DB_KEY << "='" <<
				calibObject.getLinkPath() << "';";
#ifdef __DEBUG	
#if MYSQL_VERSION_ID<50067
		cout << "Executed query: " << query.preview() << endl;
#endif
#endif
//#endif // __USE_SSQLS

		query.execute();
		if (!query) {
			// TODO : log message
#ifdef __DEBUG
			cout << "Removing of entry failed" << endl;
#endif
		} else {
			// TODO : log message
#ifdef __DEBUG	
			cout << "MySQL DB entry successfully removed" << endl;
#endif
			nRet = ALIHLT_OK;
		}

	} catch (const mysqlpp::BadQuery& e) {
		// Something went wrong with the SQL query.
#ifdef __DEBUG
		cout << "'Remove Entry' query failed: " << e.what() << endl;
#endif
		// TODO : log message
	} catch (const mysqlpp::Exception& er) {
		// Catch-all for any other MySQL++ exceptions
#ifdef __DEBUG
		cout << "Remove-Error: " << er.what() << endl;
#endif
		// TODO : log message
	}

	calibObject.setStoreStatus(ALIHLT_STORAGE_ERROR);
	return nRet;
}

// new since version 0.2: details see header file
int AliHLTShuttleDBConnector::testEntry(AliHLTCalibrationObject& calibObject) {
	int nRet = ALIHLT_DB_TEST_SET_FAILED;
	unsigned short int num = 0;
#if 0 // Unused variable
	unsigned short internalState = 0;
#endif

	Query query = mCon.query();
	try {		
		// get latest version number from table 
        query << "SELECT " << ALI_HLT_SHUTTLE_DB_FILE_VERSION_NUMBER << 
				" FROM " << ALI_HLT_SHUTTLE_DB_TABLE << 
				" WHERE " << ALI_HLT_SHUTTLE_DB_KEY <<
				"='" << calibObject.getLinkPath() << "';";
#ifdef __DEBUG      
#if MYSQL_VERSION_ID<50067
	  	cout << "Executed query: " << query.preview() << endl;
#endif
#endif
		
#if MYSQL_VERSION_ID<50067
		Result result = query.store();
#else
		StoreQueryResult result = query.store();
#endif
		switch ((int) (result.size())) {
			case 1: { 
				// entry already exists, use version number
				Row row = result.at(0);
				num = (unsigned short int) row.at(0);
#ifdef __DEBUG
				cout << "Latest version number: " << num << endl;
#endif
				++num;
				if (calibObject.renameFileID(num)) {
					nRet = ALIHLT_OK;
				}
				break;
			}
			case 0: {
				nRet = setEntry(calibObject);
				break;
			}
			default: {
#ifdef __DEBUG
				cout << "Received more than one version number - System ERROR!" 
						<< endl;
#endif
			}
		}

    } catch (const mysqlpp::BadQuery& e) {
		// TODO !!! make log message
        // Something went wrong with the SQL query.
#ifdef __DEBUG
		cout << "Request to MySQL DB failed with 'BadQuery' exception: " << e.what() << endl;
#endif
		calibObject.setStoreStatus(ALIHLT_STORAGE_ERROR);
    } catch (const mysqlpp::Exception& er) {
    	// TODO !!! make log message
        // Catch-all for any other MySQL++ exceptions
#ifdef __DEBUG
		cout << "Request to MySQL DB failed: " << er.what() << endl;
#endif
        calibObject.setStoreStatus(ALIHLT_STORAGE_ERROR);
    }
	
	return nRet;
}

