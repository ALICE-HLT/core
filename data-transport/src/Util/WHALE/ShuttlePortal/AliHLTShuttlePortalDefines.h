#ifndef ALI_HLT_SHUTTLE_PORTAL_DEFINES_H
#define ALI_HLT_SHUTTLE_PORTAL_DEFINES_H

/**
 * Define for a return value indicating call was successful
 */
#define ALIHLT_OK 0

/**
 * Define for a return value indicating object not initialized
 */
#define ALIHLT_NOT_INIT -1

/**
 * Define for a return value indicating that the store mutex could not be 
 * initialized.
 */
#define ALIHLT_MUTEX_INIT_FAILED -2

/**
 * Define for a return value indicating that locking the store mutex failed
 */
#define ALIHLT_MUTEX_LOCK_FAILED -3

/**
 * Define for return value indictaing fatal shuttle portal error, 
 * reinit required.
 */
#define ALIHLT_SHUTTLE_PORTAL_FATAL_ERROR -9

/**
 * Define for a return value indicating setting of DB entry failed.
 */
#define ALIHLT_DB_ENTRY_NOT_SET -11

/**
 * Define for a return value indicating updating of DB entry failed.
 */
#define ALIHLT_DB_ENTRY_NOT_UPDATED -12

/**
 * Define for a return value indicating connection to DB failed.
 */
#define ALIHLT_DB_CONNECTION_FAILED -13

/**
 * Define for a return value indicating removing of DB entry failed.
 */
#define ALIHLT_DB_ENTRY_NOT_REMOVED -14

/**
 * Define for a return value indicating renaming unique key failed
 */
#define ALIHLT_DB_RENAMING_UNIQUE_KEY_FAILED -15

/**
 * Define for a return value indicating testing DB entry and retrieving
 * version number failed.
 */
#define ALIHLT_DB_TEST_SET_FAILED -16

/**
 * Define for a return value indicating error in storing file to FES
 */
#define ALIHLT_FES_STORING_FAILED -21

/**
 * Define for a return value indicating error in creating directory
 */
#define ALIHLT_FES_DIRECTORY_PATH_ERROR -22

/**
 * Define for a return value indicating file catalogue of FES not reachable
 */
#define ALIHLT_FES_CONNECTION_FAILED -23

/**
 * Define for a return value indicating invalid base path for FES
 */
#define ALIHLT_FES_INVALID_BASEPATH -24

/**
 * Define for a return value indicating calculation of checksum in FXS failed
 */
#define ALIHLT_FES_CALCULATING_CHECKSUM_FAILED -25

/**
 * Define for a return value indicating calculation of checksum in FXS failed
 */
#define ALIHLT_FES_SETTING_LINK_FAILED -26

// --- Calibration store Status defines ---

/**
 * Define for Calibration object store status:
 * ALIHLT_RECEIVED: object just received, but not yet filled anything in
 * portal.
 */
#define ALIHLT_RECEIVED 1

/**
 * Define for Calibration object store status:
 * ALIHLT_DB_TESTED: MySQL DB contacted to check for duplicity, but not
 * finally entered.
 */
#define ALIHLT_DB_TESTED 2

/**
 * Define for Calibration object store status:
 * ALIHLT_FES_FILLED: Calibration BLOB stored to FileExchangeServer.
 */
#define ALIHLT_FES_FILLED 3

/**
 * Define for Calibration object store status:
 * ALIHLT_STORAGE_FINISHED: MySQL DB updated with all entries, storage
 * procedure finished.
 */
#define ALIHLT_STORAGE_FINISHED 4

/**
 * Define for Calibration object store status:
 * ALIHLT_STORAGE_ERROR: indicates, if an unrecoverable error occured during
 * storing.
 */
#define ALIHLT_STORAGE_ERROR 13

/**
 * Defines the maximum version number for a calibration object from one run
 */
#define ALIHLT_CALIB_OBJECT_MAX_VERSION 65000

/**
 * Define for maxinaml tries to set a DB entry in case of duplicity and renaming
 */
#define MAX_TRIES_TO_SET_DB_ENTRY ALIHLT_CALIB_OBJECT_MAX_VERSION

/**
 * Define for the maximum size of the Calibration file postfix 
 * ("_<versionNumber>"). The maximum size should match the number of digits for 
 * ALIHLT_CALIB_OBJECT_MAX_VERSION + 1.
 */
#define CALIB_FILE_SIZE_OF_POSTFIX 6

// ---

/**
 * Define for Calibration BLOB is binary data
 */
#define ALIHLT_CALIB_DATA_BIN 23 // ((int)'CBIN')

/**
 * Define for Calibration BLOB is ascii data
 */
#define ALIHLT_CALIB_DATA_ASCII 24 // ((int)'CASC')

/**
 * Define for Calibration BLOB is ROOT data
 */
#define ALIHLT_CALIB_DATA_ROOT 25 // ((int)'CROO')


#endif //  ALI_HLT_SHUTTLE_PORTAL_DEFINES_H

